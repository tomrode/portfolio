/**
    @file                    Garden_Water_Project Sketch
    @brief                   This file contains all functions related to This Project
    @copyright               Thomas Rode., Delphos, OH  45833
    @date                    12/28/2015

    @remark Author:          Tom Rode
    @remark Project Tree:    Garden_Water_Proj

    @note                    Set up initial tasks

*/

#include <avr/wdt.h>
#include <Arduino.h>
#include <Wire.h>  // Comes with Arduino IDE
// Get the LCD I2C Library here: 
// https://bitbucket.org/fmalpartida/new-liquidcrystal/downloads
// Move any other LCD libraries to another folder or delete them
// See Library "Docs" folder for possible commands etc.
#include <LiquidCrystal_I2C.h>

/* --------------------CONSTANT'S-----------------------------------*/
const byte FALSE               = 0;
const byte TRUE                = 1;
const byte FIVE_MS             = 5;
const byte ONE_HUNDER_MS       = 100;
const int  ONE_THOUSAND_MS     = 1000;
const int  TEN_SECONDS         = 10;

/* ---------------------PIN ASSIGNMENTS-----------------------------*/
const int  BLINK_LED           = 13;
const int  BUTTON1             = 2;
const int  SEN_PWR             = 8;
/*SENSOR #1*/
const int EchoSensor1          = 7;    /* was pingPin7*/
const int TriggerSensor1       = 12;
const int analogInPin          = A0;  // Analog input pin that the potentiometer is attached to

/*  --------------------LIMITS TABLES------------------------------*/
const int LOST_SOIL_ADC_READ   = 0;
const int SOIL_DRY_ADC_READ    = 90;   /* Reading based on test data ADC counts */
const int TANK_LOW_ULTRA_SONIC = 30;   /* Inches */

/*-----( Declare objects )-----*/
// set the LCD address to 0x27 for a 16 chars 2 line display
// A FEW use address 0x3F
// Set the pins on the I2C chip used for LCD connections:
//                    addr, en,rw,rs,d4,d5,d6,d7,bl,blpol
LiquidCrystal_I2C lcd(0x27, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);  // Set the LCD I2C address

/* -------------------Type Def ***********************************/
typedef struct Key_Switches
{
  uint32_t ubHistory[5];
  uint16_t ubPointer;
  uint32_t uwSwitches[6];
} Key_Switches_t;

typedef enum
{
   NORMAL_SCROLL_SELECT          = 0,
   ADC_SOIL_SENSOR_BTN_SELECT    = 1,
   ULTRA_SONIC_SENSOR_BTN_SELECT = 2
}ButtonSlate_t;

typedef enum
{
  SOIL_SENSOR_READ    = 0,  /** < Line 0 */
  SATURATION          = 1,  /** < Line 1 */
  FIELD_CAPACITY      = 2,  /** < Line 1 */
  WILTING_POINT      = 3,  /** < Line 1 */
  ADC_SOIL_SENSOR    = 4,  /** < Line 0 */  
  PERCENT_100_FULL    = 5,  /** < Line 1 */
  PERCENT_90_FULL    = 6,  /** < Line 1 */
  PERCENT_80_FULL    = 7,  /** < Line 1 */
  PERCENT_70_FULL    = 8,  /** < Line 1 */
  PERCENT_60_FULL    = 9,  /** < Line 1 */
  PERCENT_50_FULL    = 10,  /** < Line 1 */
  PERCENT_40_FULL    = 11,  /** < Line 1 */
  PERCENT_30_FULL    = 12,  /** < Line 1 */
  PERCENT_20_FULL    = 13,  /** < Line 1 */
  PERCENT_10_FULL    = 14,  /** < Line 1 */
  PERCENT_00_FULL    = 15,  /** < Line 1 */
  ULTRA_SONIC_SENSOR  = 16  /** < Line 0 */  
}DisplayMessages_t;
/* --------------------Global Variables--------------------------*/

int uwFiveMsTaskCnt = 0;
int uwHundredMsTaskCnt;
int uwThousandMsTaskCnt;
int uwTenSecondsCnt;
char MODULE_LED;
int SensorDuration1, SensorInches1, SensorCm1;
float AdcVoltsRead;
byte TruncatedSoilSenOutputValue = 0; // 8 bit value
int sensorValue = 0;        // value read from the pot
byte ubDisplayState;        // Which display state to be in
Key_Switches_t gKey_Switches;

/**
   @fn     setup()

   @brief   Initializations here .

   @param   N/A

   @return  N/A

   @note    H/W and varaibles setup.

   @author  Tom Rode

   @date    12/28/2015

*/
void setup()
{
  /** ###Functional overview: */

  /** initialize digital pin 13 as an output used to time tasks. */
  pinMode(BLINK_LED, OUTPUT);

  /** Transistor turn on for Moisture sensor*/
  pinMode(SEN_PWR, OUTPUT);

  /** initialize the pushbutton pin as an input */
  pinMode(BUTTON1, INPUT);

  /** initialize serial communications at 9600 bps: */
  Serial.begin(9600);

  /** LCD Init*/
  lcd.begin(16,2);   // initialize the lcd for 16 chars 2 lines, turn on backlight

  // ------- Quick 3 blinks of backlight  -------------
  for(int i = 0; i< 3; i++)
  {
    lcd.backlight();
    delay(250);
    lcd.noBacklight();
    delay(250);
  }
  lcd.backlight(); // finish with backlight on  
  /** Watchdog timer set up with visual indication  */
  wdt_disable();
  MODULE_LED = 0;
  gBlink(25, TRUE);
  wdt_enable(WDTO_500MS);
  Serial.println(  "Initilization" );
}

/**
   @fn     loop()

   @brief   Main Loop

   @param   N/A

   @return  N/A

   @note    Task called here.

   @author  Tom Rode

   @date    12/28/2015

*/
void loop()
{
  /** ###Functional overview: */

  /* 5 MS TASK*/
  if ( FIVE_MS == uwFiveMsTaskCnt )
  {
    /** Fire off the task now */
    gvFiveMsTask();

    /** reset the count to one */
    uwFiveMsTaskCnt = 1;
  }
  else
  {
    /** increment count */
    uwFiveMsTaskCnt++;
  }

  /* 100 MS TASK*/
  if ( ONE_HUNDER_MS == uwHundredMsTaskCnt )
  {
    /** Fire off the task now */
    gvOneHundredMsTask();

    /** reset the count to one */
    uwHundredMsTaskCnt = 1;
  }
  else
  {
    /** increment count */
    uwHundredMsTaskCnt++;
  }

  /* 1000 MS TASK*/
  if ( ONE_THOUSAND_MS ==  uwThousandMsTaskCnt )
  {

    /** Fire off the task now */
    gvOneThousandMsTask();

    /** reset the count to one */
    uwThousandMsTaskCnt = 1;
  }
  else
  {
    /** increment count */
    uwThousandMsTaskCnt++;
  }

  /** Idle State */
  NullTask();

  /** Wait for one millisecond*/
  delay(1);
}

/**
   @fn      NullTask()

   @brief   Task used idle state.

   @param   N/A

   @return  N/A

   @note    N/A.

   @author  Tom Rode

   @date    12/28/2015

*/
void NullTask()
{
  /** ###Functional overview: */

  /** Pet Watchdog*/
  wdt_reset();
}

/**
   @fn      gvFiveMsTask()

   @brief   Task used to de-bounce buttons as needed .

   @param   N/A

   @return  N/A

   @note    Add buttons here.

   @author  Tom Rode

   @date    12/28/2015

*/
void gvFiveMsTask()
{
  static byte ubOneShot;
  static byte ubBtnState;

  /** ###Functional overview: */

  /** Get the switch de-bounced */
  vGetDebRowSw();

  if ( (TRUE == gKey_Switches.uwSwitches[0] ) && ( FALSE == ubOneShot) )
  {
    if ( FALSE == ubOneShot )
    {
      Serial.println(  "Pressed " );
      ubOneShot = TRUE;
      digitalWrite(BLINK_LED, TRUE);   // turn the LED on (HIGH is the voltage level)
    }
    else if ( TRUE == ubOneShot )
    {
      ubOneShot = FALSE;
    }
  }
  else if ( FALSE == gKey_Switches.uwSwitches[0] )
  {
    ubOneShot = FALSE;
    digitalWrite(BLINK_LED, FALSE);   // turn the LED off (Low is the voltage level)
  }
  else {}
  /** Button logic here*/

    /** - Button state machine */
  switch( ubBtnState )
  {
       case NORMAL_SCROLL_SELECT:
           /** - Display will scroll*/
           ubDisplayState = NORMAL_SCROLL_SELECT;
		   
		       if( TRUE == ubOneShot )
		       {
		           /** - Make it possible to read ADC from the soil */
               ubBtnState = ADC_SOIL_SENSOR_BTN_SELECT;
           }		   
           break;  
           
      case ADC_SOIL_SENSOR_BTN_SELECT:
           /** - Display will Read out the ADC from the soil sensor*/
           ubDisplayState = ADC_SOIL_SENSOR_BTN_SELECT;
		   
		       if( TRUE == ubOneShot )
		       {
		           /** - Make it possible to read ADC from the soil */
               ubBtnState = ULTRA_SONIC_SENSOR_BTN_SELECT;
           }		   
           break;  	
           	   
      case ULTRA_SONIC_SENSOR_BTN_SELECT:
           /** - Make it possible to read Ultra Sonic sensor */
           ubDisplayState = ULTRA_SONIC_SENSOR_BTN_SELECT;
		   
		       if( TRUE == ubOneShot )
		       {
		           /** - Make it possible to read Ultra Sonic sensor */
               ubBtnState = NORMAL_SCROLL_SELECT;
           }		   
           break;
		 default:
           break;		 

  } 
  
}

/**
   @fn      gvOneHundredMsTask()

   @brief   Task to read in A-D Moisture sensors, ultra sonic, temp sensor with button presses to see
            raw reads .

   @param   N/A

   @return  N/A

   @note    A/D reads here.

   @author  Tom Rode

   @date    12/28/2015



*/
void gvOneHundredMsTask()
{
  /** ###Functional overview: */
 uint8_t ubTemp;

    if ( ubDisplayState == ADC_SOIL_SENSOR_BTN_SELECT )
    { 
        /** Turn on transistor*/
        digitalWrite(SEN_PWR, TRUE);   // turn the LED on (HIGH is the voltage level)

        /** Trigger the Ultra Sonic Re-instate once done*/
        //gvUltraSonic(void)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    AgvUl1tA~44444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444c5x                                                                            traSonic();

        /** Simple delay for test remove once done*/
        /** Wait for eleven millisecond*/
        delay(11);
    
        /** ADC READ */
        ubTemp = gADCread(ADC_SOIL_SENSOR);
 
        /** Turn off transistor*/
        digitalWrite(SEN_PWR, FALSE);   // turn the LED on (HIGH is the voltage level)

        /** Display Soil raw count */
        gI2CLCD_VarFormat(TRUE,ubTemp, ADC_SOIL_SENSOR);
    }
	else if ( ubDisplayState == ULTRA_SONIC_SENSOR_BTN_SELECT )
	{
	     /** Trigger the Ultra Sonic Re-instate once done*/
        //gvUltraSonic(void);
	
	}
}

/**
   @fn      gvOneThousandMsTask()

   @brief   Task to update display, ultrasonic read.

   @param   N/A

   @return  N/A

   @note    TBD.

   @author  Tom Rode

   @date    12/28/2015

*/
void gvOneThousandMsTask()
{
  /** ###Functional overview: */
uint8_t ubTemp;

  /** - This is an indication of application */
  //MODULE_LED = ~(MODULE_LED & 0x01);
  gBlink(TRUE, TRUE);

  /** ADC READ */
  //gADCread();

  /** TEN Second Task */
  if ( uwTenSecondsCnt >= TEN_SECONDS )
  {
    /** Turn on transistor*/
    //digitalWrite(SEN_PWR, TRUE);   // turn the LED on (HIGH is the voltage level)

    /** Triger the Ultra Sonic Re-instate once done*/
    //gvUltraSonic(void)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    AgvUl1tA~44444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444c5x                                                                            traSonic();

    /** Simple delay for test remove once done*/
    /** Wait for eleven millisecond*/
    //delay(11);
    
    /** ADC READ */
    //ubTemp = gADCread(ADC_SOIL_SENSOR);
 
    /** Turn off transistor*/
    //digitalWrite(SEN_PWR, FALSE);   // turn the LED on (HIGH is the voltage level)

    /** Display something */
    //gI2CLCD_VarFormat(TRUE,ubTemp, ADC_SOIL_SENSOR);

    /** Reset Count */
    uwTenSecondsCnt = 0;
  }

  /** This are is the main algorithm for the operation using ADC reads, Ultrasonic reads, etc. */
  if ( LOST_SOIL_ADC_READ == TruncatedSoilSenOutputValue )
  {
    /** Shut down the pump */
    Serial.println( " LOST SOIL SENSOR INPUT PUMP DISABLED " );
    gvPumpEn(FALSE);
  }


  /** Increment Counts*/
  uwTenSecondsCnt++;
}

/**
   @fn      gvUltraSonic();

   @brief   ultrasonic read.

   @param   N/A

   @return  N/A

   @note    The SR04))) is triggered by a HIGH pulse of 2 or more microseconds.
            Give a short LOW pulse beforehand to ensure a clean HIGH pulse:

            If pin 7 echo lost then watchdogs occur
   @author  Tom Rode

   @date    12/28/2015

*/
void  gvUltraSonic(void)
{

  /* SENSOR 1 HANDLING*/
  pinMode(TriggerSensor1, OUTPUT);
  digitalWrite(TriggerSensor1, LOW);
  delayMicroseconds(2);
  digitalWrite(TriggerSensor1, HIGH);
  delayMicroseconds(5);
  digitalWrite(TriggerSensor1, LOW);

  /** The same pin is used to read the signal from the SR04))): a HIGH */
  /** pulse whose duration is the time (in microseconds) from the sending */
  /** of the ping to the reception of its echo off of an object. */
  pinMode(EchoSensor1, INPUT);
  SensorDuration1 = pulseIn(EchoSensor1, HIGH);

  /** convert the time into a distance */
  SensorInches1 = microsecondsToInches(SensorDuration1);
  SensorCm1 = microsecondsToCentimeters(SensorDuration1);

  /** print the results to the serial monitor: */
  Serial.print(" SensorInches1 = ");
  Serial.print(SensorInches1);
  Serial.print("  SensorCm1 = ");
  Serial.println(  SensorCm1);

  /*END SENSOR 1 HANDLING*/
}

/**
   @fn      long microsecondsToInches(long microseconds)

   @brief   Convert time to inches measurement.

   @param   micro seconds

   @return  inches measurement

   @note    According to Parallax's datasheet for the PING))), there are.
            73.746 microseconds per inch (i.e. sound travels at 1130 feet per
            second).  This gives the distance travelled by the ping, outbound
            and return, so we divide by 2 to get the distance of the obstacle.
            See: http://www.parallax.com/dl/docs/prod/acc/28015-PING-v1.3.pdf

   @author  Tom Rode

   @date    12/28/2015

*/
long microsecondsToInches(long microseconds)
{
  /** ###Functional overview: */

  return microseconds / 74 / 2;
}

/**
   @fn      long microsecondsToCentimeters(long microseconds)

   @brief   Convert time to centimeters measurement.

   @param   micro seconds

   @return  centimeter measurement

   @note    The speed of sound is 340 m/s or 29 microseconds per centimeter.
            The ping travels out and back, so to find the distance of the
            object we take half of the distance travelled.

   @author  Tom Rode

   @date    12/28/2015

*/
long microsecondsToCentimeters(long microseconds)
{
  /** ###Functional overview: */

  return microseconds / 29 / 2;
}

/**
   @fn      gADCread()

   @brief   All ADC conversion read.

   @param   ADC Channel

   @return  ubTemp = 8 nit ADC read

   @note    TBD.

   @author  Tom Rode

   @date    12/28/2015

*/
uint8_t gADCread(uint8_t ADChn)
{
  /** ###Functional overview: */
uint8_t ubTemp;

    if(ADC_SOIL_SENSOR)
    {
        /** read the analog in value */
        sensorValue = analogRead(analogInPin);

        /** truncated lower 8 bits from 10 bit conversion */
        TruncatedSoilSenOutputValue = ( sensorValue >> 2 );

        /** make the read readable in volts */
        AdcVoltsRead = TruncatedSoilSenOutputValue * (5.0 / 255.0);

        /** print the results to the serial monitor: */
        Serial.print(" ADC 8bit = ");
        Serial.print(  TruncatedSoilSenOutputValue);
        Serial.print(" sensor in volts = ");
        Serial.println(  AdcVoltsRead);

        /** output to be returned*/
        ubTemp = TruncatedSoilSenOutputValue;
    }


return(ubTemp); 
}


/**
   @fn      void gBlink(int UwTimes)

   @brief   Blink an output pin for indication.

   @param   UwTimes =  Amount of times to blink,
   @param   ubRepeat = if to blink

   @return  N/A

   @note    TBD.

   @author  Tom Rode

   @date    12/28/2015

*/
void gBlink(int UwTimes, byte ubRepeat)
{
  int uwindex;

  /** ###Functional overview: */

  /** Toggle LED */
  if ( FALSE == ubRepeat )
  {
    /** Back and fourth */
    if ( TRUE == MODULE_LED )
    {
      /** Toggle LED */
      MODULE_LED = FALSE;
      digitalWrite(BLINK_LED, TRUE);   // turn the LED on (HIGH is the voltage level)
    }
    else
    {
      MODULE_LED = TRUE;
      digitalWrite(BLINK_LED, LOW);   // turn the LED on (HIGH is the voltage level)
    }
  }
  else
  {
    /** Amount to blink a pin */
    for ( uwindex = 0; uwindex < UwTimes; uwindex++ )
    {
      /** Blink the indicator LED specified amount of times*/
      if ( TRUE == MODULE_LED )
      {
        MODULE_LED = FALSE;
        digitalWrite(BLINK_LED, HIGH);   // turn the LED on (HIGH is the voltage level)
      }
      else
      {
        MODULE_LED = TRUE;
        digitalWrite(BLINK_LED, LOW);   // turn the LED on (HIGH is the voltage level)
      }

      /** - Delay will be visible */
      delay(100);
    }
  }
}

/**
   @fn       GetDebRowSw(void)

   @brief    This function returns Debounced Switch state

   @param    None

   @return   N/A

   @note     Debounced Switch state after five consecutive 1-ms reads

   @author   Tom Rode

*/
void vGetDebRowSw(void)
{

  /** ###Functional overview: */

  /** - Read in state of push button value */
  gKey_Switches.ubHistory[gKey_Switches.ubPointer] = digitalRead(BUTTON1);

  /** - Capture five same state readings */
  if ((gKey_Switches.ubHistory[0] == gKey_Switches.ubHistory[1]) &&
      (gKey_Switches.ubHistory[1] == gKey_Switches.ubHistory[2]) &&
      (gKey_Switches.ubHistory[2] == gKey_Switches.ubHistory[3]) &&
      (gKey_Switches.ubHistory[3] == gKey_Switches.ubHistory[4]) )

  {
    /** - Five of the same state readings place into respective byte in switches array, index is correct now*/
    gKey_Switches.uwSwitches[0] = gKey_Switches.ubHistory[0];

  }
  /** - increment the count */
  gKey_Switches.ubPointer++;

  /** - Five of the same read state for state latching purposes*/
  if (gKey_Switches.ubPointer > 4)
  {
    /** - reset the count */
    gKey_Switches.ubPointer = 0;
  }

}

/**
   @fn      gvPumpEn();

   @brief   Submersible pump.

   @param   N/A

   @return  N/A

   @note

   @author  Tom Rode

   @date    12/28/2015

*/
void gvPumpEn(byte Enable)
{

}


/**
* @fn    void gI2CLCD_VarFormat(uint8_t ubVal)
*
* @brief  This function Will format a byte worth and places the digit in repective cursor position.
*  
* @param  ubDisplayVal = Flag used to enable/diable 8 bit value dispaly.  
* @param  ubVal = 8 bit value to be displayed. 
* @param  ubMsg = List of predetermined messages to be displayed.
*  
* @return N/A.
* 
* @author Tom Rode 
*  
* @note  N/A 
*
*/
void gI2CLCD_VarFormat(uint8_t ubDisplayVal, uint8_t ubVal, uint8_t ubMsg)
{
uint8_t i;
uint8_t ubOnesOut=0;
uint8_t ubTensCnt=0;
uint8_t ubTensAmt=0;
uint8_t ubTensOut=0;
uint8_t ubHundCnt=0;
uint8_t ubHundAmt=0;
uint8_t ubHundOut=0;

    for (i=0; i<ubVal; i++)
    {
        if ( ubTensCnt >= 9 )
        {
            /** Tally up the amount of tens*/
            ubTensAmt++;

            /** Reset tens count*/
            ubTensCnt=0;
        }
        else
        {
            /** increment tens count*/
            ubTensCnt++;
        } 

        if ( ubHundCnt >= 99 )
        {
            /** Tally up the amount of Hundred*/
            ubHundAmt++;

            /** Reset hundred count*/
            ubHundCnt=0;
        }
        else
        {
          /** increment hundred count*/
          ubHundCnt++;
        } 
    }
        /** Count Correction for output*/
        ubHundOut = ubHundAmt; 
        ubTensOut = (ubHundAmt * 10);
        ubTensOut = (ubTensAmt - ubTensOut);
        ubOnesOut = (ubVal - ((ubHundAmt * 100) + (ubTensOut * 10)));
        /** - ASCII Encoded */
        ubHundOut += 0x30;
        ubTensOut += 0x30;
        ubOnesOut += 0x30;
  
        /** LCD outptu control*/
        lcd.clear();        
        /** - Message line */

        switch ( ubMsg )
        {
            case SATURATION:
                lcd.setCursor(0,0); //Start at character 0 on line 0
                lcd.print("SOIL SENSOR READ");
                lcd.setCursor(0,1);
                lcd.print("SATURATION");
                break;

            case FIELD_CAPACITY:
                lcd.setCursor(0,0); //Start at character 0 on line 0
                lcd.print("SOIL SENSOR READ");
                lcd.setCursor(0,1);
                lcd.print("FIELD CAPACITY");
                break;

            case WILTING_POINT:
                lcd.setCursor(0,0); //Start at character 0 on line 0
                lcd.print("SOIL SENSOR READ");
                lcd.setCursor(0,1);
                lcd.print("WILTING POINT");
                break;

            case ADC_SOIL_SENSOR:
                lcd.setCursor(0,0); //Start at character 0 on line 0
                lcd.print("ADC SOIL SENSOR");
                if ( ubDisplayVal == 1 )
                {
                    lcd.setCursor(5,1);
                    lcd.write(ubHundOut);
                    lcd.setCursor(6,1);
                    lcd.write(ubTensOut);
                    lcd.setCursor(7,1);
                    lcd.write(ubOnesOut);
                }  
                break;
                
            case PERCENT_100_FULL:
                lcd.setCursor(0,0); //Start at character 0 on line 0
                lcd.print("TANK LEVEL READ");
                lcd.setCursor(0,1);
                lcd.print("PERCENT 100 FULL");
                break;

            case PERCENT_90_FULL:
                lcd.setCursor(0,0); //Start at character 0 on line 0
                lcd.print("TANK LEVEL READ");
                lcd.setCursor(0,1);
                lcd.print("PERCENT 90 FULL");
                break;

            case PERCENT_80_FULL:
                lcd.setCursor(0,0); //Start at character 0 on line 0
                lcd.print("TANK LEVEL READ");
                lcd.setCursor(0,1);
                lcd.print("PERCENT 80 FULL");
                break;

            case PERCENT_70_FULL:
                lcd.setCursor(0,0); //Start at character 0 on line 0
                lcd.print("TANK LEVEL READ");
                lcd.setCursor(0,1);
                lcd.print("PERCENT 70 FULL");
                break;

            case PERCENT_60_FULL:
                lcd.setCursor(0,0); //Start at character 0 on line 0
                lcd.print("TANK LEVEL READ");
                lcd.setCursor(0,1);
                lcd.print("PERCENT 60 FULL");
                break;

            case PERCENT_50_FULL:
                lcd.setCursor(0,0); //Start at character 0 on line 0
                lcd.print("TANK LEVEL READ");
                lcd.setCursor(0,1);
                lcd.print("PERCENT 50 FULL");
                break;

            case PERCENT_40_FULL:
                lcd.setCursor(0,0); //Start at character 0 on line 0
                lcd.print("TANK LEVEL READ");
                lcd.setCursor(0,1);
                lcd.print("PERCENT 40 FULL");
                break;

            case PERCENT_30_FULL:
                lcd.setCursor(0,0); //Start at character 0 on line 0
                lcd.print("TANK LEVEL READ");
                lcd.setCursor(0,1);
                lcd.print("PERCENT 30 FULL");
                break;

            case PERCENT_20_FULL:
                lcd.setCursor(0,0); //Start at character 0 on line 0
                lcd.print("TANK LEVEL READ");
                lcd.setCursor(0,1);
                lcd.print("PERCENT 20 FULL");
                break;

            case PERCENT_10_FULL:
                lcd.setCursor(0,0); //Start at character 0 on line 0
                lcd.print("TANK LEVEL READ");
                lcd.setCursor(0,1);
                lcd.print("PERCENT 10 FULL");
                break;

            case PERCENT_00_FULL:
                lcd.setCursor(0,0); //Start at character 0 on line 0
                lcd.print("TANK LEVEL READ");
                lcd.setCursor(0,1);
                lcd.print("PERCENT 00 FULL");
                break;  

            case ULTRA_SONIC_SENSOR:
                lcd.setCursor(0,0); //Start at character 0 on line 0
                lcd.print("ULTRA SONIC SENSOR");
                if ( ubDisplayVal == 1 )
                {
                    lcd.setCursor(5,1);
                    lcd.write(ubHundOut);
                    lcd.setCursor(6,1);
                    lcd.write(ubTensOut);
                    lcd.setCursor(7,1);
                    lcd.write(ubOnesOut);
                }  
                break;                  
              default:
                break;  
        }
        
        
}

