
#include <avr/wdt.h>
#include <Arduino.h>

/*
 * Threshold value   Constant name   Supported on
 * 15ms              WDTO_15MS       ATMega 8, 168, 328, 1280, 2560
 * 30ms              WDTO_30MS       ATMega 8, 168, 328, 1280, 2560 
 * 60ms              WDTO_60MS       ATMega 8, 168, 328, 1280, 2560 
 * 120 ms            WDTO_120MS      ATMega 8, 168, 328, 1280, 2560 
 * 250 ms            WDTO_250MS      ATMega 8, 168, 328, 1280, 2560 
 * 500 ms            WDTO_500MS      ATMega 8, 168, 328, 1280, 2560 
 * 1 s               WDTO_1S         ATMega 8, 168, 328, 1280, 2560 
 * 2 s               WDTO_2S         ATMega 8, 168, 328, 1280, 2560 
 * 4 s               WDTO_4S         ATMega 168, 328, 1280, 2560 
 * 8 s               WDTO_8S         ATMega 168, 328, 1280, 2560 
 */


void setup() {
   // put your setup code here, to run once:
   wdt_disable(); 

   // the following forces a pause before enabling WDT. This gives the IDE a chance to
   // call the bootloader in case something dumb happens during development and the WDT
   // resets the MCU too quickly. Once the code is solid, remove this.

   delay(2L * 1000L);

   wdt_enable(WDTO_4S);

}

void loop() {
  // put your main code here, to run repeatedly:

  // kick watch dog
  wdt_reset();
}


