
// ESP8266 test code 
// Plot sensors data on thingspeak.com using an Arduino and an ESP8266 WiFi module
// By deba168 on 23/02/2015

#include <SoftwareSerial.h>
#define SOL_AMPS_CHAN 1                // Defining the adc channel to read solar amps
#define SOL_VOLTS_CHAN 0               // defining the adc channel to read solar volts
#define BAT_VOLTS_CHAN 2               // defining the adc channel to read battery volts

#define AVG_NUM 8                      // number of iterations of the adc routine to average the adc readings
#define SOL_AMPS_SCALE 0.0251          // the scaling value for raw adc reading to get solar amps   // (5/1024)*(R1+R2)/R2
#define SOL_VOLTS_SCALE 0.02976        // the scaling value for raw adc reading to get solar volts
#define BAT_VOLTS_SCALE 0.02976          // the scaling value for raw adc reading to get battery volts 

float sol_amps;                         // solar amps 
float sol_volts;                        // solar volts 
float bat_volts;                        // battery volts 
float sol_watts;                        // solar watts
int ledPin = 13;

// replace with your channel's thingspeak API key
String apiKey = "DPK8RMTFY2B1XCAF";

// connect 2 to TX of Serial USB
// connect 3 to RX of serial USB
SoftwareSerial ser(2,3); // RX, TX

// this runs once
void setup() {                
  // initialize the digital pin as an output.
  pinMode(ledPin, OUTPUT);    

  // enable debug serial
  Serial.begin(9600); 
  // enable software serial
  ser.begin(9600);
  
  // reset ESP8266
  ser.println("AT+RST");
}
 
void loop()
{
  
  // blink LED on board
  digitalWrite(ledPin, HIGH);   
  delay(200);               
  digitalWrite(ledPin, LOW);

 read_data();         //read data from sensors
 esp_8266();
}
//------------------------------------------------------------------------------------------------------
// This routine reads and averages the analog inputs for this system, solar volts, solar amps and 
// battery volts. 
//------------------------------------------------------------------------------------------------------
int read_adc(int channel)
{
  
  int sum = 0;
  int temp;
  int i;
  
  for (i=0; i<AVG_NUM; i++) {            // loop through reading raw adc values AVG_NUM number of times  
    temp = analogRead(channel);          // read the input pin  
    sum += temp;                        // store sum for averaging
    delayMicroseconds(50);              // pauses for 50 microseconds  
  }
  return(sum / AVG_NUM);                // divide sum by AVG_NUM to get average and return it
}


//------------------------------------------------------------------------------------------------------
// This routine reads all the analog input values for the system. Then it multiplies them by the scale
// factor to get actual value in volts or amps. 
//------------------------------------------------------------------------------------------------------
void read_data(void) {
  
  sol_amps =  (read_adc(SOL_AMPS_CHAN)  * SOL_AMPS_SCALE -13.51);    //input of solar amps
  sol_volts =  read_adc(SOL_VOLTS_CHAN) * SOL_VOLTS_SCALE;   //input of solar volts 
  bat_volts =  read_adc(BAT_VOLTS_CHAN) * BAT_VOLTS_SCALE;  //input of battery volts 
  sol_watts =  sol_amps * sol_volts  ;   //calculations of solar watts                  
}
//------------------------------------------------------------------------------------------------------
// ----------Uploading system data to www.thingspeaks.com by ESP8266 WiFi Module 
//------------------------------------------------------------------------------------------------------

void esp_8266()
{
 // convert to string
  char buf[32];
  String strTemp1 = dtostrf( sol_volts, 4, 1, buf);
  String strTemp2 = dtostrf( sol_amps, 4, 1, buf);
  String strTemp3 = dtostrf( sol_watts, 4, 1, buf);
  String strTemp4 = dtostrf( bat_volts, 4, 1, buf);
  Serial.print("Solar Voltage =");
  Serial.print(strTemp1);
  Serial.println("V");
  Serial.print("Solar Current =");
  Serial.print(strTemp2);
  Serial.println("A");
  Serial.print("Solar Power =");
  Serial.print(strTemp3);
  Serial.println("W");
  Serial.print("Battery Voltage =");
  Serial.print(strTemp4);
  Serial.println("V");
  // TCP connection
  String cmd = "AT+CIPSTART=\"TCP\",\"";
  cmd += "184.106.153.149"; // api.thingspeak.com
  cmd += "\",80";
  ser.println(cmd);
   
  if(ser.find("Error")){
    Serial.println("AT+CIPSTART error");
    return;
  }
  
  // prepare GET string
  String getStr = "GET /update?api_key=";
 /* 
  getStr = apiKey;
  getStr +="&field1=" +String(strTemp1) +"&field2="+ String(strTemp2) + "&field3=" + String(strTemp3)+"\r\n\r\n";
 */

  getStr += apiKey;
  getStr +="&field1=";
  getStr += String(strTemp1);
  getStr +="&field2=";
  getStr += String(strTemp2);
  getStr +="&field3=";
  getStr += String(strTemp3);
  getStr +="&field4=";
  getStr += String(strTemp4);
  getStr += "\r\n\r\n";

  // send data length
  cmd = "AT+CIPSEND=";
  cmd += String(getStr.length());
  ser.println(cmd);

  if(ser.find(">")){
    ser.print(getStr);
  }
  else{
    ser.println("AT+CIPCLOSE");
    // alert user
    Serial.println("AT+CIPCLOSE");
  }
    
  // thingspeak needs 15 sec delay between updates
  delay(16000);  
}
