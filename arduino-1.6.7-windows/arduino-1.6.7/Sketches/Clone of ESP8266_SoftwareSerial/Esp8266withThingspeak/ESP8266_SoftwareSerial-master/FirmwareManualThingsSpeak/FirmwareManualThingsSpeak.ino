      /*  Robo-Plan Technologies LTD: 08/2016

   The purpose of this Arduino Sketch is to check the ESP8266-01 as a peripheral wifi board connected to
   an Arduino Uno board via a 5-3.3 logic converter with SoftwareSerial using ESP8266 AT command library created by ITEAD.
   
   The original library can be found: https://github.com/itead/ITEADLIB_Arduino_WeeESP8266

   Our goal was to create an Esp8266 AT command library (based on ITEAD library), 
   that would work well on software serial on most ESP8266 devices, no matter the firmware, or the initial baudrate.
   
   Therefore, we are distributing this preliminary library. Please connect the Esp8266-01 and run this example.
   Write to us about any bugs, comments, issues, improvement proposals, etc.

   We have modified the original library due to Software serial baudrate problems.
   Now, the initialize function, when using software serial only, will set the ESP8266 baudrate to 9600.
   
   The sketch sets the ESP8266 baudrate to 9600 by default for software serial and to 115200 with hardware serial. 
   Then it connects to your AP, checks the version of the ESP8266, sends a TCP request to google.com and displays the response on the serial monitor.

   Notes:
   -  In order to run the example, first connect the ESP8266 via Software Serial to your arduino board using a logic converter,
        as shown in the wiring figure attached.
   -  Enter your SSID and PASSWORD below.
   -  If using SoftwareSerial make sure the line "#define ESP8266_USE_SOFTWARE_SERIAL" in ESP8266.h is uncommented.


 Troubleshooting:
   -  If you receive partial response from the esp8266 when using software serial - 
      go to C:\Program Files (x86)\Arduino\hardware\arduino\avr\libraries\SoftwareSerial\src\SoftwareSerial.h
      Change line 42: #define _SS_MAX_RX_BUFF 64 // RX buffer size
      To: #define _SS_MAX_RX_BUFF 256 // RX buffer size
      this will enlarge the software serial buffer.
   -  Sometime setting the baudrate on initialization fails, try resetting the Arduino, it should work fine.
   
*/
#include <SoftwareSerial.h>
#include "ESP8266.h"
//#include "ThingSpeak.h"

const char *SSID     = "CrownGuest";//"NETGEAR36_2GEXT";//"CrownGuest";//"WIFI-SSID";
const char *PASSWORD = "Access123";//"festiveapple513";//"Access123";//"WIFI-PASWWORD";

// replace with your channel's thingspeak API key Write API key this is Garden Waterer//TomsTest
String apiKey = "H7Z21QYQ36G7VTAD";//"GIU176KPWXU1VYXY";

//SoftwareSerial ser(8,9);//(10, 11); // Rx, Tx SoftwareSerial pins for MEGA/Uno. For other boards see: https://www.arduino.cc/en/Reference/SoftwareSerial
SoftwareSerial ser(10, 11); // Rx, Tx SoftwareSerial pins for MEGA/Uno. For other boards see: https://www.arduino.cc/en/Reference/SoftwareSerial


ESP8266 wifi(ser);

static byte APListCnt = 0;

void setup(void)
{
  // This is used to debug remove me once done
   pinMode(13, OUTPUT);
   digitalWrite(13, LOW);
   
  //Start Serial Monitor at any BaudRate
  Serial.begin(57600);
  Serial.println("Begin");
  
  if (!wifi.init(SSID, PASSWORD))
  {
    //digitalWrite(13, HIGH); 
    Serial.println("Wifi Init failed. Check configuration.");
    
    while (true) ; // loop eternally  
  } 
   //digitalWrite(13, HIGH);  
}

void loop(void)
{         
     Serial.println("Sending Request to ThingsSpeak");
     esp_8266();  
}
static float bat_volt = 1;
static byte WaterOutput = 0x01;
static int Cnt;
void esp_8266()
{
 // convert to string
  char buf[32];

  //WaterOutput = ~(WaterOutput);//(WaterOutput & 01);
  if(WaterOutput == 1)
  {
     WaterOutput = 0;
  }
  else
  {
     WaterOutput = 1;
  }
  String strVolt = dtostrf( bat_volt, 4, 1, buf);
  String strVolt2 = dtostrf( (bat_volt + 20), 4, 1, buf);
  
  Serial.print(strVolt);
  Serial.println(" V1");
  Serial.print(strVolt2);
  Serial.println(" V2");
  Serial.print(WaterOutput);
  Serial.println(" Waterstate");
  Serial.print(Cnt);
  Serial.println(" Counts of loop");
  
  // TCP connection
  String cmd = "AT+CIPSTART=\"TCP\",\"";
  cmd += "184.106.153.149"; // api.thingspeak.com
  cmd += "\",80";
  ser.println(cmd);
   
  if(ser.find("Error")){
    Serial.println("AT+CIPSTART error");
    return;
  }
  
  // prepare GET string
  String getStr = "GET /update?api_key=";
  getStr += apiKey;
  getStr +="&field1=";
  getStr += String(WaterOutput);
  getStr +="&field2=";
  getStr += String(strVolt2);
  getStr +="&field3=";
  getStr += String(strVolt);
  getStr += "\r\n\r\n";

  // send data length
  cmd = "AT+CIPSEND=";
  cmd += String(getStr.length());
  ser.println(cmd);
  if(ser.find(">")){
    /* Test to see whats going out */
    //Serial.print(getStr);
    ser.print(getStr);    
  }
  else{
    ser.println("AT+CIPCLOSE");
    // alert user
    Serial.println("AT+CIPCLOSE");

    if (!wifi.init(SSID, PASSWORD))
    { 
        Serial.println("Wifi Init failed. Check configuration.");
    }    
  }
  
   bat_volt++; 
  // thingspeak needs 15 sec delay between updates
  delay(16000);//(600000);//(120000);//(16000); 
  Cnt++; 
}


