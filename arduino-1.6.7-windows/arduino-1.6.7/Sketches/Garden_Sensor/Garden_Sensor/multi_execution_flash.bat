copy %5 vcast.exe

echo flash burn -baseaddress=0 -type=elf  -offset=0 -executable=vcast.exe -endian=auto -rambase=0x40000000 -script=%3 -eraseonly=no -unlock=yes -verify=yes -memrequire=256 > mp_flash.p
echo hardbrk vCAST_END >> mp_flash.p
echo output -multi -io -noprefix multi_standard_output.txt >> mp_flash.p
echo c;wait >> mp_flash.p
echo quitall >> mp_flash.p
 

%1 %2 -remote "setup=%3 mpserv %4 -usb" -p mp_flash.p vcast.exe

 
type multi_standard_output.txt