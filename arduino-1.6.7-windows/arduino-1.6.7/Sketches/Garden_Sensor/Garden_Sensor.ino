/**
*    @file                    Garden Sensor Sketch
*    @brief                   This file contains all functions related to This Project
*    @copyright               Thomas Rode., Delphos, OH  45833
*    @date                    5/24/2017
*
*    @remark Author:          Tom Rode
*    @remark Project Tree:    Garden_Sensor_Proj
*
*    @note        Ver 1.1     This is the code to run
*    
*/

#include <math.h>         //loads the more advanced math functions
#include <SoftwareSerial.h>
#include "ESP8266.h"


/* -------------------Enumerations  ------------------------------*/
typedef enum
{
  ADC_SOIL_SENSOR           = A0,   /** < Line 0 */
  VAL1                      = 1,  /** < Line 0 */
  VAL2                      = 2,  /** < Line 1 */
  VAL3                      = 3,  /** < Line 1 */
  VAL4                      = 4,  /** < Line 1 */
  NTC_THERMISTOR            = A5,  /** < Line 1  */
  
}ADCChn_t;
/* --------------------CONSTANT'S-----------------------------------*/
const byte  FALSE                                   = 0;
const byte  TRUE                                    = 1;
const int   FIVE_HUNDERD_MS                         = 500;
const int   ONE_THOUSAND_MS                         = 1000;
const int   SIXTY_THOUSAND_MS                       = 20000;//60000;
/* ---------------------PIN ASSIGNMENTS-----------------------------*/
const byte  SEN_PWR                                 = 8; // Arduino UNO
const byte  NTC_IN                                  = NTC_THERMISTOR; // Arduino UNO
const byte  MOISTURE_IN                             = ADC_SOIL_SENSOR;  // Analog input pin that the potentiometer is attached to
const byte  SFW_RX                                  = 10; // Software serial Rx
const byte  SFW_TX                                  = 11; // Software serial Tx
/* ---------------------Wifi Settings-----------------------------*/
const char *SSID     = "CrownGuest";//"NETGEAR36_2GEXT";//"CrownGuest";//"WIFI-SSID";
const char *PASSWORD = "Access123";//"festiveapple513";//"Access123";//"WIFI-PASWWORD";

/* -------------------Global Variables------------------------------*/
int uwFiveHundredMsTaskCnt;
int uwThousandMsTaskCnt;
int uwSixtyThousandMsTaskCnt;

// replace with your channel's thingspeak API key Write API key this is Garden Waterer//TomsTest
String apiKey = "H7Z21QYQ36G7VTAD";//"GIU176KPWXU1VYXY";
SoftwareSerial ser(SFW_RX, SFW_TX); // Rx, Tx SoftwareSerial pins for MEGA/Uno. For other boards see: https://www.arduino.cc/en/Reference/SoftwareSerial
ESP8266 wifi(ser);


/**
   @fn     setup()

   @brief   Initializations here .

   @param   N/A

   @return  N/A

   @note    H/W and varaibles setup.

   @author  Tom Rode

   @date    5/24/2017

*/
void setup() 
{
    /** ###Functional overview: */
    
    /** initialize serial communications at 57600 bps: */
    Serial.begin(57600);
    Serial.println(  "Initilization of Garden Sensor" );
  
    /** Transistor High Side turn on for Moisture sensor*/
    pinMode(SEN_PWR, OUTPUT);

    if (!wifi.init(SSID, PASSWORD))
    {
        //digitalWrite(13, HIGH); 
        Serial.println("Wifi Init failed. Check configuration.");
    
        while (true) ; // loop eternally  
    } 

}

/**
   @fn     loop()

   @brief   Main Loop

   @param   N/A

   @return  N/A

   @note    Task called here.

   @author  Tom Rode

   @date    5/24/2017

*/
void loop()
{
  /** ###Functional overview: */

  /* 500 MS TASK*/
  if ( FIVE_HUNDERD_MS == uwFiveHundredMsTaskCnt )
  {
    /** Fire off the task now */
    gvFiveHundredMsTask();

    /** reset the count to one */
    uwFiveHundredMsTaskCnt = 1;
  }
  else
  {
    /** increment count */
    uwFiveHundredMsTaskCnt++;
  }

  /* 1000 MS TASK*/
  if ( ONE_THOUSAND_MS == uwThousandMsTaskCnt )
  {

    /** Fire off the task now */
    gvOneThousandMsTask();

    /** reset the count to one */
    uwThousandMsTaskCnt = 1;
  }
  else
  {
    /** increment count */
    uwThousandMsTaskCnt++;
  }

   /* 60000 MS TASK or ONE Minute*/
  if ( SIXTY_THOUSAND_MS == uwSixtyThousandMsTaskCnt )
  {

    /** Fire off the task now */
    gvSixtyThousandMsTask();

    /** reset the count to one */
    uwSixtyThousandMsTaskCnt = 1;
  }
  else
  {
    /** increment count */
   uwSixtyThousandMsTaskCnt++;
  }
  /** Idle State */
  //NullTask();

  /** Wait for one millisecond*/
  delay(1);
}

/**
   @fn      NullTask()

   @brief   Task used idle state.

   @param   N/A

   @return  N/A

   @note    N/A.

   @author  Tom Rode

   @date    5/24/2017

*/
void NullTask()
{
  /** ###Functional overview: */

  /** Pet Watchdog*/
  //wdt_reset();

  /** - Pet the Hardware watch dog */
  //gvHWWatchdog();
}

/**
   @fn      gvFiveHundredMsTask()

   @brief   None.

   @param   N/A

   @return  N/A

   @note    

   @author  Tom Rode

   @date     5/24/2017

*/
void gvFiveHundredMsTask()
{ }

/**
   @fn      gvOneThousandMsTask()

   @brief   None.

   @param   N/A

   @return  N/A

   @note    

   @author  Tom Rode

   @date     5/24/2017

*/
void gvOneThousandMsTask()
{
   
      
}

/**
   @fn      gvSixtyThousandMsTask()

   @brief   None.

   @param   N/A

   @return  N/A

   @note    

   @author  Tom Rode

   @date     5/24/2017

*/
void gvSixtyThousandMsTask()
{
    double NTCVal;
    uint8_t MoistureSensor; 
    
    /** ###Functional overview: */

    /** - Collect values from Sensors */
    NTCVal = gvNTCThermisterRead();
    MoistureSensor = gADCread(ADC_SOIL_SENSOR);
    
    Serial.println("Sending Request to ThingsSpeak");
    esp_8266(NTCVal,MoistureSensor);
  
}

/**
   @fn      gADCread()

   @brief   All ADC conversion read.

   @param   ADC Channel

   @return  ubTemp = 16bit ADC read

   @note    TBD.

   @author  Tom Rode

   @date    12/28/2015

*/
uint16_t gADCread(ADCChn_t ADChn)
{
  /** ###Functional overview: */
uint8_t ubTemp;
int sensorValue = 0;                   // value read from the pot
byte TruncatedSoilSenOutputValue = 0; // 8 bit value
float AdcVoltsRead;

    if( ADChn == ADC_SOIL_SENSOR)
    {
        /** read the analog in value */
        sensorValue = analogRead(MOISTURE_IN);

        /** truncated lower 8 bits from 10 bit conversion */
        TruncatedSoilSenOutputValue = ( sensorValue >> 2 );

        /** make the read readable in volts */
        AdcVoltsRead = TruncatedSoilSenOutputValue * (5.0 / 255.0);

        /** print the results to the serial monitor: */
        //Serial.print(" ADC 8bit = ");
        //Serial.print(  TruncatedSoilSenOutputValue);
        //Serial.print(" sensor in volts = ");
        //Serial.println(  AdcVoltsRead);

       /** output to be returned*/
       return(TruncatedSoilSenOutputValue);
    }
    else if ( ADChn == NTC_THERMISTOR )
    {
        sensorValue=analogRead(NTC_IN); //Read the analog port 1 and store the value in val
        return(sensorValue);
    }
}

/**
   @fn      gvNTCThermisterRead()

   @brief   None.

   @param   N/A

   @return  N/A

   @note    

   @author  Tom Rode

   @date     5/24/2017

*/
double gvNTCThermisterRead()
{ 
   
    int val;                //Create an integer variable
    double temp;            //Variable to hold a temperature value
   
    /** ###Functional overview: */
  
    //val=analogRead(NTC_IN); //Read the analog port 1 and store the value in val
    val=gADCread(NTC_THERMISTOR);
    temp=Thermister(val);   //Runs the fancy math on the raw analog value
    //Serial.println(temp);   //Print the value to the serial port
    
    return temp;
}

/**
   @fn      double Thermister(int RawADC)

   @brief   None.

   @param   N/A

   @return  double

   @note    

   @author  Tom Rode

   @date     5/24/2017

*/
double Thermister(int RawADC) 
{
    
    double Temp;

    /** ###Functional overview: */

    /** - Function to perform the fancy math of the Steinhart-Hart equation */
    Temp = log(((10240000/RawADC) - 10000));
    Temp = 1 / (0.001129148 + (0.000234125 + (0.0000000876741 * Temp * Temp ))* Temp );
    Temp = Temp - 273.15;              // Convert Kelvin to Celsius
    Temp = (Temp * 9.0)/ 5.0 + 32.0; // Celsius to Fahrenheit - comment out this line if you need Celsius
    
    return Temp;
}

/**
   @fn      esp_8266()

   @brief   None.

   @param   N/A

   @return  N/A

   @note    

   @author  Tom Rode

   @date     5/24/2017

*/
static float bat_volt = 1;
static int Cnt;
void esp_8266(double NTCValue, uint8_t MoistureSensor)
{
 // convert to string
  char buf[32];

  String strVolt = dtostrf( bat_volt, 4, 1, buf);
  String strVolt2 = dtostrf( (bat_volt + 20), 4, 1, buf);
  
  Serial.print(strVolt);
  Serial.println(" V1");
  Serial.print(MoistureSensor);
  Serial.println(" Moisture Reading");
  Serial.print(NTCValue);
  Serial.println(" NTC Value");
  Serial.print(Cnt);
  Serial.println(" Counts of loop");
  
  // TCP connection
  String cmd = "AT+CIPSTART=\"TCP\",\"";
  cmd += "184.106.153.149"; // api.thingspeak.com
  cmd += "\",80";
  ser.println(cmd);
   
  if(ser.find("Error")){
    Serial.println("AT+CIPSTART error");
    return;
  }
  
  // prepare GET string
  String getStr = "GET /update?api_key=";
  getStr += apiKey;
  getStr +="&field1=";
  getStr += String(NTCValue);
  getStr +="&field2=";
  getStr += String(MoistureSensor);
  getStr +="&field3=";
  getStr += String(strVolt);
  getStr += "\r\n\r\n";

  // send data length
  cmd = "AT+CIPSEND=";
  cmd += String(getStr.length());
  ser.println(cmd);
  if(ser.find(">")){
    /* Test to see whats going out */
    //Serial.print(getStr);
    ser.print(getStr);    
  }
  else{
    ser.println("AT+CIPCLOSE");
    // alert user
    Serial.println("AT+CIPCLOSE");

    if (!wifi.init(SSID, PASSWORD))
    { 
        Serial.println("Wifi Init failed. Check configuration.");
    }    
  }
  
   bat_volt++; 
  // thingspeak needs 15 sec delay between updates
  //delay(16000);//(600000);//(120000);//(16000); 
  Cnt++; 
}
