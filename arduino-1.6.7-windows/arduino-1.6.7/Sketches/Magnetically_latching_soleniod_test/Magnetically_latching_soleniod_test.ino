const byte TRANS_1 = 4;
const byte TRANS_2 = 5;
const byte TRANS_3 = 6;
const byte TRANS_4 = 7;
const byte SOL_LATCH_DELAY = 100;//50;
static int CycleCnt;

void setup() {
    // put your setup code here, to run once:
    // set the digital pin as output:
    pinMode(TRANS_1, OUTPUT);
    pinMode(TRANS_2, OUTPUT);
    pinMode(TRANS_3, OUTPUT);
    pinMode(TRANS_4, OUTPUT);

   // set the output to bridge off initially:
   digitalWrite(TRANS_1, LOW);
   digitalWrite(TRANS_2, LOW);
   digitalWrite(TRANS_3, LOW);
   digitalWrite(TRANS_4, LOW);

   // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
}

void loop() {
    // put your main code here, to run repeatedly:
    digitalWrite(TRANS_1, HIGH);
    digitalWrite(TRANS_3, HIGH);
   
    // delay according to spec
    delay(SOL_LATCH_DELAY);

    digitalWrite(TRANS_1, LOW);
    digitalWrite(TRANS_3, LOW);

    // print out the value you read:
    Serial.println("Open");
   
    // delay two seconds turn on the other way
    delay(2000);

    // All off
    digitalWrite(TRANS_2, HIGH);
    digitalWrite(TRANS_4, HIGH);

    // delay according to spec
    delay(SOL_LATCH_DELAY);

    digitalWrite(TRANS_2, LOW);
    digitalWrite(TRANS_4, LOW);

    // print out the value you read:
    Serial.println("Closed");
    // delay two seconds turn on the other way
    delay(2000);

    Serial.println(" Cycle Count ");
    Serial.println(CycleCnt++);

}
