#include <EEPROM.h>

/** the current address in the EEPROM (i.e. which byte we're going to write to next) **/
int addr = 0;

void setup() {
  // put your setup code here, to run once:
  
  Serial.begin(9600);
  Serial.write("Power On");

}



char inData[20]; // Allocate some space for the string
char inChar; // Where to store the character read
byte index = 0; // Index into array; where to store the character
char value;
static byte WriteOnce = 0;
void loop()
{
   while(Serial.available() > 0) // Don't read unless
                                                  // there you know there is data
   {
       if(index < 19) // One less than the size of the array
       {
           inChar = Serial.read(); // Read a character
           EEPROM.write(addr, inChar); // Save to EEPROM
           addr++; //increment EEPROM address
           inData[index] = inChar; // Store it
           index++; // Increment where to write next
           inData[index] = '\0'; // Null terminate the string
       }
   }
   // Now do something with the string (but not using ==)

   if ((index > 18) && (WriteOnce == 0))
   {
      Serial.write("Here I AM");

      //read a byte from the current address of the EEPROM
      for (index = 0; index < 19; index++)
      {    
           value = EEPROM.read(index);
           Serial.print(value); 
      }
      WriteOnce = 1;
   }
   
}

/*
 // read a byte from the current address of the EEPROM
           for (index = 0; index < 19; index++)
           {    
               value = EEPROM.read(index);
               Serial.print(value); 
           }*/
           
