/**
 *  @file                    Garden_Water_Project Sketch
 *  @brief                   This file contains all functions related to This Project
 *  @copyright               Thomas Rode., Delphos, OH  45833
 *  @date                    12/28/2015
 *
 *  @remark Author:          Tom Rode
 *  @remark Project Tree:    Garden_Water_Proj
 *
 *  @note                    Set up initial tasks
 *
 */

#include <avr/wdt.h>
#include <Arduino.h>

/* --------------------CONSTANT'S-------------------------------*/
const byte FALSE            = 0;
const byte TRUE             = 1;
const byte FIVE_MS          = 5;
const byte ONE_HUNDER_MS    = 100;
const int  ONE_THOUSAND_MS  = 1000;
const int  TEN_SECONDS      = 10;
const int  BLINK_LED        = 13;
const int  BUTTON1          = 2;


/*SENSOR #1*/
const int EchoSensor1 = 7;    /* was pingPin7*/
const int TriggerSensor1 = 12;
const int analogInPin = A0;  // Analog input pin that the potentiometer is attached to

/* -------------------Type Def ***********************************/
typedef struct Key_Switches
{
  uint32_t ubHistory[5];
  uint16_t ubPointer;
  uint32_t uwSwitches[6];
} Key_Switches_t;

/* --------------------Global Variables--------------------------*/

int uwFiveMsTaskCnt = 0;
int uwHundredMsTaskCnt;
int uwThousandMsTaskCnt;
int uwTenSecondsCnt;
char MODULE_LED;
int SensorDuration1, SensorInches1, SensorCm1;
float AdcVoltsRead;
byte TruncatedOutputValue = 0; // 8 bit value
int sensorValue = 0;        // value read from the pot
Key_Switches_t gKey_Switches;

/**
 * @fn     setup()
 *
 * @brief   Initializations here .
 *
 * @param   N/A
 *
 * @return  N/A
 *
 * @note    H/W and varaibles setup.
 *
 * @author  Tom Rode
 *
 * @date    12/28/2015
 *
 */
void setup()
{
    /** ###Functional overview: */

    /** initialize digital pin 13 as an output used to time tasks. */
    pinMode(BLINK_LED, OUTPUT);

    /** initialize the pushbutton pin as an input */
    pinMode(BUTTON1, INPUT);

    /** initialize serial communications at 9600 bps: */
    Serial.begin(9600);

    /** Watchdog timer set up with visual indication  */
    wdt_disable();
    MODULE_LED = 0;
    gBlink(25, TRUE);
    wdt_enable(WDTO_500MS);
    Serial.println(  "Initilization" );
}

/**
 * @fn     loop()
 *
 * @brief   Main Loop
 *
 * @param   N/A
 *
 * @return  N/A
 *
 * @note    Task called here.
 *
 * @author  Tom Rode
 *
 * @date    12/28/2015
 *
 */
void loop()
{
    /** ###Functional overview: */

    /* 5 MS TASK*/
    if( FIVE_MS == uwFiveMsTaskCnt )
    {
       /** Fire off the task now */
       gvFiveMsTask();

       /** reset the count to one */
       uwFiveMsTaskCnt = 1;
    }
    else
    {
       /** increment count */
       uwFiveMsTaskCnt++;
    }

    /* 100 MS TASK*/
    if( ONE_HUNDER_MS == uwHundredMsTaskCnt )
    {
        /** Fire off the task now */
        gvOneHundredMsTask();

        /** reset the count to one */
        uwHundredMsTaskCnt = 1;
    }
    else
    {
        /** increment count */
        uwHundredMsTaskCnt++;
    }

    /* 1000 MS TASK*/
    if( ONE_THOUSAND_MS ==  uwThousandMsTaskCnt )
    {

       /** Fire off the task now */
       gvOneThousandMsTask();

       /** reset the count to one */
       uwThousandMsTaskCnt = 1;
    }
    else
    {
        /** increment count */
        uwThousandMsTaskCnt++;
    }

    /** Idle State */
    NullTask();

    /** Wait for one millisecond*/
    delay(1);
}

/**
 * @fn      NullTask()
 *
 * @brief   Task used idle state.
 *
 * @param   N/A
 *
 * @return  N/A
 *
 * @note    N/A.
 *
 * @author  Tom Rode
 *
 * @date    12/28/2015
 *
 */
void NullTask()
{
    /** ###Functional overview: */

    /** Pet Watchdog*/
    wdt_reset();
}

/**
 * @fn      gvFiveMsTask()
 *
 * @brief   Task used to de-bounce buttons as needed .
 *
 * @param   N/A
 *
 * @return  N/A
 *
 * @note    Add buttons here.
 *
 * @author  Tom Rode
 *
 * @date    12/28/2015
 *
 */
void gvFiveMsTask()
{
    /** ###Functional overview: */

    /** Button logic here*/
}

/**
 * @fn      gvOneHundredMsTask()
 *
 * @brief   Task to read in A-D Moisture sensors, temp sensor.
 *
 * @param   N/A
 *
 * @return  N/A
 *
 * @note    A/D reads here.
 *
 * @author  Tom Rode
 *
 * @date    12/28/2015
 *
 */
void gvOneHundredMsTask()
{
    /** ###Functional overview: */

   /** Possible possible tasks here */
}

/**
 * @fn      gvOneThousandMsTask()
 *
 * @brief   Task to update display, ultrasonic read.
 *
 * @param   N/A
 *
 * @return  N/A
 *
 * @note    TBD.
 *
 * @author  Tom Rode
 *
 * @date    12/28/2015
 *
 */
void gvOneThousandMsTask()
{
    /** ###Functional overview: */

    /** - This is an indication of application */
    //MODULE_LED = ~(MODULE_LED & 0x01);
    gBlink(FALSE, TRUE);

    /** ADC READ */
    gADCread();

    /** TEN Second Task */
    if( uwTenSecondsCnt >= TEN_SECONDS )
    {
        /** Triger the Ultra Sonic */
        gvUltraSonic();

        /** Reset Count */
        uwTenSecondsCnt = 0;
    }

    /** Increment Counts*/
    uwTenSecondsCnt++;
}

/**
 * @fn      gvUltraSonic();
 *
 * @brief   ultrasonic read.
 *
 * @param   N/A
 *
 * @return  N/A
 *
 * @note    The SR04))) is triggered by a HIGH pulse of 2 or more microseconds.
 *          Give a short LOW pulse beforehand to ensure a clean HIGH pulse:
 *
 * @author  Tom Rode
 *
 * @date    12/28/2015
 *
 */
void  gvUltraSonic(void)
{
    /** ###Functional overview: */

    /* SENSOR 1 HANDLING*/
    pinMode(TriggerSensor1, OUTPUT);
    digitalWrite(TriggerSensor1, LOW);
    delayMicroseconds(2);
    digitalWrite(TriggerSensor1, HIGH);
    delayMicroseconds(5);
    digitalWrite(TriggerSensor1, LOW);

    /** The same pin is used to read the signal from the SR04))): a HIGH */
    /** pulse whose duration is the time (in microseconds) from the sending */
    /** of the ping to the reception of its echo off of an object. */
    pinMode(EchoSensor1, INPUT);
    SensorDuration1 = pulseIn(EchoSensor1, HIGH);

    /** convert the time into a distance */
    SensorInches1 = microsecondsToInches(SensorDuration1);
    SensorCm1 = microsecondsToCentimeters(SensorDuration1);

    /** print the results to the serial monitor: */
    Serial.print(" SensorInches1 = ");
    Serial.print(SensorInches1);
    Serial.print("  SensorCm1 = ");
    Serial.println(  SensorCm1);

    /*END SENSOR 1 HANDLING*/
}

/**
 * @fn      long microsecondsToInches(long microseconds)
 *
 * @brief   Convert time to inches measurement.
 *
 * @param   micro seconds
 *
 * @return  inches measurement
 *
 * @note    According to Parallax's datasheet for the PING))), there are.
 *          73.746 microseconds per inch (i.e. sound travels at 1130 feet per
 *          second).  This gives the distance travelled by the ping, outbound
 *          and return, so we divide by 2 to get the distance of the obstacle.
 *          See: http://www.parallax.com/dl/docs/prod/acc/28015-PING-v1.3.pdf
 *
 * @author  Tom Rode
 *
 * @date    12/28/2015
 *
 */
long microsecondsToInches(long microseconds)
{
    /** ###Functional overview: */

    return microseconds / 74 / 2;
}

/**
 * @fn      long microsecondsToCentimeters(long microseconds)
 *
 * @brief   Convert time to centimeters measurement.
 *
 * @param   micro seconds
 *
 * @return  centimeter measurement
 *
 * @note    The speed of sound is 340 m/s or 29 microseconds per centimeter.
 *          The ping travels out and back, so to find the distance of the
 *          object we take half of the distance travelled.
 *
 * @author  Tom Rode
 *
 * @date    12/28/2015
 *
 */
long microsecondsToCentimeters(long microseconds)
{
    /** ###Functional overview: */

    return microseconds / 29 / 2;
}

/**
 * @fn      gADCread()
 *
 * @brief   All ADC conversion read.
 *
 * @param   N/A
 *
 * @return  N/A
 *
 * @note    TBD.
 *
 * @author  Tom Rode
 *
 * @date    12/28/2015
 *
 */
void gADCread(void)
{
    /** ###Functional overview: */

    /** read the analog in value */
    sensorValue = analogRead(analogInPin);

    /** truncated lower 8 bits from 10 bit conversion */
    TruncatedOutputValue = ( sensorValue >> 2 );

    /** make the read readable in volts */
    AdcVoltsRead = TruncatedOutputValue * (5.0 / 255.0);

    /** print the results to the serial monitor: */
    Serial.print(" ADC 8bit = ");
    Serial.print(  TruncatedOutputValue);
    Serial.print(" sensor in volts = ");
    Serial.println(  AdcVoltsRead);
}

/**
 * @fn      void gBlink(int UwTimes)
 *
 * @brief   Blink an output pin for indication.
 *
 * @param   UwTimes =  Amount of times to blink,
 * @param   ubRepeat = if to blink
 *
 * @return  N/A
 *
 * @note    TBD.
 *
 * @author  Tom Rode
 *
 * @date    12/28/2015
 *
 */
void gBlink(int UwTimes, byte ubRepeat)
{
    int uwindex;

    /** ###Functional overview: */

    /** Toggle LED */
    if ( TRUE == ubRepeat )
    {
        /** Back and fourth */
        if( TRUE == MODULE_LED )
        {
           /** Toggle LED */
           MODULE_LED = FALSE;
           digitalWrite(BLINK_LED, TRUE);   // turn the LED on (HIGH is the voltage level)
        }
        else
        {
           MODULE_LED = TRUE;
           digitalWrite(BLINK_LED, LOW);   // turn the LED on (HIGH is the voltage level)
        }
    }
    else
    {
        /** Amount to blink a pin */
        for ( uwindex = 0; uwindex < UwTimes; uwindex++ )
        {
      /** Blink the indicator LED specified amount of times*/
            if ( TRUE == MODULE_LED )
            {
                MODULE_LED = FALSE;
                digitalWrite(BLINK_LED, HIGH);   // turn the LED on (HIGH is the voltage level)
            }
            else
            {
                MODULE_LED = TRUE;
                digitalWrite(BLINK_LED, LOW);   // turn the LED on (HIGH is the voltage level)
            }

            /** - Delay will be visible */
            delay(100);
        }
    }
}

/**
 * @fn       GetDebRowSw(void)
 *
 * @brief    This function returns Debounced Switch state
 *
 * @param    None
 *
 * @return   N/A
 *
 * @note     Debounced Switch state after five consecutive 1-ms reads
 *
 * @author   Tom Rode
 *
 */
void vGetDebRowSw(void)
{

    /** ###Functional overview: */

    /** - Read in state of push button value */
    gKey_Switches.ubHistory[gKey_Switches.ubPointer] = digitalRead(BUTTON1);;

    /** - Capture five same state readings */
    if ((gKey_Switches.ubHistory[0] == gKey_Switches.ubHistory[1]) &&
        (gKey_Switches.ubHistory[1] == gKey_Switches.ubHistory[2]) &&
        (gKey_Switches.ubHistory[2] == gKey_Switches.ubHistory[3]) &&
        (gKey_Switches.ubHistory[3] == gKey_Switches.ubHistory[4]) )

    {
        /** - Five of the same state readings place into respective byte in switches array, index is correct now*/
        gKey_Switches.uwSwitches[0] = gKey_Switches.ubHistory[0];

    }
    /** - increment the count */
    gKey_Switches.ubPointer++;

    /** - Five of the same read state for state latching purposes*/
    if (gKey_Switches.ubPointer > 4)
    {
        /** - reset the count */
        gKey_Switches.ubPointer = 0;
    }

}

