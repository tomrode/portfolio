/**
*    @file                    Garden_Water_Project Sketch
*    @brief                   This file contains all functions related to This Project
*    @copyright               Thomas Rode., Delphos, OH  45833
*    @date                    12/28/2015
*
*    @remark Author:          Tom Rode
*    @remark Project Tree:    Garden_Water_Proj
*
*    @note        Ver 1.1     This is the code to run
*    @note        Ver 1.2     Changed Moisture thresholds upped wilting point 14 June 2016
*    @note        Ver 1.3     Changed 100 ms task to 500 ms to help with flickering 16 June 2016
*                             Display the Thresholds on start up
*    @note        Ver 1.4     Added in an ultra sonic sensor override to disable this function if need be for testing 7 July 2016
*                             Added in ADC soil sensor averaging and Display it.
*                             Added 5 count hysteresis
*
*/

#include <avr/wdt.h>
#include <Arduino.h>
#include <Wire.h>  // Comes with Arduino IDE
// Get the LCD I2C Library here:
// https://bitbucket.org/fmalpartida/new-liquidcrystal/downloads
// Move any other LCD libraries to another folder or delete them
// See Library "Docs" folder for possible commands etc.
#include <LiquidCrystal_I2C.h>

/* --------------------CONSTANT'S-----------------------------------*/
const byte  FALSE                                   = 0;
const byte  TRUE                                    = 1;
const byte  FIVE_MS                                 = 5;
const int   FIVE_HUNDERD_MS                         = 500;
const int   ONE_THOUSAND_MS                         = 1000;
const byte  TWO_SECONDS_SCROLL                      = 20;
const byte  FIVE_SECONDS                            = 5;
const byte  TEN_SECONDS                             = 10;
const byte  TIME_OUT_TWENTY_SECONDS_IN_500_MS_TASK  = 40;
const byte  TWO_SECONDS_SCROLL_IN_1000_MS_TASK      = 2;
const byte  PUMP_OVRD_FOUR_SECONDS_IN_500_MS_TASK   = 8;
const byte  PUMP_OVRD_THREE_SECONDS_IN_500_MS_TASK  = 6;
const byte  PUMP_OVRD_TWO_SECONDS_IN_500_MS_TASK    = 4;
const byte  PUMP_OVRD_ONE_SECONDS_IN_500_MS_TASK    = 2;
const byte  SOIL_SEN_ADC_AVERAGE_DIVISOR            = 6;

/* ---------------------PIN ASSIGNMENTS-----------------------------*/
const byte  BLINK_LED              = 13;
const byte  BUTTON1                = 2;
const byte  SEN_PWR                = 8;
const byte  PUMP                   = 4;
/*SENSOR #1*/
const int EchoSensor1             = 7;    /* was pingPin7*/
const int TriggerSensor1          = 12;
const int analogInPin             = A0;  // Analog input pin that the potentiometer is attached to

/*  --------------------LIMITS TABLES-------------------------------*/
/*  --------------------Moisture Sensor-----------------------------*/
const int LOST_SOIL_ADC_READ            = 0;
const int SOIL_WILTING_POINT_ADC_READ   = 159;   /* Reading based on test data ADC counts */
const int SOIL_WILTING_POINT_HYST_ADC_READ = 164;
const int SOIL_FIELD_CAPACITY_ADC_READ  = 160;
const int SOIL_SATURATION_ADC_READ      = 185;
/*  --------------------UltraSonic----------------------------------*/
const byte TANK_PERCENT_00_FULL          = 30;   /* Inches */
const byte TANK_PERCENT_10_FULL          = 28;//27;   /* Inches */
const byte TANK_PERCENT_20_FULL          = 25;//24;   /* Inches */
const byte TANK_PERCENT_30_FULL          = 22;//21;   /* Inches */
const byte TANK_PERCENT_40_FULL          = 19;//18;   /* Inches */
const byte TANK_PERCENT_50_FULL          = 16;//15;   /* Inches */
const byte TANK_PERCENT_60_FULL          = 13;//12;   /* Inches */
const byte TANK_PERCENT_70_FULL          = 10;//9;    /* Inches */
const byte TANK_PERCENT_80_FULL          = 7;//6;    /* Inches */
const byte TANK_PERCENT_90_FULL          = 4;//3;    /* Inches */
const byte TANK_PERCENT_100_FULL         = 1;    /* Inches */


/*-----( Declare objects )-----*/
// set the LCD address to 0x27 for a 16 chars 2 line display
// A FEW use address 0x3F
// Set the pins on the I2C chip used for LCD connections:
//                    addr, en,rw,rs,d4,d5,d6,d7,bl,blpol
LiquidCrystal_I2C lcd(0x27, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);  // Set the LCD I2C address

/* -------------------Type Def ***********************************/
typedef struct Key_Switches
{
  uint32_t ubHistory[5];
  uint16_t ubPointer;
  uint32_t uwSwitches[6];
} Key_Switches_t;

typedef enum
{
   NORMAL_SCROLL_SELECT           = 0,
   ADC_SOIL_SENSOR_BTN_SELECT     = 1,
   ULTRA_SONIC_SENSOR_BTN_SELECT  = 2,
   PUMP_OVRD_EN_BTN_SELECT        = 3,
   ADC_AVG_SOIL_SENSOR_BTN_SELECT = 4
}ButtonState_t;

typedef enum
{
  SOIL_SENSOR_READ_LOST     = 0,  /** < Line 0 */
  SATURATION                = 1,  /** < Line 1 */
  FIELD_CAPACITY            = 2,  /** < Line 1 */
  WILTING_POINT             = 3,  /** < Line 1 */
  ADC_SOIL_SENSOR           = 4,  /** < Line 0 */
  PERCENT_100_FULL          = 5,  /** < Line 1 */
  PERCENT_90_FULL           = 6,  /** < Line 1 */
  PERCENT_80_FULL           = 7,  /** < Line 1 */
  PERCENT_70_FULL           = 8,  /** < Line 1 */
  PERCENT_60_FULL           = 9,  /** < Line 1 */
  PERCENT_50_FULL           = 10,  /** < Line 1 */
  PERCENT_40_FULL           = 11,  /** < Line 1 */
  PERCENT_30_FULL           = 12,  /** < Line 1 */
  PERCENT_20_FULL           = 13,  /** < Line 1 */
  PERCENT_10_FULL           = 14,  /** < Line 1 */
  PERCENT_00_FULL           = 15,  /** < Line 1 */
  ULTRA_SONIC_SENSOR        = 16,  /** < Line 0 */
  PUMP_ENABLED              = 17,  /** < Line 1 */
  PUMP_DISABLED             = 18,  /** < Line 1 */
  PUMPOVRD_ENABLED_IN_3_SEC = 19,  /** < Line 1 */
  PUMPOVRD_ENABLED_IN_2_SEC = 20,  /** < Line 1 */
  PUMPOVRD_ENABLED_IN_1_SEC = 21,   /** < Line 1 */
  PUMPOVRD_ENABLED_TO_NORMAL_MODE = 22,
  PUMPOVRD_PERCENT_00_FULL = 23,
  INTRO                    = 24,
  SATURATION_THR_INTRO     = 25,
  FIELD_THR_INTRO          = 26,
  WILTING_THR_INTRO        = 27,
  AVG_SOIL_READING         = 28
}DisplayMessages_t;

typedef enum
{
   SOIL_MESSAGE_DISPLAY          = 0,
   TANK_LEVEL_MESSAGE_DISPLAY    = 1,
   PUMP_OUTPUT_MESSAGE_DISPLAY   = 2
}OutputMsgToI2CDisplay_t;
/* --------------------Global Variables--------------------------*/

int uwFiveMsTaskCnt = 0;
int uwFiveHundredMsTaskCnt;
int uwThousandMsTaskCnt;
byte ubTenSecondsCnt;
byte ubFiveSecondsCnt;
char MODULE_LED;
int SensorDuration1, SensorInches1, SensorCm1;
float AdcVoltsRead;
byte TruncatedSoilSenOutputValue = 0; // 8 bit value
int sensorValue = 0;        // value read from the pot
byte ubDisplayState;        // Which display state to be in
byte ubNormalOpMode;        // Usec to read out raw data or normal running mode
static byte ubNormalOpModeOvrdPumpEn; // this will signify pump override enable
uint8_t ubNormalModeLockOut;
byte ubgvOvrdPumpEn;
static uint8_t ubTempUltraSonicPumpOverride;
static uint8_t ubUltranSonicDisable = TRUE;  // This will enable/disable the ultrasonic sensor all together FALSE = Enables sensor
Key_Switches_t gKey_Switches;
static uint8_t ubAVGSoilADC;  // Used this to display on I2C

/**
   @fn     setup()

   @brief   Initializations here .

   @param   N/A

   @return  N/A

   @note    H/W and varaibles setup.

   @author  Tom Rode

   @date    12/28/2015

*/
void setup()
{

  /** initialize serial communications at 9600 bps: */
  Serial.begin(9600);

  /** ###Functional overview: */
  Serial.println(  "Initilization" );

  /** initialize digital pin 4 as an output used to run output to pump. */
  pinMode(PUMP, OUTPUT);

  /** initialize digital pin 13 as an output used to time tasks. */
  pinMode(BLINK_LED, OUTPUT);

  /** Transistor turn on for Moisture sensor*/
  pinMode(SEN_PWR, OUTPUT);

  /** initialize the pushbutton pin as an input */
  pinMode(BUTTON1, INPUT);

  /** - Delay for LCD issue */
  delay(10);

  /** LCD Init*/
  lcd.begin(16,2);   // initialize the lcd for 16 chars 2 lines, turn on backlight

  // ------- Quick 3 blinks of backlight  -------------
  for(int i = 0; i< 3; i++)
  {
    lcd.backlight();
    delay(250);
    lcd.noBacklight();
    delay(250);
  }
  lcd.backlight(); // finish with backlight on
  /** Watchdog timer set up with visual indication  */
  wdt_disable();
  MODULE_LED = 0;

  /** Display intro message The Version it's at */
  gI2CLCD_VarFormat(FALSE, 0,  INTRO);
  delay(1000);

  /** Display intro message for Saturation threshold */
  gI2CLCD_VarFormat(TRUE, SOIL_SATURATION_ADC_READ, SATURATION_THR_INTRO);
  delay(700);

  /** Display intro message for Field Capacity threshold */
  gI2CLCD_VarFormat(TRUE, SOIL_FIELD_CAPACITY_ADC_READ, FIELD_THR_INTRO);
  delay(700);

  /** Display intro message for Wilting point threshold */
  gI2CLCD_VarFormat(TRUE, SOIL_WILTING_POINT_ADC_READ, WILTING_THR_INTRO);
  delay(700);

  gBlink(25, TRUE);
  wdt_enable(WDTO_500MS);
}

/**
   @fn     loop()

   @brief   Main Loop

   @param   N/A

   @return  N/A

   @note    Task called here.

   @author  Tom Rode

   @date    12/28/2015

*/
void loop()
{
  /** ###Functional overview: */

  /* 5 MS TASK*/
  if ( FIVE_MS == uwFiveMsTaskCnt )
  {
    /** Fire off the task now */
    gvFiveMsTask();

    /** reset the count to one */
    uwFiveMsTaskCnt = 1;
  }
  else
  {
    /** increment count */
    uwFiveMsTaskCnt++;
  }

  /* 500 MS TASK*/
  if ( FIVE_HUNDERD_MS == uwFiveHundredMsTaskCnt )
  {
    /** Fire off the task now */
    gvFiveHundredMsTask();

    /** reset the count to one */
    uwFiveHundredMsTaskCnt = 1;
  }
  else
  {
    /** increment count */
    uwFiveHundredMsTaskCnt++;
  }

  /* 1000 MS TASK*/
  if ( ONE_THOUSAND_MS == uwThousandMsTaskCnt )
  {

    /** Fire off the task now */
    gvOneThousandMsTask();

    /** reset the count to one */
    uwThousandMsTaskCnt = 1;
  }
  else
  {
    /** increment count */
    uwThousandMsTaskCnt++;
  }

  /** Idle State */
  NullTask();

  /** Wait for one millisecond*/
  delay(1);
}

/**
   @fn      NullTask()

   @brief   Task used idle state.

   @param   N/A

   @return  N/A

   @note    N/A.

   @author  Tom Rode

   @date    12/28/2015

*/
void NullTask()
{
  /** ###Functional overview: */

  /** Pet Watchdog*/
  wdt_reset();
}

/**
   @fn      gvFiveMsTask()

   @brief   Task used to de-bounce buttons as needed .

   @param   N/A

   @return  N/A

   @note    Add buttons here.

   @author  Tom Rode

   @date    12/28/2015

*/

void gvFiveMsTask()
{
  static byte ubBtnState;
  static byte ubOneShot;
  static byte ubBtnNormScroolState;
  static byte ubBtnADCSoilState;
  static byte ubBtnAVG_ADCSoilState;
  static byte ubBtnUltraSonicState;
  static byte ubBtnPumpOvrdState;
  static byte UbBtnUnPrsd;
  /** ###Functional overview: */

  /** Get the switch de-bounced */
  vGetDebRowSw();

  if ( (TRUE == gKey_Switches.uwSwitches[0] ) && ( FALSE == ubOneShot) )
  {
    if ( FALSE == ubOneShot )
    {
      Serial.println(  "Pressed " );
      ubOneShot = TRUE;
      digitalWrite(BLINK_LED, TRUE);   // turn the LED on (HIGH is the voltage level)
    }
    else if ( TRUE == ubOneShot )
    {
      ubOneShot = FALSE;
    }
  }
  else if ( FALSE == gKey_Switches.uwSwitches[0] )
  {
    ubOneShot = FALSE;
    digitalWrite(BLINK_LED, FALSE);   // turn the LED off (Low is the voltage level)
  }
  else {}
  /** Button logic here*/

    /** - Button state machine */
  switch( ubBtnState )
  {
       case NORMAL_SCROLL_SELECT:
              /** - Display will scroll*/
           if((TRUE == ubOneShot ) && (ubBtnNormScroolState == FALSE) )
           {
               /** - Display will scroll*/
               ubDisplayState = NORMAL_SCROLL_SELECT;

               /** - Make it possible normal state */
               ubBtnState = NORMAL_SCROLL_SELECT;

               /** - Show it was in this state before*/
               ubBtnNormScroolState = TRUE;

           }
           /** - Button physically not pressed now */
           else if((FALSE == gKey_Switches.uwSwitches[0]) && ( ubBtnNormScroolState == TRUE))
           {
               /** - Make it goto next state */
               ubBtnState = ADC_SOIL_SENSOR_BTN_SELECT;

               /** - Reset for next time*/
               ubBtnNormScroolState = FALSE;

           }

           break;

      case ADC_SOIL_SENSOR_BTN_SELECT:

           if((TRUE == ubOneShot) && ( ubBtnADCSoilState == FALSE))
           {
               /** - Display will Read out the ADC from the soil sensor*/
               ubDisplayState = ADC_SOIL_SENSOR_BTN_SELECT;

               /** - Make it possible to read ADC from the soil */
               ubBtnState = ADC_SOIL_SENSOR_BTN_SELECT;

               /** - Show its been here before*/
               ubBtnADCSoilState = TRUE;
           }

            /** - Button physically not pressed now */
           else if( (FALSE == gKey_Switches.uwSwitches[0]) && (ubBtnADCSoilState == TRUE) )
           {
               /** - Make it goto next state */
               ubBtnState = ADC_AVG_SOIL_SENSOR_BTN_SELECT;

               /** - Reset for next time*/
               ubBtnADCSoilState = FALSE;
           }

           break;

      case ADC_AVG_SOIL_SENSOR_BTN_SELECT:

           if((TRUE == ubOneShot) && ( ubBtnAVG_ADCSoilState == FALSE))
           {
               /** - Display will Read out the Averaged ADC from the soil sensor*/
               ubDisplayState = ADC_AVG_SOIL_SENSOR_BTN_SELECT;

               /** - Make it possible to read Averaged ADC from the soil */
               ubBtnState = ADC_AVG_SOIL_SENSOR_BTN_SELECT;

               /** - Show its been here before*/
               ubBtnAVG_ADCSoilState = TRUE;
           }

            /** - Button physically not pressed now */
           else if( (FALSE == gKey_Switches.uwSwitches[0]) && (ubBtnAVG_ADCSoilState == TRUE) )
           {
               /** - Make it goto next state */
               ubBtnState = ULTRA_SONIC_SENSOR_BTN_SELECT;

               /** - Reset for next time*/
               ubBtnAVG_ADCSoilState = FALSE;
           }

           break;

      case ULTRA_SONIC_SENSOR_BTN_SELECT:

           if( (TRUE == ubOneShot ) && (ubBtnUltraSonicState == FALSE))
           {
               /** - Make it possible to read Ultra Sonic sensor */
               ubDisplayState = ULTRA_SONIC_SENSOR_BTN_SELECT;

               /** - Make it possible to read Ultra Sonic sensor */
               ubBtnState = ULTRA_SONIC_SENSOR_BTN_SELECT;

               /** - Show its been here before*/
               ubBtnUltraSonicState = TRUE;
           }
          /** - Button physically not pressed now */
           else if( (FALSE == gKey_Switches.uwSwitches[0]) && (ubBtnUltraSonicState == TRUE) )
           {
              /** - Make it goto next state */
               ubBtnState = PUMP_OVRD_EN_BTN_SELECT;//NORMAL_SCROLL_SELECT;  /////// Change to next state once ready /////////////////////////////////////////////////////////////////////////////////

               /** - Reset for next time */
               ubBtnUltraSonicState = FALSE;
           }
          break;

      /** UNDER CONSTRUCTION */
       case PUMP_OVRD_EN_BTN_SELECT:

           if( (TRUE == ubOneShot ) && (ubBtnPumpOvrdState == FALSE))
           {
               /** - Make it possible to rSet dispaly state for pump override */
               ubDisplayState = PUMP_OVRD_EN_BTN_SELECT;

               /** - state machine to rSet dispaly state for pump override */
               ubBtnState = PUMP_OVRD_EN_BTN_SELECT;

               /** - Show its been here before*/
               ubBtnPumpOvrdState = TRUE;
           }
          /** - Button physically not pressed now */
           else if( (FALSE == gKey_Switches.uwSwitches[0]) && ( ubBtnPumpOvrdState == TRUE) )
           {
              /** - Make it goto next state */
               ubBtnState = NORMAL_SCROLL_SELECT;

               /** - Reset for next time */
               ubBtnPumpOvrdState = FALSE;
           }

           break;
     default:
           break;

  }

}

/**
   @fn      gvFiveHundredMsTask()

   @brief   Task to read in A-D Moisture sensors, ultra sonic, temp sensor with button presses to see
            raw reads .

   @param   N/A

   @return  N/A

   @note    A/D reads here.

   @author  Tom Rode

   @date    12/28/2015



*/
void gvFiveHundredMsTask()
{
/** ###Functional overview: */
uint8_t ubTemp;
static uint8_t ubDisplayScrollTime;
static uint8_t ubDisplaySpecificReadTimeOut;
static uint8_t ubDisplaySpecificReadTimeOutPumpEn;
static uint8_t ubDisplaySpecificReadTimeOutPumpEnMsgToggle;


    if ( ubDisplayState == ADC_SOIL_SENSOR_BTN_SELECT )
    {

        /** - Suspend the normal operation */
        /** turn off pump here to*/
        ubNormalOpMode = FALSE;

        /** - Normal lock out disabled */
        ubNormalModeLockOut = FALSE;

        /** Turn on transistor*/
        digitalWrite(SEN_PWR, TRUE);   // turn the LED on (HIGH is the voltage level)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    AgvUl1tA~44444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444c5x                                                                            traSonic();

        /** Simple delay for test remove once done*/
        /** Wait for eleven millisecond*/
        delay(4);

        /** ADC READ */
        ubTemp = gADCread(ADC_SOIL_SENSOR);

        /** Turn off transistor*/
        digitalWrite(SEN_PWR, FALSE);   // turn the LED on (HIGH is the voltage level)

        /** Display Soil raw count */
        gI2CLCD_VarFormat(TRUE,ubTemp, ADC_SOIL_SENSOR);

        /** - Ouput debug messages */
        Serial.print("  Raw ADC Read1 = ");
        Serial.println(  ubTemp );

        /** - Tick up the timeout*/
        ubDisplaySpecificReadTimeOut++;

        /** - Timeout occurs */
        if ( ubDisplaySpecificReadTimeOut >= TIME_OUT_TWENTY_SECONDS_IN_500_MS_TASK )
        {
            /** - Reset the state to normal scroll*/
            ubDisplayState = NORMAL_SCROLL_SELECT;

            /** - Normal mode enabled */
            ubNormalModeLockOut = TRUE;
        }
    }
    else if ( ubDisplayState == ADC_AVG_SOIL_SENSOR_BTN_SELECT )
    {

        /** - Suspend the normal operation */
        /** turn off pump here to*/
        ubNormalOpMode = FALSE;

        /** - Normal lock out disabled */
        ubNormalModeLockOut = FALSE;

        /** - Average ADC READ from global variable  */
        ubTemp = ubAVGSoilADC;

        /** Display Soil Averaged raw count */
        gI2CLCD_VarFormat(TRUE,ubTemp, AVG_SOIL_READING);

        /** - Output debug messages */
        Serial.print("  Averaged ADC Read1 = ");
        Serial.println(  ubTemp );

        /** - Tick up the timeout*/
        ubDisplaySpecificReadTimeOut++;

        /** - Timeout occurs */
        if ( ubDisplaySpecificReadTimeOut >= TIME_OUT_TWENTY_SECONDS_IN_500_MS_TASK )
        {
            /** - Reset the state to normal scroll*/
            ubDisplayState = NORMAL_SCROLL_SELECT;

            /** - Normal mode enabled */
            ubNormalModeLockOut = TRUE;
        }
   }
  else if ( ubDisplayState == ULTRA_SONIC_SENSOR_BTN_SELECT )
  {
        /** turn off pump here to*/
        ubNormalOpMode = FALSE;

       /** - Normal lock out disabled */
        ubNormalModeLockOut = FALSE;

       /** Trigger the Ultra Sonic Re-instate once done*/
        ubTemp = gvUltraSonic();

         /** Display Soil raw count */
        gI2CLCD_VarFormat(TRUE,ubTemp, ULTRA_SONIC_SENSOR);

        /** - this resets once out of this stae for a three second time out*/
        ubDisplayScrollTime = 0;

         /** - Tick up the timeout*/
        ubDisplaySpecificReadTimeOut++;

        /** - If we are here then we want to go to pump en zero out this count*/
        ubDisplaySpecificReadTimeOutPumpEn = 0;

         /** - Timeout occurs */
        if ( ubDisplaySpecificReadTimeOut >= TIME_OUT_TWENTY_SECONDS_IN_500_MS_TASK )
        {
            /** - Reset the state to normal scroll*/
            ubDisplayState = NORMAL_SCROLL_SELECT;

            /** - Normal mode enabled */
            ubNormalModeLockOut = TRUE;
        }
  }
 else if ( ubDisplayState == PUMP_OVRD_EN_BTN_SELECT )
 {
       /** turn off pump here to*/
        ubNormalOpMode = FALSE;

       /** - Normal lock out disabled */
       ubNormalModeLockOut = FALSE;

       /** - Show in pump over ride enable */
       //ubNormalOpModeOvrdPumpEn = TRUE;

        /** - this resets once out of this state for a three second time out*/
       // ubDisplayScrollTime = 0;

        /** - Timeout occurs reset to 0 and display you have 3 seconds till pump on */
        if (ubDisplaySpecificReadTimeOutPumpEn == 0 )
        {
            /** -  Turn off pump */
            ubgvOvrdPumpEn = FALSE;

           /** Display PUMP OVERRIDE MESSAGE */
            gI2CLCD_VarFormat(TRUE,ubTemp, PUMPOVRD_ENABLED_IN_3_SEC);
        }
        else if ( (ubDisplaySpecificReadTimeOutPumpEn >= PUMP_OVRD_ONE_SECONDS_IN_500_MS_TASK) && (ubDisplaySpecificReadTimeOutPumpEn <= PUMP_OVRD_TWO_SECONDS_IN_500_MS_TASK) )
        {
           /** Display PUMP OVERRIDE MESSAGE */
            gI2CLCD_VarFormat(TRUE,ubTemp, PUMPOVRD_ENABLED_IN_2_SEC);
        }
        else if ( (ubDisplaySpecificReadTimeOutPumpEn >= PUMP_OVRD_TWO_SECONDS_IN_500_MS_TASK) && (ubDisplaySpecificReadTimeOutPumpEn <= PUMP_OVRD_THREE_SECONDS_IN_500_MS_TASK) )
        {
           /** Display PUMP OVERRIDE MESSAGE */
            gI2CLCD_VarFormat(TRUE,ubTemp, PUMPOVRD_ENABLED_IN_1_SEC);
        }
        else if ( ubDisplaySpecificReadTimeOutPumpEn >= PUMP_OVRD_THREE_SECONDS_IN_500_MS_TASK)
        {
            /** -  Turn on pump */
            ubgvOvrdPumpEn = TRUE;

            /** - Show in pump over ride enable */
            ubNormalOpModeOvrdPumpEn = TRUE;

            /** Toggle the Messages for pump enabled and do you want to return to normal mode  */
            if( ubDisplaySpecificReadTimeOutPumpEnMsgToggle <= PUMP_OVRD_TWO_SECONDS_IN_500_MS_TASK )
            {
                /** Display pump on since water level okay */
                if (  ubTempUltraSonicPumpOverride == FALSE )
                {
                    /** Display PUMP ON */
                     gI2CLCD_VarFormat(TRUE,ubTemp, PUMP_ENABLED);
                     ubDisplaySpecificReadTimeOutPumpEnMsgToggle++;
                }
                else
                {
                     /** Pump overrride empty tank */
                    gI2CLCD_VarFormat(FALSE, 0,  PUMPOVRD_PERCENT_00_FULL );
                    ubDisplaySpecificReadTimeOutPumpEnMsgToggle++;
                }
            }
            else if( (ubDisplaySpecificReadTimeOutPumpEnMsgToggle >= PUMP_OVRD_TWO_SECONDS_IN_500_MS_TASK) && (ubDisplaySpecificReadTimeOutPumpEnMsgToggle <= PUMP_OVRD_FOUR_SECONDS_IN_500_MS_TASK) )
            {
                /** Display PUMP OVERRIDE MESSAGE */
                gI2CLCD_VarFormat(TRUE,ubTemp, PUMPOVRD_ENABLED_TO_NORMAL_MODE);
                ubDisplaySpecificReadTimeOutPumpEnMsgToggle++;
            }
            else
            {
                 ubDisplaySpecificReadTimeOutPumpEnMsgToggle = 0;;
            }
        }
       /** - Tick up the timeout if less than the three seconds time*/
       if(  ubDisplaySpecificReadTimeOutPumpEn <= PUMP_OVRD_THREE_SECONDS_IN_500_MS_TASK )
       {
           /** - Tick up the timeout*/
           ubDisplaySpecificReadTimeOutPumpEn++;
       }
  }

  else if ( ubDisplayState == NORMAL_SCROLL_SELECT )
  {
        /** - Normal mode enabled */
        ubNormalModeLockOut = TRUE;

        /** - Show in pump over ride disable */
        ubNormalOpModeOvrdPumpEn = FALSE;

        /** - Reset 20 second timeout*/
        ubDisplaySpecificReadTimeOut = 0;

       if( ubDisplayScrollTime <= TWO_SECONDS_SCROLL)
       {
           /** LCD outptu control*/
          lcd.clear();
          lcd.setCursor(0,0); //Start at character 0 on line 0
          lcd.print("NORMAL SCROLLING ");
          lcd.setCursor(0,1); //Start at character 0 on line 0
          lcd.print(" MODE SELECTED    ");

          ubDisplayScrollTime++;
       }
       else
       {
            /** - Return to normal operation*/
            ubNormalOpMode = TRUE;
       }
   }
}

/**
   @fn      gvOneThousandMsTask()

   @brief   Task to update display, ultrasonic read.

   @param   N/A

   @return  N/A

   @note    TBD.

   @author  Tom Rode

   @date    12/28/2015

*/
void gvOneThousandMsTask()
{

static uint8_t ubTempSoilADC;
static uint8_t ubTempSoilDisplay;
static uint8_t ubTempUltraSonic;
static uint8_t ubTempUltraSonicDisplay;
static uint8_t ubPumpEn;
static uint8_t ubPumpEnPrevious;
static uint8_t ubTempPumpDisplay;
static uint8_t ubDisplayStateMch;
static uint8_t ubDisplayStateMchCnt;

    /** ###Functional overview: */

    /** - This is an indication of application */
    gBlink(TRUE, TRUE);

    /** FIVE Second Task and in normal operating mode or pump override state */
    if ( (ubFiveSecondsCnt >= FIVE_SECONDS) && ((ubNormalOpMode == TRUE) || (ubNormalOpModeOvrdPumpEn == TRUE)) )
    {
         /** Trigger the Ultra Sonic Re-instate once done*/
         ubTempUltraSonic = gvUltraSonic();


         if( ubTempUltraSonic <= TANK_PERCENT_100_FULL)
         {
             /** - display tank full*/
             ubTempUltraSonicDisplay = PERCENT_100_FULL;

             /** - Enable pump */
             ubTempUltraSonicPumpOverride = FALSE;
         }
         else if( (ubTempUltraSonic > TANK_PERCENT_100_FULL) && (ubTempUltraSonic <= TANK_PERCENT_90_FULL))
         {
             /** - display tank 90%*/
             ubTempUltraSonicDisplay = PERCENT_90_FULL;

             /** - Enable pump */
             ubTempUltraSonicPumpOverride = FALSE;
         }
         else if( (ubTempUltraSonic > TANK_PERCENT_90_FULL) && (ubTempUltraSonic <= TANK_PERCENT_80_FULL))
         {
             /** - display tank 80%*/
             ubTempUltraSonicDisplay = PERCENT_80_FULL;

             /** - Enable pump */
             ubTempUltraSonicPumpOverride = FALSE;
         }
         else if( (ubTempUltraSonic > TANK_PERCENT_80_FULL) && (ubTempUltraSonic <= TANK_PERCENT_70_FULL))
         {
             /** - display tank 70%*/
             ubTempUltraSonicDisplay = PERCENT_70_FULL;

             /** - Enable pump */
             //ubTempUltraSonicPumpOverride = FALSE;
         }
         else if( (ubTempUltraSonic > TANK_PERCENT_70_FULL) && (ubTempUltraSonic <= TANK_PERCENT_60_FULL))
         {
             /** - display tank 60%*/
             ubTempUltraSonicDisplay = PERCENT_60_FULL;

             /** - Enable pump */
             ubTempUltraSonicPumpOverride = FALSE;
         }
         else if( (ubTempUltraSonic > TANK_PERCENT_60_FULL) && (ubTempUltraSonic <= TANK_PERCENT_50_FULL))
         {
             /** - display tank 50%*/
             ubTempUltraSonicDisplay = PERCENT_50_FULL;

             /** - Enable pump */
             ubTempUltraSonicPumpOverride = FALSE;
         }
         else if( (ubTempUltraSonic > TANK_PERCENT_50_FULL) && (ubTempUltraSonic <= TANK_PERCENT_40_FULL))
         {
             /** - display tank 40%*/
             ubTempUltraSonicDisplay = PERCENT_40_FULL;

             /** - Enable pump */
             ubTempUltraSonicPumpOverride = FALSE;
         }
         else if( (ubTempUltraSonic > TANK_PERCENT_40_FULL) && (ubTempUltraSonic <= TANK_PERCENT_30_FULL))
         {
             /** - display tank 30%*/
             ubTempUltraSonicDisplay = PERCENT_30_FULL;

             /** - Enable pump */
             ubTempUltraSonicPumpOverride = FALSE;
         }
         else if( (ubTempUltraSonic > TANK_PERCENT_30_FULL) && (ubTempUltraSonic <= TANK_PERCENT_20_FULL))
         {
             /** - display tank 20%*/
             ubTempUltraSonicDisplay = PERCENT_20_FULL;

             /** - Enable pump */
             ubTempUltraSonicPumpOverride = FALSE;
         }
         else if( (ubTempUltraSonic > TANK_PERCENT_20_FULL) && (ubTempUltraSonic <= TANK_PERCENT_10_FULL))
         {
             /** - display tank 10%*/
             ubTempUltraSonicDisplay = PERCENT_10_FULL;

             /** - Enable pump */
             ubTempUltraSonicPumpOverride = FALSE;

         }
         else if (ubTempUltraSonic > TANK_PERCENT_10_FULL)
         {
             /** - display tank 00%*/
             ubTempUltraSonicDisplay = PERCENT_00_FULL;

             /** - Disable pump*/
             ubTempUltraSonicPumpOverride = TRUE;

         }
         /** Display something */
         //gI2CLCD_VarFormat(FALSE, ubTempUltraSonic, ubTempUltraSonicDisplay );

         /** Reset Count */
         ubFiveSecondsCnt = 0;
    }

    /** TEN Second Task */
    if ( (ubTenSecondsCnt >= TEN_SECONDS) && (ubNormalOpMode == TRUE) )
    {
        /** Turn on transistor*/
        digitalWrite(SEN_PWR, TRUE);   // turn the LED on (HIGH is the voltage level)

        /** Simple delay for test remove once done*/
        /** Wait for eleven millisecond*/
        delay(4);

        /** RAW ADC READ */
        ubTempSoilADC = gADCread(ADC_SOIL_SENSOR);

        /** - Averaged ADC soil read */
        ubTempSoilADC = gADCAverageRead(ubTempSoilADC);

        /** - Set the global variable used by display */
        ubAVGSoilADC = ubTempSoilADC;

        /** - Setting dubug messages */
        Serial.print(" Averaged ADC 8bit = ");
        Serial.print(  ubAVGSoilADC );

        /** Turn off transistor*/
        digitalWrite(SEN_PWR, FALSE);   // turn the LED on (HIGH is the voltage level)

        /** - Settings for Soil sensor*/
        if( ubTempSoilADC == LOST_SOIL_ADC_READ )
        {
             /** - report the sensor lost a read which is forced to zero*/
             ubTempSoilDisplay = SOIL_SENSOR_READ_LOST;

              /** Shut down the pump */
             Serial.println( " LOST SOIL SENSOR INPUT PUMP DISABLED " );

             /** - Turn pump off*/
             ubPumpEn = FALSE;

        }
        else if ( (ubTempSoilADC <= SOIL_WILTING_POINT_ADC_READ) && ( ubTempSoilADC > LOST_SOIL_ADC_READ))
        {
            /** - report the soil is Too Dry*/
            ubTempSoilDisplay = WILTING_POINT;

            /** - Turn pump on and not overriden by low level*/
            if ( FALSE == ubTempUltraSonicPumpOverride)
            {
                ubPumpEn = TRUE;

                /** - Used for Hysteresis */
                ubPumpEnPrevious = TRUE;
            }
            else
            {
                ubPumpEn = FALSE;
            }
        }
        else if ( (ubTempSoilADC >= SOIL_FIELD_CAPACITY_ADC_READ) && (ubTempSoilADC <= SOIL_SATURATION_ADC_READ) )
        {
            /** - report the soil is good*/
            ubTempSoilDisplay = FIELD_CAPACITY;

            /** - Hysteresis Checking */
            if( (ubTempSoilADC >= SOIL_WILTING_POINT_HYST_ADC_READ) && (ubPumpEnPrevious == TRUE) )
            {
                /** - Turn pump off*/
                ubPumpEn = FALSE;

                /** - Used for Hysteresis */
                ubPumpEnPrevious = FALSE;
            }
        }
        else if (ubTempSoilADC > SOIL_SATURATION_ADC_READ)
        {
            /** - report the soil is wet*/
            ubTempSoilDisplay = SATURATION;

            /** - Turn pump off*/
            ubPumpEn = FALSE;
        }

        /** Display something */
        //gI2CLCD_VarFormat(FALSE,ubTempSoilADC, ubTempSoilDisplay);

        /** Reset Count */
        ubTenSecondsCnt = 0;
     }

    /** - Message Contol Section*/
    if( (ubDisplayStateMchCnt >= TWO_SECONDS_SCROLL_IN_1000_MS_TASK) && ( TRUE == ubNormalModeLockOut) )
    {
        /** - Reset the count */
        ubDisplayStateMchCnt = 0;

       switch (ubDisplayStateMch)
       {
           case SOIL_MESSAGE_DISPLAY:
                /** Display something */
                gI2CLCD_VarFormat(FALSE,ubTempSoilADC, ubTempSoilDisplay);

                /** - Display next state in 1 second*/
                ubDisplayStateMch = TANK_LEVEL_MESSAGE_DISPLAY;
                break;

          case TANK_LEVEL_MESSAGE_DISPLAY:
                /** Display something */
                gI2CLCD_VarFormat(FALSE, ubTempUltraSonic, ubTempUltraSonicDisplay );

                /** - Display next state in 1 second*/
                ubDisplayStateMch = PUMP_OUTPUT_MESSAGE_DISPLAY;
                break;

           case PUMP_OUTPUT_MESSAGE_DISPLAY:
                if ( ubPumpEn == TRUE)
                {
                    /** Display something */
                    gI2CLCD_VarFormat(FALSE, 0, PUMP_ENABLED );
                }
                else if ( ubPumpEn == FALSE)
                {
                    /** Display something */
                    gI2CLCD_VarFormat(FALSE, 0, PUMP_DISABLED );
                }
                /** - Display next state in 1 second*/
                ubDisplayStateMch = SOIL_MESSAGE_DISPLAY;
                break;

            default:
            break;
       }
    }
    /** - Pump Control Section */
    if ( ubNormalOpModeOvrdPumpEn == TRUE)
    {
         /* Disable Pump */
        if (  ubTempUltraSonicPumpOverride == TRUE )
        {
            /** Force the override off */
            ubgvOvrdPumpEn = FALSE;
        }

        /** - Manual pump control */
        gvPumpEn(ubgvOvrdPumpEn);
    }
    else
    {
        /** - Normal pump control */
        gvPumpEn(ubPumpEn);
    }
    /** Increment Counts*/
    ubDisplayStateMchCnt++;
    ubFiveSecondsCnt++;
    ubTenSecondsCnt++;
}

/**
   @fn      gvUltraSonic();

   @brief   ultrasonic read.

   @param   N/A

   @return  N/A

   @note    The SR04))) is triggered by a HIGH pulse of 2 or more microseconds.
            Give a short LOW pulse beforehand to ensure a clean HIGH pulse:

            If pin 7 echo lost then watchdogs occur
   @author  Tom Rode

   @date    12/28/2015

*/
uint8_t gvUltraSonic(void)
{
    if (ubUltranSonicDisable == FALSE)
    {
        /* SENSOR 1 HANDLING*/
        pinMode(TriggerSensor1, OUTPUT);
        digitalWrite(TriggerSensor1, LOW);
        delayMicroseconds(2);
        digitalWrite(TriggerSensor1, HIGH);
        delayMicroseconds(5);
        digitalWrite(TriggerSensor1, LOW);

        /** The same pin is used to read the signal from the SR04))): a HIGH */
        /** pulse whose duration is the time (in microseconds) from the sending */
        /** of the ping to the reception of its echo off of an object. */
        pinMode(EchoSensor1, INPUT);
        SensorDuration1 = pulseIn(EchoSensor1, HIGH);

        /** convert the time into a distance */
        SensorInches1 = microsecondsToInches(SensorDuration1);
        SensorCm1 = microsecondsToCentimeters(SensorDuration1);
    }
    else
    {
        /** - Display something to indicate sensor override */
        SensorInches1 = 999;
    }

    /** print the results to the serial monitor: */
    Serial.print(" SensorInches1 = ");
    Serial.print(SensorInches1);
    Serial.print("  SensorCm1 = ");
    Serial.println(  SensorCm1);

    /*END SENSOR 1 HANDLING*/
    return(SensorInches1);
}

/**
   @fn      long microsecondsToInches(long microseconds)

   @brief   Convert time to inches measurement.

   @param   micro seconds

   @return  inches measurement

   @note    According to Parallax's datasheet for the PING))), there are.
            73.746 microseconds per inch (i.e. sound travels at 1130 feet per
            second).  This gives the distance travelled by the ping, outbound
            and return, so we divide by 2 to get the distance of the obstacle.
            See: http://www.parallax.com/dl/docs/prod/acc/28015-PING-v1.3.pdf

   @author  Tom Rode

   @date    12/28/2015

*/
long microsecondsToInches(long microseconds)
{
  /** ###Functional overview: */

  return microseconds / 74 / 2;
}

/**
   @fn      long microsecondsToCentimeters(long microseconds)

   @brief   Convert time to centimeters measurement.

   @param   micro seconds

   @return  centimeter measurement

   @note    The speed of sound is 340 m/s or 29 microseconds per centimeter.
            The ping travels out and back, so to find the distance of the
            object we take half of the distance travelled.

   @author  Tom Rode

   @date    12/28/2015

*/
long microsecondsToCentimeters(long microseconds)
{
  /** ###Functional overview: */

  return microseconds / 29 / 2;
}

/**
   @fn      gADCread()

   @brief   All ADC conversion read.

   @param   ADC Channel

   @return  ubTemp = 8 nit ADC read

   @note    TBD.

   @author  Tom Rode

   @date    12/28/2015

*/
uint8_t gADCread(uint8_t ADChn)
{
  /** ###Functional overview: */
uint8_t ubTemp;

    if(ADC_SOIL_SENSOR)
    {
        /** read the analog in value */
        sensorValue = analogRead(analogInPin);

        /** truncated lower 8 bits from 10 bit conversion */
        TruncatedSoilSenOutputValue = ( sensorValue >> 2 );

        /** make the read readable in volts */
        AdcVoltsRead = TruncatedSoilSenOutputValue * (5.0 / 255.0);

        /** print the results to the serial monitor: */
        //Serial.print(" ADC 8bit = ");
        //Serial.print(  TruncatedSoilSenOutputValue);
        //Serial.print(" sensor in volts = ");
        //Serial.println(  AdcVoltsRead);

        /** output to be returned*/
        ubTemp = TruncatedSoilSenOutputValue;
    }


return(ubTemp);
}

/**
   @fn      gADCAverageRead()

   @brief   Average the Soil sensor readings.

   @param   ADC Channel

   @return  ubTemp = 8 bit ADC averaged read

   @note    TBD.

   @author  Tom Rode

   @date    07/06/2016

*/
uint8_t gADCAverageRead(uint8_t ADCIn)
{
static uint16_t uwAggregateCnt;
static uint8_t  ubDivideByCnt;
static uint8_t  ubResult;

    if ( ubDivideByCnt < SOIL_SEN_ADC_AVERAGE_DIVISOR )
    {
        /** - Get a Summing of Current ADC in */
        uwAggregateCnt += ADCIn;

        /** - Increment the counter */
        ubDivideByCnt++;

        /** - This should be what ever was the last result */
        ubResult = ubResult;
    }
    if( SOIL_SEN_ADC_AVERAGE_DIVISOR == ubDivideByCnt )
    {
        /** - Make the average calculation */
        ubResult = (uwAggregateCnt / SOIL_SEN_ADC_AVERAGE_DIVISOR);

        /** - Clear out the running total */
        uwAggregateCnt = 0;

        /** - Zero the counter */
        ubDivideByCnt = 0;
    }
return(ubResult);
}

/**
   @fn      void gBlink(int UwTimes)

   @brief   Blink an output pin for indication.

   @param   UwTimes =  Amount of times to blink,
   @param   ubRepeat = if to blink

   @return  N/A

   @note    TBD.

   @author  Tom Rode

   @date    12/28/2015

*/
void gBlink(int UwTimes, byte ubRepeat)
{
  int uwindex;

  /** ###Functional overview: */

  /** Toggle LED */
  if ( FALSE == ubRepeat )
  {
    /** Back and fourth */
    if ( TRUE == MODULE_LED )
    {
      /** Toggle LED */
      MODULE_LED = FALSE;
      digitalWrite(BLINK_LED, TRUE);   // turn the LED on (HIGH is the voltage level)
    }
    else
    {
      MODULE_LED = TRUE;
      digitalWrite(BLINK_LED, LOW);   // turn the LED on (HIGH is the voltage level)
    }
  }
  else
  {
    /** Amount to blink a pin */
    for ( uwindex = 0; uwindex < UwTimes; uwindex++ )
    {
      /** Blink the indicator LED specified amount of times*/
      if ( TRUE == MODULE_LED )
      {
        MODULE_LED = FALSE;
        digitalWrite(BLINK_LED, HIGH);   // turn the LED on (HIGH is the voltage level)
      }
      else
      {
        MODULE_LED = TRUE;
        digitalWrite(BLINK_LED, LOW);   // turn the LED on (HIGH is the voltage level)
      }

      /** - Delay will be visible */
      delay(100);
    }
  }
}

/**
   @fn       GetDebRowSw(void)

   @brief    This function returns Debounced Switch state

   @param    None

   @return   N/A

   @note     Debounced Switch state after five consecutive 1-ms reads

   @author   Tom Rode

*/
void vGetDebRowSw(void)
{

  /** ###Functional overview: */

  /** - Read in state of push button value */
  gKey_Switches.ubHistory[gKey_Switches.ubPointer] = digitalRead(BUTTON1);

  /** - Capture five same state readings */
  if ((gKey_Switches.ubHistory[0] == gKey_Switches.ubHistory[1]) &&
      (gKey_Switches.ubHistory[1] == gKey_Switches.ubHistory[2]) &&
      (gKey_Switches.ubHistory[2] == gKey_Switches.ubHistory[3]) &&
      (gKey_Switches.ubHistory[3] == gKey_Switches.ubHistory[4]) )

  {
    /** - Five of the same state readings place into respective byte in switches array, index is correct now*/
    gKey_Switches.uwSwitches[0] = gKey_Switches.ubHistory[0];

  }
  /** - increment the count */
  gKey_Switches.ubPointer++;

  /** - Five of the same read state for state latching purposes*/
  if (gKey_Switches.ubPointer > 4)
  {
    /** - reset the count */
    gKey_Switches.ubPointer = 0;
  }

}

/**
   @fn      gvPumpEn();

   @brief   Submersible pump.

   @param   N/A

   @return  N/A

   @note

   @author  Tom Rode

   @date    12/28/2015

*/
void gvPumpEn(byte Enable)
{
    /** - Turn pump on or off*/
    if ((TRUE == Enable) || (TRUE == ubNormalOpMode) )/** turn off pump here to*/
    {
         digitalWrite(PUMP, TRUE);   // turn the LED on (HIGH is the voltage level)
    }
    if ( (FALSE == Enable ) || (FALSE == ubNormalOpMode) )
    {
         digitalWrite(PUMP, FALSE);   // turn the LED off (LOW is the voltage level)
    }
    if ( (TRUE == ubNormalOpModeOvrdPumpEn) && (TRUE == Enable) )
    {
         digitalWrite(PUMP, TRUE);   // turn the LED on (HIGH is the voltage level)
    }
}

/**
* @fn    void gI2CLCD_VarFormat(uint8_t ubVal)
*
* @brief  This function Will format a byte worth and places the digit in repective cursor position.
*
* @param  ubDisplayVal = Flag used to enable/diable 8 bit value dispaly.
* @param  ubVal = 8 bit value to be displayed.
* @param  ubMsg = List of predetermined messages to be displayed.
*
* @return N/A.
*
* @author Tom Rode
*
* @note  N/A
*
*/
void gI2CLCD_VarFormat(uint8_t ubDisplayVal, uint8_t ubVal, uint8_t ubMsg)
{
uint8_t i;
uint8_t ubOnesOut=0;
uint8_t ubTensCnt=0;
uint8_t ubTensAmt=0;
uint8_t ubTensOut=0;
uint8_t ubHundCnt=0;
uint8_t ubHundAmt=0;
uint8_t ubHundOut=0;

    for (i=0; i<ubVal; i++)
    {
        if ( ubTensCnt >= 9 )
        {
            /** Tally up the amount of tens*/
            ubTensAmt++;

            /** Reset tens count*/
            ubTensCnt=0;
        }
        else
        {
            /** increment tens count*/
            ubTensCnt++;
        }

        if ( ubHundCnt >= 99 )
        {
            /** Tally up the amount of Hundred*/
            ubHundAmt++;

            /** Reset hundred count*/
            ubHundCnt=0;
        }
        else
        {
          /** increment hundred count*/
          ubHundCnt++;
        }
    }
        /** Count Correction for output*/
        ubHundOut = ubHundAmt;
        ubTensOut = (ubHundAmt * 10);
        ubTensOut = (ubTensAmt - ubTensOut);
        ubOnesOut = (ubVal - ((ubHundAmt * 100) + (ubTensOut * 10)));
        /** - ASCII Encoded */
        ubHundOut += 0x30;
        ubTensOut += 0x30;
        ubOnesOut += 0x30;

        /** LCD outptu control*/
        lcd.clear();
        /** - Message line */

        switch ( ubMsg )
        {
            case SOIL_SENSOR_READ_LOST:
                lcd.setCursor(0,0); //Start at character 0 on line 0
                lcd.print("SOIL SENSOR:");
                lcd.setCursor(0,1);
                lcd.print(" CHECK SENSOR");
                break;


            case SATURATION:
                lcd.setCursor(0,0); //Start at character 0 on line 0
                lcd.print("SOIL SENSOR:");
                lcd.setCursor(0,1);
                lcd.print(" SATURATION");
                break;

            case FIELD_CAPACITY:
                lcd.setCursor(0,0); //Start at character 0 on line 0
                lcd.print("SOIL SENSOR:");
                lcd.setCursor(0,1);
                lcd.print(" FIELD CAPACITY");
                break;

            case WILTING_POINT:
                lcd.setCursor(0,0); //Start at character 0 on line 0
                lcd.print("SOIL SENSOR:");
                lcd.setCursor(0,1);
                lcd.print(" WILTING POINT");
                break;

            case ADC_SOIL_SENSOR:
                lcd.setCursor(0,0); //Start at character 0 on line 0
                lcd.print("ADC SOIL SENSOR");
                if ( ubDisplayVal == 1 )
                {
                    lcd.setCursor(5,1);
                    lcd.write(ubHundOut);
                    lcd.setCursor(6,1);
                    lcd.write(ubTensOut);
                    lcd.setCursor(7,1);
                    lcd.write(ubOnesOut);
                }
                break;

            case PERCENT_100_FULL:
                lcd.setCursor(0,0); //Start at character 0 on line 0
                lcd.print("TANK LEVEL: ");
                lcd.setCursor(0,1);
                lcd.print(" 100% FULL");
                break;

            case PERCENT_90_FULL:
                lcd.setCursor(0,0); //Start at character 0 on line 0
                lcd.print("TANK LEVEL: ");
                lcd.setCursor(0,1);
                lcd.print(" 90% FULL");
                break;

            case PERCENT_80_FULL:
                lcd.setCursor(0,0); //Start at character 0 on line 0
                lcd.print("TANK LEVEL: ");
                lcd.setCursor(0,1);
                lcd.print(" 80% FULL");
                break;

            case PERCENT_70_FULL:
                lcd.setCursor(0,0); //Start at character 0 on line 0
                lcd.print("TANK LEVEL: ");
                lcd.setCursor(0,1);
                lcd.print(" 70% FULL");
                break;

            case PERCENT_60_FULL:
                lcd.setCursor(0,0); //Start at character 0 on line 0
                lcd.print("TANK LEVEL: ");
                lcd.setCursor(0,1);
                lcd.print(" 60% FULL");
                break;

            case PERCENT_50_FULL:
                lcd.setCursor(0,0); //Start at character 0 on line 0
                lcd.print("TANK LEVEL: ");
                lcd.setCursor(0,1);
                lcd.print(" 50% FULL");
                break;

            case PERCENT_40_FULL:
                lcd.setCursor(0,0); //Start at character 0 on line 0
                lcd.print("TANK LEVEL: ");
                lcd.setCursor(0,1);
                lcd.print(" 40% FULL");
                break;

            case PERCENT_30_FULL:
                lcd.setCursor(0,0); //Start at character 0 on line 0
                lcd.print("TANK LEVEL: ");
                lcd.setCursor(0,1);
                lcd.print(" 30% FULL");
                break;

            case PERCENT_20_FULL:
                lcd.setCursor(0,0); //Start at character 0 on line 0
                lcd.print("TANK LEVEL:");
                lcd.setCursor(0,1);
                lcd.print(" 20% FULL");
                break;

            case PERCENT_10_FULL:
                lcd.setCursor(0,0); //Start at character 0 on line 0
                lcd.print("TANK LEVEL: ");
                lcd.setCursor(0,1);
                lcd.print(" 10% FULL");
                break;

            case PERCENT_00_FULL:
                lcd.setCursor(0,0); //Start at character 0 on line 0
                lcd.print("TANK LEVEL: ");
                lcd.setCursor(0,1);
                lcd.print(" EMPTY     ");
                break;

            case ULTRA_SONIC_SENSOR:
                lcd.setCursor(0,0); //Start at character 0 on line 0
                lcd.print("ULTRA SONIC SENSOR");
                if ( ubDisplayVal == 1 )
                {
                    lcd.setCursor(5,1);
                    lcd.write(ubHundOut);
                    lcd.setCursor(6,1);
                    lcd.write(ubTensOut);
                    lcd.setCursor(7,1);
                    lcd.write(ubOnesOut);
                }
                break;

           case PUMP_ENABLED:
                lcd.setCursor(0,0); //Start at character 0 on line 0
                lcd.print("PUMP STATUS:    ");
                lcd.setCursor(0,1);
                lcd.print(" ON             ");
                break;

          case PUMP_DISABLED:
               lcd.setCursor(0,0); //Start at character 0 on line 0
               lcd.print("PUMP STATUS:    ");
               lcd.setCursor(0,1);
               lcd.print(" OFF            ");
               break;

          case PUMPOVRD_ENABLED_IN_3_SEC:
               lcd.setCursor(0,0); //Start at character 0 on line 0
               lcd.print("PUMP OVERRIDE:  ");
               lcd.setCursor(0,1);
               lcd.print("IN 3 SECONDS    ");
               break;

          case PUMPOVRD_ENABLED_IN_2_SEC:
               lcd.setCursor(0,0); //Start at character 0 on line 0
               lcd.print("PUMP OVERRIDE:  ");
               lcd.setCursor(0,1);
               lcd.print("IN 2 SECONDS    ");
               break;

          case PUMPOVRD_ENABLED_IN_1_SEC:
               lcd.setCursor(0,0); //Start at character 0 on line 0
               lcd.print("PUMP OVERRIDE:  ");
               lcd.setCursor(0,1);
               lcd.print("IN 1 SECONDS    ");
               break;

         case PUMPOVRD_ENABLED_TO_NORMAL_MODE:
               lcd.setCursor(0,0); //Start at character 0 on line 0
               lcd.print("HIT BUTTON FOR ");
               lcd.setCursor(0,1);
               lcd.print("  NORMAL MODE  ");
               break;

         case PUMPOVRD_PERCENT_00_FULL:
              lcd.setCursor(0,0); //Start at character 0 on line 0
              lcd.print("PUMP STATUS: OFF");
              lcd.setCursor(0,1);
              lcd.print("TANK: EMPTY     ");
              break;

        case INTRO:
              lcd.setCursor(0,0); //Start at character 0 on line 0
              lcd.print("GARDEN WATERER  ");
              lcd.setCursor(0,1);
              lcd.print("Version 1.4    ");
              break;

         case SATURATION_THR_INTRO:
              lcd.setCursor(0,0); //Start at character 0 on line 0
              lcd.print("SATURATION THR");
              if ( ubDisplayVal == 1 )
              {
                  lcd.setCursor(5,1);
                  lcd.write(ubHundOut);
                  lcd.setCursor(6,1);
                  lcd.write(ubTensOut);
                  lcd.setCursor(7,1);
                  lcd.write(ubOnesOut);
               }
               break;

        case FIELD_THR_INTRO:
             lcd.setCursor(0,0); //Start at character 0 on line 0
             lcd.print("FIELD CAP THR");
             if ( ubDisplayVal == 1 )
             {
                 lcd.setCursor(5,1);
                 lcd.write(ubHundOut);
                 lcd.setCursor(6,1);
                 lcd.write(ubTensOut);
                 lcd.setCursor(7,1);
                 lcd.write(ubOnesOut);
              }
              break;

        case WILTING_THR_INTRO:
             lcd.setCursor(0,0); //Start at character 0 on line 0
             lcd.print("WILTING THR");
             if ( ubDisplayVal == 1 )
             {
                 lcd.setCursor(5,1);
                 lcd.write(ubHundOut);
                 lcd.setCursor(6,1);
                 lcd.write(ubTensOut);
                 lcd.setCursor(7,1);
                 lcd.write(ubOnesOut);
              }
              break;

        case AVG_SOIL_READING:
             lcd.setCursor(0,0); //Start at character 0 on line 0
             lcd.print("AVG SOIL READING");
             if ( ubDisplayVal == 1 )
             {
                 lcd.setCursor(5,1);
                 lcd.write(ubHundOut);
                 lcd.setCursor(6,1);
                 lcd.write(ubTensOut);
                 lcd.setCursor(7,1);
                 lcd.write(ubOnesOut);
              }
              break;

              default:
                break;
        }
}


