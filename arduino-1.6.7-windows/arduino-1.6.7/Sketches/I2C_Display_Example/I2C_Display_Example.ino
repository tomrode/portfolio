/* YourDuino.com Example Software Sketch
 16 character 2 line I2C Display
 Backpack Interface labelled "A0 A1 A2" at lower right.
 ..and
 Backpack Interface labelled "YwRobot Arduino LCM1602 IIC V1"
 MOST use address 0x27, a FEW use 0x3F
 terry@yourduino.com */

/*-----( Import needed libraries )-----*/
#include <Wire.h>  // Comes with Arduino IDE
// Get the LCD I2C Library here: 
// https://bitbucket.org/fmalpartida/new-liquidcrystal/downloads
// Move any other LCD libraries to another folder or delete them
// See Library "Docs" folder for possible commands etc.
#include <LiquidCrystal_I2C.h>

/*-----( Declare Constants )-----*/
/*-----( Declare objects )-----*/
// set the LCD address to 0x27 for a 16 chars 2 line display
// A FEW use address 0x3F
// Set the pins on the I2C chip used for LCD connections:
//                    addr, en,rw,rs,d4,d5,d6,d7,bl,blpol
LiquidCrystal_I2C lcd(0x27, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);  // Set the LCD I2C address

/*-----( Declare Variables )-----*/
//NONE

typedef enum
{
   SOIL_SENSOR_READ    = 0,   /** < Line 0 */
   SATURATION          = 1,   /** < Line 1 */
   FIELD_CAPACITY      = 2,   /** < Line 1 */
   WILTING_POINT       = 3,   /** < Line 1 */
   ADC_SOIL_SENSOR     = 4,   /** < Line 0 */  
   PERCENT_100_FULL    = 5,   /** < Line 1 */
   PERCENT_90_FULL     = 6,   /** < Line 1 */
   PERCENT_80_FULL     = 7,   /** < Line 1 */
   PERCENT_70_FULL     = 8,   /** < Line 1 */
   PERCENT_60_FULL     = 9,  /** < Line 1 */
   PERCENT_50_FULL     = 10,  /** < Line 1 */
   PERCENT_40_FULL     = 11,  /** < Line 1 */
   PERCENT_30_FULL     = 12,  /** < Line 1 */
   PERCENT_20_FULL     = 13,  /** < Line 1 */
   PERCENT_10_FULL     = 14,  /** < Line 1 */
   PERCENT_00_FULL     = 15,  /** < Line 1 */
   ULTRA_SONIC_SENSOR  = 16   /** < Line 0 */
   
}DisplayMessages;

void setup()   /*----( SETUP: RUNS ONCE )----*/
{
  Serial.begin(9600);  // Used to type in characters

  lcd.begin(16,2);   // initialize the lcd for 16 chars 2 lines, turn on backlight

// ------- Quick 3 blinks of backlight  -------------
  for(int i = 0; i< 3; i++)
  {
    lcd.backlight();
    delay(250);
    lcd.noBacklight();
    delay(250);
  }
  lcd.backlight(); // finish with backlight on  

//-------- Write characters on the display ------------------
// NOTE: Cursor Position: (CHAR, LINE) start at 0  
  lcd.setCursor(0,0); //Start at character 4 on line 0
  lcd.print("Hello, world!");
  delay(1000);
  lcd.setCursor(0,1);
  lcd.print("HI!YourDuino.com");
  delay(1000);  

// Wait and then tell user they can start the Serial Monitor and type in characters to
// Display. (Set Serial Monitor option to "No Line Ending")
  lcd.clear();
  lcd.setCursor(0,0); //Start at character 0 on line 0
  lcd.print("Use Serial Mon");
  lcd.setCursor(0,1);
  lcd.print("Type to display");  
}/*--(end setup )---*/

/**
 * @fn     void gI2CLCD_VarFormat(uint8_t ubVal)
 *
 * @brief  This function Will format a byte worth and places the digit in repective cursor position.
 *  
 * @param  ubDisplayVal = Flag used to enable/diable 8 bit value dispaly.  
 * @param  ubVal = 8 bit value to be displayed. 
 * @param  ubMsg = List of predetermined messages to be displayed.
 *  
 * @return N/A.
 * 
 * @author Tom Rode 
 *  
 * @note   N/A 
 *
 */
void gI2CLCD_VarFormat(uint8_t ubDisplayVal, uint8_t ubVal, uint8_t ubMsg)
{
uint8_t i;
uint8_t ubOnesOut=0;
uint8_t ubTensCnt=0;
uint8_t ubTensAmt=0;
uint8_t ubTensOut=0;
uint8_t ubHundCnt=0;
uint8_t ubHundAmt=0;
uint8_t ubHundOut=0;

    for (i=0; i<ubVal; i++)
    {
        if ( ubTensCnt >= 9 )
        {
            /** Tally up the amount of tens*/
            ubTensAmt++;

            /** Reset tens count*/
            ubTensCnt=0;
        }
        else
        {
            /** increment tens count*/
            ubTensCnt++;
        } 

        if ( ubHundCnt >= 99 )
        {
            /** Tally up the amount of Hundred*/
            ubHundAmt++;

            /** Reset hundred count*/
            ubHundCnt=0;
        }
        else
        {
           /** increment hundred count*/
           ubHundCnt++;
        } 
     }
         /** Count Correction for output*/
         ubHundOut = ubHundAmt; 
         ubTensOut = (ubHundAmt * 10);
         ubTensOut = (ubTensAmt - ubTensOut);
         ubOnesOut = (ubVal - ((ubHundAmt * 100) + (ubTensOut * 10)));
        /** - ASCII Encoded */
         ubHundOut += 0x30;
         ubTensOut += 0x30;
         ubOnesOut += 0x30;
  
         /** LCD outptu control*/
         lcd.clear();         
         /** - Message line */

         switch ( ubMsg )
         {
            case SATURATION:
                 lcd.setCursor(0,0); //Start at character 0 on line 0
                 lcd.print("SOIL SENSOR READ");
                 lcd.setCursor(0,1);
                 lcd.print("SATURATION");
                 break;

            case FIELD_CAPACITY:
                 lcd.setCursor(0,0); //Start at character 0 on line 0
                 lcd.print("SOIL SENSOR READ");
                 lcd.setCursor(0,1);
                 lcd.print("FIELD CAPACITY");
                 break;

            case WILTING_POINT:
                 lcd.setCursor(0,0); //Start at character 0 on line 0
                 lcd.print("SOIL SENSOR READ");
                 lcd.setCursor(0,1);
                 lcd.print("WILTING POINT");
                 break;

            case ADC_SOIL_SENSOR:
                 lcd.setCursor(0,0); //Start at character 0 on line 0
                 lcd.print("ADC SOIL SENSOR");
                 if ( ubDisplayVal == 1 )
                 {
                     lcd.setCursor(5,1);
                     lcd.write(ubHundOut);
                     lcd.setCursor(6,1);
                     lcd.write(ubTensOut);
                     lcd.setCursor(7,1);
                     lcd.write(ubOnesOut);
                 }   
                 break;
                 
            case PERCENT_100_FULL:
                 lcd.setCursor(0,0); //Start at character 0 on line 0
                 lcd.print("TANK LEVEL READ");
                 lcd.setCursor(0,1);
                 lcd.print("PERCENT 100 FULL");
                 break;

            case PERCENT_90_FULL:
                 lcd.setCursor(0,0); //Start at character 0 on line 0
                 lcd.print("TANK LEVEL READ");
                 lcd.setCursor(0,1);
                 lcd.print("PERCENT 90 FULL");
                 break;

            case PERCENT_80_FULL:
                 lcd.setCursor(0,0); //Start at character 0 on line 0
                 lcd.print("TANK LEVEL READ");
                 lcd.setCursor(0,1);
                 lcd.print("PERCENT 80 FULL");
                 break;

            case PERCENT_70_FULL:
                 lcd.setCursor(0,0); //Start at character 0 on line 0
                 lcd.print("TANK LEVEL READ");
                 lcd.setCursor(0,1);
                 lcd.print("PERCENT 70 FULL");
                 break;

            case PERCENT_60_FULL:
                 lcd.setCursor(0,0); //Start at character 0 on line 0
                 lcd.print("TANK LEVEL READ");
                 lcd.setCursor(0,1);
                 lcd.print("PERCENT 60 FULL");
                 break;

            case PERCENT_50_FULL:
                 lcd.setCursor(0,0); //Start at character 0 on line 0
                 lcd.print("TANK LEVEL READ");
                 lcd.setCursor(0,1);
                 lcd.print("PERCENT 50 FULL");
                 break;

            case PERCENT_40_FULL:
                 lcd.setCursor(0,0); //Start at character 0 on line 0
                 lcd.print("TANK LEVEL READ");
                 lcd.setCursor(0,1);
                 lcd.print("PERCENT 40 FULL");
                 break;

            case PERCENT_30_FULL:
                 lcd.setCursor(0,0); //Start at character 0 on line 0
                 lcd.print("TANK LEVEL READ");
                 lcd.setCursor(0,1);
                 lcd.print("PERCENT 30 FULL");
                 break;

            case PERCENT_20_FULL:
                 lcd.setCursor(0,0); //Start at character 0 on line 0
                 lcd.print("TANK LEVEL READ");
                 lcd.setCursor(0,1);
                 lcd.print("PERCENT 20 FULL");
                 break;

            case PERCENT_10_FULL:
                 lcd.setCursor(0,0); //Start at character 0 on line 0
                 lcd.print("TANK LEVEL READ");
                 lcd.setCursor(0,1);
                 lcd.print("PERCENT 10 FULL");
                 break;

            case PERCENT_00_FULL:
                 lcd.setCursor(0,0); //Start at character 0 on line 0
                 lcd.print("TANK LEVEL READ");
                 lcd.setCursor(0,1);
                 lcd.print("PERCENT 00 FULL");
                 break;  

            case ULTRA_SONIC_SENSOR:
                 lcd.setCursor(0,0); //Start at character 0 on line 0
                 lcd.print("ULTRA SONIC SENSOR");
                 if ( ubDisplayVal == 1 )
                 {
                     lcd.setCursor(5,1);
                     lcd.write(ubHundOut);
                     lcd.setCursor(6,1);
                     lcd.write(ubTensOut);
                     lcd.setCursor(7,1);
                     lcd.write(ubOnesOut);
                 }   
                 break;                  
               default:
                 break;  
         }
        
        
}
char AsciiCnt = 0x30;
void loop()
{
uint8_t ubTestVal;

  for (ubTestVal=0; ubTestVal <= 255; ubTestVal++)
  {
    //void gI2CLCD_VarFormat(uint8_t ubDisplayVal, uint8_t ubVal, uint8_t ubMsg)
      gI2CLCD_VarFormat(0,ubTestVal, PERCENT_80_FULL);
      delay(400);
  }
}

/* ( THE END ) */

