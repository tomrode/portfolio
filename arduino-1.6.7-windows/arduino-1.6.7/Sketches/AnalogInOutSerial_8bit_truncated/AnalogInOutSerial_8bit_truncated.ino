/*
  Analog input, analog output, serial output

 Reads an analog input pin, maps the result to a range from 0 to 255
 and uses the result to set the pulsewidth modulation (PWM) of an output pin.
 Also prints the results to the serial monitor.

 The circuit:
 * potentiometer connected to analog pin 0.
   Center pin of the potentiometer goes to the analog pin.
   side pins of the potentiometer go to +5V and ground
 * LED connected from digital pin 9 to ground

 created 29 Dec. 2008
 modified 9 Apr 2012
 by Tom Igoe

 This example code is in the public domain.

 */
#include <EEPROM.h> 

// These constants won't change.  They're used to give names
// to the pins used:
const int analogInPin = A0;  // Analog input pin that the potentiometer is attached to
const int analogOutPin = 9; // Analog output pin that the LED is attached to
const unsigned long TEN_MINUTES = 600000; // 1000ms * 600 Seconds = 10 minutes
const unsigned long ONE_MINUTES = 60000; // 1000ms * 60 Seconds = 1 minutes

int address = 0;
int sensorValue = 0;        // value read from the pot
int outputValue = 0;        // value output to the PWM (analog out)
byte TruncatedOutputValue = 0; // 8 bit value
byte value;
byte i = 0;
byte blinking = 0;
float AdcVoltsRead;

void setup() {
   // initialize serial communications at 9600 bps:
   Serial.begin(9600);

   // initialize digital pin 13 as an output.
   pinMode(13, OUTPUT);
   // initialize digital pin 12 as an output.
   pinMode(12, OUTPUT);

}

/******************************************************************
 * USE THIS TO READ OUT FROM EEPROM
 ******************************************************************/
void loop1() {
  // read a byte from the current address of the EEPROM
  value = EEPROM.read(address);

  Serial.print(address);
  Serial.print("\t");
  Serial.print(value, DEC);
  Serial.println();

  /***
    Advance to the next address, when at the end restart at the beginning.

    Larger AVR processors have larger EEPROM sizes, E.g:
    - Arduno Duemilanove: 512b EEPROM storage.
    - Arduino Uno:        1kb EEPROM storage.
    - Arduino Mega:       4kb EEPROM storage.

    Rather than hard-coding the length, you should use the pre-provided length function.
    This will make your code portable to all AVR processors.
  ***/
  address = address + 1;
  if (address == EEPROM.length()) {
    address = 0;
  }

  delay(100);
}

/******************************************************************
 * USE THIS TO WRITE ADC VALUE TO EEPROM
 ******************************************************************/
void loop() {
  /* Give an indication that ADC started only once*/
  if(0 == i)
  {
      for (i=0; i<=2; i++)
      {
         if(0 == i)
         {
            digitalWrite(12, HIGH);   // turn the LED on (HIGH is the voltage level)
         }
         if(1 == i)
         {
            digitalWrite(12, LOW);   // turn the LED on (LOW is the voltage level)
         }
         if(2 == i)
         {
            digitalWrite(12, HIGH);   // turn the LED on (HIGH is the voltage level)
         }
     
         delay(500); 
      }
  }
  
  // read the analog in value:
  sensorValue = analogRead(analogInPin);
  // truncated lower 8 bits
  TruncatedOutputValue = ( sensorValue >> 2 );
  // map it to the range of the analog out:
  outputValue = map(sensorValue, 0, 1023, 0, 255);
  // change the analog out value:
  analogWrite(analogOutPin, outputValue);
  // make the read readable in volts
  AdcVoltsRead = TruncatedOutputValue * (5.0 / 255.0);

  // print the results to the serial monitor:
  Serial.print(" sensor10bit = ");
  Serial.print(sensorValue);
  Serial.print(" truncated8bit = ");
  Serial.print(  TruncatedOutputValue);
  Serial.print(" sensor in volts = ");
  Serial.println(  AdcVoltsRead);
  
  

  // write to EEprom
  if( 0 == address)
  {
      /* Show the start of EEprom with a distinctive value A6 is 166 decimal */
      EEPROM.write(address, 0xA6);

       /* Update the address to write to EEprom*/
       address++; 
  }
  else if ( 1022 >= address)
  {    
       /* write ADC value in a range*/
       EEPROM.write(address, TruncatedOutputValue);

       /* Update the address to write to EEprom*/
       address++; 
  }
  else
  {
      /* write ADC value*/
      EEPROM.write(address, 0xF4);
      while(1)
      {
          if(0 == blinking)
          {  
             digitalWrite(12, LOW);   // Stuck here forever turn the LED on (LOW is the voltage level)

             blinking = 1;
          }
          else
          {  
             digitalWrite(12, HIGH);   // Stuck here forever turn the LED on (HIGH is the voltage level)

             blinking = 0;
          }
          delay(100); 
      }
  }
  
  // wait 2 or more milliseconds before the next loop
  // for the analog-to-digital converter to settle
  // after the last reading:
  delay(1000 /*TEN_MINUTES*/); 
}
