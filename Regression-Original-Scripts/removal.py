# Name:       removal.py
#
# Purpose:   This python script will delete files in the regression
#
# Author:    Thomas Rode
#
# Options:   None
#
# Note:
#
# Python Version: 2.7.11
#          
# Reference: https://www.python.org/ (open source)
#





import glob
import os
import shutil

def RemoveRegressionFiles():
    GlobalExtensionList = ["*.env",
                           "*.CFG",
                           "*.pptx",
                           "*.pyc",
                           "*.txt",
                           "*.tst",
                           "*.vce",
                           "*.tmp",
                           "*.bat",
                           "*.xlsx",
                          ]
    GlobalExtensionEndsWithList = ["0.env",
                                   "0.CFG",
                                   "0.pptx",
                                   "0.pyc",
                                   "0.txt",
                                   "0.tst",
                                   "0.vce",
                                   "0.tmp",
                                   "0.bat",
                                   "0.xlsx",
                                  ]
    # Remove unnecessary files out of the directory    
    try:
        for i in range(len(GlobalExtensionList)):
            for f in glob.glob(GlobalExtensionList[i]): # find all env files
                if not f.endswith(GlobalExtensionEndsWithList[i]):
                    print(f)
                    os.remove(f)  # if file name ends in 0.env, delete it
    except:
        print("Could not remove files")
    
# Main program - Supports execution from the command line
if __name__ == "__main__":
     RemoveRegressionFiles()
