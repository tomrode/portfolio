# Name:     Copy2Corp2013.py
#
# Purpose:   This python script will gather .html files to be sent to Corp2013
#            To be called after Emailing 
#
# Author:    Thomas Rode
#
# Options:   None
#
# Note:
#
# Python Version: 2.7.11
#          
# Reference: https://www.python.org/ (open source)
#

import os
import os.path
import shutil
from ParseHtmlForManage import ParseHtml


dest_path_corp2013 = r'\\corp2013\Vectorcast_Dev_Archive'
dest_path_corp2013_quality = r'\\corp2013\Vectorcast_Qual_Archive'
dest_path_corp2103_mbr = r'\\corp2013\mbs_buildserver\ModelBaseReport\VCAST'
dest_path_emaildevelopers = 'C:/vcast-buildsystem-orig/Regression-Original-Scripts/EmailDeveloperData'
rtc_txt_file = 'RTC_Stream.txt'
regression_unit_test_results_master = 'Regression_Unit_Test_Results_MASTER.vcr'
regression_unit_test_results_cbt_master = 'Regression_Unit_Test_Results_CBT_MASTER.vcr'
regression_unit_test_results_supv = 'Regression_Unit_Test_Results_SUPV.vcr'
regression_unit_test_results_cbt_supv = 'Regression_Unit_Test_Results_CBT_SUPV.vcr'
rtc_txt_file_dev = 'MAIN DEVELOPMENT STREAM'
rtc_txt_file_quality = 'QUALITY STREAM'
rtc_outfile ='C:/Embedded/iRTC Specific Files/antTmlVcmMasterBuildProperties.out'
definition3 = 'buildDefinitionId=TmlVcmMasterBuildDef003'

def Copy2Corp2013():
    
    print ("Start Copy to Corp2013 and C:\vcast-buildsystem-orig\Regression-Original-Scripts\EmailDeveloperData")
  
    # Get the Current working directory path + VCAST
    sourcepath = os.getcwd()
    
    # get path from text file to be displayed in email message
    fp = open('EmailPathFolderResults.txt', 'rb')  
    
    # Make a copy of this directory name to Corp2013", truncate first 25 characters C:\vcast-buildsystem-orig
    RegressionResultfolder = fp.read()
    RegressionResultfolder = RegressionResultfolder[25:]   
    fp.close()

    # Determine where results should go on corp2013 Dev vs Quality
    if rtc_txt_file_dev in open(rtc_txt_file,'r+').read():
        dest_path = (dest_path_corp2013 + RegressionResultfolder )
    elif rtc_txt_file_quality in open(rtc_txt_file,'r+').read():
        dest_path = (dest_path_corp2013_quality + RegressionResultfolder )

    # Get just Htmls
    justfails, bothpassfails = ParseHtml()
 
    try: 
        # Make a new folder in Corp2013 of time stamped regression
        os.makedirs(dest_path)
    
        # Get just Htmls
        for filename in bothpassfails:

            # Copy ParseHtmlForManage.py to /VCAST Directory, Copy the files from source to destination 
            if os.path.isfile(os.path.join(sourcepath, filename)):
                shutil.copy(os.path.join(sourcepath, filename), os.path.join(dest_path, filename))

        # Copy over RTC text file to know which Stream
        filename = rtc_txt_file
        if os.path.isfile(os.path.join(sourcepath, filename)):
            shutil.copy(os.path.join(sourcepath, filename), os.path.join(dest_path, filename))
    except:
        print("Error occured in Accessing or Writing to: Corp2013\Vectorcast_Dev_Archive")
        
    # This will copy HTMLS to ModelBaseReport Folder
    try:
        # Change the path to the mbs folder now
        dest_path = dest_path_corp2103_mbr

        # Copy to \mbs_buildserver\ModelBaseReport\VCAST  Directory, Copy the files from source to destination 
        for filename in bothpassfails:
            if os.path.isfile(os.path.join(sourcepath, filename)):
                shutil.copy(os.path.join(sourcepath, filename), os.path.join(dest_path, filename))

        # This will copy .vcr for Change Based Testing on MASTER file to ModelBaseReport Folder or the nightly build file
        if definition3 in open(rtc_outfile,'r+').read():
            filename = regression_unit_test_results_cbt_master
        else:
            filename = regression_unit_test_results_master
        if os.path.isfile(os.path.join(sourcepath, filename)):
            shutil.copy(os.path.join(sourcepath, filename), os.path.join(dest_path, filename))

         # This will copy .vcr for Change Based Testing on SUPV file to ModelBaseReport Folder or the nightly build file
        if definition3 in open(rtc_outfile,'r+').read():
            filename = regression_unit_test_results_cbt_supv
        else:
            filename = regression_unit_test_results_supv
        if os.path.isfile(os.path.join(sourcepath, filename)):
            shutil.copy(os.path.join(sourcepath, filename), os.path.join(dest_path, filename))
    
    except:
        print("Error occured in Accessing or Writing to: Corp2013\mbs_buildserver\ModelBaseReport\VCAST ")

    # This will copy the HTMLS to EmailDevelopersData folder  
    try:
        # Change the path to the mbs folder now
        dest_path = dest_path_emaildevelopers

        # Remove old html files 
        test = os.listdir(dest_path)
        for item in test:
            if item.endswith(".html"):
                os.remove(os.path.join(dest_path, item))
        # Copy failed .html's to Directory, Copy the files from source to destination 
        for filename in justfails:     
            if os.path.isfile(os.path.join(sourcepath, filename)):
                shutil.copy(os.path.join(sourcepath, filename), os.path.join(dest_path, filename))  
    except:
        print("Error occured in Accessing or Writing to: C:\vcast-buildsystem-orig\Regression-Original-Scripts\EmailDeveloperData ")
        
    print ("End Copy to Corp2013 and C:\vcast-buildsystem-orig\Regression-Original-Scripts\EmailDeveloperData")
     
if __name__ == '__main__':
    Copy2Corp2013()
