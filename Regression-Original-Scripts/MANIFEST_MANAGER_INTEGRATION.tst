-- VectorCAST 6.4p (01/31/17)
-- Test Case Script
-- 
-- Environment    : MANIFEST_MANAGER_INTEGRATION
-- Unit(s) Under Test: Manifest_mgr
-- 
-- Script Features
TEST.SCRIPT_FEATURE:C_DIRECT_ARRAY_INDEXING
TEST.SCRIPT_FEATURE:CPP_CLASS_OBJECT_REVISION
TEST.SCRIPT_FEATURE:MULTIPLE_UUT_SUPPORT
TEST.SCRIPT_FEATURE:MIXED_CASE_NAMES
TEST.SCRIPT_FEATURE:STATIC_HEADER_FUNCS_IN_UUTS
--

-- Unit: Manifest_mgr

-- Subprogram: gfMM_CheckOperationalState

-- Test Case: Iin_operational_state_TRUE_gfMM_CheckOperationalState
TEST.UNIT:Manifest_mgr
TEST.SUBPROGRAM:gfMM_CheckOperationalState
TEST.NEW
TEST.NAME:Iin_operational_state_TRUE_gfMM_CheckOperationalState
TEST.VALUE:Manifest_mgr.<<GLOBAL>>.gVehicle_Manifest.can.uwNumbDrivers:1
TEST.VALUE:Manifest_mgr.<<GLOBAL>>.gVehicle_Manifest.can.driver[0].data.fHB_Monitor:1
TEST.VALUE:Manifest_mgr.<<GLOBAL>>.gVehicle_Manifest.can.driver[0].data.fConfigured:1
TEST.VALUE:Manifest_mgr.<<GLOBAL>>.gVehicle_Manifest.can.driver[0].data.fReadModule:1
TEST.VALUE:Manifest_mgr.<<GLOBAL>>.gVehicle_Manifest.can.driver[0].data.uwErrorNumber_startup:1
TEST.VALUE:Manifest_mgr.gfMM_CheckOperationalState.fLogEvent:1
TEST.VALUE:uut_prototype_stubs.gfCO_CheckNodeOperational.return:1
TEST.VALUE:uut_prototype_stubs.gfFI_CheckFeatureDependency_EventId.return:1
TEST.EXPECTED:Manifest_mgr.<<GLOBAL>>.gVehicle_Manifest.can.uwNumbDrivers:1
TEST.END

-- Test Case: Not_in_operational_state_TRUE_==_fLogEvent__gfMM_CheckOperationalState
TEST.UNIT:Manifest_mgr
TEST.SUBPROGRAM:gfMM_CheckOperationalState
TEST.NEW
TEST.NAME:Not_in_operational_state_TRUE_==_fLogEvent__gfMM_CheckOperationalState
TEST.VALUE:Manifest_mgr.<<GLOBAL>>.gVehicle_Manifest.can.uwNumbDrivers:1
TEST.VALUE:Manifest_mgr.<<GLOBAL>>.gVehicle_Manifest.can.driver[0].data.fHB_Monitor:1
TEST.VALUE:Manifest_mgr.<<GLOBAL>>.gVehicle_Manifest.can.driver[0].data.fConfigured:1
TEST.VALUE:Manifest_mgr.<<GLOBAL>>.gVehicle_Manifest.can.driver[0].data.fReadModule:1
TEST.VALUE:Manifest_mgr.<<GLOBAL>>.gVehicle_Manifest.can.driver[0].data.uwErrorNumber_startup:1
TEST.VALUE:Manifest_mgr.gfMM_CheckOperationalState.fLogEvent:1
TEST.VALUE:uut_prototype_stubs.gfFI_CheckFeatureDependency_EventId.return:1
TEST.EXPECTED:Manifest_mgr.<<GLOBAL>>.gVehicle_Manifest.can.uwNumbDrivers:1
TEST.END

-- Subprogram: gfMM_CheckRestartPending

-- Test Case: gfMM_CheckRestartPending.001
TEST.UNIT:Manifest_mgr
TEST.SUBPROGRAM:gfMM_CheckRestartPending
TEST.NEW
TEST.NAME:gfMM_CheckRestartPending.001
TEST.VALUE:Manifest_mgr.<<GLOBAL>>.gVehicle_Manifest.can.uwNumbDrivers:1
TEST.END

-- Subprogram: gvMM_ConfigureModules

-- Test Case: wNumbDrivers_=_=_1
TEST.UNIT:Manifest_mgr
TEST.SUBPROGRAM:gvMM_ConfigureModules
TEST.NEW
TEST.NAME:wNumbDrivers_=_=_1
TEST.VALUE:Manifest_mgr.<<GLOBAL>>.gVehicle_Manifest.can.uwNumbDrivers:1
TEST.END

-- Subprogram: gvMM_Initialize

-- Test Case: 0u_!=_pConfig->ulErrorNumber_init
TEST.UNIT:Manifest_mgr
TEST.SUBPROGRAM:gvMM_Initialize
TEST.NEW
TEST.NAME:0u_!=_pConfig->ulErrorNumber_init
TEST.END

-- Test Case: gfMI_Initialize_pConfig->eDriverEnum
TEST.UNIT:Manifest_mgr
TEST.SUBPROGRAM:gvMM_Initialize
TEST.NEW
TEST.NAME:gfMI_Initialize_pConfig->eDriverEnum
TEST.VALUE:uut_prototype_stubs.gfMI_Initialize.eEnum:CODRVSEL_AMC_001
TEST.VALUE:uut_prototype_stubs.gfMI_Initialize.return:1
TEST.EXPECTED:uut_prototype_stubs.gfMI_Initialize.eEnum:CODRVSEL_AMC_001
TEST.END

-- Subprogram: gvMM_PrepareOnetimeConfig

-- Test Case: FALSE_==_pDriver->data.fOnetimeConfigReset_
TEST.UNIT:Manifest_mgr
TEST.SUBPROGRAM:gvMM_PrepareOnetimeConfig
TEST.NEW
TEST.NAME:FALSE_==_pDriver->data.fOnetimeConfigReset_
TEST.VALUE:Manifest_mgr.<<GLOBAL>>.gVehicle_Manifest.can.uwNumbDrivers:1
TEST.VALUE:Manifest_mgr.<<GLOBAL>>.gVehicle_Manifest.can.driver[0].data.fOneTimeConfig:1
TEST.VALUE:Manifest_mgr.<<GLOBAL>>.gVehicle_Manifest.can.driver[0].uwOneTimeConfigPassValue:1234
TEST.EXPECTED:Manifest_mgr.<<GLOBAL>>.gVehicle_Manifest.can.uwNumbDrivers:1
TEST.END

-- Test Case: TRUE_==_pDriver->data.fOnetimeConfigReset
TEST.UNIT:Manifest_mgr
TEST.SUBPROGRAM:gvMM_PrepareOnetimeConfig
TEST.NEW
TEST.NAME:TRUE_==_pDriver->data.fOnetimeConfigReset
TEST.VALUE:Manifest_mgr.<<GLOBAL>>.gVehicle_Manifest.can.uwNumbDrivers:1
TEST.VALUE:Manifest_mgr.<<GLOBAL>>.gVehicle_Manifest.can.driver[0].data.fOneTimeConfig:1
TEST.VALUE:Manifest_mgr.<<GLOBAL>>.gVehicle_Manifest.can.driver[0].data.fOnetimeConfigReset:1
TEST.VALUE:Manifest_mgr.<<GLOBAL>>.gVehicle_Manifest.can.driver[0].uwOneTimeConfigPassValue:1234
TEST.EXPECTED:Manifest_mgr.<<GLOBAL>>.gVehicle_Manifest.can.uwNumbDrivers:1
TEST.END

-- Subprogram: gvMM_ReadFram

-- Test Case: uwIdx_<_gVehicle_Manifest.can.uwNumbDrivers;
TEST.UNIT:Manifest_mgr
TEST.SUBPROGRAM:gvMM_ReadFram
TEST.NEW
TEST.NAME:uwIdx_<_gVehicle_Manifest.can.uwNumbDrivers;
TEST.VALUE:Manifest_mgr.<<GLOBAL>>.gVehicle_Manifest.can.uwNumbDrivers:4
TEST.EXPECTED:Manifest_mgr.<<GLOBAL>>.gVehicle_Manifest.can.uwNumbDrivers:4
TEST.END

-- Subprogram: gvMM_ReadModuleInfo

-- Test Case: uwIdx_<_gVehicle_Manifest.can.uwNumbDrivers
TEST.UNIT:Manifest_mgr
TEST.SUBPROGRAM:gvMM_ReadModuleInfo
TEST.NEW
TEST.NAME:uwIdx_<_gVehicle_Manifest.can.uwNumbDrivers
TEST.VALUE:Manifest_mgr.<<GLOBAL>>.gVehicle_Manifest.can.uwNumbDrivers:1
TEST.END

-- Subprogram: gvMM_RestartModules

-- Test Case: gvMM_RestartModules.001
TEST.UNIT:Manifest_mgr
TEST.SUBPROGRAM:gvMM_RestartModules
TEST.NEW
TEST.NAME:gvMM_RestartModules.001
TEST.VALUE:Manifest_mgr.<<GLOBAL>>.gVehicle_Manifest.can.uwNumbDrivers:1
TEST.END

-- Subprogram: gvMM_StartModules

-- Test Case: FALSE_==_pDriver->data.fHB_Monitor_
TEST.UNIT:Manifest_mgr
TEST.SUBPROGRAM:gvMM_StartModules
TEST.NEW
TEST.NAME:FALSE_==_pDriver->data.fHB_Monitor_
TEST.VALUE:Manifest_mgr.<<GLOBAL>>.gVehicle_Manifest.can.uwNumbDrivers:1
TEST.VALUE:Manifest_mgr.<<GLOBAL>>.gVehicle_Manifest.can.driver[0].data.fConfigured:1
TEST.VALUE:Manifest_mgr.<<GLOBAL>>.gVehicle_Manifest.can.driver[0].data.fReadModule:1
TEST.END

-- Test Case: TRUE_==_pDriver->data.fHB_Monitor
TEST.UNIT:Manifest_mgr
TEST.SUBPROGRAM:gvMM_StartModules
TEST.NEW
TEST.NAME:TRUE_==_pDriver->data.fHB_Monitor
TEST.VALUE:Manifest_mgr.<<GLOBAL>>.gVehicle_Manifest.can.uwNumbDrivers:1
TEST.VALUE:Manifest_mgr.<<GLOBAL>>.gVehicle_Manifest.can.driver[0].data.fHB_Monitor:1
TEST.VALUE:Manifest_mgr.<<GLOBAL>>.gVehicle_Manifest.can.driver[0].data.fConfigured:1
TEST.VALUE:Manifest_mgr.<<GLOBAL>>.gVehicle_Manifest.can.driver[0].data.fReadModule:1
TEST.END

-- Subprogram: gvMM_StopGuarding

-- Test Case: gvMM_StopGuarding.001
TEST.UNIT:Manifest_mgr
TEST.SUBPROGRAM:gvMM_StopGuarding
TEST.NEW
TEST.NAME:gvMM_StopGuarding.001
TEST.VALUE:Manifest_mgr.<<GLOBAL>>.gVehicle_Manifest.can.uwNumbDrivers:1
TEST.VALUE:Manifest_mgr.<<GLOBAL>>.gVehicle_Manifest.can.driver[0].data.fConfigured:1
TEST.VALUE:Manifest_mgr.<<GLOBAL>>.gVehicle_Manifest.can.driver[0].data.fReadModule:1
TEST.EXPECTED:Manifest_mgr.<<GLOBAL>>.gVehicle_Manifest.can.uwNumbDrivers:1
TEST.END

-- Subprogram: gvMM_WriteFram

-- Test Case: uwIdx_<_gVehicle_Manifest.can.uwNumbDrivers;
TEST.UNIT:Manifest_mgr
TEST.SUBPROGRAM:gvMM_WriteFram
TEST.NEW
TEST.NAME:uwIdx_<_gVehicle_Manifest.can.uwNumbDrivers;
TEST.VALUE:Manifest_mgr.<<GLOBAL>>.gVehicle_Manifest.can.uwNumbDrivers:1
TEST.VALUE:Manifest_mgr.<<GLOBAL>>.gVehicle_Manifest.can.driver[0].data.fOnetimeConfigReset:1
TEST.END

-- Subprogram: read_tbl

-- Test Case: read_tbl.001
TEST.UNIT:Manifest_mgr
TEST.SUBPROGRAM:read_tbl
TEST.NEW
TEST.NAME:read_tbl.001
TEST.END

-- Subprogram: vMM_GetModuleManifestDefaults

-- Test Case: vMM_GetModuleManifestDefaults.001
TEST.UNIT:Manifest_mgr
TEST.SUBPROGRAM:vMM_GetModuleManifestDefaults
TEST.NEW
TEST.NAME:vMM_GetModuleManifestDefaults.001
TEST.END

-- Subprogram: vMM_GetProcessorInfo

-- Test Case: vMM_GetProcessorInfo.001
TEST.UNIT:Manifest_mgr
TEST.SUBPROGRAM:vMM_GetProcessorInfo
TEST.NEW
TEST.NAME:vMM_GetProcessorInfo.001
TEST.VALUE:Manifest_mgr.vMM_GetProcessorInfo.pManifest:<<malloc 2>>
TEST.VALUE:uut_prototype_stubs.gpAppInfo_GetPointer.return:<<malloc 1>>
TEST.VALUE:uut_prototype_stubs.gpAppInfo_GetPointer.return[0].szVehicleSfwrPN:<<malloc 2>>
TEST.VALUE:uut_prototype_stubs.gpAppInfo_GetPointer.return[0].szVehicleSfwrPN:"0"
TEST.VALUE:uut_prototype_stubs.gpAppInfo_GetPointer.return[0].szAppSfwrPN:<<malloc 5>>
TEST.VALUE:uut_prototype_stubs.gpAppInfo_GetPointer.return[0].szAppSfwrPN:"1234"
TEST.VALUE:uut_prototype_stubs.gpGetBootParameterPointer.return:<<malloc 1>>
TEST.END
