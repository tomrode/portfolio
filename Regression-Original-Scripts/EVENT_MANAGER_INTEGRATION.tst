-- VectorCAST 6.4p (01/31/17)
-- Test Case Script
-- 
-- Environment    : EVENT_MANAGER_INTEGRATION
-- Unit(s) Under Test: EventManager NvMemory OS_Interface XcpUserFlash crc debug_support imm_interface immcomm_driver instrument io_thread param pd_data_test watchdog
-- 
-- Script Features
TEST.SCRIPT_FEATURE:C_DIRECT_ARRAY_INDEXING
TEST.SCRIPT_FEATURE:CPP_CLASS_OBJECT_REVISION
TEST.SCRIPT_FEATURE:MULTIPLE_UUT_SUPPORT
TEST.SCRIPT_FEATURE:MIXED_CASE_NAMES
TEST.SCRIPT_FEATURE:STATIC_HEADER_FUNCS_IN_UUTS
--

-- Subprogram: <<INIT>>

-- Test Case: <<INIT>>.001
TEST.SUBPROGRAM:<<INIT>>
TEST.NEW
TEST.NAME:<<INIT>>.001
TEST.COMPOUND_ONLY
TEST.STUB:EventManager.gulDEM_Initialize
TEST.STUB:OS_Interface.gulOS_ThreadCreate
TEST.STUB:OS_Interface.gulOS_ThreadSuspend
TEST.STUB:watchdog.gvWatchdog_RegisterThread
TEST.VALUE:OS_Interface.gulOS_ThreadCreate.pszThreadName:"THD_DEM_Service"
TEST.VALUE:OS_Interface.gulOS_ThreadCreate.pvEntryPoint:<<null>>
TEST.VALUE:OS_Interface.gulOS_ThreadCreate.ulEntryInput:0
TEST.VALUE:OS_Interface.gulOS_ThreadCreate.ulStackSize:1024
TEST.VALUE:OS_Interface.gulOS_ThreadCreate.ulPriority:5
TEST.VALUE:OS_Interface.gulOS_ThreadCreate.ulPreemptThreashold:5
TEST.VALUE:OS_Interface.gulOS_ThreadCreate.ulTimeSlice:0
TEST.VALUE:OS_Interface.gulOS_ThreadCreate.ulAutoStart:0
TEST.VALUE:OS_Interface.gulOS_ThreadCreate.return:0
TEST.EXPECTED:OS_Interface.gulOS_ThreadCreate.pszThreadName:"THD_DEM_Service"
TEST.EXPECTED:OS_Interface.gulOS_ThreadCreate.pvEntryPoint:<<null>>
TEST.EXPECTED:OS_Interface.gulOS_ThreadCreate.ulEntryInput:0
TEST.EXPECTED:OS_Interface.gulOS_ThreadCreate.ulStackSize:1024
TEST.EXPECTED:OS_Interface.gulOS_ThreadCreate.ulPriority:5
TEST.EXPECTED:OS_Interface.gulOS_ThreadCreate.ulPreemptThreashold:5
TEST.EXPECTED:OS_Interface.gulOS_ThreadCreate.ulTimeSlice:0
TEST.EXPECTED:OS_Interface.gulOS_ThreadCreate.ulAutoStart:0
TEST.STUB_VAL_USER_CODE:OS_Interface.gulOS_ThreadSuspend.pThread
static os_thread_t DEM_ServiceThread; 
<<OS_Interface.gulOS_ThreadSuspend.pThread>> = ( &DEM_ServiceThread  );
TEST.END_STUB_VAL_USER_CODE:
TEST.STUB_EXP_USER_CODE:OS_Interface.gulOS_ThreadSuspend.pThread
static os_thread_t DEM_ServiceThread; 
{{ <<OS_Interface.gulOS_ThreadSuspend.pThread>> == ( &DEM_ServiceThread ) }}
TEST.END_STUB_EXP_USER_CODE:
TEST.END

-- Unit: EventManager

-- Subprogram: eDEM_ReportChangeSet

-- Test Case: eDEM_ReportChangeSet.001
TEST.UNIT:EventManager
TEST.SUBPROGRAM:eDEM_ReportChangeSet
TEST.NEW
TEST.NAME:eDEM_ReportChangeSet.001
TEST.COMPOUND_ONLY
TEST.STUB:OS_Interface.gulOS_ThreadSleep
TEST.STUB:OS_Interface.gulOS_SemaphoreGet
TEST.STUB:OS_Interface.gulOS_GetTime
TEST.VALUE:EventManager.<<GLOBAL>>.gDEM_ReportStructure.uwNumberOfRecords:1
TEST.VALUE:EventManager.<<GLOBAL>>.gDEM_ReportStructure.uwSequenceNumberAcked:1
TEST.VALUE:OS_Interface.gulOS_ThreadSleep.return:1
TEST.EXPECTED:immcomm_driver.<<GLOBAL>>.fIMM_NetworkReady:0
TEST.EXPECTED:EventManager.<<GLOBAL>>.gDEM_ReportStructure.uwNumberOfRecords:1
TEST.EXPECTED:EventManager.<<GLOBAL>>.gDEM_ReportStructure.uwSequenceNumberAcked:1
TEST.EXPECTED:EventManager.<<GLOBAL>>.gDEM_ReportStructure.Record[0].uwEventReferenceID:0
TEST.EXPECTED:EventManager.<<GLOBAL>>.gDEM_ReportStructure.Record[0].ubState:0
TEST.EXPECTED:EventManager.<<GLOBAL>>.gDEM_ReportStructure.Record[0].ubJ1939Count:0
TEST.EXPECTED:OS_Interface.gulOS_ThreadSleep.ulTimerTicks:100
TEST.EXPECTED:imm_interface.<<GLOBAL>>.gDEM_ReportStructure.uwNumberOfRecords:1
TEST.EXPECTED:imm_interface.<<GLOBAL>>.gDEM_ReportStructure.uwSequenceNumberReported:1
TEST.EXPECTED:imm_interface.<<GLOBAL>>.gDEM_ReportStructure.uwSequenceNumberAcked:1
TEST.STUB_VAL_USER_CODE:OS_Interface.gulOS_SemaphoreGet.pSemaphore
static os_semaphore_t DEM_AckSemaphore; 
<<OS_Interface.gulOS_SemaphoreGet.pSemaphore>> = ( &DEM_AckSemaphore  );
TEST.END_STUB_VAL_USER_CODE:
TEST.STUB_EXP_USER_CODE:OS_Interface.gulOS_SemaphoreGet.pSemaphore
static os_semaphore_t DEM_AckSemaphore; 
{{ <<OS_Interface.gulOS_SemaphoreGet.pSemaphore>> == (&DEM_AckSemaphore  ) }}
TEST.END_STUB_EXP_USER_CODE:
TEST.END

-- Subprogram: fDEM_CheckReport

-- Test Case: fDEM_CheckReport.001
TEST.UNIT:EventManager
TEST.SUBPROGRAM:fDEM_CheckReport
TEST.NEW
TEST.NAME:fDEM_CheckReport.001
TEST.COMPOUND_ONLY
TEST.VALUE:EventManager.fDEM_CheckReport.ubStateLast:DEM_STATE_ACTIVE2
TEST.EXPECTED:EventManager.fDEM_CheckReport.ubStateCurrent:DEM_STATE_INACTIVE
TEST.EXPECTED:EventManager.fDEM_CheckReport.ubStateLast:DEM_STATE_ACTIVE2
TEST.EXPECTED:EventManager.fDEM_CheckReport.return:1
TEST.END

-- Subprogram: gpDEM_GetCurrentControlLimitsPointer

-- Test Case: gpDEM_GetCurrentControlLimitsPointer.001
TEST.UNIT:EventManager
TEST.SUBPROGRAM:gpDEM_GetCurrentControlLimitsPointer
TEST.NEW
TEST.NAME:gpDEM_GetCurrentControlLimitsPointer.001
TEST.COMPOUND_ONLY
TEST.EXPECTED_USER_CODE:EventManager.gpDEM_GetCurrentControlLimitsPointer.return
{{ <<EventManager.gpDEM_GetCurrentControlLimitsPointer.return>> == ( &dem_current_control_limits ) }}
TEST.END_EXPECTED_USER_CODE:
TEST.END

-- Subprogram: gpDEM_GetReportThreadPointer

-- Test Case: gpDEM_GetReportThreadPointer.001
TEST.UNIT:EventManager
TEST.SUBPROGRAM:gpDEM_GetReportThreadPointer
TEST.NEW
TEST.NAME:gpDEM_GetReportThreadPointer.001
TEST.COMPOUND_ONLY
TEST.EXPECTED_USER_CODE:EventManager.gpDEM_GetReportThreadPointer.return
{{ <<EventManager.gpDEM_GetReportThreadPointer.return>> == ( &DEM_ReportThread  ) }}
TEST.END_EXPECTED_USER_CODE:
TEST.END

-- Subprogram: gpDEM_GetServiceThreadPointer

-- Test Case: gpDEM_GetServiceThreadPointer.001
TEST.UNIT:EventManager
TEST.SUBPROGRAM:gpDEM_GetServiceThreadPointer
TEST.NEW
TEST.NAME:gpDEM_GetServiceThreadPointer.001
TEST.COMPOUND_ONLY
TEST.EXPECTED_USER_CODE:EventManager.gpDEM_GetServiceThreadPointer.return
{{ <<EventManager.gpDEM_GetServiceThreadPointer.return>> == ( &DEM_ServiceThread ) }}
TEST.END_EXPECTED_USER_CODE:
TEST.END

-- Subprogram: gulDEM_Initialize

-- Test Case: gulDEM_Initialize.001
TEST.UNIT:EventManager
TEST.SUBPROGRAM:gulDEM_Initialize
TEST.NEW
TEST.NAME:gulDEM_Initialize.001
TEST.COMPOUND_ONLY
TEST.STUB:OS_Interface.gulOS_ThreadCreate
TEST.EXPECTED:EventManager.gulDEM_Initialize.return:0
TEST.EXPECTED:OS_Interface.gulOS_ThreadCreate.ulEntryInput:0
TEST.EXPECTED:OS_Interface.gulOS_ThreadCreate.ulStackSize:1024
TEST.EXPECTED:OS_Interface.gulOS_ThreadCreate.ulPriority:5
TEST.EXPECTED:OS_Interface.gulOS_ThreadCreate.ulPreemptThreashold:5
TEST.EXPECTED:OS_Interface.gulOS_ThreadCreate.ulTimeSlice:0
TEST.EXPECTED:OS_Interface.gulOS_ThreadCreate.ulAutoStart:1
TEST.ATTRIBUTES:OS_Interface.gulOS_ThreadCreate.pThread[0].tx_thread_preempt_threshold:EXPECTED_BASE=8
TEST.END

-- Subprogram: gvDEM_EventReportAcknowlege_cb

-- Test Case: gvDEM_EventReportAcknowlege_cb.001
TEST.UNIT:EventManager
TEST.SUBPROGRAM:gvDEM_EventReportAcknowlege_cb
TEST.NEW
TEST.NAME:gvDEM_EventReportAcknowlege_cb.001
TEST.COMPOUND_ONLY
TEST.VALUE:EventManager.gvDEM_EventReportAcknowlege_cb.pszString:<<malloc 2>>
TEST.VALUE:EventManager.gvDEM_EventReportAcknowlege_cb.pszString:"1"
TEST.EXPECTED:EventManager.gvDEM_EventReportAcknowlege_cb.uwMessageID:0
TEST.EXPECTED:EventManager.gvDEM_EventReportAcknowlege_cb.pszString:"1"
TEST.END

-- Subprogram: gvDEM_LogUndervoltageEvents

-- Test Case: gvDEM_LogUndervoltageEvents.001
TEST.UNIT:EventManager
TEST.SUBPROGRAM:gvDEM_LogUndervoltageEvents
TEST.NEW
TEST.NAME:gvDEM_LogUndervoltageEvents.001
TEST.COMPOUND_ONLY
TEST.VALUE:EventManager.<<GLOBAL>>.ubUvIndex:1
TEST.EXPECTED:EventManager.<<GLOBAL>>.DEM_EventsLoggedAtUnderVoltage[0].uwEventNumber:0
TEST.EXPECTED:EventManager.<<GLOBAL>>.DEM_EventsLoggedAtUnderVoltage[0].ubEventStatus:0
TEST.END

-- Subprogram: gvDEM_ReportEventState

-- Test Case: Use_for_Log_Under_Voltage_Events
TEST.UNIT:EventManager
TEST.SUBPROGRAM:gvDEM_ReportEventState
TEST.NEW
TEST.NAME:Use_for_Log_Under_Voltage_Events
TEST.COMPOUND_ONLY
TEST.VALUE:EventManager.<<GLOBAL>>.ubDEM_EventStatesCurrent:<<malloc 23>>
TEST.VALUE:EventManager.<<GLOBAL>>.ubDEM_EventStatesCurrent:"DEMEV_NUMBER_OF_EVENTS"
TEST.VALUE:EventManager.<<GLOBAL>>.uwDEM_CurrentActiveEvent[0]:20
TEST.VALUE:EventManager.<<GLOBAL>>.ubDEM_EventStatesLast:<<malloc 23>>
TEST.VALUE:EventManager.<<GLOBAL>>.ubDEM_EventStatesLast:"DEMEV_NUMBER_OF_EVENTS"
TEST.VALUE:EventManager.gvDEM_ReportEventState.uwEventReferenceID:1072
TEST.VALUE:EventManager.gvDEM_ReportEventState.ubState:8
TEST.EXPECTED:EventManager.<<GLOBAL>>.ubDEM_EventStatesCurrent:"DEMEV_NUMBER_OF_EVENTS"
TEST.EXPECTED:EventManager.<<GLOBAL>>.uwDEM_CurrentActiveEvent[0]:20
TEST.EXPECTED:EventManager.<<GLOBAL>>.ubUvIndex:DEM_NUMBER_REPORT_RECORDS
TEST.EXPECTED:EventManager.<<GLOBAL>>.ubDEM_EventStatesLast:"DEMEV_NUMBER_OF_EVENTS"
TEST.EXPECTED:EventManager.gvDEM_ReportEventState.uwEventReferenceID:1072
TEST.EXPECTED:EventManager.gvDEM_ReportEventState.ubState:8
TEST.END

-- Test Case: gvDEM_ReportEventState.001
TEST.UNIT:EventManager
TEST.SUBPROGRAM:gvDEM_ReportEventState
TEST.NEW
TEST.NAME:gvDEM_ReportEventState.001
TEST.COMPOUND_ONLY
TEST.VALUE:<<OPTIONS>>.EVENT_LIMIT:500
TEST.VALUE:EventManager.<<GLOBAL>>.ubDEM_EventStatesCurrent:<<malloc 18>>
TEST.VALUE:EventManager.<<GLOBAL>>.ubDEM_EventStatesCurrent:"DEM_STATE_ACTIVE1"
TEST.VALUE:EventManager.<<GLOBAL>>.ubUvIndex:16
TEST.VALUE:EventManager.gvDEM_ReportEventState.uwEventReferenceID:1072
TEST.VALUE:EventManager.gvDEM_ReportEventState.ubState:8
TEST.EXPECTED:EventManager.<<GLOBAL>>.ubDEM_EventStatesCurrent:"DEM_STATE_ACTIVE1"
TEST.EXPECTED:EventManager.<<GLOBAL>>.uwDEM_CurrentActiveEvent[0]:0
TEST.EXPECTED:EventManager.<<GLOBAL>>.ubUvIndex:16
TEST.EXPECTED:EventManager.gvDEM_ReportEventState.uwEventReferenceID:1072
TEST.EXPECTED:EventManager.gvDEM_ReportEventState.ubState:8
TEST.END

-- Subprogram: gvDEM_SetParametersInvalid

-- Test Case: gvDEM_SetParametersInvalid.001
TEST.UNIT:EventManager
TEST.SUBPROGRAM:gvDEM_SetParametersInvalid
TEST.NEW
TEST.NAME:gvDEM_SetParametersInvalid.001
TEST.COMPOUND_ONLY
TEST.EXPECTED:EventManager.<<GLOBAL>>.gDEM_ReportStructure.uwNumberOfRecords:0
TEST.EXPECTED:EventManager.<<GLOBAL>>.gDEM_ReportStructure.uwSequenceNumberReported:1
TEST.EXPECTED:EventManager.<<GLOBAL>>.gDEM_ReportStructure.uwSequenceNumberAcked:1
TEST.EXPECTED:EventManager.<<GLOBAL>>.sfDEM_InvalidParams:1
TEST.EXPECTED:EventManager.<<GLOBAL>>.sfDEM_InvalidApp:1
TEST.END

-- Subprogram: vDEM_ActiveEventListUpdate

-- Test Case: vDEM_ActiveEventListUpdate.001
TEST.UNIT:EventManager
TEST.SUBPROGRAM:vDEM_ActiveEventListUpdate
TEST.NEW
TEST.NAME:vDEM_ActiveEventListUpdate.001
TEST.COMPOUND_ONLY
TEST.VALUE:EventManager.<<GLOBAL>>.ubDEM_EventStatesCurrent:<<malloc 25>>
TEST.VALUE:EventManager.<<GLOBAL>>.ubDEM_EventStatesCurrent:" DEM_DEFAULT_EVENT_INDEX"
TEST.EXPECTED:EventManager.<<GLOBAL>>.ubDEM_EventStatesCurrent:" DEM_DEFAULT_EVENT_INDEX"
TEST.EXPECTED:EventManager.<<GLOBAL>>.uwDEM_CurrentActiveEvent[0]:0
TEST.END

-- Subprogram: vDEM_ProcessEventStateChanges

-- Test Case: vDEM_ProcessEventStateChanges.001
TEST.UNIT:EventManager
TEST.SUBPROGRAM:vDEM_ProcessEventStateChanges
TEST.NEW
TEST.NAME:vDEM_ProcessEventStateChanges.001
TEST.COMPOUND_ONLY
TEST.STUB:immcomm_driver.gfIMMCOMM_NetworkReady
TEST.VALUE:immcomm_driver.gfIMMCOMM_NetworkReady.return:0
TEST.VALUE:EventManager.<<GLOBAL>>.gDEM_ReportStructure.uwNumberOfRecords:1
TEST.VALUE:EventManager.<<GLOBAL>>.gDEM_ReportStructure.uwSequenceNumberAcked:1
TEST.EXPECTED:EventManager.<<GLOBAL>>.gDEM_ReportStructure.uwNumberOfRecords:1
TEST.EXPECTED:EventManager.<<GLOBAL>>.gDEM_ReportStructure.uwSequenceNumberReported:1
TEST.EXPECTED:EventManager.<<GLOBAL>>.gDEM_ReportStructure.uwSequenceNumberAcked:1
TEST.END

-- Subprogram: vDEM_ReportThreadEntry

-- Test Case: vDEM_ReportThreadEntry.001
TEST.UNIT:EventManager
TEST.SUBPROGRAM:vDEM_ReportThreadEntry
TEST.NEW
TEST.NAME:vDEM_ReportThreadEntry.001
TEST.COMPOUND_ONLY
TEST.STUB:XcpUserFlash.gfXCP_EraseSector
TEST.STUB:XcpUserFlash.gvXCP_ReflashTrap
TEST.STUB:EventManager.gvDEM_SetApplicationInvalid
TEST.STUB:EventManager.gvDEM_SetParametersInvalid
TEST.STUB:OS_Interface.gulOS_ThreadSleep
TEST.STUB:OS_Interface.gulOS_ThreadSuspend
TEST.STUB:OS_Interface.gulOS_SemaphoreCreate
TEST.VALUE:XcpUserFlash.<<GLOBAL>>.fXCP_ReflashInitiated:1
TEST.VALUE:XcpUserFlash.<<GLOBAL>>.fXCP_ParametersReflashed:1
TEST.VALUE:XcpUserFlash.gfXCP_EraseSector.return:1
TEST.VALUE:EventManager.<<GLOBAL>>.sfDEM_InvalidParams:1
TEST.VALUE:EventManager.<<GLOBAL>>.sfDEM_InvalidApp:1
TEST.VALUE:EventManager.vDEM_ReportThreadEntry.ulInput:0
TEST.VALUE:OS_Interface.gulOS_ThreadSleep.return:1
TEST.EXPECTED:XcpUserFlash.<<GLOBAL>>.fXCP_ReflashInitiated:1
TEST.EXPECTED:XcpUserFlash.<<GLOBAL>>.fXCP_ParametersReflashed:1
TEST.EXPECTED:EventManager.<<GLOBAL>>.gDEM_ReportStructure.uwNumberOfRecords:0
TEST.EXPECTED:EventManager.<<GLOBAL>>.gDEM_ReportStructure.uwSequenceNumberReported:0
TEST.EXPECTED:EventManager.<<GLOBAL>>.gDEM_ReportStructure.uwSequenceNumberAcked:0
TEST.EXPECTED:EventManager.<<GLOBAL>>.sfDEM_InvalidParams:1
TEST.EXPECTED:EventManager.<<GLOBAL>>.sfDEM_InvalidApp:1
TEST.EXPECTED:OS_Interface.gulOS_ThreadSleep.ulTimerTicks:100
TEST.EXPECTED:OS_Interface.gulOS_SemaphoreCreate.pSemaphore[0].tx_semaphore_id:0
TEST.EXPECTED:OS_Interface.gulOS_SemaphoreCreate.pSemaphore[0].tx_semaphore_count:0
TEST.EXPECTED:OS_Interface.gulOS_SemaphoreCreate.pSemaphore[0].tx_semaphore_suspended_count:0
TEST.EXPECTED:OS_Interface.gulOS_SemaphoreCreate.pszSemaphoreName:"SEMA_DEM_Ack"
TEST.EXPECTED:OS_Interface.gulOS_SemaphoreCreate.ulInitialCount:0
TEST.EXPECTED:imm_interface.<<GLOBAL>>.gDEM_ReportStructure.uwNumberOfRecords:0
TEST.EXPECTED:imm_interface.<<GLOBAL>>.gDEM_ReportStructure.uwSequenceNumberReported:0
TEST.EXPECTED:imm_interface.<<GLOBAL>>.gDEM_ReportStructure.uwSequenceNumberAcked:0
TEST.EXPECTED:imm_interface.<<GLOBAL>>.gDEM_ReportStructure.Record[0].uwEventReferenceID:0
TEST.EXPECTED:imm_interface.<<GLOBAL>>.gDEM_ReportStructure.Record[0].ubState:0
TEST.EXPECTED:imm_interface.<<GLOBAL>>.gDEM_ReportStructure.Record[0].ubJ1939Count:0
TEST.STUB_EXP_USER_CODE:OS_Interface.gulOS_ThreadSuspend.pThread
static os_thread_t DEM_ServiceThread; 
{{ <<OS_Interface.gulOS_ThreadSuspend.pThread>> == ( &DEM_ServiceThread ) }}
TEST.END_STUB_EXP_USER_CODE:
TEST.END

-- Subprogram: vDEM_ServiceThreadEntry

-- Test Case: vDEM_ServiceThreadEntry.001
TEST.UNIT:EventManager
TEST.SUBPROGRAM:vDEM_ServiceThreadEntry
TEST.NEW
TEST.NAME:vDEM_ServiceThreadEntry.001
TEST.COMPOUND_ONLY
TEST.STUB:OS_Interface.gulOS_ThreadSuspend
TEST.STUB:instrument.gvINST_Thread_Init
TEST.EXPECTED:OS_Interface.gulOS_ThreadSuspend.pThread[0].tx_thread_id:0
TEST.EXPECTED:OS_Interface.gulOS_ThreadSuspend.pThread[0].tx_thread_run_count:0
TEST.EXPECTED:OS_Interface.gulOS_ThreadSuspend.pThread[0].tx_thread_stack_size:0
TEST.EXPECTED:OS_Interface.gulOS_ThreadSuspend.pThread[0].tx_thread_time_slice:0
TEST.EXPECTED:OS_Interface.gulOS_ThreadSuspend.pThread[0].tx_thread_new_time_slice:0
TEST.EXPECTED:instrument.gvINST_Thread_Init.pThreadData[0].ulRateExpected_us:0
TEST.EXPECTED:instrument.gvINST_Thread_Init.pThreadData[0].ExeRate.ulMinimum:0
TEST.EXPECTED:instrument.gvINST_Thread_Init.pThreadData[0].ExeRate.ulMaximum:0
TEST.EXPECTED:instrument.gvINST_Thread_Init.pThreadData[0].ExeRate.ulAverage:0
TEST.EXPECTED:instrument.gvINST_Thread_Init.pThreadData[0].ExeRate.ulCurrent:0
TEST.EXPECTED:instrument.gvINST_Thread_Init.ulExpectedRate_us:10000
TEST.END

-- Subprogram: vDEM_TimerEntry

-- Test Case: vDEM_TimerEntry.001
TEST.UNIT:EventManager
TEST.SUBPROGRAM:vDEM_TimerEntry
TEST.NEW
TEST.NAME:vDEM_TimerEntry.001
TEST.COMPOUND_ONLY
TEST.STUB:OS_Interface.gulOS_ThreadResume
TEST.VALUE:EventManager.vDEM_TimerEntry.ulInput:10000
TEST.EXPECTED:EventManager.vDEM_TimerEntry.ulInput:10000
TEST.EXPECTED:OS_Interface.gulOS_ThreadResume.pThread[0].tx_thread_id:0
TEST.EXPECTED:OS_Interface.gulOS_ThreadResume.pThread[0].tx_thread_run_count:0
TEST.EXPECTED:OS_Interface.gulOS_ThreadResume.pThread[0].tx_thread_stack_size:0
TEST.EXPECTED:OS_Interface.gulOS_ThreadResume.pThread[0].tx_thread_time_slice:0
TEST.EXPECTED:OS_Interface.gulOS_ThreadResume.pThread[0].tx_thread_new_time_slice:0
TEST.EXPECTED:OS_Interface.gulOS_ThreadResume.pThread[0].tx_thread_priority:0
TEST.EXPECTED:OS_Interface.gulOS_ThreadResume.pThread[0].tx_thread_state:0
TEST.EXPECTED:OS_Interface.gulOS_ThreadResume.pThread[0].tx_thread_delayed_suspend:0
TEST.EXPECTED:OS_Interface.gulOS_ThreadResume.pThread[0].tx_thread_suspending:0
TEST.EXPECTED:OS_Interface.gulOS_ThreadResume.pThread[0].tx_thread_preempt_threshold:0
TEST.EXPECTED:OS_Interface.gulOS_ThreadResume.pThread[0].tx_thread_entry_parameter:0
TEST.EXPECTED:OS_Interface.gulOS_ThreadResume.pThread[0].tx_thread_timer.tx_timer_internal_remaining_ticks:0
TEST.EXPECTED:OS_Interface.gulOS_ThreadResume.pThread[0].tx_thread_timer.tx_timer_internal_re_initialize_ticks:0
TEST.EXPECTED:OS_Interface.gulOS_ThreadResume.pThread[0].tx_thread_timer.tx_timer_internal_timeout_param:0
TEST.EXPECTED:OS_Interface.gulOS_ThreadResume.pThread[0].tx_thread_suspend_info:0
TEST.EXPECTED:OS_Interface.gulOS_ThreadResume.pThread[0].tx_thread_suspend_option:0
TEST.EXPECTED:OS_Interface.gulOS_ThreadResume.pThread[0].tx_thread_suspend_status:0
TEST.EXPECTED:OS_Interface.gulOS_ThreadResume.pThread[0].tx_thread_execution_time_total:0
TEST.EXPECTED:OS_Interface.gulOS_ThreadResume.pThread[0].tx_thread_execution_time_last_start:0
TEST.ATTRIBUTES:OS_Interface.gulOS_ThreadResume.return:DISPLAY_STATE=DISPLAY
TEST.END

-- Subprogram: vDEM_UpdateControlStructure

-- Test Case: vDEM_UpdateControlStructure.001
TEST.UNIT:EventManager
TEST.SUBPROGRAM:vDEM_UpdateControlStructure
TEST.NEW
TEST.NAME:vDEM_UpdateControlStructure.001
TEST.COMPOUND_ONLY
TEST.VALUE:EventManager.<<GLOBAL>>.ubDEM_EventStatesCurrent:<<malloc 23>>
TEST.VALUE:EventManager.<<GLOBAL>>.ubDEM_EventStatesCurrent:"DEMEV_NUMBER_OF_EVENTS"
TEST.EXPECTED:EventManager.<<GLOBAL>>.ubDEM_EventStatesCurrent:"DEMEV_NUMBER_OF_EVENTS"
TEST.EXPECTED:EventManager.<<GLOBAL>>.gfDEM_SendEvents:0
TEST.EXPECTED:EventManager.<<GLOBAL>>.ubWatchdogID:0
TEST.EXPECTED:EventManager.<<GLOBAL>>.sINST_Thread_DEMservice.ullTimerLast:0
TEST.EXPECTED:EventManager.<<GLOBAL>>.sINST_Thread_DEMservice.ulRateExpected_us:0
TEST.EXPECTED:EventManager.<<GLOBAL>>.sINST_Thread_DEMservice.ExeRate.ulMinimum:0
TEST.EXPECTED:EventManager.<<GLOBAL>>.sINST_Thread_DEMservice.ExeRate.ulMaximum:0
TEST.EXPECTED:EventManager.<<GLOBAL>>.sINST_Thread_DEMservice.ExeRate.ulAverage:0
TEST.EXPECTED:EventManager.<<GLOBAL>>.sINST_Thread_DEMservice.ExeRate.ulCurrent:0
TEST.EXPECTED:EventManager.<<GLOBAL>>.sINST_Thread_DEMservice.ulRateHistogram[0]:0
TEST.ATTRIBUTES:EventManager.<<GLOBAL>>.sfDEM_InvalidParams:DISPLAY_STATE=DISPLAY
TEST.END

-- Unit: OS_Interface

-- Subprogram: gulOS_SemaphoreCreate

-- Test Case: gulOS_SemaphoreCreate.001
TEST.UNIT:OS_Interface
TEST.SUBPROGRAM:gulOS_SemaphoreCreate
TEST.NEW
TEST.NAME:gulOS_SemaphoreCreate.001
TEST.COMPOUND_ONLY
TEST.EXPECTED_USER_CODE:OS_Interface.gulOS_SemaphoreCreate.pSemaphore
static os_semaphore_t DEM_AckSemaphore; 
{{ <<OS_Interface.gulOS_SemaphoreCreate.pSemaphore>> == ( &DEM_AckSemaphore ) }}
TEST.END_EXPECTED_USER_CODE:
TEST.END

-- Subprogram: gulOS_SemaphoreGet

-- Test Case: gulOS_SemaphoreGet.001
TEST.UNIT:OS_Interface
TEST.SUBPROGRAM:gulOS_SemaphoreGet
TEST.NEW
TEST.NAME:gulOS_SemaphoreGet.001
TEST.COMPOUND_ONLY
TEST.EXPECTED_USER_CODE:OS_Interface.gulOS_SemaphoreGet.pSemaphore
static os_semaphore_t DEM_AckSemaphore; 
{{ <<OS_Interface.gulOS_SemaphoreGet.pSemaphore>> == ( &DEM_AckSemaphore) }}
TEST.END_EXPECTED_USER_CODE:
TEST.END

-- Subprogram: gulOS_ThreadCreate

-- Test Case: gulOS_ThreadCreate.001
TEST.UNIT:OS_Interface
TEST.SUBPROGRAM:gulOS_ThreadCreate
TEST.NEW
TEST.NAME:gulOS_ThreadCreate.001
TEST.COMPOUND_ONLY
TEST.EXPECTED:OS_Interface.gulOS_ThreadCreate.pszThreadName:"THD_DEM_Service"
TEST.EXPECTED:OS_Interface.gulOS_ThreadCreate.ulStackSize:1024
TEST.EXPECTED:OS_Interface.gulOS_ThreadCreate.ulPriority:5
TEST.EXPECTED:OS_Interface.gulOS_ThreadCreate.ulPreemptThreashold:5
TEST.EXPECTED:OS_Interface.gulOS_ThreadCreate.ulTimeSlice:0
TEST.EXPECTED:OS_Interface.gulOS_ThreadCreate.ulAutoStart:1
TEST.EXPECTED:OS_Interface.gulOS_ThreadCreate.return:0
TEST.EXPECTED_USER_CODE:OS_Interface.gulOS_ThreadCreate.pThread
static os_thread_t DEM_ReportThread; 
{{ <<OS_Interface.gulOS_ThreadCreate.pThread>> == ( &DEM_ReportThread ) }}
TEST.END_EXPECTED_USER_CODE:
TEST.END

-- Subprogram: gulOS_ThreadResume

-- Test Case: gulOS_ThreadResume.001
TEST.UNIT:OS_Interface
TEST.SUBPROGRAM:gulOS_ThreadResume
TEST.NEW
TEST.NAME:gulOS_ThreadResume.001
TEST.COMPOUND_ONLY
TEST.EXPECTED:OS_Interface.gulOS_ThreadResume.return:0
TEST.VALUE_USER_CODE:OS_Interface.gulOS_ThreadResume.pThread
<<OS_Interface.gulOS_ThreadResume.pThread>> = ( 10000 );
TEST.END_VALUE_USER_CODE:
TEST.EXPECTED_USER_CODE:OS_Interface.gulOS_ThreadResume.pThread
{{ <<OS_Interface.gulOS_ThreadResume.pThread>> == ( 10000 ) }}
TEST.END_EXPECTED_USER_CODE:
TEST.END

-- Subprogram: gulOS_ThreadSleep

-- Test Case: gulOS_ThreadSleep.001
TEST.UNIT:OS_Interface
TEST.SUBPROGRAM:gulOS_ThreadSleep
TEST.NEW
TEST.NAME:gulOS_ThreadSleep.001
TEST.COMPOUND_ONLY
TEST.VALUE:OS_Interface.gulOS_ThreadSleep.return:0
TEST.EXPECTED:OS_Interface.gulOS_ThreadSleep.ulTimerTicks:0
TEST.EXPECTED:OS_Interface.gulOS_ThreadSleep.return:0
TEST.END

-- Subprogram: gulOS_ThreadSuspend

-- Test Case: gulOS_ThreadSuspend.001
TEST.UNIT:OS_Interface
TEST.SUBPROGRAM:gulOS_ThreadSuspend
TEST.NEW
TEST.NAME:gulOS_ThreadSuspend.001
TEST.COMPOUND_ONLY
TEST.END

-- Subprogram: gulOS_TimerActivate

-- Test Case: gulOS_TimerActivate.001
TEST.UNIT:OS_Interface
TEST.SUBPROGRAM:gulOS_TimerActivate
TEST.NEW
TEST.NAME:gulOS_TimerActivate.001
TEST.COMPOUND_ONLY
TEST.VALUE:EventManager.<<GLOBAL>>.ubDEM_EventStatesCurrent:<<malloc 23>>
TEST.VALUE:EventManager.<<GLOBAL>>.ubDEM_EventStatesCurrent:"DEMEV_NUMBER_OF_EVENTS"
TEST.VALUE:EventManager.<<GLOBAL>>.uwDEM_CurrentActiveEvent[0]:20
TEST.VALUE:EventManager.<<GLOBAL>>.ubDEM_EventStatesLast:<<malloc 23>>
TEST.VALUE:EventManager.<<GLOBAL>>.ubDEM_EventStatesLast:"DEMEV_NUMBER_OF_EVENTS"
TEST.EXPECTED:EventManager.<<GLOBAL>>.ubDEM_EventStatesCurrent:"DEMEV_NUMBER_OF_EVENTS"
TEST.EXPECTED:EventManager.<<GLOBAL>>.uwDEM_CurrentActiveEvent[0]:20
TEST.EXPECTED:EventManager.<<GLOBAL>>.ubUvIndex:DEM_NUMBER_REPORT_RECORDS
TEST.EXPECTED:EventManager.<<GLOBAL>>.ubDEM_EventStatesLast:"DEMEV_NUMBER_OF_EVENTS"
TEST.END

-- Subprogram: tx_application_define

-- Test Case: tx_application_define.001
TEST.UNIT:OS_Interface
TEST.SUBPROGRAM:tx_application_define
TEST.NEW
TEST.NAME:tx_application_define.001
TEST.COMPOUND_ONLY
TEST.STUB:OS_Interface.gvOS_StartApplication
TEST.STUB:OS_Interface.gulOS_ThreadSleep
TEST.STUB:OS_Interface.gulOS_ThreadSuspend
TEST.VALUE:OS_Interface.gulOS_ThreadSuspend.pThread[0].tx_thread_suspending:1000
TEST.EXPECTED:OS_Interface.gulOS_ThreadSuspend.pThread[0].tx_thread_id:12
TEST.EXPECTED:OS_Interface.gulOS_ThreadSuspend.pThread[0].tx_thread_suspending:1000
TEST.END

-- Unit: XcpUserFlash

-- Subprogram: gvXCP_ReflashTrap

-- Test Case: gvXCP_ReflashTrap.001
TEST.UNIT:XcpUserFlash
TEST.SUBPROGRAM:gvXCP_ReflashTrap
TEST.NEW
TEST.NAME:gvXCP_ReflashTrap.001
TEST.COMPOUND_ONLY
TEST.STUB:XcpUserFlash.gvXCP_ProgramComplete
TEST.END

-- Unit: immcomm_driver

-- Subprogram: gfIMMCOMM_NetworkReady

-- Test Case: gfIMMCOMM_NetworkReady.001
TEST.UNIT:immcomm_driver
TEST.SUBPROGRAM:gfIMMCOMM_NetworkReady
TEST.NEW
TEST.NAME:gfIMMCOMM_NetworkReady.001
TEST.COMPOUND_ONLY
TEST.VALUE:immcomm_driver.<<GLOBAL>>.fIMM_NetworkReady:1
TEST.EXPECTED:immcomm_driver.<<GLOBAL>>.fIMM_NetworkReady:1
TEST.EXPECTED:immcomm_driver.gfIMMCOMM_NetworkReady.return:0
TEST.END

-- Subprogram: gulIMMCOMM_DriverInitialize

-- Test Case: gulIMMCOMM_DriverInitialize.001
TEST.UNIT:immcomm_driver
TEST.SUBPROGRAM:gulIMMCOMM_DriverInitialize
TEST.NEW
TEST.NAME:gulIMMCOMM_DriverInitialize.001
TEST.COMPOUND_ONLY
TEST.END

-- Subprogram: gulIMMCOMM_SendMessage

-- Test Case: gulIMMCOMM_SendMessage.001
TEST.UNIT:immcomm_driver
TEST.SUBPROGRAM:gulIMMCOMM_SendMessage
TEST.NEW
TEST.NAME:gulIMMCOMM_SendMessage.001
TEST.COMPOUND_ONLY
TEST.EXPECTED:immcomm_driver.<<GLOBAL>>.fIMM_NetworkReady:0
TEST.EXPECTED:immcomm_driver.<<GLOBAL>>.immcomm_driver.Data.fInitialized:1
TEST.EXPECTED:immcomm_driver.<<GLOBAL>>.immcomm_driver.Data.uwNumberObjects:334
TEST.EXPECTED:immcomm_driver.gulIMMCOMM_SendMessage.eSocketType:IP_TCP_SOCKET
TEST.EXPECTED:immcomm_driver.gulIMMCOMM_SendMessage.return:0
TEST.ATTRIBUTES:EventManager.<<GLOBAL>>.gDEM_ReportStructure.Record.Record[0].ubJ1939Count:DISPLAY_STATE=DISPLAY
TEST.END

-- Subprogram: vIMMCOMM_RxThreadEntry

-- Test Case: vIMMCOMM_RxThreadEntry.001
TEST.UNIT:immcomm_driver
TEST.SUBPROGRAM:vIMMCOMM_RxThreadEntry
TEST.NEW
TEST.NAME:vIMMCOMM_RxThreadEntry.001
TEST.COMPOUND_ONLY
TEST.END

-- Subprogram: vIMMCOMM_TxThreadEntry

-- Test Case: vIMMCOMM_TxThreadEntry.001
TEST.UNIT:immcomm_driver
TEST.SUBPROGRAM:vIMMCOMM_TxThreadEntry
TEST.NEW
TEST.NAME:vIMMCOMM_TxThreadEntry.001
TEST.COMPOUND_ONLY
TEST.END

-- Unit: instrument

-- Subprogram: gvINST_Thread_Init

-- Test Case: gvINST_Thread_Init.001
TEST.UNIT:instrument
TEST.SUBPROGRAM:gvINST_Thread_Init
TEST.NEW
TEST.NAME:gvINST_Thread_Init.001
TEST.COMPOUND_ONLY
TEST.END

-- Unit: io_thread

-- Subprogram: gfIO_UndervoltageISR_Status

-- Test Case: gfIO_UndervoltageISR_Status.001
TEST.UNIT:io_thread
TEST.SUBPROGRAM:gfIO_UndervoltageISR_Status
TEST.NEW
TEST.NAME:gfIO_UndervoltageISR_Status.001
TEST.COMPOUND_ONLY
TEST.EXPECTED_USER_CODE:io_thread.gfIO_UndervoltageISR_Status.return
{{ <<io_thread.gfIO_UndervoltageISR_Status.return>> == ( fUndervoltageActive ) }}
TEST.END_EXPECTED_USER_CODE:
TEST.END

-- COMPOUND TESTS

TEST.SUBPROGRAM:<<COMPOUND>>
TEST.NEW
TEST.NAME:<<COMPOUND>>Active_Event_List_Update
TEST.SLOT: "1", "EventManager", "vDEM_ActiveEventListUpdate", "1", "vDEM_ActiveEventListUpdate.001"
TEST.END
--

-- COMPOUND TESTS

TEST.SUBPROGRAM:<<COMPOUND>>
TEST.NEW
TEST.NAME:<<COMPOUND>>Dem_Report_Change_Set
TEST.SLOT: "1", "EventManager", "eDEM_ReportChangeSet", "1", "eDEM_ReportChangeSet.001"
TEST.END
--

-- COMPOUND TESTS

TEST.SUBPROGRAM:<<COMPOUND>>
TEST.NEW
TEST.NAME:<<COMPOUND>>Dem_Service_ThreadEntry
TEST.SLOT: "1", "EventManager", "vDEM_ServiceThreadEntry", "1", "vDEM_ServiceThreadEntry.001"
TEST.END
--

-- COMPOUND TESTS

TEST.SUBPROGRAM:<<COMPOUND>>
TEST.NEW
TEST.NAME:<<COMPOUND>>Event_Report_Acknowledge
TEST.SLOT: "1", "EventManager", "gvDEM_EventReportAcknowlege_cb", "1", "gvDEM_EventReportAcknowlege_cb.001"
TEST.END
--

-- COMPOUND TESTS

TEST.SUBPROGRAM:<<COMPOUND>>
TEST.NEW
TEST.NAME:<<COMPOUND>>GetService_Thread_Pointer
TEST.SLOT: "1", "EventManager", "gpDEM_GetServiceThreadPointer", "1", "gpDEM_GetServiceThreadPointer.001"
TEST.END
--

-- COMPOUND TESTS

TEST.SUBPROGRAM:<<COMPOUND>>
TEST.NEW
TEST.NAME:<<COMPOUND>>Get_Cuurent_Control_Limit_Pointer
TEST.SLOT: "1", "EventManager", "gpDEM_GetCurrentControlLimitsPointer", "1", "gpDEM_GetCurrentControlLimitsPointer.001"
TEST.END
--

-- COMPOUND TESTS

TEST.SUBPROGRAM:<<COMPOUND>>
TEST.NEW
TEST.NAME:<<COMPOUND>>Get_Report_Thread_Pointer
TEST.SLOT: "1", "EventManager", "gpDEM_GetReportThreadPointer", "1", "gpDEM_GetReportThreadPointer.001"
TEST.END
--

-- COMPOUND TESTS

TEST.SUBPROGRAM:<<COMPOUND>>
TEST.NEW
TEST.NAME:<<COMPOUND>>Initialize
TEST.SLOT: "1", "EventManager", "gulDEM_Initialize", "1", "gulDEM_Initialize.001"
TEST.END
--

-- COMPOUND TESTS

TEST.SUBPROGRAM:<<COMPOUND>>
TEST.NEW
TEST.NAME:<<COMPOUND>>Log_Under_Voltage_Events
TEST.SLOT: "1", "EventManager", "gvDEM_LogUndervoltageEvents", "1", "gvDEM_LogUndervoltageEvents.001"
TEST.END
--

-- COMPOUND TESTS

TEST.SUBPROGRAM:<<COMPOUND>>
TEST.NEW
TEST.NAME:<<COMPOUND>>Process_Event_State_Changes
TEST.SLOT: "1", "EventManager", "vDEM_ProcessEventStateChanges", "1", "vDEM_ProcessEventStateChanges.001"
TEST.END
--

-- COMPOUND TESTS

TEST.SUBPROGRAM:<<COMPOUND>>
TEST.NEW
TEST.NAME:<<COMPOUND>>Report_Event_State
TEST.SLOT: "1", "EventManager", "gvDEM_ReportEventState", "1", "gvDEM_ReportEventState.001"
TEST.SLOT: "2", "io_thread", "gfIO_UndervoltageISR_Status", "1", "gfIO_UndervoltageISR_Status.001"
TEST.END
--

-- COMPOUND TESTS

TEST.SUBPROGRAM:<<COMPOUND>>
TEST.NEW
TEST.NAME:<<COMPOUND>>Report_Thread_Entry
TEST.SLOT: "1", "EventManager", "vDEM_ReportThreadEntry", "1", "vDEM_ReportThreadEntry.001"
TEST.END
--

-- COMPOUND TESTS

TEST.SUBPROGRAM:<<COMPOUND>>
TEST.NEW
TEST.NAME:<<COMPOUND>>Timer_Entry
TEST.SLOT: "1", "EventManager", "vDEM_TimerEntry", "1", "vDEM_TimerEntry.001"
TEST.SLOT: "2", "OS_Interface", "gulOS_ThreadResume", "1", "gulOS_ThreadResume.001"
TEST.END
--

-- COMPOUND TESTS

TEST.SUBPROGRAM:<<COMPOUND>>
TEST.NEW
TEST.NAME:<<COMPOUND>>Update_Control_Structure
TEST.SLOT: "1", "EventManager", "vDEM_UpdateControlStructure", "1", "vDEM_UpdateControlStructure.001"
TEST.END
--

-- COMPOUND TESTS

TEST.SUBPROGRAM:<<COMPOUND>>
TEST.NEW
TEST.NAME:<<COMPOUND>>fDEM_Check_Report
TEST.SLOT: "1", "EventManager", "fDEM_CheckReport", "1", "fDEM_CheckReport.001"
TEST.END
--
