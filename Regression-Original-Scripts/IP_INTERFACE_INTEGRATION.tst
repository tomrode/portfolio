-- VectorCAST 6.4p (01/31/17)
-- Test Case Script
-- 
-- Environment    : IP_INTERFACE_INTEGRATION
-- Unit(s) Under Test: IP_Interface
-- 
-- Script Features
TEST.SCRIPT_FEATURE:C_DIRECT_ARRAY_INDEXING
TEST.SCRIPT_FEATURE:CPP_CLASS_OBJECT_REVISION
TEST.SCRIPT_FEATURE:MULTIPLE_UUT_SUPPORT
TEST.SCRIPT_FEATURE:MIXED_CASE_NAMES
TEST.SCRIPT_FEATURE:STATIC_HEADER_FUNCS_IN_UUTS
--

-- Subprogram: <<INIT>>

-- Test Case: <<INIT>>.001
TEST.SUBPROGRAM:<<INIT>>
TEST.NEW
TEST.NAME:<<INIT>>.001
TEST.END

-- Unit: IP_Interface

-- Subprogram: geIP_AppendDataPacket

-- Test Case: BASIS-PATH-001
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:geIP_AppendDataPacket
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 1
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.VALUE:IP_Interface.geIP_AppendDataPacket.pPacket:<<malloc 1>>
TEST.VALUE:IP_Interface.geIP_AppendDataPacket.pubData:<<malloc 1>>
TEST.VALUE:IP_Interface.geIP_AppendDataPacket.ulDataSize:<<MIN>>
TEST.VALUE:IP_Interface.geIP_AppendDataPacket.pPacketPool:<<malloc 1>>
TEST.VALUE:IP_Interface.geIP_AppendDataPacket.ulWaitOption:<<MIN>>
TEST.END

-- Subprogram: geIP_Delete

-- Test Case: BASIS-PATH-001
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:geIP_Delete
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 2
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (1) if ((IP_DEL_SUCCESS) == eStatus) ==> FALSE
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:uut_prototype_stubs._nxe_ip_delete.return:<<MIN>>
TEST.END

-- Test Case: BASIS-PATH-002
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:geIP_Delete
TEST.NEW
TEST.NAME:BASIS-PATH-002
TEST.BASIS_PATH:2 of 2
TEST.NOTES:
This is an automatically generated test case.
   Test Path 2
      (1) if ((IP_DEL_SUCCESS) == eStatus) ==> TRUE
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:uut_prototype_stubs._nxe_ip_delete.return:0
TEST.END

-- Subprogram: geIP_FtpServerCreate

-- Test Case: BASIS-PATH-001
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:geIP_FtpServerCreate
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 1
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.END

-- Subprogram: geIP_FtpServerDelete

-- Test Case: BASIS-PATH-001
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:geIP_FtpServerDelete
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 1
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.END

-- Subprogram: geIP_Initialize

-- Test Case: BASIS-PATH-001
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:geIP_Initialize
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 10
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (1) if ((IP_INIT_SUCCESS) == eStatus) ==> FALSE
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:uut_prototype_stubs._nxe_packet_pool_create.return:<<MIN>>
TEST.END

-- Subprogram: geIP_MessageSend

-- Test Case: BASIS-PATH-001-PARTIAL
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:geIP_MessageSend
TEST.NEW
TEST.NAME:BASIS-PATH-001-PARTIAL
TEST.BASIS_PATH:1 of 9 (partial)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (1) if ((IP_TCP_SOCKET) == eSocketType) ==> FALSE
      (2) if (((IP_TX_SUCCESS) == eStatus && (void *)0 != pPacket) && (void *)0 != pPacket->nx_packet_prepend_ptr) ==> FALSE
   Test Case Generation Notes:
      Cannot set local variable pPacket in branch 2
TEST.END_NOTES:
TEST.VALUE:IP_Interface.geIP_MessageSend.pSocket:VECTORCAST_INT1
TEST.VALUE:IP_Interface.geIP_MessageSend.pubBuffer:<<malloc 1>>
TEST.VALUE:IP_Interface.geIP_MessageSend.ulMessageLength:<<MIN>>
TEST.VALUE:IP_Interface.geIP_MessageSend.ulWaitOption:<<MIN>>
TEST.VALUE:IP_Interface.geIP_MessageSend.eSocketType:IP_UDP_SOCKET
TEST.VALUE:IP_Interface.geIP_MessageSend.ulIpAddress:<<MIN>>
TEST.VALUE:IP_Interface.geIP_MessageSend.ulPort:<<MIN>>
TEST.VALUE:uut_prototype_stubs._nxe_packet_allocate.return:<<MIN>>
TEST.END

-- Test Case: IP_TCP_SOCKET_==_eSocketType
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:geIP_MessageSend
TEST.NEW
TEST.NAME:IP_TCP_SOCKET_==_eSocketType
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (1) if ((IP_TCP_SOCKET) == eSocketType) ==> FALSE
      (2) if (((IP_TX_SUCCESS) == eStatus && (void *)0 != pPacket) && (void *)0 != pPacket->nx_packet_prepend_ptr) ==> FALSE
   Test Case Generation Notes:
      Cannot set local variable pPacket in branch 2
TEST.END_NOTES:
TEST.VALUE:IP_Interface.geIP_MessageSend.pSocket:VECTORCAST_INT1
TEST.VALUE:IP_Interface.geIP_MessageSend.pubBuffer:<<malloc 1>>
TEST.VALUE:IP_Interface.geIP_MessageSend.ulMessageLength:<<MIN>>
TEST.VALUE:IP_Interface.geIP_MessageSend.ulWaitOption:<<MIN>>
TEST.VALUE:IP_Interface.geIP_MessageSend.eSocketType:IP_TCP_SOCKET
TEST.VALUE:IP_Interface.geIP_MessageSend.ulIpAddress:<<MIN>>
TEST.VALUE:IP_Interface.geIP_MessageSend.ulPort:<<MIN>>
TEST.VALUE:uut_prototype_stubs._nxe_packet_allocate.return:<<MIN>>
TEST.END

-- Test Case: if_(_(IP_TX_SUCCESS_==_eStatus)_&&_(((void_*)0)_!=_pPacket)_&&_(((void_*)0)_!=_pPacket->nx_packet_prepend_ptr)_)
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:geIP_MessageSend
TEST.NEW
TEST.NAME:if_(_(IP_TX_SUCCESS_==_eStatus)_&&_(((void_*)0)_!=_pPacket)_&&_(((void_*)0)_!=_pPacket->nx_packet_prepend_ptr)_)
TEST.END

-- Subprogram: geIP_ReleasePacket

-- Test Case: BASIS-PATH-001
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:geIP_ReleasePacket
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 1
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.VALUE:IP_Interface.geIP_ReleasePacket.pPacket:<<malloc 1>>
TEST.END

-- Subprogram: geIP_SendLcpEchoRequest

-- Test Case: BASIS-PATH-001
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:geIP_SendLcpEchoRequest
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 1
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.END

-- Subprogram: geIP_SocketAccept

-- Test Case: BASIS-PATH-001
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:geIP_SocketAccept
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 1
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.VALUE:IP_Interface.geIP_SocketAccept.pSocket:<<malloc 1>>
TEST.VALUE:IP_Interface.geIP_SocketAccept.ulWaitOption:<<MIN>>
TEST.END

-- Subprogram: geIP_SocketCreate

-- Test Case: BASIS-PATH-001
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:geIP_SocketCreate
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 2
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (1) if ((IP_SOCKET_SUCCESS) == eStatus && (void *)0 != pSocket) ==> FALSE
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.geIP_SocketCreate.pSocket:<<null>>
TEST.VALUE:IP_Interface.geIP_SocketCreate.pszName:<<malloc 1>>
TEST.VALUE:uut_prototype_stubs._nxe_tcp_socket_create.return:<<MIN>>
TEST.END

-- Test Case: BASIS-PATH-002
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:geIP_SocketCreate
TEST.NEW
TEST.NAME:BASIS-PATH-002
TEST.BASIS_PATH:2 of 2
TEST.NOTES:
This is an automatically generated test case.
   Test Path 2
      (1) if ((IP_SOCKET_SUCCESS) == eStatus && (void *)0 != pSocket) ==> TRUE
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.geIP_SocketCreate.pSocket:<<malloc 1>>
TEST.VALUE:IP_Interface.geIP_SocketCreate.pszName:<<malloc 1>>
TEST.VALUE:uut_prototype_stubs._nxe_tcp_socket_create.return:0
TEST.END

-- Subprogram: geIP_SocketDelete

-- Test Case: BASIS-PATH-001
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:geIP_SocketDelete
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 3
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (1) if ((IP_SOCKET_SUCCESS) == eStatus) ==> FALSE
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.geIP_SocketDelete.pSocket:<<malloc 1>>
TEST.VALUE:IP_Interface.geIP_SocketDelete.ulPortNumber:<<MIN>>
TEST.VALUE:uut_prototype_stubs._nxe_tcp_server_socket_unaccept.return:<<MIN>>
TEST.END

-- Test Case: BASIS-PATH-002-PARTIAL
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:geIP_SocketDelete
TEST.NEW
TEST.NAME:BASIS-PATH-002-PARTIAL
TEST.BASIS_PATH:2 of 3 (partial)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 2
      (1) if ((IP_SOCKET_SUCCESS) == eStatus) ==> FALSE
      (2) if ((IP_SOCKET_SUCCESS) == eStatus) ==> TRUE
   Test Case Generation Notes:
      Conflict: Trying to set variable uut_prototype_stubs._nxe_tcp_server_socket_unaccept.return 'equal to' and 'not equal to' same value in branches 1/2
TEST.END_NOTES:
TEST.VALUE:IP_Interface.geIP_SocketDelete.pSocket:<<malloc 1>>
TEST.VALUE:IP_Interface.geIP_SocketDelete.ulPortNumber:<<MIN>>
TEST.VALUE:uut_prototype_stubs._nxe_tcp_server_socket_unaccept.return:<<MIN>>
TEST.END

-- Test Case: BASIS-PATH-003
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:geIP_SocketDelete
TEST.NEW
TEST.NAME:BASIS-PATH-003
TEST.BASIS_PATH:3 of 3
TEST.NOTES:
This is an automatically generated test case.
   Test Path 3
      (1) if ((IP_SOCKET_SUCCESS) == eStatus) ==> TRUE
      (2) if ((IP_SOCKET_SUCCESS) == eStatus) ==> FALSE
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.geIP_SocketDelete.pSocket:<<malloc 1>>
TEST.VALUE:IP_Interface.geIP_SocketDelete.ulPortNumber:<<MIN>>
TEST.VALUE:uut_prototype_stubs._nxe_tcp_server_socket_unaccept.return:0
TEST.VALUE:uut_prototype_stubs._nxe_tcp_server_socket_unlisten.return:<<MIN>>
TEST.END

-- Subprogram: geIP_SocketListen

-- Test Case: BASIS-PATH-001
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:geIP_SocketListen
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 1
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.VALUE:IP_Interface.geIP_SocketListen.pSocket:<<malloc 1>>
TEST.VALUE:IP_Interface.geIP_SocketListen.ulPortNumber:<<MIN>>
TEST.END

-- Subprogram: geIP_UDPSocketCreate

-- Test Case: BASIS-PATH-001
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:geIP_UDPSocketCreate
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 1
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.VALUE:IP_Interface.geIP_UDPSocketCreate.pSocket:<<malloc 1>>
TEST.VALUE:IP_Interface.geIP_UDPSocketCreate.pszName:<<malloc 1>>
TEST.END

-- Subprogram: geIP_UDPSocketDelete

-- Test Case: BASIS-PATH-001
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:geIP_UDPSocketDelete
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 2
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (1) if ((IP_SOCKET_SUCCESS) == eStatus) ==> FALSE
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.geIP_UDPSocketDelete.pSocket:<<malloc 1>>
TEST.VALUE:uut_prototype_stubs._nxe_udp_socket_unbind.return:<<MIN>>
TEST.END

-- Test Case: BASIS-PATH-002
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:geIP_UDPSocketDelete
TEST.NEW
TEST.NAME:BASIS-PATH-002
TEST.BASIS_PATH:2 of 2
TEST.NOTES:
This is an automatically generated test case.
   Test Path 2
      (1) if ((IP_SOCKET_SUCCESS) == eStatus) ==> TRUE
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.geIP_UDPSocketDelete.pSocket:<<malloc 1>>
TEST.VALUE:uut_prototype_stubs._nxe_udp_socket_unbind.return:0
TEST.END

-- Subprogram: geIP_UDPSocket_Bind

-- Test Case: BASIS-PATH-001
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:geIP_UDPSocket_Bind
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 1
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.VALUE:IP_Interface.geIP_UDPSocket_Bind.pSocket:<<malloc 1>>
TEST.VALUE:IP_Interface.geIP_UDPSocket_Bind.ulportnumber:<<MIN>>
TEST.VALUE:IP_Interface.geIP_UDPSocket_Bind.ulwait_option:<<MIN>>
TEST.END

-- Subprogram: gfIP_CheckLinkUp

-- Test Case: BASIS-PATH-001
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gfIP_CheckLinkUp
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 2
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (1) if (0U == _nxe_ip_status_check(&ip, 0x4U, &ip_status, ulWaitOption)) ==> FALSE
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.gfIP_CheckLinkUp.ulWaitOption:<<MIN>>
TEST.VALUE:uut_prototype_stubs._nxe_ip_status_check.return:<<MIN>>
TEST.END

-- Test Case: BASIS-PATH-002
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gfIP_CheckLinkUp
TEST.NEW
TEST.NAME:BASIS-PATH-002
TEST.BASIS_PATH:2 of 2
TEST.NOTES:
This is an automatically generated test case.
   Test Path 2
      (1) if (0U == _nxe_ip_status_check(&ip, 0x4U, &ip_status, ulWaitOption)) ==> TRUE
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.gfIP_CheckLinkUp.ulWaitOption:<<MIN>>
TEST.VALUE:uut_prototype_stubs._nxe_ip_status_check.return:0
TEST.END

-- Subprogram: gfIP_LcpEchoResponseReceived

-- Test Case: BASIS-PATH-001
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gfIP_LcpEchoResponseReceived
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 2
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (1) if (ppp.nx_ppp_lcp_echo_reply_id > 0U) ==> FALSE
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.<<GLOBAL>>.ppp.nx_ppp_lcp_echo_reply_id:<<MIN>>
TEST.END

-- Test Case: BASIS-PATH-002
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gfIP_LcpEchoResponseReceived
TEST.NEW
TEST.NAME:BASIS-PATH-002
TEST.BASIS_PATH:2 of 2
TEST.NOTES:
This is an automatically generated test case.
   Test Path 2
      (1) if (ppp.nx_ppp_lcp_echo_reply_id > 0U) ==> TRUE
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.<<GLOBAL>>.ppp.nx_ppp_lcp_echo_reply_id:<<MAX>>
TEST.END

-- Subprogram: gpIP_GetFtpThreadPointer

-- Test Case: BASIS-PATH-001
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gpIP_GetFtpThreadPointer
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 1
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.END

-- Subprogram: gpIP_GetIpThreadPointer

-- Test Case: BASIS-PATH-001
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gpIP_GetIpThreadPointer
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 1
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.END

-- Subprogram: gpIP_GetPppThreadPointer

-- Test Case: BASIS-PATH-001
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gpIP_GetPppThreadPointer
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 1
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.END

-- Subprogram: gpszIP_GetInitializationStatus

-- Test Case: BASIS-PATH-001
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gpszIP_GetInitializationStatus
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 9
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (9) case eStatus ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.gpszIP_GetInitializationStatus.eStatus:22
TEST.EXPECTED:IP_Interface.gpszIP_GetInitializationStatus.eStatus:22
TEST.END

-- Test Case: BASIS-PATH-002
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gpszIP_GetInitializationStatus
TEST.NEW
TEST.NAME:BASIS-PATH-002
TEST.BASIS_PATH:2 of 9
TEST.NOTES:
This is an automatically generated test case.
   Test Path 2
      (1) case eStatus ==> IP_INIT_SUCCESS
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.gpszIP_GetInitializationStatus.eStatus:IP_INIT_SUCCESS
TEST.END

-- Test Case: BASIS-PATH-003
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gpszIP_GetInitializationStatus
TEST.NEW
TEST.NAME:BASIS-PATH-003
TEST.BASIS_PATH:3 of 9
TEST.NOTES:
This is an automatically generated test case.
   Test Path 3
      (2) case eStatus ==> IP_INIT_PTR_ERROR
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.gpszIP_GetInitializationStatus.eStatus:IP_INIT_PTR_ERROR
TEST.EXPECTED:IP_Interface.gpszIP_GetInitializationStatus.eStatus:IP_INIT_PTR_ERROR
TEST.END

-- Test Case: BASIS-PATH-004
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gpszIP_GetInitializationStatus
TEST.NEW
TEST.NAME:BASIS-PATH-004
TEST.BASIS_PATH:4 of 9
TEST.NOTES:
This is an automatically generated test case.
   Test Path 4
      (3) case eStatus ==> IP_INIT_SIZE_ERROR
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.gpszIP_GetInitializationStatus.eStatus:IP_INIT_SIZE_ERROR
TEST.END

-- Test Case: BASIS-PATH-005
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gpszIP_GetInitializationStatus
TEST.NEW
TEST.NAME:BASIS-PATH-005
TEST.BASIS_PATH:5 of 9
TEST.NOTES:
This is an automatically generated test case.
   Test Path 5
      (4) case eStatus ==> IP_INIT_CALLER_ERROR
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.gpszIP_GetInitializationStatus.eStatus:IP_INIT_CALLER_ERROR
TEST.END

-- Test Case: BASIS-PATH-006
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gpszIP_GetInitializationStatus
TEST.NEW
TEST.NAME:BASIS-PATH-006
TEST.BASIS_PATH:6 of 9
TEST.NOTES:
This is an automatically generated test case.
   Test Path 6
      (5) case eStatus ==> IP_INIT_PPP_START_ERROR
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.gpszIP_GetInitializationStatus.eStatus:IP_INIT_PPP_START_ERROR
TEST.END

-- Test Case: BASIS-PATH-007
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gpszIP_GetInitializationStatus
TEST.NEW
TEST.NAME:BASIS-PATH-007
TEST.BASIS_PATH:7 of 9
TEST.NOTES:
This is an automatically generated test case.
   Test Path 7
      (6) case eStatus ==> IP_INIT_IP_ADDR_ERROR
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.gpszIP_GetInitializationStatus.eStatus:IP_INIT_IP_ADDR_ERROR
TEST.END

-- Test Case: BASIS-PATH-008
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gpszIP_GetInitializationStatus
TEST.NEW
TEST.NAME:BASIS-PATH-008
TEST.BASIS_PATH:8 of 9
TEST.NOTES:
This is an automatically generated test case.
   Test Path 8
      (7) case eStatus ==> IP_INIT_IP_START_ERROR
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.gpszIP_GetInitializationStatus.eStatus:IP_INIT_IP_START_ERROR
TEST.END

-- Test Case: BASIS-PATH-009
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gpszIP_GetInitializationStatus
TEST.NEW
TEST.NAME:BASIS-PATH-009
TEST.BASIS_PATH:9 of 9
TEST.NOTES:
This is an automatically generated test case.
   Test Path 9
      (8) case eStatus ==> IP_INIT_ALREADY_ENABLED
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.gpszIP_GetInitializationStatus.eStatus:IP_INIT_ALREADY_ENABLED
TEST.END

-- Subprogram: gpszIP_GetMessageReceiveStatus

-- Test Case: BASIS-PATH-001
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gpszIP_GetMessageReceiveStatus
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 10
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (10) case eStatus ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.gpszIP_GetMessageReceiveStatus.eStatus:65281
TEST.END

-- Test Case: BASIS-PATH-002
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gpszIP_GetMessageReceiveStatus
TEST.NEW
TEST.NAME:BASIS-PATH-002
TEST.BASIS_PATH:2 of 10
TEST.NOTES:
This is an automatically generated test case.
   Test Path 2
      (1) case eStatus ==> IP_RX_SUCCESS
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.gpszIP_GetMessageReceiveStatus.eStatus:IP_RX_SUCCESS
TEST.END

-- Test Case: BASIS-PATH-003
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gpszIP_GetMessageReceiveStatus
TEST.NEW
TEST.NAME:BASIS-PATH-003
TEST.BASIS_PATH:3 of 10
TEST.NOTES:
This is an automatically generated test case.
   Test Path 3
      (2) case eStatus ==> IP_RX_NOT_BOUND
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.gpszIP_GetMessageReceiveStatus.eStatus:IP_RX_NOT_BOUND
TEST.END

-- Test Case: BASIS-PATH-004
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gpszIP_GetMessageReceiveStatus
TEST.NEW
TEST.NAME:BASIS-PATH-004
TEST.BASIS_PATH:4 of 10
TEST.NOTES:
This is an automatically generated test case.
   Test Path 4
      (3) case eStatus ==> IP_RX_NO_PACKET
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.gpszIP_GetMessageReceiveStatus.eStatus:IP_RX_NO_PACKET
TEST.END

-- Test Case: BASIS-PATH-005
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gpszIP_GetMessageReceiveStatus
TEST.NEW
TEST.NAME:BASIS-PATH-005
TEST.BASIS_PATH:5 of 10
TEST.NOTES:
This is an automatically generated test case.
   Test Path 5
      (4) case eStatus ==> IP_RX_WAIT_ABORTED
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.gpszIP_GetMessageReceiveStatus.eStatus:IP_RX_WAIT_ABORTED
TEST.END

-- Test Case: BASIS-PATH-006
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gpszIP_GetMessageReceiveStatus
TEST.NEW
TEST.NAME:BASIS-PATH-006
TEST.BASIS_PATH:6 of 10
TEST.NOTES:
This is an automatically generated test case.
   Test Path 6
      (5) case eStatus ==> IP_RX_NOT_CONNECTED
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.gpszIP_GetMessageReceiveStatus.eStatus:IP_RX_NOT_CONNECTED
TEST.END

-- Test Case: BASIS-PATH-007
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gpszIP_GetMessageReceiveStatus
TEST.NEW
TEST.NAME:BASIS-PATH-007
TEST.BASIS_PATH:7 of 10
TEST.NOTES:
This is an automatically generated test case.
   Test Path 7
      (6) case eStatus ==> IP_RX_PTR_ERROR
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.gpszIP_GetMessageReceiveStatus.eStatus:IP_RX_PTR_ERROR
TEST.END

-- Test Case: BASIS-PATH-008
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gpszIP_GetMessageReceiveStatus
TEST.NEW
TEST.NAME:BASIS-PATH-008
TEST.BASIS_PATH:8 of 10
TEST.NOTES:
This is an automatically generated test case.
   Test Path 8
      (7) case eStatus ==> IP_RX_CALLER_ERROR
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.gpszIP_GetMessageReceiveStatus.eStatus:IP_RX_CALLER_ERROR
TEST.END

-- Test Case: BASIS-PATH-009
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gpszIP_GetMessageReceiveStatus
TEST.NEW
TEST.NAME:BASIS-PATH-009
TEST.BASIS_PATH:9 of 10
TEST.NOTES:
This is an automatically generated test case.
   Test Path 9
      (8) case eStatus ==> IP_RX_NOT_ENABLED
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.gpszIP_GetMessageReceiveStatus.eStatus:IP_RX_NOT_ENABLED
TEST.END

-- Test Case: BASIS-PATH-010
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gpszIP_GetMessageReceiveStatus
TEST.NEW
TEST.NAME:BASIS-PATH-010
TEST.BASIS_PATH:10 of 10
TEST.NOTES:
This is an automatically generated test case.
   Test Path 10
      (9) case eStatus ==> IP_RX_BUFFER_TOO_SMALL
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.gpszIP_GetMessageReceiveStatus.eStatus:IP_RX_BUFFER_TOO_SMALL
TEST.END

-- Subprogram: gpszIP_GetMessageSendStatus

-- Test Case: BASIS-PATH-001
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gpszIP_GetMessageSendStatus
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 19
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (19) case eStatus ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.gpszIP_GetMessageSendStatus.eStatus:IP_TX_SIZE_ERROR
TEST.END

-- Test Case: BASIS-PATH-002
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gpszIP_GetMessageSendStatus
TEST.NEW
TEST.NAME:BASIS-PATH-002
TEST.BASIS_PATH:2 of 19
TEST.NOTES:
This is an automatically generated test case.
   Test Path 2
      (1) case eStatus ==> IP_TX_SUCCESS
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.gpszIP_GetMessageSendStatus.eStatus:IP_TX_SUCCESS
TEST.END

-- Test Case: BASIS-PATH-003
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gpszIP_GetMessageSendStatus
TEST.NEW
TEST.NAME:BASIS-PATH-003
TEST.BASIS_PATH:3 of 19
TEST.NOTES:
This is an automatically generated test case.
   Test Path 3
      (2) case eStatus ==> IP_TX_NO_PACKET
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.gpszIP_GetMessageSendStatus.eStatus:IP_TX_NO_PACKET
TEST.END

-- Test Case: BASIS-PATH-004
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gpszIP_GetMessageSendStatus
TEST.NEW
TEST.NAME:BASIS-PATH-004
TEST.BASIS_PATH:4 of 19
TEST.NOTES:
This is an automatically generated test case.
   Test Path 4
      (3) case eStatus ==> IP_TX_WAIT_ABORTED
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.gpszIP_GetMessageSendStatus.eStatus:IP_TX_WAIT_ABORTED
TEST.END

-- Test Case: BASIS-PATH-005
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gpszIP_GetMessageSendStatus
TEST.NEW
TEST.NAME:BASIS-PATH-005
TEST.BASIS_PATH:5 of 19
TEST.NOTES:
This is an automatically generated test case.
   Test Path 5
      (4) case eStatus ==> IP_TX_OPTION_ERROR
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.gpszIP_GetMessageSendStatus.eStatus:IP_TX_OPTION_ERROR
TEST.END

-- Test Case: BASIS-PATH-006
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gpszIP_GetMessageSendStatus
TEST.NEW
TEST.NAME:BASIS-PATH-006
TEST.BASIS_PATH:6 of 19
TEST.NOTES:
This is an automatically generated test case.
   Test Path 6
      (5) case eStatus ==> IP_TX_PTR_ERROR
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.gpszIP_GetMessageSendStatus.eStatus:IP_TX_PTR_ERROR
TEST.END

-- Test Case: BASIS-PATH-007
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gpszIP_GetMessageSendStatus
TEST.NEW
TEST.NAME:BASIS-PATH-007
TEST.BASIS_PATH:7 of 19
TEST.NOTES:
This is an automatically generated test case.
   Test Path 7
      (6) case eStatus ==> IP_TX_INVALID_PARAMETERS
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.gpszIP_GetMessageSendStatus.eStatus:IP_TX_INVALID_PARAMETERS
TEST.END

-- Test Case: BASIS-PATH-008
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gpszIP_GetMessageSendStatus
TEST.NEW
TEST.NAME:BASIS-PATH-008
TEST.BASIS_PATH:8 of 19
TEST.NOTES:
This is an automatically generated test case.
   Test Path 8
      (7) case eStatus ==> IP_TX_CALLER_ERROR
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.gpszIP_GetMessageSendStatus.eStatus:IP_TX_CALLER_ERROR
TEST.END

-- Test Case: BASIS-PATH-009
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gpszIP_GetMessageSendStatus
TEST.NEW
TEST.NAME:BASIS-PATH-009
TEST.BASIS_PATH:9 of 19
TEST.NOTES:
This is an automatically generated test case.
   Test Path 9
      (8) case eStatus ==> IP_TX_NOT_BOUND
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.gpszIP_GetMessageSendStatus.eStatus:IP_TX_NOT_BOUND
TEST.END

-- Test Case: BASIS-PATH-010
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gpszIP_GetMessageSendStatus
TEST.NEW
TEST.NAME:BASIS-PATH-010
TEST.BASIS_PATH:10 of 19
TEST.NOTES:
This is an automatically generated test case.
   Test Path 10
      (9) case eStatus ==> IP_TX_NO_INTERFACE_ADDRESS
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.gpszIP_GetMessageSendStatus.eStatus:IP_TX_NO_INTERFACE_ADDRESS
TEST.END

-- Test Case: BASIS-PATH-011
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gpszIP_GetMessageSendStatus
TEST.NEW
TEST.NAME:BASIS-PATH-011
TEST.BASIS_PATH:11 of 19
TEST.NOTES:
This is an automatically generated test case.
   Test Path 11
      (10) case eStatus ==> IP_TX_NOT_CONNECTED
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.gpszIP_GetMessageSendStatus.eStatus:IP_TX_NOT_CONNECTED
TEST.END

-- Test Case: BASIS-PATH-012
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gpszIP_GetMessageSendStatus
TEST.NEW
TEST.NAME:BASIS-PATH-012
TEST.BASIS_PATH:12 of 19
TEST.NOTES:
This is an automatically generated test case.
   Test Path 12
      (11) case eStatus ==> IP_TX_ALREADY_SUSPENDED
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.gpszIP_GetMessageSendStatus.eStatus:IP_TX_ALREADY_SUSPENDED
TEST.END

-- Test Case: BASIS-PATH-013
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gpszIP_GetMessageSendStatus
TEST.NEW
TEST.NAME:BASIS-PATH-013
TEST.BASIS_PATH:13 of 19
TEST.NOTES:
This is an automatically generated test case.
   Test Path 13
      (12) case eStatus ==> IP_TX_WINDOW_OVERFLOW
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.gpszIP_GetMessageSendStatus.eStatus:IP_TX_WINDOW_OVERFLOW
TEST.END

-- Test Case: BASIS-PATH-014
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gpszIP_GetMessageSendStatus
TEST.NEW
TEST.NAME:BASIS-PATH-014
TEST.BASIS_PATH:14 of 19
TEST.NOTES:
This is an automatically generated test case.
   Test Path 14
      (13) case eStatus ==> IP_TX_INVALID_PACKET
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.gpszIP_GetMessageSendStatus.eStatus:IP_TX_INVALID_PACKET
TEST.END

-- Test Case: BASIS-PATH-015
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gpszIP_GetMessageSendStatus
TEST.NEW
TEST.NAME:BASIS-PATH-015
TEST.BASIS_PATH:15 of 19
TEST.NOTES:
This is an automatically generated test case.
   Test Path 15
      (14) case eStatus ==> IP_TX_QUEUE_DEPTH
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.gpszIP_GetMessageSendStatus.eStatus:IP_TX_QUEUE_DEPTH
TEST.END

-- Test Case: BASIS-PATH-016
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gpszIP_GetMessageSendStatus
TEST.NEW
TEST.NAME:BASIS-PATH-016
TEST.BASIS_PATH:16 of 19
TEST.NOTES:
This is an automatically generated test case.
   Test Path 16
      (15) case eStatus ==> IP_TX_OVERFLOW
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.gpszIP_GetMessageSendStatus.eStatus:IP_TX_OVERFLOW
TEST.END

-- Test Case: BASIS-PATH-017
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gpszIP_GetMessageSendStatus
TEST.NEW
TEST.NAME:BASIS-PATH-017
TEST.BASIS_PATH:17 of 19
TEST.NOTES:
This is an automatically generated test case.
   Test Path 17
      (16) case eStatus ==> IP_TX_NOT_ENABLED
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.gpszIP_GetMessageSendStatus.eStatus:IP_TX_NOT_ENABLED
TEST.END

-- Test Case: BASIS-PATH-018
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gpszIP_GetMessageSendStatus
TEST.NEW
TEST.NAME:BASIS-PATH-018
TEST.BASIS_PATH:18 of 19
TEST.NOTES:
This is an automatically generated test case.
   Test Path 18
      (17) case eStatus ==> IP_TX_UNDERFLOW
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.gpszIP_GetMessageSendStatus.eStatus:IP_TX_UNDERFLOW
TEST.END

-- Test Case: BASIS-PATH-019
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gpszIP_GetMessageSendStatus
TEST.NEW
TEST.NAME:BASIS-PATH-019
TEST.BASIS_PATH:19 of 19
TEST.NOTES:
This is an automatically generated test case.
   Test Path 19
      (18) case eStatus ==> IP_TX_LENGTH_INCORRECT
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.gpszIP_GetMessageSendStatus.eStatus:IP_TX_LENGTH_INCORRECT
TEST.END

-- Subprogram: gpszIP_GetSocketCreateStatus

-- Test Case: BASIS-PATH-001
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gpszIP_GetSocketCreateStatus
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 17
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (17) case eStatus ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.gpszIP_GetSocketCreateStatus.eStatus:IP_SOCKET_NOT_CONNECTED
TEST.END

-- Test Case: BASIS-PATH-002
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gpszIP_GetSocketCreateStatus
TEST.NEW
TEST.NAME:BASIS-PATH-002
TEST.BASIS_PATH:2 of 17
TEST.NOTES:
This is an automatically generated test case.
   Test Path 2
      (1) case eStatus ==> IP_SOCKET_SUCCESS
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.gpszIP_GetSocketCreateStatus.eStatus:IP_SOCKET_SUCCESS
TEST.END

-- Test Case: BASIS-PATH-003
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gpszIP_GetSocketCreateStatus
TEST.NEW
TEST.NAME:BASIS-PATH-003
TEST.BASIS_PATH:3 of 17
TEST.NOTES:
This is an automatically generated test case.
   Test Path 3
      (2) case eStatus ==> IP_SOCKET_OPTION_ERROR
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.gpszIP_GetSocketCreateStatus.eStatus:IP_SOCKET_OPTION_ERROR
TEST.END

-- Test Case: BASIS-PATH-004
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gpszIP_GetSocketCreateStatus
TEST.NEW
TEST.NAME:BASIS-PATH-004
TEST.BASIS_PATH:4 of 17
TEST.NOTES:
This is an automatically generated test case.
   Test Path 4
      (3) case eStatus ==> IP_SOCKET_PTR_ERROR
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.gpszIP_GetSocketCreateStatus.eStatus:IP_SOCKET_PTR_ERROR
TEST.END

-- Test Case: BASIS-PATH-005
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gpszIP_GetSocketCreateStatus
TEST.NEW
TEST.NAME:BASIS-PATH-005
TEST.BASIS_PATH:5 of 17
TEST.NOTES:
This is an automatically generated test case.
   Test Path 5
      (4) case eStatus ==> IP_SOCKET_CALLER_ERROR
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.gpszIP_GetSocketCreateStatus.eStatus:IP_SOCKET_CALLER_ERROR
TEST.END

-- Test Case: BASIS-PATH-006
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gpszIP_GetSocketCreateStatus
TEST.NEW
TEST.NAME:BASIS-PATH-006
TEST.BASIS_PATH:6 of 17
TEST.NOTES:
This is an automatically generated test case.
   Test Path 6
      (5) case eStatus ==> IP_SOCKET_NOT_ENABLED
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.gpszIP_GetSocketCreateStatus.eStatus:IP_SOCKET_NOT_ENABLED
TEST.END

-- Test Case: BASIS-PATH-007
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gpszIP_GetSocketCreateStatus
TEST.NEW
TEST.NAME:BASIS-PATH-007
TEST.BASIS_PATH:7 of 17
TEST.NOTES:
This is an automatically generated test case.
   Test Path 7
      (6) case eStatus ==> IP_SOCKET_MAX_LISTEN
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.gpszIP_GetSocketCreateStatus.eStatus:IP_SOCKET_MAX_LISTEN
TEST.END

-- Test Case: BASIS-PATH-008
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gpszIP_GetSocketCreateStatus
TEST.NEW
TEST.NAME:BASIS-PATH-008
TEST.BASIS_PATH:8 of 17
TEST.NOTES:
This is an automatically generated test case.
   Test Path 8
      (7) case eStatus ==> IP_SOCKET_NOT_CLOSED
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.gpszIP_GetSocketCreateStatus.eStatus:IP_SOCKET_NOT_CLOSED
TEST.END

-- Test Case: BASIS-PATH-009
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gpszIP_GetSocketCreateStatus
TEST.NEW
TEST.NAME:BASIS-PATH-009
TEST.BASIS_PATH:9 of 17
TEST.NOTES:
This is an automatically generated test case.
   Test Path 9
      (8) case eStatus ==> IP_SOCKET_ALREADY_BOUND
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.gpszIP_GetSocketCreateStatus.eStatus:IP_SOCKET_ALREADY_BOUND
TEST.END

-- Test Case: BASIS-PATH-010
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gpszIP_GetSocketCreateStatus
TEST.NEW
TEST.NAME:BASIS-PATH-010
TEST.BASIS_PATH:10 of 17
TEST.NOTES:
This is an automatically generated test case.
   Test Path 10
      (9) case eStatus ==> IP_SOCKET_DUPLICATE_LISTEN
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.gpszIP_GetSocketCreateStatus.eStatus:IP_SOCKET_DUPLICATE_LISTEN
TEST.END

-- Test Case: BASIS-PATH-011
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gpszIP_GetSocketCreateStatus
TEST.NEW
TEST.NAME:BASIS-PATH-011
TEST.BASIS_PATH:11 of 17
TEST.NOTES:
This is an automatically generated test case.
   Test Path 11
      (10) case eStatus ==> IP_SOCKET_INVALID_PORT
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.gpszIP_GetSocketCreateStatus.eStatus:IP_SOCKET_INVALID_PORT
TEST.END

-- Test Case: BASIS-PATH-012
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gpszIP_GetSocketCreateStatus
TEST.NEW
TEST.NAME:BASIS-PATH-012
TEST.BASIS_PATH:12 of 17
TEST.NOTES:
This is an automatically generated test case.
   Test Path 12
      (11) case eStatus ==> IP_SOCKET_NOT_LISTEN_STATE
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.gpszIP_GetSocketCreateStatus.eStatus:IP_SOCKET_NOT_LISTEN_STATE
TEST.END

-- Test Case: BASIS-PATH-013
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gpszIP_GetSocketCreateStatus
TEST.NEW
TEST.NAME:BASIS-PATH-013
TEST.BASIS_PATH:13 of 17
TEST.NOTES:
This is an automatically generated test case.
   Test Path 13
      (12) case eStatus ==> IP_SOCKET_IN_PROGRESS
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.gpszIP_GetSocketCreateStatus.eStatus:IP_SOCKET_IN_PROGRESS
TEST.END

-- Test Case: BASIS-PATH-014
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gpszIP_GetSocketCreateStatus
TEST.NEW
TEST.NAME:BASIS-PATH-014
TEST.BASIS_PATH:14 of 17
TEST.NOTES:
This is an automatically generated test case.
   Test Path 14
      (13) case eStatus ==> IP_SOCKET_WAIT_ABORTED
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.gpszIP_GetSocketCreateStatus.eStatus:IP_SOCKET_WAIT_ABORTED
TEST.END

-- Test Case: BASIS-PATH-015
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gpszIP_GetSocketCreateStatus
TEST.NEW
TEST.NAME:BASIS-PATH-015
TEST.BASIS_PATH:15 of 17
TEST.NOTES:
This is an automatically generated test case.
   Test Path 15
      (14) case eStatus ==> IP_SOCKET_STILL_BOUND
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.gpszIP_GetSocketCreateStatus.eStatus:IP_SOCKET_STILL_BOUND
TEST.END

-- Test Case: BASIS-PATH-016
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gpszIP_GetSocketCreateStatus
TEST.NEW
TEST.NAME:BASIS-PATH-016
TEST.BASIS_PATH:16 of 17
TEST.NOTES:
This is an automatically generated test case.
   Test Path 16
      (15) case eStatus ==> IP_SOCKET_PORT_UNAVAILABLE
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.gpszIP_GetSocketCreateStatus.eStatus:IP_SOCKET_PORT_UNAVAILABLE
TEST.END

-- Test Case: BASIS-PATH-017
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gpszIP_GetSocketCreateStatus
TEST.NEW
TEST.NAME:BASIS-PATH-017
TEST.BASIS_PATH:17 of 17
TEST.NOTES:
This is an automatically generated test case.
   Test Path 17
      (16) case eStatus ==> IP_SOCKET_NO_FREE_PORTS
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.gpszIP_GetSocketCreateStatus.eStatus:IP_SOCKET_NO_FREE_PORTS
TEST.END

-- Subprogram: gszIP_MessageReceive

-- Test Case: eState_==_IP_ST_COMPLETE_MESSAGE
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gszIP_MessageReceive
TEST.NEW
TEST.NAME:eState_==_IP_ST_COMPLETE_MESSAGE
TEST.COMPOUND_ONLY
TEST.VALUE:IP_Interface.gszIP_MessageReceive.pSocket:<<malloc 1>>
TEST.VALUE:IP_Interface.gszIP_MessageReceive.pPacketBuffer:<<malloc 1>>
TEST.VALUE:IP_Interface.gszIP_MessageReceive.pPacketBuffer[0].ulUnprocessedCharacters:1
TEST.VALUE:IP_Interface.gszIP_MessageReceive.peStatus:<<malloc 1>>
TEST.VALUE:IP_Interface.gszIP_MessageReceive.pulMessageSize:<<malloc 1>>
TEST.VALUE:IP_Interface.gszIP_MessageReceive.pulMessageSize[0]:2
TEST.END

-- Test Case: gszIP_MessageReceive.001
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gszIP_MessageReceive
TEST.NEW
TEST.NAME:gszIP_MessageReceive.001
TEST.COMPOUND_ONLY
TEST.VALUE:IP_Interface.gszIP_MessageReceive.pSocket:<<malloc 1>>
TEST.VALUE:IP_Interface.gszIP_MessageReceive.pPacketBuffer:<<malloc 1>>
TEST.VALUE:IP_Interface.gszIP_MessageReceive.pPacketBuffer[0].ulUnprocessedCharacters:0
TEST.VALUE:IP_Interface.gszIP_MessageReceive.pulMessageSize:<<malloc 1>>
TEST.END

-- Subprogram: gulIP_Address

-- Test Case: BASIS-PATH-001
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gulIP_Address
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 1
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.VALUE:IP_Interface.gulIP_Address.ubA:<<MIN>>
TEST.VALUE:IP_Interface.gulIP_Address.ubB:<<MIN>>
TEST.VALUE:IP_Interface.gulIP_Address.ubC:<<MIN>>
TEST.VALUE:IP_Interface.gulIP_Address.ubD:<<MIN>>
TEST.END

-- Subprogram: gulIP_GetLcpEchoRequestsCounter

-- Test Case: BASIS-PATH-001
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gulIP_GetLcpEchoRequestsCounter
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 1
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.END

-- Subprogram: gulIP_GetPacket

-- Test Case: BASIS-PATH-001
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gulIP_GetPacket
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 2
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (1) if ((IP_RX_SUCCESS) == eStatus && (void *)0 != pPacket) ==> FALSE
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.gulIP_GetPacket.pSocket:<<malloc 1>>
TEST.VALUE:IP_Interface.gulIP_GetPacket.pPacket:<<null>>
TEST.VALUE:IP_Interface.gulIP_GetPacket.ulWaitTime:<<MIN>>
TEST.VALUE:uut_prototype_stubs._nxe_tcp_socket_receive.return:<<MIN>>
TEST.END

-- Test Case: BASIS-PATH-002
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gulIP_GetPacket
TEST.NEW
TEST.NAME:BASIS-PATH-002
TEST.BASIS_PATH:2 of 2
TEST.NOTES:
This is an automatically generated test case.
   Test Path 2
      (1) if ((IP_RX_SUCCESS) == eStatus && (void *)0 != pPacket) ==> TRUE
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.gulIP_GetPacket.pSocket:<<malloc 1>>
TEST.VALUE:IP_Interface.gulIP_GetPacket.pPacket:<<malloc 1>>
TEST.VALUE:IP_Interface.gulIP_GetPacket.ulWaitTime:<<MIN>>
TEST.VALUE:uut_prototype_stubs._nxe_tcp_socket_receive.return:0
TEST.END

-- Subprogram: gulIP_GetSocketInfo

-- Test Case: BASIS-PATH-001
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gulIP_GetSocketInfo
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 1
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.VALUE:IP_Interface.gulIP_GetSocketInfo.pSocket:<<malloc 1>>
TEST.VALUE:IP_Interface.gulIP_GetSocketInfo.pInfo:<<malloc 1>>
TEST.END

-- Subprogram: gulIP_UDPGetPacket

-- Test Case: BASIS-PATH-001
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gulIP_UDPGetPacket
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 2
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (1) if ((IP_RX_SUCCESS) == eStatus && (void *)0 != pPacket) ==> FALSE
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.gulIP_UDPGetPacket.pSocket:<<malloc 1>>
TEST.VALUE:IP_Interface.gulIP_UDPGetPacket.pPacket:<<null>>
TEST.VALUE:IP_Interface.gulIP_UDPGetPacket.ulWaitTime:<<MIN>>
TEST.VALUE:uut_prototype_stubs._nxe_udp_socket_receive.return:<<MIN>>
TEST.END

-- Test Case: BASIS-PATH-002
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gulIP_UDPGetPacket
TEST.NEW
TEST.NAME:BASIS-PATH-002
TEST.BASIS_PATH:2 of 2
TEST.NOTES:
This is an automatically generated test case.
   Test Path 2
      (1) if ((IP_RX_SUCCESS) == eStatus && (void *)0 != pPacket) ==> TRUE
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.gulIP_UDPGetPacket.pSocket:<<malloc 1>>
TEST.VALUE:IP_Interface.gulIP_UDPGetPacket.pPacket:<<malloc 1>>
TEST.VALUE:IP_Interface.gulIP_UDPGetPacket.ulWaitTime:<<MIN>>
TEST.VALUE:uut_prototype_stubs._nxe_udp_socket_receive.return:0
TEST.END

-- Subprogram: gulIP_UDPSourceExtract

-- Test Case: BASIS-PATH-001
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gulIP_UDPSourceExtract
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 1
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.VALUE:IP_Interface.gulIP_UDPSourceExtract.pPacket:<<malloc 1>>
TEST.VALUE:IP_Interface.gulIP_UDPSourceExtract.pulIpAddress:<<malloc 1>>
TEST.VALUE:IP_Interface.gulIP_UDPSourceExtract.pulPort:<<malloc 1>>
TEST.END

-- Subprogram: gvIP_BackgroundProcess

-- Test Case: BASIS-PATH-001
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gvIP_BackgroundProcess
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 2
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (1) while (1U == gfESCIA_GetRxCharacter(&chRxChar)) ==> FALSE
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:uut_prototype_stubs.gfESCIA_GetRxCharacter.return:<<MIN>>
TEST.END

-- Test Case: BASIS-PATH-002-TEMPLATE
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gvIP_BackgroundProcess
TEST.NEW
TEST.NAME:BASIS-PATH-002-TEMPLATE
TEST.BASIS_PATH:2 of 2 (template)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 2
      (1) while (1U == gfESCIA_GetRxCharacter(&chRxChar)) ==> TRUE
   Test Case Generation Notes:
      Cannot set variable to out of range value 1 in branch 1
TEST.END_NOTES:
TEST.END

-- Subprogram: gvIP_GetAppPacketPoolInfo

-- Test Case: BASIS-PATH-001
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gvIP_GetAppPacketPoolInfo
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 1
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.END

-- Subprogram: gvIP_GetFtpPacketPoolInfo

-- Test Case: BASIS-PATH-001
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gvIP_GetFtpPacketPoolInfo
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 1
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.END

-- Subprogram: gvIP_GetNetxPacketPoolInfo

-- Test Case: BASIS-PATH-001
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:gvIP_GetNetxPacketPoolInfo
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 1
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.END

-- Subprogram: vIP_GetPacketPoolInfo

-- Test Case: BASIS-PATH-001
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:vIP_GetPacketPoolInfo
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 3
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (1) if ((void *)0 != pPacketPool && (void *)0 != pInfo) ==> FALSE
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.vIP_GetPacketPoolInfo.pPacketPool:<<null>>
TEST.VALUE:IP_Interface.vIP_GetPacketPoolInfo.pInfo:<<null>>
TEST.END

-- Test Case: BASIS-PATH-002
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:vIP_GetPacketPoolInfo
TEST.NEW
TEST.NAME:BASIS-PATH-002
TEST.BASIS_PATH:2 of 3
TEST.NOTES:
This is an automatically generated test case.
   Test Path 2
      (1) if ((void *)0 != pPacketPool && (void *)0 != pInfo) ==> TRUE
      (2) if (ubCurrentUsage > pInfo->ubPeakUsage) ==> FALSE
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.vIP_GetPacketPoolInfo.pPacketPool:<<malloc 1>>
TEST.VALUE:IP_Interface.vIP_GetPacketPoolInfo.pPacketPool[0].nx_packet_pool_total:<<MIN>>
TEST.VALUE:IP_Interface.vIP_GetPacketPoolInfo.pInfo:<<malloc 1>>
TEST.VALUE:IP_Interface.vIP_GetPacketPoolInfo.pInfo[0].ubPeakUsage:<<MAX>>
TEST.END

-- Test Case: BASIS-PATH-003
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:vIP_GetPacketPoolInfo
TEST.NEW
TEST.NAME:BASIS-PATH-003
TEST.BASIS_PATH:3 of 3
TEST.NOTES:
This is an automatically generated test case.
   Test Path 3
      (1) if ((void *)0 != pPacketPool && (void *)0 != pInfo) ==> TRUE
      (2) if (ubCurrentUsage > pInfo->ubPeakUsage) ==> TRUE
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:IP_Interface.vIP_GetPacketPoolInfo.pPacketPool:<<malloc 1>>
TEST.VALUE:IP_Interface.vIP_GetPacketPoolInfo.pPacketPool[0].nx_packet_pool_total:<<MIN>>
TEST.VALUE:IP_Interface.vIP_GetPacketPoolInfo.pInfo:<<malloc 1>>
TEST.VALUE:IP_Interface.vIP_GetPacketPoolInfo.pInfo[0].ubPeakUsage:<<MIN>>
TEST.END

-- Subprogram: vIP_Invalid_packet_handler

-- Test Case: BASIS-PATH-001
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:vIP_Invalid_packet_handler
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 1
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.END

-- Subprogram: vIP_Link_down_callback

-- Test Case: BASIS-PATH-001
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:vIP_Link_down_callback
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 1
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.VALUE:IP_Interface.vIP_Link_down_callback.pPPP:<<malloc 1>>
TEST.END

-- Subprogram: vIP_Link_up_callback

-- Test Case: BASIS-PATH-001
TEST.UNIT:IP_Interface
TEST.SUBPROGRAM:vIP_Link_up_callback
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 1
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.VALUE:IP_Interface.vIP_Link_up_callback.pPPP:<<malloc 1>>
TEST.END

-- COMPOUND TESTS

TEST.SUBPROGRAM:<<COMPOUND>>
TEST.NEW
TEST.NAME:<<COMPOUND>>.001
TEST.SLOT: "1", "IP_Interface", "gszIP_MessageReceive", "1", "gszIP_MessageReceive.001"
TEST.SLOT: "2", "IP_Interface", "gszIP_MessageReceive", "1", "eState_==_IP_ST_COMPLETE_MESSAGE"
TEST.END
--
