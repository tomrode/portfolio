del commands.tmp
echo options C_COMPILER_CFG_SOURCE PY_CONFIGURATOR >> commands.tmp
echo options C_COMPILER_FAMILY_NAME Green_Hills >> commands.tmp
echo options C_COMPILER_HIERARCHY_STRING Green Hills_PPC_Bare Board_MBX860_mpserv-multi_C >> commands.tmp
echo options C_COMPILER_PY_ARGS  >> commands.tmp
echo options C_COMPILER_TAG PPC_GH_BARE_MBX_MP_MULTI >> commands.tmp
echo options C_COMPILER_VERSION_CMD ccppc -V not.here.c >> commands.tmp
echo options C_COMPILE_CMD ccppc -c -G -w -bsp pxr40 >> commands.tmp
echo options C_COMPILE_EXCLUDE_FLAGS -o^^** >> commands.tmp
echo options C_DEBUG_CMD multi -nosplash -remote \"mpserv ghprobe" -p $(VECTORCAST_DIR)/DATA/green_hills/multi_playback_file_debug.p >> commands.tmp
echo options C_DEFINE_LIST TX_NO_EVENT_INFO TX_ENABLE_EVENT_FILTERS NX_DISABLE_IPV6 NX_MAX_MULTICAST_GROUPS=1 NX_MAX_LISTEN_REQUESTS=4 NX_FTP_MAX_CLIENTS=1 TX_DISABLE_PREEMPTION_THRESHOLD TX_DISABLE_NOTIFY_CALLBACKS TRUCK_TYPE_DEFINITION=TRUCK_TYPE_PC MODULE_TYPE_DEFINITION=MODULE_TYPE_SMALL_DV GEN_TYPE_DEFINITION=GEN_TYPE_GEN1 >> commands.tmp
echo options C_EDG_FLAGS -w --c --ghs --define_macro=restrict= >> commands.tmp
echo options C_EXECUTE_CMD $(VECTORCAST_DIR)/DATA/green_hills/multi_execution_flash.bat multi -nosplash C:\\ghs\\comp_201654\\target\\ppc\\pxr40\\mpserv_standard.mbs ghprobe >> commands.tmp
echo options C_LINKER_VERSION_CMD elxr -V >> commands.tmp
echo options C_LINK_CMD ccppc -G -bsp pxr40 >> commands.tmp
echo options C_LINK_OPTIONS memory.ld standalone_config.ld standalone_romdebug.ld -LC:\\VCAST\\DATA\\green_hills\\flash_pxr40 -lstartup -lsys -lboardinit >> commands.tmp
echo options C_OUTPUT_FLAG -o^^ >> commands.tmp
echo options C_PREPROCESS_CMD ccppc -E -C -bsp pxr40 >> commands.tmp
echo options C_PREPROCESS_FILE  >> commands.tmp
echo options EVENT_LIMIT 500 >> commands.tmp
echo options RANGE_CHECK NONE >> commands.tmp
echo options VCAST_ASSEMBLY_FILE_EXTENSIONS asm s >> commands.tmp
echo options VCAST_COMPILER_SUPPORTS_CPP_CASTS FALSE >> commands.tmp
echo options VCAST_COVERAGE_FOR_AGGREGATE_INIT FALSE >> commands.tmp
echo options VCAST_DISABLE_CPP_EXCEPTIONS TRUE >> commands.tmp
echo options VCAST_DISPLAY_UNINST_EXPR FALSE >> commands.tmp
echo options VCAST_ENABLE_FUNCTION_CALL_COVERAGE FALSE >> commands.tmp
echo options VCAST_ENVIRONMENT_FILES C:\\ghs\\comp_201654\\target\\ppc\\pxr40\\memory.ld,C:\\ghs\\comp_201654\\target\\ppc\\pxr40\\standalone_config.ld,C:\\ghs\\comp_201654\\target\\ppc\\pxr40\\standalone_romdebug.ld >> commands.tmp
echo options VCAST_EXECUTE_WITH_STDOUT TRUE >> commands.tmp
echo options VCAST_HAS_LONGLONG FALSE >> commands.tmp
echo options VCAST_NO_STDIN TRUE >> commands.tmp
echo options VCAST_REMOVE_PREPROCESSOR_COMMENTS FALSE >> commands.tmp
echo options VCAST_RPTS_DEFAULT_FONT_FACE Arial(6) >> commands.tmp
echo options VCAST_STDIO FALSE >> commands.tmp
echo options VCAST_UNIT_TYPE UUT >> commands.tmp
echo options WHITEBOX YES >> commands.tmp
echo clear_default_source_dirs  >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\CanStackSrc\CANopen\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\CanStackSrc\CRT\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\CanStackSrc\DLLfl32c\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\CanStackSrc\DLLmul\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\CanStackSrc\Include\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\CanStackSrc\LSS\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\CommonSource\go_driver\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\CommonSource\switch_board\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\threadx_pxr40\ppc55xx_vle\green\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\BusHeaders\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\parameters\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\sharedutils\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\SIMu1\CalibrationSIM\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\SIMu1\CalibrationSIM\referenced_model_includes >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\SIMu1\CalibrationSIMbuild_ert_rtw\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\SIMu1\commonSIM\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\SIMu1\commonSIMbuild_ert_rtw\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\SIMu1\EnergySourceSIM\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\SIMu1\EnergySourceSIMbuild_ert_rtw\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\SIMu1\HmsSim_Brake\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\SIMu1\HmsSim_BrakeBuild_ert_rtw\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\SIMu1\HmsSim_Communication\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\SIMu1\HmsSim_CommunicationBuild_ert_rtw\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\SIMu1\HmsSim_Ed\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\SIMu1\HmsSim_EdBuild_ert_rtw\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\SIMu1\HmsSim_Gpd\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\SIMu1\HmsSim_GpdBuild_ert_rtw\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\SIMu1\HmsSim_HcmTcm\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\SIMu1\HmsSim_HcmTcmBuild_ert_rtw\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\SIMu1\HmsSim_Hydraulic\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\SIMu1\HmsSim_HydraulicBuild_ert_rtw\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\SIMu1\HmsSim_PowerSupplies\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\SIMu1\HmsSim_PowerSuppliesBuild_ert_rtw\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\SIMu1\HmsSim_Steering\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\SIMu1\HmsSim_SteeringBuild_ert_rtw\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\SIMu1\HmsSim_Traction\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\SIMu1\HmsSim_TractionBuild_ert_rtw\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\SIMu1\HmsSim_VcmInputs\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\SIMu1\HmsSim_VcmInputsBuild_ert_rtw\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\SIMu1\hydraulicSIM\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\SIMu1\hydraulicSIMbuild_ert_rtw\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\SIMu1\SteeringSIM\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\SIMu1\SteeringSIMbuild_ert_rtw\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\SIMu1\TractionSIM\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\SIMu1\TractionSIMbuild_ert_rtw\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\SIMu1\VehicleFBSIMbuild_ert_rtw\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\SIMu1\Vehicle_fb_processing_SIM\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen_API\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\CANopen\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\diagnostics\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\drivers\accel\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\drivers\aecp\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\drivers\becp\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\drivers\can\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\drivers\comm\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\drivers\etpu2\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\drivers\file_sys\api\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\drivers\fram\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\drivers\gyro\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\drivers\io\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\drivers\xcp\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\drivers\xcp\cfg\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\drivers\xcp\usr\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\ftp\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\general\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\hal\MPC5674F\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\hal\MPC5674F\ETPU2\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\interface\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\interface\codrv\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\nvstorage\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\nvstorage\fram\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\parameters\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\parameters\Param_Gen1 >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\utilities\ >> commands.tmp
echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\tgt\ >> commands.tmp
echo environment build IP_INTERFACE_INTEGRATION.env >> commands.tmp
echo /E:IP_INTERFACE_INTEGRATION tools script run IP_INTERFACE_INTEGRATION.tst >> commands.tmp
echo /E:IP_INTERFACE_INTEGRATION execute batch >> commands.tmp
echo /E:IP_INTERFACE_INTEGRATION reports custom management IP_INTERFACE_INTEGRATION_management_report.html >> commands.tmp
"%VECTORCAST_DIR%\CLICAST"  /L:C tools execute commands.tmp false
