# Name:      EmailTestResults.py
#
# Purpose:   This python script configures an email using the MIME( Multi-Purpose Internet Mail Extensions) protocol,  
#            and then establishes an SMPT(Simple Mail Transfer Protocol) between servers. This script also makes a copy    
#            of the auto generated test folder and clones it to the EngSTL1 server. The resulting emails are sent to the  
#            list of recipents with the "VCAST Test Times.txt" as an attachment and a message of where the results are stored 
#
# Author:    Thomas Rode
#
# Options:   None
#
# Note:      The host email servers uses Gmail and this might change to a Crown email in the future  
#
# Python Version: 2.7.11
#          
# Reference: https://www.python.org/ (open source)
#

import os
import os.path
import smtplib, ldap
from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import shutil
import sys
import errno
import time
from RcvEmailGmail import RcvEmailGmail


COMMASPACE = ', '
Server = "ldap://corp208.us.crownlift.net"
sender = 'CrownTmlVcast@crown.com'
GmailSender = 'CrownTmlVcast@gmail.com'
GmailPassword = 'crown123'
Imap4SslGmail = 'imap.gmail.com'
    
CrownNetwork = 'TML_Build_Server@crown.com' 
CrownPswd = ''
CrownEmail = 'namail.crown.com'
recipients = ['tom.rode@crown.com','michael.kovach@crown.com','walter.meharg@crown.com', 'robert.pham@crown.com','chris.graunke@crown.com', 'nick.thobe@crown.com', 'gregg.schmitmeyer']

rtc_outfile ='C:/Embedded/iRTC Specific Files/antTmlVcmMasterBuildProperties.out'
definition1 = 'buildDefinitionId=TmlVcmMasterBuildDef001'
definition2 = 'buildDefinitionId=TmlVcmMasterBuildDef002'

SECINMIN = 60 

def email_FoldersMissing(Path):
    
    # Create the enclosing (outer) message
    outer = MIMEMultipart()
    outer['Subject'] = 'VECTOR CAST AUTOMATED TEST ABORTED'
    outer['To'] = COMMASPACE.join(recipients)
    outer['From'] = sender
    outer.preamble = 'You will not see this in a MIME-aware mail reader.\n'
    
    # Create a text/plain message
    msg = MIMEText('Regression error, missing file folders or .C files please see "MissingFiles.txt or Missing_files_list.txt" located at:  ' + Path)
    outer.attach(msg)

    # Put together message
    composed = outer.as_string()

    # Authenticate via LDAP    
    l = ldap.initialize(Server)
    l.protocol_version = 3
    l.set_option(ldap.OPT_REFERRALS, 0)
    l.simple_bind_s(CrownNetwork, CrownPswd)
    
    # Send the email
    server = smtplib.SMTP(CrownEmail, 25) 
    text = msg.as_string()
    server.sendmail(sender, recipients, composed)
    server.quit()


def email(Path, TargetOrSim):

    # Copy folder and get new destination path
    dest_path = Path
       
    # Create the enclosing (outer) message, deterime which message in subject 
    outer = MIMEMultipart()

    #Open .OUT file 
    if definition1 in open(rtc_outfile,'r+').read():
        print("Found TmlVcmMasterBuildDef001")
        output = 'MAIN DEVELOPMENT STREAM' 
    elif definition2 in open(rtc_outfile,'r+').read():
        print("Found TmlVcmMasterBuildDef002")
        output = 'QUALITY STREAM'
    else:
        print("Build definition not found")
        output = 'Undefined'  
    
    # Determine if On-Target or Sim
    if TargetOrSim == True:
        outer['Subject'] = ('VECTOR CAST AUTOMATED SIMULATION TEST RESULTS ' + output)
    else:
        outer['Subject'] = ('VECTOR CAST AUTOMATED ON-TARGET TEST RESULTS ' + output)
    outer['To'] = COMMASPACE.join(recipients)
    outer['From'] = sender
    outer.preamble = 'You will not see this in a MIME-aware mail reader.\n'
    
    # get the path of the folder this lives in 
    attachments = [os.getcwd()]
    
    # Create a text/plain message
    msg = MIMEText('Test results stored on server EngSTL1 in folder:  ' + dest_path)
    outer.attach(msg)
     
    # List of attachments to email, this may include HMTL's in the future
    emailfiles = ['VCAST Test Times.txt', \
             'Cumulative_Results.xlsx']
 
    # Add the attachments to the message
    for file in attachments:
        try:
            for line in emailfiles:              
                fp = open(line , 'rb')            
                msg = MIMEBase('application', "octet-stream")
                msg.set_payload(fp.read()) 
                encoders.encode_base64(msg)
                msg.add_header('Content-Disposition', 'attachment', filename=line)
                outer.attach(msg)
                #msg.attach(MIMEText(attachments, 'plain'))
        except:
            print("Unable to open one of the attachments. Error: ", sys.exc_info()[0])
            raise

    composed = outer.as_string()

    # Authenticate via LDAP
    l = ldap.initialize(Server)
    l.protocol_version = 3
    l.set_option(ldap.OPT_REFERRALS, 0)
    l.simple_bind_s(CrownNetwork, CrownPswd)
    
    # Send the email
    server = smtplib.SMTP(CrownEmail, 25) 
    text = msg.as_string()
    server.sendmail(sender, recipients, composed)
    server.quit()
    
    
def copy_files():
    # Set this false so this function may run twice
    FoldersCopied = False
    
    # get the path of the folder this lives in 
    attachments = [os.getcwd()]

    # email body message, store the path in a text file 
    f = open('EmailPathFolderResults.txt', "w")
    f.writelines(attachments)
    f.close()
    
    # get path from text file to be displayed in email message
    fp = open('EmailPathFolderResults.txt', 'rb')  
    
    # Make a copy of this directory name to "EngSTL1", truncate first 26 characters C:\vcast-buildsystem-orig\
    RegressionResultfolder = fp.read()
    sourcepath = RegressionResultfolder
    RegressionResultfolder = RegressionResultfolder[26:]
    dest_path = ('Y:\\vcast-buildsystem-orig\\' + RegressionResultfolder )
    fp.close()

    # email body message, store the path in a text file 
    f = open('EmailPathFolderResults.txt', "w")
    f.writelines(dest_path)
    f.close()
    
    # New Folder made in EngSTL1
    try:
        os.makedirs(dest_path)
         # Copy the files from source to destination 
        for filename in os.listdir(sourcepath):
            if os.path.isfile(os.path.join(sourcepath, filename)):
                shutil.copy(os.path.join(sourcepath, filename), os.path.join(dest_path, filename))

        # Copy the folders from source to destination
        for item in os.listdir(sourcepath):
            s = os.path.join(sourcepath, item)
            d = os.path.join(dest_path, item)
            if os.path.isdir(s):
                shutil.copytree(s, d, False, None)
            else:
                shutil.copy2(s, d)       
    except:
        print("New Folder now copied to location: EngSTL1 ")
        FoldersCopied = True

    return FoldersCopied

def email_developer(Path, TargetOrSim):
 
    DevRecipients_Completelist = []

    # Call external function that worked before for proper recipient developer
    DevRecipients_Completelist, RcvEmailErrorCode  = RcvEmailGmail()
    
    # Copy folder and get new destination path
    dest_path = Path
       
    # Create the enclosing (outer) message, deterime which message in subject 
    outer = MIMEMultipart()

    #Open .OUT file 
    if definition1 in open(rtc_outfile,'r+').read():
        print("Found TmlVcmMasterBuildDef001")
        output = 'MAIN DEVELOPMENT STREAM' 
    elif definition2 in open(rtc_outfile,'r+').read():
        print("Found TmlVcmMasterBuildDef002")
        output = 'QUALITY STREAM'
    else:
        print("Build definition not found")
        output = 'Undefined'  
    
    # Determine if On-Target or Sim
    if TargetOrSim == True:
        outer['Subject'] = ('VECTOR CAST AUTOMATED SIMULATION TEST RESULTS ' + output)
    else:
        outer['Subject'] = ('VECTOR CAST AUTOMATED ON-TARGET TEST RESULTS ' + output)
    outer['To'] = COMMASPACE.join(DevRecipients_Completelist)
    outer['From'] = sender
    outer.preamble = 'You will not see this in a MIME-aware mail reader.\n'
    
    # get the path of the folder this lives in 
    attachments = [os.getcwd()]
    
    # Create a text/plain message if no error from email
    if RcvEmailErrorCode == 1:
        msg = MIMEText('Jazz Server Received Gmail Email Error')
    elif RcvEmailErrorCode == 2:
        msg = MIMEText('Jazz Server Out Data Error')
    else:
        msg = MIMEText('Test results stored on server EngSTL1 in folder:  ' + dest_path)
    outer.attach(msg)
     
    # List of attachments to email, this may include HMTL's in the future
    emailfiles = ['VCAST Test Times.txt', \
                  'Cumulative_Results.xlsx']
 
    # Add the attachments to the message
    for file in attachments:
        try:
            for line in emailfiles:              
                fp = open(line , 'rb')            
                msg = MIMEBase('application', "octet-stream")
                msg.set_payload(fp.read()) 
                encoders.encode_base64(msg)
                msg.add_header('Content-Disposition', 'attachment', filename=line)
                outer.attach(msg)
                #msg.attach(MIMEText(attachments, 'plain'))
        except:
            print("Unable to open one of the attachments. Error: ", sys.exc_info()[0])
            raise

    composed = outer.as_string()

    # Authenticate via LDAP
    l = ldap.initialize(Server)
    l.protocol_version = 3
    l.set_option(ldap.OPT_REFERRALS, 0)
    l.simple_bind_s(CrownNetwork, CrownPswd)
    
    # Send the email
    server = smtplib.SMTP(CrownEmail, 25) 
    text = msg.as_string()
    server.sendmail(sender, recipients, composed)
    server.quit()
    

def email_main(Initiate_email, TargetOrSim):
  
    # Copy this folder from local "C" drive to "EngSTL1"
    FldsCopy = copy_files()
  
    if Initiate_email == True and FldsCopy == True:
        # get path from text file to be displayed in email message
        fp = open('EmailPathFolderResults.txt', 'rb')
        dest_path = fp.read()
        fp.close()
     
        # email the path and times to Core Management        
        email(dest_path, TargetOrSim)

        # Added 3 minute delay for Jazz Server email to post to Gmail 
        #time.sleep(SECINMIN * 3)

        # Disabled as of 16 Nov 2017 
        # email the path and times to Individual Developer, There may need to be a delay introduced so this function does not enact on old data  
        #email_developer(dest_path, TargetOrSim)

if __name__ == '__main__':
    email_main(True, False)
