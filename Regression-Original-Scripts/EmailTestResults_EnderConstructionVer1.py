# Name:      EmailTestResults.py
#
# Purpose:   This python script configures an email using the MIME( Multi-Purpose Internet Mail Extensions) protocol,  
#            and then establishes an SMPT(Simple Mail Transfer Protocol) between servers. This script also makes a copy    
#            of the auto generated test folder and clones it to the EngSTL1 server. The resulting emails are sent to the  
#            list of recipents with the "VCAST Test Times.txt" as an attachment and a message of where the results are stored 
#
# Author:    Thomas Rode
#
# Options:   None
#
# Note:      The host email servers uses Gmail and this might change to a Crown email in the future  
#
# Python Version: 2.7.11
#          
# Reference: https://www.python.org/ (open source)
#

import os
import os.path
import smtplib, ldap
from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import shutil
import sys
import errno
import time
import imaplib
import email


COMMASPACE = ', '
Server = "ldap://corp208.us.crownlift.net"
sender = 'CrownTmlVcast@crown.com'
GmailSender = 'CrownTmlVcast@gmail.com'
GmailPassword = 'crown123'
Imap4SslGmail = 'imap.gmail.com'
    
CrownNetwork = 'TML_Build_Server@crown.com' 
CrownPswd = ''
CrownEmail = 'namail.crown.com'
recipients = ['tom.rode@crown.com']#,'michael.kovach@crown.com','walter.meharg@crown.com', 'robert.pham@crown.com','chris.graunke@crown.com', 'nick.thobe@crown.com', 'gregg.schmitmeyer']

rtc_outfile ='C:/Embedded/iRTC Specific Files/antTmlVcmMasterBuildProperties.out'
definition1 = 'buildDefinitionId=TmlVcmMasterBuildDef001'
definition2 = 'buildDefinitionId=TmlVcmMasterBuildDef002'

def email_FoldersMissing(Path):
    
    # Create the enclosing (outer) message
    outer = MIMEMultipart()
    outer['Subject'] = 'VECTOR CAST AUTOMATED TEST ABORTED'
    outer['To'] = COMMASPACE.join(recipients)
    outer['From'] = sender
    outer.preamble = 'You will not see this in a MIME-aware mail reader.\n'
    
    # Create a text/plain message
    msg = MIMEText('Regression did not Execute due to missing file folders please see "MissingFiles.txt" located at:  ' + Path)
    outer.attach(msg)

    # Put together message
    composed = outer.as_string()

    # Authenticate via LDAP    
    l = ldap.initialize(Server)
    l.protocol_version = 3
    l.set_option(ldap.OPT_REFERRALS, 0)
    l.simple_bind_s(CrownNetwork, CrownPswd)
    
    # Send the email
    server = smtplib.SMTP(CrownEmail, 25) 
    text = msg.as_string()
    server.sendmail(sender, recipients, composed)
    server.quit()


def email(Path, TargetOrSim):

    # Copy folder and get new destination path
    dest_path = Path
       
    # Create the enclosing (outer) message, deterime which message in subject 
    outer = MIMEMultipart()

    #Open .OUT file 
    if definition1 in open(rtc_outfile,'r+').read():
        print("Found TmlVcmMasterBuildDef001")
        output = 'MAIN DEVELOPMENT STREAM' 
    elif definition2 in open(rtc_outfile,'r+').read():
        print("Found TmlVcmMasterBuildDef002")
        output = 'QUALITY STREAM'
    else:
        print("Build definition not found")
        output = 'Undefined'  
    
    # Determine if On-Target or Sim
    if TargetOrSim == True:
        outer['Subject'] = ('VECTOR CAST AUTOMATED SIMULATION TEST RESULTS ' + output)
    else:
        outer['Subject'] = ('VECTOR CAST AUTOMATED ON-TARGET TEST RESULTS ' + output)
    outer['To'] = COMMASPACE.join(recipients)
    outer['From'] = sender
    outer.preamble = 'You will not see this in a MIME-aware mail reader.\n'
    
    # get the path of the folder this lives in 
    attachments = [os.getcwd()]
    
    # Create a text/plain message
    msg = MIMEText('Test results stored on server EngSTL1 in folder:  ' + dest_path)
    outer.attach(msg)
     
    # List of attachments to email, this may include HMTL's in the future
    emailfiles = ['VCAST Test Times.txt', \
             'Cumulative_Results.xlsx']
 
    # Add the attachments to the message
    for file in attachments:
        try:
            for line in emailfiles:              
                fp = open(line , 'rb')            
                msg = MIMEBase('application', "octet-stream")
                msg.set_payload(fp.read()) 
                encoders.encode_base64(msg)
                msg.add_header('Content-Disposition', 'attachment', filename=line)
                outer.attach(msg)
                #msg.attach(MIMEText(attachments, 'plain'))
        except:
            print("Unable to open one of the attachments. Error: ", sys.exc_info()[0])
            raise

    composed = outer.as_string()

    # Authenticate via LDAP
    l = ldap.initialize(Server)
    l.protocol_version = 3
    l.set_option(ldap.OPT_REFERRALS, 0)
    l.simple_bind_s(CrownNetwork, CrownPswd)
    
    # Send the email
    server = smtplib.SMTP(CrownEmail, 25) 
    text = msg.as_string()
    server.sendmail(sender, recipients, composed)
    server.quit()
    
    
def copy_files():
    # Set this false so this function may run twice
    FoldersCopied = False
    
    # get the path of the folder this lives in 
    attachments = [os.getcwd()]

    # email body message, store the path in a text file 
    f = open('EmailPathFolderResults.txt', "w")
    f.writelines(attachments)
    f.close()
    
    # get path from text file to be displayed in email message
    fp = open('EmailPathFolderResults.txt', 'rb')  
    
    # Make a copy of this directory name to "EngSTL1", truncate first 26 characters C:\vcast-buildsystem-orig\
    RegressionResultfolder = fp.read()
    sourcepath = RegressionResultfolder
    RegressionResultfolder = RegressionResultfolder[26:]
    dest_path = ('Y:\\vcast-buildsystem-orig\\' + RegressionResultfolder )
    fp.close()

    # email body message, store the path in a text file 
    f = open('EmailPathFolderResults.txt', "w")
    f.writelines(dest_path)
    f.close()
    
    # New Folder made in EngSTL1
    try:
        os.makedirs(dest_path)
         # Copy the files from source to destination 
        for filename in os.listdir(sourcepath):
            if os.path.isfile(os.path.join(sourcepath, filename)):
                shutil.copy(os.path.join(sourcepath, filename), os.path.join(dest_path, filename))

        # Copy the folders from source to destination
        for item in os.listdir(sourcepath):
            s = os.path.join(sourcepath, item)
            d = os.path.join(dest_path, item)
            if os.path.isdir(s):
                shutil.copytree(s, d, False, None)
            else:
                shutil.copy2(s, d)       
    except:
        print("New Folder now copied to location: EngSTL1 ")
        FoldersCopied = True

    return FoldersCopied

def email_developer(Path, TargetOrSim):

    DevRecipients_list = []
    DevRecipients_Completelist = []
    RcvEmailErrorOccured = False
    OutDataErrorOccured = False

    # Call Gmail to see if Jazz build "TmlVcmMasterBuildDef00x is there use a flag to see if a exception happened, False means no errors fill up developers list
    if RcvEmailGmail() == False:

        # Collect current Developers that delivered out of OutDataGmail.txt
        try:
            # Add this in email developer function, remove \r\n and replace a " " with a "."witin list
            with open('OutDataGmail.txt', 'r') as infile:
                for line in infile:
                    DevRecipients_list.append(line)
                
                DevRecipients_list = [x.replace("\r\n","") for x in DevRecipients_list]
                DevRecipients_list = [x.replace(" ",".") for x in DevRecipients_list]

                # add '@crown.com' to DevRecipients_Complete
                for line in DevRecipients_list:
                    line = line + '@crown.com'
                    DevRecipients_Completelist.append(line)

                #print("Here is the DevRecipients_list\n")
                print(DevRecipients_Completelist)
        except:
            print("Missing OutDataGmail.txt")
            OutDataErrorOccured = True

    else:
        # error occured so default to Tom Rode as recipient
        DevRecipients_Completelist = ['tom.rode@crown.com']
        RcvEmailErrorOccured = True
        
    
    # Copy folder and get new destination path
    dest_path = Path
       
    # Create the enclosing (outer) message, deterime which message in subject 
    outer = MIMEMultipart()

    #Open .OUT file 
    if definition1 in open(rtc_outfile,'r+').read():
        print("Found TmlVcmMasterBuildDef001")
        output = 'MAIN DEVELOPMENT STREAM' 
    elif definition2 in open(rtc_outfile,'r+').read():
        print("Found TmlVcmMasterBuildDef002")
        output = 'QUALITY STREAM'
    else:
        print("Build definition not found")
        output = 'Undefined'  
    
    # Determine if On-Target or Sim
    if TargetOrSim == True:
        outer['Subject'] = ('VECTOR CAST AUTOMATED SIMULATION TEST RESULTS ' + output)
    else:
        outer['Subject'] = ('VECTOR CAST AUTOMATED ON-TARGET TEST RESULTS ' + output)
    outer['To'] = COMMASPACE.join(recipients)
    outer['From'] = sender
    outer.preamble = 'You will not see this in a MIME-aware mail reader.\n'
    
    # get the path of the folder this lives in 
    attachments = [os.getcwd()]
    
    # Create a text/plain message if no error from email
    if RcvEmailErrorOccured == True:
        msg = MIMEText('Jazz Server Received Email Error')
    elif OutDataErrorOccured == True:
        msg = MIMEText('Jazz Server Out Data Error')
    else:
        msg = MIMEText('Test results stored on server EngSTL1 in folder:  ' + dest_path)
    outer.attach(msg)
     
    # List of attachments to email, this may include HMTL's in the future
    emailfiles = ['VCAST Test Times.txt']#, \
                  #'Cumulative_Results.xlsx']
 
    # Add the attachments to the message
    for file in attachments:
        try:
            for line in emailfiles:              
                fp = open(line , 'rb')            
                msg = MIMEBase('application', "octet-stream")
                msg.set_payload(fp.read()) 
                encoders.encode_base64(msg)
                msg.add_header('Content-Disposition', 'attachment', filename=line)
                outer.attach(msg)
                #msg.attach(MIMEText(attachments, 'plain'))
        except:
            print("Unable to open one of the attachments. Error: ", sys.exc_info()[0])
            raise

    composed = outer.as_string()

    # Authenticate via LDAP
    l = ldap.initialize(Server)
    l.protocol_version = 3
    l.set_option(ldap.OPT_REFERRALS, 0)
    l.simple_bind_s(CrownNetwork, CrownPswd)
    
    # Send the email
    server = smtplib.SMTP(CrownEmail, 25) 
    text = msg.as_string()
    server.sendmail(sender, recipients, composed)
    server.quit()
    

def RcvEmailGmail():

    Developer_list = []
    GmailError = True
    
    try:     
        #Connect over an encrypted SSL socket to gmail host
        mail = imaplib.IMAP4_SSL(Imap4SslGmail)
        mail.login(GmailSender, GmailPassword)
        mail.list()

        # Out: list of "folders" aka labels in gmail.
        mail.select("inbox") # connect to inbox.
        result, data = mail.search(None, "ALL")

        # data is a list.
        ids = data[0]

        # ids is a space separated string
        id_list = ids.split()
    
        # get the latest   
        latest_email_id = id_list[-1] 

        # fetch the email body (RFC822) for the given ID
        result, data = mail.fetch(latest_email_id, "(RFC822)") 

        # here's the body, which is raw text of the whole email, ncluding headers and alternate payloads
        raw_email = data[0][1] 
    
        #print raw_email
        msg = email.message_from_string(raw_email)

        #print msg['From']
        print msg.get_payload(decode=True)

        # write this complete email to file 
        f = open('InDataGmail.txt', "w")
        f.writelines(msg.get_payload(decode=True))
        f.close()
        
        with open('InDataGmail.txt', 'r') as infile:
            for line in infile:
                if 'Jeff Campbell'in line or 'Linden R. Peterson'in line or 'Don Bartlett'in line or \
                   'Chris Graunke'in line or 'Mike Corbett'in line or 'Gupta, Subhanshu'in line or \
                   'Padsmanabha Manjunatha' in  line or 'Alexander Mueller'in line or 'Niels Franzen'in line or 'Tom Rode'in line : # remove me once finished
                    print (line)
                    Developer_list.append(line)
            
            # write the developers from email to file 
            f = open('OutDataGmail.txt', "w")
            f.writelines(Developer_list)
            f.close()

            #Indicate No Error
            GmailError = False
            
    except:
        print("In RcvEmailGmail() Error With Gmail")

    return GmailError
 

def email_main(Initiate_email, TargetOrSim):
  
    # Copy this folder from local "C" drive to "EngSTL1"
    FldsCopy = copy_files()
  
    if Initiate_email == True and FldsCopy == True:
        # get path from text file to be displayed in email message
        fp = open('EmailPathFolderResults.txt', 'rb')
        dest_path = fp.read()
        fp.close()
     
        # email the path and times to Core Management        
        #email(dest_path, TargetOrSim)
       
        # email the path and times to Individual Developer, There may need to be a delay introduced so this function does not enact on old data  
        email_developer(dest_path, TargetOrSim)

if __name__ == '__main__':
    email_main(True, False)
