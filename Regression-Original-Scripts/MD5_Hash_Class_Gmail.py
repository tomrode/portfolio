# Name:      MD5_Hash_Class.py
#
# Purpose:   This python script will calculate the MD5 hash for a given ".C" file, it will record if a difference has been made
#            from a specified set of Hashes. Also it will determine if given C file exists or not and return appropriate batch file to run.
#        
#
# Author:    Thomas Rode
#
# Options:   MD5HashDriver(SelectReadWrite) determines if Hash needed to be saved or read  
#
# Return:    Test file list that had a diffence in Hash from a previous baseline
# 
# Note:      This driver can be used in a standalone script as well to read or recalculate Hashes if need be. Also care needs to be taken when modifying 
#            all_regressions[] so its up to date and correct.
#
# Python Version: 2.7.11
#          
# Reference: https://www.python.org/ (open source)
#

import os, sys
import hashlib
#from EmailTestResults import email_FoldersMissing


class MD5Hash(object):

  # Constructor
  def __init__(self, C_fileToHash, HashValfile, CurrentHash, SavedHash):
    # instance variables
    self.C_fileToHash = C_fileToHash
    self.HashValfile = HashValfile
    self.CurrentHash = CurrentHash
    self.SavedHash = SavedHash

  # Accesser Methods (getters) and Mutator Method (setters)      
  def WriteHash(self):    
    f = open(self.HashValfile, "w")
    fhash = self.CurrentHash
    #print("This is the Saved Hash:", str(fhash))
    f.write(fhash)
    f.close()

  def ReadHash(self):
    f = open(self.HashValfile, 'r')
    self.SavedHash = f.read()
    #print("This is what was Saved Hash:", str(self.SavedHash))
    f.close()
    return self.SavedHash

  def MD5_Hash_Calc(self):
    BLOCKSIZE = 65536
    hasher = hashlib.md5()
    with open(self.C_fileToHash, 'rb') as afile:
        buf = afile.read(BLOCKSIZE)
        while len(buf) > 0:
            hasher.update(buf)
            buf = afile.read(BLOCKSIZE)
    self.CurrentHash = hasher.hexdigest()
    #print("This is the Current Hash:", str(self.CurrentHash))
    return self.CurrentHash


def MD5HashDriver(SelectReadWrite):
  
  # MD5 txt file suffix
  MD5HASHtxt = '_MD5Hash.txt'
  
  # Folder location for all Hashes
  md5_output_path =   'C:/vcast-buildsystem-orig/Regression-Original-Current-Gmail/'

  # Folder where Gmail message text file saved at
  GmailMessageToMD5HASH = 'C:/vcast-buildsystem-orig/Regression-Original-Current-Gmail/'

       
#while(1):
  # selection will possible use a text file to determine if it need to be re-hashed after a base line was ran on target once
  #selection = str(input("Type '1' to calculate and save hash, '2' to see if saved hash and current has match: "))
  selection = SelectReadWrite
    
  # Writing Hash for email message ititially only needed once in theory
  if selection == '3':
      print("Here I Am in 3")
      # assingment of the class to this variable with correspond file and output MD5 .txt file
      regression = MD5Hash((GmailMessageToMD5HASH + 'GmailMessage.txt'), (md5_output_path + 'GmailMessageNowHashed' + MD5HASHtxt) , None, None) 
      regression.MD5_Hash_Calc()
      regression.WriteHash()

  # Writing Hash for email message ititially only needed once in theory
  if selection == '4':
      print("Here I Am in 4")
      # assingment of the class to this variable with correspond file and output MD5 .txt file
      regression = MD5Hash((GmailMessageToMD5HASH + 'GmailMessage.txt'), (md5_output_path + 'GmailMessageNowHashed' + MD5HASHtxt) , None, None) 

      # calculate and read saved Hashes 
      CurrentHash = regression.MD5_Hash_Calc()
      SavedHash = regression.ReadHash()

      # is the current and saved hash value the same the remove the test from the overall list?
      if CurrentHash == SavedHash:
          print("Current and saved Hashes match")
          return True
      # re-calcultate new hash 
      else:
          print("Current and saved Hashes DO NOT match")
          regression.MD5_Hash_Calc()
          regression.WriteHash()
          return False 
        
        
if __name__ == '__main__':
  MD5HashDriver()
    
  
