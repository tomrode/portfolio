# Name:      RcvEmailGmail.py
#
# Purpose:   This python script utilizes Gmail to get the Jazz build "TmlVcmMasterBuildDef00x to determine which developer
#            Delivered a change set.  
#
# Author:    Thomas Rode
#
# Options:   none
# Returns:   DevRecipients_Completelist: List of developers that submitted Change set From Gmail account.
#            RcvEmailErrorCode: which errors are present if any. 
#
# Note:      Could not place this function in EmailTestResults.py due to importing conflict
#
# Python Version: 2.7.11
#          
# Reference: https://www.python.org/ (open source)
#

import imaplib
import email
from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import time
import os
import shutil
import ldap
import smtplib
from MD5_Hash_Class_Gmail import MD5HashDriver
from ParseHtmlForManage import ParseHtml

COMMASPACE = ', '
Server = "ldap://corp208.us.crownlift.net"
sender = 'CrownTmlVcast@crown.com'
CrownNetwork = 'TML_Build_Server@crown.com' 
CrownPswd = ''
CrownEmail = 'namail.crown.com'

GmailSender = 'CrownTmlVcast@gmail.com'
GmailPassword = 'crown123'
Imap4SslGmail = 'imap.gmail.com'
EmailMessageDestination  = 'C:/vcast-buildsystem-orig/Regression-Original-Current-Gmail/'
EmailDeveloperDataSource = 'C:/vcast-buildsystem-orig/Regression-Original-Scripts/EmailDeveloperData/'

SelectReadGmail = '4'

rtc_outfile ='C:/Embedded/iRTC Specific Files/antTmlVcmMasterBuildProperties.out'
definition1 = 'buildDefinitionId=TmlVcmMasterBuildDef001'
definition2 = 'buildDefinitionId=TmlVcmMasterBuildDef002'
source_path_emaildevelopers = 'C:/vcast-buildsystem-orig/Regression-Original-Scripts/EmailDeveloperData'

def RcvEmailGmail():
                       
    Developer_list = []
    DevRecipients_list = []
    DevRecipients_Completelist = []
    GmailError = True
    RcvEmailErrorCode = 0
    HashMatch = True
    try:
        #Connect over an encrypted SSL socket to gmail host
        mail = imaplib.IMAP4_SSL(Imap4SslGmail)
        mail.login(GmailSender, GmailPassword)
        mail.list()

        # Out: list of "folders" aka labels in gmail.
        mail.select("inbox") # connect to inbox.
        result, data = mail.search(None, "ALL")

        # data is a list.
        ids = data[0]

        # ids is a space separated string
        id_list = ids.split()
    
        # get the latest   
        latest_email_id = id_list[-1] 

        # fetch the email body (RFC822) for the given ID
        result, data = mail.fetch(latest_email_id, "(RFC822)") 

        # here's the body, which is raw text of the whole email, ncluding headers and alternate payloads
        raw_email = data[0][1] 
    
        #print raw_email
        msg = email.message_from_string(raw_email)

        #print msg['From']
        #print (msg.get_payload(decode=True))
        MsgToHash = (msg.get_payload(decode=True))
        print(MsgToHash)

        # change directory to specified
        os.chdir(EmailMessageDestination) 
        f = open("GmailMessage.txt", "w")
        f.write(MsgToHash)
        f.close()

        # this point run the HASH to determine if need to finish process to email developer
        HashMatch = MD5HashDriver(SelectReadGmail)
                
        # write this complete email to file 
        f = open('InDataGmail.txt', "w")
        f.writelines(msg.get_payload(decode=True))
        f.close()

        with open('InDataGmail.txt', 'r') as infile:
            for line in infile:
                if 'Jeff Campbell'in line or 'Linden R. Peterson'in line or 'Don Bartlett'in line or \
                   'Chris Graunke'in line or 'Mike Corbett'in line or 'Gupta, Subhanshu'in line or \
                   'Padsmanabha Manjunatha' in  line or 'Alexander Mueller'in line or 'Niels Franzen'in line or 'Tom Rode'in line: # remove me once
                    #print (line)
                    Developer_list.append(line)
            
            # write the developers from email to file 
            f = open('OutDataGmail.txt', "w")
            f.writelines(Developer_list)
            f.close()

            #Indicate No Error
            GmailError = False
            
    except:
        print("Error With Gmail")
        RcvEmailErrorCode = 1
        DevRecipients_Completelist = ['tom.rode@crown.com']
        return DevRecipients_Completelist, RcvEmailErrorCode, HashMatch
    
    try:
        # Add this in email developer function, remove \r\n and replace a " " with a "."witin list
        with open('OutDataGmail.txt', 'r') as infile:
            for line in infile:
                DevRecipients_list.append(line)
            DevRecipients_list = [x.replace("\r\n","") for x in DevRecipients_list]
            DevRecipients_list = [x.replace(" ",".") for x in DevRecipients_list]

            # add '@crown.com' to DevRecipients_Complete
            for line in DevRecipients_list:
                line = line + '@crown.com'
                DevRecipients_Completelist.append(line)

            # adjust email to lower case and non crown@.com recipients
            for line in DevRecipients_Completelist:
                if 'Jeff.Campbell@crown.com' in line:                   
                    DevRecipients_Completelist = [x.replace("Jeff.Campbell@crown.com", "jeff.campbell@crown.com") for x in DevRecipients_Completelist]
                if 'Linden.R..Peterson@crown.com' in line:                   
                    DevRecipients_Completelist = [x.replace("Linden.R..Peterson@crown.com", "linden.peterson") for x in DevRecipients_Completelist]
                if 'Don.Bartlett@crown.com' in line:                   
                    DevRecipients_Completelist = [x.replace("Don.Bartlett@crown.com", "don.bartlett@crown.com") for x in DevRecipients_Completelist]
                if 'Gupta,.Subhanshu@crown.com' in line:                   
                    DevRecipients_Completelist = [x.replace("Gupta,.Subhanshu@crown.com", "Subhanshu.Gupta@lnttechservices.com") for x in DevRecipients_Completelist]
                if 'Padsmanabha.Manjunatha@crown.com' in line:                   
                    DevRecipients_Completelist = [x.replace("Padsmanabha.Manjunatha@crown.com", "Padmanabha.M@lnttechservices.com") for x in DevRecipients_Completelist]
                if 'Tom.Rode@crown.com' in line:                   
                    DevRecipients_Completelist = [x.replace("Tom.Rode@crown.com", "tom.rode@crown.com") for x in DevRecipients_Completelist]
                if 'Mike.Corbett@crown.com' in line:                   
                    DevRecipients_Completelist = [x.replace("Mike.Corbett@crown.com", "michael.corbett@crown.com") for x in DevRecipients_Completelist]
                if 'Niels.Franzen@crown.com' in line:                   
                    DevRecipients_Completelist = [x.replace("Niels.Franzen@crown.com", "niels.franzen@crown.com") for x in DevRecipients_Completelist]
                if 'Alexander.Mueller@crown.com' in line:                   
                    DevRecipients_Completelist = [x.replace("Alexander.Mueller@crown.com", "alexander.mueller@crown.com") for x in DevRecipients_Completelist]
                if 'Chris.Graunke@crown.com' in line:                   
                    DevRecipients_Completelist = [x.replace("Chris.Graunke@crown.com", "chris.graunke@crown.com") for x in DevRecipients_Completelist]


            print("Here is the Final DevRecipients_Completelist\n")
            print(DevRecipients_Completelist)

            
    except:
        print("Missing OutDataGmail.txt")
        RcvEmailErrorCode = 2
        DevRecipients_Completelist = ['tom.rode@crown.com']

    return DevRecipients_Completelist, RcvEmailErrorCode, HashMatch


def email_developer():

    DevRecipients_Completelist = []
    recipients = []

    # Call external function that worked before for proper recipient developer
    DevRecipients_Completelist, RcvEmailErrorCode, HashMatch = RcvEmailGmail()
    recipients = DevRecipients_Completelist

    # Make a copy of C:\vcast-buildsystem-orig\Regression-Original-Scripts\EmailDeveloperData\EmailDeveloperData folder 
    try:        
        # Copy the files from source to destination 
        for filename in os.listdir(EmailDeveloperDataSource):
            if os.path.isfile(os.path.join(EmailDeveloperDataSource, filename)):
                shutil.copy(os.path.join(EmailDeveloperDataSource, filename), os.path.join(EmailMessageDestination, filename))
    except:
        print("Error opening data file")

    # get new destination path of last regression
    try:
        f = open('CurrentPath.txt', 'r') 
        dest_path = f.read()
        f.close()
    except:
        print("Could not open CurrentPath.txt")
        
    # get if On-Target or Sim of last regression
    try:
        f = open('TargetOrSim.txt', 'r')
        TargetOrSim = f.read()
        f.close()
    except:
        print("Could not open TargetOrSim.txt") 
       
    # Create the enclosing (outer) message, deterime which message in subject 
    outer = MIMEMultipart()

    #Open .OUT file 
    if definition1 in open(rtc_outfile,'r+').read():
        print("Found TmlVcmMasterBuildDef001")
        output = 'MAIN DEVELOPMENT STREAM' 
    elif definition2 in open(rtc_outfile,'r+').read():
        print("Found TmlVcmMasterBuildDef002")
        output = 'QUALITY STREAM'
    else:
        print("Build definition not found")
        output = 'Undefined'  
    
    # Determine if On-Target or Sim
    if TargetOrSim == '1':
        outer['Subject'] = ('FAILED VECTOR CAST AUTOMATED SIMULATION TEST RESULTS ' + output)
    else:
        outer['Subject'] = ('FAILED VECTOR CAST AUTOMATED ON-TARGET TEST RESULTS ' + output)
    outer['To'] = COMMASPACE.join(recipients)
    outer['From'] = sender
    outer.preamble = 'You will not see this in a MIME-aware mail reader.\n'
    
    # get the path of the folder this lives in 
    attachments = source_path_emaildevelopers #[os.getcwd()]
    
    # Create a text/plain message if no error from email
    if RcvEmailErrorCode == 1:
        msg = MIMEText('Jazz Server Received Gmail Email Error')
    elif RcvEmailErrorCode == 2:
        msg = MIMEText('Jazz Server Out Data Error')
    else:
        msg = MIMEText('Test results stored on server EngSTL1 in folder:  ' + dest_path)
    outer.attach(msg)
     
    # List of attachments to email, this may include HMTL's in the future
    #emailfiles = ['VCAST Test Times.txt', \
    #              'Cumulative_Results.xlsx']
    emailfiles, bothpassfails= ParseHtml()
    
    # Add the attachments to the message 
    try:      
        for line in emailfiles:
            fp = open(line , 'rb')            
            msg = MIMEBase('application', "octet-stream")
            msg.set_payload(fp.read()) 
            encoders.encode_base64(msg)
            msg.add_header('Content-Disposition', 'attachment', filename=line)
            outer.attach(msg)
            #msg.attach(MIMEText(attachments, 'plain'))
            
    except:
        print("Unable to open one of the attachments. Error: ", sys.exc_info()[0])
        raise

    composed = outer.as_string()

    # Authenticate via LDAP
    l = ldap.initialize(Server)
    l.protocol_version = 3
    l.set_option(ldap.OPT_REFERRALS, 0)
    l.simple_bind_s(CrownNetwork, CrownPswd)

    # Send email only if a difference in hash or failure html
    if ( HashMatch == False or RcvEmailErrorCode == 1 or RcvEmailErrorCode == 2):  # remove me once done
        # New condition send only failures in .html reports 
        if emailfiles:
            # Send the email
            server = smtplib.SMTP(CrownEmail, 25) 
            text = msg.as_string()
            server.sendmail(sender, recipients, composed)
            server.quit()
            print("Email Sent")
        else:
            print("***********************************************************************\n")
            print("*********** EMAIL NOT SEND, NO FAILURES TO REPORT *********************\n")
            print("***********************************************************************\n")
 
    
        
 
if __name__ == '__main__':
   
    # looping ever three minutes
    while 1:
        email_developer()    
        time.sleep(180)
