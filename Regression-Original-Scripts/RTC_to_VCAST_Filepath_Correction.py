# Name:         RTC_to_VCAST_Filepath_Correction.py
#
# Purpose:      This python script Read in VCAST Test file paths from The RTC sandbox and if a change happened then change path accordingly.
#
# Options:      The folders names this script searches for are absolute and it they change the entry will be omitted.
#               The Autogen has the most freedom to change 
#
# Note:         Changes .env and .bat files
#
# Author:       Thomas Rode
#
# Python Version: 2.7.11
# Reference: https://www.python.org/ (open source)
#

import os
import sys
import fileinput
import time
from EmailTestResults import email_FoldersMissing

class eTestname:
    AECP_MAIN_INTEGRATION = 0,
    BECP_MAIN_INTEGRATION = 1,
    SWITCH_BOARD_INTEGRATION = 2,
    EVENT_MANAGER_INTEGRATION = 3,
    EXTERNAL_ALARM_INTEGRATION = 4,
    GYRO_DRIVER_INTEGRATION = 5,
    FEATURES_INTERFACE_INTEGRATION = 6,
    SUPV_INTERFACE_INTEGRATION = 7,
    IMM_INTERFACE_INTEGRATION = 8,
    OS_INTERFACE_INTEGRATION = 9,
    STEERING_INTEGRATION = 10,
    IP_INTERFACE_INTEGRATION = 11,
    MANIFEST_MANAGER_INTEGRATION = 12,
    TRACTION_INTEGRATION = 13,
    FRAM_DRIVER_INTEGRATION = 14,
    UTILITIES_INTEGRATION = 15,
    DIAGNOSTICS_INTEGRATION = 16,
    MAX_INTEGRATION = 17
         

TRUNCATE_LENGTH = 49

def CheckForFolder():

    folder_list = []
    FoldersMissingFlg = False
    
    # set all of these folders flag false until found
    # Embedded side
    CANopen = False
    CRT = False
    DLLf132c = False
    LSS = False
    DLLmul = False
    Include = False    
    go_driver = False
    switch_board = False
    green = False
    Autogen_API = False
    VCM_APP_CANopen = False
    diagnostic = False    
    drivers_fram = False
    io = False
    etpu2 = False
    aecp = False
    comm = False
    can = False
    xcp = False
    xcp_cfg = False
    xcp_usr = False
    gyro = False
    becp = False
    accel = False
    interface = False
    file_sys_api = False
    ftp = False
    general = False
    #hal = False
    MPC5777C = False
    codrv = False
    nvstorage_fram = False
    utilities = False
    parameters = False
    Param_Gen2 = False
    tgt_MPC5777C = False
    nvstorage = False
    # EVENT MANAGER only
    # Flash folder gone 

    # AECP, BECP, SWITCHBOARD, EXTERNAL_ALARM, GYRO_DRIVER, FEATURES_INTERFACE, SUPV_INTERFACE_INTEGRATION, IMM_INTERFACE, OS_INTERFACE, IP_INTERFACE, TRACTION, FRAM_DRIVER Modeled side 
    BusHeaders = False
    sharedutils = False
    VehicleFBSIMbuild_ert_rtw = False
    TractionSIM = False
    SteeringSIMbuild_ert_rtw = False
    hydraulicSIM = False
    EnergySourceSIM = False    
    CalibrationSIMbuild_ert_rtw = False
    commonSIMbuild_ert_rtw = False
    commonSIM = False
    CalibrationSIM = False
    EnergySourceSIMbuild_ert_rtw = False
    hydraulicSIMbuild_ert_rtw = False
    SteeringSIM = False
    TractionSIMbuild_ert_rtw = False
    HmsSim_VcmInputsBuild_ert_rtw = False    
    HmsSim_HcmTcmBuild_ert_rtw = False
    Vehicle_fb_processing_SIM = False
    HmsSim_TractionBuild_ert_rtw = False
    HmsSim_EdBuild_ert_rtw = False
    HmsSim_HydraulicBuild_ert_rtw = False
    HmsSim_PowerSuppliesBuild_ert_rtw = False
    HmsSim_SteeringBuild_ert_rtw = False
    HmsSim_CommunicationBuild_ert_rtw = False
    HmsSim_Traction = False
    HmsSim_Communication = False
    HmsSim_Steering = False
    HmsSim_PowerSupplies = False
    HmsSim_VcmInputs = False
    HmsSim_HcmTcm = False
    HmsSim_GpdBuild_ert_rtw = False
    HmsSim_Ed = False
    HmsSim_Brake = False
    HmsSim_BrakeBuild_ert_rtw = False
    HmsSim_Hydraulic = False
    HmsSim_Gpd = False
    Autogen_parameters = False
    # BECP and SWITCHBOARD
    
    # SWITCH_BOARD and EVENT MANAGER and STEERING only
    Sw_Outputs = False
    AutomationSIMbuild_ert_rtw = False
    AutomationSIM = False
    SwitchBoardOUTSIMbuild_ert_rtw = False
    TestOutput = False
    TestOutputSIMbuild_ert_rtw = False

    # STEERING
    AssistCmd = False   
    PosCmd = False
    PosCmd_referenced_model_includes = False
    TfdOvrMode = False
    TfdCurrentSpProfile = False
    TfdOpMode = False
    TfdOpMode_referenced_model_includes = False
    StrMstrApp = False
    StrMstrApp_referenced_model_includes = False
    StrPosHndlSpdSense = False
    StrPos_SetPntRateLmtr = False
    StrPos_SahNmbns = False
    StrPosWhAngLimiter = False
    SpdCmd = False
    SpdCmd_referenced_model_includes = False
    StrMtrSpdLmt = False
    StrMtrSpdLmt_referenced_model_includes = False
    StrMstrSup = False
    TrxSpd_StrMtrSpdLmt = False
    WhAngErr_StrMtrSpdLmt = False
    WhAng_StrMtrSpdLmt = False
    Til2DuScaling = False    
    Cmd_Pos_Integrator = False
    HmsSim_Steering_referenced_model_includes = False

    # TRACTION
    BrakeLgc = False
    OnTrac = False
    PIControlLoop = False
    ParameterDiagnostics = False
    ThrottleSpdCmd = False
    TorqueLimiter = False
    Traction_State_Machine = False
    accel_decel_slew = False
    ProgLgc = False
    BrakeLgc_referenced_model_includes = False
    TractionSIM_referenced_model_includes = False
            
    for root, dirs, files in os.walk("C:/rtc_workspaces/TmlVcmMasterBuildDef001_Sandbox"):  #dirs,
        # See if these folders exist
        if'.jazz5' not in root:
            if'CANopen' in root and 'CanStackSrc' in root:
                if(len('CANopen')) == (len(os.path.basename(root))):
                    CANopen = True                                    
            if'CRT' in root:
                if(len('CRT')) == (len(os.path.basename(root))):
                    CRT = True     
            if'DLLfl32c' in root:
                if(len('DLLfl32c')) == (len(os.path.basename(root))):
                    DLLf132c = True           
            if'LSS' in root:
                if(len('LSS')) == (len(os.path.basename(root))):
                    LSS = True
            if'DLLmul' in  root:
                if(len('DLLmul')) == (len(os.path.basename(root))):
                    DLLmul = True
            if'Include' in root:
                if(len('Include')) == (len(os.path.basename(root))):
                    Include = True
            if'go_driver' in root:
                if(len('go_driver')) == (len(os.path.basename(root))):
                    go_driver = True
            if'switch_board' in root:
                if(len('switch_board')) == (len(os.path.basename(root))):
                    switch_board = True
            if'green' in root:
                if(len('green')) == (len(os.path.basename(root))):
                    green = True
            if'Autogen_API' in root:
                if(len('Autogen_API')) == (len(os.path.basename(root))):
                    Autogen_API = True
            if'VCM_APP' in root:
                if 'CANopen' in root:
                    if(len('CANopen')) == (len(os.path.basename(root))):
                        VCM_APP_CANopen = True
            if'diagnostics' in root:
                if(len('diagnostics')) == (len(os.path.basename(root))):
                    diagnostic = True            
            if 'drivers' in root:
                if 'fram' in root:
                    if(len('fram')) == (len(os.path.basename(root))):
                        drivers_fram = True
            if'io' in root:
                if(len('io')) == (len(os.path.basename(root))):
                    io = True
            if'etpu2' in root:
                if(len('etpu2')) == (len(os.path.basename(root))):
                    etpu2 = True
            if'aecp' in root:
                if(len('aecp')) == (len(os.path.basename(root))):
                    aecp = True
            if'comm' in root:
                if(len('comm')) == (len(os.path.basename(root))):
                    comm = True
            if'can' in root and 'driver' in root:
                if(len('can')) == (len(os.path.basename(root))):
                    can = True
            if'xcp' in root:
                if(len('xcp')) == (len(os.path.basename(root))):
                    xcp = True
            if'xcp\cfg' in root:  
                if(len('cfg')) == (len(os.path.basename(root))):
                    xcp_cfg = True
            if'xcp\usr' in root:
                if(len('usr')) == (len(os.path.basename(root))):
                    xcp_usr = True
            if'gyro' in root:
                if(len('gyro')) == (len(os.path.basename(root))):
                    gyro = True
            if'becp' in root:
                 if(len('becp')) == (len(os.path.basename(root))):
                    becp = True
            if'accel' in root:
                if(len('accel')) == (len(os.path.basename(root))):
                    accel = True            
            if'interface' in root:
                if(len('interface')) == (len(os.path.basename(root))):
                    interface = True
            if'file_sys' in root and 'api' in root:
                if(len('api')) == (len(os.path.basename(root))):
                    file_sys_api = True
            if'VCM_APP' in root and 'ftp' in root:
                if(len('ftp')) == (len(os.path.basename(root))):
                    ftp = True
            if'general' in root:
                if(len('general')) == (len(os.path.basename(root))):
                    general = True
            if'hal' in root:
                if 'MPC5777C' in root:
                    if(len('MPC5777C')) == (len(os.path.basename(root))):
                        MPC5777C = True
            if'codrv' in root:
                if(len('codrv')) == (len(os.path.basename(root))): 
                    codrv = True
            if'nvstorage' in root:
                if 'fram' in root:
                    if(len('fram')) == (len(os.path.basename(root))): 
                        nvstorage_fram = True
            if'utilities' in root:
                if(len('utilities')) == (len(os.path.basename(root))): 
                    utilities = True
            if'VCM_APP' in root and not 'Autogen' in root:
                if 'parameters' in root: 
                    if(len('parameters')) == (len(os.path.basename(root))): 
                        parameters = True
                if 'parameters' in root:
                    if 'Param_Gen2' in root:
                         if(len('Param_Gen2')) == (len(os.path.basename(root))): 
                            Param_Gen2 = True
            if'tgt_MPC5777C' in root:
                if(len('tgt_MPC5777C')) == (len(os.path.basename(root))): 
                    tgt_MPC5777C = True
            if'nvstorage' in root:
                if(len('nvstorage')) == (len(os.path.basename(root))): 
                    nvstorage = True

            
        # Autogen     
        if'VCM_APP'in root and '.jazz5' not in root:    
            if'BusHeaders' in root and 'referenced_model_includes' not in root:
               if(len('BusHeaders')) == (len(os.path.basename(root))):  
                    BusHeaders = True
            if'sharedutils' in root and 'referenced_model_includes' not in root:
               if(len('sharedutils')) == (len(os.path.basename(root))): 
                    sharedutils = True
            if'VehicleFBSIMbuild_ert_rtw' in root and 'referenced_model_includes' not in root:
                if(len('VehicleFBSIMbuild_ert_rtw')) == (len(os.path.basename(root))):
                    VehicleFBSIMbuild_ert_rtw = True
            if'TractionSIM' in root and 'referenced_model_includes' not in root:
                if(len('TractionSIM')) == (len(os.path.basename(root))):
                    TractionSIM = True
            if'SteeringSIMbuild_ert_rtw' in root and 'referenced_model_includes' not in root:
                if(len('SteeringSIMbuild_ert_rtw')) == (len(os.path.basename(root))): 
                    SteeringSIMbuild_ert_rtw = True
            if'hydraulicSIM' in root and 'referenced_model_includes' not in root:
                if(len('hydraulicSIM')) == (len(os.path.basename(root))): 
                   hydraulicSIM = True
            if'EnergySourceSIM' in root and 'referenced_model_includes' not in root:
               if(len('EnergySourceSIM')) == (len(os.path.basename(root))):   
                    EnergySourceSIM = True
            if'CalibrationSIMbuild_ert_rtw' in root and 'referenced_model_includes' not in root:
                if(len('CalibrationSIMbuild_ert_rtw')) == (len(os.path.basename(root))):                            
                    CalibrationSIMbuild_ert_rtw = True
            if'commonSIMbuild_ert_rtw' in root and 'referenced_model_includes' not in root:
                if(len('commonSIMbuild_ert_rtw')) == (len(os.path.basename(root))):
                    commonSIMbuild_ert_rtw = True
            if 'commonSIM' in root and 'referenced_model_includes' not in root:
                if(len('commonSIM')) == (len(os.path.basename(root))):
                    commonSIM = True
            if 'CalibrationSIM' in root and 'referenced_model_includes' not in root:
                if(len('CalibrationSIM')) == (len(os.path.basename(root))):
                    CalibrationSIM = True
            if'EnergySourceSIMbuild_ert_rtw' in root and 'referenced_model_includes' not in root:
                if(len('EnergySourceSIMbuild_ert_rtw')) == (len(os.path.basename(root))):
                    EnergySourceSIMbuild_ert_rtw= True
            if'hydraulicSIMbuild_ert_rtw' in root and 'referenced_model_includes' not in root:
                if(len('hydraulicSIMbuild_ert_rtw')) == (len(os.path.basename(root))):
                    hydraulicSIMbuild_ert_rtw = True
            if'SteeringSIM'in root and 'referenced_model_includes' not in root:
                if(len('SteeringSIM')) == (len(os.path.basename(root))):
                    SteeringSIM = True
            if'TractionSIMbuild_ert_rtw' in root and 'referenced_model_includes' not in root:
                if(len('TractionSIMbuild_ert_rtw')) == (len(os.path.basename(root))):
                    TractionSIMbuild_ert_rtw = True
            if'HmsSim_VcmInputsBuild_ert_rtw' in root and 'referenced_model_includes' not in root:
                if(len('HmsSim_VcmInputsBuild_ert_rtw')) == (len(os.path.basename(root))):
                    HmsSim_VcmInputsBuild_ert_rtw = True
            if'HmsSim_HcmTcmBuild_ert_rtw' in root and 'referenced_model_includes' not in root:
                if(len('HmsSim_HcmTcmBuild_ert_rtw')) == (len(os.path.basename(root))):
                    HmsSim_HcmTcmBuild_ert_rtw = True
            if'Vehicle_fb_processing_SIM' in root and 'referenced_model_includes' not in root:
                if(len('Vehicle_fb_processing_SIM')) == (len(os.path.basename(root))): 
                    Vehicle_fb_processing_SIM = True
            if'HmsSim_TractionBuild_ert_rtw' in root and 'referenced_model_includes' not in root:
                if(len('HmsSim_TractionBuild_ert_rtw')) == (len(os.path.basename(root))):
                    HmsSim_TractionBuild_ert_rtw = True
            if'HmsSim_EdBuild_ert_rtw' in root and 'referenced_model_includes' not in root:
                if(len('HmsSim_EdBuild_ert_rtw')) == (len(os.path.basename(root))):
                    HmsSim_EdBuild_ert_rtw = True
            if'HmsSim_HydraulicBuild_ert_rtw' in root and 'referenced_model_includes' not in root:
                if(len('HmsSim_HydraulicBuild_ert_rtw')) == (len(os.path.basename(root))):
                    HmsSim_HydraulicBuild_ert_rtw = True
            if'HmsSim_PowerSuppliesBuild_ert_rtw' in root and 'referenced_model_includes' not in root:
                if(len('HmsSim_PowerSuppliesBuild_ert_rtw')) == (len(os.path.basename(root))):
                    HmsSim_PowerSuppliesBuild_ert_rtw = True
            if'HmsSim_SteeringBuild_ert_rtw' in root and 'referenced_model_includes' not in root:
                if(len('HmsSim_SteeringBuild_ert_rtw')) == (len(os.path.basename(root))):
                    HmsSim_SteeringBuild_ert_rtw = True
            if'HmsSim_CommunicationBuild_ert_rtw' in root and 'referenced_model_includes' not in root:
                if(len('HmsSim_CommunicationBuild_ert_rtw')) == (len(os.path.basename(root))):
                    HmsSim_CommunicationBuild_ert_rtw = True
            if'HmsSim_Traction' in root and 'referenced_model_includes' not in root:
                if(len('HmsSim_Traction')) == (len(os.path.basename(root))):
                    HmsSim_Traction = True
            if'HmsSim_Communication' in root and 'referenced_model_includes' not in root:
                if(len('HmsSim_Communication')) == (len(os.path.basename(root))):
                    HmsSim_Communication = True
            if'HmsSim_Steering' in root and 'referenced_model_includes' not in root:
                if(len('HmsSim_Steering')) == (len(os.path.basename(root))):
                    HmsSim_Steering = True
            if'HmsSim_PowerSupplies' in root and 'referenced_model_includes' not in root:
                if(len('HmsSim_PowerSupplies')) == (len(os.path.basename(root))):
                    HmsSim_PowerSupplies = True         
            if'HmsSim_VcmInputs' in root and 'referenced_model_includes' not in root:
                if(len('HmsSim_VcmInputs')) == (len(os.path.basename(root))):
                    HmsSim_VcmInputs = True
            if'HmsSim_HcmTcm' in root and 'referenced_model_includes' not in root:
                if(len('HmsSim_HcmTcm')) == (len(os.path.basename(root))):
                    HmsSim_HcmTcm = True
            if'HmsSim_GpdBuild_ert_rtw' in root and 'referenced_model_includes' not in root:
                if(len('HmsSim_GpdBuild_ert_rtw')) == (len(os.path.basename(root))):
                    HmsSim_GpdBuild_ert_rtw = True
            if'HmsSim_Ed' in root and 'referenced_model_includes' not in root:
                if(len('HmsSim_Ed')) == (len(os.path.basename(root))):
                    HmsSim_Ed = True
            if'HmsSim_Brake' in root and 'referenced_model_includes' not in root:
                if(len('HmsSim_Brake')) == (len(os.path.basename(root))):
                    HmsSim_Brake = True
            if'HmsSim_BrakeBuild_ert_rtw' in root and 'referenced_model_includes' not in root:
                if(len('HmsSim_BrakeBuild_ert_rtw')) == (len(os.path.basename(root))):
                    HmsSim_BrakeBuild_ert_rtw = True
            if'HmsSim_Hydraulic' in root and 'referenced_model_includes' not in root:
                if(len('HmsSim_Hydraulic')) == (len(os.path.basename(root))):
                    HmsSim_Hydraulic = True
            if'HmsSim_Gpd' in root and 'referenced_model_includes' not in root:
                if(len('HmsSim_Gpd')) == (len(os.path.basename(root))):
                    HmsSim_Gpd = True
            if'Autogen' in root and 'parameters' in root and 'referenced_model_includes' not in root:
                if(len('parameters')) == (len(os.path.basename(root))):
                    Autogen_parameters = True
                # BECP and SWITCHBOARD
            
                #SWITCH_BOARD and EVENT MANAGER and STEERING only
            if'Sw_Outputs'in root and 'referenced_model_includes' not in root:
                if(len('Sw_Outputs')) == (len(os.path.basename(root))):
                    Sw_Outputs = True
            if'AutomationSIMbuild_ert_rtw' in root and 'referenced_model_includes' not in root:
                if(len('AutomationSIMbuild_ert_rtw')) == (len(os.path.basename(root))):
                    AutomationSIMbuild_ert_rtw = True
            if'AutomationSIM' in root and 'referenced_model_includes' not in root:               
                if(len('AutomationSIM')) == (len(os.path.basename(root))):
                    AutomationSIM = True    
            if'SwitchBoardOUTSIMbuild_ert_rtw' in root and 'referenced_model_includes' not in root:               
                if(len('SwitchBoardOUTSIMbuild_ert_rtw')) == (len(os.path.basename(root))):
                    SwitchBoardOUTSIMbuild_ert_rtw = True
            if'TestOutput' in root and 'referenced_model_includes' not in root:
                if(len('TestOutput')) == (len(os.path.basename(root))):
                    TestOutput = True
            if'TestOutputSIMbuild_ert_rtw' in root and 'referenced_model_includes' not in root:
                if(len('TestOutputSIMbuild_ert_rtw')) == (len(os.path.basename(root))):
                    TestOutputSIMbuild_ert_rtw = True

                 # STEERING
            if'AssistCmd' in root and 'referenced_model_includes' not in root:
                if(len('AssistCmd')) == (len(os.path.basename(root))):
                    AssistCmd = True
            if'PosCmd' in root and 'referenced_model_includes' not in root:
                if(len('PosCmd')) == (len(os.path.basename(root))):
                    PosCmd = True
            if'PosCmd' in root:
                if 'referenced_model_includes' in root:
                    if(len('referenced_model_includes')) == (len(os.path.basename(root))):
                        PosCmd_referenced_model_includes = True
            if'TfdOvrMode' in root and 'referenced_model_includes' not in root:
                if(len('TfdOvrMode')) == (len(os.path.basename(root))):
                    TfdOvrMode = True
            if'TfdCurrentSpProfile' in root and 'referenced_model_includes' not in root:
                if(len('TfdCurrentSpProfile')) == (len(os.path.basename(root))):
                    TfdCurrentSpProfile = True
            if'TfdOpMode' in root and 'referenced_model_includes' not in root:
                if(len('TfdOpMode')) == (len(os.path.basename(root))):
                    TfdOpMode = True
            if'TfdOpMode' in root:
                if 'referenced_model_includes' in root:
                    if(len('referenced_model_includes')) == (len(os.path.basename(root))):
                        TfdOpMode_referenced_model_includes = True
            if'StrMstrApp' in root and 'referenced_model_includes' not in root:
                if(len('StrMstrApp')) == (len(os.path.basename(root))):
                    StrMstrApp = True
            if'StrMstrApp' in root: 
                if 'referenced_model_includes' in root:
                    if(len('referenced_model_includes')) == (len(os.path.basename(root))):
                        StrMstrApp_referenced_model_includes = True
            if'StrPosHndlSpdSense' in root and 'referenced_model_includes' not in root:
                if(len('StrPosHndlSpdSense')) == (len(os.path.basename(root))):
                    StrPosHndlSpdSense = True
            if'StrPos_SetPntRateLmtr' in root and 'referenced_model_includes' not in root:
                if(len('StrPos_SetPntRateLmtr')) == (len(os.path.basename(root))):
                    StrPos_SetPntRateLmtr = True
            if'StrPos_SahNmbns' in root and 'referenced_model_includes' not in root:
                if(len('StrPos_SahNmbns')) == (len(os.path.basename(root))):
                    StrPos_SahNmbns = True
            if'StrPosWhAngLimiter' in root and 'referenced_model_includes' not in root:
                if(len('StrPosWhAngLimiter')) == (len(os.path.basename(root))):
                    StrPosWhAngLimiter = True
            if'SpdCmd' in root and 'referenced_model_includes' not in root:
                if(len('SpdCmd')) == (len(os.path.basename(root))):
                    SpdCmd = True
            if'SpdCmd' in root: 
                if 'referenced_model_includes' in root:
                    if(len('referenced_model_includes')) == (len(os.path.basename(root))):
                        SpdCmd_referenced_model_includes = True
            if'StrMtrSpdLmt' in root and 'referenced_model_includes' not in root:
                if(len('StrMtrSpdLmt')) == (len(os.path.basename(root))):
                    StrMtrSpdLmt = True
            if'StrMtrSpdLmt' in root: 
                if 'referenced_model_includes' in root:
                    if(len('referenced_model_includes')) == (len(os.path.basename(root))):
                        StrMtrSpdLmt_referenced_model_includes = True
            if'StrMstrSup' in root and 'referenced_model_includes' not in root:
                if(len('StrMstrSup')) == (len(os.path.basename(root))):
                    StrMstrSup = True
            if'TrxSpd_StrMtrSpdLmt' in root and 'referenced_model_includes' not in root:
                if(len('TrxSpd_StrMtrSpdLmt')) == (len(os.path.basename(root))):
                    TrxSpd_StrMtrSpdLmt = True
            if'WhAngErr_StrMtrSpdLmt' in root and 'referenced_model_includes' not in root:
                if(len('WhAngErr_StrMtrSpdLmt')) == (len(os.path.basename(root))):
                    WhAngErr_StrMtrSpdLmt = True
            if'WhAng_StrMtrSpdLmt' in root and 'referenced_model_includes' not in root:
                if(len('WhAng_StrMtrSpdLmt')) == (len(os.path.basename(root))):
                    WhAng_StrMtrSpdLmt = True
            if'Til2DuScaling' in root and 'referenced_model_includes' not in root:
                if(len('Til2DuScaling')) == (len(os.path.basename(root))):
                    Til2DuScaling = True
            if'Cmd_Pos_Integrator' in root and 'referenced_model_includes' not in root:
                if(len('Cmd_Pos_Integrator')) == (len(os.path.basename(root))):
                    Cmd_Pos_Integrator = True
            if'HmsSim_Steering' in root:
                if 'referenced_model_includes' in root:
                    if(len('referenced_model_includes')) == (len(os.path.basename(root))):
                        HmsSim_Steering_referenced_model_includes = True

            # TRACTION           
            if'BrakeLgc' in root and 'referenced_model_includes' not in root:
                if(len('BrakeLgc')) == (len(os.path.basename(root))):
                    BrakeLgc = True
            if'OnTrac' in root and 'referenced_model_includes' not in root:
                if(len('OnTrac')) == (len(os.path.basename(root))):
                    OnTrac = True
            if'PIControlLoop' in root and 'referenced_model_includes' not in root:
                if(len('PIControlLoop')) == (len(os.path.basename(root))):
                    PIControlLoop = True
            if'ParameterDiagnostics' in root and 'referenced_model_includes' not in root:
                if(len('ParameterDiagnostics')) == (len(os.path.basename(root))):
                    ParameterDiagnostics = True
            if'ThrottleSpdCmd' in root and 'referenced_model_includes' not in root:
                if(len('ThrottleSpdCmd')) == (len(os.path.basename(root))):
                    ThrottleSpdCmd = True
            if'TorqueLimiter' in root and 'referenced_model_includes' not in root:
                if(len('TorqueLimiter')) == (len(os.path.basename(root))):
                    TorqueLimiter = True
            if'Traction_State_Machine' in root and 'referenced_model_includes' not in root:
                if(len('Traction_State_Machine')) == (len(os.path.basename(root))):
                    Traction_State_Machine = True
            if'accel_decel_slew' in root and 'referenced_model_includes' not in root:
                if(len('accel_decel_slew')) == (len(os.path.basename(root))):
                    accel_decel_slew = True
            if'ProgLgc' in root and 'referenced_model_includes' not in root:
                if(len('ProgLgc')) == (len(os.path.basename(root))):
                    ProgLgc = True
            if'BrakeLgc' in root: 
                if 'referenced_model_includes' in root:
                    if(len('referenced_model_includes')) == (len(os.path.basename(root))):
                        BrakeLgc_referenced_model_includes = True
            if'TractionSIM' in root: 
                if 'referenced_model_includes' in root:
                    if(len('referenced_model_includes')) == (len(os.path.basename(root))):
                        TractionSIM_referenced_model_includes = True
                         
       
    # append the list with folders that are missing
    if CANopen == False:
        folder_list.append('CANopen' + '\n')       
    if CRT == False:
        folder_list.append('CRT'+ '\n')
    if DLLf132c == False:
        folder_list.append('DLLf132c'+ '\n')
    if LSS == False:
        folder_list.append('LSS'+ '\n')
    if DLLmul == False:
        folder_list.append('DLLmul'+ '\n')
    if Include == False:
        folder_list.append('Include'+ '\n')
    if go_driver == False:
        folder_list.append('go_driver'+ '\n')
    if switch_board == False:
        folder_list.append('switch_board'+ '\n')    
    if green == False:
        folder_list.append('green'+ '\n')
    if Autogen_API == False:
        folder_list.append('Autogen_API'+ '\n')
    if VCM_APP_CANopen == False:
        folder_list.append('VCM_APP/CANopen' + '\n')
    if diagnostic == False:
        folder_list.append('diagnostic' + '\n')
    if drivers_fram == False:
        folder_list.append('drivers/fram' + '\n')
    if io == False:
        folder_list.append('io' + '\n')
    if etpu2 == False:
        folder_list.append('etpu2' + '\n')
    if aecp == False:
        folder_list.append('aecp' + '\n')
    if comm == False:
        folder_list.append('comm' + '\n')
    if can == False:
        folder_list.append('can' + '\n')
    if xcp == False:
        folder_list.append('xcp' + '\n')
    if xcp_cfg == False:
        folder_list.append('xcp/cfg' + '\n')
    if xcp_usr == False:
        folder_list.append('xcp/usr' + '\n')
    if gyro == False:
        folder_list.append('gyro' + '\n')
    if becp == False:
        folder_list.append('becp' + '\n')
    if accel == False:
        folder_list.append('accel' + '\n')
    if interface == False:
        folder_list.append('interface' + '\n')
    if file_sys_api == False:
        folder_list.append('file_sys/api' + '\n')
    if ftp == False:
        folder_list.append('ftp' + '\n')
    if general == False:
        folder_list.append('general' + '\n')
    if MPC5777C == False:
        folder_list.append('MPC5777C' + '\n')
    if codrv == False:
        folder_list.append('codrv' + '\n')
    if nvstorage_fram == False:
        folder_list.append('nvstorage/fram' + '\n')
    if utilities == False:
        folder_list.append('utilities' + '\n')
    if parameters == False:
        folder_list.append('parameters' + '\n')
    if Param_Gen2 == False:
        folder_list.append('Param_Gen2' + '\n')
    if tgt_MPC5777C == False:
        folder_list.append('tgt_MPC5777C' + '\n')
    if nvstorage == False:
        folder_list.append('nvstorage' + '\n')
    if BusHeaders == False:
        folder_list.append('BusHeaders' + '\n')
    if sharedutils == False:
        folder_list.append('sharedutils' + '\n')
    if VehicleFBSIMbuild_ert_rtw == False:
        folder_list.append('VehicleFBSIMbuild_ert_rtw' + '\n')
    if TractionSIM == False:
        folder_list.append('TractionSIM' + '\n')
    if SteeringSIMbuild_ert_rtw == False:
        folder_list.append('SteeringSIMbuild_ert_rtw' + '\n')
    if hydraulicSIM == False:
        folder_list.append('hydraulicSIM' + '\n')
    if EnergySourceSIM == False:    
        folder_list.append('EnergySourceSIM' + '\n')        
    if CalibrationSIMbuild_ert_rtw == False:
        folder_list.append('CalibrationSIMbuild_ert_rtw' + '\n')    
    if commonSIMbuild_ert_rtw == False:
        folder_list.append('commonSIMbuild_ert_rtw' + '\n') 
    if commonSIM == False:
        folder_list.append('commonSIM' + '\n')   
    if CalibrationSIM == False:
        folder_list.append('CalibrationSIM' + '\n')
    if EnergySourceSIMbuild_ert_rtw == False:
        folder_list.append('EnergySourceSIMbuild_ert_rtw' + '\n')
    if hydraulicSIMbuild_ert_rtw == False:
        folder_list.append('hydraulicSIMbuild_ert_rtw' + '\n')
    if SteeringSIM == False:
        folder_list.append('SteeringSIM' + '\n')
    if TractionSIMbuild_ert_rtw == False:
        folder_list.append('TractionSIMbuild_ert_rtw' + '\n') 
    if HmsSim_VcmInputsBuild_ert_rtw == False:
        folder_list.append('HmsSim_VcmInputsBuild_ert_rtw' + '\n')
    if HmsSim_HcmTcmBuild_ert_rtw == False:
        folder_list.append('HmsSim_HcmTcmBuild_ert_rtw' + '\n')
    if Vehicle_fb_processing_SIM == False:
        folder_list.append('Vehicle_fb_processing_SIM' + '\n')
    if HmsSim_TractionBuild_ert_rtw == False:
        folder_list.append('HmsSim_TractionBuild_ert_rtw' + '\n')
    if HmsSim_EdBuild_ert_rtw == False:
        folder_list.append('HmsSim_EdBuild_ert_rtw' + '\n')
    if HmsSim_HydraulicBuild_ert_rtw == False:
        folder_list.append('HmsSim_HydraulicBuild_ert_rtw' + '\n')
    if HmsSim_PowerSuppliesBuild_ert_rtw == False:
        folder_list.append('HmsSim_PowerSuppliesBuild_ert_rtw' + '\n')
    if HmsSim_SteeringBuild_ert_rtw == False:
        folder_list.append('HmsSim_SteeringBuild_ert_rtw' + '\n') 
    if HmsSim_CommunicationBuild_ert_rtw == False:
        folder_list.append('HmsSim_CommunicationBuild_ert_rtw' + '\n')
    if HmsSim_Traction == False:
        folder_list.append('HmsSim_Traction' + '\n') 
    if HmsSim_Communication == False:
        folder_list.append('HmsSim_Communication' + '\n') 
    if HmsSim_Steering == False:
        folder_list.append('HmsSim_Steering' + '\n') 
    if HmsSim_PowerSupplies == False:
        folder_list.append('HmsSim_PowerSupplies' + '\n')
    if HmsSim_VcmInputs == False:
        folder_list.append('HmsSim_VcmInputs' + '\n')
    if HmsSim_HcmTcm == False:
        folder_list.append('HmsSim_HcmTcm' + '\n')
    if HmsSim_GpdBuild_ert_rtw == False:
        folder_list.append('HmsSim_GpdBuild_ert_rtw' + '\n')
    if HmsSim_Ed == False:
        folder_list.append('HmsSim_Ed' + '\n')
    if HmsSim_Brake == False:
        folder_list.append('HmsSim_Brake' + '\n')
    if HmsSim_BrakeBuild_ert_rtw == False:
        folder_list.append('HmsSim_BrakeBuild_ert_rtw' + '\n')
    if HmsSim_Hydraulic == False:
        folder_list.append('HmsSim_Hydraulic' + '\n')
    if HmsSim_Gpd == False:
        folder_list.append('HmsSim_Gpd' + '\n')
    if Autogen_parameters == False:
        folder_list.append('Autogen_parameters' + '\n')
    # BECP and SWITCHBOARD
    
    #SWITCH_BOARD and EVENT MANAGER and STEERING only
    if Sw_Outputs == False:
        folder_list.append('Sw_Outputs' + '\n')
    if AutomationSIMbuild_ert_rtw == False:
        folder_list.append('AutomationSIMbuild_ert_rtw' + '\n')
    if AutomationSIM == False:
        folder_list.append('AutomationSIM' + '\n')
    if SwitchBoardOUTSIMbuild_ert_rtw == False:
        folder_list.append('SwitchBoardOUTSIMbuild_ert_rtw' + '\n')
    if TestOutput == False:
        folder_list.append('TestOutput' + '\n')
    if TestOutputSIMbuild_ert_rtw == False:
         folder_list.append('TestOutputSIMbuild_ert_rtw' + '\n')


    # STEERING only
    if AssistCmd == False:
        folder_list.append('AssistCmd' + '\n')
    if PosCmd == False:
        folder_list.append('PosCmd' + '\n')
    if PosCmd_referenced_model_includes == False:
        folder_list.append('PosCmd_referenced_model_includes' + '\n')
    if TfdOvrMode == False:
        folder_list.append('TfdOvrMode' + '\n')
    if TfdCurrentSpProfile == False:
        folder_list.append('TfdCurrentSpProfile' + '\n')
    if TfdOpMode == False:
        folder_list.append('TfdOpMode' + '\n')
    if TfdOpMode_referenced_model_includes == False:
        folder_list.append('TfdOpMode_referenced_model_includes' + '\n')
    if StrMstrApp == False:
        folder_list.append('StrMstrApp' + '\n')
    if StrMstrApp_referenced_model_includes == False:
        folder_list.append('StrMstrApp_referenced_model_includes' + '\n')
    if StrPosHndlSpdSense == False:
        folder_list.append('StrPosHndlSpdSense' + '\n')
    if StrPos_SetPntRateLmtr == False:
        folder_list.append('StrPos_SetPntRateLmtr' + '\n')
    if StrPos_SahNmbns == False:
        folder_list.append('StrPos_SahNmbns' + '\n')
    if StrPosWhAngLimiter == False:
        folder_list.append('StrPosWhAngLimiter' + '\n')
    if SpdCmd == False:
        folder_list.append('SpdCmd' + '\n')
    if SpdCmd_referenced_model_includes == False:
        folder_list.append('SpdCmd_referenced_model_includes' + '\n')
    if StrMtrSpdLmt == False:
        folder_list.append('StrMtrSpdLmt' + '\n')
    if StrMtrSpdLmt_referenced_model_includes == False:
        folder_list.append('StrMtrSpdLmt_referenced_model_includes' + '\n')
    if StrMstrSup == False:
        folder_list.append('StrMstrSup' + '\n')
    if TrxSpd_StrMtrSpdLmt == False:
        folder_list.append('TrxSpd_StrMtrSpdLmt' + '\n')
    if WhAngErr_StrMtrSpdLmt == False:
        folder_list.append('WhAngErr_StrMtrSpdLmt' + '\n')
    if WhAng_StrMtrSpdLmt == False:
        folder_list.append('WhAng_StrMtrSpdLmt' + '\n')
    if Til2DuScaling == False:
        folder_list.append('Til2DuScaling' + '\n')
    if Cmd_Pos_Integrator == False:
        folder_list.append('Cmd_Pos_Integrator' + '\n')
    if HmsSim_Steering_referenced_model_includes == False:
        folder_list.append('HmsSim_Steering_referenced_model_includes' + '\n')

    # TRACTION
    if BrakeLgc == False:
        folder_list.append('BrakeLgc' + '\n')
    if OnTrac == False:
        folder_list.append('OnTrac' + '\n')
    if PIControlLoop == False:
        folder_list.append('PIControlLoop' + '\n')
    if ParameterDiagnostics == False:
        folder_list.append('ParameterDiagnostics' + '\n')
    if ThrottleSpdCmd == False:
        folder_list.append('ThrottleSpdCmd' + '\n')
    if TorqueLimiter == False:
        folder_list.append('TorqueLimiter' + '\n')
    if Traction_State_Machine == False:
        folder_list.append('Traction_State_Machine' + '\n')
    if accel_decel_slew == False:
        folder_list.append('accel_decel_slew' + '\n')
    if ProgLgc == False:
        folder_list.append('ProgLgc' + '\n')
    if BrakeLgc_referenced_model_includes == False:
        folder_list.append('BrakeLgc_referenced_model_includes' + '\n')
    if TractionSIM_referenced_model_includes == False:
        folder_list.append('TractionSIM_referenced_model_includes' + '\n')    
        
    print(folder_list) # See whats in list remove once done  
    # any of the folder are missing
    if CANopen == False or CRT == False or DLLf132c == False or LSS == False or DLLmul == False or Include == False or \
       go_driver == False or switch_board == False or green == False or Autogen_API == False or VCM_APP_CANopen == False or \
       diagnostic == False or drivers_fram == False or io == False or etpu2 == False or aecp == False or comm == False or \
       can == False or xcp == False or xcp_cfg == False or xcp_usr == False or gyro == False or becp == False or accel == False or \
       interface == False or file_sys_api == False or ftp == False or general == False or MPC5777C == False or codrv == False or \
       nvstorage_fram == False or utilities == False or parameters == False or Param_Gen2 == False or tgt_MPC5777C == False or nvstorage == False or \
       BusHeaders == False or sharedutils == False or VehicleFBSIMbuild_ert_rtw == False or TractionSIM == False or SteeringSIMbuild_ert_rtw == False or \
       hydraulicSIM == False or EnergySourceSIM == False or CalibrationSIMbuild_ert_rtw == False or commonSIMbuild_ert_rtw == False or \
       commonSIM == False or CalibrationSIM  == False or EnergySourceSIMbuild_ert_rtw == False or hydraulicSIMbuild_ert_rtw == False or \
       SteeringSIM == False or TractionSIMbuild_ert_rtw  == False or TractionSIMbuild_ert_rtw == False or HmsSim_HcmTcmBuild_ert_rtw == False or \
       Vehicle_fb_processing_SIM == False or HmsSim_TractionBuild_ert_rtw  == False or HmsSim_EdBuild_ert_rtw == False or HmsSim_HydraulicBuild_ert_rtw == False or \
       HmsSim_PowerSuppliesBuild_ert_rtw == False or HmsSim_SteeringBuild_ert_rtw == False or HmsSim_CommunicationBuild_ert_rtw == False or HmsSim_Traction == False or \
       HmsSim_Communication == False or HmsSim_Steering == False or HmsSim_PowerSupplies == False or HmsSim_VcmInputs == False or HmsSim_HcmTcm == False or \
       HmsSim_GpdBuild_ert_rtw == False or HmsSim_Ed == False or HmsSim_Brake == False or HmsSim_BrakeBuild_ert_rtw == False or HmsSim_Hydraulic == False or HmsSim_Gpd == False or \
       Autogen_parameters == False or Sw_Outputs == False or AutomationSIMbuild_ert_rtw == False or AutomationSIM == False or SwitchBoardOUTSIMbuild_ert_rtw == False or \
       TestOutput == False or TestOutputSIMbuild_ert_rtw == False or AssistCmd == False or PosCmd == False or PosCmd_referenced_model_includes == False or TfdOvrMode == False or \
       TfdCurrentSpProfile == False or TfdOpMode == False or TfdOpMode_referenced_model_includes == False or StrMstrApp == False or StrMstrApp_referenced_model_includes == False or \
       StrPosHndlSpdSense == False or StrPos_SetPntRateLmtr == False or StrPos_SahNmbns == False or StrPosWhAngLimiter == False or SpdCmd == False or SpdCmd_referenced_model_includes == False or \
       StrMtrSpdLmt == False or StrMtrSpdLmt_referenced_model_includes == False or StrMstrSup == False or TrxSpd_StrMtrSpdLmt == False or WhAngErr_StrMtrSpdLmt == False or \
       WhAngErr_StrMtrSpdLmt == False or WhAng_StrMtrSpdLmt == False or Til2DuScaling == False or Cmd_Pos_Integrator == False or HmsSim_Steering_referenced_model_includes == False or \
       BrakeLgc == False or OnTrac == False or PIControlLoop == False or ParameterDiagnostics == False or ThrottleSpdCmd == False or TorqueLimiter == False or Traction_State_Machine == False or \
       accel_decel_slew == False or ProgLgc == False or BrakeLgc_referenced_model_includes == False or TractionSIM_referenced_model_includes == False:
           
        FoldersMissingFlg = True     
        
    else:
        # print message and remove folder if exists
        print ("--------------------------------------------------------------------------------\n")
        print ("                        No missing folders                                      \n")
        print ("--------------------------------------------------------------------------------\n")
        time.sleep(1)
        try:
            os.remove('MissingFiles.txt')
        except OSError, e:
            print("Error: %s - %s." % (e.filename,e.strerror))    

    # Display missing folders and write file
    if FoldersMissingFlg == True:
        print ("--------------------------------------------------------------------------------\n")
        print ("                        These Folders are missing                               \n")
        print ("--------------------------------------------------------------------------------\n")
        print (folder_list)
        f = open("MissingFiles.txt", "w")
        f.writelines(folder_list)
        f.close()

        # Send email alert of missing files and where to look for MissingFiles.txt
        email_FoldersMissing(os.getcwd())
        
    return FoldersMissingFlg   

def Traverse(Testname):
    
    file_list = []
    OneGreenHillsFolder = False
    Onetgt_MPC5777CFolder = False
    OnefilesysFolder = False 
    
    for root, dirs, files in os.walk("C:/rtc_workspaces/TmlVcmMasterBuildDef001_Sandbox"):
       
        # Looks at Root directory for /CANstack folders used currently in VCAST .env and .bat
        if'CanStackSrc' in root and '.jazz5' not in root:
            if'CANopen' in root or \
              'CRT' in root or \
              'DLLfl32c'in root or \
              'LSS' in root or \
              'DLLmul' in  root or \
              'Include' in root:
                   
                #truncate first 49 characters "C:/rtc_workspaces/TmlVcmMasterBuildDef001_Sandbox"
                root = root[TRUNCATE_LENGTH:]
                    
                # this formats the entry to one line
                file_list.append(root + '\n' )

        # Looks at Root directory for /CommonSource folders used currently in VCAST .env and .bat
        if'CommonSource' in root and '.jazz5' not in root:
            if'go_driver' in root or \
              'switch_board' in root:
                
                #truncate first 49 characters "C:/rtc_workspaces/TmlVcmMasterBuildDef001_Sandbox/"
                root = root[TRUNCATE_LENGTH:]
                    
                # this formats the entry to one line
                file_list.append(root + '\n' )

        # Looks at Root directory for /threadx_pxr40 folder used currently in VCAST .env and .bat
        if'threadx_pxr40' in root and '.jazz5' not in root:
            if'green' in root and OneGreenHillsFolder == False:
                #Top level Green folder only
                OneGreenHillsFolder = True;
                
                #truncate first 49 characters "C:/rtc_workspaces/TmlVcmMasterBuildDef001_Sandbox/"
                root = root[TRUNCATE_LENGTH:]
                    
                #this formats the entry to one line
                file_list.append(root + '\n' )

        # Looks at Root directory for VCM_APP\src folder used currently in VCAST .env and .bat
        if 'VCM_APP' in  root and 'src' in root and '.jazz5' not in root:
            # Looks with in the Autogen_API root directory
            if 'Autogen_API' in root:                         
                #truncate first 49 characters "C:/rtc_workspaces/TmlVcmMasterBuildDef001_Sandbox/"
                root = root[TRUNCATE_LENGTH:]
                    
                #this formats the entry to one line
                file_list.append(root + '\n' )

            # Looks with in the /CANopen root directory
            if 'CANopen' in root:                         
                #truncate first 49 characters "C:/rtc_workspaces/TmlVcmMasterBuildDef001_Sandbox/"
                root = root[TRUNCATE_LENGTH:]
                    
                #this formats the entry to one line
                file_list.append(root + '\n' )

            # Looks with in the /diagnostic root directory
            if 'diagnostic' in root:                         
                #truncate first 49 characters "C:/rtc_workspaces/TmlVcmMasterBuildDef001_Sandbox/"
                root = root[TRUNCATE_LENGTH:]
                    
                #this formats the entry to one line
                file_list.append(root + '\n' )

            # Looks with in the /drivers root directory
            if 'drivers' in root:
                if 'fram' in root or \
                   'io' in root or \
                   'etpu2' in root or \
                   'aecp' in root or \
                   'comm' in root or \
                   'can' in root or \
                   'xcp' in root or \
                   'xcp\cfg' in root or \
                   'xcp\usr' in root or \
                   'gyro' in root or \
                   'becp' in root or \
                   'accel' in root:  

                    # file_sys has too many sub directories to filter 
                    if 'file_sys'  not in root:
                        #truncate first 49 characters "C:/rtc_workspaces/TmlVcmMasterBuildDef001_Sandbox/"
                        root = root[TRUNCATE_LENGTH:]
                    
                        #this formats the entry to one line
                        file_list.append(root + '\n' )                                  
            if 'drivers' in root and 'file_sys' in root and 'api' in root:
                 #truncate first 49 characters "C:/rtc_workspaces/TmlVcmMasterBuildDef001_Sandbox/"
                    root = root[TRUNCATE_LENGTH:]
                    
                    #this formats the entry to one line
                    file_list.append(root + '\n' )

            # Looks with in the /ftp root directory
            if 'ftp' in root:
                         
                #truncate first 49 characters "C:/rtc_workspaces/TmlVcmMasterBuildDef001_Sandbox/"
                root = root[TRUNCATE_LENGTH:]
                    
                #this formats the entry to one line
                file_list.append(root + '\n' )

            # Looks with in the /general root directory
            if 'general' in root:
                         
                #truncate first 49 characters "C:/rtc_workspaces/TmlVcmMasterBuildDef001_Sandbox/"
                root = root[TRUNCATE_LENGTH:]
                    
                #this formats the entry to one line
                file_list.append(root + '\n' )

            # Looks with in the /hal root directory
            if 'hal' in root:
                if 'MPC5777C' in root:         
                    #truncate first 49 characters "C:/rtc_workspaces/TmlVcmMasterBuildDef001_Sandbox/"
                    root = root[TRUNCATE_LENGTH:]
                    
                    #this formats the entry to one line
                    file_list.append(root + '\n' )

                if 'MPC5777C' in root:
                    if 'ETPU2' in root:
                        #truncate first 49 characters "C:/rtc_workspaces/TmlVcmMasterBuildDef001_Sandbox/"
                        root = root[TRUNCATE_LENGTH:]
                    
                        #this formats the entry to one line
                        file_list.append(root + '\n' ) 

            # Looks with in the /interface root directory
            if 'interface' in root or \
               'codrv' in root:
                         
                #truncate first 49 characters "C:/rtc_workspaces/TmlVcmMasterBuildDef001_Sandbox/"
                root = root[TRUNCATE_LENGTH:]
                    
                #this formats the entry to one line
                file_list.append(root + '\n' )

            # Looks with in the /nvstorage root directory
            if 'nvstorage' in root or \
               'fram' in root:
                         
                #truncate first 49 characters "C:/rtc_workspaces/TmlVcmMasterBuildDef001_Sandbox/"
                root = root[TRUNCATE_LENGTH:]
                    
                #this formats the entry to one line
                file_list.append(root + '\n' )

            # Looks with in the /utilities root directory
            if 'utilities' in root:
                         
                #truncate first 49 characters "C:/rtc_workspaces/TmlVcmMasterBuildDef001_Sandbox/"
                root = root[TRUNCATE_LENGTH:]
                    
                #this formats the entry to one line
                file_list.append(root + '\n' )

            # Looks with in the /parameters root directory
            if 'parameters' in root:
                         
                #truncate first 49 characters "C:/rtc_workspaces/TmlVcmMasterBuildDef001_Sandbox/"
                root = root[TRUNCATE_LENGTH:]
                    
                #this formats the entry to one line
                file_list.append(root + '\n' )

                if 'Param_Gen2' in root:
                    #truncate first 49 characters "C:/rtc_workspaces/TmlVcmMasterBuildDef001_Sandbox/"
                    root = root[TRUNCATE_LENGTH:]
                    
                    #this formats the entry to one line
                    file_list.append(root + '\n' )
                    

        # Looks at Root directory for VCM_APP\tgt_MPC5777C folder used currently in VCAST .env and .bat
        if 'VCM_APP' in  root and '.jazz5' not in root:
            if 'tgt_MPC5777C' in root and Onetgt_MPC5777CFolder == False:

                #Top level tgt_MPC5777C folder only
                Onetgt_MPC5777CFolder = True;    
                        
                #truncate first 49 characters "C:/rtc_workspaces/TmlVcmMasterBuildDef001_Sandbox/"
                root = root[TRUNCATE_LENGTH:]
                    
                #this formats the entry to one line
                file_list.append(root + '\n' )        

        # Looks at Root directory for VCMAPP\src\Autogen folder used currently in VCAST .env and .bat
        if'VCM_APP'in root and 'src' in root and 'Autogen' in root and '.jazz5' not in root:
            if'VehicleFBSIMbuild_ert_rtw' in root and 'referenced_model_includes' not in root or \
              'TractionSIM' in root and 'referenced_model_includes' not in root or \
              'SteeringSIM' in root and 'referenced_model_includes' not in root or \
              'hydraulicSIM' in root and 'referenced_model_includes' not in root or \
              'commonSIM' in root and 'referenced_model_includes' not in root or \
              'CalibrationSIM' in root and 'referenced_model_includes' not in root or \
              'EnergySourceSIM' in root and 'referenced_model_includes' not in root or \
              'HmsSim_VcmInputs' in root and 'referenced_model_includes' not in root or \
              'HmsSim_HcmTcm' in root and 'referenced_model_includes' not in root or \
              'Vehicle_fb_processing_SIM' in root and 'referenced_model_includes' not in root or \
              'HmsSim_Traction' in root and 'referenced_model_includes' not in root or \
              'HmsSim_Ed' in root and 'referenced_model_includes' not in root or \
              'HmsSim_Hydraulic' in root and 'referenced_model_includes' not in root or \
              'HmsSim_PowerSupplies' in root and 'referenced_model_includes' not in root or \
              'HmsSim_Steering' in root and 'referenced_model_includes' not in root or \
              'HmsSim_Communication' in root and 'referenced_model_includes' not in root or \
              'HmsSim_Brake' in root and 'referenced_model_includes' not in root or \
              'HmsSim_Gpd' in root and 'referenced_model_includes' not in root or \
              'parameters' in root and 'referenced_model_includes' not in root or \
              'BusHeaders' in root and 'referenced_model_includes' not in root or \
              'sharedutils' in root and 'referenced_model_includes' not in root:
                         
                #truncate first 49 characters "C:/rtc_workspaces/TmlVcmMasterBuildDef001_Sandbox/"
                root = root[TRUNCATE_LENGTH:]
                    
                #this formats the entry to one line
                file_list.append(root + '\n' )

            # these extra files in BECP
            if Testname == eTestname.BECP_MAIN_INTEGRATION:
                donothing = True

            # these extra files in SWITCHBOARD and STEERING
            if Testname == eTestname.SWITCH_BOARD_INTEGRATION or Testname == eTestname.STEERING_INTEGRATION:   
                if 'Sw_Outputs' in root and 'referenced_model_includes' not in root or \
                   'AutomationSIM' in root and 'referenced_model_includes' not in root or \
                   'SwitchBoardOUTSIMbuild_ert_rtw' in root and 'referenced_model_includes' not in root or \
                   'TestOutputSIMbuild_ert_rtw' in root and 'referenced_model_includes' not in root or \
                   'TestOutput' in root and 'referenced_model_includes' not in root:
                    #truncate first 49 characters "C:/rtc_workspaces/TmlVcmMasterBuildDef001_Sandbox/"
                    root = root[TRUNCATE_LENGTH:]
                    
                    #this formats the entry to one line
                    file_list.append(root + '\n' )

            # these extra files for STEERING
            if Testname == eTestname.STEERING_INTEGRATION:
                if 'AssistCmd' in root and 'referenced_model_includes' not in root or \
                   'PosCmd' in root and 'referenced_model_includes' not in root or \
                   'PosCmd' in root and 'referenced_model_includes' in root or \
                   'TfdOvrMode' in root and 'referenced_model_includes' not in root or \
                   'TfdCurrentSpProfile' in root and 'referenced_model_includes' not in root or \
                   'TfdOpMode' in root and 'referenced_model_includes' not in root or \
                   'TfdOpMode' in root and 'referenced_model_includes' in root or \
                   'StrMstrApp' in root and 'referenced_model_includes' not in root or \
                   'StrMstrApp' in root and 'referenced_model_includes' in root or \
                   'StrPosHndlSpdSense' in root and 'referenced_model_includes' not in root or \
                   'StrPos_SetPntRateLmtr' in root and 'referenced_model_includes' not in root or \
                   'StrPos_SahNmbns' in root and 'referenced_model_includes' not in root or \
                   'StrPosWhAngLimiter' in root and 'referenced_model_includes' not in root or \
                   'SpdCmd' in root and 'referenced_model_includes' not in root or \
                   'SpdCmd' in root and 'referenced_model_includes' in root or \
                   'StrMtrSpdLmt' in root and 'referenced_model_includes' not in root or \
                   'StrMtrSpdLmt' in root and 'referenced_model_includes' in root or \
                   'StrMstrSup' in root and 'referenced_model_includes' not in root or \
                   'TrxSpd_StrMtrSpdLmt' in root and 'referenced_model_includes' not in root or \
                   'WhAngErr_StrMtrSpdLmt' in root and 'referenced_model_includes' not in root or \
                   'WhAng_StrMtrSpdLmt' in root and 'referenced_model_includes' not in root or \
                   'Til2DuScaling' in root and 'referenced_model_includes' not in root or \
                   'Cmd_Pos_Integrator' in root and 'referenced_model_includes' not in root or \
                   'HmsSim_Steering' in root and 'referenced_model_includes' in root:
                    #truncate first 49 characters "C:/rtc_workspaces/TmlVcmMasterBuildDef001_Sandbox/"
                    root = root[TRUNCATE_LENGTH:]
                    
                    #this formats the entry to one line
                    file_list.append(root + '\n' )

            # these extra files for TRACTION
            if Testname == eTestname.TRACTION_INTEGRATION:
                if 'BrakeLgc' in root and 'referenced_model_includes' not in root or \
                   'OnTrac' in root and 'referenced_model_includes' not in root or \
                   'PIControlLoop' in root and 'referenced_model_includes' not in root or \
                   'ParameterDiagnostics' in root and 'referenced_model_includes' not in root or \
                   'ThrottleSpdCmd' in root and 'referenced_model_includes' not in root or \
                   'TorqueLimiter' in root and 'referenced_model_includes' not in root or \
                   'Traction_State_Machine' in root and 'referenced_model_includes' not in root or \
                   'accel_decel_slew' in root and 'referenced_model_includes' not in root or \
                   'ProgLgc' in root and 'referenced_model_includes' not in root or \
                   'BrakeLgc' in root and 'referenced_model_includes' in root or \
                   'TractionSIM' in root and 'referenced_model_includes' in root:
                    #truncate first 49 characters "C:/rtc_workspaces/TmlVcmMasterBuildDef001_Sandbox/"
                    root = root[TRUNCATE_LENGTH:]
                    
                    #this formats the entry to one line
                    file_list.append(root + '\n' )

                    
        """
        # these extra files in EVENTMANAGER and STEERING
        if Testname == eTestname.EVENT_MANAGER_INTEGRATION or Testname == eTestname.STEERING_INTEGRATION:
            if 'drivers' in root and 'flash' in root:
                #if 'flash' in root:
                #truncate first 49 characters "C:/rtc_workspaces/TmlVcmMasterBuildDef001_Sandbox/"
                root = root[TRUNCATE_LENGTH:]
                    
                #this formats the entry to one line
                file_list.append(root + '\n' )

                # Removed these March 2 2017
                
                if'AnalyzerSIM' in root and 'referenced_model_includes' not in root or \
                  'Sw_Outputs' in root and 'referenced_model_includes' not in root or \
                  'AutomationSIM' in root and 'referenced_model_includes' not in root or \
                  'SwitchBoardOUTSIMbuild_ert_rtw' in root and 'referenced_model_includes' not in root or \
                  'TestOutputSIMbuild_ert_rtw' in root and 'referenced_model_includes' not in root or \
                  'TestOutput' in root and 'referenced_model_includes' not in root:
                    #truncate first 49 characters "C:/rtc_workspaces/TmlVcmMasterBuildDef001_Sandbox/"
                     root = root[TRUNCATE_LENGTH:]
                    
                     #this formats the entry to one line
                     file_list.append(root + '\n' )
                """
                                 
           
    # write to the appropriate .txt file to save the entry
    if Testname == eTestname.BECP_MAIN_INTEGRATION:
        # update file test name
        TestNamePathfile = "BECP_MAIN_INTEGRATION_PathList.txt"
        
    elif Testname == eTestname.SWITCH_BOARD_INTEGRATION:
        # update file test name
        TestNamePathfile = "SWITCH_BOARD_INTEGRATION_PathList.txt"
        
    elif Testname == eTestname.EVENT_MANAGER_INTEGRATION:
        # update file test name
        TestNamePathfile = "EVENT_MANAGER_INTEGRATION_PathList.txt"

    elif Testname == eTestname.EXTERNAL_ALARM_INTEGRATION:
        # update file test name
        TestNamePathfile = "EXTERNAL_ALARM_INTEGRATION_PathList.txt"

    elif Testname == eTestname.GYRO_DRIVER_INTEGRATION:
        # update file test name
        TestNamePathfile = "GYRO_DRIVER_INTEGRATION_PathList.txt"
        
    elif Testname == eTestname.FEATURES_INTERFACE_INTEGRATION:
        # update file test name
        TestNamePathfile = "FEATURES_INTERFACE_INTEGRATION_PathList.txt"

    elif Testname == eTestname.SUPV_INTERFACE_INTEGRATION:
        # update file test name
        TestNamePathfile = "SUPV_INTERFACE_INTEGRATION_PathList.txt"

    elif Testname == eTestname.IMM_INTERFACE_INTEGRATION:
        # update file test name
        TestNamePathfile = "IMM_INTERFACE_INTEGRATION_PathList.txt"

    elif Testname == eTestname.OS_INTERFACE_INTEGRATION:
        # update file test name
        TestNamePathfile = "OS_INTERFACE_INTEGRATION_PathList.txt"

    elif Testname == eTestname.IP_INTERFACE_INTEGRATION:
        # update file test name
        TestNamePathfile = "IP_INTERFACE_INTEGRATION_PathList.txt"

    elif Testname == eTestname.STEERING_INTEGRATION:
        # update file test name
        TestNamePathfile = "STEERING_INTEGRATION_PathList.txt"

    elif Testname == eTestname.MANIFEST_MANAGER_INTEGRATION:
        # update file test name
        TestNamePathfile = "MANIFEST_MANAGER_INTEGRATION_PathList.txt"

    elif Testname == eTestname.TRACTION_INTEGRATION:
        # update file test name
        TestNamePathfile = "TRACTION_INTEGRATION_PathList.txt"
        
    elif Testname == eTestname.FRAM_DRIVER_INTEGRATION:
        # update file test name
        TestNamePathfile = "FRAM_DRIVER_INTEGRATION_PathList.txt"

    elif Testname == eTestname.UTILITIES_INTEGRATION:
        # update file test name
        TestNamePathfile = "UTILITIES_INTEGRATION_PathList.txt"

    elif Testname == eTestname.DIAGNOSTICS_INTEGRATION:
         # update file test name
        TestNamePathfile = "DIAGNOSTICS_INTEGRATION_PathList.txt"
    
    else:    
        # update file test name
        TestNamePathfile = "AECP_MAIN_INTEGRATION_PathList.txt"
    
    f = open(TestNamePathfile, "w")
    f.writelines(file_list)
    f.close()

    # Remove blank spaces
    RemoveBlankSpace(TestNamePathfile)

    # show the current list of paths saved
    f = open(TestNamePathfile, "r")

     
    print ("--------------------------------------------------------------------------------\n")
    print ("                        Current directory paths                                 \n")
    print ("--------------------------------------------------------------------------------\n")
    print (" The Test set running is: {} ".format(Testname))
    print ("")
    print(f.read())
    f.close()
    time.sleep(1)
    
    print ("--------------------------------------------------------------------------------\n")
    print (" Finished traversing the: rtc_workspaces/TmlVcmMasterBuildDef001_Sandbox folder \n")
    print (" The Paths for Test is finished: {}" .format(Testname))                           
    print ("--------------------------------------------------------------------------------\n")
    return True

def RemoveBlankSpace(filename):
    new_contents = []
    
    # Get file contents
    #fd = open('PathList.txt')
    fd = open(filename)
    contents = fd.readlines()
    fd.close()

    # Get rid of empty lines
    for line in contents:
        # Strip whitespace, should leave nothing if empty line was just "\n"
        if not line.strip():
            continue
        # We got something, save it
        else:
            new_contents.append(line)
    # Print file sans empty lines
    #print "".join(new_contents)

    # Reopen this file and rewrite it without spaces
    #f = open("PathList.txt", "w")
    f = open(filename, "w")
    f.writelines(new_contents )
    f.close()

def Vcast_envFile(Testname, fRunSimulated):
   
    new_contents = []
    string1 = 'ENVIRO.SEARCH_LIST: $(CROWN_SOURCE_BASE)'

    # On-Target Options 
    aecp_header_options = [ 'ENVIRO.NEW \n'
                            'ENVIRO.NAME:AECP_MAIN_INTEGRATION \n'
                            'ENVIRO.COVERAGE_TYPE:Statement+Branch \n'
                            'ENVIRO.STUB_BY_FUNCTION:aecp_main \n'
                            'ENVIRO.STUB_BY_FUNCTION:imm_interface \n'
                            'ENVIRO.STUB_BY_FUNCTION:immcomm_driver \n'
                            'ENVIRO.WHITE_BOX:YES \n'
                            'ENVIRO.MAX_VARY_RANGE: 20 \n'
                            'ENVIRO.STUB: ALL_BY_PROTOTYPE \n'
                           ]
    
    becp_header_options = [ 'ENVIRO.NEW \n'
                            'ENVIRO.NAME:BECP_MAIN_INTEGRATION \n'
                            'ENVIRO.COVERAGE_TYPE:Statement+Branch \n'
                            'ENVIRO.STUB_BY_FUNCTION:becp_main \n'
                            'ENVIRO.STUB_BY_FUNCTION:supv_interface \n'
                            'ENVIRO.WHITE_BOX:YES \n'
                            'ENVIRO.MAX_VARY_RANGE: 20 \n'
                            'ENVIRO.STUB: ALL_BY_PROTOTYPE \n'
                           ]

    switchboard_header_options = [ 'ENVIRO.NEW \n'
                            'ENVIRO.NAME:SWITCH_BOARD_INTEGRATION \n'
                            'ENVIRO.COVERAGE_TYPE:Statement+Branch \n'
                            'ENVIRO.STUB_BY_FUNCTION:ag_common \n'
                            'ENVIRO.STUB_BY_FUNCTION:ag_input_router \n'
                            'ENVIRO.STUB_BY_FUNCTION:ag_output_router \n'
                            'ENVIRO.STUB_BY_FUNCTION:param \n'
                            'ENVIRO.STUB_BY_FUNCTION:switchboard \n'       
                            'ENVIRO.WHITE_BOX:YES \n'
                            'ENVIRO.MAX_VARY_RANGE: 20 \n'
                            'ENVIRO.STUB: ALL_BY_PROTOTYPE \n'
                           ]
    eventmanager_header_options = ['ENVIRO.NEW \n'
                            'ENVIRO.NAME:EVENT_MANAGER_INTEGRATION \n'
                            'ENVIRO.COVERAGE_TYPE:Statement+Branch \n'
                            'ENVIRO.INDUSTRY_MODE:IEC-61508 (Industrial) \n'
                            'ENVIRO.STUB_BY_FUNCTION:EventManager \n'
                            'ENVIRO.STUB_BY_FUNCTION:NvMemory \n'
                            'ENVIRO.STUB_BY_FUNCTION:OS_Interface \n'
                            'ENVIRO.STUB_BY_FUNCTION:XcpUserFlash \n'       
                            'ENVIRO.STUB_BY_FUNCTION:crc \n'
                            'ENVIRO.STUB_BY_FUNCTION:debug_support \n'
                            'ENVIRO.STUB_BY_FUNCTION:imm_interface \n'
                            'ENVIRO.STUB_BY_FUNCTION:immcomm_driver \n'
                            'ENVIRO.STUB_BY_FUNCTION:instrument \n'
                            'ENVIRO.STUB_BY_FUNCTION:io_thread \n'
                            'ENVIRO.STUB_BY_FUNCTION:param \n'
                            'ENVIRO.STUB_BY_FUNCTION:pd_data_test \n'
                            'ENVIRO.STUB_BY_FUNCTION:watchdog \n'
                            'ENVIRO.WHITE_BOX:YES \n'
                            'ENVIRO.MAX_VARY_RANGE: 20 \n'
                            'ENVIRO.STUB: ALL_BY_PROTOTYPE \n'
                                   ]
    externalalarm_header_options = [ 'ENVIRO.NEW \n'
                            'ENVIRO.NAME:EXTERNAL_ALARM_INTEGRATION \n'
                            'ENVIRO.COVERAGE_TYPE:Statement+Branch \n'
                            'ENVIRO.STUB_BY_FUNCTION:external_alarm \n'
                            'ENVIRO.WHITE_BOX:YES \n'
                            'ENVIRO.MAX_VARY_RANGE: 20 \n'
                            'ENVIRO.STUB: ALL_BY_PROTOTYPE \n'
                            ]

    gyrodriver_header_options = [ 'ENVIRO.NEW \n'
                            'ENVIRO.NAME:GYRO_DRIVER_INTEGRATION \n'
                            'ENVIRO.COVERAGE_TYPE:Statement+Branch \n'
                            'ENVIRO.INDUSTRY_MODE:IEC-61508 (Industrial) \n'
                            'ENVIRO.STUB_BY_FUNCTION:gyro_driver \n'
                            'ENVIRO.WHITE_BOX:YES \n'
                            'ENVIRO.MAX_VARY_RANGE: 20 \n'
                            'ENVIRO.STUB: ALL_BY_PROTOTYPE \n'
                            ]
    
    features_interface_header_options = [  'ENVIRO.NEW \n'
                            'ENVIRO.NAME: FEATURES_INTERFACE_INTEGRATION \n'
                            'ENVIRO.STUB_BY_FUNCTION: features_interface \n'
                            'ENVIRO.WHITE_BOX: YES \n'
                            'ENVIRO.COVERAGE_TYPE: Statement+Branch \n'
                            'ENVIRO.INDUSTRY_MODE: IEC-61508 (Industrial) \n'                            
                            'ENVIRO.LIBRARY_STUBS: \n'  
                            'ENVIRO.STUB: ALL_BY_PROTOTYPE \n'
                            'ENVIRO.COMPILER: CC \n'
                            'ENVIRO.TYPE_HANDLED_DIRS_ALLOWED: \n'
                            'ENVIRO.USER_GLOBALS: \n'
                            ' \n'
                            '/***************************************************************************** \n'
                            'S0000008.c: This file contains the definitions of variables used in user code. \n'
                            'Preface all variable declarations with VCAST_USER_GLOBALS_EXTERN to ensure \n'
                            'that only one definition of the variable is created in the test harness. \n' 
                            '*****************************************************************************/ \n'
                            ' \n'
                            '#ifndef VCAST_USER_GLOBALS_EXTERN \n'
                            '#define VCAST_USER_GLOBALS_EXTERN \n'
                            '#endif \n'
                            ' \n'
                            '#ifdef __cplusplus \n'
                            'extern "C"{ \n'
                            '#endif \n'
                            '  VCAST_USER_GLOBALS_EXTERN int VECTORCAST_INT1; \n'
                            '  VCAST_USER_GLOBALS_EXTERN int VECTORCAST_INT2; \n'
                            '  VCAST_USER_GLOBALS_EXTERN int VECTORCAST_INT3; \n'
                            '  VCAST_USER_GLOBALS_EXTERN unsigned long VECTORCAST_ULONG; \n'
                            '#ifndef VCAST_NO_FLOAT \n' 
                            '  VCAST_USER_GLOBALS_EXTERN float VECTORCAST_FLT1; \n'
                            '#endif \n'   
                            '  VCAST_USER_GLOBALS_EXTERN char VECTORCAST_STR1[8]; \n'
                            ' \n '
                            '  VCAST_USER_GLOBALS_EXTERN int  VECTORCAST_BUFFER[4]; \n'
                            '#ifdef __cplusplus \n'
                            '} \n'
                            '#endif \n'
                            'ENVIRO.END_USER_GLOBALS: \n'
                            'ENVIRO.DRIVER_PREFIX_USER_CODE: \n'
                            'ENVIRO.DRIVER_PREFIX_USER_CODE_FILE:aecp_main \n'
                            'aecp_driver_t testdriver; \n' 
                            'char szTxBuffer[256]; \n' 
                            'ENVIRO.END_DRIVER_PREFIX_USER_CODE_FILE: \n'
                            'ENVIRO.DRIVER_PREFIX_USER_CODE_FILE:ag_input_router \n'
                            'static void * const spvAG_ModuleInputAddressArray[AG_NUMB_MODULE_INPUTS]; \n' 
                            'ENVIRO.END_DRIVER_PREFIX_USER_CODE_FILE: \n'
                            'ENVIRO.DRIVER_PREFIX_USER_CODE_FILE:becp_main \n'
                            'becp_driver_t testdriver; \n' 
                            'uint16_t uwmessage[128]; \n' 
                            'ENVIRO.END_DRIVER_PREFIX_USER_CODE_FILE: \n'
                            'ENVIRO.END_DRIVER_PREFIX_USER_CODE: \n'
                            ]

    supv_interface_header_options = [ 'ENVIRO.NEW \n'
                            'ENVIRO.NAME: SUPV_INTERFACE_INTEGRATION \n'
                            'ENVIRO.STUB_BY_FUNCTION: features_interface \n'
                            'ENVIRO.STUB_BY_FUNCTION: supv_interface \n'
                            'ENVIRO.WHITE_BOX: YES \n'
                            'ENVIRO.COVERAGE_TYPE: Statement+Branch \n'
                            'ENVIRO.INDUSTRY_MODE: IEC-61508 (Industrial) \n'
                            'ENVIRO.LIBRARY_STUBS: \n'  
                            'ENVIRO.ADDITIONAL_STUB: gfFI_FeaturesConfigured() \n'
                            'ENVIRO.ADDITIONAL_STUB: gulOS_TimerChange(pTimer,ulInitialTicks,ulRescheduleTicks) \n'
                            'ENVIRO.STUB: ALL_BY_PROTOTYPE \n'
                            'ENVIRO.COMPILER: CC \n'
                            ]

    imm_interface_header_options = [ 'ENVIRO.NEW \n'
                            'ENVIRO.NAME: IMM_INTERFACE_INTEGRATION \n'
                            'ENVIRO.STUB_BY_FUNCTION: imm_interface \n'
                            'ENVIRO.WHITE_BOX: YES \n'
                            'ENVIRO.COVERAGE_TYPE: Statement+Branch \n'
                            'ENVIRO.INDUSTRY_MODE: IEC-61508 (Industrial) \n'
                            'ENVIRO.LIBRARY_STUBS: \n'  
                            'ENVIRO.ADDITIONAL_STUB: gfFI_FeaturesConfigured() \n'
                            'ENVIRO.ADDITIONAL_STUB: gulOS_TimerChange(pTimer,ulInitialTicks,ulRescheduleTicks) \n'
                            'ENVIRO.STUB: ALL_BY_PROTOTYPE \n'
                            'ENVIRO.COMPILER: CC \n'
                             ]

    os_interface_header_options = [ 'ENVIRO.NEW \n'
                            'ENVIRO.NAME: OS_INTERFACE_INTEGRATION \n'
                            'ENVIRO.COVERAGE_TYPE:Statement+Branch \n'
                            'ENVIRO.INDUSTRY_MODE:IEC-61508 (Industrial) \n'
                            'ENVIRO.STUB_BY_FUNCTION:OS_Interface \n'
                            'ENVIRO.WHITE_BOX:YES \n'
                            'ENVIRO.MAX_VARY_RANGE: 20 \n'
                            'ENVIRO.STUB: ALL_BY_PROTOTYPE \n'
                             ]
    steering_header_options = [ 'ENVIRO.NEW \n'
                            'ENVIRO.NAME: STEERING_INTEGRATION \n'
                            'ENVIRO.STUB_BY_FUNCTION: AssistCmd \n'
                            'ENVIRO.STUB_BY_FUNCTION: Cmd_Pos_Integrator \n'
                            'ENVIRO.STUB_BY_FUNCTION: HmsSim_Steering \n'
                            'ENVIRO.STUB_BY_FUNCTION: HmsSim_SteeringBuild \n'
                            'ENVIRO.STUB_BY_FUNCTION: PosCmd \n'
                            'ENVIRO.STUB_BY_FUNCTION: PosCmd_data \n'
                            'ENVIRO.STUB_BY_FUNCTION: SpdCmd \n'
                            'ENVIRO.STUB_BY_FUNCTION: SteeringSIM \n'
                            'ENVIRO.STUB_BY_FUNCTION: SteeringSIMbuild \n'
                            'ENVIRO.STUB_BY_FUNCTION: StrMstrApp \n'
                            'ENVIRO.STUB_BY_FUNCTION: StrMstrSup \n'
                            'ENVIRO.STUB_BY_FUNCTION: StrMtrSpdLmt \n'
                            'ENVIRO.STUB_BY_FUNCTION: StrPosHndlSpdSense \n'
                            'ENVIRO.STUB_BY_FUNCTION: StrPosWhAngLimiter \n'
                            'ENVIRO.STUB_BY_FUNCTION: StrPos_SahNmbns \n'
                            'ENVIRO.STUB_BY_FUNCTION: StrPos_SetPntRateLmtr \n'
                            'ENVIRO.STUB_BY_FUNCTION: TfdCurrentSpProfile \n'
                            'ENVIRO.STUB_BY_FUNCTION: TfdOpMode \n'
                            'ENVIRO.STUB_BY_FUNCTION: TfdOvrMode \n'
                            'ENVIRO.STUB_BY_FUNCTION: Til2DuScaling \n'
                            'ENVIRO.STUB_BY_FUNCTION: TrxSpd_StrMtrSpdLmt \n'
                            'ENVIRO.STUB_BY_FUNCTION: WhAngErr_StrMtrSpdLmt \n'
                            'ENVIRO.STUB_BY_FUNCTION: WhAng_StrMtrSpdLmt \n'
                            'ENVIRO.WHITE_BOX: YES \n'
                            'ENVIRO.COVERAGE_TYPE: Statement+Branch \n'
                            'ENVIRO.INDUSTRY_MODE: IEC-61508 (Industrial) \n'
                            'ENVIRO.LIBRARY_STUBS: \n'  
                            'ENVIRO.STUB: ALL_BY_PROTOTYPE \n'
                            'ENVIRO.COMPILER: CC \n'
                            'ENVIRO.TYPE_HANDLED_DIRS_ALLOWED: \n' 
                            'ENVIRO.DRIVER_PREFIX_USER_CODE: \n'
                            'ENVIRO.DRIVER_PREFIX_USER_CODE_FILE:aecp_main \n'
                            'aecp_driver_t testdriver; \n' 
                            'char szTxBuffer[256]; \n'
                            'ENVIRO.END_DRIVER_PREFIX_USER_CODE_FILE: \n'
                            'ENVIRO.DRIVER_PREFIX_USER_CODE_FILE:ag_input_router \n'
                            'static void * const spvAG_ModuleInputAddressArray[AG_NUMB_MODULE_INPUTS]; \n'
                            'ENVIRO.END_DRIVER_PREFIX_USER_CODE_FILE: \n'
                            'ENVIRO.DRIVER_PREFIX_USER_CODE_FILE:becp_main \n'
                            '\n'
                            'becp_driver_t testdriver; \n'
                            'uint16_t uwmessage[128]; \n'
                            'ENVIRO.END_DRIVER_PREFIX_USER_CODE_FILE: \n'
                            'ENVIRO.DRIVER_PREFIX_USER_CODE_FILE:fram_driver \n'
                            '\n'
                            '\n'
                            'ENVIRO.END_DRIVER_PREFIX_USER_CODE_FILE: \n'
                            'ENVIRO.END_DRIVER_PREFIX_USER_CODE: \n'
                            'ENVIRO.UNIT_PREFIX_USER_CODE: \n'
                            'ENVIRO.UNIT_PREFIX_USER_CODE_FILE:aecp_main \n'
                            '\n'
                            '\n'
                            'ENVIRO.END_UNIT_PREFIX_USER_CODE_FILE: \n'
                            'ENVIRO.END_UNIT_PREFIX_USER_CODE: \n'           
                             ]
    
    ip_interface_header_options = [ 'ENVIRO.NEW \n'
                            'ENVIRO.NAME: IP_INTERFACE_INTEGRATION \n'
                            'ENVIRO.STUB_BY_FUNCTION: IP_Interface \n'
                            'ENVIRO.WHITE_BOX: YES \n'
                            'ENVIRO.COVERAGE_TYPE: Statement+Branch \n'
                            'ENVIRO.INDUSTRY_MODE: IEC-61508 (Industrial) \n'
                            'ENVIRO.LIBRARY_STUBS: \n'  
                            'ENVIRO.ADDITIONAL_STUB: gfFI_FeaturesConfigured() \n'
                            'ENVIRO.ADDITIONAL_STUB: gulOS_TimerChange(pTimer,ulInitialTicks,ulRescheduleTicks) \n'
                            'ENVIRO.STUB: ALL_BY_PROTOTYPE \n'
                            'ENVIRO.COMPILER: CC \n'
                             ]
    manifest_manager_header_options = [ 'ENVIRO.NEW \n'
                            'ENVIRO.NAME: MANIFEST_MANAGER_INTEGRATION \n'
                            'ENVIRO.STUB_BY_FUNCTION: Manifest_mgr \n'
                            'ENVIRO.WHITE_BOX: YES \n'
                            'ENVIRO.COVERAGE_TYPE: Statement+Branch \n'
                            'ENVIRO.INDUSTRY_MODE: IEC-61508 (Industrial) \n'
                            'ENVIRO.LIBRARY_STUBS: \n'  
                            'ENVIRO.STUB: ALL_BY_PROTOTYPE \n'
                            'ENVIRO.COMPILER: CC \n'
                            ]
    traction_header_options = [ 'ENVIRO.NEW \n'
                                'ENVIRO.NAME:TRACTION_INTEGRATION \n'
                                'ENVIRO.COVERAGE_TYPE:Statement+Branch \n'
                                'ENVIRO.INDUSTRY_MODE:IEC-61508 (Industrial) \n'
                                'ENVIRO.STUB_BY_FUNCTION:GetSpeed \n'
                                'ENVIRO.STUB_BY_FUNCTION:OnTrac \n'
                                'ENVIRO.STUB_BY_FUNCTION:PIControlLoop \n'
                                'ENVIRO.STUB_BY_FUNCTION:ParameterDiagnostics \n'
                                'ENVIRO.STUB_BY_FUNCTION:ThrottleSpdCmd \n'
                                'ENVIRO.STUB_BY_FUNCTION:TorqueLimiter \n'
                                'ENVIRO.STUB_BY_FUNCTION:TractionSIM \n'
                                'ENVIRO.STUB_BY_FUNCTION:TractionSIMbuild \n'
                                'ENVIRO.STUB_BY_FUNCTION:Traction_State_Machine \n'
                                'ENVIRO.STUB_BY_FUNCTION:accel_decel_slew \n'
                                'ENVIRO.WHITE_BOX:YES \n'
                                'ENVIRO.MAX_VARY_RANGE: 20 \n'
                                'ENVIRO.STUB: ALL_BY_PROTOTYPE \n'
                            ]
    fram_driver_header_options = [ 'ENVIRO.NEW \n'
                                'ENVIRO.NAME: FRAM_DRIVER_INTEGRATION \n'
                                'ENVIRO.STUB_BY_FUNCTION: NvMemory \n'
                                'ENVIRO.STUB_BY_FUNCTION: fram_driver \n'
                                'ENVIRO.WHITE_BOX: YES \n'
                                'ENVIRO.COVERAGE_TYPE: Statement+Branch \n'
                                'ENVIRO.INDUSTRY_MODE: IEC-61508 (Industrial) \n'
                                'ENVIRO.LIBRARY_STUBS: \n'  
                                'ENVIRO.STUB: ALL_BY_PROTOTYPE \n'
                                'ENVIRO.COMPILER: CC \n'
                                'ENVIRO.TYPE_HANDLED_DIRS_ALLOWED: \n' 
                                'ENVIRO.UNIT_APPENDIX_USER_CODE: \n'
                                'ENVIRO.UNIT_APPENDIX_USER_CODE_FILE: NvMemory \n'
                                'static fram_serial_port_t fram_port; \n'
                                'static fram_drv_obj_t fram_drv; \n'   
                                'ENVIRO.END_UNIT_APPENDIX_USER_CODE_FILE: \n'
                                'ENVIRO.END_UNIT_APPENDIX_USER_CODE: \n'
                                'ENVIRO.DRIVER_PREFIX_USER_CODE: \n'
                                'ENVIRO.DRIVER_PREFIX_USER_CODE_FILE: NvMemory \n'  
                                'static fram_serial_port_t fram_port; \n'
                                'static fram_drv_obj_t fram_drv; \n'
                                '//gfFRAM_Init(&fram_drv, &fram_port); \n'
                                'ENVIRO.END_DRIVER_PREFIX_USER_CODE_FILE: \n'
                                'ENVIRO.DRIVER_PREFIX_USER_CODE_FILE:aecp_main \n'
                                'aecp_driver_t testdriver; \n'
                                'char szTxBuffer[256]; \n'
                                'ENVIRO.END_DRIVER_PREFIX_USER_CODE_FILE: \n'
                                'ENVIRO.DRIVER_PREFIX_USER_CODE_FILE:ag_input_router \n'
                                'static void * const spvAG_ModuleInputAddressArray[AG_NUMB_MODULE_INPUTS]; \n'
                                'ENVIRO.END_DRIVER_PREFIX_USER_CODE_FILE: \n'
                                'ENVIRO.DRIVER_PREFIX_USER_CODE_FILE:becp_main \n'
                                'becp_driver_t testdriver; \n'
                                'uint16_t uwmessage[128]; \n'
                                'ENVIRO.END_DRIVER_PREFIX_USER_CODE_FILE: \n'
                                'ENVIRO.END_DRIVER_PREFIX_USER_CODE: \n'
                                'ENVIRO.UNIT_PREFIX_USER_CODE: \n'
                                'ENVIRO.UNIT_PREFIX_USER_CODE_FILE:aecp_main \n'
                                'ENVIRO.END_UNIT_PREFIX_USER_CODE_FILE: \n'
                                'ENVIRO.END_UNIT_PREFIX_USER_CODE: \n'                                  
                              ]
    utilities_header_options = [ 'ENVIRO.NEW \n'
                                'ENVIRO.NAME: UTILITIES_INTEGRATION \n'
                                'ENVIRO.STUB_BY_FUNCTION: app_info \n'
                                'ENVIRO.STUB_BY_FUNCTION: crc \n'
                                'ENVIRO.STUB_BY_FUNCTION: debug_support \n'
                                'ENVIRO.STUB_BY_FUNCTION: flash_validate \n'
                                'ENVIRO.STUB_BY_FUNCTION: instrument \n'
                                'ENVIRO.STUB_BY_FUNCTION: stopwatch \n'
                                'ENVIRO.STUB_BY_FUNCTION: util \n'
                                'ENVIRO.WHITE_BOX: YES \n'
                                'ENVIRO.COVERAGE_TYPE: Statement+Branch \n'
                                'ENVIRO.INDUSTRY_MODE: IEC-61508 (Industrial) \n'
                                'ENVIRO.LIBRARY_STUBS: \n'  
                                'ENVIRO.STUB: ALL_BY_PROTOTYPE \n'
                                'ENVIRO.COMPILER: CC \n'
                                'ENVIRO.TYPE_HANDLED_DIRS_ALLOWED: \n'
                                 ]
    diagnostics_header_options = ['ENVIRO.NEW \n'
                                'ENVIRO.NAME: DIAGNOSTICS_INTEGRATION \n'
                                'ENVIRO.STUB_BY_FUNCTION: devrep_external_event_handler \n'
                                'ENVIRO.STUB_BY_FUNCTION: devrep_hdm \n'
                                'ENVIRO.STUB_BY_FUNCTION: devrep_iom \n'
                                'ENVIRO.STUB_BY_FUNCTION: devrep_scm \n'
                                'ENVIRO.STUB_BY_FUNCTION: devrep_tcm_hcm \n'
                                'ENVIRO.STUB_BY_FUNCTION: devrep_vcm_model \n'
                                'ENVIRO.STUB_BY_FUNCTION: devrep_vcm_supv \n'
                                'ENVIRO.WHITE_BOX: YES \n'
                                'ENVIRO.COVERAGE_TYPE: Statement+Branch \n'
                                'ENVIRO.INDUSTRY_MODE: IEC-61508 (Industrial) \n'
                                'ENVIRO.LIBRARY_STUBS: \n'  
                                'ENVIRO.STUB: ALL_BY_PROTOTYPE \n'
                                'ENVIRO.COMPILER: CC \n'
                                'ENVIRO.TYPE_HANDLED_DIRS_ALLOWED: \n'
                                 ]
    # Simulation header Options

    steering_header_options_simulation = ['ENVIRO.NEW \n'
                            'ENVIRO.NAME: STEERING_SIMULATION_INTEGRATION \n'
                            'ENVIRO.COVERAGE_TYPE: Statement+Branch \n'
                            'ENVIRO.INDUSTRY_MODE: IEC-61508 (Industrial) \n'
                            'ENVIRO.STUB_BY_FUNCTION: AssistCmd \n'
                            'ENVIRO.STUB_BY_FUNCTION: Cmd_Pos_Integrator \n'
                            'ENVIRO.STUB_BY_FUNCTION: HmsSim_Steering \n'
                            'ENVIRO.STUB_BY_FUNCTION: HmsSim_SteeringBuild \n'
                            'ENVIRO.STUB_BY_FUNCTION: PosCmd \n'
                            'ENVIRO.STUB_BY_FUNCTION: PosCmd_data \n'
                            'ENVIRO.STUB_BY_FUNCTION: SpdCmd \n'
                            'ENVIRO.STUB_BY_FUNCTION: SteeringSIM \n'
                            'ENVIRO.STUB_BY_FUNCTION: SteeringSIMbuild \n'
                            'ENVIRO.STUB_BY_FUNCTION: StrMstrApp \n'
                            'ENVIRO.STUB_BY_FUNCTION: StrMstrSup \n'
                            'ENVIRO.STUB_BY_FUNCTION: StrMtrSpdLmt \n'
                            'ENVIRO.STUB_BY_FUNCTION: StrPosHndlSpdSense \n'
                            'ENVIRO.STUB_BY_FUNCTION: StrPosWhAngLimiter \n'
                            'ENVIRO.STUB_BY_FUNCTION: StrPos_SahNmbns \n'
                            'ENVIRO.STUB_BY_FUNCTION: StrPos_SetPntRateLmtr \n'
                            'ENVIRO.STUB_BY_FUNCTION: TfdCurrentSpProfile \n'
                            'ENVIRO.STUB_BY_FUNCTION: TfdOpMode \n'
                            'ENVIRO.STUB_BY_FUNCTION: TfdOvrMode \n'
                            'ENVIRO.STUB_BY_FUNCTION: Til2DuScaling \n'
                            'ENVIRO.STUB_BY_FUNCTION: TrxSpd_StrMtrSpdLmt \n'
                            'ENVIRO.STUB_BY_FUNCTION: WhAngErr_StrMtrSpdLmt \n'
                            'ENVIRO.STUB_BY_FUNCTION: WhAng_StrMtrSpdLmt \n'
                            'ENVIRO.WHITE_BOX: YES \n'
                            'ENVIRO.MAX_VARY_RANGE: 20 \n'
                            'ENVIRO.STUB: ALL_BY_PROTOTYPE \n'
                             ]

    traction_header_options_simulation = [ 'ENVIRO.NEW \n'
                                'ENVIRO.NAME: TRACTION_SIMULATION_INTEGRATION \n'
                                'ENVIRO.STUB_BY_FUNCTION: BrakeLgc \n'
                                'ENVIRO.STUB_BY_FUNCTION: OnTrac \n'
                                'ENVIRO.STUB_BY_FUNCTION: PIControlLoop \n'
                                'ENVIRO.STUB_BY_FUNCTION: ParameterDiagnostics \n'
                                'ENVIRO.STUB_BY_FUNCTION: ThrottleSpdCmd \n'
                                'ENVIRO.STUB_BY_FUNCTION: TorqueLimiter \n'
                                'ENVIRO.STUB_BY_FUNCTION: TractionSIM \n'
                                'ENVIRO.STUB_BY_FUNCTION: TractionSIMbuild \n'
                                'ENVIRO.STUB_BY_FUNCTION: Traction_State_Machine \n'
                                'ENVIRO.STUB_BY_FUNCTION: accel_decel_slew \n'
                                'ENVIRO.WHITE_BOX: YES \n'
                                'ENVIRO.COVERAGE_TYPE: Statement+Branch \n'
                                'ENVIRO.INDUSTRY_MODE: IEC-61508 (Industrial) \n'
                                'ENVIRO.LIBRARY_STUBS: memset \n'
                                'ENVIRO.STUB: ALL_BY_PROTOTYPE \n'
                                'ENVIRO.COMPILER: CC \n'
                                'ENVIRO.TYPE_HANDLED_DIRS_ALLOWED: \n'
                             ]
    
    tail_options = [        'ENVIRO.TYPE_HANDLED_DIRS_ALLOWED: \n'
                            'ENVIRO.LIBRARY_STUBS: \n'
                            # NEW For EVENT MANAGER
                            'ENVIRO.UNIT_APPENDIX_USER_CODE: \n'
                            'ENVIRO.UNIT_APPENDIX_USER_CODE_FILE:gyro_driver \n'
                            'uint64_t ullTimerVal; \n' 
                            'uint32_t ulTime_us; \n'
                            'ENVIRO.END_UNIT_APPENDIX_USER_CODE_FILE: \n'
                            'ENVIRO.END_UNIT_APPENDIX_USER_CODE: \n'
                            # END NEW
                            'ENVIRO.DRIVER_PREFIX_USER_CODE: \n'
                            'ENVIRO.DRIVER_PREFIX_USER_CODE_FILE:aecp_main \n'
                            'aecp_driver_t testdriver; \n'
                            'char szTxBuffer[256]; \n'
                            'ENVIRO.END_DRIVER_PREFIX_USER_CODE_FILE: \n'
                            'ENVIRO.DRIVER_PREFIX_USER_CODE_FILE:ag_input_router \n'
                            'static void * const spvAG_ModuleInputAddressArray[AG_NUMB_MODULE_INPUTS]; \n'
                            'ENVIRO.END_DRIVER_PREFIX_USER_CODE_FILE: \n'
                            'ENVIRO.DRIVER_PREFIX_USER_CODE_FILE:becp_main \n'

                            'becp_driver_t testdriver; \n'
                            'uint16_t uwmessage[128]; \n'
                            'ENVIRO.END_DRIVER_PREFIX_USER_CODE_FILE: \n'
                            'ENVIRO.DRIVER_PREFIX_USER_CODE_FILE:fram_driver \n'


                            'ENVIRO.END_DRIVER_PREFIX_USER_CODE_FILE: \n'
                             # NEW
                             'ENVIRO.DRIVER_PREFIX_USER_CODE_FILE:gyro_driver \n'
                             'uint64_t ullTB; \n' 
                             'uint32_t ulTime_us; \n'
                             'ENVIRO.END_DRIVER_PREFIX_USER_CODE_FILE: \n'
                             # END NEW
                            
                            'ENVIRO.END_DRIVER_PREFIX_USER_CODE: \n'
                            'ENVIRO.UNIT_PREFIX_USER_CODE: \n'
                            'ENVIRO.UNIT_PREFIX_USER_CODE_FILE:aecp_main \n'


                            'ENVIRO.END_UNIT_PREFIX_USER_CODE_FILE: \n'
                            # NEW
                            'ENVIRO.UNIT_PREFIX_USER_CODE_FILE:go_driver \n'
                            'boolean fDoSelfTest; \n'    
                            'vGYRO_DelayInvenSense(); \n'
                            'ENVIRO.END_UNIT_PREFIX_USER_CODE_FILE: \n'
                            # END NEW
                            'ENVIRO.END_UNIT_PREFIX_USER_CODE: \n'
                            # NEW for OS_INTERFACE Remove this if other regressions fail 
                            'ENVIRO.USER_GLOBALS: \n'
                            ' \n'
                            '/***************************************************************************** \n'
                            'S0000008.c: This file contains the definitions of variables used in user code. \n'
                            'Preface all variable declarations with VCAST_USER_GLOBALS_EXTERN to ensure \n'
                            'that only one definition of the variable is created in the test harness. \n' 
                            '*****************************************************************************/ \n'
                            ' \n'
                            '#ifndef VCAST_USER_GLOBALS_EXTERN \n'
                            '#define VCAST_USER_GLOBALS_EXTERN \n'
                            '#endif \n'
                            ' \n'
                            '#ifdef __cplusplus \n'
                            'extern "C"{ \n'
                            '#endif \n'
                            '  VCAST_USER_GLOBALS_EXTERN char VECTORCAST_CHAR1; \n'
                            '  VCAST_USER_GLOBALS_EXTERN int VECTORCAST_INT1; \n'
                            '  VCAST_USER_GLOBALS_EXTERN int VECTORCAST_INT2; \n'
                            '  VCAST_USER_GLOBALS_EXTERN int VECTORCAST_INT3; \n'
                            '  VCAST_USER_GLOBALS_EXTERN unsigned char  VECTORCAST_UINT8; \n'
                            '  VCAST_USER_GLOBALS_EXTERN unsigned long VECTORCAST_ULONG; \n'
                            '#ifndef VCAST_NO_FLOAT \n' 
                            '  VCAST_USER_GLOBALS_EXTERN float VECTORCAST_FLT1; \n'
                            '#endif \n'  
                            '  VCAST_USER_GLOBALS_EXTERN char VECTORCAST_STR1[8]; \n'
                            ' \n'
                            '  VCAST_USER_GLOBALS_EXTERN int  VECTORCAST_BUFFER[4]; \n'
                            '#ifdef __cplusplus \n'
                            '} \n'
                            '#endif \n'
                            'ENVIRO.END_USER_GLOBALS: \n'
                            # END NEW for OS_INTERFACE Remove this if other regressions fail
                            'ENVIRO.END \n'
                         ]
    # FEATURES_INTERFACE On-Target and simulation Tail options
    features_interface_tail_options = [ 'ENVIRO.END \n' ]

    # STEERING On-Target Tail options
    steering_tail_options = [ 'ENVIRO.END \n' ]

    # STEERING Simulation Tail options
    steering_tail_options_simulation = [
                            'ENVIRO.TYPE_HANDLED_DIRS_ALLOWED: \n'
                            'ENVIRO.LIBRARY_STUBS: \n'
                            'ENVIRO.DRIVER_PREFIX_USER_CODE: \n'
                            'ENVIRO.DRIVER_PREFIX_USER_CODE_FILE:aecp_main \n'
                            'aecp_driver_t testdriver; \n' 
                            'char szTxBuffer[256]; \n'
                            'ENVIRO.END_DRIVER_PREFIX_USER_CODE_FILE: \n'
                            'ENVIRO.DRIVER_PREFIX_USER_CODE_FILE:ag_input_router \n'
                            'static void * const spvAG_ModuleInputAddressArray[AG_NUMB_MODULE_INPUTS]; \n'
                            'ENVIRO.END_DRIVER_PREFIX_USER_CODE_FILE: \n'
                            'ENVIRO.DRIVER_PREFIX_USER_CODE_FILE:becp_main \n'
                            '\n'
                            'becp_driver_t testdriver; \n'
                            'uint16_t uwmessage[128]; \n'
                            'ENVIRO.END_DRIVER_PREFIX_USER_CODE_FILE: \n'
                            'ENVIRO.DRIVER_PREFIX_USER_CODE_FILE:fram_driver \n'
                            '\n'
                            'ENVIRO.END_DRIVER_PREFIX_USER_CODE_FILE: \n'
                            'ENVIRO.END_DRIVER_PREFIX_USER_CODE: \n'
                            'ENVIRO.UNIT_PREFIX_USER_CODE: \n'
                            'ENVIRO.UNIT_PREFIX_USER_CODE_FILE:aecp_main \n'
                            '\n' 
                            'ENVIRO.END_UNIT_PREFIX_USER_CODE_FILE: \n'
                            'ENVIRO.END_UNIT_PREFIX_USER_CODE: \n'
                            'ENVIRO.END \n'
                          ]

    # TRACTION On-Target Tail options
    traction_tail_options = [ 'ENVIRO.TYPE_HANDLED_DIRS_ALLOWED: \n'
                              'ENVIRO.LIBRARY_STUBS:  \n'
                              'ENVIRO.END  \n'
                            ]
    # TRACTION Simulation Tail options
    traction_tail_options_simulation = [ 'ENVIRO.END  \n' ]

    # FRAM_DRIVER On-Target and Simulation tail options
    fram_driver_tail_options = [ 'ENVIRO.END  \n' ]

    # UTILITIES_DRIVER On-Target and Simulation tail options
    utilities_tail_options = [ 'ENVIRO.END  \n' ]

    # determine path list and test .env
    if Testname == eTestname.BECP_MAIN_INTEGRATION:
        CurrentPathList = "BECP_MAIN_INTEGRATION_PathList.txt"
        CurrentEnvfile = "BECP_MAIN_INTEGRATION.env"
        
    elif Testname == eTestname.SWITCH_BOARD_INTEGRATION:
        CurrentPathList = "SWITCH_BOARD_INTEGRATION_PathList.txt"
        CurrentEnvfile = "SWITCH_BOARD_INTEGRATION.env"

    elif Testname == eTestname.EVENT_MANAGER_INTEGRATION:
        CurrentPathList = "EVENT_MANAGER_INTEGRATION_PathList.txt"
        CurrentEnvfile = "EVENT_MANAGER_INTEGRATION.env"
        
    elif Testname == eTestname.EXTERNAL_ALARM_INTEGRATION:
        CurrentPathList = "EXTERNAL_ALARM_INTEGRATION_PathList.txt"
        CurrentEnvfile = "EXTERNAL_ALARM_INTEGRATION.env"

    elif Testname == eTestname.GYRO_DRIVER_INTEGRATION:
        CurrentPathList = "GYRO_DRIVER_INTEGRATION_PathList.txt"
        CurrentEnvfile = "GYRO_DRIVER_INTEGRATION.env"
        
    elif Testname == eTestname.FEATURES_INTERFACE_INTEGRATION:
        CurrentPathList = "FEATURES_INTERFACE_INTEGRATION_PathList.txt"
        CurrentEnvfile = "FEATURES_INTERFACE_INTEGRATION.env"

    elif Testname == eTestname.SUPV_INTERFACE_INTEGRATION: 
        CurrentPathList = "SUPV_INTERFACE_INTEGRATION_PathList.txt"
        CurrentEnvfile = "SUPV_INTERFACE_INTEGRATION.env"

    elif Testname == eTestname.IMM_INTERFACE_INTEGRATION: 
        CurrentPathList = "IMM_INTERFACE_INTEGRATION_PathList.txt"
        CurrentEnvfile = "IMM_INTERFACE_INTEGRATION.env"

    elif Testname == eTestname.OS_INTERFACE_INTEGRATION: 
        CurrentPathList = "OS_INTERFACE_INTEGRATION_PathList.txt"
        CurrentEnvfile = "OS_INTERFACE_INTEGRATION.env"

    elif Testname == eTestname.STEERING_INTEGRATION: 
        CurrentPathList = "STEERING_INTEGRATION_PathList.txt"
        if fRunSimulated == True:
            CurrentEnvfile = "STEERING_SIMULATION_INTEGRATION.env"
        else:   
            CurrentEnvfile = "STEERING_INTEGRATION.env"
            
    elif Testname == eTestname.IP_INTERFACE_INTEGRATION: 
        CurrentPathList = "IP_INTERFACE_INTEGRATION_PathList.txt"
        CurrentEnvfile = "IP_INTERFACE_INTEGRATION.env"

    elif Testname == eTestname.MANIFEST_MANAGER_INTEGRATION: 
        CurrentPathList = "MANIFEST_MANAGER_INTEGRATION_PathList.txt"
        CurrentEnvfile = "MANIFEST_MANAGER_INTEGRATION.env"

    elif Testname == eTestname.TRACTION_INTEGRATION: 
        CurrentPathList = "TRACTION_INTEGRATION_PathList.txt"
        if fRunSimulated == True:
            CurrentEnvfile = "TRACTION_SIMULATION_INTEGRATION.env"
        else:   
            CurrentEnvfile = "TRACTION_INTEGRATION.env"

    elif Testname == eTestname.FRAM_DRIVER_INTEGRATION: 
        CurrentPathList = "FRAM_DRIVER_INTEGRATION_PathList.txt"
        CurrentEnvfile = "FRAM_DRIVER_INTEGRATION.env"

    elif Testname == eTestname.UTILITIES_INTEGRATION: 
        CurrentPathList = "UTILITIES_INTEGRATION_PathList.txt"
        CurrentEnvfile = "UTILITIES_INTEGRATION.env"

    elif Testname == eTestname.DIAGNOSTICS_INTEGRATION: 
        CurrentPathList = "DIAGNOSTICS_INTEGRATION_PathList.txt"
        CurrentEnvfile = "DIAGNOSTICS_INTEGRATION.env"
        
    else:    
         CurrentPathList = "AECP_MAIN_INTEGRATION_PathList.txt"
         CurrentEnvfile = "AECP_MAIN_INTEGRATION.env"
         
    # Open up ....PathList.txt and dump the contents to a input file variable formated as required 
    input_file = open(CurrentPathList, 'r')
    for line in input_file:
        new_contents.append(string1 + line + '\n')
    
    # Update a temp file with correct path information    
    fd = open('.envfile.txt', "w")
    fd.writelines(new_contents)        
    fd.close()

    # open .env and erase contents
    f = open(CurrentEnvfile, "w")
    f.writelines(' ')
    f.close()

    # write the header options information
    f = open(CurrentEnvfile,"w")
    if Testname == eTestname.BECP_MAIN_INTEGRATION:
        f.writelines(becp_header_options)        
    elif Testname == eTestname.SWITCH_BOARD_INTEGRATION:
        f.writelines(switchboard_header_options)
    elif Testname == eTestname.EVENT_MANAGER_INTEGRATION:
        f.writelines(eventmanager_header_options)
    elif Testname == eTestname.EXTERNAL_ALARM_INTEGRATION:
        f.writelines(externalalarm_header_options)
    elif Testname == eTestname.GYRO_DRIVER_INTEGRATION:
        f.writelines(gyrodriver_header_options)
    elif Testname == eTestname.FEATURES_INTERFACE_INTEGRATION:
        f.writelines(features_interface_header_options)
    elif Testname == eTestname.SUPV_INTERFACE_INTEGRATION:
        f.writelines(supv_interface_header_options)
    elif Testname == eTestname.IMM_INTERFACE_INTEGRATION:
        f.writelines(imm_interface_header_options)
    elif Testname == eTestname.OS_INTERFACE_INTEGRATION:
        f.writelines(os_interface_header_options)
    elif Testname == eTestname.STEERING_INTEGRATION:
        if fRunSimulated == True:
            f.writelines(steering_header_options_simulation)
        else:
            f.writelines(steering_header_options)
    elif Testname == eTestname.IP_INTERFACE_INTEGRATION:
        f.writelines(ip_interface_header_options)
    elif Testname == eTestname.MANIFEST_MANAGER_INTEGRATION:
        f.writelines(manifest_manager_header_options)
    elif Testname == eTestname.TRACTION_INTEGRATION:
        if fRunSimulated == True:
            f.writelines(traction_header_options_simulation)
        else:
            f.writelines(traction_header_options)
    elif Testname == eTestname.FRAM_DRIVER_INTEGRATION:
        f.writelines(fram_driver_header_options)
    elif Testname == eTestname.UTILITIES_INTEGRATION:
        f.writelines(utilities_header_options)
    elif Testname == eTestname.DIAGNOSTICS_INTEGRATION:
        f.writelines(diagnostics_header_options)
            
    else:
        f.writelines(aecp_header_options)
    
    # append new paths back in
    f = open(CurrentEnvfile,"a")
    f.writelines(new_contents)
    f.close()

    # delete blank spaces
    RemoveBlankSpace(CurrentEnvfile)

    # append to the end the tail options to all except what is specified
    f = open(CurrentEnvfile,"a")    
    if Testname == eTestname.STEERING_INTEGRATION:
        if fRunSimulated == True:
            f.writelines(steering_tail_options_simulation)
        else:
            f.writelines(steering_tail_options)
    elif Testname == eTestname.TRACTION_INTEGRATION:
        if fRunSimulated == True:
            f.writelines(traction_tail_options_simulation)
        else:
            f.writelines(traction_tail_options)

    elif Testname == eTestname.FEATURES_INTERFACE_INTEGRATION:
        f.writelines(features_interface_tail_options)

    elif Testname == eTestname.FRAM_DRIVER_INTEGRATION:
        f.writelines(fram_driver_tail_options)

    elif Testname == eTestname.UTILITIES_INTEGRATION:
        f.writelines(utilities_tail_options)
        
    else:        
        f.writelines(tail_options)
        f.close()

    print ("--------------------------------------------------------------------------------\n")
    print ("                 Wrote .env file with new paths                                 \n")
    print ("--------------------------------------------------------------------------------\n")

def Vcast_batFile(Testname, fRunSimulated):
   
    new_contents = []
    no_space = []
    string1 = 'echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE)'
    string2 = '\ >> commands.tmp'

    #######################  These below options are for On-Target test regressions #####################################################
    ccast_header_options = ['del commands.tmp \n'
		'echo options C_COMPILER_CFG_SOURCE PY_CONFIGURATOR >> commands.tmp \n'
		'echo options C_COMPILER_FAMILY_NAME Green_Hills >> commands.tmp \n'
		'echo options C_COMPILER_HIERARCHY_STRING Green Hills_PPC_Bare Board_MBX860_mpserv-multi_C >> commands.tmp \n'
		'echo options C_COMPILER_PY_ARGS >> commands.tmp \n' 
		'echo options C_COMPILER_TAG PPC_GH_BARE_MBX_MP_MULTI >> commands.tmp \n'
		'echo options C_COMPILER_VERSION_CMD ccppc -V not.here.c >> commands.tmp \n'
		'echo options C_COMPILE_CMD ccppc -c -G -w -bsp pxr40 >> commands.tmp \n'
		'echo options C_COMPILE_CMD_FLAG -c >> commands.tmp \n'
		'echo options C_COMPILE_EXCLUDE_FLAGS -o^^** >> commands.tmp \n'
		'echo options C_DEBUG_CMD multi >> commands.tmp \n'
		'echo options C_DEFINE_LIST TX_NO_EVENT_INFO TX_ENABLE_EVENT_FILTERS NX_DISABLE_IPV6 NX_MAX_MULTICAST_GROUPS=1 NX_MAX_LISTEN_REQUESTS=4 NX_FTP_MAX_CLIENTS=1 TX_DISABLE_PREEMPTION_THRESHOLD TX_DISABLE_NOTIFY_CALLBACKS TRUCK_TYPE_DEFINITION=TRUCK_TYPE_PC MODULE_TYPE_DEFINITION=MODULE_TYPE_SMALL_DV GEN_TYPE_DEFINITION=GEN_TYPE_GEN1 >> commands.tmp \n'
		'echo options C_EDG_FLAGS -w --c --ghs --define_macro=restrict= >> commands.tmp \n'
		'echo options C_EXECUTE_CMD $(VECTORCAST_DIR)/DATA/green_hills/multi_execution_flash.bat multi -nosplash C:\\ghs\\comp_201654\\target\\ppc\\pxr40\\mpserv_standard.mbs ghprobe >> commands.tmp \n'
		'echo options C_LINKER_VERSION_CMD elxr -V >> commands.tmp \n'
		'echo options C_LINK_CMD ccppc -G -bsp pxr40 >> commands.tmp \n'
		'echo options C_LINK_OPTIONS memory.ld standalone_config.ld standalone_romdebug.ld -LC:\\VCAST\\DATA\\green_hills\\flash_pxr40 -lstartup -lsys -lboardinit >> commands.tmp \n'
		'echo options C_OUTPUT_FLAG -o^^ >> commands.tmp \n'
		'echo options C_PREPROCESS_CMD ccppc -E -C -bsp pxr40 >> commands.tmp \n'
		'echo options C_PREPROCESS_FILE >> commands.tmp \n' 
		'echo options EVENT_LIMIT 500 >> commands.tmp \n'
		'echo options EXECUTABLE_EXTENSION >> commands.tmp \n' 
		'echo options MAX_VARY_RANGE 20 >> commands.tmp \n'
		'echo options RANGE_CHECK DISABLE >> commands.tmp \n'
		'echo options SBF_LOC_MEMBER_IN_NSP DECL_NAMESPACE >> commands.tmp \n'
		'echo options SBF_LOC_MEMBER_OUTSIDE_NSP DECL_NAMESPACE >> commands.tmp \n'
		'echo options SBF_LOC_NONMEMBER_IN_NSP DECL_NAMESPACE >> commands.tmp \n'
		'echo options SBF_LOC_NONMEMBER_OUTSIDE_NSP DECL_NAMESPACE >> commands.tmp \n'
		'echo options SUBSTITUTE_CODE_FOR_C_FILE FALSE >> commands.tmp \n'
		'echo options VCAST_ASSEMBLY_FILE_EXTENSIONS asm s >> commands.tmp \n'
		'echo options VCAST_AUTO_CLEAR_TEST_USER_CODE FALSE >> commands.tmp \n'
		'echo options VCAST_COLLAPSE_STD_HEADERS COLLAPSE_NON_SEARCH_HEADERS >> commands.tmp \n'
		'echo options VCAST_COMMAND_LINE_DEBUGGER FALSE >> commands.tmp \n'
		'echo options VCAST_COMPILER_SUPPORTS_CPP_CASTS TRUE >> commands.tmp \n'
		'echo options VCAST_COVERAGE_FOR_AGGREGATE_INIT FALSE >> commands.tmp \n'
		'echo options VCAST_DISABLE_CPP_EXCEPTIONS TRUE >> commands.tmp \n'
		'echo options VCAST_DISPLAY_UNINST_EXPR FALSE >> commands.tmp \n'
		'echo options VCAST_ENABLE_FUNCTION_CALL_COVERAGE FALSE >> commands.tmp \n'
		'echo options VCAST_ENVIRONMENT_FILES C:\\ghs\\comp_201654\\target\\ppc\\pxr40\\standalone_romdebug.ld,C:\\ghs\\comp_201654\\target\\ppc\\pxr40\\standalone_config.ld,C:\\ghs\\comp_201654\\target\\ppc\\pxr40\\memory.ld >> commands.tmp \n'
                'echo options VCAST_EXECUTE_WITH_STDOUT TRUE >> commands.tmp \n'
		'echo options VCAST_FORCE_ELAB_TYPE_SPEC TRUE >> commands.tmp \n'
		'echo options VCAST_HAS_LONGLONG TRUE >> commands.tmp \n'
		'echo options VCAST_INST_FILE_MAX_LINES 0 >> commands.tmp \n'
		'echo options VCAST_MAX_STRING_LENGTH 1000 >> commands.tmp \n'
		'echo options VCAST_MAX_TARGET_FILES 50 >> commands.tmp \n'
		'echo options VCAST_MICROSOFT_LONG_LONG FALSE >> commands.tmp \n'
		'echo options VCAST_NO_STDIN TRUE >> commands.tmp \n'
		'echo options VCAST_REMOVE_PREPROCESSOR_COMMENTS FALSE >> commands.tmp \n'
		'echo options VCAST_RPTS_DEFAULT_FONT_FACE Arial(7) >> commands.tmp \n'
		'echo options VCAST_STDIO FALSE >> commands.tmp \n'
		'echo options VCAST_TORNADO_CONSTRUCTOR_CALL_FILE FALSE >> commands.tmp \n'
		'echo options VCAST_UNIT_TYPE SBF >> commands.tmp \n'
		'echo options WHITEBOX YES >> commands.tmp \n'
		'echo clear_default_source_dirs >> commands.tmp \n' 
		]
    ccast_header_options_gen2 = ['del commands.tmp \n'                
                'echo options C_COMPILER_CFG_SOURCE PY_CONFIGURATOR >> commands.tmp \n'
                'echo options C_COMPILER_FAMILY_NAME Green_Hills >> commands.tmp \n'
                'echo options C_COMPILER_HIERARCHY_STRING Green Hills_PPC_Bare Board_MBX860_mpserv-multi_C >> commands.tmp \n'
                'echo options C_COMPILER_PY_ARGS  >> commands.tmp \n'
                'echo options C_COMPILER_TAG PPC_GH_BARE_MBX_MP_MULTI >> commands.tmp \n'
                'echo options C_COMPILER_VERSION_CMD ccppc -V not.here.c >> commands.tmp \n'
                'echo options C_COMPILE_CMD ccppc -c -G -w -bsp generic -cpu=ppc5777c -bigendian -vle -fhard -floatsingle -fprecise >> commands.tmp \n'
                'echo options C_COMPILE_CMD_FLAG -c >> commands.tmp \n'
                'echo options C_COMPILE_EXCLUDE_FLAGS -o^^** >> commands.tmp \n'
                'echo options C_DEBUG_CMD multi >> commands.tmp \n'
                'echo options C_DEFINE_LIST TX_NO_EVENT_INFO TX_ENABLE_EVENT_FILTERS NX_DISABLE_IPV6 NX_MAX_MULTICAST_GROUPS=1 NX_MAX_LISTEN_REQUESTS=4 NX_FTP_MAX_CLIENTS=1 TX_DISABLE_PREEMPTION_THRESHOLD TX_DISABLE_NOTIFY_CALLBACKS TRUCK_TYPE_DEFINITION=TRUCK_TYPE_PC MODULE_TYPE_DEFINITION=MODULE_TYPE_SMALL_DV GEN_TYPE_DEFINITION=GEN_TYPE_GEN2 >> commands.tmp \n'
                'echo options C_EDG_FLAGS -w --c --ghs --define_macro=restrict= >> commands.tmp \n'
                'echo options C_EXECUTE_CMD $(VECTORCAST_DIR)/DATA/green_hills/multi_execution_flash.bat multi -nosplash C:\\ghs\\comp_201654\\target\\ppc\\mpc5777c\\mpserv_standard.mbs ghprobe GEN2 >> commands.tmp \n'
                'echo options C_LINKER_VERSION_CMD elxr -V >> commands.tmp \n'
                'echo options C_LINK_CMD ccppc -G -bsp generic -cpu=ppc5777c -bigendian -Mx -Mu -vle -fhard -floatsingle -fprecise -e 0x0 >> commands.tmp \n'
                'echo options C_LINK_OPTIONS $(VECTORCAST_DIR)\\DATA\\green_hills\\standalone_romdebug_vcm.ld >> commands.tmp \n'
                'echo options C_OUTPUT_FLAG -o^^ >> commands.tmp \n'
                'echo options C_PREPROCESS_CMD ccppc -E -C -bsp generic -cpu=ppc5777c -bigendian -vle -fhard -floatsingle -fprecise >> commands.tmp \n'
                'echo options C_PREPROCESS_FILE  >> commands.tmp \n'
                'echo options EXECUTABLE_EXTENSION  >> commands.tmp \n'
                'echo options RANGE_CHECK FULL >> commands.tmp \n'
                'echo options SBF_LOC_MEMBER_IN_NSP DECL_NAMESPACE >> commands.tmp \n'
                'echo options SBF_LOC_MEMBER_OUTSIDE_NSP DECL_NAMESPACE >> commands.tmp \n'
                'echo options SBF_LOC_NONMEMBER_IN_NSP DECL_NAMESPACE >> commands.tmp \n'
                'echo options SBF_LOC_NONMEMBER_OUTSIDE_NSP DECL_NAMESPACE >> commands.tmp \n'
                'echo options SUBSTITUTE_CODE_FOR_C_FILE FALSE >> commands.tmp \n'
                'echo options VCAST_ASSEMBLY_FILE_EXTENSIONS asm s >> commands.tmp \n'
                'echo options VCAST_AUTO_CLEAR_TEST_USER_CODE FALSE >> commands.tmp \n'
                'echo options VCAST_BUFFER_OUTPUT TRUE >> commands.tmp \n'
                'echo options VCAST_COLLAPSE_STD_HEADERS COLLAPSE_NON_SEARCH_HEADERS >> commands.tmp \n'
                'echo options VCAST_COMMAND_LINE_DEBUGGER FALSE >> commands.tmp \n'
                'echo options VCAST_COMPILER_SUPPORTS_CPP_CASTS TRUE >> commands.tmp \n'
                'echo options VCAST_COVERAGE_FOR_AGGREGATE_INIT FALSE >> commands.tmp \n'
                'echo options VCAST_DISABLE_CPP_EXCEPTIONS TRUE >> commands.tmp \n'
                'echo options VCAST_DISPLAY_UNINST_EXPR FALSE >> commands.tmp \n'
                'echo options VCAST_DUMP_BUFFER TRUE >> commands.tmp \n'
                'echo options VCAST_ENABLE_FUNCTION_CALL_COVERAGE FALSE >> commands.tmp \n'
                'echo options VCAST_ENVIRONMENT_FILES C:\\ghs\\comp_201654\\target\\ppc\\mpc5777c\\standalone_romdebug.ld,C:\\ghs\\comp_201654\\target\\ppc\\mpc5777c\\memory.ld,C:\\ghs\\comp_201654\\target\\ppc\\mpc5777c\\standalone_config.ld >> commands.tmp \n'
                'echo options VCAST_EXECUTE_WITH_STDOUT TRUE >> commands.tmp \n'
                'echo options VCAST_FORCE_ELAB_TYPE_SPEC TRUE >> commands.tmp \n'
                'echo options VCAST_HAS_LONGLONG TRUE >> commands.tmp \n'
                'echo options VCAST_INST_FILE_MAX_LINES 0 >> commands.tmp \n'
                'echo options VCAST_MAX_STRING_LENGTH 1000 >> commands.tmp \n'
                'echo options VCAST_MAX_TARGET_FILES 50 >> commands.tmp \n'
                'echo options VCAST_MICROSOFT_LONG_LONG FALSE >> commands.tmp \n'
                'echo options VCAST_NO_SETJMP FALSE >> commands.tmp \n'
                'echo options VCAST_NO_SIGNAL FALSE >> commands.tmp \n'
                'echo options VCAST_NO_STDIN TRUE >> commands.tmp \n'
                'echo options VCAST_NO_STDLIB FALSE >> commands.tmp \n'
                'echo options VCAST_OUTPUT_BUFFER_SIZE 8000 >> commands.tmp \n'
                'echo options VCAST_REMOVE_PREPROCESSOR_COMMENTS FALSE >> commands.tmp \n'
                'echo options VCAST_RPTS_DEFAULT_FONT_FACE Arial(6) >> commands.tmp \n'
                'echo options VCAST_STDIO FALSE >> commands.tmp \n'
                'echo options VCAST_TORNADO_CONSTRUCTOR_CALL_FILE FALSE >> commands.tmp \n'
                'echo options VCAST_USE_STD_STRING TRUE >> commands.tmp \n'
                'echo options WHITEBOX YES >> commands.tmp \n'
                'echo clear_default_source_dirs  >> commands.tmp \n'
                ]                 
    
    ccast_header_options_traction = ['del commands.tmp \n'
                'echo options C_COMPILER_CFG_SOURCE PY_CONFIGURATOR >> commands.tmp \n'
                'echo options C_COMPILER_FAMILY_NAME Green_Hills >> commands.tmp \n'
                'echo options C_COMPILER_HIERARCHY_STRING Green Hills_PPC_Bare Board_MBX860_mpserv-multi_C >> commands.tmp \n'
                'echo options C_COMPILER_PY_ARGS  >> commands.tmp \n'
                'echo options C_COMPILER_TAG PPC_GH_BARE_MBX_MP_MULTI >> commands.tmp \n'
                'echo options C_COMPILER_VERSION_CMD ccppc -V not.here.c >> commands.tmp \n'
                'echo options C_COMPILE_CMD ccppc -c -G -w -bsp pxr40 >> commands.tmp \n'
                'echo options C_COMPILE_EXCLUDE_FLAGS -o^^** >> commands.tmp \n'
                'echo options C_DEBUG_CMD multi -nosplash -remote \"mpserv ghprobe" -p $(VECTORCAST_DIR)/DATA/green_hills/multi_playback_file_debug.p >> commands.tmp \n'
                'echo options C_DEFINE_LIST TX_NO_EVENT_INFO TX_ENABLE_EVENT_FILTERS NX_DISABLE_IPV6 NX_MAX_MULTICAST_GROUPS=1 NX_MAX_LISTEN_REQUESTS=4 NX_FTP_MAX_CLIENTS=1 TX_DISABLE_PREEMPTION_THRESHOLD TX_DISABLE_NOTIFY_CALLBACKS TRUCK_TYPE_DEFINITION=TRUCK_TYPE_PC MODULE_TYPE_DEFINITION=MODULE_TYPE_SMALL_DV >> commands.tmp \n'
                'echo options C_EDG_FLAGS -w --c --ghs --define_macro=restrict= >> commands.tmp \n'
                'echo options C_EXECUTE_CMD $(VECTORCAST_DIR)/DATA/green_hills/multi_execution_flash.bat multi -nosplash C:\\ghs\\comp_201654\\target\\ppc\\pxr40\\mpserv_standard.mbs ghprobe >> commands.tmp \n'
                'echo options C_LINKER_VERSION_CMD elxr -V >> commands.tmp \n'
                'echo options C_LINK_CMD ccppc -G -bsp pxr40 >> commands.tmp \n'
                'echo options C_LINK_OPTIONS memory.ld standalone_config.ld standalone_romdebug.ld -LC:\\VCAST\\DATA\\green_hills\\flash_pxr40 -lstartup -lsys -lboardinit >> commands.tmp \n'
                'echo options C_OUTPUT_FLAG -o^^ >> commands.tmp \n'
                'echo options C_PREPROCESS_CMD ccppc -E -C -bsp pxr40 >> commands.tmp \n'
                'echo options C_PREPROCESS_FILE  >> commands.tmp \n'
                'echo options EVENT_LIMIT 500 >> commands.tmp \n'
                'echo options RANGE_CHECK NONE >> commands.tmp \n'
                'echo options VCAST_ASSEMBLY_FILE_EXTENSIONS asm s >> commands.tmp \n'
                'echo options VCAST_COMPILER_SUPPORTS_CPP_CASTS FALSE >> commands.tmp \n'
                'echo options VCAST_COVERAGE_FOR_AGGREGATE_INIT FALSE >> commands.tmp \n'
                'echo options VCAST_DISABLE_CPP_EXCEPTIONS TRUE >> commands.tmp \n'
                'echo options VCAST_DISPLAY_UNINST_EXPR FALSE >> commands.tmp \n'
                'echo options VCAST_ENABLE_FUNCTION_CALL_COVERAGE FALSE >> commands.tmp \n'
                'echo options VCAST_ENVIRONMENT_FILES C:\\ghs\\comp_201654\\target\\ppc\\pxr40\\memory.ld,C:\\ghs\\comp_201654\\target\\ppc\\pxr40\\standalone_romdebug.ld,C:\\ghs\\comp_201654\\target\\ppc\\pxr40\\standalone_config.ld >> commands.tmp \n'
                'echo options VCAST_EXECUTE_WITH_STDOUT TRUE >> commands.tmp \n'
                'echo options VCAST_HAS_LONGLONG FALSE >> commands.tmp \n'
                'echo options VCAST_MAX_TARGET_FILES 50 >> commands.tmp \n'
                'echo options VCAST_NO_STDIN TRUE >> commands.tmp \n'
                'echo options VCAST_REMOVE_PREPROCESSOR_COMMENTS FALSE >> commands.tmp \n'
                'echo options VCAST_RPTS_DEFAULT_FONT_FACE Arial(6) >> commands.tmp \n'
                'echo options VCAST_STDIO FALSE >> commands.tmp \n'
                'echo options VCAST_UNIT_TYPE UUT >> commands.tmp \n'
                'echo options WHITEBOX YES >> commands.tmp \n'
                'echo clear_default_source_dirs  >> commands.tmp \n'
                ]
    
    aecp_tail_options = ['echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE) \n', 
                   'echo environment build AECP_MAIN_INTEGRATION.env >> commands.tmp \n', 
                   'echo /E:AECP_MAIN_INTEGRATION tools script run AECP_MAIN_INTEGRATION.tst >> commands.tmp \n', 
                   'echo /E:AECP_MAIN_INTEGRATION execute batch >> commands.tmp \n',
                   'echo /E:AECP_MAIN_INTEGRATION reports custom management AECP_MAIN_INTEGRATION_management_report.html >> commands.tmp \n', 
                   '"%VECTORCAST_DIR%\CLICAST"  /L:C tools execute commands.tmp false \n'
                         ]

    becp_tail_options = ['echo environment build BECP_MAIN_INTEGRATION.env >> commands.tmp \n',
                    'echo /E:BECP_MAIN_INTEGRATION tools script run BECP_MAIN_INTEGRATION.tst >> commands.tmp \n',
                    'echo /E:BECP_MAIN_INTEGRATION execute batch >> commands.tmp \n',
                    'echo /E:BECP_MAIN_INTEGRATION reports custom management BECP_MAIN_INTEGRATION_management_report.html >> commands.tmp \n',
                    '"%VECTORCAST_DIR%\CLICAST"  /L:C tools execute commands.tmp false \n'
                         ]

    switchboard_tail_options = ['echo environment build SWITCH_BOARD_INTEGRATION.env >> commands.tmp \n',
                    'echo /E:SWITCH_BOARD_INTEGRATION tools script run SWITCH_BOARD_INTEGRATION.tst >> commands.tmp \n',
                    'echo /E:SWITCH_BOARD_INTEGRATION execute batch >> commands.tmp \n',
                    'echo /E:SWITCH_BOARD_INTEGRATION reports custom management SWITCH_BOARD_INTEGRATION_management_report.html >> commands.tmp \n',
                    '"%VECTORCAST_DIR%\CLICAST"  /L:C tools execute commands.tmp false \n'
                               ]
    eventmanager_tail_options = ['echo environment build EVENT_MANAGER_INTEGRATION.env >> commands.tmp \n',
                    'echo /E:EVENT_MANAGER_INTEGRATION tools script run EVENT_MANAGER_INTEGRATION.tst >> commands.tmp \n',
                    'echo /E:EVENT_MANAGER_INTEGRATION execute batch >> commands.tmp \n',
                    'echo /E:EVENT_MANAGER_INTEGRATION reports custom management EVENT_MANAGER_INTEGRATION_management_report.html >> commands.tmp \n',
                    '"%VECTORCAST_DIR%\CLICAST"  /L:C tools execute commands.tmp false \n'
                                 ]

    externalalarm_tail_options = ['echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE) \n', 
                   'echo environment build EXTERNAL_ALARM_INTEGRATION.env >> commands.tmp \n', 
                   'echo /E:EXTERNAL_ALARM_INTEGRATION tools script run EXTERNAL_ALARM_INTEGRATION.tst >> commands.tmp \n', 
                   'echo /E:EXTERNAL_ALARM_INTEGRATION execute batch >> commands.tmp \n',
                   'echo /E:EXTERNAL_ALARM_INTEGRATION reports custom management EXTERNAL_ALARM_INTEGRATION_management_report.html >> commands.tmp \n', 
                   '"%VECTORCAST_DIR%\CLICAST"  /L:C tools execute commands.tmp false \n'
                         ]

    gyrodriver_tail_options = [ 'echo environment build GYRO_DRIVER_INTEGRATION.env >> commands.tmp \n',
                                'echo /E:GYRO_DRIVER_INTEGRATION tools script run GYRO_DRIVER_INTEGRATION.tst >> commands.tmp \n',
                                'echo /E:GYRO_DRIVER_INTEGRATION execute batch >> commands.tmp \n',
                                'echo /E:GYRO_DRIVER_INTEGRATION reports custom management GYRO_DRIVER_INTEGRATION_management_report.html >> commands.tmp \n',
                                '"%VECTORCAST_DIR%\CLICAST"  /L:C tools execute commands.tmp false \n'
                              ]
    featuresinterface_tail_options = [ 'echo environment build FEATURES_INTERFACE_INTEGRATION.env >> commands.tmp \n',
                                'echo /E:FEATURES_INTERFACE_INTEGRATION tools script run FEATURES_INTERFACE_INTEGRATION.tst >> commands.tmp \n',
                                'echo /E:FEATURES_INTERFACE_INTEGRATION execute batch >> commands.tmp \n', 
                                'echo /E:FEATURES_INTERFACE_INTEGRATION reports custom management FEATURES_INTERFACE_INTEGRATION_management_report.html >> commands.tmp \n', 
                                '"%VECTORCAST_DIR%\CLICAST"  /L:C tools execute commands.tmp false \n' 
                              ]
    supvinterface_tail_options = [ 'echo environment build SUPV_INTERFACE_INTEGRATION.env >> commands.tmp \n',
                                 'echo /E:SUPV_INTERFACE_INTEGRATION tools script run SUPV_INTERFACE_INTEGRATION.tst >> commands.tmp \n',
                                 'echo /E:SUPV_INTERFACE_INTEGRATION execute batch >> commands.tmp\n',
                                 'echo /E:SUPV_INTERFACE_INTEGRATION reports custom management SUPV_INTERFACE_INTEGRATION_management_report.html >> commands.tmp \n',
                                 '"%VECTORCAST_DIR%\CLICAST"  /L:C tools execute commands.tmp false \n'
                              ]
    imminterface_tail_options = [  'echo environment build IMM_INTERFACE_INTEGRATION.env >> commands.tmp \n',
                                 'echo /E:IMM_INTERFACE_INTEGRATION tools script run IMM_INTERFACE_INTEGRATION.tst >> commands.tmp \n',
                                 'echo /E:IMM_INTERFACE_INTEGRATION execute batch >> commands.tmp\n',
                                 'echo /E:IMM_INTERFACE_INTEGRATION reports custom management IMM_INTERFACE_INTEGRATION_management_report.html >> commands.tmp \n',
                                 '"%VECTORCAST_DIR%\CLICAST"  /L:C tools execute commands.tmp false \n'
                              ]
    osinterface_tail_options = [  'echo environment build OS_INTERFACE_INTEGRATION.env >> commands.tmp \n',
                                 'echo /E:OS_INTERFACE_INTEGRATION tools script run OS_INTERFACE_INTEGRATION.tst >> commands.tmp \n',
                                 'echo /E:OS_INTERFACE_INTEGRATION execute batch >> commands.tmp\n',
                                 'echo /E:OS_INTERFACE_INTEGRATION reports custom management OS_INTERFACE_INTEGRATION_management_report.html >> commands.tmp \n',
                                 '"%VECTORCAST_DIR%\CLICAST"  /L:C tools execute commands.tmp false \n'
                              ]

    steering_tail_options = [  'echo environment build STEERING_INTEGRATION.env >> commands.tmp \n',
                                 'echo /E:STEERING_INTEGRATION tools script run STEERING_INTEGRATION.tst >> commands.tmp \n',
                                 'echo /E:STEERING_INTEGRATION execute batch >> commands.tmp\n',
                                 'echo /E:STEERING_INTEGRATION reports custom management STEERING_INTEGRATION_management_report.html >> commands.tmp \n',
                                 '"%VECTORCAST_DIR%\CLICAST"  /L:C tools execute commands.tmp false \n'
                              ]
    ipinterface_tail_options = [  'echo environment build IP_INTERFACE_INTEGRATION.env >> commands.tmp \n',
                                 'echo /E:IP_INTERFACE_INTEGRATION tools script run IP_INTERFACE_INTEGRATION.tst >> commands.tmp \n',
                                 'echo /E:IP_INTERFACE_INTEGRATION execute batch >> commands.tmp\n',
                                 'echo /E:IP_INTERFACE_INTEGRATION reports custom management IP_INTERFACE_INTEGRATION_management_report.html >> commands.tmp \n',
                                 '"%VECTORCAST_DIR%\CLICAST"  /L:C tools execute commands.tmp false \n'
                              ]
    manifestmanager_tail_options = [  'echo environment build MANIFEST_MANAGER_INTEGRATION.env >> commands.tmp \n',
                                 'echo /E:MANIFEST_MANAGER_INTEGRATION tools script run MANIFEST_MANAGER_INTEGRATION.tst >> commands.tmp \n',
                                 'echo /E:MANIFEST_MANAGER_INTEGRATION execute batch >> commands.tmp\n',
                                 'echo /E:MANIFEST_MANAGER_INTEGRATION reports custom management MANIFEST_MANAGER_INTEGRATION_management_report.html >> commands.tmp \n',
                                 '"%VECTORCAST_DIR%\CLICAST"  /L:C tools execute commands.tmp false \n'
                              ]
    traction_tail_options = [    'echo environment build TRACTION_INTEGRATION.env >> commands.tmp \n',
                                 'echo /E:TRACTION_INTEGRATION tools script run TRACTION_INTEGRATION.tst >> commands.tmp \n',
                                 'echo /E:TRACTION_INTEGRATION execute batch >> commands.tmp\n',
                                 'echo /E:TRACTION_INTEGRATION reports custom management TRACTION_INTEGRATION_management_report.html >> commands.tmp \n',
                                 '"%VECTORCAST_DIR%\CLICAST"  /L:C tools execute commands.tmp false \n'
                              ]
    fram_driver_tail_options = [ 'echo environment build FRAM_DRIVER_INTEGRATION.env >> commands.tmp \n',
                                 'echo /E:FRAM_DRIVER_INTEGRATION tools script run FRAM_DRIVER_INTEGRATION.tst >> commands.tmp \n',
                                 'echo /E:FRAM_DRIVER_INTEGRATION execute batch >> commands.tmp\n',
                                 'echo /E:FRAM_DRIVER_INTEGRATION reports custom management FRAM_DRIVER_INTEGRATION_management_report.html >> commands.tmp \n',
                                 '"%VECTORCAST_DIR%\CLICAST"  /L:C tools execute commands.tmp false \n'
                              ]
    utilities_tail_options = [   'echo environment build UTILITIES_INTEGRATION.env >> commands.tmp \n', 
                                 'echo /E:UTILITIES_INTEGRATION tools script run UTILITIES_INTEGRATION.tst >> commands.tmp \n', 
                                 'echo /E:UTILITIES_INTEGRATION execute batch >> commands.tmp \n',
                                 'echo /E:UTILITIES_INTEGRATION reports custom management UTILITIES_INTEGRATION_management_report.html >> commands.tmp \n',
                                 '"%VECTORCAST_DIR%\CLICAST"  /L:C tools execute commands.tmp false \n'
                              ]
    diagnostics_tail_options = [  'echo environment build DIAGNOSTICS_INTEGRATION.env >> commands.tmp \n', 
                                 'echo /E:DIAGNOSTICS_INTEGRATION tools script run DIAGNOSTICS_INTEGRATION.tst >> commands.tmp \n', 
                                 'echo /E:DIAGNOSTICS_INTEGRATION execute batch >> commands.tmp \n',
                                 'echo /E:DIAGNOSTICS_INTEGRATION reports custom management DIAGNOSTICS_INTEGRATION_management_report.html >> commands.tmp \n',
                                 '"%VECTORCAST_DIR%\CLICAST"  /L:C tools execute commands.tmp false \n'
                              ]
   ####################### Next set of lists are for simulation mode head and tail options #####################################################
    ccast_header_options_simulation = ['del commands.tmp \n' 
			'echo options ASSEMBLER_CMD >> commands.tmp \n'
			'echo options C_COMPILER_CFG_SOURCE PY_CONFIGURATOR >> commands.tmp \n'
			'echo options C_COMPILER_FAMILY_NAME Green_Hills >> commands.tmp \n'
			'echo options C_COMPILER_HIERARCHY_STRING Green Hills PPC Bare Board Simulator (C) >> commands.tmp \n'
			'echo options C_COMPILER_PY_ARGS --lang c --cpu ppc --io stdout --target sim >> commands.tmp \n'
			'echo options C_COMPILER_TAG PPC_GH_SIM >> commands.tmp \n'
			'echo options C_COMPILER_VERSION_CMD ccppc -V not.here.c >> commands.tmp \n'
			'echo options C_COMPILE_CMD ccppc -c -G -w -cpu=p4040 >> commands.tmp \n'
			'echo options C_COMPILE_CMD_FLAG -c >> commands.tmp \n'
			'echo options C_COMPILE_EXCLUDE_FLAGS -o^^** >> commands.tmp \n'
			'echo options C_DEBUG_CMD multi -nosplash -remote "simppc -X83" -p $(VECTORCAST_DIR)/DATA/green_hills/multi_playback_file_debug.p >> commands.tmp \n'
			'echo options C_DEFINE_LIST TX_NO_EVENT_INFO TX_ENABLE_EVENT_FILTERS NX_DISABLE_IPV6 NX_MAX_MULTICAST_GROUPS=1 NX_MAX_LISTEN_REQUESTS=4 NX_FTP_MAX_CLIENTS=1 TX_DISABLE_PREEMPTION_THRESHOLD TX_DISABLE_NOTIFY_CALLBACKS TRUCK_TYPE_DEFINITION=TRUCK_TYPE_PC MODULE_TYPE_DEFINITION=MODULE_TYPE_SMALL_DV __noinline= VCAST_USE_GH_SYSCALL GEN_TYPE_DEFINITION=GEN_TYPE_GEN1 >> commands.tmp \n'
			'echo options C_EDG_FLAGS -w --c --ghs --define_macro=restrict= >> commands.tmp \n'
			'echo options C_EXECUTE_CMD multi -nosplash -remote "simppc -X83 -cpu=p4040" -p $(VECTORCAST_DIR)/DATA/green_hills/multi_playback_file_multi4.p >> commands.tmp \n'
			'echo options C_LINKER_VERSION_CMD elxr -V >> commands.tmp \n'
			'echo options C_LINK_CMD ccppc -G -map=vcast.map -cpu=p4040 >> commands.tmp \n'
			'echo options C_LINK_OPTIONS >> commands.tmp \n'
			'echo options C_OUTPUT_FLAG -o^^ >> commands.tmp \n'
			'echo options C_PREPROCESS_CMD ccppc -E -C -cpu=p4040 >> commands.tmp \n'
			'echo options C_PREPROCESS_FILE >> commands.tmp \n'
			'echo options EVENT_LIMIT 500 >> commands.tmp \n'
			'echo options EXECUTABLE_EXTENSION >> commands.tmp \n'
			'echo options MAX_VARY_RANGE 20 >> commands.tmp \n'
			'echo options RANGE_CHECK NONE >> commands.tmp \n'
			'echo options SBF_LOC_MEMBER_IN_NSP DECL_NAMESPACE >> commands.tmp \n'
			'echo options SBF_LOC_MEMBER_OUTSIDE_NSP DECL_NAMESPACE >> commands.tmp \n'
			'echo options SBF_LOC_NONMEMBER_IN_NSP DECL_NAMESPACE >> commands.tmp \n'
			'echo options SBF_LOC_NONMEMBER_OUTSIDE_NSP DECL_NAMESPACE >> commands.tmp \n'
			'echo options STUB_DEPENDENCIES YES >> commands.tmp \n'
			'echo options SUBSTITUTE_CODE_FOR_C_FILE FALSE >> commands.tmp \n'
			'echo options VCAST_ASSEMBLY_FILE_EXTENSIONS asm s >> commands.tmp \n'
			'echo options VCAST_AUTO_CLEAR_TEST_USER_CODE FALSE >> commands.tmp \n'
			'echo options VCAST_BUFFER_OUTPUT TRUE >> commands.tmp \n'
			'echo options VCAST_COLLAPSE_STD_HEADERS COLLAPSE_NON_SEARCH_HEADERS >> commands.tmp \n'
			'echo options VCAST_COMMAND_LINE_DEBUGGER FALSE >> commands.tmp \n'
			'echo options VCAST_COMPILER_SUPPORTS_CPP_CASTS TRUE >> commands.tmp \n'
			'echo options VCAST_COVERAGE_FOR_AGGREGATE_INIT FALSE >> commands.tmp \n'
			'echo options VCAST_DISABLE_CPP_EXCEPTIONS TRUE >> commands.tmp \n'
			'echo options VCAST_DISABLE_STD_WSTRING_DETECTION FALSE >> commands.tmp \n'
			'echo options VCAST_DISPLAY_UNINST_EXPR FALSE >> commands.tmp \n'
			'echo options VCAST_DUMP_BUFFER TRUE >> commands.tmp \n'
			'echo options VCAST_ENABLE_FUNCTION_CALL_COVERAGE FALSE >> commands.tmp \n'
			'echo options VCAST_ENVIRONMENT_FILES C:\\ghs\\comp_201654\\target\\ppc\\pxr40\\standalone_romdebug.ld,C:\\ghs\\comp_201654\\target\\ppc\\pxr40\\standalone_config.ld,C:\\ghs\\comp_201654\\target\\ppc\\pxr40\\memory.ld >> commands.tmp \n'
			'echo options VCAST_EXECUTE_WITH_STDOUT FALSE >> commands.tmp \n'
			'echo options VCAST_FORCE_ELAB_TYPE_SPEC TRUE >> commands.tmp \n'
			'echo options VCAST_GH_INT_FILE >> commands.tmp \n'
			'echo options VCAST_HAS_LONGLONG TRUE >> commands.tmp \n'
			'echo options VCAST_INST_FILE_MAX_LINES 0 >> commands.tmp \n'
			'echo options VCAST_MAX_STRING_LENGTH 1000 >> commands.tmp \n'
			'echo options VCAST_MAX_TARGET_FILES 50 >> commands.tmp \n'
			'echo options VCAST_MICROSOFT_LONG_LONG FALSE >> commands.tmp \n'
			'echo options VCAST_NO_MALLOC FALSE >> commands.tmp \n'
			'echo options VCAST_NO_STDIN TRUE >> commands.tmp \n'
			'echo options VCAST_REMOVE_PREPROCESSOR_COMMENTS FALSE >> commands.tmp \n'
			'echo options VCAST_RPTS_DEFAULT_FONT_FACE Arial(6) >> commands.tmp \n'
			'echo options VCAST_STDIO FALSE >> commands.tmp \n'
			'echo options VCAST_TORNADO_CONSTRUCTOR_CALL_FILE FALSE >> commands.tmp \n'
			'echo options VCAST_UNIT_TYPE SBF >> commands.tmp \n'
			'echo options WHITEBOX YES >> commands.tmp \n'
			'echo clear_default_source_dirs  >> commands.tmp \n' 
			 ]
    ccast_header_options_simulation_traction = [' Auto-Generated file \n'
                        'del commands.tmp \n'
                        'echo options ASM_FUNCS_BEHAVE_AS_INLINES TRUE >> commands.tmp \n'
                        'echo options CASE_FALLTHROUGH_BRANCH_COVERAGE FALSE >> commands.tmp \n'
                        'echo options C_COMPILER_CFG_SOURCE PY_CONFIGURATOR >> commands.tmp \n'
                        'echo options C_COMPILER_FAMILY_NAME Green_Hills >> commands.tmp \n'
                        'echo options C_COMPILER_HIERARCHY_STRING Green Hills_PPC_Bare Board_Simulator_C >> commands.tmp \n'
                        'echo options C_COMPILER_PY_ARGS --lang c --cpu=p4040 --io stdout --target sim >> commands.tmp \n'
                        'echo options C_COMPILER_TAG PPC_GH_SIM >> commands.tmp \n'
                        'echo options C_COMPILER_VERSION_CMD ccppc -V not.here.c >> commands.tmp \n'
                        'echo options C_COMPILE_CMD ccppc -c -G -w -cpu=p4040 >> commands.tmp \n'
                        'echo options C_COMPILE_EXCLUDE_FLAGS -o^^** >> commands.tmp \n'
                        'echo options C_DEBUG_CMD multi -nosplash -remote \"simppc -X83 -cpu=p4040" -p $(VECTORCAST_DIR)/DATA/green_hills/multi_playback_file_multi4.p >> commands.tmp \n'
                        'echo options C_DEFINE_LIST TX_NO_EVENT_INFO TX_ENABLE_EVENT_FILTERS NX_DISABLE_IPV6 NX_MAX_MULTICAST_GROUPS=1 NX_MAX_LISTEN_REQUESTS=4 NX_FTP_MAX_CLIENTS=1 TX_DISABLE_PREEMPTION_THRESHOLD TX_DISABLE_NOTIFY_CALLBACKS TRUCK_TYPE_DEFINITION=TRUCK_TYPE_PC MODULE_TYPE_DEFINITION=MODULE_TYPE_SMALL_DV __noinline= VCAST_USE_GH_SYSCALL >> commands.tmp \n'
                        'echo options C_EDG_FLAGS -w --c --ghs --define_macro=restrict= >> commands.tmp \n'
                        'echo options C_EXECUTE_CMD multi -nosplash -remote \"simppc -X83 -cpu=p4040" -p $(VECTORCAST_DIR)/DATA/green_hills/multi_playback_file_multi4.p >> commands.tmp \n'
                        'echo options C_LINKER_VERSION_CMD elxr -V >> commands.tmp \n'
                        'echo options C_LINK_CMD ccppc -G -map=vcast.map -cpu=p4040 >> commands.tmp \n'
                        'echo options C_LINK_OPTIONS  >> commands.tmp \n'
                        'echo options C_OUTPUT_FLAG -o^^ >> commands.tmp \n'
                        'echo options C_PREPROCESS_CMD ccppc -E -C -cpu=p4040 >> commands.tmp \n'
                        'echo options C_PREPROCESS_FILE  >> commands.tmp \n'
                        'echo options EVENT_LIMIT 500 >> commands.tmp \n'
                        'echo options RANGE_CHECK NONE >> commands.tmp \n'
                        'echo options STANDARD_ERROR NORMAL >> commands.tmp \n'
                        'echo options VCAST_ASSEMBLY_FILE_EXTENSIONS asm s >> commands.tmp \n'
                        'echo options VCAST_BUFFER_OUTPUT TRUE >> commands.tmp \n'
                        'echo options VCAST_COMMAND_LINE_DEBUGGER FALSE >> commands.tmp \n'
                        'echo options VCAST_COMPILER_SUPPORTS_CPP_CASTS TRUE >> commands.tmp \n'
                        'echo options VCAST_COVERAGE_FOR_AGGREGATE_INIT FALSE >> commands.tmp \n'
                        'echo options VCAST_DISABLE_CPP_EXCEPTIONS FALSE >> commands.tmp \n'
                        'echo options VCAST_DISPLAY_UNINST_EXPR FALSE >> commands.tmp \n'
                        'echo options VCAST_DUMP_BUFFER TRUE >> commands.tmp \n'
                        'echo options VCAST_ENABLE_FUNCTION_CALL_COVERAGE FALSE >> commands.tmp \n'
                        'echo options VCAST_ENVIRONMENT_FILES C:\\ghs\\comp_201654\\target\\ppc\\pxr40\\memory.ld,C:\\ghs\\comp_201654\\target\\ppc\\pxr40\\standalone_romdebug.ld,C:\\ghs\\comp_201654\\target\\ppc\\pxr40\\standalone_config.ld >> commands.tmp \n'
                        'echo options VCAST_EXECUTE_WITH_STDOUT FALSE >> commands.tmp\n'
                        'echo options VCAST_HAS_LONGLONG TRUE >> commands.tmp \n'
                        'echo options VCAST_LIBRARY_STUBS memset >> commands.tmp \n'
                        'echo options VCAST_MAX_TARGET_FILES 50 >> commands.tmp \n'
                        'echo options VCAST_NO_MALLOC FALSE >> commands.tmp \n'
                        'echo options VCAST_NO_STDIN TRUE >> commands.tmp \n'
                        'echo options VCAST_REMOVE_PREPROCESSOR_COMMENTS FALSE >> commands.tmp \n'
                        'echo options VCAST_RPTS_DEFAULT_FONT_FACE Arial(6) >> commands.tmp \n'
                        'echo options VCAST_STDIO FALSE >> commands.tmp \n'
                        'echo options VCAST_TYPEOF_OPERATOR TRUE >> commands.tmp \n'
                        'echo options VCAST_UNIT_TYPE UUT >> commands.tmp \n'
                        'echo options WHITEBOX YES >> commands.tmp \n'
                        'echo clear_default_source_dirs  >> commands.tmp \n'
                        ]
    aecp_tail_options_simulation = ['echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE) \n', 
                   'echo environment build AECP_MAIN_INTEGRATION.env >> commands.tmp \n', 
                   'echo /E:AECP_MAIN_INTEGRATION tools script run AECP_MAIN_INTEGRATION.tst >> commands.tmp \n', 
                   'echo /E:AECP_MAIN_INTEGRATION execute batch >> commands.tmp \n',
                   'echo /E:AECP_MAIN_INTEGRATION reports custom management AECP_MAIN_SIMULATION_INTEGRATION_management_report.html >> commands.tmp \n', 
                   '"%VECTORCAST_DIR%\CLICAST"  /L:C tools execute commands.tmp false \n'
                         ]

    becp_tail_options_simulation = ['echo environment build BECP_MAIN_INTEGRATION.env >> commands.tmp \n',
                    'echo /E:BECP_MAIN_INTEGRATION tools script run BECP_MAIN_INTEGRATION.tst >> commands.tmp \n',
                    'echo /E:BECP_MAIN_INTEGRATION execute batch >> commands.tmp \n',
                    'echo /E:BECP_MAIN_INTEGRATION reports custom management BECP_MAIN_SIMULATION_INTEGRATION_management_report.html >> commands.tmp \n',
                    '"%VECTORCAST_DIR%\CLICAST"  /L:C tools execute commands.tmp false \n'
                         ]

    switchboard_tail_options_simulation = ['echo environment build SWITCH_BOARD_INTEGRATION.env >> commands.tmp \n',
                    'echo /E:SWITCH_BOARD_INTEGRATION tools script run SWITCH_BOARD_INTEGRATION.tst >> commands.tmp \n',
                    'echo /E:SWITCH_BOARD_INTEGRATION execute batch >> commands.tmp \n',
                    'echo /E:SWITCH_BOARD_INTEGRATION reports custom management SWITCH_BOARD_SIMULATION_INTEGRATION_management_report.html >> commands.tmp \n',
                    '"%VECTORCAST_DIR%\CLICAST"  /L:C tools execute commands.tmp false \n'
                         ]

    eventmanager_tail_options_simulation = ['echo environment build EVENT_MANAGER_INTEGRATION.env >> commands.tmp \n',
                    'echo /E:EVENT_MANAGER_INTEGRATION tools script run EVENT_MANAGER_INTEGRATION.tst >> commands.tmp \n',
                    'echo /E:EVENT_MANAGER_INTEGRATION execute batch >> commands.tmp \n',
                    'echo /E:EVENT_MANAGER_INTEGRATION reports custom management EVENT_MANAGER_SIMULATION_INTEGRATION_management_report.html >> commands.tmp \n',
                    '"%VECTORCAST_DIR%\CLICAST"  /L:C tools execute commands.tmp false \n'
                         ]

    externalalarm_tail_options_simulation = ['echo options TESTABLE_SOURCE_DIR $(CROWN_SOURCE_BASE) \n', 
                   'echo environment build EXTERNAL_ALARM_INTEGRATION.env >> commands.tmp \n', 
                   'echo /E:EXTERNAL_ALARM_INTEGRATION tools script run EXTERNAL_ALARM_INTEGRATION.tst >> commands.tmp \n', 
                   'echo /E:EXTERNAL_ALARM_INTEGRATION execute batch >> commands.tmp \n',
                   'echo /E:EXTERNAL_ALARM_INTEGRATION reports custom management EXTERNAL_ALARM_SIMULATION_INTEGRATION_management_report.html >> commands.tmp \n', 
                   '"%VECTORCAST_DIR%\CLICAST"  /L:C tools execute commands.tmp false \n'
                         ]

    gyrodriver_tail_options_simulation = ['echo environment build GYRO_DRIVER_INTEGRATION.env >> commands.tmp \n',
                    'echo /E:GYRO_DRIVER_INTEGRATION tools script run GYRO_DRIVER_INTEGRATION.tst >> commands.tmp \n',
                    'echo /E:GYRO_DRIVER_INTEGRATION execute batch >> commands.tmp \n',
                    'echo /E:GYRO_DRIVER_INTEGRATION reports custom management GYRO_DRIVER_SIMULATION_INTEGRATION_management_report.html >> commands.tmp \n',
                    '"%VECTORCAST_DIR%\CLICAST"  /L:C tools execute commands.tmp false \n'
                         ]
    
    featuresinterface_tail_options_simulation = [ 'echo environment build FEATURES_INTERFACE_INTEGRATION.env >> commands.tmp \n',
                    'echo /E:FEATURES_INTERFACE_INTEGRATION tools script run FEATURES_INTERFACE_INTEGRATION.tst >> commands.tmp \n',
                    'echo /E:FEATURES_INTERFACE_INTEGRATION execute batch >> commands.tmp \n', 
                    'echo /E:FEATURES_INTERFACE_INTEGRATION reports custom management FEATURES_INTERFACE_SIMULATION_INTEGRATION_management_report.html >> commands.tmp \n', 
                    '"%VECTORCAST_DIR%\CLICAST"  /L:C tools execute commands.tmp false \n' 
                         ]

    supvinterface_tail_options_simulation = [ 'echo environment build SUPV_INTERFACE_INTEGRATION.env >> commands.tmp \n',
                                 'echo /E:SUPV_INTERFACE_INTEGRATION tools script run SUPV_INTERFACE_INTEGRATION.tst >> commands.tmp \n',
                                 'echo /E:SUPV_INTERFACE_INTEGRATION execute batch >> commands.tmp\n',
                                 'echo /E:SUPV_INTERFACE_INTEGRATION reports custom management SUPV_INTERFACE_SIMULATION_INTEGRATION_management_report.html >> commands.tmp \n',
                                 '"%VECTORCAST_DIR%\CLICAST"  /L:C tools execute commands.tmp false \n'
                              ]
    imminterface_tail_options_simulation = [ 'echo environment build IMM_INTERFACE_INTEGRATION.env >> commands.tmp \n',
                                 'echo /E:IMM_INTERFACE_INTEGRATION tools script run IMM_INTERFACE_INTEGRATION.tst >> commands.tmp \n',
                                 'echo /E:IMM_INTERFACE_INTEGRATION execute batch >> commands.tmp\n',
                                 'echo /E:IMM_INTERFACE_INTEGRATION reports custom management IMM_INTERFACE_SIMULATION_INTEGRATION_management_report.html >> commands.tmp \n',
                                 '"%VECTORCAST_DIR%\CLICAST"  /L:C tools execute commands.tmp false \n'
                              ]
    osinterface_tail_options_simulation = [ 'echo environment build OS_INTERFACE_INTEGRATION.env >> commands.tmp \n',
                                 'echo /E:OS_INTERFACE_INTEGRATION tools script run OS_INTERFACE_INTEGRATION.tst >> commands.tmp \n',
                                 'echo /E:OS_INTERFACE_INTEGRATION execute batch >> commands.tmp\n',
                                 'echo /E:OS_INTERFACE_INTEGRATION reports custom management OS_INTERFACE_SIMULATION_INTEGRATION_management_report.html >> commands.tmp \n',
                                 '"%VECTORCAST_DIR%\CLICAST"  /L:C tools execute commands.tmp false \n'
                              ]
    steering_tail_options_simulation = [  'echo environment build STEERING_SIMULATION_INTEGRATION.env >> commands.tmp \n',
                                 'echo /E:STEERING_SIMULATION_INTEGRATION tools script run STEERING_SIMULATION_INTEGRATION.tst >> commands.tmp \n',
                                 'echo /E:STEERING_SIMULATION_INTEGRATION execute batch >> commands.tmp\n',
                                 'echo /E:STEERING_SIMULATION_INTEGRATION reports custom management STEERING_SIMULATION_INTEGRATION_management_report.html >> commands.tmp \n',
                                 '"%VECTORCAST_DIR%\CLICAST"  /L:C tools execute commands.tmp false \n'
                              ]
    ipinterface_tail_options_simulation = [ 'echo environment build IP_INTERFACE_INTEGRATION.env >> commands.tmp \n',
                                 'echo /E:IP_INTERFACE_INTEGRATION tools script run IP_INTERFACE_INTEGRATION.tst >> commands.tmp \n',
                                 'echo /E:IP_INTERFACE_INTEGRATION execute batch >> commands.tmp\n',
                                 'echo /E:IP_INTERFACE_INTEGRATION reports custom management IP_INTERFACE_SIMULATION_INTEGRATION_management_report.html >> commands.tmp \n',
                                 '"%VECTORCAST_DIR%\CLICAST"  /L:C tools execute commands.tmp false \n'
                              ]
    manifestmanager_tail_options_simulation = [  'echo environment build MANIFEST_MANAGER_INTEGRATION.env >> commands.tmp \n',
                                 'echo /E:MANIFEST_MANAGER_INTEGRATION tools script run MANIFEST_MANAGER_INTEGRATION.tst >> commands.tmp \n',
                                 'echo /E:MANIFEST_MANAGER_INTEGRATION execute batch >> commands.tmp\n',
                                 'echo /E:MANIFEST_MANAGER_INTEGRATION reports custom management MANIFEST_MANAGER_SIMULATION_INTEGRATION_management_report.html >> commands.tmp \n',
                                 '"%VECTORCAST_DIR%\CLICAST"  /L:C tools execute commands.tmp false \n'
                              ]
    traction_tail_options_simulation = [  'echo environment build TRACTION_SIMULATION_INTEGRATION.env >> commands.tmp \n',
                                 'echo /E:TRACTION_SIMULATION_INTEGRATION tools script run TRACTION_SIMULATION_INTEGRATION.tst >> commands.tmp \n',
                                 'echo /E:TRACTION_SIMULATION_INTEGRATION execute batch >> commands.tmp\n',
                                 'echo /E:TRACTION_SIMULATION_INTEGRATION reports custom management TRACTION_SIMULATION_SIMULATION_INTEGRATION_management_report.html >> commands.tmp \n',
                                 '"%VECTORCAST_DIR%\CLICAST"  /L:C tools execute commands.tmp false \n'
                              ]
    fram_driver_tail_options_simulation = [    'echo environment build FRAM_DRIVER_INTEGRATION.env >> commands.tmp \n',
                                 'echo /E:FRAM_DRIVER_INTEGRATION tools script run FRAM_DRIVER_INTEGRATION.tst >> commands.tmp \n',
                                 'echo /E:FRAM_DRIVER_INTEGRATION execute batch >> commands.tmp\n',
                                 'echo /E:FRAM_DRIVER_INTEGRATION reports custom management FRAM_DRIVER_SIMULATION_INTEGRATION_management_report.html >> commands.tmp \n',
                                 '"%VECTORCAST_DIR%\CLICAST"  /L:C tools execute commands.tmp false \n'
                              ]
    utilities_tail_options_simulation = [  'echo environment build UTILITIES_INTEGRATION.env >> commands.tmp \n',
                                 'echo /E:UTILITIES_INTEGRATION tools script run UTILITIES_INTEGRATION.tst >> commands.tmp \n',
                                 'echo /E:UTILITIES_INTEGRATION execute batch >> commands.tmp \n',
                                 'echo /E:UTILITIES_INTEGRATION reports custom management UTILITIES_SIMULATION_INTEGRATION_management_report.html >> commands.tmp \n',
                                 '"%VECTORCAST_DIR%\CLICAST"  /L:C tools execute commands.tmp false \n'
                               ]
    diagnostics_tail_options_simulation = [  'echo environment build DIAGNOSTICS_INTEGRATION.env >> commands.tmp \n',
                                 'echo /E:DIAGNOSTICS_INTEGRATION tools script run DIAGNOSTICS_INTEGRATION.tst >> commands.tmp \n',
                                 'echo /E:DIAGNOSTICS_INTEGRATION execute batch >> commands.tmp \n',
                                 'echo /E:DIAGNOSTICS_INTEGRATION reports custom management DIAGNOSTICS_SIMULATION_INTEGRATION_management_report.html >> commands.tmp \n',
                                 '"%VECTORCAST_DIR%\CLICAST"  /L:C tools execute commands.tmp false \n'
                               ]

    # Determine path list and test .bat tail options setting and if on target or simulated 
    if Testname == eTestname.BECP_MAIN_INTEGRATION:
        if fRunSimulated == True:
            CurrentPathList = "BECP_MAIN_INTEGRATION_PathList.txt"
            CurrentBatfile = "BECP_MAIN_SIMULATION_INTEGRATION.bat"
            CurrentTailOptions = becp_tail_options_simulation  
        else:
            CurrentPathList = "BECP_MAIN_INTEGRATION_PathList.txt"
            CurrentBatfile = "BECP_MAIN_INTEGRATION_GEN2.bat"
            CurrentTailOptions = becp_tail_options                        
        
    elif Testname == eTestname.SWITCH_BOARD_INTEGRATION:
        if fRunSimulated == True:
            CurrentPathList = "SWITCH_BOARD_INTEGRATION_PathList.txt"
            CurrentBatfile = "SWITCH_BOARD_SIMULATION_INTEGRATION.bat"
            CurrentTailOptions = switchboard_tail_options_simulation
        else:
            CurrentPathList = "SWITCH_BOARD_INTEGRATION_PathList.txt"
            CurrentBatfile = "SWITCH_BOARD_INTEGRATION_GEN2.bat"
            CurrentTailOptions = switchboard_tail_options
            
    elif Testname == eTestname.EVENT_MANAGER_INTEGRATION:
        if fRunSimulated == True:
            CurrentPathList = "EVENT_MANAGER_INTEGRATION_PathList.txt"
            CurrentBatfile = "EVENT_MANAGER_SIMULATION_INTEGRATION.bat"
            CurrentTailOptions = eventmanager_tail_options_simulation
        else:
            CurrentPathList = "EVENT_MANAGER_INTEGRATION_PathList.txt"
            CurrentBatfile = "EVENT_MANAGER_INTEGRATION_GEN2.bat"
            CurrentTailOptions = eventmanager_tail_options

    elif Testname == eTestname.EXTERNAL_ALARM_INTEGRATION:
        if fRunSimulated == True:
            CurrentPathList = "EXTERNAL_ALARM_INTEGRATION_PathList.txt"
            CurrentBatfile = "EXTERNAL_ALARM_SIMULATION_INTEGRATION.bat"
            CurrentTailOptions = externalalarm_tail_options_simulation
        else:
            CurrentPathList = "EXTERNAL_ALARM_INTEGRATION_PathList.txt"
            CurrentBatfile = "EXTERNAL_ALARM_INTEGRATION_GEN2.bat"
            CurrentTailOptions = externalalarm_tail_options

    elif Testname == eTestname.GYRO_DRIVER_INTEGRATION:
        if fRunSimulated == True:
            CurrentPathList = "GYRO_DRIVER_INTEGRATION_PathList.txt"
            CurrentBatfile = "GYRO_DRIVER_SIMULATION_INTEGRATION.bat"
            CurrentTailOptions = gyrodriver_tail_options_simulation
        else:
            CurrentPathList = "GYRO_DRIVER_INTEGRATION_PathList.txt"
            CurrentBatfile = "GYRO_DRIVER_INTEGRATION_GEN2.bat"
            CurrentTailOptions = gyrodriver_tail_options

    elif Testname == eTestname.FEATURES_INTERFACE_INTEGRATION:
        if fRunSimulated == True:
            CurrentPathList = "FEATURES_INTERFACE_INTEGRATION_PathList.txt"
            CurrentBatfile = "FEATURES_INTERFACE_SIMULATION_INTEGRATION.bat"
            CurrentTailOptions = featuresinterface_tail_options_simulation
        else:
            CurrentPathList = "FEATURES_INTERFACE_INTEGRATION_PathList.txt"
            CurrentBatfile = "FEATURES_INTERFACE_INTEGRATION_GEN2.bat"
            CurrentTailOptions = featuresinterface_tail_options

    elif Testname == eTestname.SUPV_INTERFACE_INTEGRATION:
        if fRunSimulated == True:
            CurrentPathList = "SUPV_INTERFACE_INTEGRATION_PathList.txt"
            CurrentBatfile = "SUPV_INTERFACE_SIMULATION_INTEGRATION.bat"
            CurrentTailOptions = supvinterface_tail_options_simulation
        else:
            CurrentPathList = "SUPV_INTERFACE_INTEGRATION_PathList.txt"
            CurrentBatfile = "SUPV_INTERFACE_INTEGRATION_GEN2.bat"
            CurrentTailOptions = supvinterface_tail_options
            
    elif Testname == eTestname.IMM_INTERFACE_INTEGRATION:
        if fRunSimulated == True:
            CurrentPathList = "IMM_INTERFACE_INTEGRATION_PathList.txt"
            CurrentBatfile = "IMM_INTERFACE_SIMULATION_INTEGRATION.bat"
            CurrentTailOptions = imminterface_tail_options_simulation
        else:
            CurrentPathList = "IMM_INTERFACE_INTEGRATION_PathList.txt"
            CurrentBatfile = "IMM_INTERFACE_INTEGRATION_GEN2.bat"
            CurrentTailOptions = imminterface_tail_options
            
    elif Testname == eTestname.OS_INTERFACE_INTEGRATION:
        if fRunSimulated == True:
            CurrentPathList = "OS_INTERFACE_INTEGRATION_PathList.txt"
            CurrentBatfile = "OS_INTERFACE_SIMULATION_INTEGRATION.bat"
            CurrentTailOptions = osinterface_tail_options_simulation
        else:
            CurrentPathList = "OS_INTERFACE_INTEGRATION_PathList.txt"
            CurrentBatfile = "OS_INTERFACE_INTEGRATION_GEN2.bat"
            CurrentTailOptions = osinterface_tail_options
            
    elif Testname == eTestname.STEERING_INTEGRATION:
        if fRunSimulated == True:
            CurrentPathList = "STEERING_INTEGRATION_PathList.txt"
            CurrentBatfile = "STEERING_SIMULATION_INTEGRATION.bat"
            CurrentTailOptions = steering_tail_options_simulation
        else:
            CurrentPathList = "STEERING_INTEGRATION_PathList.txt"
            CurrentBatfile = "STEERING_INTEGRATION_GEN2.bat"
            CurrentTailOptions = steering_tail_options

    elif Testname == eTestname.IP_INTERFACE_INTEGRATION:
        if fRunSimulated == True:
            CurrentPathList = "IP_INTERFACE_INTEGRATION_PathList.txt"
            CurrentBatfile = "IP_INTERFACE_SIMULATION_INTEGRATION.bat"
            CurrentTailOptions = ipinterface_tail_options_simulation
        else:
            CurrentPathList = "IP_INTERFACE_INTEGRATION_PathList.txt"
            CurrentBatfile = "IP_INTERFACE_INTEGRATION_GEN2.bat"
            CurrentTailOptions = ipinterface_tail_options

    elif Testname == eTestname.MANIFEST_MANAGER_INTEGRATION:
        if fRunSimulated == True:
            CurrentPathList = "MANIFEST_MANAGER_INTEGRATION_PathList.txt"
            CurrentBatfile = "MANIFEST_MANAGER_SIMULATION_INTEGRATION.bat"
            CurrentTailOptions = manifestmanager_tail_options_simulation
        else:
            CurrentPathList = "MANIFEST_MANAGER_INTEGRATION_PathList.txt"
            CurrentBatfile = "MANIFEST_MANAGER_INTEGRATION_GEN2.bat"
            CurrentTailOptions = manifestmanager_tail_options
            
    elif Testname == eTestname.TRACTION_INTEGRATION:
        if fRunSimulated == True:
            CurrentPathList = "TRACTION_INTEGRATION_PathList.txt"
            CurrentBatfile = "TRACTION_SIMULATION_INTEGRATION.bat"
            CurrentTailOptions = traction_tail_options_simulation
        else:
            CurrentPathList = "TRACTION_INTEGRATION_PathList.txt"
            CurrentBatfile = "TRACTION_INTEGRATION_GEN2.bat"
            CurrentTailOptions = traction_tail_options

    elif Testname == eTestname.FRAM_DRIVER_INTEGRATION:
        if fRunSimulated == True:
            CurrentPathList = "FRAM_DRIVER_INTEGRATION_PathList.txt"
            CurrentBatfile = "FRAM_DRIVER_SIMULATION_INTEGRATION.bat"
            CurrentTailOptions = fram_driver_tail_options_simulation
        else:
            CurrentPathList = "FRAM_DRIVER_INTEGRATION_PathList.txt"
            CurrentBatfile = "FRAM_DRIVER_INTEGRATION_GEN2.bat"
            CurrentTailOptions = fram_driver_tail_options

    elif Testname == eTestname.UTILITIES_INTEGRATION:
        if fRunSimulated == True:
            CurrentPathList = "UTILITIES_INTEGRATION_PathList.txt"
            CurrentBatfile = "UTILITIES_SIMULATION_INTEGRATION.bat"
            CurrentTailOptions = utilities_tail_options_simulation
        else:
            CurrentPathList = "UTILITIES_INTEGRATION_PathList.txt"
            CurrentBatfile = "UTILITIES_INTEGRATION_GEN2.bat"
            CurrentTailOptions = utilities_tail_options

    elif Testname == eTestname.DIAGNOSTICS_INTEGRATION:
        if fRunSimulated == True:
            CurrentPathList = "DIAGNOSTICS_INTEGRATION_PathList.txt"
            CurrentBatfile = "DIAGNOSTICS_SIMULATION_INTEGRATION.bat"
            CurrentTailOptions = diagnostics_tail_options_simulation
        else:
            CurrentPathList = "DIAGNOSTICS_INTEGRATION_PathList.txt"
            CurrentBatfile = "DIAGNOSTICS_INTEGRATION_GEN2.bat"
            CurrentTailOptions = diagnostics_tail_options
            
    else:
        if fRunSimulated == True:
            CurrentPathList = "AECP_MAIN_INTEGRATION_PathList.txt"
            CurrentBatfile = "AECP_MAIN_SIMULATION_INTEGRATION.bat"
            CurrentTailOptions = aecp_tail_options_simulation
        else:
            CurrentPathList = "AECP_MAIN_INTEGRATION_PathList.txt"
            CurrentBatfile = "AECP_MAIN_INTEGRATION_GEN2.bat"
            CurrentTailOptions = aecp_tail_options

    # Open up PathList.txt and dump the contents to a input file variable formated as required and strip white space
    input_file = open(CurrentPathList, 'r')
    for line in input_file:
        line = line.strip()
        no_space.append(line)
    
    input_file = no_space
    for line in input_file:    
        new_contents.append(string1 + line + string2 + '\n')
                
    # Update a temp file with correct path information    
    fd = open('.batfile.txt', "w")
    fd.writelines(new_contents )        
    fd.close()

    # new work here open .bat file and erase contents
    f = open(CurrentBatfile, "w")
    f.writelines(' ')
    f.close()

    # write the header options information according to if on target or simulated 
    f = open(CurrentBatfile,"w")
    if Testname == eTestname.BECP_MAIN_INTEGRATION:
        if fRunSimulated == True:
            f.writelines(ccast_header_options_simulation)
        else:
            f.writelines(ccast_header_options_gen2)
            
    elif Testname == eTestname.SWITCH_BOARD_INTEGRATION:
        if fRunSimulated == True:
            f.writelines(ccast_header_options_simulation)
        else:
            f.writelines(ccast_header_options_gen2)
            
    elif Testname == eTestname.EVENT_MANAGER_INTEGRATION:
        if fRunSimulated == True:
            f.writelines(ccast_header_options_simulation)
        else:
            f.writelines(ccast_header_options_gen2)
            
    elif Testname == eTestname.EXTERNAL_ALARM_INTEGRATION:
        if fRunSimulated == True:
            f.writelines(ccast_header_options_simulation)
        else:
            f.writelines(ccast_header_options_gen2)

    elif Testname == eTestname.GYRO_DRIVER_INTEGRATION:
        if fRunSimulated == True:
            f.writelines(ccast_header_options_simulation)
        else:
            f.writelines(ccast_header_options_gen2)

    elif Testname == eTestname.FEATURES_INTERFACE_INTEGRATION:
        if fRunSimulated == True:
            f.writelines(ccast_header_options_simulation)
        else:
            f.writelines(ccast_header_options_gen2)

    elif Testname == eTestname.SUPV_INTERFACE_INTEGRATION:
        if fRunSimulated == True:
            f.writelines(ccast_header_options_simulation)
        else:
            f.writelines(ccast_header_options_gen2)
            
    elif Testname == eTestname.IMM_INTERFACE_INTEGRATION:
        if fRunSimulated == True:
            f.writelines(ccast_header_options_simulation)
        else:
            f.writelines(ccast_header_options_gen2)

    elif Testname == eTestname.OS_INTERFACE_INTEGRATION:
        if fRunSimulated == True:
            f.writelines(ccast_header_options_simulation)
        else:
            f.writelines(ccast_header_options_gen2)

    elif Testname == eTestname.STEERING_INTEGRATION:
        if fRunSimulated == True:
            f.writelines(ccast_header_options_simulation)
        else:
            f.writelines(ccast_header_options_gen2)

    elif Testname == eTestname.IP_INTERFACE_INTEGRATION:
        if fRunSimulated == True:
            f.writelines(ccast_header_options_simulation)
        else:
            f.writelines(ccast_header_options_gen2)
            
    elif Testname == eTestname.MANIFEST_MANAGER_INTEGRATION:
        if fRunSimulated == True:
            f.writelines(ccast_header_options_simulation)
        else:
            f.writelines(ccast_header_options_gen2)

    elif Testname == eTestname.TRACTION_INTEGRATION:
        if fRunSimulated == True:
            f.writelines(ccast_header_options_simulation_traction)
        else:
            f.writelines(ccast_header_options_gen2)

    elif Testname == eTestname.FRAM_DRIVER_INTEGRATION:
        if fRunSimulated == True:
            f.writelines(ccast_header_options_simulation)
        else:
            f.writelines(ccast_header_options_gen2)

    elif Testname == eTestname.UTILITIES_INTEGRATION:
        if fRunSimulated == True:
            f.writelines(ccast_header_options_simulation)
        else:
            f.writelines(ccast_header_options_gen2)
            
    elif Testname == eTestname.DIAGNOSTICS_INTEGRATION:
        if fRunSimulated == True:
            f.writelines(ccast_header_options_simulation)
        else:
            f.writelines(ccast_header_options_gen2)
            
    else:
        if fRunSimulated == True:
            f.writelines(ccast_header_options_simulation)
        else:
            f.writelines(ccast_header_options_gen2)

    # append new paths back in
    f = open(CurrentBatfile,"a")
    f.writelines(new_contents)
    f.close()

    # append to the end the tail options
    f = open(CurrentBatfile,"a")
    f.writelines(CurrentTailOptions)
    f.close()

    print ("--------------------------------------------------------------------------------\n")
    print ("                 Wrote .bat file with new paths                                 \n")
    print ("--------------------------------------------------------------------------------\n")

def ProcessTestFolders(fRunSimulated):

    
    # Are the folders specified there    
    AllowFolderUpdate = CheckForFolder()
    
    # The file are all there proceed
    if AllowFolderUpdate != True:   
        
        # Cycle through all the tests add more here when needed
        for index in range (0,eTestname.MAX_INTEGRATION):

            if index == 0:  
                # Walk through the file structure
                CurrentTest = eTestname.AECP_MAIN_INTEGRATION

            elif index == 1:    
                # Walk through the file structure
                CurrentTest = eTestname.BECP_MAIN_INTEGRATION
            
            elif index == 2:     
                # Walk through the file structure
                CurrentTest = eTestname.SWITCH_BOARD_INTEGRATION

            elif index == 3:     
                # Walk through the file structure
                CurrentTest = eTestname.EVENT_MANAGER_INTEGRATION

            elif index == 4:     
                # Walk through the file structure
                CurrentTest = eTestname.EXTERNAL_ALARM_INTEGRATION

            elif index == 5:     
                # Walk through the file structure
                CurrentTest = eTestname.GYRO_DRIVER_INTEGRATION

            elif index == 6:
                # Walk through the file structure
                CurrentTest = eTestname.FEATURES_INTERFACE_INTEGRATION

            elif index == 7:
                # Walk through the file structure
                CurrentTest = eTestname.SUPV_INTERFACE_INTEGRATION

            elif index == 8:
                # Walk through the file structure
                CurrentTest = eTestname.IMM_INTERFACE_INTEGRATION

            elif index == 9:
                # Walk through the file structure
                CurrentTest = eTestname.OS_INTERFACE_INTEGRATION   

            elif index == 10:
                # Walk through the file structure
                CurrentTest = eTestname.STEERING_INTEGRATION

            elif index == 11:
                # Walk through the file structure
                CurrentTest = eTestname.IP_INTERFACE_INTEGRATION

            elif index == 12:
                # Walk through the file structure
                CurrentTest = eTestname.MANIFEST_MANAGER_INTEGRATION
            
            elif index == 13:
                # Walk through the file structure
                CurrentTest = eTestname.TRACTION_INTEGRATION

            elif index == 14:
                # Walk through the file structure
                CurrentTest = eTestname.FRAM_DRIVER_INTEGRATION

            elif index == 15:
                # Walk through the file structure
                CurrentTest = eTestname.UTILITIES_INTEGRATION

            elif index == 16:
                # Walk through the file structure
                CurrentTest = eTestname.DIAGNOSTICS_INTEGRATION 
            
            # Walk through the file structure
            Traverse(CurrentTest)

            # Open find and replace within .env file adjust for simulated vs on target 
            Vcast_envFile(CurrentTest, fRunSimulated)

            # Open find and replace within .bat file adjust for simulated vs on target 
            Vcast_batFile(CurrentTest, fRunSimulated)
      
    # Return the status
    return AllowFolderUpdate
   
# Main program - Supports execution from the command line
if __name__ == "__main__":

    print ("--------------------------------------------------------------------------------\n")
    print ("This Python script traverses the: rtc_workspaces/TmlVcmMasterBuildDef001_Sandbox\n ")
    print ("--------------------------------------------------------------------------------\n")

    ProcessTestFolders()
    
