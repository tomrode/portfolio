# Name:      MD5_Hash_Class.py
#
# Purpose:   This python script will calculate the MD5 hash for a given ".C" file, it will record if a difference has been made
#            from a specified set of Hashes. Also it will determine if given C file exists or not and return appropriate batch file to run.
#        
#
# Author:    Thomas Rode
#
# Options:   MD5HashDriver(SelectReadWrite) determines if Hash needed to be saved or read  
#
# Return:    Test file list that had a diffence in Hash from a previous baseline
# 
# Note:      This driver can be used in a standalone script as well to read or recalculate Hashes if need be. Also care needs to be taken when modifying 
#            all_regressions[] so its up to date and correct.
#
# Python Version: 2.7.11
#          
# Reference: https://www.python.org/ (open source)
#

import os, sys
import hashlib
from EmailTestResults import email_FoldersMissing


class MD5Hash(object):

  # Constructor
  def __init__(self, C_fileToHash, HashValfile, CurrentHash, SavedHash):
    # instance variables
    self.C_fileToHash = C_fileToHash
    self.HashValfile = HashValfile
    self.CurrentHash = CurrentHash
    self.SavedHash = SavedHash

  # Accesser Methods (getters) and Mutator Method (setters)      
  def WriteHash(self):    
    f = open(self.HashValfile, "w")
    fhash = self.CurrentHash
    #print("This is the Saved Hash:", str(fhash))
    f.write(fhash)
    f.close()

  def ReadHash(self):
    f = open(self.HashValfile, 'r')
    self.SavedHash = f.read()
    #print("This is what was Saved Hash:", str(self.SavedHash))
    f.close()
    return self.SavedHash

  def MD5_Hash_Calc(self):
    BLOCKSIZE = 65536
    hasher = hashlib.md5()
    with open(self.C_fileToHash, 'rb') as afile:
        buf = afile.read(BLOCKSIZE)
        while len(buf) > 0:
            hasher.update(buf)
            buf = afile.read(BLOCKSIZE)
    self.CurrentHash = hasher.hexdigest()
    #print("This is the Current Hash:", str(self.CurrentHash))
    return self.CurrentHash


def MD5HashDriver(SelectReadWrite):
  
  # This list will be appended if a hash is different and returned to StartRegression.py
  Testname = []
  TestnameStripped = []

  # List of changed files
  changed_files_list = []

  # List of missing .C files
  Missing_files_list = []

  # MD5 txt file suffix
  MD5HASHtxt = '_MD5Hash.txt'

  # Set base path
  CROWN_SOURCE_BASE = 'C:/rtc_workspaces/TmlVcmMasterBuildDef001_Sandbox/'
 
  # Folder location for all Hashes
  md5_output_path =   'C:/vcast-buildsystem-orig/Regression-Original-Scripts/MD5Hashes/'

  # All overall regressions lists
  all_regressions = [
                      #AECP 
                      [
                         #'C' file            path where the file resides                           batch file to add if Hash mis-match
                         ['aecp_main',        (CROWN_SOURCE_BASE + 'VCM_APP/src/drivers/aecp/'),    'AECP_MAIN_INTEGRATION_GEN2.bat'],
                         ['imm_interface',    (CROWN_SOURCE_BASE + 'VCM_APP/src/general/'     ),    'AECP_MAIN_INTEGRATION_GEN2.bat'],
                         ['immcomm_driver',   (CROWN_SOURCE_BASE + 'VCM_APP/src/drivers/comm/'),    'AECP_MAIN_INTEGRATION_GEN2.bat']
                      ],
                      #BECP 
                      [                         
                         ['becp_main',        (CROWN_SOURCE_BASE + 'VCM_APP/src/drivers/becp/'),    'BECP_MAIN_INTEGRATION_GEN2.bat'],
                         ['supv_interface',   (CROWN_SOURCE_BASE + 'VCM_APP/src/general/'),         'BECP_MAIN_INTEGRATION_GEN2.bat']                      
                      ],
                      #EVENT_MANAGER
                      [                          
                         ['EventManager',     (CROWN_SOURCE_BASE + 'VCM_APP/src/general/'),        'EVENT_MANAGER_INTEGRATION_GEN2.bat'],
                         ['NvMemory',         (CROWN_SOURCE_BASE + 'VCM_APP/src/nvstorage/fram/'), 'EVENT_MANAGER_INTEGRATION_GEN2.bat'],
                         ['OS_Interface',     (CROWN_SOURCE_BASE + 'VCM_APP/src/general/'),        'EVENT_MANAGER_INTEGRATION_GEN2.bat'],
                         ['XcpUserFlash',     (CROWN_SOURCE_BASE + 'VCM_APP/src/drivers/xcp/usr/'),'EVENT_MANAGER_INTEGRATION_GEN2.bat'],
                         ['crc',              (CROWN_SOURCE_BASE + 'VCM_APP/src/utilities/'),      'EVENT_MANAGER_INTEGRATION_GEN2.bat'],
                         ['debug_support',    (CROWN_SOURCE_BASE + 'VCM_APP/src/utilities/'),      'EVENT_MANAGER_INTEGRATION_GEN2.bat'],
                         ['imm_interface',    (CROWN_SOURCE_BASE + 'VCM_APP/src/general/'     ),   'EVENT_MANAGER_INTEGRATION_GEN2.bat'],
                         ['immcomm_driver',   (CROWN_SOURCE_BASE + 'VCM_APP/src/drivers/comm/'),   'EVENT_MANAGER_INTEGRATION_GEN2.bat'],
                         ['instrument',       (CROWN_SOURCE_BASE + 'VCM_APP/src/utilities/'),      'EVENT_MANAGER_INTEGRATION_GEN2.bat'],
                         ['io_thread',        (CROWN_SOURCE_BASE + 'VCM_APP/src/drivers/io/'),     'EVENT_MANAGER_INTEGRATION_GEN2.bat'],
                         ['param',            (CROWN_SOURCE_BASE + 'VCM_APP/src/parameters/'),     'EVENT_MANAGER_INTEGRATION_GEN2.bat'],
                         ['pd_data_test',     (CROWN_SOURCE_BASE + 'VCM_APP/src/nvstorage/'),      'EVENT_MANAGER_INTEGRATION_GEN2.bat'],
                         ['watchdog',         (CROWN_SOURCE_BASE + 'VCM_APP/src/general/'),        'EVENT_MANAGER_INTEGRATION_GEN2.bat']
                      ],
                      #SWITCHBOARD
                      [
                         #'C' file            path where the file resides                          batch file to add if Hash mis-match
                         ['ag_common',       (CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen_API/'),     'SWITCH_BOARD_INTEGRATION_GEN2.bat'],
                         ['ag_input_router', (CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen_API/'),     'SWITCH_BOARD_INTEGRATION_GEN2.bat'],
                         ['ag_output_router',(CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen_API/'),     'SWITCH_BOARD_INTEGRATION_GEN2.bat'],
                         ['param',           (CROWN_SOURCE_BASE + 'VCM_APP/src/parameters/'),      'SWITCH_BOARD_INTEGRATION_GEN2.bat'],
                         ['switchboard',     (CROWN_SOURCE_BASE + 'CommonSource/switch_board/'),   'SWITCH_BOARD_INTEGRATION_GEN2.bat'] 
                      ],
                      #EXTERNAL_ALARM
                      [                         
                         ['external_alarm', (CROWN_SOURCE_BASE + 'VCM_APP/src/drivers/io/'),       'EXTERNAL_ALARM_INTEGRATION_GEN2.bat']                    
                      ],
                      #GYRO_DRIVER
                      [                         
                         ['gyro_driver',    (CROWN_SOURCE_BASE + 'VCM_APP/src/drivers/gyro/'),     'GYRO_DRIVER_INTEGRATION_GEN2.bat']                    
                      ],
                      #FEATURES_INTERFACE
                      [                         
                         ['features_interface', (CROWN_SOURCE_BASE + 'VCM_APP/src/interface/'),    'FEATURES_INTERFACE_INTEGRATION_GEN2.bat']                    
                      ],
                      #SUPV_INTERFACE
                      [                         
                         ['supv_interface', (CROWN_SOURCE_BASE + 'VCM_APP/src/general/'),          'SUPV_INTERFACE_INTEGRATION_GEN2.bat'],
                         ['features_interface', (CROWN_SOURCE_BASE + 'VCM_APP/src/interface/'),    'SUPV_INTERFACE_INTEGRATION_GEN2.bat'] 
                      ],
                      #IMM_INTERFACE
                      [                         
                         ['imm_interface', (CROWN_SOURCE_BASE + 'VCM_APP/src/general/'),           'IMM_INTERFACE_INTEGRATION_GEN2.bat']                    
                      ],
                      #OS_INTERFACE
                      [                         
                         ['OS_Interface', (CROWN_SOURCE_BASE + 'VCM_APP/src/general/'),            'OS_INTERFACE_INTEGRATION_GEN2.bat']                    
                      ],
                      #STEERING
                      [                          
                         ['AssistCmd',            (CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen/SIMu1/AssistCmd/'),                   'STEERING_INTEGRATION_GEN2.bat'],
                         ['Cmd_Pos_Integrator',   (CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen/SIMu1/Cmd_Pos_Integrator/'),          'STEERING_INTEGRATION_GEN2.bat'],
                         ['HmsSim_Steering',      (CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen/SIMu1/HmsSim_Steering/'),             'STEERING_INTEGRATION_GEN2.bat'],
                         ['HmsSim_SteeringBuild', (CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen/SIMu1/HmsSim_SteeringBuild_ert_rtw/'),'STEERING_INTEGRATION_GEN2.bat'],
                         ['PosCmd',               (CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen/SIMu1/PosCmd/'),                      'STEERING_INTEGRATION_GEN2.bat'],
                         ['PosCmd_data',          (CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen/SIMu1/PosCmd/'),                      'STEERING_INTEGRATION_GEN2.bat'],
                         ['SpdCmd',               (CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen/SIMu1/SpdCmd/'),                      'STEERING_INTEGRATION_GEN2.bat'],
                         ['SteeringSIM',          (CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen/SIMu1/SteeringSIM/'),                 'STEERING_INTEGRATION_GEN2.bat'],
                         ['SteeringSIMbuild',     (CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen/SIMu1/SteeringSIMbuild_ert_rtw/'),    'STEERING_INTEGRATION_GEN2.bat'],
                         ['StrMstrApp',           (CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen/SIMu1/StrMstrApp/'),                  'STEERING_INTEGRATION_GEN2.bat'],
                         ['StrMstrSup',           (CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen/SIMu1/StrMstrSup/'),                  'STEERING_INTEGRATION_GEN2.bat'],
                         ['StrMtrSpdLmt',         (CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen/SIMu1/StrMtrSpdLmt/'),                'STEERING_INTEGRATION_GEN2.bat'],
                         ['StrPosHndlSpdSense',   (CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen/SIMu1/StrPosHndlSpdSense/'),          'STEERING_INTEGRATION_GEN2.bat'],
                         ['StrPosWhAngLimiter',   (CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen/SIMu1/StrPosWhAngLimiter/'),          'STEERING_INTEGRATION_GEN2.bat'],
                         ['StrPos_SahNmbns',      (CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen/SIMu1/StrPos_SahNmbns/'),             'STEERING_INTEGRATION_GEN2.bat'],
                         ['StrPos_SetPntRateLmtr',(CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen/SIMu1/StrPos_SetPntRateLmtr/'),       'STEERING_INTEGRATION_GEN2.bat'],
                         ['TfdCurrentSpProfile',  (CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen/SIMu1/TfdCurrentSpProfile/'),         'STEERING_INTEGRATION_GEN2.bat'],
                         ['TfdOpMode',            (CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen/SIMu1/TfdOpMode/'),                   'STEERING_INTEGRATION_GEN2.bat'],
                         ['TfdOvrMode',           (CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen/SIMu1/TfdOvrMode/'),                  'STEERING_INTEGRATION_GEN2.bat'],
                         ['Til2DuScaling',        (CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen/SIMu1/Til2DuScaling/'),               'STEERING_INTEGRATION_GEN2.bat'],
                         ['TrxSpd_StrMtrSpdLmt',  (CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen/SIMu1/TrxSpd_StrMtrSpdLmt/'),         'STEERING_INTEGRATION_GEN2.bat'],
                         ['WhAngErr_StrMtrSpdLmt',(CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen/SIMu1/WhAngErr_StrMtrSpdLmt/'),       'STEERING_INTEGRATION_GEN2.bat'],
                         ['WhAng_StrMtrSpdLmt',   (CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen/SIMu1/WhAng_StrMtrSpdLmt/'),          'STEERING_INTEGRATION_GEN2.bat']
                      ],
                      #IP_INTERFACE
                      [                         
                         ['IP_Interface',         (CROWN_SOURCE_BASE + 'VCM_APP/src/general/'),                                   'IP_INTERFACE_INTEGRATION_GEN2.bat']                    
                      ],
                      #MANIFEST_MANAGER
                      [                         
                         ['Manifest_mgr',         (CROWN_SOURCE_BASE + 'VCM_APP/src/interface/'),                                 'MANIFEST_MANAGER_INTEGRATION_GEN2.bat']                    
                      ],
                      #TRACTION
                      [                         
                         ['GetSpeed',              (CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen/SIMu1/GetSpeed/'),                   'TRACTION_INTEGRATION_GEN2.bat'],
                         ['OnTrac',                (CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen/SIMu1/OnTrac/'),                     'TRACTION_INTEGRATION_GEN2.bat'],
                         ['PIControlLoop',         (CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen/SIMu1/PIControlLoop/'),              'TRACTION_INTEGRATION_GEN2.bat'],
                         ['ParameterDiagnostics',  (CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen/SIMu1/ParameterDiagnostics/'),       'TRACTION_INTEGRATION_GEN2.bat'],
                         ['ThrottleSpdCmd',        (CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen/SIMu1/ThrottleSpdCmd/'),             'TRACTION_INTEGRATION_GEN2.bat'],
                         ['TorqueLimiter',         (CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen/SIMu1/TorqueLimiter/'),              'TRACTION_INTEGRATION_GEN2.bat'],
                         ['TractionSIM',           (CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen/SIMu1/TractionSIM/'),                'TRACTION_INTEGRATION_GEN2.bat'],
                         ['TractionSIMbuild',      (CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen/SIMu1/TractionSIMbuild_ert_rtw/'),   'TRACTION_INTEGRATION_GEN2.bat'],
                         ['Traction_State_Machine',(CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen/SIMu1/Traction_State_Machine/'),     'TRACTION_INTEGRATION_GEN2.bat'],
                         ['accel_decel_slew',      (CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen/SIMu1/accel_decel_slew/'),           'TRACTION_INTEGRATION_GEN2.bat']             
                      ],
                      #FRAM_DRIVER                      
                      [                         
                         ['fram_driver',           (CROWN_SOURCE_BASE + 'VCM_APP/src/drivers/fram/'),                             'FRAM_DRIVER_INTEGRATION_GEN2.bat'],
                         ['NvMemory',              (CROWN_SOURCE_BASE + 'VCM_APP/src/nvstorage/fram/'),                           'FRAM_DRIVER_INTEGRATION_GEN2.bat']
                      ],
                      #UTILITIES                      
                      [                         
                         ['app_info',              (CROWN_SOURCE_BASE + 'VCM_APP/src/utilities/'),                                'UTILITIES_INTEGRATION_GEN2.bat'],
                         ['crc',                   (CROWN_SOURCE_BASE + 'VCM_APP/src/utilities/'),                                'UTILITIES_INTEGRATION_GEN2.bat'],
                         ['debug_support',         (CROWN_SOURCE_BASE + 'VCM_APP/src/utilities/'),                                'UTILITIES_INTEGRATION_GEN2.bat'],
                         ['instrument',            (CROWN_SOURCE_BASE + 'VCM_APP/src/utilities/'),                                'UTILITIES_INTEGRATION_GEN2.bat'],
                         ['stopwatch',             (CROWN_SOURCE_BASE + 'VCM_APP/src/utilities/'),                                'UTILITIES_INTEGRATION_GEN2.bat'],
                         ['util',                  (CROWN_SOURCE_BASE + 'VCM_APP/src/utilities/'),                                'UTILITIES_INTEGRATION_GEN2.bat']
                      ],
                      #DIAGNOSTICS                      
                      [                         
                         ['devrep_external_event_handler', (CROWN_SOURCE_BASE + 'VCM_APP/src/diagnostics/'),                      'DIAGNOSTICS_INTEGRATION_GEN2.bat'],
                         ['devrep_hdm',            (CROWN_SOURCE_BASE + 'VCM_APP/src/diagnostics/'),                              'DIAGNOSTICS_INTEGRATION_GEN2.bat'],
                         ['devrep_iom',            (CROWN_SOURCE_BASE + 'VCM_APP/src/diagnostics/'),                              'DIAGNOSTICS_INTEGRATION_GEN2.bat'],
                         ['devrep_scm',            (CROWN_SOURCE_BASE + 'VCM_APP/src/diagnostics/'),                              'DIAGNOSTICS_INTEGRATION_GEN2.bat'],
                         ['devrep_tcm_hcm',        (CROWN_SOURCE_BASE + 'VCM_APP/src/diagnostics/'),                              'DIAGNOSTICS_INTEGRATION_GEN2.bat'],
                         ['devrep_vcm_model',      (CROWN_SOURCE_BASE + 'VCM_APP/src/diagnostics/'),                              'DIAGNOSTICS_INTEGRATION_GEN2.bat'],
                         ['devrep_vcm_supv',       (CROWN_SOURCE_BASE + 'VCM_APP/src/diagnostics/'),                              'DIAGNOSTICS_INTEGRATION_GEN2.bat']
                      ]
                  ]

     
#while(1):
  # selection will possible use a text file to determine if it need to be re-hashed after a base line was ran on target once
  #selection = str(input("Type '1' to calculate and save hash, '2' to see if saved hash and current has match: "))
  selection = SelectReadWrite
  # Hash Writing only needed once in theory 
  if selection == '1':  
      i = 0
      for element in all_regressions:          
          j = 0
          for line in all_regressions[i]:
              
              # assingment of the class to this variable with correspond 'C' file and output MD5 .txt file
              regression = MD5Hash((all_regressions[i][j][1] + all_regressions[i][j][0] + '.c'), (md5_output_path + all_regressions[i][j][0] + MD5HASHtxt) , None, None)                    
              regression.MD5_Hash_Calc()
              regression.WriteHash()
              j = j + 1
          i = i + 1
                                               
  # Hash Reading this will be mostly used                               
  if selection == '2':
      CurrentBatRun = None
      LastBatRun = None        
      fFirstrun = True
      fChangedfileWrite = False
      fMissingCfileWrite = False
      fTestnameToRunWrite = False
      i = 0
        
      for element in all_regressions:          
          j = 0
          for line in all_regressions[i]:
              # assingment of the class to this variable with correspond 'C' file and output MD5 .txt file
              try:
                  regression = MD5Hash((all_regressions[i][j][1] + all_regressions[i][j][0] + '.c'), (md5_output_path + all_regressions[i][j][0] + MD5HASHtxt) , None, None)
          
                  # calculate and read saved Hashes 
                  CurrentHash = regression.MD5_Hash_Calc()
                  SavedHash = regression.ReadHash()
              except:
                  # Allow missing .C files list to be written
                  print("This file missing", all_regressions[i][j][0])
                  fMissingCfileWrite = True
                  
                  # append the list of missing .C files and a new line escape sequence 
                  Missing_files_list.append(all_regressions[i][j][0])
                  Missing_files_list.append('\n')
                   

              # is the current and saved hash value the same the remove the test from the overall list?
              if CurrentHash == SavedHash:
                  print("Current and saved Hashes match", all_regressions[i][j][0])
                             
              else:
                  # Allow what files hashes have changed list to write 
                  print("Current and saved Hashes Do Not match in:", all_regressions[i][j][0])
                  fChangedfileWrite = True 
                 
                  # Append and place to a new line escape sequence 
                  changed_files_list.append(all_regressions[i][j][0])
                  changed_files_list.append('\n')
                  
                  if fFirstrun == True:
                      # initialy append this list with first value 
                      fFirstrun = False
                      
                      # load initially the value from first C file line from this list add new line escape sequence and allow what test to run list to be written
                      fTestnameToRunWrite = True
                      LastBatRun = all_regressions[i][j][2]
                      Testname.append(LastBatRun)
                      Testname.append('\n')
                      
                  else:
                      CurrentBatRun = all_regressions[i][j][2]                        
                      if CurrentBatRun != LastBatRun:
                        
                          #if there is a difference then append Testname list add new line escape sequence                            
                          Testname.append(CurrentBatRun)
                          Testname.append('\n')
                          
                      # update the last bat file for next run through      
                      LastBatRun = CurrentBatRun
                      
              j = j + 1                
          i = i + 1
          
      
      # Store fault data in a text files if flagged to 
      if  fChangedfileWrite == True:  
          f = open('Changed_files_list.txt', "w")
          f.writelines(changed_files_list)
          f.close()

      if fTestnameToRunWrite == True: 
          f = open('Testname.txt', "w")
          f.writelines(Testname)
          f.close()

          # Need to strip out '\n' so test times dont report as a test to run
          for line in Testname:
             # Strip whitespace, should leave nothing if empty line was just "\n"
              if not line.strip():
                  continue
             # We got something, save it
              else:
                  TestnameStripped.append(line)
          Testname = TestnameStripped  

      if fMissingCfileWrite == True:
          f = open('Missing_files_list.txt', "w")
          f.writelines(Missing_files_list)
          f.close()

          # Send email alert of missing files 
          email_FoldersMissing(os.getcwd())
      
      print("\n")       
      print("List of source files with non-matching hashes",changed_files_list)
      print("List of in-correct hashes, these Regressions to be run", Testname)
      print("List of missing .C files",Missing_files_list)
      print("\n")

      # The tests needed if need run
      return Testname

      
if __name__ == '__main__':
  MD5HashDriver()
 
