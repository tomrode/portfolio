/**********************************************************************
* File: f2812_flash.cmd -- Linker command file for Boot to Flash bootmode.
* History: 09/08/03 - original (based on DSP28 header files v1.00, D. Alter)
**********************************************************************/


/**************************************************************/
/* Link all user defined sections                             */
/**************************************************************/
SECTIONS
{

/*** User Defined Sections ***/
   codestart         : > BEGIN_FLASH,      PAGE = 0        /* Used by file CodeStartBranch.asm */
   .reset            : > BEGIN_FLASH,      PAGE = 0, TYPE = DSECT
                                          /* Section ramfuncs used by InitFlash() in SysCtrl.c */
   ramfuncs          :   LOAD = FLASH_BCDEFG,  PAGE = 0 /* can be ROM */
                         RUN = L1SARAM,    PAGE = 1        /* must be CSM secured RAM */
                         LOAD_START(_ramfuncs_loadstart),
                         LOAD_END(_ramfuncs_loadend),
                         RUN_START(_ramfuncs_runstart)

   parameterflag           : > PARAMFLAG        PAGE = 0
   parameters              : > FLASH_H          PAGE = 0
   {
/*      param.obj */
   }
   /* define the information block for the application */

/*   trademark           : > TRADEMARK */
/*   partnumber          : > PARTNUM */
/*   fillmap             : > FILLMAP */
/*   memorymap           : > MEMMAP */
/*   compatableboards    : > COMPATABLE */
/*   appcrc              : > APPCRC */
/*   appcs               : > APPCS */

   gFpgafile           : > ZONE0               PAGE = 1

   extflash            : > ZONE7       PAGE = 0

   /* .reset is a standard section used by the compiler.  It contains the */
   /* the address of the start of _c_int00 for C Code.   /*
   /* When using the boot ROM this section and the CPU vector */
   /* table is not needed.  Thus the default type is set here to  */
   /* DSECT  */
   vectors             : > VECTORS     PAGE = 0, TYPE = DSECT

   /* Allocate ADC_cal function (pre-programmed by factory into TI reserved memory) */
   .adc_cal     : load = ADC_CAL,   PAGE = 0, TYPE = NOLOAD

}

/******************* end of file ************************/
