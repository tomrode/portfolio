set target=%1
set tcf_filename=%2
set ccs_version=%3

if "%ccs_version%"=="CCS_V4" goto :CCS_V4
if "%ccs_version%"=="CCS_V5" goto :CCS_V5
if "%ccs_version%"=="CCS_V6" goto :CCS_V6

%BIOS_INSTALL_DIR%\xdctools\tconf -b -Dconfig.importPath=%BIOS_INSTALL_DIR%/packages %TCF_FILENAME%.tcf

if "%target%"=="64XX" goto :64xx_target
if "%target%"=="64X+" goto :64x+_target
if "%target%"=="28XX" goto :28xx_target
if "%target%"=="2812" goto :28xx_target
if "%target%"=="2833" goto :2833_target
if "%target%"=="6713" goto :67xx_target

:28xx_target
rem build [<tcf_filename>cfg.s28]
"%VCAST_CCS_INSTALL_DIR%\C2000\cgtools\bin\cl2000" -g -d"_DEBUG" -d"LARGE_MODEL" -ml -v28 -i%VCAST_CCS_INSTALL_DIR%/C2000/xdais/include -i%BIOS_INSTALL_DIR%/packages/ti/bios/include -i%BIOS_INSTALL_DIR%/packages/ti/rtdx/include/c2000 -i%VCAST_CCS_INSTALL_DIR%/C2000/cgtools/include "%TCF_FILENAME%cfg.s28" 

rem build [<tcf_filename>cfg_c.c]
"%VCAST_CCS_INSTALL_DIR%\C2000\cgtools\bin\cl2000" -g -d"_DEBUG" -d"LARGE_MODEL" -ml -v28 -i%VCAST_CCS_INSTALL_DIR%/C2000/xdais/include -i%BIOS_INSTALL_DIR%/packages/ti/bios/include -i%BIOS_INSTALL_DIR%/packages/ti/rtdx/include/c2000 -i%VCAST_CCS_INSTALL_DIR%/C2000/cgtools/include "%TCF_FILENAME%cfg_c.c" 
goto end

:2833_target
rem build [<tcf_filename>cfg.s28]
"%VCAST_CCS_INSTALL_DIR%\C2000\cgtools\bin\cl2000" -g -d"_DEBUG" -d"LARGE_MODEL" -ml -v28 --float_support=fpu32 -i%VCAST_CCS_INSTALL_DIR%/C2000/xdais/include -i%BIOS_INSTALL_DIR%/packages/ti/bios/include -i%BIOS_INSTALL_DIR%/packages/ti/rtdx/include/c2000 -i%VCAST_CCS_INSTALL_DIR%/C2000/cgtools/include "%TCF_FILENAME%cfg.s28" 

rem build [<tcf_filename>cfg_c.c]
"%VCAST_CCS_INSTALL_DIR%\C2000\cgtools\bin\cl2000" -g -d"_DEBUG" -d"LARGE_MODEL" -ml -v28 --float_support=fpu32 -i%VCAST_CCS_INSTALL_DIR%/C2000/xdais/include -i%BIOS_INSTALL_DIR%/packages/ti/bios/include -i%BIOS_INSTALL_DIR%/packages/ti/rtdx/include/c2000 -i%VCAST_CCS_INSTALL_DIR%/C2000/cgtools/include "%TCF_FILENAME%cfg_c.c" 
goto end

:64xx_target
rem build [<tcf_filename>cfg.s62]
  "%VCAST_CCS_INSTALL_DIR%\tools\compiler\c6000\bin\cl6x" -g -i"." -d"_DEBUG" -mv6400 -i"%BIOS_INSTALL_DIR%/packages/ti/bios/include" -i"%BIOS_INSTALL_DIR%/packages/ti/rtdx/include/c6000" -i"%VCAST_CCS_INSTALL_DIR%/C6000/cgtools/include" "%TCF_FILENAME%cfg.s62"
 
rem build [<tcf_filename>cfg_c.c]
  "%VCAST_CCS_INSTALL_DIR%\tools\compiler\c6000\bin\cl6x" -g -i"." -d"_DEBUG" -mv6400 -i"%BIOS_INSTALL_DIR%/packages/ti/bios/include" -i"%BIOS_INSTALL_DIR%/packages/ti/rtdx/include/c6000" -i"%VCAST_CCS_INSTALL_DIR%/C6000/cgtools/include" "%TCF_FILENAME%cfg_c.c"
goto end

:64x+_target
rem build [<tcf_filename>cfg.s62]
  "%VCAST_CCS_INSTALL_DIR%\C6000\cgtools\bin\cl6x" -g -i"." -d"_DEBUG" -mv6400+ -i"%VCAST_CCS_INSTALL_DIR%/C6000/csl/include" -i"%VCAST_CCS_INSTALL_DIR%/C6000/xdais/include" -i"%BIOS_INSTALL_DIR%/packages/ti/bios/include" -i"%BIOS_INSTALL_DIR%/packages/ti/rtdx/include/c6000" -i"%VCAST_CCS_INSTALL_DIR%/C6000/cgtools/include" "%TCF_FILENAME%cfg.s62"
 
rem build [<tcf_filename>cfg_c.c]
  "%VCAST_CCS_INSTALL_DIR%\C6000\cgtools\bin\cl6x" -g -i"." -d"_DEBUG" -mv6400+ -i"%VCAST_CCS_INSTALL_DIR%/C6000/csl/include" -i"%VCAST_CCS_INSTALL_DIR%/C6000/xdais/include" -i"%BIOS_INSTALL_DIR%/packages/ti/bios/include" -i"%BIOS_INSTALL_DIR%/packages/ti/rtdx/include/c6000" -i"%VCAST_CCS_INSTALL_DIR%/C6000/cgtools/include" "%TCF_FILENAME%cfg_c.c"
goto end

:67xx_target
rem build [<tcf_filename>cfg.s62]
  "%VCAST_CCS_INSTALL_DIR%\C6000\cgtools\bin\cl6x" -g -i"." -d"_DEBUG" -mv6710 -i"%VCAST_CCS_INSTALL_DIR%/C6000/csl/include" -i"%VCAST_CCS_INSTALL_DIR%/C6000/xdais/include" -i"%BIOS_INSTALL_DIR%/packages/ti/bios/include" -i"%BIOS_INSTALL_DIR%/packages/ti/rtdx/include/c6000" -i"%VCAST_CCS_INSTALL_DIR%/C6000/cgtools/include" "%TCF_FILENAME%cfg.s62"
 
rem build [<tcf_filename>cfg_c.c]
  "%VCAST_CCS_INSTALL_DIR%\C6000\cgtools\bin\cl6x" -g -i"." -d"_DEBUG" -mv6710 -i"%VCAST_CCS_INSTALL_DIR%/C6000/csl/include" -i"%VCAST_CCS_INSTALL_DIR%/C6000/xdais/include" -i"%BIOS_INSTALL_DIR%/packages/ti/bios/include" -i"%BIOS_INSTALL_DIR%/packages/ti/rtdx/include/c6000" -i"%VCAST_CCS_INSTALL_DIR%/C6000/cgtools/include" "%TCF_FILENAME%cfg_c.c"
goto end


REM ========================================================================================================
:CCS_V4

REM %BIOS_INSTALL_DIR%\xdctools\tconf -b -Dconfig.importPath=%BIOS_INSTALL_DIR%/packages %TCF_FILENAME%.tcf
REM set the path to tconf for ccs v4 (C:\Program Files\Texas Instruments\xdctools_3_15_02_62)
tconf -b -Dconfig.importPath="%BIOS_INSTALL_DIR%/packages" "%TCF_FILENAME%.tcf"

if "%target%"=="64XX" goto :v4_64xx_target
if "%target%"=="55XX" goto :v4_55xx_target
if "%target%"=="28XX" goto :v4_28xx_target
if "%target%"=="2812" goto :v4_28xx_target

:v4_28xx_target
rem build [<tcf_filename>cfg.s28]
cl2000 --silicon_version=28 -g -d"_DEBUG" -i"%VCAST_CCS_INSTALL_DIR%/tools/compiler/c2000/include" -i"%BIOS_INSTALL_DIR%/packages/ti/bios/include" -i"%BIOS_INSTALL_DIR%/packages/ti/rtdx/include/c2000" -sat_reassoc=off --large_memory_model --fp_reassoc=off  --preproc_with_compile --preproc_dependency="%TCF_FILENAME%cfg.pp" "%TCF_FILENAME%cfg.s28" 

rem build [<tcf_filename>cfg_c.c]
cl2000 --silicon_version=28 -g -d"_DEBUG" -i"%VCAST_CCS_INSTALL_DIR%/tools/compiler/c2000/include" -i"%BIOS_INSTALL_DIR%/packages/ti/bios/include" -i"%BIOS_INSTALL_DIR%/packages/ti/rtdx/include/c2000" -sat_reassoc=off --large_memory_model --fp_reassoc=off  --preproc_with_compile --preproc_dependency="%TCF_FILENAME%cfg_c.pp" "%TCF_FILENAME%cfg_c.c" 
goto end

:v4_64xx_target
rem build [<tcf_filename>cfg.s62]
cl6x -mv6400 -g --fp_mode=strict -i"." -i"%VCAST_CCS_INSTALL_DIR%\tools\compiler\c6000\include" -i"%BIOS_INSTALL_DIR%\packages\ti\bios\include" -i"%BIOS_INSTALL_DIR%\packages\ti\rtdx\include\c6000" --sat_reassoc=off --fp_reassoc=on --mem_model:const=data --mem_model:data=far_aggregates  --preproc_with_compile --preproc_dependency="%TCF_FILENAME%cfg.pp" "%TCF_FILENAME%cfg.s62"
 
rem build [<tcf_filename>cfg_c.c]
cl6x -mv6400 -g --fp_mode=strict -i"." -i"%VCAST_CCS_INSTALL_DIR%\tools\compiler\c6000\include" -i"%BIOS_INSTALL_DIR%\packages\ti\bios\include" -i"%BIOS_INSTALL_DIR%\packages\ti\rtdx\include\c6000" --sat_reassoc=off --fp_reassoc=on --mem_model:const=data --mem_model:data=far_aggregates  --preproc_with_compile --preproc_dependency="%TCF_FILENAME%cfg_c.pp" "%TCF_FILENAME%cfg_c.c"
goto end

:v4_55xx_target
rem build [<tcf_filename>cfg.s55]
cl55 -g -i"." -i"%VCAST_CCS_INSTALL_DIR%\tools\compiler\c5500\include" -i"%BIOS_INSTALL_DIR%\packages\ti\bios\include" -i"%BIOS_INSTALL_DIR%\packages\ti\rtdx\include\c5500" --sat_reassoc=off --large_memory_model --ptrdiff_size=16 --fp_reassoc=off --preproc_with_compile --preproc_dependency="%TCF_FILENAME%cfg.pp" "%TCF_FILENAME%cfg.s55"
 
rem build [<tcf_filename>cfg_c.c]
cl55 -g -i"." -i"%VCAST_CCS_INSTALL_DIR%\tools\compiler\c5500\include" -i"%BIOS_INSTALL_DIR%\packages\ti\bios\include" -i"%BIOS_INSTALL_DIR%\packages\ti\rtdx\include\c5500" --sat_reassoc=off --large_memory_model --ptrdiff_size=16 --fp_reassoc=off --preproc_with_compile --preproc_dependency="%TCF_FILENAME%cfg_c.pp" "%TCF_FILENAME%cfg_c.c"
goto end

REM ========================================================================================================
:CCS_V5

REM %BIOS_INSTALL_DIR%\xdctools\tconf -b -Dconfig.importPath=%BIOS_INSTALL_DIR%/packages %TCF_FILENAME%.tcf
REM set XDCROOT, (path to tconf) for ccs v5 (set XDCROOT=C:\CCS_v5.1\xdctools_3_22_04_46)
REM set BIOS_INSTALL_DIR for ccs v5 (set BIOS_INSTALL_DIR=C:\CCS_v5.1\bios_5_41_11_38)
tconf -b -Dconfig.importPath="%BIOS_INSTALL_DIR%/packages" "%TCF_FILENAME%.tcf"

if "%target%"=="64XX" goto :v5_64xx_target
if "%target%"=="6416" goto :v5_64xx_target
if "%target%"=="55XX" goto :v5_55xx_target
if "%target%"=="28XX" goto :v5_28xx_target
if "%target%"=="2812" goto :v5_28xx_target
if "%target%"=="2833" goto :v5_2833_target

:v5_2833_target
rem build [<tcf_filename>cfg.s28]
cl2000 -v28  --float_support=fpu32 --silicon_version=28 -g -d"_DEBUG" -i"%VCAST_CCS_COMPILER_INSTALL_DIR%\include" -i"%BIOS_INSTALL_DIR%/packages/ti/bios/include" -i"%BIOS_INSTALL_DIR%/packages/ti/rtdx/include/c2000" -sat_reassoc=off --large_memory_model --fp_reassoc=off  --preproc_with_compile --preproc_dependency="%TCF_FILENAME%cfg.pp" "%TCF_FILENAME%cfg.s28" 

rem build [<tcf_filename>cfg_c.c]
cl2000 -v28  --float_support=fpu32 --silicon_version=28 -g -d"_DEBUG" -i"%VCAST_CCS_COMPILER_INSTALL_DIR%\include" -i"%BIOS_INSTALL_DIR%/packages/ti/bios/include" -i"%BIOS_INSTALL_DIR%/packages/ti/rtdx/include/c2000" -sat_reassoc=off --large_memory_model --fp_reassoc=off  --preproc_with_compile --preproc_dependency="%TCF_FILENAME%cfg_c.pp" "%TCF_FILENAME%cfg_c.c" 
goto end

:v5_28xx_target
rem build [<tcf_filename>cfg.s28]
cl2000 --silicon_version=28 -g -d"_DEBUG" -i"%VCAST_CCS_INSTALL_DIR%/tools/compiler/c2000/include" -i"%BIOS_INSTALL_DIR%/packages/ti/bios/include" -i"%BIOS_INSTALL_DIR%/packages/ti/rtdx/include/c2000" -sat_reassoc=off --large_memory_model --fp_reassoc=off  --preproc_with_compile --preproc_dependency="%TCF_FILENAME%cfg.pp" "%TCF_FILENAME%cfg.s28" 

rem build [<tcf_filename>cfg_c.c]
cl2000 --silicon_version=28 -g -d"_DEBUG" -i"%VCAST_CCS_INSTALL_DIR%/tools/compiler/c2000/include" -i"%BIOS_INSTALL_DIR%/packages/ti/bios/include" -i"%BIOS_INSTALL_DIR%/packages/ti/rtdx/include/c2000" -sat_reassoc=off --large_memory_model --fp_reassoc=off  --preproc_with_compile --preproc_dependency="%TCF_FILENAME%cfg_c.pp" "%TCF_FILENAME%cfg_c.c" 
goto end

:v5_64xx_target
rem build [<tcf_filename>cfg.s62]
cl6x -mv6400 -g --fp_mode=strict -i"." -i"%VCAST_CCS_INSTALL_DIR%\tools\compiler\c6000\include" -i"%BIOS_INSTALL_DIR%\packages\ti\bios\include" -i"%BIOS_INSTALL_DIR%\packages\ti\rtdx\include\c6000" --sat_reassoc=off --fp_reassoc=on --mem_model:const=data --mem_model:data=far_aggregates  --preproc_with_compile --preproc_dependency="%TCF_FILENAME%cfg.pp" "%TCF_FILENAME%cfg.s62"
 
rem build [<tcf_filename>cfg_c.c]
cl6x -mv6400 -g --fp_mode=strict -i"." -i"%VCAST_CCS_INSTALL_DIR%\tools\compiler\c6000\include" -i"%BIOS_INSTALL_DIR%\packages\ti\bios\include" -i"%BIOS_INSTALL_DIR%\packages\ti\rtdx\include\c6000" --sat_reassoc=off --fp_reassoc=on --mem_model:const=data --mem_model:data=far_aggregates  --preproc_with_compile --preproc_dependency="%TCF_FILENAME%cfg_c.pp" "%TCF_FILENAME%cfg_c.c"
goto end

:v5_55xx_target
rem build [<tcf_filename>cfg.s55]
cl55 -g -i"." -i"%VCAST_CCS_INSTALL_DIR%\tools\compiler\c5500\include" -i"%BIOS_INSTALL_DIR%\packages\ti\bios\include" -i"%BIOS_INSTALL_DIR%\packages\ti\rtdx\include\c5500" --sat_reassoc=off --large_memory_model --ptrdiff_size=16 --fp_reassoc=off --preproc_with_compile --preproc_dependency="%TCF_FILENAME%cfg.pp" "%TCF_FILENAME%cfg.s55"
 
rem build [<tcf_filename>cfg_c.c]
cl55 -g -i"." -i"%VCAST_CCS_INSTALL_DIR%\tools\compiler\c5500\include" -i"%BIOS_INSTALL_DIR%\packages\ti\bios\include" -i"%BIOS_INSTALL_DIR%\packages\ti\rtdx\include\c5500" --sat_reassoc=off --large_memory_model --ptrdiff_size=16 --fp_reassoc=off --preproc_with_compile --preproc_dependency="%TCF_FILENAME%cfg_c.pp" "%TCF_FILENAME%cfg_c.c"
goto end

:CCS_V6

REM %BIOS_INSTALL_DIR%\xdctools\tconf -b -Dconfig.importPath=%BIOS_INSTALL_DIR%/packages %TCF_FILENAME%.tcf
REM set XDCROOT, (path to tconf) for ccs v5 (set XDCROOT=C:\CCS_v5.1\xdctools_3_22_04_46)
REM set BIOS_INSTALL_DIR for ccs v5 (set BIOS_INSTALL_DIR=C:\CCS_v5.1\bios_5_41_11_38)
tconf -b -Dconfig.importPath="%BIOS_INSTALL_DIR%/packages" "%TCF_FILENAME%.tcf"

if "%target%"=="64XX" goto :v6_64xx_target
if "%target%"=="6416" goto :v6_64xx_target
if "%target%"=="55XX" goto :v6_55xx_target
if "%target%"=="28XX" goto :v6_28xx_target
if "%target%"=="2812" goto :v6_28xx_target
if "%target%"=="2833" goto :v6_2833_target

:v6_2833_target
rem build [<tcf_filename>cfg.s28]
cl2000 -v28  --float_support=fpu32 --silicon_version=28 -g -d"_DEBUG" -i"%VCAST_CCS_COMPILER_INSTALL_DIR%\include" -i"%BIOS_INSTALL_DIR%/packages/ti/bios/include" -sat_reassoc=off --large_memory_model --fp_reassoc=off  --preproc_with_compile --preproc_dependency="%TCF_FILENAME%cfg.pp" "%TCF_FILENAME%cfg.s28" 

rem build [<tcf_filename>cfg_c.c]
cl2000 -v28  --float_support=fpu32 --silicon_version=28 -g -d"_DEBUG" -i"%VCAST_CCS_COMPILER_INSTALL_DIR%\include" -i"%BIOS_INSTALL_DIR%/packages/ti/bios/include" -sat_reassoc=off --large_memory_model --fp_reassoc=off  --preproc_with_compile --preproc_dependency="%TCF_FILENAME%cfg_c.pp" "%TCF_FILENAME%cfg_c.c" 
goto end

:v6_28xx_target
rem build [<tcf_filename>cfg.s28]
cl2000 --silicon_version=28 -g -d"_DEBUG" -i"%VCAST_CCS_INSTALL_DIR%/tools/compiler/c2000/include" -i"%BIOS_INSTALL_DIR%/packages/ti/bios/include" -i"%BIOS_INSTALL_DIR%/packages/ti/rtdx/include/c2000" -sat_reassoc=off --large_memory_model --fp_reassoc=off  --preproc_with_compile --preproc_dependency="%TCF_FILENAME%cfg.pp" "%TCF_FILENAME%cfg.s28" 

rem build [<tcf_filename>cfg_c.c]
cl2000 --silicon_version=28 -g -d"_DEBUG" -i"%VCAST_CCS_INSTALL_DIR%/tools/compiler/c2000/include" -i"%BIOS_INSTALL_DIR%/packages/ti/bios/include" -i"%BIOS_INSTALL_DIR%/packages/ti/rtdx/include/c2000" -sat_reassoc=off --large_memory_model --fp_reassoc=off  --preproc_with_compile --preproc_dependency="%TCF_FILENAME%cfg_c.pp" "%TCF_FILENAME%cfg_c.c" 
goto end

:v6_64xx_target
rem build [<tcf_filename>cfg.s62]
cl6x -mv6400 -g --fp_mode=strict -i"." -i"%VCAST_CCS_INSTALL_DIR%\tools\compiler\c6000\include" -i"%BIOS_INSTALL_DIR%\packages\ti\bios\include" -i"%BIOS_INSTALL_DIR%\packages\ti\rtdx\include\c6000" --sat_reassoc=off --fp_reassoc=on --mem_model:const=data --mem_model:data=far_aggregates  --preproc_with_compile --preproc_dependency="%TCF_FILENAME%cfg.pp" "%TCF_FILENAME%cfg.s62"
 
rem build [<tcf_filename>cfg_c.c]
cl6x -mv6400 -g --fp_mode=strict -i"." -i"%VCAST_CCS_INSTALL_DIR%\tools\compiler\c6000\include" -i"%BIOS_INSTALL_DIR%\packages\ti\bios\include" -i"%BIOS_INSTALL_DIR%\packages\ti\rtdx\include\c6000" --sat_reassoc=off --fp_reassoc=on --mem_model:const=data --mem_model:data=far_aggregates  --preproc_with_compile --preproc_dependency="%TCF_FILENAME%cfg_c.pp" "%TCF_FILENAME%cfg_c.c"
goto end

:v6_55xx_target
rem build [<tcf_filename>cfg.s55]
cl55 -g -i"." -i"%VCAST_CCS_INSTALL_DIR%\tools\compiler\c5500\include" -i"%BIOS_INSTALL_DIR%\packages\ti\bios\include" -i"%BIOS_INSTALL_DIR%\packages\ti\rtdx\include\c5500" --sat_reassoc=off --large_memory_model --ptrdiff_size=16 --fp_reassoc=off --preproc_with_compile --preproc_dependency="%TCF_FILENAME%cfg.pp" "%TCF_FILENAME%cfg.s55"
 
rem build [<tcf_filename>cfg_c.c]
cl55 -g -i"." -i"%VCAST_CCS_INSTALL_DIR%\tools\compiler\c5500\include" -i"%BIOS_INSTALL_DIR%\packages\ti\bios\include" -i"%BIOS_INSTALL_DIR%\packages\ti\rtdx\include\c5500" --sat_reassoc=off --large_memory_model --ptrdiff_size=16 --fp_reassoc=off --preproc_with_compile --preproc_dependency="%TCF_FILENAME%cfg_c.pp" "%TCF_FILENAME%cfg_c.c"
goto end



:end
