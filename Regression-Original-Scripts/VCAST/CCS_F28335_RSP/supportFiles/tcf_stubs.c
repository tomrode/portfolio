extern void *RamfuncsRunStart, *RamfuncsLoadStart, *RamfuncsLoadEnd;

unsigned long ramfuncs_size = 0;

void SixteenMsTick                  (void) {}     
void Task_16ms                      (void) {}     
void UserInit(void)
{
#if 0
    /** - Section ramfuncs contains user defined code that runs
     *   from CSM secured RAM */
	 
    ramfuncs_size = &RamfuncsLoadEnd - &RamfuncsLoadStart;
		
    memcpy( &RamfuncsRunStart,
            &RamfuncsLoadStart,
            ramfuncs_size * 2);
#endif

}

void gv1_8_RST_ISR                  (void) {}     
void gvMSCOMM_RxMessageComplete_ISR (void) {}     
void gvMSCOMM_RxThreadEntry         (void) {}     
void gvMSCOMM_TxMessageComplete_ISR (void) {}     
void gvMSCOMM_TxThreadEntry         (void) {}     
void gvSW15_EdgeDetect_ISR          (void) {}     
void v5msTask                       (void) {}     
void vErrorISR                      (void) {}     
void vLossOfPower                   (void) {}     
void vNullTaskProcesses             (void) {}     
void vPowerGoingToDrop              (void) {}     
void vWatchdogGuarding              (void) {} 
void gvDEM_ReportThreadEntry		(void) {}    
void vXCP_ThreadEntry				(void) {}    

#ifdef NEED_ADCINT_ISR
//void ADCINT_ISR (void) {}
#endif
