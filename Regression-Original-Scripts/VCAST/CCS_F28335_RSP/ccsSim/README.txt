The files in this folder must be copied into your CCS installation folder in order to get the VectorCast simulator working for the VCM Supv project.

Copy the files to the following locations:

1) configurations/*
  --> <TI_dir>/ccs_base/common/targetdb/configurations/

2) simulation/*
  --> <TI_dir>/ccs_base/siumulation

3) tisim_connection.xml
  --> <TI_dir>/ccs_base/common/targetdb/connections/

4) tisim_isa_f283xx_unsup.xml
  --> <TI_dir>/ccs_base/common/targetdb/drivers/