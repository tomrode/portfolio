;; ARMulator configuration file type 3
;; - armulate.dsc -
;; Copyright (c) 2002 ARM Limited. All Rights Reserved.

;; RCS $Revision: 1.1.2.1 $
;; Checkin $Date: 2002/02/22 17:50:54 $
;; Revising $Author: dsinclai $
 
;;
;; This is the configuration file for ARMulator 
;;
ARMULATE_VERSION=120

; For the moment we assume that if no clock speed has been set on the
; command-line, the user wishes to use a wall-clock for timing
#if !CPUSPEED
Clock=Real
#endif


;; This line controls whether (some) models give more useful descriptions
;; of what they are on startup, and during running.
;Verbose=False

;; To enable faster watchpoints, set "WatchpointsEnabled"
WatchpointsEnabled=False


;; *****************************************************************
;; ARMulator Peripheral Models
;; Central list of peripherals
;; Use this list to enable/disable peripherals
;; *****************************************************************
;; To enable a peripheral change the rhs to TRUE
;; To disable a peripheral change the rhs to FALSE

;This is False by default.
WDogEnabled=False

; Note that DCC is enabled by default if running on a processor
; with an EmbeddedICE macrocell

;; end of peripheral list
;; *****************************************************************


;; ARMulator can tell you how much stack your program uses (at a
;; substantial runtime cost)
TrackStack=False



{ PeripheralSets

{ Processors_Common_ARMULATE=Processors_Common_No_Peripherals

;; This probably has to be loaded after after Flatmem (for now),
;; so we load it in Default_Common_Peipherals.
; Load code-sequences.
;{ Codeseq=Codeseq
;}

; The BUS model.
{ MemIntf=MemIntf
{Peripherals=Common_Peripherals
}
}

;End Processors_Common_ARMULATE
}

;End PeripheralSets
}


;;
;; This is the list of all processors supported by ARMulator.
;;

{ Processors

;; This isn't a processor.
Default=ARM926EJ-S

;; Entries are of the form:
;
; { <processor-name>=<processor-prototype>
; ... features ...
; }
;


;; ARM9 family
;


;This is a REV2 (but REV1 Jazelle)
{ ARM926EJ-S=Processors_Common_ARMULATE
;Processor=ARM926EJ-S
{meta_moduleserver_processor=ARM926EJ-S
revision=0
}
Revision=3
;CHIPID=0x41069262
Core=ARM9
ARMulator=ARM9ulator
Architecture=5T
Nexec=True
MultipleEarlyAborts=True
AbortsStopMultiple=True
CoreCycles=True
HighExceptionVectors=True
ARM9Extensions=True
ARMv5PExtensions=True
ARMJavaExtensions=True
ARMJavaV1Extensions=True
ARM9OptimizedMemory=True
ARM9OptimizedDAborts=True
ARM9CoprocessorInterface=True
HASMMU=True
HASTCRAM=True
HasSRAM=True
Memory=ARM926EJ_S
IRamSize=0x00000
DRamSize=0x00000
ICache_Associativity=4
DCache_Associativity=4
ICache_Lines=512
DCache_Lines=256
{DCC=No_DebugComms
}
}



;; New processors/variants can be added here.
}

;;
;; List of memory models
;;

{ Memories

;;
;; If there is a cache/mmu memory-model defined, then it loads the
;; memory named as Memories:Default to handle external accesses.
;;

;; Default memory model is the "Flat" model.
;; [Formerly, or the "MapFile" model if there is an armsd.map file to load.]
;; Validation suite uses the trickbox
;; but that's a SORDI plugin now.

;;;; Default default is the flat memory map
;;Default=Flat
Default=NUL_BIU

;Future: This performs some of the duties of a cache model
; for uncached cores (E.g. ARM7TDMI).
;Currently: This increments bus_BusyUntil once per call.
{Nul_BIU
Memory=Flat
}

ARM926EJ_S=ARM9xxE


{ ARM9xxE
;#if Validate
;Config=Standard
;#endif
Config=Enhanced


#if ARM9x6BootHiVectors || ARM966BootHiVectors
VIntHi=TRUE
#else
VIntHi=FALSE
#endif


{ARM926EJ_S
;ARM926EJ_S=ARM9xxE
IuTag_Lines = 4
DuTag_Lines = 8
IuTLB_Lines = 8
DuTLB_Lines = 8
TLB_Lines = 64
TLB_Associativity=1
TLB_LockdownLines=8
WriteBufferWords=16
ChipNumber=0x926
; Revision has moved to core's config, present here for historical reasons only.
ProcessId=True
;DRS 2001-06-16
;These are commented out because these are not in BRA_Stan.
;ARM926EJ_S:TLB_LookupOnlyStored
;ARM926EJ_S:ICache_ReadFillBuffer
;ARM926EJ_S:ICache_HitUnderMiss
;ARM926EJ_S:DCache_HitUnderMiss
;ARM926EJ_S:ICache_CriticalWordFirst
;ARM926EJ_S:DCache_CriticalWordFirst
;ARM926EJ_S:IFastCache_lines = 0
;ARM926EJ_S:DFastCache_lines = 0
}


MCCFG=1
;this was commented earlier. by setting this, the speed of both the AHB bus and the core is the same.
Memory=Default

}



#if MEMORY_BytelaneVeneer
BytelaneVeneer:Memory=Default
#endif


;; end of memories
}

;; Co-processor bus
CoprocessorBus=ARMCoprocessorBus

;;
;; Coprocessor configurations
;;

{ Coprocessors

; Here is the list of co-processors, in the form:
; Coprocessor[<n>]=Name

}

;;
;; Basic models (ARMulator extensions)
;;

{ EarlyModels
;;
;; "EarlyModels" get run before memory initialisation, "Models" after.
;;



{ EventConverter
}

;; End of EarlyModels
}

{ Models
;; This file is no longer used for Model configuration.
;; End of Models.
}




;; EOF armulate.dsc




