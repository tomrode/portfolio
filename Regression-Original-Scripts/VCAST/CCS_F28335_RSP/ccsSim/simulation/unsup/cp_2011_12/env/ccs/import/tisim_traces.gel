/* This GEL file is used for EP(Execute Packet) trace generation.
 *
 */

/****************************************************************************
 *       EP trace with cycle.CPU timestamp                                  *
 ****************************************************************************
 *                                                                          * 
 *                                                                          *
 * The trace generation can be controlled using one of the below methods    *
 *                                                                          *
 *    1. GEL Commands (executed from the CCS Command window)                *
 *       -> eval GEL_DriverString("START_EP_TRACE");                        *
 *       -> eval GEL_DriverString("STOP_EP_TRACE");                         *
 *                                                                          *
 *    2. GEL Menu (CCS->GEL->EP Trace)                                      *
 *       -> CCS->GEL->EP Trace->StartTrace                                  *
 *       -> CCS->GEL->EP Trace->StopTrace                                   *
 *                                                                          *
 * You could do multiple start and stop trace commands                      *
 *                                                                          *
 * The trace file has a .pcdt extension and will be generated in the        *    
 * COFF file path, with <coff_file_name>.pcdt                               *
 *                                                                          *
 ****************************************************************************/

menuitem "EPTrace"

hotmenu StartEPTrace(){
   GEL_DriverString("START_EP_TRACE");
   GEL_TextOut("Started EP Trace collection\n");
}

hotmenu StopEPTrace(){
   GEL_DriverString("STOP_EP_TRACE");
   GEL_TextOut("Stop EP Trace collection\n");
}

/* This GEL file is used for PCDT trace generation.
 *
 */

/****************************************************************************
 *       PC discontinuity trace with cycle.total timestamp                  *
 ****************************************************************************
 *                                                                          * 
 *                                                                          *
 * The trace generation can be controlled using one of the below methods    *
 *                                                                          *
 *    1. GEL Commands (executed from the CCS Command window)                *
 *       -> eval GEL_DriverString("START_PCDT");                            *
 *       -> eval GEL_DriverString("STOP_PCDT");                             *
 *                                                                          *
 *    2. GEL Menu (CCS->GEL->PDATS Trace)                                   *
 *       -> CCS->GEL->PCDT->StartTrace                                      *
 *       -> CCS->GEL->PCDT->StopTrace                                       *
 *                                                                          *
 * You could do multiple start and stop trace commands                      *
 *                                                                          *
 * The trace file has a .pcdt extension and will be generated in the        *    
 * COFF file path, with <coff_file_name>.pcdt                               *
 *                                                                          *
 ****************************************************************************/

menuitem "PCDT"

hotmenu StartPCDT(){
   GEL_DriverString("START_PCDT");
   GEL_TextOut("Started PCDT collection\n");
}

hotmenu StopPCDT(){
   GEL_DriverString("STOP_PCDT");
   GEL_TextOut("Stopped PCDT collections\n");
}

/* This GEL file is used for PDATS trace generation.
 *
 */

/****************************************************************************
 *       PDATS (Packed Differential Address and Time Stamp) Trace           *
 ****************************************************************************
 *                                                                          * 
 * PDATS is a loss less address trace compression for reducing file size    *
 * and access time. A PDATS file is n a binary format.                      *
 *                                                                          *
 * The simulator generates PDATS trace for the following access             *
 *    -> CPU Data read/Write                                                * 
 *    -> CPU Program fetch                                                  *
 *                                                                          *
 * The trace generation can be controlled using one of the below methods    *
 *                                                                          *
 *    1. GEL Commands (executed from the CCS Command window)                *
 *       -> eval GEL_DriverString("START_PDATS_COLLECTION");                *
 *       -> eval GEL_DriverString("STOP_PDATS_COLLECTION");                 *
 *                                                                          *
 *    2. GEL Menu (CCS->GEL->PDATS Trace)                                   *
 *       -> CCS->GEL->PDATS Trace->StartTrace                               *
 *       -> CCS->GEL->PDATS Trace->StopTrace                                *
 *                                                                          *
 * You could do multiple start and stop trace commands                      *
 *                                                                          *
 * The trace file has a .pdats extension and will be generated in the       *    
 * COFF file path, with <coff_file_name>.pdats                              *
 *                                                                          *
 *                                                                          *
 * The PDATS trace is a binary format and hence need a script to convert    *
 * it to the textual format. The pdats2text.exe utility available with      *
 * the CCS (at <CCS installation path> bin\utilities\trace) helps you to    *
 * convert the PDATS format to textual format.                              *
 *                                                                          *
 * Kindly note that the pdats file gets emptied on Target(simulator) reset. * 
 * Hence the usage model is                                                 *
 *    -> Run the application on the simulator                               *
 *    -> Start and Stop trace generation as needed                          *
 *    -> Quit the simulation (without a Target reset)                       *
 *    -> Use the trace data                                                 * 
 *                                                                          *
 ****************************************************************************/

menuitem "PDATS Trace"

hotmenu StartTrace(){
   GEL_DriverString("START_PDATS_COLLECTION");
   GEL_TextOut("Started PDATS collection\n");
}

hotmenu StopTrace(){
   GEL_DriverString("STOP_PDATS_COLLECTION");
   GEL_TextOut("Stopped PDATS collection\n");
}

