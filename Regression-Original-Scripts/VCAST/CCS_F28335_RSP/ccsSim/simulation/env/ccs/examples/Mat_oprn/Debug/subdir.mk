################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../mat_oprn.c \
../mat_xpose_oprn.c 

CMD_SRCS += \
../mat_xpose_oprn_1.cmd 

OBJS += \
./mat_oprn.obj \
./mat_xpose_oprn.obj 

C_DEPS += \
./mat_oprn.pp \
./mat_xpose_oprn.pp 


# Each subdirectory must supply rules for building sources it contributes
mat_oprn.obj: ../mat_oprn.c $(GEN_SRCS) $(GEN_OPTS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler v6.1'
	"D:/Texas_Instruments/CCSv4/tools/compiler/c6000/bin/cl6x" $(GEN_OPTS_QUOTED) --fp_reassoc=on --include_path="D:/Texas_Instruments/CCSv4/tools/compiler/c6000/include" --sat_reassoc=off --symdebug:dwarf --fp_mode=strict --mem_model:const=data --mem_model:data=far_aggregates --silicon_version=64+ --preproc_with_compile --preproc_dependency="mat_oprn.pp" $(subst #,$(wildcard $(subst $(SPACE),\$(SPACE),$<)),"#")
	@echo 'Finished building: $<'
	@echo ' '

mat_xpose_oprn.obj: ../mat_xpose_oprn.c $(GEN_SRCS) $(GEN_OPTS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler v6.1'
	"D:/Texas_Instruments/CCSv4/tools/compiler/c6000/bin/cl6x" $(GEN_OPTS_QUOTED) --fp_reassoc=on --include_path="D:/Texas_Instruments/CCSv4/tools/compiler/c6000/include" --sat_reassoc=off --symdebug:dwarf --fp_mode=strict --mem_model:const=data --mem_model:data=far_aggregates --silicon_version=64+ --preproc_with_compile --preproc_dependency="mat_xpose_oprn.pp" $(subst #,$(wildcard $(subst $(SPACE),\$(SPACE),$<)),"#")
	@echo 'Finished building: $<'
	@echo ' '


