/*
 *  Copyright 2002 by Texas Instruments Incorporated.
 *  All rights reserved. Property of Texas Instruments Incorporated.
 *  Restricted rights to use, duplicate or disclose this code are
 *  granted through contract.
 *  
 */

#include "mat_oprn.h"

/*****************************************************

Function Name : mat_oprn

Arguments: i,j - Elements of the matrix

Called from: mat_xpose_oprn

Purpose: Performs a dummy operation on the elements
of the matrix and returns the result.

*******************************************************/

#pragma CODE_SECTION(mat_oprn, ".mat_oprn")

short mat_oprn(short i, short j){
	short l;
	short k;
	for (l = 0; l < 4; l++){
		for (k = 0; k < 4; k++){
			i += k * (l + 1);
			j += l * (k + 1);	
		}
	}
	i = i + j;
	j = i * i;
	i = j * j;
	j = i * j;
	i = (i + j) * j;
	return (i + j);
}
