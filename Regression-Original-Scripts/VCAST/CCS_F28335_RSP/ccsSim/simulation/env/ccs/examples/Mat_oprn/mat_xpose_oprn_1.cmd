/* ======================================================================== */
/*                                                                          */
/*  TEXAS INSTRUMENTS, INC.                                                 */
/*                                                                          */
/*  NAME                                                                    */
/*      mat_xpose_oprn_1.cmd					                            */
/*                                                                          */
/*  PLATFORM                                                                */
/*      C64x                                                                */
/*                                                                          */
/* ------------------------------------------------------------------------ */
/*            Copyright (c) 2003 Texas Instruments, Incorporated.           */
/*                           All Rights Reserved.                           */
/* ======================================================================== */

-stack 1024
-heap 32768 
 
MEMORY
{
	MEM0: 		o = 00000000h   l = 00001000h
    MEM1:		o = 00001000h	l = 00004000h
    MEM2:		o = 00005000h	l = 00004000h
    SRAM:       o = 00009000h   l = 000F7000h                           
    CE0:		o = 80000000h	l = 01000000h 
    CE1:		o = 70000000h	l = 01000000h 
}

SECTIONS
{

    .text:				>       CE0
    .mat_xpose_oprn  	>		MEM0
    .mat_oprn		  	>		MEM0

    .stack      >       MEM1
    .cinit      >       MEM1
    .data		>       CE0
    .data:A		>		CE1		
    .bss        >       CE0
    .const      >       MEM1
    .far        >       CE0
    .cio		>		SRAM
    .sysmem		>		SRAM
    .external   >       SRAM
}                             

/* ======================================================================== */
/*            Copyright (c) 2002 Texas Instruments, Incorporated.           */
/*                           All Rights Reserved.                           */
/* ======================================================================== */