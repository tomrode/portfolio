Example: Mat_oprn
About: This example does a matrix mutliplication of matrix A & B and stores the result in C.
       Matrix A is stored at 0x70000000 and a memory pointer which corrupts that locations.

Objective: Identify the memory corrupting code in the application using data watchpoints.

Instructions:

Step 1: Bring-up C674x Simulator & load the program.
Step 2: Set a data watchpoint for Variable A - property type - write access
Step 3: Right click on the watchpoint & select properties. Modify the length to 0x4000 & skip count to 0x1000 (Length to monitor access on the entire matrix & skip count is to skip the initialization of matrix A)
Step 3: Run the simulator and it will stop at source line where the memory corruption loop in place.



