/*
 *  Copyright 2002 by Texas Instruments Incorporated.
 *  All rights reserved. Property of Texas Instruments Incorporated.
 *  Restricted rights to use, duplicate or disclose this code are
 *  granted through contract.
 *
 */

#include "mat_oprn.h"


#define MAT_ORDER 64
short A[MAT_ORDER][MAT_ORDER], B[MAT_ORDER][MAT_ORDER], C[MAT_ORDER][MAT_ORDER];

#pragma DATA_SECTION(A , ".data:A")

/*************************************************************

Function Name : mat_xpose_oprn

Arguments: None

Called from: main

Purpose: Performs the following operation on matrices
A and B and stores the result in C

C = A <mat_oprn> transpose(B)

Note: The matrix B in the function below is accessed in a column
major order. This causes the rows of the matrix B that are
in the cache to be replaced by the rows that are fetched later
as the size of the matrix is large. This causes a lot of
data cache misses for B.

Further mat_xpose_oprn is placed in the memory such that
it conflicts with mat_oprn in the cache ( refer mat_xpose_oprn_1.cmd).
Hence there are a lot of program cache misses.
***************************************************************/

#pragma CODE_SECTION(mat_xpose_oprn, ".mat_xpose_oprn")

void mat_xpose_oprn(void){
	short i,j;
	
	for (i = 0; i < MAT_ORDER; i++) {
		for (j = 0; j < MAT_ORDER; j++){
			C[i][j] = mat_oprn(A[i][j],B[j][i]);
		}
		printf("Processing row %d & column \n",i);
	}
}

void array_initialization(void)
{
	int i,j;
	
	for (i = 0; i < MAT_ORDER; i++) {
		for (j = 0; j < MAT_ORDER; j++){
			A[i][j] = i;
			B[i][j] = j;
			C[i][j] = i+j;
		}
	}
}

main()
{
   int i;
   int data_ptr = 0x70000000;

	array_initialization();
   

	// Memory coruption code
	//*data_ptr = 0x70000000; & It is corrupting A matrix	
	for (i=0;i<10;i++)
	{
		*(int *)(data_ptr+4*i) = i;
	}

	mat_xpose_oprn();
}
