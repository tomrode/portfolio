set PATH=%PATH%;C:\ghs\comp_201654;C:\ghs\multi_714

REM set CROWN_SOURCE_BASE=%cd%\..

set VECTORCAST_DIR=C:\VCAST\6.4v
set CROWN_SOURCE_BASE=C:\rtc_workspaces\TmlVcmMasterBuildDef001_Sandbox
set VCAST_SUPPORT_FILES=%cd%\supportFiles

SET VCAST_DISABLE_TEST_MAINTENANCE=1

set VCAST_CCS_INSTALL_DIR=C:\ti\ccsv7

REM Path to tools for debugging
set VCAST_CCS_COMPILER_INSTALL_DIR=C:\ti\c2000_6.2.11

set BIOS_INSTALL_DIR=C:\ti\bios_6_35_04_50

REM Path to compiler
set PATH=%VCAST_CCS_COMPILER_INSTALL_DIR%\bin;%BIOS_INSTALL_DIR%\xdctools;%PATH%;

set C2000INC=%VCAST_CCS_INSTALL_DIR%\ccs_base\c2000\include
set C2000LIB=%VCAST_CCS_INSTALL_DIR%\ccs_base\c2000\nowFlash\libraries


%VECTORCAST_DIR%\vcastqt -lc