/*
 * Do not modify this file; it is automatically generated from the template
 * linkcmd.xdt in the ti.targets package and will be overwritten.
 */

/*
 * put '"'s around paths because, without this, the linker
 * considers '-' as minus operator, not a file name character.
 */


-l"C:\rtc\cgra_ws2\VCM_SUPERVISOR_APP\apps\Vcm\Release\configPkg\package\cfg\Svcm_p28FP.o28FP"
-l"C:\ti\bios_6_35_04_50\packages\ti\sysbios\lib\sysbios\instrumented\sysbios.a28FP"
-l"C:\ti\xdctools_3_24_06_63\packages\ti\catalog\c2800\init\lib\Boot.a28FP"
-l"C:\ti\xdctools_3_24_06_63\packages\ti\targets\rts2800\lib\ti.targets.rts2800.a28FP"
-l"C:\ti\xdctools_3_24_06_63\packages\ti\targets\rts2800\lib\boot.a28FP"


/* function aliases */

--symbol_map _xdc_runtime_Text_putSite__E=_xdc_runtime_Text_putSite__F
--symbol_map _xdc_runtime_Text_putLab__E=_xdc_runtime_Text_putLab__F
--symbol_map _ti_sysbios_family_c28_TimestampProvider_rolloverFunc__E=_ti_sysbios_family_c28_TimestampProvider_rolloverFunc__F
--symbol_map _xdc_runtime_Registry_getModuleId__E=_xdc_runtime_Registry_getModuleId__F
--symbol_map _ti_sysbios_family_c28_TimestampProvider_startTimer__E=_ti_sysbios_family_c28_TimestampProvider_startTimer__F
--symbol_map _xdc_runtime_Error_check__E=_xdc_runtime_Error_check__F
--symbol_map _xdc_runtime_System_sprintf_va__E=_xdc_runtime_System_sprintf_va__F
--symbol_map _xdc_runtime_Text_cordText__E=_xdc_runtime_Text_cordText__F
--symbol_map _xdc_runtime_Registry_findByName__E=_xdc_runtime_Registry_findByName__F
--symbol_map _xdc_runtime_System_avsprintf__E=_xdc_runtime_System_avsprintf__F
--symbol_map _xdc_runtime_Registry_getMask__E=_xdc_runtime_Registry_getMask__F
--symbol_map _xdc_runtime_Memory_getMaxDefaultTypeAlign__E=_xdc_runtime_Memory_getMaxDefaultTypeAlign__F
--symbol_map _xdc_runtime_Gate_leaveSystem__E=_xdc_runtime_Gate_leaveSystem__F
--symbol_map _xdc_runtime_Error_getSite__E=_xdc_runtime_Error_getSite__F
--symbol_map _xdc_runtime_Timestamp_get64__E=_xdc_runtime_Timestamp_get64__F
--symbol_map _xdc_runtime_Timestamp_get32__E=_xdc_runtime_Timestamp_get32__F
--symbol_map _xdc_runtime_System_printf_va__E=_xdc_runtime_System_printf_va__F
--symbol_map _xdc_runtime_System_vsprintf__E=_xdc_runtime_System_vsprintf__F
--symbol_map _xdc_runtime_Log_doPrint__E=_xdc_runtime_Log_doPrint__F
--symbol_map _ti_sysbios_family_c28_Timer_setPrescale__E=_ti_sysbios_family_c28_Timer_setPrescale__F
--symbol_map _xdc_runtime_SysMin_ready__E=_xdc_runtime_SysMin_ready__F
--symbol_map _xdc_runtime_SysMin_abort__E=_xdc_runtime_SysMin_abort__F
--symbol_map _xdc_runtime_Text_matchRope__E=_xdc_runtime_Text_matchRope__F
--symbol_map _xdc_runtime_SysMin_putch__E=_xdc_runtime_SysMin_putch__F
--symbol_map _xdc_runtime_Error_init__E=_xdc_runtime_Error_init__F
--symbol_map _xdc_runtime_Error_getId__E=_xdc_runtime_Error_getId__F
--symbol_map _xdc_runtime_Error_raiseX__E=_xdc_runtime_Error_raiseX__F
--symbol_map _xdc_runtime_Registry_getModuleName__E=_xdc_runtime_Registry_getModuleName__F
--symbol_map _xdc_runtime_System_putch__E=_xdc_runtime_System_putch__F
--symbol_map _xdc_runtime_System_abort__E=_xdc_runtime_System_abort__F
--symbol_map _xdc_runtime_Diags_setMask__E=_xdc_runtime_Diags_setMask__F
--symbol_map _xdc_runtime_System_aprintf_va__E=_xdc_runtime_System_aprintf_va__F
--symbol_map _xdc_runtime_Registry_getNextModule__E=_xdc_runtime_Registry_getNextModule__F
--symbol_map _xdc_runtime_Error_getData__E=_xdc_runtime_Error_getData__F
--symbol_map _xdc_runtime_Registry_isMember__E=_xdc_runtime_Registry_isMember__F
--symbol_map _xdc_runtime_Text_putMod__E=_xdc_runtime_Text_putMod__F
--symbol_map _xdc_runtime_Memory_getStats__E=_xdc_runtime_Memory_getStats__F
--symbol_map _xdc_runtime_Memory_free__E=_xdc_runtime_Memory_free__F
--symbol_map _ti_sysbios_family_c28_Timer_getPrescale__E=_ti_sysbios_family_c28_Timer_getPrescale__F
--symbol_map _xdc_runtime_Startup_exec__E=_xdc_runtime_Startup_exec__F
--symbol_map _xdc_runtime_Memory_query__E=_xdc_runtime_Memory_query__F
--symbol_map _ti_sysbios_gates_GateMutex_query__E=_ti_sysbios_gates_GateMutex_query__F
--symbol_map _xdc_runtime_Timestamp_getFreq__E=_xdc_runtime_Timestamp_getFreq__F
--symbol_map _xdc_runtime_System_vprintf__E=_xdc_runtime_System_vprintf__F
--symbol_map _xdc_runtime_SysMin_flush__E=_xdc_runtime_SysMin_flush__F
--symbol_map _xdc_runtime_Error_getCode__E=_xdc_runtime_Error_getCode__F
--symbol_map _xdc_runtime_System_exit__E=_xdc_runtime_System_exit__F
--symbol_map _xdc_runtime_Registry_findById__E=_xdc_runtime_Registry_findById__F
--symbol_map _ti_sysbios_gates_GateHwi_query__E=_ti_sysbios_gates_GateHwi_query__F
--symbol_map _xdc_runtime_Error_getMsg__E=_xdc_runtime_Error_getMsg__F
--symbol_map _xdc_runtime_Startup_rtsDone__E=_xdc_runtime_Startup_rtsDone__F
--symbol_map _xdc_runtime_Registry_addModule__E=_xdc_runtime_Registry_addModule__F
--symbol_map _xdc_runtime_Memory_alloc__E=_xdc_runtime_Memory_alloc__F
--symbol_map _xdc_runtime_System_flush__E=_xdc_runtime_System_flush__F
--symbol_map _xdc_runtime_System_avprintf__E=_xdc_runtime_System_avprintf__F
--symbol_map _xdc_runtime_Text_ropeText__E=_xdc_runtime_Text_ropeText__F
--symbol_map _xdc_runtime_Error_print__E=_xdc_runtime_Error_print__F
--symbol_map _xdc_runtime_Memory_calloc__E=_xdc_runtime_Memory_calloc__F
--symbol_map _ti_sysbios_family_c28_Timer_getPrescaleCount__E=_ti_sysbios_family_c28_Timer_getPrescaleCount__F
--symbol_map _xdc_runtime_System_asprintf_va__E=_xdc_runtime_System_asprintf_va__F
--symbol_map _xdc_runtime_System_atexit__E=_xdc_runtime_System_atexit__F
--symbol_map _xdc_runtime_SysMin_exit__E=_xdc_runtime_SysMin_exit__F
--symbol_map _xdc_runtime_Registry_findByNamePattern__E=_xdc_runtime_Registry_findByNamePattern__F
--symbol_map _xdc_runtime_Gate_enterSystem__E=_xdc_runtime_Gate_enterSystem__F
--symbol_map _xdc_runtime_Memory_valloc__E=_xdc_runtime_Memory_valloc__F


/* Elf symbols */
--symbol_map ___TI_STACK_BASE=__stack
--symbol_map ___TI_STACK_SIZE=__STACK_SIZE
--symbol_map ___TI_STATIC_BASE=___bss__
--symbol_map __c_int00=_c_int00



/*-stack 0x400
/*PAGE 1: M0SARAM (RWX) : org = 0x0, len = 0x400*/
/*    PAGE 1: M1SARAM (RWX) : org = 0x400, len = 0x400*/
/* --args 0x0 */
/* -heap  0x0 */
-stack 0x800

MEMORY
{
    PAGE 0: MEMMAP (RWX) : org = 0x337e60, len = 0x50
    PAGE 0: BOOTROM (RWX) : org = 0x3fe000, len = 0x1fc0
    PAGE 0: TRADEMARK (RWX) : org = 0x337e00, len = 0x1f
    PAGE 0: PASSWORDS (RWX) : org = 0x33fff8, len = 0x8
    PAGE 0: FILLMAP (RWX) : org = 0x337e50, len = 0xf
    PAGE 0: PARAMFLAG (RWX) : org = 0x300000, len = 0x1
    PAGE 1: L0SARAM (RWX) : org = 0x8000, len = 0x1000
    PAGE 1: H0SARAM (RWX) : org = 0xc002, len = 0x3ffe
    PAGE 1: M1SARAM (RWX) : org = 0x0, len = 0x800
    PAGE 1: L1SARAM (RWX) : org = 0x9000, len = 0x3000
    PAGE 0: OTP (RWX) : org = 0x380400, len = 0x400
    PAGE 0: CSM_RSVD (RWX) : org = 0x33ff80, len = 0x76
    PAGE 0: APPCS (RWX) : org = 0x337ff2, len = 0x1
    PAGE 0: PARTNUM (RWX) : org = 0x337e20, len = 0x2f
    PAGE 0: FLASH_H (RWX) : org = 0x300001, len = 0x7fff
    PAGE 0: FLASH_A (RWX) : org = 0x338000, len = 0x7f80
    PAGE 0: ADC_CAL (RWX) : org = 0x380080, len = 0x9
    PAGE 0: VECTORS (RWX) : org = 0x3fffc0, len = 0x3e
    PAGE 0: COMPATABLE (RWX) : org = 0x337eb0, len = 0x140
    PAGE 0: ZONE7 (RWX) : org = 0x200000, len = 0x40000
    PAGE 0: APPCRC (RWX) : org = 0x337ff0, len = 0x1
    PAGE 0: FLASH_BCDEFG (RWX) : org = 0x308002, len = 0x2fdfd
    PAGE 0: BEGIN_FLASH (RWX) : org = 0x308000, len = 0x2
    PAGE 1: ZONE0 (RWX) : org = 0x4000, len = 0xff
    PAGE 1: PIEVECT (RWX) : org = 0xd00, len = 0x100
}

/*
 * Linker command file contributions from all loaded packages:
 */

/* Content from xdc.services.global (null): */

/* Content from xdc (null): */

/* Content from xdc.corevers (null): */

/* Content from xdc.shelf (null): */

/* Content from xdc.services.spec (null): */

/* Content from xdc.services.intern.xsr (null): */

/* Content from xdc.services.intern.gen (null): */

/* Content from xdc.services.intern.cmd (null): */

/* Content from xdc.bld (null): */

/* Content from ti.targets (null): */

/* Content from xdc.rov (null): */

/* Content from xdc.runtime (null): */

/* Content from ti.targets.rts2800 (null): */

/* Content from ti.sysbios.interfaces (null): */

/* Content from ti.sysbios.family (null): */

/* Content from ti.sysbios.hal (null): */

/* Content from xdc.services.getset (null): */

/* Content from ti.catalog.c2800.init (ti/catalog/c2800/init/linkcmd.xdt): */

/* Content from ti.sysbios.rts (null): */

/* Content from ti.sysbios (null): */

/* Content from ti.sysbios.knl (null): */

/* Content from ti.sysbios.family.c28 (null): */

/* Content from ti.sysbios.utils (null): */

/* Content from xdc.runtime.knl (null): */

/* Content from ti.sysbios.gates (null): */

/* Content from ti.sysbios.xdcruntime (null): */

/* Content from ti.sysbios.heaps (null): */

/* Content from ti.catalog.c2800 (null): */

/* Content from ti.catalog (null): */

/* Content from xdc.platform (null): */

/* Content from xdc.cfg (null): */

/* Content from ti.platforms.generic (null): */

/* Content from svcm_tms320x28 (null): */

/* Content from configPkg (null): */


/*
 * symbolic aliases for numeric constants or static instance objects
 */
_xdc_runtime_Startup__EXECFXN__C = 1;
_xdc_runtime_Startup__RESETFXN__C = 1;
_TSK_idle = _ti_sysbios_knl_Task_Object__table__V + 170;

SECTIONS
{
    .text: load >> FLASH_BCDEFG PAGE 0
    .switch: load >> FLASH_BCDEFG PAGE 0
    .data: load >> FLASH_BCDEFG PAGE 0
    .cinit: load > FLASH_BCDEFG PAGE 0
    .bss: load > H0SARAM PAGE 1
    .ebss: load >> H0SARAM PAGE 1
	.cio:   load >> H0SARAM PAGE 1
    .econst: load >> FLASH_BCDEFG PAGE 0
    .const: load >> FLASH_BCDEFG PAGE 0
    .stack: load > M1SARAM PAGE 1
    .sysmem: load > OTP PAGE 0
    .esysmem: load > L1SARAM PAGE 1
    .pinit: load > FLASH_BCDEFG PAGE 0
    .args: load > L0SARAM PAGE 1 align = 0x4, fill = 0 {_argsize = 0x0; }

    .idl: load >> L0SARAM PAGE 1
    .ixxat: load >> L1SARAM PAGE 1
    .ebss:taskStackSection: load >> M1SARAM PAGE 1
    xdc.meta: load >> FLASH_BCDEFG PAGE 0, type = COPY

}

/*     .cio: load >> M0SARAM PAGE 1 */

