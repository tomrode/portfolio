-- VectorCAST 6.4p (01/31/17)
-- Test Case Script
-- 
-- Environment    : OS_INTERFACE_INTEGRATION
-- Unit(s) Under Test: OS_Interface
-- 
-- Script Features
TEST.SCRIPT_FEATURE:C_DIRECT_ARRAY_INDEXING
TEST.SCRIPT_FEATURE:CPP_CLASS_OBJECT_REVISION
TEST.SCRIPT_FEATURE:MULTIPLE_UUT_SUPPORT
TEST.SCRIPT_FEATURE:MIXED_CASE_NAMES
TEST.SCRIPT_FEATURE:STATIC_HEADER_FUNCS_IN_UUTS
--

-- Subprogram: <<INIT>>

-- Test Case: <<INIT>>.001
TEST.SUBPROGRAM:<<INIT>>
TEST.NEW
TEST.NAME:<<INIT>>.001
TEST.END

-- Subprogram: gpOS_ThreadIdentify

-- Test Case: BASIS-PATH-001
TEST.UNIT:OS_Interface
TEST.SUBPROGRAM:gpOS_ThreadIdentify
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 1
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.END

-- Subprogram: gulOS_CalcUnusedStackSize

-- Test Case: While_Loop_Entry_==_0xEF
TEST.UNIT:OS_Interface
TEST.SUBPROGRAM:gulOS_CalcUnusedStackSize
TEST.NEW
TEST.NAME:While_Loop_Entry_==_0xEF
TEST.BASIS_PATH:1 of 3 (partial)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (1) while (pubStackBytes[ulUnusedBytes] == (uint8_t)0xefU) ==> FALSE
   Test Case Generation Notes:
      Cannot set local variable pubStackBytes in branch 1
      Cannot set local variable ulUnusedBytes in branch 1
TEST.END_NOTES:
TEST.VALUE:USER_GLOBALS_VCAST.<<GLOBAL>>.VECTORCAST_UINT8:0xEF
TEST.VALUE:OS_Interface.gulOS_CalcUnusedStackSize.pvStackStart:VECTORCAST_UINT8
TEST.VALUE:OS_Interface.gulOS_CalcUnusedStackSize.ulStackSize:1
TEST.EXPECTED:OS_Interface.gulOS_CalcUnusedStackSize.ulStackSize:1
TEST.ATTRIBUTES:USER_GLOBALS_VCAST.<<GLOBAL>>.VECTORCAST_BUFFER.VECTORCAST_BUFFER[0]:INPUT_BASE=16,EXPECTED_BASE=16
TEST.END

-- Subprogram: gulOS_GetTime

-- Test Case: BASIS-PATH-001
TEST.UNIT:OS_Interface
TEST.SUBPROGRAM:gulOS_GetTime
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 1
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.END

-- Subprogram: gulOS_InterruptControl

-- Test Case: BASIS-PATH-001
TEST.UNIT:OS_Interface
TEST.SUBPROGRAM:gulOS_InterruptControl
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 1
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.VALUE:OS_Interface.gulOS_InterruptControl.ulNewPosture:<<MIN>>
TEST.END

-- Subprogram: gulOS_MutexCreate

-- Test Case: BASIS-PATH-001
TEST.UNIT:OS_Interface
TEST.SUBPROGRAM:gulOS_MutexCreate
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 1
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.VALUE:OS_Interface.gulOS_MutexCreate.pMutex:<<malloc 1>>
TEST.VALUE:OS_Interface.gulOS_MutexCreate.pszMutexName:<<malloc 1>>
TEST.VALUE:OS_Interface.gulOS_MutexCreate.ulPriorityInheritOption:<<MIN>>
TEST.END

-- Subprogram: gulOS_MutexGet

-- Test Case: BASIS-PATH-001
TEST.UNIT:OS_Interface
TEST.SUBPROGRAM:gulOS_MutexGet
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 1
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.VALUE:OS_Interface.gulOS_MutexGet.pMutex:<<malloc 1>>
TEST.VALUE:OS_Interface.gulOS_MutexGet.ulWaitOption:<<MIN>>
TEST.END

-- Subprogram: gulOS_MutexPut

-- Test Case: BASIS-PATH-001
TEST.UNIT:OS_Interface
TEST.SUBPROGRAM:gulOS_MutexPut
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 1
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.VALUE:OS_Interface.gulOS_MutexPut.pMutex:<<malloc 1>>
TEST.END

-- Subprogram: gulOS_QueueCreate

-- Test Case: BASIS-PATH-001
TEST.UNIT:OS_Interface
TEST.SUBPROGRAM:gulOS_QueueCreate
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 1
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.VALUE:OS_Interface.gulOS_QueueCreate.pQueue:<<malloc 1>>
TEST.VALUE:OS_Interface.gulOS_QueueCreate.pszQueueName:<<malloc 1>>
TEST.VALUE:OS_Interface.gulOS_QueueCreate.ulMessageSize:<<MIN>>
TEST.VALUE:OS_Interface.gulOS_QueueCreate.pvQueueStart:VECTORCAST_INT1
TEST.VALUE:OS_Interface.gulOS_QueueCreate.ulQueueSize:<<MIN>>
TEST.END

-- Subprogram: gulOS_QueueDisableSendNotify

-- Test Case: BASIS-PATH-001
TEST.UNIT:OS_Interface
TEST.SUBPROGRAM:gulOS_QueueDisableSendNotify
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 1
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.VALUE:OS_Interface.gulOS_QueueDisableSendNotify.pQueue:<<malloc 1>>
TEST.END

-- Subprogram: gulOS_QueueGetAvailableStorage

-- Test Case: BASIS-PATH-001
TEST.UNIT:OS_Interface
TEST.SUBPROGRAM:gulOS_QueueGetAvailableStorage
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 2
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (1) if ((os_queue_t *)0U == pQueue) ==> FALSE
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:OS_Interface.gulOS_QueueGetAvailableStorage.pQueue:<<malloc 1>>
TEST.END

-- Test Case: BASIS-PATH-002
TEST.UNIT:OS_Interface
TEST.SUBPROGRAM:gulOS_QueueGetAvailableStorage
TEST.NEW
TEST.NAME:BASIS-PATH-002
TEST.BASIS_PATH:2 of 2
TEST.NOTES:
This is an automatically generated test case.
   Test Path 2
      (1) if ((os_queue_t *)0U == pQueue) ==> TRUE
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:OS_Interface.gulOS_QueueGetAvailableStorage.pQueue:<<null>>
TEST.END

-- Subprogram: gulOS_QueueReceive

-- Test Case: BASIS-PATH-001
TEST.UNIT:OS_Interface
TEST.SUBPROGRAM:gulOS_QueueReceive
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 1
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.VALUE:OS_Interface.gulOS_QueueReceive.pQueue:<<malloc 1>>
TEST.VALUE:OS_Interface.gulOS_QueueReceive.pvDestination:VECTORCAST_INT1
TEST.VALUE:OS_Interface.gulOS_QueueReceive.ulWaitOption:<<MIN>>
TEST.END

-- Subprogram: gulOS_QueueSend

-- Test Case: BASIS-PATH-001
TEST.UNIT:OS_Interface
TEST.SUBPROGRAM:gulOS_QueueSend
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 1
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.VALUE:OS_Interface.gulOS_QueueSend.pQueue:<<malloc 1>>
TEST.VALUE:OS_Interface.gulOS_QueueSend.pvSource:VECTORCAST_INT1
TEST.VALUE:OS_Interface.gulOS_QueueSend.ulWaitOption:<<MIN>>
TEST.END

-- Subprogram: gulOS_QueueSendNotify

-- Test Case: BASIS-PATH-001
TEST.UNIT:OS_Interface
TEST.SUBPROGRAM:gulOS_QueueSendNotify
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 1
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.VALUE:OS_Interface.gulOS_QueueSendNotify.pQueue:<<malloc 1>>
TEST.END

-- Subprogram: gulOS_SemaphoreCeilingPut

-- Test Case: BASIS-PATH-001
TEST.UNIT:OS_Interface
TEST.SUBPROGRAM:gulOS_SemaphoreCeilingPut
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 1
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.VALUE:OS_Interface.gulOS_SemaphoreCeilingPut.pSemaphore:<<malloc 1>>
TEST.VALUE:OS_Interface.gulOS_SemaphoreCeilingPut.ulCeiling:<<MIN>>
TEST.END

-- Subprogram: gulOS_SemaphoreCreate

-- Test Case: BASIS-PATH-001
TEST.UNIT:OS_Interface
TEST.SUBPROGRAM:gulOS_SemaphoreCreate
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 1
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.VALUE:OS_Interface.gulOS_SemaphoreCreate.pSemaphore:<<malloc 1>>
TEST.VALUE:OS_Interface.gulOS_SemaphoreCreate.pszSemaphoreName:<<malloc 1>>
TEST.VALUE:OS_Interface.gulOS_SemaphoreCreate.ulInitialCount:<<MIN>>
TEST.END

-- Subprogram: gulOS_SemaphoreGet

-- Test Case: BASIS-PATH-001
TEST.UNIT:OS_Interface
TEST.SUBPROGRAM:gulOS_SemaphoreGet
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 1
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.VALUE:OS_Interface.gulOS_SemaphoreGet.pSemaphore:<<malloc 1>>
TEST.VALUE:OS_Interface.gulOS_SemaphoreGet.ulWaitOption:<<MIN>>
TEST.END

-- Subprogram: gulOS_SemaphoreGetCount

-- Test Case: BASIS-PATH-001
TEST.UNIT:OS_Interface
TEST.SUBPROGRAM:gulOS_SemaphoreGetCount
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 1
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.VALUE:OS_Interface.gulOS_SemaphoreGetCount.pSemaphore:<<malloc 1>>
TEST.VALUE:OS_Interface.gulOS_SemaphoreGetCount.pulCurrentValue:<<malloc 1>>
TEST.END

-- Subprogram: gulOS_SemaphorePut

-- Test Case: BASIS-PATH-001
TEST.UNIT:OS_Interface
TEST.SUBPROGRAM:gulOS_SemaphorePut
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 1
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.VALUE:OS_Interface.gulOS_SemaphorePut.pSemaphore:<<malloc 1>>
TEST.END

-- Subprogram: gulOS_ThreadCreate

-- Test Case: BASIS-PATH-001
TEST.UNIT:OS_Interface
TEST.SUBPROGRAM:gulOS_ThreadCreate
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 1
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.VALUE:OS_Interface.gulOS_ThreadCreate.pThread:<<malloc 1>>
TEST.VALUE:OS_Interface.gulOS_ThreadCreate.pszThreadName:<<malloc 1>>
TEST.VALUE:OS_Interface.gulOS_ThreadCreate.ulEntryInput:<<MIN>>
TEST.VALUE:OS_Interface.gulOS_ThreadCreate.pvStackStart:VECTORCAST_INT1
TEST.VALUE:OS_Interface.gulOS_ThreadCreate.ulStackSize:<<MIN>>
TEST.VALUE:OS_Interface.gulOS_ThreadCreate.ulPriority:<<MIN>>
TEST.VALUE:OS_Interface.gulOS_ThreadCreate.ulPreemptThreashold:<<MIN>>
TEST.VALUE:OS_Interface.gulOS_ThreadCreate.ulTimeSlice:<<MIN>>
TEST.VALUE:OS_Interface.gulOS_ThreadCreate.ulAutoStart:<<MIN>>
TEST.END

-- Subprogram: gulOS_ThreadPriorityChange

-- Test Case: BASIS-PATH-001
TEST.UNIT:OS_Interface
TEST.SUBPROGRAM:gulOS_ThreadPriorityChange
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 2
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (1) if ((0U) == pOldPriority) ==> FALSE
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:OS_Interface.gulOS_ThreadPriorityChange.pThread:<<malloc 1>>
TEST.VALUE:OS_Interface.gulOS_ThreadPriorityChange.ulPriority:<<MIN>>
TEST.VALUE:OS_Interface.gulOS_ThreadPriorityChange.pOldPriority:<<malloc 1>>
TEST.END

-- Test Case: BASIS-PATH-002
TEST.UNIT:OS_Interface
TEST.SUBPROGRAM:gulOS_ThreadPriorityChange
TEST.NEW
TEST.NAME:BASIS-PATH-002
TEST.BASIS_PATH:2 of 2
TEST.NOTES:
This is an automatically generated test case.
   Test Path 2
      (1) if ((0U) == pOldPriority) ==> TRUE
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:OS_Interface.gulOS_ThreadPriorityChange.pThread:<<malloc 1>>
TEST.VALUE:OS_Interface.gulOS_ThreadPriorityChange.ulPriority:<<MIN>>
TEST.VALUE:OS_Interface.gulOS_ThreadPriorityChange.pOldPriority:<<null>>
TEST.END

-- Subprogram: gulOS_ThreadResume

-- Test Case: BASIS-PATH-001
TEST.UNIT:OS_Interface
TEST.SUBPROGRAM:gulOS_ThreadResume
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 1
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.VALUE:OS_Interface.gulOS_ThreadResume.pThread:<<malloc 1>>
TEST.END

-- Subprogram: gulOS_ThreadSleep

-- Test Case: BASIS-PATH-001
TEST.UNIT:OS_Interface
TEST.SUBPROGRAM:gulOS_ThreadSleep
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 2
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (1) if (0U != ulTimerTicks) ==> FALSE
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:OS_Interface.gulOS_ThreadSleep.ulTimerTicks:1
TEST.END

-- Subprogram: gulOS_ThreadSuspend

-- Test Case: BASIS-PATH-001
TEST.UNIT:OS_Interface
TEST.SUBPROGRAM:gulOS_ThreadSuspend
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 1
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.VALUE:OS_Interface.gulOS_ThreadSuspend.pThread:<<malloc 1>>
TEST.END

-- Subprogram: gulOS_TimerActivate

-- Test Case: BASIS-PATH-001
TEST.UNIT:OS_Interface
TEST.SUBPROGRAM:gulOS_TimerActivate
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 1
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.VALUE:OS_Interface.gulOS_TimerActivate.pTimer:<<malloc 1>>
TEST.END

-- Subprogram: gulOS_TimerChange

-- Test Case: BASIS-PATH-001
TEST.UNIT:OS_Interface
TEST.SUBPROGRAM:gulOS_TimerChange
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 1
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.VALUE:OS_Interface.gulOS_TimerChange.pTimer:<<malloc 1>>
TEST.VALUE:OS_Interface.gulOS_TimerChange.ulInitialTicks:<<MIN>>
TEST.VALUE:OS_Interface.gulOS_TimerChange.ulRescheduleTicks:<<MIN>>
TEST.END

-- Subprogram: gulOS_TimerCreate

-- Test Case: BASIS-PATH-001
TEST.UNIT:OS_Interface
TEST.SUBPROGRAM:gulOS_TimerCreate
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 2
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (1) if (1U == fAutoActivate) ==> FALSE
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:OS_Interface.gulOS_TimerCreate.pTimer:<<malloc 1>>
TEST.VALUE:OS_Interface.gulOS_TimerCreate.pszTimerName:<<malloc 1>>
TEST.VALUE:OS_Interface.gulOS_TimerCreate.ulEntryInput:<<MIN>>
TEST.VALUE:OS_Interface.gulOS_TimerCreate.ulInitialTicks:<<MIN>>
TEST.VALUE:OS_Interface.gulOS_TimerCreate.ulRescheduleTicks:<<MIN>>
TEST.VALUE:OS_Interface.gulOS_TimerCreate.fAutoActivate:<<MIN>>
TEST.END

-- Test Case: BASIS-PATH-002-PARTIAL
TEST.UNIT:OS_Interface
TEST.SUBPROGRAM:gulOS_TimerCreate
TEST.NEW
TEST.NAME:BASIS-PATH-002-PARTIAL
TEST.BASIS_PATH:2 of 2 (partial)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 2
      (1) if (1U == fAutoActivate) ==> TRUE
   Test Case Generation Notes:
      Cannot set variable to out of range value 1 in branch 1
TEST.END_NOTES:
TEST.VALUE:OS_Interface.gulOS_TimerCreate.pTimer:<<malloc 1>>
TEST.VALUE:OS_Interface.gulOS_TimerCreate.pszTimerName:<<malloc 1>>
TEST.VALUE:OS_Interface.gulOS_TimerCreate.ulEntryInput:<<MIN>>
TEST.VALUE:OS_Interface.gulOS_TimerCreate.ulInitialTicks:<<MIN>>
TEST.VALUE:OS_Interface.gulOS_TimerCreate.ulRescheduleTicks:<<MIN>>
TEST.VALUE:OS_Interface.gulOS_TimerCreate.fAutoActivate:<<MIN>>
TEST.END

-- Subprogram: gulOS_TimerDeactivate

-- Test Case: BASIS-PATH-001
TEST.UNIT:OS_Interface
TEST.SUBPROGRAM:gulOS_TimerDeactivate
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 1
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.VALUE:OS_Interface.gulOS_TimerDeactivate.pTimer:<<malloc 1>>
TEST.END

-- Subprogram: gulOS_TimerGetInfo

-- Test Case: BASIS-PATH-001
TEST.UNIT:OS_Interface
TEST.SUBPROGRAM:gulOS_TimerGetInfo
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 1
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.VALUE:OS_Interface.gulOS_TimerGetInfo.pTimer:<<malloc 1>>
TEST.VALUE:OS_Interface.gulOS_TimerGetInfo.pszName:<<malloc 1>>
TEST.VALUE:OS_Interface.gulOS_TimerGetInfo.pulActive:<<malloc 1>>
TEST.VALUE:OS_Interface.gulOS_TimerGetInfo.pulRemainingTicks:<<malloc 1>>
TEST.VALUE:OS_Interface.gulOS_TimerGetInfo.pulRescheduleTicks:<<malloc 1>>
TEST.VALUE:OS_Interface.gulOS_TimerGetInfo.pTimerNext:<<malloc 1>>
TEST.END

-- Subprogram: gvOS_StartApplication

-- Test Case: BASIS-PATH-001-PARTIAL
TEST.UNIT:OS_Interface
TEST.SUBPROGRAM:gvOS_StartApplication
TEST.NEW
TEST.NAME:BASIS-PATH-001-PARTIAL
TEST.BASIS_PATH:1 of 11 (partial)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (1) if ((uint32_t)((UINT)0) != ulStatus) ==> FALSE
      (3) if (1U != gfSUPV_Initialize()) ==> FALSE
   Test Case Generation Notes:
      Cannot set variable to out of range value 1 in branch 3
TEST.END_NOTES:
TEST.VALUE:OS_Interface.gvOS_StartApplication.pvStartUnusedMemory:VECTORCAST_INT1
TEST.VALUE:uut_prototype_stubs.gulThreadNull_Initialize.return:0
TEST.VALUE:uut_prototype_stubs.gulHPThread_Initialize.return:0
TEST.VALUE:uut_prototype_stubs.gulIO_Initialize.return:0
TEST.VALUE:uut_prototype_stubs.gulMOTSENSE_Initialize.return:0
TEST.VALUE:uut_prototype_stubs.gulXCP_Initialize.return:0
TEST.VALUE:uut_prototype_stubs.gulIMM_CommunicationInitialize.return:0
TEST.VALUE:uut_prototype_stubs.gulAG_Common_Initialize.return:0
TEST.VALUE:uut_prototype_stubs.gulNvWriteThread_Initialize.return:0
TEST.VALUE:uut_prototype_stubs.gulFSThread_Initialize.return:0
TEST.END

-- Test Case: ulStatus_=_gulAG_Common_Initialize()
TEST.UNIT:OS_Interface
TEST.SUBPROGRAM:gvOS_StartApplication
TEST.NEW
TEST.NAME:ulStatus_=_gulAG_Common_Initialize()
TEST.VALUE:uut_prototype_stubs.gulAG_Common_Initialize.return:1
TEST.END

-- Test Case: ulStatus_=_gulFSThread_Initialize()
TEST.UNIT:OS_Interface
TEST.SUBPROGRAM:gvOS_StartApplication
TEST.NEW
TEST.NAME:ulStatus_=_gulFSThread_Initialize()
TEST.VALUE:uut_prototype_stubs.gulFSThread_Initialize.return:1
TEST.END

-- Test Case: ulStatus_=_gulHPThread_Initialize()
TEST.UNIT:OS_Interface
TEST.SUBPROGRAM:gvOS_StartApplication
TEST.NEW
TEST.NAME:ulStatus_=_gulHPThread_Initialize()
TEST.VALUE:uut_prototype_stubs.gulHPThread_Initialize.return:1
TEST.END

-- Test Case: ulStatus_=_gulIMM_CommunicationInitialize();
TEST.UNIT:OS_Interface
TEST.SUBPROGRAM:gvOS_StartApplication
TEST.NEW
TEST.NAME:ulStatus_=_gulIMM_CommunicationInitialize();
TEST.VALUE:uut_prototype_stubs.gulIMM_CommunicationInitialize.return:1
TEST.END

-- Test Case: ulStatus_=_gulIO_Initialize()
TEST.UNIT:OS_Interface
TEST.SUBPROGRAM:gvOS_StartApplication
TEST.NEW
TEST.NAME:ulStatus_=_gulIO_Initialize()
TEST.VALUE:uut_prototype_stubs.gulIO_Initialize.return:1
TEST.END

-- Test Case: ulStatus_=_gulMOTSENSE_Initialize()
TEST.UNIT:OS_Interface
TEST.SUBPROGRAM:gvOS_StartApplication
TEST.NEW
TEST.NAME:ulStatus_=_gulMOTSENSE_Initialize()
TEST.VALUE:uut_prototype_stubs.gulMOTSENSE_Initialize.return:1
TEST.END

-- Test Case: ulStatus_=_gulNvWriteThread_Initialize()
TEST.UNIT:OS_Interface
TEST.SUBPROGRAM:gvOS_StartApplication
TEST.NEW
TEST.NAME:ulStatus_=_gulNvWriteThread_Initialize()
TEST.VALUE:uut_prototype_stubs.gulNvWriteThread_Initialize.return:1
TEST.END

-- Test Case: ulStatus_=_gulThreadNull_Initialize();
TEST.UNIT:OS_Interface
TEST.SUBPROGRAM:gvOS_StartApplication
TEST.NEW
TEST.NAME:ulStatus_=_gulThreadNull_Initialize();
TEST.VALUE:uut_prototype_stubs.gulThreadNull_Initialize.return:1
TEST.END

-- Test Case: ulStatus_=_gulXCP_Initialize()
TEST.UNIT:OS_Interface
TEST.SUBPROGRAM:gvOS_StartApplication
TEST.NEW
TEST.NAME:ulStatus_=_gulXCP_Initialize()
TEST.VALUE:uut_prototype_stubs.gulXCP_Initialize.return:1
TEST.END

-- Subprogram: gvOS_ThreadRelinquish

-- Test Case: BASIS-PATH-001
TEST.UNIT:OS_Interface
TEST.SUBPROGRAM:gvOS_ThreadRelinquish
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 1
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.END

-- Subprogram: tx_application_define

-- Test Case: BASIS-PATH-001
TEST.UNIT:OS_Interface
TEST.SUBPROGRAM:tx_application_define
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 1
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.END
