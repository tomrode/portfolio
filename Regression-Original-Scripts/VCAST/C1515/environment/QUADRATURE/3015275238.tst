
-- Test Case: gvQUAD_Initialize.001
TEST.UNIT:Quadrature
TEST.SUBPROGRAM:gvQUAD_Initialize
TEST.NEW
TEST.NAME:gvQUAD_Initialize.001
TEST.SPECIALIZED
TEST.EXPECTED:Quadrature.<<GLOBAL>>.fQuadratureActive[QUAD_ECR1]:1
TEST.EXPECTED:Quadrature.<<GLOBAL>>.fQuadratureActive[QUAD_ECR3]:0
TEST.EXPECTED:Quadrature.<<GLOBAL>>.fQuadratureActive[QUAD_ECR4]:0
TEST.EXPECTED:Quadrature.<<GLOBAL>>.fQuadratureActive[QUAD_ECR5]:0
TEST.END
