
-- Subprogram: gvQUAD_Initialize

-- Test Case: gvQUAD_Initialize.001
TEST.UNIT:Quadrature
TEST.SUBPROGRAM:gvQUAD_Initialize
TEST.NEW
TEST.NAME:gvQUAD_Initialize.001
TEST.SPECIALIZED
TEST.STUB:Quadrature.gvQUAD_Process
TEST.STUB:Quadrature.vQUAD_GetFrequency
TEST.EXPECTED:Quadrature.<<GLOBAL>>.fQuadratureActive[QUAD_ECR1]:1
TEST.EXPECTED:Quadrature.<<GLOBAL>>.fQuadratureActive[QUAD_ECR3]:0
TEST.EXPECTED:Quadrature.<<GLOBAL>>.fQuadratureActive[QUAD_ECR4]:0
TEST.EXPECTED:Quadrature.<<GLOBAL>>.fQuadratureActive[QUAD_ECR5]:0
TEST.END

-- Subprogram: vQUAD_GetFrequency

-- Test Case: vQUAD_GetFrequency.002
TEST.UNIT:Quadrature
TEST.SUBPROGRAM:vQUAD_GetFrequency
TEST.NEW
TEST.NAME:vQUAD_GetFrequency.002
TEST.SPECIALIZED
TEST.VALUE:Quadrature.vQUAD_GetFrequency.eEncoder:QUAD_ECR1
TEST.VALUE:Quadrature.vQUAD_GetFrequency.pObj:<<malloc 1>>
TEST.VALUE:Quadrature.vQUAD_GetFrequency.pObj[0].slCounts:100
TEST.VALUE:Quadrature.vQUAD_GetFrequency.pObj[0].slLastCounts:100
TEST.VALUE:Quadrature.vQUAD_GetFrequency.pObj[0].ulTimeMs:10
TEST.VALUE:Quadrature.vQUAD_GetFrequency.ulCallRateMs:5
TEST.EXPECTED:Quadrature.vQUAD_GetFrequency.pObj[0].slLastCounts:100
TEST.EXPECTED:Quadrature.vQUAD_GetFrequency.pObj[0].rAvgHz:0.0
TEST.EXPECTED:Quadrature.vQUAD_GetFrequency.pObj[0].ulTimeMs:0
TEST.END

-- Test Case: vQUAD_GetFrequency.003
TEST.UNIT:Quadrature
TEST.SUBPROGRAM:vQUAD_GetFrequency
TEST.NEW
TEST.NAME:vQUAD_GetFrequency.003
TEST.SPECIALIZED
TEST.VALUE:Quadrature.vQUAD_GetFrequency.eEncoder:QUAD_ECR1
TEST.VALUE:Quadrature.vQUAD_GetFrequency.pObj:<<malloc 1>>
TEST.VALUE:Quadrature.vQUAD_GetFrequency.pObj[0].slCounts:100
TEST.VALUE:Quadrature.vQUAD_GetFrequency.pObj[0].slLastCounts:0
TEST.VALUE:Quadrature.vQUAD_GetFrequency.pObj[0].rAvgHz:0.0
TEST.VALUE:Quadrature.vQUAD_GetFrequency.pObj[0].ulTimeMs:0
TEST.VALUE:Quadrature.vQUAD_GetFrequency.ulCallRateMs:5
TEST.EXPECTED:Quadrature.vQUAD_GetFrequency.pObj[0].slLastCounts:0
TEST.EXPECTED:Quadrature.vQUAD_GetFrequency.pObj[0].rAvgHz:0.0
TEST.EXPECTED:Quadrature.vQUAD_GetFrequency.pObj[0].ulTimeMs:5
TEST.END

-- Test Case: vQUAD_GetFrequency.004
TEST.UNIT:Quadrature
TEST.SUBPROGRAM:vQUAD_GetFrequency
TEST.NEW
TEST.NAME:vQUAD_GetFrequency.004
TEST.SPECIALIZED
TEST.VALUE:Quadrature.vQUAD_GetFrequency.eEncoder:QUAD_ECR1
TEST.VALUE:Quadrature.vQUAD_GetFrequency.pObj:<<malloc 1>>
TEST.VALUE:Quadrature.vQUAD_GetFrequency.pObj[0].slCounts:10,-10,INT24_MIN_VALUE,INT24_MAX_VALUE
TEST.VALUE:Quadrature.vQUAD_GetFrequency.pObj[0].slLastCounts:(2)0,INT24_MAX_VALUE,INT24_MIN_VALUE
TEST.VALUE:Quadrature.vQUAD_GetFrequency.pObj[0].ulTimeMs:(4)5
TEST.VALUE:Quadrature.vQUAD_GetFrequency.ulCallRateMs:5
TEST.EXPECTED:Quadrature.vQUAD_GetFrequency.pObj[0].slLastCounts:10,-10,INT24_MIN_VALUE,INT24_MAX_VALUE
TEST.EXPECTED:Quadrature.vQUAD_GetFrequency.pObj[0].rAvgHz:1000.0,-1000.0,100.0,-100.0
TEST.EXPECTED:Quadrature.vQUAD_GetFrequency.pObj[0].ulTimeMs:0
TEST.END
