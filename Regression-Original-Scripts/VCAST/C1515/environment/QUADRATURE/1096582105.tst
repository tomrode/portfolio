
-- Unit: Quadrature

-- Subprogram: gvQUAD_Initialize

-- Test Case: gvQUAD_Initialize.002
TEST.UNIT:Quadrature
TEST.SUBPROGRAM:gvQUAD_Initialize
TEST.NEW
TEST.NAME:gvQUAD_Initialize.002
TEST.SPECIALIZED
TEST.VALUE:uut_prototype_stubs.geQUAD_Initialize.return:QUAD_INVALID_OBJECT,QUAD_INVALID_CHANNEL,QUAD_INVALID_PIN
TEST.EXPECTED:Quadrature.<<GLOBAL>>.fQuadratureActive[QUAD_ECR1]:0
TEST.EXPECTED:Quadrature.<<GLOBAL>>.fQuadratureActive[QUAD_ECR3]:0
TEST.EXPECTED:Quadrature.<<GLOBAL>>.fQuadratureActive[QUAD_ECR4]:0
TEST.EXPECTED:Quadrature.<<GLOBAL>>.fQuadratureActive[QUAD_ECR5]:0
TEST.END

-- Subprogram: gvQUAD_Process

-- Test Case: gvQUAD_Process.001
TEST.UNIT:Quadrature
TEST.SUBPROGRAM:gvQUAD_Process
TEST.NEW
TEST.NAME:gvQUAD_Process.001
TEST.SPECIALIZED
TEST.STUB:Quadrature.vQUAD_GetFrequency
TEST.VALUE:Quadrature.<<GLOBAL>>.gQuadratureInput[QUAD_ECR1].slCounts:0
TEST.VALUE:Quadrature.<<GLOBAL>>.gQuadratureInput[QUAD_ECR1].rAvgHz:0.0
TEST.VALUE:Quadrature.<<GLOBAL>>.gQuadratureInput[QUAD_ECR3].slCounts:0
TEST.VALUE:Quadrature.<<GLOBAL>>.gQuadratureInput[QUAD_ECR3].rAvgHz:0.0
TEST.VALUE:Quadrature.<<GLOBAL>>.gQuadratureInput[QUAD_ECR4].slCounts:0
TEST.VALUE:Quadrature.<<GLOBAL>>.gQuadratureInput[QUAD_ECR4].rAvgHz:0.0
TEST.VALUE:Quadrature.<<GLOBAL>>.gQuadratureInput[QUAD_ECR5].slCounts:0
TEST.VALUE:Quadrature.<<GLOBAL>>.gQuadratureInput[QUAD_ECR5].rAvgHz:0.0
TEST.VALUE:Quadrature.<<GLOBAL>>.QuadratureDiagnosticAND[QUAD_ECR1].eState:GPIO_STATE_LOW
TEST.VALUE:Quadrature.<<GLOBAL>>.QuadratureDiagnosticAND[QUAD_ECR3].eState:GPIO_STATE_LOW
TEST.VALUE:Quadrature.<<GLOBAL>>.QuadratureDiagnosticAND[QUAD_ECR4].eState:GPIO_STATE_LOW
TEST.VALUE:Quadrature.<<GLOBAL>>.QuadratureDiagnosticAND[QUAD_ECR5].eState:GPIO_STATE_LOW
TEST.VALUE:Quadrature.<<GLOBAL>>.QuadratureDiagnosticOR[QUAD_ECR1].eState:GPIO_STATE_LOW
TEST.VALUE:Quadrature.<<GLOBAL>>.QuadratureDiagnosticOR[QUAD_ECR3].eState:GPIO_STATE_LOW
TEST.VALUE:Quadrature.<<GLOBAL>>.QuadratureDiagnosticOR[QUAD_ECR4].eState:GPIO_STATE_LOW
TEST.VALUE:Quadrature.<<GLOBAL>>.QuadratureDiagnosticOR[QUAD_ECR5].eState:GPIO_STATE_LOW
TEST.VALUE:Quadrature.<<GLOBAL>>.fQuadratureActive[QUAD_ECR1]:1
TEST.VALUE:Quadrature.<<GLOBAL>>.fQuadratureActive[QUAD_ECR3]:0
TEST.VALUE:Quadrature.<<GLOBAL>>.fQuadratureActive[QUAD_ECR4]:1
TEST.VALUE:Quadrature.<<GLOBAL>>.fQuadratureActive[QUAD_ECR5]:0
TEST.VALUE:Quadrature.gvQUAD_Process.ulCallRateMs:5
TEST.VALUE:Quadrature.vQUAD_GetFrequency.pObj[0].rAvgHz:12.0
TEST.VALUE:uut_prototype_stubs.geGPIO_GetInputState.pObject[0].eState:GPIO_STATE_HIGH
TEST.VALUE:uut_prototype_stubs.geQUAD_GetCount.pObj[0].slCounts:125
TEST.VALUE:uut_prototype_stubs.geQUAD_GetCount.return:QUAD_OKAY
TEST.EXPECTED:Quadrature.<<GLOBAL>>.gQuadratureInput[QUAD_ECR1].slCounts:125
TEST.EXPECTED:Quadrature.<<GLOBAL>>.gQuadratureInput[QUAD_ECR1].rAvgHz:12.0
TEST.EXPECTED:Quadrature.<<GLOBAL>>.gQuadratureInput[QUAD_ECR3].slCounts:0
TEST.EXPECTED:Quadrature.<<GLOBAL>>.gQuadratureInput[QUAD_ECR3].rAvgHz:0.0
TEST.EXPECTED:Quadrature.<<GLOBAL>>.gQuadratureInput[QUAD_ECR4].slCounts:125
TEST.EXPECTED:Quadrature.<<GLOBAL>>.gQuadratureInput[QUAD_ECR4].rAvgHz:12.0
TEST.EXPECTED:Quadrature.<<GLOBAL>>.gQuadratureInput[QUAD_ECR5].slCounts:0
TEST.EXPECTED:Quadrature.<<GLOBAL>>.gQuadratureInput[QUAD_ECR5].rAvgHz:0.0
TEST.EXPECTED:Quadrature.<<GLOBAL>>.QuadratureDiagnosticAND[QUAD_ECR1].eState:GPIO_STATE_HIGH
TEST.EXPECTED:Quadrature.<<GLOBAL>>.QuadratureDiagnosticAND[QUAD_ECR3].eState:GPIO_STATE_LOW
TEST.EXPECTED:Quadrature.<<GLOBAL>>.QuadratureDiagnosticAND[QUAD_ECR4].eState:GPIO_STATE_HIGH
TEST.EXPECTED:Quadrature.<<GLOBAL>>.QuadratureDiagnosticAND[QUAD_ECR5].eState:GPIO_STATE_LOW
TEST.EXPECTED:Quadrature.<<GLOBAL>>.QuadratureDiagnosticOR[QUAD_ECR1].eState:GPIO_STATE_HIGH
TEST.EXPECTED:Quadrature.<<GLOBAL>>.QuadratureDiagnosticOR[QUAD_ECR3].eState:GPIO_STATE_LOW
TEST.EXPECTED:Quadrature.<<GLOBAL>>.QuadratureDiagnosticOR[QUAD_ECR4].eState:GPIO_STATE_HIGH
TEST.EXPECTED:Quadrature.<<GLOBAL>>.QuadratureDiagnosticOR[QUAD_ECR5].eState:GPIO_STATE_LOW
TEST.END

-- Test Case: gvQUAD_Process.002
TEST.UNIT:Quadrature
TEST.SUBPROGRAM:gvQUAD_Process
TEST.NEW
TEST.NAME:gvQUAD_Process.002
TEST.SPECIALIZED
TEST.STUB:Quadrature.vQUAD_GetFrequency
TEST.VALUE:Quadrature.<<GLOBAL>>.gQuadratureInput[QUAD_ECR1].slCounts:0
TEST.VALUE:Quadrature.<<GLOBAL>>.gQuadratureInput[QUAD_ECR1].rAvgHz:0.0
TEST.VALUE:Quadrature.<<GLOBAL>>.gQuadratureInput[QUAD_ECR3].slCounts:0
TEST.VALUE:Quadrature.<<GLOBAL>>.gQuadratureInput[QUAD_ECR3].rAvgHz:0.0
TEST.VALUE:Quadrature.<<GLOBAL>>.gQuadratureInput[QUAD_ECR4].slCounts:0
TEST.VALUE:Quadrature.<<GLOBAL>>.gQuadratureInput[QUAD_ECR4].rAvgHz:0.0
TEST.VALUE:Quadrature.<<GLOBAL>>.gQuadratureInput[QUAD_ECR5].slCounts:0
TEST.VALUE:Quadrature.<<GLOBAL>>.gQuadratureInput[QUAD_ECR5].rAvgHz:0.0
TEST.VALUE:Quadrature.<<GLOBAL>>.QuadratureDiagnosticAND[QUAD_ECR1].eState:GPIO_STATE_LOW
TEST.VALUE:Quadrature.<<GLOBAL>>.QuadratureDiagnosticAND[QUAD_ECR3].eState:GPIO_STATE_LOW
TEST.VALUE:Quadrature.<<GLOBAL>>.QuadratureDiagnosticAND[QUAD_ECR4].eState:GPIO_STATE_LOW
TEST.VALUE:Quadrature.<<GLOBAL>>.QuadratureDiagnosticAND[QUAD_ECR5].eState:GPIO_STATE_LOW
TEST.VALUE:Quadrature.<<GLOBAL>>.QuadratureDiagnosticOR[QUAD_ECR1].eState:GPIO_STATE_LOW
TEST.VALUE:Quadrature.<<GLOBAL>>.QuadratureDiagnosticOR[QUAD_ECR3].eState:GPIO_STATE_LOW
TEST.VALUE:Quadrature.<<GLOBAL>>.QuadratureDiagnosticOR[QUAD_ECR4].eState:GPIO_STATE_LOW
TEST.VALUE:Quadrature.<<GLOBAL>>.QuadratureDiagnosticOR[QUAD_ECR5].eState:GPIO_STATE_LOW
TEST.VALUE:Quadrature.<<GLOBAL>>.fQuadratureActive[QUAD_ECR1]:1
TEST.VALUE:Quadrature.<<GLOBAL>>.fQuadratureActive[QUAD_ECR3]:0
TEST.VALUE:Quadrature.<<GLOBAL>>.fQuadratureActive[QUAD_ECR4]:1
TEST.VALUE:Quadrature.<<GLOBAL>>.fQuadratureActive[QUAD_ECR5]:0
TEST.VALUE:Quadrature.gvQUAD_Process.ulCallRateMs:5
TEST.VALUE:Quadrature.vQUAD_GetFrequency.pObj[0].rAvgHz:12.0
TEST.VALUE:uut_prototype_stubs.geGPIO_GetInputState.pObject[0].eState:GPIO_STATE_HIGH
TEST.VALUE:uut_prototype_stubs.geQUAD_GetCount.pObj[0].slCounts:-1
TEST.VALUE:uut_prototype_stubs.geQUAD_GetCount.return:QUAD_INVALID_OBJECT
TEST.EXPECTED:Quadrature.<<GLOBAL>>.gQuadratureInput[QUAD_ECR1].slCounts:-1
TEST.EXPECTED:Quadrature.<<GLOBAL>>.gQuadratureInput[QUAD_ECR1].rAvgHz:0.0
TEST.EXPECTED:Quadrature.<<GLOBAL>>.gQuadratureInput[QUAD_ECR3].slCounts:0
TEST.EXPECTED:Quadrature.<<GLOBAL>>.gQuadratureInput[QUAD_ECR3].rAvgHz:0.0
TEST.EXPECTED:Quadrature.<<GLOBAL>>.gQuadratureInput[QUAD_ECR4].slCounts:-1
TEST.EXPECTED:Quadrature.<<GLOBAL>>.gQuadratureInput[QUAD_ECR4].rAvgHz:0.0
TEST.EXPECTED:Quadrature.<<GLOBAL>>.gQuadratureInput[QUAD_ECR5].slCounts:0
TEST.EXPECTED:Quadrature.<<GLOBAL>>.gQuadratureInput[QUAD_ECR5].rAvgHz:0.0
TEST.EXPECTED:Quadrature.<<GLOBAL>>.QuadratureDiagnosticAND[QUAD_ECR1].eState:GPIO_STATE_HIGH
TEST.EXPECTED:Quadrature.<<GLOBAL>>.QuadratureDiagnosticAND[QUAD_ECR3].eState:GPIO_STATE_LOW
TEST.EXPECTED:Quadrature.<<GLOBAL>>.QuadratureDiagnosticAND[QUAD_ECR4].eState:GPIO_STATE_HIGH
TEST.EXPECTED:Quadrature.<<GLOBAL>>.QuadratureDiagnosticAND[QUAD_ECR5].eState:GPIO_STATE_LOW
TEST.EXPECTED:Quadrature.<<GLOBAL>>.QuadratureDiagnosticOR[QUAD_ECR1].eState:GPIO_STATE_HIGH
TEST.EXPECTED:Quadrature.<<GLOBAL>>.QuadratureDiagnosticOR[QUAD_ECR3].eState:GPIO_STATE_LOW
TEST.EXPECTED:Quadrature.<<GLOBAL>>.QuadratureDiagnosticOR[QUAD_ECR4].eState:GPIO_STATE_HIGH
TEST.EXPECTED:Quadrature.<<GLOBAL>>.QuadratureDiagnosticOR[QUAD_ECR5].eState:GPIO_STATE_LOW
TEST.END

-- Subprogram: vQUAD_GetFrequency

-- Test Case: vQUAD_GetFrequency.001
TEST.UNIT:Quadrature
TEST.SUBPROGRAM:vQUAD_GetFrequency
TEST.NEW
TEST.NAME:vQUAD_GetFrequency.001
TEST.SPECIALIZED
TEST.VALUE:Quadrature.vQUAD_GetFrequency.eEncoder:QUAD_ECR5
TEST.VALUE:Quadrature.vQUAD_GetFrequency.pObj:<<malloc 1>>
TEST.VALUE:Quadrature.vQUAD_GetFrequency.pObj[0].slCounts:100
TEST.VALUE:Quadrature.vQUAD_GetFrequency.pObj[0].slLastCounts:0
TEST.VALUE:Quadrature.vQUAD_GetFrequency.pObj[0].rAvgHz:0.0
TEST.EXPECTED:Quadrature.vQUAD_GetFrequency.pObj[0].slLastCounts:0
TEST.EXPECTED:Quadrature.vQUAD_GetFrequency.pObj[0].rAvgHz:0.0
TEST.END

-- Test Case: vQUAD_GetFrequency.002
TEST.UNIT:Quadrature
TEST.SUBPROGRAM:vQUAD_GetFrequency
TEST.NEW
TEST.NAME:vQUAD_GetFrequency.002
TEST.SPECIALIZED
TEST.VALUE:Quadrature.vQUAD_GetFrequency.pObj:<<null>>
TEST.EXPECTED:Quadrature.vQUAD_GetFrequency.pObj:<<null>>
TEST.END
