-- VectorCAST 6.4.3 (03/08/18)
-- Test Case Script
-- 
-- Environment    : IO_THREAD
-- Unit(s) Under Test: io_thread param
-- 
-- Script Features
TEST.SCRIPT_FEATURE:C_DIRECT_ARRAY_INDEXING
TEST.SCRIPT_FEATURE:CPP_CLASS_OBJECT_REVISION
TEST.SCRIPT_FEATURE:MULTIPLE_UUT_SUPPORT
TEST.SCRIPT_FEATURE:MIXED_CASE_NAMES
TEST.SCRIPT_FEATURE:STATIC_HEADER_FUNCS_IN_UUTS
--

-- Unit: io_thread

-- Subprogram: gfIO_UndervoltageISR_Status

-- Test Case: gfIO_UndervoltageISR_Status.001
TEST.UNIT:io_thread
TEST.SUBPROGRAM:gfIO_UndervoltageISR_Status
TEST.NEW
TEST.NAME:gfIO_UndervoltageISR_Status.001
TEST.VALUE:io_thread.<<GLOBAL>>.xfUndervoltageActive:FALSE,TRUE
TEST.EXPECTED:io_thread.gfIO_UndervoltageISR_Status.return:FALSE,TRUE
TEST.END

-- Subprogram: gulIO_Initialize

-- Test Case: gulIO_Initialize.001
TEST.UNIT:io_thread
TEST.SUBPROGRAM:gulIO_Initialize
TEST.NEW
TEST.NAME:gulIO_Initialize.001
TEST.VALUE:uut_prototype_stubs.gulOS_ThreadCreate.return:0
TEST.EXPECTED:io_thread.<<GLOBAL>>.gulIoThreadRateUs:5000
TEST.EXPECTED:io_thread.gulIO_Initialize.return:0
TEST.END

-- Test Case: gulIO_Initialize.002
TEST.UNIT:io_thread
TEST.SUBPROGRAM:gulIO_Initialize
TEST.NEW
TEST.NAME:gulIO_Initialize.002
TEST.VALUE:uut_prototype_stubs.gulOS_ThreadCreate.return:1
TEST.EXPECTED:io_thread.<<GLOBAL>>.gulIoThreadRateUs:5000
TEST.EXPECTED:io_thread.gulIO_Initialize.return:1
TEST.END

-- Subprogram: vIO_Thread_TimerEntry

-- Test Case: vIO_Thread_TimerEntry.001
TEST.UNIT:io_thread
TEST.SUBPROGRAM:vIO_Thread_TimerEntry
TEST.NEW
TEST.NAME:vIO_Thread_TimerEntry.001
TEST.VALUE:uut_prototype_stubs.gulOS_ThreadResume.return:FALSE
TEST.END
