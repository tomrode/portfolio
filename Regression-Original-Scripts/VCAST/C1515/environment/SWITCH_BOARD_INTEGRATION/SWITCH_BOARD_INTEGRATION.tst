-- VectorCAST 6.4q (03/02/17)
-- Test Case Script
-- 
-- Environment    : SWITCH_BOARD_INTEGRATION
-- Unit(s) Under Test: ag_common ag_input_router ag_output_router param switchboard
-- 
-- Script Features
TEST.SCRIPT_FEATURE:C_DIRECT_ARRAY_INDEXING
TEST.SCRIPT_FEATURE:CPP_CLASS_OBJECT_REVISION
TEST.SCRIPT_FEATURE:MULTIPLE_UUT_SUPPORT
TEST.SCRIPT_FEATURE:MIXED_CASE_NAMES
TEST.SCRIPT_FEATURE:STATIC_HEADER_FUNCS_IN_UUTS
--

-- Unit: ag_input_router

-- Subprogram: gvAG_InputRouter_Process

-- Test Case: gvAG_InputRouter_Process.001
TEST.UNIT:ag_input_router
TEST.SUBPROGRAM:gvAG_InputRouter_Process
TEST.NEW
TEST.NAME:gvAG_InputRouter_Process.001
TEST.COMPOUND_ONLY
TEST.VALUE:ag_input_router.gvAG_InputRouter_Process.eProcess:PROCESS_SYS_MODEL
TEST.EXPECTED:ag_input_router.gvAG_InputRouter_Process.eProcess:PROCESS_SYS_MODEL
TEST.END

-- Unit: switchboard

-- Subprogram: gvSB_RouteMatrixEntry

-- Test Case: Use_for_Integration_SB_Boolean
TEST.UNIT:switchboard
TEST.SUBPROGRAM:gvSB_RouteMatrixEntry
TEST.NEW
TEST.NAME:Use_for_Integration_SB_Boolean
TEST.COMPOUND_ONLY
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pDestCfg:<<malloc 1>>
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pDestCfg[0].eDataType:SB_BOOLEAN
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pDestCfg[0].eBitType:SB_NO_BIT_MAN
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pvSource:VECTORCAST_INT1
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pSourceCfg:<<malloc 1>>
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pSourceCfg[0].eDataType:SB_BOOLEAN
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pSourceCfg[0].eBitType:SB_NO_BIT_MAN
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pScaleFactors:<<malloc 1>>
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pScaleFactors[0]:<<malloc 1>>
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pScaleFactors[0][0].rGain:100.0
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pScaleFactors[0][0].rOffset:20.100
TEST.EXPECTED:switchboard.gvSB_RouteMatrixEntry.pDestCfg[0].eDataType:SB_BOOLEAN
TEST.EXPECTED:switchboard.gvSB_RouteMatrixEntry.pDestCfg[0].eBitType:SB_NO_BIT_MAN
TEST.EXPECTED:switchboard.gvSB_RouteMatrixEntry.pSourceCfg[0].eDataType:SB_BOOLEAN
TEST.EXPECTED:switchboard.gvSB_RouteMatrixEntry.pSourceCfg[0].eBitType:SB_NO_BIT_MAN
TEST.EXPECTED:switchboard.gvSB_RouteMatrixEntry.pScaleFactors[0][0].rGain:100.0
TEST.EXPECTED:switchboard.gvSB_RouteMatrixEntry.pScaleFactors[0][0].rOffset:20.100
TEST.END

-- Test Case: Use_for_Integration_SB_Float32
TEST.UNIT:switchboard
TEST.SUBPROGRAM:gvSB_RouteMatrixEntry
TEST.NEW
TEST.NAME:Use_for_Integration_SB_Float32
TEST.COMPOUND_ONLY
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pDestCfg:<<malloc 1>>
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pDestCfg[0].eDataType:SB_FLOAT32
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pDestCfg[0].eBitType:SB_LEFT_RMW
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pvSource:VECTORCAST_INT1
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pSourceCfg:<<malloc 1>>
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pSourceCfg[0].eDataType:SB_FLOAT32
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pSourceCfg[0].eBitType:SB_LEFT_RMW
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pScaleFactors:<<malloc 1>>
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pScaleFactors[0]:<<malloc 1>>
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pScaleFactors[0][0].rGain:1.0
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pScaleFactors[0][0].rOffset:0.0
TEST.EXPECTED:switchboard.gvSB_RouteMatrixEntry.pDestCfg[0].eDataType:SB_FLOAT32
TEST.EXPECTED:switchboard.gvSB_RouteMatrixEntry.pDestCfg[0].eBitType:SB_LEFT_RMW
TEST.EXPECTED:switchboard.gvSB_RouteMatrixEntry.pSourceCfg[0].eDataType:SB_FLOAT32
TEST.EXPECTED:switchboard.gvSB_RouteMatrixEntry.pSourceCfg[0].eBitType:SB_LEFT_RMW
TEST.EXPECTED:switchboard.gvSB_RouteMatrixEntry.pSourceCfg[0].ulMask:0
TEST.EXPECTED:switchboard.gvSB_RouteMatrixEntry.pSourceCfg[0].ulShiftBits:0
TEST.EXPECTED:switchboard.gvSB_RouteMatrixEntry.pScaleFactors[0][0].rGain:1.0
TEST.EXPECTED:switchboard.gvSB_RouteMatrixEntry.pScaleFactors[0][0].rOffset:0.0
TEST.END

-- Test Case: Use_for_Integration_SB_Int16
TEST.UNIT:switchboard
TEST.SUBPROGRAM:gvSB_RouteMatrixEntry
TEST.NEW
TEST.NAME:Use_for_Integration_SB_Int16
TEST.COMPOUND_ONLY
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pDestCfg:<<malloc 1>>
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pDestCfg[0].eDataType:SB_INT16
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pDestCfg[0].eBitType:SB_NO_BIT_MAN
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pvSource:VECTORCAST_INT1
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pSourceCfg:<<malloc 1>>
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pSourceCfg[0].eDataType:SB_INT16
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pSourceCfg[0].eBitType:SB_RIGHT
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pScaleFactors:<<malloc 1>>
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pScaleFactors[0]:<<malloc 1>>
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pScaleFactors[0][0].rGain:1.0
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pScaleFactors[0][0].rOffset:0.0
TEST.EXPECTED:switchboard.gvSB_RouteMatrixEntry.pDestCfg[0].eDataType:SB_INT16
TEST.EXPECTED:switchboard.gvSB_RouteMatrixEntry.pDestCfg[0].eBitType:SB_NO_BIT_MAN
TEST.EXPECTED:switchboard.gvSB_RouteMatrixEntry.pSourceCfg[0].eDataType:SB_INT16
TEST.EXPECTED:switchboard.gvSB_RouteMatrixEntry.pSourceCfg[0].eBitType:SB_RIGHT
TEST.EXPECTED:switchboard.gvSB_RouteMatrixEntry.pSourceCfg[0].ulMask:0
TEST.EXPECTED:switchboard.gvSB_RouteMatrixEntry.pSourceCfg[0].ulShiftBits:0
TEST.EXPECTED:switchboard.gvSB_RouteMatrixEntry.pScaleFactors[0][0].rGain:1.0
TEST.EXPECTED:switchboard.gvSB_RouteMatrixEntry.pScaleFactors[0][0].rOffset:0.0
TEST.END

-- Test Case: Use_for_Integration_SB_Int32
TEST.UNIT:switchboard
TEST.SUBPROGRAM:gvSB_RouteMatrixEntry
TEST.NEW
TEST.NAME:Use_for_Integration_SB_Int32
TEST.COMPOUND_ONLY
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pDestCfg:<<malloc 1>>
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pDestCfg[0].eDataType:SB_INT32
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pDestCfg[0].eBitType:SB_NO_BIT_MAN
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pvSource:VECTORCAST_INT1
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pSourceCfg:<<malloc 1>>
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pSourceCfg[0].eDataType:SB_INT32
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pSourceCfg[0].eBitType:SB_LEFT
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pScaleFactors:<<malloc 1>>
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pScaleFactors[0]:<<malloc 1>>
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pScaleFactors[0][0].rGain:1.0
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pScaleFactors[0][0].rOffset:0.0
TEST.EXPECTED:switchboard.gvSB_RouteMatrixEntry.pDestCfg[0].eDataType:SB_INT32
TEST.EXPECTED:switchboard.gvSB_RouteMatrixEntry.pDestCfg[0].eBitType:SB_NO_BIT_MAN
TEST.EXPECTED:switchboard.gvSB_RouteMatrixEntry.pSourceCfg[0].eDataType:SB_INT32
TEST.EXPECTED:switchboard.gvSB_RouteMatrixEntry.pSourceCfg[0].eBitType:SB_LEFT
TEST.EXPECTED:switchboard.gvSB_RouteMatrixEntry.pSourceCfg[0].ulMask:0
TEST.EXPECTED:switchboard.gvSB_RouteMatrixEntry.pSourceCfg[0].ulShiftBits:0
TEST.EXPECTED:switchboard.gvSB_RouteMatrixEntry.pScaleFactors[0][0].rGain:1.0
TEST.EXPECTED:switchboard.gvSB_RouteMatrixEntry.pScaleFactors[0][0].rOffset:0.0
TEST.END

-- Test Case: Use_for_Integration_SB_Int8
TEST.UNIT:switchboard
TEST.SUBPROGRAM:gvSB_RouteMatrixEntry
TEST.NEW
TEST.NAME:Use_for_Integration_SB_Int8
TEST.COMPOUND_ONLY
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pDestCfg:<<malloc 1>>
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pDestCfg[0].eDataType:SB_INT8
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pDestCfg[0].eBitType:SB_NO_BIT_MAN
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pvSource:VECTORCAST_INT1
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pSourceCfg:<<malloc 1>>
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pSourceCfg[0].eDataType:SB_INT8
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pSourceCfg[0].eBitType:SB_NO_BIT_MAN
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pScaleFactors:<<malloc 1>>
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pScaleFactors[0]:<<malloc 1>>
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pScaleFactors[0][0].rGain:10.0
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pScaleFactors[0][0].rOffset:0.0
TEST.EXPECTED:switchboard.gvSB_RouteMatrixEntry.pDestCfg[0].eDataType:SB_INT8
TEST.EXPECTED:switchboard.gvSB_RouteMatrixEntry.pDestCfg[0].eBitType:SB_NO_BIT_MAN
TEST.EXPECTED:switchboard.gvSB_RouteMatrixEntry.pDestCfg[0].ulMask:0
TEST.EXPECTED:switchboard.gvSB_RouteMatrixEntry.pDestCfg[0].ulShiftBits:0
TEST.EXPECTED:switchboard.gvSB_RouteMatrixEntry.pDestCfg[0].ulScaleFactor:0
TEST.EXPECTED:switchboard.gvSB_RouteMatrixEntry.pSourceCfg[0].eDataType:SB_INT8
TEST.EXPECTED:switchboard.gvSB_RouteMatrixEntry.pSourceCfg[0].eBitType:SB_NO_BIT_MAN
TEST.EXPECTED:switchboard.gvSB_RouteMatrixEntry.pSourceCfg[0].ulMask:0
TEST.EXPECTED:switchboard.gvSB_RouteMatrixEntry.pSourceCfg[0].ulShiftBits:0
TEST.EXPECTED:switchboard.gvSB_RouteMatrixEntry.pSourceCfg[0].ulScaleFactor:0
TEST.EXPECTED:switchboard.gvSB_RouteMatrixEntry.pScaleFactors[0][0].rGain:10.0
TEST.EXPECTED:switchboard.gvSB_RouteMatrixEntry.pScaleFactors[0][0].rOffset:0.0
TEST.END

-- Test Case: Use_for_Integration_SB_Uint16
TEST.UNIT:switchboard
TEST.SUBPROGRAM:gvSB_RouteMatrixEntry
TEST.NEW
TEST.NAME:Use_for_Integration_SB_Uint16
TEST.COMPOUND_ONLY
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pDestCfg:<<malloc 1>>
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pDestCfg[0].eDataType:SB_UINT16
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pDestCfg[0].eBitType:SB_LEFT_RMW
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pvSource:VECTORCAST_INT1
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pSourceCfg:<<malloc 1>>
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pSourceCfg[0].eDataType:SB_UINT16
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pSourceCfg[0].eBitType:SB_LEFT
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pScaleFactors:<<malloc 1>>
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pScaleFactors[0]:<<malloc 1>>
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pScaleFactors[0][0].rGain:1.0
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pScaleFactors[0][0].rOffset:0.0
TEST.EXPECTED:switchboard.gvSB_RouteMatrixEntry.pDestCfg[0].eDataType:SB_UINT16
TEST.EXPECTED:switchboard.gvSB_RouteMatrixEntry.pDestCfg[0].eBitType:SB_LEFT_RMW
TEST.EXPECTED:switchboard.gvSB_RouteMatrixEntry.pSourceCfg[0].eDataType:SB_UINT16
TEST.EXPECTED:switchboard.gvSB_RouteMatrixEntry.pSourceCfg[0].eBitType:SB_LEFT
TEST.EXPECTED:switchboard.gvSB_RouteMatrixEntry.pSourceCfg[0].ulMask:0
TEST.EXPECTED:switchboard.gvSB_RouteMatrixEntry.pSourceCfg[0].ulShiftBits:0
TEST.EXPECTED:switchboard.gvSB_RouteMatrixEntry.pScaleFactors[0][0].rGain:1.0
TEST.EXPECTED:switchboard.gvSB_RouteMatrixEntry.pScaleFactors[0][0].rOffset:0.0
TEST.END

-- Test Case: Use_for_Integration_SB_Uint32
TEST.UNIT:switchboard
TEST.SUBPROGRAM:gvSB_RouteMatrixEntry
TEST.NEW
TEST.NAME:Use_for_Integration_SB_Uint32
TEST.COMPOUND_ONLY
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pDestCfg:<<malloc 1>>
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pDestCfg[0].eDataType:SB_UINT32
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pDestCfg[0].eBitType:SB_RIGHT
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pvSource:VECTORCAST_INT1
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pSourceCfg:<<malloc 1>>
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pSourceCfg[0].eDataType:SB_UINT32
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pSourceCfg[0].eBitType:SB_LEFT
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pSourceCfg[0].ulMask:1
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pScaleFactors:<<malloc 1>>
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pScaleFactors[0]:<<malloc 1>>
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pScaleFactors[0][0].rGain:20.0
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pScaleFactors[0][0].rOffset:0.0
TEST.EXPECTED:switchboard.gvSB_RouteMatrixEntry.pDestCfg[0].eDataType:SB_UINT32
TEST.EXPECTED:switchboard.gvSB_RouteMatrixEntry.pDestCfg[0].eBitType:SB_RIGHT
TEST.EXPECTED:switchboard.gvSB_RouteMatrixEntry.pDestCfg[0].ulShiftBits:0
TEST.EXPECTED:switchboard.gvSB_RouteMatrixEntry.pSourceCfg[0].eDataType:SB_UINT32
TEST.EXPECTED:switchboard.gvSB_RouteMatrixEntry.pSourceCfg[0].eBitType:SB_LEFT
TEST.EXPECTED:switchboard.gvSB_RouteMatrixEntry.pSourceCfg[0].ulMask:1
TEST.EXPECTED:switchboard.gvSB_RouteMatrixEntry.pSourceCfg[0].ulShiftBits:0
TEST.EXPECTED:switchboard.gvSB_RouteMatrixEntry.pScaleFactors[0][0].rGain:20.0
TEST.EXPECTED:switchboard.gvSB_RouteMatrixEntry.pScaleFactors[0][0].rOffset:0.0
TEST.END

-- Test Case: Use_for_Integration_SB_Uint8
TEST.UNIT:switchboard
TEST.SUBPROGRAM:gvSB_RouteMatrixEntry
TEST.NEW
TEST.NAME:Use_for_Integration_SB_Uint8
TEST.COMPOUND_ONLY
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pDestCfg:<<malloc 1>>
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pDestCfg[0].eDataType:SB_UINT8
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pDestCfg[0].eBitType:SB_RIGHT_RMW
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pDestCfg[0].ulMask:3
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pDestCfg[0].ulShiftBits:2
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pDestCfg[0].ulScaleFactor:5
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pvSource:VECTORCAST_INT1
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pSourceCfg:<<malloc 1>>
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pSourceCfg[0].eDataType:SB_UINT8
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pSourceCfg[0].eBitType:SB_RIGHT_RMW
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pSourceCfg[0].ulMask:3
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pSourceCfg[0].ulShiftBits:2
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pSourceCfg[0].ulScaleFactor:5
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pScaleFactors:<<malloc 1>>
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pScaleFactors[0]:<<malloc 1>>
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pScaleFactors[0][0].rGain:1.0
TEST.VALUE:switchboard.gvSB_RouteMatrixEntry.pScaleFactors[0][0].rOffset:0.0
TEST.EXPECTED:switchboard.gvSB_RouteMatrixEntry.pDestCfg[0].eDataType:SB_UINT8
TEST.EXPECTED:switchboard.gvSB_RouteMatrixEntry.pDestCfg[0].eBitType:SB_RIGHT_RMW
TEST.EXPECTED:switchboard.gvSB_RouteMatrixEntry.pDestCfg[0].ulMask:3
TEST.EXPECTED:switchboard.gvSB_RouteMatrixEntry.pDestCfg[0].ulShiftBits:2
TEST.EXPECTED:switchboard.gvSB_RouteMatrixEntry.pDestCfg[0].ulScaleFactor:5
TEST.EXPECTED:switchboard.gvSB_RouteMatrixEntry.pSourceCfg[0].eDataType:SB_UINT8
TEST.EXPECTED:switchboard.gvSB_RouteMatrixEntry.pSourceCfg[0].eBitType:SB_RIGHT_RMW
TEST.EXPECTED:switchboard.gvSB_RouteMatrixEntry.pSourceCfg[0].ulMask:3
TEST.EXPECTED:switchboard.gvSB_RouteMatrixEntry.pSourceCfg[0].ulShiftBits:2
TEST.EXPECTED:switchboard.gvSB_RouteMatrixEntry.pSourceCfg[0].ulScaleFactor:5
TEST.EXPECTED:switchboard.gvSB_RouteMatrixEntry.pScaleFactors[0][0].rGain:1.0
TEST.EXPECTED:switchboard.gvSB_RouteMatrixEntry.pScaleFactors[0][0].rOffset:0.0
TEST.END

-- Subprogram: rSB_ApplyScaling

-- Test Case: Use_For_Integration_SB_Float32
TEST.UNIT:switchboard
TEST.SUBPROGRAM:rSB_ApplyScaling
TEST.NEW
TEST.NAME:Use_For_Integration_SB_Float32
TEST.COMPOUND_ONLY
TEST.BASIS_PATH:1 of 1
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.VALUE:switchboard.rSB_ApplyScaling.rData:0.0
TEST.VALUE:switchboard.rSB_ApplyScaling.pSF:<<malloc 1>>
TEST.VALUE:switchboard.rSB_ApplyScaling.pSF[0].rGain:1.0
TEST.VALUE:switchboard.rSB_ApplyScaling.pSF[0].rOffset:0.0
TEST.EXPECTED:switchboard.rSB_ApplyScaling.rData:0.0
TEST.EXPECTED:switchboard.rSB_ApplyScaling.pSF[0].rGain:1.0
TEST.EXPECTED:switchboard.rSB_ApplyScaling.pSF[0].rOffset:0.0
TEST.END

-- Subprogram: ubSB_ApplyBitManipulations

-- Test Case: Use_for_Integration_UB_UINT8
TEST.UNIT:switchboard
TEST.SUBPROGRAM:ubSB_ApplyBitManipulations
TEST.NEW
TEST.NAME:Use_for_Integration_UB_UINT8
TEST.COMPOUND_ONLY
TEST.BASIS_PATH:1 of 7
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (7) case (pConfig->eBitType) ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.ubSB_ApplyBitManipulations.ubBits:<<MIN>>
TEST.VALUE:switchboard.ubSB_ApplyBitManipulations.pConfig:<<malloc 1>>
TEST.VALUE:switchboard.ubSB_ApplyBitManipulations.pConfig[0].eDataType:SB_UINT8
TEST.VALUE:switchboard.ubSB_ApplyBitManipulations.pConfig[0].eBitType:SB_LEFT
TEST.VALUE:switchboard.ubSB_ApplyBitManipulations.pConfig[0].ulMask:1
TEST.VALUE:switchboard.ubSB_ApplyBitManipulations.pConfig[0].ulShiftBits:2
TEST.VALUE:switchboard.ubSB_ApplyBitManipulations.pConfig[0].ulScaleFactor:10
TEST.VALUE:switchboard.ubSB_ApplyBitManipulations.ubReadData:<<MIN>>
TEST.EXPECTED:switchboard.ubSB_ApplyBitManipulations.ubBits:0
TEST.EXPECTED:switchboard.ubSB_ApplyBitManipulations.pConfig[0].eDataType:SB_UINT8
TEST.EXPECTED:switchboard.ubSB_ApplyBitManipulations.pConfig[0].eBitType:SB_LEFT
TEST.EXPECTED:switchboard.ubSB_ApplyBitManipulations.pConfig[0].ulMask:1
TEST.EXPECTED:switchboard.ubSB_ApplyBitManipulations.pConfig[0].ulShiftBits:2
TEST.EXPECTED:switchboard.ubSB_ApplyBitManipulations.pConfig[0].ulScaleFactor:10
TEST.EXPECTED:switchboard.ubSB_ApplyBitManipulations.ubReadData:0
TEST.EXPECTED:switchboard.ubSB_ApplyBitManipulations.return:0
TEST.END

-- Subprogram: ulSB_ApplyBitManipulations

-- Test Case: Use_For_Integration_UL_UINT32
TEST.UNIT:switchboard
TEST.SUBPROGRAM:ulSB_ApplyBitManipulations
TEST.NEW
TEST.NAME:Use_For_Integration_UL_UINT32
TEST.COMPOUND_ONLY
TEST.BASIS_PATH:1 of 7
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (7) case (pConfig->eBitType) ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.ulSB_ApplyBitManipulations.ulBits:<<MIN>>
TEST.VALUE:switchboard.ulSB_ApplyBitManipulations.pConfig:<<malloc 1>>
TEST.VALUE:switchboard.ulSB_ApplyBitManipulations.pConfig[0].eDataType:SB_UINT32
TEST.VALUE:switchboard.ulSB_ApplyBitManipulations.pConfig[0].eBitType:SB_LEFT
TEST.VALUE:switchboard.ulSB_ApplyBitManipulations.ulReadData:<<MIN>>
TEST.EXPECTED:switchboard.ulSB_ApplyBitManipulations.ulBits:0
TEST.EXPECTED:switchboard.ulSB_ApplyBitManipulations.pConfig[0].eDataType:SB_UINT32
TEST.EXPECTED:switchboard.ulSB_ApplyBitManipulations.pConfig[0].eBitType:SB_LEFT
TEST.EXPECTED:switchboard.ulSB_ApplyBitManipulations.ulReadData:0
TEST.END

-- Subprogram: uwSB_ApplyBitManipulations

-- Test Case: Use_For_Int_SB_Uint16
TEST.UNIT:switchboard
TEST.SUBPROGRAM:uwSB_ApplyBitManipulations
TEST.NEW
TEST.NAME:Use_For_Int_SB_Uint16
TEST.COMPOUND_ONLY
TEST.BASIS_PATH:1 of 7
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (7) case (pConfig->eBitType) ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.uwSB_ApplyBitManipulations.uwBits:<<MIN>>
TEST.VALUE:switchboard.uwSB_ApplyBitManipulations.pConfig:<<malloc 1>>
TEST.VALUE:switchboard.uwSB_ApplyBitManipulations.pConfig[0].eDataType:SB_UINT16
TEST.VALUE:switchboard.uwSB_ApplyBitManipulations.pConfig[0].eBitType:SB_RIGHT_RMW
TEST.VALUE:switchboard.uwSB_ApplyBitManipulations.uwReadData:<<MIN>>
TEST.EXPECTED:switchboard.uwSB_ApplyBitManipulations.uwBits:0
TEST.EXPECTED:switchboard.uwSB_ApplyBitManipulations.pConfig[0].eDataType:SB_UINT16
TEST.EXPECTED:switchboard.uwSB_ApplyBitManipulations.pConfig[0].eBitType:SB_RIGHT_RMW
TEST.EXPECTED:switchboard.uwSB_ApplyBitManipulations.uwReadData:0
TEST.EXPECTED:switchboard.uwSB_ApplyBitManipulations.return:0
TEST.END

-- Subprogram: vSB_ApplyBitManipulations

-- Test Case: Use_For_Int_SB_Boolean
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_ApplyBitManipulations
TEST.NEW
TEST.NAME:Use_For_Int_SB_Boolean
TEST.COMPOUND_ONLY
TEST.BASIS_PATH:1 of 4
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (4) case (pConfig->eDataType) ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_ApplyBitManipulations.pvBitAddress:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_ApplyBitManipulations.pConfig:<<malloc 1>>
TEST.VALUE:switchboard.vSB_ApplyBitManipulations.pConfig[0].eDataType:SB_BOOLEAN
TEST.VALUE:switchboard.vSB_ApplyBitManipulations.pConfig[0].eBitType:SB_NO_BIT_MAN
TEST.VALUE:switchboard.vSB_ApplyBitManipulations.pvReadAddress:VECTORCAST_INT1
TEST.EXPECTED:switchboard.vSB_ApplyBitManipulations.pConfig[0].eDataType:SB_BOOLEAN
TEST.EXPECTED:switchboard.vSB_ApplyBitManipulations.pConfig[0].eBitType:SB_NO_BIT_MAN
TEST.EXPECTED:switchboard.vSB_ApplyBitManipulations.pConfig[0].ulMask:0
TEST.EXPECTED:switchboard.vSB_ApplyBitManipulations.pConfig[0].ulShiftBits:0
TEST.EXPECTED:switchboard.vSB_ApplyBitManipulations.pConfig[0].ulScaleFactor:0
TEST.END

-- Test Case: Use_For_Int_SB_Iint8
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_ApplyBitManipulations
TEST.NEW
TEST.NAME:Use_For_Int_SB_Iint8
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 2
      (1) case (pConfig->eDataType) ==> SB_UINT8
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_ApplyBitManipulations.pvBitAddress:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_ApplyBitManipulations.pConfig:<<malloc 1>>
TEST.VALUE:switchboard.vSB_ApplyBitManipulations.pConfig[0].eDataType:SB_INT8
TEST.VALUE:switchboard.vSB_ApplyBitManipulations.pConfig[0].eBitType:SB_NO_BIT_MAN
TEST.VALUE:switchboard.vSB_ApplyBitManipulations.pvReadAddress:VECTORCAST_INT1
TEST.EXPECTED:switchboard.vSB_ApplyBitManipulations.pConfig[0].eDataType:SB_INT8
TEST.EXPECTED:switchboard.vSB_ApplyBitManipulations.pConfig[0].eBitType:SB_NO_BIT_MAN
TEST.EXPECTED:switchboard.vSB_ApplyBitManipulations.pConfig[0].ulMask:0
TEST.EXPECTED:switchboard.vSB_ApplyBitManipulations.pConfig[0].ulShiftBits:0
TEST.EXPECTED:switchboard.vSB_ApplyBitManipulations.pConfig[0].ulScaleFactor:0
TEST.END

-- Test Case: Use_For_Int_SB_Int16
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_ApplyBitManipulations
TEST.NEW
TEST.NAME:Use_For_Int_SB_Int16
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 3
      (2) case (pConfig->eDataType) ==> SB_UINT16
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_ApplyBitManipulations.pvBitAddress:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_ApplyBitManipulations.pConfig:<<malloc 1>>
TEST.VALUE:switchboard.vSB_ApplyBitManipulations.pConfig[0].eDataType:SB_UINT8
TEST.VALUE:switchboard.vSB_ApplyBitManipulations.pConfig[0].eBitType:SB_RIGHT
TEST.VALUE:switchboard.vSB_ApplyBitManipulations.pvReadAddress:VECTORCAST_INT1
TEST.EXPECTED:switchboard.vSB_ApplyBitManipulations.pConfig[0].eDataType:SB_UINT8
TEST.EXPECTED:switchboard.vSB_ApplyBitManipulations.pConfig[0].eBitType:SB_RIGHT
TEST.EXPECTED:switchboard.vSB_ApplyBitManipulations.pConfig[0].ulMask:0
TEST.EXPECTED:switchboard.vSB_ApplyBitManipulations.pConfig[0].ulShiftBits:0
TEST.EXPECTED:switchboard.vSB_ApplyBitManipulations.pConfig[0].ulScaleFactor:0
TEST.END

-- Test Case: Use_For_Int_SB_Uint16
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_ApplyBitManipulations
TEST.NEW
TEST.NAME:Use_For_Int_SB_Uint16
TEST.COMPOUND_ONLY
TEST.BASIS_PATH:3 of 4
TEST.NOTES:
This is an automatically generated test case.
   Test Path 3
      (2) case (pConfig->eDataType) ==> SB_UINT16
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_ApplyBitManipulations.pvBitAddress:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_ApplyBitManipulations.pConfig:<<malloc 1>>
TEST.VALUE:switchboard.vSB_ApplyBitManipulations.pConfig[0].eDataType:SB_UINT16
TEST.VALUE:switchboard.vSB_ApplyBitManipulations.pConfig[0].eBitType:SB_RIGHT
TEST.VALUE:switchboard.vSB_ApplyBitManipulations.pConfig[0].ulMask:2
TEST.VALUE:switchboard.vSB_ApplyBitManipulations.pConfig[0].ulShiftBits:0
TEST.VALUE:switchboard.vSB_ApplyBitManipulations.pConfig[0].ulScaleFactor:15
TEST.VALUE:switchboard.vSB_ApplyBitManipulations.pvReadAddress:VECTORCAST_INT1
TEST.EXPECTED:switchboard.vSB_ApplyBitManipulations.pConfig[0].eDataType:SB_UINT16
TEST.EXPECTED:switchboard.vSB_ApplyBitManipulations.pConfig[0].eBitType:SB_RIGHT
TEST.EXPECTED:switchboard.vSB_ApplyBitManipulations.pConfig[0].ulMask:2
TEST.EXPECTED:switchboard.vSB_ApplyBitManipulations.pConfig[0].ulShiftBits:0
TEST.EXPECTED:switchboard.vSB_ApplyBitManipulations.pConfig[0].ulScaleFactor:15
TEST.END

-- Test Case: Use_For_Int_SB_Uint8
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_ApplyBitManipulations
TEST.NEW
TEST.NAME:Use_For_Int_SB_Uint8
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 3
      (2) case (pConfig->eDataType) ==> SB_UINT16
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_ApplyBitManipulations.pvBitAddress:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_ApplyBitManipulations.pConfig:<<malloc 1>>
TEST.VALUE:switchboard.vSB_ApplyBitManipulations.pConfig[0].eDataType:SB_UINT8
TEST.VALUE:switchboard.vSB_ApplyBitManipulations.pConfig[0].eBitType:SB_RIGHT
TEST.VALUE:switchboard.vSB_ApplyBitManipulations.pConfig[0].ulMask:2
TEST.VALUE:switchboard.vSB_ApplyBitManipulations.pConfig[0].ulShiftBits:0
TEST.VALUE:switchboard.vSB_ApplyBitManipulations.pConfig[0].ulScaleFactor:15
TEST.VALUE:switchboard.vSB_ApplyBitManipulations.pvReadAddress:VECTORCAST_INT1
TEST.EXPECTED:switchboard.vSB_ApplyBitManipulations.pConfig[0].eDataType:SB_UINT8
TEST.EXPECTED:switchboard.vSB_ApplyBitManipulations.pConfig[0].eBitType:SB_RIGHT
TEST.EXPECTED:switchboard.vSB_ApplyBitManipulations.pConfig[0].ulMask:2
TEST.EXPECTED:switchboard.vSB_ApplyBitManipulations.pConfig[0].ulShiftBits:0
TEST.EXPECTED:switchboard.vSB_ApplyBitManipulations.pConfig[0].ulScaleFactor:15
TEST.END

-- Test Case: Use_For_SB_Float32
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_ApplyBitManipulations
TEST.NEW
TEST.NAME:Use_For_SB_Float32
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 4
      (3) case (pConfig->eDataType) ==> SB_UINT32
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_ApplyBitManipulations.pvBitAddress:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_ApplyBitManipulations.pConfig:<<malloc 1>>
TEST.VALUE:switchboard.vSB_ApplyBitManipulations.pConfig[0].eDataType:SB_FLOAT32
TEST.VALUE:switchboard.vSB_ApplyBitManipulations.pConfig[0].eBitType:SB_RIGHT
TEST.VALUE:switchboard.vSB_ApplyBitManipulations.pvReadAddress:VECTORCAST_INT1
TEST.EXPECTED:switchboard.vSB_ApplyBitManipulations.pConfig[0].eDataType:SB_FLOAT32
TEST.EXPECTED:switchboard.vSB_ApplyBitManipulations.pConfig[0].eBitType:SB_RIGHT
TEST.EXPECTED:switchboard.vSB_ApplyBitManipulations.pConfig[0].ulMask:0
TEST.EXPECTED:switchboard.vSB_ApplyBitManipulations.pConfig[0].ulShiftBits:0
TEST.EXPECTED:switchboard.vSB_ApplyBitManipulations.pConfig[0].ulScaleFactor:0
TEST.END

-- Test Case: Use_For_SB_Int32
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_ApplyBitManipulations
TEST.NEW
TEST.NAME:Use_For_SB_Int32
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 4
      (3) case (pConfig->eDataType) ==> SB_UINT32
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_ApplyBitManipulations.pvBitAddress:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_ApplyBitManipulations.pConfig:<<malloc 1>>
TEST.VALUE:switchboard.vSB_ApplyBitManipulations.pConfig[0].eDataType:SB_INT32
TEST.VALUE:switchboard.vSB_ApplyBitManipulations.pConfig[0].eBitType:SB_RIGHT_RMW
TEST.VALUE:switchboard.vSB_ApplyBitManipulations.pvReadAddress:VECTORCAST_INT1
TEST.EXPECTED:switchboard.vSB_ApplyBitManipulations.pConfig[0].eDataType:SB_INT32
TEST.EXPECTED:switchboard.vSB_ApplyBitManipulations.pConfig[0].eBitType:SB_RIGHT_RMW
TEST.EXPECTED:switchboard.vSB_ApplyBitManipulations.pConfig[0].ulMask:0
TEST.EXPECTED:switchboard.vSB_ApplyBitManipulations.pConfig[0].ulShiftBits:0
TEST.EXPECTED:switchboard.vSB_ApplyBitManipulations.pConfig[0].ulScaleFactor:0
TEST.END

-- Test Case: Use_For_SB_Uint32
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_ApplyBitManipulations
TEST.NEW
TEST.NAME:Use_For_SB_Uint32
TEST.COMPOUND_ONLY
TEST.BASIS_PATH:4 of 4
TEST.NOTES:
This is an automatically generated test case.
   Test Path 4
      (3) case (pConfig->eDataType) ==> SB_UINT32
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_ApplyBitManipulations.pvBitAddress:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_ApplyBitManipulations.pConfig:<<malloc 1>>
TEST.VALUE:switchboard.vSB_ApplyBitManipulations.pConfig[0].eDataType:SB_UINT32
TEST.VALUE:switchboard.vSB_ApplyBitManipulations.pConfig[0].eBitType:SB_RIGHT_RMW
TEST.VALUE:switchboard.vSB_ApplyBitManipulations.pvReadAddress:VECTORCAST_INT1
TEST.EXPECTED:switchboard.vSB_ApplyBitManipulations.pConfig[0].eDataType:SB_UINT32
TEST.EXPECTED:switchboard.vSB_ApplyBitManipulations.pConfig[0].eBitType:SB_RIGHT_RMW
TEST.EXPECTED:switchboard.vSB_ApplyBitManipulations.pConfig[0].ulMask:0
TEST.EXPECTED:switchboard.vSB_ApplyBitManipulations.pConfig[0].ulShiftBits:0
TEST.EXPECTED:switchboard.vSB_ApplyBitManipulations.pConfig[0].ulScaleFactor:0
TEST.END

-- Subprogram: vSB_ApplyScaling

-- Test Case: Use_For_Int_SB_Boolean
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_ApplyScaling
TEST.NEW
TEST.NAME:Use_For_Int_SB_Boolean
TEST.COMPOUND_ONLY
TEST.BASIS_PATH:1 of 8
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (8) case (pConfig->eDataType) ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_ApplyScaling.pvData:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_ApplyScaling.pConfig:<<malloc 1>>
TEST.VALUE:switchboard.vSB_ApplyScaling.pConfig[0].eDataType:SB_BOOLEAN
TEST.VALUE:switchboard.vSB_ApplyScaling.pConfig[0].eBitType:SB_NO_BIT_MAN
TEST.VALUE:switchboard.vSB_ApplyScaling.pScaleFactors:<<malloc 1>>
TEST.VALUE:switchboard.vSB_ApplyScaling.pScaleFactors[0]:<<malloc 1>>
TEST.VALUE:switchboard.vSB_ApplyScaling.pScaleFactors[0][0].rOffset:1000.0
TEST.EXPECTED:switchboard.vSB_ApplyScaling.pConfig[0].eDataType:SB_BOOLEAN
TEST.EXPECTED:switchboard.vSB_ApplyScaling.pConfig[0].eBitType:SB_NO_BIT_MAN
TEST.EXPECTED:switchboard.vSB_ApplyScaling.pConfig[0].ulScaleFactor:0
TEST.EXPECTED:switchboard.vSB_ApplyScaling.pScaleFactors[0][0].rOffset:1000.0
TEST.VALUE_USER_CODE:switchboard.vSB_ApplyScaling.pScaleFactors.pScaleFactors[0].pScaleFactors[0][0].rGain
<<switchboard.vSB_ApplyScaling.pScaleFactors>>[0][0].rGain = ( (1*5)/2.5 );
TEST.END_VALUE_USER_CODE:
TEST.EXPECTED_USER_CODE:switchboard.vSB_ApplyScaling.pScaleFactors.pScaleFactors[0].pScaleFactors[0][0].rGain
{{ <<switchboard.vSB_ApplyScaling.pScaleFactors>>[0][0].rGain == ( (1*5)/2.5 ) }}
TEST.END_EXPECTED_USER_CODE:
TEST.END

-- Test Case: Use_For_Int_SB_Float32
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_ApplyScaling
TEST.NEW
TEST.NAME:Use_For_Int_SB_Float32
TEST.COMPOUND_ONLY
TEST.BASIS_PATH:1 of 8
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (8) case (pConfig->eDataType) ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_ApplyScaling.pvData:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_ApplyScaling.pConfig:<<malloc 1>>
TEST.VALUE:switchboard.vSB_ApplyScaling.pConfig[0].eDataType:SB_FLOAT32
TEST.VALUE:switchboard.vSB_ApplyScaling.pConfig[0].eBitType:SB_LEFT
TEST.VALUE:switchboard.vSB_ApplyScaling.pScaleFactors:<<malloc 1>>
TEST.VALUE:switchboard.vSB_ApplyScaling.pScaleFactors[0]:<<malloc 1>>
TEST.VALUE:switchboard.vSB_ApplyScaling.pScaleFactors[0][0].rGain:10.0
TEST.VALUE:switchboard.vSB_ApplyScaling.pScaleFactors[0][0].rOffset:1001.0
TEST.EXPECTED:switchboard.vSB_ApplyScaling.pConfig[0].eDataType:SB_FLOAT32
TEST.EXPECTED:switchboard.vSB_ApplyScaling.pConfig[0].eBitType:SB_LEFT
TEST.EXPECTED:switchboard.vSB_ApplyScaling.pScaleFactors[0][0].rGain:10.0
TEST.EXPECTED:switchboard.vSB_ApplyScaling.pScaleFactors[0][0].rOffset:1001.0
TEST.END

-- Test Case: Use_For_Int_SB_Int16
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_ApplyScaling
TEST.NEW
TEST.NAME:Use_For_Int_SB_Int16
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (8) case (pConfig->eDataType) ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_ApplyScaling.pvData:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_ApplyScaling.pConfig:<<malloc 1>>
TEST.VALUE:switchboard.vSB_ApplyScaling.pConfig[0].eDataType:SB_INT16
TEST.VALUE:switchboard.vSB_ApplyScaling.pConfig[0].eBitType:SB_LEFT
TEST.VALUE:switchboard.vSB_ApplyScaling.pScaleFactors:<<malloc 1>>
TEST.VALUE:switchboard.vSB_ApplyScaling.pScaleFactors[0]:<<malloc 1>>
TEST.VALUE:switchboard.vSB_ApplyScaling.pScaleFactors[0][0].rGain:1.0
TEST.VALUE:switchboard.vSB_ApplyScaling.pScaleFactors[0][0].rOffset:0.0
TEST.EXPECTED:switchboard.vSB_ApplyScaling.pConfig[0].eDataType:SB_INT16
TEST.EXPECTED:switchboard.vSB_ApplyScaling.pConfig[0].eBitType:SB_LEFT
TEST.EXPECTED:switchboard.vSB_ApplyScaling.pScaleFactors[0][0].rGain:1.0
TEST.EXPECTED:switchboard.vSB_ApplyScaling.pScaleFactors[0][0].rOffset:0.0
TEST.END

-- Test Case: Use_For_Int_SB_Int8
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_ApplyScaling
TEST.NEW
TEST.NAME:Use_For_Int_SB_Int8
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (8) case (pConfig->eDataType) ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_ApplyScaling.pvData:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_ApplyScaling.pConfig:<<malloc 1>>
TEST.VALUE:switchboard.vSB_ApplyScaling.pConfig[0].eDataType:SB_INT8
TEST.VALUE:switchboard.vSB_ApplyScaling.pConfig[0].eBitType:SB_NO_BIT_MAN
TEST.VALUE:switchboard.vSB_ApplyScaling.pScaleFactors:<<malloc 1>>
TEST.VALUE:switchboard.vSB_ApplyScaling.pScaleFactors[0]:<<malloc 1>>
TEST.VALUE:switchboard.vSB_ApplyScaling.pScaleFactors[0][0].rGain:1.0
TEST.VALUE:switchboard.vSB_ApplyScaling.pScaleFactors[0][0].rOffset:0.0
TEST.EXPECTED:switchboard.vSB_ApplyScaling.pConfig[0].eDataType:SB_INT8
TEST.EXPECTED:switchboard.vSB_ApplyScaling.pConfig[0].eBitType:SB_NO_BIT_MAN
TEST.EXPECTED:switchboard.vSB_ApplyScaling.pConfig[0].ulShiftBits:0
TEST.EXPECTED:switchboard.vSB_ApplyScaling.pConfig[0].ulScaleFactor:0
TEST.EXPECTED:switchboard.vSB_ApplyScaling.pScaleFactors[0][0].rGain:1.0
TEST.EXPECTED:switchboard.vSB_ApplyScaling.pScaleFactors[0][0].rOffset:0.0
TEST.END

-- Test Case: Use_For_Int_SL_Int32
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_ApplyScaling
TEST.NEW
TEST.NAME:Use_For_Int_SL_Int32
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (8) case (pConfig->eDataType) ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_ApplyScaling.pvData:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_ApplyScaling.pConfig:<<malloc 1>>
TEST.VALUE:switchboard.vSB_ApplyScaling.pConfig[0].eDataType:SB_INT32
TEST.VALUE:switchboard.vSB_ApplyScaling.pConfig[0].eBitType:SB_LEFT
TEST.VALUE:switchboard.vSB_ApplyScaling.pScaleFactors:<<malloc 1>>
TEST.VALUE:switchboard.vSB_ApplyScaling.pScaleFactors[0]:<<malloc 1>>
TEST.VALUE:switchboard.vSB_ApplyScaling.pScaleFactors[0][0].rGain:1.0
TEST.VALUE:switchboard.vSB_ApplyScaling.pScaleFactors[0][0].rOffset:0.0
TEST.EXPECTED:switchboard.vSB_ApplyScaling.pConfig[0].eDataType:SB_INT32
TEST.EXPECTED:switchboard.vSB_ApplyScaling.pConfig[0].eBitType:SB_LEFT
TEST.EXPECTED:switchboard.vSB_ApplyScaling.pScaleFactors[0][0].rGain:1.0
TEST.EXPECTED:switchboard.vSB_ApplyScaling.pScaleFactors[0][0].rOffset:0.0
TEST.END

-- Test Case: Use_For_Int_UB_Uint8
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_ApplyScaling
TEST.NEW
TEST.NAME:Use_For_Int_UB_Uint8
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (8) case (pConfig->eDataType) ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_ApplyScaling.pvData:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_ApplyScaling.pConfig:<<malloc 1>>
TEST.VALUE:switchboard.vSB_ApplyScaling.pConfig[0].eDataType:SB_UINT8
TEST.VALUE:switchboard.vSB_ApplyScaling.pConfig[0].eBitType:SB_NO_BIT_MAN
TEST.VALUE:switchboard.vSB_ApplyScaling.pScaleFactors:<<malloc 1>>
TEST.VALUE:switchboard.vSB_ApplyScaling.pScaleFactors[0]:<<malloc 1>>
TEST.VALUE:switchboard.vSB_ApplyScaling.pScaleFactors[0][0].rGain:1.0
TEST.VALUE:switchboard.vSB_ApplyScaling.pScaleFactors[0][0].rOffset:0.0
TEST.EXPECTED:switchboard.vSB_ApplyScaling.pConfig[0].eDataType:SB_UINT8
TEST.EXPECTED:switchboard.vSB_ApplyScaling.pConfig[0].eBitType:SB_NO_BIT_MAN
TEST.EXPECTED:switchboard.vSB_ApplyScaling.pConfig[0].ulShiftBits:0
TEST.EXPECTED:switchboard.vSB_ApplyScaling.pConfig[0].ulScaleFactor:0
TEST.EXPECTED:switchboard.vSB_ApplyScaling.pScaleFactors[0][0].rGain:1.0
TEST.EXPECTED:switchboard.vSB_ApplyScaling.pScaleFactors[0][0].rOffset:0.0
TEST.END

-- Test Case: Use_For_Int_UL_Uint32
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_ApplyScaling
TEST.NEW
TEST.NAME:Use_For_Int_UL_Uint32
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (8) case (pConfig->eDataType) ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_ApplyScaling.pvData:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_ApplyScaling.pConfig:<<malloc 1>>
TEST.VALUE:switchboard.vSB_ApplyScaling.pConfig[0].eDataType:SB_UINT32
TEST.VALUE:switchboard.vSB_ApplyScaling.pConfig[0].eBitType:SB_LEFT
TEST.VALUE:switchboard.vSB_ApplyScaling.pScaleFactors:<<malloc 1>>
TEST.VALUE:switchboard.vSB_ApplyScaling.pScaleFactors[0]:<<malloc 1>>
TEST.VALUE:switchboard.vSB_ApplyScaling.pScaleFactors[0][0].rGain:1.0
TEST.VALUE:switchboard.vSB_ApplyScaling.pScaleFactors[0][0].rOffset:0.0
TEST.EXPECTED:switchboard.vSB_ApplyScaling.pConfig[0].eDataType:SB_UINT32
TEST.EXPECTED:switchboard.vSB_ApplyScaling.pConfig[0].eBitType:SB_LEFT
TEST.EXPECTED:switchboard.vSB_ApplyScaling.pScaleFactors[0][0].rGain:1.0
TEST.EXPECTED:switchboard.vSB_ApplyScaling.pScaleFactors[0][0].rOffset:0.0
TEST.END

-- Test Case: Use_For_Int_UW_Uint16
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_ApplyScaling
TEST.NEW
TEST.NAME:Use_For_Int_UW_Uint16
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (8) case (pConfig->eDataType) ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_ApplyScaling.pvData:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_ApplyScaling.pConfig:<<malloc 1>>
TEST.VALUE:switchboard.vSB_ApplyScaling.pConfig[0].eDataType:SB_UINT16
TEST.VALUE:switchboard.vSB_ApplyScaling.pConfig[0].eBitType:SB_RIGHT
TEST.VALUE:switchboard.vSB_ApplyScaling.pScaleFactors:<<malloc 1>>
TEST.VALUE:switchboard.vSB_ApplyScaling.pScaleFactors[0]:<<malloc 1>>
TEST.VALUE:switchboard.vSB_ApplyScaling.pScaleFactors[0][0].rGain:1.0
TEST.VALUE:switchboard.vSB_ApplyScaling.pScaleFactors[0][0].rOffset:0.0
TEST.EXPECTED:switchboard.vSB_ApplyScaling.pConfig[0].eDataType:SB_UINT16
TEST.EXPECTED:switchboard.vSB_ApplyScaling.pConfig[0].eBitType:SB_RIGHT
TEST.EXPECTED:switchboard.vSB_ApplyScaling.pScaleFactors[0][0].rGain:1.0
TEST.EXPECTED:switchboard.vSB_ApplyScaling.pScaleFactors[0][0].rOffset:0.0
TEST.END

-- Subprogram: vSB_CopyNodeData

-- Test Case: Use_For_Integration_SB_Boolean
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_CopyNodeData
TEST.NEW
TEST.NAME:Use_For_Integration_SB_Boolean
TEST.COMPOUND_ONLY
TEST.BASIS_PATH:1 of 9
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (9) case eSourceType ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.STUB:ag_input_router.gvAG_InputRouter_Process
TEST.VALUE:switchboard.vSB_CopyNodeData.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_CopyNodeData.eDestType:SB_BOOLEAN
TEST.VALUE:switchboard.vSB_CopyNodeData.eSourceType:SB_BOOLEAN
TEST.EXPECTED:switchboard.vSB_CopyNodeData.eDestType:SB_BOOLEAN
TEST.EXPECTED:switchboard.vSB_CopyNodeData.eSourceType:SB_BOOLEAN
TEST.EXPECTED:ag_input_router.gvAG_InputRouter_Process.eProcess:PROCESS_SYS_MODEL
TEST.END

-- Test Case: Use_For_Integration_SB_Int16
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_CopyNodeData
TEST.NEW
TEST.NAME:Use_For_Integration_SB_Int16
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (9) case eSourceType ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_CopyNodeData.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_CopyNodeData.eDestType:SB_INT16
TEST.VALUE:switchboard.vSB_CopyNodeData.pvSource:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_CopyNodeData.eSourceType:SB_INT16
TEST.EXPECTED:switchboard.vSB_CopyNodeData.eDestType:SB_INT16
TEST.EXPECTED:switchboard.vSB_CopyNodeData.eSourceType:SB_INT16
TEST.END

-- Test Case: Use_For_Integration_SB_Int32
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_CopyNodeData
TEST.NEW
TEST.NAME:Use_For_Integration_SB_Int32
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (9) case eSourceType ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_CopyNodeData.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_CopyNodeData.eDestType:SB_INT32
TEST.VALUE:switchboard.vSB_CopyNodeData.pvSource:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_CopyNodeData.eSourceType:SB_INT32
TEST.EXPECTED:switchboard.vSB_CopyNodeData.eDestType:SB_INT32
TEST.EXPECTED:switchboard.vSB_CopyNodeData.eSourceType:SB_INT32
TEST.END

-- Test Case: Use_For_Integration_SB_Int8
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_CopyNodeData
TEST.NEW
TEST.NAME:Use_For_Integration_SB_Int8
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (9) case eSourceType ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_CopyNodeData.pvDest:<<null>>
TEST.VALUE:switchboard.vSB_CopyNodeData.eDestType:SB_INT8
TEST.VALUE:switchboard.vSB_CopyNodeData.pvSource:<<null>>
TEST.VALUE:switchboard.vSB_CopyNodeData.eSourceType:SB_INT8
TEST.EXPECTED:switchboard.vSB_CopyNodeData.eDestType:SB_INT8
TEST.EXPECTED:switchboard.vSB_CopyNodeData.eSourceType:SB_INT8
TEST.END

-- Test Case: Use_For_Integration_SB_Uiint32
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_CopyNodeData
TEST.NEW
TEST.NAME:Use_For_Integration_SB_Uiint32
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (9) case eSourceType ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_CopyNodeData.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_CopyNodeData.eDestType:SB_UINT32
TEST.VALUE:switchboard.vSB_CopyNodeData.pvSource:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_CopyNodeData.eSourceType:SB_UINT32
TEST.EXPECTED:switchboard.vSB_CopyNodeData.eDestType:SB_UINT32
TEST.EXPECTED:switchboard.vSB_CopyNodeData.eSourceType:SB_UINT32
TEST.END

-- Test Case: Use_For_Integration_SB_Uint16
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_CopyNodeData
TEST.NEW
TEST.NAME:Use_For_Integration_SB_Uint16
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (9) case eSourceType ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_CopyNodeData.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_CopyNodeData.eDestType:SB_UINT16
TEST.VALUE:switchboard.vSB_CopyNodeData.pvSource:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_CopyNodeData.eSourceType:SB_UINT16
TEST.EXPECTED:switchboard.vSB_CopyNodeData.eDestType:SB_UINT16
TEST.EXPECTED:switchboard.vSB_CopyNodeData.eSourceType:SB_UINT16
TEST.END

-- Test Case: Use_For_Integration_SB_Uint8
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_CopyNodeData
TEST.NEW
TEST.NAME:Use_For_Integration_SB_Uint8
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (9) case eSourceType ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.STUB:switchboard.vSB_fCastEntry
TEST.STUB:switchboard.ubSB_ApplyBitManipulations
TEST.STUB:switchboard.vSB_ApplyScaling
TEST.VALUE:switchboard.vSB_CopyNodeData.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_CopyNodeData.eDestType:SB_UINT8
TEST.VALUE:switchboard.vSB_CopyNodeData.pvSource:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_CopyNodeData.eSourceType:SB_UINT8
TEST.EXPECTED:switchboard.vSB_CopyNodeData.eDestType:SB_UINT8
TEST.EXPECTED:switchboard.vSB_CopyNodeData.eSourceType:SB_UINT8
TEST.END

-- Subprogram: vSB_fCastEntry

-- Test Case: Use_For_Integration_SB_Boolean
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_fCastEntry
TEST.NEW
TEST.NAME:Use_For_Integration_SB_Boolean
TEST.COMPOUND_ONLY
TEST.BASIS_PATH:3 of 9
TEST.NOTES:
This is an automatically generated test case.
   Test Path 3
      (2) case eDestType ==> SB_INT8
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_fCastEntry.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_fCastEntry.eDestType:SB_BOOLEAN
TEST.VALUE:switchboard.vSB_fCastEntry.fSourceData:<<MIN>>
TEST.EXPECTED:switchboard.vSB_fCastEntry.eDestType:SB_BOOLEAN
TEST.EXPECTED:switchboard.vSB_fCastEntry.fSourceData:0
TEST.END

-- Test Case: Use_For_Integration_SB_Float32
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_fCastEntry
TEST.NEW
TEST.NAME:Use_For_Integration_SB_Float32
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 3
      (2) case eDestType ==> SB_INT8
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_fCastEntry.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_fCastEntry.eDestType:SB_FLOAT32
TEST.VALUE:switchboard.vSB_fCastEntry.fSourceData:<<MIN>>
TEST.EXPECTED:switchboard.vSB_fCastEntry.eDestType:SB_FLOAT32
TEST.EXPECTED:switchboard.vSB_fCastEntry.fSourceData:0
TEST.END

-- Test Case: Use_For_Integration_SB_Int16
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_fCastEntry
TEST.NEW
TEST.NAME:Use_For_Integration_SB_Int16
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 3
      (2) case eDestType ==> SB_INT8
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_fCastEntry.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_fCastEntry.eDestType:SB_INT16
TEST.VALUE:switchboard.vSB_fCastEntry.fSourceData:<<MIN>>
TEST.EXPECTED:switchboard.vSB_fCastEntry.eDestType:SB_INT16
TEST.EXPECTED:switchboard.vSB_fCastEntry.fSourceData:0
TEST.END

-- Test Case: Use_For_Integration_SB_Int32
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_fCastEntry
TEST.NEW
TEST.NAME:Use_For_Integration_SB_Int32
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 3
      (2) case eDestType ==> SB_INT8
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_fCastEntry.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_fCastEntry.eDestType:SB_INT32
TEST.VALUE:switchboard.vSB_fCastEntry.fSourceData:<<MIN>>
TEST.EXPECTED:switchboard.vSB_fCastEntry.eDestType:SB_INT32
TEST.EXPECTED:switchboard.vSB_fCastEntry.fSourceData:0
TEST.END

-- Test Case: Use_For_Integration_SB_Int8
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_fCastEntry
TEST.NEW
TEST.NAME:Use_For_Integration_SB_Int8
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 3
      (2) case eDestType ==> SB_INT8
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_fCastEntry.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_fCastEntry.eDestType:SB_INT8
TEST.VALUE:switchboard.vSB_fCastEntry.fSourceData:<<MIN>>
TEST.EXPECTED:switchboard.vSB_fCastEntry.eDestType:SB_INT8
TEST.EXPECTED:switchboard.vSB_fCastEntry.fSourceData:0
TEST.END

-- Test Case: Use_For_Integration_SB_Uiint32
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_fCastEntry
TEST.NEW
TEST.NAME:Use_For_Integration_SB_Uiint32
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 3
      (2) case eDestType ==> SB_INT8
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_fCastEntry.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_fCastEntry.eDestType:SB_UINT32
TEST.VALUE:switchboard.vSB_fCastEntry.fSourceData:<<MIN>>
TEST.EXPECTED:switchboard.vSB_fCastEntry.eDestType:SB_UINT32
TEST.EXPECTED:switchboard.vSB_fCastEntry.fSourceData:0
TEST.END

-- Test Case: Use_For_Integration_SB_Uint16
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_fCastEntry
TEST.NEW
TEST.NAME:Use_For_Integration_SB_Uint16
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 3
      (2) case eDestType ==> SB_INT8
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_fCastEntry.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_fCastEntry.eDestType:SB_UINT16
TEST.VALUE:switchboard.vSB_fCastEntry.fSourceData:<<MIN>>
TEST.EXPECTED:switchboard.vSB_fCastEntry.eDestType:SB_UINT16
TEST.EXPECTED:switchboard.vSB_fCastEntry.fSourceData:0
TEST.END

-- Test Case: Use_For_Integration_SB_Uint8
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_fCastEntry
TEST.NEW
TEST.NAME:Use_For_Integration_SB_Uint8
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 3
      (2) case eDestType ==> SB_INT8
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_fCastEntry.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_fCastEntry.eDestType:SB_UINT8
TEST.VALUE:switchboard.vSB_fCastEntry.fSourceData:<<MIN>>
TEST.EXPECTED:switchboard.vSB_fCastEntry.eDestType:SB_UINT8
TEST.EXPECTED:switchboard.vSB_fCastEntry.fSourceData:0
TEST.END

-- Subprogram: vSB_rCastEntry

-- Test Case: Use_For_Integration__SB_Boolean
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_rCastEntry
TEST.NEW
TEST.NAME:Use_For_Integration__SB_Boolean
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (9) case eDestType ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_rCastEntry.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_rCastEntry.eDestType:SB_BOOLEAN
TEST.VALUE:switchboard.vSB_rCastEntry.rSourceData:10.0
TEST.EXPECTED:switchboard.vSB_rCastEntry.eDestType:SB_BOOLEAN
TEST.EXPECTED:switchboard.vSB_rCastEntry.rSourceData:10.0
TEST.END

-- Test Case: Use_For_Integration__SB_Iint16
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_rCastEntry
TEST.NEW
TEST.NAME:Use_For_Integration__SB_Iint16
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (9) case eDestType ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_rCastEntry.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_rCastEntry.eDestType:SB_INT16
TEST.VALUE:switchboard.vSB_rCastEntry.rSourceData:1.0
TEST.EXPECTED:switchboard.vSB_rCastEntry.eDestType:SB_INT16
TEST.EXPECTED:switchboard.vSB_rCastEntry.rSourceData:1.0
TEST.END

-- Test Case: Use_For_Integration__SB_Iint8
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_rCastEntry
TEST.NEW
TEST.NAME:Use_For_Integration__SB_Iint8
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (9) case eDestType ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_rCastEntry.pvDest:VECTORCAST_INT2
TEST.VALUE:switchboard.vSB_rCastEntry.eDestType:SB_INT8
TEST.VALUE:switchboard.vSB_rCastEntry.rSourceData:1.0
TEST.EXPECTED:switchboard.vSB_rCastEntry.eDestType:SB_INT8
TEST.EXPECTED:switchboard.vSB_rCastEntry.rSourceData:1.0
TEST.END

-- Test Case: Use_For_Integration__SB_Int32
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_rCastEntry
TEST.NEW
TEST.NAME:Use_For_Integration__SB_Int32
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (9) case eDestType ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_rCastEntry.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_rCastEntry.eDestType:SB_INT32
TEST.VALUE:switchboard.vSB_rCastEntry.rSourceData:1.0
TEST.EXPECTED:switchboard.vSB_rCastEntry.eDestType:SB_INT32
TEST.EXPECTED:switchboard.vSB_rCastEntry.rSourceData:1.0
TEST.END

-- Test Case: Use_For_Integration__SB_Uiint16
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_rCastEntry
TEST.NEW
TEST.NAME:Use_For_Integration__SB_Uiint16
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (9) case eDestType ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_rCastEntry.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_rCastEntry.eDestType:SB_UINT16
TEST.VALUE:switchboard.vSB_rCastEntry.rSourceData:1.0
TEST.EXPECTED:switchboard.vSB_rCastEntry.eDestType:SB_UINT16
TEST.EXPECTED:switchboard.vSB_rCastEntry.rSourceData:1.0
TEST.END

-- Test Case: Use_For_Integration__SB_Uiint8
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_rCastEntry
TEST.NEW
TEST.NAME:Use_For_Integration__SB_Uiint8
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (9) case eDestType ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_rCastEntry.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_rCastEntry.eDestType:SB_UINT8
TEST.VALUE:switchboard.vSB_rCastEntry.rSourceData:1000.0
TEST.EXPECTED:switchboard.vSB_rCastEntry.eDestType:SB_UINT8
TEST.EXPECTED:switchboard.vSB_rCastEntry.rSourceData:1000.0
TEST.END

-- Test Case: Use_For_Integration__SB_Uint32
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_rCastEntry
TEST.NEW
TEST.NAME:Use_For_Integration__SB_Uint32
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (9) case eDestType ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_rCastEntry.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_rCastEntry.eDestType:SB_UINT32
TEST.VALUE:switchboard.vSB_rCastEntry.rSourceData:1.0
TEST.EXPECTED:switchboard.vSB_rCastEntry.eDestType:SB_UINT32
TEST.EXPECTED:switchboard.vSB_rCastEntry.rSourceData:1.0
TEST.END

-- Subprogram: vSB_sbCastEntry

-- Test Case: Use_For_Integration_SB_Boolean
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_sbCastEntry
TEST.NEW
TEST.NAME:Use_For_Integration_SB_Boolean
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (9) case eDestType ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_sbCastEntry.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_sbCastEntry.eDestType:SB_BOOLEAN
TEST.VALUE:switchboard.vSB_sbCastEntry.sbSourceData:0
TEST.EXPECTED:switchboard.vSB_sbCastEntry.eDestType:SB_BOOLEAN
TEST.EXPECTED:switchboard.vSB_sbCastEntry.sbSourceData:0
TEST.END

-- Test Case: Use_For_Integration_SB_FLOAT32
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_sbCastEntry
TEST.NEW
TEST.NAME:Use_For_Integration_SB_FLOAT32
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (9) case eDestType ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_sbCastEntry.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_sbCastEntry.eDestType:SB_FLOAT32
TEST.VALUE:switchboard.vSB_sbCastEntry.sbSourceData:0
TEST.EXPECTED:switchboard.vSB_sbCastEntry.eDestType:SB_FLOAT32
TEST.EXPECTED:switchboard.vSB_sbCastEntry.sbSourceData:0
TEST.END

-- Test Case: Use_For_Integration_SB_INT16
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_sbCastEntry
TEST.NEW
TEST.NAME:Use_For_Integration_SB_INT16
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (9) case eDestType ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_sbCastEntry.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_sbCastEntry.eDestType:SB_INT16
TEST.VALUE:switchboard.vSB_sbCastEntry.sbSourceData:0
TEST.EXPECTED:switchboard.vSB_sbCastEntry.eDestType:SB_INT16
TEST.EXPECTED:switchboard.vSB_sbCastEntry.sbSourceData:0
TEST.END

-- Test Case: Use_For_Integration_SB_INT32
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_sbCastEntry
TEST.NEW
TEST.NAME:Use_For_Integration_SB_INT32
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (9) case eDestType ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_sbCastEntry.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_sbCastEntry.eDestType:SB_INT32
TEST.VALUE:switchboard.vSB_sbCastEntry.sbSourceData:0
TEST.EXPECTED:switchboard.vSB_sbCastEntry.eDestType:SB_INT32
TEST.EXPECTED:switchboard.vSB_sbCastEntry.sbSourceData:0
TEST.END

-- Test Case: Use_For_Integration_SB_INT8
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_sbCastEntry
TEST.NEW
TEST.NAME:Use_For_Integration_SB_INT8
TEST.COMPOUND_ONLY
TEST.BASIS_PATH:1 of 9
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (9) case eDestType ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_sbCastEntry.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_sbCastEntry.eDestType:SB_INT8
TEST.VALUE:switchboard.vSB_sbCastEntry.sbSourceData:0
TEST.EXPECTED:switchboard.vSB_sbCastEntry.eDestType:SB_INT8
TEST.EXPECTED:switchboard.vSB_sbCastEntry.sbSourceData:0
TEST.END

-- Test Case: Use_For_Integration_SB_UINT16
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_sbCastEntry
TEST.NEW
TEST.NAME:Use_For_Integration_SB_UINT16
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (9) case eDestType ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_sbCastEntry.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_sbCastEntry.eDestType:SB_UINT16
TEST.VALUE:switchboard.vSB_sbCastEntry.sbSourceData:0
TEST.EXPECTED:switchboard.vSB_sbCastEntry.eDestType:SB_UINT16
TEST.EXPECTED:switchboard.vSB_sbCastEntry.sbSourceData:0
TEST.END

-- Test Case: Use_For_Integration_SB_UINT32
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_sbCastEntry
TEST.NEW
TEST.NAME:Use_For_Integration_SB_UINT32
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (9) case eDestType ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_sbCastEntry.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_sbCastEntry.eDestType:SB_UINT32
TEST.VALUE:switchboard.vSB_sbCastEntry.sbSourceData:0
TEST.EXPECTED:switchboard.vSB_sbCastEntry.eDestType:SB_UINT32
TEST.EXPECTED:switchboard.vSB_sbCastEntry.sbSourceData:0
TEST.END

-- Test Case: Use_For_Integration_SB_UINT8
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_sbCastEntry
TEST.NEW
TEST.NAME:Use_For_Integration_SB_UINT8
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (9) case eDestType ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_sbCastEntry.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_sbCastEntry.eDestType:SB_UINT8
TEST.VALUE:switchboard.vSB_sbCastEntry.sbSourceData:0
TEST.EXPECTED:switchboard.vSB_sbCastEntry.eDestType:SB_UINT8
TEST.EXPECTED:switchboard.vSB_sbCastEntry.sbSourceData:0
TEST.END

-- Subprogram: vSB_slCastEntry

-- Test Case: Use_For_Int_SL_Boolean
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_slCastEntry
TEST.NEW
TEST.NAME:Use_For_Int_SL_Boolean
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (9) case eDestType ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_slCastEntry.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_slCastEntry.slSourceData:0
TEST.EXPECTED:switchboard.vSB_slCastEntry.eDestType:SB_BOOLEAN
TEST.EXPECTED:switchboard.vSB_slCastEntry.slSourceData:0
TEST.END

-- Test Case: Use_For_Int_SL_FLOAT32
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_slCastEntry
TEST.NEW
TEST.NAME:Use_For_Int_SL_FLOAT32
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (9) case eDestType ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_slCastEntry.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_slCastEntry.eDestType:SB_FLOAT32
TEST.VALUE:switchboard.vSB_slCastEntry.slSourceData:0
TEST.EXPECTED:switchboard.vSB_slCastEntry.eDestType:SB_FLOAT32
TEST.EXPECTED:switchboard.vSB_slCastEntry.slSourceData:0
TEST.END

-- Test Case: Use_For_Int_SL_Int16
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_slCastEntry
TEST.NEW
TEST.NAME:Use_For_Int_SL_Int16
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (9) case eDestType ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_slCastEntry.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_slCastEntry.eDestType:SB_INT16
TEST.VALUE:switchboard.vSB_slCastEntry.slSourceData:0
TEST.EXPECTED:switchboard.vSB_slCastEntry.eDestType:SB_INT16
TEST.EXPECTED:switchboard.vSB_slCastEntry.slSourceData:0
TEST.END

-- Test Case: Use_For_Int_SL_Int32
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_slCastEntry
TEST.NEW
TEST.NAME:Use_For_Int_SL_Int32
TEST.COMPOUND_ONLY
TEST.BASIS_PATH:1 of 9
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (9) case eDestType ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_slCastEntry.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_slCastEntry.eDestType:SB_INT32
TEST.VALUE:switchboard.vSB_slCastEntry.slSourceData:0
TEST.EXPECTED:switchboard.vSB_slCastEntry.eDestType:SB_INT32
TEST.EXPECTED:switchboard.vSB_slCastEntry.slSourceData:0
TEST.END

-- Test Case: Use_For_Int_SL_Int8
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_slCastEntry
TEST.NEW
TEST.NAME:Use_For_Int_SL_Int8
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (9) case eDestType ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_slCastEntry.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_slCastEntry.eDestType:SB_INT8
TEST.VALUE:switchboard.vSB_slCastEntry.slSourceData:0
TEST.EXPECTED:switchboard.vSB_slCastEntry.eDestType:SB_INT8
TEST.EXPECTED:switchboard.vSB_slCastEntry.slSourceData:0
TEST.END

-- Test Case: Use_For_Int_SL_UInt16
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_slCastEntry
TEST.NEW
TEST.NAME:Use_For_Int_SL_UInt16
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (9) case eDestType ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_slCastEntry.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_slCastEntry.eDestType:SB_UINT16
TEST.VALUE:switchboard.vSB_slCastEntry.slSourceData:0
TEST.EXPECTED:switchboard.vSB_slCastEntry.eDestType:SB_UINT16
TEST.EXPECTED:switchboard.vSB_slCastEntry.slSourceData:0
TEST.END

-- Test Case: Use_For_Int_SL_UInt32
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_slCastEntry
TEST.NEW
TEST.NAME:Use_For_Int_SL_UInt32
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (9) case eDestType ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_slCastEntry.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_slCastEntry.eDestType:SB_UINT32
TEST.VALUE:switchboard.vSB_slCastEntry.slSourceData:0
TEST.EXPECTED:switchboard.vSB_slCastEntry.eDestType:SB_UINT32
TEST.EXPECTED:switchboard.vSB_slCastEntry.slSourceData:0
TEST.END

-- Test Case: Use_For_Int_SL_UInt8
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_slCastEntry
TEST.NEW
TEST.NAME:Use_For_Int_SL_UInt8
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (9) case eDestType ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_slCastEntry.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_slCastEntry.eDestType:SB_UINT8
TEST.VALUE:switchboard.vSB_slCastEntry.slSourceData:0
TEST.EXPECTED:switchboard.vSB_slCastEntry.eDestType:SB_UINT8
TEST.EXPECTED:switchboard.vSB_slCastEntry.slSourceData:0
TEST.END

-- Subprogram: vSB_swCastEntry

-- Test Case: Use_fOR_Int_SW_Boolean
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_swCastEntry
TEST.NEW
TEST.NAME:Use_fOR_Int_SW_Boolean
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (9) case eDestType ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_swCastEntry.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_swCastEntry.eDestType:SB_BOOLEAN
TEST.VALUE:switchboard.vSB_swCastEntry.swSourceData:0
TEST.EXPECTED:switchboard.vSB_swCastEntry.eDestType:SB_BOOLEAN
TEST.EXPECTED:switchboard.vSB_swCastEntry.swSourceData:0
TEST.END

-- Test Case: Use_fOR_Int_SW_FLOAT32
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_swCastEntry
TEST.NEW
TEST.NAME:Use_fOR_Int_SW_FLOAT32
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (9) case eDestType ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_swCastEntry.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_swCastEntry.eDestType:SB_FLOAT32
TEST.VALUE:switchboard.vSB_swCastEntry.swSourceData:0
TEST.EXPECTED:switchboard.vSB_swCastEntry.eDestType:SB_FLOAT32
TEST.EXPECTED:switchboard.vSB_swCastEntry.swSourceData:0
TEST.END

-- Test Case: Use_fOR_Int_SW_Int16
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_swCastEntry
TEST.NEW
TEST.NAME:Use_fOR_Int_SW_Int16
TEST.COMPOUND_ONLY
TEST.BASIS_PATH:1 of 9
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (9) case eDestType ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_swCastEntry.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_swCastEntry.eDestType:SB_INT16
TEST.VALUE:switchboard.vSB_swCastEntry.swSourceData:0
TEST.EXPECTED:switchboard.vSB_swCastEntry.eDestType:SB_INT16
TEST.EXPECTED:switchboard.vSB_swCastEntry.swSourceData:0
TEST.END

-- Test Case: Use_fOR_Int_SW_Int32
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_swCastEntry
TEST.NEW
TEST.NAME:Use_fOR_Int_SW_Int32
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (9) case eDestType ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_swCastEntry.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_swCastEntry.eDestType:SB_INT32
TEST.VALUE:switchboard.vSB_swCastEntry.swSourceData:0
TEST.EXPECTED:switchboard.vSB_swCastEntry.eDestType:SB_INT32
TEST.EXPECTED:switchboard.vSB_swCastEntry.swSourceData:0
TEST.END

-- Test Case: Use_fOR_Int_SW_Int8
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_swCastEntry
TEST.NEW
TEST.NAME:Use_fOR_Int_SW_Int8
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (9) case eDestType ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_swCastEntry.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_swCastEntry.eDestType:SB_INT8
TEST.VALUE:switchboard.vSB_swCastEntry.swSourceData:0
TEST.EXPECTED:switchboard.vSB_swCastEntry.eDestType:SB_INT8
TEST.EXPECTED:switchboard.vSB_swCastEntry.swSourceData:0
TEST.END

-- Test Case: Use_fOR_Int_SW_UINT16
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_swCastEntry
TEST.NEW
TEST.NAME:Use_fOR_Int_SW_UINT16
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (9) case eDestType ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_swCastEntry.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_swCastEntry.eDestType:SB_UINT16
TEST.VALUE:switchboard.vSB_swCastEntry.swSourceData:0
TEST.EXPECTED:switchboard.vSB_swCastEntry.eDestType:SB_UINT16
TEST.EXPECTED:switchboard.vSB_swCastEntry.swSourceData:0
TEST.END

-- Test Case: Use_fOR_Int_SW_UINT32
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_swCastEntry
TEST.NEW
TEST.NAME:Use_fOR_Int_SW_UINT32
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (9) case eDestType ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_swCastEntry.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_swCastEntry.eDestType:SB_UINT32
TEST.VALUE:switchboard.vSB_swCastEntry.swSourceData:0
TEST.EXPECTED:switchboard.vSB_swCastEntry.eDestType:SB_UINT32
TEST.EXPECTED:switchboard.vSB_swCastEntry.swSourceData:0
TEST.END

-- Test Case: Use_fOR_Int_SW_UINT8
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_swCastEntry
TEST.NEW
TEST.NAME:Use_fOR_Int_SW_UINT8
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (9) case eDestType ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_swCastEntry.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_swCastEntry.eDestType:SB_UINT8
TEST.VALUE:switchboard.vSB_swCastEntry.swSourceData:0
TEST.EXPECTED:switchboard.vSB_swCastEntry.eDestType:SB_UINT8
TEST.EXPECTED:switchboard.vSB_swCastEntry.swSourceData:0
TEST.END

-- Test Case: vSB_swCastEntry.001
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_swCastEntry
TEST.NEW
TEST.NAME:vSB_swCastEntry.001
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (9) case eDestType ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.END

-- Subprogram: vSB_ubCastEntry

-- Test Case: Use_For_Int_UB_Boolean
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_ubCastEntry
TEST.NEW
TEST.NAME:Use_For_Int_UB_Boolean
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (9) case eDestType ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_ubCastEntry.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_ubCastEntry.eDestType:SB_BOOLEAN
TEST.VALUE:switchboard.vSB_ubCastEntry.ubSourceData:<<MIN>>
TEST.EXPECTED:switchboard.vSB_ubCastEntry.eDestType:SB_BOOLEAN
TEST.EXPECTED:switchboard.vSB_ubCastEntry.ubSourceData:0
TEST.END

-- Test Case: Use_For_Int_UB_FLOAT32
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_ubCastEntry
TEST.NEW
TEST.NAME:Use_For_Int_UB_FLOAT32
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (9) case eDestType ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_ubCastEntry.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_ubCastEntry.eDestType:SB_FLOAT32
TEST.VALUE:switchboard.vSB_ubCastEntry.ubSourceData:<<MIN>>
TEST.EXPECTED:switchboard.vSB_ubCastEntry.eDestType:SB_FLOAT32
TEST.EXPECTED:switchboard.vSB_ubCastEntry.ubSourceData:0
TEST.END

-- Test Case: Use_For_Int_UB_INT16
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_ubCastEntry
TEST.NEW
TEST.NAME:Use_For_Int_UB_INT16
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (9) case eDestType ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_ubCastEntry.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_ubCastEntry.eDestType:SB_INT16
TEST.VALUE:switchboard.vSB_ubCastEntry.ubSourceData:<<MIN>>
TEST.EXPECTED:switchboard.vSB_ubCastEntry.eDestType:SB_INT16
TEST.EXPECTED:switchboard.vSB_ubCastEntry.ubSourceData:0
TEST.END

-- Test Case: Use_For_Int_UB_INT32
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_ubCastEntry
TEST.NEW
TEST.NAME:Use_For_Int_UB_INT32
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (9) case eDestType ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_ubCastEntry.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_ubCastEntry.eDestType:SB_INT32
TEST.VALUE:switchboard.vSB_ubCastEntry.ubSourceData:<<MIN>>
TEST.EXPECTED:switchboard.vSB_ubCastEntry.eDestType:SB_INT32
TEST.EXPECTED:switchboard.vSB_ubCastEntry.ubSourceData:0
TEST.END

-- Test Case: Use_For_Int_UB_INT8
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_ubCastEntry
TEST.NEW
TEST.NAME:Use_For_Int_UB_INT8
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (9) case eDestType ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_ubCastEntry.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_ubCastEntry.eDestType:SB_INT8
TEST.VALUE:switchboard.vSB_ubCastEntry.ubSourceData:<<MIN>>
TEST.EXPECTED:switchboard.vSB_ubCastEntry.eDestType:SB_INT8
TEST.EXPECTED:switchboard.vSB_ubCastEntry.ubSourceData:0
TEST.END

-- Test Case: Use_For_Int_UB_UINT16
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_ubCastEntry
TEST.NEW
TEST.NAME:Use_For_Int_UB_UINT16
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (9) case eDestType ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_ubCastEntry.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_ubCastEntry.eDestType:SB_UINT16
TEST.VALUE:switchboard.vSB_ubCastEntry.ubSourceData:<<MIN>>
TEST.EXPECTED:switchboard.vSB_ubCastEntry.eDestType:SB_UINT16
TEST.EXPECTED:switchboard.vSB_ubCastEntry.ubSourceData:0
TEST.END

-- Test Case: Use_For_Int_UB_UINT32
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_ubCastEntry
TEST.NEW
TEST.NAME:Use_For_Int_UB_UINT32
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (9) case eDestType ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_ubCastEntry.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_ubCastEntry.eDestType:SB_UINT32
TEST.VALUE:switchboard.vSB_ubCastEntry.ubSourceData:<<MIN>>
TEST.EXPECTED:switchboard.vSB_ubCastEntry.eDestType:SB_UINT32
TEST.EXPECTED:switchboard.vSB_ubCastEntry.ubSourceData:0
TEST.END

-- Test Case: Use_For_Int_UB_UINT8
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_ubCastEntry
TEST.NEW
TEST.NAME:Use_For_Int_UB_UINT8
TEST.COMPOUND_ONLY
TEST.BASIS_PATH:1 of 9
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (9) case eDestType ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_ubCastEntry.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_ubCastEntry.eDestType:SB_UINT8
TEST.VALUE:switchboard.vSB_ubCastEntry.ubSourceData:<<MIN>>
TEST.EXPECTED:switchboard.vSB_ubCastEntry.eDestType:SB_UINT8
TEST.EXPECTED:switchboard.vSB_ubCastEntry.ubSourceData:0
TEST.END

-- Test Case: vSB_ubCastEntry.001
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_ubCastEntry
TEST.NEW
TEST.NAME:vSB_ubCastEntry.001
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (9) case eDestType ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.END

-- Test Case: vSB_ubCastEntry.002
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_ubCastEntry
TEST.NEW
TEST.NAME:vSB_ubCastEntry.002
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (9) case eDestType ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.END

-- Subprogram: vSB_ulCastEntry

-- Test Case: Use_For_Int_UL_Boolean
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_ulCastEntry
TEST.NEW
TEST.NAME:Use_For_Int_UL_Boolean
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (9) case eDestType ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_ulCastEntry.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_ulCastEntry.eDestType:SB_BOOLEAN
TEST.VALUE:switchboard.vSB_ulCastEntry.ulSourceData:<<MIN>>
TEST.EXPECTED:switchboard.vSB_ulCastEntry.eDestType:SB_BOOLEAN
TEST.EXPECTED:switchboard.vSB_ulCastEntry.ulSourceData:0
TEST.END

-- Test Case: Use_For_Int_UL_FLOAT32
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_ulCastEntry
TEST.NEW
TEST.NAME:Use_For_Int_UL_FLOAT32
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (9) case eDestType ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_ulCastEntry.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_ulCastEntry.eDestType:SB_FLOAT32
TEST.VALUE:switchboard.vSB_ulCastEntry.ulSourceData:<<MIN>>
TEST.EXPECTED:switchboard.vSB_ulCastEntry.eDestType:SB_FLOAT32
TEST.EXPECTED:switchboard.vSB_ulCastEntry.ulSourceData:0
TEST.END

-- Test Case: Use_For_Int_UL_Int16
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_ulCastEntry
TEST.NEW
TEST.NAME:Use_For_Int_UL_Int16
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (9) case eDestType ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_ulCastEntry.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_ulCastEntry.eDestType:SB_INT16
TEST.VALUE:switchboard.vSB_ulCastEntry.ulSourceData:<<MIN>>
TEST.EXPECTED:switchboard.vSB_ulCastEntry.eDestType:SB_INT16
TEST.EXPECTED:switchboard.vSB_ulCastEntry.ulSourceData:0
TEST.END

-- Test Case: Use_For_Int_UL_Int32
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_ulCastEntry
TEST.NEW
TEST.NAME:Use_For_Int_UL_Int32
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (9) case eDestType ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_ulCastEntry.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_ulCastEntry.eDestType:SB_INT32
TEST.VALUE:switchboard.vSB_ulCastEntry.ulSourceData:<<MIN>>
TEST.EXPECTED:switchboard.vSB_ulCastEntry.eDestType:SB_INT32
TEST.EXPECTED:switchboard.vSB_ulCastEntry.ulSourceData:0
TEST.END

-- Test Case: Use_For_Int_UL_Int8
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_ulCastEntry
TEST.NEW
TEST.NAME:Use_For_Int_UL_Int8
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (9) case eDestType ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_ulCastEntry.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_ulCastEntry.eDestType:SB_INT8
TEST.VALUE:switchboard.vSB_ulCastEntry.ulSourceData:<<MIN>>
TEST.EXPECTED:switchboard.vSB_ulCastEntry.eDestType:SB_INT8
TEST.EXPECTED:switchboard.vSB_ulCastEntry.ulSourceData:0
TEST.END

-- Test Case: Use_For_Int_UL_Uint16
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_ulCastEntry
TEST.NEW
TEST.NAME:Use_For_Int_UL_Uint16
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (9) case eDestType ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_ulCastEntry.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_ulCastEntry.eDestType:SB_UINT16
TEST.VALUE:switchboard.vSB_ulCastEntry.ulSourceData:<<MIN>>
TEST.EXPECTED:switchboard.vSB_ulCastEntry.eDestType:SB_UINT16
TEST.EXPECTED:switchboard.vSB_ulCastEntry.ulSourceData:0
TEST.END

-- Test Case: Use_For_Int_UL_Uint32
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_ulCastEntry
TEST.NEW
TEST.NAME:Use_For_Int_UL_Uint32
TEST.COMPOUND_ONLY
TEST.BASIS_PATH:1 of 9
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (9) case eDestType ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_ulCastEntry.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_ulCastEntry.eDestType:SB_UINT32
TEST.VALUE:switchboard.vSB_ulCastEntry.ulSourceData:<<MIN>>
TEST.EXPECTED:switchboard.vSB_ulCastEntry.eDestType:SB_UINT32
TEST.EXPECTED:switchboard.vSB_ulCastEntry.ulSourceData:0
TEST.END

-- Test Case: Use_For_Int_UL_Uint8
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_ulCastEntry
TEST.NEW
TEST.NAME:Use_For_Int_UL_Uint8
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (9) case eDestType ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_ulCastEntry.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_ulCastEntry.eDestType:SB_UINT8
TEST.VALUE:switchboard.vSB_ulCastEntry.ulSourceData:<<MIN>>
TEST.EXPECTED:switchboard.vSB_ulCastEntry.eDestType:SB_UINT8
TEST.EXPECTED:switchboard.vSB_ulCastEntry.ulSourceData:0
TEST.END

-- Subprogram: vSB_uwCastEntry

-- Test Case: Use_For_Int_UW_Boolean
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_uwCastEntry
TEST.NEW
TEST.NAME:Use_For_Int_UW_Boolean
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (9) case eDestType ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_uwCastEntry.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_uwCastEntry.eDestType:SB_BOOLEAN
TEST.VALUE:switchboard.vSB_uwCastEntry.uwSourceData:<<MIN>>
TEST.EXPECTED:switchboard.vSB_uwCastEntry.eDestType:SB_BOOLEAN
TEST.EXPECTED:switchboard.vSB_uwCastEntry.uwSourceData:0
TEST.END

-- Test Case: Use_For_Int_UW_FLOAT32
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_uwCastEntry
TEST.NEW
TEST.NAME:Use_For_Int_UW_FLOAT32
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (9) case eDestType ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_uwCastEntry.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_uwCastEntry.eDestType:SB_FLOAT32
TEST.VALUE:switchboard.vSB_uwCastEntry.uwSourceData:<<MIN>>
TEST.EXPECTED:switchboard.vSB_uwCastEntry.eDestType:SB_FLOAT32
TEST.EXPECTED:switchboard.vSB_uwCastEntry.uwSourceData:0
TEST.END

-- Test Case: Use_For_Int_UW_INT16
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_uwCastEntry
TEST.NEW
TEST.NAME:Use_For_Int_UW_INT16
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (9) case eDestType ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_uwCastEntry.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_uwCastEntry.eDestType:SB_INT16
TEST.VALUE:switchboard.vSB_uwCastEntry.uwSourceData:<<MIN>>
TEST.EXPECTED:switchboard.vSB_uwCastEntry.eDestType:SB_INT16
TEST.EXPECTED:switchboard.vSB_uwCastEntry.uwSourceData:0
TEST.END

-- Test Case: Use_For_Int_UW_INT32
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_uwCastEntry
TEST.NEW
TEST.NAME:Use_For_Int_UW_INT32
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (9) case eDestType ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_uwCastEntry.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_uwCastEntry.eDestType:SB_INT32
TEST.VALUE:switchboard.vSB_uwCastEntry.uwSourceData:<<MIN>>
TEST.EXPECTED:switchboard.vSB_uwCastEntry.eDestType:SB_INT32
TEST.EXPECTED:switchboard.vSB_uwCastEntry.uwSourceData:0
TEST.END

-- Test Case: Use_For_Int_UW_INT8
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_uwCastEntry
TEST.NEW
TEST.NAME:Use_For_Int_UW_INT8
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (9) case eDestType ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_uwCastEntry.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_uwCastEntry.eDestType:SB_INT8
TEST.VALUE:switchboard.vSB_uwCastEntry.uwSourceData:<<MIN>>
TEST.EXPECTED:switchboard.vSB_uwCastEntry.eDestType:SB_INT8
TEST.EXPECTED:switchboard.vSB_uwCastEntry.uwSourceData:0
TEST.END

-- Test Case: Use_For_Int_UW_Uint16
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_uwCastEntry
TEST.NEW
TEST.NAME:Use_For_Int_UW_Uint16
TEST.COMPOUND_ONLY
TEST.BASIS_PATH:1 of 9
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (9) case eDestType ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_uwCastEntry.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_uwCastEntry.eDestType:SB_UINT16
TEST.VALUE:switchboard.vSB_uwCastEntry.uwSourceData:<<MIN>>
TEST.EXPECTED:switchboard.vSB_uwCastEntry.eDestType:SB_UINT16
TEST.EXPECTED:switchboard.vSB_uwCastEntry.uwSourceData:0
TEST.END

-- Test Case: Use_For_Int_UW_Uint32
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_uwCastEntry
TEST.NEW
TEST.NAME:Use_For_Int_UW_Uint32
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (9) case eDestType ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_uwCastEntry.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_uwCastEntry.eDestType:SB_UINT32
TEST.VALUE:switchboard.vSB_uwCastEntry.uwSourceData:<<MIN>>
TEST.EXPECTED:switchboard.vSB_uwCastEntry.eDestType:SB_UINT32
TEST.EXPECTED:switchboard.vSB_uwCastEntry.uwSourceData:0
TEST.END

-- Test Case: Use_For_Int_UW_Uint8
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_uwCastEntry
TEST.NEW
TEST.NAME:Use_For_Int_UW_Uint8
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (9) case eDestType ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:switchboard.vSB_uwCastEntry.pvDest:VECTORCAST_INT1
TEST.VALUE:switchboard.vSB_uwCastEntry.eDestType:SB_UINT8
TEST.VALUE:switchboard.vSB_uwCastEntry.uwSourceData:<<MIN>>
TEST.EXPECTED:switchboard.vSB_uwCastEntry.eDestType:SB_UINT8
TEST.EXPECTED:switchboard.vSB_uwCastEntry.uwSourceData:0
TEST.END

-- Test Case: vSB_uwCastEntry.001
TEST.UNIT:switchboard
TEST.SUBPROGRAM:vSB_uwCastEntry
TEST.NEW
TEST.NAME:vSB_uwCastEntry.001
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (9) case eDestType ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.END

-- COMPOUND TESTS

TEST.SUBPROGRAM:<<COMPOUND>>
TEST.NEW
TEST.NAME:<<COMPOUND>>.MatrixEntry_sb_Boolean
TEST.SLOT: "1", "switchboard", "gvSB_RouteMatrixEntry", "1", "Use_for_Integration_SB_Boolean"
TEST.SLOT: "2", "switchboard", "vSB_fCastEntry", "1", "Use_For_Integration_SB_Boolean"
TEST.SLOT: "3", "switchboard", "vSB_sbCastEntry", "1", "Use_For_Integration_SB_Boolean"
TEST.SLOT: "4", "switchboard", "vSB_ubCastEntry", "1", "Use_For_Int_UB_Boolean"
TEST.SLOT: "5", "switchboard", "vSB_swCastEntry", "1", "Use_fOR_Int_SW_Boolean"
TEST.SLOT: "6", "switchboard", "vSB_uwCastEntry", "1", "Use_For_Int_UW_Boolean"
TEST.SLOT: "7", "switchboard", "vSB_slCastEntry", "1", "Use_For_Int_SL_Boolean"
TEST.SLOT: "8", "switchboard", "vSB_ulCastEntry", "1", "Use_For_Int_UL_Boolean"
TEST.SLOT: "9", "switchboard", "vSB_rCastEntry", "1", "Use_For_Integration__SB_Boolean"
TEST.END
--

-- COMPOUND TESTS

TEST.SUBPROGRAM:<<COMPOUND>>
TEST.NEW
TEST.NAME:<<COMPOUND>>.MatrixEntry_sb_float32
TEST.SLOT: "1", "switchboard", "gvSB_RouteMatrixEntry", "1", "Use_for_Integration_SB_Float32"
TEST.SLOT: "2", "switchboard", "vSB_fCastEntry", "1", "Use_For_Integration_SB_Float32"
TEST.SLOT: "3", "switchboard", "vSB_sbCastEntry", "1", "Use_For_Integration_SB_FLOAT32"
TEST.SLOT: "4", "switchboard", "vSB_ubCastEntry", "1", "Use_For_Int_UB_FLOAT32"
TEST.SLOT: "5", "switchboard", "vSB_swCastEntry", "1", "Use_fOR_Int_SW_FLOAT32"
TEST.SLOT: "6", "switchboard", "vSB_uwCastEntry", "1", "Use_For_Int_UW_FLOAT32"
TEST.SLOT: "7", "switchboard", "vSB_slCastEntry", "1", "Use_For_Int_SL_FLOAT32"
TEST.SLOT: "8", "switchboard", "vSB_ulCastEntry", "1", "Use_For_Int_UL_FLOAT32"
TEST.SLOT: "9", "switchboard", "rSB_ApplyScaling", "1", "Use_For_Integration_SB_Float32"
TEST.END
--

-- COMPOUND TESTS

TEST.SUBPROGRAM:<<COMPOUND>>
TEST.NEW
TEST.NAME:<<COMPOUND>>.MatrixEntry_sb_int16
TEST.SLOT: "1", "switchboard", "gvSB_RouteMatrixEntry", "1", "Use_for_Integration_SB_Int16"
TEST.SLOT: "2", "switchboard", "vSB_swCastEntry", "1", "Use_fOR_Int_SW_Int16"
TEST.SLOT: "3", "switchboard", "vSB_fCastEntry", "1", "Use_For_Integration_SB_Int16"
TEST.SLOT: "4", "switchboard", "vSB_sbCastEntry", "1", "Use_For_Integration_SB_INT16"
TEST.SLOT: "5", "switchboard", "vSB_ubCastEntry", "1", "Use_For_Int_UB_INT16"
TEST.SLOT: "6", "switchboard", "vSB_uwCastEntry", "1", "Use_For_Int_UW_INT16"
TEST.SLOT: "7", "switchboard", "vSB_slCastEntry", "1", "Use_For_Int_SL_Int16"
TEST.SLOT: "8", "switchboard", "vSB_ulCastEntry", "1", "Use_For_Int_UL_Int16"
TEST.SLOT: "9", "switchboard", "vSB_rCastEntry", "1", "Use_For_Integration__SB_Iint16"
TEST.END
--

-- COMPOUND TESTS

TEST.SUBPROGRAM:<<COMPOUND>>
TEST.NEW
TEST.NAME:<<COMPOUND>>.MatrixEntry_sb_int32
TEST.SLOT: "1", "switchboard", "gvSB_RouteMatrixEntry", "1", "Use_for_Integration_SB_Int32"
TEST.SLOT: "2", "switchboard", "vSB_slCastEntry", "1", "Use_For_Int_SL_Int32"
TEST.SLOT: "3", "switchboard", "vSB_ApplyScaling", "1", "Use_For_Int_SL_Int32"
TEST.SLOT: "4", "switchboard", "vSB_sbCastEntry", "1", "Use_For_Integration_SB_INT32"
TEST.SLOT: "5", "switchboard", "vSB_ubCastEntry", "1", "Use_For_Int_UB_INT32"
TEST.SLOT: "6", "switchboard", "vSB_swCastEntry", "1", "Use_fOR_Int_SW_Int32"
TEST.SLOT: "7", "switchboard", "vSB_uwCastEntry", "1", "Use_For_Int_UW_INT32"
TEST.SLOT: "8", "switchboard", "vSB_fCastEntry", "1", "Use_For_Integration_SB_Int32"
TEST.SLOT: "9", "switchboard", "vSB_ulCastEntry", "1", "Use_For_Int_UL_Int32"
TEST.SLOT: "10", "switchboard", "vSB_rCastEntry", "1", "Use_For_Integration__SB_Int32"
TEST.END
--

-- COMPOUND TESTS

TEST.SUBPROGRAM:<<COMPOUND>>
TEST.NEW
TEST.NAME:<<COMPOUND>>.MatrixEntry_sb_int8
TEST.SLOT: "1", "switchboard", "gvSB_RouteMatrixEntry", "1", "Use_for_Integration_SB_Int8"
TEST.SLOT: "2", "switchboard", "vSB_sbCastEntry", "1", "Use_For_Integration_SB_INT8"
TEST.SLOT: "3", "switchboard", "vSB_fCastEntry", "1", "Use_For_Integration_SB_Int8"
TEST.SLOT: "4", "switchboard", "vSB_ubCastEntry", "1", "Use_For_Int_UB_INT8"
TEST.SLOT: "5", "switchboard", "vSB_swCastEntry", "1", "Use_fOR_Int_SW_Int8"
TEST.SLOT: "6", "switchboard", "vSB_uwCastEntry", "1", "Use_For_Int_UW_INT8"
TEST.SLOT: "7", "switchboard", "vSB_slCastEntry", "1", "Use_For_Int_SL_Int8"
TEST.SLOT: "8", "switchboard", "vSB_ulCastEntry", "1", "Use_For_Int_UL_Int8"
TEST.SLOT: "9", "switchboard", "vSB_rCastEntry", "1", "Use_For_Integration__SB_Iint8"
TEST.END
--

-- COMPOUND TESTS

TEST.SUBPROGRAM:<<COMPOUND>>
TEST.NEW
TEST.NAME:<<COMPOUND>>.MatrixEntry_sb_uint16
TEST.SLOT: "1", "switchboard", "gvSB_RouteMatrixEntry", "1", "Use_for_Integration_SB_Uint16"
TEST.SLOT: "2", "switchboard", "vSB_uwCastEntry", "1", "Use_For_Int_UW_Uint16"
TEST.SLOT: "3", "switchboard", "vSB_fCastEntry", "1", "Use_For_Integration_SB_Uint16"
TEST.SLOT: "4", "switchboard", "vSB_sbCastEntry", "1", "Use_For_Integration_SB_UINT16"
TEST.SLOT: "5", "switchboard", "vSB_ubCastEntry", "1", "Use_For_Int_UB_UINT16"
TEST.SLOT: "6", "switchboard", "vSB_swCastEntry", "1", "Use_fOR_Int_SW_UINT16"
TEST.SLOT: "7", "switchboard", "vSB_slCastEntry", "1", "Use_For_Int_SL_UInt16"
TEST.SLOT: "8", "switchboard", "vSB_ulCastEntry", "1", "Use_For_Int_UL_Uint16"
TEST.SLOT: "9", "switchboard", "vSB_ApplyBitManipulations", "1", "Use_For_Int_SB_Uint16"
TEST.SLOT: "10", "switchboard", "vSB_rCastEntry", "1", "Use_For_Integration__SB_Uiint16"
TEST.SLOT: "11", "switchboard", "uwSB_ApplyBitManipulations", "1", "Use_For_Int_SB_Uint16"
TEST.END
--

-- COMPOUND TESTS

TEST.SUBPROGRAM:<<COMPOUND>>
TEST.NEW
TEST.NAME:<<COMPOUND>>.MatrixEntry_sb_uint32
TEST.SLOT: "1", "switchboard", "gvSB_RouteMatrixEntry", "1", "Use_for_Integration_SB_Uint32"
TEST.SLOT: "2", "switchboard", "vSB_ulCastEntry", "1", "Use_For_Int_UL_Uint32"
TEST.SLOT: "3", "switchboard", "vSB_fCastEntry", "1", "Use_For_Integration_SB_Uiint32"
TEST.SLOT: "4", "switchboard", "vSB_sbCastEntry", "1", "Use_For_Integration_SB_UINT32"
TEST.SLOT: "5", "switchboard", "vSB_ubCastEntry", "1", "Use_For_Int_UB_UINT32"
TEST.SLOT: "6", "switchboard", "vSB_swCastEntry", "1", "Use_fOR_Int_SW_UINT32"
TEST.SLOT: "7", "switchboard", "vSB_uwCastEntry", "1", "Use_For_Int_UW_Uint32"
TEST.SLOT: "8", "switchboard", "vSB_slCastEntry", "1", "Use_For_Int_SL_UInt32"
TEST.SLOT: "9", "switchboard", "vSB_ApplyBitManipulations", "1", "Use_For_SB_Uint32"
TEST.SLOT: "10", "switchboard", "vSB_rCastEntry", "1", "Use_For_Integration__SB_Uint32"
TEST.END
--

-- COMPOUND TESTS

TEST.SUBPROGRAM:<<COMPOUND>>
TEST.NEW
TEST.NAME:<<COMPOUND>>.MatrixEntry_sb_uint8
TEST.SLOT: "1", "switchboard", "gvSB_RouteMatrixEntry", "1", "Use_for_Integration_SB_Uint8"
TEST.SLOT: "2", "switchboard", "vSB_fCastEntry", "1", "Use_For_Integration_SB_Uint8"
TEST.SLOT: "3", "switchboard", "vSB_ubCastEntry", "1", "Use_For_Int_UB_UINT8"
TEST.SLOT: "4", "switchboard", "ubSB_ApplyBitManipulations", "1", "Use_for_Integration_UB_UINT8"
TEST.SLOT: "5", "switchboard", "vSB_sbCastEntry", "1", "Use_For_Integration_SB_UINT8"
TEST.SLOT: "6", "switchboard", "vSB_swCastEntry", "1", "Use_fOR_Int_SW_UINT8"
TEST.SLOT: "7", "switchboard", "vSB_uwCastEntry", "1", "Use_For_Int_UW_Uint8"
TEST.SLOT: "8", "switchboard", "vSB_slCastEntry", "1", "Use_For_Int_SL_UInt8"
TEST.SLOT: "9", "switchboard", "vSB_ulCastEntry", "1", "Use_For_Int_UL_Uint8"
TEST.SLOT: "10", "switchboard", "vSB_rCastEntry", "1", "Use_For_Integration__SB_Uiint8"
TEST.SLOT: "11", "switchboard", "vSB_ApplyBitManipulations", "1", "Use_For_Int_SB_Uint8"
TEST.END
--
