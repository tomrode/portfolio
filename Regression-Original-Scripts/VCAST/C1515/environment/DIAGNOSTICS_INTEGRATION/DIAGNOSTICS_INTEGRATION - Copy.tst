-- VectorCAST 6.4p (01/31/17)
-- Test Case Script
-- 
-- Environment    : DIAGNOSTICS_INTEGRATION
-- Unit(s) Under Test: devrep_external_event_handler devrep_hdm devrep_iom devrep_scm devrep_tcm_hcm devrep_vcm_model devrep_vcm_supv
-- 
-- Script Features
TEST.SCRIPT_FEATURE:C_DIRECT_ARRAY_INDEXING
TEST.SCRIPT_FEATURE:CPP_CLASS_OBJECT_REVISION
TEST.SCRIPT_FEATURE:MULTIPLE_UUT_SUPPORT
TEST.SCRIPT_FEATURE:MIXED_CASE_NAMES
TEST.SCRIPT_FEATURE:STATIC_HEADER_FUNCS_IN_UUTS
--

-- Unit: devrep_external_event_handler

-- Subprogram: fProcessAllArrayEvents

-- Test Case: fProcessAllArrayEvents.001
TEST.UNIT:devrep_external_event_handler
TEST.SUBPROGRAM:fProcessAllArrayEvents
TEST.NEW
TEST.NAME:fProcessAllArrayEvents.001
TEST.NOTES:
Could not reference this pointer "pubEventInfo" with out abnormal temination  
TEST.END_NOTES:
TEST.VALUE:USER_GLOBALS_VCAST.<<GLOBAL>>.VECTORCAST_CHAR1:'1'
TEST.VALUE:devrep_external_event_handler.fProcessAllArrayEvents.pData:<<malloc 1>>
TEST.VALUE:devrep_external_event_handler.fProcessAllArrayEvents.pData[0].uwConversionMapSize:1
TEST.VALUE:devrep_external_event_handler.fProcessAllArrayEvents.pData[0].eConversionType:DEVREP_CONVERSION_ARRAY
TEST.VALUE:devrep_external_event_handler.fProcessAllArrayEvents.pData[0].uwEmergencyCode:2
TEST.VALUE:devrep_external_event_handler.fProcessAllArrayEvents.pubEventInfo:<<malloc 11>>
TEST.VALUE:devrep_external_event_handler.fProcessAllArrayEvents.pubEventInfo:"'A_String'"
TEST.VALUE_USER_CODE:devrep_external_event_handler.fProcessAllArrayEvents.pData.pData[0].puwExternalToDemConversionMap
<<devrep_external_event_handler.fProcessAllArrayEvents.pData>>[0].puwExternalToDemConversionMap = ( &VECTORCAST_CHAR1 );
TEST.END_VALUE_USER_CODE:
TEST.END

-- Subprogram: fProcessAllBitfieldEvents

-- Test Case: fProcessAllBitfieldEvents.001
TEST.UNIT:devrep_external_event_handler
TEST.SUBPROGRAM:fProcessAllBitfieldEvents
TEST.NEW
TEST.NAME:fProcessAllBitfieldEvents.001
TEST.VALUE:devrep_external_event_handler.fProcessAllBitfieldEvents.pData:<<malloc 1>>
TEST.VALUE:devrep_external_event_handler.fProcessAllBitfieldEvents.pData[0].uwConversionMapSize:1
TEST.VALUE:devrep_external_event_handler.fProcessAllBitfieldEvents.pData[0].eConversionType:DEVREP_CONVERSION_ARRAY
TEST.VALUE:devrep_external_event_handler.fProcessAllBitfieldEvents.pData[0].uwEmergencyCode:1
TEST.VALUE:devrep_external_event_handler.fProcessAllBitfieldEvents.pubEventInfo:<<malloc 11>>
TEST.VALUE:devrep_external_event_handler.fProcessAllBitfieldEvents.pubEventInfo:"'A_String'"
TEST.VALUE_USER_CODE:devrep_external_event_handler.fProcessAllBitfieldEvents.pData.pData[0].puwExternalToDemConversionMap
<<devrep_external_event_handler.fProcessAllBitfieldEvents.pData>>[0].puwExternalToDemConversionMap = ( &VECTORCAST_CHAR1 );
TEST.END_VALUE_USER_CODE:
TEST.END

-- Subprogram: fProcessLookupTableEvents

-- Test Case: fProcessLookupTableEvents.001
TEST.UNIT:devrep_external_event_handler
TEST.SUBPROGRAM:fProcessLookupTableEvents
TEST.NEW
TEST.NAME:fProcessLookupTableEvents.001
TEST.VALUE:USER_GLOBALS_VCAST.<<GLOBAL>>.VECTORCAST_CHAR1:'1'
TEST.VALUE:devrep_external_event_handler.fProcessLookupTableEvents.pData:<<malloc 1>>
TEST.VALUE:devrep_external_event_handler.fProcessLookupTableEvents.pData[0].uwConversionMapSize:1
TEST.VALUE:devrep_external_event_handler.fProcessLookupTableEvents.pData[0].eConversionType:DEVREP_CONVERSION_BITFIELD
TEST.VALUE:devrep_external_event_handler.fProcessLookupTableEvents.pData[0].uwEmergencyCode:1
TEST.VALUE:devrep_external_event_handler.fProcessLookupTableEvents.pubEventInfo:<<malloc 11>>
TEST.VALUE:devrep_external_event_handler.fProcessLookupTableEvents.pubEventInfo:"'A_String'"
TEST.VALUE_USER_CODE:devrep_external_event_handler.fProcessLookupTableEvents.pData.pData[0].puwExternalToDemConversionMap
<<devrep_external_event_handler.fProcessLookupTableEvents.pData>>[0].puwExternalToDemConversionMap = ( &VECTORCAST_INT1 );
TEST.END_VALUE_USER_CODE:
TEST.END

-- Subprogram: geDEVREP_Initialize

-- Test Case: geDEVREP_Initialize.001
TEST.UNIT:devrep_external_event_handler
TEST.SUBPROGRAM:geDEVREP_Initialize
TEST.NEW
TEST.NAME:geDEVREP_Initialize.001
TEST.COMPOUND_ONLY
TEST.STUB:devrep_vcm_model.gvDEVREP_VCM_Model_Initialize
TEST.STUB:devrep_vcm_supv.gvDEVREP_VCM_Supervisor_Initialize
TEST.VALUE:devrep_external_event_handler.geDEVREP_Initialize.pExtEvent:<<malloc 1>>
TEST.END

-- Subprogram: gfDEVREP_ProcessRecords

-- Test Case: gfDEVREP_ProcessRecords.001
TEST.UNIT:devrep_external_event_handler
TEST.SUBPROGRAM:gfDEVREP_ProcessRecords
TEST.NEW
TEST.NAME:gfDEVREP_ProcessRecords.001
TEST.VALUE:USER_GLOBALS_VCAST.<<GLOBAL>>.VECTORCAST_INT1:1
TEST.VALUE:devrep_external_event_handler.gfDEVREP_ProcessRecords.pEventData:<<malloc 1>>
TEST.VALUE:devrep_external_event_handler.gfDEVREP_ProcessRecords.pEventData[0].uwConversionMapSize:1
TEST.VALUE:devrep_external_event_handler.gfDEVREP_ProcessRecords.pEventData[0].eConversionType:DEVREP_CONVERSION_ARRAY
TEST.VALUE:devrep_external_event_handler.gfDEVREP_ProcessRecords.pEventData[0].uwEmergencyCode:1
TEST.VALUE:devrep_external_event_handler.gfDEVREP_ProcessRecords.pRecords:<<malloc 1>>
TEST.VALUE:devrep_external_event_handler.gfDEVREP_ProcessRecords.pRecords[0].uwEventReferenceID:1
TEST.VALUE:devrep_external_event_handler.gfDEVREP_ProcessRecords.pRecords[0].ubState:1
TEST.VALUE:devrep_external_event_handler.gfDEVREP_ProcessRecords.uwSize:1
TEST.VALUE_USER_CODE:devrep_external_event_handler.gfDEVREP_ProcessRecords.pEventData.pEventData[0].puwExternalToDemConversionMap
<<devrep_external_event_handler.gfDEVREP_ProcessRecords.pEventData>>[0].puwExternalToDemConversionMap = ( &VECTORCAST_INT1 );
TEST.END_VALUE_USER_CODE:
TEST.END

-- Unit: devrep_hdm

-- Subprogram: gvDEVREP_HDM_Initialize

-- Test Case: gvDEVREP_HDM_Initialize.001
TEST.UNIT:devrep_hdm
TEST.SUBPROGRAM:gvDEVREP_HDM_Initialize
TEST.NEW
TEST.NAME:gvDEVREP_HDM_Initialize.001
TEST.COMPOUND_ONLY
TEST.END

-- Subprogram: gvDEVREP_HDM_Process

-- Test Case: gvDEVREP_HDM_Process.001
TEST.UNIT:devrep_hdm
TEST.SUBPROGRAM:gvDEVREP_HDM_Process
TEST.NEW
TEST.NAME:gvDEVREP_HDM_Process.001
TEST.COMPOUND_ONLY
TEST.END

-- Unit: devrep_iom

-- Subprogram: gvDEVREP_IOM_Initialize

-- Test Case: gvDEVREP_IOM_Initialize.001
TEST.UNIT:devrep_iom
TEST.SUBPROGRAM:gvDEVREP_IOM_Initialize
TEST.NEW
TEST.NAME:gvDEVREP_IOM_Initialize.001
TEST.COMPOUND_ONLY
TEST.END

-- Subprogram: gvDEVREP_IOM_Process

-- Test Case: gvDEVREP_IOM_Process.001
TEST.UNIT:devrep_iom
TEST.SUBPROGRAM:gvDEVREP_IOM_Process
TEST.NEW
TEST.NAME:gvDEVREP_IOM_Process.001
TEST.COMPOUND_ONLY
TEST.END

-- Unit: devrep_scm

-- Subprogram: gvDEVREP_SCM_Initialize

-- Test Case: gvDEVREP_SCM_Initialize.001
TEST.UNIT:devrep_scm
TEST.SUBPROGRAM:gvDEVREP_SCM_Initialize
TEST.NEW
TEST.NAME:gvDEVREP_SCM_Initialize.001
TEST.COMPOUND_ONLY
TEST.END

-- Subprogram: gvDEVREP_SCM_Process

-- Test Case: gvDEVREP_SCM_Process.001
TEST.UNIT:devrep_scm
TEST.SUBPROGRAM:gvDEVREP_SCM_Process
TEST.NEW
TEST.NAME:gvDEVREP_SCM_Process.001
TEST.COMPOUND_ONLY
TEST.END

-- Subprogram: gvDEVREP_SCM_Process_Micro2

-- Test Case: gvDEVREP_SCM_Process_Micro2.001
TEST.UNIT:devrep_scm
TEST.SUBPROGRAM:gvDEVREP_SCM_Process_Micro2
TEST.NEW
TEST.NAME:gvDEVREP_SCM_Process_Micro2.001
TEST.COMPOUND_ONLY
TEST.END

-- Unit: devrep_tcm_hcm

-- Subprogram: gvDEVREP_HCM_Initialize

-- Test Case: gvDEVREP_HCM_Initialize.001
TEST.UNIT:devrep_tcm_hcm
TEST.SUBPROGRAM:gvDEVREP_HCM_Initialize
TEST.NEW
TEST.NAME:gvDEVREP_HCM_Initialize.001
TEST.COMPOUND_ONLY
TEST.END

-- Subprogram: gvDEVREP_HCM_Process

-- Test Case: gvDEVREP_HCM_Process.001
TEST.UNIT:devrep_tcm_hcm
TEST.SUBPROGRAM:gvDEVREP_HCM_Process
TEST.NEW
TEST.NAME:gvDEVREP_HCM_Process.001
TEST.COMPOUND_ONLY
TEST.VALUE:devrep_tcm_hcm.gvDEVREP_HCM_Process.pubEventStates:<<malloc 11>>
TEST.VALUE:devrep_tcm_hcm.gvDEVREP_HCM_Process.pubEventStates:"'A_string'"
TEST.END

-- Subprogram: gvDEVREP_SZAPI_Initialize

-- Test Case: gvDEVREP_SZAPI_Initialize.001
TEST.UNIT:devrep_tcm_hcm
TEST.SUBPROGRAM:gvDEVREP_SZAPI_Initialize
TEST.NEW
TEST.NAME:gvDEVREP_SZAPI_Initialize.001
TEST.COMPOUND_ONLY
TEST.END

-- Subprogram: gvDEVREP_SZAPI_Process

-- Test Case: gvDEVREP_SZAPI_Process.001
TEST.UNIT:devrep_tcm_hcm
TEST.SUBPROGRAM:gvDEVREP_SZAPI_Process
TEST.NEW
TEST.NAME:gvDEVREP_SZAPI_Process.001
TEST.COMPOUND_ONLY
TEST.VALUE:devrep_tcm_hcm.gvDEVREP_SZAPI_Process.pubEventStates:<<malloc 11>>
TEST.VALUE:devrep_tcm_hcm.gvDEVREP_SZAPI_Process.pubEventStates:"'A_String'"
TEST.END

-- Subprogram: gvDEVREP_TCM_Initialize

-- Test Case: gvDEVREP_TCM_Initialize.001
TEST.UNIT:devrep_tcm_hcm
TEST.SUBPROGRAM:gvDEVREP_TCM_Initialize
TEST.NEW
TEST.NAME:gvDEVREP_TCM_Initialize.001
TEST.COMPOUND_ONLY
TEST.END

-- Subprogram: gvDEVREP_TCM_Process

-- Test Case: gvDEVREP_TCM_Process.001
TEST.UNIT:devrep_tcm_hcm
TEST.SUBPROGRAM:gvDEVREP_TCM_Process
TEST.NEW
TEST.NAME:gvDEVREP_TCM_Process.001
TEST.COMPOUND_ONLY
TEST.STUB:devrep_tcm_hcm.gvDEVREP_TCM_Initialize
TEST.VALUE:devrep_tcm_hcm.gvDEVREP_TCM_Process.pubEventStates:<<malloc 9>>
TEST.VALUE:devrep_tcm_hcm.gvDEVREP_TCM_Process.pubEventStates:"'string'"
TEST.END

-- Unit: devrep_vcm_model

-- Subprogram: guwDEVREP_VCM_Model_GetEventIdFromArrayIndex

-- Test Case: guwDEVREP_VCM_Model_GetEventIdFromArrayIndex.001
TEST.UNIT:devrep_vcm_model
TEST.SUBPROGRAM:guwDEVREP_VCM_Model_GetEventIdFromArrayIndex
TEST.NEW
TEST.NAME:guwDEVREP_VCM_Model_GetEventIdFromArrayIndex.001
TEST.VALUE:devrep_vcm_model.guwDEVREP_VCM_Model_GetEventIdFromArrayIndex.eEventArrayIndex:EV_VCM_PS1_VOLTS_ORL
TEST.EXPECTED:devrep_vcm_model.guwDEVREP_VCM_Model_GetEventIdFromArrayIndex.return:65
TEST.END

-- Subprogram: gvDEVREP_VCM_Model_Initialize

-- Test Case: gvDEVREP_VCM_Model_Initialize.001
TEST.UNIT:devrep_vcm_model
TEST.SUBPROGRAM:gvDEVREP_VCM_Model_Initialize
TEST.NEW
TEST.NAME:gvDEVREP_VCM_Model_Initialize.001
TEST.COMPOUND_ONLY
TEST.END

-- Unit: devrep_vcm_supv

-- Subprogram: guwDEVREP_VCM_Supervisor_GetEventIdFromArrayIndex

-- Test Case: guwDEVREP_VCM_Supervisor_GetEventIdFromArrayIndex.001
TEST.UNIT:devrep_vcm_supv
TEST.SUBPROGRAM:guwDEVREP_VCM_Supervisor_GetEventIdFromArrayIndex
TEST.NEW
TEST.NAME:guwDEVREP_VCM_Supervisor_GetEventIdFromArrayIndex.001
TEST.END

-- Subprogram: gvDEVREP_VCM_Supervisor_Initialize

-- Test Case: gvDEVREP_VCM_Supervisor_Initialize.001
TEST.UNIT:devrep_vcm_supv
TEST.SUBPROGRAM:gvDEVREP_VCM_Supervisor_Initialize
TEST.NEW
TEST.NAME:gvDEVREP_VCM_Supervisor_Initialize.001
TEST.COMPOUND_ONLY
TEST.END

-- Subprogram: gvDEVREP_VCM_Supervisor_Process

-- Test Case: gvDEVREP_VCM_Supervisor_Process.001
TEST.UNIT:devrep_vcm_supv
TEST.SUBPROGRAM:gvDEVREP_VCM_Supervisor_Process
TEST.NEW
TEST.NAME:gvDEVREP_VCM_Supervisor_Process.001
TEST.VALUE:devrep_vcm_supv.gvDEVREP_VCM_Supervisor_Process.pEventRecords:<<malloc 1>>
TEST.END

-- COMPOUND TESTS

TEST.SUBPROGRAM:<<COMPOUND>>
TEST.NEW
TEST.NAME:gvDEVREP_HCM_Initialize
TEST.SLOT: "1", "devrep_tcm_hcm", "gvDEVREP_HCM_Initialize", "1", "gvDEVREP_HCM_Initialize.001"
TEST.SLOT: "2", "devrep_external_event_handler", "geDEVREP_Initialize", "1", "geDEVREP_Initialize.001"
TEST.END
--

-- COMPOUND TESTS

TEST.SUBPROGRAM:<<COMPOUND>>
TEST.NEW
TEST.NAME:gvDEVREP_HCM_Process
TEST.SLOT: "1", "devrep_tcm_hcm", "gvDEVREP_HCM_Initialize", "1", "gvDEVREP_HCM_Initialize.001"
TEST.SLOT: "2", "devrep_tcm_hcm", "gvDEVREP_HCM_Process", "1", "gvDEVREP_HCM_Process.001"
TEST.END
--

-- COMPOUND TESTS

TEST.SUBPROGRAM:<<COMPOUND>>
TEST.NEW
TEST.NAME:gvDEVREP_HDM_Initialize
TEST.SLOT: "1", "devrep_hdm", "gvDEVREP_HDM_Initialize", "1", "gvDEVREP_HDM_Initialize.001"
TEST.SLOT: "2", "devrep_external_event_handler", "geDEVREP_Initialize", "1", "geDEVREP_Initialize.001"
TEST.END
--

-- COMPOUND TESTS

TEST.SUBPROGRAM:<<COMPOUND>>
TEST.NEW
TEST.NAME:gvDEVREP_HDM_Process
TEST.SLOT: "1", "devrep_hdm", "gvDEVREP_HDM_Initialize", "1", "gvDEVREP_HDM_Initialize.001"
TEST.SLOT: "2", "devrep_hdm", "gvDEVREP_HDM_Process", "1", "gvDEVREP_HDM_Process.001"
TEST.END
--

-- COMPOUND TESTS

TEST.SUBPROGRAM:<<COMPOUND>>
TEST.NEW
TEST.NAME:gvDEVREP_IOM_Initialize
TEST.SLOT: "1", "devrep_iom", "gvDEVREP_IOM_Initialize", "1", "gvDEVREP_IOM_Initialize.001"
TEST.SLOT: "2", "devrep_external_event_handler", "geDEVREP_Initialize", "1", "geDEVREP_Initialize.001"
TEST.END
--

-- COMPOUND TESTS

TEST.SUBPROGRAM:<<COMPOUND>>
TEST.NEW
TEST.NAME:gvDEVREP_IOM_Process
TEST.SLOT: "1", "devrep_iom", "gvDEVREP_IOM_Initialize", "1", "gvDEVREP_IOM_Initialize.001"
TEST.SLOT: "2", "devrep_iom", "gvDEVREP_IOM_Process", "1", "gvDEVREP_IOM_Process.001"
TEST.END
--

-- COMPOUND TESTS

TEST.SUBPROGRAM:<<COMPOUND>>
TEST.NEW
TEST.NAME:gvDEVREP_SCM_Initialize
TEST.SLOT: "1", "devrep_scm", "gvDEVREP_SCM_Initialize", "1", "gvDEVREP_SCM_Initialize.001"
TEST.SLOT: "2", "devrep_external_event_handler", "geDEVREP_Initialize", "1", "geDEVREP_Initialize.001"
TEST.END
--

-- COMPOUND TESTS

TEST.SUBPROGRAM:<<COMPOUND>>
TEST.NEW
TEST.NAME:gvDEVREP_SCM_Process
TEST.SLOT: "1", "devrep_scm", "gvDEVREP_SCM_Initialize", "1", "gvDEVREP_SCM_Initialize.001"
TEST.SLOT: "2", "devrep_scm", "gvDEVREP_SCM_Process", "1", "gvDEVREP_SCM_Process.001"
TEST.END
--

-- COMPOUND TESTS

TEST.SUBPROGRAM:<<COMPOUND>>
TEST.NEW
TEST.NAME:gvDEVREP_SCM_Process_Micro2
TEST.SLOT: "1", "devrep_scm", "gvDEVREP_SCM_Initialize", "1", "gvDEVREP_SCM_Initialize.001"
TEST.SLOT: "2", "devrep_scm", "gvDEVREP_SCM_Process_Micro2", "1", "gvDEVREP_SCM_Process_Micro2.001"
TEST.END
--

-- COMPOUND TESTS

TEST.SUBPROGRAM:<<COMPOUND>>
TEST.NEW
TEST.NAME:gvDEVREP_SZAPI_Initialize
TEST.SLOT: "1", "devrep_tcm_hcm", "gvDEVREP_SZAPI_Initialize", "1", "gvDEVREP_SZAPI_Initialize.001"
TEST.SLOT: "2", "devrep_external_event_handler", "geDEVREP_Initialize", "1", "geDEVREP_Initialize.001"
TEST.END
--

-- COMPOUND TESTS

TEST.SUBPROGRAM:<<COMPOUND>>
TEST.NEW
TEST.NAME:gvDEVREP_SZAPI_Process
TEST.SLOT: "1", "devrep_tcm_hcm", "gvDEVREP_SZAPI_Initialize", "1", "gvDEVREP_SZAPI_Initialize.001"
TEST.SLOT: "2", "devrep_tcm_hcm", "gvDEVREP_SZAPI_Process", "1", "gvDEVREP_SZAPI_Process.001"
TEST.END
--

-- COMPOUND TESTS

TEST.SUBPROGRAM:<<COMPOUND>>
TEST.NEW
TEST.NAME:gvDEVREP_TCM_Initialize
TEST.SLOT: "1", "devrep_tcm_hcm", "gvDEVREP_TCM_Initialize", "1", "gvDEVREP_TCM_Initialize.001"
TEST.SLOT: "2", "devrep_external_event_handler", "geDEVREP_Initialize", "1", "geDEVREP_Initialize.001"
TEST.END
--

-- COMPOUND TESTS

TEST.SUBPROGRAM:<<COMPOUND>>
TEST.NEW
TEST.NAME:gvDEVREP_TCM_Process
TEST.SLOT: "1", "devrep_tcm_hcm", "gvDEVREP_TCM_Initialize", "1", "gvDEVREP_TCM_Initialize.001"
TEST.SLOT: "2", "devrep_tcm_hcm", "gvDEVREP_TCM_Process", "1", "gvDEVREP_TCM_Process.001"
TEST.END
--

-- COMPOUND TESTS

TEST.SUBPROGRAM:<<COMPOUND>>
TEST.NEW
TEST.NAME:gvDEVREP_VCM_Model_Initialize
TEST.SLOT: "1", "devrep_vcm_model", "gvDEVREP_VCM_Model_Initialize", "1", "gvDEVREP_VCM_Model_Initialize.001"
TEST.SLOT: "2", "devrep_external_event_handler", "geDEVREP_Initialize", "1", "geDEVREP_Initialize.001"
TEST.END
--

-- COMPOUND TESTS

TEST.SUBPROGRAM:<<COMPOUND>>
TEST.NEW
TEST.NAME:gvDEVREP_VCM_Supervisor_Initialize
TEST.SLOT: "1", "devrep_vcm_supv", "gvDEVREP_VCM_Supervisor_Initialize", "1", "gvDEVREP_VCM_Supervisor_Initialize.001"
TEST.SLOT: "2", "devrep_external_event_handler", "geDEVREP_Initialize", "1", "geDEVREP_Initialize.001"
TEST.END
--

-- COMPOUND TESTS

TEST.SUBPROGRAM:<<COMPOUND>>
TEST.NEW
TEST.NAME:gvDEVREP_VCM_Supervisor_Process
TEST.SLOT: "1", "devrep_vcm_supv", "gvDEVREP_VCM_Supervisor_Process", "1", "gvDEVREP_VCM_Supervisor_Process.001"
TEST.SLOT: "2", "devrep_external_event_handler", "gfDEVREP_ProcessRecords", "1", "gfDEVREP_ProcessRecords.001"
TEST.END
--
