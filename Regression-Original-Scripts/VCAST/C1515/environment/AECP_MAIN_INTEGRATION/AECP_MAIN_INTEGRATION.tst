-- VectorCAST 6.4p (01/31/17)
-- Test Case Script
-- 
-- Environment    : AECP_MAIN_INTEGRATION
-- Unit(s) Under Test: aecp_main imm_interface immcomm_driver
-- 
-- Script Features
TEST.SCRIPT_FEATURE:C_DIRECT_ARRAY_INDEXING
TEST.SCRIPT_FEATURE:CPP_CLASS_OBJECT_REVISION
TEST.SCRIPT_FEATURE:MULTIPLE_UUT_SUPPORT
TEST.SCRIPT_FEATURE:MIXED_CASE_NAMES
TEST.SCRIPT_FEATURE:STATIC_HEADER_FUNCS_IN_UUTS
--

-- Subprogram: eAECP_DecodeMessage

-- Test Case: eAECP_DecodeMessage.AECP_Data_Only
TEST.UNIT:aecp_main
TEST.SUBPROGRAM:eAECP_DecodeMessage
TEST.NEW
TEST.NAME:eAECP_DecodeMessage.AECP_Data_Only
TEST.COMPOUND_ONLY
TEST.VALUE:aecp_main.eAECP_DecodeMessage.pszMessage:<<malloc 58>>
TEST.VALUE:aecp_main.eAECP_DecodeMessage.pszMessage:"\2\63\60\60\61\54\60\60\60\61\54\60\60\60\65\54\143\146\3"
TEST.EXPECTED:imm_interface.<<GLOBAL>>.gDEM_ReportStructure.uwSequenceNumberAcked:5
TEST.VALUE_USER_CODE:aecp_main.eAECP_DecodeMessage.pDriverData
<<aecp_main.eAECP_DecodeMessage.pDriverData>> = ( &testdriver );
TEST.END_VALUE_USER_CODE:
TEST.END

-- Test Case: eAECP_DecodeMessage.AECP_OBJS_wData
TEST.UNIT:aecp_main
TEST.SUBPROGRAM:eAECP_DecodeMessage
TEST.NEW
TEST.NAME:eAECP_DecodeMessage.AECP_OBJS_wData
TEST.COMPOUND_ONLY
TEST.VALUE:aecp_main.eAECP_DecodeMessage.pszMessage:<<malloc 83>>
TEST.VALUE:aecp_main.eAECP_DecodeMessage.pszMessage:"\2\60\60\143\145\54\60\60\60\61\54\63\60\60\60\72\60\60\75\60\60\67\71\54\144\70\3"
TEST.EXPECTED:imm_interface.<<GLOBAL>>.imm_command_msg_obj_list[0].uwIndex:0x3000
TEST.EXPECTED:imm_interface.<<GLOBAL>>.imm_command_msg_obj_list[0].ubSubindex:0
TEST.EXPECTED:imm_interface.<<GLOBAL>>.gDEM_ReportStructure.uwNumberOfRecords:0x79
TEST.ATTRIBUTES:imm_interface.<<GLOBAL>>.imm_command_msg_obj_list[1].uwIndex:EXPECTED_BASE=16
TEST.ATTRIBUTES:imm_interface.<<GLOBAL>>.imm_command_msg_obj_list[1].ubSubindex:EXPECTED_BASE=8
TEST.VALUE_USER_CODE:aecp_main.eAECP_DecodeMessage.pDriverData
<<aecp_main.eAECP_DecodeMessage.pDriverData>> = ( &testdriver );
TEST.END_VALUE_USER_CODE:
TEST.END

-- Subprogram: eAECP_EncodeMessage

-- Test Case: eAECP_EncodeMessage.AECP_Data_Only
TEST.UNIT:aecp_main
TEST.SUBPROGRAM:eAECP_EncodeMessage
TEST.NEW
TEST.NAME:eAECP_EncodeMessage.AECP_Data_Only
TEST.COMPOUND_ONLY
TEST.BASIS_PATH:1 of 13
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (1) if ((0U) == pMessageListEntry) ==> FALSE
      (2) if (0U == uwOverrideNumberObjects) ==> FALSE
      (3) for (uwObjCount < uwObjCountMax) ==> FALSE
      (13) if (1U == pDriverData->fUseChecksum) ==> FALSE
      (14) if ((AECP_SUCCESS) == eStatus && (0U) != pMessageListEntry->vCallback) ==> FALSE
   Test Case Generation Notes:
      Cannot set variable pMessageListEntry in branch 1 since it requires user code.
      Cannot set variable pMessageListEntry in branch 3 since it requires user code.
      Cannot set variable pMessageListEntry->puwNumberObjects in branch 3 since it requires user code.
      Cannot set local variable eStatus in branch 14
TEST.END_NOTES:
TEST.VALUE:aecp_main.eAECP_EncodeMessage.uwMaxChars:512
TEST.VALUE:aecp_main.eAECP_EncodeMessage.uwOverrideNumberObjects:0
TEST.VALUE:imm_interface.<<GLOBAL>>.gvehicle_sro_word.ulTruckSro:1515
TEST.VALUE:imm_interface.<<GLOBAL>>.gvehicle_sro_word.ulTruckSro1:100
TEST.VALUE:imm_interface.<<GLOBAL>>.gvehicle_sro_word.ulTruckAdvise:2000
TEST.EXPECTED:aecp_main.eAECP_EncodeMessage.pDriverData[0].fInitialized:1
TEST.EXPECTED:aecp_main.eAECP_EncodeMessage.pDriverData[0].fUseChecksum:1
TEST.EXPECTED:aecp_main.eAECP_EncodeMessage.pDriverData[0].uwNumberObjects:334
TEST.EXPECTED:aecp_main.eAECP_EncodeMessage.pDriverData[0].uwNumberTxMessages:18
TEST.EXPECTED:aecp_main.eAECP_EncodeMessage.pDriverData[0].uwNumberRxMessages:10
TEST.EXPECTED:aecp_main.eAECP_EncodeMessage.pszMessage:"\2\61\71\66\141\54\60\60\60\64\54\60\60\60\60\60\65\145\142\54\60\60\60\60\60\60\66\64\54\60\60\60\60\60\67\144\60\54\60\60\60\60\60\60\60\60\54\67\146\3"
TEST.EXPECTED:aecp_main.eAECP_EncodeMessage.uwMaxChars:512
TEST.EXPECTED:aecp_main.eAECP_EncodeMessage.uwMessageID:6506
TEST.EXPECTED:aecp_main.eAECP_EncodeMessage.uwOverrideNumberObjects:0
TEST.EXPECTED:imm_interface.<<GLOBAL>>.gvehicle_sro_word.ulTruckSro:1515
TEST.EXPECTED:imm_interface.<<GLOBAL>>.gvehicle_sro_word.ulTruckSro1:100
TEST.EXPECTED:imm_interface.<<GLOBAL>>.gvehicle_sro_word.ulTruckAdvise:2000
TEST.ATTRIBUTES:imm_interface.<<GLOBAL>>.gvehicle_sro_word.ulMainHoistSro:DISPLAY_STATE=HIDE
TEST.ATTRIBUTES:imm_interface.<<GLOBAL>>.gvehicle_sro_word.ulMainHoistSro1:DISPLAY_STATE=HIDE
TEST.ATTRIBUTES:imm_interface.<<GLOBAL>>.gvehicle_sro_word.ulMainHoistAdvise:DISPLAY_STATE=HIDE
TEST.ATTRIBUTES:imm_interface.<<GLOBAL>>.gvehicle_sro_word.ulAuxHoistSro:DISPLAY_STATE=HIDE
TEST.ATTRIBUTES:imm_interface.<<GLOBAL>>.gvehicle_sro_word.ulAuxHoistSro1:DISPLAY_STATE=HIDE
TEST.ATTRIBUTES:imm_interface.<<GLOBAL>>.gvehicle_sro_word.ulAuxHoistAdvise:DISPLAY_STATE=HIDE
TEST.ATTRIBUTES:imm_interface.<<GLOBAL>>.gvehicle_sro_word.ulTiltSro:DISPLAY_STATE=HIDE
TEST.ATTRIBUTES:imm_interface.<<GLOBAL>>.gvehicle_sro_word.ulTiltSro1:DISPLAY_STATE=HIDE
TEST.ATTRIBUTES:imm_interface.<<GLOBAL>>.gvehicle_sro_word.ulTiltAdvise:DISPLAY_STATE=HIDE
TEST.ATTRIBUTES:imm_interface.<<GLOBAL>>.gvehicle_sro_word.ulAccy1Sro:DISPLAY_STATE=HIDE
TEST.ATTRIBUTES:imm_interface.<<GLOBAL>>.gvehicle_sro_word.ulAccy1Sro1:DISPLAY_STATE=HIDE
TEST.ATTRIBUTES:imm_interface.<<GLOBAL>>.gvehicle_sro_word.ulAccy1Advise:DISPLAY_STATE=HIDE
TEST.ATTRIBUTES:imm_interface.<<GLOBAL>>.gvehicle_sro_word.ulAccy2Sro:DISPLAY_STATE=HIDE
TEST.ATTRIBUTES:imm_interface.<<GLOBAL>>.gvehicle_sro_word.ulAccy2Sro1:DISPLAY_STATE=HIDE
TEST.ATTRIBUTES:imm_interface.<<GLOBAL>>.gvehicle_sro_word.ulAccy2Advise:DISPLAY_STATE=HIDE
TEST.ATTRIBUTES:imm_interface.<<GLOBAL>>.gvehicle_sro_word.ulAccy3Sro:DISPLAY_STATE=HIDE
TEST.ATTRIBUTES:imm_interface.<<GLOBAL>>.gvehicle_sro_word.ulAccy3Sro1:DISPLAY_STATE=HIDE
TEST.ATTRIBUTES:imm_interface.<<GLOBAL>>.gvehicle_sro_word.ulAccy3Advise:DISPLAY_STATE=HIDE
TEST.ATTRIBUTES:imm_interface.<<GLOBAL>>.gvehicle_sro_word.ulTracSro:DISPLAY_STATE=HIDE
TEST.ATTRIBUTES:imm_interface.<<GLOBAL>>.gvehicle_sro_word.ulTracSro1:DISPLAY_STATE=HIDE
TEST.ATTRIBUTES:imm_interface.<<GLOBAL>>.gvehicle_sro_word.ulTracAdvise:DISPLAY_STATE=HIDE
TEST.VALUE_USER_CODE:aecp_main.eAECP_EncodeMessage.pDriverData
<<aecp_main.eAECP_EncodeMessage.pDriverData>> = &testdriver;
<<aecp_main.eAECP_EncodeMessage.pszMessage>> = &szTxBuffer;
TEST.END_VALUE_USER_CODE:
TEST.VALUE_USER_CODE:aecp_main.eAECP_EncodeMessage.uwMessageID
<<aecp_main.eAECP_EncodeMessage.uwMessageID>> = (0x196a);
TEST.END_VALUE_USER_CODE:
TEST.END

-- Test Case: eAECP_EncodeMessage.AECP_OBJS
TEST.UNIT:aecp_main
TEST.SUBPROGRAM:eAECP_EncodeMessage
TEST.NEW
TEST.NAME:eAECP_EncodeMessage.AECP_OBJS
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (1) if ((0U) == pMessageListEntry) ==> FALSE
      (2) if (0U == uwOverrideNumberObjects) ==> FALSE
      (3) for (uwObjCount < uwObjCountMax) ==> FALSE
      (13) if (1U == pDriverData->fUseChecksum) ==> FALSE
      (14) if ((AECP_SUCCESS) == eStatus && (0U) != pMessageListEntry->vCallback) ==> FALSE
   Test Case Generation Notes:
      Cannot set variable pMessageListEntry in branch 1 since it requires user code.
      Cannot set variable pMessageListEntry in branch 3 since it requires user code.
      Cannot set variable pMessageListEntry->puwNumberObjects in branch 3 since it requires user code.
      Cannot set local variable eStatus in branch 14
TEST.END_NOTES:
TEST.VALUE:aecp_main.eAECP_EncodeMessage.uwMaxChars:512
TEST.VALUE:aecp_main.eAECP_EncodeMessage.uwOverrideNumberObjects:0
TEST.VALUE:imm_interface.<<GLOBAL>>.imm_command_msg_obj_list[0].uwIndex:0x3000
TEST.VALUE:imm_interface.<<GLOBAL>>.imm_command_msg_obj_list[0].ubSubindex:0
TEST.VALUE:imm_interface.<<GLOBAL>>.uwIMM_command_message_size:1
TEST.EXPECTED:aecp_main.eAECP_EncodeMessage.pDriverData[0].fInitialized:1
TEST.EXPECTED:aecp_main.eAECP_EncodeMessage.pDriverData[0].fUseChecksum:1
TEST.EXPECTED:aecp_main.eAECP_EncodeMessage.pDriverData[0].uwNumberObjects:334
TEST.EXPECTED:aecp_main.eAECP_EncodeMessage.pDriverData[0].uwNumberTxMessages:18
TEST.EXPECTED:aecp_main.eAECP_EncodeMessage.pDriverData[0].uwNumberRxMessages:10
TEST.EXPECTED:aecp_main.eAECP_EncodeMessage.pszMessage:"\2\60\60\143\144\54\60\60\60\61\54\63\60\60\60\72\60\60\54\143\141\3"
TEST.EXPECTED:aecp_main.eAECP_EncodeMessage.uwMaxChars:512
TEST.EXPECTED:aecp_main.eAECP_EncodeMessage.uwMessageID:0xCD
TEST.EXPECTED:aecp_main.eAECP_EncodeMessage.uwOverrideNumberObjects:0
TEST.EXPECTED:imm_interface.<<GLOBAL>>.imm_command_msg_obj_list[0].uwIndex:0x3000
TEST.EXPECTED:imm_interface.<<GLOBAL>>.imm_command_msg_obj_list[0].ubSubindex:0
TEST.EXPECTED:imm_interface.<<GLOBAL>>.uwIMM_command_message_size:1
TEST.ATTRIBUTES:aecp_main.eAECP_EncodeMessage.pszMessage[19]:EXPECTED_BASE=16
TEST.ATTRIBUTES:aecp_main.eAECP_EncodeMessage.pszMessage[20]:EXPECTED_BASE=16
TEST.ATTRIBUTES:aecp_main.eAECP_EncodeMessage.pszMessage[21]:EXPECTED_BASE=16
TEST.ATTRIBUTES:aecp_main.eAECP_EncodeMessage.pszMessage[22]:EXPECTED_BASE=16
TEST.VALUE_USER_CODE:aecp_main.eAECP_EncodeMessage.pDriverData
<<aecp_main.eAECP_EncodeMessage.pDriverData>> = &testdriver;
<<aecp_main.eAECP_EncodeMessage.pszMessage>> = &szTxBuffer;
TEST.END_VALUE_USER_CODE:
TEST.VALUE_USER_CODE:aecp_main.eAECP_EncodeMessage.uwMessageID
<<aecp_main.eAECP_EncodeMessage.uwMessageID>> = (0xCD);
TEST.END_VALUE_USER_CODE:
TEST.END

-- Test Case: eAECP_EncodeMessage.AECP_OBJS_wData
TEST.UNIT:aecp_main
TEST.SUBPROGRAM:eAECP_EncodeMessage
TEST.NEW
TEST.NAME:eAECP_EncodeMessage.AECP_OBJS_wData
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (1) if ((0U) == pMessageListEntry) ==> FALSE
      (2) if (0U == uwOverrideNumberObjects) ==> FALSE
      (3) for (uwObjCount < uwObjCountMax) ==> FALSE
      (13) if (1U == pDriverData->fUseChecksum) ==> FALSE
      (14) if ((AECP_SUCCESS) == eStatus && (0U) != pMessageListEntry->vCallback) ==> FALSE
   Test Case Generation Notes:
      Cannot set variable pMessageListEntry in branch 1 since it requires user code.
      Cannot set variable pMessageListEntry in branch 3 since it requires user code.
      Cannot set variable pMessageListEntry->puwNumberObjects in branch 3 since it requires user code.
      Cannot set local variable eStatus in branch 14
TEST.END_NOTES:
TEST.VALUE:aecp_main.eAECP_EncodeMessage.uwMaxChars:512
TEST.VALUE:aecp_main.eAECP_EncodeMessage.uwOverrideNumberObjects:0
TEST.EXPECTED:aecp_main.eAECP_EncodeMessage.pDriverData[0].fInitialized:1
TEST.EXPECTED:aecp_main.eAECP_EncodeMessage.pDriverData[0].fUseChecksum:1
TEST.EXPECTED:aecp_main.eAECP_EncodeMessage.pDriverData[0].uwNumberObjects:334
TEST.EXPECTED:aecp_main.eAECP_EncodeMessage.pDriverData[0].uwNumberTxMessages:18
TEST.EXPECTED:aecp_main.eAECP_EncodeMessage.pDriverData[0].uwNumberRxMessages:10
TEST.EXPECTED:aecp_main.eAECP_EncodeMessage.pszMessage:"\2\60\60\65\66\54\60\60\60\61\54\63\60\60\60\72\60\60\75\60\60\67\71\54\67\142\3"
TEST.EXPECTED:aecp_main.eAECP_EncodeMessage.uwMaxChars:512
TEST.EXPECTED:aecp_main.eAECP_EncodeMessage.uwMessageID:0x56
TEST.EXPECTED:aecp_main.eAECP_EncodeMessage.uwOverrideNumberObjects:0
TEST.VALUE_USER_CODE:aecp_main.eAECP_EncodeMessage.pDriverData
<<aecp_main.eAECP_EncodeMessage.pDriverData>> = &testdriver;
<<aecp_main.eAECP_EncodeMessage.pszMessage>> = &szTxBuffer;
TEST.END_VALUE_USER_CODE:
TEST.VALUE_USER_CODE:aecp_main.eAECP_EncodeMessage.uwMessageID
<<aecp_main.eAECP_EncodeMessage.uwMessageID>> = (0x56);
TEST.END_VALUE_USER_CODE:
TEST.END

-- Subprogram: geAECP_Initialize

-- Test Case: geAECP_Initialize.001
TEST.UNIT:aecp_main
TEST.SUBPROGRAM:geAECP_Initialize
TEST.NEW
TEST.NAME:geAECP_Initialize.001
TEST.COMPOUND_ONLY
TEST.VALUE:aecp_main.geAECP_Initialize.uwNumberObjects:334
TEST.VALUE:aecp_main.geAECP_Initialize.uwNumberTxMessages:18
TEST.VALUE:aecp_main.geAECP_Initialize.uwNumberRxMessages:10
TEST.VALUE:aecp_main.geAECP_Initialize.fUseChecksum:1
TEST.EXPECTED:aecp_main.geAECP_Initialize.pDriver[0].Data.fInitialized:1
TEST.EXPECTED:aecp_main.geAECP_Initialize.pDriver[0].Data.fUseChecksum:1
TEST.EXPECTED:aecp_main.geAECP_Initialize.pDriver[0].Data.uwNumberObjects:334
TEST.EXPECTED:aecp_main.geAECP_Initialize.pDriver[0].Data.uwNumberTxMessages:18
TEST.EXPECTED:aecp_main.geAECP_Initialize.pDriver[0].Data.uwNumberRxMessages:10
TEST.EXPECTED:aecp_main.geAECP_Initialize.pDriver[0].Procedures.eEncodeMessage:eAECP_EncodeMessage
TEST.EXPECTED:aecp_main.geAECP_Initialize.pDriver[0].Procedures.eDecodeMessage:eAECP_DecodeMessage
TEST.EXPECTED:aecp_main.geAECP_Initialize.pDriver[0].Procedures.fLookupObject:fAECP_LookupObject
TEST.EXPECTED:aecp_main.geAECP_Initialize.uwNumberObjects:334
TEST.EXPECTED:aecp_main.geAECP_Initialize.uwNumberTxMessages:18
TEST.EXPECTED:aecp_main.geAECP_Initialize.uwNumberRxMessages:10
TEST.EXPECTED:aecp_main.geAECP_Initialize.fUseChecksum:1
TEST.EXPECTED:aecp_main.geAECP_Initialize.return:AECP_SUCCESS
TEST.VALUE_USER_CODE:aecp_main.geAECP_Initialize.pDriver
<<aecp_main.geAECP_Initialize.pDriver>> = ( &testdriver );
<<aecp_main.geAECP_Initialize.pDriver[0].Data.fInitialized>> = ( 0 );


TEST.END_VALUE_USER_CODE:
TEST.VALUE_USER_CODE:aecp_main.geAECP_Initialize.pObjectDictionary
extern const aecp_obj_entry_t imm_obj_dictionary[];

<<aecp_main.geAECP_Initialize.pObjectDictionary>> = (&imm_obj_dictionary);
TEST.END_VALUE_USER_CODE:
TEST.VALUE_USER_CODE:aecp_main.geAECP_Initialize.pTxMessageList
extern const aecp_message_map_t imm_tx_messages[];

<<aecp_main.geAECP_Initialize.pTxMessageList>> = ( &imm_tx_messages );
TEST.END_VALUE_USER_CODE:
TEST.VALUE_USER_CODE:aecp_main.geAECP_Initialize.pRxMessageList
extern const aecp_message_map_t imm_rx_messages[];

<<aecp_main.geAECP_Initialize.pRxMessageList>> = ( &imm_rx_messages );
TEST.END_VALUE_USER_CODE:
TEST.END

-- COMPOUND TESTS

TEST.SUBPROGRAM:<<COMPOUND>>
TEST.NEW
TEST.NAME:<<COMPOUND>>.Encode_AECP_Data_Only
TEST.SLOT: "1", "aecp_main", "geAECP_Initialize", "1", "geAECP_Initialize.001"
TEST.SLOT: "2", "aecp_main", "eAECP_EncodeMessage", "1", "eAECP_EncodeMessage.AECP_Data_Only"
TEST.END
--

-- COMPOUND TESTS

TEST.SUBPROGRAM:<<COMPOUND>>
TEST.NEW
TEST.NAME:<<COMPOUND>>.Encode_AECP_OBJS
TEST.SLOT: "1", "aecp_main", "geAECP_Initialize", "1", "geAECP_Initialize.001"
TEST.SLOT: "2", "aecp_main", "eAECP_EncodeMessage", "1", "eAECP_EncodeMessage.AECP_OBJS"
TEST.END
--

-- COMPOUND TESTS

TEST.SUBPROGRAM:<<COMPOUND>>
TEST.NEW
TEST.NAME:<<COMPOUND>>.Encode_AECP_OBJS_wData
TEST.SLOT: "1", "aecp_main", "geAECP_Initialize", "1", "geAECP_Initialize.001"
TEST.SLOT: "2", "aecp_main", "eAECP_DecodeMessage", "1", "eAECP_DecodeMessage.AECP_OBJS_wData"
TEST.SLOT: "3", "aecp_main", "eAECP_EncodeMessage", "1", "eAECP_EncodeMessage.AECP_OBJS_wData"
TEST.END
--

-- COMPOUND TESTS

TEST.SUBPROGRAM:<<COMPOUND>>
TEST.NEW
TEST.NAME:<<COMPOUND>>Decode_AECP_Data_Only
TEST.SLOT: "1", "aecp_main", "geAECP_Initialize", "1", "geAECP_Initialize.001"
TEST.SLOT: "2", "aecp_main", "eAECP_DecodeMessage", "1", "eAECP_DecodeMessage.AECP_Data_Only"
TEST.END
--

-- COMPOUND TESTS

TEST.SUBPROGRAM:<<COMPOUND>>
TEST.NEW
TEST.NAME:<<COMPOUND>>Decode_AECP_OBJS_wData
TEST.SLOT: "1", "aecp_main", "geAECP_Initialize", "1", "geAECP_Initialize.001"
TEST.SLOT: "2", "aecp_main", "eAECP_DecodeMessage", "1", "eAECP_DecodeMessage.AECP_OBJS_wData"
TEST.END
--
