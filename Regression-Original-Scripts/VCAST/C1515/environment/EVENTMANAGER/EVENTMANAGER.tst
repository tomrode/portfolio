-- VectorCAST 6.4.3 (03/08/18)
-- Test Case Script
-- 
-- Environment    : EVENTMANAGER
-- Unit(s) Under Test: EventManager
-- 
-- Script Features
TEST.SCRIPT_FEATURE:C_DIRECT_ARRAY_INDEXING
TEST.SCRIPT_FEATURE:CPP_CLASS_OBJECT_REVISION
TEST.SCRIPT_FEATURE:MULTIPLE_UUT_SUPPORT
TEST.SCRIPT_FEATURE:MIXED_CASE_NAMES
TEST.SCRIPT_FEATURE:STATIC_HEADER_FUNCS_IN_UUTS
--

-- Unit: EventManager

-- Subprogram: gulDEM_Initialize

-- Test Case: gulDEM_Initialize.001
TEST.UNIT:EventManager
TEST.SUBPROGRAM:gulDEM_Initialize
TEST.NEW
TEST.NAME:gulDEM_Initialize.001
TEST.VALUE:uut_prototype_stubs.gulOS_ThreadCreate.pvEntryPoint:vDEM_TimerEntry
TEST.VALUE:uut_prototype_stubs.gulOS_ThreadCreate.return:47
TEST.END
