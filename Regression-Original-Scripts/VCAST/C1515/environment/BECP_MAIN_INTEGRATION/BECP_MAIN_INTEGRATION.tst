-- VectorCAST 6.4p (01/31/17)
-- Test Case Script
-- 
-- Environment    : BECP_MAIN_INTEGRATION
-- Unit(s) Under Test: becp_main supv_interface
-- 
-- Script Features
TEST.SCRIPT_FEATURE:C_DIRECT_ARRAY_INDEXING
TEST.SCRIPT_FEATURE:CPP_CLASS_OBJECT_REVISION
TEST.SCRIPT_FEATURE:MULTIPLE_UUT_SUPPORT
TEST.SCRIPT_FEATURE:MIXED_CASE_NAMES
TEST.SCRIPT_FEATURE:STATIC_HEADER_FUNCS_IN_UUTS
--

-- Unit: becp_main

-- Subprogram: eBECP_DecodeMessage

-- Test Case: Decode_BECP_Data_Only
TEST.UNIT:becp_main
TEST.SUBPROGRAM:eBECP_DecodeMessage
TEST.NEW
TEST.NAME:Decode_BECP_Data_Only
TEST.COMPOUND_ONLY
TEST.BASIS_PATH:1 of 26
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (1) if (0xbeefU == uwSTX && 0xfeedU == uwETX) ==> FALSE
      (27) if ((BECP_SUCCESS) == eStatus) ==> FALSE
   Test Case Generation Notes:
      Cannot set local variable eStatus in branch 27
TEST.END_NOTES:
TEST.VALUE:becp_main.eBECP_DecodeMessage.puwMessage:<<malloc 30>>
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.pDriverData[0].uwNumberObjects:282
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.pDriverData[0].uwNumberTxMessages:9
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.pDriverData[0].uwNumberRxMessages:11
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[0]:48879
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[1]:30
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[2]:256
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[3]:12
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[4]:0
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[5]:0
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[6]:16256
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[7]:0
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[8]:16384
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[9]:0
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[10]:16448
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[11]:0
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[12]:16512
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[13]:0
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[14]:16544
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[15]:0
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[16]:16576
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[17]:0
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[18]:16608
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[19]:0
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[20]:16640
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[21]:0
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[22]:16656
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[23]:0
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[24]:16672
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[25]:0
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[26]:16688
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[27]:0
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[28]:51209
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[29]:65261
TEST.VALUE_USER_CODE:becp_main.eBECP_DecodeMessage.pDriverData
<<becp_main.eBECP_DecodeMessage.pDriverData>> = &testdriver;
<<becp_main.eBECP_DecodeMessage.puwMessage>> = &uwmessage;


TEST.END_VALUE_USER_CODE:
TEST.END

-- Test Case: Decode_BECP_Data_Only.001
TEST.UNIT:becp_main
TEST.SUBPROGRAM:eBECP_DecodeMessage
TEST.NEW
TEST.NAME:Decode_BECP_Data_Only.001
TEST.COMPOUND_ONLY
TEST.BASIS_PATH:1 of 26
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (1) if (0xbeefU == uwSTX && 0xfeedU == uwETX) ==> FALSE
      (27) if ((BECP_SUCCESS) == eStatus) ==> FALSE
   Test Case Generation Notes:
      Cannot set local variable eStatus in branch 27
TEST.END_NOTES:
TEST.VALUE:becp_main.eBECP_DecodeMessage.puwMessage:<<malloc 33>>
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.pDriverData[0].uwNumberObjects:282
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.pDriverData[0].uwNumberTxMessages:9
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.pDriverData[0].uwNumberRxMessages:11
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[0]:48879
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[1]:33
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[2]:256
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[3]:15
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[4]:0
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[5]:0
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[6]:16256
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[7]:0
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[8]:16384
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[9]:0
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[10]:16448
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[11]:0
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[12]:16512
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[13]:0
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[14]:16544
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[15]:0
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[16]:16576
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[17]:0
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[18]:16608
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[19]:0
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[20]:16640
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[21]:0
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[22]:16656
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[23]:0
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[24]:16672
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[25]:0
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[26]:16688
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[27]:0
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[28]:0
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[29]:0
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[30]:0
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[31]:51215
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[32]:65261
TEST.VALUE_USER_CODE:becp_main.eBECP_DecodeMessage.pDriverData
<<becp_main.eBECP_DecodeMessage.pDriverData>> = &testdriver;
<<becp_main.eBECP_DecodeMessage.puwMessage>> = &uwmessage;


TEST.END_VALUE_USER_CODE:
TEST.END

-- Test Case: Decode_BECP_OBJ_Only
TEST.UNIT:becp_main
TEST.SUBPROGRAM:eBECP_DecodeMessage
TEST.NEW
TEST.NAME:Decode_BECP_OBJ_Only
TEST.COMPOUND_ONLY
TEST.BASIS_PATH:6 of 26
TEST.NOTES:
This is an automatically generated test case.
   Test Path 6
      (1) if (0xbeefU == uwSTX && 0xfeedU == uwETX) ==> TRUE
      (2) if (uwBECP_CalcChecksum(&(puwMessage[1U]), uwLengthCks) == uwCKS) ==> TRUE
      (3) if ((0U) == pMessageListEntry) ==> FALSE
      (4) for (uwObj < uwNumObjs) ==> TRUE
      (7) case (pMessageListEntry->eMessageType) ==> default
      (27) if ((BECP_SUCCESS) == eStatus) ==> FALSE
   Test Case Generation Notes:
      Cannot set variable pMessageListEntry in branch 3 since it requires user code.
      Cannot set variable pMessageListEntry->eMessageType in branch 5 since it requires user code.
      Cannot set variable pMessageListEntry->eMessageType in branch 6 since it requires user code.
      Cannot set variable pMessageListEntry->eMessageType in branch 8 since it requires user code.
      Cannot set variable pMessageListEntry->eMessageType in branch 9 since it requires user code.
      Cannot set eStatus due to assignment
TEST.END_NOTES:
TEST.STUB:becp_main.uwBECP_CalcChecksum
TEST.VALUE:becp_main.eBECP_DecodeMessage.pDriverData:<<malloc 1>>
TEST.VALUE:becp_main.eBECP_DecodeMessage.puwMessage:<<malloc 10>>
TEST.VALUE:becp_main.eBECP_DecodeMessage.puwNumberWords:<<malloc 1>>
TEST.VALUE:becp_main.uwBECP_CalcChecksum.return:<<MIN>>
TEST.EXPECTED:supv_interface.<<GLOBAL>>.supv_io_config_msg_obj_list[0].uwIndex:0x3004
TEST.EXPECTED:supv_interface.<<GLOBAL>>.supv_io_config_msg_obj_list[0].uwSubindex:0
TEST.END

-- Test Case: Decode_BECP_OBJ_Only.001
TEST.UNIT:becp_main
TEST.SUBPROGRAM:eBECP_DecodeMessage
TEST.NEW
TEST.NAME:Decode_BECP_OBJ_Only.001
TEST.COMPOUND_ONLY
TEST.BASIS_PATH:6 of 26
TEST.NOTES:
This is an automatically generated test case.
   Test Path 6
      (1) if (0xbeefU == uwSTX && 0xfeedU == uwETX) ==> TRUE
      (2) if (uwBECP_CalcChecksum(&(puwMessage[1U]), uwLengthCks) == uwCKS) ==> TRUE
      (3) if ((0U) == pMessageListEntry) ==> FALSE
      (4) for (uwObj < uwNumObjs) ==> TRUE
      (7) case (pMessageListEntry->eMessageType) ==> default
      (27) if ((BECP_SUCCESS) == eStatus) ==> FALSE
   Test Case Generation Notes:
      Cannot set variable pMessageListEntry in branch 3 since it requires user code.
      Cannot set variable pMessageListEntry->eMessageType in branch 5 since it requires user code.
      Cannot set variable pMessageListEntry->eMessageType in branch 6 since it requires user code.
      Cannot set variable pMessageListEntry->eMessageType in branch 8 since it requires user code.
      Cannot set variable pMessageListEntry->eMessageType in branch 9 since it requires user code.
      Cannot set eStatus due to assignment
TEST.END_NOTES:
TEST.STUB:becp_main.uwBECP_CalcChecksum
TEST.VALUE:becp_main.eBECP_DecodeMessage.pDriverData:<<malloc 1>>
TEST.VALUE:becp_main.eBECP_DecodeMessage.puwMessage:<<malloc 10>>
TEST.VALUE:becp_main.eBECP_DecodeMessage.puwNumberWords:<<malloc 1>>
TEST.VALUE:becp_main.uwBECP_CalcChecksum.return:<<MIN>>
TEST.EXPECTED:supv_interface.<<GLOBAL>>.supv_io_config_msg_obj_list[0].uwIndex:0x3004
TEST.EXPECTED:supv_interface.<<GLOBAL>>.supv_io_config_msg_obj_list[0].uwSubindex:0
TEST.END

-- Test Case: Decode_BECP_Obj_wData
TEST.UNIT:becp_main
TEST.SUBPROGRAM:eBECP_DecodeMessage
TEST.NEW
TEST.NAME:Decode_BECP_Obj_wData
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (1) if (0xbeefU == uwSTX && 0xfeedU == uwETX) ==> FALSE
      (27) if ((BECP_SUCCESS) == eStatus) ==> FALSE
   Test Case Generation Notes:
      Cannot set local variable eStatus in branch 27
TEST.END_NOTES:
TEST.VALUE:becp_main.eBECP_DecodeMessage.puwMessage:<<malloc 10>>
TEST.VALUE:becp_main.eBECP_DecodeMessage.puwNumberWords:<<malloc 1>>
TEST.VALUE:supv_interface.<<GLOBAL>>.supv_io_config_msg_obj_list[0].uwIndex:0x3003
TEST.VALUE:supv_interface.<<GLOBAL>>.supv_io_config_msg_obj_list[0].uwSubindex:0
TEST.VALUE:supv_interface.<<GLOBAL>>.uwSUPV_io_config_message_size:4
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[0]:48879
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[1]:15
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[2]:513
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[3]:4
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[4]:12291
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[5]:0
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[6]:0
TEST.EXPECTED:supv_interface.<<GLOBAL>>.supv_io_config_msg_obj_list[0].uwIndex:0x3003
TEST.EXPECTED:supv_interface.<<GLOBAL>>.supv_io_config_msg_obj_list[0].uwSubindex:0
TEST.EXPECTED:supv_interface.<<GLOBAL>>.uwSUPV_io_config_message_size:4
TEST.ATTRIBUTES:becp_main.eBECP_EncodeMessage.uwMessageID:INPUT_BASE=16,EXPECTED_BASE=16
TEST.VALUE_USER_CODE:becp_main.eBECP_DecodeMessage.pDriverData
<<becp_main.eBECP_DecodeMessage.pDriverData>> = &testdriver;
<<becp_main.eBECP_DecodeMessage.puwMessage>> = &uwmessage;





TEST.END_VALUE_USER_CODE:
TEST.END

-- Test Case: Decode_BECP_Obj_wData.001
TEST.UNIT:becp_main
TEST.SUBPROGRAM:eBECP_DecodeMessage
TEST.NEW
TEST.NAME:Decode_BECP_Obj_wData.001
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (1) if (0xbeefU == uwSTX && 0xfeedU == uwETX) ==> FALSE
      (27) if ((BECP_SUCCESS) == eStatus) ==> FALSE
   Test Case Generation Notes:
      Cannot set local variable eStatus in branch 27
TEST.END_NOTES:
TEST.VALUE:becp_main.eBECP_DecodeMessage.puwMessage:<<malloc 10>>
TEST.VALUE:becp_main.eBECP_DecodeMessage.puwNumberWords:<<malloc 1>>
TEST.VALUE:supv_interface.<<GLOBAL>>.supv_io_config_msg_obj_list[0].uwIndex:0x3003
TEST.VALUE:supv_interface.<<GLOBAL>>.supv_io_config_msg_obj_list[0].uwSubindex:0
TEST.VALUE:supv_interface.<<GLOBAL>>.uwSUPV_io_config_message_size:4
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[0]:48879
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[1]:15
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[2]:513
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[3]:4
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[4]:12291
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[5]:0
TEST.EXPECTED:becp_main.eBECP_DecodeMessage.puwMessage[6]:0
TEST.EXPECTED:supv_interface.<<GLOBAL>>.supv_io_config_msg_obj_list[0].uwIndex:0x3003
TEST.EXPECTED:supv_interface.<<GLOBAL>>.supv_io_config_msg_obj_list[0].uwSubindex:0
TEST.EXPECTED:supv_interface.<<GLOBAL>>.uwSUPV_io_config_message_size:4
TEST.ATTRIBUTES:becp_main.eBECP_EncodeMessage.uwMessageID:INPUT_BASE=16,EXPECTED_BASE=16
TEST.VALUE_USER_CODE:becp_main.eBECP_DecodeMessage.pDriverData
<<becp_main.eBECP_DecodeMessage.pDriverData>> = &testdriver;
<<becp_main.eBECP_DecodeMessage.puwMessage>> = &uwmessage;





TEST.END_VALUE_USER_CODE:
TEST.END

-- Subprogram: eBECP_EncodeMessage

-- Test Case: Encode_BECP_DATA_ONLY
TEST.UNIT:becp_main
TEST.SUBPROGRAM:eBECP_EncodeMessage
TEST.NEW
TEST.NAME:Encode_BECP_DATA_ONLY
TEST.COMPOUND_ONLY
TEST.BASIS_PATH:1 of 33
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (1) if ((0U) == pMessageListEntry) ==> FALSE
      (2) if (0U == uwNumObjects) ==> FALSE
      (3) if (uwIndex + (((1U + 1U) + 1U) + 1U) < uwBufferSize) ==> FALSE
      (4) for (uwObjCount < uwObjCountMax) ==> FALSE
      (34) if (uwIndex + (1U + 1U) <= uwBufferSize) ==> FALSE
      (35) if ((BECP_SUCCESS) == eStatus && (0U) != pMessageListEntry->vCallback) ==> FALSE
   Test Case Generation Notes:
      Cannot set variable pMessageListEntry in branch 1 since it requires user code.
      Cannot set variable pMessageListEntry in branch 3 since it requires user code.
      Cannot set variable pMessageListEntry->puwNumberObjects in branch 3 since it requires user code.
      Cannot set eStatus due to assignment
TEST.END_NOTES:
TEST.VALUE:becp_main.eBECP_EncodeMessage.puwMessage:<<malloc 30>>
TEST.VALUE:becp_main.eBECP_EncodeMessage.puwNumberWords:<<malloc 1>>
TEST.VALUE:becp_main.eBECP_EncodeMessage.uwBufferSize:128
TEST.VALUE:becp_main.eBECP_EncodeMessage.uwMessageID:0x100
TEST.VALUE:becp_main.eBECP_EncodeMessage.uwNumObjects:0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[0].rCommand:0.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[1].rCommand:1.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[2].rCommand:2.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[3].rCommand:3.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[4].rCommand:4.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[5].rCommand:5.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[6].rCommand:6.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[7].rCommand:7.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[8].rCommand:8.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[9].rCommand:9.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[10].rCommand:10.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[11].rCommand:11.0
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.pDriverData[0].uwNumberObjects:282
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.pDriverData[0].uwNumberTxMessages:9
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.pDriverData[0].uwNumberRxMessages:11
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.puwMessage[0]:48879
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.puwMessage[1]:30
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.puwMessage[2]:256
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.puwMessage[3]:12
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.puwMessage[4]:0
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.puwMessage[5]:0
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.puwMessage[6]:16256
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.puwMessage[7]:0
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.puwMessage[8]:16384
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.puwMessage[9]:0
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.puwMessage[10]:16448
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.puwMessage[11]:0
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.puwMessage[12]:16512
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.puwMessage[13]:0
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.puwMessage[14]:16544
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.puwMessage[15]:0
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.puwMessage[16]:16576
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.puwMessage[17]:0
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.puwMessage[18]:16608
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.puwMessage[19]:0
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.puwMessage[20]:16640
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.puwMessage[21]:0
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.puwMessage[22]:16656
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.puwMessage[23]:0
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.puwMessage[24]:16672
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.puwMessage[25]:0
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.puwMessage[26]:16688
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.puwMessage[27]:0
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.puwMessage[28]:51209
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.puwMessage[29]:65261
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.uwBufferSize:128
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.uwMessageID:0x100
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.uwNumObjects:0
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.return:BECP_SUCCESS
TEST.VALUE_USER_CODE:becp_main.eBECP_EncodeMessage.pDriverData
extern const becp_obj_entry_t supv_obj_dictionary[];
extern const becp_message_map_t supv_tx_messages[];
extern const becp_message_map_t supv_rx_messages[];

<<becp_main.eBECP_EncodeMessage.pDriverData>> = &testdriver;
<<becp_main.eBECP_EncodeMessage.puwMessage>> = &uwmessage;

<<becp_main.eBECP_EncodeMessage.pDriverData[0].fInitialized>> = 1;
<<becp_main.eBECP_EncodeMessage.pDriverData[0].uwNumberObjects>> = 282;
<<becp_main.eBECP_EncodeMessage.pDriverData[0].pObjectDictionary>> = &supv_obj_dictionary;
<<becp_main.eBECP_EncodeMessage.pDriverData[0].uwNumberTxMessages>> = 9;
<<becp_main.eBECP_EncodeMessage.pDriverData[0].pTxMessageList>> =  &supv_tx_messages;
<<becp_main.eBECP_EncodeMessage.pDriverData[0].uwNumberRxMessages>> = 11;
<<becp_main.eBECP_EncodeMessage.pDriverData[0].pRxMessageList>> =  &supv_rx_messages;
TEST.END_VALUE_USER_CODE:
TEST.END

-- Test Case: Encode_BECP_DATA_ONLY.001
TEST.UNIT:becp_main
TEST.SUBPROGRAM:eBECP_EncodeMessage
TEST.NEW
TEST.NAME:Encode_BECP_DATA_ONLY.001
TEST.COMPOUND_ONLY
TEST.BASIS_PATH:1 of 33
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (1) if ((0U) == pMessageListEntry) ==> FALSE
      (2) if (0U == uwNumObjects) ==> FALSE
      (3) if (uwIndex + (((1U + 1U) + 1U) + 1U) < uwBufferSize) ==> FALSE
      (4) for (uwObjCount < uwObjCountMax) ==> FALSE
      (34) if (uwIndex + (1U + 1U) <= uwBufferSize) ==> FALSE
      (35) if ((BECP_SUCCESS) == eStatus && (0U) != pMessageListEntry->vCallback) ==> FALSE
   Test Case Generation Notes:
      Cannot set variable pMessageListEntry in branch 1 since it requires user code.
      Cannot set variable pMessageListEntry in branch 3 since it requires user code.
      Cannot set variable pMessageListEntry->puwNumberObjects in branch 3 since it requires user code.
      Cannot set eStatus due to assignment
TEST.END_NOTES:
TEST.VALUE:becp_main.eBECP_EncodeMessage.puwMessage:<<malloc 33>>
TEST.VALUE:becp_main.eBECP_EncodeMessage.puwNumberWords:<<malloc 1>>
TEST.VALUE:becp_main.eBECP_EncodeMessage.uwBufferSize:128
TEST.VALUE:becp_main.eBECP_EncodeMessage.uwMessageID:0x100
TEST.VALUE:becp_main.eBECP_EncodeMessage.uwNumObjects:0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[0].rCommand:0.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[1].rCommand:1.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[2].rCommand:2.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[3].rCommand:3.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[4].rCommand:4.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[5].rCommand:5.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[6].rCommand:6.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[7].rCommand:7.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[8].rCommand:8.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[9].rCommand:9.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[10].rCommand:10.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[11].rCommand:11.0
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.pDriverData[0].uwNumberObjects:282
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.pDriverData[0].uwNumberTxMessages:9
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.pDriverData[0].uwNumberRxMessages:11
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.puwMessage[0]:48879
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.puwMessage[1]:33
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.puwMessage[2]:256
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.puwMessage[3]:15
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.puwMessage[4]:0
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.puwMessage[5]:0
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.puwMessage[6]:16256
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.puwMessage[7]:0
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.puwMessage[8]:16384
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.puwMessage[9]:0
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.puwMessage[10]:16448
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.puwMessage[11]:0
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.puwMessage[12]:16512
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.puwMessage[13]:0
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.puwMessage[14]:16544
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.puwMessage[15]:0
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.puwMessage[16]:16576
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.puwMessage[17]:0
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.puwMessage[18]:16608
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.puwMessage[19]:0
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.puwMessage[20]:16640
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.puwMessage[21]:0
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.puwMessage[22]:16656
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.puwMessage[23]:0
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.puwMessage[24]:16672
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.puwMessage[25]:0
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.puwMessage[26]:16688
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.puwMessage[27]:0
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.puwMessage[28]:0
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.puwMessage[29]:0
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.puwMessage[30]:0
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.puwMessage[31]:51215
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.puwMessage[32]:65261
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.uwBufferSize:128
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.uwMessageID:0x100
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.uwNumObjects:0
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.return:BECP_SUCCESS
TEST.VALUE_USER_CODE:becp_main.eBECP_EncodeMessage.pDriverData
extern const becp_obj_entry_t supv_obj_dictionary[];
extern const becp_message_map_t supv_tx_messages[];
extern const becp_message_map_t supv_rx_messages[];

<<becp_main.eBECP_EncodeMessage.pDriverData>> = &testdriver;
<<becp_main.eBECP_EncodeMessage.puwMessage>> = &uwmessage;

<<becp_main.eBECP_EncodeMessage.pDriverData[0].fInitialized>> = 1;
<<becp_main.eBECP_EncodeMessage.pDriverData[0].uwNumberObjects>> = 282;
<<becp_main.eBECP_EncodeMessage.pDriverData[0].pObjectDictionary>> = &supv_obj_dictionary;
<<becp_main.eBECP_EncodeMessage.pDriverData[0].uwNumberTxMessages>> = 9;
<<becp_main.eBECP_EncodeMessage.pDriverData[0].pTxMessageList>> =  &supv_tx_messages;
<<becp_main.eBECP_EncodeMessage.pDriverData[0].uwNumberRxMessages>> = 11;
<<becp_main.eBECP_EncodeMessage.pDriverData[0].pRxMessageList>> =  &supv_rx_messages;
TEST.END_VALUE_USER_CODE:
TEST.END

-- Test Case: Encode_BECP_OBJS_ONLY
TEST.UNIT:becp_main
TEST.SUBPROGRAM:eBECP_EncodeMessage
TEST.NEW
TEST.NAME:Encode_BECP_OBJS_ONLY
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (1) if ((0U) == pMessageListEntry) ==> FALSE
      (2) if (0U == uwNumObjects) ==> FALSE
      (3) if (uwIndex + (((1U + 1U) + 1U) + 1U) < uwBufferSize) ==> FALSE
      (4) for (uwObjCount < uwObjCountMax) ==> FALSE
      (34) if (uwIndex + (1U + 1U) <= uwBufferSize) ==> FALSE
      (35) if ((BECP_SUCCESS) == eStatus && (0U) != pMessageListEntry->vCallback) ==> FALSE
   Test Case Generation Notes:
      Cannot set variable pMessageListEntry in branch 1 since it requires user code.
      Cannot set variable pMessageListEntry in branch 3 since it requires user code.
      Cannot set variable pMessageListEntry->puwNumberObjects in branch 3 since it requires user code.
      Cannot set eStatus due to assignment
TEST.END_NOTES:
TEST.VALUE:becp_main.eBECP_EncodeMessage.puwMessage:<<malloc 30>>
TEST.VALUE:becp_main.eBECP_EncodeMessage.puwNumberWords:<<malloc 1>>
TEST.VALUE:becp_main.eBECP_EncodeMessage.uwBufferSize:128
TEST.VALUE:becp_main.eBECP_EncodeMessage.uwMessageID:0x200
TEST.VALUE:becp_main.eBECP_EncodeMessage.uwNumObjects:0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[0].rCommand:12.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[1].rCommand:1.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[2].rCommand:2.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[3].rCommand:3.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[4].rCommand:4.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[5].rCommand:5.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[6].rCommand:6.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[7].rCommand:7.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[8].rCommand:8.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[9].rCommand:9.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[10].rCommand:10.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[11].rCommand:11.0
TEST.VALUE:supv_interface.<<GLOBAL>>.supv_io_config_msg_obj_list[0].uwIndex:12292
TEST.VALUE:supv_interface.<<GLOBAL>>.supv_io_config_msg_obj_list[0].uwSubindex:0
TEST.VALUE:supv_interface.<<GLOBAL>>.uwSUPV_io_config_message_size:2
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.pDriverData[0].uwNumberObjects:282
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.pDriverData[0].uwNumberTxMessages:9
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.pDriverData[0].uwNumberRxMessages:11
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.uwBufferSize:128
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.uwMessageID:0x200
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.uwNumObjects:0
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.return:BECP_SUCCESS
TEST.ATTRIBUTES:supv_interface.<<GLOBAL>>.supv_io_config_msg_obj_list.supv_io_config_msg_obj_list[0].uwIndex:EXPECTED_BASE=16
TEST.VALUE_USER_CODE:becp_main.eBECP_EncodeMessage.pDriverData
extern const becp_obj_entry_t supv_obj_dictionary[];
extern const becp_message_map_t supv_tx_messages[];
extern const becp_message_map_t supv_rx_messages[];

<<becp_main.eBECP_EncodeMessage.pDriverData>> = &testdriver;
<<becp_main.eBECP_EncodeMessage.puwMessage>> = &uwmessage;

<<becp_main.eBECP_EncodeMessage.pDriverData[0].fInitialized>> = 1;
<<becp_main.eBECP_EncodeMessage.pDriverData[0].uwNumberObjects>> = 282;
<<becp_main.eBECP_EncodeMessage.pDriverData[0].pObjectDictionary>> = &supv_obj_dictionary;
<<becp_main.eBECP_EncodeMessage.pDriverData[0].uwNumberTxMessages>> = 9;
<<becp_main.eBECP_EncodeMessage.pDriverData[0].pTxMessageList>> =  &supv_tx_messages;
<<becp_main.eBECP_EncodeMessage.pDriverData[0].uwNumberRxMessages>> = 11;
<<becp_main.eBECP_EncodeMessage.pDriverData[0].pRxMessageList>> =  &supv_rx_messages;
TEST.END_VALUE_USER_CODE:
TEST.END

-- Test Case: Encode_BECP_OBJS_ONLY.001
TEST.UNIT:becp_main
TEST.SUBPROGRAM:eBECP_EncodeMessage
TEST.NEW
TEST.NAME:Encode_BECP_OBJS_ONLY.001
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (1) if ((0U) == pMessageListEntry) ==> FALSE
      (2) if (0U == uwNumObjects) ==> FALSE
      (3) if (uwIndex + (((1U + 1U) + 1U) + 1U) < uwBufferSize) ==> FALSE
      (4) for (uwObjCount < uwObjCountMax) ==> FALSE
      (34) if (uwIndex + (1U + 1U) <= uwBufferSize) ==> FALSE
      (35) if ((BECP_SUCCESS) == eStatus && (0U) != pMessageListEntry->vCallback) ==> FALSE
   Test Case Generation Notes:
      Cannot set variable pMessageListEntry in branch 1 since it requires user code.
      Cannot set variable pMessageListEntry in branch 3 since it requires user code.
      Cannot set variable pMessageListEntry->puwNumberObjects in branch 3 since it requires user code.
      Cannot set eStatus due to assignment
TEST.END_NOTES:
TEST.VALUE:becp_main.eBECP_EncodeMessage.puwMessage:<<malloc 30>>
TEST.VALUE:becp_main.eBECP_EncodeMessage.puwNumberWords:<<malloc 1>>
TEST.VALUE:becp_main.eBECP_EncodeMessage.uwBufferSize:128
TEST.VALUE:becp_main.eBECP_EncodeMessage.uwMessageID:0x200
TEST.VALUE:becp_main.eBECP_EncodeMessage.uwNumObjects:0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[0].rCommand:0.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[1].rCommand:1.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[2].rCommand:2.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[3].rCommand:3.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[4].rCommand:4.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[5].rCommand:5.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[6].rCommand:6.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[7].rCommand:7.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[8].rCommand:8.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[9].rCommand:9.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[10].rCommand:10.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[11].rCommand:11.0
TEST.VALUE:supv_interface.<<GLOBAL>>.supv_io_config_msg_obj_list[0].uwIndex:0x3004
TEST.VALUE:supv_interface.<<GLOBAL>>.supv_io_config_msg_obj_list[0].uwSubindex:0
TEST.VALUE:supv_interface.<<GLOBAL>>.uwSUPV_io_config_message_size:2
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.pDriverData[0].uwNumberObjects:282
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.pDriverData[0].uwNumberTxMessages:9
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.pDriverData[0].uwNumberRxMessages:11
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.uwBufferSize:128
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.uwMessageID:0x200
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.uwNumObjects:0
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.return:BECP_SUCCESS
TEST.ATTRIBUTES:supv_interface.<<GLOBAL>>.supv_io_config_msg_obj_list[0].uwIndex:EXPECTED_BASE=16
TEST.VALUE_USER_CODE:becp_main.eBECP_EncodeMessage.pDriverData
extern const becp_obj_entry_t supv_obj_dictionary[];
extern const becp_message_map_t supv_tx_messages[];
extern const becp_message_map_t supv_rx_messages[];

<<becp_main.eBECP_EncodeMessage.pDriverData>> = &testdriver;
<<becp_main.eBECP_EncodeMessage.puwMessage>> = &uwmessage;

<<becp_main.eBECP_EncodeMessage.pDriverData[0].fInitialized>> = 1;
<<becp_main.eBECP_EncodeMessage.pDriverData[0].uwNumberObjects>> = 282;
<<becp_main.eBECP_EncodeMessage.pDriverData[0].pObjectDictionary>> = &supv_obj_dictionary;
<<becp_main.eBECP_EncodeMessage.pDriverData[0].uwNumberTxMessages>> = 9;
<<becp_main.eBECP_EncodeMessage.pDriverData[0].pTxMessageList>> =  &supv_tx_messages;
<<becp_main.eBECP_EncodeMessage.pDriverData[0].uwNumberRxMessages>> = 11;
<<becp_main.eBECP_EncodeMessage.pDriverData[0].pRxMessageList>> =  &supv_rx_messages;
TEST.END_VALUE_USER_CODE:
TEST.END

-- Test Case: Encode_BECP_OBJS_wDATA
TEST.UNIT:becp_main
TEST.SUBPROGRAM:eBECP_EncodeMessage
TEST.NEW
TEST.NAME:Encode_BECP_OBJS_wDATA
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (1) if ((0U) == pMessageListEntry) ==> FALSE
      (2) if (0U == uwNumObjects) ==> FALSE
      (3) if (uwIndex + (((1U + 1U) + 1U) + 1U) < uwBufferSize) ==> FALSE
      (4) for (uwObjCount < uwObjCountMax) ==> FALSE
      (34) if (uwIndex + (1U + 1U) <= uwBufferSize) ==> FALSE
      (35) if ((BECP_SUCCESS) == eStatus && (0U) != pMessageListEntry->vCallback) ==> FALSE
   Test Case Generation Notes:
      Cannot set variable pMessageListEntry in branch 1 since it requires user code.
      Cannot set variable pMessageListEntry in branch 3 since it requires user code.
      Cannot set variable pMessageListEntry->puwNumberObjects in branch 3 since it requires user code.
      Cannot set eStatus due to assignment
TEST.END_NOTES:
TEST.VALUE:becp_main.eBECP_EncodeMessage.puwMessage:<<malloc 10>>
TEST.VALUE:becp_main.eBECP_EncodeMessage.puwNumberWords:<<malloc 1>>
TEST.VALUE:becp_main.eBECP_EncodeMessage.uwBufferSize:128
TEST.VALUE:becp_main.eBECP_EncodeMessage.uwMessageID:0x201
TEST.VALUE:becp_main.eBECP_EncodeMessage.uwNumObjects:0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[0].rCommand:0.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[1].rCommand:1.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[2].rCommand:2.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[3].rCommand:3.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[4].rCommand:4.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[5].rCommand:5.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[6].rCommand:6.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[7].rCommand:7.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[8].rCommand:8.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[9].rCommand:9.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[10].rCommand:10.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[11].rCommand:11.0
TEST.VALUE:supv_interface.<<GLOBAL>>.supv_io_config_msg_obj_list[0].uwIndex:0x3003
TEST.VALUE:supv_interface.<<GLOBAL>>.supv_io_config_msg_obj_list[0].uwSubindex:0
TEST.VALUE:supv_interface.<<GLOBAL>>.uwSUPV_io_config_message_size:0x4
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.pDriverData[0].uwNumberObjects:282
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.pDriverData[0].uwNumberTxMessages:9
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.pDriverData[0].uwNumberRxMessages:11
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.puwMessage[0]:48879
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.puwMessage[1]:15
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.puwMessage[2]:513
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.puwMessage[3]:4
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.puwMessage[4]:12291
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.uwBufferSize:128
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.uwMessageID:513
TEST.EXPECTED:becp_main.eBECP_EncodeMessage.uwNumObjects:0
TEST.EXPECTED:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.IO_Config.Pot2.eVoltageSelect:VOLTAGE_SELECT_5V
TEST.ATTRIBUTES:supv_interface.<<GLOBAL>>.supv_io_config_msg_obj_list[0].uwIndex:EXPECTED_BASE=16
TEST.ATTRIBUTES:supv_interface.<<GLOBAL>>.uwSUPV_io_config_message_size:EXPECTED_BASE=16
TEST.VALUE_USER_CODE:becp_main.eBECP_EncodeMessage.pDriverData
extern const becp_obj_entry_t supv_obj_dictionary[];
extern const becp_message_map_t supv_tx_messages[];
extern const becp_message_map_t supv_rx_messages[];

<<becp_main.eBECP_EncodeMessage.pDriverData>> = &testdriver;
<<becp_main.eBECP_EncodeMessage.puwMessage>> = &uwmessage;

<<becp_main.eBECP_EncodeMessage.pDriverData[0].fInitialized>> = 1;
<<becp_main.eBECP_EncodeMessage.pDriverData[0].uwNumberObjects>> = 282;
<<becp_main.eBECP_EncodeMessage.pDriverData[0].pObjectDictionary>> = &supv_obj_dictionary;
<<becp_main.eBECP_EncodeMessage.pDriverData[0].uwNumberTxMessages>> = 9;
<<becp_main.eBECP_EncodeMessage.pDriverData[0].pTxMessageList>> =  &supv_tx_messages;
<<becp_main.eBECP_EncodeMessage.pDriverData[0].uwNumberRxMessages>> = 11;
<<becp_main.eBECP_EncodeMessage.pDriverData[0].pRxMessageList>> =  &supv_rx_messages;
TEST.END_VALUE_USER_CODE:
TEST.END

-- Subprogram: geBECP_Initialize

-- Test Case: Initialize_Use_for_Integration_OBJs_Only
TEST.UNIT:becp_main
TEST.SUBPROGRAM:geBECP_Initialize
TEST.NEW
TEST.NAME:Initialize_Use_for_Integration_OBJs_Only
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (1) if ((0U) == pDriver) ==> FALSE
      (2) if (1U == (pDriver->Data).fInitialized) ==> FALSE
      (3) if ((0U) == pObjectDictionary) ==> FALSE
      (4) if (0U == uwNumberObjects) ==> FALSE
      (5) if (0U == uwNumberTxMessages) ==> FALSE
      (6) if ((0U) == pTxMessageList) ==> FALSE
      (7) if (0U == uwNumberRxMessages) ==> FALSE
      (8) if ((0U) == pRxMessageList) ==> FALSE
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:becp_main.geBECP_Initialize.pDriver:<<malloc 1>>
TEST.VALUE:becp_main.geBECP_Initialize.pDriver[0].Data.fInitialized:0
TEST.VALUE:becp_main.geBECP_Initialize.pDriver[0].Data.uwNumberObjects:282
TEST.VALUE:becp_main.geBECP_Initialize.pDriver[0].Data.uwNumberTxMessages:9
TEST.VALUE:becp_main.geBECP_Initialize.pDriver[0].Procedures.eEncodeMessage:eBECP_EncodeMessage
TEST.VALUE:becp_main.geBECP_Initialize.pDriver[0].Procedures.eDecodeMessage:eBECP_DecodeMessage
TEST.VALUE:becp_main.geBECP_Initialize.pDriver[0].Procedures.fLookupObject:fBECP_LookupObject
TEST.VALUE:becp_main.geBECP_Initialize.uwNumberObjects:282
TEST.VALUE:becp_main.geBECP_Initialize.uwNumberTxMessages:9
TEST.VALUE:becp_main.geBECP_Initialize.uwNumberRxMessages:11
TEST.VALUE:becp_main.geBECP_Initialize.return:BECP_SUCCESS
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[0].rCommand:0.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[1].rCommand:1.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[2].rCommand:2.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[3].rCommand:3.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[4].rCommand:4.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[5].rCommand:5.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[6].rCommand:6.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[7].rCommand:7.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[8].rCommand:8.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[9].rCommand:9.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[10].rCommand:10.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[11].rCommand:11.0
TEST.EXPECTED:becp_main.geBECP_Initialize.pDriver[0].Data.uwNumberObjects:282
TEST.EXPECTED:becp_main.geBECP_Initialize.pDriver[0].Data.uwNumberTxMessages:9
TEST.EXPECTED:becp_main.geBECP_Initialize.pDriver[0].Data.uwNumberRxMessages:11
TEST.EXPECTED:becp_main.geBECP_Initialize.pDriver[0].Procedures.eEncodeMessage:eBECP_EncodeMessage
TEST.EXPECTED:becp_main.geBECP_Initialize.pDriver[0].Procedures.eDecodeMessage:eBECP_DecodeMessage
TEST.EXPECTED:becp_main.geBECP_Initialize.pDriver[0].Procedures.fLookupObject:fBECP_LookupObject
TEST.EXPECTED:becp_main.geBECP_Initialize.uwNumberObjects:282
TEST.EXPECTED:becp_main.geBECP_Initialize.uwNumberTxMessages:9
TEST.EXPECTED:becp_main.geBECP_Initialize.uwNumberRxMessages:11
TEST.EXPECTED:becp_main.geBECP_Initialize.return:BECP_SUCCESS
TEST.VALUE_USER_CODE:becp_main.geBECP_Initialize.pDriver.pDriver[0].Data.pObjectDictionary
extern const becp_obj_entry_t supv_obj_dictionary[];
<<becp_main.geBECP_Initialize.pDriver>>[0].Data.pObjectDictionary = ( &supv_obj_dictionary );
TEST.END_VALUE_USER_CODE:
TEST.VALUE_USER_CODE:becp_main.geBECP_Initialize.pObjectDictionary
extern const becp_obj_entry_t supv_obj_dictionary[];
<<becp_main.geBECP_Initialize.pObjectDictionary>> = ( &supv_obj_dictionary );
TEST.END_VALUE_USER_CODE:
TEST.VALUE_USER_CODE:becp_main.geBECP_Initialize.pTxMessageList
extern const becp_message_map_t supv_tx_messages[]; 
<<becp_main.geBECP_Initialize.pTxMessageList>> = ( &supv_tx_messages );
TEST.END_VALUE_USER_CODE:
TEST.VALUE_USER_CODE:becp_main.geBECP_Initialize.pRxMessageList
extern const becp_message_map_t supv_rx_messages[];
<<becp_main.geBECP_Initialize.pRxMessageList>> = (&supv_rx_messages);
TEST.END_VALUE_USER_CODE:
TEST.END

-- Test Case: Initialize_Use_for_Integration_OBJs_wData
TEST.UNIT:becp_main
TEST.SUBPROGRAM:geBECP_Initialize
TEST.NEW
TEST.NAME:Initialize_Use_for_Integration_OBJs_wData
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (1) if ((0U) == pDriver) ==> FALSE
      (2) if (1U == (pDriver->Data).fInitialized) ==> FALSE
      (3) if ((0U) == pObjectDictionary) ==> FALSE
      (4) if (0U == uwNumberObjects) ==> FALSE
      (5) if (0U == uwNumberTxMessages) ==> FALSE
      (6) if ((0U) == pTxMessageList) ==> FALSE
      (7) if (0U == uwNumberRxMessages) ==> FALSE
      (8) if ((0U) == pRxMessageList) ==> FALSE
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:becp_main.geBECP_Initialize.pDriver:<<malloc 1>>
TEST.VALUE:becp_main.geBECP_Initialize.pDriver[0].Data.fInitialized:0
TEST.VALUE:becp_main.geBECP_Initialize.pDriver[0].Data.uwNumberObjects:282
TEST.VALUE:becp_main.geBECP_Initialize.pDriver[0].Data.uwNumberTxMessages:9
TEST.VALUE:becp_main.geBECP_Initialize.pDriver[0].Data.uwNumberRxMessages:11
TEST.VALUE:becp_main.geBECP_Initialize.pDriver[0].Procedures.eEncodeMessage:eBECP_EncodeMessage
TEST.VALUE:becp_main.geBECP_Initialize.pDriver[0].Procedures.eDecodeMessage:eBECP_DecodeMessage
TEST.VALUE:becp_main.geBECP_Initialize.pDriver[0].Procedures.fLookupObject:fBECP_LookupObject
TEST.VALUE:becp_main.geBECP_Initialize.uwNumberObjects:282
TEST.VALUE:becp_main.geBECP_Initialize.uwNumberTxMessages:9
TEST.VALUE:becp_main.geBECP_Initialize.uwNumberRxMessages:11
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[0].rCommand:0.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[1].rCommand:1.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[2].rCommand:2.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[3].rCommand:3.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[4].rCommand:4.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[5].rCommand:5.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[6].rCommand:6.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[7].rCommand:7.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[8].rCommand:8.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[9].rCommand:9.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[10].rCommand:10.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[11].rCommand:11.0
TEST.VALUE:supv_interface.<<GLOBAL>>.supv_io_config_msg_obj_list[0].uwIndex:12291
TEST.VALUE:supv_interface.<<GLOBAL>>.supv_io_config_msg_obj_list[0].uwSubindex:0
TEST.EXPECTED:becp_main.geBECP_Initialize.pDriver[0].Data.uwNumberObjects:282
TEST.EXPECTED:becp_main.geBECP_Initialize.pDriver[0].Data.uwNumberTxMessages:9
TEST.EXPECTED:becp_main.geBECP_Initialize.pDriver[0].Data.uwNumberRxMessages:11
TEST.EXPECTED:becp_main.geBECP_Initialize.pDriver[0].Procedures.eEncodeMessage:eBECP_EncodeMessage
TEST.EXPECTED:becp_main.geBECP_Initialize.pDriver[0].Procedures.eDecodeMessage:eBECP_DecodeMessage
TEST.EXPECTED:becp_main.geBECP_Initialize.pDriver[0].Procedures.fLookupObject:fBECP_LookupObject
TEST.EXPECTED:becp_main.geBECP_Initialize.uwNumberObjects:282
TEST.EXPECTED:becp_main.geBECP_Initialize.uwNumberTxMessages:9
TEST.EXPECTED:becp_main.geBECP_Initialize.uwNumberRxMessages:11
TEST.EXPECTED:supv_interface.<<GLOBAL>>.supv_io_config_msg_obj_list[0].uwIndex:12291
TEST.EXPECTED:supv_interface.<<GLOBAL>>.supv_io_config_msg_obj_list[0].uwSubindex:0
TEST.VALUE_USER_CODE:becp_main.geBECP_Initialize.pDriver.pDriver[0].Data.pObjectDictionary
extern const becp_obj_entry_t supv_obj_dictionary[];
<<becp_main.geBECP_Initialize.pDriver>>[0].Data.pObjectDictionary = ( &supv_obj_dictionary );
TEST.END_VALUE_USER_CODE:
TEST.VALUE_USER_CODE:becp_main.geBECP_Initialize.pDriver.pDriver[0].Data.pTxMessageList
extern const becp_message_map_t supv_tx_messages[]; 
<<becp_main.geBECP_Initialize.pDriver>>[0].Data.pTxMessageList = ( &supv_tx_messages);

TEST.END_VALUE_USER_CODE:
TEST.VALUE_USER_CODE:becp_main.geBECP_Initialize.pDriver.pDriver[0].Data.pRxMessageList
extern const becp_message_map_t supv_rx_messages[]; 
<<becp_main.geBECP_Initialize.pDriver>>[0].Data.pRxMessageList = ( &supv_rx_messages);

TEST.END_VALUE_USER_CODE:
TEST.VALUE_USER_CODE:becp_main.geBECP_Initialize.pObjectDictionary
extern const becp_obj_entry_t supv_obj_dictionary[];
<<becp_main.geBECP_Initialize.pObjectDictionary>> = ( &supv_obj_dictionary );
TEST.END_VALUE_USER_CODE:
TEST.VALUE_USER_CODE:becp_main.geBECP_Initialize.pTxMessageList
extern const becp_message_map_t supv_tx_messages[]; 
<<becp_main.geBECP_Initialize.pTxMessageList>> = ( &supv_tx_messages );
TEST.END_VALUE_USER_CODE:
TEST.VALUE_USER_CODE:becp_main.geBECP_Initialize.pRxMessageList
extern const becp_message_map_t supv_rx_messages[];
<<becp_main.geBECP_Initialize.pRxMessageList>> = (&supv_rx_messages);
TEST.END_VALUE_USER_CODE:
TEST.END

-- Test Case: Use_For_Integration_Data_Only
TEST.UNIT:becp_main
TEST.SUBPROGRAM:geBECP_Initialize
TEST.NEW
TEST.NAME:Use_For_Integration_Data_Only
TEST.COMPOUND_ONLY
TEST.BASIS_PATH:1 of 9
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (1) if ((0U) == pDriver) ==> FALSE
      (2) if (1U == (pDriver->Data).fInitialized) ==> FALSE
      (3) if ((0U) == pObjectDictionary) ==> FALSE
      (4) if (0U == uwNumberObjects) ==> FALSE
      (5) if (0U == uwNumberTxMessages) ==> FALSE
      (6) if ((0U) == pTxMessageList) ==> FALSE
      (7) if (0U == uwNumberRxMessages) ==> FALSE
      (8) if ((0U) == pRxMessageList) ==> FALSE
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:becp_main.geBECP_Initialize.pDriver:<<malloc 1>>
TEST.VALUE:becp_main.geBECP_Initialize.pDriver[0].Data.fInitialized:0
TEST.VALUE:becp_main.geBECP_Initialize.pDriver[0].Data.uwNumberObjects:282
TEST.VALUE:becp_main.geBECP_Initialize.pDriver[0].Data.uwNumberTxMessages:9
TEST.VALUE:becp_main.geBECP_Initialize.pDriver[0].Data.uwNumberRxMessages:11
TEST.VALUE:becp_main.geBECP_Initialize.pDriver[0].Procedures.eEncodeMessage:eBECP_EncodeMessage
TEST.VALUE:becp_main.geBECP_Initialize.pDriver[0].Procedures.eDecodeMessage:eBECP_DecodeMessage
TEST.VALUE:becp_main.geBECP_Initialize.pDriver[0].Procedures.fLookupObject:fBECP_LookupObject
TEST.VALUE:becp_main.geBECP_Initialize.uwNumberObjects:282
TEST.VALUE:becp_main.geBECP_Initialize.uwNumberTxMessages:9
TEST.VALUE:becp_main.geBECP_Initialize.uwNumberRxMessages:11
TEST.VALUE:becp_main.geBECP_Initialize.return:BECP_SUCCESS
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[0].rCommand:0.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[1].rCommand:1.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[2].rCommand:2.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[3].rCommand:3.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[4].rCommand:4.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[5].rCommand:5.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[6].rCommand:6.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[7].rCommand:7.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[8].rCommand:8.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[9].rCommand:9.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[10].rCommand:10.0
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[11].rCommand:11.0
TEST.EXPECTED:becp_main.geBECP_Initialize.pDriver[0].Data.uwNumberObjects:282
TEST.EXPECTED:becp_main.geBECP_Initialize.pDriver[0].Data.uwNumberTxMessages:9
TEST.EXPECTED:becp_main.geBECP_Initialize.pDriver[0].Data.uwNumberRxMessages:11
TEST.EXPECTED:becp_main.geBECP_Initialize.pDriver[0].Procedures.eEncodeMessage:eBECP_EncodeMessage
TEST.EXPECTED:becp_main.geBECP_Initialize.pDriver[0].Procedures.eDecodeMessage:eBECP_DecodeMessage
TEST.EXPECTED:becp_main.geBECP_Initialize.pDriver[0].Procedures.fLookupObject:fBECP_LookupObject
TEST.EXPECTED:becp_main.geBECP_Initialize.uwNumberObjects:282
TEST.EXPECTED:becp_main.geBECP_Initialize.uwNumberTxMessages:9
TEST.EXPECTED:becp_main.geBECP_Initialize.uwNumberRxMessages:11
TEST.EXPECTED:becp_main.geBECP_Initialize.return:BECP_SUCCESS
TEST.ATTRIBUTES:becp_main.eBECP_EncodeMessage.uwMessageID:INPUT_BASE=16,EXPECTED_BASE=16
TEST.ATTRIBUTES:supv_interface.<<GLOBAL>>.supv_io_config_msg_obj_list[0].uwIndex:INPUT_BASE=16,EXPECTED_BASE=16
TEST.VALUE_USER_CODE:becp_main.geBECP_Initialize.pDriver.pDriver[0].Data.pTxMessageList
extern const becp_message_map_t supv_tx_messages[]; 
<<becp_main.geBECP_Initialize.pDriver>>[0].Data.pTxMessageList = ( &supv_tx_messages);

TEST.END_VALUE_USER_CODE:
TEST.VALUE_USER_CODE:becp_main.geBECP_Initialize.pDriver.pDriver[0].Data.pRxMessageList
extern const becp_message_map_t supv_rx_messages[]; 
<<becp_main.geBECP_Initialize.pDriver>>[0].Data.pRxMessageList = ( &supv_rx_messages);

TEST.END_VALUE_USER_CODE:
TEST.VALUE_USER_CODE:becp_main.geBECP_Initialize.pObjectDictionary
extern const becp_obj_entry_t supv_obj_dictionary[];
<<becp_main.geBECP_Initialize.pObjectDictionary>> = ( &supv_obj_dictionary );
TEST.END_VALUE_USER_CODE:
TEST.VALUE_USER_CODE:becp_main.geBECP_Initialize.pTxMessageList
extern const becp_message_map_t supv_tx_messages[]; 
<<becp_main.geBECP_Initialize.pTxMessageList>> = ( &supv_tx_messages );
TEST.END_VALUE_USER_CODE:
TEST.VALUE_USER_CODE:becp_main.geBECP_Initialize.pRxMessageList
extern const becp_message_map_t supv_rx_messages[];
<<becp_main.geBECP_Initialize.pRxMessageList>> = (&supv_rx_messages);
TEST.END_VALUE_USER_CODE:
TEST.END

-- COMPOUND TESTS

TEST.SUBPROGRAM:<<COMPOUND>>
TEST.NEW
TEST.NAME:<<COMPOUND>>.Encode_BECP_Data_Only
TEST.SLOT: "1", "becp_main", "geBECP_Initialize", "1", "Use_For_Integration_Data_Only"
TEST.SLOT: "2", "becp_main", "eBECP_EncodeMessage", "1", "Encode_BECP_DATA_ONLY.001"
TEST.END
--

-- COMPOUND TESTS

TEST.SUBPROGRAM:<<COMPOUND>>
TEST.NEW
TEST.NAME:<<COMPOUND>>.Encode_BECP_OBJS_Only
TEST.SLOT: "1", "becp_main", "geBECP_Initialize", "1", "Initialize_Use_for_Integration_OBJs_Only"
TEST.SLOT: "2", "becp_main", "eBECP_EncodeMessage", "1", "Encode_BECP_OBJS_ONLY.001"
TEST.END
--

-- COMPOUND TESTS

TEST.SUBPROGRAM:<<COMPOUND>>
TEST.NEW
TEST.NAME:<<COMPOUND>>.Encode_BECP_OBJS_wData
TEST.SLOT: "1", "becp_main", "geBECP_Initialize", "1", "Initialize_Use_for_Integration_OBJs_wData"
TEST.SLOT: "2", "becp_main", "eBECP_EncodeMessage", "1", "Encode_BECP_OBJS_wDATA"
TEST.END
--

-- COMPOUND TESTS

TEST.SUBPROGRAM:<<COMPOUND>>
TEST.NEW
TEST.NAME:<<COMPOUND>>Decode_BECP_Data_Only
TEST.SLOT: "1", "becp_main", "eBECP_EncodeMessage", "1", "Encode_BECP_DATA_ONLY.001"
TEST.SLOT: "2", "becp_main", "eBECP_DecodeMessage", "1", "Decode_BECP_Data_Only.001"
TEST.END
--

-- COMPOUND TESTS

TEST.SUBPROGRAM:<<COMPOUND>>
TEST.NEW
TEST.NAME:<<COMPOUND>>Decode_BECP_OBJS_Only
TEST.SLOT: "1", "becp_main", "eBECP_EncodeMessage", "1", "Encode_BECP_OBJS_ONLY.001"
TEST.SLOT: "2", "becp_main", "eBECP_DecodeMessage", "1", "Decode_BECP_OBJ_Only.001"
TEST.END
--

-- COMPOUND TESTS

TEST.SUBPROGRAM:<<COMPOUND>>
TEST.NEW
TEST.NAME:<<COMPOUND>>Decode_BECP_OBJS_wData
TEST.SLOT: "1", "becp_main", "eBECP_EncodeMessage", "1", "Encode_BECP_OBJS_wDATA"
TEST.SLOT: "2", "becp_main", "eBECP_DecodeMessage", "1", "Decode_BECP_Obj_wData.001"
TEST.END
--
