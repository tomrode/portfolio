-- VectorCAST 6.4v (08/01/17)
-- Test Case Script
-- 
-- Environment    : UTILITIES_INTEGRATION
-- Unit(s) Under Test: app_info crc debug_support flash_validate instrument stopwatch util
-- 
-- Script Features
TEST.SCRIPT_FEATURE:C_DIRECT_ARRAY_INDEXING
TEST.SCRIPT_FEATURE:CPP_CLASS_OBJECT_REVISION
TEST.SCRIPT_FEATURE:MULTIPLE_UUT_SUPPORT
TEST.SCRIPT_FEATURE:MIXED_CASE_NAMES
TEST.SCRIPT_FEATURE:STATIC_HEADER_FUNCS_IN_UUTS
--

-- Unit: app_info

-- Subprogram: gpAppInfo_GetPointer

-- Test Case: gpAppInfo_GetPointer
TEST.UNIT:app_info
TEST.SUBPROGRAM:gpAppInfo_GetPointer
TEST.NEW
TEST.NAME:gpAppInfo_GetPointer
TEST.END

-- Subprogram: gvAppInfo_Initialize

-- Test Case: gvAppInfo_Initialize
TEST.UNIT:app_info
TEST.SUBPROGRAM:gvAppInfo_Initialize
TEST.NEW
TEST.NAME:gvAppInfo_Initialize
TEST.VALUE:app_info.<<GLOBAL>>.CRC:0x1234
TEST.VALUE:app_info.<<GLOBAL>>.CKS:0x1234
TEST.EXPECTED:app_info.<<GLOBAL>>.sAppInfo.uwAppCRC:0x1234
TEST.EXPECTED:app_info.<<GLOBAL>>.sAppInfo.uwAppCKS:0x1234
TEST.END

-- Subprogram: pszGetVehicleTypeString

-- Test Case: pszGetVehicleTypeString.001
TEST.UNIT:app_info
TEST.SUBPROGRAM:pszGetVehicleTypeString
TEST.NEW
TEST.NAME:pszGetVehicleTypeString.001
TEST.END

-- Unit: debug_support

-- Subprogram: gulPrintf1_b

-- Test Case: gulPrintf1_b
TEST.UNIT:debug_support
TEST.SUBPROGRAM:gulPrintf1_b
TEST.NEW
TEST.NAME:gulPrintf1_b
TEST.VALUE:debug_support.gulPrintf1_b.pszString:<<malloc 6>>
TEST.VALUE:debug_support.gulPrintf1_b.pszString:"Hello"
TEST.VALUE:debug_support.gulPrintf1_b.pvArg1:VECTORCAST_INT1
TEST.EXPECTED:debug_support.gulPrintf1_b.return:0
TEST.END

-- Subprogram: gulPrintf2_b

-- Test Case: gulPrintf2_b
TEST.UNIT:debug_support
TEST.SUBPROGRAM:gulPrintf2_b
TEST.NEW
TEST.NAME:gulPrintf2_b
TEST.VALUE:debug_support.gulPrintf2_b.pszString:<<malloc 6>>
TEST.VALUE:debug_support.gulPrintf2_b.pszString:"Hello"
TEST.VALUE:debug_support.gulPrintf2_b.pvArg1:VECTORCAST_INT1
TEST.VALUE:debug_support.gulPrintf2_b.pvArg2:VECTORCAST_INT1
TEST.EXPECTED:debug_support.gulPrintf2_b.return:0
TEST.END

-- Subprogram: gulPrintf3_b

-- Test Case: gulPrintf3_b
TEST.UNIT:debug_support
TEST.SUBPROGRAM:gulPrintf3_b
TEST.NEW
TEST.NAME:gulPrintf3_b
TEST.VALUE:debug_support.gulPrintf3_b.pszString:<<malloc 6>>
TEST.VALUE:debug_support.gulPrintf3_b.pszString:"Hello"
TEST.VALUE:debug_support.gulPrintf3_b.pvArg1:VECTORCAST_INT1
TEST.VALUE:debug_support.gulPrintf3_b.pvArg2:VECTORCAST_INT1
TEST.VALUE:debug_support.gulPrintf3_b.pvArg3:VECTORCAST_INT1
TEST.EXPECTED:debug_support.gulPrintf3_b.return:0
TEST.END

-- Subprogram: gulPrintf4_b

-- Test Case: gulPrintf4_b
TEST.UNIT:debug_support
TEST.SUBPROGRAM:gulPrintf4_b
TEST.NEW
TEST.NAME:gulPrintf4_b
TEST.VALUE:debug_support.gulPrintf4_b.pszString:<<malloc 6>>
TEST.VALUE:debug_support.gulPrintf4_b.pszString:"Hello"
TEST.VALUE:debug_support.gulPrintf4_b.pvArg1:VECTORCAST_INT1
TEST.VALUE:debug_support.gulPrintf4_b.pvArg2:VECTORCAST_INT1
TEST.VALUE:debug_support.gulPrintf4_b.pvArg3:VECTORCAST_INT1
TEST.VALUE:debug_support.gulPrintf4_b.pvArg4:VECTORCAST_INT1
TEST.END

-- Subprogram: gulPrintf5_b

-- Test Case: gulPrintf5_b
TEST.UNIT:debug_support
TEST.SUBPROGRAM:gulPrintf5_b
TEST.NEW
TEST.NAME:gulPrintf5_b
TEST.VALUE:debug_support.gulPrintf5_b.pszString:<<malloc 6>>
TEST.VALUE:debug_support.gulPrintf5_b.pszString:"Hello"
TEST.VALUE:debug_support.gulPrintf5_b.pvArg1:VECTORCAST_INT1
TEST.VALUE:debug_support.gulPrintf5_b.pvArg2:VECTORCAST_INT1
TEST.VALUE:debug_support.gulPrintf5_b.pvArg3:VECTORCAST_INT2
TEST.VALUE:debug_support.gulPrintf5_b.pvArg4:VECTORCAST_INT1
TEST.VALUE:debug_support.gulPrintf5_b.pvArg5:VECTORCAST_INT1
TEST.END

-- Subprogram: gulPrintf6_b

-- Test Case: gulPrintf6_b
TEST.UNIT:debug_support
TEST.SUBPROGRAM:gulPrintf6_b
TEST.NEW
TEST.NAME:gulPrintf6_b
TEST.VALUE:debug_support.gulPrintf6_b.pszString:<<malloc 6>>
TEST.VALUE:debug_support.gulPrintf6_b.pszString:"Hello"
TEST.VALUE:debug_support.gulPrintf6_b.pvArg1:VECTORCAST_INT1
TEST.VALUE:debug_support.gulPrintf6_b.pvArg2:VECTORCAST_INT1
TEST.VALUE:debug_support.gulPrintf6_b.pvArg3:VECTORCAST_INT1
TEST.VALUE:debug_support.gulPrintf6_b.pvArg4:VECTORCAST_INT1
TEST.VALUE:debug_support.gulPrintf6_b.pvArg5:VECTORCAST_INT1
TEST.VALUE:debug_support.gulPrintf6_b.pvArg6:VECTORCAST_INT1
TEST.END

-- Subprogram: gulPrintf7_b

-- Test Case: gulPrintf7_b
TEST.UNIT:debug_support
TEST.SUBPROGRAM:gulPrintf7_b
TEST.NEW
TEST.NAME:gulPrintf7_b
TEST.VALUE:debug_support.gulPrintf7_b.pszString:<<malloc 6>>
TEST.VALUE:debug_support.gulPrintf7_b.pszString:"Hello"
TEST.VALUE:debug_support.gulPrintf7_b.pvArg1:VECTORCAST_INT1
TEST.VALUE:debug_support.gulPrintf7_b.pvArg2:VECTORCAST_INT1
TEST.VALUE:debug_support.gulPrintf7_b.pvArg3:VECTORCAST_INT2
TEST.VALUE:debug_support.gulPrintf7_b.pvArg4:VECTORCAST_INT1
TEST.VALUE:debug_support.gulPrintf7_b.pvArg5:VECTORCAST_INT1
TEST.VALUE:debug_support.gulPrintf7_b.pvArg6:VECTORCAST_INT1
TEST.VALUE:debug_support.gulPrintf7_b.pvArg7:VECTORCAST_INT1
TEST.END

-- Subprogram: gulPrintf8_b

-- Test Case: gulPrintf8_b
TEST.UNIT:debug_support
TEST.SUBPROGRAM:gulPrintf8_b
TEST.NEW
TEST.NAME:gulPrintf8_b
TEST.VALUE:debug_support.gulPrintf8_b.pszString:<<malloc 6>>
TEST.VALUE:debug_support.gulPrintf8_b.pszString:"Hello"
TEST.VALUE:debug_support.gulPrintf8_b.pvArg1:VECTORCAST_INT1
TEST.VALUE:debug_support.gulPrintf8_b.pvArg2:VECTORCAST_INT1
TEST.VALUE:debug_support.gulPrintf8_b.pvArg3:VECTORCAST_INT1
TEST.VALUE:debug_support.gulPrintf8_b.pvArg4:VECTORCAST_INT1
TEST.VALUE:debug_support.gulPrintf8_b.pvArg5:VECTORCAST_INT1
TEST.VALUE:debug_support.gulPrintf8_b.pvArg6:VECTORCAST_INT1
TEST.VALUE:debug_support.gulPrintf8_b.pvArg7:VECTORCAST_INT1
TEST.VALUE:debug_support.gulPrintf8_b.pvArg8:VECTORCAST_INT1
TEST.END

-- Subprogram: gvDebug_Initialization

-- Test Case: gvDebug_Initialization
TEST.UNIT:debug_support
TEST.SUBPROGRAM:gvDebug_Initialization
TEST.NEW
TEST.NAME:gvDebug_Initialization
TEST.END

-- Subprogram: gvDebug_Process

-- Test Case: gvDebug_Process
TEST.UNIT:debug_support
TEST.SUBPROGRAM:gvDebug_Process
TEST.NEW
TEST.NAME:gvDebug_Process
TEST.END

-- Subprogram: gvDebug_ReceiveSupvCharISR

-- Test Case: gvDebug_ReceiveSupvCharISR
TEST.UNIT:debug_support
TEST.SUBPROGRAM:gvDebug_ReceiveSupvCharISR
TEST.NEW
TEST.NAME:gvDebug_ReceiveSupvCharISR
TEST.VALUE:uut_prototype_stubs.gfMyGetChar_c.return:1
TEST.END

-- Subprogram: gvMyPrintf_b

-- Test Case: gvMyPrintf_b
TEST.UNIT:debug_support
TEST.SUBPROGRAM:gvMyPrintf_b
TEST.NEW
TEST.NAME:gvMyPrintf_b
TEST.VALUE:debug_support.gvMyPrintf_b.pszString:<<malloc 6>>
TEST.VALUE:debug_support.gvMyPrintf_b.pszString:"Hello"
TEST.END

-- Subprogram: gvPrint_MutexCreateStatus

-- Test Case: gvPrint_MutexCreateStatus
TEST.UNIT:debug_support
TEST.SUBPROGRAM:gvPrint_MutexCreateStatus
TEST.NEW
TEST.NAME:gvPrint_MutexCreateStatus
TEST.VALUE:debug_support.gvPrint_MutexCreateStatus.pszName:<<malloc 6>>
TEST.VALUE:debug_support.gvPrint_MutexCreateStatus.pszName:"Hello"
TEST.VALUE:debug_support.gvPrint_MutexCreateStatus.ulStatus:0x1C,0x13,0x1F
TEST.END

-- Subprogram: gvPrint_QueueCreateStatus

-- Test Case: gvPrint_QueueCreateStatus
TEST.UNIT:debug_support
TEST.SUBPROGRAM:gvPrint_QueueCreateStatus
TEST.NEW
TEST.NAME:gvPrint_QueueCreateStatus
TEST.VALUE:debug_support.gvPrint_QueueCreateStatus.pszName:<<malloc 6>>
TEST.VALUE:debug_support.gvPrint_QueueCreateStatus.pszName:"Hello"
TEST.VALUE:debug_support.gvPrint_QueueCreateStatus.ulStatus:9,3,5,19
TEST.END

-- Subprogram: gvPrint_QueueReceiveStatus

-- Test Case: gvPrint_QueueReceiveStatus
TEST.UNIT:debug_support
TEST.SUBPROGRAM:gvPrint_QueueReceiveStatus
TEST.NEW
TEST.NAME:gvPrint_QueueReceiveStatus
TEST.VALUE:debug_support.gvPrint_QueueReceiveStatus.pszName:<<malloc 6>>
TEST.VALUE:debug_support.gvPrint_QueueReceiveStatus.pszName:"Hello"
TEST.END

-- Subprogram: gvPrint_QueueSendStatus

-- Test Case: gvPrint_QueueSendStatus
TEST.UNIT:debug_support
TEST.SUBPROGRAM:gvPrint_QueueSendStatus
TEST.NEW
TEST.NAME:gvPrint_QueueSendStatus
TEST.VALUE:debug_support.gvPrint_QueueSendStatus.pszName:<<malloc 6>>
TEST.VALUE:debug_support.gvPrint_QueueSendStatus.pszName:"Hello"
TEST.END

-- Subprogram: gvPrint_SemaphoreCreateStatus

-- Test Case: gvPrint_SemaphoreCreateStatus
TEST.UNIT:debug_support
TEST.SUBPROGRAM:gvPrint_SemaphoreCreateStatus
TEST.NEW
TEST.NAME:gvPrint_SemaphoreCreateStatus
TEST.VALUE:debug_support.gvPrint_SemaphoreCreateStatus.pszName:<<malloc 6>>
TEST.VALUE:debug_support.gvPrint_SemaphoreCreateStatus.pszName:"Hello"
TEST.VALUE:debug_support.gvPrint_SemaphoreCreateStatus.ulStatus:0x13,0xC
TEST.END

-- Subprogram: gvPrint_ThreadCreateStatus

-- Test Case: gvPrint_ThreadCreateStatus.001
TEST.UNIT:debug_support
TEST.SUBPROGRAM:gvPrint_ThreadCreateStatus
TEST.NEW
TEST.NAME:gvPrint_ThreadCreateStatus.001
TEST.VALUE:debug_support.gvPrint_ThreadCreateStatus.pszName:<<malloc 6>>
TEST.VALUE:debug_support.gvPrint_ThreadCreateStatus.pszName:"Hello"
TEST.VALUE:debug_support.gvPrint_ThreadCreateStatus.ulStatus:0xE,0x3,0x5,0xF,0x18,0x10,0x13
TEST.END

-- Subprogram: gvPrint_TimerCreateStatus

-- Test Case: gvPrint_TimerCreateStatus
TEST.UNIT:debug_support
TEST.SUBPROGRAM:gvPrint_TimerCreateStatus
TEST.NEW
TEST.NAME:gvPrint_TimerCreateStatus
TEST.VALUE:debug_support.gvPrint_TimerCreateStatus.pszName:<<malloc 6>>
TEST.VALUE:debug_support.gvPrint_TimerCreateStatus.pszName:"Hello"
TEST.END

-- Subprogram: vShowCANopenVersion

-- Test Case: vShowCANopenVersion.001
TEST.UNIT:debug_support
TEST.SUBPROGRAM:vShowCANopenVersion
TEST.NEW
TEST.NAME:vShowCANopenVersion.001
TEST.VALUE:debug_support.vShowCANopenVersion.pszVersion:<<malloc 6>>
TEST.VALUE:debug_support.vShowCANopenVersion.pszVersion:"Hello"
TEST.END

-- Unit: flash_validate

-- Subprogram: gvValidate_Application

-- Test Case: BASIS-PATH-003-PARTIAL
TEST.UNIT:flash_validate
TEST.SUBPROGRAM:gvValidate_Application
TEST.NEW
TEST.NAME:BASIS-PATH-003-PARTIAL
TEST.BASIS_PATH:3 of 5 (partial)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 3
      (1) if (MEM_MAP_NUM > 0U && MEM_MAP_NUM <= 8U) ==> TRUE
      (2) for (ubBlock < MEM_MAP_NUM) ==> FALSE
      (4) if (ulCrcCalc != CRC32 || uwCksCalc != CKS) ==> FALSE
   Test Case Generation Notes:
      Conflict: Cannot resolve multiple comparisons ( flash_validate.<<GLOBAL>>.MEM_MAP_NUM ) in branches 1/2
      Cannot set local variable ulCrcCalc in branch 4
      Cannot set local variable uwCksCalc in branch 4
TEST.END_NOTES:
TEST.STUB:crc.guwCks16_Range
TEST.STUB:crc.gulCrc32_Range
TEST.END

-- Test Case: if_(_(MEM_MAP_NUM_>_0u)_&&_(MEM_MAP_NUM_<=_(8u))_)
TEST.UNIT:flash_validate
TEST.SUBPROGRAM:gvValidate_Application
TEST.NEW
TEST.NAME:if_(_(MEM_MAP_NUM_>_0u)_&&_(MEM_MAP_NUM_<=_(8u))_)
TEST.BASIS_PATH:1 of 5 (partial)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (1) if (MEM_MAP_NUM > 0U && MEM_MAP_NUM <= 8U) ==> FALSE
      (4) if (ulCrcCalc != CRC32 || uwCksCalc != CKS) ==> FALSE
   Test Case Generation Notes:
      Cannot set local variable ulCrcCalc in branch 4
      Cannot set local variable uwCksCalc in branch 4
TEST.END_NOTES:
TEST.STUB:crc.guwCks16_Range
TEST.STUB:crc.gulCrc32_Range
TEST.END

-- Subprogram: gvValidate_TruckType

-- Test Case: gvValidate_TruckType.001
TEST.UNIT:flash_validate
TEST.SUBPROGRAM:gvValidate_TruckType
TEST.NEW
TEST.NAME:gvValidate_TruckType.001
TEST.END

-- Unit: instrument

-- Subprogram: gfINST_ReportQueueDepth

-- Test Case: gfINST_ReportQueueDepth.001
TEST.UNIT:instrument
TEST.SUBPROGRAM:gfINST_ReportQueueDepth
TEST.NEW
TEST.NAME:gfINST_ReportQueueDepth.001
TEST.VALUE:instrument.gfINST_ReportQueueDepth.eQueue_ID:INST_QUEUE_IMM_TX
TEST.VALUE:instrument.gfINST_ReportQueueDepth.pQueue:<<malloc 1>>
TEST.END

-- Subprogram: gvINST_Background

-- Test Case: gvINST_Background.uwseconds_<_10
TEST.UNIT:instrument
TEST.SUBPROGRAM:gvINST_Background
TEST.NEW
TEST.NAME:gvINST_Background.uwseconds_<_10
TEST.VALUE:uut_prototype_stubs.gulOS_GetTime.return:1000
TEST.END

-- Subprogram: gvINST_Thread_End

-- Test Case: gvINST_Thread_End
TEST.UNIT:instrument
TEST.SUBPROGRAM:gvINST_Thread_End
TEST.NEW
TEST.NAME:gvINST_Thread_End
TEST.VALUE:instrument.gvINST_Thread_End.pThreadData:<<malloc 1>>
TEST.END

-- Subprogram: gvINST_Thread_Init

-- Test Case: gvINST_Thread_Init
TEST.UNIT:instrument
TEST.SUBPROGRAM:gvINST_Thread_Init
TEST.NEW
TEST.NAME:gvINST_Thread_Init
TEST.VALUE:instrument.gvINST_Thread_Init.pThreadData:<<malloc 1>>
TEST.END

-- Subprogram: gvINST_Thread_Start

-- Test Case: gvINST_Thread_Start
TEST.UNIT:instrument
TEST.SUBPROGRAM:gvINST_Thread_Start
TEST.NEW
TEST.NAME:gvINST_Thread_Start
TEST.VALUE:instrument.gvINST_Thread_Start.pThreadData:<<malloc 1>>
TEST.END

-- Subprogram: gvINST_WD_ReportEarly

-- Test Case: gvINST_WD_ReportEarly
TEST.UNIT:instrument
TEST.SUBPROGRAM:gvINST_WD_ReportEarly
TEST.NEW
TEST.NAME:gvINST_WD_ReportEarly
TEST.END

-- Subprogram: gvINST_WD_ReportLate

-- Test Case: gvINST_WD_ReportLate
TEST.UNIT:instrument
TEST.SUBPROGRAM:gvINST_WD_ReportLate
TEST.NEW
TEST.NAME:gvINST_WD_ReportLate
TEST.END

-- Subprogram: gvINST_WD_ReportTimeout

-- Test Case: gvINST_WD_ReportTimeout
TEST.UNIT:instrument
TEST.SUBPROGRAM:gvINST_WD_ReportTimeout
TEST.NEW
TEST.NAME:gvINST_WD_ReportTimeout
TEST.END

-- Subprogram: srINST_ISR_utilization

-- Test Case: srINST_ISR_utilization
TEST.UNIT:instrument
TEST.SUBPROGRAM:srINST_ISR_utilization
TEST.NEW
TEST.NAME:srINST_ISR_utilization
TEST.VALUE:instrument.srINST_ISR_utilization.pMeas:<<malloc 1>>
TEST.VALUE:instrument.srINST_ISR_utilization.pMeas[0].ulTimer:20
TEST.VALUE:instrument.srINST_ISR_utilization.pMeas[0].rPercent:2.0
TEST.VALUE:instrument.srINST_ISR_utilization.ulScaleFactor:10
TEST.EXPECTED:instrument.srINST_ISR_utilization.return:2.0
TEST.END

-- Subprogram: srINST_Thread_utilization

-- Test Case: srINST_Thread_utilization.001
TEST.UNIT:instrument
TEST.SUBPROGRAM:srINST_Thread_utilization
TEST.NEW
TEST.NAME:srINST_Thread_utilization.001
TEST.VALUE:instrument.srINST_Thread_utilization.pThread:<<malloc 1>>
TEST.VALUE:instrument.srINST_Thread_utilization.pMeas:<<malloc 1>>
TEST.VALUE:instrument.srINST_Thread_utilization.pMeas[0].rPercent:0.0
TEST.END

-- Subprogram: svINST_CPU_utilization

-- Test Case: svINST_CPU_utilization.001
TEST.UNIT:instrument
TEST.SUBPROGRAM:svINST_CPU_utilization
TEST.NEW
TEST.NAME:svINST_CPU_utilization.001
TEST.END

-- Subprogram: vINST_Bucketize

-- Test Case: vINST_Bucketize
TEST.UNIT:instrument
TEST.SUBPROGRAM:vINST_Bucketize
TEST.NEW
TEST.NAME:vINST_Bucketize
TEST.VALUE:instrument.vINST_Bucketize.slData:1
TEST.VALUE:instrument.vINST_Bucketize.slThreshold:2
TEST.VALUE:instrument.vINST_Bucketize.pulBuckets:<<malloc 1>>
TEST.END

-- Unit: stopwatch

-- Subprogram: gulStopwatch_End

-- Test Case: gulStopwatch_End
TEST.UNIT:stopwatch
TEST.SUBPROGRAM:gulStopwatch_End
TEST.NEW
TEST.NAME:gulStopwatch_End
TEST.END

-- Subprogram: gvStopwatch_Start

-- Test Case: gvStopwatch_Start
TEST.UNIT:stopwatch
TEST.SUBPROGRAM:gvStopwatch_Start
TEST.NEW
TEST.NAME:gvStopwatch_Start
TEST.END

-- Subprogram: gvStopwatch_StartCritical

-- Test Case: gvStopwatch_StartCritical
TEST.UNIT:stopwatch
TEST.SUBPROGRAM:gvStopwatch_StartCritical
TEST.NEW
TEST.NAME:gvStopwatch_StartCritical
TEST.END

-- Unit: util

-- Subprogram: gslGetMaxInt

-- Test Case: gslGetMaxInt
TEST.UNIT:util
TEST.SUBPROGRAM:gslGetMaxInt
TEST.NEW
TEST.NAME:gslGetMaxInt
TEST.VALUE:util.gslGetMaxInt.ubBytes:8
TEST.EXPECTED:util.gslGetMaxInt.return:-1
TEST.END

-- Subprogram: gulGetMaxUint

-- Test Case: gulGetMaxUint
TEST.UNIT:util
TEST.SUBPROGRAM:gulGetMaxUint
TEST.NEW
TEST.NAME:gulGetMaxUint
TEST.VALUE:util.gulGetMaxUint.ubBytes:8
TEST.EXPECTED:util.gulGetMaxUint.return:4294967295
TEST.END

-- Subprogram: gulRAM_GetEnd

-- Test Case: gulRAM_GetEnd
TEST.UNIT:util
TEST.SUBPROGRAM:gulRAM_GetEnd
TEST.NEW
TEST.NAME:gulRAM_GetEnd
TEST.END

-- Subprogram: gulRAM_GetFirstUnused

-- Test Case: gulRAM_GetFirstUnused
TEST.UNIT:util
TEST.SUBPROGRAM:gulRAM_GetFirstUnused
TEST.NEW
TEST.NAME:gulRAM_GetFirstUnused
TEST.END

-- Subprogram: gulRAM_GetStart

-- Test Case: gulRAM_GetStart
TEST.UNIT:util
TEST.SUBPROGRAM:gulRAM_GetStart
TEST.NEW
TEST.NAME:gulRAM_GetStart
TEST.END

-- Subprogram: guwConcatenateBytes

-- Test Case: guwConcatenateBytes
TEST.UNIT:util
TEST.SUBPROGRAM:guwConcatenateBytes
TEST.NEW
TEST.NAME:guwConcatenateBytes
TEST.VALUE:util.guwConcatenateBytes.ubMSByte:0x12
TEST.VALUE:util.guwConcatenateBytes.ubLSByte:0x34
TEST.EXPECTED:util.guwConcatenateBytes.return:0x1234
TEST.END

-- Subprogram: gvLed_On

-- Test Case: gvLed_On
TEST.UNIT:util
TEST.SUBPROGRAM:gvLed_On
TEST.NEW
TEST.NAME:gvLed_On
TEST.END

-- Subprogram: rClip

-- Test Case: rClip.rMax
TEST.UNIT:util
TEST.SUBPROGRAM:rClip
TEST.NEW
TEST.NAME:rClip.rMax
TEST.VALUE:util.rClip.rMin:101.0
TEST.VALUE:util.rClip.rVal:200.0
TEST.VALUE:util.rClip.rMax:199.0
TEST.EXPECTED:util.rClip.return:199.0
TEST.END

-- Test Case: rClip.rMin
TEST.UNIT:util
TEST.SUBPROGRAM:rClip
TEST.NEW
TEST.NAME:rClip.rMin
TEST.VALUE:util.rClip.rMin:101.0
TEST.VALUE:util.rClip.rVal:100.0
TEST.VALUE:util.rClip.rMax:200.0
TEST.EXPECTED:util.rClip.return:101.0
TEST.END
