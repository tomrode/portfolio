-- VectorCAST 6.4p (01/31/17)
-- Test Case Script
-- 
-- Environment    : FRAM_DRIVER_INTEGRATION
-- Unit(s) Under Test: NvMemory fram_driver
-- 
-- Script Features
TEST.SCRIPT_FEATURE:C_DIRECT_ARRAY_INDEXING
TEST.SCRIPT_FEATURE:CPP_CLASS_OBJECT_REVISION
TEST.SCRIPT_FEATURE:MULTIPLE_UUT_SUPPORT
TEST.SCRIPT_FEATURE:MIXED_CASE_NAMES
TEST.SCRIPT_FEATURE:STATIC_HEADER_FUNCS_IN_UUTS
--

-- Unit: NvMemory

-- Subprogram: gfNvMemory_Initialize

-- Test Case: FujiMemSize_1M:
TEST.UNIT:NvMemory
TEST.SUBPROGRAM:gfNvMemory_Initialize
TEST.NEW
TEST.NAME:FujiMemSize_1M:
TEST.COMPOUND_ONLY
TEST.STUB:fram_driver.fFRAM_IdentifyFuji
TEST.VALUE:fram_driver.fFRAM_IdentifyFuji.return:1
TEST.VALUE:uut_prototype_stubs.gfSpiB_Initialize.return:1
TEST.VALUE:NvMemory.<<GLOBAL>>.fram_drv.Description.ubManufID:1
TEST.VALUE:NvMemory.<<GLOBAL>>.fram_drv.Description.ubDeviceID1:0x27
TEST.VALUE:NvMemory.<<GLOBAL>>.fram_drv.Description.SerialPort.fSPI_Initialize:gfSpiB_Initialize
TEST.END

-- Test Case: FujiMemSize_256K:
TEST.UNIT:NvMemory
TEST.SUBPROGRAM:gfNvMemory_Initialize
TEST.NEW
TEST.NAME:FujiMemSize_256K:
TEST.COMPOUND_ONLY
TEST.STUB:fram_driver.fFRAM_IdentifyFuji
TEST.VALUE:fram_driver.fFRAM_IdentifyFuji.return:1
TEST.VALUE:uut_prototype_stubs.gfSpiB_Initialize.return:1
TEST.VALUE:NvMemory.<<GLOBAL>>.fram_drv.Description.ubManufID:1
TEST.VALUE:NvMemory.<<GLOBAL>>.fram_drv.Description.ubDeviceID1:5
TEST.VALUE:NvMemory.<<GLOBAL>>.fram_drv.Description.SerialPort.fSPI_Initialize:gfSpiB_Initialize
TEST.EXPECTED:NvMemory.<<GLOBAL>>.fram_drv.Description.SerialPort.fSPI_Send:fSPIB_API_Send
TEST.END

-- Test Case: FujiMemSize_2M:
TEST.UNIT:NvMemory
TEST.SUBPROGRAM:gfNvMemory_Initialize
TEST.NEW
TEST.NAME:FujiMemSize_2M:
TEST.COMPOUND_ONLY
TEST.STUB:fram_driver.fFRAM_IdentifyFuji
TEST.VALUE:fram_driver.fFRAM_IdentifyFuji.return:1
TEST.VALUE:uut_prototype_stubs.gfSpiB_Initialize.return:1
TEST.VALUE:NvMemory.<<GLOBAL>>.fram_drv.Description.ubManufID:1
TEST.VALUE:NvMemory.<<GLOBAL>>.fram_drv.Description.ubDeviceID1:0x28
TEST.VALUE:NvMemory.<<GLOBAL>>.fram_drv.Description.SerialPort.fSPI_Initialize:gfSpiB_Initialize
TEST.END

-- Test Case: FujiMemSize_512K:
TEST.UNIT:NvMemory
TEST.SUBPROGRAM:gfNvMemory_Initialize
TEST.NEW
TEST.NAME:FujiMemSize_512K:
TEST.COMPOUND_ONLY
TEST.STUB:fram_driver.fFRAM_IdentifyFuji
TEST.VALUE:fram_driver.fFRAM_IdentifyFuji.return:1
TEST.VALUE:uut_prototype_stubs.gfSpiB_Initialize.return:1
TEST.VALUE:NvMemory.<<GLOBAL>>.fram_drv.Description.ubManufID:1
TEST.VALUE:NvMemory.<<GLOBAL>>.fram_drv.Description.ubDeviceID1:0x26
TEST.VALUE:NvMemory.<<GLOBAL>>.fram_drv.Description.SerialPort.fSPI_Initialize:gfSpiB_Initialize
TEST.END

-- Test Case: FujiMemSize_default
TEST.UNIT:NvMemory
TEST.SUBPROGRAM:gfNvMemory_Initialize
TEST.NEW
TEST.NAME:FujiMemSize_default
TEST.COMPOUND_ONLY
TEST.STUB:fram_driver.fFRAM_IdentifyFuji
TEST.VALUE:fram_driver.fFRAM_IdentifyFuji.return:1
TEST.VALUE:uut_prototype_stubs.gfSpiB_Initialize.return:1
TEST.VALUE:NvMemory.<<GLOBAL>>.fram_drv.Description.ubManufID:1
TEST.VALUE:NvMemory.<<GLOBAL>>.fram_drv.Description.ubDeviceID1:0
TEST.VALUE:NvMemory.<<GLOBAL>>.fram_drv.Description.SerialPort.fSPI_Initialize:gfSpiB_Initialize
TEST.END

-- Test Case: RamtronMemSize_128K:
TEST.UNIT:NvMemory
TEST.SUBPROGRAM:gfNvMemory_Initialize
TEST.NEW
TEST.NAME:RamtronMemSize_128K:
TEST.COMPOUND_ONLY
TEST.STUB:fram_driver.fFRAM_IdentifyRamtron
TEST.VALUE:fram_driver.fFRAM_IdentifyRamtron.pfdesc[0].ubDeviceID1:1
TEST.VALUE:uut_prototype_stubs.gfSpiB_Initialize.return:1
TEST.VALUE:NvMemory.<<GLOBAL>>.fram_drv.Description.ubManufID:1
TEST.VALUE:NvMemory.<<GLOBAL>>.fram_drv.Description.ubDeviceID1:0x1
TEST.VALUE:NvMemory.<<GLOBAL>>.fram_drv.Description.SerialPort.fSPI_Initialize:gfSpiB_Initialize
TEST.EXPECTED:NvMemory.<<GLOBAL>>.fram_drv.Description.ubDeviceID1:1
TEST.END

-- Test Case: RamtronMemSize_1M
TEST.UNIT:NvMemory
TEST.SUBPROGRAM:gfNvMemory_Initialize
TEST.NEW
TEST.NAME:RamtronMemSize_1M
TEST.COMPOUND_ONLY
TEST.STUB:fram_driver.fFRAM_IdentifyRamtron
TEST.VALUE:fram_driver.fFRAM_IdentifyRamtron.pfdesc[0].ubDeviceID1:4
TEST.VALUE:uut_prototype_stubs.gfSpiB_Initialize.return:1
TEST.VALUE:NvMemory.<<GLOBAL>>.fram_drv.Description.ubManufID:1
TEST.VALUE:NvMemory.<<GLOBAL>>.fram_drv.Description.ubDeviceID1:0x4
TEST.VALUE:NvMemory.<<GLOBAL>>.fram_drv.Description.SerialPort.fSPI_Initialize:gfSpiB_Initialize
TEST.EXPECTED:NvMemory.<<GLOBAL>>.fram_drv.Description.ubDeviceID1:4
TEST.END

-- Test Case: RamtronMemSize_256k
TEST.UNIT:NvMemory
TEST.SUBPROGRAM:gfNvMemory_Initialize
TEST.NEW
TEST.NAME:RamtronMemSize_256k
TEST.COMPOUND_ONLY
TEST.STUB:fram_driver.fFRAM_IdentifyRamtron
TEST.VALUE:fram_driver.fFRAM_IdentifyRamtron.pfdesc[0].ubDeviceID1:2
TEST.VALUE:uut_prototype_stubs.gfSpiB_Initialize.return:1
TEST.VALUE:NvMemory.<<GLOBAL>>.fram_drv.Description.ubManufID:1
TEST.VALUE:NvMemory.<<GLOBAL>>.fram_drv.Description.ubDeviceID1:0x2
TEST.VALUE:NvMemory.<<GLOBAL>>.fram_drv.Description.SerialPort.fSPI_Initialize:gfSpiB_Initialize
TEST.EXPECTED:NvMemory.<<GLOBAL>>.fram_drv.Description.ubDeviceID1:2
TEST.END

-- Test Case: RamtronMemSize_2M
TEST.UNIT:NvMemory
TEST.SUBPROGRAM:gfNvMemory_Initialize
TEST.NEW
TEST.NAME:RamtronMemSize_2M
TEST.COMPOUND_ONLY
TEST.STUB:fram_driver.fFRAM_IdentifyRamtron
TEST.VALUE:fram_driver.fFRAM_IdentifyRamtron.pfdesc[0].ubDeviceID1:5
TEST.VALUE:uut_prototype_stubs.gfSpiB_Initialize.return:1
TEST.VALUE:NvMemory.<<GLOBAL>>.fram_drv.Description.ubManufID:1
TEST.VALUE:NvMemory.<<GLOBAL>>.fram_drv.Description.ubDeviceID1:0x5
TEST.VALUE:NvMemory.<<GLOBAL>>.fram_drv.Description.SerialPort.fSPI_Initialize:gfSpiB_Initialize
TEST.EXPECTED:NvMemory.<<GLOBAL>>.fram_drv.Description.ubDeviceID1:0x5
TEST.END

-- Test Case: RamtronMemSize_512k
TEST.UNIT:NvMemory
TEST.SUBPROGRAM:gfNvMemory_Initialize
TEST.NEW
TEST.NAME:RamtronMemSize_512k
TEST.COMPOUND_ONLY
TEST.STUB:fram_driver.fFRAM_IdentifyRamtron
TEST.VALUE:fram_driver.fFRAM_IdentifyRamtron.pfdesc[0].ubDeviceID1:3
TEST.VALUE:uut_prototype_stubs.gfSpiB_Initialize.return:1
TEST.VALUE:NvMemory.<<GLOBAL>>.fram_drv.Description.ubManufID:1
TEST.VALUE:NvMemory.<<GLOBAL>>.fram_drv.Description.ubDeviceID1:0x3
TEST.VALUE:NvMemory.<<GLOBAL>>.fram_drv.Description.SerialPort.fSPI_Initialize:gfSpiB_Initialize
TEST.EXPECTED:NvMemory.<<GLOBAL>>.fram_drv.Description.ubDeviceID1:3
TEST.END

-- Test Case: gfNvMemory_Initialize.Everspin
TEST.UNIT:NvMemory
TEST.SUBPROGRAM:gfNvMemory_Initialize
TEST.NEW
TEST.NAME:gfNvMemory_Initialize.Everspin
TEST.COMPOUND_ONLY
TEST.STUB:fram_driver.fFRAM_IdentifyEverspin
TEST.VALUE:fram_driver.fFRAM_IdentifyEverspin.pfdesc[0].ubDeviceID1:0
TEST.VALUE:fram_driver.fFRAM_IdentifyEverspin.return:1
TEST.VALUE:uut_prototype_stubs.gfSpiB_Initialize.return:1
TEST.VALUE:NvMemory.<<GLOBAL>>.fram_drv.Description.ubManufID:1
TEST.VALUE:NvMemory.<<GLOBAL>>.fram_drv.Description.SerialPort.fSPI_Initialize:gfSpiB_Initialize
TEST.END

-- Test Case: gfNvMemory_Initialize.Everspin.default
TEST.UNIT:NvMemory
TEST.SUBPROGRAM:gfNvMemory_Initialize
TEST.NEW
TEST.NAME:gfNvMemory_Initialize.Everspin.default
TEST.COMPOUND_ONLY
TEST.STUB:fram_driver.fFRAM_IdentifyEverspin
TEST.VALUE:fram_driver.fFRAM_IdentifyEverspin.pfdesc[0].ubDeviceID1:1
TEST.VALUE:fram_driver.fFRAM_IdentifyEverspin.return:1
TEST.VALUE:uut_prototype_stubs.gfSpiB_Initialize.return:1
TEST.VALUE:NvMemory.<<GLOBAL>>.fram_drv.Description.ubManufID:1
TEST.VALUE:NvMemory.<<GLOBAL>>.fram_drv.Description.SerialPort.fSPI_Initialize:gfSpiB_Initialize
TEST.END

-- Test Case: gfNvMemory_Initialize.Fuji
TEST.UNIT:NvMemory
TEST.SUBPROGRAM:gfNvMemory_Initialize
TEST.NEW
TEST.NAME:gfNvMemory_Initialize.Fuji
TEST.COMPOUND_ONLY
TEST.STUB:fram_driver.fFRAM_IdentifyRamtron
TEST.VALUE:fram_driver.fFRAM_IdentifyRamtron.return:0
TEST.VALUE:uut_prototype_stubs.gfSpiB_Initialize.return:1
TEST.VALUE:NvMemory.<<GLOBAL>>.fram_drv.Description.ubManufID:1
TEST.VALUE:NvMemory.<<GLOBAL>>.fram_drv.Description.SerialPort.fSPI_Initialize:gfSpiB_Initialize
TEST.END

-- Test Case: gfNvMemory_Initialize.Ramtron
TEST.UNIT:NvMemory
TEST.SUBPROGRAM:gfNvMemory_Initialize
TEST.NEW
TEST.NAME:gfNvMemory_Initialize.Ramtron
TEST.COMPOUND_ONLY
TEST.STUB:fram_driver.fFRAM_IdentifyRamtron
TEST.VALUE:fram_driver.fFRAM_IdentifyRamtron.return:1
TEST.VALUE:uut_prototype_stubs.gfSpiB_Initialize.return:1
TEST.VALUE:NvMemory.<<GLOBAL>>.fram_drv.Description.ubManufID:1
TEST.VALUE:NvMemory.<<GLOBAL>>.fram_drv.Description.SerialPort.fSPI_Initialize:gfSpiB_Initialize
TEST.END

-- Subprogram: gfNvMemory_ReadBuffer

-- Test Case: gfNvMemory_ReadBuffer.001
TEST.UNIT:NvMemory
TEST.SUBPROGRAM:gfNvMemory_ReadBuffer
TEST.NEW
TEST.NAME:gfNvMemory_ReadBuffer.001
TEST.COMPOUND_ONLY
TEST.STUB:fram_driver.fFRAM_IdentifyRamtron
TEST.VALUE:fram_driver.fFRAM_IdentifyRamtron.pfdesc[0].ubDeviceID1:2
TEST.VALUE:uut_prototype_stubs.gfSpiB_Initialize.return:1
TEST.VALUE:NvMemory.<<GLOBAL>>.fram_drv.Description.ubManufID:1
TEST.VALUE:NvMemory.<<GLOBAL>>.fram_drv.Description.ubDeviceID1:0x1
TEST.VALUE:NvMemory.<<GLOBAL>>.fram_drv.Description.SerialPort.fSPI_Initialize:gfSpiB_Initialize
TEST.EXPECTED:NvMemory.<<GLOBAL>>.fram_drv.Description.ubDeviceID1:1
TEST.END

-- Unit: fram_driver

-- Subprogram: fFRAM_IdentifyFuji

-- Test Case: fFRAM_IdentifyFuji
TEST.UNIT:fram_driver
TEST.SUBPROGRAM:fFRAM_IdentifyFuji
TEST.NEW
TEST.NAME:fFRAM_IdentifyFuji
TEST.COMPOUND_ONLY
TEST.END

-- Subprogram: fFRAM_WriteDisable

-- Test Case: fFRAM_WriteDisable
TEST.UNIT:fram_driver
TEST.SUBPROGRAM:fFRAM_WriteDisable
TEST.NEW
TEST.NAME:fFRAM_WriteDisable
TEST.COMPOUND_ONLY
TEST.VALUE:fram_driver.fFRAM_WriteDisable.pfdesc:<<malloc 1>>
TEST.END

-- Subprogram: fFRAM_WriteEnable

-- Test Case: 0U_!=_pfdesc->SerialPort.fSPI_Send_
TEST.UNIT:fram_driver
TEST.SUBPROGRAM:fFRAM_WriteEnable
TEST.NEW
TEST.NAME:0U_!=_pfdesc->SerialPort.fSPI_Send_
TEST.VALUE:fram_driver.fFRAM_WriteEnable.pfdesc:<<malloc 1>>
TEST.END

-- Subprogram: gfFRAM_Init

-- Test Case: gfFRAM_Init.Everspin
TEST.UNIT:fram_driver
TEST.SUBPROGRAM:gfFRAM_Init
TEST.NEW
TEST.NAME:gfFRAM_Init.Everspin
TEST.COMPOUND_ONLY
TEST.STUB:fram_driver.fFRAM_IdentifyEverspin
TEST.VALUE:fram_driver.fFRAM_IdentifyEverspin.pfdesc[0].ubManufID:1
TEST.VALUE:fram_driver.fFRAM_IdentifyEverspin.return:1
TEST.END

-- Test Case: gfFRAM_Init.Everspin.default
TEST.UNIT:fram_driver
TEST.SUBPROGRAM:gfFRAM_Init
TEST.NEW
TEST.NAME:gfFRAM_Init.Everspin.default
TEST.COMPOUND_ONLY
TEST.STUB:fram_driver.fFRAM_IdentifyEverspin
TEST.VALUE:fram_driver.gfFRAM_Init.pfdo:<<malloc 1>>
TEST.VALUE:fram_driver.gfFRAM_Init.pfdo[0].Description.ubDeviceID1:0
TEST.VALUE:fram_driver.fFRAM_IdentifyEverspin.pfdesc[0].ubDeviceID1:0
TEST.VALUE:fram_driver.fFRAM_IdentifyEverspin.return:1
TEST.END

-- Test Case: gfFRAM_Init.Fuji
TEST.UNIT:fram_driver
TEST.SUBPROGRAM:gfFRAM_Init
TEST.NEW
TEST.NAME:gfFRAM_Init.Fuji
TEST.COMPOUND_ONLY
TEST.STUB:fram_driver.fFRAM_IdentifyFuji
TEST.VALUE:fram_driver.gfFRAM_Init.pfdo:<<malloc 1>>
TEST.VALUE:fram_driver.gfFRAM_Init.pPort:<<malloc 1>>
TEST.VALUE:fram_driver.fFRAM_IdentifyFuji.return:1
TEST.VALUE:uut_prototype_stubs.gfSpiB_Initialize.return:1
TEST.VALUE_USER_CODE:fram_driver.gfFRAM_Init.pPort.pPort[0].fSPI_Initialize
//static fram_serial_port_t fram_portx;

<<fram_driver.gfFRAM_Init.pPort>>[0].fSPI_Initialize = ( 1 );
TEST.END_VALUE_USER_CODE:
TEST.VALUE_USER_CODE:fram_driver.gfFRAM_Init.pPort.pPort[0].fSPI_Send
//static fram_serial_port_t fram_port;

//<<fram_driver.gfFRAM_Init.pPort>>[0].fSPI_Send = ( &fram_port );
TEST.END_VALUE_USER_CODE:
TEST.END

-- Test Case: gfFRAM_Init.Ramtron
TEST.UNIT:fram_driver
TEST.SUBPROGRAM:gfFRAM_Init
TEST.NEW
TEST.NAME:gfFRAM_Init.Ramtron
TEST.COMPOUND_ONLY
TEST.STUB:fram_driver.fFRAM_IdentifyRamtron
TEST.VALUE:fram_driver.fFRAM_IdentifyRamtron.return:1
TEST.END

-- Subprogram: pszFRAM_GetTextRamtronDevType

-- Test Case: RamtronMemSize_128K:
TEST.UNIT:fram_driver
TEST.SUBPROGRAM:pszFRAM_GetTextRamtronDevType
TEST.NEW
TEST.NAME:RamtronMemSize_128K:
TEST.VALUE:fram_driver.pszFRAM_GetTextRamtronDevType.pfdesc:<<malloc 1>>
TEST.VALUE:fram_driver.pszFRAM_GetTextRamtronDevType.pfdesc[0].ubDeviceID1:1
TEST.EXPECTED:fram_driver.pszFRAM_GetTextRamtronDevType.return:"FM25V01"
TEST.END

-- Test Case: RamtronMemSize_1M:
TEST.UNIT:fram_driver
TEST.SUBPROGRAM:pszFRAM_GetTextRamtronDevType
TEST.NEW
TEST.NAME:RamtronMemSize_1M:
TEST.VALUE:fram_driver.pszFRAM_GetTextRamtronDevType.pfdesc:<<malloc 1>>
TEST.VALUE:fram_driver.pszFRAM_GetTextRamtronDevType.pfdesc[0].ubDeviceID1:4
TEST.EXPECTED:fram_driver.pszFRAM_GetTextRamtronDevType.return:"FM25V10"
TEST.END

-- Test Case: RamtronMemSize_256K:
TEST.UNIT:fram_driver
TEST.SUBPROGRAM:pszFRAM_GetTextRamtronDevType
TEST.NEW
TEST.NAME:RamtronMemSize_256K:
TEST.VALUE:fram_driver.pszFRAM_GetTextRamtronDevType.pfdesc:<<malloc 1>>
TEST.VALUE:fram_driver.pszFRAM_GetTextRamtronDevType.pfdesc[0].ubDeviceID1:2
TEST.EXPECTED:fram_driver.pszFRAM_GetTextRamtronDevType.return:"FM25V02"
TEST.END

-- Test Case: RamtronMemSize_2M:
TEST.UNIT:fram_driver
TEST.SUBPROGRAM:pszFRAM_GetTextRamtronDevType
TEST.NEW
TEST.NAME:RamtronMemSize_2M:
TEST.VALUE:fram_driver.pszFRAM_GetTextRamtronDevType.pfdesc:<<malloc 1>>
TEST.VALUE:fram_driver.pszFRAM_GetTextRamtronDevType.pfdesc[0].ubDeviceID1:5
TEST.EXPECTED:fram_driver.pszFRAM_GetTextRamtronDevType.return:"FM25V20"
TEST.END

-- Test Case: RamtronMemSize_512K:
TEST.UNIT:fram_driver
TEST.SUBPROGRAM:pszFRAM_GetTextRamtronDevType
TEST.NEW
TEST.NAME:RamtronMemSize_512K:
TEST.VALUE:fram_driver.pszFRAM_GetTextRamtronDevType.pfdesc:<<malloc 1>>
TEST.VALUE:fram_driver.pszFRAM_GetTextRamtronDevType.pfdesc[0].ubDeviceID1:3
TEST.EXPECTED:fram_driver.pszFRAM_GetTextRamtronDevType.return:"FM25V05"
TEST.END

-- COMPOUND TESTS

TEST.SUBPROGRAM:<<COMPOUND>>
TEST.NEW
TEST.NAME:Everspin_Init
TEST.SLOT: "1", "NvMemory", "gfNvMemory_Initialize", "1", "gfNvMemory_Initialize.Everspin"
TEST.SLOT: "2", "fram_driver", "gfFRAM_Init", "1", "gfFRAM_Init.Everspin"
TEST.SLOT: "3", "NvMemory", "gfNvMemory_Initialize", "1", "gfNvMemory_Initialize.Everspin.default"
TEST.END
--

-- COMPOUND TESTS

TEST.SUBPROGRAM:<<COMPOUND>>
TEST.NEW
TEST.NAME:Fuji_Init
TEST.SLOT: "1", "NvMemory", "gfNvMemory_Initialize", "1", "gfNvMemory_Initialize.Fuji"
TEST.SLOT: "2", "fram_driver", "gfFRAM_Init", "1", "gfFRAM_Init.Fuji"
TEST.SLOT: "3", "NvMemory", "gfNvMemory_Initialize", "1", "FujiMemSize_512K:"
TEST.SLOT: "4", "NvMemory", "gfNvMemory_Initialize", "1", "FujiMemSize_1M:"
TEST.SLOT: "5", "NvMemory", "gfNvMemory_Initialize", "1", "FujiMemSize_2M:"
TEST.END
--

-- COMPOUND TESTS

TEST.SUBPROGRAM:<<COMPOUND>>
TEST.NEW
TEST.NAME:Ramtron_Init
TEST.SLOT: "1", "NvMemory", "gfNvMemory_Initialize", "1", "gfNvMemory_Initialize.Ramtron"
TEST.SLOT: "2", "fram_driver", "gfFRAM_Init", "1", "gfFRAM_Init.Ramtron"
TEST.SLOT: "3", "NvMemory", "gfNvMemory_Initialize", "1", "RamtronMemSize_128K:"
TEST.SLOT: "4", "NvMemory", "gfNvMemory_Initialize", "1", "RamtronMemSize_256k"
TEST.SLOT: "5", "NvMemory", "gfNvMemory_Initialize", "1", "RamtronMemSize_512k"
TEST.END
--
