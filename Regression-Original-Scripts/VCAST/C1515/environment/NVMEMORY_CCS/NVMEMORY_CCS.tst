-- VectorCAST 6.4.3 (03/08/18)
-- Test Case Script
-- 
-- Environment    : NVMEMORY_CCS
-- Unit(s) Under Test: NvMemory
-- 
-- Script Features
TEST.SCRIPT_FEATURE:C_DIRECT_ARRAY_INDEXING
TEST.SCRIPT_FEATURE:CPP_CLASS_OBJECT_REVISION
TEST.SCRIPT_FEATURE:MULTIPLE_UUT_SUPPORT
TEST.SCRIPT_FEATURE:MIXED_CASE_NAMES
TEST.SCRIPT_FEATURE:STATIC_HEADER_FUNCS_IN_UUTS
--

-- Unit: NvMemory

-- Subprogram: ReturnModuleId

-- Test Case: BASIS-PATH-001
TEST.UNIT:NvMemory
TEST.SUBPROGRAM:ReturnModuleId
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 1
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.END

-- Subprogram: gfNvMemory_Initialize

-- Test Case: BASIS-PATH-001-TEMPLATE
TEST.UNIT:NvMemory
TEST.SUBPROGRAM:gfNvMemory_Initialize
TEST.NEW
TEST.NAME:BASIS-PATH-001-TEMPLATE
TEST.BASIS_PATH:1 of 2 (template)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (1) if (1U != gfFRAM_Init(&fram_drv, &fram_port)) ==> FALSE
   Test Case Generation Notes:
      Cannot set variable to out of range value 1 in branch 1
TEST.END_NOTES:
TEST.END

-- Test Case: BASIS-PATH-002
TEST.UNIT:NvMemory
TEST.SUBPROGRAM:gfNvMemory_Initialize
TEST.NEW
TEST.NAME:BASIS-PATH-002
TEST.BASIS_PATH:2 of 2
TEST.NOTES:
This is an automatically generated test case.
   Test Path 2
      (1) if (1U != gfFRAM_Init(&fram_drv, &fram_port)) ==> TRUE
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:uut_prototype_stubs.gfFRAM_Init.return:<<MIN>>
TEST.END

-- Subprogram: gpGetBootParamBackupDescriptorPointer

-- Test Case: BASIS-PATH-001
TEST.UNIT:NvMemory
TEST.SUBPROGRAM:gpGetBootParamBackupDescriptorPointer
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 1
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.END

-- Subprogram: gpGetBootParameterPointer

-- Test Case: BASIS-PATH-001
TEST.UNIT:NvMemory
TEST.SUBPROGRAM:gpGetBootParameterPointer
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 1
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.END

-- Subprogram: gpGetFramDriverObjectPointer

-- Test Case: BASIS-PATH-001
TEST.UNIT:NvMemory
TEST.SUBPROGRAM:gpGetFramDriverObjectPointer
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 1
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.END

-- Subprogram: gvNvMemory_ReadBuffer

-- Test Case: BASIS-PATH-001-PARTIAL
TEST.UNIT:NvMemory
TEST.SUBPROGRAM:gvNvMemory_ReadBuffer
TEST.NEW
TEST.NAME:BASIS-PATH-001-PARTIAL
TEST.BASIS_PATH:1 of 2 (partial)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (1) if ((0U) != (fram_drv.Operation).fRead) ==> FALSE
   Test Case Generation Notes:
      Cannot set variable (fram_drv.Operation).fRead in branch 1 since it has a type which requires user code.
TEST.END_NOTES:
TEST.VALUE:NvMemory.gvNvMemory_ReadBuffer.ulAddress:<<MIN>>
TEST.VALUE:NvMemory.gvNvMemory_ReadBuffer.pubBuffer:<<malloc 1>>
TEST.VALUE:NvMemory.gvNvMemory_ReadBuffer.ulBufferLength:<<MIN>>
TEST.END

-- Test Case: BASIS-PATH-002-PARTIAL
TEST.UNIT:NvMemory
TEST.SUBPROGRAM:gvNvMemory_ReadBuffer
TEST.NEW
TEST.NAME:BASIS-PATH-002-PARTIAL
TEST.BASIS_PATH:2 of 2 (partial)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 2
      (1) if ((0U) != (fram_drv.Operation).fRead) ==> TRUE
   Test Case Generation Notes:
      Cannot set variable (fram_drv.Operation).fRead in branch 1 since it has a type which requires user code.
TEST.END_NOTES:
TEST.VALUE:NvMemory.gvNvMemory_ReadBuffer.ulAddress:<<MIN>>
TEST.VALUE:NvMemory.gvNvMemory_ReadBuffer.pubBuffer:<<malloc 1>>
TEST.VALUE:NvMemory.gvNvMemory_ReadBuffer.ulBufferLength:<<MIN>>
TEST.END

-- Subprogram: gvNvMemory_WrReq_ByPointer

-- Test Case: BASIS-PATH-001
TEST.UNIT:NvMemory
TEST.SUBPROGRAM:gvNvMemory_WrReq_ByPointer
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 2
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (1) if ((0U) != pubData) ==> FALSE
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:NvMemory.gvNvMemory_WrReq_ByPointer.ulNvAddr_data:<<MIN>>
TEST.VALUE:NvMemory.gvNvMemory_WrReq_ByPointer.pubData:<<malloc 9>>
TEST.VALUE:NvMemory.gvNvMemory_WrReq_ByPointer.pubData:<<null>>
TEST.VALUE:NvMemory.gvNvMemory_WrReq_ByPointer.ulLength:<<MIN>>
TEST.END

-- Test Case: BASIS-PATH-002
TEST.UNIT:NvMemory
TEST.SUBPROGRAM:gvNvMemory_WrReq_ByPointer
TEST.NEW
TEST.NAME:BASIS-PATH-002
TEST.BASIS_PATH:2 of 2
TEST.NOTES:
This is an automatically generated test case.
   Test Path 2
      (1) if ((0U) != pubData) ==> TRUE
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:NvMemory.gvNvMemory_WrReq_ByPointer.ulNvAddr_data:<<MIN>>
TEST.VALUE:NvMemory.gvNvMemory_WrReq_ByPointer.pubData:<<malloc 1>>
TEST.VALUE:NvMemory.gvNvMemory_WrReq_ByPointer.ulLength:<<MIN>>
TEST.END

-- Subprogram: gvNvMemory_WriteBuffer

-- Test Case: BASIS-PATH-001-PARTIAL
TEST.UNIT:NvMemory
TEST.SUBPROGRAM:gvNvMemory_WriteBuffer
TEST.NEW
TEST.NAME:BASIS-PATH-001-PARTIAL
TEST.BASIS_PATH:1 of 2 (partial)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (1) if ((0U) != (fram_drv.Operation).fWrite) ==> FALSE
   Test Case Generation Notes:
      Cannot set variable (fram_drv.Operation).fWrite in branch 1 since it has a type which requires user code.
TEST.END_NOTES:
TEST.VALUE:NvMemory.gvNvMemory_WriteBuffer.ulAddress:<<MIN>>
TEST.VALUE:NvMemory.gvNvMemory_WriteBuffer.pubBuffer:<<malloc 1>>
TEST.VALUE:NvMemory.gvNvMemory_WriteBuffer.ulBufferLength:<<MIN>>
TEST.END

-- Test Case: BASIS-PATH-002-PARTIAL
TEST.UNIT:NvMemory
TEST.SUBPROGRAM:gvNvMemory_WriteBuffer
TEST.NEW
TEST.NAME:BASIS-PATH-002-PARTIAL
TEST.BASIS_PATH:2 of 2 (partial)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 2
      (1) if ((0U) != (fram_drv.Operation).fWrite) ==> TRUE
   Test Case Generation Notes:
      Cannot set variable (fram_drv.Operation).fWrite in branch 1 since it has a type which requires user code.
TEST.END_NOTES:
TEST.VALUE:NvMemory.gvNvMemory_WriteBuffer.ulAddress:<<MIN>>
TEST.VALUE:NvMemory.gvNvMemory_WriteBuffer.pubBuffer:<<malloc 1>>
TEST.VALUE:NvMemory.gvNvMemory_WriteBuffer.ulBufferLength:<<MIN>>
TEST.END

-- Subprogram: vNvMemory_DeviceWrite

-- Test Case: BASIS-PATH-001
TEST.UNIT:NvMemory
TEST.SUBPROGRAM:vNvMemory_DeviceWrite
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 5
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (1) if ((0U) != pRequest->pubData && (0U) != pRequest->ulLength) ==> FALSE
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:NvMemory.vNvMemory_DeviceWrite.pRequest:<<malloc 1>>
TEST.VALUE:NvMemory.vNvMemory_DeviceWrite.pRequest[0].pubData:<<malloc 9>>
TEST.VALUE:NvMemory.vNvMemory_DeviceWrite.pRequest[0].pubData:<<null>>
TEST.END

-- Test Case: BASIS-PATH-002
TEST.UNIT:NvMemory
TEST.SUBPROGRAM:vNvMemory_DeviceWrite
TEST.NEW
TEST.NAME:BASIS-PATH-002
TEST.BASIS_PATH:2 of 5
TEST.NOTES:
This is an automatically generated test case.
   Test Path 2
      (1) if ((0U) != pRequest->pubData && (0U) != pRequest->ulLength) ==> TRUE
      (2) if ((0U) != pRequest->pDescriptor) ==> FALSE
      (3) if ((0U) != pRequest->ulNvAddr_CRC) ==> FALSE
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:NvMemory.vNvMemory_DeviceWrite.pRequest:<<malloc 1>>
TEST.VALUE:NvMemory.vNvMemory_DeviceWrite.pRequest[0].pDescriptor:<<null>>
TEST.VALUE:NvMemory.vNvMemory_DeviceWrite.pRequest[0].pubData:<<malloc 1>>
TEST.VALUE:NvMemory.vNvMemory_DeviceWrite.pRequest[0].ulLength:1
TEST.VALUE:NvMemory.vNvMemory_DeviceWrite.pRequest[0].ulNvAddr_CRC:0
TEST.END

-- Test Case: BASIS-PATH-003-PARTIAL
TEST.UNIT:NvMemory
TEST.SUBPROGRAM:vNvMemory_DeviceWrite
TEST.NEW
TEST.NAME:BASIS-PATH-003-PARTIAL
TEST.BASIS_PATH:3 of 5 (partial)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 3
      (1) if ((0U) != pRequest->pubData && (0U) != pRequest->ulLength) ==> TRUE
      (2) if ((0U) != pRequest->pDescriptor) ==> FALSE
      (3) if ((0U) != pRequest->ulNvAddr_CRC) ==> TRUE
      (4) if ((0U) == pRequest->puwGet_crc) ==> FALSE
   Test Case Generation Notes:
      Cannot set variable pRequest->puwGet_crc in branch 4 since it has a type which requires user code.
TEST.END_NOTES:
TEST.VALUE:NvMemory.vNvMemory_DeviceWrite.pRequest:<<malloc 1>>
TEST.VALUE:NvMemory.vNvMemory_DeviceWrite.pRequest[0].pDescriptor:<<null>>
TEST.VALUE:NvMemory.vNvMemory_DeviceWrite.pRequest[0].pubData:<<malloc 1>>
TEST.VALUE:NvMemory.vNvMemory_DeviceWrite.pRequest[0].ulLength:1
TEST.VALUE:NvMemory.vNvMemory_DeviceWrite.pRequest[0].ulNvAddr_CRC:1
TEST.END

-- Test Case: BASIS-PATH-004-PARTIAL
TEST.UNIT:NvMemory
TEST.SUBPROGRAM:vNvMemory_DeviceWrite
TEST.NEW
TEST.NAME:BASIS-PATH-004-PARTIAL
TEST.BASIS_PATH:4 of 5 (partial)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 4
      (1) if ((0U) != pRequest->pubData && (0U) != pRequest->ulLength) ==> TRUE
      (2) if ((0U) != pRequest->pDescriptor) ==> FALSE
      (3) if ((0U) != pRequest->ulNvAddr_CRC) ==> TRUE
      (4) if ((0U) == pRequest->puwGet_crc) ==> TRUE
   Test Case Generation Notes:
      Cannot set variable pRequest->puwGet_crc in branch 4 since it has a type which requires user code.
TEST.END_NOTES:
TEST.VALUE:NvMemory.vNvMemory_DeviceWrite.pRequest:<<malloc 1>>
TEST.VALUE:NvMemory.vNvMemory_DeviceWrite.pRequest[0].pDescriptor:<<null>>
TEST.VALUE:NvMemory.vNvMemory_DeviceWrite.pRequest[0].pubData:<<malloc 1>>
TEST.VALUE:NvMemory.vNvMemory_DeviceWrite.pRequest[0].ulLength:1
TEST.VALUE:NvMemory.vNvMemory_DeviceWrite.pRequest[0].ulNvAddr_CRC:1
TEST.END

-- Test Case: BASIS-PATH-005
TEST.UNIT:NvMemory
TEST.SUBPROGRAM:vNvMemory_DeviceWrite
TEST.NEW
TEST.NAME:BASIS-PATH-005
TEST.BASIS_PATH:5 of 5
TEST.NOTES:
This is an automatically generated test case.
   Test Path 5
      (1) if ((0U) != pRequest->pubData && (0U) != pRequest->ulLength) ==> TRUE
      (2) if ((0U) != pRequest->pDescriptor) ==> TRUE
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:NvMemory.vNvMemory_DeviceWrite.pRequest:<<malloc 1>>
TEST.VALUE:NvMemory.vNvMemory_DeviceWrite.pRequest[0].pDescriptor:<<malloc 1>>
TEST.VALUE:NvMemory.vNvMemory_DeviceWrite.pRequest[0].pubData:<<malloc 1>>
TEST.VALUE:NvMemory.vNvMemory_DeviceWrite.pRequest[0].ulLength:1
TEST.END

-- Subprogram: vReadBootParameters

-- Test Case: BASIS-PATH-001-PARTIAL
TEST.UNIT:NvMemory
TEST.SUBPROGRAM:vReadBootParameters
TEST.NEW
TEST.NAME:BASIS-PATH-001-PARTIAL
TEST.BASIS_PATH:1 of 12 (partial)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (1) if (1U != gfNvBkupRead(pNvBackup, pfdo, (uint8_t *)pBootParameters)) ==> FALSE
      (11) case (pBootParameters->uwBaudRateCAN) ==> default
      (12) if (pBootParameters->ubModuleID < 1U || pBootParameters->ubModuleID > 127U) ==> FALSE
   Test Case Generation Notes:
      Cannot set variable to out of range value 1 in branch 1
TEST.END_NOTES:
TEST.VALUE:NvMemory.vReadBootParameters.pNvBackup:<<malloc 1>>
TEST.VALUE:NvMemory.vReadBootParameters.pfdo:<<malloc 1>>
TEST.VALUE:NvMemory.vReadBootParameters.pBootParameters:<<malloc 1>>
TEST.VALUE:NvMemory.vReadBootParameters.pBootParameters[0].ubModuleID:1
TEST.VALUE:NvMemory.vReadBootParameters.pBootParameters[0].uwBaudRateCAN:<<MIN>>
TEST.END

-- Test Case: BASIS-PATH-002-PARTIAL
TEST.UNIT:NvMemory
TEST.SUBPROGRAM:vReadBootParameters
TEST.NEW
TEST.NAME:BASIS-PATH-002-PARTIAL
TEST.BASIS_PATH:2 of 12 (partial)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 2
      (1) if (1U != gfNvBkupRead(pNvBackup, pfdo, (uint8_t *)pBootParameters)) ==> FALSE
      (11) case (pBootParameters->uwBaudRateCAN) ==> default
      (12) if (pBootParameters->ubModuleID < 1U || pBootParameters->ubModuleID > 127U) ==> TRUE
   Test Case Generation Notes:
      Cannot set variable to out of range value 1 in branch 1
TEST.END_NOTES:
TEST.VALUE:NvMemory.vReadBootParameters.pNvBackup:<<malloc 1>>
TEST.VALUE:NvMemory.vReadBootParameters.pfdo:<<malloc 1>>
TEST.VALUE:NvMemory.vReadBootParameters.pBootParameters:<<malloc 1>>
TEST.VALUE:NvMemory.vReadBootParameters.pBootParameters[0].ubModuleID:<<MIN>>
TEST.VALUE:NvMemory.vReadBootParameters.pBootParameters[0].uwBaudRateCAN:<<MIN>>
TEST.END

-- Test Case: BASIS-PATH-003-PARTIAL
TEST.UNIT:NvMemory
TEST.SUBPROGRAM:vReadBootParameters
TEST.NEW
TEST.NAME:BASIS-PATH-003-PARTIAL
TEST.BASIS_PATH:3 of 12 (partial)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 3
      (1) if (1U != gfNvBkupRead(pNvBackup, pfdo, (uint8_t *)pBootParameters)) ==> FALSE
      (2) case (pBootParameters->uwBaudRateCAN) ==> 50
      (12) if (pBootParameters->ubModuleID < 1U || pBootParameters->ubModuleID > 127U) ==> FALSE
   Test Case Generation Notes:
      Cannot set variable to out of range value 1 in branch 1
TEST.END_NOTES:
TEST.VALUE:NvMemory.vReadBootParameters.pNvBackup:<<malloc 1>>
TEST.VALUE:NvMemory.vReadBootParameters.pfdo:<<malloc 1>>
TEST.VALUE:NvMemory.vReadBootParameters.pBootParameters:<<malloc 1>>
TEST.VALUE:NvMemory.vReadBootParameters.pBootParameters[0].ubModuleID:1
TEST.VALUE:NvMemory.vReadBootParameters.pBootParameters[0].uwBaudRateCAN:50
TEST.END

-- Test Case: BASIS-PATH-004-PARTIAL
TEST.UNIT:NvMemory
TEST.SUBPROGRAM:vReadBootParameters
TEST.NEW
TEST.NAME:BASIS-PATH-004-PARTIAL
TEST.BASIS_PATH:4 of 12 (partial)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 4
      (1) if (1U != gfNvBkupRead(pNvBackup, pfdo, (uint8_t *)pBootParameters)) ==> FALSE
      (3) case (pBootParameters->uwBaudRateCAN) ==> 100
      (12) if (pBootParameters->ubModuleID < 1U || pBootParameters->ubModuleID > 127U) ==> FALSE
   Test Case Generation Notes:
      Cannot set variable to out of range value 1 in branch 1
TEST.END_NOTES:
TEST.VALUE:NvMemory.vReadBootParameters.pNvBackup:<<malloc 1>>
TEST.VALUE:NvMemory.vReadBootParameters.pfdo:<<malloc 1>>
TEST.VALUE:NvMemory.vReadBootParameters.pBootParameters:<<malloc 1>>
TEST.VALUE:NvMemory.vReadBootParameters.pBootParameters[0].ubModuleID:1
TEST.VALUE:NvMemory.vReadBootParameters.pBootParameters[0].uwBaudRateCAN:100
TEST.END

-- Test Case: BASIS-PATH-005-PARTIAL
TEST.UNIT:NvMemory
TEST.SUBPROGRAM:vReadBootParameters
TEST.NEW
TEST.NAME:BASIS-PATH-005-PARTIAL
TEST.BASIS_PATH:5 of 12 (partial)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 5
      (1) if (1U != gfNvBkupRead(pNvBackup, pfdo, (uint8_t *)pBootParameters)) ==> FALSE
      (4) case (pBootParameters->uwBaudRateCAN) ==> 125
      (12) if (pBootParameters->ubModuleID < 1U || pBootParameters->ubModuleID > 127U) ==> FALSE
   Test Case Generation Notes:
      Cannot set variable to out of range value 1 in branch 1
TEST.END_NOTES:
TEST.VALUE:NvMemory.vReadBootParameters.pNvBackup:<<malloc 1>>
TEST.VALUE:NvMemory.vReadBootParameters.pfdo:<<malloc 1>>
TEST.VALUE:NvMemory.vReadBootParameters.pBootParameters:<<malloc 1>>
TEST.VALUE:NvMemory.vReadBootParameters.pBootParameters[0].ubModuleID:1
TEST.VALUE:NvMemory.vReadBootParameters.pBootParameters[0].uwBaudRateCAN:125
TEST.END

-- Test Case: BASIS-PATH-006-PARTIAL
TEST.UNIT:NvMemory
TEST.SUBPROGRAM:vReadBootParameters
TEST.NEW
TEST.NAME:BASIS-PATH-006-PARTIAL
TEST.BASIS_PATH:6 of 12 (partial)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 6
      (1) if (1U != gfNvBkupRead(pNvBackup, pfdo, (uint8_t *)pBootParameters)) ==> FALSE
      (5) case (pBootParameters->uwBaudRateCAN) ==> 200
      (12) if (pBootParameters->ubModuleID < 1U || pBootParameters->ubModuleID > 127U) ==> FALSE
   Test Case Generation Notes:
      Cannot set variable to out of range value 1 in branch 1
TEST.END_NOTES:
TEST.VALUE:NvMemory.vReadBootParameters.pNvBackup:<<malloc 1>>
TEST.VALUE:NvMemory.vReadBootParameters.pfdo:<<malloc 1>>
TEST.VALUE:NvMemory.vReadBootParameters.pBootParameters:<<malloc 1>>
TEST.VALUE:NvMemory.vReadBootParameters.pBootParameters[0].ubModuleID:1
TEST.VALUE:NvMemory.vReadBootParameters.pBootParameters[0].uwBaudRateCAN:200
TEST.END

-- Test Case: BASIS-PATH-007-PARTIAL
TEST.UNIT:NvMemory
TEST.SUBPROGRAM:vReadBootParameters
TEST.NEW
TEST.NAME:BASIS-PATH-007-PARTIAL
TEST.BASIS_PATH:7 of 12 (partial)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 7
      (1) if (1U != gfNvBkupRead(pNvBackup, pfdo, (uint8_t *)pBootParameters)) ==> FALSE
      (6) case (pBootParameters->uwBaudRateCAN) ==> 250
      (12) if (pBootParameters->ubModuleID < 1U || pBootParameters->ubModuleID > 127U) ==> FALSE
   Test Case Generation Notes:
      Cannot set variable to out of range value 1 in branch 1
TEST.END_NOTES:
TEST.VALUE:NvMemory.vReadBootParameters.pNvBackup:<<malloc 1>>
TEST.VALUE:NvMemory.vReadBootParameters.pfdo:<<malloc 1>>
TEST.VALUE:NvMemory.vReadBootParameters.pBootParameters:<<malloc 1>>
TEST.VALUE:NvMemory.vReadBootParameters.pBootParameters[0].ubModuleID:1
TEST.VALUE:NvMemory.vReadBootParameters.pBootParameters[0].uwBaudRateCAN:250
TEST.END

-- Test Case: BASIS-PATH-008-PARTIAL
TEST.UNIT:NvMemory
TEST.SUBPROGRAM:vReadBootParameters
TEST.NEW
TEST.NAME:BASIS-PATH-008-PARTIAL
TEST.BASIS_PATH:8 of 12 (partial)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 8
      (1) if (1U != gfNvBkupRead(pNvBackup, pfdo, (uint8_t *)pBootParameters)) ==> FALSE
      (7) case (pBootParameters->uwBaudRateCAN) ==> 400
      (12) if (pBootParameters->ubModuleID < 1U || pBootParameters->ubModuleID > 127U) ==> FALSE
   Test Case Generation Notes:
      Cannot set variable to out of range value 1 in branch 1
TEST.END_NOTES:
TEST.VALUE:NvMemory.vReadBootParameters.pNvBackup:<<malloc 1>>
TEST.VALUE:NvMemory.vReadBootParameters.pfdo:<<malloc 1>>
TEST.VALUE:NvMemory.vReadBootParameters.pBootParameters:<<malloc 1>>
TEST.VALUE:NvMemory.vReadBootParameters.pBootParameters[0].ubModuleID:1
TEST.VALUE:NvMemory.vReadBootParameters.pBootParameters[0].uwBaudRateCAN:400
TEST.END

-- Test Case: BASIS-PATH-009-PARTIAL
TEST.UNIT:NvMemory
TEST.SUBPROGRAM:vReadBootParameters
TEST.NEW
TEST.NAME:BASIS-PATH-009-PARTIAL
TEST.BASIS_PATH:9 of 12 (partial)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 9
      (1) if (1U != gfNvBkupRead(pNvBackup, pfdo, (uint8_t *)pBootParameters)) ==> FALSE
      (8) case (pBootParameters->uwBaudRateCAN) ==> 500
      (12) if (pBootParameters->ubModuleID < 1U || pBootParameters->ubModuleID > 127U) ==> FALSE
   Test Case Generation Notes:
      Cannot set variable to out of range value 1 in branch 1
TEST.END_NOTES:
TEST.VALUE:NvMemory.vReadBootParameters.pNvBackup:<<malloc 1>>
TEST.VALUE:NvMemory.vReadBootParameters.pfdo:<<malloc 1>>
TEST.VALUE:NvMemory.vReadBootParameters.pBootParameters:<<malloc 1>>
TEST.VALUE:NvMemory.vReadBootParameters.pBootParameters[0].ubModuleID:1
TEST.VALUE:NvMemory.vReadBootParameters.pBootParameters[0].uwBaudRateCAN:500
TEST.END

-- Test Case: BASIS-PATH-010-PARTIAL
TEST.UNIT:NvMemory
TEST.SUBPROGRAM:vReadBootParameters
TEST.NEW
TEST.NAME:BASIS-PATH-010-PARTIAL
TEST.BASIS_PATH:10 of 12 (partial)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 10
      (1) if (1U != gfNvBkupRead(pNvBackup, pfdo, (uint8_t *)pBootParameters)) ==> FALSE
      (9) case (pBootParameters->uwBaudRateCAN) ==> 800
      (12) if (pBootParameters->ubModuleID < 1U || pBootParameters->ubModuleID > 127U) ==> FALSE
   Test Case Generation Notes:
      Cannot set variable to out of range value 1 in branch 1
TEST.END_NOTES:
TEST.VALUE:NvMemory.vReadBootParameters.pNvBackup:<<malloc 1>>
TEST.VALUE:NvMemory.vReadBootParameters.pfdo:<<malloc 1>>
TEST.VALUE:NvMemory.vReadBootParameters.pBootParameters:<<malloc 1>>
TEST.VALUE:NvMemory.vReadBootParameters.pBootParameters[0].ubModuleID:1
TEST.VALUE:NvMemory.vReadBootParameters.pBootParameters[0].uwBaudRateCAN:800
TEST.END

-- Test Case: BASIS-PATH-011-PARTIAL
TEST.UNIT:NvMemory
TEST.SUBPROGRAM:vReadBootParameters
TEST.NEW
TEST.NAME:BASIS-PATH-011-PARTIAL
TEST.BASIS_PATH:11 of 12 (partial)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 11
      (1) if (1U != gfNvBkupRead(pNvBackup, pfdo, (uint8_t *)pBootParameters)) ==> FALSE
      (10) case (pBootParameters->uwBaudRateCAN) ==> 1000
      (12) if (pBootParameters->ubModuleID < 1U || pBootParameters->ubModuleID > 127U) ==> FALSE
   Test Case Generation Notes:
      Cannot set variable to out of range value 1 in branch 1
TEST.END_NOTES:
TEST.VALUE:NvMemory.vReadBootParameters.pNvBackup:<<malloc 1>>
TEST.VALUE:NvMemory.vReadBootParameters.pfdo:<<malloc 1>>
TEST.VALUE:NvMemory.vReadBootParameters.pBootParameters:<<malloc 1>>
TEST.VALUE:NvMemory.vReadBootParameters.pBootParameters[0].ubModuleID:1
TEST.VALUE:NvMemory.vReadBootParameters.pBootParameters[0].uwBaudRateCAN:1000
TEST.END

-- Test Case: BASIS-PATH-012
TEST.UNIT:NvMemory
TEST.SUBPROGRAM:vReadBootParameters
TEST.NEW
TEST.NAME:BASIS-PATH-012
TEST.BASIS_PATH:12 of 12
TEST.NOTES:
This is an automatically generated test case.
   Test Path 12
      (1) if (1U != gfNvBkupRead(pNvBackup, pfdo, (uint8_t *)pBootParameters)) ==> TRUE
      (11) case (pBootParameters->uwBaudRateCAN) ==> default
      (12) if (pBootParameters->ubModuleID < 1U || pBootParameters->ubModuleID > 127U) ==> FALSE
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:NvMemory.vReadBootParameters.pNvBackup:<<malloc 1>>
TEST.VALUE:NvMemory.vReadBootParameters.pfdo:<<malloc 1>>
TEST.VALUE:NvMemory.vReadBootParameters.pBootParameters:<<malloc 1>>
TEST.VALUE:NvMemory.vReadBootParameters.pBootParameters[0].ubModuleID:1
TEST.VALUE:NvMemory.vReadBootParameters.pBootParameters[0].uwBaudRateCAN:<<MIN>>
TEST.VALUE:uut_prototype_stubs.gfNvBkupRead.return:<<MIN>>
TEST.END
