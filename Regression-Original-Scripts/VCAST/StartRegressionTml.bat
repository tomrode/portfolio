set PATH=%PATH%;C:\ghs\comp_201654;C:\ghs\multi_714

set VECTORCAST_DIR=C:\VCAST\6.4v
set CROWN_SOURCE_BASE=C:\rtc_workspaces\TmlVcmMasterBuildDef001_Sandbox
set VCAST_SUPPORT_FILES=%cd%\supportFiles

SET VCAST_DISABLE_TEST_MAINTENANCE=1

set VCAST_CCS_INSTALL_DIR=C:\ti\ccsv7

REM Path to tools for debugging
set VCAST_CCS_COMPILER_INSTALL_DIR=C:\ti\c2000_6.2.11

set BIOS_INSTALL_DIR=C:\ti\bios_6_35_04_50

REM Path to compiler
set PATH=%VCAST_CCS_COMPILER_INSTALL_DIR%\bin;%BIOS_INSTALL_DIR%\xdctools;%PATH%;

set C2000INC=%VCAST_CCS_INSTALL_DIR%\ccs_base\c2000\include
set C2000LIB=%VCAST_CCS_INSTALL_DIR%\ccs_base\c2000\nowFlash\libraries



REM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
REM Building and Executing the Project
REM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
%VECTORCAST_DIR%\manage --project=C1515 --workspace=.\C1515\build --build-execute



REM %%%%%%%%%%%%%%%%%%%%%%%%%%
REM Generating Reports Section
REM %%%%%%%%%%%%%%%%%%%%%%%%%%

REM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
REM Generating Reports SUPV_CommonTruckCode
REM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
%VECTORCAST_DIR%\manage --project=C1515 --level=Source/Windows/Green_Hills_PPC_Bare_Board_Simulator/SUPV_CommonTruckCode --full-status=SUPV_CommonTruckCode_status.html

REM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
REM Generating Reports SUPV_TRUCK_TYPE_ESR_REGEN
REM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
%VECTORCAST_DIR%\manage --project=C1515 --level=Source/Windows/Green_Hills_PPC_Bare_Board_Simulator/SUPV_TRUCK_TYPE_ESR_REGEN --full-status=SUPV_TRUCK_TYPE_ESR_REGEN_status.html

REM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
REM Generating Reports SUPV_TRUCK_TYPE_RM_42
REM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
%VECTORCAST_DIR%\manage --project=C1515 --level=Source/Windows/Green_Hills_PPC_Bare_Board_Simulator/SUPV_TRUCK_TYPE_RM_42 --full-status=SUPV_TRUCK_TYPE_RM_42_status.html

REM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
REM Generating Reports SUPV_TRUCK_TYPE_TC
REM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
%VECTORCAST_DIR%\manage --project=C1515 --level=Source/Windows/Green_Hills_PPC_Bare_Board_Simulator/SUPV_TRUCK_TYPE_TC --full-status=SUPV_TRUCK_TYPE_TC_status.html

REM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
REM Generating Reports CommonTruckCode
REM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
%VECTORCAST_DIR%\manage --project=C1515 --level=Source/Windows/Green_Hills_PPC_Bare_Board_Simulator/CommonTruckCode --full-status=CommonTruckCode_status.html

REM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
REM Generating Reports REGRESSION
REM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
%VECTORCAST_DIR%\manage --project=C1515 --level=Source/Windows/Green_Hills_PPC_Bare_Board_Simulator/REGRESSION --full-status=REGRESSION_status.html

REM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
REM Generating Reports TRUCK_TYPE_ESR_REGEN
REM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
%VECTORCAST_DIR%\manage --project=C1515 --level=Source/Windows/Green_Hills_PPC_Bare_Board_Simulator/TRUCK_TYPE_ESR_REGEN --full-status=TRUCK_TYPE_ESR_REGEN_status.html

REM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
REM Generating Reports TRUCK_TYPE_RM_42
REM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
%VECTORCAST_DIR%\manage --project=C1515 --level=Source/Windows/Green_Hills_PPC_Bare_Board_Simulator/TRUCK_TYPE_RM_42 --full-status=TRUCK_TYPE_RM_42_status.html

REM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
REM Generating Reports TRUCK_TYPE_TC
REM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
%VECTORCAST_DIR%\manage --project=C1515 --level=Source/Windows/Green_Hills_PPC_Bare_Board_Simulator/TRUCK_TYPE_TC --full-status=TRUCK_TYPE_TC_status.html

REM %%%%%%%%%%%%%%%%%%%%%%%
REM Generating Full Reports
REM %%%%%%%%%%%%%%%%%%%%%%%
 
REM %VECTORCAST_DIR%\manage --project=C1515 --full-status=full_status.html

REM %VECTORCAST_DIR%\vcastqt -lc