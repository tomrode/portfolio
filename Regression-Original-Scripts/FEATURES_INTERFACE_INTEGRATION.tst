-- VectorCAST 6.4p (01/31/17)
-- Test Case Script
-- 
-- Environment    : FEATURES_INTERFACE_INTEGRATION
-- Unit(s) Under Test: features_interface
-- 
-- Script Features
TEST.SCRIPT_FEATURE:C_DIRECT_ARRAY_INDEXING
TEST.SCRIPT_FEATURE:CPP_CLASS_OBJECT_REVISION
TEST.SCRIPT_FEATURE:MULTIPLE_UUT_SUPPORT
TEST.SCRIPT_FEATURE:MIXED_CASE_NAMES
TEST.SCRIPT_FEATURE:STATIC_HEADER_FUNCS_IN_UUTS
--

-- Unit: features_interface

-- Subprogram: gvFI_SetFeatures

-- Test Case: NO_HSL_!=_gpFeatures_bus->eHighSpeedLowerType_
TEST.UNIT:features_interface
TEST.SUBPROGRAM:gvFI_SetFeatures
TEST.NEW
TEST.NAME:NO_HSL_!=_gpFeatures_bus->eHighSpeedLowerType_
TEST.VALUE:features_interface.<<GLOBAL>>.gpFeatures_bus:<<malloc 1>>
TEST.VALUE:features_interface.<<GLOBAL>>.gpFeatures_bus[0].eHighSpeedLowerType:HSL_MONO_MAST
TEST.VALUE_USER_CODE:features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield
<<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>> |= (0x0000004u );
VECTORCAST_ULONG = <<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>>;
TEST.END_VALUE_USER_CODE:
TEST.EXPECTED_GLOBALS_USER_CODE:features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield
VECTORCAST_ULONG = <<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>>;
VECTORCAST_ULONG |= 0x00000004u;

{{ VECTORCAST_ULONG == <<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>>}}
TEST.END_EXPECTED_GLOBALS_USER_CODE:
TEST.END

-- Test Case: gpFeatures_bus->eHeightSenseType__FULL_HGT_SENSE_ONE_SENSOR:
TEST.UNIT:features_interface
TEST.SUBPROGRAM:gvFI_SetFeatures
TEST.NEW
TEST.NAME:gpFeatures_bus->eHeightSenseType__FULL_HGT_SENSE_ONE_SENSOR:
TEST.VALUE:features_interface.<<GLOBAL>>.gpFeatures_bus:<<malloc 1>>
TEST.VALUE:features_interface.<<GLOBAL>>.gpFeatures_bus[0].eHeightSenseType:FULL_HGT_SENSE_ONE_SENSOR
TEST.VALUE_USER_CODE:features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield
<<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>> |= (0x00002000u);
VECTORCAST_ULONG = <<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>>;

TEST.END_VALUE_USER_CODE:
TEST.EXPECTED_GLOBALS_USER_CODE:features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield
VECTORCAST_ULONG = <<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>>;
VECTORCAST_ULONG |= 0x00002000u;

{{ VECTORCAST_ULONG == <<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>>}}
TEST.END_EXPECTED_GLOBALS_USER_CODE:
TEST.END

-- Test Case: gpFeatures_bus->eHeightSenseType__FULL_HGT_SENSE_TWO_SENSOR:
TEST.UNIT:features_interface
TEST.SUBPROGRAM:gvFI_SetFeatures
TEST.NEW
TEST.NAME:gpFeatures_bus->eHeightSenseType__FULL_HGT_SENSE_TWO_SENSOR:
TEST.VALUE:features_interface.<<GLOBAL>>.gpFeatures_bus:<<malloc 1>>
TEST.VALUE:features_interface.<<GLOBAL>>.gpFeatures_bus[0].eHeightSenseType:FULL_HGT_SENSE_TWO_SENSORS
TEST.VALUE_USER_CODE:features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield
<<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>> |= (0x00004000u);
VECTORCAST_ULONG = <<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>>;
TEST.END_VALUE_USER_CODE:
TEST.EXPECTED_GLOBALS_USER_CODE:features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield
VECTORCAST_ULONG = <<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>>;
VECTORCAST_ULONG |= 0x00004000u;

{{ VECTORCAST_ULONG == <<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>>}}
TEST.END_EXPECTED_GLOBALS_USER_CODE:
TEST.END

-- Test Case: gpFeatures_bus->eHeightSenseType__SECONDARY_ONLY
TEST.UNIT:features_interface
TEST.SUBPROGRAM:gvFI_SetFeatures
TEST.NEW
TEST.NAME:gpFeatures_bus->eHeightSenseType__SECONDARY_ONLY
TEST.VALUE:features_interface.<<GLOBAL>>.gpFeatures_bus:<<malloc 1>>
TEST.VALUE:features_interface.<<GLOBAL>>.gpFeatures_bus[0].eHeightSenseType:SECONDARY_ONLY
TEST.VALUE_USER_CODE:features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield
<<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>> |= (0x00008000u);
VECTORCAST_ULONG = <<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>>;
TEST.END_VALUE_USER_CODE:
TEST.EXPECTED_GLOBALS_USER_CODE:features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield
VECTORCAST_ULONG = <<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>>;
VECTORCAST_ULONG |= 0x00008000u;

{{ VECTORCAST_ULONG == <<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>>}}
TEST.END_EXPECTED_GLOBALS_USER_CODE:
TEST.END

-- Test Case: gvFI_SetFeatures.001
TEST.UNIT:features_interface
TEST.SUBPROGRAM:gvFI_SetFeatures
TEST.NEW
TEST.NAME:gvFI_SetFeatures.001
TEST.VALUE:<<OPTIONS>>.EVENT_LIMIT:500
TEST.VALUE:features_interface.<<GLOBAL>>.xfFeaturesConfigured:1,0
TEST.VALUE:features_interface.<<GLOBAL>>.gpFeatures_bus:<<malloc 1>>
TEST.EXPECTED:features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield:289409035
TEST.END

-- Test Case: if_(_(E_STEER_==_gpFeatures_bus->eSteeringType)_||_(E_STEER_ASSIST_==_gpFeatures_bus->eSteeringType)_)
TEST.UNIT:features_interface
TEST.SUBPROGRAM:gvFI_SetFeatures
TEST.NEW
TEST.NAME:if_(_(E_STEER_==_gpFeatures_bus->eSteeringType)_||_(E_STEER_ASSIST_==_gpFeatures_bus->eSteeringType)_)
TEST.VALUE:features_interface.<<GLOBAL>>.gpFeatures_bus:<<malloc 1>>
TEST.VALUE:features_interface.<<GLOBAL>>.gpFeatures_bus[0].eSteeringType:E_STEER_ASSIST
TEST.VALUE_USER_CODE:features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield
<<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>> |= (0x00000010u );
VECTORCAST_ULONG = <<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>>;

TEST.END_VALUE_USER_CODE:
TEST.EXPECTED_GLOBALS_USER_CODE:features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield
VECTORCAST_ULONG = <<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>>;
VECTORCAST_ULONG |= 0x00000010u;

{{ VECTORCAST_ULONG == <<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>>}}

TEST.END_EXPECTED_GLOBALS_USER_CODE:
TEST.END

-- Test Case: if_(_1U_==_(boolean)gpFeatures_bus->eUseAccy1_)
TEST.UNIT:features_interface
TEST.SUBPROGRAM:gvFI_SetFeatures
TEST.NEW
TEST.NAME:if_(_1U_==_(boolean)gpFeatures_bus->eUseAccy1_)
TEST.VALUE:features_interface.<<GLOBAL>>.gpFeatures_bus:<<malloc 1>>
TEST.VALUE:features_interface.<<GLOBAL>>.gpFeatures_bus[0].eUseAccy1:Accy1_Active
TEST.VALUE_USER_CODE:features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield
<<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>> |= (0x00000100u );
VECTORCAST_ULONG = <<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>>;

TEST.END_VALUE_USER_CODE:
TEST.EXPECTED_GLOBALS_USER_CODE:features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield
VECTORCAST_ULONG = <<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>>;
VECTORCAST_ULONG |= 0x00000100u;

{{ VECTORCAST_ULONG == <<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>>}}
TEST.END_EXPECTED_GLOBALS_USER_CODE:
TEST.END

-- Test Case: if_(_1U_==_(boolean)gpFeatures_bus->eUseAccy2_)
TEST.UNIT:features_interface
TEST.SUBPROGRAM:gvFI_SetFeatures
TEST.NEW
TEST.NAME:if_(_1U_==_(boolean)gpFeatures_bus->eUseAccy2_)
TEST.VALUE:features_interface.<<GLOBAL>>.gpFeatures_bus:<<malloc 1>>
TEST.VALUE:features_interface.<<GLOBAL>>.gpFeatures_bus[0].eUseAccy2:Accy2_with_FSS
TEST.VALUE_USER_CODE:features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield
<<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>> |= (0x00000200u );
VECTORCAST_ULONG = <<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>>;

TEST.END_VALUE_USER_CODE:
TEST.EXPECTED_GLOBALS_USER_CODE:features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield
VECTORCAST_ULONG = <<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>>;
VECTORCAST_ULONG |= 0x00000200u;

{{ VECTORCAST_ULONG == <<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>>}}
TEST.END_EXPECTED_GLOBALS_USER_CODE:
TEST.END

-- Test Case: if_(_1U_==_(boolean)gpFeatures_bus->eUseAccy3_)
TEST.UNIT:features_interface
TEST.SUBPROGRAM:gvFI_SetFeatures
TEST.NEW
TEST.NAME:if_(_1U_==_(boolean)gpFeatures_bus->eUseAccy3_)
TEST.VALUE:features_interface.<<GLOBAL>>.gpFeatures_bus:<<malloc 1>>
TEST.VALUE:features_interface.<<GLOBAL>>.gpFeatures_bus[0].eUseAccy3:Accy3_Active
TEST.VALUE_USER_CODE:features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield
<<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>> |= (0x00000400u );
VECTORCAST_ULONG = <<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>>;
TEST.END_VALUE_USER_CODE:
TEST.EXPECTED_GLOBALS_USER_CODE:features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield
VECTORCAST_ULONG = <<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>>;
VECTORCAST_ULONG |= 0x00000400u;

{{ VECTORCAST_ULONG == <<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>>}}
TEST.END_EXPECTED_GLOBALS_USER_CODE:
TEST.END

-- Test Case: if_(_1U_==_(boolean)gpFeatures_bus->eUseTilt_)
TEST.UNIT:features_interface
TEST.SUBPROGRAM:gvFI_SetFeatures
TEST.NEW
TEST.NAME:if_(_1U_==_(boolean)gpFeatures_bus->eUseTilt_)
TEST.VALUE:features_interface.<<GLOBAL>>.gpFeatures_bus:<<malloc 1>>
TEST.VALUE:features_interface.<<GLOBAL>>.gpFeatures_bus[0].eUseTilt:Tilt_Active
TEST.VALUE_USER_CODE:features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield
<<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>> |= (0x00000080u );
VECTORCAST_ULONG = <<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>>;

TEST.END_VALUE_USER_CODE:
TEST.EXPECTED_GLOBALS_USER_CODE:features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield
VECTORCAST_ULONG = <<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>>;
VECTORCAST_ULONG |= 0x00000080u;

{{ VECTORCAST_ULONG == <<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>>}}
TEST.END_EXPECTED_GLOBALS_USER_CODE:
TEST.END

-- Test Case: if_(_AGV_Active_!=_gFeatures_bus.eAGV_)
TEST.UNIT:features_interface
TEST.SUBPROGRAM:gvFI_SetFeatures
TEST.NEW
TEST.NAME:if_(_AGV_Active_!=_gFeatures_bus.eAGV_)
TEST.NOTES:
Could not get this to be equal thus failing test 
TEST.END_NOTES:
TEST.VALUE:<<OPTIONS>>.EVENT_LIMIT:200
TEST.VALUE:features_interface.<<GLOBAL>>.gpFeatures_bus:<<malloc 1>>
TEST.VALUE:features_interface.<<GLOBAL>>.gpFeatures_bus[0].eAGV:AGV_Not_Active
TEST.VALUE_USER_CODE:features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield
<<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>> |= (0x10000000u );
VECTORCAST_ULONG = <<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>>;
TEST.END_VALUE_USER_CODE:
TEST.EXPECTED_GLOBALS_USER_CODE:features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield
VECTORCAST_ULONG = <<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>>;
VECTORCAST_ULONG |= 0x10000000u;

{{ VECTORCAST_ULONG == <<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>>}}
TEST.END_EXPECTED_GLOBALS_USER_CODE:
TEST.END

-- Test Case: if_(_Encoder_Tilt_Sense_==_gpFeatures_bus->eUseTiltSense_)
TEST.UNIT:features_interface
TEST.SUBPROGRAM:gvFI_SetFeatures
TEST.NEW
TEST.NAME:if_(_Encoder_Tilt_Sense_==_gpFeatures_bus->eUseTiltSense_)
TEST.VALUE:features_interface.<<GLOBAL>>.gpFeatures_bus:<<malloc 1>>
TEST.VALUE:features_interface.<<GLOBAL>>.gpFeatures_bus[0].eUseTiltSense:Encoder_Tilt_Sense
TEST.VALUE_USER_CODE:features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield
<<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>> |= (0x20000000u );
VECTORCAST_ULONG = <<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>>;
TEST.END_VALUE_USER_CODE:
TEST.EXPECTED_GLOBALS_USER_CODE:features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield
VECTORCAST_ULONG = <<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>>;
VECTORCAST_ULONG |= 0x20000000u;

{{ VECTORCAST_ULONG == <<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>>}}
TEST.END_EXPECTED_GLOBALS_USER_CODE:
TEST.END

-- Test Case: if_(_LWB_Present_==_gFeatures_bus.eLoadWheelBrakes_)
TEST.UNIT:features_interface
TEST.SUBPROGRAM:gvFI_SetFeatures
TEST.NEW
TEST.NAME:if_(_LWB_Present_==_gFeatures_bus.eLoadWheelBrakes_)
TEST.NOTES:
Cannot get this to equal
TEST.END_NOTES:
TEST.VALUE:features_interface.<<GLOBAL>>.gpFeatures_bus:<<malloc 1>>
TEST.VALUE:features_interface.<<GLOBAL>>.gpFeatures_bus[0].eLoadWheelBrakes:LWB_Present
TEST.END

-- Test Case: if_(_NO_ALARM_!=_gpFeatures_bus->eTravelAlarm_)
TEST.UNIT:features_interface
TEST.SUBPROGRAM:gvFI_SetFeatures
TEST.NEW
TEST.NAME:if_(_NO_ALARM_!=_gpFeatures_bus->eTravelAlarm_)
TEST.VALUE:features_interface.<<GLOBAL>>.gpFeatures_bus:<<malloc 1>>
TEST.VALUE:features_interface.<<GLOBAL>>.gpFeatures_bus[0].eTravelAlarm:BOTH_ALARM
TEST.VALUE_USER_CODE:features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield
<<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>> |= (0x00001000u );
VECTORCAST_ULONG = <<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>>;
TEST.END_VALUE_USER_CODE:
TEST.EXPECTED_GLOBALS_USER_CODE:features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield
VECTORCAST_ULONG = <<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>>;
VECTORCAST_ULONG |= 0x00001000u ;

{{ VECTORCAST_ULONG == <<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>>}}
TEST.END_EXPECTED_GLOBALS_USER_CODE:
TEST.END

-- Test Case: if_(_NO_AUX_!=_gpFeatures_bus->eAuxHoistType_)
TEST.UNIT:features_interface
TEST.SUBPROGRAM:gvFI_SetFeatures
TEST.NEW
TEST.NAME:if_(_NO_AUX_!=_gpFeatures_bus->eAuxHoistType_)
TEST.VALUE:features_interface.<<GLOBAL>>.gpFeatures_bus:<<malloc 1>>
TEST.VALUE:features_interface.<<GLOBAL>>.gpFeatures_bus[0].eAuxHoistType:AUX_SYSTEM
TEST.VALUE_USER_CODE:features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield
<<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>> |= (0x00000040u );
VECTORCAST_ULONG = <<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>>;
TEST.END_VALUE_USER_CODE:
TEST.EXPECTED_GLOBALS_USER_CODE:features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield
VECTORCAST_ULONG = <<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>>;
VECTORCAST_ULONG |= 0x00000040u;

{{ VECTORCAST_ULONG == <<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>>}}
TEST.END_EXPECTED_GLOBALS_USER_CODE:
TEST.END

-- Test Case: if_(_NO_Accy2_!=_gpFeatures_bus->eUseAccy2_)
TEST.UNIT:features_interface
TEST.SUBPROGRAM:gvFI_SetFeatures
TEST.NEW
TEST.NAME:if_(_NO_Accy2_!=_gpFeatures_bus->eUseAccy2_)
TEST.VALUE:features_interface.<<GLOBAL>>.gpFeatures_bus:<<malloc 1>>
TEST.VALUE:features_interface.<<GLOBAL>>.gpFeatures_bus[0].eUseAccy2:Accy2_with_FSS
TEST.VALUE_USER_CODE:features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield
<<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>> |= (0x00000200u );
VECTORCAST_ULONG = <<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>>;
TEST.END_VALUE_USER_CODE:
TEST.EXPECTED_GLOBALS_USER_CODE:features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield
VECTORCAST_ULONG = <<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>>;
VECTORCAST_ULONG |= 0x00000200u;

{{ VECTORCAST_ULONG == <<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>>}}
TEST.END_EXPECTED_GLOBALS_USER_CODE:
TEST.END

-- Test Case: if_(_NO_TPA_!=_gFeatures_bus.eTPA_Type_)
TEST.UNIT:features_interface
TEST.SUBPROGRAM:gvFI_SetFeatures
TEST.NEW
TEST.NAME:if_(_NO_TPA_!=_gFeatures_bus.eTPA_Type_)
TEST.NOTES:
Cannot get this to equal
TEST.END_NOTES:
TEST.VALUE:features_interface.<<GLOBAL>>.gpFeatures_bus:<<malloc 1>>
TEST.VALUE:features_interface.<<GLOBAL>>.gpFeatures_bus[0].eTPA_Type:TPA_Load_Compensated_without_FSS
TEST.END

-- Test Case: if_(_NoLoadSense_!=_gpFeatures_bus->eLoadSenseType_)
TEST.UNIT:features_interface
TEST.SUBPROGRAM:gvFI_SetFeatures
TEST.NEW
TEST.NAME:if_(_NoLoadSense_!=_gpFeatures_bus->eLoadSenseType_)
TEST.VALUE:features_interface.<<GLOBAL>>.gpFeatures_bus:<<malloc 1>>
TEST.VALUE:features_interface.<<GLOBAL>>.gpFeatures_bus[0].eLoadSenseType:MainMast
TEST.VALUE_USER_CODE:features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield
<<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>> |= (0x00010000u);
VECTORCAST_ULONG = <<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>>;
TEST.END_VALUE_USER_CODE:
TEST.EXPECTED_GLOBALS_USER_CODE:features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield
VECTORCAST_ULONG = <<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>>;
VECTORCAST_ULONG |= 0x00010000u;

{{ VECTORCAST_ULONG == <<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>>}}
TEST.END_EXPECTED_GLOBALS_USER_CODE:
TEST.END

-- Test Case: if_(_OC_Levers_==_gpFeatures_bus->eOperatorControlType_)
TEST.UNIT:features_interface
TEST.SUBPROGRAM:gvFI_SetFeatures
TEST.NEW
TEST.NAME:if_(_OC_Levers_==_gpFeatures_bus->eOperatorControlType_)
TEST.VALUE:features_interface.<<GLOBAL>>.gpFeatures_bus:<<malloc 1>>
TEST.VALUE:features_interface.<<GLOBAL>>.gpFeatures_bus[0].eOperatorControlType:OC_Levers
TEST.VALUE_USER_CODE:features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield
<<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>> |= (0x00400000u);
VECTORCAST_ULONG = <<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>>;
TEST.END_VALUE_USER_CODE:
TEST.EXPECTED_GLOBALS_USER_CODE:features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield
VECTORCAST_ULONG = <<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>>;
VECTORCAST_ULONG |= 0x00400000u;

{{ VECTORCAST_ULONG == <<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>>}}
TEST.END_EXPECTED_GLOBALS_USER_CODE:
TEST.END

-- Test Case: if_(_Poti_Tilt_Sense_==_gpFeatures_bus->eUseTiltSense_)
TEST.UNIT:features_interface
TEST.SUBPROGRAM:gvFI_SetFeatures
TEST.NEW
TEST.NAME:if_(_Poti_Tilt_Sense_==_gpFeatures_bus->eUseTiltSense_)
TEST.VALUE:features_interface.<<GLOBAL>>.gpFeatures_bus:<<malloc 1>>
TEST.VALUE:features_interface.<<GLOBAL>>.gpFeatures_bus[0].eUseTiltSense:Poti_Tilt_Sense
TEST.VALUE_USER_CODE:features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield
<<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>> |= (0x40000000u );
VECTORCAST_ULONG = <<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>>;
TEST.END_VALUE_USER_CODE:
TEST.EXPECTED_GLOBALS_USER_CODE:features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield
VECTORCAST_ULONG = <<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>>;
VECTORCAST_ULONG |= 0x40000000u;

{{ VECTORCAST_ULONG == <<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>>}}
TEST.END_EXPECTED_GLOBALS_USER_CODE:
TEST.END

-- Test Case: if_(_STROBE_OFF_!=_gpFeatures_bus->eStrobeType_)
TEST.UNIT:features_interface
TEST.SUBPROGRAM:gvFI_SetFeatures
TEST.NEW
TEST.NAME:if_(_STROBE_OFF_!=_gpFeatures_bus->eStrobeType_)
TEST.VALUE:features_interface.<<GLOBAL>>.gpFeatures_bus:<<malloc 1>>
TEST.VALUE:features_interface.<<GLOBAL>>.gpFeatures_bus[0].eStrobeType:STROBE_BOTH
TEST.VALUE_USER_CODE:features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield
<<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>> |= (0x00800000u );
VECTORCAST_ULONG = <<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>>;
TEST.END_VALUE_USER_CODE:
TEST.EXPECTED_GLOBALS_USER_CODE:features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield
VECTORCAST_ULONG = <<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>>;
VECTORCAST_ULONG |= 0x00800000u;

{{ VECTORCAST_ULONG == <<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>>}}
TEST.END_EXPECTED_GLOBALS_USER_CODE:
TEST.END

-- Test Case: if_(_gpFeatures_bus->eUseChainSlackDetection_>_0_)
TEST.UNIT:features_interface
TEST.SUBPROGRAM:gvFI_SetFeatures
TEST.NEW
TEST.NAME:if_(_gpFeatures_bus->eUseChainSlackDetection_>_0_)
TEST.VALUE:features_interface.<<GLOBAL>>.gpFeatures_bus:<<malloc 1>>
TEST.VALUE:features_interface.<<GLOBAL>>.gpFeatures_bus[0].eUseChainSlackDetection:CS_Active
TEST.VALUE_USER_CODE:features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield
<<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>> |= (0x00020000u);
VECTORCAST_ULONG = <<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>>;
TEST.END_VALUE_USER_CODE:
TEST.EXPECTED_GLOBALS_USER_CODE:features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield
VECTORCAST_ULONG = <<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>>;
VECTORCAST_ULONG |= 0x00020000u;

{{ VECTORCAST_ULONG == <<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>>}}
TEST.END_EXPECTED_GLOBALS_USER_CODE:
TEST.END

-- Test Case: if_(_gpFeatures_bus->ulNumImpactSensors_>_0u_)
TEST.UNIT:features_interface
TEST.SUBPROGRAM:gvFI_SetFeatures
TEST.NEW
TEST.NAME:if_(_gpFeatures_bus->ulNumImpactSensors_>_0u_)
TEST.VALUE:<<OPTIONS>>.EVENT_LIMIT:100
TEST.VALUE:features_interface.<<GLOBAL>>.gpFeatures_bus:<<malloc 1>>
TEST.VALUE:features_interface.<<GLOBAL>>.gpFeatures_bus[0].ulNumImpactSensors:1
TEST.VALUE_USER_CODE:features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield
<<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>> |= (0x00040000u);
VECTORCAST_ULONG = <<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>>;
TEST.END_VALUE_USER_CODE:
TEST.EXPECTED_GLOBALS_USER_CODE:features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield
VECTORCAST_ULONG = <<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>>;
VECTORCAST_ULONG |= 0x00040000u;

{{ VECTORCAST_ULONG == <<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>>}}
TEST.END_EXPECTED_GLOBALS_USER_CODE:
TEST.END

-- Test Case: if_(_gpFeatures_bus->ulNumImpactSensors_>_1u_)
TEST.UNIT:features_interface
TEST.SUBPROGRAM:gvFI_SetFeatures
TEST.NEW
TEST.NAME:if_(_gpFeatures_bus->ulNumImpactSensors_>_1u_)
TEST.VALUE:<<OPTIONS>>.EVENT_LIMIT:100
TEST.VALUE:features_interface.<<GLOBAL>>.gpFeatures_bus:<<malloc 1>>
TEST.VALUE:features_interface.<<GLOBAL>>.gpFeatures_bus[0].ulNumImpactSensors:2
TEST.VALUE_USER_CODE:features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield
<<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>> |= (0x00080000u);
VECTORCAST_ULONG = <<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>>;
TEST.END_VALUE_USER_CODE:
TEST.EXPECTED_GLOBALS_USER_CODE:features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield
VECTORCAST_ULONG = <<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>>;
VECTORCAST_ULONG |= 0x00080000u;

{{ VECTORCAST_ULONG == <<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>>}}
TEST.END_EXPECTED_GLOBALS_USER_CODE:
TEST.END

-- Test Case: if_(_gpFeatures_bus->ulNumImpactSensors_>_2u_)
TEST.UNIT:features_interface
TEST.SUBPROGRAM:gvFI_SetFeatures
TEST.NEW
TEST.NAME:if_(_gpFeatures_bus->ulNumImpactSensors_>_2u_)
TEST.VALUE:<<OPTIONS>>.EVENT_LIMIT:100
TEST.VALUE:features_interface.<<GLOBAL>>.gpFeatures_bus:<<malloc 1>>
TEST.VALUE:features_interface.<<GLOBAL>>.gpFeatures_bus[0].ulNumImpactSensors:3
TEST.VALUE_USER_CODE:features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield
<<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>> |= (0x00100000u);
VECTORCAST_ULONG = <<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>>;
TEST.END_VALUE_USER_CODE:
TEST.EXPECTED_GLOBALS_USER_CODE:features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield
VECTORCAST_ULONG = <<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>>;
VECTORCAST_ULONG |= 0x00100000u;

{{ VECTORCAST_ULONG == <<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>>}}
TEST.END_EXPECTED_GLOBALS_USER_CODE:
TEST.END

-- Test Case: if_(_gpFeatures_bus->ulNumImpactSensors_>_3u_)
TEST.UNIT:features_interface
TEST.SUBPROGRAM:gvFI_SetFeatures
TEST.NEW
TEST.NAME:if_(_gpFeatures_bus->ulNumImpactSensors_>_3u_)
TEST.VALUE:<<OPTIONS>>.EVENT_LIMIT:100
TEST.VALUE:features_interface.<<GLOBAL>>.gpFeatures_bus:<<malloc 1>>
TEST.VALUE:features_interface.<<GLOBAL>>.gpFeatures_bus[0].ulNumImpactSensors:4
TEST.VALUE_USER_CODE:features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield
<<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>> |= (0x00200000u);
VECTORCAST_ULONG = <<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>>;
TEST.END_VALUE_USER_CODE:
TEST.EXPECTED_GLOBALS_USER_CODE:features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield
VECTORCAST_ULONG = <<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>>;
VECTORCAST_ULONG |= 0x00200000u;

{{ VECTORCAST_ULONG == <<features_interface.<<GLOBAL>>.xulEnabledFeaturesBitfield>>}}
TEST.END_EXPECTED_GLOBALS_USER_CODE:
TEST.END

-- COMPOUND TESTS

TEST.SUBPROGRAM:<<COMPOUND>>
TEST.NEW
TEST.NAME:gvFI_SetFeatures
TEST.SLOT: "1", "features_interface", "gvFI_SetFeatures", "1", "gvFI_SetFeatures.001"
TEST.SLOT: "2", "features_interface", "gvFI_SetFeatures", "1", "if_(_(E_STEER_==_gpFeatures_bus->eSteeringType)_||_(E_STEER_ASSIST_==_gpFeatures_bus->eSteeringType)_)"
TEST.END
--
