-- VectorCAST 6.4p (01/31/17)
-- Test Case Script
-- 
-- Environment    : SUPV_INTERFACE_INTEGRATION
-- Unit(s) Under Test: features_interface supv_interface
-- 
-- Script Features
TEST.SCRIPT_FEATURE:C_DIRECT_ARRAY_INDEXING
TEST.SCRIPT_FEATURE:CPP_CLASS_OBJECT_REVISION
TEST.SCRIPT_FEATURE:MULTIPLE_UUT_SUPPORT
TEST.SCRIPT_FEATURE:MIXED_CASE_NAMES
TEST.SCRIPT_FEATURE:STATIC_HEADER_FUNCS_IN_UUTS
--

-- Subprogram: <<INIT>>

-- Test Case: <<INIT>>.001
TEST.SUBPROGRAM:<<INIT>>
TEST.NEW
TEST.NAME:<<INIT>>.001
TEST.END

-- Unit: features_interface

-- Subprogram: gfFI_FeaturesConfigured

-- Test Case: gfFI_FeaturesConfigured.001
TEST.UNIT:features_interface
TEST.SUBPROGRAM:gfFI_FeaturesConfigured
TEST.NEW
TEST.NAME:gfFI_FeaturesConfigured.001
TEST.VALUE:features_interface.gfFI_FeaturesConfigured.return:1
TEST.END

-- Unit: supv_interface

-- Subprogram: fSUPV_CheckHeartbeatTimerActive

-- Test Case: fSUPV_CheckHeartbeatTimerActive.001
TEST.UNIT:supv_interface
TEST.SUBPROGRAM:fSUPV_CheckHeartbeatTimerActive
TEST.NEW
TEST.NAME:fSUPV_CheckHeartbeatTimerActive.001
TEST.END

-- Subprogram: fSUPV_ClearOutputDrivers

-- Test Case: BASIS-PATH-001-TEMPLATE
TEST.UNIT:supv_interface
TEST.SUBPROGRAM:fSUPV_ClearOutputDrivers
TEST.NEW
TEST.NAME:BASIS-PATH-001-TEMPLATE
TEST.BASIS_PATH:1 of 5 (template)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (1) for (uwDriver < 12U) ==> FALSE
   Test Case Generation Notes:
      Conflict: Unable to validate expression-to-expression comparison in branch 1
TEST.END_NOTES:
TEST.END

-- Test Case: BASIS-PATH-002-PARTIAL
TEST.UNIT:supv_interface
TEST.SUBPROGRAM:fSUPV_ClearOutputDrivers
TEST.NEW
TEST.NAME:BASIS-PATH-002-PARTIAL
TEST.BASIS_PATH:2 of 5 (partial)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 2
      (1) for (uwDriver < 12U) ==> TRUE
      (2) if (1U == (((gSupv_Data.IO_Interface).GP_Interface)[uwDriver]).fClear) ==> FALSE
   Test Case Generation Notes:
      Conflict: Unable to validate expression-to-expression comparison in branch 1
      Cannot set uwDriver due to assignment
TEST.END_NOTES:
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[0].fClear:<<MIN>>
TEST.END

-- Test Case: BASIS-PATH-003-PARTIAL
TEST.UNIT:supv_interface
TEST.SUBPROGRAM:fSUPV_ClearOutputDrivers
TEST.NEW
TEST.NAME:BASIS-PATH-003-PARTIAL
TEST.BASIS_PATH:3 of 5 (partial)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 3
      (1) for (uwDriver < 12U) ==> TRUE
      (2) if (1U == (((gSupv_Data.IO_Interface).GP_Interface)[uwDriver]).fClear) ==> TRUE
      (3) if (1U == gfMSCOMM_LookUpAddress(&((((gSupv_Data.IO_Interface).GP_Interface)[uwDriver]).fClear), &uwIndex, &uwSubindex)) ==> FALSE
   Test Case Generation Notes:
      Conflict: Unable to validate expression-to-expression comparison in branch 1
      Cannot set variable to out of range value 1 in branch 2
      Cannot set uwDriver due to assignment
TEST.END_NOTES:
TEST.VALUE:uut_prototype_stubs.gfMSCOMM_LookUpAddress.return:<<MIN>>
TEST.END

-- Test Case: BASIS-PATH-004-PARTIAL
TEST.UNIT:supv_interface
TEST.SUBPROGRAM:fSUPV_ClearOutputDrivers
TEST.NEW
TEST.NAME:BASIS-PATH-004-PARTIAL
TEST.BASIS_PATH:4 of 5 (partial)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 4
      (1) for (uwDriver < 12U) ==> TRUE
      (2) if (1U == (((gSupv_Data.IO_Interface).GP_Interface)[uwDriver]).fClear) ==> TRUE
      (3) if (1U == gfMSCOMM_LookUpAddress(&((((gSupv_Data.IO_Interface).GP_Interface)[uwDriver]).fClear), &uwIndex, &uwSubindex)) ==> TRUE
      (4) if (0U == fSUPV_SendConfigurationObject(1U, uwIndex, uwSubindex)) ==> FALSE
   Test Case Generation Notes:
      Conflict: Unable to validate expression-to-expression comparison in branch 1
      Cannot set variable to out of range value 1 in branch 2
      Cannot set uwDriver due to assignment
      Cannot set variable to out of range value 1 in branch 3
TEST.END_NOTES:
TEST.STUB:supv_interface.fSUPV_SendConfigurationObject
TEST.VALUE:supv_interface.fSUPV_SendConfigurationObject.return:<<MIN>>
TEST.END

-- Test Case: BASIS-PATH-005-PARTIAL
TEST.UNIT:supv_interface
TEST.SUBPROGRAM:fSUPV_ClearOutputDrivers
TEST.NEW
TEST.NAME:BASIS-PATH-005-PARTIAL
TEST.BASIS_PATH:5 of 5 (partial)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 5
      (1) for (uwDriver < 12U) ==> TRUE
      (2) if (1U == (((gSupv_Data.IO_Interface).GP_Interface)[uwDriver]).fClear) ==> TRUE
      (3) if (1U == gfMSCOMM_LookUpAddress(&((((gSupv_Data.IO_Interface).GP_Interface)[uwDriver]).fClear), &uwIndex, &uwSubindex)) ==> TRUE
      (4) if (0U == fSUPV_SendConfigurationObject(1U, uwIndex, uwSubindex)) ==> TRUE
   Test Case Generation Notes:
      Conflict: Unable to validate expression-to-expression comparison in branch 1
      Cannot set variable to out of range value 1 in branch 2
      Cannot set uwDriver due to assignment
      Cannot set variable to out of range value 1 in branch 3
TEST.END_NOTES:
TEST.STUB:supv_interface.fSUPV_SendConfigurationObject
TEST.VALUE:supv_interface.fSUPV_SendConfigurationObject.return:0
TEST.END

-- Test Case: TRUE_==_gSupv_Data.IO_Interface.GP_Interface[uwDriver].fClear
TEST.UNIT:supv_interface
TEST.SUBPROGRAM:fSUPV_ClearOutputDrivers
TEST.NEW
TEST.NAME:TRUE_==_gSupv_Data.IO_Interface.GP_Interface[uwDriver].fClear
TEST.BASIS_PATH:2 of 5 (partial)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 2
      (1) for (uwDriver < 12U) ==> TRUE
      (2) if (1U == (((gSupv_Data.IO_Interface).GP_Interface)[uwDriver]).fClear) ==> FALSE
   Test Case Generation Notes:
      Conflict: Unable to validate expression-to-expression comparison in branch 1
      Cannot set uwDriver due to assignment
TEST.END_NOTES:
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.IO_Interface.GP_Interface[0].fClear:1
TEST.END

-- Subprogram: fSUPV_HeartbeatReceived

-- Test Case: BASIS-PATH-001
TEST.UNIT:supv_interface
TEST.SUBPROGRAM:fSUPV_HeartbeatReceived
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 3
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (3) case ulStatus ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:uut_prototype_stubs.gulOS_SemaphoreGet.return:<<MIN>>
TEST.END

-- Test Case: BASIS-PATH-002
TEST.UNIT:supv_interface
TEST.SUBPROGRAM:fSUPV_HeartbeatReceived
TEST.NEW
TEST.NAME:BASIS-PATH-002
TEST.BASIS_PATH:2 of 3
TEST.NOTES:
This is an automatically generated test case.
   Test Path 2
      (1) case ulStatus ==> (uint32_t)((UINT)0)
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:uut_prototype_stubs.gulOS_SemaphoreGet.return:0
TEST.END

-- Test Case: BASIS-PATH-003
TEST.UNIT:supv_interface
TEST.SUBPROGRAM:fSUPV_HeartbeatReceived
TEST.NEW
TEST.NAME:BASIS-PATH-003
TEST.BASIS_PATH:3 of 3
TEST.NOTES:
This is an automatically generated test case.
   Test Path 3
      (2) case ulStatus ==> (uint32_t)((UINT)0xd)
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:uut_prototype_stubs.gulOS_SemaphoreGet.return:13
TEST.END

-- Subprogram: fSUPV_NvRead

-- Test Case: fSUPV_NvRead.001
TEST.UNIT:supv_interface
TEST.SUBPROGRAM:fSUPV_NvRead
TEST.NEW
TEST.NAME:fSUPV_NvRead.001
TEST.VALUE:supv_interface.fSUPV_NvRead.pConfig:<<malloc 1>>
TEST.VALUE:uut_prototype_stubs.gfNvMemory_ReadBuffer.pubBuffer:"0xab91u"
TEST.END

-- Subprogram: fSUPV_NvWrite

-- Test Case: BASIS-PATH-001
TEST.UNIT:supv_interface
TEST.SUBPROGRAM:fSUPV_NvWrite
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 2
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (1) if (1U == fUseNvMemory) ==> FALSE
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:supv_interface.fSUPV_NvWrite.pConfig:<<malloc 1>>
TEST.VALUE:supv_interface.fSUPV_NvWrite.fUseNvMemory:<<MIN>>
TEST.END

-- Test Case: BASIS-PATH-002-PARTIAL
TEST.UNIT:supv_interface
TEST.SUBPROGRAM:fSUPV_NvWrite
TEST.NEW
TEST.NAME:BASIS-PATH-002-PARTIAL
TEST.BASIS_PATH:2 of 2 (partial)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 2
      (1) if (1U == fUseNvMemory) ==> TRUE
   Test Case Generation Notes:
      Cannot set variable to out of range value 1 in branch 1
TEST.END_NOTES:
TEST.VALUE:supv_interface.fSUPV_NvWrite.pConfig:<<malloc 1>>
TEST.VALUE:supv_interface.fSUPV_NvWrite.fUseNvMemory:<<MIN>>
TEST.END

-- Subprogram: fSUPV_ReadCompatibility

-- Test Case: BASIS-PATH-001-TEMPLATE
TEST.UNIT:supv_interface
TEST.SUBPROGRAM:fSUPV_ReadCompatibility
TEST.NEW
TEST.NAME:BASIS-PATH-001-TEMPLATE
TEST.BASIS_PATH:1 of 5 (template)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (1) while (0U == fDone) ==> FALSE
   Test Case Generation Notes:
      Cannot set local variable fDone in branch 1
TEST.END_NOTES:
TEST.END

-- Test Case: BASIS-PATH-002-PARTIAL
TEST.UNIT:supv_interface
TEST.SUBPROGRAM:fSUPV_ReadCompatibility
TEST.NEW
TEST.NAME:BASIS-PATH-002-PARTIAL
TEST.BASIS_PATH:2 of 5 (partial)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 2
      (1) while (0U == fDone) ==> TRUE
      (2) if (1U == fSUPV_SendConfigurationObject(0U, ((supv_compat_obj_list)[ubObj]).uwIndex, ((supv_compat_obj_list)[ubObj]).uwSubindex)) ==> FALSE
      (4) if (ubRetries > (gParameters.SUPV_comm_parameters).ulConfigTransferRetries) ==> FALSE
   Test Case Generation Notes:
      Cannot set local variable fDone in branch 1
      Cannot set ubRetries due to assignment
TEST.END_NOTES:
TEST.STUB:supv_interface.fSUPV_SendConfigurationObject
TEST.VALUE:supv_interface.fSUPV_SendConfigurationObject.return:<<MIN>>
TEST.END

-- Test Case: BASIS-PATH-003-PARTIAL
TEST.UNIT:supv_interface
TEST.SUBPROGRAM:fSUPV_ReadCompatibility
TEST.NEW
TEST.NAME:BASIS-PATH-003-PARTIAL
TEST.BASIS_PATH:3 of 5 (partial)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 3
      (1) while (0U == fDone) ==> TRUE
      (2) if (1U == fSUPV_SendConfigurationObject(0U, ((supv_compat_obj_list)[ubObj]).uwIndex, ((supv_compat_obj_list)[ubObj]).uwSubindex)) ==> FALSE
      (4) if (ubRetries > (gParameters.SUPV_comm_parameters).ulConfigTransferRetries) ==> TRUE
   Test Case Generation Notes:
      Cannot set local variable fDone in branch 1
      Cannot set ubRetries due to assignment
TEST.END_NOTES:
TEST.STUB:supv_interface.fSUPV_SendConfigurationObject
TEST.VALUE:supv_interface.fSUPV_SendConfigurationObject.return:<<MIN>>
TEST.END

-- Test Case: BASIS-PATH-004-PARTIAL
TEST.UNIT:supv_interface
TEST.SUBPROGRAM:fSUPV_ReadCompatibility
TEST.NEW
TEST.NAME:BASIS-PATH-004-PARTIAL
TEST.BASIS_PATH:4 of 5 (partial)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 4
      (1) while (0U == fDone) ==> TRUE
      (2) if (1U == fSUPV_SendConfigurationObject(0U, ((supv_compat_obj_list)[ubObj]).uwIndex, ((supv_compat_obj_list)[ubObj]).uwSubindex)) ==> TRUE
      (3) if (ubObj >= uwSVCM_compat_message_size) ==> FALSE
   Test Case Generation Notes:
      Cannot set local variable fDone in branch 1
      Cannot set variable to out of range value 1 in branch 2
TEST.END_NOTES:
TEST.STUB:supv_interface.fSUPV_SendConfigurationObject
TEST.VALUE:supv_interface.<<GLOBAL>>.uwSVCM_compat_message_size:<<MAX>>
TEST.END

-- Test Case: BASIS-PATH-005-PARTIAL
TEST.UNIT:supv_interface
TEST.SUBPROGRAM:fSUPV_ReadCompatibility
TEST.NEW
TEST.NAME:BASIS-PATH-005-PARTIAL
TEST.BASIS_PATH:5 of 5 (partial)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 5
      (1) while (0U == fDone) ==> TRUE
      (2) if (1U == fSUPV_SendConfigurationObject(0U, ((supv_compat_obj_list)[ubObj]).uwIndex, ((supv_compat_obj_list)[ubObj]).uwSubindex)) ==> TRUE
      (3) if (ubObj >= uwSVCM_compat_message_size) ==> TRUE
   Test Case Generation Notes:
      Cannot set local variable fDone in branch 1
      Cannot set variable to out of range value 1 in branch 2
TEST.END_NOTES:
TEST.STUB:supv_interface.fSUPV_SendConfigurationObject
TEST.VALUE:supv_interface.<<GLOBAL>>.uwSVCM_compat_message_size:<<MIN>>
TEST.END

-- Subprogram: fSUPV_SendConfigurationObject

-- Test Case: BASIS-PATH-001
TEST.UNIT:supv_interface
TEST.SUBPROGRAM:fSUPV_SendConfigurationObject
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 4
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (1) if (1U == fWrite) ==> FALSE
      (2) if ((uint32_t)((UINT)0) == ulStatus) ==> FALSE
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:supv_interface.fSUPV_SendConfigurationObject.fWrite:<<MIN>>
TEST.VALUE:supv_interface.fSUPV_SendConfigurationObject.uwIndex:<<MIN>>
TEST.VALUE:supv_interface.fSUPV_SendConfigurationObject.uwSubindex:<<MIN>>
TEST.VALUE:uut_prototype_stubs.gulMSCOMM_SendMessage.return:<<MIN>>
TEST.END

-- Test Case: BASIS-PATH-002
TEST.UNIT:supv_interface
TEST.SUBPROGRAM:fSUPV_SendConfigurationObject
TEST.NEW
TEST.NAME:BASIS-PATH-002
TEST.BASIS_PATH:2 of 4
TEST.NOTES:
This is an automatically generated test case.
   Test Path 2
      (1) if (1U == fWrite) ==> FALSE
      (2) if ((uint32_t)((UINT)0) == ulStatus) ==> TRUE
      (3) if ((uint32_t)((UINT)0) == ulStatus) ==> FALSE
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:supv_interface.fSUPV_SendConfigurationObject.fWrite:<<MIN>>
TEST.VALUE:supv_interface.fSUPV_SendConfigurationObject.uwIndex:<<MIN>>
TEST.VALUE:supv_interface.fSUPV_SendConfigurationObject.uwSubindex:<<MIN>>
TEST.VALUE:uut_prototype_stubs.gulOS_SemaphoreGet.return:<<MIN>>
TEST.VALUE:uut_prototype_stubs.gulMSCOMM_SendMessage.return:0
TEST.END

-- Test Case: BASIS-PATH-003
TEST.UNIT:supv_interface
TEST.SUBPROGRAM:fSUPV_SendConfigurationObject
TEST.NEW
TEST.NAME:BASIS-PATH-003
TEST.BASIS_PATH:3 of 4
TEST.NOTES:
This is an automatically generated test case.
   Test Path 3
      (1) if (1U == fWrite) ==> FALSE
      (2) if ((uint32_t)((UINT)0) == ulStatus) ==> TRUE
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:supv_interface.fSUPV_SendConfigurationObject.fWrite:<<MIN>>
TEST.VALUE:supv_interface.fSUPV_SendConfigurationObject.uwIndex:<<MIN>>
TEST.VALUE:supv_interface.fSUPV_SendConfigurationObject.uwSubindex:<<MIN>>
TEST.VALUE:uut_prototype_stubs.gulOS_SemaphoreGet.return:0
TEST.VALUE:uut_prototype_stubs.gulMSCOMM_SendMessage.return:0
TEST.END

-- Test Case: BASIS-PATH-004-PARTIAL
TEST.UNIT:supv_interface
TEST.SUBPROGRAM:fSUPV_SendConfigurationObject
TEST.NEW
TEST.NAME:BASIS-PATH-004-PARTIAL
TEST.BASIS_PATH:4 of 4 (partial)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 4
      (1) if (1U == fWrite) ==> TRUE
      (2) if ((uint32_t)((UINT)0) == ulStatus) ==> FALSE
   Test Case Generation Notes:
      Cannot set variable to out of range value 1 in branch 1
TEST.END_NOTES:
TEST.VALUE:supv_interface.fSUPV_SendConfigurationObject.fWrite:<<MIN>>
TEST.VALUE:supv_interface.fSUPV_SendConfigurationObject.uwIndex:<<MIN>>
TEST.VALUE:supv_interface.fSUPV_SendConfigurationObject.uwSubindex:<<MIN>>
TEST.VALUE:uut_prototype_stubs.gulMSCOMM_SendMessage.return:<<MIN>>
TEST.END

-- Subprogram: fSUPV_XferConfiguration

-- Test Case: BASIS-PATH-001-PARTIAL
TEST.UNIT:supv_interface
TEST.SUBPROGRAM:fSUPV_XferConfiguration
TEST.NEW
TEST.NAME:BASIS-PATH-001-PARTIAL
TEST.BASIS_PATH:1 of 6 (partial)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (1) while (0U == fDone) ==> FALSE
   Test Case Generation Notes:
      Cannot set local variable fDone in branch 1
TEST.END_NOTES:
TEST.VALUE:supv_interface.fSUPV_XferConfiguration.fWrite:<<MIN>>
TEST.VALUE:supv_interface.fSUPV_XferConfiguration.pConfig:<<malloc 1>>
TEST.END

-- Test Case: BASIS-PATH-002-PARTIAL
TEST.UNIT:supv_interface
TEST.SUBPROGRAM:fSUPV_XferConfiguration
TEST.NEW
TEST.NAME:BASIS-PATH-002-PARTIAL
TEST.BASIS_PATH:2 of 6 (partial)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 2
      (1) while (0U == fDone) ==> TRUE
      (2) if (1U == fSuccess) ==> FALSE
      (5) if (ubRetries > (gParameters.SUPV_comm_parameters).ulConfigTransferRetries) ==> FALSE
   Test Case Generation Notes:
      Cannot set local variable fDone in branch 1
      Cannot set ubRetries due to assignment
TEST.END_NOTES:
TEST.VALUE:supv_interface.fSUPV_XferConfiguration.fWrite:<<MIN>>
TEST.VALUE:supv_interface.fSUPV_XferConfiguration.pConfig:<<malloc 1>>
TEST.VALUE:supv_interface.fSUPV_SendConfigurationObject.return:<<MIN>>
TEST.END

-- Test Case: BASIS-PATH-003-PARTIAL
TEST.UNIT:supv_interface
TEST.SUBPROGRAM:fSUPV_XferConfiguration
TEST.NEW
TEST.NAME:BASIS-PATH-003-PARTIAL
TEST.BASIS_PATH:3 of 6 (partial)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 3
      (1) while (0U == fDone) ==> TRUE
      (2) if (1U == fSuccess) ==> FALSE
      (5) if (ubRetries > (gParameters.SUPV_comm_parameters).ulConfigTransferRetries) ==> TRUE
   Test Case Generation Notes:
      Cannot set local variable fDone in branch 1
      Cannot set ubRetries due to assignment
TEST.END_NOTES:
TEST.VALUE:supv_interface.fSUPV_XferConfiguration.fWrite:<<MIN>>
TEST.VALUE:supv_interface.fSUPV_XferConfiguration.pConfig:<<malloc 1>>
TEST.VALUE:supv_interface.fSUPV_SendConfigurationObject.return:<<MIN>>
TEST.END

-- Test Case: BASIS-PATH-004-PARTIAL
TEST.UNIT:supv_interface
TEST.SUBPROGRAM:fSUPV_XferConfiguration
TEST.NEW
TEST.NAME:BASIS-PATH-004-PARTIAL
TEST.BASIS_PATH:4 of 6 (partial)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 4
      (1) while (0U == fDone) ==> TRUE
      (2) if (1U == fSuccess) ==> TRUE
      (3) if (uwSC_Subindex > (((gParameters.SUPV_io_xfer_list).SUPV_config_objects)[uwObjectListIndex]).ulSubindexLast) ==> FALSE
   Test Case Generation Notes:
      Cannot set local variable fDone in branch 1
      Cannot set variable to out of range value 1 in branch 2
      Cannot set uwSC_Subindex due to assignment
TEST.END_NOTES:
TEST.VALUE:supv_interface.fSUPV_XferConfiguration.fWrite:<<MIN>>
TEST.VALUE:supv_interface.fSUPV_XferConfiguration.pConfig:<<malloc 1>>
TEST.END

-- Test Case: BASIS-PATH-005-PARTIAL
TEST.UNIT:supv_interface
TEST.SUBPROGRAM:fSUPV_XferConfiguration
TEST.NEW
TEST.NAME:BASIS-PATH-005-PARTIAL
TEST.BASIS_PATH:5 of 6 (partial)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 5
      (1) while (0U == fDone) ==> TRUE
      (2) if (1U == fSuccess) ==> TRUE
      (3) if (uwSC_Subindex > (((gParameters.SUPV_io_xfer_list).SUPV_config_objects)[uwObjectListIndex]).ulSubindexLast) ==> TRUE
      (4) if (uwObjectListIndex < (gParameters.SUPV_io_xfer_list).uwNumberOfEntries) ==> FALSE
   Test Case Generation Notes:
      Cannot set local variable fDone in branch 1
      Cannot set variable to out of range value 1 in branch 2
      Cannot set uwSC_Subindex due to assignment
      Cannot set uwObjectListIndex due to assignment
TEST.END_NOTES:
TEST.VALUE:supv_interface.fSUPV_XferConfiguration.fWrite:<<MIN>>
TEST.VALUE:supv_interface.fSUPV_XferConfiguration.pConfig:<<malloc 1>>
TEST.END

-- Test Case: BASIS-PATH-006-PARTIAL
TEST.UNIT:supv_interface
TEST.SUBPROGRAM:fSUPV_XferConfiguration
TEST.NEW
TEST.NAME:BASIS-PATH-006-PARTIAL
TEST.BASIS_PATH:6 of 6 (partial)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 6
      (1) while (0U == fDone) ==> TRUE
      (2) if (1U == fSuccess) ==> TRUE
      (3) if (uwSC_Subindex > (((gParameters.SUPV_io_xfer_list).SUPV_config_objects)[uwObjectListIndex]).ulSubindexLast) ==> TRUE
      (4) if (uwObjectListIndex < (gParameters.SUPV_io_xfer_list).uwNumberOfEntries) ==> TRUE
   Test Case Generation Notes:
      Cannot set local variable fDone in branch 1
      Cannot set variable to out of range value 1 in branch 2
      Cannot set uwSC_Subindex due to assignment
      Cannot set uwObjectListIndex due to assignment
TEST.END_NOTES:
TEST.VALUE:supv_interface.fSUPV_XferConfiguration.fWrite:<<MIN>>
TEST.VALUE:supv_interface.fSUPV_XferConfiguration.pConfig:<<malloc 1>>
TEST.END

-- Subprogram: gfGetPSP2CfgReceived

-- Test Case: BASIS-PATH-001
TEST.UNIT:supv_interface
TEST.SUBPROGRAM:gfGetPSP2CfgReceived
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 1
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.END

-- Subprogram: gfSUPV_CommunicationEstablished

-- Test Case: BASIS-PATH-001
TEST.UNIT:supv_interface
TEST.SUBPROGRAM:gfSUPV_CommunicationEstablished
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 2
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (1) if ((gSupv_Data.COMM_Data).eCOMM_State == (SUPV_OPERATIONAL)) ==> FALSE
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.COMM_Data.eCOMM_State:SUPV_INIT
TEST.END

-- Test Case: BASIS-PATH-002
TEST.UNIT:supv_interface
TEST.SUBPROGRAM:gfSUPV_CommunicationEstablished
TEST.NEW
TEST.NAME:BASIS-PATH-002
TEST.BASIS_PATH:2 of 2
TEST.NOTES:
This is an automatically generated test case.
   Test Path 2
      (1) if ((gSupv_Data.COMM_Data).eCOMM_State == (SUPV_OPERATIONAL)) ==> TRUE
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.COMM_Data.eCOMM_State:SUPV_OPERATIONAL
TEST.END

-- Subprogram: gfSUPV_Initialize

-- Test Case: gfSUPV_Initialize.001
TEST.UNIT:supv_interface
TEST.SUBPROGRAM:gfSUPV_Initialize
TEST.NEW
TEST.NAME:gfSUPV_Initialize.001
TEST.END

-- Subprogram: gpSUPV_GetCommThreadPointer

-- Test Case: BASIS-PATH-001
TEST.UNIT:supv_interface
TEST.SUBPROGRAM:gpSUPV_GetCommThreadPointer
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 1
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.END

-- Subprogram: gpSUPV_GetConfigThreadPointer

-- Test Case: BASIS-PATH-001
TEST.UNIT:supv_interface
TEST.SUBPROGRAM:gpSUPV_GetConfigThreadPointer
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 1
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.END

-- Subprogram: gvSUPV_IvhmDataSend

-- Test Case: BASIS-PATH-001-PARTIAL
TEST.UNIT:supv_interface
TEST.SUBPROGRAM:gvSUPV_IvhmDataSend
TEST.NEW
TEST.NAME:BASIS-PATH-001-PARTIAL
TEST.BASIS_PATH:1 of 2 (partial)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (1) if (ulIvhmCounterMs >= 50U) ==> FALSE
   Test Case Generation Notes:
      Cannot set ulIvhmCounterMs due to assignment
TEST.END_NOTES:
TEST.VALUE:supv_interface.gvSUPV_IvhmDataSend.ulCallRateMs:<<MIN>>
TEST.END

-- Test Case: BASIS-PATH-002-PARTIAL
TEST.UNIT:supv_interface
TEST.SUBPROGRAM:gvSUPV_IvhmDataSend
TEST.NEW
TEST.NAME:BASIS-PATH-002-PARTIAL
TEST.BASIS_PATH:2 of 2 (partial)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 2
      (1) if (ulIvhmCounterMs >= 50U) ==> TRUE
   Test Case Generation Notes:
      Cannot set ulIvhmCounterMs due to assignment
TEST.END_NOTES:
TEST.VALUE:supv_interface.gvSUPV_IvhmDataSend.ulCallRateMs:51
TEST.END

-- Subprogram: gvSUPV_Psp1CfgSend

-- Test Case: BASIS-PATH-001
TEST.UNIT:supv_interface
TEST.SUBPROGRAM:gvSUPV_Psp1CfgSend
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 1
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.END

-- Subprogram: gvSUPV_SteeringSend

-- Test Case: BASIS-PATH-001-PARTIAL
TEST.UNIT:supv_interface
TEST.SUBPROGRAM:gvSUPV_SteeringSend
TEST.NEW
TEST.NAME:BASIS-PATH-001-PARTIAL
TEST.BASIS_PATH:1 of 2 (partial)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (1) if (ulSteerCounterMs >= 10U) ==> FALSE
   Test Case Generation Notes:
      Cannot set ulSteerCounterMs due to assignment
TEST.END_NOTES:
TEST.VALUE:supv_interface.gvSUPV_SteeringSend.ulCallRateMs:<<MIN>>
TEST.END

-- Test Case: BASIS-PATH-002-PARTIAL
TEST.UNIT:supv_interface
TEST.SUBPROGRAM:gvSUPV_SteeringSend
TEST.NEW
TEST.NAME:BASIS-PATH-002-PARTIAL
TEST.BASIS_PATH:2 of 2 (partial)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 2
      (1) if (ulSteerCounterMs >= 10U) ==> TRUE
   Test Case Generation Notes:
      Cannot set ulSteerCounterMs due to assignment
TEST.END_NOTES:
TEST.VALUE:supv_interface.gvSUPV_SteeringSend.ulCallRateMs:11
TEST.END

-- Subprogram: svSUPV_Event_cb

-- Test Case: BASIS-PATH-001
TEST.UNIT:supv_interface
TEST.SUBPROGRAM:svSUPV_Event_cb
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 1
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.VALUE:supv_interface.svSUPV_Event_cb.uwMessageID:<<MIN>>
TEST.END

-- Subprogram: svSUPV_RxPsp2Cfg

-- Test Case: BASIS-PATH-001
TEST.UNIT:supv_interface
TEST.SUBPROGRAM:svSUPV_RxPsp2Cfg
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 1
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.VALUE:supv_interface.svSUPV_RxPsp2Cfg.uwMessageID:<<MIN>>
TEST.END

-- Subprogram: svSUPV_rx_driver_status_cb

-- Test Case: BASIS-PATH-001-PARTIAL
TEST.UNIT:supv_interface
TEST.SUBPROGRAM:svSUPV_rx_driver_status_cb
TEST.NEW
TEST.NAME:BASIS-PATH-001-PARTIAL
TEST.BASIS_PATH:1 of 2 (partial)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (1) for (ubIndex < 12U) ==> FALSE
   Test Case Generation Notes:
      Conflict: Unable to validate expression-to-expression comparison in branch 1
TEST.END_NOTES:
TEST.VALUE:supv_interface.svSUPV_rx_driver_status_cb.uwMessageID:<<MIN>>
TEST.END

-- Test Case: BASIS-PATH-002-PARTIAL
TEST.UNIT:supv_interface
TEST.SUBPROGRAM:svSUPV_rx_driver_status_cb
TEST.NEW
TEST.NAME:BASIS-PATH-002-PARTIAL
TEST.BASIS_PATH:2 of 2 (partial)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 2
      (1) for (ubIndex < 12U) ==> TRUE
   Test Case Generation Notes:
      Conflict: Unable to validate expression-to-expression comparison in branch 1
TEST.END_NOTES:
TEST.VALUE:supv_interface.svSUPV_rx_driver_status_cb.uwMessageID:<<MIN>>
TEST.END

-- Subprogram: vSUPV_AgExec_cb

-- Test Case: BASIS-PATH-001
TEST.UNIT:supv_interface
TEST.SUBPROGRAM:vSUPV_AgExec_cb
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 2
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (1) if (1U == xfSupvModelThreadClock) ==> FALSE
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:supv_interface.<<GLOBAL>>.xfSupvModelThreadClock:<<MIN>>
TEST.VALUE:supv_interface.vSUPV_AgExec_cb.uwMessageID:<<MIN>>
TEST.END

-- Test Case: BASIS-PATH-002-PARTIAL
TEST.UNIT:supv_interface
TEST.SUBPROGRAM:vSUPV_AgExec_cb
TEST.NEW
TEST.NAME:BASIS-PATH-002-PARTIAL
TEST.BASIS_PATH:2 of 2 (partial)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 2
      (1) if (1U == xfSupvModelThreadClock) ==> TRUE
   Test Case Generation Notes:
      Cannot set variable to out of range value 1 in branch 1
TEST.END_NOTES:
TEST.VALUE:supv_interface.vSUPV_AgExec_cb.uwMessageID:<<MIN>>
TEST.END

-- Subprogram: vSUPV_CfgHandshake_cb

-- Test Case: vSUPV_CfgHandshake_cb.001
TEST.UNIT:supv_interface
TEST.SUBPROGRAM:vSUPV_CfgHandshake_cb
TEST.NEW
TEST.NAME:vSUPV_CfgHandshake_cb.001
TEST.END

-- Subprogram: vSUPV_CheckHeartbeatCounter

-- Test Case: BASIS-PATH-001-TEMPLATE
TEST.UNIT:supv_interface
TEST.SUBPROGRAM:vSUPV_CheckHeartbeatCounter
TEST.NEW
TEST.NAME:BASIS-PATH-001-TEMPLATE
TEST.BASIS_PATH:1 of 3 (template)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (1) if (ubPrevious_cnt != uwCounter) ==> FALSE
      (2) if (ulConcernTimeMs >= (gParameters.SUPV_comm_parameters).ulHeatbeatNotCountingTimeoutMs) ==> FALSE
   Test Case Generation Notes:
      Cannot set local variable ubPrevious_cnt in branch 1
      Cannot set ulConcernTimeMs due to assignment
TEST.END_NOTES:
TEST.END

-- Test Case: BASIS-PATH-002-TEMPLATE
TEST.UNIT:supv_interface
TEST.SUBPROGRAM:vSUPV_CheckHeartbeatCounter
TEST.NEW
TEST.NAME:BASIS-PATH-002-TEMPLATE
TEST.BASIS_PATH:2 of 3 (template)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 2
      (1) if (ubPrevious_cnt != uwCounter) ==> FALSE
      (2) if (ulConcernTimeMs >= (gParameters.SUPV_comm_parameters).ulHeatbeatNotCountingTimeoutMs) ==> TRUE
   Test Case Generation Notes:
      Cannot set local variable ubPrevious_cnt in branch 1
      Cannot set ulConcernTimeMs due to assignment
TEST.END_NOTES:
TEST.END

-- Test Case: BASIS-PATH-003-TEMPLATE
TEST.UNIT:supv_interface
TEST.SUBPROGRAM:vSUPV_CheckHeartbeatCounter
TEST.NEW
TEST.NAME:BASIS-PATH-003-TEMPLATE
TEST.BASIS_PATH:3 of 3 (template)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 3
      (1) if (ubPrevious_cnt != uwCounter) ==> TRUE
   Test Case Generation Notes:
      Cannot set local variable ubPrevious_cnt in branch 1
TEST.END_NOTES:
TEST.END

-- Subprogram: vSUPV_DEMControlLimitsSend

-- Test Case: vSUPV_DEMControlLimitsSend.001
TEST.UNIT:supv_interface
TEST.SUBPROGRAM:vSUPV_DEMControlLimitsSend
TEST.NEW
TEST.NAME:vSUPV_DEMControlLimitsSend.001
TEST.VALUE:supv_interface.<<GLOBAL>>.pDEM_ControlLimits:<<malloc 1>>
TEST.VALUE:supv_interface.<<GLOBAL>>.pDEM_ControlLimits[0].ulED_Allowed:1
TEST.END

-- Subprogram: vSUPV_HbeatMessage_cb

-- Test Case: BASIS-PATH-001
TEST.UNIT:supv_interface
TEST.SUBPROGRAM:vSUPV_HbeatMessage_cb
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 1
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.END

-- Subprogram: vSUPV_HeartbeatTimeoutEntry

-- Test Case: BASIS-PATH-001
TEST.UNIT:supv_interface
TEST.SUBPROGRAM:vSUPV_HeartbeatTimeoutEntry
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 3
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (3) case ((gSupv_Data.COMM_Data).eCOMM_State) ==> default
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.COMM_Data.eCOMM_State:SUPV_INIT
TEST.VALUE:supv_interface.vSUPV_HeartbeatTimeoutEntry.ulInput:<<MIN>>
TEST.END

-- Test Case: BASIS-PATH-002
TEST.UNIT:supv_interface
TEST.SUBPROGRAM:vSUPV_HeartbeatTimeoutEntry
TEST.NEW
TEST.NAME:BASIS-PATH-002
TEST.BASIS_PATH:2 of 3
TEST.NOTES:
This is an automatically generated test case.
   Test Path 2
      (1) case ((gSupv_Data.COMM_Data).eCOMM_State) ==> SUPV_WAIT_HB
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.COMM_Data.eCOMM_State:SUPV_WAIT_HB
TEST.VALUE:supv_interface.vSUPV_HeartbeatTimeoutEntry.ulInput:<<MIN>>
TEST.END

-- Test Case: BASIS-PATH-003
TEST.UNIT:supv_interface
TEST.SUBPROGRAM:vSUPV_HeartbeatTimeoutEntry
TEST.NEW
TEST.NAME:BASIS-PATH-003
TEST.BASIS_PATH:3 of 3
TEST.NOTES:
This is an automatically generated test case.
   Test Path 3
      (2) case ((gSupv_Data.COMM_Data).eCOMM_State) ==> SUPV_OPERATIONAL
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.COMM_Data.eCOMM_State:SUPV_OPERATIONAL
TEST.VALUE:supv_interface.vSUPV_HeartbeatTimeoutEntry.ulInput:<<MIN>>
TEST.END

-- Subprogram: vSUPV_HeartbeatTimerActivate

-- Test Case: vSUPV_HeartbeatTimerActivate.001
TEST.UNIT:supv_interface
TEST.SUBPROGRAM:vSUPV_HeartbeatTimerActivate
TEST.NEW
TEST.NAME:vSUPV_HeartbeatTimerActivate.001
TEST.VALUE:uut_prototype_stubs.gulOS_TimerChange.return:1
TEST.END

-- Subprogram: vSUPV_HeartbeatTimerChange

-- Test Case: vSUPV_HeartbeatTimerChange.001
TEST.UNIT:supv_interface
TEST.SUBPROGRAM:vSUPV_HeartbeatTimerChange
TEST.NEW
TEST.NAME:vSUPV_HeartbeatTimerChange.001
TEST.STUB:supv_interface.gfGetPSP2CfgReceived
TEST.VALUE:uut_prototype_stubs.gulOS_TimerChange.return:1
TEST.END

-- Subprogram: vSUPV_HeartbeatTimerReset

-- Test Case: vSUPV_HeartbeatTimerReset.001
TEST.UNIT:supv_interface
TEST.SUBPROGRAM:vSUPV_HeartbeatTimerReset
TEST.NEW
TEST.NAME:vSUPV_HeartbeatTimerReset.001
TEST.STUB:supv_interface.fSUPV_CheckHeartbeatTimerActive
TEST.VALUE:supv_interface.fSUPV_CheckHeartbeatTimerActive.return:1
TEST.END

-- Subprogram: vSUPV_PspCompSend

-- Test Case: BASIS-PATH-001-PARTIAL
TEST.UNIT:supv_interface
TEST.SUBPROGRAM:vSUPV_PspCompSend
TEST.NEW
TEST.NAME:BASIS-PATH-001-PARTIAL
TEST.BASIS_PATH:1 of 2 (partial)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (1) if (ulPspCounterMs >= 10U) ==> FALSE
   Test Case Generation Notes:
      Cannot set ulPspCounterMs due to assignment
TEST.END_NOTES:
TEST.VALUE:supv_interface.vSUPV_PspCompSend.ulCallRateMs:<<MIN>>
TEST.END

-- Test Case: BASIS-PATH-002-PARTIAL
TEST.UNIT:supv_interface
TEST.SUBPROGRAM:vSUPV_PspCompSend
TEST.NEW
TEST.NAME:BASIS-PATH-002-PARTIAL
TEST.BASIS_PATH:2 of 2 (partial)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 2
      (1) if (ulPspCounterMs >= 10U) ==> TRUE
   Test Case Generation Notes:
      Cannot set ulPspCounterMs due to assignment
TEST.END_NOTES:
TEST.VALUE:supv_interface.vSUPV_PspCompSend.ulCallRateMs:<<MIN>>
TEST.END

-- Subprogram: vSUPV_ThreadEntry

-- Test Case: 0x00_!=_gulOS_SemaphoreCreate()
TEST.UNIT:supv_interface
TEST.SUBPROGRAM:vSUPV_ThreadEntry
TEST.NEW
TEST.NAME:0x00_!=_gulOS_SemaphoreCreate()
TEST.VALUE:uut_prototype_stubs.gulOS_SemaphoreCreate.return:1
TEST.END

-- Test Case: 1U_==_gSupv_Data.COMM_Data.fTrig_ClearDrivers
TEST.UNIT:supv_interface
TEST.SUBPROGRAM:vSUPV_ThreadEntry
TEST.NEW
TEST.NAME:1U_==_gSupv_Data.COMM_Data.fTrig_ClearDrivers
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.COMM_Data.eCOMM_State:SUPV_OPERATIONAL
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.COMM_Data.fTrig_ClearDrivers:1
TEST.VALUE:supv_interface.<<GLOBAL>>.pDEM_ControlLimits:<<malloc 1>>
TEST.END

-- Test Case: 1U_==_gSupv_Data.COMM_Data.fTrig_ReadCompat
TEST.UNIT:supv_interface
TEST.SUBPROGRAM:vSUPV_ThreadEntry
TEST.NEW
TEST.NAME:1U_==_gSupv_Data.COMM_Data.fTrig_ReadCompat
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.COMM_Data.eCOMM_State:SUPV_OPERATIONAL
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.COMM_Data.fTrig_ReadCompat:1
TEST.VALUE:supv_interface.<<GLOBAL>>.pDEM_ControlLimits:<<malloc 1>>
TEST.END

-- Test Case: 1U_==_gSupv_Data.COMM_Data.fTrig_ReadConfig
TEST.UNIT:supv_interface
TEST.SUBPROGRAM:vSUPV_ThreadEntry
TEST.NEW
TEST.NAME:1U_==_gSupv_Data.COMM_Data.fTrig_ReadConfig
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.COMM_Data.eCOMM_State:SUPV_OPERATIONAL
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.COMM_Data.fTrig_ReadConfig:1
TEST.VALUE:supv_interface.<<GLOBAL>>.pDEM_ControlLimits:<<malloc 1>>
TEST.EXPECTED:supv_interface.<<GLOBAL>>.gSupv_Data.COMM_Data.eCOMM_State:SUPV_OPERATIONAL
TEST.END

-- Test Case: 1U_==_gfFI_FeaturesConfigured()
TEST.UNIT:supv_interface
TEST.SUBPROGRAM:vSUPV_ThreadEntry
TEST.NEW
TEST.NAME:1U_==_gfFI_FeaturesConfigured()
TEST.STUB:features_interface.gfFI_FeaturesConfigured
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.COMM_Data.eCOMM_State:SUPV_WAIT_FEATURES
TEST.VALUE:supv_interface.<<GLOBAL>>.pDEM_ControlLimits:<<malloc 1>>
TEST.VALUE:features_interface.gfFI_FeaturesConfigured.return:1
TEST.END

-- Test Case: 1U_==_gfMSCOMM_CheckOnline()_
TEST.UNIT:supv_interface
TEST.SUBPROGRAM:vSUPV_ThreadEntry
TEST.NEW
TEST.NAME:1U_==_gfMSCOMM_CheckOnline()_
TEST.VALUE:uut_prototype_stubs.gfMSCOMM_CheckOnline.return:1
TEST.END

-- Test Case: gSupv_Data.COMM_Data.fTrig_SaveConfig
TEST.UNIT:supv_interface
TEST.SUBPROGRAM:vSUPV_ThreadEntry
TEST.NEW
TEST.NAME:gSupv_Data.COMM_Data.fTrig_SaveConfig
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.COMM_Data.eCOMM_State:SUPV_OPERATIONAL
TEST.VALUE:supv_interface.<<GLOBAL>>.gSupv_Data.COMM_Data.fTrig_SaveConfig:1
TEST.VALUE:supv_interface.<<GLOBAL>>.pDEM_ControlLimits:<<malloc 1>>
TEST.END

-- Subprogram: vSUPV_TimerEntry

-- Test Case: BASIS-PATH-001
TEST.UNIT:supv_interface
TEST.SUBPROGRAM:vSUPV_TimerEntry
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 1
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.END
