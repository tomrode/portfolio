-- VectorCAST 6.4s (05/01/17)
-- Test Case Script
-- 
-- Environment    : STEERING_INTEGRATION
-- Unit(s) Under Test: AssistCmd Cmd_Pos_Integrator HmsSim_Steering HmsSim_SteeringBuild PosCmd PosCmd_data SpdCmd SteeringSIM SteeringSIMbuild StrMstrApp StrMstrSup StrMtrSpdLmt StrPosHndlSpdSense StrPosWhAngLimiter StrPos_SahNmbns StrPos_SetPntRateLmtr TfdCurrentSpProfile TfdOpMode TfdOvrMode Til2DuScaling TrxSpd_StrMtrSpdLmt WhAngErr_StrMtrSpdLmt WhAng_StrMtrSpdLmt
-- 
-- Script Features
TEST.SCRIPT_FEATURE:C_DIRECT_ARRAY_INDEXING
TEST.SCRIPT_FEATURE:CPP_CLASS_OBJECT_REVISION
TEST.SCRIPT_FEATURE:MULTIPLE_UUT_SUPPORT
TEST.SCRIPT_FEATURE:MIXED_CASE_NAMES
TEST.SCRIPT_FEATURE:STATIC_HEADER_FUNCS_IN_UUTS
--

-- Unit: AssistCmd

-- Subprogram: AssistCmd

-- Test Case: AssistCmd.001
TEST.UNIT:AssistCmd
TEST.SUBPROGRAM:AssistCmd
TEST.NEW
TEST.NAME:AssistCmd.001
TEST.COMPOUND_ONLY
TEST.STUB:AssistCmd.AssistCmd_initialize
TEST.VALUE:AssistCmd.<<GLOBAL>>.gpSteer_params:<<malloc 1>>
TEST.VALUE:AssistCmd.AssistCmd.rtu_AsstCmdIn:1.0
TEST.VALUE:AssistCmd.AssistCmd.rtu_DuInCwStop:0
TEST.VALUE:AssistCmd.AssistCmd.rtu_DuInCcwStop:0
TEST.VALUE:AssistCmd.AssistCmd.rtu_StrAllowedIn:3
TEST.VALUE:AssistCmd.AssistCmd.rtu_gpSensor_cals_c2m:<<malloc 1>>
TEST.VALUE:AssistCmd.AssistCmd.rtu_gpSensor_cals_c2m[0].rSecHgtSensorCalGain:0.0
TEST.VALUE:AssistCmd.AssistCmd.rtu_gpSensor_cals_c2m[0].rPriHgtSensorCalGain:1.0
TEST.VALUE:AssistCmd.AssistCmd.rtu_gpSensor_cals_c2m[0].rPrimaryCalCountAtMax:2.0
TEST.VALUE:AssistCmd.AssistCmd.rtu_gpSensor_cals_c2m[0].rAuxHtSensorCalGain:3.0
TEST.VALUE:AssistCmd.AssistCmd.rtu_gpSensor_cals_c2m[0].rTiltSensorLevel:8.0
TEST.VALUE:AssistCmd.AssistCmd.rtu_gpSensor_cals_c2m[0].rPivotSensorCalGain:7.0
TEST.VALUE:AssistCmd.AssistCmd.rtu_gpSensor_cals_c2m[0].rTraverseSensorCalGain:6.0
TEST.VALUE:AssistCmd.AssistCmd.rtu_gpSensor_cals_c2m[0].rLSOffset_Primary:4.0
TEST.VALUE:AssistCmd.AssistCmd.rtu_gpSensor_cals_c2m[0].rLSOffset_Secondary:4.0
TEST.VALUE:AssistCmd.AssistCmd.rtu_gpSensor_cals_c2m[0].rLSOffset_Aux_cyl:3.0
TEST.VALUE:AssistCmd.AssistCmd.rtu_gpSensor_cals_c2m[0].rAccelXoffset:1.0
TEST.VALUE:AssistCmd.AssistCmd.rtu_gpSensor_cals_c2m[0].rAccelYoffset:1.0
TEST.VALUE:AssistCmd.AssistCmd.rtu_gpSensor_cals_c2m[0].rAccelZoffset:1.0
TEST.VALUE:AssistCmd.AssistCmd.rtu_gpSensor_cals_c2m[0].rStrHndlCmd1[1]:1.0
TEST.VALUE:AssistCmd.AssistCmd.rtu_gpSensor_cals_c2m[0].slHndlAng1[0]:2
TEST.VALUE:AssistCmd.AssistCmd.rtu_gpSensor_cals_c2m[0].rStrHndlCmd2[0]:3.0
TEST.VALUE:AssistCmd.AssistCmd.rtu_gpSensor_cals_c2m[0].slHndlAng2[0]:0
TEST.VALUE:AssistCmd.AssistCmd.rtu_gpSensor_cals_c2m[0].rMainHeightMax:7.0
TEST.VALUE:AssistCmd.AssistCmd.rtu_gpSensor_cals_c2m[0].rCustomHeight:4.0
TEST.VALUE:AssistCmd.AssistCmd.rty_Asst_Out:<<malloc 1>>
TEST.VALUE:AssistCmd.AssistCmd.rty_Asst_Out[0]:1
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.StrAssistCmd_fCmdActive:1
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.StrAssistCmd_rStrAsstDiffVolts:0.0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.StrAssistCmd_rStrAsstClippedDiffVolts:0.0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.StrAssistCmd_slMilliVoltsCmdOut:0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.StrAsstEndStopLmts_fStrAsstDUInRange:1
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.StrAsstEndStopLmts_fStrAsstCmdSign:1
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.StrAsstEndStopLmts_fStrAsstInCwStopMovingOut:0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.StrAsstEndStopLmts_fStrAsstInCcwStopMovingOut:0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.StrAsstEndStopLmts_fStrAsstCmdOk:1
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.StrAsstEndStopLmts_fStrAsstCmdAllowed:1
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].eSensorType:ENCODER
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].eControlMode:ASSIST
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].ulFbEcr1Cnts:0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].ulFbEcr2Cnts:0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].eUseDirFeature:DO_NOT_USE
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].slSteerRotation:0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].eUseWalkieMode:DO_NOT_USE
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].eUseRabbitTurtleMode:DO_NOT_USE
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].ulEndStopZoneCW:0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].ulEndStopZoneCCW:0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].slAttTurtleWhAngLmt[0]:0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].slQPWhAngLmt[0]:0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].rTil2WhRatio:0.0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].ulSpdModeCntPerTickMax:0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].rSpdModeKp:0.0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].rSpdModeKi:0.0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].rSpdModeKd:0.0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].rStrTs:0.0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].rSpdModeDerFltrCoef:0.0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].rStrGearRatio:0.0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].rStrSpdDeadBand:0.0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].ulStrSpdUpperBoundary:0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].ulStrSpdLowerBoundary:0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].rStrSpdROTO:0.0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].rStrSpdDiffCntsFltrGain:0.0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].rTfdCurrent_VS_TractionSpeed_Speed[0]:0.0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].rTfdCurrent_VS_TractionSpeed_Current[0]:0.0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].rTfdCurrent_VS_SteeringError_Error[0]:0.0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].rTfdCurrent_VS_SteeringError_Current[0]:0.0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].rTfdMaxCurrent:0.0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].rTfdReduction:0.0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].rTfdStiffCurrent:0.0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].rTfdRampDnHoldCurrent:0.0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].rTfdCurrentRampRateLimiter:0.0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].rTfdRampDnHoldTime:0.0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].rTfdHoldUpSpeedMax:0.0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].rStrMtrZeroSpd:0.0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].slStrMtrSpdLmt:0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].rStrMtrSpdRedTrxSpdBP:0.0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].rStrMtrSpdRedMaxTrxSpd:0.0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].rStrMtrSpdMaxTrxSpdRf:0.0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].rStrMtrSpdRedWhAngBP:0.0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].rStrMtrSpdRedMaxWhAng:0.0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].rStrMtrSpdMaxWhAngRf:0.0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].ulTrigFunct:0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].ulStrMtrSpdWhAngRfType:0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].rTilScalingTrxSpdBP:0.0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].rTilScalingTrxSpdMax:0.0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].rTilScalingMaxTrxSpdSf:0.0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].rTillerMotionThreshold:0.0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].rCmd1_RO_Lwr:0.0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].rCmd1_Rng_Upr:0.0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].rCmd1_Rng_Lwr:0.0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].rAssistGain:0.0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].rStrCmdDeadBand:0.0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].rTrxSpdWhAng_SpdOs:0.0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].eUse360Feature:DO_NOT_USE
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].rStrMtrSpdRedWhAngErrBP:0.0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].rStrMtrSpdRedMaxWhAngErr:0.0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].rStrMtrSpdMaxWhAngErrRf:0.0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].rHndlSpdClip:0.0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].rHndlSpdFltrCoef:0.0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].rHndlSpdThrshld:0.0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].rWhAngTrgtFltGain:0.0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].rSahNmbnsTrxSpdBpSf:0.0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].rSahNmbnsTlr2DuSf:0.0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].rSpFltrExpSf:0.0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].rSpFltrExpRef:0.0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].rSahNmbnsMaxInitAng:0.0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].rSahNmbnsTlr2DuNom:0.0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].rSahNmbnsMaxNumbedAng:0.0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].rSahNmbnsTransAng:0.0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].rStrNoEdRequired:0.0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].rSteerAngleBeforeED:0.0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].eSteer_Direction:Forward
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].slSensorSlope:0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].slSensorSignalRelation:0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].rStrSpdBumpThrshld:0.0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].rBumpCancelThrshld:0.0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].rDeadBandGain:0.0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].rDeadBandHyst:0.0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].rDeadBandReEntDly:0.0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].rBumpCmdRawBoundary:0.0
TEST.EXPECTED:AssistCmd.<<GLOBAL>>.gpSteer_params[0].rStrSpdDbBumpThrshldGain:0.0
TEST.EXPECTED:AssistCmd.AssistCmd.rtu_AsstCmdIn:1.0
TEST.EXPECTED:AssistCmd.AssistCmd.rtu_DuInCwStop:0
TEST.EXPECTED:AssistCmd.AssistCmd.rtu_DuInCcwStop:0
TEST.EXPECTED:AssistCmd.AssistCmd.rtu_StrAllowedIn:3
TEST.EXPECTED:AssistCmd.AssistCmd.rtu_gpSensor_cals_c2m[0].rSecHgtSensorCalGain:0.0
TEST.EXPECTED:AssistCmd.AssistCmd.rtu_gpSensor_cals_c2m[0].rPriHgtSensorCalGain:1.0
TEST.EXPECTED:AssistCmd.AssistCmd.rtu_gpSensor_cals_c2m[0].rPrimaryCalCountAtMax:2.0
TEST.EXPECTED:AssistCmd.AssistCmd.rtu_gpSensor_cals_c2m[0].rAuxHtSensorCalGain:3.0
TEST.EXPECTED:AssistCmd.AssistCmd.rtu_gpSensor_cals_c2m[0].rReachSensorMinMax[0]:0.0
TEST.EXPECTED:AssistCmd.AssistCmd.rtu_gpSensor_cals_c2m[0].rTiltSensorLevel:8.0
TEST.EXPECTED:AssistCmd.AssistCmd.rtu_gpSensor_cals_c2m[0].rPivotSensorCalGain:7.0
TEST.EXPECTED:AssistCmd.AssistCmd.rtu_gpSensor_cals_c2m[0].rTraverseSensorCalGain:6.0
TEST.EXPECTED:AssistCmd.AssistCmd.rtu_gpSensor_cals_c2m[0].rLSOffset_Primary:4.0
TEST.EXPECTED:AssistCmd.AssistCmd.rtu_gpSensor_cals_c2m[0].rLSOffset_Secondary:4.0
TEST.EXPECTED:AssistCmd.AssistCmd.rtu_gpSensor_cals_c2m[0].rLSOffset_Aux_cyl:3.0
TEST.EXPECTED:AssistCmd.AssistCmd.rtu_gpSensor_cals_c2m[0].rAccelXoffset:1.0
TEST.EXPECTED:AssistCmd.AssistCmd.rtu_gpSensor_cals_c2m[0].rAccelYoffset:1.0
TEST.EXPECTED:AssistCmd.AssistCmd.rtu_gpSensor_cals_c2m[0].rAccelZoffset:1.0
TEST.EXPECTED:AssistCmd.AssistCmd.rtu_gpSensor_cals_c2m[0].rStrHndlCmd1[1]:1.0
TEST.EXPECTED:AssistCmd.AssistCmd.rtu_gpSensor_cals_c2m[0].slHndlAng1[0]:2
TEST.EXPECTED:AssistCmd.AssistCmd.rtu_gpSensor_cals_c2m[0].rStrHndlCmd2[0]:3.0
TEST.END

-- Subprogram: AssistCmd_initialize

-- Test Case: AssistCmd_initialize.001
TEST.UNIT:AssistCmd
TEST.SUBPROGRAM:AssistCmd_initialize
TEST.NEW
TEST.NAME:AssistCmd_initialize.001
TEST.COMPOUND_ONLY
TEST.END

-- Unit: Cmd_Pos_Integrator

-- Subprogram: Cmd_Pos_Integrator

-- Test Case: Cmd_Pos_Integrator.001
TEST.UNIT:Cmd_Pos_Integrator
TEST.SUBPROGRAM:Cmd_Pos_Integrator
TEST.NEW
TEST.NAME:Cmd_Pos_Integrator.001
TEST.COMPOUND_ONLY
TEST.VALUE:Cmd_Pos_Integrator.Cmd_Pos_Integrator.rtu_gpSensor_cals_c2m:<<malloc 1>>
TEST.VALUE:Cmd_Pos_Integrator.Cmd_Pos_Integrator.rtu_gpSensor_cals_c2m[0].slDuAng[2]:10
TEST.EXPECTED:Cmd_Pos_Integrator.Cmd_Pos_Integrator.rtu_gpSensor_cals_c2m[0].slDuAng[2]:10
TEST.END

-- Subprogram: Cmd_Pos_Integrator_CompStdDev

-- Test Case: Cmd_Pos_Integrator_CompStdDev.001
TEST.UNIT:Cmd_Pos_Integrator
TEST.SUBPROGRAM:Cmd_Pos_Integrator_CompStdDev
TEST.NEW
TEST.NAME:Cmd_Pos_Integrator_CompStdDev.001
TEST.COMPOUND_ONLY
TEST.STUB:PosCmd.PosCmd_initialize
TEST.VALUE:Cmd_Pos_Integrator.Cmd_Pos_Integrator_CompStdDev.SampleSet[0]:0.0
TEST.VALUE:Cmd_Pos_Integrator.Cmd_Pos_Integrator_CompStdDev.Samples:0
TEST.VALUE:Cmd_Pos_Integrator.Cmd_Pos_Integrator_CompStdDev.return:0.0
TEST.EXPECTED:Cmd_Pos_Integrator.Cmd_Pos_Integrator_CompStdDev.SampleSet[0]:0.0
TEST.EXPECTED:Cmd_Pos_Integrator.Cmd_Pos_Integrator_CompStdDev.Samples:0
TEST.EXPECTED:Cmd_Pos_Integrator.Cmd_Pos_Integrator_CompStdDev.return:0.0
TEST.END

-- Subprogram: Cmd_Pos_Integrator_Init

-- Test Case: Cmd_Pos_Integrator_Init.001
TEST.UNIT:Cmd_Pos_Integrator
TEST.SUBPROGRAM:Cmd_Pos_Integrator_Init
TEST.NEW
TEST.NAME:Cmd_Pos_Integrator_Init.001
TEST.COMPOUND_ONLY
TEST.STUB:AssistCmd.AssistCmd
TEST.STUB:Cmd_Pos_Integrator.Cmd_Pos_Integrator_CompStdDev
TEST.STUB:Cmd_Pos_Integrator.Cmd_Pos_Integrator_UpdateMat
TEST.STUB:Cmd_Pos_Integrator.Cmd_Pos_Integrator
TEST.STUB:Cmd_Pos_Integrator.Cmd_Pos_Integrator_initialize
TEST.STUB:HmsSim_Steering.HmsSim_Steering_Init
TEST.STUB:HmsSim_Steering.HmsSim_Steering_Start
TEST.STUB:HmsSim_Steering.HmsSim_Steering
TEST.STUB:HmsSim_Steering.HmsSim_Steering_initialize
TEST.STUB:PosCmd.PosCmd_ForceMonoinc
TEST.STUB:PosCmd.PosCmd_ForceMonoinc1
TEST.STUB:PosCmd.PosCmd_FwdLoad
TEST.STUB:PosCmd.PosCmd_RevLoad
TEST.STUB:PosCmd.PosCmd_FwdLdd
TEST.STUB:PosCmd.PosCmd_FwdLoad_c
TEST.STUB:PosCmd.PosCmd_RevLoad_g
TEST.STUB:PosCmd.PosCmd_RevUnldd1
TEST.STUB:PosCmd.PosCmd_RevLdd1
TEST.STUB:PosCmd.PosCmd_FwdUnLdd1
TEST.STUB:PosCmd.PosCmd_FwdLdd_a
TEST.STUB:PosCmd.PosCmd_FwdLdd_j
TEST.STUB:PosCmd.PosCmd_RevUnldd
TEST.STUB:PosCmd.PosCmd_RevLdd
TEST.STUB:PosCmd.PosCmd_FwdUnLdd
TEST.STUB:PosCmd.PosCmd_FwdLdd_m
TEST.STUB:PosCmd.PosCmd_FwdLdd_n
TEST.STUB:PosCmd.PosCmd_Init
TEST.STUB:PosCmd.PosCmd
TEST.STUB:PosCmd.PosCmd_initialize
TEST.STUB:SteeringSIM.SteeringSIM_Init
TEST.STUB:SteeringSIM.SteeringSIM
TEST.STUB:SteeringSIM.SteeringSIM_initialize
TEST.STUB:StrMstrApp.StrMstrApp_Init
TEST.STUB:StrMstrApp.StrMstrApp
TEST.STUB:StrMstrApp.StrMstrApp_initialize
TEST.STUB:StrMstrSup.StrMstrSup
TEST.STUB:StrMtrSpdLmt.StrMtrSpdLmt_Init
TEST.STUB:StrMtrSpdLmt.StrMtrSpdLmt
TEST.STUB:StrMtrSpdLmt.StrMtrSpdLmt_initialize
TEST.STUB:StrPosHndlSpdSense.StrPosHndlSpdSense_FltrDecay1a
TEST.STUB:StrPosHndlSpdSense.StrPosHndlSpdSense_Init
TEST.STUB:StrPosHndlSpdSense.StrPosHndlSpdSense
TEST.STUB:StrPosHndlSpdSense.StrPosHndlSpdSense_initialize
TEST.STUB:StrPosWhAngLimiter.StrPosWhAngLimiter_Init
TEST.STUB:StrPosWhAngLimiter.StrPosWhAngLimiter
TEST.STUB:StrPos_SahNmbns.StrPos_SahNmbns_HndlInNmbnsRng
TEST.STUB:StrPos_SahNmbns.StrPos_SahNmbns_HndlInTransRng
TEST.STUB:StrPos_SahNmbns.StrPos_SahNmbns_Init
TEST.STUB:StrPos_SahNmbns.StrPos_SahNmbns
TEST.STUB:StrPos_SahNmbns.StrPos_SahNmbns_initialize
TEST.STUB:StrPos_SetPntRateLmtr.StrPos_SetPntRateLmtr
TEST.STUB:StrPos_SetPntRateLmtr.StrPos_SetPntRateLmt_initialize
TEST.STUB:TfdCurrentSpProfile.TfdCurrentSpProfile
TEST.STUB:TfdCurrentSpProfile.TfdCurrentSpProfile_initialize
TEST.STUB:TfdOpMode.TfdOpMode_TfdCurrentSpProfile
TEST.STUB:TfdOpMode.TfdOpMode_Init
TEST.STUB:TfdOpMode.TfdOpMode
TEST.STUB:TfdOpMode.TfdOpMode_initialize
TEST.STUB:TfdOvrMode.TfdOvrMode_Init
TEST.STUB:TfdOvrMode.TfdOvrMode
TEST.STUB:TfdOvrMode.TfdOvrMode_initialize
TEST.STUB:Til2DuScaling.Til2DuScaling
TEST.STUB:Til2DuScaling.Til2DuScaling_initialize
TEST.STUB:TrxSpd_StrMtrSpdLmt.TrxSpd_StrMtrSpdLmt_Init
TEST.STUB:TrxSpd_StrMtrSpdLmt.TrxSpd_StrMtrSpdLmt
TEST.STUB:TrxSpd_StrMtrSpdLmt.TrxSpd_StrMtrSpdLmt_initialize
TEST.STUB:WhAngErr_StrMtrSpdLmt.WhAngErr_StrMtrSpdLmt_Init
TEST.STUB:WhAngErr_StrMtrSpdLmt.WhAngErr_StrMtrSpdLmt
TEST.STUB:WhAngErr_StrMtrSpdLmt.WhAngErr_StrMtrSpdLm_initialize
TEST.STUB:WhAng_StrMtrSpdLmt.WhAng_StrMtrSpdLmt_Init
TEST.STUB:WhAng_StrMtrSpdLmt.WhAng_StrMtrSpdLmt
TEST.STUB:WhAng_StrMtrSpdLmt.WhAng_StrMtrSpdLmt_initialize
TEST.VALUE:Cmd_Pos_Integrator.Cmd_Pos_Integrator_CompStdDev.Samples:0
TEST.VALUE:Cmd_Pos_Integrator.Cmd_Pos_Integrator_CompStdDev.return:0.0
TEST.VALUE:Cmd_Pos_Integrator.Cmd_Pos_Integrator_UpdateMat.NewSample:0.0
TEST.VALUE:Cmd_Pos_Integrator.Cmd_Pos_Integrator_UpdateMat.UpdatedMat[0]:0.0
TEST.VALUE:Cmd_Pos_Integrator.Cmd_Pos_Integrator.rtu_DiffCmd_In:0.0
TEST.VALUE:Cmd_Pos_Integrator.Cmd_Pos_Integrator.rtu_Til2WhRatio:0.0
TEST.VALUE:Cmd_Pos_Integrator.Cmd_Pos_Integrator.rtu_gpSensor_cals_c2m[0].slHndlAng2[0]:1
TEST.END

-- Subprogram: Cmd_Pos_Integrator_UpdateMat

-- Test Case: Cmd_Pos_Integrator_UpdateMat.001
TEST.UNIT:Cmd_Pos_Integrator
TEST.SUBPROGRAM:Cmd_Pos_Integrator_UpdateMat
TEST.NEW
TEST.NAME:Cmd_Pos_Integrator_UpdateMat.001
TEST.COMPOUND_ONLY
TEST.VALUE:Cmd_Pos_Integrator.Cmd_Pos_Integrator_UpdateMat.NewSample:0.0
TEST.EXPECTED:Cmd_Pos_Integrator.Cmd_Pos_Integrator_UpdateMat.NewSample:0.0
TEST.END

-- Subprogram: Cmd_Pos_Integrator_initialize

-- Test Case: Cmd_Pos_Integrator_initialize.001
TEST.UNIT:Cmd_Pos_Integrator
TEST.SUBPROGRAM:Cmd_Pos_Integrator_initialize
TEST.NEW
TEST.NAME:Cmd_Pos_Integrator_initialize.001
TEST.COMPOUND_ONLY
TEST.STUB:Cmd_Pos_Integrator.Cmd_Pos_Integrator_UpdateMat
TEST.STUB:Cmd_Pos_Integrator.Cmd_Pos_Integrator_Init
TEST.VALUE:Cmd_Pos_Integrator.Cmd_Pos_Integrator_UpdateMat.NewSample:0.0
TEST.ATTRIBUTES:Cmd_Pos_Integrator.<<GLOBAL>>.gpSteer_params[0].ulStrSpdUpperBoundary:INPUT_BASE=8
TEST.END

-- Unit: HmsSim_Steering

-- Subprogram: HmsSim_Steering

-- Test Case: HmsSim_Steering.001
TEST.UNIT:HmsSim_Steering
TEST.SUBPROGRAM:HmsSim_Steering
TEST.NEW
TEST.NAME:HmsSim_Steering.001
TEST.COMPOUND_ONLY
TEST.STUB:HmsSim_Steering.HmsSim_Steering_Init
TEST.STUB:HmsSim_Steering.HmsSim_Steering_Start
TEST.STUB:HmsSim_Steering.HmsSim_Steering_initialize
TEST.STUB:HmsSim_SteeringBuild.HmsSim_SteeringBuild_step
TEST.STUB:HmsSim_SteeringBuild.HmsSim_SteeringBuild_initialize
TEST.STUB:HmsSim_SteeringBuild.HmsSim_SteeringBuild_terminate
TEST.VALUE:uut_prototype_stubs.Str_Traction_Speed_Crosscheck.rty_StrTractionSpeedCrosscheck:""
TEST.VALUE:uut_prototype_stubs.Str_Sp_Crosscheck1.rtu_SteerOutputBus[0].slMstrStrCmd:0
TEST.VALUE:uut_prototype_stubs.Str_Sp_Crosscheck1.rtu_SteerOutputBus[0].slSlvStrCmd:0
TEST.VALUE:uut_prototype_stubs.Str_Sp_Crosscheck1.rtu_SteerOutputBus[0].fSteeringEnabled:0
TEST.VALUE:uut_prototype_stubs.Str_Sp_Crosscheck1.rtu_SteerOutputBus[0].rTfdCmd:0.0
TEST.VALUE:uut_prototype_stubs.Str_Sp_Crosscheck1.rtu_SteerOutputBus[0].slStrMtrSpdMax:0
TEST.VALUE:uut_prototype_stubs.Str_Sp_Crosscheck1.rtu_SteerOutputBus[0].ulStrCmdFreshCntr:0
TEST.VALUE:uut_prototype_stubs.Str_Sp_Crosscheck1.rtu_SteerOutputBus[0].ulStopSteering:0
TEST.VALUE:HmsSim_Steering.<<GLOBAL>>.gpSteering_fdbk:<<malloc 1>>
TEST.VALUE:HmsSim_Steering.<<GLOBAL>>.gpSteering_fdbk[0].MstrFdbk.rStrPos:0.0
TEST.VALUE:HmsSim_Steering.<<GLOBAL>>.gpSteering_fdbk[0].MstrFdbk.rStrSpd:10.0
TEST.VALUE:HmsSim_Steering.<<GLOBAL>>.gpSteering_fdbk[0].MstrFdbk.rStrCur:6.0
TEST.VALUE:HmsSim_Steering.<<GLOBAL>>.gpSteering_fdbk[0].MstrFdbk.ulStopTrx:4
TEST.VALUE:HmsSim_Steering.<<GLOBAL>>.gpSteering_fdbk[0].MstrFdbk.ulFreshCnt:2
TEST.VALUE:HmsSim_Steering.<<GLOBAL>>.gpSteering_fdbk[0].SlvFdbk.rStrPos:3.0
TEST.VALUE:HmsSim_Steering.<<GLOBAL>>.gpSteering_fdbk[0].SlvFdbk.rStrSpd:4.0
TEST.VALUE:HmsSim_Steering.<<GLOBAL>>.gpSteering_fdbk[0].SlvFdbk.rStrCur:3.0
TEST.VALUE:HmsSim_Steering.<<GLOBAL>>.gpSteering_fdbk[0].SlvFdbk.ulStopTrx:1
TEST.VALUE:HmsSim_Steering.<<GLOBAL>>.gpSteering_fdbk[0].SlvFdbk.ulFreshCnt:2
TEST.VALUE:HmsSim_Steering.<<GLOBAL>>.gpFeatureEnabledEvents:<<malloc 1>>
TEST.VALUE:HmsSim_Steering.<<GLOBAL>>.gpFeatureEnabledEvents[0].ulEventsEnabled[0]:1
TEST.VALUE:HmsSim_Steering.<<GLOBAL>>.gpFeatureEnabledEvents[0].fAnyEventActive:1
TEST.VALUE:StrMstrSup.<<GLOBAL>>.gpFeatures_bus:<<malloc 1>>
TEST.EXPECTED:uut_prototype_stubs.Str_U1_Fresh_Counter.rtu_gTraction_output[0].rTorqueCmd:0.0
TEST.EXPECTED:uut_prototype_stubs.Str_U1_Fresh_Counter.rtu_gTraction_output[0].rBrake1Cmd:0.0
TEST.EXPECTED:uut_prototype_stubs.Str_U1_Fresh_Counter.rtu_gTraction_output[0].rBrake2Cmd:0.0
TEST.EXPECTED:uut_prototype_stubs.Str_U1_Fresh_Counter.rtu_gTraction_output[0].rBrake3Cmd:0.0
TEST.EXPECTED:uut_prototype_stubs.Str_U1_Fresh_Counter.rtu_gTraction_output[0].rVehicleSpeed:0.0
TEST.EXPECTED:uut_prototype_stubs.Str_U1_Fresh_Counter.rtu_gTraction_output[0].rSpeedCommand:0.0
TEST.EXPECTED:uut_prototype_stubs.Str_U1_Fresh_Counter.rtu_gTraction_output[0].rDistanceTraveled:0.0
TEST.EXPECTED:uut_prototype_stubs.Str_U1_Fresh_Counter.rtu_gTraction_output[0].ulFwdSwitch:0
TEST.EXPECTED:uut_prototype_stubs.Str_U1_Fresh_Counter.rtu_gTraction_output[0].ulRevSwitch:0
TEST.EXPECTED:uut_prototype_stubs.Str_U1_Fresh_Counter.rtu_gTraction_output[0].fBrakeRequest:0
TEST.EXPECTED:uut_prototype_stubs.Str_U1_Fresh_Counter.rtu_gTraction_output[0].fBrake_Coils_In_Lockout:0
TEST.EXPECTED:uut_prototype_stubs.Str_U1_Fresh_Counter.rtu_gTraction_output[0].eTractionState:TRACT_STATE_PARK
TEST.EXPECTED:uut_prototype_stubs.Str_U1_Fresh_Counter.rtu_gTraction_output[0].ulTravelAlarmCmd:0
TEST.EXPECTED:uut_prototype_stubs.Str_U1_Fresh_Counter.rtu_gTraction_output[0].eTractionMotorState:TRACT_MOTOR_STATE_MOTORING
TEST.EXPECTED:uut_prototype_stubs.Str_U1_Fresh_Counter.rtu_gTraction_output[0].fTrxAppClock:0
TEST.EXPECTED:uut_prototype_stubs.Str_U1_Fresh_Counter.rtu_gTraction_output[0].fSlippageActiveStat:0
TEST.EXPECTED:uut_prototype_stubs.Str_U1_Fresh_Counter.rtu_gTraction_output[0].rAccelCommanded:0.0
TEST.EXPECTED:uut_prototype_stubs.Str_U1_Fresh_Counter.rtu_gTraction_output[0].rAccelMeasured:0.0
TEST.EXPECTED:uut_prototype_stubs.Str_U1_Fresh_Counter.rtu_gTraction_output[0].rAccelCalculated:0.0
TEST.EXPECTED:uut_prototype_stubs.Str_U1_Fresh_Counter.rtu_gTraction_output[0].rX_Speed:0.0
TEST.EXPECTED:uut_prototype_stubs.Str_U1_Fresh_Counter.rtu_gTraction_output[0].slDirectionFlipper:0
TEST.EXPECTED:uut_prototype_stubs.Str_U1_Fresh_Counter.rtu_gTraction_output[0].fStatusWordOK:0
TEST.EXPECTED:uut_prototype_stubs.Str_U1_Fresh_Counter.rtu_gTraction_output[0].fSpeedCutbackSRO:0
TEST.EXPECTED:uut_prototype_stubs.Str_U1_Fresh_Counter.rtu_gTraction_output[0].ulStrobeCmd:0
TEST.EXPECTED:uut_prototype_stubs.Str_U1_Fresh_Counter.rtu_gTraction_output[0].rBrake1TorqueGap:0.0
TEST.EXPECTED:uut_prototype_stubs.Str_U1_Fresh_Counter.rtu_gTraction_output[0].rBrake2TorqueGap:0.0
TEST.EXPECTED:uut_prototype_stubs.Str_U1_Fresh_Counter.rty_U1FreshCount[0]:0
TEST.EXPECTED:uut_prototype_stubs.Str_U1_Fresh_Counter.rty_U1OkCrossCk:""
TEST.EXPECTED:uut_prototype_stubs.Str_Traction_Speed_Crosscheck.rtu_traction_output_bus[0].rTorqueCmd:0.0
TEST.EXPECTED:uut_prototype_stubs.Str_Traction_Speed_Crosscheck.rtu_traction_output_bus[0].rBrake1Cmd:0.0
TEST.EXPECTED:uut_prototype_stubs.Str_Traction_Speed_Crosscheck.rtu_traction_output_bus[0].rBrake2Cmd:0.0
TEST.EXPECTED:uut_prototype_stubs.Str_Traction_Speed_Crosscheck.rtu_traction_output_bus[0].rBrake3Cmd:0.0
TEST.EXPECTED:uut_prototype_stubs.Str_Traction_Speed_Crosscheck.rtu_traction_output_bus[0].rVehicleSpeed:0.0
TEST.EXPECTED:uut_prototype_stubs.Str_Traction_Speed_Crosscheck.rtu_traction_output_bus[0].rSpeedCommand:0.0
TEST.EXPECTED:uut_prototype_stubs.Str_Traction_Speed_Crosscheck.rtu_traction_output_bus[0].rDistanceTraveled:0.0
TEST.EXPECTED:uut_prototype_stubs.Str_Traction_Speed_Crosscheck.rtu_traction_output_bus[0].ulFwdSwitch:0
TEST.EXPECTED:uut_prototype_stubs.Str_Traction_Speed_Crosscheck.rtu_traction_output_bus[0].ulRevSwitch:0
TEST.EXPECTED:uut_prototype_stubs.Str_Traction_Speed_Crosscheck.rtu_traction_output_bus[0].fBrakeRequest:0
TEST.EXPECTED:uut_prototype_stubs.Str_Traction_Speed_Crosscheck.rtu_traction_output_bus[0].fBrake_Coils_In_Lockout:0
TEST.EXPECTED:uut_prototype_stubs.Str_Traction_Speed_Crosscheck.rtu_traction_output_bus[0].eTractionState:TRACT_STATE_PARK
TEST.EXPECTED:uut_prototype_stubs.Str_Traction_Speed_Crosscheck.rtu_traction_output_bus[0].ulTravelAlarmCmd:0
TEST.EXPECTED:uut_prototype_stubs.Str_Traction_Speed_Crosscheck.rtu_traction_output_bus[0].eTractionMotorState:TRACT_MOTOR_STATE_MOTORING
TEST.EXPECTED:uut_prototype_stubs.Str_Traction_Speed_Crosscheck.rtu_traction_output_bus[0].fTrxAppClock:0
TEST.EXPECTED:uut_prototype_stubs.Str_Traction_Speed_Crosscheck.rtu_traction_output_bus[0].fSlippageActiveStat:0
TEST.EXPECTED:uut_prototype_stubs.Str_Traction_Speed_Crosscheck.rtu_traction_output_bus[0].rAccelCommanded:0.0
TEST.EXPECTED:uut_prototype_stubs.Str_Traction_Speed_Crosscheck.rtu_traction_output_bus[0].rAccelMeasured:0.0
TEST.EXPECTED:uut_prototype_stubs.Str_Traction_Speed_Crosscheck.rtu_traction_output_bus[0].rAccelCalculated:0.0
TEST.EXPECTED:uut_prototype_stubs.Str_Traction_Speed_Crosscheck.rtu_traction_output_bus[0].rX_Speed:0.0
TEST.EXPECTED:uut_prototype_stubs.Str_Traction_Speed_Crosscheck.rtu_traction_output_bus[0].slDirectionFlipper:0
TEST.EXPECTED:uut_prototype_stubs.Str_Traction_Speed_Crosscheck.rtu_traction_output_bus[0].fStatusWordOK:0
TEST.EXPECTED:uut_prototype_stubs.Str_Traction_Speed_Crosscheck.rtu_traction_output_bus[0].fSpeedCutbackSRO:0
TEST.EXPECTED:uut_prototype_stubs.Str_Traction_Speed_Crosscheck.rtu_traction_output_bus[0].ulStrobeCmd:0
TEST.EXPECTED:uut_prototype_stubs.Str_Traction_Speed_Crosscheck.rtu_traction_output_bus[0].rBrake1TorqueGap:0.0
TEST.EXPECTED:uut_prototype_stubs.Str_Traction_Speed_Crosscheck.rtu_traction_output_bus[0].rBrake2TorqueGap:0.0
TEST.EXPECTED:uut_prototype_stubs.Str_Traction_Speed_Crosscheck.rtu_steer_m2m_output_bus[0].fStopTrac:0
TEST.EXPECTED:uut_prototype_stubs.Str_Traction_Speed_Crosscheck.rtu_steer_m2m_output_bus[0].uwSdmPwrCmdMode:0
TEST.EXPECTED:uut_prototype_stubs.Str_Traction_Speed_Crosscheck.rtu_steer_m2m_output_bus[0].rWhAngTarget:0.0
TEST.EXPECTED:uut_prototype_stubs.Str_Traction_Speed_Crosscheck.rtu_steer_m2m_output_bus[0].fStrCmdActive:0
TEST.EXPECTED:uut_prototype_stubs.Str_Traction_Speed_Crosscheck.rtu_steer_m2m_output_bus[0].fStrMtrMoving:0
TEST.EXPECTED:uut_prototype_stubs.Str_Traction_Speed_Crosscheck.rtu_steer_m2m_output_bus[0].eStrOpMode_State:STR_OP_NoState
TEST.EXPECTED:uut_prototype_stubs.Str_Traction_Speed_Crosscheck.rtu_steer_m2m_output_bus[0].fSteerOkClk:0
TEST.EXPECTED:uut_prototype_stubs.Str_Traction_Speed_Crosscheck.rtu_steer_m2m_output_bus[0].rTrxSpdLmtWhAngCmd:0.0
TEST.EXPECTED:uut_prototype_stubs.Str_Traction_Speed_Crosscheck.rtu_steer_m2m_output_bus[0].fDuInRng:0
TEST.EXPECTED:uut_prototype_stubs.Str_Traction_Speed_Crosscheck.rtu_steer_m2m_output_bus[0].fStopSteering:0
TEST.EXPECTED:uut_prototype_stubs.Str_Traction_Speed_Crosscheck.rtu_steer_m2m_output_bus[0].slFwdRevSteer:0
TEST.EXPECTED:uut_prototype_stubs.Str_Traction_Speed_Crosscheck.rtu_steer_m2m_output_bus[0].slStrPos_HndlPos:0
TEST.EXPECTED:uut_prototype_stubs.Str_Traction_Speed_Crosscheck.rtu_steer_m2m_output_bus[0].rStrSpd_DiffCnts:0.0
TEST.EXPECTED:uut_prototype_stubs.Str_Traction_Speed_Crosscheck.rty_StrTractionSpeedCrosscheck:""
TEST.EXPECTED:uut_prototype_stubs.Str_Stop_Tract_Crosscheck1.rtu_steering_ms_fdbk_bus[0].MstrFdbk.rStrPos:0.0
TEST.EXPECTED:uut_prototype_stubs.Str_Stop_Tract_Crosscheck1.rtu_steering_ms_fdbk_bus[0].MstrFdbk.rStrSpd:10.0
TEST.EXPECTED:uut_prototype_stubs.Str_Stop_Tract_Crosscheck1.rtu_steering_ms_fdbk_bus[0].MstrFdbk.rStrCur:6.0
TEST.EXPECTED:uut_prototype_stubs.Str_Stop_Tract_Crosscheck1.rtu_steering_ms_fdbk_bus[0].MstrFdbk.ulStopTrx:4
TEST.EXPECTED:uut_prototype_stubs.Str_Stop_Tract_Crosscheck1.rtu_steering_ms_fdbk_bus[0].MstrFdbk.ulFreshCnt:2
TEST.EXPECTED:uut_prototype_stubs.Str_Stop_Tract_Crosscheck1.rtu_ed_output_bus[0].ulTcmCmd:0
TEST.EXPECTED:uut_prototype_stubs.Str_Stop_Tract_Crosscheck1.rtu_ed_output_bus[0].ulScmCmd:0
TEST.EXPECTED:uut_prototype_stubs.Str_Stop_Tract_Crosscheck1.rtu_ed_output_bus[0].ulHcmCmd:0
TEST.EXPECTED:uut_prototype_stubs.Str_Stop_Tract_Crosscheck1.rtu_ed_output_bus[0].ulEdDriversOn:0
TEST.EXPECTED:uut_prototype_stubs.Str_Stop_Tract_Crosscheck1.rtu_ed_output_bus[0].ulHdmCmd:0
TEST.EXPECTED:uut_prototype_stubs.Str_Stop_Tract_Crosscheck1.rtu_ed_output_bus[0].fEdClosed:0
TEST.EXPECTED:uut_prototype_stubs.Str_Stop_Tract_Crosscheck1.rtu_ed_output_bus[0].fEdWillDrop:0
TEST.EXPECTED:uut_prototype_stubs.Str_Stop_Tract_Crosscheck1.rtu_ed_output_bus[0].fPwrDrvAllowed:0
TEST.EXPECTED:uut_prototype_stubs.Str_Stop_Tract_Crosscheck1.rtu_ed_output_bus[0].uwFaultDetected:0
TEST.EXPECTED:uut_prototype_stubs.Str_Stop_Tract_Crosscheck1.rtu_ed_output_bus[0].eEdState:ED_NoState
TEST.EXPECTED:uut_prototype_stubs.Str_Stop_Tract_Crosscheck1.rtu_ed_output_bus[0].rBusVoltsStart:0.0
TEST.EXPECTED:uut_prototype_stubs.Str_Stop_Tract_Crosscheck1.rtu_feature_enabled_events_bus1[0].ulEventsEnabled[0]:1
TEST.EXPECTED:uut_prototype_stubs.Str_Stop_Tract_Crosscheck1.rtu_feature_enabled_events_bus1[0].fAnyEventActive:1
TEST.EXPECTED:uut_prototype_stubs.Str_Stop_Tract_Crosscheck1.rtu_autocal_active_bus[0].fMainHoistThrotCalActive:0
TEST.EXPECTED:uut_prototype_stubs.Str_Stop_Tract_Crosscheck1.rtu_autocal_active_bus[0].fAuxHoistThrotCalActive:0
TEST.EXPECTED:uut_prototype_stubs.Str_Stop_Tract_Crosscheck1.rtu_autocal_active_bus[0].fTxnThrotCalActive:0
TEST.EXPECTED:uut_prototype_stubs.Str_Stop_Tract_Crosscheck1.rtu_autocal_active_bus[0].fAccy1ThrotCalActive:0
TEST.EXPECTED:uut_prototype_stubs.Str_Stop_Tract_Crosscheck1.rtu_autocal_active_bus[0].fAccy2ThrotCalActive:0
TEST.EXPECTED:uut_prototype_stubs.Str_Stop_Tract_Crosscheck1.rtu_autocal_active_bus[0].fTiltThrotCalActive:0
TEST.EXPECTED:uut_prototype_stubs.Str_Stop_Tract_Crosscheck1.rtu_autocal_active_bus[0].fBrakeThrotCalActive:0
TEST.EXPECTED:uut_prototype_stubs.Str_Stop_Tract_Crosscheck1.rtu_autocal_active_bus[0].fSteerHandleCalActive:0
TEST.EXPECTED:uut_prototype_stubs.Str_Stop_Tract_Crosscheck1.rtu_autocal_active_bus[0].fLoadSensorCalActive:0
TEST.EXPECTED:uut_prototype_stubs.Str_Stop_Tract_Crosscheck1.rtu_autocal_active_bus[0].fTiltFbCalActive:0
TEST.EXPECTED:uut_prototype_stubs.Str_Stop_Tract_Crosscheck1.rtu_autocal_active_bus[0].fMainPriHgtFbCalActive:0
TEST.EXPECTED:uut_prototype_stubs.Str_Stop_Tract_Crosscheck1.rtu_autocal_active_bus[0].fMainSecHgtFbCalActive:0
TEST.EXPECTED:uut_prototype_stubs.Str_Stop_Tract_Crosscheck1.rtu_autocal_active_bus[0].fReachFbCalActive:0
TEST.EXPECTED:uut_prototype_stubs.Str_Stop_Tract_Crosscheck1.rtu_autocal_active_bus[0].fAccelCalActive:0
TEST.EXPECTED:uut_prototype_stubs.Str_Stop_Tract_Crosscheck1.rtu_autocal_active_bus[0].fSteerWheelCtrCalActive:0
TEST.EXPECTED:uut_prototype_stubs.Str_Stop_Tract_Crosscheck1.rtu_autocal_active_bus[0].fSteerCtrCalInitiate:0
TEST.EXPECTED:uut_prototype_stubs.Str_Stop_Tract_Crosscheck1.rtu_autocal_active_bus[0].fSteerCtrCalSave:0
TEST.EXPECTED:uut_prototype_stubs.Str_Stop_Tract_Crosscheck1.rtu_autocal_active_bus[0].fMainLowerValveCalActive:0
TEST.EXPECTED:uut_prototype_stubs.Str_Stop_Tract_Crosscheck1.rtu_autocal_active_bus[0].fAccessoryValveCalActive:0
TEST.EXPECTED:uut_prototype_stubs.Str_Stop_Tract_Crosscheck1.rtu_autocal_active_bus[0].fPVC1ValveCalActive:0
TEST.EXPECTED:uut_prototype_stubs.Str_Stop_Tract_Crosscheck1.rtu_autocal_active_bus[0].fPVC2ValveCalActive:0
TEST.EXPECTED:uut_prototype_stubs.Str_Stop_Tract_Crosscheck1.rtu_autocal_active_bus[0].fUserCutOutCalActive:0
TEST.EXPECTED:uut_prototype_stubs.Str_Stop_Tract_Crosscheck1.rtu_autocal_active_bus[0].fRackSelectCalActive:0
TEST.EXPECTED:uut_prototype_stubs.Str_Stop_Tract_Crosscheck1.rtu_autocal_active_bus[0].fCustomHeightCalActive:0
TEST.EXPECTED:uut_prototype_stubs.Str_Stop_Tract_Crosscheck1.rtu_autocal_active_bus[0].fAutoHeightSelectCalActive:0
TEST.EXPECTED:uut_prototype_stubs.Str_Stop_Tract_Crosscheck1.rtu_autocal_active_bus[0].fMainRaiseValveCalActive:0
TEST.EXPECTED:uut_prototype_stubs.Str_Stop_Tract_Crosscheck1.rtu_autocal_active_bus[0].fAccy3ThrotCalActive:0
TEST.EXPECTED:uut_prototype_stubs.Str_Stop_Tract_Crosscheck1.rty_StrStopTractCrossCk1:""
TEST.EXPECTED:uut_prototype_stubs.Str_Stop_Tract_Crosscheck1.rty_IsStopTractCrossCkDetect:""
TEST.EXPECTED:uut_prototype_stubs.Str_Fb_Crosscheck1.rtu_SteeringFdbkBus[0].SlvFdbk.rStrPos:3.0
TEST.EXPECTED:uut_prototype_stubs.Str_Fb_Crosscheck1.rtu_SteeringFdbkBus[0].SlvFdbk.rStrSpd:4.0
TEST.EXPECTED:uut_prototype_stubs.Str_Fb_Crosscheck1.rtu_SteeringFdbkBus[0].SlvFdbk.rStrCur:3.0
TEST.EXPECTED:uut_prototype_stubs.Str_Fb_Crosscheck1.rtu_SteeringFdbkBus[0].SlvFdbk.ulStopTrx:1
TEST.EXPECTED:uut_prototype_stubs.Str_Fb_Crosscheck1.rtu_SteeringFdbkBus[0].SlvFdbk.ulFreshCnt:2
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gSteer_sim_outputs.slMstrStrCmd:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gSteer_sim_outputs.slSlvStrCmd:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gSteer_sim_outputs.fSteeringEnabled:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gSteer_sim_outputs.rTfdCmd:0.0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gSteer_sim_outputs.slStrMtrSpdMax:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gSteer_sim_outputs.ulStrCmdFreshCntr:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gSteer_sim_outputs.ulStopSteering:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.HmsSim_Steering_DW.Delay_DSTATE:0.0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.HmsSim_Steering_DW.Model25_o1:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.HmsSim_Steering_DW.Model27_o1:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.HmsSim_Steering_DW.Model32:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.HmsSim_Steering_DW.Model33:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.HmsSim_Steering_DW.Model4_o1:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.HmsSim_Steering_DW.Model53:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gpSteering_fdbk[0].MstrFdbk.rStrPos:0.0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gpSteering_fdbk[0].MstrFdbk.rStrSpd:10.0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gpSteering_fdbk[0].MstrFdbk.rStrCur:6.0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gpSteering_fdbk[0].MstrFdbk.ulStopTrx:4
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gpSteering_fdbk[0].MstrFdbk.ulFreshCnt:2
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gpSteering_fdbk[0].SlvFdbk.rStrPos:3.0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gpSteering_fdbk[0].SlvFdbk.rStrSpd:4.0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gpSteering_fdbk[0].SlvFdbk.rStrCur:3.0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gpSteering_fdbk[0].SlvFdbk.ulStopTrx:1
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gpSteering_fdbk[0].SlvFdbk.ulFreshCnt:2
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gpFeatureEnabledEvents[0].ulEventsEnabled[0]:1
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gpFeatureEnabledEvents[0].fAnyEventActive:1
TEST.END

-- Subprogram: HmsSim_Steering_Init

-- Test Case: HmsSim_Steering_Init.001
TEST.UNIT:HmsSim_Steering
TEST.SUBPROGRAM:HmsSim_Steering_Init
TEST.NEW
TEST.NAME:HmsSim_Steering_Init.001
TEST.COMPOUND_ONLY
TEST.STUB:HmsSim_Steering.HmsSim_Steering_Start
TEST.STUB:HmsSim_Steering.HmsSim_Steering
TEST.STUB:HmsSim_Steering.HmsSim_Steering_initialize
TEST.VALUE:HmsSim_Steering.<<GLOBAL>>.gpSteering_fdbk:<<malloc 1>>
TEST.VALUE:HmsSim_Steering.<<GLOBAL>>.gpDem_control_limits:<<malloc 1>>
TEST.VALUE:HmsSim_Steering.<<GLOBAL>>.gpFeatureEnabledEvents:<<malloc 1>>
TEST.VALUE:HmsSim_Steering.<<GLOBAL>>.gpSteer_inputs:<<malloc 1>>
TEST.VALUE:HmsSim_Steering.<<GLOBAL>>.gpSensor_cals_c2m:<<malloc 1>>
TEST.VALUE:HmsSim_Steering.<<GLOBAL>>.gpSteer_dx_inputs:<<malloc 1>>
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gEvent_status_steer_bus.fStrSpCrosscheck1:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gEvent_status_steer_bus.fStrSnsrMismatch:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gEvent_status_steer_bus.fStrStopTractCrosscheck1:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gEvent_status_steer_bus.fStrSpCalcCrosscheck1:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gEvent_status_steer_bus.fStrFbCrosscheck1:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gEvent_status_steer_bus.fTracSpdCrosscheck:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gSteer_dx_out_bus.ulU1StrSuperFreshCount:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gSteer_dx_out_bus.fStrU1OkCrossCk:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gSteer_dx_out_bus.fIsStopTractCrossCkDetect:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gSteer_dx_out_bus.fStrSnsrCrossCkDetect:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gSteer_dx_out_bus.rTempStrPos:0.0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gSteer_sim_outputs.slMstrStrCmd:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gSteer_sim_outputs.slSlvStrCmd:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gSteer_sim_outputs.fSteeringEnabled:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gSteer_sim_outputs.rTfdCmd:0.0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gSteer_sim_outputs.ulStrCmdFreshCntr:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gSteer_sim_outputs.ulStopSteering:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gSteer_m2m_outputs.fStopTrac:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gSteer_m2m_outputs.uwSdmPwrCmdMode:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gSteer_m2m_outputs.rWhAngTarget:0.0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gSteer_m2m_outputs.fStrCmdActive:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gSteer_m2m_outputs.fStrMtrMoving:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gSteer_m2m_outputs.eStrOpMode_State:STR_OP_NoState
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gSteer_m2m_outputs.fSteerOkClk:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gSteer_m2m_outputs.rTrxSpdLmtWhAngCmd:0.0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gSteer_m2m_outputs.fDuInRng:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gSteer_m2m_outputs.fStopSteering:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gSteer_m2m_outputs.slFwdRevSteer:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gSteer_m2m_outputs.slStrPos_HndlPos:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gSteer_m2m_outputs.rStrSpd_DiffCnts:0.0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gSteer_dx_out_bus_ulU1StrSuperFreshCount:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gSteer_dx_out_bus_fIsStopTractCrossCkDetect:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gSteer_dx_out_bus_fStrSnsrCrossCkDetect:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gSteer_dx_out_bus_fStrU1OkCrossCk:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gSteer_sp_calc_out.eCmdLmtMode:WALKIE
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gSteer_sp_calc_out.rHndlCmd1[0]:0.0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gSteer_sp_calc_out.slHndlAng1[0]:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gSteer_sp_calc_out.eStrOpMode_State:STR_OP_NoState
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.HmsSim_Steering_DW.Delay_DSTATE:0.0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.HmsSim_Steering_DW.Model25_o1:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.HmsSim_Steering_DW.Model27_o1:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.HmsSim_Steering_DW.Model32:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.HmsSim_Steering_DW.Model33:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.HmsSim_Steering_DW.Model4_o1:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.HmsSim_Steering_DW.Model53:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gEd_outputs.ulScmCmd:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gEd_outputs.ulHcmCmd:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gEd_outputs.ulEdDriversOn:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gEd_outputs.ulHdmCmd:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gEd_outputs.fEdClosed:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gEd_outputs.fEdWillDrop:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gEd_outputs.fPwrDrvAllowed:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gEd_outputs.uwFaultDetected:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gEd_outputs.eEdState:ED_NoState
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gEd_outputs.rBusVoltsStart:0.0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gEd_outputs.fNotActPwrDwn:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gTraction_sim_output.rBrake1Cmd:0.0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gTraction_sim_output.rBrake2Cmd:0.0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gTraction_sim_output.rBrake3Cmd:0.0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gTraction_sim_output.rVehicleSpeed:0.0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gTraction_sim_output.rSpeedCommand:0.0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gTraction_sim_output.rDistanceTraveled:0.0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gTraction_sim_output.ulFwdSwitch:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gTraction_sim_output.ulRevSwitch:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gTraction_sim_output.fBrake_Coils_In_Lockout:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gTraction_sim_output.eTractionState:TRACT_STATE_PARK
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gTraction_sim_output.ulTravelAlarmCmd:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gTraction_sim_output.eTractionMotorState:TRACT_MOTOR_STATE_MOTORING
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gTraction_sim_output.fTrxAppClock:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gTraction_sim_output.fSlippageActiveStat:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gTraction_sim_output.rAccelCommanded:0.0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gTraction_sim_output.rAccelMeasured:0.0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gTraction_sim_output.rAccelCalculated:0.0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gTraction_sim_output.rX_Speed:0.0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gTraction_sim_output.slDirectionFlipper:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gTraction_sim_output.fStatusWordOK:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gTraction_sim_output.fSpeedCutbackSRO:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gTraction_sim_output.ulStrobeCmd:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gTraction_sim_output.rBrake1TorqueGap:0.0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gTraction_sim_output.rBrake2TorqueGap:0.0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gOperator_interface_output.fTruckAttended:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gOperator_interface_output.fTrxRun:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gOperator_interface_output.fHydRun1:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gOperator_interface_output.fHydRun2:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gOperator_interface_output.fHydRun3:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gOperator_interface_output.fBrake:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gOperator_interface_output.fBrkTstComp:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gOperator_interface_output.fPdlTstComp:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gOperator_interface_output.fTrxTieDownActive:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gOperator_interface_output.fHyd1TieDownActive:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gOperator_interface_output.fHyd2TieDownActive:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gOperator_interface_output.fZoneChange:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gOperator_interface_output.fCmd1:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gOperator_interface_output.fCmd2:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gOperator_interface_output.eCmdLmtMode:WALKIE
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gOperator_interface_output.fEML:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gOperator_interface_output.fTowMode_Attended:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gAutoCalActive.fMainHoistThrotCalActive:0
TEST.END

-- Subprogram: HmsSim_Steering_Start

-- Test Case: HmsSim_Steering_Start.001
TEST.UNIT:HmsSim_Steering
TEST.SUBPROGRAM:HmsSim_Steering_Start
TEST.NEW
TEST.NAME:HmsSim_Steering_Start.001
TEST.COMPOUND_ONLY
TEST.STUB:HmsSim_Steering.HmsSim_Steering_Init
TEST.STUB:HmsSim_Steering.HmsSim_Steering
TEST.STUB:HmsSim_Steering.HmsSim_Steering_initialize
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gEvent_status_steer_bus.fStrSpCrosscheck1:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gEvent_status_steer_bus.fStrSnsrMismatch:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gEvent_status_steer_bus.fStrStopTractCrosscheck1:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gEvent_status_steer_bus.fStrSpCalcCrosscheck1:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gEvent_status_steer_bus.fStrFbCrosscheck1:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gEvent_status_steer_bus.fTracSpdCrosscheck:0
TEST.END

-- Subprogram: HmsSim_Steering_initialize

-- Test Case: HmsSim_Steering_initialize.001
TEST.UNIT:HmsSim_Steering
TEST.SUBPROGRAM:HmsSim_Steering_initialize
TEST.NEW
TEST.NAME:HmsSim_Steering_initialize.001
TEST.COMPOUND_ONLY
TEST.STUB:HmsSim_Steering.HmsSim_Steering_Init
TEST.STUB:HmsSim_Steering.HmsSim_Steering_Start
TEST.STUB:HmsSim_Steering.HmsSim_Steering
TEST.STUB:HmsSim_SteeringBuild.HmsSim_SteeringBuild_step
TEST.STUB:HmsSim_SteeringBuild.HmsSim_SteeringBuild_initialize
TEST.STUB:HmsSim_SteeringBuild.HmsSim_SteeringBuild_terminate
TEST.STUB:SteeringSIM.SteeringSIM_Init
TEST.STUB:SteeringSIM.SteeringSIM
TEST.STUB:SteeringSIM.SteeringSIM_initialize
TEST.VALUE:uut_prototype_stubs.Str_Sp_Calc_Crosscheck1.rtu_steer_dx_inputs_bus[0].ulU2StrSuperFreshCount:0
TEST.VALUE:uut_prototype_stubs.Str_Sp_Calc_Crosscheck1.rtu_steer_dx_inputs_bus[0].ulU2StrHndCdeFreshCount:0
TEST.VALUE:uut_prototype_stubs.Str_Sp_Calc_Crosscheck1.rtu_steer_dx_inputs_bus[0].rSpRngUprBound:0.0
TEST.VALUE:uut_prototype_stubs.Str_Sp_Calc_Crosscheck1.rtu_steer_dx_inputs_bus[0].rSpRngLwrBound:0.0
TEST.EXPECTED:HmsSim_SteeringBuild.<<GLOBAL>>.gEvent_status_steer_bus.fStrSpCrosscheck1:0
TEST.EXPECTED:HmsSim_SteeringBuild.<<GLOBAL>>.gEvent_status_steer_bus.fStrSnsrMismatch:0
TEST.EXPECTED:HmsSim_SteeringBuild.<<GLOBAL>>.gEvent_status_steer_bus.fStrStopTractCrosscheck1:0
TEST.EXPECTED:HmsSim_SteeringBuild.<<GLOBAL>>.gEvent_status_steer_bus.fStrSpCalcCrosscheck1:0
TEST.EXPECTED:HmsSim_SteeringBuild.<<GLOBAL>>.gEvent_status_steer_bus.fStrFbCrosscheck1:0
TEST.EXPECTED:HmsSim_SteeringBuild.<<GLOBAL>>.gEvent_status_steer_bus.fTracSpdCrosscheck:0
TEST.EXPECTED:HmsSim_SteeringBuild.<<GLOBAL>>.gSteer_dx_out_bus.ulU1StrSuperFreshCount:0
TEST.EXPECTED:HmsSim_SteeringBuild.<<GLOBAL>>.gSteer_dx_out_bus.fStrU1OkCrossCk:0
TEST.EXPECTED:HmsSim_SteeringBuild.<<GLOBAL>>.gSteer_dx_out_bus.fIsStopTractCrossCkDetect:0
TEST.EXPECTED:HmsSim_SteeringBuild.<<GLOBAL>>.gSteer_dx_out_bus.fStrSnsrCrossCkDetect:0
TEST.EXPECTED:HmsSim_SteeringBuild.<<GLOBAL>>.gSteer_dx_out_bus.rTempStrPos:0.0
TEST.END

-- Unit: HmsSim_SteeringBuild

-- Subprogram: HmsSim_SteeringBuild_initialize

-- Test Case: HmsSim_SteeringBuild_initialize.001
TEST.UNIT:HmsSim_SteeringBuild
TEST.SUBPROGRAM:HmsSim_SteeringBuild_initialize
TEST.NEW
TEST.NAME:HmsSim_SteeringBuild_initialize.001
TEST.COMPOUND_ONLY
TEST.STUB:HmsSim_SteeringBuild.HmsSim_SteeringBuild_step
TEST.STUB:HmsSim_SteeringBuild.HmsSim_SteeringBuild_terminate
TEST.EXPECTED:HmsSim_SteeringBuild.<<GLOBAL>>.gEvent_status_steer_bus.fStrSpCrosscheck1:0
TEST.EXPECTED:HmsSim_SteeringBuild.<<GLOBAL>>.gEvent_status_steer_bus.fStrSnsrMismatch:0
TEST.EXPECTED:HmsSim_SteeringBuild.<<GLOBAL>>.gEvent_status_steer_bus.fStrStopTractCrosscheck1:0
TEST.EXPECTED:HmsSim_SteeringBuild.<<GLOBAL>>.gEvent_status_steer_bus.fStrSpCalcCrosscheck1:0
TEST.EXPECTED:HmsSim_SteeringBuild.<<GLOBAL>>.gEvent_status_steer_bus.fStrFbCrosscheck1:0
TEST.EXPECTED:HmsSim_SteeringBuild.<<GLOBAL>>.gEvent_status_steer_bus.fTracSpdCrosscheck:0
TEST.EXPECTED:HmsSim_SteeringBuild.<<GLOBAL>>.gSteer_dx_out_bus.ulU1StrSuperFreshCount:0
TEST.EXPECTED:HmsSim_SteeringBuild.<<GLOBAL>>.gSteer_dx_out_bus.fStrU1OkCrossCk:0
TEST.EXPECTED:HmsSim_SteeringBuild.<<GLOBAL>>.gSteer_dx_out_bus.fIsStopTractCrossCkDetect:0
TEST.EXPECTED:HmsSim_SteeringBuild.<<GLOBAL>>.gSteer_dx_out_bus.fStrSnsrCrossCkDetect:0
TEST.EXPECTED:HmsSim_SteeringBuild.<<GLOBAL>>.gSteer_dx_out_bus.rTempStrPos:0.0
TEST.END

-- Subprogram: HmsSim_SteeringBuild_step

-- Test Case: HmsSim_SteeringBuild_step.001
TEST.UNIT:HmsSim_SteeringBuild
TEST.SUBPROGRAM:HmsSim_SteeringBuild_step
TEST.NEW
TEST.NAME:HmsSim_SteeringBuild_step.001
TEST.COMPOUND_ONLY
TEST.VALUE:StrMstrSup.<<GLOBAL>>.gpSteer_params:<<malloc 1>>
TEST.VALUE:StrMstrSup.<<GLOBAL>>.gpFeatures_bus:<<malloc 1>>
TEST.VALUE:StrPos_SahNmbns.<<GLOBAL>>.gpTraction_performance:<<malloc 1>>
TEST.VALUE:StrPos_SahNmbns.<<GLOBAL>>.gpSteer_params:<<malloc 1>>
TEST.VALUE:StrPos_SetPntRateLmtr.<<GLOBAL>>.gpSteer_params:<<malloc 1>>
TEST.VALUE:Til2DuScaling.<<GLOBAL>>.gpSteer_params:<<malloc 1>>
TEST.VALUE:WhAngErr_StrMtrSpdLmt.<<GLOBAL>>.gpSteer_params:<<malloc 1>>
TEST.VALUE:WhAng_StrMtrSpdLmt.<<GLOBAL>>.StrWhAngLmtComponent_rStrMtrSpdWhAngRf:0.0
TEST.VALUE:WhAng_StrMtrSpdLmt.<<GLOBAL>>.gpSteer_params:<<malloc 1>>
TEST.VALUE:WhAng_StrMtrSpdLmt.<<GLOBAL>>.StrWhAngLmtComponent_rStrMtrSpdWhAngLutRf:0.0
TEST.VALUE:WhAng_StrMtrSpdLmt.<<GLOBAL>>.StrWhAngLmtComponent_rStrMtrSpdRedWhAngDelta:0.0
TEST.VALUE:WhAng_StrMtrSpdLmt.<<GLOBAL>>.StrWhAngLmtComponent_rStrMtrSpdWhAngRfUnclamped:0.0
TEST.VALUE:WhAng_StrMtrSpdLmt.<<GLOBAL>>.StrWhAngLmtComponent_rStrMtrSpdRedWhAngClamped:0.0
TEST.VALUE:WhAng_StrMtrSpdLmt.<<GLOBAL>>.StrWhAngLmtComponent_rStrMtrSpdWhAngTrigRf:0.0
TEST.VALUE:WhAng_StrMtrSpdLmt.<<GLOBAL>>.StrWhAngLmtComponent_fStrMtrSpdRedWhAngGTBP:0
TEST.EXPECTED:StrMstrSup.<<GLOBAL>>.StrMstSup_fStrAllowed:0
TEST.EXPECTED:StrMstrSup.<<GLOBAL>>.gpFeatures_bus[0].eHighSpeedLowerType:NO_HSL
TEST.EXPECTED:StrMstrSup.<<GLOBAL>>.gpFeatures_bus[0].eSteeringType:MANUAL
TEST.EXPECTED:StrMstrSup.<<GLOBAL>>.gpFeatures_bus[0].eSteerDirection:Forward
TEST.EXPECTED:StrMstrSup.<<GLOBAL>>.gpFeatures_bus[0].eAuxHoistType:NO_AUX
TEST.EXPECTED:StrMstrSup.<<GLOBAL>>.gpFeatures_bus[0].rTRUCK_WEIGHT:0.0
TEST.EXPECTED:StrMstrSup.<<GLOBAL>>.gpFeatures_bus[0].rCLPSD_HGT:0.0
TEST.EXPECTED:StrMstrSup.<<GLOBAL>>.gpFeatures_bus[0].rLIFT_HGT:0.0
TEST.EXPECTED:StrMstrSup.<<GLOBAL>>.gpFeatures_bus[0].eZONESW:ZONE_SW_NO
TEST.EXPECTED:StrMstrSup.<<GLOBAL>>.gpFeatures_bus[0].eRackSelect:RACK_SELECT_OFF
TEST.EXPECTED:StrMstrSup.<<GLOBAL>>.gpFeatures_bus[0].eUSeBatteryRestraintSwitch:USE_INPUT
TEST.EXPECTED:StrMstrSup.<<GLOBAL>>.gpFeatures_bus[0].eRegionType:AllRegions
TEST.EXPECTED:StrMstrSup.<<GLOBAL>>.gpFeatures_bus[0].eForkType:Telescopic
TEST.EXPECTED:StrMstrSup.<<GLOBAL>>.gpFeatures_bus[0].eUseTilt:Tilt_Not_Active
TEST.EXPECTED:StrMstrSup.<<GLOBAL>>.gpFeatures_bus[0].eTPA_Type:NO_TPA
TEST.EXPECTED:StrMstrSup.<<GLOBAL>>.gpFeatures_bus[0].eUseTiltSense:No_Tilt_Sense
TEST.EXPECTED:StrMstrSup.<<GLOBAL>>.gpFeatures_bus[0].eUseAccy1:Accy1_Not_Active
TEST.EXPECTED:StrMstrSup.<<GLOBAL>>.gpFeatures_bus[0].eUseAccy2:NO_Accy2
TEST.EXPECTED:StrMstrSup.<<GLOBAL>>.gpFeatures_bus[0].eUseAccy3:Accy3_Not_Active
TEST.EXPECTED:StrMstrSup.<<GLOBAL>>.gpFeatures_bus[0].eTravelAlarm:NO_ALARM
TEST.EXPECTED:StrMstrSup.<<GLOBAL>>.gpFeatures_bus[0].eTRK_TYPE:NO_TRUCK
TEST.EXPECTED:StrMstrSup.<<GLOBAL>>.gpFeatures_bus[0].eReachType:ReachNone
TEST.EXPECTED:StrPos_SahNmbns.<<GLOBAL>>.SahNmbns_slHndlPosOut:0
TEST.EXPECTED:StrPos_SahNmbns.<<GLOBAL>>.SahNmbns_rTrxSpdBp:0.0
TEST.EXPECTED:StrPos_SahNmbns.<<GLOBAL>>.SahNmbns_rTrxSpdMax:0.0
TEST.EXPECTED:StrPos_SahNmbns.<<GLOBAL>>.SahNmbns_rHndlPosNmbRng:0.0
TEST.EXPECTED:StrPos_SahNmbns.<<GLOBAL>>.SahNmbns_rHndlPosTransRng:0.0
TEST.EXPECTED:StrPos_SahNmbns.<<GLOBAL>>.SahNmbns_rHndlPosUnNmbd:0.0
TEST.EXPECTED:StrPos_SahNmbns.<<GLOBAL>>.SahNmbns_rTrxSpdInNmbRng:0
TEST.EXPECTED:StrPos_SahNmbns.<<GLOBAL>>.SahNmbns_eState:SahNmbns_INIT
TEST.EXPECTED:StrPos_SahNmbns.<<GLOBAL>>.SahNmbns_rHndlAngFromEndOfTransRng:0.0
TEST.EXPECTED:StrPos_SahNmbns.<<GLOBAL>>.SahNmbns_rHndlAngFrctFromEndOfTransRng:0.0
TEST.EXPECTED:StrPos_SahNmbns.<<GLOBAL>>.SahNmbns_rTrxSpdAboveBp:0.0
TEST.EXPECTED:StrPos_SahNmbns.<<GLOBAL>>.SahNmbns_rTrxSpdDeltaNmbRng:0.0
TEST.EXPECTED:StrPos_SahNmbns.<<GLOBAL>>.SahNmbns_rTrxSpdFrctOfNmbRng:0.0
TEST.EXPECTED:StrPos_SahNmbns.<<GLOBAL>>.SahNmbns_rTil2DuDeltaNmbRng:0.0
TEST.EXPECTED:StrPos_SahNmbns.<<GLOBAL>>.SahNmbns_rTil2DuDelta:0.0
TEST.EXPECTED:StrPos_SahNmbns.<<GLOBAL>>.SahNmbns_rTil2DuRatio:0.0
TEST.EXPECTED:StrPos_SahNmbns.<<GLOBAL>>.StrPos_SahNmbns_DW.is_active_c3_StrPos_SahNmbns:0
TEST.EXPECTED:StrPos_SahNmbns.<<GLOBAL>>.StrPos_SahNmbns_DW.is_c3_StrPos_SahNmbns:0
TEST.EXPECTED:StrPos_SetPntRateLmtr.<<GLOBAL>>.PosCmd_rFilterExp:0.0
TEST.EXPECTED:StrPos_SetPntRateLmtr.<<GLOBAL>>.PosCmd_rFilterExponential:0.0
TEST.EXPECTED:StrPos_SetPntRateLmtr.<<GLOBAL>>.PosCmd_rSpDiff:0.0
TEST.EXPECTED:StrPos_SetPntRateLmtr.<<GLOBAL>>.PosCmd_slSpLmtrOutPrev:0
TEST.EXPECTED:StrPos_SetPntRateLmtr.<<GLOBAL>>.PosCmd_slSpOffset:0
TEST.EXPECTED:StrPos_SetPntRateLmtr.<<GLOBAL>>.PosCmd_slSpDiffScaled:0
TEST.EXPECTED:StrPos_SetPntRateLmtr.<<GLOBAL>>.PosCmd_fLmtSetPnt:0
TEST.EXPECTED:StrPos_SetPntRateLmtr.<<GLOBAL>>.PosCmd_fTrxSpdGTETrxSpdLmt:0
TEST.EXPECTED:StrPos_SetPntRateLmtr.<<GLOBAL>>.PosCmd_fWhAngTrgtGTWhAng:0
TEST.EXPECTED:StrPos_SetPntRateLmtr.<<GLOBAL>>.StrPos_SetPntRateLmtr_DW.UnitDelay1_DSTATE:0
TEST.EXPECTED:Til2DuScaling.<<GLOBAL>>.TilScaling_rSfDelta:0.0
TEST.EXPECTED:Til2DuScaling.<<GLOBAL>>.TilScaling_rTrxSpdDelta:0.0
TEST.EXPECTED:Til2DuScaling.<<GLOBAL>>.TilScaling_rSfDelUnclamped:0.0
TEST.EXPECTED:Til2DuScaling.<<GLOBAL>>.TilScaling_fTrxSpdGTBP:0
TEST.EXPECTED:TrxSpd_StrMtrSpdLmt.<<GLOBAL>>.StrTrxSpdLmtComponent_rStrMtrSpdTrxSpdRf:0.0
TEST.EXPECTED:TrxSpd_StrMtrSpdLmt.<<GLOBAL>>.StrTrxSpdLmtComponent_rStrMtrSpdRedTrxSpdDelta:0.0
TEST.EXPECTED:TrxSpd_StrMtrSpdLmt.<<GLOBAL>>.StrTrxSpdLmtComponent_rStrMtrSpdTrxSpdRfUnclamped:0.0
TEST.EXPECTED:TrxSpd_StrMtrSpdLmt.<<GLOBAL>>.StrTrxSpdLmtComponent_fStrMtrSpdRedTrxSpdGTBP:0
TEST.EXPECTED:WhAngErr_StrMtrSpdLmt.<<GLOBAL>>.StrWhAngErrLmt_rWhAngSp:0.0
TEST.EXPECTED:WhAngErr_StrMtrSpdLmt.<<GLOBAL>>.StrWhAngErrLmt_rWhAngErr:0.0
TEST.EXPECTED:WhAngErr_StrMtrSpdLmt.<<GLOBAL>>.StrWhAngErrLmt_rStrMtrSpdWhAngErrRf:0.0
TEST.EXPECTED:WhAngErr_StrMtrSpdLmt.<<GLOBAL>>.StrWhAngErrLmt_rWhAngIn:0.0
TEST.EXPECTED:WhAngErr_StrMtrSpdLmt.<<GLOBAL>>.StrWhAngErrLmt_rWhAngErrDelta:0.0
TEST.EXPECTED:WhAngErr_StrMtrSpdLmt.<<GLOBAL>>.StrWhAngErrLmt_rRfMag:0.0
TEST.EXPECTED:WhAngErr_StrMtrSpdLmt.<<GLOBAL>>.StrWhAngErrLmt_rRfUnclamped:0.0
TEST.EXPECTED:WhAngErr_StrMtrSpdLmt.<<GLOBAL>>.StrWhAngErrLmt_fWhAngErrGtBp:0
TEST.EXPECTED:WhAng_StrMtrSpdLmt.<<GLOBAL>>.StrWhAngLmtComponent_rStrMtrSpdWhAngLutRf:0.0
TEST.EXPECTED:WhAng_StrMtrSpdLmt.<<GLOBAL>>.StrWhAngLmtComponent_rStrMtrSpdRedWhAngDelta:0.0
TEST.EXPECTED:WhAng_StrMtrSpdLmt.<<GLOBAL>>.StrWhAngLmtComponent_rStrMtrSpdWhAngRfUnclamped:0.0
TEST.EXPECTED:WhAng_StrMtrSpdLmt.<<GLOBAL>>.StrWhAngLmtComponent_rStrMtrSpdRedWhAngClamped:0.0
TEST.EXPECTED:WhAng_StrMtrSpdLmt.<<GLOBAL>>.StrWhAngLmtComponent_rStrMtrSpdWhAngTrigRf:0.0
TEST.EXPECTED:WhAng_StrMtrSpdLmt.<<GLOBAL>>.StrWhAngLmtComponent_fStrMtrSpdRedWhAngGTBP:0
TEST.END

-- Subprogram: HmsSim_SteeringBuild_terminate

-- Test Case: HmsSim_SteeringBuild_terminate.001
TEST.UNIT:HmsSim_SteeringBuild
TEST.SUBPROGRAM:HmsSim_SteeringBuild_terminate
TEST.NEW
TEST.NAME:HmsSim_SteeringBuild_terminate.001
TEST.COMPOUND_ONLY
TEST.STUB:HmsSim_SteeringBuild.HmsSim_SteeringBuild_step
TEST.STUB:HmsSim_SteeringBuild.HmsSim_SteeringBuild_initialize
TEST.EXPECTED:HmsSim_SteeringBuild.<<GLOBAL>>.gEvent_status_steer_bus.fStrSpCrosscheck1:0
TEST.EXPECTED:HmsSim_SteeringBuild.<<GLOBAL>>.gEvent_status_steer_bus.fStrSnsrMismatch:0
TEST.EXPECTED:HmsSim_SteeringBuild.<<GLOBAL>>.gEvent_status_steer_bus.fStrStopTractCrosscheck1:0
TEST.EXPECTED:HmsSim_SteeringBuild.<<GLOBAL>>.gEvent_status_steer_bus.fStrSpCalcCrosscheck1:0
TEST.EXPECTED:HmsSim_SteeringBuild.<<GLOBAL>>.gEvent_status_steer_bus.fStrFbCrosscheck1:0
TEST.EXPECTED:HmsSim_SteeringBuild.<<GLOBAL>>.gEvent_status_steer_bus.fTracSpdCrosscheck:0
TEST.EXPECTED:HmsSim_SteeringBuild.<<GLOBAL>>.gSteer_dx_out_bus.ulU1StrSuperFreshCount:0
TEST.EXPECTED:HmsSim_SteeringBuild.<<GLOBAL>>.gSteer_dx_out_bus.fStrU1OkCrossCk:0
TEST.EXPECTED:HmsSim_SteeringBuild.<<GLOBAL>>.gSteer_dx_out_bus.fIsStopTractCrossCkDetect:0
TEST.EXPECTED:HmsSim_SteeringBuild.<<GLOBAL>>.gSteer_dx_out_bus.fStrSnsrCrossCkDetect:0
TEST.EXPECTED:HmsSim_SteeringBuild.<<GLOBAL>>.gSteer_dx_out_bus.rTempStrPos:0.0
TEST.END

-- Unit: PosCmd

-- Subprogram: PosCmd

-- Test Case: PosCmd.001
TEST.UNIT:PosCmd
TEST.SUBPROGRAM:PosCmd
TEST.NEW
TEST.NAME:PosCmd.001
TEST.COMPOUND_ONLY
TEST.STUB:Cmd_Pos_Integrator.Cmd_Pos_Integrator_CompStdDev
TEST.STUB:Cmd_Pos_Integrator.Cmd_Pos_Integrator_initialize
TEST.STUB:StrPosWhAngLimiter.StrPosWhAngLimiter_Init
TEST.STUB:StrPosWhAngLimiter.StrPosWhAngLimiter
TEST.STUB:StrPos_SahNmbns.StrPos_SahNmbns_HndlInNmbnsRng
TEST.STUB:StrPos_SahNmbns.StrPos_SahNmbns_Init
TEST.STUB:StrPos_SahNmbns.StrPos_SahNmbns_initialize
TEST.STUB:StrPos_SetPntRateLmtr.StrPos_SetPntRateLmtr
TEST.STUB:StrPos_SetPntRateLmtr.StrPos_SetPntRateLmt_initialize
TEST.STUB:WhAng_StrMtrSpdLmt.WhAng_StrMtrSpdLmt_Init
TEST.STUB:WhAng_StrMtrSpdLmt.WhAng_StrMtrSpdLmt
TEST.STUB:WhAng_StrMtrSpdLmt.WhAng_StrMtrSpdLmt_initialize
TEST.VALUE:uut_prototype_stubs.LookUp_S32_S32.pY[0]:0
TEST.VALUE:Cmd_Pos_Integrator.Cmd_Pos_Integrator_CompStdDev.Samples:0
TEST.VALUE:Cmd_Pos_Integrator.Cmd_Pos_Integrator_CompStdDev.return:0.0
TEST.VALUE:HmsSim_Steering.<<GLOBAL>>.gSteer_dx_out_bus_fStrU1OkCrossCk:1
TEST.VALUE:HmsSim_Steering.<<GLOBAL>>.HmsSim_Steering_DW.Model27_o1:0
TEST.VALUE:HmsSim_Steering.<<GLOBAL>>.gpSteering_fdbk:<<malloc 1>>
TEST.VALUE:HmsSim_Steering.<<GLOBAL>>.gpSteering_fdbk[0].MstrFdbk.rStrPos:0.0
TEST.VALUE:HmsSim_Steering.<<GLOBAL>>.gpSteering_fdbk[0].MstrFdbk.rStrSpd:0.0
TEST.VALUE:HmsSim_Steering.<<GLOBAL>>.gpSteering_fdbk[0].MstrFdbk.rStrCur:0.0
TEST.VALUE:HmsSim_Steering.<<GLOBAL>>.gpSteering_fdbk[0].MstrFdbk.ulStopTrx:0
TEST.VALUE:HmsSim_Steering.<<GLOBAL>>.gpSteering_fdbk[0].MstrFdbk.ulFreshCnt:0
TEST.VALUE:HmsSim_Steering.<<GLOBAL>>.gpDem_control_limits:<<malloc 1>>
TEST.VALUE:PosCmd.<<GLOBAL>>.StrPos_rTrxSpd:10.0
TEST.VALUE:PosCmd.<<GLOBAL>>.StrPosCmd_slStrPosWQPClipped:0
TEST.VALUE:PosCmd.<<GLOBAL>>.gpSteer_params:<<malloc 1>>
TEST.VALUE:PosCmd.<<GLOBAL>>.StrPosCmd_rTfdCurrentSp:1.0
TEST.VALUE:PosCmd.<<GLOBAL>>.PosCmd_rWhAngTrgtRaw:1.0
TEST.VALUE:PosCmd.<<GLOBAL>>.SahNmbns_slHndlPosOut:0
TEST.VALUE:PosCmd.<<GLOBAL>>.StrPosCmd_slStrPosCmdModeClipped:0
TEST.VALUE:PosCmd.<<GLOBAL>>.StrPosCmd_slStrPosStopZoneClipped:0
TEST.VALUE:PosCmd.<<GLOBAL>>.StrPosCmd_slWhAngLmt_TrxSpd:0
TEST.VALUE:PosCmd.<<GLOBAL>>.StrPosCmd_slPrevCmd:0
TEST.VALUE:PosCmd.<<GLOBAL>>.StrPosCmd_slStrPosCmd:0
TEST.VALUE:PosCmd.<<GLOBAL>>.StrPosCmd_slStrPosCmdTrxSpdClipped:0
TEST.VALUE:PosCmd.<<GLOBAL>>.StrPosSroDetect_ePrevCmdLmtMode:0
TEST.VALUE:PosCmd.<<GLOBAL>>.StrPosSroDetect_fRbtTrtlChange:0
TEST.VALUE:PosCmd.<<GLOBAL>>.StrPosCmd_fSteppedOff:0
TEST.VALUE:PosCmd.<<GLOBAL>>.StrPosCmd_fPrevRbtTrtlSwitchSro:0
TEST.VALUE:PosCmd.<<GLOBAL>>.StrPosSroDetect_fPrevHss:0
TEST.VALUE:PosCmd.<<GLOBAL>>.StrPosSroDetect_fWhAngGT40DegOorSro:0
TEST.VALUE:PosCmd.<<GLOBAL>>.StrPosSroDetect_fWhAngCmdGT45Deg:0
TEST.VALUE:PosCmd.<<GLOBAL>>.StrPosSroDetect_fWhAngCmdGT3Deg:0
TEST.VALUE:PosCmd.<<GLOBAL>>.StrPosSroDetect_fWhAngGT3Deg:0
TEST.VALUE:PosCmd.<<GLOBAL>>.StrPosSroDetect_fWhAngErrorGT3Deg:0
TEST.VALUE:PosCmd.<<GLOBAL>>.StrPosSroDetect_fTrxSpdGTZero:0
TEST.VALUE:PosCmd.<<GLOBAL>>.PosCmd_DW.Memory_PreviousInput:1
TEST.VALUE:PosCmd.<<GLOBAL>>.PosCmd_DW.UnitDelay2_DSTATE:1
TEST.VALUE:PosCmd.<<GLOBAL>>.PosCmd_DW.Model1_o3:1
TEST.VALUE:PosCmd.<<GLOBAL>>.PosCmd_DW.UnitDelay1_DSTATE:1
TEST.VALUE:PosCmd.<<GLOBAL>>.gpTraction_performance:<<malloc 1>>
TEST.VALUE:PosCmd.<<GLOBAL>>.gpPerformance_bus:<<malloc 1>>
TEST.VALUE:PosCmd.PosCmd.rtu_StrCmd:3.0
TEST.VALUE:PosCmd.PosCmd.rtu_Op_int_output_bus:<<malloc 1>>
TEST.VALUE:PosCmd.PosCmd.rtu_Op_int_output_bus[0].eCmdLmtMode:NORMAL
TEST.VALUE:PosCmd.PosCmd.rtu_ForkLoad:1.0
TEST.VALUE:PosCmd.PosCmd.rtu_gpSensor_cals_c2m:<<malloc 1>>
TEST.VALUE:PosCmd.PosCmd.rtu_gpSensor_cals_c2m[0].slDuAng[2]:9
TEST.VALUE:PosCmd.PosCmd.rtu_gpSensor_cals_c2m[0].rMainHeightMax:1.0
TEST.VALUE:PosCmd.PosCmd.rtu_gpSensor_cals_c2m[0].rCustomHeight:0.0
TEST.VALUE:PosCmd.PosCmd.rty_Str_SP_Out:<<malloc 1>>
TEST.VALUE:StrPos_SahNmbns.<<GLOBAL>>.gpTraction_performance:<<malloc 1>>
TEST.VALUE:StrPos_SahNmbns.<<GLOBAL>>.StrPos_SahNmbns_DW.is_active_c3_StrPos_SahNmbns:1
TEST.EXPECTED:uut_prototype_stubs.LookUp_S32_real32_T.pY[0]:0
TEST.EXPECTED:uut_prototype_stubs.LookUp_S32_real32_T.u:3.0
TEST.EXPECTED:uut_prototype_stubs.LookUp_S32_real32_T.iHi:2
TEST.EXPECTED:uut_prototype_stubs.LookUp_S32_S32.pY[0]:0
TEST.EXPECTED:uut_prototype_stubs.LookUp_S32_S32.pYData[0]:0
TEST.EXPECTED:uut_prototype_stubs.LookUp_S32_S32.pUData[0]:0
TEST.EXPECTED:uut_prototype_stubs.LookUp_S32_S32.iHi:2
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gSteer_dx_out_bus_fStrU1OkCrossCk:1
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.HmsSim_Steering_DW.Model27_o1:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gpSteering_fdbk[0].MstrFdbk.rStrPos:0.0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gpSteering_fdbk[0].MstrFdbk.rStrSpd:0.0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gpSteering_fdbk[0].MstrFdbk.rStrCur:0.0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gpSteering_fdbk[0].MstrFdbk.ulStopTrx:0
TEST.EXPECTED:HmsSim_Steering.<<GLOBAL>>.gpSteering_fdbk[0].MstrFdbk.ulFreshCnt:0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.StrPos_rTrxSpd:10.0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.StrPosSroDetect_fHss:0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.StrPosCmd_rTrxSpdLmt_WhAngCmd:0.0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.StrPos_rWhAngTrgt:0.0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.StrPosCmd_slHandlePos:0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.StrPosCmd_slStrPosWQPClipped:0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.PosCmd_fHndlRngSro:0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.StrPosSroDetect_fPreventRbtTrtlSw:0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.PosCmd_fRbtTrtlSwitchSro:0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.StrPosCmd_fCmdActive:1
TEST.EXPECTED:PosCmd.<<GLOBAL>>.StrPosCmd_eTfdOpModeState:TFD_OP_MODE_Init
TEST.EXPECTED:PosCmd.<<GLOBAL>>.StrPosCmd_eTfdOvrState:TFD_OVR_Init
TEST.EXPECTED:PosCmd.<<GLOBAL>>.StrPosCmd_eCmdLmtNmbns_State:CmdLmtNmbns_NoTrxRedux
TEST.EXPECTED:PosCmd.PosCmd.rtu_Op_int_output_bus[0].fTruckAttended:0
TEST.EXPECTED:PosCmd.PosCmd.rtu_Op_int_output_bus[0].fTrxRun:0
TEST.EXPECTED:PosCmd.PosCmd.rtu_Op_int_output_bus[0].fHydRun1:0
TEST.EXPECTED:PosCmd.PosCmd.rtu_Op_int_output_bus[0].fHydRun2:0
TEST.EXPECTED:PosCmd.PosCmd.rtu_Op_int_output_bus[0].fHydRun3:0
TEST.EXPECTED:PosCmd.PosCmd.rtu_Op_int_output_bus[0].fBrake:0
TEST.EXPECTED:PosCmd.PosCmd.rtu_Op_int_output_bus[0].fBrkTstComp:0
TEST.EXPECTED:PosCmd.PosCmd.rtu_Op_int_output_bus[0].fPdlTstComp:0
TEST.EXPECTED:PosCmd.PosCmd.rtu_Op_int_output_bus[0].fTrxTieDownActive:0
TEST.EXPECTED:PosCmd.PosCmd.rtu_Op_int_output_bus[0].fHyd1TieDownActive:0
TEST.EXPECTED:PosCmd.PosCmd.rtu_Op_int_output_bus[0].fHyd2TieDownActive:0
TEST.EXPECTED:PosCmd.PosCmd.rtu_Op_int_output_bus[0].fZoneChange:0
TEST.EXPECTED:PosCmd.PosCmd.rtu_Op_int_output_bus[0].fCmd1:0
TEST.EXPECTED:PosCmd.PosCmd.rtu_Op_int_output_bus[0].fCmd2:0
TEST.EXPECTED:PosCmd.PosCmd.rtu_Op_int_output_bus[0].eCmdLmtMode:NORMAL
TEST.EXPECTED:PosCmd.PosCmd.rtu_Op_int_output_bus[0].fEML:0
TEST.EXPECTED:PosCmd.PosCmd.rtu_Op_int_output_bus[0].fTowMode_Attended:0
TEST.EXPECTED:PosCmd.PosCmd.rtu_ForkLoad:1.0
TEST.EXPECTED:PosCmd.PosCmd.rtu_ThrottleEnable:0
TEST.EXPECTED:PosCmd.PosCmd.rtu_gpSensor_cals_c2m[0].rSecHgtSensorCalGain:0.0
TEST.EXPECTED:PosCmd.PosCmd.rtu_gpSensor_cals_c2m[0].rPriHgtSensorCalGain:0.0
TEST.EXPECTED:PosCmd.PosCmd.rtu_gpSensor_cals_c2m[0].rPrimaryCalCountAtMax:0.0
TEST.EXPECTED:PosCmd.PosCmd.rtu_gpSensor_cals_c2m[0].rAuxHtSensorCalGain:0.0
TEST.EXPECTED:PosCmd.PosCmd.rtu_gpSensor_cals_c2m[0].rReachSensorMinMax[0..1]:0.0
TEST.EXPECTED:PosCmd.PosCmd.rtu_gpSensor_cals_c2m[0].rTiltSensorMinZeroMax[0..1]:0.0
TEST.EXPECTED:PosCmd.PosCmd.rtu_gpSensor_cals_c2m[0].rTiltSensorLevel:0.0
TEST.EXPECTED:PosCmd.PosCmd.rtu_gpSensor_cals_c2m[0].rPivotSensorCalGain:0.0
TEST.EXPECTED:PosCmd.PosCmd.rtu_gpSensor_cals_c2m[0].rTraverseSensorCalGain:0.0
TEST.EXPECTED:PosCmd.PosCmd.rtu_gpSensor_cals_c2m[0].rLSOffset_Primary:0.0
TEST.EXPECTED:PosCmd.PosCmd.rtu_gpSensor_cals_c2m[0].rLSOffset_Secondary:0.0
TEST.EXPECTED:PosCmd.PosCmd.rtu_gpSensor_cals_c2m[0].rLSOffset_Aux_cyl:0.0
TEST.EXPECTED:PosCmd.PosCmd.rtu_gpSensor_cals_c2m[0].rAccelXoffset:0.0
TEST.EXPECTED:PosCmd.PosCmd.rtu_gpSensor_cals_c2m[0].rAccelZoffset:0.0
TEST.EXPECTED:PosCmd.PosCmd.rtu_gpSensor_cals_c2m[0].slDuAng[2]:9
TEST.EXPECTED:PosCmd.PosCmd.rtu_gpSensor_cals_c2m[0].rMainHeightMax:1.0
TEST.EXPECTED:PosCmd.PosCmd.rtu_gpSensor_cals_c2m[0].rCustomHeight:0.0
TEST.EXPECTED:PosCmd.PosCmd.rty_Str_SP_Out[0]:1
TEST.EXPECTED:StrPosWhAngLimiter.StrPosWhAngLimiter.rtu_AngLmtMode:NORMAL
TEST.EXPECTED:StrPos_SahNmbns.<<GLOBAL>>.SahNmbns_slHndlPosOut:0
TEST.EXPECTED:StrPos_SahNmbns.<<GLOBAL>>.SahNmbns_rTrxSpdBp:0.0
TEST.EXPECTED:StrPos_SahNmbns.<<GLOBAL>>.SahNmbns_rTrxSpdMax:0.0
TEST.EXPECTED:StrPos_SahNmbns.<<GLOBAL>>.SahNmbns_rHndlPosNmbRng:0.0
TEST.EXPECTED:StrPos_SahNmbns.<<GLOBAL>>.SahNmbns_rHndlPosTransRng:0.0
TEST.EXPECTED:StrPos_SahNmbns.<<GLOBAL>>.SahNmbns_rHndlPosUnNmbd:0.0
TEST.EXPECTED:StrPos_SahNmbns.<<GLOBAL>>.SahNmbns_rTrxSpdInNmbRng:1
TEST.EXPECTED:StrPos_SahNmbns.<<GLOBAL>>.SahNmbns_eState:SahNmbns_WAIT_FOR_HNDL_IN_RNG
TEST.EXPECTED:StrPos_SahNmbns.<<GLOBAL>>.SahNmbns_rHndlAngFromEndOfTransRng:0.0
TEST.EXPECTED:StrPos_SahNmbns.<<GLOBAL>>.SahNmbns_rHndlAngFrctFromEndOfTransRng:0.0
TEST.EXPECTED:StrPos_SahNmbns.<<GLOBAL>>.SahNmbns_rTrxSpdAboveBp:0.0
TEST.EXPECTED:StrPos_SahNmbns.<<GLOBAL>>.SahNmbns_rTrxSpdDeltaNmbRng:0.0
TEST.EXPECTED:StrPos_SahNmbns.<<GLOBAL>>.SahNmbns_rTrxSpdFrctOfNmbRng:0.0
TEST.EXPECTED:StrPos_SahNmbns.<<GLOBAL>>.SahNmbns_rTil2DuDeltaNmbRng:0.0
TEST.EXPECTED:StrPos_SahNmbns.<<GLOBAL>>.SahNmbns_rTil2DuDelta:0.0
TEST.EXPECTED:StrPos_SahNmbns.<<GLOBAL>>.SahNmbns_rTil2DuRatio:0.0
TEST.EXPECTED:StrPos_SahNmbns.<<GLOBAL>>.StrPos_SahNmbns_DW.is_active_c3_StrPos_SahNmbns:1
TEST.EXPECTED:StrPos_SahNmbns.<<GLOBAL>>.StrPos_SahNmbns_DW.is_c3_StrPos_SahNmbns:1
TEST.END

-- Subprogram: PosCmd_ForceMonoinc

-- Test Case: PosCmd_ForceMonoinc.001
TEST.UNIT:PosCmd
TEST.SUBPROGRAM:PosCmd_ForceMonoinc
TEST.NEW
TEST.NAME:PosCmd_ForceMonoinc.001
TEST.COMPOUND_ONLY
TEST.EXPECTED:PosCmd.PosCmd_ForceMonoinc.rty_y[0]:0.0
TEST.EXPECTED:PosCmd.PosCmd_ForceMonoinc.rty_y[1]:0.01
TEST.EXPECTED:PosCmd.PosCmd_ForceMonoinc.rty_y[2]:0.02
TEST.END

-- Subprogram: PosCmd_ForceMonoinc1

-- Test Case: PosCmd_ForceMonoinc1.001
TEST.UNIT:PosCmd
TEST.SUBPROGRAM:PosCmd_ForceMonoinc1
TEST.NEW
TEST.NAME:PosCmd_ForceMonoinc1.001
TEST.COMPOUND_ONLY
TEST.VALUE:PosCmd.PosCmd_ForceMonoinc1.rtu_u[0]:2
TEST.VALUE:PosCmd.PosCmd_ForceMonoinc1.rtu_u[1]:1
TEST.VALUE:PosCmd.PosCmd_ForceMonoinc1.rtu_u[2]:0
TEST.VALUE:PosCmd.PosCmd_ForceMonoinc1.rty_y[0]:2
TEST.VALUE:PosCmd.PosCmd_ForceMonoinc1.rty_y[1]:3
TEST.VALUE:PosCmd.PosCmd_ForceMonoinc1.rty_y[2]:4
TEST.EXPECTED:PosCmd.PosCmd_ForceMonoinc1.rtu_u[0]:2
TEST.EXPECTED:PosCmd.PosCmd_ForceMonoinc1.rtu_u[1]:1
TEST.EXPECTED:PosCmd.PosCmd_ForceMonoinc1.rtu_u[2]:0
TEST.EXPECTED:PosCmd.PosCmd_ForceMonoinc1.rty_y[0]:2
TEST.EXPECTED:PosCmd.PosCmd_ForceMonoinc1.rty_y[1]:3
TEST.EXPECTED:PosCmd.PosCmd_ForceMonoinc1.rty_y[2]:4
TEST.END

-- Subprogram: PosCmd_FwdLdd

-- Test Case: PosCmd_FwdLdd.001
TEST.UNIT:PosCmd
TEST.SUBPROGRAM:PosCmd_FwdLdd
TEST.NEW
TEST.NAME:PosCmd_FwdLdd.001
TEST.COMPOUND_ONLY
TEST.EXPECTED:PosCmd.PosCmd_FwdLdd.rty_Out1[0]:0.0
TEST.END

-- Subprogram: PosCmd_FwdLdd_a

-- Test Case: PosCmd_FwdLdd_a.001
TEST.UNIT:PosCmd
TEST.SUBPROGRAM:PosCmd_FwdLdd_a
TEST.NEW
TEST.NAME:PosCmd_FwdLdd_a.001
TEST.COMPOUND_ONLY
TEST.EXPECTED:PosCmd.PosCmd_FwdLdd_a.rty_Out1[0]:0.0
TEST.END

-- Subprogram: PosCmd_FwdLdd_j

-- Test Case: PosCmd_FwdLdd_j.001
TEST.UNIT:PosCmd
TEST.SUBPROGRAM:PosCmd_FwdLdd_j
TEST.NEW
TEST.NAME:PosCmd_FwdLdd_j.001
TEST.COMPOUND_ONLY
TEST.VALUE:PosCmd.PosCmd_FwdLdd_j.rty_Out1[0..2]:0.0
TEST.EXPECTED:PosCmd.PosCmd_FwdLdd_j.rty_Out1[0..2]:0.0
TEST.END

-- Subprogram: PosCmd_FwdLdd_m

-- Test Case: PosCmd_FwdLdd_m.001
TEST.UNIT:PosCmd
TEST.SUBPROGRAM:PosCmd_FwdLdd_m
TEST.NEW
TEST.NAME:PosCmd_FwdLdd_m.001
TEST.COMPOUND_ONLY
TEST.VALUE:PosCmd.PosCmd_FwdLdd_m.rty_Out1[0..2]:0.0
TEST.EXPECTED:PosCmd.PosCmd_FwdLdd_m.rty_Out1[0..2]:0.0
TEST.END

-- Subprogram: PosCmd_FwdLdd_n

-- Test Case: PosCmd_FwdLdd_n.001
TEST.UNIT:PosCmd
TEST.SUBPROGRAM:PosCmd_FwdLdd_n
TEST.NEW
TEST.NAME:PosCmd_FwdLdd_n.001
TEST.COMPOUND_ONLY
TEST.EXPECTED:PosCmd.PosCmd_FwdLdd_n.rty_Out1[0..2]:0.0
TEST.END

-- Subprogram: PosCmd_FwdLoad

-- Test Case: PosCmd_FwdLoad.001
TEST.UNIT:PosCmd
TEST.SUBPROGRAM:PosCmd_FwdLoad
TEST.NEW
TEST.NAME:PosCmd_FwdLoad.001
TEST.COMPOUND_ONLY
TEST.VALUE:PosCmd.PosCmd_FwdLoad.rty_Out1:<<malloc 1>>
TEST.EXPECTED:PosCmd.PosCmd_FwdLoad.rty_Out1[0]:0.0
TEST.END

-- Subprogram: PosCmd_FwdLoad_c

-- Test Case: PosCmd_FwdLoad_c.001
TEST.UNIT:PosCmd
TEST.SUBPROGRAM:PosCmd_FwdLoad_c
TEST.NEW
TEST.NAME:PosCmd_FwdLoad_c.001
TEST.COMPOUND_ONLY
TEST.VALUE:PosCmd.PosCmd_FwdLoad_c.rty_Out1:<<malloc 1>>
TEST.VALUE:PosCmd.PosCmd_FwdLoad_c.rty_Out1[0]:0.0
TEST.EXPECTED:PosCmd.PosCmd_FwdLoad_c.rty_Out1[0]:0.0
TEST.END

-- Subprogram: PosCmd_FwdUnLdd

-- Test Case: PosCmd_FwdUnLdd.001
TEST.UNIT:PosCmd
TEST.SUBPROGRAM:PosCmd_FwdUnLdd
TEST.NEW
TEST.NAME:PosCmd_FwdUnLdd.001
TEST.COMPOUND_ONLY
TEST.EXPECTED:PosCmd.PosCmd_FwdUnLdd.rty_Out1[0..2]:0.0
TEST.END

-- Subprogram: PosCmd_FwdUnLdd1

-- Test Case: PosCmd_FwdUnLdd1.001
TEST.UNIT:PosCmd
TEST.SUBPROGRAM:PosCmd_FwdUnLdd1
TEST.NEW
TEST.NAME:PosCmd_FwdUnLdd1.001
TEST.COMPOUND_ONLY
TEST.EXPECTED:PosCmd.PosCmd_FwdUnLdd1.rty_Out1[0..2]:0.0
TEST.END

-- Subprogram: PosCmd_Init

-- Test Case: PosCmd_Init.001
TEST.UNIT:PosCmd
TEST.SUBPROGRAM:PosCmd_Init
TEST.NEW
TEST.NAME:PosCmd_Init.001
TEST.COMPOUND_ONLY
TEST.VALUE:<<OPTIONS>>.EVENT_LIMIT:50
TEST.STUB:Cmd_Pos_Integrator.Cmd_Pos_Integrator_CompStdDev
TEST.STUB:Cmd_Pos_Integrator.Cmd_Pos_Integrator_UpdateMat
TEST.STUB:Cmd_Pos_Integrator.Cmd_Pos_Integrator_Init
TEST.STUB:Cmd_Pos_Integrator.Cmd_Pos_Integrator
TEST.STUB:Cmd_Pos_Integrator.Cmd_Pos_Integrator_initialize
TEST.VALUE:uut_prototype_stubs.Str_Sp_Calc_Crosscheck1.rtu_sensor_calib_c2m_bus[0].rSecHgtSensorCalGain:1.0
TEST.VALUE:uut_prototype_stubs.Str_Sp_Calc_Crosscheck1.rtu_sensor_calib_c2m_bus[0].rPriHgtSensorCalGain:0.0
TEST.VALUE:uut_prototype_stubs.Str_Sp_Calc_Crosscheck1.rtu_sensor_calib_c2m_bus[0].rPrimaryCalCountAtMax:0.0
TEST.VALUE:uut_prototype_stubs.Str_Sp_Calc_Crosscheck1.rtu_sensor_calib_c2m_bus[0].rAuxHtSensorCalGain:0.0
TEST.VALUE:uut_prototype_stubs.Str_Sp_Calc_Crosscheck1.rtu_sensor_calib_c2m_bus[0].rReachSensorMinMax[0]:0.0
TEST.VALUE:uut_prototype_stubs.Str_Sp_Calc_Crosscheck1.rtu_sensor_calib_c2m_bus[0].rTiltSensorLevel:0.0
TEST.VALUE:uut_prototype_stubs.Str_Sp_Calc_Crosscheck1.rtu_sensor_calib_c2m_bus[0].rTraverseSensorCalGain:0.0
TEST.VALUE:uut_prototype_stubs.Str_Sp_Calc_Crosscheck1.rtu_sensor_calib_c2m_bus[0].rLSOffset_Primary:0.0
TEST.VALUE:uut_prototype_stubs.Str_Sp_Calc_Crosscheck1.rtu_sensor_calib_c2m_bus[0].rLSOffset_Secondary:0.0
TEST.VALUE:uut_prototype_stubs.Str_Sp_Calc_Crosscheck1.rtu_sensor_calib_c2m_bus[0].rLSOffset_Aux_cyl:0.0
TEST.VALUE:uut_prototype_stubs.Str_Sp_Calc_Crosscheck1.rtu_sensor_calib_c2m_bus[0].rAccelXoffset:0.0
TEST.VALUE:uut_prototype_stubs.Str_Sp_Calc_Crosscheck1.rtu_sensor_calib_c2m_bus[0].rAccelYoffset:0.0
TEST.VALUE:uut_prototype_stubs.Str_Sp_Calc_Crosscheck1.rtu_sensor_calib_c2m_bus[0].rAccelZoffset:0.0
TEST.VALUE:uut_prototype_stubs.Str_Sp_Calc_Crosscheck1.rtu_sensor_calib_c2m_bus[0].rStrHndlCmd1[0]:0.0
TEST.VALUE:uut_prototype_stubs.Str_Sp_Calc_Crosscheck1.rtu_sensor_calib_c2m_bus[0].slHndlAng1[0]:0
TEST.VALUE:uut_prototype_stubs.Str_Sp_Calc_Crosscheck1.rtu_sensor_calib_c2m_bus[0].rStrHndlCmd2[0]:0.0
TEST.VALUE:uut_prototype_stubs.Str_Sp_Calc_Crosscheck1.rtu_sensor_calib_c2m_bus[0].slHndlAng2[0]:0
TEST.VALUE:uut_prototype_stubs.Str_Sp_Calc_Crosscheck1.rtu_sensor_calib_c2m_bus[0].rMainHeightMax:0.0
TEST.VALUE:uut_prototype_stubs.Str_Sp_Calc_Crosscheck1.rtu_sensor_calib_c2m_bus[0].rCustomHeight:0.0
TEST.VALUE:uut_prototype_stubs.Str_Fb_Crosscheck1.rtu_SteeringFdbkBus[0].MstrFdbk.rStrPos:0.0
TEST.VALUE:uut_prototype_stubs.Str_Fb_Crosscheck1.rtu_SteeringFdbkBus[0].MstrFdbk.rStrSpd:0.0
TEST.VALUE:uut_prototype_stubs.Str_Fb_Crosscheck1.rtu_SteeringFdbkBus[0].MstrFdbk.rStrCur:0.0
TEST.VALUE:uut_prototype_stubs.Str_Fb_Crosscheck1.rtu_SteeringFdbkBus[0].MstrFdbk.ulStopTrx:0
TEST.VALUE:uut_prototype_stubs.Str_Fb_Crosscheck1.rtu_SteeringFdbkBus[0].MstrFdbk.ulFreshCnt:0
TEST.VALUE:Cmd_Pos_Integrator.Cmd_Pos_Integrator_CompStdDev.return:0.0
TEST.VALUE:Cmd_Pos_Integrator.Cmd_Pos_Integrator_UpdateMat.NewSample:0.0
TEST.VALUE:PosCmd.<<GLOBAL>>.StrPosCmd_slStrPosStopZoneClipped:2
TEST.VALUE:PosCmd.<<GLOBAL>>.gpTraction_performance:<<malloc 1>>
TEST.EXPECTED:PosCmd.<<GLOBAL>>.StrPos_rTrxSpd:10.0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.StrPosCmd_rWhAng:0.0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.StrPosSroDetect_fHss:0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.StrPosCmd_rTrxSpdLmt_WhAngCmd:0.0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.StrPosCmd_rTfdCurrentSp2:0.0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.StrPos_rWhAngTrgt:0.0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.StrPosCmd_slHandlePos:0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.StrPosCmd_slStrPosWQPClipped:0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.PosCmd_fHndlRngSro:0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.StrPosSroDetect_fPreventRbtTrtlSw:0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.PosCmd_fRbtTrtlSwitchSro:0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.StrPosCmd_fCmdActive:1
TEST.EXPECTED:PosCmd.<<GLOBAL>>.StrPosCmd_rWhAngVTrxSpdSpeedVector[0]:0.0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.StrPosCmd_rTfdCurrentSp:0.0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.PosCmd_rWhAngTrgtRaw:0.0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.SahNmbns_slHndlPosOut:0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.StrPosCmd_slStrPosCmdModeWhAngLmt[0]:0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.StrPosCmd_slStrPosCmdModeClipped:0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.StrPosCmd_slStrPosStopZoneClipped:2
TEST.EXPECTED:PosCmd.<<GLOBAL>>.StrPosCmd_slWhAngLmt_TrxSpd:0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.StrPosCmd_slPrevCmd:1
TEST.EXPECTED:PosCmd.<<GLOBAL>>.StrPosCmd_slStrPosCmd:1
TEST.EXPECTED:PosCmd.<<GLOBAL>>.StrPosCmd_slStrPosCmdTrxSpdClipped:0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.StrPosSroDetect_ePrevCmdLmtMode:1
TEST.EXPECTED:PosCmd.<<GLOBAL>>.StrPosSroDetect_fRbtTrtlChange:0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.StrPosCmd_eTfdOpModeState:TFD_OP_NoState
TEST.EXPECTED:PosCmd.<<GLOBAL>>.StrPosCmd_eTfdOvrState:TFD_OVR_NoState
TEST.EXPECTED:PosCmd.<<GLOBAL>>.StrPosCmd_eCmdLmtNmbns_State:CmdLmtNmbns_NoState
TEST.EXPECTED:PosCmd.<<GLOBAL>>.StrPosCmd_fSteppedOff:0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.StrPosCmd_fPrevRbtTrtlSwitchSro:0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.StrPosSroDetect_fPrevHss:1
TEST.EXPECTED:PosCmd.<<GLOBAL>>.StrPosSroDetect_fWhAngGT40DegOorSro:0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.StrPosSroDetect_fWhAngCmdGT45Deg:0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.StrPosSroDetect_fWhAngCmdGT3Deg:0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.StrPosSroDetect_fWhAngGT3Deg:0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.StrPosSroDetect_fWhAngErrorGT3Deg:0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.PosCmd_DW.Memory_PreviousInput:1
TEST.EXPECTED:PosCmd.<<GLOBAL>>.PosCmd_DW.UnitDelay2_DSTATE:2
TEST.EXPECTED:PosCmd.<<GLOBAL>>.PosCmd_DW.Model1_o3:0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.PosCmd_DW.UnitDelay1_DSTATE:0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.gpTraction_performance[0].rAccelCal_SPEED:0.0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.gpTraction_performance[0].rAccelLimitForkLoad[0]:0.0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.gpTraction_performance[0].rAccelLimitReachPosition[0]:0.0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.gpTraction_performance[0].rAccelLimitsFWD_PC1[0]:0.0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.gpTraction_performance[0].rAccelLimitsFWD_PC2[0]:0.0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.gpTraction_performance[0].rAccelLimitsFWD_PC3[0]:0.0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.gpTraction_performance[0].rAccelLimitsREV_PC1[0]:0.0
TEST.END

-- Subprogram: PosCmd_RevLdd

-- Test Case: PosCmd_RevLdd.001
TEST.UNIT:PosCmd
TEST.SUBPROGRAM:PosCmd_RevLdd
TEST.NEW
TEST.NAME:PosCmd_RevLdd.001
TEST.COMPOUND_ONLY
TEST.VALUE:PosCmd.PosCmd_RevLdd.rty_Out1[0]:0.0
TEST.EXPECTED:PosCmd.PosCmd_RevLdd.rty_Out1[0]:0.0
TEST.END

-- Subprogram: PosCmd_RevLdd1

-- Test Case: PosCmd_RevLdd1.001
TEST.UNIT:PosCmd
TEST.SUBPROGRAM:PosCmd_RevLdd1
TEST.NEW
TEST.NAME:PosCmd_RevLdd1.001
TEST.COMPOUND_ONLY
TEST.VALUE:PosCmd.<<GLOBAL>>.gpPerformance_bus:<<malloc 1>>
TEST.VALUE:PosCmd.<<GLOBAL>>.gpPerformance_bus[0].rSPEED_LIMIT_VS_ANGLE_NORMAL_REV_LOADED_ANGLE[0]:0.0
TEST.VALUE:PosCmd.<<GLOBAL>>.gpPerformance_bus[0].rSPEED_LIMIT_VS_ANGLE_NORMAL_REV_LOADED_ANGLE[1]:1.0
TEST.VALUE:PosCmd.<<GLOBAL>>.gpPerformance_bus[0].rSPEED_LIMIT_VS_ANGLE_NORMAL_REV_LOADED_ANGLE[2]:2.0
TEST.VALUE:PosCmd.<<GLOBAL>>.gpPerformance_bus[0].rSPEED_LIMIT_VS_ANGLE_NORMAL_REV_LOADED_ANGLE[3]:3.0
TEST.VALUE:PosCmd.<<GLOBAL>>.gpPerformance_bus[0].rSPEED_LIMIT_VS_ANGLE_NORMAL_REV_LOADED_ANGLE[4]:4.0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.gpPerformance_bus[0].rSPEED_LIMIT_VS_ANGLE_NORMAL_REV_LOADED_ANGLE[0]:0.0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.gpPerformance_bus[0].rSPEED_LIMIT_VS_ANGLE_NORMAL_REV_LOADED_ANGLE[1]:1.0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.gpPerformance_bus[0].rSPEED_LIMIT_VS_ANGLE_NORMAL_REV_LOADED_ANGLE[2]:2.0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.gpPerformance_bus[0].rSPEED_LIMIT_VS_ANGLE_NORMAL_REV_LOADED_ANGLE[3]:3.0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.gpPerformance_bus[0].rSPEED_LIMIT_VS_ANGLE_NORMAL_REV_LOADED_ANGLE[4]:4.0
TEST.END

-- Subprogram: PosCmd_RevLoad

-- Test Case: PosCmd_RevLoad.001
TEST.UNIT:PosCmd
TEST.SUBPROGRAM:PosCmd_RevLoad
TEST.NEW
TEST.NAME:PosCmd_RevLoad.001
TEST.COMPOUND_ONLY
TEST.VALUE:PosCmd.<<GLOBAL>>.gpTraction_performance:<<malloc 1>>
TEST.VALUE:PosCmd.<<GLOBAL>>.gpTraction_performance[0].rSPEED_LIMIT_VS_ANGLE_HI_REV_LOADED_SPEED[0]:5.0
TEST.VALUE:PosCmd.<<GLOBAL>>.gpTraction_performance[0].rSPEED_LIMIT_VS_ANGLE_NORMAL_REV_LOADED_LOAD:10.0
TEST.VALUE:PosCmd.PosCmd_RevLoad.rty_Out1:<<malloc 1>>
TEST.EXPECTED:PosCmd.<<GLOBAL>>.gpTraction_performance[0].rSPEED_LIMIT_VS_ANGLE_HI_REV_LOADED_SPEED[0]:5.0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.gpTraction_performance[0].rSPEED_LIMIT_VS_ANGLE_NORMAL_FWD_LOADED_LOAD:0.0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.gpTraction_performance[0].rSPEED_LIMIT_VS_ANGLE_NORMAL_REV_LOADED_LOAD:10.0
TEST.EXPECTED:PosCmd.PosCmd_RevLoad.rty_Out1[0]:0.0
TEST.END

-- Subprogram: PosCmd_RevLoad_g

-- Test Case: PosCmd_RevLoad_g.001
TEST.UNIT:PosCmd
TEST.SUBPROGRAM:PosCmd_RevLoad_g
TEST.NEW
TEST.NAME:PosCmd_RevLoad_g.001
TEST.COMPOUND_ONLY
TEST.VALUE:PosCmd.PosCmd_RevLoad_g.rty_Out1:<<malloc 1>>
TEST.VALUE:PosCmd.PosCmd_RevLoad_g.rty_Out1[0]:0.0
TEST.EXPECTED:PosCmd.PosCmd_RevLoad_g.rty_Out1[0]:0.0
TEST.END

-- Subprogram: PosCmd_RevUnldd

-- Test Case: PosCmd_RevUnldd.001
TEST.UNIT:PosCmd
TEST.SUBPROGRAM:PosCmd_RevUnldd
TEST.NEW
TEST.NAME:PosCmd_RevUnldd.001
TEST.COMPOUND_ONLY
TEST.VALUE:PosCmd.PosCmd_RevUnldd.rty_Out1[0]:0.0
TEST.EXPECTED:PosCmd.PosCmd_RevUnldd.rty_Out1[0]:0.0
TEST.END

-- Subprogram: PosCmd_RevUnldd1

-- Test Case: PosCmd_RevUnldd1.001
TEST.UNIT:PosCmd
TEST.SUBPROGRAM:PosCmd_RevUnldd1
TEST.NEW
TEST.NAME:PosCmd_RevUnldd1.001
TEST.COMPOUND_ONLY
TEST.VALUE:PosCmd.PosCmd_RevUnldd1.rty_Out1[0]:0.0
TEST.EXPECTED:PosCmd.PosCmd_RevUnldd1.rty_Out1[0]:0.0
TEST.END

-- Subprogram: PosCmd_initialize

-- Test Case: PosCmd_initialize.001
TEST.UNIT:PosCmd
TEST.SUBPROGRAM:PosCmd_initialize
TEST.NEW
TEST.NAME:PosCmd_initialize.001
TEST.COMPOUND_ONLY
TEST.STUB:PosCmd.PosCmd_ForceMonoinc
TEST.STUB:PosCmd.PosCmd_ForceMonoinc1
TEST.STUB:TfdCurrentSpProfile.TfdCurrentSpProfile
TEST.STUB:TfdCurrentSpProfile.TfdCurrentSpProfile_initialize
TEST.VALUE:PosCmd.<<GLOBAL>>.StrPosCmd_rWhAng:0.0
TEST.VALUE:PosCmd.<<GLOBAL>>.StrPosSroDetect_fHss:0
TEST.VALUE:PosCmd.<<GLOBAL>>.StrPosCmd_rTrxSpdLmt_WhAngCmd:0.0
TEST.VALUE:PosCmd.<<GLOBAL>>.StrPosCmd_rTfdCurrentSp2:0.0
TEST.VALUE:PosCmd.<<GLOBAL>>.StrPos_rWhAngTrgt:0.0
TEST.VALUE:PosCmd.<<GLOBAL>>.StrPosCmd_slHandlePos:0
TEST.VALUE:PosCmd.<<GLOBAL>>.StrPosCmd_slStrPosWQPClipped:0
TEST.VALUE:PosCmd.<<GLOBAL>>.PosCmd_fHndlRngSro:0
TEST.VALUE:PosCmd.<<GLOBAL>>.StrPosSroDetect_fPreventRbtTrtlSw:0
TEST.VALUE:PosCmd.<<GLOBAL>>.PosCmd_fRbtTrtlSwitchSro:0
TEST.VALUE:PosCmd.<<GLOBAL>>.StrPosCmd_fCmdActive:0
TEST.VALUE:PosCmd.<<GLOBAL>>.StrPosCmd_rWhAngVTrxSpdAngleVector[0]:0.0
TEST.VALUE:PosCmd.<<GLOBAL>>.StrPosCmd_fSteppedOff:0
TEST.VALUE:PosCmd.<<GLOBAL>>.StrPosCmd_fPrevRbtTrtlSwitchSro:0
TEST.VALUE:PosCmd.<<GLOBAL>>.StrPosSroDetect_fPrevHss:0
TEST.VALUE:PosCmd.<<GLOBAL>>.StrPosSroDetect_fWhAngGT40DegOorSro:0
TEST.VALUE:PosCmd.<<GLOBAL>>.StrPosSroDetect_fWhAngCmdGT45Deg:0
TEST.VALUE:PosCmd.<<GLOBAL>>.StrPosSroDetect_fWhAngCmdGT3Deg:0
TEST.VALUE:PosCmd.<<GLOBAL>>.StrPosSroDetect_fWhAngGT3Deg:0
TEST.VALUE:PosCmd.<<GLOBAL>>.StrPosSroDetect_fWhAngErrorGT3Deg:0
TEST.VALUE:PosCmd.<<GLOBAL>>.StrPosSroDetect_fTrxSpdGTZero:0
TEST.VALUE:PosCmd.<<GLOBAL>>.PosCmd_DW.Memory_PreviousInput:0
TEST.VALUE:PosCmd.<<GLOBAL>>.PosCmd_DW.UnitDelay2_DSTATE:0
TEST.VALUE:PosCmd.<<GLOBAL>>.PosCmd_DW.Model1_o3:0
TEST.VALUE:PosCmd.<<GLOBAL>>.PosCmd_DW.UnitDelay1_DSTATE:0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.StrPos_rTrxSpd:10.0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.StrPosCmd_rWhAng:0.0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.StrPosSroDetect_fHss:0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.StrPosCmd_rTrxSpdLmt_WhAngCmd:0.0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.StrPosCmd_slStrPosWQPClipped:0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.PosCmd_fHndlRngSro:0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.StrPosSroDetect_fPreventRbtTrtlSw:0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.PosCmd_fRbtTrtlSwitchSro:0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.StrPosCmd_fCmdActive:0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.StrPosCmd_rWhAngVTrxSpdAngleVector[0]:0.0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.StrPosCmd_rWhAngVTrxSpdSpeedVector[0]:0.0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.StrPosCmd_rTfdCurrentSp:0.0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.SahNmbns_slHndlPosOut:0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.StrPosCmd_fSteppedOff:0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.StrPosCmd_fPrevRbtTrtlSwitchSro:0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.StrPosSroDetect_fPrevHss:0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.StrPosSroDetect_fWhAngGT40DegOorSro:0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.StrPosSroDetect_fWhAngCmdGT45Deg:0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.StrPosSroDetect_fWhAngCmdGT3Deg:0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.StrPosSroDetect_fTrxSpdGTZero:0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.PosCmd_DW.Memory_PreviousInput:0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.PosCmd_DW.UnitDelay2_DSTATE:0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.PosCmd_DW.Model1_o3:0
TEST.EXPECTED:PosCmd.<<GLOBAL>>.PosCmd_DW.UnitDelay1_DSTATE:0
TEST.END

-- Unit: SpdCmd

-- Subprogram: SpdCmd

-- Test Case: SpdCmd.001
TEST.UNIT:SpdCmd
TEST.SUBPROGRAM:SpdCmd
TEST.NEW
TEST.NAME:SpdCmd.001
TEST.COMPOUND_ONLY
TEST.STUB:SpdCmd.SpdCmd_Input_1_OOR
TEST.STUB:SpdCmd.SpdCmd_RevLoad
TEST.STUB:SpdCmd.SpdCmd_Init
TEST.STUB:SpdCmd.SpdCmd_initialize
TEST.STUB:StrPos_SahNmbns.StrPos_SahNmbns_initialize
TEST.STUB:StrPos_SetPntRateLmtr.StrPos_SetPntRateLmt_initialize
TEST.STUB:TfdCurrentSpProfile.TfdCurrentSpProfile
TEST.STUB:TfdCurrentSpProfile.TfdCurrentSpProfile_initialize
TEST.VALUE:<<OPTIONS>>.EVENT_LIMIT:10
TEST.VALUE:SpdCmd.<<GLOBAL>>.gpTraction_performance:<<malloc 1>>
TEST.VALUE:SpdCmd.<<GLOBAL>>.gpPerformance_bus:<<malloc 1>>
TEST.VALUE:SpdCmd.<<GLOBAL>>.gpSteer_params:<<malloc 1>>
TEST.VALUE:SpdCmd.<<GLOBAL>>.gpSteer_params[0].eControlMode:ASSIST
TEST.VALUE:SpdCmd.SpdCmd_Input_1_OOR.rty_Out1[0]:0.0
TEST.VALUE:SpdCmd.SpdCmd_RevLoad.rty_Out1[0]:0.0
TEST.VALUE:SpdCmd.SpdCmd.rtu_SpdFdbk:0
TEST.VALUE:SpdCmd.SpdCmd.rtu_DuInCwStop:0
TEST.VALUE:SpdCmd.SpdCmd.rtu_DuInCcwStop:0
TEST.VALUE:SpdCmd.SpdCmd.rtu_StrAllowedIn:0
TEST.VALUE:SpdCmd.SpdCmd.rtu_ForkLoadIn:0.0
TEST.VALUE:SpdCmd.SpdCmd.rtu_gpSensor_cals_c2m:<<malloc 1>>
TEST.VALUE:SpdCmd.SpdCmd.rtu_AGV_output_bus_t:<<malloc 1>>
TEST.VALUE:SpdCmd.SpdCmd.rtu_AGV_output_bus_t[0].rHydraulicThrottle:0.0
TEST.VALUE:SpdCmd.SpdCmd.rtu_AGV_output_bus_t[0].rTractionThrottle:0.0
TEST.VALUE:SpdCmd.SpdCmd.rtu_AGV_output_bus_t[0].rTractionSpeedCmd:0.0
TEST.VALUE:SpdCmd.SpdCmd.rty_SpdCmdOut:<<malloc 1>>
TEST.VALUE:StrMstrApp.<<GLOBAL>>.gpSteer_inputs:<<malloc 1>>
TEST.VALUE:StrMstrApp.<<GLOBAL>>.gpSteer_params:<<malloc 1>>
TEST.VALUE:StrMtrSpdLmt.<<GLOBAL>>.StrMtrSpdLmt_DW.WhAngErrSf:3.0
TEST.VALUE:StrPos_SetPntRateLmtr.<<GLOBAL>>.gpSteer_params:<<malloc 1>>
TEST.VALUE:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params:<<malloc 1>>
TEST.VALUE:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].eUseDirFeature:USE_MODE
TEST.VALUE:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].eUseWalkieMode:USE_MODE
TEST.VALUE:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].eUseRabbitTurtleMode:USE_MODE
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.StrSpdCmd_rCmd1:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.StrSpd_rTrxSpd:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.StrSpd_rWhAng:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.StrSpdCmd_rCmd2:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.StrSpdCmd_rDiffCmd:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.StrSpd_rTrxSpdLmt_WhAngCmd:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.StrSpd_rWhAngTrgt:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rAccelCal_SPEED:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rAccelLimitForkLoad[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rAccelLimitReachPosition[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rAccelLimitsFWD_PC1[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rAccelLimitsFWD_PC2[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rAccelLimitsFWD_PC3[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rAccelLimitsREV_PC1[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rAccelLimitsREV_PC2[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rAccelLimitsREV_PC3[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rAngleHys:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rBRES_DECEL:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rBRES_SPEED:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rBrs1_BrakeLevel:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rBrs2_BrakeLevel:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rBrs3_BrakeLevel:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rCmdLimit_DECEL:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rCoastResetRate:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rCoastSteerAngle:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rDecelLimitForkLoad[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rDecelLimitReachPosition[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rDecelLimits[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rDirection_SPEED:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rDMS_COAST_FOR[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rSPEED_LIMIT_VS_ANGLE_NORMAL_FWD_LOADED_LOAD:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rSPEED_LIMIT_VS_ANGLE_NORMAL_REV_LOADED_LOAD:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rSPEED_LIMIT_VS_ANGLE_PICK_FWD_EMPTY_ANGLE[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rSPEED_LIMIT_VS_ANGLE_PICK_FWD_EMPTY_SPEED[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rSPEED_LIMIT_VS_ANGLE_PICK_FWD_LOADED_LOAD:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rSPEED_LIMIT_VS_ANGLE_PICK_FWD_LOADED_SPEED[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rSPEED_LIMIT_VS_ANGLE_PICK_REV_EMPTY_ANGLE[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rSPEED_LIMIT_VS_ANGLE_PICK_REV_EMPTY_SPEED[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rSPEED_LIMIT_VS_ANGLE_PICK_REV_LOADED_ANGLE[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rSPEED_LIMIT_VS_ANGLE_PICK_REV_LOADED_LOAD:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rSPEED_LIMIT_VS_ANGLE_PICK_REV_LOADED_SPEED[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rSPEED_LIMIT_VS_ANGLE_WALKIE_FWD_EMPTY_ANGLE[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rSPEED_LIMIT_VS_ANGLE_WALKIE_FWD_EMPTY_SPEED[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rSPEED_LIMIT_VS_ANGLE_WALKIE_FWD_LOADED_ANGLE[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rSPEED_LIMIT_VS_ANGLE_WALKIE_FWD_LOADED_LOAD:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rSPEED_LIMIT_VS_ANGLE_WALKIE_FWD_LOADED_SPEED[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rSPEED_LIMIT_VS_ANGLE_WALKIE_REV_EMPTY_ANGLE[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rSPEED_LIMIT_VS_ANGLE_WALKIE_REV_EMPTY_SPEED[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rSPEED_LIMIT_VS_ANGLE_WALKIE_REV_LOADED_ANGLE[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rSPEED_LIMIT_VS_ANGLE_WALKIE_REV_LOADED_LOAD:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rSPEED_LIMIT_VS_ANGLE_WALKIE_REV_LOADED_SPEED[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rSPEED_LIMIT_VS_GRADE_DOWN_GRADE_SPEED[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rSPEED_LIMIT_VS_GRADE_GRADE[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rSPEED_LIMIT_VS_GRADE_LOAD[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rSPEED_LIMIT_VS_GRADE_UP_GRADE_SPEED[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rSPEED_VS_HEIGHT_FWD[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rSPEED_VS_HEIGHT_FWD_HEIGHT[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rSPEED_VS_HEIGHT_REV[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rSPEED_VS_HEIGHT_REV_HEIGHT[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rSteerAngleCoast:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rSteps_DECEL:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rSteps_SPEED:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rTARF2_REDUCTION_0_4[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rTARF2_SPEED_0_4[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].eSensorType:ENCODER
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].eControlMode:ASSIST
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].ulFbEcr1Cnts:0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].ulFbEcr2Cnts:0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].eUseDirFeature:USE_MODE
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].slSteerRotation:0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].eUseWalkieMode:USE_MODE
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].eUseRabbitTurtleMode:USE_MODE
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].ulEndStopZoneCW:0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].ulEndStopZoneCCW:0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].slQPWhAngLmt[0]:0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].rTil2WhRatio:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].ulSpdModeCntPerTickMax:0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].rSpdModeKp:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].rSpdModeKi:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].rSpdModeKd:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].rSpdModeDerFltrCoef:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].rStrGearRatio:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].rStrSpdDeadBand:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].ulStrSpdUpperBoundary:0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].ulStrSpdLowerBoundary:0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].rStrSpdROTO:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].rStrSpdDiffCntsFltrGain:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].rTfdCurrent_VS_TractionSpeed_Speed[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].rTfdCurrent_VS_TractionSpeed_Current[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].rTfdCurrent_VS_SteeringError_Error[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].rTfdCurrent_VS_SteeringError_Current[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].rTfdMaxCurrent:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].rTfdStiffCurrent:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].rTfdRampDnHoldCurrent:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].rTfdCurrentRampRateLimiter:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].rTfdRampDnHoldTime:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].rTfdHoldUpSpeedMax:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].rStrMtrZeroSpd:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].slStrMtrSpdLmt:0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].rStrMtrSpdRedTrxSpdBP:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].rStrMtrSpdRedMaxTrxSpd:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].rStrMtrSpdMaxTrxSpdRf:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].rStrMtrSpdRedWhAngBP:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].rStrMtrSpdRedMaxWhAng:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].rStrMtrSpdMaxWhAngRf:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].ulTrigFunct:0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].ulStrMtrSpdWhAngRfType:0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].rTilScalingTrxSpdBP:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].rTilScalingTrxSpdMax:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].rTilScalingMaxTrxSpdSf:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].rTillerMotionThreshold:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].rCmd1_RO_Upr:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].rCmd1_RO_Lwr:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].rCmd1_Rng_Upr:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].rCmd1_Rng_Lwr:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].rAssistGain:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].rStrCmdDeadBand:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].rTrxSpdWhAng_SpdOs:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].eUse360Feature:DO_NOT_USE
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].rStrMtrSpdRedWhAngErrBP:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].rStrMtrSpdRedMaxWhAngErr:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].rStrMtrSpdMaxWhAngErrRf:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].rHndlSpdClip:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].rHndlSpdFltrCoef:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].rHndlSpdThrshld:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].rWhAngTrgtFltGain:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].rSahNmbnsTrxSpdBpSf:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].rSahNmbnsTlr2DuSf:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].rSpFltrExpSf:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].rSpFltrExpRef:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].rSahNmbnsMaxInitAng:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].rSahNmbnsTlr2DuNom:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].rSahNmbnsMaxNumbedAng:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].rSahNmbnsTransAng:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].rStrNoEdRequired:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].rSteerAngleBeforeED:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].eSteer_Direction:Forward
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].slSensorSlope:0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].slSensorSignalRelation:0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].rStrSpdBumpThrshld:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].rBumpCancelThrshld:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].rDeadBandGain:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].rDeadBandHyst:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].rDeadBandReEntDly:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].rBumpCmdRawBoundary:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpSteer_params[0].rStrSpdDbBumpThrshldGain:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.StrSpdCmd_rCmdAccumAng:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.StrSpdCmd_rDUAccumAng:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.StrSpd_rCmdErrInRaw:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.StrSpdCmd_rCmdErrOut:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.StrSpdCmd_rCmdErrOut_WhAng:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.StrSpdCmd_rSpdxKd:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.StrSpdCmd_rDerFdbkFltrd:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.StrSpd_rTil2DuRatio:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.StrSpdCmd_rIntAng:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.StrSpdCmd_rDerSumxCoef:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.StrSpd_rWhAngInc:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.StrSpd_rNextPos:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.StrSpd_rWhAngCmd:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.StrSpd_rWhAngVTrxSpdAngleVector[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.StrSpd_rWhAngVTrxSpdSpeedVector[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.StrSpdCmd_rAnaSnsr1Prev:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.StrSpdCmd_rAnaSnsr2Prev:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.StrSpdCmd_rAnaSnsrDiffCmd:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.StrSpdCmd_rEncSnsr1Prev:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.StrSpdCmd_slPropTerm:0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.StrSpd_slCmdMaxSpdClipped:0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.StrSpdCmd_slCmdClipped:0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.SpdMode_slWhAngCmdRaw:0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.StrSpd_slWhAngLmt_TrxSpd:0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.SpdMode_slWhAngSP:0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.StrSpd_slCmdDeadBandRng:0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.StrSpdCmd_eSelectAnaSnsrState:AnaSnsrSelect_INIT
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.StrSpd_eCmdLmtNmbns_State:CmdLmtNmbns_NoTrxReduxTrgtWhAng
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.StrSpdCmd_fCmdSign:0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.StrSpdCmd_fInCwStopMovingOut:0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.StrSpdCmd_fInCcwStopMovingOut:0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.StrSpdCmd_fDUCInRange:1
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.StrSpdCmd_fCmdOk:1
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.StrSpdCmd_fCmdAllowed:0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.StrSpdCmd_fResetLo:0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.StrSpdCmd_fDeadBandDet:1
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.SpdCmd_DW.UnitDelay_DSTATE:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.SpdCmd_DW.UnitDelay_DSTATE_i:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.SpdCmd_DW.UnitDelay_DSTATE_h:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.SpdCmd_DW.UnitDelay2_DSTATE:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.SpdCmd_DW.UnitDelay1_DSTATE:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.SpdCmd_DW.UnitDelay2_DSTATE_n:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.SpdCmd_DW.UnitDelay1_DSTATE_j:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.SpdCmd_DW.UnitDelay1_DSTATE_k:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.SpdCmd_DW.is_active_c1_SpdCmd:0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.SpdCmd_DW.is_c1_SpdCmd:0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.SpdCmd_DW.UnitDelay2_DSTATE_c:0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.SpdCmd_DW.UnitDelay4_DSTATE:0
TEST.EXPECTED:SpdCmd.SpdCmd_Input_1_OOR.rty_Out1[0]:0.0
TEST.EXPECTED:SpdCmd.SpdCmd.rtu_SpdFdbk:0
TEST.EXPECTED:SpdCmd.SpdCmd.rtu_DuInCwStop:0
TEST.EXPECTED:SpdCmd.SpdCmd.rtu_DuInCcwStop:0
TEST.EXPECTED:SpdCmd.SpdCmd.rtu_StrAllowedIn:0
TEST.EXPECTED:SpdCmd.SpdCmd.rtu_ForkLoadIn:0.0
TEST.EXPECTED:SpdCmd.SpdCmd.rtu_gpSensor_cals_c2m[0].rSecHgtSensorCalGain:0.0
TEST.EXPECTED:SpdCmd.SpdCmd.rtu_gpSensor_cals_c2m[0].rPriHgtSensorCalGain:0.0
TEST.EXPECTED:SpdCmd.SpdCmd.rtu_gpSensor_cals_c2m[0].rPrimaryCalCountAtMax:0.0
TEST.EXPECTED:SpdCmd.SpdCmd.rtu_gpSensor_cals_c2m[0].rAuxHtSensorCalGain:0.0
TEST.EXPECTED:SpdCmd.SpdCmd.rtu_gpSensor_cals_c2m[0].rReachSensorMinMax[0]:0.0
TEST.EXPECTED:SpdCmd.SpdCmd.rtu_gpSensor_cals_c2m[0].rTiltSensorMinZeroMax[0]:0.0
TEST.EXPECTED:SpdCmd.SpdCmd.rtu_gpSensor_cals_c2m[0].rTiltSensorLevel:0.0
TEST.EXPECTED:SpdCmd.SpdCmd.rtu_gpSensor_cals_c2m[0].rPivotSensorCalGain:0.0
TEST.EXPECTED:SpdCmd.SpdCmd.rtu_gpSensor_cals_c2m[0].rTraverseSensorCalGain:0.0
TEST.EXPECTED:SpdCmd.SpdCmd.rtu_gpSensor_cals_c2m[0].rLSOffset_Primary:0.0
TEST.EXPECTED:SpdCmd.SpdCmd.rtu_gpSensor_cals_c2m[0].rLSOffset_Secondary:0.0
TEST.EXPECTED:SpdCmd.SpdCmd.rtu_gpSensor_cals_c2m[0].rLSOffset_Aux_cyl:0.0
TEST.EXPECTED:SpdCmd.SpdCmd.rtu_gpSensor_cals_c2m[0].rAccelYoffset:0.0
TEST.EXPECTED:SpdCmd.SpdCmd.rtu_gpSensor_cals_c2m[0].rAccelZoffset:0.0
TEST.EXPECTED:SpdCmd.SpdCmd.rtu_gpSensor_cals_c2m[0].rStrHndlCmd1[0]:0.0
TEST.EXPECTED:SpdCmd.SpdCmd.rtu_gpSensor_cals_c2m[0].slHndlAng1[0]:0
TEST.EXPECTED:SpdCmd.SpdCmd.rtu_gpSensor_cals_c2m[0].rStrHndlCmd2[0]:0.0
TEST.EXPECTED:SpdCmd.SpdCmd.rtu_gpSensor_cals_c2m[0].slHndlAng2[0]:0
TEST.EXPECTED:SpdCmd.SpdCmd.rtu_gpSensor_cals_c2m[0].slDuAng[0]:0
TEST.EXPECTED:SpdCmd.SpdCmd.rtu_gpSensor_cals_c2m[0].rMainHeightMax:0.0
TEST.EXPECTED:SpdCmd.SpdCmd.rtu_gpSensor_cals_c2m[0].rCustomHeight:0.0
TEST.EXPECTED:SpdCmd.SpdCmd.rtu_FwdRevSteer:0
TEST.EXPECTED:SpdCmd.SpdCmd.rtu_RgsIn:0
TEST.EXPECTED:SpdCmd.SpdCmd.rtu_AGV_output_bus_t[0].rHydraulicThrottle:0.0
TEST.EXPECTED:SpdCmd.SpdCmd.rtu_AGV_output_bus_t[0].rTractionThrottle:0.0
TEST.EXPECTED:SpdCmd.SpdCmd.rtu_AGV_output_bus_t[0].rTractionSpeedCmd:0.0
TEST.EXPECTED:SpdCmd.SpdCmd.rtu_AGV_output_bus_t[0].rSteerCommand1:0.0
TEST.EXPECTED:SpdCmd.SpdCmd.rtu_AGV_output_bus_t[0].rSteerCommand2:0.0
TEST.EXPECTED:SpdCmd.SpdCmd.rtu_AGV_output_bus_t[0].rHeight:0.0
TEST.EXPECTED:SpdCmd.SpdCmd.rtu_AGV_output_bus_t[0].sbHoistRaiseOrLower:0
TEST.EXPECTED:SpdCmd.SpdCmd.rtu_AGV_output_bus_t[0].fAutoMode:0
TEST.EXPECTED:SpdCmd.SpdCmd.rtu_AGV_output_bus_t[0].fBrake:0
TEST.EXPECTED:SpdCmd.SpdCmd.rtu_AGV_output_bus_t[0].ubTruckState:0
TEST.EXPECTED:SpdCmd.SpdCmd.rtu_PwrDrvAllowed:0
TEST.EXPECTED:SpdCmd.SpdCmd.rty_SpdCmdOut[0]:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_inputs[0].rStrCmd1:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_inputs[0].rStrCmd2:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_inputs[0].ulStrMstrStat:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_inputs[0].ulStrSlvStat:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].eSensorType:ENCODER
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].eControlMode:ASSIST
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].ulFbEcr1Cnts:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].ulFbEcr2Cnts:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].eUseDirFeature:USE_MODE
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].slSteerRotation:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].eUseWalkieMode:USE_MODE
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].eUseRabbitTurtleMode:USE_MODE
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].ulEndStopZoneCW:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].ulEndStopZoneCCW:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].slAttTurtleWhAngLmt[0]:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].slQPWhAngLmt[0]:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].rTil2WhRatio:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].ulSpdModeCntPerTickMax:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].rSpdModeKp:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].rSpdModeKd:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].rStrTs:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].rSpdModeDerFltrCoef:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].rStrGearRatio:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].rStrSpdDeadBand:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].ulStrSpdUpperBoundary:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].ulStrSpdLowerBoundary:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].rStrSpdROTO:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].rStrSpdDiffCntsFltrGain:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].rTfdCurrent_VS_TractionSpeed_Speed[0]:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].rTfdCurrent_VS_TractionSpeed_Current[0]:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].rTfdCurrent_VS_SteeringError_Error[0]:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].rTfdCurrent_VS_SteeringError_Current[0]:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].rTfdMaxCurrent:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].rTfdReduction:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].rTfdStiffCurrent:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].rTfdCurrentRampRateLimiter:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].rTfdRampDnHoldTime:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].rTfdHoldUpSpeedMax:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].rStrMtrZeroSpd:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].slStrMtrSpdLmt:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].rStrMtrSpdRedTrxSpdBP:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].rStrMtrSpdRedMaxTrxSpd:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].rStrMtrSpdMaxTrxSpdRf:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].rStrMtrSpdRedWhAngBP:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].rStrMtrSpdRedMaxWhAng:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].rStrMtrSpdMaxWhAngRf:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].ulTrigFunct:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].ulStrMtrSpdWhAngRfType:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].rTilScalingTrxSpdBP:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].rTilScalingTrxSpdMax:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].rTilScalingMaxTrxSpdSf:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].rCmd1_RO_Upr:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].rCmd1_RO_Lwr:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].rCmd1_Rng_Upr:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].rStrCmdDeadBand:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].rTrxSpdWhAng_SpdOs:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].eUse360Feature:DO_NOT_USE
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].rStrMtrSpdRedWhAngErrBP:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].rStrMtrSpdRedMaxWhAngErr:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].rStrMtrSpdMaxWhAngErrRf:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].rHndlSpdClip:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].rHndlSpdFltrCoef:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].rHndlSpdThrshld:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].rWhAngTrgtFltGain:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].rSahNmbnsTrxSpdBpSf:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].rSahNmbnsTlr2DuSf:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].rSpFltrExpSf:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].rSpFltrExpRef:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].rSahNmbnsMaxInitAng:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].rSahNmbnsTlr2DuNom:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].rSahNmbnsMaxNumbedAng:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].rSahNmbnsTransAng:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].rStrNoEdRequired:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].eSteer_Direction:Forward
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].slSensorSlope:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].slSensorSignalRelation:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].rStrSpdBumpThrshld:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].rBumpCancelThrshld:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].rDeadBandGain:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].rDeadBandHyst:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].rDeadBandReEntDly:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].rBumpCmdRawBoundary:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].rStrSpdDbBumpThrshldGain:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.StrPosCmd_rWhAng:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.StrSpdCmd_rCmd1:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.StrSpd_rTrxSpd:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.StrSpd_rWhAng:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.StrSpdCmd_rCmd2:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gSteer_sim_outputs_rTfdCmd:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.StrSpd_rWhAngTrgt:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.StrSpd_rTrxSpdLmt_WhAngCmd:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.StrSpdCmd_rDiffCmd:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.StrPos_rWhAngTrgt:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.StrPosCmd_rTfdCurrentSp2:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.StrPosCmd_rTrxSpdLmt_WhAngCmd:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.StrMstrApp_slFwdRevStrRaw:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.StrMstrApp_slFwdRevStrPrev:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.StrMstrApp_slLmtrWhAng:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.StrMstrApp_slDuLimits[0]:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gSteer_sim_outputs_slMstrStrCmd:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gSteer_sim_outputs_slSlvStrCmd:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gSteer_sim_outputs_slStrMtrSpdMax:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.StrPosCmd_slHandlePos:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.StrPosCmd_slStrPosWQPClipped:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gSteer_sim_outputs_fSteeringEnabled:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gSteer_sim_outputs_ulStrCmdFreshCntr:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gSteer_sim_outputs_ulStopSteering:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.StrMstrApp_eSteerDirectionRaw:Forward
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.StrMstrApp_eStrDirChng_State:StrDirChange_INIT
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.StrMstrApp_fStrRunMode:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.StrMstrApp_ChangeHemiState:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.StrMstrApp_fNoIdxBeforeStr:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.StrMstrApp_fScmRdyOnEn:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.StrMstrApp_fUseUprHemi:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.StrSteeringRunMode_fStrDuInCwStop:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.StrSteeringRunMode_fStrDuInCcwStop:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.StrSpdCmd_fCmdActive:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.StrPosCmd_fCmdActive:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.PosCmd_fRbtTrtlSwitchSro:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.PosCmd_fHndlRngSro:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.StrPosSroDetect_fPreventRbtTrtlSw:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.StrAssistCmd_fCmdActive:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.StrMstrApp_fDirChngViolation:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.StrMstrApp_fStrDirInit:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.StrMstrApp_fDirChngPending:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.StrMstrApp_fDuInLwrHemi:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.StrMstrApp_DW.BusCreator.slMstrStrCmd:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.StrMstrApp_DW.BusCreator.slSlvStrCmd:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.StrMstrApp_DW.BusCreator.fSteeringEnabled:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.StrMstrApp_DW.BusCreator.rTfdCmd:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.StrMstrApp_DW.BusCreator.slStrMtrSpdMax:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.StrMstrApp_DW.BusCreator.ulStrCmdFreshCntr:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.StrMstrApp_DW.BusCreator.ulStopSteering:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.StrMstrApp_DW.OutportBufferForStrSpd_DiffCnts:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.StrMstrApp_DW.OutportBufferForWhAngTarget:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.StrMstrApp_DW.OutportBufferForTfdCurrentSp2:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.StrMstrApp_DW.MaxStrMtrSpdInit:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.StrMstrApp_DW.Merge:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.StrMstrApp_DW.OutportBufferForStr_SP_Raw:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.StrMstrApp_DW.StrDir_Out:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.StrMstrApp_DW.StrDir_Previous_Out:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.StrMstrApp_DW.UnitDelay_DSTATE:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.StrMstrApp_DW.UnitDelay1_DSTATE:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.StrMstrApp_DW.StrPwrCmdMode:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.StrMstrApp_DW.UnitDelay1_DSTATE_b:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.StrMstrApp_DW.is_active_c1_StrMstrApp:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.StrMstrApp_DW.is_c1_StrMstrApp:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.StrMstrApp_DW.is_STR_OP_MODE_PositionSynch:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.StrMstrApp_DW.is_STR_OP_MODE_SpeedSynch:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.StrMstrApp_DW.is_active_c2_StrMstrApp:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.StrMstrApp_DW.is_c2_StrMstrApp:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.StrMstrApp_DW.is_active_c3_StrMstrApp:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.StrMstrApp_DW.is_c3_StrMstrApp:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.StrMstrApp_DW.UnitDelay_DSTATE_o:0
TEST.EXPECTED:StrMtrSpdLmt.<<GLOBAL>>.StrMtrSpdLmt_DW.WhAngErrSf:3.0
TEST.EXPECTED:StrPos_SetPntRateLmtr.<<GLOBAL>>.PosCmd_rFilterTimeRamp:0.0
TEST.EXPECTED:StrPos_SetPntRateLmtr.<<GLOBAL>>.PosCmd_rFilterExp:0.0
TEST.EXPECTED:StrPos_SetPntRateLmtr.<<GLOBAL>>.PosCmd_rFilterExponential:0.0
TEST.EXPECTED:StrPos_SetPntRateLmtr.<<GLOBAL>>.PosCmd_rSpDiff:0.0
TEST.EXPECTED:StrPos_SetPntRateLmtr.<<GLOBAL>>.PosCmd_slSpLmtrOutPrev:0
TEST.EXPECTED:StrPos_SetPntRateLmtr.<<GLOBAL>>.PosCmd_slSpOffset:0
TEST.EXPECTED:StrPos_SetPntRateLmtr.<<GLOBAL>>.PosCmd_slSpDiffScaled:0
TEST.EXPECTED:StrPos_SetPntRateLmtr.<<GLOBAL>>.PosCmd_fLmtSetPnt:0
TEST.EXPECTED:StrPos_SetPntRateLmtr.<<GLOBAL>>.PosCmd_fTrxSpdGTETrxSpdLmt:0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].eSensorType:ENCODER
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].eControlMode:ASSIST
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].ulFbEcr1Cnts:0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].ulFbEcr2Cnts:0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].eUseDirFeature:USE_MODE
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].eUseWalkieMode:USE_MODE
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].eUseRabbitTurtleMode:USE_MODE
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].ulEndStopZoneCW:0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].ulEndStopZoneCCW:0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].slAttTurtleWhAngLmt[0]:0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].slQPWhAngLmt[0]:0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rTil2WhRatio:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].ulSpdModeCntPerTickMax:0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rSpdModeKp:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rSpdModeKi:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rSpdModeKd:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rStrTs:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rSpdModeDerFltrCoef:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rStrGearRatio:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rStrSpdDeadBand:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].ulStrSpdUpperBoundary:0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].ulStrSpdLowerBoundary:0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rStrSpdROTO:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rStrSpdDiffCntsFltrGain:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rTfdCurrent_VS_TractionSpeed_Speed[0]:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rTfdCurrent_VS_TractionSpeed_Current[0]:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rTfdCurrent_VS_SteeringError_Error[0]:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rTfdReduction:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rTfdStiffCurrent:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rTfdRampDnHoldCurrent:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rTfdCurrentRampRateLimiter:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rTfdRampDnHoldTime:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rTfdHoldUpSpeedMax:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rStrMtrZeroSpd:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].slStrMtrSpdLmt:0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rStrMtrSpdMaxTrxSpdRf:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rStrMtrSpdRedWhAngBP:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rStrMtrSpdRedMaxWhAng:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rStrMtrSpdMaxWhAngRf:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].ulTrigFunct:0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].ulStrMtrSpdWhAngRfType:0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rTilScalingTrxSpdMax:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rTilScalingMaxTrxSpdSf:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rTillerMotionThreshold:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rCmd1_RO_Upr:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rCmd1_RO_Lwr:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rCmd1_Rng_Upr:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rCmd1_Rng_Lwr:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rAssistGain:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rStrCmdDeadBand:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rTrxSpdWhAng_SpdOs:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].eUse360Feature:DO_NOT_USE
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rStrMtrSpdRedWhAngErrBP:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rStrMtrSpdRedMaxWhAngErr:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rStrMtrSpdMaxWhAngErrRf:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rHndlSpdClip:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rHndlSpdFltrCoef:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rHndlSpdThrshld:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rWhAngTrgtFltGain:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rSahNmbnsTrxSpdBpSf:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rSpFltrExpSf:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rSpFltrExpRef:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rSahNmbnsMaxInitAng:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rSahNmbnsTlr2DuNom:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rSahNmbnsMaxNumbedAng:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rSahNmbnsTransAng:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].eSteer_Direction:Forward
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].slSensorSlope:0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].slSensorSignalRelation:0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rStrSpdBumpThrshld:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rBumpCancelThrshld:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rDeadBandGain:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rDeadBandHyst:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rBumpCmdRawBoundary:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rStrSpdDbBumpThrshldGain:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.TfdOpMode_rSteerError:0.0
TEST.END

-- Subprogram: SpdCmd_Init

-- Test Case: SpdCmd_Init.001
TEST.UNIT:SpdCmd
TEST.SUBPROGRAM:SpdCmd_Init
TEST.NEW
TEST.NAME:SpdCmd_Init.001
TEST.COMPOUND_ONLY
TEST.STUB:TfdCurrentSpProfile.TfdCurrentSpProfile
TEST.VALUE:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params:<<malloc 1>>
TEST.VALUE:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].eControlMode:POSITION
TEST.VALUE:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].eUseDirFeature:DO_NOT_USE
TEST.VALUE:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].eUseWalkieMode:DO_NOT_USE
TEST.VALUE:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].eUseRabbitTurtleMode:DO_NOT_USE
TEST.VALUE:TfdCurrentSpProfile.TfdCurrentSpProfile.rty_rTfdCurrentSp[0]:1.0
TEST.EXPECTED:StrPos_SahNmbns.<<GLOBAL>>.SahNmbns_slHndlPosOut:0
TEST.EXPECTED:StrPos_SahNmbns.<<GLOBAL>>.SahNmbns_rTrxSpdBp:0.0
TEST.EXPECTED:StrPos_SahNmbns.<<GLOBAL>>.SahNmbns_rTrxSpdMax:0.0
TEST.EXPECTED:StrPos_SahNmbns.<<GLOBAL>>.SahNmbns_rHndlPosNmbRng:0.0
TEST.EXPECTED:StrPos_SahNmbns.<<GLOBAL>>.SahNmbns_rHndlPosTransRng:0.0
TEST.EXPECTED:StrPos_SahNmbns.<<GLOBAL>>.SahNmbns_rHndlPosUnNmbd:0.0
TEST.EXPECTED:StrPos_SahNmbns.<<GLOBAL>>.SahNmbns_rTrxSpdInNmbRng:0
TEST.EXPECTED:StrPos_SahNmbns.<<GLOBAL>>.SahNmbns_eState:SahNmbns_INIT
TEST.EXPECTED:StrPos_SahNmbns.<<GLOBAL>>.SahNmbns_rHndlAngFromEndOfTransRng:0.0
TEST.EXPECTED:StrPos_SahNmbns.<<GLOBAL>>.SahNmbns_rHndlAngFrctFromEndOfTransRng:0.0
TEST.EXPECTED:StrPos_SahNmbns.<<GLOBAL>>.SahNmbns_rTrxSpdAboveBp:0.0
TEST.EXPECTED:StrPos_SahNmbns.<<GLOBAL>>.SahNmbns_rTrxSpdDeltaNmbRng:0.0
TEST.EXPECTED:StrPos_SahNmbns.<<GLOBAL>>.SahNmbns_rTrxSpdFrctOfNmbRng:0.0
TEST.EXPECTED:StrPos_SahNmbns.<<GLOBAL>>.SahNmbns_rTil2DuDeltaNmbRng:0.0
TEST.EXPECTED:StrPos_SahNmbns.<<GLOBAL>>.SahNmbns_rTil2DuDelta:0.0
TEST.EXPECTED:StrPos_SahNmbns.<<GLOBAL>>.SahNmbns_rTil2DuRatio:0.0
TEST.EXPECTED:StrPos_SahNmbns.<<GLOBAL>>.StrPos_SahNmbns_DW.is_active_c3_StrPos_SahNmbns:0
TEST.EXPECTED:StrPos_SahNmbns.<<GLOBAL>>.StrPos_SahNmbns_DW.is_c3_StrPos_SahNmbns:0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].eSensorType:ENCODER
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].eControlMode:POSITION
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].ulFbEcr1Cnts:0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].ulFbEcr2Cnts:0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].eUseDirFeature:DO_NOT_USE
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].slSteerRotation:0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].eUseWalkieMode:DO_NOT_USE
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].eUseRabbitTurtleMode:DO_NOT_USE
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].ulEndStopZoneCW:0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].ulEndStopZoneCCW:0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].slAttTurtleWhAngLmt[0]:0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].slQPWhAngLmt[0]:0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rTil2WhRatio:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].ulSpdModeCntPerTickMax:0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rSpdModeKp:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rSpdModeKi:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rSpdModeKd:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rStrTs:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rSpdModeDerFltrCoef:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rStrGearRatio:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rStrSpdDeadBand:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].ulStrSpdUpperBoundary:0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].ulStrSpdLowerBoundary:0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rStrSpdROTO:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rStrSpdDiffCntsFltrGain:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rTfdCurrent_VS_TractionSpeed_Speed[0]:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rTfdCurrent_VS_TractionSpeed_Current[0]:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rTfdCurrent_VS_SteeringError_Error[0]:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rTfdMaxCurrent:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rTfdReduction:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rTfdStiffCurrent:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rTfdRampDnHoldCurrent:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rTfdCurrentRampRateLimiter:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rTfdRampDnHoldTime:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rTfdHoldUpSpeedMax:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rStrMtrZeroSpd:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].slStrMtrSpdLmt:0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rStrMtrSpdRedTrxSpdBP:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rStrMtrSpdRedMaxTrxSpd:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rStrMtrSpdMaxTrxSpdRf:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rStrMtrSpdRedWhAngBP:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rStrMtrSpdRedMaxWhAng:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rStrMtrSpdMaxWhAngRf:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].ulTrigFunct:0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].ulStrMtrSpdWhAngRfType:0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rTilScalingTrxSpdBP:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rTilScalingTrxSpdMax:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rTilScalingMaxTrxSpdSf:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rTillerMotionThreshold:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rCmd1_RO_Upr:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rCmd1_RO_Lwr:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rCmd1_Rng_Upr:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rCmd1_Rng_Lwr:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rAssistGain:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rStrCmdDeadBand:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rTrxSpdWhAng_SpdOs:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].eUse360Feature:DO_NOT_USE
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rStrMtrSpdRedWhAngErrBP:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rStrMtrSpdRedMaxWhAngErr:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rStrMtrSpdMaxWhAngErrRf:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rHndlSpdClip:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rHndlSpdFltrCoef:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rHndlSpdThrshld:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rWhAngTrgtFltGain:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rSahNmbnsTlr2DuSf:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rStrNoEdRequired:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rSteerAngleBeforeED:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].eSteer_Direction:Forward
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].slSensorSignalRelation:0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rBumpCancelThrshld:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rDeadBandGain:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rDeadBandHyst:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.gpSteer_params[0].rDeadBandReEntDly:0.0
TEST.EXPECTED:TfdCurrentSpProfile.<<GLOBAL>>.TfdOpMode_rSteerError:0.0
TEST.END

-- Subprogram: SpdCmd_Input_1_OOR

-- Test Case: SpdCmd_Input_1_OOR.001
TEST.UNIT:SpdCmd
TEST.SUBPROGRAM:SpdCmd_Input_1_OOR
TEST.NEW
TEST.NAME:SpdCmd_Input_1_OOR.001
TEST.COMPOUND_ONLY
TEST.STUB:SpdCmd.SpdCmd_FwdLoad
TEST.VALUE:SpdCmd.SpdCmd_Input_1_OOR.rty_Out1:<<malloc 1>>
TEST.VALUE:SpdCmd.SpdCmd_FwdLoad.rty_Out1[0]:1.0
TEST.EXPECTED:SpdCmd.SpdCmd_Input_1_OOR.rty_Out1[0]:0.0
TEST.END

-- Subprogram: SpdCmd_RevLoad

-- Test Case: SpdCmd_RevLoad.001
TEST.UNIT:SpdCmd
TEST.SUBPROGRAM:SpdCmd_RevLoad
TEST.NEW
TEST.NAME:SpdCmd_RevLoad.001
TEST.COMPOUND_ONLY
TEST.VALUE:SpdCmd.SpdCmd_RevLoad.rty_Out1:<<malloc 1>>
TEST.EXPECTED:SpdCmd.SpdCmd_RevLoad.rty_Out1[0]:10.0
TEST.END

-- Subprogram: SpdCmd_initialize

-- Test Case: SpdCmd_initialize.001
TEST.UNIT:SpdCmd
TEST.SUBPROGRAM:SpdCmd_initialize
TEST.NEW
TEST.NAME:SpdCmd_initialize.001
TEST.COMPOUND_ONLY
TEST.STUB:SpdCmd.SpdCmd_Input_1_OOR
TEST.STUB:SpdCmd.SpdCmd_ForceMonoinc
TEST.STUB:SpdCmd.SpdCmd_FwdLoad
TEST.STUB:SpdCmd.SpdCmd_RevLoad
TEST.STUB:SpdCmd.SpdCmd_FwdLdd1
TEST.STUB:SpdCmd.SpdCmd_FwdLdd
TEST.STUB:SpdCmd.SpdCmd_Init
TEST.STUB:SpdCmd.SpdCmd
TEST.VALUE:AssistCmd.<<GLOBAL>>.gpSteer_params:<<malloc 1>>
TEST.VALUE:AssistCmd.<<GLOBAL>>.gpSteer_params[0].eSensorType:ENCODER
TEST.VALUE:AssistCmd.<<GLOBAL>>.gpSteer_params[0].eControlMode:ASSIST
TEST.VALUE:AssistCmd.<<GLOBAL>>.gpSteer_params[0].eUseDirFeature:DO_NOT_USE
TEST.VALUE:AssistCmd.<<GLOBAL>>.gpSteer_params[0].slSteerRotation:0
TEST.VALUE:AssistCmd.<<GLOBAL>>.gpSteer_params[0].eUseWalkieMode:DO_NOT_USE
TEST.VALUE:AssistCmd.<<GLOBAL>>.gpSteer_params[0].rTil2WhRatio:0.0
TEST.VALUE:AssistCmd.<<GLOBAL>>.gpSteer_params[0].ulSpdModeCntPerTickMax:0
TEST.VALUE:SpdCmd.<<GLOBAL>>.gpTraction_performance:<<malloc 1>>
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.StrSpdCmd_rCmd1:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.StrSpd_rTrxSpd:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.StrSpd_rWhAng:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.StrSpdCmd_rCmd2:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.StrSpdCmd_rDiffCmd:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.StrSpd_rWhAngTrgt:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rAccelCal_SPEED:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rAccelLimitForkLoad[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rAccelLimitReachPosition[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rAccelLimitsFWD_PC1[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rAccelLimitsFWD_PC2[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rAccelLimitsFWD_PC3[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rAccelLimitsREV_PC1[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rAccelLimitsREV_PC2[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rAccelLimitsREV_PC3[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rAngleHys:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rBRES_DECEL:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rBRES_SPEED:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rBrs1_BrakeLevel:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rBrs2_BrakeLevel:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rBrs3_BrakeLevel:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rCmdLimit_DECEL:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rCoastResetRate:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rCoastSteerAngle:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rDecelLimitForkLoad[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rDecelLimitReachPosition[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rDMS_COAST_FOR[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rDMS_COAST_REV[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rDMS_SPEED:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rDMS_SPEED_FOR:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rDMS_SPEED_REV:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rEntry_DECEL:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rEntry_SPEED:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rFL_Offset[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rGate_DECEL:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rGate_SPEED:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rGrade_DECEL:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rGradeSpeedLimitAngle:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rLimitsSpeed[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rLms5_DECEL:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rLms5_SPEED:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rLoadShed_DECEL:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rNoWalkAlong_DECEL:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rPBRF_REDUCTION_0_3[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rPBRF_THROTTLE_0_3[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rPBRF1_ANGLE_0_3[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rPBRF1_HEIGHT:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rPERF_HT0_5[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rQuickerResponseThrottlePercent:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rReachSpeedLimit[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rSPEED_LIMIT_VS_ANGLE_HI_FWD_EMPTY_ANGLE[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rSPEED_LIMIT_VS_ANGLE_HI_FWD_EMPTY_SPEED[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rSPEED_LIMIT_VS_ANGLE_HI_FWD_LOADED_SPEED[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rSPEED_LIMIT_VS_ANGLE_HI_REV_EMPTY_ANGLE[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rSPEED_LIMIT_VS_ANGLE_HI_REV_EMPTY_SPEED[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rSPEED_LIMIT_VS_ANGLE_HI_REV_LOADED_ANGLE[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rSPEED_LIMIT_VS_ANGLE_HI_REV_LOADED_SPEED[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rSPEED_LIMIT_VS_ANGLE_NORMAL_FWD_LOADED_LOAD:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rSPEED_LIMIT_VS_ANGLE_NORMAL_REV_LOADED_LOAD:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rSPEED_LIMIT_VS_ANGLE_PICK_FWD_LOADED_ANGLE[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rSPEED_LIMIT_VS_ANGLE_PICK_FWD_LOADED_LOAD:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rSPEED_LIMIT_VS_ANGLE_PICK_REV_EMPTY_SPEED[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rSPEED_LIMIT_VS_ANGLE_PICK_REV_LOADED_ANGLE[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rSPEED_LIMIT_VS_ANGLE_WALKIE_FWD_EMPTY_SPEED[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rSPEED_LIMIT_VS_ANGLE_WALKIE_FWD_LOADED_ANGLE[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rSPEED_LIMIT_VS_ANGLE_WALKIE_FWD_LOADED_LOAD:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rSPEED_LIMIT_VS_ANGLE_WALKIE_REV_LOADED_LOAD:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rSPEED_LIMIT_VS_ANGLE_WALKIE_REV_LOADED_SPEED[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rSPEED_LIMIT_VS_GRADE_LOAD[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rSPEED_LIMIT_VS_GRADE_UP_GRADE_SPEED[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rSPEED_VS_HEIGHT_FWD[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rSPEED_VS_HEIGHT_FWD_HEIGHT[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rSPEED_VS_HEIGHT_REV[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rSPEED_VS_HEIGHT_REV_HEIGHT[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rSteerAngleCoast:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rSteps_DECEL:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rSteps_SPEED:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rTARF1_ANGLE_0_3[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rTARF1_REDUCTION_0_3[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rTARF2_REDUCTION_0_4[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rTARF2_SPEED_0_4[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rTARF3_THROTTLE_0_3[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rTBRF_REDUCTION_0_3[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rTBRF_THROTTLE_0_3[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rTBRF1_ANGLE_0_3[0]:0.0
TEST.EXPECTED:SpdCmd.<<GLOBAL>>.gpTraction_performance[0].rTBRF1_HEIGHT:0.0
TEST.END

-- Unit: SteeringSIM

-- Subprogram: SteeringSIM

-- Test Case: SteeringSIM.001
TEST.UNIT:SteeringSIM
TEST.SUBPROGRAM:SteeringSIM
TEST.NEW
TEST.NAME:SteeringSIM.001
TEST.COMPOUND_ONLY
TEST.STUB:SteeringSIM.SteeringSIM_Init
TEST.STUB:SteeringSIM.SteeringSIM_initialize
TEST.VALUE:SteeringSIM.<<GLOBAL>>.gEvent_status_steer_bus.fStrSpCrosscheck1:0
TEST.VALUE:SteeringSIM.<<GLOBAL>>.gEvent_status_steer_bus.fStrSnsrMismatch:0
TEST.VALUE:SteeringSIM.<<GLOBAL>>.gEvent_status_steer_bus.fStrStopTractCrosscheck1:0
TEST.VALUE:SteeringSIM.<<GLOBAL>>.gEvent_status_steer_bus.fStrSpCalcCrosscheck1:0
TEST.VALUE:SteeringSIM.<<GLOBAL>>.gEvent_status_steer_bus.fStrFbCrosscheck1:0
TEST.VALUE:SteeringSIM.<<GLOBAL>>.gEvent_status_steer_bus.fTracSpdCrosscheck:0
TEST.VALUE:SteeringSIM.SteeringSIM.rtu_gpSensor_cals_c2m:<<malloc 1>>
TEST.VALUE:SteeringSIM.SteeringSIM.rtu_SlvSteerStat:<<malloc 1>>
TEST.VALUE:SteeringSIM.SteeringSIM.rtu_SlvSteerStat[0].fStrU1OkCrossChk:2
TEST.VALUE:SteeringSIM.SteeringSIM.rtu_SlvSteerStat[0].fStrStopTractCrossChk2:4
TEST.VALUE:SteeringSIM.SteeringSIM.rtu_SlvSteerStat[0].fStrSpEstCrossChk2:5
TEST.VALUE:SteeringSIM.SteeringSIM.rtu_SlvSteerStat[0].fStrSpCrossChk2:6
TEST.VALUE:SteeringSIM.SteeringSIM.rtu_SlvSteerStat[0].fStrFbCrossChk2:10
TEST.VALUE:SteeringSIM.SteeringSIM.rtu_SlvSteerStat[0].fStrSnsrCrossChk2:9
TEST.VALUE:SteeringSIM.SteeringSIM.rtu_SlvSteerStat[0].fTracSpdCrossChk2:4
TEST.VALUE:SteeringSIM.SteeringSIM.rtu_SlvSteerStat[0].swSteerSetPoint1U2:0
TEST.VALUE:SteeringSIM.SteeringSIM.rtu_EventStatVcmInput:<<malloc 1>>
TEST.VALUE:SteeringSIM.SteeringSIM.rtu_EventStatVcmInput[0].fFourToTwentyOrl:0
TEST.VALUE:SteeringSIM.SteeringSIM.rtu_EventStatVcmInput[0].fFourToTwentyOrh:1
TEST.VALUE:SteeringSIM.SteeringSIM.rtu_EventStatVcmInput[0].fPot1Orh:2
TEST.VALUE:SteeringSIM.SteeringSIM.rtu_EventStatVcmInput[0].fPot1Orl:3
TEST.VALUE:SteeringSIM.SteeringSIM.rtu_EventStatVcmInput[0].fPot3InputOrl:4
TEST.VALUE:SteeringSIM.SteeringSIM.rtu_EventStatVcmInput[0].fPot3InputOrh:5
TEST.VALUE:SteeringSIM.SteeringSIM.rtu_EventStatVcmInput[0].fPot4InputOrl:6
TEST.VALUE:SteeringSIM.SteeringSIM.rtu_EventStatVcmInput[0].fPot4InputOrh:7
TEST.VALUE:SteeringSIM.SteeringSIM.rtu_EventStatVcmInput[0].fANA5VoltsOrl:8
TEST.VALUE:SteeringSIM.SteeringSIM.rtu_EventStatVcmInput[0].fANA5VoltsOrh:9
TEST.VALUE:SteeringSIM.SteeringSIM.rtu_EventStatVcmInput[0].fANA11VoltsOrl:0
TEST.VALUE:SteeringSIM.SteeringSIM.rtu_EventStatVcmInput[0].fANA11VoltsOrh:9
TEST.VALUE:SteeringSIM.SteeringSIM.rtu_EventStatVcmInput[0].fAna15VoltsOrl:8
TEST.VALUE:SteeringSIM.SteeringSIM.rtu_EventStatVcmInput[0].fAna15VoltsOrh:7
TEST.VALUE:SteeringSIM.SteeringSIM.rtu_EventStatVcmInput[0].fBatterySenseVoltsOrh:6
TEST.VALUE:SteeringSIM.SteeringSIM.rtu_EventStatVcmInput[0].fBatterySenseVoltsOrl:5
TEST.VALUE:SteeringSIM.SteeringSIM.rtu_EventStatVcmInput[0].fKysVoltsBnorEL:4
TEST.VALUE:SteeringSIM.SteeringSIM.rtu_EventStatVcmInput[0].fKysVoltsAnorEL:3
TEST.VALUE:SteeringSIM.SteeringSIM.rtu_EventStatVcmInput[0].fCssWithout:2
TEST.VALUE:SteeringSIM.SteeringSIM.rtu_EventStatVcmInput[0].fCssWith:1
TEST.VALUE:SteeringSIM.SteeringSIM.rtu_EventStatVcmInput[0].fCsCrosscheck:0
TEST.VALUE:SteeringSIM.SteeringSIM.rtu_EventStatVcmInput[0].fANA10VoltsOrh:0
TEST.VALUE:SteeringSIM.SteeringSIM.rtu_EventStatVcmInput[0].fANA10VoltsOrl:0
TEST.VALUE:SteeringSIM.SteeringSIM.rtu_EventStatVcmInput[0].fBrkFluidSw:0
TEST.VALUE:SteeringSIM.SteeringSIM.rtu_EventStatVcmInput[0].fIncorrectBattery:0
TEST.VALUE:SteeringSIM.SteeringSIM.rtu_AGV_output_bus_t:<<malloc 1>>
TEST.VALUE:SteeringSIM.SteeringSIM.rtu_AGV_output_bus_t[0].rHydraulicThrottle:0.0
TEST.VALUE:SteeringSIM.SteeringSIM.rtu_AGV_output_bus_t[0].rTractionThrottle:0.0
TEST.VALUE:StrMstrApp.<<GLOBAL>>.gpSteer_inputs:<<malloc 1>>
TEST.VALUE:StrMstrApp.<<GLOBAL>>.gpSteer_params:<<malloc 1>>
TEST.VALUE:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].ulFbEcr1Cnts:0
TEST.VALUE:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].ulFbEcr2Cnts:0
TEST.VALUE:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].slSteerRotation:0
TEST.VALUE:StrMstrApp.<<GLOBAL>>.gpFeatures_bus:<<malloc 1>>
TEST.VALUE:StrPosWhAngLimiter.<<GLOBAL>>.gpSteer_params:<<malloc 1>>
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gEvent_status_steer_bus.fStrSpCrosscheck1:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gEvent_status_steer_bus.fStrSnsrMismatch:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gEvent_status_steer_bus.fStrStopTractCrosscheck1:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gEvent_status_steer_bus.fStrSpCalcCrosscheck1:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gEvent_status_steer_bus.fStrFbCrosscheck1:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gEvent_status_steer_bus.fTracSpdCrosscheck:0
TEST.EXPECTED:SteeringSIM.SteeringSIM.rtu_SlvSteerStat[0].fStrU1OkCrossChk:2
TEST.EXPECTED:SteeringSIM.SteeringSIM.rtu_SlvSteerStat[0].fStrStopTractCrossChk2:4
TEST.EXPECTED:SteeringSIM.SteeringSIM.rtu_SlvSteerStat[0].fStrSpEstCrossChk2:5
TEST.EXPECTED:SteeringSIM.SteeringSIM.rtu_SlvSteerStat[0].fStrSpCrossChk2:6
TEST.EXPECTED:SteeringSIM.SteeringSIM.rtu_SlvSteerStat[0].fStrFbCrossChk2:10
TEST.EXPECTED:SteeringSIM.SteeringSIM.rtu_SlvSteerStat[0].fStrSnsrCrossChk2:9
TEST.EXPECTED:SteeringSIM.SteeringSIM.rtu_SlvSteerStat[0].fTracSpdCrossChk2:4
TEST.EXPECTED:SteeringSIM.SteeringSIM.rtu_SlvSteerStat[0].swSteerSetPoint1U2:0
TEST.EXPECTED:SteeringSIM.SteeringSIM.rtu_EventStatVcmInput[0].fFourToTwentyOrl:0
TEST.EXPECTED:SteeringSIM.SteeringSIM.rtu_EventStatVcmInput[0].fFourToTwentyOrh:1
TEST.EXPECTED:SteeringSIM.SteeringSIM.rtu_EventStatVcmInput[0].fPot1Orh:2
TEST.EXPECTED:SteeringSIM.SteeringSIM.rtu_EventStatVcmInput[0].fPot1Orl:3
TEST.EXPECTED:SteeringSIM.SteeringSIM.rtu_EventStatVcmInput[0].fPot3InputOrl:4
TEST.EXPECTED:SteeringSIM.SteeringSIM.rtu_EventStatVcmInput[0].fPot3InputOrh:5
TEST.EXPECTED:SteeringSIM.SteeringSIM.rtu_EventStatVcmInput[0].fPot4InputOrl:6
TEST.EXPECTED:SteeringSIM.SteeringSIM.rtu_EventStatVcmInput[0].fPot4InputOrh:7
TEST.EXPECTED:SteeringSIM.SteeringSIM.rtu_EventStatVcmInput[0].fANA5VoltsOrl:8
TEST.EXPECTED:SteeringSIM.SteeringSIM.rtu_EventStatVcmInput[0].fANA5VoltsOrh:9
TEST.EXPECTED:SteeringSIM.SteeringSIM.rtu_EventStatVcmInput[0].fANA11VoltsOrl:0
TEST.EXPECTED:SteeringSIM.SteeringSIM.rtu_EventStatVcmInput[0].fANA11VoltsOrh:9
TEST.EXPECTED:SteeringSIM.SteeringSIM.rtu_EventStatVcmInput[0].fAna15VoltsOrl:8
TEST.EXPECTED:SteeringSIM.SteeringSIM.rtu_EventStatVcmInput[0].fAna15VoltsOrh:7
TEST.EXPECTED:SteeringSIM.SteeringSIM.rtu_EventStatVcmInput[0].fBatterySenseVoltsOrh:6
TEST.EXPECTED:SteeringSIM.SteeringSIM.rtu_EventStatVcmInput[0].fKysVoltsBnorEL:4
TEST.EXPECTED:SteeringSIM.SteeringSIM.rtu_EventStatVcmInput[0].fKysVoltsAnorEL:3
TEST.EXPECTED:SteeringSIM.SteeringSIM.rtu_EventStatVcmInput[0].fCssWithout:2
TEST.EXPECTED:SteeringSIM.SteeringSIM.rtu_EventStatVcmInput[0].fCssWith:1
TEST.EXPECTED:SteeringSIM.SteeringSIM.rtu_EventStatVcmInput[0].fCsCrosscheck:0
TEST.EXPECTED:SteeringSIM.SteeringSIM.rtu_EventStatVcmInput[0].fANA10VoltsOrh:0
TEST.EXPECTED:SteeringSIM.SteeringSIM.rtu_EventStatVcmInput[0].fANA10VoltsOrl:0
TEST.EXPECTED:SteeringSIM.SteeringSIM.rtu_EventStatVcmInput[0].fBrkFluidSw:0
TEST.EXPECTED:SteeringSIM.SteeringSIM.rtu_EventStatVcmInput[0].fIncorrectBattery:0
TEST.EXPECTED:SteeringSIM.SteeringSIM.rtu_AGV_output_bus_t[0].rHydraulicThrottle:0.0
TEST.EXPECTED:SteeringSIM.SteeringSIM.rtu_AGV_output_bus_t[0].rTractionThrottle:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].eSensorType:ENCODER
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].eControlMode:ASSIST
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].ulFbEcr1Cnts:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].eUseDirFeature:DO_NOT_USE
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].slSteerRotation:0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpSteer_params[0].eSteer_Direction:Forward
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpFeatures_bus[0].eHighSpeedLowerType:NO_HSL
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpFeatures_bus[0].eSteeringType:MANUAL
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpFeatures_bus[0].eSteerDirection:Forward
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpFeatures_bus[0].eAuxHoistType:NO_AUX
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpFeatures_bus[0].rTRUCK_WEIGHT:0.0
TEST.EXPECTED:StrMstrApp.<<GLOBAL>>.gpFeatures_bus[0].rLIFT_HGT:0.0
TEST.END

-- Subprogram: SteeringSIM_Init

-- Test Case: SteeringSIM_Init.001
TEST.UNIT:SteeringSIM
TEST.SUBPROGRAM:SteeringSIM_Init
TEST.NEW
TEST.NAME:SteeringSIM_Init.001
TEST.COMPOUND_ONLY
TEST.STUB:SteeringSIM.SteeringSIM_initialize
TEST.VALUE:SteeringSIM.<<GLOBAL>>.gpSteer_inputs:<<malloc 1>>
TEST.VALUE:SteeringSIM.<<GLOBAL>>.gpSteering_fdbk:<<malloc 1>>
TEST.VALUE:SteeringSIM.<<GLOBAL>>.StrPos_rTrxSpd:1.0
TEST.VALUE:SteeringSIM.<<GLOBAL>>.gSteer_m2m_outputs_rWhAngTarget:2.0
TEST.VALUE:SteeringSIM.<<GLOBAL>>.gSteer_m2m_outputs_rStrSpd_DiffCnts:3.0
TEST.VALUE:SteeringSIM.<<GLOBAL>>.Steering_SIM_Code_Version:3
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteer_m2m_outputs.fStopTrac:1
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteer_m2m_outputs.uwSdmPwrCmdMode:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteer_m2m_outputs.rWhAngTarget:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteer_m2m_outputs.fStrCmdActive:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteer_m2m_outputs.fStrMtrMoving:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteer_m2m_outputs.eStrOpMode_State:STR_OP_MODE_Init
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteer_m2m_outputs.fSteerOkClk:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteer_m2m_outputs.rTrxSpdLmtWhAngCmd:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteer_m2m_outputs.fDuInRng:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteer_m2m_outputs.fStopSteering:1
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteer_m2m_outputs.slFwdRevSteer:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteer_m2m_outputs.slStrPos_HndlPos:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteer_m2m_outputs.rStrSpd_DiffCnts:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteer_sim_alarms.fSnsrMismatchAlarm:1
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteer_sim_alarms.fSpEstAlarm:1
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteer_sim_alarms.fSetPntAlarm:1
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteer_sim_alarms.fFbAlarm:1
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteer_sim_alarms.fStopTracAlarm:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteer_sim_alarms.fMstrOkAlarm:1
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteer_sim_alarms.fTracSpdAlarm:1
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteer_sim_alarms.fDirChngAlarm:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteer_sim_outputs.slMstrStrCmd:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteer_sim_outputs.slSlvStrCmd:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteer_sim_outputs.fSteeringEnabled:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteer_sim_outputs.rTfdCmd:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteer_sim_outputs.slStrMtrSpdMax:3000
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteer_sim_outputs.ulStrCmdFreshCntr:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteer_sim_outputs.ulStopSteering:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteering_sim_sro.fSteeringThrottleEnable:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteering_sim_sro.fScmHighTempSensor:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteering_sim_sro.fRabbitTurtle:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteering_sim_sro.fWalkieHndlRange:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteering_sim_sro.fPreventRbtTrtlSw:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteering_sim_sro.fPptIndexingAdvise:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteering_sim_sro.fStrNoEdBrkTSTSro:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteering_sim_sro.fSteerThruIndex:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteering_sim_sro.fRotateDriveUnit:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteering_sim_sro.fSteerAllowed:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gEd_outputs.ulTcmCmd:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gEd_outputs.ulScmCmd:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gEd_outputs.ulHcmCmd:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gEd_outputs.ulEdDriversOn:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gEd_outputs.ulHdmCmd:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gEd_outputs.fEdClosed:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gEd_outputs.fEdWillDrop:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gEd_outputs.fPwrDrvAllowed:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gEd_outputs.uwFaultDetected:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gEd_outputs.eEdState:ED_NoState
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gEd_outputs.rBusVoltsStart:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gEd_outputs.fNotActPwrDwn:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gOperator_interface_output.fTruckAttended:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gOperator_interface_output.fTrxRun:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gOperator_interface_output.fHydRun1:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gOperator_interface_output.fHydRun2:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gOperator_interface_output.fHydRun3:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gOperator_interface_output.fBrake:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gOperator_interface_output.fBrkTstComp:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gOperator_interface_output.fPdlTstComp:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gOperator_interface_output.fTrxTieDownActive:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gOperator_interface_output.fHyd1TieDownActive:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gOperator_interface_output.fHyd2TieDownActive:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gOperator_interface_output.fZoneChange:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gOperator_interface_output.fCmd1:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gOperator_interface_output.fCmd2:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gOperator_interface_output.eCmdLmtMode:WALKIE
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gOperator_interface_output.fEML:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gOperator_interface_output.fTowMode_Attended:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gTraction_sim_output.rTorqueCmd:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gTraction_sim_output.rBrake1Cmd:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gTraction_sim_output.rBrake2Cmd:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gTraction_sim_output.rBrake3Cmd:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gTraction_sim_output.rVehicleSpeed:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gTraction_sim_output.rSpeedCommand:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gTraction_sim_output.rDistanceTraveled:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gTraction_sim_output.ulFwdSwitch:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gTraction_sim_output.ulRevSwitch:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gTraction_sim_output.fBrakeRequest:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gTraction_sim_output.fBrake_Coils_In_Lockout:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gTraction_sim_output.eTractionState:TRACT_STATE_PARK
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gTraction_sim_output.ulTravelAlarmCmd:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gTraction_sim_output.eTractionMotorState:TRACT_MOTOR_STATE_MOTORING
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gTraction_sim_output.fTrxAppClock:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gTraction_sim_output.fSlippageActiveStat:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gTraction_sim_output.rAccelCommanded:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gTraction_sim_output.rAccelMeasured:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gTraction_sim_output.rAccelCalculated:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gTraction_sim_output.rX_Speed:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gTraction_sim_output.slDirectionFlipper:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gTraction_sim_output.fStatusWordOK:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gTraction_sim_output.fSpeedCutbackSRO:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gTraction_sim_output.ulStrobeCmd:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gTraction_sim_output.rBrake1TorqueGap:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gTraction_sim_output.rBrake2TorqueGap:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gEvent_status_steer_bus.fStrSpCrosscheck1:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gEvent_status_steer_bus.fStrSnsrMismatch:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gEvent_status_steer_bus.fStrStopTractCrosscheck1:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gEvent_status_steer_bus.fStrSpCalcCrosscheck1:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gEvent_status_steer_bus.fStrFbCrosscheck1:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gEvent_status_steer_bus.fTracSpdCrosscheck:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gpSteering_fdbk[0].MstrFdbk.rStrPos:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gpSteering_fdbk[0].MstrFdbk.rStrSpd:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gpSteering_fdbk[0].MstrFdbk.rStrCur:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gpSteering_fdbk[0].MstrFdbk.ulStopTrx:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gpSteering_fdbk[0].MstrFdbk.ulFreshCnt:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gpSteering_fdbk[0].SlvFdbk.rStrPos:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gpSteering_fdbk[0].SlvFdbk.rStrSpd:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gpSteering_fdbk[0].SlvFdbk.rStrCur:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gpSteering_fdbk[0].SlvFdbk.ulStopTrx:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.StrPos_rTrxSpd:1.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteer_m2m_outputs_rWhAngTarget:2.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteer_m2m_outputs_rStrSpd_DiffCnts:3.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.Steering_SIM_Code_Version:3
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteer_m2m_outputs_eStrOpMode_State:STR_OP_NoState
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteer_m2m_outputs_uwSdmPwrCmdMode:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteer_m2m_outputs_fStopTrac:1
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.StrMstSup_fStrAllowed:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteer_m2m_outputs_fStopSteering:1
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.StrPosSroDetect_fHss:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteer_m2m_outputs_fStrCmdActive:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteer_m2m_outputs_fStrMtrMoving:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteering_sim_sro_fStrNoEdBrkTSTSro:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteering_sim_sro_fRabbitTurtle:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteering_sim_sro_fWalkieHndlRange:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteering_sim_sro_fSteeringThrottleEnable:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteering_sim_sro_fPreventRbtTrtlSw:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteering_sim_sro_fPptIndexingAdvise:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteering_sim_sro_fSteerThruIndex:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteering_sim_sro_fRotateDriveUnit:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteering_sim_sro_fScmHighTempSensor:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteering_sim_sro_fSteerAllowed:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.SteeringSIM_DW.gSteer_m2m_outputs_rTrxSpdLmtWh:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.SteeringSIM_DW.gSteer_m2m_outputs_slFwdRevStee:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.SteeringSIM_DW.gSteer_m2m_outputs_slStrPos_Hnd:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.SteeringSIM_DW.gSteer_m2m_outputs_fSteerOkClk:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.SteeringSIM_DW.gSteer_m2m_outputs_fDuInRng:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.SteeringSIM_DW.Str_App_o13:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.SteeringSIM_DW.Str_App_o15:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.SteeringSIM_DW.UnitDelay3_DSTATE:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.SteeringSIM_DW.UnitDelay4_DSTATE:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.SteeringSIM_DW.UnitDelay2_DSTATE:1
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gVehicleFB_output.rForkHeight:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gVehicleFB_output.rForkLoad:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gVehicleFB_output.rMainHeight:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gVehicleFB_output.rMainHoistSpeed:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gVehicleFB_output.rAuxHeight:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gVehicleFB_output.rAuxHoistSpeed:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gVehicleFB_output.rTiltPosition:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gVehicleFB_output.rTiltSpeed:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gVehicleFB_output.rTiltLevelAngle:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gVehicleFB_output.rAccy1Position:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gVehicleFB_output.rAccy1Speed:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gVehicleFB_output.fForkHome:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gVehicleFB_output.rAccy2Position:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gVehicleFB_output.rAccy2Speed:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gVehicleFB_output.rGrade_truck_x_axis:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gVehicleFB_output.rTruck_Pitch_angle:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gVehicleFB_output.rTruck_Roll_angle:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gVehicleFB_output.cald_truck_x_axis_raw_accel:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gVehicleFB_output.cald_truck_y_axis_raw_accel:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gVehicleFB_output.cald_truck_z_axis_raw_accel:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gVehicleFB_output.eMastZone:BELOW_PRIMARY_RESET_SWITCH
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gVehicleFB_output.ePriHoistDirDelta:HgtFb_Raise
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gVehicleFB_output.eSecHoistDirDelta:HgtFb_Raise
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gVehicleFB_output.rPriHoistSpeed:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gVehicleFB_output.rSecHoistSpeed:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gVehicleFB_output.rPriHgtCountsCAL:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gVehicleFB_output.rSecHgtCountsCAL:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gVehicleFB_output.fMainHydMtrEncoderOk:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gVehicleFB_output.fAccyHydMtrEncoderOk:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gVehicleFB_output.fTracMtr1EncoderOk:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gVehicleFB_output.fTracMtr2EncoderOk:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gVehicleFB_output.rFreeLiftHeight:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gVehicleFB_output.rDisplay_Load_weight:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gVehicleFB_output.fTestForProductivityLoaded:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gVehicleFB_output.rTruckAccel_from_accelerometer:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gVehicleFB_output.rVehicleSpeed_from_accelerometer:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gVehicleFB_output.rTruck_Yaw_Angle:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gVehicleFB_output.rRaw_gyro_Truck_x_axis:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gVehicleFB_output.rRaw_gyro_Truck_y_axis:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gVehicleFB_output.rRaw_gyro_Truck_z_axis:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gVehicleFB_output.eAboveHeightLimit:NOT_ABOVE_HEIGHT_LIMIT
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gVehicleFB_output.fAbove_Free_lift_height:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gVehicleFB_output.rDisplayHoistSpeed:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gVehicleFB_output.fVirtMainHeightPrecisionLoss:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gVehicleFB_output.fVirtMainHeightValid:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gVehicleFB_output.rVirtMainHeight_m:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gVehicleFB_output.fTpaLevel:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gVehicleFB_output.ulCdmWeightHeightLimit:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gVehicleFB_output.ubCdmEvent:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gThrottle_out.Throttle_output_traction1.rThrottleSignal:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gThrottle_out.Throttle_output_traction1.ubThrottleEvent:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gThrottle_out.Throttle_output_traction1.fValidThrottle:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gThrottle_out.Throttle_output_traction1.fThrottleEnable:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gThrottle_out.Throttle_output_traction1.sbThrottleDirection:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gThrottle_out.Throttle_output_traction1.sbMotorDirection:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gThrottle_out.Throttle_output_traction1.eDirState:Dir_State_Quad1_pos
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gThrottle_out.Throttle_output_traction2.rThrottleSignal:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gThrottle_out.Throttle_output_traction2.ubThrottleEvent:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gThrottle_out.Throttle_output_traction2.fValidThrottle:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gThrottle_out.Throttle_output_traction2.fThrottleEnable:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gThrottle_out.Throttle_output_traction2.sbThrottleDirection:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gThrottle_out.Throttle_output_traction2.sbMotorDirection:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gThrottle_out.Throttle_output_traction2.eDirState:Dir_State_Quad1_pos
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSw_flag_outputs.fBrs1Flag:0
TEST.END

-- Subprogram: SteeringSIM_initialize

-- Test Case: SteeringSIM_initialize.001
TEST.UNIT:SteeringSIM
TEST.SUBPROGRAM:SteeringSIM_initialize
TEST.NEW
TEST.NAME:SteeringSIM_initialize.001
TEST.COMPOUND_ONLY
TEST.STUB:SteeringSIM.SteeringSIM_Init
TEST.STUB:SteeringSIM.SteeringSIM
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteer_m2m_outputs.eStrOpMode_State:STR_OP_MODE_Init
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteer_m2m_outputs.fSteerOkClk:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteer_m2m_outputs.rTrxSpdLmtWhAngCmd:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteer_m2m_outputs.fDuInRng:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteer_m2m_outputs.fStopSteering:1
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteer_m2m_outputs.slFwdRevSteer:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteer_m2m_outputs.slStrPos_HndlPos:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteer_m2m_outputs.rStrSpd_DiffCnts:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteer_sim_alarms.fSnsrMismatchAlarm:1
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteer_sim_alarms.fSpEstAlarm:1
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteer_sim_alarms.fSetPntAlarm:1
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteer_sim_alarms.fFbAlarm:1
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteer_sim_alarms.fStopTracAlarm:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteer_sim_alarms.fMstrOkAlarm:1
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteer_sim_alarms.fTracSpdAlarm:1
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteer_sim_alarms.fDirChngAlarm:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteer_sim_outputs.slMstrStrCmd:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteer_sim_outputs.slSlvStrCmd:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteer_sim_outputs.fSteeringEnabled:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteer_sim_outputs.rTfdCmd:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteer_sim_outputs.slStrMtrSpdMax:3000
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteer_sim_outputs.ulStrCmdFreshCntr:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteer_sim_outputs.ulStopSteering:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteering_sim_sro.fSteeringThrottleEnable:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteering_sim_sro.fWalkieHndlRange:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteering_sim_sro.fPreventRbtTrtlSw:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteering_sim_sro.fPptIndexingAdvise:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteering_sim_sro.fStrNoEdBrkTSTSro:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteering_sim_sro.fSteerThruIndex:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteering_sim_sro.fRotateDriveUnit:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteering_sim_sro.fSteerAllowed:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gEd_outputs.ulTcmCmd:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gEd_outputs.ulScmCmd:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gEd_outputs.ulHcmCmd:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gEd_outputs.ulEdDriversOn:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gEd_outputs.ulHdmCmd:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gEd_outputs.fEdClosed:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gEd_outputs.fEdWillDrop:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gEd_outputs.fPwrDrvAllowed:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gEd_outputs.uwFaultDetected:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gEd_outputs.rBusVoltsStart:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gEd_outputs.fNotActPwrDwn:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gOperator_interface_output.fTruckAttended:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gOperator_interface_output.fTrxRun:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gOperator_interface_output.fHydRun1:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gOperator_interface_output.fHydRun2:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gOperator_interface_output.fHydRun3:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gOperator_interface_output.fBrake:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gOperator_interface_output.fBrkTstComp:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gOperator_interface_output.fPdlTstComp:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gOperator_interface_output.fTrxTieDownActive:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gOperator_interface_output.eCmdLmtMode:WALKIE
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gOperator_interface_output.fEML:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gOperator_interface_output.fTowMode_Attended:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gTraction_sim_output.rBrake2Cmd:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gTraction_sim_output.rBrake3Cmd:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gTraction_sim_output.rSpeedCommand:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gTraction_sim_output.rDistanceTraveled:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gTraction_sim_output.ulFwdSwitch:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gTraction_sim_output.ulRevSwitch:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gTraction_sim_output.fBrakeRequest:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gTraction_sim_output.fBrake_Coils_In_Lockout:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gTraction_sim_output.eTractionState:TRACT_STATE_PARK
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gTraction_sim_output.ulTravelAlarmCmd:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gTraction_sim_output.eTractionMotorState:TRACT_MOTOR_STATE_MOTORING
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gTraction_sim_output.fTrxAppClock:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gTraction_sim_output.fSlippageActiveStat:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gTraction_sim_output.rAccelCommanded:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gTraction_sim_output.rAccelMeasured:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gTraction_sim_output.slDirectionFlipper:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gTraction_sim_output.fStatusWordOK:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gTraction_sim_output.fSpeedCutbackSRO:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gTraction_sim_output.ulStrobeCmd:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gTraction_sim_output.rBrake1TorqueGap:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gTraction_sim_output.rBrake2TorqueGap:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gEvent_status_steer_bus.fStrSpCrosscheck1:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gEvent_status_steer_bus.fStrSnsrMismatch:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gEvent_status_steer_bus.fStrStopTractCrosscheck1:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gEvent_status_steer_bus.fStrSpCalcCrosscheck1:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gEvent_status_steer_bus.fStrFbCrosscheck1:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gEvent_status_steer_bus.fTracSpdCrosscheck:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.StrPos_rTrxSpd:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteer_m2m_outputs_rWhAngTarget:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteer_m2m_outputs_rStrSpd_DiffCnts:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.Steering_SIM_Code_Version:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteer_m2m_outputs_eStrOpMode_State:STR_OP_NoState
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteer_m2m_outputs_uwSdmPwrCmdMode:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteer_m2m_outputs_fStopTrac:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.StrMstSup_fStrAllowed:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteer_m2m_outputs_fStopSteering:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.StrPosSroDetect_fHss:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteer_m2m_outputs_fStrCmdActive:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteer_m2m_outputs_fStrMtrMoving:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteering_sim_sro_fStrNoEdBrkTSTSro:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteering_sim_sro_fRabbitTurtle:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteering_sim_sro_fWalkieHndlRange:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteering_sim_sro_fSteeringThrottleEnable:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteering_sim_sro_fPreventRbtTrtlSw:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteering_sim_sro_fPptIndexingAdvise:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteering_sim_sro_fSteerThruIndex:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteering_sim_sro_fRotateDriveUnit:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteering_sim_sro_fScmHighTempSensor:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.gSteering_sim_sro_fSteerAllowed:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.SteeringSIM_DW.gSteer_m2m_outputs_rTrxSpdLmtWh:0.0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.SteeringSIM_DW.gSteer_m2m_outputs_slFwdRevStee:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.SteeringSIM_DW.gSteer_m2m_outputs_slStrPos_Hnd:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.SteeringSIM_DW.gSteer_m2m_outputs_fSteerOkClk:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.SteeringSIM_DW.Str_App_o15:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.SteeringSIM_DW.UnitDelay3_DSTATE:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.SteeringSIM_DW.UnitDelay4_DSTATE:0
TEST.EXPECTED:SteeringSIM.<<GLOBAL>>.SteeringSIM_DW.UnitDelay2_DSTATE:0
TEST.END

-- Unit: SteeringSIMbuild

-- Subprogram: SteeringSIMbuild_initialize

-- Test Case: SteeringSIMbuild_initialize.001
TEST.UNIT:SteeringSIMbuild
TEST.SUBPROGRAM:SteeringSIMbuild_initialize
TEST.NEW
TEST.NAME:SteeringSIMbuild_initialize.001
TEST.COMPOUND_ONLY
TEST.STUB:SteeringSIMbuild.SteeringSIMbuild_step
TEST.STUB:SteeringSIMbuild.SteeringSIMbuild_terminate
TEST.VALUE:SteeringSIMbuild.<<GLOBAL>>.gpSensor_cals_c2m:<<malloc 1>>
TEST.VALUE:SteeringSIMbuild.<<GLOBAL>>.gpSlave_steer_status:<<malloc 1>>
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gSteer_m2m_outputs.fStopTrac:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gSteer_m2m_outputs.uwSdmPwrCmdMode:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gSteer_m2m_outputs.rWhAngTarget:0.0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gSteer_m2m_outputs.fStrCmdActive:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gSteer_m2m_outputs.fStrMtrMoving:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gSteer_m2m_outputs.eStrOpMode_State:STR_OP_NoState
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gSteer_m2m_outputs.fSteerOkClk:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gSteer_m2m_outputs.rTrxSpdLmtWhAngCmd:0.0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gSteer_m2m_outputs.fDuInRng:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gSteer_m2m_outputs.fStopSteering:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gSteer_m2m_outputs.rStrSpd_DiffCnts:0.0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gSteer_sim_alarms.fSnsrMismatchAlarm:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gSteer_sim_alarms.fSpEstAlarm:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gSteer_sim_alarms.fSetPntAlarm:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gSteer_sim_alarms.fFbAlarm:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gSteer_sim_alarms.fStopTracAlarm:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gSteer_sim_alarms.fMstrOkAlarm:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gSteer_sim_alarms.fTracSpdAlarm:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gSteer_sim_alarms.fDirChngAlarm:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gSteer_sim_outputs.slMstrStrCmd:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gSteer_sim_outputs.slSlvStrCmd:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gSteer_sim_outputs.fSteeringEnabled:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gSteer_sim_outputs.slStrMtrSpdMax:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gSteer_sim_outputs.ulStopSteering:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gSteering_sim_sro.fSteeringThrottleEnable:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gSteering_sim_sro.fScmHighTempSensor:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gSteering_sim_sro.fRabbitTurtle:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gSteering_sim_sro.fWalkieHndlRange:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gSteering_sim_sro.fPreventRbtTrtlSw:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gSteering_sim_sro.fPptIndexingAdvise:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gSteering_sim_sro.fStrNoEdBrkTSTSro:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gSteering_sim_sro.fSteerThruIndex:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gSteering_sim_sro.fRotateDriveUnit:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gSteering_sim_sro.fSteerAllowed:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gEvent_status_vcmin_bus.fFourToTwentyOrl:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gEvent_status_vcmin_bus.fFourToTwentyOrh:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gEvent_status_vcmin_bus.fPot1Orh:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gEvent_status_vcmin_bus.fPot1Orl:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gEvent_status_vcmin_bus.fPot3InputOrl:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gEvent_status_vcmin_bus.fPot3InputOrh:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gEvent_status_vcmin_bus.fPot4InputOrl:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gEvent_status_vcmin_bus.fPot4InputOrh:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gEvent_status_vcmin_bus.fANA5VoltsOrl:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gEvent_status_vcmin_bus.fANA5VoltsOrh:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gEvent_status_vcmin_bus.fANA11VoltsOrl:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gEvent_status_vcmin_bus.fANA11VoltsOrh:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gEvent_status_vcmin_bus.fAna15VoltsOrl:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gEvent_status_vcmin_bus.fAna15VoltsOrh:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gEvent_status_vcmin_bus.fBatterySenseVoltsOrh:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gEvent_status_vcmin_bus.fBatterySenseVoltsOrl:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gEvent_status_vcmin_bus.fKysVoltsBnorEL:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gEvent_status_vcmin_bus.fKysVoltsAnorEL:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gEvent_status_vcmin_bus.fCssWithout:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gEvent_status_vcmin_bus.fCssWith:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gEvent_status_vcmin_bus.fCsCrosscheck:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gEvent_status_vcmin_bus.fANA10VoltsOrh:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gEvent_status_vcmin_bus.fANA10VoltsOrl:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gEvent_status_vcmin_bus.fBrkFluidSw:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gEvent_status_vcmin_bus.fIncorrectBattery:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gAGV_output.rHydraulicThrottle:0.0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gAGV_output.rTractionThrottle:0.0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gAGV_output.rTractionSpeedCmd:0.0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gAGV_output.rSteerCommand1:0.0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gAGV_output.rSteerCommand2:0.0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gAGV_output.rHeight:0.0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gAGV_output.ubTruckState:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gpSlave_steer_status[0].fStrU1OkCrossChk:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gpSlave_steer_status[0].fStrStopTractCrossChk2:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gpSlave_steer_status[0].fStrSpEstCrossChk2:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gpSlave_steer_status[0].fStrSpCrossChk2:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gpSlave_steer_status[0].fStrFbCrossChk2:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gpSlave_steer_status[0].fStrSnsrCrossChk2:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gpSlave_steer_status[0].fTracSpdCrossChk2:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gpSlave_steer_status[0].swSteerSetPoint1U2:0
TEST.END

-- Subprogram: SteeringSIMbuild_step

-- Test Case: SteeringSIMbuild_step.001
TEST.UNIT:SteeringSIMbuild
TEST.SUBPROGRAM:SteeringSIMbuild_step
TEST.NEW
TEST.NAME:SteeringSIMbuild_step.001
TEST.COMPOUND_ONLY
TEST.STUB:SteeringSIMbuild.SteeringSIMbuild_initialize
TEST.STUB:SteeringSIMbuild.SteeringSIMbuild_terminate
TEST.VALUE:SteeringSIMbuild.<<GLOBAL>>.gpSensor_cals_c2m:<<malloc 1>>
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gpSensor_cals_c2m[0].rSecHgtSensorCalGain:0.0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gpSensor_cals_c2m[0].rPriHgtSensorCalGain:0.0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gpSensor_cals_c2m[0].rPrimaryCalCountAtMax:0.0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gpSensor_cals_c2m[0].rAuxHtSensorCalGain:0.0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gpSensor_cals_c2m[0].rReachSensorMinMax[0]:0.0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gpSensor_cals_c2m[0].rTiltSensorMinZeroMax[0]:0.0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gpSensor_cals_c2m[0].rTiltSensorLevel:0.0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gpSensor_cals_c2m[0].rPivotSensorCalGain:0.0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gpSensor_cals_c2m[0].rTraverseSensorCalGain:0.0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gpSensor_cals_c2m[0].rLSOffset_Primary:0.0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gpSensor_cals_c2m[0].rLSOffset_Secondary:0.0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gpSensor_cals_c2m[0].rLSOffset_Aux_cyl:0.0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gpSensor_cals_c2m[0].rAccelXoffset:0.0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gpSensor_cals_c2m[0].rAccelYoffset:0.0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gpSensor_cals_c2m[0].rAccelZoffset:0.0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gpSensor_cals_c2m[0].rStrHndlCmd1[0]:0.0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gpSensor_cals_c2m[0].slHndlAng1[0]:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gpSensor_cals_c2m[0].rStrHndlCmd2[0]:0.0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gpSensor_cals_c2m[0].slHndlAng2[0]:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gpSensor_cals_c2m[0].slDuAng[0]:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gpSensor_cals_c2m[0].rMainHeightMax:0.0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gpSensor_cals_c2m[0].rCustomHeight:0.0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gSteer_m2m_outputs.fStopTrac:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gSteer_m2m_outputs.uwSdmPwrCmdMode:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gSteer_m2m_outputs.rWhAngTarget:0.0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gSteer_m2m_outputs.fStrCmdActive:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gSteer_m2m_outputs.fStrMtrMoving:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gSteer_m2m_outputs.eStrOpMode_State:STR_OP_NoState
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gSteer_m2m_outputs.fSteerOkClk:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gSteer_m2m_outputs.fDuInRng:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gSteer_m2m_outputs.fStopSteering:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gSteer_m2m_outputs.slFwdRevSteer:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gSteer_m2m_outputs.slStrPos_HndlPos:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gSteer_sim_alarms.fSnsrMismatchAlarm:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gSteer_sim_alarms.fSpEstAlarm:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gSteer_sim_alarms.fSetPntAlarm:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gSteer_sim_alarms.fFbAlarm:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gSteer_sim_alarms.fStopTracAlarm:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gSteer_sim_alarms.fMstrOkAlarm:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gSteer_sim_alarms.fTracSpdAlarm:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gSteer_sim_alarms.fDirChngAlarm:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gSteer_sim_outputs.slMstrStrCmd:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gSteer_sim_outputs.slSlvStrCmd:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gSteer_sim_outputs.fSteeringEnabled:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gSteer_sim_outputs.rTfdCmd:0.0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gSteer_sim_outputs.slStrMtrSpdMax:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gSteer_sim_outputs.ulStrCmdFreshCntr:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gSteer_sim_outputs.ulStopSteering:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gSteering_sim_sro.fSteeringThrottleEnable:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gSteering_sim_sro.fScmHighTempSensor:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gSteering_sim_sro.fRabbitTurtle:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gSteering_sim_sro.fWalkieHndlRange:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gSteering_sim_sro.fPreventRbtTrtlSw:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gSteering_sim_sro.fPptIndexingAdvise:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gSteering_sim_sro.fStrNoEdBrkTSTSro:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gSteering_sim_sro.fSteerThruIndex:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gSteering_sim_sro.fRotateDriveUnit:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gSteering_sim_sro.fSteerAllowed:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gEvent_status_vcmin_bus.fFourToTwentyOrl:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gEvent_status_vcmin_bus.fFourToTwentyOrh:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gEvent_status_vcmin_bus.fPot1Orh:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gEvent_status_vcmin_bus.fPot1Orl:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gEvent_status_vcmin_bus.fPot3InputOrl:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gEvent_status_vcmin_bus.fPot3InputOrh:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gEvent_status_vcmin_bus.fPot4InputOrl:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gEvent_status_vcmin_bus.fPot4InputOrh:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gEvent_status_vcmin_bus.fANA5VoltsOrl:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gEvent_status_vcmin_bus.fANA5VoltsOrh:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gEvent_status_vcmin_bus.fANA11VoltsOrl:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gEvent_status_vcmin_bus.fANA11VoltsOrh:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gEvent_status_vcmin_bus.fAna15VoltsOrl:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gEvent_status_vcmin_bus.fAna15VoltsOrh:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gEvent_status_vcmin_bus.fBatterySenseVoltsOrh:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gEvent_status_vcmin_bus.fBatterySenseVoltsOrl:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gEvent_status_vcmin_bus.fKysVoltsBnorEL:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gEvent_status_vcmin_bus.fKysVoltsAnorEL:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gEvent_status_vcmin_bus.fCssWith:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gEvent_status_vcmin_bus.fCsCrosscheck:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gEvent_status_vcmin_bus.fANA10VoltsOrh:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gEvent_status_vcmin_bus.fANA10VoltsOrl:0
TEST.EXPECTED:SteeringSIMbuild.<<GLOBAL>>.gEvent_status_vcmin_bus.fBrkFluidSw:0
TEST.END

-- Subprogram: SteeringSIMbuild_terminate

-- Test Case: SteeringSIMbuild_terminate.001
TEST.UNIT:SteeringSIMbuild
TEST.SUBPROGRAM:SteeringSIMbuild_terminate
TEST.NEW
TEST.NAME:SteeringSIMbuild_terminate.001
TEST.COMPOUND_ONLY
TEST.END

-- Unit: StrMstrApp

-- Subprogram: StrMstrApp_initialize

-- Test Case: StrMstrApp_initialize.001
TEST.UNIT:StrMstrApp
TEST.SUBPROGRAM:StrMstrApp_initialize
TEST.NEW
TEST.NAME:StrMstrApp_initialize.001
TEST.COMPOUND_ONLY
TEST.END

-- Unit: StrPosWhAngLimiter

-- Subprogram: StrPosWhAngLimiter_Init

-- Test Case: StrPosWhAngLimiter_Init.001
TEST.UNIT:StrPosWhAngLimiter
TEST.SUBPROGRAM:StrPosWhAngLimiter_Init
TEST.NEW
TEST.NAME:StrPosWhAngLimiter_Init.001
TEST.COMPOUND_ONLY
TEST.VALUE:StrPosWhAngLimiter.StrPosWhAngLimiter_Init.rty_CmdWhAngLmt[0]:0
TEST.EXPECTED:StrPosWhAngLimiter.StrPosWhAngLimiter_Init.rty_CmdWhAngLmt[0]:0
TEST.END

-- Unit: StrPos_SahNmbns

-- Subprogram: StrPos_SahNmbns

-- Test Case: StrPos_SahNmbns.001
TEST.UNIT:StrPos_SahNmbns
TEST.SUBPROGRAM:StrPos_SahNmbns
TEST.NEW
TEST.NAME:StrPos_SahNmbns.001
TEST.COMPOUND_ONLY
TEST.VALUE:<<OPTIONS>>.EVENT_LIMIT:10
TEST.STUB:StrPos_SahNmbns.StrPos_SahNmbns_HndlInNmbnsRng
TEST.STUB:StrPos_SahNmbns.StrPos_SahNmbns_initialize
TEST.STUB:StrPos_SetPntRateLmtr.StrPos_SetPntRateLmtr
TEST.STUB:StrPos_SetPntRateLmtr.StrPos_SetPntRateLmt_initialize
TEST.VALUE:StrPos_SahNmbns.StrPos_SahNmbns_HndlInNmbnsRng.rtu_TrxSpdBpNumb:0.0
TEST.VALUE:StrPos_SahNmbns.StrPos_SahNmbns_HndlInNmbnsRng.rtu_TrxSpdInNumb:0.0
TEST.VALUE:StrPos_SahNmbns.StrPos_SahNmbns_HndlInNmbnsRng.rtu_TrxSpdMaxNumb:0.0
TEST.VALUE:StrPos_SahNmbns.StrPos_SahNmbns_HndlInNmbnsRng.rtu_HndlPosInNumbRng:0.0
TEST.VALUE:StrPos_SahNmbns.StrPos_SahNmbns.rtu_TrxSpd:0.0
TEST.VALUE:StrPos_SahNmbns.StrPos_SahNmbns.rtu_HndlPosIn:0
TEST.EXPECTED:StrPos_SahNmbns.StrPos_SahNmbns.rtu_TrxSpd:0.0
TEST.EXPECTED:StrPos_SahNmbns.StrPos_SahNmbns.rtu_HndlPosIn:0
TEST.END

-- Subprogram: StrPos_SahNmbns_HndlInNmbnsRng

-- Test Case: StrPos_SahNmbns_HndlInNmbnsRng.001
TEST.UNIT:StrPos_SahNmbns
TEST.SUBPROGRAM:StrPos_SahNmbns_HndlInNmbnsRng
TEST.NEW
TEST.NAME:StrPos_SahNmbns_HndlInNmbnsRng.001
TEST.COMPOUND_ONLY
TEST.STUB:StrPos_SahNmbns.StrPos_SahNmbns_HndlInTransRng
TEST.STUB:StrPos_SahNmbns.StrPos_SahNmbns_Init
TEST.STUB:StrPos_SahNmbns.StrPos_SahNmbns
TEST.STUB:StrPos_SahNmbns.StrPos_SahNmbns_initialize
TEST.VALUE:StrPos_SahNmbns.StrPos_SahNmbns_HndlInNmbnsRng.rty_HndlPosOutNumbRng:<<malloc 1>>
TEST.EXPECTED:StrPos_SahNmbns.<<GLOBAL>>.SahNmbns_rTil2DuRatio:0.5
TEST.EXPECTED:StrPos_SahNmbns.StrPos_SahNmbns_HndlInNmbnsRng.rtu_TrxSpdBpNumb:0.0
TEST.EXPECTED:StrPos_SahNmbns.StrPos_SahNmbns_HndlInNmbnsRng.rtu_TrxSpdInNumb:0.0
TEST.EXPECTED:StrPos_SahNmbns.StrPos_SahNmbns_HndlInNmbnsRng.rtu_TrxSpdMaxNumb:0.0
TEST.EXPECTED:StrPos_SahNmbns.StrPos_SahNmbns_HndlInNmbnsRng.rtu_HndlPosInNumbRng:0.0
TEST.EXPECTED:StrPos_SahNmbns.StrPos_SahNmbns_HndlInNmbnsRng.rty_HndlPosOutNumbRng[0]:0.0
TEST.END

-- Subprogram: StrPos_SahNmbns_HndlInTransRng

-- Test Case: StrPos_SahNmbns_HndlInTransRng.001
TEST.UNIT:StrPos_SahNmbns
TEST.SUBPROGRAM:StrPos_SahNmbns_HndlInTransRng
TEST.NEW
TEST.NAME:StrPos_SahNmbns_HndlInTransRng.001
TEST.COMPOUND_ONLY
TEST.VALUE:<<OPTIONS>>.EVENT_LIMIT:50
TEST.VALUE:StrPos_SahNmbns.<<GLOBAL>>.gpSteer_params:<<malloc 1>>
TEST.VALUE:StrPos_SahNmbns.<<GLOBAL>>.gpSteer_params[0].ulEndStopZoneCCW:1
TEST.VALUE:StrPos_SahNmbns.<<GLOBAL>>.gpSteer_params[0].ulSpdModeCntPerTickMax:1
TEST.VALUE:StrPos_SahNmbns.StrPos_SahNmbns_HndlInTransRng.rty_HndlPosOutTransRng:<<malloc 1>>
TEST.EXPECTED:StrPos_SahNmbns.<<GLOBAL>>.gpSteer_params[0].eSensorType:ENCODER
TEST.EXPECTED:StrPos_SahNmbns.<<GLOBAL>>.gpSteer_params[0].eUseWalkieMode:DO_NOT_USE
TEST.EXPECTED:StrPos_SahNmbns.<<GLOBAL>>.gpSteer_params[0].ulEndStopZoneCCW:1
TEST.EXPECTED:StrPos_SahNmbns.<<GLOBAL>>.gpSteer_params[0].ulSpdModeCntPerTickMax:1
TEST.EXPECTED:StrPos_SahNmbns.<<GLOBAL>>.SahNmbns_rTrxSpdBp:0.0
TEST.EXPECTED:StrPos_SahNmbns.<<GLOBAL>>.SahNmbns_rHndlPosTransRng:0.0
TEST.EXPECTED:StrPos_SahNmbns.<<GLOBAL>>.SahNmbns_eState:SahNmbns_INIT
TEST.EXPECTED:StrPos_SahNmbns.<<GLOBAL>>.StrPos_SahNmbns_DW.is_active_c3_StrPos_SahNmbns:0
TEST.EXPECTED:StrPos_SahNmbns.<<GLOBAL>>.StrPos_SahNmbns_DW.is_c3_StrPos_SahNmbns:0
TEST.EXPECTED:StrPos_SahNmbns.StrPos_SahNmbns_HndlInTransRng.rtu_HndlPosInTransRng:0.0
TEST.EXPECTED:StrPos_SahNmbns.StrPos_SahNmbns_HndlInTransRng.rty_HndlPosOutTransRng[0]:0.0
TEST.END

-- Unit: StrPos_SetPntRateLmtr

-- Subprogram: StrPos_SetPntRateLmtr

-- Test Case: StrPos_SetPntRateLmtr.001
TEST.UNIT:StrPos_SetPntRateLmtr
TEST.SUBPROGRAM:StrPos_SetPntRateLmtr
TEST.NEW
TEST.NAME:StrPos_SetPntRateLmtr.001
TEST.COMPOUND_ONLY
TEST.END

-- Unit: TfdOpMode

-- Subprogram: TfdOpMode_TfdCurrentSpProfile

-- Test Case: TfdOpMode_TfdCurrentSpProfile.001
TEST.UNIT:TfdOpMode
TEST.SUBPROGRAM:TfdOpMode_TfdCurrentSpProfile
TEST.NEW
TEST.NAME:TfdOpMode_TfdCurrentSpProfile.001
TEST.COMPOUND_ONLY
TEST.STUB:TfdCurrentSpProfile.TfdCurrentSpProfile
TEST.STUB:TfdCurrentSpProfile.TfdCurrentSpProfile_initialize
TEST.VALUE:TfdCurrentSpProfile.TfdCurrentSpProfile.rtu_Vehicle_Speed:0.0
TEST.VALUE:TfdCurrentSpProfile.TfdCurrentSpProfile.rtu_StrPos:3.0
TEST.VALUE:TfdCurrentSpProfile.TfdCurrentSpProfile.rtu_StrSp:4
TEST.VALUE:TfdCurrentSpProfile.TfdCurrentSpProfile.rtu_SteerRotation:5
TEST.VALUE:TfdCurrentSpProfile.TfdCurrentSpProfile.rty_rTfdCurrentSp[0]:4.0
TEST.END

-- Unit: TfdOvrMode

-- Subprogram: TfdOvrMode_initialize

-- Test Case: TfdOvrMode_initialize.001
TEST.UNIT:TfdOvrMode
TEST.SUBPROGRAM:TfdOvrMode_initialize
TEST.NEW
TEST.NAME:TfdOvrMode_initialize.001
TEST.COMPOUND_ONLY
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.VALUE:TfdOvrMode.<<GLOBAL>>.gpSteer_params:<<malloc 1>>
TEST.VALUE:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].eUseDirFeature:USE_MODE
TEST.VALUE:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].eUseWalkieMode:USE_MODE
TEST.VALUE:TfdOvrMode.<<GLOBAL>>.gpFeatures_bus:<<malloc 1>>
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].eSensorType:ENCODER
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].eControlMode:ASSIST
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].ulFbEcr1Cnts:0
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].ulFbEcr2Cnts:0
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].eUseDirFeature:USE_MODE
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].eUseWalkieMode:USE_MODE
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].eUseRabbitTurtleMode:DO_NOT_USE
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].ulEndStopZoneCW:0
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].ulEndStopZoneCCW:0
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].slAttTurtleWhAngLmt[0]:0
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].slQPWhAngLmt[0]:0
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].rTil2WhRatio:0.0
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].ulSpdModeCntPerTickMax:0
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].rSpdModeKp:0.0
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].rSpdModeKi:0.0
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].rSpdModeKd:0.0
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].rStrTs:0.0
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].rSpdModeDerFltrCoef:0.0
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].rStrGearRatio:0.0
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].rStrSpdDeadBand:0.0
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].ulStrSpdUpperBoundary:0
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].ulStrSpdLowerBoundary:0
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].rStrSpdROTO:0.0
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].rStrSpdDiffCntsFltrGain:0.0
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].rTfdCurrent_VS_TractionSpeed_Speed[0]:0.0
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].rTfdCurrent_VS_TractionSpeed_Current[0]:0.0
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].rTfdCurrent_VS_SteeringError_Error[0]:0.0
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].rTfdMaxCurrent:0.0
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].rTfdReduction:0.0
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].rTfdStiffCurrent:0.0
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].rTfdRampDnHoldCurrent:0.0
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].rTfdCurrentRampRateLimiter:0.0
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].rTfdRampDnHoldTime:0.0
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].rTfdHoldUpSpeedMax:0.0
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].rStrMtrZeroSpd:0.0
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].slStrMtrSpdLmt:0
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].rStrMtrSpdRedTrxSpdBP:0.0
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].rStrMtrSpdMaxTrxSpdRf:0.0
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].rStrMtrSpdRedWhAngBP:0.0
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].rStrMtrSpdRedMaxWhAng:0.0
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].rStrMtrSpdMaxWhAngRf:0.0
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].ulTrigFunct:0
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].ulStrMtrSpdWhAngRfType:0
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].rTilScalingMaxTrxSpdSf:0.0
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].rTillerMotionThreshold:0.0
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].rCmd1_RO_Upr:0.0
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].rCmd1_RO_Lwr:0.0
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].rCmd1_Rng_Upr:0.0
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].rCmd1_Rng_Lwr:0.0
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].rAssistGain:0.0
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].rStrCmdDeadBand:0.0
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].rTrxSpdWhAng_SpdOs:0.0
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].eUse360Feature:DO_NOT_USE
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].rStrMtrSpdRedWhAngErrBP:0.0
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].rStrMtrSpdRedMaxWhAngErr:0.0
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].rStrMtrSpdMaxWhAngErrRf:0.0
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].rHndlSpdFltrCoef:0.0
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].rHndlSpdThrshld:0.0
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].rWhAngTrgtFltGain:0.0
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].rSahNmbnsTlr2DuSf:0.0
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].rSpFltrExpSf:0.0
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].rSpFltrExpRef:0.0
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].rSahNmbnsMaxInitAng:0.0
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].rSahNmbnsTlr2DuNom:0.0
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].rSahNmbnsMaxNumbedAng:0.0
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].rSahNmbnsTransAng:0.0
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].rStrNoEdRequired:0.0
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].rSteerAngleBeforeED:0.0
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].eSteer_Direction:Forward
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].slSensorSlope:0
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].rStrSpdBumpThrshld:0.0
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].rBumpCancelThrshld:0.0
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].rDeadBandGain:0.0
TEST.EXPECTED:TfdOvrMode.<<GLOBAL>>.gpSteer_params[0].rDeadBandReEntDly:0.0
TEST.END

-- Unit: TrxSpd_StrMtrSpdLmt

-- Subprogram: TrxSpd_StrMtrSpdLmt

-- Test Case: TrxSpd_StrMtrSpdLmt.001
TEST.UNIT:TrxSpd_StrMtrSpdLmt
TEST.SUBPROGRAM:TrxSpd_StrMtrSpdLmt
TEST.NEW
TEST.NAME:TrxSpd_StrMtrSpdLmt.001
TEST.COMPOUND_ONLY
TEST.END

-- Subprogram: TrxSpd_StrMtrSpdLmt_Init

-- Test Case: TrxSpd_StrMtrSpdLmt_Init.001
TEST.UNIT:TrxSpd_StrMtrSpdLmt
TEST.SUBPROGRAM:TrxSpd_StrMtrSpdLmt_Init
TEST.NEW
TEST.NAME:TrxSpd_StrMtrSpdLmt_Init.001
TEST.COMPOUND_ONLY
TEST.END

-- Subprogram: TrxSpd_StrMtrSpdLmt_initialize

-- Test Case: TrxSpd_StrMtrSpdLmt_initialize.001
TEST.UNIT:TrxSpd_StrMtrSpdLmt
TEST.SUBPROGRAM:TrxSpd_StrMtrSpdLmt_initialize
TEST.NEW
TEST.NAME:TrxSpd_StrMtrSpdLmt_initialize.001
TEST.COMPOUND_ONLY
TEST.VALUE:TrxSpd_StrMtrSpdLmt.<<GLOBAL>>.StrTrxSpdLmtComponent_rStrMtrSpdTrxSpdRf:0.0
TEST.VALUE:TrxSpd_StrMtrSpdLmt.<<GLOBAL>>.gpSteer_params:<<malloc 1>>
TEST.VALUE:TrxSpd_StrMtrSpdLmt.<<GLOBAL>>.StrTrxSpdLmtComponent_rStrMtrSpdRedTrxSpdDelta:0.0
TEST.VALUE:TrxSpd_StrMtrSpdLmt.<<GLOBAL>>.StrTrxSpdLmtComponent_rStrMtrSpdTrxSpdRfUnclamped:0.0
TEST.VALUE:TrxSpd_StrMtrSpdLmt.<<GLOBAL>>.StrTrxSpdLmtComponent_fStrMtrSpdRedTrxSpdGTBP:0
TEST.END

-- Unit: WhAngErr_StrMtrSpdLmt

-- Subprogram: WhAngErr_StrMtrSpdLm_initialize

-- Test Case: WhAngErr_StrMtrSpdLm_initialize.001
TEST.UNIT:WhAngErr_StrMtrSpdLmt
TEST.SUBPROGRAM:WhAngErr_StrMtrSpdLm_initialize
TEST.NEW
TEST.NAME:WhAngErr_StrMtrSpdLm_initialize.001
TEST.COMPOUND_ONLY
TEST.END

-- Subprogram: WhAngErr_StrMtrSpdLmt

-- Test Case: WhAngErr_StrMtrSpdLmt.001
TEST.UNIT:WhAngErr_StrMtrSpdLmt
TEST.SUBPROGRAM:WhAngErr_StrMtrSpdLmt
TEST.NEW
TEST.NAME:WhAngErr_StrMtrSpdLmt.001
TEST.COMPOUND_ONLY
TEST.STUB:WhAng_StrMtrSpdLmt.WhAng_StrMtrSpdLmt_Init
TEST.STUB:WhAng_StrMtrSpdLmt.WhAng_StrMtrSpdLmt
TEST.STUB:WhAng_StrMtrSpdLmt.WhAng_StrMtrSpdLmt_initialize
TEST.VALUE:WhAng_StrMtrSpdLmt.<<GLOBAL>>.gpSteer_params:<<malloc 1>>
TEST.VALUE:WhAng_StrMtrSpdLmt.WhAng_StrMtrSpdLmt.rtu_WhAngIn:0.0
TEST.EXPECTED:WhAng_StrMtrSpdLmt.<<GLOBAL>>.StrWhAngLmtComponent_rStrMtrSpdWhAngRf:0.0
TEST.EXPECTED:WhAng_StrMtrSpdLmt.<<GLOBAL>>.gpSteer_params[0].eSensorType:ENCODER
TEST.EXPECTED:WhAng_StrMtrSpdLmt.<<GLOBAL>>.gpSteer_params[0].eControlMode:ASSIST
TEST.EXPECTED:WhAng_StrMtrSpdLmt.<<GLOBAL>>.gpSteer_params[0].ulFbEcr1Cnts:0
TEST.EXPECTED:WhAng_StrMtrSpdLmt.<<GLOBAL>>.gpSteer_params[0].ulFbEcr2Cnts:0
TEST.EXPECTED:WhAng_StrMtrSpdLmt.<<GLOBAL>>.gpSteer_params[0].eUseDirFeature:DO_NOT_USE
TEST.EXPECTED:WhAng_StrMtrSpdLmt.<<GLOBAL>>.gpSteer_params[0].eUseWalkieMode:DO_NOT_USE
TEST.EXPECTED:WhAng_StrMtrSpdLmt.<<GLOBAL>>.gpSteer_params[0].eUseRabbitTurtleMode:USE_MODE
TEST.EXPECTED:WhAng_StrMtrSpdLmt.<<GLOBAL>>.gpSteer_params[0].ulEndStopZoneCW:0
TEST.EXPECTED:WhAng_StrMtrSpdLmt.<<GLOBAL>>.gpSteer_params[0].ulEndStopZoneCCW:0
TEST.EXPECTED:WhAng_StrMtrSpdLmt.<<GLOBAL>>.gpSteer_params[0].slAttTurtleWhAngLmt[0]:0
TEST.EXPECTED:WhAng_StrMtrSpdLmt.<<GLOBAL>>.gpSteer_params[0].slQPWhAngLmt[0]:0
TEST.EXPECTED:WhAng_StrMtrSpdLmt.<<GLOBAL>>.gpSteer_params[0].rTil2WhRatio:0.0
TEST.EXPECTED:WhAng_StrMtrSpdLmt.<<GLOBAL>>.gpSteer_params[0].ulSpdModeCntPerTickMax:0
TEST.EXPECTED:WhAng_StrMtrSpdLmt.<<GLOBAL>>.gpSteer_params[0].rSpdModeKp:0.0
TEST.EXPECTED:WhAng_StrMtrSpdLmt.<<GLOBAL>>.gpSteer_params[0].rSpdModeKi:0.0
TEST.EXPECTED:WhAng_StrMtrSpdLmt.<<GLOBAL>>.gpSteer_params[0].rSpdModeKd:0.0
TEST.EXPECTED:WhAng_StrMtrSpdLmt.<<GLOBAL>>.gpSteer_params[0].rStrTs:0.0
TEST.EXPECTED:WhAng_StrMtrSpdLmt.<<GLOBAL>>.gpSteer_params[0].rSpdModeDerFltrCoef:0.0
TEST.EXPECTED:WhAng_StrMtrSpdLmt.<<GLOBAL>>.gpSteer_params[0].rStrGearRatio:0.0
TEST.EXPECTED:WhAng_StrMtrSpdLmt.<<GLOBAL>>.gpSteer_params[0].rStrSpdDeadBand:0.0
TEST.EXPECTED:WhAng_StrMtrSpdLmt.<<GLOBAL>>.gpSteer_params[0].ulStrSpdUpperBoundary:0
TEST.EXPECTED:WhAng_StrMtrSpdLmt.<<GLOBAL>>.gpSteer_params[0].ulStrSpdLowerBoundary:0
TEST.EXPECTED:WhAng_StrMtrSpdLmt.<<GLOBAL>>.gpSteer_params[0].rStrSpdROTO:0.0
TEST.EXPECTED:WhAng_StrMtrSpdLmt.<<GLOBAL>>.gpSteer_params[0].rStrSpdDiffCntsFltrGain:0.0
TEST.EXPECTED:WhAng_StrMtrSpdLmt.<<GLOBAL>>.gpSteer_params[0].rTfdCurrent_VS_TractionSpeed_Speed[0]:0.0
TEST.EXPECTED:WhAng_StrMtrSpdLmt.<<GLOBAL>>.gpSteer_params[0].rTfdCurrent_VS_TractionSpeed_Current[0]:0.0
TEST.EXPECTED:WhAng_StrMtrSpdLmt.<<GLOBAL>>.gpSteer_params[0].rTfdCurrent_VS_SteeringError_Error[0]:0.0
TEST.EXPECTED:WhAng_StrMtrSpdLmt.<<GLOBAL>>.gpSteer_params[0].rTfdCurrent_VS_SteeringError_Current[0]:0.0
TEST.EXPECTED:WhAng_StrMtrSpdLmt.<<GLOBAL>>.gpSteer_params[0].rTfdMaxCurrent:0.0
TEST.EXPECTED:WhAng_StrMtrSpdLmt.<<GLOBAL>>.gpSteer_params[0].rTfdReduction:0.0
TEST.EXPECTED:WhAng_StrMtrSpdLmt.<<GLOBAL>>.gpSteer_params[0].rTfdStiffCurrent:0.0
TEST.EXPECTED:WhAng_StrMtrSpdLmt.<<GLOBAL>>.gpSteer_params[0].rTfdRampDnHoldCurrent:0.0
TEST.EXPECTED:WhAng_StrMtrSpdLmt.<<GLOBAL>>.gpSteer_params[0].rTfdCurrentRampRateLimiter:0.0
TEST.EXPECTED:WhAng_StrMtrSpdLmt.<<GLOBAL>>.gpSteer_params[0].rTfdRampDnHoldTime:0.0
TEST.EXPECTED:WhAng_StrMtrSpdLmt.<<GLOBAL>>.gpSteer_params[0].rTfdHoldUpSpeedMax:0.0
TEST.EXPECTED:WhAng_StrMtrSpdLmt.<<GLOBAL>>.gpSteer_params[0].rStrMtrZeroSpd:0.0
TEST.EXPECTED:WhAng_StrMtrSpdLmt.<<GLOBAL>>.gpSteer_params[0].rStrMtrSpdRedTrxSpdBP:0.0
TEST.EXPECTED:WhAng_StrMtrSpdLmt.<<GLOBAL>>.gpSteer_params[0].rTilScalingMaxTrxSpdSf:0.0
TEST.EXPECTED:WhAng_StrMtrSpdLmt.<<GLOBAL>>.gpSteer_params[0].rTillerMotionThreshold:0.0
TEST.EXPECTED:WhAng_StrMtrSpdLmt.<<GLOBAL>>.gpSteer_params[0].rCmd1_RO_Upr:0.0
TEST.EXPECTED:WhAng_StrMtrSpdLmt.<<GLOBAL>>.gpSteer_params[0].rCmd1_RO_Lwr:0.0
TEST.EXPECTED:WhAng_StrMtrSpdLmt.<<GLOBAL>>.gpSteer_params[0].rCmd1_Rng_Upr:0.0
TEST.EXPECTED:WhAng_StrMtrSpdLmt.<<GLOBAL>>.gpSteer_params[0].rCmd1_Rng_Lwr:0.0
TEST.EXPECTED:WhAng_StrMtrSpdLmt.<<GLOBAL>>.gpSteer_params[0].rStrCmdDeadBand:0.0
TEST.EXPECTED:WhAng_StrMtrSpdLmt.<<GLOBAL>>.gpSteer_params[0].rSahNmbnsMaxNumbedAng:0.0
TEST.EXPECTED:WhAng_StrMtrSpdLmt.<<GLOBAL>>.gpSteer_params[0].rSahNmbnsTransAng:0.0
TEST.EXPECTED:WhAng_StrMtrSpdLmt.<<GLOBAL>>.gpSteer_params[0].rStrNoEdRequired:0.0
TEST.EXPECTED:WhAng_StrMtrSpdLmt.<<GLOBAL>>.gpSteer_params[0].rSteerAngleBeforeED:0.0
TEST.EXPECTED:WhAng_StrMtrSpdLmt.<<GLOBAL>>.gpSteer_params[0].eSteer_Direction:Forward
TEST.EXPECTED:WhAng_StrMtrSpdLmt.<<GLOBAL>>.gpSteer_params[0].slSensorSlope:0
TEST.EXPECTED:WhAng_StrMtrSpdLmt.<<GLOBAL>>.gpSteer_params[0].slSensorSignalRelation:0
TEST.EXPECTED:WhAng_StrMtrSpdLmt.<<GLOBAL>>.gpSteer_params[0].rDeadBandHyst:0.0
TEST.EXPECTED:WhAng_StrMtrSpdLmt.<<GLOBAL>>.gpSteer_params[0].rDeadBandReEntDly:0.0
TEST.EXPECTED:WhAng_StrMtrSpdLmt.<<GLOBAL>>.gpSteer_params[0].rStrSpdDbBumpThrshldGain:0.0
TEST.EXPECTED:WhAng_StrMtrSpdLmt.<<GLOBAL>>.StrWhAngLmtComponent_rStrMtrSpdWhAngLutRf:0.0
TEST.EXPECTED:WhAng_StrMtrSpdLmt.<<GLOBAL>>.StrWhAngLmtComponent_rStrMtrSpdRedWhAngDelta:0.0
TEST.EXPECTED:WhAng_StrMtrSpdLmt.<<GLOBAL>>.StrWhAngLmtComponent_rStrMtrSpdWhAngRfUnclamped:0.0
TEST.EXPECTED:WhAng_StrMtrSpdLmt.<<GLOBAL>>.StrWhAngLmtComponent_rStrMtrSpdRedWhAngClamped:0.0
TEST.EXPECTED:WhAng_StrMtrSpdLmt.<<GLOBAL>>.StrWhAngLmtComponent_rStrMtrSpdWhAngTrigRf:0.0
TEST.EXPECTED:WhAng_StrMtrSpdLmt.<<GLOBAL>>.StrWhAngLmtComponent_fStrMtrSpdRedWhAngGTBP:0
TEST.EXPECTED:WhAng_StrMtrSpdLmt.WhAng_StrMtrSpdLmt.rtu_WhAngIn:0.0
TEST.END

-- Subprogram: WhAngErr_StrMtrSpdLmt_Init

-- Test Case: WhAngErr_StrMtrSpdLmt_Init.001
TEST.UNIT:WhAngErr_StrMtrSpdLmt
TEST.SUBPROGRAM:WhAngErr_StrMtrSpdLmt_Init
TEST.NEW
TEST.NAME:WhAngErr_StrMtrSpdLmt_Init.001
TEST.COMPOUND_ONLY
TEST.END

-- Unit: WhAng_StrMtrSpdLmt

-- Subprogram: WhAng_StrMtrSpdLmt

-- Test Case: WhAng_StrMtrSpdLmt.001
TEST.UNIT:WhAng_StrMtrSpdLmt
TEST.SUBPROGRAM:WhAng_StrMtrSpdLmt
TEST.NEW
TEST.NAME:WhAng_StrMtrSpdLmt.001
TEST.COMPOUND_ONLY
TEST.END

-- Subprogram: WhAng_StrMtrSpdLmt_Init

-- Test Case: WhAng_StrMtrSpdLmt_Init.001
TEST.UNIT:WhAng_StrMtrSpdLmt
TEST.SUBPROGRAM:WhAng_StrMtrSpdLmt_Init
TEST.NEW
TEST.NAME:WhAng_StrMtrSpdLmt_Init.001
TEST.COMPOUND_ONLY
TEST.STUB:WhAngErr_StrMtrSpdLmt.WhAngErr_StrMtrSpdLmt_Init
TEST.STUB:WhAngErr_StrMtrSpdLmt.WhAngErr_StrMtrSpdLmt
TEST.STUB:WhAngErr_StrMtrSpdLmt.WhAngErr_StrMtrSpdLm_initialize
TEST.VALUE:WhAngErr_StrMtrSpdLmt.WhAngErr_StrMtrSpdLmt_Init.rty_WhAngErrRF_Out[0]:0.0
TEST.VALUE:WhAngErr_StrMtrSpdLmt.WhAngErr_StrMtrSpdLmt.rtu_WhAngIn:0.0
TEST.VALUE:WhAngErr_StrMtrSpdLmt.WhAngErr_StrMtrSpdLmt.rtu_WhAngSPIn:0
TEST.VALUE:WhAngErr_StrMtrSpdLmt.WhAngErr_StrMtrSpdLmt.rtu_FwdRevSteer:0
TEST.END

-- Subprogram: WhAng_StrMtrSpdLmt_initialize

-- Test Case: WhAng_StrMtrSpdLmt_initialize.001
TEST.UNIT:WhAng_StrMtrSpdLmt
TEST.SUBPROGRAM:WhAng_StrMtrSpdLmt_initialize
TEST.NEW
TEST.NAME:WhAng_StrMtrSpdLmt_initialize.001
TEST.COMPOUND_ONLY
TEST.END

-- COMPOUND TESTS

TEST.SUBPROGRAM:<<COMPOUND>>
TEST.NEW
TEST.NAME:<<HmsSim_Steering>>
TEST.SLOT: "1", "HmsSim_Steering", "HmsSim_Steering", "1", "HmsSim_Steering.001"
TEST.SLOT: "2", "HmsSim_Steering", "HmsSim_Steering_initialize", "1", "HmsSim_Steering_initialize.001"
TEST.SLOT: "3", "HmsSim_Steering", "HmsSim_Steering_Init", "1", "HmsSim_Steering_Init.001"
TEST.SLOT: "4", "HmsSim_Steering", "HmsSim_Steering_Start", "1", "HmsSim_Steering_Start.001"
TEST.SLOT: "5", "SteeringSIM", "SteeringSIM", "1", "SteeringSIM.001"
TEST.SLOT: "6", "HmsSim_SteeringBuild", "HmsSim_SteeringBuild_step", "1", "HmsSim_SteeringBuild_step.001"
TEST.SLOT: "7", "HmsSim_SteeringBuild", "HmsSim_SteeringBuild_terminate", "1", "HmsSim_SteeringBuild_terminate.001"
TEST.END
--

-- COMPOUND TESTS

TEST.SUBPROGRAM:<<COMPOUND>>
TEST.NEW
TEST.NAME:<<PosCmd>>
TEST.SLOT: "1", "PosCmd", "PosCmd", "1", "PosCmd.001"
TEST.SLOT: "2", "Cmd_Pos_Integrator", "Cmd_Pos_Integrator_CompStdDev", "1", "Cmd_Pos_Integrator_CompStdDev.001"
TEST.SLOT: "3", "PosCmd", "PosCmd_ForceMonoinc1", "1", "PosCmd_ForceMonoinc1.001"
TEST.SLOT: "4", "PosCmd", "PosCmd_FwdLoad", "1", "PosCmd_FwdLoad.001"
TEST.SLOT: "5", "PosCmd", "PosCmd_FwdLdd", "1", "PosCmd_FwdLdd.001"
TEST.SLOT: "6", "PosCmd", "PosCmd_FwdLdd_j", "1", "PosCmd_FwdLdd_j.001"
TEST.SLOT: "7", "PosCmd", "PosCmd_FwdLdd_m", "1", "PosCmd_FwdLdd_m.001"
TEST.SLOT: "8", "PosCmd", "PosCmd_Init", "1", "PosCmd_Init.001"
TEST.SLOT: "9", "PosCmd", "PosCmd_ForceMonoinc", "1", "PosCmd_ForceMonoinc.001"
TEST.SLOT: "10", "PosCmd", "PosCmd_FwdLdd_a", "1", "PosCmd_FwdLdd_a.001"
TEST.SLOT: "11", "PosCmd", "PosCmd_FwdLdd_n", "1", "PosCmd_FwdLdd_n.001"
TEST.SLOT: "12", "PosCmd", "PosCmd_FwdLoad_c", "1", "PosCmd_FwdLoad_c.001"
TEST.SLOT: "13", "PosCmd", "PosCmd_FwdUnLdd", "1", "PosCmd_FwdUnLdd.001"
TEST.SLOT: "14", "PosCmd", "PosCmd_FwdUnLdd1", "1", "PosCmd_FwdUnLdd1.001"
TEST.SLOT: "15", "PosCmd", "PosCmd_RevLdd", "1", "PosCmd_RevLdd.001"
TEST.SLOT: "16", "PosCmd", "PosCmd_initialize", "1", "PosCmd_initialize.001"
TEST.SLOT: "17", "PosCmd", "PosCmd_RevLoad_g", "1", "PosCmd_RevLoad_g.001"
TEST.SLOT: "18", "PosCmd", "PosCmd_RevUnldd", "1", "PosCmd_RevUnldd.001"
TEST.SLOT: "19", "PosCmd", "PosCmd_RevUnldd1", "1", "PosCmd_RevUnldd1.001"
TEST.END
--

-- COMPOUND TESTS

TEST.SUBPROGRAM:<<COMPOUND>>
TEST.NEW
TEST.NAME:<<Steering_Sim>>.001
TEST.SLOT: "1", "SteeringSIM", "SteeringSIM", "1", "SteeringSIM.001"
TEST.SLOT: "2", "SteeringSIM", "SteeringSIM_Init", "1", "SteeringSIM_Init.001"
TEST.SLOT: "3", "SteeringSIM", "SteeringSIM_initialize", "1", "SteeringSIM_initialize.001"
TEST.SLOT: "4", "HmsSim_SteeringBuild", "HmsSim_SteeringBuild_initialize", "1", "HmsSim_SteeringBuild_initialize.001"
TEST.SLOT: "5", "SteeringSIMbuild", "SteeringSIMbuild_initialize", "1", "SteeringSIMbuild_initialize.001"
TEST.SLOT: "6", "SteeringSIMbuild", "SteeringSIMbuild_step", "1", "SteeringSIMbuild_step.001"
TEST.END
--

-- COMPOUND TESTS

TEST.SUBPROGRAM:<<COMPOUND>>
TEST.NEW
TEST.NAME:<Spd_Cmd>>.001
TEST.SLOT: "1", "SpdCmd", "SpdCmd", "1", "SpdCmd.001"
TEST.SLOT: "2", "SpdCmd", "SpdCmd_initialize", "1", "SpdCmd_initialize.001"
TEST.SLOT: "3", "SpdCmd", "SpdCmd_Init", "1", "SpdCmd_Init.001"
TEST.SLOT: "4", "AssistCmd", "AssistCmd", "1", "AssistCmd.001"
TEST.SLOT: "5", "StrPosWhAngLimiter", "StrPosWhAngLimiter_Init", "1", "StrPosWhAngLimiter_Init.001"
TEST.SLOT: "6", "TfdOvrMode", "TfdOvrMode_initialize", "1", "TfdOvrMode_initialize.001"
TEST.SLOT: "7", "PosCmd", "PosCmd_RevLoad", "1", "PosCmd_RevLoad.001"
TEST.SLOT: "8", "SpdCmd", "SpdCmd_RevLoad", "1", "SpdCmd_RevLoad.001"
TEST.SLOT: "9", "SpdCmd", "SpdCmd_initialize", "1", "SpdCmd_initialize.001"
TEST.SLOT: "10", "SpdCmd", "SpdCmd_Input_1_OOR", "1", "SpdCmd_Input_1_OOR.001"
TEST.SLOT: "11", "StrPos_SahNmbns", "StrPos_SahNmbns_HndlInNmbnsRng", "1", "StrPos_SahNmbns_HndlInNmbnsRng.001"
TEST.SLOT: "12", "StrPos_SahNmbns", "StrPos_SahNmbns_HndlInTransRng", "1", "StrPos_SahNmbns_HndlInTransRng.001"
TEST.END
--
