# Name:       ParseHtmlForManage.py
#
# Purpose:   This python script will loop through files for .html format and look for RGB codes
#            to determine if a fail condition occured in Manage reports
#
# Return:    List of all reports and list of Failed Html reports
#
# Author:    Thomas Rode
#
# Options:   None
#
# Note:
#
# Python Version: 2.7.11
#          
# Reference: https://www.python.org/ (open source)
#

from bs4 import BeautifulSoup
from os import walk
import re, os


PASS = "#CCFFCC"
FAIL = "#FFCCCC"

def ParseHtml():
    
    allfiles = []
    justhtml = []
    justfails = []
    bothpassfails = []

    # get all files into list
    for (dirpath, dirnames, filenames) in walk(os.getcwd()):
        allfiles.extend(filenames)
 
    # Loop through files populate just html
    for files in allfiles:

        fileName,fileExtension = os.path.splitext(files)
        if fileExtension == '.html':
            justhtml.append(files)
    
    # loop through html report 
    for files in justhtml:

        try:
            # Open file for soup instance
            soup = BeautifulSoup(open(files, 'r'), "html.parser")#("UTILITIES_SIMULATION_INTEGRATION_management_report.html", 'r'), "html.parser")  
        
            # Get all data cell 'td' tags  
            a_list = soup.find_all('td') #td bgcolor

            # make buffer a sting 
            buff = str(a_list)

            if re.search(FAIL,buff):#"#FFCCCC", buff):
                print (files),;print(" Report Indicates -> Fail\n")
                justfails.append(files)
                bothpassfails.append(files)
            else:
                print (files),;print (" Report Indicates -> Pass\n")
                bothpassfails.append(files)
        except:
            print (" Complete ")
            break
        
    return (justfails, bothpassfails)    
       
     
if __name__ == '__main__':

    ParseHtml()
