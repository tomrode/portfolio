# Name:       RemoveFiles.py
#
# Purpose:   This python script will querry the regression time stamped folders and call removal.py which will delete files in the regression
#
# Author:    Thomas Rode
#
# Options:   None
#
# Note:
#
# Python Version: 2.7.11
#          
# Reference: https://www.python.org/ (open source)
#


import glob
import os
import shutil
import subprocess



def RemoveRegressionFiles():
    RootDir = 'C:/vcast-buildsystem-orig'
    #RootDir = 'C:/Users/t0028919/Desktop/Remove files'
    Folders = []
    i = 0

    #Get a list of the directoires in this folder 
    directories_in_curdir = filter(os.path.isdir, os.listdir(os.curdir))
    Folders = directories_in_curdir
   
    # Call the remove.py in each folder if it there
    for item in Folders:
       os.chdir( RootDir  + "//" + item)

       if "Regression-Original-Scripts" not in item:    
           subprocess.call("removal.py", shell=True) 
    
# Main program - Supports execution from the command line
if __name__ == "__main__":
    
    RemoveRegressionFiles()
   
   
