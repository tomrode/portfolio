
CALL :MainScript
GOTO :EOF

:MainScript
	REM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	REM Call Analytics server forthe Project MASTER on port :8128
	REM Call Analytics server forthe Project SUPV on port :8129
	REM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        CALL :RunProgramAsync "C:\VCAST\VCDASH -p C1515_VCM_MASTER --history-dir=C:\vcast-buildsystem-orig\Regression-Original-Scripts\VCASTAnalyticsHistory\C1515_VCM_MASTER -P 8128"
        CALL :RunProgramAsync "C:\VCAST\VCDASH -p C1515_VCM_SUPV --history-dir=C:\vcast-buildsystem-orig\Regression-Original-Scripts\VCASTAnalyticsHistory\C1515_VCM_SUPV -P 8129"

GOTO :EOF

:RunProgramAsync
  REM ~sI expands the variable to contain short DOS names only
  start %~s1
GOTO :EOF