set PATH=C:\ghs\comp_201654;C:\ghs\multi_714;%PATH%; 

set VECTORCAST_DIR=C:\VCAST\2018sp2
set CROWN_SOURCE_BASE=C:\rtc_workspaces\TmlVcmMasterBuildDef001_Sandbox
set VCAST_SUPPORT_FILES=%cd%\RSPs

SET VCAST_DISABLE_TEST_MAINTENANCE=1

set VCAST_CCS_INSTALL_DIR=C:\ti\ccsv7

REM Path to tools for debugging
set VCAST_CCS_COMPILER_INSTALL_DIR=C:\ti\c2000_6.2.11

set BIOS_INSTALL_DIR=C:\ti\bios_6_35_04_50
set XDC_INSTALL_DIR=C:\ti\xdctools_3_24_06_63

REM Path to compiler
set PATH=%VCAST_CCS_COMPILER_INSTALL_DIR%\bin;%BIOS_INSTALL_DIR%\xdctools;%PATH%;

set C2000INC=%VCAST_CCS_INSTALL_DIR%\ccs_base\c2000\include
set C2000LIB=%VCAST_CCS_INSTALL_DIR%\ccs_base\c2000\nowFlash\libraries


REM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
REM Building and Executing the Project MASTER
REM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%VECTORCAST_DIR%\manage --project=C1515_VCM_MASTER --workspace=.\C1515_VCM_MASTER\build --build-execute

REM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
REM Generating Full Reports for MASTER
REM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%VECTORCAST_DIR%\manage --project=C1515_VCM_MASTER --full-status=full_status_MASTER_Quality.html

REM %%%%%%%%%%%%%%%%%%%%%%%%%%
REM Generating .vcr for MASTER
REM %%%%%%%%%%%%%%%%%%%%%%%%%%
%VECTORCAST_DIR%\manage --project=C1515_VCM_MASTER --export-result=Regression_Unit_Test_Results_MASTER_Quality.vcr

REM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
REM Create a Directory for Tracking Trends and MASTER Project History  
REM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
REM %VECTORCAST_DIR%/vcdash -p C1515_VCM_MASTER --history-dir=C:\vcast-buildsystem-orig\Regression-Original-Scripts\VCASTAnalyticsHistory\C1515_VCM_MASTER --save-history


REM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
REM Building and Executing the Project SUPV
REM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%VECTORCAST_DIR%\manage --project=C1515_VCM_SUPV --workspace=.\C1515_VCM_SUPV\build --build-execute

REM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
REM Generating Full Reports for SUPV
REM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%VECTORCAST_DIR%\manage --project=C1515_VCM_SUPV --full-status=full_status_SUPV_Quality.html

REM %%%%%%%%%%%%%%%%%%%%%%%
REM Generating .vcr for SUP
REM %%%%%%%%%%%%%%%%%%%%%%%
%VECTORCAST_DIR%\manage --project=C1515_VCM_SUPV --export-result=Regression_Unit_Test_Results_SUPV_Quality.vcr

REM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
REM Create a Directory for Tracking Trends and SUPV Project History  
REM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
REM %VECTORCAST_DIR%/vcdash -p C1515_VCM_SUPV --history-dir=C:\vcast-buildsystem-orig\Regression-Original-Scripts\VCASTAnalyticsHistory\C1515_VCM_SUPV --save-history



