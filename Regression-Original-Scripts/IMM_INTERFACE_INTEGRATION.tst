-- VectorCAST 6.4p (01/31/17)
-- Test Case Script
-- 
-- Environment    : IMM_INTERFACE_INTEGRATION
-- Unit(s) Under Test: imm_interface
-- 
-- Script Features
TEST.SCRIPT_FEATURE:C_DIRECT_ARRAY_INDEXING
TEST.SCRIPT_FEATURE:CPP_CLASS_OBJECT_REVISION
TEST.SCRIPT_FEATURE:MULTIPLE_UUT_SUPPORT
TEST.SCRIPT_FEATURE:MIXED_CASE_NAMES
TEST.SCRIPT_FEATURE:STATIC_HEADER_FUNCS_IN_UUTS
--

-- Subprogram: <<INIT>>

-- Test Case: <<INIT>>.001
TEST.SUBPROGRAM:<<INIT>>
TEST.NEW
TEST.NAME:<<INIT>>.001
TEST.END

-- Subprogram: gfIMM_ConfigurationComplete

-- Test Case: BASIS-PATH-001-PARTIAL
TEST.UNIT:imm_interface
TEST.SUBPROGRAM:gfIMM_ConfigurationComplete
TEST.NEW
TEST.NAME:BASIS-PATH-001-PARTIAL
TEST.BASIS_PATH:1 of 3 (partial)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (1) if ((gIMMControl.ulImm2VcmFlags & (uint32_t)0x10U) == (uint32_t)0x10U) ==> FALSE
      (2) if (gParameters.ulIMMDisable == 0x1a1a1a1aU) ==> FALSE
   Test Case Generation Notes:
            Cannot set value of field for a const class/struct/union for gParameters.ulIMMDisable in branch 2
TEST.END_NOTES:
TEST.VALUE:imm_interface.<<GLOBAL>>.gIMMControl.ulImm2VcmFlags:0
TEST.END

-- Test Case: BASIS-PATH-002-PARTIAL
TEST.UNIT:imm_interface
TEST.SUBPROGRAM:gfIMM_ConfigurationComplete
TEST.NEW
TEST.NAME:BASIS-PATH-002-PARTIAL
TEST.BASIS_PATH:2 of 3 (partial)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 2
      (1) if ((gIMMControl.ulImm2VcmFlags & (uint32_t)0x10U) == (uint32_t)0x10U) ==> FALSE
      (2) if (gParameters.ulIMMDisable == 0x1a1a1a1aU) ==> TRUE
   Test Case Generation Notes:
            Cannot set value of field for a const class/struct/union for gParameters.ulIMMDisable in branch 2
TEST.END_NOTES:
TEST.VALUE:imm_interface.<<GLOBAL>>.gIMMControl.ulImm2VcmFlags:0
TEST.END

-- Test Case: BASIS-PATH-003
TEST.UNIT:imm_interface
TEST.SUBPROGRAM:gfIMM_ConfigurationComplete
TEST.NEW
TEST.NAME:BASIS-PATH-003
TEST.BASIS_PATH:3 of 3
TEST.NOTES:
This is an automatically generated test case.
   Test Path 3
      (1) if ((gIMMControl.ulImm2VcmFlags & (uint32_t)0x10U) == (uint32_t)0x10U) ==> TRUE
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:imm_interface.<<GLOBAL>>.gIMMControl.ulImm2VcmFlags:16
TEST.END

-- Subprogram: gfIMM_KpmEnabled

-- Test Case: BASIS-PATH-001
TEST.UNIT:imm_interface
TEST.SUBPROGRAM:gfIMM_KpmEnabled
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 1
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.END

-- Subprogram: gfIMM_ShutDownEventsLoggingInvalid

-- Test Case: gfIMM_ShutDownEventsLoggingInvalid-PARTITIONS
TEST.UNIT:imm_interface
TEST.SUBPROGRAM:gfIMM_ShutDownEventsLoggingInvalid
TEST.NEW
TEST.NAME:gfIMM_ShutDownEventsLoggingInvalid-PARTITIONS
TEST.END

-- Subprogram: gfIMM_ShutdownCommanded

-- Test Case: BASIS-PATH-001
TEST.UNIT:imm_interface
TEST.SUBPROGRAM:gfIMM_ShutdownCommanded
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 2
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (1) if (1U == gIMMControl.ubImm2VcmCommand) ==> FALSE
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:imm_interface.<<GLOBAL>>.gIMMControl.ubImm2VcmCommand:<<MIN>>
TEST.END

-- Test Case: BASIS-PATH-001.002
TEST.UNIT:imm_interface
TEST.SUBPROGRAM:gfIMM_ShutdownCommanded
TEST.NEW
TEST.NAME:BASIS-PATH-001.002
TEST.BASIS_PATH:1 of 2
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (1) if (1U == gIMMControl.ubImm2VcmCommand) ==> FALSE
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:imm_interface.<<GLOBAL>>.gIMMControl.ubImm2VcmCommand:<<MIN>>
TEST.END

-- Test Case: BASIS-PATH-002-TEMPLATE
TEST.UNIT:imm_interface
TEST.SUBPROGRAM:gfIMM_ShutdownCommanded
TEST.NEW
TEST.NAME:BASIS-PATH-002-TEMPLATE
TEST.BASIS_PATH:2 of 2 (template)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 2
      (1) if (1U == gIMMControl.ubImm2VcmCommand) ==> TRUE
   Test Case Generation Notes:
      Cannot set variable to out of range value 1 in branch 1
TEST.END_NOTES:
TEST.END

-- Subprogram: gulIMM_CommunicationInitialize

-- Test Case: BASIS-PATH-001
TEST.UNIT:imm_interface
TEST.SUBPROGRAM:gulIMM_CommunicationInitialize
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 1
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.END

-- Subprogram: gvIMM_ProcessIpsPDO2

-- Test Case: gImpactSensorPdoData[eIpsIndex].ubRxData2[7u]_&_0x10u)_==_0x04u
TEST.UNIT:imm_interface
TEST.SUBPROGRAM:gvIMM_ProcessIpsPDO2
TEST.NEW
TEST.NAME:gImpactSensorPdoData[eIpsIndex].ubRxData2[7u]_&_0x10u)_==_0x04u
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (1) if (0U != (((gImpactSensorPdoData)[eIpsIndex]).ubRxData2)[0U]) ==> FALSE
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:imm_interface.<<GLOBAL>>.gImpactSensorPdoData[IPS_1].ubRxData2[0]:1
TEST.VALUE:imm_interface.<<GLOBAL>>.gImpactSensorPdoData[IPS_1].ubRxData2[7]:0x4
TEST.VALUE:imm_interface.gvIMM_ProcessIpsPDO2.eIpsIndex:IPS_3
TEST.VALUE:imm_interface.gvIMM_ProcessIpsPDO2.pubDataBytes:<<malloc 8>>
TEST.VALUE:imm_interface.gvIMM_ProcessIpsPDO2.pubDataBytes[0]:0x4
TEST.VALUE:imm_interface.gvIMM_ProcessIpsPDO2.pubDataBytes[7]:0x4
TEST.EXPECTED:imm_interface.<<GLOBAL>>.gImpactSensorPdoData[IPS_1].ubRxData2[0]:0x1
TEST.EXPECTED:imm_interface.<<GLOBAL>>.gImpactSensorPdoData[IPS_1].ubRxData2[7]:0x4
TEST.EXPECTED:imm_interface.gvIMM_ProcessIpsPDO2.eIpsIndex:IPS_3
TEST.END

-- Test Case: gImpactSensorPdoData[eIpsIndex].ubRxData2[7u]_&_0x10u)_==_0x10u_
TEST.UNIT:imm_interface
TEST.SUBPROGRAM:gvIMM_ProcessIpsPDO2
TEST.NEW
TEST.NAME:gImpactSensorPdoData[eIpsIndex].ubRxData2[7u]_&_0x10u)_==_0x10u_
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (1) if (0U != (((gImpactSensorPdoData)[eIpsIndex]).ubRxData2)[0U]) ==> FALSE
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:imm_interface.<<GLOBAL>>.gImpactSensorPdoData[IPS_1].ubRxData2[0]:1
TEST.VALUE:imm_interface.<<GLOBAL>>.gImpactSensorPdoData[IPS_1].ubRxData2[7]:0x10
TEST.VALUE:imm_interface.gvIMM_ProcessIpsPDO2.eIpsIndex:IPS_3
TEST.VALUE:imm_interface.gvIMM_ProcessIpsPDO2.pubDataBytes:<<malloc 8>>
TEST.VALUE:imm_interface.gvIMM_ProcessIpsPDO2.pubDataBytes[0]:0x10
TEST.VALUE:imm_interface.gvIMM_ProcessIpsPDO2.pubDataBytes[7]:0x10
TEST.EXPECTED:imm_interface.<<GLOBAL>>.gImpactSensorPdoData[IPS_1].ubRxData2[0]:0x1
TEST.EXPECTED:imm_interface.<<GLOBAL>>.gImpactSensorPdoData[IPS_1].ubRxData2[7]:0x10
TEST.EXPECTED:imm_interface.gvIMM_ProcessIpsPDO2.eIpsIndex:IPS_3
TEST.END

-- Test Case: gImpactSensorPdoData[eIpsIndex].ubRxData2[7u]_&_0x20u)_==_0x20u_
TEST.UNIT:imm_interface
TEST.SUBPROGRAM:gvIMM_ProcessIpsPDO2
TEST.NEW
TEST.NAME:gImpactSensorPdoData[eIpsIndex].ubRxData2[7u]_&_0x20u)_==_0x20u_
TEST.COMPOUND_ONLY
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (1) if (0U != (((gImpactSensorPdoData)[eIpsIndex]).ubRxData2)[0U]) ==> FALSE
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:imm_interface.<<GLOBAL>>.gImpactSensorPdoData[IPS_1].ubRxData2[0]:1
TEST.VALUE:imm_interface.<<GLOBAL>>.gImpactSensorPdoData[IPS_1].ubRxData2[7]:0x20
TEST.VALUE:imm_interface.gvIMM_ProcessIpsPDO2.eIpsIndex:IPS_3
TEST.VALUE:imm_interface.gvIMM_ProcessIpsPDO2.pubDataBytes:<<malloc 8>>
TEST.VALUE:imm_interface.gvIMM_ProcessIpsPDO2.pubDataBytes[0]:0x20
TEST.VALUE:imm_interface.gvIMM_ProcessIpsPDO2.pubDataBytes[7]:0x20
TEST.EXPECTED:imm_interface.<<GLOBAL>>.gImpactSensorPdoData[IPS_1].ubRxData2[0]:0x1
TEST.EXPECTED:imm_interface.<<GLOBAL>>.gImpactSensorPdoData[IPS_1].ubRxData2[7]:0x20
TEST.EXPECTED:imm_interface.gvIMM_ProcessIpsPDO2.eIpsIndex:IPS_3
TEST.END

-- Subprogram: gvIMM_SendCalibrationMessage

-- Test Case: BASIS-PATH-001
TEST.UNIT:imm_interface
TEST.SUBPROGRAM:gvIMM_SendCalibrationMessage
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 3
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (1) if ((Cal_Proc_None) != gAutoCalImmStat.eMode) ==> FALSE
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:imm_interface.<<GLOBAL>>.gAutoCalImmStat.eMode:Cal_Proc_None
TEST.VALUE:imm_interface.gvIMM_SendCalibrationMessage.ulUpdateRateMs:<<MIN>>
TEST.END

-- Test Case: BASIS-PATH-002-PARTIAL
TEST.UNIT:imm_interface
TEST.SUBPROGRAM:gvIMM_SendCalibrationMessage
TEST.NEW
TEST.NAME:BASIS-PATH-002-PARTIAL
TEST.BASIS_PATH:2 of 3 (partial)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 2
      (1) if ((Cal_Proc_None) != gAutoCalImmStat.eMode) ==> TRUE
      (2) if (sulCalTimer >= gParameters.ulIMM_CalibrationMessageSendRateMs) ==> FALSE
   Test Case Generation Notes:
      Cannot set sulCalTimer due to assignment
TEST.END_NOTES:
TEST.VALUE:imm_interface.<<GLOBAL>>.gAutoCalImmStat.eMode:Cal_Proc_Main_Hoist_Throt
TEST.VALUE:imm_interface.gvIMM_SendCalibrationMessage.ulUpdateRateMs:<<MIN>>
TEST.END

-- Test Case: BASIS-PATH-003-PARTIAL
TEST.UNIT:imm_interface
TEST.SUBPROGRAM:gvIMM_SendCalibrationMessage
TEST.NEW
TEST.NAME:BASIS-PATH-003-PARTIAL
TEST.BASIS_PATH:3 of 3 (partial)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 3
      (1) if ((Cal_Proc_None) != gAutoCalImmStat.eMode) ==> TRUE
      (2) if (sulCalTimer >= gParameters.ulIMM_CalibrationMessageSendRateMs) ==> TRUE
   Test Case Generation Notes:
      Cannot set sulCalTimer due to assignment
TEST.END_NOTES:
TEST.VALUE:imm_interface.<<GLOBAL>>.gAutoCalImmStat.eMode:Cal_Proc_Main_Hoist_Throt
TEST.VALUE:imm_interface.gvIMM_SendCalibrationMessage.ulUpdateRateMs:<<MIN>>
TEST.END

-- Subprogram: gvIMM_SendSROMessage

-- Test Case: BASIS-PATH-001-PARTIAL
TEST.UNIT:imm_interface
TEST.SUBPROGRAM:gvIMM_SendSROMessage
TEST.NEW
TEST.NAME:BASIS-PATH-001-PARTIAL
TEST.BASIS_PATH:1 of 2 (partial)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (1) if (sulSROTimer >= gParameters.ulIMM_SROMessageSendRateMs) ==> FALSE
   Test Case Generation Notes:
      Cannot set sulSROTimer due to assignment
TEST.END_NOTES:
TEST.VALUE:imm_interface.gvIMM_SendSROMessage.ulUpdateRateMs:<<MIN>>
TEST.END

-- Test Case: BASIS-PATH-002-PARTIAL
TEST.UNIT:imm_interface
TEST.SUBPROGRAM:gvIMM_SendSROMessage
TEST.NEW
TEST.NAME:BASIS-PATH-002-PARTIAL
TEST.BASIS_PATH:2 of 2 (partial)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 2
      (1) if (sulSROTimer >= gParameters.ulIMM_SROMessageSendRateMs) ==> TRUE
   Test Case Generation Notes:
      Cannot set sulSROTimer due to assignment
TEST.END_NOTES:
TEST.VALUE:imm_interface.gvIMM_SendSROMessage.ulUpdateRateMs:<<MIN>>
TEST.END

-- Subprogram: gvIMM_SendStreamingMsgs

-- Test Case: BASIS-PATH-001-PARTIAL
TEST.UNIT:imm_interface
TEST.SUBPROGRAM:gvIMM_SendStreamingMsgs
TEST.NEW
TEST.NAME:BASIS-PATH-001-PARTIAL
TEST.BASIS_PATH:1 of 4 (partial)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (1) for (ubMessageIndex < 8U) ==> FALSE
   Test Case Generation Notes:
      Conflict: Unable to validate expression-to-expression comparison in branch 1
TEST.END_NOTES:
TEST.VALUE:imm_interface.gvIMM_SendStreamingMsgs.uwUpdateRateMs:<<MIN>>
TEST.END

-- Test Case: BASIS-PATH-002-PARTIAL
TEST.UNIT:imm_interface
TEST.SUBPROGRAM:gvIMM_SendStreamingMsgs
TEST.NEW
TEST.NAME:BASIS-PATH-002-PARTIAL
TEST.BASIS_PATH:2 of 4 (partial)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 2
      (1) for (ubMessageIndex < 8U) ==> TRUE
      (2) if (ulMessageMask == (ulMessageMask & xulStreamMessageEnables) && 0U != (xuwStreamMessageRateMs)[ubMessageIndex]) ==> FALSE
   Test Case Generation Notes:
      Conflict: Unable to validate expression-to-expression comparison in branch 1
      Cannot set variable to unknown value in branch 2
      Cannot set ubMessageIndex due to assignment
TEST.END_NOTES:
TEST.VALUE:imm_interface.<<GLOBAL>>.xuwStreamMessageRateMs[0]:0
TEST.VALUE:imm_interface.gvIMM_SendStreamingMsgs.uwUpdateRateMs:<<MIN>>
TEST.END

-- Test Case: BASIS-PATH-003-PARTIAL
TEST.UNIT:imm_interface
TEST.SUBPROGRAM:gvIMM_SendStreamingMsgs
TEST.NEW
TEST.NAME:BASIS-PATH-003-PARTIAL
TEST.BASIS_PATH:3 of 4 (partial)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 3
      (1) for (ubMessageIndex < 8U) ==> TRUE
      (2) if (ulMessageMask == (ulMessageMask & xulStreamMessageEnables) && 0U != (xuwStreamMessageRateMs)[ubMessageIndex]) ==> TRUE
      (3) if ((suwMessageTimers)[ubMessageIndex] >= (xuwStreamMessageRateMs)[ubMessageIndex]) ==> TRUE
   Test Case Generation Notes:
      Conflict: Unable to validate expression-to-expression comparison in branch 1
      Cannot set variable to unknown value in branch 2
      Cannot set ubMessageIndex due to assignment
TEST.END_NOTES:
TEST.VALUE:imm_interface.<<GLOBAL>>.xuwStreamMessageRateMs[0]:<<MIN>>
TEST.VALUE:imm_interface.gvIMM_SendStreamingMsgs.uwUpdateRateMs:<<MIN>>
TEST.END

-- Test Case: BASIS-PATH-004-PARTIAL
TEST.UNIT:imm_interface
TEST.SUBPROGRAM:gvIMM_SendStreamingMsgs
TEST.NEW
TEST.NAME:BASIS-PATH-004-PARTIAL
TEST.BASIS_PATH:4 of 4 (partial)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 4
      (1) for (ubMessageIndex < 8U) ==> TRUE
      (2) if (ulMessageMask == (ulMessageMask & xulStreamMessageEnables) && 0U != (xuwStreamMessageRateMs)[ubMessageIndex]) ==> TRUE
      (3) if ((suwMessageTimers)[ubMessageIndex] >= (xuwStreamMessageRateMs)[ubMessageIndex]) ==> FALSE
   Test Case Generation Notes:
      Conflict: Unable to validate expression-to-expression comparison in branch 1
      Cannot set variable to unknown value in branch 2
      Cannot set ubMessageIndex due to assignment
TEST.END_NOTES:
TEST.VALUE:imm_interface.<<GLOBAL>>.xuwStreamMessageRateMs[0]:<<MAX>>
TEST.VALUE:imm_interface.gvIMM_SendStreamingMsgs.uwUpdateRateMs:<<MIN>>
TEST.END

-- Subprogram: gvIMM_SendTruckOperationMsg

-- Test Case: BASIS-PATH-001-PARTIAL
TEST.UNIT:imm_interface
TEST.SUBPROGRAM:gvIMM_SendTruckOperationMsg
TEST.NEW
TEST.NAME:BASIS-PATH-001-PARTIAL
TEST.BASIS_PATH:1 of 2 (partial)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (1) if (sulTruckOpTimer >= gParameters.ulIMM_TruckOpMessageSendRateMs) ==> FALSE
   Test Case Generation Notes:
      Cannot set sulTruckOpTimer due to assignment
TEST.END_NOTES:
TEST.VALUE:imm_interface.gvIMM_SendTruckOperationMsg.ulUpdateRateMs:<<MIN>>
TEST.END

-- Test Case: BASIS-PATH-002-PARTIAL
TEST.UNIT:imm_interface
TEST.SUBPROGRAM:gvIMM_SendTruckOperationMsg
TEST.NEW
TEST.NAME:BASIS-PATH-002-PARTIAL
TEST.BASIS_PATH:2 of 2 (partial)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 2
      (1) if (sulTruckOpTimer >= gParameters.ulIMM_TruckOpMessageSendRateMs) ==> TRUE
   Test Case Generation Notes:
      Cannot set sulTruckOpTimer due to assignment
TEST.END_NOTES:
TEST.VALUE:imm_interface.gvIMM_SendTruckOperationMsg.ulUpdateRateMs:<<MIN>>
TEST.END

-- Subprogram: gvIMM_SetStatusBits

-- Test Case: BASIS-PATH-001
TEST.UNIT:imm_interface
TEST.SUBPROGRAM:gvIMM_SetStatusBits
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 2
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (1) if (1U == fSetBits) ==> FALSE
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:imm_interface.gvIMM_SetStatusBits.ulBitmask:<<MIN>>
TEST.VALUE:imm_interface.gvIMM_SetStatusBits.fSetBits:<<MIN>>
TEST.END

-- Test Case: BASIS-PATH-002-PARTIAL
TEST.UNIT:imm_interface
TEST.SUBPROGRAM:gvIMM_SetStatusBits
TEST.NEW
TEST.NAME:BASIS-PATH-002-PARTIAL
TEST.BASIS_PATH:2 of 2 (partial)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 2
      (1) if (1U == fSetBits) ==> TRUE
   Test Case Generation Notes:
      Cannot set variable to out of range value 1 in branch 1
TEST.END_NOTES:
TEST.VALUE:imm_interface.gvIMM_SetStatusBits.ulBitmask:<<MIN>>
TEST.VALUE:imm_interface.gvIMM_SetStatusBits.fSetBits:<<MIN>>
TEST.END

-- Subprogram: vIMM_CalibReadCmdMessage_cb

-- Test Case: BASIS-PATH-001
TEST.UNIT:imm_interface
TEST.SUBPROGRAM:vIMM_CalibReadCmdMessage_cb
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 1
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.END

-- Subprogram: vIMM_CalibWriteCmdMessage_cb

-- Test Case: BASIS-PATH-001
TEST.UNIT:imm_interface
TEST.SUBPROGRAM:vIMM_CalibWriteCmdMessage_cb
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 1
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.END

-- Subprogram: vIMM_EventReport_cb

-- Test Case: BASIS-PATH-001
TEST.UNIT:imm_interface
TEST.SUBPROGRAM:vIMM_EventReport_cb
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 2
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (1) for (ubReportIndex < xIMMEventReport.uwNumberOfRecords) ==> FALSE
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:imm_interface.<<GLOBAL>>.xIMMEventReport.uwNumberOfRecords:0
TEST.VALUE:imm_interface.vIMM_EventReport_cb.uwMessageID:<<MIN>>
TEST.VALUE:imm_interface.vIMM_EventReport_cb.pszMessage:<<malloc 1>>
TEST.END

-- Test Case: BASIS-PATH-002
TEST.UNIT:imm_interface
TEST.SUBPROGRAM:vIMM_EventReport_cb
TEST.NEW
TEST.NAME:BASIS-PATH-002
TEST.BASIS_PATH:2 of 2
TEST.NOTES:
This is an automatically generated test case.
   Test Path 2
      (1) for (ubReportIndex < xIMMEventReport.uwNumberOfRecords) ==> TRUE
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:imm_interface.<<GLOBAL>>.xIMMEventReport.uwNumberOfRecords:1
TEST.VALUE:imm_interface.vIMM_EventReport_cb.uwMessageID:<<MIN>>
TEST.VALUE:imm_interface.vIMM_EventReport_cb.pszMessage:<<malloc 1>>
TEST.END

-- Subprogram: vIMM_FlagsCmdMessage_cb

-- Test Case: BASIS-PATH-001-PARTIAL
TEST.UNIT:imm_interface
TEST.SUBPROGRAM:vIMM_FlagsCmdMessage_cb
TEST.NEW
TEST.NAME:BASIS-PATH-001-PARTIAL
TEST.BASIS_PATH:1 of 4 (partial)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (1) if (gParameters.ulIMMDisable == 0x1a1a1a1aU) ==> FALSE
      (2) if (0U == xfConfigComplete) ==> FALSE
   Test Case Generation Notes:
            Cannot set value of field for a const class/struct/union for gParameters.ulIMMDisable in branch 1
TEST.END_NOTES:
TEST.VALUE:imm_interface.<<GLOBAL>>.xfConfigComplete:<<MIN>>
TEST.VALUE:imm_interface.vIMM_FlagsCmdMessage_cb.uwMessageID:<<MIN>>
TEST.VALUE:imm_interface.vIMM_FlagsCmdMessage_cb.pszMessage:<<malloc 1>>
TEST.END

-- Test Case: BASIS-PATH-002-PARTIAL
TEST.UNIT:imm_interface
TEST.SUBPROGRAM:vIMM_FlagsCmdMessage_cb
TEST.NEW
TEST.NAME:BASIS-PATH-002-PARTIAL
TEST.BASIS_PATH:2 of 4 (partial)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 2
      (1) if (gParameters.ulIMMDisable == 0x1a1a1a1aU) ==> FALSE
      (2) if (0U == xfConfigComplete) ==> TRUE
      (3) if ((gIMMControl.ulImm2VcmFlags & (uint32_t)0x8U) == (uint32_t)0x8U) ==> FALSE
   Test Case Generation Notes:
            Cannot set value of field for a const class/struct/union for gParameters.ulIMMDisable in branch 1
TEST.END_NOTES:
TEST.VALUE:imm_interface.<<GLOBAL>>.gIMMControl.ulImm2VcmFlags:0
TEST.VALUE:imm_interface.<<GLOBAL>>.xfConfigComplete:0
TEST.VALUE:imm_interface.vIMM_FlagsCmdMessage_cb.uwMessageID:<<MIN>>
TEST.VALUE:imm_interface.vIMM_FlagsCmdMessage_cb.pszMessage:<<malloc 1>>
TEST.END

-- Test Case: BASIS-PATH-003-PARTIAL
TEST.UNIT:imm_interface
TEST.SUBPROGRAM:vIMM_FlagsCmdMessage_cb
TEST.NEW
TEST.NAME:BASIS-PATH-003-PARTIAL
TEST.BASIS_PATH:3 of 4 (partial)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 3
      (1) if (gParameters.ulIMMDisable == 0x1a1a1a1aU) ==> FALSE
      (2) if (0U == xfConfigComplete) ==> TRUE
      (3) if ((gIMMControl.ulImm2VcmFlags & (uint32_t)0x8U) == (uint32_t)0x8U) ==> TRUE
   Test Case Generation Notes:
            Cannot set value of field for a const class/struct/union for gParameters.ulIMMDisable in branch 1
TEST.END_NOTES:
TEST.VALUE:imm_interface.<<GLOBAL>>.gIMMControl.ulImm2VcmFlags:8
TEST.VALUE:imm_interface.<<GLOBAL>>.xfConfigComplete:0
TEST.VALUE:imm_interface.vIMM_FlagsCmdMessage_cb.uwMessageID:<<MIN>>
TEST.VALUE:imm_interface.vIMM_FlagsCmdMessage_cb.pszMessage:<<malloc 1>>
TEST.END

-- Test Case: BASIS-PATH-004-PARTIAL
TEST.UNIT:imm_interface
TEST.SUBPROGRAM:vIMM_FlagsCmdMessage_cb
TEST.NEW
TEST.NAME:BASIS-PATH-004-PARTIAL
TEST.BASIS_PATH:4 of 4 (partial)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 4
      (1) if (gParameters.ulIMMDisable == 0x1a1a1a1aU) ==> TRUE
      (2) if (0U == xfConfigComplete) ==> FALSE
   Test Case Generation Notes:
            Cannot set value of field for a const class/struct/union for gParameters.ulIMMDisable in branch 1
TEST.END_NOTES:
TEST.VALUE:imm_interface.<<GLOBAL>>.xfConfigComplete:<<MIN>>
TEST.VALUE:imm_interface.vIMM_FlagsCmdMessage_cb.uwMessageID:<<MIN>>
TEST.VALUE:imm_interface.vIMM_FlagsCmdMessage_cb.pszMessage:<<malloc 1>>
TEST.END

-- Subprogram: vIMM_StatusReadCmdMessage_cb

-- Test Case: BASIS-PATH-001
TEST.UNIT:imm_interface
TEST.SUBPROGRAM:vIMM_StatusReadCmdMessage_cb
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 1
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.END

-- Subprogram: vIMM_WriteCmdMessage_cb

-- Test Case: BASIS-PATH-001
TEST.UNIT:imm_interface
TEST.SUBPROGRAM:vIMM_WriteCmdMessage_cb
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 4
TEST.NOTES:
This is an automatically generated test case.
   Test Path 1
      (1) if (uwIMM_command_message_size < 8U) ==> FALSE
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:imm_interface.<<GLOBAL>>.uwIMM_command_message_size:<<MAX>>
TEST.VALUE:imm_interface.vIMM_WriteCmdMessage_cb.uwMessageID:<<MIN>>
TEST.VALUE:imm_interface.vIMM_WriteCmdMessage_cb.pszMessage:<<malloc 1>>
TEST.END

-- Test Case: BASIS-PATH-002
TEST.UNIT:imm_interface
TEST.SUBPROGRAM:vIMM_WriteCmdMessage_cb
TEST.NEW
TEST.NAME:BASIS-PATH-002
TEST.BASIS_PATH:2 of 4
TEST.NOTES:
This is an automatically generated test case.
   Test Path 2
      (1) if (uwIMM_command_message_size < 8U) ==> TRUE
      (2) for (ubObjCtr < uwIMM_command_message_size) ==> FALSE
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:imm_interface.<<GLOBAL>>.uwIMM_command_message_size:0
TEST.VALUE:imm_interface.vIMM_WriteCmdMessage_cb.uwMessageID:<<MIN>>
TEST.VALUE:imm_interface.vIMM_WriteCmdMessage_cb.pszMessage:<<malloc 1>>
TEST.END

-- Test Case: BASIS-PATH-003
TEST.UNIT:imm_interface
TEST.SUBPROGRAM:vIMM_WriteCmdMessage_cb
TEST.NEW
TEST.NAME:BASIS-PATH-003
TEST.BASIS_PATH:3 of 4
TEST.NOTES:
This is an automatically generated test case.
   Test Path 3
      (1) if (uwIMM_command_message_size < 8U) ==> TRUE
      (2) for (ubObjCtr < uwIMM_command_message_size) ==> TRUE
      (3) if (0x3035U == uwIndex) ==> FALSE
   Test Case Generation Notes:
TEST.END_NOTES:
TEST.VALUE:imm_interface.<<GLOBAL>>.imm_command_msg_obj_list[0].uwIndex:<<MIN>>
TEST.VALUE:imm_interface.<<GLOBAL>>.uwIMM_command_message_size:1
TEST.VALUE:imm_interface.vIMM_WriteCmdMessage_cb.uwMessageID:<<MIN>>
TEST.VALUE:imm_interface.vIMM_WriteCmdMessage_cb.pszMessage:<<malloc 1>>
TEST.END

-- Test Case: BASIS-PATH-004-PARTIAL
TEST.UNIT:imm_interface
TEST.SUBPROGRAM:vIMM_WriteCmdMessage_cb
TEST.NEW
TEST.NAME:BASIS-PATH-004-PARTIAL
TEST.BASIS_PATH:4 of 4 (partial)
TEST.NOTES:
This is an automatically generated test case.
   Test Path 4
      (1) if (uwIMM_command_message_size < 8U) ==> TRUE
      (2) for (ubObjCtr < uwIMM_command_message_size) ==> TRUE
      (3) if (0x3035U == uwIndex) ==> TRUE
   Test Case Generation Notes:
      Cannot set variable to out of range value 12341 in branch 3
TEST.END_NOTES:
TEST.VALUE:imm_interface.<<GLOBAL>>.uwIMM_command_message_size:1
TEST.VALUE:imm_interface.vIMM_WriteCmdMessage_cb.uwMessageID:<<MIN>>
TEST.VALUE:imm_interface.vIMM_WriteCmdMessage_cb.pszMessage:<<malloc 1>>
TEST.END

-- Subprogram: vIMM_WriteRespCmdMessage_cb

-- Test Case: BASIS-PATH-001
TEST.UNIT:imm_interface
TEST.SUBPROGRAM:vIMM_WriteRespCmdMessage_cb
TEST.NEW
TEST.NAME:BASIS-PATH-001
TEST.BASIS_PATH:1 of 1
TEST.NOTES:
   No branches in subprogram
TEST.END_NOTES:
TEST.END

-- COMPOUND TESTS

TEST.SUBPROGRAM:<<COMPOUND>>
TEST.NEW
TEST.NAME:gvIMM_ProcessIpsPDO2_StateMachine
TEST.SLOT: "1", "imm_interface", "gvIMM_ProcessIpsPDO2", "1", "gImpactSensorPdoData[eIpsIndex].ubRxData2[7u]_&_0x10u)_==_0x04u"
TEST.SLOT: "2", "imm_interface", "gvIMM_ProcessIpsPDO2", "1", "gImpactSensorPdoData[eIpsIndex].ubRxData2[7u]_&_0x10u)_==_0x10u_"
TEST.SLOT: "3", "imm_interface", "gvIMM_ProcessIpsPDO2", "1", "gImpactSensorPdoData[eIpsIndex].ubRxData2[7u]_&_0x20u)_==_0x20u_"
TEST.END
--
