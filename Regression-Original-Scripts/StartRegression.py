# Name:       StartRegression.py
#
# Purpose:   This python script will call each VCAST test and time how long it takes to finish from the command line.
#            Additionally, it calls a check from "ProcessTestFolders()" to make sure all folders are there and if needed
#            changes the path. Lastly, The results are sent to EngSTL1 server and an email is generated with test times and where
#            on the server they are stored.
#
# Author:    Thomas Rode
#
# Options:   None
#
# Note:
#
# Python Version: 2.7.11
#          
# Reference: https://www.python.org/ (open source)
#
import re
import datetime
import time
import os
import shutil
from RTC_to_VCAST_Filepath_Correction import ProcessTestFolders
from EmailTestResults import email_main
from report_builder import build_report_main
from MD5_Hash_Class import MD5HashDriver

rtc_outfile ='C:/Embedded/iRTC Specific Files/antTmlVcmMasterBuildProperties.out'
definition1 = 'buildDefinitionId=TmlVcmMasterBuildDef001'
definition2 = 'buildDefinitionId=TmlVcmMasterBuildDef002'
definition3 = 'buildDefinitionId=TmlVcmMasterBuildDef003'
info_file = 'C:/rtc_workspaces/TmlVcmMasterBuildDef001_Sandbox/VCM_APP/src/general/'
SelectWrite = '1'
SelectRead = '2'
VehicleBundlePartNumberFile = 'VehicleBundlePartNumberFile.txt'
CurrentWorkingDirectory = ''
SourcePathForVcast = 'C:/rtc_workspaces/TmlVcmMasterBuildDef001_Sandbox/VCAST'


def RunSimulatedOrReal():

    # Check info.s for current Vehicle bundle if part number has "-X"  for example "154620-903-34-X" then this is an intermediate release, run simulated
    with open(info_file + "info.s") as f:
        for line in f:
            if "VEH_BUNDLE_PN: .strz" in line:
                 if "-X" in line or "-x" in line:
                     print("Not-Released")
                     CodeReleased = False
                 else:
                     print("Released")
                     CodeReleased = True

                 # Get Current Vehicle Bundle Part number, remove the "", Record this to a file
                 line = line.strip('VEH_BUNDLE_PN: .strz ')
                 line = re.sub(r'^"|"$', '', line)
                 f = open(VehicleBundlePartNumberFile, "w")
                 f.writelines(line)
                 f.close()

    # Run On-Target or not
    if CodeReleased == True:
        print("\n")
        print("Running VectorCast:>>>>>>>>>> On-Target <<<<<<<<<<<<< ")
        print("\n")

        # Run tests real time 
        fRunSimulated = False
        
    else:
        print("\n")
        print("Running VectorCast:>>>>>>>>>> Simulated <<<<<<<<<<<<< ")
        print("\n")

        # Run tests simulated
        fRunSimulated = True

    # Force this try in the interum    
    fRunSimulated = True   
    return fRunSimulated  


def vcast_integration():

    # Should VectorCast Run in simulated or On-Target
    fRunSimulated = RunSimulatedOrReal()

    if fRunSimulated == True:
     
        Testname = ['StartRegressionTml.bat']
        
    else:  
        Testname = ['StartRegressionTml.bat']

    # Determine if Change Based Testing batch file to run
    if definition3 in open(rtc_outfile,'r+').read():
        Testname = ['StartRegressionTmlCBT.bat']

    # Determine if Quality batch file to run
    if definition2 in open(rtc_outfile,'r+').read():
        Testname = ['StartRegressionTmlQuality.bat']

    # Make folder and Copy VCAST Manage project folder over to this regression run
    print("***********************************************************************\n")
    print("************************* COPY VECTOR CAST MANAGE *********************\n")
    print("***********************************************************************\n")
    try:
        if not os.path.exists("./VCAST"):
            os.makedirs("./VCAST")
    except OSError:
        print ('Error: Creating directory. ' +  "/VCAST")

    # Make a VCAST folder
    CurrentRegressionDirectory = (CurrentWorkingDirectory + "/VCAST")

    # Copy RTC VCAST folder completely 
    CopyDir(SourcePathForVcast,CurrentRegressionDirectory)

    # Copy ParseHtmlForManage.py to /VCAST Directory
    shutil.copy('ParseHtmlForManage.py', CurrentRegressionDirectory)

    # Copy Copy2Corp2013.py to /VCAST Directory
    shutil.copy('Copy2Corp2013.py', CurrentRegressionDirectory)

    # Copy StartRegressionTml.bat This may be temporary since its not there in RTC to /VCAST Directory
    shutil.copy('StartRegressionTml.bat', CurrentRegressionDirectory)

    # Copy StartRegressionTmlCBT.bat This may be temporary since its not there in RTC to /VCAST Directory
    shutil.copy('StartRegressionTmlCBT.bat', CurrentRegressionDirectory)

    # Copy StartRegressionTmlQuality.bat This may be temporary since its not there in RTC to /VCAST Directory
    shutil.copy('StartRegressionTmlQuality.bat', CurrentRegressionDirectory)

    # Copy CallAnalytics.bat to /VCAST Directory
    shutil.copy('CallAnalytics.bat', CurrentRegressionDirectory)

    # Copy KillAnalytics.bat to /VCAST Directory
    shutil.copy('KillAnalytics.bat', CurrentRegressionDirectory)

    # Write the RTC test file needed for the report
    WriteRTCtxtfile()

    # Copy RTC_Stream.txt to /VCAST Directory
    shutil.copy('RTC_Stream.txt', CurrentRegressionDirectory)

    print("***********************************************************************\n")
    print("************************* COPY VECTOR CAST MANAGE COMPLETE ************\n")
    print("***********************************************************************\n")

    # Change directory to VCAST to allow batch to run
    os.chdir(CurrentRegressionDirectory)
        
             
    # Check to see if folders are corrrect and update the .env and .bat(.bat sets run simulated or real compiler setting) test files 
    #if ProcessTestFolders(fRunSimulated) == False:
    BypassProcessTestFolders = False 
    if BypassProcessTestFolders == False:

        # Test folder are all present
        fTestFoldersPresent = True

        # if On-Target should Testname be updated because of Hash difference
        if fRunSimulated == False:
            
            #Update this list to reflect what needs to run with a pause
            Testname = MD5HashDriver(SelectRead)            
            print("Here is what StartRegression.py wil run ON-target", Testname)          
            time.sleep(10)
            
        else:
            # Run MD5 Hash and output to text files if differences
            MD5HashDriver(SelectRead)           
    
        # loop through all test test module
        # write to the .txt file to save the entry 
        f = open("VCAST Test Times.txt", "w")
        TotalTestTimeMinutes = 0
        
        for CurrentTest in Testname:
            print(CurrentTest)
    
            # test time measurement start
            print("***********************************************************************\n")
            print("************************* STARTING VCAST TEST *************************\n")
            print("***********************************************************************\n")

            # Get a starting time
            t0 = time.time()

            # This call to the batch file that actually runs all the VCAST stuff this is the thing to time
            os.system(CurrentTest)

            # Print how long the test took, round the number to two decimal places
            Testtime = time.time() - t0
            Testtime = round(Testtime,2)
            OriginalTesttime = Testtime

            # Calculate the time into minutes, seconds format
            if Testtime >= 60:
                TestTimeMinutes = (Testtime / 60)
            else:
                TestTimeMinutes = 0
            
            TestTimeSeconds = (Testtime % 60)
            
            # Increment the total test time   
            TotalTestTimeMinutes += TestTimeMinutes

            # Print out to command line 
            print ("Time in minutes: {0} Time in seconds: {1}" .format(round(TestTimeMinutes,0), TestTimeSeconds)) 
            print ("Test: {0}   Test Time: {1}".format(CurrentTest, OriginalTesttime))
            print('\n')

            # Format the output text file
            f.writelines(CurrentTest)
            f.write('\n')
            f.write('INTEGRATION TESTING TIME IN MINUTES: ' + str(round(TestTimeMinutes,0)) + ' ' + 'SECONDS: ' + str(TestTimeSeconds))            
            f.write('\n')
            f.write('\n')

        # Calculated total hours and minutes print as last line
        if TotalTestTimeMinutes >= 60:
            TotalTestTimeHours = (TotalTestTimeMinutes / 60)

            #Get the remainder in minutes
            TotalTestTimeMinutes = (TotalTestTimeMinutes % 60)
        else:
            TotalTestTimeHours = 0

        #Truncate all least significant numbers
        TotalTestTimeHours = truncate(TotalTestTimeHours,0)    
        
        # Format the output text file last entry
        f.write('\n')
        f.write('\n')
        f.write('TOTAL INTEGRATION TESTING TIME IN HOURS: ' + str(TotalTestTimeHours) + ' ' + 'MINUTES: ' + str(round(TotalTestTimeMinutes,0))) 
        
        # Close test file
        f.close()

        # Update the Hashes of All test .C files after On-Target regression to re-baseline for next regression
        if fRunSimulated == False:
            
            #Update all hashes 
            MD5HashDriver(SelectWrite)           

        print("***********************************************************************\n")
        print("************************ ENDING VCAST TEST ****************************\n")
        print("***********************************************************************\n")
        
    else:
        # All test folders not present
        fTestFoldersPresent = False
        
    return fRunSimulated, fTestFoldersPresent 


def truncate(f, n):
    '''Truncates/pads a float f to n decimal places without rounding'''
    s = '%.12f' % f
    i, p, d = s.partition('.')
    return '.'.join([i, (d+'0'*n)[:n]])

def WriteRTCtxtfile():
     #Open .OUT file 
    if definition1 in open(rtc_outfile,'r+').read():
        print("Found TmlVcmMasterBuildDef001")
        output = 'MAIN DEVELOPMENT STREAM' 
    elif definition2 in open(rtc_outfile,'r+').read():
        print("Found TmlVcmMasterBuildDef002")
        output = 'QUALITY STREAM'
    elif definition3 in open(rtc_outfile,'r+').read():
        print("Found TmlVcmMasterBuildDef003")
        output = 'MAIN DEVELOPMENT STREAM CBT'
    else:
        print("Build definition not found")
        output = 'Undefined'

    # write this file to flag which RTC stream was used
    f = open('RTC_Stream.txt', "w")
    f.writelines(output)
    f.close()    

def RemoveRegressionFolders():

    IntegrationList =   [ 'VCAST' ]
                        #['AECP_MAIN_INTEGRATION',
                        #'BECP_MAIN_INTEGRATION',
                        #'EVENT_MANAGER_INTEGRATION',
                        #'SWITCH_BOARD_INTEGRATION',
                        #'EXTERNAL_ALARM_INTEGRATION',
                        #'GYRO_DRIVER_INTEGRATION',
                        #'FEATURES_INTERFACE_INTEGRATION',
                        #'SUPV_INTERFACE_INTEGRATION',
                        #'IMM_INTERFACE_INTEGRATION',
                        #'OS_INTERFACE_INTEGRATION',
                        #'STEERING_SIMULATION_INTEGRATION',
                        #'IP_INTERFACE_INTEGRATION',
                        #'MANIFEST_MANAGER_INTEGRATION',
                        #'TRACTION_SIMULATION_INTEGRATION',
                        #'FRAM_DRIVER_INTEGRATION',
                        #'UTILITIES_INTEGRATION.',
                        #'DIAGNOSTICS_INTEGRATION'
                        #]
       
    # change the directory back
    os.chdir(CurrentWorkingDirectory)
   
    # Remove entire test folders
    try:
        for i in range(len(IntegrationList)):
            shutil.rmtree(IntegrationList[i])
            
    except:
        print("Could not remove")
    time.sleep(3)

def CopyDir(sourcepath,dest_path):
    
    # New Folder made in Regression
    try:
        print ("Start directory Copy")#os.makedirs(dest_path))
         # Copy the files from source to destination 
        for filename in os.listdir(sourcepath):
            if os.path.isfile(os.path.join(sourcepath, filename)):
                shutil.copy(os.path.join(sourcepath, filename), os.path.join(dest_path, filename))

        # Copy the folders from source to destination
        for item in os.listdir(sourcepath):
            s = os.path.join(sourcepath, item)
            d = os.path.join(dest_path, item)
            if os.path.isdir(s):
                shutil.copytree(s, d, False, None)
            else:
                shutil.copy2(s, d)       
    except ValueError:
        print("Error")

    print ("Complete directory Copy")
   
    
# Main program - Supports execution from the command line
if __name__ == "__main__":

    # Get current directory
    CurrentWorkingDirectory = os.getcwd()
   
    # Perform build according to command line options
    fRunSimulated, fTestFoldersPresent = vcast_integration()
       

    # check to see if folders are all there before emailing results
    if fTestFoldersPresent == True:

        # Write the RTC test file needed for the report
        #WriteRTCtxtfile()
        
        print("***********************************************************************\n")
        print("*********** GENERATING Cumulative_results.xlsx  IN-PROGRESS ***********\n")
        print("***********************************************************************\n")
        #Convert HTML to XLSX
        #build_report_main()

        print("***********************************************************************\n")
        print("*********** GENERATING Cumulative_results.xlsx  COMPLETE **************\n")
        print("***********************************************************************\n")

        
        print("***********************************************************************\n")
        print("*********** STORING AND EMAILING TEST RESULTS IN-PROGRESS *************\n")
        print("***********************************************************************\n")
        
        # Email the results Do this due to outlook not receiving the sent email
        email_main(False, fRunSimulated)
        time.sleep(10)
        email_main(True, fRunSimulated) 

        print("***********************************************************************\n")
        print("*********** STORING AND EMAILING TEST RESULTS COMPLETE ****************\n")
        print("***********************************************************************\n")

        print("***********************************************************************\n")
        print("*********** CLEARING NO LONGER NEEDED REGRESSION FILES AND FOLDERS  ***\n")
        print("***********************************************************************\n")
        # Clear out folders
        #RemoveRegressionFolders()
        print("*******************************************************************************\n")
        print("*********** CLEARING NO LONGER NEEDED REGRESSION FILES AND FOLDERS COMPLETE ***\n")
        print("*******************************************************************************\n")

    else:
        print("***********************************************************************\n")
        print("*********** SOME/ALL TEST FOLDERS ARE NOT PRESENT NEEDED FOR REGRESSION\n")
        print("***********************************************************************\n")

      
