-- VectorCAST 6.4p (01/31/17)
-- Test Case Script
-- 
-- Environment    : GYRO_DRIVER_INTEGRATION
-- Unit(s) Under Test: gyro_driver
-- 
-- Script Features
TEST.SCRIPT_FEATURE:C_DIRECT_ARRAY_INDEXING
TEST.SCRIPT_FEATURE:CPP_CLASS_OBJECT_REVISION
TEST.SCRIPT_FEATURE:MULTIPLE_UUT_SUPPORT
TEST.SCRIPT_FEATURE:MIXED_CASE_NAMES
TEST.SCRIPT_FEATURE:STATIC_HEADER_FUNCS_IN_UUTS
--

-- Unit: gyro_driver

-- Subprogram: fGYRO_DisableIntInvenSense

-- Test Case: fGYRO_DisableIntInvenSense.001
TEST.UNIT:gyro_driver
TEST.SUBPROGRAM:fGYRO_DisableIntInvenSense
TEST.NEW
TEST.NAME:fGYRO_DisableIntInvenSense.001
TEST.COMPOUND_ONLY
TEST.VALUE:gyro_driver.fGYRO_DisableIntInvenSense.pDesc:<<malloc 1>>
TEST.VALUE:uut_prototype_stubs.gullTimer_ReadTimer.return:3000000
TEST.VALUE:uut_prototype_stubs.gulTimer_Counts_to_us.return:1,3
TEST.EXPECTED:gyro_driver.fGYRO_DisableIntInvenSense.return:0
TEST.END

-- Subprogram: fGYRO_DisableIntNull

-- Test Case: fGYRO_DisableIntNull.001
TEST.UNIT:gyro_driver
TEST.SUBPROGRAM:fGYRO_DisableIntNull
TEST.NEW
TEST.NAME:fGYRO_DisableIntNull.001
TEST.COMPOUND_ONLY
TEST.VALUE:gyro_driver.fGYRO_DisableIntNull.pDesc:<<malloc 1>>
TEST.EXPECTED:gyro_driver.fGYRO_DisableIntNull.return:0
TEST.END

-- Subprogram: fGYRO_ReadIdInvenSense

-- Test Case: fGYRO_ReadIdInvenSense.001
TEST.UNIT:gyro_driver
TEST.SUBPROGRAM:fGYRO_ReadIdInvenSense
TEST.NEW
TEST.NAME:fGYRO_ReadIdInvenSense.001
TEST.COMPOUND_ONLY
TEST.STUB:gyro_driver.fGYRO_ReadMeasureInvenSense
TEST.VALUE:gyro_driver.fGYRO_ReadIdInvenSense.pDesc:<<malloc 1>>
TEST.VALUE:gyro_driver.fGYRO_ReadIdInvenSense.pDesc[0].Data.rSampleRateMs:2.0
TEST.VALUE:gyro_driver.fGYRO_ReadIdInvenSense.pDesc[0].Data.swSelfTestDisabledSampleAvg[GYRO_X_AXIS]:0
TEST.VALUE:gyro_driver.fGYRO_ReadIdInvenSense.pDesc[0].Data.swSelfTestEnabledSampleAvg[GYRO_X_AXIS]:1
TEST.VALUE:gyro_driver.fGYRO_ReadIdInvenSense.pDesc[0].Data.swStrValue[GYRO_X_AXIS]:0
TEST.VALUE:uut_prototype_stubs.gullTimer_ReadTimer.return:3000000
TEST.VALUE:uut_prototype_stubs.gulTimer_Counts_to_us.return:1,3
TEST.EXPECTED:gyro_driver.fGYRO_ReadIdInvenSense.pDesc[0].Debug.fDoSelfTest:0
TEST.EXPECTED:gyro_driver.fGYRO_ReadIdInvenSense.pDesc[0].Data.rSampleRateMs:2.0
TEST.EXPECTED:gyro_driver.fGYRO_ReadIdInvenSense.pDesc[0].Data.swSelfTestDisabledSampleAvg[GYRO_X_AXIS]:0
TEST.EXPECTED:gyro_driver.fGYRO_ReadIdInvenSense.pDesc[0].Data.swSelfTestEnabledSampleAvg[GYRO_X_AXIS]:1
TEST.EXPECTED:gyro_driver.fGYRO_ReadIdInvenSense.pDesc[0].Data.swStrValue[GYRO_X_AXIS]:0
TEST.EXPECTED:gyro_driver.fGYRO_ReadIdInvenSense.return:0
TEST.END

-- Subprogram: fGYRO_ReadIdNull

-- Test Case: fGYRO_ReadIdNull.001
TEST.UNIT:gyro_driver
TEST.SUBPROGRAM:fGYRO_ReadIdNull
TEST.NEW
TEST.NAME:fGYRO_ReadIdNull.001
TEST.COMPOUND_ONLY
TEST.EXPECTED:gyro_driver.fGYRO_ReadIdNull.return:0
TEST.END

-- Subprogram: fGYRO_ReadMeasureInvenSense

-- Test Case: fGYRO_ReadMeasureInvenSense.001
TEST.UNIT:gyro_driver
TEST.SUBPROGRAM:fGYRO_ReadMeasureInvenSense
TEST.NEW
TEST.NAME:fGYRO_ReadMeasureInvenSense.001
TEST.COMPOUND_ONLY
TEST.VALUE:<<OPTIONS>>.EVENT_LIMIT:100
TEST.VALUE:gyro_driver.fGYRO_ReadMeasureInvenSense.pDesc:<<malloc 1>>
TEST.VALUE:uut_prototype_stubs.gullTimer_ReadTimer.return:3000000
TEST.VALUE:uut_prototype_stubs.gulTimer_Counts_to_us.return:1,3
TEST.VALUE_USER_CODE:gyro_driver.fGYRO_ReadMeasureInvenSense.pDesc.pDesc[0].Data.swRawCounts.swRawCounts[GYRO_X_AXIS]
uint16_t *puwWorkingValue; 
<<gyro_driver.fGYRO_ReadMeasureInvenSense.pDesc>>[0].Data.swRawCounts[GYRO_X_AXIS] = (*puwWorkingValue );
TEST.END_VALUE_USER_CODE:
TEST.VALUE_USER_CODE:gyro_driver.fGYRO_ReadMeasureInvenSense.pDesc.pDesc[0].Data.rScaledValue.rScaledValue[GYRO_X_AXIS]
gyro_axis_t eAxisCtr;
<<gyro_driver.fGYRO_ReadMeasureInvenSense.pDesc>>[0].Data.rScaledValue[GYRO_X_AXIS] = ( eAxisCtr );
TEST.END_VALUE_USER_CODE:
TEST.VALUE_USER_CODE:gyro_driver.fGYRO_ReadMeasureInvenSense.pDesc.pDesc[0].SerialPort.fSPI_Send
 const uint8_t READ_GYRO_CMD ;
<<gyro_driver.fGYRO_ReadMeasureInvenSense.pDesc>>[0].SerialPort.fSPI_Send = ( READ_GYRO_CMD );
TEST.END_VALUE_USER_CODE:
TEST.EXPECTED_USER_CODE:gyro_driver.fGYRO_ReadMeasureInvenSense.pDesc.pDesc[0].SerialPort.fSPI_Send
const uint8_t READ_GYRO_CMD;
{{ <<gyro_driver.fGYRO_ReadMeasureInvenSense.pDesc>>[0].SerialPort.fSPI_Send == ( READ_GYRO_CMD ) }}
TEST.END_EXPECTED_USER_CODE:
TEST.END

-- Test Case: fGYRO_ReadMeasureInvenSense.002
TEST.UNIT:gyro_driver
TEST.SUBPROGRAM:fGYRO_ReadMeasureInvenSense
TEST.NEW
TEST.NAME:fGYRO_ReadMeasureInvenSense.002
TEST.COMPOUND_ONLY
TEST.STUB:gyro_driver.gfGYRO_Init
TEST.STUB:gyro_driver.fGYRO_ReadIdInvenSense
TEST.VALUE:gyro_driver.fGYRO_ReadIdInvenSense.return:0
TEST.VALUE:uut_prototype_stubs.gullTimer_ReadTimer.return:3000000
TEST.VALUE:uut_prototype_stubs.gulTimer_Counts_to_us.return:1,3
TEST.EXPECTED:gyro_driver.fGYRO_ReadMeasureInvenSense.return:0
TEST.END

-- Subprogram: fGYRO_ReadMeasureNull

-- Test Case: fGYRO_ReadMeasureNull.001
TEST.UNIT:gyro_driver
TEST.SUBPROGRAM:fGYRO_ReadMeasureNull
TEST.NEW
TEST.NAME:fGYRO_ReadMeasureNull.001
TEST.COMPOUND_ONLY
TEST.VALUE:gyro_driver.fGYRO_ReadMeasureNull.pDesc:<<malloc 1>>
TEST.EXPECTED:gyro_driver.fGYRO_ReadMeasureNull.return:0
TEST.END

-- Subprogram: fGYRO_SelfTestNull

-- Test Case: fGYRO_SelfTestNull.001
TEST.UNIT:gyro_driver
TEST.SUBPROGRAM:fGYRO_SelfTestNull
TEST.NEW
TEST.NAME:fGYRO_SelfTestNull.001
TEST.COMPOUND_ONLY
TEST.EXPECTED:gyro_driver.fGYRO_SelfTestNull.return:0
TEST.END

-- Subprogram: gfGYRO_Init

-- Test Case: gfGYRO_Init.001
TEST.UNIT:gyro_driver
TEST.SUBPROGRAM:gfGYRO_Init
TEST.NEW
TEST.NAME:gfGYRO_Init.001
TEST.COMPOUND_ONLY
TEST.VALUE:<<OPTIONS>>.EVENT_LIMIT:100
TEST.VALUE:gyro_driver.gfGYRO_Init.pGyro:<<malloc 1>>
TEST.VALUE:gyro_driver.gfGYRO_Init.pGyro[0].Operation.fReadID:fGYRO_ReadIdNull
TEST.VALUE:gyro_driver.gfGYRO_Init.pGyro[0].Operation.fReadMeasurement:fGYRO_ReadMeasureNull
TEST.VALUE:gyro_driver.gfGYRO_Init.pGyro[0].Operation.fPerformSelfTest:fGYRO_SelfTestNull
TEST.VALUE:gyro_driver.gfGYRO_Init.pGyro[0].Operation.fDisableInterrupt:fGYRO_DisableIntNull
TEST.VALUE:gyro_driver.gfGYRO_Init.pPort:<<malloc 1>>
TEST.EXPECTED:gyro_driver.gfGYRO_Init.pGyro[0].Description.Data.ubDeviceId:0
TEST.EXPECTED:gyro_driver.gfGYRO_Init.pGyro[0].Operation.fReadID:fGYRO_ReadIdNull
TEST.EXPECTED:gyro_driver.gfGYRO_Init.pGyro[0].Operation.fReadMeasurement:fGYRO_ReadMeasureNull
TEST.EXPECTED:gyro_driver.gfGYRO_Init.pGyro[0].Operation.fPerformSelfTest:fGYRO_SelfTestNull
TEST.EXPECTED:gyro_driver.gfGYRO_Init.pGyro[0].Operation.fDisableInterrupt:fGYRO_DisableIntNull
TEST.EXPECTED:gyro_driver.gfGYRO_Init.return:0
TEST.VALUE_USER_CODE:gyro_driver.gfGYRO_Init.pGyro.pGyro[0].Description.SerialPort.fSPI_Initialize
boolean (*fSPI_Initialize)(void);                 /**< Pointer to function that initilizes SPI port used by the gyroscope driver */
    boolean (*fSPI_Send)(const uint8_t* pubSendData,  /**< Pointer to SPI send handler used by the gyroscope driver */
                         uint8_t ubSendSize,
                         uint8_t* pubReceiveData,
                         uint8_t ubReceiveSize); 
<<gyro_driver.gfGYRO_Init.pGyro>>[0].Description.SerialPort.fSPI_Initialize = (&fSPI_Initialize);
TEST.END_VALUE_USER_CODE:
TEST.VALUE_USER_CODE:gyro_driver.gfGYRO_Init.pPort.pPort[0].fSPI_Initialize
 boolean (*fSPI_Initialize)(void);                 /**< Pointer to function that initilizes SPI port used by the gyroscope driver */
    boolean (*fSPI_Send)(const uint8_t* pubSendData,  /**< Pointer to SPI send handler used by the gyroscope driver */
                         uint8_t ubSendSize,
                         uint8_t* pubReceiveData,
                         uint8_t ubReceiveSize); 
<<gyro_driver.gfGYRO_Init.pPort>>[0].fSPI_Initialize = (&fSPI_Initialize);
TEST.END_VALUE_USER_CODE:
TEST.END

-- Subprogram: gvGYRO_AbortSelfTest

-- Test Case: gvGYRO_AbortSelfTest.001
TEST.UNIT:gyro_driver
TEST.SUBPROGRAM:gvGYRO_AbortSelfTest
TEST.NEW
TEST.NAME:gvGYRO_AbortSelfTest.001
TEST.COMPOUND_ONLY
TEST.VALUE:<<OPTIONS>>.EVENT_LIMIT:100
TEST.VALUE:gyro_driver.gvGYRO_AbortSelfTest.pDesc:<<malloc 1>>
TEST.VALUE:uut_prototype_stubs.gullTimer_ReadTimer.return:3000000
TEST.VALUE:uut_prototype_stubs.gulTimer_Counts_to_us.return:1,3
TEST.EXPECTED:gyro_driver.gvGYRO_AbortSelfTest.pDesc[0].Debug.fDeviceIdRequested:0
TEST.EXPECTED:gyro_driver.gvGYRO_AbortSelfTest.pDesc[0].Debug.fDoSelfTest:0
TEST.EXPECTED:gyro_driver.gvGYRO_AbortSelfTest.pDesc[0].Data.fPassedSelfTest:"\0"
TEST.VALUE_USER_CODE:gyro_driver.gvGYRO_AbortSelfTest.pDesc.pDesc[0].SerialPort.fSPI_Send
<<gyro_driver.gvGYRO_AbortSelfTest.pDesc>>[0].SerialPort.fSPI_Send = ( &vGYRO_Delay );

TEST.END_VALUE_USER_CODE:
TEST.END

-- Subprogram: gvGYRO_ProcessDebug

-- Test Case: gvGYRO_ProcessDebug.001
TEST.UNIT:gyro_driver
TEST.SUBPROGRAM:gvGYRO_ProcessDebug
TEST.NEW
TEST.NAME:gvGYRO_ProcessDebug.001
TEST.COMPOUND_ONLY
TEST.VALUE:gyro_driver.gvGYRO_ProcessDebug.pGyro:<<malloc 1>>
TEST.VALUE:gyro_driver.gvGYRO_ProcessDebug.pGyro[0].Description.Debug.fDeviceIdRequested:1
TEST.VALUE:gyro_driver.gvGYRO_ProcessDebug.pGyro[0].Description.Debug.fDoSelfTest:0
TEST.VALUE:uut_prototype_stubs.gullTimer_ReadTimer.return:3000000
TEST.VALUE:uut_prototype_stubs.gulTimer_Counts_to_us.return:1,3
TEST.EXPECTED:gyro_driver.gvGYRO_ProcessDebug.pGyro[0].Description.Debug.fDeviceIdRequested:1
TEST.EXPECTED:gyro_driver.gvGYRO_ProcessDebug.pGyro[0].Description.Debug.fDoSelfTest:0
TEST.END

-- COMPOUND TESTS

TEST.SUBPROGRAM:<<COMPOUND>>
TEST.NEW
TEST.NAME:<<AbortSelfTest>>.004
TEST.SLOT: "1", "gyro_driver", "gvGYRO_AbortSelfTest", "1", "gvGYRO_AbortSelfTest.001"
TEST.END
--

-- COMPOUND TESTS

TEST.SUBPROGRAM:<<COMPOUND>>
TEST.NEW
TEST.NAME:<<ConfigureIvenSense>>.002
TEST.SLOT: "1", "gyro_driver", "gfGYRO_Init", "1", "gfGYRO_Init.001"
TEST.SLOT: "2", "gyro_driver", "gvGYRO_ProcessDebug", "1", "gvGYRO_ProcessDebug.001"
TEST.SLOT: "3", "gyro_driver", "fGYRO_ReadMeasureInvenSense", "1", "fGYRO_ReadMeasureInvenSense.002"
TEST.SLOT: "4", "gyro_driver", "fGYRO_ReadIdInvenSense", "1", "fGYRO_ReadIdInvenSense.001"
TEST.SLOT: "5", "gyro_driver", "fGYRO_ReadIdNull", "1", "fGYRO_ReadIdNull.001"
TEST.SLOT: "6", "gyro_driver", "gvGYRO_ProcessDebug", "1", "gvGYRO_ProcessDebug.001"
TEST.END
--

-- COMPOUND TESTS

TEST.SUBPROGRAM:<<COMPOUND>>
TEST.NEW
TEST.NAME:<<DisableIntInvenSense>>.001
TEST.SLOT: "1", "gyro_driver", "fGYRO_DisableIntInvenSense", "1", "fGYRO_DisableIntInvenSense.001"
TEST.END
--

-- COMPOUND TESTS

TEST.SUBPROGRAM:<<COMPOUND>>
TEST.NEW
TEST.NAME:<<ReadInvenSense>>.003
TEST.SLOT: "1", "gyro_driver", "fGYRO_ReadIdInvenSense", "1", "fGYRO_ReadIdInvenSense.001"
TEST.END
--
