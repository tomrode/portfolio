del commands.tmp
echo options C_COMPILER_CFG_SOURCE PY_CONFIGURATOR >> commands.tmp
echo options C_COMPILER_FAMILY_NAME Green_Hills >> commands.tmp
echo options C_COMPILER_HIERARCHY_STRING Green Hills_PPC_Bare Board_MBX860_mpserv-multi_C >> commands.tmp
echo options C_COMPILER_PY_ARGS  >> commands.tmp
echo options C_COMPILER_TAG PPC_GH_BARE_MBX_MP_MULTI >> commands.tmp
echo options C_COMPILER_VERSION_CMD ccppc -V not.here.c >> commands.tmp
echo options C_COMPILE_CMD ccppc -c -G -w -bsp generic -cpu=ppc5777c -bigendian -vle -fhard -floatsingle -fprecise >> commands.tmp
echo options C_COMPILE_CMD_FLAG -c >> commands.tmp
echo options C_COMPILE_EXCLUDE_FLAGS -o^^** >> commands.tmp
echo options C_DEBUG_CMD multi >> commands.tmp
echo options C_DEFINE_LIST TX_NO_EVENT_INFO TX_ENABLE_EVENT_FILTERS NX_DISABLE_IPV6 NX_MAX_MULTICAST_GROUPS=1 NX_MAX_LISTEN_REQUESTS=4 NX_FTP_MAX_CLIENTS=1 TX_DISABLE_PREEMPTION_THRESHOLD TX_DISABLE_NOTIFY_CALLBACKS TRUCK_TYPE_DEFINITION=TRUCK_TYPE_PC MODULE_TYPE_DEFINITION=MODULE_TYPE_SMALL_DV GEN_TYPE_DEFINITION=GEN_TYPE_GEN2 >> commands.tmp
echo options C_EDG_FLAGS -w --c --ghs --define_macro=restrict= >> commands.tmp
echo options C_EXECUTE_CMD $(VECTORCAST_DIR)/DATA/green_hills/multi_execution_flash.bat multi -nosplash C:\\ghs\\comp_201654\\target\\ppc\\mpc5777c\\mpserv_standard.mbs ghprobe GEN2 >> commands.tmp
echo options C_LINKER_VERSION_CMD elxr -V >> commands.tmp
echo options C_LINK_CMD ccppc -G -bsp generic -cpu=ppc5777c -bigendian -Mx -Mu -vle -fhard -floatsingle -fprecise -e 0x0 >> commands.tmp
echo options C_LINK_OPTIONS $(VECTORCAST_DIR)\\DATA\\green_hills\\standalone_romdebug_vcm.ld >> commands.tmp
echo options C_OUTPUT_FLAG -o^^ >> commands.tmp
echo options C_PREPROCESS_CMD ccppc -E -C -bsp generic -cpu=ppc5777c -bigendian -vle -fhard -floatsingle -fprecise >> commands.tmp
echo options C_PREPROCESS_FILE  >> commands.tmp
echo options EXECUTABLE_EXTENSION  >> commands.tmp
echo options RANGE_CHECK FULL >> commands.tmp
echo options SBF_LOC_MEMBER_IN_NSP DECL_NAMESPACE >> commands.tmp
echo options SBF_LOC_MEMBER_OUTSIDE_NSP DECL_NAMESPACE >> commands.tmp
echo options SBF_LOC_NONMEMBER_IN_NSP DECL_NAMESPACE >> commands.tmp
echo options SBF_LOC_NONMEMBER_OUTSIDE_NSP DECL_NAMESPACE >> commands.tmp
echo options SUBSTITUTE_CODE_FOR_C_FILE FALSE >> commands.tmp
echo options VCAST_ASSEMBLY_FILE_EXTENSIONS asm s >> commands.tmp
echo options VCAST_AUTO_CLEAR_TEST_USER_CODE FALSE >> commands.tmp
echo options VCAST_BUFFER_OUTPUT TRUE >> commands.tmp
echo options VCAST_COLLAPSE_STD_HEADERS COLLAPSE_NON_SEARCH_HEADERS >> commands.tmp
echo options VCAST_COMMAND_LINE_DEBUGGER FALSE >> commands.tmp
echo options VCAST_COMPILER_SUPPORTS_CPP_CASTS TRUE >> commands.tmp
echo options VCAST_COVERAGE_FOR_AGGREGATE_INIT FALSE >> commands.tmp
echo options VCAST_DISABLE_CPP_EXCEPTIONS TRUE >> commands.tmp
echo options VCAST_DISPLAY_UNINST_EXPR FALSE >> commands.tmp
echo options VCAST_DUMP_BUFFER TRUE >> commands.tmp
echo options VCAST_ENABLE_FUNCTION_CALL_COVERAGE FALSE >> commands.tmp
echo options VCAST_ENVIRONMENT_FILES C:\\ghs\\comp_201654\\target\\ppc\\mpc5777c\\standalone_romdebug.ld,C:\\ghs\\comp_201654\\target\\ppc\\mpc5777c\\memory.ld,C:\\ghs\\comp_201654\\target\\ppc\\mpc5777c\\standalone_config.ld >> commands.tmp
echo options VCAST_EXECUTE_WITH_STDOUT TRUE >> commands.tmp
echo options VCAST_FORCE_ELAB_TYPE_SPEC TRUE >> commands.tmp
echo options VCAST_HAS_LONGLONG TRUE >> commands.tmp
echo options VCAST_INST_FILE_MAX_LINES 0 >> commands.tmp
echo options VCAST_MAX_STRING_LENGTH 1000 >> commands.tmp
echo options VCAST_MAX_TARGET_FILES 50 >> commands.tmp
echo options VCAST_MICROSOFT_LONG_LONG FALSE >> commands.tmp
echo options VCAST_NO_SETJMP FALSE >> commands.tmp
echo options VCAST_NO_SIGNAL FALSE >> commands.tmp
echo options VCAST_NO_STDIN TRUE >> commands.tmp
echo options VCAST_NO_STDLIB FALSE >> commands.tmp
echo options VCAST_OUTPUT_BUFFER_SIZE 8000 >> commands.tmp
echo options VCAST_REMOVE_PREPROCESSOR_COMMENTS FALSE >> commands.tmp
echo options VCAST_RPTS_DEFAULT_FONT_FACE Arial(6) >> commands.tmp
echo options VCAST_STDIO FALSE >> commands.tmp
echo options VCAST_TORNADO_CONSTRUCTOR_CALL_FILE FALSE >> commands.tmp
echo options VCAST_USE_STD_STRING TRUE >> commands.tmp
echo options WHITEBOX YES >> commands.tmp
echo clear_default_source_dirs  >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\CommonSource\go_driver\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\CanStackSrc\CANopen\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\CanStackSrc\CRT\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\CanStackSrc\DLLfl32c\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\CanStackSrc\LSS\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\CanStackSrc\DLLmul\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\tgt\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\CANopen\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\drivers\fram\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\general\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\hal\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\nvstorage\fram\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\utilities\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\drivers\io\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\drivers\etpu2\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\drivers\aecp\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\drivers\comm\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\drivers\can\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\drivers\xcp\cfg\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\drivers\xcp\usr\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\parameters\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\BusHeaders\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\sharedutils\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\drivers\xcp\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\interface\codrv\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\interface\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\diagnostics\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\nvstorage\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\CanStackSrc\Include\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\drivers\gyro\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen_API\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\CommonSource\switch_board\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\drivers\file_sys\api\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\ftp\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\threadx_pxr40\ppc55xx_vle\green\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\drivers\becp\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\drivers\accel\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\SIMu1\VehicleFBSIMbuild_ert_rtw\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\SIMu1\TractionSIM\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\SIMu1\SteeringSIMbuild_ert_rtw\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\SIMu1\hydraulicSIM\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\SIMu1\EnergySourceSIM\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\SIMu1\CalibrationSIMbuild_ert_rtw\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\SIMu1\commonSIMbuild_ert_rtw\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\SIMu1\commonSIM\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\SIMu1\CalibrationSIM\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\SIMu1\EnergySourceSIMbuild_ert_rtw\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\SIMu1\hydraulicSIMbuild_ert_rtw\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\SIMu1\SteeringSIM\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\SIMu1\TractionSIMbuild_ert_rtw\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\SIMu1\HmsSim_VcmInputsBuild_ert_rtw\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\SIMu1\HmsSim_HcmTcmBuild_ert_rtw\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\SIMu1\Vehicle_fb_processing_SIM\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\SIMu1\HmsSim_TractionBuild_ert_rtw\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\SIMu1\HmsSim_EdBuild_ert_rtw\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\SIMu1\HmsSim_HydraulicBuild_ert_rtw\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\SIMu1\HmsSim_PowerSuppliesBuild_ert_rtw\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\SIMu1\HmsSim_SteeringBuild_ert_rtw\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\SIMu1\HmsSim_CommunicationBuild_ert_rtw\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\SIMu1\HmsSim_Traction\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\SIMu1\HmsSim_Communication\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\SIMu1\HmsSim_Steering\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\SIMu1\HmsSim_PowerSupplies\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\SIMu1\HmsSim_VcmInputs\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\SIMu1\HmsSim_HcmTcm\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\SIMu1\HmsSim_GpdBuild_ert_rtw\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\SIMu1\HmsSim_Ed\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\SIMu1\HmsSim_Brake\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\SIMu1\HmsSim_BrakeBuild_ert_rtw\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\SIMu1\HmsSim_Hydraulic\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\SIMu1\HmsSim_Gpd\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\Autogen\parameters\ >> commands.tmp
echo options TYPE_HANDLED_SOURCE_DIR $(CROWN_SOURCE_BASE)\VCM_APP\src\drivers\flash\ >> commands.tmp
echo environment build TRACTION_INTEGRATION.env >> commands.tmp
echo /E:TRACTION_INTEGRATION tools script run TRACTION_INTEGRATION.tst >> commands.tmp
echo /E:TRACTION_INTEGRATION execute batch >> commands.tmp
echo /E:TRACTION_INTEGRATION reports custom management TRACTION_INTEGRATION_management_report.html >> commands.tmp
"%VECTORCAST_DIR%\CLICAST"  /L:C tools execute commands.tmp false
