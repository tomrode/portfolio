/*
 * File: Traffic_Light_Toms_Example2.c
 *
 * Code generated for Simulink model 'Traffic_Light_Toms_Example2'.
 *
 * Model version                  : 1.17
 * Simulink Coder version         : 8.4 (R2013a) 13-Feb-2013
 * TLC version                    : 8.4 (Jan 19 2013)
 * C/C++ source code generated on : Fri Jan 10 12:49:51 2014
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: 32-bit Generic
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include  "Model\h\Traffic_Light_Toms_Example2.h"
#include  "Model\h\Traffic_Light_Toms_Example2_private.h"

/* Named constants for Chart: '<Root>/Cross_Walk_Indication' */
#define Traffic_Li_IN_DO_NOT_WALK_GREEN ((uint8_T)3U)
#define Traffic_Ligh_IN_NO_ACTIVE_CHILD ((uint8_T)0U)
#define Traffic_Light_To_IN_CAUTION_OFF ((uint8_T)1U)
#define Traffic_Light_Tom_IN_CAUTION_ON ((uint8_T)2U)
#define Traffic_Light_Toms_E_IN_DEFAULT ((uint8_T)2U)
#define Traffic_Light_Toms_Exa_IN_COUNT ((uint8_T)1U)
#define Traffic_Light_Toms__IN_WALK_RED ((uint8_T)5U)
#define Traffic__IN_WALK_CAUTION_YELLOW ((uint8_T)4U)

/* Named constants for Chart: '<Root>/Traffic_Light' */
#define Traffic_Light_Toms_E_IN_Caution ((uint8_T)1U)
#define Traffic_Light_Toms_Exam_IN_Stop ((uint8_T)3U)
#define Traffic_Light_Toms_Examp_IN_OFF ((uint8_T)1U)
#define Traffic_Light_Toms_Exampl_IN_Go ((uint8_T)2U)
#define Traffic_Light_Toms_Exampl_IN_ON ((uint8_T)2U)

/* Block states (auto storage) */
DW_Traffic_Light_Toms_Example_T Traffic_Light_Toms_Example2_DW;

/* Previous zero-crossings (trigger) states */
PrevZCX_Traffic_Light_Toms_Ex_T Traffic_Light_Toms_Exam_PrevZCX;

/* External inputs (root inport signals with auto storage) */
ExtU_Traffic_Light_Toms_Examp_T Traffic_Light_Toms_Example2_U;

/* External outputs (root outports fed by signals with auto storage) */
ExtY_Traffic_Light_Toms_Examp_T Traffic_Light_Toms_Example2_Y;

/* Real-time model */
RT_MODEL_Traffic_Light_Toms_E_T Traffic_Light_Toms_Example2_M_;
RT_MODEL_Traffic_Light_Toms_E_T *const Traffic_Light_Toms_Example2_M =
  &Traffic_Light_Toms_Example2_M_;

/* Model step function */
void Traffic_Light_Toms_Example2_step(void)
{
  boolean_T guard = FALSE;
  ZCEventType zcEvent;

  /* Chart: '<Root>/Traffic_Light' incorporates:
   *  TriggerPort: '<S2>/Tick'
   */
  /* Inport: '<Root>/ClockIn' */
  zcEvent = rt_ZCFcn(RISING_ZERO_CROSSING,
                     &Traffic_Light_Toms_Exam_PrevZCX.Traffic_Light_Trig_ZCE,
                     (Traffic_Light_Toms_Example2_U.ClockIn));
  if (zcEvent != NO_ZCEVENT) {
    Traffic_Light_Toms_Example2_DW.presentTicks =
      Traffic_Light_Toms_Example2_M->Timing.clockTick0;
    Traffic_Light_Toms_Example2_DW.elapsedTicks =
      Traffic_Light_Toms_Example2_DW.presentTicks -
      Traffic_Light_Toms_Example2_DW.previousTicks;
    Traffic_Light_Toms_Example2_DW.previousTicks =
      Traffic_Light_Toms_Example2_DW.presentTicks;
    if (Traffic_Light_Toms_Example2_DW.temporalCounter_i1 +
        Traffic_Light_Toms_Example2_DW.elapsedTicks <= 7U) {
      Traffic_Light_Toms_Example2_DW.temporalCounter_i1 +=
        Traffic_Light_Toms_Example2_DW.elapsedTicks;
    } else {
      Traffic_Light_Toms_Example2_DW.temporalCounter_i1++; //= 7U; Not working
    }

    /* Gateway: Traffic_Light */
    /* Event: '<S2>:20' */
    /* During: Traffic_Light */
    if (Traffic_Light_Toms_Example2_DW.is_active_c1_Traffic_Light_Toms == 0U) {
      /* Entry: Traffic_Light */
      Traffic_Light_Toms_Example2_DW.is_active_c1_Traffic_Light_Toms = 1U;

      /* Entry Internal: Traffic_Light */
      /* Transition: '<S2>:27' */
      Traffic_Light_Toms_Example2_DW.is_c1_Traffic_Light_Toms_Exampl =
        Traffic_Light_Toms_Examp_IN_OFF;

      /* Outport: '<Root>/redOut' */
      /* Entry 'OFF': '<S2>:22' */
      Traffic_Light_Toms_Example2_Y.redOut = TRUE;

      /* Outport: '<Root>/yellowOut' */
      Traffic_Light_Toms_Example2_Y.yellowOut = TRUE;

      /* Outport: '<Root>/greenOut' */
      Traffic_Light_Toms_Example2_Y.greenOut = TRUE;
    } else if (Traffic_Light_Toms_Example2_DW.is_c1_Traffic_Light_Toms_Exampl ==
               Traffic_Light_Toms_Examp_IN_OFF) {
      /* Inport: '<Root>/EnableIn' */
      /* During 'OFF': '<S2>:22' */
      if (Traffic_Light_Toms_Example2_U.EnableIn == 1) {
        /* Transition: '<S2>:31' */
        Traffic_Light_Toms_Example2_DW.is_c1_Traffic_Light_Toms_Exampl =
          Traffic_Light_Toms_Exampl_IN_ON;

        /* Entry Internal 'ON': '<S2>:24' */
        if (Traffic_Light_Toms_Example2_U.EnableIn == 1) {
          /* Transition: '<S2>:18' */
          Traffic_Light_Toms_Example2_DW.is_ON = Traffic_Light_Toms_Exam_IN_Stop;
          Traffic_Light_Toms_Example2_DW.temporalCounter_i1 = 0U;

          /* Outport: '<Root>/redOut' */
          /* Entry 'Stop': '<S2>:4' */
          Traffic_Light_Toms_Example2_Y.redOut = TRUE;
        }
      }
    } else {
      /* During 'ON': '<S2>:24' */
      switch (Traffic_Light_Toms_Example2_DW.is_ON) {
       case Traffic_Light_Toms_E_IN_Caution:
        /* During 'Caution': '<S2>:6' */
        if (Traffic_Light_Toms_Example2_DW.temporalCounter_i1 >= 1U) {
          /* Outport: '<Root>/yellowOut' */
          /* Transition: '<S2>:11' */
          /* Exit 'Caution': '<S2>:6' */
          Traffic_Light_Toms_Example2_Y.yellowOut = FALSE;
          Traffic_Light_Toms_Example2_DW.is_ON = Traffic_Light_Toms_Exam_IN_Stop;
          Traffic_Light_Toms_Example2_DW.temporalCounter_i1 = 0U;

          /* Outport: '<Root>/redOut' */
          /* Entry 'Stop': '<S2>:4' */
          Traffic_Light_Toms_Example2_Y.redOut = TRUE;
        }
        break;

       case Traffic_Light_Toms_Exampl_IN_Go:
        /* During 'Go': '<S2>:5' */
        if (Traffic_Light_Toms_Example2_DW.temporalCounter_i1 >= 5U) {
          /* Outport: '<Root>/greenOut' */
          /* Transition: '<S2>:26' */
          /* Exit 'Go': '<S2>:5' */
          Traffic_Light_Toms_Example2_Y.greenOut = FALSE;
          Traffic_Light_Toms_Example2_DW.is_ON = Traffic_Light_Toms_E_IN_Caution;
          Traffic_Light_Toms_Example2_DW.temporalCounter_i1 = 0U;

          /* Outport: '<Root>/yellowOut' */
          /* Entry 'Caution': '<S2>:6' */
          Traffic_Light_Toms_Example2_Y.yellowOut = TRUE;
        }
        break;

       default:
        /* During 'Stop': '<S2>:4' */
        if (Traffic_Light_Toms_Example2_DW.temporalCounter_i1 >= 5U) {
          /* Outport: '<Root>/redOut' */
          /* Transition: '<S2>:25' */
          /* Exit 'Stop': '<S2>:4' */
          Traffic_Light_Toms_Example2_Y.redOut = FALSE;
          Traffic_Light_Toms_Example2_DW.is_ON = Traffic_Light_Toms_Exampl_IN_Go;
          Traffic_Light_Toms_Example2_DW.temporalCounter_i1 = 0U;

          /* Outport: '<Root>/greenOut' */
          /* Entry 'Go': '<S2>:5' */
          Traffic_Light_Toms_Example2_Y.greenOut = TRUE;
        }
        break;
      }
    }
  }

  /* Chart: '<Root>/Cross_Walk_Indication' incorporates:
   *  Inport: '<Root>/EnableIn'
   */
  Traffic_Light_Toms_Example2_DW.presentTicks_c =
    Traffic_Light_Toms_Example2_M->Timing.clockTick0;
  Traffic_Light_Toms_Example2_DW.elapsedTicks_h =
    Traffic_Light_Toms_Example2_DW.presentTicks_c -
    Traffic_Light_Toms_Example2_DW.previousTicks_m;
  Traffic_Light_Toms_Example2_DW.previousTicks_m =
    Traffic_Light_Toms_Example2_DW.presentTicks_c;
  if (Traffic_Light_Toms_Example2_DW.temporalCounter_i1_c +
      Traffic_Light_Toms_Example2_DW.elapsedTicks_h <= 3U) {
    Traffic_Light_Toms_Example2_DW.temporalCounter_i1_c +=
      Traffic_Light_Toms_Example2_DW.elapsedTicks_h;
  } else {
    Traffic_Light_Toms_Example2_DW.temporalCounter_i1_c = 3U;
  }

  /* Gateway: Cross_Walk_Indication */
  /* During: Cross_Walk_Indication */
  if (Traffic_Light_Toms_Example2_DW.is_active_c2_Traffic_Light_Toms == 0U) {
    /* Entry: Cross_Walk_Indication */
    Traffic_Light_Toms_Example2_DW.is_active_c2_Traffic_Light_Toms = 1U;

    /* Entry Internal: Cross_Walk_Indication */
    /* Transition: '<S1>:60' */
    Traffic_Light_Toms_Example2_DW.is_c2_Traffic_Light_Toms_Exampl =
      Traffic_Light_Toms_E_IN_DEFAULT;
  } else {
    switch (Traffic_Light_Toms_Example2_DW.is_c2_Traffic_Light_Toms_Exampl) {
     case Traffic_Light_Toms_Exa_IN_COUNT:
      /* During 'COUNT': '<S1>:36' */
      /* Transition: '<S1>:40' */
      /* Exit Internal 'COUNT': '<S1>:36' */
      Traffic_Light_Toms_Example2_DW.is_active_cnt2 = 0U;
      Traffic_Light_Toms_Example2_DW.is_active_cnt1 = 0U;

      /* Exit 'COUNT': '<S1>:36' */
      Traffic_Light_Toms_Example2_DW.is_c2_Traffic_Light_Toms_Exampl =
        Traffic_Light_Toms__IN_WALK_RED;

      /* Outport: '<Root>/WalkCautionLedOut' */
      /* Entry 'WALK_RED': '<S1>:1' */
      Traffic_Light_Toms_Example2_Y.WalkCautionLedOut = FALSE;

      /* Outport: '<Root>/WalkLedOut' */
      Traffic_Light_Toms_Example2_Y.WalkLedOut = TRUE;
      break;

     case Traffic_Light_Toms_E_IN_DEFAULT:
      /* During 'DEFAULT': '<S1>:59' */
      if (Traffic_Light_Toms_Example2_U.EnableIn == 1) {
        /* Transition: '<S1>:61' */
        Traffic_Light_Toms_Example2_DW.is_c2_Traffic_Light_Toms_Exampl =
          Traffic_Light_Toms__IN_WALK_RED;

        /* Outport: '<Root>/WalkCautionLedOut' */
        /* Entry 'WALK_RED': '<S1>:1' */
        Traffic_Light_Toms_Example2_Y.WalkCautionLedOut = FALSE;

        /* Outport: '<Root>/WalkLedOut' */
        Traffic_Light_Toms_Example2_Y.WalkLedOut = TRUE;
      }
      break;

     case Traffic_Li_IN_DO_NOT_WALK_GREEN:
      /* During 'DO_NOT_WALK_GREEN': '<S1>:3' */
      if (Traffic_Light_Toms_Example2_Y.yellowOut == 1) {
        /* Transition: '<S1>:5' */
        Traffic_Light_Toms_Example2_DW.is_c2_Traffic_Light_Toms_Exampl =
          Traffic__IN_WALK_CAUTION_YELLOW;

        /* Outport: '<Root>/DoNotWalkLedOut' */
        /* Entry 'WALK_CAUTION_YELLOW': '<S1>:2' */
        Traffic_Light_Toms_Example2_Y.DoNotWalkLedOut = FALSE;

        /* Entry Internal 'WALK_CAUTION_YELLOW': '<S1>:2' */
        /* Transition: '<S1>:32' */
        Traffic_Light_Toms_Example2_DW.is_WALK_CAUTION_YELLOW =
          Traffic_Light_Tom_IN_CAUTION_ON;
        Traffic_Light_Toms_Example2_DW.temporalCounter_i1_c = 0U;

        /* Outport: '<Root>/WalkCautionLedOut' */
        /* Entry 'CAUTION_ON': '<S1>:28' */
        Traffic_Light_Toms_Example2_Y.WalkCautionLedOut = TRUE;
      }
      break;

     case Traffic__IN_WALK_CAUTION_YELLOW:
      /* During 'WALK_CAUTION_YELLOW': '<S1>:2' */
      if (Traffic_Light_Toms_Example2_Y.redOut == 1) {
        /* Transition: '<S1>:6' */
        if ((Traffic_Light_Toms_Example2_DW.cnt1 <= 3.0) &&
            (Traffic_Light_Toms_Example2_DW.cnt2 <= 3.0)) {
          /* Transition: '<S1>:55' */
          /* Exit Internal 'WALK_CAUTION_YELLOW': '<S1>:2' */
          Traffic_Light_Toms_Example2_DW.is_WALK_CAUTION_YELLOW =
            Traffic_Ligh_IN_NO_ACTIVE_CHILD;
          Traffic_Light_Toms_Example2_DW.is_c2_Traffic_Light_Toms_Exampl =
            Traffic_Light_Toms_Exa_IN_COUNT;

          /* Entry 'COUNT': '<S1>:36' */
          /* Entry Internal 'COUNT': '<S1>:36' */
          Traffic_Light_Toms_Example2_DW.is_active_cnt1 = 1U;

          /* Entry 'cnt1': '<S1>:37' */
          Traffic_Light_Toms_Example2_DW.cnt1++;
          Traffic_Light_Toms_Example2_DW.is_active_cnt2 = 1U;

          /* Entry 'cnt2': '<S1>:38' */
          Traffic_Light_Toms_Example2_DW.cnt2++;
        } else if ((Traffic_Light_Toms_Example2_DW.cnt1 > 3.0) &&
                   (Traffic_Light_Toms_Example2_DW.cnt2 > 3.0)) {
          /* Transition: '<S1>:56' */
          /* Exit Internal 'WALK_CAUTION_YELLOW': '<S1>:2' */
          Traffic_Light_Toms_Example2_DW.is_WALK_CAUTION_YELLOW =
            Traffic_Ligh_IN_NO_ACTIVE_CHILD;
          Traffic_Light_Toms_Example2_DW.is_c2_Traffic_Light_Toms_Exampl =
            Traffic_Light_Toms__IN_WALK_RED;

          /* Outport: '<Root>/WalkCautionLedOut' */
          /* Entry 'WALK_RED': '<S1>:1' */
          Traffic_Light_Toms_Example2_Y.WalkCautionLedOut = FALSE;

          /* Outport: '<Root>/WalkLedOut' */
          Traffic_Light_Toms_Example2_Y.WalkLedOut = TRUE;
        } else {
          guard = TRUE;
        }
      } else {
        guard = TRUE;
      }
      break;

     default:
      /* During 'WALK_RED': '<S1>:1' */
      if (Traffic_Light_Toms_Example2_Y.greenOut == 1) {
        /* Transition: '<S1>:7' */
        Traffic_Light_Toms_Example2_DW.is_c2_Traffic_Light_Toms_Exampl =
          Traffic_Li_IN_DO_NOT_WALK_GREEN;

        /* Outport: '<Root>/WalkLedOut' */
        /* Entry 'DO_NOT_WALK_GREEN': '<S1>:3' */
        Traffic_Light_Toms_Example2_Y.WalkLedOut = FALSE;

        /* Outport: '<Root>/DoNotWalkLedOut' */
        Traffic_Light_Toms_Example2_Y.DoNotWalkLedOut = TRUE;
      }
      break;
    }

    if (guard) {
      if (Traffic_Light_Toms_Example2_DW.is_WALK_CAUTION_YELLOW ==
          Traffic_Light_To_IN_CAUTION_OFF) {
        /* During 'CAUTION_OFF': '<S1>:29' */
        if (Traffic_Light_Toms_Example2_DW.temporalCounter_i1_c >= 1U) {
          /* Transition: '<S1>:31' */
          Traffic_Light_Toms_Example2_DW.is_WALK_CAUTION_YELLOW =
            Traffic_Light_Tom_IN_CAUTION_ON;
          Traffic_Light_Toms_Example2_DW.temporalCounter_i1_c = 0U;

          /* Outport: '<Root>/WalkCautionLedOut' */
          /* Entry 'CAUTION_ON': '<S1>:28' */
          Traffic_Light_Toms_Example2_Y.WalkCautionLedOut = TRUE;
        }
      } else {
        /* During 'CAUTION_ON': '<S1>:28' */
        if (Traffic_Light_Toms_Example2_DW.temporalCounter_i1_c >= 1U) {
          /* Transition: '<S1>:30' */
          Traffic_Light_Toms_Example2_DW.is_WALK_CAUTION_YELLOW =
            Traffic_Light_To_IN_CAUTION_OFF;
          Traffic_Light_Toms_Example2_DW.temporalCounter_i1_c = 0U;

          /* Outport: '<Root>/WalkCautionLedOut' */
          /* Entry 'CAUTION_OFF': '<S1>:29' */
          Traffic_Light_Toms_Example2_Y.WalkCautionLedOut = FALSE;
        }
      }
    }
  }

  /* End of Chart: '<Root>/Cross_Walk_Indication' */

  /* Update absolute time for base rate */
  /* The "clockTick0" counts the number of times the code of this task has
   * been executed. The resolution of this integer timer is 2.0, which is the step size
   * of the task. Size of "clockTick0" ensures timer will not overflow during the
   * application lifespan selected.
   */
  Traffic_Light_Toms_Example2_M->Timing.clockTick0++;
}

/* Model initialize function */
void Traffic_Light_Toms_Example2_initialize(void)
{
  /* Registration code */

  /* initialize real-time model */
  (void) memset((void *)Traffic_Light_Toms_Example2_M, 0,
                sizeof(RT_MODEL_Traffic_Light_Toms_E_T));

  /* states (dwork) */
  (void) memset((void *)&Traffic_Light_Toms_Example2_DW, 0,
                sizeof(DW_Traffic_Light_Toms_Example_T));

  /* external inputs */
  (void) memset((void *)&Traffic_Light_Toms_Example2_U, 0,
                sizeof(ExtU_Traffic_Light_Toms_Examp_T));

  /* external outputs */
  (void) memset((void *)&Traffic_Light_Toms_Example2_Y, 0,
                sizeof(ExtY_Traffic_Light_Toms_Examp_T));
  Traffic_Light_Toms_Exam_PrevZCX.Traffic_Light_Trig_ZCE = UNINITIALIZED_ZCSIG;

  /* InitializeConditions for Chart: '<Root>/Traffic_Light' */
  Traffic_Light_Toms_Example2_DW.is_ON = Traffic_Ligh_IN_NO_ACTIVE_CHILD;
  Traffic_Light_Toms_Example2_DW.temporalCounter_i1 = 0U;
  Traffic_Light_Toms_Example2_DW.is_active_c1_Traffic_Light_Toms = 0U;
  Traffic_Light_Toms_Example2_DW.is_c1_Traffic_Light_Toms_Exampl =
    Traffic_Ligh_IN_NO_ACTIVE_CHILD;

  /* InitializeConditions for Outport: '<Root>/redOut' incorporates:
   *  InitializeConditions for Chart: '<Root>/Traffic_Light'
   */
  Traffic_Light_Toms_Example2_Y.redOut = FALSE;

  /* InitializeConditions for Outport: '<Root>/greenOut' incorporates:
   *  InitializeConditions for Chart: '<Root>/Traffic_Light'
   */
  Traffic_Light_Toms_Example2_Y.greenOut = FALSE;

  /* InitializeConditions for Outport: '<Root>/yellowOut' incorporates:
   *  InitializeConditions for Chart: '<Root>/Traffic_Light'
   */
  Traffic_Light_Toms_Example2_Y.yellowOut = FALSE;

  /* InitializeConditions for Chart: '<Root>/Traffic_Light' */
  Traffic_Light_Toms_Example2_DW.presentTicks = 0U;
  Traffic_Light_Toms_Example2_DW.elapsedTicks = 0U;
  Traffic_Light_Toms_Example2_DW.previousTicks = 0U;

  /* InitializeConditions for Chart: '<Root>/Cross_Walk_Indication' */
  Traffic_Light_Toms_Example2_DW.is_active_cnt1 = 0U;
  Traffic_Light_Toms_Example2_DW.is_active_cnt2 = 0U;
  Traffic_Light_Toms_Example2_DW.is_WALK_CAUTION_YELLOW =
    Traffic_Ligh_IN_NO_ACTIVE_CHILD;
  Traffic_Light_Toms_Example2_DW.temporalCounter_i1_c = 0U;
  Traffic_Light_Toms_Example2_DW.is_active_c2_Traffic_Light_Toms = 0U;
  Traffic_Light_Toms_Example2_DW.is_c2_Traffic_Light_Toms_Exampl =
    Traffic_Ligh_IN_NO_ACTIVE_CHILD;
  Traffic_Light_Toms_Example2_DW.cnt2 = 0.0;
  Traffic_Light_Toms_Example2_DW.cnt1 = 0.0;

  /* InitializeConditions for Outport: '<Root>/WalkLedOut' incorporates:
   *  InitializeConditions for Chart: '<Root>/Cross_Walk_Indication'
   */
  Traffic_Light_Toms_Example2_Y.WalkLedOut = FALSE;

  /* InitializeConditions for Outport: '<Root>/DoNotWalkLedOut' incorporates:
   *  InitializeConditions for Chart: '<Root>/Cross_Walk_Indication'
   */
  Traffic_Light_Toms_Example2_Y.DoNotWalkLedOut = FALSE;

  /* InitializeConditions for Outport: '<Root>/WalkCautionLedOut' incorporates:
   *  InitializeConditions for Chart: '<Root>/Cross_Walk_Indication'
   */
  Traffic_Light_Toms_Example2_Y.WalkCautionLedOut = FALSE;

  /* InitializeConditions for Chart: '<Root>/Cross_Walk_Indication' */
  Traffic_Light_Toms_Example2_DW.presentTicks_c = 0U;
  Traffic_Light_Toms_Example2_DW.elapsedTicks_h = 0U;
  Traffic_Light_Toms_Example2_DW.previousTicks_m = 0U;

  /* Enable for Chart: '<Root>/Traffic_Light' */
  Traffic_Light_Toms_Example2_DW.presentTicks =
    Traffic_Light_Toms_Example2_M->Timing.clockTick0;
  Traffic_Light_Toms_Example2_DW.previousTicks =
    Traffic_Light_Toms_Example2_DW.presentTicks;

  /* Enable for Chart: '<Root>/Cross_Walk_Indication' */
  Traffic_Light_Toms_Example2_DW.presentTicks_c =
    Traffic_Light_Toms_Example2_M->Timing.clockTick0;
  Traffic_Light_Toms_Example2_DW.previousTicks_m =
    Traffic_Light_Toms_Example2_DW.presentTicks_c;
}

/* Model terminate function */
void Traffic_Light_Toms_Example2_terminate(void)
{
  /* (no terminate code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
