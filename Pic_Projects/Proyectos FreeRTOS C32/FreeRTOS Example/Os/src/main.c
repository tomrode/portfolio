#include <p32xxxx.h>
/** CONFIGURATION **************************************************/
// Cristal de Kit Ethernet PIC32 8MHz.-
// FPLLDIV 1:2, PLLMULT x20, PLLODIV 1:1. -> 80MHz
// FPBDIV 1:2 -> 40MHz
#pragma config FPLLODIV=DIV_1, FPLLIDIV=DIV_2, FPLLMUL=MUL_20, FPBDIV=DIV_2
#pragma config FWDTEN=OFF, FCKSM=CSDCMD, POSCMOD=XT, FNOSC=PRIPLL
#pragma config CP=OFF, BWP=OFF
/** END CONFIGURATION **************************************************/
#include <plib.h>
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

#include "Model\h\Traffic_Light_Toms_Example2.h"
#include "Model\h\rt_zcfcn.h"
//#include "Model\h\rt_zcfcn.h"
#define QUEUE_TASKS
#define PRIORITY_TASK0		( tskIDLE_PRIORITY + 1 )
#define PRIORITY_TASK1		( tskIDLE_PRIORITY + 2 )
/*************************** Tasks Prototypes ****************************/
static void vTASK0( void *pvParameters );
static void vTASK1( void *pvParameters );
static void vTASK2( void *pvParameters );
#ifdef QUEUE_TASKS
static void vTASK3( void *pvParameters );
#endif
/*-----------------------------------------------------------*/
/**************************** Example Queue*********************************/
struct AMessage
{
	portCHAR ucMessageID;
	portCHAR ucData[ 20 ];
};
/*****************************************************************************/

int main( void )
{
	
	SYSTEMConfigPerformance( configCPU_CLOCK_HZ - 1 );
	mOSCSetPBDIV( OSC_PB_DIV_2 );
	/* Setup to use the external interrupt controller. */
    INTEnableSystemMultiVectoredInt();
	portDISABLE_INTERRUPTS();
	
	TRISDbits.TRISD0=0;
	TRISDbits.TRISD1=0;
    TRISDbits.TRISD2=0;

	vPortInitialiseBlocks();

	xTaskCreate( vTASK0, ( const char * const ) "T0", configMINIMAL_STACK_SIZE, NULL, PRIORITY_TASK0, NULL );
	xTaskCreate( vTASK1, ( const char * const ) "T1", configMINIMAL_STACK_SIZE, NULL, PRIORITY_TASK1, NULL );
    xTaskCreate( vTASK2, ( const char * const ) "T2", configMINIMAL_STACK_SIZE, NULL, PRIORITY_TASK0, NULL );  
#ifdef QUEUE_TASKS  
    xTaskCreate( vTASK3, ( const char * const ) "T3", configMINIMAL_STACK_SIZE, NULL, PRIORITY_TASK1, NULL );
#endif	
	vTaskStartScheduler();
}
/*********************************************************************************************
 *********************************************************************************************
 ********               BEGIN OF TASK SECTION HERE                                 ***********
 *********************************************************************************************
 *********************************************************************************************/

/*************************************************************
* TASK0()
* update Model and output the lights on LED 0-2, PORTD0-D2
**************************************************************/
static void vTASK0( void *pvParameters )
{
Traffic_Light_Toms_Example2_U.EnableIn = TRUE;
	while(1)
	{
	//	LATDbits.LATD0=!PORTDbits.RD0;
		vTaskDelay(10/portTICK_RATE_MS);
        Traffic_Light_Toms_Example2_step();    /* Goto the model*/
            if(Traffic_Light_Toms_Example2_Y.redOut == TRUE)
            {
              PORTSetBits(IOPORT_D,BIT_0); 
              PORTClearBits(IOPORT_D,BIT_1 | BIT_2); 
            }
            else if (Traffic_Light_Toms_Example2_Y.yellowOut == TRUE)
            {
              PORTSetBits(IOPORT_D,BIT_1); 
              PORTClearBits(IOPORT_D,BIT_0 | BIT_2); 
            }
             else if (Traffic_Light_Toms_Example2_Y.greenOut == TRUE)
            {
              PORTSetBits(IOPORT_D,BIT_2); 
              PORTClearBits(IOPORT_D,BIT_0 | BIT_1); 
            }
            else
             {} 
	}
}
/*************************************************************
* vTASK1()
* Send a clock frequency to the model at 1 Hz 
*************************************************************/
static void vTASK1( void *pvParameters )
{
static ExtU_Traffic_Light_Toms_Examp_T ModelClock;

	while(1)
	{
		//LATDbits.LATD1=!PORTDbits.RD1;
		vTaskDelay(500/portTICK_RATE_MS);
        ModelClock.ClockIn ^= 0x01;
        Traffic_Light_Toms_Example2_U.ClockIn = ModelClock.ClockIn; 
 
	}
}

/*************************************************************
*  vTASK2()
*  Adding a queue Send a message 
*************************************************************/

xQueueHandle xQueue;
xQueueHandle xQueue1, xQueue2;

static void vTASK2( void *pvParameters )
{
 struct AMessage ulVar;
 struct AMessage xMessage;
 struct AMessage *pxMessage1;
 struct AMessage *pxMessage2;
 
while(1){
/* Put in some data to send*/
  ulVar.ucMessageID = 0x20;
  ulVar.ucData[0]   = 0x21;
  ulVar.ucData[1]   = 0x22;
  ulVar.ucData[2]   = 0x23;
  ulVar.ucData[3]   = 0x69;

 // Create a queue capable of containing 10 unsigned long values.
 xQueue1 = xQueueCreate( 10, sizeof( unsigned portLONG ) );
 // Create a queue capable of containing 10 pointers to AMessage structures.
 // These should be passed by pointer as they contain a lot of data.
 xQueue2 = xQueueCreate( 10, sizeof( struct AMessage * ) );

 if( xQueue1 != 0 )
 {
  // Send a pointer to a struct AMessage object. Don't block if the
  // queue is already full.
  pxMessage1 = & ulVar;//xMessage;
  xQueueSend( xQueue1, ( void * ) &pxMessage1, ( portTickType ) 0 );
 }
  pxMessage2 = & xMessage;
  /* data into pointer */
  pxMessage2 -> ucMessageID = 0xDE;
  pxMessage2 -> ucData[0] =   0xAD;
  pxMessage2 -> ucData[1] =   0xBE;
  pxMessage2 -> ucData[2] =   0xEF;
// Send an unsigned long. Wait for 10 ticks for space to become
// available if necessary.
  if( xQueue2 != 0 )
 {
  // Send a pointer to a struct AMessage object. Don't block if the
  // queue is already full.
 //pxMessage2 = & xMessage;
  xQueueSend( xQueue2, ( void * ) &pxMessage2, ( portTickType ) 0 );
 }		
 vTaskDelay(100/portTICK_RATE_MS);
 }/* End while*/

}
#ifdef QUEUE_TASKS
/*************************************************************
 * vTASK3()
 * Adding a queue to Receive a message 
 *************************************************************/
static void vTASK3( void *pvParameters )
{
 struct AMessage *pxRxedMessage1,*pxRxedMessage2;

while(1)
 {
  if( xQueue1 != 0 )
  {
    // Receive a message on the created queue. Block for 10 ticks if a
    // message is not immediately available.
   if( xQueueReceive( xQueue1, &( pxRxedMessage1 ), ( portTickType ) 10 ) )
   {
   // pcRxedMessage now points to the struct AMessage variable posted
   // by vATask.
   }
  }
if( xQueue2 != 0 )
  {
    // Receive a message on the created queue. Block for 10 ticks if a
    // message is not immediately available.
   if( xQueueReceive( xQueue2, &( pxRxedMessage2 ), ( portTickType ) 10 ) )
   {
   // pcRxedMessage now points to the struct AMessage variable posted
   // by vATask.
   }
  }
 vTaskDelay(600/portTICK_RATE_MS);  
 }/* End while*/  
}

/***********************END OF TASK SECTION HERE *********************************************/
#endif 

/*------------------------------------------------------------*/
void vApplicationStackOverflowHook( void )
{
	/* Look at pxCurrentTCB to see which task overflowed its stack. */
	for( ;; );
}
/*-----------------------------------------------------------*/
void _general_exception_handler( unsigned portLONG ulCause, unsigned portLONG ulStatus )
{
	/* This overrides the definition provided by the kernel.  Other exceptions 
	should be handled here. */
	for( ;; );
}
/*-----------------------------------------------------------*/
