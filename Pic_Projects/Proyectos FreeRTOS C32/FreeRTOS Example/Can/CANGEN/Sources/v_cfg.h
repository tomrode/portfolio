/**************************************************************
*  File D:\rtc_wa_KL15\BCM_NET_DIAG\CUSW\TOOLS\CANGEN\Sources\v_cfg.h
*  generated at Wed Dec 04 09:34:27 2013
*             Toolversion:   427
*               Bussystem:   CAN
*
*  generated out of CANdb:   D:\rtc_wa_KL15\BCM_NET_DIAG\CUSW\TOOLS\CANGEN\CCAN.dbc
*                            D:\rtc_wa_KL15\BCM_NET_DIAG\CUSW\TOOLS\CANGEN\BCAN.dbc

*            Manufacturer:   Fiat
*                for node:   BCM
*   Generation parameters:   Target system = FR60
*                            Compiler      = Fujitsu / Softune
*
* License information:       
*   -    Serialnumber:       CBD0800214
*   - Date of license:       19.9.2008
*
***************************************************************
Software is licensed for:    
Magneti Marelli Sistemi Elettronici S.p.A.
Fiat / SLP2 / MB91460P / Fujitsu Softune V60L01 / MB91F467
**************************************************************/
#ifndef VECTOR_CFG_H
#define VECTOR_CFG_H



#define VGEN_CANGEN_VERSION 0x0427
#define VGEN_CANGEN_RELEASE_VERSION 0x60
#define CANGENEXE_VERSION 0x0427
#define CANGENEXE_RELEASE_VERSION 0x60

#define C_CLIENT_FIAT


#define C_COMP_FUJITSU_FR60_CCAN
#define C_PROCESSOR_MB91F467D
#define C_ENABLE_CAN_CHANNELS
#define C_VERSION_REF_IMPLEMENTATION 0x140
#define V_ENABLE_USE_DUMMY_STATEMENT
#define VGEN_ENABLE_VSTDLIB
#define C_CPUTYPE_32BIT
#define C_CPUTYPE_BIGENDIAN
#define C_CPUTYPE_BITORDER_MSB2LSB

/**************************************
*    Version defines for Ini Files    *
***************************************/

#define VGEN_FIAT_INI_VERSION 0x0101
#define VGEN_FIAT_INI_RELEASE_VERSION 0x01

/**************************************
*           used modules              *
***************************************/

#define VGEN_DISABLE_BRS
#define VGEN_ENABLE_CAN_DRV

/* TP Modules */

#define VGEN_ENABLE_TP_ISO_MC

/* Diagnose Modules */

#define VGEN_ENABLE_DIAG_CANDESC
#define VGEN_ENABLE_CCP

#define VGEN_DISABLE_DCI
#endif
