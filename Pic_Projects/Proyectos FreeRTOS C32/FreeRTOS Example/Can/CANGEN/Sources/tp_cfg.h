/**************************************************************
*  File tp_cfg.h
*  generated at Wed Dec 04 09:34:28 2013
*             Toolversion:   427
*               Bussystem:   CAN
*
*  generated out of CANdb:   D:\rtc_wa_KL15\BCM_NET_DIAG\CUSW\TOOLS\CANGEN\CCAN.dbc
*                            D:\rtc_wa_KL15\BCM_NET_DIAG\CUSW\TOOLS\CANGEN\BCAN.dbc

*            Manufacturer:   Fiat
*                for node:   BCM
*   Generation parameters:   Target system = FR60
*                            Compiler      = Fujitsu / Softune
*
* License information:       
*   -    Serialnumber:       CBD0800214
*   - Date of license:       19.9.2008
*
***************************************************************
Software is licensed for:    
Magneti Marelli Sistemi Elettronici S.p.A.
Fiat / SLP2 / MB91460P / Fujitsu Softune V60L01 / MB91F467
**************************************************************/
#ifndef TP_CFG_H
#define TP_CFG_H


#define kTpNumberOfCanChannels 2
typedef struct tTpCopyToCanInfoStruct_s 	tTpCopyToCanInfoStruct;
typedef tTpCopyToCanInfoStruct            *TpCopyToCanInfoStructPtr;



#define TP_TYPE_MULTI_NORMAL_FIXED_ADDRESSING

#define TPC_ENABLE_DYN_TX_OBJECTS
#define TPC_ENABLE_DYN_TX_ID
#define TPC_ENABLE_DYN_TX_DLC
#define TPC_DISABLE_DYN_TX_DATAPTR
#define TPC_DISABLE_DYN_TX_PRETRANS_FCT
#define TPC_DISABLE_DYN_TX_CONF_FCT

#define TP_USE_DYN_HANDLE kTpOff
#define TP_FUNC_ENABLE_RECEPTION
#define TP_FUNC_ENABLE_NORMAL_FIXED_ADDRESSING
#define __ApplTpFuncGetBuffer DescGetFuncBuffer
#define __ApplTpFuncIndication DescFuncReqInd
vuint8 * DescGetFuncBuffer (vuint16 dataLength);
void DescFuncReqInd(vuint16 dataLength);

#define TP_ENABLE_ISO_15765_2_2
#define TP_USE_NO_STMIN_AFTER_FC kTpOn


#include "can_inc.h"


#include "can_msg.h"


#define TpTransmitDiag(dataPtr, dataLen)	TpTransmit(0, dataPtr, dataLen)
#define TpTransmitSFDiag(dataLen)	TpTransmitSF(0, dataLen)





#ifndef kTpOn

#define kTpOn  1
#define kTpOff 0
#endif

#define TP_USE_ISO_COMPLIANCE kTpOn

#define TP_USE_PADDING kTpOff

#define TP_ECU_NUMBER 0x40

#define TP_TX_ECU_NR(i) TP_ECU_NUMBER

#define TP_RX_ECU_NR(i) TP_ECU_NUMBER

#define TP_CAN_ID_HIGH 0x18da

#define TP_USE_DIAGPRECOPY kTpOff
#define TP_USE_FAST_PRECOPY kTpOn

/* Addressing mode */
#define TP_USE_NORMAL_ADDRESSING kTpOff
#define TP_USE_EXTENDED_ADDRESSING kTpOff
#define TP_USE_NORMAL_FIXED_ADDRESSING kTpOn
#define TP_DISABLE_SINGLE_CHANNEL_TP_COMMENT
#define TP_DISABLE_SINGLE_CHANNEL_TP
#define TP_DISABLE_MULTIPLE_ADDRESSING_COMMENT
#define TP_DISABLE_MULTIPLE_ADDRESSING
#define TP_DISABLE_MULTIPLE_NODES

/* assertions / runtime checks */
#define TP_ENABLE_USER_CHECK
#define TP_DISABLE_INTERNAL_CHECK
#define TP_DISABLE_GEN_CHECK
#define TP_DISABLE_RUNTIME_CHECK

/* Code variants rx part */
#define TP_USE_EXTENDED_API_BS kTpOff
#define TP_USE_EXTENDED_API_STMIN kTpOff
#define TP_USE_PRE_COPY_CHECK kTpOff
#define TP_USE_APPL_PRECOPY kTpOn
#define TP_USE_FREE_CHANNEL_SEARCH kTpOn
#define TP_USE_RX_CHANNEL_WITHOUT_FC kTpOff

/* Code variants tx part */
#define TP_USE_STMIN_OF_FC kTpOn
#define TP_USE_ONLY_FIRST_FC kTpOn
#define TP_USE_VARIABLE_DLC kTpOn
#define TP_USE_TP_TRANSMIT_QUEUE kTpOn
#define TP_USE_DYN_ID kTpOn
#define TP_USE_TX_CHANNEL_WITHOUT_FC kTpOff

/* Code variants rx and tx part */
#define TP_USE_MULTIPLE_ECU kTpOff
#define TP_USE_MULTIPLE_ECU_NR kTpOff
#define TP_USE_GATEWAY_API kTpOff
#define TP_USE_WAIT_FRAMES kTpOff
#define TP_USE_MULTIPLE_BASEADDRESS kTpOff

/* Parameters */
#define kTpTxChannelCount 2
#define kTpRxChannelCount 2




#define MEMORY_NEAR_TP_SAVE
#define TpMemCpy memcpy
#define TpTxCallCycle 5
#define TpRxCallCycle 5
#define  kTpTxConfirmationTimeout 15
#define  kTpRxConfirmationTimeout 15

#define kBSRequested 0
#define TpSTMin 0
#define TpTxTimeoutFC 201
#define TpRxTimeoutCF 201
#define TpTxTransmitCF 1
#define __ApplTpPreCopyCheckFunction(x) 
#define __ApplTpRxCanMessageReceived


#define __ApplTpRxIndication(channel, datLen) DescPhysReqInd(channel, datLen)
#define __ApplTpTxConfirmation(channel, state) DescConfirmation(channel, state)
#define __ApplTpTxNotification(channel, datLen) 
#define __ApplTpTxCanMessageTransmitted(channel) 
#if ((OSEK_TRANSPORT_LAYER_VERSION < 0x0250) && defined (C_CPUTYPE_8BIT))
#define __ApplTpTxCopyToCAN(channel, datPos, datLen) DescCopyToCAN(channel, datPos, datLen)
extern vuint8 DescCopyToCAN(vuint8 tpChannel, vuint8 offset, vuint8 count);
#else
#define __ApplTpTxCopyToCAN(infoStruct) DescCopyToCAN(infoStruct)
extern vuint8 DescCopyToCAN(TpCopyToCanInfoStructPtr infoStruct);
#endif

#define __ApplTpTxFC(channel) 
#define __ApplTpRxSF(channel) 
#define __ApplTpRxFF(channel) 
#define __ApplTpRxCF(channel) 
#define __ApplTpRxGetBuffer(channel, datLen) DescGetBuffer(channel, datLen)
#ifdef TP_TX_ERROR_IND_COMPATIBILITY

#define __ApplTpTxErrorIndication(channel, errNo) DescTxErrorIndication(channel, errNo) 
#else

#define __ApplTpTxErrorIndication(channel, errNo) DescTxErrorIndication(channel, errNo) 
#endif

#define __ApplTpRxErrorIndication(channel, errNo) DescRxErrorIndication(channel, errNo) 
#define __ApplTpRxCopyFromCAN(dest, src, datLen) 
#define __ApplTpPrecopy(target) ApplTpCheckTA(target)
#define __ApplTpRxGetTxID(id) 

/* Tp callback prototypes */
void DescConfirmation(canuint8 channel, canuint8 state);

#ifdef TP_TX_ERROR_IND_COMPATIBILITY
void DescTxErrorIndication(canuint8 channel, canuint8 state);

#else
canuint8 DescTxErrorIndication(canuint8 channel, canuint8 state);

#endif
void DescRxErrorIndication(canuint8 channel, canuint8 state);
void DescPhysReqInd(canuint8 channel, canuint16 dataLen);
canuint8* DescGetBuffer(canuint8 channel, canuint16 dataLength);
canuint8 ApplTpCheckTA(canuint8 tpCurrentTargetAddress);


#define CAN_Transmit(n) CanTransmit(n)

#define TP_USE_CUSTOM_TX_MEMCPY kTpOn
#define TP_USE_CUSTOM_RX_MEMCPY kTpOff
#define TP_FC_OFFSET 0
#endif
