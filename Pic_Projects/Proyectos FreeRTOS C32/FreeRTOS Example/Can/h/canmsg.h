/*****************************************************************************/
/* FILE NAME:  can_msg.h                                                     */
/*                                                                           */
/* Description:  Message list.                                               */
/*****************************************************************************/
#include "GenericTypeDefs.h"
#ifndef CANMSG_H
#define CANMSG_H



typedef enum IGN_TYPE {IGN_LK , IGN_OFF , IGN_ACC , IGN_OFF_ACC , IGN_RUN , IGN_START , IGN_UND_6}; 
typedef struct {
	             INT8 	unused0: 1;
	             INT8 	RFHUB_A1ValidKeySts: 2;
	             INT8	RFHUB_A1IgnPosSts: 3;
	             INT8	RFHUB_A1CustKeyInIgnSts: 2;
               } canmsg_c_RFHUB_A1_msgType;





enum
      {
       DEFAULT_SESSION,
       ECU_SN,
       ECU_PN,
       APPSW_ID,
       EXT_FRAME
      }DIAG_REQ_FRAMES;

const INT32 control_diag_eid[4] = {
                                    0x18DA40F1,    /*EID BCM Diagnostic request EID*/
                                    1,             /*IDE = 1 means Extended ID message.	*/
                                    0,             /*RTR no RTR*/
                                    8              /*DLC*/
                                  };                                  

const INT16 output_diag[5][8] = {
                            /*byte  0    1    2    3    4    5    6    7   */
                                     {0x02,0x10,0x01,0x00,0x00,0x00,0x00,0x00},      /* Default_Session line #0*/
                                     {0x03,0x22,0xF1,0x8C,0x00,0x00,0x00,0x00},      /* ECU SERIAL NUMBER*/
                                     {0x03,0x22,0xF1,0x32,0x00,0x00,0x00,0x00},      /* ECU PART NUMBER*/
                                     {0x03,0x22,0xF1,0x81,0x00,0x00,0x00,0x00},      /* ApplicationSoftwareIdentification*/
                                     {0x30,0x00,0x00,0x00,0x00,0x00,0x00,0x00}       /* Extended frame*/
                                    };
const INT16 ign_stat [5] =           
                                    {
                                      0x2F6,   /*SID*/   
                                      0,       /*IDE*/
                                      0,       /*RTR*/
                                      1,       /*DLC*/
                                      0x12     /*Data0 = IgnPosSts = Run and CUST _KEY_IN_IGNITION*/
                                     }; 

extern  BOOL IgnRunFlag;
#endif
