#include "Can\h\GenericTypeDefs.h"
#include "Can\h\statemachine.h"
#include "Can\h\CANFunctions.h"
enum
   {
     DEFAULT_SESSION,  /*0*/
     EXTENDED_SESSION,
     Udsm_IO_5005_ON,
	 Udsm_IO_5005_OFF,
	 Udsm_IO_5006_ON,
	 Udsm_IO_5006_OFF,
     Udsm_IO_5007_ON,
	 Udsm_IO_5007_OFF,
     Udsm_IO_5008_ON,
	 Udsm_IO_5008_OFF,/*10*/
     Udsm_IO_500B_ON,
	 Udsm_IO_500B_OFF,
     Udsm_IO_500C_ON,
	 Udsm_IO_500C_OFF,
     Udsm_IO_500D_ON,
	 Udsm_IO_500D_OFF,
     Udsm_IO_500E_ON,     
	 Udsm_IO_500E_OFF,
     Udsm_IO_5010_ON,
	 Udsm_IO_5010_OFF,/*20*/
     Udsm_IO_5011_ON,
	 Udsm_IO_5011_OFF,
     Udsm_IO_5012_ON,
	 Udsm_IO_5012_OFF,
     Udsm_IO_5019_ON,
	 Udsm_IO_5019_OFF,
     Udsm_IO_5021_ON,
	 Udsm_IO_5021_OFF,
     Udsm_IO_5022_ON,
	 Udsm_IO_5022_OFF,/*30*/
     Udsm_IO_5024_ON,
	 Udsm_IO_5024_OFF,
     Udsm_IO_502C_ON,
	 Udsm_IO_502C_OFF,
     Udsm_IO_5037_ON,
	 Udsm_IO_5037_OFF,
     Udsm_IO_5040_ON,
	 Udsm_IO_5040_OFF,
     Udsm_IO_5042_ON,
	 Udsm_IO_5042_OFF,/*40*/
     Udsm_IO_5043_ON,
	 Udsm_IO_5043_OFF, 
     Udsm_IO_5045_ON,
	 Udsm_IO_5045_OFF, 
     Udsm_IO_5046_ON,
	 Udsm_IO_5046_OFF,  
     Udsm_IO_5047_ON,
	 Udsm_IO_5047_OFF, 
     Udsm_IO_5048_ON,
	 Udsm_IO_5048_OFF,/*50*/ 
	 Udsm_IO_5049_ON,
	 Udsm_IO_5049_OFF,
	 Udsm_IO_504A_ON,
	 Udsm_IO_504A_OFF,
	 Udsm_IO_504B_ON,
	 Udsm_IO_504B_OFF,
	 Udsm_IO_504E_ON,
	 Udsm_IO_504E_OFF,
	 Udsm_IO_5050_ON,
	 Udsm_IO_5050_OFF,/*60*/
	 Udsm_IO_5051_ON,
	 Udsm_IO_5051_OFF,
	 Udsm_IO_5053_ON,
	 Udsm_IO_5053_OFF,
	 Udsm_IO_5054_ON,
	 Udsm_IO_5054_OFF, 
	 Udsm_IO_5055_ON,
	 Udsm_IO_5055_OFF, 
	 Udsm_IO_5056_ON,
	 Udsm_IO_5056_OFF,/*70*/ 
	 Udsm_IO_5058_ON,
	 Udsm_IO_5058_OFF, 
	 Udsm_IO_5059_ON,
	 Udsm_IO_5059_OFF,
     Udsm_IO_505B_ON,     
	 Udsm_IO_505B_OFF,
     Udsm_IO_505C_ON,
	 Udsm_IO_505C_OFF, 
     Udsm_IO_505E_ON,
	 Udsm_IO_505E_OFF,/*80*/   
	 MAX_SERVICE_ENTRIES
   }DIAG_REQ_FRAMES;

enum 
     {
        DEF_DIAG_SESSION_SM         = 0x00,
	    DEFAULT_SESSION_RES_SM      = 0x01,
        EXT_DIAG_SESSION_SM         = 0x02,
	    EXT_DIAG_SESSION_RES_SM     = 0x03,
        ALL_OUTPUTS_CYCLE           = 0x04
     }STATE_MACHINE;

const INT16 output_diag_req[MAX_SERVICE_ENTRIES+1][FRAME_LENGTH] = {
                                                                       /*byte  0    1    2    3    4    5    6    7   */
                                                                           {0x02,0x10,0x01,0x00,0x00,0x00,0x00,0x00},      /*Default_Session */
                                                                           {0x02,0x10,0x03,0x00,0x00,0x00,0x00,0x00},      /*Extended Diag Session*/
                                                                           {0x05,0x2F,0x50,0x05,0x03,0xFF,0x00,0x00},      /*ApplDescControlTAILS_LEFT_RIGHT_Moveable on*/
										                                   {0x05,0x2F,0x50,0x05,0x03,0x00,0x00,0x00},      /*ApplDescControlTAILS_LEFT_RIGHT_Moveable off */ 
										                                   {0x05,0x2F,0x50,0x06,0x03,0xFF,0x00,0x00},      /*ApplDescControlBack_Up_Lamps_Both_Moveable on*/ 
                                                                           {0x05,0x2F,0x50,0x06,0x03,0x00,0x00,0x00},      /*ApplDescControlBack_Up_Lamps_Both_Moveable off */ 
                                                                           {0x05,0x2F,0x50,0x07,0x03,0xFF,0x00,0x00},      /*ApplDescControlRight DRL on*/ 
                                                                           {0x05,0x2F,0x50,0x07,0x03,0x00,0x00,0x00},      /*ApplDescControlRight DRL off */ 
                                                                           {0x05,0x2F,0x50,0x08,0x03,0xFF,0x00,0x00},      /*ApplDescControlLeft DRL on*/ 
                                                                           {0x05,0x2F,0x50,0x08,0x03,0x00,0x00,0x00},      /*ApplDescControlLeft DRL off */
                                                                           {0x05,0x2F,0x50,0x0B,0x03,0xFF,0x00,0x00},      /*ApplDescControlLicense plate lamp*/ 
                                                                           {0x05,0x2F,0x50,0x0B,0x03,0x00,0x00,0x00},      /*ApplDescControlLicense plate lamp off */
                                                                           {0x05,0x2F,0x50,0x0C,0x03,0xFF,0x00,0x00},      /*ApplDescControlParking and Plate Lights*/ 
                                                                           {0x05,0x2F,0x50,0x0C,0x03,0x00,0x00,0x00},      /*ApplDescControlParking and Plate Lights off */
                                                                           {0x05,0x2F,0x50,0x0D,0x03,0xFF,0x00,0x00},      /*ApplDescControlCHSML anf Stop/Turn Lights*/ 
                                                                           {0x05,0x2F,0x50,0x0D,0x03,0x00,0x00,0x00},      /*ApplDescControlCHSML anf Stop/Turn Lights off */
                                                                           {0x05,0x2F,0x50,0x0E,0x03,0xFF,0x00,0x00},      /*ApplDescControlCentral stop lamp*/ 
                                                                           {0x05,0x2F,0x50,0x0E,0x03,0x00,0x00,0x00},      /*ApplDescControlCentral stop lamp off */
                                                                           {0x05,0x2F,0x50,0x10,0x03,0xFF,0x00,0x00},      /*ApplDescControlFront left turn*/ 
                                                                           {0x05,0x2F,0x50,0x10,0x03,0x00,0x00,0x00},      /*ApplDescControlFront left turn off */
                                                                           {0x05,0x2F,0x50,0x11,0x03,0xFF,0x00,0x00},      /*ApplDescControlFront right turn*/ 
                                                                           {0x05,0x2F,0x50,0x11,0x03,0x00,0x00,0x00},      /*ApplDescControlFront right turn off */
                                                                           {0x05,0x2F,0x50,0x12,0x03,0xFF,0x00,0x00},      /*ApplDescControlFog lights on*/ 
                                                                           {0x05,0x2F,0x50,0x12,0x03,0x00,0x00,0x00},      /*ApplDescControlFog lights off */
                                                                           {0x05,0x2F,0x50,0x19,0x03,0xFF,0x00,0x00},      /*ApplDescControlTheft LED on*/ 
                                                                           {0x05,0x2F,0x50,0x19,0x03,0x00,0x00,0x00},      /*ApplDescControlTheft LED off */
                                                                           {0x05,0x2F,0x50,0x21,0x03,0xFF,0x00,0x00},      /*ApplDescControlCentral lock relay on*/ 
                                                                           {0x05,0x2F,0x50,0x21,0x03,0x00,0x00,0x00},      /*ApplDescControlCentral lock relay off */
                                                                           {0x05,0x2F,0x50,0x22,0x03,0xFF,0x00,0x00},      /*ApplDescControlTrunk release relayon*/ 
                                                                           {0x05,0x2F,0x50,0x22,0x03,0x00,0x00,0x00},      /*ApplDescControlTrunk release relay off */
                                                                           {0x05,0x2F,0x50,0x24,0x03,0xFF,0x00,0x00},      /*ApplDescControlHeated rear window relay on*/ 
                                                                           {0x05,0x2F,0x50,0x24,0x03,0x00,0x00,0x00},      /*ApplDescControlHeated rear window relayoff */
                                                                           {0x05,0x2F,0x50,0x2C,0x03,0xFF,0x00,0x00},      /*ApplDescControlInverter Enable on*/ 
                                                                           {0x05,0x2F,0x50,0x2C,0x03,0x00,0x00,0x00},      /*ApplDescControlInverter Enable off */
                                                                           {0x05,0x2F,0x50,0x37,0x03,0xFF,0x00,0x00},      /*ApplDescControlStop start relay on*/ 
                                                                           {0x05,0x2F,0x50,0x37,0x03,0x00,0x00,0x00},      /*ApplDescControlStop start relay off */ 
                                                                           {0x05,0x2F,0x50,0x40,0x03,0xFF,0x00,0x00},      /*ApplDescControlAmbient lighty on*/ 
                                                                           {0x05,0x2F,0x50,0x40,0x03,0x00,0x00,0x00},      /*ApplDescControlAmbient light off */
                                                                           {0x05,0x2F,0x50,0x42,0x03,0xFF,0x00,0x00},      /*ApplDescControlFront dome reading light on*/ 
                                                                           {0x05,0x2F,0x50,0x42,0x03,0x00,0x00,0x00},      /*ApplDescControlFront dome reading light off */
                                                                           {0x05,0x2F,0x50,0x43,0x03,0xFF,0x00,0x00},      /*ApplDescControlRear dimming lights on*/ 
                                                                           {0x05,0x2F,0x50,0x43,0x03,0x00,0x00,0x00},      /*ApplDescControlRear dimming lights off */ 
                                                                           {0x05,0x2F,0x50,0x45,0x03,0xFF,0x00,0x00},      /*ApplDescControlrun start relay  on*/ 
                                                                           {0x05,0x2F,0x50,0x45,0x03,0x00,0x00,0x00},      /*ApplDescControlrun start relay  off */
                                                                           {0x05,0x2F,0x50,0x46,0x03,0xFF,0x00,0x00},      /*ApplDescControlrun acc relay on*/ 
                                                                           {0x05,0x2F,0x50,0x46,0x03,0x00,0x00,0x00},      /*ApplDescControlrun acc relay off */ 
                                                                           {0x05,0x2F,0x50,0x47,0x03,0xFF,0x00,0x00},      /*ApplDescControlrun relay on*/ 
                                                                           {0x05,0x2F,0x50,0x47,0x03,0x00,0x00,0x00},      /*ApplDescControlrun relay off */ 
                                                                           {0x05,0x2F,0x50,0x48,0x03,0xFF,0x00,0x00},      /*ApplDescControlAcc run start relay on*/ 
                                                                           {0x05,0x2F,0x50,0x48,0x03,0x00,0x00,0x00},      /*ApplDescControlAcc run start relay off*/ 
										                                   {0x05,0x2F,0x50,0x49,0x03,0xFF,0x00,0x00},      /*ApplDescControlHorn LS driver on*/ 
                                                                           {0x05,0x2F,0x50,0x49,0x03,0x00,0x00,0x00},      /*ApplDescControlHorn LS driver off */
								                                    	   {0x05,0x2F,0x50,0x4A,0x03,0xFF,0x00,0x00},      /*ApplDescControlHeadlight shutter on*/ 
                                                                           {0x05,0x2F,0x50,0x4A,0x03,0x00,0x00,0x00},      /*ApplDescControlHeadlight shutter off */
										                                   {0x05,0x2F,0x50,0x4B,0x03,0xFF,0x00,0x00},      /*ApplDescControlHeadlamps on*/ 
                                                                           {0x05,0x2F,0x50,0x4B,0x03,0x00,0x00,0x00},      /*ApplDescControlHeadlamps off */
										                                   {0x05,0x2F,0x50,0x4E,0x03,0xFF,0x00,0x00},      /*ApplDescControlHigh beam lamps on*/ 
                                                                           {0x05,0x2F,0x50,0x4E,0x03,0x00,0x00,0x00},      /*ApplDescControlHigh beam lamps off */
										                                   {0x05,0x2F,0x50,0x50,0x03,0xFF,0x00,0x00},      /*ApplDescControlRear left stop/turn lights on*/ 
                                                                           {0x05,0x2F,0x50,0x50,0x03,0x00,0x00,0x00},      /*ApplDescControlRear left stop/turn lights off */
										                                   {0x05,0x2F,0x50,0x51,0x03,0xFF,0x00,0x00},      /*ApplDescControlRear right stop/turn light on*/ 
                                                                           {0x05,0x2F,0x50,0x51,0x03,0x00,0x00,0x00},      /*ApplDescControlRear right stop/turn light off */
										                                   {0x05,0x2F,0x50,0x53,0x03,0xFF,0x00,0x00},      /*ApplDescControlTrunk lamp on*/ 
                                                                           {0x05,0x2F,0x50,0x53,0x03,0x00,0x00,0x00},      /*ApplDescControlTrunk lamp off */
										                                   {0x05,0x2F,0x50,0x54,0x03,0xFF,0x00,0x00},      /*ApplDescControlPassenger and rear door unlock relay on*/ 
                                                                           {0x05,0x2F,0x50,0x54,0x03,0x00,0x00,0x00},      /*ApplDescControlPassenger and rear door unlock relay off */
										                                   {0x05,0x2F,0x50,0x55,0x03,0xFF,0x00,0x00},      /*ApplDescControlDriver door unlock relay on*/ 
                                                                           {0x05,0x2F,0x50,0x55,0x03,0x00,0x00,0x00},      /*ApplDescControlDriver door unlock relay off */
										                                   {0x05,0x2F,0x50,0x56,0x03,0xFF,0x00,0x00},      /*ApplDescControlUGDO theft deterrent  on*/ 
                                                                           {0x05,0x2F,0x50,0x56,0x03,0x00,0x00,0x00},      /*ApplDescControlUGDO theft deterrent  off */
										                                   {0x05,0x2F,0x50,0x58,0x03,0xFF,0x00,0x00},      /*ApplDescControlleft park tail  on*/ 
                                                                           {0x05,0x2F,0x50,0x58,0x03,0x00,0x00,0x00},      /*ApplDescControlleft park tail  off */
										                                   {0x05,0x2F,0x50,0x59,0x03,0xFF,0x00,0x00},      /*ApplDescControlRight park tail on*/ 
                                                                           {0x05,0x2F,0x50,0x59,0x03,0x00,0x00,0x00},      /*ApplDescControlRight park tail  off */
										                                   {0x05,0x2F,0x50,0x5B,0x03,0xFF,0x00,0x00},      /*ApplDescControlCourtesy Convenience Function on*/ 
                                                                           {0x05,0x2F,0x50,0x5B,0x03,0x00,0x00,0x00},      /*ApplDescControlCourtesy Convenience Function off */
										                                   {0x05,0x2F,0x50,0x5C,0x03,0xFF,0x00,0x00},      /*ApplDescControlBack up Lamps both on*/ 
                                                                           {0x05,0x2F,0x50,0x5C,0x03,0x00,0x00,0x00},      /*ApplDescControlBack up Lamps both off */
										                                   {0x05,0x2F,0x50,0x5E,0x03,0xFF,0x00,0x00},      /*ApplDescControlCourtesy Lamps Rear Dome Light on*/ 
                                                                           {0x05,0x2F,0x50,0x5E,0x03,0x00,0x00,0x00},      /*ApplDescControlCourtesy Lamps Rear Dome Light off */
                                                                           {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}       /*MAX_SERVICE_ENTRIES*/
                                                                     }; 

//INT16 diag_temp_buffer[FRAME_LENGTH];
void DiagReqStateMachine(void)  
{
 static INT8 CaseLoopState;
 static INT8 SPECIFIC_OUTPUT;
 INT8 i;

 switch(CaseLoopState)
 {
   case DEF_DIAG_SESSION_SM: 
   for(i=0;i<FRAME_LENGTH;i++)
          {
            diag_temp_buffer[i] = output_diag_req[DEFAULT_SESSION][i];
          }
   CAN2TxSendLEDMsg();
   CaseLoopState = EXT_DIAG_SESSION_SM;
   break;

   case  EXT_DIAG_SESSION_SM:
   for(i=0;i<FRAME_LENGTH;i++)
          {
            diag_temp_buffer[i] = output_diag_req[EXTENDED_SESSION][i];
          }
   CAN2TxSendLEDMsg();
   SPECIFIC_OUTPUT = Udsm_IO_5005_ON;       /* Preload the first output state*/
   CaseLoopState = ALL_OUTPUTS_CYCLE;     
   break;

   case ALL_OUTPUTS_CYCLE:
    for(i=0;i<FRAME_LENGTH;i++)
          {
            diag_temp_buffer[i] = output_diag_req[SPECIFIC_OUTPUT][i];
          }
   CAN2TxSendLEDMsg();
   SPECIFIC_OUTPUT++;
   CaseLoopState = ALL_OUTPUTS_CYCLE; 
   if(SPECIFIC_OUTPUT >= MAX_SERVICE_ENTRIES)
   {
     SPECIFIC_OUTPUT = Udsm_IO_5005_ON;       /* Preload the first output state*/ 
   }
   else
    {/*MISRA*/} 
   break;

   default:
    break;

  
 }

}

