;**********************************************************************
;   This file is a basic code template for assembly code generation   *
;   on the PIC16F887. This file contains the basic code               *
;   building blocks to build upon.                                    *
;                                                                     *
;   Refer to the MPASM User's Guide for additional information on     *
;   features of the assembler (Document DS33014).                     *
;                                                                     *
;   Refer to the respective PIC data sheet for additional             *
;   information on the instruction set.                               *
;                                                                     *
;**********************************************************************
;                                                                     *
;    Filename:	   timer.asm                                           *
;    Date:                                                            *
;    File Version:                                                    *
;                                                                     *
;    Author:                                                          *
;    Company:                                                         *
;                                                                     *
;                                                                     *
;**********************************************************************
;                                                                     *
;    Files Required: P16F887.INC                                      *
;                                                                     *
;**********************************************************************
;                                                                     *
;    Notes:                                                           *
;                                                                     *
;**********************************************************************


	list		p=16f887	; list directive to define processor
	#include	<p16f887.inc>	; processor specific variable definitions


; '__CONFIG' directive is used to embed configuration data within .asm file.
; The labels following the directive are located in the respective .inc file.
; See respective data sheet for additional information on configuration word.

	__CONFIG    _CONFIG1, _LVP_OFF & _FCMEN_ON & _IESO_OFF & _BOR_OFF & _CPD_OFF & _CP_OFF & _MCLRE_ON & _PWRTE_ON & _WDT_OFF & _INTRC_OSC_NOCLKOUT
	__CONFIG    _CONFIG2, _WRT_OFF & _BOR21V



;***** VARIABLE DEFINITIONS
tick_count   EQU    0x7D        ; tick count for T0IF flag of timer0
status_temp	 EQU	0x7E		; variable used for context saving
pclath_temp	 EQU	0x7F		; variable used for context saving


;**********************************************************************
	ORG     0x000             ; processor reset vector

	nop
    goto    main              ; go to beginning of program
   

	ORG     0x004             ; interrupt vector location

    retfie

; isr code can go here or be located as a call subroutine elsewhere





main
                   
                   movlw     0x61                ; Select internal 4 MHz clock.
                   banksel   OSCCON
                   movwf     OSCCON
        

          
                 
wait_stable
                   btfss     OSCCON,HTS          ; Is the high-speed internal oscillator stable?
                 ;  goto      wait_stable         ; If not, wait until it is.

                   movlw     0x00                ; Disable all interrupts.
                   banksel   INTCON
                   movwf     INTCON
                   banksel   PIE1
                   movwf     PIE1
                   
                   banksel   TRISE               ; Initializing for switch 
                   bsf       TRISE,RE0                
                   banksel   ANSEL 
                   CLRF      ANSEL                   ;digital I/O    

                   banksel   TMR0                             
                   clrwdt                     ; timer0 initialization, clear wdT and prescaler   
                   banksel   OPTION_REG       
                   
                   movlw     0xD0             ; Mask TMR0 select  ;Clearing:  T0CS,PSA,PS2,PS1,PS0
                   andwf     OPTION_REG,W     ; prescale bits
                   iorlw     0x07             ; set to 1:256 before it rolls over 256 micro-seconds later Setting: PS2,PS1,PS0
                   movwf     OPTION_REG                           

;***************************************************************************************************
;**************************THIS IS THE BEGINING, WAITING FOR A SWITCH PRESS**************************

          
;main_loop
                   banksel   PORTE
wait_pushed        btfss     PORTE,RE0            
                  ; goto      wait_pushed
wait_released      btfsc     PORTE,RE0
                   ;goto      wait_released
                        
                   
                   movlw     0x00               ;test to see all output PORTD on
                   banksel   TRISD
                   movwf     TRISD
                   movlw     0x01               ;RD0 on is channel 1 on scope           
                   banksel   PORTD
                   movwf     PORTD

                   banksel   TMR0
                   clrf      TMR0
                   banksel   INTCON
                   bcf       INTCON,T0IF
restart_tickcount                 
                   movlw     0x00               ; Initialize tick_count              
                   banksel   tick_count 
                   movwf     tick_count 

portd_bits6and7         
                             
                   banksel   TMR0
                   clrf      TMR0
                   banksel   INTCON
                   bcf       INTCON,T0IF          ; clear this after
toif_count         btfss     INTCON,T0IF          ; this will set every time timer0 rolls over but this flag must be reset 
                   goto      toif_count
                   banksel   INTCON
                   bcf       INTCON,T0IF          ; clear this after
                              
                   banksel   tick_count  
                   incf      tick_count           ; increment the current count
                   movf      tick_count,W                 
                   
                   xorlw     0x07                 ; HOW DO I SET THE Z to 0 using a logic operation? I want to stop at 15 0x0F
               
                   banksel   STATUS            
                   btfsc     STATUS,Z
                   goto      portd_off                 
                   movlw     0x00                 ; test to see all output PORTD on
                   banksel   TRISD
                   movwf     TRISD
                   movlw     0xC0                 ;RD6 and RD7 ON        
                   banksel   PORTD
                   movwf     PORTD
                   
                   goto      portd_bits6and7

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;PORTD now off alll the way;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;                 
portd_off           
                   banksel   TMR0                ;Set timer again
                   clrf      TMR0  
                   movlw     0x00               ; Initialize tick_count              
                   banksel   tick_count 
                   movwf     tick_count 
               
portd_alloff                 
                   banksel   INTCON
toif_count1        btfss     INTCON,T0IF          ; this will set every time timer0 rolls over but this flag must be reset 
                   goto      toif_count1
                   banksel   INTCON
                   bcf       INTCON,T0IF          ; clear this after 
                   
                   banksel   tick_count  
                   incf      tick_count           ; increment the current count
                   movf      tick_count,W                 
                   
                   xorlw     0x07                 ; HOW DO I SET THE Z to 0 using a logic operation? I want to stop at 15 0x0F
                
                   banksel   STATUS 
                   ;btfss     STATUS,Z           
                   btfsc     STATUS,Z
                   goto      restart_tickcount    ;go back to leds on         
                   movlw     0x00                 ; test to see all output PORTD on
                   banksel   TRISD
                   movwf     TRISD
                   movlw     0x00                 ;All off       
                   banksel   PORTD
                   movwf     PORTD
                   goto      portd_alloff  
   
                
                  


              
                     







                     
                  
 
                   goto      portd_off


	END                                         ; directive 'end of program'

