@echo off


@rem Clear out everything in output directory
DEL "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Batch_file_build\output"

@rem Delete out any auto generated in root directory
del *.as
del *.cof
del *.mcof
del *.hex
del *.hxl
del *.lst
del *.map
del *.obj
del *.p1
del *.pre
del *.s19
del *.sdb
del *.sym
del *.rlf
del *.dmp
del *.
cls
echo Done!


