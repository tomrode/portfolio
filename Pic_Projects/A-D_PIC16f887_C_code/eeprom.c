
#include "htc.h"
#include "typedefs.h"
#define MAX_LENGTH (256)  
#define EEPROM_BLOCK_MAX_SIZE (16)   
const eep_navy_personnel_default[MAX_LENGTH] =
                        {
                         'J','O','H','N','S','F','C','4','6','8','6','2','8','1','3','7', 
                         'R','O','D','E','T','A','T','3','6','6','0','2','3','1','6','9',
                         'H','A','W','N','J','M','M','2','7','8','2','9','5','4','3','6',
                         'M','A','D','I','P','Y','N','6','9','7','2','9','2','0','0','4',
                         'J','E','N','S','L','A','T','3','1','6','4','2','3','1','6','7',
                         'W','A','L','K','T','P','N','9','6','6','7','2','0','8','1','2',
                         'L','A','N','T','T','B','M','3','1','3','4','2','3','7','6','5',
                         'L','E','M','O','J','A','D','3','3','6','4','2','3','6','6','7',
                         'W','I','L','L','W','A','O','4','1','6','4','2','3','7','6','7',
                         'G','O','M','E','J','T','M','2','8','5','4','2','3','5','6','3',
                        };


                    
void eep_default(void)
{
   uint8_t const_byte_addr;
   uint8_t i;

for(i=0;i<=160;i++)
  {
     eeprom_write(i,eep_navy_personnel_default[i]);
     // write value to EEPROM address   
       /*eeprom_write(address, value);*/
  }
}

void eep_clear(void)
{
  
uint8_t i;
for(i=0;i<=254;i++)
  {
     eeprom_write(i,0x00);
  }
}

void eep_write_add_new(uint8_t *new_guy)
{
 /* Local Delclarations*/
 uint8_t i;
 //NAVY_PERSONEL new_guy_rec;
 uint8_t new_guy_rec_ar[16];
 
  asm("nop");

 for(i=0;i<=16;i++)
  {
    new_guy_rec_ar[i] = *new_guy;    /* de-reference it to get data out*/
    *new_guy++;                      /* increment the pointer*/
  }

 asm("nop");
}
/**********************************************************************************************************
* eeprom_write_block(): Description, The function takes in below parameters. It protects for block writes 
*                       bigger than a line (16 bytes) at a time.
*  INPUT 1    -> 8_bit address. 
*  INPUT 2   -> The size of the block in number byte's.
*  INPUT 3   -> A pointer value at the begging of block thats desired to be written in EEprom.
*
*  OUTPUTS   -> None
**********************************************************************************************************/
void eeprom_write_block(unsigned char address, unsigned char size, unsigned char * buffer)
{
/* Local Declarations*/
uint8_t i;
                                      
if (size <= EEPROM_BLOCK_MAX_SIZE)
 {
 for(i=0;i<=size;i++)                                 /* How many bytes to write?*/     
  {      
       eeprom_write(address,  * buffer);             /* write value to EEPROM address, then value from pointer that was de-referenced*/   
       buffer++;                                     /* increment pointer to next element*/
       address = address + 1;                        /* increment the address*/
  } 
 }
else
 {
     /*  Do Nothing */    
 } 
}

/**********************************************************************************************************
* eeprom_read_block(): Description, The function takes in below parameters. It protects for block writes 
*                       bigger than a line (16 bytes) at a time.
*  INPUT 1    -> 8_bit address. 
*  INPUT 2   -> The size of the block in number byte's.
*  
*
*  OUTPUTS    -> A pointer value at the beggining of block thats desired to be read from EEprom.
**********************************************************************************************************/
 unsigned char *eeprom_read_block(unsigned char address, unsigned char size)
{
/* Local Declarations*/
uint8_t i;
uint8_t holding_buffer[EEPROM_BLOCK_MAX_SIZE];                                      
if (size <= EEPROM_BLOCK_MAX_SIZE)
 {
 for(i=0;i<=size;i++)                                /* How many bytes to read?*/     
  {      
       holding_buffer[i]  = eeprom_read(address);    /* read value to EEPROM address*/   
       address = address + 1;                        /* increment the address*/
  } 
 }
else
 {
     /*  Do Nothing */    
 } 
return &holding_buffer;
}


/*****************************************************************************/
/* eeprom_read_block()                                                       */
/*                                                                           */
/* Description:    This function will read a block of EEPROM data and store  */
/*                 it into the caller's buffer.  NOTE:  Specifying a size    */
/*                 of zero will cause nothing to occur.                      */
/*                                                                           */
/* Inputs:         address -> Starting EEPROM address                        */
/*                 size    -> Number of bytes to read                        */
/*                 buffer  -> Data buffer pointer                            */
/*****************************************************************************/

void eeprom_read_block1(unsigned char address, unsigned char size, unsigned char * buffer)
{
  unsigned char i;

  /* Is there data to be read?  If so... */
  if (size != 0)
  {
    for (i = 0; i < size; i++)
    {
      /* Read one EEPROM byte and store it in the caller's buffer. */
      /* buffer[i] = (unsigned char)(eeprom_read(address)); It was this*/
      *buffer = (unsigned char)(eeprom_read(address));
      /* Go to the next buffer byte. */
      buffer++;
      /* Go to the next EEPROM location. */
      address++;
    }
  }
}
