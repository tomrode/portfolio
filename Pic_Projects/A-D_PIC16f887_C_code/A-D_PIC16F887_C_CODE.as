opt subtitle "HI-TECH Software Omniscient Code Generator (Lite mode) build 6738"

opt pagewidth 120

	opt lm

	processor	16F887
clrc	macro
	bcf	3,0
	endm
clrz	macro
	bcf	3,2
	endm
setc	macro
	bsf	3,0
	endm
setz	macro
	bsf	3,2
	endm
skipc	macro
	btfss	3,0
	endm
skipz	macro
	btfss	3,2
	endm
skipnc	macro
	btfsc	3,0
	endm
skipnz	macro
	btfsc	3,2
	endm
indf	equ	0
indf0	equ	0
pc	equ	2
pcl	equ	2
status	equ	3
fsr	equ	4
fsr0	equ	4
c	equ	1
z	equ	0
pclath	equ	10
# 38 "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\A-D_PIC16f887_C_code\main.c"
	psect config,class=CONFIG,delta=2 ;#
# 38 "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\A-D_PIC16f887_C_code\main.c"
	dw 0x1FFF & 0x3FFF & 0x3FFF & 0x3BFF & 0x3EFF & 0x3FFF & 0x3FFF & 0x3FFF & 0x3FEF & 0x3FF7 & 0x3FFD ;#
# 39 "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\A-D_PIC16f887_C_code\main.c"
	psect config,class=CONFIG,delta=2 ;#
# 39 "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\A-D_PIC16f887_C_code\main.c"
	dw 0x3FFF ;#
	FNCALL	_main,_sfreg
	FNCALL	_main,_adc_init
	FNCALL	_main,_temperature_state_machine
	FNCALL	_main,_temperature_set
	FNCALL	_main,_humidity_state_machine
	FNCALL	_main,_humidity_set
	FNCALL	_main,_eep_clear
	FNCALL	_main,_adc_convert
	FNCALL	_main,_adc_bargraph
	FNCALL	_main,_eeprom_write_block
	FNCALL	_main,_adc_volt_convert
	FNCALL	_main,___lwtoft
	FNCALL	_main,___lbtoft
	FNCALL	_main,___ftmul
	FNCALL	___lwtoft,___ftpack
	FNCALL	___lbtoft,___ftpack
	FNCALL	___ftmul,___ftpack
	FNCALL	_adc_volt_convert,_adc_convert
	FNCALL	_adc_volt_convert,___lmul
	FNCALL	_eeprom_write_block,_eeprom_write
	FNCALL	_eep_clear,_eeprom_write
	FNCALL	_humidity_state_machine,_timer_set
	FNCALL	_humidity_state_machine,_get_timer
	FNCALL	_temperature_state_machine,_timer_set
	FNCALL	_temperature_state_machine,_get_timer
	FNROOT	_main
	FNCALL	_interrupt_handler,_adc_isr
	FNCALL	_interrupt_handler,_timer_isr
	FNCALL	intlevel1,_interrupt_handler
	global	intlevel1
	FNROOT	intlevel1
	global	_adc_bargraph_array2
	global	humidity_state_machine@led_state
	global	temperature_state_machine@led_state
	global	_adc_bargraph_array
psect	idataBANK0,class=CODE,space=0,delta=2
global __pidataBANK0
__pidataBANK0:
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\A-D_PIC16f887_C_code\adc_func.c"
	line	40

;initializer for _adc_bargraph_array2
	retlw	0
	retlw	01h
	retlw	02h
	retlw	04h
	retlw	08h
	retlw	010h
	retlw	020h
	retlw	040h
	retlw	080h
psect	idataCOMMON,class=CODE,space=0,delta=2
global __pidataCOMMON
__pidataCOMMON:
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\A-D_PIC16f887_C_code\humidity.c"
	line	27

;initializer for humidity_state_machine@led_state
	retlw	03h
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\A-D_PIC16f887_C_code\temperature.c"

;initializer for temperature_state_machine@led_state
	retlw	03h
psect	idataBANK1,class=CODE,space=0,delta=2
global __pidataBANK1
__pidataBANK1:
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\A-D_PIC16f887_C_code\adc_func.c"
	line	20

;initializer for _adc_bargraph_array
	retlw	0
	retlw	01h
	retlw	03h
	retlw	03h
	retlw	07h
	retlw	07h
	retlw	0Fh
	retlw	0Fh
	retlw	01Fh
	retlw	01Fh
	retlw	03Fh
	retlw	03Fh
	retlw	07Fh
	retlw	07Fh
	retlw	0FFh
	retlw	0FFh
	global	_eep_navy_personnel_default
psect	stringtext,class=STRCODE,delta=2,reloc=256
global __pstringtext
__pstringtext:
;	global	stringtab,__stringbase
stringtab:
;	String table - string pointers are 2 bytes each
	btfsc	(btemp+1),7
	ljmp	stringcode
	bcf	status,7
	btfsc	(btemp+1),0
	bsf	status,7
	movf	indf,w
	incf fsr
skipnz
incf btemp+1
	return
stringcode:
	movf btemp+1,w
andlw 7Fh
movwf	pclath
	movf	fsr,w
incf fsr
skipnz
incf btemp+1
	movwf pc
__stringbase:
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\A-D_PIC16f887_C_code\eeprom.c"
	line	6
_eep_navy_personnel_default:
	retlw	04Ah
	retlw	0

	retlw	04Fh
	retlw	0

	retlw	048h
	retlw	0

	retlw	04Eh
	retlw	0

	retlw	053h
	retlw	0

	retlw	046h
	retlw	0

	retlw	043h
	retlw	0

	retlw	034h
	retlw	0

	retlw	036h
	retlw	0

	retlw	038h
	retlw	0

	retlw	036h
	retlw	0

	retlw	032h
	retlw	0

	retlw	038h
	retlw	0

	retlw	031h
	retlw	0

	retlw	033h
	retlw	0

	retlw	037h
	retlw	0

	retlw	052h
	retlw	0

	retlw	04Fh
	retlw	0

	retlw	044h
	retlw	0

	retlw	045h
	retlw	0

	retlw	054h
	retlw	0

	retlw	041h
	retlw	0

	retlw	054h
	retlw	0

	retlw	033h
	retlw	0

	retlw	036h
	retlw	0

	retlw	036h
	retlw	0

	retlw	030h
	retlw	0

	retlw	032h
	retlw	0

	retlw	033h
	retlw	0

	retlw	031h
	retlw	0

	retlw	036h
	retlw	0

	retlw	039h
	retlw	0

	retlw	048h
	retlw	0

	retlw	041h
	retlw	0

	retlw	057h
	retlw	0

	retlw	04Eh
	retlw	0

	retlw	04Ah
	retlw	0

	retlw	04Dh
	retlw	0

	retlw	04Dh
	retlw	0

	retlw	032h
	retlw	0

	retlw	037h
	retlw	0

	retlw	038h
	retlw	0

	retlw	032h
	retlw	0

	retlw	039h
	retlw	0

	retlw	035h
	retlw	0

	retlw	034h
	retlw	0

	retlw	033h
	retlw	0

	retlw	036h
	retlw	0

	retlw	04Dh
	retlw	0

	retlw	041h
	retlw	0

	retlw	044h
	retlw	0

	retlw	049h
	retlw	0

	retlw	050h
	retlw	0

	retlw	059h
	retlw	0

	retlw	04Eh
	retlw	0

	retlw	036h
	retlw	0

	retlw	039h
	retlw	0

	retlw	037h
	retlw	0

	retlw	032h
	retlw	0

	retlw	039h
	retlw	0

	retlw	032h
	retlw	0

	retlw	030h
	retlw	0

	retlw	030h
	retlw	0

	retlw	034h
	retlw	0

	retlw	04Ah
	retlw	0

	retlw	045h
	retlw	0

	retlw	04Eh
	retlw	0

	retlw	053h
	retlw	0

	retlw	04Ch
	retlw	0

	retlw	041h
	retlw	0

	retlw	054h
	retlw	0

	retlw	033h
	retlw	0

	retlw	031h
	retlw	0

	retlw	036h
	retlw	0

	retlw	034h
	retlw	0

	retlw	032h
	retlw	0

	retlw	033h
	retlw	0

	retlw	031h
	retlw	0

	retlw	036h
	retlw	0

	retlw	037h
	retlw	0

	retlw	057h
	retlw	0

	retlw	041h
	retlw	0

	retlw	04Ch
	retlw	0

	retlw	04Bh
	retlw	0

	retlw	054h
	retlw	0

	retlw	050h
	retlw	0

	retlw	04Eh
	retlw	0

	retlw	039h
	retlw	0

	retlw	036h
	retlw	0

	retlw	036h
	retlw	0

	retlw	037h
	retlw	0

	retlw	032h
	retlw	0

	retlw	030h
	retlw	0

	retlw	038h
	retlw	0

	retlw	031h
	retlw	0

	retlw	032h
	retlw	0

	retlw	04Ch
	retlw	0

	retlw	041h
	retlw	0

	retlw	04Eh
	retlw	0

	retlw	054h
	retlw	0

	retlw	054h
	retlw	0

	retlw	042h
	retlw	0

	retlw	04Dh
	retlw	0

	retlw	033h
	retlw	0

	retlw	031h
	retlw	0

	retlw	033h
	retlw	0

	retlw	034h
	retlw	0

	retlw	032h
	retlw	0

	retlw	033h
	retlw	0

	retlw	037h
	retlw	0

	retlw	036h
	retlw	0

	retlw	035h
	retlw	0

	retlw	04Ch
	retlw	0

	retlw	045h
	retlw	0

	retlw	04Dh
	retlw	0

	retlw	04Fh
	retlw	0

	retlw	04Ah
	retlw	0

	retlw	041h
	retlw	0

	retlw	044h
	retlw	0

	retlw	033h
	retlw	0

	retlw	033h
	retlw	0

	retlw	036h
	retlw	0

	retlw	034h
	retlw	0

	retlw	032h
	retlw	0

	retlw	033h
	retlw	0

	retlw	036h
	retlw	0

	retlw	036h
	retlw	0

	retlw	037h
	retlw	0

	retlw	057h
	retlw	0

	retlw	049h
	retlw	0

	retlw	04Ch
	retlw	0

	retlw	04Ch
	retlw	0

	retlw	057h
	retlw	0

	retlw	041h
	retlw	0

	retlw	04Fh
	retlw	0

	retlw	034h
	retlw	0

	retlw	031h
	retlw	0

	retlw	036h
	retlw	0

	retlw	034h
	retlw	0

	retlw	032h
	retlw	0

	retlw	033h
	retlw	0

	retlw	037h
	retlw	0

	retlw	036h
	retlw	0

	retlw	037h
	retlw	0

	retlw	047h
	retlw	0

	retlw	04Fh
	retlw	0

	retlw	04Dh
	retlw	0

	retlw	045h
	retlw	0

	retlw	04Ah
	retlw	0

	retlw	054h
	retlw	0

	retlw	04Dh
	retlw	0

	retlw	032h
	retlw	0

	retlw	038h
	retlw	0

	retlw	035h
	retlw	0

	retlw	034h
	retlw	0

	retlw	032h
	retlw	0

	retlw	033h
	retlw	0

	retlw	035h
	retlw	0

	retlw	036h
	retlw	0

	retlw	033h
	retlw	0

	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	global	_eep_navy_personnel_default
	global	_timer_array
	global	_adc_isr_flag
	global	_int_count
	global	_temp_set_val
	global	_tmr1_counter
	global	_tmr1_isr_counter
	global	_loops
	global	_toms_variable
	global	_union_vars
	global	humidity@loops
	global	_persistent_watch_dog_count
psect	nvBANK0,class=BANK0,space=1
global __pnvBANK0
__pnvBANK0:
_persistent_watch_dog_count:
       ds      2

	global	_ADCON0
_ADCON0	set	31
	global	_ADRESH
_ADRESH	set	30
	global	_PORTB
_PORTB	set	6
	global	_PORTC
_PORTC	set	7
	global	_T1CON
_T1CON	set	16
	global	_TMR1H
_TMR1H	set	15
	global	_TMR1L
_TMR1L	set	14
	global	_ADIF
_ADIF	set	102
	global	_CARRY
_CARRY	set	24
	global	_GIE
_GIE	set	95
	global	_GODONE
_GODONE	set	249
	global	_PEIE
_PEIE	set	94
	global	_TMR1IF
_TMR1IF	set	96
	global	_TO
_TO	set	28
	global	_ADCON1
_ADCON1	set	159
	global	_ADRESL
_ADRESL	set	158
	global	_OPTION
_OPTION	set	129
	global	_OSCCON
_OSCCON	set	143
	global	_TRISA
_TRISA	set	133
	global	_TRISB
_TRISB	set	134
	global	_TRISC
_TRISC	set	135
	global	_TRISD
_TRISD	set	136
	global	_ADIE
_ADIE	set	1126
	global	_HTS
_HTS	set	1146
	global	_TMR1IE
_TMR1IE	set	1120
	global	_EEADR
_EEADR	set	269
	global	_EEDATA
_EEDATA	set	268
	global	_WDTCON
_WDTCON	set	261
	global	_ANSEL
_ANSEL	set	392
	global	_ANSELH
_ANSELH	set	393
	global	_EECON1
_EECON1	set	396
	global	_EECON2
_EECON2	set	397
	global	_RD
_RD	set	3168
	global	_WR
_WR	set	3169
	global	_WREN
_WREN	set	3170
	global	_PORTD
_PORTD	set	8
	file	"A-D_PIC16F887_C_CODE.as"
	line	#
psect cinit,class=CODE,delta=2
global start_initialization
start_initialization:

psect	bssCOMMON,class=COMMON,space=1
global __pbssCOMMON
__pbssCOMMON:
_loops:
       ds      1

_toms_variable:
       ds      1

_union_vars:
       ds      1

humidity@loops:
       ds      1

psect	dataCOMMON,class=COMMON,space=1
global __pdataCOMMON
__pdataCOMMON:
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\A-D_PIC16f887_C_code\humidity.c"
	line	27
humidity_state_machine@led_state:
       ds      1

psect	dataCOMMON
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\A-D_PIC16f887_C_code\temperature.c"
temperature_state_machine@led_state:
       ds      1

psect	bssBANK0,class=BANK0,space=1
global __pbssBANK0
__pbssBANK0:
_timer_array:
       ds      4

_adc_isr_flag:
       ds      2

_int_count:
       ds      2

_temp_set_val:
       ds      1

_tmr1_counter:
       ds      1

_tmr1_isr_counter:
       ds      1

psect	dataBANK0,class=BANK0,space=1
global __pdataBANK0
__pdataBANK0:
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\A-D_PIC16f887_C_code\adc_func.c"
	line	40
_adc_bargraph_array2:
       ds      9

psect	dataBANK1,class=BANK1,space=1
global __pdataBANK1
__pdataBANK1:
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\A-D_PIC16f887_C_code\adc_func.c"
	line	20
_adc_bargraph_array:
       ds      16

; Clear objects allocated to COMMON
psect cinit,class=CODE,delta=2
	clrf	((__pbssCOMMON)+0)&07Fh
	clrf	((__pbssCOMMON)+1)&07Fh
	clrf	((__pbssCOMMON)+2)&07Fh
	clrf	((__pbssCOMMON)+3)&07Fh
; Clear objects allocated to BANK0
psect cinit,class=CODE,delta=2
	clrf	((__pbssBANK0)+0)&07Fh
	clrf	((__pbssBANK0)+1)&07Fh
	clrf	((__pbssBANK0)+2)&07Fh
	clrf	((__pbssBANK0)+3)&07Fh
	clrf	((__pbssBANK0)+4)&07Fh
	clrf	((__pbssBANK0)+5)&07Fh
	clrf	((__pbssBANK0)+6)&07Fh
	clrf	((__pbssBANK0)+7)&07Fh
	clrf	((__pbssBANK0)+8)&07Fh
	clrf	((__pbssBANK0)+9)&07Fh
	clrf	((__pbssBANK0)+10)&07Fh
global btemp
psect inittext,class=CODE,delta=2
global init_fetch,btemp
;	Called with low address in FSR and high address in W
init_fetch:
	movf btemp,w
	movwf pclath
	movf btemp+1,w
	movwf pc
global init_ram
;Called with:
;	high address of idata address in btemp 
;	low address of idata address in btemp+1 
;	low address of data in FSR
;	high address + 1 of data in btemp-1
init_ram:
	fcall init_fetch
	movwf indf,f
	incf fsr,f
	movf fsr,w
	xorwf btemp-1,w
	btfsc status,2
	retlw 0
	incf btemp+1,f
	btfsc status,2
	incf btemp,f
	goto init_ram
; Initialize objects allocated to BANK1
psect cinit,class=CODE,delta=2
global init_ram, __pidataBANK1
	bcf	status, 7	;select IRP bank0
	movlw low(__pdataBANK1+16)
	movwf btemp-1,f
	movlw high(__pidataBANK1)
	movwf btemp,f
	movlw low(__pidataBANK1)
	movwf btemp+1,f
	movlw low(__pdataBANK1)
	movwf fsr,f
	fcall init_ram
; Initialize objects allocated to BANK0
psect cinit,class=CODE,delta=2
global init_ram, __pidataBANK0
	movlw low(__pdataBANK0+9)
	movwf btemp-1,f
	movlw high(__pidataBANK0)
	movwf btemp,f
	movlw low(__pidataBANK0)
	movwf btemp+1,f
	movlw low(__pdataBANK0)
	movwf fsr,f
	fcall init_ram
; Initialize objects allocated to COMMON
	global __pidataCOMMON
psect cinit,class=CODE,delta=2
	fcall	__pidataCOMMON+0		;fetch initializer
	movwf	__pdataCOMMON+0&07fh		
	fcall	__pidataCOMMON+1		;fetch initializer
	movwf	__pdataCOMMON+1&07fh		
psect cinit,class=CODE,delta=2
global end_of_initialization

;End of C runtime variable initialization code

end_of_initialization:
clrf status
ljmp _main	;jump to C main() function
psect	cstackCOMMON,class=COMMON,space=1
global __pcstackCOMMON
__pcstackCOMMON:
	global	?_sfreg
?_sfreg:	; 0 bytes @ 0x0
	global	?_adc_init
?_adc_init:	; 0 bytes @ 0x0
	global	?_temperature_state_machine
?_temperature_state_machine:	; 0 bytes @ 0x0
	global	?_temperature_set
?_temperature_set:	; 0 bytes @ 0x0
	global	?_humidity_state_machine
?_humidity_state_machine:	; 0 bytes @ 0x0
	global	?_humidity_set
?_humidity_set:	; 0 bytes @ 0x0
	global	?_eep_clear
?_eep_clear:	; 0 bytes @ 0x0
	global	?_timer_isr
?_timer_isr:	; 0 bytes @ 0x0
	global	??_timer_isr
??_timer_isr:	; 0 bytes @ 0x0
	global	?_adc_isr
?_adc_isr:	; 0 bytes @ 0x0
	global	??_adc_isr
??_adc_isr:	; 0 bytes @ 0x0
	global	?_interrupt_handler
?_interrupt_handler:	; 0 bytes @ 0x0
	global	?_main
?_main:	; 2 bytes @ 0x0
	ds	1
	global	timer_isr@i
timer_isr@i:	; 1 bytes @ 0x1
	ds	1
	global	??_interrupt_handler
??_interrupt_handler:	; 0 bytes @ 0x2
	ds	4
psect	cstackBANK0,class=BANK0,space=1
global __pcstackBANK0
__pcstackBANK0:
	global	??_sfreg
??_sfreg:	; 0 bytes @ 0x0
	global	??_adc_init
??_adc_init:	; 0 bytes @ 0x0
	global	??_temperature_set
??_temperature_set:	; 0 bytes @ 0x0
	global	??_humidity_set
??_humidity_set:	; 0 bytes @ 0x0
	global	?_eeprom_write
?_eeprom_write:	; 0 bytes @ 0x0
	global	?_timer_set
?_timer_set:	; 0 bytes @ 0x0
	global	?_adc_bargraph
?_adc_bargraph:	; 1 bytes @ 0x0
	global	?_adc_convert
?_adc_convert:	; 2 bytes @ 0x0
	global	?_get_timer
?_get_timer:	; 2 bytes @ 0x0
	global	?___ftpack
?___ftpack:	; 3 bytes @ 0x0
	global	?___lmul
?___lmul:	; 4 bytes @ 0x0
	global	eeprom_write@value
eeprom_write@value:	; 1 bytes @ 0x0
	global	adc_bargraph@value
adc_bargraph@value:	; 2 bytes @ 0x0
	global	timer_set@value
timer_set@value:	; 2 bytes @ 0x0
	global	___ftpack@arg
___ftpack@arg:	; 3 bytes @ 0x0
	global	___lmul@multiplier
___lmul@multiplier:	; 4 bytes @ 0x0
	ds	1
	global	??_eeprom_write
??_eeprom_write:	; 0 bytes @ 0x1
	global	temperature_set@set_loops
temperature_set@set_loops:	; 1 bytes @ 0x1
	global	humidity_set@set_loops
humidity_set@set_loops:	; 1 bytes @ 0x1
	ds	1
	global	??_adc_convert
??_adc_convert:	; 0 bytes @ 0x2
	global	??_adc_bargraph
??_adc_bargraph:	; 0 bytes @ 0x2
	global	??_timer_set
??_timer_set:	; 0 bytes @ 0x2
	global	??_get_timer
??_get_timer:	; 0 bytes @ 0x2
	global	eeprom_write@addr
eeprom_write@addr:	; 1 bytes @ 0x2
	ds	1
	global	??_eep_clear
??_eep_clear:	; 0 bytes @ 0x3
	global	?_eeprom_write_block
?_eeprom_write_block:	; 0 bytes @ 0x3
	global	timer_set@index
timer_set@index:	; 1 bytes @ 0x3
	global	eeprom_write_block@size
eeprom_write_block@size:	; 1 bytes @ 0x3
	global	___ftpack@exp
___ftpack@exp:	; 1 bytes @ 0x3
	global	get_timer@result
get_timer@result:	; 2 bytes @ 0x3
	ds	1
	global	eep_clear@i
eep_clear@i:	; 1 bytes @ 0x4
	global	eeprom_write_block@buffer
eeprom_write_block@buffer:	; 1 bytes @ 0x4
	global	___ftpack@sign
___ftpack@sign:	; 1 bytes @ 0x4
	global	adc_convert@adresh
adc_convert@adresh:	; 2 bytes @ 0x4
	global	___lmul@multiplicand
___lmul@multiplicand:	; 4 bytes @ 0x4
	ds	1
	global	??_eeprom_write_block
??_eeprom_write_block:	; 0 bytes @ 0x5
	global	??___ftpack
??___ftpack:	; 0 bytes @ 0x5
	global	adc_bargraph@result
adc_bargraph@result:	; 1 bytes @ 0x5
	global	get_timer@index
get_timer@index:	; 1 bytes @ 0x5
	ds	1
	global	??_temperature_state_machine
??_temperature_state_machine:	; 0 bytes @ 0x6
	global	??_humidity_state_machine
??_humidity_state_machine:	; 0 bytes @ 0x6
	global	eeprom_write_block@i
eeprom_write_block@i:	; 1 bytes @ 0x6
	global	adc_convert@adresl
adc_convert@adresl:	; 2 bytes @ 0x6
	ds	1
	global	eeprom_write_block@address
eeprom_write_block@address:	; 1 bytes @ 0x7
	ds	1
	global	??___lmul
??___lmul:	; 0 bytes @ 0x8
	global	?___lbtoft
?___lbtoft:	; 3 bytes @ 0x8
	global	adc_convert@result
adc_convert@result:	; 2 bytes @ 0x8
	ds	1
	global	___lmul@product
___lmul@product:	; 4 bytes @ 0x9
	ds	2
	global	??___lbtoft
??___lbtoft:	; 0 bytes @ 0xB
	ds	2
	global	?_adc_volt_convert
?_adc_volt_convert:	; 2 bytes @ 0xD
	ds	2
	global	??_adc_volt_convert
??_adc_volt_convert:	; 0 bytes @ 0xF
	global	___lbtoft@c
___lbtoft@c:	; 1 bytes @ 0xF
	ds	1
	global	?___ftmul
?___ftmul:	; 3 bytes @ 0x10
	global	___ftmul@f1
___ftmul@f1:	; 3 bytes @ 0x10
	ds	3
	global	adc_volt_convert@adc_rawvalue
adc_volt_convert@adc_rawvalue:	; 2 bytes @ 0x13
	global	___ftmul@f2
___ftmul@f2:	; 3 bytes @ 0x13
	ds	2
	global	adc_volt_convert@adc_millivolt_macroval
adc_volt_convert@adc_millivolt_macroval:	; 2 bytes @ 0x15
	ds	1
	global	??___ftmul
??___ftmul:	; 0 bytes @ 0x16
	ds	1
	global	adc_volt_convert@adc_millivolt
adc_volt_convert@adc_millivolt:	; 2 bytes @ 0x17
	ds	2
	global	?___lwtoft
?___lwtoft:	; 3 bytes @ 0x19
	global	___lwtoft@c
___lwtoft@c:	; 2 bytes @ 0x19
	ds	1
	global	___ftmul@exp
___ftmul@exp:	; 1 bytes @ 0x1A
	ds	1
	global	___ftmul@f3_as_product
___ftmul@f3_as_product:	; 3 bytes @ 0x1B
	ds	1
	global	??___lwtoft
??___lwtoft:	; 0 bytes @ 0x1C
	ds	2
	global	___ftmul@cntr
___ftmul@cntr:	; 1 bytes @ 0x1E
	ds	1
	global	___ftmul@sign
___ftmul@sign:	; 1 bytes @ 0x1F
	ds	1
	global	??_main
??_main:	; 0 bytes @ 0x20
	ds	2
	global	main@count
main@count:	; 4 bytes @ 0x22
	ds	4
	global	main@num1
main@num1:	; 4 bytes @ 0x26
	ds	4
	global	main@num2
main@num2:	; 4 bytes @ 0x2A
	ds	4
	global	main@adc_millivolts
main@adc_millivolts:	; 3 bytes @ 0x2E
	ds	3
	global	main@i
main@i:	; 1 bytes @ 0x31
	ds	1
	global	main@xor_num1
main@xor_num1:	; 1 bytes @ 0x32
	ds	1
	global	main@watchdog
main@watchdog:	; 1 bytes @ 0x33
	ds	1
	global	main@adc_result
main@adc_result:	; 2 bytes @ 0x34
	ds	2
;;Data sizes: Strings 0, constant 512, data 27, bss 15, persistent 2 stack 0
;;Auto spaces:   Size  Autos    Used
;; COMMON          14      6      12
;; BANK0           80     54      76
;; BANK1           80      0      16
;; BANK3           96      0       0
;; BANK2           96      0       0

;;
;; Pointer list with targets:

;; ?___lbtoft	float  size(1) Largest target is 0
;;
;; ?___ftpack	float  size(1) Largest target is 0
;;
;; ?_get_timer	unsigned short  size(1) Largest target is 0
;;
;; ?_adc_convert	unsigned short  size(1) Largest target is 0
;;
;; ?_adc_volt_convert	unsigned short  size(1) Largest target is 0
;;
;; ?___lmul	unsigned long  size(1) Largest target is 0
;;
;; ?___ftmul	float  size(1) Largest target is 0
;;
;; ?___lwtoft	float  size(1) Largest target is 0
;;
;; sp__eeprom_read_block	PTR unsigned char  size(1) Largest target is 16
;;		 -> eeprom_read_block@holding_buffer(BANK0[16]), 
;;
;; eeprom_write_block@buffer	PTR unsigned char  size(1) Largest target is 2
;;		 -> main@adc_result(BANK0[2]), 
;;


;;
;; Critical Paths under _main in COMMON
;;
;;   None.
;;
;; Critical Paths under _interrupt_handler in COMMON
;;
;;   _interrupt_handler->_timer_isr
;;
;; Critical Paths under _main in BANK0
;;
;;   _main->___ftmul
;;   ___lwtoft->_adc_volt_convert
;;   ___lbtoft->___ftpack
;;   ___ftmul->___lbtoft
;;   _adc_volt_convert->___lmul
;;   _eeprom_write_block->_eeprom_write
;;   _eep_clear->_eeprom_write
;;   _humidity_state_machine->_get_timer
;;   _temperature_state_machine->_get_timer
;;
;; Critical Paths under _interrupt_handler in BANK0
;;
;;   None.
;;
;; Critical Paths under _main in BANK1
;;
;;   None.
;;
;; Critical Paths under _interrupt_handler in BANK1
;;
;;   None.
;;
;; Critical Paths under _main in BANK3
;;
;;   None.
;;
;; Critical Paths under _interrupt_handler in BANK3
;;
;;   None.
;;
;; Critical Paths under _main in BANK2
;;
;;   None.
;;
;; Critical Paths under _interrupt_handler in BANK2
;;
;;   None.

;;
;;Main: autosize = 0, tempsize = 2, incstack = 0, save=0
;;

;;
;;Call Graph Tables:
;;
;; ---------------------------------------------------------------------------------
;; (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;; ---------------------------------------------------------------------------------
;; (0) _main                                                40    40      0    2992
;;                                             32 BANK0     22    22      0
;;                              _sfreg
;;                           _adc_init
;;          _temperature_state_machine
;;                    _temperature_set
;;             _humidity_state_machine
;;                       _humidity_set
;;                          _eep_clear
;;                        _adc_convert
;;                       _adc_bargraph
;;                 _eeprom_write_block
;;                   _adc_volt_convert
;;                           ___lwtoft
;;                           ___lbtoft
;;                            ___ftmul
;; ---------------------------------------------------------------------------------
;; (1) ___lwtoft                                             4     1      3     343
;;                                             25 BANK0      4     1      3
;;                           ___ftpack
;;                   _adc_volt_convert (ARG)
;; ---------------------------------------------------------------------------------
;; (1) ___lbtoft                                             8     5      3     343
;;                                              8 BANK0      8     5      3
;;                           ___ftpack
;; ---------------------------------------------------------------------------------
;; (1) ___ftmul                                             16    10      6     800
;;                                             16 BANK0     16    10      6
;;                           ___ftpack
;;                           ___lbtoft (ARG)
;; ---------------------------------------------------------------------------------
;; (1) _adc_volt_convert                                    12    10      2     343
;;                                             13 BANK0     12    10      2
;;                        _adc_convert
;;                             ___lmul
;; ---------------------------------------------------------------------------------
;; (1) _eeprom_write_block                                   5     3      2     260
;;                                              3 BANK0      5     3      2
;;                       _eeprom_write
;; ---------------------------------------------------------------------------------
;; (1) _eep_clear                                            2     2      0     161
;;                                              3 BANK0      2     2      0
;;                       _eeprom_write
;; ---------------------------------------------------------------------------------
;; (1) _humidity_state_machine                               1     1      0     192
;;                                              6 BANK0      1     1      0
;;                          _timer_set
;;                          _get_timer
;; ---------------------------------------------------------------------------------
;; (1) _temperature_state_machine                            1     1      0     192
;;                                              6 BANK0      1     1      0
;;                          _timer_set
;;                          _get_timer
;; ---------------------------------------------------------------------------------
;; (2) ___lmul                                              13     5      8     136
;;                                              0 BANK0     13     5      8
;; ---------------------------------------------------------------------------------
;; (2) ___ftpack                                             8     3      5     312
;;                                              0 BANK0      8     3      5
;; ---------------------------------------------------------------------------------
;; (2) _eeprom_write                                         3     2      1      62
;;                                              0 BANK0      3     2      1
;; ---------------------------------------------------------------------------------
;; (2) _get_timer                                            6     4      2      99
;;                                              0 BANK0      6     4      2
;; ---------------------------------------------------------------------------------
;; (2) _timer_set                                            6     4      2      93
;;                                              0 BANK0      4     2      2
;; ---------------------------------------------------------------------------------
;; (1) _adc_bargraph                                         6     4      2      99
;;                                              0 BANK0      6     4      2
;; ---------------------------------------------------------------------------------
;; (2) _adc_convert                                         10     8      2     102
;;                                              0 BANK0     10     8      2
;; ---------------------------------------------------------------------------------
;; (1) _humidity_set                                         2     2      0      31
;;                                              0 BANK0      2     2      0
;; ---------------------------------------------------------------------------------
;; (1) _temperature_set                                      2     2      0      31
;;                                              0 BANK0      2     2      0
;; ---------------------------------------------------------------------------------
;; (1) _adc_init                                             0     0      0       0
;; ---------------------------------------------------------------------------------
;; (1) _sfreg                                                1     1      0       0
;;                                              0 BANK0      1     1      0
;; ---------------------------------------------------------------------------------
;; Estimated maximum stack depth 2
;; ---------------------------------------------------------------------------------
;; (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;; ---------------------------------------------------------------------------------
;; (3) _interrupt_handler                                    4     4      0      90
;;                                              2 COMMON     4     4      0
;;                            _adc_isr
;;                          _timer_isr
;; ---------------------------------------------------------------------------------
;; (4) _timer_isr                                            2     2      0      90
;;                                              0 COMMON     2     2      0
;; ---------------------------------------------------------------------------------
;; (4) _adc_isr                                              0     0      0       0
;; ---------------------------------------------------------------------------------
;; Estimated maximum stack depth 4
;; ---------------------------------------------------------------------------------

;; Call Graph Graphs:

;; _main (ROOT)
;;   _sfreg
;;   _adc_init
;;   _temperature_state_machine
;;     _timer_set
;;     _get_timer
;;   _temperature_set
;;   _humidity_state_machine
;;     _timer_set
;;     _get_timer
;;   _humidity_set
;;   _eep_clear
;;     _eeprom_write
;;   _adc_convert
;;   _adc_bargraph
;;   _eeprom_write_block
;;     _eeprom_write
;;   _adc_volt_convert
;;     _adc_convert
;;     ___lmul
;;   ___lwtoft
;;     ___ftpack
;;     _adc_volt_convert (ARG)
;;       _adc_convert
;;       ___lmul
;;   ___lbtoft
;;     ___ftpack
;;   ___ftmul
;;     ___ftpack
;;     ___lbtoft (ARG)
;;       ___ftpack
;;
;; _interrupt_handler (ROOT)
;;   _adc_isr
;;   _timer_isr
;;

;; Address spaces:

;;Name               Size   Autos  Total    Cost      Usage
;;BANK3               60      0       0       9        0.0%
;;BITBANK3            60      0       0       8        0.0%
;;SFR3                 0      0       0       4        0.0%
;;BITSFR3              0      0       0       4        0.0%
;;BANK2               60      0       0      11        0.0%
;;BITBANK2            60      0       0      10        0.0%
;;SFR2                 0      0       0       5        0.0%
;;BITSFR2              0      0       0       5        0.0%
;;SFR1                 0      0       0       2        0.0%
;;BITSFR1              0      0       0       2        0.0%
;;BANK1               50      0      10       7       20.0%
;;BITBANK1            50      0       0       6        0.0%
;;CODE                 0      0       0       0        0.0%
;;DATA                 0      0      6E      12        0.0%
;;ABS                  0      0      68       3        0.0%
;;NULL                 0      0       0       0        0.0%
;;STACK                0      0       6       2        0.0%
;;BANK0               50     36      4C       5       95.0%
;;BITBANK0            50      0       0       4        0.0%
;;SFR0                 0      0       0       1        0.0%
;;BITSFR0              0      0       0       1        0.0%
;;COMMON               E      6       C       1       85.7%
;;BITCOMMON            E      0       0       0        0.0%
;;EEDATA             100      0       0       0        0.0%

	global	_main
psect	maintext,global,class=CODE,delta=2
global __pmaintext
__pmaintext:

;; *************** function _main *****************
;; Defined at:
;;		line 55 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\A-D_PIC16f887_C_code\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  num2            4   42[BANK0 ] unsigned long 
;;  num1            4   38[BANK0 ] unsigned long 
;;  count           4   34[BANK0 ] unsigned long 
;;  modulus_resu    4    0        unsigned long 
;;  divided_resu    4    0        unsigned long 
;;  adc_millivol    3   46[BANK0 ] float 
;;  adc_result      2   52[BANK0 ] unsigned short 
;;  adc_all         2    0        unsigned short 
;;  ptr_array       2    0        PTR unsigned char 
;;  trip_value      2    0        unsigned short 
;;  watchdog        1   51[BANK0 ] unsigned char 
;;  xor_num1        1   50[BANK0 ] unsigned char 
;;  i               1   49[BANK0 ] unsigned char 
;;  xor_result      1    0        unsigned char 
;;  xor_num2        1    0        unsigned char 
;;  adc_high        1    0        unsigned char 
;;  ptr_variable    1    0        unsigned char 
;; Return value:  Size  Location     Type
;;                  2  866[COMMON] int 
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, btemp+1, pclath, cstack
;; Tracked objects:
;;		On entry : 17F/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0      20       0       0       0
;;      Temps:          0       2       0       0       0
;;      Totals:         0      22       0       0       0
;;Total ram usage:       22 bytes
;; Hardware stack levels required when called:    4
;; This function calls:
;;		_sfreg
;;		_adc_init
;;		_temperature_state_machine
;;		_temperature_set
;;		_humidity_state_machine
;;		_humidity_set
;;		_eep_clear
;;		_adc_convert
;;		_adc_bargraph
;;		_eeprom_write_block
;;		_adc_volt_convert
;;		___lwtoft
;;		___lbtoft
;;		___ftmul
;; This function is called by:
;;		Startup code after reset
;; This function uses a non-reentrant model
;;
psect	maintext
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\A-D_PIC16f887_C_code\main.c"
	line	55
	global	__size_of_main
	__size_of_main	equ	__end_of_main-_main
	
_main:	
	opt	stack 4
; Regs used in _main: [wreg-fsr0h+status,2+status,0+btemp+1+pclath+cstack]
	line	58
	
l9773:	
;main.c: 57: uint8_t watchdog;
;main.c: 58: uint32_t count=0;
	movlw	0
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(main@count+3)
	movlw	0
	movwf	(main@count+2)
	movlw	0
	movwf	(main@count+1)
	movlw	0
	movwf	(main@count)

	line	62
;main.c: 59: uint16_t adc_result;
;main.c: 61: uint16_t trip_value;
;main.c: 62: uint32_t num1=0x13131313;
	movlw	013h
	movwf	(main@num1+3)
	movlw	013h
	movwf	(main@num1+2)
	movlw	013h
	movwf	(main@num1+1)
	movlw	013h
	movwf	(main@num1)

	line	63
;main.c: 63: uint32_t num2=0x03030303;
	movlw	03h
	movwf	(main@num2+3)
	movlw	03h
	movwf	(main@num2+2)
	movlw	03h
	movwf	(main@num2+1)
	movlw	03h
	movwf	(main@num2)

	line	66
	
l9775:	
;main.c: 64: uint32_t divided_result;
;main.c: 65: uint32_t modulus_result;
;main.c: 66: uint8_t i=0;
	clrc
	movlw	0
	btfsc	status,0
	movlw	1
	movwf	(main@i)
	line	78
	
l9777:	
;main.c: 67: uint8_t *ptr_array;
;main.c: 68: uint8_t ptr_variable;
;main.c: 69: uint16_t adc_all;
;main.c: 70: uint8_t adc_high;
;main.c: 71: float adc_millivolts;
;main.c: 72: uint8_t xor_num1;
;main.c: 73: uint8_t xor_num2;
;main.c: 74: uint8_t xor_result;
;main.c: 78: sfreg();
	fcall	_sfreg
	line	79
	
l9779:	
;main.c: 79: TMR1IE = 1;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bsf	(1120/8)^080h,(1120)&7
	line	80
	
l9781:	
;main.c: 80: ADIE = 1;
	bsf	(1126/8)^080h,(1126)&7
	line	81
	
l9783:	
;main.c: 81: PEIE = 1;
	bsf	(94/8),(94)&7
	line	82
	
l9785:	
;main.c: 82: (GIE = 1);
	bsf	(95/8),(95)&7
	line	83
	
l9787:	
;main.c: 83: adc_init();
	fcall	_adc_init
	line	84
	
l9789:	
;main.c: 84: temperature_state_machine();
	fcall	_temperature_state_machine
	line	85
	
l9791:	
;main.c: 85: temperature_set(TEMP_L4);
	movlw	(04h)
	fcall	_temperature_set
	line	86
	
l9793:	
;main.c: 86: humidity_state_machine();
	fcall	_humidity_state_machine
	line	87
	
l9795:	
;main.c: 87: humidity_set(HUMIDITY_L8);
	movlw	(08h)
	fcall	_humidity_set
	line	93
;main.c: 93: while (HTS ==0)
	goto	l867
	
l868:	
	line	94
;main.c: 94: {}
	
l867:	
	line	93
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	btfss	(1146/8)^080h,(1146)&7
	goto	u5081
	goto	u5080
u5081:
	goto	l867
u5080:
	goto	l9797
	
l869:	
	line	95
	
l9797:	
;main.c: 95: eep_clear();
	fcall	_eep_clear
	line	96
	
l9799:	
;main.c: 96: PORTC = 0xFF;
	movlw	(0FFh)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(7)	;volatile
	goto	l9801
	line	97
;main.c: 97: while(1)
	
l870:	
	line	99
	
l9801:	
;main.c: 98: {
;main.c: 99: if(TO==1)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfss	(28/8),(28)&7
	goto	u5091
	goto	u5090
u5091:
	goto	l9821
u5090:
	line	105
	
l9803:	
;main.c: 100: {
;main.c: 105: watchdog = TO;
	movlw	0
	btfsc	(28/8),(28)&7
	movlw	1
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(main@watchdog)
	line	106
	
l9805:	
;main.c: 106: temperature_state_machine();
	fcall	_temperature_state_machine
	line	107
	
l9807:	
;main.c: 107: humidity_state_machine();
	fcall	_humidity_state_machine
	line	108
	
l9809:	
;main.c: 108: adc_result = adc_convert();
	fcall	_adc_convert
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(1+(?_adc_convert)),w
	clrf	(main@adc_result+1)
	addwf	(main@adc_result+1)
	movf	(0+(?_adc_convert)),w
	clrf	(main@adc_result)
	addwf	(main@adc_result)

	line	109
	
l9811:	
;main.c: 109: PORTD = adc_bargraph(adc_result);
	movf	(main@adc_result+1),w
	clrf	(?_adc_bargraph+1)
	addwf	(?_adc_bargraph+1)
	movf	(main@adc_result),w
	clrf	(?_adc_bargraph)
	addwf	(?_adc_bargraph)

	fcall	_adc_bargraph
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(8)	;volatile
	line	110
	
l9813:	
;main.c: 110: eeprom_write_block(0x00, 2, &adc_result);
	movlw	(02h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(?_eeprom_write_block)
	movlw	(main@adc_result)&0ffh
	movwf	(??_main+1)+0
	movf	(??_main+1)+0,w
	movwf	(0+?_eeprom_write_block+01h)
	movlw	(0)
	fcall	_eeprom_write_block
	line	111
	
l9815:	
;main.c: 111: adc_millivolts = adc_volt_convert();
	fcall	_adc_volt_convert
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(1+(?_adc_volt_convert)),w
	clrf	(?___lwtoft+1)
	addwf	(?___lwtoft+1)
	movf	(0+(?_adc_volt_convert)),w
	clrf	(?___lwtoft)
	addwf	(?___lwtoft)

	fcall	___lwtoft
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(0+(?___lwtoft)),w
	movwf	(main@adc_millivolts)
	movf	(1+(?___lwtoft)),w
	movwf	(main@adc_millivolts+1)
	movf	(2+(?___lwtoft)),w
	movwf	(main@adc_millivolts+2)
	line	112
;main.c: 112: xor_num1 = 0x0A^0x08;
	movlw	(02h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(main@xor_num1)
	line	115
	
l9817:	
;main.c: 115: adc_millivolts = (((PORTD)*(.00488))*(1000));
	movlw	0x29
	movwf	(?___ftmul)
	movlw	0x9c
	movwf	(?___ftmul+1)
	movlw	0x40
	movwf	(?___ftmul+2)
	movf	(8),w	;volatile
	fcall	___lbtoft
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(0+(?___lbtoft)),w
	movwf	0+(?___ftmul)+03h
	movf	(1+(?___lbtoft)),w
	movwf	1+(?___ftmul)+03h
	movf	(2+(?___lbtoft)),w
	movwf	2+(?___ftmul)+03h
	fcall	___ftmul
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(0+(?___ftmul)),w
	movwf	(main@adc_millivolts)
	movf	(1+(?___ftmul)),w
	movwf	(main@adc_millivolts+1)
	movf	(2+(?___ftmul)),w
	movwf	(main@adc_millivolts+2)
	line	127
	
l9819:	
# 127 "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\A-D_PIC16f887_C_code\main.c"
clrwdt ;#
psect	maintext
	line	128
;main.c: 128: }
	goto	l9801
	line	129
	
l871:	
	line	131
	
l9821:	
;main.c: 129: else
;main.c: 130: {
;main.c: 131: watchdog = TO;
	movlw	0
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfsc	(28/8),(28)&7
	movlw	1
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(main@watchdog)
	line	132
;main.c: 132: PORTD = persistent_watch_dog_count++;
	movf	(_persistent_watch_dog_count),w
	movwf	(8)	;volatile
	movlw	low(01h)
	addwf	(_persistent_watch_dog_count),f
	skipnc
	incf	(_persistent_watch_dog_count+1),f
	movlw	high(01h)
	addwf	(_persistent_watch_dog_count+1),f
	line	133
	
l9823:	
# 133 "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\A-D_PIC16f887_C_code\main.c"
clrwdt ;#
psect	maintext
	goto	l9801
	line	134
	
l872:	
	goto	l9801
	line	135
	
l873:	
	line	97
	goto	l9801
	
l874:	
	line	136
	
l875:	
	global	start
	ljmp	start
	opt stack 0
GLOBAL	__end_of_main
	__end_of_main:
;; =============== function _main ends ============

	signat	_main,90
	global	___lwtoft
psect	text805,local,class=CODE,delta=2
global __ptext805
__ptext805:

;; *************** function ___lwtoft *****************
;; Defined at:
;;		line 29 in file "C:\Program Files (x86)\HI-TECH Software\PICC\9.80\sources\lwtoft.c"
;; Parameters:    Size  Location     Type
;;  c               2   25[BANK0 ] unsigned int 
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;                  3   25[BANK0 ] float 
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       3       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       1       0       0       0
;;      Totals:         0       4       0       0       0
;;Total ram usage:        4 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		___ftpack
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text805
	file	"C:\Program Files (x86)\HI-TECH Software\PICC\9.80\sources\lwtoft.c"
	line	29
	global	__size_of___lwtoft
	__size_of___lwtoft	equ	__end_of___lwtoft-___lwtoft
	
___lwtoft:	
	opt	stack 4
; Regs used in ___lwtoft: [wreg+status,2+status,0+pclath+cstack]
	line	30
	
l9769:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(___lwtoft@c),w
	movwf	(?___ftpack)
	movf	(___lwtoft@c+1),w
	movwf	(?___ftpack+1)
	clrf	(?___ftpack+2)
	movlw	(08Eh)
	movwf	(??___lwtoft+0)+0
	movf	(??___lwtoft+0)+0,w
	movwf	0+(?___ftpack)+03h
	clrc
	movlw	0
	btfsc	status,0
	movlw	1
	movwf	0+(?___ftpack)+04h
	fcall	___ftpack
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(0+(?___ftpack)),w
	movwf	(?___lwtoft)
	movf	(1+(?___ftpack)),w
	movwf	(?___lwtoft+1)
	movf	(2+(?___ftpack)),w
	movwf	(?___lwtoft+2)
	goto	l7172
	
l9771:	
	line	31
	
l7172:	
	return
	opt stack 0
GLOBAL	__end_of___lwtoft
	__end_of___lwtoft:
;; =============== function ___lwtoft ends ============

	signat	___lwtoft,4219
	global	___lbtoft
psect	text806,local,class=CODE,delta=2
global __ptext806
__ptext806:

;; *************** function ___lbtoft *****************
;; Defined at:
;;		line 28 in file "C:\Program Files (x86)\HI-TECH Software\PICC\9.80\sources\lbtoft.c"
;; Parameters:    Size  Location     Type
;;  c               1    wreg     unsigned char 
;; Auto vars:     Size  Location     Type
;;  c               1   15[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;                  3    8[BANK0 ] float 
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       3       0       0       0
;;      Locals:         0       1       0       0       0
;;      Temps:          0       4       0       0       0
;;      Totals:         0       8       0       0       0
;;Total ram usage:        8 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		___ftpack
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text806
	file	"C:\Program Files (x86)\HI-TECH Software\PICC\9.80\sources\lbtoft.c"
	line	28
	global	__size_of___lbtoft
	__size_of___lbtoft	equ	__end_of___lbtoft-___lbtoft
	
___lbtoft:	
	opt	stack 4
; Regs used in ___lbtoft: [wreg+status,2+status,0+pclath+cstack]
;___lbtoft@c stored from wreg
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(___lbtoft@c)
	line	29
	
l9765:	
	movf	(___lbtoft@c),w
	movwf	((??___lbtoft+0)+0)
	clrf	((??___lbtoft+0)+0+1)
	clrf	((??___lbtoft+0)+0+2)
	movf	0+(??___lbtoft+0)+0,w
	movwf	(?___ftpack)
	movf	1+(??___lbtoft+0)+0,w
	movwf	(?___ftpack+1)
	movf	2+(??___lbtoft+0)+0,w
	movwf	(?___ftpack+2)
	movlw	(08Eh)
	movwf	(??___lbtoft+3)+0
	movf	(??___lbtoft+3)+0,w
	movwf	0+(?___ftpack)+03h
	clrc
	movlw	0
	btfsc	status,0
	movlw	1
	movwf	0+(?___ftpack)+04h
	fcall	___ftpack
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(0+(?___ftpack)),w
	movwf	(?___lbtoft)
	movf	(1+(?___ftpack)),w
	movwf	(?___lbtoft+1)
	movf	(2+(?___ftpack)),w
	movwf	(?___lbtoft+2)
	goto	l7074
	
l9767:	
	line	30
	
l7074:	
	return
	opt stack 0
GLOBAL	__end_of___lbtoft
	__end_of___lbtoft:
;; =============== function ___lbtoft ends ============

	signat	___lbtoft,4219
	global	___ftmul
psect	text807,local,class=CODE,delta=2
global __ptext807
__ptext807:

;; *************** function ___ftmul *****************
;; Defined at:
;;		line 52 in file "C:\Program Files (x86)\HI-TECH Software\PICC\9.80\sources\ftmul.c"
;; Parameters:    Size  Location     Type
;;  f1              3   16[BANK0 ] float 
;;  f2              3   19[BANK0 ] float 
;; Auto vars:     Size  Location     Type
;;  f3_as_produc    3   27[BANK0 ] unsigned um
;;  sign            1   31[BANK0 ] unsigned char 
;;  cntr            1   30[BANK0 ] unsigned char 
;;  exp             1   26[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;                  3   16[BANK0 ] float 
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       6       0       0       0
;;      Locals:         0       6       0       0       0
;;      Temps:          0       4       0       0       0
;;      Totals:         0      16       0       0       0
;;Total ram usage:       16 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		___ftpack
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text807
	file	"C:\Program Files (x86)\HI-TECH Software\PICC\9.80\sources\ftmul.c"
	line	52
	global	__size_of___ftmul
	__size_of___ftmul	equ	__end_of___ftmul-___ftmul
	
___ftmul:	
	opt	stack 4
; Regs used in ___ftmul: [wreg+status,2+status,0+pclath+cstack]
	line	56
	
l9715:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(___ftmul@f1),w
	movwf	((??___ftmul+0)+0)
	movf	(___ftmul@f1+1),w
	movwf	((??___ftmul+0)+0+1)
	movf	(___ftmul@f1+2),w
	movwf	((??___ftmul+0)+0+2)
	clrc
	rlf	(??___ftmul+0)+1,w
	rlf	(??___ftmul+0)+2,w
	movwf	(??___ftmul+3)+0
	movf	(??___ftmul+3)+0,w
	movwf	(___ftmul@exp)
	movf	((___ftmul@exp)),f
	skipz
	goto	u4941
	goto	u4940
u4941:
	goto	l9721
u4940:
	line	57
	
l9717:	
	movlw	0x0
	movwf	(?___ftmul)
	movlw	0x0
	movwf	(?___ftmul+1)
	movlw	0x0
	movwf	(?___ftmul+2)
	goto	l7048
	
l9719:	
	goto	l7048
	
l7047:	
	line	58
	
l9721:	
	movf	(___ftmul@f2),w
	movwf	((??___ftmul+0)+0)
	movf	(___ftmul@f2+1),w
	movwf	((??___ftmul+0)+0+1)
	movf	(___ftmul@f2+2),w
	movwf	((??___ftmul+0)+0+2)
	clrc
	rlf	(??___ftmul+0)+1,w
	rlf	(??___ftmul+0)+2,w
	movwf	(??___ftmul+3)+0
	movf	(??___ftmul+3)+0,w
	movwf	(___ftmul@sign)
	movf	((___ftmul@sign)),f
	skipz
	goto	u4951
	goto	u4950
u4951:
	goto	l9727
u4950:
	line	59
	
l9723:	
	movlw	0x0
	movwf	(?___ftmul)
	movlw	0x0
	movwf	(?___ftmul+1)
	movlw	0x0
	movwf	(?___ftmul+2)
	goto	l7048
	
l9725:	
	goto	l7048
	
l7049:	
	line	60
	
l9727:	
	movf	(___ftmul@sign),w
	addlw	07Bh
	movwf	(??___ftmul+0)+0
	movf	(??___ftmul+0)+0,w
	addwf	(___ftmul@exp),f
	line	61
	movf	(___ftmul@f1),w
	movwf	((??___ftmul+0)+0)
	movf	(___ftmul@f1+1),w
	movwf	((??___ftmul+0)+0+1)
	movf	(___ftmul@f1+2),w
	movwf	((??___ftmul+0)+0+2)
	movlw	010h
u4965:
	clrc
	rrf	(??___ftmul+0)+2,f
	rrf	(??___ftmul+0)+1,f
	rrf	(??___ftmul+0)+0,f
u4960:
	addlw	-1
	skipz
	goto	u4965
	movf	0+(??___ftmul+0)+0,w
	movwf	(??___ftmul+3)+0
	movf	(??___ftmul+3)+0,w
	movwf	(___ftmul@sign)
	line	62
	movf	(___ftmul@f2),w
	movwf	((??___ftmul+0)+0)
	movf	(___ftmul@f2+1),w
	movwf	((??___ftmul+0)+0+1)
	movf	(___ftmul@f2+2),w
	movwf	((??___ftmul+0)+0+2)
	movlw	010h
u4975:
	clrc
	rrf	(??___ftmul+0)+2,f
	rrf	(??___ftmul+0)+1,f
	rrf	(??___ftmul+0)+0,f
u4970:
	addlw	-1
	skipz
	goto	u4975
	movf	0+(??___ftmul+0)+0,w
	movwf	(??___ftmul+3)+0
	movf	(??___ftmul+3)+0,w
	xorwf	(___ftmul@sign),f
	line	63
	movlw	(080h)
	movwf	(??___ftmul+0)+0
	movf	(??___ftmul+0)+0,w
	andwf	(___ftmul@sign),f
	line	64
	
l9729:	
	bsf	(___ftmul@f1)+(15/8),(15)&7
	line	66
	
l9731:	
	bsf	(___ftmul@f2)+(15/8),(15)&7
	line	67
	
l9733:	
	movlw	0FFh
	andwf	(___ftmul@f2),f
	movlw	0FFh
	andwf	(___ftmul@f2+1),f
	movlw	0
	andwf	(___ftmul@f2+2),f
	line	68
	
l9735:	
	movlw	0
	movwf	(___ftmul@f3_as_product)
	movlw	0
	movwf	(___ftmul@f3_as_product+1)
	movlw	0
	movwf	(___ftmul@f3_as_product+2)
	line	69
	
l9737:	
	movlw	(07h)
	movwf	(??___ftmul+0)+0
	movf	(??___ftmul+0)+0,w
	movwf	(___ftmul@cntr)
	goto	l9739
	line	70
	
l7050:	
	line	71
	
l9739:	
	btfss	(___ftmul@f1),(0)&7
	goto	u4981
	goto	u4980
u4981:
	goto	l9743
u4980:
	line	72
	
l9741:	
	movf	(___ftmul@f2),w
	addwf	(___ftmul@f3_as_product),f
	movf	(___ftmul@f2+1),w
	clrz
	skipnc
	incf	(___ftmul@f2+1),w
	skipnz
	goto	u4991
	addwf	(___ftmul@f3_as_product+1),f
u4991:
	movf	(___ftmul@f2+2),w
	clrz
	skipnc
	incf	(___ftmul@f2+2),w
	skipnz
	goto	u4992
	addwf	(___ftmul@f3_as_product+2),f
u4992:

	goto	l9743
	
l7051:	
	line	73
	
l9743:	
	movlw	01h
u5005:
	clrc
	rrf	(___ftmul@f1+2),f
	rrf	(___ftmul@f1+1),f
	rrf	(___ftmul@f1),f
	addlw	-1
	skipz
	goto	u5005

	line	74
	
l9745:	
	movlw	01h
u5015:
	clrc
	rlf	(___ftmul@f2),f
	rlf	(___ftmul@f2+1),f
	rlf	(___ftmul@f2+2),f
	addlw	-1
	skipz
	goto	u5015
	line	75
	
l9747:	
	movlw	low(01h)
	subwf	(___ftmul@cntr),f
	btfss	status,2
	goto	u5021
	goto	u5020
u5021:
	goto	l9739
u5020:
	goto	l9749
	
l7052:	
	line	76
	
l9749:	
	movlw	(09h)
	movwf	(??___ftmul+0)+0
	movf	(??___ftmul+0)+0,w
	movwf	(___ftmul@cntr)
	goto	l9751
	line	77
	
l7053:	
	line	78
	
l9751:	
	btfss	(___ftmul@f1),(0)&7
	goto	u5031
	goto	u5030
u5031:
	goto	l9755
u5030:
	line	79
	
l9753:	
	movf	(___ftmul@f2),w
	addwf	(___ftmul@f3_as_product),f
	movf	(___ftmul@f2+1),w
	clrz
	skipnc
	incf	(___ftmul@f2+1),w
	skipnz
	goto	u5041
	addwf	(___ftmul@f3_as_product+1),f
u5041:
	movf	(___ftmul@f2+2),w
	clrz
	skipnc
	incf	(___ftmul@f2+2),w
	skipnz
	goto	u5042
	addwf	(___ftmul@f3_as_product+2),f
u5042:

	goto	l9755
	
l7054:	
	line	80
	
l9755:	
	movlw	01h
u5055:
	clrc
	rrf	(___ftmul@f1+2),f
	rrf	(___ftmul@f1+1),f
	rrf	(___ftmul@f1),f
	addlw	-1
	skipz
	goto	u5055

	line	81
	
l9757:	
	movlw	01h
u5065:
	clrc
	rrf	(___ftmul@f3_as_product+2),f
	rrf	(___ftmul@f3_as_product+1),f
	rrf	(___ftmul@f3_as_product),f
	addlw	-1
	skipz
	goto	u5065

	line	82
	
l9759:	
	movlw	low(01h)
	subwf	(___ftmul@cntr),f
	btfss	status,2
	goto	u5071
	goto	u5070
u5071:
	goto	l9751
u5070:
	goto	l9761
	
l7055:	
	line	83
	
l9761:	
	movf	(___ftmul@f3_as_product),w
	movwf	(?___ftpack)
	movf	(___ftmul@f3_as_product+1),w
	movwf	(?___ftpack+1)
	movf	(___ftmul@f3_as_product+2),w
	movwf	(?___ftpack+2)
	movf	(___ftmul@exp),w
	movwf	(??___ftmul+0)+0
	movf	(??___ftmul+0)+0,w
	movwf	0+(?___ftpack)+03h
	movf	(___ftmul@sign),w
	movwf	(??___ftmul+1)+0
	movf	(??___ftmul+1)+0,w
	movwf	0+(?___ftpack)+04h
	fcall	___ftpack
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(0+(?___ftpack)),w
	movwf	(?___ftmul)
	movf	(1+(?___ftpack)),w
	movwf	(?___ftmul+1)
	movf	(2+(?___ftpack)),w
	movwf	(?___ftmul+2)
	goto	l7048
	
l9763:	
	line	84
	
l7048:	
	return
	opt stack 0
GLOBAL	__end_of___ftmul
	__end_of___ftmul:
;; =============== function ___ftmul ends ============

	signat	___ftmul,8315
	global	_adc_volt_convert
psect	text808,local,class=CODE,delta=2
global __ptext808
__ptext808:

;; *************** function _adc_volt_convert *****************
;; Defined at:
;;		line 58 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\A-D_PIC16f887_C_code\adc_func.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  adc_millivol    2   23[BANK0 ] unsigned short 
;;  adc_millivol    2   21[BANK0 ] unsigned short 
;;  adc_rawvalue    2   19[BANK0 ] unsigned short 
;; Return value:  Size  Location     Type
;;                  2   13[BANK0 ] unsigned short 
;; Registers used:
;;		wreg, status,2, status,0, btemp+1, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       2       0       0       0
;;      Locals:         0       6       0       0       0
;;      Temps:          0       4       0       0       0
;;      Totals:         0      12       0       0       0
;;Total ram usage:       12 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		_adc_convert
;;		___lmul
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text808
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\A-D_PIC16f887_C_code\adc_func.c"
	line	58
	global	__size_of_adc_volt_convert
	__size_of_adc_volt_convert	equ	__end_of_adc_volt_convert-_adc_volt_convert
	
_adc_volt_convert:	
	opt	stack 4
; Regs used in _adc_volt_convert: [wreg+status,2+status,0+btemp+1+pclath+cstack]
	line	62
	
l9701:	
;adc_func.c: 60: uint16_t adc_rawvalue;
;adc_func.c: 61: uint16_t adc_millivolt_macroval;
;adc_func.c: 62: uint16_t adc_millivolt=0;
	movlw	low(0)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(adc_volt_convert@adc_millivolt)
	movlw	high(0)
	movwf	((adc_volt_convert@adc_millivolt))+1
	line	64
	
l9703:	
;adc_func.c: 64: adc_rawvalue = adc_convert();
	fcall	_adc_convert
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(1+(?_adc_convert)),w
	clrf	(adc_volt_convert@adc_rawvalue+1)
	addwf	(adc_volt_convert@adc_rawvalue+1)
	movf	(0+(?_adc_convert)),w
	clrf	(adc_volt_convert@adc_rawvalue)
	addwf	(adc_volt_convert@adc_rawvalue)

	line	65
	
l9705:	
;adc_func.c: 65: adc_millivolt_macroval = ((uint16_t)(((uint32_t)(adc_rawvalue)*(uint32_t)((5000))) >> (10)));
	movf	(adc_volt_convert@adc_rawvalue),w
	movwf	(?___lmul)
	movf	(adc_volt_convert@adc_rawvalue+1),w
	movwf	((?___lmul))+1
	clrf	2+((?___lmul))
	clrf	3+((?___lmul))
	movlw	0
	movwf	3+(?___lmul)+04h
	movlw	0
	movwf	2+(?___lmul)+04h
	movlw	013h
	movwf	1+(?___lmul)+04h
	movlw	088h
	movwf	0+(?___lmul)+04h

	fcall	___lmul
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(0+?___lmul),w
	movwf	(??_adc_volt_convert+0)+0
	movf	(1+?___lmul),w
	movwf	((??_adc_volt_convert+0)+0+1)
	movf	(2+?___lmul),w
	movwf	((??_adc_volt_convert+0)+0+2)
	movf	(3+?___lmul),w
	movwf	((??_adc_volt_convert+0)+0+3)
	movlw	0Ah
u4935:
	clrc
	rrf	(??_adc_volt_convert+0)+3,f
	rrf	(??_adc_volt_convert+0)+2,f
	rrf	(??_adc_volt_convert+0)+1,f
	rrf	(??_adc_volt_convert+0)+0,f
u4930:
	addlw	-1
	skipz
	goto	u4935
	movf	1+(??_adc_volt_convert+0)+0,w
	clrf	(adc_volt_convert@adc_millivolt_macroval+1)
	addwf	(adc_volt_convert@adc_millivolt_macroval+1)
	movf	0+(??_adc_volt_convert+0)+0,w
	clrf	(adc_volt_convert@adc_millivolt_macroval)
	addwf	(adc_volt_convert@adc_millivolt_macroval)

	line	66
	
l9707:	
;adc_func.c: 66: adc_millivolt = adc_millivolt_macroval;
	movf	(adc_volt_convert@adc_millivolt_macroval+1),w
	clrf	(adc_volt_convert@adc_millivolt+1)
	addwf	(adc_volt_convert@adc_millivolt+1)
	movf	(adc_volt_convert@adc_millivolt_macroval),w
	clrf	(adc_volt_convert@adc_millivolt)
	addwf	(adc_volt_convert@adc_millivolt)

	line	68
	
l9709:	
# 68 "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\A-D_PIC16f887_C_code\adc_func.c"
nop ;#
psect	text808
	line	69
	
l9711:	
;adc_func.c: 69: return adc_millivolt;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(adc_volt_convert@adc_millivolt+1),w
	clrf	(?_adc_volt_convert+1)
	addwf	(?_adc_volt_convert+1)
	movf	(adc_volt_convert@adc_millivolt),w
	clrf	(?_adc_volt_convert)
	addwf	(?_adc_volt_convert)

	goto	l1725
	
l9713:	
	line	71
	
l1725:	
	return
	opt stack 0
GLOBAL	__end_of_adc_volt_convert
	__end_of_adc_volt_convert:
;; =============== function _adc_volt_convert ends ============

	signat	_adc_volt_convert,90
	global	_eeprom_write_block
psect	text809,local,class=CODE,delta=2
global __ptext809
__ptext809:

;; *************** function _eeprom_write_block *****************
;; Defined at:
;;		line 72 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\A-D_PIC16f887_C_code\eeprom.c"
;; Parameters:    Size  Location     Type
;;  address         1    wreg     unsigned char 
;;  size            1    3[BANK0 ] unsigned char 
;;  buffer          1    4[BANK0 ] PTR unsigned char 
;;		 -> main@adc_result(2), 
;; Auto vars:     Size  Location     Type
;;  address         1    7[BANK0 ] unsigned char 
;;  i               1    6[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       2       0       0       0
;;      Locals:         0       2       0       0       0
;;      Temps:          0       1       0       0       0
;;      Totals:         0       5       0       0       0
;;Total ram usage:        5 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		_eeprom_write
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text809
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\A-D_PIC16f887_C_code\eeprom.c"
	line	72
	global	__size_of_eeprom_write_block
	__size_of_eeprom_write_block	equ	__end_of_eeprom_write_block-_eeprom_write_block
	
_eeprom_write_block:	
	opt	stack 4
; Regs used in _eeprom_write_block: [wreg-fsr0h+status,2+status,0+pclath+cstack]
;eeprom_write_block@address stored from wreg
	line	76
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(eeprom_write_block@address)
	
l9687:	
;eeprom.c: 74: uint8_t i;
;eeprom.c: 76: if (size <= (16))
	movlw	(011h)
	subwf	(eeprom_write_block@size),w
	skipnc
	goto	u4911
	goto	u4910
u4911:
	goto	l6884
u4910:
	line	78
	
l9689:	
;eeprom.c: 77: {
;eeprom.c: 78: for(i=0;i<=size;i++)
	clrc
	movlw	0
	btfsc	status,0
	movlw	1
	movwf	(eeprom_write_block@i)
	goto	l9699
	line	79
	
l6881:	
	line	80
	
l9691:	
;eeprom.c: 79: {
;eeprom.c: 80: eeprom_write(address, * buffer);
	movf	(eeprom_write_block@buffer),w
	movwf	fsr0
	bcf	status, 7	;select IRP bank0
	movf	indf,w
	movwf	(??_eeprom_write_block+0)+0
	movf	(??_eeprom_write_block+0)+0,w
	movwf	(?_eeprom_write)
	movf	(eeprom_write_block@address),w
	fcall	_eeprom_write
	line	81
	
l9693:	
;eeprom.c: 81: buffer++;
	movlw	(01h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_eeprom_write_block+0)+0
	movf	(??_eeprom_write_block+0)+0,w
	addwf	(eeprom_write_block@buffer),f
	line	82
	
l9695:	
;eeprom.c: 82: address = address + 1;
	movf	(eeprom_write_block@address),w
	addlw	01h
	movwf	(??_eeprom_write_block+0)+0
	movf	(??_eeprom_write_block+0)+0,w
	movwf	(eeprom_write_block@address)
	line	78
	
l9697:	
	movlw	(01h)
	movwf	(??_eeprom_write_block+0)+0
	movf	(??_eeprom_write_block+0)+0,w
	addwf	(eeprom_write_block@i),f
	goto	l9699
	
l6880:	
	
l9699:	
	movf	(eeprom_write_block@i),w
	subwf	(eeprom_write_block@size),w
	skipnc
	goto	u4921
	goto	u4920
u4921:
	goto	l9691
u4920:
	goto	l6884
	
l6882:	
	line	84
;eeprom.c: 83: }
;eeprom.c: 84: }
	goto	l6884
	line	85
	
l6879:	
	goto	l6884
	line	88
;eeprom.c: 85: else
;eeprom.c: 86: {
	
l6883:	
	line	89
	
l6884:	
	return
	opt stack 0
GLOBAL	__end_of_eeprom_write_block
	__end_of_eeprom_write_block:
;; =============== function _eeprom_write_block ends ============

	signat	_eeprom_write_block,12408
	global	_eep_clear
psect	text810,local,class=CODE,delta=2
global __ptext810
__ptext810:

;; *************** function _eep_clear *****************
;; Defined at:
;;		line 36 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\A-D_PIC16f887_C_code\eeprom.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  i               1    4[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       1       0       0       0
;;      Temps:          0       1       0       0       0
;;      Totals:         0       2       0       0       0
;;Total ram usage:        2 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		_eeprom_write
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text810
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\A-D_PIC16f887_C_code\eeprom.c"
	line	36
	global	__size_of_eep_clear
	__size_of_eep_clear	equ	__end_of_eep_clear-_eep_clear
	
_eep_clear:	
	opt	stack 4
; Regs used in _eep_clear: [wreg+status,2+status,0+pclath+cstack]
	line	39
	
l9675:	
;eeprom.c: 38: uint8_t i;
;eeprom.c: 39: for(i=0;i<=254;i++)
	clrc
	movlw	0
	btfsc	status,0
	movlw	1
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(eep_clear@i)
	
l9677:	
	movf	(eep_clear@i),w
	xorlw	0FFh
	skipz
	goto	u4891
	goto	u4890
u4891:
	goto	l9681
u4890:
	goto	l6871
	
l9679:	
	goto	l6871
	line	40
	
l6869:	
	line	41
	
l9681:	
;eeprom.c: 40: {
;eeprom.c: 41: eeprom_write(i,0x00);
	clrc
	movlw	0
	btfsc	status,0
	movlw	1
	movwf	(?_eeprom_write)
	movf	(eep_clear@i),w
	fcall	_eeprom_write
	line	39
	
l9683:	
	movlw	(01h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_eep_clear+0)+0
	movf	(??_eep_clear+0)+0,w
	addwf	(eep_clear@i),f
	
l9685:	
	movf	(eep_clear@i),w
	xorlw	0FFh
	skipz
	goto	u4901
	goto	u4900
u4901:
	goto	l9681
u4900:
	goto	l6871
	
l6870:	
	line	43
	
l6871:	
	return
	opt stack 0
GLOBAL	__end_of_eep_clear
	__end_of_eep_clear:
;; =============== function _eep_clear ends ============

	signat	_eep_clear,88
	global	_humidity_state_machine
psect	text811,local,class=CODE,delta=2
global __ptext811
__ptext811:

;; *************** function _humidity_state_machine *****************
;; Defined at:
;;		line 26 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\A-D_PIC16f887_C_code\humidity.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       1       0       0       0
;;      Totals:         0       1       0       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		_timer_set
;;		_get_timer
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text811
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\A-D_PIC16f887_C_code\humidity.c"
	line	26
	global	__size_of_humidity_state_machine
	__size_of_humidity_state_machine	equ	__end_of_humidity_state_machine-_humidity_state_machine
	
_humidity_state_machine:	
	opt	stack 4
; Regs used in _humidity_state_machine: [wreg-fsr0h+status,2+status,0+pclath+cstack]
	line	29
	
l9639:	
;humidity.c: 27: static uint8_t led_state = HUMIDITY_LED_DEFAULT;
;humidity.c: 29: if (led_state == HUMIDITY_LED_WAIT)
	movf	(humidity_state_machine@led_state),w
	xorlw	02h
	skipz
	goto	u4821
	goto	u4820
u4821:
	goto	l9651
u4820:
	line	31
	
l9641:	
;humidity.c: 30: {
;humidity.c: 31: if (loops != 0)
	movf	(humidity@loops),w
	skipz
	goto	u4830
	goto	l6021
u4830:
	line	33
	
l9643:	
;humidity.c: 32: {
;humidity.c: 33: PORTB |= 0x04;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	bsf	(6)+(2/8),(2)&7	;volatile
	line	34
	
l9645:	
;humidity.c: 34: timer_set(TIMER_2, 500);
	movlw	low(01F4h)
	movwf	(?_timer_set)
	movlw	high(01F4h)
	movwf	((?_timer_set))+1
	movlw	(01h)
	fcall	_timer_set
	line	35
	
l9647:	
;humidity.c: 35: led_state = HUMIDITY_LED_ON;
	clrf	(humidity_state_machine@led_state)
	bsf	status,0
	rlf	(humidity_state_machine@led_state),f
	line	36
	
l9649:	
;humidity.c: 36: toms_variable = 1;
	movlw	(01h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_humidity_state_machine+0)+0
	movf	(??_humidity_state_machine+0)+0,w
	movwf	(_toms_variable)	;volatile
	goto	l6021
	line	37
	
l6012:	
	line	38
;humidity.c: 37: }
;humidity.c: 38: }
	goto	l6021
	line	39
	
l6011:	
	
l9651:	
;humidity.c: 39: else if (led_state == HUMIDITY_LED_ON)
	movf	(humidity_state_machine@led_state),w
	xorlw	01h
	skipz
	goto	u4841
	goto	u4840
u4841:
	goto	l9661
u4840:
	line	41
	
l9653:	
;humidity.c: 40: {
;humidity.c: 41: if (get_timer(TIMER_2) == 0)
	movlw	(01h)
	fcall	_get_timer
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	((1+(?_get_timer))),w
	iorwf	((0+(?_get_timer))),w
	skipz
	goto	u4851
	goto	u4850
u4851:
	goto	l6021
u4850:
	line	43
	
l9655:	
;humidity.c: 42: {
;humidity.c: 43: PORTB &= 0xFB;
	movlw	(0FBh)
	movwf	(??_humidity_state_machine+0)+0
	movf	(??_humidity_state_machine+0)+0,w
	andwf	(6),f	;volatile
	line	45
	
l9657:	
;humidity.c: 45: timer_set(TIMER_2, 500);
	movlw	low(01F4h)
	movwf	(?_timer_set)
	movlw	high(01F4h)
	movwf	((?_timer_set))+1
	movlw	(01h)
	fcall	_timer_set
	line	46
	
l9659:	
;humidity.c: 46: led_state = HUMIDITY_LED_OFF;
	clrc
	movlw	0
	btfsc	status,0
	movlw	1
	movwf	(humidity_state_machine@led_state)
	goto	l6021
	line	47
	
l6015:	
	line	48
;humidity.c: 47: }
;humidity.c: 48: }
	goto	l6021
	line	49
	
l6014:	
	
l9661:	
;humidity.c: 49: else if (led_state == HUMIDITY_LED_OFF)
	movf	(humidity_state_machine@led_state),f
	skipz
	goto	u4861
	goto	u4860
u4861:
	goto	l9671
u4860:
	line	51
	
l9663:	
;humidity.c: 50: {
;humidity.c: 51: if (get_timer(TIMER_2) == 0)
	movlw	(01h)
	fcall	_get_timer
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	((1+(?_get_timer))),w
	iorwf	((0+(?_get_timer))),w
	skipz
	goto	u4871
	goto	u4870
u4871:
	goto	l6021
u4870:
	line	53
	
l9665:	
;humidity.c: 52: {
;humidity.c: 53: if (loops != 0)
	movf	(humidity@loops),w
	skipz
	goto	u4880
	goto	l9669
u4880:
	line	55
	
l9667:	
;humidity.c: 54: {
;humidity.c: 55: loops--;
	movlw	low(01h)
	subwf	(humidity@loops),f
	goto	l9669
	line	56
	
l6019:	
	line	57
	
l9669:	
;humidity.c: 56: }
;humidity.c: 57: led_state = HUMIDITY_LED_WAIT;
	movlw	(02h)
	movwf	(??_humidity_state_machine+0)+0
	movf	(??_humidity_state_machine+0)+0,w
	movwf	(humidity_state_machine@led_state)
	goto	l6021
	line	58
	
l6018:	
	line	59
;humidity.c: 58: }
;humidity.c: 59: }
	goto	l6021
	line	60
	
l6017:	
	line	63
	
l9671:	
;humidity.c: 60: else
;humidity.c: 61: {
;humidity.c: 63: PORTB = 0x00;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(6)	;volatile
	line	64
	
l9673:	
;humidity.c: 64: led_state = HUMIDITY_LED_WAIT;
	movlw	(02h)
	movwf	(??_humidity_state_machine+0)+0
	movf	(??_humidity_state_machine+0)+0,w
	movwf	(humidity_state_machine@led_state)
	goto	l6021
	line	65
	
l6020:	
	goto	l6021
	
l6016:	
	goto	l6021
	
l6013:	
	line	66
	
l6021:	
	return
	opt stack 0
GLOBAL	__end_of_humidity_state_machine
	__end_of_humidity_state_machine:
;; =============== function _humidity_state_machine ends ============

	signat	_humidity_state_machine,88
	global	_temperature_state_machine
psect	text812,local,class=CODE,delta=2
global __ptext812
__ptext812:

;; *************** function _temperature_state_machine *****************
;; Defined at:
;;		line 26 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\A-D_PIC16f887_C_code\temperature.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       1       0       0       0
;;      Totals:         0       1       0       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		_timer_set
;;		_get_timer
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text812
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\A-D_PIC16f887_C_code\temperature.c"
	line	26
	global	__size_of_temperature_state_machine
	__size_of_temperature_state_machine	equ	__end_of_temperature_state_machine-_temperature_state_machine
	
_temperature_state_machine:	
	opt	stack 4
; Regs used in _temperature_state_machine: [wreg-fsr0h+status,2+status,0+pclath+cstack]
	line	29
	
l9605:	
;temperature.c: 27: static uint8_t led_state = TEMP_LED_DEFAULT;
;temperature.c: 29: if (led_state == TEMP_LED_WAIT)
	movf	(temperature_state_machine@led_state),w
	xorlw	02h
	skipz
	goto	u4751
	goto	u4750
u4751:
	goto	l9615
u4750:
	line	32
	
l9607:	
;temperature.c: 30: {
;temperature.c: 32: if (loops != 0)
	movf	(_loops),w
	skipz
	goto	u4760
	goto	l5162
u4760:
	line	34
	
l9609:	
;temperature.c: 33: {
;temperature.c: 34: PORTB |= 0x02;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	bsf	(6)+(1/8),(1)&7	;volatile
	line	35
	
l9611:	
;temperature.c: 35: timer_set(TIMER_1, 500);
	movlw	low(01F4h)
	movwf	(?_timer_set)
	movlw	high(01F4h)
	movwf	((?_timer_set))+1
	movlw	(0)
	fcall	_timer_set
	line	36
	
l9613:	
;temperature.c: 36: led_state = TEMP_LED_ON;
	clrf	(temperature_state_machine@led_state)
	bsf	status,0
	rlf	(temperature_state_machine@led_state),f
	goto	l5162
	line	38
	
l5153:	
	line	39
;temperature.c: 38: }
;temperature.c: 39: }
	goto	l5162
	line	40
	
l5152:	
	
l9615:	
;temperature.c: 40: else if (led_state == TEMP_LED_ON)
	movf	(temperature_state_machine@led_state),w
	xorlw	01h
	skipz
	goto	u4771
	goto	u4770
u4771:
	goto	l9625
u4770:
	line	42
	
l9617:	
;temperature.c: 41: {
;temperature.c: 42: if (get_timer(TIMER_1) == 0)
	movlw	(0)
	fcall	_get_timer
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	((1+(?_get_timer))),w
	iorwf	((0+(?_get_timer))),w
	skipz
	goto	u4781
	goto	u4780
u4781:
	goto	l5162
u4780:
	line	44
	
l9619:	
;temperature.c: 43: {
;temperature.c: 44: PORTB &= 0xFD;
	movlw	(0FDh)
	movwf	(??_temperature_state_machine+0)+0
	movf	(??_temperature_state_machine+0)+0,w
	andwf	(6),f	;volatile
	line	46
	
l9621:	
;temperature.c: 46: timer_set(TIMER_1, 500);
	movlw	low(01F4h)
	movwf	(?_timer_set)
	movlw	high(01F4h)
	movwf	((?_timer_set))+1
	movlw	(0)
	fcall	_timer_set
	line	47
	
l9623:	
;temperature.c: 47: led_state = TEMP_LED_OFF;
	clrc
	movlw	0
	btfsc	status,0
	movlw	1
	movwf	(temperature_state_machine@led_state)
	goto	l5162
	line	48
	
l5156:	
	line	49
;temperature.c: 48: }
;temperature.c: 49: }
	goto	l5162
	line	50
	
l5155:	
	
l9625:	
;temperature.c: 50: else if (led_state == TEMP_LED_OFF)
	movf	(temperature_state_machine@led_state),f
	skipz
	goto	u4791
	goto	u4790
u4791:
	goto	l9635
u4790:
	line	52
	
l9627:	
;temperature.c: 51: {
;temperature.c: 52: if (get_timer(TIMER_1) == 0)
	movlw	(0)
	fcall	_get_timer
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	((1+(?_get_timer))),w
	iorwf	((0+(?_get_timer))),w
	skipz
	goto	u4801
	goto	u4800
u4801:
	goto	l5162
u4800:
	line	54
	
l9629:	
;temperature.c: 53: {
;temperature.c: 54: if (loops != 0)
	movf	(_loops),w
	skipz
	goto	u4810
	goto	l9633
u4810:
	line	56
	
l9631:	
;temperature.c: 55: {
;temperature.c: 56: loops--;
	movlw	low(01h)
	subwf	(_loops),f
	goto	l9633
	line	57
	
l5160:	
	line	58
	
l9633:	
;temperature.c: 57: }
;temperature.c: 58: led_state = TEMP_LED_WAIT;
	movlw	(02h)
	movwf	(??_temperature_state_machine+0)+0
	movf	(??_temperature_state_machine+0)+0,w
	movwf	(temperature_state_machine@led_state)
	goto	l5162
	line	59
	
l5159:	
	line	60
;temperature.c: 59: }
;temperature.c: 60: }
	goto	l5162
	line	61
	
l5158:	
	line	63
	
l9635:	
;temperature.c: 61: else
;temperature.c: 62: {
;temperature.c: 63: PORTB = 0x00;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(6)	;volatile
	line	64
	
l9637:	
;temperature.c: 64: led_state = TEMP_LED_WAIT;
	movlw	(02h)
	movwf	(??_temperature_state_machine+0)+0
	movf	(??_temperature_state_machine+0)+0,w
	movwf	(temperature_state_machine@led_state)
	goto	l5162
	line	65
	
l5161:	
	goto	l5162
	
l5157:	
	goto	l5162
	
l5154:	
	line	66
	
l5162:	
	return
	opt stack 0
GLOBAL	__end_of_temperature_state_machine
	__end_of_temperature_state_machine:
;; =============== function _temperature_state_machine ends ============

	signat	_temperature_state_machine,88
	global	___lmul
psect	text813,local,class=CODE,delta=2
global __ptext813
__ptext813:

;; *************** function ___lmul *****************
;; Defined at:
;;		line 3 in file "C:\Program Files (x86)\HI-TECH Software\PICC\9.80\sources\lmul.c"
;; Parameters:    Size  Location     Type
;;  multiplier      4    0[BANK0 ] unsigned long 
;;  multiplicand    4    4[BANK0 ] unsigned long 
;; Auto vars:     Size  Location     Type
;;  product         4    9[BANK0 ] unsigned long 
;; Return value:  Size  Location     Type
;;                  4    0[BANK0 ] unsigned long 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       8       0       0       0
;;      Locals:         0       4       0       0       0
;;      Temps:          0       1       0       0       0
;;      Totals:         0      13       0       0       0
;;Total ram usage:       13 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_adc_volt_convert
;; This function uses a non-reentrant model
;;
psect	text813
	file	"C:\Program Files (x86)\HI-TECH Software\PICC\9.80\sources\lmul.c"
	line	3
	global	__size_of___lmul
	__size_of___lmul	equ	__end_of___lmul-___lmul
	
___lmul:	
	opt	stack 4
; Regs used in ___lmul: [wreg+status,2+status,0]
	line	4
	
l9483:	
	movlw	0
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(___lmul@product+3)
	movlw	0
	movwf	(___lmul@product+2)
	movlw	0
	movwf	(___lmul@product+1)
	movlw	0
	movwf	(___lmul@product)

	goto	l9485
	line	6
	
l7024:	
	line	7
	
l9485:	
	btfss	(___lmul@multiplier),(0)&7
	goto	u4541
	goto	u4540
u4541:
	goto	l9489
u4540:
	line	8
	
l9487:	
	movf	(___lmul@multiplicand),w
	addwf	(___lmul@product),f
	movf	(___lmul@multiplicand+1),w
	clrz
	skipnc
	addlw	1
	skipnz
	goto	u4551
	addwf	(___lmul@product+1),f
u4551:
	movf	(___lmul@multiplicand+2),w
	clrz
	skipnc
	addlw	1
	skipnz
	goto	u4552
	addwf	(___lmul@product+2),f
u4552:
	movf	(___lmul@multiplicand+3),w
	clrz
	skipnc
	addlw	1
	skipnz
	goto	u4553
	addwf	(___lmul@product+3),f
u4553:

	goto	l9489
	
l7025:	
	line	9
	
l9489:	
	movlw	01h
	movwf	(??___lmul+0)+0
u4565:
	clrc
	rlf	(___lmul@multiplicand),f
	rlf	(___lmul@multiplicand+1),f
	rlf	(___lmul@multiplicand+2),f
	rlf	(___lmul@multiplicand+3),f
	decfsz	(??___lmul+0)+0
	goto	u4565
	line	10
	
l9491:	
	movlw	01h
u4575:
	clrc
	rrf	(___lmul@multiplier+3),f
	rrf	(___lmul@multiplier+2),f
	rrf	(___lmul@multiplier+1),f
	rrf	(___lmul@multiplier),f
	addlw	-1
	skipz
	goto	u4575

	line	11
	movf	(___lmul@multiplier+3),w
	iorwf	(___lmul@multiplier+2),w
	iorwf	(___lmul@multiplier+1),w
	iorwf	(___lmul@multiplier),w
	skipz
	goto	u4581
	goto	u4580
u4581:
	goto	l9485
u4580:
	goto	l9493
	
l7026:	
	line	12
	
l9493:	
	movf	(___lmul@product+3),w
	movwf	(?___lmul+3)
	movf	(___lmul@product+2),w
	movwf	(?___lmul+2)
	movf	(___lmul@product+1),w
	movwf	(?___lmul+1)
	movf	(___lmul@product),w
	movwf	(?___lmul)

	goto	l7027
	
l9495:	
	line	13
	
l7027:	
	return
	opt stack 0
GLOBAL	__end_of___lmul
	__end_of___lmul:
;; =============== function ___lmul ends ============

	signat	___lmul,8316
	global	___ftpack
psect	text814,local,class=CODE,delta=2
global __ptext814
__ptext814:

;; *************** function ___ftpack *****************
;; Defined at:
;;		line 63 in file "C:\Program Files (x86)\HI-TECH Software\PICC\9.80\sources\float.c"
;; Parameters:    Size  Location     Type
;;  arg             3    0[BANK0 ] unsigned um
;;  exp             1    3[BANK0 ] unsigned char 
;;  sign            1    4[BANK0 ] unsigned char 
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;                  3    0[BANK0 ] float 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       5       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       3       0       0       0
;;      Totals:         0       8       0       0       0
;;Total ram usage:        8 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		___ftmul
;;		___lbtoft
;;		___lwtoft
;;		___ftadd
;;		___ftdiv
;;		___abtoft
;;		___awtoft
;;		___altoft
;;		___lltoft
;;		___attoft
;;		___lttoft
;; This function uses a non-reentrant model
;;
psect	text814
	file	"C:\Program Files (x86)\HI-TECH Software\PICC\9.80\sources\float.c"
	line	63
	global	__size_of___ftpack
	__size_of___ftpack	equ	__end_of___ftpack-___ftpack
	
___ftpack:	
	opt	stack 4
; Regs used in ___ftpack: [wreg+status,2+status,0]
	line	64
	
l9453:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(___ftpack@exp),w
	skipz
	goto	u4430
	goto	l9457
u4430:
	
l9455:	
	movf	(___ftpack@arg+2),w
	iorwf	(___ftpack@arg+1),w
	iorwf	(___ftpack@arg),w
	skipz
	goto	u4441
	goto	u4440
u4441:
	goto	l9463
u4440:
	goto	l9457
	
l7283:	
	line	65
	
l9457:	
	movlw	0x0
	movwf	(?___ftpack)
	movlw	0x0
	movwf	(?___ftpack+1)
	movlw	0x0
	movwf	(?___ftpack+2)
	goto	l7284
	
l9459:	
	goto	l7284
	
l7281:	
	line	66
	goto	l9463
	
l7286:	
	line	67
	
l9461:	
	movlw	(01h)
	movwf	(??___ftpack+0)+0
	movf	(??___ftpack+0)+0,w
	addwf	(___ftpack@exp),f
	line	68
	movlw	01h
u4455:
	clrc
	rrf	(___ftpack@arg+2),f
	rrf	(___ftpack@arg+1),f
	rrf	(___ftpack@arg),f
	addlw	-1
	skipz
	goto	u4455

	goto	l9463
	line	69
	
l7285:	
	line	66
	
l9463:	
	movlw	low highword(0FE0000h)
	andwf	(___ftpack@arg+2),w
	btfss	status,2
	goto	u4461
	goto	u4460
u4461:
	goto	l9461
u4460:
	goto	l7288
	
l7287:	
	line	70
	goto	l7288
	
l7289:	
	line	71
	
l9465:	
	movlw	(01h)
	movwf	(??___ftpack+0)+0
	movf	(??___ftpack+0)+0,w
	addwf	(___ftpack@exp),f
	line	72
	
l9467:	
	movlw	01h
	addwf	(___ftpack@arg),f
	movlw	0
	skipnc
movlw 1
	addwf	(___ftpack@arg+1),f
	movlw	0
	skipnc
movlw 1
	addwf	(___ftpack@arg+2),f
	line	73
	
l9469:	
	movlw	01h
u4475:
	clrc
	rrf	(___ftpack@arg+2),f
	rrf	(___ftpack@arg+1),f
	rrf	(___ftpack@arg),f
	addlw	-1
	skipz
	goto	u4475

	line	74
	
l7288:	
	line	70
	movlw	low highword(0FF0000h)
	andwf	(___ftpack@arg+2),w
	btfss	status,2
	goto	u4481
	goto	u4480
u4481:
	goto	l9465
u4480:
	goto	l9473
	
l7290:	
	line	75
	goto	l9473
	
l7292:	
	line	76
	
l9471:	
	movlw	low(01h)
	subwf	(___ftpack@exp),f
	line	77
	movlw	01h
u4495:
	clrc
	rlf	(___ftpack@arg),f
	rlf	(___ftpack@arg+1),f
	rlf	(___ftpack@arg+2),f
	addlw	-1
	skipz
	goto	u4495
	goto	l9473
	line	78
	
l7291:	
	line	75
	
l9473:	
	btfss	(___ftpack@arg+1),(15)&7
	goto	u4501
	goto	u4500
u4501:
	goto	l9471
u4500:
	
l7293:	
	line	79
	btfsc	(___ftpack@exp),(0)&7
	goto	u4511
	goto	u4510
u4511:
	goto	l7294
u4510:
	line	80
	
l9475:	
	movlw	0FFh
	andwf	(___ftpack@arg),f
	movlw	07Fh
	andwf	(___ftpack@arg+1),f
	movlw	0FFh
	andwf	(___ftpack@arg+2),f
	
l7294:	
	line	81
	clrc
	rrf	(___ftpack@exp),f

	line	82
	
l9477:	
	movf	(___ftpack@exp),w
	movwf	((??___ftpack+0)+0)
	clrf	((??___ftpack+0)+0+1)
	clrf	((??___ftpack+0)+0+2)
	movlw	010h
u4525:
	clrc
	rlf	(??___ftpack+0)+0,f
	rlf	(??___ftpack+0)+1,f
	rlf	(??___ftpack+0)+2,f
u4520:
	addlw	-1
	skipz
	goto	u4525
	movf	0+(??___ftpack+0)+0,w
	iorwf	(___ftpack@arg),f
	movf	1+(??___ftpack+0)+0,w
	iorwf	(___ftpack@arg+1),f
	movf	2+(??___ftpack+0)+0,w
	iorwf	(___ftpack@arg+2),f
	line	83
	
l9479:	
	movf	(___ftpack@sign),w
	skipz
	goto	u4530
	goto	l7295
u4530:
	line	84
	
l9481:	
	bsf	(___ftpack@arg)+(23/8),(23)&7
	
l7295:	
	line	85
	line	86
	
l7284:	
	return
	opt stack 0
GLOBAL	__end_of___ftpack
	__end_of___ftpack:
;; =============== function ___ftpack ends ============

	signat	___ftpack,12411
	global	_eeprom_write
psect	text815,local,class=CODE,delta=2
global __ptext815
__ptext815:

;; *************** function _eeprom_write *****************
;; Defined at:
;;		line 8 in file "C:\Program Files (x86)\HI-TECH Software\PICC\9.80\sources\eewrite.c"
;; Parameters:    Size  Location     Type
;;  addr            1    wreg     unsigned char 
;;  value           1    0[BANK0 ] unsigned char 
;; Auto vars:     Size  Location     Type
;;  addr            1    2[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       1       0       0       0
;;      Locals:         0       1       0       0       0
;;      Temps:          0       1       0       0       0
;;      Totals:         0       3       0       0       0
;;Total ram usage:        3 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_eep_clear
;;		_eeprom_write_block
;;		_eep_default
;; This function uses a non-reentrant model
;;
psect	text815
	file	"C:\Program Files (x86)\HI-TECH Software\PICC\9.80\sources\eewrite.c"
	line	8
	global	__size_of_eeprom_write
	__size_of_eeprom_write	equ	__end_of_eeprom_write-_eeprom_write
	
_eeprom_write:	
	opt	stack 4
; Regs used in _eeprom_write: [wreg+status,2+status,0]
;eeprom_write@addr stored from wreg
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(eeprom_write@addr)
	line	9
	
l6933:	
	goto	l6934
	
l6935:	
	
l6934:	
	bsf	status, 5	;RP0=1, select bank3
	bsf	status, 6	;RP1=1, select bank3
	btfsc	(3169/8)^0180h,(3169)&7
	goto	u4161
	goto	u4160
u4161:
	goto	l6934
u4160:
	goto	l9361
	
l6936:	
	
l9361:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(eeprom_write@addr),w
	bcf	status, 5	;RP0=0, select bank2
	bsf	status, 6	;RP1=1, select bank2
	movwf	(269)^0100h	;volatile
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(eeprom_write@value),w
	bcf	status, 5	;RP0=0, select bank2
	bsf	status, 6	;RP1=1, select bank2
	movwf	(268)^0100h	;volatile
	
l9363:	
	movlw	(03Fh)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_eeprom_write+0)+0
	movf	(??_eeprom_write+0)+0,w
	bsf	status, 5	;RP0=1, select bank3
	bsf	status, 6	;RP1=1, select bank3
	andwf	(396)^0180h,f	;volatile
	
l9365:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	bcf	(24/8),(24)&7
	
l9367:	
	btfss	(95/8),(95)&7
	goto	u4171
	goto	u4170
u4171:
	goto	l6937
u4170:
	
l9369:	
	bsf	(24/8),(24)&7
	
l6937:	
	bcf	(95/8),(95)&7
	bsf	status, 5	;RP0=1, select bank3
	bsf	status, 6	;RP1=1, select bank3
	bsf	(3170/8)^0180h,(3170)&7
	
l9371:	
	movlw	(055h)
	movwf	(397)^0180h	;volatile
	movlw	(0AAh)
	movwf	(397)^0180h	;volatile
	
l9373:	
	bsf	(3169/8)^0180h,(3169)&7
	
l9375:	
	bcf	(3170/8)^0180h,(3170)&7
	
l9377:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfss	(24/8),(24)&7
	goto	u4181
	goto	u4180
u4181:
	goto	l6940
u4180:
	
l9379:	
	bsf	(95/8),(95)&7
	goto	l6940
	
l6938:	
	goto	l6940
	
l6939:	
	line	10
	
l6940:	
	return
	opt stack 0
GLOBAL	__end_of_eeprom_write
	__end_of_eeprom_write:
;; =============== function _eeprom_write ends ============

	signat	_eeprom_write,8312
	global	_get_timer
psect	text816,local,class=CODE,delta=2
global __ptext816
__ptext816:

;; *************** function _get_timer *****************
;; Defined at:
;;		line 33 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\A-D_PIC16f887_C_code\timer.c"
;; Parameters:    Size  Location     Type
;;  index           1    wreg     unsigned char 
;; Auto vars:     Size  Location     Type
;;  index           1    5[BANK0 ] unsigned char 
;;  result          2    3[BANK0 ] unsigned short 
;; Return value:  Size  Location     Type
;;                  2    0[BANK0 ] unsigned short 
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       2       0       0       0
;;      Locals:         0       3       0       0       0
;;      Temps:          0       1       0       0       0
;;      Totals:         0       6       0       0       0
;;Total ram usage:        6 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_temperature_state_machine
;;		_humidity_state_machine
;; This function uses a non-reentrant model
;;
psect	text816
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\A-D_PIC16f887_C_code\timer.c"
	line	33
	global	__size_of_get_timer
	__size_of_get_timer	equ	__end_of_get_timer-_get_timer
	
_get_timer:	
	opt	stack 4
; Regs used in _get_timer: [wreg-fsr0h+status,2+status,0]
;get_timer@index stored from wreg
	line	36
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(get_timer@index)
	
l9335:	
;timer.c: 34: uint16_t result;
;timer.c: 36: if (index < TIMER_MAX)
	movlw	(02h)
	subwf	(get_timer@index),w
	skipnc
	goto	u4121
	goto	u4120
u4121:
	goto	l9343
u4120:
	line	38
	
l9337:	
;timer.c: 37: {
;timer.c: 38: (GIE = 0);
	bcf	(95/8),(95)&7
	line	39
	
l9339:	
;timer.c: 39: result = timer_array[index];
	movf	(get_timer@index),w
	movwf	(??_get_timer+0)+0
	addwf	(??_get_timer+0)+0,w
	addlw	_timer_array&0ffh
	movwf	fsr0
	bcf	status, 7	;select IRP bank0
	movf	indf,w
	movwf	(get_timer@result)
	incf	fsr0,f
	movf	indf,w
	movwf	(get_timer@result+1)
	line	40
	
l9341:	
;timer.c: 40: (GIE = 1);
	bsf	(95/8),(95)&7
	line	41
;timer.c: 41: }
	goto	l9345
	line	42
	
l3447:	
	line	44
	
l9343:	
;timer.c: 42: else
;timer.c: 43: {
;timer.c: 44: result = 0;
	movlw	low(0)
	movwf	(get_timer@result)
	movlw	high(0)
	movwf	((get_timer@result))+1
	goto	l9345
	line	45
	
l3448:	
	line	47
	
l9345:	
;timer.c: 45: }
;timer.c: 47: return result;
	movf	(get_timer@result+1),w
	clrf	(?_get_timer+1)
	addwf	(?_get_timer+1)
	movf	(get_timer@result),w
	clrf	(?_get_timer)
	addwf	(?_get_timer)

	goto	l3449
	
l9347:	
	line	49
	
l3449:	
	return
	opt stack 0
GLOBAL	__end_of_get_timer
	__end_of_get_timer:
;; =============== function _get_timer ends ============

	signat	_get_timer,4218
	global	_timer_set
psect	text817,local,class=CODE,delta=2
global __ptext817
__ptext817:

;; *************** function _timer_set *****************
;; Defined at:
;;		line 18 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\A-D_PIC16f887_C_code\timer.c"
;; Parameters:    Size  Location     Type
;;  index           1    wreg     unsigned char 
;;  value           2    0[BANK0 ] unsigned short 
;; Auto vars:     Size  Location     Type
;;  index           1    3[BANK0 ] unsigned char 
;;  array_conten    2    0        unsigned short 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       2       0       0       0
;;      Locals:         0       1       0       0       0
;;      Temps:          0       1       0       0       0
;;      Totals:         0       4       0       0       0
;;Total ram usage:        4 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_temperature_state_machine
;;		_humidity_state_machine
;; This function uses a non-reentrant model
;;
psect	text817
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\A-D_PIC16f887_C_code\timer.c"
	line	18
	global	__size_of_timer_set
	__size_of_timer_set	equ	__end_of_timer_set-_timer_set
	
_timer_set:	
	opt	stack 4
; Regs used in _timer_set: [wreg-fsr0h+status,2+status,0]
;timer_set@index stored from wreg
	line	20
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(timer_set@index)
	
l9327:	
;timer.c: 19: uint16_t array_contents;
;timer.c: 20: if (index < TIMER_MAX)
	movlw	(02h)
	subwf	(timer_set@index),w
	skipnc
	goto	u4111
	goto	u4110
u4111:
	goto	l3444
u4110:
	line	22
	
l9329:	
;timer.c: 21: {
;timer.c: 22: (GIE = 0);
	bcf	(95/8),(95)&7
	line	23
	
l9331:	
;timer.c: 23: timer_array[index] = value;
	movf	(timer_set@index),w
	movwf	(??_timer_set+0)+0
	addwf	(??_timer_set+0)+0,w
	addlw	_timer_array&0ffh
	movwf	fsr0
	movf	(timer_set@value),w
	bcf	status, 7	;select IRP bank0
	movwf	indf
	incf	fsr0,f
	movf	(timer_set@value+1),w
	movwf	indf
	line	24
	
l9333:	
;timer.c: 24: (GIE = 1);
	bsf	(95/8),(95)&7
	line	25
;timer.c: 25: }
	goto	l3444
	line	26
	
l3442:	
	goto	l3444
	line	28
;timer.c: 26: else
;timer.c: 27: {
	
l3443:	
	line	29
	
l3444:	
	return
	opt stack 0
GLOBAL	__end_of_timer_set
	__end_of_timer_set:
;; =============== function _timer_set ends ============

	signat	_timer_set,8312
	global	_adc_bargraph
psect	text818,local,class=CODE,delta=2
global __ptext818
__ptext818:

;; *************** function _adc_bargraph *****************
;; Defined at:
;;		line 148 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\A-D_PIC16f887_C_code\adc_func.c"
;; Parameters:    Size  Location     Type
;;  value           2    0[BANK0 ] unsigned short 
;; Auto vars:     Size  Location     Type
;;  result          1    5[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       2       0       0       0
;;      Locals:         0       1       0       0       0
;;      Temps:          0       3       0       0       0
;;      Totals:         0       6       0       0       0
;;Total ram usage:        6 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text818
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\A-D_PIC16f887_C_code\adc_func.c"
	line	148
	global	__size_of_adc_bargraph
	__size_of_adc_bargraph	equ	__end_of_adc_bargraph-_adc_bargraph
	
_adc_bargraph:	
	opt	stack 5
; Regs used in _adc_bargraph: [wreg-fsr0h+status,2+status,0]
	line	151
	
l9317:	
;adc_func.c: 149: uint8_t result;
;adc_func.c: 151: if (value > (0x3FF))
	movlw	high(0400h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	subwf	(adc_bargraph@value+1),w
	movlw	low(0400h)
	skipnz
	subwf	(adc_bargraph@value),w
	skipc
	goto	u4091
	goto	u4090
u4091:
	goto	l9321
u4090:
	line	153
	
l9319:	
;adc_func.c: 152: {
;adc_func.c: 153: value = (0x3FF);
	movlw	low(03FFh)
	movwf	(adc_bargraph@value)
	movlw	high(03FFh)
	movwf	((adc_bargraph@value))+1
	goto	l9321
	line	154
	
l1747:	
	line	156
	
l9321:	
;adc_func.c: 154: }
;adc_func.c: 156: result = adc_bargraph_array[value >> ((10) - 4)];
	movf	(adc_bargraph@value+1),w
	movwf	(??_adc_bargraph+0)+0+1
	movf	(adc_bargraph@value),w
	movwf	(??_adc_bargraph+0)+0
	movlw	06h
u4105:
	clrc
	rrf	(??_adc_bargraph+0)+1,f
	rrf	(??_adc_bargraph+0)+0,f
	addlw	-1
	skipz
	goto	u4105
	movf	0+(??_adc_bargraph+0)+0,w
	addlw	_adc_bargraph_array&0ffh
	movwf	fsr0
	bcf	status, 7	;select IRP bank1
	movf	indf,w
	movwf	(??_adc_bargraph+2)+0
	movf	(??_adc_bargraph+2)+0,w
	movwf	(adc_bargraph@result)
	line	158
	
l9323:	
;adc_func.c: 158: return result;
	movf	(adc_bargraph@result),w
	goto	l1748
	
l9325:	
	line	159
	
l1748:	
	return
	opt stack 0
GLOBAL	__end_of_adc_bargraph
	__end_of_adc_bargraph:
;; =============== function _adc_bargraph ends ============

	signat	_adc_bargraph,4217
	global	_adc_convert
psect	text819,local,class=CODE,delta=2
global __ptext819
__ptext819:

;; *************** function _adc_convert *****************
;; Defined at:
;;		line 75 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\A-D_PIC16f887_C_code\adc_func.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  result          2    8[BANK0 ] unsigned short 
;;  adresl          2    6[BANK0 ] unsigned short 
;;  adresh          2    4[BANK0 ] unsigned short 
;; Return value:  Size  Location     Type
;;                  2    0[BANK0 ] unsigned short 
;; Registers used:
;;		wreg, status,2, status,0, btemp+1
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       2       0       0       0
;;      Locals:         0       6       0       0       0
;;      Temps:          0       2       0       0       0
;;      Totals:         0      10       0       0       0
;;Total ram usage:       10 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;;		_adc_volt_convert
;; This function uses a non-reentrant model
;;
psect	text819
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\A-D_PIC16f887_C_code\adc_func.c"
	line	75
	global	__size_of_adc_convert
	__size_of_adc_convert	equ	__end_of_adc_convert-_adc_convert
	
_adc_convert:	
	opt	stack 4
; Regs used in _adc_convert: [wreg+status,2+status,0+btemp+1]
	line	79
	
l9303:	
;adc_func.c: 76: uint16_t adresh;
;adc_func.c: 77: uint16_t adresl;
;adc_func.c: 78: uint16_t result;
;adc_func.c: 79: adc_isr_flag = 0;
	movlw	low(0)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(_adc_isr_flag)
	movlw	high(0)
	movwf	((_adc_isr_flag))+1
	line	81
	
l9305:	
;adc_func.c: 81: GODONE = 1;
	bsf	(249/8),(249)&7
	line	83
;adc_func.c: 83: while (adc_isr_flag == 0)
	goto	l9307
	
l1729:	
	goto	l9307
	line	84
;adc_func.c: 84: {}
	
l1728:	
	line	83
	
l9307:	
	movf	(_adc_isr_flag+1),w
	iorwf	(_adc_isr_flag),w
	skipnz
	goto	u4071
	goto	u4070
u4071:
	goto	l9307
u4070:
	goto	l9309
	
l1730:	
	line	85
	
l9309:	
;adc_func.c: 85: adresl = ADRESL;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movf	(158)^080h,w	;volatile
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_adc_convert+0)+0
	clrf	(??_adc_convert+0)+0+1
	movf	0+(??_adc_convert+0)+0,w
	movwf	(adc_convert@adresl)
	movf	1+(??_adc_convert+0)+0,w
	movwf	(adc_convert@adresl+1)
	line	86
;adc_func.c: 86: adresh = ADRESH;
	movf	(30),w	;volatile
	movwf	(??_adc_convert+0)+0
	clrf	(??_adc_convert+0)+0+1
	movf	0+(??_adc_convert+0)+0,w
	movwf	(adc_convert@adresh)
	movf	1+(??_adc_convert+0)+0,w
	movwf	(adc_convert@adresh+1)
	line	87
	
l9311:	
;adc_func.c: 87: result = ((adresh << 8) |(adresl));
	movf	(adc_convert@adresh+1),w
	movwf	(??_adc_convert+0)+0+1
	movf	(adc_convert@adresh),w
	movwf	(??_adc_convert+0)+0
	movlw	08h
	movwf	btemp+1
u4085:
	clrc
	rlf	(??_adc_convert+0)+0,f
	rlf	(??_adc_convert+0)+1,f
	decfsz	btemp+1,f
	goto	u4085
	movf	(adc_convert@adresl),w
	iorwf	0+(??_adc_convert+0)+0,w
	movwf	(adc_convert@result)
	movf	(adc_convert@adresl+1),w
	iorwf	1+(??_adc_convert+0)+0,w
	movwf	1+(adc_convert@result)
	line	89
	
l9313:	
;adc_func.c: 89: return result;
	movf	(adc_convert@result+1),w
	clrf	(?_adc_convert+1)
	addwf	(?_adc_convert+1)
	movf	(adc_convert@result),w
	clrf	(?_adc_convert)
	addwf	(?_adc_convert)

	goto	l1731
	
l9315:	
	line	90
	
l1731:	
	return
	opt stack 0
GLOBAL	__end_of_adc_convert
	__end_of_adc_convert:
;; =============== function _adc_convert ends ============

	signat	_adc_convert,90
	global	_humidity_set
psect	text820,local,class=CODE,delta=2
global __ptext820
__ptext820:

;; *************** function _humidity_set *****************
;; Defined at:
;;		line 18 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\A-D_PIC16f887_C_code\humidity.c"
;; Parameters:    Size  Location     Type
;;  set_loops       1    wreg     unsigned char 
;; Auto vars:     Size  Location     Type
;;  set_loops       1    1[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       1       0       0       0
;;      Temps:          0       1       0       0       0
;;      Totals:         0       2       0       0       0
;;Total ram usage:        2 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text820
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\A-D_PIC16f887_C_code\humidity.c"
	line	18
	global	__size_of_humidity_set
	__size_of_humidity_set	equ	__end_of_humidity_set-_humidity_set
	
_humidity_set:	
	opt	stack 5
; Regs used in _humidity_set: [wreg+status,2+status,0]
;humidity_set@set_loops stored from wreg
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(humidity_set@set_loops)
	line	19
	
l9299:	
;humidity.c: 19: if (loops == 0)
	movf	(humidity@loops),f
	skipz
	goto	u4061
	goto	u4060
u4061:
	goto	l6006
u4060:
	line	21
	
l9301:	
;humidity.c: 20: {
;humidity.c: 21: loops = set_loops;
	movf	(humidity_set@set_loops),w
	movwf	(??_humidity_set+0)+0
	movf	(??_humidity_set+0)+0,w
	movwf	(humidity@loops)
	goto	l6006
	line	22
	
l6005:	
	line	23
	
l6006:	
	return
	opt stack 0
GLOBAL	__end_of_humidity_set
	__end_of_humidity_set:
;; =============== function _humidity_set ends ============

	signat	_humidity_set,4216
	global	_temperature_set
psect	text821,local,class=CODE,delta=2
global __ptext821
__ptext821:

;; *************** function _temperature_set *****************
;; Defined at:
;;		line 18 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\A-D_PIC16f887_C_code\temperature.c"
;; Parameters:    Size  Location     Type
;;  set_loops       1    wreg     unsigned char 
;; Auto vars:     Size  Location     Type
;;  set_loops       1    1[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       1       0       0       0
;;      Temps:          0       1       0       0       0
;;      Totals:         0       2       0       0       0
;;Total ram usage:        2 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text821
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\A-D_PIC16f887_C_code\temperature.c"
	line	18
	global	__size_of_temperature_set
	__size_of_temperature_set	equ	__end_of_temperature_set-_temperature_set
	
_temperature_set:	
	opt	stack 5
; Regs used in _temperature_set: [wreg+status,2+status,0]
;temperature_set@set_loops stored from wreg
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(temperature_set@set_loops)
	line	19
	
l9295:	
;temperature.c: 19: if (loops == 0)
	movf	(_loops),f
	skipz
	goto	u4051
	goto	u4050
u4051:
	goto	l5147
u4050:
	line	21
	
l9297:	
;temperature.c: 20: {
;temperature.c: 21: loops = set_loops;
	movf	(temperature_set@set_loops),w
	movwf	(??_temperature_set+0)+0
	movf	(??_temperature_set+0)+0,w
	movwf	(_loops)
	goto	l5147
	line	22
	
l5146:	
	line	23
	
l5147:	
	return
	opt stack 0
GLOBAL	__end_of_temperature_set
	__end_of_temperature_set:
;; =============== function _temperature_set ends ============

	signat	_temperature_set,4216
	global	_adc_init
psect	text822,local,class=CODE,delta=2
global __ptext822
__ptext822:

;; *************** function _adc_init *****************
;; Defined at:
;;		line 176 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\A-D_PIC16f887_C_code\adc_func.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text822
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\A-D_PIC16f887_C_code\adc_func.c"
	line	176
	global	__size_of_adc_init
	__size_of_adc_init	equ	__end_of_adc_init-_adc_init
	
_adc_init:	
	opt	stack 5
; Regs used in _adc_init: [wreg]
	line	177
	
l9293:	
;adc_func.c: 177: ADCON0 = 0xC1;
	movlw	(0C1h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(31)	;volatile
	line	178
;adc_func.c: 178: ADCON1 = 0x80;
	movlw	(080h)
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movwf	(159)^080h	;volatile
	line	179
	
l1755:	
	return
	opt stack 0
GLOBAL	__end_of_adc_init
	__end_of_adc_init:
;; =============== function _adc_init ends ============

	signat	_adc_init,88
	global	_sfreg
psect	text823,local,class=CODE,delta=2
global __ptext823
__ptext823:

;; *************** function _sfreg *****************
;; Defined at:
;;		line 139 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\A-D_PIC16f887_C_code\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       1       0       0       0
;;      Totals:         0       1       0       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text823
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\A-D_PIC16f887_C_code\main.c"
	line	139
	global	__size_of_sfreg
	__size_of_sfreg	equ	__end_of_sfreg-_sfreg
	
_sfreg:	
	opt	stack 5
; Regs used in _sfreg: [wreg+status,2+status,0]
	line	140
	
l9279:	
;main.c: 140: WDTCON = 0x17;
	movlw	(017h)
	bcf	status, 5	;RP0=0, select bank2
	bsf	status, 6	;RP1=1, select bank2
	movwf	(261)^0100h	;volatile
	line	141
;main.c: 141: OSCCON = 0x61;
	movlw	(061h)
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movwf	(143)^080h	;volatile
	line	142
	
l9281:	
;main.c: 142: OPTION = 0x00;
	clrf	(129)^080h	;volatile
	line	143
	
l9283:	
;main.c: 143: TRISA = 0x01;
	movlw	(01h)
	movwf	(133)^080h	;volatile
	line	144
;main.c: 144: TRISC = 0x00;
	clrf	(135)^080h	;volatile
	line	145
;main.c: 145: TRISD = 0x00;
	clrf	(136)^080h	;volatile
	line	146
	
l9285:	
;main.c: 146: ANSEL = 0x01;
	movlw	(01h)
	bsf	status, 5	;RP0=1, select bank3
	bsf	status, 6	;RP1=1, select bank3
	movwf	(392)^0180h	;volatile
	line	147
	
l9287:	
;main.c: 147: T1CON = 0x35;
	movlw	(035h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(16)	;volatile
	line	148
	
l9289:	
;main.c: 148: TRISB = 0xF9;
	movlw	(0F9h)
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movwf	(134)^080h	;volatile
	line	149
	
l9291:	
;main.c: 149: ANSELH &= 0xFA;
	movlw	(0FAh)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_sfreg+0)+0
	movf	(??_sfreg+0)+0,w
	bsf	status, 5	;RP0=1, select bank3
	bsf	status, 6	;RP1=1, select bank3
	andwf	(393)^0180h,f	;volatile
	line	150
	
l878:	
	return
	opt stack 0
GLOBAL	__end_of_sfreg
	__end_of_sfreg:
;; =============== function _sfreg ends ============

	signat	_sfreg,88
	global	_interrupt_handler
psect	text824,local,class=CODE,delta=2
global __ptext824
__ptext824:

;; *************** function _interrupt_handler *****************
;; Defined at:
;;		line 7 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\A-D_PIC16f887_C_code\interrupt.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          4       0       0       0       0
;;      Totals:         4       0       0       0       0
;;Total ram usage:        4 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		_adc_isr
;;		_timer_isr
;; This function is called by:
;;		Interrupt level 1
;; This function uses a non-reentrant model
;;
psect	text824
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\A-D_PIC16f887_C_code\interrupt.c"
	line	7
	global	__size_of_interrupt_handler
	__size_of_interrupt_handler	equ	__end_of_interrupt_handler-_interrupt_handler
	
_interrupt_handler:	
	opt	stack 4
; Regs used in _interrupt_handler: [wreg-fsr0h+status,2+status,0+pclath+cstack]
psect	intentry,class=CODE,delta=2
global __pintentry
__pintentry:
global interrupt_function
interrupt_function:
	global saved_w
	saved_w	set	btemp+0
	movwf	saved_w
	movf	status,w
	movwf	(??_interrupt_handler+0)
	movf	fsr0,w
	movwf	(??_interrupt_handler+1)
	movf	pclath,w
	movwf	(??_interrupt_handler+2)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	btemp+1,w
	movwf	(??_interrupt_handler+3)
	ljmp	_interrupt_handler
psect	text824
	line	8
	
i1l8707:	
;interrupt.c: 8: adc_isr();
	fcall	_adc_isr
	line	9
	
i1l8709:	
;interrupt.c: 9: timer_isr();
	fcall	_timer_isr
	line	10
	
i1l2601:	
	movf	(??_interrupt_handler+3),w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	btemp+1
	movf	(??_interrupt_handler+2),w
	movwf	pclath
	movf	(??_interrupt_handler+1),w
	movwf	fsr0
	movf	(??_interrupt_handler+0),w
	movwf	status
	swapf	saved_w,f
	swapf	saved_w,w
	retfie
	opt stack 0
GLOBAL	__end_of_interrupt_handler
	__end_of_interrupt_handler:
;; =============== function _interrupt_handler ends ============

	signat	_interrupt_handler,88
	global	_timer_isr
psect	text825,local,class=CODE,delta=2
global __ptext825
__ptext825:

;; *************** function _timer_isr *****************
;; Defined at:
;;		line 54 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\A-D_PIC16f887_C_code\timer.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  i               1    1[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         1       0       0       0       0
;;      Temps:          1       0       0       0       0
;;      Totals:         2       0       0       0       0
;;Total ram usage:        2 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_interrupt_handler
;; This function uses a non-reentrant model
;;
psect	text825
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\A-D_PIC16f887_C_code\timer.c"
	line	54
	global	__size_of_timer_isr
	__size_of_timer_isr	equ	__end_of_timer_isr-_timer_isr
	
_timer_isr:	
	opt	stack 4
; Regs used in _timer_isr: [wreg-fsr0h+status,2+status,0]
	line	56
	
i1l8711:	
;timer.c: 55: uint8_t i;
;timer.c: 56: if((TMR1IE)&&(TMR1IF))
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	btfss	(1120/8)^080h,(1120)&7
	goto	u292_21
	goto	u292_20
u292_21:
	goto	i1l3458
u292_20:
	
i1l8713:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfss	(96/8),(96)&7
	goto	u293_21
	goto	u293_20
u293_21:
	goto	i1l3458
u293_20:
	line	58
	
i1l8715:	
;timer.c: 57: {
;timer.c: 58: TMR1IF=0;
	bcf	(96/8),(96)&7
	line	59
	
i1l8717:	
;timer.c: 59: T1CON &= ~(0x01);
	movlw	(0FEh)
	movwf	(??_timer_isr+0)+0
	movf	(??_timer_isr+0)+0,w
	andwf	(16),f	;volatile
	line	60
	
i1l8719:	
;timer.c: 60: TMR1L = 0x89;
	movlw	(089h)
	movwf	(14)	;volatile
	line	61
	
i1l8721:	
;timer.c: 61: TMR1H = 0xFF;
	movlw	(0FFh)
	movwf	(15)	;volatile
	line	62
	
i1l8723:	
;timer.c: 62: T1CON |= 0x01;
	bsf	(16)+(0/8),(0)&7	;volatile
	line	64
;timer.c: 64: for (i = 0; i < TIMER_MAX; i++)
	clrc
	movlw	0
	btfsc	status,0
	movlw	1
	movwf	(timer_isr@i)
	
i1l8725:	
	movlw	(02h)
	subwf	(timer_isr@i),w
	skipc
	goto	u294_21
	goto	u294_20
u294_21:
	goto	i1l8729
u294_20:
	goto	i1l3458
	
i1l8727:	
	goto	i1l3458
	line	65
	
i1l3453:	
	line	66
	
i1l8729:	
;timer.c: 65: {
;timer.c: 66: if (timer_array[i] != 0)
	movf	(timer_isr@i),w
	movwf	(??_timer_isr+0)+0
	addwf	(??_timer_isr+0)+0,w
	addlw	_timer_array&0ffh
	movwf	fsr0
	bcf	status, 7	;select IRP bank0
	movf	indf,w
	incf	fsr0,f
	iorwf	indf,w
	skipnz
	goto	u295_21
	goto	u295_20
u295_21:
	goto	i1l8733
u295_20:
	line	68
	
i1l8731:	
;timer.c: 67: {
;timer.c: 68: timer_array[i]--;
	movf	(timer_isr@i),w
	movwf	(??_timer_isr+0)+0
	addwf	(??_timer_isr+0)+0,w
	addlw	_timer_array&0ffh
	movwf	fsr0
	movlw	low(01h)
	subwf	indf,f
	incfsz	fsr0,f
	movlw	high(01h)
	skipc
	decf	indf,f
	subwf	indf,f
	decf	fsr0,f
	line	69
;timer.c: 69: }
	goto	i1l8733
	line	70
	
i1l3455:	
	goto	i1l8733
	line	72
;timer.c: 70: else
;timer.c: 71: {
	
i1l3456:	
	line	64
	
i1l8733:	
	movlw	(01h)
	movwf	(??_timer_isr+0)+0
	movf	(??_timer_isr+0)+0,w
	addwf	(timer_isr@i),f
	
i1l8735:	
	movlw	(02h)
	subwf	(timer_isr@i),w
	skipc
	goto	u296_21
	goto	u296_20
u296_21:
	goto	i1l8729
u296_20:
	goto	i1l3458
	
i1l3454:	
	line	75
;timer.c: 72: }
;timer.c: 73: }
;timer.c: 75: }
	goto	i1l3458
	line	76
	
i1l3452:	
	goto	i1l3458
	line	78
;timer.c: 76: else
;timer.c: 77: {
	
i1l3457:	
	line	79
	
i1l3458:	
	return
	opt stack 0
GLOBAL	__end_of_timer_isr
	__end_of_timer_isr:
;; =============== function _timer_isr ends ============

	signat	_timer_isr,88
	global	_adc_isr
psect	text826,local,class=CODE,delta=2
global __ptext826
__ptext826:

;; *************** function _adc_isr *****************
;; Defined at:
;;		line 182 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\A-D_PIC16f887_C_code\adc_func.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_interrupt_handler
;; This function uses a non-reentrant model
;;
psect	text826
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\A-D_PIC16f887_C_code\adc_func.c"
	line	182
	global	__size_of_adc_isr
	__size_of_adc_isr	equ	__end_of_adc_isr-_adc_isr
	
_adc_isr:	
	opt	stack 4
; Regs used in _adc_isr: [wreg]
	line	183
	
i1l8699:	
;adc_func.c: 183: if ((ADIF)&&(ADIE))
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfss	(102/8),(102)&7
	goto	u290_21
	goto	u290_20
u290_21:
	goto	i1l1760
u290_20:
	
i1l8701:	
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	btfss	(1126/8)^080h,(1126)&7
	goto	u291_21
	goto	u291_20
u291_21:
	goto	i1l1760
u291_20:
	line	186
	
i1l8703:	
;adc_func.c: 184: {
;adc_func.c: 186: ADIF=0;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	bcf	(102/8),(102)&7
	line	187
	
i1l8705:	
;adc_func.c: 187: adc_isr_flag=1;
	movlw	low(01h)
	movwf	(_adc_isr_flag)
	movlw	high(01h)
	movwf	((_adc_isr_flag))+1
	line	188
;adc_func.c: 188: int_count++;
	movlw	low(01h)
	addwf	(_int_count),f
	skipnc
	incf	(_int_count+1),f
	movlw	high(01h)
	addwf	(_int_count+1),f
	line	189
;adc_func.c: 189: }
	goto	i1l1760
	line	190
	
i1l1758:	
	goto	i1l1760
	line	192
;adc_func.c: 190: else
;adc_func.c: 191: {
	
i1l1759:	
	line	193
	
i1l1760:	
	return
	opt stack 0
GLOBAL	__end_of_adc_isr
	__end_of_adc_isr:
;; =============== function _adc_isr ends ============

	signat	_adc_isr,88
psect	text827,local,class=CODE,delta=2
global __ptext827
__ptext827:
	global	btemp
	btemp set 07Eh

	DABS	1,126,2	;btemp
	global	wtemp0
	wtemp0 set btemp
	end
