#include "humidity.h"
#include "temperature.h"
#include "htc.h"
#include "adc_func.h"
#include "timer.h"
#include "typedefs.h"
#include "math.h"
#include "eeprom.h"
#define  CAPTURE_ARRAY 42
#define  adc_volt_convert_macro(x) (((x)*(.00488))*(1000)) // conversion macro raw adc count to volts in mV
union {
      uint8_t var_a;
      uint8_t var_b;
      }union_vars;      
 
 enum
 {
  TEMP_DEFAULT,
  TEMP_L1,
  TEMP_L2,
  TEMP_L3,
  TEMP_L4
 };
 enum
{
  HUMIDITY_DEFAULT,
  HUMIDITY_L1,
  HUMIDITY_L2,
  HUMIDITY_L3,
  HUMIDITY_L4,
  HUMIDITY_L5,
  HUMIDITY_L6,
  HUMIDITY_L7,
  HUMIDITY_L8
};
  
/*PIC16F887*/
__CONFIG(DEBUGEN & LVPEN & FCMEN & IESODIS & BORXSLP & DUNPROTECT & UNPROTECT & MCLREN & PWRTEN & WDTDIS & INTCLK);//HS);
__CONFIG(BORV40);

/* Function prototypes*/
void sfreg(void);
//void state_machine(void);
/* non writeable ram variable location*/
persistent int persistent_watch_dog_count;
/* Volatile variable*/
vuint8_t tmr1_counter;      // volatile isr variable for timer 1
uint8_t temp_set_val;
//vuint8_t initial_flag;
//uint8_t  tempset_count;
 /*********************************************************************/
/*        THE MAIN LOOP                                              */         
/*********************************************************************/
int main (void)
{
  /* variables for the main*/
  uint8_t  watchdog;
  uint32_t count=0;                                   // A count of A/D reads if no wdt expired
  uint16_t adc_result;                                // low 8 bits of atd read
 // uint16_t timer1_countdown = TIMER1_VALUE;         // value for timer set function
  uint16_t trip_value;
  uint32_t num1=0x13131313;
  uint32_t num2=0x03030303;
  uint32_t divided_result;
  uint32_t modulus_result;
  uint8_t  i=0;                                       // index into array
  uint8_t *ptr_array;
  uint8_t  ptr_variable;
  uint16_t adc_all;
  uint8_t  adc_high;
  float  adc_millivolts;
  uint8_t xor_num1;
  uint8_t xor_num2;
  uint8_t xor_result;
/***********************************************************************/
/*          INITIALIZATIONS
/***********************************************************************/
  sfreg();                                            // initialize sfr's   
  TMR1IE = 1;                                         // Timer 1 interrupt enable
  ADIE = 1;                                           // AtD intrrupt enable
  PEIE = 1;                                           // enable all peripheral interrupts
  ei();                                               // enable all interrupts
  adc_init();                                         // initialize the Atd module in pic 
  temperature_state_machine();                        // Get this into Default state                                          
  temperature_set(TEMP_L4);                           // This example says to do 1 loop of off to on
  humidity_state_machine();
  humidity_set(HUMIDITY_L8);
//  persistent_watch_dog_count=0;                     // watch dog count
//  PORTD = persistent_watch_dog_count; 
/*************************************************************************/
/*           CODE WILL REMAIN IN while loop if no watch dog reset
/*************************************************************************/ 
 while (HTS ==0)
  {}                                                 // wait until clock stable 
  eep_clear();
 PORTC = 0xFF;
  while(1)
  {
    if(TO==1)                                       // No watchdog reset proceed with adc read      
    { 
      // ptr_array=&capture_array[2];
      // ptr_variable=*ptr_array;
      // (*ptr_array++);
      // ptr_variable=*ptr_array;
       watchdog = TO;
       temperature_state_machine();
       humidity_state_machine();
       adc_result = adc_convert();
       PORTD = adc_bargraph(adc_result); 
       eeprom_write_block(0x00, 2, &adc_result);           
       adc_millivolts = adc_volt_convert();
       xor_num1 = 0x0A^0x08;
       //xor_num2 = 0x08;
       //xor_result = num1 ^ num2;
       adc_millivolts = adc_volt_convert_macro(PORTD); //THIS CONVERTED RIGHT, IN HEXADECIMAL
        /*
       adc_all = (((ADRESH & 0x03) << 8)|(ADRESL));
       capture_array[i]= adc_all;
       if (i <= CAPTURE_ARRAY-1 )
           {
            i++; 
           }
       divided_result = divided(num1, num2);        //32 bit add  
       modulus_result = modulus(num1, num2);
       */

       CLRWDT();
    }
    else                                            // a watchdog reset happend update this count
    { 
      watchdog = TO;
      PORTD = persistent_watch_dog_count++;         // Keep a un-accessed RAM count of wdt 
      CLRWDT();
    }
  }
}

void sfreg(void)
{
WDTCON = 0x17;       // WDTPS3=1|WDTPS2=0|WDTPS1=1|WDTPS0=1|SWDTEN=1 5 seconds before watch dog expires
OSCCON = 0x61;       // 4 Mhz clock
OPTION = 0x00;       // |PSA=Prescale to WDT|most prescale rate|
TRISA  = 0x01;       // Make RA0 an input for A/D 
TRISC  = 0x00;       // MAKE PORTC all outputs
TRISD  = 0x00;       // Port D as outputs
ANSEL  = 0x01;       // Select channel ANS0
T1CON  = 0x35;       // T1CKPS1,T1CKPS0 = 1:8 prescaler,T1SYNC = do not synchronise external clock input ,TMR1ON = enabled
TRISB  = 0xF9;       // Make PB1 and PB2 an output.
ANSELH &= 0xFA;       // MAKE PB1/ANS10 and PB2/ANS8 a digital pin.       
}





