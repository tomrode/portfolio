
#include "typedefs.h"

/*Function Prototypes*/
uint16_t get_timer(uint8_t index);             // Which timer to use out of enum
void timer_set(uint8_t index, uint16_t value); 
void timer_isr(void);
void timer_init(void);

enum sw_timers_e
{
TIMER_1,
TIMER_2,
TIMER_MAX
};