#include "typedefs.h"
#include "htc.h"
#include "timer.h"
#include "humidity.h"
#define  TIMER2_VALUE    500
#define  TIMER2_VALUE2   300
enum
{
  HUMIDITY_LED_OFF,
  HUMIDITY_LED_ON,
  HUMIDITY_LED_WAIT,
  HUMIDITY_LED_DEFAULT 
};

static uint8_t loops = 0;

void humidity_set(uint8_t set_loops)
{
  if (loops == 0)                                   // Are there any loops in progress?  If not...
  {
    loops = set_loops;                              // Update the loop counter.
  }
}

void humidity_state_machine(void)
{
  static uint8_t led_state = HUMIDITY_LED_DEFAULT;  // Set initial state
 
  if (led_state == HUMIDITY_LED_WAIT)               // LED WAIT STATE
  { 
    if (loops != 0)                                 // Are there loops to do?  If so...
    { 
      PORTB |= 0x04;                                 // Turn on the LED PB2
      timer_set(TIMER_2, TIMER2_VALUE);             // Start timer
      led_state = HUMIDITY_LED_ON;                  // Next state
      toms_variable = 1;
     }
  }
  else if (led_state == HUMIDITY_LED_ON)            // LED ON STATE
  {
    if (get_timer(TIMER_2) == 0)                    // Has the timer expired?  If so...
    { 
      PORTB &= 0xFB;
      //PORTB = 0x00;                               // Turn off the LED
      timer_set(TIMER_2, TIMER2_VALUE);             // Start timer
      led_state = HUMIDITY_LED_OFF;                 // Next state
    } 
  }                                       
  else if (led_state == HUMIDITY_LED_OFF)           // LED OFF STATE 
  {
    if (get_timer(TIMER_2) == 0)                    // Has the timer expired?  If so...
    {
      if (loops != 0)                               // Is the loop counter non-zero?  If so...
      {
        loops--;                                    // Decrement the loop counter
      }
      led_state = HUMIDITY_LED_WAIT;                // Next state
    }         
  }
  else                                              // DEFAULT STATE
  { 
     //PORTB &= 0xFB;
    PORTB = 0x00;                                 // Turn off the LED
    led_state = HUMIDITY_LED_WAIT;                  // Next state
  }
}