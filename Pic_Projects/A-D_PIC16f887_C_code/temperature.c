#include "typedefs.h"
#include "htc.h"
#include "timer.h"
#include "temperature.h"
#define  TIMER1_VALUE    500   
#define  TIMER1_VALUE2   300
enum
{
  TEMP_LED_OFF,
  TEMP_LED_ON,
  TEMP_LED_WAIT,
  TEMP_LED_DEFAULT 
};

static uint8_t loops = 0;

void temperature_set(uint8_t set_loops)
{
  if (loops == 0)                                 // Are there any loops in progress?  If not...
  {
    loops = set_loops;                            // Update the loop counter.
  }
}

void temperature_state_machine(void)
{
  static uint8_t led_state = TEMP_LED_DEFAULT;     // Set initial state

  if (led_state == TEMP_LED_WAIT)                 // LED WAIT STATE
  {
   
    if (loops != 0)                                 // Are there loops to do?  If so...
    { 
      PORTB |= 0x02;                               // Turn on the LED
      timer_set(TIMER_1, TIMER1_VALUE);           // Start timer
      led_state = TEMP_LED_ON;                    // Next state
      
     }
  }
  else if (led_state == TEMP_LED_ON)              // LED ON STATE
  {
    if (get_timer(TIMER_1) == 0)                  // Has the timer expired?  If so...
    { 
      PORTB &= 0xFD;     
      //PORTB =  0x00;                               // Turn off the LED
      timer_set(TIMER_1, TIMER1_VALUE);           // Start timer
      led_state = TEMP_LED_OFF;                   // Next state
    } 
  }                                       
  else if (led_state == TEMP_LED_OFF)             // LED OFF STATE 
  {
    if (get_timer(TIMER_1) == 0)                  // Has the timer expired?  If so...
    {
      if (loops != 0)                             // Is the loop counter non-zero?  If so...
      {
        loops--;                                  // Decrement the loop counter
      }
      led_state = TEMP_LED_WAIT;                  // Next state
    }         
  }
  else                                            // DEFAULT STATE
  {   //PORTB &= 0xFD;
    PORTB = 0x00;                                 // Turn off the LED
    led_state = TEMP_LED_WAIT;                    // Next state
  }
}