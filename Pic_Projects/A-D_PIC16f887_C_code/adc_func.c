
#include "typedefs.h"
#include "htc.h"
#include "adc_func.h"

#define ADC_MAX  (0x3FF)
#define ADC_BITS (10)
/* Macro tested in main*/

#define ADC_FULL_SCALE_MV         (5000)
#define ADC_BITS                  (10)
#define ADC_MULTIPLY(x,y)         ((uint32_t)(x)*(uint32_t)(y))
#define adc_volt_convert_macro(x) ((uint16_t)(ADC_MULTIPLY(x,ADC_FULL_SCALE_MV) >> ADC_BITS))


//#define  adc_volt_convert_macro(x) (((x)*(.00488))*(1000)) // conversion macro raw adc count to volts in mV
/* variables*/
uint16_t int_count=0;
uint16_t adc_isr_flag;
uint8_t adc_bargraph_array[] = 
{
  0x00,
  0x01,
  0x03,
  0x03,
  0x07,
  0x07,
  0x0F,
  0x0F,
  0x1F,
  0x1F,
  0x3F,
  0x3F,
  0x7F,
  0x7F,
  0xFF,
  0xFF
};

uint8_t adc_bargraph_array2[] =
{
  0x00,
  0x01,
  0x02,
  0x04,
  0x08,
  0x10,
  0x20,
  0x40,
  0x80,
};

/*function prototypes*/
//void adc_init(void);
void adc_isr(void);

uint16_t adc_volt_convert(void)
{
/* Variables*/
uint16_t adc_rawvalue;
uint16_t adc_millivolt_macroval;
uint16_t adc_millivolt=0;

adc_rawvalue = adc_convert();                                  // Get Adc value
adc_millivolt_macroval = adc_volt_convert_macro(adc_rawvalue); // invoke the macro 
adc_millivolt = adc_millivolt_macroval;

asm("nop");
return adc_millivolt;                                          // send out this to any call that wants it 

}


uint16_t adc_convert(void)
{
  uint16_t adresh;                   //return value from the atd conversion 
  uint16_t adresl;                   //return value from the atd conversion
  uint16_t result; 
  adc_isr_flag = 0;                  //*CRITICAL* INTERRUPT FLAG MUST BE SET BEFORE CONVERTING
 
  GODONE = 1;                        //*CRITICAL* INTERRUPT FLAG MUST BE SET BEFORE CONVERTING
  
  while (adc_isr_flag == 0)           // Did an interrupt happened to a conversion
  {}
  adresl = ADRESL;                    // 
  adresh = ADRESH;                    //
  result = ((adresh << 8) |(adresl)); // get into word format

  return result;
}

uint8_t adc_toms_graph(uint16_t value)
{ 
  uint8_t result;

  if(value <= 0x71 )                 //0-113
  {
     result=0x00;
  }

  if((value >= 0x72)&&(value <= 0xE3))//114-227
  {
    result=0x01;
  }

  if((value >= 0xE4)&&(value <=0x0155))//228-341
  {
    result=0x02;
  }

  if((value >= 0x0156) && (value <= 0x01C7))//342-455
  {
    result=0x04;
  }

  if ((value >= 0x01C8) && (value <= 0x0239))//456-569
  {
    result=0x08;
  } 

  if ((value >= 0x0240) && (value <= 0x02AB))//570-683
  {
    result=0x10;
  }

  if ((value >= 0x02AC) && (value <= 0x31D))//684-797
  {
   result=0x20;
  }

  if ((value >= 0x031E) && (value <= 0x038F))//798-911
  { 
   result=0x40;
  }

  if ((value >= 0x0390) && (value <= 0x03FF))//912-1024
  {
   result=0x80;
  }
  else
  {
  }

  return result;
}

uint8_t adc_bargraph(uint16_t value)
{
  uint8_t result;

  if (value > ADC_MAX)
  {
    value = ADC_MAX;
  }

  result = adc_bargraph_array[value >> (ADC_BITS - 4)];

  return result;
}

uint8_t adc_bargraph2(uint16_t value)
{
  uint8_t result;

  if (value > ADC_MAX)
  {
    value = ADC_MAX;
  }

  result = adc_bargraph_array2[value/114];

  return result;
}

void adc_init(void)
{
  ADCON0 = 0xC1;    //Setting FRC internal A/D clock,  analog channel = RA0 ,ADON = selected
  ADCON1 = 0x80;    //Right justify
}

void adc_isr(void)
{
  if ((ADIF)&&(ADIE))
  {
    
    ADIF=0;          // clear flag
    adc_isr_flag=1;  // set the indication 
    int_count++; 
  }
  else 
  {
  }
}