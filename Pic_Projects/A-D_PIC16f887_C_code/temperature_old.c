#include "typedefs.h"
#include "htc.h"
#include "timer.h"
#include "temperature.h"
#define  TIMER1_VALUE    500
#define  TIMER1_VALUE2   300
enum
{
 LED_OFF,
 LED_ON,
 LED_DEFAULT 
 };

enum
{
FALSE,                       // implied to be zero
TRUE,                        // implied to be one
DEFAULT
}; 
static uint8_t locked_loops;

void temperature_set(uint8_t set_loops)
{
static uint32_t tempset_count;              // THIS VARIABLE IS SCREWING UP 
uint8_t loops; 
asm ("nop");
loops = set_loops;
if (tempset_count==0)
{
locked_loops = loops;                        // This will lock after one time
}
tempset_count++;
}

void temperature_state_machine(void)
{

static uint8_t led_state = LED_DEFAULT;         // Dont unassign variable 
if (locked_loops !=0)  
{ 
  if(led_state==LED_OFF)//&&(initial_flag==FALSE))                        // LED OFF STATE
  {
    if(get_timer(TIMER_1)==0)
    {  
     PORTB=0x02;                                //PORT 
     timer_set(TIMER_1, TIMER1_VALUE);          //Start timer
     led_state=LED_ON;                          // the next state     
     } 
  }                                       
  else if (led_state==LED_ON)//&&(initial_flag==FALSE))                   // LED ON STATE 
  {
    if(get_timer(TIMER_1)==0)
    {  
    PORTB=0x00;                                 //PORT 
    timer_set(TIMER_1, TIMER1_VALUE2);          //Start timer
    led_state=LED_OFF;
    locked_loops--;                                    // the next state
    }         
   }
  else                                          // DEFAULT STATE
   {
   PORTB=0x02;                                  // LED OFF
   led_state=LED_OFF;
  //initial_flag = FALSE;
   asm("nop");
   }
} 
}