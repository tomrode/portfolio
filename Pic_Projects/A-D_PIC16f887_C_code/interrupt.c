#include "typedefs.h"
#include "htc.h"
#include "adc_func.h"
#include "timer.h"

void interrupt interrupt_handler(void)
{
  adc_isr();
  timer_isr();
}
