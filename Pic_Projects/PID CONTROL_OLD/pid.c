//#ifndef PID_H_
//#define PID_H_
#include "pid.h" 
#include "htc.h"
#include "typedefs.h"
#include "float.h"
//Define parameter
#define epsilon 0.01
#define dt 0.01             //100ms loop time
#define MAX  100            //was 4 For Current Saturation
#define MIN -100
#define Kp  30.0             /*This made a huge differnence was this 0.1*/
#define Kd  0.01
#define Ki  0.005             //0.005

float PIDcal(float setpoint,float actual_position)
{
	static float pre_error = 0;
	static float integral = 0;
	float error;
	float derivative;
	float output;

	//Caculate P,I,D
	error = setpoint - actual_position;

	//In case of error too small then stop intergration
	if(abs(error) > epsilon)
	{
		integral = integral + error*dt;
	}
	derivative = (error - pre_error)/dt;
	output = Kp*error + Ki*integral + Kd*derivative;

	//Saturation Filter
	if(output > MAX)
	{
		output = MAX;
	}
	else if(output < MIN)
	{
		output = MIN;
	}
        //Update error
        pre_error = error;

 return output;
}

//#endif /*PID_H_*/

float abs(float a)
{
if (a < 0)
 {  
   return -a;
 }
 else
 {
  return a;
 }
}