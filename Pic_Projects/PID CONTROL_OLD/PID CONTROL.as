opt subtitle "HI-TECH Software Omniscient Code Generator (Lite mode) build 10920"

opt pagewidth 120

	opt lm

	processor	16F887
clrc	macro
	bcf	3,0
	endm
clrz	macro
	bcf	3,2
	endm
setc	macro
	bsf	3,0
	endm
setz	macro
	bsf	3,2
	endm
skipc	macro
	btfss	3,0
	endm
skipz	macro
	btfss	3,2
	endm
skipnc	macro
	btfsc	3,0
	endm
skipnz	macro
	btfsc	3,2
	endm
indf	equ	0
indf0	equ	0
pc	equ	2
pcl	equ	2
status	equ	3
fsr	equ	4
fsr0	equ	4
c	equ	1
z	equ	0
pclath	equ	10
# 33 "F:\pic_projects\PID CONTROL\main.c"
	psect config,class=CONFIG,delta=2 ;#
# 33 "F:\pic_projects\PID CONTROL\main.c"
	dw 0x3FF7 ;#
	FNCALL	_main,_PIDcal
	FNCALL	_PIDcal,___lmul
	FNCALL	_PIDcal,___aldiv
	FNROOT	_main
	global	PIDcal@integral
	global	PIDcal@pre_error
	global	_toms_variable
	global	_CARRY
psect	text117,local,class=CODE,delta=2
global __ptext117
__ptext117:
_CARRY	set	24
	global	_GIE
_GIE	set	95
	global	_EEADR
_EEADR	set	269
	global	_EEDATA
_EEDATA	set	268
	global	_EECON1
_EECON1	set	396
	global	_EECON2
_EECON2	set	397
	global	_RD
_RD	set	3168
	global	_WR
_WR	set	3169
	global	_WREN
_WREN	set	3170
	file	"PID CONTROL.as"
	line	#
psect cinit,class=CODE,delta=2
global start_initialization
start_initialization:

psect	bssCOMMON,class=COMMON,space=1
global __pbssCOMMON
__pbssCOMMON:
_toms_variable:
       ds      1

psect	bssBANK0,class=BANK0,space=1
global __pbssBANK0
__pbssBANK0:
PIDcal@integral:
       ds      4

PIDcal@pre_error:
       ds      2

; Clear objects allocated to COMMON
psect cinit,class=CODE,delta=2
	clrf	((__pbssCOMMON)+0)&07Fh
; Clear objects allocated to BANK0
psect cinit,class=CODE,delta=2
	clrf	((__pbssBANK0)+0)&07Fh
	clrf	((__pbssBANK0)+1)&07Fh
	clrf	((__pbssBANK0)+2)&07Fh
	clrf	((__pbssBANK0)+3)&07Fh
	clrf	((__pbssBANK0)+4)&07Fh
	clrf	((__pbssBANK0)+5)&07Fh
psect cinit,class=CODE,delta=2
global end_of_initialization

;End of C runtime variable initialization code

end_of_initialization:
clrf status
ljmp _main	;jump to C main() function
psect	cstackCOMMON,class=COMMON,space=1
global __pcstackCOMMON
__pcstackCOMMON:
	global	?_main
?_main:	; 0 bytes @ 0x0
	global	?___lmul
?___lmul:	; 4 bytes @ 0x0
	global	?___aldiv
?___aldiv:	; 4 bytes @ 0x0
	global	___lmul@multiplier
___lmul@multiplier:	; 4 bytes @ 0x0
	global	___aldiv@divisor
___aldiv@divisor:	; 4 bytes @ 0x0
	ds	4
	global	___lmul@multiplicand
___lmul@multiplicand:	; 4 bytes @ 0x4
	global	___aldiv@dividend
___aldiv@dividend:	; 4 bytes @ 0x4
	ds	4
	global	??___lmul
??___lmul:	; 0 bytes @ 0x8
	global	??___aldiv
??___aldiv:	; 0 bytes @ 0x8
	ds	1
	global	___lmul@product
___lmul@product:	; 4 bytes @ 0x9
	ds	4
psect	cstackBANK0,class=BANK0,space=1
global __pcstackBANK0
__pcstackBANK0:
	global	___aldiv@counter
___aldiv@counter:	; 1 bytes @ 0x0
	ds	1
	global	___aldiv@sign
___aldiv@sign:	; 1 bytes @ 0x1
	ds	1
	global	___aldiv@quotient
___aldiv@quotient:	; 4 bytes @ 0x2
	ds	4
	global	?_PIDcal
?_PIDcal:	; 2 bytes @ 0x6
	global	PIDcal@actual_position
PIDcal@actual_position:	; 1 bytes @ 0x6
	ds	2
	global	??_PIDcal
??_PIDcal:	; 0 bytes @ 0x8
	ds	8
	global	PIDcal@derivative
PIDcal@derivative:	; 4 bytes @ 0x10
	ds	4
	global	PIDcal@setpoint
PIDcal@setpoint:	; 1 bytes @ 0x14
	ds	1
	global	PIDcal@output
PIDcal@output:	; 4 bytes @ 0x15
	ds	4
	global	PIDcal@error
PIDcal@error:	; 4 bytes @ 0x19
	ds	4
	global	??_main
??_main:	; 0 bytes @ 0x1D
	ds	2
	global	main@max
main@max:	; 2 bytes @ 0x1F
	ds	2
	global	main@setpoint
main@setpoint:	; 1 bytes @ 0x21
	ds	1
	global	main@actual_position
main@actual_position:	; 1 bytes @ 0x22
	ds	1
	global	main@result
main@result:	; 1 bytes @ 0x23
	ds	1
;;Data sizes: Strings 0, constant 0, data 0, bss 7, persistent 0 stack 0
;;Auto spaces:   Size  Autos    Used
;; COMMON          14     13      14
;; BANK0           80     36      42
;; BANK1           80      0       0
;; BANK3           96      0       0
;; BANK2           96      0       0

;;
;; Pointer list with targets:

;; ?___aldiv	long  size(1) Largest target is 0
;;
;; ?___lmul	unsigned long  size(1) Largest target is 0
;;
;; ?_PIDcal	short  size(1) Largest target is 4
;;		 -> PIDcal@error(BANK0[4]), 
;;


;;
;; Critical Paths under _main in COMMON
;;
;;   _PIDcal->___lmul
;;
;; Critical Paths under _main in BANK0
;;
;;   _main->_PIDcal
;;   _PIDcal->___aldiv
;;
;; Critical Paths under _main in BANK1
;;
;;   None.
;;
;; Critical Paths under _main in BANK3
;;
;;   None.
;;
;; Critical Paths under _main in BANK2
;;
;;   None.

;;
;;Main: autosize = 0, tempsize = 2, incstack = 0, save=0
;;

;;
;;Call Graph Tables:
;;
;; ---------------------------------------------------------------------------------
;; (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;; ---------------------------------------------------------------------------------
;; (0) _main                                                 7     7      0     718
;;                                             29 BANK0      7     7      0
;;                             _PIDcal
;; ---------------------------------------------------------------------------------
;; (1) _PIDcal                                              23    21      2     662
;;                                              6 BANK0     23    21      2
;;                             ___lmul
;;                            ___aldiv
;; ---------------------------------------------------------------------------------
;; (2) ___aldiv                                             15     7      8     300
;;                                              0 COMMON     9     1      8
;;                                              0 BANK0      6     6      0
;; ---------------------------------------------------------------------------------
;; (2) ___lmul                                              13     5      8      92
;;                                              0 COMMON    13     5      8
;; ---------------------------------------------------------------------------------
;; Estimated maximum stack depth 2
;; ---------------------------------------------------------------------------------

;; Call Graph Graphs:

;; _main (ROOT)
;;   _PIDcal
;;     ___lmul
;;     ___aldiv
;;

;; Address spaces:

;;Name               Size   Autos  Total    Cost      Usage
;;BITCOMMON            E      0       0       0        0.0%
;;EEDATA             100      0       0       0        0.0%
;;NULL                 0      0       0       0        0.0%
;;CODE                 0      0       0       0        0.0%
;;COMMON               E      D       E       1      100.0%
;;BITSFR0              0      0       0       1        0.0%
;;SFR0                 0      0       0       1        0.0%
;;BITSFR1              0      0       0       2        0.0%
;;SFR1                 0      0       0       2        0.0%
;;STACK                0      0       2       2        0.0%
;;ABS                  0      0      38       3        0.0%
;;BITBANK0            50      0       0       4        0.0%
;;BITSFR3              0      0       0       4        0.0%
;;SFR3                 0      0       0       4        0.0%
;;BANK0               50     24      2A       5       52.5%
;;BITSFR2              0      0       0       5        0.0%
;;SFR2                 0      0       0       5        0.0%
;;BITBANK1            50      0       0       6        0.0%
;;BANK1               50      0       0       7        0.0%
;;BITBANK3            60      0       0       8        0.0%
;;BANK3               60      0       0       9        0.0%
;;BITBANK2            60      0       0      10        0.0%
;;BANK2               60      0       0      11        0.0%
;;DATA                 0      0      3A      12        0.0%

	global	_main
psect	maintext,global,class=CODE,delta=2
global __pmaintext
__pmaintext:

;; *************** function _main *****************
;; Defined at:
;;		line 40 in file "F:\pic_projects\PID CONTROL\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  max             2   31[BANK0 ] short 
;;  result          1   35[BANK0 ] char 
;;  actual_posit    1   34[BANK0 ] char 
;;  setpoint        1   33[BANK0 ] char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0, btemp+1, pclath, cstack
;; Tracked objects:
;;		On entry : 17F/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       5       0       0       0
;;      Temps:          0       2       0       0       0
;;      Totals:         0       7       0       0       0
;;Total ram usage:        7 bytes
;; Hardware stack levels required when called:    2
;; This function calls:
;;		_PIDcal
;; This function is called by:
;;		Startup code after reset
;; This function uses a non-reentrant model
;;
psect	maintext
	file	"F:\pic_projects\PID CONTROL\main.c"
	line	40
	global	__size_of_main
	__size_of_main	equ	__end_of_main-_main
	
_main:	
	opt	stack 6
; Regs used in _main: [wreg+status,2+status,0+btemp+1+pclath+cstack]
	line	47
;main.c: 41: sint16_t max;
;main.c: 43: sint8_t setpoint;
;main.c: 44: sint8_t actual_position;
;main.c: 45: sint8_t result;
;main.c: 47: while(1)
	
l839:	
	line	49
	
l3344:	
;main.c: 48: {
;main.c: 49: max = 10000;
	movlw	low(02710h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(main@max)
	movlw	high(02710h)
	movwf	((main@max))+1
	line	50
;main.c: 50: setpoint = 0x45;
	movlw	(045h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(main@setpoint)
	line	51
;main.c: 51: actual_position = 0x44;
	movlw	(044h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(main@actual_position)
	line	53
	
l3346:	
;main.c: 53: result=PIDcal(setpoint,actual_position);
	movf	(main@actual_position),w
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(?_PIDcal)
	movf	(main@setpoint),w
	fcall	_PIDcal
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(0+(?_PIDcal)),w
	movwf	(??_main+1)+0
	movf	(??_main+1)+0,w
	movwf	(main@result)
	line	54
	
l3348:	
# 54 "F:\pic_projects\PID CONTROL\main.c"
nop ;#
psect	maintext
	line	55
	
l3350:	
;main.c: 55: result=PIDcal(0x63,0);
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(?_PIDcal)
	movlw	(063h)
	fcall	_PIDcal
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(0+(?_PIDcal)),w
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(main@result)
	line	56
	
l3352:	
# 56 "F:\pic_projects\PID CONTROL\main.c"
nop ;#
psect	maintext
	line	57
	
l3354:	
;main.c: 57: result=PIDcal(0x09,0x50);
	movlw	(050h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(?_PIDcal)
	movlw	(09h)
	fcall	_PIDcal
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(0+(?_PIDcal)),w
	movwf	(??_main+1)+0
	movf	(??_main+1)+0,w
	movwf	(main@result)
	line	58
	
l3356:	
# 58 "F:\pic_projects\PID CONTROL\main.c"
nop ;#
psect	maintext
	line	59
	
l3358:	
;main.c: 59: result=PIDcal(0x44,0x33);
	movlw	(033h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(?_PIDcal)
	movlw	(044h)
	fcall	_PIDcal
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(0+(?_PIDcal)),w
	movwf	(??_main+1)+0
	movf	(??_main+1)+0,w
	movwf	(main@result)
	line	60
	
l3360:	
# 60 "F:\pic_projects\PID CONTROL\main.c"
nop ;#
psect	maintext
	line	61
	
l3362:	
;main.c: 61: result=PIDcal(0x44,0x42);
	movlw	(042h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(?_PIDcal)
	movlw	(044h)
	fcall	_PIDcal
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(0+(?_PIDcal)),w
	movwf	(??_main+1)+0
	movf	(??_main+1)+0,w
	movwf	(main@result)
	line	62
	
l3364:	
# 62 "F:\pic_projects\PID CONTROL\main.c"
nop ;#
psect	maintext
	line	63
	
l3366:	
;main.c: 63: result=PIDcal(0x44,0x43);
	movlw	(043h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(?_PIDcal)
	movlw	(044h)
	fcall	_PIDcal
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(0+(?_PIDcal)),w
	movwf	(??_main+1)+0
	movf	(??_main+1)+0,w
	movwf	(main@result)
	line	64
	
l3368:	
# 64 "F:\pic_projects\PID CONTROL\main.c"
nop ;#
psect	maintext
	line	65
	
l3370:	
;main.c: 65: result=PIDcal(0x44,0x44);
	movlw	(044h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(?_PIDcal)
	movlw	(044h)
	fcall	_PIDcal
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(0+(?_PIDcal)),w
	movwf	(??_main+1)+0
	movf	(??_main+1)+0,w
	movwf	(main@result)
	line	66
	
l3372:	
# 66 "F:\pic_projects\PID CONTROL\main.c"
nop ;#
psect	maintext
	line	67
	
l3374:	
;main.c: 67: result=PIDcal(0x44,0x44);
	movlw	(044h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(?_PIDcal)
	movlw	(044h)
	fcall	_PIDcal
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(0+(?_PIDcal)),w
	movwf	(??_main+1)+0
	movf	(??_main+1)+0,w
	movwf	(main@result)
	line	68
	
l3376:	
# 68 "F:\pic_projects\PID CONTROL\main.c"
nop ;#
psect	maintext
	line	69
	
l3378:	
;main.c: 69: result=PIDcal(0x44,0x44);
	movlw	(044h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(?_PIDcal)
	movlw	(044h)
	fcall	_PIDcal
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(0+(?_PIDcal)),w
	movwf	(??_main+1)+0
	movf	(??_main+1)+0,w
	movwf	(main@result)
	line	70
	
l3380:	
# 70 "F:\pic_projects\PID CONTROL\main.c"
nop ;#
psect	maintext
	goto	l839
	line	71
	
l840:	
	line	47
	goto	l839
	
l841:	
	line	73
	
l842:	
	global	start
	ljmp	start
	opt stack 0
GLOBAL	__end_of_main
	__end_of_main:
;; =============== function _main ends ============

	signat	_main,88
	global	_PIDcal
psect	text118,local,class=CODE,delta=2
global __ptext118
__ptext118:

;; *************** function _PIDcal *****************
;; Defined at:
;;		line 77 in file "F:\pic_projects\PID CONTROL\main.c"
;; Parameters:    Size  Location     Type
;;  setpoint        1    wreg     char 
;;  actual_posit    1    6[BANK0 ] char 
;; Auto vars:     Size  Location     Type
;;  setpoint        1   20[BANK0 ] char 
;;  error           4   25[BANK0 ] long 
;;  output          4   21[BANK0 ] long 
;;  derivative      4   16[BANK0 ] long 
;; Return value:  Size  Location     Type
;;                  2    6[BANK0 ] short 
;; Registers used:
;;		wreg, status,2, status,0, btemp+1, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       2       0       0       0
;;      Locals:         0      13       0       0       0
;;      Temps:          0       8       0       0       0
;;      Totals:         0      23       0       0       0
;;Total ram usage:       23 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		___lmul
;;		___aldiv
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text118
	file	"F:\pic_projects\PID CONTROL\main.c"
	line	77
	global	__size_of_PIDcal
	__size_of_PIDcal	equ	__end_of_PIDcal-_PIDcal
	
_PIDcal:	
	opt	stack 6
; Regs used in _PIDcal: [wreg+status,2+status,0+btemp+1+pclath+cstack]
;PIDcal@setpoint stored from wreg
	line	86
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(PIDcal@setpoint)
	
l3318:	
;main.c: 78: static sint16_t pre_error = 0;
;main.c: 79: static sint32_t integral = 0;
;main.c: 80: sint32_t error;
;main.c: 81: sint32_t derivative;
;main.c: 82: sint32_t output;
;main.c: 86: error = (setpoint - actual_position);
	movf	(PIDcal@actual_position),w
	movwf	(??_PIDcal+0)+0
	clrf	(??_PIDcal+0)+0+1
	btfsc	(??_PIDcal+0)+0,7
	decf	(??_PIDcal+0)+0+1,f
	comf	(??_PIDcal+0)+0,f
	comf	(??_PIDcal+0)+1,f
	incf	(??_PIDcal+0)+0,f
	skipnz
	incf	(??_PIDcal+0)+1,f
	movf	(PIDcal@setpoint),w
	movwf	(??_PIDcal+2)+0
	clrf	(??_PIDcal+2)+0+1
	btfsc	(??_PIDcal+2)+0,7
	decf	(??_PIDcal+2)+0+1,f
	movf	0+(??_PIDcal+0)+0,w
	addwf	0+(??_PIDcal+2)+0,w
	movwf	(PIDcal@error)
	movf	1+(??_PIDcal+0)+0,w
	skipnc
	incf	1+(??_PIDcal+0)+0,w
	addwf	1+(??_PIDcal+2)+0,w
	movwf	1+(PIDcal@error)
	clrf	(PIDcal@error)+2
	btfsc	(PIDcal@error)+1,7
	decf	2+(PIDcal@error),f
	movf	(PIDcal@error)+2,w
	movwf	3+(PIDcal@error)
	line	89
	
l3320:	
;main.c: 89: if(error >= 1)
	movf	(PIDcal@error+3),w
	xorlw	80h
	movwf	btemp+1
	movlw	0
	xorlw	80h
	subwf	btemp+1,w
	
	skipz
	goto	u2453
	movlw	0
	subwf	(PIDcal@error+2),w
	skipz
	goto	u2453
	movlw	0
	subwf	(PIDcal@error+1),w
	skipz
	goto	u2453
	movlw	01h
	subwf	(PIDcal@error),w
u2453:
	skipc
	goto	u2451
	goto	u2450
u2451:
	goto	l3324
u2450:
	line	91
	
l3322:	
;main.c: 90: {
;main.c: 91: integral = integral + error*1;
	movf	(PIDcal@error),w
	addwf	(PIDcal@integral),w
	movwf	((??_PIDcal+0)+0+0)
	movlw	0
	skipnc
	movlw	1
	addwf	(PIDcal@error+1),w
	clrf	((??_PIDcal+0)+0+2)
	skipnc
	incf	((??_PIDcal+0)+0+2),f
	addwf	(PIDcal@integral+1),w
	movwf	((??_PIDcal+0)+0+1)
	skipnc
	incf	((??_PIDcal+0)+0+2),f
	movf	(PIDcal@error+2),w
	addwf	((??_PIDcal+0)+0+2),w
	clrf	((??_PIDcal+0)+0+3)
	skipnc
	incf	((??_PIDcal+0)+0+3),f
	addwf	(PIDcal@integral+2),w
	movwf	((??_PIDcal+0)+0+2)
	skipnc
	incf	((??_PIDcal+0)+0+3),f
	movf	(PIDcal@error+3),w
	addwf	((??_PIDcal+0)+0+3),w
	addwf	(PIDcal@integral+3),w
	movwf	((??_PIDcal+0)+0+3)
	movf	3+(??_PIDcal+0)+0,w
	movwf	(PIDcal@integral+3)
	movf	2+(??_PIDcal+0)+0,w
	movwf	(PIDcal@integral+2)
	movf	1+(??_PIDcal+0)+0,w
	movwf	(PIDcal@integral+1)
	movf	0+(??_PIDcal+0)+0,w
	movwf	(PIDcal@integral)

	line	92
;main.c: 92: }
	goto	l3326
	line	93
	
l849:	
	line	95
	
l3324:	
;main.c: 93: else
;main.c: 94: {
;main.c: 95: integral = 0;
	movlw	0
	movwf	(PIDcal@integral+3)
	movlw	0
	movwf	(PIDcal@integral+2)
	movlw	0
	movwf	(PIDcal@integral+1)
	movlw	0
	movwf	(PIDcal@integral)

	goto	l3326
	line	96
	
l850:	
	line	98
	
l3326:	
;main.c: 96: }
;main.c: 98: derivative = (error - pre_error)*100;
	movf	(PIDcal@error),w
	movwf	(??_PIDcal+0)+0
	movf	(PIDcal@error+1),w
	movwf	((??_PIDcal+0)+0+1)
	movf	(PIDcal@error+2),w
	movwf	((??_PIDcal+0)+0+2)
	movf	(PIDcal@error+3),w
	movwf	((??_PIDcal+0)+0+3)
	movf	(PIDcal@pre_error),w
	movwf	((??_PIDcal+4)+0)
	movf	(PIDcal@pre_error+1),w
	movwf	((??_PIDcal+4)+0+1)
	movlw	0
	btfsc	((??_PIDcal+4)+0+1),7
	movlw	255
	movwf	((??_PIDcal+4)+0+2)
	movwf	((??_PIDcal+4)+0+3)
	comf	(??_PIDcal+4)+0,f
	comf	(??_PIDcal+4)+1,f
	comf	(??_PIDcal+4)+2,f
	comf	(??_PIDcal+4)+3,f
	incf	(??_PIDcal+4)+0,f
	skipnz
	incf	(??_PIDcal+4)+1,f
	skipnz
	incf	(??_PIDcal+4)+2,f
	skipnz
	incf	(??_PIDcal+4)+3,f
	movf	0+(??_PIDcal+4)+0,w
	addwf	(??_PIDcal+0)+0,f
	movf	1+(??_PIDcal+4)+0,w
	skipnc
	incfsz	1+(??_PIDcal+4)+0,w
	goto	u2460
	goto	u2461
u2460:
	addwf	(??_PIDcal+0)+1,f
u2461:
	movf	2+(??_PIDcal+4)+0,w
	skipnc
	incfsz	2+(??_PIDcal+4)+0,w
	goto	u2462
	goto	u2463
u2462:
	addwf	(??_PIDcal+0)+2,f
u2463:
	movf	3+(??_PIDcal+4)+0,w
	skipnc
	incf	3+(??_PIDcal+4)+0,w
	addwf	(??_PIDcal+0)+3,f
	movf	3+(??_PIDcal+0)+0,w
	movwf	(?___lmul+3)
	movf	2+(??_PIDcal+0)+0,w
	movwf	(?___lmul+2)
	movf	1+(??_PIDcal+0)+0,w
	movwf	(?___lmul+1)
	movf	0+(??_PIDcal+0)+0,w
	movwf	(?___lmul)

	movlw	0
	movwf	3+(?___lmul)+04h
	movlw	0
	movwf	2+(?___lmul)+04h
	movlw	0
	movwf	1+(?___lmul)+04h
	movlw	064h
	movwf	0+(?___lmul)+04h

	fcall	___lmul
	movf	(3+(?___lmul)),w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(PIDcal@derivative+3)
	movf	(2+(?___lmul)),w
	movwf	(PIDcal@derivative+2)
	movf	(1+(?___lmul)),w
	movwf	(PIDcal@derivative+1)
	movf	(0+(?___lmul)),w
	movwf	(PIDcal@derivative)

	line	100
	
l3328:	
;main.c: 100: output = 3000*error + 5*integral + 1*derivative;
	movf	(PIDcal@derivative),w
	movwf	(??_PIDcal+0)+0
	movf	(PIDcal@derivative+1),w
	movwf	((??_PIDcal+0)+0+1)
	movf	(PIDcal@derivative+2),w
	movwf	((??_PIDcal+0)+0+2)
	movf	(PIDcal@derivative+3),w
	movwf	((??_PIDcal+0)+0+3)
	movf	(PIDcal@integral+3),w
	movwf	(?___lmul+3)
	movf	(PIDcal@integral+2),w
	movwf	(?___lmul+2)
	movf	(PIDcal@integral+1),w
	movwf	(?___lmul+1)
	movf	(PIDcal@integral),w
	movwf	(?___lmul)

	movlw	0
	movwf	3+(?___lmul)+04h
	movlw	0
	movwf	2+(?___lmul)+04h
	movlw	0
	movwf	1+(?___lmul)+04h
	movlw	05h
	movwf	0+(?___lmul)+04h

	fcall	___lmul
	movf	(0+?___lmul),w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_PIDcal+4)+0
	movf	(1+?___lmul),w
	movwf	((??_PIDcal+4)+0+1)
	movf	(2+?___lmul),w
	movwf	((??_PIDcal+4)+0+2)
	movf	(3+?___lmul),w
	movwf	((??_PIDcal+4)+0+3)
	movf	(PIDcal@error+3),w
	movwf	(?___lmul+3)
	movf	(PIDcal@error+2),w
	movwf	(?___lmul+2)
	movf	(PIDcal@error+1),w
	movwf	(?___lmul+1)
	movf	(PIDcal@error),w
	movwf	(?___lmul)

	movlw	0
	movwf	3+(?___lmul)+04h
	movlw	0
	movwf	2+(?___lmul)+04h
	movlw	0Bh
	movwf	1+(?___lmul)+04h
	movlw	0B8h
	movwf	0+(?___lmul)+04h

	fcall	___lmul
	movf	(0+(?___lmul)),w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	addwf	(??_PIDcal+4)+0,f
	movf	(1+(?___lmul)),w
	skipnc
	incfsz	(1+(?___lmul)),w
	goto	u2470
	goto	u2471
u2470:
	addwf	(??_PIDcal+4)+1,f
u2471:
	movf	(2+(?___lmul)),w
	skipnc
	incfsz	(2+(?___lmul)),w
	goto	u2472
	goto	u2473
u2472:
	addwf	(??_PIDcal+4)+2,f
u2473:
	movf	(3+(?___lmul)),w
	skipnc
	incf	(3+(?___lmul)),w
	addwf	(??_PIDcal+4)+3,f
	movf	0+(??_PIDcal+4)+0,w
	addwf	(??_PIDcal+0)+0,f
	movf	1+(??_PIDcal+4)+0,w
	skipnc
	incfsz	1+(??_PIDcal+4)+0,w
	goto	u2480
	goto	u2481
u2480:
	addwf	(??_PIDcal+0)+1,f
u2481:
	movf	2+(??_PIDcal+4)+0,w
	skipnc
	incfsz	2+(??_PIDcal+4)+0,w
	goto	u2482
	goto	u2483
u2482:
	addwf	(??_PIDcal+0)+2,f
u2483:
	movf	3+(??_PIDcal+4)+0,w
	skipnc
	incf	3+(??_PIDcal+4)+0,w
	addwf	(??_PIDcal+0)+3,f
	movf	3+(??_PIDcal+0)+0,w
	movwf	(PIDcal@output+3)
	movf	2+(??_PIDcal+0)+0,w
	movwf	(PIDcal@output+2)
	movf	1+(??_PIDcal+0)+0,w
	movwf	(PIDcal@output+1)
	movf	0+(??_PIDcal+0)+0,w
	movwf	(PIDcal@output)

	line	101
	
l3330:	
;main.c: 101: output = output/100;
	movlw	0
	movwf	(?___aldiv+3)
	movlw	0
	movwf	(?___aldiv+2)
	movlw	0
	movwf	(?___aldiv+1)
	movlw	064h
	movwf	(?___aldiv)

	movf	(PIDcal@output+3),w
	movwf	3+(?___aldiv)+04h
	movf	(PIDcal@output+2),w
	movwf	2+(?___aldiv)+04h
	movf	(PIDcal@output+1),w
	movwf	1+(?___aldiv)+04h
	movf	(PIDcal@output),w
	movwf	0+(?___aldiv)+04h

	fcall	___aldiv
	movf	(3+(?___aldiv)),w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(PIDcal@output+3)
	movf	(2+(?___aldiv)),w
	movwf	(PIDcal@output+2)
	movf	(1+(?___aldiv)),w
	movwf	(PIDcal@output+1)
	movf	(0+(?___aldiv)),w
	movwf	(PIDcal@output)

	line	104
	
l3332:	
;main.c: 104: if(output > (10000 - 9900))
	movf	(PIDcal@output+3),w
	xorlw	80h
	movwf	btemp+1
	movlw	0
	xorlw	80h
	subwf	btemp+1,w
	
	skipz
	goto	u2493
	movlw	0
	subwf	(PIDcal@output+2),w
	skipz
	goto	u2493
	movlw	0
	subwf	(PIDcal@output+1),w
	skipz
	goto	u2493
	movlw	065h
	subwf	(PIDcal@output),w
u2493:
	skipc
	goto	u2491
	goto	u2490
u2491:
	goto	l851
u2490:
	line	106
	
l3334:	
;main.c: 105: {
;main.c: 106: output = 10000 - 9900;
	movlw	0
	movwf	(PIDcal@output+3)
	movlw	0
	movwf	(PIDcal@output+2)
	movlw	0
	movwf	(PIDcal@output+1)
	movlw	064h
	movwf	(PIDcal@output)

	line	107
;main.c: 107: }
	goto	l3338
	line	108
	
l851:	
;main.c: 108: else if(output < 0)
	btfss	(PIDcal@output+3),7
	goto	u2501
	goto	u2500
u2501:
	goto	l3338
u2500:
	line	110
	
l3336:	
;main.c: 109: {
;main.c: 110: output = -10000 + 9900;
	movlw	0FFh
	movwf	(PIDcal@output+3)
	movlw	0FFh
	movwf	(PIDcal@output+2)
	movlw	0FFh
	movwf	(PIDcal@output+1)
	movlw	09Ch
	movwf	(PIDcal@output)

	goto	l3338
	line	111
	
l853:	
	goto	l3338
	line	113
	
l852:	
	
l3338:	
;main.c: 111: }
;main.c: 113: pre_error = error;
	movf	(PIDcal@error+1),w
	clrf	(PIDcal@pre_error+1)
	addwf	(PIDcal@pre_error+1)
	movf	(PIDcal@error),w
	clrf	(PIDcal@pre_error)
	addwf	(PIDcal@pre_error)

	line	115
	
l3340:	
;main.c: 115: return (output);
	movf	(PIDcal@output+1),w
	clrf	(?_PIDcal+1)
	addwf	(?_PIDcal+1)
	movf	(PIDcal@output),w
	clrf	(?_PIDcal)
	addwf	(?_PIDcal)

	goto	l854
	
l3342:	
	line	116
	
l854:	
	return
	opt stack 0
GLOBAL	__end_of_PIDcal
	__end_of_PIDcal:
;; =============== function _PIDcal ends ============

	signat	_PIDcal,8314
	global	___aldiv
psect	text119,local,class=CODE,delta=2
global __ptext119
__ptext119:

;; *************** function ___aldiv *****************
;; Defined at:
;;		line 5 in file "C:\Program Files\HI-TECH Software\PICC\9.83\sources\aldiv.c"
;; Parameters:    Size  Location     Type
;;  divisor         4    0[COMMON] long 
;;  dividend        4    4[COMMON] long 
;; Auto vars:     Size  Location     Type
;;  quotient        4    2[BANK0 ] long 
;;  sign            1    1[BANK0 ] unsigned char 
;;  counter         1    0[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;                  4    0[COMMON] long 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         8       0       0       0       0
;;      Locals:         0       6       0       0       0
;;      Temps:          1       0       0       0       0
;;      Totals:         9       6       0       0       0
;;Total ram usage:       15 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_PIDcal
;; This function uses a non-reentrant model
;;
psect	text119
	file	"C:\Program Files\HI-TECH Software\PICC\9.83\sources\aldiv.c"
	line	5
	global	__size_of___aldiv
	__size_of___aldiv	equ	__end_of___aldiv-___aldiv
	
___aldiv:	
	opt	stack 6
; Regs used in ___aldiv: [wreg+status,2+status,0]
	line	9
	
l3276:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(___aldiv@sign)
	line	10
	btfss	(___aldiv@divisor+3),7
	goto	u2351
	goto	u2350
u2351:
	goto	l3282
u2350:
	line	11
	
l3278:	
	comf	(___aldiv@divisor),f
	comf	(___aldiv@divisor+1),f
	comf	(___aldiv@divisor+2),f
	comf	(___aldiv@divisor+3),f
	incf	(___aldiv@divisor),f
	skipnz
	incf	(___aldiv@divisor+1),f
	skipnz
	incf	(___aldiv@divisor+2),f
	skipnz
	incf	(___aldiv@divisor+3),f
	line	12
	
l3280:	
	clrf	(___aldiv@sign)
	bsf	status,0
	rlf	(___aldiv@sign),f
	goto	l3282
	line	13
	
l2043:	
	line	14
	
l3282:	
	btfss	(___aldiv@dividend+3),7
	goto	u2361
	goto	u2360
u2361:
	goto	l3288
u2360:
	line	15
	
l3284:	
	comf	(___aldiv@dividend),f
	comf	(___aldiv@dividend+1),f
	comf	(___aldiv@dividend+2),f
	comf	(___aldiv@dividend+3),f
	incf	(___aldiv@dividend),f
	skipnz
	incf	(___aldiv@dividend+1),f
	skipnz
	incf	(___aldiv@dividend+2),f
	skipnz
	incf	(___aldiv@dividend+3),f
	line	16
	
l3286:	
	movlw	(01h)
	movwf	(??___aldiv+0)+0
	movf	(??___aldiv+0)+0,w
	xorwf	(___aldiv@sign),f
	goto	l3288
	line	17
	
l2044:	
	line	18
	
l3288:	
	movlw	0
	movwf	(___aldiv@quotient+3)
	movlw	0
	movwf	(___aldiv@quotient+2)
	movlw	0
	movwf	(___aldiv@quotient+1)
	movlw	0
	movwf	(___aldiv@quotient)

	line	19
	
l3290:	
	movf	(___aldiv@divisor+3),w
	iorwf	(___aldiv@divisor+2),w
	iorwf	(___aldiv@divisor+1),w
	iorwf	(___aldiv@divisor),w
	skipnz
	goto	u2371
	goto	u2370
u2371:
	goto	l3310
u2370:
	line	20
	
l3292:	
	clrf	(___aldiv@counter)
	bsf	status,0
	rlf	(___aldiv@counter),f
	line	21
	goto	l3296
	
l2047:	
	line	22
	
l3294:	
	movlw	01h
	movwf	(??___aldiv+0)+0
u2385:
	clrc
	rlf	(___aldiv@divisor),f
	rlf	(___aldiv@divisor+1),f
	rlf	(___aldiv@divisor+2),f
	rlf	(___aldiv@divisor+3),f
	decfsz	(??___aldiv+0)+0
	goto	u2385
	line	23
	movlw	(01h)
	movwf	(??___aldiv+0)+0
	movf	(??___aldiv+0)+0,w
	addwf	(___aldiv@counter),f
	goto	l3296
	line	24
	
l2046:	
	line	21
	
l3296:	
	btfss	(___aldiv@divisor+3),(31)&7
	goto	u2391
	goto	u2390
u2391:
	goto	l3294
u2390:
	goto	l3298
	
l2048:	
	goto	l3298
	line	25
	
l2049:	
	line	26
	
l3298:	
	movlw	01h
	movwf	(??___aldiv+0)+0
u2405:
	clrc
	rlf	(___aldiv@quotient),f
	rlf	(___aldiv@quotient+1),f
	rlf	(___aldiv@quotient+2),f
	rlf	(___aldiv@quotient+3),f
	decfsz	(??___aldiv+0)+0
	goto	u2405
	line	27
	
l3300:	
	movf	(___aldiv@divisor+3),w
	subwf	(___aldiv@dividend+3),w
	skipz
	goto	u2415
	movf	(___aldiv@divisor+2),w
	subwf	(___aldiv@dividend+2),w
	skipz
	goto	u2415
	movf	(___aldiv@divisor+1),w
	subwf	(___aldiv@dividend+1),w
	skipz
	goto	u2415
	movf	(___aldiv@divisor),w
	subwf	(___aldiv@dividend),w
u2415:
	skipc
	goto	u2411
	goto	u2410
u2411:
	goto	l3306
u2410:
	line	28
	
l3302:	
	movf	(___aldiv@divisor),w
	subwf	(___aldiv@dividend),f
	movf	(___aldiv@divisor+1),w
	skipc
	incfsz	(___aldiv@divisor+1),w
	subwf	(___aldiv@dividend+1),f
	movf	(___aldiv@divisor+2),w
	skipc
	incfsz	(___aldiv@divisor+2),w
	subwf	(___aldiv@dividend+2),f
	movf	(___aldiv@divisor+3),w
	skipc
	incfsz	(___aldiv@divisor+3),w
	subwf	(___aldiv@dividend+3),f
	line	29
	
l3304:	
	bsf	(___aldiv@quotient)+(0/8),(0)&7
	goto	l3306
	line	30
	
l2050:	
	line	31
	
l3306:	
	movlw	01h
u2425:
	clrc
	rrf	(___aldiv@divisor+3),f
	rrf	(___aldiv@divisor+2),f
	rrf	(___aldiv@divisor+1),f
	rrf	(___aldiv@divisor),f
	addlw	-1
	skipz
	goto	u2425

	line	32
	
l3308:	
	movlw	low(01h)
	subwf	(___aldiv@counter),f
	btfss	status,2
	goto	u2431
	goto	u2430
u2431:
	goto	l3298
u2430:
	goto	l3310
	
l2051:	
	goto	l3310
	line	33
	
l2045:	
	line	34
	
l3310:	
	movf	(___aldiv@sign),w
	skipz
	goto	u2440
	goto	l3314
u2440:
	line	35
	
l3312:	
	comf	(___aldiv@quotient),f
	comf	(___aldiv@quotient+1),f
	comf	(___aldiv@quotient+2),f
	comf	(___aldiv@quotient+3),f
	incf	(___aldiv@quotient),f
	skipnz
	incf	(___aldiv@quotient+1),f
	skipnz
	incf	(___aldiv@quotient+2),f
	skipnz
	incf	(___aldiv@quotient+3),f
	goto	l3314
	
l2052:	
	line	36
	
l3314:	
	movf	(___aldiv@quotient+3),w
	movwf	(?___aldiv+3)
	movf	(___aldiv@quotient+2),w
	movwf	(?___aldiv+2)
	movf	(___aldiv@quotient+1),w
	movwf	(?___aldiv+1)
	movf	(___aldiv@quotient),w
	movwf	(?___aldiv)

	goto	l2053
	
l3316:	
	line	37
	
l2053:	
	return
	opt stack 0
GLOBAL	__end_of___aldiv
	__end_of___aldiv:
;; =============== function ___aldiv ends ============

	signat	___aldiv,8316
	global	___lmul
psect	text120,local,class=CODE,delta=2
global __ptext120
__ptext120:

;; *************** function ___lmul *****************
;; Defined at:
;;		line 3 in file "C:\Program Files\HI-TECH Software\PICC\9.83\sources\lmul.c"
;; Parameters:    Size  Location     Type
;;  multiplier      4    0[COMMON] unsigned long 
;;  multiplicand    4    4[COMMON] unsigned long 
;; Auto vars:     Size  Location     Type
;;  product         4    9[COMMON] unsigned long 
;; Return value:  Size  Location     Type
;;                  4    0[COMMON] unsigned long 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         8       0       0       0       0
;;      Locals:         4       0       0       0       0
;;      Temps:          1       0       0       0       0
;;      Totals:        13       0       0       0       0
;;Total ram usage:       13 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_PIDcal
;; This function uses a non-reentrant model
;;
psect	text120
	file	"C:\Program Files\HI-TECH Software\PICC\9.83\sources\lmul.c"
	line	3
	global	__size_of___lmul
	__size_of___lmul	equ	__end_of___lmul-___lmul
	
___lmul:	
	opt	stack 6
; Regs used in ___lmul: [wreg+status,2+status,0]
	line	4
	
l3262:	
	movlw	0
	movwf	(___lmul@product+3)
	movlw	0
	movwf	(___lmul@product+2)
	movlw	0
	movwf	(___lmul@product+1)
	movlw	0
	movwf	(___lmul@product)

	goto	l3264
	line	6
	
l1923:	
	line	7
	
l3264:	
	btfss	(___lmul@multiplier),(0)&7
	goto	u2301
	goto	u2300
u2301:
	goto	l3268
u2300:
	line	8
	
l3266:	
	movf	(___lmul@multiplicand),w
	addwf	(___lmul@product),f
	movf	(___lmul@multiplicand+1),w
	clrz
	skipnc
	addlw	1
	skipnz
	goto	u2311
	addwf	(___lmul@product+1),f
u2311:
	movf	(___lmul@multiplicand+2),w
	clrz
	skipnc
	addlw	1
	skipnz
	goto	u2312
	addwf	(___lmul@product+2),f
u2312:
	movf	(___lmul@multiplicand+3),w
	clrz
	skipnc
	addlw	1
	skipnz
	goto	u2313
	addwf	(___lmul@product+3),f
u2313:

	goto	l3268
	
l1924:	
	line	9
	
l3268:	
	movlw	01h
	movwf	(??___lmul+0)+0
u2325:
	clrc
	rlf	(___lmul@multiplicand),f
	rlf	(___lmul@multiplicand+1),f
	rlf	(___lmul@multiplicand+2),f
	rlf	(___lmul@multiplicand+3),f
	decfsz	(??___lmul+0)+0
	goto	u2325
	line	10
	
l3270:	
	movlw	01h
u2335:
	clrc
	rrf	(___lmul@multiplier+3),f
	rrf	(___lmul@multiplier+2),f
	rrf	(___lmul@multiplier+1),f
	rrf	(___lmul@multiplier),f
	addlw	-1
	skipz
	goto	u2335

	line	11
	movf	(___lmul@multiplier+3),w
	iorwf	(___lmul@multiplier+2),w
	iorwf	(___lmul@multiplier+1),w
	iorwf	(___lmul@multiplier),w
	skipz
	goto	u2341
	goto	u2340
u2341:
	goto	l3264
u2340:
	goto	l3272
	
l1925:	
	line	12
	
l3272:	
	movf	(___lmul@product+3),w
	movwf	(?___lmul+3)
	movf	(___lmul@product+2),w
	movwf	(?___lmul+2)
	movf	(___lmul@product+1),w
	movwf	(?___lmul+1)
	movf	(___lmul@product),w
	movwf	(?___lmul)

	goto	l1926
	
l3274:	
	line	13
	
l1926:	
	return
	opt stack 0
GLOBAL	__end_of___lmul
	__end_of___lmul:
;; =============== function ___lmul ends ============

	signat	___lmul,8316
psect	text121,local,class=CODE,delta=2
global __ptext121
__ptext121:
	global	btemp
	btemp set 07Eh

	DABS	1,126,2	;btemp
	global	wtemp0
	wtemp0 set btemp
	end
