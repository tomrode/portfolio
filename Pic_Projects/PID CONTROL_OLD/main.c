#include "htc.h"
#include "typedefs.h"
#include "float.h"
//Define parameter
//#define  FLOATING_POINT_PID   
#define  FIXED_POINT_PID

/* All these #defines for floating point*/
#ifdef FLOATING_POINT_PID
#define epsilon 0.01
#define dt 0.01             //100ms loop time
#define MAX  100            //was 4 For Current Saturation
#define MIN -100
#define Kp  30.0             /*This made a huge differnence was this 0.1*/
#define Kd  0.01
#define Ki  0.05             //0.005
#endif

/* All these #defines for fixed point*/
#ifdef FIXED_POINT_PID
#define epsilon      1
#define dt           1            //100ms loop time
#define MAX      10000            //was 4 For Current Saturation
#define MIN     -10000
#define Kp        3000             /*This made a huge differnence was this 0.1*/
#define Kd           1
#define Ki           5             //0.005
#define offset    9900
#define ZERO         0
#endif


__CONFIG(WDTDIS);//WDTE_OFF);
/* Function Prototypes*/
//float PIDcal(float setpoint,float actual_position);
sint16_t PIDcal(sint8_t setpoint,sint8_t actual_position);
float abs(float a);

void main(void)
{
sint16_t max;

sint8_t setpoint;
sint8_t actual_position;
sint8_t result;

while(1)
{
max = MAX;
setpoint = 0x45;
actual_position = 0x44;
 
result=PIDcal(setpoint,actual_position);    /* Floating point      Fixed point  integration to 0        */ 
asm("nop");                                 /* =31                 =31          =31                     */         
result=PIDcal(0x63,0);
asm("nop");                                 /* =100                =100         =100                   */                             
result=PIDcal(0x09,0x50);
asm("nop");                                 /* =-100               =-100        =-100                   */                  
result=PIDcal(0x44,0x33);
asm("nop");                                 /* =100                =100         =100                    */
result=PIDcal(0x44,0x42);
asm("nop");                                 /* =45                 =50          =45                     */
result=PIDcal(0x44,0x43); 
asm("nop");                                 /* =29                 =35         =30                      */
result=PIDcal(0x44,0x44);     
asm("nop");                                 /* =0                  =5          =-100                    */
result=PIDcal(0x44,0x44);     
asm("nop");                                 /* =0                  =6          =0                       */
result=PIDcal(0x44,0x44);      
asm("nop");                                 /*                     =6          =0                       */
}

}

#ifdef FIXED_POINT_PID
sint16_t PIDcal(sint8_t setpoint,sint8_t actual_position)
{
	static sint16_t pre_error = 0;
	static sint32_t integral = 0;
	sint32_t error;
	sint32_t derivative;
	sint32_t output;
 

    //Caculate P,I,D
	error = (setpoint - actual_position);

	//In case of error too small then stop intergration
	if(error >= epsilon)
	{
		integral = integral + error*dt;
	}
     else 
     {
       integral = 0;                      /* This should make this term force to zero if not integrating due to too small or zero error*/
     }   
     
     derivative = (error - pre_error)*100; 
 
     output = Kp*error + Ki*integral + Kd*derivative;
     output = output/100;
      
	//Saturation Filter
	if(output > (MAX - offset))
	{
		output = MAX - offset;
	}
	else if(output < ZERO)
	{
		output = MIN + offset;
	}
        //Update error
        pre_error = error;

 return (output);
}
#endif








#ifdef FLOATING_POINT_PID
float PIDcal(float setpoint,float actual_position)
{
	static float pre_error = 0;
	static float integral = 0;
	float error;
	float derivative;
	float output;

	//Caculate P,I,D
	error = setpoint - actual_position;

	//In case of error too small then stop intergration
	if(abs(error) > epsilon)
	{
		integral = integral + error*dt;
	}
	derivative = (error - pre_error)/dt;
	output = Kp*error + Ki*integral + Kd*derivative;

	//Saturation Filter
	if(output > MAX)
	{
		output = MAX;
	}
	else if(output < MIN)
	{
		output = MIN;
	}
        //Update error
        pre_error = error;

 return output;
}
#endif

float abs(float a)
{
if (a < 0)
 {  
   return -a;
 }
 else
 {
  return a;
 }
}
