#include <p32xxxx.h>			// Required for SFR defs
#include "GenericTypeDefs.h"
#include "MoreTypeDefs.h"
#include "specificTypeDefs.h"

/********************************************************************************************************
/*
/*        MY FIRST EXPOSURE TO THE MPLAB C32 COMPILIER 
/*
/********************************************************************************************************/

/* TYPE DEFS*/

typedef struct
            {
             INT mem1;
             INT mem2;
             INT8 mem_bit0    :1;
             INT8 mem_bits1   :2;
             INT8 mem_bits2   :5; 
             INT8 array[5];
            }TEST_STRUCT;

/* Constants Arrays*/
const INT block01[10];
INT block02[20];
INT return_proc;
/* Function prototypes*/
void main (void);
INT checksum(INT *data, INT length);
INT proc_block01(void);

/*********************************************************************/
/*        THE MAIN LOOP                                              */         
/*********************************************************************/

void main (void)
{
float divide;
TEST_STRUCT see_work;
INT test_switch;
INT var;
INT count;
INT size_of;
im_hwo_type im_type_test;

im_type_test.hwo_tk_lmp = 1;
im_type_test.hwo_vta_lamp = 1;
count = 0;
test_switch       = 1;


see_work.mem1      = 0xBA;
see_work.mem2      = 0xDE;
see_work.mem_bits1 = 0x03;
see_work.array[1]  = 0x55;
size_of = sizeof(see_work.array[1]);
while(1)
 {
  divide = 600/9.0;
   switch(test_switch)
    {
      case 1:
       var = 1;
       var = 99;
       var = 100;
       var = 1;
       test_switch = 2;
      break;
   
      case 2:
       var = 2;
       test_switch = 3;
      break;

      case 3:
       var = 3;
       test_switch = 1;  /* loop forever*/
      break;

      default:
        break;
    } 

 }

}


INT checksum(INT *data, INT length)
{
INT res;
INT i;

  res = 0;
  for(i = 0; i <=length; i++)
  {
    res += (INT)*data;
  }		

 return (res & 0x00ff); 	
}	



INT proc_block01(void)
{
INT temp;	
		
 temp = checksum(block01,10);
 return(temp);
}