/*******************************************************************

                  Generic Type Definitions

********************************************************************
 FileName:        GenericTypeDefs.h
 Dependencies:    None
 Processor:       PIC10, PIC12, PIC16, PIC18, PIC24, dsPIC, PIC32
 Compiler:        MPLAB C Compilers for PIC18, PIC24, dsPIC, & PIC32
                  Hi-Tech PICC PRO, Hi-Tech PICC18 PRO
 Company:         Microchip Technology Inc.

 Software License Agreement

 The software supplied herewith by Microchip Technology Incorporated
 (the "Company") is intended and supplied to you, the Company's
 customer, for use solely and exclusively with products manufactured
 by the Company.

 The software is owned by the Company and/or its supplier, and is
 protected under applicable copyright laws. All rights are reserved.
 Any use in violation of the foregoing restrictions may subject the
 user to criminal sanctions under applicable laws, as well as to
 civil liability for the breach of the terms and conditions of this
 license.

 THIS SOFTWARE IS PROVIDED IN AN "AS IS" CONDITION. NO WARRANTIES,
 WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED
 TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. THE COMPANY SHALL NOT,
 IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL OR
 CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.

********************************************************************
 File Description:

 Change History:
  Rev   Date         Description
  1.1   09/11/06     Add base signed types
  1.2   02/28/07     Add QWORD, LONGLONG, QWORD_VAL
  1.3   02/06/08     Add def's for PIC32
  1.4   08/08/08     Remove LSB/MSB Macros, adopted by Peripheral lib
  1.5   08/14/08     Simplify file header
  Draft 2.0   07/13/09     Updated for new release of coding standards
*******************************************************************/
#include "GenericTypeDefs.h"
#ifndef MORE_TYPE_DEFS
#define MORE_TYPE_DEFS
typedef struct
{  INT8     hwo_tk_lmp					:1;	// Trunk Lamp
   INT8  	hwo_ugdo_enbl				:1;	// Ugdo Enabled
   INT8  	hwo_vta_lamp				:1;	// VTA output
   INT8 	hwo_InadvertentLight		:1;	// (load shed after 15 min in low power mode)
   INT8  	hwo_inverter_on				:1; // HWO_INVERTER_ON (power inverter output) - added for UF
   INT8  	hwo_RearDefrostOut		    :1;	// Rear EBL
   INT8  	hwo_DrvDrSwPumpStart		:1;	// Driver Door SW Pump Start (pulsed signal to trans hydralic pump)
   INT8	    hwo_spare_bit4_7			:1;
 
} im_hwo_type;

#endif /* MORE_TYPE_DEFS*/
