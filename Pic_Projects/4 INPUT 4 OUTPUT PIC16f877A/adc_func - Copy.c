#include <htc.h>
#include "typedefs.h"
#include "adc_func.h"
//#include "timer.h"

#define ADC_MAX  (0x3FF)
#define ADC_BITS (10)
/* Macro tested in main*/

#define ADC_FULL_SCALE_MV         (5000)
#define ADC_BITS                  (10)
#define ADC_MULTIPLY(x,y)         ((uint32_t)(x)*(uint32_t)(y))
#define adc_volt_convert_macro(x) ((uint16_t)(ADC_MULTIPLY(x,ADC_FULL_SCALE_MV) >> ADC_BITS))
#define MAX_COUNT                 (10)                                                           /* used in linear_Motor()    

//#define  adc_volt_convert_macro(x) (((x)*(.00488))*(1000)) // conversion macro raw adc count to volts in mV
/* variables*/
uint16_t int_count=0;
uint16_t adc_isr_flag;



const uint16_t motor_speed_array[] =  
{                           // FROM EXCELL
  0x000A,                   // index 0  speed 125 RPM
  0x000B,                   // index 1  speed 115 RPM  
  0x000C,                   // index 2  speed 105 RPM
  0x000D,                   // index 3  speed 95  RPM
  0x000F,                   // index 4  speed 85  RPM
  0x0011,                   // index 5  speed 75  RPM
  0x0013,                   // index 6  speed 65  RPM                  
  0x0017,                   // index 7  speed 55  RPM
  0x001C,                   // index 8  speed 45  RPM
  0x0024,                   // index 9  speed 35  RPM
  0x0032,                   // index 10 speed 25  RPM
  0x0053,                   // index 11 speed 15  RPM
  0x00FA,                   // index 12 speed 05  RPM
  0x01A1,                   // index 13 speed 03  RPM
  0x0271,                   // index 14 speed 02  RPM  
  0x1388                    // index 15 VERY slow
};
/*function prototypes*/


void adc_isr(void);

uint16_t adc_volt_convert(void)
{
/* Variables*/
uint16_t adc_rawvalue;
uint16_t adc_millivolt_macroval;
uint16_t adc_millivolt=0;

adc_rawvalue = adc_convert();                                  // Get Adc value
PORTC = 0x40;
adc_millivolt_macroval = adc_volt_convert_macro(adc_rawvalue); // invoke the macro 
PORTC = 0x00;
adc_millivolt = adc_millivolt_macroval;

asm("nop");
return adc_millivolt;                                          // send out this to any call that wants it 

}


uint16_t adc_convert(void)
{
  uint16_t adresh;                   //return value from the atd conversion 
  uint16_t adresl;                   //return value from the atd conversion
  uint16_t result; 
  adc_isr_flag = 0;                  //*CRITICAL* INTERRUPT FLAG MUST BE SET BEFORE CONVERTING
 
  GODONE = 1;                        //*CRITICAL* INTERRUPT FLAG MUST BE SET BEFORE CONVERTING
  
  while (adc_isr_flag == 0)           // Did an interrupt happened to a conversion
  {}
  adresl = ADRESL;                    // 
  adresh = ADRESH;                    //
  result = ((adresh << 8) |(adresl)); // get into word format
 
  return result;
}






void adc_init(void)
{
  ADCON0 = 0xC5;    //|ADCS1 = 1|ADCS0 = 1|CHS2 =0|CHS1=0|CHS0=0|Go/DONE=0|ADON = 1|  A/D clock is FRC internal RC, analog channel = AN0 ,ADON = selected
  ADCON1 = 0x80;    //|ADFM=0 Right justify|
}

void adc_isr(void)
{
  if ((ADIF)&&(ADIE))
  {
    
    ADIF=0;          // clear flag
    adc_isr_flag=1;  // set the indication 
    int_count++; 
  }
  else 
  {
  }
}