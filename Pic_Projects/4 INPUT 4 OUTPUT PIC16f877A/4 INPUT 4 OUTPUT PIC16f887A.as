opt subtitle "HI-TECH Software Omniscient Code Generator (Lite mode) build 6738"

opt pagewidth 120

	opt lm

	processor	16F877A
clrc	macro
	bcf	3,0
	endm
clrz	macro
	bcf	3,2
	endm
setc	macro
	bsf	3,0
	endm
setz	macro
	bsf	3,2
	endm
skipc	macro
	btfss	3,0
	endm
skipz	macro
	btfss	3,2
	endm
skipnc	macro
	btfsc	3,0
	endm
skipnz	macro
	btfsc	3,2
	endm
indf	equ	0
indf0	equ	0
pc	equ	2
pcl	equ	2
status	equ	3
fsr	equ	4
fsr0	equ	4
c	equ	1
z	equ	0
pclath	equ	10
# 36 "F:\pic_projects\4 INPUT 4 OUTPUT PIC16f877A\main.c"
	psect config,class=CONFIG,delta=2 ;#
# 36 "F:\pic_projects\4 INPUT 4 OUTPUT PIC16f877A\main.c"
	dw 0x3FFF & 0x3FFF & 0x3FBF & 0x3FFF & 0x3FFF & 0x3FFF & 0x3FFF & 0x3FFF & 0x3FFE ;#
	FNCALL	_main,_init
	FNCALL	_main,_adc_init
	FNCALL	_main,_uart_init
	FNCALL	_main,_timer_set
	FNCALL	_main,_blink_infinite_led
	FNCALL	_main,_uart_rx
	FNCALL	_main,_blink_led
	FNCALL	_main,_led_rx
	FNCALL	_main,_uart_tx
	FNCALL	_main,_led_rx_and_adc
	FNCALL	_uart_tx,_adc_convert
	FNCALL	_blink_led,_get_timer
	FNCALL	_blink_led,_timer_set
	FNCALL	_uart_rx,_eeprom_write
	FNCALL	_uart_rx,_uart_init
	FNCALL	_blink_infinite_led,_get_timer
	FNCALL	_blink_infinite_led,_timer_set
	FNCALL	_init,_eeprom_write
	FNROOT	_main
	FNCALL	_interrupt_handler,_adc_isr
	FNCALL	_interrupt_handler,_timer_isr
	FNCALL	intlevel1,_interrupt_handler
	global	intlevel1
	FNROOT	intlevel1
	global	blink_infinite_led@led_blink
	global	blink_led@led_blink
psect	idataBANK0,class=CODE,space=0,delta=2
global __pidataBANK0
__pidataBANK0:
	file	"F:\pic_projects\4 INPUT 4 OUTPUT PIC16f877A\main.c"
	line	200

;initializer for blink_infinite_led@led_blink
	retlw	01h
	retlw	0

	line	235

;initializer for blink_led@led_blink
	retlw	03h
	retlw	0

	global	_motor_speed_array
psect	strings,class=STRING,delta=2
global __pstrings
__pstrings:
;	global	stringdir,stringtab,__stringbase
stringtab:
;	String table - string pointers are 1 byte each
stringcode:stringdir:
movlw high(stringdir)
movwf pclath
movf fsr,w
incf fsr
	addwf pc
__stringbase:
	retlw	0
psect	strings
	file	"F:\pic_projects\4 INPUT 4 OUTPUT PIC16f877A\adc_func.c"
	line	23
_motor_speed_array:
	retlw	0Ah
	retlw	0

	retlw	0Bh
	retlw	0

	retlw	0Ch
	retlw	0

	retlw	0Dh
	retlw	0

	retlw	0Fh
	retlw	0

	retlw	011h
	retlw	0

	retlw	013h
	retlw	0

	retlw	017h
	retlw	0

	retlw	01Ch
	retlw	0

	retlw	024h
	retlw	0

	retlw	032h
	retlw	0

	retlw	053h
	retlw	0

	retlw	0FAh
	retlw	0

	retlw	0A1h
	retlw	01h

	retlw	071h
	retlw	02h

	retlw	088h
	retlw	013h

	global	_portd_off_val
psect	strings
	file	"F:\pic_projects\4 INPUT 4 OUTPUT PIC16f877A\uart_func.c"
	line	27
_portd_off_val:
	retlw	0FEh
	retlw	0FDh
	retlw	0FBh
	retlw	0F7h
	retlw	0EFh
	retlw	0DFh
	retlw	0BFh
	retlw	07Fh
	global	_portd_on_val
psect	strings
	file	"F:\pic_projects\4 INPUT 4 OUTPUT PIC16f877A\uart_func.c"
	line	14
_portd_on_val:
	retlw	01h
	retlw	02h
	retlw	04h
	retlw	08h
	retlw	010h
	retlw	020h
	retlw	040h
	retlw	080h
	global	_motor_speed_array
	global	_portd_off_val
	global	_portd_on_val
	global	_timer_array
	global	_adc_isr_flag
	global	_int_count
	global	blink_led@count
	global	_tmr1_isr_counter
	global	_teststruct
	global	_toms_variable
	global	_uartrx_isr_flag
	global	_uarttx_isr_flag
	global	led_rx@rcv_byte
	global	led_rx_and_adc@rcv_byte
	global	_watch_dog_count
psect	nvBANK0,class=BANK0,space=1
global __pnvBANK0
__pnvBANK0:
_watch_dog_count:
       ds      2

	global	_ADCON0
_ADCON0	set	31
	global	_ADRESH
_ADRESH	set	30
	global	_PORTB
_PORTB	set	6
	global	_PORTC
_PORTC	set	7
	global	_PORTD
_PORTD	set	8
	global	_RCREG
_RCREG	set	26
	global	_RCSTA
_RCSTA	set	24
	global	_T1CON
_T1CON	set	16
	global	_TMR1H
_TMR1H	set	15
	global	_TMR1L
_TMR1L	set	14
	global	_TXREG
_TXREG	set	25
	global	_ADIF
_ADIF	set	102
	global	_CARRY
_CARRY	set	24
	global	_CREN
_CREN	set	196
	global	_GIE
_GIE	set	95
	global	_GODONE
_GODONE	set	250
	global	_OERR
_OERR	set	193
	global	_PEIE
_PEIE	set	94
	global	_RCIF
_RCIF	set	101
	global	_TMR1IF
_TMR1IF	set	96
	global	_TO
_TO	set	28
	global	_TXIF
_TXIF	set	100
	global	_ADCON1
_ADCON1	set	159
	global	_ADRESL
_ADRESL	set	158
	global	_OPTION
_OPTION	set	129
	global	_SPBRG
_SPBRG	set	153
	global	_TRISA
_TRISA	set	133
	global	_TRISB
_TRISB	set	134
	global	_TRISD
_TRISD	set	136
	global	_TXSTA
_TXSTA	set	152
	global	_ADIE
_ADIE	set	1126
	global	_RCIE
_RCIE	set	1125
	global	_TMR1IE
_TMR1IE	set	1120
	global	_TRMT
_TRMT	set	1217
	global	_TXIE
_TXIE	set	1124
	global	_EEADR
_EEADR	set	269
	global	_EEDATA
_EEDATA	set	268
	global	_EECON1
_EECON1	set	396
	global	_EECON2
_EECON2	set	397
	global	_WR
_WR	set	3169
	global	_WREN
_WREN	set	3170
	file	"4 INPUT 4 OUTPUT PIC16f887A.as"
	line	#
psect cinit,class=CODE,delta=2
global start_initialization
start_initialization:

psect	bssCOMMON,class=COMMON,space=1
global __pbssCOMMON
__pbssCOMMON:
_teststruct:
       ds      1

_toms_variable:
       ds      1

_uartrx_isr_flag:
       ds      1

_uarttx_isr_flag:
       ds      1

led_rx@rcv_byte:
       ds      1

led_rx_and_adc@rcv_byte:
       ds      1

psect	bssBANK0,class=BANK0,space=1
global __pbssBANK0
__pbssBANK0:
_timer_array:
       ds      4

_adc_isr_flag:
       ds      2

_int_count:
       ds      2

blink_led@count:
       ds      2

_tmr1_isr_counter:
       ds      1

psect	dataBANK0,class=BANK0,space=1
global __pdataBANK0
__pdataBANK0:
	file	"F:\pic_projects\4 INPUT 4 OUTPUT PIC16f877A\main.c"
	line	200
blink_infinite_led@led_blink:
       ds      2

psect	dataBANK0
	file	"F:\pic_projects\4 INPUT 4 OUTPUT PIC16f877A\main.c"
	line	235
blink_led@led_blink:
       ds      2

; Clear objects allocated to COMMON
psect cinit,class=CODE,delta=2
	clrf	((__pbssCOMMON)+0)&07Fh
	clrf	((__pbssCOMMON)+1)&07Fh
	clrf	((__pbssCOMMON)+2)&07Fh
	clrf	((__pbssCOMMON)+3)&07Fh
	clrf	((__pbssCOMMON)+4)&07Fh
	clrf	((__pbssCOMMON)+5)&07Fh
; Clear objects allocated to BANK0
psect cinit,class=CODE,delta=2
	clrf	((__pbssBANK0)+0)&07Fh
	clrf	((__pbssBANK0)+1)&07Fh
	clrf	((__pbssBANK0)+2)&07Fh
	clrf	((__pbssBANK0)+3)&07Fh
	clrf	((__pbssBANK0)+4)&07Fh
	clrf	((__pbssBANK0)+5)&07Fh
	clrf	((__pbssBANK0)+6)&07Fh
	clrf	((__pbssBANK0)+7)&07Fh
	clrf	((__pbssBANK0)+8)&07Fh
	clrf	((__pbssBANK0)+9)&07Fh
	clrf	((__pbssBANK0)+10)&07Fh
; Initialize objects allocated to BANK0
	global __pidataBANK0
psect cinit,class=CODE,delta=2
	fcall	__pidataBANK0+0		;fetch initializer
	movwf	__pdataBANK0+0&07fh		
	fcall	__pidataBANK0+1		;fetch initializer
	movwf	__pdataBANK0+1&07fh		
	fcall	__pidataBANK0+2		;fetch initializer
	movwf	__pdataBANK0+2&07fh		
	fcall	__pidataBANK0+3		;fetch initializer
	movwf	__pdataBANK0+3&07fh		
psect cinit,class=CODE,delta=2
global end_of_initialization

;End of C runtime variable initialization code

end_of_initialization:
clrf status
ljmp _main	;jump to C main() function
psect	cstackCOMMON,class=COMMON,space=1
global __pcstackCOMMON
__pcstackCOMMON:
	global	?_init
?_init:	; 0 bytes @ 0x0
	global	?_adc_init
?_adc_init:	; 0 bytes @ 0x0
	global	?_uart_init
?_uart_init:	; 0 bytes @ 0x0
	global	?_blink_infinite_led
?_blink_infinite_led:	; 0 bytes @ 0x0
	global	?_led_rx
?_led_rx:	; 0 bytes @ 0x0
	global	?_uart_tx
?_uart_tx:	; 0 bytes @ 0x0
	global	?_led_rx_and_adc
?_led_rx_and_adc:	; 0 bytes @ 0x0
	global	?_timer_isr
?_timer_isr:	; 0 bytes @ 0x0
	global	??_timer_isr
??_timer_isr:	; 0 bytes @ 0x0
	global	?_adc_isr
?_adc_isr:	; 0 bytes @ 0x0
	global	??_adc_isr
??_adc_isr:	; 0 bytes @ 0x0
	global	?_interrupt_handler
?_interrupt_handler:	; 0 bytes @ 0x0
	global	?_uart_rx
?_uart_rx:	; 1 bytes @ 0x0
	global	?_main
?_main:	; 2 bytes @ 0x0
	ds	1
	global	timer_isr@i
timer_isr@i:	; 1 bytes @ 0x1
	ds	1
	global	??_interrupt_handler
??_interrupt_handler:	; 0 bytes @ 0x2
	ds	4
psect	cstackBANK0,class=BANK0,space=1
global __pcstackBANK0
__pcstackBANK0:
	global	??_adc_init
??_adc_init:	; 0 bytes @ 0x0
	global	??_uart_init
??_uart_init:	; 0 bytes @ 0x0
	global	?_timer_set
?_timer_set:	; 0 bytes @ 0x0
	global	??_led_rx
??_led_rx:	; 0 bytes @ 0x0
	global	??_led_rx_and_adc
??_led_rx_and_adc:	; 0 bytes @ 0x0
	global	?_eeprom_write
?_eeprom_write:	; 0 bytes @ 0x0
	global	?_get_timer
?_get_timer:	; 2 bytes @ 0x0
	global	?_adc_convert
?_adc_convert:	; 2 bytes @ 0x0
	global	eeprom_write@value
eeprom_write@value:	; 1 bytes @ 0x0
	global	timer_set@value
timer_set@value:	; 2 bytes @ 0x0
	ds	1
	global	??_eeprom_write
??_eeprom_write:	; 0 bytes @ 0x1
	ds	1
	global	??_timer_set
??_timer_set:	; 0 bytes @ 0x2
	global	??_get_timer
??_get_timer:	; 0 bytes @ 0x2
	global	??_adc_convert
??_adc_convert:	; 0 bytes @ 0x2
	global	led_rx@final_led_val
led_rx@final_led_val:	; 1 bytes @ 0x2
	global	led_rx_and_adc@final_led_val
led_rx_and_adc@final_led_val:	; 1 bytes @ 0x2
	global	eeprom_write@addr
eeprom_write@addr:	; 1 bytes @ 0x2
	ds	1
	global	??_init
??_init:	; 0 bytes @ 0x3
	global	??_uart_rx
??_uart_rx:	; 0 bytes @ 0x3
	global	led_rx@led_on
led_rx@led_on:	; 1 bytes @ 0x3
	global	led_rx_and_adc@led_on
led_rx_and_adc@led_on:	; 1 bytes @ 0x3
	global	timer_set@index
timer_set@index:	; 1 bytes @ 0x3
	global	get_timer@result
get_timer@result:	; 2 bytes @ 0x3
	ds	1
	global	led_rx@rcv_byte_new
led_rx@rcv_byte_new:	; 1 bytes @ 0x4
	global	led_rx_and_adc@rcv_byte_new
led_rx_and_adc@rcv_byte_new:	; 1 bytes @ 0x4
	global	adc_convert@adresh
adc_convert@adresh:	; 2 bytes @ 0x4
	global	uart_rx@eorr_count
uart_rx@eorr_count:	; 2 bytes @ 0x4
	ds	1
	global	led_rx@index
led_rx@index:	; 1 bytes @ 0x5
	global	led_rx_and_adc@led_state
led_rx_and_adc@led_state:	; 1 bytes @ 0x5
	global	get_timer@index
get_timer@index:	; 1 bytes @ 0x5
	ds	1
	global	??_blink_infinite_led
??_blink_infinite_led:	; 0 bytes @ 0x6
	global	?_blink_led
?_blink_led:	; 2 bytes @ 0x6
	global	uart_rx@rcv_byte
uart_rx@rcv_byte:	; 1 bytes @ 0x6
	global	led_rx@led_state
led_rx@led_state:	; 1 bytes @ 0x6
	global	led_rx_and_adc@index
led_rx_and_adc@index:	; 1 bytes @ 0x6
	global	blink_led@blinks
blink_led@blinks:	; 2 bytes @ 0x6
	global	adc_convert@adresl
adc_convert@adresl:	; 2 bytes @ 0x6
	ds	1
	global	led_rx@led_num
led_rx@led_num:	; 1 bytes @ 0x7
	global	led_rx_and_adc@led_num
led_rx_and_adc@led_num:	; 1 bytes @ 0x7
	ds	1
	global	??_blink_led
??_blink_led:	; 0 bytes @ 0x8
	global	adc_convert@result
adc_convert@result:	; 2 bytes @ 0x8
	ds	2
	global	??_uart_tx
??_uart_tx:	; 0 bytes @ 0xA
	ds	2
	global	uart_tx@adc_hiaddrch
uart_tx@adc_hiaddrch:	; 1 bytes @ 0xC
	ds	1
	global	uart_tx@chn_sel
uart_tx@chn_sel:	; 1 bytes @ 0xD
	ds	1
	global	uart_tx@rawadc_in
uart_tx@rawadc_in:	; 2 bytes @ 0xE
	ds	2
	global	uart_tx@adc_lo
uart_tx@adc_lo:	; 2 bytes @ 0x10
	ds	2
	global	uart_tx@adc_hi
uart_tx@adc_hi:	; 2 bytes @ 0x12
	ds	2
	global	??_main
??_main:	; 0 bytes @ 0x14
	ds	3
	global	main@test_chnsel
main@test_chnsel:	; 1 bytes @ 0x17
	ds	1
	global	main@blinking_while_condition
main@blinking_while_condition:	; 1 bytes @ 0x18
	ds	1
	global	main@exit
main@exit:	; 1 bytes @ 0x19
	ds	1
	global	main@mainprogressflags
main@mainprogressflags:	; 1 bytes @ 0x1A
	ds	1
	global	main@rcv_chn
main@rcv_chn:	; 1 bytes @ 0x1B
	ds	1
	global	main@while_condition
main@while_condition:	; 1 bytes @ 0x1C
	ds	1
;;Data sizes: Strings 0, constant 48, data 4, bss 17, persistent 2 stack 0
;;Auto spaces:   Size  Autos    Used
;; COMMON          14      6      12
;; BANK0           80     29      46
;; BANK1           80      0       0
;; BANK3           96      0       0
;; BANK2           96      0       0

;;
;; Pointer list with targets:

;; ?_get_timer	unsigned short  size(1) Largest target is 0
;;
;; ?_adc_convert	unsigned short  size(1) Largest target is 0
;;


;;
;; Critical Paths under _main in COMMON
;;
;;   None.
;;
;; Critical Paths under _interrupt_handler in COMMON
;;
;;   _interrupt_handler->_timer_isr
;;
;; Critical Paths under _main in BANK0
;;
;;   _main->_uart_tx
;;   _uart_tx->_adc_convert
;;   _blink_led->_get_timer
;;   _uart_rx->_eeprom_write
;;   _blink_infinite_led->_get_timer
;;   _init->_eeprom_write
;;
;; Critical Paths under _interrupt_handler in BANK0
;;
;;   None.
;;
;; Critical Paths under _main in BANK1
;;
;;   None.
;;
;; Critical Paths under _interrupt_handler in BANK1
;;
;;   None.
;;
;; Critical Paths under _main in BANK3
;;
;;   None.
;;
;; Critical Paths under _interrupt_handler in BANK3
;;
;;   None.
;;
;; Critical Paths under _main in BANK2
;;
;;   None.
;;
;; Critical Paths under _interrupt_handler in BANK2
;;
;;   None.

;;
;;Main: autosize = 0, tempsize = 3, incstack = 0, save=0
;;

;;
;;Call Graph Tables:
;;
;; ---------------------------------------------------------------------------------
;; (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;; ---------------------------------------------------------------------------------
;; (0) _main                                                11    11      0    2342
;;                                             20 BANK0      9     9      0
;;                               _init
;;                           _adc_init
;;                          _uart_init
;;                          _timer_set
;;                 _blink_infinite_led
;;                            _uart_rx
;;                          _blink_led
;;                             _led_rx
;;                            _uart_tx
;;                     _led_rx_and_adc
;; ---------------------------------------------------------------------------------
;; (1) _uart_tx                                             11    11      0     433
;;                                             10 BANK0     10    10      0
;;                        _adc_convert
;; ---------------------------------------------------------------------------------
;; (1) _blink_led                                            3     1      2     223
;;                                              6 BANK0      3     1      2
;;                          _get_timer
;;                          _timer_set
;; ---------------------------------------------------------------------------------
;; (1) _uart_rx                                              4     4      0     130
;;                                              3 BANK0      4     4      0
;;                       _eeprom_write
;;                          _uart_init
;; ---------------------------------------------------------------------------------
;; (1) _blink_infinite_led                                   0     0      0     192
;;                          _get_timer
;;                          _timer_set
;; ---------------------------------------------------------------------------------
;; (1) _init                                                 0     0      0      62
;;                       _eeprom_write
;; ---------------------------------------------------------------------------------
;; (2) _adc_convert                                         10     8      2     102
;;                                              0 BANK0     10     8      2
;; ---------------------------------------------------------------------------------
;; (2) _get_timer                                            6     4      2      99
;;                                              0 BANK0      6     4      2
;; ---------------------------------------------------------------------------------
;; (2) _eeprom_write                                         3     2      1      62
;;                                              0 BANK0      3     2      1
;; ---------------------------------------------------------------------------------
;; (1) _led_rx_and_adc                                       8     8      0     424
;;                                              0 BANK0      8     8      0
;; ---------------------------------------------------------------------------------
;; (1) _led_rx                                               8     8      0     393
;;                                              0 BANK0      8     8      0
;; ---------------------------------------------------------------------------------
;; (2) _timer_set                                            6     4      2      93
;;                                              0 BANK0      4     2      2
;; ---------------------------------------------------------------------------------
;; (2) _uart_init                                            0     0      0       0
;; ---------------------------------------------------------------------------------
;; (1) _adc_init                                             0     0      0       0
;; ---------------------------------------------------------------------------------
;; Estimated maximum stack depth 2
;; ---------------------------------------------------------------------------------
;; (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;; ---------------------------------------------------------------------------------
;; (3) _interrupt_handler                                    4     4      0      90
;;                                              2 COMMON     4     4      0
;;                            _adc_isr
;;                          _timer_isr
;; ---------------------------------------------------------------------------------
;; (4) _timer_isr                                            2     2      0      90
;;                                              0 COMMON     2     2      0
;; ---------------------------------------------------------------------------------
;; (4) _adc_isr                                              0     0      0       0
;; ---------------------------------------------------------------------------------
;; Estimated maximum stack depth 4
;; ---------------------------------------------------------------------------------

;; Call Graph Graphs:

;; _main (ROOT)
;;   _init
;;     _eeprom_write
;;   _adc_init
;;   _uart_init
;;   _timer_set
;;   _blink_infinite_led
;;     _get_timer
;;     _timer_set
;;   _uart_rx
;;     _eeprom_write
;;     _uart_init
;;   _blink_led
;;     _get_timer
;;     _timer_set
;;   _led_rx
;;   _uart_tx
;;     _adc_convert
;;   _led_rx_and_adc
;;
;; _interrupt_handler (ROOT)
;;   _adc_isr
;;   _timer_isr
;;

;; Address spaces:

;;Name               Size   Autos  Total    Cost      Usage
;;BANK3               60      0       0       9        0.0%
;;BITBANK3            60      0       0       8        0.0%
;;SFR3                 0      0       0       4        0.0%
;;BITSFR3              0      0       0       4        0.0%
;;BANK2               60      0       0      11        0.0%
;;BITBANK2            60      0       0      10        0.0%
;;SFR2                 0      0       0       5        0.0%
;;BITSFR2              0      0       0       5        0.0%
;;SFR1                 0      0       0       2        0.0%
;;BITSFR1              0      0       0       2        0.0%
;;BANK1               50      0       0       7        0.0%
;;BITBANK1            50      0       0       6        0.0%
;;CODE                 0      0       0       0        0.0%
;;DATA                 0      0      40      12        0.0%
;;ABS                  0      0      3A       3        0.0%
;;NULL                 0      0       0       0        0.0%
;;STACK                0      0       6       2        0.0%
;;BANK0               50     1D      2E       5       57.5%
;;BITBANK0            50      0       0       4        0.0%
;;SFR0                 0      0       0       1        0.0%
;;BITSFR0              0      0       0       1        0.0%
;;COMMON               E      6       C       1       85.7%
;;BITCOMMON            E      0       0       0        0.0%
;;EEDATA             100      0       0       0        0.0%

	global	_main
psect	maintext,global,class=CODE,delta=2
global __pmaintext
__pmaintext:

;; *************** function _main *****************
;; Defined at:
;;		line 57 in file "F:\pic_projects\4 INPUT 4 OUTPUT PIC16f877A\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  adc_result      2    0        unsigned short 
;;  while_condit    1   28[BANK0 ] unsigned char 
;;  rcv_chn         1   27[BANK0 ] unsigned char 
;;  mainprogress    1   26[BANK0 ] struct .
;;  exit            1   25[BANK0 ] unsigned char 
;;  blinking_whi    1   24[BANK0 ] unsigned char 
;;  test_chnsel     1   23[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;                  2  588[COMMON] int 
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, btemp+1, pclath, cstack
;; Tracked objects:
;;		On entry : 17F/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       6       0       0       0
;;      Temps:          0       3       0       0       0
;;      Totals:         0       9       0       0       0
;;Total ram usage:        9 bytes
;; Hardware stack levels required when called:    4
;; This function calls:
;;		_init
;;		_adc_init
;;		_uart_init
;;		_timer_set
;;		_blink_infinite_led
;;		_uart_rx
;;		_blink_led
;;		_led_rx
;;		_uart_tx
;;		_led_rx_and_adc
;; This function is called by:
;;		Startup code after reset
;; This function uses a non-reentrant model
;;
psect	maintext
	file	"F:\pic_projects\4 INPUT 4 OUTPUT PIC16f877A\main.c"
	line	57
	global	__size_of_main
	__size_of_main	equ	__end_of_main-_main
	
_main:	
	opt	stack 4
; Regs used in _main: [wreg-fsr0h+status,2+status,0+btemp+1+pclath+cstack]
	line	61
	
l5447:	
;main.c: 61: watch_dog_count = 0;
	movlw	low(0)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(_watch_dog_count)
	movlw	high(0)
	movwf	((_watch_dog_count))+1
	line	62
	
l5449:	
;main.c: 62: uint8_t test_chnsel=0;
	clrc
	movlw	0
	btfsc	status,0
	movlw	1
	movwf	(main@test_chnsel)
	line	67
	
l5451:	
;main.c: 63: uint16_t adc_result;
;main.c: 64: uint8_t rcv_chn;
;main.c: 65: uint8_t while_condition;
;main.c: 66: uint8_t blinking_while_condition;
;main.c: 67: uint8_t exit=0;
	clrc
	movlw	0
	btfsc	status,0
	movlw	1
	movwf	(main@exit)
	line	82
	
l5453:	
;main.c: 69: struct
;main.c: 70: {
;main.c: 71: uint8_t inits : 1;
;main.c: 72: uint8_t infinite_blink : 1;
;main.c: 73: uint8_t indicator_blink : 1;
;main.c: 74: uint8_t padding : 1;
;main.c: 75: uint8_t while_con : 4;
;main.c: 76: }
;main.c: 77: mainprogressflags;
;main.c: 82: mainprogressflags.inits = FALSE;
	bcf	(main@mainprogressflags),0
	line	83
	
l5455:	
;main.c: 83: mainprogressflags.infinite_blink = FALSE;
	bcf	(main@mainprogressflags),1
	line	84
	
l5457:	
;main.c: 84: mainprogressflags.indicator_blink = FALSE;
	bcf	(main@mainprogressflags),2
	line	85
	
l5459:	
;main.c: 85: mainprogressflags.padding = FALSE;
	bcf	(main@mainprogressflags),3
	line	86
	
l5461:	
;main.c: 86: mainprogressflags.while_con = FALSE;
	movlw	((0 & ((1<<4)-1))<<4)|not (((1<<4)-1)<<4)
	andwf	(main@mainprogressflags),f
	line	88
	
l5463:	
;main.c: 88: init();
	fcall	_init
	line	90
	
l5465:	
;main.c: 90: mainprogressflags.inits = TRUE;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	bsf	(main@mainprogressflags),0
	line	92
	
l5467:	
;main.c: 92: TMR1IE = 1;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bsf	(1120/8)^080h,(1120)&7
	line	93
	
l5469:	
;main.c: 93: ADIE = 1;
	bsf	(1126/8)^080h,(1126)&7
	line	95
	
l5471:	
;main.c: 95: PEIE = 1;
	bsf	(94/8),(94)&7
	line	96
	
l5473:	
;main.c: 96: (GIE = 1);
	bsf	(95/8),(95)&7
	line	97
	
l5475:	
;main.c: 97: adc_init();
	fcall	_adc_init
	line	98
	
l5477:	
;main.c: 98: uart_init();
	fcall	_uart_init
	line	103
	
l5479:	
;main.c: 103: timer_set(TIMER_1,10);
	movlw	low(0Ah)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(?_timer_set)
	movlw	high(0Ah)
	movwf	((?_timer_set))+1
	movlw	(0)
	fcall	_timer_set
	line	104
;main.c: 104: while((uart_rx() != (0x0A)) && (uart_rx() != (0x0B)) && (uart_rx() != (0X0C) ))
	goto	l5485
	
l590:	
	line	106
	
l5481:	
;main.c: 105: {
;main.c: 106: blink_infinite_led();
	fcall	_blink_infinite_led
	line	107
	
l5483:	
# 107 "F:\pic_projects\4 INPUT 4 OUTPUT PIC16f877A\main.c"
clrwdt ;#
psect	maintext
	goto	l5485
	line	109
	
l589:	
	line	104
	
l5485:	
	fcall	_uart_rx
	xorlw	0Ah
	skipnz
	goto	u3401
	goto	u3400
u3401:
	goto	l593
u3400:
	
l5487:	
	fcall	_uart_rx
	xorlw	0Bh
	skipnz
	goto	u3411
	goto	u3410
u3411:
	goto	l593
u3410:
	
l5489:	
	fcall	_uart_rx
	xorlw	0Ch
	skipz
	goto	u3421
	goto	u3420
u3421:
	goto	l5481
u3420:
	goto	l593
	
l592:	
	
l593:	
	line	110
;main.c: 109: }
;main.c: 110: mainprogressflags.infinite_blink = TRUE;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	bsf	(main@mainprogressflags),1
	line	112
	
l5491:	
;main.c: 112: while_condition = uart_rx();
	fcall	_uart_rx
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(main@while_condition)
	line	113
	
l5493:	
;main.c: 113: blinking_while_condition = while_condition - (0x0A);
	movf	(main@while_condition),w
	addlw	0F6h
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(main@blinking_while_condition)
	line	114
	
l5495:	
;main.c: 114: timer_set(TIMER_1,10);
	movlw	low(0Ah)
	movwf	(?_timer_set)
	movlw	high(0Ah)
	movwf	((?_timer_set))+1
	movlw	(0)
	fcall	_timer_set
	line	119
;main.c: 119: while (exit == 0)
	goto	l5503
	
l595:	
	line	121
	
l5497:	
;main.c: 120: {
;main.c: 121: exit = blink_led(blinking_while_condition);
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(main@blinking_while_condition),w
	movwf	(??_main+0)+0
	clrf	(??_main+0)+0+1
	movf	0+(??_main+0)+0,w
	movwf	(?_blink_led)
	movf	1+(??_main+0)+0,w
	movwf	(?_blink_led+1)
	fcall	_blink_led
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(0+(?_blink_led)),w
	movwf	(??_main+2)+0
	movf	(??_main+2)+0,w
	movwf	(main@exit)
	line	122
	
l5499:	
# 122 "F:\pic_projects\4 INPUT 4 OUTPUT PIC16f877A\main.c"
nop ;#
psect	maintext
	line	123
	
l5501:	
# 123 "F:\pic_projects\4 INPUT 4 OUTPUT PIC16f877A\main.c"
clrwdt ;#
psect	maintext
	goto	l5503
	line	124
	
l594:	
	line	119
	
l5503:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(main@exit),w
	skipz
	goto	u3430
	goto	l5497
u3430:
	
l596:	
	line	125
;main.c: 124: }
;main.c: 125: mainprogressflags.indicator_blink = TRUE;
	bsf	(main@mainprogressflags),2
	line	126
	
l5505:	
;main.c: 126: mainprogressflags.while_con = while_condition;
	movf	(main@while_condition),w
	movwf	(??_main+0)+0
	swapf	(??_main+0)+0,f
	movf	(main@mainprogressflags),w
	xorwf	(??_main+0)+0,w
	andlw	not (((1<<4)-1)<<4)
	xorwf	(??_main+0)+0,w
	movwf	(main@mainprogressflags)
	goto	l5513
	line	133
;main.c: 133: while(1)
	
l597:	
	line	136
;main.c: 134: {
;main.c: 136: while(while_condition == (0x0A))
	goto	l5513
	
l599:	
	line	138
;main.c: 137: {
;main.c: 138: if (TO == 1)
	btfss	(28/8),(28)&7
	goto	u3441
	goto	u3440
u3441:
	goto	l600
u3440:
	line	140
	
l5507:	
;main.c: 139: {
;main.c: 140: rcv_chn = uart_rx();
	fcall	_uart_rx
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(main@rcv_chn)
	line	141
	
l5509:	
;main.c: 141: led_rx(rcv_chn);
	movf	(main@rcv_chn),w
	fcall	_led_rx
	line	142
	
l5511:	
# 142 "F:\pic_projects\4 INPUT 4 OUTPUT PIC16f877A\main.c"
clrwdt ;#
psect	maintext
	line	143
;main.c: 143: }
	goto	l5513
	line	144
	
l600:	
	line	146
# 146 "F:\pic_projects\4 INPUT 4 OUTPUT PIC16f877A\main.c"
clrwdt ;#
psect	maintext
	goto	l5513
	line	147
	
l601:	
	goto	l5513
	line	148
	
l598:	
	line	136
	
l5513:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(main@while_condition),w
	xorlw	0Ah
	skipnz
	goto	u3451
	goto	u3450
u3451:
	goto	l599
u3450:
	goto	l5521
	
l602:	
	line	150
;main.c: 147: }
;main.c: 148: }
;main.c: 150: while(while_condition == (0x0B))
	goto	l5521
	
l604:	
	line	152
;main.c: 151: {
;main.c: 152: if (TO == 1)
	btfss	(28/8),(28)&7
	goto	u3461
	goto	u3460
u3461:
	goto	l605
u3460:
	line	154
	
l5515:	
;main.c: 153: {
;main.c: 154: rcv_chn = uart_rx();
	fcall	_uart_rx
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(main@rcv_chn)
	line	155
	
l5517:	
;main.c: 155: uart_tx(rcv_chn);
	movf	(main@rcv_chn),w
	fcall	_uart_tx
	line	156
	
l5519:	
# 156 "F:\pic_projects\4 INPUT 4 OUTPUT PIC16f877A\main.c"
clrwdt ;#
psect	maintext
	line	157
;main.c: 157: }
	goto	l5521
	line	158
	
l605:	
	line	160
# 160 "F:\pic_projects\4 INPUT 4 OUTPUT PIC16f877A\main.c"
clrwdt ;#
psect	maintext
	goto	l5521
	line	161
	
l606:	
	goto	l5521
	line	162
	
l603:	
	line	150
	
l5521:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(main@while_condition),w
	xorlw	0Bh
	skipnz
	goto	u3471
	goto	u3470
u3471:
	goto	l604
u3470:
	goto	l5531
	
l607:	
	line	165
;main.c: 161: }
;main.c: 162: }
;main.c: 165: while(while_condition == (0X0C))
	goto	l5531
	
l609:	
	line	167
;main.c: 166: {
;main.c: 167: if (TO == 1)
	btfss	(28/8),(28)&7
	goto	u3481
	goto	u3480
u3481:
	goto	l610
u3480:
	line	169
	
l5523:	
;main.c: 168: {
;main.c: 169: rcv_chn = uart_rx();
	fcall	_uart_rx
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(main@rcv_chn)
	line	170
	
l5525:	
;main.c: 170: uart_tx(rcv_chn);
	movf	(main@rcv_chn),w
	fcall	_uart_tx
	line	171
	
l5527:	
;main.c: 171: led_rx_and_adc(rcv_chn);
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(main@rcv_chn),w
	fcall	_led_rx_and_adc
	line	172
	
l5529:	
# 172 "F:\pic_projects\4 INPUT 4 OUTPUT PIC16f877A\main.c"
clrwdt ;#
psect	maintext
	line	173
;main.c: 173: }
	goto	l5531
	line	174
	
l610:	
	line	176
# 176 "F:\pic_projects\4 INPUT 4 OUTPUT PIC16f877A\main.c"
clrwdt ;#
psect	maintext
	goto	l5531
	line	177
	
l611:	
	goto	l5531
	line	178
	
l608:	
	line	165
	
l5531:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(main@while_condition),w
	xorlw	0Ch
	skipnz
	goto	u3491
	goto	u3490
u3491:
	goto	l609
u3490:
	goto	l5513
	
l612:	
	goto	l5513
	line	179
	
l613:	
	line	133
	goto	l5513
	
l614:	
	line	180
	
l615:	
	global	start
	ljmp	start
	opt stack 0
GLOBAL	__end_of_main
	__end_of_main:
;; =============== function _main ends ============

	signat	_main,90
	global	_uart_tx
psect	text644,local,class=CODE,delta=2
global __ptext644
__ptext644:

;; *************** function _uart_tx *****************
;; Defined at:
;;		line 58 in file "F:\pic_projects\4 INPUT 4 OUTPUT PIC16f877A\uart_func.c"
;; Parameters:    Size  Location     Type
;;  chn_sel         1    wreg     unsigned char 
;; Auto vars:     Size  Location     Type
;;  chn_sel         1   13[BANK0 ] unsigned char 
;;  adc_hi          2   18[BANK0 ] unsigned short 
;;  adc_lo          2   16[BANK0 ] unsigned short 
;;  rawadc_in       2   14[BANK0 ] unsigned short 
;;  adc_hiaddrch    1   12[BANK0 ] unsigned char 
;;  delay_count     1    0        unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0, btemp+1, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       8       0       0       0
;;      Temps:          0       2       0       0       0
;;      Totals:         0      10       0       0       0
;;Total ram usage:       10 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		_adc_convert
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text644
	file	"F:\pic_projects\4 INPUT 4 OUTPUT PIC16f877A\uart_func.c"
	line	58
	global	__size_of_uart_tx
	__size_of_uart_tx	equ	__end_of_uart_tx-_uart_tx
	
_uart_tx:	
	opt	stack 4
; Regs used in _uart_tx: [wreg+status,2+status,0+btemp+1+pclath+cstack]
;uart_tx@chn_sel stored from wreg
	line	66
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(uart_tx@chn_sel)
	
l5411:	
;uart_func.c: 59: uint8_t delay_count;
;uart_func.c: 61: uint16_t adc_lo;
;uart_func.c: 62: uint16_t adc_hi;
;uart_func.c: 63: uint8_t adc_hiaddrch;
;uart_func.c: 64: uint16_t rawadc_in;
;uart_func.c: 66: ADCON0 &= 0xC7;
	movlw	(0C7h)
	movwf	(??_uart_tx+0)+0
	movf	(??_uart_tx+0)+0,w
	andwf	(31),f	;volatile
	line	67
;uart_func.c: 67: ADCON0 |= (chn_sel<<3);
	movf	(uart_tx@chn_sel),w
	movwf	(??_uart_tx+0)+0
	movlw	(03h)-1
u3355:
	clrc
	rlf	(??_uart_tx+0)+0,f
	addlw	-1
	skipz
	goto	u3355
	clrc
	rlf	(??_uart_tx+0)+0,w
	movwf	(??_uart_tx+1)+0
	movf	(??_uart_tx+1)+0,w
	iorwf	(31),f	;volatile
	line	70
	
l5413:	
# 70 "F:\pic_projects\4 INPUT 4 OUTPUT PIC16f877A\uart_func.c"
nop ;#
psect	text644
	line	71
	
l5415:	
# 71 "F:\pic_projects\4 INPUT 4 OUTPUT PIC16f877A\uart_func.c"
nop ;#
psect	text644
	line	72
	
l5417:	
# 72 "F:\pic_projects\4 INPUT 4 OUTPUT PIC16f877A\uart_func.c"
nop ;#
psect	text644
	line	73
	
l5419:	
# 73 "F:\pic_projects\4 INPUT 4 OUTPUT PIC16f877A\uart_func.c"
nop ;#
psect	text644
	line	74
	
l5421:	
# 74 "F:\pic_projects\4 INPUT 4 OUTPUT PIC16f877A\uart_func.c"
nop ;#
psect	text644
	line	76
	
l5423:	
;uart_func.c: 76: rawadc_in = adc_convert();
	fcall	_adc_convert
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(1+(?_adc_convert)),w
	clrf	(uart_tx@rawadc_in+1)
	addwf	(uart_tx@rawadc_in+1)
	movf	(0+(?_adc_convert)),w
	clrf	(uart_tx@rawadc_in)
	addwf	(uart_tx@rawadc_in)

	line	78
	
l5425:	
;uart_func.c: 78: adc_lo = rawadc_in;
	movf	(uart_tx@rawadc_in+1),w
	clrf	(uart_tx@adc_lo+1)
	addwf	(uart_tx@adc_lo+1)
	movf	(uart_tx@rawadc_in),w
	clrf	(uart_tx@adc_lo)
	addwf	(uart_tx@adc_lo)

	line	79
	
l5427:	
;uart_func.c: 79: adc_lo = (uint8_t)(adc_lo);
	movf	(uart_tx@adc_lo),w
	movwf	(??_uart_tx+0)+0
	clrf	(??_uart_tx+0)+0+1
	movf	0+(??_uart_tx+0)+0,w
	movwf	(uart_tx@adc_lo)
	movf	1+(??_uart_tx+0)+0,w
	movwf	(uart_tx@adc_lo+1)
	line	81
	
l5429:	
;uart_func.c: 81: adc_hi = (rawadc_in & 0x0300);
	movlw	low(0300h)
	andwf	(uart_tx@rawadc_in),w
	movwf	(uart_tx@adc_hi)
	movlw	high(0300h)
	andwf	(uart_tx@rawadc_in+1),w
	movwf	1+(uart_tx@adc_hi)
	line	82
	
l5431:	
;uart_func.c: 82: adc_hi = (adc_hi >> 8);
	movf	(uart_tx@adc_hi+1),w
	movwf	(??_uart_tx+0)+0+1
	movf	(uart_tx@adc_hi),w
	movwf	(??_uart_tx+0)+0
	movlw	08h
u3365:
	clrc
	rrf	(??_uart_tx+0)+1,f
	rrf	(??_uart_tx+0)+0,f
	addlw	-1
	skipz
	goto	u3365
	movf	0+(??_uart_tx+0)+0,w
	movwf	(uart_tx@adc_hi)
	movf	1+(??_uart_tx+0)+0,w
	movwf	(uart_tx@adc_hi+1)
	line	83
	
l5433:	
;uart_func.c: 83: adc_hi = (uint8_t)(adc_hi);
	movf	(uart_tx@adc_hi),w
	movwf	(??_uart_tx+0)+0
	clrf	(??_uart_tx+0)+0+1
	movf	0+(??_uart_tx+0)+0,w
	movwf	(uart_tx@adc_hi)
	movf	1+(??_uart_tx+0)+0,w
	movwf	(uart_tx@adc_hi+1)
	line	85
	
l5435:	
;uart_func.c: 85: adc_hiaddrch = (adc_hi | (chn_sel << 4));
	movf	(uart_tx@chn_sel),w
	movwf	(??_uart_tx+0)+0
	movlw	(04h)-1
u3375:
	clrc
	rlf	(??_uart_tx+0)+0,f
	addlw	-1
	skipz
	goto	u3375
	clrc
	rlf	(??_uart_tx+0)+0,w
	iorwf	(uart_tx@adc_hi),w
	movwf	(??_uart_tx+1)+0
	movf	(??_uart_tx+1)+0,w
	movwf	(uart_tx@adc_hiaddrch)
	line	88
	
l5437:	
;uart_func.c: 88: TXREG = adc_hiaddrch;
	movf	(uart_tx@adc_hiaddrch),w
	movwf	(25)	;volatile
	line	89
	
l5439:	
;uart_func.c: 89: (GIE = 0);
	bcf	(95/8),(95)&7
	line	90
;uart_func.c: 90: while (TRMT = 0)
	goto	l2383
	
l2384:	
	line	93
;uart_func.c: 91: {
	
l2383:	
	line	90
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bcf	(1217/8)^080h,(1217)&7
	btfsc	(1217/8)^080h,(1217)&7
	goto	u3381
	goto	u3380
u3381:
	goto	l2383
u3380:
	goto	l5441
	
l2385:	
	line	95
	
l5441:	
;uart_func.c: 93: }
;uart_func.c: 95: _delay(3000);
	opt asmopt_off
movlw	4
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
movwf	((??_uart_tx+0)+0+1),f
	movlw	228
movwf	((??_uart_tx+0)+0),f
u3507:
	decfsz	((??_uart_tx+0)+0),f
	goto	u3507
	decfsz	((??_uart_tx+0)+0+1),f
	goto	u3507
	clrwdt
opt asmopt_on

	line	96
;uart_func.c: 96: TXREG = adc_lo;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(uart_tx@adc_lo),w
	movwf	(25)	;volatile
	line	97
;uart_func.c: 97: while (TRMT = 0)
	goto	l2386
	
l2387:	
	line	100
;uart_func.c: 98: {
	
l2386:	
	line	97
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bcf	(1217/8)^080h,(1217)&7
	btfsc	(1217/8)^080h,(1217)&7
	goto	u3391
	goto	u3390
u3391:
	goto	l2386
u3390:
	goto	l5443
	
l2388:	
	line	101
	
l5443:	
;uart_func.c: 100: }
;uart_func.c: 101: _delay(9000);
	opt asmopt_off
movlw	12
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
movwf	((??_uart_tx+0)+0+1),f
	movlw	175
movwf	((??_uart_tx+0)+0),f
u3517:
	decfsz	((??_uart_tx+0)+0),f
	goto	u3517
	decfsz	((??_uart_tx+0)+0+1),f
	goto	u3517
opt asmopt_on

	line	102
	
l5445:	
;uart_func.c: 102: (GIE = 1);
	bsf	(95/8),(95)&7
	line	104
	
l2389:	
	return
	opt stack 0
GLOBAL	__end_of_uart_tx
	__end_of_uart_tx:
;; =============== function _uart_tx ends ============

	signat	_uart_tx,4216
	global	_blink_led
psect	text645,local,class=CODE,delta=2
global __ptext645
__ptext645:

;; *************** function _blink_led *****************
;; Defined at:
;;		line 234 in file "F:\pic_projects\4 INPUT 4 OUTPUT PIC16f877A\main.c"
;; Parameters:    Size  Location     Type
;;  blinks          2    6[BANK0 ] int 
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;                  2    6[BANK0 ] int 
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       2       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       1       0       0       0
;;      Totals:         0       3       0       0       0
;;Total ram usage:        3 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		_get_timer
;;		_timer_set
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text645
	file	"F:\pic_projects\4 INPUT 4 OUTPUT PIC16f877A\main.c"
	line	234
	global	__size_of_blink_led
	__size_of_blink_led	equ	__end_of_blink_led-_blink_led
	
_blink_led:	
	opt	stack 4
; Regs used in _blink_led: [wreg-fsr0h+status,2+status,0+pclath+cstack]
	line	238
	
l5341:	
;main.c: 235: static int led_blink =3;
;main.c: 236: static int count;
;main.c: 238: switch(led_blink)
	goto	l5405
	line	240
;main.c: 239: {
;main.c: 240: case 0:
	
l640:	
	line	241
	
l5343:	
;main.c: 241: if (get_timer(TIMER_1) == 0)
	movlw	(0)
	fcall	_get_timer
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	((1+(?_get_timer))),w
	iorwf	((0+(?_get_timer))),w
	skipz
	goto	u3321
	goto	u3320
u3321:
	goto	l5357
u3320:
	line	243
	
l5345:	
;main.c: 242: {
;main.c: 243: PORTB = 0x00;
	clrf	(6)	;volatile
	line	244
	
l5347:	
;main.c: 244: led_blink = 1;
	movlw	low(01h)
	movwf	(blink_led@led_blink)
	movlw	high(01h)
	movwf	((blink_led@led_blink))+1
	line	245
	
l5349:	
;main.c: 245: timer_set(TIMER_1,(1000));
	movlw	low(03E8h)
	movwf	(?_timer_set)
	movlw	high(03E8h)
	movwf	((?_timer_set))+1
	movlw	(0)
	fcall	_timer_set
	line	246
	
l5351:	
;main.c: 246: return 0;
	movlw	low(0)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(?_blink_led)
	movlw	high(0)
	movwf	((?_blink_led))+1
	goto	l642
	
l5353:	
	goto	l642
	line	247
	
l5355:	
;main.c: 247: }
	goto	l642
	line	248
	
l641:	
	line	250
	
l5357:	
;main.c: 248: else
;main.c: 249: {
;main.c: 250: return 0;
	movlw	low(0)
	movwf	(?_blink_led)
	movlw	high(0)
	movwf	((?_blink_led))+1
	goto	l642
	
l5359:	
	goto	l642
	line	251
	
l643:	
	line	253
;main.c: 251: }
;main.c: 253: break;
	goto	l642
	line	255
;main.c: 255: case 1:
	
l645:	
	line	256
	
l5361:	
;main.c: 256: if (get_timer(TIMER_1) == 0)
	movlw	(0)
	fcall	_get_timer
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	((1+(?_get_timer))),w
	iorwf	((0+(?_get_timer))),w
	skipz
	goto	u3331
	goto	u3330
u3331:
	goto	l5375
u3330:
	line	258
	
l5363:	
;main.c: 257: {
;main.c: 258: PORTB = 0xFF;
	movlw	(0FFh)
	movwf	(6)	;volatile
	line	259
;main.c: 259: led_blink = 2;
	movlw	low(02h)
	movwf	(blink_led@led_blink)
	movlw	high(02h)
	movwf	((blink_led@led_blink))+1
	line	260
;main.c: 260: count++;
	movlw	low(01h)
	addwf	(blink_led@count),f
	skipnc
	incf	(blink_led@count+1),f
	movlw	high(01h)
	addwf	(blink_led@count+1),f
	line	261
	
l5365:	
;main.c: 261: timer_set(TIMER_1,(1000));
	movlw	low(03E8h)
	movwf	(?_timer_set)
	movlw	high(03E8h)
	movwf	((?_timer_set))+1
	movlw	(0)
	fcall	_timer_set
	line	262
	
l5367:	
# 262 "F:\pic_projects\4 INPUT 4 OUTPUT PIC16f877A\main.c"
nop ;#
psect	text645
	line	263
	
l5369:	
;main.c: 263: return 0;
	movlw	low(0)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(?_blink_led)
	movlw	high(0)
	movwf	((?_blink_led))+1
	goto	l642
	
l5371:	
	goto	l642
	line	264
	
l5373:	
;main.c: 264: }
	goto	l642
	line	265
	
l646:	
	line	267
	
l5375:	
;main.c: 265: else
;main.c: 266: {
;main.c: 267: return 0;
	movlw	low(0)
	movwf	(?_blink_led)
	movlw	high(0)
	movwf	((?_blink_led))+1
	goto	l642
	
l5377:	
	goto	l642
	line	268
	
l647:	
	line	269
;main.c: 268: }
;main.c: 269: break;
	goto	l642
	line	271
;main.c: 271: case 2:
	
l648:	
	line	272
	
l5379:	
;main.c: 272: if(count <= blinks)
	movf	(blink_led@blinks+1),w
	xorlw	80h
	movwf	(??_blink_led+0)+0
	movf	(blink_led@count+1),w
	xorlw	80h
	subwf	(??_blink_led+0)+0,w
	skipz
	goto	u3345
	movf	(blink_led@count),w
	subwf	(blink_led@blinks),w
u3345:

	skipc
	goto	u3341
	goto	u3340
u3341:
	goto	l5387
u3340:
	line	274
	
l5381:	
;main.c: 273: {
;main.c: 274: led_blink = 0;
	movlw	low(0)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(blink_led@led_blink)
	movlw	high(0)
	movwf	((blink_led@led_blink))+1
	line	275
;main.c: 275: return 0;
	movlw	low(0)
	movwf	(?_blink_led)
	movlw	high(0)
	movwf	((?_blink_led))+1
	goto	l642
	
l5383:	
	goto	l642
	line	276
	
l5385:	
;main.c: 276: }
	goto	l642
	line	277
	
l649:	
	line	279
	
l5387:	
;main.c: 277: else
;main.c: 278: {
;main.c: 279: return 1;
	movlw	low(01h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(?_blink_led)
	movlw	high(01h)
	movwf	((?_blink_led))+1
	goto	l642
	
l5389:	
	goto	l642
	line	280
	
l650:	
	line	281
;main.c: 280: }
;main.c: 281: break;
	goto	l642
	line	283
;main.c: 283: case 3:
	
l651:	
	line	285
	
l5391:	
;main.c: 285: PORTB = 0xFF;
	movlw	(0FFh)
	movwf	(6)	;volatile
	line	286
;main.c: 286: led_blink = 0;
	movlw	low(0)
	movwf	(blink_led@led_blink)
	movlw	high(0)
	movwf	((blink_led@led_blink))+1
	line	287
	
l5393:	
;main.c: 287: timer_set(TIMER_1, (1000));
	movlw	low(03E8h)
	movwf	(?_timer_set)
	movlw	high(03E8h)
	movwf	((?_timer_set))+1
	movlw	(0)
	fcall	_timer_set
	line	288
	
l5395:	
# 288 "F:\pic_projects\4 INPUT 4 OUTPUT PIC16f877A\main.c"
nop ;#
psect	text645
	line	289
	
l5397:	
;main.c: 289: return 0;
	movlw	low(0)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(?_blink_led)
	movlw	high(0)
	movwf	((?_blink_led))+1
	goto	l642
	
l5399:	
	goto	l642
	line	290
	
l5401:	
;main.c: 290: break;
	goto	l642
	line	292
;main.c: 292: default:
	
l652:	
	line	293
;main.c: 293: break;
	goto	l642
	line	295
	
l5403:	
;main.c: 295: }
	goto	l642
	line	238
	
l639:	
	
l5405:	
	; Switch on 2 bytes has been partitioned into a top level switch of size 1, and 1 sub-switches
; Switch size 1, requested type "space"
; Number of cases is 1, Range of values is 0 to 0
; switch strategies available:
; Name         Bytes Cycles
; simple_byte     4     3 (average)
; direct_byte    22    19 (fixed)
;	Chosen strategy is simple_byte

	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf (blink_led@led_blink+1),w
	xorlw	0^0	; case 0
	skipnz
	goto	l5573
	goto	l642
	
l5573:	
; Switch size 1, requested type "space"
; Number of cases is 4, Range of values is 0 to 3
; switch strategies available:
; Name         Bytes Cycles
; simple_byte    13     7 (average)
; direct_byte    31    19 (fixed)
;	Chosen strategy is simple_byte

	movf (blink_led@led_blink),w
	xorlw	0^0	; case 0
	skipnz
	goto	l5343
	xorlw	1^0	; case 1
	skipnz
	goto	l5361
	xorlw	2^1	; case 2
	skipnz
	goto	l5379
	xorlw	3^2	; case 3
	skipnz
	goto	l5391
	goto	l642

	line	295
	
l644:	
	line	296
	
l642:	
	return
	opt stack 0
GLOBAL	__end_of_blink_led
	__end_of_blink_led:
;; =============== function _blink_led ends ============

	signat	_blink_led,4218
	global	_uart_rx
psect	text646,local,class=CODE,delta=2
global __ptext646
__ptext646:

;; *************** function _uart_rx *****************
;; Defined at:
;;		line 118 in file "F:\pic_projects\4 INPUT 4 OUTPUT PIC16f877A\uart_func.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  eorr_count      2    4[BANK0 ] persistent unsigned shor
;;  rcv_byte        1    6[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       3       0       0       0
;;      Temps:          0       1       0       0       0
;;      Totals:         0       4       0       0       0
;;Total ram usage:        4 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		_eeprom_write
;;		_uart_init
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text646
	file	"F:\pic_projects\4 INPUT 4 OUTPUT PIC16f877A\uart_func.c"
	line	118
	global	__size_of_uart_rx
	__size_of_uart_rx	equ	__end_of_uart_rx-_uart_rx
	
_uart_rx:	
	opt	stack 4
; Regs used in _uart_rx: [wreg+status,2+status,0+pclath+cstack]
	line	122
	
l5323:	
;uart_func.c: 119: uint8_t rcv_byte;
;uart_func.c: 120: persistent uint16_t eorr_count;
;uart_func.c: 122: rcv_byte = RCREG;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(26),w	;volatile
	movwf	(??_uart_rx+0)+0
	movf	(??_uart_rx+0)+0,w
	movwf	(uart_rx@rcv_byte)
	line	124
	
l5325:	
;uart_func.c: 124: if (OERR ==1)
	btfss	(193/8),(193)&7
	goto	u3291
	goto	u3290
u3291:
	goto	l2394
u3290:
	line	126
	
l5327:	
;uart_func.c: 125: {
;uart_func.c: 126: eorr_count++;
	movlw	low(01h)
	addwf	(uart_rx@eorr_count),f
	skipnc
	incf	(uart_rx@eorr_count+1),f
	movlw	high(01h)
	addwf	(uart_rx@eorr_count+1),f
	line	127
	
l5329:	
;uart_func.c: 127: eeprom_write(0x10,eorr_count);
	movf	(uart_rx@eorr_count),w
	movwf	(??_uart_rx+0)+0
	movf	(??_uart_rx+0)+0,w
	movwf	(?_eeprom_write)
	movlw	(010h)
	fcall	_eeprom_write
	line	128
	
l5331:	
;uart_func.c: 128: CREN = 0;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	bcf	(196/8),(196)&7
	line	129
	
l5333:	
;uart_func.c: 129: uart_init();
	fcall	_uart_init
	line	131
;uart_func.c: 131: }
	goto	l5337
	line	132
	
l2392:	
	line	134
;uart_func.c: 132: else
;uart_func.c: 133: {
;uart_func.c: 134: while ((RCIF = 1) && (OERR != 1))
	goto	l2394
	
l2395:	
	line	137
;uart_func.c: 135: {
	
l2394:	
	line	134
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	bsf	(101/8),(101)&7
	btfss	(101/8),(101)&7
	goto	u3301
	goto	u3300
u3301:
	goto	l5337
u3300:
	
l5335:	
	btfss	(193/8),(193)&7
	goto	u3311
	goto	u3310
u3311:
	goto	l2394
u3310:
	goto	l5337
	
l2397:	
	goto	l5337
	
l2398:	
	goto	l5337
	line	138
	
l2393:	
	line	140
	
l5337:	
;uart_func.c: 137: }
;uart_func.c: 138: }
;uart_func.c: 140: return rcv_byte;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(uart_rx@rcv_byte),w
	goto	l2399
	
l5339:	
	line	141
	
l2399:	
	return
	opt stack 0
GLOBAL	__end_of_uart_rx
	__end_of_uart_rx:
;; =============== function _uart_rx ends ============

	signat	_uart_rx,89
	global	_blink_infinite_led
psect	text647,local,class=CODE,delta=2
global __ptext647
__ptext647:

;; *************** function _blink_infinite_led *****************
;; Defined at:
;;		line 199 in file "F:\pic_projects\4 INPUT 4 OUTPUT PIC16f877A\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		_get_timer
;;		_timer_set
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text647
	file	"F:\pic_projects\4 INPUT 4 OUTPUT PIC16f877A\main.c"
	line	199
	global	__size_of_blink_infinite_led
	__size_of_blink_infinite_led	equ	__end_of_blink_infinite_led-_blink_infinite_led
	
_blink_infinite_led:	
	opt	stack 4
; Regs used in _blink_infinite_led: [wreg-fsr0h+status,2+status,0+pclath+cstack]
	line	201
	
l5297:	
;main.c: 200: static int led_blink = 1;
;main.c: 201: switch(led_blink)
	goto	l5317
	line	203
;main.c: 202: {
;main.c: 203: case 0:
	
l624:	
	line	204
	
l5299:	
;main.c: 204: if (get_timer(TIMER_1) == 0)
	movlw	(0)
	fcall	_get_timer
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	((1+(?_get_timer))),w
	iorwf	((0+(?_get_timer))),w
	skipz
	goto	u3271
	goto	u3270
u3271:
	goto	l632
u3270:
	line	206
	
l5301:	
;main.c: 205: {
;main.c: 206: PORTB = 0x00;
	clrf	(6)	;volatile
	line	207
	
l5303:	
;main.c: 207: led_blink = 1;
	movlw	low(01h)
	movwf	(blink_infinite_led@led_blink)
	movlw	high(01h)
	movwf	((blink_infinite_led@led_blink))+1
	line	208
	
l5305:	
;main.c: 208: timer_set(TIMER_1,(100));
	movlw	low(064h)
	movwf	(?_timer_set)
	movlw	high(064h)
	movwf	((?_timer_set))+1
	movlw	(0)
	fcall	_timer_set
	line	209
;main.c: 209: }
	goto	l632
	line	210
	
l625:	
	goto	l632
	line	212
;main.c: 210: else
;main.c: 211: {
	
l626:	
	line	213
;main.c: 212: }
;main.c: 213: break;
	goto	l632
	line	215
;main.c: 215: case 1:
	
l628:	
	line	216
	
l5307:	
;main.c: 216: if (get_timer(TIMER_1) == 0)
	movlw	(0)
	fcall	_get_timer
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	((1+(?_get_timer))),w
	iorwf	((0+(?_get_timer))),w
	skipz
	goto	u3281
	goto	u3280
u3281:
	goto	l632
u3280:
	line	218
	
l5309:	
;main.c: 217: {
;main.c: 218: PORTB = 0xFF;
	movlw	(0FFh)
	movwf	(6)	;volatile
	line	219
;main.c: 219: led_blink = 0;
	movlw	low(0)
	movwf	(blink_infinite_led@led_blink)
	movlw	high(0)
	movwf	((blink_infinite_led@led_blink))+1
	line	220
	
l5311:	
;main.c: 220: timer_set(TIMER_1,(100));
	movlw	low(064h)
	movwf	(?_timer_set)
	movlw	high(064h)
	movwf	((?_timer_set))+1
	movlw	(0)
	fcall	_timer_set
	line	221
;main.c: 221: }
	goto	l632
	line	222
	
l629:	
	goto	l632
	line	224
;main.c: 222: else
;main.c: 223: {
	
l630:	
	goto	l632
	line	225
;main.c: 224: }
;main.c: 225: default:
	
l631:	
	line	226
;main.c: 226: break;
	goto	l632
	line	228
	
l5313:	
;main.c: 228: break;
	goto	l632
	line	229
	
l5315:	
;main.c: 229: }
	goto	l632
	line	201
	
l623:	
	
l5317:	
	; Switch on 2 bytes has been partitioned into a top level switch of size 1, and 1 sub-switches
; Switch size 1, requested type "space"
; Number of cases is 1, Range of values is 0 to 0
; switch strategies available:
; Name         Bytes Cycles
; simple_byte     4     3 (average)
; direct_byte    22    19 (fixed)
;	Chosen strategy is simple_byte

	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf (blink_infinite_led@led_blink+1),w
	xorlw	0^0	; case 0
	skipnz
	goto	l5575
	goto	l632
	
l5575:	
; Switch size 1, requested type "space"
; Number of cases is 2, Range of values is 0 to 1
; switch strategies available:
; Name         Bytes Cycles
; simple_byte     7     4 (average)
; direct_byte    25    19 (fixed)
;	Chosen strategy is simple_byte

	movf (blink_infinite_led@led_blink),w
	xorlw	0^0	; case 0
	skipnz
	goto	l5299
	xorlw	1^0	; case 1
	skipnz
	goto	l5307
	goto	l632

	line	229
	
l627:	
	line	230
	
l632:	
	return
	opt stack 0
GLOBAL	__end_of_blink_infinite_led
	__end_of_blink_infinite_led:
;; =============== function _blink_infinite_led ends ============

	signat	_blink_infinite_led,88
	global	_init
psect	text648,local,class=CODE,delta=2
global __ptext648
__ptext648:

;; *************** function _init *****************
;; Defined at:
;;		line 184 in file "F:\pic_projects\4 INPUT 4 OUTPUT PIC16f877A\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		_eeprom_write
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text648
	file	"F:\pic_projects\4 INPUT 4 OUTPUT PIC16f877A\main.c"
	line	184
	global	__size_of_init
	__size_of_init	equ	__end_of_init-_init
	
_init:	
	opt	stack 4
; Regs used in _init: [wreg+status,2+status,0+pclath+cstack]
	line	185
	
l5285:	
;main.c: 185: OPTION = 0x8F;
	movlw	(08Fh)
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movwf	(129)^080h	;volatile
	line	186
;main.c: 186: TRISA = 0x0F;
	movlw	(0Fh)
	movwf	(133)^080h	;volatile
	line	187
	
l5287:	
;main.c: 187: TRISB = 0x00;
	clrf	(134)^080h	;volatile
	line	188
	
l5289:	
;main.c: 188: TRISD = 0x00;
	clrf	(136)^080h	;volatile
	line	189
	
l5291:	
;main.c: 189: PORTB = 0x00;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(6)	;volatile
	line	190
	
l5293:	
;main.c: 190: PORTD = 0x00;
	clrf	(8)	;volatile
	line	193
;main.c: 193: T1CON = 0x35;
	movlw	(035h)
	movwf	(16)	;volatile
	line	194
	
l5295:	
;main.c: 194: eeprom_write(0x10,0x00);
	clrc
	movlw	0
	btfsc	status,0
	movlw	1
	movwf	(?_eeprom_write)
	movlw	(010h)
	fcall	_eeprom_write
	line	196
	
l618:	
	return
	opt stack 0
GLOBAL	__end_of_init
	__end_of_init:
;; =============== function _init ends ============

	signat	_init,88
	global	_adc_convert
psect	text649,local,class=CODE,delta=2
global __ptext649
__ptext649:

;; *************** function _adc_convert *****************
;; Defined at:
;;		line 67 in file "F:\pic_projects\4 INPUT 4 OUTPUT PIC16f877A\adc_func.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  result          2    8[BANK0 ] unsigned short 
;;  adresl          2    6[BANK0 ] unsigned short 
;;  adresh          2    4[BANK0 ] unsigned short 
;; Return value:  Size  Location     Type
;;                  2    0[BANK0 ] unsigned short 
;; Registers used:
;;		wreg, status,2, status,0, btemp+1
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       2       0       0       0
;;      Locals:         0       6       0       0       0
;;      Temps:          0       2       0       0       0
;;      Totals:         0      10       0       0       0
;;Total ram usage:       10 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_uart_tx
;;		_adc_volt_convert
;; This function uses a non-reentrant model
;;
psect	text649
	file	"F:\pic_projects\4 INPUT 4 OUTPUT PIC16f877A\adc_func.c"
	line	67
	global	__size_of_adc_convert
	__size_of_adc_convert	equ	__end_of_adc_convert-_adc_convert
	
_adc_convert:	
	opt	stack 4
; Regs used in _adc_convert: [wreg+status,2+status,0+btemp+1]
	line	71
	
l5271:	
;adc_func.c: 68: uint16_t adresh;
;adc_func.c: 69: uint16_t adresl;
;adc_func.c: 70: uint16_t result;
;adc_func.c: 71: adc_isr_flag = 0;
	movlw	low(0)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(_adc_isr_flag)
	movlw	high(0)
	movwf	((_adc_isr_flag))+1
	line	73
	
l5273:	
;adc_func.c: 73: GODONE = 1;
	bsf	(250/8),(250)&7
	line	75
;adc_func.c: 75: while (adc_isr_flag == 0)
	goto	l5275
	
l1227:	
	goto	l5275
	line	76
;adc_func.c: 76: {}
	
l1226:	
	line	75
	
l5275:	
	movf	(_adc_isr_flag+1),w
	iorwf	(_adc_isr_flag),w
	skipnz
	goto	u3251
	goto	u3250
u3251:
	goto	l5275
u3250:
	goto	l5277
	
l1228:	
	line	77
	
l5277:	
;adc_func.c: 77: adresl = ADRESL;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movf	(158)^080h,w	;volatile
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_adc_convert+0)+0
	clrf	(??_adc_convert+0)+0+1
	movf	0+(??_adc_convert+0)+0,w
	movwf	(adc_convert@adresl)
	movf	1+(??_adc_convert+0)+0,w
	movwf	(adc_convert@adresl+1)
	line	78
;adc_func.c: 78: adresh = ADRESH;
	movf	(30),w	;volatile
	movwf	(??_adc_convert+0)+0
	clrf	(??_adc_convert+0)+0+1
	movf	0+(??_adc_convert+0)+0,w
	movwf	(adc_convert@adresh)
	movf	1+(??_adc_convert+0)+0,w
	movwf	(adc_convert@adresh+1)
	line	79
	
l5279:	
;adc_func.c: 79: result = ((adresh << 8) |(adresl));
	movf	(adc_convert@adresh+1),w
	movwf	(??_adc_convert+0)+0+1
	movf	(adc_convert@adresh),w
	movwf	(??_adc_convert+0)+0
	movlw	08h
	movwf	btemp+1
u3265:
	clrc
	rlf	(??_adc_convert+0)+0,f
	rlf	(??_adc_convert+0)+1,f
	decfsz	btemp+1,f
	goto	u3265
	movf	(adc_convert@adresl),w
	iorwf	0+(??_adc_convert+0)+0,w
	movwf	(adc_convert@result)
	movf	(adc_convert@adresl+1),w
	iorwf	1+(??_adc_convert+0)+0,w
	movwf	1+(adc_convert@result)
	line	81
	
l5281:	
;adc_func.c: 81: return result;
	movf	(adc_convert@result+1),w
	clrf	(?_adc_convert+1)
	addwf	(?_adc_convert+1)
	movf	(adc_convert@result),w
	clrf	(?_adc_convert)
	addwf	(?_adc_convert)

	goto	l1229
	
l5283:	
	line	82
	
l1229:	
	return
	opt stack 0
GLOBAL	__end_of_adc_convert
	__end_of_adc_convert:
;; =============== function _adc_convert ends ============

	signat	_adc_convert,90
	global	_get_timer
psect	text650,local,class=CODE,delta=2
global __ptext650
__ptext650:

;; *************** function _get_timer *****************
;; Defined at:
;;		line 33 in file "F:\pic_projects\4 INPUT 4 OUTPUT PIC16f877A\timer.c"
;; Parameters:    Size  Location     Type
;;  index           1    wreg     unsigned char 
;; Auto vars:     Size  Location     Type
;;  index           1    5[BANK0 ] unsigned char 
;;  result          2    3[BANK0 ] unsigned short 
;; Return value:  Size  Location     Type
;;                  2    0[BANK0 ] unsigned short 
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       2       0       0       0
;;      Locals:         0       3       0       0       0
;;      Temps:          0       1       0       0       0
;;      Totals:         0       6       0       0       0
;;Total ram usage:        6 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_blink_infinite_led
;;		_blink_led
;; This function uses a non-reentrant model
;;
psect	text650
	file	"F:\pic_projects\4 INPUT 4 OUTPUT PIC16f877A\timer.c"
	line	33
	global	__size_of_get_timer
	__size_of_get_timer	equ	__end_of_get_timer-_get_timer
	
_get_timer:	
	opt	stack 4
; Regs used in _get_timer: [wreg-fsr0h+status,2+status,0]
;get_timer@index stored from wreg
	line	36
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(get_timer@index)
	
l5241:	
;timer.c: 34: uint16_t result;
;timer.c: 36: if (index < TIMER_MAX)
	movlw	(02h)
	subwf	(get_timer@index),w
	skipnc
	goto	u3231
	goto	u3230
u3231:
	goto	l5249
u3230:
	line	38
	
l5243:	
;timer.c: 37: {
;timer.c: 38: (GIE = 0);
	bcf	(95/8),(95)&7
	line	39
	
l5245:	
;timer.c: 39: result = timer_array[index];
	movf	(get_timer@index),w
	movwf	(??_get_timer+0)+0
	addwf	(??_get_timer+0)+0,w
	addlw	_timer_array&0ffh
	movwf	fsr0
	bcf	status, 7	;select IRP bank0
	movf	indf,w
	movwf	(get_timer@result)
	incf	fsr0,f
	movf	indf,w
	movwf	(get_timer@result+1)
	line	40
	
l5247:	
;timer.c: 40: (GIE = 1);
	bsf	(95/8),(95)&7
	line	41
;timer.c: 41: }
	goto	l5251
	line	42
	
l3002:	
	line	44
	
l5249:	
;timer.c: 42: else
;timer.c: 43: {
;timer.c: 44: result = 0;
	movlw	low(0)
	movwf	(get_timer@result)
	movlw	high(0)
	movwf	((get_timer@result))+1
	goto	l5251
	line	45
	
l3003:	
	line	47
	
l5251:	
;timer.c: 45: }
;timer.c: 47: return result;
	movf	(get_timer@result+1),w
	clrf	(?_get_timer+1)
	addwf	(?_get_timer+1)
	movf	(get_timer@result),w
	clrf	(?_get_timer)
	addwf	(?_get_timer)

	goto	l3004
	
l5253:	
	line	49
	
l3004:	
	return
	opt stack 0
GLOBAL	__end_of_get_timer
	__end_of_get_timer:
;; =============== function _get_timer ends ============

	signat	_get_timer,4218
	global	_eeprom_write
psect	text651,local,class=CODE,delta=2
global __ptext651
__ptext651:

;; *************** function _eeprom_write *****************
;; Defined at:
;;		line 8 in file "eewrite.c"
;; Parameters:    Size  Location     Type
;;  addr            1    wreg     unsigned char 
;;  value           1    0[BANK0 ] unsigned char 
;; Auto vars:     Size  Location     Type
;;  addr            1    2[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       1       0       0       0
;;      Locals:         0       1       0       0       0
;;      Temps:          0       1       0       0       0
;;      Totals:         0       3       0       0       0
;;Total ram usage:        3 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_init
;;		_uart_rx
;; This function uses a non-reentrant model
;;
psect	text651
	file	"C:\Program Files (x86)\HI-TECH Software\PICC\9.80\sources\eewrite.c"
	line	8
	global	__size_of_eeprom_write
	__size_of_eeprom_write	equ	__end_of_eeprom_write-_eeprom_write
	
_eeprom_write:	
	opt	stack 4
; Regs used in _eeprom_write: [wreg+status,2+status,0]
;eeprom_write@addr stored from wreg
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(eeprom_write@addr)
	line	9
	
l3037:	
	goto	l3038
	
l3039:	
	
l3038:	
	bsf	status, 5	;RP0=1, select bank3
	bsf	status, 6	;RP1=1, select bank3
	btfsc	(3169/8)^0180h,(3169)&7
	goto	u3201
	goto	u3200
u3201:
	goto	l3038
u3200:
	goto	l5221
	
l3040:	
	
l5221:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(eeprom_write@addr),w
	bcf	status, 5	;RP0=0, select bank2
	bsf	status, 6	;RP1=1, select bank2
	movwf	(269)^0100h	;volatile
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(eeprom_write@value),w
	bcf	status, 5	;RP0=0, select bank2
	bsf	status, 6	;RP1=1, select bank2
	movwf	(268)^0100h	;volatile
	
l5223:	
	movlw	(03Fh)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_eeprom_write+0)+0
	movf	(??_eeprom_write+0)+0,w
	bsf	status, 5	;RP0=1, select bank3
	bsf	status, 6	;RP1=1, select bank3
	andwf	(396)^0180h,f	;volatile
	
l5225:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	bcf	(24/8),(24)&7
	
l5227:	
	btfss	(95/8),(95)&7
	goto	u3211
	goto	u3210
u3211:
	goto	l3041
u3210:
	
l5229:	
	bsf	(24/8),(24)&7
	
l3041:	
	bcf	(95/8),(95)&7
	bsf	status, 5	;RP0=1, select bank3
	bsf	status, 6	;RP1=1, select bank3
	bsf	(3170/8)^0180h,(3170)&7
	
l5231:	
	movlw	(055h)
	movwf	(397)^0180h	;volatile
	movlw	(0AAh)
	movwf	(397)^0180h	;volatile
	
l5233:	
	bsf	(3169/8)^0180h,(3169)&7
	
l5235:	
	bcf	(3170/8)^0180h,(3170)&7
	
l5237:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfss	(24/8),(24)&7
	goto	u3221
	goto	u3220
u3221:
	goto	l3044
u3220:
	
l5239:	
	bsf	(95/8),(95)&7
	goto	l3044
	
l3042:	
	goto	l3044
	
l3043:	
	line	10
	
l3044:	
	return
	opt stack 0
GLOBAL	__end_of_eeprom_write
	__end_of_eeprom_write:
;; =============== function _eeprom_write ends ============

	signat	_eeprom_write,8312
	global	_led_rx_and_adc
psect	text652,local,class=CODE,delta=2
global __ptext652
__ptext652:

;; *************** function _led_rx_and_adc *****************
;; Defined at:
;;		line 211 in file "F:\pic_projects\4 INPUT 4 OUTPUT PIC16f877A\uart_func.c"
;; Parameters:    Size  Location     Type
;;  led_on          1    wreg     unsigned char 
;; Auto vars:     Size  Location     Type
;;  led_on          1    3[BANK0 ] unsigned char 
;;  led_num         1    7[BANK0 ] unsigned char 
;;  index           1    6[BANK0 ] unsigned char 
;;  led_state       1    5[BANK0 ] unsigned char 
;;  rcv_byte_new    1    4[BANK0 ] unsigned char 
;;  final_led_va    1    2[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, pclath
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       6       0       0       0
;;      Temps:          0       2       0       0       0
;;      Totals:         0       8       0       0       0
;;Total ram usage:        8 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text652
	file	"F:\pic_projects\4 INPUT 4 OUTPUT PIC16f877A\uart_func.c"
	line	211
	global	__size_of_led_rx_and_adc
	__size_of_led_rx_and_adc	equ	__end_of_led_rx_and_adc-_led_rx_and_adc
	
_led_rx_and_adc:	
	opt	stack 5
; Regs used in _led_rx_and_adc: [wreg-fsr0h+status,2+status,0+pclath]
;led_rx_and_adc@led_on stored from wreg
	line	218
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(led_rx_and_adc@led_on)
	
l5195:	
;uart_func.c: 212: static uint8_t rcv_byte;
;uart_func.c: 213: uint8_t led_num;
;uart_func.c: 214: uint8_t led_state;
;uart_func.c: 215: uint8_t final_led_val;
;uart_func.c: 216: uint8_t rcv_byte_new;
;uart_func.c: 217: uint8_t index;
;uart_func.c: 218: rcv_byte_new = led_on;
	movf	(led_rx_and_adc@led_on),w
	movwf	(??_led_rx_and_adc+0)+0
	movf	(??_led_rx_and_adc+0)+0,w
	movwf	(led_rx_and_adc@rcv_byte_new)
	line	223
	
l5197:	
;uart_func.c: 223: led_num = (rcv_byte_new & 0xF0);
	movf	(led_rx_and_adc@rcv_byte_new),w
	andlw	0F0h
	movwf	(??_led_rx_and_adc+0)+0
	movf	(??_led_rx_and_adc+0)+0,w
	movwf	(led_rx_and_adc@led_num)
	line	224
	
l5199:	
;uart_func.c: 224: led_state = ((rcv_byte_new & 0x0F) - 1);
	movf	(led_rx_and_adc@rcv_byte_new),w
	andlw	0Fh
	addlw	0FFh
	movwf	(??_led_rx_and_adc+0)+0
	movf	(??_led_rx_and_adc+0)+0,w
	movwf	(led_rx_and_adc@led_state)
	line	225
	
l5201:	
;uart_func.c: 225: final_led_val = (led_num | led_state);
	movf	(led_rx_and_adc@led_num),w
	iorwf	(led_rx_and_adc@led_state),w
	movwf	(??_led_rx_and_adc+0)+0
	movf	(??_led_rx_and_adc+0)+0,w
	movwf	(led_rx_and_adc@final_led_val)
	line	226
	
l5203:	
;uart_func.c: 226: led_num = led_num >> 4;
	movf	(led_rx_and_adc@led_num),w
	movwf	(??_led_rx_and_adc+0)+0
	movlw	04h
u3165:
	clrc
	rrf	(??_led_rx_and_adc+0)+0,f
	addlw	-1
	skipz
	goto	u3165
	movf	0+(??_led_rx_and_adc+0)+0,w
	movwf	(??_led_rx_and_adc+1)+0
	movf	(??_led_rx_and_adc+1)+0,w
	movwf	(led_rx_and_adc@led_num)
	line	227
	
l5205:	
;uart_func.c: 227: if (led_state == (0x01))
	movf	(led_rx_and_adc@led_state),w
	xorlw	01h
	skipz
	goto	u3171
	goto	u3170
u3171:
	goto	l5213
u3170:
	line	229
	
l5207:	
;uart_func.c: 228: {
;uart_func.c: 229: index = led_num - 1;
	movf	(led_rx_and_adc@led_num),w
	addlw	0FFh
	movwf	(??_led_rx_and_adc+0)+0
	movf	(??_led_rx_and_adc+0)+0,w
	movwf	(led_rx_and_adc@index)
	line	230
	
l5209:	
;uart_func.c: 230: if (index < 8)
	movlw	(08h)
	subwf	(led_rx_and_adc@index),w
	skipnc
	goto	u3181
	goto	u3180
u3181:
	goto	l2419
u3180:
	line	232
	
l5211:	
;uart_func.c: 231: {
;uart_func.c: 232: PORTD = portd_on_val[index];
	movf	(led_rx_and_adc@index),w
	addlw	low((_portd_on_val-__stringbase))
	movwf	fsr0
	fcall	stringdir
	movwf	(8)	;volatile
	line	233
;uart_func.c: 233: }
	goto	l2419
	line	234
	
l2414:	
	goto	l2419
	line	236
;uart_func.c: 234: else
;uart_func.c: 235: {
	
l2415:	
	line	237
;uart_func.c: 236: }
;uart_func.c: 237: }
	goto	l2419
	line	238
	
l2413:	
	
l5213:	
;uart_func.c: 238: else if(led_state == (0x00))
	movf	(led_rx_and_adc@led_state),f
	skipz
	goto	u3191
	goto	u3190
u3191:
	goto	l2419
u3190:
	line	240
	
l5215:	
;uart_func.c: 239: {
;uart_func.c: 240: index = led_num-1 ;
	movf	(led_rx_and_adc@led_num),w
	addlw	0FFh
	movwf	(??_led_rx_and_adc+0)+0
	movf	(??_led_rx_and_adc+0)+0,w
	movwf	(led_rx_and_adc@index)
	line	241
	
l5217:	
;uart_func.c: 241: PORTD &= portd_off_val[index];
	movf	(led_rx_and_adc@index),w
	addlw	low((_portd_off_val-__stringbase))
	movwf	fsr0
	fcall	stringdir
	movwf	(??_led_rx_and_adc+0)+0
	movf	(??_led_rx_and_adc+0)+0,w
	andwf	(8),f	;volatile
	line	242
	
l5219:	
# 242 "F:\pic_projects\4 INPUT 4 OUTPUT PIC16f877A\uart_func.c"
nop ;#
psect	text652
	line	243
;uart_func.c: 243: }
	goto	l2419
	line	244
	
l2417:	
	goto	l2419
	line	246
;uart_func.c: 244: else
;uart_func.c: 245: {
	
l2418:	
	goto	l2419
	
l2416:	
	line	253
	
l2419:	
	return
	opt stack 0
GLOBAL	__end_of_led_rx_and_adc
	__end_of_led_rx_and_adc:
;; =============== function _led_rx_and_adc ends ============

	signat	_led_rx_and_adc,4216
	global	_led_rx
psect	text653,local,class=CODE,delta=2
global __ptext653
__ptext653:

;; *************** function _led_rx *****************
;; Defined at:
;;		line 156 in file "F:\pic_projects\4 INPUT 4 OUTPUT PIC16f877A\uart_func.c"
;; Parameters:    Size  Location     Type
;;  led_on          1    wreg     unsigned char 
;; Auto vars:     Size  Location     Type
;;  led_on          1    3[BANK0 ] unsigned char 
;;  led_num         1    7[BANK0 ] unsigned char 
;;  led_state       1    6[BANK0 ] unsigned char 
;;  index           1    5[BANK0 ] unsigned char 
;;  rcv_byte_new    1    4[BANK0 ] unsigned char 
;;  final_led_va    1    2[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, pclath
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       6       0       0       0
;;      Temps:          0       2       0       0       0
;;      Totals:         0       8       0       0       0
;;Total ram usage:        8 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text653
	file	"F:\pic_projects\4 INPUT 4 OUTPUT PIC16f877A\uart_func.c"
	line	156
	global	__size_of_led_rx
	__size_of_led_rx	equ	__end_of_led_rx-_led_rx
	
_led_rx:	
	opt	stack 5
; Regs used in _led_rx: [wreg-fsr0h+status,2+status,0+pclath]
;led_rx@led_on stored from wreg
	line	163
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(led_rx@led_on)
	
l5171:	
;uart_func.c: 157: static uint8_t rcv_byte;
;uart_func.c: 158: uint8_t led_num;
;uart_func.c: 159: uint8_t led_state;
;uart_func.c: 160: uint8_t final_led_val;
;uart_func.c: 161: uint8_t rcv_byte_new;
;uart_func.c: 162: uint8_t index;
;uart_func.c: 163: rcv_byte_new = led_on;
	movf	(led_rx@led_on),w
	movwf	(??_led_rx+0)+0
	movf	(??_led_rx+0)+0,w
	movwf	(led_rx@rcv_byte_new)
	line	168
	
l5173:	
;uart_func.c: 168: led_num = (rcv_byte_new & 0xF0);
	movf	(led_rx@rcv_byte_new),w
	andlw	0F0h
	movwf	(??_led_rx+0)+0
	movf	(??_led_rx+0)+0,w
	movwf	(led_rx@led_num)
	line	169
	
l5175:	
;uart_func.c: 169: led_state = ((rcv_byte_new & 0x0F) - 1);
	movf	(led_rx@rcv_byte_new),w
	andlw	0Fh
	addlw	0FFh
	movwf	(??_led_rx+0)+0
	movf	(??_led_rx+0)+0,w
	movwf	(led_rx@led_state)
	line	170
	
l5177:	
;uart_func.c: 170: final_led_val = (led_num | led_state);
	movf	(led_rx@led_num),w
	iorwf	(led_rx@led_state),w
	movwf	(??_led_rx+0)+0
	movf	(??_led_rx+0)+0,w
	movwf	(led_rx@final_led_val)
	line	171
	
l5179:	
;uart_func.c: 171: led_num = led_num >> 4;
	movf	(led_rx@led_num),w
	movwf	(??_led_rx+0)+0
	movlw	04h
u3135:
	clrc
	rrf	(??_led_rx+0)+0,f
	addlw	-1
	skipz
	goto	u3135
	movf	0+(??_led_rx+0)+0,w
	movwf	(??_led_rx+1)+0
	movf	(??_led_rx+1)+0,w
	movwf	(led_rx@led_num)
	line	172
	
l5181:	
;uart_func.c: 172: if (led_state == (0x01))
	movf	(led_rx@led_state),w
	xorlw	01h
	skipz
	goto	u3141
	goto	u3140
u3141:
	goto	l5187
u3140:
	line	174
	
l5183:	
;uart_func.c: 173: {
;uart_func.c: 174: index = led_num - 1;
	movf	(led_rx@led_num),w
	addlw	0FFh
	movwf	(??_led_rx+0)+0
	movf	(??_led_rx+0)+0,w
	movwf	(led_rx@index)
	line	175
	
l5185:	
;uart_func.c: 175: PORTD = portd_on_val[index];
	movf	(led_rx@index),w
	addlw	low((_portd_on_val-__stringbase))
	movwf	fsr0
	fcall	stringdir
	movwf	(8)	;volatile
	line	176
;uart_func.c: 176: }
	goto	l2408
	line	177
	
l2404:	
	
l5187:	
;uart_func.c: 177: else if(led_state == (0x00))
	movf	(led_rx@led_state),f
	skipz
	goto	u3151
	goto	u3150
u3151:
	goto	l2408
u3150:
	line	179
	
l5189:	
;uart_func.c: 178: {
;uart_func.c: 179: index = led_num-1 ;
	movf	(led_rx@led_num),w
	addlw	0FFh
	movwf	(??_led_rx+0)+0
	movf	(??_led_rx+0)+0,w
	movwf	(led_rx@index)
	line	180
	
l5191:	
;uart_func.c: 180: PORTD &= portd_off_val[index];
	movf	(led_rx@index),w
	addlw	low((_portd_off_val-__stringbase))
	movwf	fsr0
	fcall	stringdir
	movwf	(??_led_rx+0)+0
	movf	(??_led_rx+0)+0,w
	andwf	(8),f	;volatile
	line	181
	
l5193:	
# 181 "F:\pic_projects\4 INPUT 4 OUTPUT PIC16f877A\uart_func.c"
nop ;#
psect	text653
	line	182
;uart_func.c: 182: }
	goto	l2408
	line	183
	
l2406:	
	goto	l2408
	line	185
;uart_func.c: 183: else
;uart_func.c: 184: {
	
l2407:	
	goto	l2408
	
l2405:	
	line	192
	
l2408:	
	return
	opt stack 0
GLOBAL	__end_of_led_rx
	__end_of_led_rx:
;; =============== function _led_rx ends ============

	signat	_led_rx,4216
	global	_timer_set
psect	text654,local,class=CODE,delta=2
global __ptext654
__ptext654:

;; *************** function _timer_set *****************
;; Defined at:
;;		line 18 in file "F:\pic_projects\4 INPUT 4 OUTPUT PIC16f877A\timer.c"
;; Parameters:    Size  Location     Type
;;  index           1    wreg     unsigned char 
;;  value           2    0[BANK0 ] unsigned short 
;; Auto vars:     Size  Location     Type
;;  index           1    3[BANK0 ] unsigned char 
;;  array_conten    2    0        unsigned short 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       2       0       0       0
;;      Locals:         0       1       0       0       0
;;      Temps:          0       1       0       0       0
;;      Totals:         0       4       0       0       0
;;Total ram usage:        4 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;;		_blink_infinite_led
;;		_blink_led
;; This function uses a non-reentrant model
;;
psect	text654
	file	"F:\pic_projects\4 INPUT 4 OUTPUT PIC16f877A\timer.c"
	line	18
	global	__size_of_timer_set
	__size_of_timer_set	equ	__end_of_timer_set-_timer_set
	
_timer_set:	
	opt	stack 4
; Regs used in _timer_set: [wreg-fsr0h+status,2+status,0]
;timer_set@index stored from wreg
	line	20
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(timer_set@index)
	
l5163:	
;timer.c: 19: uint16_t array_contents;
;timer.c: 20: if (index < TIMER_MAX)
	movlw	(02h)
	subwf	(timer_set@index),w
	skipnc
	goto	u3121
	goto	u3120
u3121:
	goto	l2999
u3120:
	line	22
	
l5165:	
;timer.c: 21: {
;timer.c: 22: (GIE = 0);
	bcf	(95/8),(95)&7
	line	23
	
l5167:	
;timer.c: 23: timer_array[index] = value;
	movf	(timer_set@index),w
	movwf	(??_timer_set+0)+0
	addwf	(??_timer_set+0)+0,w
	addlw	_timer_array&0ffh
	movwf	fsr0
	movf	(timer_set@value),w
	bcf	status, 7	;select IRP bank0
	movwf	indf
	incf	fsr0,f
	movf	(timer_set@value+1),w
	movwf	indf
	line	24
	
l5169:	
;timer.c: 24: (GIE = 1);
	bsf	(95/8),(95)&7
	line	25
;timer.c: 25: }
	goto	l2999
	line	26
	
l2997:	
	goto	l2999
	line	28
;timer.c: 26: else
;timer.c: 27: {
	
l2998:	
	line	29
	
l2999:	
	return
	opt stack 0
GLOBAL	__end_of_timer_set
	__end_of_timer_set:
;; =============== function _timer_set ends ============

	signat	_timer_set,8312
	global	_uart_init
psect	text655,local,class=CODE,delta=2
global __ptext655
__ptext655:

;; *************** function _uart_init *****************
;; Defined at:
;;		line 259 in file "F:\pic_projects\4 INPUT 4 OUTPUT PIC16f877A\uart_func.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;;		_uart_rx
;; This function uses a non-reentrant model
;;
psect	text655
	file	"F:\pic_projects\4 INPUT 4 OUTPUT PIC16f877A\uart_func.c"
	line	259
	global	__size_of_uart_init
	__size_of_uart_init	equ	__end_of_uart_init-_uart_init
	
_uart_init:	
	opt	stack 4
; Regs used in _uart_init: [wreg]
	line	261
	
l5161:	
;uart_func.c: 261: SPBRG = 0x19;
	movlw	(019h)
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movwf	(153)^080h	;volatile
	line	262
;uart_func.c: 262: TXSTA = 0x24;
	movlw	(024h)
	movwf	(152)^080h	;volatile
	line	263
;uart_func.c: 263: RCSTA = 0x90;
	movlw	(090h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(24)	;volatile
	line	264
	
l2422:	
	return
	opt stack 0
GLOBAL	__end_of_uart_init
	__end_of_uart_init:
;; =============== function _uart_init ends ============

	signat	_uart_init,88
	global	_adc_init
psect	text656,local,class=CODE,delta=2
global __ptext656
__ptext656:

;; *************** function _adc_init *****************
;; Defined at:
;;		line 90 in file "F:\pic_projects\4 INPUT 4 OUTPUT PIC16f877A\adc_func.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text656
	file	"F:\pic_projects\4 INPUT 4 OUTPUT PIC16f877A\adc_func.c"
	line	90
	global	__size_of_adc_init
	__size_of_adc_init	equ	__end_of_adc_init-_adc_init
	
_adc_init:	
	opt	stack 5
; Regs used in _adc_init: [wreg]
	line	91
	
l5159:	
;adc_func.c: 91: ADCON0 = 0xC5;
	movlw	(0C5h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(31)	;volatile
	line	92
;adc_func.c: 92: ADCON1 = 0x80;
	movlw	(080h)
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movwf	(159)^080h	;volatile
	line	93
	
l1232:	
	return
	opt stack 0
GLOBAL	__end_of_adc_init
	__end_of_adc_init:
;; =============== function _adc_init ends ============

	signat	_adc_init,88
	global	_interrupt_handler
psect	text657,local,class=CODE,delta=2
global __ptext657
__ptext657:

;; *************** function _interrupt_handler *****************
;; Defined at:
;;		line 7 in file "F:\pic_projects\4 INPUT 4 OUTPUT PIC16f877A\interrupt.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          4       0       0       0       0
;;      Totals:         4       0       0       0       0
;;Total ram usage:        4 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		_adc_isr
;;		_timer_isr
;; This function is called by:
;;		Interrupt level 1
;; This function uses a non-reentrant model
;;
psect	text657
	file	"F:\pic_projects\4 INPUT 4 OUTPUT PIC16f877A\interrupt.c"
	line	7
	global	__size_of_interrupt_handler
	__size_of_interrupt_handler	equ	__end_of_interrupt_handler-_interrupt_handler
	
_interrupt_handler:	
	opt	stack 4
; Regs used in _interrupt_handler: [wreg-fsr0h+status,2+status,0+pclath+cstack]
psect	intentry,class=CODE,delta=2
global __pintentry
__pintentry:
global interrupt_function
interrupt_function:
	global saved_w
	saved_w	set	btemp+0
	movwf	saved_w
	movf	status,w
	movwf	(??_interrupt_handler+0)
	movf	fsr0,w
	movwf	(??_interrupt_handler+1)
	movf	pclath,w
	movwf	(??_interrupt_handler+2)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	btemp+1,w
	movwf	(??_interrupt_handler+3)
	ljmp	_interrupt_handler
psect	text657
	line	9
	
i1l4827:	
;interrupt.c: 9: adc_isr();
	fcall	_adc_isr
	line	10
	
i1l4829:	
;interrupt.c: 10: timer_isr();
	fcall	_timer_isr
	line	11
	
i1l1804:	
	movf	(??_interrupt_handler+3),w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	btemp+1
	movf	(??_interrupt_handler+2),w
	movwf	pclath
	movf	(??_interrupt_handler+1),w
	movwf	fsr0
	movf	(??_interrupt_handler+0),w
	movwf	status
	swapf	saved_w,f
	swapf	saved_w,w
	retfie
	opt stack 0
GLOBAL	__end_of_interrupt_handler
	__end_of_interrupt_handler:
;; =============== function _interrupt_handler ends ============

	signat	_interrupt_handler,88
	global	_timer_isr
psect	text658,local,class=CODE,delta=2
global __ptext658
__ptext658:

;; *************** function _timer_isr *****************
;; Defined at:
;;		line 54 in file "F:\pic_projects\4 INPUT 4 OUTPUT PIC16f877A\timer.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  i               1    1[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         1       0       0       0       0
;;      Temps:          1       0       0       0       0
;;      Totals:         2       0       0       0       0
;;Total ram usage:        2 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_interrupt_handler
;; This function uses a non-reentrant model
;;
psect	text658
	file	"F:\pic_projects\4 INPUT 4 OUTPUT PIC16f877A\timer.c"
	line	54
	global	__size_of_timer_isr
	__size_of_timer_isr	equ	__end_of_timer_isr-_timer_isr
	
_timer_isr:	
	opt	stack 4
; Regs used in _timer_isr: [wreg-fsr0h+status,2+status,0]
	line	56
	
i1l4831:	
;timer.c: 55: uint8_t i;
;timer.c: 56: if((TMR1IE)&&(TMR1IF))
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	btfss	(1120/8)^080h,(1120)&7
	goto	u282_21
	goto	u282_20
u282_21:
	goto	i1l3013
u282_20:
	
i1l4833:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfss	(96/8),(96)&7
	goto	u283_21
	goto	u283_20
u283_21:
	goto	i1l3013
u283_20:
	line	59
	
i1l4835:	
;timer.c: 57: {
;timer.c: 59: TMR1IF=0;
	bcf	(96/8),(96)&7
	line	60
	
i1l4837:	
;timer.c: 60: T1CON &= ~(0x01);
	movlw	(0FEh)
	movwf	(??_timer_isr+0)+0
	movf	(??_timer_isr+0)+0,w
	andwf	(16),f	;volatile
	line	61
	
i1l4839:	
;timer.c: 61: TMR1L = 0x08;
	movlw	(08h)
	movwf	(14)	;volatile
	line	62
	
i1l4841:	
;timer.c: 62: TMR1H = 0xFF;
	movlw	(0FFh)
	movwf	(15)	;volatile
	line	63
	
i1l4843:	
;timer.c: 63: T1CON |= 0x01;
	bsf	(16)+(0/8),(0)&7	;volatile
	line	65
;timer.c: 65: for (i = 0; i < TIMER_MAX; i++)
	clrc
	movlw	0
	btfsc	status,0
	movlw	1
	movwf	(timer_isr@i)
	
i1l4845:	
	movlw	(02h)
	subwf	(timer_isr@i),w
	skipc
	goto	u284_21
	goto	u284_20
u284_21:
	goto	i1l4849
u284_20:
	goto	i1l3013
	
i1l4847:	
	goto	i1l3013
	line	66
	
i1l3008:	
	line	67
	
i1l4849:	
;timer.c: 66: {
;timer.c: 67: if (timer_array[i] != 0)
	movf	(timer_isr@i),w
	movwf	(??_timer_isr+0)+0
	addwf	(??_timer_isr+0)+0,w
	addlw	_timer_array&0ffh
	movwf	fsr0
	bcf	status, 7	;select IRP bank0
	movf	indf,w
	incf	fsr0,f
	iorwf	indf,w
	skipnz
	goto	u285_21
	goto	u285_20
u285_21:
	goto	i1l4853
u285_20:
	line	69
	
i1l4851:	
;timer.c: 68: {
;timer.c: 69: timer_array[i]--;
	movf	(timer_isr@i),w
	movwf	(??_timer_isr+0)+0
	addwf	(??_timer_isr+0)+0,w
	addlw	_timer_array&0ffh
	movwf	fsr0
	movlw	low(01h)
	subwf	indf,f
	incfsz	fsr0,f
	movlw	high(01h)
	skipc
	decf	indf,f
	subwf	indf,f
	decf	fsr0,f
	line	70
;timer.c: 70: }
	goto	i1l4853
	line	71
	
i1l3010:	
	goto	i1l4853
	line	73
;timer.c: 71: else
;timer.c: 72: {
	
i1l3011:	
	line	65
	
i1l4853:	
	movlw	(01h)
	movwf	(??_timer_isr+0)+0
	movf	(??_timer_isr+0)+0,w
	addwf	(timer_isr@i),f
	
i1l4855:	
	movlw	(02h)
	subwf	(timer_isr@i),w
	skipc
	goto	u286_21
	goto	u286_20
u286_21:
	goto	i1l4849
u286_20:
	goto	i1l3013
	
i1l3009:	
	line	76
;timer.c: 73: }
;timer.c: 74: }
;timer.c: 76: }
	goto	i1l3013
	line	77
	
i1l3007:	
	goto	i1l3013
	line	79
;timer.c: 77: else
;timer.c: 78: {
	
i1l3012:	
	line	80
	
i1l3013:	
	return
	opt stack 0
GLOBAL	__end_of_timer_isr
	__end_of_timer_isr:
;; =============== function _timer_isr ends ============

	signat	_timer_isr,88
	global	_adc_isr
psect	text659,local,class=CODE,delta=2
global __ptext659
__ptext659:

;; *************** function _adc_isr *****************
;; Defined at:
;;		line 96 in file "F:\pic_projects\4 INPUT 4 OUTPUT PIC16f877A\adc_func.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_interrupt_handler
;; This function uses a non-reentrant model
;;
psect	text659
	file	"F:\pic_projects\4 INPUT 4 OUTPUT PIC16f877A\adc_func.c"
	line	96
	global	__size_of_adc_isr
	__size_of_adc_isr	equ	__end_of_adc_isr-_adc_isr
	
_adc_isr:	
	opt	stack 4
; Regs used in _adc_isr: [wreg]
	line	97
	
i1l4819:	
;adc_func.c: 97: if ((ADIF)&&(ADIE))
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfss	(102/8),(102)&7
	goto	u280_21
	goto	u280_20
u280_21:
	goto	i1l1237
u280_20:
	
i1l4821:	
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	btfss	(1126/8)^080h,(1126)&7
	goto	u281_21
	goto	u281_20
u281_21:
	goto	i1l1237
u281_20:
	line	100
	
i1l4823:	
;adc_func.c: 98: {
;adc_func.c: 100: ADIF=0;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	bcf	(102/8),(102)&7
	line	101
	
i1l4825:	
;adc_func.c: 101: adc_isr_flag=1;
	movlw	low(01h)
	movwf	(_adc_isr_flag)
	movlw	high(01h)
	movwf	((_adc_isr_flag))+1
	line	102
;adc_func.c: 102: int_count++;
	movlw	low(01h)
	addwf	(_int_count),f
	skipnc
	incf	(_int_count+1),f
	movlw	high(01h)
	addwf	(_int_count+1),f
	line	103
;adc_func.c: 103: }
	goto	i1l1237
	line	104
	
i1l1235:	
	goto	i1l1237
	line	106
;adc_func.c: 104: else
;adc_func.c: 105: {
	
i1l1236:	
	line	107
	
i1l1237:	
	return
	opt stack 0
GLOBAL	__end_of_adc_isr
	__end_of_adc_isr:
;; =============== function _adc_isr ends ============

	signat	_adc_isr,88
psect	text660,local,class=CODE,delta=2
global __ptext660
__ptext660:
	global	btemp
	btemp set 07Eh

	DABS	1,126,2	;btemp
	global	wtemp0
	wtemp0 set btemp
	end
