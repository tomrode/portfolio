/****************************************************************************************************
/*
/*            30 JAN 2013  Function Pointer further example
/*            31 JAN 2013  Expanded a bit further with another function pointer and Macro
/***************************************************************************************************/
#include "stdlib.h"


#define W32(addr, value)  	(*((volatile unsigned long *) (addr)) = (value))		//write 32 bits of data
#define SERIAL_BOOT1_ADDR	0x0A00
#define SERIAL_PASSWORD1	0xFEEDFACE//FACE

/*type definitions*/
typedef int(*T_FUNCPTR_1)(int);
typedef int(*T_FUNCPTR_2)(int,int);
typedef enum
{
  INCREMENT,
  DECREMENT,
  SQUARE,
  CUBED,
  ADD
}MATH;

/*MACRO's*/
#define INCREMENT_BYONE(x)   (increment(x)) 

/*Function prototypes*/
int increment(int x);
int decrement(int x);
int square(int x);
int cubed(int x);
int add(int x, int y);
int subtract(int x, int y);
int(*FUNC_PTR2)(int);
int(*functionPtr)(int x, int y);
int(*rProcessOutput)(int *pData,int rFeedback);
int inSomething(int *pData,int rFeedback);
int outSomething(void);
void main(void);

/*Constant Definitions */
const T_FUNCPTR_1 func_array[] =
{
   increment,
   decrement,
   square,
   cubed,
   add
};

/*FUNCTIONS*/

int increment(int x)
{ return (x + 1); }

int decrement(int x)
{ return (x - 1); }

int square(int x)
{ return (x * x); }

int cubed(int x)
{ return (x*x*x); }

int add(int x, int y)
{ return (x+y);   }

int subtract(int x, int y)
{ return (x-y);   }
int inSomething(int *pData,int rFeedback)
{ 
 int inSomethingpData = *pData;
 int inSomethingFeedback = rFeedback;
 return (inSomethingFeedback - inSomethingpData );   
}
int outSomething(void)
{ return (876);   }

/*Main loop*/
void main(void)
{
 /** declaration section */ 
 int a,b,sum,diff,pDataEx,rProcessOutputFunctPtrval, *rProcessOutputFunctPtr;

 /** Here is an example using the typedef and then it gets the address of function to be used all in one line*/
 T_FUNCPTR_2 pFunct = &add;; 

 pDataEx = 321;

  
while(1)
{

W32(SERIAL_BOOT1_ADDR, SERIAL_PASSWORD1);	//interlock write

/** derefr the address to the pointer*/
functionPtr = &add;
sum = (*functionPtr)(5,6);

/** derefr the address to the pointer*/
functionPtr = &subtract;
diff = (*functionPtr)(20,5);

/** this example did the dereferanin g whenthe pointer was declared*/
sum = (*pFunct)(10,20);

   a = 5;
   b = (*func_array[INCREMENT])(a);
   b = 0;
   b = (*func_array[DECREMENT])(a);
   b = 0;
   b = (*func_array[SQUARE])(a);
   b = 0;
   b = (*func_array[CUBED])(a);
   b = 0;
   b = (&square)(a);          /* To me its more intutive to pass the address of the the function desired*/
   a = 55;
   b =  INCREMENT_BYONE(a); 
   b = 0;

 // rProcessOutputFunctPtrval = rProcessOutputFunctPtr.rProcessOutput(&pDataEx,outSomething()); 
 }
}
 