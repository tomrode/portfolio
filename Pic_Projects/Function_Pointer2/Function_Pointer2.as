opt subtitle "HI-TECH Software Omniscient Code Generator (Lite mode) build 6738"

opt pagewidth 120

	opt lm

	processor	16F887
clrc	macro
	bcf	3,0
	endm
clrz	macro
	bcf	3,2
	endm
setc	macro
	bsf	3,0
	endm
setz	macro
	bsf	3,2
	endm
skipc	macro
	btfss	3,0
	endm
skipz	macro
	btfss	3,2
	endm
skipnc	macro
	btfsc	3,0
	endm
skipnz	macro
	btfsc	3,2
	endm
indf	equ	0
indf0	equ	0
pc	equ	2
pcl	equ	2
status	equ	3
fsr	equ	4
fsr0	equ	4
c	equ	1
z	equ	0
pclath	equ	10
	FNCALL	_main,Fake
	FNCALL	_main,_subtract
	FNCALL	_main,_add
	FNCALL	_main,_cubed
	FNCALL	_main,_square
	FNCALL	_main,_decrement
	FNCALL	_main,_increment
	FNCALL	_cubed,___wmul
	FNCALL	_square,___wmul
	FNROOT	_main
	global	_func_array
psect	strings,class=STRING,delta=2
global __pstrings
__pstrings:
;	global	stringdir,stringtab,__stringbase
stringtab:
;	String table - string pointers are 1 byte each
stringcode:stringdir:
movlw high(stringdir)
movwf pclath
movf fsr,w
incf fsr
	addwf pc
__stringbase:
	retlw	0
psect	strings
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Function_Pointer2\FunctionPointer.c"
	line	43
_func_array:
	retlw	(fp__increment-fpbase)&0ffh
	retlw	(fp__decrement-fpbase)&0ffh
	retlw	(fp__square-fpbase)&0ffh
	retlw	(fp__cubed-fpbase)&0ffh
	retlw	(fp__add-fpbase)&0ffh
	global	_func_array
	global	_rProcessOutput
	global	_FUNC_PTR2
	global	_functionPtr
psect	nvBANK0,class=BANK0,space=1
global __pnvBANK0
__pnvBANK0:
_functionPtr:
       ds      1

	file	"Function_Pointer2.as"
	line	#
psect cinit,class=CODE,delta=2
global start_initialization
start_initialization:

psect	bssCOMMON,class=COMMON,space=1
global __pbssCOMMON
__pbssCOMMON:
_FUNC_PTR2:
       ds      1

psect	bssBANK0,class=BANK0,space=1
global __pbssBANK0
__pbssBANK0:
_rProcessOutput:
       ds      1

; Clear objects allocated to COMMON
psect cinit,class=CODE,delta=2
	clrf	((__pbssCOMMON)+0)&07Fh
; Clear objects allocated to BANK0
psect cinit,class=CODE,delta=2
	clrf	((__pbssBANK0)+0)&07Fh
psect cinit,class=CODE,delta=2
global end_of_initialization

;End of C runtime variable initialization code

end_of_initialization:
clrf status
ljmp _main	;jump to C main() function
psect	cstackCOMMON,class=COMMON,space=1
global __pcstackCOMMON
__pcstackCOMMON:
	global	?_main
?_main:	; 0 bytes @ 0x0
	global	?___wmul
?___wmul:	; 2 bytes @ 0x0
	global	___wmul@multiplier
___wmul@multiplier:	; 2 bytes @ 0x0
	ds	2
	global	___wmul@multiplicand
___wmul@multiplicand:	; 2 bytes @ 0x2
	ds	2
	global	??___wmul
??___wmul:	; 0 bytes @ 0x4
	global	___wmul@product
___wmul@product:	; 2 bytes @ 0x4
	ds	2
	global	?_increment
?_increment:	; 2 bytes @ 0x6
	global	?_decrement
?_decrement:	; 2 bytes @ 0x6
	global	?_square
?_square:	; 2 bytes @ 0x6
	global	?_cubed
?_cubed:	; 2 bytes @ 0x6
	global	?_subtract
?_subtract:	; 2 bytes @ 0x6
	global	increment@x
increment@x:	; 2 bytes @ 0x6
	global	decrement@x
decrement@x:	; 2 bytes @ 0x6
	global	square@x
square@x:	; 2 bytes @ 0x6
	global	cubed@x
cubed@x:	; 2 bytes @ 0x6
	global	add@x
add@x:	; 2 bytes @ 0x6
	global	subtract@x
subtract@x:	; 2 bytes @ 0x6
	ds	2
	global	??_increment
??_increment:	; 0 bytes @ 0x8
	global	??_decrement
??_decrement:	; 0 bytes @ 0x8
	global	??_square
??_square:	; 0 bytes @ 0x8
	global	??_cubed
??_cubed:	; 0 bytes @ 0x8
	global	add@y
add@y:	; 2 bytes @ 0x8
	global	subtract@y
subtract@y:	; 2 bytes @ 0x8
	ds	2
	global	??_add
??_add:	; 0 bytes @ 0xA
	global	??_subtract
??_subtract:	; 0 bytes @ 0xA
	ds	2
	global	??_main
??_main:	; 0 bytes @ 0xC
	ds	1
psect	cstackBANK0,class=BANK0,space=1
global __pcstackBANK0
__pcstackBANK0:
	global	main@diff
main@diff:	; 2 bytes @ 0x0
	ds	2
	global	main@pDataEx
main@pDataEx:	; 2 bytes @ 0x2
	ds	2
	global	main@sum
main@sum:	; 2 bytes @ 0x4
	ds	2
	global	main@pFunct
main@pFunct:	; 1 bytes @ 0x6
	ds	1
	global	main@b
main@b:	; 2 bytes @ 0x7
	ds	2
	global	main@a
main@a:	; 2 bytes @ 0x9
	ds	2
;;Data sizes: Strings 0, constant 5, data 0, bss 2, persistent 1 stack 0
;;Auto spaces:   Size  Autos    Used
;; COMMON          14     13      14
;; BANK0           80     11      13
;; BANK1           80      0       0
;; BANK3           96      0       0
;; BANK2           96      0       0

;;
;; Pointer list with targets:

;; ?_subtract	int  size(1) Largest target is 0
;;
;; ?_add	int  size(1) Largest target is 0
;;
;; ?_cubed	int  size(1) Largest target is 0
;;
;; ?_square	int  size(1) Largest target is 0
;;
;; ?___wmul	unsigned int  size(1) Largest target is 0
;;
;; ?_increment	int  size(1) Largest target is 0
;;
;; rProcessOutput	PTR FTN(PTR int ,int ,)int  size(1) Largest target is 2
;;		 -> Absolute function(), 
;;
;; FUNC_PTR2	PTR FTN(int ,)int  size(1) Largest target is 2
;;		 -> Absolute function(), 
;;
;; functionPtr	PTR FTN(int ,int ,)int  size(1) Largest target is 2
;;		 -> Absolute function(), subtract(), add(), 
;;
;; main@pFunct	PTR FTN(int ,int ,)int  size(1) Largest target is 2
;;		 -> add(), 
;;
;; func_array	const PTR FTN(int ,)int [5] size(1) Largest target is 2
;;		 -> add(), cubed(), square(), decrement(), 
;;		 -> increment(), 
;;


;;
;; Critical Paths under _main in COMMON
;;
;;   _main->_subtract
;;   _cubed->___wmul
;;   _square->___wmul
;;
;; Critical Paths under _main in BANK0
;;
;;   None.
;;
;; Critical Paths under _main in BANK1
;;
;;   None.
;;
;; Critical Paths under _main in BANK3
;;
;;   None.
;;
;; Critical Paths under _main in BANK2
;;
;;   None.

;;
;;Main: autosize = 0, tempsize = 1, incstack = 0, save=0
;;

;;
;;Call Graph Tables:
;;
;; ---------------------------------------------------------------------------------
;; (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;; ---------------------------------------------------------------------------------
;; (0) _main                                                16    16      0     598
;;                                             12 COMMON     1     1      0
;;                                              0 BANK0     11    11      0
;;                   Absolute function
;;                           _subtract
;;                                _add
;;                              _cubed
;;                             _square
;;                          _decrement
;;                          _increment
;; ---------------------------------------------------------------------------------
;; (1) _cubed                                                2     0      2     158
;;                                              6 COMMON     4     0      4
;;                             ___wmul
;; ---------------------------------------------------------------------------------
;; (1) _square                                               2     0      2     136
;;                                              6 COMMON     4     0      4
;;                             ___wmul
;; ---------------------------------------------------------------------------------
;; (1) Absolute function(Fake)                               4     0      4       0
;;                                              0 COMMON     4     0      4
;; ---------------------------------------------------------------------------------
;; (2) ___wmul                                               6     2      4      92
;;                                              0 COMMON     6     2      4
;; ---------------------------------------------------------------------------------
;; (1) _subtract                                             6     2      4      44
;;                                              6 COMMON     6     2      4
;; ---------------------------------------------------------------------------------
;; (1) _add                                                  4     0      4      44
;;                                              6 COMMON     4     0      4
;; ---------------------------------------------------------------------------------
;; (1) _decrement                                            2     0      2      22
;;                                              6 COMMON     4     0      4
;; ---------------------------------------------------------------------------------
;; (1) _increment                                            2     0      2      22
;;                                              6 COMMON     4     0      4
;; ---------------------------------------------------------------------------------
;; Estimated maximum stack depth 2
;; ---------------------------------------------------------------------------------

;; Call Graph Graphs:

;; _main (ROOT)
;;   Absolute function(Fake)
;;   _subtract
;;   _add
;;   _cubed
;;     ___wmul
;;   _square
;;     ___wmul
;;   _decrement
;;   _increment
;;

;; Address spaces:

;;Name               Size   Autos  Total    Cost      Usage
;;BITCOMMON            E      0       0       0        0.0%
;;EEDATA             100      0       0       0        0.0%
;;NULL                 0      0       0       0        0.0%
;;CODE                 0      0       0       0        0.0%
;;COMMON               E      D       E       1      100.0%
;;BITSFR0              0      0       0       1        0.0%
;;SFR0                 0      0       0       1        0.0%
;;BITSFR1              0      0       0       2        0.0%
;;SFR1                 0      0       0       2        0.0%
;;STACK                0      0       2       2        0.0%
;;ABS                  0      0      1B       3        0.0%
;;BITBANK0            50      0       0       4        0.0%
;;BITSFR3              0      0       0       4        0.0%
;;SFR3                 0      0       0       4        0.0%
;;BANK0               50      B       D       5       16.3%
;;BITSFR2              0      0       0       5        0.0%
;;SFR2                 0      0       0       5        0.0%
;;BITBANK1            50      0       0       6        0.0%
;;BANK1               50      0       0       7        0.0%
;;BITBANK3            60      0       0       8        0.0%
;;BANK3               60      0       0       9        0.0%
;;BITBANK2            60      0       0      10        0.0%
;;BANK2               60      0       0      11        0.0%
;;DATA                 0      0      1D      12        0.0%

	global	_main
psect	maintext,global,class=CODE,delta=2
global __pmaintext
__pmaintext:

;; *************** function _main *****************
;; Defined at:
;;		line 82 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Function_Pointer2\FunctionPointer.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  a               2    9[BANK0 ] int 
;;  b               2    7[BANK0 ] int 
;;  sum             2    4[BANK0 ] int 
;;  pDataEx         2    2[BANK0 ] int 
;;  diff            2    0[BANK0 ] int 
;;  rProcessOutp    2    0        PTR int 
;;  rProcessOutp    2    0        int 
;;  pFunct          1    6[BANK0 ] PTR FTN(int ,int ,)int 
;;		 -> add(2), 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 17F/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0      11       0       0       0
;;      Temps:          1       0       0       0       0
;;      Totals:         1      11       0       0       0
;;Total ram usage:       12 bytes
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Absolute function
;;		_subtract
;;		_add
;;		_cubed
;;		_square
;;		_decrement
;;		_increment
;; This function is called by:
;;		Startup code after reset
;; This function uses a non-reentrant model
;;
psect	maintext
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Function_Pointer2\FunctionPointer.c"
	line	82
	global	__size_of_main
	__size_of_main	equ	__end_of_main-_main
	
_main:	
	opt	stack 6
; Regs used in _main: [wreg-fsr0h+status,2+status,0+pclath+cstack]
	line	87
	
l1357:	
;FunctionPointer.c: 84: int a,b,sum,diff,pDataEx,rProcessOutputFunctPtrval, *rProcessOutputFunctPtr;
;FunctionPointer.c: 87: T_FUNCPTR_2 pFunct = &add;;
	movlw	((fp__add-fpbase))&0ffh
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(main@pFunct)
	line	89
;FunctionPointer.c: 89: pDataEx = 321;
	movlw	low(0141h)
	movwf	(main@pDataEx)
	movlw	high(0141h)
	movwf	((main@pDataEx))+1
	line	92
;FunctionPointer.c: 92: while(1)
	
l45:	
	line	95
;FunctionPointer.c: 93: {
;FunctionPointer.c: 95: (*((volatile unsigned long *) (0x0A00)) = (0xFEEDFACE));
	movlw	0FEh
	bcf	status, 5	;RP0=0, select bank20
	bcf	status, 6	;RP1=0, select bank20
	movwf	(2560+3)^0A00h	;volatile
	movlw	0EDh
	bcf	status, 5	;RP0=0, select bank20
	bcf	status, 6	;RP1=0, select bank20
	movwf	(2560+2)^0A00h	;volatile
	movlw	0FAh
	bcf	status, 5	;RP0=0, select bank20
	bcf	status, 6	;RP1=0, select bank20
	movwf	(2560+1)^0A00h	;volatile
	movlw	0CEh
	bcf	status, 5	;RP0=0, select bank20
	bcf	status, 6	;RP1=0, select bank20
	movwf	(2560)^0A00h	;volatile

	line	98
;FunctionPointer.c: 98: functionPtr = &add;
	movlw	((fp__add-fpbase))&0ffh
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(_functionPtr)
	line	99
	
l1359:	
;FunctionPointer.c: 99: sum = (*functionPtr)(5,6);
	movlw	low(05h)
	movwf	(?_subtract)
	movlw	high(05h)
	movwf	((?_subtract))+1
	movlw	low(06h)
	movwf	0+(?_subtract)+02h
	movlw	high(06h)
	movwf	(0+(?_subtract)+02h)+1
	movf	(_functionPtr),w
	fcall	fptable
	movf	(1+(?_subtract)),w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(main@sum+1)
	addwf	(main@sum+1)
	movf	(0+(?_subtract)),w
	clrf	(main@sum)
	addwf	(main@sum)

	line	102
	
l1361:	
;FunctionPointer.c: 102: functionPtr = &subtract;
	movlw	((fp__subtract-fpbase))&0ffh
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(_functionPtr)
	line	103
;FunctionPointer.c: 103: diff = (*functionPtr)(20,5);
	movlw	low(014h)
	movwf	(?_subtract)
	movlw	high(014h)
	movwf	((?_subtract))+1
	movlw	low(05h)
	movwf	0+(?_subtract)+02h
	movlw	high(05h)
	movwf	(0+(?_subtract)+02h)+1
	movf	(_functionPtr),w
	fcall	fptable
	movf	(1+(?_subtract)),w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(main@diff+1)
	addwf	(main@diff+1)
	movf	(0+(?_subtract)),w
	clrf	(main@diff)
	addwf	(main@diff)

	line	106
;FunctionPointer.c: 106: sum = (*pFunct)(10,20);
	movlw	low(0Ah)
	movwf	(?_subtract)
	movlw	high(0Ah)
	movwf	((?_subtract))+1
	movlw	low(014h)
	movwf	0+(?_subtract)+02h
	movlw	high(014h)
	movwf	(0+(?_subtract)+02h)+1
	movf	(main@pFunct),w
	fcall	fptable
	movf	(1+(?_subtract)),w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(main@sum+1)
	addwf	(main@sum+1)
	movf	(0+(?_subtract)),w
	clrf	(main@sum)
	addwf	(main@sum)

	line	108
	
l1363:	
;FunctionPointer.c: 108: a = 5;
	movlw	low(05h)
	movwf	(main@a)
	movlw	high(05h)
	movwf	((main@a))+1
	line	109
	
l1365:	
;FunctionPointer.c: 109: b = (*func_array[INCREMENT])(a);
	movf	(main@a+1),w
	clrf	(?_subtract+1)
	addwf	(?_subtract+1)
	movf	(main@a),w
	clrf	(?_subtract)
	addwf	(?_subtract)

	movlw	(_func_array-__stringbase)
	movwf	fsr0
	fcall	stringdir
	fcall	fptable
	movf	(1+(?_subtract)),w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(main@b+1)
	addwf	(main@b+1)
	movf	(0+(?_subtract)),w
	clrf	(main@b)
	addwf	(main@b)

	line	110
	
l1367:	
;FunctionPointer.c: 110: b = 0;
	movlw	low(0)
	movwf	(main@b)
	movlw	high(0)
	movwf	((main@b))+1
	line	111
	
l1369:	
;FunctionPointer.c: 111: b = (*func_array[DECREMENT])(a);
	movf	(main@a+1),w
	clrf	(?_subtract+1)
	addwf	(?_subtract+1)
	movf	(main@a),w
	clrf	(?_subtract)
	addwf	(?_subtract)

	movlw	(_func_array-__stringbase)+01h
	movwf	fsr0
	fcall	stringdir
	fcall	fptable
	movf	(1+(?_subtract)),w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(main@b+1)
	addwf	(main@b+1)
	movf	(0+(?_subtract)),w
	clrf	(main@b)
	addwf	(main@b)

	line	112
	
l1371:	
;FunctionPointer.c: 112: b = 0;
	movlw	low(0)
	movwf	(main@b)
	movlw	high(0)
	movwf	((main@b))+1
	line	113
	
l1373:	
;FunctionPointer.c: 113: b = (*func_array[SQUARE])(a);
	movf	(main@a+1),w
	clrf	(?_subtract+1)
	addwf	(?_subtract+1)
	movf	(main@a),w
	clrf	(?_subtract)
	addwf	(?_subtract)

	movlw	(_func_array-__stringbase)+02h
	movwf	fsr0
	fcall	stringdir
	fcall	fptable
	movf	(1+(?_subtract)),w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(main@b+1)
	addwf	(main@b+1)
	movf	(0+(?_subtract)),w
	clrf	(main@b)
	addwf	(main@b)

	line	114
	
l1375:	
;FunctionPointer.c: 114: b = 0;
	movlw	low(0)
	movwf	(main@b)
	movlw	high(0)
	movwf	((main@b))+1
	line	115
	
l1377:	
;FunctionPointer.c: 115: b = (*func_array[CUBED])(a);
	movf	(main@a+1),w
	clrf	(?_subtract+1)
	addwf	(?_subtract+1)
	movf	(main@a),w
	clrf	(?_subtract)
	addwf	(?_subtract)

	movlw	(_func_array-__stringbase)+03h
	movwf	fsr0
	fcall	stringdir
	fcall	fptable
	movf	(1+(?_subtract)),w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(main@b+1)
	addwf	(main@b+1)
	movf	(0+(?_subtract)),w
	clrf	(main@b)
	addwf	(main@b)

	line	116
	
l1379:	
;FunctionPointer.c: 116: b = 0;
	movlw	low(0)
	movwf	(main@b)
	movlw	high(0)
	movwf	((main@b))+1
	line	117
	
l1381:	
;FunctionPointer.c: 117: b = (&square)(a);
	movf	(main@a+1),w
	clrf	(?_square+1)
	addwf	(?_square+1)
	movf	(main@a),w
	clrf	(?_square)
	addwf	(?_square)

	fcall	_square
	movf	(1+(?_square)),w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(main@b+1)
	addwf	(main@b+1)
	movf	(0+(?_square)),w
	clrf	(main@b)
	addwf	(main@b)

	line	118
;FunctionPointer.c: 118: a = 55;
	movlw	low(037h)
	movwf	(main@a)
	movlw	high(037h)
	movwf	((main@a))+1
	line	119
	
l1383:	
;FunctionPointer.c: 119: b = (increment(a));
	movf	(main@a+1),w
	clrf	(?_increment+1)
	addwf	(?_increment+1)
	movf	(main@a),w
	clrf	(?_increment)
	addwf	(?_increment)

	fcall	_increment
	movf	(1+(?_increment)),w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(main@b+1)
	addwf	(main@b+1)
	movf	(0+(?_increment)),w
	clrf	(main@b)
	addwf	(main@b)

	line	120
	
l1385:	
;FunctionPointer.c: 120: b = 0;
	movlw	low(0)
	movwf	(main@b)
	movlw	high(0)
	movwf	((main@b))+1
	goto	l45
	line	123
	
l46:	
	line	92
	goto	l45
	
l47:	
	line	124
	
l48:	
	global	start
	ljmp	start
	opt stack 0
GLOBAL	__end_of_main
	__end_of_main:
;; =============== function _main ends ============

	signat	_main,88
	global	_cubed
	global	_increment
	global	_decrement
	global	_square
	global	_add
	global	_subtract
psect	text147,local,class=CODE,delta=2
global __ptext147
__ptext147:

;; *************** function _subtract *****************
;; Defined at:
;;		line 70 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Function_Pointer2\FunctionPointer.c"
;; Parameters:    Size  Location     Type
;;  x               2    6[COMMON] int 
;;  y               2    8[COMMON] int 
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;                  2    6[COMMON] int 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         4       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          2       0       0       0       0
;;      Totals:         6       0       0       0       0
;;Total ram usage:        6 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text147
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Function_Pointer2\FunctionPointer.c"
	line	70
	global	__size_of_subtract
	__size_of_subtract	equ	__end_of_subtract-_subtract
	
_subtract:	
	opt	stack 7
; Regs used in _subtract: [wreg+status,2+status,0]
	
l1333:	
	comf	(subtract@y),w
	movwf	(??_subtract+0)+0
	comf	(subtract@y+1),w
	movwf	((??_subtract+0)+0+1)
	incf	(??_subtract+0)+0,f
	skipnz
	incf	((??_subtract+0)+0+1),f
	movf	(subtract@x),w
	addwf	0+(??_subtract+0)+0,w
	movwf	(?_subtract)
	movf	(subtract@x+1),w
	skipnc
	incf	(subtract@x+1),w
	addwf	1+(??_subtract+0)+0,w
	movwf	1+(?_subtract)
	goto	l36
	
l1335:	
	
l36:	
	return
	opt stack 0
GLOBAL	__end_of_subtract
	__end_of_subtract:
;; =============== function _subtract ends ============

	signat	_subtract,8314
psect	text148,local,class=CODE,delta=2
global __ptext148
__ptext148:

;; *************** function _add *****************
;; Defined at:
;;		line 67 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Function_Pointer2\FunctionPointer.c"
;; Parameters:    Size  Location     Type
;;  x               2    6[COMMON] int 
;;  y               2    8[COMMON] int 
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;                  2    6[COMMON] int 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         4       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         4       0       0       0       0
;;Total ram usage:        4 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text148
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Function_Pointer2\FunctionPointer.c"
	line	67
	global	__size_of_add
	__size_of_add	equ	__end_of_add-_add
	
_add:	
	opt	stack 7
; Regs used in _add: [wreg+status,2+status,0]
	
l1329:	
	movf	(add@y),w
	addwf	(add@x),w
	movwf	(?_subtract)
	movf	(add@y+1),w
	skipnc
	incf	(add@y+1),w
	addwf	(add@x+1),w
	movwf	1+(?_subtract)
	goto	l33
	
l1331:	
	
l33:	
	return
	opt stack 0
GLOBAL	__end_of_add
	__end_of_add:
;; =============== function _add ends ============

	signat	_add,8314
psect	text149,local,class=CODE,delta=2
global __ptext149
__ptext149:

;; *************** function _square *****************
;; Defined at:
;;		line 61 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Function_Pointer2\FunctionPointer.c"
;; Parameters:    Size  Location     Type
;;  x               2    6[COMMON] int 
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;                  2    6[COMMON] int 
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         4       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         4       0       0       0       0
;;Total ram usage:        4 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		___wmul
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text149
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Function_Pointer2\FunctionPointer.c"
	line	61
	global	__size_of_square
	__size_of_square	equ	__end_of_square-_square
	
_square:	
	opt	stack 6
; Regs used in _square: [wreg+status,2+status,0+pclath+cstack]
	
l1349:	
	movf	(square@x+1),w
	clrf	(?___wmul+1)
	addwf	(?___wmul+1)
	movf	(square@x),w
	clrf	(?___wmul)
	addwf	(?___wmul)

	movf	(square@x+1),w
	clrf	1+(?___wmul)+02h
	addwf	1+(?___wmul)+02h
	movf	(square@x),w
	clrf	0+(?___wmul)+02h
	addwf	0+(?___wmul)+02h

	fcall	___wmul
	movf	(1+(?___wmul)),w
	clrf	(?_square+1)
	addwf	(?_square+1)
	movf	(0+(?___wmul)),w
	clrf	(?_square)
	addwf	(?_square)

	goto	l27
	
l1351:	
	
l27:	
	return
	opt stack 0
GLOBAL	__end_of_square
	__end_of_square:
;; =============== function _square ends ============

	signat	_square,4218
psect	text150,local,class=CODE,delta=2
global __ptext150
__ptext150:

;; *************** function _decrement *****************
;; Defined at:
;;		line 58 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Function_Pointer2\FunctionPointer.c"
;; Parameters:    Size  Location     Type
;;  x               2    6[COMMON] int 
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;                  2    6[COMMON] int 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         4       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         4       0       0       0       0
;;Total ram usage:        4 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text150
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Function_Pointer2\FunctionPointer.c"
	line	58
	global	__size_of_decrement
	__size_of_decrement	equ	__end_of_decrement-_decrement
	
_decrement:	
	opt	stack 7
; Regs used in _decrement: [wreg+status,2+status,0]
	
l1325:	
	movf	(decrement@x),w
	addlw	low(-1)
	movwf	(?_decrement)
	movf	(decrement@x+1),w
	skipnc
	addlw	1
	addlw	high(-1)
	movwf	1+(?_decrement)
	goto	l24
	
l1327:	
	
l24:	
	return
	opt stack 0
GLOBAL	__end_of_decrement
	__end_of_decrement:
;; =============== function _decrement ends ============

	signat	_decrement,4218
psect	text151,local,class=CODE,delta=2
global __ptext151
__ptext151:

;; *************** function _increment *****************
;; Defined at:
;;		line 55 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Function_Pointer2\FunctionPointer.c"
;; Parameters:    Size  Location     Type
;;  x               2    6[COMMON] int 
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;                  2    6[COMMON] int 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         4       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         4       0       0       0       0
;;Total ram usage:        4 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text151
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Function_Pointer2\FunctionPointer.c"
	line	55
	global	__size_of_increment
	__size_of_increment	equ	__end_of_increment-_increment
	
_increment:	
	opt	stack 7
; Regs used in _increment: [wreg+status,2+status,0]
	
l1321:	
	movf	(increment@x),w
	addlw	low(01h)
	movwf	(?_increment)
	movf	(increment@x+1),w
	skipnc
	addlw	1
	addlw	high(01h)
	movwf	1+(?_increment)
	goto	l21
	
l1323:	
	
l21:	
	return
	opt stack 0
GLOBAL	__end_of_increment
	__end_of_increment:
;; =============== function _increment ends ============

	signat	_increment,4218
psect	text152,local,class=CODE,delta=2
global __ptext152
__ptext152:

;; *************** function _cubed *****************
;; Defined at:
;;		line 64 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Function_Pointer2\FunctionPointer.c"
;; Parameters:    Size  Location     Type
;;  x               2    6[COMMON] int 
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;                  2    6[COMMON] int 
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         4       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         4       0       0       0       0
;;Total ram usage:        4 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		___wmul
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text152
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Function_Pointer2\FunctionPointer.c"
	line	64
	global	__size_of_cubed
	__size_of_cubed	equ	__end_of_cubed-_cubed
	
_cubed:	
	opt	stack 6
; Regs used in _cubed: [wreg+status,2+status,0+pclath+cstack]
	
l1353:	
	movf	(cubed@x+1),w
	clrf	(?___wmul+1)
	addwf	(?___wmul+1)
	movf	(cubed@x),w
	clrf	(?___wmul)
	addwf	(?___wmul)

	movf	(cubed@x+1),w
	clrf	1+(?___wmul)+02h
	addwf	1+(?___wmul)+02h
	movf	(cubed@x),w
	clrf	0+(?___wmul)+02h
	addwf	0+(?___wmul)+02h

	fcall	___wmul
	movf	(1+(?___wmul)),w
	clrf	(?___wmul+1)
	addwf	(?___wmul+1)
	movf	(0+(?___wmul)),w
	clrf	(?___wmul)
	addwf	(?___wmul)

	movf	(cubed@x+1),w
	clrf	1+(?___wmul)+02h
	addwf	1+(?___wmul)+02h
	movf	(cubed@x),w
	clrf	0+(?___wmul)+02h
	addwf	0+(?___wmul)+02h

	fcall	___wmul
	movf	(1+(?___wmul)),w
	clrf	(?_cubed+1)
	addwf	(?_cubed+1)
	movf	(0+(?___wmul)),w
	clrf	(?_cubed)
	addwf	(?_cubed)

	goto	l30
	
l1355:	
	
l30:	
	return
	opt stack 0
GLOBAL	__end_of_cubed
	__end_of_cubed:
;; =============== function _cubed ends ============

	signat	_cubed,4218
	global	___wmul
psect	text153,local,class=CODE,delta=2
global __ptext153
__ptext153:

;; *************** function ___wmul *****************
;; Defined at:
;;		line 3 in file "C:\Program Files (x86)\HI-TECH Software\PICC\9.80\sources\wmul.c"
;; Parameters:    Size  Location     Type
;;  multiplier      2    0[COMMON] unsigned int 
;;  multiplicand    2    2[COMMON] unsigned int 
;; Auto vars:     Size  Location     Type
;;  product         2    4[COMMON] unsigned int 
;; Return value:  Size  Location     Type
;;                  2    0[COMMON] unsigned int 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         4       0       0       0       0
;;      Locals:         2       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         6       0       0       0       0
;;Total ram usage:        6 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_square
;;		_cubed
;; This function uses a non-reentrant model
;;
psect	text153
	file	"C:\Program Files (x86)\HI-TECH Software\PICC\9.80\sources\wmul.c"
	line	3
	global	__size_of___wmul
	__size_of___wmul	equ	__end_of___wmul-___wmul
	
___wmul:	
	opt	stack 6
; Regs used in ___wmul: [wreg+status,2+status,0]
	line	4
	
l1337:	
	movlw	low(0)
	movwf	(___wmul@product)
	movlw	high(0)
	movwf	((___wmul@product))+1
	goto	l1339
	line	6
	
l57:	
	line	7
	
l1339:	
	btfss	(___wmul@multiplier),(0)&7
	goto	u2141
	goto	u2140
u2141:
	goto	l58
u2140:
	line	8
	
l1341:	
	movf	(___wmul@multiplicand),w
	addwf	(___wmul@product),f
	skipnc
	incf	(___wmul@product+1),f
	movf	(___wmul@multiplicand+1),w
	addwf	(___wmul@product+1),f
	
l58:	
	line	9
	movlw	01h
	
u2155:
	clrc
	rlf	(___wmul@multiplicand),f
	rlf	(___wmul@multiplicand+1),f
	addlw	-1
	skipz
	goto	u2155
	line	10
	
l1343:	
	movlw	01h
	
u2165:
	clrc
	rrf	(___wmul@multiplier+1),f
	rrf	(___wmul@multiplier),f
	addlw	-1
	skipz
	goto	u2165
	line	11
	movf	((___wmul@multiplier+1)),w
	iorwf	((___wmul@multiplier)),w
	skipz
	goto	u2171
	goto	u2170
u2171:
	goto	l1339
u2170:
	goto	l1345
	
l59:	
	line	12
	
l1345:	
	movf	(___wmul@product+1),w
	clrf	(?___wmul+1)
	addwf	(?___wmul+1)
	movf	(___wmul@product),w
	clrf	(?___wmul)
	addwf	(?___wmul)

	goto	l60
	
l1347:	
	line	13
	
l60:	
	return
	opt stack 0
GLOBAL	__end_of___wmul
	__end_of___wmul:
;; =============== function ___wmul ends ============

	signat	___wmul,8314
	global	fptotal
fptotal equ 6
	file ""
	line	0
psect	functab,class=CODE,delta=2,reloc=256
global __pfunctab
__pfunctab:
	global	fptable
fptable:
	movwf (btemp+1)&07Fh
	movlw high(fptable)
	movwf pclath
	movf (btemp+1)&07Fh,w
	addwf pc
	global	fpbase
fpbase:
	goto fpbase	; Call via a null pointer and you will get stuck here.
fp__increment:
entry__increment:
	global	entry__increment
	ljmp	_increment
	file ""
	line	0
psect	functab
fp__decrement:
	ljmp	_decrement
	file ""
	line	0
psect	functab
fp__square:
entry__square:
	global	entry__square
	ljmp	_square
	file ""
	line	0
psect	functab
fp__cubed:
	ljmp	_cubed
	file ""
	line	0
psect	functab
fp__add:
	ljmp	_add
	file ""
	line	0
psect	functab
fp__subtract:
	ljmp	_subtract
	global	btemp
	btemp set 07Eh

	DABS	1,126,2	;btemp
	global	wtemp0
	wtemp0 set btemp
	end
