/******************************************************************          
*                    HITCH_HELPER                             
*              Initial Development 22 MAR 2013 
*                Author Tom Rode  intials -TR            
*  First task test the Parallax PING))) #28015 Ultra sonic sensor
*  Range on sensor 2cm to 3m  (0.8 inches to 3.3 yards).
*  Input trigger pulse on device 2 us min or 5 us typical.
*  THoldoff 750 us.
*  Echo Return Pulse Minimum 115 us.
*  Echo Return Pulse Maximum 18.5 ms.
*  Delay before next measurement 200 us. 
*  -                    25 MAR 2013
*  1.In this setup  5 micro second pulse sent.
*  2.Wait for capture_state() to finish, which indicates a complete ring edge time start to falling edge time stop.
*  At this point the ulta sonic sensor should trigger a rising edege indicating the distance.
*             Pulse out of PIC    Pulse back from sensor      
*                    2-5us        115us---18.5ms
*                      _          ___________
*                   __| |________|_ |_ _ _ _ |________  
*                         750 us              200 us delay until next measurement.
*                       hold off
* Having issues with the PulseWidth1Counter did not seem like the timer interrupt was updating that.
* Also I made a statemachine for the capture module. Therefore not all of the logic is inside the capture
* insterrupt.  
*                        26 MAR 2013
* Added in a fine measurement if PulseWidth1CurrentCount == 1 and PORTD will use the reult of CCPR2L.
* Also can't seem to get any faster then 500ms per read.
*
*                         27 MAR 2013 
* Added in EEPROM modules for entering serial number and when or if a calibration needs to be stored.
* Considering adding in SW Watchdog and propose HW Watchdog.
* Setting up HH_PROCESS_STATE_MACHINE for future processing of sensor and UART Management.
*
*                        28 MAR 2013
* Disabled interrrupts at 5 micrsecond pulse State. Testing yielded that if Interrupts not disabled then pulse time 
* extended to 75 microseconds.  
*
*                       2 APR 2013 
* Selected to use 8Mhz internal oscillator setting to output 4 reads a second. Added in second sensor to see if CCP1 module
* working. I need to be clear  RC1 -> CCP2 SENSOR1.    
*                              RC2 -> CCP1 SENSOR2.
* Will have to adjust the timer_set roll over because oscillator frequency changed.
* Seems like CCP1 module is working the same as CCP2. Need second sensor to see interations.
* Added in versioning in EEPROM starting with V1 at address 0xF0 and developing calibration scheme.
*
*                      3 APR 2013
* Adding in EEprom logic to detect if sensor has a valid non 0xFF value. Tested with CCP1 and CCP2 Individually works good. 
* Put in a it field for flags to save bytes.
*
*                      4 APR 2013
* Added in logic for recalling sensors. Push button need to see to logic transitions before it will follow into 
* the same logic if there are no values in EEPROM for all four bytes. Also made a Constant for recall Pushbutton and a flag but
* Not using the flag may need to remove.
*
*                      5 APR 2013
* Added in UART module from previous project. Upped speed to 55555 bps. Also fixed EEprom test. Now ready for 2 complete 
* Sensors.
*
*                      6 APR 2013
* Put CCPRxL as constants to easily change resolution. Found that the higher resolution the to less variability the sensor has
* for object dection therefor may be hard to ever pick up something because its way too accurate. This test on only one sensor.
* The combiniation of two should help. Found the if CCPRxL is only >> 3 its too accurate and the following happens. So for if 
* CCPRxL >> 4 seems to allow for a liitle more slop but its any where from .25 inches to a little more than .5  of pickup.
*
*                      8 APR 2013
* Got UART to send 4 bytes one actaul sensor and one dummy sensor. Fixed EEPROM copy checking. 
*
*                      9 APR 2013
* Will test out some logic for portd LED logic SENSOR1 is RIGHT SIDE INDICATION goes on left side MAKE SURE TO THINK THE OUTPUT PLACEMENT THROUGH.
*                                              SENSOR2 is LEFT SIDE INDICATION goes on right side
*
*                      12 APR 2013
* Setting up watch dog timer. It is set for a pet rate of 590 ms before a reset. Also now dispaly will be handled in UART Statemachine.
* Testing confirms watch dog straegy working well. If sensor removed then it will never get another WD pet and will appear to remain in
* the initilization area forever. 
*
*                      15 APR 2013
* Set up PORT D to display the distance at consistant points and then if too close it will display accordingly.
*
*                      16 APR 2013
* Set up Sensor 2 to display like sensor 1. THe HC04 are here so it will be sensor 2. plan to test them independantly until
* It understood how characteristics of both are.   
*
*                      18 APR 2013
* With 2 sensors I will enable both and have them display sharing PORTD.
*
*                      19 APR 2013
* Changed watch do time to 1.2 seconds. Added in logic to detect if both sensors are in the calibrated zone if so the turn on 
* PORTD = 0xC9. This logic will be similar for the prototypes. But will have two ports to display both left and right side.
* 
*                      21 APR 2013
* Set up for RC0 to control the HC-SR04 sensor to replace the parallax. Also set a place for BIM to read in sensor counts
* in UART state. There is a problem with his set-up so will try to find a solution.
*
*                     29 APR 2013
*                   MICRO PIN ASSINMENTS
* PORTC Assignment, This will need to be followed exactly
*                    RC0 = Pulse out for CCP2 Sensor #1 assignment
*                    RC1 = CCP2 Echo back
*                    RC2 = CCP1 Echo Back
*                    RC3 = Pulse out for CCP1 Sensor #2 assingment
*                    RC4 = Pulse out for CCP1 Sensor #3 assignment
*                    RC5 = Address select for analog switch DG4157 pin 6
*                    RC6 = UART Tx pin
*                    RC7 - UART Rx pin
* PORTE Assignment,  RE0 = Switch input for EEPROM handling.
*************************************************************************************************************************/
#include <htc.h>
#include "typedefs.h"
#include "capture.h"
#include "timer.h"
#include "eeprom.h"
#include "uart_func.h"
/*********************************************************/
/*  Constants used in main
/*********************************************************/
#define HH_PROCESS_STATE_MACHINE_TIME      (50)   /* in milliseconds*/
#define HH_PROCESS_STATE_MACHINE_INIT_TIME (200)   /* in milliseconds*/
#define SENSOR_CAL_BUTTON                  (RE0)
#define SENSOR1_RESOLUTION_ADJUSTMENT      (CCPR2L >> 4)
#define SENSOR2_RESOLUTION_ADJUSTMENT      (CCPR1L >> 4)
#define PROCESS_SENSOR1_ENABLED
#define PROCESS_SENSOR2_ENABLED 

const uint8_t RightOutDisplay[] = {0x00,0x00,0x01,0x02,0x03,0x04,0x05};
const uint8_t LeftOutDisplay[]  = {0x00,0x03,0x02,0x01,0x00};
/**********************************************************/
/* Device Configuration
/**********************************************************/  
      /*PIC16F887*/
__CONFIG(DEBUGEN & LVPEN & FCMEN & IESODIS & BORXSLP & DUNPROTECT & UNPROTECT & MCLREN & PWRTEN & WDTEN & INTCLK); //HS means resonator 8Mhz
__CONFIG(BORV40);        
      
 /* Global define for this module*/      
enum
    {
    INIT,
    PULSE_OUT_CCP2,
    PULSE_OUT_CCP1,
    UART,
    EEPROM_SENSOR_CALIBRATION,
    ECHO_TIME
  }HH_PROCESS_STATE_MACHINE_STATES;

/**********************************************************/
/* Function Prototypes 
/**********************************************************/
void init_micro(void);                  /* SFR inits*/
int main(void);
void AreSensorsCaled(void);
void RecalSensors(void);
void AreCalCopyCorrect(void);
void eepSens1WriteCal(void);
void eepSens2WriteCal(void);
void read_serial_ver(void);
/***********************************************************/
/* Global Variable Definition in module
/***********************************************************/
static uint8_t read_current_sn[3];
static uint8_t HH_PROCESS_STATE_MACHINE;                    /* This variable is the name of the Overall State the hitch helper is in */
//uint32_t PulseWidth1Counter;
 struct
     {
       uint8_t EepSensor1CalFlag     : 1;
       uint8_t EepSensor2CalFlag     : 1;
       uint8_t EepSensor1CalAchieved : 1;
       uint8_t EepSensor2CalAchieved : 1;
       uint8_t padding : 4;
     }EepFlags;
int main(void)
{
/************************************************************/
/* LOCAL MAIN VARIABLES                                     */
/************************************************************/
//static uint8_t HH_PROCESS_STATE_MACHINE;                    /* This variable is the name of the Overall State the hitch helper is in */
static uint8_t PulseStateCountInitCount;
static uint8_t OneMillisecondTimerDisable;
//static uint8_t  EepSensor1CalFlag;                            /* If no value in EEPROM for sensors flags may need to be in a bit field*/
static uint16_t EchoBackRisingEdge;
static uint16_t EchoBackFallingEdge;
/************************************************************/
/* STUCTURES FLAGS IN MAIN
/************************************************************/
//struct
//     {
//       uint8_t EepSensor1CalFlag : 1;
       //uint8_t RecalSensorsFlag : 1;
//       uint8_t padding : 7;
//     }EepFlags;
/***********************************************************/
/* INITIALIZATIONS                                         */
/***********************************************************/ 
init_micro();                                               /* Initialize SFR*/                                            
TMR1IE = 1;                                                 /* Timer 1 interrupt enable*/
timer_init();                                               /* clear out timer one*/
CCP1IE = 1;                                                 /* enable Capture 1 interrupt*/
CCP2IE = 1;                                                 /* enable Capture 2 interrupt*/
PEIE = 1;                                                   /* enable all peripheral interrupts*/
ei();                                                       /* enable all interrupts*/
timer_set(PULSE_OUT_TIMER,HH_PROCESS_STATE_MACHINE_TIME);   /* This will have to be tested for 5 micro seconds adjust the tmer isr*/  
HH_PROCESS_STATE_MACHINE = INIT;                            /* Goto to the begining of HH_PROCESS_STATE_MACHINE*/
OneMillisecondTimerDisable = FALSE;                         /* Do not preload timer on with 1 ms tick*/ 
//eeprom_write_block(0xF0,4,&default_sn[0]);  
//eeprom_write_block(EepSwVersionAddr,1,&ver1[0]);          /* Write the Software version at 0xF0*/
//eeprom_read_block1(0xFC,4,&read_current_sn[0]); 
//eep_default();                                             /*Erase EEPROM memory method*.  
//eeprom_write(EepSen1LoValAddr,  0x49);                    /* Test the copy check logic*/
/************************************************************/
/*  MAIN LOOP OVERALL                                       */
/************************************************************/

 while (HTS == 0)
  {}                                                         /* wait until internal clock stable*/ 

while(1)                   
{  
  switch(HH_PROCESS_STATE_MACHINE)
  {
    case INIT:
      if(get_timer(PULSE_OUT_TIMER) == 0)
       {
          CLRWDT();                                /* Pet watchdog while waiting in ititialization*/
          PORTD ^= 0xFF;                           /* BIM PUT A BREAK POINT HERE TO SEE IF DEBUGGER GETS HERE*/
          SENSOR_CAL_BUTTON = FALSE;               /* Test RC0 if damaged*/ 
          read_serial_ver();
          timer_set(PULSE_OUT_TIMER,HH_PROCESS_STATE_MACHINE_INIT_TIME);
          HH_PROCESS_STATE_MACHINE = INIT;
          AreSensorsCaled();                       /* Test if Sensors EEPROM addresses are not 0xFF all 4 bytes*/
          EepFlags.EepSensor1CalFlag = FALSE;
          EepFlags.EepSensor2CalFlag = FALSE;
          PulseStateCountInitCount++;
          RecalSensors();                          /* Do the sensors need to be recal? */ 
          AreCalCopyCorrect();                     /* Check to see if original and copied values match for both sensors*/               
          if(PulseStateCountInitCount >=20)//>=10)
          {
              /*  EEPROM CONTENTS now valid*/
              HH_PROCESS_STATE_MACHINE = PULSE_OUT_CCP2;
              timer_set(PULSE_OUT_TIMER,HH_PROCESS_STATE_MACHINE_TIME);
          } 
       } 
       else
          {
           asm("nop");
          }
     
       break;
    /* SENSOR 1 RIGHT SIDE INDICATION STATE MACHINE*/
    case PULSE_OUT_CCP2:
        if(get_timer(PULSE_OUT_TIMER) == 0)
        {
         OneMillisecondTimerDisable = TRUE;
         CCP2CON = RISING_EDGE_CCP2;                /*0101 = Capture mode, every rising edge*/
        /* Testing yields a 5 micro second pulse*/
         //TRISC0 = FALSE;                            /* Make RC0 output when needed*/
         TRISC1 = FALSE;                            /* Make RC1 an output*/
         di();                                      /* Found pulse with larger time if not disabling int*/ 
         //RC0 = TRUE;
         RC1 = TRUE;
         asm("nop"); 
         asm("nop");
         asm("nop"); 
         asm("nop");
         asm("nop");
         //RC0 = FALSE;
         RC1 = FALSE;
         ei();
         TRISC1 = TRUE;                            /* Make RC1 an input*/
         asm("nop");
         ProcessingCaptureModule = FALSE;
         while(ProcessingCaptureModule == FALSE)
         {
            capture_state1();                      /*Wait here until capture process is complete*/
         };                                            
         OneMillisecondTimerDisable = FALSE;      /* This will reinitialize timer 1 settings for a 1 ms tick time*/
       /* OLD WAY OF OUTPUTING FROM CCP2 TO PORT D */
        //eepSens1WriteCal();
         timer_set(PULSE_OUT_TIMER,HH_PROCESS_STATE_MACHINE_TIME);           
         if(EepFlags.EepSensor1CalFlag == TRUE)
         { 
           eepSens1WriteCal();
           HH_PROCESS_STATE_MACHINE = PULSE_OUT_CCP1;        /* Go to the nect sensor and calibrate it*/
         }
         else
         {
           HH_PROCESS_STATE_MACHINE = PULSE_OUT_CCP1; /* test when 2 sensors available but normal sceanrio for flow*/
        // HH_PROCESS_STATE_MACHINE = UART;//PULSE_OUT_CCP2;
         }
        }else
           {
              /*no else for get_timer*/
           }
  break; 
  /* SENSOR 2 LEFT SIDE INDICATION STATE MACHINE*/
  case PULSE_OUT_CCP1:
    if(get_timer(PULSE_OUT_TIMER) == 0)
    {
      OneMillisecondTimerDisable = TRUE;
      CCP1CON = RISING_EDGE_CCP1;                /*0101 = Capture mode, every rising edge*/
      /* Testing yields a 5 micro second pulse*/
      TRISC3 = FALSE;                           /* Make RC3 an output*/
      di();                                     /* Found pulse with larger time if not disabling int*/ 
      RC3 = TRUE;
      asm("nop"); 
      asm("nop");
      asm("nop"); 
      asm("nop");
      asm("nop");
      RC3 = FALSE;
      ei();
      TRISC2 = TRUE;                           /* Make RC2 an input*/
      asm("nop");
      ProcessingCaptureModule = FALSE;
      while(ProcessingCaptureModule == FALSE)
      {
        capture_state2();                      /*Wait here until capture process is complete*/
      };                                            
      OneMillisecondTimerDisable = FALSE;      /* This will reinitialize timer 1 settings for a 1 ms tick time*/
       /* OLD WAY OF OUTPUTING FROM CCP1 TO PORT D */
      //eepSens2WriteCal();                     /* Used for testing*/
      timer_set(PULSE_OUT_TIMER,HH_PROCESS_STATE_MACHINE_TIME);           
      if(EepFlags.EepSensor2CalFlag == TRUE)
      { 
        eepSens2WriteCal();
        HH_PROCESS_STATE_MACHINE = INIT;        /* Go back to the initialization*/
      }
      else
      {
        //HH_PROCESS_STATE_MACHINE = PULSE_OUT_CCP1; /* This tested good 3 APR 2013 */
        HH_PROCESS_STATE_MACHINE = UART;             /* Normal scenario*/
      }
      
    }else
    {
      /*no else for get_timer*/
    }
    break;
    
  case UART:
     /* Now at this point there is a current sensor count for CCP2 and CCP1
       SENSOR2 -> PulseWidth2CurrentCount = coarse, CCPR1L = Fine.
       SENSOR1 -> PulseWidth1CurrentCount = coarse, CCPR2L = Fine.
       These need to be tested here. See if output on target.
     */   
/*********************START OF PROCESSING SENSOR 1 RIGHT RIGHT SIDE INDICATION**********************/
#ifdef PROCESS_SENSOR1_ENABLED 
    
    if((EepSen1HiVal == PulseWidth1CurrentCount) && (EepSen1LoVal == SENSOR1_RESOLUTION_ADJUSTMENT))
    {
      EepFlags.EepSensor1CalAchieved = TRUE;
      //  PORTD &= 0xF0;          /* maintain the Hi byte the cal so output the cal*/
      //  PORTD ^= 0x03;          /* Turn on lo byte*/
    }
    else if(PulseWidth1CurrentCount == 0)//<= 1)      /* CLOSE or Fine adjustment Zero will never happen*/
    { 
      EepFlags.EepSensor1CalAchieved = FALSE;
      if(SENSOR1_RESOLUTION_ADJUSTMENT <= 15)
      {
        PORTD &= 0xF0;
        PORTD ^= 0x0F;
      }
      if(SENSOR1_RESOLUTION_ADJUSTMENT <= 9)
      {
        PORTD &= 0xF0;
        PORTD ^= 0x07;
      }
      if(SENSOR1_RESOLUTION_ADJUSTMENT <= 6)
      {
        PORTD &= 0xF0;
        PORTD ^= 0x03;
      }
      if(SENSOR1_RESOLUTION_ADJUSTMENT <= 3)
      {
        PORTD &= 0xF0;
        PORTD ^= 0x01;
      }
    }
    else if (PulseWidth1CurrentCount >= 1)//> 1)                   /* FAR or Coarse adjustment*/
    {  
       EepFlags.EepSensor1CalAchieved = FALSE;
       PORTD &= 0xF0;                                         /* Refresh the value while maintaining Hi byte and resetting lo to zero*/
       PulseWidth1CurrentCount = (PulseWidth1CurrentCount >> 1);
       PORTD = (PORTD | (0x08 >> (PulseWidth1CurrentCount - 1)));
      //PulseWidth1CurrentCount = (PulseWidth1CurrentCount - 1);/* Set this to 0x01 not 0x02*/
      //PORTD = (PORTD | (0x08 >> PulseWidth1CurrentCount));   // was 0x01 <<
    }

#endif /* PROCESS_SENSOR1_ENABLED */     
/******************* END OF PROCESSING SENSOR 1 RIGHT SIDE INDICATION********************************/
/****************************************************************************************************/
/********************START OF PROCESSING SENSOR 2 LEFT SIDE INDICATION*******************************/ 
#ifdef PROCESS_SENSOR2_ENABLED  
if((EepSen2HiVal == PulseWidth2CurrentCount) && (EepSen2LoVal == SENSOR2_RESOLUTION_ADJUSTMENT))
     {
       EepFlags.EepSensor2CalAchieved = TRUE;
       //PORTD &= 0x0F;          /* maintain the Lo byte the cal so output the cal*/
      // PORTD  = 0xC0;          /* test which worked it is seeing the cal so output the cal*/
     }    
    else if(PulseWidth2CurrentCount == 0)//<= 1)       /* CLOSE or Fine adjustment 0 Never happen*/
     {
      EepFlags.EepSensor2CalAchieved = FALSE; 
      if(SENSOR2_RESOLUTION_ADJUSTMENT <= 15)
      {
        PORTD &= 0x0F;
        PORTD ^= 0xF0;
      }
      if(SENSOR2_RESOLUTION_ADJUSTMENT <= 9)
      {
        PORTD &= 0x0F;
        PORTD ^= 0x70;
      }
      if(SENSOR2_RESOLUTION_ADJUSTMENT <= 6)
      {
        PORTD &= 0x0F;
        PORTD ^= 0x30;
      }
      if(SENSOR2_RESOLUTION_ADJUSTMENT <= 3)
      {
        PORTD &= 0x0F;
        PORTD ^= 0x10;
      }
     }
     else if (PulseWidth2CurrentCount >= 1)//> 1)
     {  
       EepFlags.EepSensor2CalAchieved = FALSE;
       PORTD &= 0x0F;                               /* Refresh the value while maintaining Lo byte and resetting lo to zero*/
       PulseWidth2CurrentCount = (PulseWidth2CurrentCount >> 1);
       PORTD = (PORTD | (0x10 << (PulseWidth2CurrentCount - 1)));
      /* Breakpoint here at the nop*/
       asm("nop");
       //PulseWidth2CurrentCount = (PulseWidth2CurrentCount - 1);/* Set this to 0x01 not 0x02*/
       //PORTD = (PORTD | (0x10 << PulseWidth2CurrentCount )); //was 0x10 << Pul...
     }
     if((EepFlags.EepSensor1CalAchieved == TRUE) && (EepFlags.EepSensor2CalAchieved == TRUE))
     {
       PORTD = 0xC3;
     } 
     
#endif /* PROCESS_SENSOR2_ENABLED */    
     
     
/******************* END OF PROCESSING SENSOR 2 LEFT SIDE INDICATION********************************/     
          /* HiVal SENSOR1           LoVal SENSOR1                 HiVal SENSOR2           LoVal SENSOR2*              */
     uart_tx(PulseWidth1CurrentCount,SENSOR1_RESOLUTION_ADJUSTMENT,PulseWidth2CurrentCount,SENSOR2_RESOLUTION_ADJUSTMENT);
     //uart_tx(PulseWidth1CurrentCount,SENSOR1_RESOLUTION_ADJUSTMENT,0x33,0x99); /* For testing purposes*/
     timer_set(PULSE_OUT_TIMER,HH_PROCESS_STATE_MACHINE_TIME);
     HH_PROCESS_STATE_MACHINE = PULSE_OUT_CCP2; 
     CLRWDT();              /* This is a starting place for petting the watchdog in the normal scenario*/ 
    break;
  
  case EEPROM_SENSOR_CALIBRATION:
    //RecalSensorsFlag = TRUE;                        /* Button un-pressed*/
     PORTD = 0x22;                                    /* Give some kind of visible indication*/
     CLRWDT();                                        /* Pet watchdog while waiting for a calibration*/
     while(SENSOR_CAL_BUTTON == TRUE)                 /* Is button pressed no then wait*/
     {
       PORTD = 0x99;                                  /* Give some kind of visible indication*/
       EepFlags.EepSensor1CalFlag = TRUE;
     EepFlags.EepSensor2CalFlag = TRUE;
       timer_set(PULSE_OUT_TIMER,HH_PROCESS_STATE_MACHINE_TIME);           
       HH_PROCESS_STATE_MACHINE = PULSE_OUT_CCP2;    /* This will have to start at CCP2 in the normal scenario*/  
     }
     
    
    break;
  
  /* The below state machine may be used if the capture module methid does not work may need this for third sensor*/
   case ECHO_TIME:
        OneMillisecondTimerDisable = FALSE;
        /* This way may work but the count should be really high the farther the distance*/
        if(RC1 == TRUE)                             /* the begging of sensor pulse*/
        {
           PulseWidth2CurrentCount++;
         if(RC1 == FALSE)                            /* the end of sensor pulse*/
          { 
            PulseWidth1CurrentCount = PulseWidth2Counter;
          }
         else{}
        }
    break;

    
    default:
     break;

  }  
  
 }
}

/*******************************************************************************
* init_micro()
* Descrption: Application and Micro Specific Specical Function Register 
*             Settings. Peripherals,ports(etc).
* INPUTS: NONE
* OUTPUTS:NONE
********************************************************************************/ 
void init_micro(void)
{
 OSCCON     = 0x71;                    /* 8 Mhz clock*/
 WDTCON     = 0x15;                  /* WDTCON � � � WDTPS3=1 WDTPS2=0 WSTPS1=1 WDTPS0=1 SWDTEN=1 -> WDT turned on and prescaller value is 1:32768 1160 ms was 1:16384 or 580 ms to pet watchdog at 8 Mhz*/
 OPTION     = 0x08;                    /*|PSA=Prescale to WDT|WD rate 1:1|PS2=0|PS1=0|PS0=0| 20ms/Div*/
 ANSEL      = 0x00;                    /* Make pins digital*/
 ANSELH     = 0x00;                    /* Make pins digital*/
 TRISA      = 0x00;                    /* Indicate forward or back*/
 TRISB      = 0x00;                    /* Either left or right side */
 TRISC      = 0x00;                    /* RC1 Pin 16 CCP2, RC2 pin 17 CCP1 on pic16f887  port directions: 1=input, 0=output*/
 TRISD      = 0x00;                    /* Either left or right side */
 PORTD      = 0x0F;
 T1CON      = 0x31;                    /* T1CKPS1,T1CKPS0 = 1:8 prescaler,T1SYNC = do not synchronise external clock input ,TMR1ON = enabled*/ 
 TRISE      = 0x01;                    /* RE0 input switch*/
 uart_init();
} 
/*******************************************************************************
* AreSensorsCaled()
* Descrption: Read Sensor 1 & 2 Hi and Lo byte position into respective RAM variables. 
*             Test if all are blank.(i.e. 0xFF) if so then recalibrate Goto EEPROM
*             State Machine.
* INPUTS: NONE
* OUTPUTS:NONE
********************************************************************************/ 
void AreSensorsCaled(void)
{
   
  /* Get the contents out of EEPROM*/
  EepSen1LoVal = eeprom_read(EepSen1LoValAddr);
  EepSen1HiVal = eeprom_read(EepSen1HiValAddr);
  EepSen2LoVal = eeprom_read(EepSen2LoValAddr);
  EepSen2HiVal = eeprom_read(EepSen2HiValAddr);
  /* See if all of the 4 EEPROM locations are blank 0xFF*/
  if((EepSen1LoVal == 0xFF) || (EepSen1HiVal == 0xFF) || (EepSen2LoVal == 0xFF) || (EepSen2HiVal == 0xFF)) //was &&
  {
    timer_set(PULSE_OUT_TIMER,HH_PROCESS_STATE_MACHINE_TIME);           
    HH_PROCESS_STATE_MACHINE = EEPROM_SENSOR_CALIBRATION;
    //while(1){};/*stay here remove after testing*/
  }
  else
    {
      /*NOTHING*/
    }
  
}
/*******************************************************************************
* AreCalCopyCorrect()
* Descrption: Read in original and copied value from both sensors and make a test 
*             byte for byte. If not then recal.
*             
* INPUTS: NONE
* OUTPUTS:NONE
********************************************************************************/ 
void AreCalCopyCorrect(void)
{
   /* Get the Original contents out of EEPROM*/
  EepSen1LoVal = eeprom_read(EepSen1LoValAddr);
  EepSen1HiVal = eeprom_read(EepSen1HiValAddr);
  EepSen2LoVal = eeprom_read(EepSen2LoValAddr);
  EepSen2HiVal = eeprom_read(EepSen2HiValAddr);
   /* Get the Copy contents out of EEPROM*/
  EepSen1LoValCopy = eeprom_read(EepSen1LoValAddrCopy);
  EepSen1HiValCopy = eeprom_read(EepSen1HiValAddrCopy);
  EepSen2LoValCopy = eeprom_read(EepSen2LoValAddrCopy);
  EepSen2HiValCopy = eeprom_read(EepSen2HiValAddrCopy);
 
  if((EepSen1LoVal != EepSen1LoValCopy) || (EepSen1HiVal != EepSen1HiValCopy) || (EepSen2LoVal != EepSen2LoValCopy) || (EepSen2HiVal != EepSen2HiValCopy))
   {
     timer_set(PULSE_OUT_TIMER,HH_PROCESS_STATE_MACHINE_TIME);           
     HH_PROCESS_STATE_MACHINE = EEPROM_SENSOR_CALIBRATION;
   }
  else
    {
      /*NOTHING*/
    }
}
/*******************************************************************************
* RecalSensors()
* Descrption: Read in push button while in the initialization. If pushed go through 
*             Read and write CCp1 and CCP2 sensors. Need a complete button cycle to 
*             get into EEPROM calibration logic once inside then another button press.
*             
* INPUTS: NONE
* OUTPUTS:NONE
********************************************************************************/ 
void RecalSensors(void)
{
 if(SENSOR_CAL_BUTTON == TRUE)            /* Button pressed*/
 {
    timer_set(PULSE_OUT_TIMER,HH_PROCESS_STATE_MACHINE_TIME);           
    HH_PROCESS_STATE_MACHINE = EEPROM_SENSOR_CALIBRATION; 

 }
else{}
}
/*******************************************************************************
* eepSens1WriteCal()
* Descrption: Write eeprom value for sensor 1, Byte order Hi-Byte then Lo-Byte.              
* INPUTS: NONE
* OUTPUTS:NONE
********************************************************************************/ 
void eepSens1WriteCal(void)
{
  EepSen1LoVal = SENSOR1_RESOLUTION_ADJUSTMENT;
  EepSen1LoValCopy = SENSOR1_RESOLUTION_ADJUSTMENT;  
  eeprom_write(EepSen1LoValAddr,  EepSen1LoVal);     /* Original value*/
  eeprom_write(EepSen1LoValAddrCopy,  EepSen1LoValCopy); /* Copy value*/
 
  EepSen1HiVal = PulseWidth1CurrentCount;
  EepSen1HiValCopy = PulseWidth1CurrentCount;
  eeprom_write(EepSen1HiValAddr,  EepSen1HiVal);     /* Original value*/
  eeprom_write(EepSen1HiValAddrCopy,  EepSen1HiValCopy); /* Copy value*/
}
/*******************************************************************************
* eepSens2WriteCal()
* Descrption: Write eeprom value for sensor 2, Byte order Hi-Byte then Lo-Byte.              
* INPUTS: NONE
* OUTPUTS:NONE
********************************************************************************/ 
void eepSens2WriteCal(void)
{
  EepSen2LoVal = SENSOR2_RESOLUTION_ADJUSTMENT; 
  EepSen2LoValCopy = SENSOR2_RESOLUTION_ADJUSTMENT; 
  eeprom_write(EepSen2LoValAddr,  EepSen2LoVal);     /* Original value*/
  eeprom_write(EepSen2LoValAddrCopy,  EepSen2LoValCopy); /* Copy value*/
  
  EepSen2HiVal = PulseWidth2CurrentCount;
  EepSen2HiValCopy = PulseWidth2CurrentCount;
  eeprom_write(EepSen2HiValAddr,  EepSen2HiVal);     /* Original value*/
  eeprom_write(EepSen2HiValAddrCopy,  EepSen2HiValCopy); /* Copy value*/
}


/*******************************************************************************
* read_serial_ver()
* Descrption: Read Serial numbers at bytes 0xFC. if none specified then write 
* a default.              
* INPUTS: NONE
* OUTPUTS:NONE
********************************************************************************/ 
void read_serial_ver(void)
{
uint8_t i;
//eeprom_write_block(EepVersionInfo ,2,&ver1[0]);
eeprom_read_block1(0xFC,3,&read_current_sn[0]);
for(i=0;i<=3;i++)
 {
   if(read_current_sn[i] != tom_sn1[i])
   {
     eeprom_write_block(EepSerialNumbyte0,3,&default_sn[0]); 
   }
  else if(read_current_sn[i] != tom_sn2[i])
   {
     eeprom_write_block(EepSerialNumbyte0,3,&default_sn[0]);  
   } 
  else if(read_current_sn[i] != bim_sn1[i])
   {
     eeprom_write_block(EepSerialNumbyte0,3,&default_sn[0]);  
   }
  else if(read_current_sn[i] != bim_sn2[i])
   {
     eeprom_write_block(EepSerialNumbyte0,3,&default_sn[0]);   
   }
  else
    {
      /*Do Nothing*/
    } 
 }  
}
