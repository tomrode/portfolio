/*****************************************************************************/
/* FILE NAME:  eeprom.h                                                    */
/*                                                                           */
/* Description:  eeprom functionality.                                  */
/*****************************************************************************/

#include "htc.h"
#include "typedefs.h"

             /* EEPROM ADDRESSES*/
enum
{
  EepSen1HiValAddr     = 0x00,
  EepSen1LoValAddr     = 0x01,
  EepSen2HiValAddr     = 0x02,
  EepSen2LoValAddr     = 0x03,
  EepSen3HiValAddr     = 0x04,
  EepSen3LoValAddr     = 0x05,
  EepSen1HiValAddrCopy = 0x70,
  EepSen1LoValAddrCopy = 0x71,
  EepSen2HiValAddrCopy = 0x72,
  EepSen2LoValAddrCopy = 0x73,
  EepSen3HiValAddrCopy = 0x74,
  EepSen3LoValAddrCopy = 0x75,
  EepSwVersionAddr     = 0xF0,
  EepSerialNumbyte0    = 0xFC
  
}SERIAL_NUMBER_EEPROM_ADDRESS;
/* Structures*/
/* EEPROM VALUES FOR PROTOTYPE SERIAL NUMBERS EEPROM ADDRESS RANGE 0x0C-0x0F*/
const uint8_t tom_sn1[4]    = {"TOM1"};  /*Serial number to first board*/
const uint8_t tom_sn2[4]    = {"TOM2"};
const uint8_t bim_sn1[4]    = {"BIM1"};
const uint8_t bim_sn2[4]    = {"BIM2"}; 
const uint8_t default_sn[4] = {"DFLT"};
/* EEPROM VALUE FOR MODULE VERSION EEPROM ADDRESS RANGE 0xF0-F1*/
const uint8_t ver1[2]       = {"V1"};
/*******************  ORIGINAL VALUE *******************************************/
/* EEPROM VALUE FOR Sensor 1 fine value*/ 
uint8_t EepSen1LoVal;
/* EEPROM VALUE FOR Sensor 1 coarse value PulseWidth1CurrentCount*/ 
uint8_t EepSen1HiVal;
/* EEPROM VALUE FOR Sensor 2 fine value*/ 
uint8_t EepSen2LoVal;
/* EEPROM VALUE FOR Sensor 2 coarse value PulseWidth2CurrentCount*/ 
uint8_t EepSen2HiVal;
/* EEPROM VALUE FOR Sensor 3 fine value */ 
uint8_t EepSen3LoVal;
/* EEPROM VALUE FOR Sensor 3 coarse value PulseWidth3CurrentCount*/ 
uint8_t EepSen3HiVal;
/*********************  COPIED VALUE *******************************************/
/* EEPROM VALUE FOR Sensor 1 fine value */ 
uint8_t EepSen1LoValCopy;
/* EEPROM VALUE FOR Sensor 1 coarse value PulseWidth1CurrentCount*/ 
uint8_t EepSen1HiValCopy;
/* EEPROM VALUE FOR Sensor 2 fine value */ 
uint8_t EepSen2LoValCopy;
/* EEPROM VALUE FOR Sensor 2 coarse value PulseWidth2CurrentCount*/ 
uint8_t EepSen2HiValCopy;
/* EEPROM VALUE FOR Sensor 3 fine value */ 
uint8_t EepSen3LoValCopy;
/* EEPROM VALUE FOR Sensor 3 coarse value PulseWidth3CurrentCount*/ 
uint8_t EepSen3HiValCopy;

/* Function prototypes*/
void eep_default(void);
void eeprom_write_block(unsigned char address, unsigned char size, unsigned char * buffer); 
unsigned char *eeprom_read_block(unsigned char address, unsigned char size);
void eeprom_read_block1(unsigned char address, unsigned char size, unsigned char * buffer);
