#include <htc.h>
#include "typedefs.h"

#ifndef UART_H
#define UART_H
/* Function prototypes */
void uart_tx(uint8_t EepSen1HiValUart,uint8_t EepSen1LoValUart,uint8_t EepSen2HiValUart,uint8_t EepSen2LoValUart,uint8_t EepSen3HiValUart,uint8_t EepSen3LoValUart);
void uart3bytes_tx(uint8_t EepSen1HiValUart,uint8_t EepSen2HiValUart,uint8_t EepSen3HiValUart);
uint8_t uart_rx(void);
void uart_init(void);
void uarttx_isr(void);
void uartrx_isr(void);
#endif


