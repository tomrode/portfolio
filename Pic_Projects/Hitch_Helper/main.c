/******************************************************************          
*                    HITCH_HELPER                             
*              Initial Development 22 MAR 2013 
*                Author Tom Rode  intials -TR            
*  First task test the Parallax PING))) #28015 Ultra sonic sensor
*  Range on sensor 2cm to 3m  (0.8 inches to 3.3 yards).
*  Input trigger pulse on device 2 us min or 5 us typical.
*  THoldoff 750 us.
*  Echo Return Pulse Minimum 115 us.
*  Echo Return Pulse Maximum 18.5 ms.
*  Delay before next measurement 200 us. 
*  -                    25 MAR 2013
*  1.In this setup  5 micro second pulse sent.
*  2.Wait for capture_state() to finish, which indicates a complete ring edge time start to falling edge time stop.
*  At this point the ulta sonic sensor should trigger a rising edege indicating the distance.
*             Pulse out of PIC    Pulse back from sensor      
*                    2-5us        115us---18.5ms
*                      _          ___________
*                   __| |________|_ |_ _ _ _ |________  
*                         750 us              200 us delay until next measurement.
*                       hold off
* Having issues with the PulseWidth1Counter did not seem like the timer interrupt was updating that.
* Also I made a statemachine for the capture module. Therefore not all of the logic is inside the capture
* insterrupt.  
*                        26 MAR 2013
* Added in a fine measurement if PulseWidth1CurrentCount == 1 and PORTD will use the reult of CCPR2L.
* Also can't seem to get any faster then 500ms per read.
*
*                         27 MAR 2013 
* Added in EEPROM modules for entering serial number and when or if a calibration needs to be stored.
* Considering adding in SW Watchdog and propose HW Watchdog.
* Setting up HH_PROCESS_STATE_MACHINE for future processing of sensor and UART Management.
*
*                        28 MAR 2013
* Disabled interrrupts at 5 micrsecond pulse State. Testing yielded that if Interrupts not disabled then pulse time 
* extended to 75 microseconds.  
*
*                       2 APR 2013 
* Selected to use 8Mhz internal oscillator setting to output 4 reads a second. Added in second sensor to see if CCP1 module
* working. I need to be clear  RC1 -> CCP2 SENSOR1.    
*                              RC2 -> CCP1 SENSOR2.
* Will have to adjust the timer_set roll over because oscillator frequency changed.
* Seems like CCP1 module is working the same as CCP2. Need second sensor to see interations.
* Added in versioning in EEPROM starting with V1 at address 0xF0 and developing calibration scheme.
*
*                      3 APR 2013
* Adding in EEprom logic to detect if sensor has a valid non 0xFF value. Tested with CCP1 and CCP2 Individually works good. 
* Put in a it field for flags to save bytes.
*
*                      4 APR 2013
* Added in logic for recalling sensors. Push button need to see to logic transitions before it will follow into 
* the same logic if there are no values in EEPROM for all four bytes. Also made a Constant for recall Pushbutton and a flag but
* Not using the flag may need to remove.
*
*                      5 APR 2013
* Added in UART module from previous project. Upped speed to 55555 bps. Also fixed EEprom test. Now ready for 2 complete 
* Sensors.
*
*                      6 APR 2013
* Put CCPRxL as constants to easily change resolution. Found that the higher resolution the to less variability the sensor has
* for object dection therefor may be hard to ever pick up something because its way too accurate. This test on only one sensor.
* The combiniation of two should help. Found the if CCPRxL is only >> 3 its too accurate and the following happens. So for if 
* CCPRxL >> 4 seems to allow for a liitle more slop but its any where from .25 inches to a little more than .5  of pickup.
*
*                      8 APR 2013
* Got UART to send 4 bytes one actaul sensor and one dummy sensor. Fixed EEPROM copy checking. 
*
*                      9 APR 2013
* Will test out some logic for portd LED logic SENSOR1 is RIGHT SIDE INDICATION goes on left side MAKE SURE TO THINK THE OUTPUT PLACEMENT THROUGH.
*                                              SENSOR2 is LEFT SIDE INDICATION goes on right side
*
*                      12 APR 2013
* Setting up watch dog timer. It is set for a pet rate of 590 ms before a reset. Also now dispaly will be handled in UART Statemachine.
* Testing confirms watch dog straegy working well. If sensor removed then it will never get another WD pet and will appear to remain in
* the initilization area forever. 
*
*                      15 APR 2013
* Set up PORT D to display the distance at consistant points and then if too close it will display accordingly.
*
*                      16 APR 2013
* Set up Sensor 2 to display like sensor 1. THe HC04 are here so it will be sensor 2. plan to test them independantly until
* It understood how characteristics of both are.   
*
*                      18 APR 2013
* With 2 sensors I will enable both and have them display sharing PORTD.
*
*                      19 APR 2013
* Changed watch do time to 1.2 seconds. Added in logic to detect if both sensors are in the calibrated zone if so the turn on 
* PORTD = 0xC9. This logic will be similar for the prototypes. But will have two ports to display both left and right side.
* 
*                      21 APR 2013
* Set up for RC0 to control the HC-SR04 sensor to replace the parallax. Also set a place for BIM to read in sensor counts
* in UART state. There is a problem with his set-up so will try to find a solution.
*
*                     29 APR 2013
*                   MICRO PIN ASSINMENTS
* PORTC Assignment, This will need to be followed exactly
*                    RC0 = Pulse out for CCP2 Sensor #1 assignment
*                    RC1 = CCP2 Echo back
*                    RC2 = CCP1 Echo Back
*                    RC3 = Pulse out for CCP1 Sensor #2 assingment
*                    RC4 = Pulse out for CCP1 Sensor #3 assignment
*                    RC5 = Address select for analog switch DG4157 pin 6
*                    RC6 = UART Tx pin
*                    RC7 - UART Rx pin
* PORTE Assignment,  RE0 = Switch input for EEPROM handling, RE1 will indicate if calibration has been achieved.
* Renaming HH_PROCESS_STATE_MACHINE_STATES to accomidate for third sensor. Sensor 2 and 3 will share CCP1 module.
* Adding all the logic for Sensor3(inside EEPROM, UART, CAPTURE).
* For Analog switch the logic is as follows:     DG4157 ANALOG SWITCH
*                                       Function name   Pin#    
*                                             NO (B1)  | Pin1 is from Sensor#3
*                                             NC (B0)  | Pin3 is from Sensor#2
*                                             A  (out) | Pin4 is to CCP1 or RC2
*                                             S  (Addr)| Pin6 is from ANALOG_SW_ADDRESS_POS or RC5
*                                             V+ (PWR) | Pin5 is Vdd
*                                             GND(GND) | Pin2 is Gnd 
*                                             -------------------------------------
*                                             |          TRUTH TABLE              |
*                                             ------------------------------------
*                                             |    S(input)|    FUNCTION          |
*                                             -------------------------------------
*                                             |    0       |Bo Connects to A      |
*                                             -------------------------------------
*                                             |    1       |B1 Connects to A      |
*                                             ------------------------------------- 
* Cheched Browm out in Config bits: BOR enabled during operation and disabled during sleep, BOR set to 4.0 Volts
*
*                     2 MAY 2013
* Installed Analog swith on PICKit 2 board, Conducted tests on B0 and B1 Channel. Testing proved that when addressed accordingly
* Output (A) on this device followed. Also looked at voltage on meter and noticed .01 volts of drop 4.84 in 4.83 out.
* Installed 20 MHz crystal and used 22 pF Load capacitors. I order 18 Pf but too small to use. I see 20 MHz on scope and all okay.          
*
*                     6 MAY 2013
* Setup Sensor array and tested, all sensors seemed to work. Noticed that when I did a calibration the left and right sensors 
* Were not reporting a correct value in EEPROM. May have to make a second reading and compare the two. 
* will put in RE3-1 LEDS to read forward to back.
*
*                    12 MAY 2013
* Adding in LEDS on PORTA to display the forward/back distance. Will now use RE1 as the indication that the object has been calibrated.
* 
*                    11 JUNE 2013
* Remap the sensors so Middle Fwd/Back is on CCP2 for calibration. As it stands: RC0 is the left, RC3 is the right, RC4 is FWD/BCK.
*                                                                            eed RC4 as Fwd/Bck,  RC3 as right,     RC0 as left.
* Also will change calibration flow to do only the Fwd/bck calibration only.
*
*                    26 JUNE 2013
* Made a recovery from Various documents folder due to not compiling right. First test with Display box and it sort of works but suspect boxes
* timer interrupts messing it up.
*
*                   2 JULY 2013 
* Sending out Three sensor coarse reading in uart_tx(); no more fine count.
*
*                   5 JULY 2013
* Changed UARTtx() function to only send 3 bytes now. uart3bytes_tx() swapped positions to reflect proper right and left pickup.
*
*                  30 JULY 2013
* If needed higher buad rate inside of uart_init() //BRG16 = TRUE; /* Set for higher frequency with SPBRG set to 16 decimal should make 117.6k Hz
*                                                  //SPBRG = 0x10;
*************************************************************************************************************************/
#include <htc.h>
#include "typedefs.h"
#include "capture.h"
#include "timer.h"
#include "eeprom.h"
#include "uart_func.h"
/*********************************************************/
/*  Constants used in main
/*********************************************************/
#define HH_PROCESS_STATE_MACHINE_TIME      (50)   /* in milliseconds*/
#define HH_PROCESS_STATE_MACHINE_INIT_TIME (200)   /* in milliseconds*/
#define SENSOR_CAL_BUTTON                  (RE0)
#define ANALOG_SW_ADDRESS_POS              (RC5)
#define SENSOR2_ADDR_POSITION              (0)
#define SENSOR3_ADDR_POSITION              (1)
#define SENSOR1_RESOLUTION_ADJUSTMENT      (Sensor1FineCount >> 4)//(CCPR2L >> 4)
#define SENSOR2_RESOLUTION_ADJUSTMENT      (Sensor2FineCount >> 4)//(CCPR1L >> 4)
#define SENSOR3_RESOLUTION_ADJUSTMENT      (Sensor3FineCount >> 4)//(CCPR1L >> 4) 
#define PROCESS_SENSOR1_ENABLED
#define PROCESS_SENSOR2_ENABLED 
#define PROCESS_SENSOR3_ENABLED

const uint8_t RightOutDisplay[] = {0x00,0x00,0x01,0x02,0x03,0x04,0x05};
const uint8_t LeftOutDisplay[]  = {0x00,0x03,0x02,0x01,0x00};
/**********************************************************/
/* Device Configuration
/**********************************************************/  
      /*PIC16F887*/
__CONFIG(DEBUGEN & LVPEN & FCMEN & IESODIS & BORXSLP & DUNPROTECT & UNPROTECT & MCLREN & PWRTEN & WDTEN & INTCLK); //HS means resonator 8Mhz
__CONFIG(BORV40);        
      
 /* Global define for this module*/      
enum
    {
    INIT,
    PULSE_OUT_CCP2_SENSOR1,
    PULSE_OUT_CCP1_SENSOR2,
    PULSE_OUT_CCP1_SENSOR3,
    UART,
    EEPROM_SENSOR_CALIBRATION
  }HH_PROCESS_STATE_MACHINE_STATES;

/**********************************************************/
/* Function Prototypes 
/**********************************************************/
void init_micro(void);                  /* SFR inits*/
int main(void);
void AreSensorsCaled(void);
void RecalSensors(void);
void AreCalCopyCorrect(void);
void eepSens1WriteCal(void);
void eepSens2WriteCal(void);
void eepSens3WriteCal(void);
void read_serial_ver(void);
/***********************************************************/
/* Global Variable Definition in module
/***********************************************************/
static uint8_t read_current_sn[4];
static uint8_t Sensor1FineCount;
static uint8_t Sensor2FineCount;
static uint8_t Sensor3FineCount;
static uint8_t Sensor1ResolutionAdjustedCount;
static uint8_t Sensor2ResolutionAdjustedCount;
static uint8_t Sensor3ResolutionAdjustedCount;
//static uint8_t PulseStateCountInitCount;
static uint8_t HH_PROCESS_STATE_MACHINE;                    /* This variable is the name of the Overall State the hitch helper is in */
union
{
  struct
      {
        uint8_t EepSensor1CalFlag     : 1;
        uint8_t EepSensor2CalFlag     : 1;
        uint8_t EepSensor3CalFlag     : 1;
        uint8_t EepSensor1CalAchieved : 1;
        uint8_t EepSensor2CalAchieved : 1;
        uint8_t EepSensor3CalAchieved : 1;
        uint8_t padding : 2;
       }EepFlags;
       uint8_t ClearAllEepBits;
   }EepFlagsSetting; 
  
/*******************************************************************************
* main()
* Description:     The main function overall. This runs infinately(etc).
* INPUTS: NONE
* OUTPUTS:NONE
*******************************************************************************/ 
int main(void)
{
/************************************************************/
/* LOCAL MAIN VARIABLES                                     */
/************************************************************/
//static uint8_t HH_PROCESS_STATE_MACHINE;                    /* This variable is the name of the Overall State the hitch helper is in */
static uint8_t PulseStateCountInitCount;
static uint8_t OneMillisecondTimerDisable;
//static uint8_t  EepSensor1CalFlag;                            /* If no value in EEPROM for sensors flags may need to be in a bit field*/
static uint16_t EchoBackRisingEdge;
static uint16_t EchoBackFallingEdge;
/************************************************************/
/* STUCTURES FLAGS IN MAIN
/************************************************************/
//union
//  {
//   struct
//        {
//          uint8_t EepSensor1CalFlag1 : 1;
//          uint8_t RecalSensorsFlag1 : 1;
//          uint8_t padding1 : 7;
//        }EepFlagsBits;
//   uint8_t ClearAllEepBits;
//  }EepFlagsSetting;  
/***********************************************************/
/* INITIALIZATIONS                                         */
/***********************************************************/ 
init_micro();                                               /* Initialize SFR*/ 
TMR1IE = 1;                                                 /* Timer 1 interrupt enable*/
timer_init();                                               /* clear out timer one*/
CCP1IE = 1;                                                 /* enable Capture 1 interrupt*/
CCP2IE = 1;                                                 /* enable Capture 2 interrupt*/
PEIE = 1;                                                   /* enable all peripheral interrupts*/
ei();                                                       /* enable all interrupts*/
timer_set(PULSE_OUT_TIMER,HH_PROCESS_STATE_MACHINE_TIME);   /* This will have to be tested for 5 micro seconds adjust the tmer isr*/  
HH_PROCESS_STATE_MACHINE = INIT;                            /* Goto to the begining of HH_PROCESS_STATE_MACHINE*/
OneMillisecondTimerDisable = FALSE;                         /* Do not preload timer on with 1 ms tick*/ 
eeprom_write_block(EepSerialNumbyte0,3,&tom_sn1[0]);       /* I will write tom1 one 
//eeprom_write_block(EepSwVersionAddr,1,&ver1[0]);          /* Write the Software version at 0xF0*/
//eep_default();                                             /*Erase EEPROM memory method*.  
//eeprom_write(EepSen1LoValAddr,  0x49);                    /* Test the copy check logic*/
EepFlagsSetting.ClearAllEepBits = FALSE;
/************************************************************/
/*  MAIN LOOP OVERALL                                       */
/************************************************************/
while (HTS == 0)
{}                                                         /* wait until internal clock stable*/ 
while(1)                   
{  
  switch(HH_PROCESS_STATE_MACHINE)
  {
    case INIT:
      if(get_timer(PULSE_OUT_TIMER) == 0)
       {
          CLRWDT();                                /* Pet watchdog while waiting in ititialization*/
          SENSOR_CAL_BUTTON = FALSE;               /* Test RC0 if damaged*/ 
          read_serial_ver();
          timer_set(PULSE_OUT_TIMER,HH_PROCESS_STATE_MACHINE_INIT_TIME);
          HH_PROCESS_STATE_MACHINE = INIT;
          AreSensorsCaled();                       /* Test if Sensors EEPROM addresses are not 0xFF all 6 bytes*/
          PulseStateCountInitCount++;
          RecalSensors();                          /* Do the sensors need to be recal? */ 
          AreCalCopyCorrect();                     /* Check to see if original and copied values match for all 3 sensors*/               
           if(EepFlagsSetting.EepFlags.EepSensor1CalFlag == TRUE) //||
              //(EepFlagsSetting.EepFlags.EepSensor2CalFlag == TRUE) ||
              //(EepFlagsSetting.EepFlags.EepSensor3CalFlag == TRUE))
              {
                PulseStateCountInitCount = 20; 
                HH_PROCESS_STATE_MACHINE = PULSE_OUT_CCP2_SENSOR1;
                timer_set(PULSE_OUT_TIMER,HH_PROCESS_STATE_MACHINE_TIME);
				EepFlagsSetting.EepFlags.EepSensor1CalFlag = FALSE;   /* Make this exit from state this may have been a bug before*/
              }
            else
            {
              RE1 ^= TRUE;                        /* Blink the Calibration LED*/  
              if(PulseStateCountInitCount >=20)
              {
              /*  EEPROM CONTENTS now valid*/
                RE1 = FALSE;                            /* Turn off Indicator light now*/                    
                HH_PROCESS_STATE_MACHINE = PULSE_OUT_CCP2_SENSOR1;
                timer_set(PULSE_OUT_TIMER,HH_PROCESS_STATE_MACHINE_TIME);
				EepFlagsSetting.EepFlags.EepSensor1CalFlag = FALSE;   /* Make this exit from state this may have been a bug before*/
              }             
            } 
       } 
       else
          {
           asm("nop");
          }    
       break;
    /* SENSOR 1 Fwd/Bck INDICATION STATE MACHINE*/
    case PULSE_OUT_CCP2_SENSOR1:
        if(get_timer(PULSE_OUT_TIMER) == 0)
        {
         OneMillisecondTimerDisable = TRUE;
         CCP2CON = RISING_EDGE_CCP2;                             /*0101 = Capture mode, every rising edge*/
         /* Testing yields a 5 micro second pulse*/
         di();                                                   /* Found pulse with larger time if not disabling int*/ 
         RC0 = TRUE;                                           /* This was found to be left sensor*/
		 //RC4 = TRUE;
         asm("nop"); 
         asm("nop");
         asm("nop"); 
         asm("nop");
         asm("nop");
         RC0 = FALSE;
		 //RC4 = FALSE;
         ei();
         asm("nop");
         ProcessingCaptureModule = FALSE;
         while(ProcessingCaptureModule == FALSE)
         {
            capture_state1();                                    /*Wait here until capture process is complete*/
         };                                            
         OneMillisecondTimerDisable = FALSE;                     /* This will reinitialize timer 1 settings for a 1 ms tick time*/
       /* OLD WAY OF OUTPUTING FROM CCP2 TO PORT D */
        //eepSens1WriteCal();
         timer_set(PULSE_OUT_TIMER,HH_PROCESS_STATE_MACHINE_TIME);           
         if(EepFlagsSetting.EepFlags.EepSensor1CalFlag == TRUE)
         { 
		   Sensor1FineCount = CCPR2L; 
           eepSens1WriteCal();
           //ANALOG_SW_ADDRESS_POS    = SENSOR2_ADDR_POSITION;      /* Address the analog switch to connect from sensor 2*/
           //HH_PROCESS_STATE_MACHINE = PULSE_OUT_CCP1_SENSOR2;   /* Go to the nect sensor and calibrate it*/
		   HH_PROCESS_STATE_MACHINE = INIT;                       /* Only middle sensor will get caled.*/
         }
         else
         {
           ANALOG_SW_ADDRESS_POS    = SENSOR2_ADDR_POSITION;      /* Address the analog switch to connect from sensor 2*/
           HH_PROCESS_STATE_MACHINE = PULSE_OUT_CCP1_SENSOR2;    /* test when 2 sensors available but normal sceanrio for flow*/
         }
        }else
           {
              /*no else for get_timer*/
           }
  Sensor1FineCount = CCPR2L;                                     /* get the fine count and store*/
  Sensor1ResolutionAdjustedCount = SENSOR1_RESOLUTION_ADJUSTMENT;
  break; 
  /* SENSOR 2 RIGHT SIDE INDICATION STATE MACHINE*/
  case PULSE_OUT_CCP1_SENSOR2:
    if(get_timer(PULSE_OUT_TIMER) == 0)
    {
      OneMillisecondTimerDisable = TRUE;
      CCP1CON = RISING_EDGE_CCP1;                                /*0101 = Capture mode, every rising edge*/
      /* Testing yields a 5 micro second pulse*/
      //TRISC3 = FALSE;                                          /* Make RC3 an output*/
      di();                                                      /* Found pulse with larger time if not disabling int*/ 
      RC3 = TRUE;
      asm("nop"); 
      asm("nop");
      asm("nop"); 
      asm("nop");
      asm("nop");
      RC3 = FALSE;
      ei();
      //TRISC2 = TRUE;                                           /* Make RC2 an input*/
      asm("nop");
      ProcessingCaptureModule = FALSE;
      while(ProcessingCaptureModule == FALSE)
      {
        capture_state2();                                        /*Wait here until capture process is complete*/
      };                                            
      OneMillisecondTimerDisable = FALSE;                        /* This will reinitialize timer 1 settings for a 1 ms tick time*/
       /* OLD WAY OF OUTPUTING FROM CCP1 TO PORT D */
      //eepSens2WriteCal();                        /* Used for testing*/
      timer_set(PULSE_OUT_TIMER,HH_PROCESS_STATE_MACHINE_TIME);           
      if(EepFlagsSetting.EepFlags.EepSensor2CalFlag == TRUE)
      { 
        eepSens2WriteCal();
        ANALOG_SW_ADDRESS_POS    = SENSOR3_ADDR_POSITION;        /* Address the analog switch to connect from sensor 3*/
        HH_PROCESS_STATE_MACHINE = PULSE_OUT_CCP1_SENSOR3;       /* Go to Sensor3*/
      }
      else
      {
        ANALOG_SW_ADDRESS_POS    = SENSOR3_ADDR_POSITION;        /* Address the analog switch to connect from sensor 3*/
        HH_PROCESS_STATE_MACHINE = PULSE_OUT_CCP1_SENSOR3;       /* Normal scenario*/
        CLRWDT();                                                /* This is a starting place for petting the watchdog in the normal scenario*/ 
      }
      
    }else
    {
      /*no else for get_timer*/
    }
  Sensor2FineCount = CCPR1L;                                   /* get the fine count and store*/
  Sensor2ResolutionAdjustedCount = SENSOR2_RESOLUTION_ADJUSTMENT;
    break;
    
      /* SENSOR 3 FORWARD/BACK INDICATION STATE MACHINE*/
   case PULSE_OUT_CCP1_SENSOR3:
     if(get_timer(PULSE_OUT_TIMER) == 0)
     {
       OneMillisecondTimerDisable = TRUE;
       CCP1CON = RISING_EDGE_CCP1;                 /*0101 = Capture mode, every rising edge*/
       /* Testing yields a 5 micro second pulse*/
       //TRISC4 = FALSE;                           /* Make RC4 an output*/
       di();                                       /* Found pulse with larger time if not disabling int*/ 
       RC4 = TRUE;                               /* This is now for Fwd/Bck 11 June 2013*/
	   //RC0 = TRUE;
       asm("nop"); 
       asm("nop");
       asm("nop"); 
       asm("nop");
       asm("nop");
       RC4 = FALSE;
	   //RC0 = FALSE;
       ei();
       asm("nop");
       ProcessingCaptureModule = FALSE;
       while(ProcessingCaptureModule == FALSE)
       {
         capture_state3();                         /*Wait here until capture process is complete*/
       };                                            
       OneMillisecondTimerDisable = FALSE;         /* This will reinitialize timer 1 settings for a 1 ms tick time*/
       /* OLD WAY OF OUTPUTING FROM CCP1 TO PORT D */
       //eepSens2WriteCal();                       /* Used for testing*/
       timer_set(PULSE_OUT_TIMER,HH_PROCESS_STATE_MACHINE_TIME);           
       if(EepFlagsSetting.EepFlags.EepSensor3CalFlag == TRUE)
       { 
         eepSens3WriteCal();
         HH_PROCESS_STATE_MACHINE = INIT;          /* Go back to the initialization*/
       }
       else
       {
         //HH_PROCESS_STATE_MACHINE = PULSE_OUT_CCP1_SENSOR2; /* This tested good 3 APR 2013 */
         HH_PROCESS_STATE_MACHINE = UART;             /* Normal scenario*/
       }
       
     }else
     {
       /*no else for get_timer*/
     }
  Sensor3FineCount = CCPR1L;                         /* get the fine count and store*/  
  Sensor3ResolutionAdjustedCount = SENSOR3_RESOLUTION_ADJUSTMENT;
     break;
  case UART:
     /* Now at this point there is a current sensor count for CCP2 and CCP1
       SENSOR2 -> PulseWidth2CurrentCount = coarse, CCPR1L = Fine.
       SENSOR1 -> PulseWidth1CurrentCount = coarse, CCPR2L = Fine.
       These need to be tested here. See if output on target.
     */   
  /*********************START OF PROCESSING SENSOR 3 LEFT SIDE INDICATION**********************/
  #ifdef PROCESS_SENSOR3_ENABLED 
    
    if((EepSen3HiVal <= PulseWidth3CurrentCount) && (EepSen3LoVal <= SENSOR3_RESOLUTION_ADJUSTMENT))
    {
      EepFlagsSetting.EepFlags.EepSensor3CalAchieved = TRUE;
      //  PORTD &= 0xF0;          /* maintain the Hi byte the cal so output the cal*/
      //  PORTD ^= 0x03;          /* Turn on lo byte*/
    }
    else if(PulseWidth3CurrentCount == 0)//<= 1)      /* CLOSE or Fine adjustment Zero will never happen*/
    { 
      EepFlagsSetting.EepFlags.EepSensor3CalAchieved = FALSE;
      if(SENSOR3_RESOLUTION_ADJUSTMENT <= 15)
      {
        PORTD &= 0xF0;
        PORTD ^= 0x0F;
      }
      if(SENSOR3_RESOLUTION_ADJUSTMENT <= 9)
      {
        PORTD &= 0xF0;
        PORTD ^= 0x07;
      }
      if(SENSOR3_RESOLUTION_ADJUSTMENT <= 6)
      {
        PORTD &= 0xF0;
        PORTD ^= 0x03;
      }
      if(SENSOR3_RESOLUTION_ADJUSTMENT <= 3)
      {
        PORTD &= 0xF0;
        PORTD ^= 0x01;
      }
    }
    //else if (PulseWidth1CurrentCount >= 1)//> 1)                   /* FAR or Coarse adjustment*/
    if (PulseWidth3CurrentCount >= 1)//> 1)                   /* FAR or Coarse adjustment*/
    {  
       EepFlagsSetting.EepFlags.EepSensor3CalAchieved = FALSE;
       PORTD &= 0xF0;                                         /* Refresh the value while maintaining Hi byte and resetting lo to zero*/
       PulseWidth3CurrentCount = (PulseWidth3CurrentCount >> 1);
       PORTD = (PORTD | (0x08 >> (PulseWidth3CurrentCount - 1)));
      //PulseWidth1CurrentCount = (PulseWidth1CurrentCount - 1);/* Set this to 0x01 not 0x02*/
      //PORTD = (PORTD | (0x08 >> PulseWidth1CurrentCount));   // was 0x01 <<
    }

  #endif /* PROCESS_SENSOR3_ENABLED */     
  /******************* END OF PROCESSING SENSOR 3 LEFT SIDE INDICATION********************************/
  /****************************************************************************************************/
  /********************START OF PROCESSING SENSOR 2 RIGHT SIDE INDICATION*******************************/ 
  #ifdef PROCESS_SENSOR2_ENABLED  
     if((EepSen2HiVal <= PulseWidth2CurrentCount) && (EepSen2LoVal <= SENSOR2_RESOLUTION_ADJUSTMENT))
     {
       EepFlagsSetting.EepFlags.EepSensor2CalAchieved = TRUE;
       //PORTD &= 0x0F;          /* maintain the Lo byte the cal so output the cal*/
      // PORTD  = 0xC0;          /* test which worked it is seeing the cal so output the cal*/
     }    
    else if(PulseWidth2CurrentCount == 0)//<= 1)       /* CLOSE or Fine adjustment 0 Never happen*/
     {
      EepFlagsSetting.EepFlags.EepSensor2CalAchieved = FALSE; 
      if(SENSOR2_RESOLUTION_ADJUSTMENT <= 15)
      {
        PORTD &= 0x0F;
        PORTD ^= 0xF0;
      }
      if(SENSOR2_RESOLUTION_ADJUSTMENT <= 9)
      {
        PORTD &= 0x0F;
        PORTD ^= 0x70;
      }
      if(SENSOR2_RESOLUTION_ADJUSTMENT <= 6)
      {
        PORTD &= 0x0F;
        PORTD ^= 0x30;
      }
      if(SENSOR2_RESOLUTION_ADJUSTMENT <= 3)
      {
        PORTD &= 0x0F;
        PORTD ^= 0x10;
      }
     }
     //else if (PulseWidth2CurrentCount >= 1)//> 1)
     if (PulseWidth2CurrentCount >= 1)//> 1)
     {  
       EepFlagsSetting.EepFlags.EepSensor2CalAchieved = FALSE;
       PORTD &= 0x0F;                               /* Refresh the value while maintaining Lo byte and resetting lo to zero*/
       PulseWidth2CurrentCount = (PulseWidth2CurrentCount >> 1);
       PORTD = (PORTD | (0x10 << (PulseWidth2CurrentCount - 1)));
      /* Breakpoint here at the nop*/
       asm("nop");
       //PulseWidth2CurrentCount = (PulseWidth2CurrentCount - 1);/* Set this to 0x01 not 0x02*/
       //PORTD = (PORTD | (0x10 << PulseWidth2CurrentCount )); //was 0x10 << Pul...
     }
     
     
  #endif /* PROCESS_SENSOR2_ENABLED */    
  /******************* END OF PROCESSING SENSOR 2 RIGHT SIDE INDICATION********************************/
  /****************************************************************************************************/
  /********************START OF PROCESSING SENSOR 1 FWD/BACK  INDICATION*******************************/      
  #ifdef PROCESS_SENSOR1_ENABLED  
    if((EepSen1HiVal == PulseWidth1CurrentCount) && (EepSen1LoVal == SENSOR1_RESOLUTION_ADJUSTMENT))
    {
      EepFlagsSetting.EepFlags.EepSensor1CalAchieved = TRUE;
    } 
    else
     { 
       EepFlagsSetting.EepFlags.EepSensor1CalAchieved = FALSE;
     }
    if(PulseWidth1CurrentCount <= 4)
    {
       PORTA = 0x08;
    }
    if(PulseWidth1CurrentCount <= 3)
    {
       PORTA = 0x04;
    }
    if(PulseWidth1CurrentCount <= 2)
    {
       PORTA = 0x02;
    }
    if(PulseWidth1CurrentCount <= 1)
    {
       PORTA = 0x01;
    }
   if(EepFlagsSetting.EepFlags.EepSensor1CalAchieved == TRUE) //&& 
      //(EepFlagsSetting.EepFlags.EepSensor2CalAchieved == TRUE) && 
      //(EepFlagsSetting.EepFlags.EepSensor3CalAchieved == TRUE))
    {
      RE1 = TRUE; /* 11011011 Pattern on display*/
    }
    else
    {
       RE1 = FALSE;
    } 
 
   /******************* END OF PROCESSING SENSOR 3 FWD/BACK INDICATION********************************/     
  #endif /* PROCESS_SENSOR1_ENABLED */   
          /* HiVal SENSOR1           LoVal SENSOR1    HiVal SENSOR2           LoVal SENSOR2    HiVal SENSOR3           LoVal SENSOR3       */
  uart3bytes_tx(PulseWidth1CurrentCount,PulseWidth3CurrentCount,PulseWidth2CurrentCount);
  // uart_tx(PulseWidth1CurrentCount,PulseWidth1CurrentCount,PulseWidth2CurrentCount,PulseWidth2CurrentCount,PulseWidth3CurrentCount,PulseWidth3CurrentCount);
  //uart_tx(PulseWidth1CurrentCount,Sensor1FineCount,PulseWidth2CurrentCount,Sensor2FineCount,PulseWidth3CurrentCount,Sensor3FineCount);
     //uart_tx(PulseWidth1CurrentCount,SENSOR1_RESOLUTION_ADJUSTMENT,0x33,0x99); /* For testing purposes*/
   timer_set(PULSE_OUT_TIMER,HH_PROCESS_STATE_MACHINE_TIME);
   HH_PROCESS_STATE_MACHINE = PULSE_OUT_CCP2_SENSOR1; 
   CLRWDT();              /* This is a starting place for petting the watchdog in the normal scenario*/ 
  break;
  
  case EEPROM_SENSOR_CALIBRATION:

     PORTD = 0x22;                                    /* Give some kind of visible indication It needs to learn cal*/
     CLRWDT();                                        /* Pet watchdog while waiting for a calibration*/
     while(SENSOR_CAL_BUTTON == TRUE)                 /* Is button pressed no then wait*/
     {
       RE1 = TRUE;                         /* Turn on the Calibration LED hard to indicate its learning EEPROM*/
       EepFlagsSetting.EepFlags.EepSensor1CalFlag = TRUE;
       EepFlagsSetting.EepFlags.EepSensor2CalFlag = FALSE;//TRUE;  /* Only fwd/back sensor now 11 June 2013*/
       EepFlagsSetting.EepFlags.EepSensor3CalFlag = FALSE;//TRUE;
       timer_set(PULSE_OUT_TIMER,HH_PROCESS_STATE_MACHINE_TIME);           
       HH_PROCESS_STATE_MACHINE = PULSE_OUT_CCP2_SENSOR1;    /* This will have to start at CCP2 in the normal scenario*/  
     }
    break;

    default:
     break;
  }  
  
 }
}

/*******************************************************************************
* init_micro()
* Descrption: Application and Micro Specific Specical Function Register 
*             Settings. Peripherals,ports(etc).
* INPUTS: NONE
* OUTPUTS:NONE
********************************************************************************/ 
void init_micro(void)
{
 OSCCON     = 0x71;                    /* 8 Mhz clock*/
 WDTCON     = 0x15;                  /* WDTCON � � � WDTPS3=1 WDTPS2=0 WSTPS1=1 WDTPS0=1 SWDTEN=1 -> WDT turned on and prescaller value is 1:32768 1160 ms was 1:16384 or 580 ms to pet watchdog at 8 Mhz*/
 OPTION     = 0x08;                    /*|PSA=Prescale to WDT|WD rate 1:1|PS2=0|PS1=0|PS0=0| 20ms/Div*/
 ANSEL      = 0x00;                    /* Make pins digital*/
 ANSELH     = 0x00;                    /* Make pins digital*/
 TRISA      = 0x00;                    /* Indicate forward or back*/
 PORTA      = 0x00;                    /* Turn ofo All portA*/
 TRISB      = 0x00;                    /* Either left or right side */
 TRISC      = 0x06;                    /* RC7=UART sets|RC6=UART sets|RC5=0|RC4=0|RC3=0|RC2=1|RC1=1|RC0=0| 1=input, 0=output*/
 TRISD      = 0x00;                    /* Either left or right side */
 PORTD      = 0x00;
 T1CON      = 0x31;                    /* T1CKPS1,T1CKPS0 = 1:8 prescaler,T1SYNC = do not synchronise external clock input ,TMR1ON = enabled*/ 
 TRISE      = 0x01;                    /* RE0 input switch*/
 uart_init();
} 
/*******************************************************************************
* AreSensorsCaled()
* Descrption: Read Sensor 1 & 2 Hi and Lo byte position into respective RAM variables. 
*             Test if all are blank.(i.e. 0xFF) if so then recalibrate Goto EEPROM
*             State Machine.
* INPUTS: NONE
* OUTPUTS:NONE
********************************************************************************/ 
void AreSensorsCaled(void)
{
   
  /* Get the contents out of EEPROM*/
  EepSen1LoVal = eeprom_read(EepSen1LoValAddr);
  EepSen1HiVal = eeprom_read(EepSen1HiValAddr);
  EepSen2LoVal = eeprom_read(EepSen2LoValAddr);
  EepSen2HiVal = eeprom_read(EepSen2HiValAddr);
  EepSen3LoVal = eeprom_read(EepSen3LoValAddr);
  EepSen3HiVal = eeprom_read(EepSen3HiValAddr);
  /* See if all of the 6 EEPROM locations are blank 0xFF. 11 June 2013 just fwd/back sen.*/
  if((EepSen1LoVal == 0xFF) || (EepSen1HiVal == 0xFF))// || (EepSen2LoVal == 0xFF) || 
     //(EepSen2HiVal == 0xFF) || (EepSen3LoVal == 0xFF) || (EepSen3HiVal == 0xFF)) //was &&
  {
    timer_set(PULSE_OUT_TIMER,HH_PROCESS_STATE_MACHINE_TIME);           
    HH_PROCESS_STATE_MACHINE = EEPROM_SENSOR_CALIBRATION;
    //while(1){};/*stay here remove after testing*/
  }
  else
    {
      /*NOTHING*/
    }
  
}
/*******************************************************************************
* AreCalCopyCorrect()
* Descrption: Read in original and copied value from all 3 sensors and make a test 
*             byte for byte. If not then recal.
*             
* INPUTS: NONE
* OUTPUTS:NONE
********************************************************************************/ 
void AreCalCopyCorrect(void)
{
   /* Get the Original contents out of EEPROM*/
  EepSen1LoVal = eeprom_read(EepSen1LoValAddr);
  EepSen1HiVal = eeprom_read(EepSen1HiValAddr);
  EepSen2LoVal = eeprom_read(EepSen2LoValAddr);
  EepSen2HiVal = eeprom_read(EepSen2HiValAddr);
  EepSen3LoVal = eeprom_read(EepSen3LoValAddr);
  EepSen3HiVal = eeprom_read(EepSen3HiValAddr);
   /* Get the Copy contents out of EEPROM*/
  EepSen1LoValCopy = eeprom_read(EepSen1LoValAddrCopy);
  EepSen1HiValCopy = eeprom_read(EepSen1HiValAddrCopy);
  EepSen2LoValCopy = eeprom_read(EepSen2LoValAddrCopy);
  EepSen2HiValCopy = eeprom_read(EepSen2HiValAddrCopy);
  EepSen3LoValCopy = eeprom_read(EepSen3LoValAddrCopy);
  EepSen3HiValCopy = eeprom_read(EepSen3HiValAddrCopy);
  /* 11 June 2013 Only fwd/back sensor caled/*/
  if((EepSen1LoVal != EepSen1LoValCopy) || (EepSen1HiVal != EepSen1HiValCopy)) //|| 
     //(EepSen2LoVal != EepSen2LoValCopy) || (EepSen2HiVal != EepSen2HiValCopy) ||
     //(EepSen3LoVal != EepSen3LoValCopy) || (EepSen3HiVal != EepSen3HiValCopy))
   
    {
     timer_set(PULSE_OUT_TIMER,HH_PROCESS_STATE_MACHINE_TIME);           
     HH_PROCESS_STATE_MACHINE = EEPROM_SENSOR_CALIBRATION;
   }
  else
    {
      /*NOTHING*/
    }
}
/*******************************************************************************
* RecalSensors()
* Descrption: Read in push button while in the initialization. If pushed go through 
*             Read and write CCp1 and CCP2 sensors. Need a complete button cycle to 
*             get into EEPROM calibration logic once inside then another button press.
*             
* INPUTS: NONE
* OUTPUTS:NONE
********************************************************************************/ 
void RecalSensors(void)
{
 if(SENSOR_CAL_BUTTON == TRUE)            /* Button pressed*/
 {
    timer_set(PULSE_OUT_TIMER,HH_PROCESS_STATE_MACHINE_TIME);           
    HH_PROCESS_STATE_MACHINE = EEPROM_SENSOR_CALIBRATION; 

 }
else{}
}
/*******************************************************************************
* eepSens1WriteCal()
* Descrption: Write eeprom value for sensor 1, Byte order Hi-Byte then Lo-Byte.              
* INPUTS: NONE
* OUTPUTS:NONE
********************************************************************************/ 
void eepSens1WriteCal(void)
{
  EepSen1LoVal = SENSOR1_RESOLUTION_ADJUSTMENT;
  EepSen1LoValCopy = SENSOR1_RESOLUTION_ADJUSTMENT;  
  eeprom_write(EepSen1LoValAddr,  EepSen1LoVal);     /* Original value*/
  eeprom_write(EepSen1LoValAddrCopy,  EepSen1LoValCopy); /* Copy value*/
 
  EepSen1HiVal = PulseWidth1CurrentCount;
  EepSen1HiValCopy = PulseWidth1CurrentCount;
  eeprom_write(EepSen1HiValAddr,  EepSen1HiVal);     /* Original value*/
  eeprom_write(EepSen1HiValAddrCopy,  EepSen1HiValCopy); /* Copy value*/
 
}
/*******************************************************************************
* eepSens2WriteCal()
* Descrption: Write eeprom value for sensor 2, Byte order Hi-Byte then Lo-Byte.              
* INPUTS: NONE
* OUTPUTS:NONE
********************************************************************************/ 
void eepSens2WriteCal(void)
{
  EepSen2LoVal = SENSOR2_RESOLUTION_ADJUSTMENT; 
  EepSen2LoValCopy = SENSOR2_RESOLUTION_ADJUSTMENT; 
  eeprom_write(EepSen2LoValAddr,  EepSen2LoVal);     /* Original value*/
  eeprom_write(EepSen2LoValAddrCopy,  EepSen2LoValCopy); /* Copy value*/
  
  EepSen2HiVal = PulseWidth2CurrentCount;
  EepSen2HiValCopy = PulseWidth2CurrentCount;
  eeprom_write(EepSen2HiValAddr,  EepSen2HiVal);     /* Original value*/
  eeprom_write(EepSen2HiValAddrCopy,  EepSen2HiValCopy); /* Copy value*/
}
/*******************************************************************************
* eepSens3WriteCal()
* Descrption: Write eeprom value for sensor 3, Byte order Hi-Byte then Lo-Byte.              
* INPUTS: NONE
* OUTPUTS:NONE
********************************************************************************/ 
void eepSens3WriteCal(void)
{
  EepSen3LoVal = SENSOR3_RESOLUTION_ADJUSTMENT; 
  EepSen3LoValCopy = SENSOR3_RESOLUTION_ADJUSTMENT; 
  eeprom_write(EepSen3LoValAddr,  EepSen3LoVal);     /* Original value*/
  eeprom_write(EepSen3LoValAddrCopy,  EepSen3LoValCopy); /* Copy value*/
  
  EepSen3HiVal = PulseWidth3CurrentCount;
  EepSen3HiValCopy = PulseWidth3CurrentCount;
  eeprom_write(EepSen3HiValAddr,  EepSen3HiVal);     /* Original value*/
  eeprom_write(EepSen3HiValAddrCopy,  EepSen3HiValCopy); /* Copy value*/
}

/*******************************************************************************
* read_serial_ver()
* Descrption: Read Serial numbers at bytes 0xFC. if none specified then write 
* a default.              
* INPUTS: NONE
* OUTPUTS:NONE
********************************************************************************/ 
void read_serial_ver(void)
{
uint8_t i;
//eeprom_write_block(EepVersionInfo ,2,&ver1[0]);
eeprom_read_block1(0xFC,4,&read_current_sn[0]);
for(i=0;i<=3;i++)
 {
   if(read_current_sn[i] != tom_sn1[i]) 
   {
    if(read_current_sn[i] != tom_sn2[i])
    {
     if(read_current_sn[i] != bim_sn1[i])
     {
      if(read_current_sn[i] != bim_sn2[i])
      {
          eeprom_write_block(EepSerialNumbyte0,3,&default_sn[0]); 
      }     
     }
    }
   }
  } 
}
