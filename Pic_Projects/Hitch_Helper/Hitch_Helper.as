opt subtitle "HI-TECH Software Omniscient Code Generator (Lite mode) build 7503"

opt pagewidth 120

	opt lm

	processor	16F887
clrc	macro
	bcf	3,0
	endm
clrz	macro
	bcf	3,2
	endm
setc	macro
	bsf	3,0
	endm
setz	macro
	bsf	3,2
	endm
skipc	macro
	btfss	3,0
	endm
skipz	macro
	btfss	3,2
	endm
skipnc	macro
	btfsc	3,0
	endm
skipnz	macro
	btfsc	3,2
	endm
indf	equ	0
indf0	equ	0
pc	equ	2
pcl	equ	2
status	equ	3
fsr	equ	4
fsr0	equ	4
c	equ	1
z	equ	0
pclath	equ	10
# 188 "F:\pic_projects\Hitch_Helper\main.c"
	psect config,class=CONFIG,delta=2 ;#
# 188 "F:\pic_projects\Hitch_Helper\main.c"
	dw 0x1FFF & 0x3FFF & 0x3FFF & 0x3BFF & 0x3EFF & 0x3FFF & 0x3FFF & 0x3FFF & 0x3FEF & 0x3FFF & 0x3FFD ;#
# 189 "F:\pic_projects\Hitch_Helper\main.c"
	psect config,class=CONFIG,delta=2 ;#
# 189 "F:\pic_projects\Hitch_Helper\main.c"
	dw 0x3FFF ;#
	FNCALL	_main,_init_micro
	FNCALL	_main,_timer_init
	FNCALL	_main,_timer_set
	FNCALL	_main,_eeprom_write_block
	FNCALL	_main,_get_timer
	FNCALL	_main,_read_serial_ver
	FNCALL	_main,_AreSensorsCaled
	FNCALL	_main,_RecalSensors
	FNCALL	_main,_AreCalCopyCorrect
	FNCALL	_main,_capture_state1
	FNCALL	_main,_eepSens1WriteCal
	FNCALL	_main,_capture_state2
	FNCALL	_main,_eepSens2WriteCal
	FNCALL	_main,_capture_state3
	FNCALL	_main,_eepSens3WriteCal
	FNCALL	_main,_uart3bytes_tx
	FNCALL	_read_serial_ver,_eeprom_read_block1
	FNCALL	_read_serial_ver,_eeprom_write_block
	FNCALL	_eeprom_read_block1,_eeprom_read
	FNCALL	_eepSens3WriteCal,_eeprom_write
	FNCALL	_eepSens2WriteCal,_eeprom_write
	FNCALL	_eepSens1WriteCal,_eeprom_write
	FNCALL	_AreCalCopyCorrect,_eeprom_read
	FNCALL	_AreCalCopyCorrect,_timer_set
	FNCALL	_RecalSensors,_timer_set
	FNCALL	_AreSensorsCaled,_eeprom_read
	FNCALL	_AreSensorsCaled,_timer_set
	FNCALL	_eeprom_write_block,_eeprom_write
	FNCALL	_init_micro,_uart_init
	FNROOT	_main
	FNCALL	_interrupt_handler,_timer_isr
	FNCALL	_interrupt_handler,_capture_isr
	FNCALL	intlevel1,_interrupt_handler
	global	intlevel1
	FNROOT	intlevel1
	global	_RightOutDisplay
psect	strings,class=STRING,delta=2
global __pstrings
__pstrings:
;	global	stringdir,stringtab,__stringbase
stringtab:
;	String table - string pointers are 1 byte each
stringcode:stringdir:
movlw high(stringdir)
movwf pclath
movf fsr,w
incf fsr
	addwf pc
__stringbase:
	retlw	0
psect	strings
	file	"F:\pic_projects\Hitch_Helper\main.c"
	line	182
_RightOutDisplay:
	retlw	0
	retlw	0
	retlw	01h
	retlw	02h
	retlw	03h
	retlw	04h
	retlw	05h
	global	_LeftOutDisplay
psect	strings
	file	"F:\pic_projects\Hitch_Helper\main.c"
	line	183
_LeftOutDisplay:
	retlw	0
	retlw	03h
	retlw	02h
	retlw	01h
	retlw	0
	global	_bim_sn1
psect	strings
	file	"F:\pic_projects\Hitch_Helper\eeprom.h"
	line	33
_bim_sn1:
	retlw	042h
	retlw	049h
	retlw	04Dh
	retlw	031h
	global	_bim_sn2
psect	strings
	file	"F:\pic_projects\Hitch_Helper\eeprom.h"
	line	34
_bim_sn2:
	retlw	042h
	retlw	049h
	retlw	04Dh
	retlw	032h
	global	_tom_sn2
psect	strings
	file	"F:\pic_projects\Hitch_Helper\eeprom.h"
	line	32
_tom_sn2:
	retlw	054h
	retlw	04Fh
	retlw	04Dh
	retlw	032h
	global	_ver1
psect	strings
	file	"F:\pic_projects\Hitch_Helper\eeprom.h"
	line	37
_ver1:
	retlw	056h
	retlw	031h
	global	_default_sn
psect	strings
	file	"F:\pic_projects\Hitch_Helper\eeprom.h"
	line	35
_default_sn:
	retlw	044h
	retlw	046h
	retlw	04Ch
	retlw	054h
	global	_tom_sn1
psect	strings
	file	"F:\pic_projects\Hitch_Helper\eeprom.h"
	line	31
_tom_sn1:
	retlw	054h
	retlw	04Fh
	retlw	04Dh
	retlw	031h
	global	_RightOutDisplay
	global	_LeftOutDisplay
	global	_bim_sn1
	global	_bim_sn2
	global	_tom_sn2
	global	_ver1
	global	_default_sn
	global	_tom_sn1
	global	main@EchoBackFallingEdge
	global	main@EchoBackRisingEdge
	global	_CaptureHi2
	global	_CaptureLo2
	global	_EepFlagsSetting
	global	_EepSen1HiVal
	global	_EepSen1HiValCopy
	global	_EepSen1LoVal
	global	_EepSen1LoValCopy
	global	_EepSen2HiVal
	global	_EepSen2HiValCopy
	global	_EepSen2LoVal
	global	_EepSen2LoValCopy
	global	_EepSen3HiVal
	global	_EepSen3HiValCopy
	global	_EepSen3LoVal
	global	_EepSen3LoValCopy
	global	_HH_PROCESS_STATE_MACHINE
	global	_HH_PROCESS_STATE_MACHINE_STATES
	global	_OneMillisecondTimerDisable
	global	_ProcessingCaptureModule
	global	_PulseWidth1CurrentCount
	global	_PulseWidth2CurrentCount
	global	_PulseWidth3CurrentCount
	global	_SERIAL_NUMBER_EEPROM_ADDRESS
	global	_Sensor1FineCount
	global	_Sensor1ResolutionAdjustedCount
	global	_Sensor2FineCount
	global	_Sensor2ResolutionAdjustedCount
	global	_Sensor3FineCount
	global	_Sensor3ResolutionAdjustedCount
	global	_capture_isr_flag_ccp2
	global	_tmr1_isr_counter
	global	_toms_variable
	global	capture_state1@CAPTURE_STATE
	global	capture_state2@CAPTURE_STATE
	global	capture_state3@CAPTURE_STATE
	global	main@OneMillisecondTimerDisable
	global	main@PulseStateCountInitCount
	global	_timer_array
	global	_PulseWidth1Counter
	global	_PulseWidth2Counter
	global	_PulseWidth3Counter
	global	_capture_isr_flag_ccp1
	global	_read_current_sn
	global	_CCP1CON
_CCP1CON	set	23
	global	_CCP2CON
_CCP2CON	set	29
	global	_CCPR1L
_CCPR1L	set	21
	global	_CCPR2L
_CCPR2L	set	27
	global	_PORTA
_PORTA	set	5
	global	_PORTD
_PORTD	set	8
	global	_RCREG
_RCREG	set	26
	global	_RCSTA
_RCSTA	set	24
	global	_T1CON
_T1CON	set	16
	global	_TMR1H
_TMR1H	set	15
	global	_TMR1L
_TMR1L	set	14
	global	_TXREG
_TXREG	set	25
	global	_CARRY
_CARRY	set	24
	global	_CCP1IF
_CCP1IF	set	98
	global	_CCP2IF
_CCP2IF	set	104
	global	_CREN
_CREN	set	196
	global	_GIE
_GIE	set	95
	global	_OERR
_OERR	set	193
	global	_PEIE
_PEIE	set	94
	global	_RC0
_RC0	set	56
	global	_RC3
_RC3	set	59
	global	_RC4
_RC4	set	60
	global	_RC5
_RC5	set	61
	global	_RCIF
_RCIF	set	101
	global	_RE0
_RE0	set	72
	global	_RE1
_RE1	set	73
	global	_TMR1IF
_TMR1IF	set	96
	global	_OPTION
_OPTION	set	129
	global	_OSCCON
_OSCCON	set	143
	global	_SPBRG
_SPBRG	set	153
	global	_TRISA
_TRISA	set	133
	global	_TRISB
_TRISB	set	134
	global	_TRISC
_TRISC	set	135
	global	_TRISD
_TRISD	set	136
	global	_TRISE
_TRISE	set	137
	global	_TXSTA
_TXSTA	set	152
	global	_CCP1IE
_CCP1IE	set	1122
	global	_CCP2IE
_CCP2IE	set	1128
	global	_HTS
_HTS	set	1146
	global	_TMR1IE
_TMR1IE	set	1120
	global	_TRMT
_TRMT	set	1217
	global	_EEADR
_EEADR	set	269
	global	_EEDATA
_EEDATA	set	268
	global	_WDTCON
_WDTCON	set	261
	global	_ANSEL
_ANSEL	set	392
	global	_ANSELH
_ANSELH	set	393
	global	_EECON1
_EECON1	set	396
	global	_EECON2
_EECON2	set	397
	global	_RD
_RD	set	3168
	global	_WR
_WR	set	3169
	global	_WREN
_WREN	set	3170
	file	"Hitch_Helper.as"
	line	#
psect cinit,class=CODE,delta=2
global start_initialization
start_initialization:

psect	bssCOMMON,class=COMMON,space=1
global __pbssCOMMON
__pbssCOMMON:
_timer_array:
       ds      2

_PulseWidth1Counter:
       ds      1

_PulseWidth2Counter:
       ds      1

_PulseWidth3Counter:
       ds      1

_capture_isr_flag_ccp1:
       ds      1

psect	bssBANK0,class=BANK0,space=1
global __pbssBANK0
__pbssBANK0:
main@EchoBackFallingEdge:
       ds      2

main@EchoBackRisingEdge:
       ds      2

_CaptureHi2:
       ds      1

_CaptureLo2:
       ds      1

_EepFlagsSetting:
       ds      1

_EepSen1HiVal:
       ds      1

_EepSen1HiValCopy:
       ds      1

_EepSen1LoVal:
       ds      1

_EepSen1LoValCopy:
       ds      1

_EepSen2HiVal:
       ds      1

_EepSen2HiValCopy:
       ds      1

_EepSen2LoVal:
       ds      1

_EepSen2LoValCopy:
       ds      1

_EepSen3HiVal:
       ds      1

_EepSen3HiValCopy:
       ds      1

_EepSen3LoVal:
       ds      1

_EepSen3LoValCopy:
       ds      1

_HH_PROCESS_STATE_MACHINE:
       ds      1

_HH_PROCESS_STATE_MACHINE_STATES:
       ds      1

_OneMillisecondTimerDisable:
       ds      1

_ProcessingCaptureModule:
       ds      1

_PulseWidth1CurrentCount:
       ds      1

_PulseWidth2CurrentCount:
       ds      1

_PulseWidth3CurrentCount:
       ds      1

_SERIAL_NUMBER_EEPROM_ADDRESS:
       ds      1

_Sensor1FineCount:
       ds      1

_Sensor1ResolutionAdjustedCount:
       ds      1

_Sensor2FineCount:
       ds      1

_Sensor2ResolutionAdjustedCount:
       ds      1

_Sensor3FineCount:
       ds      1

_Sensor3ResolutionAdjustedCount:
       ds      1

_capture_isr_flag_ccp2:
       ds      1

_tmr1_isr_counter:
       ds      1

_toms_variable:
       ds      1

capture_state1@CAPTURE_STATE:
       ds      1

capture_state2@CAPTURE_STATE:
       ds      1

capture_state3@CAPTURE_STATE:
       ds      1

main@OneMillisecondTimerDisable:
       ds      1

main@PulseStateCountInitCount:
       ds      1

_read_current_sn:
       ds      4

psect clrtext,class=CODE,delta=2
global clear_ram
;	Called with FSR containing the base address, and
;	W with the last address+1
clear_ram:
	clrwdt			;clear the watchdog before getting into this loop
clrloop:
	clrf	indf		;clear RAM location pointed to by FSR
	incf	fsr,f		;increment pointer
	xorwf	fsr,w		;XOR with final address
	btfsc	status,2	;have we reached the end yet?
	retlw	0		;all done for this memory range, return
	xorwf	fsr,w		;XOR again to restore value
	goto	clrloop		;do the next byte

; Clear objects allocated to COMMON
psect cinit,class=CODE,delta=2
	clrf	((__pbssCOMMON)+0)&07Fh
	clrf	((__pbssCOMMON)+1)&07Fh
	clrf	((__pbssCOMMON)+2)&07Fh
	clrf	((__pbssCOMMON)+3)&07Fh
	clrf	((__pbssCOMMON)+4)&07Fh
	clrf	((__pbssCOMMON)+5)&07Fh
; Clear objects allocated to BANK0
psect cinit,class=CODE,delta=2
	bcf	status, 7	;select IRP bank0
	movlw	low(__pbssBANK0)
	movwf	fsr
	movlw	low((__pbssBANK0)+02Dh)
	fcall	clear_ram
psect cinit,class=CODE,delta=2
global end_of_initialization

;End of C runtime variable initialization code

end_of_initialization:
clrf status
ljmp _main	;jump to C main() function
psect	cstackCOMMON,class=COMMON,space=1
global __pcstackCOMMON
__pcstackCOMMON:
	global	?_timer_isr
?_timer_isr:	; 0 bytes @ 0x0
	global	??_timer_isr
??_timer_isr:	; 0 bytes @ 0x0
	global	?_init_micro
?_init_micro:	; 0 bytes @ 0x0
	global	?_timer_init
?_timer_init:	; 0 bytes @ 0x0
	global	?_read_serial_ver
?_read_serial_ver:	; 0 bytes @ 0x0
	global	?_AreSensorsCaled
?_AreSensorsCaled:	; 0 bytes @ 0x0
	global	?_RecalSensors
?_RecalSensors:	; 0 bytes @ 0x0
	global	?_AreCalCopyCorrect
?_AreCalCopyCorrect:	; 0 bytes @ 0x0
	global	?_eepSens1WriteCal
?_eepSens1WriteCal:	; 0 bytes @ 0x0
	global	?_eepSens2WriteCal
?_eepSens2WriteCal:	; 0 bytes @ 0x0
	global	?_eepSens3WriteCal
?_eepSens3WriteCal:	; 0 bytes @ 0x0
	global	?_uart_init
?_uart_init:	; 0 bytes @ 0x0
	global	?_capture_state1
?_capture_state1:	; 0 bytes @ 0x0
	global	?_capture_state2
?_capture_state2:	; 0 bytes @ 0x0
	global	?_capture_state3
?_capture_state3:	; 0 bytes @ 0x0
	global	?_capture_isr
?_capture_isr:	; 0 bytes @ 0x0
	global	??_capture_isr
??_capture_isr:	; 0 bytes @ 0x0
	global	?_interrupt_handler
?_interrupt_handler:	; 0 bytes @ 0x0
	global	?_eeprom_read
?_eeprom_read:	; 1 bytes @ 0x0
	global	?_main
?_main:	; 2 bytes @ 0x0
	ds	1
	global	timer_isr@i
timer_isr@i:	; 1 bytes @ 0x1
	ds	1
	global	??_interrupt_handler
??_interrupt_handler:	; 0 bytes @ 0x2
	ds	4
psect	cstackBANK0,class=BANK0,space=1
global __pcstackBANK0
__pcstackBANK0:
	global	??_init_micro
??_init_micro:	; 0 bytes @ 0x0
	global	??_timer_init
??_timer_init:	; 0 bytes @ 0x0
	global	?_timer_set
?_timer_set:	; 0 bytes @ 0x0
	global	?_uart3bytes_tx
?_uart3bytes_tx:	; 0 bytes @ 0x0
	global	??_uart_init
??_uart_init:	; 0 bytes @ 0x0
	global	??_eeprom_read
??_eeprom_read:	; 0 bytes @ 0x0
	global	?_eeprom_write
?_eeprom_write:	; 0 bytes @ 0x0
	global	??_capture_state1
??_capture_state1:	; 0 bytes @ 0x0
	global	??_capture_state2
??_capture_state2:	; 0 bytes @ 0x0
	global	??_capture_state3
??_capture_state3:	; 0 bytes @ 0x0
	global	?_get_timer
?_get_timer:	; 2 bytes @ 0x0
	global	uart3bytes_tx@EepSen2HiValUart
uart3bytes_tx@EepSen2HiValUart:	; 1 bytes @ 0x0
	global	eeprom_write@value
eeprom_write@value:	; 1 bytes @ 0x0
	global	timer_set@value
timer_set@value:	; 2 bytes @ 0x0
	ds	1
	global	??_eeprom_write
??_eeprom_write:	; 0 bytes @ 0x1
	global	timer_init@i
timer_init@i:	; 1 bytes @ 0x1
	global	uart3bytes_tx@EepSen3HiValUart
uart3bytes_tx@EepSen3HiValUart:	; 1 bytes @ 0x1
	global	eeprom_read@addr
eeprom_read@addr:	; 1 bytes @ 0x1
	ds	1
	global	??_timer_set
??_timer_set:	; 0 bytes @ 0x2
	global	??_get_timer
??_get_timer:	; 0 bytes @ 0x2
	global	??_uart3bytes_tx
??_uart3bytes_tx:	; 0 bytes @ 0x2
	global	?_eeprom_read_block1
?_eeprom_read_block1:	; 0 bytes @ 0x2
	global	eeprom_read_block1@size
eeprom_read_block1@size:	; 1 bytes @ 0x2
	global	eeprom_write@addr
eeprom_write@addr:	; 1 bytes @ 0x2
	ds	1
	global	?_eeprom_write_block
?_eeprom_write_block:	; 0 bytes @ 0x3
	global	??_eepSens1WriteCal
??_eepSens1WriteCal:	; 0 bytes @ 0x3
	global	??_eepSens2WriteCal
??_eepSens2WriteCal:	; 0 bytes @ 0x3
	global	??_eepSens3WriteCal
??_eepSens3WriteCal:	; 0 bytes @ 0x3
	global	timer_set@index
timer_set@index:	; 1 bytes @ 0x3
	global	eeprom_write_block@size
eeprom_write_block@size:	; 1 bytes @ 0x3
	global	eeprom_read_block1@buffer
eeprom_read_block1@buffer:	; 1 bytes @ 0x3
	global	get_timer@result
get_timer@result:	; 2 bytes @ 0x3
	ds	1
	global	??_AreSensorsCaled
??_AreSensorsCaled:	; 0 bytes @ 0x4
	global	??_RecalSensors
??_RecalSensors:	; 0 bytes @ 0x4
	global	??_AreCalCopyCorrect
??_AreCalCopyCorrect:	; 0 bytes @ 0x4
	global	??_eeprom_read_block1
??_eeprom_read_block1:	; 0 bytes @ 0x4
	global	eeprom_write_block@buffer
eeprom_write_block@buffer:	; 1 bytes @ 0x4
	global	uart3bytes_tx@EepSen1HiValUart
uart3bytes_tx@EepSen1HiValUart:	; 1 bytes @ 0x4
	ds	1
	global	??_eeprom_write_block
??_eeprom_write_block:	; 0 bytes @ 0x5
	global	get_timer@index
get_timer@index:	; 1 bytes @ 0x5
	global	eeprom_read_block1@address
eeprom_read_block1@address:	; 1 bytes @ 0x5
	ds	1
	global	eeprom_write_block@i
eeprom_write_block@i:	; 1 bytes @ 0x6
	global	eeprom_read_block1@i
eeprom_read_block1@i:	; 1 bytes @ 0x6
	ds	1
	global	eeprom_write_block@address
eeprom_write_block@address:	; 1 bytes @ 0x7
	ds	1
	global	??_read_serial_ver
??_read_serial_ver:	; 0 bytes @ 0x8
	ds	2
	global	read_serial_ver@i
read_serial_ver@i:	; 1 bytes @ 0xA
	ds	1
	global	??_main
??_main:	; 0 bytes @ 0xB
	ds	3
;;Data sizes: Strings 0, constant 34, data 0, bss 51, persistent 0 stack 0
;;Auto spaces:   Size  Autos    Used
;; COMMON          14      6      12
;; BANK0           80     14      59
;; BANK1           80      0       0
;; BANK3           96      0       0
;; BANK2           96      0       0

;;
;; Pointer list with targets:

;; ?_get_timer	unsigned short  size(1) Largest target is 0
;;
;; eeprom_read_block1@buffer	PTR unsigned char  size(1) Largest target is 4
;;		 -> read_current_sn(BANK0[4]), 
;;
;; sp__eeprom_read_block	PTR unsigned char  size(1) Largest target is 16
;;		 -> eeprom_read_block@holding_buffer(BANK0[16]), 
;;
;; eeprom_write_block@buffer	PTR unsigned char  size(1) Largest target is 4
;;		 -> default_sn(CODE[4]), tom_sn1(CODE[4]), 
;;


;;
;; Critical Paths under _main in COMMON
;;
;;   None.
;;
;; Critical Paths under _interrupt_handler in COMMON
;;
;;   _interrupt_handler->_timer_isr
;;
;; Critical Paths under _main in BANK0
;;
;;   _main->_read_serial_ver
;;   _read_serial_ver->_eeprom_write_block
;;   _eeprom_read_block1->_eeprom_read
;;   _eepSens3WriteCal->_eeprom_write
;;   _eepSens2WriteCal->_eeprom_write
;;   _eepSens1WriteCal->_eeprom_write
;;   _AreCalCopyCorrect->_timer_set
;;   _RecalSensors->_timer_set
;;   _AreSensorsCaled->_timer_set
;;   _eeprom_write_block->_eeprom_write
;;
;; Critical Paths under _interrupt_handler in BANK0
;;
;;   None.
;;
;; Critical Paths under _main in BANK1
;;
;;   None.
;;
;; Critical Paths under _interrupt_handler in BANK1
;;
;;   None.
;;
;; Critical Paths under _main in BANK3
;;
;;   None.
;;
;; Critical Paths under _interrupt_handler in BANK3
;;
;;   None.
;;
;; Critical Paths under _main in BANK2
;;
;;   None.
;;
;; Critical Paths under _interrupt_handler in BANK2
;;
;;   None.

;;
;;Main: autosize = 0, tempsize = 3, incstack = 0, save=0
;;

;;
;;Call Graph Tables:
;;
;; ---------------------------------------------------------------------------------
;; (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;; ---------------------------------------------------------------------------------
;; (0) _main                                                 3     3      0    1945
;;                                             11 BANK0      3     3      0
;;                         _init_micro
;;                         _timer_init
;;                          _timer_set
;;                 _eeprom_write_block
;;                          _get_timer
;;                    _read_serial_ver
;;                    _AreSensorsCaled
;;                       _RecalSensors
;;                  _AreCalCopyCorrect
;;                     _capture_state1
;;                   _eepSens1WriteCal
;;                     _capture_state2
;;                   _eepSens2WriteCal
;;                     _capture_state3
;;                   _eepSens3WriteCal
;;                      _uart3bytes_tx
;; ---------------------------------------------------------------------------------
;; (1) _read_serial_ver                                      3     3      0     774
;;                                              8 BANK0      3     3      0
;;                 _eeprom_read_block1
;;                 _eeprom_write_block
;; ---------------------------------------------------------------------------------
;; (2) _eeprom_read_block1                                   5     3      2     198
;;                                              2 BANK0      5     3      2
;;                        _eeprom_read
;; ---------------------------------------------------------------------------------
;; (1) _uart3bytes_tx                                        5     3      2      93
;;                                              0 BANK0      5     3      2
;; ---------------------------------------------------------------------------------
;; (1) _eepSens3WriteCal                                     2     2      0      62
;;                                              3 BANK0      2     2      0
;;                       _eeprom_write
;; ---------------------------------------------------------------------------------
;; (1) _eepSens2WriteCal                                     2     2      0      62
;;                                              3 BANK0      2     2      0
;;                       _eeprom_write
;; ---------------------------------------------------------------------------------
;; (1) _eepSens1WriteCal                                     2     2      0      62
;;                                              3 BANK0      2     2      0
;;                       _eeprom_write
;; ---------------------------------------------------------------------------------
;; (1) _AreCalCopyCorrect                                    1     1      0     124
;;                                              4 BANK0      1     1      0
;;                        _eeprom_read
;;                          _timer_set
;; ---------------------------------------------------------------------------------
;; (1) _RecalSensors                                         1     1      0      93
;;                                              4 BANK0      1     1      0
;;                          _timer_set
;; ---------------------------------------------------------------------------------
;; (1) _AreSensorsCaled                                      1     1      0     124
;;                                              4 BANK0      1     1      0
;;                        _eeprom_read
;;                          _timer_set
;; ---------------------------------------------------------------------------------
;; (2) _eeprom_write_block                                   5     3      2     260
;;                                              3 BANK0      5     3      2
;;                       _eeprom_write
;; ---------------------------------------------------------------------------------
;; (1) _init_micro                                           0     0      0       0
;;                          _uart_init
;; ---------------------------------------------------------------------------------
;; (2) _eeprom_write                                         3     2      1      62
;;                                              0 BANK0      3     2      1
;; ---------------------------------------------------------------------------------
;; (2) _eeprom_read                                          2     2      0      31
;;                                              0 BANK0      2     2      0
;; ---------------------------------------------------------------------------------
;; (2) _uart_init                                            0     0      0       0
;; ---------------------------------------------------------------------------------
;; (1) _get_timer                                            6     4      2      99
;;                                              0 BANK0      6     4      2
;; ---------------------------------------------------------------------------------
;; (2) _timer_set                                            6     4      2      93
;;                                              0 BANK0      4     2      2
;; ---------------------------------------------------------------------------------
;; (1) _timer_init                                           2     2      0      99
;;                                              0 BANK0      2     2      0
;; ---------------------------------------------------------------------------------
;; (1) _capture_state3                                       1     1      0       0
;;                                              0 BANK0      1     1      0
;; ---------------------------------------------------------------------------------
;; (1) _capture_state2                                       1     1      0       0
;;                                              0 BANK0      1     1      0
;; ---------------------------------------------------------------------------------
;; (1) _capture_state1                                       1     1      0       0
;;                                              0 BANK0      1     1      0
;; ---------------------------------------------------------------------------------
;; Estimated maximum stack depth 2
;; ---------------------------------------------------------------------------------
;; (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;; ---------------------------------------------------------------------------------
;; (4) _interrupt_handler                                    4     4      0      90
;;                                              2 COMMON     4     4      0
;;                          _timer_isr
;;                        _capture_isr
;; ---------------------------------------------------------------------------------
;; (5) _timer_isr                                            2     2      0      90
;;                                              0 COMMON     2     2      0
;; ---------------------------------------------------------------------------------
;; (5) _capture_isr                                          0     0      0       0
;; ---------------------------------------------------------------------------------
;; Estimated maximum stack depth 5
;; ---------------------------------------------------------------------------------

;; Call Graph Graphs:

;; _main (ROOT)
;;   _init_micro
;;     _uart_init
;;   _timer_init
;;   _timer_set
;;   _eeprom_write_block
;;     _eeprom_write
;;   _get_timer
;;   _read_serial_ver
;;     _eeprom_read_block1
;;       _eeprom_read
;;     _eeprom_write_block
;;       _eeprom_write
;;   _AreSensorsCaled
;;     _eeprom_read
;;     _timer_set
;;   _RecalSensors
;;     _timer_set
;;   _AreCalCopyCorrect
;;     _eeprom_read
;;     _timer_set
;;   _capture_state1
;;   _eepSens1WriteCal
;;     _eeprom_write
;;   _capture_state2
;;   _eepSens2WriteCal
;;     _eeprom_write
;;   _capture_state3
;;   _eepSens3WriteCal
;;     _eeprom_write
;;   _uart3bytes_tx
;;
;; _interrupt_handler (ROOT)
;;   _timer_isr
;;   _capture_isr
;;

;; Address spaces:

;;Name               Size   Autos  Total    Cost      Usage
;;BITCOMMON            E      0       0       0        0.0%
;;EEDATA             100      0       0       0        0.0%
;;NULL                 0      0       0       0        0.0%
;;CODE                 0      0       0       0        0.0%
;;COMMON               E      6       C       1       85.7%
;;BITSFR0              0      0       0       1        0.0%
;;SFR0                 0      0       0       1        0.0%
;;BITSFR1              0      0       0       2        0.0%
;;SFR1                 0      0       0       2        0.0%
;;STACK                0      0       7       2        0.0%
;;ABS                  0      0      47       3        0.0%
;;BITBANK0            50      0       0       4        0.0%
;;BITSFR3              0      0       0       4        0.0%
;;SFR3                 0      0       0       4        0.0%
;;BANK0               50      E      3B       5       73.8%
;;BITSFR2              0      0       0       5        0.0%
;;SFR2                 0      0       0       5        0.0%
;;BITBANK1            50      0       0       6        0.0%
;;BANK1               50      0       0       7        0.0%
;;BITBANK3            60      0       0       8        0.0%
;;BANK3               60      0       0       9        0.0%
;;BITBANK2            60      0       0      10        0.0%
;;BANK2               60      0       0      11        0.0%
;;DATA                 0      0      4E      12        0.0%

	global	_main
psect	maintext,global,class=CODE,delta=2
global __pmaintext
__pmaintext:

;; *************** function _main *****************
;; Defined at:
;;		line 248 in file "F:\pic_projects\Hitch_Helper\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;                  2  2709[COMMON] int 
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 17F/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       3       0       0       0
;;      Totals:         0       3       0       0       0
;;Total ram usage:        3 bytes
;; Hardware stack levels required when called:    5
;; This function calls:
;;		_init_micro
;;		_timer_init
;;		_timer_set
;;		_eeprom_write_block
;;		_get_timer
;;		_read_serial_ver
;;		_AreSensorsCaled
;;		_RecalSensors
;;		_AreCalCopyCorrect
;;		_capture_state1
;;		_eepSens1WriteCal
;;		_capture_state2
;;		_eepSens2WriteCal
;;		_capture_state3
;;		_eepSens3WriteCal
;;		_uart3bytes_tx
;; This function is called by:
;;		Startup code after reset
;; This function uses a non-reentrant model
;;
psect	maintext
	file	"F:\pic_projects\Hitch_Helper\main.c"
	line	248
	global	__size_of_main
	__size_of_main	equ	__end_of_main-_main
	
_main:	
	opt	stack 3
; Regs used in _main: [wreg-fsr0h+status,2+status,0+pclath+cstack]
	line	274
	
l8450:	
;main.c: 253: static uint8_t PulseStateCountInitCount;
;main.c: 254: static uint8_t OneMillisecondTimerDisable;
;main.c: 256: static uint16_t EchoBackRisingEdge;
;main.c: 257: static uint16_t EchoBackFallingEdge;
;main.c: 274: init_micro();
	fcall	_init_micro
	line	275
	
l8452:	
;main.c: 275: TMR1IE = 1;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bsf	(1120/8)^080h,(1120)&7
	line	276
	
l8454:	
;main.c: 276: timer_init();
	fcall	_timer_init
	line	277
	
l8456:	
;main.c: 277: CCP1IE = 1;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bsf	(1122/8)^080h,(1122)&7
	line	278
	
l8458:	
;main.c: 278: CCP2IE = 1;
	bsf	(1128/8)^080h,(1128)&7
	line	279
	
l8460:	
;main.c: 279: PEIE = 1;
	bsf	(94/8),(94)&7
	line	280
	
l8462:	
;main.c: 280: (GIE = 1);
	bsf	(95/8),(95)&7
	line	281
	
l8464:	
;main.c: 281: timer_set(PULSE_OUT_TIMER,(50));
	movlw	low(032h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(?_timer_set)
	movlw	high(032h)
	movwf	((?_timer_set))+1
	movlw	(0)
	fcall	_timer_set
	line	282
	
l8466:	
;main.c: 282: HH_PROCESS_STATE_MACHINE = INIT;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(_HH_PROCESS_STATE_MACHINE)
	line	283
	
l8468:	
;main.c: 283: OneMillisecondTimerDisable = FALSE;
	clrf	(main@OneMillisecondTimerDisable)
	line	284
	
l8470:	
;main.c: 284: eeprom_write_block(EepSerialNumbyte0,3,&tom_sn1[0]);
	movlw	(03h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(?_eeprom_write_block)
	movlw	((_tom_sn1-__stringbase))&0ffh
	movwf	(??_main+1)+0
	movf	(??_main+1)+0,w
	movwf	(0+?_eeprom_write_block+01h)
	movlw	(0FCh)
	fcall	_eeprom_write_block
	line	288
	
l8472:	
;main.c: 288: EepFlagsSetting.ClearAllEepBits = FALSE;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(_EepFlagsSetting)
	line	292
;main.c: 292: while (HTS == 0)
	goto	l2718
	
l2719:	
	line	293
;main.c: 293: {}
	
l2718:	
	line	292
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	btfss	(1146/8)^080h,(1146)&7
	goto	u4751
	goto	u4750
u4751:
	goto	l2718
u4750:
	goto	l8746
	
l2720:	
	goto	l8746
	line	294
;main.c: 294: while(1)
	
l2721:	
	line	296
;main.c: 295: {
;main.c: 296: switch(HH_PROCESS_STATE_MACHINE)
	goto	l8746
	line	298
;main.c: 297: {
;main.c: 298: case INIT:
	
l2723:	
	line	299
	
l8474:	
;main.c: 299: if(get_timer(PULSE_OUT_TIMER) == 0)
	movlw	(0)
	fcall	_get_timer
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	((1+(?_get_timer))),w
	iorwf	((0+(?_get_timer))),w
	skipz
	goto	u4761
	goto	u4760
u4761:
	goto	l2724
u4760:
	line	301
	
l8476:	
# 301 "F:\pic_projects\Hitch_Helper\main.c"
clrwdt ;#
psect	maintext
	line	302
;main.c: 302: (RE0) = FALSE;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	bcf	(72/8),(72)&7
	line	303
	
l8478:	
;main.c: 303: read_serial_ver();
	fcall	_read_serial_ver
	line	304
;main.c: 304: timer_set(PULSE_OUT_TIMER,(200));
	movlw	low(0C8h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(?_timer_set)
	movlw	high(0C8h)
	movwf	((?_timer_set))+1
	movlw	(0)
	fcall	_timer_set
	line	305
	
l8480:	
;main.c: 305: HH_PROCESS_STATE_MACHINE = INIT;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(_HH_PROCESS_STATE_MACHINE)
	line	306
;main.c: 306: AreSensorsCaled();
	fcall	_AreSensorsCaled
	line	307
	
l8482:	
;main.c: 307: PulseStateCountInitCount++;
	movlw	(01h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	addwf	(main@PulseStateCountInitCount),f
	line	308
	
l8484:	
;main.c: 308: RecalSensors();
	fcall	_RecalSensors
	line	309
	
l8486:	
;main.c: 309: AreCalCopyCorrect();
	fcall	_AreCalCopyCorrect
	line	310
	
l8488:	
;main.c: 310: if(EepFlagsSetting.EepFlags.EepSensor1CalFlag == TRUE)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfss	(_EepFlagsSetting),0
	goto	u4771
	goto	u4770
u4771:
	goto	l8498
u4770:
	line	314
	
l8490:	
;main.c: 313: {
;main.c: 314: PulseStateCountInitCount = 20;
	movlw	(014h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(main@PulseStateCountInitCount)
	line	315
	
l8492:	
;main.c: 315: HH_PROCESS_STATE_MACHINE = PULSE_OUT_CCP2_SENSOR1;
	clrf	(_HH_PROCESS_STATE_MACHINE)
	bsf	status,0
	rlf	(_HH_PROCESS_STATE_MACHINE),f
	line	316
	
l8494:	
;main.c: 316: timer_set(PULSE_OUT_TIMER,(50));
	movlw	low(032h)
	movwf	(?_timer_set)
	movlw	high(032h)
	movwf	((?_timer_set))+1
	movlw	(0)
	fcall	_timer_set
	line	317
	
l8496:	
;main.c: 317: EepFlagsSetting.EepFlags.EepSensor1CalFlag = FALSE;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	bcf	(_EepFlagsSetting),0
	line	318
;main.c: 318: }
	goto	l8746
	line	319
	
l2725:	
	line	321
	
l8498:	
;main.c: 319: else
;main.c: 320: {
;main.c: 321: RE1 ^= TRUE;
	movlw	1<<((73)&7)
	xorwf	((73)/8),f
	line	322
;main.c: 322: if(PulseStateCountInitCount >=20)
	movlw	(014h)
	subwf	(main@PulseStateCountInitCount),w
	skipc
	goto	u4781
	goto	u4780
u4781:
	goto	l8746
u4780:
	line	325
	
l8500:	
;main.c: 323: {
;main.c: 325: RE1 = FALSE;
	bcf	(73/8),(73)&7
	line	326
	
l8502:	
;main.c: 326: HH_PROCESS_STATE_MACHINE = PULSE_OUT_CCP2_SENSOR1;
	clrf	(_HH_PROCESS_STATE_MACHINE)
	bsf	status,0
	rlf	(_HH_PROCESS_STATE_MACHINE),f
	line	327
	
l8504:	
;main.c: 327: timer_set(PULSE_OUT_TIMER,(50));
	movlw	low(032h)
	movwf	(?_timer_set)
	movlw	high(032h)
	movwf	((?_timer_set))+1
	movlw	(0)
	fcall	_timer_set
	line	328
	
l8506:	
;main.c: 328: EepFlagsSetting.EepFlags.EepSensor1CalFlag = FALSE;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	bcf	(_EepFlagsSetting),0
	goto	l8746
	line	329
	
l2727:	
	goto	l8746
	line	330
	
l2726:	
	line	331
;main.c: 329: }
;main.c: 330: }
;main.c: 331: }
	goto	l8746
	line	332
	
l2724:	
	line	334
# 334 "F:\pic_projects\Hitch_Helper\main.c"
nop ;#
psect	maintext
	goto	l8746
	line	335
	
l2728:	
	line	336
;main.c: 335: }
;main.c: 336: break;
	goto	l8746
	line	338
;main.c: 338: case PULSE_OUT_CCP2_SENSOR1:
	
l2730:	
	line	339
	
l8508:	
;main.c: 339: if(get_timer(PULSE_OUT_TIMER) == 0)
	movlw	(0)
	fcall	_get_timer
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	((1+(?_get_timer))),w
	iorwf	((0+(?_get_timer))),w
	skipz
	goto	u4791
	goto	u4790
u4791:
	goto	l8552
u4790:
	line	341
	
l8510:	
;main.c: 340: {
;main.c: 341: OneMillisecondTimerDisable = TRUE;
	clrf	(main@OneMillisecondTimerDisable)
	bsf	status,0
	rlf	(main@OneMillisecondTimerDisable),f
	line	342
	
l8512:	
;main.c: 342: CCP2CON = RISING_EDGE_CCP2;
	movlw	(05h)
	movwf	(29)	;volatile
	line	344
	
l8514:	
;main.c: 344: (GIE = 0);
	bcf	(95/8),(95)&7
	line	345
	
l8516:	
;main.c: 345: RC0 = TRUE;
	bsf	(56/8),(56)&7
	line	347
	
l8518:	
# 347 "F:\pic_projects\Hitch_Helper\main.c"
nop ;#
psect	maintext
	line	348
	
l8520:	
# 348 "F:\pic_projects\Hitch_Helper\main.c"
nop ;#
psect	maintext
	line	349
	
l8522:	
# 349 "F:\pic_projects\Hitch_Helper\main.c"
nop ;#
psect	maintext
	line	350
	
l8524:	
# 350 "F:\pic_projects\Hitch_Helper\main.c"
nop ;#
psect	maintext
	line	351
	
l8526:	
# 351 "F:\pic_projects\Hitch_Helper\main.c"
nop ;#
psect	maintext
	line	352
	
l8528:	
;main.c: 352: RC0 = FALSE;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	bcf	(56/8),(56)&7
	line	354
	
l8530:	
;main.c: 354: (GIE = 1);
	bsf	(95/8),(95)&7
	line	355
	
l8532:	
# 355 "F:\pic_projects\Hitch_Helper\main.c"
nop ;#
psect	maintext
	line	356
	
l8534:	
;main.c: 356: ProcessingCaptureModule = FALSE;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(_ProcessingCaptureModule)
	line	357
;main.c: 357: while(ProcessingCaptureModule == FALSE)
	goto	l8538
	
l2733:	
	line	359
	
l8536:	
;main.c: 358: {
;main.c: 359: capture_state1();
	fcall	_capture_state1
	goto	l8538
	line	360
	
l2732:	
	line	357
	
l8538:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(_ProcessingCaptureModule),w
	skipz
	goto	u4800
	goto	l8536
u4800:
	
l2734:	
	line	361
;main.c: 360: };
;main.c: 361: OneMillisecondTimerDisable = FALSE;
	clrf	(main@OneMillisecondTimerDisable)
	line	364
	
l8540:	
;main.c: 364: timer_set(PULSE_OUT_TIMER,(50));
	movlw	low(032h)
	movwf	(?_timer_set)
	movlw	high(032h)
	movwf	((?_timer_set))+1
	movlw	(0)
	fcall	_timer_set
	line	365
	
l8542:	
;main.c: 365: if(EepFlagsSetting.EepFlags.EepSensor1CalFlag == TRUE)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfss	(_EepFlagsSetting),0
	goto	u4811
	goto	u4810
u4811:
	goto	l2735
u4810:
	line	367
	
l8544:	
;main.c: 366: {
;main.c: 367: Sensor1FineCount = CCPR2L;
	movf	(27),w	;volatile
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(_Sensor1FineCount)
	line	368
	
l8546:	
;main.c: 368: eepSens1WriteCal();
	fcall	_eepSens1WriteCal
	line	371
	
l8548:	
;main.c: 371: HH_PROCESS_STATE_MACHINE = INIT;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(_HH_PROCESS_STATE_MACHINE)
	line	372
;main.c: 372: }
	goto	l8552
	line	373
	
l2735:	
	line	375
;main.c: 373: else
;main.c: 374: {
;main.c: 375: (RC5) = (0);
	bcf	(61/8),(61)&7
	line	376
	
l8550:	
;main.c: 376: HH_PROCESS_STATE_MACHINE = PULSE_OUT_CCP1_SENSOR2;
	movlw	(02h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(_HH_PROCESS_STATE_MACHINE)
	goto	l8552
	line	377
	
l2736:	
	line	378
;main.c: 377: }
;main.c: 378: }else
	goto	l8552
	
l2731:	
	goto	l8552
	line	381
;main.c: 379: {
	
l2737:	
	line	382
	
l8552:	
;main.c: 381: }
;main.c: 382: Sensor1FineCount = CCPR2L;
	movf	(27),w	;volatile
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(_Sensor1FineCount)
	line	383
	
l8554:	
;main.c: 383: Sensor1ResolutionAdjustedCount = (Sensor1FineCount >> 4);
	movf	(_Sensor1FineCount),w
	movwf	(??_main+0)+0
	movlw	04h
u4825:
	clrc
	rrf	(??_main+0)+0,f
	addlw	-1
	skipz
	goto	u4825
	movf	0+(??_main+0)+0,w
	movwf	(??_main+1)+0
	movf	(??_main+1)+0,w
	movwf	(_Sensor1ResolutionAdjustedCount)
	line	384
;main.c: 384: break;
	goto	l8746
	line	386
;main.c: 386: case PULSE_OUT_CCP1_SENSOR2:
	
l2738:	
	line	387
	
l8556:	
;main.c: 387: if(get_timer(PULSE_OUT_TIMER) == 0)
	movlw	(0)
	fcall	_get_timer
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	((1+(?_get_timer))),w
	iorwf	((0+(?_get_timer))),w
	skipz
	goto	u4831
	goto	u4830
u4831:
	goto	l8602
u4830:
	line	389
	
l8558:	
;main.c: 388: {
;main.c: 389: OneMillisecondTimerDisable = TRUE;
	clrf	(main@OneMillisecondTimerDisable)
	bsf	status,0
	rlf	(main@OneMillisecondTimerDisable),f
	line	390
	
l8560:	
;main.c: 390: CCP1CON = RISING_EDGE_CCP1;
	movlw	(05h)
	movwf	(23)	;volatile
	line	393
	
l8562:	
;main.c: 393: (GIE = 0);
	bcf	(95/8),(95)&7
	line	394
	
l8564:	
;main.c: 394: RC3 = TRUE;
	bsf	(59/8),(59)&7
	line	395
	
l8566:	
# 395 "F:\pic_projects\Hitch_Helper\main.c"
nop ;#
psect	maintext
	line	396
	
l8568:	
# 396 "F:\pic_projects\Hitch_Helper\main.c"
nop ;#
psect	maintext
	line	397
	
l8570:	
# 397 "F:\pic_projects\Hitch_Helper\main.c"
nop ;#
psect	maintext
	line	398
	
l8572:	
# 398 "F:\pic_projects\Hitch_Helper\main.c"
nop ;#
psect	maintext
	line	399
	
l8574:	
# 399 "F:\pic_projects\Hitch_Helper\main.c"
nop ;#
psect	maintext
	line	400
	
l8576:	
;main.c: 400: RC3 = FALSE;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	bcf	(59/8),(59)&7
	line	401
	
l8578:	
;main.c: 401: (GIE = 1);
	bsf	(95/8),(95)&7
	line	403
	
l8580:	
# 403 "F:\pic_projects\Hitch_Helper\main.c"
nop ;#
psect	maintext
	line	404
	
l8582:	
;main.c: 404: ProcessingCaptureModule = FALSE;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(_ProcessingCaptureModule)
	line	405
;main.c: 405: while(ProcessingCaptureModule == FALSE)
	goto	l8586
	
l2741:	
	line	407
	
l8584:	
;main.c: 406: {
;main.c: 407: capture_state2();
	fcall	_capture_state2
	goto	l8586
	line	408
	
l2740:	
	line	405
	
l8586:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(_ProcessingCaptureModule),w
	skipz
	goto	u4840
	goto	l8584
u4840:
	
l2742:	
	line	409
;main.c: 408: };
;main.c: 409: OneMillisecondTimerDisable = FALSE;
	clrf	(main@OneMillisecondTimerDisable)
	line	412
	
l8588:	
;main.c: 412: timer_set(PULSE_OUT_TIMER,(50));
	movlw	low(032h)
	movwf	(?_timer_set)
	movlw	high(032h)
	movwf	((?_timer_set))+1
	movlw	(0)
	fcall	_timer_set
	line	413
	
l8590:	
;main.c: 413: if(EepFlagsSetting.EepFlags.EepSensor2CalFlag == TRUE)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfss	(_EepFlagsSetting),1
	goto	u4851
	goto	u4850
u4851:
	goto	l2743
u4850:
	line	415
	
l8592:	
;main.c: 414: {
;main.c: 415: eepSens2WriteCal();
	fcall	_eepSens2WriteCal
	line	416
	
l8594:	
;main.c: 416: (RC5) = (1);
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	bsf	(61/8),(61)&7
	line	417
	
l8596:	
;main.c: 417: HH_PROCESS_STATE_MACHINE = PULSE_OUT_CCP1_SENSOR3;
	movlw	(03h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(_HH_PROCESS_STATE_MACHINE)
	line	418
;main.c: 418: }
	goto	l8602
	line	419
	
l2743:	
	line	421
;main.c: 419: else
;main.c: 420: {
;main.c: 421: (RC5) = (1);
	bsf	(61/8),(61)&7
	line	422
	
l8598:	
;main.c: 422: HH_PROCESS_STATE_MACHINE = PULSE_OUT_CCP1_SENSOR3;
	movlw	(03h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(_HH_PROCESS_STATE_MACHINE)
	line	423
	
l8600:	
# 423 "F:\pic_projects\Hitch_Helper\main.c"
clrwdt ;#
psect	maintext
	goto	l8602
	line	424
	
l2744:	
	line	426
;main.c: 424: }
;main.c: 426: }else
	goto	l8602
	
l2739:	
	goto	l8602
	line	429
;main.c: 427: {
	
l2745:	
	line	430
	
l8602:	
;main.c: 429: }
;main.c: 430: Sensor2FineCount = CCPR1L;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(21),w	;volatile
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(_Sensor2FineCount)
	line	431
	
l8604:	
;main.c: 431: Sensor2ResolutionAdjustedCount = (Sensor2FineCount >> 4);
	movf	(_Sensor2FineCount),w
	movwf	(??_main+0)+0
	movlw	04h
u4865:
	clrc
	rrf	(??_main+0)+0,f
	addlw	-1
	skipz
	goto	u4865
	movf	0+(??_main+0)+0,w
	movwf	(??_main+1)+0
	movf	(??_main+1)+0,w
	movwf	(_Sensor2ResolutionAdjustedCount)
	line	432
;main.c: 432: break;
	goto	l8746
	line	435
;main.c: 435: case PULSE_OUT_CCP1_SENSOR3:
	
l2746:	
	line	436
	
l8606:	
;main.c: 436: if(get_timer(PULSE_OUT_TIMER) == 0)
	movlw	(0)
	fcall	_get_timer
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	((1+(?_get_timer))),w
	iorwf	((0+(?_get_timer))),w
	skipz
	goto	u4871
	goto	u4870
u4871:
	goto	l8648
u4870:
	line	438
	
l8608:	
;main.c: 437: {
;main.c: 438: OneMillisecondTimerDisable = TRUE;
	clrf	(main@OneMillisecondTimerDisable)
	bsf	status,0
	rlf	(main@OneMillisecondTimerDisable),f
	line	439
	
l8610:	
;main.c: 439: CCP1CON = RISING_EDGE_CCP1;
	movlw	(05h)
	movwf	(23)	;volatile
	line	442
	
l8612:	
;main.c: 442: (GIE = 0);
	bcf	(95/8),(95)&7
	line	443
	
l8614:	
;main.c: 443: RC4 = TRUE;
	bsf	(60/8),(60)&7
	line	445
	
l8616:	
# 445 "F:\pic_projects\Hitch_Helper\main.c"
nop ;#
psect	maintext
	line	446
	
l8618:	
# 446 "F:\pic_projects\Hitch_Helper\main.c"
nop ;#
psect	maintext
	line	447
	
l8620:	
# 447 "F:\pic_projects\Hitch_Helper\main.c"
nop ;#
psect	maintext
	line	448
	
l8622:	
# 448 "F:\pic_projects\Hitch_Helper\main.c"
nop ;#
psect	maintext
	line	449
	
l8624:	
# 449 "F:\pic_projects\Hitch_Helper\main.c"
nop ;#
psect	maintext
	line	450
	
l8626:	
;main.c: 450: RC4 = FALSE;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	bcf	(60/8),(60)&7
	line	452
	
l8628:	
;main.c: 452: (GIE = 1);
	bsf	(95/8),(95)&7
	line	453
	
l8630:	
# 453 "F:\pic_projects\Hitch_Helper\main.c"
nop ;#
psect	maintext
	line	454
	
l8632:	
;main.c: 454: ProcessingCaptureModule = FALSE;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(_ProcessingCaptureModule)
	line	455
;main.c: 455: while(ProcessingCaptureModule == FALSE)
	goto	l8636
	
l2749:	
	line	457
	
l8634:	
;main.c: 456: {
;main.c: 457: capture_state3();
	fcall	_capture_state3
	goto	l8636
	line	458
	
l2748:	
	line	455
	
l8636:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(_ProcessingCaptureModule),w
	skipz
	goto	u4880
	goto	l8634
u4880:
	
l2750:	
	line	459
;main.c: 458: };
;main.c: 459: OneMillisecondTimerDisable = FALSE;
	clrf	(main@OneMillisecondTimerDisable)
	line	462
	
l8638:	
;main.c: 462: timer_set(PULSE_OUT_TIMER,(50));
	movlw	low(032h)
	movwf	(?_timer_set)
	movlw	high(032h)
	movwf	((?_timer_set))+1
	movlw	(0)
	fcall	_timer_set
	line	463
	
l8640:	
;main.c: 463: if(EepFlagsSetting.EepFlags.EepSensor3CalFlag == TRUE)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfss	(_EepFlagsSetting),2
	goto	u4891
	goto	u4890
u4891:
	goto	l8646
u4890:
	line	465
	
l8642:	
;main.c: 464: {
;main.c: 465: eepSens3WriteCal();
	fcall	_eepSens3WriteCal
	line	466
	
l8644:	
;main.c: 466: HH_PROCESS_STATE_MACHINE = INIT;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(_HH_PROCESS_STATE_MACHINE)
	line	467
;main.c: 467: }
	goto	l8648
	line	468
	
l2751:	
	line	471
	
l8646:	
;main.c: 468: else
;main.c: 469: {
;main.c: 471: HH_PROCESS_STATE_MACHINE = UART;
	movlw	(04h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(_HH_PROCESS_STATE_MACHINE)
	goto	l8648
	line	472
	
l2752:	
	line	474
;main.c: 472: }
;main.c: 474: }else
	goto	l8648
	
l2747:	
	goto	l8648
	line	477
;main.c: 475: {
	
l2753:	
	line	478
	
l8648:	
;main.c: 477: }
;main.c: 478: Sensor3FineCount = CCPR1L;
	movf	(21),w	;volatile
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(_Sensor3FineCount)
	line	479
	
l8650:	
;main.c: 479: Sensor3ResolutionAdjustedCount = (Sensor3FineCount >> 4);
	movf	(_Sensor3FineCount),w
	movwf	(??_main+0)+0
	movlw	04h
u4905:
	clrc
	rrf	(??_main+0)+0,f
	addlw	-1
	skipz
	goto	u4905
	movf	0+(??_main+0)+0,w
	movwf	(??_main+1)+0
	movf	(??_main+1)+0,w
	movwf	(_Sensor3ResolutionAdjustedCount)
	line	480
;main.c: 480: break;
	goto	l8746
	line	481
;main.c: 481: case UART:
	
l2754:	
	line	490
	
l8652:	
;main.c: 490: if((EepSen3HiVal <= PulseWidth3CurrentCount) && (EepSen3LoVal <= (Sensor3FineCount >> 4)))
	movf	(_EepSen3HiVal),w
	subwf	(_PulseWidth3CurrentCount),w
	skipc
	goto	u4911
	goto	u4910
u4911:
	goto	l8658
u4910:
	
l8654:	
	movf	(_Sensor3FineCount),w
	movwf	(??_main+0)+0
	movlw	04h
u4925:
	clrc
	rrf	(??_main+0)+0,f
	addlw	-1
	skipz
	goto	u4925
	movf	(_EepSen3LoVal),w
	subwf	0+(??_main+0)+0,w
	skipc
	goto	u4931
	goto	u4930
u4931:
	goto	l8658
u4930:
	line	492
	
l8656:	
;main.c: 491: {
;main.c: 492: EepFlagsSetting.EepFlags.EepSensor3CalAchieved = TRUE;
	bsf	(_EepFlagsSetting),5
	line	495
;main.c: 495: }
	goto	l8672
	line	496
	
l2755:	
	
l8658:	
;main.c: 496: else if(PulseWidth3CurrentCount == 0)
	movf	(_PulseWidth3CurrentCount),f
	skipz
	goto	u4941
	goto	u4940
u4941:
	goto	l8672
u4940:
	line	498
	
l8660:	
;main.c: 497: {
;main.c: 498: EepFlagsSetting.EepFlags.EepSensor3CalAchieved = FALSE;
	bcf	(_EepFlagsSetting),5
	line	499
	
l8662:	
;main.c: 499: if((Sensor3FineCount >> 4) <= 15)
	movf	(_Sensor3FineCount),w
	movwf	(??_main+0)+0
	movlw	04h
u4955:
	clrc
	rrf	(??_main+0)+0,f
	addlw	-1
	skipz
	goto	u4955
	movlw	(010h)
	subwf	0+(??_main+0)+0,w
	skipnc
	goto	u4961
	goto	u4960
u4961:
	goto	l2758
u4960:
	line	501
	
l8664:	
;main.c: 500: {
;main.c: 501: PORTD &= 0xF0;
	movlw	(0F0h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	andwf	(8),f	;volatile
	line	502
;main.c: 502: PORTD ^= 0x0F;
	movlw	(0Fh)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	xorwf	(8),f	;volatile
	line	503
	
l2758:	
	line	504
;main.c: 503: }
;main.c: 504: if((Sensor3FineCount >> 4) <= 9)
	movf	(_Sensor3FineCount),w
	movwf	(??_main+0)+0
	movlw	04h
u4975:
	clrc
	rrf	(??_main+0)+0,f
	addlw	-1
	skipz
	goto	u4975
	movlw	(0Ah)
	subwf	0+(??_main+0)+0,w
	skipnc
	goto	u4981
	goto	u4980
u4981:
	goto	l2759
u4980:
	line	506
	
l8666:	
;main.c: 505: {
;main.c: 506: PORTD &= 0xF0;
	movlw	(0F0h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	andwf	(8),f	;volatile
	line	507
;main.c: 507: PORTD ^= 0x07;
	movlw	(07h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	xorwf	(8),f	;volatile
	line	508
	
l2759:	
	line	509
;main.c: 508: }
;main.c: 509: if((Sensor3FineCount >> 4) <= 6)
	movf	(_Sensor3FineCount),w
	movwf	(??_main+0)+0
	movlw	04h
u4995:
	clrc
	rrf	(??_main+0)+0,f
	addlw	-1
	skipz
	goto	u4995
	movlw	(07h)
	subwf	0+(??_main+0)+0,w
	skipnc
	goto	u5001
	goto	u5000
u5001:
	goto	l2760
u5000:
	line	511
	
l8668:	
;main.c: 510: {
;main.c: 511: PORTD &= 0xF0;
	movlw	(0F0h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	andwf	(8),f	;volatile
	line	512
;main.c: 512: PORTD ^= 0x03;
	movlw	(03h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	xorwf	(8),f	;volatile
	line	513
	
l2760:	
	line	514
;main.c: 513: }
;main.c: 514: if((Sensor3FineCount >> 4) <= 3)
	movf	(_Sensor3FineCount),w
	movwf	(??_main+0)+0
	movlw	04h
u5015:
	clrc
	rrf	(??_main+0)+0,f
	addlw	-1
	skipz
	goto	u5015
	movlw	(04h)
	subwf	0+(??_main+0)+0,w
	skipnc
	goto	u5021
	goto	u5020
u5021:
	goto	l8672
u5020:
	line	516
	
l8670:	
;main.c: 515: {
;main.c: 516: PORTD &= 0xF0;
	movlw	(0F0h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	andwf	(8),f	;volatile
	line	517
;main.c: 517: PORTD ^= 0x01;
	movlw	(01h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	xorwf	(8),f	;volatile
	goto	l8672
	line	518
	
l2761:	
	goto	l8672
	line	519
	
l2757:	
	goto	l8672
	line	521
	
l2756:	
	
l8672:	
;main.c: 518: }
;main.c: 519: }
;main.c: 521: if (PulseWidth3CurrentCount >= 1)
	movf	(_PulseWidth3CurrentCount),w
	skipz
	goto	u5030
	goto	l8678
u5030:
	line	523
	
l8674:	
;main.c: 522: {
;main.c: 523: EepFlagsSetting.EepFlags.EepSensor3CalAchieved = FALSE;
	bcf	(_EepFlagsSetting),5
	line	524
	
l8676:	
;main.c: 524: PORTD &= 0xF0;
	movlw	(0F0h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	andwf	(8),f	;volatile
	line	525
;main.c: 525: PulseWidth3CurrentCount = (PulseWidth3CurrentCount >> 1);
	movf	(_PulseWidth3CurrentCount),w
	movwf	(??_main+0)+0
	clrc
	rrf	(??_main+0)+0,w
	movwf	(??_main+1)+0
	movf	(??_main+1)+0,w
	movwf	(_PulseWidth3CurrentCount)
	line	526
;main.c: 526: PORTD = (PORTD | (0x08 >> (PulseWidth3CurrentCount - 1)));
	movf	(_PulseWidth3CurrentCount),w
	addlw	-1
	movwf	(??_main+0)+0
	incf	(??_main+0)+0,f
	movlw	low(08h)
	movwf	(??_main+1)+0
	movlw	high(08h)
	movwf	(??_main+1)+0+1
	goto	u5044
u5045:
	rlf	(??_main+1)+1,w
	rrf	(??_main+1)+1,f
	rrf	(??_main+1)+0,f
u5044:
	decfsz	(??_main+0)+0,f
	goto	u5045
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	0+(??_main+1)+0,w
	iorwf	(8),w	;volatile
	movwf	(8)	;volatile
	goto	l8678
	line	529
	
l2762:	
	line	536
	
l8678:	
;main.c: 529: }
;main.c: 536: if((EepSen2HiVal <= PulseWidth2CurrentCount) && (EepSen2LoVal <= (Sensor2FineCount >> 4)))
	movf	(_EepSen2HiVal),w
	subwf	(_PulseWidth2CurrentCount),w
	skipc
	goto	u5051
	goto	u5050
u5051:
	goto	l8684
u5050:
	
l8680:	
	movf	(_Sensor2FineCount),w
	movwf	(??_main+0)+0
	movlw	04h
u5065:
	clrc
	rrf	(??_main+0)+0,f
	addlw	-1
	skipz
	goto	u5065
	movf	(_EepSen2LoVal),w
	subwf	0+(??_main+0)+0,w
	skipc
	goto	u5071
	goto	u5070
u5071:
	goto	l8684
u5070:
	line	538
	
l8682:	
;main.c: 537: {
;main.c: 538: EepFlagsSetting.EepFlags.EepSensor2CalAchieved = TRUE;
	bsf	(_EepFlagsSetting),4
	line	541
;main.c: 541: }
	goto	l8698
	line	542
	
l2763:	
	
l8684:	
;main.c: 542: else if(PulseWidth2CurrentCount == 0)
	movf	(_PulseWidth2CurrentCount),f
	skipz
	goto	u5081
	goto	u5080
u5081:
	goto	l8698
u5080:
	line	544
	
l8686:	
;main.c: 543: {
;main.c: 544: EepFlagsSetting.EepFlags.EepSensor2CalAchieved = FALSE;
	bcf	(_EepFlagsSetting),4
	line	545
	
l8688:	
;main.c: 545: if((Sensor2FineCount >> 4) <= 15)
	movf	(_Sensor2FineCount),w
	movwf	(??_main+0)+0
	movlw	04h
u5095:
	clrc
	rrf	(??_main+0)+0,f
	addlw	-1
	skipz
	goto	u5095
	movlw	(010h)
	subwf	0+(??_main+0)+0,w
	skipnc
	goto	u5101
	goto	u5100
u5101:
	goto	l2766
u5100:
	line	547
	
l8690:	
;main.c: 546: {
;main.c: 547: PORTD &= 0x0F;
	movlw	(0Fh)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	andwf	(8),f	;volatile
	line	548
;main.c: 548: PORTD ^= 0xF0;
	movlw	(0F0h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	xorwf	(8),f	;volatile
	line	549
	
l2766:	
	line	550
;main.c: 549: }
;main.c: 550: if((Sensor2FineCount >> 4) <= 9)
	movf	(_Sensor2FineCount),w
	movwf	(??_main+0)+0
	movlw	04h
u5115:
	clrc
	rrf	(??_main+0)+0,f
	addlw	-1
	skipz
	goto	u5115
	movlw	(0Ah)
	subwf	0+(??_main+0)+0,w
	skipnc
	goto	u5121
	goto	u5120
u5121:
	goto	l2767
u5120:
	line	552
	
l8692:	
;main.c: 551: {
;main.c: 552: PORTD &= 0x0F;
	movlw	(0Fh)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	andwf	(8),f	;volatile
	line	553
;main.c: 553: PORTD ^= 0x70;
	movlw	(070h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	xorwf	(8),f	;volatile
	line	554
	
l2767:	
	line	555
;main.c: 554: }
;main.c: 555: if((Sensor2FineCount >> 4) <= 6)
	movf	(_Sensor2FineCount),w
	movwf	(??_main+0)+0
	movlw	04h
u5135:
	clrc
	rrf	(??_main+0)+0,f
	addlw	-1
	skipz
	goto	u5135
	movlw	(07h)
	subwf	0+(??_main+0)+0,w
	skipnc
	goto	u5141
	goto	u5140
u5141:
	goto	l2768
u5140:
	line	557
	
l8694:	
;main.c: 556: {
;main.c: 557: PORTD &= 0x0F;
	movlw	(0Fh)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	andwf	(8),f	;volatile
	line	558
;main.c: 558: PORTD ^= 0x30;
	movlw	(030h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	xorwf	(8),f	;volatile
	line	559
	
l2768:	
	line	560
;main.c: 559: }
;main.c: 560: if((Sensor2FineCount >> 4) <= 3)
	movf	(_Sensor2FineCount),w
	movwf	(??_main+0)+0
	movlw	04h
u5155:
	clrc
	rrf	(??_main+0)+0,f
	addlw	-1
	skipz
	goto	u5155
	movlw	(04h)
	subwf	0+(??_main+0)+0,w
	skipnc
	goto	u5161
	goto	u5160
u5161:
	goto	l8698
u5160:
	line	562
	
l8696:	
;main.c: 561: {
;main.c: 562: PORTD &= 0x0F;
	movlw	(0Fh)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	andwf	(8),f	;volatile
	line	563
;main.c: 563: PORTD ^= 0x10;
	movlw	(010h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	xorwf	(8),f	;volatile
	goto	l8698
	line	564
	
l2769:	
	goto	l8698
	line	565
	
l2765:	
	goto	l8698
	line	567
	
l2764:	
	
l8698:	
;main.c: 564: }
;main.c: 565: }
;main.c: 567: if (PulseWidth2CurrentCount >= 1)
	movf	(_PulseWidth2CurrentCount),w
	skipz
	goto	u5170
	goto	l8706
u5170:
	line	569
	
l8700:	
;main.c: 568: {
;main.c: 569: EepFlagsSetting.EepFlags.EepSensor2CalAchieved = FALSE;
	bcf	(_EepFlagsSetting),4
	line	570
	
l8702:	
;main.c: 570: PORTD &= 0x0F;
	movlw	(0Fh)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	andwf	(8),f	;volatile
	line	571
;main.c: 571: PulseWidth2CurrentCount = (PulseWidth2CurrentCount >> 1);
	movf	(_PulseWidth2CurrentCount),w
	movwf	(??_main+0)+0
	clrc
	rrf	(??_main+0)+0,w
	movwf	(??_main+1)+0
	movf	(??_main+1)+0,w
	movwf	(_PulseWidth2CurrentCount)
	line	572
;main.c: 572: PORTD = (PORTD | (0x10 << (PulseWidth2CurrentCount - 1)));
	movlw	(010h)
	movwf	(??_main+0)+0
	movf	(_PulseWidth2CurrentCount),w
	addlw	-1
	addlw	1
	goto	u5184
u5185:
	clrc
	rlf	(??_main+0)+0,f
u5184:
	addlw	-1
	skipz
	goto	u5185
	movf	0+(??_main+0)+0,w
	iorwf	(8),w	;volatile
	movwf	(8)	;volatile
	line	574
	
l8704:	
# 574 "F:\pic_projects\Hitch_Helper\main.c"
nop ;#
psect	maintext
	goto	l8706
	line	577
	
l2770:	
	line	585
	
l8706:	
;main.c: 577: }
;main.c: 585: if((EepSen1HiVal == PulseWidth1CurrentCount) && (EepSen1LoVal == (Sensor1FineCount >> 4)))
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(_EepSen1HiVal),w
	xorwf	(_PulseWidth1CurrentCount),w
	skipz
	goto	u5191
	goto	u5190
u5191:
	goto	l2771
u5190:
	
l8708:	
	movf	(_Sensor1FineCount),w
	movwf	(??_main+0)+0
	movlw	04h
u5205:
	clrc
	rrf	(??_main+0)+0,f
	addlw	-1
	skipz
	goto	u5205
	movf	0+(??_main+0)+0,w
	xorwf	(_EepSen1LoVal),w
	skipz
	goto	u5211
	goto	u5210
u5211:
	goto	l2771
u5210:
	line	587
	
l8710:	
;main.c: 586: {
;main.c: 587: EepFlagsSetting.EepFlags.EepSensor1CalAchieved = TRUE;
	bsf	(_EepFlagsSetting),3
	line	588
;main.c: 588: }
	goto	l8712
	line	589
	
l2771:	
	line	591
;main.c: 589: else
;main.c: 590: {
;main.c: 591: EepFlagsSetting.EepFlags.EepSensor1CalAchieved = FALSE;
	bcf	(_EepFlagsSetting),3
	goto	l8712
	line	592
	
l2772:	
	line	593
	
l8712:	
;main.c: 592: }
;main.c: 593: if(PulseWidth1CurrentCount <= 4)
	movlw	(05h)
	subwf	(_PulseWidth1CurrentCount),w
	skipnc
	goto	u5221
	goto	u5220
u5221:
	goto	l2773
u5220:
	line	595
	
l8714:	
;main.c: 594: {
;main.c: 595: PORTA = 0x08;
	movlw	(08h)
	movwf	(5)	;volatile
	line	596
	
l2773:	
	line	597
;main.c: 596: }
;main.c: 597: if(PulseWidth1CurrentCount <= 3)
	movlw	(04h)
	subwf	(_PulseWidth1CurrentCount),w
	skipnc
	goto	u5231
	goto	u5230
u5231:
	goto	l2774
u5230:
	line	599
	
l8716:	
;main.c: 598: {
;main.c: 599: PORTA = 0x04;
	movlw	(04h)
	movwf	(5)	;volatile
	line	600
	
l2774:	
	line	601
;main.c: 600: }
;main.c: 601: if(PulseWidth1CurrentCount <= 2)
	movlw	(03h)
	subwf	(_PulseWidth1CurrentCount),w
	skipnc
	goto	u5241
	goto	u5240
u5241:
	goto	l2775
u5240:
	line	603
	
l8718:	
;main.c: 602: {
;main.c: 603: PORTA = 0x02;
	movlw	(02h)
	movwf	(5)	;volatile
	line	604
	
l2775:	
	line	605
;main.c: 604: }
;main.c: 605: if(PulseWidth1CurrentCount <= 1)
	movlw	(02h)
	subwf	(_PulseWidth1CurrentCount),w
	skipnc
	goto	u5251
	goto	u5250
u5251:
	goto	l8722
u5250:
	line	607
	
l8720:	
;main.c: 606: {
;main.c: 607: PORTA = 0x01;
	movlw	(01h)
	movwf	(5)	;volatile
	goto	l8722
	line	608
	
l2776:	
	line	609
	
l8722:	
;main.c: 608: }
;main.c: 609: if(EepFlagsSetting.EepFlags.EepSensor1CalAchieved == TRUE)
	btfss	(_EepFlagsSetting),3
	goto	u5261
	goto	u5260
u5261:
	goto	l2777
u5260:
	line	613
	
l8724:	
;main.c: 612: {
;main.c: 613: RE1 = TRUE;
	bsf	(73/8),(73)&7
	line	614
;main.c: 614: }
	goto	l8726
	line	615
	
l2777:	
	line	617
;main.c: 615: else
;main.c: 616: {
;main.c: 617: RE1 = FALSE;
	bcf	(73/8),(73)&7
	goto	l8726
	line	618
	
l2778:	
	line	623
	
l8726:	
;main.c: 618: }
;main.c: 623: uart3bytes_tx(PulseWidth1CurrentCount,PulseWidth3CurrentCount,PulseWidth2CurrentCount);
	movf	(_PulseWidth3CurrentCount),w
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(?_uart3bytes_tx)
	movf	(_PulseWidth2CurrentCount),w
	movwf	(??_main+1)+0
	movf	(??_main+1)+0,w
	movwf	0+(?_uart3bytes_tx)+01h
	movf	(_PulseWidth1CurrentCount),w
	fcall	_uart3bytes_tx
	line	627
	
l8728:	
;main.c: 627: timer_set(PULSE_OUT_TIMER,(50));
	movlw	low(032h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(?_timer_set)
	movlw	high(032h)
	movwf	((?_timer_set))+1
	movlw	(0)
	fcall	_timer_set
	line	628
	
l8730:	
;main.c: 628: HH_PROCESS_STATE_MACHINE = PULSE_OUT_CCP2_SENSOR1;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(_HH_PROCESS_STATE_MACHINE)
	bsf	status,0
	rlf	(_HH_PROCESS_STATE_MACHINE),f
	line	629
	
l8732:	
# 629 "F:\pic_projects\Hitch_Helper\main.c"
clrwdt ;#
psect	maintext
	line	630
;main.c: 630: break;
	goto	l8746
	line	632
;main.c: 632: case EEPROM_SENSOR_CALIBRATION:
	
l2779:	
	line	634
	
l8734:	
;main.c: 634: PORTD = 0x22;
	movlw	(022h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(8)	;volatile
	line	635
	
l8736:	
# 635 "F:\pic_projects\Hitch_Helper\main.c"
clrwdt ;#
psect	maintext
	line	636
;main.c: 636: while((RE0) == TRUE)
	goto	l8742
	
l2781:	
	line	638
;main.c: 637: {
;main.c: 638: RE1 = TRUE;
	bsf	(73/8),(73)&7
	line	639
;main.c: 639: EepFlagsSetting.EepFlags.EepSensor1CalFlag = TRUE;
	bsf	(_EepFlagsSetting),0
	line	640
;main.c: 640: EepFlagsSetting.EepFlags.EepSensor2CalFlag = FALSE;
	bcf	(_EepFlagsSetting),1
	line	641
;main.c: 641: EepFlagsSetting.EepFlags.EepSensor3CalFlag = FALSE;
	bcf	(_EepFlagsSetting),2
	line	642
	
l8738:	
;main.c: 642: timer_set(PULSE_OUT_TIMER,(50));
	movlw	low(032h)
	movwf	(?_timer_set)
	movlw	high(032h)
	movwf	((?_timer_set))+1
	movlw	(0)
	fcall	_timer_set
	line	643
	
l8740:	
;main.c: 643: HH_PROCESS_STATE_MACHINE = PULSE_OUT_CCP2_SENSOR1;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(_HH_PROCESS_STATE_MACHINE)
	bsf	status,0
	rlf	(_HH_PROCESS_STATE_MACHINE),f
	goto	l8742
	line	644
	
l2780:	
	line	636
	
l8742:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfsc	(72/8),(72)&7
	goto	u5271
	goto	u5270
u5271:
	goto	l2781
u5270:
	goto	l8746
	
l2782:	
	line	645
;main.c: 644: }
;main.c: 645: break;
	goto	l8746
	line	647
;main.c: 647: default:
	
l2783:	
	line	648
;main.c: 648: break;
	goto	l8746
	line	649
	
l8744:	
;main.c: 649: }
	goto	l8746
	line	296
	
l2722:	
	
l8746:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(_HH_PROCESS_STATE_MACHINE),w
	; Switch size 1, requested type "space"
; Number of cases is 6, Range of values is 0 to 5
; switch strategies available:
; Name         Instructions Cycles
; simple_byte           19    10 (average)
; direct_byte           26     8 (fixed)
; jumptable            260     6 (fixed)
; rangetable            10     6 (fixed)
; spacedrange           18     9 (fixed)
; locatedrange           6     3 (fixed)
;	Chosen strategy is simple_byte

	opt asmopt_off
	xorlw	0^0	; case 0
	skipnz
	goto	l8474
	xorlw	1^0	; case 1
	skipnz
	goto	l8508
	xorlw	2^1	; case 2
	skipnz
	goto	l8556
	xorlw	3^2	; case 3
	skipnz
	goto	l8606
	xorlw	4^3	; case 4
	skipnz
	goto	l8652
	xorlw	5^4	; case 5
	skipnz
	goto	l8734
	goto	l8746
	opt asmopt_on

	line	649
	
l2729:	
	goto	l8746
	line	651
	
l2784:	
	line	294
	goto	l8746
	
l2785:	
	line	652
	
l2786:	
	global	start
	ljmp	start
	opt stack 0
GLOBAL	__end_of_main
	__end_of_main:
;; =============== function _main ends ============

	signat	_main,90
	global	_read_serial_ver
psect	text858,local,class=CODE,delta=2
global __ptext858
__ptext858:

;; *************** function _read_serial_ver *****************
;; Defined at:
;;		line 831 in file "F:\pic_projects\Hitch_Helper\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  i               1   10[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       1       0       0       0
;;      Temps:          0       2       0       0       0
;;      Totals:         0       3       0       0       0
;;Total ram usage:        3 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    4
;; This function calls:
;;		_eeprom_read_block1
;;		_eeprom_write_block
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text858
	file	"F:\pic_projects\Hitch_Helper\main.c"
	line	831
	global	__size_of_read_serial_ver
	__size_of_read_serial_ver	equ	__end_of_read_serial_ver-_read_serial_ver
	
_read_serial_ver:	
	opt	stack 3
; Regs used in _read_serial_ver: [wreg-fsr0h+status,2+status,0+pclath+cstack]
	line	834
	
l8428:	
;main.c: 832: uint8_t i;
;main.c: 834: eeprom_read_block1(0xFC,4,&read_current_sn[0]);
	movlw	(04h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_read_serial_ver+0)+0
	movf	(??_read_serial_ver+0)+0,w
	movwf	(?_eeprom_read_block1)
	movlw	(_read_current_sn)&0ffh
	movwf	(??_read_serial_ver+1)+0
	movf	(??_read_serial_ver+1)+0,w
	movwf	(0+?_eeprom_read_block1+01h)
	movlw	(0FCh)
	fcall	_eeprom_read_block1
	line	835
	
l8430:	
;main.c: 835: for(i=0;i<=3;i++)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(read_serial_ver@i)
	
l8432:	
	movlw	(04h)
	subwf	(read_serial_ver@i),w
	skipc
	goto	u4691
	goto	u4690
u4691:
	goto	l8436
u4690:
	goto	l2826
	
l8434:	
	goto	l2826
	line	836
	
l2820:	
	line	837
	
l8436:	
;main.c: 836: {
;main.c: 837: if(read_current_sn[i] != tom_sn1[i])
	movf	(read_serial_ver@i),w
	addlw	low((_tom_sn1-__stringbase))
	movwf	fsr0
	fcall	stringdir
	movwf	(??_read_serial_ver+0)+0
	movf	(read_serial_ver@i),w
	addlw	_read_current_sn&0ffh
	movwf	fsr0
	bcf	status, 7	;select IRP bank0
	movf	indf,w
	xorwf	(??_read_serial_ver+0)+0,w
	skipnz
	goto	u4701
	goto	u4700
u4701:
	goto	l8446
u4700:
	line	839
	
l8438:	
;main.c: 838: {
;main.c: 839: if(read_current_sn[i] != tom_sn2[i])
	movf	(read_serial_ver@i),w
	addlw	low((_tom_sn2-__stringbase))
	movwf	fsr0
	fcall	stringdir
	movwf	(??_read_serial_ver+0)+0
	movf	(read_serial_ver@i),w
	addlw	_read_current_sn&0ffh
	movwf	fsr0
	movf	indf,w
	xorwf	(??_read_serial_ver+0)+0,w
	skipnz
	goto	u4711
	goto	u4710
u4711:
	goto	l8446
u4710:
	line	841
	
l8440:	
;main.c: 840: {
;main.c: 841: if(read_current_sn[i] != bim_sn1[i])
	movf	(read_serial_ver@i),w
	addlw	low((_bim_sn1-__stringbase))
	movwf	fsr0
	fcall	stringdir
	movwf	(??_read_serial_ver+0)+0
	movf	(read_serial_ver@i),w
	addlw	_read_current_sn&0ffh
	movwf	fsr0
	movf	indf,w
	xorwf	(??_read_serial_ver+0)+0,w
	skipnz
	goto	u4721
	goto	u4720
u4721:
	goto	l8446
u4720:
	line	843
	
l8442:	
;main.c: 842: {
;main.c: 843: if(read_current_sn[i] != bim_sn2[i])
	movf	(read_serial_ver@i),w
	addlw	low((_bim_sn2-__stringbase))
	movwf	fsr0
	fcall	stringdir
	movwf	(??_read_serial_ver+0)+0
	movf	(read_serial_ver@i),w
	addlw	_read_current_sn&0ffh
	movwf	fsr0
	movf	indf,w
	xorwf	(??_read_serial_ver+0)+0,w
	skipnz
	goto	u4731
	goto	u4730
u4731:
	goto	l8446
u4730:
	line	845
	
l8444:	
;main.c: 844: {
;main.c: 845: eeprom_write_block(EepSerialNumbyte0,3,&default_sn[0]);
	movlw	(03h)
	movwf	(??_read_serial_ver+0)+0
	movf	(??_read_serial_ver+0)+0,w
	movwf	(?_eeprom_write_block)
	movlw	((_default_sn-__stringbase))&0ffh
	movwf	(??_read_serial_ver+1)+0
	movf	(??_read_serial_ver+1)+0,w
	movwf	(0+?_eeprom_write_block+01h)
	movlw	(0FCh)
	fcall	_eeprom_write_block
	goto	l8446
	line	846
	
l2825:	
	goto	l8446
	line	847
	
l2824:	
	goto	l8446
	line	848
	
l2823:	
	goto	l8446
	line	849
	
l2822:	
	line	835
	
l8446:	
	movlw	(01h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_read_serial_ver+0)+0
	movf	(??_read_serial_ver+0)+0,w
	addwf	(read_serial_ver@i),f
	
l8448:	
	movlw	(04h)
	subwf	(read_serial_ver@i),w
	skipc
	goto	u4741
	goto	u4740
u4741:
	goto	l8436
u4740:
	goto	l2826
	
l2821:	
	line	851
	
l2826:	
	return
	opt stack 0
GLOBAL	__end_of_read_serial_ver
	__end_of_read_serial_ver:
;; =============== function _read_serial_ver ends ============

	signat	_read_serial_ver,88
	global	_eeprom_read_block1
psect	text859,local,class=CODE,delta=2
global __ptext859
__ptext859:

;; *************** function _eeprom_read_block1 *****************
;; Defined at:
;;		line 90 in file "F:\pic_projects\Hitch_Helper\eeprom.c"
;; Parameters:    Size  Location     Type
;;  address         1    wreg     unsigned char 
;;  size            1    2[BANK0 ] unsigned char 
;;  buffer          1    3[BANK0 ] PTR unsigned char 
;;		 -> read_current_sn(4), 
;; Auto vars:     Size  Location     Type
;;  address         1    5[BANK0 ] unsigned char 
;;  i               1    6[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       2       0       0       0
;;      Locals:         0       2       0       0       0
;;      Temps:          0       1       0       0       0
;;      Totals:         0       5       0       0       0
;;Total ram usage:        5 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		_eeprom_read
;; This function is called by:
;;		_read_serial_ver
;; This function uses a non-reentrant model
;;
psect	text859
	file	"F:\pic_projects\Hitch_Helper\eeprom.c"
	line	90
	global	__size_of_eeprom_read_block1
	__size_of_eeprom_read_block1	equ	__end_of_eeprom_read_block1-_eeprom_read_block1
	
_eeprom_read_block1:	
	opt	stack 3
; Regs used in _eeprom_read_block1: [wreg-fsr0h+status,2+status,0+pclath+cstack]
;eeprom_read_block1@address stored from wreg
	line	94
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(eeprom_read_block1@address)
	
l8414:	
;eeprom.c: 91: unsigned char i;
;eeprom.c: 94: if (size != 0)
	movf	(eeprom_read_block1@size),w
	skipz
	goto	u4670
	goto	l4564
u4670:
	line	96
	
l8416:	
;eeprom.c: 95: {
;eeprom.c: 96: for (i = 0; i < size; i++)
	clrf	(eeprom_read_block1@i)
	goto	l8426
	line	97
	
l4562:	
	line	100
	
l8418:	
;eeprom.c: 97: {
;eeprom.c: 100: *buffer = (unsigned char)(eeprom_read(address));
	movf	(eeprom_read_block1@address),w
	fcall	_eeprom_read
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_eeprom_read_block1+0)+0
	movf	(eeprom_read_block1@buffer),w
	movwf	fsr0
	movf	(??_eeprom_read_block1+0)+0,w
	bcf	status, 7	;select IRP bank0
	movwf	indf
	line	102
	
l8420:	
;eeprom.c: 102: buffer++;
	movlw	(01h)
	movwf	(??_eeprom_read_block1+0)+0
	movf	(??_eeprom_read_block1+0)+0,w
	addwf	(eeprom_read_block1@buffer),f
	line	104
	
l8422:	
;eeprom.c: 104: address++;
	movlw	(01h)
	movwf	(??_eeprom_read_block1+0)+0
	movf	(??_eeprom_read_block1+0)+0,w
	addwf	(eeprom_read_block1@address),f
	line	96
	
l8424:	
	movlw	(01h)
	movwf	(??_eeprom_read_block1+0)+0
	movf	(??_eeprom_read_block1+0)+0,w
	addwf	(eeprom_read_block1@i),f
	goto	l8426
	
l4561:	
	
l8426:	
	movf	(eeprom_read_block1@size),w
	subwf	(eeprom_read_block1@i),w
	skipc
	goto	u4681
	goto	u4680
u4681:
	goto	l8418
u4680:
	goto	l4564
	
l4563:	
	goto	l4564
	line	106
	
l4560:	
	line	107
	
l4564:	
	return
	opt stack 0
GLOBAL	__end_of_eeprom_read_block1
	__end_of_eeprom_read_block1:
;; =============== function _eeprom_read_block1 ends ============

	signat	_eeprom_read_block1,12408
	global	_uart3bytes_tx
psect	text860,local,class=CODE,delta=2
global __ptext860
__ptext860:

;; *************** function _uart3bytes_tx *****************
;; Defined at:
;;		line 85 in file "F:\pic_projects\Hitch_Helper\uart_func.c"
;; Parameters:    Size  Location     Type
;;  EepSen1HiVal    1    wreg     unsigned char 
;;  EepSen2HiVal    1    0[BANK0 ] unsigned char 
;;  EepSen3HiVal    1    1[BANK0 ] unsigned char 
;; Auto vars:     Size  Location     Type
;;  EepSen1HiVal    1    4[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       2       0       0       0
;;      Locals:         0       1       0       0       0
;;      Temps:          0       2       0       0       0
;;      Totals:         0       5       0       0       0
;;Total ram usage:        5 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text860
	file	"F:\pic_projects\Hitch_Helper\uart_func.c"
	line	85
	global	__size_of_uart3bytes_tx
	__size_of_uart3bytes_tx	equ	__end_of_uart3bytes_tx-_uart3bytes_tx
	
_uart3bytes_tx:	
	opt	stack 5
; Regs used in _uart3bytes_tx: [wreg+status,2]
;uart3bytes_tx@EepSen1HiValUart stored from wreg
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(uart3bytes_tx@EepSen1HiValUart)
	line	86
	
l8398:	
# 86 "F:\pic_projects\Hitch_Helper\uart_func.c"
nop ;#
	line	87
# 87 "F:\pic_projects\Hitch_Helper\uart_func.c"
nop ;#
	line	88
# 88 "F:\pic_projects\Hitch_Helper\uart_func.c"
nop ;#
psect	text860
	line	91
;uart_func.c: 91: (GIE = 0);
	bcf	(95/8),(95)&7
	line	92
	
l8400:	
;uart_func.c: 92: TXREG = (EepSen1HiValUart | (0x20));
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(uart3bytes_tx@EepSen1HiValUart),w
	iorlw	020h
	movwf	(25)	;volatile
	line	93
;uart_func.c: 93: while (TRMT = 0)
	goto	l5426
	
l5427:	
	line	96
;uart_func.c: 94: {
	
l5426:	
	line	93
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bcf	(1217/8)^080h,(1217)&7
	btfsc	(1217/8)^080h,(1217)&7
	goto	u4641
	goto	u4640
u4641:
	goto	l5426
u4640:
	goto	l8402
	
l5428:	
	line	97
	
l8402:	
;uart_func.c: 96: }
;uart_func.c: 97: _delay(3000);
	opt asmopt_off
movlw	4
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
movwf	((??_uart3bytes_tx+0)+0+1),f
	movlw	228
movwf	((??_uart3bytes_tx+0)+0),f
u5287:
	decfsz	((??_uart3bytes_tx+0)+0),f
	goto	u5287
	decfsz	((??_uart3bytes_tx+0)+0+1),f
	goto	u5287
	clrwdt
opt asmopt_on

	line	98
	
l8404:	
;uart_func.c: 98: TXREG = (EepSen2HiValUart | (0x40));
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(uart3bytes_tx@EepSen2HiValUart),w
	iorlw	040h
	movwf	(25)	;volatile
	line	99
;uart_func.c: 99: while (TRMT = 0)
	goto	l5429
	
l5430:	
	line	102
;uart_func.c: 100: {
	
l5429:	
	line	99
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bcf	(1217/8)^080h,(1217)&7
	btfsc	(1217/8)^080h,(1217)&7
	goto	u4651
	goto	u4650
u4651:
	goto	l5429
u4650:
	goto	l8406
	
l5431:	
	line	103
	
l8406:	
;uart_func.c: 102: }
;uart_func.c: 103: _delay(3000);
	opt asmopt_off
movlw	4
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
movwf	((??_uart3bytes_tx+0)+0+1),f
	movlw	228
movwf	((??_uart3bytes_tx+0)+0),f
u5297:
	decfsz	((??_uart3bytes_tx+0)+0),f
	goto	u5297
	decfsz	((??_uart3bytes_tx+0)+0+1),f
	goto	u5297
	clrwdt
opt asmopt_on

	line	104
	
l8408:	
;uart_func.c: 104: TXREG = (EepSen3HiValUart | (0x80));
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(uart3bytes_tx@EepSen3HiValUart),w
	iorlw	080h
	movwf	(25)	;volatile
	line	105
;uart_func.c: 105: while (TRMT = 0)
	goto	l5432
	
l5433:	
	line	108
;uart_func.c: 106: {
	
l5432:	
	line	105
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bcf	(1217/8)^080h,(1217)&7
	btfsc	(1217/8)^080h,(1217)&7
	goto	u4661
	goto	u4660
u4661:
	goto	l5432
u4660:
	goto	l8410
	
l5434:	
	line	109
	
l8410:	
;uart_func.c: 108: }
;uart_func.c: 109: _delay(3000);
	opt asmopt_off
movlw	4
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
movwf	((??_uart3bytes_tx+0)+0+1),f
	movlw	228
movwf	((??_uart3bytes_tx+0)+0),f
u5307:
	decfsz	((??_uart3bytes_tx+0)+0),f
	goto	u5307
	decfsz	((??_uart3bytes_tx+0)+0+1),f
	goto	u5307
	clrwdt
opt asmopt_on

	line	110
	
l8412:	
;uart_func.c: 110: (GIE = 1);
	bsf	(95/8),(95)&7
	line	111
	
l5435:	
	return
	opt stack 0
GLOBAL	__end_of_uart3bytes_tx
	__end_of_uart3bytes_tx:
;; =============== function _uart3bytes_tx ends ============

	signat	_uart3bytes_tx,12408
	global	_eepSens3WriteCal
psect	text861,local,class=CODE,delta=2
global __ptext861
__ptext861:

;; *************** function _eepSens3WriteCal *****************
;; Defined at:
;;		line 811 in file "F:\pic_projects\Hitch_Helper\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       2       0       0       0
;;      Totals:         0       2       0       0       0
;;Total ram usage:        2 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		_eeprom_write
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text861
	file	"F:\pic_projects\Hitch_Helper\main.c"
	line	811
	global	__size_of_eepSens3WriteCal
	__size_of_eepSens3WriteCal	equ	__end_of_eepSens3WriteCal-_eepSens3WriteCal
	
_eepSens3WriteCal:	
	opt	stack 4
; Regs used in _eepSens3WriteCal: [wreg+status,2+status,0+pclath+cstack]
	line	812
	
l8384:	
;main.c: 812: EepSen3LoVal = (Sensor3FineCount >> 4);
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(_Sensor3FineCount),w
	movwf	(??_eepSens3WriteCal+0)+0
	movlw	04h
u4625:
	clrc
	rrf	(??_eepSens3WriteCal+0)+0,f
	addlw	-1
	skipz
	goto	u4625
	movf	0+(??_eepSens3WriteCal+0)+0,w
	movwf	(??_eepSens3WriteCal+1)+0
	movf	(??_eepSens3WriteCal+1)+0,w
	movwf	(_EepSen3LoVal)
	line	813
;main.c: 813: EepSen3LoValCopy = (Sensor3FineCount >> 4);
	movf	(_Sensor3FineCount),w
	movwf	(??_eepSens3WriteCal+0)+0
	movlw	04h
u4635:
	clrc
	rrf	(??_eepSens3WriteCal+0)+0,f
	addlw	-1
	skipz
	goto	u4635
	movf	0+(??_eepSens3WriteCal+0)+0,w
	movwf	(??_eepSens3WriteCal+1)+0
	movf	(??_eepSens3WriteCal+1)+0,w
	movwf	(_EepSen3LoValCopy)
	line	814
	
l8386:	
;main.c: 814: eeprom_write(EepSen3LoValAddr, EepSen3LoVal);
	movf	(_EepSen3LoVal),w
	movwf	(??_eepSens3WriteCal+0)+0
	movf	(??_eepSens3WriteCal+0)+0,w
	movwf	(?_eeprom_write)
	movlw	(05h)
	fcall	_eeprom_write
	line	815
	
l8388:	
;main.c: 815: eeprom_write(EepSen3LoValAddrCopy, EepSen3LoValCopy);
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(_EepSen3LoValCopy),w
	movwf	(??_eepSens3WriteCal+0)+0
	movf	(??_eepSens3WriteCal+0)+0,w
	movwf	(?_eeprom_write)
	movlw	(075h)
	fcall	_eeprom_write
	line	817
	
l8390:	
;main.c: 817: EepSen3HiVal = PulseWidth3CurrentCount;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(_PulseWidth3CurrentCount),w
	movwf	(??_eepSens3WriteCal+0)+0
	movf	(??_eepSens3WriteCal+0)+0,w
	movwf	(_EepSen3HiVal)
	line	818
	
l8392:	
;main.c: 818: EepSen3HiValCopy = PulseWidth3CurrentCount;
	movf	(_PulseWidth3CurrentCount),w
	movwf	(??_eepSens3WriteCal+0)+0
	movf	(??_eepSens3WriteCal+0)+0,w
	movwf	(_EepSen3HiValCopy)
	line	819
	
l8394:	
;main.c: 819: eeprom_write(EepSen3HiValAddr, EepSen3HiVal);
	movf	(_EepSen3HiVal),w
	movwf	(??_eepSens3WriteCal+0)+0
	movf	(??_eepSens3WriteCal+0)+0,w
	movwf	(?_eeprom_write)
	movlw	(04h)
	fcall	_eeprom_write
	line	820
	
l8396:	
;main.c: 820: eeprom_write(EepSen3HiValAddrCopy, EepSen3HiValCopy);
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(_EepSen3HiValCopy),w
	movwf	(??_eepSens3WriteCal+0)+0
	movf	(??_eepSens3WriteCal+0)+0,w
	movwf	(?_eeprom_write)
	movlw	(074h)
	fcall	_eeprom_write
	line	821
	
l2817:	
	return
	opt stack 0
GLOBAL	__end_of_eepSens3WriteCal
	__end_of_eepSens3WriteCal:
;; =============== function _eepSens3WriteCal ends ============

	signat	_eepSens3WriteCal,88
	global	_eepSens2WriteCal
psect	text862,local,class=CODE,delta=2
global __ptext862
__ptext862:

;; *************** function _eepSens2WriteCal *****************
;; Defined at:
;;		line 793 in file "F:\pic_projects\Hitch_Helper\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       2       0       0       0
;;      Totals:         0       2       0       0       0
;;Total ram usage:        2 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		_eeprom_write
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text862
	file	"F:\pic_projects\Hitch_Helper\main.c"
	line	793
	global	__size_of_eepSens2WriteCal
	__size_of_eepSens2WriteCal	equ	__end_of_eepSens2WriteCal-_eepSens2WriteCal
	
_eepSens2WriteCal:	
	opt	stack 4
; Regs used in _eepSens2WriteCal: [wreg+status,2+status,0+pclath+cstack]
	line	794
	
l8370:	
;main.c: 794: EepSen2LoVal = (Sensor2FineCount >> 4);
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(_Sensor2FineCount),w
	movwf	(??_eepSens2WriteCal+0)+0
	movlw	04h
u4605:
	clrc
	rrf	(??_eepSens2WriteCal+0)+0,f
	addlw	-1
	skipz
	goto	u4605
	movf	0+(??_eepSens2WriteCal+0)+0,w
	movwf	(??_eepSens2WriteCal+1)+0
	movf	(??_eepSens2WriteCal+1)+0,w
	movwf	(_EepSen2LoVal)
	line	795
;main.c: 795: EepSen2LoValCopy = (Sensor2FineCount >> 4);
	movf	(_Sensor2FineCount),w
	movwf	(??_eepSens2WriteCal+0)+0
	movlw	04h
u4615:
	clrc
	rrf	(??_eepSens2WriteCal+0)+0,f
	addlw	-1
	skipz
	goto	u4615
	movf	0+(??_eepSens2WriteCal+0)+0,w
	movwf	(??_eepSens2WriteCal+1)+0
	movf	(??_eepSens2WriteCal+1)+0,w
	movwf	(_EepSen2LoValCopy)
	line	796
	
l8372:	
;main.c: 796: eeprom_write(EepSen2LoValAddr, EepSen2LoVal);
	movf	(_EepSen2LoVal),w
	movwf	(??_eepSens2WriteCal+0)+0
	movf	(??_eepSens2WriteCal+0)+0,w
	movwf	(?_eeprom_write)
	movlw	(03h)
	fcall	_eeprom_write
	line	797
	
l8374:	
;main.c: 797: eeprom_write(EepSen2LoValAddrCopy, EepSen2LoValCopy);
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(_EepSen2LoValCopy),w
	movwf	(??_eepSens2WriteCal+0)+0
	movf	(??_eepSens2WriteCal+0)+0,w
	movwf	(?_eeprom_write)
	movlw	(073h)
	fcall	_eeprom_write
	line	799
	
l8376:	
;main.c: 799: EepSen2HiVal = PulseWidth2CurrentCount;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(_PulseWidth2CurrentCount),w
	movwf	(??_eepSens2WriteCal+0)+0
	movf	(??_eepSens2WriteCal+0)+0,w
	movwf	(_EepSen2HiVal)
	line	800
	
l8378:	
;main.c: 800: EepSen2HiValCopy = PulseWidth2CurrentCount;
	movf	(_PulseWidth2CurrentCount),w
	movwf	(??_eepSens2WriteCal+0)+0
	movf	(??_eepSens2WriteCal+0)+0,w
	movwf	(_EepSen2HiValCopy)
	line	801
	
l8380:	
;main.c: 801: eeprom_write(EepSen2HiValAddr, EepSen2HiVal);
	movf	(_EepSen2HiVal),w
	movwf	(??_eepSens2WriteCal+0)+0
	movf	(??_eepSens2WriteCal+0)+0,w
	movwf	(?_eeprom_write)
	movlw	(02h)
	fcall	_eeprom_write
	line	802
	
l8382:	
;main.c: 802: eeprom_write(EepSen2HiValAddrCopy, EepSen2HiValCopy);
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(_EepSen2HiValCopy),w
	movwf	(??_eepSens2WriteCal+0)+0
	movf	(??_eepSens2WriteCal+0)+0,w
	movwf	(?_eeprom_write)
	movlw	(072h)
	fcall	_eeprom_write
	line	803
	
l2814:	
	return
	opt stack 0
GLOBAL	__end_of_eepSens2WriteCal
	__end_of_eepSens2WriteCal:
;; =============== function _eepSens2WriteCal ends ============

	signat	_eepSens2WriteCal,88
	global	_eepSens1WriteCal
psect	text863,local,class=CODE,delta=2
global __ptext863
__ptext863:

;; *************** function _eepSens1WriteCal *****************
;; Defined at:
;;		line 774 in file "F:\pic_projects\Hitch_Helper\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       2       0       0       0
;;      Totals:         0       2       0       0       0
;;Total ram usage:        2 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		_eeprom_write
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text863
	file	"F:\pic_projects\Hitch_Helper\main.c"
	line	774
	global	__size_of_eepSens1WriteCal
	__size_of_eepSens1WriteCal	equ	__end_of_eepSens1WriteCal-_eepSens1WriteCal
	
_eepSens1WriteCal:	
	opt	stack 4
; Regs used in _eepSens1WriteCal: [wreg+status,2+status,0+pclath+cstack]
	line	775
	
l8356:	
;main.c: 775: EepSen1LoVal = (Sensor1FineCount >> 4);
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(_Sensor1FineCount),w
	movwf	(??_eepSens1WriteCal+0)+0
	movlw	04h
u4585:
	clrc
	rrf	(??_eepSens1WriteCal+0)+0,f
	addlw	-1
	skipz
	goto	u4585
	movf	0+(??_eepSens1WriteCal+0)+0,w
	movwf	(??_eepSens1WriteCal+1)+0
	movf	(??_eepSens1WriteCal+1)+0,w
	movwf	(_EepSen1LoVal)
	line	776
;main.c: 776: EepSen1LoValCopy = (Sensor1FineCount >> 4);
	movf	(_Sensor1FineCount),w
	movwf	(??_eepSens1WriteCal+0)+0
	movlw	04h
u4595:
	clrc
	rrf	(??_eepSens1WriteCal+0)+0,f
	addlw	-1
	skipz
	goto	u4595
	movf	0+(??_eepSens1WriteCal+0)+0,w
	movwf	(??_eepSens1WriteCal+1)+0
	movf	(??_eepSens1WriteCal+1)+0,w
	movwf	(_EepSen1LoValCopy)
	line	777
	
l8358:	
;main.c: 777: eeprom_write(EepSen1LoValAddr, EepSen1LoVal);
	movf	(_EepSen1LoVal),w
	movwf	(??_eepSens1WriteCal+0)+0
	movf	(??_eepSens1WriteCal+0)+0,w
	movwf	(?_eeprom_write)
	movlw	(01h)
	fcall	_eeprom_write
	line	778
	
l8360:	
;main.c: 778: eeprom_write(EepSen1LoValAddrCopy, EepSen1LoValCopy);
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(_EepSen1LoValCopy),w
	movwf	(??_eepSens1WriteCal+0)+0
	movf	(??_eepSens1WriteCal+0)+0,w
	movwf	(?_eeprom_write)
	movlw	(071h)
	fcall	_eeprom_write
	line	780
	
l8362:	
;main.c: 780: EepSen1HiVal = PulseWidth1CurrentCount;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(_PulseWidth1CurrentCount),w
	movwf	(??_eepSens1WriteCal+0)+0
	movf	(??_eepSens1WriteCal+0)+0,w
	movwf	(_EepSen1HiVal)
	line	781
	
l8364:	
;main.c: 781: EepSen1HiValCopy = PulseWidth1CurrentCount;
	movf	(_PulseWidth1CurrentCount),w
	movwf	(??_eepSens1WriteCal+0)+0
	movf	(??_eepSens1WriteCal+0)+0,w
	movwf	(_EepSen1HiValCopy)
	line	782
	
l8366:	
;main.c: 782: eeprom_write(EepSen1HiValAddr, EepSen1HiVal);
	movf	(_EepSen1HiVal),w
	movwf	(??_eepSens1WriteCal+0)+0
	movf	(??_eepSens1WriteCal+0)+0,w
	movwf	(?_eeprom_write)
	movlw	(0)
	fcall	_eeprom_write
	line	783
	
l8368:	
;main.c: 783: eeprom_write(EepSen1HiValAddrCopy, EepSen1HiValCopy);
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(_EepSen1HiValCopy),w
	movwf	(??_eepSens1WriteCal+0)+0
	movf	(??_eepSens1WriteCal+0)+0,w
	movwf	(?_eeprom_write)
	movlw	(070h)
	fcall	_eeprom_write
	line	785
	
l2811:	
	return
	opt stack 0
GLOBAL	__end_of_eepSens1WriteCal
	__end_of_eepSens1WriteCal:
;; =============== function _eepSens1WriteCal ends ============

	signat	_eepSens1WriteCal,88
	global	_AreCalCopyCorrect
psect	text864,local,class=CODE,delta=2
global __ptext864
__ptext864:

;; *************** function _AreCalCopyCorrect *****************
;; Defined at:
;;		line 719 in file "F:\pic_projects\Hitch_Helper\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       1       0       0       0
;;      Totals:         0       1       0       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		_eeprom_read
;;		_timer_set
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text864
	file	"F:\pic_projects\Hitch_Helper\main.c"
	line	719
	global	__size_of_AreCalCopyCorrect
	__size_of_AreCalCopyCorrect	equ	__end_of_AreCalCopyCorrect-_AreCalCopyCorrect
	
_AreCalCopyCorrect:	
	opt	stack 4
; Regs used in _AreCalCopyCorrect: [wreg-fsr0h+status,2+status,0+pclath+cstack]
	line	721
	
l8346:	
;main.c: 721: EepSen1LoVal = eeprom_read(EepSen1LoValAddr);
	movlw	(01h)
	fcall	_eeprom_read
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_AreCalCopyCorrect+0)+0
	movf	(??_AreCalCopyCorrect+0)+0,w
	movwf	(_EepSen1LoVal)
	line	722
;main.c: 722: EepSen1HiVal = eeprom_read(EepSen1HiValAddr);
	movlw	(0)
	fcall	_eeprom_read
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_AreCalCopyCorrect+0)+0
	movf	(??_AreCalCopyCorrect+0)+0,w
	movwf	(_EepSen1HiVal)
	line	723
;main.c: 723: EepSen2LoVal = eeprom_read(EepSen2LoValAddr);
	movlw	(03h)
	fcall	_eeprom_read
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_AreCalCopyCorrect+0)+0
	movf	(??_AreCalCopyCorrect+0)+0,w
	movwf	(_EepSen2LoVal)
	line	724
;main.c: 724: EepSen2HiVal = eeprom_read(EepSen2HiValAddr);
	movlw	(02h)
	fcall	_eeprom_read
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_AreCalCopyCorrect+0)+0
	movf	(??_AreCalCopyCorrect+0)+0,w
	movwf	(_EepSen2HiVal)
	line	725
;main.c: 725: EepSen3LoVal = eeprom_read(EepSen3LoValAddr);
	movlw	(05h)
	fcall	_eeprom_read
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_AreCalCopyCorrect+0)+0
	movf	(??_AreCalCopyCorrect+0)+0,w
	movwf	(_EepSen3LoVal)
	line	726
;main.c: 726: EepSen3HiVal = eeprom_read(EepSen3HiValAddr);
	movlw	(04h)
	fcall	_eeprom_read
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_AreCalCopyCorrect+0)+0
	movf	(??_AreCalCopyCorrect+0)+0,w
	movwf	(_EepSen3HiVal)
	line	728
;main.c: 728: EepSen1LoValCopy = eeprom_read(EepSen1LoValAddrCopy);
	movlw	(071h)
	fcall	_eeprom_read
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_AreCalCopyCorrect+0)+0
	movf	(??_AreCalCopyCorrect+0)+0,w
	movwf	(_EepSen1LoValCopy)
	line	729
;main.c: 729: EepSen1HiValCopy = eeprom_read(EepSen1HiValAddrCopy);
	movlw	(070h)
	fcall	_eeprom_read
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_AreCalCopyCorrect+0)+0
	movf	(??_AreCalCopyCorrect+0)+0,w
	movwf	(_EepSen1HiValCopy)
	line	730
;main.c: 730: EepSen2LoValCopy = eeprom_read(EepSen2LoValAddrCopy);
	movlw	(073h)
	fcall	_eeprom_read
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_AreCalCopyCorrect+0)+0
	movf	(??_AreCalCopyCorrect+0)+0,w
	movwf	(_EepSen2LoValCopy)
	line	731
;main.c: 731: EepSen2HiValCopy = eeprom_read(EepSen2HiValAddrCopy);
	movlw	(072h)
	fcall	_eeprom_read
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_AreCalCopyCorrect+0)+0
	movf	(??_AreCalCopyCorrect+0)+0,w
	movwf	(_EepSen2HiValCopy)
	line	732
;main.c: 732: EepSen3LoValCopy = eeprom_read(EepSen3LoValAddrCopy);
	movlw	(075h)
	fcall	_eeprom_read
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_AreCalCopyCorrect+0)+0
	movf	(??_AreCalCopyCorrect+0)+0,w
	movwf	(_EepSen3LoValCopy)
	line	733
;main.c: 733: EepSen3HiValCopy = eeprom_read(EepSen3HiValAddrCopy);
	movlw	(074h)
	fcall	_eeprom_read
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_AreCalCopyCorrect+0)+0
	movf	(??_AreCalCopyCorrect+0)+0,w
	movwf	(_EepSen3HiValCopy)
	line	735
	
l8348:	
;main.c: 735: if((EepSen1LoVal != EepSen1LoValCopy) || (EepSen1HiVal != EepSen1HiValCopy))
	movf	(_EepSen1LoVal),w
	xorwf	(_EepSen1LoValCopy),w
	skipz
	goto	u4561
	goto	u4560
u4561:
	goto	l8352
u4560:
	
l8350:	
	movf	(_EepSen1HiVal),w
	xorwf	(_EepSen1HiValCopy),w
	skipnz
	goto	u4571
	goto	u4570
u4571:
	goto	l2803
u4570:
	goto	l8352
	
l2801:	
	line	740
	
l8352:	
;main.c: 739: {
;main.c: 740: timer_set(PULSE_OUT_TIMER,(50));
	movlw	low(032h)
	movwf	(?_timer_set)
	movlw	high(032h)
	movwf	((?_timer_set))+1
	movlw	(0)
	fcall	_timer_set
	line	741
	
l8354:	
;main.c: 741: HH_PROCESS_STATE_MACHINE = EEPROM_SENSOR_CALIBRATION;
	movlw	(05h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_AreCalCopyCorrect+0)+0
	movf	(??_AreCalCopyCorrect+0)+0,w
	movwf	(_HH_PROCESS_STATE_MACHINE)
	line	742
;main.c: 742: }
	goto	l2803
	line	743
	
l2799:	
	goto	l2803
	line	746
;main.c: 743: else
;main.c: 744: {
	
l2802:	
	line	747
	
l2803:	
	return
	opt stack 0
GLOBAL	__end_of_AreCalCopyCorrect
	__end_of_AreCalCopyCorrect:
;; =============== function _AreCalCopyCorrect ends ============

	signat	_AreCalCopyCorrect,88
	global	_RecalSensors
psect	text865,local,class=CODE,delta=2
global __ptext865
__ptext865:

;; *************** function _RecalSensors *****************
;; Defined at:
;;		line 758 in file "F:\pic_projects\Hitch_Helper\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       1       0       0       0
;;      Totals:         0       1       0       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		_timer_set
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text865
	file	"F:\pic_projects\Hitch_Helper\main.c"
	line	758
	global	__size_of_RecalSensors
	__size_of_RecalSensors	equ	__end_of_RecalSensors-_RecalSensors
	
_RecalSensors:	
	opt	stack 4
; Regs used in _RecalSensors: [wreg-fsr0h+status,2+status,0+pclath+cstack]
	line	759
	
l8340:	
;main.c: 759: if((RE0) == TRUE)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfss	(72/8),(72)&7
	goto	u4551
	goto	u4550
u4551:
	goto	l2808
u4550:
	line	761
	
l8342:	
;main.c: 760: {
;main.c: 761: timer_set(PULSE_OUT_TIMER,(50));
	movlw	low(032h)
	movwf	(?_timer_set)
	movlw	high(032h)
	movwf	((?_timer_set))+1
	movlw	(0)
	fcall	_timer_set
	line	762
	
l8344:	
;main.c: 762: HH_PROCESS_STATE_MACHINE = EEPROM_SENSOR_CALIBRATION;
	movlw	(05h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_RecalSensors+0)+0
	movf	(??_RecalSensors+0)+0,w
	movwf	(_HH_PROCESS_STATE_MACHINE)
	line	764
;main.c: 764: }
	goto	l2808
	line	765
	
l2806:	
	goto	l2808
;main.c: 765: else{}
	
l2807:	
	line	766
	
l2808:	
	return
	opt stack 0
GLOBAL	__end_of_RecalSensors
	__end_of_RecalSensors:
;; =============== function _RecalSensors ends ============

	signat	_RecalSensors,88
	global	_AreSensorsCaled
psect	text866,local,class=CODE,delta=2
global __ptext866
__ptext866:

;; *************** function _AreSensorsCaled *****************
;; Defined at:
;;		line 687 in file "F:\pic_projects\Hitch_Helper\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       1       0       0       0
;;      Totals:         0       1       0       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		_eeprom_read
;;		_timer_set
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text866
	file	"F:\pic_projects\Hitch_Helper\main.c"
	line	687
	global	__size_of_AreSensorsCaled
	__size_of_AreSensorsCaled	equ	__end_of_AreSensorsCaled-_AreSensorsCaled
	
_AreSensorsCaled:	
	opt	stack 4
; Regs used in _AreSensorsCaled: [wreg-fsr0h+status,2+status,0+pclath+cstack]
	line	690
	
l8330:	
;main.c: 690: EepSen1LoVal = eeprom_read(EepSen1LoValAddr);
	movlw	(01h)
	fcall	_eeprom_read
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_AreSensorsCaled+0)+0
	movf	(??_AreSensorsCaled+0)+0,w
	movwf	(_EepSen1LoVal)
	line	691
;main.c: 691: EepSen1HiVal = eeprom_read(EepSen1HiValAddr);
	movlw	(0)
	fcall	_eeprom_read
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_AreSensorsCaled+0)+0
	movf	(??_AreSensorsCaled+0)+0,w
	movwf	(_EepSen1HiVal)
	line	692
;main.c: 692: EepSen2LoVal = eeprom_read(EepSen2LoValAddr);
	movlw	(03h)
	fcall	_eeprom_read
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_AreSensorsCaled+0)+0
	movf	(??_AreSensorsCaled+0)+0,w
	movwf	(_EepSen2LoVal)
	line	693
;main.c: 693: EepSen2HiVal = eeprom_read(EepSen2HiValAddr);
	movlw	(02h)
	fcall	_eeprom_read
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_AreSensorsCaled+0)+0
	movf	(??_AreSensorsCaled+0)+0,w
	movwf	(_EepSen2HiVal)
	line	694
;main.c: 694: EepSen3LoVal = eeprom_read(EepSen3LoValAddr);
	movlw	(05h)
	fcall	_eeprom_read
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_AreSensorsCaled+0)+0
	movf	(??_AreSensorsCaled+0)+0,w
	movwf	(_EepSen3LoVal)
	line	695
;main.c: 695: EepSen3HiVal = eeprom_read(EepSen3HiValAddr);
	movlw	(04h)
	fcall	_eeprom_read
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_AreSensorsCaled+0)+0
	movf	(??_AreSensorsCaled+0)+0,w
	movwf	(_EepSen3HiVal)
	line	697
	
l8332:	
;main.c: 697: if((EepSen1LoVal == 0xFF) || (EepSen1HiVal == 0xFF))
	movf	(_EepSen1LoVal),w
	xorlw	0FFh
	skipnz
	goto	u4531
	goto	u4530
u4531:
	goto	l8336
u4530:
	
l8334:	
	movf	(_EepSen1HiVal),w
	xorlw	0FFh
	skipz
	goto	u4541
	goto	u4540
u4541:
	goto	l2796
u4540:
	goto	l8336
	
l2794:	
	line	700
	
l8336:	
;main.c: 699: {
;main.c: 700: timer_set(PULSE_OUT_TIMER,(50));
	movlw	low(032h)
	movwf	(?_timer_set)
	movlw	high(032h)
	movwf	((?_timer_set))+1
	movlw	(0)
	fcall	_timer_set
	line	701
	
l8338:	
;main.c: 701: HH_PROCESS_STATE_MACHINE = EEPROM_SENSOR_CALIBRATION;
	movlw	(05h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_AreSensorsCaled+0)+0
	movf	(??_AreSensorsCaled+0)+0,w
	movwf	(_HH_PROCESS_STATE_MACHINE)
	line	703
;main.c: 703: }
	goto	l2796
	line	704
	
l2792:	
	goto	l2796
	line	707
;main.c: 704: else
;main.c: 705: {
	
l2795:	
	line	709
	
l2796:	
	return
	opt stack 0
GLOBAL	__end_of_AreSensorsCaled
	__end_of_AreSensorsCaled:
;; =============== function _AreSensorsCaled ends ============

	signat	_AreSensorsCaled,88
	global	_eeprom_write_block
psect	text867,local,class=CODE,delta=2
global __ptext867
__ptext867:

;; *************** function _eeprom_write_block *****************
;; Defined at:
;;		line 28 in file "F:\pic_projects\Hitch_Helper\eeprom.c"
;; Parameters:    Size  Location     Type
;;  address         1    wreg     unsigned char 
;;  size            1    3[BANK0 ] unsigned char 
;;  buffer          1    4[BANK0 ] PTR unsigned char 
;;		 -> default_sn(4), tom_sn1(4), 
;; Auto vars:     Size  Location     Type
;;  address         1    7[BANK0 ] unsigned char 
;;  i               1    6[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       2       0       0       0
;;      Locals:         0       2       0       0       0
;;      Temps:          0       1       0       0       0
;;      Totals:         0       5       0       0       0
;;Total ram usage:        5 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		_eeprom_write
;; This function is called by:
;;		_main
;;		_read_serial_ver
;; This function uses a non-reentrant model
;;
psect	text867
	file	"F:\pic_projects\Hitch_Helper\eeprom.c"
	line	28
	global	__size_of_eeprom_write_block
	__size_of_eeprom_write_block	equ	__end_of_eeprom_write_block-_eeprom_write_block
	
_eeprom_write_block:	
	opt	stack 3
; Regs used in _eeprom_write_block: [wreg-fsr0h+status,2+status,0+pclath+cstack]
;eeprom_write_block@address stored from wreg
	line	32
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(eeprom_write_block@address)
	
l8316:	
;eeprom.c: 30: uint8_t i;
;eeprom.c: 32: if (size <= (16))
	movlw	(011h)
	subwf	(eeprom_write_block@size),w
	skipnc
	goto	u4511
	goto	u4510
u4511:
	goto	l4549
u4510:
	line	34
	
l8318:	
;eeprom.c: 33: {
;eeprom.c: 34: for(i=0;i<=size;i++)
	clrf	(eeprom_write_block@i)
	goto	l8328
	line	35
	
l4546:	
	line	36
	
l8320:	
;eeprom.c: 35: {
;eeprom.c: 36: eeprom_write(address, * buffer);
	movf	(eeprom_write_block@buffer),w
	movwf	fsr0
	fcall	stringdir
	movwf	(??_eeprom_write_block+0)+0
	movf	(??_eeprom_write_block+0)+0,w
	movwf	(?_eeprom_write)
	movf	(eeprom_write_block@address),w
	fcall	_eeprom_write
	line	37
	
l8322:	
;eeprom.c: 37: buffer++;
	movlw	(01h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_eeprom_write_block+0)+0
	movf	(??_eeprom_write_block+0)+0,w
	addwf	(eeprom_write_block@buffer),f
	line	38
	
l8324:	
;eeprom.c: 38: address = address + 1;
	movf	(eeprom_write_block@address),w
	addlw	01h
	movwf	(??_eeprom_write_block+0)+0
	movf	(??_eeprom_write_block+0)+0,w
	movwf	(eeprom_write_block@address)
	line	34
	
l8326:	
	movlw	(01h)
	movwf	(??_eeprom_write_block+0)+0
	movf	(??_eeprom_write_block+0)+0,w
	addwf	(eeprom_write_block@i),f
	goto	l8328
	
l4545:	
	
l8328:	
	movf	(eeprom_write_block@i),w
	subwf	(eeprom_write_block@size),w
	skipnc
	goto	u4521
	goto	u4520
u4521:
	goto	l8320
u4520:
	goto	l4549
	
l4547:	
	line	40
;eeprom.c: 39: }
;eeprom.c: 40: }
	goto	l4549
	line	41
	
l4544:	
	goto	l4549
	line	44
;eeprom.c: 41: else
;eeprom.c: 42: {
	
l4548:	
	line	45
	
l4549:	
	return
	opt stack 0
GLOBAL	__end_of_eeprom_write_block
	__end_of_eeprom_write_block:
;; =============== function _eeprom_write_block ends ============

	signat	_eeprom_write_block,12408
	global	_init_micro
psect	text868,local,class=CODE,delta=2
global __ptext868
__ptext868:

;; *************** function _init_micro *****************
;; Defined at:
;;		line 662 in file "F:\pic_projects\Hitch_Helper\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		_uart_init
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text868
	file	"F:\pic_projects\Hitch_Helper\main.c"
	line	662
	global	__size_of_init_micro
	__size_of_init_micro	equ	__end_of_init_micro-_init_micro
	
_init_micro:	
	opt	stack 4
; Regs used in _init_micro: [wreg+status,2+status,0+pclath+cstack]
	line	663
	
l8296:	
;main.c: 663: OSCCON = 0x71;
	movlw	(071h)
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movwf	(143)^080h	;volatile
	line	664
;main.c: 664: WDTCON = 0x15;
	movlw	(015h)
	bcf	status, 5	;RP0=0, select bank2
	bsf	status, 6	;RP1=1, select bank2
	movwf	(261)^0100h	;volatile
	line	665
;main.c: 665: OPTION = 0x08;
	movlw	(08h)
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movwf	(129)^080h	;volatile
	line	666
	
l8298:	
;main.c: 666: ANSEL = 0x00;
	bsf	status, 5	;RP0=1, select bank3
	bsf	status, 6	;RP1=1, select bank3
	clrf	(392)^0180h	;volatile
	line	667
	
l8300:	
;main.c: 667: ANSELH = 0x00;
	clrf	(393)^0180h	;volatile
	line	668
	
l8302:	
;main.c: 668: TRISA = 0x00;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	clrf	(133)^080h	;volatile
	line	669
	
l8304:	
;main.c: 669: PORTA = 0x00;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(5)	;volatile
	line	670
	
l8306:	
;main.c: 670: TRISB = 0x00;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	clrf	(134)^080h	;volatile
	line	671
	
l8308:	
;main.c: 671: TRISC = 0x06;
	movlw	(06h)
	movwf	(135)^080h	;volatile
	line	672
;main.c: 672: TRISD = 0x00;
	clrf	(136)^080h	;volatile
	line	673
;main.c: 673: PORTD = 0x00;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(8)	;volatile
	line	674
	
l8310:	
;main.c: 674: T1CON = 0x31;
	movlw	(031h)
	movwf	(16)	;volatile
	line	675
	
l8312:	
;main.c: 675: TRISE = 0x01;
	movlw	(01h)
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movwf	(137)^080h	;volatile
	line	676
	
l8314:	
;main.c: 676: uart_init();
	fcall	_uart_init
	line	677
	
l2789:	
	return
	opt stack 0
GLOBAL	__end_of_init_micro
	__end_of_init_micro:
;; =============== function _init_micro ends ============

	signat	_init_micro,88
	global	_eeprom_write
psect	text869,local,class=CODE,delta=2
global __ptext869
__ptext869:

;; *************** function _eeprom_write *****************
;; Defined at:
;;		line 8 in file "C:\Program Files (x86)\HI-TECH Software\PICC\9.81\sources\eewrite.c"
;; Parameters:    Size  Location     Type
;;  addr            1    wreg     unsigned char 
;;  value           1    0[BANK0 ] unsigned char 
;; Auto vars:     Size  Location     Type
;;  addr            1    2[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       1       0       0       0
;;      Locals:         0       1       0       0       0
;;      Temps:          0       1       0       0       0
;;      Totals:         0       3       0       0       0
;;Total ram usage:        3 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_eepSens1WriteCal
;;		_eepSens2WriteCal
;;		_eepSens3WriteCal
;;		_eeprom_write_block
;;		_eep_default
;; This function uses a non-reentrant model
;;
psect	text869
	file	"C:\Program Files (x86)\HI-TECH Software\PICC\9.81\sources\eewrite.c"
	line	8
	global	__size_of_eeprom_write
	__size_of_eeprom_write	equ	__end_of_eeprom_write-_eeprom_write
	
_eeprom_write:	
	opt	stack 4
; Regs used in _eeprom_write: [wreg+status,2+status,0]
;eeprom_write@addr stored from wreg
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(eeprom_write@addr)
	line	9
	
l5482:	
	goto	l5483
	
l5484:	
	
l5483:	
	bsf	status, 5	;RP0=1, select bank3
	bsf	status, 6	;RP1=1, select bank3
	btfsc	(3169/8)^0180h,(3169)&7
	goto	u4431
	goto	u4430
u4431:
	goto	l5483
u4430:
	goto	l8246
	
l5485:	
	
l8246:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(eeprom_write@addr),w
	bcf	status, 5	;RP0=0, select bank2
	bsf	status, 6	;RP1=1, select bank2
	movwf	(269)^0100h	;volatile
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(eeprom_write@value),w
	bcf	status, 5	;RP0=0, select bank2
	bsf	status, 6	;RP1=1, select bank2
	movwf	(268)^0100h	;volatile
	
l8248:	
	movlw	(03Fh)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_eeprom_write+0)+0
	movf	(??_eeprom_write+0)+0,w
	bsf	status, 5	;RP0=1, select bank3
	bsf	status, 6	;RP1=1, select bank3
	andwf	(396)^0180h,f	;volatile
	
l8250:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	bcf	(24/8),(24)&7
	
l8252:	
	btfss	(95/8),(95)&7
	goto	u4441
	goto	u4440
u4441:
	goto	l5486
u4440:
	
l8254:	
	bsf	(24/8),(24)&7
	
l5486:	
	bcf	(95/8),(95)&7
	bsf	status, 5	;RP0=1, select bank3
	bsf	status, 6	;RP1=1, select bank3
	bsf	(3170/8)^0180h,(3170)&7
	
l8256:	
	movlw	(055h)
	movwf	(397)^0180h	;volatile
	movlw	(0AAh)
	movwf	(397)^0180h	;volatile
	
l8258:	
	bsf	(3169/8)^0180h,(3169)&7
	
l8260:	
	bcf	(3170/8)^0180h,(3170)&7
	
l8262:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfss	(24/8),(24)&7
	goto	u4451
	goto	u4450
u4451:
	goto	l5489
u4450:
	
l8264:	
	bsf	(95/8),(95)&7
	goto	l5489
	
l5487:	
	goto	l5489
	
l5488:	
	line	10
	
l5489:	
	return
	opt stack 0
GLOBAL	__end_of_eeprom_write
	__end_of_eeprom_write:
;; =============== function _eeprom_write ends ============

	signat	_eeprom_write,8312
	global	_eeprom_read
psect	text870,local,class=CODE,delta=2
global __ptext870
__ptext870:

;; *************** function _eeprom_read *****************
;; Defined at:
;;		line 8 in file "eeread.c"
;; Parameters:    Size  Location     Type
;;  addr            1    wreg     unsigned char 
;; Auto vars:     Size  Location     Type
;;  addr            1    1[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       1       0       0       0
;;      Temps:          0       1       0       0       0
;;      Totals:         0       2       0       0       0
;;Total ram usage:        2 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_AreSensorsCaled
;;		_AreCalCopyCorrect
;;		_eeprom_read_block1
;;		_eeprom_read_block
;; This function uses a non-reentrant model
;;
psect	text870
	file	"C:\Program Files (x86)\HI-TECH Software\PICC\9.81\sources\eeread.c"
	line	8
	global	__size_of_eeprom_read
	__size_of_eeprom_read	equ	__end_of_eeprom_read-_eeprom_read
	
_eeprom_read:	
	opt	stack 4
; Regs used in _eeprom_read: [wreg+status,2+status,0]
;eeprom_read@addr stored from wreg
	line	10
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(eeprom_read@addr)
	line	9
	
l5461:	
	line	10
# 10 "C:\Program Files (x86)\HI-TECH Software\PICC\9.81\sources\eeread.c"
clrwdt ;#
psect	text870
	line	11
	bsf	status, 5	;RP0=1, select bank3
	bsf	status, 6	;RP1=1, select bank3
	btfsc	(3169/8)^0180h,(3169)&7
	goto	u4421
	goto	u4420
u4421:
	goto	l5461
u4420:
	goto	l8242
	
l5462:	
	line	12
	
l8242:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(eeprom_read@addr),w
	bcf	status, 5	;RP0=0, select bank2
	bsf	status, 6	;RP1=1, select bank2
	movwf	(269)^0100h	;volatile
	movlw	(03Fh)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_eeprom_read+0)+0
	movf	(??_eeprom_read+0)+0,w
	bsf	status, 5	;RP0=1, select bank3
	bsf	status, 6	;RP1=1, select bank3
	andwf	(396)^0180h,f	;volatile
	bsf	(3168/8)^0180h,(3168)&7
	clrc
	btfsc	(3168/8)^0180h,(3168)&7
	setc
	movlw	0
	skipnc
	movlw	1

	bcf	status, 5	;RP0=0, select bank2
	bsf	status, 6	;RP1=1, select bank2
	movf	(268)^0100h,w	;volatile
	goto	l5463
	
l8244:	
	line	13
	
l5463:	
	return
	opt stack 0
GLOBAL	__end_of_eeprom_read
	__end_of_eeprom_read:
;; =============== function _eeprom_read ends ============

	signat	_eeprom_read,4217
	global	_uart_init
psect	text871,local,class=CODE,delta=2
global __ptext871
__ptext871:

;; *************** function _uart_init *****************
;; Defined at:
;;		line 148 in file "F:\pic_projects\Hitch_Helper\uart_func.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_init_micro
;;		_uart_rx
;; This function uses a non-reentrant model
;;
psect	text871
	file	"F:\pic_projects\Hitch_Helper\uart_func.c"
	line	148
	global	__size_of_uart_init
	__size_of_uart_init	equ	__end_of_uart_init-_uart_init
	
_uart_init:	
	opt	stack 4
; Regs used in _uart_init: [wreg]
	line	154
	
l8240:	
;uart_func.c: 154: SPBRG = 0x06;
	movlw	(06h)
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movwf	(153)^080h	;volatile
	line	155
;uart_func.c: 155: TXSTA = 0x24;
	movlw	(024h)
	movwf	(152)^080h	;volatile
	line	156
;uart_func.c: 156: RCSTA = 0x90;
	movlw	(090h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(24)	;volatile
	line	157
	
l5448:	
	return
	opt stack 0
GLOBAL	__end_of_uart_init
	__end_of_uart_init:
;; =============== function _uart_init ends ============

	signat	_uart_init,88
	global	_get_timer
psect	text872,local,class=CODE,delta=2
global __ptext872
__ptext872:

;; *************** function _get_timer *****************
;; Defined at:
;;		line 131 in file "F:\pic_projects\Hitch_Helper\timer.c"
;; Parameters:    Size  Location     Type
;;  index           1    wreg     unsigned char 
;; Auto vars:     Size  Location     Type
;;  index           1    5[BANK0 ] unsigned char 
;;  result          2    3[BANK0 ] unsigned short 
;; Return value:  Size  Location     Type
;;                  2    0[BANK0 ] unsigned short 
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       2       0       0       0
;;      Locals:         0       3       0       0       0
;;      Temps:          0       1       0       0       0
;;      Totals:         0       6       0       0       0
;;Total ram usage:        6 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text872
	file	"F:\pic_projects\Hitch_Helper\timer.c"
	line	131
	global	__size_of_get_timer
	__size_of_get_timer	equ	__end_of_get_timer-_get_timer
	
_get_timer:	
	opt	stack 5
; Regs used in _get_timer: [wreg-fsr0h+status,2+status,0]
;get_timer@index stored from wreg
	line	134
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(get_timer@index)
	
l8226:	
;timer.c: 132: uint16_t result;
;timer.c: 134: if (index < TIMER_MAX)
	movf	(get_timer@index),f
	skipz
	goto	u4411
	goto	u4410
u4411:
	goto	l8234
u4410:
	line	136
	
l8228:	
;timer.c: 135: {
;timer.c: 136: (GIE = 0);
	bcf	(95/8),(95)&7
	line	137
	
l8230:	
;timer.c: 137: result = timer_array[index];
	movf	(get_timer@index),w
	movwf	(??_get_timer+0)+0
	addwf	(??_get_timer+0)+0,w
	addlw	_timer_array&0ffh
	movwf	fsr0
	movf	indf,w
	movwf	(get_timer@result)
	incf	fsr0,f
	movf	indf,w
	movwf	(get_timer@result+1)
	line	138
	
l8232:	
;timer.c: 138: (GIE = 1);
	bsf	(95/8),(95)&7
	line	139
;timer.c: 139: }
	goto	l8236
	line	140
	
l3680:	
	line	142
	
l8234:	
;timer.c: 140: else
;timer.c: 141: {
;timer.c: 142: result = 0;
	clrf	(get_timer@result)
	clrf	(get_timer@result+1)
	goto	l8236
	line	143
	
l3681:	
	line	145
	
l8236:	
;timer.c: 143: }
;timer.c: 145: return result;
	movf	(get_timer@result+1),w
	clrf	(?_get_timer+1)
	addwf	(?_get_timer+1)
	movf	(get_timer@result),w
	clrf	(?_get_timer)
	addwf	(?_get_timer)

	goto	l3682
	
l8238:	
	line	147
	
l3682:	
	return
	opt stack 0
GLOBAL	__end_of_get_timer
	__end_of_get_timer:
;; =============== function _get_timer ends ============

	signat	_get_timer,4218
	global	_timer_set
psect	text873,local,class=CODE,delta=2
global __ptext873
__ptext873:

;; *************** function _timer_set *****************
;; Defined at:
;;		line 116 in file "F:\pic_projects\Hitch_Helper\timer.c"
;; Parameters:    Size  Location     Type
;;  index           1    wreg     unsigned char 
;;  value           2    0[BANK0 ] unsigned short 
;; Auto vars:     Size  Location     Type
;;  index           1    3[BANK0 ] unsigned char 
;;  array_conten    2    0        unsigned short 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       2       0       0       0
;;      Locals:         0       1       0       0       0
;;      Temps:          0       1       0       0       0
;;      Totals:         0       4       0       0       0
;;Total ram usage:        4 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;;		_AreSensorsCaled
;;		_AreCalCopyCorrect
;;		_RecalSensors
;; This function uses a non-reentrant model
;;
psect	text873
	file	"F:\pic_projects\Hitch_Helper\timer.c"
	line	116
	global	__size_of_timer_set
	__size_of_timer_set	equ	__end_of_timer_set-_timer_set
	
_timer_set:	
	opt	stack 4
; Regs used in _timer_set: [wreg-fsr0h+status,2+status,0]
;timer_set@index stored from wreg
	line	118
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(timer_set@index)
	
l8218:	
;timer.c: 117: uint16_t array_contents;
;timer.c: 118: if (index < TIMER_MAX)
	movf	(timer_set@index),f
	skipz
	goto	u4401
	goto	u4400
u4401:
	goto	l3677
u4400:
	line	120
	
l8220:	
;timer.c: 119: {
;timer.c: 120: (GIE = 0);
	bcf	(95/8),(95)&7
	line	121
	
l8222:	
;timer.c: 121: timer_array[index] = value;
	movf	(timer_set@index),w
	movwf	(??_timer_set+0)+0
	addwf	(??_timer_set+0)+0,w
	addlw	_timer_array&0ffh
	movwf	fsr0
	movf	(timer_set@value),w
	movwf	indf
	incf	fsr0,f
	movf	(timer_set@value+1),w
	movwf	indf
	line	122
	
l8224:	
;timer.c: 122: (GIE = 1);
	bsf	(95/8),(95)&7
	line	123
;timer.c: 123: }
	goto	l3677
	line	124
	
l3675:	
	goto	l3677
	line	126
;timer.c: 124: else
;timer.c: 125: {
	
l3676:	
	line	127
	
l3677:	
	return
	opt stack 0
GLOBAL	__end_of_timer_set
	__end_of_timer_set:
;; =============== function _timer_set ends ============

	signat	_timer_set,8312
	global	_timer_init
psect	text874,local,class=CODE,delta=2
global __ptext874
__ptext874:

;; *************** function _timer_init *****************
;; Defined at:
;;		line 191 in file "F:\pic_projects\Hitch_Helper\timer.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  i               1    1[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       1       0       0       0
;;      Temps:          0       1       0       0       0
;;      Totals:         0       2       0       0       0
;;Total ram usage:        2 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text874
	file	"F:\pic_projects\Hitch_Helper\timer.c"
	line	191
	global	__size_of_timer_init
	__size_of_timer_init	equ	__end_of_timer_init-_timer_init
	
_timer_init:	
	opt	stack 5
; Regs used in _timer_init: [wreg-fsr0h+status,2+status,0]
	line	193
	
l8206:	
;timer.c: 192: uint8_t i;
;timer.c: 193: for (i = 0; i < TIMER_MAX; i++)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(timer_init@i)
	
l8208:	
	movf	(timer_init@i),w
	skipz
	goto	u4380
	goto	l8212
u4380:
	goto	l3698
	
l8210:	
	goto	l3698
	line	194
	
l3696:	
	line	195
	
l8212:	
;timer.c: 194: {
;timer.c: 195: timer_array[i]=0;
	movf	(timer_init@i),w
	movwf	(??_timer_init+0)+0
	addwf	(??_timer_init+0)+0,w
	addlw	_timer_array&0ffh
	movwf	fsr0
	movlw	low(0)
	movwf	indf
	incf	fsr0,f
	movlw	high(0)
	movwf	indf
	line	193
	
l8214:	
	movlw	(01h)
	movwf	(??_timer_init+0)+0
	movf	(??_timer_init+0)+0,w
	addwf	(timer_init@i),f
	
l8216:	
	movf	(timer_init@i),w
	skipz
	goto	u4390
	goto	l8212
u4390:
	goto	l3698
	
l3697:	
	line	197
	
l3698:	
	return
	opt stack 0
GLOBAL	__end_of_timer_init
	__end_of_timer_init:
;; =============== function _timer_init ends ============

	signat	_timer_init,88
	global	_capture_state3
psect	text875,local,class=CODE,delta=2
global __ptext875
__ptext875:

;; *************** function _capture_state3 *****************
;; Defined at:
;;		line 149 in file "F:\pic_projects\Hitch_Helper\capture.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       1       0       0       0
;;      Totals:         0       1       0       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text875
	file	"F:\pic_projects\Hitch_Helper\capture.c"
	line	149
	global	__size_of_capture_state3
	__size_of_capture_state3	equ	__end_of_capture_state3-_capture_state3
	
_capture_state3:	
	opt	stack 5
; Regs used in _capture_state3: [wreg-fsr0h+status,2+status,0]
	line	152
	
l8166:	
;capture.c: 150: static uint8_t CAPTURE_STATE;
;capture.c: 152: CAPTURE_STATE = CCP1CON;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(23),w	;volatile
	movwf	(??_capture_state3+0)+0
	movf	(??_capture_state3+0)+0,w
	movwf	(capture_state3@CAPTURE_STATE)
	line	154
;capture.c: 154: switch(CAPTURE_STATE)
	goto	l8204
	line	156
;capture.c: 155: {
;capture.c: 156: case(RISING_EDGE_CCP1):
	
l894:	
	line	157
;capture.c: 157: capture_isr_flag_ccp1 = FALSE;
	clrf	(_capture_isr_flag_ccp1)
	line	158
;capture.c: 158: while(capture_isr_flag_ccp1 == FALSE)
	goto	l8168
	
l896:	
	goto	l8168
	line	159
;capture.c: 159: {}
	
l895:	
	line	158
	
l8168:	
	movf	(_capture_isr_flag_ccp1),w
	skipz
	goto	u4360
	goto	l8168
u4360:
	
l897:	
	line	160
;capture.c: 160: CCP1IE = FALSE;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bcf	(1122/8)^080h,(1122)&7
	line	161
	
l8170:	
;capture.c: 161: CCP1CON = MODULE_CCP1_OFF;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(23)	;volatile
	line	162
	
l8172:	
;capture.c: 162: PulseWidth3Counter = 0;
	clrf	(_PulseWidth3Counter)	;volatile
	line	164
	
l8174:	
;capture.c: 164: T1CON &= ~(0x01);
	movlw	(0FEh)
	movwf	(??_capture_state3+0)+0
	movf	(??_capture_state3+0)+0,w
	andwf	(16),f	;volatile
	line	165
	
l8176:	
;capture.c: 165: TMR1L = 0xF0;
	movlw	(0F0h)
	movwf	(14)	;volatile
	line	166
	
l8178:	
;capture.c: 166: TMR1H = 0xFF;
	movlw	(0FFh)
	movwf	(15)	;volatile
	line	167
	
l8180:	
;capture.c: 167: T1CON |= 0x01;
	bsf	(16)+(0/8),(0)&7	;volatile
	line	168
	
l8182:	
;capture.c: 168: CCP1CON = FALLING_EDGE_CCP1;
	movlw	(04h)
	movwf	(23)	;volatile
	line	169
	
l8184:	
;capture.c: 169: CCP1IE = TRUE;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bsf	(1122/8)^080h,(1122)&7
	line	170
	
l8186:	
;capture.c: 170: ProcessingCaptureModule = FALSE;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(_ProcessingCaptureModule)
	line	171
;capture.c: 171: break;
	goto	l904
	line	173
;capture.c: 173: case(FALLING_EDGE_CCP1):
	
l899:	
	line	174
;capture.c: 174: capture_isr_flag_ccp1 = FALSE;
	clrf	(_capture_isr_flag_ccp1)
	line	175
;capture.c: 175: while(capture_isr_flag_ccp1 == FALSE)
	goto	l8188
	
l901:	
	goto	l8188
	line	176
;capture.c: 176: {}
	
l900:	
	line	175
	
l8188:	
	movf	(_capture_isr_flag_ccp1),w
	skipz
	goto	u4370
	goto	l8188
u4370:
	goto	l8190
	
l902:	
	line	177
	
l8190:	
;capture.c: 177: PulseWidth3CurrentCount = PulseWidth3Counter;
	movf	(_PulseWidth3Counter),w	;volatile
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_capture_state3+0)+0
	movf	(??_capture_state3+0)+0,w
	movwf	(_PulseWidth3CurrentCount)
	line	178
	
l8192:	
;capture.c: 178: T1CON &= ~(0x01);
	movlw	(0FEh)
	movwf	(??_capture_state3+0)+0
	movf	(??_capture_state3+0)+0,w
	andwf	(16),f	;volatile
	line	179
	
l8194:	
;capture.c: 179: TMR1L = 0x00;
	clrf	(14)	;volatile
	line	180
	
l8196:	
;capture.c: 180: TMR1H = 0x00;
	clrf	(15)	;volatile
	line	181
	
l8198:	
;capture.c: 181: T1CON |= 0x01;
	bsf	(16)+(0/8),(0)&7	;volatile
	line	182
	
l8200:	
;capture.c: 182: PulseWidth3Counter = 0;
	clrf	(_PulseWidth3Counter)	;volatile
	line	183
;capture.c: 183: ProcessingCaptureModule = TRUE;
	clrf	(_ProcessingCaptureModule)
	bsf	status,0
	rlf	(_ProcessingCaptureModule),f
	line	184
;capture.c: 184: break;
	goto	l904
	line	187
;capture.c: 187: default:
	
l903:	
	line	188
;capture.c: 188: break;
	goto	l904
	line	189
	
l8202:	
;capture.c: 189: }
	goto	l904
	line	154
	
l893:	
	
l8204:	
	movf	(capture_state3@CAPTURE_STATE),w
	; Switch size 1, requested type "space"
; Number of cases is 2, Range of values is 4 to 5
; switch strategies available:
; Name         Instructions Cycles
; simple_byte            7     4 (average)
; direct_byte           17    11 (fixed)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	opt asmopt_off
	xorlw	4^0	; case 4
	skipnz
	goto	l899
	xorlw	5^4	; case 5
	skipnz
	goto	l894
	goto	l904
	opt asmopt_on

	line	189
	
l898:	
	line	190
	
l904:	
	return
	opt stack 0
GLOBAL	__end_of_capture_state3
	__end_of_capture_state3:
;; =============== function _capture_state3 ends ============

	signat	_capture_state3,88
	global	_capture_state2
psect	text876,local,class=CODE,delta=2
global __ptext876
__ptext876:

;; *************** function _capture_state2 *****************
;; Defined at:
;;		line 92 in file "F:\pic_projects\Hitch_Helper\capture.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       1       0       0       0
;;      Totals:         0       1       0       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text876
	file	"F:\pic_projects\Hitch_Helper\capture.c"
	line	92
	global	__size_of_capture_state2
	__size_of_capture_state2	equ	__end_of_capture_state2-_capture_state2
	
_capture_state2:	
	opt	stack 5
; Regs used in _capture_state2: [wreg-fsr0h+status,2+status,0]
	line	95
	
l8126:	
;capture.c: 93: static uint8_t CAPTURE_STATE;
;capture.c: 95: CAPTURE_STATE = CCP1CON;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(23),w	;volatile
	movwf	(??_capture_state2+0)+0
	movf	(??_capture_state2+0)+0,w
	movwf	(capture_state2@CAPTURE_STATE)
	line	97
;capture.c: 97: switch(CAPTURE_STATE)
	goto	l8164
	line	99
;capture.c: 98: {
;capture.c: 99: case(RISING_EDGE_CCP1):
	
l878:	
	line	100
;capture.c: 100: capture_isr_flag_ccp1 = FALSE;
	clrf	(_capture_isr_flag_ccp1)
	line	101
;capture.c: 101: while(capture_isr_flag_ccp1 == FALSE)
	goto	l8128
	
l880:	
	goto	l8128
	line	102
;capture.c: 102: {}
	
l879:	
	line	101
	
l8128:	
	movf	(_capture_isr_flag_ccp1),w
	skipz
	goto	u4340
	goto	l8128
u4340:
	
l881:	
	line	103
;capture.c: 103: CCP1IE = FALSE;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bcf	(1122/8)^080h,(1122)&7
	line	104
	
l8130:	
;capture.c: 104: CCP1CON = MODULE_CCP1_OFF;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(23)	;volatile
	line	105
	
l8132:	
;capture.c: 105: PulseWidth2Counter = 0;
	clrf	(_PulseWidth2Counter)	;volatile
	line	107
	
l8134:	
;capture.c: 107: T1CON &= ~(0x01);
	movlw	(0FEh)
	movwf	(??_capture_state2+0)+0
	movf	(??_capture_state2+0)+0,w
	andwf	(16),f	;volatile
	line	108
	
l8136:	
;capture.c: 108: TMR1L = 0xF0;
	movlw	(0F0h)
	movwf	(14)	;volatile
	line	109
	
l8138:	
;capture.c: 109: TMR1H = 0xFF;
	movlw	(0FFh)
	movwf	(15)	;volatile
	line	110
	
l8140:	
;capture.c: 110: T1CON |= 0x01;
	bsf	(16)+(0/8),(0)&7	;volatile
	line	111
	
l8142:	
;capture.c: 111: CCP1CON = FALLING_EDGE_CCP1;
	movlw	(04h)
	movwf	(23)	;volatile
	line	112
	
l8144:	
;capture.c: 112: CCP1IE = TRUE;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bsf	(1122/8)^080h,(1122)&7
	line	113
	
l8146:	
;capture.c: 113: ProcessingCaptureModule = FALSE;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(_ProcessingCaptureModule)
	line	114
;capture.c: 114: break;
	goto	l888
	line	116
;capture.c: 116: case(FALLING_EDGE_CCP1):
	
l883:	
	line	117
;capture.c: 117: capture_isr_flag_ccp1 = FALSE;
	clrf	(_capture_isr_flag_ccp1)
	line	118
;capture.c: 118: while(capture_isr_flag_ccp1 == FALSE)
	goto	l8148
	
l885:	
	goto	l8148
	line	119
;capture.c: 119: {}
	
l884:	
	line	118
	
l8148:	
	movf	(_capture_isr_flag_ccp1),w
	skipz
	goto	u4350
	goto	l8148
u4350:
	goto	l8150
	
l886:	
	line	120
	
l8150:	
;capture.c: 120: PulseWidth2CurrentCount = PulseWidth2Counter;
	movf	(_PulseWidth2Counter),w	;volatile
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_capture_state2+0)+0
	movf	(??_capture_state2+0)+0,w
	movwf	(_PulseWidth2CurrentCount)
	line	121
	
l8152:	
;capture.c: 121: T1CON &= ~(0x01);
	movlw	(0FEh)
	movwf	(??_capture_state2+0)+0
	movf	(??_capture_state2+0)+0,w
	andwf	(16),f	;volatile
	line	122
	
l8154:	
;capture.c: 122: TMR1L = 0x00;
	clrf	(14)	;volatile
	line	123
	
l8156:	
;capture.c: 123: TMR1H = 0x00;
	clrf	(15)	;volatile
	line	124
	
l8158:	
;capture.c: 124: T1CON |= 0x01;
	bsf	(16)+(0/8),(0)&7	;volatile
	line	125
	
l8160:	
;capture.c: 125: PulseWidth2Counter = 0;
	clrf	(_PulseWidth2Counter)	;volatile
	line	126
;capture.c: 126: ProcessingCaptureModule = TRUE;
	clrf	(_ProcessingCaptureModule)
	bsf	status,0
	rlf	(_ProcessingCaptureModule),f
	line	127
;capture.c: 127: break;
	goto	l888
	line	130
;capture.c: 130: default:
	
l887:	
	line	131
;capture.c: 131: break;
	goto	l888
	line	132
	
l8162:	
;capture.c: 132: }
	goto	l888
	line	97
	
l877:	
	
l8164:	
	movf	(capture_state2@CAPTURE_STATE),w
	; Switch size 1, requested type "space"
; Number of cases is 2, Range of values is 4 to 5
; switch strategies available:
; Name         Instructions Cycles
; simple_byte            7     4 (average)
; direct_byte           17    11 (fixed)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	opt asmopt_off
	xorlw	4^0	; case 4
	skipnz
	goto	l883
	xorlw	5^4	; case 5
	skipnz
	goto	l878
	goto	l888
	opt asmopt_on

	line	132
	
l882:	
	line	133
	
l888:	
	return
	opt stack 0
GLOBAL	__end_of_capture_state2
	__end_of_capture_state2:
;; =============== function _capture_state2 ends ============

	signat	_capture_state2,88
	global	_capture_state1
psect	text877,local,class=CODE,delta=2
global __ptext877
__ptext877:

;; *************** function _capture_state1 *****************
;; Defined at:
;;		line 34 in file "F:\pic_projects\Hitch_Helper\capture.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       1       0       0       0
;;      Totals:         0       1       0       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text877
	file	"F:\pic_projects\Hitch_Helper\capture.c"
	line	34
	global	__size_of_capture_state1
	__size_of_capture_state1	equ	__end_of_capture_state1-_capture_state1
	
_capture_state1:	
	opt	stack 5
; Regs used in _capture_state1: [wreg-fsr0h+status,2+status,0]
	line	37
	
l8086:	
;capture.c: 35: static uint8_t CAPTURE_STATE;
;capture.c: 37: CAPTURE_STATE = CCP2CON;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(29),w	;volatile
	movwf	(??_capture_state1+0)+0
	movf	(??_capture_state1+0)+0,w
	movwf	(capture_state1@CAPTURE_STATE)
	line	39
;capture.c: 39: switch(CAPTURE_STATE)
	goto	l8124
	line	41
;capture.c: 40: {
;capture.c: 41: case(RISING_EDGE_CCP2):
	
l862:	
	line	42
;capture.c: 42: capture_isr_flag_ccp2 = FALSE;
	clrf	(_capture_isr_flag_ccp2)
	line	43
;capture.c: 43: while(capture_isr_flag_ccp2 == FALSE)
	goto	l8088
	
l864:	
	goto	l8088
	line	44
;capture.c: 44: {}
	
l863:	
	line	43
	
l8088:	
	movf	(_capture_isr_flag_ccp2),w
	skipz
	goto	u4320
	goto	l8088
u4320:
	
l865:	
	line	45
;capture.c: 45: CCP2IE = FALSE;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bcf	(1128/8)^080h,(1128)&7
	line	46
	
l8090:	
;capture.c: 46: CCP2CON = MODULE_CCP2_OFF;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(29)	;volatile
	line	47
	
l8092:	
;capture.c: 47: PulseWidth1Counter = 0;
	clrf	(_PulseWidth1Counter)	;volatile
	line	49
	
l8094:	
;capture.c: 49: T1CON &= ~(0x01);
	movlw	(0FEh)
	movwf	(??_capture_state1+0)+0
	movf	(??_capture_state1+0)+0,w
	andwf	(16),f	;volatile
	line	50
	
l8096:	
;capture.c: 50: TMR1L = 0xF0;
	movlw	(0F0h)
	movwf	(14)	;volatile
	line	51
	
l8098:	
;capture.c: 51: TMR1H = 0xFF;
	movlw	(0FFh)
	movwf	(15)	;volatile
	line	52
	
l8100:	
;capture.c: 52: T1CON |= 0x01;
	bsf	(16)+(0/8),(0)&7	;volatile
	line	53
	
l8102:	
;capture.c: 53: CCP2CON = FALLING_EDGE_CCP2;
	movlw	(04h)
	movwf	(29)	;volatile
	line	54
	
l8104:	
;capture.c: 54: CCP2IE = TRUE;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bsf	(1128/8)^080h,(1128)&7
	line	55
	
l8106:	
;capture.c: 55: ProcessingCaptureModule = FALSE;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(_ProcessingCaptureModule)
	line	56
;capture.c: 56: break;
	goto	l872
	line	58
;capture.c: 58: case(FALLING_EDGE_CCP2):
	
l867:	
	line	59
;capture.c: 59: capture_isr_flag_ccp2 = FALSE;
	clrf	(_capture_isr_flag_ccp2)
	line	60
;capture.c: 60: while(capture_isr_flag_ccp2 == FALSE)
	goto	l8108
	
l869:	
	goto	l8108
	line	61
;capture.c: 61: {}
	
l868:	
	line	60
	
l8108:	
	movf	(_capture_isr_flag_ccp2),w
	skipz
	goto	u4330
	goto	l8108
u4330:
	goto	l8110
	
l870:	
	line	63
	
l8110:	
;capture.c: 63: PulseWidth1CurrentCount = PulseWidth1Counter;
	movf	(_PulseWidth1Counter),w	;volatile
	movwf	(??_capture_state1+0)+0
	movf	(??_capture_state1+0)+0,w
	movwf	(_PulseWidth1CurrentCount)
	line	64
	
l8112:	
;capture.c: 64: T1CON &= ~(0x01);
	movlw	(0FEh)
	movwf	(??_capture_state1+0)+0
	movf	(??_capture_state1+0)+0,w
	andwf	(16),f	;volatile
	line	65
	
l8114:	
;capture.c: 65: TMR1L = 0x00;
	clrf	(14)	;volatile
	line	66
	
l8116:	
;capture.c: 66: TMR1H = 0x00;
	clrf	(15)	;volatile
	line	67
	
l8118:	
;capture.c: 67: T1CON |= 0x01;
	bsf	(16)+(0/8),(0)&7	;volatile
	line	68
	
l8120:	
;capture.c: 68: PulseWidth1Counter = 0;
	clrf	(_PulseWidth1Counter)	;volatile
	line	69
;capture.c: 69: ProcessingCaptureModule = TRUE;
	clrf	(_ProcessingCaptureModule)
	bsf	status,0
	rlf	(_ProcessingCaptureModule),f
	line	70
;capture.c: 70: break;
	goto	l872
	line	73
;capture.c: 73: default:
	
l871:	
	line	74
;capture.c: 74: break;
	goto	l872
	line	75
	
l8122:	
;capture.c: 75: }
	goto	l872
	line	39
	
l861:	
	
l8124:	
	movf	(capture_state1@CAPTURE_STATE),w
	; Switch size 1, requested type "space"
; Number of cases is 2, Range of values is 4 to 5
; switch strategies available:
; Name         Instructions Cycles
; simple_byte            7     4 (average)
; direct_byte           17    11 (fixed)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	opt asmopt_off
	xorlw	4^0	; case 4
	skipnz
	goto	l867
	xorlw	5^4	; case 5
	skipnz
	goto	l862
	goto	l872
	opt asmopt_on

	line	75
	
l866:	
	line	76
	
l872:	
	return
	opt stack 0
GLOBAL	__end_of_capture_state1
	__end_of_capture_state1:
;; =============== function _capture_state1 ends ============

	signat	_capture_state1,88
	global	_interrupt_handler
psect	text878,local,class=CODE,delta=2
global __ptext878
__ptext878:

;; *************** function _interrupt_handler *****************
;; Defined at:
;;		line 7 in file "F:\pic_projects\Hitch_Helper\interrupt.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          4       0       0       0       0
;;      Totals:         4       0       0       0       0
;;Total ram usage:        4 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		_timer_isr
;;		_capture_isr
;; This function is called by:
;;		Interrupt level 1
;; This function uses a non-reentrant model
;;
psect	text878
	file	"F:\pic_projects\Hitch_Helper\interrupt.c"
	line	7
	global	__size_of_interrupt_handler
	__size_of_interrupt_handler	equ	__end_of_interrupt_handler-_interrupt_handler
	
_interrupt_handler:	
	opt	stack 3
; Regs used in _interrupt_handler: [wreg-fsr0h+status,2+status,0+pclath+cstack]
psect	intentry,class=CODE,delta=2
global __pintentry
__pintentry:
global interrupt_function
interrupt_function:
	global saved_w
	saved_w	set	btemp+0
	movwf	saved_w
	swapf	status,w
	movwf	(??_interrupt_handler+0)
	movf	fsr0,w
	movwf	(??_interrupt_handler+1)
	movf	pclath,w
	movwf	(??_interrupt_handler+2)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	btemp+1,w
	movwf	(??_interrupt_handler+3)
	ljmp	_interrupt_handler
psect	text878
	line	8
	
i1l7504:	
;interrupt.c: 8: timer_isr();
	fcall	_timer_isr
	line	9
	
i1l7506:	
;interrupt.c: 9: capture_isr();
	fcall	_capture_isr
	line	10
	
i1l1757:	
	movf	(??_interrupt_handler+3),w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	btemp+1
	movf	(??_interrupt_handler+2),w
	movwf	pclath
	movf	(??_interrupt_handler+1),w
	movwf	fsr0
	swapf	(??_interrupt_handler+0)^0FFFFFF80h,w
	movwf	status
	swapf	saved_w,f
	swapf	saved_w,w
	retfie
	opt stack 0
GLOBAL	__end_of_interrupt_handler
	__end_of_interrupt_handler:
;; =============== function _interrupt_handler ends ============

	signat	_interrupt_handler,88
	global	_timer_isr
psect	text879,local,class=CODE,delta=2
global __ptext879
__ptext879:

;; *************** function _timer_isr *****************
;; Defined at:
;;		line 152 in file "F:\pic_projects\Hitch_Helper\timer.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  i               1    1[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         1       0       0       0       0
;;      Temps:          1       0       0       0       0
;;      Totals:         2       0       0       0       0
;;Total ram usage:        2 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_interrupt_handler
;; This function uses a non-reentrant model
;;
psect	text879
	file	"F:\pic_projects\Hitch_Helper\timer.c"
	line	152
	global	__size_of_timer_isr
	__size_of_timer_isr	equ	__end_of_timer_isr-_timer_isr
	
_timer_isr:	
	opt	stack 3
; Regs used in _timer_isr: [wreg-fsr0h+status,2+status,0]
	line	154
	
i1l7508:	
;timer.c: 153: uint8_t i;
;timer.c: 154: if((TMR1IE)&&(TMR1IF))
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	btfss	(1120/8)^080h,(1120)&7
	goto	u336_21
	goto	u336_20
u336_21:
	goto	i1l3693
u336_20:
	
i1l7510:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfss	(96/8),(96)&7
	goto	u337_21
	goto	u337_20
u337_21:
	goto	i1l3693
u337_20:
	line	156
	
i1l7512:	
;timer.c: 155: {
;timer.c: 156: CCP1IE = FALSE;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bcf	(1122/8)^080h,(1122)&7
	line	157
;timer.c: 157: CCP2IE = FALSE;
	bcf	(1128/8)^080h,(1128)&7
	line	158
	
i1l7514:	
;timer.c: 158: PulseWidth1Counter++;
	movlw	(01h)
	movwf	(??_timer_isr+0)+0
	movf	(??_timer_isr+0)+0,w
	addwf	(_PulseWidth1Counter),f	;volatile
	line	159
;timer.c: 159: PulseWidth2Counter++;
	movlw	(01h)
	movwf	(??_timer_isr+0)+0
	movf	(??_timer_isr+0)+0,w
	addwf	(_PulseWidth2Counter),f	;volatile
	line	160
;timer.c: 160: PulseWidth3Counter++;
	movlw	(01h)
	movwf	(??_timer_isr+0)+0
	movf	(??_timer_isr+0)+0,w
	addwf	(_PulseWidth3Counter),f	;volatile
	line	161
	
i1l7516:	
;timer.c: 161: CCP1IE = TRUE;
	bsf	(1122/8)^080h,(1122)&7
	line	162
	
i1l7518:	
;timer.c: 162: CCP2IE = TRUE;
	bsf	(1128/8)^080h,(1128)&7
	line	163
	
i1l7520:	
;timer.c: 163: if(OneMillisecondTimerDisable == FALSE)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(_OneMillisecondTimerDisable),f
	skipz
	goto	u338_21
	goto	u338_20
u338_21:
	goto	i1l3687
u338_20:
	line	165
	
i1l7522:	
;timer.c: 164: {
;timer.c: 165: TMR1IF=0;
	bcf	(96/8),(96)&7
	line	166
	
i1l7524:	
;timer.c: 166: T1CON &= ~(0x01);
	movlw	(0FEh)
	movwf	(??_timer_isr+0)+0
	movf	(??_timer_isr+0)+0,w
	andwf	(16),f	;volatile
	line	167
	
i1l7526:	
;timer.c: 167: TMR1L = 0x0F;
	movlw	(0Fh)
	movwf	(14)	;volatile
	line	168
	
i1l7528:	
;timer.c: 168: TMR1H = 0xFF;
	movlw	(0FFh)
	movwf	(15)	;volatile
	line	169
	
i1l7530:	
;timer.c: 169: T1CON |= 0x01;
	bsf	(16)+(0/8),(0)&7	;volatile
	line	170
;timer.c: 170: }
	goto	i1l3687
	line	171
	
i1l3686:	
	line	172
;timer.c: 171: else{
	
i1l3687:	
	line	173
;timer.c: 172: }
;timer.c: 173: for (i = 0; i < TIMER_MAX; i++)
	clrf	(timer_isr@i)
	
i1l7532:	
	movf	(timer_isr@i),w
	skipz
	goto	u339_20
	goto	i1l7536
u339_20:
	goto	i1l3693
	
i1l7534:	
	goto	i1l3693
	line	174
	
i1l3688:	
	line	175
	
i1l7536:	
;timer.c: 174: {
;timer.c: 175: if (timer_array[i] != 0)
	movf	(timer_isr@i),w
	movwf	(??_timer_isr+0)+0
	addwf	(??_timer_isr+0)+0,w
	addlw	_timer_array&0ffh
	movwf	fsr0
	movf	indf,w
	incf	fsr0,f
	iorwf	indf,w
	skipnz
	goto	u340_21
	goto	u340_20
u340_21:
	goto	i1l7540
u340_20:
	line	177
	
i1l7538:	
;timer.c: 176: {
;timer.c: 177: timer_array[i]--;
	movf	(timer_isr@i),w
	movwf	(??_timer_isr+0)+0
	addwf	(??_timer_isr+0)+0,w
	addlw	_timer_array&0ffh
	movwf	fsr0
	movlw	low(01h)
	subwf	indf,f
	incfsz	fsr0,f
	movlw	high(01h)
	skipc
	decf	indf,f
	subwf	indf,f
	decf	fsr0,f
	line	178
;timer.c: 178: }
	goto	i1l7540
	line	179
	
i1l3690:	
	goto	i1l7540
	line	181
;timer.c: 179: else
;timer.c: 180: {
	
i1l3691:	
	line	173
	
i1l7540:	
	movlw	(01h)
	movwf	(??_timer_isr+0)+0
	movf	(??_timer_isr+0)+0,w
	addwf	(timer_isr@i),f
	
i1l7542:	
	movf	(timer_isr@i),w
	skipz
	goto	u341_20
	goto	i1l7536
u341_20:
	goto	i1l3693
	
i1l3689:	
	line	184
;timer.c: 181: }
;timer.c: 182: }
;timer.c: 184: }
	goto	i1l3693
	line	185
	
i1l3685:	
	goto	i1l3693
	line	187
;timer.c: 185: else
;timer.c: 186: {
	
i1l3692:	
	line	188
	
i1l3693:	
	return
	opt stack 0
GLOBAL	__end_of_timer_isr
	__end_of_timer_isr:
;; =============== function _timer_isr ends ============

	signat	_timer_isr,88
	global	_capture_isr
psect	text880,local,class=CODE,delta=2
global __ptext880
__ptext880:

;; *************** function _capture_isr *****************
;; Defined at:
;;		line 200 in file "F:\pic_projects\Hitch_Helper\capture.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_interrupt_handler
;; This function uses a non-reentrant model
;;
psect	text880
	file	"F:\pic_projects\Hitch_Helper\capture.c"
	line	200
	global	__size_of_capture_isr
	__size_of_capture_isr	equ	__end_of_capture_isr-_capture_isr
	
_capture_isr:	
	opt	stack 3
; Regs used in _capture_isr: [status]
	line	202
	
i1l7488:	
;capture.c: 202: if((CCP2IE)&&(CCP2IF))
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	btfss	(1128/8)^080h,(1128)&7
	goto	u332_21
	goto	u332_20
u332_21:
	goto	i1l7496
u332_20:
	
i1l7490:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfss	(104/8),(104)&7
	goto	u333_21
	goto	u333_20
u333_21:
	goto	i1l7496
u333_20:
	line	207
	
i1l7492:	
;capture.c: 203: {
;capture.c: 207: CCP2IF = FALSE;
	bcf	(104/8),(104)&7
	line	208
	
i1l7494:	
;capture.c: 208: capture_isr_flag_ccp2 = TRUE;
	clrf	(_capture_isr_flag_ccp2)
	bsf	status,0
	rlf	(_capture_isr_flag_ccp2),f
	goto	i1l7496
	line	210
	
i1l907:	
	line	211
	
i1l7496:	
;capture.c: 210: }
;capture.c: 211: if((CCP1IE)&&(CCP1IF))
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	btfss	(1122/8)^080h,(1122)&7
	goto	u334_21
	goto	u334_20
u334_21:
	goto	i1l910
u334_20:
	
i1l7498:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfss	(98/8),(98)&7
	goto	u335_21
	goto	u335_20
u335_21:
	goto	i1l910
u335_20:
	line	216
	
i1l7500:	
;capture.c: 212: {
;capture.c: 216: CCP1IF = FALSE;
	bcf	(98/8),(98)&7
	line	217
	
i1l7502:	
;capture.c: 217: capture_isr_flag_ccp1 = TRUE;
	clrf	(_capture_isr_flag_ccp1)
	bsf	status,0
	rlf	(_capture_isr_flag_ccp1),f
	line	219
;capture.c: 219: }
	goto	i1l910
	line	221
	
i1l908:	
	goto	i1l910
;capture.c: 221: else{}
	
i1l909:	
	line	222
	
i1l910:	
	return
	opt stack 0
GLOBAL	__end_of_capture_isr
	__end_of_capture_isr:
;; =============== function _capture_isr ends ============

	signat	_capture_isr,88
psect	text881,local,class=CODE,delta=2
global __ptext881
__ptext881:
	global	btemp
	btemp set 07Eh

	DABS	1,126,2	;btemp
	global	wtemp0
	wtemp0 set btemp
	end
