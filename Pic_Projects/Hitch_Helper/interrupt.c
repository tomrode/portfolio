
#include <htc.h>
#include "typedefs.h"
#include "timer.h"
#include "capture.h"
void interrupt interrupt_handler(void)
{  
   timer_isr();
   capture_isr(); 
}
