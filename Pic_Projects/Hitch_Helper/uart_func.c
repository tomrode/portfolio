
#include <htc.h>
#include "uart_func.h"
#include "typedefs.h"

/* FUTURE USE TO ENCODE SPECIFIC BYTES WHILE MAINTAINING ENOUGH PAYLOAD CAPABILITY*/
#define SENSOR1_HI_ENCODE   (0x20)
#define SENSOR2_HI_ENCODE   (0x40)
#define SENSOR3_HI_ENCODE   (0x80)


     
    
/* Global variables to this module*/
//uint8_t uarttx_isr_flag;
//uint8_t uartrx_isr_flag;

/*****************************************************************************************************/
/* function name: uart_tx()
/* DATE 5 APR 2013 
/* DESCRIPTION: This function will output 2 sensors (4bytes) ans possible alignment byte 
/*
/* INPUTS-> 6 bytes SENSOR1 Hi and Lo SENSOR2 Hi and Lo May need to put if Calibrated and aligned. 
/* OUTPUTS-> None
/*                         
/*****************************************************************************************************/
void uart_tx(uint8_t EepSen1HiValUart,uint8_t EepSen1LoValUart,uint8_t EepSen2HiValUart,uint8_t EepSen2LoValUart,uint8_t EepSen3HiValUart,uint8_t EepSen3LoValUart)
{
asm("nop");
asm("nop");
asm("nop");

/*******************************SEND SOME ACTUALL BYTES OUT HERE**************************************/ 
 di();                                          /* Nothing should interrupt putting this into a polling mode*/  
TXREG = (EepSen1HiValUart | SENSOR1_HI_ENCODE); /* Send out hi byte with SENSOR 1 May need to encode this */             
    while (TRMT = 0)                            /* TMRT = transmit buffer Reg Empty flag*/                   
      {
       /* Wait here until transmittion is complete */
      }
     _delay(3000);                              /* 1000 around 330 micro seconds */
TXREG = EepSen1LoValUart;                       /* Send out lo byte with SENSOR 1 May need to encode this */              
    while (TRMT = 0)                            /* TMRT = transmit buffer Reg Empty flag*/                   
      {
       /* Wait here until transmittion is complete */
      }
     _delay(3000);                              /* 1000 around 330 micro seconds */
TXREG = (EepSen2HiValUart | SENSOR2_HI_ENCODE); /* Send out hi byte with SENSOR 2 May need to encode this */             
     while (TRMT = 0)                           /* TMRT = transmit buffer Reg Empty flag*/                   
     {
       /* Wait here until transmittion is complete */
     }
     _delay(3000);                              /* 1000 around 330 micro seconds */
TXREG = EepSen2LoValUart;                       /* Send out lo byte with SENSOR 2 May need to encode this */              
     while (TRMT = 0)                           /* TMRT = transmit buffer Reg Empty flag*/                   
     {
       /* Wait here until transmittion is complete */
     }
     _delay(3000);                              /* 1000 around 330 micro seconds */ 
TXREG = (EepSen3HiValUart | SENSOR3_HI_ENCODE); /* Send out hi byte with SENSOR 3 May need to encode this */             
     while (TRMT = 0)                           /* TMRT = transmit buffer Reg Empty flag*/                   
     {
       /* Wait here until transmittion is complete */
     }
     _delay(3000);                              /* 1000 around 330 micro seconds */
TXREG = EepSen3LoValUart;                       /* Send out lo byte with SENSOR 3 May need to encode this */              
     while (TRMT = 0)                           /* TMRT = transmit buffer Reg Empty flag*/                   
     {
       /* Wait here until transmittion is complete */
     }
     _delay(3000);                            /* 1000 around 330 micro seconds */ 
     /* Maybe the sensor is aligned here byte it will be another input into this function*/
 ei();
}

/*****************************************************************************************************/
/* function name: uart3bytes_tx()
/* DATE 5 JULY 2013 
/* DESCRIPTION: This function will output 3 sensors (3bytes) ans possible alignment byte 
/*
/* INPUTS-> 3 bytes SENSORx Hi only. 
/* OUTPUTS-> None
/*                         
/*****************************************************************************************************/
void uart3bytes_tx(uint8_t EepSen1HiValUart,uint8_t EepSen2HiValUart,uint8_t EepSen3HiValUart)
{
asm("nop");
asm("nop");
asm("nop");

/*******************************SEND SOME ACTUALL BYTES OUT HERE**************************************/ 
 di();                                          /* Nothing should interrupt putting this into a polling mode*/  
TXREG = (EepSen1HiValUart | SENSOR1_HI_ENCODE); /* Send out hi byte with SENSOR 1 May need to encode this */             
    while (TRMT = 0)                            /* TMRT = transmit buffer Reg Empty flag*/                   
      {
       /* Wait here until transmittion is complete */
      }
     _delay(3000);                              /* 1000 around 330 micro seconds */
TXREG = (EepSen2HiValUart | SENSOR2_HI_ENCODE); /* Send out hi byte with SENSOR 2 May need to encode this */             
     while (TRMT = 0)                           /* TMRT = transmit buffer Reg Empty flag*/                   
     {
       /* Wait here until transmittion is complete */
     }
     _delay(3000);                              /* 1000 around 330 micro seconds */                           /* 1000 around 330 micro seconds */ 
TXREG = (EepSen3HiValUart | SENSOR3_HI_ENCODE); /* Send out hi byte with SENSOR 3 May need to encode this */             
     while (TRMT = 0)                           /* TMRT = transmit buffer Reg Empty flag*/                   
     {
       /* Wait here until transmittion is complete */
     }
     _delay(3000);                              /* 1000 around 330 micro seconds */
 ei();
}
/*********************************************************************************************/
/* function name: uart_rx()
/* Date 4 DEC 2011
/* DESCRIPTION: 
/*
/* INPUTS  -> NONE
/* OUTPUTS -> 8-bit received read (in this case from CAPL)
/* Update history:
/* - 21 DEC 2011: Fixed the lock up added in a test for overrun if condition then 
/*                re-initialize the UART.
/*********************************************************************************************/
uint8_t uart_rx(void)
{
uint8_t rcv_byte;

rcv_byte = RCREG;        /* init a receive */ 
  
if (OERR ==1)
 {
  CREN = 0;                      /* Shut off Receive */
  uart_init();                   /* try re-initialise*/
  
 }
else
 {
  while ((RCIF == 1) && (OERR != 1))
  {
  /* Wait until flag clears */
  }
 } 

return rcv_byte;  
}


void uart_init(void)
{
//RC6 44 � � � � TX/CK � � � �
//RC7 1 � � � � RX/DT
//TRISC    = 0x80;              /* Pin 26 RC7/RX/DT  PIN 25 Rc6/TX/DT 
//BRG16      = TRUE;              /* Set for higher frequency with SPBRG set to 16 decimal should make 117.6k Hz*/
//SPBRG      = 0x10;
SPBRG      = 0x06;//0x08;       /*08 made 18us or 55555 bps 19 made 52.2 us scope 19200 baud says 33 for 104 us Baud rate = FOSC/(16(X+1))  "51" at 8MHz at 9600 baud "25" at 8MHz at 19200 baud */ 
TXSTA      = 0x24;              /* |CSRC=0|TX9=0|TXEN=1|SYNC=0| -  |BRGH=1|TRMT=0|TX9D=0| */
RCSTA      = 0x90;              /* |SPEN=1(Serial port enabled)|RX9=0|SREN=0|CREN=1(RECEIVER ON)|ADDEN=0|FERR=0|OERR=0|RX9D=0|*/
}


#ifdef UART_INTERRUPT_DESIRED
void uartrx_isr(void)
{

  if ((RCIF==1)&&(RCIE==1))
  {
    //RCIF=0;             // cleared by hardware
    uartrx_isr_flag=1;  // set the indication 
  }
  else 
  {
  }
}

 


void uarttx_isr(void)
{
  if ((TXIF)&&(TXIE))
  {
    
    TXIF=0;             // clear flag
    uarttx_isr_flag=1;  // set the indication 
     
  }
  else 
  {
  }
}
#endif //UART_INTERRUPT_DESIRED
