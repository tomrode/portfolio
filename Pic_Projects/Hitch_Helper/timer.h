
#include "typedefs.h"
#include "htc.h"


//extern uint8_t OneMillisecondTimerDisable; 
// uint32_t PulseWidth1Counter;
// uint32_t PulseWith1CurrentCount;

/*Function Prototypes*/
uint16_t get_timer(uint8_t index);             // Which timer to use out of enum
void timer_set(uint8_t index, uint16_t value); 
void timer_isr(void);
void timer_init(void);

enum sw_timers_e
{
PULSE_OUT_TIMER,                  /* Time setting for firing the Ultra sonic sensor*/
TIMER_MAX
};