#include <htc.h>
#include "typedefs.h"


#ifndef CAPTURE_H
#define CAPTURE_H 
/*External Variables*/
extern uint8_t CaptureHi2;
extern uint8_t CaptureLo2;
extern uint8_t ProcessingCaptureModule;

vuint8_t PulseWidth1Counter;
extern uint8_t PulseWidth1CurrentCount;
vuint8_t PulseWidth2Counter;
extern uint8_t PulseWidth2CurrentCount;
vuint8_t PulseWidth3Counter;
extern uint8_t PulseWidth3CurrentCount;
/*External enums*/
 extern enum
   {
     MODULE_CCP2_OFF   = 0x00,
     RISING_EDGE_CCP2  = 0x05,
     FALLING_EDGE_CCP2 = 0x04
   }CCP2CON_STATES;
   extern enum
   {
     MODULE_CCP1_OFF   = 0x00,
     RISING_EDGE_CCP1  = 0x05,
     FALLING_EDGE_CCP1 = 0x04
   }CCP1CON_STATES;
/*Function Prototypes*/
void capture_state1(void);
void capture_state2(void);
void capture_state3(void);
void capture_isr(void); 
#endif 
