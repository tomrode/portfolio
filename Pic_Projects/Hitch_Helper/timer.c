
#include "htc.h"
#include "typedefs.h"
#include "timer.h"
#include "capture.h"
#define ORIGNAL_TIMER_CONFIG

/*Global variable*/
vuint8_t tmr1_isr_counter;      // volatile isr variable for timer 1 
uint8_t OneMillisecondTimerDisable; 
uint16_t timer_array[TIMER_MAX]; 
vuint8_t PulseWidth1Counter;
// uint32_t PulseWith1CurrentCount;


/* Functions prototypes*/
void timer_isr(void);

#ifdef HITCH_HELPER_TIMER
/***************************************************************/
/* Timer1 main functions
/***************************************************************/
void timer_set(uint8_t index, uint16_t value)
{ 
 uint16_t array_contents;                             
  if (index < TIMER_MAX)
     {
      di();                       // disable interrrupts 
      timer_array[index] = value;
      ei();                          
     }
   else
     {
     } 
} 


uint16_t get_timer(uint8_t index)
{
uint16_t result;

  if (index < TIMER_MAX)
  {
    di();
    result = timer_array[index];
    ei();
  }
  else
  {
    result = 0;
  }

  return result;

}



void timer_isr(void) 
{
uint8_t i;                        // index  
if((TMR1IE)&&(TMR1IF))
 {   
   /* need to put a condition here to overide this timer set for the pulse firing*/
     PulseWidth1Counter++;
     asm("nop");
     //PulseWith1CurrentCount = PulseWidth1Counter;

  //if(OneMillisecondTimerDisable == FALSE)
  //{
     T1CON &= ~(0x01);
     TMR1L =   0x80;//0x08;              //0x08 for pic16f887A 0x89;THIS GIVES A 1.00910 ms roll over on scope for pic16f887
     TMR1H =   0xFF;
	 //TMR1L =   0x90;//0x89;              // THIS GIVES A 1.012 ms roll over on scope at 20MHz
     //TMR1H =   0xFD;
     T1CON |=  0x01;
       
    for (i = 0; i < TIMER_MAX; i++)
      {
        if(timer_array[i] != 0)
        {
           timer_array[i]--;
        }
         else{
              /*DO NOTHING*/
              }
  
       } /*END FOR*/
  //}  
 //else
  //   {
       /* No else OneMillisecondTimerDisable*/
  //   }   
 } 
 else
      {
      /* No else for TMR1IE && TMR1IF*/
      }
}

void timer_init(void)
{
uint8_t i;                        // index 
  for (i = 0; i < TIMER_MAX; i++)
  {
    timer_array[i]=0;
  }
}
#endif /*HITCH_HELPER_TIMER*/

#ifdef ORIGNAL_TIMER_CONFIG
/***************************************************************/
/* Timer1 main functions
/***************************************************************/
void timer_set(uint8_t index, uint16_t value)
{ 
 uint16_t array_contents;                             
  if (index < TIMER_MAX)
     {
      di();                       // disable interrrupts 
      timer_array[index] = value;
      ei();                          
     }
   else
     {
     } 
} 


uint16_t get_timer(uint8_t index)
{
uint16_t result;

  if (index < TIMER_MAX)
  {
    di();
    result = timer_array[index];
    ei();
  }
  else
  {
    result = 0;
  }

  return result;

}



void timer_isr(void) 
{
uint8_t i;                        // index  
if((TMR1IE)&&(TMR1IF))
 { 
  CCP1IE = FALSE; 
  CCP2IE = FALSE;
  PulseWidth1Counter++;              /* Sensor1 */
  PulseWidth2Counter++;              /* Sensor2 */
  PulseWidth3Counter++;              /* Sensor3 */
  CCP1IE = TRUE;
  CCP2IE = TRUE;
  if(OneMillisecondTimerDisable == FALSE)
    {
     TMR1IF=0;
     T1CON &= ~(0x01);
     TMR1L =   0x0F;               /*for pic16f887 0x0F THIS GIVES A 1.010 ms roll over on scope*/
     TMR1H =   0xFF;
     T1CON |=  0x01;
    }
    else{
        }         
    for (i = 0; i < TIMER_MAX; i++)
      {
        if (timer_array[i] != 0)
            {
            timer_array[i]--;
            }
         else
            {
            }
      }

}
 else
 {
 }
}

void timer_init(void)
{
uint8_t i;                        // index 
  for (i = 0; i < TIMER_MAX; i++)
  {
    timer_array[i]=0;
  }
}
#endif /*ORIGINAL_TIMER_CONFIG*/
