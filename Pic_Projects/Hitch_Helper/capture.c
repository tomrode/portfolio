#include "htc.h"
#include "capture.h"
#include "typedefs.h"
#include "timer.h"


/* Local variables*/
uint8_t CaptureHi2;
uint8_t CaptureLo2;
uint8_t capture_isr_flag_ccp2;
uint8_t capture_isr_flag_ccp1;
uint8_t ProcessingCaptureModule;
/* Below were 32 bit before*/
vuint8_t PulseWidth1Counter;
uint8_t PulseWidth1CurrentCount;
vuint8_t PulseWidth2Counter;
uint8_t PulseWidth2CurrentCount;
vuint8_t PulseWidth3Counter;
uint8_t PulseWidth3CurrentCount;
/*********************************************************************************
*capture_state1()          SENSOR1
*Descrition: This function is called by main to set the CCP2CON register which
*            controls if the capture is supposed to set on rising or falling edge.
*            If Interrupt occurs and it in the rising edge dectect mode, then 
*            a counter called PulseWidth1Counter reset to 0 and timer 1 turned off cleared
*            and cleared then turn back on where the counter increments.
*            Once in capture mode in falling edge state the contents of the counter are]
*            retrieved and timer1 reset and tuen off again. then flag will cleared for main
*            to preceed.
*INPUT:  NONE
*OUTPUT: NONE
**********************************************************************************/
void capture_state1(void)
{
static uint8_t CAPTURE_STATE;

CAPTURE_STATE = CCP2CON;

 switch(CAPTURE_STATE)
 {
  case(RISING_EDGE_CCP2):                            /*0101 = Capture mode, every rising edge STATE*/
      capture_isr_flag_ccp2 = FALSE;       
      while(capture_isr_flag_ccp2 == FALSE)          /* waiting for interrupt to occur*/ 
      {}
      CCP2IE  = FALSE;
      CCP2CON    = MODULE_CCP2_OFF;                  /*Shut off capture module so no false interrupt from data sheet*/
      PulseWidth1Counter = 0;                        /* Zero out the counter */   
      /* Maybe shut timer 1 off the preload these to zero then restart*/ 
      T1CON &= ~(0x01);
      TMR1L =   0xF0;
      TMR1H =   0xFF;
      T1CON |=  0x01;
      CCP2CON = FALLING_EDGE_CCP2;                    /* Put the capture module into Falling edge state 0x0100*/
      CCP2IE  = TRUE;
      ProcessingCaptureModule = FALSE; 
   break;
    
    case(FALLING_EDGE_CCP2):                          /*010 = Capture mode, every Falling edge STATE*/
      capture_isr_flag_ccp2 = FALSE;       
      while(capture_isr_flag_ccp2 == FALSE)           /* waiting for interrupt to occur*/ 
      {}
  
      PulseWidth1CurrentCount = PulseWidth1Counter;   /* extract the count */
      T1CON &= ~(0x01);                               /* Turn the timer off and clear it out*/
      TMR1L =   0x00;
      TMR1H =   0x00;
      T1CON |=  0x01;
      PulseWidth1Counter = 0;
      ProcessingCaptureModule = TRUE;                  /* Exit the capture evolution inside of main*/
    break;


  default:
   break;
 }
}

/*********************************************************************************
*capture_state2()                 SENSOR2
*Description: This function is called by main to set the CCP1CON register which
*            controls if the capture is supposed to set on rising or falling edge.
*            If Interrupt occurs and it in the rising edge dectect mode, then 
*            a counter called PulseWidth2Counter reset to 0 and timer 1 turned off cleared
*            and cleared then turn back on where the counter increments.
*            Once in capture mode in falling edge state the contents of the counter are]
*            retrieved and timer1 reset and tuen off again. then flag will cleared for main
*            to preceed.
*INPUT:  NONE
*OUTPUT: NONE
**********************************************************************************/
void capture_state2(void)
{
static uint8_t CAPTURE_STATE;

CAPTURE_STATE = CCP1CON;

 switch(CAPTURE_STATE)
 {
  case(RISING_EDGE_CCP1):                            /*0101 = Capture mode, every rising edge STATE*/
      capture_isr_flag_ccp1 = FALSE;       
      while(capture_isr_flag_ccp1 == FALSE)          /* waiting for interrupt to occur*/ 
      {}
      CCP1IE  = FALSE;
      CCP1CON = MODULE_CCP1_OFF;                             /*Shut off capture module so no false interrupt from data sheet*/
      PulseWidth2Counter = 0;                        /* Zero out the counter */   
      /* Maybe shut timer 1 off the preload these to zero then restart*/ 
      T1CON &= ~(0x01);
      TMR1L =   0xF0;
      TMR1H =   0xFF;
      T1CON |=  0x01;
      CCP1CON = FALLING_EDGE_CCP1;                   /* Put the capture module into Falling edge state 0x0100*/
      CCP1IE = TRUE;
      ProcessingCaptureModule = FALSE; 
   break;
    
    case(FALLING_EDGE_CCP1):                         /*010 = Capture mode, every Falling edge STATE*/
      capture_isr_flag_ccp1 = FALSE;       
      while(capture_isr_flag_ccp1 == FALSE)          /* waiting for interrupt to occur*/ 
      {}
      PulseWidth2CurrentCount = PulseWidth2Counter;  /* extract the count */
      T1CON &= ~(0x01);                              /* Turn the timer off and clear it out*/
      TMR1L =   0x00;
      TMR1H =   0x00;
      T1CON |=  0x01;
      PulseWidth2Counter = 0;
      ProcessingCaptureModule = TRUE;                /* Exit the capture evolution inside of main*/
    break;


  default:
   break;
 }
}

/*********************************************************************************
*capture_state3()                 SENSOR3
*Description: This function is called by main to set the CCP1CON register which
*            controls if the capture is supposed to set on rising or falling edge.
*            If Interrupt occurs and it in the rising edge dectect mode, then 
*            a counter called PulseWidth2Counter reset to 0 and timer 1 turned off cleared
*            and cleared then turn back on where the counter increments.
*            Once in capture mode in falling edge state the contents of the counter are]
*            retrieved and timer1 reset and tuen off again. then flag will cleared for main
*            to preceed.
*INPUT:  NONE
*OUTPUT: NONE
**********************************************************************************/
void capture_state3(void)
{
static uint8_t CAPTURE_STATE;

CAPTURE_STATE = CCP1CON;

 switch(CAPTURE_STATE)
 {
  case(RISING_EDGE_CCP1):                            /*0101 = Capture mode, every rising edge STATE*/
      capture_isr_flag_ccp1 = FALSE;       
      while(capture_isr_flag_ccp1 == FALSE)          /* waiting for interrupt to occur*/ 
      {}
      CCP1IE  = FALSE;
      CCP1CON = MODULE_CCP1_OFF;                      /*Shut off capture module so no false interrupt from data sheet*/
      PulseWidth3Counter = 0;                        /* Zero out the counter */   
      /* Maybe shut timer 1 off the preload these to zero then restart*/ 
      T1CON &= ~(0x01);
      TMR1L =   0xF0;
      TMR1H =   0xFF;
      T1CON |=  0x01;
      CCP1CON = FALLING_EDGE_CCP1;                   /* Put the capture module into Falling edge state 0x0100*/
      CCP1IE = TRUE; 
     ProcessingCaptureModule = FALSE; 
   break;
    
    case(FALLING_EDGE_CCP1):                         /*010 = Capture mode, every Falling edge STATE*/
      capture_isr_flag_ccp1 = FALSE;       
      while(capture_isr_flag_ccp1 == FALSE)          /* waiting for interrupt to occur*/ 
      {}
      PulseWidth3CurrentCount = PulseWidth3Counter;  /* extract the count */
      T1CON &= ~(0x01);                              /* Turn the timer off and clear it out*/
      TMR1L =   0x00;
      TMR1H =   0x00;
      T1CON |=  0x01;
      PulseWidth3Counter = 0;
      ProcessingCaptureModule = TRUE;                /* Exit the capture evolution inside of main*/
    break;


  default:
   break;
 }
}

/*********************************************************************************
*capture_isr()
*Description: Capture modules interrupt service routine.
*
*INPUT:  NONE
*OUTPUT: NONE
**********************************************************************************/
void capture_isr(void) 
{

if((CCP2IE)&&(CCP2IF))
{

  /* If flag not cleared and another capture occurs before the value in the CCPRxH, CCPRxL register pair
      is read, the old captured value is overwritten by the new captured value.*/
  CCP2IF = FALSE;                /*Hardware interrupt flag need to clear */
  capture_isr_flag_ccp2 = TRUE;       /* local isr indication*/  

}
if((CCP1IE)&&(CCP1IF))
{

  /* If flag not cleared and another capture occurs before the value in the CCPRxH, CCPRxL register pair
      is read, the old captured value is overwritten by the new captured value.*/
  CCP1IF = FALSE;                /*Hardware interrupt flag need to clear */
  capture_isr_flag_ccp1 = TRUE;       /* local isr indication*/  

}
/* when seconds sensor available add in the CCP1IE*/
else{}
}
