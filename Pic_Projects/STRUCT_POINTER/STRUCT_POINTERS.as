opt subtitle "HI-TECH Software Omniscient Code Generator (Lite mode) build 10920"

opt pagewidth 120

	opt lm

	processor	16F877A
clrc	macro
	bcf	3,0
	endm
clrz	macro
	bcf	3,2
	endm
setc	macro
	bsf	3,0
	endm
setz	macro
	bsf	3,2
	endm
skipc	macro
	btfss	3,0
	endm
skipz	macro
	btfss	3,2
	endm
skipnc	macro
	btfsc	3,0
	endm
skipnz	macro
	btfsc	3,2
	endm
indf	equ	0
indf0	equ	0
pc	equ	2
pcl	equ	2
status	equ	3
fsr	equ	4
fsr0	equ	4
c	equ	1
z	equ	0
pclath	equ	10
	FNCALL	_main,_copy_func
	FNCALL	_main,_add
	FNCALL	_main,_convert
	FNCALL	_main,_string_count
	FNROOT	_main
	global	_count_array
psect	idataBANK1,class=CODE,space=0,delta=2
global __pidataBANK1
__pidataBANK1:
	file	"C:\pic_projects\STRUCT_POINTER\main.c"
	line	53

;initializer for _count_array
	retlw	048h
	retlw	045h
	retlw	04Ch
	retlw	04Ch
	retlw	04Fh
	retlw	020h
	retlw	057h
	retlw	04Fh
	retlw	052h
	retlw	04Ch
	retlw	044h
	retlw	0
	global	_overall_count
	global	_toms_variable
	global	_CARRY
_CARRY	set	24
	global	_GIE
_GIE	set	95
	global	_EEADR
_EEADR	set	269
	global	_EEDATA
_EEDATA	set	268
	global	_EECON1
_EECON1	set	396
	global	_EECON2
_EECON2	set	397
	global	_RD
_RD	set	3168
	global	_WR
_WR	set	3169
	global	_WREN
_WREN	set	3170
	file	"STRUCT_POINTERS.as"
	line	#
psect cinit,class=CODE,delta=2
global start_initialization
start_initialization:

psect	bssBANK0,class=BANK0,space=1
global __pbssBANK0
__pbssBANK0:
_overall_count:
       ds      1

_toms_variable:
       ds      1

psect	dataBANK1,class=BANK1,space=1
global __pdataBANK1
__pdataBANK1:
	file	"C:\pic_projects\STRUCT_POINTER\main.c"
_count_array:
       ds      12

; Clear objects allocated to BANK0
psect cinit,class=CODE,delta=2
	clrf	((__pbssBANK0)+0)&07Fh
	clrf	((__pbssBANK0)+1)&07Fh
global btemp
psect inittext,class=CODE,delta=2
global init_fetch,btemp
;	Called with low address in FSR and high address in W
init_fetch:
	movf btemp,w
	movwf pclath
	movf btemp+1,w
	movwf pc
global init_ram
;Called with:
;	high address of idata address in btemp 
;	low address of idata address in btemp+1 
;	low address of data in FSR
;	high address + 1 of data in btemp-1
init_ram:
	fcall init_fetch
	movwf indf,f
	incf fsr,f
	movf fsr,w
	xorwf btemp-1,w
	btfsc status,2
	retlw 0
	incf btemp+1,f
	btfsc status,2
	incf btemp,f
	goto init_ram
; Initialize objects allocated to BANK1
psect cinit,class=CODE,delta=2
global init_ram, __pidataBANK1
	bcf	status, 7	;select IRP bank0
	movlw low(__pdataBANK1+12)
	movwf btemp-1,f
	movlw high(__pidataBANK1)
	movwf btemp,f
	movlw low(__pidataBANK1)
	movwf btemp+1,f
	movlw low(__pdataBANK1)
	movwf fsr,f
	fcall init_ram
psect cinit,class=CODE,delta=2
global end_of_initialization

;End of C runtime variable initialization code

end_of_initialization:
clrf status
ljmp _main	;jump to C main() function
psect	cstackBANK1,class=BANK1,space=1
global __pcstackBANK1
__pcstackBANK1:
	global	main@strcnt
main@strcnt:	; 1 bytes @ 0x0
	ds	1
	global	main@val
main@val:	; 1 bytes @ 0x1
	ds	1
	global	main@portd_state
main@portd_state:	; 2 bytes @ 0x2
	ds	2
	global	main@p2
main@p2:	; 4 bytes @ 0x4
	ds	4
	global	main@d_new
main@d_new:	; 1 bytes @ 0x8
	ds	1
	global	main@bb
main@bb:	; 1 bytes @ 0x9
	ds	1
	global	main@aa
main@aa:	; 1 bytes @ 0xA
	ds	1
	global	main@p1
main@p1:	; 4 bytes @ 0xB
	ds	4
	global	main@d
main@d:	; 24 bytes @ 0xF
	ds	24
	global	main@tomsvar
main@tomsvar:	; 2 bytes @ 0x27
	ds	2
	global	main@c
main@c:	; 1 bytes @ 0x29
	ds	1
	global	main@a
main@a:	; 3 bytes @ 0x2A
	ds	3
	global	main@b
main@b:	; 3 bytes @ 0x2D
	ds	3
	global	main@g_cnt
main@g_cnt:	; 3 bytes @ 0x30
	ds	3
	global	main@i
main@i:	; 1 bytes @ 0x33
	ds	1
psect	cstackCOMMON,class=COMMON,space=1
global __pcstackCOMMON
__pcstackCOMMON:
	global	?_copy_func
?_copy_func:	; 0 bytes @ 0x0
	global	??_copy_func
??_copy_func:	; 0 bytes @ 0x0
	global	??_convert
??_convert:	; 0 bytes @ 0x0
	global	??_string_count
??_string_count:	; 0 bytes @ 0x0
	global	?_add
?_add:	; 1 bytes @ 0x0
	global	?_convert
?_convert:	; 1 bytes @ 0x0
	global	?_string_count
?_string_count:	; 1 bytes @ 0x0
	global	?_main
?_main:	; 2 bytes @ 0x0
	global	add@num2
add@num2:	; 1 bytes @ 0x0
	global	copy_func@copy_global_count
copy_func@copy_global_count:	; 3 bytes @ 0x0
	ds	1
	global	??_add
??_add:	; 0 bytes @ 0x1
	global	string_count@i
string_count@i:	; 1 bytes @ 0x1
	global	add@result
add@result:	; 1 bytes @ 0x1
	ds	1
	global	string_count@begin_temp
string_count@begin_temp:	; 1 bytes @ 0x2
	global	add@num1
add@num1:	; 1 bytes @ 0x2
	ds	1
	global	string_count@array_counter
string_count@array_counter:	; 1 bytes @ 0x3
	global	copy_func@buffer
copy_func@buffer:	; 1 bytes @ 0x3
	ds	1
	global	string_count@strt_ptr
string_count@strt_ptr:	; 1 bytes @ 0x4
	ds	1
	global	??_main
??_main:	; 0 bytes @ 0x5
	ds	2
psect	cstackBANK0,class=BANK0,space=1
global __pcstackBANK0
__pcstackBANK0:
	global	convert@ptr
convert@ptr:	; 1 bytes @ 0x0
	ds	1
	global	convert@result
convert@result:	; 24 bytes @ 0x1
	ds	24
	global	convert@gina
convert@gina:	; 6 bytes @ 0x19
	ds	6
	global	convert@lo_result
convert@lo_result:	; 24 bytes @ 0x1F
	ds	24
	global	convert@new_vin
convert@new_vin:	; 1 bytes @ 0x37
	ds	1
;;Data sizes: Strings 0, constant 0, data 12, bss 2, persistent 0 stack 0
;;Auto spaces:   Size  Autos    Used
;; COMMON          14      7       7
;; BANK0           80     56      58
;; BANK1           80     52      64
;; BANK3           96      0       0
;; BANK2           96      0       0

;;
;; Pointer list with targets:

;; ?_convert	PTR struct . size(1) Largest target is 24
;;		 -> convert@result(BANK0[24]), 
;;
;; convert@new_vin	PTR struct . size(1) Largest target is 24
;;		 -> convert@lo_result(BANK0[24]), 
;;
;; convert@ptr	PTR struct . size(1) Largest target is 24
;;		 -> main@d(BANK1[24]), 
;;
;; copy_func@buffer	PTR struct . size(1) Largest target is 3
;;		 -> main@g_cnt(BANK1[3]), 
;;
;; string_count@strt_ptr	PTR unsigned char  size(1) Largest target is 12
;;		 -> count_array(BANK1[12]), 
;;
;; main@c	PTR struct . size(1) Largest target is 3
;;		 -> main@b(BANK1[3]), 
;;
;; sp__convert	PTR struct . size(1) Largest target is 24
;;		 -> convert@result(BANK0[24]), 
;;
;; main@d_new	PTR unsigned char  size(1) Largest target is 24
;;		 -> convert@result(BANK0[24]), 
;;
;; main@bb	PTR struct . size(1) Largest target is 4
;;		 -> main@p2(BANK1[4]), main@p1(BANK1[4]), 
;;
;; main@aa	PTR struct . size(1) Largest target is 4
;;		 -> main@p1(BANK1[4]), 
;;


;;
;; Critical Paths under _main in COMMON
;;
;;   _main->_string_count
;;
;; Critical Paths under _main in BANK0
;;
;;   _main->_convert
;;
;; Critical Paths under _main in BANK1
;;
;;   None.
;;
;; Critical Paths under _main in BANK3
;;
;;   None.
;;
;; Critical Paths under _main in BANK2
;;
;;   None.

;;
;;Main: autosize = 0, tempsize = 2, incstack = 0, save=0
;;

;;
;;Call Graph Tables:
;;
;; ---------------------------------------------------------------------------------
;; (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;; ---------------------------------------------------------------------------------
;; (0) _main                                                79    79      0    1414
;;                                              5 COMMON     2     2      0
;;                                              0 BANK1     52    52      0
;;                          _copy_func
;;                                _add
;;                            _convert
;;                       _string_count
;; ---------------------------------------------------------------------------------
;; (1) _string_count                                         5     5      0      94
;;                                              0 COMMON     5     5      0
;; ---------------------------------------------------------------------------------
;; (1) _convert                                             59    59      0     285
;;                                              0 COMMON     3     3      0
;;                                              0 BANK0     56    56      0
;; ---------------------------------------------------------------------------------
;; (1) _add                                                  3     2      1      45
;;                                              0 COMMON     3     2      1
;; ---------------------------------------------------------------------------------
;; (1) _copy_func                                            4     4      0      23
;;                                              0 COMMON     4     4      0
;; ---------------------------------------------------------------------------------
;; Estimated maximum stack depth 1
;; ---------------------------------------------------------------------------------

;; Call Graph Graphs:

;; _main (ROOT)
;;   _copy_func
;;   _add
;;   _convert
;;   _string_count
;;

;; Address spaces:

;;Name               Size   Autos  Total    Cost      Usage
;;BANK3               60      0       0       9        0.0%
;;BITBANK3            60      0       0       8        0.0%
;;SFR3                 0      0       0       4        0.0%
;;BITSFR3              0      0       0       4        0.0%
;;BANK2               60      0       0      11        0.0%
;;BITBANK2            60      0       0      10        0.0%
;;SFR2                 0      0       0       5        0.0%
;;BITSFR2              0      0       0       5        0.0%
;;SFR1                 0      0       0       2        0.0%
;;BITSFR1              0      0       0       2        0.0%
;;BANK1               50     34      40       7       80.0%
;;BITBANK1            50      0       0       6        0.0%
;;CODE                 0      0       0       0        0.0%
;;DATA                 0      0      82      12        0.0%
;;ABS                  0      0      81       3        0.0%
;;NULL                 0      0       0       0        0.0%
;;STACK                0      0       1       2        0.0%
;;BANK0               50     38      3A       5       72.5%
;;BITBANK0            50      0       0       4        0.0%
;;SFR0                 0      0       0       1        0.0%
;;BITSFR0              0      0       0       1        0.0%
;;COMMON               E      7       7       1       50.0%
;;BITCOMMON            E      0       0       0        0.0%
;;EEDATA             100      0       0       0        0.0%

	global	_main
psect	maintext,global,class=CODE,delta=2
global __pmaintext
__pmaintext:

;; *************** function _main *****************
;; Defined at:
;;		line 72 in file "C:\pic_projects\STRUCT_POINTER\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  d              24   15[BANK1 ] struct .
;;  lo_result      24    0        struct .
;;  p1              4   11[BANK1 ] struct .
;;  p2              4    4[BANK1 ] struct .
;;  g_cnt           3   48[BANK1 ] struct .
;;  b               3   45[BANK1 ] struct .
;;  a               3   42[BANK1 ] struct .
;;  tomsvar         2   39[BANK1 ] unsigned short 
;;  portd_state     2    2[BANK1 ] unsigned short 
;;  i               1   51[BANK1 ] unsigned char 
;;  c               1   41[BANK1 ] PTR struct .
;;		 -> main@b(3), 
;;  aa              1   10[BANK1 ] PTR struct .
;;		 -> main@p1(4), 
;;  bb              1    9[BANK1 ] PTR struct .
;;		 -> main@p2(4), main@p1(4), 
;;  d_new           1    8[BANK1 ] PTR unsigned char 
;;		 -> convert@result(24), 
;;  val             1    1[BANK1 ] unsigned char 
;;  strcnt          1    0[BANK1 ] unsigned char 
;;  j               1    0        unsigned char 
;; Return value:  Size  Location     Type
;;                  2  572[COMMON] int 
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, btemp+1, pclath, cstack
;; Tracked objects:
;;		On entry : 17F/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0      52       0       0
;;      Temps:          2       0       0       0       0
;;      Totals:         2       0      52       0       0
;;Total ram usage:       54 bytes
;; Hardware stack levels required when called:    1
;; This function calls:
;;		_copy_func
;;		_add
;;		_convert
;;		_string_count
;; This function is called by:
;;		Startup code after reset
;; This function uses a non-reentrant model
;;
psect	maintext
	file	"C:\pic_projects\STRUCT_POINTER\main.c"
	line	72
	global	__size_of_main
	__size_of_main	equ	__end_of_main-_main
	
_main:	
	opt	stack 7
; Regs used in _main: [wreg-fsr0h+status,2+status,0+btemp+1+pclath+cstack]
	line	99
	
l3303:	
;main.c: 75: uint8_t i;
;main.c: 76: uint8_t j;
;main.c: 77: uint8_t strcnt;
;main.c: 78: uint8_t val;
;main.c: 79: uint16_t tomsvar;
;main.c: 80: uint16_t portd_state;
;main.c: 82: ORIGINAL_TIME a;
;main.c: 83: CURRENT_TIME b;
;main.c: 84: ORIGINAL_TIME *c;
;main.c: 85: GLOBAL_COUNT g_cnt;
;main.c: 87: VIN d;
;main.c: 88: VIN lo_result;
;main.c: 89: uint8_t *d_new;
;main.c: 91: coordinate_t p1;
;main.c: 92: coordinate_t p2;
;main.c: 93: coordinate_t *aa;
;main.c: 94: coordinate_t *bb;
;main.c: 99: for(i=0;i<100;i++)
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	clrf	(main@i)^080h
	
l3305:	
	movlw	(064h)
	subwf	(main@i)^080h,w
	skipc
	goto	u2391
	goto	u2390
u2391:
	goto	l3309
u2390:
	goto	l3323
	
l3307:	
	goto	l3323
	line	100
	
l573:	
	line	101
	
l3309:	
;main.c: 100: {
;main.c: 101: g_cnt.main_count++;
	movlw	low(01h)
	addwf	(main@g_cnt)^080h,f
	skipnc
	incf	(main@g_cnt+1)^080h,f
	movlw	high(01h)
	addwf	(main@g_cnt+1)^080h,f
	line	102
;main.c: 102: if(g_cnt.main_count == 10)
	movlw	0Ah
	xorwf	(main@g_cnt)^080h,w
	iorwf	(main@g_cnt+1)^080h,w
	skipz
	goto	u2401
	goto	u2400
u2401:
	goto	l3313
u2400:
	line	104
	
l3311:	
;main.c: 103: {
;main.c: 104: g_cnt.BIT_FIELD_STAT.bit0 = TRUE;
	bsf	0+(main@g_cnt)^080h+02h,0
	line	105
;main.c: 105: }
	goto	l3319
	line	106
	
l575:	
	
l3313:	
;main.c: 106: else if(g_cnt.main_count == 11)
	movlw	0Bh
	xorwf	(main@g_cnt)^080h,w
	iorwf	(main@g_cnt+1)^080h,w
	skipz
	goto	u2411
	goto	u2410
u2411:
	goto	l3319
u2410:
	line	108
	
l3315:	
;main.c: 107: {
;main.c: 108: g_cnt.BIT_FIELD_STAT.bit1 = TRUE;
	bsf	0+(main@g_cnt)^080h+02h,1
	line	109
;main.c: 109: if(!g_cnt.BIT_FIELD_STAT.bit1)
	btfsc	0+(main@g_cnt)^080h+02h,1
	goto	u2421
	goto	u2420
u2421:
	goto	l578
u2420:
	line	111
	
l3317:	
;main.c: 110: {
;main.c: 111: g_cnt.BIT_FIELD_STAT.bit7 = TRUE;
	bsf	0+(main@g_cnt)^080h+02h,7
	line	112
;main.c: 112: }
	goto	l3319
	line	113
	
l578:	
	line	115
;main.c: 113: else
;main.c: 114: {
;main.c: 115: g_cnt.BIT_FIELD_STAT.bit6 = TRUE;
	bsf	0+(main@g_cnt)^080h+02h,6
	goto	l3319
	line	116
	
l579:	
	goto	l3319
	line	118
	
l577:	
	goto	l3319
	line	120
	
l576:	
	line	99
	
l3319:	
	movlw	(01h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	addwf	(main@i)^080h,f
	
l3321:	
	movlw	(064h)
	subwf	(main@i)^080h,w
	skipc
	goto	u2431
	goto	u2430
u2431:
	goto	l3309
u2430:
	goto	l3323
	
l574:	
	line	122
	
l3323:	
;main.c: 116: }
;main.c: 118: }
;main.c: 120: }
;main.c: 122: copy_func(&g_cnt);
	movlw	(main@g_cnt)&0ffh
	fcall	_copy_func
	line	130
	
l3325:	
;main.c: 130: tomsvar = 0x00;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	clrf	(main@tomsvar)^080h
	clrf	(main@tomsvar+1)^080h
	line	132
	
l3327:	
;main.c: 132: if(!tomsvar)
	movf	((main@tomsvar+1)^080h),w
	iorwf	((main@tomsvar)^080h),w
	skipz
	goto	u2441
	goto	u2440
u2441:
	goto	l580
u2440:
	line	134
	
l3329:	
# 134 "C:\pic_projects\STRUCT_POINTER\main.c"
nop ;#
psect	maintext
	line	135
;main.c: 135: }
	goto	l3331
	line	136
	
l580:	
	line	138
# 138 "C:\pic_projects\STRUCT_POINTER\main.c"
nop ;#
psect	maintext
	goto	l3331
	line	139
	
l581:	
	line	142
	
l3331:	
;main.c: 139: }
;main.c: 142: tomsvar = 0x01;
	movlw	low(01h)
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movwf	(main@tomsvar)^080h
	movlw	high(01h)
	movwf	((main@tomsvar)^080h)+1
	line	143
;main.c: 143: tomsvar ^= 0x02;
	movlw	low(02h)
	xorwf	(main@tomsvar)^080h,f
	movlw	high(02h)
	xorwf	(main@tomsvar+1)^080h,f
	line	144
	
l3333:	
;main.c: 144: tomsvar = (tomsvar << 1);
	movf	(main@tomsvar+1)^080h,w
	movwf	(??_main+0)+0+1
	movf	(main@tomsvar)^080h,w
	movwf	(??_main+0)+0
	movlw	01h
	movwf	btemp+1
u2455:
	clrc
	rlf	(??_main+0)+0,f
	rlf	(??_main+0)+1,f
	decfsz	btemp+1,f
	goto	u2455
	movf	0+(??_main+0)+0,w
	movwf	(main@tomsvar)^080h
	movf	1+(??_main+0)+0,w
	movwf	(main@tomsvar+1)^080h
	line	145
	
l3335:	
;main.c: 145: tomsvar = 0x55;
	movlw	low(055h)
	movwf	(main@tomsvar)^080h
	movlw	high(055h)
	movwf	((main@tomsvar)^080h)+1
	line	146
	
l3337:	
;main.c: 146: tomsvar = ~(0xFF55);
	movlw	low(0AAh)
	movwf	(main@tomsvar)^080h
	movlw	high(0AAh)
	movwf	((main@tomsvar)^080h)+1
	line	148
	
l3339:	
;main.c: 148: aa = &p1;
	movlw	(main@p1)&0ffh
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(main@aa)^080h
	line	151
	
l3341:	
;main.c: 151: bb = &p2;
	movlw	(main@p2)&0ffh
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(main@bb)^080h
	line	154
	
l3343:	
;main.c: 154: aa -> x = 35;
	movf	(main@aa)^080h,w
	movwf	fsr0
	movlw	low(023h)
	bcf	status, 7	;select IRP bank0
	movwf	indf
	incf	fsr0,f
	movlw	high(023h)
	movwf	indf
	line	155
	
l3345:	
;main.c: 155: aa -> y = 37;
	movf	(main@aa)^080h,w
	addlw	02h
	movwf	fsr0
	movlw	low(025h)
	movwf	indf
	incf	fsr0,f
	movlw	high(025h)
	movwf	indf
	line	160
	
l3347:	
;main.c: 160: bb = &p1;
	movlw	(main@p1)&0ffh
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(main@bb)^080h
	line	161
	
l3349:	
;main.c: 161: p2 = *bb;
	movf	(main@bb)^080h,w
	movwf	fsr0
	movf	indf,w
	movwf	(main@p2)^080h
	incf	fsr0,f
	movf	indf,w
	movwf	(main@p2+1)^080h
	incf	fsr0,f
	movf	indf,w
	movwf	(main@p2+2)^080h
	incf	fsr0,f
	movf	indf,w
	movwf	(main@p2+3)^080h
	line	171
	
l3351:	
;main.c: 171: overall_count=0;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(_overall_count)
	goto	l3353
	line	172
;main.c: 172: while(1)
	
l582:	
	line	175
	
l3353:	
;main.c: 173: {
;main.c: 175: portd_state = (*((volatile unsigned short*)(0x002)));
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(2+1),w	;volatile
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	clrf	(main@portd_state+1)^080h
	addwf	(main@portd_state+1)^080h
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(2),w	;volatile
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	clrf	(main@portd_state)^080h
	addwf	(main@portd_state)^080h

	line	176
	
l3355:	
;main.c: 176: portd_state = (*((volatile unsigned short*)(0x002)));
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(2+1),w	;volatile
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	clrf	(main@portd_state+1)^080h
	addwf	(main@portd_state+1)^080h
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(2),w	;volatile
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	clrf	(main@portd_state)^080h
	addwf	(main@portd_state)^080h

	line	177
	
l3357:	
;main.c: 177: portd_state = (*((volatile unsigned short*)(0x002)));
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(2+1),w	;volatile
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	clrf	(main@portd_state+1)^080h
	addwf	(main@portd_state+1)^080h
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(2),w	;volatile
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	clrf	(main@portd_state)^080h
	addwf	(main@portd_state)^080h

	line	178
	
l3359:	
;main.c: 178: portd_state = (*((volatile unsigned short*)(0x002)));
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(2+1),w	;volatile
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	clrf	(main@portd_state+1)^080h
	addwf	(main@portd_state+1)^080h
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(2),w	;volatile
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	clrf	(main@portd_state)^080h
	addwf	(main@portd_state)^080h

	line	181
	
l3361:	
;main.c: 181: val = add(33,-44);
	movlw	(-44)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(?_add)
	movlw	(021h)
	fcall	_add
	movwf	(??_main+1)+0
	movf	(??_main+1)+0,w
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movwf	(main@val)^080h
	line	182
	
l3363:	
;main.c: 182: for (i=0;i<24;i++)
	clrf	(main@i)^080h
	
l3365:	
	movlw	(018h)
	subwf	(main@i)^080h,w
	skipc
	goto	u2461
	goto	u2460
u2461:
	goto	l3369
u2460:
	goto	l3375
	
l3367:	
	goto	l3375
	line	183
	
l583:	
	line	184
	
l3369:	
;main.c: 183: {
;main.c: 184: d.lo[i]=0xFF;
	movlw	(0FFh)
	movwf	(??_main+0)+0
	movf	(main@i)^080h,w
	addlw	main@d&0ffh
	movwf	fsr0
	movf	(??_main+0)+0,w
	bcf	status, 7	;select IRP bank1
	movwf	indf
	line	182
	
l3371:	
	movlw	(01h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	addwf	(main@i)^080h,f
	
l3373:	
	movlw	(018h)
	subwf	(main@i)^080h,w
	skipc
	goto	u2471
	goto	u2470
u2471:
	goto	l3369
u2470:
	goto	l3375
	
l584:	
	line	187
	
l3375:	
;main.c: 185: }
;main.c: 187: overall_count++;
	movlw	(01h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	addwf	(_overall_count),f
	line	188
;main.c: 188: toms_variable++;
	movlw	(01h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	addwf	(_toms_variable),f	;volatile
	line	189
	
l3377:	
;main.c: 189: d_new = convert(&d);
	movlw	(main@d)&0ffh
	fcall	_convert
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movwf	(main@d_new)^080h
	line	191
	
l3379:	
;main.c: 191: i = 0;
	clrf	(main@i)^080h
	line	198
	
l3381:	
;main.c: 198: for (i=0;i<=24;i++)
	clrf	(main@i)^080h
	
l3383:	
	movlw	(019h)
	subwf	(main@i)^080h,w
	skipc
	goto	u2481
	goto	u2480
u2481:
	goto	l3387
u2480:
	goto	l586
	
l3385:	
	goto	l586
	line	199
	
l585:	
	line	200
	
l3387:	
;main.c: 199: {
;main.c: 200: d.lo[i] = *d_new;
	movf	(main@d_new)^080h,w
	movwf	fsr0
	bcf	status, 7	;select IRP bank0
	movf	indf,w
	movwf	(??_main+0)+0
	movf	(main@i)^080h,w
	addlw	main@d&0ffh
	movwf	fsr0
	movf	(??_main+0)+0,w
	movwf	indf
	line	201
	
l3389:	
;main.c: 201: *d_new++;
	movlw	(01h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	addwf	(main@d_new)^080h,f
	line	198
	
l3391:	
	movlw	(01h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	addwf	(main@i)^080h,f
	
l3393:	
	movlw	(019h)
	subwf	(main@i)^080h,w
	skipc
	goto	u2491
	goto	u2490
u2491:
	goto	l3387
u2490:
	
l586:	
	line	204
# 204 "C:\pic_projects\STRUCT_POINTER\main.c"
nop ;#
psect	maintext
	line	205
	
l3395:	
;main.c: 205: strcnt = string_count(&count_array [0]);
	movlw	(_count_array)&0ffh
	fcall	_string_count
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movwf	(main@strcnt)^080h
	line	208
	
l3397:	
;main.c: 208: a.hours =6;
	movlw	(06h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(main@a)^080h
	line	209
	
l3399:	
;main.c: 209: a.minutes = 7;
	movlw	(07h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	0+(main@a)^080h+01h
	line	210
	
l3401:	
;main.c: 210: a.seconds = 10;
	movlw	(0Ah)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	0+(main@a)^080h+02h
	line	213
	
l3403:	
;main.c: 213: b.hours = 0;
	clrf	(main@b)^080h
	line	214
	
l3405:	
;main.c: 214: b.minutes = 0;
	clrf	0+(main@b)^080h+01h
	line	215
	
l3407:	
;main.c: 215: b.seconds = 0;
	clrf	0+(main@b)^080h+02h
	line	217
	
l3409:	
;main.c: 217: b.hours = a.hours;
	movf	(main@a)^080h,w
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(main@b)^080h
	line	218
	
l3411:	
;main.c: 218: b.minutes = a.minutes;
	movf	0+(main@a)^080h+01h,w
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	0+(main@b)^080h+01h
	line	219
	
l3413:	
;main.c: 219: b.seconds = a.seconds;
	movf	0+(main@a)^080h+02h,w
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	0+(main@b)^080h+02h
	line	222
	
l3415:	
;main.c: 222: c = &b;
	movlw	(main@b)&0ffh
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(main@c)^080h
	line	223
	
l3417:	
;main.c: 223: c->hours = 5;
	movlw	(05h)
	movwf	(??_main+0)+0
	movf	(main@c)^080h,w
	movwf	fsr0
	movf	(??_main+0)+0,w
	bcf	status, 7	;select IRP bank0
	movwf	indf
	line	224
	
l3419:	
;main.c: 224: c->minutes = 14;
	movlw	(0Eh)
	movwf	(??_main+0)+0
	movf	(main@c)^080h,w
	addlw	01h
	movwf	fsr0
	movf	(??_main+0)+0,w
	movwf	indf
	line	225
	
l3421:	
;main.c: 225: c->seconds = 37;
	movlw	(025h)
	movwf	(??_main+0)+0
	movf	(main@c)^080h,w
	addlw	02h
	movwf	fsr0
	movf	(??_main+0)+0,w
	movwf	indf
	line	227
	
l3423:	
# 227 "C:\pic_projects\STRUCT_POINTER\main.c"
nop ;#
psect	maintext
	goto	l3353
	line	229
	
l587:	
	line	172
	goto	l3353
	
l588:	
	line	230
	
l589:	
	global	start
	ljmp	start
	opt stack 0
GLOBAL	__end_of_main
	__end_of_main:
;; =============== function _main ends ============

	signat	_main,90
	global	_string_count
psect	text155,local,class=CODE,delta=2
global __ptext155
__ptext155:

;; *************** function _string_count *****************
;; Defined at:
;;		line 234 in file "C:\pic_projects\STRUCT_POINTER\main.c"
;; Parameters:    Size  Location     Type
;;  strt_ptr        1    wreg     PTR unsigned char 
;;		 -> count_array(12), 
;; Auto vars:     Size  Location     Type
;;  strt_ptr        1    4[COMMON] PTR unsigned char 
;;		 -> count_array(12), 
;;  array_counte    1    3[COMMON] unsigned char 
;;  begin_temp      1    2[COMMON] unsigned char 
;;  i               1    1[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         4       0       0       0       0
;;      Temps:          1       0       0       0       0
;;      Totals:         5       0       0       0       0
;;Total ram usage:        5 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text155
	file	"C:\pic_projects\STRUCT_POINTER\main.c"
	line	234
	global	__size_of_string_count
	__size_of_string_count	equ	__end_of_string_count-_string_count
	
_string_count:	
	opt	stack 7
; Regs used in _string_count: [wreg-fsr0h+status,2+status,0]
;string_count@strt_ptr stored from wreg
	line	240
	movwf	(string_count@strt_ptr)
	
l3287:	
;main.c: 235: uint8_t begin_temp;
;main.c: 237: uint8_t i;
;main.c: 238: uint8_t array_counter;
;main.c: 240: i=0;
	clrf	(string_count@i)
	line	241
;main.c: 241: array_counter=0;
	clrf	(string_count@array_counter)
	line	243
	
l3289:	
;main.c: 243: begin_temp = *strt_ptr;
	movf	(string_count@strt_ptr),w
	movwf	fsr0
	bcf	status, 7	;select IRP bank0
	movf	indf,w
	movwf	(??_string_count+0)+0
	movf	(??_string_count+0)+0,w
	movwf	(string_count@begin_temp)
	line	245
	
l3291:	
# 245 "C:\pic_projects\STRUCT_POINTER\main.c"
nop ;#
psect	text155
	line	247
;main.c: 247: while(begin_temp != '\0' )
	goto	l3297
	
l593:	
	line	249
	
l3293:	
;main.c: 248: {
;main.c: 249: *strt_ptr++;
	movlw	(01h)
	movwf	(??_string_count+0)+0
	movf	(??_string_count+0)+0,w
	addwf	(string_count@strt_ptr),f
	line	250
;main.c: 250: array_counter++;
	movlw	(01h)
	movwf	(??_string_count+0)+0
	movf	(??_string_count+0)+0,w
	addwf	(string_count@array_counter),f
	line	251
	
l3295:	
;main.c: 251: begin_temp = *strt_ptr;
	movf	(string_count@strt_ptr),w
	movwf	fsr0
	bcf	status, 7	;select IRP bank0
	movf	indf,w
	movwf	(??_string_count+0)+0
	movf	(??_string_count+0)+0,w
	movwf	(string_count@begin_temp)
	goto	l3297
	line	252
	
l592:	
	line	247
	
l3297:	
	movf	(string_count@begin_temp),f
	skipz
	goto	u2381
	goto	u2380
u2381:
	goto	l3293
u2380:
	goto	l3299
	
l594:	
	line	254
	
l3299:	
;main.c: 252: }
;main.c: 254: return array_counter;
	movf	(string_count@array_counter),w
	goto	l595
	
l3301:	
	line	255
	
l595:	
	return
	opt stack 0
GLOBAL	__end_of_string_count
	__end_of_string_count:
;; =============== function _string_count ends ============

	signat	_string_count,4217
	global	_convert
psect	text156,local,class=CODE,delta=2
global __ptext156
__ptext156:

;; *************** function _convert *****************
;; Defined at:
;;		line 17 in file "C:\pic_projects\STRUCT_POINTER\func1.c"
;; Parameters:    Size  Location     Type
;;  ptr             1    wreg     PTR struct .
;;		 -> main@d(24), 
;; Auto vars:     Size  Location     Type
;;  ptr             1    0[BANK0 ] PTR struct .
;;		 -> main@d(24), 
;;  lo_result      24   31[BANK0 ] struct .
;;  result         24    1[BANK0 ] struct .
;;  gina            6   25[BANK0 ] struct .
;;  new_vin         1   55[BANK0 ] PTR struct .
;;		 -> convert@lo_result(24), 
;; Return value:  Size  Location     Type
;;                  1    wreg      PTR struct .
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0      56       0       0       0
;;      Temps:          3       0       0       0       0
;;      Totals:         3      56       0       0       0
;;Total ram usage:       59 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text156
	file	"C:\pic_projects\STRUCT_POINTER\func1.c"
	line	17
	global	__size_of_convert
	__size_of_convert	equ	__end_of_convert-_convert
	
_convert:	
	opt	stack 7
; Regs used in _convert: [wreg-fsr0h+status,2+status,0]
;convert@ptr stored from wreg
	line	19
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(convert@ptr)
	
l3253:	
;func1.c: 18: student gina;
;func1.c: 19: gina.age=1;
	clrf	(convert@gina)
	bsf	status,0
	rlf	(convert@gina),f
	line	20
;func1.c: 20: gina.sex=1;
	clrf	0+(convert@gina)+01h
	bsf	status,0
	rlf	0+(convert@gina)+01h,f
	line	29
	
l3255:	
;func1.c: 25: VIN lo_result;
;func1.c: 26: VIN *new_vin;
;func1.c: 27: VIN result;
;func1.c: 29: result = *ptr;
	movlw	(convert@result)&0ffh
	movwf	fsr0
	movf	fsr0,w
	movwf	(??_convert+0)+0
	movf	(convert@ptr),w
	movwf	((??_convert+0)+0+1)
	movlw	24
	movwf	((??_convert+0)+0+2)
u2360:
	
	movf	((??_convert+0)+0+1),w
	movwf	fsr0
	bcf	status, 7	;select IRP bank0
	
	movf	indf,w
	movwf	((??_convert+0)+0+3)
	incf	((??_convert+0)+0+1),f
	movf	(??_convert+0)+0,w
	movwf	fsr0
	movf	((??_convert+0)+0+3),w
	movwf	indf
	incf	(??_convert+0)+0,f
	decfsz	((??_convert+0)+0+2),f
	goto	u2360
	line	30
	
l3257:	
# 30 "C:\pic_projects\STRUCT_POINTER\func1.c"
nop ;#
psect	text156
	line	31
	
l3259:	
;func1.c: 31: new_vin = &lo_result;
	movlw	(convert@lo_result)&0ffh
	movwf	(??_convert+0)+0
	movf	(??_convert+0)+0,w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(convert@new_vin)
	line	33
	
l3261:	
;func1.c: 33: overall_count++;
	movlw	(01h)
	movwf	(??_convert+0)+0
	movf	(??_convert+0)+0,w
	addwf	(_overall_count),f
	line	34
	
l3263:	
;func1.c: 34: toms_variable += 2;
	movlw	(02h)
	movwf	(??_convert+0)+0
	movf	(??_convert+0)+0,w
	addwf	(_toms_variable),f	;volatile
	line	35
	
l3265:	
;func1.c: 35: new_vin->lo[0] = 'B';
	movlw	(042h)
	movwf	(??_convert+0)+0
	movf	(convert@new_vin),w
	movwf	fsr0
	movf	(??_convert+0)+0,w
	bcf	status, 7	;select IRP bank0
	movwf	indf
	line	36
	
l3267:	
;func1.c: 36: new_vin->lo[1] = 'A';
	movlw	(041h)
	movwf	(??_convert+0)+0
	movf	(convert@new_vin),w
	addlw	01h
	movwf	fsr0
	movf	(??_convert+0)+0,w
	movwf	indf
	line	37
	
l3269:	
;func1.c: 37: new_vin->mid[0] ='D';
	movlw	(044h)
	movwf	(??_convert+0)+0
	movf	(convert@new_vin),w
	addlw	08h
	movwf	fsr0
	movf	(??_convert+0)+0,w
	movwf	indf
	line	38
	
l3271:	
;func1.c: 38: new_vin->mid[1] ='B';
	movlw	(042h)
	movwf	(??_convert+0)+0
	movf	(convert@new_vin),w
	addlw	09h
	movwf	fsr0
	movf	(??_convert+0)+0,w
	movwf	indf
	line	39
	
l3273:	
;func1.c: 39: new_vin->hi[0] ='E';
	movlw	(045h)
	movwf	(??_convert+0)+0
	movf	(convert@new_vin),w
	addlw	010h
	movwf	fsr0
	movf	(??_convert+0)+0,w
	movwf	indf
	line	40
	
l3275:	
;func1.c: 40: new_vin->hi[1] = 'E';
	movlw	(045h)
	movwf	(??_convert+0)+0
	movf	(convert@new_vin),w
	addlw	011h
	movwf	fsr0
	movf	(??_convert+0)+0,w
	movwf	indf
	line	41
	
l3277:	
;func1.c: 41: new_vin->hi[2] = 'F';
	movlw	(046h)
	movwf	(??_convert+0)+0
	movf	(convert@new_vin),w
	addlw	012h
	movwf	fsr0
	movf	(??_convert+0)+0,w
	movwf	indf
	line	43
	
l3279:	
;func1.c: 43: result = *new_vin;
	movlw	(convert@result)&0ffh
	movwf	fsr0
	movf	fsr0,w
	movwf	(??_convert+0)+0
	movf	(convert@new_vin),w
	movwf	((??_convert+0)+0+1)
	movlw	24
	movwf	((??_convert+0)+0+2)
u2370:
	
	movf	((??_convert+0)+0+1),w
	movwf	fsr0
	bcf	status, 7	;select IRP bank0
	
	movf	indf,w
	movwf	((??_convert+0)+0+3)
	incf	((??_convert+0)+0+1),f
	movf	(??_convert+0)+0,w
	movwf	fsr0
	movf	((??_convert+0)+0+3),w
	movwf	indf
	incf	(??_convert+0)+0,f
	decfsz	((??_convert+0)+0+2),f
	goto	u2370
	line	44
	
l3281:	
# 44 "C:\pic_projects\STRUCT_POINTER\func1.c"
nop ;#
psect	text156
	line	45
	
l3283:	
;func1.c: 45: return (&result);
	movlw	(convert@result)&0ffh
	goto	l1161
	
l3285:	
	line	46
	
l1161:	
	return
	opt stack 0
GLOBAL	__end_of_convert
	__end_of_convert:
;; =============== function _convert ends ============

	signat	_convert,4217
	global	_add
psect	text157,local,class=CODE,delta=2
global __ptext157
__ptext157:

;; *************** function _add *****************
;; Defined at:
;;		line 49 in file "C:\pic_projects\STRUCT_POINTER\func1.c"
;; Parameters:    Size  Location     Type
;;  num1            1    wreg     char 
;;  num2            1    0[COMMON] char 
;; Auto vars:     Size  Location     Type
;;  num1            1    2[COMMON] char 
;;  result          1    1[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      char 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         1       0       0       0       0
;;      Locals:         2       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         3       0       0       0       0
;;Total ram usage:        3 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text157
	file	"C:\pic_projects\STRUCT_POINTER\func1.c"
	line	49
	global	__size_of_add
	__size_of_add	equ	__end_of_add-_add
	
_add:	
	opt	stack 7
; Regs used in _add: [wreg+status,2+status,0]
;add@num1 stored from wreg
	line	51
	movwf	(add@num1)
	
l3249:	
;func1.c: 50: uint8_t result;
;func1.c: 51: return(result = num1 + num2);
	movf	(add@num2),w
	addwf	(add@num1),w
	movwf	(add@result)
	goto	l1164
	
l3251:	
	line	52
	
l1164:	
	return
	opt stack 0
GLOBAL	__end_of_add
	__end_of_add:
;; =============== function _add ends ============

	signat	_add,8313
	global	_copy_func
psect	text158,local,class=CODE,delta=2
global __ptext158
__ptext158:

;; *************** function _copy_func *****************
;; Defined at:
;;		line 259 in file "C:\pic_projects\STRUCT_POINTER\main.c"
;; Parameters:    Size  Location     Type
;;  buffer          1    wreg     PTR struct .
;;		 -> main@g_cnt(3), 
;; Auto vars:     Size  Location     Type
;;  buffer          1    3[COMMON] PTR struct .
;;		 -> main@g_cnt(3), 
;;  copy_global_    3    0[COMMON] struct .
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         4       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         4       0       0       0       0
;;Total ram usage:        4 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text158
	file	"C:\pic_projects\STRUCT_POINTER\main.c"
	line	259
	global	__size_of_copy_func
	__size_of_copy_func	equ	__end_of_copy_func-_copy_func
	
_copy_func:	
	opt	stack 7
; Regs used in _copy_func: [wregfsr0]
;copy_func@buffer stored from wreg
	line	262
	movwf	(copy_func@buffer)
	
l3247:	
;main.c: 260: GLOBAL_COUNT copy_global_count;
;main.c: 262: copy_global_count = *buffer;
	movf	(copy_func@buffer),w
	movwf	fsr0
	bcf	status, 7	;select IRP bank0
	movf	indf,w
	movwf	(copy_func@copy_global_count)
	incf	fsr0,f
	movf	indf,w
	movwf	(copy_func@copy_global_count+1)
	incf	fsr0,f
	movf	indf,w
	movwf	(copy_func@copy_global_count+2)
	line	264
	
l598:	
	return
	opt stack 0
GLOBAL	__end_of_copy_func
	__end_of_copy_func:
;; =============== function _copy_func ends ============

	signat	_copy_func,4216
psect	text159,local,class=CODE,delta=2
global __ptext159
__ptext159:
	global	btemp
	btemp set 07Eh

	DABS	1,126,2	;btemp
	global	wtemp0
	wtemp0 set btemp
	end
