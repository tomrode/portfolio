 #include "htc.h"
#include "typedefs.h"
#include "func1.h"
#include "func2.h"

#define MAX_LENGTH (10)
#define FALSE (0)
#define TRUE  (1)
#define BLAST (100)
#define NOBLAST (0)
#define stW_EFUSE_reset_b        stW_Flag1_c.b.b7    // bit used to reset the EFUSE (not used anymore)
#define MY_REG_STATUS (*((volatile unsigned short*)(0x007))) /* PORTD */ 
/*PIC16F887*/
//__CONFIG(DEBUGEN & LVPEN & FCMEN & IESODIS & BORXSLP & DUNPROTECT & UNPROTECT & MCLREN & PWRTEN & WDTDIS & INTCLK);//HS);
//__CONFIG(BORV40);
/* STRUCTURES */


    typedef struct status
    {       
        uint8_t ubNumOfCycles;     // Number of cycles between on & off
        uint8_t ubOnTime1;         // Alarm on time length 1
        uint8_t ubOffTime1;        // Alarm off time length 1
        uint8_t ubOnTime2;         // Alarm on time length 2
        uint8_t ubOffTime2;        // Alarm off time length 2
        uint8_t ubPauseTime;       // Pause time if repeating pattern
        uint8_t fCycleRepeat;      // Repeat Cycle
    }ALARM_DEFINITIONS;
   
    const ALARM_DEFINITIONS ParamAlarmTimes[]=
    {   //Cycle  T1on   T1off  T2on  T2off  TPause  Repeat
        { 1,     0,     0,     0,     1,     0,     0},
        { 1,     1,     0,     0,     0,     0,     1},
        { 2,     5,     5,     0,     0,    35,     1},
        { 3,     5,     5,     0,     0,   100,     1},
        { 1,    50,     0,     0,     0,     0,     0},
        { 1,     5,     5,     5,     5,     0,     0},
        { 1,     6,    19,    25,   200,     0,     1},
        { 1,     6,    19,    25,    50,     0,     1},
        { 1,    12,    12,    12,    12,     0,     1},
        { 1,   100,     0,     0,     0,     0,     0},
        { 1,   200,     0,     0,     0,     0,     0}
    };
  
typedef struct
        {
        uint8_t hours;
        uint8_t minutes;
        uint8_t seconds;
       }ORIGINAL_TIME; 

typedef struct
        {
        uint8_t hours;
        uint8_t minutes;
        uint8_t seconds;
       }CURRENT_TIME; 

typedef struct
{
  uint16_t x;
  uint16_t y;
} coordinate_t;          /* John example*/

typedef struct
          { 
          uint16_t main_count;
          struct
              {
              uint8_t bit0 :1;   
              uint8_t bit1 :1;
              uint8_t bit2 :1;
              uint8_t bit3 :1;
              uint8_t bit4 :1;
              uint8_t bit5 :1;
              uint8_t bit6 :1;
              uint8_t bit7 :1;
             }BIT_FIELD_STAT;
          }GLOBAL_COUNT; 
typedef struct
              {
                 uint8_t Jon;
                 uint8_t Joe;
              }boyname_t;

typedef struct
              {
                 uint8_t Jill;
                 uint8_t Jen;
              }girlname_t;
typedef struct
              {
                  boyname_t myboyname;
                  girlname_t mygirlname;
              }myfamily_t;




/* Arrays */
uint8_t count_array [] = "HELLO WORLD";


/* Function prototypes*/
uint8_t string_count (uint8_t *strt_ptr);
void copy_func(GLOBAL_COUNT *buffer);
void sendname(myfamily_t *Ptrfam);
void One(void);
void TheOther(void);
int main(void);

/* non writeable ram variable location*/


/* Volatile variable*/

void sendname(myfamily_t *Ptrfam)
{
  myfamily_t RecvNam;
  myfamily_t *sendPtr;
 
  RecvNam = *Ptrfam;
  
  if(22 == Ptrfam->myboyname.Jon)
  {
    asm("nop");
  }
 
   sendPtr = &RecvNam;
   sendPtr -> myboyname.Jon = 250;

}
    

void One(void)
{
  asm("nop");
}
void TheOther(void)
{
   asm("nop");
}
  
 /*********************************************************************/
/*        THE MAIN LOOP                                              */         
/*********************************************************************/


int main (void)
{
 /* variables and arrays for the main*/

uint8_t i;
uint8_t j;
uint8_t strcnt;
uint8_t val;
uint16_t tomsvar;
uint16_t portd_state; 
uint8_t arr[5] ={1,2,3,4,5};
/* Structure variables */ 
ORIGINAL_TIME  a;          /* all are of type TIME structure*/
CURRENT_TIME   b;
ORIGINAL_TIME *c; 
GLOBAL_COUNT  g_cnt;
ALARM_DEFINITIONS *AlarmConstViewPtr;
uint8_t ubNumOfCycles;     // Number of cycles between on & off
uint8_t ubOnTime1;         // Alarm on time length 1
uint8_t ubOffTime1;        // Alarm off time length 1
uint8_t ubOnTime2;         // Alarm on time length 2
uint8_t ubOffTime2;        // Alarm off time length 2
uint8_t ubPauseTime;       // Pause time if repeating pattern
uint8_t fCycleRepeat;      // Repeat Cycle
uint8_t fCycleRepeatBlast;
VIN d;  
VIN lo_result;
uint8_t *d_new;
// Pointers and Multidimensional Arrays
uint8_t nums[2][3] = { {16, 18, 20}, {25, 26, 27} };
uint8_t test2d;
//test2d = *(*(nums + 1));
test2d = **nums;

coordinate_t p1;           /* John example*/
coordinate_t p2;           /* John example*/
coordinate_t GetPtrVal;
coordinate_t *aa;          /* John example*/
coordinate_t *bb;          /* John example*/

coordinate_t *PtrReview;    /* Skill test */
coordinate_t Review;    

myfamily_t *familyPtr;
myfamily_t Tomsfam;

PtrReview = &Review;  
PtrReview->x = 12;
PtrReview->y = 13;

// Null pointer check
uint8_t *NullPtr;
uint8_t ValidVal = 8;
uint8_t TestNull;

NullPtr = &ValidVal;
TestNull = *NullPtr;




if( PtrReview->x == 12)
{
  asm("nop");
}


for(i=0; i<12; i++)
{ 
    AlarmConstViewPtr = &ParamAlarmTimes[i]; 
    ubNumOfCycles = AlarmConstViewPtr->ubNumOfCycles;
    ubOnTime1 = AlarmConstViewPtr->ubOnTime1;
    ubOffTime1 = AlarmConstViewPtr->ubOffTime1;
    ubOnTime2 = AlarmConstViewPtr->ubOnTime2;
    ubOffTime2 = AlarmConstViewPtr->ubOffTime2;
    ubPauseTime = AlarmConstViewPtr->ubPauseTime;
    fCycleRepeat = AlarmConstViewPtr->fCycleRepeat;
    fCycleRepeatBlast = (AlarmConstViewPtr->fCycleRepeat == TRUE) ? BLAST : NOBLAST;
    PORTD = 0x04;
}



familyPtr = &Tomsfam;

familyPtr -> myboyname.Jon   = 41;
familyPtr -> myboyname.Joe   = 150;;
familyPtr -> mygirlname.Jill = 82;
familyPtr -> mygirlname.Jen  = 53;

sendname(&Tomsfam);
 
val = add(2,2);

j = string_count(&arr[0]);                   /* got to make sure to only have right amount of elements in string so cout them*/
 
   while(1)
    {
      
      i = (~i& 0x01);

    }

/***********************************************************************/
/*          INITIALIZATIONS
/***********************************************************************/
/* The for loop below is total non sense but it shows logic flow to set the bit flags */ 
for(i=0;i<100;i++)
 {
  g_cnt.main_count++;
  if(g_cnt.main_count == 10) 
   {
      g_cnt.BIT_FIELD_STAT.bit0 = TRUE;
   }
   else if(g_cnt.main_count == 11)
         {
             g_cnt.BIT_FIELD_STAT.bit1 = TRUE;
             if(!g_cnt.BIT_FIELD_STAT.bit1)   /* like saying if not true or logic  1*/
              {
                  g_cnt.BIT_FIELD_STAT.bit7 = TRUE;
              } 
             else
              {
                   g_cnt.BIT_FIELD_STAT.bit6 = TRUE;
              }
               
         }
  
  } /* End FOR loop*/    

copy_func(&g_cnt);  /* Make a copy of the GLOBAL_COUNT struct*/



/*************************************************************************/
/*           CODE WILL REMAIN IN while loop if no watch dog reset
/*************************************************************************/ 

tomsvar = 0x00; /*Only true of 0*/ 

if(!tomsvar)
{
asm("nop");
}
else
{
asm("nop");
}


 tomsvar = 0x01;
 tomsvar ^= 0x02;
 tomsvar = (tomsvar << 1);
 tomsvar = 0x55;
 tomsvar = ~(0xFF55);
  /* Point "aa" to "p1" */
  aa = &p1;

  /* Point "bb" to "p2" */
  bb = &p2;

  /* Assign values to "p1" using "aa" */
  aa -> x = 35;
  aa -> y = 37;
  
  GetPtrVal = *aa;   /* See value in the pointer*/


/* Solution it copies over */
  bb = &p1;
  p2 = *bb;


  






overall_count=0;
  while(1)
  {
TRISC = 0x00;
//PORTC = 0x55;
while(1)
 {
   portd_state = MY_REG_STATUS; 
   PORTC = 0x10;
 
   portd_state = MY_REG_STATUS;
   PORTC = 0x00; 
  for(i=0; i<20; i++)
   {
    asm("nop");
   }
  
   }
portd_state = MY_REG_STATUS;
PORTC = 0xA9; 
portd_state = MY_REG_STATUS; 


val = add(33,-44);
 for (i=0;i<24;i++)
   {
   d.lo[i]=0xFF;                             /*Load in start d.lo[0] - d.hi[7] = 0xFF*/
   }
  
 overall_count++; 
 toms_variable++; 
d_new = convert(&d);                        /* Goto call pass a pointer and return one*/

 i = 0;
//while((d.lo[i]!=0)&&(i<=MAX))                 /* This is probably not neccessary*/
// {
// i++;
 //}

 for (i=0;i<=24;i++)
  {
    d.lo[i] = *d_new;                              /* beat this into your head this is getting the data oput of the pointer */
   *d_new++;                                      /* Increment the pointer*/ 
  }
// }
asm("nop");
strcnt = string_count(&count_array [0]);     /* Function call for counting */
 
/* ORIGINAL_TIME*/
  a.hours =6;   
  a.minutes = 7;
  a.seconds = 10;
  
  /* CURRENT_TIME*/ 
  b.hours = 0;
  b.minutes = 0;
  b.seconds = 0;
 
  b.hours = a.hours;
  b.minutes = a.minutes;
  b.seconds = a.seconds;
  

  c = &b;
  c->hours = 5;
  c->minutes = 14;
  c->seconds = 37;
  
  asm("nop"); 
 
}
}


uint8_t string_count(uint8_t *strt_ptr)
{
uint8_t begin_temp;

uint8_t i;
uint8_t array_counter;

i=0;
array_counter=0;

begin_temp = *strt_ptr;

asm("nop");
//uint8_t arr[5] ={1,2,3,4,5};
while(begin_temp != '\0' )
{ 
  //*strt_ptr++;                      /* Advance pointer*/
  array_counter++;                  /* Increment string count */
  begin_temp = strt_ptr[i];           /* Dereference the begining pointer for its value */
  i++;
}

return array_counter;
}


void copy_func(GLOBAL_COUNT *buffer)
{
GLOBAL_COUNT copy_global_count;

copy_global_count = *buffer;

}
