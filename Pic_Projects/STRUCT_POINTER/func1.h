#include "htc.h"
#include "typedefs.h"
#define MAX   (9)

 
typedef struct
        {
        uint8_t lo[MAX-1];
        uint8_t mid[MAX-1];
        uint8_t hi[MAX-1];
        }VIN;



/* Function Prototypes*/
VIN* convert(VIN *ptr);
sint8_t add(sint8_t num1, sint8_t num2);

