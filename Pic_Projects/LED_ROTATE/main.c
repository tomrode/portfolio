#include "htc.h"
#include "timer.h"
#include "led_func.h"
#include "typedefs.h"

/*PIC16F887*/
__CONFIG(DEBUGEN & LVPEN & FCMEN & IESODIS & BORXSLP & DUNPROTECT & UNPROTECT & MCLREN & PWRTEN & WDTDIS & INTCLK);//HS);
__CONFIG(BORV40);
/* STRUCTURES */
 typedef struct
        {
        uint8_t real;
        uint8_t imaginary;
        uint8_t nothing;
        }COMPLEX; 
/* Function prototypes*/
void sfreg(void);                                   // Initializr  the hard ware

/* non writeable ram variable location*/
persistent int persistent_watch_dog_count;

/* Volatile variable*/
vuint8_t tmr1_counter;      // volatile isr variable for timer 1

      
 /*********************************************************************/
/*        THE MAIN LOOP                                              */         
/*********************************************************************/
int main (void)
{
 
  /* variables for the main*/
  uint8_t  watchdog;
  COMPLEX vector;
  
/***********************************************************************/
/*          INITIALIZATIONS
/***********************************************************************/
  sfreg();                                            // initialize sfr's   
  TMR1IE = 1;                                         // Timer 1 interrupt enable
  PEIE = 1;                                           // enable all peripheral interrupts
  ei();                                               // enable all interrupts
  led_state_machine();                                // start the state machine with a default led on
/*************************************************************************/
/*           CODE WILL REMAIN IN while loop if no watch dog reset
/*************************************************************************/ 
 while (HTS == 0)
  {}                                                 // wait until clock stable 

  while(1)
  {
    if(TO == 1)                                       // No watchdog reset proceed with adc read      
    {
     vector.real = 0x11;
     vector.imaginary = 0x22;
     vector.nothing = 0x33;
     led_state_machine();
     watchdog = TO;
     CLRWDT();
     }
    else                                            // a watchdog reset happend update this count
    { 
      watchdog = TO;
      PORTD = persistent_watch_dog_count++;         // Keep a un-accessed RAM count of wdt 
      CLRWDT();
    }
  }
}

void sfreg(void)
{
WDTCON = 0x17;       // WDTPS3=1|WDTPS2=0|WDTPS1=1|WDTPS0=1|SWDTEN=1 5 seconds before watch dog expires
OSCCON = 0x61;       // 4 Mhz clock
OPTION = 0x00;       // |PSA=Prescale to WDT|most prescale rate|
TRISE  = 0x01;       // MAKE PORTE RE1 an input
TRISD  = 0x00;       // Port D as outputs
T1CON  = 0x35;       // T1CKPS1,T1CKPS0 = 1:8 prescaler,T1SYNC = do not synchronise external clock input ,TMR1ON = enabled
ANSEL  &=0x00;       // Make a digital I/O
}





