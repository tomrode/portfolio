
#include "typedefs.h"
#include "htc.h"
#include "timer.h"
#include "led_func.h"
#define TIMER1_VALUE 10

enum
 {
  LED_DEFAULT,   
  LED_L1,        //RD7 
  LED_L2,        //RD6
  LED_L3,        //RD5
  LED_L4,        //RD4
  LED_L5,        //RD3
  LED_L6,        //RD2
  LED_L7,        //RD1 
  LED_L8,        //RR0 
  LED_END
 };

/* VARIABLES GLOBAL TO THS MODULE*/


/*function */

void led_state_machine(void)
{
static uint8_t case_loop_counter = LED_L1;
uint16_t led_timer_temp;

led_timer_temp = get_timer(TIMER_1);
switch (case_loop_counter)
{
     case LED_L1:
     {
     
     PORTD = 0x00;
     PORTD |= 0x80;              //  RD7 ON
     case_loop_counter=LED_L2;   // Set up for RD6 to be on
     timer_set(TIMER_1,TIMER1_VALUE);
     break;
     }

     case LED_L2:
     {
     if ((RE0 == 1) && (led_timer_temp == 0))
     { 
     PORTD <<= 1;
     case_loop_counter=LED_L1;   // Set up for RD6 to be on
     timer_set(TIMER_1,TIMER1_VALUE);
     }
     else if(led_timer_temp == 0)
     {
      PORTD >>= 1;                // RD6 ON
      case_loop_counter=LED_L3;   // Set up for RD6 to be on
      timer_set(TIMER_1,TIMER1_VALUE);
     }
     else
     {
     }
      break;
     }     

     case LED_L3:
     {
     if ((RE0 == 1) && (led_timer_temp == 0))
     { 
     PORTD <<= 1;
     case_loop_counter=LED_L2;   // Set up for RD6 to be on
     timer_set(TIMER_1,TIMER1_VALUE);
     }
     else if(led_timer_temp == 0)
     {
      PORTD >>= 1;                //RD5 On
      case_loop_counter=LED_L4;   // Set up for RD6 to be on
      timer_set(TIMER_1,TIMER1_VALUE);
     }
     else
     {
     }
      break; 
     }

     case LED_L4:
     {
     if ((RE0 == 1) && (led_timer_temp == 0))
     { 
     PORTD <<= 1;
     case_loop_counter=LED_L3;   // Set up for RD6 to be on
     timer_set(TIMER_1,TIMER1_VALUE);
     }
     else if(led_timer_temp == 0)
     {
      PORTD >>= 1;                //RD4 On
      case_loop_counter=LED_L5;
      timer_set(TIMER_1,TIMER1_VALUE);
	 }
     else
     {
     }
      break;
     }
      
     case LED_L5:
     {
     if ((RE0 == 1) && (led_timer_temp == 0))
     { 
     PORTD <<= 1;
     case_loop_counter=LED_L4;   // Set up for RD6 to be on
     timer_set(TIMER_1,TIMER1_VALUE);
     }
     else if(led_timer_temp == 0)
     {
      PORTD >>= 1;                  //RD3 ON
      case_loop_counter=LED_L6;
      timer_set(TIMER_1,TIMER1_VALUE);
	 }
     else
     {
     }
      break;
     }
     
     case LED_L6:
     {
     if ((RE0 == 1) && (led_timer_temp == 0))
     { 
     PORTD <<= 1;
     case_loop_counter=LED_L5;   // Set up for RD6 to be on
     timer_set(TIMER_1,TIMER1_VALUE);
     }
     else if(led_timer_temp == 0)
     {
      PORTD >>= 1;                 //RD2 On
      case_loop_counter=LED_L7;
      timer_set(TIMER_1,TIMER1_VALUE);
	 }
     else
     {
     }
      break;
     }
     case LED_L7:
     {
     if ((RE0 == 1) && (led_timer_temp == 0))
     { 
     PORTD <<= 1;
     case_loop_counter=LED_L6;   // RD1 on
     timer_set(TIMER_1,TIMER1_VALUE);
     }
     else if(led_timer_temp == 0)
     {
      PORTD >>= 1;                //RD1 On
      case_loop_counter=LED_L8;
      timer_set(TIMER_1,TIMER1_VALUE);
	 }
     else
     {
     }
      break;
     }

      case LED_L8:
     {
     if ((RE0 == 1) && (led_timer_temp == 0))
     { 
     PORTD <<= 1;
     case_loop_counter=LED_L7;   // Set up for RD6 to be on
     timer_set(TIMER_1,TIMER1_VALUE);
     } 
     else if(led_timer_temp == 0)
     {
      PORTD >>= 1;
      case_loop_counter=LED_END;
      timer_set(TIMER_1,TIMER1_VALUE);
	 }
     else
     {
     }
      break;
     }
      
      case LED_END:
     {
     if ((RE0 == 1) && (led_timer_temp == 0))
     { 
     PORTD <<= 1;
     case_loop_counter=LED_L8;   // Set up for RD6 to be on
     timer_set(TIMER_1,TIMER1_VALUE);
     } 
     else if(led_timer_temp == 0)
     {
      PORTD == 0x01;
      case_loop_counter=LED_END;
      timer_set(TIMER_1,TIMER1_VALUE);
	 }
     else
     {
     }
      break;
     }

     default:
     break;

}  
}

