opt subtitle "HI-TECH Software Omniscient Code Generator (Lite mode) build 6738"

opt pagewidth 120

	opt lm

	processor	16F887
clrc	macro
	bcf	3,0
	endm
clrz	macro
	bcf	3,2
	endm
setc	macro
	bsf	3,0
	endm
setz	macro
	bsf	3,2
	endm
skipc	macro
	btfss	3,0
	endm
skipz	macro
	btfss	3,2
	endm
skipnc	macro
	btfsc	3,0
	endm
skipnz	macro
	btfsc	3,2
	endm
indf	equ	0
indf0	equ	0
pc	equ	2
pcl	equ	2
status	equ	3
fsr	equ	4
fsr0	equ	4
c	equ	1
z	equ	0
pclath	equ	10
# 7 "C:\pic_projects\LED_ROTATE\main.c"
	psect config,class=CONFIG,delta=2 ;#
# 7 "C:\pic_projects\LED_ROTATE\main.c"
	dw 0x1FFF & 0x3FFF & 0x3FFF & 0x3BFF & 0x3EFF & 0x3FFF & 0x3FFF & 0x3FFF & 0x3FEF & 0x3FF7 & 0x3FFD ;#
# 8 "C:\pic_projects\LED_ROTATE\main.c"
	psect config,class=CONFIG,delta=2 ;#
# 8 "C:\pic_projects\LED_ROTATE\main.c"
	dw 0x3FFF ;#
	FNCALL	_main,_sfreg
	FNCALL	_main,_led_state_machine
	FNCALL	_led_state_machine,_get_timer
	FNCALL	_led_state_machine,_timer_set
	FNROOT	_main
	FNCALL	_interrupt_handler,_timer_isr
	FNCALL	intlevel1,_interrupt_handler
	global	intlevel1
	FNROOT	intlevel1
	global	led_state_machine@case_loop_counter
psect	idataBANK0,class=CODE,space=0,delta=2
global __pidataBANK0
__pidataBANK0:
	file	"C:\pic_projects\LED_ROTATE\led_func_led_timer_temp.c"
	line	29

;initializer for led_state_machine@case_loop_counter
	retlw	01h
	global	_timer_array
	global	_tmr1_counter
	global	_tmr1_isr_counter
	global	_toms_variable
	global	_persistent_watch_dog_count
psect	nvBANK0,class=BANK0,space=1
global __pnvBANK0
__pnvBANK0:
_persistent_watch_dog_count:
       ds      2

	global	_PORTD
_PORTD	set	8
	global	_T1CON
_T1CON	set	16
	global	_TMR1H
_TMR1H	set	15
	global	_TMR1L
_TMR1L	set	14
	global	_GIE
_GIE	set	95
	global	_PEIE
_PEIE	set	94
	global	_RE0
_RE0	set	72
	global	_TMR1IF
_TMR1IF	set	96
	global	_TO
_TO	set	28
	global	_OPTION
_OPTION	set	129
	global	_OSCCON
_OSCCON	set	143
	global	_TRISD
_TRISD	set	136
	global	_TRISE
_TRISE	set	137
	global	_HTS
_HTS	set	1146
	global	_TMR1IE
_TMR1IE	set	1120
	global	_WDTCON
_WDTCON	set	261
	global	_ANSEL
_ANSEL	set	392
	file	"LED_ROTATE.as"
	line	#
psect cinit,class=CODE,delta=2
global start_initialization
start_initialization:

psect	bssCOMMON,class=COMMON,space=1
global __pbssCOMMON
__pbssCOMMON:
_tmr1_counter:
       ds      1

_tmr1_isr_counter:
       ds      1

_toms_variable:
       ds      1

psect	bssBANK0,class=BANK0,space=1
global __pbssBANK0
__pbssBANK0:
_timer_array:
       ds      4

psect	dataBANK0,class=BANK0,space=1
global __pdataBANK0
__pdataBANK0:
	file	"C:\pic_projects\LED_ROTATE\led_func_led_timer_temp.c"
led_state_machine@case_loop_counter:
       ds      1

; Clear objects allocated to COMMON
psect cinit,class=CODE,delta=2
	clrf	((__pbssCOMMON)+0)&07Fh
	clrf	((__pbssCOMMON)+1)&07Fh
	clrf	((__pbssCOMMON)+2)&07Fh
; Clear objects allocated to BANK0
psect cinit,class=CODE,delta=2
	clrf	((__pbssBANK0)+0)&07Fh
	clrf	((__pbssBANK0)+1)&07Fh
	clrf	((__pbssBANK0)+2)&07Fh
	clrf	((__pbssBANK0)+3)&07Fh
; Initialize objects allocated to BANK0
	global __pidataBANK0
psect cinit,class=CODE,delta=2
	fcall	__pidataBANK0+0		;fetch initializer
	movwf	__pdataBANK0+0&07fh		
psect cinit,class=CODE,delta=2
global end_of_initialization

;End of C runtime variable initialization code

end_of_initialization:
clrf status
ljmp _main	;jump to C main() function
psect	cstackCOMMON,class=COMMON,space=1
global __pcstackCOMMON
__pcstackCOMMON:
	global	?_sfreg
?_sfreg:	; 0 bytes @ 0x0
	global	?_led_state_machine
?_led_state_machine:	; 0 bytes @ 0x0
	global	?_timer_isr
?_timer_isr:	; 0 bytes @ 0x0
	global	??_timer_isr
??_timer_isr:	; 0 bytes @ 0x0
	global	?_interrupt_handler
?_interrupt_handler:	; 0 bytes @ 0x0
	global	?_main
?_main:	; 2 bytes @ 0x0
	ds	1
	global	timer_isr@i
timer_isr@i:	; 1 bytes @ 0x1
	ds	1
	global	??_interrupt_handler
??_interrupt_handler:	; 0 bytes @ 0x2
	ds	4
psect	cstackBANK0,class=BANK0,space=1
global __pcstackBANK0
__pcstackBANK0:
	global	??_sfreg
??_sfreg:	; 0 bytes @ 0x0
	global	?_timer_set
?_timer_set:	; 0 bytes @ 0x0
	global	?_get_timer
?_get_timer:	; 2 bytes @ 0x0
	global	timer_set@value
timer_set@value:	; 2 bytes @ 0x0
	ds	2
	global	??_timer_set
??_timer_set:	; 0 bytes @ 0x2
	global	??_get_timer
??_get_timer:	; 0 bytes @ 0x2
	ds	1
	global	timer_set@index
timer_set@index:	; 1 bytes @ 0x3
	global	get_timer@result
get_timer@result:	; 2 bytes @ 0x3
	ds	2
	global	get_timer@index
get_timer@index:	; 1 bytes @ 0x5
	ds	1
	global	??_led_state_machine
??_led_state_machine:	; 0 bytes @ 0x6
	ds	1
	global	led_state_machine@led_timer_temp
led_state_machine@led_timer_temp:	; 2 bytes @ 0x7
	ds	2
	global	??_main
??_main:	; 0 bytes @ 0x9
	ds	1
	global	main@watchdog
main@watchdog:	; 1 bytes @ 0xA
	ds	1
	global	main@vector
main@vector:	; 3 bytes @ 0xB
	ds	3
;;Data sizes: Strings 0, constant 0, data 1, bss 7, persistent 2 stack 0
;;Auto spaces:   Size  Autos    Used
;; COMMON          14      6       9
;; BANK0           80     14      21
;; BANK1           80      0       0
;; BANK3           96      0       0
;; BANK2           96      0       0

;;
;; Pointer list with targets:

;; ?_get_timer	unsigned short  size(1) Largest target is 0
;;


;;
;; Critical Paths under _main in COMMON
;;
;;   None.
;;
;; Critical Paths under _interrupt_handler in COMMON
;;
;;   _interrupt_handler->_timer_isr
;;
;; Critical Paths under _main in BANK0
;;
;;   _main->_led_state_machine
;;   _led_state_machine->_get_timer
;;
;; Critical Paths under _interrupt_handler in BANK0
;;
;;   None.
;;
;; Critical Paths under _main in BANK1
;;
;;   None.
;;
;; Critical Paths under _interrupt_handler in BANK1
;;
;;   None.
;;
;; Critical Paths under _main in BANK3
;;
;;   None.
;;
;; Critical Paths under _interrupt_handler in BANK3
;;
;;   None.
;;
;; Critical Paths under _main in BANK2
;;
;;   None.
;;
;; Critical Paths under _interrupt_handler in BANK2
;;
;;   None.

;;
;;Main: autosize = 0, tempsize = 1, incstack = 0, save=0
;;

;;
;;Call Graph Tables:
;;
;; ---------------------------------------------------------------------------------
;; (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;; ---------------------------------------------------------------------------------
;; (0) _main                                                 5     5      0     774
;;                                              9 BANK0      5     5      0
;;                              _sfreg
;;                  _led_state_machine
;; ---------------------------------------------------------------------------------
;; (1) _led_state_machine                                    3     3      0     691
;;                                              6 BANK0      3     3      0
;;                          _get_timer
;;                          _timer_set
;; ---------------------------------------------------------------------------------
;; (2) _get_timer                                            6     4      2      99
;;                                              0 BANK0      6     4      2
;; ---------------------------------------------------------------------------------
;; (2) _timer_set                                            6     4      2      93
;;                                              0 BANK0      4     2      2
;; ---------------------------------------------------------------------------------
;; (1) _sfreg                                                0     0      0       0
;; ---------------------------------------------------------------------------------
;; Estimated maximum stack depth 2
;; ---------------------------------------------------------------------------------
;; (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;; ---------------------------------------------------------------------------------
;; (3) _interrupt_handler                                    4     4      0      90
;;                                              2 COMMON     4     4      0
;;                          _timer_isr
;; ---------------------------------------------------------------------------------
;; (4) _timer_isr                                            2     2      0      90
;;                                              0 COMMON     2     2      0
;; ---------------------------------------------------------------------------------
;; Estimated maximum stack depth 4
;; ---------------------------------------------------------------------------------

;; Call Graph Graphs:

;; _main (ROOT)
;;   _sfreg
;;   _led_state_machine
;;     _get_timer
;;     _timer_set
;;
;; _interrupt_handler (ROOT)
;;   _timer_isr
;;

;; Address spaces:

;;Name               Size   Autos  Total    Cost      Usage
;;BANK3               60      0       0       9        0.0%
;;BITBANK3            60      0       0       8        0.0%
;;SFR3                 0      0       0       4        0.0%
;;BITSFR3              0      0       0       4        0.0%
;;BANK2               60      0       0      11        0.0%
;;BITBANK2            60      0       0      10        0.0%
;;SFR2                 0      0       0       5        0.0%
;;BITSFR2              0      0       0       5        0.0%
;;SFR1                 0      0       0       2        0.0%
;;BITSFR1              0      0       0       2        0.0%
;;BANK1               50      0       0       7        0.0%
;;BITBANK1            50      0       0       6        0.0%
;;CODE                 0      0       0       0        0.0%
;;DATA                 0      0      24      12        0.0%
;;ABS                  0      0      1E       3        0.0%
;;NULL                 0      0       0       0        0.0%
;;STACK                0      0       6       2        0.0%
;;BANK0               50      E      15       5       26.3%
;;BITBANK0            50      0       0       4        0.0%
;;SFR0                 0      0       0       1        0.0%
;;BITSFR0              0      0       0       1        0.0%
;;COMMON               E      6       9       1       64.3%
;;BITCOMMON            E      0       0       0        0.0%
;;EEDATA             100      0       0       0        0.0%

	global	_main
psect	maintext,global,class=CODE,delta=2
global __pmaintext
__pmaintext:

;; *************** function _main *****************
;; Defined at:
;;		line 30 in file "C:\pic_projects\LED_ROTATE\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  vector          3   11[BANK0 ] struct .
;;  watchdog        1   10[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;                  2  844[COMMON] int 
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 17F/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       4       0       0       0
;;      Temps:          0       1       0       0       0
;;      Totals:         0       5       0       0       0
;;Total ram usage:        5 bytes
;; Hardware stack levels required when called:    4
;; This function calls:
;;		_sfreg
;;		_led_state_machine
;; This function is called by:
;;		Startup code after reset
;; This function uses a non-reentrant model
;;
psect	maintext
	file	"C:\pic_projects\LED_ROTATE\main.c"
	line	30
	global	__size_of_main
	__size_of_main	equ	__end_of_main-_main
	
_main:	
	opt	stack 4
; Regs used in _main: [wreg-fsr0h+status,2+status,0+pclath+cstack]
	line	39
	
l5155:	
;main.c: 33: uint8_t watchdog;
;main.c: 34: COMPLEX vector;
;main.c: 39: sfreg();
	fcall	_sfreg
	line	40
	
l5157:	
;main.c: 40: TMR1IE = 1;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bsf	(1120/8)^080h,(1120)&7
	line	41
	
l5159:	
;main.c: 41: PEIE = 1;
	bsf	(94/8),(94)&7
	line	42
	
l5161:	
;main.c: 42: (GIE = 1);
	bsf	(95/8),(95)&7
	line	43
	
l5163:	
;main.c: 43: led_state_machine();
	fcall	_led_state_machine
	line	47
;main.c: 47: while (HTS == 0)
	goto	l845
	
l846:	
	line	48
;main.c: 48: {}
	
l845:	
	line	47
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	btfss	(1146/8)^080h,(1146)&7
	goto	u3101
	goto	u3100
u3101:
	goto	l845
u3100:
	goto	l848
	
l847:	
	line	50
;main.c: 50: while(1)
	
l848:	
	line	52
;main.c: 51: {
;main.c: 52: if(TO == 1)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfss	(28/8),(28)&7
	goto	u3111
	goto	u3110
u3111:
	goto	l5173
u3110:
	line	54
	
l5165:	
;main.c: 53: {
;main.c: 54: vector.real = 0x11;
	movlw	(011h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(main@vector)
	line	55
;main.c: 55: vector.imaginary = 0x22;
	movlw	(022h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	0+(main@vector)+01h
	line	56
;main.c: 56: vector.nothing = 0x33;
	movlw	(033h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	0+(main@vector)+02h
	line	57
	
l5167:	
;main.c: 57: led_state_machine();
	fcall	_led_state_machine
	line	58
	
l5169:	
;main.c: 58: watchdog = TO;
	movlw	0
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfsc	(28/8),(28)&7
	movlw	1
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(main@watchdog)
	line	59
	
l5171:	
# 59 "C:\pic_projects\LED_ROTATE\main.c"
clrwdt ;#
psect	maintext
	line	60
;main.c: 60: }
	goto	l848
	line	61
	
l849:	
	line	63
	
l5173:	
;main.c: 61: else
;main.c: 62: {
;main.c: 63: watchdog = TO;
	movlw	0
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfsc	(28/8),(28)&7
	movlw	1
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(main@watchdog)
	line	64
;main.c: 64: PORTD = persistent_watch_dog_count++;
	movf	(_persistent_watch_dog_count),w
	movwf	(8)	;volatile
	movlw	low(01h)
	addwf	(_persistent_watch_dog_count),f
	skipnc
	incf	(_persistent_watch_dog_count+1),f
	movlw	high(01h)
	addwf	(_persistent_watch_dog_count+1),f
	line	65
	
l5175:	
# 65 "C:\pic_projects\LED_ROTATE\main.c"
clrwdt ;#
psect	maintext
	goto	l848
	line	66
	
l850:	
	goto	l848
	line	67
	
l851:	
	line	50
	goto	l848
	
l852:	
	line	68
	
l853:	
	global	start
	ljmp	start
	opt stack 0
GLOBAL	__end_of_main
	__end_of_main:
;; =============== function _main ends ============

	signat	_main,90
	global	_led_state_machine
psect	text276,local,class=CODE,delta=2
global __ptext276
__ptext276:

;; *************** function _led_state_machine *****************
;; Defined at:
;;		line 28 in file "C:\pic_projects\LED_ROTATE\led_func_led_timer_temp.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  led_timer_te    2    7[BANK0 ] unsigned short 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       2       0       0       0
;;      Temps:          0       1       0       0       0
;;      Totals:         0       3       0       0       0
;;Total ram usage:        3 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		_get_timer
;;		_timer_set
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text276
	file	"C:\pic_projects\LED_ROTATE\led_func_led_timer_temp.c"
	line	28
	global	__size_of_led_state_machine
	__size_of_led_state_machine	equ	__end_of_led_state_machine-_led_state_machine
	
_led_state_machine:	
	opt	stack 4
; Regs used in _led_state_machine: [wreg-fsr0h+status,2+status,0+pclath+cstack]
	line	32
	
l5043:	
;led_func_led_timer_temp.c: 29: static uint8_t case_loop_counter = LED_L1;
;led_func_led_timer_temp.c: 30: uint16_t led_timer_temp;
;led_func_led_timer_temp.c: 32: led_timer_temp = get_timer(TIMER_1);
	movlw	(0)
	fcall	_get_timer
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(1+(?_get_timer)),w
	clrf	(led_state_machine@led_timer_temp+1)
	addwf	(led_state_machine@led_timer_temp+1)
	movf	(0+(?_get_timer)),w
	clrf	(led_state_machine@led_timer_temp)
	addwf	(led_state_machine@led_timer_temp)

	line	33
;led_func_led_timer_temp.c: 33: switch (case_loop_counter)
	goto	l5153
	line	35
;led_func_led_timer_temp.c: 34: {
;led_func_led_timer_temp.c: 35: case LED_L1:
	
l3401:	
	line	38
	
l5045:	
;led_func_led_timer_temp.c: 36: {
;led_func_led_timer_temp.c: 38: PORTD = 0x00;
	clrf	(8)	;volatile
	line	39
	
l5047:	
;led_func_led_timer_temp.c: 39: PORTD |= 0x80;
	bsf	(8)+(7/8),(7)&7	;volatile
	line	40
	
l5049:	
;led_func_led_timer_temp.c: 40: case_loop_counter=LED_L2;
	movlw	(02h)
	movwf	(??_led_state_machine+0)+0
	movf	(??_led_state_machine+0)+0,w
	movwf	(led_state_machine@case_loop_counter)
	line	41
	
l5051:	
;led_func_led_timer_temp.c: 41: timer_set(TIMER_1,10);
	movlw	low(0Ah)
	movwf	(?_timer_set)
	movlw	high(0Ah)
	movwf	((?_timer_set))+1
	movlw	(0)
	fcall	_timer_set
	line	42
;led_func_led_timer_temp.c: 42: break;
	goto	l3444
	line	45
;led_func_led_timer_temp.c: 43: }
;led_func_led_timer_temp.c: 45: case LED_L2:
	
l3403:	
	line	47
;led_func_led_timer_temp.c: 46: {
;led_func_led_timer_temp.c: 47: if ((RE0 == 1) && (led_timer_temp == 0))
	btfss	(72/8),(72)&7
	goto	u2861
	goto	u2860
u2861:
	goto	l5061
u2860:
	
l5053:	
	movf	((led_state_machine@led_timer_temp+1)),w
	iorwf	((led_state_machine@led_timer_temp)),w
	skipz
	goto	u2871
	goto	u2870
u2871:
	goto	l5061
u2870:
	line	49
	
l5055:	
;led_func_led_timer_temp.c: 48: {
;led_func_led_timer_temp.c: 49: PORTD <<= 1;
	clrc
	rlf	(8),f	;volatile

	line	50
	
l5057:	
;led_func_led_timer_temp.c: 50: case_loop_counter=LED_L1;
	clrf	(led_state_machine@case_loop_counter)
	bsf	status,0
	rlf	(led_state_machine@case_loop_counter),f
	line	51
	
l5059:	
;led_func_led_timer_temp.c: 51: timer_set(TIMER_1,10);
	movlw	low(0Ah)
	movwf	(?_timer_set)
	movlw	high(0Ah)
	movwf	((?_timer_set))+1
	movlw	(0)
	fcall	_timer_set
	line	52
;led_func_led_timer_temp.c: 52: }
	goto	l3444
	line	53
	
l3404:	
	
l5061:	
;led_func_led_timer_temp.c: 53: else if(led_timer_temp == 0)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	((led_state_machine@led_timer_temp+1)),w
	iorwf	((led_state_machine@led_timer_temp)),w
	skipz
	goto	u2881
	goto	u2880
u2881:
	goto	l3444
u2880:
	line	55
	
l5063:	
;led_func_led_timer_temp.c: 54: {
;led_func_led_timer_temp.c: 55: PORTD >>= 1;
	clrc
	rrf	(8),f	;volatile

	line	56
;led_func_led_timer_temp.c: 56: case_loop_counter=LED_L3;
	movlw	(03h)
	movwf	(??_led_state_machine+0)+0
	movf	(??_led_state_machine+0)+0,w
	movwf	(led_state_machine@case_loop_counter)
	line	57
	
l5065:	
;led_func_led_timer_temp.c: 57: timer_set(TIMER_1,10);
	movlw	low(0Ah)
	movwf	(?_timer_set)
	movlw	high(0Ah)
	movwf	((?_timer_set))+1
	movlw	(0)
	fcall	_timer_set
	line	58
;led_func_led_timer_temp.c: 58: }
	goto	l3444
	line	59
	
l3406:	
	goto	l3444
	line	61
;led_func_led_timer_temp.c: 59: else
;led_func_led_timer_temp.c: 60: {
	
l3407:	
	goto	l3444
	
l3405:	
	line	62
;led_func_led_timer_temp.c: 61: }
;led_func_led_timer_temp.c: 62: break;
	goto	l3444
	line	65
;led_func_led_timer_temp.c: 63: }
;led_func_led_timer_temp.c: 65: case LED_L3:
	
l3408:	
	line	67
;led_func_led_timer_temp.c: 66: {
;led_func_led_timer_temp.c: 67: if ((RE0 == 1) && (led_timer_temp == 0))
	btfss	(72/8),(72)&7
	goto	u2891
	goto	u2890
u2891:
	goto	l5073
u2890:
	
l5067:	
	movf	((led_state_machine@led_timer_temp+1)),w
	iorwf	((led_state_machine@led_timer_temp)),w
	skipz
	goto	u2901
	goto	u2900
u2901:
	goto	l5073
u2900:
	line	69
	
l5069:	
;led_func_led_timer_temp.c: 68: {
;led_func_led_timer_temp.c: 69: PORTD <<= 1;
	clrc
	rlf	(8),f	;volatile

	line	70
;led_func_led_timer_temp.c: 70: case_loop_counter=LED_L2;
	movlw	(02h)
	movwf	(??_led_state_machine+0)+0
	movf	(??_led_state_machine+0)+0,w
	movwf	(led_state_machine@case_loop_counter)
	line	71
	
l5071:	
;led_func_led_timer_temp.c: 71: timer_set(TIMER_1,10);
	movlw	low(0Ah)
	movwf	(?_timer_set)
	movlw	high(0Ah)
	movwf	((?_timer_set))+1
	movlw	(0)
	fcall	_timer_set
	line	72
;led_func_led_timer_temp.c: 72: }
	goto	l3444
	line	73
	
l3409:	
	
l5073:	
;led_func_led_timer_temp.c: 73: else if(led_timer_temp == 0)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	((led_state_machine@led_timer_temp+1)),w
	iorwf	((led_state_machine@led_timer_temp)),w
	skipz
	goto	u2911
	goto	u2910
u2911:
	goto	l3444
u2910:
	line	75
	
l5075:	
;led_func_led_timer_temp.c: 74: {
;led_func_led_timer_temp.c: 75: PORTD >>= 1;
	clrc
	rrf	(8),f	;volatile

	line	76
;led_func_led_timer_temp.c: 76: case_loop_counter=LED_L4;
	movlw	(04h)
	movwf	(??_led_state_machine+0)+0
	movf	(??_led_state_machine+0)+0,w
	movwf	(led_state_machine@case_loop_counter)
	line	77
	
l5077:	
;led_func_led_timer_temp.c: 77: timer_set(TIMER_1,10);
	movlw	low(0Ah)
	movwf	(?_timer_set)
	movlw	high(0Ah)
	movwf	((?_timer_set))+1
	movlw	(0)
	fcall	_timer_set
	line	78
;led_func_led_timer_temp.c: 78: }
	goto	l3444
	line	79
	
l3411:	
	goto	l3444
	line	81
;led_func_led_timer_temp.c: 79: else
;led_func_led_timer_temp.c: 80: {
	
l3412:	
	goto	l3444
	
l3410:	
	line	82
;led_func_led_timer_temp.c: 81: }
;led_func_led_timer_temp.c: 82: break;
	goto	l3444
	line	85
;led_func_led_timer_temp.c: 83: }
;led_func_led_timer_temp.c: 85: case LED_L4:
	
l3413:	
	line	87
;led_func_led_timer_temp.c: 86: {
;led_func_led_timer_temp.c: 87: if ((RE0 == 1) && (led_timer_temp == 0))
	btfss	(72/8),(72)&7
	goto	u2921
	goto	u2920
u2921:
	goto	l5085
u2920:
	
l5079:	
	movf	((led_state_machine@led_timer_temp+1)),w
	iorwf	((led_state_machine@led_timer_temp)),w
	skipz
	goto	u2931
	goto	u2930
u2931:
	goto	l5085
u2930:
	line	89
	
l5081:	
;led_func_led_timer_temp.c: 88: {
;led_func_led_timer_temp.c: 89: PORTD <<= 1;
	clrc
	rlf	(8),f	;volatile

	line	90
;led_func_led_timer_temp.c: 90: case_loop_counter=LED_L3;
	movlw	(03h)
	movwf	(??_led_state_machine+0)+0
	movf	(??_led_state_machine+0)+0,w
	movwf	(led_state_machine@case_loop_counter)
	line	91
	
l5083:	
;led_func_led_timer_temp.c: 91: timer_set(TIMER_1,10);
	movlw	low(0Ah)
	movwf	(?_timer_set)
	movlw	high(0Ah)
	movwf	((?_timer_set))+1
	movlw	(0)
	fcall	_timer_set
	line	92
;led_func_led_timer_temp.c: 92: }
	goto	l3444
	line	93
	
l3414:	
	
l5085:	
;led_func_led_timer_temp.c: 93: else if(led_timer_temp == 0)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	((led_state_machine@led_timer_temp+1)),w
	iorwf	((led_state_machine@led_timer_temp)),w
	skipz
	goto	u2941
	goto	u2940
u2941:
	goto	l3444
u2940:
	line	95
	
l5087:	
;led_func_led_timer_temp.c: 94: {
;led_func_led_timer_temp.c: 95: PORTD >>= 1;
	clrc
	rrf	(8),f	;volatile

	line	96
;led_func_led_timer_temp.c: 96: case_loop_counter=LED_L5;
	movlw	(05h)
	movwf	(??_led_state_machine+0)+0
	movf	(??_led_state_machine+0)+0,w
	movwf	(led_state_machine@case_loop_counter)
	line	97
	
l5089:	
;led_func_led_timer_temp.c: 97: timer_set(TIMER_1,10);
	movlw	low(0Ah)
	movwf	(?_timer_set)
	movlw	high(0Ah)
	movwf	((?_timer_set))+1
	movlw	(0)
	fcall	_timer_set
	line	98
;led_func_led_timer_temp.c: 98: }
	goto	l3444
	line	99
	
l3416:	
	goto	l3444
	line	101
;led_func_led_timer_temp.c: 99: else
;led_func_led_timer_temp.c: 100: {
	
l3417:	
	goto	l3444
	
l3415:	
	line	102
;led_func_led_timer_temp.c: 101: }
;led_func_led_timer_temp.c: 102: break;
	goto	l3444
	line	105
;led_func_led_timer_temp.c: 103: }
;led_func_led_timer_temp.c: 105: case LED_L5:
	
l3418:	
	line	107
;led_func_led_timer_temp.c: 106: {
;led_func_led_timer_temp.c: 107: if ((RE0 == 1) && (led_timer_temp == 0))
	btfss	(72/8),(72)&7
	goto	u2951
	goto	u2950
u2951:
	goto	l5097
u2950:
	
l5091:	
	movf	((led_state_machine@led_timer_temp+1)),w
	iorwf	((led_state_machine@led_timer_temp)),w
	skipz
	goto	u2961
	goto	u2960
u2961:
	goto	l5097
u2960:
	line	109
	
l5093:	
;led_func_led_timer_temp.c: 108: {
;led_func_led_timer_temp.c: 109: PORTD <<= 1;
	clrc
	rlf	(8),f	;volatile

	line	110
;led_func_led_timer_temp.c: 110: case_loop_counter=LED_L4;
	movlw	(04h)
	movwf	(??_led_state_machine+0)+0
	movf	(??_led_state_machine+0)+0,w
	movwf	(led_state_machine@case_loop_counter)
	line	111
	
l5095:	
;led_func_led_timer_temp.c: 111: timer_set(TIMER_1,10);
	movlw	low(0Ah)
	movwf	(?_timer_set)
	movlw	high(0Ah)
	movwf	((?_timer_set))+1
	movlw	(0)
	fcall	_timer_set
	line	112
;led_func_led_timer_temp.c: 112: }
	goto	l3444
	line	113
	
l3419:	
	
l5097:	
;led_func_led_timer_temp.c: 113: else if(led_timer_temp == 0)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	((led_state_machine@led_timer_temp+1)),w
	iorwf	((led_state_machine@led_timer_temp)),w
	skipz
	goto	u2971
	goto	u2970
u2971:
	goto	l3444
u2970:
	line	115
	
l5099:	
;led_func_led_timer_temp.c: 114: {
;led_func_led_timer_temp.c: 115: PORTD >>= 1;
	clrc
	rrf	(8),f	;volatile

	line	116
;led_func_led_timer_temp.c: 116: case_loop_counter=LED_L6;
	movlw	(06h)
	movwf	(??_led_state_machine+0)+0
	movf	(??_led_state_machine+0)+0,w
	movwf	(led_state_machine@case_loop_counter)
	line	117
	
l5101:	
;led_func_led_timer_temp.c: 117: timer_set(TIMER_1,10);
	movlw	low(0Ah)
	movwf	(?_timer_set)
	movlw	high(0Ah)
	movwf	((?_timer_set))+1
	movlw	(0)
	fcall	_timer_set
	line	118
;led_func_led_timer_temp.c: 118: }
	goto	l3444
	line	119
	
l3421:	
	goto	l3444
	line	121
;led_func_led_timer_temp.c: 119: else
;led_func_led_timer_temp.c: 120: {
	
l3422:	
	goto	l3444
	
l3420:	
	line	122
;led_func_led_timer_temp.c: 121: }
;led_func_led_timer_temp.c: 122: break;
	goto	l3444
	line	125
;led_func_led_timer_temp.c: 123: }
;led_func_led_timer_temp.c: 125: case LED_L6:
	
l3423:	
	line	127
;led_func_led_timer_temp.c: 126: {
;led_func_led_timer_temp.c: 127: if ((RE0 == 1) && (led_timer_temp == 0))
	btfss	(72/8),(72)&7
	goto	u2981
	goto	u2980
u2981:
	goto	l5109
u2980:
	
l5103:	
	movf	((led_state_machine@led_timer_temp+1)),w
	iorwf	((led_state_machine@led_timer_temp)),w
	skipz
	goto	u2991
	goto	u2990
u2991:
	goto	l5109
u2990:
	line	129
	
l5105:	
;led_func_led_timer_temp.c: 128: {
;led_func_led_timer_temp.c: 129: PORTD <<= 1;
	clrc
	rlf	(8),f	;volatile

	line	130
;led_func_led_timer_temp.c: 130: case_loop_counter=LED_L5;
	movlw	(05h)
	movwf	(??_led_state_machine+0)+0
	movf	(??_led_state_machine+0)+0,w
	movwf	(led_state_machine@case_loop_counter)
	line	131
	
l5107:	
;led_func_led_timer_temp.c: 131: timer_set(TIMER_1,10);
	movlw	low(0Ah)
	movwf	(?_timer_set)
	movlw	high(0Ah)
	movwf	((?_timer_set))+1
	movlw	(0)
	fcall	_timer_set
	line	132
;led_func_led_timer_temp.c: 132: }
	goto	l3444
	line	133
	
l3424:	
	
l5109:	
;led_func_led_timer_temp.c: 133: else if(led_timer_temp == 0)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	((led_state_machine@led_timer_temp+1)),w
	iorwf	((led_state_machine@led_timer_temp)),w
	skipz
	goto	u3001
	goto	u3000
u3001:
	goto	l3444
u3000:
	line	135
	
l5111:	
;led_func_led_timer_temp.c: 134: {
;led_func_led_timer_temp.c: 135: PORTD >>= 1;
	clrc
	rrf	(8),f	;volatile

	line	136
;led_func_led_timer_temp.c: 136: case_loop_counter=LED_L7;
	movlw	(07h)
	movwf	(??_led_state_machine+0)+0
	movf	(??_led_state_machine+0)+0,w
	movwf	(led_state_machine@case_loop_counter)
	line	137
	
l5113:	
;led_func_led_timer_temp.c: 137: timer_set(TIMER_1,10);
	movlw	low(0Ah)
	movwf	(?_timer_set)
	movlw	high(0Ah)
	movwf	((?_timer_set))+1
	movlw	(0)
	fcall	_timer_set
	line	138
;led_func_led_timer_temp.c: 138: }
	goto	l3444
	line	139
	
l3426:	
	goto	l3444
	line	141
;led_func_led_timer_temp.c: 139: else
;led_func_led_timer_temp.c: 140: {
	
l3427:	
	goto	l3444
	
l3425:	
	line	142
;led_func_led_timer_temp.c: 141: }
;led_func_led_timer_temp.c: 142: break;
	goto	l3444
	line	144
;led_func_led_timer_temp.c: 143: }
;led_func_led_timer_temp.c: 144: case LED_L7:
	
l3428:	
	line	146
;led_func_led_timer_temp.c: 145: {
;led_func_led_timer_temp.c: 146: if ((RE0 == 1) && (led_timer_temp == 0))
	btfss	(72/8),(72)&7
	goto	u3011
	goto	u3010
u3011:
	goto	l5121
u3010:
	
l5115:	
	movf	((led_state_machine@led_timer_temp+1)),w
	iorwf	((led_state_machine@led_timer_temp)),w
	skipz
	goto	u3021
	goto	u3020
u3021:
	goto	l5121
u3020:
	line	148
	
l5117:	
;led_func_led_timer_temp.c: 147: {
;led_func_led_timer_temp.c: 148: PORTD <<= 1;
	clrc
	rlf	(8),f	;volatile

	line	149
;led_func_led_timer_temp.c: 149: case_loop_counter=LED_L6;
	movlw	(06h)
	movwf	(??_led_state_machine+0)+0
	movf	(??_led_state_machine+0)+0,w
	movwf	(led_state_machine@case_loop_counter)
	line	150
	
l5119:	
;led_func_led_timer_temp.c: 150: timer_set(TIMER_1,10);
	movlw	low(0Ah)
	movwf	(?_timer_set)
	movlw	high(0Ah)
	movwf	((?_timer_set))+1
	movlw	(0)
	fcall	_timer_set
	line	151
;led_func_led_timer_temp.c: 151: }
	goto	l3444
	line	152
	
l3429:	
	
l5121:	
;led_func_led_timer_temp.c: 152: else if(led_timer_temp == 0)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	((led_state_machine@led_timer_temp+1)),w
	iorwf	((led_state_machine@led_timer_temp)),w
	skipz
	goto	u3031
	goto	u3030
u3031:
	goto	l3444
u3030:
	line	154
	
l5123:	
;led_func_led_timer_temp.c: 153: {
;led_func_led_timer_temp.c: 154: PORTD >>= 1;
	clrc
	rrf	(8),f	;volatile

	line	155
;led_func_led_timer_temp.c: 155: case_loop_counter=LED_L8;
	movlw	(08h)
	movwf	(??_led_state_machine+0)+0
	movf	(??_led_state_machine+0)+0,w
	movwf	(led_state_machine@case_loop_counter)
	line	156
	
l5125:	
;led_func_led_timer_temp.c: 156: timer_set(TIMER_1,10);
	movlw	low(0Ah)
	movwf	(?_timer_set)
	movlw	high(0Ah)
	movwf	((?_timer_set))+1
	movlw	(0)
	fcall	_timer_set
	line	157
;led_func_led_timer_temp.c: 157: }
	goto	l3444
	line	158
	
l3431:	
	goto	l3444
	line	160
;led_func_led_timer_temp.c: 158: else
;led_func_led_timer_temp.c: 159: {
	
l3432:	
	goto	l3444
	
l3430:	
	line	161
;led_func_led_timer_temp.c: 160: }
;led_func_led_timer_temp.c: 161: break;
	goto	l3444
	line	164
;led_func_led_timer_temp.c: 162: }
;led_func_led_timer_temp.c: 164: case LED_L8:
	
l3433:	
	line	166
;led_func_led_timer_temp.c: 165: {
;led_func_led_timer_temp.c: 166: if ((RE0 == 1) && (led_timer_temp == 0))
	btfss	(72/8),(72)&7
	goto	u3041
	goto	u3040
u3041:
	goto	l5133
u3040:
	
l5127:	
	movf	((led_state_machine@led_timer_temp+1)),w
	iorwf	((led_state_machine@led_timer_temp)),w
	skipz
	goto	u3051
	goto	u3050
u3051:
	goto	l5133
u3050:
	line	168
	
l5129:	
;led_func_led_timer_temp.c: 167: {
;led_func_led_timer_temp.c: 168: PORTD <<= 1;
	clrc
	rlf	(8),f	;volatile

	line	169
;led_func_led_timer_temp.c: 169: case_loop_counter=LED_L7;
	movlw	(07h)
	movwf	(??_led_state_machine+0)+0
	movf	(??_led_state_machine+0)+0,w
	movwf	(led_state_machine@case_loop_counter)
	line	170
	
l5131:	
;led_func_led_timer_temp.c: 170: timer_set(TIMER_1,10);
	movlw	low(0Ah)
	movwf	(?_timer_set)
	movlw	high(0Ah)
	movwf	((?_timer_set))+1
	movlw	(0)
	fcall	_timer_set
	line	171
;led_func_led_timer_temp.c: 171: }
	goto	l3444
	line	172
	
l3434:	
	
l5133:	
;led_func_led_timer_temp.c: 172: else if(led_timer_temp == 0)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	((led_state_machine@led_timer_temp+1)),w
	iorwf	((led_state_machine@led_timer_temp)),w
	skipz
	goto	u3061
	goto	u3060
u3061:
	goto	l3444
u3060:
	line	174
	
l5135:	
;led_func_led_timer_temp.c: 173: {
;led_func_led_timer_temp.c: 174: PORTD >>= 1;
	clrc
	rrf	(8),f	;volatile

	line	175
;led_func_led_timer_temp.c: 175: case_loop_counter=LED_END;
	movlw	(09h)
	movwf	(??_led_state_machine+0)+0
	movf	(??_led_state_machine+0)+0,w
	movwf	(led_state_machine@case_loop_counter)
	line	176
	
l5137:	
;led_func_led_timer_temp.c: 176: timer_set(TIMER_1,10);
	movlw	low(0Ah)
	movwf	(?_timer_set)
	movlw	high(0Ah)
	movwf	((?_timer_set))+1
	movlw	(0)
	fcall	_timer_set
	line	177
;led_func_led_timer_temp.c: 177: }
	goto	l3444
	line	178
	
l3436:	
	goto	l3444
	line	180
;led_func_led_timer_temp.c: 178: else
;led_func_led_timer_temp.c: 179: {
	
l3437:	
	goto	l3444
	
l3435:	
	line	181
;led_func_led_timer_temp.c: 180: }
;led_func_led_timer_temp.c: 181: break;
	goto	l3444
	line	184
;led_func_led_timer_temp.c: 182: }
;led_func_led_timer_temp.c: 184: case LED_END:
	
l3438:	
	line	186
;led_func_led_timer_temp.c: 185: {
;led_func_led_timer_temp.c: 186: if ((RE0 == 1) && (led_timer_temp == 0))
	btfss	(72/8),(72)&7
	goto	u3071
	goto	u3070
u3071:
	goto	l5145
u3070:
	
l5139:	
	movf	((led_state_machine@led_timer_temp+1)),w
	iorwf	((led_state_machine@led_timer_temp)),w
	skipz
	goto	u3081
	goto	u3080
u3081:
	goto	l5145
u3080:
	line	188
	
l5141:	
;led_func_led_timer_temp.c: 187: {
;led_func_led_timer_temp.c: 188: PORTD <<= 1;
	clrc
	rlf	(8),f	;volatile

	line	189
;led_func_led_timer_temp.c: 189: case_loop_counter=LED_L8;
	movlw	(08h)
	movwf	(??_led_state_machine+0)+0
	movf	(??_led_state_machine+0)+0,w
	movwf	(led_state_machine@case_loop_counter)
	line	190
	
l5143:	
;led_func_led_timer_temp.c: 190: timer_set(TIMER_1,10);
	movlw	low(0Ah)
	movwf	(?_timer_set)
	movlw	high(0Ah)
	movwf	((?_timer_set))+1
	movlw	(0)
	fcall	_timer_set
	line	191
;led_func_led_timer_temp.c: 191: }
	goto	l3444
	line	192
	
l3439:	
	
l5145:	
;led_func_led_timer_temp.c: 192: else if(led_timer_temp == 0)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	((led_state_machine@led_timer_temp+1)),w
	iorwf	((led_state_machine@led_timer_temp)),w
	skipz
	goto	u3091
	goto	u3090
u3091:
	goto	l3444
u3090:
	line	194
	
l5147:	
;led_func_led_timer_temp.c: 193: {
;led_func_led_timer_temp.c: 194: PORTD == 0x01;
	movf	(8),w	;volatile
	line	195
;led_func_led_timer_temp.c: 195: case_loop_counter=LED_END;
	movlw	(09h)
	movwf	(??_led_state_machine+0)+0
	movf	(??_led_state_machine+0)+0,w
	movwf	(led_state_machine@case_loop_counter)
	line	196
	
l5149:	
;led_func_led_timer_temp.c: 196: timer_set(TIMER_1,10);
	movlw	low(0Ah)
	movwf	(?_timer_set)
	movlw	high(0Ah)
	movwf	((?_timer_set))+1
	movlw	(0)
	fcall	_timer_set
	line	197
;led_func_led_timer_temp.c: 197: }
	goto	l3444
	line	198
	
l3441:	
	goto	l3444
	line	200
;led_func_led_timer_temp.c: 198: else
;led_func_led_timer_temp.c: 199: {
	
l3442:	
	goto	l3444
	
l3440:	
	line	201
;led_func_led_timer_temp.c: 200: }
;led_func_led_timer_temp.c: 201: break;
	goto	l3444
	line	204
;led_func_led_timer_temp.c: 202: }
;led_func_led_timer_temp.c: 204: default:
	
l3443:	
	line	205
;led_func_led_timer_temp.c: 205: break;
	goto	l3444
	line	207
	
l5151:	
;led_func_led_timer_temp.c: 207: }
	goto	l3444
	line	33
	
l3400:	
	
l5153:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(led_state_machine@case_loop_counter),w
	; Switch size 1, requested type "space"
; Number of cases is 9, Range of values is 1 to 9
; switch strategies available:
; Name         Bytes Cycles
; simple_byte    28    15 (average)
; direct_byte    49    22 (fixed)
;	Chosen strategy is simple_byte

	xorlw	1^0	; case 1
	skipnz
	goto	l5045
	xorlw	2^1	; case 2
	skipnz
	goto	l3403
	xorlw	3^2	; case 3
	skipnz
	goto	l3408
	xorlw	4^3	; case 4
	skipnz
	goto	l3413
	xorlw	5^4	; case 5
	skipnz
	goto	l3418
	xorlw	6^5	; case 6
	skipnz
	goto	l3423
	xorlw	7^6	; case 7
	skipnz
	goto	l3428
	xorlw	8^7	; case 8
	skipnz
	goto	l3433
	xorlw	9^8	; case 9
	skipnz
	goto	l3438
	goto	l3444

	line	207
	
l3402:	
	line	208
	
l3444:	
	return
	opt stack 0
GLOBAL	__end_of_led_state_machine
	__end_of_led_state_machine:
;; =============== function _led_state_machine ends ============

	signat	_led_state_machine,88
	global	_get_timer
psect	text277,local,class=CODE,delta=2
global __ptext277
__ptext277:

;; *************** function _get_timer *****************
;; Defined at:
;;		line 33 in file "C:\pic_projects\LED_ROTATE\timer.c"
;; Parameters:    Size  Location     Type
;;  index           1    wreg     unsigned char 
;; Auto vars:     Size  Location     Type
;;  index           1    5[BANK0 ] unsigned char 
;;  result          2    3[BANK0 ] unsigned short 
;; Return value:  Size  Location     Type
;;                  2    0[BANK0 ] unsigned short 
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       2       0       0       0
;;      Locals:         0       3       0       0       0
;;      Temps:          0       1       0       0       0
;;      Totals:         0       6       0       0       0
;;Total ram usage:        6 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_led_state_machine
;; This function uses a non-reentrant model
;;
psect	text277
	file	"C:\pic_projects\LED_ROTATE\timer.c"
	line	33
	global	__size_of_get_timer
	__size_of_get_timer	equ	__end_of_get_timer-_get_timer
	
_get_timer:	
	opt	stack 4
; Regs used in _get_timer: [wreg-fsr0h+status,2+status,0]
;get_timer@index stored from wreg
	line	36
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(get_timer@index)
	
l5029:	
;timer.c: 34: uint16_t result;
;timer.c: 36: if (index < TIMER_MAX)
	movlw	(02h)
	subwf	(get_timer@index),w
	skipnc
	goto	u2851
	goto	u2850
u2851:
	goto	l5037
u2850:
	line	38
	
l5031:	
;timer.c: 37: {
;timer.c: 38: (GIE = 0);
	bcf	(95/8),(95)&7
	line	39
	
l5033:	
;timer.c: 39: result = timer_array[index];
	movf	(get_timer@index),w
	movwf	(??_get_timer+0)+0
	addwf	(??_get_timer+0)+0,w
	addlw	_timer_array&0ffh
	movwf	fsr0
	bcf	status, 7	;select IRP bank0
	movf	indf,w
	movwf	(get_timer@result)
	incf	fsr0,f
	movf	indf,w
	movwf	(get_timer@result+1)
	line	40
	
l5035:	
;timer.c: 40: (GIE = 1);
	bsf	(95/8),(95)&7
	line	41
;timer.c: 41: }
	goto	l5039
	line	42
	
l1702:	
	line	44
	
l5037:	
;timer.c: 42: else
;timer.c: 43: {
;timer.c: 44: result = 0;
	movlw	low(0)
	movwf	(get_timer@result)
	movlw	high(0)
	movwf	((get_timer@result))+1
	goto	l5039
	line	45
	
l1703:	
	line	47
	
l5039:	
;timer.c: 45: }
;timer.c: 47: return result;
	movf	(get_timer@result+1),w
	clrf	(?_get_timer+1)
	addwf	(?_get_timer+1)
	movf	(get_timer@result),w
	clrf	(?_get_timer)
	addwf	(?_get_timer)

	goto	l1704
	
l5041:	
	line	49
	
l1704:	
	return
	opt stack 0
GLOBAL	__end_of_get_timer
	__end_of_get_timer:
;; =============== function _get_timer ends ============

	signat	_get_timer,4218
	global	_timer_set
psect	text278,local,class=CODE,delta=2
global __ptext278
__ptext278:

;; *************** function _timer_set *****************
;; Defined at:
;;		line 18 in file "C:\pic_projects\LED_ROTATE\timer.c"
;; Parameters:    Size  Location     Type
;;  index           1    wreg     unsigned char 
;;  value           2    0[BANK0 ] unsigned short 
;; Auto vars:     Size  Location     Type
;;  index           1    3[BANK0 ] unsigned char 
;;  array_conten    2    0        unsigned short 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       2       0       0       0
;;      Locals:         0       1       0       0       0
;;      Temps:          0       1       0       0       0
;;      Totals:         0       4       0       0       0
;;Total ram usage:        4 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_led_state_machine
;; This function uses a non-reentrant model
;;
psect	text278
	file	"C:\pic_projects\LED_ROTATE\timer.c"
	line	18
	global	__size_of_timer_set
	__size_of_timer_set	equ	__end_of_timer_set-_timer_set
	
_timer_set:	
	opt	stack 4
; Regs used in _timer_set: [wreg-fsr0h+status,2+status,0]
;timer_set@index stored from wreg
	line	20
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(timer_set@index)
	
l5021:	
;timer.c: 19: uint16_t array_contents;
;timer.c: 20: if (index < TIMER_MAX)
	movlw	(02h)
	subwf	(timer_set@index),w
	skipnc
	goto	u2841
	goto	u2840
u2841:
	goto	l1699
u2840:
	line	22
	
l5023:	
;timer.c: 21: {
;timer.c: 22: (GIE = 0);
	bcf	(95/8),(95)&7
	line	23
	
l5025:	
;timer.c: 23: timer_array[index] = value;
	movf	(timer_set@index),w
	movwf	(??_timer_set+0)+0
	addwf	(??_timer_set+0)+0,w
	addlw	_timer_array&0ffh
	movwf	fsr0
	movf	(timer_set@value),w
	bcf	status, 7	;select IRP bank0
	movwf	indf
	incf	fsr0,f
	movf	(timer_set@value+1),w
	movwf	indf
	line	24
	
l5027:	
;timer.c: 24: (GIE = 1);
	bsf	(95/8),(95)&7
	line	25
;timer.c: 25: }
	goto	l1699
	line	26
	
l1697:	
	goto	l1699
	line	28
;timer.c: 26: else
;timer.c: 27: {
	
l1698:	
	line	29
	
l1699:	
	return
	opt stack 0
GLOBAL	__end_of_timer_set
	__end_of_timer_set:
;; =============== function _timer_set ends ============

	signat	_timer_set,8312
	global	_sfreg
psect	text279,local,class=CODE,delta=2
global __ptext279
__ptext279:

;; *************** function _sfreg *****************
;; Defined at:
;;		line 71 in file "C:\pic_projects\LED_ROTATE\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text279
	file	"C:\pic_projects\LED_ROTATE\main.c"
	line	71
	global	__size_of_sfreg
	__size_of_sfreg	equ	__end_of_sfreg-_sfreg
	
_sfreg:	
	opt	stack 5
; Regs used in _sfreg: [wreg+status,2]
	line	72
	
l5011:	
;main.c: 72: WDTCON = 0x17;
	movlw	(017h)
	bcf	status, 5	;RP0=0, select bank2
	bsf	status, 6	;RP1=1, select bank2
	movwf	(261)^0100h	;volatile
	line	73
;main.c: 73: OSCCON = 0x61;
	movlw	(061h)
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movwf	(143)^080h	;volatile
	line	74
	
l5013:	
;main.c: 74: OPTION = 0x00;
	clrf	(129)^080h	;volatile
	line	75
	
l5015:	
;main.c: 75: TRISE = 0x01;
	movlw	(01h)
	movwf	(137)^080h	;volatile
	line	76
;main.c: 76: TRISD = 0x00;
	clrf	(136)^080h	;volatile
	line	77
	
l5017:	
;main.c: 77: T1CON = 0x35;
	movlw	(035h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(16)	;volatile
	line	78
	
l5019:	
;main.c: 78: ANSEL &=0x00;
	bsf	status, 5	;RP0=1, select bank3
	bsf	status, 6	;RP1=1, select bank3
	clrf	(392)^0180h	;volatile
	line	79
	
l856:	
	return
	opt stack 0
GLOBAL	__end_of_sfreg
	__end_of_sfreg:
;; =============== function _sfreg ends ============

	signat	_sfreg,88
	global	_interrupt_handler
psect	text280,local,class=CODE,delta=2
global __ptext280
__ptext280:

;; *************** function _interrupt_handler *****************
;; Defined at:
;;		line 6 in file "C:\pic_projects\LED_ROTATE\interrupt.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          4       0       0       0       0
;;      Totals:         4       0       0       0       0
;;Total ram usage:        4 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		_timer_isr
;; This function is called by:
;;		Interrupt level 1
;; This function uses a non-reentrant model
;;
psect	text280
	file	"C:\pic_projects\LED_ROTATE\interrupt.c"
	line	6
	global	__size_of_interrupt_handler
	__size_of_interrupt_handler	equ	__end_of_interrupt_handler-_interrupt_handler
	
_interrupt_handler:	
	opt	stack 4
; Regs used in _interrupt_handler: [wreg-fsr0h+status,2+status,0+pclath+cstack]
psect	intentry,class=CODE,delta=2
global __pintentry
__pintentry:
global interrupt_function
interrupt_function:
	global saved_w
	saved_w	set	btemp+0
	movwf	saved_w
	movf	status,w
	movwf	(??_interrupt_handler+0)
	movf	fsr0,w
	movwf	(??_interrupt_handler+1)
	movf	pclath,w
	movwf	(??_interrupt_handler+2)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	btemp+1,w
	movwf	(??_interrupt_handler+3)
	ljmp	_interrupt_handler
psect	text280
	line	7
	
i1l4875:	
;interrupt.c: 7: timer_isr();
	fcall	_timer_isr
	line	8
	
i1l2557:	
	movf	(??_interrupt_handler+3),w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	btemp+1
	movf	(??_interrupt_handler+2),w
	movwf	pclath
	movf	(??_interrupt_handler+1),w
	movwf	fsr0
	movf	(??_interrupt_handler+0),w
	movwf	status
	swapf	saved_w,f
	swapf	saved_w,w
	retfie
	opt stack 0
GLOBAL	__end_of_interrupt_handler
	__end_of_interrupt_handler:
;; =============== function _interrupt_handler ends ============

	signat	_interrupt_handler,88
	global	_timer_isr
psect	text281,local,class=CODE,delta=2
global __ptext281
__ptext281:

;; *************** function _timer_isr *****************
;; Defined at:
;;		line 54 in file "C:\pic_projects\LED_ROTATE\timer.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  i               1    1[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         1       0       0       0       0
;;      Temps:          1       0       0       0       0
;;      Totals:         2       0       0       0       0
;;Total ram usage:        2 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_interrupt_handler
;; This function uses a non-reentrant model
;;
psect	text281
	file	"C:\pic_projects\LED_ROTATE\timer.c"
	line	54
	global	__size_of_timer_isr
	__size_of_timer_isr	equ	__end_of_timer_isr-_timer_isr
	
_timer_isr:	
	opt	stack 4
; Regs used in _timer_isr: [wreg-fsr0h+status,2+status,0]
	line	56
	
i1l4837:	
;timer.c: 55: uint8_t i;
;timer.c: 56: if((TMR1IE)&&(TMR1IF))
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	btfss	(1120/8)^080h,(1120)&7
	goto	u251_21
	goto	u251_20
u251_21:
	goto	i1l1713
u251_20:
	
i1l4839:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfss	(96/8),(96)&7
	goto	u252_21
	goto	u252_20
u252_21:
	goto	i1l1713
u252_20:
	line	59
	
i1l4841:	
;timer.c: 57: {
;timer.c: 59: TMR1IF=0;
	bcf	(96/8),(96)&7
	line	60
	
i1l4843:	
;timer.c: 60: T1CON &= ~(0x01);
	movlw	(0FEh)
	movwf	(??_timer_isr+0)+0
	movf	(??_timer_isr+0)+0,w
	andwf	(16),f	;volatile
	line	61
	
i1l4845:	
;timer.c: 61: TMR1L = 0x89;
	movlw	(089h)
	movwf	(14)	;volatile
	line	62
	
i1l4847:	
;timer.c: 62: TMR1H = 0xFF;
	movlw	(0FFh)
	movwf	(15)	;volatile
	line	63
	
i1l4849:	
;timer.c: 63: T1CON |= 0x01;
	bsf	(16)+(0/8),(0)&7	;volatile
	line	65
;timer.c: 65: for (i = 0; i < TIMER_MAX; i++)
	clrc
	movlw	0
	btfsc	status,0
	movlw	1
	movwf	(timer_isr@i)
	
i1l4851:	
	movlw	(02h)
	subwf	(timer_isr@i),w
	skipc
	goto	u253_21
	goto	u253_20
u253_21:
	goto	i1l4855
u253_20:
	goto	i1l1713
	
i1l4853:	
	goto	i1l1713
	line	66
	
i1l1708:	
	line	67
	
i1l4855:	
;timer.c: 66: {
;timer.c: 67: if (timer_array[i] != 0)
	movf	(timer_isr@i),w
	movwf	(??_timer_isr+0)+0
	addwf	(??_timer_isr+0)+0,w
	addlw	_timer_array&0ffh
	movwf	fsr0
	bcf	status, 7	;select IRP bank0
	movf	indf,w
	incf	fsr0,f
	iorwf	indf,w
	skipnz
	goto	u254_21
	goto	u254_20
u254_21:
	goto	i1l4859
u254_20:
	line	69
	
i1l4857:	
;timer.c: 68: {
;timer.c: 69: timer_array[i]--;
	movf	(timer_isr@i),w
	movwf	(??_timer_isr+0)+0
	addwf	(??_timer_isr+0)+0,w
	addlw	_timer_array&0ffh
	movwf	fsr0
	movlw	low(01h)
	subwf	indf,f
	incfsz	fsr0,f
	movlw	high(01h)
	skipc
	decf	indf,f
	subwf	indf,f
	decf	fsr0,f
	line	70
;timer.c: 70: }
	goto	i1l4859
	line	71
	
i1l1710:	
	goto	i1l4859
	line	73
;timer.c: 71: else
;timer.c: 72: {
	
i1l1711:	
	line	65
	
i1l4859:	
	movlw	(01h)
	movwf	(??_timer_isr+0)+0
	movf	(??_timer_isr+0)+0,w
	addwf	(timer_isr@i),f
	
i1l4861:	
	movlw	(02h)
	subwf	(timer_isr@i),w
	skipc
	goto	u255_21
	goto	u255_20
u255_21:
	goto	i1l4855
u255_20:
	goto	i1l1713
	
i1l1709:	
	line	76
;timer.c: 73: }
;timer.c: 74: }
;timer.c: 76: }
	goto	i1l1713
	line	77
	
i1l1707:	
	goto	i1l1713
	line	79
;timer.c: 77: else
;timer.c: 78: {
	
i1l1712:	
	line	80
	
i1l1713:	
	return
	opt stack 0
GLOBAL	__end_of_timer_isr
	__end_of_timer_isr:
;; =============== function _timer_isr ends ============

	signat	_timer_isr,88
psect	text282,local,class=CODE,delta=2
global __ptext282
__ptext282:
	global	btemp
	btemp set 07Eh

	DABS	1,126,2	;btemp
	global	wtemp0
	wtemp0 set btemp
	end
