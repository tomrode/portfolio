
#include "typedefs.h"
#include "htc.h"
#include "timer.h"
#include "led_func.h"
#define TIMER1_VALUE 300
/* CONSTANTS*/


/*VARIABLES*/


/*function prototypes*/

void led_state_machine(void)
{
static uint8_t case_loop_counter = LED_L1;
uint8_t temp;
switch (case_loop_counter)
{
     case LED_L1:
     {
     PORTD = 0x00;
     PORTD |= 0x80;              //  RD7 ON
     case_loop_counter=LED_L2;   // Set up for RD6 to be on
     timer_set(TIMER_1,TIMER1_VALUE);
     break;
     }

     case LED_L2:
     {
     if ((RE0 == 1) && (get_timer(TIMER_1) == 0))
     { 
     //PORTD <<= 1;
     case_loop_counter=LED_L1;   // Set up for RD6 to be on
     timer_set(TIMER_1,TIMER1_VALUE);
     }
     else if(get_timer(TIMER_1) == 0)
     {
      PORTD >>= 1;                // RD6 ON
      case_loop_counter=LED_L3;   // Set up for RD6 to be on
      timer_set(TIMER_1,TIMER1_VALUE);
     }
     else
     {
     }
      break;
     }     

     case LED_L3:
     {
     if ((RE0 == 1) && (get_timer(TIMER_1) == 0))
     { 
     PORTD <<= 1;
     case_loop_counter=LED_L2;   // Set up for RD6 to be on
     timer_set(TIMER_1,TIMER1_VALUE);
     }
     else if(get_timer(TIMER_1) == 0)
     {
      PORTD >>= 1;                //RD5 On
      case_loop_counter=LED_L4;   // Set up for RD6 to be on
      timer_set(TIMER_1,TIMER1_VALUE);
     }
     else
     {
     }
      break; 
     }

     case LED_L4:
     {
     if ((RE0 == 1) && (get_timer(TIMER_1) == 0))
     { 
     PORTD <<= 1;
     case_loop_counter=LED_L3;   // Set up for RD6 to be on
     timer_set(TIMER_1,TIMER1_VALUE);
     }
     else if(get_timer(TIMER_1) == 0)
     {
      PORTD >>= 1;                //RD4 On
      case_loop_counter=LED_L5;
      timer_set(TIMER_1,TIMER1_VALUE);
	 }
     else
     {
     }
      break;
     }
      
     case LED_L5:
     {
     if ((RE0 == 1) && (get_timer(TIMER_1) == 0))
     { 
     PORTD <<= 1;
     case_loop_counter=LED_L4;   // Set up for RD6 to be on
     timer_set(TIMER_1,TIMER1_VALUE);
     }
     else if(get_timer(TIMER_1) == 0)
     {
      PORTD >>= 1;                  //RD3 ON
      case_loop_counter=LED_L6;
      timer_set(TIMER_1,TIMER1_VALUE);
	 }
     else
     {
     }
      break;
     }
     
     case LED_L6:
     {
     if ((RE0 == 1) && (get_timer(TIMER_1) == 0))
     { 
     PORTD <<= 1;
     case_loop_counter=LED_L5;   // Set up for RD6 to be on
     timer_set(TIMER_1,TIMER1_VALUE);
     }
     else if(get_timer(TIMER_1) == 0)
     {
      PORTD >>= 1;                 //RD2 On
      case_loop_counter=LED_L7;
      timer_set(TIMER_1,TIMER1_VALUE);
	 }
     else
     {
     }
      break;
     }
     case LED_L7:
     {
     if ((RE0 == 1) && (get_timer(TIMER_1) == 0))
     { 
     PORTD <<= 1;
     case_loop_counter=LED_L6;   // RD1 on
     timer_set(TIMER_1,TIMER1_VALUE);
     }
     else if(get_timer(TIMER_1) == 0)
     {
      PORTD >>= 1;                //RD1 On
      case_loop_counter=LED_L8;
      timer_set(TIMER_1,TIMER1_VALUE);
	 }
     else
     {
     }
      break;
     }

      case LED_L8:
     {
     if ((RE0 == 1) && (get_timer(TIMER_1) == 0))
     { 
     PORTD <<= 1;
     case_loop_counter=LED_L7;   // Set up for RD6 to be on
     timer_set(TIMER_1,TIMER1_VALUE);
     } 
     else if(get_timer(TIMER_1) == 0)
     {
      PORTD >>= 1;
      case_loop_counter=LED_END;
      timer_set(TIMER_1,TIMER1_VALUE);
	 }
     else
     {
     }
      break;
     }
      
      case LED_END:
     {
     if ((RE0 == 1) && (get_timer(TIMER_1) == 0))
     { 
     PORTD <<= 1;
     case_loop_counter=LED_L8;   // Set up for RD6 to be on
     timer_set(TIMER_1,TIMER1_VALUE);
     } 
     else if(get_timer(TIMER_1) == 0)
     {
      PORTD == 0x01;
      case_loop_counter=LED_END;
      timer_set(TIMER_1,TIMER1_VALUE);
	 }
     else
     {
     }
      break;
     }

     default:
     break;

}  
}

