        list p=16F887, b=8, c= 102, n=71, t=on, st=off, f=inhx32
;******************************************************************
;*                                                                *
;*  Filename: 16F887 Boot 24.asm                                  *
;*    Author: Mike McLaren, K8LH                                  *
;*      Date: 02-May-08  (last rev. 10-Aug-08)                    *
;*                                                                *
;*   16F887 Boot Loader resides in memory at 0000..00FF and       *
;*   will write program (flash) and data (eeprom) memory from     *
;*   a hex file downloaded via the serial port @ 19200 baud.      *
;*                                                                *
;*   hex file records are copied into and written to program      *
;*   memory from a 16 word 'row' buffer (double buffered) to      *
;*   allow processing of native hex files which may have gaps     *
;*   or holes in program memory space.                            *
;*                                                                *
;*                                                                *
;*     MPLab: 8.01    (tabs=8)                                    *
;*     MPAsm: 5.15                                                *
;*                                                                *
;******************************************************************

        processor PIC16F887
        include "p16f887.inc"
        errorlevel -302
        radix   dec
        
;--< config settings >---------------------------------------------

        __CONFIG  _CONFIG1, _LVP_OFF&_FCMEN_OFF&_PWRTE_ON&_WDT_OFF&_INTOSCIO
        __CONFIG  _CONFIG2, _WRT_256
;
;       _DEBUG_OFF      ; default
;       _LVP_OFF
;       _FCMEN_OFF
;       _IESO_ON        ; default
;       _BOR_ON         ; default
;       _CPD_OFF        ; default
;       _CP_OFF         ; default
;       _MCLR_ON        ; default
;       _PWRTE_OFF      ; default
;       _WDT_OFF
;       _INTOSCIO
;
;       _WRT_256        ; default, 0000..00FF write protected
;       _BOR40V         ; default

;--< constants >---------------------------------------------------

clock   equ     8       ; 8 MHz INTOSC
brgval  equ     clock*1000000/19200/16-1        

;--< variables >---------------------------------------------------
;
;  using RAM in Bank 2 [120..17F] because most of the program
;  operation takes place in Bank 2.
;
Temp    equ     h'120'          ; Get_Byte routine
Count   equ     h'121'          ; New_Record routine

Dsize   equ     h'123'          ;
ChkSum  equ     h'124'          ;
ErrFlg  equ     h'125'          ;
Shadow  equ     h'126'          ;

Dcount  equ     h'12C'          ; record data byte count
AddrH   equ     h'12D'          ; record address hi
AddrL   equ     h'12E'          ; record address lo
RType   equ     h'12F'          ; record type
DBUFF   equ     h'130'          ; record data 130..13F

DBLOCK  equ     h'140'          ; row buffer 140..15F


;******************************************************************
;  PIC16F' Reset Vector                                           *
;******************************************************************
        org     h'0000'

Vector_Reset
        clrf    STATUS          ; force bank 0                    |B0
        btfss   PORTC,0         ; spkr jumper open (RC0 = '1')?   |B0
        goto    h'0100'         ; no, branch to user code, else   |B0
     if clock == 8
        goto    Boot1           ; entry for 8 MHz INTOSC          |B0
     else
        goto    Boot2           ; entry for HS crystal osc        |B0
     endif

;******************************************************************
;  PIC16F' Interrupt Vector                                       *
;******************************************************************
        org     h'0004'

Vector_Interrupt
        goto    h'0104'         ; branch to user ISR              |B?

;******************************************************************
;  16F887 bootloader (resides in memory at 0000..00FF             *
;******************************************************************
;
;  select 8-MHz INTOSC and wait until the oscillator is stable
;
Boot1   
        bsf     STATUS,RP0      ; bank 1                          |B1
        movlw   b'01110000'     ;                                 |B1
        movwf   OSCCON          ; select 8-MHz                    |B1
Stable  btfss   OSCCON,HTS      ; INTOSC frequency stable?        |B1
        goto    Stable          ; no, branch, else                |B1
;
;  setup serial port to 19.2 kb
;
Boot2
        bsf     STATUS,RP0      ; bank 1                          |B1
        movlw   brgval          ; 25@8-MHz, 64@20-MHz, etc.       |B1
        movwf   SPBRG           ; 19200 baud                      |B1
        movlw   1<<TXEN|1<<BRGH ; TXEN=1, SYNC=0, BRGH=1, TX9=0   |B1
        movwf   TXSTA           ; Async, 8, 1, none               |B1
        bcf     STATUS,RP0      ; bank 0                          |B0
        movlw   1<<SPEN|1<<CREN ; SPEN=1, TX9=0, CREN=1, ADDEN=0  |B0
        movwf   RCSTA           ; enable serial port              |B0
        movf    RCREG,W         ; flush Rx Buffer                 |B0
        movf    RCREG,W         ;                                 |B0
;
        bsf     STATUS,RP1      ; bank 2 (RP0 already clr)        |B2
        bsf     STATUS,IRP      ; indirect access bank 2 & 3      |B2
        clrf    ErrFlg          ;                                 |B2
        clrf    Dsize           ;                                 |B2
        movlw   sHello%100h     ; print greeting screen           |B2
Prt_Message
        call    Put_String      ; print the string                |B2
;
;
;  New_Record processes a hex record from the serial port.
;
;  HyperTerminal should be set to 19.2 kb, 8 data bits, 1 stop
;  bit, no parity, <Xon>/<Xoff> software handshaking, no local
;  echo, and 1-msec line delay in ASCII setup.
;
;  your PIC programs should org +0x100.  download the hex file
;  using Hyperterminal <Transfer> menu and <Send Text File...>
;  menu item.
;
;  each 'hex record' starts with a colon ":" char, ends with
;  <cr> and <lf> chars, and looks something like this;
;
;  <:><bytecount><addrh><addrl><type><data..data><chksum><cr><lf>
;
;  the <addrh> and <addrl> "byte" address must be converted into
;  an EEADRH/EEADR "word" address before write operations.
;
;  <type> field values
;    00 = Data Record > address and data
;    01 = End of File Record
;    02 = Segment Address Record > ?
;    04 = Linear Address Record > two byte extended address
;         0000=Program, 0020=IDLocs, 0040=Config, 00F0=EEProm
;
;  one Linear Address Record (type=04) appears at the beginning of
;  16F' Hex files (":020000040000FA") with an address of 0000h.
;
;  the 16F887 only supports writing program/flash and data/eeprom
;  memory from within a program.  no IDLOCS or configuration bits.
;
New_Record
        movlw   d'17'           ; the <XON> character             |B2
        call    Put232          ; tell HyperTerminal ok to send   |B2
;
;  dump characters until we receive the ":" start-of-line character
;  then grab <bytecount> and setup our buffer and variables.
;
Start_Char
        call    Get232          ; get char from serial port       |B2
        xorlw   a':'            ; start-of-line character?        |B2
        bnz     Start_Char      ; no, loop                        |B2
        call    Get_Byte        ; get datacount byte              |B2
        movwf   Dcount          ; save Data Count <bytecount>     |B2
        movwf   ChkSum          ; initialize CHKSUM variable      |B2
        addlw   h'03'           ; add 3 for <addrh>, <addrl>,     |B2
        movwf   Count           ; and record <type> bytes         |B2
        movlw   AddrH           ; record buffer address           |B2
        movwf   FSR             ; setup indirect address          |B2
;
;  input <ADDRH>, <ADDRL>, and <TYPE> bytes, fill data buffer
;
GETDAT  call    Get_Byte        ; get a hex byte                  |B2
        addwf   ChkSum,F        ; update CHKSUM                   |B2
        movwf   INDF            ; stuff it in the buffer          |B2
        incf    FSR,F           ; increment buffer pointer        |B2
        decfsz  Count,F         ; more bytes?                     |B2
        goto    GETDAT          ; yes, branch                     |B2
        call    Get_Byte        ; get the CHKSUM byte             |B2
;
;  test for valid checksum
;
        sublw   h'00'           ; make it twos compliment         |B2
        xorwf   ChkSum,W        ; good checksum?                  |B2
        skpz                    ; yes, skip, else                 |B2
        bsf     ErrFlg,0        ; indicate error                  |B2
;
;  tell HyperTerminal to wait while we process the hex record
;
        movlw   d'19'           ; the <XOFF> character            |B2
        call    Put232          ; tell HyperTerminal to wait      |B2
;
;******************************************************************
;
        movf    ErrFlg,W        ; any errors?                     |B2
        bnz     EOF             ; yes, branch, else               |B2
;
;  record <type> 00 (data record)?
;
        movf    RType,W         ; a '00' <data> record?           |B2
        bz      Data_Prep       ; yes, branch, else               |B2
;
;  not a data record <type> 00 so write any buffered data before
;  processing the <type> 01 or <type> 04 record
;
        call    Wr_Buffer       ; write buffer if necessary       |B2
;
;  record <type> 01 (end of file record)?
;
EOF     
        movf    RType,W         ; get record type var             |B2
        xorlw   b'00000001'     ; a '01' <eof> record?            |B2
        bnz     New_Record      ; no, branch, else                |B2
        movlw   sDone%100h      ; get "Ok" message index          |B2
        btfsc   ErrFlg,0        ; any errors?  no, skip, else     |B2
        movlw   sError%100h     ; use "Error" message index       |B2
        goto    Prt_Message     ;                                 |B2

;******************************************************************
;
;  Data Record - Code, IDLOC, Config, or EEProm data
;
Data_Prep
        clrc                    ;                                 |B2
        rrf     AddrH,F         ; convert byte addr to word addr  |B2
        rrf     AddrL,F         ;                                 |B2
        movf    AddrH,W         ; record buffer address hi        |B2
        xorwf   EEADRH,W        ; same as row buffer latch hi?    |B2
        bnz     Data_Overflow   ; no, branch, else                |B2
        movf    AddrL,W         ; record buffer address lo        |B2
        andlw   b'11110000'     ; 00/10/20/30..F0 'row'           |B2
        xorwf   EEADR,W         ; same as row buffer latch lo?    |B2
        skpz                    ; yes, skip, else                 |B2

Data_Overflow
        call    Wr_Buffer       ; write buffer if necessary       |B2
;
;  update target row address latch (16 word rows)
;
;  hi = 01..1F or 21
;  lo = 00/10/20/30...F0
;
Data_Update
        movf    AddrH,W         ; record buffer address hi        |B2
        movwf   EEADRH          ; set row buffer latch hi         |B2
        movf    AddrL,W         ; record buffer address lo        |B2
        andlw   b'11110000'     ; 00/10/20/30..F0 'row'           |B2
        movwf   EEADR           ; set row buffer latch lo         |B2
;
;  copy data from DBUFF record buffer to 32 byte DBLOCK row buffer
;
        movf    Dcount,W        ; get record byte count           |B2
Data_Copy
        movwf   Count           ; save count                      |B2
        addlw   DBUFF-1         ; record buffer address           |B2
        movwf   FSR             ; set source DBUFF indirect       |B2
        movf    INDF,W          ; get data byte                   |B2
        movwf   Temp            ; save it                         |B2
        rlf     AddrL,W         ; convert back to bytes           |B2
        andlw   b'00011110'     ; AddrL %= 32                     |B2
        addlw   DBLOCK-DBUFF    ; add buffer span                 |B2
        addwf   FSR,F           ; set target DBLOCK indirect      |B2
        movf    Temp,W          ; get data                        |B2
        movwf   INDF            ; copy into DBLOCK                |B2
        decfsz  Count,W         ; all bytes copied?               |B2
        goto    Data_Copy       ; no, branch, else                |B2
;
;  valid code/flash 0100..1FFF or data/eeprom 2100..21FF range?
;
        movf    AddrH,W         ; boot loader area 0000..00FF?    |B2
        skpnz                   ; no, skip, else                  |B2
        bsf     ErrFlg,0        ; indicate loader protect error   |B2
        andlw   b'11110000'     ; valid 0100..0FFF code range?    |B2
        skpnz                   ; no, skip, else                  |B2
        bsf     Dsize,4         ; buffer has data (16 words)      |B2
        movlw   h'21'           ; eeprom 2100..21FF               |B2
        xorwf   AddrH,W         ; valid 2100..21FF eeprom?        |B2
        bnz     New_Record      ; no, branch, else                |B2
        clrc                    ;                                 |B2
        rrf     Dcount,W        ; get record word count           |B2
        movwf   Dsize           ;                                 |B2
        movlw   b'00000100'     ; EEPGD=0, WREN=1                 |B2
        movwf   Shadow          ; EECON1 shadow register          |B2
        call    Wr_Data         ; write eeprom                    |B2
        goto    New_Record      ;                                 |B2

;******************************************************************
;
Wr_Buffer
        btfss   Dsize,4         ; data in buffer?                 |B2
        return                  ; no, exit, else                  |B2
        movlw   DBLOCK          ; data buffer address             |B2
        movwf   FSR             ; setup indirect access           |B2
        movlw   b'10000100'     ; EEPGD=1, WREN=1                 |B2
        movwf   Shadow          ;                                 |B2
        call    Wr_Data         ; write 16 word row (32 bytes)    |B2
        bsf     Dsize,4         ; reset 16 word counter           |B2
Verify
        decf    EEADR,F         ;                                 |B2
        bsf     STATUS,RP0      ; bank 3 (RP1 already set)        |B3
        bsf     EECON1,RD       ; read word                       |B3
        nop                     ; required 'nop'                  |B3
        nop                     ; required 'nop'                  |B3
        bcf     STATUS,RP0      ; bank 2 (RP1 already set)        |B2
        decf    FSR,F           ;                                 |B2
        movf    INDF,W          ;                                 |B2
        andlw   0x3F            ;                                 |B2
        xorwf   EEDATH,W        ; test high byte                  |B2
        skpz                    ; yes, branch, else               |B2
        bsf     ErrFlg,0        ; indicate verify error           |B2
        movlw   0x3F            ;                                 |B2
        movwf   INDF            ;                                 |B2
        decf    FSR,F           ;                                 |B2
        movf    EEDATA,W        ;                                 |B2
        xorwf   INDF,W          ; match?                          |B2
        skpz                    ; yes, skip, else                 |B2
        bsf     ErrFlg,0        ; indicate verify error           |B2
        movlw   0xFF            ;                                 |B2
        movwf   INDF            ;                                 |B2
        decfsz  Dsize,F         ; all words/bytes verified?       |B2
        goto    Verify          ; no, branch, else                |B2
        return                  ;                                 |B2

;******************************************************************
;
;  write 16 words from the row buffer or 'Dsize' eeprom bytes
;  from the record buffer
;
Wr_Data
        movf    INDF,W          ; get data byte (lo)              |B2
        movwf   EEDATA          ;                                 |B2
        incf    FSR,F           ;                                 |B2
        movf    INDF,W          ; get data byte (hi)              |B2
        movwf   EEDATH          ;                                 |B2
        incf    FSR,F           ;                                 |B2
        call    Write           ; write word                      |B2
        incf    EEADR,F         ; increment word address          |B2
        decfsz  Dsize,F         ; all words or bytes written?     |B2
        goto    Wr_Data         ; no, branch, write another       |B2
        return                  ;                                 |B2
;
;  16F887 write 'unlock' procedure
;
Write   
        movf    Shadow,W        ; EECON1 shadow register          |B2
        bsf     STATUS,RP0      ; bank 3 (RP1 already set)        |B3
        movwf   EECON1          ; setup EECON1                    |B3
        movlw   h'55'           ;                                 |B3
        movwf   EECON2          ;                                 |B3
        movlw   h'AA'           ;                                 |B3
        movwf   EECON2          ;                                 |B3
        bsf     EECON1,WR       ; start write operation           |B3
        nop                     ; required 'nop'                  |B3
        nop                     ; required 'nop'                  |B3
        btfsc   EECON1,WR       ; write eeprom complete?          |B3
        goto    $-1             ; no, branch and wait, else       |B3
        bcf     EECON1,WREN     ; clear write enable bit          |B3
        bcf     STATUS,RP0      ; bank 2 (RP1 already set)        |B2
        return                  ;                                 |B2

;******************************************************************
;
;  Get_Byte converts two ASCII HEX characters to a hex byte
;
Get_Byte
        clrf    Temp            ; use temporarily                 |B2
        call    Get_Nybble      ; get char, convert to hex        |B2
        movwf   Temp            ;                                 |B2
        swapf   Temp,f          ;                                 |B2
Get_Nybble
        call    Get232          ; get char                        |B2
        addlw   h'C0'           ;                                 |B2
        skpc                    ; was it 30..39 (result F0..F9)?  |B2
        addlw   h'07'           ; yes, add 07 (result F7..00)     |B2
        addlw   h'09'           ; fix range (result 00..0F)       |B2
        iorwf   Temp,W          ; <or> W with TEMP left nybble    |B2
        return                  ;
;
Get232  bcf     STATUS,RP1      ; bank 0 (RP0 already clr)        |B0
        btfss   PIR1,RCIF       ; character available?            |B0
        goto    $-1             ; no, branch, else                |B0
        movf    RCREG,W         ; get character                   |B0
;
Put232  bcf     STATUS,RP1      ; bank 0 (RP0 already clr)        |B0
        btfss   PIR1,TXIF       ; transmit buffer empty?          |B0
        goto    $-1             ; no, branch, else                |B0
        movwf   TXREG           ; send character                  |B0
        bsf     STATUS,RP1      ; bank 2 (RP0 already clr)        |B2
        return                  ;                                 |B2
;
Put_String
        movwf   Temp            ; save pointer                    |B2
        call    Get_Table       ; get a string character          |B2
        andlw   b'01111111'     ;                                 |B2
        skpnz                   ; last character?                 |B2
        return                  ; yes, exit, else                 |B2
        call    Put232          ; print character                 |B2
        incf    Temp,W          ;                                 |B2
        goto    Put_String      ;                                 |B2
;
Get_Table
        movwf   PCL             ;                                 |B2
;
sHello  dt      "\x1b[2J"       ; home cursor, clear screen
        dt      "K8LH 16F887 Bootloader v2.4\r\n\n\0"
sDone   dt      "\r\n\n Ok\0"
sError  dt      "\r\n\n Error\0"
;
        END

