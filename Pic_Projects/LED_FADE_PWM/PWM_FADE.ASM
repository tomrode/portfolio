;**********************************************************************
;   This file is a basic code template for assembly code generation   *
;   on the PIC16F887. This file contains the basic code               *
;   building blocks to build upon.                                    *
;                                                                     *
;   Refer to the MPASM User's Guide for additional information on     *
;   features of the assembler (Document DS33014).                     *
;                                                                     *
;   Refer to the respective PIC data sheet for additional             *
;   information on the instruction set.                               *
;                                                                     *
;**********************************************************************
;                                                                     *
;    Filename:	    xxx.asm                                           *
;    Date:                                                            *
;    File Version:                                                    *
;                                                                     *
;    Author:                                                          *
;    Company:                                                         *
;                                                                     *
;                                                                     *
;**********************************************************************
;                                                                     *
;    Files Required: P16F887.INC                                      *
;                                                                     *
;**********************************************************************
;                                                                     *
;    Notes:                                                           *
;                                                                     *
;**********************************************************************


	list		p=16f887	; list directive to define processor
	#include	<p16f887.inc>	; processor specific variable definitions


; '__CONFIG' directive is used to embed configuration data within .asm file.
; The labels following the directive are located in the respective .inc file.
; See respective data sheet for additional information on configuration word.

	__CONFIG    _CONFIG1, _LVP_OFF & _FCMEN_ON & _IESO_OFF & _BOR_OFF & _CPD_OFF & _CP_OFF & _MCLRE_ON & _PWRTE_ON & _WDT_OFF & _INTRC_OSC_NOCLKOUT
	__CONFIG    _CONFIG2, _WRT_OFF & _BOR21V



;***** VARIABLE DEFINITIONS*******************************************
result      EQU 0x7C        ; A/D value that will be used for changing CCPR1L for duty cyle
w_temp		EQU	0x7D		; variable used for context saving
status_temp	EQU	0x7E		; variable used for context saving
pclath_temp	EQU	0x7F		; variable used for context saving


;**********************************************************************
	                ORG     0x000             ; processor reset vector
                    
                    ORG     0x001
                	nop
  	                goto      main              ; go to beginning of program


	                ORG     0x004             ; interrupt vector location

	                movwf     w_temp            ; save off current W register contents
                 	movf	  STATUS,w          ; move status register into W register
	                movwf	  status_temp       ; save off contents of STATUS register
	                movf	  PCLATH,w	      ; move pclath register into w register
	                movwf	  pclath_temp	      ; save off contents of PCLATH register

; isr code can go here or be located as a call subroutine elsewhere

                 	movf	  pclath_temp,w	  ; retrieve copy of PCLATH register
	                movwf	  PCLATH		      ; restore pre-isr PCLATH register contents
	                movf      status_temp,w     ; retrieve copy of STATUS register
	                movwf	  STATUS            ; restore pre-isr STATUS register contents
	                swapf     w_temp,f
	                swapf     w_temp,w          ; restore pre-isr W register contents
	                retfie                    ; return from interrupt

main
                   
                   movlw      0x61                ; Select internal 4 MHz clock.
                   banksel    OSCCON
                   movwf      OSCCON
                        
wait_stable
                   btfss      OSCCON,HTS          ; Is the high-speed internal oscillator stable?
                   goto       wait_stable         ; If not, wait until it is.

                   movlw      0x00                ; Disable all interrupts.
                   banksel    INTCON
                   movwf      INTCON
                   banksel    PIE1
                   movwf      PIE1
            
                   movlw      0xFF                ;Set up comparison value for resetting TMR2 for period   
                   banksel    PR2                 
                   movwf      PR2
                   
                   banksel    TRISA
                   bsf        TRISA,TRISA0       ;set PA0 as an input
                
                   banksel    ANSEL
                   bsf        ANSEL,ANS0          ;set PA0 to analog mode.
                   
                   banksel    ADCON1
                   bcf        ADCON1,ADFM        ;left justify 
                    
                   movlw      0xC1               ;setting FRC internal A/D clock,  analog channel = RA0 ,ADON = selected 
                   banksel    ADCON0
                   movwf      ADCON0
        
                   movlw      0xFB                ;Enable the CCPx pin output driver by clearing the associated TRIS bit. 
                   banksel    TRISC
                   movwf      TRISC
                  
                   movlw      0x0C                ;P1M1 = 0, P1M0 = 0, DC1B1 = 0, DC1B0 = 0, CCP1M3..CCP1M0 = 1100
                   banksel    CCP1CON
                   movwf      CCP1CON
                    
                   movlw      0x06                ;turn on timer 2 with the prescaler  1:16, by default post scaler is 1:1 
                   banksel    T2CON
                   movwf      T2CON

                    
led_loop                  
                   banksel   ADCON0
                   bsf       ADCON0,GO             ;Start the conversion
conversion_loop                 
                   btfsc     ADCON0,GO
                   goto      conversion_loop
                   
                   banksel   ADRESH
                   movf      ADRESH,W
                                              
                   banksel   CCPR1L                ;Control the duty cycle
                   movwf     CCPR1L
                
                   goto      led_loop
                      

	END                       ; directive 'end of program'

