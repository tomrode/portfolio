;**********************************************************************
;   This file is a basic code template for assembly code generation   *
;   on the PIC16F887. This file contains the basic code               *
;   building blocks to build upon.                                    *
;                                                                     *
;   Refer to the MPASM User's Guide for additional information on     *
;   features of the assembler (Document DS33014).                     *
;                                                                     *
;   Refer to the respective PIC data sheet for additional             *
;   information on the instruction set.                               *
;                                                                     *
;**********************************************************************
;                                                                     *
;    Filename:	   timer.asm                                           *
;    Date:                                                            *
;    File Version:                                                    *
;                                                                     *
;    Author:                                                          *
;    Company:                                                         *
;                                                                     *
;                                                                     *
;**********************************************************************
;                                                                     *
;    Files Required: P16F887.INC                                      *
;                                                                     *
;**********************************************************************
;                                                                     *
;    Notes:                                                           *
;                                                                     *
;**********************************************************************


	list		p=16f887	; list directive to define processor
	#include	<p16f887.inc>	; processor specific variable definitions


; '__CONFIG' directive is used to embed configuration data within .asm file.
; The labels following the directive are located in the respective .inc file.
; See respective data sheet for additional information on configuration word.

	__CONFIG    _CONFIG1, _LVP_OFF & _FCMEN_ON & _IESO_OFF & _BOR_OFF & _CPD_OFF & _CP_OFF & _MCLRE_ON & _PWRTE_ON & _WDT_OFF & _INTRC_OSC_NOCLKOUT
	__CONFIG    _CONFIG2, _WRT_OFF & _BOR21V



;***** VARIABLE DEFINITIONS
resultlo     EQU    0x7C        ; Result of AD lo
resulthi     EQU    0x7D        ; Result of AD hi
status_temp	 EQU	0x7E		; variable used for context saving
pclath_temp	 EQU	0x7F		; variable used for context saving


;**********************************************************************
	ORG     0x000             ; processor reset vector

	nop
    goto    main              ; go to beginning of program
   

	ORG     0x004             ; interrupt vector location

    retfie

; isr code can go here or be located as a call subroutine elsewhere


main
                   
                   movlw     0x61                ; Select internal 4 MHz clock.
                   banksel   OSCCON
                   movwf     OSCCON        
                 
wait_stable
                   btfss     OSCCON,HTS          ; Is the high-speed internal oscillator stable?
                   goto      wait_stable         ; If not, wait until it is.

                   movlw     0x00                ; Disable all interrupts.
                   banksel   INTCON
                   movwf     INTCON
                   banksel   PIE1
                   movwf     PIE1
                   
                   movlw     0x00 
                   banksel   TRISD
                   movwf     TRISD        
;*****************************************************A/D Set up******************************************************************                   
                  
                   banksel    ADCON1
                   bsf        ADCON1,ADFM       ;right justify 
                    
                   movlw      0x01              ;make RA0 in input
                   banksel    TRISA
                   movwf      TRISA
                    
                   banksel    ANSEL
                   bsf        ANSEL,ANS0       ; Set RA0 as a analog input

                   movlw      0xC1             ;setting FRC internal A/D clock,  analog channel = RA0 ,ADON = selected 
                   banksel    ADCON0
                   movwf      ADCON0
loop                   
                   bsf        ADCON0,GO        ; Start conversion
conversion_done 
                   btfsc      ADCON0,GO
                   goto       conversion_done
                  
                   banksel    ADRESH
                   movf       ADRESH,W           ;store hibyte in W the put into RAM variable
                   banksel    resulthi 
                   movwf      resulthi      
                  
                   banksel    ADRESL
                   movf       ADRESL,W

                   banksel    resultlo
                   movwf      resultlo            ;teston PORTD the low byte          
                   
                   
                   banksel    PORTD
                   movwf      PORTD
                   goto       loop
	END                                         ; directive 'end of program'

