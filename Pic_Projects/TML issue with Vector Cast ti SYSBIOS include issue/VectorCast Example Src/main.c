#include "htc.h"
#include "typedefs.h"
#include "packagedefs.h"

/*PIC16F887*/
__CONFIG(DEBUGEN & LVPEN & FCMEN & IESODIS & BORXSLP & DUNPROTECT & UNPROTECT & MCLREN & PWRTEN & WDTDIS & INTCLK);//HS);
__CONFIG(BORV40);
/* STRUCTURES */
 
/* Function prototypes*/
int main(void);

/* non writeable ram variable location*/
persistent int persistent_watch_dog_count;

/* Volatile variable*/

/* Local Variables */
knl_Semaphore_Object_t *Pknl_Semaphore_Handle;
 
/*arrays*/


 /*********************************************************************/
/*        THE MAIN LOOP                                              */         
/*********************************************************************/
int main(void)
{
   knl_Semaphore_Object_t Semaphore;
   Pknl_Semaphore_Handle = &Semaphore;
   Pknl_Semaphore_Handle->event  = 1;

}

