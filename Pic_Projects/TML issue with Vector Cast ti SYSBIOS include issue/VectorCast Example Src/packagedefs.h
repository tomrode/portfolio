/*****************************************************************************/
/* FILE NAME:  packagedefs.h                                                    */
/*                                                                           */
/* Description:  Standard type definitions.                                  */
/*****************************************************************************/

#ifndef PACKAGEDEFS_H
#define PACKAGEDEFS_H

#include "typedefs.h"

/******************************************************************/
/* Global variables to entire project                             */
/*                                                                */
/*                                                                */       
/******************************************************************/

/* Object */ 
typedef struct knl_Semaphore_Object {
    uint8_t event;
    uint16_t eventId;
    uint32_t mode;
    volatile uint16_t count;
    char __dummy;
}knl_Semaphore_Object_t;

#endif
