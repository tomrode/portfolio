opt subtitle "HI-TECH Software Omniscient Code Generator (Lite mode) build 6738"

opt pagewidth 120

	opt lm

	processor	16F887
clrc	macro
	bcf	3,0
	endm
clrz	macro
	bcf	3,2
	endm
setc	macro
	bsf	3,0
	endm
setz	macro
	bsf	3,2
	endm
skipc	macro
	btfss	3,0
	endm
skipz	macro
	btfss	3,2
	endm
skipnc	macro
	btfsc	3,0
	endm
skipnz	macro
	btfsc	3,2
	endm
indf	equ	0
indf0	equ	0
pc	equ	2
pcl	equ	2
status	equ	3
fsr	equ	4
fsr0	equ	4
c	equ	1
z	equ	0
pclath	equ	10
# 6 "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\TML issue with Vector Cast ti SYSBIOS include issue\main.c"
	psect config,class=CONFIG,delta=2 ;#
# 6 "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\TML issue with Vector Cast ti SYSBIOS include issue\main.c"
	dw 0x1FFF & 0x3FFF & 0x3FFF & 0x3BFF & 0x3EFF & 0x3FFF & 0x3FFF & 0x3FFF & 0x3FEF & 0x3FF7 & 0x3FFD ;#
# 7 "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\TML issue with Vector Cast ti SYSBIOS include issue\main.c"
	psect config,class=CONFIG,delta=2 ;#
# 7 "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\TML issue with Vector Cast ti SYSBIOS include issue\main.c"
	dw 0x3FFF ;#
	FNCALL	_main,_gADCAverageRead
	FNCALL	_main,_Schedule
	FNCALL	_main,_PostSchedule
	FNCALL	_gADCAverageRead,___lwdiv
	FNROOT	_main
	global	_Week1Out
	global	_Week1OutPtr
	global	gADCAverageRead@uwAggregateCnt
	global	_toms_variable
	global	gADCAverageRead@ubDivideByCnt
	global	gADCAverageRead@ubResult
	global	_Week1In
	global	_persistent_watch_dog_count
psect	nvBANK0,class=BANK0,space=1
global __pnvBANK0
__pnvBANK0:
_persistent_watch_dog_count:
       ds      2

	global	_Pknl_Semaphore_Handle
_Pknl_Semaphore_Handle:
       ds      1

	file	"TML issue with Vector Cast ti SYSBIOS include issue.as"
	line	#
psect cinit,class=CODE,delta=2
global start_initialization
start_initialization:

psect	bssCOMMON,class=COMMON,space=1
global __pbssCOMMON
__pbssCOMMON:
_toms_variable:
       ds      1

gADCAverageRead@ubDivideByCnt:
       ds      1

gADCAverageRead@ubResult:
       ds      1

psect	bssBANK0,class=BANK0,space=1
global __pbssBANK0
__pbssBANK0:
_Week1Out:
       ds      14

_Week1OutPtr:
       ds      14

gADCAverageRead@uwAggregateCnt:
       ds      2

_Week1In:
       ds      14

psect clrtext,class=CODE,delta=2
global clear_ram
;	Called with FSR containing the base address, and
;	W with the last address+1
clear_ram:
	clrwdt			;clear the watchdog before getting into this loop
clrloop:
	clrf	indf		;clear RAM location pointed to by FSR
	incf	fsr,f		;increment pointer
	xorwf	fsr,w		;XOR with final address
	btfsc	status,2	;have we reached the end yet?
	retlw	0		;all done for this memory range, return
	xorwf	fsr,w		;XOR again to restore value
	goto	clrloop		;do the next byte

; Clear objects allocated to COMMON
psect cinit,class=CODE,delta=2
	clrf	((__pbssCOMMON)+0)&07Fh
	clrf	((__pbssCOMMON)+1)&07Fh
	clrf	((__pbssCOMMON)+2)&07Fh
; Clear objects allocated to BANK0
psect cinit,class=CODE,delta=2
	bcf	status, 7	;select IRP bank0
	movlw	low(__pbssBANK0)
	movwf	fsr
	movlw	low((__pbssBANK0)+02Ch)
	fcall	clear_ram
psect cinit,class=CODE,delta=2
global end_of_initialization

;End of C runtime variable initialization code

end_of_initialization:
clrf status
ljmp _main	;jump to C main() function
psect	cstackCOMMON,class=COMMON,space=1
global __pcstackCOMMON
__pcstackCOMMON:
	global	?_Schedule
?_Schedule:	; 0 bytes @ 0x0
	global	??_Schedule
??_Schedule:	; 0 bytes @ 0x0
	global	?_PostSchedule
?_PostSchedule:	; 0 bytes @ 0x0
	global	?_gADCAverageRead
?_gADCAverageRead:	; 1 bytes @ 0x0
	global	?_main
?_main:	; 2 bytes @ 0x0
	global	?___lwdiv
?___lwdiv:	; 2 bytes @ 0x0
	global	PostSchedule@Size
PostSchedule@Size:	; 1 bytes @ 0x0
	global	___lwdiv@divisor
___lwdiv@divisor:	; 2 bytes @ 0x0
	ds	1
	global	??_PostSchedule
??_PostSchedule:	; 0 bytes @ 0x1
	global	Schedule@StaffPtr
Schedule@StaffPtr:	; 1 bytes @ 0x1
	ds	1
	global	___lwdiv@dividend
___lwdiv@dividend:	; 2 bytes @ 0x2
	ds	2
	global	??___lwdiv
??___lwdiv:	; 0 bytes @ 0x4
	ds	1
	global	___lwdiv@quotient
___lwdiv@quotient:	; 2 bytes @ 0x5
	ds	2
	global	___lwdiv@counter
___lwdiv@counter:	; 1 bytes @ 0x7
	ds	1
	global	??_gADCAverageRead
??_gADCAverageRead:	; 0 bytes @ 0x8
	ds	1
	global	gADCAverageRead@ADCIn
gADCAverageRead@ADCIn:	; 1 bytes @ 0x9
	ds	1
	global	??_main
??_main:	; 0 bytes @ 0xA
	ds	1
psect	cstackBANK0,class=BANK0,space=1
global __pcstackBANK0
__pcstackBANK0:
	global	PostSchedule@Week1In
PostSchedule@Week1In:	; 14 bytes @ 0x0
	ds	14
	global	PostSchedule@StaffPtr
PostSchedule@StaffPtr:	; 1 bytes @ 0xE
	ds	1
	global	PostSchedule@i
PostSchedule@i:	; 1 bytes @ 0xF
	ds	1
	global	main@ADCtest
main@ADCtest:	; 1 bytes @ 0x10
	ds	1
	global	main@Semaphore
main@Semaphore:	; 10 bytes @ 0x11
	ds	10
;;Data sizes: Strings 0, constant 0, data 0, bss 47, persistent 3 stack 0
;;Auto spaces:   Size  Autos    Used
;; COMMON          14     11      14
;; BANK0           80     27      74
;; BANK1           80      0       0
;; BANK3           96      0       0
;; BANK2           96      0       0

;;
;; Pointer list with targets:

;; ?___lwdiv	unsigned int  size(1) Largest target is 0
;;
;; PostSchedule@StaffPtr	PTR struct . size(1) Largest target is 14
;;		 -> Week1In(BANK0[14]), 
;;
;; Schedule@StaffPtr	PTR struct . size(1) Largest target is 14
;;		 -> Week1In(BANK0[14]), 
;;
;; Pknl_Semaphore_Handle	PTR volatile struct knl_Semaphore_Object size(1) Largest target is 10
;;		 -> NULL(NULL[0]), main@Semaphore(BANK0[10]), 
;;


;;
;; Critical Paths under _main in COMMON
;;
;;   _main->_gADCAverageRead
;;   _gADCAverageRead->___lwdiv
;;
;; Critical Paths under _main in BANK0
;;
;;   _main->_PostSchedule
;;
;; Critical Paths under _main in BANK1
;;
;;   None.
;;
;; Critical Paths under _main in BANK3
;;
;;   None.
;;
;; Critical Paths under _main in BANK2
;;
;;   None.

;;
;;Main: autosize = 0, tempsize = 1, incstack = 0, save=0
;;

;;
;;Call Graph Tables:
;;
;; ---------------------------------------------------------------------------------
;; (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;; ---------------------------------------------------------------------------------
;; (0) _main                                                12    12      0     660
;;                                             10 COMMON     1     1      0
;;                                             16 BANK0     11    11      0
;;                    _gADCAverageRead
;;                           _Schedule
;;                       _PostSchedule
;; ---------------------------------------------------------------------------------
;; (1) _gADCAverageRead                                      2     2      0     184
;;                                              8 COMMON     2     2      0
;;                            ___lwdiv
;; ---------------------------------------------------------------------------------
;; (2) ___lwdiv                                              8     4      4     162
;;                                              0 COMMON     8     4      4
;; ---------------------------------------------------------------------------------
;; (1) _PostSchedule                                        20    19      1     115
;;                                              0 COMMON     4     3      1
;;                                              0 BANK0     16    16      0
;; ---------------------------------------------------------------------------------
;; (1) _Schedule                                             2     2      0     314
;;                                              0 COMMON     2     2      0
;; ---------------------------------------------------------------------------------
;; Estimated maximum stack depth 2
;; ---------------------------------------------------------------------------------

;; Call Graph Graphs:

;; _main (ROOT)
;;   _gADCAverageRead
;;     ___lwdiv
;;   _Schedule
;;   _PostSchedule
;;

;; Address spaces:

;;Name               Size   Autos  Total    Cost      Usage
;;BITCOMMON            E      0       0       0        0.0%
;;EEDATA             100      0       0       0        0.0%
;;NULL                 0      0       0       0        0.0%
;;CODE                 0      0       0       0        0.0%
;;COMMON               E      B       E       1      100.0%
;;BITSFR0              0      0       0       1        0.0%
;;SFR0                 0      0       0       1        0.0%
;;BITSFR1              0      0       0       2        0.0%
;;SFR1                 0      0       0       2        0.0%
;;STACK                0      0       2       2        0.0%
;;ABS                  0      0      58       3        0.0%
;;BITBANK0            50      0       0       4        0.0%
;;BITSFR3              0      0       0       4        0.0%
;;SFR3                 0      0       0       4        0.0%
;;BANK0               50     1B      4A       5       92.5%
;;BITSFR2              0      0       0       5        0.0%
;;SFR2                 0      0       0       5        0.0%
;;BITBANK1            50      0       0       6        0.0%
;;BANK1               50      0       0       7        0.0%
;;BITBANK3            60      0       0       8        0.0%
;;BANK3               60      0       0       9        0.0%
;;BITBANK2            60      0       0      10        0.0%
;;BANK2               60      0       0      11        0.0%
;;DATA                 0      0      5A      12        0.0%

	global	_main
psect	maintext,global,class=CODE,delta=2
global __pmaintext
__pmaintext:

;; *************** function _main *****************
;; Defined at:
;;		line 33 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\TML issue with Vector Cast ti SYSBIOS include issue\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  Semaphore      10   17[BANK0 ] volatile struct knl_Sema
;;  ADCtest         1   16[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;                  2  852[COMMON] int 
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 17F/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0      11       0       0       0
;;      Temps:          1       0       0       0       0
;;      Totals:         1      11       0       0       0
;;Total ram usage:       12 bytes
;; Hardware stack levels required when called:    2
;; This function calls:
;;		_gADCAverageRead
;;		_Schedule
;;		_PostSchedule
;; This function is called by:
;;		Startup code after reset
;; This function uses a non-reentrant model
;;
psect	maintext
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\TML issue with Vector Cast ti SYSBIOS include issue\main.c"
	line	33
	global	__size_of_main
	__size_of_main	equ	__end_of_main-_main
	
_main:	
	opt	stack 6
; Regs used in _main: [wreg-fsr0h+status,2+status,0+pclath+cstack]
	line	35
	
l2198:	
;main.c: 34: knl_Semaphore_Object_t Semaphore;
;main.c: 35: Pknl_Semaphore_Handle = &Semaphore;
	movlw	(main@Semaphore)&0ffh
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(_Pknl_Semaphore_Handle)
	line	36
	
l2200:	
;main.c: 36: Pknl_Semaphore_Handle->event = 1;
	movlw	(01h)
	movwf	(??_main+0)+0
	movf	(_Pknl_Semaphore_Handle),w
	movwf	fsr0
	movf	(??_main+0)+0,w
	bcf	status, 7	;select IRP bank0
	movwf	indf
	line	39
	
l2202:	
;main.c: 37: uint8_t ADCtest;
;main.c: 39: ADCtest = gADCAverageRead(ADCtest);
	movf	(main@ADCtest),w
	fcall	_gADCAverageRead
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(main@ADCtest)
	line	42
;main.c: 42: Schedule(&Week1In[0]);
	movlw	(_Week1In)&0ffh
	fcall	_Schedule
	line	45
;main.c: 45: PostSchedule(&Week1In[0], (sizeof(Week1In)/sizeof(Staff_t)) );
	movlw	(07h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(?_PostSchedule)
	movlw	(_Week1In)&0ffh
	fcall	_PostSchedule
	line	47
	
l853:	
	global	start
	ljmp	start
	opt stack 0
GLOBAL	__end_of_main
	__end_of_main:
;; =============== function _main ends ============

	signat	_main,90
	global	_gADCAverageRead
psect	text132,local,class=CODE,delta=2
global __ptext132
__ptext132:

;; *************** function _gADCAverageRead *****************
;; Defined at:
;;		line 65 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\TML issue with Vector Cast ti SYSBIOS include issue\main.c"
;; Parameters:    Size  Location     Type
;;  ADCIn           1    wreg     unsigned char 
;; Auto vars:     Size  Location     Type
;;  ADCIn           1    9[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         1       0       0       0       0
;;      Temps:          1       0       0       0       0
;;      Totals:         2       0       0       0       0
;;Total ram usage:        2 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		___lwdiv
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text132
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\TML issue with Vector Cast ti SYSBIOS include issue\main.c"
	line	65
	global	__size_of_gADCAverageRead
	__size_of_gADCAverageRead	equ	__end_of_gADCAverageRead-_gADCAverageRead
	
_gADCAverageRead:	
	opt	stack 6
; Regs used in _gADCAverageRead: [wreg+status,2+status,0+pclath+cstack]
;gADCAverageRead@ADCIn stored from wreg
	line	70
	movwf	(gADCAverageRead@ADCIn)
	
l2184:	
;main.c: 66: static uint16_t uwAggregateCnt;
;main.c: 67: static uint8_t ubDivideByCnt;
;main.c: 68: static uint8_t ubResult;
;main.c: 70: if ( ubDivideByCnt < 6 )
	movlw	(06h)
	subwf	(gADCAverageRead@ubDivideByCnt),w
	skipnc
	goto	u2241
	goto	u2240
u2241:
	goto	l2190
u2240:
	line	73
	
l2186:	
;main.c: 71: {
;main.c: 73: uwAggregateCnt = (uwAggregateCnt + ADCIn);
	movf	(gADCAverageRead@ADCIn),w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	addwf	(gADCAverageRead@uwAggregateCnt),w
	movwf	(gADCAverageRead@uwAggregateCnt)
	movf	(gADCAverageRead@uwAggregateCnt+1),w
	skipnc
	incf	(gADCAverageRead@uwAggregateCnt+1),w
	movwf	((gADCAverageRead@uwAggregateCnt))+1
	line	76
	
l2188:	
;main.c: 76: ubDivideByCnt++;
	movlw	(01h)
	movwf	(??_gADCAverageRead+0)+0
	movf	(??_gADCAverageRead+0)+0,w
	addwf	(gADCAverageRead@ubDivideByCnt),f
	line	77
;main.c: 77: }
	goto	l2194
	line	78
	
l862:	
	line	81
	
l2190:	
;main.c: 78: else
;main.c: 79: {
;main.c: 81: ubResult = (uwAggregateCnt / 6);
	movlw	low(06h)
	movwf	(?___lwdiv)
	movlw	high(06h)
	movwf	((?___lwdiv))+1
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(gADCAverageRead@uwAggregateCnt+1),w
	clrf	1+(?___lwdiv)+02h
	addwf	1+(?___lwdiv)+02h
	movf	(gADCAverageRead@uwAggregateCnt),w
	clrf	0+(?___lwdiv)+02h
	addwf	0+(?___lwdiv)+02h

	fcall	___lwdiv
	movf	(0+(?___lwdiv)),w
	movwf	(??_gADCAverageRead+0)+0
	movf	(??_gADCAverageRead+0)+0,w
	movwf	(gADCAverageRead@ubResult)
	line	84
	
l2192:	
;main.c: 84: ubDivideByCnt = 0;
	clrc
	movlw	0
	btfsc	status,0
	movlw	1
	movwf	(gADCAverageRead@ubDivideByCnt)
	goto	l2194
	line	85
	
l863:	
	line	86
	
l2194:	
;main.c: 85: }
;main.c: 86: return(ubResult);
	movf	(gADCAverageRead@ubResult),w
	goto	l864
	
l2196:	
	line	87
	
l864:	
	return
	opt stack 0
GLOBAL	__end_of_gADCAverageRead
	__end_of_gADCAverageRead:
;; =============== function _gADCAverageRead ends ============

	signat	_gADCAverageRead,4217
	global	___lwdiv
psect	text133,local,class=CODE,delta=2
global __ptext133
__ptext133:

;; *************** function ___lwdiv *****************
;; Defined at:
;;		line 5 in file "C:\Program Files (x86)\HI-TECH Software\PICC\9.80\sources\lwdiv.c"
;; Parameters:    Size  Location     Type
;;  divisor         2    0[COMMON] unsigned int 
;;  dividend        2    2[COMMON] unsigned int 
;; Auto vars:     Size  Location     Type
;;  quotient        2    5[COMMON] unsigned int 
;;  counter         1    7[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;                  2    0[COMMON] unsigned int 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         4       0       0       0       0
;;      Locals:         3       0       0       0       0
;;      Temps:          1       0       0       0       0
;;      Totals:         8       0       0       0       0
;;Total ram usage:        8 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_gADCAverageRead
;; This function uses a non-reentrant model
;;
psect	text133
	file	"C:\Program Files (x86)\HI-TECH Software\PICC\9.80\sources\lwdiv.c"
	line	5
	global	__size_of___lwdiv
	__size_of___lwdiv	equ	__end_of___lwdiv-___lwdiv
	
___lwdiv:	
	opt	stack 6
; Regs used in ___lwdiv: [wreg+status,2+status,0]
	line	9
	
l2160:	
	movlw	low(0)
	movwf	(___lwdiv@quotient)
	movlw	high(0)
	movwf	((___lwdiv@quotient))+1
	line	10
	movf	(___lwdiv@divisor+1),w
	iorwf	(___lwdiv@divisor),w
	skipnz
	goto	u2171
	goto	u2170
u2171:
	goto	l2180
u2170:
	line	11
	
l2162:	
	clrf	(___lwdiv@counter)
	bsf	status,0
	rlf	(___lwdiv@counter),f
	line	12
	goto	l2168
	
l890:	
	line	13
	
l2164:	
	movlw	01h
	
u2185:
	clrc
	rlf	(___lwdiv@divisor),f
	rlf	(___lwdiv@divisor+1),f
	addlw	-1
	skipz
	goto	u2185
	line	14
	
l2166:	
	movlw	(01h)
	movwf	(??___lwdiv+0)+0
	movf	(??___lwdiv+0)+0,w
	addwf	(___lwdiv@counter),f
	goto	l2168
	line	15
	
l889:	
	line	12
	
l2168:	
	btfss	(___lwdiv@divisor+1),(15)&7
	goto	u2191
	goto	u2190
u2191:
	goto	l2164
u2190:
	goto	l2170
	
l891:	
	goto	l2170
	line	16
	
l892:	
	line	17
	
l2170:	
	movlw	01h
	
u2205:
	clrc
	rlf	(___lwdiv@quotient),f
	rlf	(___lwdiv@quotient+1),f
	addlw	-1
	skipz
	goto	u2205
	line	18
	movf	(___lwdiv@divisor+1),w
	subwf	(___lwdiv@dividend+1),w
	skipz
	goto	u2215
	movf	(___lwdiv@divisor),w
	subwf	(___lwdiv@dividend),w
u2215:
	skipc
	goto	u2211
	goto	u2210
u2211:
	goto	l2176
u2210:
	line	19
	
l2172:	
	movf	(___lwdiv@divisor),w
	subwf	(___lwdiv@dividend),f
	movf	(___lwdiv@divisor+1),w
	skipc
	decf	(___lwdiv@dividend+1),f
	subwf	(___lwdiv@dividend+1),f
	line	20
	
l2174:	
	bsf	(___lwdiv@quotient)+(0/8),(0)&7
	goto	l2176
	line	21
	
l893:	
	line	22
	
l2176:	
	movlw	01h
	
u2225:
	clrc
	rrf	(___lwdiv@divisor+1),f
	rrf	(___lwdiv@divisor),f
	addlw	-1
	skipz
	goto	u2225
	line	23
	
l2178:	
	movlw	low(01h)
	subwf	(___lwdiv@counter),f
	btfss	status,2
	goto	u2231
	goto	u2230
u2231:
	goto	l2170
u2230:
	goto	l2180
	
l894:	
	goto	l2180
	line	24
	
l888:	
	line	25
	
l2180:	
	movf	(___lwdiv@quotient+1),w
	clrf	(?___lwdiv+1)
	addwf	(?___lwdiv+1)
	movf	(___lwdiv@quotient),w
	clrf	(?___lwdiv)
	addwf	(?___lwdiv)

	goto	l895
	
l2182:	
	line	26
	
l895:	
	return
	opt stack 0
GLOBAL	__end_of___lwdiv
	__end_of___lwdiv:
;; =============== function ___lwdiv ends ============

	signat	___lwdiv,8314
	global	_PostSchedule
psect	text134,local,class=CODE,delta=2
global __ptext134
__ptext134:

;; *************** function _PostSchedule *****************
;; Defined at:
;;		line 121 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\TML issue with Vector Cast ti SYSBIOS include issue\main.c"
;; Parameters:    Size  Location     Type
;;  StaffPtr        1    wreg     PTR struct .
;;		 -> Week1In(14), 
;;  Size            1    0[COMMON] unsigned char 
;; Auto vars:     Size  Location     Type
;;  StaffPtr        1   14[BANK0 ] PTR struct .
;;		 -> Week1In(14), 
;;  Week1In        14    0[BANK0 ] struct .[7]
;;  i               1   15[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         1       0       0       0       0
;;      Locals:         0      16       0       0       0
;;      Temps:          3       0       0       0       0
;;      Totals:         4      16       0       0       0
;;Total ram usage:       20 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text134
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\TML issue with Vector Cast ti SYSBIOS include issue\main.c"
	line	121
	global	__size_of_PostSchedule
	__size_of_PostSchedule	equ	__end_of_PostSchedule-_PostSchedule
	
_PostSchedule:	
	opt	stack 7
; Regs used in _PostSchedule: [wreg-fsr0h+status,2+status,0]
;PostSchedule@StaffPtr stored from wreg
	line	126
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(PostSchedule@StaffPtr)
	
l2150:	
;main.c: 122: Staff_t Week1In[7];
;main.c: 123: char i;
;main.c: 126: for(i=0; i<Size; i++)
	clrc
	movlw	0
	btfsc	status,0
	movlw	1
	movwf	(PostSchedule@i)
	goto	l2158
	line	127
	
l871:	
	line	128
	
l2152:	
;main.c: 127: {
;main.c: 128: Week1In[i] = *StaffPtr;
	movf	(PostSchedule@StaffPtr),w
	movwf	fsr0
	bcf	status, 7	;select IRP bank0
	movf	indf,w
	movwf	(??_PostSchedule+0)+0+0
	incf	fsr0,f
	movf	indf,w
	movwf	(??_PostSchedule+0)+0+1
	movf	(PostSchedule@i),w
	movwf	(??_PostSchedule+2)+0
	addwf	(??_PostSchedule+2)+0,w
	addlw	PostSchedule@Week1In&0ffh
	movwf	fsr0
	movf	0+(??_PostSchedule+0)+0,w
	movwf	indf
	incf	fsr0,f
	movf	1+(??_PostSchedule+0)+0,w
	movwf	indf
	line	129
	
l2154:	
;main.c: 129: StaffPtr++;
	movlw	(02h)
	movwf	(??_PostSchedule+0)+0
	movf	(??_PostSchedule+0)+0,w
	addwf	(PostSchedule@StaffPtr),f
	line	126
	
l2156:	
	movlw	(01h)
	movwf	(??_PostSchedule+0)+0
	movf	(??_PostSchedule+0)+0,w
	addwf	(PostSchedule@i),f
	goto	l2158
	
l870:	
	
l2158:	
	movf	(PostSchedule@Size),w
	subwf	(PostSchedule@i),w
	skipc
	goto	u2161
	goto	u2160
u2161:
	goto	l2152
u2160:
	goto	l873
	
l872:	
	line	131
	
l873:	
	return
	opt stack 0
GLOBAL	__end_of_PostSchedule
	__end_of_PostSchedule:
;; =============== function _PostSchedule ends ============

	signat	_PostSchedule,8312
	global	_Schedule
psect	text135,local,class=CODE,delta=2
global __ptext135
__ptext135:

;; *************** function _Schedule *****************
;; Defined at:
;;		line 91 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\TML issue with Vector Cast ti SYSBIOS include issue\main.c"
;; Parameters:    Size  Location     Type
;;  StaffPtr        1    wreg     PTR struct .
;;		 -> Week1In(14), 
;; Auto vars:     Size  Location     Type
;;  StaffPtr        1    1[COMMON] PTR struct .
;;		 -> Week1In(14), 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         1       0       0       0       0
;;      Temps:          1       0       0       0       0
;;      Totals:         2       0       0       0       0
;;Total ram usage:        2 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text135
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\TML issue with Vector Cast ti SYSBIOS include issue\main.c"
	line	91
	global	__size_of_Schedule
	__size_of_Schedule	equ	__end_of_Schedule-_Schedule
	
_Schedule:	
	opt	stack 7
; Regs used in _Schedule: [wreg-fsr0h+status,2+status,0]
;Schedule@StaffPtr stored from wreg
	movwf	(Schedule@StaffPtr)
	line	92
	
l2116:	
;main.c: 92: StaffPtr->People = TOM;
	clrc
	movf	(Schedule@StaffPtr),w
	movwf	fsr0
	movlw	0
	btfsc	status,0
	movlw	1
	bcf	status, 7	;select IRP bank0
	movwf	indf
	line	93
;main.c: 93: StaffPtr->Days = MON;
	clrc
	movf	(Schedule@StaffPtr),w
	addlw	01h
	movwf	fsr0
	movlw	0
	btfsc	status,0
	movlw	1
	movwf	indf
	line	94
	
l2118:	
;main.c: 94: StaffPtr++;
	movlw	(02h)
	movwf	(??_Schedule+0)+0
	movf	(??_Schedule+0)+0,w
	addwf	(Schedule@StaffPtr),f
	line	96
	
l2120:	
;main.c: 96: StaffPtr->People = JOHN;
	movf	(Schedule@StaffPtr),w
	movwf	fsr0
	clrf	indf
	bsf	status,0
	rlf	indf,f
	line	97
	
l2122:	
;main.c: 97: StaffPtr->Days = TUE;
	movf	(Schedule@StaffPtr),w
	addlw	01h
	movwf	fsr0
	clrf	indf
	bsf	status,0
	rlf	indf,f
	line	98
	
l2124:	
;main.c: 98: StaffPtr++;
	movlw	(02h)
	movwf	(??_Schedule+0)+0
	movf	(??_Schedule+0)+0,w
	addwf	(Schedule@StaffPtr),f
	line	100
	
l2126:	
;main.c: 100: StaffPtr->People = RICK;
	movlw	(02h)
	movwf	(??_Schedule+0)+0
	movf	(Schedule@StaffPtr),w
	movwf	fsr0
	movf	(??_Schedule+0)+0,w
	movwf	indf
	line	101
	
l2128:	
;main.c: 101: StaffPtr->Days = WED;
	movlw	(02h)
	movwf	(??_Schedule+0)+0
	movf	(Schedule@StaffPtr),w
	addlw	01h
	movwf	fsr0
	movf	(??_Schedule+0)+0,w
	movwf	indf
	line	102
	
l2130:	
;main.c: 102: StaffPtr++;
	movlw	(02h)
	movwf	(??_Schedule+0)+0
	movf	(??_Schedule+0)+0,w
	addwf	(Schedule@StaffPtr),f
	line	104
	
l2132:	
;main.c: 104: StaffPtr->People = RICK;
	movlw	(02h)
	movwf	(??_Schedule+0)+0
	movf	(Schedule@StaffPtr),w
	movwf	fsr0
	movf	(??_Schedule+0)+0,w
	movwf	indf
	line	105
	
l2134:	
;main.c: 105: StaffPtr->Days = THR;
	movlw	(03h)
	movwf	(??_Schedule+0)+0
	movf	(Schedule@StaffPtr),w
	addlw	01h
	movwf	fsr0
	movf	(??_Schedule+0)+0,w
	movwf	indf
	line	106
	
l2136:	
;main.c: 106: StaffPtr++;
	movlw	(02h)
	movwf	(??_Schedule+0)+0
	movf	(??_Schedule+0)+0,w
	addwf	(Schedule@StaffPtr),f
	line	108
;main.c: 108: StaffPtr->People = TOM;
	clrc
	movf	(Schedule@StaffPtr),w
	movwf	fsr0
	movlw	0
	btfsc	status,0
	movlw	1
	movwf	indf
	line	109
	
l2138:	
;main.c: 109: StaffPtr->Days = FRI;
	movlw	(04h)
	movwf	(??_Schedule+0)+0
	movf	(Schedule@StaffPtr),w
	addlw	01h
	movwf	fsr0
	movf	(??_Schedule+0)+0,w
	movwf	indf
	line	110
	
l2140:	
;main.c: 110: StaffPtr++;
	movlw	(02h)
	movwf	(??_Schedule+0)+0
	movf	(??_Schedule+0)+0,w
	addwf	(Schedule@StaffPtr),f
	line	112
	
l2142:	
;main.c: 112: StaffPtr->People = JOHN;
	movf	(Schedule@StaffPtr),w
	movwf	fsr0
	clrf	indf
	bsf	status,0
	rlf	indf,f
	line	113
	
l2144:	
;main.c: 113: StaffPtr->Days = SAT;
	movlw	(05h)
	movwf	(??_Schedule+0)+0
	movf	(Schedule@StaffPtr),w
	addlw	01h
	movwf	fsr0
	movf	(??_Schedule+0)+0,w
	movwf	indf
	line	114
;main.c: 114: StaffPtr++;
	movlw	(02h)
	movwf	(??_Schedule+0)+0
	movf	(??_Schedule+0)+0,w
	addwf	(Schedule@StaffPtr),f
	line	116
	
l2146:	
;main.c: 116: StaffPtr->People = RICK;
	movlw	(02h)
	movwf	(??_Schedule+0)+0
	movf	(Schedule@StaffPtr),w
	movwf	fsr0
	movf	(??_Schedule+0)+0,w
	movwf	indf
	line	117
	
l2148:	
;main.c: 117: StaffPtr->Days = SUN;
	movlw	(06h)
	movwf	(??_Schedule+0)+0
	movf	(Schedule@StaffPtr),w
	addlw	01h
	movwf	fsr0
	movf	(??_Schedule+0)+0,w
	movwf	indf
	line	118
	
l867:	
	return
	opt stack 0
GLOBAL	__end_of_Schedule
	__end_of_Schedule:
;; =============== function _Schedule ends ============

	signat	_Schedule,4216
psect	text136,local,class=CODE,delta=2
global __ptext136
__ptext136:
	global	btemp
	btemp set 07Eh

	DABS	1,126,2	;btemp
	global	wtemp0
	wtemp0 set btemp
	end
