#include "htc.h"
#include "typedefs.h"
#include "packagedefs.h"
#define SOIL_SEN_ADC_AVERAGE_DIVISOR 6
/*PIC16F887*/
__CONFIG(DEBUGEN & LVPEN & FCMEN & IESODIS & BORXSLP & DUNPROTECT & UNPROTECT & MCLREN & PWRTEN & WDTDIS & INTCLK);//HS);
__CONFIG(BORV40);
/* STRUCTURES */
 
/* Function prototypes*/
int main(void);
uint8_t gADCAverageRead(uint8_t ADCIn);
void Schedule(Staff_t* StaffPtr);
void PostSchedule(Staff_t *StaffPtr, uint8_t Size);

/* non writeable ram variable location*/
persistent int persistent_watch_dog_count;

/* Volatile variable*/

/* Local Variables */
knl_Semaphore_Object_t *Pknl_Semaphore_Handle;
Staff_t Week1In[7];
Staff_t *Week1OutPtr[7];
Staff_t  Week1Out[7];
/*arrays*/


 /*********************************************************************/
/*        THE MAIN LOOP                                              */         
/*********************************************************************/
int main(void)
{
   knl_Semaphore_Object_t Semaphore;
   Pknl_Semaphore_Handle = &Semaphore;
   Pknl_Semaphore_Handle->event  = 1;
   uint8_t ADCtest; 

   ADCtest = gADCAverageRead(ADCtest);
 
   /** - Place in Data */
   Schedule(&Week1In[0]);

   /** - Get data */
   PostSchedule(&Week1In[0], (sizeof(Week1In)/sizeof(Staff_t)) );
   
}
/**
   @fn      gADCAverageRead()

   @brief   Average the Soil sensor readings.

   @param   ADC Channel

   @return  ubTemp = 8 bit ADC averaged read

   @note    TBD.

   @author  Tom Rode

   @date    07/06/2016

*/
uint8_t gADCAverageRead(uint8_t ADCIn)
{
static uint16_t uwAggregateCnt;
static uint8_t  ubDivideByCnt;
static uint8_t  ubResult;

    if ( ubDivideByCnt < SOIL_SEN_ADC_AVERAGE_DIVISOR )
    {
        /** - Get a Summing of Current ADC in */
        uwAggregateCnt = (uwAggregateCnt + ADCIn);

        /** - Increment the counter */
        ubDivideByCnt++;
    }
    else
    {
        /** - Make the average calculation */
        ubResult = (uwAggregateCnt / SOIL_SEN_ADC_AVERAGE_DIVISOR);

         /** - Zero the counter */
        ubDivideByCnt = 0;
    }
return(ubResult);
}


void Schedule(Staff_t* StaffPtr)
{
   StaffPtr->People = TOM;
   StaffPtr->Days = MON;
   StaffPtr++;
 
   StaffPtr->People = JOHN;
   StaffPtr->Days = TUE;
   StaffPtr++;

   StaffPtr->People = RICK;
   StaffPtr->Days = WED;
   StaffPtr++;

   StaffPtr->People = RICK;
   StaffPtr->Days = THR;
   StaffPtr++;

   StaffPtr->People = TOM;
   StaffPtr->Days = FRI;
   StaffPtr++;

   StaffPtr->People = JOHN;
   StaffPtr->Days = SAT;
   StaffPtr++;

   StaffPtr->People = RICK;
   StaffPtr->Days = SUN;
}

void PostSchedule(Staff_t *StaffPtr, uint8_t Size)
{
   Staff_t  Week1In[7];
   char i;

   /** - Fill in this local variable by de-ref the pointer */
   for(i=0; i<Size; i++)
   { 
      Week1In[i] = *StaffPtr;
      StaffPtr++;
   } 
}