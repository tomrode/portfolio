                   list      p=16f887
                   #include  <p16f887.inc>

;******************************************************************************
; Device Configuration
;******************************************************************************

                   __CONFIG  _CONFIG1, _LVP_OFF & _FCMEN_ON & _IESO_OFF & _BOR_OFF & _CPD_OFF & _CP_OFF & _MCLRE_ON & _PWRTE_ON & _WDT_OFF & _INTRC_OSC_NOCLKOUT
                   __CONFIG  _CONFIG2, _WRT_OFF & _BOR21V



;******************************************************************************
; Constant Definitions
;******************************************************************************

TABLE_LENGTH            equ       0x38   ;56  

;******************************************************************************
; Variable Definitions
;******************************************************************************
           
temp                    equ       0x7C
table_input             equ       0x7D
table_output            equ       0x7E 
offset                  equ       0x7F

;******************************************************************************
; Reset Vector
;******************************************************************************

                   org       0x000

                   goto      main

;******************************************************************************
; Interrupt Vector
;******************************************************************************
                   org       0x400
                   retfie

;******************************************************************************
; Main Program
;******************************************************************************

main
                   movlw     0x61                          ; Select internal 4 MHz clock.
                   banksel   OSCCON
                   movwf     OSCCON
wait_stable
                   btfss     OSCCON,HTS                    ; Is the high-speed internal oscillator stable?
                  ; goto      wait_stable                   ; If not, wait until it is.

                   banksel   INTCON
                   bcf       INTCON,GIE
                               
 
                 

;******************************************************************************
; Main Loop
;******************************************************************************

main_loop           
                   
                   movlw     0x37   ;0x19=z                ;value in for offset
                   banksel   table_input
                   movwf     table_input             
                   call      table_read 
                   banksel   table_output        ;character returned from jump table
                   movwf     table_output               
                   goto      main_loop


;******************************************************************************
;read_table ()
;
; Description:  This function performs a table read based on
;               the index passed in the W register 
;
; Inputs:       W -> Index into the table  
;
; Outputs:      W -> Value from table
;******************************************************************************
table_read
                  banksel temp   
                  movwf   temp
                  movlw   TABLE_LENGTH   ; limit to the offset only
                  subwf   temp,F
                  banksel STATUS
                  btfsc   STATUS,C       ; This will set if a carry out occured 
invalid_entry
                  retlw   0x00           ; Set to a dafault

                  banksel table_input
                  movf    table_input,W   ;This will reload from W table input value
                  banksel offset
                  movwf   offset          ;store W into offset
                  
                  movlw   low table       ;get low 8 bits of address
                  addwf   offset,F        ;do an 8-bit add operation
                  movlw   high table      ;get high bits of address
                  banksel STATUS 
                  btfsc   STATUS,C        ;page crossed?
                  addlw   0x01            ;yes then increment high address
                  banksel PCLATH 
                  movwf   PCLATH          ;load high address in latch
                  banksel offset
                  movf    offset,W        ;load computed offset in w reg
                  banksel PCL
                  movwf   PCL             ;load computed offset in PCL
                  
                  
table
                 
                  retlw   'a'             ;offset 0
                  retlw   'b'             ; 
                  retlw   'c'             ;
                  retlw   'd'             ;
                  retlw   'e'             ;
                  retlw   'f'             ; 
                  retlw   'g'             ; 
                  retlw   'h'             ; 
                  retlw   'i'             ; 
                  retlw   'j'             ; 
                  retlw   'k'             ; 
                  retlw   'l'             ; 
                  retlw   'm'             ; 
                  retlw   'n'             ; 
                  retlw   'o'             ; 
                  retlw   'p'             ; 
                  retlw   'q'             ; 
                  retlw   'r'             ; 
                  retlw   's'             ; 
                  retlw   't'             ; 
                  retlw   'u'             ; 
                  retlw   'v'             ; 
                  retlw   'w'             ; 
                  retlw   'x'             ; 
                  retlw   'y'             ; 
                  retlw   'z'             ;offset 25 
                  retlw   'A'             ; 
                  retlw   'B'             ;  
                  retlw   'C'             ; 
                  retlw   'D'             ; 
                  retlw   'E'             ; 
                  retlw   'F'             ; 
                  retlw   'G'             ; 
                  retlw   'H'             ; 
                  retlw   'I'             ; 
                  retlw   'J'             ; 
                  retlw   'K'             ; 
                  retlw   'L'             ; 
                  retlw   'M'             ; 
                  retlw   'N'             ; 
                  retlw   'O'             ; 
                  retlw   'P'             ; 
                  retlw   'Q'             ; 
                  retlw   'R'             ; 
                  retlw   'S'             ; 
                  retlw   'T'             ; 
                  retlw   'U'             ; 
                  retlw   'V'             ; 
                  retlw   'W'             ; 
                  retlw   'X'             ; 
                  retlw   'Y'             ; 
                  retlw   'Z'             ; 
                  retlw   '1'             ; 
                  retlw   '2'             ; 
                  retlw   '3'             ;  
                  retlw   '4'             ;offset 55 













;******************************************************************************
; End of program
;******************************************************************************

                   end
