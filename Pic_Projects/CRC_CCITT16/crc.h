/**
 *  @file                   crc.h
 *  @brief                  Interface for the CRC and checksum function
 *  @copyright              2012-2017 Crown Equipment Corp., New Bremen, OH 45869
 *  @date                   09/01/2012
 *
 *  @remark Author:         Walter Conley
 *  @remark Project Tree:   C1515
 *
 */

#ifndef CRC_MODULE
#define CRC_MODULE 1

/*========================================================================*
 *  SECTION - Global definitions
 *========================================================================*
 */

/** @brief  The number of entries into the CRC Frame Check Sequence lookup table
 */
#define CRC_TABLE_SIZE 256

/** @brief  The default CRC seed to calculate CRC */
#define CRC_SEED_DEF  0xFFFFu

/*========================================================================*
 *  SECTION - extern global variables (minimize global variable use)      *
 *========================================================================*
 */

/*========================================================================*
 *  SECTION - extern global functions                                     *
 *========================================================================*
 */
extern uint16_t guwCrc16_Range(uint32_t ulLength, uint16_t uwSeed, const uint8_t *pubAddress);
extern uint16_t guwCks16_Range(uint32_t ulLength, uint16_t uwSeed, const uint8_t *pubAddress);
extern uint32_t gulCrc32_Range(uint32_t ulLength, uint32_t ulSeed, const uint8_t *pubAddress);

#endif /* #ifndef CRC_MODULE */
