/**
 *  @file                   utf_8.h
 *  @brief                  Defines the data structures and variables
 *  @copyright              2014 Crown Equipment Corp., New Bremen, OH  45869
 *  @date                   10/15/2014    
 *
 *  @remark Author:         Tom Rode
 *
 *  @remark Project:        C1515
 *              
 */

#ifndef UTF_8
#define UTF_8
#include "typedefs.h"


/*==============================================================*
 *  SECTION - Global definitions
 *========================================================================*
 */
typedef uint32_t Utf8CommSize_t;

typedef struct keyboard_switch
{
	Utf8CommSize_t KeySw1;       /**< Row 1   Col 1 */
	Utf8CommSize_t KeySw2;       /**< Row 2   Col 1 */
	Utf8CommSize_t KeySw3;       /**< Row 3   Col 1 */
	Utf8CommSize_t KeySw4;       /**< Row 4   Col 1 */
	Utf8CommSize_t KeySw5;       /**< Row 5   Col 1 */
	Utf8CommSize_t KeySw6;       /**< Row 1   Col 2 */
	Utf8CommSize_t KeySw7;       /**< Row 2   Col 2 */
	Utf8CommSize_t KeySw8;       /**< Row 3   Col 2 */
	Utf8CommSize_t KeySw9;       /**< Row 4   Col 2 */
    Utf8CommSize_t KeySw10;      /**< Row 5   Col 2 */
	Utf8CommSize_t KeySw11;      /**< Row 1   Col 3 */
	Utf8CommSize_t KeySw12;      /**< Row 2   Col 3 */
	Utf8CommSize_t KeySw13;      /**< Row 3   Col 3 */
	Utf8CommSize_t KeySw14;      /**< Row 4   Col 3 */
	Utf8CommSize_t KeySw15;      /**< Row 5   Col 3 */
	Utf8CommSize_t KeySw16;      /**< Row 1   Col 4 */
	Utf8CommSize_t KeySw17;      /**< Row 2   Col 4 */
	Utf8CommSize_t KeySw18;      /**< Row 3   Col 4 */
	Utf8CommSize_t KeySw19;      /**< Row 4   Col 4 */
	Utf8CommSize_t KeySw20;      /**< Row 5   Col 4 */
	Utf8CommSize_t KeySw21;      /**< Row 1   Col 5 */
	Utf8CommSize_t KeySw22;      /**< Row 2   Col 5 */
	Utf8CommSize_t KeySw23;      /**< Row 3   Col 5 */
	Utf8CommSize_t KeySw24;      /**< Row 4   Col 5 */
	Utf8CommSize_t KeySw25;      /**< Row 5   Col 5 */
	Utf8CommSize_t KeySw26;      /**< Row 1   Col 6 */
	Utf8CommSize_t KeySw27;      /**< Row 2   Col 6 */
	Utf8CommSize_t KeySw28;      /**< Row 3   Col 6 */
	Utf8CommSize_t KeySw29;      /**< Row 4   Col 6 */
	Utf8CommSize_t KeySw30;      /**< Row 5   Col 6 */
	Utf8CommSize_t KeySw31;      /**< Row 1   Col 7 */
	Utf8CommSize_t KeySw32;      /**< Row 2   Col 7 */
    Utf8CommSize_t KeySw33;      /**< Row 3   Col 7 */
	Utf8CommSize_t KeySw34;      /**< Row 4   Col 7 */
	Utf8CommSize_t KeySw35;      /**< Row 5   Col 7 */
	Utf8CommSize_t KeySw36;      /**< Row 1   Col 8 */
	Utf8CommSize_t KeySw37;      /**< Row 2   Col 8 */
	Utf8CommSize_t KeySw38;      /**< Row 3   Col 8 */
	Utf8CommSize_t KeySw39;      /**< Row 4   Col 8 */
	Utf8CommSize_t KeySw40;      /**< Row 5   Col 8 */
	Utf8CommSize_t KeySw41;      /**< Row 1   Col 9 */
	Utf8CommSize_t KeySw42;      /**< Row 2   Col 9 */
	Utf8CommSize_t KeySw43;      /**< Row 3   Col 9 */
	Utf8CommSize_t KeySw44;      /**< Row 4   Col 9 */
	Utf8CommSize_t KeySw45;      /**< Row 5   Col 9 */
	Utf8CommSize_t KeySw46;      /**< Row 1   Col 10 */
	Utf8CommSize_t KeySw47;      /**< Row 2   Col 10 */
	Utf8CommSize_t KeySw48;      /**< Row 3   Col 10 */
	Utf8CommSize_t KeySw49;      /**< Row 4   Col 10 */
	Utf8CommSize_t KeySw50;      /**< Row 5   Col 10 */
	Utf8CommSize_t KeySw51;      /**< Row 1   Col 11 */
	Utf8CommSize_t KeySw52;      /**< Row 2   Col 11 */
	Utf8CommSize_t KeySw53;      /**< Row 3   Col 11 */
	Utf8CommSize_t KeySw54;      /**< Row 4   Col 11 */
	Utf8CommSize_t KeySw55;      /**< Row 5   Col 11 */
} keyboard_switch_output_t;

typedef enum
{
    KEYSWNONE = 0,
    KEYSW1 = 1,
    KEYSW2 = 2,
    KEYSW3 = 3,
    KEYSW4 = 4,
    KEYSW5 = 5,
    KEYSW6 = 6,
    KEYSW7 = 7,
    KEYSW8 = 8,
    KEYSW9 = 9,
    KEYSW10 = 10,
    KEYSW11 = 11,
    KEYSW12 = 12,
    KEYSW13 = 13,
    KEYSW14 = 14,
    KEYSW15 = 15,
    KEYSW16 = 16,
    KEYSW17 = 17,
    KEYSW18 = 18,
    KEYSW19 = 19,
    KEYSW20 = 20,
    KEYSW21 = 21,
    KEYSW22 = 22,
    KEYSW23 = 23,
    KEYSW24 = 24,
    KEYSW25 = 25,
    KEYSW26 = 26,
    KEYSW27 = 27,
    KEYSW28 = 28,
    KEYSW29 = 29,
    KEYSW30 = 30,
    KEYSW31 = 31,
    KEYSW32 = 32,
    KEYSW33 = 33,
    KEYSW34 = 34,
    KEYSW35 = 35,
    KEYSW36 = 36,
    KEYSW37 = 37,
    KEYSW38 = 38,
    KEYSW39 = 39,
    KEYSW40 = 40,
    KEYSW41 = 41,
    KEYSW42 = 42,
    KEYSW43 = 43,
    KEYSW44 = 44,
    KEYSW45 = 45,
    KEYSW46 = 46,
    KEYSW47 = 47,
    KEYSW48 = 48,
    KEYSW49 = 49,
    KEYSW50 = 50,
    KEYSW51 = 51,
    KEYSW52 = 52,
    KEYSW53 = 53,
    KEYSW54 = 54,
    KEYSW55 = 55,
    KEYSWMAX = 56

} keyboard_switch_num_t;

 
/*========================================================================*
 *  SECTION - extern global variabbles                                    *
 *========================================================================*
 */
extern keyboard_switch_output_t GetDefCalCopy;

/*========================================================================*
 *  SECTION - extern global functions                                     *
 *========================================================================*
 */

extern void GetRomSwDef(void);

#endif /* #ifndef UTF_8_H */
