/************************************************************************************************
 *  File:       utf_8.c
 *
 *  Purpose:    Defines the data structures and variables exchanged via CANopen
 *
 *  Project:    C1515
 *
 *  Copyright:  2014 Crown Equipment Corp., New Bremen, OH  45869
 *
 *  Author:     Tom Rode
 *
 *  Revision History:
 *              
 ************************************************************************************************/
 #include "utf_8.h"
 #include "typedefs.h"

const keyboard_switch_output_t BasicUtf8Defaults[KEYSWMAX - 1] = { /*    SW1     SW2,    SW3,    SW4,    SW5,    */
		                                                                 0x31,   0x45,   0x41,   0x1B,   0x11,
 
                                                                   /*    SW6,    SW7,    SW8,    SW9,    SW10     */
                                                                         0x32,   0x57,   0x53,   0x5A,   0x12,

                                                                   /*    SW11,   SW12,   SW13,   SW14,   SW15     */
                                                                         0x33,   0x45,   0x44,   0x58,   0x13,

                                                                   /*    SW16,   SW17,   SW18,   SW19,   SW20     */
                                                                         0x34,   0x52,   0x46,   0x43,   0x20,

                                                                   /*    SW21,   SW22,   SW23,   SW24,   SW25     */
                                                                         0x35,   0x54,   0x47,   0x56,   0x20,

                                                                   /*    SW26,   SW27,   SW28,   SW29,   SW30     */
                                                                         0x36,   0x59,   0x48,   0x42,   0x20,

                                                                   /*    SW31,   SW32,   SW33,   SW34,   SW35     */
                                                                         0x37,   0x55,   0x4A,   0x4E,   0x14,

                                                                   /*    SW36,   SW37,   SW38,   SW39,   SW40     */
                                                                         0x37,   0x55,   0x4A,   0x4E,   0x15,

                                                                   /*    SW41,   SW42,   SW43,   SW44,   SW45     */
                                                                         0x39,   0x4F,   0x4C,   0x23,   0x16,

                                                                   /*    SW46,   SW47,   SW48,   SW49,   SW50     */
                                                                         0x30,   0x50,   0x38,   0x40,   0x17,

                                                                   /*    SW51,   SW42,   SW53,   SW54,   SW55     */
                                                                         0x00,   0x4F,   0x3F,   0x3C,   0x18
                                                                   };
 
/*========================================================================*
 *  SECTION - extern global variabbles                                    *
 *========================================================================*
 */

keyboard_switch_output_t GetDefCalCopy;

/**
 * @fn       GetSwDef(void)
 *
 * @brief    This function place defaults variables to all 55 switches
 *
 * @param    None
 *
 * @return   None
 *
 * @note
 *
 * @author  Tom Rode
 *
 */
extern void GetRomSwDef(void)
{
    /* Local Declarations */
    keyboard_switch_output_t *GetSwDefPtr;

    /** ###Functional overview: */

    /** - Point to the Hard coded table */
    GetSwDefPtr = (keyboard_switch_output_t *)BasicUtf8Defaults;
    //GetSwDefPtr = &BasicUtf8Defaults;
    /** - Deref pointer */
    GetDefCalCopy = *GetSwDefPtr;

}