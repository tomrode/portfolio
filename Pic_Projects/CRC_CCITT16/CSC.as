opt subtitle "HI-TECH Software Omniscient Code Generator (Lite mode) build 7503"

opt pagewidth 120

	opt lm

	processor	16F887
clrc	macro
	bcf	3,0
	endm
clrz	macro
	bcf	3,2
	endm
setc	macro
	bsf	3,0
	endm
setz	macro
	bsf	3,2
	endm
skipc	macro
	btfss	3,0
	endm
skipz	macro
	btfss	3,2
	endm
skipnc	macro
	btfsc	3,0
	endm
skipnz	macro
	btfsc	3,2
	endm
indf	equ	0
indf0	equ	0
pc	equ	2
pcl	equ	2
status	equ	3
fsr	equ	4
fsr0	equ	4
c	equ	1
z	equ	0
pclath	equ	10
	FNCALL	_main,_N07337_crc_ccitt
	FNCALL	_N07337_crc_ccitt,_N07337_crc_ccitt_byte
	FNROOT	_main
	global	_N07337_crc_ccitt_table
psect	stringtext,class=STRCODE,delta=2,reloc=256
global __pstringtext
__pstringtext:
;	global	stringtab,__stringbase
stringtab:
;	String table - string pointers are 2 bytes each
	btfsc	(btemp+1),7
	ljmp	stringcode
	bcf	status,7
	btfsc	(btemp+1),0
	bsf	status,7
	movf	indf,w
	incf fsr
skipnz
incf btemp+1
	return
stringcode:
	movf btemp+1,w
andlw 7Fh
movwf	pclath
	movf	fsr,w
incf fsr
skipnz
incf btemp+1
	movwf pc
__stringbase:
	file	"H:\Tom\Various Documents\pic_projects\CRC_CCITT16\main.c"
	line	33
_N07337_crc_ccitt_table:
	retlw	0
	retlw	0

	retlw	089h
	retlw	011h

	retlw	012h
	retlw	023h

	retlw	09Bh
	retlw	032h

	retlw	024h
	retlw	046h

	retlw	0ADh
	retlw	057h

	retlw	036h
	retlw	065h

	retlw	0BFh
	retlw	074h

	retlw	048h
	retlw	08Ch

	retlw	0C1h
	retlw	09Dh

	retlw	05Ah
	retlw	0AFh

	retlw	0D3h
	retlw	0BEh

	retlw	06Ch
	retlw	0CAh

	retlw	0E5h
	retlw	0DBh

	retlw	07Eh
	retlw	0E9h

	retlw	0F7h
	retlw	0F8h

	retlw	081h
	retlw	010h

	retlw	08h
	retlw	01h

	retlw	093h
	retlw	033h

	retlw	01Ah
	retlw	022h

	retlw	0A5h
	retlw	056h

	retlw	02Ch
	retlw	047h

	retlw	0B7h
	retlw	075h

	retlw	03Eh
	retlw	064h

	retlw	0C9h
	retlw	09Ch

	retlw	040h
	retlw	08Dh

	retlw	0DBh
	retlw	0BFh

	retlw	052h
	retlw	0AEh

	retlw	0EDh
	retlw	0DAh

	retlw	064h
	retlw	0CBh

	retlw	0FFh
	retlw	0F9h

	retlw	076h
	retlw	0E8h

	retlw	02h
	retlw	021h

	retlw	08Bh
	retlw	030h

	retlw	010h
	retlw	02h

	retlw	099h
	retlw	013h

	retlw	026h
	retlw	067h

	retlw	0AFh
	retlw	076h

	retlw	034h
	retlw	044h

	retlw	0BDh
	retlw	055h

	retlw	04Ah
	retlw	0ADh

	retlw	0C3h
	retlw	0BCh

	retlw	058h
	retlw	08Eh

	retlw	0D1h
	retlw	09Fh

	retlw	06Eh
	retlw	0EBh

	retlw	0E7h
	retlw	0FAh

	retlw	07Ch
	retlw	0C8h

	retlw	0F5h
	retlw	0D9h

	retlw	083h
	retlw	031h

	retlw	0Ah
	retlw	020h

	retlw	091h
	retlw	012h

	retlw	018h
	retlw	03h

	retlw	0A7h
	retlw	077h

	retlw	02Eh
	retlw	066h

	retlw	0B5h
	retlw	054h

	retlw	03Ch
	retlw	045h

	retlw	0CBh
	retlw	0BDh

	retlw	042h
	retlw	0ACh

	retlw	0D9h
	retlw	09Eh

	retlw	050h
	retlw	08Fh

	retlw	0EFh
	retlw	0FBh

	retlw	066h
	retlw	0EAh

	retlw	0FDh
	retlw	0D8h

	retlw	074h
	retlw	0C9h

	retlw	04h
	retlw	042h

	retlw	08Dh
	retlw	053h

	retlw	016h
	retlw	061h

	retlw	09Fh
	retlw	070h

	retlw	020h
	retlw	04h

	retlw	0A9h
	retlw	015h

	retlw	032h
	retlw	027h

	retlw	0BBh
	retlw	036h

	retlw	04Ch
	retlw	0CEh

	retlw	0C5h
	retlw	0DFh

	retlw	05Eh
	retlw	0EDh

	retlw	0D7h
	retlw	0FCh

	retlw	068h
	retlw	088h

	retlw	0E1h
	retlw	099h

	retlw	07Ah
	retlw	0ABh

	retlw	0F3h
	retlw	0BAh

	retlw	085h
	retlw	052h

	retlw	0Ch
	retlw	043h

	retlw	097h
	retlw	071h

	retlw	01Eh
	retlw	060h

	retlw	0A1h
	retlw	014h

	retlw	028h
	retlw	05h

	retlw	0B3h
	retlw	037h

	retlw	03Ah
	retlw	026h

	retlw	0CDh
	retlw	0DEh

	retlw	044h
	retlw	0CFh

	retlw	0DFh
	retlw	0FDh

	retlw	056h
	retlw	0ECh

	retlw	0E9h
	retlw	098h

	retlw	060h
	retlw	089h

	retlw	0FBh
	retlw	0BBh

	retlw	072h
	retlw	0AAh

	retlw	06h
	retlw	063h

	retlw	08Fh
	retlw	072h

	retlw	014h
	retlw	040h

	retlw	09Dh
	retlw	051h

	retlw	022h
	retlw	025h

	retlw	0ABh
	retlw	034h

	retlw	030h
	retlw	06h

	retlw	0B9h
	retlw	017h

	retlw	04Eh
	retlw	0EFh

	retlw	0C7h
	retlw	0FEh

	retlw	05Ch
	retlw	0CCh

	retlw	0D5h
	retlw	0DDh

	retlw	06Ah
	retlw	0A9h

	retlw	0E3h
	retlw	0B8h

	retlw	078h
	retlw	08Ah

	retlw	0F1h
	retlw	09Bh

	retlw	087h
	retlw	073h

	retlw	0Eh
	retlw	062h

	retlw	095h
	retlw	050h

	retlw	01Ch
	retlw	041h

	retlw	0A3h
	retlw	035h

	retlw	02Ah
	retlw	024h

	retlw	0B1h
	retlw	016h

	retlw	038h
	retlw	07h

	retlw	0CFh
	retlw	0FFh

	retlw	046h
	retlw	0EEh

	retlw	0DDh
	retlw	0DCh

	retlw	054h
	retlw	0CDh

	retlw	0EBh
	retlw	0B9h

	retlw	062h
	retlw	0A8h

	retlw	0F9h
	retlw	09Ah

	retlw	070h
	retlw	08Bh

	retlw	08h
	retlw	084h

	retlw	081h
	retlw	095h

	retlw	01Ah
	retlw	0A7h

	retlw	093h
	retlw	0B6h

	retlw	02Ch
	retlw	0C2h

	retlw	0A5h
	retlw	0D3h

	retlw	03Eh
	retlw	0E1h

	retlw	0B7h
	retlw	0F0h

	retlw	040h
	retlw	08h

	retlw	0C9h
	retlw	019h

	retlw	052h
	retlw	02Bh

	retlw	0DBh
	retlw	03Ah

	retlw	064h
	retlw	04Eh

	retlw	0EDh
	retlw	05Fh

	retlw	076h
	retlw	06Dh

	retlw	0FFh
	retlw	07Ch

	retlw	089h
	retlw	094h

	retlw	0
	retlw	085h

	retlw	09Bh
	retlw	0B7h

	retlw	012h
	retlw	0A6h

	retlw	0ADh
	retlw	0D2h

	retlw	024h
	retlw	0C3h

	retlw	0BFh
	retlw	0F1h

	retlw	036h
	retlw	0E0h

	retlw	0C1h
	retlw	018h

	retlw	048h
	retlw	09h

	retlw	0D3h
	retlw	03Bh

	retlw	05Ah
	retlw	02Ah

	retlw	0E5h
	retlw	05Eh

	retlw	06Ch
	retlw	04Fh

	retlw	0F7h
	retlw	07Dh

	retlw	07Eh
	retlw	06Ch

	retlw	0Ah
	retlw	0A5h

	retlw	083h
	retlw	0B4h

	retlw	018h
	retlw	086h

	retlw	091h
	retlw	097h

	retlw	02Eh
	retlw	0E3h

	retlw	0A7h
	retlw	0F2h

	retlw	03Ch
	retlw	0C0h

	retlw	0B5h
	retlw	0D1h

	retlw	042h
	retlw	029h

	retlw	0CBh
	retlw	038h

	retlw	050h
	retlw	0Ah

	retlw	0D9h
	retlw	01Bh

	retlw	066h
	retlw	06Fh

	retlw	0EFh
	retlw	07Eh

	retlw	074h
	retlw	04Ch

	retlw	0FDh
	retlw	05Dh

	retlw	08Bh
	retlw	0B5h

	retlw	02h
	retlw	0A4h

	retlw	099h
	retlw	096h

	retlw	010h
	retlw	087h

	retlw	0AFh
	retlw	0F3h

	retlw	026h
	retlw	0E2h

	retlw	0BDh
	retlw	0D0h

	retlw	034h
	retlw	0C1h

	retlw	0C3h
	retlw	039h

	retlw	04Ah
	retlw	028h

	retlw	0D1h
	retlw	01Ah

	retlw	058h
	retlw	0Bh

	retlw	0E7h
	retlw	07Fh

	retlw	06Eh
	retlw	06Eh

	retlw	0F5h
	retlw	05Ch

	retlw	07Ch
	retlw	04Dh

	retlw	0Ch
	retlw	0C6h

	retlw	085h
	retlw	0D7h

	retlw	01Eh
	retlw	0E5h

	retlw	097h
	retlw	0F4h

	retlw	028h
	retlw	080h

	retlw	0A1h
	retlw	091h

	retlw	03Ah
	retlw	0A3h

	retlw	0B3h
	retlw	0B2h

	retlw	044h
	retlw	04Ah

	retlw	0CDh
	retlw	05Bh

	retlw	056h
	retlw	069h

	retlw	0DFh
	retlw	078h

	retlw	060h
	retlw	0Ch

	retlw	0E9h
	retlw	01Dh

	retlw	072h
	retlw	02Fh

	retlw	0FBh
	retlw	03Eh

	retlw	08Dh
	retlw	0D6h

	retlw	04h
	retlw	0C7h

	retlw	09Fh
	retlw	0F5h

	retlw	016h
	retlw	0E4h

	retlw	0A9h
	retlw	090h

	retlw	020h
	retlw	081h

	retlw	0BBh
	retlw	0B3h

	retlw	032h
	retlw	0A2h

	retlw	0C5h
	retlw	05Ah

	retlw	04Ch
	retlw	04Bh

	retlw	0D7h
	retlw	079h

	retlw	05Eh
	retlw	068h

	retlw	0E1h
	retlw	01Ch

	retlw	068h
	retlw	0Dh

	retlw	0F3h
	retlw	03Fh

	retlw	07Ah
	retlw	02Eh

	retlw	0Eh
	retlw	0E7h

	retlw	087h
	retlw	0F6h

	retlw	01Ch
	retlw	0C4h

	retlw	095h
	retlw	0D5h

	retlw	02Ah
	retlw	0A1h

	retlw	0A3h
	retlw	0B0h

	retlw	038h
	retlw	082h

	retlw	0B1h
	retlw	093h

	retlw	046h
	retlw	06Bh

	retlw	0CFh
	retlw	07Ah

	retlw	054h
	retlw	048h

	retlw	0DDh
	retlw	059h

	retlw	062h
	retlw	02Dh

	retlw	0EBh
	retlw	03Ch

	retlw	070h
	retlw	0Eh

	retlw	0F9h
	retlw	01Fh

	retlw	08Fh
	retlw	0F7h

	retlw	06h
	retlw	0E6h

	retlw	09Dh
	retlw	0D4h

	retlw	014h
	retlw	0C5h

	retlw	0ABh
	retlw	0B1h

	retlw	022h
	retlw	0A0h

	retlw	0B9h
	retlw	092h

	retlw	030h
	retlw	083h

	retlw	0C7h
	retlw	07Bh

	retlw	04Eh
	retlw	06Ah

	retlw	0D5h
	retlw	058h

	retlw	05Ch
	retlw	049h

	retlw	0E3h
	retlw	03Dh

	retlw	06Ah
	retlw	02Ch

	retlw	0F1h
	retlw	01Eh

	retlw	078h
	retlw	0Fh

	global	_VPC_CRC_318990
psect	stringtext
	file	"H:\Tom\Various Documents\pic_projects\CRC_CCITT16\main.c"
	line	89
_VPC_CRC_318990:
	retlw	030h
	retlw	030h
	retlw	030h
	retlw	030h
	retlw	030h
	retlw	030h
	retlw	033h
	retlw	031h
	retlw	038h
	retlw	039h
	retlw	039h
	retlw	030h
	retlw	030h
	retlw	030h
	retlw	030h
	retlw	030h
	retlw	030h
	retlw	030h
	retlw	030h
	retlw	030h
	retlw	030h
	retlw	020h
	retlw	011h
	retlw	010h
	retlw	027h
	retlw	06Fh
	retlw	0Ch
	retlw	040h
	retlw	05h
	retlw	02h
	retlw	0C0h
	retlw	03Dh
	retlw	0
	retlw	02h
	retlw	040h
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	06Fh
	retlw	0Ch
	retlw	040h
	retlw	05h
	retlw	02h
	retlw	0C0h
	retlw	019h
	retlw	0
	retlw	02h
	retlw	040h
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	034h
	retlw	058h
	retlw	0EFh
	retlw	031h
	retlw	0CDh
	retlw	0C6h
	retlw	07h
	retlw	0C9h
	retlw	025h
	retlw	033h
	retlw	0E1h
	retlw	09Fh
	retlw	0F3h
	retlw	011h
	retlw	03Eh
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	052h
	retlw	0
	retlw	0D0h
	retlw	05h
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	01h
	retlw	0
	retlw	09h
	retlw	024h
	retlw	0
	retlw	0
	retlw	012h
	retlw	0
	retlw	02h
	retlw	04h
	retlw	011h
	retlw	056h
	retlw	0
	retlw	0
	retlw	018h
	retlw	03h
	retlw	0A5h
	retlw	0
	retlw	0
	retlw	050h
	retlw	0
	retlw	02h
	retlw	0Fh
	retlw	0B1h
	retlw	020h
	retlw	053h
	retlw	0B5h
	retlw	019h
	retlw	082h
	retlw	03h
	retlw	0
	retlw	031h
	retlw	04h
	retlw	01Bh
	retlw	0
	retlw	03h
	retlw	0A7h
	retlw	09h
	retlw	055h
	retlw	0Ch
	retlw	0
	retlw	0
	retlw	0
	global	_N07337_crc_ccitt_table
	global	_VPC_CRC_318990
	global	_toms_variable
	file	"CSC.as"
	line	#
psect cinit,class=CODE,delta=2
global start_initialization
start_initialization:

psect	bssCOMMON,class=COMMON,space=1
global __pbssCOMMON
__pbssCOMMON:
_toms_variable:
       ds      1

; Clear objects allocated to COMMON
psect cinit,class=CODE,delta=2
	clrf	((__pbssCOMMON)+0)&07Fh
psect cinit,class=CODE,delta=2
global end_of_initialization

;End of C runtime variable initialization code

end_of_initialization:
clrf status
ljmp _main	;jump to C main() function
psect	cstackCOMMON,class=COMMON,space=1
global __pcstackCOMMON
__pcstackCOMMON:
	global	?_main
?_main:	; 0 bytes @ 0x0
	global	?_N07337_crc_ccitt_byte
?_N07337_crc_ccitt_byte:	; 2 bytes @ 0x0
	global	N07337_crc_ccitt_byte@crc
N07337_crc_ccitt_byte@crc:	; 2 bytes @ 0x0
	ds	2
	global	N07337_crc_ccitt_byte@c
N07337_crc_ccitt_byte@c:	; 1 bytes @ 0x2
	ds	1
	global	??_N07337_crc_ccitt_byte
??_N07337_crc_ccitt_byte:	; 0 bytes @ 0x3
	ds	8
	global	??_main
??_main:	; 0 bytes @ 0xB
	ds	1
psect	cstackBANK0,class=BANK0,space=1
global __pcstackBANK0
__pcstackBANK0:
	global	?_N07337_crc_ccitt
?_N07337_crc_ccitt:	; 2 bytes @ 0x0
	global	N07337_crc_ccitt@crc
N07337_crc_ccitt@crc:	; 2 bytes @ 0x0
	ds	2
	global	N07337_crc_ccitt@buffer
N07337_crc_ccitt@buffer:	; 2 bytes @ 0x2
	ds	2
	global	N07337_crc_ccitt@len
N07337_crc_ccitt@len:	; 4 bytes @ 0x4
	ds	4
	global	??_N07337_crc_ccitt
??_N07337_crc_ccitt:	; 0 bytes @ 0x8
	ds	4
	global	main@Len
main@Len:	; 1 bytes @ 0xC
	ds	1
	global	main@crc
main@crc:	; 2 bytes @ 0xD
	ds	2
;;Data sizes: Strings 0, constant 640, data 0, bss 1, persistent 0 stack 0
;;Auto spaces:   Size  Autos    Used
;; COMMON          14     12      13
;; BANK0           80     15      15
;; BANK1           80      0       0
;; BANK3           96      0       0
;; BANK2           96      0       0

;;
;; Pointer list with targets:

;; ?_N07337_crc_ccitt_byte	unsigned short  size(1) Largest target is 0
;;
;; ?_N07337_crc_ccitt	unsigned short  size(1) Largest target is 0
;;
;; N07337_crc_ccitt@buffer	PTR unsigned char  size(2) Largest target is 128
;;		 -> VPC_CRC_318990(CODE[128]), 
;;


;;
;; Critical Paths under _main in COMMON
;;
;;   _N07337_crc_ccitt->_N07337_crc_ccitt_byte
;;
;; Critical Paths under _main in BANK0
;;
;;   _main->_N07337_crc_ccitt
;;
;; Critical Paths under _main in BANK1
;;
;;   None.
;;
;; Critical Paths under _main in BANK3
;;
;;   None.
;;
;; Critical Paths under _main in BANK2
;;
;;   None.

;;
;;Main: autosize = 0, tempsize = 1, incstack = 0, save=0
;;

;;
;;Call Graph Tables:
;;
;; ---------------------------------------------------------------------------------
;; (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;; ---------------------------------------------------------------------------------
;; (0) _main                                                 4     4      0     204
;;                                             11 COMMON     1     1      0
;;                                             12 BANK0      3     3      0
;;                   _N07337_crc_ccitt
;; ---------------------------------------------------------------------------------
;; (1) _N07337_crc_ccitt                                    12     4      8     179
;;                                              0 BANK0     12     4      8
;;              _N07337_crc_ccitt_byte
;; ---------------------------------------------------------------------------------
;; (3) _N07337_crc_ccitt_byte                               11     8      3      89
;;                                              0 COMMON    11     8      3
;; ---------------------------------------------------------------------------------
;; Estimated maximum stack depth 3
;; ---------------------------------------------------------------------------------

;; Call Graph Graphs:

;; _main (ROOT)
;;   _N07337_crc_ccitt
;;     _N07337_crc_ccitt_byte
;;

;; Address spaces:

;;Name               Size   Autos  Total    Cost      Usage
;;BITCOMMON            E      0       0       0        0.0%
;;EEDATA             100      0       0       0        0.0%
;;NULL                 0      0       0       0        0.0%
;;CODE                 0      0       0       0        0.0%
;;COMMON               E      C       D       1       92.9%
;;BITSFR0              0      0       0       1        0.0%
;;SFR0                 0      0       0       1        0.0%
;;BITSFR1              0      0       0       2        0.0%
;;SFR1                 0      0       0       2        0.0%
;;STACK                0      0       3       2        0.0%
;;ABS                  0      0      1C       3        0.0%
;;BITBANK0            50      0       0       4        0.0%
;;BITSFR3              0      0       0       4        0.0%
;;SFR3                 0      0       0       4        0.0%
;;BANK0               50      F       F       5       18.8%
;;BITSFR2              0      0       0       5        0.0%
;;SFR2                 0      0       0       5        0.0%
;;BITBANK1            50      0       0       6        0.0%
;;BANK1               50      0       0       7        0.0%
;;BITBANK3            60      0       0       8        0.0%
;;BANK3               60      0       0       9        0.0%
;;BITBANK2            60      0       0      10        0.0%
;;BANK2               60      0       0      11        0.0%
;;DATA                 0      0      1F      12        0.0%

	global	_main
psect	maintext,global,class=CODE,delta=2
global __pmaintext
__pmaintext:

;; *************** function _main *****************
;; Defined at:
;;		line 133 in file "H:\Tom\Various Documents\pic_projects\CRC_CCITT16\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  crc             2   13[BANK0 ] unsigned short 
;;  Len             1   12[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, btemp+1, pclath, cstack
;; Tracked objects:
;;		On entry : 17F/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       3       0       0       0
;;      Temps:          1       0       0       0       0
;;      Totals:         1       3       0       0       0
;;Total ram usage:        4 bytes
;; Hardware stack levels required when called:    3
;; This function calls:
;;		_N07337_crc_ccitt
;; This function is called by:
;;		Startup code after reset
;; This function uses a non-reentrant model
;;
psect	maintext
	file	"H:\Tom\Various Documents\pic_projects\CRC_CCITT16\main.c"
	line	133
	global	__size_of_main
	__size_of_main	equ	__end_of_main-_main
	
_main:	
	opt	stack 5
; Regs used in _main: [wreg-fsr0h+status,2+status,0+btemp+1+pclath+cstack]
	line	138
	
l1195:	
;main.c: 134: uint8_t Len;
;main.c: 135: uint16_t crc;
;main.c: 138: Len = sizeof(VPC_CRC_318990);
	movlw	(080h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(main@Len)
	line	139
	
l1197:	
;main.c: 139: crc = 0x0000;
	clrf	(main@crc)
	clrf	(main@crc+1)
	line	140
	
l1199:	
;main.c: 140: crc = N07337_crc_ccitt(crc, &VPC_CRC_318990[25], (256-25));
	movf	(main@crc+1),w
	clrf	(?_N07337_crc_ccitt+1)
	addwf	(?_N07337_crc_ccitt+1)
	movf	(main@crc),w
	clrf	(?_N07337_crc_ccitt)
	addwf	(?_N07337_crc_ccitt)

	movlw	low(_VPC_CRC_318990|8000h+019h)
	movwf	(0+?_N07337_crc_ccitt+02h)
	movlw	high(_VPC_CRC_318990|8000h+019h)
	movwf	((0+?_N07337_crc_ccitt+02h))+1
	movlw	0
	movwf	3+(?_N07337_crc_ccitt)+04h
	movlw	0
	movwf	2+(?_N07337_crc_ccitt)+04h
	movlw	0
	movwf	1+(?_N07337_crc_ccitt)+04h
	movlw	0E7h
	movwf	0+(?_N07337_crc_ccitt)+04h

	fcall	_N07337_crc_ccitt
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(1+(?_N07337_crc_ccitt)),w
	clrf	(main@crc+1)
	addwf	(main@crc+1)
	movf	(0+(?_N07337_crc_ccitt)),w
	clrf	(main@crc)
	addwf	(main@crc)

	line	141
	
l1201:	
# 141 "H:\Tom\Various Documents\pic_projects\CRC_CCITT16\main.c"
nop ;#
psect	maintext
	line	142
	
l11:	
	global	start
	ljmp	start
	opt stack 0
GLOBAL	__end_of_main
	__end_of_main:
;; =============== function _main ends ============

	signat	_main,88
	global	_N07337_crc_ccitt
psect	text97,local,class=CODE,delta=2
global __ptext97
__ptext97:

;; *************** function _N07337_crc_ccitt *****************
;; Defined at:
;;		line 179 in file "H:\Tom\Various Documents\pic_projects\CRC_CCITT16\main.c"
;; Parameters:    Size  Location     Type
;;  crc             2    0[BANK0 ] unsigned short 
;;  buffer          2    2[BANK0 ] PTR unsigned char 
;;		 -> VPC_CRC_318990(128), 
;;  len             4    4[BANK0 ] unsigned long 
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;                  2    0[BANK0 ] unsigned short 
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, btemp+1, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       8       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       4       0       0       0
;;      Totals:         0      12       0       0       0
;;Total ram usage:       12 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		_N07337_crc_ccitt_byte
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text97
	file	"H:\Tom\Various Documents\pic_projects\CRC_CCITT16\main.c"
	line	179
	global	__size_of_N07337_crc_ccitt
	__size_of_N07337_crc_ccitt	equ	__end_of_N07337_crc_ccitt-_N07337_crc_ccitt
	
_N07337_crc_ccitt:	
	opt	stack 5
; Regs used in _N07337_crc_ccitt: [wreg-fsr0h+status,2+status,0+btemp+1+pclath+cstack]
	line	180
	
l1187:	
;main.c: 180: while (len--)
	goto	l1193
	
l18:	
	line	182
	
l1189:	
;main.c: 181: {
;main.c: 182: crc = N07337_crc_ccitt_byte(crc, *buffer++);
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(N07337_crc_ccitt@crc+1),w
	clrf	(?_N07337_crc_ccitt_byte+1)
	addwf	(?_N07337_crc_ccitt_byte+1)
	movf	(N07337_crc_ccitt@crc),w
	clrf	(?_N07337_crc_ccitt_byte)
	addwf	(?_N07337_crc_ccitt_byte)

	movf	(N07337_crc_ccitt@buffer+1),w
	movwf	btemp+1
	movf	(N07337_crc_ccitt@buffer),w
	movwf	fsr0
	fcall	stringtab
	movwf	(??_N07337_crc_ccitt+0)+0
	movf	(??_N07337_crc_ccitt+0)+0,w
	movwf	0+(?_N07337_crc_ccitt_byte)+02h
	fcall	_N07337_crc_ccitt_byte
	movf	(1+(?_N07337_crc_ccitt_byte)),w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(N07337_crc_ccitt@crc+1)
	addwf	(N07337_crc_ccitt@crc+1)
	movf	(0+(?_N07337_crc_ccitt_byte)),w
	clrf	(N07337_crc_ccitt@crc)
	addwf	(N07337_crc_ccitt@crc)

	
l1191:	
	movlw	low(01h)
	addwf	(N07337_crc_ccitt@buffer),f
	skipnc
	incf	(N07337_crc_ccitt@buffer+1),f
	movlw	high(01h)
	addwf	(N07337_crc_ccitt@buffer+1),f
	goto	l1193
	line	183
	
l17:	
	line	180
	
l1193:	
	movlw	01h
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	((??_N07337_crc_ccitt+0)+0)
	movlw	0
	movwf	((??_N07337_crc_ccitt+0)+0+1)
	movlw	0
	movwf	((??_N07337_crc_ccitt+0)+0+2)
	movlw	0
	movwf	((??_N07337_crc_ccitt+0)+0+3)
	movf	0+(??_N07337_crc_ccitt+0)+0,w
	subwf	(N07337_crc_ccitt@len),f
	movf	1+(??_N07337_crc_ccitt+0)+0,w
	skipc
	incfsz	1+(??_N07337_crc_ccitt+0)+0,w
	goto	u2205
	goto	u2206
u2205:
	subwf	(N07337_crc_ccitt@len+1),f
u2206:
	movf	2+(??_N07337_crc_ccitt+0)+0,w
	skipc
	incfsz	2+(??_N07337_crc_ccitt+0)+0,w
	goto	u2207
	goto	u2208
u2207:
	subwf	(N07337_crc_ccitt@len+2),f
u2208:
	movf	3+(??_N07337_crc_ccitt+0)+0,w
	skipc
	incfsz	3+(??_N07337_crc_ccitt+0)+0,w
	goto	u2209
	goto	u2200
u2209:
	subwf	(N07337_crc_ccitt@len+3),f
u2200:

	movlw	0FFh
	xorwf	((N07337_crc_ccitt@len+3)),w
	skipz
	goto	u2215
	movlw	0FFh
	xorwf	((N07337_crc_ccitt@len+2)),w
	skipz
	goto	u2215
	movlw	0FFh
	xorwf	((N07337_crc_ccitt@len+1)),w
	skipz
	goto	u2215
	movlw	0FFh
	xorwf	((N07337_crc_ccitt@len)),w
u2215:
	skipz
	goto	u2211
	goto	u2210
u2211:
	goto	l1189
u2210:
	
l19:	
	line	185
;main.c: 183: }
;main.c: 185: return crc;
	line	186
	
l20:	
	return
	opt stack 0
GLOBAL	__end_of_N07337_crc_ccitt
	__end_of_N07337_crc_ccitt:
;; =============== function _N07337_crc_ccitt ends ============

	signat	_N07337_crc_ccitt,12410
	global	_N07337_crc_ccitt_byte
psect	text98,local,class=CODE,delta=2
global __ptext98
__ptext98:

;; *************** function _N07337_crc_ccitt_byte *****************
;; Defined at:
;;		line 158 in file "H:\Tom\Various Documents\pic_projects\CRC_CCITT16\main.c"
;; Parameters:    Size  Location     Type
;;  crc             2    0[COMMON] unsigned short 
;;  c               1    2[COMMON] const unsigned char 
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;                  2    0[COMMON] unsigned short 
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, btemp+1, pclath
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         3       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          8       0       0       0       0
;;      Totals:        11       0       0       0       0
;;Total ram usage:       11 bytes
;; Hardware stack levels used:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_N07337_crc_ccitt
;; This function uses a non-reentrant model
;;
psect	text98
	file	"H:\Tom\Various Documents\pic_projects\CRC_CCITT16\main.c"
	line	158
	global	__size_of_N07337_crc_ccitt_byte
	__size_of_N07337_crc_ccitt_byte	equ	__end_of_N07337_crc_ccitt_byte-_N07337_crc_ccitt_byte
	
_N07337_crc_ccitt_byte:	
	opt	stack 5
; Regs used in _N07337_crc_ccitt_byte: [wreg-fsr0h+status,2+status,0+btemp+1+pclath]
	line	159
	
l1181:	
;main.c: 159: crc = (crc >> 8) ^ N07337_crc_ccitt_table[(crc ^ c) & 0xff];
	movf	(N07337_crc_ccitt_byte@crc),w
	xorwf	(N07337_crc_ccitt_byte@c),w
	movwf	(??_N07337_crc_ccitt_byte+0)+0
	movf	(N07337_crc_ccitt_byte@crc+1),w
	movwf	1+(??_N07337_crc_ccitt_byte+0)+0
	movlw	low(0FFh)
	andwf	0+(??_N07337_crc_ccitt_byte+0)+0,w
	movwf	(??_N07337_crc_ccitt_byte+2)+0
	movlw	high(0FFh)
	andwf	1+(??_N07337_crc_ccitt_byte+0)+0,w
	movwf	1+(??_N07337_crc_ccitt_byte+2)+0
	movlw	01h
	movwf	btemp+1
u2185:
	clrc
	rlf	(??_N07337_crc_ccitt_byte+2)+0,f
	rlf	(??_N07337_crc_ccitt_byte+2)+1,f
	decfsz	btemp+1,f
	goto	u2185
	movlw	high(_N07337_crc_ccitt_table|8000h)
	addwf	1+(??_N07337_crc_ccitt_byte+2)+0,w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	btemp+1
	movlw	low(_N07337_crc_ccitt_table|8000h)
	addwf	0+(??_N07337_crc_ccitt_byte+2)+0,w
	movwf	fsr0
	skipnc
	incf	btemp+1,f
	fcall	stringtab
	movwf	(??_N07337_crc_ccitt_byte+4)+0
	fcall	stringtab
	movwf	(??_N07337_crc_ccitt_byte+4)+0+1
	movf	(N07337_crc_ccitt_byte@crc+1),w
	movwf	(??_N07337_crc_ccitt_byte+6)+0+1
	movf	(N07337_crc_ccitt_byte@crc),w
	movwf	(??_N07337_crc_ccitt_byte+6)+0
	movlw	08h
u2195:
	clrc
	rrf	(??_N07337_crc_ccitt_byte+6)+1,f
	rrf	(??_N07337_crc_ccitt_byte+6)+0,f
	addlw	-1
	skipz
	goto	u2195
	movf	0+(??_N07337_crc_ccitt_byte+4)+0,w
	xorwf	0+(??_N07337_crc_ccitt_byte+6)+0,w
	movwf	(N07337_crc_ccitt_byte@crc)
	movf	1+(??_N07337_crc_ccitt_byte+4)+0,w
	xorwf	1+(??_N07337_crc_ccitt_byte+6)+0,w
	movwf	1+(N07337_crc_ccitt_byte@crc)
	line	161
	
l1183:	
;main.c: 161: return crc;
	goto	l14
	
l1185:	
	line	162
	
l14:	
	return
	opt stack 0
GLOBAL	__end_of_N07337_crc_ccitt_byte
	__end_of_N07337_crc_ccitt_byte:
;; =============== function _N07337_crc_ccitt_byte ends ============

	signat	_N07337_crc_ccitt_byte,8314
psect	text99,local,class=CODE,delta=2
global __ptext99
__ptext99:
	global	btemp
	btemp set 07Eh

	DABS	1,126,2	;btemp
	global	wtemp0
	wtemp0 set btemp
	end
