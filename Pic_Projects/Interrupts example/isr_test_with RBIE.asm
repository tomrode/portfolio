                   list      p=16f887
                   #include  <p16f887.inc>

;******************************************************************************
; Device Configuration
;******************************************************************************

                   __CONFIG  _CONFIG1, _LVP_OFF & _FCMEN_ON & _IESO_OFF & _BOR_OFF & _CPD_OFF & _CP_OFF & _MCLRE_ON & _PWRTE_ON & _WDT_OFF & _INTRC_OSC_NOCLKOUT
                   __CONFIG  _CONFIG2, _WRT_OFF & _BOR21V

;******************************************************************************
; Variable Definitions
;******************************************************************************
iocb_flag_count    equ       0x7A
iocb_flag          equ       0x7B
sw_timer           equ       0x7C
pclath_temp        equ       0x7D
status_temp        equ       0x7E
w_temp             equ       0x7F

;******************************************************************************
; Reset Vector
;******************************************************************************

                   org       0x000

                   goto      main

;******************************************************************************
; Interrupt Vector
;******************************************************************************

                   org       0x004

                   movwf     w_temp                        ; Save the W register.
                   swapf     STATUS,W                      ; Copy the STATUS register into W (nibbles reversed).
                   movwf     status_temp                   ; Save this value.
                   movf      PCLATH,W                      ; Copy the PCLATH register into W.
                   movwf     pclath_temp                   ; Save this value.

                   banksel   INTCON
                   btfsc     INTCON,T0IF                   ; Has a Timer 0 interrupt occurred?
                   call      timer_0_isr                   ; If so, call the Timer 0 interrupt handler.
                  
                   banksel   INTCON
                   btfsc     INTCON,RBIF                   ; Has PORTB changed state?
                   call      portb_change_isr              ; If so, call PORTB interrupt handler 
                 
                   movf      pclath_temp,W                 ; Load the saved PCLATH value into W.
                   movwf     PCLATH                        ; Update the PCLATH register.
                   swapf     status_temp,W                 ; Load the saved STATUS register value into W (nibbles reversed).
                   movwf     STATUS                        ; Update the STATUS register.
                   swapf     w_temp,F                      ; Swap the nibbles of the saved W register value.
                   swapf     w_temp,W                      ; Swap the nibbles again into the W register, restoring it.
                   retfie                                  ; Return from interrupt.

;******************************************************************************
; Main Program
;******************************************************************************

main
                   movlw     0x61                          ; Select internal 4 MHz clock.
                   banksel   OSCCON
                   movwf     OSCCON
wait_stable
                   btfss     OSCCON,HTS                    ; Is the high-speed internal oscillator stable?
                   goto      wait_stable                   ; If not, wait until it is.

                   banksel   OPTION_REG
                   movf      OPTION_REG,W                  ; Load the current value of the OPTION register.
                   andlw      0x57
                  ; andlw     0xD7                          ; Clear T0CS and PSA RBPU.
                   iorlw     0x07                          ; Set PS2, PS1, and PS0.
                   movwf     OPTION_REG                    ; Update the OPTION register.
 
                   banksel   INTCON
                   bsf       INTCON,GIE
                   bsf       INTCON,T0IE                   ; Allow only the Timer 0 interrupt.
                   bsf       INTCON,RBIE                   ; Allow PORTB interrupt change                   
 
                   movlw     0x00                          ; Set PD0 to PD7 to outputs.
                   banksel   TRISD
                   movwf     TRISD

                   movlw     0x0F
                   banksel   PORTD
                   movwf     PORTD                         ; Initialize PORTD with a display patter
                 
                   movlw     0x04                          ;Enable weak pull ups RB2
                   banksel   WPUB 
                   movwf     WPUB                    

                   movlw     0x00                          ;Make sure the analog is off
                   banksel   ANSELH
                   movwf     ANSELH 

                   movlw     0x04                          ;Make an input
                   banksel   TRISB 
                   movwf     TRISB                   

                   movlw     0x04                          ; Initialize the IOCB2 interrupt -on- change portb pin
                   banksel   IOCB
                   movwf     IOCB

                   movlw     0x00                          ; Clear out intiailly
                   banksel   iocb_flag
                   movwf     iocb_flag

;******************************************************************************
; Main Loop
;******************************************************************************

main_loop           
                 
                   
                   movlw     0xFE;0x04                          ; Load the software timer value.
                   call      sw_timer_init                 ; Initialize the software timer.

wait_loop          

                   banksel   iocb_flag
                   movf      iocb_flag,W
                  ; movlw     0xBA
                   banksel   iocb_flag_count
                   movwf     iocb_flag_count
           
                   banksel   sw_timer
                   movf      sw_timer,F                    ; Test the software timer value.
                   banksel   STATUS
                   btfss     STATUS,Z                      ; Is it zero?
                   goto      wait_loop                     ; If not, wait until it is.

led_update
                   banksel   PORTD
                   movf      PORTD,W                       ; Copy PORTD into the W register.
                   xorlw     0xFF                          ; Toggle all bits.
                   movwf     PORTD                         ; Update PORTD.

                   goto      main_loop

;******************************************************************************
; sw_timer_init()
;
; Description:  This function intializes the software timer.  After this call,
;               the software timer will count down until it reaches zero.
;
; Inputs:       W -> Contains the initial count value.
;
; Outputs:      None
;******************************************************************************

sw_timer_init
                   banksel   INTCON
                   bcf       INTCON,GIE                    ; Globally disable interrupts for the following critical code section.
                   banksel   sw_timer
                   movwf     sw_timer                      ; Update the software timer value.
                   banksel   TMR0
                   clrf      TMR0                          ; Clear the hardware timer (Timer 0).
                   banksel   INTCON
                   bsf       INTCON,GIE                    ; Globally enable interrupts.
                   return

;******************************************************************************
; timer_0_isr()
;
; Description:  This is the interrupt service routine (ISR) for the Timer 0
;               interrupt.
;
; Inputs:       None
;
; Outputs:      None
;******************************************************************************

timer_0_isr
                   banksel   INTCON
                   bcf       INTCON,T0IF                   ; Clear the Timer 0 interrupt flag.

                   banksel   sw_timer
                   movf      sw_timer,F                    ; Test the software timer.
                   banksel   STATUS
                   btfsc     STATUS,Z                      ; Is it zero?
                   goto      timer_0_isr_done              ; If so, simply exit.

                   banksel   sw_timer
                   decf      sw_timer,F                    ; Otherwise, decrement it.

timer_0_isr_done
                   return                                  ; Return to the main interrupt handler.


;*******************************************************************************
; portb_change_isr()
;
; Description:    This is the interrupt service routine (ISR) for PortB State change
;                 interrupt.
; 
; Inputs:                         
;  
; Outputs:             
;*********************************************************************************
 portb_change_isr
                  banksel   INTCON
                  bcf       INTCON,RBIF                   ; Clear the PORTB interrupt flag.     
    
                  banksel   iocb_flag 
                  incf      iocb_flag 
                 
                                   
                 ; banksel   TMR0                          ; Clear the hardware timer (Timer 0).
                 ; clrf      TMR0    
                 
                 ; movlw     0x00                          ; Set PD0 to PD7 to outputs.
                 ; banksel   TRISD
                 ; movwf     TRISD

                 ; movlw     0x55
                 ; banksel   PORTD
                 ; movwf     PORTD                         ; Initialize PORTD with a 0x55 display pattern.
                  
                 ; banksel   INTCON
;timer0_rollover   btfss     INTCON,T0IF                    
                  ;goto      timer0_rollover                  

                  ;banksel   INTCON
                 ; bcf       INTCON,T0IF                   ; Clear the Timer 0 interrupt flag.

                  return                                  ; Return to the main interrupt handler.


;******************************************************************************
; End of program
;******************************************************************************

                   end
