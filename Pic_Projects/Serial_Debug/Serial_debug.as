opt subtitle "HI-TECH Software Omniscient Code Generator (Lite mode) build 9453"

opt pagewidth 120

	opt lm

	processor	16F887
clrc	macro
	bcf	3,0
	endm
clrz	macro
	bcf	3,2
	endm
setc	macro
	bsf	3,0
	endm
setz	macro
	bsf	3,2
	endm
skipc	macro
	btfss	3,0
	endm
skipz	macro
	btfss	3,2
	endm
skipnc	macro
	btfsc	3,0
	endm
skipnz	macro
	btfsc	3,2
	endm
indf	equ	0
indf0	equ	0
pc	equ	2
pcl	equ	2
status	equ	3
fsr	equ	4
fsr0	equ	4
c	equ	1
z	equ	0
pclath	equ	10
# 14 "E:\pic_projects\Serial_Debug\main.c"
	psect config,class=CONFIG,delta=2 ;#
# 14 "E:\pic_projects\Serial_Debug\main.c"
	dw 0x1FFF & 0x3FFF & 0x3FFF & 0x3BFF & 0x3EFF & 0x3FFF & 0x3FFF & 0x3FFF & 0x3FEF & 0x3FF7 & 0x3FFD ;#
# 15 "E:\pic_projects\Serial_Debug\main.c"
	psect config,class=CONFIG,delta=2 ;#
# 15 "E:\pic_projects\Serial_Debug\main.c"
	dw 0x3FFF ;#
	FNCALL	_main,_serial_data_debug_john
	FNROOT	_main
	global	_PORTD
psect	text79,local,class=CODE,delta=2
global __ptext79
__ptext79:
_PORTD	set	8
	global	_CARRY
_CARRY	set	24
	global	_GIE
_GIE	set	95
	global	_RD0
_RD0	set	64
	global	_RD1
_RD1	set	65
	global	_TRISD
_TRISD	set	136
	global	_EEADR
_EEADR	set	269
	global	_EEDATA
_EEDATA	set	268
	global	_EECON1
_EECON1	set	396
	global	_EECON2
_EECON2	set	397
	global	_RD
_RD	set	3168
	global	_WR
_WR	set	3169
	global	_WREN
_WREN	set	3170
	file	"Serial_debug.as"
	line	#
psect cinit,class=CODE,delta=2
global start_initialization
start_initialization:

psect cinit,class=CODE,delta=2
global end_of_initialization

;End of C runtime variable initialization code

end_of_initialization:
clrf status
ljmp _main	;jump to C main() function
psect	cstackCOMMON,class=COMMON,space=1
global __pcstackCOMMON
__pcstackCOMMON:
	global	?_serial_data_debug_john
?_serial_data_debug_john:	; 0 bytes @ 0x0
	global	??_serial_data_debug_john
??_serial_data_debug_john:	; 0 bytes @ 0x0
	global	?_main
?_main:	; 0 bytes @ 0x0
	ds	3
	global	serial_data_debug_john@x
serial_data_debug_john@x:	; 1 bytes @ 0x3
	ds	1
	global	serial_data_debug_john@port_out
serial_data_debug_john@port_out:	; 1 bytes @ 0x4
	ds	1
	global	serial_data_debug_john@data_out
serial_data_debug_john@data_out:	; 2 bytes @ 0x5
	ds	2
	global	serial_data_debug_john@i
serial_data_debug_john@i:	; 1 bytes @ 0x7
	ds	1
	global	??_main
??_main:	; 0 bytes @ 0x8
	ds	1
;;Data sizes: Strings 0, constant 0, data 0, bss 0, persistent 0 stack 0
;;Auto spaces:   Size  Autos    Used
;; COMMON          13      9       9
;; BANK0           80      0       0
;; BANK1           80      0       0
;; BANK3           85      0       0
;; BANK2           96      0       0

;;
;; Pointer list with targets:



;;
;; Critical Paths under _main in COMMON
;;
;;   _main->_serial_data_debug_john
;;
;; Critical Paths under _main in BANK0
;;
;;   None.
;;
;; Critical Paths under _main in BANK1
;;
;;   None.
;;
;; Critical Paths under _main in BANK3
;;
;;   None.
;;
;; Critical Paths under _main in BANK2
;;
;;   None.

;;
;;Main: autosize = 0, tempsize = 1, incstack = 0, save=0
;;

;;
;;Call Graph Tables:
;;
;; ---------------------------------------------------------------------------------
;; (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;; ---------------------------------------------------------------------------------
;; (0) _main                                                 1     1      0      90
;;                                              8 COMMON     1     1      0
;;             _serial_data_debug_john
;; ---------------------------------------------------------------------------------
;; (1) _serial_data_debug_john                               8     8      0      90
;;                                              0 COMMON     8     8      0
;; ---------------------------------------------------------------------------------
;; Estimated maximum stack depth 1
;; ---------------------------------------------------------------------------------

;; Call Graph Graphs:

;; _main (ROOT)
;;   _serial_data_debug_john
;;

;; Address spaces:

;;Name               Size   Autos  Total    Cost      Usage
;;BITCOMMON            D      0       0       0        0.0%
;;EEDATA             100      0       0       0        0.0%
;;NULL                 0      0       0       0        0.0%
;;CODE                 0      0       0       0        0.0%
;;COMMON               D      9       9       1       69.2%
;;BITSFR0              0      0       0       1        0.0%
;;SFR0                 0      0       0       1        0.0%
;;BITSFR1              0      0       0       2        0.0%
;;SFR1                 0      0       0       2        0.0%
;;STACK                0      0       1       2        0.0%
;;ABS                  0      0       0       3        0.0%
;;BITBANK0            50      0       0       4        0.0%
;;BITSFR3              0      0       0       4        0.0%
;;SFR3                 0      0       0       4        0.0%
;;BANK0               50      0       0       5        0.0%
;;BITSFR2              0      0       0       5        0.0%
;;SFR2                 0      0       0       5        0.0%
;;BITBANK1            50      0       0       6        0.0%
;;BANK1               50      0       0       7        0.0%
;;BITBANK3            55      0       0       8        0.0%
;;BANK3               55      0       0       9        0.0%
;;BITBANK2            60      0       0      10        0.0%
;;BANK2               60      0       0      11        0.0%
;;DATA                 0      0       0      12        0.0%

	global	_main
psect	maintext,global,class=CODE,delta=2
global __pmaintext
__pmaintext:

;; *************** function _main *****************
;; Defined at:
;;		line 31 in file "E:\pic_projects\Serial_Debug\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0, btemp+1, pclath, cstack
;; Tracked objects:
;;		On entry : 17F/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          1       0       0       0       0
;;      Totals:         1       0       0       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels required when called:    1
;; This function calls:
;;		_serial_data_debug_john
;; This function is called by:
;;		Startup code after reset
;; This function uses a non-reentrant model
;;
psect	maintext
	file	"E:\pic_projects\Serial_Debug\main.c"
	line	31
	global	__size_of_main
	__size_of_main	equ	__end_of_main-_main
	
_main:	
	opt	stack 7
; Regs used in _main: [wreg+status,2+status,0+btemp+1+pclath+cstack]
	line	32
	
l3935:	
;main.c: 32: TRISD &= 0xFD;
	movlw	(0FDh)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	andwf	(136)^080h,f	;volatile
	goto	l3937
	line	33
;main.c: 33: while(1)
	
l837:	
	line	39
	
l3937:	
;main.c: 34: {
;main.c: 39: RD1 = TRUE;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	bsf	(65/8),(65)&7
	line	40
	
l3939:	
;main.c: 40: serial_data_debug_john(0x32);
	movlw	(032h)
	fcall	_serial_data_debug_john
	line	41
	
l3941:	
;main.c: 41: RD1 = FALSE;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	bcf	(65/8),(65)&7
	line	42
	
l3943:	
# 42 "E:\pic_projects\Serial_Debug\main.c"
nop ;#
psect	maintext
	goto	l3937
	line	55
	
l838:	
	line	33
	goto	l3937
	
l839:	
	line	56
	
l840:	
	global	start
	ljmp	start
	opt stack 0
GLOBAL	__end_of_main
	__end_of_main:
;; =============== function _main ends ============

	signat	_main,88
	global	_serial_data_debug_john
psect	text80,local,class=CODE,delta=2
global __ptext80
__ptext80:

;; *************** function _serial_data_debug_john *****************
;; Defined at:
;;		line 57 in file "E:\pic_projects\Serial_Debug\serial_dubug.c"
;; Parameters:    Size  Location     Type
;;  x               1    wreg     unsigned char 
;; Auto vars:     Size  Location     Type
;;  x               1    3[COMMON] unsigned char 
;;  data_out        2    5[COMMON] unsigned short 
;;  i               1    7[COMMON] unsigned char 
;;  port_out        1    4[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0, btemp+1
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         5       0       0       0       0
;;      Temps:          3       0       0       0       0
;;      Totals:         8       0       0       0       0
;;Total ram usage:        8 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text80
	file	"E:\pic_projects\Serial_Debug\serial_dubug.c"
	line	57
	global	__size_of_serial_data_debug_john
	__size_of_serial_data_debug_john	equ	__end_of_serial_data_debug_john-_serial_data_debug_john
	
_serial_data_debug_john:	
	opt	stack 7
; Regs used in _serial_data_debug_john: [wreg+status,2+status,0+btemp+1]
;serial_data_debug_john@x stored from wreg
	line	64
	movwf	(serial_data_debug_john@x)
	
l3035:	
;serial_dubug.c: 59: uint16_t data_out;
;serial_dubug.c: 60: uint8_t port_out;
;serial_dubug.c: 61: uint8_t i;
;serial_dubug.c: 64: TRISD &= 0xFE;
	movlw	(0FEh)
	movwf	(??_serial_data_debug_john+0)+0
	movf	(??_serial_data_debug_john+0)+0,w
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	andwf	(136)^080h,f	;volatile
	line	70
	
l3037:	
;serial_dubug.c: 70: data_out = (uint16_t)((0x8000) | ((uint16_t)(x) << 7));
	movf	(serial_data_debug_john@x),w
	movwf	(??_serial_data_debug_john+0)+0
	clrf	(??_serial_data_debug_john+0)+0+1
	movlw	07h
	movwf	btemp+1
u15:
	clrc
	rlf	(??_serial_data_debug_john+0)+0,f
	rlf	(??_serial_data_debug_john+0)+1,f
	decfsz	btemp+1,f
	goto	u15
	movlw	low(08000h)
	iorwf	0+(??_serial_data_debug_john+0)+0,w
	movwf	(serial_data_debug_john@data_out)
	movlw	high(08000h)
	iorwf	1+(??_serial_data_debug_john+0)+0,w
	movwf	1+(serial_data_debug_john@data_out)
	line	73
	
l3039:	
;serial_dubug.c: 73: for (i = 0; i < 10; i++)
	clrf	(serial_data_debug_john@i)
	
l3041:	
	movlw	(0Ah)
	subwf	(serial_data_debug_john@i),w
	skipc
	goto	u21
	goto	u20
u21:
	goto	l3045
u20:
	goto	l1682
	
l3043:	
	goto	l1682
	line	74
	
l1680:	
	line	76
	
l3045:	
;serial_dubug.c: 74: {
;serial_dubug.c: 76: port_out = (uint8_t)(PORTD & 0xFE);
	bcf	status, 5	;RP0=0, select bank0
	movf	(8),w
	andlw	0FEh
	movwf	(??_serial_data_debug_john+0)+0
	movf	(??_serial_data_debug_john+0)+0,w
	movwf	(serial_data_debug_john@port_out)
	line	79
	
l3047:	
;serial_dubug.c: 79: port_out |= (uint8_t)((0x8000 & data_out) >> 15);
	movf	(serial_data_debug_john@data_out+1),w
	movwf	(??_serial_data_debug_john+0)+0+1
	movf	(serial_data_debug_john@data_out),w
	movwf	(??_serial_data_debug_john+0)+0
	movlw	0Fh
u35:
	clrc
	rrf	(??_serial_data_debug_john+0)+1,f
	rrf	(??_serial_data_debug_john+0)+0,f
	addlw	-1
	skipz
	goto	u35
	movf	0+(??_serial_data_debug_john+0)+0,w
	andlw	01h
	movwf	(??_serial_data_debug_john+2)+0
	movf	(??_serial_data_debug_john+2)+0,w
	iorwf	(serial_data_debug_john@port_out),f
	line	82
	
l3049:	
;serial_dubug.c: 82: data_out = data_out << 1;
	movf	(serial_data_debug_john@data_out+1),w
	movwf	(??_serial_data_debug_john+0)+0+1
	movf	(serial_data_debug_john@data_out),w
	movwf	(??_serial_data_debug_john+0)+0
	movlw	01h
	movwf	btemp+1
u45:
	clrc
	rlf	(??_serial_data_debug_john+0)+0,f
	rlf	(??_serial_data_debug_john+0)+1,f
	decfsz	btemp+1,f
	goto	u45
	movf	0+(??_serial_data_debug_john+0)+0,w
	movwf	(serial_data_debug_john@data_out)
	movf	1+(??_serial_data_debug_john+0)+0,w
	movwf	(serial_data_debug_john@data_out+1)
	line	85
	
l3051:	
;serial_dubug.c: 85: PORTD = port_out;
	movf	(serial_data_debug_john@port_out),w
	movwf	(8)	;volatile
	line	73
	
l3053:	
	movlw	(01h)
	movwf	(??_serial_data_debug_john+0)+0
	movf	(??_serial_data_debug_john+0)+0,w
	addwf	(serial_data_debug_john@i),f
	
l3055:	
	movlw	(0Ah)
	subwf	(serial_data_debug_john@i),w
	skipc
	goto	u51
	goto	u50
u51:
	goto	l3045
u50:
	goto	l1682
	
l1681:	
	line	87
	
l1682:	
	return
	opt stack 0
GLOBAL	__end_of_serial_data_debug_john
	__end_of_serial_data_debug_john:
;; =============== function _serial_data_debug_john ends ============

	signat	_serial_data_debug_john,4216
psect	text81,local,class=CODE,delta=2
global __ptext81
__ptext81:
	global	btemp
	btemp set 07Eh

	DABS	1,126,2	;btemp
	global	wtemp0
	wtemp0 set btemp
	end
