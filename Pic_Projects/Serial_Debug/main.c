
/***********************************************
/*DATE 18 FEB 2013 
/* PROJECT NAME: Serial_debug example
/*
/*
/**********************************************/
#include <htc.h>
#include "typedefs.h"
#include "serial_debug.h"


      /*PIC16F887 Configuration*/
__CONFIG(DEBUGEN & LVPEN & FCMEN & IESODIS & BORXSLP & DUNPROTECT & UNPROTECT & MCLREN & PWRTEN & WDTDIS & INTCLK);
__CONFIG(BORV40);  //HS means resonator 8Mhz      
      
/*CONSTANT's*/
/*NONE*/

/*typedef*/
/*NONE*/

/*global variables*/
//CANTXMESSAGE *buffer;
//CANTXMESSAGE  Toms_message;
/*Function prototypes*/
void main(void);

void main(void)
{
   TRISD &= 0xFD;   /* Enable Port 1 output*/
  while(1)
  {
   // RD1 = TRUE;
   // serial_data_debug_tom(0x32);
   // RD1 = FALSE;
   // asm("nop");
    RD1 = TRUE;
    serial_data_debug_john(0x32);
    RD1 = FALSE;
    asm("nop");
    // buffer = &Toms_message; 
    // buffer -> messageword[0] = 0;
    // buffer -> messageword[1] = 0;
    // buffer -> messageword[2] = 0;
    // buffer -> messageword[3] = 0;
    // buffer -> msgSID.SID = 0x515; /* CAN ID*/
     //buffer -> msgEID.DLC = 4;     /* 4 bytes of payload*/
    // buffer -> msgEID.SRR = 2;     /* SRR reg*/
    // buffer -> data[0]    = 0xDE;
    // buffer -> data[1]    = 0xAD;
    // buffer -> data[2]    = 0xBE;
    // buffer -> data[3]    = 0xEF;
   }
}