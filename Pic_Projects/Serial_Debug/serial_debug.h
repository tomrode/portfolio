#include "typedefs.h"
/***********************************************
/* 
/* PROJECT NAME: Serial_debug
/*
/*
/**********************************************/
#ifndef SERIAL_DEBUG_H
#define SERIAL_DEBUG_H
/* Function Prototypes*/
void serial_data_debug_tom(uint8_t x);
void serial_data_debug_john(uint8_t x);
#endif 