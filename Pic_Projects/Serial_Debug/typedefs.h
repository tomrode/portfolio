/*****************************************************************************/
/* FILE NAME:  typedefs.h                                                    */
/*                                                                           */
/* Description:  Standard type definitions.                                  */
/*****************************************************************************/

#ifndef TYPEDEFS_H
#define TYPEDEFS_H

enum
{
FALSE,                       // implied to be zero
TRUE,                        // implied to be one
DEFAULT
};

typedef signed char sint8_t;
typedef unsigned char uint8_t;
typedef volatile signed char vsint8_t;
typedef volatile unsigned char vuint8_t;

typedef signed short sint16_t;
typedef unsigned short uint16_t;
typedef volatile signed short vsint16_t;
typedef volatile unsigned short vuint16_t;

typedef signed long sint32_t;
typedef unsigned long uint32_t;
typedef volatile signed long vsint32_t;
typedef volatile unsigned long vuint32_t;

/******************************************************************/
/* Global TYPE FOR THE WHOLE PROJECT                           */
/*                                                                */
/*                                                                */       
/******************************************************************/
/*
typedef struct
             {
               uint32_t SID     : 11;
               uint32_t Padding : 21;  

             }CAN_SID;
typedef struct
             {
               uint32_t DLC:4;
	           uint32_t RB0:1;
	           uint32_t   :3;
               uint32_t RB1:1;
	           uint32_t RTR:1;
	           uint32_t EID:18; 
	           uint32_t IDE:1;
	           uint32_t SRR:1;
	           uint32_t :2;
              }CAN_EID;

typedef union
             {
               struct
                    {
                      CAN_SID msgSID;
                      CAN_EID msgEID;
                      uint8_t data[8];
                    };
                uint32_t messageword[4];   
             }CANTXMESSAGE;
*/
#endif
