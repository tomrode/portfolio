#include "htc.h"
#include "typedefs.h"

/*****************************************************************************/
/* serial_data_debug()                                                       */
/*                                                                           */
/* Description:  Used as a serial marker for debugging purposes.  It will    */
/*               send out data in the following format:                      */
/*               [ START | D7 | D6 | D5 | D4 | D3 | D2 | D1 | D0 | STOP ]    */
/*                                                                           */
/*               Where:                                                      */
/*               START = Start bit ("1")                                     */
/*               DN    = Data bit N (MSB to LSB)                             */
/*               STOP  = Stop bit ("0")                                      */
/*                                                                           */
/* Inputs:       Data byte to transmit.                                      */
/*                                                                           */
/* Outputs:      None                                                        */
/*                                                                           */
/* Calls To:     None                                                        */
/*                                                                           */
/* Calls From:   User-defined.                                               */
/*****************************************************************************/

void serial_data_debug (uint8_t x)
{
  /* Local variables */
  uint16_t data_out;
  uint8_t port_out;
  uint8_t i;

  /* Set only RD0 to output. */
  TRISD &= 0xFE;

  /* Setup serial data output (current outgoing bit is in the MSB). */
  /* Note that "data_out" will contain the following data (but not all bits will be transmitted):  */
  /* [ 1 | D7 | D6 | D5 | D4 | D3 | D2 | D1 | D0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 ] */
  
  data_out = (uint16_t)((0x8000) | ((uint16_t)(x) << 7));

  /* Loop to send out all data bits (10 total). */
//asm("nop");
  for (i = 0; i < 10; i++)
  {
    /* Read the current value of PORTD with BIT 0 cleared initially. */
    port_out = (uint8_t)(PORTD & 0xFE);

    /* Calculate the BIT 0 value based on the outgoing serial data bit (BIT 15). */
    port_out |= (uint8_t)((0x8000 & data_out) >> 15);

    /* Shift the next outgoing serial data bit into position. */
    data_out = data_out << 1;

    /* Update PORTD. */
    PORTD = port_out; 
  }
//asm("nop");
}
