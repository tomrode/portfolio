#include <htc.h>
#include "serial_debug.h"
/***************************************************************************************************************
* serial_data_debug_tom()
* 
* Intent: Used as a Serial marker for debugging purposes.
*  
* INPUT  -> 1 byte
* OUTPUT -> NONE
*
* CALLS TO:   NONE
* CALLS FROM: User defined 
/****************************************************************************************************************/
void serial_data_debug_tom(uint8_t x)
{
 /* Local variables*/
  uint8_t i; 
  uint16_t test_val;
  uint8_t and_against_mask;
  uint16_t result;
  /* Initialization Section */
  TRISD &= 0xFE;                                     /* Make TRIS0 as output or what every Data direction available.*/
  result = 0;                                           /* Clear out the result to known state.*/      
  and_against_mask = 0x01;                    /* set the mask for logical and operation. */
  test_val =(0x0001 | (x << 8));               /* get the start bit and byte in question, put together in 16 bit form. */ 
  
 for(i=0; i<16; i++)
   { 
      result = (test_val & and_against_mask);  /* Logical and, if true output to port*/
      RD0 = result;                                        /* out to port pin*/
      test_val = (test_val >> 1);                     /* shift left this word for next iteration of tests*/
   } 
}
 
/*****************************************************************************/
/* serial_data_debug_john()                                                       */
/*                                                                           */
/* Description:  Used as a serial marker for debugging purposes.  It will    */
/*               send out data in the following format:                      */
/*               [ START | D7 | D6 | D5 | D4 | D3 | D2 | D1 | D0 | STOP ]    */
/*                                                                           */
/*               Where:                                                      */
/*               START = Start bit ("1")                                     */
/*               DN    = Data bit N (MSB to LSB)                             */
/*               STOP  = Stop bit ("0")                                      */
/*                                                                           */
/* Inputs:       Data byte to transmit.                                      */
/*                                                                           */
/* Outputs:      None                                                        */
/*                                                                           */
/* Calls To:     None                                                        */
/*                                                                           */
/* Calls From:   User-defined.                                               */
/*****************************************************************************/

void serial_data_debug_john (uint8_t x)
{
  /* Local variables */
  uint16_t data_out;
  uint8_t port_out;
  uint8_t i;

  /* Set only RD0 to output. */
  TRISD &= 0xFE;

  /* Setup serial data output (current outgoing bit is in the MSB). */
  /* Note that "data_out" will contain the following data (but not all bits will be transmitted):  */
  /* [ 1 | D7 | D6 | D5 | D4 | D3 | D2 | D1 | D0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 ] */
  
  data_out = (uint16_t)((0x8000) | ((uint16_t)(x) << 7));

  /* Loop to send out all data bits (10 total). */
  for (i = 0; i < 10; i++)
  {
    /* Read the current value of PORTD with BIT 0 cleared initially. */
    port_out = (uint8_t)(PORTD & 0xFE);

    /* Calculate the BIT 0 value based on the outgoing serial data bit (BIT 15). */
    port_out |= (uint8_t)((0x8000 & data_out) >> 15);

    /* Shift the next outgoing serial data bit into position. */
    data_out = data_out << 1;

    /* Update PORTD. */
    PORTD = port_out; 
  }
}
