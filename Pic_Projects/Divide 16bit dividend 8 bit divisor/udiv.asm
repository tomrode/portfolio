                   list      p=16f887
                   #include  <p16f887.inc>

;******************************************************************************
; Device Configuration
;******************************************************************************

                   __CONFIG  _CONFIG1, _LVP_OFF & _FCMEN_ON & _IESO_OFF & _BOR_OFF & _CPD_OFF & _CP_OFF & _MCLRE_ON & _PWRTE_ON & _WDT_OFF & _INTRC_OSC_NOCLKOUT
                   __CONFIG  _CONFIG2, _WRT_OFF & _BOR21V

;******************************************************************************
; Variable Definitions
;******************************************************************************

dvnd_hi            equ       0x79
dvnd_lo            equ       0x7A
dvsr               equ       0x7B
res_hi             equ       0x7C
res_lo             equ       0x7D
rmndr              equ       0x7E
udiv_ovf           equ       0x7F

;******************************************************************************
; Reset Vector
;******************************************************************************

                   org       0x000

                   goto      main

;******************************************************************************
; Interrupt Vector
;******************************************************************************

                   org       0x004

                   retfie                                  ; Return from interrupt.

;******************************************************************************
; Main Program
;******************************************************************************

main
                   movlw     0x61                          ; Select internal 4 MHz clock.
                   banksel   OSCCON
                   movwf     OSCCON
wait_stable
                   ;btfss     OSCCON,HTS                    ; Is the high-speed internal oscillator stable?
                   ;goto      wait_stable                   ; If not, wait until it is.

                   banksel   OPTION_REG
                   movf      OPTION_REG,W                  ; Load the current value of the OPTION register.
                   andlw     0xD7                          ; Clear T0CS and PSA.
                   iorlw     0x07                          ; Set PS2, PS1, and PS0.
                   movwf     OPTION_REG                    ; Update the OPTION register.

;******************************************************************************
; Main Loop
;******************************************************************************

main_loop
                   movlw     0xEE
                   banksel   dvnd_hi
                   movwf     dvnd_hi                       ; Set the dividend high byte.

                   movlw     0x44
                   banksel   dvnd_lo
                   movwf     dvnd_lo                       ; Set the dividend low byte.

                   movlw     0x05
                   banksel   dvsr
                   movwf     dvsr                          ; Set the divisor value.

                   call      udiv                          ; Perform an unsigned division.

main_end
                   goto      main_end

;******************************************************************************
; umult()
;
; Description:  This function performs an unsigned 16-bit by 8-bit
;               division (16-bit result, 8-bit remainder).  Division by
;               zero will return a result of all binary 1's and a remainder
;               of zero.
;
; Inputs:       dvnd_hi -> Dividend high byte
;               dvnd_lo -> Dividend low byte
;               dvsr    -> Divisor
;
; Outputs:      res_hi  -> Result high byte
;               res_lo  -> Result low byte
;               rmndr   -> Remainder
;******************************************************************************

udiv
                   banksel   res_hi
                   clrf      res_hi                        ; Clear the result high byte.

                   movlw     0x01
                   banksel   res_lo
                   movwf     res_lo                        ; Set the termination bit in the result low byte.

                   banksel   rmndr
                   clrf      rmndr                         ; Clear the remainder.
                   banksel   udiv_ovf
                   clrf      udiv_ovf                      ; Clear the remainder overflow.

udiv_loop
                   banksel   STATUS
                   bcf       STATUS,C                      ; Clear the carry bit.
                   banksel   dvnd_lo
                   rlf       dvnd_lo,F                     ; Rotate left the dividend low byte.
                   banksel   dvnd_hi
                   rlf       dvnd_hi,F                     ; Rotate left the dividend high byte.
                   banksel   rmndr
                   rlf       rmndr,F                       ; Rotate left the partial remainder.
                   banksel   udiv_ovf
                   rlf       udiv_ovf,F                    ; Rotate left the partial remainder overflow.

                   banksel   dvsr
                   movf      dvsr,W                        ; Load the divisor into W.
                   banksel   rmndr
                   subwf     rmndr,F                       ; Subtract it from the partial remainder.

                   banksel   STATUS
                   btfsc     STATUS,C                      ; Did a borrow occur (carry bit equals zero)?
                   goto      udiv_update                   ; If not then continue on with the carry bit set (no borrow).

                   movlw     0x01                          ; Load the borrow value.
                   banksel   udiv_ovf
                   subwf     udiv_ovf,F                    ; Subtract it from the partial remainder overflow.

                   banksel   STATUS
                   btfsc     STATUS,C                      ; Did another borrow occur (carry bit equals zero)?
                   goto      udiv_update                   ; If not then continue on with the carry bit set (no borrow).

                   banksel   dvsr
                   movf      dvsr,W                        ; Reload the divisor into W.
                   banksel   rmndr
                   addwf     rmndr,F                       ; Undo the subtraction from the partial remainder.

                   movlw     0x00                          ; Assume no carry occurred.
                   banksel   STATUS
                   btfsc     STATUS,C                      ; Did a carry occur from the addition?
                   movlw     0x01                          ; If so then account for the carry.
                   banksel   udiv_ovf
                   addwf     udiv_ovf,F                    ; Add in the carry value (whether set or cleared).

                   banksel   STATUS
                   bcf       STATUS,C                      ; Clear the carry bit to indicate that a borrow occurred.

udiv_update
                   banksel   res_lo
                   rlf       res_lo,F                      ; Rotate left the result low byte (bringing in the borrow bit).
                   banksel   res_hi
                   rlf       res_hi,F                      ; Rotate left the result high byte.

                   banksel   STATUS
                   btfss     STATUS,C                      ; Is the termination bit now in the carry bit?
                   goto      udiv_loop                     ; If not then continue looping until all iterations are complete.

udiv_by_zero
                   banksel   dvsr
                   movf      dvsr,F                        ; Test if the divisor is zero.
                   banksel   STATUS
                   btfss     STATUS,Z                      ; Is it zero?
                   goto      udiv_end                      ; If not then continue on.

                   banksel   rmndr
                   clrf      rmndr                         ; Set the remainder to zero.

udiv_end
                   return

;******************************************************************************
; End of program
;******************************************************************************

                   end
