opt subtitle "HI-TECH Software Omniscient Code Generator (Lite mode) build 10920"

opt pagewidth 120

	opt lm

	processor	16F887
clrc	macro
	bcf	3,0
	endm
clrz	macro
	bcf	3,2
	endm
setc	macro
	bsf	3,0
	endm
setz	macro
	bsf	3,2
	endm
skipc	macro
	btfss	3,0
	endm
skipz	macro
	btfss	3,2
	endm
skipnc	macro
	btfsc	3,0
	endm
skipnz	macro
	btfsc	3,2
	endm
indf	equ	0
indf0	equ	0
pc	equ	2
pcl	equ	2
status	equ	3
fsr	equ	4
fsr0	equ	4
c	equ	1
z	equ	0
pclath	equ	10
	FNCALL	_main,_push_dtc_to_stack
	FNROOT	_main
	global	_dtc_array_list
psect	strings,class=STRING,delta=2
global __pstrings
__pstrings:
;	global	stringdir,stringtab,__stringbase
stringtab:
;	String table - string pointers are 1 byte each
stringcode:stringdir:
movlw high(stringdir)
movwf pclath
movf fsr,w
incf fsr
	addwf pc
__stringbase:
	retlw	0
psect	strings
	file	"C:\pic_projects\dtc_chrono_stack\main.c"
	line	21
_dtc_array_list:
	retlw	0
	retlw	01h
	retlw	02h
	retlw	03h
	retlw	04h
	retlw	05h
	global	_dtc_array_list
	global	_current_dtc_stack
	global	_dtc_names
	global	_toms_variable
	global	push_dtc_to_stack@dtc_counter
	global	_CARRY
_CARRY	set	24
	global	_GIE
_GIE	set	95
	global	_EEADR
_EEADR	set	269
	global	_EEDATA
_EEDATA	set	268
	global	_EECON1
_EECON1	set	396
	global	_EECON2
_EECON2	set	397
	global	_RD
_RD	set	3168
	global	_WR
_WR	set	3169
	global	_WREN
_WREN	set	3170
	file	"dtc_chrono_stack.as"
	line	#
psect cinit,class=CODE,delta=2
global start_initialization
start_initialization:

psect	bssCOMMON,class=COMMON,space=1
global __pbssCOMMON
__pbssCOMMON:
push_dtc_to_stack@dtc_counter:
       ds      1

psect	bssBANK0,class=BANK0,space=1
global __pbssBANK0
__pbssBANK0:
_current_dtc_stack:
       ds      6

_dtc_names:
       ds      1

_toms_variable:
       ds      1

; Clear objects allocated to COMMON
psect cinit,class=CODE,delta=2
	clrf	((__pbssCOMMON)+0)&07Fh
; Clear objects allocated to BANK0
psect cinit,class=CODE,delta=2
	clrf	((__pbssBANK0)+0)&07Fh
	clrf	((__pbssBANK0)+1)&07Fh
	clrf	((__pbssBANK0)+2)&07Fh
	clrf	((__pbssBANK0)+3)&07Fh
	clrf	((__pbssBANK0)+4)&07Fh
	clrf	((__pbssBANK0)+5)&07Fh
	clrf	((__pbssBANK0)+6)&07Fh
	clrf	((__pbssBANK0)+7)&07Fh
psect cinit,class=CODE,delta=2
global end_of_initialization

;End of C runtime variable initialization code

end_of_initialization:
clrf status
ljmp _main	;jump to C main() function
psect	cstackCOMMON,class=COMMON,space=1
global __pcstackCOMMON
__pcstackCOMMON:
	global	?_push_dtc_to_stack
?_push_dtc_to_stack:	; 0 bytes @ 0x0
	global	??_push_dtc_to_stack
??_push_dtc_to_stack:	; 0 bytes @ 0x0
	global	?_main
?_main:	; 0 bytes @ 0x0
	ds	1
	global	push_dtc_to_stack@temp
push_dtc_to_stack@temp:	; 6 bytes @ 0x1
	ds	6
	global	push_dtc_to_stack@flag
push_dtc_to_stack@flag:	; 1 bytes @ 0x7
	ds	1
	global	push_dtc_to_stack@j
push_dtc_to_stack@j:	; 1 bytes @ 0x8
	ds	1
	global	push_dtc_to_stack@dtc_num
push_dtc_to_stack@dtc_num:	; 1 bytes @ 0x9
	ds	1
	global	push_dtc_to_stack@i
push_dtc_to_stack@i:	; 1 bytes @ 0xA
	ds	1
	global	??_main
??_main:	; 0 bytes @ 0xB
	ds	1
	global	main@reported_dtc
main@reported_dtc:	; 1 bytes @ 0xC
	ds	1
;;Data sizes: Strings 0, constant 6, data 0, bss 9, persistent 0 stack 0
;;Auto spaces:   Size  Autos    Used
;; COMMON          14     13      14
;; BANK0           80      0       8
;; BANK1           80      0       0
;; BANK3           96      0       0
;; BANK2           96      0       0

;;
;; Pointer list with targets:



;;
;; Critical Paths under _main in COMMON
;;
;;   _main->_push_dtc_to_stack
;;
;; Critical Paths under _main in BANK0
;;
;;   None.
;;
;; Critical Paths under _main in BANK1
;;
;;   None.
;;
;; Critical Paths under _main in BANK3
;;
;;   None.
;;
;; Critical Paths under _main in BANK2
;;
;;   None.

;;
;;Main: autosize = 0, tempsize = 1, incstack = 0, save=0
;;

;;
;;Call Graph Tables:
;;
;; ---------------------------------------------------------------------------------
;; (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;; ---------------------------------------------------------------------------------
;; (0) _main                                                 2     2      0     617
;;                                             11 COMMON     2     2      0
;;                  _push_dtc_to_stack
;; ---------------------------------------------------------------------------------
;; (1) _push_dtc_to_stack                                   11    11      0     479
;;                                              0 COMMON    11    11      0
;; ---------------------------------------------------------------------------------
;; Estimated maximum stack depth 1
;; ---------------------------------------------------------------------------------

;; Call Graph Graphs:

;; _main (ROOT)
;;   _push_dtc_to_stack
;;

;; Address spaces:

;;Name               Size   Autos  Total    Cost      Usage
;;BITCOMMON            E      0       0       0        0.0%
;;EEDATA             100      0       0       0        0.0%
;;NULL                 0      0       0       0        0.0%
;;CODE                 0      0       0       0        0.0%
;;COMMON               E      D       E       1      100.0%
;;BITSFR0              0      0       0       1        0.0%
;;SFR0                 0      0       0       1        0.0%
;;BITSFR1              0      0       0       2        0.0%
;;SFR1                 0      0       0       2        0.0%
;;STACK                0      0       1       2        0.0%
;;ABS                  0      0      16       3        0.0%
;;BITBANK0            50      0       0       4        0.0%
;;BITSFR3              0      0       0       4        0.0%
;;SFR3                 0      0       0       4        0.0%
;;BANK0               50      0       8       5       10.0%
;;BITSFR2              0      0       0       5        0.0%
;;SFR2                 0      0       0       5        0.0%
;;BITBANK1            50      0       0       6        0.0%
;;BANK1               50      0       0       7        0.0%
;;BITBANK3            60      0       0       8        0.0%
;;BANK3               60      0       0       9        0.0%
;;BITBANK2            60      0       0      10        0.0%
;;BANK2               60      0       0      11        0.0%
;;DATA                 0      0      17      12        0.0%

	global	_main
psect	maintext,global,class=CODE,delta=2
global __pmaintext
__pmaintext:

;; *************** function _main *****************
;; Defined at:
;;		line 42 in file "C:\pic_projects\dtc_chrono_stack\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  reported_dtc    1   12[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 17F/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         1       0       0       0       0
;;      Temps:          1       0       0       0       0
;;      Totals:         2       0       0       0       0
;;Total ram usage:        2 bytes
;; Hardware stack levels required when called:    1
;; This function calls:
;;		_push_dtc_to_stack
;; This function is called by:
;;		Startup code after reset
;; This function uses a non-reentrant model
;;
psect	maintext
	file	"C:\pic_projects\dtc_chrono_stack\main.c"
	line	42
	global	__size_of_main
	__size_of_main	equ	__end_of_main-_main
	
_main:	
	opt	stack 7
; Regs used in _main: [wreg-fsr0h+status,2+status,0+pclath+cstack]
	line	45
;main.c: 43: uint8_t reported_dtc;
;main.c: 45: while(1)
	
l845:	
	line	47
	
l3228:	
;main.c: 46: {
;main.c: 47: reported_dtc = OPEN;
	clrf	(main@reported_dtc)
	bsf	status,0
	rlf	(main@reported_dtc),f
	line	48
	
l3230:	
;main.c: 48: push_dtc_to_stack(reported_dtc);
	movf	(main@reported_dtc),w
	fcall	_push_dtc_to_stack
	line	50
	
l3232:	
;main.c: 50: reported_dtc = SHORT;
	movlw	(02h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(main@reported_dtc)
	line	51
	
l3234:	
;main.c: 51: push_dtc_to_stack(reported_dtc);
	movf	(main@reported_dtc),w
	fcall	_push_dtc_to_stack
	line	53
	
l3236:	
;main.c: 53: reported_dtc = INVALID_CONFIG;
	movlw	(04h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(main@reported_dtc)
	line	54
	
l3238:	
;main.c: 54: push_dtc_to_stack(reported_dtc);
	movf	(main@reported_dtc),w
	fcall	_push_dtc_to_stack
	line	56
	
l3240:	
;main.c: 56: reported_dtc = PARTIAL_LOAD;
	movlw	(05h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(main@reported_dtc)
	line	57
	
l3242:	
;main.c: 57: push_dtc_to_stack(reported_dtc);
	movf	(main@reported_dtc),w
	fcall	_push_dtc_to_stack
	line	59
	
l3244:	
;main.c: 59: reported_dtc = LOSS_COMM;
	movlw	(03h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(main@reported_dtc)
	line	60
	
l3246:	
;main.c: 60: push_dtc_to_stack(reported_dtc);
	movf	(main@reported_dtc),w
	fcall	_push_dtc_to_stack
	line	62
	
l3248:	
# 62 "C:\pic_projects\dtc_chrono_stack\main.c"
nop ;#
psect	maintext
	line	63
	
l3250:	
;main.c: 63: reported_dtc = INVALID_CONFIG;
	movlw	(04h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(main@reported_dtc)
	line	64
	
l3252:	
;main.c: 64: push_dtc_to_stack(reported_dtc);
	movf	(main@reported_dtc),w
	fcall	_push_dtc_to_stack
	goto	l845
	line	70
	
l846:	
	line	45
	goto	l845
	
l847:	
	line	71
	
l848:	
	global	start
	ljmp	start
	opt stack 0
GLOBAL	__end_of_main
	__end_of_main:
;; =============== function _main ends ============

	signat	_main,88
	global	_push_dtc_to_stack
psect	text75,local,class=CODE,delta=2
global __ptext75
__ptext75:

;; *************** function _push_dtc_to_stack *****************
;; Defined at:
;;		line 86 in file "C:\pic_projects\dtc_chrono_stack\main.c"
;; Parameters:    Size  Location     Type
;;  dtc_num         1    wreg     unsigned char 
;; Auto vars:     Size  Location     Type
;;  dtc_num         1    9[COMMON] unsigned char 
;;  temp            6    1[COMMON] unsigned char [6]
;;  i               1   10[COMMON] unsigned char 
;;  j               1    8[COMMON] unsigned char 
;;  flag            1    7[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:        10       0       0       0       0
;;      Temps:          1       0       0       0       0
;;      Totals:        11       0       0       0       0
;;Total ram usage:       11 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text75
	file	"C:\pic_projects\dtc_chrono_stack\main.c"
	line	86
	global	__size_of_push_dtc_to_stack
	__size_of_push_dtc_to_stack	equ	__end_of_push_dtc_to_stack-_push_dtc_to_stack
	
_push_dtc_to_stack:	
	opt	stack 7
; Regs used in _push_dtc_to_stack: [wreg-fsr0h+status,2+status,0]
;push_dtc_to_stack@dtc_num stored from wreg
	line	95
	movwf	(push_dtc_to_stack@dtc_num)
	
l3166:	
;main.c: 88: uint8_t i;
;main.c: 89: uint8_t j;
;main.c: 90: uint8_t flag;
;main.c: 91: uint8_t temp[(6)];
;main.c: 92: static uint8_t dtc_counter;
;main.c: 95: if(dtc_num >= (6))
	movlw	(06h)
	subwf	(push_dtc_to_stack@dtc_num),w
	skipc
	goto	u2321
	goto	u2320
u2321:
	goto	l3170
u2320:
	line	98
	
l3168:	
# 98 "C:\pic_projects\dtc_chrono_stack\main.c"
nop ;#
psect	text75
	line	99
;main.c: 99: }
	goto	l867
	line	101
	
l853:	
	line	103
	
l3170:	
;main.c: 101: else
;main.c: 102: {
;main.c: 103: if(dtc_counter == (6) -1)
	movf	(push_dtc_to_stack@dtc_counter),w
	xorlw	05h
	skipz
	goto	u2331
	goto	u2330
u2331:
	goto	l3216
u2330:
	line	105
	
l3172:	
;main.c: 104: {
;main.c: 105: dtc_counter=0;
	clrf	(push_dtc_to_stack@dtc_counter)
	line	107
;main.c: 107: j=0;
	clrf	(push_dtc_to_stack@j)
	line	110
;main.c: 110: for(i=0; (i<(6)-1);i++)
	clrf	(push_dtc_to_stack@i)
	
l3174:	
	movlw	(05h)
	subwf	(push_dtc_to_stack@i),w
	skipc
	goto	u2341
	goto	u2340
u2341:
	goto	l3178
u2340:
	goto	l857
	
l3176:	
	goto	l857
	line	111
	
l856:	
	line	112
	
l3178:	
;main.c: 111: {
;main.c: 112: if(current_dtc_stack[i] == dtc_num)
	movf	(push_dtc_to_stack@i),w
	addlw	_current_dtc_stack&0ffh
	movwf	fsr0
	bcf	status, 7	;select IRP bank0
	movf	indf,w
	xorwf	(push_dtc_to_stack@dtc_num),w
	skipz
	goto	u2351
	goto	u2350
u2351:
	goto	l3182
u2350:
	line	114
	
l3180:	
;main.c: 113: {
;main.c: 114: current_dtc_stack[i]=0;
	movf	(push_dtc_to_stack@i),w
	addlw	_current_dtc_stack&0ffh
	movwf	fsr0
	clrf	indf
	line	115
;main.c: 115: }
	goto	l3182
	line	116
	
l858:	
	goto	l3182
	line	119
;main.c: 116: else
;main.c: 117: {
	
l859:	
	line	110
	
l3182:	
	movlw	(01h)
	movwf	(??_push_dtc_to_stack+0)+0
	movf	(??_push_dtc_to_stack+0)+0,w
	addwf	(push_dtc_to_stack@i),f
	
l3184:	
	movlw	(05h)
	subwf	(push_dtc_to_stack@i),w
	skipc
	goto	u2361
	goto	u2360
u2361:
	goto	l3178
u2360:
	
l857:	
	line	122
;main.c: 119: }
;main.c: 120: }
;main.c: 122: for(i=0; (i<(6)-1);i++)
	clrf	(push_dtc_to_stack@i)
	
l3186:	
	movlw	(05h)
	subwf	(push_dtc_to_stack@i),w
	skipc
	goto	u2371
	goto	u2370
u2371:
	goto	l3190
u2370:
	goto	l861
	
l3188:	
	goto	l861
	line	123
	
l860:	
	line	124
	
l3190:	
;main.c: 123: {
;main.c: 124: j++;
	movlw	(01h)
	movwf	(??_push_dtc_to_stack+0)+0
	movf	(??_push_dtc_to_stack+0)+0,w
	addwf	(push_dtc_to_stack@j),f
	line	125
	
l3192:	
;main.c: 125: temp[j] = current_dtc_stack[i];
	movf	(push_dtc_to_stack@i),w
	addlw	_current_dtc_stack&0ffh
	movwf	fsr0
	bcf	status, 7	;select IRP bank0
	movf	indf,w
	movwf	(??_push_dtc_to_stack+0)+0
	movf	(push_dtc_to_stack@j),w
	addlw	push_dtc_to_stack@temp&0ffh
	movwf	fsr0
	movf	(??_push_dtc_to_stack+0)+0,w
	movwf	indf
	line	122
	
l3194:	
	movlw	(01h)
	movwf	(??_push_dtc_to_stack+0)+0
	movf	(??_push_dtc_to_stack+0)+0,w
	addwf	(push_dtc_to_stack@i),f
	
l3196:	
	movlw	(05h)
	subwf	(push_dtc_to_stack@i),w
	skipc
	goto	u2381
	goto	u2380
u2381:
	goto	l3190
u2380:
	
l861:	
	line	127
;main.c: 126: }
;main.c: 127: temp[(6)-1]=0;
	clrf	0+(push_dtc_to_stack@temp)+05h
	line	128
	
l3198:	
;main.c: 128: j=1;
	clrf	(push_dtc_to_stack@j)
	bsf	status,0
	rlf	(push_dtc_to_stack@j),f
	line	130
;main.c: 130: for(i=1; (i<(6)-1);i++)
	clrf	(push_dtc_to_stack@i)
	bsf	status,0
	rlf	(push_dtc_to_stack@i),f
	
l3200:	
	movlw	(05h)
	subwf	(push_dtc_to_stack@i),w
	skipc
	goto	u2391
	goto	u2390
u2391:
	goto	l3204
u2390:
	goto	l3212
	
l3202:	
	goto	l3212
	line	131
	
l862:	
	line	132
	
l3204:	
;main.c: 131: {
;main.c: 132: current_dtc_stack[i] = temp[j];
	movf	(push_dtc_to_stack@j),w
	addlw	push_dtc_to_stack@temp&0ffh
	movwf	fsr0
	movf	indf,w
	movwf	(??_push_dtc_to_stack+0)+0
	movf	(push_dtc_to_stack@i),w
	addlw	_current_dtc_stack&0ffh
	movwf	fsr0
	movf	(??_push_dtc_to_stack+0)+0,w
	bcf	status, 7	;select IRP bank0
	movwf	indf
	line	133
	
l3206:	
;main.c: 133: j++;
	movlw	(01h)
	movwf	(??_push_dtc_to_stack+0)+0
	movf	(??_push_dtc_to_stack+0)+0,w
	addwf	(push_dtc_to_stack@j),f
	line	130
	
l3208:	
	movlw	(01h)
	movwf	(??_push_dtc_to_stack+0)+0
	movf	(??_push_dtc_to_stack+0)+0,w
	addwf	(push_dtc_to_stack@i),f
	
l3210:	
	movlw	(05h)
	subwf	(push_dtc_to_stack@i),w
	skipc
	goto	u2401
	goto	u2400
u2401:
	goto	l3204
u2400:
	goto	l3212
	
l863:	
	line	136
	
l3212:	
;main.c: 134: }
;main.c: 136: current_dtc_stack[dtc_counter] = dtc_num;
	movf	(push_dtc_to_stack@dtc_num),w
	movwf	(??_push_dtc_to_stack+0)+0
	movf	(push_dtc_to_stack@dtc_counter),w
	addlw	_current_dtc_stack&0ffh
	movwf	fsr0
	movf	(??_push_dtc_to_stack+0)+0,w
	bcf	status, 7	;select IRP bank0
	movwf	indf
	line	137
	
l3214:	
;main.c: 137: flag=1;
	clrf	(push_dtc_to_stack@flag)
	bsf	status,0
	rlf	(push_dtc_to_stack@flag),f
	line	138
;main.c: 138: }
	goto	l867
	line	139
	
l855:	
	
l3216:	
;main.c: 139: else if (flag == 1)
	movf	(push_dtc_to_stack@flag),w
	xorlw	01h
	skipz
	goto	u2411
	goto	u2410
u2411:
	goto	l3224
u2410:
	line	141
	
l3218:	
;main.c: 140: {
;main.c: 141: current_dtc_stack[dtc_counter] = dtc_num;
	movf	(push_dtc_to_stack@dtc_num),w
	movwf	(??_push_dtc_to_stack+0)+0
	movf	(push_dtc_to_stack@dtc_counter),w
	addlw	_current_dtc_stack&0ffh
	movwf	fsr0
	movf	(??_push_dtc_to_stack+0)+0,w
	bcf	status, 7	;select IRP bank0
	movwf	indf
	line	142
	
l3220:	
;main.c: 142: dtc_counter = (6) -1;
	movlw	(05h)
	movwf	(??_push_dtc_to_stack+0)+0
	movf	(??_push_dtc_to_stack+0)+0,w
	movwf	(push_dtc_to_stack@dtc_counter)
	line	143
	
l3222:	
;main.c: 143: flag=0;
	clrf	(push_dtc_to_stack@flag)
	line	144
;main.c: 144: }
	goto	l867
	line	146
	
l865:	
	line	148
	
l3224:	
;main.c: 146: else
;main.c: 147: {
;main.c: 148: current_dtc_stack[dtc_counter] = dtc_num;
	movf	(push_dtc_to_stack@dtc_num),w
	movwf	(??_push_dtc_to_stack+0)+0
	movf	(push_dtc_to_stack@dtc_counter),w
	addlw	_current_dtc_stack&0ffh
	movwf	fsr0
	movf	(??_push_dtc_to_stack+0)+0,w
	bcf	status, 7	;select IRP bank0
	movwf	indf
	line	149
	
l3226:	
;main.c: 149: dtc_counter++;
	movlw	(01h)
	movwf	(??_push_dtc_to_stack+0)+0
	movf	(??_push_dtc_to_stack+0)+0,w
	addwf	(push_dtc_to_stack@dtc_counter),f
	goto	l867
	line	151
	
l866:	
	goto	l867
	
l864:	
	goto	l867
	line	154
	
l854:	
	line	157
	
l867:	
	return
	opt stack 0
GLOBAL	__end_of_push_dtc_to_stack
	__end_of_push_dtc_to_stack:
;; =============== function _push_dtc_to_stack ends ============

	signat	_push_dtc_to_stack,4216
psect	text76,local,class=CODE,delta=2
global __ptext76
__ptext76:
	global	btemp
	btemp set 07Eh

	DABS	1,126,2	;btemp
	global	wtemp0
	wtemp0 set btemp
	end
