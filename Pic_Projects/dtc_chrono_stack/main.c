#include "htc.h"
#include "typedefs.h"
#define MAX_LENGTH (6)

/*PIC16F887*/
//__CONFIG(DEBUGEN & LVPEN & FCMEN & IESODIS & BORXSLP & DUNPROTECT & UNPROTECT & MCLREN & PWRTEN & WDTDIS & INTCLK);//HS);
//__CONFIG(BORV40);

/*ENUMS*/
enum
   {
    NO_DTC,                              /* implied 0*/
    OPEN,
    SHORT,
    LOSS_COMM,
    INVALID_CONFIG,                       /* implied 4*/
    PARTIAL_LOAD
    }dtc_names;

/* CONSTANT ARRAY */
const uint8_t dtc_array_list[] = 
                          {
                           NO_DTC,        /* implied 0*/
                           OPEN,         
                           SHORT,
                           LOSS_COMM,
                           INVALID_CONFIG, /* implied 4*/ 
                           PARTIAL_LOAD
                          };
/* This array will have the most current dtc*/
uint8_t current_dtc_stack[MAX_LENGTH];


/* Function prototypes*/
void main(void);
void push_dtc_to_stack(uint8_t dtc_num);      
 /*********************************************************************/
/*        THE MAIN LOOP                                              */         
/*********************************************************************/

void main(void)
{
   uint8_t reported_dtc;

while(1)
   {
     reported_dtc = OPEN;
     push_dtc_to_stack(reported_dtc);
     
     reported_dtc = SHORT;
     push_dtc_to_stack(reported_dtc);

     reported_dtc = INVALID_CONFIG;
     push_dtc_to_stack(reported_dtc);

     reported_dtc = PARTIAL_LOAD;
     push_dtc_to_stack(reported_dtc);
  
     reported_dtc = LOSS_COMM;
     push_dtc_to_stack(reported_dtc);
  
     asm("nop");
     reported_dtc = INVALID_CONFIG;
     push_dtc_to_stack(reported_dtc);

     //asm("nop");
     //reported_dtc = OPEN;
     //push_dtc_to_stack(reported_dtc);
    
   } 
}
/*******************************************************************************************************
* push_dtc_to_stack()
*  
* Description: This takes upto five dtc's and pushes them sequentially down current_dtc_stack[] array.
*              This will protect against non Dtcs, Once limit reached, new dtc's will be pushed to the top
*              of the stack and previous faults remain in sequential order ,oldest ones drop out.  
*
* Inputs -> 1 byte DTC number
* 
* Outputs -> None  
* Caveats: Not yet implemented repeated dtc protection.          
/*********************************************************************************************************/

void push_dtc_to_stack(uint8_t dtc_num)
{ 
/* local vars*/
uint8_t i;
uint8_t j;
uint8_t flag;
uint8_t temp[MAX_LENGTH];
static uint8_t dtc_counter;
 
/* Off the bat ckeck for non dtc numbers*/ 
  if(dtc_num >= MAX_LENGTH)
   {
       /* Maintian the stack as is dtc_counter is still at previous value*/
       asm("nop");       
   }
  /* okay keep going for an actual dtc number at this point*/
  else 
    {
        if(dtc_counter == MAX_LENGTH -1)
         {    
              dtc_counter=0;                                     /* Reset the array to the begining */ 
              /* This point here shift array contents down one e.g [0] -> [1]*/
                j=0;

                /* Check for duplicate DTC's antoher loop here not yet implemented */
                for(i=0; (i<MAX_LENGTH-1);i++)
                  {
                      if(current_dtc_stack[i] == dtc_num)
                       {
                          current_dtc_stack[i]=0;
                       }
                       else
                        {
                           /* Do nothing*/
                        }
                  } 
           
                for(i=0; (i<MAX_LENGTH-1);i++)
                   {
                      j++;
                      temp[j] = current_dtc_stack[i];             /*Dump to temp*/                                                                            
                   }
                   temp[MAX_LENGTH-1]=0;
                   j=1;
               
                 for(i=1; (i<MAX_LENGTH-1);i++)
                   {
                     current_dtc_stack[i] = temp[j];              /*Dump temp to current_dtc_*/                                                                            
                     j++;
                   }
                /* Done Shifting */ 
              current_dtc_stack[dtc_counter] = dtc_num; 
              flag=1;
           }
      else if (flag == 1)
            {
                  current_dtc_stack[dtc_counter] = dtc_num; 
                  dtc_counter = MAX_LENGTH -1;
                  flag=0;   
            } 
        
          else
           {   /* this is here to fill array initially when its zero */
               current_dtc_stack[dtc_counter] = dtc_num;          
               dtc_counter++; 
             
           }     
   
     
    }
 
     
}  


