opt subtitle "HI-TECH Software Omniscient Code Generator (Lite mode) build 10920"

opt pagewidth 120

	opt lm

	processor	16F887
clrc	macro
	bcf	3,0
	endm
clrz	macro
	bcf	3,2
	endm
setc	macro
	bsf	3,0
	endm
setz	macro
	bsf	3,2
	endm
skipc	macro
	btfss	3,0
	endm
skipz	macro
	btfss	3,2
	endm
skipnc	macro
	btfsc	3,0
	endm
skipnz	macro
	btfsc	3,2
	endm
indf	equ	0
indf0	equ	0
pc	equ	2
pcl	equ	2
status	equ	3
fsr	equ	4
fsr0	equ	4
c	equ	1
z	equ	0
pclath	equ	10
# 17 "C:\pic_projects\SLEEP PIC16F877\main.c"
	psect config,class=CONFIG,delta=2 ;#
# 17 "C:\pic_projects\SLEEP PIC16F877\main.c"
	dw 0x1FFF & 0x3FFF & 0x3FFF & 0x3BFF & 0x3EFF & 0x3FFF & 0x3FFF & 0x3FFF & 0x3FEF & 0x3FF7 & 0x3FFA ;#
# 18 "C:\pic_projects\SLEEP PIC16F877\main.c"
	psect config,class=CONFIG,delta=2 ;#
# 18 "C:\pic_projects\SLEEP PIC16F877\main.c"
	dw 0x3FFF ;#
	FNCALL	_main,_init
	FNCALL	_main,_func1
	FNROOT	_main
	global	func1@test_func1_var
	global	main@count
	global	_PORTD
psect	text88,local,class=CODE,delta=2
global __ptext88
__ptext88:
_PORTD	set	8
	global	_CARRY
_CARRY	set	24
	global	_GIE
_GIE	set	95
	global	_TRISD
_TRISD	set	136
	global	_EEADR
_EEADR	set	269
	global	_EEDATA
_EEDATA	set	268
	global	_EECON1
_EECON1	set	396
	global	_EECON2
_EECON2	set	397
	global	_RD
_RD	set	3168
	global	_WR
_WR	set	3169
	global	_WREN
_WREN	set	3170
	file	"Sleep 16F877.as"
	line	#
psect cinit,class=CODE,delta=2
global start_initialization
start_initialization:

psect	bssCOMMON,class=COMMON,space=1
global __pbssCOMMON
__pbssCOMMON:
func1@test_func1_var:
       ds      2

main@count:
       ds      2

; Clear objects allocated to COMMON
psect cinit,class=CODE,delta=2
	clrf	((__pbssCOMMON)+0)&07Fh
	clrf	((__pbssCOMMON)+1)&07Fh
	clrf	((__pbssCOMMON)+2)&07Fh
	clrf	((__pbssCOMMON)+3)&07Fh
psect cinit,class=CODE,delta=2
global end_of_initialization

;End of C runtime variable initialization code

end_of_initialization:
clrf status
ljmp _main	;jump to C main() function
psect	cstackCOMMON,class=COMMON,space=1
global __pcstackCOMMON
__pcstackCOMMON:
	global	?_init
?_init:	; 0 bytes @ 0x0
	global	??_init
??_init:	; 0 bytes @ 0x0
	global	?_func1
?_func1:	; 0 bytes @ 0x0
	global	??_func1
??_func1:	; 0 bytes @ 0x0
	global	?_main
?_main:	; 0 bytes @ 0x0
	ds	3
	global	func1@i
func1@i:	; 2 bytes @ 0x3
	ds	2
	global	??_main
??_main:	; 0 bytes @ 0x5
	ds	2
;;Data sizes: Strings 0, constant 0, data 0, bss 4, persistent 0 stack 0
;;Auto spaces:   Size  Autos    Used
;; COMMON          13      7      11
;; BANK0           80      0       0
;; BANK1           80      0       0
;; BANK3           85      0       0
;; BANK2           96      0       0

;;
;; Pointer list with targets:



;;
;; Critical Paths under _main in COMMON
;;
;;   _main->_func1
;;
;; Critical Paths under _main in BANK0
;;
;;   None.
;;
;; Critical Paths under _main in BANK1
;;
;;   None.
;;
;; Critical Paths under _main in BANK3
;;
;;   None.
;;
;; Critical Paths under _main in BANK2
;;
;;   None.

;;
;;Main: autosize = 0, tempsize = 2, incstack = 0, save=0
;;

;;
;;Call Graph Tables:
;;
;; ---------------------------------------------------------------------------------
;; (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;; ---------------------------------------------------------------------------------
;; (0) _main                                                 2     2      0      60
;;                                              5 COMMON     2     2      0
;;                               _init
;;                              _func1
;; ---------------------------------------------------------------------------------
;; (1) _func1                                                5     5      0      60
;;                                              0 COMMON     5     5      0
;; ---------------------------------------------------------------------------------
;; (1) _init                                                 0     0      0       0
;; ---------------------------------------------------------------------------------
;; Estimated maximum stack depth 1
;; ---------------------------------------------------------------------------------

;; Call Graph Graphs:

;; _main (ROOT)
;;   _init
;;   _func1
;;

;; Address spaces:

;;Name               Size   Autos  Total    Cost      Usage
;;BITCOMMON            D      0       0       0        0.0%
;;EEDATA             100      0       0       0        0.0%
;;NULL                 0      0       0       0        0.0%
;;CODE                 0      0       0       0        0.0%
;;COMMON               D      7       B       1       84.6%
;;BITSFR0              0      0       0       1        0.0%
;;SFR0                 0      0       0       1        0.0%
;;BITSFR1              0      0       0       2        0.0%
;;SFR1                 0      0       0       2        0.0%
;;STACK                0      0       1       2        0.0%
;;ABS                  0      0       B       3        0.0%
;;BITBANK0            50      0       0       4        0.0%
;;BITSFR3              0      0       0       4        0.0%
;;SFR3                 0      0       0       4        0.0%
;;BANK0               50      0       0       5        0.0%
;;BITSFR2              0      0       0       5        0.0%
;;SFR2                 0      0       0       5        0.0%
;;BITBANK1            50      0       0       6        0.0%
;;BANK1               50      0       0       7        0.0%
;;BITBANK3            55      0       0       8        0.0%
;;BANK3               55      0       0       9        0.0%
;;BITBANK2            60      0       0      10        0.0%
;;BANK2               60      0       0      11        0.0%
;;DATA                 0      0       C      12        0.0%

	global	_main
psect	maintext,global,class=CODE,delta=2
global __pmaintext
__pmaintext:

;; *************** function _main *****************
;; Defined at:
;;		line 33 in file "C:\pic_projects\SLEEP PIC16F877\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0, btemp+1, pclath, cstack
;; Tracked objects:
;;		On entry : 17F/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          2       0       0       0       0
;;      Totals:         2       0       0       0       0
;;Total ram usage:        2 bytes
;; Hardware stack levels required when called:    1
;; This function calls:
;;		_init
;;		_func1
;; This function is called by:
;;		Startup code after reset
;; This function uses a non-reentrant model
;;
psect	maintext
	file	"C:\pic_projects\SLEEP PIC16F877\main.c"
	line	33
	global	__size_of_main
	__size_of_main	equ	__end_of_main-_main
	
_main:	
	opt	stack 7
; Regs used in _main: [wreg+status,2+status,0+btemp+1+pclath+cstack]
	line	35
	
l3966:	
;main.c: 34: static int count;
;main.c: 35: init();
	fcall	_init
	line	36
	
l3968:	
;main.c: 36: count=0;
	clrf	(main@count)
	clrf	(main@count+1)
	goto	l3970
	line	37
;main.c: 37: while (1)
	
l843:	
	line	39
	
l3970:	
;main.c: 38: {
;main.c: 39: if(count<10)
	movf	(main@count+1),w
	xorlw	80h
	movwf	btemp+1
	movlw	(high(0Ah))^80h
	subwf	btemp+1,w
	skipz
	goto	u2325
	movlw	low(0Ah)
	subwf	(main@count),w
u2325:

	skipnc
	goto	u2321
	goto	u2320
u2321:
	goto	l3978
u2320:
	line	41
	
l3972:	
;main.c: 40: {
;main.c: 41: PORTD ^= 0xFF;
	movlw	(0FFh)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	xorwf	(8),f	;volatile
	line	42
	
l3974:	
;main.c: 42: _delay(100000);
	opt asmopt_off
movlw	130
movwf	((??_main+0)+0+1),f
	movlw	221
movwf	((??_main+0)+0),f
u2337:
	decfsz	((??_main+0)+0),f
	goto	u2337
	decfsz	((??_main+0)+0+1),f
	goto	u2337
	nop2
opt asmopt_on

	line	43
	
l3976:	
;main.c: 43: count++;
	movlw	low(01h)
	addwf	(main@count),f
	skipnc
	incf	(main@count+1),f
	movlw	high(01h)
	addwf	(main@count+1),f
	line	44
;main.c: 44: }
	goto	l3970
	line	45
	
l844:	
	line	47
	
l3978:	
;main.c: 45: else
;main.c: 46: {
;main.c: 47: func1();
	fcall	_func1
	line	48
	
l3980:	
;main.c: 48: _delay(10000);
	opt asmopt_off
movlw	13
movwf	((??_main+0)+0+1),f
	movlw	251
movwf	((??_main+0)+0),f
u2347:
	decfsz	((??_main+0)+0),f
	goto	u2347
	decfsz	((??_main+0)+0+1),f
	goto	u2347
	nop2
opt asmopt_on

	goto	l3970
	line	49
	
l845:	
	goto	l3970
	line	50
	
l846:	
	line	37
	goto	l3970
	
l847:	
	line	51
	
l848:	
	global	start
	ljmp	start
	opt stack 0
GLOBAL	__end_of_main
	__end_of_main:
;; =============== function _main ends ============

	signat	_main,88
	global	_func1
psect	text89,local,class=CODE,delta=2
global __ptext89
__ptext89:

;; *************** function _func1 *****************
;; Defined at:
;;		line 11 in file "C:\pic_projects\SLEEP PIC16F877\func1.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  i               2    3[COMMON] int 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, btemp+1
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         2       0       0       0       0
;;      Temps:          3       0       0       0       0
;;      Totals:         5       0       0       0       0
;;Total ram usage:        5 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text89
	file	"C:\pic_projects\SLEEP PIC16F877\func1.c"
	line	11
	global	__size_of_func1
	__size_of_func1	equ	__end_of_func1-_func1
	
_func1:	
	opt	stack 7
; Regs used in _func1: [wreg+status,2+btemp+1]
	line	14
	
l3916:	
;func1.c: 12: static int test_func1_var;
;func1.c: 13: int i;
;func1.c: 14: test_func1_var++;
	movlw	low(01h)
	addwf	(func1@test_func1_var),f
	skipnc
	incf	(func1@test_func1_var+1),f
	movlw	high(01h)
	addwf	(func1@test_func1_var+1),f
	line	17
	
l3918:	
;func1.c: 17: if((test_func1_var >=127) && (test_func1_var <=160))
	movf	(func1@test_func1_var+1),w
	xorlw	80h
	movwf	btemp+1
	movlw	(high(07Fh))^80h
	subwf	btemp+1,w
	skipz
	goto	u2225
	movlw	low(07Fh)
	subwf	(func1@test_func1_var),w
u2225:

	skipc
	goto	u2221
	goto	u2220
u2221:
	goto	l3936
u2220:
	
l3920:	
	movf	(func1@test_func1_var+1),w
	xorlw	80h
	movwf	btemp+1
	movlw	(high(0A1h))^80h
	subwf	btemp+1,w
	skipz
	goto	u2235
	movlw	low(0A1h)
	subwf	(func1@test_func1_var),w
u2235:

	skipnc
	goto	u2231
	goto	u2230
u2231:
	goto	l3936
u2230:
	line	19
	
l3922:	
;func1.c: 18: {
;func1.c: 19: PORTD = 0xAA;
	movlw	(0AAh)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(8)	;volatile
	line	20
;func1.c: 20: _delay(100000);
	opt asmopt_off
movlw	130
movwf	((??_func1+0)+0+1),f
	movlw	221
movwf	((??_func1+0)+0),f
u2357:
	decfsz	((??_func1+0)+0),f
	goto	u2357
	decfsz	((??_func1+0)+0+1),f
	goto	u2357
	nop2
opt asmopt_on

	line	21
;func1.c: 21: PORTD = 0x55;
	movlw	(055h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(8)	;volatile
	line	22
;func1.c: 22: _delay(100000);
	opt asmopt_off
movlw	130
movwf	((??_func1+0)+0+1),f
	movlw	221
movwf	((??_func1+0)+0),f
u2367:
	decfsz	((??_func1+0)+0),f
	goto	u2367
	decfsz	((??_func1+0)+0+1),f
	goto	u2367
	nop2
opt asmopt_on

	line	23
;func1.c: 23: if(test_func1_var == 140)
	movlw	08Ch
	xorwf	(func1@test_func1_var),w
	iorwf	(func1@test_func1_var+1),w
	skipz
	goto	u2241
	goto	u2240
u2241:
	goto	l1695
u2240:
	goto	l3924
	line	25
;func1.c: 24: {
;func1.c: 25: do
	
l1692:	
	line	27
	
l3924:	
;func1.c: 26: {
;func1.c: 27: PORTD &= 0x00;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(8)	;volatile
	line	28
	
l3926:	
;func1.c: 28: _delay(100000);
	opt asmopt_off
movlw	130
movwf	((??_func1+0)+0+1),f
	movlw	221
movwf	((??_func1+0)+0),f
u2377:
	decfsz	((??_func1+0)+0),f
	goto	u2377
	decfsz	((??_func1+0)+0+1),f
	goto	u2377
	nop2
opt asmopt_on

	line	29
	
l3928:	
;func1.c: 29: PORTD = 0xFF;
	movlw	(0FFh)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(8)	;volatile
	line	30
	
l3930:	
;func1.c: 30: test_func1_var++;
	movlw	low(01h)
	addwf	(func1@test_func1_var),f
	skipnc
	incf	(func1@test_func1_var+1),f
	movlw	high(01h)
	addwf	(func1@test_func1_var+1),f
	line	31
	
l3932:	
;func1.c: 31: _delay(100000);
	opt asmopt_off
movlw	130
movwf	((??_func1+0)+0+1),f
	movlw	221
movwf	((??_func1+0)+0),f
u2387:
	decfsz	((??_func1+0)+0),f
	goto	u2387
	decfsz	((??_func1+0)+0+1),f
	goto	u2387
	nop2
opt asmopt_on

	line	33
	
l3934:	
;func1.c: 32: }
;func1.c: 33: while(test_func1_var <= 145);
	movf	(func1@test_func1_var+1),w
	xorlw	80h
	movwf	btemp+1
	movlw	(high(092h))^80h
	subwf	btemp+1,w
	skipz
	goto	u2255
	movlw	low(092h)
	subwf	(func1@test_func1_var),w
u2255:

	skipc
	goto	u2251
	goto	u2250
u2251:
	goto	l3924
u2250:
	goto	l1695
	
l1693:	
	line	34
;func1.c: 34: }
	goto	l1695
	line	35
	
l1691:	
	goto	l1695
	line	38
;func1.c: 35: else
;func1.c: 36: {
	
l1694:	
	line	40
;func1.c: 38: }
;func1.c: 40: }
	goto	l1695
	line	41
	
l1690:	
	
l3936:	
;func1.c: 41: else if (test_func1_var >=161)
	movf	(func1@test_func1_var+1),w
	xorlw	80h
	movwf	btemp+1
	movlw	(high(0A1h))^80h
	subwf	btemp+1,w
	skipz
	goto	u2265
	movlw	low(0A1h)
	subwf	(func1@test_func1_var),w
u2265:

	skipc
	goto	u2261
	goto	u2260
u2261:
	goto	l1695
u2260:
	line	43
	
l3938:	
;func1.c: 42: {
;func1.c: 43: if(test_func1_var == 161)
	movlw	0A1h
	xorwf	(func1@test_func1_var),w
	iorwf	(func1@test_func1_var+1),w
	skipz
	goto	u2271
	goto	u2270
u2271:
	goto	l3942
u2270:
	line	45
	
l3940:	
;func1.c: 44: {
;func1.c: 45: PORTD = 0x01;
	movlw	(01h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(8)	;volatile
	line	46
;func1.c: 46: }
	goto	l3942
	line	47
	
l1697:	
	goto	l3942
	line	50
;func1.c: 47: else
;func1.c: 48: {
	
l1698:	
	line	54
	
l3942:	
;func1.c: 50: }
;func1.c: 54: for(i=0;i<7;i++)
	clrf	(func1@i)
	clrf	(func1@i+1)
	
l3944:	
	movf	(func1@i+1),w
	xorlw	80h
	movwf	btemp+1
	movlw	(high(07h))^80h
	subwf	btemp+1,w
	skipz
	goto	u2285
	movlw	low(07h)
	subwf	(func1@i),w
u2285:

	skipc
	goto	u2281
	goto	u2280
u2281:
	goto	l3948
u2280:
	goto	l3952
	
l3946:	
	goto	l3952
	line	55
	
l1699:	
	line	56
	
l3948:	
;func1.c: 55: {
;func1.c: 56: PORTD <<= 1;
	clrc
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	rlf	(8),f	;volatile

	line	57
;func1.c: 57: _delay(800000);
	opt asmopt_off
movlw  5
movwf	((??_func1+0)+0+2),f
movlw	15
movwf	((??_func1+0)+0+1),f
	movlw	244
movwf	((??_func1+0)+0),f
u2397:
	decfsz	((??_func1+0)+0),f
	goto	u2397
	decfsz	((??_func1+0)+0+1),f
	goto	u2397
	decfsz	((??_func1+0)+0+2),f
	goto	u2397
opt asmopt_on

	line	54
	movlw	low(01h)
	addwf	(func1@i),f
	skipnc
	incf	(func1@i+1),f
	movlw	high(01h)
	addwf	(func1@i+1),f
	
l3950:	
	movf	(func1@i+1),w
	xorlw	80h
	movwf	btemp+1
	movlw	(high(07h))^80h
	subwf	btemp+1,w
	skipz
	goto	u2295
	movlw	low(07h)
	subwf	(func1@i),w
u2295:

	skipc
	goto	u2291
	goto	u2290
u2291:
	goto	l3948
u2290:
	goto	l3952
	
l1700:	
	line	59
	
l3952:	
;func1.c: 58: }
;func1.c: 59: PORTD = 0x80;
	movlw	(080h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(8)	;volatile
	line	60
	
l3954:	
;func1.c: 60: for(i=0;i<7;i++)
	clrf	(func1@i)
	clrf	(func1@i+1)
	
l3956:	
	movf	(func1@i+1),w
	xorlw	80h
	movwf	btemp+1
	movlw	(high(07h))^80h
	subwf	btemp+1,w
	skipz
	goto	u2305
	movlw	low(07h)
	subwf	(func1@i),w
u2305:

	skipc
	goto	u2301
	goto	u2300
u2301:
	goto	l3960
u2300:
	goto	l3964
	
l3958:	
	goto	l3964
	line	61
	
l1701:	
	line	62
	
l3960:	
;func1.c: 61: {
;func1.c: 62: PORTD >>= 1;
	clrc
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	rrf	(8),f	;volatile

	line	63
;func1.c: 63: _delay(800000);
	opt asmopt_off
movlw  5
movwf	((??_func1+0)+0+2),f
movlw	15
movwf	((??_func1+0)+0+1),f
	movlw	244
movwf	((??_func1+0)+0),f
u2407:
	decfsz	((??_func1+0)+0),f
	goto	u2407
	decfsz	((??_func1+0)+0+1),f
	goto	u2407
	decfsz	((??_func1+0)+0+2),f
	goto	u2407
opt asmopt_on

	line	60
	movlw	low(01h)
	addwf	(func1@i),f
	skipnc
	incf	(func1@i+1),f
	movlw	high(01h)
	addwf	(func1@i+1),f
	
l3962:	
	movf	(func1@i+1),w
	xorlw	80h
	movwf	btemp+1
	movlw	(high(07h))^80h
	subwf	btemp+1,w
	skipz
	goto	u2315
	movlw	low(07h)
	subwf	(func1@i),w
u2315:

	skipc
	goto	u2311
	goto	u2310
u2311:
	goto	l3960
u2310:
	goto	l3964
	
l1702:	
	line	65
	
l3964:	
;func1.c: 64: }
;func1.c: 65: PORTD = 0x01;
	movlw	(01h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(8)	;volatile
	goto	l1695
	line	67
	
l1696:	
	line	68
	
l1695:	
;func1.c: 67: }
;func1.c: 68: PORTD = test_func1_var;
	movf	(func1@test_func1_var),w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(8)	;volatile
	line	69
	
l1703:	
	return
	opt stack 0
GLOBAL	__end_of_func1
	__end_of_func1:
;; =============== function _func1 ends ============

	signat	_func1,88
	global	_init
psect	text90,local,class=CODE,delta=2
global __ptext90
__ptext90:

;; *************** function _init *****************
;; Defined at:
;;		line 55 in file "C:\pic_projects\SLEEP PIC16F877\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		status,2
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text90
	file	"C:\pic_projects\SLEEP PIC16F877\main.c"
	line	55
	global	__size_of_init
	__size_of_init	equ	__end_of_init-_init
	
_init:	
	opt	stack 7
; Regs used in _init: [status,2]
	line	56
	
l3056:	
;main.c: 56: TRISD = 0x00;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	clrf	(136)^080h	;volatile
	line	57
;main.c: 57: PORTD = 0x00;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(8)	;volatile
	line	60
	
l851:	
	return
	opt stack 0
GLOBAL	__end_of_init
	__end_of_init:
;; =============== function _init ends ============

	signat	_init,88
psect	text91,local,class=CODE,delta=2
global __ptext91
__ptext91:
	global	btemp
	btemp set 07Eh

	DABS	1,126,2	;btemp
	global	wtemp0
	wtemp0 set btemp
	end
