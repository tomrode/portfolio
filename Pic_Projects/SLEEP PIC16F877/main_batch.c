/**********************************************************/
/* Simple program to test if I can caomplie using a batch */
/* file                                                   */
/* 8 AUG 2012 using PIC16F887                             */ 
/*                                                        */
/**********************************************************/
#include <htc.h>
/**********************************************************/
/* Device Configuration
/**********************************************************/

          /*PIC16F887*/
__CONFIG(DEBUGEN & LVPEN & FCMEN & IESODIS & BORXSLP & DUNPROTECT & UNPROTECT & MCLREN & PWRTEN & WDTDIS & HS);
__CONFIG(BORV40);  //HS means resonator 8Mhz    
       
 
/**********************************************************/
/* Initialize SFR 
/**********************************************************/


/***********************************************************/
/* MAIN
/***********************************************************/
void init(void);
int main(void)
{

init();         /* Turn on TRISD*/
while (1)
  {
   PORTD ^= 0xFF; 
  _delay(2000000);         /*Amount of instruction cycles*/
  }
}

void init(void)
{
TRISD = 0x00;
}