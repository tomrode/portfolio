/**********************************************************/
/* Sample program to test sleep current on my meter       */
/* 3 NOV 2011 using PIC16F877A                            */ 
/*                                                        */
/**********************************************************/
#include <htc.h>
#include "func1.h"
/**********************************************************/
/* Device Configuration
/**********************************************************/

      /* PIC16F877A*/
    // __CONFIG (WDTDIS & PWRTDIS & BORDIS & LVPEN & WRTEN &
          //DEBUGDIS & DUNPROT & UNPROTECT & HS);
      
                /*PIC16F887*/
__CONFIG(DEBUGEN & LVPEN & FCMEN & IESODIS & BORXSLP & DUNPROTECT & UNPROTECT & MCLREN & PWRTEN & WDTDIS & HS);
__CONFIG(BORV40);  //HS means resonator 8Mhz   
 
/**********************************************************/
/* Initialize SFR 
/**********************************************************/


/***********************************************************/
/* MAIN
/***********************************************************/
void init(void);
//void func1(void);
void main(void);

void main(void)
{
static int count;
init();         /* Turn on TRISD*/
count=0;
while (1)
  {
   if(count<10)
    {     
       PORTD ^= 0xFF;
      _delay(100000);         /*Amount of instruction cycles*/
       count++;
    }
   else
     {
        func1();
        _delay(10000);         /*Amount of instruction cycles*/
     }   
  }
}


void init(void)
{
TRISD = 0x00;
PORTD = 0x00;
//TRISB = 0x00;     /* When I remove the following two lines it acts up */
//PORTB = 0x00;
}

//void func1(void)
//{
//   static int test_func1_var;

//   test_func1_var++;
//   PORTD = test_func1_var;
//}
