/**********************************************************/
/* Simple program to test if I can caomplie using a batch */
/* file                                                   */
/* 8 AUG 2012 using PIC16F887                             */ 
/*                                                        */
/**********************************************************/
#include <htc.h>
#include "func1.h"

void func1(void)
{
   static int test_func1_var;
   int i;
   test_func1_var++;
  

   if((test_func1_var >=127) && (test_func1_var <=160))
    {
        PORTD = 0xAA;
        _delay(100000);                     /*Amount of instruction cycles*/ 
        PORTD = 0x55; 
        _delay(100000);                     /*Amount of instruction cycles*/
        if(test_func1_var == 140)
          {  
             do
              {
                 PORTD &= 0x00;             /*shut off everything*/
                 _delay(100000);            /*Amount of instruction cycles*/ 
                 PORTD = 0xFF;
                 test_func1_var++;
                 _delay(100000);            /*Amount of instruction cycles*/ 
              }
              while(test_func1_var <= 145); /* do this five times*/  
          }
         else
           {
            /* Do nothing */
           }
 
   }
   else if (test_func1_var >=161)
    {
     if(test_func1_var == 161)
      {  
         PORTD = 0x01;
      }
      else
       {
          /* Do nothing*/
       }  
      
  // while(1)                 /* Stuck here forever*/
   //  {
      for(i=0;i<7;i++)
          { 
             PORTD <<= 1;  // (PORTD << 1); 
             _delay(800000);            /*Amount of instruction cycles*/ 
          }
        PORTD = 0x80;
       for(i=0;i<7;i++)
          { 
             PORTD >>= 1;   //(PORTD >> 1); 
             _delay(800000);            /*Amount of instruction cycles*/ 
          } 
        PORTD = 0x01; 
     //  }
    }
      PORTD = test_func1_var;
}
