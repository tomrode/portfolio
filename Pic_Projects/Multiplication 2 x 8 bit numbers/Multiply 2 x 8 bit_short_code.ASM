                   list      p=16f887
                   #include  <p16f887.inc>

;******************************************************************************
; Device Configuration
;******************************************************************************

                   __CONFIG  _CONFIG1, _LVP_OFF & _FCMEN_ON & _IESO_OFF & _BOR_OFF & _CPD_OFF & _CP_OFF & _MCLRE_ON & _PWRTE_ON & _WDT_OFF & _INTRC_OSC_NOCLKOUT
                   __CONFIG  _CONFIG2, _WRT_OFF & _BOR21V

;******************************************************************************
; Variable Definitions
;******************************************************************************
mask               equ       0x77 
x_hi               equ       0x78
temp_hi            equ       0x79                          ; Multiplication variables  
temp_lo            equ       0x7A  
z_hi               equ       0x7B
z_lo               equ       0x7C

pclath_temp        equ       0x7D                           ; For isr's if needed
status_temp        equ       0x7E
w_temp             equ       0x7F

;******************************************************************************
; Reset Vector
;******************************************************************************

                   org       0x000

                   goto      main

;******************************************************************************
; Interrupt Vector
;******************************************************************************

                   org       0x004

                  
                   retfie                                  ; Return from interrupt.

;******************************************************************************
; Main Program
;******************************************************************************

main
                   movlw     0x61                          ; Select internal 4 MHz clock.
                   banksel   OSCCON
                   movwf     OSCCON
wait_stable
                   btfss     OSCCON,HTS                    ; Is the high-speed internal oscillator stable?
                 ;  goto      wait_stable                   ; If not, wait until it is.

                   banksel   OPTION_REG
                   movf      OPTION_REG,W                  ; Load the current value of the OPTION register.
                   andlw      0x17
               
                   iorlw     0x07                          ; Set PS2, PS1, and PS0.
                   movwf     OPTION_REG                    ; Update the OPTION register.
                          
                 

;******************************************************************************
; Main Loop         INITIALIZE, MULTIPLIER, MULTIPLICAND, Z_HI & Z_LO 
;                   RESULTS HERE.
;******************************************************************************

main_loop        
                 movlw        0xFD      ;Multiplicand                 
                 banksel      temp_hi   
                 movwf        temp_hi 
                                   
    
                 movlw        0x01    ; Multiplier                   
                 banksel      temp_lo
                 movwf        temp_lo

                 movlw        0x00     ; clear the lo result out initially
                 banksel      z_lo
                 movwf        z_lo
                  
                 banksel      z_hi     ; clear the hi result out initially
                 movwf        z_hi
                          

                 movlw        0x01      ;Load the mask  value prepare for first test of temp_lo bit 0 
                 banksel      mask  
                 movwf        mask 

                 banksel      x_hi
                 clrf         x_hi       ; No need for the bit set

                 
             
;****************************************************************************************************                 
;                 TEST AREA FOR MULTIPLIER
;****************************************************************************************************                 
;                 BEGIN THE MULTIPLIER BIT TESTING
;****************************************************************************************************      
;                 ALWAY A MULTIPLY BY ONE
;****************************************************************************************************                                
;                 
;**************************************************************************************************                 
;                Begin the ANDING variable "mask" with each bit position <7-0> of multiplier variable "temp_lo"
;***************************************************************************************************
   
z_lo_loop
              
                  
                 banksel     temp_lo             ; somehow I need to bit test z_lo and shift left 8 bits worth
                 movf        temp_lo,W
                 
                 banksel     mask 
                 andwf       mask,w             ; Test the current mask vale to determine if a rotae happens

                 banksel     STATUS             ; Test the mask if one then mask picked up a bit in multiplier 
                 btfss       STATUS,Z           ; Z=0 (Result not zero) Z=1 (Result is zero) after logic operation 
                 goto        adding_lo 
shifts                
                 banksel     STATUS
                 bcf         STATUS,C  
               
                 banksel     mask               ; If Z did result a zero  
                 rlf         mask               ; Increment the mask and try this again
                 
                 banksel     STATUS
                 btfsc       STATUS,C           ;This is to check if mask rotted all the way through temp_lo  
                 goto        all_end
                      
                 banksel     STATUS
                 bcf         STATUS,C           ; clear the carry out       
      
                 
                 banksel     temp_hi 
                 rlf         temp_hi,f          ;Multiplicand Shifted to the left by one
                 
                 banksel     x_hi               ;This section for shifting the whole 16 bit Multiplicand 
                 rlf         x_hi                   

                ; banksel     STATUS 
                ; btfsc       STATUS,C
                ; goto        shift_x_hi        
                
                 ;banksel      STATUS             ; This is the last mask carryout bit test 
                ; btfsc        STATUS,C
                  
               ;  goto        all_end 
                

                 goto         z_lo_loop                
  
               
;*******************************************************************************************************
;                   DONE HERE AFTER BIT TEST
;*******************************************************************************************************
adding_lo          
                  banksel     temp_hi
                  movf        temp_hi,W
                 
                  banksel     z_lo
                  addwf       z_lo,f           ; result here of low byte
                              
adding_hi              
                  banksel     x_hi
                  movf        x_hi,W
      
                  banksel     STATUS
                  btfsc       STATUS,C         ; This will add in a carry if need be
                  addlw       0x01 
 
                  banksel     z_hi
                  addwf       z_hi,f     
                  
                  goto        z_lo_loop
                  ;goto        shifts




;********************************************************************************************************
;                     USED for shifting the high part of multiplicand
;********************************************************************************************************
;shift_x_hi  
 ;        
  ;                banksel     STATUS
   ;               bcf         STATUS,C           ; clear the carry out     
    ;               
     ;             banksel     x_hi               ;This section for shifting the whole 16 bit Multiplicand 
      ;            rlf         x_hi  
;
 ;                 banksel     temp_lo
  ;                movf        temp_lo,w          
;
 ;                 banksel     mask 
  ;                andwf       mask,w             ; Test the current mask vale to determine if a rotae happens
;
 ;                 banksel     STATUS             ; Test the mask if one then mask picked up a bit in multiplier 
  ;                btfss       STATUS,Z           ; Z=0 (Result not zero) Z=1 (Result is zero) after logic operation 
   ;               goto        adding_hi     
  ;
   ;                    
    ;              goto        z_lo_loop



all_end
                  goto        all_end    
;*******************************************************************************
; isr()
;
; Description:    This is the interrupt service routine (ISR) does nothing yet
;                 
; 
; Inputs:                         
;  
; Outputs:             
;*********************************************************************************
 ;isr
                                   
                                   
                  ;return                                  ; Return to the main interrupt handler.


;******************************************************************************
; End of program
;******************************************************************************

                   end
