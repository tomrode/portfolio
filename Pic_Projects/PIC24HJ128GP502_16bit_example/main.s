;
;	File: Example2.asm
;	MPASM assembler/MPLINK linker example - file 2
;	Use with Example.asm 
;	17 May 2004
;	Updated: 26 Aug 2009
;       Used 21 MAR 2012

;	list	 p=p24HJ128GP502
	#include p24HJ128GP502.inc
    #define NOTHING (22)
	;extern main, service    ; These routines are in Example.asm
    config __FOSC, CSW_FSCM_OFF & XT_PLL16 
    ORG     0x000       						

	goto main				; Jump to main code defined in Example.asm
	

main
   
    nop						; Pad out so interrupt
	nop						;  service routine gets
	nop						;    put at address 0x0004.
	;goto service			; Points to interrupt service routine
    
	end
