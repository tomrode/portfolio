#include <p32xxxx.h>			// Required for SFR defs
#include "GenericTypeDefs.h"
#define FROM_DATA_SHEET
/********************************************************************************************************
/*
/*        MY FIRST EXPOSURE TO THE MPLAB C32 COMPILIER 
/*
/********************************************************************************************************/

/* TYPE DEFS*/
 //NONE

/* Function prototypes*/


#ifdef FROM_DATA_SHEET
void main(void);
//void __ISR(TIMER_1_VECTOR, IPL3) T1_Interrupt_ISR(void);

#endif

#ifdef FROM_DATA_SHEET
void main(void)
{
 
   AD1PCFG = 0xFFFF;     // Initialize AN pins as digital need this to enable portb
   TRISB   = 0;
   LATB    = 0x00000003;
   PORTB   = LATB;
   T1CON   = 0x0;        // Stop the Timer and Reset Control register
                         // Set prescaler at 1:1, internal clock source

   TMR1 = 0x0;            // Clear timer register
   PR1 = 0xFFFF;          // Load period register
   IPC1SET = 0x000C;      // Set priority level=3
   IPC1SET = 0x0001;      // Set subpriority level=1
                          // Could have also done this in single
                          // operation by assigning IPC1SET = 0x000D
   IFS0CLR = 0x0010;      // Clear Timer interrupt status flag
   IEC0SET = 0x0010;      // Enable Timer interrupts
   T1CONSET = 0x8000;     // Start Timer
 while(1)
   {
    /*Do NOTHING FOREVER*/
   } 
  
}


void __ISR(TIMER_1_VECTOR, IPL3) T1_Interrupt_ISR(void)
{
//... perform application specific operations in response to the interrupt
  IFS0CLR = 0x0010; // Be sure to clear the Timer 1 interrupt status
}

#endif                    // FROM_DATA_SHEET








#ifdef FROM_WEB_EXAMPLE
void main (void);
void Timer1_interrupt(void);
#endif
/*********************************************************************/
/*        THE MAIN LOOP   FROM EXAMPLE                               */         
/*********************************************************************/
#ifdef FROM_WEB_EXAMPLE 
void main()
 {
  AD1PCFG = 0xFFFF;         // Initialize AN pins as digital
  TRISB  = 0;               // initialize PORTB as output
  LATB   = 0xAAAA;          // Initialize PORTB value
  
  TMR1 = 0;                 // reset timer value to zero
  PR1 = 65535;              // Load period register

  T1IP0_bit = 1;            // set interrupt
  T1IP1_bit = 1;            // priority
  T1IP2_bit = 1;            // to 7

  TCKPS0_bit = 1;           // Set Timer Input Clock
  TCKPS1_bit = 1;           // Prescale value to 1:256

  EnableInterrupts();       // Enable all interrupts

  T1IE_bit = 1;             // Enable Timer1 Interrupt
  ON__T1CON_bit = 1;        // Enable Timer1
}

void Timer1_interrupt() iv IVT_TIMER_1 ilevel 7 ics ICS_SRS 
{
  T1IF_bit = 0;             // Clear T1IF
  LATB = ~ PORTB;           // Invert PORTB
}
#endif