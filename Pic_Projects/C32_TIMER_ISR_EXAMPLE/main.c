
#include <p32xxxx.h>			// Required for SFR defs
//#include <plib.h>      //interrupt support library 

#include "GenericTypeDefs.h"
#include "TimerFunctions.h"
#include "attribs.h"
#define FROM_TIMER_FUNCTIONS_C
//#define FROM_DATA_SHEET_PAGE_338
//#define FROM_WEB_EXAMPLE
/********************************************************************************************************
/*
/*        MY FIRST EXPOSURE TO THE MPLAB C32 COMPILIER 
/*        DATA SHEET PIC32MX FAMILY
/*        DATE 11/2/2012
/*        Author TR
/********************************************************************************************************/

/* TYPE DEFS*/
 //NONE


#ifdef FROM_TIMER_FUNCTIONS_C
/*Constants*/
//#define TIMER1_PERIOD 	0x1F40	/* For a 0.1 millisecond interval	*/
#define ONE_SECOND_MSEC	0x2710	/* For a 1 second timeout in terms 	*/
/* Function prototypes*/
int main(void);
void Timer1Init(void);
BOOL IsOneSecondUp(void);
/*ifdef includes*/
//#include "TimerFunctions.h"

static BOOL oneSecondUp;
static UINT32 msecCounter;



// Configuration Bit settings
// SYSCLK = 80 MHz (8MHz Crystal/ FPLLIDIV * FPLLMUL / FPLLODIV)
// PBCLK = 40 MHz
// Primary Osc w/PLL (XT+,HS+,EC+PLL)
// WDT OFF
// Other options are don't care
//
#pragma config FPLLMUL = MUL_20, FPLLIDIV = DIV_2, FPLLODIV = DIV_1, FWDTEN = OFF
#pragma config POSCMOD = XT/*HS*/, FNOSC = PRIPLL, FPBDIV = DIV_2


int main(void)
 {
 _nop();
  /* QUICK TEST TO SEE IF INTERRUPT WILL TOGGLE PORTB*/
  AD1PCFG = 0xFFFF;         // Initialize AN pins as digital
  TRISB  = 0;               // initialize PORTB as output
  LATB   = 0x0001;          // Initialize PORTB value
 
  Timer1Init();           /* Initialize the timer as per instruction from Micro Chip*/
   while(1)
   {
     if(IFS0 = 0x00000010)  /* poll the TIMER1 interrupt flag*/ 
       {
       // _nop(); 
       //  IFS0 ^= 0x00000010; /* Clear the flag int flag or use IFS0CLR = 0x10;	is fine to.  */   
         
       }
     IsOneSecondUp();  
    /* Do Nothing*/
   }
   
 }
void Timer1Init(void)
{
	/* This function will intialize the Timer 1
	 * for basic timer operation. It will enable
	 * timer interrupt. The one second flag is
	 * initialized and the millisecond counter is 
	 * initialized. */

	T1CON = 0x0;			/* Basic Timer Operation				*/
	PR1 = TIMER1_PERIOD;	/* Timer period in TimerFunctions.h 	*/

	IFS0CLR = 0x10;			/* Clear interrupt flag and enable		*/
	IEC0SET = 0x10;			/* timer 1 interrupt. 					*/
	IPC1bits.T1IP = 4;		/* Timer 1 priority is 4				*/

	oneSecondUp = FALSE;	/* Intialize the one second flag		*/
	msecCounter = 0;		/* and the millisecond counter.			*/
 
    // enable multi-vector interrupts
    INTEnableSystemMultiVectoredInt();
	T1CONSET = 0x8000;		/* Start the timer.						*/

}

BOOL IsOneSecondUp(void)
{
	/* This function will return TRUE if
	 * a second has expired since the last
	 * time the function had returned
	 * TRUE. If not then function returns
	 * FALSE. */

	BOOL result;

	if (oneSecondUp == TRUE)
	{
		/* If a second has expired
		 * then return true and reset 
		 * the one second flag. */

		result = TRUE;	
		oneSecondUp = FALSE;
	}
	else
	{
		result = FALSE;
	}
	
	return(result);
}

//void __attribute__((vector(4), interrupt(ipl4), nomips16))
//Timer1InterruptHandler(void)
void __ISR(_TIMER_1_VECTOR, ipl7) _Timer1Handler(void)
{
	/* This is the Timer 1 ISR */
	LATB += 1;                     // Add to PORTB
    IFS0CLR = 0x10; 	           /* Clear the Interrupt Flag	*/
	
	msecCounter ++;	               /* Increment illisecond counter.	*/ 
	
	if(msecCounter == ONE_SECOND_MSEC)
	{
		/* This means that one second
		 * has expired since the last time
		 * msecCounter was 0. */

		oneSecondUp = TRUE;	/* Indicate that one second is up	*/
		msecCounter = 0;	/* Reset the millisecond counter.	*/
	}
}

#endif/* FROM_TIMER_FUNCTIONS_C */






#ifdef FROM_DATA_SHEET_PAGE_338
void main(void);
#endif

#ifdef FROM_DATA_SHEET_PAGE_338
void main(void)
{
T1CON = 0x0;               // Stop the Timer and Reset Control register
                           // Set prescaler at 1:1, internal clock source
TMR1 = 0x0;                // Clear timer register
PR1 = 0xFFFF;              // Load period register
IPC1SET = 0x000C;          // Set priority level=3
IPC1SET = 0x0001;          // Set subpriority level=1
                           // Could have also done this in single
                           // operation by assigning IPC1SET = 0x000D
IFS0CLR = 0x0010;          // Clear Timer interrupt status flag
IEC0SET = 0x0010;          // Enable Timer interrupts
T1CONSET = 0x8000;         // Start Timer
 while(1)
   {
    /*Do NOTHING FOREVER*/
   } 
  
}
void __ISR(TIMER_1_VECTOR, IPL3)// T1_Interrupt_ISR(void)
{
//... perform application specific operations in response to the interrupt
  IFS0CLR = 0x0010; // Be sure to clear the Timer 1 interrupt status
}

#endif                    // FROM_DATA_SHEET_PAGE_338








#ifdef FROM_WEB_EXAMPLE
void main (void);
void Timer1_interrupt(void);
#endif
/*********************************************************************/
/*        THE MAIN LOOP   FROM EXAMPLE                               */         
/*********************************************************************/
#ifdef FROM_WEB_EXAMPLE 
void main()
 {
  AD1PCFG = 0xFFFF;         // Initialize AN pins as digital
  TRISB  = 0;               // initialize PORTB as output
  LATB   = 0xAAAA;          // Initialize PORTB value
  
  TMR1 = 0;                 // reset timer value to zero
  PR1 = 65535;              // Load period register

  T1IP0_bit = 1;            // set interrupt
  T1IP1_bit = 1;            // priority
  T1IP2_bit = 1;            // to 7

  TCKPS0_bit = 1;           // Set Timer Input Clock
  TCKPS1_bit = 1;           // Prescale value to 1:256

  EnableInterrupts();       // Enable all interrupts

  T1IE_bit = 1;             // Enable Timer1 Interrupt
  ON__T1CON_bit = 1;        // Enable Timer1
}

void Timer1_interrupt() iv IVT_TIMER_1 ilevel 7 ics ICS_SRS 
{
  T1IF_bit = 0;             // Clear T1IF
  LATB = ~ PORTB;           // Invert PORTB
}
#endif