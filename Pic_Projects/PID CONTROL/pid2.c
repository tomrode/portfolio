
#include "pid.h" 
#include "htc.h"
#include "typedefs.h"
#include "float.h"
//#define  FLOATING_POINT_PID   /* In .h file*/
//#define  FIXED_POINT_PID


/*Define parameter For floating point*/
#ifdef FLOATING_POINT_PID 
#define epsilon 0.01
#define dt   0.01          //100ms loop time
#define MAX  100           //was 4 For Current Saturation
#define MIN -100
#define Kp  20.0          /*This made a huge differnence was this 0.1*/
#define Ki  0.050         //0.005   
#define Kd  0.10          // was 0.01
#endif

/*Define parameters for fixed point*/
#ifdef FIXED_POINT_PID
//#define epsilon      1
//#define dt           1            //100ms loop time
#define MAX      10000            //was 4 For Current Saturation
#define MIN          0
#define Kp        1500             /*This made a huge differnence was this 0.1*/
#define Kd           1
#define Ki          50             //0.005
//#define offset    9900
//#define ZERO         0
#endif


#ifdef FIXED_POINT_PID
/********************************************************************************************************/
/*PIDcal()
/*          
/*     Description:  This function implements a PID controller with clamped output saturation for overflow              
/*                   and underflow protection. Current call time is 100ms.       
/*
/*     INPUT#1-> Unsigned 8 bit set-point from ADC conversion. 
/*     INPUT#2-> Unsigned feedback 8 bit feedback from ADC conversion.
/*         
/*     OUTPUTS-> Signed 16 bit pwm value (0%-100%)  
/*********************************************************************************************************/

sint16_t PIDcal(uint8_t setpoint,uint8_t actual_position)
{
  static sint16_t pre_error = 0;
  static sint32_t integral = 0;
  sint32_t output;
  sint16_t error;
  sint16_t derivative;
   
  // Calculate P,I,D
  error = (setpoint - actual_position);

  integral = integral + error;

  if (integral > MAX)
  {
    integral = MAX;
  }
  else if (integral < MIN)
  {
    integral = MIN;
  }

  derivative = (error - pre_error);
 
  output = (sint32_t)(Kp)*(sint32_t)(error) + (sint32_t)(Ki)*(sint32_t)(integral) + (sint32_t)(Kd)*(sint32_t)(derivative);
  
  // Saturation Filter
  if (output > MAX)
  {
    output = MAX;
  }
  else if (output < MIN)
  {
    output = MIN;
  }

  // Update error
  pre_error = error;
 
  return (output/100);     

}
#endif


#ifdef FLOATING_POINT_PID
float PIDcal(float setpoint,float actual_position)
{
	static float pre_error = 0;
	static float integral = 0;
	float error;
	float derivative;
	float output = 0;
    int max;
	//Calculate P,I,D
	error = setpoint - actual_position;
    max = MAX;
	//In case of error too small then stop intergration
	if(abs(error) > epsilon)
	{
		integral = integral + error*dt;
	}
	derivative = (error - pre_error)/dt;
	output = Kp*error + Ki*integral + Kd*derivative;

	//Saturation Filter
	if(output > MAX)
	{
		output = MAX;
	}
	else if(output < MIN)
	{
		output = MIN;
	}
        //Update error
        pre_error = error;

 return output;
}
#endif


float abs(float a)
{
if (a < 0)
 {  
   return -a;
 }
 else
 {
  return a;
 }
}