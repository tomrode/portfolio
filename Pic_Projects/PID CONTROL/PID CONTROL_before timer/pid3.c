
#include "pid.h" 
#include "htc.h"
#include "typedefs.h"

#define MAX      (0x00FFFFFF)   /* Maximum in S16.16 format */
#define MIN      (0)
#define Kp       (2000)
#define Kd       (1)
#define Ki       (5)

/*****************************************************************************/
/* PIDcal():                                                                 */
/*                                                                           */
/* Description:  This function implements a PID controller with overflow     */
/*               and underflow protection.                                   */
/*                                                                           */
/* Inputs:       setpoint -> Input setpoint value                            */
/*               actual_position -> Input feedback value                     */
/*                                                                           */
/* Outputs:      output -> PWM output (0x00 = 0%, 0xFF = 100%)               */
/*****************************************************************************/

uint8_t PIDcal(uint8_t setpoint,uint8_t actual_position)
{
  static sint16_t pre_error = 0;
  sint16_t error;
  sint32_t output;

  sint32_t p_term;
  static sint32_t i_term = 0;
  sint32_t d_term;
   
  /* Calculate the error. */
  error = (setpoint - actual_position);

  /* Calculate the proportional term. */
  p_term = (sint32_t)(Kp)*(sint32_t)(error);

  /* Calculate the integral term and saturate. */
  i_term = i_term + (sint32_t)(Ki)*(sint32_t)(error);

  if (i_term > MAX)
  {
    i_term = MAX;
  }
  else if (i_term < MIN)
  {
    i_term = MIN;
  }

  /* Calculate the derivative term. */
  d_term = (sint32_t)(Kd)*(sint32_t)(error - pre_error);

  /* Calculate the output and saturate. */ 
  output = p_term; //+ i_term + d_term;
  
  if (output > MAX)
  {
    output = MAX;
  }
  else if (output < MIN)
  {
    output = MIN;
  }

  /* Update the error history. */
  pre_error = error;
  output = ((uint8_t)(output >> 16));
  return (output);
}
