opt subtitle "HI-TECH Software Omniscient Code Generator (Lite mode) build 10920"

opt pagewidth 120

	opt lm

	processor	16F887
clrc	macro
	bcf	3,0
	endm
clrz	macro
	bcf	3,2
	endm
setc	macro
	bsf	3,0
	endm
setz	macro
	bsf	3,2
	endm
skipc	macro
	btfss	3,0
	endm
skipz	macro
	btfss	3,2
	endm
skipnc	macro
	btfsc	3,0
	endm
skipnz	macro
	btfsc	3,2
	endm
indf	equ	0
indf0	equ	0
pc	equ	2
pcl	equ	2
status	equ	3
fsr	equ	4
fsr0	equ	4
c	equ	1
z	equ	0
pclath	equ	10
# 16 "C:\pic_projects\PID CONTROL\main.c"
	psect config,class=CONFIG,delta=2 ;#
# 16 "C:\pic_projects\PID CONTROL\main.c"
	dw 0x1FFF & 0x3FFF & 0x3FFF & 0x3BFF & 0x3EFF & 0x3FFF & 0x3FFF & 0x3FFF & 0x3FEF & 0x3FF7 & 0x3FFA ;#
# 17 "C:\pic_projects\PID CONTROL\main.c"
	psect config,class=CONFIG,delta=2 ;#
# 17 "C:\pic_projects\PID CONTROL\main.c"
	dw 0x3FFF ;#
	FNCALL	_main,_init
	FNCALL	_main,_adc_init
	FNCALL	_main,_pwm_init
	FNCALL	_main,_get_setpoint
	FNCALL	_main,_get_position
	FNCALL	_main,_PIDcal
	FNCALL	_main,___lbtoft
	FNCALL	_main,_output_portd
	FNCALL	_main,___ftge
	FNCALL	___lbtoft,___ftpack
	FNCALL	_output_portd,___ftge
	FNCALL	_output_portd,___fttol
	FNCALL	_output_portd,_absolute_value
	FNCALL	_PIDcal,___lmul
	FNCALL	_get_position,_adc_convert
	FNCALL	_get_setpoint,_adc_convert
	FNROOT	_main
	FNCALL	_interrupt_handler,_adc_isr
	FNCALL	intlevel1,_interrupt_handler
	global	intlevel1
	FNROOT	intlevel1
	global	_motor_speed_array
psect	strings,class=STRING,delta=2
global __pstrings
__pstrings:
;	global	stringdir,stringtab,__stringbase
stringtab:
;	String table - string pointers are 1 byte each
stringcode:stringdir:
movlw high(stringdir)
movwf pclath
movf fsr,w
incf fsr
	addwf pc
__stringbase:
	retlw	0
psect	strings
	file	"C:\pic_projects\PID CONTROL\adc_func.c"
	line	23
_motor_speed_array:
	retlw	0Ah
	retlw	0

	retlw	0Bh
	retlw	0

	retlw	0Ch
	retlw	0

	retlw	0Dh
	retlw	0

	retlw	0Fh
	retlw	0

	retlw	011h
	retlw	0

	retlw	013h
	retlw	0

	retlw	017h
	retlw	0

	retlw	01Ch
	retlw	0

	retlw	024h
	retlw	0

	retlw	032h
	retlw	0

	retlw	053h
	retlw	0

	retlw	0FAh
	retlw	0

	retlw	0A1h
	retlw	01h

	retlw	071h
	retlw	02h

	retlw	088h
	retlw	013h

	global	_motor_speed_array
	global	PIDcal@i_term
	global	_int_count
	global	_adc_isr_flag
	global	_teststruct
	global	_toms_variable
	global	PIDcal@pre_error
	global	_ADCON0
_ADCON0	set	31
	global	_ADRESH
_ADRESH	set	30
	global	_CCP1CON
_CCP1CON	set	23
	global	_CCPR1L
_CCPR1L	set	21
	global	_PORTB
_PORTB	set	6
	global	_PORTD
_PORTD	set	8
	global	_T1CON
_T1CON	set	16
	global	_T2CON
_T2CON	set	18
	global	_ADIF
_ADIF	set	102
	global	_CARRY
_CARRY	set	24
	global	_GIE
_GIE	set	95
	global	_GODONE
_GODONE	set	249
	global	_PEIE
_PEIE	set	94
	global	_RC0
_RC0	set	56
	global	_RC5
_RC5	set	61
	global	_RC6
_RC6	set	62
	global	_TO
_TO	set	28
	global	_ADCON1
_ADCON1	set	159
	global	_ADRESL
_ADRESL	set	158
	global	_OPTION
_OPTION	set	129
	global	_PR2
_PR2	set	146
	global	_TRISA
_TRISA	set	133
	global	_TRISC
_TRISC	set	135
	global	_TRISD
_TRISD	set	136
	global	_ADIE
_ADIE	set	1126
	global	_EEADR
_EEADR	set	269
	global	_EEDATA
_EEDATA	set	268
	global	_ANSEL
_ANSEL	set	392
	global	_EECON1
_EECON1	set	396
	global	_EECON2
_EECON2	set	397
	global	_RD
_RD	set	3168
	global	_WR
_WR	set	3169
	global	_WREN
_WREN	set	3170
	file	"PID CONTROL.as"
	line	#
psect cinit,class=CODE,delta=2
global start_initialization
start_initialization:

psect	bssCOMMON,class=COMMON,space=1
global __pbssCOMMON
__pbssCOMMON:
_adc_isr_flag:
       ds      2

_teststruct:
       ds      1

_toms_variable:
       ds      1

psect	bssBANK0,class=BANK0,space=1
global __pbssBANK0
__pbssBANK0:
PIDcal@i_term:
       ds      4

_int_count:
       ds      2

PIDcal@pre_error:
       ds      2

; Clear objects allocated to COMMON
psect cinit,class=CODE,delta=2
	clrf	((__pbssCOMMON)+0)&07Fh
	clrf	((__pbssCOMMON)+1)&07Fh
	clrf	((__pbssCOMMON)+2)&07Fh
	clrf	((__pbssCOMMON)+3)&07Fh
; Clear objects allocated to BANK0
psect cinit,class=CODE,delta=2
	clrf	((__pbssBANK0)+0)&07Fh
	clrf	((__pbssBANK0)+1)&07Fh
	clrf	((__pbssBANK0)+2)&07Fh
	clrf	((__pbssBANK0)+3)&07Fh
	clrf	((__pbssBANK0)+4)&07Fh
	clrf	((__pbssBANK0)+5)&07Fh
	clrf	((__pbssBANK0)+6)&07Fh
	clrf	((__pbssBANK0)+7)&07Fh
psect cinit,class=CODE,delta=2
global end_of_initialization

;End of C runtime variable initialization code

end_of_initialization:
clrf status
ljmp _main	;jump to C main() function
psect	cstackCOMMON,class=COMMON,space=1
global __pcstackCOMMON
__pcstackCOMMON:
	global	?_init
?_init:	; 0 bytes @ 0x0
	global	?_adc_init
?_adc_init:	; 0 bytes @ 0x0
	global	?_pwm_init
?_pwm_init:	; 0 bytes @ 0x0
	global	?_adc_isr
?_adc_isr:	; 0 bytes @ 0x0
	global	??_adc_isr
??_adc_isr:	; 0 bytes @ 0x0
	global	?_interrupt_handler
?_interrupt_handler:	; 0 bytes @ 0x0
	global	??_interrupt_handler
??_interrupt_handler:	; 0 bytes @ 0x0
	global	?_get_setpoint
?_get_setpoint:	; 1 bytes @ 0x0
	global	?_get_position
?_get_position:	; 1 bytes @ 0x0
	global	?_absolute_value
?_absolute_value:	; 1 bytes @ 0x0
	global	?_main
?_main:	; 2 bytes @ 0x0
	ds	4
	global	??_adc_init
??_adc_init:	; 0 bytes @ 0x4
	global	??_pwm_init
??_pwm_init:	; 0 bytes @ 0x4
	global	??_absolute_value
??_absolute_value:	; 0 bytes @ 0x4
	global	??___lmul
??___lmul:	; 0 bytes @ 0x4
	global	___lbtoft@c
___lbtoft@c:	; 1 bytes @ 0x4
	ds	1
psect	cstackBANK0,class=BANK0,space=1
global __pcstackBANK0
__pcstackBANK0:
	global	??_init
??_init:	; 0 bytes @ 0x0
	global	?_adc_convert
?_adc_convert:	; 2 bytes @ 0x0
	global	?___ftpack
?___ftpack:	; 3 bytes @ 0x0
	global	?___lmul
?___lmul:	; 4 bytes @ 0x0
	global	?___fttol
?___fttol:	; 4 bytes @ 0x0
	global	absolute_value@result
absolute_value@result:	; 1 bytes @ 0x0
	global	___fttol@f1
___fttol@f1:	; 3 bytes @ 0x0
	global	___ftpack@arg
___ftpack@arg:	; 3 bytes @ 0x0
	global	___lmul@multiplier
___lmul@multiplier:	; 4 bytes @ 0x0
	ds	1
	global	absolute_value@value
absolute_value@value:	; 1 bytes @ 0x1
	ds	1
	global	??_adc_convert
??_adc_convert:	; 0 bytes @ 0x2
	ds	1
	global	___ftpack@exp
___ftpack@exp:	; 1 bytes @ 0x3
	ds	1
	global	??___fttol
??___fttol:	; 0 bytes @ 0x4
	global	___ftpack@sign
___ftpack@sign:	; 1 bytes @ 0x4
	global	adc_convert@adresh
adc_convert@adresh:	; 2 bytes @ 0x4
	global	___lmul@multiplicand
___lmul@multiplicand:	; 4 bytes @ 0x4
	ds	1
	global	??___ftpack
??___ftpack:	; 0 bytes @ 0x5
	ds	1
	global	adc_convert@adresl
adc_convert@adresl:	; 2 bytes @ 0x6
	ds	2
	global	?___ftge
?___ftge:	; 1 bit 
	global	___fttol@sign1
___fttol@sign1:	; 1 bytes @ 0x8
	global	adc_convert@result
adc_convert@result:	; 2 bytes @ 0x8
	global	___ftge@ff1
___ftge@ff1:	; 3 bytes @ 0x8
	global	___lmul@product
___lmul@product:	; 4 bytes @ 0x8
	ds	1
	global	___fttol@lval
___fttol@lval:	; 4 bytes @ 0x9
	ds	1
	global	??_get_setpoint
??_get_setpoint:	; 0 bytes @ 0xA
	global	??_get_position
??_get_position:	; 0 bytes @ 0xA
	ds	1
	global	___ftge@ff2
___ftge@ff2:	; 3 bytes @ 0xB
	ds	1
	global	?_PIDcal
?_PIDcal:	; 1 bytes @ 0xC
	global	get_setpoint@new_setpoint
get_setpoint@new_setpoint:	; 1 bytes @ 0xC
	global	get_position@new_position
get_position@new_position:	; 1 bytes @ 0xC
	global	PIDcal@actual_position
PIDcal@actual_position:	; 1 bytes @ 0xC
	ds	1
	global	??_PIDcal
??_PIDcal:	; 0 bytes @ 0xD
	global	___fttol@exp1
___fttol@exp1:	; 1 bytes @ 0xD
	global	get_setpoint@current_setpoint
get_setpoint@current_setpoint:	; 2 bytes @ 0xD
	global	get_position@current_position
get_position@current_position:	; 2 bytes @ 0xD
	ds	1
	global	??___ftge
??___ftge:	; 0 bytes @ 0xE
	ds	6
	global	?___lbtoft
?___lbtoft:	; 3 bytes @ 0x14
	ds	1
	global	PIDcal@p_term
PIDcal@p_term:	; 4 bytes @ 0x15
	ds	2
	global	??___lbtoft
??___lbtoft:	; 0 bytes @ 0x17
	ds	2
	global	PIDcal@d_term
PIDcal@d_term:	; 4 bytes @ 0x19
	ds	2
	global	?_output_portd
?_output_portd:	; 0 bytes @ 0x1B
	global	output_portd@num
output_portd@num:	; 3 bytes @ 0x1B
	ds	2
	global	PIDcal@setpoint
PIDcal@setpoint:	; 1 bytes @ 0x1D
	ds	1
	global	??_output_portd
??_output_portd:	; 0 bytes @ 0x1E
	global	PIDcal@output
PIDcal@output:	; 4 bytes @ 0x1E
	ds	1
	global	output_portd@test
output_portd@test:	; 1 bytes @ 0x1F
	ds	3
	global	PIDcal@error
PIDcal@error:	; 2 bytes @ 0x22
	ds	2
	global	??_main
??_main:	; 0 bytes @ 0x24
	ds	3
	global	main@setpoint
main@setpoint:	; 1 bytes @ 0x27
	ds	1
	global	main@actual_position
main@actual_position:	; 1 bytes @ 0x28
	ds	1
	global	main@pid_cal_result
main@pid_cal_result:	; 1 bytes @ 0x29
	ds	1
;;Data sizes: Strings 0, constant 32, data 0, bss 12, persistent 0 stack 0
;;Auto spaces:   Size  Autos    Used
;; COMMON          13      5       9
;; BANK0           80     42      50
;; BANK1           80      0       0
;; BANK3           85      0       0
;; BANK2           96      0       0

;;
;; Pointer list with targets:

;; ?___lbtoft	float  size(1) Largest target is 0
;;
;; ?___ftpack	float  size(1) Largest target is 0
;;
;; ?___lmul	unsigned long  size(1) Largest target is 0
;;
;; ?_adc_convert	unsigned short  size(1) Largest target is 0
;;
;; ?___fttol	long  size(1) Largest target is 0
;;


;;
;; Critical Paths under _main in COMMON
;;
;;   _main->___lbtoft
;;   _output_portd->_absolute_value
;;   _output_portd->___lbtoft
;;   _PIDcal->___lmul
;;
;; Critical Paths under _interrupt_handler in COMMON
;;
;;   None.
;;
;; Critical Paths under _main in BANK0
;;
;;   _main->_PIDcal
;;   ___lbtoft->___ftge
;;   _output_portd->___lbtoft
;;   _PIDcal->___lmul
;;   _get_position->_adc_convert
;;   _get_setpoint->_adc_convert
;;   ___ftge->___ftpack
;;
;; Critical Paths under _interrupt_handler in BANK0
;;
;;   None.
;;
;; Critical Paths under _main in BANK1
;;
;;   None.
;;
;; Critical Paths under _interrupt_handler in BANK1
;;
;;   None.
;;
;; Critical Paths under _main in BANK3
;;
;;   None.
;;
;; Critical Paths under _interrupt_handler in BANK3
;;
;;   None.
;;
;; Critical Paths under _main in BANK2
;;
;;   None.
;;
;; Critical Paths under _interrupt_handler in BANK2
;;
;;   None.

;;
;;Main: autosize = 0, tempsize = 3, incstack = 0, save=0
;;

;;
;;Call Graph Tables:
;;
;; ---------------------------------------------------------------------------------
;; (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;; ---------------------------------------------------------------------------------
;; (0) _main                                                 6     6      0    2279
;;                                             36 BANK0      6     6      0
;;                               _init
;;                           _adc_init
;;                           _pwm_init
;;                       _get_setpoint
;;                       _get_position
;;                             _PIDcal
;;                           ___lbtoft
;;                       _output_portd
;;                             ___ftge
;; ---------------------------------------------------------------------------------
;; (1) ___lbtoft                                             8     5      3     343
;;                                              4 COMMON     1     1      0
;;                                             20 BANK0      7     4      3
;;                           ___ftpack
;;                             ___ftge (ARG)
;; ---------------------------------------------------------------------------------
;; (1) _output_portd                                         5     2      3     764
;;                                             27 BANK0      5     2      3
;;                             ___ftge
;;                            ___fttol
;;                     _absolute_value
;;                           ___lbtoft (ARG)
;; ---------------------------------------------------------------------------------
;; (1) _PIDcal                                              24    23      1     495
;;                                             12 BANK0     24    23      1
;;                             ___lmul
;; ---------------------------------------------------------------------------------
;; (1) _get_position                                         5     5      0     204
;;                                             10 BANK0      5     5      0
;;                        _adc_convert
;; ---------------------------------------------------------------------------------
;; (1) _get_setpoint                                         5     5      0     204
;;                                             10 BANK0      5     5      0
;;                        _adc_convert
;; ---------------------------------------------------------------------------------
;; (1) _init                                                 3     3      0       0
;;                                              0 BANK0      3     3      0
;; ---------------------------------------------------------------------------------
;; (1) ___ftge                                              12     6      6     136
;;                                              8 BANK0     12     6      6
;;                           ___ftpack (ARG)
;; ---------------------------------------------------------------------------------
;; (2) ___fttol                                             14    10      4     371
;;                                              0 BANK0     14    10      4
;; ---------------------------------------------------------------------------------
;; (2) ___lmul                                              13     5      8     136
;;                                              4 COMMON     1     1      0
;;                                              0 BANK0     12     4      8
;; ---------------------------------------------------------------------------------
;; (2) ___ftpack                                             8     3      5     312
;;                                              0 BANK0      8     3      5
;; ---------------------------------------------------------------------------------
;; (2) _adc_convert                                         10     8      2     102
;;                                              0 BANK0     10     8      2
;; ---------------------------------------------------------------------------------
;; (2) _absolute_value                                       3     3      0     130
;;                                              4 COMMON     1     1      0
;;                                              0 BANK0      2     2      0
;; ---------------------------------------------------------------------------------
;; (1) _pwm_init                                             0     0      0       0
;; ---------------------------------------------------------------------------------
;; (1) _adc_init                                             0     0      0       0
;; ---------------------------------------------------------------------------------
;; Estimated maximum stack depth 2
;; ---------------------------------------------------------------------------------
;; (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;; ---------------------------------------------------------------------------------
;; (3) _interrupt_handler                                    4     4      0       0
;;                                              0 COMMON     4     4      0
;;                            _adc_isr
;; ---------------------------------------------------------------------------------
;; (4) _adc_isr                                              0     0      0       0
;; ---------------------------------------------------------------------------------
;; Estimated maximum stack depth 4
;; ---------------------------------------------------------------------------------

;; Call Graph Graphs:

;; _main (ROOT)
;;   _init
;;   _adc_init
;;   _pwm_init
;;   _get_setpoint
;;     _adc_convert
;;   _get_position
;;     _adc_convert
;;   _PIDcal
;;     ___lmul
;;   ___lbtoft
;;     ___ftpack
;;     ___ftge (ARG)
;;       ___ftpack (ARG)
;;   _output_portd
;;     ___ftge
;;       ___ftpack (ARG)
;;     ___fttol
;;     _absolute_value
;;     ___lbtoft (ARG)
;;       ___ftpack
;;       ___ftge (ARG)
;;         ___ftpack (ARG)
;;   ___ftge
;;     ___ftpack (ARG)
;;
;; _interrupt_handler (ROOT)
;;   _adc_isr
;;

;; Address spaces:

;;Name               Size   Autos  Total    Cost      Usage
;;BITCOMMON            D      0       0       0        0.0%
;;EEDATA             100      0       0       0        0.0%
;;NULL                 0      0       0       0        0.0%
;;CODE                 0      0       0       0        0.0%
;;COMMON               D      5       9       1       69.2%
;;BITSFR0              0      0       0       1        0.0%
;;SFR0                 0      0       0       1        0.0%
;;BITSFR1              0      0       0       2        0.0%
;;SFR1                 0      0       0       2        0.0%
;;STACK                0      0       6       2        0.0%
;;ABS                  0      0      3B       3        0.0%
;;BITBANK0            50      0       0       4        0.0%
;;BITSFR3              0      0       0       4        0.0%
;;SFR3                 0      0       0       4        0.0%
;;BANK0               50     2A      32       5       62.5%
;;BITSFR2              0      0       0       5        0.0%
;;SFR2                 0      0       0       5        0.0%
;;BITBANK1            50      0       0       6        0.0%
;;BANK1               50      0       0       7        0.0%
;;BITBANK3            55      0       0       8        0.0%
;;BANK3               55      0       0       9        0.0%
;;BITBANK2            60      0       0      10        0.0%
;;BANK2               60      0       0      11        0.0%
;;DATA                 0      0      41      12        0.0%

	global	_main
psect	maintext,global,class=CODE,delta=2
global __pmaintext
__pmaintext:

;; *************** function _main *****************
;; Defined at:
;;		line 34 in file "C:\pic_projects\PID CONTROL\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  pid_cal_resu    1   41[BANK0 ] unsigned char 
;;  actual_posit    1   40[BANK0 ] unsigned char 
;;  setpoint        1   39[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;                  2  856[COMMON] int 
;; Registers used:
;;		wreg, status,2, status,0, btemp+1, pclath, cstack
;; Tracked objects:
;;		On entry : 17F/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       3       0       0       0
;;      Temps:          0       3       0       0       0
;;      Totals:         0       6       0       0       0
;;Total ram usage:        6 bytes
;; Hardware stack levels required when called:    4
;; This function calls:
;;		_init
;;		_adc_init
;;		_pwm_init
;;		_get_setpoint
;;		_get_position
;;		_PIDcal
;;		___lbtoft
;;		_output_portd
;;		___ftge
;; This function is called by:
;;		Startup code after reset
;; This function uses a non-reentrant model
;;
psect	maintext
	file	"C:\pic_projects\PID CONTROL\main.c"
	line	34
	global	__size_of_main
	__size_of_main	equ	__end_of_main-_main
	
_main:	
	opt	stack 4
; Regs used in _main: [wreg+status,2+status,0+btemp+1+pclath+cstack]
	line	50
	
l8109:	
;main.c: 39: uint8_t setpoint;
;main.c: 41: uint8_t actual_position;
;main.c: 42: uint8_t pid_cal_result;
;main.c: 50: init();
	fcall	_init
	line	51
;main.c: 51: adc_init();
	fcall	_adc_init
	line	52
;main.c: 52: pwm_init();
	fcall	_pwm_init
	line	54
	
l8111:	
;main.c: 54: ADIE = 1;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bsf	(1126/8)^080h,(1126)&7
	line	55
	
l8113:	
;main.c: 55: PEIE = 1;
	bsf	(94/8),(94)&7
	line	56
	
l8115:	
;main.c: 56: (GIE = 1);
	bsf	(95/8),(95)&7
	goto	l8117
	line	63
;main.c: 63: while(1)
	
l857:	
	line	66
	
l8117:	
;main.c: 64: {
;main.c: 66: if (TO == 1)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfss	(28/8),(28)&7
	goto	u4271
	goto	u4270
u4271:
	goto	l858
u4270:
	line	68
	
l8119:	
;main.c: 67: {
;main.c: 68: RC6 = 1;
	bsf	(62/8),(62)&7
	line	69
	
l8121:	
;main.c: 69: setpoint = get_setpoint();
	fcall	_get_setpoint
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(main@setpoint)
	line	70
;main.c: 70: actual_position = get_position();
	fcall	_get_position
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(main@actual_position)
	line	71
	
l8123:	
;main.c: 71: RC5 = 1;
	bsf	(61/8),(61)&7
	line	72
	
l8125:	
;main.c: 72: pid_cal_result = PIDcal(setpoint, actual_position);
	movf	(main@actual_position),w
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(?_PIDcal)
	movf	(main@setpoint),w
	fcall	_PIDcal
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_main+1)+0
	movf	(??_main+1)+0,w
	movwf	(main@pid_cal_result)
	line	73
	
l8127:	
;main.c: 73: RC5 = 0;
	bcf	(61/8),(61)&7
	line	74
	
l8129:	
;main.c: 74: output_portd(pid_cal_result);
	movf	(main@pid_cal_result),w
	fcall	___lbtoft
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(0+(?___lbtoft)),w
	movwf	(?_output_portd)
	movf	(1+(?___lbtoft)),w
	movwf	(?_output_portd+1)
	movf	(2+(?___lbtoft)),w
	movwf	(?_output_portd+2)
	fcall	_output_portd
	line	75
	
l8131:	
;main.c: 75: if(pid_cal_result > 0.0)
	movlw	0x0
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(?___ftge)
	movlw	0x0
	movwf	(?___ftge+1)
	movlw	0x0
	movwf	(?___ftge+2)
	movf	(main@pid_cal_result),w
	fcall	___lbtoft
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(0+(?___lbtoft)),w
	movwf	0+(?___ftge)+03h
	movf	(1+(?___lbtoft)),w
	movwf	1+(?___ftge)+03h
	movf	(2+(?___lbtoft)),w
	movwf	2+(?___ftge)+03h
	fcall	___ftge
	goto	l860
	line	78
	
l8133:	
;main.c: 76: {
;main.c: 78: }
	goto	l860
	line	79
	
l859:	
	line	82
;main.c: 79: else
;main.c: 80: {
	
l860:	
	line	83
;main.c: 82: }
;main.c: 83: RC6 = 0;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	bcf	(62/8),(62)&7
	line	84
	
l8135:	
;main.c: 84: _delay(200000);
	opt asmopt_off
movlw  2
movwf	((??_main+0)+0+2),f
movlw	4
movwf	((??_main+0)+0+1),f
	movlw	186
movwf	((??_main+0)+0),f
u4287:
	decfsz	((??_main+0)+0),f
	goto	u4287
	decfsz	((??_main+0)+0+1),f
	goto	u4287
	decfsz	((??_main+0)+0+2),f
	goto	u4287
	clrwdt
opt asmopt_on

	line	86
;main.c: 86: }
	goto	l8117
	line	87
	
l858:	
	line	89
# 89 "C:\pic_projects\PID CONTROL\main.c"
clrwdt ;#
psect	maintext
	goto	l8117
	line	90
	
l861:	
	goto	l8117
	line	92
	
l862:	
	line	63
	goto	l8117
	
l863:	
	line	93
	
l864:	
	global	start
	ljmp	start
	opt stack 0
GLOBAL	__end_of_main
	__end_of_main:
;; =============== function _main ends ============

	signat	_main,90
	global	___lbtoft
psect	text613,local,class=CODE,delta=2
global __ptext613
__ptext613:

;; *************** function ___lbtoft *****************
;; Defined at:
;;		line 28 in file "C:\Program Files\HI-TECH Software\PICC\9.83\sources\lbtoft.c"
;; Parameters:    Size  Location     Type
;;  c               1    wreg     unsigned char 
;; Auto vars:     Size  Location     Type
;;  c               1    4[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;                  3   20[BANK0 ] float 
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       3       0       0       0
;;      Locals:         1       0       0       0       0
;;      Temps:          0       4       0       0       0
;;      Totals:         1       7       0       0       0
;;Total ram usage:        8 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		___ftpack
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text613
	file	"C:\Program Files\HI-TECH Software\PICC\9.83\sources\lbtoft.c"
	line	28
	global	__size_of___lbtoft
	__size_of___lbtoft	equ	__end_of___lbtoft-___lbtoft
	
___lbtoft:	
	opt	stack 4
; Regs used in ___lbtoft: [wreg+status,2+status,0+pclath+cstack]
;___lbtoft@c stored from wreg
	movwf	(___lbtoft@c)
	line	29
	
l8105:	
	movf	(___lbtoft@c),w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	((??___lbtoft+0)+0)
	clrf	((??___lbtoft+0)+0+1)
	clrf	((??___lbtoft+0)+0+2)
	movf	0+(??___lbtoft+0)+0,w
	movwf	(?___ftpack)
	movf	1+(??___lbtoft+0)+0,w
	movwf	(?___ftpack+1)
	movf	2+(??___lbtoft+0)+0,w
	movwf	(?___ftpack+2)
	movlw	(08Eh)
	movwf	(??___lbtoft+3)+0
	movf	(??___lbtoft+3)+0,w
	movwf	0+(?___ftpack)+03h
	clrf	0+(?___ftpack)+04h
	fcall	___ftpack
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(0+(?___ftpack)),w
	movwf	(?___lbtoft)
	movf	(1+(?___ftpack)),w
	movwf	(?___lbtoft+1)
	movf	(2+(?___ftpack)),w
	movwf	(?___lbtoft+2)
	goto	l5758
	
l8107:	
	line	30
	
l5758:	
	return
	opt stack 0
GLOBAL	__end_of___lbtoft
	__end_of___lbtoft:
;; =============== function ___lbtoft ends ============

	signat	___lbtoft,4219
	global	_output_portd
psect	text614,local,class=CODE,delta=2
global __ptext614
__ptext614:

;; *************** function _output_portd *****************
;; Defined at:
;;		line 113 in file "C:\pic_projects\PID CONTROL\main.c"
;; Parameters:    Size  Location     Type
;;  num             3   27[BANK0 ] float 
;; Auto vars:     Size  Location     Type
;;  test            1   31[BANK0 ] char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       3       0       0       0
;;      Locals:         0       1       0       0       0
;;      Temps:          0       1       0       0       0
;;      Totals:         0       5       0       0       0
;;Total ram usage:        5 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		___ftge
;;		___fttol
;;		_absolute_value
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text614
	file	"C:\pic_projects\PID CONTROL\main.c"
	line	113
	global	__size_of_output_portd
	__size_of_output_portd	equ	__end_of_output_portd-_output_portd
	
_output_portd:	
	opt	stack 4
; Regs used in _output_portd: [wreg+status,2+status,0+pclath+cstack]
	line	115
	
l8087:	
;main.c: 114: signed char test;
;main.c: 115: if(num <= -0.00000000)
	movlw	0x0
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(?___ftge)
	movlw	0x0
	movwf	(?___ftge+1)
	movlw	0x0
	movwf	(?___ftge+2)
	movf	(output_portd@num),w
	movwf	0+(?___ftge)+03h
	movf	(output_portd@num+1),w
	movwf	1+(?___ftge)+03h
	movf	(output_portd@num+2),w
	movwf	2+(?___ftge)+03h
	fcall	___ftge
	btfss	status,0
	goto	u4261
	goto	u4260
u4261:
	goto	l870
u4260:
	line	117
	
l8089:	
;main.c: 116: {
;main.c: 117: RC0 = 0;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	bcf	(56/8),(56)&7
	line	118
# 118 "C:\pic_projects\PID CONTROL\main.c"
nop ;#
psect	text614
	line	119
	
l8091:	
;main.c: 119: test = (signed char)(num);
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(output_portd@num),w
	movwf	(?___fttol)
	movf	(output_portd@num+1),w
	movwf	(?___fttol+1)
	movf	(output_portd@num+2),w
	movwf	(?___fttol+2)
	fcall	___fttol
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	0+(((0+(?___fttol)))),w
	movwf	(??_output_portd+0)+0
	movf	(??_output_portd+0)+0,w
	movwf	(output_portd@test)
	line	120
;main.c: 120: PORTD = absolute_value(test);
	movf	(output_portd@test),w
	fcall	_absolute_value
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(8)
	line	121
	
l8093:	
;main.c: 121: CCPR1L = 0;
	clrf	(21)	;volatile
	line	122
	
l8095:	
# 122 "C:\pic_projects\PID CONTROL\main.c"
nop ;#
psect	text614
	line	123
;main.c: 123: }
	goto	l8103
	line	124
	
l870:	
	line	126
;main.c: 124: else
;main.c: 125: {
;main.c: 126: RC0 = 1;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	bsf	(56/8),(56)&7
	line	127
	
l8097:	
;main.c: 127: PORTD = (char)(num);
	movf	(output_portd@num),w
	movwf	(?___fttol)
	movf	(output_portd@num+1),w
	movwf	(?___fttol+1)
	movf	(output_portd@num+2),w
	movwf	(?___fttol+2)
	fcall	___fttol
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	0+(((0+(?___fttol)))),w
	movwf	(8)	;volatile
	line	128
	
l8099:	
;main.c: 128: CCPR1L = PORTD;
	movf	(8),w	;volatile
	movwf	(21)	;volatile
	line	129
	
l8101:	
# 129 "C:\pic_projects\PID CONTROL\main.c"
nop ;#
psect	text614
	goto	l8103
	line	130
	
l871:	
	line	132
	
l8103:	
# 132 "C:\pic_projects\PID CONTROL\main.c"
nop ;#
psect	text614
	line	133
	
l872:	
	return
	opt stack 0
GLOBAL	__end_of_output_portd
	__end_of_output_portd:
;; =============== function _output_portd ends ============

	signat	_output_portd,4216
	global	_PIDcal
psect	text615,local,class=CODE,delta=2
global __ptext615
__ptext615:

;; *************** function _PIDcal *****************
;; Defined at:
;;		line 27 in file "C:\pic_projects\PID CONTROL\pid4.c"
;; Parameters:    Size  Location     Type
;;  setpoint        1    wreg     unsigned char 
;;  actual_posit    1   12[BANK0 ] unsigned char 
;; Auto vars:     Size  Location     Type
;;  setpoint        1   29[BANK0 ] unsigned char 
;;  output          4   30[BANK0 ] long 
;;  d_term          4   25[BANK0 ] long 
;;  p_term          4   21[BANK0 ] long 
;;  error           2   34[BANK0 ] short 
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, status,2, status,0, btemp+1, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       1       0       0       0
;;      Locals:         0      15       0       0       0
;;      Temps:          0       8       0       0       0
;;      Totals:         0      24       0       0       0
;;Total ram usage:       24 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		___lmul
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text615
	file	"C:\pic_projects\PID CONTROL\pid4.c"
	line	27
	global	__size_of_PIDcal
	__size_of_PIDcal	equ	__end_of_PIDcal-_PIDcal
	
_PIDcal:	
	opt	stack 4
; Regs used in _PIDcal: [wreg+status,2+status,0+btemp+1+pclath+cstack]
;PIDcal@setpoint stored from wreg
	line	37
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(PIDcal@setpoint)
	
l8055:	
;pid4.c: 28: static sint16_t pre_error = 0;
;pid4.c: 29: sint16_t error;
;pid4.c: 30: sint32_t output;
;pid4.c: 32: sint32_t p_term;
;pid4.c: 33: static sint32_t i_term = 0;
;pid4.c: 34: sint32_t d_term;
;pid4.c: 37: (GIE = 0);
	bcf	(95/8),(95)&7
	line	38
	
l8057:	
;pid4.c: 38: error = (setpoint - actual_position);
	movf	(PIDcal@actual_position),w
	movwf	(??_PIDcal+0)+0
	clrf	(??_PIDcal+0)+0+1
	comf	(??_PIDcal+0)+0,f
	comf	(??_PIDcal+0)+1,f
	incf	(??_PIDcal+0)+0,f
	skipnz
	incf	(??_PIDcal+0)+1,f
	movf	(PIDcal@setpoint),w
	addwf	0+(??_PIDcal+0)+0,w
	movwf	(PIDcal@error)
	movf	1+(??_PIDcal+0)+0,w
	skipnc
	incf	1+(??_PIDcal+0)+0,w
	movwf	((PIDcal@error))+1
	line	41
	
l8059:	
;pid4.c: 41: p_term = (sint32_t)((40*(65536)))*(sint32_t)(error);
	movf	(PIDcal@error),w
	movwf	(?___lmul)
	movf	(PIDcal@error+1),w
	movwf	(?___lmul+1)
	movlw	0
	btfsc	(?___lmul+1),7
	movlw	255
	movwf	(?___lmul+2)
	movwf	(?___lmul+3)
	movlw	0
	movwf	3+(?___lmul)+04h
	movlw	028h
	movwf	2+(?___lmul)+04h
	movlw	0
	movwf	1+(?___lmul)+04h
	movlw	0
	movwf	0+(?___lmul)+04h

	fcall	___lmul
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(3+(?___lmul)),w
	movwf	(PIDcal@p_term+3)
	movf	(2+(?___lmul)),w
	movwf	(PIDcal@p_term+2)
	movf	(1+(?___lmul)),w
	movwf	(PIDcal@p_term+1)
	movf	(0+(?___lmul)),w
	movwf	(PIDcal@p_term)

	line	44
	
l8061:	
;pid4.c: 44: i_term = i_term + (sint32_t)((5.00*(65536))*(0.01))*(sint32_t)(error);
	movf	(PIDcal@error),w
	movwf	(?___lmul)
	movf	(PIDcal@error+1),w
	movwf	(?___lmul+1)
	movlw	0
	btfsc	(?___lmul+1),7
	movlw	255
	movwf	(?___lmul+2)
	movwf	(?___lmul+3)
	movlw	0
	movwf	3+(?___lmul)+04h
	movlw	0
	movwf	2+(?___lmul)+04h
	movlw	0Ch
	movwf	1+(?___lmul)+04h
	movlw	0CCh
	movwf	0+(?___lmul)+04h

	fcall	___lmul
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(0+(?___lmul)),w
	addwf	(PIDcal@i_term),w
	movwf	((??_PIDcal+0)+0+0)
	movlw	0
	skipnc
	movlw	1
	addwf	(1+(?___lmul)),w
	clrf	((??_PIDcal+0)+0+2)
	skipnc
	incf	((??_PIDcal+0)+0+2),f
	addwf	(PIDcal@i_term+1),w
	movwf	((??_PIDcal+0)+0+1)
	skipnc
	incf	((??_PIDcal+0)+0+2),f
	movf	(2+(?___lmul)),w
	addwf	((??_PIDcal+0)+0+2),w
	clrf	((??_PIDcal+0)+0+3)
	skipnc
	incf	((??_PIDcal+0)+0+3),f
	addwf	(PIDcal@i_term+2),w
	movwf	((??_PIDcal+0)+0+2)
	skipnc
	incf	((??_PIDcal+0)+0+3),f
	movf	(3+(?___lmul)),w
	addwf	((??_PIDcal+0)+0+3),w
	addwf	(PIDcal@i_term+3),w
	movwf	((??_PIDcal+0)+0+3)
	movf	3+(??_PIDcal+0)+0,w
	movwf	(PIDcal@i_term+3)
	movf	2+(??_PIDcal+0)+0,w
	movwf	(PIDcal@i_term+2)
	movf	1+(??_PIDcal+0)+0,w
	movwf	(PIDcal@i_term+1)
	movf	0+(??_PIDcal+0)+0,w
	movwf	(PIDcal@i_term)

	line	46
	
l8063:	
;pid4.c: 46: if (i_term > (100*(65536)))
	movf	(PIDcal@i_term+3),w
	xorlw	80h
	movwf	btemp+1
	movlw	0
	xorlw	80h
	subwf	btemp+1,w
	
	skipz
	goto	u4203
	movlw	064h
	subwf	(PIDcal@i_term+2),w
	skipz
	goto	u4203
	movlw	0
	subwf	(PIDcal@i_term+1),w
	skipz
	goto	u4203
	movlw	01h
	subwf	(PIDcal@i_term),w
u4203:
	skipc
	goto	u4201
	goto	u4200
u4201:
	goto	l4638
u4200:
	line	48
	
l8065:	
;pid4.c: 47: {
;pid4.c: 48: i_term = (100*(65536));
	movlw	0
	movwf	(PIDcal@i_term+3)
	movlw	064h
	movwf	(PIDcal@i_term+2)
	movlw	0
	movwf	(PIDcal@i_term+1)
	movlw	0
	movwf	(PIDcal@i_term)

	line	49
;pid4.c: 49: }
	goto	l8069
	line	50
	
l4638:	
;pid4.c: 50: else if (i_term < (0))
	btfss	(PIDcal@i_term+3),7
	goto	u4211
	goto	u4210
u4211:
	goto	l8069
u4210:
	line	52
	
l8067:	
;pid4.c: 51: {
;pid4.c: 52: i_term = (0);
	movlw	0
	movwf	(PIDcal@i_term+3)
	movlw	0
	movwf	(PIDcal@i_term+2)
	movlw	0
	movwf	(PIDcal@i_term+1)
	movlw	0
	movwf	(PIDcal@i_term)

	goto	l8069
	line	53
	
l4640:	
	goto	l8069
	line	56
	
l4639:	
	
l8069:	
;pid4.c: 53: }
;pid4.c: 56: d_term = (sint32_t)((0.10*(65536))/(0.01))*(sint32_t)(error - pre_error);
	comf	(PIDcal@pre_error),w
	movwf	(??_PIDcal+0)+0
	comf	(PIDcal@pre_error+1),w
	movwf	((??_PIDcal+0)+0+1)
	incf	(??_PIDcal+0)+0,f
	skipnz
	incf	((??_PIDcal+0)+0+1),f
	movf	(PIDcal@error),w
	addwf	0+(??_PIDcal+0)+0,w
	movwf	(?___lmul)
	movf	(PIDcal@error+1),w
	skipnc
	incf	(PIDcal@error+1),w
	addwf	1+(??_PIDcal+0)+0,w
	movwf	1+(?___lmul)
	clrf	(?___lmul)+2
	btfsc	(?___lmul)+1,7
	decf	2+(?___lmul),f
	movf	(?___lmul)+2,w
	movwf	3+(?___lmul)
	movlw	0
	movwf	3+(?___lmul)+04h
	movlw	0Ah
	movwf	2+(?___lmul)+04h
	movlw	0
	movwf	1+(?___lmul)+04h
	movlw	0
	movwf	0+(?___lmul)+04h

	fcall	___lmul
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(3+(?___lmul)),w
	movwf	(PIDcal@d_term+3)
	movf	(2+(?___lmul)),w
	movwf	(PIDcal@d_term+2)
	movf	(1+(?___lmul)),w
	movwf	(PIDcal@d_term+1)
	movf	(0+(?___lmul)),w
	movwf	(PIDcal@d_term)

	line	59
	
l8071:	
;pid4.c: 59: output = p_term + i_term + d_term;
	movf	(PIDcal@d_term),w
	movwf	(??_PIDcal+0)+0
	movf	(PIDcal@d_term+1),w
	movwf	((??_PIDcal+0)+0+1)
	movf	(PIDcal@d_term+2),w
	movwf	((??_PIDcal+0)+0+2)
	movf	(PIDcal@d_term+3),w
	movwf	((??_PIDcal+0)+0+3)
	movf	(PIDcal@p_term),w
	addwf	(PIDcal@i_term),w
	movwf	((??_PIDcal+4)+0+0)
	movlw	0
	skipnc
	movlw	1
	addwf	(PIDcal@p_term+1),w
	clrf	((??_PIDcal+4)+0+2)
	skipnc
	incf	((??_PIDcal+4)+0+2),f
	addwf	(PIDcal@i_term+1),w
	movwf	((??_PIDcal+4)+0+1)
	skipnc
	incf	((??_PIDcal+4)+0+2),f
	movf	(PIDcal@p_term+2),w
	addwf	((??_PIDcal+4)+0+2),w
	clrf	((??_PIDcal+4)+0+3)
	skipnc
	incf	((??_PIDcal+4)+0+3),f
	addwf	(PIDcal@i_term+2),w
	movwf	((??_PIDcal+4)+0+2)
	skipnc
	incf	((??_PIDcal+4)+0+3),f
	movf	(PIDcal@p_term+3),w
	addwf	((??_PIDcal+4)+0+3),w
	addwf	(PIDcal@i_term+3),w
	movwf	((??_PIDcal+4)+0+3)
	movf	0+(??_PIDcal+4)+0,w
	addwf	(??_PIDcal+0)+0,f
	movf	1+(??_PIDcal+4)+0,w
	skipnc
	incfsz	1+(??_PIDcal+4)+0,w
	goto	u4220
	goto	u4221
u4220:
	addwf	(??_PIDcal+0)+1,f
u4221:
	movf	2+(??_PIDcal+4)+0,w
	skipnc
	incfsz	2+(??_PIDcal+4)+0,w
	goto	u4222
	goto	u4223
u4222:
	addwf	(??_PIDcal+0)+2,f
u4223:
	movf	3+(??_PIDcal+4)+0,w
	skipnc
	incf	3+(??_PIDcal+4)+0,w
	addwf	(??_PIDcal+0)+3,f
	movf	3+(??_PIDcal+0)+0,w
	movwf	(PIDcal@output+3)
	movf	2+(??_PIDcal+0)+0,w
	movwf	(PIDcal@output+2)
	movf	1+(??_PIDcal+0)+0,w
	movwf	(PIDcal@output+1)
	movf	0+(??_PIDcal+0)+0,w
	movwf	(PIDcal@output)

	line	61
	
l8073:	
;pid4.c: 61: if (output > (100*(65536)))
	movf	(PIDcal@output+3),w
	xorlw	80h
	movwf	btemp+1
	movlw	0
	xorlw	80h
	subwf	btemp+1,w
	
	skipz
	goto	u4233
	movlw	064h
	subwf	(PIDcal@output+2),w
	skipz
	goto	u4233
	movlw	0
	subwf	(PIDcal@output+1),w
	skipz
	goto	u4233
	movlw	01h
	subwf	(PIDcal@output),w
u4233:
	skipc
	goto	u4231
	goto	u4230
u4231:
	goto	l4641
u4230:
	line	63
	
l8075:	
;pid4.c: 62: {
;pid4.c: 63: output = (100*(65536));
	movlw	0
	movwf	(PIDcal@output+3)
	movlw	064h
	movwf	(PIDcal@output+2)
	movlw	0
	movwf	(PIDcal@output+1)
	movlw	0
	movwf	(PIDcal@output)

	line	64
;pid4.c: 64: }
	goto	l8079
	line	65
	
l4641:	
;pid4.c: 65: else if (output < (0))
	btfss	(PIDcal@output+3),7
	goto	u4241
	goto	u4240
u4241:
	goto	l8079
u4240:
	line	67
	
l8077:	
;pid4.c: 66: {
;pid4.c: 67: output = (0);
	movlw	0
	movwf	(PIDcal@output+3)
	movlw	0
	movwf	(PIDcal@output+2)
	movlw	0
	movwf	(PIDcal@output+1)
	movlw	0
	movwf	(PIDcal@output)

	goto	l8079
	line	68
	
l4643:	
	goto	l8079
	line	71
	
l4642:	
	
l8079:	
;pid4.c: 68: }
;pid4.c: 71: pre_error = error;
	movf	(PIDcal@error+1),w
	clrf	(PIDcal@pre_error+1)
	addwf	(PIDcal@pre_error+1)
	movf	(PIDcal@error),w
	clrf	(PIDcal@pre_error)
	addwf	(PIDcal@pre_error)

	line	73
	
l8081:	
;pid4.c: 73: (GIE = 1);
	bsf	(95/8),(95)&7
	line	74
	
l8083:	
;pid4.c: 74: return ((uint8_t)(output >> 16));
	movf	(PIDcal@output),w
	movwf	(??_PIDcal+0)+0
	movf	(PIDcal@output+1),w
	movwf	((??_PIDcal+0)+0+1)
	movf	(PIDcal@output+2),w
	movwf	((??_PIDcal+0)+0+2)
	movf	(PIDcal@output+3),w
	movwf	((??_PIDcal+0)+0+3)
	movlw	010h
	movwf	(??_PIDcal+4)+0
u4255:
	rlf	(??_PIDcal+0)+3,w
	rrf	(??_PIDcal+0)+3,f
	rrf	(??_PIDcal+0)+2,f
	rrf	(??_PIDcal+0)+1,f
	rrf	(??_PIDcal+0)+0,f
u4250:
	decfsz	(??_PIDcal+4)+0,f
	goto	u4255
	movf	0+(??_PIDcal+0)+0,w
	goto	l4644
	
l8085:	
	line	75
	
l4644:	
	return
	opt stack 0
GLOBAL	__end_of_PIDcal
	__end_of_PIDcal:
;; =============== function _PIDcal ends ============

	signat	_PIDcal,8313
	global	_get_position
psect	text616,local,class=CODE,delta=2
global __ptext616
__ptext616:

;; *************** function _get_position *****************
;; Defined at:
;;		line 96 in file "C:\pic_projects\PID CONTROL\adc_func.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  current_posi    2   13[BANK0 ] unsigned short 
;;  new_position    1   12[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       3       0       0       0
;;      Temps:          0       2       0       0       0
;;      Totals:         0       5       0       0       0
;;Total ram usage:        5 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		_adc_convert
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text616
	file	"C:\pic_projects\PID CONTROL\adc_func.c"
	line	96
	global	__size_of_get_position
	__size_of_get_position	equ	__end_of_get_position-_get_position
	
_get_position:	
	opt	stack 4
; Regs used in _get_position: [wreg+status,2+status,0+pclath+cstack]
	line	100
	
l8043:	
;adc_func.c: 97: uint16_t current_position;
;adc_func.c: 98: uint8_t new_position;
;adc_func.c: 100: ADCON0 = 0xC9;
	movlw	(0C9h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(31)	;volatile
	line	101
	
l8045:	
;adc_func.c: 101: current_position = adc_convert();
	fcall	_adc_convert
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(1+(?_adc_convert)),w
	clrf	(get_position@current_position+1)
	addwf	(get_position@current_position+1)
	movf	(0+(?_adc_convert)),w
	clrf	(get_position@current_position)
	addwf	(get_position@current_position)

	line	102
	
l8047:	
;adc_func.c: 102: current_position = (current_position >> 2);
	movf	(get_position@current_position+1),w
	movwf	(??_get_position+0)+0+1
	movf	(get_position@current_position),w
	movwf	(??_get_position+0)+0
	movlw	02h
u4195:
	clrc
	rrf	(??_get_position+0)+1,f
	rrf	(??_get_position+0)+0,f
	addlw	-1
	skipz
	goto	u4195
	movf	0+(??_get_position+0)+0,w
	movwf	(get_position@current_position)
	movf	1+(??_get_position+0)+0,w
	movwf	(get_position@current_position+1)
	line	103
	
l8049:	
;adc_func.c: 103: new_position = current_position;
	movf	(get_position@current_position),w
	movwf	(??_get_position+0)+0
	movf	(??_get_position+0)+0,w
	movwf	(get_position@new_position)
	line	104
	
l8051:	
;adc_func.c: 104: return new_position;
	movf	(get_position@new_position),w
	goto	l1731
	
l8053:	
	line	105
	
l1731:	
	return
	opt stack 0
GLOBAL	__end_of_get_position
	__end_of_get_position:
;; =============== function _get_position ends ============

	signat	_get_position,89
	global	_get_setpoint
psect	text617,local,class=CODE,delta=2
global __ptext617
__ptext617:

;; *************** function _get_setpoint *****************
;; Defined at:
;;		line 83 in file "C:\pic_projects\PID CONTROL\adc_func.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  current_setp    2   13[BANK0 ] unsigned short 
;;  new_setpoint    1   12[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       3       0       0       0
;;      Temps:          0       2       0       0       0
;;      Totals:         0       5       0       0       0
;;Total ram usage:        5 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		_adc_convert
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text617
	file	"C:\pic_projects\PID CONTROL\adc_func.c"
	line	83
	global	__size_of_get_setpoint
	__size_of_get_setpoint	equ	__end_of_get_setpoint-_get_setpoint
	
_get_setpoint:	
	opt	stack 4
; Regs used in _get_setpoint: [wreg+status,2+status,0+pclath+cstack]
	line	87
	
l8031:	
;adc_func.c: 84: uint16_t current_setpoint;
;adc_func.c: 85: uint8_t new_setpoint;
;adc_func.c: 87: ADCON0 = 0xC5;
	movlw	(0C5h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(31)	;volatile
	line	89
	
l8033:	
;adc_func.c: 89: current_setpoint = adc_convert();
	fcall	_adc_convert
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(1+(?_adc_convert)),w
	clrf	(get_setpoint@current_setpoint+1)
	addwf	(get_setpoint@current_setpoint+1)
	movf	(0+(?_adc_convert)),w
	clrf	(get_setpoint@current_setpoint)
	addwf	(get_setpoint@current_setpoint)

	line	90
	
l8035:	
;adc_func.c: 90: current_setpoint = (current_setpoint >> 2);
	movf	(get_setpoint@current_setpoint+1),w
	movwf	(??_get_setpoint+0)+0+1
	movf	(get_setpoint@current_setpoint),w
	movwf	(??_get_setpoint+0)+0
	movlw	02h
u4185:
	clrc
	rrf	(??_get_setpoint+0)+1,f
	rrf	(??_get_setpoint+0)+0,f
	addlw	-1
	skipz
	goto	u4185
	movf	0+(??_get_setpoint+0)+0,w
	movwf	(get_setpoint@current_setpoint)
	movf	1+(??_get_setpoint+0)+0,w
	movwf	(get_setpoint@current_setpoint+1)
	line	91
	
l8037:	
;adc_func.c: 91: new_setpoint = current_setpoint;
	movf	(get_setpoint@current_setpoint),w
	movwf	(??_get_setpoint+0)+0
	movf	(??_get_setpoint+0)+0,w
	movwf	(get_setpoint@new_setpoint)
	line	92
	
l8039:	
;adc_func.c: 92: return new_setpoint;
	movf	(get_setpoint@new_setpoint),w
	goto	l1728
	
l8041:	
	line	93
	
l1728:	
	return
	opt stack 0
GLOBAL	__end_of_get_setpoint
	__end_of_get_setpoint:
;; =============== function _get_setpoint ends ============

	signat	_get_setpoint,89
	global	_init
psect	text618,local,class=CODE,delta=2
global __ptext618
__ptext618:

;; *************** function _init *****************
;; Defined at:
;;		line 97 in file "C:\pic_projects\PID CONTROL\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       3       0       0       0
;;      Totals:         0       3       0       0       0
;;Total ram usage:        3 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text618
	file	"C:\pic_projects\PID CONTROL\main.c"
	line	97
	global	__size_of_init
	__size_of_init	equ	__end_of_init-_init
	
_init:	
	opt	stack 5
; Regs used in _init: [wreg+status,2]
	line	98
	
l8017:	
;main.c: 98: OPTION = 0x8F;
	movlw	(08Fh)
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movwf	(129)^080h	;volatile
	line	99
	
l8019:	
;main.c: 99: TRISC = 0x00;
	clrf	(135)^080h	;volatile
	line	100
	
l8021:	
;main.c: 100: TRISD = 0x00;
	clrf	(136)^080h	;volatile
	line	101
	
l8023:	
;main.c: 101: PORTB = 0x00;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(6)	;volatile
	line	102
	
l8025:	
;main.c: 102: PORTD = 0xFF;
	movlw	(0FFh)
	movwf	(8)	;volatile
	line	105
	
l8027:	
;main.c: 105: T1CON = 0x35;
	movlw	(035h)
	movwf	(16)	;volatile
	line	106
	
l8029:	
;main.c: 106: _delay(800000);
	opt asmopt_off
movlw  5
movwf	((??_init+0)+0+2),f
movlw	15
movwf	((??_init+0)+0+1),f
	movlw	244
movwf	((??_init+0)+0),f
u4297:
	decfsz	((??_init+0)+0),f
	goto	u4297
	decfsz	((??_init+0)+0+1),f
	goto	u4297
	decfsz	((??_init+0)+0+2),f
	goto	u4297
opt asmopt_on

	line	108
	
l867:	
	return
	opt stack 0
GLOBAL	__end_of_init
	__end_of_init:
;; =============== function _init ends ============

	signat	_init,88
	global	___ftge
psect	text619,local,class=CODE,delta=2
global __ptext619
__ptext619:

;; *************** function ___ftge *****************
;; Defined at:
;;		line 5 in file "C:\Program Files\HI-TECH Software\PICC\9.83\sources\ftge.c"
;; Parameters:    Size  Location     Type
;;  ff1             3    8[BANK0 ] float 
;;  ff2             3   11[BANK0 ] float 
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       6       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       6       0       0       0
;;      Totals:         0      12       0       0       0
;;Total ram usage:       12 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;;		_output_portd
;; This function uses a non-reentrant model
;;
psect	text619
	file	"C:\Program Files\HI-TECH Software\PICC\9.83\sources\ftge.c"
	line	5
	global	__size_of___ftge
	__size_of___ftge	equ	__end_of___ftge-___ftge
	
___ftge:	
	opt	stack 5
; Regs used in ___ftge: [wreg+status,2+status,0]
	line	6
	
l7483:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfss	(___ftge@ff1+2),(23)&7
	goto	u3151
	goto	u3150
u3151:
	goto	l7487
u3150:
	line	7
	
l7485:	
	movlw	0
	movwf	((??___ftge+0)+0)
	movlw	0
	movwf	((??___ftge+0)+0+1)
	movlw	080h
	movwf	((??___ftge+0)+0+2)
	comf	(___ftge@ff1),w
	movwf	(??___ftge+3)+0
	comf	(___ftge@ff1+1),w
	movwf	((??___ftge+3)+0+1)
	comf	(___ftge@ff1+2),w
	movwf	((??___ftge+3)+0+2)
	incf	(??___ftge+3)+0,f
	skipnz
	incf	((??___ftge+3)+0+1),f
	skipnz
	incf	((??___ftge+3)+0+2),f
	movf	0+(??___ftge+3)+0,w
	addwf	(??___ftge+0)+0,f
	movf	1+(??___ftge+3)+0,w
	skipnc
	incfsz	1+(??___ftge+3)+0,w
	goto	u3161
	goto	u3162
u3161:
	addwf	(??___ftge+0)+1,f
	
u3162:
	movf	2+(??___ftge+3)+0,w
	skipnc
	incfsz	2+(??___ftge+3)+0,w
	goto	u3163
	goto	u3164
u3163:
	addwf	(??___ftge+0)+2,f
	
u3164:
	movf	0+(??___ftge+0)+0,w
	movwf	(___ftge@ff1)
	movf	1+(??___ftge+0)+0,w
	movwf	(___ftge@ff1+1)
	movf	2+(??___ftge+0)+0,w
	movwf	(___ftge@ff1+2)
	goto	l7487
	
l5800:	
	line	8
	
l7487:	
	btfss	(___ftge@ff2+2),(23)&7
	goto	u3171
	goto	u3170
u3171:
	goto	l7491
u3170:
	line	9
	
l7489:	
	movlw	0
	movwf	((??___ftge+0)+0)
	movlw	0
	movwf	((??___ftge+0)+0+1)
	movlw	080h
	movwf	((??___ftge+0)+0+2)
	comf	(___ftge@ff2),w
	movwf	(??___ftge+3)+0
	comf	(___ftge@ff2+1),w
	movwf	((??___ftge+3)+0+1)
	comf	(___ftge@ff2+2),w
	movwf	((??___ftge+3)+0+2)
	incf	(??___ftge+3)+0,f
	skipnz
	incf	((??___ftge+3)+0+1),f
	skipnz
	incf	((??___ftge+3)+0+2),f
	movf	0+(??___ftge+3)+0,w
	addwf	(??___ftge+0)+0,f
	movf	1+(??___ftge+3)+0,w
	skipnc
	incfsz	1+(??___ftge+3)+0,w
	goto	u3181
	goto	u3182
u3181:
	addwf	(??___ftge+0)+1,f
	
u3182:
	movf	2+(??___ftge+3)+0,w
	skipnc
	incfsz	2+(??___ftge+3)+0,w
	goto	u3183
	goto	u3184
u3183:
	addwf	(??___ftge+0)+2,f
	
u3184:
	movf	0+(??___ftge+0)+0,w
	movwf	(___ftge@ff2)
	movf	1+(??___ftge+0)+0,w
	movwf	(___ftge@ff2+1)
	movf	2+(??___ftge+0)+0,w
	movwf	(___ftge@ff2+2)
	goto	l7491
	
l5801:	
	line	10
	
l7491:	
	movlw	080h
	xorwf	(___ftge@ff1+2),f
	line	11
	
l7493:	
	movlw	080h
	xorwf	(___ftge@ff2+2),f
	line	12
	
l7495:	
	movf	(___ftge@ff2+2),w
	subwf	(___ftge@ff1+2),w
	skipz
	goto	u3195
	movf	(___ftge@ff2+1),w
	subwf	(___ftge@ff1+1),w
	skipz
	goto	u3195
	movf	(___ftge@ff2),w
	subwf	(___ftge@ff1),w
u3195:
	skipnc
	goto	u3191
	goto	u3190
u3191:
	goto	l7499
u3190:
	
l7497:	
	clrc
	
	goto	l5802
	
l7181:	
	
l7499:	
	setc
	
	goto	l5802
	
l7183:	
	goto	l5802
	
l7501:	
	line	13
	
l5802:	
	return
	opt stack 0
GLOBAL	__end_of___ftge
	__end_of___ftge:
;; =============== function ___ftge ends ============

	signat	___ftge,8312
	global	___fttol
psect	text620,local,class=CODE,delta=2
global __ptext620
__ptext620:

;; *************** function ___fttol *****************
;; Defined at:
;;		line 45 in file "C:\Program Files\HI-TECH Software\PICC\9.83\sources\fttol.c"
;; Parameters:    Size  Location     Type
;;  f1              3    0[BANK0 ] float 
;; Auto vars:     Size  Location     Type
;;  lval            4    9[BANK0 ] unsigned long 
;;  exp1            1   13[BANK0 ] unsigned char 
;;  sign1           1    8[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;                  4    0[BANK0 ] long 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       4       0       0       0
;;      Locals:         0       6       0       0       0
;;      Temps:          0       4       0       0       0
;;      Totals:         0      14       0       0       0
;;Total ram usage:       14 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_output_portd
;; This function uses a non-reentrant model
;;
psect	text620
	file	"C:\Program Files\HI-TECH Software\PICC\9.83\sources\fttol.c"
	line	45
	global	__size_of___fttol
	__size_of___fttol	equ	__end_of___fttol-___fttol
	
___fttol:	
	opt	stack 4
; Regs used in ___fttol: [wreg+status,2+status,0]
	line	49
	
l7911:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(___fttol@f1),w
	movwf	((??___fttol+0)+0)
	movf	(___fttol@f1+1),w
	movwf	((??___fttol+0)+0+1)
	movf	(___fttol@f1+2),w
	movwf	((??___fttol+0)+0+2)
	clrc
	rlf	(??___fttol+0)+1,w
	rlf	(??___fttol+0)+2,w
	movwf	(??___fttol+3)+0
	movf	(??___fttol+3)+0,w
	movwf	(___fttol@exp1)
	movf	((___fttol@exp1)),f
	skipz
	goto	u4001
	goto	u4000
u4001:
	goto	l7917
u4000:
	line	50
	
l7913:	
	movlw	0
	movwf	(?___fttol+3)
	movlw	0
	movwf	(?___fttol+2)
	movlw	0
	movwf	(?___fttol+1)
	movlw	0
	movwf	(?___fttol)

	goto	l5743
	
l7915:	
	goto	l5743
	
l5742:	
	line	51
	
l7917:	
	movf	(___fttol@f1),w
	movwf	((??___fttol+0)+0)
	movf	(___fttol@f1+1),w
	movwf	((??___fttol+0)+0+1)
	movf	(___fttol@f1+2),w
	movwf	((??___fttol+0)+0+2)
	movlw	017h
u4015:
	clrc
	rrf	(??___fttol+0)+2,f
	rrf	(??___fttol+0)+1,f
	rrf	(??___fttol+0)+0,f
u4010:
	addlw	-1
	skipz
	goto	u4015
	movf	0+(??___fttol+0)+0,w
	movwf	(??___fttol+3)+0
	movf	(??___fttol+3)+0,w
	movwf	(___fttol@sign1)
	line	52
	
l7919:	
	bsf	(___fttol@f1)+(15/8),(15)&7
	line	53
	
l7921:	
	movlw	0FFh
	andwf	(___fttol@f1),f
	movlw	0FFh
	andwf	(___fttol@f1+1),f
	movlw	0
	andwf	(___fttol@f1+2),f
	line	54
	
l7923:	
	movf	(___fttol@f1),w
	movwf	(___fttol@lval)
	movf	(___fttol@f1+1),w
	movwf	((___fttol@lval))+1
	movf	(___fttol@f1+2),w
	movwf	((___fttol@lval))+2
	clrf	((___fttol@lval))+3
	line	55
	
l7925:	
	movlw	low(08Eh)
	subwf	(___fttol@exp1),f
	line	56
	
l7927:	
	btfss	(___fttol@exp1),7
	goto	u4021
	goto	u4020
u4021:
	goto	l7937
u4020:
	line	57
	
l7929:	
	movf	(___fttol@exp1),w
	xorlw	80h
	addlw	-((-15)^80h)
	skipnc
	goto	u4031
	goto	u4030
u4031:
	goto	l7935
u4030:
	line	58
	
l7931:	
	movlw	0
	movwf	(?___fttol+3)
	movlw	0
	movwf	(?___fttol+2)
	movlw	0
	movwf	(?___fttol+1)
	movlw	0
	movwf	(?___fttol)

	goto	l5743
	
l7933:	
	goto	l5743
	
l5745:	
	goto	l7935
	line	59
	
l5746:	
	line	60
	
l7935:	
	movlw	01h
u4045:
	clrc
	rrf	(___fttol@lval+3),f
	rrf	(___fttol@lval+2),f
	rrf	(___fttol@lval+1),f
	rrf	(___fttol@lval),f
	addlw	-1
	skipz
	goto	u4045

	line	61
	movlw	(01h)
	movwf	(??___fttol+0)+0
	movf	(??___fttol+0)+0,w
	addwf	(___fttol@exp1),f
	btfss	status,2
	goto	u4051
	goto	u4050
u4051:
	goto	l7935
u4050:
	goto	l7947
	
l5747:	
	line	62
	goto	l7947
	
l5744:	
	line	63
	
l7937:	
	movlw	(018h)
	subwf	(___fttol@exp1),w
	skipc
	goto	u4061
	goto	u4060
u4061:
	goto	l7945
u4060:
	line	64
	
l7939:	
	movlw	0
	movwf	(?___fttol+3)
	movlw	0
	movwf	(?___fttol+2)
	movlw	0
	movwf	(?___fttol+1)
	movlw	0
	movwf	(?___fttol)

	goto	l5743
	
l7941:	
	goto	l5743
	
l5749:	
	line	65
	goto	l7945
	
l5751:	
	line	66
	
l7943:	
	movlw	01h
	movwf	(??___fttol+0)+0
u4075:
	clrc
	rlf	(___fttol@lval),f
	rlf	(___fttol@lval+1),f
	rlf	(___fttol@lval+2),f
	rlf	(___fttol@lval+3),f
	decfsz	(??___fttol+0)+0
	goto	u4075
	line	67
	movlw	low(01h)
	subwf	(___fttol@exp1),f
	goto	l7945
	line	68
	
l5750:	
	line	65
	
l7945:	
	movf	(___fttol@exp1),f
	skipz
	goto	u4081
	goto	u4080
u4081:
	goto	l7943
u4080:
	goto	l7947
	
l5752:	
	goto	l7947
	line	69
	
l5748:	
	line	70
	
l7947:	
	movf	(___fttol@sign1),w
	skipz
	goto	u4090
	goto	l7951
u4090:
	line	71
	
l7949:	
	comf	(___fttol@lval),f
	comf	(___fttol@lval+1),f
	comf	(___fttol@lval+2),f
	comf	(___fttol@lval+3),f
	incf	(___fttol@lval),f
	skipnz
	incf	(___fttol@lval+1),f
	skipnz
	incf	(___fttol@lval+2),f
	skipnz
	incf	(___fttol@lval+3),f
	goto	l7951
	
l5753:	
	line	72
	
l7951:	
	movf	(___fttol@lval+3),w
	movwf	(?___fttol+3)
	movf	(___fttol@lval+2),w
	movwf	(?___fttol+2)
	movf	(___fttol@lval+1),w
	movwf	(?___fttol+1)
	movf	(___fttol@lval),w
	movwf	(?___fttol)

	goto	l5743
	
l7953:	
	line	73
	
l5743:	
	return
	opt stack 0
GLOBAL	__end_of___fttol
	__end_of___fttol:
;; =============== function ___fttol ends ============

	signat	___fttol,4220
	global	___lmul
psect	text621,local,class=CODE,delta=2
global __ptext621
__ptext621:

;; *************** function ___lmul *****************
;; Defined at:
;;		line 3 in file "C:\Program Files\HI-TECH Software\PICC\9.83\sources\lmul.c"
;; Parameters:    Size  Location     Type
;;  multiplier      4    0[BANK0 ] unsigned long 
;;  multiplicand    4    4[BANK0 ] unsigned long 
;; Auto vars:     Size  Location     Type
;;  product         4    8[BANK0 ] unsigned long 
;; Return value:  Size  Location     Type
;;                  4    0[BANK0 ] unsigned long 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       8       0       0       0
;;      Locals:         0       4       0       0       0
;;      Temps:          1       0       0       0       0
;;      Totals:         1      12       0       0       0
;;Total ram usage:       13 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_PIDcal
;; This function uses a non-reentrant model
;;
psect	text621
	file	"C:\Program Files\HI-TECH Software\PICC\9.83\sources\lmul.c"
	line	3
	global	__size_of___lmul
	__size_of___lmul	equ	__end_of___lmul-___lmul
	
___lmul:	
	opt	stack 4
; Regs used in ___lmul: [wreg+status,2+status,0]
	line	4
	
l7807:	
	movlw	0
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(___lmul@product+3)
	movlw	0
	movwf	(___lmul@product+2)
	movlw	0
	movwf	(___lmul@product+1)
	movlw	0
	movwf	(___lmul@product)

	goto	l7809
	line	6
	
l5708:	
	line	7
	
l7809:	
	btfss	(___lmul@multiplier),(0)&7
	goto	u3731
	goto	u3730
u3731:
	goto	l7813
u3730:
	line	8
	
l7811:	
	movf	(___lmul@multiplicand),w
	addwf	(___lmul@product),f
	movf	(___lmul@multiplicand+1),w
	clrz
	skipnc
	addlw	1
	skipnz
	goto	u3741
	addwf	(___lmul@product+1),f
u3741:
	movf	(___lmul@multiplicand+2),w
	clrz
	skipnc
	addlw	1
	skipnz
	goto	u3742
	addwf	(___lmul@product+2),f
u3742:
	movf	(___lmul@multiplicand+3),w
	clrz
	skipnc
	addlw	1
	skipnz
	goto	u3743
	addwf	(___lmul@product+3),f
u3743:

	goto	l7813
	
l5709:	
	line	9
	
l7813:	
	movlw	01h
	movwf	(??___lmul+0)+0
u3755:
	clrc
	rlf	(___lmul@multiplicand),f
	rlf	(___lmul@multiplicand+1),f
	rlf	(___lmul@multiplicand+2),f
	rlf	(___lmul@multiplicand+3),f
	decfsz	(??___lmul+0)+0
	goto	u3755
	line	10
	
l7815:	
	movlw	01h
u3765:
	clrc
	rrf	(___lmul@multiplier+3),f
	rrf	(___lmul@multiplier+2),f
	rrf	(___lmul@multiplier+1),f
	rrf	(___lmul@multiplier),f
	addlw	-1
	skipz
	goto	u3765

	line	11
	movf	(___lmul@multiplier+3),w
	iorwf	(___lmul@multiplier+2),w
	iorwf	(___lmul@multiplier+1),w
	iorwf	(___lmul@multiplier),w
	skipz
	goto	u3771
	goto	u3770
u3771:
	goto	l7809
u3770:
	goto	l7817
	
l5710:	
	line	12
	
l7817:	
	movf	(___lmul@product+3),w
	movwf	(?___lmul+3)
	movf	(___lmul@product+2),w
	movwf	(?___lmul+2)
	movf	(___lmul@product+1),w
	movwf	(?___lmul+1)
	movf	(___lmul@product),w
	movwf	(?___lmul)

	goto	l5711
	
l7819:	
	line	13
	
l5711:	
	return
	opt stack 0
GLOBAL	__end_of___lmul
	__end_of___lmul:
;; =============== function ___lmul ends ============

	signat	___lmul,8316
	global	___ftpack
psect	text622,local,class=CODE,delta=2
global __ptext622
__ptext622:

;; *************** function ___ftpack *****************
;; Defined at:
;;		line 63 in file "C:\Program Files\HI-TECH Software\PICC\9.83\sources\float.c"
;; Parameters:    Size  Location     Type
;;  arg             3    0[BANK0 ] unsigned um
;;  exp             1    3[BANK0 ] unsigned char 
;;  sign            1    4[BANK0 ] unsigned char 
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;                  3    0[BANK0 ] float 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       5       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       3       0       0       0
;;      Totals:         0       8       0       0       0
;;Total ram usage:        8 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		___lbtoft
;;		___ftadd
;;		___ftdiv
;;		___ftmul
;;		___abtoft
;;		___awtoft
;;		___lwtoft
;;		___altoft
;;		___lltoft
;;		___attoft
;;		___lttoft
;; This function uses a non-reentrant model
;;
psect	text622
	file	"C:\Program Files\HI-TECH Software\PICC\9.83\sources\float.c"
	line	63
	global	__size_of___ftpack
	__size_of___ftpack	equ	__end_of___ftpack-___ftpack
	
___ftpack:	
	opt	stack 4
; Regs used in ___ftpack: [wreg+status,2+status,0]
	line	64
	
l7777:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(___ftpack@exp),w
	skipz
	goto	u3620
	goto	l7781
u3620:
	
l7779:	
	movf	(___ftpack@arg+2),w
	iorwf	(___ftpack@arg+1),w
	iorwf	(___ftpack@arg),w
	skipz
	goto	u3631
	goto	u3630
u3631:
	goto	l7787
u3630:
	goto	l7781
	
l5967:	
	line	65
	
l7781:	
	movlw	0x0
	movwf	(?___ftpack)
	movlw	0x0
	movwf	(?___ftpack+1)
	movlw	0x0
	movwf	(?___ftpack+2)
	goto	l5968
	
l7783:	
	goto	l5968
	
l5965:	
	line	66
	goto	l7787
	
l5970:	
	line	67
	
l7785:	
	movlw	(01h)
	movwf	(??___ftpack+0)+0
	movf	(??___ftpack+0)+0,w
	addwf	(___ftpack@exp),f
	line	68
	movlw	01h
u3645:
	clrc
	rrf	(___ftpack@arg+2),f
	rrf	(___ftpack@arg+1),f
	rrf	(___ftpack@arg),f
	addlw	-1
	skipz
	goto	u3645

	goto	l7787
	line	69
	
l5969:	
	line	66
	
l7787:	
	movlw	low highword(0FE0000h)
	andwf	(___ftpack@arg+2),w
	btfss	status,2
	goto	u3651
	goto	u3650
u3651:
	goto	l7785
u3650:
	goto	l5972
	
l5971:	
	line	70
	goto	l5972
	
l5973:	
	line	71
	
l7789:	
	movlw	(01h)
	movwf	(??___ftpack+0)+0
	movf	(??___ftpack+0)+0,w
	addwf	(___ftpack@exp),f
	line	72
	
l7791:	
	movlw	01h
	addwf	(___ftpack@arg),f
	movlw	0
	skipnc
movlw 1
	addwf	(___ftpack@arg+1),f
	movlw	0
	skipnc
movlw 1
	addwf	(___ftpack@arg+2),f
	line	73
	
l7793:	
	movlw	01h
u3665:
	clrc
	rrf	(___ftpack@arg+2),f
	rrf	(___ftpack@arg+1),f
	rrf	(___ftpack@arg),f
	addlw	-1
	skipz
	goto	u3665

	line	74
	
l5972:	
	line	70
	movlw	low highword(0FF0000h)
	andwf	(___ftpack@arg+2),w
	btfss	status,2
	goto	u3671
	goto	u3670
u3671:
	goto	l7789
u3670:
	goto	l7797
	
l5974:	
	line	75
	goto	l7797
	
l5976:	
	line	76
	
l7795:	
	movlw	low(01h)
	subwf	(___ftpack@exp),f
	line	77
	movlw	01h
u3685:
	clrc
	rlf	(___ftpack@arg),f
	rlf	(___ftpack@arg+1),f
	rlf	(___ftpack@arg+2),f
	addlw	-1
	skipz
	goto	u3685
	goto	l7797
	line	78
	
l5975:	
	line	75
	
l7797:	
	btfss	(___ftpack@arg+1),(15)&7
	goto	u3691
	goto	u3690
u3691:
	goto	l7795
u3690:
	
l5977:	
	line	79
	btfsc	(___ftpack@exp),(0)&7
	goto	u3701
	goto	u3700
u3701:
	goto	l5978
u3700:
	line	80
	
l7799:	
	movlw	0FFh
	andwf	(___ftpack@arg),f
	movlw	07Fh
	andwf	(___ftpack@arg+1),f
	movlw	0FFh
	andwf	(___ftpack@arg+2),f
	
l5978:	
	line	81
	clrc
	rrf	(___ftpack@exp),f

	line	82
	
l7801:	
	movf	(___ftpack@exp),w
	movwf	((??___ftpack+0)+0)
	clrf	((??___ftpack+0)+0+1)
	clrf	((??___ftpack+0)+0+2)
	movlw	010h
u3715:
	clrc
	rlf	(??___ftpack+0)+0,f
	rlf	(??___ftpack+0)+1,f
	rlf	(??___ftpack+0)+2,f
u3710:
	addlw	-1
	skipz
	goto	u3715
	movf	0+(??___ftpack+0)+0,w
	iorwf	(___ftpack@arg),f
	movf	1+(??___ftpack+0)+0,w
	iorwf	(___ftpack@arg+1),f
	movf	2+(??___ftpack+0)+0,w
	iorwf	(___ftpack@arg+2),f
	line	83
	
l7803:	
	movf	(___ftpack@sign),w
	skipz
	goto	u3720
	goto	l5979
u3720:
	line	84
	
l7805:	
	bsf	(___ftpack@arg)+(23/8),(23)&7
	
l5979:	
	line	85
	line	86
	
l5968:	
	return
	opt stack 0
GLOBAL	__end_of___ftpack
	__end_of___ftpack:
;; =============== function ___ftpack ends ============

	signat	___ftpack,12411
	global	_adc_convert
psect	text623,local,class=CODE,delta=2
global __ptext623
__ptext623:

;; *************** function _adc_convert *****************
;; Defined at:
;;		line 65 in file "C:\pic_projects\PID CONTROL\adc_func.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  result          2    8[BANK0 ] unsigned short 
;;  adresl          2    6[BANK0 ] unsigned short 
;;  adresh          2    4[BANK0 ] unsigned short 
;; Return value:  Size  Location     Type
;;                  2    0[BANK0 ] unsigned short 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       2       0       0       0
;;      Locals:         0       6       0       0       0
;;      Temps:          0       2       0       0       0
;;      Totals:         0      10       0       0       0
;;Total ram usage:       10 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_get_setpoint
;;		_get_position
;; This function uses a non-reentrant model
;;
psect	text623
	file	"C:\pic_projects\PID CONTROL\adc_func.c"
	line	65
	global	__size_of_adc_convert
	__size_of_adc_convert	equ	__end_of_adc_convert-_adc_convert
	
_adc_convert:	
	opt	stack 4
; Regs used in _adc_convert: [wreg+status,2+status,0]
	line	69
	
l7695:	
;adc_func.c: 66: uint16_t adresh;
;adc_func.c: 67: uint16_t adresl;
;adc_func.c: 68: uint16_t result;
;adc_func.c: 69: adc_isr_flag = 0;
	clrf	(_adc_isr_flag)
	clrf	(_adc_isr_flag+1)
	line	71
	
l7697:	
;adc_func.c: 71: GODONE = 1;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	bsf	(249/8),(249)&7
	line	73
;adc_func.c: 73: while (adc_isr_flag == 0)
	goto	l7699
	
l1723:	
	goto	l7699
	line	74
;adc_func.c: 74: {}
	
l1722:	
	line	73
	
l7699:	
	movf	(_adc_isr_flag+1),w
	iorwf	(_adc_isr_flag),w
	skipnz
	goto	u3371
	goto	u3370
u3371:
	goto	l7699
u3370:
	goto	l7701
	
l1724:	
	line	75
	
l7701:	
;adc_func.c: 75: adresl = ADRESL;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movf	(158)^080h,w	;volatile
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_adc_convert+0)+0
	clrf	(??_adc_convert+0)+0+1
	movf	0+(??_adc_convert+0)+0,w
	movwf	(adc_convert@adresl)
	movf	1+(??_adc_convert+0)+0,w
	movwf	(adc_convert@adresl+1)
	line	76
;adc_func.c: 76: adresh = ADRESH;
	movf	(30),w	;volatile
	movwf	(??_adc_convert+0)+0
	clrf	(??_adc_convert+0)+0+1
	movf	0+(??_adc_convert+0)+0,w
	movwf	(adc_convert@adresh)
	movf	1+(??_adc_convert+0)+0,w
	movwf	(adc_convert@adresh+1)
	line	77
;adc_func.c: 77: result = ((adresh << 8) |(adresl));
	movf	(adc_convert@adresh),w
	movwf	(??_adc_convert+0)+0+1
	clrf	(??_adc_convert+0)+0
	movf	(adc_convert@adresl),w
	iorwf	0+(??_adc_convert+0)+0,w
	movwf	(adc_convert@result)
	movf	(adc_convert@adresl+1),w
	iorwf	1+(??_adc_convert+0)+0,w
	movwf	1+(adc_convert@result)
	line	79
	
l7703:	
;adc_func.c: 79: return result;
	movf	(adc_convert@result+1),w
	clrf	(?_adc_convert+1)
	addwf	(?_adc_convert+1)
	movf	(adc_convert@result),w
	clrf	(?_adc_convert)
	addwf	(?_adc_convert)

	goto	l1725
	
l7705:	
	line	80
	
l1725:	
	return
	opt stack 0
GLOBAL	__end_of_adc_convert
	__end_of_adc_convert:
;; =============== function _adc_convert ends ============

	signat	_adc_convert,90
	global	_absolute_value
psect	text624,local,class=CODE,delta=2
global __ptext624
__ptext624:

;; *************** function _absolute_value *****************
;; Defined at:
;;		line 140 in file "C:\pic_projects\PID CONTROL\main.c"
;; Parameters:    Size  Location     Type
;;  value           1    wreg     char 
;; Auto vars:     Size  Location     Type
;;  value           1    1[BANK0 ] char 
;;  result          1    0[BANK0 ] char 
;; Return value:  Size  Location     Type
;;                  1    wreg      char 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       2       0       0       0
;;      Temps:          1       0       0       0       0
;;      Totals:         1       2       0       0       0
;;Total ram usage:        3 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_output_portd
;; This function uses a non-reentrant model
;;
psect	text624
	file	"C:\pic_projects\PID CONTROL\main.c"
	line	140
	global	__size_of_absolute_value
	__size_of_absolute_value	equ	__end_of_absolute_value-_absolute_value
	
_absolute_value:	
	opt	stack 4
; Regs used in _absolute_value: [wreg+status,2+status,0]
;absolute_value@value stored from wreg
	line	143
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(absolute_value@value)
	
l7687:	
;main.c: 141: signed char result;
;main.c: 143: if (value < 0)
	btfss	(absolute_value@value),7
	goto	u3361
	goto	u3360
u3361:
	goto	l7691
u3360:
	line	145
	
l7689:	
;main.c: 144: {
;main.c: 145: result = ~value + 1;
	decf	(absolute_value@value),w
	xorlw	0ffh
	movwf	(??_absolute_value+0)+0
	movf	(??_absolute_value+0)+0,w
	movwf	(absolute_value@result)
	line	146
;main.c: 146: }
	goto	l876
	line	147
	
l875:	
	line	149
	
l7691:	
;main.c: 147: else
;main.c: 148: {
;main.c: 149: result = value;
	movf	(absolute_value@value),w
	movwf	(??_absolute_value+0)+0
	movf	(??_absolute_value+0)+0,w
	movwf	(absolute_value@result)
	line	150
	
l876:	
	line	152
;main.c: 150: }
;main.c: 152: return result;
	movf	(absolute_value@result),w
	goto	l877
	
l7693:	
	line	153
	
l877:	
	return
	opt stack 0
GLOBAL	__end_of_absolute_value
	__end_of_absolute_value:
;; =============== function _absolute_value ends ============

	signat	_absolute_value,4217
	global	_pwm_init
psect	text625,local,class=CODE,delta=2
global __ptext625
__ptext625:

;; *************** function _pwm_init *****************
;; Defined at:
;;		line 7 in file "C:\pic_projects\PID CONTROL\pwm.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text625
	file	"C:\pic_projects\PID CONTROL\pwm.c"
	line	7
	global	__size_of_pwm_init
	__size_of_pwm_init	equ	__end_of_pwm_init-_pwm_init
	
_pwm_init:	
	opt	stack 5
; Regs used in _pwm_init: [wreg]
	line	8
	
l7187:	
;pwm.c: 8: PR2 = 0x60;
	movlw	(060h)
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movwf	(146)^080h	;volatile
	line	9
;pwm.c: 9: T2CON = 0x06;
	movlw	(06h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(18)	;volatile
	line	10
;pwm.c: 10: CCP1CON = 0x0C;
	movlw	(0Ch)
	movwf	(23)	;volatile
	line	12
	
l3797:	
	return
	opt stack 0
GLOBAL	__end_of_pwm_init
	__end_of_pwm_init:
;; =============== function _pwm_init ends ============

	signat	_pwm_init,88
	global	_adc_init
psect	text626,local,class=CODE,delta=2
global __ptext626
__ptext626:

;; *************** function _adc_init *****************
;; Defined at:
;;		line 111 in file "C:\pic_projects\PID CONTROL\adc_func.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text626
	file	"C:\pic_projects\PID CONTROL\adc_func.c"
	line	111
	global	__size_of_adc_init
	__size_of_adc_init	equ	__end_of_adc_init-_adc_init
	
_adc_init:	
	opt	stack 5
; Regs used in _adc_init: [wreg]
	line	112
	
l7185:	
;adc_func.c: 112: TRISA = 0x06;
	movlw	(06h)
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movwf	(133)^080h	;volatile
	line	113
;adc_func.c: 113: ANSEL = 0x06;
	movlw	(06h)
	bsf	status, 5	;RP0=1, select bank3
	bsf	status, 6	;RP1=1, select bank3
	movwf	(392)^0180h	;volatile
	line	114
;adc_func.c: 114: ADCON0 = 0xC1;
	movlw	(0C1h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(31)	;volatile
	line	115
;adc_func.c: 115: ADCON1 = 0x80;
	movlw	(080h)
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movwf	(159)^080h	;volatile
	line	116
	
l1734:	
	return
	opt stack 0
GLOBAL	__end_of_adc_init
	__end_of_adc_init:
;; =============== function _adc_init ends ============

	signat	_adc_init,88
	global	_interrupt_handler
psect	text627,local,class=CODE,delta=2
global __ptext627
__ptext627:

;; *************** function _interrupt_handler *****************
;; Defined at:
;;		line 7 in file "C:\pic_projects\PID CONTROL\interrupt.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          4       0       0       0       0
;;      Totals:         4       0       0       0       0
;;Total ram usage:        4 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		_adc_isr
;; This function is called by:
;;		Interrupt level 1
;; This function uses a non-reentrant model
;;
psect	text627
	file	"C:\pic_projects\PID CONTROL\interrupt.c"
	line	7
	global	__size_of_interrupt_handler
	__size_of_interrupt_handler	equ	__end_of_interrupt_handler-_interrupt_handler
	
_interrupt_handler:	
	opt	stack 4
; Regs used in _interrupt_handler: [wreg+status,2+status,0+pclath+cstack]
psect	intentry,class=CODE,delta=2
global __pintentry
__pintentry:
global interrupt_function
interrupt_function:
	global saved_w
	saved_w	set	btemp+0
	movwf	saved_w
	swapf	status,w
	movwf	(??_interrupt_handler+0)
	movf	fsr0,w
	movwf	(??_interrupt_handler+1)
	movf	pclath,w
	movwf	(??_interrupt_handler+2)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	btemp+1,w
	movwf	(??_interrupt_handler+3)
	ljmp	_interrupt_handler
psect	text627
	line	9
	
i1l7217:	
;interrupt.c: 9: adc_isr();
	fcall	_adc_isr
	line	11
	
i1l2770:	
	movf	(??_interrupt_handler+3),w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	btemp+1
	movf	(??_interrupt_handler+2),w
	movwf	pclath
	movf	(??_interrupt_handler+1),w
	movwf	fsr0
	swapf	(??_interrupt_handler+0)^0FFFFFF80h,w
	movwf	status
	swapf	saved_w,f
	swapf	saved_w,w
	retfie
	opt stack 0
GLOBAL	__end_of_interrupt_handler
	__end_of_interrupt_handler:
;; =============== function _interrupt_handler ends ============

	signat	_interrupt_handler,88
	global	_adc_isr
psect	text628,local,class=CODE,delta=2
global __ptext628
__ptext628:

;; *************** function _adc_isr *****************
;; Defined at:
;;		line 119 in file "C:\pic_projects\PID CONTROL\adc_func.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_interrupt_handler
;; This function uses a non-reentrant model
;;
psect	text628
	file	"C:\pic_projects\PID CONTROL\adc_func.c"
	line	119
	global	__size_of_adc_isr
	__size_of_adc_isr	equ	__end_of_adc_isr-_adc_isr
	
_adc_isr:	
	opt	stack 4
; Regs used in _adc_isr: [wreg]
	line	120
	
i1l7209:	
;adc_func.c: 120: if ((ADIF)&&(ADIE))
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfss	(102/8),(102)&7
	goto	u239_21
	goto	u239_20
u239_21:
	goto	i1l1739
u239_20:
	
i1l7211:	
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	btfss	(1126/8)^080h,(1126)&7
	goto	u240_21
	goto	u240_20
u240_21:
	goto	i1l1739
u240_20:
	line	123
	
i1l7213:	
;adc_func.c: 121: {
;adc_func.c: 123: ADIF=0;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	bcf	(102/8),(102)&7
	line	124
	
i1l7215:	
;adc_func.c: 124: adc_isr_flag=1;
	movlw	low(01h)
	movwf	(_adc_isr_flag)
	movlw	high(01h)
	movwf	((_adc_isr_flag))+1
	line	125
;adc_func.c: 125: int_count++;
	movlw	low(01h)
	addwf	(_int_count),f
	skipnc
	incf	(_int_count+1),f
	movlw	high(01h)
	addwf	(_int_count+1),f
	line	126
;adc_func.c: 126: }
	goto	i1l1739
	line	127
	
i1l1737:	
	goto	i1l1739
	line	129
;adc_func.c: 127: else
;adc_func.c: 128: {
	
i1l1738:	
	line	130
	
i1l1739:	
	return
	opt stack 0
GLOBAL	__end_of_adc_isr
	__end_of_adc_isr:
;; =============== function _adc_isr ends ============

	signat	_adc_isr,88
psect	text629,local,class=CODE,delta=2
global __ptext629
__ptext629:
	global	btemp
	btemp set 07Eh

	DABS	1,126,2	;btemp
	global	wtemp0
	wtemp0 set btemp
	end
