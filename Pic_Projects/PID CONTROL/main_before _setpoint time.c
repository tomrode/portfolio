/***************************************************************/          
/* PID CONTROLL 27 MAR 2012                                    */
/***************************************************************/
#include <htc.h>
#include "typedefs.h"
#include "pid.h"
#include "float.h"
#include "adc_func.h"
#include "pwm.h"
#include "timer.h"
/**********************************************************/
/* Device Configuration
/**********************************************************/
     
   
      /*PIC16F887*/
__CONFIG(DEBUGEN & LVPEN & FCMEN & IESODIS & BORXSLP & DUNPROTECT & UNPROTECT & MCLREN & PWRTEN & WDTDIS & HS);
__CONFIG(BORV40);  //HS means resonator 8Mhz      
      
 /* Global define for this module*/      
#define ABS_H
#define PID_LOOP_TIME (160)                            /* PID cylce times*/
/**********************************************************/
/* Function Prototypes 
/**********************************************************/
void init(void);                  /* SFR inits*/
void output_portd(float num);     /* test the type cast conversion*/
//void output_portd(uint8_t num);
signed char absolute_value(signed char value);

/***********************************************************/
/* Variable Definition
/***********************************************************/

int main(void)
{
/************************************************************/
/* LOCAL MAIN VARIABLES                                     */
/************************************************************/
//float setpoint = 10.0;
uint8_t setpoint;
//float actual_position = 0.0;
uint8_t actual_position;
uint8_t pid_cal_result;
//float pid_cal_result;

/***********************************************************/
/* INITIALIZATIONS                                         */
/***********************************************************/ 


init();                                                     /* Initialize SFR*/                                            
adc_init();                                                 /* Initialize adc*/
pwm_init();                                                 /* Initialize PWM*/
TMR1IE = 1;                                                 /* Timer 1 interrupt enable*/
ADIE = 1;                                                   /* AtD interrupt enable*/
PEIE = 1;                                                   /* enable all peripheral interrupts*/
ei();                                                       /* enable all interrupts*/

timer_set(PID_TIMER,10);                                    /* initialize TIMER1*/                                                          


/************************************************************/
/*  MAIN LOOP OVERALL                                       */
/************************************************************/
while(1)                   
{  
  
  if (TO == 1)                                                           /*STATUS reg, bit 4 1=After power up,CLRWDT or sleep instruction 0=A WDT time-out occured*/ 
   {
    //RC6 = 0;                                                          /* Test loop time*/ 
   if (get_timer(PID_TIMER) == 0)
    {
    asm("nop");
    
    setpoint = get_setpoint();  
    actual_position = get_position(); 
    RC5 = 1;                                                           /* Test function call time*/
    pid_cal_result = PIDcal(setpoint, actual_position);                /* 5.46 ms functional call time*/ 
    RC5 = 0;                                                           /* Test function call time*/
    output_portd(pid_cal_result);                                      /*this changes its type*/ 
   
    timer_set(PID_TIMER,PID_LOOP_TIME);
    RC6 = 1;                                                           /* Test loop time*/
    } 
     else
    {
     RC6 = 0;                                                          /* Test loop time*/
    }
    //_delay(200000);                                                  /*Amount of instruction cycles*/
   
 } 
else
     {
       CLRWDT();                                                         // Clear watchdog timer
     } 

   }
} 


void init(void)
{
 OPTION     = 0x8F;                    /*|PSA=Prescale to WDT|most prescale rate|PS2=1|PS1=1|PS0=1| 20ms/Div*/
 TRISC      = 0x00;                    /* used for RC1 (ccp2)*/
 TRISD      = 0x00;                    /* port directions: 1=input, 0=output*/
 PORTB      = 0x00;
 PORTD      = 0xFF;                    /* port D all off*/ 
//PIE1       = 0x01;                   /* TMR1IE = enabled*/ 
//INTCON     = 0x40;                   /* PEIE = enabled*/
 T1CON      = 0x35;                    /* T1CKPS1,T1CKPS0 = 1:8 prescaler,T1SYNC = do not synchronise external clock input ,TMR1ON = enabled*/ 
  _delay(800000); 

}


void output_portd(float num)
//void output_portd(uint8_t num)
{
signed char test;
  if(num <= -0.00000000)             /* Check for a negative sign*/
      {
       RC0 = 0;                      /* indicate a negative sign*/
       asm("nop");                   /* negative result*/
       test = (signed char)(num);    /*this changes its type to output as 8 bit value */ 
       PORTD = absolute_value(test); /*-43*/
       CCPR1L = 0;                   /* This is the new pulse width spot*/
       asm("nop");
      }
     else
      {
       RC0 = 1;                      /* indicate a positive sign*/
       PORTD = (char)(num);          /*this changes its type to output as 8 bit value */ 
       CCPR1L = PORTD;               /* This is the new pulse width spot*/
       asm("nop");                   /* positive result*/
      }

asm("nop");
} 



#ifdef ABS_H

signed char absolute_value(signed char value)
{
  signed char result;

  if (value < 0)
  {
    result = ~value + 1;
  }
  else
  {
    result = value;
  }

  return result;
}
#endif