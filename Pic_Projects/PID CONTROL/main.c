/***************************************************************/          
/* PID CONTROLL 27 MAR 2012                                    */
/***************************************************************/
#include <htc.h>
#include "typedefs.h"
#include "pid.h"
#include "float.h"
#include "adc_func.h"
#include "pwm.h"
#include "timer.h"
/**********************************************************/
/* Device Configuration
/**********************************************************/
     
   
      /*PIC16F887*/
__CONFIG(DEBUGEN & LVPEN & FCMEN & IESODIS & BORXSLP & DUNPROTECT & UNPROTECT & MCLREN & PWRTEN & WDTDIS & HS);
__CONFIG(BORV40);  //HS means resonator 8Mhz      
      
 /* Global define for this module*/      
#define TEST             ((.1)*20)                            /* would'nt do ((10/100)*20)*/
#define ABS_H
#define PID_LOOP_TIME    (160)                            /* PID cycle times in ms*/
#define HIGH_SET_POINT   (0xA1)                           /* 3.10 volts NTC or 100 degrees F*/
#define MID_SET_POINT    (0x82)                           /* 2.50 volts NTC or 90  degrees F*/
#define LOW_SET_POINT    (0x6E)                           /* 2.12 volts NTC or 85  degrees F */
#define HEAT_MILLISEC    (1000)                           /* Used for the step down timer for set desired point heat incremented of milliseconds*/
#define MAX_HIGH_SECONDS (600)                            /* Around 10 minutes at 100 F then step down*/
#define MAX_MID_SECONDS  (1200)                           /* Around 10 minutes at 90 F then step down*/
//#define ENABLE
/**********************************************************/
/* Function Prototypes 
/**********************************************************/
void init(void);                  /* SFR inits*/
void output_portd(float num);     /* test the type cast conversion*/
//void output_portd(uint8_t num);
signed char absolute_value(signed char value);

/***********************************************************/
/* Variable Definition
/***********************************************************/
enum
   {
   test_enum1,
   test_enum2
   }TEST_UNUM;
int main(void)
{
/************************************************************/
/* LOCAL MAIN VARIABLES                                     */
/************************************************************/
//float setpoint = 10.0;
uint8_t setpoint;
//float actual_position = 0.0;
uint8_t actual_position;
uint8_t pid_cal_result;
//float pid_cal_result;
uint8_t max_time_exceeded;
uint16_t max_time_exceeded_counter;
static uint8_t test_timer;
uint16_t test;
uint8_t testenum;
//test = 0x5593;
test =  (unsigned short)((signed char)(0x5593));

 
/***********************************************************/
/* INITIALIZATIONS                                         */
/***********************************************************/ 
#ifdef ENABLE
test = 0;
#else
test=99;
#endif

testenum = test_enum1;
testenum = test_enum2;

init();                                                     /* Initialize SFR*/                                            
adc_init();                                                 /* Initialize adc*/
pwm_init();                                                 /* Initialize PWM*/
TMR1IE = 1;                                                 /* Timer 1 interrupt enable*/
ADIE = 1;                                                   /* AtD interrupt enable*/
PEIE = 1;                                                   /* enable all peripheral interrupts*/
ei();                                                       /* enable all interrupts*/
test_timer=1;
timer_set(PID_TIMER,10);                                    /* initialize PID Timer*/                                                          
timer_set(HEAT_SETTING,10);                                 /*high setting first*/
max_time_exceeded = FALSE;
max_time_exceeded_counter=0;                           
/************************************************************/
/*  MAIN LOOP OVERALL                                       */
/************************************************************/
while(1)                   
{  
  
  if (TO == 1)                                                          /*STATUS reg, bit 4 1=After power up,CLRWDT or sleep instruction 0=A WDT time-out occured*/ 
   {
    //RC6 = 0;                                                          /* Test loop time*/ 
   if (get_timer(PID_TIMER) == 0)
    {
    if ((get_timer(HEAT_SETTING) == 0) && (max_time_exceeded == FALSE))
       {
         setpoint = HIGH_SET_POINT;                                            /* 100 degrees =0xA1 old way-> from pot value get_setpoint();*/  
         timer_set(HEAT_SETTING,HEAT_MILLISEC);
         max_time_exceeded_counter++;
         if (max_time_exceeded_counter >= MAX_HIGH_SECONDS)                    /*600 seconds or about 10 minutes*/
            {
              setpoint = MID_SET_POINT;                                        /* 90 Degrees = 0x82*/ 
              max_time_exceeded = FALSE; 
              asm("nop");      
            }
            if (max_time_exceeded_counter >= MAX_MID_SECONDS)                  /*1200 seconds or about 20 minutes*/
            {
              setpoint = LOW_SET_POINT;                                        /* 85 Degrees = 0x6E*/  
              max_time_exceeded = TRUE;                                        /* Keep it set here */
              asm("nop");   
            }
           else
            {
             /* Do Nothing*/
            }      
      }
     else
       {
         /* Do nothing*/      
       }

    actual_position = get_position(); 
    RC5 = 1;                                                           /* Test function call time*/
    pid_cal_result = PIDcal(100, 0);
     pid_cal_result = PIDcal(100, 20);
      pid_cal_result = PIDcal(100, 30);
       pid_cal_result = PIDcal(100, 50);
        pid_cal_result = PIDcal(100, 70);
         pid_cal_result = PIDcal(100, 85);
          pid_cal_result = PIDcal(100, 90);
           pid_cal_result = PIDcal(100, 95);
            pid_cal_result = PIDcal(100, 98);
             pid_cal_result = PIDcal(100, 98);
              pid_cal_result = PIDcal(100, 99);
               pid_cal_result = PIDcal(100, 100);
                pid_cal_result = PIDcal(100, 102);
                 pid_cal_result = PIDcal(100, 104);
                  pid_cal_result = PIDcal(100, 95);
                   pid_cal_result = PIDcal(100, 98);
                    pid_cal_result = PIDcal(100, 99);
                     pid_cal_result = PIDcal(100, 99);
                      pid_cal_result = PIDcal(100, 99);
                       pid_cal_result = PIDcal(100, 100);
                        pid_cal_result = PIDcal(100, 100);
                         pid_cal_result = PIDcal(100, 100);
                          pid_cal_result = PIDcal(100, 100);
                           pid_cal_result = PIDcal(100, 100);
                            pid_cal_result = PIDcal(100, 100);
                             pid_cal_result = PIDcal(100, 100);
                              pid_cal_result = PIDcal(100, 100);
                               pid_cal_result = PIDcal(100, 100);
                               pid_cal_result = PIDcal(100, 100);
                               pid_cal_result = PIDcal(100, 100);
                               pid_cal_result = PIDcal(100, 100);
                               pid_cal_result = PIDcal(100, 100);
                               pid_cal_result = PIDcal(100, 101);
                               pid_cal_result = PIDcal(100, 100);

    pid_cal_result = PIDcal(setpoint, actual_position);                                   /* 5.46 ms functional call time*/ 
    RC5 = 0;                                                           /* Test function call time*/
    PORTD = pid_cal_result;                                            /* PWM result to PORTD*/
    CCPR1L = PORTD;                                                    /* Output it to PWM module*/
    if (PORTD == 0)
     {
         RC0 = 1;                                                       /* indicate a zero in the off period or step down*/
     }
     else
     {
       RC0 = 0;
     }
    #ifdef OUTPUT_FLOAT_PORTD
    output_portd(pid_cal_result);                                      /*this changes its type*/ 
    #endif
    timer_set(PID_TIMER,PID_LOOP_TIME);
    RC6 = 1;                                                           /* Test loop time*/
    } 
     else
    {
     RC6 = 0;                                                          /* Test loop time*/
    }
    //_delay(200000);                                                  /*Amount of instruction cycles*/
   
 } 
else
     {
       CLRWDT();                                                         // Clear watchdog timer
     } 

   }
} 


void init(void)
{
 OPTION     = 0x8F;                    /*|PSA=Prescale to WDT|most prescale rate|PS2=1|PS1=1|PS0=1| 20ms/Div*/
 TRISC      = 0x00;                    /* used for RC1 (ccp2)*/
 TRISD      = 0x00;                    /* port directions: 1=input, 0=output*/
 PORTB      = 0x00;
 PORTD      = 0xFF;                    /* port D all on*/ 
//PIE1       = 0x01;                   /* TMR1IE = enabled*/ 
//INTCON     = 0x40;                   /* PEIE = enabled*/
 T1CON      = 0x35;                    /* T1CKPS1,T1CKPS0 = 1:8 prescaler,T1SYNC = do not synchronise external clock input ,TMR1ON = enabled*/ 
 // _delay(800000); 
 while(1)
  {
  PORTD ^= 0xff;
  _delay(800000); 
  }
}

#ifdef OUTPUT_FLOAT_PORTD
void output_portd(float num)
//void output_portd(uint8_t num)
{
signed char test;
  if(num <= -0.00000000)             /* Check for a negative sign*/
      {
       RC0 = 0;                      /* indicate a negative sign*/
       asm("nop");                   /* negative result*/
       test = (signed char)(num);    /*this changes its type to output as 8 bit value */ 
       PORTD = absolute_value(test); /*-43*/
       CCPR1L = 0;                   /* This is the new pulse width spot*/
       asm("nop");
      }
     else
      {
       RC0 = 1;                      /* indicate a positive sign*/
       PORTD = (char)(num);          /*this changes its type to output as 8 bit value */ 
       CCPR1L = PORTD;               /* This is the new pulse width spot*/
       asm("nop");                   /* positive result*/
      }

asm("nop");
} 
#endif


#ifdef ABS_H

signed char absolute_value(signed char value)
{
  signed char result;

  if (value < 0)
  {
    result = ~value + 1;
  }
  else
  {
    result = value;
  }

  return result;
}
#endif