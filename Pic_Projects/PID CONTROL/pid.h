#include "typedefs.h"


 
#ifndef PID_H_
#define PID_H_

/*Function Prototypes*/
//float PIDcal(float setpoint,float actual_position);
//sint16_t PIDcal(uint8_t setpoint,uint8_t actual_position);
uint8_t PIDcal(uint8_t setpoint,uint8_t actual_position);
float abs(float a);


/* Which algo format constants desired ? */
//#define  FLOATING_POINT_PID   /* In .h file*/
#define  FIXED_POINT_PID



#endif /*PID_H_*/