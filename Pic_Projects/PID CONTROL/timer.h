
#include "typedefs.h"
#include "htc.h"


/*Function Prototypes*/
uint16_t get_timer(uint8_t index);             // Which timer to use out of enum
void timer_set(uint8_t index, uint16_t value); 
void timer_isr(void);
void timer_init(void);

enum sw_timers_e
{
PID_TIMER,                  /* for the overall PID timer out*/
HEAT_SETTING,               /* HEAT setting timer duration*/    
TIMER_MAX
};