opt subtitle "HI-TECH Software Omniscient Code Generator (Lite mode) build 6738"

opt pagewidth 120

	opt lm

	processor	16F887
clrc	macro
	bcf	3,0
	endm
clrz	macro
	bcf	3,2
	endm
setc	macro
	bsf	3,0
	endm
setz	macro
	bsf	3,2
	endm
skipc	macro
	btfss	3,0
	endm
skipz	macro
	btfss	3,2
	endm
skipnc	macro
	btfsc	3,0
	endm
skipnz	macro
	btfsc	3,2
	endm
indf	equ	0
indf0	equ	0
pc	equ	2
pcl	equ	2
status	equ	3
fsr	equ	4
fsr0	equ	4
c	equ	1
z	equ	0
pclath	equ	10
# 17 "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\PID CONTROL\main.c"
	psect config,class=CONFIG,delta=2 ;#
# 17 "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\PID CONTROL\main.c"
	dw 0x1FFF & 0x3FFF & 0x3FFF & 0x3BFF & 0x3EFF & 0x3FFF & 0x3FFF & 0x3FFF & 0x3FEF & 0x3FF7 & 0x3FFA ;#
# 18 "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\PID CONTROL\main.c"
	psect config,class=CONFIG,delta=2 ;#
# 18 "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\PID CONTROL\main.c"
	dw 0x3FFF ;#
	FNCALL	_main,_init
	FNCALL	_main,_adc_init
	FNCALL	_main,_pwm_init
	FNCALL	_main,_timer_set
	FNCALL	_main,_get_timer
	FNCALL	_main,_get_position
	FNCALL	_main,_PIDcal
	FNCALL	_PIDcal,___lmul
	FNCALL	_get_position,_adc_convert
	FNROOT	_main
	FNCALL	_interrupt_handler,_adc_isr
	FNCALL	_interrupt_handler,_timer_isr
	FNCALL	intlevel1,_interrupt_handler
	global	intlevel1
	FNROOT	intlevel1
	global	_motor_speed_array
psect	strings,class=STRING,delta=2
global __pstrings
__pstrings:
;	global	stringdir,stringtab,__stringbase
stringtab:
;	String table - string pointers are 1 byte each
stringcode:stringdir:
movlw high(stringdir)
movwf pclath
movf fsr,w
incf fsr
	addwf pc
__stringbase:
	retlw	0
psect	strings
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\PID CONTROL\adc_func.c"
	line	23
_motor_speed_array:
	retlw	0Ah
	retlw	0

	retlw	0Bh
	retlw	0

	retlw	0Ch
	retlw	0

	retlw	0Dh
	retlw	0

	retlw	0Fh
	retlw	0

	retlw	011h
	retlw	0

	retlw	013h
	retlw	0

	retlw	017h
	retlw	0

	retlw	01Ch
	retlw	0

	retlw	024h
	retlw	0

	retlw	032h
	retlw	0

	retlw	053h
	retlw	0

	retlw	0FAh
	retlw	0

	retlw	0A1h
	retlw	01h

	retlw	071h
	retlw	02h

	retlw	088h
	retlw	013h

	global	_motor_speed_array
	global	PIDcal@i_term
	global	_timer_array
	global	_int_count
	global	main@test_timer
	global	_adc_isr_flag
	global	_TEST_UNUM
	global	_teststruct
	global	_tmr1_isr_counter
	global	_toms_variable
	global	PIDcal@pre_error
	global	_ADCON0
_ADCON0	set	31
	global	_ADRESH
_ADRESH	set	30
	global	_CCP1CON
_CCP1CON	set	23
	global	_CCPR1L
_CCPR1L	set	21
	global	_PORTB
_PORTB	set	6
	global	_PORTD
_PORTD	set	8
	global	_T1CON
_T1CON	set	16
	global	_T2CON
_T2CON	set	18
	global	_TMR1H
_TMR1H	set	15
	global	_TMR1L
_TMR1L	set	14
	global	_ADIF
_ADIF	set	102
	global	_GIE
_GIE	set	95
	global	_GODONE
_GODONE	set	249
	global	_PEIE
_PEIE	set	94
	global	_RC0
_RC0	set	56
	global	_RC5
_RC5	set	61
	global	_RC6
_RC6	set	62
	global	_TMR1IF
_TMR1IF	set	96
	global	_TO
_TO	set	28
	global	_ADCON1
_ADCON1	set	159
	global	_ADRESL
_ADRESL	set	158
	global	_OPTION
_OPTION	set	129
	global	_PR2
_PR2	set	146
	global	_TRISA
_TRISA	set	133
	global	_TRISC
_TRISC	set	135
	global	_TRISD
_TRISD	set	136
	global	_ADIE
_ADIE	set	1126
	global	_TMR1IE
_TMR1IE	set	1120
	global	_ANSEL
_ANSEL	set	392
	file	"PID CONTROL.as"
	line	#
psect cinit,class=CODE,delta=2
global start_initialization
start_initialization:

psect	bssCOMMON,class=COMMON,space=1
global __pbssCOMMON
__pbssCOMMON:
_adc_isr_flag:
       ds      2

_TEST_UNUM:
       ds      1

_teststruct:
       ds      1

_tmr1_isr_counter:
       ds      1

_toms_variable:
       ds      1

psect	bssBANK0,class=BANK0,space=1
global __pbssBANK0
__pbssBANK0:
PIDcal@i_term:
       ds      4

_timer_array:
       ds      4

_int_count:
       ds      2

main@test_timer:
       ds      1

PIDcal@pre_error:
       ds      2

; Clear objects allocated to COMMON
psect cinit,class=CODE,delta=2
	clrf	((__pbssCOMMON)+0)&07Fh
	clrf	((__pbssCOMMON)+1)&07Fh
	clrf	((__pbssCOMMON)+2)&07Fh
	clrf	((__pbssCOMMON)+3)&07Fh
	clrf	((__pbssCOMMON)+4)&07Fh
	clrf	((__pbssCOMMON)+5)&07Fh
; Clear objects allocated to BANK0
psect cinit,class=CODE,delta=2
	clrf	((__pbssBANK0)+0)&07Fh
	clrf	((__pbssBANK0)+1)&07Fh
	clrf	((__pbssBANK0)+2)&07Fh
	clrf	((__pbssBANK0)+3)&07Fh
	clrf	((__pbssBANK0)+4)&07Fh
	clrf	((__pbssBANK0)+5)&07Fh
	clrf	((__pbssBANK0)+6)&07Fh
	clrf	((__pbssBANK0)+7)&07Fh
	clrf	((__pbssBANK0)+8)&07Fh
	clrf	((__pbssBANK0)+9)&07Fh
	clrf	((__pbssBANK0)+10)&07Fh
	clrf	((__pbssBANK0)+11)&07Fh
	clrf	((__pbssBANK0)+12)&07Fh
psect cinit,class=CODE,delta=2
global end_of_initialization

;End of C runtime variable initialization code

end_of_initialization:
clrf status
ljmp _main	;jump to C main() function
psect	cstackCOMMON,class=COMMON,space=1
global __pcstackCOMMON
__pcstackCOMMON:
	global	?_init
?_init:	; 0 bytes @ 0x0
	global	?_adc_init
?_adc_init:	; 0 bytes @ 0x0
	global	?_pwm_init
?_pwm_init:	; 0 bytes @ 0x0
	global	?_timer_isr
?_timer_isr:	; 0 bytes @ 0x0
	global	??_timer_isr
??_timer_isr:	; 0 bytes @ 0x0
	global	?_adc_isr
?_adc_isr:	; 0 bytes @ 0x0
	global	??_adc_isr
??_adc_isr:	; 0 bytes @ 0x0
	global	?_interrupt_handler
?_interrupt_handler:	; 0 bytes @ 0x0
	global	?_get_position
?_get_position:	; 1 bytes @ 0x0
	global	?_main
?_main:	; 2 bytes @ 0x0
	ds	1
	global	timer_isr@i
timer_isr@i:	; 1 bytes @ 0x1
	ds	1
	global	??_interrupt_handler
??_interrupt_handler:	; 0 bytes @ 0x2
	ds	4
psect	cstackBANK0,class=BANK0,space=1
global __pcstackBANK0
__pcstackBANK0:
	global	??_init
??_init:	; 0 bytes @ 0x0
	global	??_adc_init
??_adc_init:	; 0 bytes @ 0x0
	global	??_pwm_init
??_pwm_init:	; 0 bytes @ 0x0
	global	?_timer_set
?_timer_set:	; 0 bytes @ 0x0
	global	?_get_timer
?_get_timer:	; 2 bytes @ 0x0
	global	?_adc_convert
?_adc_convert:	; 2 bytes @ 0x0
	global	?___lmul
?___lmul:	; 4 bytes @ 0x0
	global	timer_set@value
timer_set@value:	; 2 bytes @ 0x0
	global	___lmul@multiplier
___lmul@multiplier:	; 4 bytes @ 0x0
	ds	2
	global	??_timer_set
??_timer_set:	; 0 bytes @ 0x2
	global	??_get_timer
??_get_timer:	; 0 bytes @ 0x2
	global	??_adc_convert
??_adc_convert:	; 0 bytes @ 0x2
	ds	1
	global	timer_set@index
timer_set@index:	; 1 bytes @ 0x3
	global	get_timer@result
get_timer@result:	; 2 bytes @ 0x3
	ds	1
	global	adc_convert@adresh
adc_convert@adresh:	; 2 bytes @ 0x4
	global	___lmul@multiplicand
___lmul@multiplicand:	; 4 bytes @ 0x4
	ds	1
	global	get_timer@index
get_timer@index:	; 1 bytes @ 0x5
	ds	1
	global	adc_convert@adresl
adc_convert@adresl:	; 2 bytes @ 0x6
	ds	2
	global	??___lmul
??___lmul:	; 0 bytes @ 0x8
	global	adc_convert@result
adc_convert@result:	; 2 bytes @ 0x8
	ds	1
	global	___lmul@product
___lmul@product:	; 4 bytes @ 0x9
	ds	1
	global	??_get_position
??_get_position:	; 0 bytes @ 0xA
	ds	2
	global	get_position@new_position
get_position@new_position:	; 1 bytes @ 0xC
	ds	1
	global	?_PIDcal
?_PIDcal:	; 1 bytes @ 0xD
	global	PIDcal@actual_position
PIDcal@actual_position:	; 1 bytes @ 0xD
	global	get_position@current_position
get_position@current_position:	; 2 bytes @ 0xD
	ds	1
	global	??_PIDcal
??_PIDcal:	; 0 bytes @ 0xE
	ds	8
	global	PIDcal@p_term
PIDcal@p_term:	; 4 bytes @ 0x16
	ds	4
	global	PIDcal@d_term
PIDcal@d_term:	; 4 bytes @ 0x1A
	ds	4
	global	PIDcal@setpoint
PIDcal@setpoint:	; 1 bytes @ 0x1E
	ds	1
	global	PIDcal@output
PIDcal@output:	; 4 bytes @ 0x1F
	ds	4
	global	PIDcal@error
PIDcal@error:	; 2 bytes @ 0x23
	ds	2
	global	??_main
??_main:	; 0 bytes @ 0x25
	ds	2
	global	main@test
main@test:	; 2 bytes @ 0x27
	ds	2
	global	main@testenum
main@testenum:	; 1 bytes @ 0x29
	ds	1
	global	main@actual_position
main@actual_position:	; 1 bytes @ 0x2A
	ds	1
	global	main@setpoint
main@setpoint:	; 1 bytes @ 0x2B
	ds	1
	global	main@max_time_exceeded
main@max_time_exceeded:	; 1 bytes @ 0x2C
	ds	1
	global	main@max_time_exceeded_counter
main@max_time_exceeded_counter:	; 2 bytes @ 0x2D
	ds	2
	global	main@pid_cal_result
main@pid_cal_result:	; 1 bytes @ 0x2F
	ds	1
;;Data sizes: Strings 0, constant 32, data 0, bss 19, persistent 0 stack 0
;;Auto spaces:   Size  Autos    Used
;; COMMON          14      6      12
;; BANK0           80     48      61
;; BANK1           80      0       0
;; BANK3           96      0       0
;; BANK2           96      0       0

;;
;; Pointer list with targets:

;; ?_get_timer	unsigned short  size(1) Largest target is 0
;;
;; ?___lmul	unsigned long  size(1) Largest target is 0
;;
;; ?_adc_convert	unsigned short  size(1) Largest target is 0
;;


;;
;; Critical Paths under _main in COMMON
;;
;;   None.
;;
;; Critical Paths under _interrupt_handler in COMMON
;;
;;   _interrupt_handler->_timer_isr
;;
;; Critical Paths under _main in BANK0
;;
;;   _main->_PIDcal
;;   _PIDcal->___lmul
;;   _get_position->_adc_convert
;;
;; Critical Paths under _interrupt_handler in BANK0
;;
;;   None.
;;
;; Critical Paths under _main in BANK1
;;
;;   None.
;;
;; Critical Paths under _interrupt_handler in BANK1
;;
;;   None.
;;
;; Critical Paths under _main in BANK3
;;
;;   None.
;;
;; Critical Paths under _interrupt_handler in BANK3
;;
;;   None.
;;
;; Critical Paths under _main in BANK2
;;
;;   None.
;;
;; Critical Paths under _interrupt_handler in BANK2
;;
;;   None.

;;
;;Main: autosize = 0, tempsize = 2, incstack = 0, save=0
;;

;;
;;Call Graph Tables:
;;
;; ---------------------------------------------------------------------------------
;; (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;; ---------------------------------------------------------------------------------
;; (0) _main                                                11    11      0    1221
;;                                             37 BANK0     11    11      0
;;                               _init
;;                           _adc_init
;;                           _pwm_init
;;                          _timer_set
;;                          _get_timer
;;                       _get_position
;;                             _PIDcal
;; ---------------------------------------------------------------------------------
;; (1) _PIDcal                                              24    23      1     495
;;                                             13 BANK0     24    23      1
;;                             ___lmul
;; ---------------------------------------------------------------------------------
;; (1) _get_position                                         5     5      0     204
;;                                             10 BANK0      5     5      0
;;                        _adc_convert
;; ---------------------------------------------------------------------------------
;; (1) _init                                                 3     3      0       0
;;                                              0 BANK0      3     3      0
;; ---------------------------------------------------------------------------------
;; (2) ___lmul                                              13     5      8     136
;;                                              0 BANK0     13     5      8
;; ---------------------------------------------------------------------------------
;; (2) _adc_convert                                         10     8      2     102
;;                                              0 BANK0     10     8      2
;; ---------------------------------------------------------------------------------
;; (1) _get_timer                                            6     4      2      99
;;                                              0 BANK0      6     4      2
;; ---------------------------------------------------------------------------------
;; (1) _timer_set                                            6     4      2      93
;;                                              0 BANK0      4     2      2
;; ---------------------------------------------------------------------------------
;; (1) _pwm_init                                             0     0      0       0
;; ---------------------------------------------------------------------------------
;; (1) _adc_init                                             0     0      0       0
;; ---------------------------------------------------------------------------------
;; Estimated maximum stack depth 2
;; ---------------------------------------------------------------------------------
;; (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;; ---------------------------------------------------------------------------------
;; (3) _interrupt_handler                                    4     4      0      90
;;                                              2 COMMON     4     4      0
;;                            _adc_isr
;;                          _timer_isr
;; ---------------------------------------------------------------------------------
;; (4) _timer_isr                                            2     2      0      90
;;                                              0 COMMON     2     2      0
;; ---------------------------------------------------------------------------------
;; (4) _adc_isr                                              0     0      0       0
;; ---------------------------------------------------------------------------------
;; Estimated maximum stack depth 4
;; ---------------------------------------------------------------------------------

;; Call Graph Graphs:

;; _main (ROOT)
;;   _init
;;   _adc_init
;;   _pwm_init
;;   _timer_set
;;   _get_timer
;;   _get_position
;;     _adc_convert
;;   _PIDcal
;;     ___lmul
;;
;; _interrupt_handler (ROOT)
;;   _adc_isr
;;   _timer_isr
;;

;; Address spaces:

;;Name               Size   Autos  Total    Cost      Usage
;;BITCOMMON            E      0       0       0        0.0%
;;EEDATA             100      0       0       0        0.0%
;;NULL                 0      0       0       0        0.0%
;;CODE                 0      0       0       0        0.0%
;;COMMON               E      6       C       1       85.7%
;;BITSFR0              0      0       0       1        0.0%
;;SFR0                 0      0       0       1        0.0%
;;BITSFR1              0      0       0       2        0.0%
;;SFR1                 0      0       0       2        0.0%
;;STACK                0      0       6       2        0.0%
;;ABS                  0      0      49       3        0.0%
;;BITBANK0            50      0       0       4        0.0%
;;BITSFR3              0      0       0       4        0.0%
;;SFR3                 0      0       0       4        0.0%
;;BANK0               50     30      3D       5       76.3%
;;BITSFR2              0      0       0       5        0.0%
;;SFR2                 0      0       0       5        0.0%
;;BITBANK1            50      0       0       6        0.0%
;;BANK1               50      0       0       7        0.0%
;;BITBANK3            60      0       0       8        0.0%
;;BANK3               60      0       0       9        0.0%
;;BITBANK2            60      0       0      10        0.0%
;;BANK2               60      0       0      11        0.0%
;;DATA                 0      0      4F      12        0.0%

	global	_main
psect	maintext,global,class=CODE,delta=2
global __pmaintext
__pmaintext:

;; *************** function _main *****************
;; Defined at:
;;		line 48 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\PID CONTROL\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  max_time_exc    2   45[BANK0 ] unsigned short 
;;  test            2   39[BANK0 ] unsigned short 
;;  pid_cal_resu    1   47[BANK0 ] unsigned char 
;;  max_time_exc    1   44[BANK0 ] unsigned char 
;;  setpoint        1   43[BANK0 ] unsigned char 
;;  actual_posit    1   42[BANK0 ] unsigned char 
;;  testenum        1   41[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;                  2  856[COMMON] int 
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, btemp+1, pclath, cstack
;; Tracked objects:
;;		On entry : 17F/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       9       0       0       0
;;      Temps:          0       2       0       0       0
;;      Totals:         0      11       0       0       0
;;Total ram usage:       11 bytes
;; Hardware stack levels required when called:    4
;; This function calls:
;;		_init
;;		_adc_init
;;		_pwm_init
;;		_timer_set
;;		_get_timer
;;		_get_position
;;		_PIDcal
;; This function is called by:
;;		Startup code after reset
;; This function uses a non-reentrant model
;;
psect	maintext
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\PID CONTROL\main.c"
	line	48
	global	__size_of_main
	__size_of_main	equ	__end_of_main-_main
	
_main:	
	opt	stack 4
; Regs used in _main: [wreg-fsr0h+status,2+status,0+btemp+1+pclath+cstack]
	line	64
	
l6954:	
;main.c: 53: uint8_t setpoint;
;main.c: 55: uint8_t actual_position;
;main.c: 56: uint8_t pid_cal_result;
;main.c: 58: uint8_t max_time_exceeded;
;main.c: 59: uint16_t max_time_exceeded_counter;
;main.c: 60: static uint8_t test_timer;
;main.c: 61: uint16_t test;
;main.c: 62: uint8_t testenum;
;main.c: 64: test = (unsigned short)((signed char)(0x5593));
	movlw	low(0FF93h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(main@test)
	movlw	high(0FF93h)
	movwf	((main@test))+1
	line	73
;main.c: 73: test=99;
	movlw	low(063h)
	movwf	(main@test)
	movlw	high(063h)
	movwf	((main@test))+1
	line	76
	
l6956:	
;main.c: 76: testenum = test_enum1;
	clrc
	movlw	0
	btfsc	status,0
	movlw	1
	movwf	(main@testenum)
	line	77
;main.c: 77: testenum = test_enum2;
	clrf	(main@testenum)
	bsf	status,0
	rlf	(main@testenum),f
	line	79
	
l6958:	
;main.c: 79: init();
	fcall	_init
	line	80
	
l6960:	
;main.c: 80: adc_init();
	fcall	_adc_init
	line	81
	
l6962:	
;main.c: 81: pwm_init();
	fcall	_pwm_init
	line	82
	
l6964:	
;main.c: 82: TMR1IE = 1;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bsf	(1120/8)^080h,(1120)&7
	line	83
	
l6966:	
;main.c: 83: ADIE = 1;
	bsf	(1126/8)^080h,(1126)&7
	line	84
	
l6968:	
;main.c: 84: PEIE = 1;
	bsf	(94/8),(94)&7
	line	85
	
l6970:	
;main.c: 85: (GIE = 1);
	bsf	(95/8),(95)&7
	line	86
	
l6972:	
;main.c: 86: test_timer=1;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(main@test_timer)
	bsf	status,0
	rlf	(main@test_timer),f
	line	87
	
l6974:	
;main.c: 87: timer_set(PID_TIMER,10);
	movlw	low(0Ah)
	movwf	(?_timer_set)
	movlw	high(0Ah)
	movwf	((?_timer_set))+1
	movlw	(0)
	fcall	_timer_set
	line	88
	
l6976:	
;main.c: 88: timer_set(HEAT_SETTING,10);
	movlw	low(0Ah)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(?_timer_set)
	movlw	high(0Ah)
	movwf	((?_timer_set))+1
	movlw	(01h)
	fcall	_timer_set
	line	89
	
l6978:	
;main.c: 89: max_time_exceeded = FALSE;
	clrc
	movlw	0
	btfsc	status,0
	movlw	1
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(main@max_time_exceeded)
	line	90
	
l6980:	
;main.c: 90: max_time_exceeded_counter=0;
	movlw	low(0)
	movwf	(main@max_time_exceeded_counter)
	movlw	high(0)
	movwf	((main@max_time_exceeded_counter))+1
	goto	l6982
	line	94
;main.c: 94: while(1)
	
l859:	
	line	97
	
l6982:	
;main.c: 95: {
;main.c: 97: if (TO == 1)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfss	(28/8),(28)&7
	goto	u2931
	goto	u2930
u2931:
	goto	l860
u2930:
	line	100
	
l6984:	
;main.c: 98: {
;main.c: 100: if (get_timer(PID_TIMER) == 0)
	movlw	(0)
	fcall	_get_timer
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	((1+(?_get_timer))),w
	iorwf	((0+(?_get_timer))),w
	skipz
	goto	u2941
	goto	u2940
u2941:
	goto	l861
u2940:
	line	102
	
l6986:	
;main.c: 101: {
;main.c: 102: if ((get_timer(HEAT_SETTING) == 0) && (max_time_exceeded == FALSE))
	movlw	(01h)
	fcall	_get_timer
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	((1+(?_get_timer))),w
	iorwf	((0+(?_get_timer))),w
	skipz
	goto	u2951
	goto	u2950
u2951:
	goto	l7012
u2950:
	
l6988:	
	movf	(main@max_time_exceeded),f
	skipz
	goto	u2961
	goto	u2960
u2961:
	goto	l7012
u2960:
	line	104
	
l6990:	
;main.c: 103: {
;main.c: 104: setpoint = (0xA1);
	movlw	(0A1h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(main@setpoint)
	line	105
	
l6992:	
;main.c: 105: timer_set(HEAT_SETTING,(1000));
	movlw	low(03E8h)
	movwf	(?_timer_set)
	movlw	high(03E8h)
	movwf	((?_timer_set))+1
	movlw	(01h)
	fcall	_timer_set
	line	106
	
l6994:	
;main.c: 106: max_time_exceeded_counter++;
	movlw	low(01h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	addwf	(main@max_time_exceeded_counter),f
	skipnc
	incf	(main@max_time_exceeded_counter+1),f
	movlw	high(01h)
	addwf	(main@max_time_exceeded_counter+1),f
	line	107
	
l6996:	
;main.c: 107: if (max_time_exceeded_counter >= (600))
	movlw	high(0258h)
	subwf	(main@max_time_exceeded_counter+1),w
	movlw	low(0258h)
	skipnz
	subwf	(main@max_time_exceeded_counter),w
	skipc
	goto	u2971
	goto	u2970
u2971:
	goto	l7004
u2970:
	line	109
	
l6998:	
;main.c: 108: {
;main.c: 109: setpoint = (0x82);
	movlw	(082h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(main@setpoint)
	line	110
	
l7000:	
;main.c: 110: max_time_exceeded = FALSE;
	clrc
	movlw	0
	btfsc	status,0
	movlw	1
	movwf	(main@max_time_exceeded)
	line	111
	
l7002:	
# 111 "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\PID CONTROL\main.c"
nop ;#
psect	maintext
	goto	l7004
	line	112
	
l863:	
	line	113
	
l7004:	
;main.c: 112: }
;main.c: 113: if (max_time_exceeded_counter >= (1200))
	movlw	high(04B0h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	subwf	(main@max_time_exceeded_counter+1),w
	movlw	low(04B0h)
	skipnz
	subwf	(main@max_time_exceeded_counter),w
	skipc
	goto	u2981
	goto	u2980
u2981:
	goto	l7012
u2980:
	line	115
	
l7006:	
;main.c: 114: {
;main.c: 115: setpoint = (0x6E);
	movlw	(06Eh)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(main@setpoint)
	line	116
	
l7008:	
;main.c: 116: max_time_exceeded = TRUE;
	clrf	(main@max_time_exceeded)
	bsf	status,0
	rlf	(main@max_time_exceeded),f
	line	117
	
l7010:	
# 117 "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\PID CONTROL\main.c"
nop ;#
psect	maintext
	line	118
;main.c: 118: }
	goto	l7012
	line	119
	
l864:	
	goto	l7012
	line	122
;main.c: 119: else
;main.c: 120: {
	
l865:	
	line	123
;main.c: 122: }
;main.c: 123: }
	goto	l7012
	line	124
	
l862:	
	goto	l7012
	line	127
;main.c: 124: else
;main.c: 125: {
	
l866:	
	line	129
	
l7012:	
;main.c: 127: }
;main.c: 129: actual_position = get_position();
	fcall	_get_position
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(main@actual_position)
	line	130
	
l7014:	
;main.c: 130: RC5 = 1;
	bsf	(61/8),(61)&7
	line	131
;main.c: 131: pid_cal_result = PIDcal(100, 0);
	clrc
	movlw	0
	btfsc	status,0
	movlw	1
	movwf	(?_PIDcal)
	movlw	(064h)
	fcall	_PIDcal
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(main@pid_cal_result)
	line	132
;main.c: 132: pid_cal_result = PIDcal(100, 20);
	movlw	(014h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(?_PIDcal)
	movlw	(064h)
	fcall	_PIDcal
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_main+1)+0
	movf	(??_main+1)+0,w
	movwf	(main@pid_cal_result)
	line	133
;main.c: 133: pid_cal_result = PIDcal(100, 30);
	movlw	(01Eh)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(?_PIDcal)
	movlw	(064h)
	fcall	_PIDcal
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_main+1)+0
	movf	(??_main+1)+0,w
	movwf	(main@pid_cal_result)
	line	134
;main.c: 134: pid_cal_result = PIDcal(100, 50);
	movlw	(032h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(?_PIDcal)
	movlw	(064h)
	fcall	_PIDcal
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_main+1)+0
	movf	(??_main+1)+0,w
	movwf	(main@pid_cal_result)
	line	135
;main.c: 135: pid_cal_result = PIDcal(100, 70);
	movlw	(046h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(?_PIDcal)
	movlw	(064h)
	fcall	_PIDcal
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_main+1)+0
	movf	(??_main+1)+0,w
	movwf	(main@pid_cal_result)
	line	136
;main.c: 136: pid_cal_result = PIDcal(100, 85);
	movlw	(055h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(?_PIDcal)
	movlw	(064h)
	fcall	_PIDcal
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_main+1)+0
	movf	(??_main+1)+0,w
	movwf	(main@pid_cal_result)
	line	137
;main.c: 137: pid_cal_result = PIDcal(100, 90);
	movlw	(05Ah)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(?_PIDcal)
	movlw	(064h)
	fcall	_PIDcal
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_main+1)+0
	movf	(??_main+1)+0,w
	movwf	(main@pid_cal_result)
	line	138
;main.c: 138: pid_cal_result = PIDcal(100, 95);
	movlw	(05Fh)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(?_PIDcal)
	movlw	(064h)
	fcall	_PIDcal
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_main+1)+0
	movf	(??_main+1)+0,w
	movwf	(main@pid_cal_result)
	line	139
;main.c: 139: pid_cal_result = PIDcal(100, 98);
	movlw	(062h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(?_PIDcal)
	movlw	(064h)
	fcall	_PIDcal
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_main+1)+0
	movf	(??_main+1)+0,w
	movwf	(main@pid_cal_result)
	line	140
;main.c: 140: pid_cal_result = PIDcal(100, 98);
	movlw	(062h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(?_PIDcal)
	movlw	(064h)
	fcall	_PIDcal
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_main+1)+0
	movf	(??_main+1)+0,w
	movwf	(main@pid_cal_result)
	line	141
;main.c: 141: pid_cal_result = PIDcal(100, 99);
	movlw	(063h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(?_PIDcal)
	movlw	(064h)
	fcall	_PIDcal
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_main+1)+0
	movf	(??_main+1)+0,w
	movwf	(main@pid_cal_result)
	line	142
;main.c: 142: pid_cal_result = PIDcal(100, 100);
	movlw	(064h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(?_PIDcal)
	movlw	(064h)
	fcall	_PIDcal
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_main+1)+0
	movf	(??_main+1)+0,w
	movwf	(main@pid_cal_result)
	line	143
;main.c: 143: pid_cal_result = PIDcal(100, 102);
	movlw	(066h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(?_PIDcal)
	movlw	(064h)
	fcall	_PIDcal
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_main+1)+0
	movf	(??_main+1)+0,w
	movwf	(main@pid_cal_result)
	line	144
;main.c: 144: pid_cal_result = PIDcal(100, 104);
	movlw	(068h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(?_PIDcal)
	movlw	(064h)
	fcall	_PIDcal
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_main+1)+0
	movf	(??_main+1)+0,w
	movwf	(main@pid_cal_result)
	line	145
;main.c: 145: pid_cal_result = PIDcal(100, 95);
	movlw	(05Fh)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(?_PIDcal)
	movlw	(064h)
	fcall	_PIDcal
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_main+1)+0
	movf	(??_main+1)+0,w
	movwf	(main@pid_cal_result)
	line	146
;main.c: 146: pid_cal_result = PIDcal(100, 98);
	movlw	(062h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(?_PIDcal)
	movlw	(064h)
	fcall	_PIDcal
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_main+1)+0
	movf	(??_main+1)+0,w
	movwf	(main@pid_cal_result)
	line	147
;main.c: 147: pid_cal_result = PIDcal(100, 99);
	movlw	(063h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(?_PIDcal)
	movlw	(064h)
	fcall	_PIDcal
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_main+1)+0
	movf	(??_main+1)+0,w
	movwf	(main@pid_cal_result)
	line	148
;main.c: 148: pid_cal_result = PIDcal(100, 99);
	movlw	(063h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(?_PIDcal)
	movlw	(064h)
	fcall	_PIDcal
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_main+1)+0
	movf	(??_main+1)+0,w
	movwf	(main@pid_cal_result)
	line	149
;main.c: 149: pid_cal_result = PIDcal(100, 99);
	movlw	(063h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(?_PIDcal)
	movlw	(064h)
	fcall	_PIDcal
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_main+1)+0
	movf	(??_main+1)+0,w
	movwf	(main@pid_cal_result)
	line	150
;main.c: 150: pid_cal_result = PIDcal(100, 100);
	movlw	(064h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(?_PIDcal)
	movlw	(064h)
	fcall	_PIDcal
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_main+1)+0
	movf	(??_main+1)+0,w
	movwf	(main@pid_cal_result)
	line	151
;main.c: 151: pid_cal_result = PIDcal(100, 100);
	movlw	(064h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(?_PIDcal)
	movlw	(064h)
	fcall	_PIDcal
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_main+1)+0
	movf	(??_main+1)+0,w
	movwf	(main@pid_cal_result)
	line	152
;main.c: 152: pid_cal_result = PIDcal(100, 100);
	movlw	(064h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(?_PIDcal)
	movlw	(064h)
	fcall	_PIDcal
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_main+1)+0
	movf	(??_main+1)+0,w
	movwf	(main@pid_cal_result)
	line	153
;main.c: 153: pid_cal_result = PIDcal(100, 100);
	movlw	(064h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(?_PIDcal)
	movlw	(064h)
	fcall	_PIDcal
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_main+1)+0
	movf	(??_main+1)+0,w
	movwf	(main@pid_cal_result)
	line	154
;main.c: 154: pid_cal_result = PIDcal(100, 100);
	movlw	(064h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(?_PIDcal)
	movlw	(064h)
	fcall	_PIDcal
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_main+1)+0
	movf	(??_main+1)+0,w
	movwf	(main@pid_cal_result)
	line	155
;main.c: 155: pid_cal_result = PIDcal(100, 100);
	movlw	(064h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(?_PIDcal)
	movlw	(064h)
	fcall	_PIDcal
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_main+1)+0
	movf	(??_main+1)+0,w
	movwf	(main@pid_cal_result)
	line	156
;main.c: 156: pid_cal_result = PIDcal(100, 100);
	movlw	(064h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(?_PIDcal)
	movlw	(064h)
	fcall	_PIDcal
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_main+1)+0
	movf	(??_main+1)+0,w
	movwf	(main@pid_cal_result)
	line	157
;main.c: 157: pid_cal_result = PIDcal(100, 100);
	movlw	(064h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(?_PIDcal)
	movlw	(064h)
	fcall	_PIDcal
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_main+1)+0
	movf	(??_main+1)+0,w
	movwf	(main@pid_cal_result)
	line	158
;main.c: 158: pid_cal_result = PIDcal(100, 100);
	movlw	(064h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(?_PIDcal)
	movlw	(064h)
	fcall	_PIDcal
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_main+1)+0
	movf	(??_main+1)+0,w
	movwf	(main@pid_cal_result)
	line	159
;main.c: 159: pid_cal_result = PIDcal(100, 100);
	movlw	(064h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(?_PIDcal)
	movlw	(064h)
	fcall	_PIDcal
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_main+1)+0
	movf	(??_main+1)+0,w
	movwf	(main@pid_cal_result)
	line	160
;main.c: 160: pid_cal_result = PIDcal(100, 100);
	movlw	(064h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(?_PIDcal)
	movlw	(064h)
	fcall	_PIDcal
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_main+1)+0
	movf	(??_main+1)+0,w
	movwf	(main@pid_cal_result)
	line	161
;main.c: 161: pid_cal_result = PIDcal(100, 100);
	movlw	(064h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(?_PIDcal)
	movlw	(064h)
	fcall	_PIDcal
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_main+1)+0
	movf	(??_main+1)+0,w
	movwf	(main@pid_cal_result)
	line	162
;main.c: 162: pid_cal_result = PIDcal(100, 100);
	movlw	(064h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(?_PIDcal)
	movlw	(064h)
	fcall	_PIDcal
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_main+1)+0
	movf	(??_main+1)+0,w
	movwf	(main@pid_cal_result)
	line	163
;main.c: 163: pid_cal_result = PIDcal(100, 101);
	movlw	(065h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(?_PIDcal)
	movlw	(064h)
	fcall	_PIDcal
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_main+1)+0
	movf	(??_main+1)+0,w
	movwf	(main@pid_cal_result)
	line	164
;main.c: 164: pid_cal_result = PIDcal(100, 100);
	movlw	(064h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(?_PIDcal)
	movlw	(064h)
	fcall	_PIDcal
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_main+1)+0
	movf	(??_main+1)+0,w
	movwf	(main@pid_cal_result)
	line	166
;main.c: 166: pid_cal_result = PIDcal(setpoint, actual_position);
	movf	(main@actual_position),w
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(?_PIDcal)
	movf	(main@setpoint),w
	fcall	_PIDcal
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_main+1)+0
	movf	(??_main+1)+0,w
	movwf	(main@pid_cal_result)
	line	167
	
l7016:	
;main.c: 167: RC5 = 0;
	bcf	(61/8),(61)&7
	line	168
	
l7018:	
;main.c: 168: PORTD = pid_cal_result;
	movf	(main@pid_cal_result),w
	movwf	(8)	;volatile
	line	169
	
l7020:	
;main.c: 169: CCPR1L = PORTD;
	movf	(8),w	;volatile
	movwf	(21)	;volatile
	line	170
	
l7022:	
;main.c: 170: if (PORTD == 0)
	movf	(8),f
	skipz	;volatile
	goto	u2991
	goto	u2990
u2991:
	goto	l867
u2990:
	line	172
	
l7024:	
;main.c: 171: {
;main.c: 172: RC0 = 1;
	bsf	(56/8),(56)&7
	line	173
;main.c: 173: }
	goto	l7026
	line	174
	
l867:	
	line	176
;main.c: 174: else
;main.c: 175: {
;main.c: 176: RC0 = 0;
	bcf	(56/8),(56)&7
	goto	l7026
	line	177
	
l868:	
	line	181
	
l7026:	
;main.c: 177: }
;main.c: 181: timer_set(PID_TIMER,(160));
	movlw	low(0A0h)
	movwf	(?_timer_set)
	movlw	high(0A0h)
	movwf	((?_timer_set))+1
	movlw	(0)
	fcall	_timer_set
	line	182
	
l7028:	
;main.c: 182: RC6 = 1;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	bsf	(62/8),(62)&7
	line	183
;main.c: 183: }
	goto	l6982
	line	184
	
l861:	
	line	186
;main.c: 184: else
;main.c: 185: {
;main.c: 186: RC6 = 0;
	bcf	(62/8),(62)&7
	goto	l6982
	line	187
	
l869:	
	line	190
;main.c: 187: }
;main.c: 190: }
	goto	l6982
	line	191
	
l860:	
	line	193
# 193 "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\PID CONTROL\main.c"
clrwdt ;#
psect	maintext
	goto	l6982
	line	194
	
l870:	
	goto	l6982
	line	196
	
l871:	
	line	94
	goto	l6982
	
l872:	
	line	197
	
l873:	
	global	start
	ljmp	start
	opt stack 0
GLOBAL	__end_of_main
	__end_of_main:
;; =============== function _main ends ============

	signat	_main,90
	global	_PIDcal
psect	text476,local,class=CODE,delta=2
global __ptext476
__ptext476:

;; *************** function _PIDcal *****************
;; Defined at:
;;		line 28 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\PID CONTROL\pid4.c"
;; Parameters:    Size  Location     Type
;;  setpoint        1    wreg     unsigned char 
;;  actual_posit    1   13[BANK0 ] unsigned char 
;; Auto vars:     Size  Location     Type
;;  setpoint        1   30[BANK0 ] unsigned char 
;;  output          4   31[BANK0 ] long 
;;  d_term          4   26[BANK0 ] long 
;;  p_term          4   22[BANK0 ] long 
;;  error           2   35[BANK0 ] short 
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, status,2, status,0, btemp+1, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       1       0       0       0
;;      Locals:         0      15       0       0       0
;;      Temps:          0       8       0       0       0
;;      Totals:         0      24       0       0       0
;;Total ram usage:       24 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		___lmul
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text476
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\PID CONTROL\pid4.c"
	line	28
	global	__size_of_PIDcal
	__size_of_PIDcal	equ	__end_of_PIDcal-_PIDcal
	
_PIDcal:	
	opt	stack 4
; Regs used in _PIDcal: [wreg+status,2+status,0+btemp+1+pclath+cstack]
;PIDcal@setpoint stored from wreg
	line	38
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(PIDcal@setpoint)
	
l6922:	
;pid4.c: 29: static sint16_t pre_error = 0;
;pid4.c: 30: sint16_t error;
;pid4.c: 31: sint32_t output;
;pid4.c: 33: sint32_t p_term;
;pid4.c: 34: static sint32_t i_term = 0;
;pid4.c: 35: sint32_t d_term;
;pid4.c: 38: (GIE = 0);
	bcf	(95/8),(95)&7
	line	39
	
l6924:	
;pid4.c: 39: error = (setpoint - actual_position);
	movf	(PIDcal@actual_position),w
	movwf	(??_PIDcal+0)+0
	clrf	(??_PIDcal+0)+0+1
	comf	(??_PIDcal+0)+0,f
	comf	(??_PIDcal+0)+1,f
	incf	(??_PIDcal+0)+0,f
	skipnz
	incf	(??_PIDcal+0)+1,f
	movf	(PIDcal@setpoint),w
	addwf	0+(??_PIDcal+0)+0,w
	movwf	(PIDcal@error)
	movf	1+(??_PIDcal+0)+0,w
	skipnc
	incf	1+(??_PIDcal+0)+0,w
	movwf	((PIDcal@error))+1
	line	42
	
l6926:	
;pid4.c: 42: p_term = (sint32_t)((40*(256)))*(sint32_t)(error);
	movf	(PIDcal@error),w
	movwf	(?___lmul)
	movf	(PIDcal@error+1),w
	movwf	(?___lmul+1)
	movlw	0
	btfsc	(?___lmul+1),7
	movlw	255
	movwf	(?___lmul+2)
	movwf	(?___lmul+3)
	movlw	0
	movwf	3+(?___lmul)+04h
	movlw	0
	movwf	2+(?___lmul)+04h
	movlw	028h
	movwf	1+(?___lmul)+04h
	movlw	0
	movwf	0+(?___lmul)+04h

	fcall	___lmul
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(3+(?___lmul)),w
	movwf	(PIDcal@p_term+3)
	movf	(2+(?___lmul)),w
	movwf	(PIDcal@p_term+2)
	movf	(1+(?___lmul)),w
	movwf	(PIDcal@p_term+1)
	movf	(0+(?___lmul)),w
	movwf	(PIDcal@p_term)

	line	45
	
l6928:	
;pid4.c: 45: i_term = i_term + (sint32_t)((5.00*(256))*(0.02))*(sint32_t)(error);
	movf	(PIDcal@error),w
	movwf	(?___lmul)
	movf	(PIDcal@error+1),w
	movwf	(?___lmul+1)
	movlw	0
	btfsc	(?___lmul+1),7
	movlw	255
	movwf	(?___lmul+2)
	movwf	(?___lmul+3)
	movlw	0
	movwf	3+(?___lmul)+04h
	movlw	0
	movwf	2+(?___lmul)+04h
	movlw	0
	movwf	1+(?___lmul)+04h
	movlw	019h
	movwf	0+(?___lmul)+04h

	fcall	___lmul
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(0+(?___lmul)),w
	addwf	(PIDcal@i_term),w
	movwf	((??_PIDcal+0)+0+0)
	movlw	0
	skipnc
	movlw	1
	addwf	(1+(?___lmul)),w
	clrf	((??_PIDcal+0)+0+2)
	skipnc
	incf	((??_PIDcal+0)+0+2),f
	addwf	(PIDcal@i_term+1),w
	movwf	((??_PIDcal+0)+0+1)
	skipnc
	incf	((??_PIDcal+0)+0+2),f
	movf	(2+(?___lmul)),w
	addwf	((??_PIDcal+0)+0+2),w
	clrf	((??_PIDcal+0)+0+3)
	skipnc
	incf	((??_PIDcal+0)+0+3),f
	addwf	(PIDcal@i_term+2),w
	movwf	((??_PIDcal+0)+0+2)
	skipnc
	incf	((??_PIDcal+0)+0+3),f
	movf	(3+(?___lmul)),w
	addwf	((??_PIDcal+0)+0+3),w
	addwf	(PIDcal@i_term+3),w
	movwf	((??_PIDcal+0)+0+3)
	movf	3+(??_PIDcal+0)+0,w
	movwf	(PIDcal@i_term+3)
	movf	2+(??_PIDcal+0)+0,w
	movwf	(PIDcal@i_term+2)
	movf	1+(??_PIDcal+0)+0,w
	movwf	(PIDcal@i_term+1)
	movf	0+(??_PIDcal+0)+0,w
	movwf	(PIDcal@i_term)

	line	47
	
l6930:	
;pid4.c: 47: if (i_term > (100*(256)))
	movf	(PIDcal@i_term+3),w
	xorlw	80h
	movwf	btemp+1
	movlw	0
	xorlw	80h
	subwf	btemp+1,w
	
	skipz
	goto	u2873
	movlw	0
	subwf	(PIDcal@i_term+2),w
	skipz
	goto	u2873
	movlw	064h
	subwf	(PIDcal@i_term+1),w
	skipz
	goto	u2873
	movlw	01h
	subwf	(PIDcal@i_term),w
u2873:
	skipc
	goto	u2871
	goto	u2870
u2871:
	goto	l4267
u2870:
	line	49
	
l6932:	
;pid4.c: 48: {
;pid4.c: 49: i_term = (100*(256));
	movlw	0
	movwf	(PIDcal@i_term+3)
	movlw	0
	movwf	(PIDcal@i_term+2)
	movlw	064h
	movwf	(PIDcal@i_term+1)
	movlw	0
	movwf	(PIDcal@i_term)

	line	50
;pid4.c: 50: }
	goto	l6936
	line	51
	
l4267:	
;pid4.c: 51: else if (i_term < (0))
	btfss	(PIDcal@i_term+3),7
	goto	u2881
	goto	u2880
u2881:
	goto	l6936
u2880:
	line	53
	
l6934:	
;pid4.c: 52: {
;pid4.c: 53: i_term = (0);
	movlw	0
	movwf	(PIDcal@i_term+3)
	movlw	0
	movwf	(PIDcal@i_term+2)
	movlw	0
	movwf	(PIDcal@i_term+1)
	movlw	0
	movwf	(PIDcal@i_term)

	goto	l6936
	line	54
	
l4269:	
	goto	l6936
	line	57
	
l4268:	
	
l6936:	
;pid4.c: 54: }
;pid4.c: 57: d_term = (sint32_t)((0.10*(256))/(0.02))*(sint32_t)(error - pre_error);
	comf	(PIDcal@pre_error),w
	movwf	(??_PIDcal+0)+0
	comf	(PIDcal@pre_error+1),w
	movwf	((??_PIDcal+0)+0+1)
	incf	(??_PIDcal+0)+0,f
	skipnz
	incf	((??_PIDcal+0)+0+1),f
	movf	(PIDcal@error),w
	addwf	0+(??_PIDcal+0)+0,w
	movwf	(?___lmul)
	movf	(PIDcal@error+1),w
	skipnc
	incf	(PIDcal@error+1),w
	addwf	1+(??_PIDcal+0)+0,w
	movwf	1+(?___lmul)
	clrf	(?___lmul)+2
	btfsc	(?___lmul)+1,7
	decf	2+(?___lmul),f
	movf	(?___lmul)+2,w
	movwf	3+(?___lmul)
	movlw	0
	movwf	3+(?___lmul)+04h
	movlw	0
	movwf	2+(?___lmul)+04h
	movlw	05h
	movwf	1+(?___lmul)+04h
	movlw	0
	movwf	0+(?___lmul)+04h

	fcall	___lmul
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(3+(?___lmul)),w
	movwf	(PIDcal@d_term+3)
	movf	(2+(?___lmul)),w
	movwf	(PIDcal@d_term+2)
	movf	(1+(?___lmul)),w
	movwf	(PIDcal@d_term+1)
	movf	(0+(?___lmul)),w
	movwf	(PIDcal@d_term)

	line	60
	
l6938:	
;pid4.c: 60: output = p_term + i_term + d_term;
	movf	(PIDcal@d_term),w
	movwf	(??_PIDcal+0)+0
	movf	(PIDcal@d_term+1),w
	movwf	((??_PIDcal+0)+0+1)
	movf	(PIDcal@d_term+2),w
	movwf	((??_PIDcal+0)+0+2)
	movf	(PIDcal@d_term+3),w
	movwf	((??_PIDcal+0)+0+3)
	movf	(PIDcal@p_term),w
	addwf	(PIDcal@i_term),w
	movwf	((??_PIDcal+4)+0+0)
	movlw	0
	skipnc
	movlw	1
	addwf	(PIDcal@p_term+1),w
	clrf	((??_PIDcal+4)+0+2)
	skipnc
	incf	((??_PIDcal+4)+0+2),f
	addwf	(PIDcal@i_term+1),w
	movwf	((??_PIDcal+4)+0+1)
	skipnc
	incf	((??_PIDcal+4)+0+2),f
	movf	(PIDcal@p_term+2),w
	addwf	((??_PIDcal+4)+0+2),w
	clrf	((??_PIDcal+4)+0+3)
	skipnc
	incf	((??_PIDcal+4)+0+3),f
	addwf	(PIDcal@i_term+2),w
	movwf	((??_PIDcal+4)+0+2)
	skipnc
	incf	((??_PIDcal+4)+0+3),f
	movf	(PIDcal@p_term+3),w
	addwf	((??_PIDcal+4)+0+3),w
	addwf	(PIDcal@i_term+3),w
	movwf	((??_PIDcal+4)+0+3)
	movf	0+(??_PIDcal+4)+0,w
	addwf	(??_PIDcal+0)+0,f
	movf	1+(??_PIDcal+4)+0,w
	skipnc
	incfsz	1+(??_PIDcal+4)+0,w
	goto	u2890
	goto	u2891
u2890:
	addwf	(??_PIDcal+0)+1,f
u2891:
	movf	2+(??_PIDcal+4)+0,w
	skipnc
	incfsz	2+(??_PIDcal+4)+0,w
	goto	u2892
	goto	u2893
u2892:
	addwf	(??_PIDcal+0)+2,f
u2893:
	movf	3+(??_PIDcal+4)+0,w
	skipnc
	incf	3+(??_PIDcal+4)+0,w
	addwf	(??_PIDcal+0)+3,f
	movf	3+(??_PIDcal+0)+0,w
	movwf	(PIDcal@output+3)
	movf	2+(??_PIDcal+0)+0,w
	movwf	(PIDcal@output+2)
	movf	1+(??_PIDcal+0)+0,w
	movwf	(PIDcal@output+1)
	movf	0+(??_PIDcal+0)+0,w
	movwf	(PIDcal@output)

	line	62
	
l6940:	
;pid4.c: 62: if (output > (100*(256)))
	movf	(PIDcal@output+3),w
	xorlw	80h
	movwf	btemp+1
	movlw	0
	xorlw	80h
	subwf	btemp+1,w
	
	skipz
	goto	u2903
	movlw	0
	subwf	(PIDcal@output+2),w
	skipz
	goto	u2903
	movlw	064h
	subwf	(PIDcal@output+1),w
	skipz
	goto	u2903
	movlw	01h
	subwf	(PIDcal@output),w
u2903:
	skipc
	goto	u2901
	goto	u2900
u2901:
	goto	l4270
u2900:
	line	64
	
l6942:	
;pid4.c: 63: {
;pid4.c: 64: output = (100*(256));
	movlw	0
	movwf	(PIDcal@output+3)
	movlw	0
	movwf	(PIDcal@output+2)
	movlw	064h
	movwf	(PIDcal@output+1)
	movlw	0
	movwf	(PIDcal@output)

	line	65
;pid4.c: 65: }
	goto	l6946
	line	66
	
l4270:	
;pid4.c: 66: else if (output < (0))
	btfss	(PIDcal@output+3),7
	goto	u2911
	goto	u2910
u2911:
	goto	l6946
u2910:
	line	68
	
l6944:	
;pid4.c: 67: {
;pid4.c: 68: output = (0);
	movlw	0
	movwf	(PIDcal@output+3)
	movlw	0
	movwf	(PIDcal@output+2)
	movlw	0
	movwf	(PIDcal@output+1)
	movlw	0
	movwf	(PIDcal@output)

	goto	l6946
	line	69
	
l4272:	
	goto	l6946
	line	72
	
l4271:	
	
l6946:	
;pid4.c: 69: }
;pid4.c: 72: pre_error = error;
	movf	(PIDcal@error+1),w
	clrf	(PIDcal@pre_error+1)
	addwf	(PIDcal@pre_error+1)
	movf	(PIDcal@error),w
	clrf	(PIDcal@pre_error)
	addwf	(PIDcal@pre_error)

	line	74
	
l6948:	
;pid4.c: 74: (GIE = 1);
	bsf	(95/8),(95)&7
	line	75
	
l6950:	
;pid4.c: 75: return ((uint8_t)(output >> 8));
	movf	(PIDcal@output),w
	movwf	(??_PIDcal+0)+0
	movf	(PIDcal@output+1),w
	movwf	((??_PIDcal+0)+0+1)
	movf	(PIDcal@output+2),w
	movwf	((??_PIDcal+0)+0+2)
	movf	(PIDcal@output+3),w
	movwf	((??_PIDcal+0)+0+3)
	movlw	08h
	movwf	(??_PIDcal+4)+0
u2925:
	rlf	(??_PIDcal+0)+3,w
	rrf	(??_PIDcal+0)+3,f
	rrf	(??_PIDcal+0)+2,f
	rrf	(??_PIDcal+0)+1,f
	rrf	(??_PIDcal+0)+0,f
u2920:
	decfsz	(??_PIDcal+4)+0,f
	goto	u2925
	movf	0+(??_PIDcal+0)+0,w
	goto	l4273
	
l6952:	
	line	76
	
l4273:	
	return
	opt stack 0
GLOBAL	__end_of_PIDcal
	__end_of_PIDcal:
;; =============== function _PIDcal ends ============

	signat	_PIDcal,8313
	global	_get_position
psect	text477,local,class=CODE,delta=2
global __ptext477
__ptext477:

;; *************** function _get_position *****************
;; Defined at:
;;		line 96 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\PID CONTROL\adc_func.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  current_posi    2   13[BANK0 ] unsigned short 
;;  new_position    1   12[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, status,2, status,0, btemp+1, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       3       0       0       0
;;      Temps:          0       2       0       0       0
;;      Totals:         0       5       0       0       0
;;Total ram usage:        5 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		_adc_convert
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text477
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\PID CONTROL\adc_func.c"
	line	96
	global	__size_of_get_position
	__size_of_get_position	equ	__end_of_get_position-_get_position
	
_get_position:	
	opt	stack 4
; Regs used in _get_position: [wreg+status,2+status,0+btemp+1+pclath+cstack]
	line	100
	
l6910:	
;adc_func.c: 97: uint16_t current_position;
;adc_func.c: 98: uint8_t new_position;
;adc_func.c: 100: ADCON0 = 0xC9;
	movlw	(0C9h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(31)	;volatile
	line	101
	
l6912:	
;adc_func.c: 101: current_position = adc_convert();
	fcall	_adc_convert
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(1+(?_adc_convert)),w
	clrf	(get_position@current_position+1)
	addwf	(get_position@current_position+1)
	movf	(0+(?_adc_convert)),w
	clrf	(get_position@current_position)
	addwf	(get_position@current_position)

	line	102
	
l6914:	
;adc_func.c: 102: current_position = (current_position >> 2);
	movf	(get_position@current_position+1),w
	movwf	(??_get_position+0)+0+1
	movf	(get_position@current_position),w
	movwf	(??_get_position+0)+0
	movlw	02h
u2865:
	clrc
	rrf	(??_get_position+0)+1,f
	rrf	(??_get_position+0)+0,f
	addlw	-1
	skipz
	goto	u2865
	movf	0+(??_get_position+0)+0,w
	movwf	(get_position@current_position)
	movf	1+(??_get_position+0)+0,w
	movwf	(get_position@current_position+1)
	line	103
	
l6916:	
;adc_func.c: 103: new_position = current_position;
	movf	(get_position@current_position),w
	movwf	(??_get_position+0)+0
	movf	(??_get_position+0)+0,w
	movwf	(get_position@new_position)
	line	104
	
l6918:	
;adc_func.c: 104: return new_position;
	movf	(get_position@new_position),w
	goto	l1738
	
l6920:	
	line	105
	
l1738:	
	return
	opt stack 0
GLOBAL	__end_of_get_position
	__end_of_get_position:
;; =============== function _get_position ends ============

	signat	_get_position,89
	global	_init
psect	text478,local,class=CODE,delta=2
global __ptext478
__ptext478:

;; *************** function _init *****************
;; Defined at:
;;		line 201 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\PID CONTROL\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       3       0       0       0
;;      Totals:         0       3       0       0       0
;;Total ram usage:        3 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text478
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\PID CONTROL\main.c"
	line	201
	global	__size_of_init
	__size_of_init	equ	__end_of_init-_init
	
_init:	
	opt	stack 5
; Regs used in _init: [wreg+status,2+status,0]
	line	202
	
l6704:	
;main.c: 202: OPTION = 0x8F;
	movlw	(08Fh)
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movwf	(129)^080h	;volatile
	line	203
	
l6706:	
;main.c: 203: TRISC = 0x00;
	clrf	(135)^080h	;volatile
	line	204
	
l6708:	
;main.c: 204: TRISD = 0x00;
	clrf	(136)^080h	;volatile
	line	205
	
l6710:	
;main.c: 205: PORTB = 0x00;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(6)	;volatile
	line	206
	
l6712:	
;main.c: 206: PORTD = 0xFF;
	movlw	(0FFh)
	movwf	(8)	;volatile
	line	209
	
l6714:	
;main.c: 209: T1CON = 0x35;
	movlw	(035h)
	movwf	(16)	;volatile
	goto	l6716
	line	211
;main.c: 211: while(1)
	
l876:	
	line	213
	
l6716:	
;main.c: 212: {
;main.c: 213: PORTD ^= 0xff;
	movlw	(0FFh)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_init+0)+0
	movf	(??_init+0)+0,w
	xorwf	(8),f	;volatile
	line	214
	
l6718:	
;main.c: 214: _delay(800000);
	opt asmopt_off
movlw  5
movwf	((??_init+0)+0+2),f
movlw	11
movwf	((??_init+0)+0+1),f
	movlw	242
movwf	((??_init+0)+0),f
u3007:
	decfsz	((??_init+0)+0),f
	goto	u3007
	decfsz	((??_init+0)+0+1),f
	goto	u3007
	decfsz	((??_init+0)+0+2),f
	goto	u3007
	nop2
opt asmopt_on

	goto	l6716
	line	215
	
l877:	
	line	211
	goto	l6716
	
l878:	
	line	216
	
l879:	
	return
	opt stack 0
GLOBAL	__end_of_init
	__end_of_init:
;; =============== function _init ends ============

	signat	_init,88
	global	___lmul
psect	text479,local,class=CODE,delta=2
global __ptext479
__ptext479:

;; *************** function ___lmul *****************
;; Defined at:
;;		line 3 in file "C:\Program Files (x86)\HI-TECH Software\PICC\9.80\sources\lmul.c"
;; Parameters:    Size  Location     Type
;;  multiplier      4    0[BANK0 ] unsigned long 
;;  multiplicand    4    4[BANK0 ] unsigned long 
;; Auto vars:     Size  Location     Type
;;  product         4    9[BANK0 ] unsigned long 
;; Return value:  Size  Location     Type
;;                  4    0[BANK0 ] unsigned long 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       8       0       0       0
;;      Locals:         0       4       0       0       0
;;      Temps:          0       1       0       0       0
;;      Totals:         0      13       0       0       0
;;Total ram usage:       13 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_PIDcal
;; This function uses a non-reentrant model
;;
psect	text479
	file	"C:\Program Files (x86)\HI-TECH Software\PICC\9.80\sources\lmul.c"
	line	3
	global	__size_of___lmul
	__size_of___lmul	equ	__end_of___lmul-___lmul
	
___lmul:	
	opt	stack 4
; Regs used in ___lmul: [wreg+status,2+status,0]
	line	4
	
l6896:	
	movlw	0
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(___lmul@product+3)
	movlw	0
	movwf	(___lmul@product+2)
	movlw	0
	movwf	(___lmul@product+1)
	movlw	0
	movwf	(___lmul@product)

	goto	l6898
	line	6
	
l5219:	
	line	7
	
l6898:	
	btfss	(___lmul@multiplier),(0)&7
	goto	u2811
	goto	u2810
u2811:
	goto	l6902
u2810:
	line	8
	
l6900:	
	movf	(___lmul@multiplicand),w
	addwf	(___lmul@product),f
	movf	(___lmul@multiplicand+1),w
	clrz
	skipnc
	addlw	1
	skipnz
	goto	u2821
	addwf	(___lmul@product+1),f
u2821:
	movf	(___lmul@multiplicand+2),w
	clrz
	skipnc
	addlw	1
	skipnz
	goto	u2822
	addwf	(___lmul@product+2),f
u2822:
	movf	(___lmul@multiplicand+3),w
	clrz
	skipnc
	addlw	1
	skipnz
	goto	u2823
	addwf	(___lmul@product+3),f
u2823:

	goto	l6902
	
l5220:	
	line	9
	
l6902:	
	movlw	01h
	movwf	(??___lmul+0)+0
u2835:
	clrc
	rlf	(___lmul@multiplicand),f
	rlf	(___lmul@multiplicand+1),f
	rlf	(___lmul@multiplicand+2),f
	rlf	(___lmul@multiplicand+3),f
	decfsz	(??___lmul+0)+0
	goto	u2835
	line	10
	
l6904:	
	movlw	01h
u2845:
	clrc
	rrf	(___lmul@multiplier+3),f
	rrf	(___lmul@multiplier+2),f
	rrf	(___lmul@multiplier+1),f
	rrf	(___lmul@multiplier),f
	addlw	-1
	skipz
	goto	u2845

	line	11
	movf	(___lmul@multiplier+3),w
	iorwf	(___lmul@multiplier+2),w
	iorwf	(___lmul@multiplier+1),w
	iorwf	(___lmul@multiplier),w
	skipz
	goto	u2851
	goto	u2850
u2851:
	goto	l6898
u2850:
	goto	l6906
	
l5221:	
	line	12
	
l6906:	
	movf	(___lmul@product+3),w
	movwf	(?___lmul+3)
	movf	(___lmul@product+2),w
	movwf	(?___lmul+2)
	movf	(___lmul@product+1),w
	movwf	(?___lmul+1)
	movf	(___lmul@product),w
	movwf	(?___lmul)

	goto	l5222
	
l6908:	
	line	13
	
l5222:	
	return
	opt stack 0
GLOBAL	__end_of___lmul
	__end_of___lmul:
;; =============== function ___lmul ends ============

	signat	___lmul,8316
	global	_adc_convert
psect	text480,local,class=CODE,delta=2
global __ptext480
__ptext480:

;; *************** function _adc_convert *****************
;; Defined at:
;;		line 65 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\PID CONTROL\adc_func.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  result          2    8[BANK0 ] unsigned short 
;;  adresl          2    6[BANK0 ] unsigned short 
;;  adresh          2    4[BANK0 ] unsigned short 
;; Return value:  Size  Location     Type
;;                  2    0[BANK0 ] unsigned short 
;; Registers used:
;;		wreg, status,2, status,0, btemp+1
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       2       0       0       0
;;      Locals:         0       6       0       0       0
;;      Temps:          0       2       0       0       0
;;      Totals:         0      10       0       0       0
;;Total ram usage:       10 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_get_position
;;		_get_setpoint
;; This function uses a non-reentrant model
;;
psect	text480
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\PID CONTROL\adc_func.c"
	line	65
	global	__size_of_adc_convert
	__size_of_adc_convert	equ	__end_of_adc_convert-_adc_convert
	
_adc_convert:	
	opt	stack 4
; Regs used in _adc_convert: [wreg+status,2+status,0+btemp+1]
	line	69
	
l6872:	
;adc_func.c: 66: uint16_t adresh;
;adc_func.c: 67: uint16_t adresl;
;adc_func.c: 68: uint16_t result;
;adc_func.c: 69: adc_isr_flag = 0;
	movlw	low(0)
	movwf	(_adc_isr_flag)
	movlw	high(0)
	movwf	((_adc_isr_flag))+1
	line	71
	
l6874:	
;adc_func.c: 71: GODONE = 1;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	bsf	(249/8),(249)&7
	line	73
;adc_func.c: 73: while (adc_isr_flag == 0)
	goto	l6876
	
l1730:	
	goto	l6876
	line	74
;adc_func.c: 74: {}
	
l1729:	
	line	73
	
l6876:	
	movf	(_adc_isr_flag+1),w
	iorwf	(_adc_isr_flag),w
	skipnz
	goto	u2781
	goto	u2780
u2781:
	goto	l6876
u2780:
	goto	l6878
	
l1731:	
	line	75
	
l6878:	
;adc_func.c: 75: adresl = ADRESL;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movf	(158)^080h,w	;volatile
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_adc_convert+0)+0
	clrf	(??_adc_convert+0)+0+1
	movf	0+(??_adc_convert+0)+0,w
	movwf	(adc_convert@adresl)
	movf	1+(??_adc_convert+0)+0,w
	movwf	(adc_convert@adresl+1)
	line	76
;adc_func.c: 76: adresh = ADRESH;
	movf	(30),w	;volatile
	movwf	(??_adc_convert+0)+0
	clrf	(??_adc_convert+0)+0+1
	movf	0+(??_adc_convert+0)+0,w
	movwf	(adc_convert@adresh)
	movf	1+(??_adc_convert+0)+0,w
	movwf	(adc_convert@adresh+1)
	line	77
	
l6880:	
;adc_func.c: 77: result = ((adresh << 8) |(adresl));
	movf	(adc_convert@adresh+1),w
	movwf	(??_adc_convert+0)+0+1
	movf	(adc_convert@adresh),w
	movwf	(??_adc_convert+0)+0
	movlw	08h
	movwf	btemp+1
u2795:
	clrc
	rlf	(??_adc_convert+0)+0,f
	rlf	(??_adc_convert+0)+1,f
	decfsz	btemp+1,f
	goto	u2795
	movf	(adc_convert@adresl),w
	iorwf	0+(??_adc_convert+0)+0,w
	movwf	(adc_convert@result)
	movf	(adc_convert@adresl+1),w
	iorwf	1+(??_adc_convert+0)+0,w
	movwf	1+(adc_convert@result)
	line	79
	
l6882:	
;adc_func.c: 79: return result;
	movf	(adc_convert@result+1),w
	clrf	(?_adc_convert+1)
	addwf	(?_adc_convert+1)
	movf	(adc_convert@result),w
	clrf	(?_adc_convert)
	addwf	(?_adc_convert)

	goto	l1732
	
l6884:	
	line	80
	
l1732:	
	return
	opt stack 0
GLOBAL	__end_of_adc_convert
	__end_of_adc_convert:
;; =============== function _adc_convert ends ============

	signat	_adc_convert,90
	global	_get_timer
psect	text481,local,class=CODE,delta=2
global __ptext481
__ptext481:

;; *************** function _get_timer *****************
;; Defined at:
;;		line 33 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\PID CONTROL\timer.c"
;; Parameters:    Size  Location     Type
;;  index           1    wreg     unsigned char 
;; Auto vars:     Size  Location     Type
;;  index           1    5[BANK0 ] unsigned char 
;;  result          2    3[BANK0 ] unsigned short 
;; Return value:  Size  Location     Type
;;                  2    0[BANK0 ] unsigned short 
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       2       0       0       0
;;      Locals:         0       3       0       0       0
;;      Temps:          0       1       0       0       0
;;      Totals:         0       6       0       0       0
;;Total ram usage:        6 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text481
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\PID CONTROL\timer.c"
	line	33
	global	__size_of_get_timer
	__size_of_get_timer	equ	__end_of_get_timer-_get_timer
	
_get_timer:	
	opt	stack 5
; Regs used in _get_timer: [wreg-fsr0h+status,2+status,0]
;get_timer@index stored from wreg
	line	36
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(get_timer@index)
	
l6858:	
;timer.c: 34: uint16_t result;
;timer.c: 36: if (index < TIMER_MAX)
	movlw	(02h)
	subwf	(get_timer@index),w
	skipnc
	goto	u2771
	goto	u2770
u2771:
	goto	l6866
u2770:
	line	38
	
l6860:	
;timer.c: 37: {
;timer.c: 38: (GIE = 0);
	bcf	(95/8),(95)&7
	line	39
	
l6862:	
;timer.c: 39: result = timer_array[index];
	movf	(get_timer@index),w
	movwf	(??_get_timer+0)+0
	addwf	(??_get_timer+0)+0,w
	addlw	_timer_array&0ffh
	movwf	fsr0
	bcf	status, 7	;select IRP bank0
	movf	indf,w
	movwf	(get_timer@result)
	incf	fsr0,f
	movf	indf,w
	movwf	(get_timer@result+1)
	line	40
	
l6864:	
;timer.c: 40: (GIE = 1);
	bsf	(95/8),(95)&7
	line	41
;timer.c: 41: }
	goto	l6868
	line	42
	
l5119:	
	line	44
	
l6866:	
;timer.c: 42: else
;timer.c: 43: {
;timer.c: 44: result = 0;
	movlw	low(0)
	movwf	(get_timer@result)
	movlw	high(0)
	movwf	((get_timer@result))+1
	goto	l6868
	line	45
	
l5120:	
	line	47
	
l6868:	
;timer.c: 45: }
;timer.c: 47: return result;
	movf	(get_timer@result+1),w
	clrf	(?_get_timer+1)
	addwf	(?_get_timer+1)
	movf	(get_timer@result),w
	clrf	(?_get_timer)
	addwf	(?_get_timer)

	goto	l5121
	
l6870:	
	line	49
	
l5121:	
	return
	opt stack 0
GLOBAL	__end_of_get_timer
	__end_of_get_timer:
;; =============== function _get_timer ends ============

	signat	_get_timer,4218
	global	_timer_set
psect	text482,local,class=CODE,delta=2
global __ptext482
__ptext482:

;; *************** function _timer_set *****************
;; Defined at:
;;		line 18 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\PID CONTROL\timer.c"
;; Parameters:    Size  Location     Type
;;  index           1    wreg     unsigned char 
;;  value           2    0[BANK0 ] unsigned short 
;; Auto vars:     Size  Location     Type
;;  index           1    3[BANK0 ] unsigned char 
;;  array_conten    2    0        unsigned short 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       2       0       0       0
;;      Locals:         0       1       0       0       0
;;      Temps:          0       1       0       0       0
;;      Totals:         0       4       0       0       0
;;Total ram usage:        4 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text482
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\PID CONTROL\timer.c"
	line	18
	global	__size_of_timer_set
	__size_of_timer_set	equ	__end_of_timer_set-_timer_set
	
_timer_set:	
	opt	stack 5
; Regs used in _timer_set: [wreg-fsr0h+status,2+status,0]
;timer_set@index stored from wreg
	line	20
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(timer_set@index)
	
l6850:	
;timer.c: 19: uint16_t array_contents;
;timer.c: 20: if (index < TIMER_MAX)
	movlw	(02h)
	subwf	(timer_set@index),w
	skipnc
	goto	u2761
	goto	u2760
u2761:
	goto	l5116
u2760:
	line	22
	
l6852:	
;timer.c: 21: {
;timer.c: 22: (GIE = 0);
	bcf	(95/8),(95)&7
	line	23
	
l6854:	
;timer.c: 23: timer_array[index] = value;
	movf	(timer_set@index),w
	movwf	(??_timer_set+0)+0
	addwf	(??_timer_set+0)+0,w
	addlw	_timer_array&0ffh
	movwf	fsr0
	movf	(timer_set@value),w
	bcf	status, 7	;select IRP bank0
	movwf	indf
	incf	fsr0,f
	movf	(timer_set@value+1),w
	movwf	indf
	line	24
	
l6856:	
;timer.c: 24: (GIE = 1);
	bsf	(95/8),(95)&7
	line	25
;timer.c: 25: }
	goto	l5116
	line	26
	
l5114:	
	goto	l5116
	line	28
;timer.c: 26: else
;timer.c: 27: {
	
l5115:	
	line	29
	
l5116:	
	return
	opt stack 0
GLOBAL	__end_of_timer_set
	__end_of_timer_set:
;; =============== function _timer_set ends ============

	signat	_timer_set,8312
	global	_pwm_init
psect	text483,local,class=CODE,delta=2
global __ptext483
__ptext483:

;; *************** function _pwm_init *****************
;; Defined at:
;;		line 7 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\PID CONTROL\pwm.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text483
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\PID CONTROL\pwm.c"
	line	7
	global	__size_of_pwm_init
	__size_of_pwm_init	equ	__end_of_pwm_init-_pwm_init
	
_pwm_init:	
	opt	stack 5
; Regs used in _pwm_init: [wreg]
	line	8
	
l6848:	
;pwm.c: 8: PR2 = 0x60;
	movlw	(060h)
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movwf	(146)^080h	;volatile
	line	9
;pwm.c: 9: T2CON = 0x06;
	movlw	(06h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(18)	;volatile
	line	10
;pwm.c: 10: CCP1CON = 0x0C;
	movlw	(0Ch)
	movwf	(23)	;volatile
	line	12
	
l3426:	
	return
	opt stack 0
GLOBAL	__end_of_pwm_init
	__end_of_pwm_init:
;; =============== function _pwm_init ends ============

	signat	_pwm_init,88
	global	_adc_init
psect	text484,local,class=CODE,delta=2
global __ptext484
__ptext484:

;; *************** function _adc_init *****************
;; Defined at:
;;		line 111 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\PID CONTROL\adc_func.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text484
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\PID CONTROL\adc_func.c"
	line	111
	global	__size_of_adc_init
	__size_of_adc_init	equ	__end_of_adc_init-_adc_init
	
_adc_init:	
	opt	stack 5
; Regs used in _adc_init: [wreg]
	line	112
	
l6846:	
;adc_func.c: 112: TRISA = 0x06;
	movlw	(06h)
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movwf	(133)^080h	;volatile
	line	113
;adc_func.c: 113: ANSEL = 0x06;
	movlw	(06h)
	bsf	status, 5	;RP0=1, select bank3
	bsf	status, 6	;RP1=1, select bank3
	movwf	(392)^0180h	;volatile
	line	114
;adc_func.c: 114: ADCON0 = 0xC1;
	movlw	(0C1h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(31)	;volatile
	line	115
;adc_func.c: 115: ADCON1 = 0x80;
	movlw	(080h)
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movwf	(159)^080h	;volatile
	line	116
	
l1741:	
	return
	opt stack 0
GLOBAL	__end_of_adc_init
	__end_of_adc_init:
;; =============== function _adc_init ends ============

	signat	_adc_init,88
	global	_interrupt_handler
psect	text485,local,class=CODE,delta=2
global __ptext485
__ptext485:

;; *************** function _interrupt_handler *****************
;; Defined at:
;;		line 7 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\PID CONTROL\interrupt.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          4       0       0       0       0
;;      Totals:         4       0       0       0       0
;;Total ram usage:        4 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		_adc_isr
;;		_timer_isr
;; This function is called by:
;;		Interrupt level 1
;; This function uses a non-reentrant model
;;
psect	text485
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\PID CONTROL\interrupt.c"
	line	7
	global	__size_of_interrupt_handler
	__size_of_interrupt_handler	equ	__end_of_interrupt_handler-_interrupt_handler
	
_interrupt_handler:	
	opt	stack 4
; Regs used in _interrupt_handler: [wreg-fsr0h+status,2+status,0+pclath+cstack]
psect	intentry,class=CODE,delta=2
global __pintentry
__pintentry:
global interrupt_function
interrupt_function:
	global saved_w
	saved_w	set	btemp+0
	movwf	saved_w
	movf	status,w
	movwf	(??_interrupt_handler+0)
	movf	fsr0,w
	movwf	(??_interrupt_handler+1)
	movf	pclath,w
	movwf	(??_interrupt_handler+2)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	btemp+1,w
	movwf	(??_interrupt_handler+3)
	ljmp	_interrupt_handler
psect	text485
	line	9
	
i1l6648:	
;interrupt.c: 9: adc_isr();
	fcall	_adc_isr
	line	10
	
i1l6650:	
;interrupt.c: 10: timer_isr();
	fcall	_timer_isr
	line	11
	
i1l2589:	
	movf	(??_interrupt_handler+3),w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	btemp+1
	movf	(??_interrupt_handler+2),w
	movwf	pclath
	movf	(??_interrupt_handler+1),w
	movwf	fsr0
	movf	(??_interrupt_handler+0),w
	movwf	status
	swapf	saved_w,f
	swapf	saved_w,w
	retfie
	opt stack 0
GLOBAL	__end_of_interrupt_handler
	__end_of_interrupt_handler:
;; =============== function _interrupt_handler ends ============

	signat	_interrupt_handler,88
	global	_timer_isr
psect	text486,local,class=CODE,delta=2
global __ptext486
__ptext486:

;; *************** function _timer_isr *****************
;; Defined at:
;;		line 54 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\PID CONTROL\timer.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  i               1    1[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         1       0       0       0       0
;;      Temps:          1       0       0       0       0
;;      Totals:         2       0       0       0       0
;;Total ram usage:        2 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_interrupt_handler
;; This function uses a non-reentrant model
;;
psect	text486
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\PID CONTROL\timer.c"
	line	54
	global	__size_of_timer_isr
	__size_of_timer_isr	equ	__end_of_timer_isr-_timer_isr
	
_timer_isr:	
	opt	stack 4
; Regs used in _timer_isr: [wreg-fsr0h+status,2+status,0]
	line	56
	
i1l6652:	
;timer.c: 55: uint8_t i;
;timer.c: 56: if((TMR1IE)&&(TMR1IF))
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	btfss	(1120/8)^080h,(1120)&7
	goto	u250_21
	goto	u250_20
u250_21:
	goto	i1l5130
u250_20:
	
i1l6654:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfss	(96/8),(96)&7
	goto	u251_21
	goto	u251_20
u251_21:
	goto	i1l5130
u251_20:
	line	59
	
i1l6656:	
;timer.c: 57: {
;timer.c: 59: TMR1IF=0;
	bcf	(96/8),(96)&7
	line	60
	
i1l6658:	
;timer.c: 60: T1CON &= ~(0x01);
	movlw	(0FEh)
	movwf	(??_timer_isr+0)+0
	movf	(??_timer_isr+0)+0,w
	andwf	(16),f	;volatile
	line	61
	
i1l6660:	
;timer.c: 61: TMR1L = 0x08;
	movlw	(08h)
	movwf	(14)	;volatile
	line	62
	
i1l6662:	
;timer.c: 62: TMR1H = 0xFF;
	movlw	(0FFh)
	movwf	(15)	;volatile
	line	63
	
i1l6664:	
;timer.c: 63: T1CON |= 0x01;
	bsf	(16)+(0/8),(0)&7	;volatile
	line	65
;timer.c: 65: for (i = 0; i < TIMER_MAX; i++)
	clrc
	movlw	0
	btfsc	status,0
	movlw	1
	movwf	(timer_isr@i)
	
i1l6666:	
	movlw	(02h)
	subwf	(timer_isr@i),w
	skipc
	goto	u252_21
	goto	u252_20
u252_21:
	goto	i1l6670
u252_20:
	goto	i1l5130
	
i1l6668:	
	goto	i1l5130
	line	66
	
i1l5125:	
	line	67
	
i1l6670:	
;timer.c: 66: {
;timer.c: 67: if (timer_array[i] != 0)
	movf	(timer_isr@i),w
	movwf	(??_timer_isr+0)+0
	addwf	(??_timer_isr+0)+0,w
	addlw	_timer_array&0ffh
	movwf	fsr0
	bcf	status, 7	;select IRP bank0
	movf	indf,w
	incf	fsr0,f
	iorwf	indf,w
	skipnz
	goto	u253_21
	goto	u253_20
u253_21:
	goto	i1l6674
u253_20:
	line	69
	
i1l6672:	
;timer.c: 68: {
;timer.c: 69: timer_array[i]--;
	movf	(timer_isr@i),w
	movwf	(??_timer_isr+0)+0
	addwf	(??_timer_isr+0)+0,w
	addlw	_timer_array&0ffh
	movwf	fsr0
	movlw	low(01h)
	subwf	indf,f
	incfsz	fsr0,f
	movlw	high(01h)
	skipc
	decf	indf,f
	subwf	indf,f
	decf	fsr0,f
	line	70
;timer.c: 70: }
	goto	i1l6674
	line	71
	
i1l5127:	
	goto	i1l6674
	line	73
;timer.c: 71: else
;timer.c: 72: {
	
i1l5128:	
	line	65
	
i1l6674:	
	movlw	(01h)
	movwf	(??_timer_isr+0)+0
	movf	(??_timer_isr+0)+0,w
	addwf	(timer_isr@i),f
	
i1l6676:	
	movlw	(02h)
	subwf	(timer_isr@i),w
	skipc
	goto	u254_21
	goto	u254_20
u254_21:
	goto	i1l6670
u254_20:
	goto	i1l5130
	
i1l5126:	
	line	76
;timer.c: 73: }
;timer.c: 74: }
;timer.c: 76: }
	goto	i1l5130
	line	77
	
i1l5124:	
	goto	i1l5130
	line	79
;timer.c: 77: else
;timer.c: 78: {
	
i1l5129:	
	line	80
	
i1l5130:	
	return
	opt stack 0
GLOBAL	__end_of_timer_isr
	__end_of_timer_isr:
;; =============== function _timer_isr ends ============

	signat	_timer_isr,88
	global	_adc_isr
psect	text487,local,class=CODE,delta=2
global __ptext487
__ptext487:

;; *************** function _adc_isr *****************
;; Defined at:
;;		line 119 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\PID CONTROL\adc_func.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_interrupt_handler
;; This function uses a non-reentrant model
;;
psect	text487
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\PID CONTROL\adc_func.c"
	line	119
	global	__size_of_adc_isr
	__size_of_adc_isr	equ	__end_of_adc_isr-_adc_isr
	
_adc_isr:	
	opt	stack 4
; Regs used in _adc_isr: [wreg]
	line	120
	
i1l6640:	
;adc_func.c: 120: if ((ADIF)&&(ADIE))
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfss	(102/8),(102)&7
	goto	u248_21
	goto	u248_20
u248_21:
	goto	i1l1746
u248_20:
	
i1l6642:	
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	btfss	(1126/8)^080h,(1126)&7
	goto	u249_21
	goto	u249_20
u249_21:
	goto	i1l1746
u249_20:
	line	123
	
i1l6644:	
;adc_func.c: 121: {
;adc_func.c: 123: ADIF=0;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	bcf	(102/8),(102)&7
	line	124
	
i1l6646:	
;adc_func.c: 124: adc_isr_flag=1;
	movlw	low(01h)
	movwf	(_adc_isr_flag)
	movlw	high(01h)
	movwf	((_adc_isr_flag))+1
	line	125
;adc_func.c: 125: int_count++;
	movlw	low(01h)
	addwf	(_int_count),f
	skipnc
	incf	(_int_count+1),f
	movlw	high(01h)
	addwf	(_int_count+1),f
	line	126
;adc_func.c: 126: }
	goto	i1l1746
	line	127
	
i1l1744:	
	goto	i1l1746
	line	129
;adc_func.c: 127: else
;adc_func.c: 128: {
	
i1l1745:	
	line	130
	
i1l1746:	
	return
	opt stack 0
GLOBAL	__end_of_adc_isr
	__end_of_adc_isr:
;; =============== function _adc_isr ends ============

	signat	_adc_isr,88
psect	text488,local,class=CODE,delta=2
global __ptext488
__ptext488:
	global	btemp
	btemp set 07Eh

	DABS	1,126,2	;btemp
	global	wtemp0
	wtemp0 set btemp
	end
