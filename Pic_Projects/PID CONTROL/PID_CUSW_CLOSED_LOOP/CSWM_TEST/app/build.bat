@echo off
cls

set ERROR_FLAG=0
set COSMIC_PATH=N:\Share\CortesC\Compiler\Cosmic_Compiler\CX12.478

call comp_app.bat
call comp_app_paged.bat
call comp_autosar.bat
call comp_can_nwm_pwrnet.bat
call comp_flash_drv.bat
call link.bat

pause
