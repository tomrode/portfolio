@echo off

if %ERROR_FLAG%==1 goto done

echo ****************
echo * FLASH Driver *
echo ****************

set BASEPATH=c:\cosmic_projects\CSWM_TEST\app
set SRCPATH=%BASEPATH%\src\flash_drv

set SRCFILES=^
%SRCPATH%\FblIf.c ^
%SRCPATH%\hwio_eeprom.c ^
%SRCPATH%\hwio_eeprom_cfg.c ^
%SRCPATH%\hwio_eeprom_direct.c ^
%SRCPATH%\memfunc.c ^
%SRCPATH%\mw_eem.c ^
%SRCPATH%\mw_tmrm.c

set OPTLIST=^
-f%BASEPATH%\cswm_MY12_compiler.cxf ^
-i%BASEPATH%\src\app ^
-i%BASEPATH%\src\app_paged ^
-i%BASEPATH%\src\autosar ^
-i%BASEPATH%\src\can_nwm_pwrnet ^
-i%BASEPATH%\src\flash_drv ^
-i%COSMIC_PATH%\Hs12x ^
-dPWR_NET -e -l +xe +debug +modf -pp ^
-ce%BASEPATH%\err ^
-cl%BASEPATH%\lst ^
-co%BASEPATH%\obj

%COSMIC_PATH%\cxs12x %OPTLIST% %SRCFILES%

if ERRORLEVEL 1 set ERROR_FLAG=1

:done
