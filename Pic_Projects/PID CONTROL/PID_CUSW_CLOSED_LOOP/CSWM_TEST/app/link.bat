@echo off

if %ERROR_FLAG%==1 goto done

echo ***********
echo * Linking *
echo ***********

set COSMIC_PATH=c:\progra~1\cosmic\cxs12x
set BASEPATH=c:\cosmic_projects\CSWM_TEST\app
set PROJNAME=cswmMY12_PwrNet

set OPTLIST=^
-m%BASEPATH%\out\%PROJNAME%.map ^
-p ^
-l%COSMIC_PATH%\lib ^
-e%BASEPATH%\err\%PROJNAME%.err ^
-o%BASEPATH%\out\%PROJNAME%.h12 ^
%BASEPATH%\lnk\cswmMY12_PwrNet_link.cmd

%COSMIC_PATH%\clnk.exe %OPTLIST%

if ERRORLEVEL 1 set ERROR_FLAG=1

if %ERROR_FLAG%==1 goto done

echo *********
echo * Debug *
echo *********

%COSMIC_PATH%\cprd.exe %BASEPATH%\out\%PROJNAME%.h12 -s -r > %BASEPATH%\out\%PROJNAME%.debug

if ERRORLEVEL 1 set ERROR_FLAG=1

if %ERROR_FLAG%==1 goto done

echo ************
echo * S-Record *
echo ************

%COSMIC_PATH%\chex.exe -o %BASEPATH%\out\%PROJNAME%.s19 %BASEPATH%\out\%PROJNAME%.h12

if ERRORLEVEL 1 set ERROR_FLAG=1

:done