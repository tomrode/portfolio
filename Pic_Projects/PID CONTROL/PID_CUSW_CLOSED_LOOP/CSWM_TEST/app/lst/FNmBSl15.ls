   1                         ; C Compiler for S12XE [COSMIC Software].
   2                         ; Generator V4.7.8 - 27 Jul 2007
   3                         ; Optimizer V4.6.13 - 27 Jul 2007
   4                         .const:	section	.data
   5  0000                   _kNmMainVersion:
   6  0000 01                	dc.b	1
   7  0001                   _kNmSubVersion:
   8  0001 30                	dc.b	48
   9  0002                   _kNmReleaseVersion:
  10  0002 01                	dc.b	1
  55                         ; 522 static void NmSetTxMsgReq( vuint8 command )
  55                         ; 523 {
  56                         .ftext:	section	.text
  57  0000                   L32f_NmSetTxMsgReq:
  59  0000 3b                	pshd	
  60       00000000          OFST:	set	0
  63                         ; 525   nmStatusInfo.flag.DllErrorSts = NmGetDllState();
  65  0001 4a01e6e6          	call	f_NmGetDllState
  67  0005 56                	rorb	
  68  0006 56                	rorb	
  69  0007 56                	rorb	
  70  0008 1d0004c0          	bclr	L11_nmStatusInfo,192
  71  000c c4c0              	andb	#192
  72  000e fa0004            	orab	L11_nmStatusInfo
  73  0011 7b0004            	stab	L11_nmStatusInfo
  74                         ; 528   nmStatusInfo.flag.SystemCommand = command;
  76  0014 e681              	ldab	OFST+1,s
  77  0016 1d000403          	bclr	L11_nmStatusInfo,3
  78  001a c403              	andb	#3
  79  001c fa0004            	orab	L11_nmStatusInfo
  80  001f 7b0004            	stab	L11_nmStatusInfo
  81                         ; 530   nmSendMess[0] = 0; /* zero byte */
  83  0022 790008            	clr	_nmSendMess
  84                         ; 531   nmSendMess[1] = nmStatusInfo.value; /* copy from INT to EXT */
  86  0025 7b0009            	stab	_nmSendMess+1
  87                         ; 534   nmTxMsgReq = 1;
  89  0028 c601              	ldab	#1
  90  002a 7b0000            	stab	L12_nmTxMsgReq
  91                         ; 535 }
  94  002d 31                	puly	
  95  002e 0a                	rtc	
 118                         ; 551 void NmInitPowerOn ( void )
 118                         ; 552 {
 119                         	switch	.ftext
 120  002f                   f_NmInitPowerOn:
 124                         ; 562   NmInit();
 126  002f 4a003434          	call	f_NmInit
 128                         ; 563 }
 131  0033 0a                	rtc	
 164                         ; 574 void NmInit ( void )
 164                         ; 575 {
 165                         	switch	.ftext
 166  0034                   f_NmInit:
 170                         ; 586   nmNodeState = kNmNetOff;
 172  0034 c601              	ldab	#1
 173  0036 7b0005            	stab	L7_nmNodeState
 174                         ; 587   nmInternalStateReq = kNmPowerOff; /* no application network request */
 176  0039 c6ff              	ldab	#255
 177  003b 7b0003            	stab	L31_nmInternalStateReq
 178                         ; 588   nmExternalStateReq = kNmNoCommand; /* no external message network request */
 180  003e 7b0002            	stab	L51_nmExternalStateReq
 181                         ; 591   nmBusOffState = 0;
 183  0041 790001            	clr	L71_nmBusOffState
 184                         ; 594   nmTxMsgReq = 0;
 186  0044 790000            	clr	L12_nmTxMsgReq
 187                         ; 597   nmNetworkTimer = kNmTimerOff;   /* init network timer */
 189  0047 790007            	clr	L3_nmNetworkTimer
 190                         ; 598   nmBusOffTimer = kNmTimerOff;    /* init busoff timer */
 192  004a 790006            	clr	L5_nmBusOffTimer
 193                         ; 601   nmStatusInfo.flag.SystemCommand = kNmGoSleepCommand;
 195  004d 1d0004fc          	bclr	L11_nmStatusInfo,252
 196  0051 1c000403          	bset	L11_nmStatusInfo,3
 197                         ; 602   nmStatusInfo.flag.ActiveLoads = kNmNoActiveLoads;
 199                         ; 603   nmStatusInfo.flag.EndOfLineSts = kNmNoEolProgAtFIAT;
 201                         ; 604   nmStatusInfo.flag.GenericFailSts = kNmNoFunctionFail;
 203                         ; 605   nmStatusInfo.flag.CurrentFailSts = kNmNoFunctionFail;
 205                         ; 606   nmStatusInfo.flag.DllErrorSts = kNmErrorActive;
 207                         ; 611   CanOffline(NM_NMTOCAN_CHANNEL_IND);
 209  0055 4a000000          	call	f_CanOffline
 211                         ; 616   CanResetBusSleep( kNmCanPara );
 213  0059 87                	clra	
 214  005a c7                	clrb	
 215  005b 4a000000          	call	f_CanInit
 217                         ; 620   ApplNmCanSleep();
 219  005f 4a000000          	call	f_ApplNmCanSleep
 221                         ; 621 }
 224  0063 0a                	rtc	
 310                         ; 643 vuint8 NmPrecopy ( NM_PRECOPY_PARAMETER_TYPE NM_PRECOPY_PARAMETER_NAME )
 310                         ; 644 #endif
 310                         ; 645 {
 311                         	switch	.ftext
 312  0064                   f_NmPrecopy:
 314       00000001          OFST:	set	1
 317                         ; 651   (void)NM_PRECOPY_PARAMETER_NAME;
 319                         ; 655   if( nmBusOffTimer == 0 )
 321  0064 f70006            	tst	L5_nmBusOffTimer
 322  0067 260b              	bne	L131
 323                         ; 661     bLocalRxNmByte = NM_PRECOPY_MSG_DATA;
 325  0069 b746              	tfr	d,y
 326  006b ed44              	ldy	4,y
 327  006d e641              	ldab	1,y
 328                         ; 663     nmExternalStateReq = bLocalRxNmByte & kNmScMask;
 330  006f c403              	andb	#3
 331  0071 7b0002            	stab	L51_nmExternalStateReq
 332  0074                   L131:
 333                         ; 680   return (kCanNoCopyData); /* no further CAN driver handling */
 335  0074 c7                	clrb	
 338  0075 0a                	rtc	
 368                         ; 692 void NmCanError( NM_CHANNEL_CANTYPE_ONLY )
 368                         ; 693 {
 369                         	switch	.ftext
 370  0076                   f_NmCanError:
 374                         ; 716   nmBusOffState = 1;
 376  0076 c601              	ldab	#1
 377  0078 7b0001            	stab	L71_nmBusOffState
 378                         ; 718   nmNetworkTimer = kNmTimerOff;
 380  007b 790007            	clr	L3_nmNetworkTimer
 381                         ; 720   nmBusOffTimer = kNmBusOffRecoveryTime;
 383  007e c665              	ldab	#101
 384  0080 7b0006            	stab	L5_nmBusOffTimer
 385                         ; 722   nmTxMsgReq = 0;
 387  0083 790000            	clr	L12_nmTxMsgReq
 388                         ; 724   nmStatusInfo.flag.DllErrorSts = kNmBusOff;
 390  0086 1d0004c0          	bclr	L11_nmStatusInfo,192
 391  008a 1c000480          	bset	L11_nmStatusInfo,128
 392                         ; 727   CanOffline( NM_CHANNEL_CANPARA_ONLY );
 394  008e 4a000000          	call	f_CanOffline
 396                         ; 732   CanResetBusOffStart( kNmCanPara );
 398  0092 87                	clra	
 399  0093 c7                	clrb	
 400  0094 4a000000          	call	f_CanInit
 402                         ; 736   ApplNmBusOff();
 404  0098 4a000000          	call	f_ApplNmBusOff
 406                         ; 738 }
 409  009c 0a                	rtc	
 442                         ; 749 void NmConfirmation ( CanTransmitHandle txObject )
 442                         ; 750 {
 443                         	switch	.ftext
 444  009d                   f_NmConfirmation:
 448                         ; 761   (void)txObject;
 450                         ; 766   if(nmBusOffState != 0x00)
 452  009d f60001            	ldab	L71_nmBusOffState
 453  00a0 2704              	beq	L751
 454                         ; 769     ApplNmBusOffEnd();
 456  00a2 4a000000          	call	f_ApplNmBusOffEnd
 458  00a6                   L751:
 459                         ; 773   nmBusOffState = 0;
 461  00a6 790001            	clr	L71_nmBusOffState
 462                         ; 776 }
 465  00a9 0a                	rtc	
 514                         ; 787 void NmTask ( void )
 514                         ; 788 {
 515                         	switch	.ftext
 516  00aa                   f_NmTask:
 518  00aa 37                	pshb	
 519       00000001          OFST:	set	1
 522                         ; 796   if( nmNodeState == kNmNetOn )
 524  00ab f60005            	ldab	L7_nmNodeState
 525  00ae c102              	cmpb	#2
 526  00b0 2620              	bne	L571
 527                         ; 799     NmInterruptDisable();
 529  00b2 4a000000          	call	f_VStdSuspendAllInterrupts
 531                         ; 801     if( nmBusOffTimer != 0x00 )
 533  00b6 f70006            	tst	L5_nmBusOffTimer
 534  00b9 2713              	beq	L771
 535                         ; 804       nmBusOffTimer--;
 537  00bb 730006            	dec	L5_nmBusOffTimer
 538                         ; 807       if( nmBusOffTimer == 0x00 )
 540  00be 2607              	bne	L102
 541                         ; 819           nmNetworkTimer = kNmStayActiveTime + (vuint8)1;
 544  00c0 c6c9              	ldab	#201
 545  00c2 7b0007            	stab	L3_nmNetworkTimer
 547  00c5 2007              	bra	L771
 548  00c7                   L102:
 549                         ; 826         NmInterruptRestore();
 551  00c7 4a000000          	call	f_VStdResumeAllInterrupts
 553                         ; 827         return;
 556  00cb 1b81              	leas	1,s
 557  00cd 0a                	rtc	
 558  00ce                   L771:
 559                         ; 831     NmInterruptRestore();
 561  00ce 4a000000          	call	f_VStdResumeAllInterrupts
 563  00d2                   L571:
 564                         ; 835   NmInterruptDisable();
 566  00d2 4a000000          	call	f_VStdSuspendAllInterrupts
 568                         ; 840   if( nmNodeState == kNmNetOn )
 570  00d6 f60005            	ldab	L7_nmNodeState
 571  00d9 c102              	cmpb	#2
 572  00db 265f              	bne	L502
 573                         ; 848     if( nmInternalStateReq == kNmNetOff )
 575  00dd f60003            	ldab	L31_nmInternalStateReq
 576  00e0 042133            	dbne	b,L702
 577                         ; 851       nmTxMsgReq = 0;
 579  00e3 790000            	clr	L12_nmTxMsgReq
 580                         ; 853       nmNetworkTimer = kNmTimerOff;
 582  00e6 790007            	clr	L3_nmNetworkTimer
 583                         ; 855       nmStatusInfo.flag.ActiveLoads = kNmNoActiveLoads;
 585  00e9 1d000404          	bclr	L11_nmStatusInfo,4
 586                         ; 857       nmNodeState = kNmNetOff;
 588  00ed c601              	ldab	#1
 589  00ef 7b0005            	stab	L7_nmNodeState
 590                         ; 860       if( nmBusOffTimer != 0x00 )
 592  00f2 f70006            	tst	L5_nmBusOffTimer
 593  00f5 2703              	beq	L112
 594                         ; 863         nmBusOffTimer = kNmTimerOff;
 596  00f7 790006            	clr	L5_nmBusOffTimer
 598  00fa                   L112:
 599                         ; 872       if( nmBusOffState != 0 )
 601  00fa f60001            	ldab	L71_nmBusOffState
 602  00fd 2707              	beq	L312
 603                         ; 875         ApplNmBusOffEnd();
 605  00ff 4a000000          	call	f_ApplNmBusOffEnd
 607                         ; 878         nmBusOffState = 0;
 609  0103 790001            	clr	L71_nmBusOffState
 610  0106                   L312:
 611                         ; 882       CanOffline(NM_NMTOCAN_CHANNEL_IND);
 613  0106 4a000000          	call	f_CanOffline
 615                         ; 888       CanResetBusSleep( kNmCanPara );
 617  010a 87                	clra	
 618  010b c7                	clrb	
 619  010c 4a000000          	call	f_CanInit
 621                         ; 892       ApplNmCanSleep();
 623  0110 4a000000          	call	f_ApplNmCanSleep
 626  0114 2049              	bra	L322
 627  0116                   L702:
 628                         ; 903       if( nmExternalStateReq == kNmStayActiveCommand )
 630  0116 f60002            	ldab	L51_nmExternalStateReq
 631  0119 c102              	cmpb	#2
 632  011b 260c              	bne	L712
 633                         ; 906         NmSetTxMsgReq( kNmStayActiveCommand );
 635  011d cc0002            	ldd	#2
 636  0120 4a000000          	call	L32f_NmSetTxMsgReq
 638                         ; 908         nmNetworkTimer = kNmStayActiveTime + (vuint8)1;
 640  0124 c6c9              	ldab	#201
 641  0126 7b0007            	stab	L3_nmNetworkTimer
 642  0129                   L712:
 643                         ; 918       nmNetworkTimer--;
 645  0129 730007            	dec	L3_nmNetworkTimer
 646                         ; 920       if( nmNetworkTimer == 0x00 )
 648  012c 2631              	bne	L322
 649                         ; 923         NmSetTxMsgReq( kNmStayActiveCommand );
 651  012e cc0002            	ldd	#2
 652  0131 4a000000          	call	L32f_NmSetTxMsgReq
 654                         ; 925         nmNetworkTimer = kNmStayActiveTime;
 656  0135 c6c8              	ldab	#200
 657  0137 7b0007            	stab	L3_nmNetworkTimer
 658  013a 2023              	bra	L322
 659  013c                   L502:
 660                         ; 940     if( nmInternalStateReq == kNmNetOn )
 662  013c f60003            	ldab	L31_nmInternalStateReq
 663  013f c102              	cmpb	#2
 664  0141 261c              	bne	L322
 665                         ; 943       nmNetworkTimer = kNmStayActiveTime;
 667  0143 c6c8              	ldab	#200
 668  0145 7b0007            	stab	L3_nmNetworkTimer
 669                         ; 945       nmStatusInfo.flag.ActiveLoads = kNmActiveLoads;
 671  0148 1c000404          	bset	L11_nmStatusInfo,4
 672                         ; 947       nmNodeState = kNmNetOn;
 674  014c c602              	ldab	#2
 675  014e 7b0005            	stab	L7_nmNodeState
 676                         ; 950       CanOnline(NM_NMTOCAN_CHANNEL_IND);
 678  0151 4a000000          	call	f_CanOnline
 680                         ; 952       NmSetTxMsgReq( kNmWakeUpCommand );
 682  0155 87                	clra	
 683  0156 c7                	clrb	
 684  0157 4a000000          	call	L32f_NmSetTxMsgReq
 686                         ; 955       ApplNmCanNormal();
 688  015b 4a000000          	call	f_ApplNmCanNormal
 690  015f                   L322:
 691                         ; 958   nmInternalStateReq = kNmPowerOff; /* no slave application network request */
 693  015f c6ff              	ldab	#255
 694  0161 7b0003            	stab	L31_nmInternalStateReq
 695                         ; 959   nmExternalStateReq = kNmNoCommand; /* no master message network request */
 697  0164 7b0002            	stab	L51_nmExternalStateReq
 698                         ; 963   if( nmTxMsgReq != 0x00 )
 700  0167 f60000            	ldab	L12_nmTxMsgReq
 701  016a 2718              	beq	L722
 702                         ; 965     if(nmBusOffState != 0)
 704  016c f60001            	ldab	L71_nmBusOffState
 705  016f 2704              	beq	L132
 706                         ; 972       CanOnline(NM_NMTOCAN_CHANNEL_IND);
 708  0171 4a000000          	call	f_CanOnline
 710  0175                   L132:
 711                         ; 975     bLocalReturnValue = CanTransmit(kNmTxObj);
 713  0175 cc0003            	ldd	#3
 714  0178 4a000000          	call	f_CanTransmit
 716  017c 6b80              	stab	OFST-1,s
 717                         ; 977     if( bLocalReturnValue == kCanTxOk )
 719  017e 042103            	dbne	b,L722
 720                         ; 980       nmTxMsgReq = 0;
 722  0181 790000            	clr	L12_nmTxMsgReq
 723  0184                   L722:
 724                         ; 985   NmInterruptRestore();
 726  0184 4a000000          	call	f_VStdResumeAllInterrupts
 728                         ; 988 } /* PRQA S 4700 */ /* 4700 Metric data for information only */
 731  0188 1b81              	leas	1,s
 732  018a 0a                	rtc	
 766                         ; 1003 void NmSetNewMode ( vuint8 state )
 766                         ; 1004 {
 767                         	switch	.ftext
 768  018b                   f_NmSetNewMode:
 770  018b 3b                	pshd	
 771       00000000          OFST:	set	0
 774                         ; 1018   NmApiInterruptDisable();
 776  018c 4a000000          	call	f_VStdSuspendAllInterrupts
 778                         ; 1022   nmInternalStateReq = state;
 780  0190 180d810003        	movb	OFST+1,s,L31_nmInternalStateReq
 781                         ; 1027   NmApiInterruptRestore();
 783  0195 4a000000          	call	f_VStdResumeAllInterrupts
 785                         ; 1029 }
 788  0199 31                	puly	
 789  019a 0a                	rtc	
 823                         ; 1041 void NmSetEolState ( vuint8 state )
 823                         ; 1042 {
 824                         	switch	.ftext
 825  019b                   f_NmSetEolState:
 827  019b 3b                	pshd	
 828       00000000          OFST:	set	0
 831                         ; 1053   NmApiInterruptDisable();
 833  019c 4a000000          	call	f_VStdSuspendAllInterrupts
 835                         ; 1056   nmStatusInfo.flag.EndOfLineSts = state;
 837  01a0 0f810106          	brclr	OFST+1,s,1,L62
 838  01a4 1c000408          	bset	L11_nmStatusInfo,8
 839  01a8 2004              	bra	L03
 840  01aa                   L62:
 841  01aa 1d000408          	bclr	L11_nmStatusInfo,8
 842  01ae                   L03:
 843                         ; 1059   NmApiInterruptRestore();
 845  01ae 4a000000          	call	f_VStdResumeAllInterrupts
 847                         ; 1060 }
 850  01b2 31                	puly	
 851  01b3 0a                	rtc	
 886                         ; 1072 void NmSetGenericFailState ( vuint8 state )
 886                         ; 1073 {
 887                         	switch	.ftext
 888  01b4                   f_NmSetGenericFailState:
 890  01b4 3b                	pshd	
 891       00000000          OFST:	set	0
 894                         ; 1084   NmApiInterruptDisable();
 896  01b5 4a000000          	call	f_VStdSuspendAllInterrupts
 898                         ; 1087   nmStatusInfo.flag.GenericFailSts = state;
 900  01b9 0f810106          	brclr	OFST+1,s,1,L43
 901  01bd 1c000410          	bset	L11_nmStatusInfo,16
 902  01c1 2004              	bra	L63
 903  01c3                   L43:
 904  01c3 1d000410          	bclr	L11_nmStatusInfo,16
 905  01c7                   L63:
 906                         ; 1090   NmApiInterruptRestore();
 908  01c7 4a000000          	call	f_VStdResumeAllInterrupts
 910                         ; 1091 }
 913  01cb 31                	puly	
 914  01cc 0a                	rtc	
 949                         ; 1103 void NmSetCurrentFailState ( vuint8 state )
 949                         ; 1104 {
 950                         	switch	.ftext
 951  01cd                   f_NmSetCurrentFailState:
 953  01cd 3b                	pshd	
 954       00000000          OFST:	set	0
 957                         ; 1114   NmApiInterruptDisable();
 959  01ce 4a000000          	call	f_VStdSuspendAllInterrupts
 961                         ; 1117   nmStatusInfo.flag.CurrentFailSts = state;
 963  01d2 0f810106          	brclr	OFST+1,s,1,L24
 964  01d6 1c000420          	bset	L11_nmStatusInfo,32
 965  01da 2004              	bra	L44
 966  01dc                   L24:
 967  01dc 1d000420          	bclr	L11_nmStatusInfo,32
 968  01e0                   L44:
 969                         ; 1120   NmApiInterruptRestore();
 971  01e0 4a000000          	call	f_VStdResumeAllInterrupts
 973                         ; 1121 }
 976  01e4 31                	puly	
 977  01e5 0a                	rtc	
1012                         ; 1134 vuint8 NmGetDllState ( void )
1012                         ; 1135 {
1013                         	switch	.ftext
1014  01e6                   f_NmGetDllState:
1016  01e6 37                	pshb	
1017       00000001          OFST:	set	1
1020                         ; 1139   NmApiInterruptDisable();
1022  01e7 4a000000          	call	f_VStdSuspendAllInterrupts
1024                         ; 1142   if( nmBusOffState != 0 )
1026  01eb f60001            	ldab	L71_nmBusOffState
1027                         ; 1145     dllStatus = kNmBusOff;
1030  01ee 2626              	bne	LC002
1031                         ; 1150     dllStatus = CanGetStatus(NM_NMTOCAN_CHANNEL_IND);
1033  01f0 4a000000          	call	f_CanGetStatus
1035  01f4 6b80              	stab	OFST-1,s
1036                         ; 1153     if( CanHwIsOk(dllStatus) == kNmCanIsOk )
1038  01f6 c570              	bitb	#112
1039  01f8 2704              	beq	L05
1040  01fa 87                	clra	
1041  01fb c7                	clrb	
1042  01fc 2003              	bra	L25
1043  01fe                   L05:
1044  01fe cc0001            	ldd	#1
1045  0201                   L25:
1046  0201 042404            	dbne	d,L533
1047                         ; 1156       dllStatus = kNmErrorActive;
1049  0204 6980              	clr	OFST-1,s
1051  0206 2016              	bra	L333
1052  0208                   L533:
1053                         ; 1161       if( CanHwIsBusOff(dllStatus) == kNmCanIsBusOff )
1055  0208 0f804005          	brclr	OFST-1,s,64,L45
1056  020c cc0001            	ldd	#1
1057  020f 2002              	bra	L65
1058  0211                   L45:
1059  0211 87                	clra	
1060  0212 c7                	clrb	
1061  0213                   L65:
1062  0213 042404            	dbne	d,L143
1063                         ; 1164         dllStatus = kNmBusOff;
1065  0216                   LC002:
1066  0216 c602              	ldab	#2
1068  0218 2002              	bra	LC001
1069  021a                   L143:
1070                         ; 1169         dllStatus = kNmErrorPassive;
1072  021a c601              	ldab	#1
1073  021c                   LC001:
1074  021c 6b80              	stab	OFST-1,s
1075  021e                   L333:
1076                         ; 1175   NmApiInterruptRestore();
1078  021e 4a000000          	call	f_VStdResumeAllInterrupts
1080                         ; 1177   return dllStatus;
1082  0222 e680              	ldab	OFST-1,s
1085  0224 1b81              	leas	1,s
1086  0226 0a                	rtc	
1109                         ; 1192 vuint8 NmGetNetState ( void )
1109                         ; 1193 {
1110                         	switch	.ftext
1111  0227                   f_NmGetNetState:
1115                         ; 1194   return nmNodeState;
1117  0227 f60005            	ldab	L7_nmNodeState
1120  022a 0a                	rtc	
1321                         	switch	.bss
1322  0000                   L12_nmTxMsgReq:
1323  0000 00                	ds.b	1
1324  0001                   L71_nmBusOffState:
1325  0001 00                	ds.b	1
1326  0002                   L51_nmExternalStateReq:
1327  0002 00                	ds.b	1
1328  0003                   L31_nmInternalStateReq:
1329  0003 00                	ds.b	1
1330  0004                   L11_nmStatusInfo:
1331  0004 00                	ds.b	1
1332  0005                   L7_nmNodeState:
1333  0005 00                	ds.b	1
1334  0006                   L5_nmBusOffTimer:
1335  0006 00                	ds.b	1
1336  0007                   L3_nmNetworkTimer:
1337  0007 00                	ds.b	1
1338                         	xref	f_ApplNmCanSleep
1339                         	xref	f_ApplNmCanNormal
1340                         	xref	f_ApplNmBusOffEnd
1341                         	xref	f_ApplNmBusOff
1342                         	xdef	f_NmGetNetState
1343                         	xdef	f_NmGetDllState
1344                         	xdef	f_NmSetCurrentFailState
1345                         	xdef	f_NmSetGenericFailState
1346                         	xdef	f_NmSetEolState
1347                         	xdef	f_NmSetNewMode
1348                         	xdef	f_NmTask
1349                         	xdef	f_NmInit
1350                         	xdef	f_NmInitPowerOn
1351                         	xdef	_kNmReleaseVersion
1352                         	xdef	_kNmSubVersion
1353                         	xdef	_kNmMainVersion
1354                         	xdef	f_NmConfirmation
1355                         	xdef	f_NmPrecopy
1356  0008                   _nmSendMess:
1357  0008 0000              	ds.b	2
1358                         	xdef	_nmSendMess
1359                         	xref	f_CanGetStatus
1360                         	xref	f_CanOffline
1361                         	xref	f_CanOnline
1362                         	xref	f_CanTransmit
1363                         	xref	f_CanInit
1364                         	xdef	f_NmCanError
1365                         	xref	f_VStdResumeAllInterrupts
1366                         	xref	f_VStdSuspendAllInterrupts
1387                         	end
