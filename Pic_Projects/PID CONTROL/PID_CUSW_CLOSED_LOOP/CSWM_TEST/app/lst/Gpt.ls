   1                         ; C Compiler for S12XE [COSMIC Software].
   2                         ; Generator V4.7.8 - 27 Jul 2007
   3                         ; Optimizer V4.6.13 - 27 Jul 2007
   4                         	switch	.data
   5  0000                   L5_Gpt_Mapindex:
   6  0000 02                	dc.b	2
   7                         .const:	section	.data
   8  0000                   L7_Gpt_TimeoutDelay:
   9  0000 0024              	dc.w	_ECT_Delay0
  10  0002 0022              	dc.w	_ECT_Delay1
  11  0004 0020              	dc.w	_ECT_Delay2
  12  0006 001e              	dc.w	_ECT_Delay3
  13  0008 001c              	dc.w	_ECT_Delay4
  14  000a 001a              	dc.w	_ECT_Delay5
  15  000c 0018              	dc.w	_ECT_Delay6
  16  000e 0016              	dc.w	_ECT_Delay7
  17  0010 0000              	dc.w	__PITLD0
  18  0012 0000              	dc.w	__PITLD1
  19  0014 0000              	dc.w	__PITLD2
  20  0016 0000              	dc.w	__PITLD3
  21  0018                   L11_Gpt_CounterValue:
  22  0018 0014              	dc.w	_ECT_Count0
  23  001a 0012              	dc.w	_ECT_Count1
  24  001c 0010              	dc.w	_ECT_Count2
  25  001e 000e              	dc.w	_ECT_Count3
  26  0020 000c              	dc.w	_ECT_Count4
  27  0022 000a              	dc.w	_ECT_Count5
  28  0024 0008              	dc.w	_ECT_Count6
  29  0026 0006              	dc.w	_ECT_Count7
  30  0028 0000              	dc.w	__PITCNT0
  31  002a 0000              	dc.w	__PITCNT1
  32  002c 0000              	dc.w	__PITCNT2
  33  002e 0000              	dc.w	__PITCNT3
  34  0030                   L31_Gpt_AddrOutputCompare:
  35  0030 0000              	dc.w	__TC0
  36  0032 0000              	dc.w	__TC1
  37  0034 0000              	dc.w	__TC2
  38  0036 0000              	dc.w	__TC3
  39  0038 0000              	dc.w	__TC4
  40  003a 0000              	dc.w	__TC5
  41  003c 0000              	dc.w	__TC6
  42  003e 0000              	dc.w	__TC7
 129                         ; 429 void Gpt_GetVersionInfo(Std_VersionInfoType *versioninfo)
 129                         ; 430 {
 130                         	switch	.text
 131  0000                   _Gpt_GetVersionInfo:
 133       00000000          OFST:	set	0
 136                         ; 433     if(NULL_PTR != versioninfo)
 138  0000 6cae              	std	2,-s
 139  0002 2715              	beq	L76
 140                         ; 436         versioninfo->moduleID=GPT_MODULE_ID;
 142  0004 c664              	ldab	#100
 143  0006 ed80              	ldy	OFST+0,s
 144  0008 6b42              	stab	2,y
 145                         ; 439         versioninfo->vendorID=GPT_VENDOR_ID;
 147  000a cc0028            	ldd	#40
 148  000d 6c40              	std	0,y
 149                         ; 443         versioninfo->sw_major_version=GPT_SW_MAJOR_VERSION_C;
 151  000f c603              	ldab	#3
 152  0011 6b43              	stab	3,y
 153                         ; 445         versioninfo->sw_minor_version=GPT_SW_MINOR_VERSION_C;
 155  0013 6b44              	stab	4,y
 156                         ; 447         versioninfo->sw_patch_version=GPT_SW_PATCH_VERSION_C;
 158  0015 c608              	ldab	#8
 159  0017 6b45              	stab	5,y
 160  0019                   L76:
 161                         ; 451 }
 164  0019 31                	puly	
 165  001a 3d                	rts	
 325                         ; 469 void Gpt_Init(const Gpt_ConfigType *ConfigPtr)
 325                         ; 470 {
 326                         	switch	.text
 327  001b                   _Gpt_Init:
 329  001b 3b                	pshd	
 330  001c 37                	pshb	
 331       00000001          OFST:	set	1
 334                         ; 471     uint8 index=0; 
 336  001d 87                	clra	
 337                         ; 475     DisableAllInterrupts();
 340  001e 1410              	sei	
 342                         ; 478     Gpt_Enable_Notification[0]=  MCAL_DISABLE;
 345                         ; 479     Gpt_Enable_Notification[1] = MCAL_DISABLE;
 347  0020 c7                	clrb	
 348  0021 7c0004            	std	L51_Gpt_Enable_Notification
 349                         ; 485     for(index=0;index<GPT_NOOFHARDWARETYPES;index++)
 351  0024 6b80              	stab	OFST-1,s
 352  0026                   L361:
 353                         ; 487        Gpt_TimerOnFlag[index] =MCAL_DISABLE;      
 355  0026 b796              	exg	b,y
 356  0028 69ea0002          	clr	L71_Gpt_TimerOnFlag,y
 357                         ; 488        Gpt_Timeoutcount[index]=MCAL_DISABLE;      
 359  002c 69ea0000          	clr	L12_Gpt_Timeoutcount,y
 360                         ; 485     for(index=0;index<GPT_NOOFHARDWARETYPES;index++)
 362  0030 6280              	inc	OFST-1,s
 365  0032 e680              	ldab	OFST-1,s
 366  0034 c102              	cmpb	#2
 367  0036 25ee              	blo	L361
 368                         ; 497     ECT_TIE &= (~(ConfigPtr->Gpt_ECTHardware.Gpt_ChannelConfigured));
 370  0038 ee81              	ldx	OFST+0,s
 371  003a e600              	ldab	0,x
 372  003c 51                	comb	
 373  003d f40000            	andb	__TIE
 374  0040 7b0000            	stab	__TIE
 375                         ; 501     ECT_TFLG1 = (ConfigPtr->Gpt_ECTHardware.Gpt_ChannelConfigured);       
 377  0043 cd0000            	ldy	#__TFLG1
 378  0046 e600              	ldab	0,x
 379  0048 6b40              	stab	0,y
 380                         ; 507     ECT_TIOS = (ECT_TIOS |  (ConfigPtr->Gpt_ECTHardware.Gpt_ChannelConfigured));
 382  004a fa0000            	orab	__TIOS
 383  004d 7b0000            	stab	__TIOS
 384                         ; 518     ECT_OC7D = (ECT_OC7D | (ConfigPtr->Gpt_ECTHardware.Gpt_DataValue));
 386  0050 b756              	tfr	x,y
 387  0052 e644              	ldab	4,y
 388  0054 fa0000            	orab	__OC7D
 389  0057 7b0000            	stab	__OC7D
 390                         ; 521     ECT_OC7M = (ECT_OC7M | (ConfigPtr->Gpt_ECTHardware.Gpt_DataTransfer));
 392  005a e643              	ldab	3,y
 393  005c fa0000            	orab	__OC7M
 394  005f 7b0000            	stab	__OC7M
 395                         ; 539     ECT_TCTL1&= GPT_ACTIVATION_DEINIT_T47_MASK;
 397  0062 1d000000          	bclr	__TCTL1,0
 398                         ; 540     ECT_TCTL2&= GPT_ACTIVATION_DEINIT_T03_MASK;
 400  0066 1d000030          	bclr	__TCTL2,48
 401                         ; 543     ECT_TCTL1 = (ECT_TCTL1 | (ConfigPtr->Gpt_ECTHardware.Gpt_OutputAction1));
 403  006a e641              	ldab	1,y
 404  006c fa0000            	orab	__TCTL1
 405  006f 7b0000            	stab	__TCTL1
 406                         ; 546     ECT_TCTL2 = (ECT_TCTL2 | (ConfigPtr->Gpt_ECTHardware.Gpt_OutputAction2));
 408  0072 e642              	ldab	2,y
 409  0074 fa0000            	orab	__TCTL2
 410  0077 7b0000            	stab	__TCTL2
 411                         ; 551     Gpt_UserConfig_Ptr = ConfigPtr;
 413  007a 7e0026            	stx	L3_Gpt_UserConfig_Ptr
 414                         ; 553     EnableAllInterrupts();
 417  007d 10ef              	cli	
 419                         ; 556 }
 423  007f 1b83              	leas	3,s
 424  0081 3d                	rts	
 507                         ; 573 Gpt_ValueType Gpt_GetTimeElapsed (Gpt_ChannelType channel) 
 507                         ; 574 {
 508                         	switch	.text
 509  0082                   _Gpt_GetTimeElapsed:
 511  0082 3b                	pshd	
 512  0083 1b97              	leas	-9,s
 513       00000009          OFST:	set	9
 516                         ; 576     uint8 index_ch = Gpt_Mapindex[channel];
 518  0085 87                	clra	
 519  0086 b746              	tfr	d,y
 520  0088 180aea000084      	movb	L5_Gpt_Mapindex,y,OFST-5,s
 521                         ; 579     Gpt_ValueType TimeElapsed = GPT_ELAPSED_ERROR_VALUE;
 523                         ; 584     if((GPT_MODE_ONESHOT == Gpt_UserConfig_Ptr->Gpt_Channel[channel].\
 523                         ; 585         Gpt_ModeDefault) && (MCAL_CLEAR!=MCAL_GET_BIT(Gpt_Timeoutcount,index_ch)))
 525  008e fd0026            	ldy	L3_Gpt_UserConfig_Ptr
 526  0091 e68a              	ldab	OFST+1,s
 527  0093 59                	lsld	
 528  0094 59                	lsld	
 529  0095 19ee              	leay	d,y
 530  0097 e645              	ldab	5,y
 531  0099 2636              	bne	L522
 533  009b e684              	ldab	OFST-5,s
 534  009d 87                	clra	
 535  009e b746              	tfr	d,y
 536  00a0 1854              	lsry	
 537  00a2 1854              	lsry	
 538  00a4 1854              	lsry	
 539  00a6 e6ea0000          	ldab	L12_Gpt_Timeoutcount,y
 540  00aa 6c80              	std	OFST-9,s
 541  00ac cd0001            	ldy	#1
 542  00af e684              	ldab	OFST-5,s
 543  00b1 c407              	andb	#7
 544  00b3 2705              	beq	L21
 545  00b5                   L41:
 546  00b5 1858              	lsly	
 547  00b7 0431fb            	dbne	b,L41
 548  00ba                   L21:
 549  00ba 18e480            	andy	OFST-9,s
 550  00bd 2712              	beq	L522
 551                         ; 593           TimeElapsed = (uint32)(*Gpt_TimeoutDelay[index_ch]);
 553  00bf e684              	ldab	OFST-5,s
 554  00c1 87                	clra	
 555  00c2 59                	lsld	
 556  00c3 b746              	tfr	d,y
 557  00c5 eceb0000          	ldd	[L7_Gpt_TimeoutDelay,y]
 558  00c9 1887              	clrx	
 559  00cb 6c87              	std	OFST-2,s
 560  00cd 6e85              	stx	OFST-4,s
 562  00cf 2065              	bra	L722
 563  00d1                   L522:
 564                         ; 598         if(index_ch <= ECT_END_CHANNEL_INDEX) 
 566  00d1 e684              	ldab	OFST-5,s
 567  00d3 c107              	cmpb	#7
 568  00d5 224d              	bhi	L132
 569                         ; 604             Current_count = ECT_TCNT;
 571  00d7 fc0000            	ldd	__TCNT
 572  00da 6c82              	std	OFST-7,s
 573                         ; 610             if((*Gpt_AddrOutputCompare[index_ch]) >= Current_count) 
 575  00dc e684              	ldab	OFST-5,s
 576  00de 87                	clra	
 577  00df 59                	lsld	
 578  00e0 b746              	tfr	d,y
 579  00e2 eceb0030          	ldd	[L31_Gpt_AddrOutputCompare,y]
 580  00e6 ac82              	cpd	OFST-7,s
 581  00e8 251e              	blo	L332
 582                         ; 613                 if ((*Gpt_TimeoutDelay[index_ch]) >=
 582                         ; 614                     ((*Gpt_AddrOutputCompare[index_ch]) - 
 582                         ; 615                        Current_count)) 
 584  00ea a382              	subd	OFST-7,s
 585  00ec 6c80              	std	OFST-9,s
 586  00ee eceb0000          	ldd	[L7_Gpt_TimeoutDelay,y]
 587  00f2 ac80              	cpd	OFST-9,s
 588  00f4 250e              	blo	L532
 589                         ; 620                     (*Gpt_CounterValue[index_ch]) =
 589                         ; 621                         ((*Gpt_AddrOutputCompare[index_ch]) -
 589                         ; 622                            Current_count);
 591  00f6 e684              	ldab	OFST-5,s
 592  00f8 87                	clra	
 593  00f9 59                	lsld	
 594  00fa b745              	tfr	d,x
 595  00fc ece30030          	ldd	[L31_Gpt_AddrOutputCompare,x]
 596  0100 a382              	subd	OFST-7,s
 598  0102 201a              	bra	LC001
 599  0104                   L532:
 600                         ; 629                     TimeElapsed=GPT_ELAPSED_ERROR_VALUE;
 602                         ; 630                     return (TimeElapsed);
 604  0104 87                	clra	
 605  0105 c7                	clrb	
 607  0106 202c              	bra	LC002
 608  0108                   L332:
 609                         ; 638                 (*Gpt_CounterValue[index_ch]) = (GPT_MAXCOUNT_VALUE-
 609                         ; 639                   Current_count + (*Gpt_AddrOutputCompare[index_ch]) + 1);
 611  0108 e684              	ldab	OFST-5,s
 612  010a 87                	clra	
 613  010b b746              	tfr	d,y
 614  010d 1858              	lsly	
 615  010f 59                	lsld	
 616  0110 b745              	tfr	d,x
 617  0112 ccffff            	ldd	#-1
 618  0115 a382              	subd	OFST-7,s
 619  0117 e3e30030          	addd	[L31_Gpt_AddrOutputCompare,x]
 620  011b c30001            	addd	#1
 621  011e                   LC001:
 622  011e 6ceb0018          	std	[L11_Gpt_CounterValue,y]
 623  0122 e684              	ldab	OFST-5,s
 624  0124                   L132:
 625                         ; 648         TimeElapsed = ((uint32)((*Gpt_TimeoutDelay[index_ch]) -
 625                         ; 649                                 (*Gpt_CounterValue[index_ch])));
 627  0124 87                	clra	
 628  0125 b746              	tfr	d,y
 629  0127 1858              	lsly	
 630  0129 59                	lsld	
 631  012a b745              	tfr	d,x
 632  012c ece30000          	ldd	[L7_Gpt_TimeoutDelay,x]
 633  0130 a3eb0018          	subd	[L11_Gpt_CounterValue,y]
 634  0134                   LC002:
 635  0134 1887              	clrx	
 636  0136                   L722:
 637                         ; 651     return (TimeElapsed);
 641  0136 1b8b              	leas	11,s
 642  0138 3d                	rts	
 706                         ; 670 Gpt_ValueType Gpt_GetTimeRemaining (Gpt_ChannelType channel) 
 706                         ; 671 {
 707                         	switch	.text
 708  0139                   _Gpt_GetTimeRemaining:
 710  0139 3b                	pshd	
 711  013a 1b97              	leas	-9,s
 712       00000009          OFST:	set	9
 715                         ; 673     uint8 index_ch = Gpt_Mapindex[channel];
 717  013c 87                	clra	
 718  013d b746              	tfr	d,y
 719  013f e6ea0000          	ldab	L5_Gpt_Mapindex,y
 720  0143 6b84              	stab	OFST-5,s
 721                         ; 676     Gpt_ValueType TimeRemaining = 0;
 723                         ; 681     if(MCAL_CLEAR == MCAL_GET_BIT(Gpt_TimerOnFlag,index_ch)&&
 723                         ; 682       (MCAL_CLEAR == MCAL_GET_BIT(Gpt_Timeoutcount,index_ch)))
 725  0145 b746              	tfr	d,y
 726  0147 1854              	lsry	
 727  0149 1854              	lsry	
 728  014b 1854              	lsry	
 729  014d e6ea0002          	ldab	L71_Gpt_TimerOnFlag,y
 730  0151 6c80              	std	OFST-9,s
 731  0153 cd0001            	ldy	#1
 732  0156 e684              	ldab	OFST-5,s
 733  0158 c407              	andb	#7
 734  015a 2705              	beq	L22
 735  015c                   L42:
 736  015c 1858              	lsly	
 737  015e 0431fb            	dbne	b,L42
 738  0161                   L22:
 739  0161 18e480            	andy	OFST-9,s
 740  0164 262c              	bne	L762
 742  0166 e684              	ldab	OFST-5,s
 743  0168 87                	clra	
 744  0169 b746              	tfr	d,y
 745  016b 1854              	lsry	
 746  016d 1854              	lsry	
 747  016f 1854              	lsry	
 748  0171 e6ea0000          	ldab	L12_Gpt_Timeoutcount,y
 749  0175 6c80              	std	OFST-9,s
 750  0177 cd0001            	ldy	#1
 751  017a e684              	ldab	OFST-5,s
 752  017c c407              	andb	#7
 753  017e 2705              	beq	L62
 754  0180                   L03:
 755  0180 1858              	lsly	
 756  0182 0431fb            	dbne	b,L03
 757  0185                   L62:
 758  0185 18e480            	andy	OFST-9,s
 759  0188 2608              	bne	L762
 760                         ; 685         TimeRemaining = GPT_REMAINING_ERROR_VALUE;
 762                         ; 686         return TimeRemaining ;
 764  018a ceffff            	ldx	#-1
 765  018d b754              	tfr	x,d
 768  018f 1b8b              	leas	11,s
 769  0191 3d                	rts	
 770  0192                   L762:
 771                         ; 690     if((GPT_MODE_ONESHOT == Gpt_UserConfig_Ptr->Gpt_Channel[channel].\
 771                         ; 691         Gpt_ModeDefault) && (MCAL_CLEAR!=MCAL_GET_BIT(Gpt_Timeoutcount,index_ch)))
 773  0192 fd0026            	ldy	L3_Gpt_UserConfig_Ptr
 774  0195 e68a              	ldab	OFST+1,s
 775  0197 87                	clra	
 776  0198 59                	lsld	
 777  0199 59                	lsld	
 778  019a 19ee              	leay	d,y
 779  019c e645              	ldab	5,y
 780  019e 262c              	bne	L172
 782  01a0 e684              	ldab	OFST-5,s
 783  01a2 87                	clra	
 784  01a3 b746              	tfr	d,y
 785  01a5 1854              	lsry	
 786  01a7 1854              	lsry	
 787  01a9 1854              	lsry	
 788  01ab e6ea0000          	ldab	L12_Gpt_Timeoutcount,y
 789  01af 6c80              	std	OFST-9,s
 790  01b1 cd0001            	ldy	#1
 791  01b4 e684              	ldab	OFST-5,s
 792  01b6 c407              	andb	#7
 793  01b8 2705              	beq	L23
 794  01ba                   L43:
 795  01ba 1858              	lsly	
 796  01bc 0431fb            	dbne	b,L43
 797  01bf                   L23:
 798  01bf 18e480            	andy	OFST-9,s
 799  01c2 2708              	beq	L172
 800                         ; 698         TimeRemaining = (uint32) 0;
 802  01c4 87                	clra	
 803  01c5 c7                	clrb	
 804  01c6 6c85              	std	OFST-4,s
 805  01c8 6c87              	std	OFST-2,s
 807  01ca 2064              	bra	L372
 808  01cc                   L172:
 809                         ; 704         if (index_ch <= ECT_END_CHANNEL_INDEX) 
 811  01cc e684              	ldab	OFST-5,s
 812  01ce c107              	cmpb	#7
 813  01d0 2250              	bhi	L572
 814                         ; 710             Current_count = ECT_TCNT;
 816  01d2 fc0000            	ldd	__TCNT
 817  01d5 6c82              	std	OFST-7,s
 818                         ; 716             if((*Gpt_AddrOutputCompare[index_ch]) >= Current_count) 
 820  01d7 e684              	ldab	OFST-5,s
 821  01d9 87                	clra	
 822  01da 59                	lsld	
 823  01db b746              	tfr	d,y
 824  01dd eceb0030          	ldd	[L31_Gpt_AddrOutputCompare,y]
 825  01e1 ac82              	cpd	OFST-7,s
 826  01e3 2521              	blo	L772
 827                         ; 719                 if ((*Gpt_TimeoutDelay[index_ch]) >=
 827                         ; 720                     ((*Gpt_AddrOutputCompare[index_ch]) - Current_count)) 
 829  01e5 a382              	subd	OFST-7,s
 830  01e7 6c80              	std	OFST-9,s
 831  01e9 eceb0000          	ldd	[L7_Gpt_TimeoutDelay,y]
 832  01ed ac80              	cpd	OFST-9,s
 833  01ef 250e              	blo	L103
 834                         ; 725                     (*Gpt_CounterValue[index_ch]) = 
 834                         ; 726                        ((*Gpt_AddrOutputCompare[index_ch]) - Current_count);
 836  01f1 e684              	ldab	OFST-5,s
 837  01f3 87                	clra	
 838  01f4 59                	lsld	
 839  01f5 b745              	tfr	d,x
 840  01f7 ece30030          	ldd	[L31_Gpt_AddrOutputCompare,x]
 841  01fb a382              	subd	OFST-7,s
 843  01fd 201d              	bra	LC003
 844  01ff                   L103:
 845                         ; 733                     TimeRemaining=GPT_REMAINING_ERROR_VALUE;
 847                         ; 734                     return (TimeRemaining);
 849  01ff ceffff            	ldx	#-1
 850  0202 b754              	tfr	x,d
 852  0204 202c              	bra	L63
 853  0206                   L772:
 854                         ; 742                 (*Gpt_CounterValue[index_ch]) = (GPT_MAXCOUNT_VALUE-
 854                         ; 743                   Current_count + (*Gpt_AddrOutputCompare[index_ch]) + 1);
 856  0206 e684              	ldab	OFST-5,s
 857  0208 87                	clra	
 858  0209 b746              	tfr	d,y
 859  020b 1858              	lsly	
 860  020d 59                	lsld	
 861  020e b745              	tfr	d,x
 862  0210 ccffff            	ldd	#-1
 863  0213 a382              	subd	OFST-7,s
 864  0215 e3e30030          	addd	[L31_Gpt_AddrOutputCompare,x]
 865  0219 c30001            	addd	#1
 866  021c                   LC003:
 867  021c 6ceb0018          	std	[L11_Gpt_CounterValue,y]
 868  0220 e684              	ldab	OFST-5,s
 869  0222                   L572:
 870                         ; 749         TimeRemaining = (uint32)(*Gpt_CounterValue[index_ch]);
 872  0222 87                	clra	
 873  0223 59                	lsld	
 874  0224 b746              	tfr	d,y
 875  0226 eceb0018          	ldd	[L11_Gpt_CounterValue,y]
 876  022a 1887              	clrx	
 877  022c 6c87              	std	OFST-2,s
 878  022e 6e85              	stx	OFST-4,s
 879  0230                   L372:
 880                         ; 751     return (TimeRemaining);
 882  0230 ee85              	ldx	OFST-4,s
 884  0232                   L63:
 886  0232 1b8b              	leas	11,s
 887  0234 3d                	rts	
 944                         ; 772 void Gpt_StartTimer (Gpt_ChannelType channel,Gpt_ValueType value)
 944                         ; 773 {   
 945                         	switch	.text
 946  0235                   _Gpt_StartTimer:
 948  0235 3b                	pshd	
 949  0236 3b                	pshd	
 950       00000002          OFST:	set	2
 953                         ; 775     uint8 index_ch = Gpt_Mapindex[channel]; 
 955  0237 87                	clra	
 956  0238 b746              	tfr	d,y
 957  023a e6ea0000          	ldab	L5_Gpt_Mapindex,y
 958  023e 6b81              	stab	OFST-1,s
 959                         ; 779     DisableAllInterrupts();
 962  0240 1410              	sei	
 964                         ; 782     MCAL_SET_BIT(Gpt_TimerOnFlag,index_ch);
 967  0242 b746              	tfr	d,y
 968  0244 1854              	lsry	
 969  0246 1854              	lsry	
 970  0248 1854              	lsry	
 971  024a c407              	andb	#7
 972  024c 6b80              	stab	OFST-2,s
 973  024e c601              	ldab	#1
 974  0250 a680              	ldaa	OFST-2,s
 975  0252 2704              	beq	L24
 976  0254                   L44:
 977  0254 58                	lslb	
 978  0255 0430fc            	dbne	a,L44
 979  0258                   L24:
 980  0258 eaea0002          	orab	L71_Gpt_TimerOnFlag,y
 981  025c 6bea0002          	stab	L71_Gpt_TimerOnFlag,y
 982                         ; 785     if((ECT_END_CHANNEL_INDEX < index_ch)&&(index_ch <= PIT_END_CHANNEL_INDEX))
 984  0260 e681              	ldab	OFST-1,s
 985  0262 c107              	cmpb	#7
 986  0264 2310              	bls	L133
 988  0266 c10b              	cmpb	#11
 989  0268 220c              	bhi	L133
 990                         ; 787         --value;
 992  026a ec88              	ldd	OFST+6,s
 993  026c 2603              	bne	L64
 994  026e 186386            	decw	OFST+4,s
 995  0271                   L64:
 996  0271 186388            	decw	OFST+6,s
 997  0274 e681              	ldab	OFST-1,s
 998  0276                   L133:
 999                         ; 789     (*Gpt_TimeoutDelay[index_ch])  = (uint16)value;
1001  0276 87                	clra	
1002  0277 59                	lsld	
1003  0278 b746              	tfr	d,y
1004  027a ec88              	ldd	OFST+6,s
1005  027c 6ceb0000          	std	[L7_Gpt_TimeoutDelay,y]
1006                         ; 792     if(index_ch <= ECT_END_CHANNEL_INDEX) 
1008  0280 e681              	ldab	OFST-1,s
1009  0282 c107              	cmpb	#7
1010  0284 2226              	bhi	L333
1011                         ; 798         *Gpt_AddrOutputCompare[index_ch] = (uint16)(ECT_TCNT + value);
1013  0286 fc0000            	ldd	__TCNT
1014  0289 e388              	addd	OFST+6,s
1015  028b 6ceb0030          	std	[L31_Gpt_AddrOutputCompare,y]
1016                         ; 801         ECT_TFLG1 = (1u << index_ch);
1018  028f c601              	ldab	#1
1019  0291 a681              	ldaa	OFST-1,s
1020  0293 2704              	beq	L05
1021  0295                   L25:
1022  0295 58                	lslb	
1023  0296 0430fc            	dbne	a,L25
1024  0299                   L05:
1025  0299 7b0000            	stab	__TFLG1
1026                         ; 804         ECT_TIE = ( ECT_TIE | (1u << index_ch));
1028  029c c601              	ldab	#1
1029  029e a681              	ldaa	OFST-1,s
1030  02a0 2704              	beq	L45
1031  02a2                   L65:
1032  02a2 58                	lslb	
1033  02a3 0430fc            	dbne	a,L65
1034  02a6                   L45:
1035  02a6 fa0000            	orab	__TIE
1036  02a9 7b0000            	stab	__TIE
1037  02ac                   L333:
1038                         ; 809     EnableAllInterrupts();
1041  02ac 10ef              	cli	
1043                         ; 811 }
1047  02ae 1b84              	leas	4,s
1048  02b0 3d                	rts	
1105                         ; 833 void Gpt_StartTimerAbsolute (Gpt_ChannelType channel,Gpt_ValueType value)
1105                         ; 834 {
1106                         	switch	.text
1107  02b1                   _Gpt_StartTimerAbsolute:
1109  02b1 3b                	pshd	
1110  02b2 3b                	pshd	
1111       00000002          OFST:	set	2
1114                         ; 836     uint8 index_ch = Gpt_Mapindex[channel];    
1116  02b3 87                	clra	
1117  02b4 b746              	tfr	d,y
1118  02b6 e6ea0000          	ldab	L5_Gpt_Mapindex,y
1119  02ba 6b81              	stab	OFST-1,s
1120                         ; 839     DisableAllInterrupts();
1123  02bc 1410              	sei	
1125                         ; 842     MCAL_SET_BIT(Gpt_TimerOnFlag,index_ch);
1128  02be b746              	tfr	d,y
1129  02c0 1854              	lsry	
1130  02c2 1854              	lsry	
1131  02c4 1854              	lsry	
1132  02c6 c407              	andb	#7
1133  02c8 6b80              	stab	OFST-2,s
1134  02ca c601              	ldab	#1
1135  02cc a680              	ldaa	OFST-2,s
1136  02ce 2704              	beq	L26
1137  02d0                   L46:
1138  02d0 58                	lslb	
1139  02d1 0430fc            	dbne	a,L46
1140  02d4                   L26:
1141  02d4 eaea0002          	orab	L71_Gpt_TimerOnFlag,y
1142  02d8 6bea0002          	stab	L71_Gpt_TimerOnFlag,y
1143                         ; 845     if((ECT_END_CHANNEL_INDEX < index_ch)&&(index_ch <= PIT_END_CHANNEL_INDEX))
1145  02dc e681              	ldab	OFST-1,s
1146  02de c107              	cmpb	#7
1147  02e0 2310              	bls	L753
1149  02e2 c10b              	cmpb	#11
1150  02e4 220c              	bhi	L753
1151                         ; 847         --value;
1153  02e6 ec88              	ldd	OFST+6,s
1154  02e8 2603              	bne	L66
1155  02ea 186386            	decw	OFST+4,s
1156  02ed                   L66:
1157  02ed 186388            	decw	OFST+6,s
1158  02f0 e681              	ldab	OFST-1,s
1159  02f2                   L753:
1160                         ; 849     (*Gpt_TimeoutDelay[index_ch])  = (uint16)value;
1162  02f2 87                	clra	
1163  02f3 59                	lsld	
1164  02f4 b746              	tfr	d,y
1165  02f6 ec88              	ldd	OFST+6,s
1166  02f8 6ceb0000          	std	[L7_Gpt_TimeoutDelay,y]
1167                         ; 852     if(index_ch <= ECT_END_CHANNEL_INDEX)
1169  02fc e681              	ldab	OFST-1,s
1170  02fe c107              	cmpb	#7
1171  0300 222d              	bhi	L163
1172                         ; 856         *Gpt_AddrOutputCompare[index_ch] = (*Gpt_TimeoutDelay[index_ch]);
1174  0302 edea0030          	ldy	L31_Gpt_AddrOutputCompare,y
1175  0306 87                	clra	
1176  0307 59                	lsld	
1177  0308 b745              	tfr	d,x
1178  030a eee20000          	ldx	L7_Gpt_TimeoutDelay,x
1179  030e 18020040          	movw	0,x,0,y
1180                         ; 859         ECT_TFLG1 = (1u << index_ch);
1182  0312 c601              	ldab	#1
1183  0314 a681              	ldaa	OFST-1,s
1184  0316 2704              	beq	L07
1185  0318                   L27:
1186  0318 58                	lslb	
1187  0319 0430fc            	dbne	a,L27
1188  031c                   L07:
1189  031c 7b0000            	stab	__TFLG1
1190                         ; 862         ECT_TIE = ( ECT_TIE | (1u << index_ch));
1192  031f c601              	ldab	#1
1193  0321 a681              	ldaa	OFST-1,s
1194  0323 2704              	beq	L47
1195  0325                   L67:
1196  0325 58                	lslb	
1197  0326 0430fc            	dbne	a,L67
1198  0329                   L47:
1199  0329 fa0000            	orab	__TIE
1200  032c 7b0000            	stab	__TIE
1201  032f                   L163:
1202                         ; 866     EnableAllInterrupts();
1205  032f 10ef              	cli	
1207                         ; 868 }
1211  0331 1b84              	leas	4,s
1212  0333 3d                	rts	
1260                         ; 886 void Gpt_StopTimer(Gpt_ChannelType channel) 
1260                         ; 887 {
1261                         	switch	.text
1262  0334                   _Gpt_StopTimer:
1264  0334 3b                	pshd	
1265  0335 3b                	pshd	
1266       00000002          OFST:	set	2
1269                         ; 889     uint8 index_ch = Gpt_Mapindex[channel];
1271  0336 b796              	exg	b,y
1272  0338 e6ea0000          	ldab	L5_Gpt_Mapindex,y
1273  033c 6b81              	stab	OFST-1,s
1274                         ; 893     DisableAllInterrupts();
1277  033e 1410              	sei	
1279                         ; 896     if(index_ch <= ECT_END_CHANNEL_INDEX) 
1282  0340 c107              	cmpb	#7
1283  0342 2220              	bhi	L304
1284                         ; 899         ECT_TIE = ECT_TIE & (~(1u << (index_ch)));
1286  0344 c601              	ldab	#1
1287  0346 a681              	ldaa	OFST-1,s
1288  0348 2704              	beq	L201
1289  034a                   L401:
1290  034a 58                	lslb	
1291  034b 0430fc            	dbne	a,L401
1292  034e                   L201:
1293  034e 51                	comb	
1294  034f f40000            	andb	__TIE
1295  0352 7b0000            	stab	__TIE
1296                         ; 902         ECT_TFLG1 = (1u << index_ch);
1298  0355 c601              	ldab	#1
1299  0357 a681              	ldaa	OFST-1,s
1300  0359 2704              	beq	L601
1301  035b                   L011:
1302  035b 58                	lslb	
1303  035c 0430fc            	dbne	a,L011
1304  035f                   L601:
1305  035f 7b0000            	stab	__TFLG1
1306  0362 e681              	ldab	OFST-1,s
1307  0364                   L304:
1308                         ; 907     MCAL_CLEAR_BIT(Gpt_Timeoutcount,index_ch);
1310  0364 87                	clra	
1311  0365 b746              	tfr	d,y
1312  0367 1854              	lsry	
1313  0369 1854              	lsry	
1314  036b 1854              	lsry	
1315  036d c407              	andb	#7
1316  036f 6b80              	stab	OFST-2,s
1317  0371 c601              	ldab	#1
1318  0373 a680              	ldaa	OFST-2,s
1319  0375 2704              	beq	L211
1320  0377                   L411:
1321  0377 58                	lslb	
1322  0378 0430fc            	dbne	a,L411
1323  037b                   L211:
1324  037b 51                	comb	
1325  037c e4ea0000          	andb	L12_Gpt_Timeoutcount,y
1326  0380 6bea0000          	stab	L12_Gpt_Timeoutcount,y
1327                         ; 910     MCAL_CLEAR_BIT(Gpt_TimerOnFlag,index_ch);
1329  0384 e681              	ldab	OFST-1,s
1330  0386 87                	clra	
1331  0387 b746              	tfr	d,y
1332  0389 1854              	lsry	
1333  038b 1854              	lsry	
1334  038d 1854              	lsry	
1335  038f c407              	andb	#7
1336  0391 6b80              	stab	OFST-2,s
1337  0393 c601              	ldab	#1
1338  0395 a680              	ldaa	OFST-2,s
1339  0397 2704              	beq	L611
1340  0399                   L021:
1341  0399 58                	lslb	
1342  039a 0430fc            	dbne	a,L021
1343  039d                   L611:
1344  039d 51                	comb	
1345  039e e4ea0002          	andb	L71_Gpt_TimerOnFlag,y
1346  03a2 6bea0002          	stab	L71_Gpt_TimerOnFlag,y
1347                         ; 912     EnableAllInterrupts();
1350  03a6 10ef              	cli	
1352                         ; 914 }
1356  03a8 1b84              	leas	4,s
1357  03aa 3d                	rts	
1401                         ; 931 void Gpt_EnableNotification(Gpt_ChannelType channel) 
1401                         ; 932 {
1402                         	switch	.text
1403  03ab                   _Gpt_EnableNotification:
1405  03ab 3b                	pshd	
1406  03ac 3b                	pshd	
1407       00000002          OFST:	set	2
1410                         ; 934     uint8 index_ch = Gpt_Mapindex[channel];
1412  03ad 87                	clra	
1413  03ae b746              	tfr	d,y
1414  03b0 e6ea0000          	ldab	L5_Gpt_Mapindex,y
1415  03b4 6b81              	stab	OFST-1,s
1416                         ; 939     MCAL_SET_BIT(Gpt_Enable_Notification,index_ch);
1418  03b6 b746              	tfr	d,y
1419  03b8 1854              	lsry	
1420  03ba 1854              	lsry	
1421  03bc 1854              	lsry	
1422  03be c407              	andb	#7
1423  03c0 6b80              	stab	OFST-2,s
1424  03c2 c601              	ldab	#1
1425  03c4 a680              	ldaa	OFST-2,s
1426  03c6 2704              	beq	L421
1427  03c8                   L621:
1428  03c8 58                	lslb	
1429  03c9 0430fc            	dbne	a,L621
1430  03cc                   L421:
1431  03cc eaea0004          	orab	L51_Gpt_Enable_Notification,y
1432  03d0 6bea0004          	stab	L51_Gpt_Enable_Notification,y
1433                         ; 941 }
1436  03d4 1b84              	leas	4,s
1437  03d6 3d                	rts	
1481                         ; 958 void Gpt_DisableNotification(Gpt_ChannelType channel) 
1481                         ; 959 {
1482                         	switch	.text
1483  03d7                   _Gpt_DisableNotification:
1485  03d7 3b                	pshd	
1486  03d8 3b                	pshd	
1487       00000002          OFST:	set	2
1490                         ; 961     uint8 index_ch = Gpt_Mapindex[channel];
1492  03d9 87                	clra	
1493  03da b746              	tfr	d,y
1494  03dc e6ea0000          	ldab	L5_Gpt_Mapindex,y
1495  03e0 6b81              	stab	OFST-1,s
1496                         ; 966     MCAL_CLEAR_BIT(Gpt_Enable_Notification,index_ch);
1498  03e2 b746              	tfr	d,y
1499  03e4 1854              	lsry	
1500  03e6 1854              	lsry	
1501  03e8 1854              	lsry	
1502  03ea c407              	andb	#7
1503  03ec 6b80              	stab	OFST-2,s
1504  03ee c601              	ldab	#1
1505  03f0 a680              	ldaa	OFST-2,s
1506  03f2 2704              	beq	L231
1507  03f4                   L431:
1508  03f4 58                	lslb	
1509  03f5 0430fc            	dbne	a,L431
1510  03f8                   L231:
1511  03f8 51                	comb	
1512  03f9 e4ea0004          	andb	L51_Gpt_Enable_Notification,y
1513  03fd 6bea0004          	stab	L51_Gpt_Enable_Notification,y
1514                         ; 968 }
1517  0401 1b84              	leas	4,s
1518  0403 3d                	rts	
1549                         ; 993 void Gpt_ECTTimeoutHandler_2_Isr(void) 
1549                         ; 994 {                
1550                         	switch	.text
1551  0404                   _Gpt_ECTTimeoutHandler_2_Isr:
1555                         ; 997     ECT_TFLG1 = (MCAL_BIT2_MASK);
1557  0404 c604              	ldab	#4
1558  0406 7b0000            	stab	__TFLG1
1559                         ; 1000     if (GPT_MODE_ONESHOT == 
1559                         ; 1001         Gpt_UserConfig_Ptr->Gpt_Channel[GPT_CHANNEL_2].Gpt_ModeDefault)
1561  0409 fd0026            	ldy	L3_Gpt_UserConfig_Ptr
1562  040c e645              	ldab	5,y
1563  040e 260e              	bne	L554
1564                         ; 1004         ECT_TIE &= (~(MCAL_BIT2_MASK));
1566  0410 1d000004          	bclr	__TIE,4
1567                         ; 1007         MCAL_CLEAR_BIT(Gpt_TimerOnFlag,ECT_CHANNEL_2);
1569  0414 1d000204          	bclr	L71_Gpt_TimerOnFlag,4
1570                         ; 1010         MCAL_SET_BIT(Gpt_Timeoutcount,ECT_CHANNEL_2);
1572  0418 1c000004          	bset	L12_Gpt_Timeoutcount,4
1574  041c 2009              	bra	L754
1575  041e                   L554:
1576                         ; 1015         *Gpt_AddrOutputCompare[ECT_CHANNEL_2] = 
1576                         ; 1016             ((*Gpt_AddrOutputCompare[ECT_CHANNEL_2]) + 
1576                         ; 1017              (*Gpt_TimeoutDelay[ECT_CHANNEL_2]));        
1578  041e fc0000            	ldd	__TC2
1579  0421 f30020            	addd	_ECT_Delay2
1580  0424 7c0000            	std	__TC2
1581  0427                   L754:
1582                         ; 1023         if ((NULL_PTR!= Gpt_UserConfig_Ptr->Gpt_Channel[GPT_CHANNEL_2].\
1582                         ; 1024              Gpt_Notification) &&
1582                         ; 1025             (MCAL_CLEAR!=MCAL_GET_BIT(Gpt_Enable_Notification,ECT_CHANNEL_2)))
1584  0427 ec47              	ldd	7,y
1585  0429 2709              	beq	L164
1587  042b 1f00040404        	brclr	L51_Gpt_Enable_Notification,4,L164
1588                         ; 1028             (Gpt_UserConfig_Ptr-> Gpt_Channel[GPT_CHANNEL_2].\
1588                         ; 1029              Gpt_Notification)();
1590  0430 15eb0007          	jsr	[7,y]
1592  0434                   L164:
1593                         ; 1033 }
1596  0434 3d                	rts	
1809                         	switch	.bss
1810  0000                   L12_Gpt_Timeoutcount:
1811  0000 0000              	ds.b	2
1812  0002                   L71_Gpt_TimerOnFlag:
1813  0002 0000              	ds.b	2
1814  0004                   L51_Gpt_Enable_Notification:
1815  0004 0000              	ds.b	2
1816  0006                   _ECT_Count7:
1817  0006 0000              	ds.b	2
1818                         	xdef	_ECT_Count7
1819  0008                   _ECT_Count6:
1820  0008 0000              	ds.b	2
1821                         	xdef	_ECT_Count6
1822  000a                   _ECT_Count5:
1823  000a 0000              	ds.b	2
1824                         	xdef	_ECT_Count5
1825  000c                   _ECT_Count4:
1826  000c 0000              	ds.b	2
1827                         	xdef	_ECT_Count4
1828  000e                   _ECT_Count3:
1829  000e 0000              	ds.b	2
1830                         	xdef	_ECT_Count3
1831  0010                   _ECT_Count2:
1832  0010 0000              	ds.b	2
1833                         	xdef	_ECT_Count2
1834  0012                   _ECT_Count1:
1835  0012 0000              	ds.b	2
1836                         	xdef	_ECT_Count1
1837  0014                   _ECT_Count0:
1838  0014 0000              	ds.b	2
1839                         	xdef	_ECT_Count0
1840  0016                   _ECT_Delay7:
1841  0016 0000              	ds.b	2
1842                         	xdef	_ECT_Delay7
1843  0018                   _ECT_Delay6:
1844  0018 0000              	ds.b	2
1845                         	xdef	_ECT_Delay6
1846  001a                   _ECT_Delay5:
1847  001a 0000              	ds.b	2
1848                         	xdef	_ECT_Delay5
1849  001c                   _ECT_Delay4:
1850  001c 0000              	ds.b	2
1851                         	xdef	_ECT_Delay4
1852  001e                   _ECT_Delay3:
1853  001e 0000              	ds.b	2
1854                         	xdef	_ECT_Delay3
1855  0020                   _ECT_Delay2:
1856  0020 0000              	ds.b	2
1857                         	xdef	_ECT_Delay2
1858  0022                   _ECT_Delay1:
1859  0022 0000              	ds.b	2
1860                         	xdef	_ECT_Delay1
1861  0024                   _ECT_Delay0:
1862  0024 0000              	ds.b	2
1863                         	xdef	_ECT_Delay0
1864  0026                   L3_Gpt_UserConfig_Ptr:
1865  0026 0000              	ds.b	2
1866                         	xdef	_Gpt_ECTTimeoutHandler_2_Isr
1867                         	xdef	_Gpt_DisableNotification
1868                         	xdef	_Gpt_EnableNotification
1869                         	xdef	_Gpt_StopTimer
1870                         	xdef	_Gpt_StartTimerAbsolute
1871                         	xdef	_Gpt_StartTimer
1872                         	xdef	_Gpt_GetTimeRemaining
1873                         	xdef	_Gpt_GetTimeElapsed
1874                         	xdef	_Gpt_Init
1875                         	xdef	_Gpt_GetVersionInfo
1876                         	xref	__PITCNT3
1877                         	xref	__PITLD3
1878                         	xref	__PITCNT2
1879                         	xref	__PITLD2
1880                         	xref	__PITCNT1
1881                         	xref	__PITLD1
1882                         	xref	__PITCNT0
1883                         	xref	__PITLD0
1884                         	xref	__TC7
1885                         	xref	__TC6
1886                         	xref	__TC5
1887                         	xref	__TC4
1888                         	xref	__TC3
1889                         	xref	__TC2
1890                         	xref	__TC1
1891                         	xref	__TC0
1892                         	xref	__TFLG1
1893                         	xref	__TIE
1894                         	xref	__TCTL2
1895                         	xref	__TCTL1
1896                         	xref	__TCNT
1897                         	xref	__OC7D
1898                         	xref	__OC7M
1899                         	xref	__TIOS
1920                         	end
