   1                         ; C Compiler for S12XE [COSMIC Software].
   2                         ; Generator V4.7.8 - 27 Jul 2007
   3                         ; Optimizer V4.6.13 - 27 Jul 2007
 238                         ; 164 void diagF_DCX_OutputTest(void)
 238                         ; 165 {
 239                         .ftext:	section	.text
 240  0000                   f_diagF_DCX_OutputTest:
 242  0000 1b9c              	leas	-4,s
 243       00000004          OFST:	set	4
 246                         ; 167 	u_8Bit DCX_test_clear_flag = 0;
 248  0002 6980              	clr	OFST-4,s
 249                         ; 168 	u_8Bit temp_Vs_FL_fault_Stat = VS_NO_FLT_MASK;
 251                         ; 169 	u_8Bit temp_Vs_FR_fault_Stat = VS_NO_FLT_MASK;
 253                         ; 170 	u_8Bit temp_RL_seat_NTC_Stat = NTC_STAT_GOOD;
 255                         ; 171 	u_8Bit temp_RR_seat_NTC_Stat = NTC_STAT_GOOD;
 257                         ; 174 	if(diag_rc_all_op_lock_b == TRUE){
 259  0004 f60000            	ldab	_diag_rc_lock_flags_c
 260  0007 c501              	bitb	#1
 261  0009 18270101          	beq	L322
 262                         ; 177 		diagLF_DCX_PreConditions_Chck();
 264  000d 4a016868          	call	f_diagLF_DCX_PreConditions_Chck
 266                         ; 180 		switch(diag_op_rc_status_c)
 268  0011 f60000            	ldab	_diag_op_rc_status_c
 270  0014 c002              	subb	#2
 271  0016 270a              	beq	L511
 272  0018 c002              	subb	#2
 273  001a 182700ec          	beq	L711
 274                         ; 284 			default:
 274                         ; 285 				/* Set the flag to clear DCX Routine Control variables */
 274                         ; 286 				DCX_test_clear_flag = TRUE;
 276  001e 182000e8          	bra	L711
 277  0022                   L511:
 278                         ; 183 			case TEST_RUNNING:
 278                         ; 184 				/* Check if we are in first two seconds of the test */
 278                         ; 185 				if(diag_DCX_Test_cntr_w < DCX_ROUTINE_HS_2SEC){
 280  0022 fc0002            	ldd	_diag_DCX_Test_cntr_w
 281  0025 8c0064            	cpd	#100
 282                         ; 187 					diag_rc_all_heaters_lock_b = TRUE;
 284                         ; 189 					diag_DCX_Test_cntr_w++;
 287  0028 250c              	blo	LC006
 288                         ; 192 					if(diag_op_rc_preCondition_stat_c == PRE_CONDITIONS_OK){
 290  002a f70000            	tst	_diag_op_rc_preCondition_stat_c
 291  002d 182600d4          	bne	L761
 292                         ; 194 						if(diag_DCX_Test_cntr_w < DCX_ROUTINE_HS_TIME){
 294  0031 8c00fa            	cpd	#250
 295  0034 2406              	bhs	L171
 296                         ; 196 							diag_rc_all_heaters_lock_b = TRUE;
 298  0036                   LC006:
 299  0036 1c000002          	bset	_diag_rc_lock_flags_c,2
 300                         ; 198 							diag_DCX_Test_cntr_w++;
 303  003a 2039              	bra	LC005
 304  003c                   L171:
 305                         ; 201 							if( (diag_DCX_Test_cntr_w < DCX_ROUTINE_TOTAL_TIME) &&
 305                         ; 202 								((CSWM_config_c & CSWM_2VS_MASK_K) == CSWM_2VS_MASK_K) ){
 307  003c 8c03b6            	cpd	#950
 308  003f 2446              	bhs	L571
 310  0041 1e00003002        	brset	_CSWM_config_c,48,LC001
 311  0046 203f              	bra	L571
 312  0048                   LC001:
 313                         ; 204 								diag_rc_all_heaters_lock_b = FALSE;
 315  0048 1d000002          	bclr	_diag_rc_lock_flags_c,2
 316                         ; 206 								diag_rc_all_vents_lock_b = TRUE;
 318  004c 1c000004          	bset	_diag_rc_lock_flags_c,4
 319                         ; 208 								if(diag_DCX_Test_cntr_w < DCX_ROUTINE_VS_2SEC){
 321  0050 fc0002            	ldd	_diag_DCX_Test_cntr_w
 322  0053 8c015e            	cpd	#350
 323                         ; 211 									diag_DCX_Test_cntr_w++;
 326  0056 251d              	blo	LC005
 327                         ; 214 									temp_Vs_FL_fault_Stat = vsF_Get_Fault_Status(VS_FL);
 329  0058 87                	clra	
 330  0059 c7                	clrb	
 331  005a 4a000000          	call	f_vsF_Get_Fault_Status
 333  005e 6b81              	stab	OFST-3,s
 334                         ; 215 									temp_Vs_FR_fault_Stat = vsF_Get_Fault_Status(VS_FR);
 336  0060 cc0001            	ldd	#1
 337  0063 4a000000          	call	f_vsF_Get_Fault_Status
 339  0067 6b82              	stab	OFST-2,s
 340                         ; 218 									if( (temp_Vs_FL_fault_Stat <= VS_PRL_FLT_MAX) && (temp_Vs_FR_fault_Stat <= VS_PRL_FLT_MAX) ){
 342  0069 e681              	ldab	OFST-3,s
 343  006b c107              	cmpb	#7
 344  006d 220e              	bhi	L302
 346  006f e682              	ldab	OFST-2,s
 347  0071 c107              	cmpb	#7
 348  0073 2208              	bhi	L302
 349                         ; 220 										diag_DCX_Test_cntr_w++;
 351  0075                   LC005:
 352  0075 18720002          	incw	_diag_DCX_Test_cntr_w
 354  0079 18200091          	bra	L322
 355  007d                   L302:
 356                         ; 223 										diag_DCX_Test_cntr_w = DCX_ROUTINE_TOTAL_TIME;
 358  007d cc03b6            	ldd	#950
 359  0080 7c0002            	std	_diag_DCX_Test_cntr_w
 360  0083 18200087          	bra	L322
 361  0087                   L571:
 362                         ; 228 								diag_op_rc_status_c &= DIAG_CLR_TEST_RUNNING_MASK;
 364  0087 1d000002          	bclr	_diag_op_rc_status_c,2
 365                         ; 231 								temp_Vs_FL_fault_Stat = vsF_Get_Fault_Status(VS_FL);
 367  008b 87                	clra	
 368  008c c7                	clrb	
 369  008d 4a000000          	call	f_vsF_Get_Fault_Status
 371  0091 6b81              	stab	OFST-3,s
 372                         ; 232 								temp_Vs_FR_fault_Stat = vsF_Get_Fault_Status(VS_FR);
 374  0093 cc0001            	ldd	#1
 375  0096 4a000000          	call	f_vsF_Get_Fault_Status
 377  009a 6b82              	stab	OFST-2,s
 378                         ; 235 								if ((CSWM_config_c & CSWM_4HS_VARIANT_MASK_K) == CSWM_4HS_VARIANT_MASK_K){
 380  009c 1e00000f02        	brset	_CSWM_config_c,15,LC002
 381  00a1 200a              	bra	L112
 382  00a3                   LC002:
 383                         ; 237 									temp_RL_seat_NTC_Stat = temperature_HS_NTC_status[REAR_LEFT];
 385  00a3 1809830002        	movb	_temperature_HS_NTC_status+2,OFST-1,s
 386                         ; 238 									temp_RR_seat_NTC_Stat = temperature_HS_NTC_status[REAR_RIGHT];
 388  00a8 f60003            	ldab	_temperature_HS_NTC_status+3
 390  00ab 2004              	bra	L312
 391  00ad                   L112:
 392                         ; 242 									temp_RL_seat_NTC_Stat = NTC_STAT_GOOD;
 394  00ad c603              	ldab	#3
 395  00af 6b83              	stab	OFST-1,s
 396                         ; 243 									temp_RR_seat_NTC_Stat = NTC_STAT_GOOD;
 398  00b1                   L312:
 399  00b1 6b80              	stab	OFST-4,s
 400                         ; 247 								if( ( (hs_status_flags_c[FRONT_LEFT].cb & DIAG_HS_MATURED_FLT_MASK) == 0x00) &&
 400                         ; 248 									( (hs_status_flags_c[FRONT_RIGHT].cb & DIAG_HS_MATURED_FLT_MASK) == 0x00) &&
 400                         ; 249 									( (hs_status_flags_c[REAR_LEFT].cb & DIAG_HS_MATURED_FLT_MASK) == 0x00) &&
 400                         ; 250 									( (hs_status_flags_c[REAR_RIGHT].cb & DIAG_HS_MATURED_FLT_MASK) == 0x00) &&
 400                         ; 251 									(stW_Flt_status_w <= STW_PRL_FLT_MAX) &&
 400                         ; 252 									(temp_Vs_FL_fault_Stat <= VS_PRL_FLT_MAX) &&
 400                         ; 253 									(temp_Vs_FR_fault_Stat <= VS_PRL_FLT_MAX) &&
 400                         ; 254 									(temperature_HS_NTC_status[FRONT_LEFT] == NTC_STAT_GOOD) &&
 400                         ; 255 									(temperature_HS_NTC_status[FRONT_RIGHT] == NTC_STAT_GOOD) && 
 400                         ; 256 									(temp_RL_seat_NTC_Stat == NTC_STAT_GOOD) && 
 400                         ; 257 									(temp_RR_seat_NTC_Stat == NTC_STAT_GOOD) ){
 402  00b3 f60000            	ldab	_hs_status_flags_c
 403  00b6 c5aa              	bitb	#170
 404  00b8 2647              	bne	L512
 406  00ba f60001            	ldab	_hs_status_flags_c+1
 407  00bd c5aa              	bitb	#170
 408  00bf 2640              	bne	L512
 410  00c1 f60002            	ldab	_hs_status_flags_c+2
 411  00c4 c5aa              	bitb	#170
 412  00c6 2639              	bne	L512
 414  00c8 f60003            	ldab	_hs_status_flags_c+3
 415  00cb c5aa              	bitb	#170
 416  00cd 2632              	bne	L512
 418  00cf fc0000            	ldd	_stW_Flt_status_w
 419  00d2 8c003f            	cpd	#63
 420  00d5 222a              	bhi	L512
 422  00d7 e681              	ldab	OFST-3,s
 423  00d9 c107              	cmpb	#7
 424  00db 2224              	bhi	L512
 426  00dd e682              	ldab	OFST-2,s
 427  00df c107              	cmpb	#7
 428  00e1 221e              	bhi	L512
 430  00e3 f60000            	ldab	_temperature_HS_NTC_status
 431  00e6 c103              	cmpb	#3
 432  00e8 2617              	bne	L512
 434  00ea f60001            	ldab	_temperature_HS_NTC_status+1
 435  00ed c103              	cmpb	#3
 436  00ef 2610              	bne	L512
 438  00f1 e683              	ldab	OFST-1,s
 439  00f3 c103              	cmpb	#3
 440  00f5 260a              	bne	L512
 442  00f7 e680              	ldab	OFST-4,s
 443  00f9 c103              	cmpb	#3
 444  00fb 2604              	bne	L512
 445                         ; 259 									diag_op_rc_status_c = TEST_PASSED;
 447  00fd c601              	ldab	#1
 449  00ff 2006              	bra	LC004
 450  0101                   L512:
 451                         ; 263 									diag_op_rc_status_c = TEST_FAILED_DTC_PRESENT;							
 453  0101 c610              	ldab	#16
 454                         ; 266 								DCX_test_clear_flag = TRUE;
 456  0103 2002              	bra	LC004
 457  0105                   L761:
 458                         ; 271 						diag_op_rc_status_c = TEST_PRE_CONDITIONS_FAILURE;
 460  0105 c608              	ldab	#8
 461  0107                   LC004:
 462  0107 7b0000            	stab	_diag_op_rc_status_c
 463                         ; 273 						DCX_test_clear_flag = TRUE;
 465  010a                   L711:
 466                         ; 279 			case TEST_STOPPED:
 466                         ; 280 				/* Set the flag to clear DCX Routine Control variables */
 466                         ; 281 				DCX_test_clear_flag = TRUE;
 468  010a c601              	ldab	#1
 469  010c 6b80              	stab	OFST-4,s
 470                         ; 282 				break;
 472                         ; 284 			default:
 472                         ; 285 				/* Set the flag to clear DCX Routine Control variables */
 472                         ; 286 				DCX_test_clear_flag = TRUE;
 473  010e                   L322:
 474                         ; 299 	if(DCX_test_clear_flag == TRUE){
 476  010e e680              	ldab	OFST-4,s
 477  0110 042107            	dbne	b,L722
 478                         ; 301 		diag_DCX_Test_cntr_w = 0;		
 480  0113 18790002          	clrw	_diag_DCX_Test_cntr_w
 481                         ; 303 		diag_rc_lock_flags_c.cb = 0x00;
 483  0117 790000            	clr	_diag_rc_lock_flags_c
 485  011a                   L722:
 486                         ; 307 }
 489  011a 1b84              	leas	4,s
 490  011c 0a                	rtc	
 533                         ; 324 void diagRtnCtrlLF_Clr_All_IO_Control(void)
 533                         ; 325 {
 534                         	switch	.ftext
 535  011d                   f_diagRtnCtrlLF_Clr_All_IO_Control:
 537  011d 37                	pshb	
 538       00000001          OFST:	set	1
 541                         ; 330 	diag_io_active_b = FALSE;
 543  011e 1d000008          	bclr	_diag_io_lock3_flags_c,8
 544                         ; 333 	diag_io_lock2_flags_c.cb = 0;
 546  0122 c7                	clrb	
 547  0123 7b0000            	stab	_diag_io_lock2_flags_c
 548                         ; 336 	for (cntr1=0; cntr1<4; cntr1++)
 550  0126 6b80              	stab	OFST-1,s
 551  0128                   L542:
 552                         ; 338 		diag_io_hs_lock_flags_c[cntr1].cb = 0;
 554  0128 b796              	exg	b,y
 555  012a 69ea0000          	clr	_diag_io_hs_lock_flags_c,y
 556                         ; 339 		diag_io_hs_rq_c[cntr1] = 0;
 558  012e 69ea0000          	clr	_diag_io_hs_rq_c,y
 559                         ; 340 		diag_io_hs_state_c[cntr1] = 0;
 561  0132 69ea0000          	clr	_diag_io_hs_state_c,y
 562                         ; 336 	for (cntr1=0; cntr1<4; cntr1++)
 564  0136 6280              	inc	OFST-1,s
 567  0138 e680              	ldab	OFST-1,s
 568  013a c104              	cmpb	#4
 569  013c 25ea              	blo	L542
 570                         ; 344 	for (cntr1=0; cntr1<2; cntr1++)
 572  013e c7                	clrb	
 573  013f 6b80              	stab	OFST-1,s
 574  0141                   L352:
 575                         ; 346 		diag_io_vs_lock_flags_c[cntr1].cb = 0;
 577  0141 b796              	exg	b,y
 578  0143 69ea0000          	clr	_diag_io_vs_lock_flags_c,y
 579                         ; 347 		diag_io_vs_rq_c[cntr1] = 0;
 581  0147 69ea0000          	clr	_diag_io_vs_rq_c,y
 582                         ; 348 		diag_io_vs_state_c[cntr1] = 0;
 584  014b 69ea0000          	clr	_diag_io_vs_state_c,y
 585                         ; 344 	for (cntr1=0; cntr1<2; cntr1++)
 587  014f 6280              	inc	OFST-1,s
 590  0151 e680              	ldab	OFST-1,s
 591  0153 c102              	cmpb	#2
 592  0155 25ea              	blo	L352
 593                         ; 352 	diag_io_st_whl_rq_lock_b = FALSE;
 595  0157 1d000080          	bclr	_diag_io_lock_flags_c,128
 596                         ; 353 	diag_io_st_whl_state_lock_b = FALSE;
 598  015b 1d000004          	bclr	_diag_io_lock3_flags_c,4
 599                         ; 354 	diag_io_st_whl_rq_c = 0; 
 601  015f 790000            	clr	_diag_io_st_whl_rq_c
 602                         ; 355 	diag_io_st_whl_state_c = 0;
 604  0162 790000            	clr	_diag_io_st_whl_state_c
 605                         ; 356 }
 608  0165 1b81              	leas	1,s
 609  0167 0a                	rtc	
 664                         ; 379 void diagLF_DCX_PreConditions_Chck(void)
 664                         ; 380 {
 665                         	switch	.ftext
 666  0168                   f_diagLF_DCX_PreConditions_Chck:
 668  0168 3b                	pshd	
 669       00000002          OFST:	set	2
 672                         ; 388 	temp_ldShd_Stat_c = BattVF_Get_LdShd_Mnt_Stat();
 674  0169 4a000000          	call	f_BattVF_Get_LdShd_Mnt_Stat
 676  016d 6b80              	stab	OFST-2,s
 677                         ; 389 	temp_PCB_tempStat_c = TempF_Get_PCB_NTC_Stat();
 679  016f 4a000000          	call	f_TempF_Get_PCB_NTC_Stat
 681  0173 6b81              	stab	OFST-1,s
 682                         ; 392 	if(canio_RX_IGN_STATUS_c != IGN_RUN){
 684  0175 f60000            	ldab	_canio_RX_IGN_STATUS_c
 685  0178 c104              	cmpb	#4
 686  017a 2706              	beq	L772
 687                         ; 397 		diag_op_rc_preCondition_stat_c |= IGN_OTHER_THEN_RUN;
 689  017c 1c000001          	bset	_diag_op_rc_preCondition_stat_c,1
 691  0180 2004              	bra	L103
 692  0182                   L772:
 693                         ; 400 		diag_op_rc_preCondition_stat_c &= DIAG_CLR_IGN_RUN;
 695  0182 1d000001          	bclr	_diag_op_rc_preCondition_stat_c,1
 696  0186                   L103:
 697                         ; 404 	if(canio_canbus_off_b == TRUE){
 699  0186 1f00000106        	brclr	_canio_status2_flags_c,1,L303
 700                         ; 406 		diag_op_rc_preCondition_stat_c |= CAN_COM_FLT;
 702  018b 1c000002          	bset	_diag_op_rc_preCondition_stat_c,2
 704  018f 2004              	bra	L503
 705  0191                   L303:
 706                         ; 409 		diag_op_rc_preCondition_stat_c &= DIAG_CLR_CAN_COM;
 708  0191 1d000002          	bclr	_diag_op_rc_preCondition_stat_c,2
 709  0195                   L503:
 710                         ; 413 	if( (intFlt_bytes[ZERO] != NO_INT_FLT) || (intFlt_bytes[ONE] != NO_INT_FLT) || (intFlt_bytes[TWO] != NO_INT_FLT) ){
 712  0195 f60000            	ldab	_intFlt_bytes
 713  0198 260a              	bne	L113
 715  019a f60001            	ldab	_intFlt_bytes+1
 716  019d 2605              	bne	L113
 718  019f f60002            	ldab	_intFlt_bytes+2
 719  01a2 2720              	beq	L703
 720  01a4                   L113:
 721                         ; 415 		diag_op_rc_preCondition_stat_c |= INT_FLT;
 723  01a4 1c000004          	bset	_diag_op_rc_preCondition_stat_c,4
 725  01a8                   L513:
 726                         ; 433 	if( (BattV_set[SYSTEM_V].crt_vltg_c > BattV_cals.Thrshld_a[SET_OV_LMT]) ||
 726                         ; 434 		(BattV_set[SYSTEM_V].crt_vltg_c< BattV_cals.Thrshld_a[SET_UV_LMT]) ||
 726                         ; 435 		(BattV_set[BATTERY_V].crt_vltg_c > BattV_cals.Thrshld_a[SET_OV_LMT]) || 
 726                         ; 436 		(BattV_set[BATTERY_V].crt_vltg_c < BattV_cals.Thrshld_a[SET_UV_LMT]) ){
 728  01a8 f60000            	ldab	_BattV_set
 729  01ab f10000            	cmpb	_BattV_cals
 730  01ae 221a              	bhi	L123
 732  01b0 f10003            	cmpb	_BattV_cals+3
 733  01b3 2515              	blo	L123
 735  01b5 f6000a            	ldab	_BattV_set+10
 736  01b8 f10000            	cmpb	_BattV_cals
 737  01bb 220d              	bhi	L123
 739  01bd f10003            	cmpb	_BattV_cals+3
 740  01c0 2416              	bhs	L713
 741  01c2 2006              	bra	L123
 742  01c4                   L703:
 743                         ; 418 		diag_op_rc_preCondition_stat_c &= DIAG_CLR_INT_FLT;
 745  01c4 1d000004          	bclr	_diag_op_rc_preCondition_stat_c,4
 746  01c8 20de              	bra	L513
 747  01ca                   L123:
 748                         ; 438 		diag_op_rc_preCondition_stat_c |= VOLTAGE_RANGE;
 750  01ca 1c000008          	bset	_diag_op_rc_preCondition_stat_c,8
 752  01ce                   L723:
 753                         ; 445 	if(temp_ldShd_Stat_c != LDSHD_MNT_GOOD){
 755  01ce e680              	ldab	OFST-2,s
 756  01d0 270c              	beq	L133
 757                         ; 447 		diag_op_rc_preCondition_stat_c |= LOAD_SHD_ACTV;
 759  01d2 1c000010          	bset	_diag_op_rc_preCondition_stat_c,16
 761  01d6 200a              	bra	L333
 762  01d8                   L713:
 763                         ; 441 		diag_op_rc_preCondition_stat_c &= DIAG_CLR_VOLTAGE_RANGE;
 765  01d8 1d000008          	bclr	_diag_op_rc_preCondition_stat_c,8
 766  01dc 20f0              	bra	L723
 767  01de                   L133:
 768                         ; 450 		diag_op_rc_preCondition_stat_c &= DIAG_CLR_LOAD_SHED_ACT;
 770  01de 1d000010          	bclr	_diag_op_rc_preCondition_stat_c,16
 771  01e2                   L333:
 772                         ; 454 	if(temp_PCB_tempStat_c == PCB_STAT_OVERTEMP){
 774  01e2 e681              	ldab	OFST-1,s
 775  01e4 c103              	cmpb	#3
 776  01e6 2606              	bne	L533
 777                         ; 456 		diag_op_rc_preCondition_stat_c |= PCB_OVERTEMP_ACTV;
 779  01e8 1c000020          	bset	_diag_op_rc_preCondition_stat_c,32
 781  01ec 2004              	bra	L733
 782  01ee                   L533:
 783                         ; 459 		diag_op_rc_preCondition_stat_c &= DIAG_CLR_PCB_OVER_TEMP;
 785  01ee 1d000020          	bclr	_diag_op_rc_preCondition_stat_c,32
 786  01f2                   L733:
 787                         ; 463 	if( (final_FCM_LOC_b == TRUE) || (final_CCN_LOC_b == TRUE) || (final_HVAC_LOC_b == TRUE) || (final_CBC_LOC_b == TRUE) || (final_TGW_LOC_b == TRUE) ){		
 789  01f2 1e00000414        	brset	_canio_status2_flags_c,4,L343
 791  01f7 1e0000020f        	brset	_canio_status2_flags_c,2,L343
 793  01fc 1e0000080a        	brset	_canio_status2_flags_c,8,L343
 795  0201 1e00001005        	brset	_canio_status2_flags_c,16,L343
 797  0206 1f0000202a        	brclr	_canio_status2_flags_c,32,L143
 798  020b                   L343:
 799                         ; 465 		diag_op_rc_preCondition_stat_c |= LOC_FLT;
 801  020b 1c000040          	bset	_diag_op_rc_preCondition_stat_c,64
 803  020f                   L353:
 804                         ; 472 	if ((CSWM_config_c & CSWM_4HS_VARIANT_MASK_K) == CSWM_4HS_VARIANT_MASK_K){
 806  020f f60000            	ldab	_CSWM_config_c
 807  0212 c40f              	andb	#15
 808  0214 c10f              	cmpb	#15
 809  0216 2668              	bne	L553
 810                         ; 476 		if( ( (NTC_AD_Readings_c[FRONT_LEFT] < hs_cal_prms.hs_prms[hs_vehicle_line_c].NTC_CutOFF_c[4]) && (NTC_AD_Readings_c[FRONT_LEFT] >= temperature_s.HS_NTC_Lw_Thrshld) ) ||
 810                         ; 477 			( (NTC_AD_Readings_c[FRONT_RIGHT] < hs_cal_prms.hs_prms[hs_vehicle_line_c].NTC_CutOFF_c[4]) && (NTC_AD_Readings_c[FRONT_RIGHT] >= temperature_s.HS_NTC_Lw_Thrshld) ) ||
 810                         ; 478 			( (NTC_AD_Readings_c[REAR_LEFT] < hs_cal_prms.hs_prms[hs_vehicle_line_c].NTC_CutOFF_c[4]) && (NTC_AD_Readings_c[REAR_LEFT] >= temperature_s.HS_NTC_Lw_Thrshld) ) ||
 810                         ; 479 			( (NTC_AD_Readings_c[REAR_RIGHT] < hs_cal_prms.hs_prms[hs_vehicle_line_c].NTC_CutOFF_c[4]) && (NTC_AD_Readings_c[REAR_RIGHT] >= temperature_s.HS_NTC_Lw_Thrshld) ) ||
 810                         ; 480 			(stW_temperature_c > stW_prms.cals[0].MaxTemp_Threshld) ){
 812  0218 f60000            	ldab	_hs_vehicle_line_c
 813  021b 860e              	ldaa	#14
 814  021d 12                	mul	
 815  021e b746              	tfr	d,y
 816  0220 e6ea000c          	ldab	_hs_cal_prms+12,y
 817  0224 f10000            	cmpb	_NTC_AD_Readings_c
 818  0227 2312              	bls	L363
 820  0229 f60000            	ldab	_NTC_AD_Readings_c
 821  022c f10000            	cmpb	_temperature_s
 822  022f 1824007f          	bhs	L704
 823  0233 2006              	bra	L363
 824  0235                   L143:
 825                         ; 468 		diag_op_rc_preCondition_stat_c &= DIAG_CLR_LOC;
 827  0235 1d000040          	bclr	_diag_op_rc_preCondition_stat_c,64
 828  0239 20d4              	bra	L353
 829  023b                   L363:
 830                         ; 476 		if( ( (NTC_AD_Readings_c[FRONT_LEFT] < hs_cal_prms.hs_prms[hs_vehicle_line_c].NTC_CutOFF_c[4]) && (NTC_AD_Readings_c[FRONT_LEFT] >= temperature_s.HS_NTC_Lw_Thrshld) ) ||
 830                         ; 477 			( (NTC_AD_Readings_c[FRONT_RIGHT] < hs_cal_prms.hs_prms[hs_vehicle_line_c].NTC_CutOFF_c[4]) && (NTC_AD_Readings_c[FRONT_RIGHT] >= temperature_s.HS_NTC_Lw_Thrshld) ) ||
 830                         ; 478 			( (NTC_AD_Readings_c[REAR_LEFT] < hs_cal_prms.hs_prms[hs_vehicle_line_c].NTC_CutOFF_c[4]) && (NTC_AD_Readings_c[REAR_LEFT] >= temperature_s.HS_NTC_Lw_Thrshld) ) ||
 830                         ; 479 			( (NTC_AD_Readings_c[REAR_RIGHT] < hs_cal_prms.hs_prms[hs_vehicle_line_c].NTC_CutOFF_c[4]) && (NTC_AD_Readings_c[REAR_RIGHT] >= temperature_s.HS_NTC_Lw_Thrshld) ) ||
 830                         ; 480 			(stW_temperature_c > stW_prms.cals[0].MaxTemp_Threshld) ){
 831  023b f60000            	ldab	_hs_vehicle_line_c
 832  023e 860e              	ldaa	#14
 833  0240 12                	mul	
 834  0241 b746              	tfr	d,y
 835  0243 e6ea000c          	ldab	_hs_cal_prms+12,y
 836  0247 f10001            	cmpb	_NTC_AD_Readings_c+1
 837  024a 230c              	bls	L763
 839  024c f60001            	ldab	_NTC_AD_Readings_c+1
 840  024f f10000            	cmpb	_temperature_s
 841  0252 245e              	bhs	L704
 842  0254 e6ea000c          	ldab	_hs_cal_prms+12,y
 843  0258                   L763:
 845  0258 f10002            	cmpb	_NTC_AD_Readings_c+2
 846  025b 230c              	bls	L373
 848  025d f60002            	ldab	_NTC_AD_Readings_c+2
 849  0260 f10000            	cmpb	_temperature_s
 850  0263 244d              	bhs	L704
 851  0265 e6ea000c          	ldab	_hs_cal_prms+12,y
 852  0269                   L373:
 854  0269 f10003            	cmpb	_NTC_AD_Readings_c+3
 855  026c 2308              	bls	L773
 857  026e f60003            	ldab	_NTC_AD_Readings_c+3
 858  0271 f10000            	cmpb	_temperature_s
 859  0274 243c              	bhs	L704
 860  0276                   L773:
 862  0276 f60000            	ldab	_stW_temperature_c
 863  0279 f1000e            	cmpb	_stW_prms+14
 864  027c 233a              	bls	L504
 865                         ; 482 			diag_op_rc_preCondition_stat_c |= TEMP_ABOVE_MAX_THRSHLD;			
 868  027e 2032              	bra	L704
 869                         ; 485 			diag_op_rc_preCondition_stat_c &= DIAG_CLR_TEMP_ABOVE_THRSHLD;
 871  0280                   L553:
 872                         ; 491 		if( ( (NTC_AD_Readings_c[FRONT_LEFT] < hs_cal_prms.hs_prms[hs_vehicle_line_c].NTC_CutOFF_c[4]) && (NTC_AD_Readings_c[FRONT_LEFT] >= temperature_s.HS_NTC_Lw_Thrshld) ) ||
 872                         ; 492 			( (NTC_AD_Readings_c[FRONT_RIGHT] < hs_cal_prms.hs_prms[hs_vehicle_line_c].NTC_CutOFF_c[4]) && (NTC_AD_Readings_c[FRONT_RIGHT] >= temperature_s.HS_NTC_Lw_Thrshld) ) ||
 872                         ; 493 			(stW_temperature_c > stW_prms.cals[0].MaxTemp_Threshld) ){
 874  0280 f60000            	ldab	_hs_vehicle_line_c
 875  0283 860e              	ldaa	#14
 876  0285 12                	mul	
 877  0286 b746              	tfr	d,y
 878  0288 e6ea000c          	ldab	_hs_cal_prms+12,y
 879  028c f10000            	cmpb	_NTC_AD_Readings_c
 880  028f 230c              	bls	L114
 882  0291 f60000            	ldab	_NTC_AD_Readings_c
 883  0294 f10000            	cmpb	_temperature_s
 884  0297 2419              	bhs	L704
 885  0299 e6ea000c          	ldab	_hs_cal_prms+12,y
 886  029d                   L114:
 888  029d f10001            	cmpb	_NTC_AD_Readings_c+1
 889  02a0 2308              	bls	L514
 891  02a2 f60001            	ldab	_NTC_AD_Readings_c+1
 892  02a5 f10000            	cmpb	_temperature_s
 893  02a8 2408              	bhs	L704
 894  02aa                   L514:
 896  02aa f60000            	ldab	_stW_temperature_c
 897  02ad f1000e            	cmpb	_stW_prms+14
 898  02b0 2306              	bls	L504
 899  02b2                   L704:
 900                         ; 495 			diag_op_rc_preCondition_stat_c |= TEMP_ABOVE_MAX_THRSHLD;
 902  02b2 1c000080          	bset	_diag_op_rc_preCondition_stat_c,128
 904  02b6 2004              	bra	L304
 905  02b8                   L504:
 906                         ; 498 			diag_op_rc_preCondition_stat_c &= DIAG_CLR_TEMP_ABOVE_THRSHLD;
 908  02b8 1d000080          	bclr	_diag_op_rc_preCondition_stat_c,128
 909  02bc                   L304:
 910                         ; 501 }
 913  02bc 31                	puly	
 914  02bd 0a                	rtc	
1199                         .const:	section	.data
1200                         	even
1201  0000                   L566:
1202  0000 00000018          	dc.w	0,24
1203  0004 0606              	dc.w	L574
1204  0006 0090c411          	dc.l	9487377
1205  000a 061d              	dc.w	L774
1206  000c 0090c412          	dc.l	9487378
1207  0010 05ea              	dc.w	L374
1208  0012 0090c413          	dc.l	9487379
1209  0016 02f0              	dc.w	L124
1210  0018 009e9911          	dc.l	10393873
1211  001c 030f              	dc.w	L324
1212  001e 009e9912          	dc.l	10393874
1213  0022 032e              	dc.w	L524
1214  0024 009e9913          	dc.l	10393875
1215  0028 034d              	dc.w	L724
1216  002a 009e991e          	dc.l	10393886
1217  002e 036c              	dc.w	L134
1218  0030 009e9a12          	dc.l	10394130
1219  0034 038b              	dc.w	L334
1220  0036 009e9a13          	dc.l	10394131
1221  003a 03aa              	dc.w	L534
1222  003c 009e9a1e          	dc.l	10394142
1223  0040 03c9              	dc.w	L734
1224  0042 009e9b11          	dc.l	10394385
1225  0046 03e8              	dc.w	L144
1226  0048 009e9b12          	dc.l	10394386
1227  004c 0407              	dc.w	L344
1228  004e 009e9b13          	dc.l	10394387
1229  0052 0426              	dc.w	L544
1230  0054 009e9b1e          	dc.l	10394398
1231  0058 0445              	dc.w	L744
1232  005a 009e9c11          	dc.l	10394641
1233  005e 0464              	dc.w	L154
1234  0060 009e9c12          	dc.l	10394642
1235  0064 0483              	dc.w	L354
1236  0066 009e9c13          	dc.l	10394643
1237  006a 04a2              	dc.w	L554
1238  006c 009e9c1e          	dc.l	10394654
1239  0070 04c1              	dc.w	L754
1240  0072 009e9d11          	dc.l	10394897
1241  0076 04f2              	dc.w	L164
1242  0078 009e9d12          	dc.l	10394898
1243  007c 0523              	dc.w	L364
1244  007e 009e9d13          	dc.l	10394899
1245  0082 0554              	dc.w	L564
1246  0084 009e9e11          	dc.l	10395153
1247  0088 0588              	dc.w	L764
1248  008a 009e9e12          	dc.l	10395154
1249  008e 05bc              	dc.w	L174
1250  0090 009e9e13          	dc.l	10395155
1251  0094 0640              	dc.w	L105
1252                         ; 505 void DESC_API_CALLBACK_TYPE ApplDescStart_31010201_Set_Fault_Status(DescMsgContext* pMsgContext)
1252                         ; 506 {
1253                         	switch	.ftext
1254  02be                   f_ApplDescStart_31010201_Set_Fault_Status:
1256  02be 3b                	pshd	
1257  02bf 1b9c              	leas	-4,s
1258       00000004          OFST:	set	4
1261                         ; 508    TempDtcCode.all = 0;
1263  02c1 18c7              	clry	
1264  02c3 6d80              	sty	OFST-4,s
1265  02c5 6d82              	sty	OFST-2,s
1266                         ; 511 	TempDtcCode.byte[1] = pMsgContext->reqData[0]; //Harsha..Re arranged
1268  02c7 b746              	tfr	d,y
1269  02c9 e6eb0000          	ldab	[0,y]
1270  02cd 6b81              	stab	OFST-3,s
1271                         ; 512 	TempDtcCode.byte[2] = pMsgContext->reqData[1];
1273  02cf edf30004          	ldy	[OFST+0,s]
1274  02d3 180a4182          	movb	1,y,OFST-2,s
1275                         ; 513 	TempDtcCode.byte[3] = pMsgContext->reqData[2];
1277  02d7 180a4283          	movb	2,y,OFST-1,s
1278                         ; 515 	if (pMsgContext->reqDataLen == 4)
1280  02db ed84              	ldy	OFST+0,s
1281  02dd ec42              	ldd	2,y
1282  02df 8c0004            	cpd	#4
1283  02e2 1826035e          	bne	L366
1284                         ; 517 		switch (TempDtcCode.all) 
1286  02e6 ec82              	ldd	OFST-2,s
1287  02e8 ee80              	ldx	OFST-4,s
1289  02ea cd0000            	ldy	#L566
1290  02ed 060000            	jmp	c_jltab
1291  02f0                   L124:
1292                         ; 521 			     if (pMsgContext->reqData[3] == 0x01)
1294  02f0 edf30004          	ldy	[OFST+0,s]
1295  02f4 e643              	ldab	3,y
1296  02f6 c101              	cmpb	#1
1297  02f8 2608              	bne	L176
1298                         ; 523 				      hs_status_flags_c[ FRONT_LEFT ].b.hs_final_short_gnd_b = TRUE;
1300  02fa 1c000002          	bset	_hs_status_flags_c,2
1302  02fe 18200347          	bra	L7511
1303  0302                   L176:
1304                         ; 527 			        if (pMsgContext->reqData[3] == 0)
1306  0302 d7                	tstb	
1307  0303 18260342          	bne	L7511
1308                         ; 529 				         hs_status_flags_c[ FRONT_LEFT ].b.hs_final_short_gnd_b = FALSE;
1310  0307 1d000002          	bclr	_hs_status_flags_c,2
1311  030b 1820033a          	bra	L7511
1312  030f                   L324:
1313                         ; 538 			     if (pMsgContext->reqData[3] == 0x01)
1315  030f edf30004          	ldy	[OFST+0,s]
1316  0313 e643              	ldab	3,y
1317  0315 c101              	cmpb	#1
1318  0317 2608              	bne	L776
1319                         ; 540 				      hs_status_flags_c[ FRONT_LEFT ].b.hs_final_short_bat_b = TRUE;
1321  0319 1c000020          	bset	_hs_status_flags_c,32
1323  031d 18200328          	bra	L7511
1324  0321                   L776:
1325                         ; 545 			    	 if (pMsgContext->reqData[3] == 0x00)
1327  0321 d7                	tstb	
1328  0322 18260323          	bne	L7511
1329                         ; 547 				         hs_status_flags_c[ FRONT_LEFT ].b.hs_final_short_bat_b = FALSE;
1331  0326 1d000020          	bclr	_hs_status_flags_c,32
1333  032a 1820031b          	bra	L7511
1334  032e                   L524:
1335                         ; 560 			     if (pMsgContext->reqData[3] == 0x01)
1337  032e edf30004          	ldy	[OFST+0,s]
1338  0332 e643              	ldab	3,y
1339  0334 c101              	cmpb	#1
1340  0336 2608              	bne	L707
1341                         ; 562 				      hs_status_flags_c[ FRONT_LEFT ].b.hs_final_open_b = TRUE;
1343  0338 1c000008          	bset	_hs_status_flags_c,8
1345  033c 18200309          	bra	L7511
1346  0340                   L707:
1347                         ; 567 			         if (pMsgContext->reqData[3] == 0x00)
1349  0340 d7                	tstb	
1350  0341 18260304          	bne	L7511
1351                         ; 569 					      hs_status_flags_c[ FRONT_LEFT ].b.hs_final_open_b = FALSE;
1353  0345 1d000008          	bclr	_hs_status_flags_c,8
1355  0349 182002fc          	bra	L7511
1356  034d                   L724:
1357                         ; 582 			     if (pMsgContext->reqData[3] == 0x01)
1359  034d edf30004          	ldy	[OFST+0,s]
1360  0351 e643              	ldab	3,y
1361  0353 c101              	cmpb	#1
1362  0355 2608              	bne	L717
1363                         ; 584 				      hs_status_flags_c[ FRONT_LEFT ].b.hs_final_partial_open_b = TRUE;
1365  0357 1c000080          	bset	_hs_status_flags_c,128
1367  035b 182002ea          	bra	L7511
1368  035f                   L717:
1369                         ; 589 				      if (pMsgContext->reqData[3] == 0x00)
1371  035f d7                	tstb	
1372  0360 182602e5          	bne	L7511
1373                         ; 591 				         hs_status_flags_c[ FRONT_LEFT ].b.hs_final_partial_open_b = FALSE;
1375  0364 1d000080          	bclr	_hs_status_flags_c,128
1377  0368 182002dd          	bra	L7511
1378  036c                   L134:
1379                         ; 604 			     if (pMsgContext->reqData[3] == 0x01)
1381  036c edf30004          	ldy	[OFST+0,s]
1382  0370 e643              	ldab	3,y
1383  0372 c101              	cmpb	#1
1384  0374 2608              	bne	L727
1385                         ; 606 				      hs_status_flags_c[ FRONT_RIGHT ].b.hs_final_short_bat_b = TRUE;
1387  0376 1c000120          	bset	_hs_status_flags_c+1,32
1389  037a 182002cb          	bra	L7511
1390  037e                   L727:
1391                         ; 611 			        if (pMsgContext->reqData[3] == 0x00)
1393  037e d7                	tstb	
1394  037f 182602c6          	bne	L7511
1395                         ; 613 				         hs_status_flags_c[ FRONT_RIGHT ].b.hs_final_short_bat_b = FALSE;
1397  0383 1d000120          	bclr	_hs_status_flags_c+1,32
1399  0387 182002be          	bra	L7511
1400  038b                   L334:
1401                         ; 625 				      if (pMsgContext->reqData[3] == 0x01) 
1403  038b edf30004          	ldy	[OFST+0,s]
1404  038f e643              	ldab	3,y
1405  0391 c101              	cmpb	#1
1406  0393 2608              	bne	L737
1407                         ; 627 					       hs_status_flags_c[ FRONT_RIGHT ].b.hs_final_open_b = TRUE;
1409  0395 1c000108          	bset	_hs_status_flags_c+1,8
1411  0399 182002ac          	bra	L7511
1412  039d                   L737:
1413                         ; 632 					        if (pMsgContext->reqData[3] == 0x00) 
1415  039d d7                	tstb	
1416  039e 182602a7          	bne	L7511
1417                         ; 634 						         hs_status_flags_c[ FRONT_RIGHT ].b.hs_final_open_b = FALSE;
1419  03a2 1d000108          	bclr	_hs_status_flags_c+1,8
1421  03a6 1820029f          	bra	L7511
1422  03aa                   L534:
1423                         ; 646 				   if (pMsgContext->reqData[3] == 0x01)
1425  03aa edf30004          	ldy	[OFST+0,s]
1426  03ae e643              	ldab	3,y
1427  03b0 c101              	cmpb	#1
1428  03b2 2608              	bne	L747
1429                         ; 648 					    hs_status_flags_c[ FRONT_RIGHT ].b.hs_final_partial_open_b = TRUE;
1431  03b4 1c000180          	bset	_hs_status_flags_c+1,128
1433  03b8 1820028d          	bra	L7511
1434  03bc                   L747:
1435                         ; 653 					      if (pMsgContext->reqData[3] == 0x00)
1437  03bc d7                	tstb	
1438  03bd 18260288          	bne	L7511
1439                         ; 655 						       hs_status_flags_c[ FRONT_RIGHT ].b.hs_final_partial_open_b = FALSE;
1441  03c1 1d000180          	bclr	_hs_status_flags_c+1,128
1443  03c5 18200280          	bra	L7511
1444  03c9                   L734:
1445                         ; 667 				   if (pMsgContext->reqData[3] == 0x01)
1447  03c9 edf30004          	ldy	[OFST+0,s]
1448  03cd e643              	ldab	3,y
1449  03cf c101              	cmpb	#1
1450  03d1 2608              	bne	L757
1451                         ; 669 					    hs_status_flags_c[ REAR_LEFT ].b.hs_final_short_gnd_b = TRUE;
1453  03d3 1c000202          	bset	_hs_status_flags_c+2,2
1455  03d7 1820026e          	bra	L7511
1456  03db                   L757:
1457                         ; 673 					      if (pMsgContext->reqData[3] == 0)
1459  03db d7                	tstb	
1460  03dc 18260269          	bne	L7511
1461                         ; 675 						       hs_status_flags_c[ REAR_LEFT ].b.hs_final_short_gnd_b = FALSE;
1463  03e0 1d000202          	bclr	_hs_status_flags_c+2,2
1464  03e4 18200261          	bra	L7511
1465  03e8                   L144:
1466                         ; 683 				   if (pMsgContext->reqData[3] == 0x01)
1468  03e8 edf30004          	ldy	[OFST+0,s]
1469  03ec e643              	ldab	3,y
1470  03ee c101              	cmpb	#1
1471  03f0 2608              	bne	L567
1472                         ; 685 					    hs_status_flags_c[ REAR_LEFT ].b.hs_final_short_bat_b = TRUE;
1474  03f2 1c000220          	bset	_hs_status_flags_c+2,32
1476  03f6 1820024f          	bra	L7511
1477  03fa                   L567:
1478                         ; 690 					      if (pMsgContext->reqData[3] == 0x00)
1480  03fa d7                	tstb	
1481  03fb 1826024a          	bne	L7511
1482                         ; 692 						       hs_status_flags_c[ REAR_LEFT ].b.hs_final_short_bat_b = FALSE;
1484  03ff 1d000220          	bclr	_hs_status_flags_c+2,32
1486  0403 18200242          	bra	L7511
1487  0407                   L344:
1488                         ; 704 				   if (pMsgContext->reqData[3] == 0x01)
1490  0407 edf30004          	ldy	[OFST+0,s]
1491  040b e643              	ldab	3,y
1492  040d c101              	cmpb	#1
1493  040f 2608              	bne	L577
1494                         ; 706 					    hs_status_flags_c[ REAR_LEFT ].b.hs_final_open_b = TRUE;
1496  0411 1c000208          	bset	_hs_status_flags_c+2,8
1498  0415 18200230          	bra	L7511
1499  0419                   L577:
1500                         ; 711 					      if (pMsgContext->reqData[3] == 0x00)
1502  0419 d7                	tstb	
1503  041a 1826022b          	bne	L7511
1504                         ; 713 						       hs_status_flags_c[ REAR_LEFT ].b.hs_final_open_b = FALSE;
1506  041e 1d000208          	bclr	_hs_status_flags_c+2,8
1508  0422 18200223          	bra	L7511
1509  0426                   L544:
1510                         ; 725 				     if (pMsgContext->reqData[3] == 0x01)
1512  0426 edf30004          	ldy	[OFST+0,s]
1513  042a e643              	ldab	3,y
1514  042c c101              	cmpb	#1
1515  042e 2608              	bne	L5001
1516                         ; 727 					      hs_status_flags_c[ REAR_LEFT ].b.hs_final_partial_open_b = TRUE;
1518  0430 1c000280          	bset	_hs_status_flags_c+2,128
1520  0434 18200211          	bra	L7511
1521  0438                   L5001:
1522                         ; 732 					        if (pMsgContext->reqData[3] == 0x00)
1524  0438 d7                	tstb	
1525  0439 1826020c          	bne	L7511
1526                         ; 734 						         hs_status_flags_c[ REAR_LEFT ].b.hs_final_partial_open_b = FALSE;
1528  043d 1d000280          	bclr	_hs_status_flags_c+2,128
1530  0441 18200204          	bra	L7511
1531  0445                   L744:
1532                         ; 745 				   if (pMsgContext->reqData[3] == 0x01)
1534  0445 edf30004          	ldy	[OFST+0,s]
1535  0449 e643              	ldab	3,y
1536  044b c101              	cmpb	#1
1537  044d 2608              	bne	L5101
1538                         ; 747 					    hs_status_flags_c[ REAR_RIGHT ].b.hs_final_short_gnd_b = TRUE;
1540  044f 1c000302          	bset	_hs_status_flags_c+3,2
1542  0453 182001f2          	bra	L7511
1543  0457                   L5101:
1544                         ; 751 					      if (pMsgContext->reqData[3] == 0)
1546  0457 d7                	tstb	
1547  0458 182601ed          	bne	L7511
1548                         ; 753 						       hs_status_flags_c[ REAR_RIGHT ].b.hs_final_short_gnd_b = FALSE;
1550  045c 1d000302          	bclr	_hs_status_flags_c+3,2
1551  0460 182001e5          	bra	L7511
1552  0464                   L154:
1553                         ; 761 				   if (pMsgContext->reqData[3] == 0x01)
1555  0464 edf30004          	ldy	[OFST+0,s]
1556  0468 e643              	ldab	3,y
1557  046a c101              	cmpb	#1
1558  046c 2608              	bne	L3201
1559                         ; 763 					    hs_status_flags_c[ REAR_RIGHT ].b.hs_final_short_bat_b = TRUE;
1561  046e 1c000320          	bset	_hs_status_flags_c+3,32
1563  0472 182001d3          	bra	L7511
1564  0476                   L3201:
1565                         ; 768 					      if (pMsgContext->reqData[3] == 0x00)
1567  0476 d7                	tstb	
1568  0477 182601ce          	bne	L7511
1569                         ; 770 						       hs_status_flags_c[ REAR_RIGHT ].b.hs_final_short_bat_b = FALSE;
1571  047b 1d000320          	bclr	_hs_status_flags_c+3,32
1573  047f 182001c6          	bra	L7511
1574  0483                   L354:
1575                         ; 782 				   if (pMsgContext->reqData[3] == 0x01)
1577  0483 edf30004          	ldy	[OFST+0,s]
1578  0487 e643              	ldab	3,y
1579  0489 c101              	cmpb	#1
1580  048b 2608              	bne	L3301
1581                         ; 784 					    hs_status_flags_c[ REAR_RIGHT ].b.hs_final_open_b = TRUE;
1583  048d 1c000308          	bset	_hs_status_flags_c+3,8
1585  0491 182001b4          	bra	L7511
1586  0495                   L3301:
1587                         ; 789 					      if (pMsgContext->reqData[3] == 0x00)
1589  0495 d7                	tstb	
1590  0496 182601af          	bne	L7511
1591                         ; 791 						       hs_status_flags_c[ REAR_RIGHT ].b.hs_final_open_b = FALSE;
1593  049a 1d000308          	bclr	_hs_status_flags_c+3,8
1595  049e 182001a7          	bra	L7511
1596  04a2                   L554:
1597                         ; 803 				   if (pMsgContext->reqData[3] == 0x01)
1599  04a2 edf30004          	ldy	[OFST+0,s]
1600  04a6 e643              	ldab	3,y
1601  04a8 c101              	cmpb	#1
1602  04aa 2608              	bne	L3401
1603                         ; 805 					    hs_status_flags_c[ REAR_RIGHT ].b.hs_final_partial_open_b = TRUE;
1605  04ac 1c000380          	bset	_hs_status_flags_c+3,128
1607  04b0 18200195          	bra	L7511
1608  04b4                   L3401:
1609                         ; 810 					      if (pMsgContext->reqData[3] == 0x00)
1611  04b4 d7                	tstb	
1612  04b5 18260190          	bne	L7511
1613                         ; 812 						       hs_status_flags_c[ REAR_RIGHT ].b.hs_final_partial_open_b = FALSE;
1615  04b9 1d000380          	bclr	_hs_status_flags_c+3,128
1617  04bd 18200188          	bra	L7511
1618  04c1                   L754:
1619                         ; 824 				   if (pMsgContext->reqData[3] == 0x01)
1621  04c1 edf30004          	ldy	[OFST+0,s]
1622  04c5 e643              	ldab	3,y
1623  04c7 c101              	cmpb	#1
1624  04c9 2612              	bne	L3501
1625                         ; 827 					    vsF_Diag_DTC_Control(VS_SET_DTC, VS_MTRD_GND_MASK, VS_FL);
1627  04cb 87                	clra	
1628  04cc c7                	clrb	
1629  04cd 3b                	pshd	
1630  04ce c610              	ldab	#16
1631  04d0 3b                	pshd	
1632  04d1 c601              	ldab	#1
1633  04d3 4a000000          	call	f_vsF_Diag_DTC_Control
1635  04d7 1b84              	leas	4,s
1637  04d9 1820016c          	bra	L7511
1638  04dd                   L3501:
1639                         ; 832 					    if (pMsgContext->reqData[3] == 0)
1641  04dd d7                	tstb	
1642  04de 18260167          	bne	L7511
1643                         ; 835 						     vsF_Diag_DTC_Control(VS_CLR_DTC, VS_MTRD_GND_MASK, VS_FL);
1645  04e2 87                	clra	
1646  04e3 3b                	pshd	
1647  04e4 c610              	ldab	#16
1648  04e6 3b                	pshd	
1649  04e7 c7                	clrb	
1650  04e8 4a000000          	call	f_vsF_Diag_DTC_Control
1652  04ec 1b84              	leas	4,s
1653  04ee 18200157          	bra	L7511
1654  04f2                   L164:
1655                         ; 843 				   if (pMsgContext->reqData[3] == 0x01)
1657  04f2 edf30004          	ldy	[OFST+0,s]
1658  04f6 e643              	ldab	3,y
1659  04f8 c101              	cmpb	#1
1660  04fa 2612              	bne	L1601
1661                         ; 846 					    vsF_Diag_DTC_Control(VS_SET_DTC, VS_MTRD_BATT_MASK, VS_FL);
1663  04fc 87                	clra	
1664  04fd c7                	clrb	
1665  04fe 3b                	pshd	
1666  04ff c620              	ldab	#32
1667  0501 3b                	pshd	
1668  0502 c601              	ldab	#1
1669  0504 4a000000          	call	f_vsF_Diag_DTC_Control
1671  0508 1b84              	leas	4,s
1673  050a 1820013b          	bra	L7511
1674  050e                   L1601:
1675                         ; 851 					    if (pMsgContext->reqData[3] == 0x00)
1677  050e d7                	tstb	
1678  050f 18260136          	bne	L7511
1679                         ; 854 						     vsF_Diag_DTC_Control(VS_CLR_DTC, VS_MTRD_BATT_MASK, VS_FL);
1681  0513 87                	clra	
1682  0514 3b                	pshd	
1683  0515 c620              	ldab	#32
1684  0517 3b                	pshd	
1685  0518 c7                	clrb	
1686  0519 4a000000          	call	f_vsF_Diag_DTC_Control
1688  051d 1b84              	leas	4,s
1690  051f 18200126          	bra	L7511
1691  0523                   L364:
1692                         ; 866 				   if (pMsgContext->reqData[3] == 0x01)
1694  0523 edf30004          	ldy	[OFST+0,s]
1695  0527 e643              	ldab	3,y
1696  0529 c101              	cmpb	#1
1697  052b 2612              	bne	L1701
1698                         ; 869 					    vsF_Diag_DTC_Control(VS_SET_DTC, VS_MTRD_OPEN_MASK, VS_FL);
1700  052d 87                	clra	
1701  052e c7                	clrb	
1702  052f 3b                	pshd	
1703  0530 c608              	ldab	#8
1704  0532 3b                	pshd	
1705  0533 c601              	ldab	#1
1706  0535 4a000000          	call	f_vsF_Diag_DTC_Control
1708  0539 1b84              	leas	4,s
1710  053b 1820010a          	bra	L7511
1711  053f                   L1701:
1712                         ; 874 					    if (pMsgContext->reqData[3] == 0x00)
1714  053f d7                	tstb	
1715  0540 18260105          	bne	L7511
1716                         ; 877 						     vsF_Diag_DTC_Control(VS_CLR_DTC, VS_MTRD_OPEN_MASK, VS_FL);
1718  0544 87                	clra	
1719  0545 3b                	pshd	
1720  0546 c608              	ldab	#8
1721  0548 3b                	pshd	
1722  0549 c7                	clrb	
1723  054a 4a000000          	call	f_vsF_Diag_DTC_Control
1725  054e 1b84              	leas	4,s
1727  0550 182000f5          	bra	L7511
1728  0554                   L564:
1729                         ; 889 				   if (pMsgContext->reqData[3] == 0x01)
1731  0554 edf30004          	ldy	[OFST+0,s]
1732  0558 e643              	ldab	3,y
1733  055a c101              	cmpb	#1
1734  055c 2613              	bne	L1011
1735                         ; 892 					    vsF_Diag_DTC_Control(VS_SET_DTC, VS_MTRD_GND_MASK, VS_FR);
1737  055e cc0001            	ldd	#1
1738  0561 3b                	pshd	
1739  0562 c610              	ldab	#16
1740  0564 3b                	pshd	
1741  0565 c601              	ldab	#1
1742  0567 4a000000          	call	f_vsF_Diag_DTC_Control
1744  056b 1b84              	leas	4,s
1746  056d 182000d8          	bra	L7511
1747  0571                   L1011:
1748                         ; 897 					      if (pMsgContext->reqData[3] == 0)
1750  0571 d7                	tstb	
1751  0572 182600d3          	bne	L7511
1752                         ; 900 						       vsF_Diag_DTC_Control(VS_CLR_DTC, VS_MTRD_GND_MASK, VS_FR);
1754  0576 cc0001            	ldd	#1
1755  0579 3b                	pshd	
1756  057a c610              	ldab	#16
1757  057c 3b                	pshd	
1758  057d c7                	clrb	
1759  057e 4a000000          	call	f_vsF_Diag_DTC_Control
1761  0582 1b84              	leas	4,s
1762  0584 182000c1          	bra	L7511
1763  0588                   L764:
1764                         ; 908 				   if (pMsgContext->reqData[3] == 0x01)
1766  0588 edf30004          	ldy	[OFST+0,s]
1767  058c e643              	ldab	3,y
1768  058e c101              	cmpb	#1
1769  0590 2613              	bne	L7011
1770                         ; 911 					    vsF_Diag_DTC_Control(VS_SET_DTC, VS_MTRD_BATT_MASK, VS_FR);
1772  0592 cc0001            	ldd	#1
1773  0595 3b                	pshd	
1774  0596 c620              	ldab	#32
1775  0598 3b                	pshd	
1776  0599 c601              	ldab	#1
1777  059b 4a000000          	call	f_vsF_Diag_DTC_Control
1779  059f 1b84              	leas	4,s
1781  05a1 182000a4          	bra	L7511
1782  05a5                   L7011:
1783                         ; 916 					    if (pMsgContext->reqData[3] == 0x00)
1785  05a5 d7                	tstb	
1786  05a6 1826009f          	bne	L7511
1787                         ; 919 						     vsF_Diag_DTC_Control(VS_CLR_DTC, VS_MTRD_BATT_MASK, VS_FR);
1789  05aa cc0001            	ldd	#1
1790  05ad 3b                	pshd	
1791  05ae c620              	ldab	#32
1792  05b0 3b                	pshd	
1793  05b1 c7                	clrb	
1794  05b2 4a000000          	call	f_vsF_Diag_DTC_Control
1796  05b6 1b84              	leas	4,s
1798  05b8 1820008d          	bra	L7511
1799  05bc                   L174:
1800                         ; 931 				   if (pMsgContext->reqData[3] == 0x01)
1802  05bc edf30004          	ldy	[OFST+0,s]
1803  05c0 e643              	ldab	3,y
1804  05c2 c101              	cmpb	#1
1805  05c4 2611              	bne	L7111
1806                         ; 934 					    vsF_Diag_DTC_Control(VS_SET_DTC, VS_MTRD_OPEN_MASK, VS_FR);
1808  05c6 cc0001            	ldd	#1
1809  05c9 3b                	pshd	
1810  05ca c608              	ldab	#8
1811  05cc 3b                	pshd	
1812  05cd c601              	ldab	#1
1813  05cf 4a000000          	call	f_vsF_Diag_DTC_Control
1815  05d3 1b84              	leas	4,s
1817  05d5 2072              	bra	L7511
1818  05d7                   L7111:
1819                         ; 939 					    if (pMsgContext->reqData[3] == 0x00)
1821  05d7 d7                	tstb	
1822  05d8 266f              	bne	L7511
1823                         ; 942 						     vsF_Diag_DTC_Control(VS_CLR_DTC, VS_MTRD_OPEN_MASK, VS_FR);
1825  05da cc0001            	ldd	#1
1826  05dd 3b                	pshd	
1827  05de c608              	ldab	#8
1828  05e0 3b                	pshd	
1829  05e1 c7                	clrb	
1830  05e2 4a000000          	call	f_vsF_Diag_DTC_Control
1832  05e6 1b84              	leas	4,s
1834  05e8 205f              	bra	L7511
1835  05ea                   L374:
1836                         ; 954 				   if (pMsgContext->reqData[3] == 0x01)
1838  05ea edf30004          	ldy	[OFST+0,s]
1839  05ee e643              	ldab	3,y
1840  05f0 c101              	cmpb	#1
1841  05f2 2608              	bne	L7211
1842                         ; 957 					    stWF_Diag_DTC_Control(STW_SET_DTC, STW_MTRD_FULL_OPEN_MASK);
1844  05f4 cc0040            	ldd	#64
1845  05f7 3b                	pshd	
1846  05f8 c601              	ldab	#1
1849  05fa 203c              	bra	LC010
1850  05fc                   L7211:
1851                         ; 962 					    if (pMsgContext->reqData[3] == 0x00)
1853  05fc 04614a            	tbne	b,L7511
1854                         ; 965 						     stWF_Diag_DTC_Control(STW_CLR_DTC, STW_MTRD_FULL_OPEN_MASK);
1856  05ff cc0040            	ldd	#64
1857  0602 3b                	pshd	
1858  0603 c7                	clrb	
1861  0604 2032              	bra	LC010
1862  0606                   L574:
1863                         ; 1000 				   if (pMsgContext->reqData[3] == 0x01)
1865  0606 edf30004          	ldy	[OFST+0,s]
1866  060a e643              	ldab	3,y
1867  060c c101              	cmpb	#1
1868  060e 2605              	bne	L7311
1869                         ; 1003 					    stWF_Diag_DTC_Control(STW_SET_DTC, STW_MTRD_GND_MASK);
1871  0610 cc0100            	ldd	#256
1874  0613 2015              	bra	LC012
1875  0615                   L7311:
1876                         ; 1008 					    if (pMsgContext->reqData[3] == 0x00)
1878  0615 046131            	tbne	b,L7511
1879                         ; 1011 						     stWF_Diag_DTC_Control(STW_CLR_DTC, STW_MTRD_GND_MASK);
1881  0618 cc0100            	ldd	#256
1884  061b 2019              	bra	LC011
1885  061d                   L774:
1886                         ; 1023 				   if (pMsgContext->reqData[3] == 0x01)
1888  061d edf30004          	ldy	[OFST+0,s]
1889  0621 e643              	ldab	3,y
1890  0623 c101              	cmpb	#1
1891  0625 2609              	bne	L7411
1892                         ; 1026 					    stWF_Diag_DTC_Control(STW_SET_DTC, STW_MTRD_BATT_MASK);
1894  0627 cc0200            	ldd	#512
1895  062a                   LC012:
1896  062a 3b                	pshd	
1897  062b cc0001            	ldd	#1
1900  062e 2008              	bra	LC010
1901  0630                   L7411:
1902                         ; 1031 					      if (pMsgContext->reqData[3] == 0x00)
1904  0630 046116            	tbne	b,L7511
1905                         ; 1034 						       stWF_Diag_DTC_Control(STW_CLR_DTC, STW_MTRD_BATT_MASK);
1907  0633 cc0200            	ldd	#512
1908  0636                   LC011:
1909  0636 3b                	pshd	
1910  0637 87                	clra	
1912  0638                   LC010:
1913  0638 4a000000          	call	f_stWF_Diag_DTC_Control
1914  063c 1b82              	leas	2,s
1916  063e 2009              	bra	L7511
1917  0640                   L105:
1918                         ; 1047 				 Response = SUBFUNCTION_NOT_SUPPORTED;
1920  0640 c607              	ldab	#7
1921  0642 2002              	bra	LC009
1923  0644                   L366:
1924                         ; 1053 			Response = LENGTH_INVALID_FORMAT;
1926  0644 c605              	ldab	#5
1927  0646                   LC009:
1928  0646 7b0001            	stab	L111_Response
1929  0649                   L7511:
1930                         ; 1057 				DataLength = 0;
1932  0649 790000            	clr	L311_DataLength
1933                         ; 1059 				if(pMsgContext->reqData[3]	!= 0x00 && pMsgContext->reqData[3] != 0x01)
1935  064c ee84              	ldx	OFST+0,s
1936  064e ed00              	ldy	0,x
1937  0650 e643              	ldab	3,y
1938  0652 2708              	beq	L1611
1940  0654 040105            	dbeq	b,L1611
1941                         ; 1061    						Response = SUBFUNCTION_NOT_SUPPORTED;
1943  0657 c607              	ldab	#7
1944  0659 7b0001            	stab	L111_Response
1945  065c                   L1611:
1946                         ; 1063  				if( Response != SUBFUNCTION_NOT_SUPPORTED && Response != LENGTH_INVALID_FORMAT )
1948  065c f60001            	ldab	L111_Response
1949  065f c107              	cmpb	#7
1950  0661 270e              	beq	L3611
1952  0663 c105              	cmpb	#5
1953  0665 270a              	beq	L3611
1954                         ; 1065 						pMsgContext->reqDataLen=3;				
1956  0667 cc0003            	ldd	#3
1957  066a 6c02              	std	2,x
1958                         ; 1066 				    Response = RESPONSE_OK;
1960  066c c601              	ldab	#1
1961  066e 7b0001            	stab	L111_Response
1962  0671                   L3611:
1963                         ; 1068         diagF_DiagResponse(Response, (pMsgContext->reqDataLen)+DataLength, pMsgContext);
1965  0671 b754              	tfr	x,d
1966  0673 3b                	pshd	
1967  0674 b746              	tfr	d,y
1968  0676 ec42              	ldd	2,y
1969  0678 fb0000            	addb	L311_DataLength
1970  067b 8900              	adca	#0
1971  067d 3b                	pshd	
1972  067e f60001            	ldab	L111_Response
1973  0681 87                	clra	
1974  0682 4a000000          	call	f_diagF_DiagResponse
1976                         ; 1069 }
1979  0686 1b8a              	leas	10,s
1980  0688 0a                	rtc	
2021                         ; 1074 void DESC_API_CALLBACK_TYPE ApplDescStart_31010202_Request_Echo_Routine(DescMsgContext* pMsgContext)
2021                         ; 1075 {
2022                         	switch	.ftext
2023  0689                   f_ApplDescStart_31010202_Request_Echo_Routine:
2025  0689 3b                	pshd	
2026       00000000          OFST:	set	0
2029                         ; 1082 				pMsgContext ->reqData[4] = pMsgContext ->reqData[3];
2031  068a b746              	tfr	d,y
2032  068c ed40              	ldy	0,y
2033  068e 180a4344          	movb	3,y,4,y
2034                         ; 1083 				DataLength = 1;
2036  0692 c601              	ldab	#1
2037  0694 7b0000            	stab	L311_DataLength
2038                         ; 1085 				Response = RESPONSE_OK;
2040  0697 7b0001            	stab	L111_Response
2041                         ; 1086 				diagF_DiagResponse(Response, DataLength, pMsgContext);
2043  069a ec80              	ldd	OFST+0,s
2044  069c 3b                	pshd	
2045  069d cc0001            	ldd	#1
2046  06a0 3b                	pshd	
2047  06a1 4a000000          	call	f_diagF_DiagResponse
2049  06a5 1b86              	leas	6,s
2050                         ; 1087 }
2053  06a7 0a                	rtc	
2102                         ; 1092 void DESC_API_CALLBACK_TYPE ApplDescStart_31010203_CSWM_Ouput_Test(DescMsgContext* pMsgContext)
2102                         ; 1093 {
2103                         	switch	.ftext
2104  06a8                   f_ApplDescStart_31010203_CSWM_Ouput_Test:
2106  06a8 3b                	pshd	
2107       00000000          OFST:	set	0
2110                         ; 1095 			diagRtnCtrlLF_Clr_All_IO_Control();				
2112  06a9 4a011d1d          	call	f_diagRtnCtrlLF_Clr_All_IO_Control
2114                         ; 1097 			diag_rc_all_op_lock_b = TRUE;
2116  06ad 1c000001          	bset	_diag_rc_lock_flags_c,1
2117                         ; 1099 			mainF_CSWM_Status();	
2119  06b1 160000            	jsr	_mainF_CSWM_Status
2121                         ; 1101 			if((CSWM_config_c & CSWM_HSW_MASK_K) == CSWM_HSW_MASK_K)
2123  06b4 1f00004004        	brclr	_CSWM_config_c,64,L7221
2124                         ; 1104 					stW_clr_ontime_b = TRUE;
2126  06b9 1c000040          	bset	_stW_Flag1_c,64
2128  06bd                   L7221:
2129                         ; 1112 			pMsgContext->resData[0] = diag_op_rc_status_c;	
2131  06bd f60000            	ldab	_diag_op_rc_status_c
2132  06c0 ee80              	ldx	OFST+0,s
2133  06c2 6be30004          	stab	[4,x]
2134                         ; 1114 			diagLF_DCX_PreConditions_Chck();
2136  06c6 4a016868          	call	f_diagLF_DCX_PreConditions_Chck
2138                         ; 1116 			pMsgContext->resData[1] = diag_op_rc_preCondition_stat_c; 
2140  06ca ed80              	ldy	OFST+0,s
2141  06cc ed44              	ldy	4,y
2142  06ce 1809410000        	movb	_diag_op_rc_preCondition_stat_c,1,y
2143                         ; 1117 		  DataLength = 2;
2145  06d3 c602              	ldab	#2
2146  06d5 7b0000            	stab	L311_DataLength
2147                         ; 1119     	diag_op_rc_status_c = TEST_RUNNING;	 			// Set DCX routine control status to TEST_RUNNING
2149  06d8 7b0000            	stab	_diag_op_rc_status_c
2150                         ; 1120     	Response = RESPONSE_OK;    						    // Set data response type to RESPONSE_OK
2152  06db 53                	decb	
2153  06dc 7b0001            	stab	L111_Response
2154                         ; 1121     	diagF_DiagResponse(Response, (pMsgContext->reqDataLen)+DataLength, pMsgContext);
2156  06df ec80              	ldd	OFST+0,s
2157  06e1 3b                	pshd	
2158  06e2 b746              	tfr	d,y
2159  06e4 ee42              	ldx	2,y
2160  06e6 1a02              	leax	2,x
2161  06e8 34                	pshx	
2162  06e9 cc0001            	ldd	#1
2163  06ec 4a000000          	call	f_diagF_DiagResponse
2165  06f0 1b86              	leas	6,s
2166                         ; 1122 }
2169  06f2 0a                	rtc	
2213                         ; 1127 void DESC_API_CALLBACK_TYPE ApplDescStop_31010203_CSWM_Ouput_Test(DescMsgContext* pMsgContext)
2213                         ; 1128 {
2214                         	switch	.ftext
2215  06f3                   f_ApplDescStop_31010203_CSWM_Ouput_Test:
2217  06f3 3b                	pshd	
2218       00000000          OFST:	set	0
2221                         ; 1130 	      diag_op_rc_status_c = TEST_STOPPED;				// Set DCX routine control status to TEST_STOPPED	
2223  06f4 c604              	ldab	#4
2224  06f6 7b0000            	stab	_diag_op_rc_status_c
2225                         ; 1132 			  pMsgContext->resData[0] = diag_op_rc_status_c;	
2227  06f9 ee80              	ldx	OFST+0,s
2228  06fb 6be30004          	stab	[4,x]
2229                         ; 1134 				diagLF_DCX_PreConditions_Chck();
2231  06ff 4a016868          	call	f_diagLF_DCX_PreConditions_Chck
2233                         ; 1136 				pMsgContext->resData[1] = diag_op_rc_preCondition_stat_c; 
2235  0703 ed80              	ldy	OFST+0,s
2236  0705 ed44              	ldy	4,y
2237  0707 1809410000        	movb	_diag_op_rc_preCondition_stat_c,1,y
2238                         ; 1137 		    DataLength = 2;									// Set Data Lenght to 2	    	
2240  070c c602              	ldab	#2
2241  070e 7b0000            	stab	L311_DataLength
2242                         ; 1138         Response = RESPONSE_OK;		    				// Set data response type to RESPONSE_OK
2244  0711 53                	decb	
2245  0712 7b0001            	stab	L111_Response
2246                         ; 1139         diagF_DiagResponse(Response, (pMsgContext->reqDataLen)+DataLength, pMsgContext);
2248  0715 ec80              	ldd	OFST+0,s
2249  0717 3b                	pshd	
2250  0718 b746              	tfr	d,y
2251  071a ee42              	ldx	2,y
2252  071c 1a02              	leax	2,x
2253  071e 34                	pshx	
2254  071f cc0001            	ldd	#1
2255  0722 4a000000          	call	f_diagF_DiagResponse
2257  0726 1b86              	leas	6,s
2258                         ; 1140 }
2261  0728 0a                	rtc	
2305                         ; 1145 void DESC_API_CALLBACK_TYPE ApplDescReqResults_31010203_CSWM_Ouput_Test(DescMsgContext* pMsgContext)
2305                         ; 1146 {
2306                         	switch	.ftext
2307  0729                   f_ApplDescReqResults_31010203_CSWM_Ouput_Test:
2309  0729 3b                	pshd	
2310       00000000          OFST:	set	0
2313                         ; 1149 			  pMsgContext->resData[0] = diag_op_rc_status_c;	
2315  072a f60000            	ldab	_diag_op_rc_status_c
2316  072d ee80              	ldx	OFST+0,s
2317  072f 6be30004          	stab	[4,x]
2318                         ; 1151 				diagLF_DCX_PreConditions_Chck();
2320  0733 4a016868          	call	f_diagLF_DCX_PreConditions_Chck
2322                         ; 1153 				pMsgContext->resData[1] = diag_op_rc_preCondition_stat_c; 
2324  0737 ed80              	ldy	OFST+0,s
2325  0739 ed44              	ldy	4,y
2326  073b 1809410000        	movb	_diag_op_rc_preCondition_stat_c,1,y
2327                         ; 1154 		    DataLength = 2;									// Set Data Lenght to 2	    	
2329  0740 c602              	ldab	#2
2330  0742 7b0000            	stab	L311_DataLength
2331                         ; 1155         Response = RESPONSE_OK;		    				// Set data response type to RESPONSE_OK
2333  0745 53                	decb	
2334  0746 7b0001            	stab	L111_Response
2335                         ; 1156         diagF_DiagResponse(Response, (pMsgContext->reqDataLen)+DataLength, pMsgContext);
2337  0749 ec80              	ldd	OFST+0,s
2338  074b 3b                	pshd	
2339  074c b746              	tfr	d,y
2340  074e ee42              	ldx	2,y
2341  0750 1a02              	leax	2,x
2342  0752 34                	pshx	
2343  0753 cc0001            	ldd	#1
2344  0756 4a000000          	call	f_diagF_DiagResponse
2346  075a 1b86              	leas	6,s
2347                         ; 1157 }
2350  075c 0a                	rtc	
2389                         ; 1191 void DESC_API_CALLBACK_TYPE ApplDescStartFlashErase(DescMsgContext* pMsgContext)
2389                         ; 1192 {
2390                         	switch	.ftext
2391  075d                   f_ApplDescStartFlashErase:
2395                         ; 1207     DescSetNegResponse(kDescNrcRequestOutOfRange);
2397  075d cc0031            	ldd	#49
2398  0760 4a000000          	call	f_DescSetNegResponse
2400                         ; 1210   DescProcessingDone();
2402  0764 4a000000          	call	f_DescProcessingDone
2404                         ; 1211 }
2407  0768 0a                	rtc	
2448                         ; 1218 void DESC_API_CALLBACK_TYPE ApplDescStartcheckProgrammingDependencies_FlashChecksum(DescMsgContext* pMsgContext)
2448                         ; 1219 {
2449                         	switch	.ftext
2450  0769                   f_ApplDescStartcheckProgrammingDependencies_FlashChecksum:
2454                         ; 1234     DescSetNegResponse(kDescNrcRequestOutOfRange);
2456  0769 cc0031            	ldd	#49
2457  076c 4a000000          	call	f_DescSetNegResponse
2459                         ; 1237   DescProcessingDone();
2461  0770 4a000000          	call	f_DescProcessingDone
2463                         ; 1238 }
2466  0774 0a                	rtc	
2505                         ; 1274 void DESC_API_CALLBACK_TYPE ApplDescStopFlashErase(DescMsgContext* pMsgContext)
2505                         ; 1275 {
2506                         	switch	.ftext
2507  0775                   f_ApplDescStopFlashErase:
2511                         ; 1290     DescSetNegResponse(kDescNrcRequestOutOfRange);
2513  0775 cc0031            	ldd	#49
2514  0778 4a000000          	call	f_DescSetNegResponse
2516                         ; 1293   DescProcessingDone();
2518  077c 4a000000          	call	f_DescProcessingDone
2520                         ; 1294 }
2523  0780 0a                	rtc	
2563                         ; 1329 void DESC_API_CALLBACK_TYPE ApplDescStopcheckProgrammingDependencies_FlashChecksum(DescMsgContext* pMsgContext)
2563                         ; 1330 {
2564                         	switch	.ftext
2565  0781                   f_ApplDescStopcheckProgrammingDependencies_FlashChecksum:
2567  0781 3b                	pshd	
2568       00000000          OFST:	set	0
2571                         ; 1334   if(pMsgContext->reqData[0] < 0xFF)
2573  0782 b746              	tfr	d,y
2574  0784 e6eb0000          	ldab	[0,y]
2575  0788 c1ff              	cmpb	#255
2576  078a 240f              	bhs	L1731
2577                         ; 1338     pMsgContext->resData[0] = 0xFF;
2579  078c c6ff              	ldab	#255
2580  078e ed80              	ldy	OFST+0,s
2581  0790 6beb0004          	stab	[4,y]
2582                         ; 1340     pMsgContext->resDataLen = 1;
2584  0794 cc0001            	ldd	#1
2585  0797 6c46              	std	6,y
2587  0799 2007              	bra	L3731
2588  079b                   L1731:
2589                         ; 1345     DescSetNegResponse(kDescNrcRequestOutOfRange);
2591  079b cc0031            	ldd	#49
2592  079e 4a000000          	call	f_DescSetNegResponse
2594  07a2                   L3731:
2595                         ; 1348   DescProcessingDone();
2597  07a2 4a000000          	call	f_DescProcessingDone
2599                         ; 1349 }
2602  07a6 31                	puly	
2603  07a7 0a                	rtc	
2641                         ; 1384 void DESC_API_CALLBACK_TYPE ApplDescReqResultsFlashErase(DescMsgContext* pMsgContext)
2641                         ; 1385 {
2642                         	switch	.ftext
2643  07a8                   f_ApplDescReqResultsFlashErase:
2645  07a8 3b                	pshd	
2646       00000000          OFST:	set	0
2649                         ; 1389   pMsgContext->resData[0] = 0xFF;
2651  07a9 c6ff              	ldab	#255
2652  07ab ed80              	ldy	OFST+0,s
2653  07ad 6beb0004          	stab	[4,y]
2654                         ; 1391   pMsgContext->resDataLen = 1;
2656  07b1 cc0001            	ldd	#1
2657  07b4 6c46              	std	6,y
2658                         ; 1393   DescProcessingDone();
2660  07b6 4a000000          	call	f_DescProcessingDone
2662                         ; 1394 }
2665  07ba 31                	puly	
2666  07bb 0a                	rtc	
2706                         ; 1428 void DESC_API_CALLBACK_TYPE ApplDescReqResultscheckProgrammingDependencies_FlashChecksum(DescMsgContext* pMsgContext)
2706                         ; 1429 {
2707                         	switch	.ftext
2708  07bc                   f_ApplDescReqResultscheckProgrammingDependencies_FlashChecksum:
2710  07bc 3b                	pshd	
2711       00000000          OFST:	set	0
2714                         ; 1433   pMsgContext->resData[0] = 0xFF;
2716  07bd c6ff              	ldab	#255
2717  07bf ed80              	ldy	OFST+0,s
2718  07c1 6beb0004          	stab	[4,y]
2719                         ; 1435   pMsgContext->resDataLen = 1;
2721  07c5 cc0001            	ldd	#1
2722  07c8 6c46              	std	6,y
2723                         ; 1437   DescProcessingDone();
2725  07ca 4a000000          	call	f_DescProcessingDone
2727                         ; 1438 }
2730  07ce 31                	puly	
2731  07cf 0a                	rtc	
2876                         	xref	f_diagF_DiagResponse
2877                         	xdef	f_diagRtnCtrlLF_Clr_All_IO_Control
2878                         	switch	.bss
2879  0000                   L311_DataLength:
2880  0000 00                	ds.b	1
2881  0001                   L111_Response:
2882  0001 00                	ds.b	1
2883  0002                   _diag_DCX_Test_cntr_w:
2884  0002 0000              	ds.b	2
2885                         	xdef	_diag_DCX_Test_cntr_w
2886                         	xref	f_BattVF_Get_LdShd_Mnt_Stat
2887                         	xref	_BattV_set
2888                         	xref	_BattV_cals
2889                         	xref	_intFlt_bytes
2890                         	xref	f_TempF_Get_PCB_NTC_Stat
2891                         	xref	_temperature_s
2892                         	xref	_temperature_HS_NTC_status
2893                         	xref	f_stWF_Diag_DTC_Control
2894                         	xref	_stW_prms
2895                         	xref	_stW_temperature_c
2896                         	xref	_stW_Flt_status_w
2897                         	xref	_stW_Flag1_c
2898                         	xref	f_vsF_Get_Fault_Status
2899                         	xref	f_vsF_Diag_DTC_Control
2900                         	xref	_hs_cal_prms
2901                         	xref	_NTC_AD_Readings_c
2902                         	xref	_CSWM_config_c
2903                         	xref	_hs_vehicle_line_c
2904                         	xref	_hs_status_flags_c
2905                         	xref	_canio_RX_IGN_STATUS_c
2906                         	xref	_canio_status2_flags_c
2907                         	xdef	f_diagLF_DCX_PreConditions_Chck
2908                         	xdef	f_diagF_DCX_OutputTest
2909                         	xref	_diag_io_vs_lock_flags_c
2910                         	xref	_diag_io_hs_lock_flags_c
2911                         	xref	_diag_io_lock3_flags_c
2912                         	xref	_diag_io_lock2_flags_c
2913                         	xref	_diag_io_lock_flags_c
2914                         	xref	_diag_io_vs_state_c
2915                         	xref	_diag_io_vs_rq_c
2916                         	xref	_diag_io_hs_state_c
2917                         	xref	_diag_io_hs_rq_c
2918                         	xref	_diag_op_rc_preCondition_stat_c
2919                         	xref	_diag_op_rc_status_c
2920                         	xref	_diag_io_st_whl_state_c
2921                         	xref	_diag_io_st_whl_rq_c
2922  0004                   L12_diag_sec_FAA_c:
2923  0004 00                	ds.b	1
2924  0005                   L71_secMod1_var2_l:
2925  0005 00000000          	ds.b	4
2926  0009                   L51_secMod1_var1_l:
2927  0009 00000000          	ds.b	4
2928  000d                   L31_diag_10sec_timer_w:
2929  000d 0000              	ds.b	2
2930  000f                   L11_hsm_key_l:
2931  000f 00000000          	ds.b	4
2932  0013                   L7_tool_key_l:
2933  0013 00000000          	ds.b	4
2934  0017                   L5_diag_seed_generated_l:
2935  0017 00000000          	ds.b	4
2936  001b                   L3_diag_seed_l:
2937  001b 00000000          	ds.b	4
2938                         	xref	_diag_rc_lock_flags_c
2939                         	xref	_mainF_CSWM_Status
2940                         	xdef	f_ApplDescReqResultscheckProgrammingDependencies_FlashChecksum
2941                         	xdef	f_ApplDescReqResultsFlashErase
2942                         	xdef	f_ApplDescReqResults_31010203_CSWM_Ouput_Test
2943                         	xdef	f_ApplDescStopcheckProgrammingDependencies_FlashChecksum
2944                         	xdef	f_ApplDescStopFlashErase
2945                         	xdef	f_ApplDescStop_31010203_CSWM_Ouput_Test
2946                         	xdef	f_ApplDescStartcheckProgrammingDependencies_FlashChecksum
2947                         	xdef	f_ApplDescStartFlashErase
2948                         	xdef	f_ApplDescStart_31010203_CSWM_Ouput_Test
2949                         	xdef	f_ApplDescStart_31010202_Request_Echo_Routine
2950                         	xdef	f_ApplDescStart_31010201_Set_Fault_Status
2951                         	xref	f_DescSetNegResponse
2952                         	xref	f_DescProcessingDone
2973                         	xref	c_jltab
2974                         	end
