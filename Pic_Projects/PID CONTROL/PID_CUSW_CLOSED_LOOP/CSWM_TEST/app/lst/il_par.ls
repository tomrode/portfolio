   1                         ; C Compiler for S12XE [COSMIC Software].
   2                         ; Generator V4.7.8 - 27 Jul 2007
   3                         ; Optimizer V4.6.13 - 27 Jul 2007
   4                         .const:	section	.data
   5  0000                   _IlTxStartCycles:
   6  0000 01                	dc.b	1
   7  0001 01                	dc.b	1
   8  0002 01                	dc.b	1
   9  0003                   _IlTxUpdateCycles:
  10  0003 01                	dc.b	1
  11  0004 01                	dc.b	1
  12  0005 03                	dc.b	3
  13  0006                   _IlTxCyclicCycles:
  14  0006 01                	dc.b	1
  15  0007 01                	dc.b	1
  16  0008 32                	dc.b	50
  17  0009                   _IlTxConfirmationFctPtr:
  18  0009 00000000          	dc.l	0
  19  000d 00000000          	dc.l	0
  20  0011 00000000          	dc.l	0
  21  0015                   _IlIndicationOffset:
  22  0015 00                	dc.b	0
  23  0016 00                	dc.b	0
  24  0017 00                	dc.b	0
  25  0018 00                	dc.b	0
  26  0019 00                	dc.b	0
  27  001a 00                	dc.b	0
  28  001b 00                	dc.b	0
  29  001c 00                	dc.b	0
  30  001d 01                	dc.b	1
  31  001e 01                	dc.b	1
  32  001f 01                	dc.b	1
  33  0020 01                	dc.b	1
  34  0021 01                	dc.b	1
  35  0022 01                	dc.b	1
  36  0023 01                	dc.b	1
  37  0024 01                	dc.b	1
  38  0025 02                	dc.b	2
  39  0026 02                	dc.b	2
  40  0027 02                	dc.b	2
  41  0028 02                	dc.b	2
  42  0029 02                	dc.b	2
  43  002a 02                	dc.b	2
  44  002b 02                	dc.b	2
  45  002c 02                	dc.b	2
  46  002d 03                	dc.b	3
  47  002e 03                	dc.b	3
  48  002f 03                	dc.b	3
  49  0030 03                	dc.b	3
  50  0031 03                	dc.b	3
  51  0032 03                	dc.b	3
  52  0033                   _IlIndicationMask:
  53  0033 01                	dc.b	1
  54  0034 02                	dc.b	2
  55  0035 04                	dc.b	4
  56  0036 08                	dc.b	8
  57  0037 10                	dc.b	16
  58  0038 20                	dc.b	32
  59  0039 40                	dc.b	64
  60  003a 80                	dc.b	128
  61  003b 01                	dc.b	1
  62  003c 02                	dc.b	2
  63  003d 04                	dc.b	4
  64  003e 08                	dc.b	8
  65  003f 10                	dc.b	16
  66  0040 20                	dc.b	32
  67  0041 40                	dc.b	64
  68  0042 80                	dc.b	128
  69  0043 01                	dc.b	1
  70  0044 02                	dc.b	2
  71  0045 04                	dc.b	4
  72  0046 08                	dc.b	8
  73  0047 10                	dc.b	16
  74  0048 20                	dc.b	32
  75  0049 40                	dc.b	64
  76  004a 80                	dc.b	128
  77  004b 01                	dc.b	1
  78  004c 02                	dc.b	2
  79  004d 04                	dc.b	4
  80  004e 08                	dc.b	8
  81  004f 10                	dc.b	16
  82  0050 20                	dc.b	32
  83  0051                   L3_ECU_APPL_CSWMIlTxDefaultInitValue:
  84  0051 00                	dc.b	0
  85  0052 00                	dc.b	0
  86  0053 00                	dc.b	0
  87  0054 00                	dc.b	0
  88  0055 00                	dc.b	0
  89  0056 00                	dc.b	0
  90  0057 00                	dc.b	0
  91  0058 00                	dc.b	0
  92  0059                   L5_CFG_DATA_CODE_RSP_CSWMIlTxDefaultInitValue:
  93  0059 00                	dc.b	0
  94  005a 00                	dc.b	0
  95  005b 00                	dc.b	0
  96  005c 00                	dc.b	0
  97  005d 00                	dc.b	0
  98  005e 00                	dc.b	0
  99  005f                   L7_CSWM_A1IlTxDefaultInitValue:
 100  005f 00                	dc.b	0
 101  0060 80                	dc.b	128
 102  0061                   L11_APPL_ECU_CSWMIlRxDefaultInitValue:
 103  0061 00                	dc.b	0
 104  0062 00                	dc.b	0
 105  0063 00                	dc.b	0
 106  0064 00                	dc.b	0
 107  0065 00                	dc.b	0
 108  0066 00                	dc.b	0
 109  0067 00                	dc.b	0
 110  0068 00                	dc.b	0
 111  0069                   L31_CONFIGURATION_DATA_CODE_REQUESTIlRxDefaultInitValue:
 112  0069 00                	dc.b	0
 113  006a 00                	dc.b	0
 114  006b 00                	dc.b	0
 115  006c 00                	dc.b	0
 116  006d 00                	dc.b	0
 117  006e 00                	dc.b	0
 118  006f                   L51_TRIP_A_BIlRxDefaultInitValue:
 119  006f 00                	dc.b	0
 120  0070 00                	dc.b	0
 121  0071 00                	dc.b	0
 122  0072 00                	dc.b	0
 123  0073                   L71_TGW_A1IlRxDefaultInitValue:
 124  0073 00                	dc.b	0
 125  0074 00                	dc.b	0
 126  0075                   L12_ENVIRONMENTAL_CONDITIONSIlRxDefaultInitValue:
 127  0075 00                	dc.b	0
 128  0076 00                	dc.b	0
 129  0077                   L32_StW_Actn_RqIlRxDefaultInitValue:
 130  0077 00                	dc.b	0
 131  0078 00                	dc.b	0
 132  0079 00                	dc.b	0
 133  007a 00                	dc.b	0
 134  007b 00                	dc.b	0
 135  007c 00                	dc.b	0
 136  007d ff                	dc.b	255
 137  007e                   L52_STATUS_C_CANIlRxDefaultInitValue:
 138  007e 00                	dc.b	0
 139  007f                   L72_VEHICLE_SPEED_ODOMETERIlRxDefaultInitValue:
 140  007f 00                	dc.b	0
 141  0080 00                	dc.b	0
 142  0081 00                	dc.b	0
 143  0082 00                	dc.b	0
 144  0083                   L13_STATUS_B_ECMIlRxDefaultInitValue:
 145  0083 00                	dc.b	0
 146  0084 00                	dc.b	0
 147  0085 00                	dc.b	0
 148  0086 00                	dc.b	0
 149  0087 00                	dc.b	0
 150  0088 00                	dc.b	0
 151  0089 00                	dc.b	0
 152  008a 00                	dc.b	0
 153  008b                   L33_VINIlRxDefaultInitValue:
 154  008b 03                	dc.b	3
 155  008c 00                	dc.b	0
 156  008d 00                	dc.b	0
 157  008e 00                	dc.b	0
 158  008f 00                	dc.b	0
 159  0090 00                	dc.b	0
 160  0091 00                	dc.b	0
 161  0092 00                	dc.b	0
 162  0093                   L53_CBC_I4IlRxDefaultInitValue:
 163  0093 00                	dc.b	0
 164  0094                   L73_CFG_RQIlRxDefaultInitValue:
 165  0094 00                	dc.b	0
 166  0095 00                	dc.b	0
 167  0096                   _IlTxDefaultInitValue:
 168  0096 0051              	dc.w	L3_ECU_APPL_CSWMIlTxDefaultInitValue
 169  0098 0059              	dc.w	L5_CFG_DATA_CODE_RSP_CSWMIlTxDefaultInitValue
 170  009a 005f              	dc.w	L7_CSWM_A1IlTxDefaultInitValue
 171  009c                   _IlRxDefaultInitValue:
 172  009c 0061              	dc.w	L11_APPL_ECU_CSWMIlRxDefaultInitValue
 173  009e 0069              	dc.w	L31_CONFIGURATION_DATA_CODE_REQUESTIlRxDefaultInitValue
 174  00a0 006f              	dc.w	L51_TRIP_A_BIlRxDefaultInitValue
 175  00a2 0073              	dc.w	L71_TGW_A1IlRxDefaultInitValue
 176  00a4 0075              	dc.w	L12_ENVIRONMENTAL_CONDITIONSIlRxDefaultInitValue
 177  00a6 0077              	dc.w	L32_StW_Actn_RqIlRxDefaultInitValue
 178  00a8 007e              	dc.w	L52_STATUS_C_CANIlRxDefaultInitValue
 179  00aa 007f              	dc.w	L72_VEHICLE_SPEED_ODOMETERIlRxDefaultInitValue
 180  00ac 0083              	dc.w	L13_STATUS_B_ECMIlRxDefaultInitValue
 181  00ae 008b              	dc.w	L33_VINIlRxDefaultInitValue
 182  00b0 0093              	dc.w	L53_CBC_I4IlRxDefaultInitValue
 183  00b2 0094              	dc.w	L73_CFG_RQIlRxDefaultInitValue
 228                         ; 447 void IlMsgAPPL_ECU_CSWMIndication(CanReceiveHandle rxObject)
 228                         ; 448 {
 229                         .ftext:	section	.text
 230  0000                   f_IlMsgAPPL_ECU_CSWMIndication:
 234                         ; 449   rxObject = rxObject;
 236                         ; 450   IlEnterCriticalFlagAccess();
 238  0000 4a000000          	call	f_VStdSuspendAllInterrupts
 240                         ; 451   ilRxIndicationFlags[0] |= (vuint8) 0x01;
 242  0004 1c000001          	bset	_ilRxIndicationFlags,1
 243                         ; 452   IlLeaveCriticalFlagAccess();
 245  0008 4a000000          	call	f_VStdResumeAllInterrupts
 247                         ; 453 }
 250  000c 0a                	rtc	
 286                         ; 456 void IlMsgCONFIGURATION_DATA_CODE_REQUESTIndication(CanReceiveHandle rxObject)
 286                         ; 457 {
 287                         	switch	.ftext
 288  000d                   f_IlMsgCONFIGURATION_DATA_CODE_REQUESTIndication:
 292                         ; 458   rxObject = rxObject;
 294                         ; 459   IlEnterCriticalFlagAccess();
 296  000d 4a000000          	call	f_VStdSuspendAllInterrupts
 298                         ; 460   ilRxIndicationFlags[0] |= (vuint8) 0xFE;
 300  0011 1c0000fe          	bset	_ilRxIndicationFlags,254
 301                         ; 461   ilRxIndicationFlags[1] |= (vuint8) 0x0F;
 303  0015 1c00010f          	bset	_ilRxIndicationFlags+1,15
 304                         ; 462   IlLeaveCriticalFlagAccess();
 306  0019 4a000000          	call	f_VStdResumeAllInterrupts
 308                         ; 463 }
 311  001d 0a                	rtc	
 346                         ; 466 void IlMsgTRIP_A_BIndication(CanReceiveHandle rxObject)
 346                         ; 467 {
 347                         	switch	.ftext
 348  001e                   f_IlMsgTRIP_A_BIndication:
 352                         ; 468   rxObject = rxObject;
 354                         ; 469   IlEnterCriticalFlagAccess();
 356  001e 4a000000          	call	f_VStdSuspendAllInterrupts
 358                         ; 470   ilRxIndicationFlags[1] |= (vuint8) 0x10;
 360  0022 1c000110          	bset	_ilRxIndicationFlags+1,16
 361                         ; 471   IlLeaveCriticalFlagAccess();
 363  0026 4a000000          	call	f_VStdResumeAllInterrupts
 365                         ; 472 }
 368  002a 0a                	rtc	
 403                         ; 475 void IlMsgTGW_A1Indication(CanReceiveHandle rxObject)
 403                         ; 476 {
 404                         	switch	.ftext
 405  002b                   f_IlMsgTGW_A1Indication:
 409                         ; 477   rxObject = rxObject;
 411                         ; 478   IlEnterCriticalFlagAccess();
 413  002b 4a000000          	call	f_VStdSuspendAllInterrupts
 415                         ; 479   ilRxIndicationFlags[1] |= (vuint8) 0xE0;
 417  002f 1c0001e0          	bset	_ilRxIndicationFlags+1,224
 418                         ; 480   IlLeaveCriticalFlagAccess();
 420  0033 4a000000          	call	f_VStdResumeAllInterrupts
 422                         ; 481 }
 425  0037 0a                	rtc	
 461                         ; 484 void IlMsgENVIRONMENTAL_CONDITIONSIndication(CanReceiveHandle rxObject)
 461                         ; 485 {
 462                         	switch	.ftext
 463  0038                   f_IlMsgENVIRONMENTAL_CONDITIONSIndication:
 467                         ; 486   rxObject = rxObject;
 469                         ; 487   IlEnterCriticalFlagAccess();
 471  0038 4a000000          	call	f_VStdSuspendAllInterrupts
 473                         ; 488   ilRxIndicationFlags[2] |= (vuint8) 0x03;
 475  003c 1c000203          	bset	_ilRxIndicationFlags+2,3
 476                         ; 489   IlLeaveCriticalFlagAccess();
 478  0040 4a000000          	call	f_VStdResumeAllInterrupts
 480                         ; 490 }
 483  0044 0a                	rtc	
 518                         ; 493 void IlMsgStW_Actn_RqIndication(CanReceiveHandle rxObject)
 518                         ; 494 {
 519                         	switch	.ftext
 520  0045                   f_IlMsgStW_Actn_RqIndication:
 524                         ; 495   rxObject = rxObject;
 526                         ; 496   IlEnterCriticalFlagAccess();
 528  0045 4a000000          	call	f_VStdSuspendAllInterrupts
 530                         ; 497   ilRxIndicationFlags[2] |= (vuint8) 0x0C;
 532  0049 1c00020c          	bset	_ilRxIndicationFlags+2,12
 533                         ; 498   IlLeaveCriticalFlagAccess();
 535  004d 4a000000          	call	f_VStdResumeAllInterrupts
 537                         ; 499 }
 540  0051 0a                	rtc	
 575                         ; 502 void IlMsgSTATUS_C_CANIndication(CanReceiveHandle rxObject)
 575                         ; 503 {
 576                         	switch	.ftext
 577  0052                   f_IlMsgSTATUS_C_CANIndication:
 581                         ; 504   rxObject = rxObject;
 583                         ; 505   IlEnterCriticalFlagAccess();
 585  0052 4a000000          	call	f_VStdSuspendAllInterrupts
 587                         ; 506   ilRxIndicationFlags[2] |= (vuint8) 0x10;
 589  0056 1c000210          	bset	_ilRxIndicationFlags+2,16
 590                         ; 507   IlLeaveCriticalFlagAccess();
 592  005a 4a000000          	call	f_VStdResumeAllInterrupts
 594                         ; 508 }
 597  005e 0a                	rtc	
 633                         ; 511 void IlMsgVEHICLE_SPEED_ODOMETERIndication(CanReceiveHandle rxObject)
 633                         ; 512 {
 634                         	switch	.ftext
 635  005f                   f_IlMsgVEHICLE_SPEED_ODOMETERIndication:
 639                         ; 513   rxObject = rxObject;
 641                         ; 514   IlEnterCriticalFlagAccess();
 643  005f 4a000000          	call	f_VStdSuspendAllInterrupts
 645                         ; 515   ilRxIndicationFlags[2] |= (vuint8) 0x20;
 647  0063 1c000220          	bset	_ilRxIndicationFlags+2,32
 648                         ; 516   IlLeaveCriticalFlagAccess();
 650  0067 4a000000          	call	f_VStdResumeAllInterrupts
 652                         ; 517 }
 655  006b 0a                	rtc	
 690                         ; 520 void IlMsgSTATUS_B_ECMIndication(CanReceiveHandle rxObject)
 690                         ; 521 {
 691                         	switch	.ftext
 692  006c                   f_IlMsgSTATUS_B_ECMIndication:
 696                         ; 522   rxObject = rxObject;
 698                         ; 523   IlEnterCriticalFlagAccess();
 700  006c 4a000000          	call	f_VStdSuspendAllInterrupts
 702                         ; 524   ilRxIndicationFlags[2] |= (vuint8) 0xC0;
 704  0070 1c0002c0          	bset	_ilRxIndicationFlags+2,192
 705                         ; 525   IlLeaveCriticalFlagAccess();
 707  0074 4a000000          	call	f_VStdResumeAllInterrupts
 709                         ; 526 }
 712  0078 0a                	rtc	
 746                         ; 529 void IlMsgVINIndication(CanReceiveHandle rxObject)
 746                         ; 530 {
 747                         	switch	.ftext
 748  0079                   f_IlMsgVINIndication:
 752                         ; 531   rxObject = rxObject;
 754                         ; 532   IlEnterCriticalFlagAccess();
 756  0079 4a000000          	call	f_VStdSuspendAllInterrupts
 758                         ; 533   ilRxIndicationFlags[3] |= (vuint8) 0x03;
 760  007d 1c000303          	bset	_ilRxIndicationFlags+3,3
 761                         ; 534   IlLeaveCriticalFlagAccess();
 763  0081 4a000000          	call	f_VStdResumeAllInterrupts
 765                         ; 535 }
 768  0085 0a                	rtc	
 803                         ; 538 void IlMsgCBC_I4Indication(CanReceiveHandle rxObject)
 803                         ; 539 {
 804                         	switch	.ftext
 805  0086                   f_IlMsgCBC_I4Indication:
 809                         ; 540   rxObject = rxObject;
 811                         ; 541   IlEnterCriticalFlagAccess();
 813  0086 4a000000          	call	f_VStdSuspendAllInterrupts
 815                         ; 542   ilRxIndicationFlags[3] |= (vuint8) 0x0C;
 817  008a 1c00030c          	bset	_ilRxIndicationFlags+3,12
 818                         ; 543   IlLeaveCriticalFlagAccess();
 820  008e 4a000000          	call	f_VStdResumeAllInterrupts
 822                         ; 544 }
 825  0092 0a                	rtc	
 860                         ; 547 void IlMsgCFG_RQIndication(CanReceiveHandle rxObject)
 860                         ; 548 {
 861                         	switch	.ftext
 862  0093                   f_IlMsgCFG_RQIndication:
 866                         ; 549   rxObject = rxObject;
 868                         ; 550   IlEnterCriticalFlagAccess();
 870  0093 4a000000          	call	f_VStdSuspendAllInterrupts
 872                         ; 551   ilRxIndicationFlags[3] |= (vuint8) 0x30;
 874  0097 1c000330          	bset	_ilRxIndicationFlags+3,48
 875                         ; 552   IlLeaveCriticalFlagAccess();
 877  009b 4a000000          	call	f_VStdResumeAllInterrupts
 879                         ; 553 }
 882  009f 0a                	rtc	
 908                         ; 616 void IlResetCanConfirmationFlags(void )
 908                         ; 617 {
 909                         	switch	.ftext
 910  00a0                   f_IlResetCanConfirmationFlags:
 914                         ; 618   CanGlobalInterruptDisable();
 916  00a0 4a000000          	call	f_VStdSuspendAllInterrupts
 918                         ; 619   IlGetTxConfirmationFlags(0) &= (vuint8) 0xF8;
 920  00a4 1d000007          	bclr	_CanConfirmationFlags,7
 921                         ; 620   CanGlobalInterruptRestore();
 923  00a8 4a000000          	call	f_VStdResumeAllInterrupts
 925                         ; 621 }
 928  00ac 0a                	rtc	
 963                         ; 634 vuint32 IlGetRxTotalOdometer(void)
 963                         ; 635 {
 964                         	switch	.ftext
 965  00ad                   f_IlGetRxTotalOdometer:
 967  00ad 1b9c              	leas	-4,s
 968       00000004          OFST:	set	4
 971                         ; 637   IlEnterCriticalTotalOdometer();
 973  00af 4a000000          	call	f_VStdSuspendAllInterrupts
 975                         ; 638   rc = ((vuint32) TRIP_A_B.TRIP_A_B.TotalOdometer_0);
 977  00b3 f60003            	ldab	_TRIP_A_B+3
 978  00b6 87                	clra	
 979  00b7 1887              	clrx	
 980  00b9 6c82              	std	OFST-2,s
 981  00bb 6e80              	stx	OFST-4,s
 982                         ; 639   rc |= ((vuint32) TRIP_A_B.TRIP_A_B.TotalOdometer_1) << 8;
 984  00bd f60002            	ldab	_TRIP_A_B+2
 985  00c0 cd0100            	ldy	#256
 986  00c3 13                	emul	
 987  00c4 aa82              	oraa	OFST-2,s
 988  00c6 ea83              	orab	OFST-1,s
 989  00c8 18ea80            	ory	OFST-4,s
 990  00cb 6c82              	std	OFST-2,s
 991  00cd 6d80              	sty	OFST-4,s
 992                         ; 640   rc |= ((vuint32) TRIP_A_B.TRIP_A_B.TotalOdometer_2) << 16;
 994  00cf f60001            	ldab	_TRIP_A_B+1
 995  00d2 c40f              	andb	#15
 996  00d4 87                	clra	
 997  00d5 b745              	tfr	d,x
 998  00d7 aa82              	oraa	OFST-2,s
 999  00d9 e683              	ldab	OFST-1,s
1000  00db 18aa80            	orx	OFST-4,s
1001  00de 6c82              	std	OFST-2,s
1002  00e0 6e80              	stx	OFST-4,s
1003                         ; 641   IlLeaveCriticalTotalOdometer();
1005  00e2 4a000000          	call	f_VStdResumeAllInterrupts
1007                         ; 642   return rc;
1009  00e6 ec82              	ldd	OFST-2,s
1010  00e8 ee80              	ldx	OFST-4,s
1013  00ea 1b84              	leas	4,s
1014  00ec 0a                	rtc	
1052                         ; 656 void IlGetRxAPPL_ECU_CSWM(vuint8* pBuffer)
1052                         ; 657 {
1053                         	switch	.ftext
1054  00ed                   f_IlGetRxAPPL_ECU_CSWM:
1056  00ed 3b                	pshd	
1057       00000000          OFST:	set	0
1060                         ; 658   IlEnterCriticalAPPL_ECU_CSWM();
1062  00ee 4a000000          	call	f_VStdSuspendAllInterrupts
1064                         ; 659   pBuffer[0] = (vuint8) APPL_ECU_CSWM.APPL_ECU_CSWM.APPL_ECU_CSWM_0;
1066  00f2 ed80              	ldy	OFST+0,s
1067  00f4 1809400007        	movb	_APPL_ECU_CSWM+7,0,y
1068                         ; 660   pBuffer[1] = (vuint8) APPL_ECU_CSWM.APPL_ECU_CSWM.APPL_ECU_CSWM_1;
1070  00f9 1809410006        	movb	_APPL_ECU_CSWM+6,1,y
1071                         ; 661   pBuffer[2] = (vuint8) APPL_ECU_CSWM.APPL_ECU_CSWM.APPL_ECU_CSWM_2;
1073  00fe 1809420005        	movb	_APPL_ECU_CSWM+5,2,y
1074                         ; 662   pBuffer[3] = (vuint8) APPL_ECU_CSWM.APPL_ECU_CSWM.APPL_ECU_CSWM_3;
1076  0103 1809430004        	movb	_APPL_ECU_CSWM+4,3,y
1077                         ; 663   pBuffer[4] = (vuint8) APPL_ECU_CSWM.APPL_ECU_CSWM.APPL_ECU_CSWM_4;
1079  0108 1809440003        	movb	_APPL_ECU_CSWM+3,4,y
1080                         ; 664   pBuffer[5] = (vuint8) APPL_ECU_CSWM.APPL_ECU_CSWM.APPL_ECU_CSWM_5;
1082  010d 1809450002        	movb	_APPL_ECU_CSWM+2,5,y
1083                         ; 665   pBuffer[6] = (vuint8) APPL_ECU_CSWM.APPL_ECU_CSWM.APPL_ECU_CSWM_6;
1085  0112 1809460001        	movb	_APPL_ECU_CSWM+1,6,y
1086                         ; 666   pBuffer[7] = (vuint8) APPL_ECU_CSWM.APPL_ECU_CSWM.APPL_ECU_CSWM_7;
1088  0117 1809470000        	movb	_APPL_ECU_CSWM,7,y
1089                         ; 667   IlLeaveCriticalAPPL_ECU_CSWM();
1091  011c 4a000000          	call	f_VStdResumeAllInterrupts
1093                         ; 668 }
1096  0120 31                	puly	
1097  0121 0a                	rtc	
1134                         ; 675 void IlGetRxVIN_DATA(vuint8* pBuffer)
1134                         ; 676 {
1135                         	switch	.ftext
1136  0122                   f_IlGetRxVIN_DATA:
1138  0122 3b                	pshd	
1139       00000000          OFST:	set	0
1142                         ; 677   IlEnterCriticalVIN_DATA();
1144  0123 4a000000          	call	f_VStdSuspendAllInterrupts
1146                         ; 678   pBuffer[0] = (vuint8) VIN.VIN.VIN_DATA_0;
1148  0127 ed80              	ldy	OFST+0,s
1149  0129 1809400007        	movb	_VIN+7,0,y
1150                         ; 679   pBuffer[1] = (vuint8) VIN.VIN.VIN_DATA_1;
1152  012e 1809410006        	movb	_VIN+6,1,y
1153                         ; 680   pBuffer[2] = (vuint8) VIN.VIN.VIN_DATA_2;
1155  0133 1809420005        	movb	_VIN+5,2,y
1156                         ; 681   pBuffer[3] = (vuint8) VIN.VIN.VIN_DATA_3;
1158  0138 1809430004        	movb	_VIN+4,3,y
1159                         ; 682   pBuffer[4] = (vuint8) VIN.VIN.VIN_DATA_4;
1161  013d 1809440003        	movb	_VIN+3,4,y
1162                         ; 683   pBuffer[5] = (vuint8) VIN.VIN.VIN_DATA_5;
1164  0142 1809450002        	movb	_VIN+2,5,y
1165                         ; 684   pBuffer[6] = (vuint8) VIN.VIN.VIN_DATA_6;
1167  0147 1809460001        	movb	_VIN+1,6,y
1168                         ; 685   IlLeaveCriticalVIN_DATA();
1170  014c 4a000000          	call	f_VStdResumeAllInterrupts
1172                         ; 686 }
1175  0150 31                	puly	
1176  0151 0a                	rtc	
1214                         ; 699 void IlPutTxECU_APPL_CSWM(vuint8* pData)
1214                         ; 700 {
1215                         	switch	.ftext
1216  0152                   f_IlPutTxECU_APPL_CSWM:
1218  0152 3b                	pshd	
1219       00000000          OFST:	set	0
1222                         ; 701   IlEnterCriticalECU_APPL_CSWM();
1224  0153 4a000000          	call	f_VStdSuspendAllInterrupts
1226                         ; 702   ECU_APPL_CSWM.ECU_APPL_CSWM.ECU_APPL_CSWM_0 = pData[0];
1228  0157 ed80              	ldy	OFST+0,s
1229  0159 180d400007        	movb	0,y,_ECU_APPL_CSWM+7
1230                         ; 703   ECU_APPL_CSWM.ECU_APPL_CSWM.ECU_APPL_CSWM_1 = pData[1];
1232  015e 180d410006        	movb	1,y,_ECU_APPL_CSWM+6
1233                         ; 704   ECU_APPL_CSWM.ECU_APPL_CSWM.ECU_APPL_CSWM_2 = pData[2];
1235  0163 180d420005        	movb	2,y,_ECU_APPL_CSWM+5
1236                         ; 705   ECU_APPL_CSWM.ECU_APPL_CSWM.ECU_APPL_CSWM_3 = pData[3];
1238  0168 180d430004        	movb	3,y,_ECU_APPL_CSWM+4
1239                         ; 706   ECU_APPL_CSWM.ECU_APPL_CSWM.ECU_APPL_CSWM_4 = pData[4];
1241  016d 180d440003        	movb	4,y,_ECU_APPL_CSWM+3
1242                         ; 707   ECU_APPL_CSWM.ECU_APPL_CSWM.ECU_APPL_CSWM_5 = pData[5];
1244  0172 180d450002        	movb	5,y,_ECU_APPL_CSWM+2
1245                         ; 708   ECU_APPL_CSWM.ECU_APPL_CSWM.ECU_APPL_CSWM_6 = pData[6];
1247  0177 180d460001        	movb	6,y,_ECU_APPL_CSWM+1
1248                         ; 709   ECU_APPL_CSWM.ECU_APPL_CSWM.ECU_APPL_CSWM_7 = pData[7];
1250  017c 180d470000        	movb	7,y,_ECU_APPL_CSWM
1251                         ; 710   IlLeaveCriticalECU_APPL_CSWM();
1253  0181 4a000000          	call	f_VStdResumeAllInterrupts
1255                         ; 711 }
1258  0185 31                	puly	
1259  0186 0a                	rtc	
1294                         ; 718 void IlPutTxDigit_01(vuint8 sigData)
1294                         ; 719 {
1295                         	switch	.ftext
1296  0187                   f_IlPutTxDigit_01:
1298  0187 3b                	pshd	
1299       00000000          OFST:	set	0
1302                         ; 720   IlEnterCriticalDigit_01();
1304  0188 4a000000          	call	f_VStdSuspendAllInterrupts
1306                         ; 721   CFG_DATA_CODE_RSP_CSWM.CFG_DATA_CODE_RSP_CSWM.Digit_01 = ((vuint8) (sigData & (vuint8) 0x0F));
1308  018c e681              	ldab	OFST+1,s
1309  018e c40f              	andb	#15
1310  0190 58                	lslb	
1311  0191 58                	lslb	
1312  0192 58                	lslb	
1313  0193 58                	lslb	
1314  0194 1d0005f0          	bclr	_CFG_DATA_CODE_RSP_CSWM+5,240
1315  0198 c4f0              	andb	#240
1316  019a fa0005            	orab	_CFG_DATA_CODE_RSP_CSWM+5
1317  019d 7b0005            	stab	_CFG_DATA_CODE_RSP_CSWM+5
1318                         ; 722   IlSetEvent(IlTxSigHndDigit_01);
1320  01a0 cc0001            	ldd	#1
1321  01a3 4a000000          	call	f_IlSetEvent
1323                         ; 723   IlLeaveCriticalDigit_01();
1325  01a7 4a000000          	call	f_VStdResumeAllInterrupts
1327                         ; 724 }
1330  01ab 31                	puly	
1331  01ac 0a                	rtc	
1367                         ; 731 void IlPutTxFL_HS_STATSts(vuint8 sigData)
1367                         ; 732 {
1368                         	switch	.ftext
1369  01ad                   f_IlPutTxFL_HS_STATSts:
1371  01ad 3b                	pshd	
1372  01ae 37                	pshb	
1373       00000001          OFST:	set	1
1376                         ; 733   if(FL_HS_STATStsValueChanged(sigData))
1378  01af f60000            	ldab	_CSWM_A1
1379  01b2 c403              	andb	#3
1380  01b4 6b80              	stab	OFST-1,s
1381  01b6 e682              	ldab	OFST+1,s
1382  01b8 e180              	cmpb	OFST-1,s
1383  01ba 271d              	beq	L514
1384                         ; 735     IlEnterCriticalFL_HS_STATSts();
1386  01bc 4a000000          	call	f_VStdSuspendAllInterrupts
1388                         ; 736     CSWM_A1.CSWM_A1.FL_HS_STATSts = ((vuint8) (sigData & (vuint8) 0x03));
1390  01c0 e682              	ldab	OFST+1,s
1391  01c2 1d000003          	bclr	_CSWM_A1,3
1392  01c6 c403              	andb	#3
1393  01c8 fa0000            	orab	_CSWM_A1
1394  01cb 7b0000            	stab	_CSWM_A1
1395                         ; 737     IlSetEvent(IlTxSigHndFL_HS_STATSts);
1397  01ce cc0002            	ldd	#2
1398  01d1 4a000000          	call	f_IlSetEvent
1400                         ; 738     IlLeaveCriticalFL_HS_STATSts();
1402  01d5 4a000000          	call	f_VStdResumeAllInterrupts
1404  01d9                   L514:
1405                         ; 740 }
1408  01d9 1b83              	leas	3,s
1409  01db 0a                	rtc	
1444                         ; 747 void IlPutTxHSW_StatSts(vuint8 sigData)
1444                         ; 748 {
1445                         	switch	.ftext
1446  01dc                   f_IlPutTxHSW_StatSts:
1448  01dc 3b                	pshd	
1449  01dd 37                	pshb	
1450       00000001          OFST:	set	1
1453                         ; 749   if(HSW_StatStsValueChanged(sigData))
1455  01de f60000            	ldab	_CSWM_A1
1456  01e1 55                	rolb	
1457  01e2 55                	rolb	
1458  01e3 c401              	andb	#1
1459  01e5 6b80              	stab	OFST-1,s
1460  01e7 e682              	ldab	OFST+1,s
1461  01e9 e180              	cmpb	OFST-1,s
1462  01eb 271d              	beq	L334
1463                         ; 751     IlEnterCriticalHSW_StatSts();
1465  01ed 4a000000          	call	f_VStdSuspendAllInterrupts
1467                         ; 752     CSWM_A1.CSWM_A1.HSW_StatSts = ((vuint8) (sigData & (vuint8) 0x01));
1469  01f1 0e820106          	brset	OFST+1,s,1,L45
1470  01f5 1d000080          	bclr	_CSWM_A1,128
1471  01f9 2004              	bra	L65
1472  01fb                   L45:
1473  01fb 1c000080          	bset	_CSWM_A1,128
1474  01ff                   L65:
1475                         ; 753     IlSetEvent(IlTxSigHndHSW_StatSts);
1477  01ff cc0002            	ldd	#2
1478  0202 4a000000          	call	f_IlSetEvent
1480                         ; 754     IlLeaveCriticalHSW_StatSts();
1482  0206 4a000000          	call	f_VStdResumeAllInterrupts
1484  020a                   L334:
1485                         ; 756 }
1488  020a 1b83              	leas	3,s
1489  020c 0a                	rtc	
1525                         ; 763 void IlPutTxFR_HS_STATSts(vuint8 sigData)
1525                         ; 764 {
1526                         	switch	.ftext
1527  020d                   f_IlPutTxFR_HS_STATSts:
1529  020d 3b                	pshd	
1530  020e 37                	pshb	
1531       00000001          OFST:	set	1
1534                         ; 765   if(FR_HS_STATStsValueChanged(sigData))
1536  020f f60001            	ldab	_CSWM_A1+1
1537  0212 c403              	andb	#3
1538  0214 6b80              	stab	OFST-1,s
1539  0216 e682              	ldab	OFST+1,s
1540  0218 e180              	cmpb	OFST-1,s
1541  021a 271d              	beq	L154
1542                         ; 767     IlEnterCriticalFR_HS_STATSts();
1544  021c 4a000000          	call	f_VStdSuspendAllInterrupts
1546                         ; 768     CSWM_A1.CSWM_A1.FR_HS_STATSts = ((vuint8) (sigData & (vuint8) 0x03));
1548  0220 e682              	ldab	OFST+1,s
1549  0222 1d000103          	bclr	_CSWM_A1+1,3
1550  0226 c403              	andb	#3
1551  0228 fa0001            	orab	_CSWM_A1+1
1552  022b 7b0001            	stab	_CSWM_A1+1
1553                         ; 769     IlSetEvent(IlTxSigHndFR_HS_STATSts);
1555  022e cc0002            	ldd	#2
1556  0231 4a000000          	call	f_IlSetEvent
1558                         ; 770     IlLeaveCriticalFR_HS_STATSts();
1560  0235 4a000000          	call	f_VStdResumeAllInterrupts
1562  0239                   L154:
1563                         ; 772 }
1566  0239 1b83              	leas	3,s
1567  023b 0a                	rtc	
1603                         ; 779 void IlPutTxRemSt_DCSSts(vuint8 sigData)
1603                         ; 780 {
1604                         	switch	.ftext
1605  023c                   f_IlPutTxRemSt_DCSSts:
1607  023c 3b                	pshd	
1608  023d 37                	pshb	
1609       00000001          OFST:	set	1
1612                         ; 781   if(RemSt_DCSStsValueChanged(sigData))
1614  023e f60001            	ldab	_CSWM_A1+1
1615  0241 55                	rolb	
1616  0242 55                	rolb	
1617  0243 c401              	andb	#1
1618  0245 6b80              	stab	OFST-1,s
1619  0247 e682              	ldab	OFST+1,s
1620  0249 e180              	cmpb	OFST-1,s
1621  024b 271d              	beq	L764
1622                         ; 783     IlEnterCriticalRemSt_DCSSts();
1624  024d 4a000000          	call	f_VStdSuspendAllInterrupts
1626                         ; 784     CSWM_A1.CSWM_A1.RemSt_DCSSts = ((vuint8) (sigData & (vuint8) 0x01));
1628  0251 0e820106          	brset	OFST+1,s,1,L46
1629  0255 1d000180          	bclr	_CSWM_A1+1,128
1630  0259 2004              	bra	L66
1631  025b                   L46:
1632  025b 1c000180          	bset	_CSWM_A1+1,128
1633  025f                   L66:
1634                         ; 785     IlSetEvent(IlTxSigHndRemSt_DCSSts);
1636  025f cc0002            	ldd	#2
1637  0262 4a000000          	call	f_IlSetEvent
1639                         ; 786     IlLeaveCriticalRemSt_DCSSts();
1641  0266 4a000000          	call	f_VStdResumeAllInterrupts
1643  026a                   L764:
1644                         ; 788 }
1647  026a 1b83              	leas	3,s
1648  026c 0a                	rtc	
1650                         	switch	.const
1651  00b4                   _IlTxIndirection:
1652  00b4 0000              	dc.w	0
1653  00b6 0001              	dc.w	1
1654  00b8 0004              	dc.w	4
1655  00ba                   _IlTxType:
1656  00ba 00                	dc.b	0
1657  00bb 00                	dc.b	0
1658  00bc 01                	dc.b	1
1947                         	xdef	f_IlResetCanConfirmationFlags
1948                         	xref	f_IlSetEvent
1949                         	xdef	_IlTxIndirection
1950                         	xdef	_IlTxConfirmationFctPtr
1951                         	xdef	_IlTxUpdateCycles
1952                         	xdef	_IlTxStartCycles
1953                         	xdef	_IlTxCyclicCycles
1954                         	xdef	_IlTxType
1955                         	xdef	_IlTxDefaultInitValue
1956                         	xdef	_IlIndicationMask
1957                         	xdef	_IlIndicationOffset
1958                         	xdef	_IlRxDefaultInitValue
1959                         	xref	_ilRxIndicationFlags
1960                         	xdef	f_IlPutTxRemSt_DCSSts
1961                         	xdef	f_IlPutTxFR_HS_STATSts
1962                         	xdef	f_IlPutTxHSW_StatSts
1963                         	xdef	f_IlPutTxFL_HS_STATSts
1964                         	xdef	f_IlPutTxDigit_01
1965                         	xdef	f_IlPutTxECU_APPL_CSWM
1966                         	xdef	f_IlGetRxVIN_DATA
1967                         	xdef	f_IlGetRxAPPL_ECU_CSWM
1968                         	xdef	f_IlGetRxTotalOdometer
1969                         	xdef	f_IlMsgCFG_RQIndication
1970                         	xdef	f_IlMsgCBC_I4Indication
1971                         	xdef	f_IlMsgVINIndication
1972                         	xdef	f_IlMsgSTATUS_B_ECMIndication
1973                         	xdef	f_IlMsgVEHICLE_SPEED_ODOMETERIndication
1974                         	xdef	f_IlMsgSTATUS_C_CANIndication
1975                         	xdef	f_IlMsgStW_Actn_RqIndication
1976                         	xdef	f_IlMsgENVIRONMENTAL_CONDITIONSIndication
1977                         	xdef	f_IlMsgTGW_A1Indication
1978                         	xdef	f_IlMsgTRIP_A_BIndication
1979                         	xdef	f_IlMsgCONFIGURATION_DATA_CODE_REQUESTIndication
1980                         	xdef	f_IlMsgAPPL_ECU_CSWMIndication
1981                         	xref	_APPL_ECU_CSWM
1982                         	xref	_CFG_DATA_CODE_RSP_CSWM
1983                         	xref	_CSWM_A1
1984                         	xref	_ECU_APPL_CSWM
1985                         	xref	_TRIP_A_B
1986                         	xref	_VIN
1987                         	xref	_CanConfirmationFlags
1988                         	xref	f_VStdResumeAllInterrupts
1989                         	xref	f_VStdSuspendAllInterrupts
2009                         	end
