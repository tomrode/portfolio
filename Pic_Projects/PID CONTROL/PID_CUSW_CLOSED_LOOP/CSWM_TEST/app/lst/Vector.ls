   1                         ; C Compiler for S12XE [COSMIC Software].
   2                         ; Generator V4.7.8 - 27 Jul 2007
   3                         ; Optimizer V4.6.13 - 27 Jul 2007
   4                         .vector:	section	.data
   5  0000                   _VectorTable:
   7  0000 0000              	dc.w	_dummy_isr
   9  0002 0000              	dc.w	_dummy_isr
  11  0004 0000              	dc.w	_dummy_isr
  13  0006 0000              	dc.w	_dummy_isr
  15  0008 0000              	dc.w	_dummy_isr
  17  000a 0000              	dc.w	_dummy_isr
  19  000c 0000              	dc.w	_dummy_isr
  21  000e 0000              	dc.w	_dummy_isr
  23  0010 0000              	dc.w	_dummy_isr
  25  0012 0000              	dc.w	_dummy_isr
  27  0014 0000              	dc.w	_dummy_isr
  29  0016 0000              	dc.w	_dummy_isr
  31  0018 0000              	dc.w	_dummy_isr
  33  001a 0000              	dc.w	_dummy_isr
  35  001c 0000              	dc.w	_dummy_isr
  37  001e 0000              	dc.w	_dummy_isr
  39  0020 0000              	dc.w	_dummy_isr
  41  0022 0000              	dc.w	_dummy_isr
  43  0024 0000              	dc.w	_dummy_isr
  45  0026 0000              	dc.w	_dummy_isr
  47  0028 0000              	dc.w	_dummy_isr
  49  002a 0000              	dc.w	_dummy_isr
  51  002c 0000              	dc.w	_dummy_isr
  53  002e 0000              	dc.w	_dummy_isr
  55  0030 0000              	dc.w	_dummy_isr
  57  0032 0000              	dc.w	_dummy_isr
  59  0034 0000              	dc.w	_dummy_isr
  61  0036 0000              	dc.w	_dummy_isr
  63  0038 0000              	dc.w	_dummy_isr
  65  003a 0000              	dc.w	_dummy_isr
  67  003c 0000              	dc.w	_dummy_isr
  69  003e 0000              	dc.w	_dummy_isr
  71  0040 0000              	dc.w	_dummy_isr
  73  0042 0000              	dc.w	_dummy_isr
  75  0044 0000              	dc.w	_dummy_isr
  77  0046 0000              	dc.w	_dummy_isr
  79  0048 0000              	dc.w	_dummy_isr
  81  004a 0000              	dc.w	_dummy_isr
  83  004c 0000              	dc.w	_dummy_isr
  85  004e 0000              	dc.w	_dummy_isr
  87  0050 0000              	dc.w	_dummy_isr
  89  0052 0000              	dc.w	_dummy_isr
  91  0054 0000              	dc.w	_dummy_isr
  93  0056 0000              	dc.w	_dummy_isr
  95  0058 0000              	dc.w	_dummy_isr
  97  005a 0000              	dc.w	_dummy_isr
  99  005c 0000              	dc.w	_dummy_isr
 101  005e 0000              	dc.w	_dummy_isr
 103  0060 0000              	dc.w	_dummy_isr
 105  0062 0000              	dc.w	_dummy_isr
 107  0064 0000              	dc.w	_dummy_isr
 109  0066 0000              	dc.w	_dummy_isr
 111  0068 0000              	dc.w	_dummy_isr
 113  006a 0000              	dc.w	_dummy_isr
 115  006c 0000              	dc.w	_dummy_isr
 117  006e 0000              	dc.w	_dummy_isr
 119  0070 0000              	dc.w	_dummy_isr
 121  0072 0000              	dc.w	_dummy_isr
 123  0074 0000              	dc.w	_dummy_isr
 125  0076 0000              	dc.w	_dummy_isr
 127  0078 0000              	dc.w	_dummy_isr
 129  007a 0000              	dc.w	_dummy_isr
 131  007c 0000              	dc.w	_dummy_isr
 133  007e 0000              	dc.w	_dummy_isr
 135  0080 0000              	dc.w	_dummy_isr
 137  0082 0000              	dc.w	_dummy_isr
 139  0084 0000              	dc.w	_dummy_isr
 141  0086 0000              	dc.w	_dummy_isr
 143  0088 0000              	dc.w	_dummy_isr
 145  008a 0000              	dc.w	_dummy_isr
 147  008c 0000              	dc.w	_dummy_isr
 149  008e 0000              	dc.w	_dummy_isr
 151  0090 0000              	dc.w	_dummy_isr
 153  0092 0000              	dc.w	_dummy_isr
 155  0094 0000              	dc.w	_dummy_isr
 157  0096 0000              	dc.w	_dummy_isr
 159  0098 0000              	dc.w	_dummy_isr
 161  009a 0000              	dc.w	_dummy_isr
 163  009c 0000              	dc.w	_dummy_isr
 165  009e 0000              	dc.w	_dummy_isr
 167  00a0 0000              	dc.w	_CanTxInterrupt_0
 169  00a2 0000              	dc.w	_CanRxInterrupt_0
 171  00a4 0000              	dc.w	_CanErrorInterrupt_0
 173  00a6 0000              	dc.w	_CanWakeUpInterrupt_0
 175  00a8 0000              	dc.w	_IsrFlashCmdCompleteInterrupt
 177  00aa 0000              	dc.w	_dummy_isr
 179  00ac 0000              	dc.w	_dummy_isr
 181  00ae 0000              	dc.w	_dummy_isr
 183  00b0 0000              	dc.w	_dummy_isr
 185  00b2 0000              	dc.w	_dummy_isr
 187  00b4 0000              	dc.w	_CrgScm_Isr
 189  00b6 0000              	dc.w	_CrgPllLck_Isr
 191  00b8 0000              	dc.w	_dummy_isr
 193  00ba 0000              	dc.w	_dummy_isr
 195  00bc 0000              	dc.w	_dummy_isr
 197  00be 0000              	dc.w	_dummy_isr
 199  00c0 0000              	dc.w	_dummy_isr
 201  00c2 0000              	dc.w	_Atd_0_Isr
 203  00c4 0000              	dc.w	_dummy_isr
 205  00c6 0000              	dc.w	_dummy_isr
 207  00c8 0000              	dc.w	_dummy_isr
 209  00ca 0000              	dc.w	_dummy_isr
 211  00cc 0000              	dc.w	_dummy_isr
 213  00ce 0000              	dc.w	_dummy_isr
 215  00d0 0000              	dc.w	_Ect_7_Isr
 217  00d2 0000              	dc.w	_Ect_6_Isr
 219  00d4 0000              	dc.w	_Ect_5_Isr
 221  00d6 0000              	dc.w	_Ect_4_Isr
 223  00d8 0000              	dc.w	_Ect_3_Isr
 225  00da 0000              	dc.w	_Ect_2_Isr
 227  00dc 0000              	dc.w	_dummy_isr
 229  00de 0000              	dc.w	_dummy_isr
 231  00e0 0000              	dc.w	_dummy_isr
 233  00e2 0000              	dc.w	_dummy_isr
 235  00e4 0000              	dc.w	_dummy_isr
 237  00e6 0000              	dc.w	_dummy_isr
 239  00e8 0000              	dc.w	_dummy_isr
 241  00ea 0000              	dc.w	_main
 243  00ec 0000              	dc.w	_dummy_isr
 245  00ee 0000              	dc.w	_main
 277                         ; 186 interrupt near void dummy_isr(void)
 277                         ; 187 {
 278                         	switch	.text
 279  0000                   _dummy_isr:
 284                         ; 188 	_asm("nop"); 
 287  0000 a7                	nop	
 289                         ; 189 }
 292  0001 0b                	rti	
 336                         	xdef	_VectorTable
 337                         	xdef	_dummy_isr
 338                         	xref	_IsrFlashCmdCompleteInterrupt
 339                         	xref	_main
 340                         	xref	_Ect_7_Isr
 341                         	xref	_Ect_6_Isr
 342                         	xref	_Ect_5_Isr
 343                         	xref	_Ect_4_Isr
 344                         	xref	_Ect_3_Isr
 345                         	xref	_Ect_2_Isr
 346                         	xref	_CrgPllLck_Isr
 347                         	xref	_CrgScm_Isr
 348                         	xref	_Atd_0_Isr
 349                         	xref	_CanErrorInterrupt_0
 350                         	xref	_CanWakeUpInterrupt_0
 351                         	xref	_CanTxInterrupt_0
 352                         	xref	_CanRxInterrupt_0
 372                         	end
