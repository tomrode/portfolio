   1                         ; C Compiler for S12XE [COSMIC Software].
   2                         ; Generator V4.7.8 - 27 Jul 2007
   3                         ; Optimizer V4.6.13 - 27 Jul 2007
   4                         .const:	section	.data
   5  0000                   L3_Port_DDR_Reg:
   6  0000 0000              	dc.w	__DDRAB
   7  0002 0001              	dc.w	__DDRAB+1
   8  0004 0000              	dc.w	__DDRE
   9  0006 0000              	dc.w	__DDRT
  10  0008 0000              	dc.w	__DDRS
  11  000a 0000              	dc.w	__DDRM
  12  000c 0000              	dc.w	__DDRP
  13  000e 0000              	dc.w	__DDRJ
  14  0010 0001              	dc.w	__DDR01AD0+1
1402                         ; 249 void Port_Init (const Port_ConfigType *ConfigPtr)
1402                         ; 250 {
1403                         	switch	.text
1404  0000                   _Port_Init:
1406  0000 3b                	pshd	
1407       00000000          OFST:	set	0
1410                         ; 274     PORTA    = ConfigPtr->Port_A.Level.PortLevel;
1412  0001 cd0000            	ldy	#__PORTAB
1413  0004 b745              	tfr	d,x
1414  0006 180a0040          	movb	0,x,0,y
1415                         ; 275     DDRA     = ConfigPtr->Port_AToA2D[PORT_A].Direction.PortDirection;
1417  000a cd0000            	ldy	#__DDRAB
1418  000d 180ae01d40        	movb	29,x,0,y
1419                         ; 278     PORTB    = ConfigPtr->Port_B.Level.PortLevel;
1421  0012 cd0001            	ldy	#__PORTAB+1
1422  0015 180a0140          	movb	1,x,0,y
1423                         ; 279     DDRB     = ConfigPtr->Port_AToA2D[PORT_B].Direction.PortDirection;
1425  0019 cd0001            	ldy	#__DDRAB+1
1426  001c 180ae01f40        	movb	31,x,0,y
1427                         ; 283     PORTE    = ConfigPtr->Port_E.Level.PortLevel;
1429  0021 cd0000            	ldy	#__PORTE
1430  0024 180a0240          	movb	2,x,0,y
1431                         ; 284     DDRE     = ConfigPtr->Port_AToA2D[PORT_E].Direction.PortDirection;
1433  0028 cd0000            	ldy	#__DDRE
1434  002b 180ae02140        	movb	33,x,0,y
1435                         ; 289     DDRK     = PORT_DIR_OUTPUT;
1437  0030 c6ff              	ldab	#255
1438  0032 7b0000            	stab	__DDRK
1439                         ; 292     PTT      = ConfigPtr->Port_T.Level.PortLevel;
1441  0035 cd0000            	ldy	#__PTT
1442  0038 ee80              	ldx	OFST+0,s
1443  003a 180a0340          	movb	3,x,0,y
1444                         ; 293     DDRT     = ConfigPtr->Port_AToA2D[PORT_T].Direction.PortDirection;
1446  003e cd0000            	ldy	#__DDRT
1447  0041 180ae02340        	movb	35,x,0,y
1448                         ; 294     RDRT     = ConfigPtr->Port_T.Drive.PortDrive;
1450  0046 cd0000            	ldy	#__RDRT
1451  0049 180a0440          	movb	4,x,0,y
1452                         ; 295     PERT     = ConfigPtr->Port_T.PullEnable.PortPullEnable;
1454  004d cd0000            	ldy	#__PERT
1455  0050 180a0540          	movb	5,x,0,y
1456                         ; 296     PPST     = ConfigPtr->Port_T.Polarity.PortPolarity;
1458  0054 cd0000            	ldy	#__PPST
1459  0057 180a0640          	movb	6,x,0,y
1460                         ; 297     Port_PTT_Level =  ConfigPtr->Port_T.Level.PortLevel;
1462  005b ed80              	ldy	OFST+0,s
1463  005d 180d430002        	movb	3,y,_Port_PTT_Level
1464                         ; 300     PTS      = ConfigPtr->Port_S.Level.PortLevel;
1466  0062 cd0000            	ldy	#__PTS
1467  0065 180a0740          	movb	7,x,0,y
1468                         ; 301     DDRS     = ConfigPtr->Port_AToA2D[PORT_S].Direction.PortDirection;
1470  0069 cd0000            	ldy	#__DDRS
1471  006c 180ae02540        	movb	37,x,0,y
1472                         ; 302     RDRS     = ConfigPtr->Port_S.Drive.PortDrive;
1474  0071 cd0000            	ldy	#__RDRS
1475  0074 180a0840          	movb	8,x,0,y
1476                         ; 303     PERS     = ConfigPtr->Port_S.PullEnable.PortPullEnable;
1478  0078 cd0000            	ldy	#__PERS
1479  007b 180a0940          	movb	9,x,0,y
1480                         ; 304     PPSS     = ConfigPtr->Port_S.Polarity.PortPolarity;
1482  007f cd0000            	ldy	#__PPSS
1483  0082 180a0a40          	movb	10,x,0,y
1484                         ; 305     WOMS     = ConfigPtr->Port_S.WiredOr.PortWiredOr;
1486  0086 cd0000            	ldy	#__WOMS
1487  0089 180a0b40          	movb	11,x,0,y
1488                         ; 308     PTM      = ConfigPtr->Port_M.Level.PortLevel;
1490  008d cd0000            	ldy	#__PTM
1491  0090 180a0c40          	movb	12,x,0,y
1492                         ; 309     DDRM     = ConfigPtr->Port_AToA2D[PORT_M].Direction.PortDirection;
1494  0094 cd0000            	ldy	#__DDRM
1495  0097 180ae02740        	movb	39,x,0,y
1496                         ; 310     RDRM     = ConfigPtr->Port_M.Drive.PortDrive;
1498  009c cd0000            	ldy	#__RDRM
1499  009f 180a0d40          	movb	13,x,0,y
1500                         ; 311     PERM     = ConfigPtr->Port_M.PullEnable.PortPullEnable;    
1502  00a3 cd0000            	ldy	#__PERM
1503  00a6 180a0e40          	movb	14,x,0,y
1504                         ; 312     PPSM     = ConfigPtr->Port_M.Polarity.PortPolarity;    
1506  00aa cd0000            	ldy	#__PPSM
1507  00ad 180a0f40          	movb	15,x,0,y
1508                         ; 313     WOMM     = ConfigPtr->Port_M.WiredOr.PortWiredOr;
1510  00b1 cd0000            	ldy	#__WOMM
1511  00b4 180ae01040        	movb	16,x,0,y
1512                         ; 316     PTP      = ConfigPtr->Port_P.Level.PortLevel;
1514  00b9 cd0000            	ldy	#__PTP
1515  00bc 180ae01140        	movb	17,x,0,y
1516                         ; 317     DDRP     = ConfigPtr->Port_AToA2D[PORT_P].Direction.PortDirection;
1518  00c1 cd0000            	ldy	#__DDRP
1519  00c4 180ae02940        	movb	41,x,0,y
1520                         ; 318     RDRP     = ConfigPtr->Port_P.Drive.PortDrive;    
1522  00c9 cd0000            	ldy	#__RDRP
1523  00cc 180ae01240        	movb	18,x,0,y
1524                         ; 319     PERP     = ConfigPtr->Port_P.PullEnable.PortPullEnable;    
1526  00d1 cd0000            	ldy	#__PERP
1527  00d4 180ae01340        	movb	19,x,0,y
1528                         ; 320     PPSP     = ConfigPtr->Port_P.Polarity.PortPolarity;
1530  00d9 cd0000            	ldy	#__PPSP
1531  00dc 180ae01440        	movb	20,x,0,y
1532                         ; 325     DDRH     = PORT_DIR_OUTPUT;
1534  00e1 7b0000            	stab	__DDRH
1535                         ; 328     PTJ      = ConfigPtr->Port_J.Level.PortLevel;
1537  00e4 cd0000            	ldy	#__PTJ
1538  00e7 180ae01540        	movb	21,x,0,y
1539                         ; 329     DDRJ     = ConfigPtr->Port_AToA2D[PORT_J].Direction.PortDirection;
1541  00ec cd0000            	ldy	#__DDRJ
1542  00ef 180ae02b40        	movb	43,x,0,y
1543                         ; 330     RDRJ     = ConfigPtr->Port_J.Drive.PortDrive;    
1545  00f4 cd0000            	ldy	#__RDRJ
1546  00f7 180ae01640        	movb	22,x,0,y
1547                         ; 331     PERJ     = ConfigPtr->Port_J.PullEnable.PortPullEnable;    
1549  00fc cd0000            	ldy	#__PERJ
1550  00ff 180ae01740        	movb	23,x,0,y
1551                         ; 332     PPSJ     = ConfigPtr->Port_J.Polarity.PortPolarity;
1553  0104 cd0000            	ldy	#__PPSJ
1554  0107 180ae01840        	movb	24,x,0,y
1555                         ; 335     PT1AD0    = ConfigPtr->Port_A2D0L.Level.PortLevel;
1557  010c cd0001            	ldy	#__PT01AD0+1
1558  010f 180ae01940        	movb	25,x,0,y
1559                         ; 336     DDR1AD0   = ConfigPtr->Port_AToA2D[PORT_A2D0L].Direction.PortDirection;
1561  0114 cd0001            	ldy	#__DDR01AD0+1
1562  0117 180ae02d40        	movb	45,x,0,y
1563                         ; 337     RDR1AD0   = ConfigPtr->Port_A2D0L.Drive.PortDrive;
1565  011c cd0001            	ldy	#__RDR01AD0+1
1566  011f 180ae01a40        	movb	26,x,0,y
1567                         ; 338     ATD0DIENL  = ConfigPtr->Port_A2D0L.DIE.PortDIE;
1569  0124 cd0000            	ldy	#__ATD0DIEN
1570  0127 180ae01c40        	movb	28,x,0,y
1571                         ; 339     PER1AD0   = ConfigPtr->Port_A2D0L.PullEnable.PortPullEnable;
1573  012c cd0001            	ldy	#__PER01AD0+1
1574  012f 180ae01b40        	movb	27,x,0,y
1575                         ; 344     DDR0AD0   = PORT_DIR_OUTPUT;
1577  0134 7b0000            	stab	__DDR01AD0
1578                         ; 350     PUCR      = ConfigPtr->Port_Control.PullEnable.PortPullEnable;
1580  0137 cd0000            	ldy	#__PUCR
1581  013a 180ae02f40        	movb	47,x,0,y
1582                         ; 351     RDRIV     = ConfigPtr->Port_Control.Drive.PortDrive;
1584  013f cd0000            	ldy	#__RDRIV
1585  0142 180ae03040        	movb	48,x,0,y
1586                         ; 352     ECLKCTL   = ConfigPtr->Port_Control.ECLK;
1588  0147 cd0000            	ldy	#__ECLKCTL
1589  014a 180ae03140        	movb	49,x,0,y
1590                         ; 353     MODRR     = ConfigPtr->Port_Control.MODR;
1592  014f cd0000            	ldy	#__MODRR
1593  0152 180ae03240        	movb	50,x,0,y
1594                         ; 354      PTTRR    = ConfigPtr->Port_Control.PTT_RR;
1596  0157 cd0000            	ldy	#__PTTRR
1597  015a 180ae03340        	movb	51,x,0,y
1598                         ; 355     IRQCR     = IRQC;
1600  015f 790000            	clr	__IRQCR
1601                         ; 358     Port_CfgPtr = (Port_ConfigType *)ConfigPtr;
1603  0162 1805800000        	movw	OFST+0,s,L5_Port_CfgPtr
1604                         ; 361 }
1607  0167 31                	puly	
1608  0168 3d                	rts	
1695                         ; 385 void Port_SetPinDirection (Port_PinType Pin, Port_PinDirectionType Direction)
1695                         ; 386 {
1696                         	switch	.text
1697  0169                   _Port_SetPinDirection:
1699  0169 3b                	pshd	
1700  016a 1b9d              	leas	-3,s
1701       00000003          OFST:	set	3
1704                         ; 396     RegIndex = Pin>>3;
1706  016c 54                	lsrb	
1707  016d 54                	lsrb	
1708  016e 54                	lsrb	
1709  016f 6b80              	stab	OFST-3,s
1710                         ; 399     BitIndex = (uint8)(Pin % PORT_REG_SIZE);
1712  0171 e684              	ldab	OFST+1,s
1713  0173 c407              	andb	#7
1714  0175 6b81              	stab	OFST-2,s
1715                         ; 402     Dir_Change=Port_CfgPtr->Port_AToA2D[RegIndex].DirChangeable.PortDirConfig;
1717  0177 fd0000            	ldy	L5_Port_CfgPtr
1718  017a e680              	ldab	OFST-3,s
1719  017c 87                	clra	
1720  017d 59                	lsld	
1721  017e 19ee              	leay	d,y
1722  0180 e6e81e            	ldab	30,y
1723  0183 6b82              	stab	OFST-1,s
1724                         ; 405     if(PORT_PIN_OUT==((Dir_Change>>BitIndex)&1))
1726  0185 b796              	exg	b,y
1727  0187 e681              	ldab	OFST-2,s
1728  0189 2705              	beq	L01
1729  018b                   L21:
1730  018b 1857              	asry	
1731  018d 0431fb            	dbne	b,L21
1732  0190                   L01:
1733  0190 b764              	tfr	y,d
1734  0192 c401              	andb	#1
1735  0194 87                	clra	
1736  0195 8c0001            	cpd	#1
1737  0198 2640              	bne	L7211
1738                         ; 408         DisableAllInterrupts();
1741  019a 1410              	sei	
1743                         ; 410         (*(Port_DDR_Reg[RegIndex])) = (uint8) 
1743                         ; 411             PORT_SET_REG_BIT((*(Port_DDR_Reg[RegIndex])),BitIndex,Direction);        
1746  019c e680              	ldab	OFST-3,s
1747  019e b746              	tfr	d,y
1748  01a0 1858              	lsly	
1749  01a2 edea0000          	ldy	L3_Port_DDR_Reg,y
1750  01a6 35                	pshy	
1751  01a7 e68a              	ldab	OFST+7,s
1752  01a9 042115            	dbne	b,L41
1753  01ac e682              	ldab	OFST-1,s
1754  01ae 59                	lsld	
1755  01af b746              	tfr	d,y
1756  01b1 c601              	ldab	#1
1757  01b3 a683              	ldaa	OFST+0,s
1758  01b5 2704              	beq	L61
1759  01b7                   L02:
1760  01b7 58                	lslb	
1761  01b8 0430fc            	dbne	a,L02
1762  01bb                   L61:
1763  01bb eaeb0000          	orab	[L3_Port_DDR_Reg,y]
1764  01bf 2014              	bra	L22
1765  01c1                   L41:
1766  01c1 e682              	ldab	OFST-1,s
1767  01c3 59                	lsld	
1768  01c4 b746              	tfr	d,y
1769  01c6 c601              	ldab	#1
1770  01c8 a683              	ldaa	OFST+0,s
1771  01ca 2704              	beq	L42
1772  01cc                   L62:
1773  01cc 58                	lslb	
1774  01cd 0430fc            	dbne	a,L62
1775  01d0                   L42:
1776  01d0 51                	comb	
1777  01d1 e4eb0000          	andb	[L3_Port_DDR_Reg,y]
1778  01d5                   L22:
1779  01d5 31                	puly	
1780  01d6 6b40              	stab	0,y
1781                         ; 413         EnableAllInterrupts();
1784  01d8 10ef              	cli	
1787  01da                   L7211:
1788                         ; 416 }
1791  01da 1b85              	leas	5,s
1792  01dc 3d                	rts	
1846                         ; 434 void Port_RefreshPortDirection(void)
1846                         ; 435 {
1847                         	switch	.text
1848  01dd                   _Port_RefreshPortDirection:
1850  01dd 1bf1ec            	leas	-20,s
1851       00000014          OFST:	set	20
1854                         ; 442     for (PortIndex =0; PortIndex<PORT_MAX_INDEX ;PortIndex++)
1856  01e0 c7                	clrb	
1857  01e1 6b81              	stab	OFST-19,s
1858  01e3                   L5511:
1859                         ; 444         Port_Dir_Change[PortIndex]=
1859                         ; 445                 Port_CfgPtr->Port_AToA2D[PortIndex].DirChangeable.PortDirConfig;
1861  01e3 fd0000            	ldy	L5_Port_CfgPtr
1862  01e6 87                	clra	
1863  01e7 59                	lsld	
1864  01e8 19ee              	leay	d,y
1865  01ea e681              	ldab	OFST-19,s
1866  01ec 87                	clra	
1867  01ed c30002            	addd	#-18+OFST
1868  01f0 180ae81ef6        	movb	30,y,d,s
1869                         ; 446         Port_Dir[PortIndex]=
1869                         ; 447                 Port_CfgPtr->Port_AToA2D[PortIndex].Direction.PortDirection;
1871  01f5 e681              	ldab	OFST-19,s
1872  01f7 87                	clra	
1873  01f8 c3000b            	addd	#-9+OFST
1874  01fb 180ae81df6        	movb	29,y,d,s
1875                         ; 453         (*(Port_DDR_Reg[PortIndex])) = 
1875                         ; 454             ((*(Port_DDR_Reg[PortIndex]))&Port_Dir_Change[PortIndex])|
1875                         ; 455             (Port_Dir[PortIndex]&(~Port_Dir_Change[PortIndex]));
1877  0200 e681              	ldab	OFST-19,s
1878  0202 87                	clra	
1879  0203 b746              	tfr	d,y
1880  0205 1858              	lsly	
1881  0207 1af6              	leax	d,s
1882  0209 c30002            	addd	#-18+OFST
1883  020c e6f6              	ldab	d,s
1884  020e 51                	comb	
1885  020f e40b              	andb	OFST-9,x
1886  0211 6b80              	stab	OFST-20,s
1887  0213 e681              	ldab	OFST-19,s
1888  0215 87                	clra	
1889  0216 b745              	tfr	d,x
1890  0218 1848              	lslx	
1891  021a c30002            	addd	#-18+OFST
1892  021d e6f6              	ldab	d,s
1893  021f e4e30000          	andb	[L3_Port_DDR_Reg,x]
1894  0223 ea80              	orab	OFST-20,s
1895  0225 6beb0000          	stab	[L3_Port_DDR_Reg,y]
1896                         ; 442     for (PortIndex =0; PortIndex<PORT_MAX_INDEX ;PortIndex++)
1898  0229 6281              	inc	OFST-19,s
1901  022b e681              	ldab	OFST-19,s
1902  022d c109              	cmpb	#9
1903  022f 25b2              	blo	L5511
1904                         ; 458 }
1907  0231 1bf014            	leas	20,s
1908  0234 3d                	rts	
1987                         ; 477 void Port_GetVersionInfo(Std_VersionInfoType *versioninfo)
1987                         ; 478 {
1988                         	switch	.text
1989  0235                   _Port_GetVersionInfo:
1991       00000000          OFST:	set	0
1994                         ; 480     if(NULL_PTR != versioninfo)
1996  0235 6cae              	std	2,-s
1997  0237 2715              	beq	L1221
1998                         ; 483         versioninfo->moduleID=PORT_MODULE_ID;
2000  0239 c67c              	ldab	#124
2001  023b ed80              	ldy	OFST+0,s
2002  023d 6b42              	stab	2,y
2003                         ; 485         versioninfo->vendorID=PORT_VENDOR_ID;
2005  023f cc0028            	ldd	#40
2006  0242 6c40              	std	0,y
2007                         ; 487         versioninfo->sw_major_version=PORT_SW_MAJOR_VERSION_C;
2009  0244 c602              	ldab	#2
2010  0246 6b43              	stab	3,y
2011                         ; 488         versioninfo->sw_minor_version=PORT_SW_MINOR_VERSION_C;
2013  0248 6944              	clr	4,y
2014                         ; 489         versioninfo->sw_patch_version=PORT_SW_PATCH_VERSION_C;
2016  024a c608              	ldab	#8
2017  024c 6b45              	stab	5,y
2018  024e                   L1221:
2019                         ; 492 }
2022  024e 31                	puly	
2023  024f 3d                	rts	
2067                         	switch	.bss
2068  0000                   L5_Port_CfgPtr:
2069  0000 0000              	ds.b	2
2070                         	xdef	_Port_GetVersionInfo
2071                         	xdef	_Port_RefreshPortDirection
2072                         	xdef	_Port_SetPinDirection
2073                         	xdef	_Port_Init
2074  0002                   _Port_PTT_Level:
2075  0002 00                	ds.b	1
2076                         	xdef	_Port_PTT_Level
2077                         	xref	__ATD0DIEN
2078                         	xref	__PER01AD0
2079                         	xref	__RDR01AD0
2080                         	xref	__DDR01AD0
2081                         	xref	__PT01AD0
2082                         	xref	__PPSJ
2083                         	xref	__PERJ
2084                         	xref	__RDRJ
2085                         	xref	__DDRJ
2086                         	xref	__PTJ
2087                         	xref	__DDRH
2088                         	xref	__PPSP
2089                         	xref	__PERP
2090                         	xref	__RDRP
2091                         	xref	__DDRP
2092                         	xref	__PTP
2093                         	xref	__MODRR
2094                         	xref	__WOMM
2095                         	xref	__PPSM
2096                         	xref	__PERM
2097                         	xref	__RDRM
2098                         	xref	__DDRM
2099                         	xref	__PTM
2100                         	xref	__WOMS
2101                         	xref	__PPSS
2102                         	xref	__PERS
2103                         	xref	__RDRS
2104                         	xref	__DDRS
2105                         	xref	__PTS
2106                         	xref	__PTTRR
2107                         	xref	__PPST
2108                         	xref	__PERT
2109                         	xref	__RDRT
2110                         	xref	__DDRT
2111                         	xref	__PTT
2112                         	xref	__DDRK
2113                         	xref	__IRQCR
2114                         	xref	__ECLKCTL
2115                         	xref	__RDRIV
2116                         	xref	__PUCR
2117                         	xref	__DDRE
2118                         	xref	__PORTE
2119                         	xref	__DDRAB
2120                         	xref	__PORTAB
2141                         	end
