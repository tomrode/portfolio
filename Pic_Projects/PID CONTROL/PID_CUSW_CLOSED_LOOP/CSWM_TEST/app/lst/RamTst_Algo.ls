   1                         ; C Compiler for S12XE [COSMIC Software].
   2                         ; Generator V4.7.8 - 27 Jul 2007
   3                         ; Optimizer V4.6.13 - 27 Jul 2007
   4                         .fdata:	section	.data
   5  0000                   _RamTst_CRC_Addr:
   6  0000 0000              	dc.w	0
 116                         .const:	section	.data
 117                         	even
 118  0000                   L61:
 119  0000 00000001          	dc.l	1
 120                         ; 162 void CheckerboardTest( void ) 
 120                         ; 163 {
 121                         	switch	.text
 122  0000                   _CheckerboardTest:
 124  0000 1b90              	leas	-16,s
 125       00000010          OFST:	set	16
 128                         ; 171     uint8 *__far Address1=0;
 130                         ; 172     uint8 *__far Address2=0;
 132                         ; 175     Variable_1=0x55;
 134  0002 c655              	ldab	#85
 135  0004 6b85              	stab	OFST-11,s
 136                         ; 176     Variable_2=0xAA;
 138  0006 c6aa              	ldab	#170
 139  0008 6b8a              	stab	OFST-6,s
 140                         ; 177     Repeat = NOT_REPEAT;
 142  000a c601              	ldab	#1
 143  000c 6b84              	stab	OFST-12,s
 144                         ; 178     Run = FIRST_RUN;
 146  000e 6b8b              	stab	OFST-5,s
 147                         ; 181     StartAddress = RamTst_PresentAddress;  
 149  0010 18018e0002        	movw	_RamTst_PresentAddress+2,OFST-2,s
 150  0015 18018c0000        	movw	_RamTst_PresentAddress,OFST-4,s
 152  001a 18200110          	bra	L35
 153  001e                   L74:
 154                         ; 187         Address1 = (uint8 *__far)RamTst_PresentAddress;
 156  001e fc0002            	ldd	_RamTst_PresentAddress+2
 157  0021 6c80              	std	OFST-16,s
 158                         ; 192         RamTst_Address = RamTst_PresentAddress;
 160  0023 fe0000            	ldx	_RamTst_PresentAddress
 161                         ; 193         RamTst_Address = (~RamTst_Address)& 0x000FFFFF;
 163  0026 51                	comb	
 164  0027 41                	coma	
 165  0028 1841              	comx	
 166  002a 1884000f          	andx	#15
 167  002e 6c88              	std	OFST-8,s
 168  0030 6e86              	stx	OFST-10,s
 169                         ; 194         Address2 = ((uint8 *__far)RamTst_Address);
 171  0032 6c82              	std	OFST-14,s
 172                         ; 197         if((Address2 >= (uint8 *__far)RAMTST_BLOCK_START_ADDRESS) && (Address2 <=\
 172                         ; 198         (uint8 *__far)RAMTST_BLOCK_END_ADDRESS)) 
 174  0034 fc000c            	ldd	_RamTst_BlockStartAddressForAlgo+2
 175  0037 ac82              	cpd	OFST-14,s
 176  0039 223e              	bhi	L75
 178  003b fc0008            	ldd	_RamTst_BlockEndAddressForAlgo+2
 179  003e ac82              	cpd	OFST-14,s
 180  0040 2537              	blo	L75
 181                         ; 201             DisableAllInterrupts();
 184  0042 1410              	sei	
 186                         ; 204             RamTst_DataSave1 = (uint8)*Address1;       
 189  0044 ed80              	ldy	OFST-16,s
 190  0046 180d400001        	movb	0,y,_RamTst_DataSave1
 191                         ; 206             RamTst_DataSave2 = (uint8)*Address2;        
 193  004b ee82              	ldx	OFST-14,s
 194  004d 180d000000        	movb	0,x,_RamTst_DataSave2
 195                         ; 209             *Address1 = Variable_1;
 197  0052 180a8540          	movb	OFST-11,s,0,y
 198                         ; 212             *Address2 = Variable_2;
 200  0056 e68a              	ldab	OFST-6,s
 201  0058 6b00              	stab	0,x
 202                         ; 215             if(Variable_2 != *Address2) 
 204  005a e100              	cmpb	0,x
 205  005c 2712              	beq	L16
 206                         ; 218                 EnableAllInterrupts();
 209  005e 10ef              	cli	
 211                         ; 221                 RamTst_Result[PRESENT_BLOCK_ID] = RAMTST_RESULT_NOT_OK;
 214  0060 c602              	ldab	#2
 215  0062 fd0002            	ldy	_RamTst_Result
 216  0065 18fb0004          	addy	_RamTst_BlockIDForAlgo
 217  0069 6b40              	stab	0,y
 218                         ; 224                 *Address2 = RamTst_DataSave2;
 220  006b f60000            	ldab	_RamTst_DataSave2
 221                         ; 226                 return;
 223  006e 202c              	bra	LC001
 224  0070                   L16:
 225                         ; 229             *Address2 = RamTst_DataSave2;
 227  0070 f60000            	ldab	_RamTst_DataSave2
 228  0073 6b00              	stab	0,x
 230  0075 e685              	ldab	OFST-11,s
 231  0077 200d              	bra	L36
 232  0079                   L75:
 233                         ; 234             DisableAllInterrupts();
 236  0079 1410              	sei	
 238                         ; 237             RamTst_DataSave1 = (uint8)*Address1;       
 241  007b ed80              	ldy	OFST-16,s
 242  007d 180d400001        	movb	0,y,_RamTst_DataSave1
 243                         ; 240             *Address1 = Variable_1;
 245  0082 e685              	ldab	OFST-11,s
 246  0084 6b40              	stab	0,y
 247  0086                   L36:
 248                         ; 244         if(Variable_1 != *Address1) 
 250  0086 b765              	tfr	y,x
 251  0088 e100              	cmpb	0,x
 252  008a 2716              	beq	L56
 253                         ; 247             EnableAllInterrupts();
 256  008c 10ef              	cli	
 258                         ; 250             RamTst_Result[PRESENT_BLOCK_ID] = RAMTST_RESULT_NOT_OK;
 261  008e c602              	ldab	#2
 262  0090 fd0002            	ldy	_RamTst_Result
 263  0093 18fb0004          	addy	_RamTst_BlockIDForAlgo
 264  0097 6b40              	stab	0,y
 265                         ; 253             *Address1 = RamTst_DataSave1;
 267  0099 f60001            	ldab	_RamTst_DataSave1
 268  009c                   LC001:
 269  009c 6b00              	stab	0,x
 270                         ; 255             return;
 271  009e                   L02:
 274  009e 1bf010            	leas	16,s
 275  00a1 3d                	rts	
 276  00a2                   L56:
 277                         ; 258         *Address1 = RamTst_DataSave1;
 279  00a2 f60001            	ldab	_RamTst_DataSave1
 280  00a5 6b00              	stab	0,x
 281                         ; 261         EnableAllInterrupts();
 284  00a7 10ef              	cli	
 286                         ; 264         if(Run == FIRST_RUN) 
 289  00a9 e68b              	ldab	OFST-5,s
 290  00ab 04213d            	dbne	b,L76
 291                         ; 267             ++RamTst_PresentAddress;
 293  00ae 18720002          	incw	_RamTst_PresentAddress+2
 294  00b2 2604              	bne	L6
 295  00b4 18720000          	incw	_RamTst_PresentAddress
 296  00b8                   L6:
 297                         ; 269             ++RamTst_TestedCellCounter;
 299  00b8 18720000          	incw	_RamTst_TestedCellCounter
 300                         ; 273             if (!((RamTst_PresentAddress <=RAMTST_BLOCK_END_ADDRESS) &&\
 300                         ; 274             (RamTst_TestedCellCounter <= RamTst_PresentNumberOfTestedCell-1))) 
 302  00bc fc0002            	ldd	_RamTst_PresentAddress+2
 303  00bf fe0000            	ldx	_RamTst_PresentAddress
 304  00c2 bc0008            	cpd	_RamTst_BlockEndAddressForAlgo+2
 305  00c5 18be0006          	cpex	_RamTst_BlockEndAddressForAlgo
 306  00c9 2209              	bhi	L37
 308  00cb fd0000            	ldy	_RamTst_PresentNumberOfTestedCell
 309  00ce 03                	dey	
 310  00cf bd0000            	cpy	_RamTst_TestedCellCounter
 311  00d2 245a              	bhs	L35
 312  00d4                   L37:
 313                         ; 277                 Run = SECOND_RUN;
 315  00d4 c602              	ldab	#2
 316  00d6 6b8b              	stab	OFST-5,s
 317                         ; 280                 RamTst_TestedCellCounter = NO_CONTENT;
 319  00d8 18790000          	clrw	_RamTst_TestedCellCounter
 320                         ; 283                 --RamTst_PresentAddress;
 322  00dc fc0002            	ldd	_RamTst_PresentAddress+2
 323  00df 2604              	bne	L01
 324  00e1 09                	dex	
 325  00e2 7e0000            	stx	_RamTst_PresentAddress
 326  00e5                   L01:
 327  00e5 18730002          	decw	_RamTst_PresentAddress+2
 328  00e9 2043              	bra	L35
 329  00eb                   L76:
 330                         ; 290             --RamTst_PresentAddress;
 332  00eb fc0002            	ldd	_RamTst_PresentAddress+2
 333  00ee 2604              	bne	L21
 334  00f0 18730000          	decw	_RamTst_PresentAddress
 335  00f4                   L21:
 336  00f4 18730002          	decw	_RamTst_PresentAddress+2
 337                         ; 293             if(RamTst_PresentAddress < StartAddress) 
 339  00f8 fc0002            	ldd	_RamTst_PresentAddress+2
 340  00fb ac8e              	cpd	OFST-2,s
 341  00fd fe0000            	ldx	_RamTst_PresentAddress
 342  0100 18ae8c            	cpex	OFST-4,s
 343  0103 2429              	bhs	L35
 344                         ; 296                 if(Repeat == NOT_REPEAT) 
 346  0105 e684              	ldab	OFST-12,s
 347  0107 c101              	cmpb	#1
 348  0109 261b              	bne	L101
 349                         ; 299                     ++RamTst_PresentAddress;
 351  010b 18720002          	incw	_RamTst_PresentAddress+2
 352  010f 2604              	bne	L41
 353  0111 08                	inx	
 354  0112 7e0000            	stx	_RamTst_PresentAddress
 355  0115                   L41:
 356                         ; 301                     Variable_1=0xAA;
 358  0115 c6aa              	ldab	#170
 359  0117 6b85              	stab	OFST-11,s
 360                         ; 303                     Variable_2=0x55;
 362  0119 c655              	ldab	#85
 363  011b 6b8a              	stab	OFST-6,s
 364                         ; 305                     Repeat = REPEAT;
 366  011d c602              	ldab	#2
 367  011f 6b84              	stab	OFST-12,s
 368                         ; 307                     Run = FIRST_RUN;
 370  0121 53                	decb	
 371  0122 6b8b              	stab	OFST-5,s
 373  0124 2008              	bra	L35
 374  0126                   L101:
 375                         ; 309                 else if (Repeat == REPEAT)
 377  0126 c102              	cmpb	#2
 378  0128 2604              	bne	L35
 379                         ; 312                     Repeat = COMPLETE;
 381  012a c603              	ldab	#3
 382  012c 6b84              	stab	OFST-12,s
 383  012e                   L35:
 384                         ; 183     while(COMPLETE != Repeat) 
 386  012e e684              	ldab	OFST-12,s
 387  0130 c103              	cmpb	#3
 388  0132 1826fee8          	bne	L74
 389                         ; 318     RamTst_PresentAddress= (StartAddress+ RamTst_PresentNumberOfTestedCell-1);
 391  0136 fc0000            	ldd	_RamTst_PresentNumberOfTestedCell
 392  0139 1887              	clrx	
 393  013b e38e              	addd	OFST-2,s
 394  013d 18a98c            	adex	OFST-4,s
 395  0140 b30002            	subd	L61+2
 396  0143 18b20000          	sbex	L61
 397  0147 7c0002            	std	_RamTst_PresentAddress+2
 398  014a 7e0000            	stx	_RamTst_PresentAddress
 399                         ; 319     return;
 401  014d 1820ff4d          	bra	L02
 403                         .safe_ram:	section		.bss
 404  0000                   _RamTst_DataSave2:
 405  0000 00                	ds.b	1
 406  0001                   _RamTst_DataSave1:
 407  0001 00                	ds.b	1
 408  0002                   _RamTst_Result:
 409  0002 0000              	ds.b	2
 410  0004                   _RamTst_BlockIDForAlgo:
 411  0004 0000              	ds.b	2
 412  0006                   _RamTst_BlockEndAddressForAlgo:
 413  0006 00000000          	ds.b	4
 414  000a                   _RamTst_BlockStartAddressForAlgo:
 415  000a 00000000          	ds.b	4
 529                         	xdef	_RamTst_CRC_Addr
 530                         	xdef	_CheckerboardTest
 531                         	xdef	_RamTst_DataSave2
 532                         	xdef	_RamTst_DataSave1
 533                         	xdef	_RamTst_Result
 534                         	xref	_RamTst_TestedCellCounter
 535                         	xref	_RamTst_PresentAddress
 536                         	xref	_RamTst_PresentNumberOfTestedCell
 537                         	xdef	_RamTst_BlockIDForAlgo
 538                         	xdef	_RamTst_BlockEndAddressForAlgo
 539                         	xdef	_RamTst_BlockStartAddressForAlgo
 559                         	end
