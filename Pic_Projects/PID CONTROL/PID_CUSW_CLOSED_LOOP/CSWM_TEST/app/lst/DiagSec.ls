   1                         ; C Compiler for S12XE [COSMIC Software].
   2                         ; Generator V4.7.8 - 27 Jul 2007
   3                         ; Optimizer V4.6.13 - 27 Jul 2007
 326                         ; 113 @far void diagSecF_Main_Init(void)
 326                         ; 114 {
 327                         .ftext:	section	.text
 328  0000                   f_diagSecF_Main_Init:
 332                         ; 117 	diag_sec_FAA_c = 0;
 334  0000 790009            	clr	L12_diag_sec_FAA_c
 335                         ; 126 		diag_sec_access1_state_c = ECU_LOCKED_SEC_DELAY_TIMER_INACTIVE;
 337  0003 c601              	ldab	#1
 338  0005 7b0025            	stab	_diag_sec_access1_state_c
 339                         ; 129 		diag_sec_access5_state_c = ECU_LOCKED_SEC_DELAY_TIMER_INACTIVE;
 341  0008 7b0024            	stab	_diag_sec_access5_state_c
 342                         ; 132 		delay_timer_active_b = FALSE;
 344  000b 1d002608          	bclr	_diag_flags_c,8
 345                         ; 146 }	
 348  000f 0a                	rtc	
 374                         ; 155 @far void diagSecF_DefSession_Init(void)
 374                         ; 156 {
 375                         	switch	.ftext
 376  0010                   f_diagSecF_DefSession_Init:
 380                         ; 161 		diag_sec_access1_state_c = ECU_LOCKED_SEC_DELAY_TIMER_INACTIVE;
 382  0010 c601              	ldab	#1
 383  0012 7b0025            	stab	_diag_sec_access1_state_c
 384                         ; 163 		diag_sec_access5_state_c = ECU_LOCKED_SEC_DELAY_TIMER_INACTIVE;
 386  0015 7b0024            	stab	_diag_sec_access5_state_c
 387                         ; 188 	diag_10sec_timer_w = 0x0000; 
 389  0018 18790012          	clrw	L31_diag_10sec_timer_w
 390                         ; 189 }
 393  001c 0a                	rtc	
 419                         ; 198 @far void diagSecF_ExtSession_Init(void)
 419                         ; 199 {
 420                         	switch	.ftext
 421  001d                   f_diagSecF_ExtSession_Init:
 425                         ; 206 		diag_sec_access1_state_c = ECU_LOCKED_SEC_DELAY_TIMER_INACTIVE;
 427  001d c601              	ldab	#1
 428  001f 7b0025            	stab	_diag_sec_access1_state_c
 429                         ; 208 		diag_sec_access5_state_c = ECU_LOCKED_SEC_DELAY_TIMER_INACTIVE;
 431  0022 7b0024            	stab	_diag_sec_access5_state_c
 432                         ; 233 	diag_10sec_timer_w = 0x0000; 
 434  0025 18790012          	clrw	L31_diag_10sec_timer_w
 435                         ; 234 }
 438  0029 0a                	rtc	
 463                         ; 243 @far void diagSecF_CyclicTask(void)
 463                         ; 244 {
 464                         	switch	.ftext
 465  002a                   f_diagSecF_CyclicTask:
 469                         ; 247 	diagSecLF_Calculate_Seed();
 471  002a 4a003333          	call	f_diagSecLF_Calculate_Seed
 473                         ; 251 	diagSecLF_Start_10sec_delay_Timer();
 475  002e 4a007676          	call	f_diagSecLF_Start_10sec_delay_Timer
 477                         ; 253 }	
 480  0032 0a                	rtc	
 522                         ; 262 void diagSecLF_Calculate_Seed(void)
 522                         ; 263 {
 523                         	switch	.ftext
 524  0033                   f_diagSecLF_Calculate_Seed:
 526  0033 1b98              	leas	-8,s
 527       00000008          OFST:	set	8
 530                         ; 268 	if (seed_generated_b)
 532  0035 1f00261006        	brclr	_diag_flags_c,16,L772
 533                         ; 270 		seed_generated_b = 1;
 535  003a 1c002610          	bset	_diag_flags_c,16
 537  003e 2033              	bra	L103
 538  0040                   L772:
 539                         ; 276 		seed_x_l = _TCNT.Word;
 541  0040 fc0000            	ldd	__TCNT
 542  0043 6c82              	std	OFST-6,s
 543  0045 186980            	clrw	OFST-8,s
 544                         ; 277 		seed_y_l = _TCNT.Word;
 546  0048 fc0000            	ldd	__TCNT
 547  004b 6c86              	std	OFST-2,s
 548  004d 186984            	clrw	OFST-4,s
 549                         ; 279 		diag_seed_l.l = seed_x_l;
 551  0050 ec82              	ldd	OFST-6,s
 552  0052 7c0022            	std	L3_diag_seed_l+2
 553                         ; 281 		diag_seed_l.l = diag_seed_l.l << 16;
 555  0055 7c0020            	std	L3_diag_seed_l
 556  0058 18790022          	clrw	L3_diag_seed_l+2
 557                         ; 282 		diag_seed_l.l |= seed_x_l * seed_y_l;
 559  005c ee80              	ldx	OFST-8,s
 560  005e 1984              	leay	OFST-4,s
 561  0060 160000            	jsr	c_lmul
 563  0063 ba0022            	oraa	L3_diag_seed_l+2
 564  0066 fa0023            	orab	L3_diag_seed_l+3
 565  0069 18ba0020          	orx	L3_diag_seed_l
 566  006d 7c0022            	std	L3_diag_seed_l+2
 567  0070 7e0020            	stx	L3_diag_seed_l
 568  0073                   L103:
 569                         ; 284 }
 572  0073 1b88              	leas	8,s
 573  0075 0a                	rtc	
 600                         ; 334 void diagSecLF_Start_10sec_delay_Timer(void)
 600                         ; 335 {
 601                         	switch	.ftext
 602  0076                   f_diagSecLF_Start_10sec_delay_Timer:
 606                         ; 338 	if (delay_timer_active_b)
 608  0076 1f0026081d        	brclr	_diag_flags_c,8,L123
 609                         ; 340 		if (diag_10sec_timer_w > DELAY_TIMER_10SEC)
 611  007b fc0012            	ldd	L31_diag_10sec_timer_w
 612  007e 8c03e8            	cpd	#1000
 613  0081 2f11              	ble	L513
 614                         ; 342 			diag_10sec_timer_w = 0;
 616  0083 18790012          	clrw	L31_diag_10sec_timer_w
 617                         ; 343 			delay_timer_active_b = FALSE;
 619  0087 1d002608          	bclr	_diag_flags_c,8
 620                         ; 345 			diag_sec_access1_state_c = ECU_LOCKED_SEC_DELAY_TIMER_INACTIVE;
 622  008b c601              	ldab	#1
 623  008d 7b0025            	stab	_diag_sec_access1_state_c
 624                         ; 346 			diag_sec_access5_state_c = ECU_LOCKED_SEC_DELAY_TIMER_INACTIVE;
 626  0090 7b0024            	stab	_diag_sec_access5_state_c
 629  0093 0a                	rtc	
 630  0094                   L513:
 631                         ; 350 			diag_10sec_timer_w++;
 633  0094 18720012          	incw	L31_diag_10sec_timer_w
 634  0098                   L123:
 635                         ; 356 }
 638  0098 0a                	rtc	
 889                         ; 358 void diagSecF_RequestSeed( DescMsgContext* pMsgContext)
 889                         ; 359 {
 890                         	switch	.ftext
 891  0099                   f_diagSecF_RequestSeed:
 893  0099 3b                	pshd	
 894       00000000          OFST:	set	0
 897                         ; 363 	if (diag_sec_access1_state_c != ECU_LOCKED_SEED_GENERATED)
 899  009a f60025            	ldab	_diag_sec_access1_state_c
 900  009d c103              	cmpb	#3
 901  009f 2773              	beq	L564
 902                         ; 366 		    if (diag_sec_access5_state_c != ECU_LOCKED_SEC_DELAY_TIMER_ACTIVE)
 904  00a1 f60024            	ldab	_diag_sec_access5_state_c
 905  00a4 c102              	cmpb	#2
 906  00a6 2762              	beq	L764
 907                         ; 368 			         if (diag_sec_access5_state_c == ECU_UNLOCKED)
 909  00a8 c105              	cmpb	#5
 910  00aa 2617              	bne	L174
 911                         ; 372 				            pMsgContext->resData[0] = 0x00;
 913  00ac ed80              	ldy	OFST+0,s
 914  00ae c7                	clrb	
 915  00af 6beb0004          	stab	[4,y]
 916                         ; 373 				            pMsgContext->resData[1] = 0x00;
 918  00b3 ed44              	ldy	4,y
 919  00b5 87                	clra	
 920  00b6 6a41              	staa	1,y
 921                         ; 374 				            pMsgContext->resData[2] = 0x00;
 923                         ; 375 				            pMsgContext->resData[3] = 0x00;
 925  00b8 6c42              	std	2,y
 926                         ; 377 				            DataLength = 4;
 928  00ba c604              	ldab	#4
 929  00bc 7c0001            	std	L702_DataLength
 930                         ; 378 				            Response = RESPONSE_OK;
 932  00bf c601              	ldab	#1
 934  00c1 2059              	bra	L305
 935  00c3                   L174:
 936                         ; 382 				            if (diag_sec_access5_state_c == ECU_LOCKED_SEED_GENERATED)
 938  00c3 c103              	cmpb	#3
 939  00c5 2619              	bne	L574
 940                         ; 386                 					memcpy((unsigned char*)(pMsgContext->resData), (unsigned char*)&diag_seed_generated_l, 4);
 942  00c7 ed80              	ldy	OFST+0,s
 943  00c9 ed44              	ldy	4,y
 944  00cb ce001c            	ldx	#L5_diag_seed_generated_l
 945  00ce 18023171          	movw	2,x+,2,y+
 946  00d2 18020040          	movw	0,x,0,y
 947                         ; 387                 					DataLength = 4;
 949  00d6 cc0004            	ldd	#4
 950  00d9 7c0001            	std	L702_DataLength
 951                         ; 388                 					Response = RESPONSE_OK;
 953  00dc c601              	ldab	#1
 955  00de 203c              	bra	L305
 956  00e0                   L574:
 957                         ; 397                 					memcpy((unsigned char*)(pMsgContext->resData), (unsigned char*)&diag_seed_l, 4);
 959  00e0 ed80              	ldy	OFST+0,s
 960  00e2 ed44              	ldy	4,y
 961  00e4 ce0020            	ldx	#L3_diag_seed_l
 962  00e7 18023171          	movw	2,x+,2,y+
 963  00eb 18020040          	movw	0,x,0,y
 964                         ; 399                 					diag_seed_generated_l = diag_seed_l;
 966  00ef 18040020001c      	movw	L3_diag_seed_l,L5_diag_seed_generated_l
 967  00f5 18040022001e      	movw	L3_diag_seed_l+2,L5_diag_seed_generated_l+2
 968                         ; 401                 					diag_sec_access5_state_c = ECU_LOCKED_SEED_GENERATED;
 970  00fb c603              	ldab	#3
 971  00fd 7b0024            	stab	_diag_sec_access5_state_c
 972                         ; 402                 					DataLength = 4;
 974  0100 cc0004            	ldd	#4
 975  0103 7c0001            	std	L702_DataLength
 976                         ; 403                 					Response = RESPONSE_OK;
 978  0106 c601              	ldab	#1
 979  0108 2012              	bra	L305
 980  010a                   L764:
 981                         ; 411 		    	DataLength = 1;
 983  010a cc0001            	ldd	#1
 984  010d 7c0001            	std	L702_DataLength
 985                         ; 412 		    	Response = TIMEDELAY_NOT_EXPIRED;
 987  0110 c60b              	ldab	#11
 988  0112 2008              	bra	L305
 989  0114                   L564:
 990                         ; 419 		DataLength = 1;
 992  0114 cc0001            	ldd	#1
 993  0117 7c0001            	std	L702_DataLength
 994                         ; 420 		Response = REQUEST_SEQUENCE_ERROR;
 996  011a c60c              	ldab	#12
 997  011c                   L305:
 998  011c 7b0000            	stab	L112_Response
 999                         ; 424 }
1002  011f 31                	puly	
1003  0120 0a                	rtc	
1054                         ; 426 void diagSecF_ProcessKey(DescMsgContext* pMsgContext)
1054                         ; 427 {
1055                         	switch	.ftext
1056  0121                   f_diagSecF_ProcessKey:
1058  0121 3b                	pshd	
1059       00000000          OFST:	set	0
1062                         ; 431 	if (diag_sec_access1_state_c != ECU_LOCKED_SEED_GENERATED)
1064  0122 f60025            	ldab	_diag_sec_access1_state_c
1065  0125 c103              	cmpb	#3
1066  0127 182700a7          	beq	L345
1067                         ; 433 		if (diag_sec_access5_state_c == ECU_LOCKED_SEED_GENERATED )
1069  012b f60024            	ldab	_diag_sec_access5_state_c
1070  012e c103              	cmpb	#3
1071  0130 18260090          	bne	L725
1072                         ; 447 			   	secMod1_var1_l = SEC_MOD1_CONST1;	/* Initiliaze SecMod1 variable1 */
1074  0134 cce84f            	ldd	#-6065
1075  0137 7c0010            	std	L51_secMod1_var1_l+2
1076  013a cc6c8f            	ldd	#27791
1077  013d 7c000e            	std	L51_secMod1_var1_l
1078                         ; 448 			   	secMod1_var2_l = SEC_MOD1_CONST2;   /* Initiliaze SecMod1 variable2 */
1080  0140 ccd3be            	ldd	#-11330
1081  0143 7c000c            	std	L71_secMod1_var2_l+2
1082  0146 cc9c30            	ldd	#-25552
1083  0149 7c000a            	std	L71_secMod1_var2_l
1084                         ; 450 			   	hsm_key_l.l = diagSecLF_Key_alg(diag_seed_generated_l.l, secMod1_var1_l, secMod1_var2_l);
1086  014c ccd3be            	ldd	#-11330
1087  014f 3b                	pshd	
1088  0150 cc9c30            	ldd	#-25552
1089  0153 3b                	pshd	
1090  0154 cce84f            	ldd	#-6065
1091  0157 3b                	pshd	
1092  0158 cc6c8f            	ldd	#27791
1093  015b 3b                	pshd	
1094  015c fc001e            	ldd	L5_diag_seed_generated_l+2
1095  015f fe001c            	ldx	L5_diag_seed_generated_l
1096  0162 4a01dfdf          	call	f_diagSecLF_Key_alg
1098  0166 1b88              	leas	8,s
1099  0168 7c0016            	std	L11_hsm_key_l+2
1100  016b 7e0014            	stx	L11_hsm_key_l
1101                         ; 452 				memcpy((unsigned char *)&tool_key_l, &(pMsgContext->reqData[0]), 4);
1103  016e cd0018            	ldy	#L7_tool_key_l
1104  0171 eef30000          	ldx	[OFST+0,s]
1105  0175 18023171          	movw	2,x+,2,y+
1106  0179 18020040          	movw	0,x,0,y
1107                         ; 453 				if (hsm_key_l.l == tool_key_l.l)
1109  017d fc0014            	ldd	L11_hsm_key_l
1110  0180 bc0018            	cpd	L7_tool_key_l
1111  0183 261f              	bne	L135
1112  0185 fc0016            	ldd	L11_hsm_key_l+2
1113  0188 bc001a            	cpd	L7_tool_key_l+2
1114  018b 2617              	bne	L135
1115                         ; 458 					DataLength = 0;
1117  018d 18790001          	clrw	L702_DataLength
1118                         ; 459 					RespLengthDefault = 1;
1120  0191 cc0001            	ldd	#1
1121  0194 7c0003            	std	L502_RespLengthDefault
1122                         ; 462 					Response = RESPONSE_OK;
1124  0197 7b0000            	stab	L112_Response
1125                         ; 464 					diag_sec_access5_state_c = ECU_UNLOCKED;
1127  019a c605              	ldab	#5
1128  019c 7b0024            	stab	_diag_sec_access5_state_c
1129                         ; 466 					diag_sec_FAA_c = 0;
1131  019f 790009            	clr	L12_diag_sec_FAA_c
1133  01a2 2039              	bra	L745
1134  01a4                   L135:
1135                         ; 472 					DataLength = 1;
1137  01a4 cc0001            	ldd	#1
1138  01a7 7c0001            	std	L702_DataLength
1139                         ; 473 					Response = INVALID_KEY;
1141  01aa c609              	ldab	#9
1142  01ac 7b0000            	stab	L112_Response
1143                         ; 475 					diag_sec_FAA_c++;
1145  01af 720009            	inc	L12_diag_sec_FAA_c
1146                         ; 478 					if(diag_sec_FAA_c >= 2)
1148  01b2 f60009            	ldab	L12_diag_sec_FAA_c
1149  01b5 c102              	cmpb	#2
1150  01b7 2524              	blo	L745
1151                         ; 481 						diag_sec_access5_state_c = ECU_LOCKED_SEC_DELAY_TIMER_ACTIVE;
1153  01b9 c602              	ldab	#2
1154  01bb 7b0024            	stab	_diag_sec_access5_state_c
1155                         ; 485 						delay_timer_active_b = TRUE;
1157  01be 1c002608          	bset	_diag_flags_c,8
1159  01c2 2019              	bra	L745
1160  01c4                   L725:
1161                         ; 513 			if (diag_sec_access5_state_c == ECU_LOCKED_SEC_DELAY_TIMER_ACTIVE)
1163  01c4 c102              	cmpb	#2
1164  01c6 260a              	bne	L345
1165                         ; 517 				DataLength = 1;
1167  01c8 cc0001            	ldd	#1
1168  01cb 7c0001            	std	L702_DataLength
1169                         ; 518 				Response = TIMEDELAY_NOT_EXPIRED;
1171  01ce c60b              	ldab	#11
1173  01d0 2008              	bra	LC001
1174  01d2                   L345:
1175                         ; 524 				DataLength = 1;
1177                         ; 525 				Response = REQUEST_SEQUENCE_ERROR;
1179                         ; 533 		DataLength = 1;
1181                         ; 534 		Response = REQUEST_SEQUENCE_ERROR;
1183  01d2 cc0001            	ldd	#1
1184  01d5 7c0001            	std	L702_DataLength
1185  01d8 c60c              	ldab	#12
1186  01da                   LC001:
1187  01da 7b0000            	stab	L112_Response
1188  01dd                   L745:
1189                         ; 538 }
1192  01dd 31                	puly	
1193  01de 0a                	rtc	
1245                         ; 551 unsigned long diagSecLF_Key_alg(unsigned long Seed, unsigned long Constant_1, unsigned long Constant_2)
1245                         ; 552 {
1246                         	switch	.ftext
1247  01df                   f_diagSecLF_Key_alg:
1249  01df 3b                	pshd	
1250  01e0 34                	pshx	
1251  01e1 1b90              	leas	-16,s
1252       00000010          OFST:	set	16
1255                         ; 558 	Key = ((Seed << 8) & 0xFF000000) + ((Seed >> 8) & 0x00FF0000) +
1255                         ; 559 		  ((Seed << 8) & 0x0000FF00) + ((Seed >> 8) & 0x000000FF);
1257  01e3 e6f012            	ldab	OFST+2,s
1258  01e6 87                	clra	
1259  01e7 1887              	clrx	
1260  01e9 6c8a              	std	OFST-6,s
1261  01eb 6e88              	stx	OFST-8,s
1262  01ed ecf012            	ldd	OFST+2,s
1263  01f0 36                	psha	
1264  01f1 b710              	tfr	b,a
1265  01f3 c7                	clrb	
1266  01f4 b745              	tfr	d,x
1267  01f6 33                	pulb	
1268  01f7 b754              	tfr	x,d
1269  01f9 c7                	clrb	
1270  01fa 1887              	clrx	
1271  01fc 6c86              	std	OFST-10,s
1272  01fe 6e84              	stx	OFST-12,s
1273  0200 e6f010            	ldab	OFST+0,s
1274  0203 87                	clra	
1275  0204 b745              	tfr	d,x
1276  0206 188400ff          	andx	#255
1277  020a c7                	clrb	
1278  020b 6c82              	std	OFST-14,s
1279  020d 6e80              	stx	OFST-16,s
1280  020f a6f012            	ldaa	OFST+2,s
1281  0212 eef010            	ldx	OFST+0,s
1282  0215 36                	psha	
1283  0216 b754              	tfr	x,d
1284  0218 b710              	tfr	b,a
1285  021a 33                	pulb	
1286  021b b745              	tfr	d,x
1287  021d 1884ff00          	andx	#-256
1288  0221 87                	clra	
1289  0222 c7                	clrb	
1290  0223 e382              	addd	OFST-14,s
1291  0225 18a980            	adex	OFST-16,s
1292  0228 e386              	addd	OFST-10,s
1293  022a 18a984            	adex	OFST-12,s
1294  022d e38a              	addd	OFST-6,s
1295  022f 18a988            	adex	OFST-8,s
1296  0232 6c8e              	std	OFST-2,s
1297  0234 6e8c              	stx	OFST-4,s
1298                         ; 562 	Key = ((Key << 11) + (Key >> 22)) & 0xFFFFFFFF;
1300  0236 b754              	tfr	x,d
1301  0238 1887              	clrx	
1302  023a 49                	lsrd	
1303  023b 49                	lsrd	
1304  023c 49                	lsrd	
1305  023d 49                	lsrd	
1306  023e 49                	lsrd	
1307  023f 49                	lsrd	
1308  0240 6c8a              	std	OFST-6,s
1309  0242 6e88              	stx	OFST-8,s
1310  0244 ec8e              	ldd	OFST-2,s
1311  0246 ee8c              	ldx	OFST-4,s
1312  0248 cd000b            	ldy	#11
1313  024b                   L23:
1314  024b 59                	lsld	
1315  024c 1845              	rolx	
1316  024e 0436fa            	dbne	y,L23
1317  0251 e38a              	addd	OFST-6,s
1318  0253 18a988            	adex	OFST-8,s
1319  0256 6c8e              	std	OFST-2,s
1320                         ; 565 	Constant_2 = (Constant_2 & Seed) & 0xFFFFFFFF;
1322  0258 ecf01b            	ldd	OFST+11,s
1323  025b a4f010            	anda	OFST+0,s
1324  025e e4f011            	andb	OFST+1,s
1325  0261 6cf01b            	std	OFST+11,s
1326  0264 ecf01d            	ldd	OFST+13,s
1327  0267 a4f012            	anda	OFST+2,s
1328  026a e4f013            	andb	OFST+3,s
1329  026d 6cf01d            	std	OFST+13,s
1330                         ; 568     Key = (Key ^ Constant_1 ^ Constant_2) & 0xFFFFFFFF;
1332  0270 ec8e              	ldd	OFST-2,s
1333  0272 a8f019            	eora	OFST+9,s
1334  0275 e8f01a            	eorb	OFST+10,s
1335  0278 18a8f017          	eorx	OFST+7,s
1336  027c a8f01d            	eora	OFST+13,s
1337  027f e8f01e            	eorb	OFST+14,s
1338  0282 18a8f01b          	eorx	OFST+11,s
1339                         ; 571 	return Key;
1343  0286 1bf014            	leas	20,s
1344  0289 0a                	rtc	
1383                         ; 590 void DESC_API_CALLBACK_TYPE ApplDescOnTransitionSecurityAccess(DescStateGroup newState, DescStateGroup formerState)
1383                         ; 591 {
1384                         	switch	.ftext
1385  028a                   f_ApplDescOnTransitionSecurityAccess:
1387       fffffffe          OFST:	set	-2
1390                         ; 597   DESC_IGNORE_UNREF_PARAM(newState);
1392                         ; 598   DESC_IGNORE_UNREF_PARAM(formerState);
1394                         ; 600 }
1397  028a 0a                	rtc	
1439                         ; 606 void DESC_API_CALLBACK_TYPE ApplDescRequestLevel1Seed(DescMsgContext* pMsgContext)
1439                         ; 607 {
1440                         	switch	.ftext
1441  028b                   f_ApplDescRequestLevel1Seed:
1443  028b 3b                	pshd	
1444       00000000          OFST:	set	0
1447                         ; 610   RespLengthDefault = SIZE_OF_BLOCKNUMBER_K;
1449  028c cc0002            	ldd	#2
1450  028f 7c0003            	std	L502_RespLengthDefault
1451                         ; 613 	if (pMsgContext->reqDataLen == 1)
1453  0292 ed80              	ldy	OFST+0,s
1454  0294 ec42              	ldd	2,y
1455  0296 042408            	dbne	d,L136
1456                         ; 615       diagSecF_RequestSeed (pMsgContext);
1458  0299 b764              	tfr	y,d
1459  029b 4a009999          	call	f_diagSecF_RequestSeed
1462  029f 2005              	bra	L336
1463  02a1                   L136:
1464                         ; 619       Response = LENGTH_INVALID_FORMAT; 
1466  02a1 c605              	ldab	#5
1467  02a3 7b0000            	stab	L112_Response
1468  02a6                   L336:
1469                         ; 622   diagF_DiagResponse(Response, DataLength+RespLengthDefault, pMsgContext);      		
1471  02a6 ec80              	ldd	OFST+0,s
1472  02a8 3b                	pshd	
1473  02a9 fc0001            	ldd	L702_DataLength
1474  02ac f30003            	addd	L502_RespLengthDefault
1475  02af 3b                	pshd	
1476  02b0 f60000            	ldab	L112_Response
1477  02b3 87                	clra	
1478  02b4 4a000000          	call	f_diagF_DiagResponse
1480  02b8 1b86              	leas	6,s
1481                         ; 623 }
1484  02ba 0a                	rtc	
1526                         ; 629 void DESC_API_CALLBACK_TYPE ApplDescSendLevel1Key(DescMsgContext* pMsgContext)
1526                         ; 630 {
1527                         	switch	.ftext
1528  02bb                   f_ApplDescSendLevel1Key:
1530  02bb 3b                	pshd	
1531       00000000          OFST:	set	0
1534                         ; 633   RespLengthDefault = SIZE_OF_BLOCKNUMBER_K;
1536  02bc cc0002            	ldd	#2
1537  02bf 7c0003            	std	L502_RespLengthDefault
1538                         ; 636 	if (pMsgContext->reqDataLen == 5)
1540  02c2 ed80              	ldy	OFST+0,s
1541  02c4 ec42              	ldd	2,y
1542  02c6 8c0005            	cpd	#5
1543  02c9 2608              	bne	L556
1544                         ; 638       diagSecF_ProcessKey (pMsgContext);  
1546  02cb b764              	tfr	y,d
1547  02cd 4a012121          	call	f_diagSecF_ProcessKey
1550  02d1 2005              	bra	L756
1551  02d3                   L556:
1552                         ; 642       Response = LENGTH_INVALID_FORMAT; 
1554  02d3 c605              	ldab	#5
1555  02d5 7b0000            	stab	L112_Response
1556  02d8                   L756:
1557                         ; 645   diagF_DiagResponse(Response, DataLength+RespLengthDefault, pMsgContext);       	
1559  02d8 ec80              	ldd	OFST+0,s
1560  02da 3b                	pshd	
1561  02db fc0001            	ldd	L702_DataLength
1562  02de f30003            	addd	L502_RespLengthDefault
1563  02e1 3b                	pshd	
1564  02e2 f60000            	ldab	L112_Response
1565  02e5 87                	clra	
1566  02e6 4a000000          	call	f_diagF_DiagResponse
1568  02ea 1b86              	leas	6,s
1569                         ; 646 }
1572  02ec 0a                	rtc	
1613                         ; 653 void DESC_API_CALLBACK_TYPE ApplDescRequestVINRequestSeed(DescMsgContext* pMsgContext)
1613                         ; 654 {
1614                         	switch	.ftext
1615  02ed                   f_ApplDescRequestVINRequestSeed:
1617  02ed 3b                	pshd	
1618       00000000          OFST:	set	0
1621                         ; 660   if(pMsgContext->reqData[0] < 0xFF)
1623  02ee b746              	tfr	d,y
1624  02f0 e6eb0000          	ldab	[0,y]
1625  02f4 c1ff              	cmpb	#255
1626  02f6 2408              	bhs	L107
1627                         ; 663       diagSecF_RequestSeed (pMsgContext);
1629  02f8 ec80              	ldd	OFST+0,s
1630  02fa 4a009999          	call	f_diagSecF_RequestSeed
1633  02fe 2005              	bra	L307
1634  0300                   L107:
1635                         ; 667       Response = LENGTH_INVALID_FORMAT;    
1637  0300 c605              	ldab	#5
1638  0302 7b0000            	stab	L112_Response
1639  0305                   L307:
1640                         ; 669   diagF_DiagResponse(Response, DataLength/*+RespLengthDefault*/, pMsgContext);       		
1642  0305 ec80              	ldd	OFST+0,s
1643  0307 3b                	pshd	
1644  0308 fc0001            	ldd	L702_DataLength
1645  030b 3b                	pshd	
1646  030c f60000            	ldab	L112_Response
1647  030f 87                	clra	
1648  0310 4a000000          	call	f_diagF_DiagResponse
1650  0314 1b86              	leas	6,s
1651                         ; 670 }
1654  0316 0a                	rtc	
1695                         ; 678 void DESC_API_CALLBACK_TYPE ApplDescSendVINSendKey(DescMsgContext* pMsgContext)
1695                         ; 679 {
1696                         	switch	.ftext
1697  0317                   f_ApplDescSendVINSendKey:
1699  0317 3b                	pshd	
1700       00000000          OFST:	set	0
1703                         ; 685   if(pMsgContext->reqData[0] < 0xFF)
1705  0318 b746              	tfr	d,y
1706  031a e6eb0000          	ldab	[0,y]
1707  031e c1ff              	cmpb	#255
1708  0320 2408              	bhs	L527
1709                         ; 687       diagSecF_ProcessKey (pMsgContext);  
1711  0322 ec80              	ldd	OFST+0,s
1712  0324 4a012121          	call	f_diagSecF_ProcessKey
1715  0328 2005              	bra	L727
1716  032a                   L527:
1717                         ; 691       Response = LENGTH_INVALID_FORMAT; 
1719  032a c605              	ldab	#5
1720  032c 7b0000            	stab	L112_Response
1721  032f                   L727:
1722                         ; 693         diagF_DiagResponse(Response, DataLength/*+RespLengthDefault*/, pMsgContext);       	
1724  032f ec80              	ldd	OFST+0,s
1725  0331 3b                	pshd	
1726  0332 fc0001            	ldd	L702_DataLength
1727  0335 3b                	pshd	
1728  0336 f60000            	ldab	L112_Response
1729  0339 87                	clra	
1730  033a 4a000000          	call	f_diagF_DiagResponse
1732  033e 1b86              	leas	6,s
1733                         ; 694 }
1736  0340 0a                	rtc	
1901                         	xdef	f_diagSecF_ProcessKey
1902                         	xdef	f_diagSecF_RequestSeed
1903                         	xref	f_diagF_DiagResponse
1904                         	xdef	f_diagSecLF_Key_alg
1905                         	xdef	f_diagSecLF_Start_10sec_delay_Timer
1906                         	xdef	f_diagSecLF_Calculate_Seed
1907                         	switch	.bss
1908  0000                   L112_Response:
1909  0000 00                	ds.b	1
1910  0001                   L702_DataLength:
1911  0001 0000              	ds.b	2
1912  0003                   L502_RespLengthDefault:
1913  0003 0000              	ds.b	2
1914  0005                   L302_temp_c:
1915  0005 00                	ds.b	1
1916  0006                   L102_Diag_Request_c:
1917  0006 00                	ds.b	1
1918  0007                   L771_DataLen:
1919  0007 0000              	ds.b	2
1920                         	xref	__TCNT
1921                         	xdef	f_diagSecF_CyclicTask
1922                         	xdef	f_diagSecF_ExtSession_Init
1923                         	xdef	f_diagSecF_DefSession_Init
1924                         	xdef	f_diagSecF_Main_Init
1925  0009                   L12_diag_sec_FAA_c:
1926  0009 00                	ds.b	1
1927  000a                   L71_secMod1_var2_l:
1928  000a 00000000          	ds.b	4
1929  000e                   L51_secMod1_var1_l:
1930  000e 00000000          	ds.b	4
1931  0012                   L31_diag_10sec_timer_w:
1932  0012 0000              	ds.b	2
1933  0014                   L11_hsm_key_l:
1934  0014 00000000          	ds.b	4
1935  0018                   L7_tool_key_l:
1936  0018 00000000          	ds.b	4
1937  001c                   L5_diag_seed_generated_l:
1938  001c 00000000          	ds.b	4
1939  0020                   L3_diag_seed_l:
1940  0020 00000000          	ds.b	4
1941  0024                   _diag_sec_access5_state_c:
1942  0024 00                	ds.b	1
1943                         	xdef	_diag_sec_access5_state_c
1944  0025                   _diag_sec_access1_state_c:
1945  0025 00                	ds.b	1
1946                         	xdef	_diag_sec_access1_state_c
1947  0026                   _diag_flags_c:
1948  0026 00                	ds.b	1
1949                         	xdef	_diag_flags_c
1950                         	xdef	f_ApplDescSendVINSendKey
1951                         	xdef	f_ApplDescRequestVINRequestSeed
1952                         	xdef	f_ApplDescSendLevel1Key
1953                         	xdef	f_ApplDescRequestLevel1Seed
1954                         	xdef	f_ApplDescOnTransitionSecurityAccess
1975                         	xref	c_lmul
1976                         	end
