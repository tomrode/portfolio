   1                         ; C Compiler for S12XE [COSMIC Software].
   2                         ; Generator V4.7.8 - 27 Jul 2007
   3                         ; Optimizer V4.6.13 - 27 Jul 2007
   4                         .const:	section	.data
   5  0000                   _Pwm_Config:
   6  0000 00                	dc.b	0
   7  0001 00                	dc.b	0
   8  0002 0e                	dc.b	14
   9  0003 00                	dc.b	0
  10  0004 0e                	dc.b	14
  11  0005 22                	dc.b	34
  12  0006 00                	dc.b	0
  13  0007 00a0              	dc.w	160
  14  0009 0000              	dc.w	0
  15  000b 00a0              	dc.w	160
  16  000d 0000              	dc.w	0
  17  000f 00a0              	dc.w	160
  18  0011 0000              	dc.w	0
  19  0013 ff                	dc.b	255
  20  0014 c0                	dc.b	192
  21  0015 aa                	dc.b	170
  22  0016 80                	dc.b	128
  23  0017 f8                	dc.b	248
  25  0018 0000              	dc.w	_Pwm_Notification_HFL
  27  001a 0000              	dc.w	_Pwm_Notification_HFR
  29  001c 0000              	dc.w	_Pwm_Notification_HRL
  31  001e 0000              	dc.w	_Pwm_Notification_HRR
  32  0020 0000              	dc.w	0
  33  0022 61a8              	dc.w	25000
  34  0024 0000              	dc.w	0
  35  0026 61a8              	dc.w	25000
  36  0028 0000              	dc.w	0
  37  002a 61a8              	dc.w	25000
  38  002c 0000              	dc.w	0
  39  002e 61a8              	dc.w	25000
  40  0030 0000              	dc.w	0
  41  0032 61a8              	dc.w	25000
  42  0034 0000              	dc.w	0
 259                         	xdef	_Pwm_Config
 260                         	xref	_Pwm_Notification_HRR
 261                         	xref	_Pwm_Notification_HRL
 262                         	xref	_Pwm_Notification_HFR
 263                         	xref	_Pwm_Notification_HFL
 282                         	end
