   1                         ; C Compiler for S12XE [COSMIC Software].
   2                         ; Generator V4.7.8 - 27 Jul 2007
   3                         ; Optimizer V4.6.13 - 27 Jul 2007
   4                         .const:	section	.data
   5  0000                   _kTpMainVersion:
   6  0000 03                	dc.b	3
   7  0001                   _kTpSubVersion:
   8  0001 05                	dc.b	5
   9  0002                   _kTpBugFixVersion:
  10  0002 00                	dc.b	0
  60                         ; 1627 void TP_API_CALL_TYPE TpInitPowerOn(void)
  60                         ; 1628 {
  61                         .ftext:	section	.text
  62  0000                   f_TpInitPowerOn:
  64  0000 3b                	pshd	
  65       00000002          OFST:	set	2
  68                         ; 1666       tpCanGetDynTxObjReturn = (TPCANGETDYNTXOBJ(canChannel)) ((CanTransmitHandle)(TP_TX_INIT_HANDLE(canChannel)));
  70  0001 cc0005            	ldd	#5
  71  0004 4a000000          	call	f_CanGetDynTxObj
  73  0008 6c80              	std	OFST-2,s
  74                         ; 1667       if (tpCanGetDynTxObjReturn == kCanNoTxDynObjAvailable)
  76  000a 04840c            	ibeq	d,L75
  77                         ; 1670         assertGeneral(kTpNoChannel, kTpErrNoDynObjAtTpInit );
  79                         ; 1677         TPCANDYNTXOBJSETDLC(canChannel) ((CanTransmitHandle)(TP_TX_INIT_HANDLE(canChannel)), FRAME_LENGTH);
  81  000d cc0008            	ldd	#8
  82  0010 3b                	pshd	
  83  0011 c605              	ldab	#5
  84  0013 4a000000          	call	f_CanDynTxObjSetDlc
  86  0017 1b82              	leas	2,s
  87  0019                   L75:
  88                         ; 1731     tpRxState[tpChannel].engine = kRxState_Idle;
  90  0019 79000c            	clr	L11_tpRxState+3
  91                         ; 1753     tpHighByte29Bit[tpChannel] = (vuint16)(kTpPhysPrioNormalFixed<<2);
  93  001c cc0018            	ldd	#24
  94  001f 7c0015            	std	L3_tpHighByte29Bit
  95                         ; 1754     tpPGN29Bit[tpChannel]  = kTpPhysPGNNormalFixed;
  97  0022 c6da              	ldab	#218
  98  0024 7c0013            	std	L5_tpPGN29Bit
  99                         ; 1757     tpTxState[tpChannel].engine = kTxState_Idle;
 101  0027 790011            	clr	L7_tpTxState+3
 102                         ; 1766     TpIntTxTransmitChannel_SetFree(tpChannel);
 104  002a c6ff              	ldab	#255
 105  002c 7b0007            	stab	L13_tpTransmitChannel
 106                         ; 1772   TpInit();
 108  002f 4a003535          	call	f_TpInit
 110                         ; 1774 }
 113  0033 31                	puly	
 114  0034 0a                	rtc	
 140                         ; 1788 void TP_API_CALL_TYPE TpInit(void)
 140                         ; 1789 {
 141                         	switch	.ftext
 142  0035                   f_TpInit:
 146                         ; 1797   tpStateTaskBusy = 0;
 148  0035 790008            	clr	L31_tpStateTaskBusy
 149                         ; 1806     __TpRxInit(tpChannel, kTpRxErrTpInitIsCalled);
 151  0038 cc0008            	ldd	#8
 152  003b 4a00a5a5          	call	L71f_TpRxInit
 154                         ; 1817     __TpTxInit((canuint8)tpChannel, kTpTxErrTpInitIsCalled);
 156  003f cc0028            	ldd	#40
 157  0042 4a004b4b          	call	L51f_TpTxInit
 159                         ; 1821   TpFuncInit();
 161  0046 4a08eaea          	call	f_TpFuncInit
 163                         ; 1824 }
 166  004a 0a                	rtc	
 211                         ; 1883 static void TpTxInit(canuint8 errorCode)
 211                         ; 1884 #else
 211                         ; 1885 static void TpTxInit(canuint8 tpChannel, canuint8 errorCode)
 211                         ; 1886 #endif
 211                         ; 1887 {
 212                         	switch	.ftext
 213  004b                   L51f_TpTxInit:
 215  004b 3b                	pshd	
 216  004c 37                	pshb	
 217       00000001          OFST:	set	1
 220                         ; 1895   TpGlobalInterruptDisable();
 222  004d 4a000000          	call	f_VStdSuspendAllInterrupts
 224                         ; 1898   tpErrorIndResult = kTpFreeChannel;
 226  0051 c601              	ldab	#1
 227  0053 6b80              	stab	OFST-1,s
 228                         ; 1899   if ((tpTxState[tpChannel].engine > kTxState_Reserved) && \
 228                         ; 1900      (tpTxState[tpChannel].engine != kTxState_Error)) /* ! (kTxState_Idle || kTxState_Reserved) */
 230  0055 f60011            	ldab	L7_tpTxState+3
 231  0058 c101              	cmpb	#1
 232  005a 2317              	bls	L701
 234  005c c109              	cmpb	#9
 235  005e 2713              	beq	L701
 236                         ; 1902     tpTxState[tpChannel].engine = kTxState_Error; 
 238  0060 c609              	ldab	#9
 239  0062 7b0011            	stab	L7_tpTxState+3
 240                         ; 1903     tpErrorIndResult = __ApplTpTxErrorIndication(tpChannel, errorCode);
 242  0065 e682              	ldab	OFST+1,s
 243  0067 87                	clra	
 244  0068 4a000000          	call	f_DescTxErrorIndication
 246  006c 6b80              	stab	OFST-1,s
 247                         ; 1905     if(tpErrorIndResult == kTpHoldChannel)
 249  006e 2603              	bne	L701
 250                         ; 1910       tpTxState[tpChannel].engine                 = kTxState_Idle;
 252  0070 790011            	clr	L7_tpTxState+3
 253  0073                   L701:
 254                         ; 1917   tpTxState[tpChannel].Timer                  = 0;
 256  0073 1879000e          	clrw	L7_tpTxState
 257                         ; 1920   tpTxState[tpChannel].queued                 = 0;
 259                         ; 1921   tpTxState[tpChannel].retransmit             = 0;
 261  0077 1d001205          	bclr	L7_tpTxState+4,5
 262                         ; 1930   if(tpErrorIndResult != kTpHoldChannel)
 264  007b e680              	ldab	OFST-1,s
 265  007d 270e              	beq	L311
 266                         ; 1934     tpTxState[tpChannel].engine                 = kTxState_Idle;
 268  007f 790011            	clr	L7_tpTxState+3
 269                         ; 1935     tpTxState[tpChannel].blocked                = 0; 
 271  0082 1d001202          	bclr	L7_tpTxState+4,2
 272                         ; 1961     tpTxInfoStruct[tpChannel].BlockSize         = 0;
 274  0086 79001e            	clr	_tpTxInfoStruct+7
 275                         ; 1982     tpTxInfoStruct[tpChannel].sequencenumber    = 0;
 277  0089 1d00200f          	bclr	_tpTxInfoStruct+9,15
 278  008d                   L311:
 279                         ; 2009   TpIntTxTransmitChannel_IsTx(tpChannel)
 281  008d f60007            	ldab	L13_tpTransmitChannel
 282  0090 260c              	bne	L511
 283                         ; 2027     TPCANCANCELTRANSMIT(TpIntTxGetCanChannel(tpChannel)) (TP_TX_HANDLE(tpChannel));
 285  0092 cc0005            	ldd	#5
 286  0095 4a000000          	call	f_CanCancelTransmit
 288                         ; 2029     TpIntTxTransmitChannel_SetFree(tpChannel);
 290  0099 c6ff              	ldab	#255
 291  009b 7b0007            	stab	L13_tpTransmitChannel
 292  009e                   L511:
 293                         ; 2036   TpGlobalInterruptRestore();
 295  009e 4a000000          	call	f_VStdResumeAllInterrupts
 297                         ; 2038 }
 300  00a2 1b83              	leas	3,s
 301  00a4 0a                	rtc	
 339                         ; 2056 static void TpRxInit(canuint8 errorCode)
 339                         ; 2057 #else
 339                         ; 2058 static void TpRxInit(canuint8 tpChannel, canuint8 errorCode)
 339                         ; 2059 #endif
 339                         ; 2060 {
 340                         	switch	.ftext
 341  00a5                   L71f_TpRxInit:
 343  00a5 3b                	pshd	
 344       00000000          OFST:	set	0
 347                         ; 2066   TpGlobalInterruptDisable();
 349  00a6 4a000000          	call	f_VStdSuspendAllInterrupts
 351                         ; 2069   if ((tpRxState[tpChannel].engine > kRxState_ApplInformed) && \
 351                         ; 2070       (tpRxState[tpChannel].engine != kRxState_Error)) /* ! (kRxState_Idle || kRxState_ApplInformed) */
 353  00aa f6000c            	ldab	L11_tpRxState+3
 354  00ad c101              	cmpb	#1
 355  00af 2310              	bls	L331
 357  00b1 c107              	cmpb	#7
 358  00b3 270c              	beq	L331
 359                         ; 2072     tpRxState[tpChannel].engine   = kRxState_Error;
 361  00b5 c607              	ldab	#7
 362  00b7 7b000c            	stab	L11_tpRxState+3
 363                         ; 2073     __ApplTpRxErrorIndication(tpChannel, errorCode);
 365  00ba e681              	ldab	OFST+1,s
 366  00bc 87                	clra	
 367  00bd 4a000000          	call	f_DescRxErrorIndication
 369  00c1                   L331:
 370                         ; 2075   tpRxState[tpChannel].Timer                    = 0;
 372  00c1 18790009          	clrw	L11_tpRxState
 373                         ; 2078   tpRxState[tpChannel].engine                   = kRxState_Idle;
 375  00c5 79000c            	clr	L11_tpRxState+3
 376                         ; 2079   tpRxState[tpChannel].queued                   = 0;
 378                         ; 2080   tpRxState[tpChannel].retransmit               = 0;
 380                         ; 2082   tpRxState[tpChannel].noCopyData               = 0;
 382  00c8 1d000d07          	bclr	L11_tpRxState+4,7
 383                         ; 2098   tpRxInfoStruct[tpChannel].SourceAddress = 0xff;
 385  00cc c6ff              	ldab	#255
 386  00ce 7b0027            	stab	_tpRxInfoStruct+6
 387                         ; 2118   tpRxInfoStruct[tpChannel].sequencenumber = 0;
 389  00d1 1d00280f          	bclr	_tpRxInfoStruct+7,15
 390                         ; 2144   tpRxInfoStruct[tpChannel].ApplGetBufferStatus = kTpFCClearToSend;
 392  00d5 1d002903          	bclr	_tpRxInfoStruct+8,3
 393                         ; 2154     TpIntRxTransmitChannel_IsRx(tpChannel)
 395  00d9 f60007            	ldab	L13_tpTransmitChannel
 396  00dc c180              	cmpb	#128
 397  00de 260c              	bne	L531
 398                         ; 2186       TPCANCANCELTRANSMIT(TpIntRxGetCanChannel(tpChannel)) (TP_RX_HANDLE(tpChannel));
 400  00e0 cc0005            	ldd	#5
 401  00e3 4a000000          	call	f_CanCancelTransmit
 403                         ; 2188       TpIntRxTransmitChannel_SetFree(tpChannel);
 405  00e7 c6ff              	ldab	#255
 406  00e9 7b0007            	stab	L13_tpTransmitChannel
 407  00ec                   L531:
 408                         ; 2198   TpGlobalInterruptRestore();
 410  00ec 4a000000          	call	f_VStdResumeAllInterrupts
 412                         ; 2200 }
 415  00f0 31                	puly	
 416  00f1 0a                	rtc	
 542                         ; 2220 canuint8 TP_INTERNAL_CALL_TYPE TpPrecopy(CanRxInfoStructPtr rxStruct)
 542                         ; 2221 #   else
 542                         ; 2222 #    if defined ( C_MULTIPLE_RECEIVE_BUFFER )
 542                         ; 2223 canuint8 TP_INTERNAL_CALL_TYPE TpPrecopy(CanChipDataPtr rxDataPtr)
 542                         ; 2224 #    endif
 542                         ; 2225 #    if defined ( C_SINGLE_RECEIVE_BUFFER )
 542                         ; 2226 canuint8 TP_INTERNAL_CALL_TYPE TpPrecopy(CanReceiveHandle rxObject)
 542                         ; 2227 #    endif
 542                         ; 2228 #   endif
 542                         ; 2229 {
 543                         	switch	.ftext
 544  00f2                   f_TpPrecopy:
 546  00f2 3b                	pshd	
 547  00f3 1b99              	leas	-7,s
 548       00000007          OFST:	set	7
 551                         ; 2296   tpCurrentSourceAddress = 0xFF;
 553                         ; 2298   tpCurrentTargetAddress = 0xFF;
 555                         ; 2365     tpCurrentSourceAddress = (canuint8)CAN_RX_ACTUAL_ID_EXT_LO; /* ( CAN_RX_ACTUAL_ID & 0x000000ff); */
 557  00f5 ed87              	ldy	OFST+0,s
 558  00f7 ed42              	ldy	2,y
 559  00f9 e643              	ldab	3,y
 560  00fb 54                	lsrb	
 561  00fc 6b81              	stab	OFST-6,s
 562  00fe e642              	ldab	2,y
 563  0100 56                	rorb	
 564  0101 56                	rorb	
 565  0102 c480              	andb	#128
 566  0104 ea81              	orab	OFST-6,s
 567  0106 6b85              	stab	OFST-2,s
 568                         ; 2367     tpCurrentTargetAddress = (canuint8)CAN_RX_ACTUAL_ID_EXT_MID_LO; /* ((CAN_RX_ACTUAL_ID & 0x0000ff00) >> 8); */
 570  0108 e642              	ldab	2,y
 571  010a 54                	lsrb	
 572  010b 6b81              	stab	OFST-6,s
 573  010d e641              	ldab	1,y
 574  010f 56                	rorb	
 575  0110 56                	rorb	
 576  0111 c480              	andb	#128
 577  0113 ea81              	orab	OFST-6,s
 578  0115 6b86              	stab	OFST-1,s
 579                         ; 2467   tpCanRxActualDLC = (canuint8)CAN_RX_ACTUAL_DLC;
 581  0117 e64c              	ldab	12,y
 582  0119 c40f              	andb	#15
 583  011b 6b84              	stab	OFST-3,s
 584                         ; 2488   switch (*(CanChipDataPtr)(TPCI_OFFSET + FORMAT_OFFSET + FRAME_DATA_PTR) & 0xF0)  
 586  011d ed87              	ldy	OFST+0,s
 587  011f e6eb0004          	ldab	[4,y]
 588  0123 c4f0              	andb	#240
 590  0125 2714              	beq	L731
 591  0127 c010              	subb	#16
 592  0129 2710              	beq	L731
 593  012b c010              	subb	#16
 594  012d 18270169          	beq	L551
 595  0131 c010              	subb	#16
 596  0133 182702ad          	beq	L751
 597  0137 18200380          	bra	L324
 598  013b                   L731:
 599                         ; 2599       if (tpRxState[tpChannel].engine != kRxState_Idle)
 601  013b f6000c            	ldab	L11_tpRxState+3
 602  013e 270d              	beq	L152
 603                         ; 2610         if (tpRxState[tpChannel].engine == kRxState_ApplInformed)
 605  0140 042103            	dbne	b,L352
 606                         ; 2617           return kCanNoCopyData;
 610  0143 1b89              	leas	9,s
 611  0145 0a                	rtc	
 612  0146                   L352:
 613                         ; 2622           __TpRxInit(tpChannel, kTpRxErrFF_SFreceivedAgain);
 615  0146 cc0006            	ldd	#6
 616  0149 4a00a5a5          	call	L71f_TpRxInit
 618  014d                   L152:
 619                         ; 2631       tpRxState[tpChannel].engine = kRxState_CanFrameReceived; 
 621  014d c602              	ldab	#2
 622  014f 7b000c            	stab	L11_tpRxState+3
 623                         ; 2665         tpRxInfoStruct[tpChannel].SourceAddress = tpCurrentSourceAddress;
 625  0152 180d850027        	movb	OFST-2,s,_tpRxInfoStruct+6
 626                         ; 2674       tpRxState[tpChannel].noCopyData = 0;
 628  0157 1d000d04          	bclr	L11_tpRxState+4,4
 629                         ; 2678       tpRxInfoStruct[tpChannel].DataIndex = 0;
 631  015b 18790023          	clrw	_tpRxInfoStruct+2
 632                         ; 2681       switch(*(CanChipDataPtr)(TPCI_OFFSET + FORMAT_OFFSET + FRAME_DATA_PTR) & 0xF0)
 634  015f ed87              	ldy	OFST+0,s
 635  0161 e6eb0004          	ldab	[4,y]
 636  0165 c4f0              	andb	#240
 638  0167 182700bd          	beq	L151
 639  016b c010              	subb	#16
 640  016d 270d              	beq	L141
 641                         ; 3061         default:
 641                         ; 3062           /* unknown frame */
 641                         ; 3063           tpRxState[tpChannel].engine = kRxState_ApplInformed;
 643  016f c601              	ldab	#1
 644  0171 7b000c            	stab	L11_tpRxState+3
 645                         ; 3064           TP_RX_RESET_CHANNEL(tpChannel);
 647  0174 4a088e8e          	call	f_TpRxResetChannel
 649                         ; 3065           break;
 652  0178 1820033f          	bra	L324
 653  017c                   L141:
 654                         ; 2722           if (tpCanRxActualDLC == FRAME_LENGTH)
 656  017c e684              	ldab	OFST-3,s
 657  017e c108              	cmpb	#8
 658  0180 18260097          	bne	L562
 659                         ; 2728             tpRxInfoStruct[tpChannel].DataLength  = (canuint16)(((canuint16)(*(CanChipDataPtr)(TPCI_OFFSET + FORMAT_OFFSET + FRAME_DATA_PTR) & 0x0F))<<8);
 661  0184 e6eb0004          	ldab	[4,y]
 662  0188 c40f              	andb	#15
 663  018a b710              	tfr	b,a
 664  018c c7                	clrb	
 665  018d 7c0025            	std	_tpRxInfoStruct+4
 666                         ; 2729             tpRxInfoStruct[tpChannel].DataLength |= (canuint16)(*(CanChipDataPtr)(DL_OFFSET + FORMAT_OFFSET + FRAME_DATA_PTR));
 668  0190 ed44              	ldy	4,y
 669  0192 e641              	ldab	1,y
 670  0194 b796              	exg	b,y
 671  0196 18fa0025          	ory	_tpRxInfoStruct+4
 672  019a 7d0025            	sty	_tpRxInfoStruct+4
 673                         ; 2732             if (tpRxInfoStruct[tpChannel].DataLength > ((canuint16)(FRAME_LENGTH - (SF_OFFSET + FORMAT_OFFSET))))
 675  019d 8d0007            	cpy	#7
 676  01a0 2379              	bls	L562
 677                         ; 2737               tpRxInfoStruct[tpChannel].ApplGetBufferStatus = kTpFCClearToSend;
 679  01a2 1d002903          	bclr	_tpRxInfoStruct+8,3
 680                         ; 2740               tpRxInfoStruct[tpChannel].DataBufferPtr.DataCanBufferPtr = (CanChipDataPtr)(FF_OFFSET + FORMAT_OFFSET + FRAME_DATA_PTR);
 682  01a6 ed87              	ldy	OFST+0,s
 683  01a8 ee44              	ldx	4,y
 684  01aa 1a02              	leax	2,x
 685  01ac 7e0021            	stx	_tpRxInfoStruct
 686                         ; 2744               tpRxInfoStruct[tpChannel].DataBufferPtr.DataApplBufferPtr = __ApplTpRxGetBuffer((canuint8)tpChannel, tpRxInfoStruct[tpChannel].DataLength);
 688  01af fc0025            	ldd	_tpRxInfoStruct+4
 689  01b2 4a000000          	call	f_DescGetBuffer
 691  01b6 7c0021            	std	_tpRxInfoStruct
 692                         ; 2747               switch(tpRxInfoStruct[tpChannel].ApplGetBufferStatus)
 694  01b9 f60029            	ldab	_tpRxInfoStruct+8
 695  01bc c403              	andb	#3
 697  01be 2713              	beq	L741
 698  01c0 c002              	subb	#2
 699  01c2 2752              	beq	L372
 700  01c4 040102            	dbeq	b,L541
 701  01c7 200a              	bra	L741
 702                         ; 2791                   tpRxState[tpChannel].engine = kRxState_Idle;
 704                         ; 2793                 break;
 706  01c9                   L541:
 707                         ; 2805                     tpRxState[tpChannel].Timer  = TpRxConfirmationTimeout(tpChannel);
 709  01c9 cc0065            	ldd	#101
 710  01cc 7c0009            	std	L11_tpRxState
 711                         ; 2806                     tpRxState[tpChannel].engine = kRxState_WaitForFCOverConfIsr;
 713  01cf c606              	ldab	#6
 714                         ; 2809                     tpRxState[tpChannel].queued = 1;
 716                         ; 2811                     __TpRxStateTask(tpChannel);
 719                         ; 2822                 break;
 721  01d1 2036              	bra	LC003
 722  01d3                   L741:
 723                         ; 2825               case kTpFCClearToSend:
 723                         ; 2826                 /* same as default - no break here */
 723                         ; 2827               default:
 723                         ; 2828 #  endif
 723                         ; 2829                 if(tpRxInfoStruct[tpChannel].DataBufferPtr.DataApplBufferPtr != V_NULL)
 725  01d3 fd0021            	ldy	_tpRxInfoStruct
 726  01d6 273e              	beq	L372
 727                         ; 2844                   *(tpRxInfoStruct[tpChannel].DataBufferPtr.DataApplBufferPtr + 0) = *(CanChipDataPtr)(FF_OFFSET + FORMAT_OFFSET + FRAME_DATA_PTR + 0);
 730  01d8 ee87              	ldx	OFST+0,s
 731  01da ee04              	ldx	4,x
 732  01dc 180a0240          	movb	2,x,0,y
 733                         ; 2845                   *(tpRxInfoStruct[tpChannel].DataBufferPtr.DataApplBufferPtr + 1) = *(CanChipDataPtr)(FF_OFFSET + FORMAT_OFFSET + FRAME_DATA_PTR + 1);
 735  01e0 180a0341          	movb	3,x,1,y
 736                         ; 2846                   *(tpRxInfoStruct[tpChannel].DataBufferPtr.DataApplBufferPtr + 2) = *(CanChipDataPtr)(FF_OFFSET + FORMAT_OFFSET + FRAME_DATA_PTR + 2);
 738  01e4 180a0442          	movb	4,x,2,y
 739                         ; 2847                   *(tpRxInfoStruct[tpChannel].DataBufferPtr.DataApplBufferPtr + 3) = *(CanChipDataPtr)(FF_OFFSET + FORMAT_OFFSET + FRAME_DATA_PTR + 3);
 741  01e8 180a0543          	movb	5,x,3,y
 742                         ; 2848                   *(tpRxInfoStruct[tpChannel].DataBufferPtr.DataApplBufferPtr + 4) = *(CanChipDataPtr)(FF_OFFSET + FORMAT_OFFSET + FRAME_DATA_PTR + 4);
 744  01ec 180a0644          	movb	6,x,4,y
 745                         ; 2851                     *(tpRxInfoStruct[tpChannel].DataBufferPtr.DataApplBufferPtr + 5) = *(CanChipDataPtr)(FF_OFFSET + FORMAT_OFFSET + FRAME_DATA_PTR + 5);
 747  01f0 180a0745          	movb	7,x,5,y
 748                         ; 2875                   tpRxInfoStruct[tpChannel].DataIndex = (canuint16)(FRAME_LENGTH - (FF_OFFSET + FORMAT_OFFSET));    
 750  01f4 cc0006            	ldd	#6
 751  01f7 7c0023            	std	_tpRxInfoStruct+2
 752                         ; 2876                   tpRxInfoStruct[tpChannel].sequencenumber = 1;        /* Await CF with SN 1 next */
 754  01fa 1d00280f          	bclr	_tpRxInfoStruct+7,15
 755  01fe 1c002801          	bset	_tpRxInfoStruct+7,1
 756                         ; 2883                     tpRxState[tpChannel].Timer  = TpRxConfirmationTimeout(tpChannel);
 758  0202 c665              	ldab	#101
 759  0204 7c0009            	std	L11_tpRxState
 760                         ; 2884                     tpRxState[tpChannel].engine = kRxState_WaitForFCConfIsr;
 762  0207 c605              	ldab	#5
 763                         ; 2886                     tpRxState[tpChannel].queued = 1; /* set transmission request */
 765                         ; 2888                     __TpRxStateTask(tpChannel);
 767  0209                   LC003:
 768  0209 7b000c            	stab	L11_tpRxState+3
 769  020c 1c000d01          	bset	L11_tpRxState+4,1
 770  0210 4a05e1e1          	call	f_TpRxStateTask
 773  0214 200e              	bra	L103
 774  0216                   L372:
 775                         ; 2903                   tpRxState[tpChannel].engine = kRxState_Idle;
 777  0216 79000c            	clr	L11_tpRxState+3
 778  0219 2009              	bra	L103
 780  021b                   L562:
 781                         ; 2909               tpRxState[tpChannel].engine = kRxState_ApplInformed;
 783                         ; 2910               TP_RX_RESET_CHANNEL(tpChannel);
 787                         ; 2916             tpRxState[tpChannel].engine = kRxState_ApplInformed;
 789                         ; 2917             TP_RX_RESET_CHANNEL(tpChannel);
 791  021b c601              	ldab	#1
 792  021d 7b000c            	stab	L11_tpRxState+3
 793  0220 4a088e8e          	call	f_TpRxResetChannel
 795  0224                   L103:
 796                         ; 2928         return kCanNoCopyData; /* Do not receive the frame */
 798  0224 c7                	clrb	
 801  0225 1b89              	leas	9,s
 802  0227 0a                	rtc	
 803  0228                   L151:
 804                         ; 2933         case kL4_SingleFrame:
 804                         ; 2934 #endif  /* defined( TP_ENABLE_MF_RECEPTION ) */
 804                         ; 2935 
 804                         ; 2936 #if (TP_USE_APPL_PRECOPY == kTpOn) && (TP_USE_FAST_PRECOPY == kTpOff) && defined (TP_DISABLE_CHECKTA_COMPATIBILITY)
 804                         ; 2937 # if defined(TP_ENABLE_NORMAL_FIXED_ADDRESSING) 
 804                         ; 2938         {
 804                         ; 2939           switch (tpApplTpPrecopyReturn)
 804                         ; 2940           {
 804                         ; 2941           case kTpNone:
 804                         ; 2942             tpRxState[tpChannel].engine = kRxState_Idle;
 804                         ; 2943             return kCanNoCopyData;
 804                         ; 2944             /*break;*/
 804                         ; 2945           case kTpPhysical:
 804                         ; 2946             tpRxInfoStruct[tpChannel].ta_type = 0;
 804                         ; 2947             break;
 804                         ; 2948           case kTpFunctional:
 804                         ; 2949             tpRxInfoStruct[tpChannel].ta_type = 1;
 804                         ; 2950             break;
 804                         ; 2951           default:
 804                         ; 2952             /* Call everytime assertion */
 804                         ; 2953             assertGeneral(tpChannel, kTpErrPrecopyCheckInvalidReturn);
 804                         ; 2954             break;
 804                         ; 2955           }      
 804                         ; 2956         }
 804                         ; 2957 # endif
 804                         ; 2958 #endif          
 804                         ; 2959           tpRxInfoStruct[tpChannel].DataLength = *(CanChipDataPtr)(TPCI_OFFSET + FORMAT_OFFSET + FRAME_DATA_PTR); 
 806  0228 e6eb0004          	ldab	[4,y]
 807  022c 87                	clra	
 808  022d 7c0025            	std	_tpRxInfoStruct+4
 809                         ; 2966           if (tpCanRxActualDLC >= (tpRxInfoStruct[tpChannel].DataLength + SF_OFFSET + FORMAT_OFFSET))
 811  0230 e684              	ldab	OFST-3,s
 812  0232 6c80              	std	OFST-7,s
 813  0234 fd0025            	ldy	_tpRxInfoStruct+4
 814  0237 02                	iny	
 815  0238 ad80              	cpy	OFST-7,s
 816  023a 2253              	bhi	L703
 817                         ; 2970             if ((tpRxInfoStruct[tpChannel].DataLength <= ((canuint16)(FRAME_LENGTH - (SF_OFFSET + FORMAT_OFFSET))))
 817                         ; 2971 #if defined (TP_ENABLE_ISO_15765_2_2)
 817                         ; 2972               && (tpRxInfoStruct[tpChannel].DataLength  != 0)
 817                         ; 2973 #endif
 817                         ; 2974               )
 819  023c fc0025            	ldd	_tpRxInfoStruct+4
 820  023f 8c0007            	cpd	#7
 821  0242 224b              	bhi	L703
 823  0244 044448            	tbeq	d,L703
 824                         ; 2978               tpRxInfoStruct[tpChannel].ApplGetBufferStatus = kTpFCClearToSend;
 826  0247 1d002903          	bclr	_tpRxInfoStruct+8,3
 827                         ; 2980               tpRxInfoStruct[tpChannel].DataBufferPtr.DataCanBufferPtr = (CanChipDataPtr)(SF_OFFSET + FORMAT_OFFSET + FRAME_DATA_PTR);
 829  024b ed87              	ldy	OFST+0,s
 830  024d ee44              	ldx	4,y
 831  024f 08                	inx	
 832  0250 7e0021            	stx	_tpRxInfoStruct
 833                         ; 2984               tpRxInfoStruct[tpChannel].DataBufferPtr.DataApplBufferPtr = __ApplTpRxGetBuffer((canuint8)tpChannel, tpRxInfoStruct[tpChannel].DataLength);
 835  0253 fc0025            	ldd	_tpRxInfoStruct+4
 836  0256 4a000000          	call	f_DescGetBuffer
 838  025a 7c0021            	std	_tpRxInfoStruct
 839                         ; 2989               if((tpRxInfoStruct[tpChannel].DataBufferPtr.DataApplBufferPtr != V_NULL)
 839                         ; 2990 #if (defined(TP_ENABLE_FC_WAIT) || defined(TP_ENABLE_FC_SUPPRESS) || defined(TP_ENABLE_FC_OVERFLOW))
 839                         ; 2991                 && (tpRxInfoStruct[tpChannel].ApplGetBufferStatus == kTpFCClearToSend)
 839                         ; 2992 #endif
 839                         ; 2993                 )
 842  025d 2730              	beq	L703
 844  025f f60029            	ldab	_tpRxInfoStruct+8
 845  0262 c503              	bitb	#3
 846  0264 2629              	bne	L703
 847                         ; 3007                 for (counter = (cansintCPUtype)(tpRxInfoStruct[tpChannel].DataLength-1); counter>=0; counter--)
 850  0266 fd0025            	ldy	_tpRxInfoStruct+4
 851  0269 03                	dey	
 852  026a 6d82              	sty	OFST-5,s
 854  026c fd0021            	ldy	_tpRxInfoStruct
 855  026f 200c              	bra	L513
 856  0271                   L113:
 857                         ; 3009                   *(tpRxInfoStruct[tpChannel].DataBufferPtr.DataApplBufferPtr + counter) = *((CanChipDataPtr)(SF_OFFSET + FORMAT_OFFSET + FRAME_DATA_PTR) + counter);
 859  0271 ee87              	ldx	OFST+0,s
 860  0273 ee04              	ldx	4,x
 861  0275 08                	inx	
 862  0276 180ae6ee          	movb	d,x,d,y
 863                         ; 3007                 for (counter = (cansintCPUtype)(tpRxInfoStruct[tpChannel].DataLength-1); counter>=0; counter--)
 865  027a 186382            	decw	OFST-5,s
 866  027d                   L513:
 869  027d ec82              	ldd	OFST-5,s
 870  027f 2cf0              	bge	L113
 871                         ; 3033                   tpRxState[tpChannel].engine = kRxState_ApplInformed; 
 873  0281 c601              	ldab	#1
 874  0283 7b000c            	stab	L11_tpRxState+3
 875                         ; 3035                   __ApplTpRxIndication((canuint8)tpChannel, tpRxInfoStruct[tpChannel].DataLength);
 877  0286 fc0025            	ldd	_tpRxInfoStruct+4
 878  0289 4a000000          	call	f_DescPhysReqInd
 881  028d 2045              	bra	L733
 882  028f                   L703:
 883                         ; 3040                 tpRxState[tpChannel].engine = kRxState_ApplInformed;
 885                         ; 3041                 TP_RX_RESET_CHANNEL(tpChannel);
 889                         ; 3046               tpRxState[tpChannel].engine = kRxState_ApplInformed;
 891                         ; 3047               TP_RX_RESET_CHANNEL(tpChannel);
 895                         ; 3053             tpRxState[tpChannel].engine = kRxState_ApplInformed;
 897                         ; 3054             TP_RX_RESET_CHANNEL(tpChannel);
 899  028f c601              	ldab	#1
 900  0291 7b000c            	stab	L11_tpRxState+3
 901  0294 4a088e8e          	call	f_TpRxResetChannel
 903                         ; 3057           return kCanNoCopyData;
 906  0298 203a              	bra	L733
 907                         ; 3069     break;
 909  029a                   L551:
 910                         ; 3089       if((tpRxState[tpChannel].engine == kRxState_WaitCF) 
 910                         ; 3090 # if (TP_HIGH_RX_LOW_TX_PRIORITY == kTpOn) 
 910                         ; 3091         || (tpRxState[tpChannel].engine == kRxState_WaitForFCConfIsr)
 910                         ; 3092 # endif
 910                         ; 3093         )
 912  029a f6000c            	ldab	L11_tpRxState+3
 913  029d c103              	cmpb	#3
 914  029f 2706              	beq	L133
 916  02a1 c105              	cmpb	#5
 917  02a3 18260214          	bne	L324
 918  02a7                   L133:
 919                         ; 3108             if( (tpRxInfoStruct[tpChannel].SourceAddress == (canuint8)tpCurrentSourceAddress) 
 919                         ; 3109 #   if (TP_USE_FAST_PRECOPY == kTpOff)
 919                         ; 3110               && (TP_RX_ECU_NR(tpChannel) == tpCurrentTargetAddress) /* Compare addresses */
 919                         ; 3111 #   endif
 919                         ; 3112               )
 921  02a7 f60027            	ldab	_tpRxInfoStruct+6
 922  02aa e185              	cmpb	OFST-2,s
 923  02ac 1826020b          	bne	L324
 925  02b0 e686              	ldab	OFST-1,s
 926  02b2 c1c2              	cmpb	#194
 927  02b4 18260203          	bne	L324
 928                         ; 3129             if(tpRxState[tpChannel].engine == kRxState_WaitForFCConfIsr)
 930  02b8 f6000c            	ldab	L11_tpRxState+3
 931  02bb c105              	cmpb	#5
 932  02bd 2619              	bne	L533
 933                         ; 3131               if(tpRxState[tpChannel].queued == 0)
 935  02bf 1e000d0110        	brset	L11_tpRxState+4,1,L733
 936                         ; 3137                 TPCANCANCELTRANSMIT(TpIntRxGetCanChannel(tpChannel)) (TP_RX_HANDLE(tpChannel)); /* suppress real confirmation */ 
 938  02c4 cc0005            	ldd	#5
 939  02c7 4a000000          	call	f_CanCancelTransmit
 941                         ; 3139                 TpDrvConfirmation(TP_RX_HANDLE(tpChannel)); /* Call confirmation function to simulate expected WaitForFCConfIsr event */
 943  02cb cc0005            	ldd	#5
 944  02ce 4a072828          	call	f_TpDrvConfirmation
 946                         ; 3143                 assertReturnInternal(tpChannel,tpRxState[tpChannel].engine == kRxState_WaitCF, kTpRxErrNotInWaitCFState,kCanNoCopyData);
 948  02d2 2004              	bra	L533
 949  02d4                   L733:
 950                         ; 3147                 return kCanNoCopyData; /* Ignore frame and wait for correct frame */
 953  02d4 c7                	clrb	
 955  02d5 1b89              	leas	9,s
 956  02d7 0a                	rtc	
 957  02d8                   L533:
 958                         ; 3152             if( (tpRxInfoStruct[tpChannel].DataIndex + ((canuint16)(FRAME_LENGTH - (CF_OFFSET + FORMAT_OFFSET))) ) >= tpRxInfoStruct[tpChannel].DataLength )
 960  02d8 fd0023            	ldy	_tpRxInfoStruct+2
 961  02db 1947              	leay	7,y
 962  02dd bd0025            	cpy	_tpRxInfoStruct+4
 963  02e0 e684              	ldab	OFST-3,s
 964  02e2 2564              	blo	L343
 965                         ; 3159               if (tpCanRxActualDLC >= (tpRxInfoStruct[tpChannel].DataLength - tpRxInfoStruct[tpChannel].DataIndex + FORMAT_OFFSET + CF_OFFSET))
 967  02e4 87                	clra	
 968  02e5 6c80              	std	OFST-7,s
 969  02e7 fc0025            	ldd	_tpRxInfoStruct+4
 970  02ea b30023            	subd	_tpRxInfoStruct+2
 971  02ed c30001            	addd	#1
 972  02f0 ac80              	cpd	OFST-7,s
 973  02f2 182200ea          	bhi	L563
 974                         ; 3164                 if( ((*(CanChipDataPtr)(TPCI_OFFSET + FORMAT_OFFSET + FRAME_DATA_PTR)) & kL4_SNMask) != (canuint8)(tpRxInfoStruct[tpChannel].sequencenumber) )
 976  02f6 f60028            	ldab	_tpRxInfoStruct+7
 977  02f9 c40f              	andb	#15
 978  02fb 6b81              	stab	OFST-6,s
 979  02fd ed87              	ldy	OFST+0,s
 980  02ff e6eb0004          	ldab	[4,y]
 981  0303 c40f              	andb	#15
 982  0305 e181              	cmpb	OFST-6,s
 983                         ; 3181                     __TpRxInit(tpChannel, kTpRxErrWrongSNreceived);
 987  0307 2658              	bne	LC005
 988                         ; 3192                   if (tpRxState[tpChannel].noCopyData == 0)
 991  0309 1e000d0426        	brset	L11_tpRxState+4,4,L353
 992                         ; 3196                     for (counter = (cansintCPUtype)(tpRxInfoStruct[tpChannel].DataLength - (tpRxInfoStruct[tpChannel].DataIndex + 1)); counter>=0; counter--)
 994  030e fd0023            	ldy	_tpRxInfoStruct+2
 995  0311 02                	iny	
 996  0312 6d80              	sty	OFST-7,s
 997  0314 fc0025            	ldd	_tpRxInfoStruct+4
 998  0317 a380              	subd	OFST-7,s
 999  0319 6c82              	std	OFST-5,s
1001  031b 2015              	bra	L163
1002  031d                   L553:
1003                         ; 3198                       *(tpRxInfoStruct[tpChannel].DataBufferPtr.DataApplBufferPtr + tpRxInfoStruct[tpChannel].DataIndex + counter) = *((CanChipDataPtr)(CF_OFFSET + FORMAT_OFFSET + FRAME_DATA_PTR) + counter);
1005  031d fd0021            	ldy	_tpRxInfoStruct
1006  0320 18fb0023          	addy	_tpRxInfoStruct+2
1007  0324 ee87              	ldx	OFST+0,s
1008  0326 ee04              	ldx	4,x
1009  0328 08                	inx	
1010  0329 180ae6ee          	movb	d,x,d,y
1011                         ; 3196                     for (counter = (cansintCPUtype)(tpRxInfoStruct[tpChannel].DataLength - (tpRxInfoStruct[tpChannel].DataIndex + 1)); counter>=0; counter--)
1013  032d 186382            	decw	OFST-5,s
1014  0330 ec82              	ldd	OFST-5,s
1015  0332                   L163:
1018  0332 2ce9              	bge	L553
1019  0334                   L353:
1020                         ; 3222                     tpRxState[tpChannel].engine = kRxState_ApplInformed; /* Stop waiting for CF/FC */
1022  0334 c601              	ldab	#1
1023  0336 7b000c            	stab	L11_tpRxState+3
1024                         ; 3223                     tpRxState[tpChannel].Timer  = 0; /* Stop Timeout Counter */
1026  0339 18790009          	clrw	L11_tpRxState
1027                         ; 3225                     __ApplTpRxIndication((canuint8)tpChannel, tpRxInfoStruct[tpChannel].DataLength);
1029  033d fc0025            	ldd	_tpRxInfoStruct+4
1030  0340 4a000000          	call	f_DescPhysReqInd
1032  0344 18200098          	bra	L563
1033  0348                   L343:
1034                         ; 3234               if (tpCanRxActualDLC == FRAME_LENGTH)
1036  0348 c108              	cmpb	#8
1037  034a 18260092          	bne	L563
1038                         ; 3238                 if( ((*(CanChipDataPtr)(TPCI_OFFSET + FORMAT_OFFSET + FRAME_DATA_PTR)) & kL4_SNMask) != (canuint8)(tpRxInfoStruct[tpChannel].sequencenumber) )
1040  034e f60028            	ldab	_tpRxInfoStruct+7
1041  0351 c40f              	andb	#15
1042  0353 6b81              	stab	OFST-6,s
1043  0355 ed87              	ldy	OFST+0,s
1044  0357 e6eb0004          	ldab	[4,y]
1045  035b c40f              	andb	#15
1046  035d e181              	cmpb	OFST-6,s
1047  035f 2709              	beq	L173
1048                         ; 3240                   __TpRxInit(tpChannel, kTpRxErrWrongSNreceived);
1050  0361                   LC005:
1051  0361 cc0001            	ldd	#1
1052  0364 4a00a5a5          	call	L71f_TpRxInit
1055  0368 2076              	bra	L563
1056  036a                   L173:
1057                         ; 3249                   if (tpRxState[tpChannel].noCopyData == 0)
1060  036a 1e000d042d        	brset	L11_tpRxState+4,4,L573
1061                         ; 3253                     *(tpRxInfoStruct[tpChannel].DataBufferPtr.DataApplBufferPtr + tpRxInfoStruct[tpChannel].DataIndex + 0) = *(CanChipDataPtr)(CF_OFFSET + FORMAT_OFFSET + FRAME_DATA_PTR + 0);
1063  036f fd0023            	ldy	_tpRxInfoStruct+2
1064  0372 fc0021            	ldd	_tpRxInfoStruct
1065  0375 ee87              	ldx	OFST+0,s
1066  0377 ee04              	ldx	4,x
1067  0379 180a01ee          	movb	1,x,d,y
1068                         ; 3254                     *(tpRxInfoStruct[tpChannel].DataBufferPtr.DataApplBufferPtr + tpRxInfoStruct[tpChannel].DataIndex + 1) = *(CanChipDataPtr)(CF_OFFSET + FORMAT_OFFSET + FRAME_DATA_PTR + 1);
1070  037d 19ee              	leay	d,y
1071  037f 180a0241          	movb	2,x,1,y
1072                         ; 3255                     *(tpRxInfoStruct[tpChannel].DataBufferPtr.DataApplBufferPtr + tpRxInfoStruct[tpChannel].DataIndex + 2) = *(CanChipDataPtr)(CF_OFFSET + FORMAT_OFFSET + FRAME_DATA_PTR + 2);
1074  0383 fd0023            	ldy	_tpRxInfoStruct+2
1075  0386 19ee              	leay	d,y
1076  0388 180a0342          	movb	3,x,2,y
1077                         ; 3256                     *(tpRxInfoStruct[tpChannel].DataBufferPtr.DataApplBufferPtr + tpRxInfoStruct[tpChannel].DataIndex + 3) = *(CanChipDataPtr)(CF_OFFSET + FORMAT_OFFSET + FRAME_DATA_PTR + 3);
1079  038c 180a0443          	movb	4,x,3,y
1080                         ; 3257                     *(tpRxInfoStruct[tpChannel].DataBufferPtr.DataApplBufferPtr + tpRxInfoStruct[tpChannel].DataIndex + 4) = *(CanChipDataPtr)(CF_OFFSET + FORMAT_OFFSET + FRAME_DATA_PTR + 4);
1082  0390 180a0544          	movb	5,x,4,y
1083                         ; 3258                     *(tpRxInfoStruct[tpChannel].DataBufferPtr.DataApplBufferPtr + tpRxInfoStruct[tpChannel].DataIndex + 5) = *(CanChipDataPtr)(CF_OFFSET + FORMAT_OFFSET + FRAME_DATA_PTR + 5);
1085  0394 180a0645          	movb	6,x,5,y
1086                         ; 3261                       *(tpRxInfoStruct[tpChannel].DataBufferPtr.DataApplBufferPtr + tpRxInfoStruct[tpChannel].DataIndex + 6) = *(CanChipDataPtr)(CF_OFFSET + FORMAT_OFFSET + FRAME_DATA_PTR + 6);
1088  0398 180a0746          	movb	7,x,6,y
1089  039c                   L573:
1090                         ; 3285                   tpRxInfoStruct[tpChannel].DataIndex = tpRxInfoStruct[tpChannel].DataIndex + (canuint16)(FRAME_LENGTH - (CF_OFFSET + FORMAT_OFFSET));   /*Set rx index to next free data element*/
1092  039c fd0023            	ldy	_tpRxInfoStruct+2
1093  039f 1947              	leay	7,y
1094  03a1 7d0023            	sty	_tpRxInfoStruct+2
1095                         ; 3289                   tpRxInfoStruct[tpChannel].sequencenumber = CANBITTYPE_CAST(tpRxInfoStruct[tpChannel].sequencenumber + 1); /*SN increment*/
1097  03a4 f60028            	ldab	_tpRxInfoStruct+7
1098  03a7 c40f              	andb	#15
1099  03a9 52                	incb	
1100  03aa 1d00280f          	bclr	_tpRxInfoStruct+7,15
1101  03ae c40f              	andb	#15
1102  03b0 fa0028            	orab	_tpRxInfoStruct+7
1103  03b3 7b0028            	stab	_tpRxInfoStruct+7
1104                         ; 3293                   if(tpRxState[tpChannel].BSCounter != 0) 
1106  03b6 f7000b            	tst	L11_tpRxState+2
1107  03b9 271a              	beq	L773
1108                         ; 3295                     tpRxState[tpChannel].BSCounter--;
1110  03bb 73000b            	dec	L11_tpRxState+2
1111                         ; 3297                     if(tpRxState[tpChannel].BSCounter == 0)
1113  03be 2615              	bne	L773
1114                         ; 3299                       tpRxState[tpChannel].engine = kRxState_WaitForFCConfIsr;
1116  03c0 c605              	ldab	#5
1117  03c2 7b000c            	stab	L11_tpRxState+3
1118                         ; 3300                       tpRxState[tpChannel].Timer = TpRxConfirmationTimeout(tpChannel);
1120  03c5 cc0065            	ldd	#101
1121  03c8 7c0009            	std	L11_tpRxState
1122                         ; 3302                       tpRxState[tpChannel].queued = 1;
1124  03cb 1c000d01          	bset	L11_tpRxState+4,1
1125                         ; 3304                       __TpRxStateTask(tpChannel);
1127  03cf 4a05e1e1          	call	f_TpRxStateTask
1129                         ; 3306                       return kCanNoCopyData;
1132  03d3 200b              	bra	L563
1133  03d5                   L773:
1134                         ; 3309                   tpRxState[tpChannel].Timer = kTimeoutCF(tpChannel);
1136  03d5 cc0065            	ldd	#101
1137  03d8 7c0009            	std	L11_tpRxState
1138                         ; 3310                   tpRxState[tpChannel].engine = kRxState_WaitCF;
1140  03db c603              	ldab	#3
1141  03dd 7b000c            	stab	L11_tpRxState+3
1142  03e0                   L563:
1143                         ; 3314             return kCanNoCopyData;
1146  03e0 c7                	clrb	
1148  03e1 1b89              	leas	9,s
1149  03e3 0a                	rtc	
1150  03e4                   L751:
1151                         ; 3337       if((  tpTxState[tpChannel].engine == kTxState_WaitFC) 
1151                         ; 3338         || (tpTxState[tpChannel].engine == kTxState_WaitForFFConfIsr)
1151                         ; 3339         || (tpTxState[tpChannel].engine == kTxState_WaitForCFConfIsr))
1153  03e4 f60011            	ldab	L7_tpTxState+3
1154  03e7 c102              	cmpb	#2
1155  03e9 270a              	beq	L504
1157  03eb c106              	cmpb	#6
1158  03ed 2706              	beq	L504
1160  03ef c107              	cmpb	#7
1161  03f1 182600c6          	bne	L324
1162  03f5                   L504:
1163                         ; 3355             if( (tpTxInfoStruct[tpChannel].TargetAddress == (canuint8)tpCurrentSourceAddress) 
1163                         ; 3356 # if (TP_USE_FAST_PRECOPY == kTpOff)          
1163                         ; 3357               && (TP_TX_ECU_NR(tpChannel) == tpCurrentTargetAddress) /* Compare addresses */         
1163                         ; 3358 # endif
1163                         ; 3359               )
1165  03f5 f6001d            	ldab	_tpTxInfoStruct+6
1166  03f8 e185              	cmpb	OFST-2,s
1167  03fa 182600bd          	bne	L324
1169  03fe e686              	ldab	OFST-1,s
1170  0400 c1c2              	cmpb	#194
1171  0402 182600b5          	bne	L324
1172                         ; 3376             if(tpTxState[tpChannel].queued == 0) 
1174  0406 1e00120145        	brset	L7_tpTxState+4,1,L314
1175                         ; 3378               if((tpTxState[tpChannel].engine == kTxState_WaitForFFConfIsr)     ||
1175                         ; 3379                 ((tpTxState[tpChannel].engine == kTxState_WaitForCFConfIsr) && 
1175                         ; 3380                  (tpTxState[tpChannel].BSCounter == 1)) )
1177  040b f60011            	ldab	L7_tpTxState+3
1178  040e c106              	cmpb	#6
1179  0410 270a              	beq	L714
1181  0412 c107              	cmpb	#7
1182  0414 2614              	bne	L124
1184  0416 f60010            	ldab	L7_tpTxState+2
1185  0419 04210e            	dbne	b,L124
1186  041c                   L714:
1187                         ; 3386                 TPCANCANCELTRANSMIT(TpIntTxGetCanChannel(tpChannel)) (TP_TX_HANDLE(tpChannel)); /* suppress scheduled event */
1189  041c cc0005            	ldd	#5
1190  041f 4a000000          	call	f_CanCancelTransmit
1192                         ; 3388                 TpDrvConfirmation(TP_TX_HANDLE(tpChannel)); /* call driver routine now */
1194  0423 cc0005            	ldd	#5
1195  0426 4a072828          	call	f_TpDrvConfirmation
1197  042a                   L124:
1198                         ; 3401             if(  (tpTxState[tpChannel].engine == kTxState_WaitFC)
1198                         ; 3402               ) 
1200  042a f60011            	ldab	L7_tpTxState+3
1201  042d c102              	cmpb	#2
1202  042f 18260088          	bne	L324
1203                         ; 3409               if (tpCanRxActualDLC >= (FC_LENGTH + FORMAT_OFFSET))
1205  0433 e684              	ldab	OFST-3,s
1206  0435 c103              	cmpb	#3
1207  0437 18250080          	blo	L324
1208                         ; 3414                 switch(*(CanChipDataPtr)(FRAME_DATA_PTR + FORMAT_OFFSET + TPCI_OFFSET))
1210  043b ed87              	ldy	OFST+0,s
1211  043d e6eb0004          	ldab	[4,y]
1213  0441 c030              	subb	#48
1214  0443 270f              	beq	L161
1215  0445 53                	decb	
1216  0446 2764              	beq	L361
1217  0448 53                	decb	
1218  0449 2769              	beq	L561
1219                         ; 3533                     __TpTxInit(tpChannel, kTpTxErrFCWrongFlowStatus);
1221  044b cc0026            	ldd	#38
1223                         ; 3536                   break;
1225  044e 2067              	bra	LC008
1226  0450                   L314:
1227                         ; 3397                 return kCanNoCopyData;
1229  0450 c7                	clrb	
1232  0451 1b89              	leas	9,s
1233  0453 0a                	rtc	
1234  0454                   L161:
1235                         ; 3420                       if (tpTxState[tpChannel].firstFC == 0)  
1237  0454 1e0012083f        	brset	L7_tpTxState+4,8,L334
1238                         ; 3424                         tpTxState[tpChannel].firstFC = 1; /* Set flag with the first received FC */
1240  0459 1c001208          	bset	L7_tpTxState+4,8
1241                         ; 3425                         tpTxInfoStruct[tpChannel].BlockSize = (*(CanChipDataPtr)(FRAME_DATA_PTR + FORMAT_OFFSET + BS_OFFSET)); 
1243  045d ed44              	ldy	4,y
1244  045f e641              	ldab	1,y
1245  0461 7b001e            	stab	_tpTxInfoStruct+7
1246                         ; 3431                         tpSTminInFrame = (*(CanChipDataPtr)(FRAME_DATA_PTR + FORMAT_OFFSET + STMIN_OFFSET));
1248  0464 ed87              	ldy	OFST+0,s
1249  0466 ed44              	ldy	4,y
1250  0468 e642              	ldab	2,y
1251  046a 87                	clra	
1252  046b 6c82              	std	OFST-5,s
1253                         ; 3437                         if (tpSTminInFrame < (canuintCPUtype)(TpTxGetSTMINDefaultTime(tpChannel)*TpTxCallCycle))
1255  046d 8c000a            	cpd	#10
1256                         ; 3439                           tpTxInfoStruct[tpChannel].STMin = (canuint8)TpTxGetSTMINDefaultTime(tpChannel);
1259  0470 250e              	blo	LC007
1260                         ; 3445                           if ((tpSTminInFrame & 0x80) != 0)
1262  0472 c580              	bitb	#128
1263  0474 2712              	beq	L144
1264                         ; 3447                             if ((tpSTminInFrame > 0xF0) && (tpSTminInFrame <= 0xF9))
1266  0476 8c00f0            	cpd	#240
1267  0479 2309              	bls	L344
1269  047b 8c00f9            	cpd	#249
1270  047e 2204              	bhi	L344
1271                         ; 3449                               tpTxInfoStruct[tpChannel].STMin = (canuint8)TpTxGetSTMINDefaultTime(tpChannel);
1273  0480                   LC007:
1274  0480 c601              	ldab	#1
1276  0482 2011              	bra	LC006
1277  0484                   L344:
1278                         ; 3453                               tpTxInfoStruct[tpChannel].STMin = (canuint8)( ((127/*maxSTminTime*/ + (TpTxCallCycle-1)) / TpTxCallCycle) + 1);
1280  0484 c60e              	ldab	#14
1281  0486 200d              	bra	LC006
1282  0488                   L144:
1283                         ; 3481                             tpTxInfoStruct[tpChannel].STMin = (canuint8)(((tpSTminInFrame + (TpTxCallCycle-1)) / TpTxCallCycle) + 1);
1285  0488 c30009            	addd	#9
1286  048b ce000a            	ldx	#10
1287  048e 1810              	idiv	
1288  0490 b754              	tfr	x,d
1289  0492 c30001            	addd	#1
1290  0495                   LC006:
1291  0495 7b001f            	stab	_tpTxInfoStruct+8
1292  0498                   L334:
1293                         ; 3489                       tpTxState[tpChannel].BSCounter = (tpTxInfoStruct[tpChannel].BlockSize); /* load BlockSize into Counter */
1295  0498 180c001e0010      	movb	_tpTxInfoStruct+7,L7_tpTxState+2
1296                         ; 3494                       tpTxState[tpChannel].Timer = TpTxGetSTMINtime(tpChannel);
1298  049e f6001f            	ldab	_tpTxInfoStruct+8
1299  04a1 87                	clra	
1300  04a2 7c000e            	std	L7_tpTxState
1301                         ; 3496                       tpTxState[tpChannel].engine = kTxState_WaitForTpTxCF;
1303  04a5 c603              	ldab	#3
1304  04a7 7b0011            	stab	L7_tpTxState+3
1305                         ; 3505                   break;
1308  04aa 200f              	bra	L324
1309  04ac                   L361:
1310                         ; 3508                 case kL4_FlowStatus_Wait:
1310                         ; 3509 #if (defined( TP_ENABLE_MCAN ) )
1310                         ; 3510                   if(tpTxState[tpChannel].WFTCounter == 0)
1310                         ; 3511                   { /* WFTmax wait frames are received now */
1310                         ; 3512                     __TpTxInit(tpChannel, kTpTxErrWFTmaxOverrun);   /* and go to idle state     */
1310                         ; 3513                   }
1310                         ; 3514                   tpTxState[tpChannel].WFTCounter--;                /* decr. wait frame counter */
1310                         ; 3515 #else
1310                         ; 3516                   /* WaitFrame received, set timer again */
1310                         ; 3517                   tpTxState[tpChannel].Timer = kTimeoutFC(tpChannel);
1312  04ac cc0065            	ldd	#101
1313  04af 7c000e            	std	L7_tpTxState
1314                         ; 3519                   break;
1316  04b2 2007              	bra	L324
1317  04b4                   L561:
1318                         ; 3521                 case kL4_FlowStatus_Overrun:
1318                         ; 3522                   /* Receiver reported an Overrun - terminate channel */
1318                         ; 3523                   __TpTxInit(tpChannel, kTpTxErrFCOverrun);
1320  04b4 cc0032            	ldd	#50
1321  04b7                   LC008:
1322  04b7 4a004b4b          	call	L51f_TpTxInit
1324                         ; 3524                   break;
1326  04bb                   L324:
1327                         ; 3540             return kCanNoCopyData;
1330                         ; 3550   default:
1330                         ; 3551     break;
1332                         ; 3553   return kCanNoCopyData;                
1335  04bb c7                	clrb	
1337  04bc 1b89              	leas	9,s
1338  04be 0a                	rtc	
1383                         ; 3632 canuint8 TP_API_CALL_TYPE TpTransmit(TP_MEMORY_MODEL_DATA canuint8 *ptrData, canuint16 count)
1383                         ; 3633 # else
1383                         ; 3634 canuint8 TP_API_CALL_TYPE TpTransmit(canuint8 tpChannel, TP_MEMORY_MODEL_DATA canuint8 *ptrData, canuint16 count)
1383                         ; 3635 # endif
1383                         ; 3636 {
1384                         	switch	.ftext
1385  04bf                   f_TpTransmit:
1387  04bf 3b                	pshd	
1388       00000000          OFST:	set	0
1391                         ; 3675   if (count == 0)
1394  04c0 ec85              	ldd	OFST+5,s
1395  04c2 2604              	bne	L174
1396                         ; 3677     return kTpFailed;
1398  04c4 c601              	ldab	#1
1401  04c6 31                	puly	
1402  04c7 0a                	rtc	
1403  04c8                   L174:
1404                         ; 3730   TpGlobalInterruptDisable();
1406  04c8 4a000000          	call	f_VStdSuspendAllInterrupts
1408                         ; 3767     if(tpTxState[tpChannel].engine != kTxState_Idle)
1410  04cc f60011            	ldab	L7_tpTxState+3
1411  04cf 2708              	beq	L574
1412                         ; 3769       TpGlobalInterruptRestore();
1414  04d1 4a000000          	call	f_VStdResumeAllInterrupts
1416                         ; 3770       return kTpBusy;
1418  04d5 c603              	ldab	#3
1421  04d7 31                	puly	
1422  04d8 0a                	rtc	
1423  04d9                   L574:
1424                         ; 3777     if((count) <= ((canuint16)(FRAME_LENGTH - (SF_OFFSET + FORMAT_OFFSET)))) 
1426  04d9 ec85              	ldd	OFST+5,s
1427  04db 8c0007            	cpd	#7
1428  04de 2204              	bhi	L774
1429                         ; 3779       tpTxState[tpChannel].engine = kTxState_WaitForSFConfIsr;
1431  04e0 c605              	ldab	#5
1433  04e2 2002              	bra	L105
1434  04e4                   L774:
1435                         ; 3783       tpTxState[tpChannel].engine = kTxState_WaitForFFConfIsr;
1437  04e4 c606              	ldab	#6
1438  04e6                   L105:
1439  04e6 7b0011            	stab	L7_tpTxState+3
1440                         ; 3785     tpTxState[tpChannel].Timer = TpTxConfirmationTimeout(tpChannel);
1442  04e9 cc0065            	ldd	#101
1443  04ec 7c000e            	std	L7_tpTxState
1444                         ; 3791   tpTxInfoStruct[tpChannel].DataBufferPtr = ptrData;
1446  04ef 1805800017        	movw	OFST+0,s,_tpTxInfoStruct
1447                         ; 3792   tpTxInfoStruct[tpChannel].DataIndex     = 0;
1449  04f4 18790019          	clrw	_tpTxInfoStruct+2
1450                         ; 3793   tpTxInfoStruct[tpChannel].DataLength    = count;
1452  04f8 180585001b        	movw	OFST+5,s,_tpTxInfoStruct+4
1453                         ; 3804     tpTxState[tpChannel].queued = 1;               /* set transmission request */
1455  04fd 1c001201          	bset	L7_tpTxState+4,1
1456                         ; 3809   TpGlobalInterruptRestore();  
1458  0501 4a000000          	call	f_VStdResumeAllInterrupts
1460                         ; 3810   return kTpSuccess;
1462  0505 c7                	clrb	
1465  0506 31                	puly	
1466  0507 0a                	rtc	
1490                         ; 3825 void TP_API_CALL_TYPE TpTask(void)
1490                         ; 3826 {
1491                         	switch	.ftext
1492  0508                   f_TpTask:
1496                         ; 3827   TpRxTask();
1498  0508 4a083838          	call	f_TpRxTask
1500                         ; 3828   TpTxTask();
1502  050c 4a07e3e3          	call	f_TpTxTask
1504                         ; 3829 }
1507  0510 0a                	rtc	
1721                         ; 3869 void TP_API_CALL_TYPE TpTxStateTask(void)
1721                         ; 3870 #else
1721                         ; 3871 void TP_API_CALL_TYPE TpTxStateTask(canuint8 tpChannel)
1721                         ; 3872 #endif
1721                         ; 3873 {
1722                         	switch	.ftext
1723  0511                   f_TpTxStateTask:
1725  0511 1b9d              	leas	-3,s
1726       00000003          OFST:	set	3
1729                         ; 3881   if(tpStateTaskBusy == 0)
1731  0513 f60008            	ldab	L31_tpStateTaskBusy
1732  0516 182600c4          	bne	L326
1733                         ; 3883     tpStateTaskBusy = 1;
1735  051a c601              	ldab	#1
1736  051c 7b0008            	stab	L31_tpStateTaskBusy
1737                         ; 3886     if(tpTxState[tpChannel].retransmit != 0)
1739  051f 1f00120429        	brclr	L7_tpTxState+4,4,L526
1740                         ; 3889       TpGlobalInterruptDisable();
1742  0524 4a000000          	call	f_VStdSuspendAllInterrupts
1744                         ; 3890       pTpTxState = (tvolatileTpTxState) &tpTxState[tpChannel];
1746  0528 cd000e            	ldy	#L7_tpTxState
1747  052b 6d80              	sty	OFST-3,s
1748                         ; 3891       if(pTpTxState->retransmit != 0)
1750  052d 0f440418          	brclr	4,y,4,L726
1751                         ; 3894         tpTxState[tpChannel].retransmit = 0;
1753  0531 1d001204          	bclr	L7_tpTxState+4,4
1754                         ; 3896         TpGlobalInterruptRestore();        
1756  0535 4a000000          	call	f_VStdResumeAllInterrupts
1758                         ; 3901         tpCanTxReturn = TPCANTRANSMIT(TpIntTxGetCanChannel(tpChannel)) (TP_TX_HANDLE(tpChannel));
1760  0539 cc0005            	ldd	#5
1761  053c 4a000000          	call	f_CanTransmit
1763  0540 6b82              	stab	OFST-1,s
1764                         ; 3902         if (tpCanTxReturn != kCanTxOk)
1766  0542 53                	decb	
1767  0543 18270094          	beq	L536
1768                         ; 3905           tpTxState[tpChannel].retransmit = 1;
1770  0547 205e              	bra	LC010
1771  0549                   L726:
1772                         ; 3912         TpGlobalInterruptRestore();
1775  0549 1820008a          	bra	L346
1776  054d                   L526:
1777                         ; 3918       if (tpTxState[tpChannel].queued != 0)
1779  054d f60012            	ldab	L7_tpTxState+4
1780  0550 c501              	bitb	#1
1781  0552 18270085          	beq	L536
1782                         ; 3921         TpGlobalInterruptDisable();
1784  0556 4a000000          	call	f_VStdSuspendAllInterrupts
1786                         ; 3923         pTpTxState = (tvolatileTpTxState) &tpTxState[tpChannel];
1788  055a cd000e            	ldy	#L7_tpTxState
1789  055d 6d80              	sty	OFST-3,s
1790                         ; 3924         if(pTpTxState->queued != 0)
1792  055f e644              	ldab	4,y
1793  0561 c501              	bitb	#1
1794  0563 2772              	beq	L346
1795                         ; 3927           TpIntTxTransmitChannel_IsFree(tpChannel) /* if statement */
1797  0565 f60007            	ldab	L13_tpTransmitChannel
1798  0568 52                	incb	
1799  0569 266c              	bne	L346
1800                         ; 3929             TpIntTxTransmitChannel_SetLock(tpChannel);
1802  056b 790007            	clr	L13_tpTransmitChannel
1803                         ; 3931             tpTxState[tpChannel].queued = 0;
1805  056e 1d001201          	bclr	L7_tpTxState+4,1
1806                         ; 3933             preTransmitResult = __TpTxPreCanTransmit(tpChannel);
1808  0572 4a068d8d          	call	L12f_TpTxPreCanTransmit
1810  0576 6c80              	std	OFST-3,s
1811                         ; 3934             if (preTransmitResult == kTpSuccess)
1813  0578 2633              	bne	L546
1814                         ; 3940               TPTXSETID(tpChannel);
1816  057a b6001d            	ldaa	_tpTxInfoStruct+6
1817  057d c300c2            	addd	#194
1818  0580 3b                	pshd	
1819  0581 b60016            	ldaa	L3_tpHighByte29Bit+1
1820  0584 c7                	clrb	
1821  0585 b746              	tfr	d,y
1822  0587 18fa0013          	ory	L5_tpPGN29Bit
1823  058b 35                	pshy	
1824  058c cc0005            	ldd	#5
1825  058f 4a000000          	call	f_CanDynTxObjSetExtId
1827  0593 1b84              	leas	4,s
1828                         ; 3942               tpCanTxReturn = TPCANTRANSMIT(TpIntTxGetCanChannel(tpChannel)) (TP_TX_HANDLE(tpChannel));
1831  0595 cc0005            	ldd	#5
1832  0598 4a000000          	call	f_CanTransmit
1834  059c 6b82              	stab	OFST-1,s
1835                         ; 3944               TpGlobalInterruptRestore();
1837  059e 4a000000          	call	f_VStdResumeAllInterrupts
1839                         ; 3946               if (tpCanTxReturn != kCanTxOk)
1841  05a2 e682              	ldab	OFST-1,s
1842  05a4 040134            	dbeq	b,L536
1843                         ; 3949                 tpTxState[tpChannel].retransmit = 1;
1845  05a7                   LC010:
1846  05a7 1c001204          	bset	L7_tpTxState+4,4
1847  05ab 202e              	bra	L536
1848  05ad                   L546:
1849                         ; 3958               if ((tpTxState[tpChannel].engine == kTxState_WaitForCFConfIsr)     || 
1849                         ; 3959                   (tpTxState[tpChannel].engine == kTxState_WaitForLastCFConfIsr) ||
1849                         ; 3960                   (tpTxState[tpChannel].engine == kTxState_WaitForFFConfIsr) 
1849                         ; 3961                  )
1851  05ad f60011            	ldab	L7_tpTxState+3
1852  05b0 c107              	cmpb	#7
1853  05b2 2708              	beq	L556
1855  05b4 c108              	cmpb	#8
1856  05b6 2704              	beq	L556
1858  05b8 c106              	cmpb	#6
1859  05ba 2612              	bne	L356
1860  05bc                   L556:
1861                         ; 3963                 if(tpTxState[tpChannel].engine == kTxState_WaitForFFConfIsr) 
1863  05bc c106              	cmpb	#6
1864  05be 2606              	bne	L166
1865                         ; 3965                   tpTxInfoStruct[tpChannel].DataIndex = 0;
1867  05c0 18790019          	clrw	_tpTxInfoStruct+2
1869  05c4 2008              	bra	L356
1870  05c6                   L166:
1871                         ; 3969                   tpTxInfoStruct[tpChannel].DataIndex = (tpTxInfoStruct[tpChannel].DataIndex) - (canuint16)(FRAME_LENGTH - (CF_OFFSET + FORMAT_OFFSET));
1873  05c6 fd0019            	ldy	_tpTxInfoStruct+2
1874  05c9 1959              	leay	-7,y
1875  05cb 7d0019            	sty	_tpTxInfoStruct+2
1876  05ce                   L356:
1877                         ; 3974               TpIntTxTransmitChannel_SetFree(tpChannel);
1879  05ce c6ff              	ldab	#255
1880  05d0 7b0007            	stab	L13_tpTransmitChannel
1881                         ; 3975               tpTxState[tpChannel].queued = 1;
1883  05d3 1c001201          	bset	L7_tpTxState+4,1
1884                         ; 3977               TpGlobalInterruptRestore();
1887  05d7                   L346:
1888                         ; 3985             TpGlobalInterruptRestore();
1891                         ; 3993           TpGlobalInterruptRestore();
1893  05d7 4a000000          	call	f_VStdResumeAllInterrupts
1895  05db                   L536:
1896                         ; 3999     tpStateTaskBusy = 0;
1898  05db 790008            	clr	L31_tpStateTaskBusy
1899  05de                   L326:
1900                         ; 4002 }
1903  05de 1b83              	leas	3,s
1904  05e0 0a                	rtc	
2085                         ; 4043 void TP_API_CALL_TYPE TpRxStateTask(void)
2085                         ; 4044 # else
2085                         ; 4045 void TP_API_CALL_TYPE TpRxStateTask(canuint8 tpChannel)
2085                         ; 4046 # endif
2085                         ; 4047 {
2086                         	switch	.ftext
2087  05e1                   f_TpRxStateTask:
2089  05e1 1b9d              	leas	-3,s
2090       00000003          OFST:	set	3
2093                         ; 4056   if(tpStateTaskBusy == 0)
2095  05e3 f60008            	ldab	L31_tpStateTaskBusy
2096  05e6 182600a0          	bne	L767
2097                         ; 4058     tpStateTaskBusy = 1;
2099  05ea c601              	ldab	#1
2100  05ec 7b0008            	stab	L31_tpStateTaskBusy
2101                         ; 4061     if(tpRxState[tpChannel].retransmit != 0)
2103  05ef 1f000d0229        	brclr	L11_tpRxState+4,2,L177
2104                         ; 4064       TpGlobalInterruptDisable();
2106  05f4 4a000000          	call	f_VStdSuspendAllInterrupts
2108                         ; 4065       pTpRxState = (tvolatileTpRxState) &tpRxState[tpChannel];
2110  05f8 cd0009            	ldy	#L11_tpRxState
2111  05fb 6d80              	sty	OFST-3,s
2112                         ; 4066       if(pTpRxState->retransmit != 0)
2114  05fd 0f44021a          	brclr	4,y,2,L377
2115                         ; 4069         tpRxState[tpChannel].retransmit = 0;
2117  0601 1d000d02          	bclr	L11_tpRxState+4,2
2118                         ; 4070         TpGlobalInterruptRestore();
2120  0605 4a000000          	call	f_VStdResumeAllInterrupts
2122                         ; 4074         tpCanTxReturn = TPCANTRANSMIT(TpIntRxGetCanChannel(tpChannel)) (TP_RX_HANDLE(tpChannel)); 
2124  0609 cc0005            	ldd	#5
2125  060c 4a000000          	call	f_CanTransmit
2127  0610 6b82              	stab	OFST-1,s
2128                         ; 4075         if ( tpCanTxReturn != kCanTxOk )
2130  0612 53                	decb	
2131  0613 2772              	beq	L1001
2132                         ; 4078           tpRxState[tpChannel].retransmit = 1;
2134  0615 1c000d02          	bset	L11_tpRxState+4,2
2135  0619 206c              	bra	L1001
2136  061b                   L377:
2137                         ; 4085         TpGlobalInterruptRestore();
2140  061b 2066              	bra	L5101
2141  061d                   L177:
2142                         ; 4091       if (tpRxState[tpChannel].queued != 0)
2144  061d f6000d            	ldab	L11_tpRxState+4
2145  0620 c501              	bitb	#1
2146  0622 2763              	beq	L1001
2147                         ; 4094         TpGlobalInterruptDisable();
2149  0624 4a000000          	call	f_VStdSuspendAllInterrupts
2151                         ; 4095         pTpRxState = (tvolatileTpRxState)&tpRxState[tpChannel];
2153  0628 cd0009            	ldy	#L11_tpRxState
2154  062b 6d80              	sty	OFST-3,s
2155                         ; 4096         if(pTpRxState->queued != 0)
2157  062d 0f440152          	brclr	4,y,1,L5101
2158                         ; 4110           TpIntRxTransmitChannel_IsFree(tpChannel)
2161  0631 f60007            	ldab	L13_tpTransmitChannel
2162  0634 04a14c            	ibne	b,L5101
2163                         ; 4112             TpIntRxTransmitChannel_SetLock(tpChannel);
2165  0637 c680              	ldab	#128
2166  0639 7b0007            	stab	L13_tpTransmitChannel
2167                         ; 4113             tpRxState[tpChannel].queued = 0;
2169  063c 1d000d01          	bclr	L11_tpRxState+4,1
2170                         ; 4152             if (tpRxState[tpChannel].engine == kRxState_WaitForFCOverConfIsr)
2172  0640 f6000c            	ldab	L11_tpRxState+3
2173  0643 c106              	cmpb	#6
2174  0645 2604              	bne	L1101
2175                         ; 4154               TpCanTxFCDataPtr(tpChannel)[ (TPCI_OFFSET + FORMAT_OFFSET) ]  = kL4_FlowStatus_Overrun;
2177  0647 c632              	ldab	#50
2179  0649 2002              	bra	L3101
2180  064b                   L1101:
2181                         ; 4171                 TpCanTxFCDataPtr(tpChannel)[ (TPCI_OFFSET + FORMAT_OFFSET) ]    = kL4_FlowControl;
2183  064b c630              	ldab	#48
2184  064d                   L3101:
2185  064d 7b0000            	stab	_TxDynamicMsg0
2186                         ; 4177               TpCanTxFCDataPtr(tpChannel)[(BS_OFFSET + FORMAT_OFFSET)]    = TpRxGetBlockSize(tpChannel);
2188                         ; 4178               TpCanTxFCDataPtr(tpChannel)[(STMIN_OFFSET + FORMAT_OFFSET)] = TpRxGetSTMINtime(tpChannel);     
2190  0650 87                	clra	
2191  0651 c7                	clrb	
2192  0652 7c0001            	std	_TxDynamicMsg0+1
2193                         ; 4180               TpRxReloadBSCnt(tpChannel);
2195  0655 7a000b            	staa	L11_tpRxState+2
2196                         ; 4185             TPRXSETID(tpChannel);
2198  0658 b60027            	ldaa	_tpRxInfoStruct+6
2199  065b c300c2            	addd	#194
2200  065e 3b                	pshd	
2201  065f b60016            	ldaa	L3_tpHighByte29Bit+1
2202  0662 c7                	clrb	
2203  0663 b746              	tfr	d,y
2204  0665 18fa0013          	ory	L5_tpPGN29Bit
2205  0669 35                	pshy	
2206  066a cc0005            	ldd	#5
2207  066d 4a000000          	call	f_CanDynTxObjSetExtId
2209  0671 1b84              	leas	4,s
2210                         ; 4187             tpCanTxReturn = ((TPCANTRANSMIT(TpIntRxGetCanChannel(tpChannel))) (TP_RX_HANDLE(tpChannel)));
2213  0673 cc0005            	ldd	#5
2214  0676 4a000000          	call	f_CanTransmit
2216  067a 6b82              	stab	OFST-1,s
2217                         ; 4188             if ( tpCanTxReturn != kCanTxOk )
2219  067c 040104            	dbeq	b,L5101
2220                         ; 4191               tpRxState[tpChannel].retransmit = 1;
2222  067f 1c000d02          	bset	L11_tpRxState+4,2
2223  0683                   L5101:
2224                         ; 4194             TpGlobalInterruptRestore();
2228                         ; 4201             TpGlobalInterruptRestore();
2231                         ; 4209           TpGlobalInterruptRestore();
2233  0683 4a000000          	call	f_VStdResumeAllInterrupts
2235  0687                   L1001:
2236                         ; 4215     tpStateTaskBusy = 0;
2238  0687 790008            	clr	L31_tpStateTaskBusy
2239  068a                   L767:
2240                         ; 4218 } /* TpRxStateTask */
2243  068a 1b83              	leas	3,s
2244  068c 0a                	rtc	
2332                         ; 4234 static canuintCPUtype TpTxPreCanTransmit(void)
2332                         ; 4235 #else
2332                         ; 4236 static canuintCPUtype TpTxPreCanTransmit(canuintCPUtype tpChannel)
2332                         ; 4237 #endif
2332                         ; 4238 {
2333                         	switch	.ftext
2334  068d                   L12f_TpTxPreCanTransmit:
2336  068d 1b97              	leas	-9,s
2337       00000009          OFST:	set	9
2340                         ; 4258   tpCopyToCanInfoStruct.pSource = &tpTxInfoStruct[tpChannel].DataBufferPtr[tpTxInfoStruct[tpChannel].DataIndex]; 
2342  068f fc0017            	ldd	_tpTxInfoStruct
2343  0692 f30019            	addd	_tpTxInfoStruct+2
2344  0695 6c83              	std	OFST-6,s
2345                         ; 4260   switch(tpTxState[tpChannel].engine)
2347  0697 f60011            	ldab	L7_tpTxState+3
2349  069a c005              	subb	#5
2350  069c 270f              	beq	L3201
2351  069e 04011e            	dbeq	b,L5201
2352  06a1 040149            	dbeq	b,L7201
2353  06a4 040146            	dbeq	b,L7201
2354                         ; 4371     return kTpFailed; /* do not proceed with wrong state */
2357  06a7 cc0001            	ldd	#1
2360  06aa 1b89              	leas	9,s
2361  06ac 0a                	rtc	
2362  06ad                   L3201:
2363                         ; 4262   case kTxState_WaitForSFConfIsr:
2363                         ; 4263     /*-----------------------------------------------------------------------------*/
2363                         ; 4264     /* NAME:              TpAssembleSF / SingleFrame                               */
2363                         ; 4265     /* DESCRIPTION:       Assemble a Transmit SingleFrame                          */
2363                         ; 4266     /*-----------------------------------------------------------------------------*/
2363                         ; 4267     TpCanTxDataPtr(tpChannel)[(TPCI_OFFSET + FORMAT_OFFSET)] = (canuint8)(tpTxInfoStruct[tpChannel].DataLength); /*kL4_SingleFrame TPCI is 0x00*/
2365  06ad 180c001c0000      	movb	_tpTxInfoStruct+5,_TxDynamicMsg0
2366                         ; 4271     tpCopyToCanInfoStruct.pDestination  = (TxDataPtr)(&(TpCanTxDataPtr(tpChannel)[(SF_OFFSET + FORMAT_OFFSET)]));
2368  06b3 cc0001            	ldd	#_TxDynamicMsg0+1
2369  06b6 6c81              	std	OFST-8,s
2370                         ; 4272     tpCopyToCanInfoStruct.Length        = tpTxInfoStruct[tpChannel].DataLength;
2372  06b8 180185001b        	movw	_tpTxInfoStruct+4,OFST-4,s
2373                         ; 4281     break;
2375  06bd 205f              	bra	L5701
2376  06bf                   L5201:
2377                         ; 4297       TpCanTxDataPtr(tpChannel)[(TPCI_OFFSET + FORMAT_OFFSET)] = (canuint8)kL4_FirstFrame;
2379  06bf c610              	ldab	#16
2380  06c1 7b0000            	stab	_TxDynamicMsg0
2381                         ; 4299     TpCanTxDataPtr(tpChannel)[(TPCI_OFFSET + FORMAT_OFFSET)] |= (canuint8)((tpTxInfoStruct[tpChannel].DataLength & 0x0F00)>>8);
2383  06c4 b6001b            	ldaa	_tpTxInfoStruct+4
2384  06c7 840f              	anda	#15
2385  06c9 b701              	tfr	a,b
2386  06cb fa0000            	orab	_TxDynamicMsg0
2387  06ce 7b0000            	stab	_TxDynamicMsg0
2388                         ; 4300     TpCanTxDataPtr(tpChannel)[(DL_OFFSET   + FORMAT_OFFSET)]  = (canuint8)( tpTxInfoStruct[tpChannel].DataLength & 0x00FF);
2390  06d1 180c001c0001      	movb	_tpTxInfoStruct+5,_TxDynamicMsg0+1
2391                         ; 4306     tpCopyToCanInfoStruct.pDestination = (TxDataPtr)(&(TpCanTxDataPtr(tpChannel)[(FF_OFFSET + FORMAT_OFFSET)]));
2393  06d7 cc0002            	ldd	#_TxDynamicMsg0+2
2394  06da 6c81              	std	OFST-8,s
2395                         ; 4307     tpCopyToCanInfoStruct.Length       = (canuint16)(FRAME_LENGTH - (FF_OFFSET + FORMAT_OFFSET));
2397  06dc cc0006            	ldd	#6
2398  06df 6c85              	std	OFST-4,s
2399                         ; 4309     tpTxInfoStruct[tpChannel].DataIndex = (canuint16)(FRAME_LENGTH - (FF_OFFSET + FORMAT_OFFSET));    /*Set tx index to next free data element*/
2401  06e1 7c0019            	std	_tpTxInfoStruct+2
2402                         ; 4317     tpTxInfoStruct[tpChannel].sequencenumber = 0;   /*SN set to zero*/
2404  06e4 1d00200f          	bclr	_tpTxInfoStruct+9,15
2405                         ; 4318     tpTxState[tpChannel].BSCounter = 0;   /*Neccessary for correct setting of next state kTxState_WaitFC*/
2407  06e8 790010            	clr	L7_tpTxState+2
2408                         ; 4319     break;
2410  06eb 2031              	bra	L5701
2411  06ed                   L7201:
2412                         ; 4337       TpCanTxDataPtr(tpChannel)[(TPCI_OFFSET + FORMAT_OFFSET)] = (canuint8)kL4_ConsecutiveFrame;
2414  06ed c620              	ldab	#32
2415  06ef 7b0000            	stab	_TxDynamicMsg0
2416                         ; 4339     TpCanTxDataPtr(tpChannel)[(TPCI_OFFSET + FORMAT_OFFSET)] |= (canuint8)tpTxInfoStruct[tpChannel].sequencenumber;
2418  06f2 f60020            	ldab	_tpTxInfoStruct+9
2419  06f5 c40f              	andb	#15
2420  06f7 fa0000            	orab	_TxDynamicMsg0
2421  06fa 7b0000            	stab	_TxDynamicMsg0
2422                         ; 4343     tpCopyToCanInfoStruct.pDestination = (TxDataPtr)(&(TpCanTxDataPtr(tpChannel)[(CF_OFFSET + FORMAT_OFFSET)]));
2424  06fd cc0001            	ldd	#_TxDynamicMsg0+1
2425  0700 6c81              	std	OFST-8,s
2426                         ; 4344     if (tpTxState[tpChannel].engine == kTxState_WaitForLastCFConfIsr)
2428  0702 f60011            	ldab	L7_tpTxState+3
2429  0705 c108              	cmpb	#8
2430  0707 2608              	bne	L7701
2431                         ; 4346       tpCopyToCanInfoStruct.Length = (canuint16)(tpTxInfoStruct[tpChannel].DataLength-tpTxInfoStruct[tpChannel].DataIndex);
2433  0709 fc001b            	ldd	_tpTxInfoStruct+4
2434  070c b30019            	subd	_tpTxInfoStruct+2
2436  070f 2003              	bra	L1011
2437  0711                   L7701:
2438                         ; 4357       tpCopyToCanInfoStruct.Length = (canuint16)(FRAME_LENGTH - (CF_OFFSET + FORMAT_OFFSET));
2440  0711 cc0007            	ldd	#7
2441  0714                   L1011:
2442  0714 6c85              	std	OFST-4,s
2443                         ; 4366     tpTxInfoStruct[tpChannel].DataIndex = tpTxInfoStruct[tpChannel].DataIndex + (canuint16)(FRAME_LENGTH - (CF_OFFSET + FORMAT_OFFSET));   /*Set tx index to next free data element*/
2445  0716 fd0019            	ldy	_tpTxInfoStruct+2
2446  0719 1947              	leay	7,y
2447  071b 7d0019            	sty	_tpTxInfoStruct+2
2448                         ; 4367     break;
2450  071e                   L5701:
2451                         ; 4398   rc = __ApplTpTxCopyToCAN(&tpCopyToCanInfoStruct);
2453  071e b774              	tfr	s,d
2454  0720 4a000000          	call	f_DescCopyToCAN
2456  0724 87                	clra	
2457                         ; 4407   return rc;
2461  0725 1b89              	leas	9,s
2462  0727 0a                	rtc	
2496                         ; 4481 void TP_INTERNAL_CALL_TYPE TpDrvConfirmation(CanTransmitHandle txObject)
2496                         ; 4482 {
2497                         	switch	.ftext
2498  0728                   f_TpDrvConfirmation:
2502                         ; 4485   (void)txObject;
2504                         ; 4525   TpIntTxTransmitChannel_IsTx(tpChannel)
2506  0728 f60007            	ldab	L13_tpTransmitChannel
2507  072b 2605              	bne	L7111
2508                         ; 4528     __TpTxPostProcessing(tpChannel);
2510  072d 4a073737          	call	L52f_TpTxPostProcessing
2514  0731 0a                	rtc	
2515  0732                   L7111:
2516                         ; 4541     __TpRxPostProcessing(tpChannel);
2518  0732 4a078e8e          	call	L72f_TpRxPostProcessing
2520                         ; 4553 }
2523  0736 0a                	rtc	
2549                         ; 4575 static void TP_INTERNAL_INLINE TpTxPostProcessing(void)
2549                         ; 4576 #else
2549                         ; 4577 static void TP_INTERNAL_INLINE TpTxPostProcessing(canuintCPUtype tpChannel)
2549                         ; 4578 #endif
2549                         ; 4579 {
2550                         	switch	.ftext
2551  0737                   L52f_TpTxPostProcessing:
2555                         ; 4589     switch(tpTxState[tpChannel].engine)
2557  0737 f60011            	ldab	L7_tpTxState+3
2559  073a c005              	subb	#5
2560  073c 270a              	beq	L3211
2561  073e 040116            	dbeq	b,L5211
2562  0741 040124            	dbeq	b,L7211
2563  0744 040101            	dbeq	b,L3211
2564                         ; 4698     default: 
2564                         ; 4699       /* default case - leave function without any changes to internal states */
2564                         ; 4700       return;
2567  0747 0a                	rtc	
2568  0748                   L3211:
2569                         ; 4604         __ApplTpTxConfirmation((canuint8)tpChannel, kTpSuccess);
2573  0748 87                	clra	
2574  0749 c7                	clrb	
2575  074a 4a000000          	call	f_DescConfirmation
2577                         ; 4622           tpTxState[tpChannel].Timer      = 0;
2579  074e 1879000e          	clrw	L7_tpTxState
2580                         ; 4623           tpTxState[tpChannel].engine     = kTxState_Idle;
2582  0752 790011            	clr	L7_tpTxState+3
2583                         ; 4646       break;
2585  0755 2031              	bra	L5411
2586  0757                   L5211:
2587                         ; 4655         tpTxState[tpChannel].Timer = kTimeoutFC(tpChannel);
2589  0757 cc0065            	ldd	#101
2590  075a 7c000e            	std	L7_tpTxState
2591                         ; 4656         tpTxState[tpChannel].engine = kTxState_WaitFC;
2593  075d c602              	ldab	#2
2594  075f 7b0011            	stab	L7_tpTxState+3
2595                         ; 4659         tpTxState[tpChannel].firstFC = 0; /* Calculate BS/STmin only out of the first received FC */
2597  0762 1d001208          	bclr	L7_tpTxState+4,8
2598                         ; 4673       break;
2601  0766 2020              	bra	L5411
2602  0768                   L7211:
2603                         ; 4683       if(tpTxState[tpChannel].BSCounter != 0)
2606  0768 f70010            	tst	L7_tpTxState+2
2607  076b 270f              	beq	L7411
2608                         ; 4685         tpTxState[tpChannel].BSCounter--;
2610  076d 730010            	dec	L7_tpTxState+2
2611                         ; 4686         if(tpTxState[tpChannel].BSCounter == 0)
2613  0770 260a              	bne	L7411
2614                         ; 4688           tpTxState[tpChannel].Timer = kTimeoutFC(tpChannel);
2616  0772 cc0065            	ldd	#101
2617  0775 7c000e            	std	L7_tpTxState
2618                         ; 4689           tpTxState[tpChannel].engine = kTxState_WaitFC;
2620  0778 c602              	ldab	#2
2621                         ; 4690           break;
2623  077a 2009              	bra	LC012
2624  077c                   L7411:
2625                         ; 4694         tpTxState[tpChannel].Timer = TpTxGetSTMINtime(tpChannel);
2627  077c f6001f            	ldab	_tpTxInfoStruct+8
2628  077f 87                	clra	
2629  0780 7c000e            	std	L7_tpTxState
2630                         ; 4695         tpTxState[tpChannel].engine = kTxState_WaitForTpTxCF;
2632  0783 c603              	ldab	#3
2633  0785                   LC012:
2634  0785 7b0011            	stab	L7_tpTxState+3
2635                         ; 4697       break;
2637  0788                   L5411:
2638                         ; 4704     TpIntTxTransmitChannel_SetFree(tpChannel);
2640  0788 c6ff              	ldab	#255
2641  078a 7b0007            	stab	L13_tpTransmitChannel
2642                         ; 4715 }
2646  078d 0a                	rtc	
2670                         ; 4732 static void TP_INTERNAL_INLINE TpRxPostProcessing(void)
2670                         ; 4733 # else
2670                         ; 4734 static void TP_INTERNAL_INLINE TpRxPostProcessing(canuintCPUtype tpChannel)
2670                         ; 4735 # endif
2670                         ; 4736 {
2671                         	switch	.ftext
2672  078e                   L72f_TpRxPostProcessing:
2676                         ; 4747     switch(tpRxState[tpChannel].engine)
2678  078e f6000c            	ldab	L11_tpRxState+3
2680  0791 c005              	subb	#5
2681  0793 2704              	beq	L3511
2682  0795 04010e            	dbeq	b,L5511
2683                         ; 4778     default:
2683                         ; 4779       return;
2686  0798 0a                	rtc	
2687  0799                   L3511:
2688                         ; 4761         tpRxState[tpChannel].Timer  = kTimeoutCF(tpChannel);
2690  0799 cc0065            	ldd	#101
2691  079c 7c0009            	std	L11_tpRxState
2692                         ; 4762         tpRxState[tpChannel].engine = kRxState_WaitCF;
2694  079f c603              	ldab	#3
2695  07a1 7b000c            	stab	L11_tpRxState+3
2696                         ; 4767       break;
2698  07a4 2007              	bra	L3711
2699  07a6                   L5511:
2700                         ; 4770     case kRxState_WaitForFCOverConfIsr:
2700                         ; 4771       /* Stop receiving frames on this tpChannel */
2700                         ; 4772       tpRxState[tpChannel].Timer  = 0;
2702  07a6 18790009          	clrw	L11_tpRxState
2703                         ; 4773       tpRxState[tpChannel].engine = kRxState_Idle;
2705  07aa 79000c            	clr	L11_tpRxState+3
2706                         ; 4774       break;
2708  07ad                   L3711:
2709                         ; 4782     TpIntRxTransmitChannel_SetFree(tpChannel);
2711  07ad c6ff              	ldab	#255
2712  07af 7b0007            	stab	L13_tpTransmitChannel
2713                         ; 4792 }
2717  07b2 0a                	rtc	
2742                         ; 4864 static TP_INTERNAL_INLINE void TpTxInternalPrepareOfCF(void)
2742                         ; 4865 # else
2742                         ; 4866 static TP_INTERNAL_INLINE void TpTxInternalPrepareOfCF(canuintCPUtype tpChannel)
2742                         ; 4867 # endif
2742                         ; 4868 {
2743                         	switch	.ftext
2744  07b3                   L32f_TpTxInternalPrepareOfCF:
2748                         ; 4871   tpTxInfoStruct[tpChannel].sequencenumber = CANBITTYPE_CAST(tpTxInfoStruct[tpChannel].sequencenumber + 1); /*SN increment*/
2750  07b3 f60020            	ldab	_tpTxInfoStruct+9
2751  07b6 c40f              	andb	#15
2752  07b8 52                	incb	
2753  07b9 1d00200f          	bclr	_tpTxInfoStruct+9,15
2754  07bd c40f              	andb	#15
2755  07bf fa0020            	orab	_tpTxInfoStruct+9
2756  07c2 7b0020            	stab	_tpTxInfoStruct+9
2757                         ; 4874   if((tpTxInfoStruct[tpChannel].DataIndex + ((canuint16)(FRAME_LENGTH - (CF_OFFSET + FORMAT_OFFSET))) ) >= tpTxInfoStruct[tpChannel].DataLength)
2759  07c5 fd0019            	ldy	_tpTxInfoStruct+2
2760  07c8 1947              	leay	7,y
2761  07ca bd001b            	cpy	_tpTxInfoStruct+4
2762  07cd 2504              	blo	L5021
2763                         ; 4876     tpTxState[tpChannel].engine = kTxState_WaitForLastCFConfIsr;
2765  07cf c608              	ldab	#8
2767  07d1 2002              	bra	L7021
2768  07d3                   L5021:
2769                         ; 4880     tpTxState[tpChannel].engine = kTxState_WaitForCFConfIsr;
2771  07d3 c607              	ldab	#7
2772  07d5                   L7021:
2773  07d5 7b0011            	stab	L7_tpTxState+3
2774                         ; 4882   tpTxState[tpChannel].Timer = TpTxConfirmationTimeout(tpChannel);
2776  07d8 cc0065            	ldd	#101
2777  07db 7c000e            	std	L7_tpTxState
2778                         ; 4885   tpTxState[tpChannel].queued = 1;
2780  07de 1c001201          	bset	L7_tpTxState+4,1
2781                         ; 4886 }
2784  07e2 0a                	rtc	
2826                         ; 4900 void TP_API_CALL_TYPE TpTxTask(void)
2826                         ; 4901 {
2827                         	switch	.ftext
2828  07e3                   f_TpTxTask:
2830  07e3 3b                	pshd	
2831       00000002          OFST:	set	2
2834                         ; 4914     if (tpTxState[tpChannel].Timer != 0)
2836  07e4 fc000e            	ldd	L7_tpTxState
2837  07e7 274d              	beq	L7521
2838                         ; 4916       TpGlobalInterruptDisable();      
2840  07e9 4a000000          	call	f_VStdSuspendAllInterrupts
2842                         ; 4917       pTpTxState = (tvolatileTpTxState)&tpTxState[tpChannel];
2844  07ed cd000e            	ldy	#L7_tpTxState
2845  07f0 6d80              	sty	OFST-2,s
2846                         ; 4918       if(pTpTxState->Timer != 0)
2848  07f2 18e740            	tstw	0,y
2849  07f5 273b              	beq	L7121
2850                         ; 4920         tpTxState[tpChannel].Timer--;
2852  07f7 1873000e          	decw	L7_tpTxState
2853                         ; 4922         if(tpTxState[tpChannel].Timer == 0)
2855  07fb 2624              	bne	LC014
2856                         ; 4924           switch(tpTxState[tpChannel].engine)
2858  07fd f60011            	ldab	L7_tpTxState+3
2860  0800 87                	clra	
2861  0801 c002              	subb	#2
2862  0803 c107              	cmpb	#7
2863  0805 242b              	bhs	L7121
2864  0807 59                	lsld	
2865  0808 05ff              	jmp	[d,pc]
2866  080a 0818              	dc.w	L1121
2867  080c 081d              	dc.w	L3121
2868  080e 0832              	dc.w	L7121
2869  0810 082b              	dc.w	L5121
2870  0812 082b              	dc.w	L5121
2871  0814 082b              	dc.w	L5121
2872  0816 082b              	dc.w	L5121
2873  0818                   L1121:
2874                         ; 4926           case kTxState_WaitFC:     /*FC Timeout occured!*/
2874                         ; 4927             __TpTxInit(tpChannel, kTpTxErrFCTimeout);
2876  0818 cc0023            	ldd	#35
2878                         ; 4928             TpGlobalInterruptRestore();
2881                         ; 4929             break;
2883  081b 2011              	bra	LC015
2884  081d                   L3121:
2885                         ; 4930           case kTxState_WaitForTpTxCF:
2885                         ; 4931             __TpTxInternalPrepareOfCF(tpChannel);
2887  081d 4a07b3b3          	call	L32f_TpTxInternalPrepareOfCF
2889                         ; 4932             TpGlobalInterruptRestore();
2892                         ; 4934             __TpTxStateTask((canuint8)tpChannel);
2894  0821                   LC014:
2895  0821 4a000000          	call	f_VStdResumeAllInterrupts
2896  0825 4a051111          	call	f_TpTxStateTask
2898                         ; 4935             break;
2900  0829 200b              	bra	L7521
2901  082b                   L5121:
2902                         ; 4937           case kTxState_WaitForSFConfIsr:
2902                         ; 4938           case kTxState_WaitForFFConfIsr:               
2902                         ; 4939           case kTxState_WaitForCFConfIsr:               
2902                         ; 4940           case kTxState_WaitForLastCFConfIsr:
2902                         ; 4941             /* Timeout, apparently no access to bus*/
2902                         ; 4942             __TpTxInit(tpChannel, kTpTxErrConfIntTimeout);
2904  082b cc0022            	ldd	#34
2905  082e                   LC015:
2906  082e 4a004b4b          	call	L51f_TpTxInit
2908                         ; 4943             TpGlobalInterruptRestore();
2911                         ; 4944             break;
2913  0832                   L7121:
2914                         ; 4988           default:
2914                         ; 4989             TpGlobalInterruptRestore();
2917                         ; 4991             break;
2921                         ; 4996           TpGlobalInterruptRestore();
2924                         ; 4998           __TpTxStateTask((canuint8)tpChannel);
2927                         ; 5003         TpGlobalInterruptRestore();
2929  0832 4a000000          	call	f_VStdResumeAllInterrupts
2931  0836                   L7521:
2932                         ; 5013 } /* TpTxTask */
2935  0836 31                	puly	
2936  0837 0a                	rtc	
2977                         ; 5027 void TP_API_CALL_TYPE TpRxTask(void)
2977                         ; 5028 {
2978                         	switch	.ftext
2979  0838                   f_TpRxTask:
2981  0838 3b                	pshd	
2982       00000002          OFST:	set	2
2985                         ; 5037     if (tpRxState[tpChannel].Timer != 0)
2987  0839 fc0009            	ldd	L11_tpRxState
2988  083c 2742              	beq	L7031
2989                         ; 5039       TpGlobalInterruptDisable();
2991  083e 4a000000          	call	f_VStdSuspendAllInterrupts
2993                         ; 5040       pTpRxState = (tvolatileTpRxState) &tpRxState[tpChannel];
2995  0842 cc0009            	ldd	#L11_tpRxState
2996  0845 6c80              	std	OFST-2,s
2997                         ; 5041       if(pTpRxState->Timer != 0)
2999  0847 18e7f30000        	tstw	[OFST-2,s]
3000  084c 272e              	beq	L1131
3001                         ; 5043         tpRxState[tpChannel].Timer--;
3003  084e 18730009          	decw	L11_tpRxState
3004                         ; 5045         if(tpRxState[tpChannel].Timer == 0)
3006  0852 261e              	bne	L3131
3007                         ; 5047           switch(tpRxState[tpChannel].engine)
3009  0854 f6000c            	ldab	L11_tpRxState+3
3011  0857 c003              	subb	#3
3012  0859 2709              	beq	L1621
3013  085b c002              	subb	#2
3014  085d 270a              	beq	L3621
3015  085f 040107            	dbeq	b,L3621
3016                         ; 5090           default:
3016                         ; 5091             TpGlobalInterruptRestore();
3019                         ; 5093             break;
3022  0862 2018              	bra	L1131
3023  0864                   L1621:
3024                         ; 5050           case kRxState_WaitCF:
3024                         ; 5051             __TpRxInit(tpChannel, kTpRxErrCFTimeout);     /* and go to idle state       */
3026  0864 cc0003            	ldd	#3
3028                         ; 5052             TpGlobalInterruptRestore();
3031                         ; 5053             break;
3033  0867 2003              	bra	LC017
3034  0869                   L3621:
3035                         ; 5055           case kRxState_WaitForFCConfIsr:
3035                         ; 5056 # if defined (TP_ENABLE_ISO_15765_2_2)
3035                         ; 5057           case kRxState_WaitForFCOverConfIsr:
3035                         ; 5058 # endif
3035                         ; 5059 #endif
3035                         ; 5060 
3035                         ; 5061 #if (defined( TP_ENABLE_MF_RECEPTION ) || defined(TP_ENABLE_SF_ACKNOWLEDGE) )
3035                         ; 5062             /* Timeout, apparently no access to bus*/
3035                         ; 5063             __TpRxInit(tpChannel, kTpRxErrConfIntTimeout); /* and go to idle state       */
3037  0869 cc0004            	ldd	#4
3038  086c                   LC017:
3039  086c 4a00a5a5          	call	L71f_TpRxInit
3041                         ; 5064             TpGlobalInterruptRestore();
3044                         ; 5065             break;
3046  0870 200a              	bra	L1131
3047                         ; 5093             break;
3048  0872                   L3131:
3049                         ; 5098           TpGlobalInterruptRestore();
3051  0872 4a000000          	call	f_VStdResumeAllInterrupts
3053                         ; 5101           __TpRxStateTask((canuint8)tpChannel);
3055  0876 4a05e1e1          	call	f_TpRxStateTask
3057  087a 2004              	bra	L7031
3058  087c                   L1131:
3059                         ; 5107         TpGlobalInterruptRestore();
3061  087c 4a000000          	call	f_VStdResumeAllInterrupts
3063  0880                   L7031:
3064                         ; 5111 } /* TpRxTask */
3067  0880 31                	puly	
3068  0881 0a                	rtc	
3092                         ; 5289 canuint8 TP_API_CALL_TYPE TpRxGetSourceAddress(void)
3092                         ; 5290 # else
3092                         ; 5291 canuint8 TP_API_CALL_TYPE TpRxGetSourceAddress(canuint8 tpChannel)
3092                         ; 5292 # endif
3092                         ; 5293 {
3093                         	switch	.ftext
3094  0882                   f_TpRxGetSourceAddress:
3098                         ; 5299   return((canuint8)tpRxInfoStruct[tpChannel].SourceAddress);
3101  0882 f60027            	ldab	_tpRxInfoStruct+6
3104  0885 0a                	rtc	
3127                         ; 5392 canuint8 TP_API_CALL_TYPE TpRxGetStatus(void)
3127                         ; 5393 #else
3127                         ; 5394 canuint8 TP_API_CALL_TYPE TpRxGetStatus(canuint8 tpChannel)
3127                         ; 5395 #endif
3127                         ; 5396 {
3128                         	switch	.ftext
3129  0886                   f_TpRxGetStatus:
3133                         ; 5401   if (tpRxState[tpChannel].engine != kRxState_Idle)
3135  0886 f6000c            	ldab	L11_tpRxState+3
3136  0889 2702              	beq	L5431
3137                         ; 5403     return kTpChannelInUse;
3139  088b c601              	ldab	#1
3142  088d                   L5431:
3143                         ; 5407     return kTpChannelNotInUse;
3147  088d 0a                	rtc	
3170                         ; 5421 void TP_API_CALL_TYPE TpRxResetChannel(void)
3170                         ; 5422 #else
3170                         ; 5423 void TP_API_CALL_TYPE TpRxResetChannel(canuint8 tpChannel)
3170                         ; 5424 #endif
3170                         ; 5425 {
3171                         	switch	.ftext
3172  088e                   f_TpRxResetChannel:
3176                         ; 5429   __TpRxInit(tpChannel, kTpRxErrRxResetChannelIsCalled); 
3178  088e cc0007            	ldd	#7
3179  0891 4a00a5a5          	call	L71f_TpRxInit
3181                         ; 5431 }
3184  0895 0a                	rtc	
3208                         ; 5609 void TP_API_CALL_TYPE TpRxSetBufferOverrun(void)
3208                         ; 5610 #else
3208                         ; 5611 void TP_API_CALL_TYPE TpRxSetBufferOverrun(canuint8 tpChannel)
3208                         ; 5612 #endif
3208                         ; 5613 {
3209                         	switch	.ftext
3210  0896                   f_TpRxSetBufferOverrun:
3214                         ; 5619   tpRxState[tpChannel].noCopyData = 1;
3217  0896 1c000d04          	bset	L11_tpRxState+4,4
3218                         ; 5620 }
3221  089a 0a                	rtc	
3245                         ; 5635 CanChipDataPtr TP_API_CALL_TYPE TpRxGetCanBuffer(void)
3245                         ; 5636 # else
3245                         ; 5637 CanChipDataPtr TP_API_CALL_TYPE TpRxGetCanBuffer(canuint8 tpChannel)
3245                         ; 5638 # endif
3245                         ; 5639 {
3246                         	switch	.ftext
3247  089b                   f_TpRxGetCanBuffer:
3251                         ; 5641   return tpRxInfoStruct[0].DataBufferPtr.DataCanBufferPtr;
3253  089b fc0021            	ldd	_tpRxInfoStruct
3256  089e 0a                	rtc	
3288                         ; 5658 void TP_API_CALL_TYPE TpRxSetFCStatus(canuint8 FCStatus)
3288                         ; 5659 # else
3288                         ; 5660 void TP_API_CALL_TYPE TpRxSetFCStatus(canuint8 tpChannel, canuint8 FCStatus)
3288                         ; 5661 # endif
3288                         ; 5662 {
3289                         	switch	.ftext
3290  089f                   f_TpRxSetFCStatus:
3294                         ; 5664   tpRxInfoStruct[0].ApplGetBufferStatus = (canbittype)FCStatus;
3296  089f 1d002903          	bclr	_tpRxInfoStruct+8,3
3297  08a3 c403              	andb	#3
3298  08a5 fa0029            	orab	_tpRxInfoStruct+8
3299  08a8 7b0029            	stab	_tpRxInfoStruct+8
3300                         ; 5668 }
3303  08ab 0a                	rtc	
3326                         ; 5683 canuint8 TP_API_CALL_TYPE TpRxGetFCStatus(void)
3326                         ; 5684 # else
3326                         ; 5685 canuint8 TP_API_CALL_TYPE TpRxGetFCStatus(canuint8 tpChannel)
3326                         ; 5686 # endif
3326                         ; 5687 {
3327                         	switch	.ftext
3328  08ac                   f_TpRxGetFCStatus:
3332                         ; 5689   return((canuint8)tpRxInfoStruct[0].ApplGetBufferStatus);
3334  08ac f60029            	ldab	_tpRxInfoStruct+8
3335  08af c403              	andb	#3
3338  08b1 0a                	rtc	
3385                         ; 5778 void TP_API_CALL_TYPE TpTxSetPriorityBits(canuint8 prio, canuint8 res, canuint8 dataPage)
3385                         ; 5779 #   else
3385                         ; 5780 void TP_API_CALL_TYPE TpTxSetPriorityBits(canuint8 tpChannel, canuint8 prio, canuint8 res, canuint8 dataPage)
3385                         ; 5781 #   endif
3385                         ; 5782 {
3386                         	switch	.ftext
3387  08b2                   f_TpTxSetPriorityBits:
3389       fffffffe          OFST:	set	-2
3392                         ; 5790   tpHighByte29Bit[tpChannel]  = prio<<2;
3394  08b2 87                	clra	
3395  08b3 59                	lsld	
3396  08b4 59                	lsld	
3397  08b5 7c0015            	std	L3_tpHighByte29Bit
3398                         ; 5791   tpHighByte29Bit[tpChannel] |= res <<1;
3400  08b8 e684              	ldab	OFST+6,s
3401  08ba 87                	clra	
3402  08bb b746              	tfr	d,y
3403  08bd 1858              	lsly	
3404  08bf 18fa0015          	ory	L3_tpHighByte29Bit
3405  08c3 7d0015            	sty	L3_tpHighByte29Bit
3406                         ; 5792   tpHighByte29Bit[tpChannel] |= dataPage;
3408  08c6 e686              	ldab	OFST+8,s
3409  08c8 b746              	tfr	d,y
3410  08ca 18fa0015          	ory	L3_tpHighByte29Bit
3411  08ce 7d0015            	sty	L3_tpHighByte29Bit
3412                         ; 5793 }
3415  08d1 0a                	rtc	
3447                         ; 5809 void TP_API_CALL_TYPE TpTxSetPGN(canuint8 pgn)
3447                         ; 5810 #   else
3447                         ; 5811 void TP_API_CALL_TYPE TpTxSetPGN(canuint8 tpChannel, canuint8 pgn)
3447                         ; 5812 #   endif
3447                         ; 5813 {
3448                         	switch	.ftext
3449  08d2                   f_TpTxSetPGN:
3453                         ; 5818   tpPGN29Bit[tpChannel]  = pgn;
3455  08d2 87                	clra	
3456  08d3 7c0013            	std	L5_tpPGN29Bit
3457                         ; 5819 }
3460  08d6 0a                	rtc	
3493                         ; 5839 void TP_API_CALL_TYPE TpTxSetTargetAddress(canuint8 address)
3493                         ; 5840 #  else
3493                         ; 5841 void TP_API_CALL_TYPE TpTxSetTargetAddress(canuint8 tpChannel, canuint8 address)
3493                         ; 5842 #  endif
3493                         ; 5843 {
3494                         	switch	.ftext
3495  08d7                   f_TpTxSetTargetAddress:
3499                         ; 5853   tpTxInfoStruct[tpChannel].TargetAddress = address;
3501  08d7 7b001d            	stab	_tpTxInfoStruct+6
3502                         ; 5854 }
3505  08da 0a                	rtc	
3529                         ; 5964 void TP_API_CALL_TYPE TpTxSetResponse(void)
3529                         ; 5965 # else
3529                         ; 5966 void TP_API_CALL_TYPE TpTxSetResponse(canuint8 rxChannel, canuint8 txChannel)
3529                         ; 5967 # endif
3529                         ; 5968 {
3530                         	switch	.ftext
3531  08db                   f_TpTxSetResponse:
3535                         ; 5985   tpTxInfoStruct[txChannel].TargetAddress = tpRxInfoStruct[rxChannel].SourceAddress;
3537  08db 180c0027001d      	movb	_tpRxInfoStruct+6,_tpTxInfoStruct+6
3538                         ; 6005 }
3541  08e1 0a                	rtc	
3564                         ; 6020 void TP_API_CALL_TYPE TpTxResetChannel(void)
3564                         ; 6021 #else
3564                         ; 6022 void TP_API_CALL_TYPE TpTxResetChannel(canuint8 tpChannel)
3564                         ; 6023 #endif
3564                         ; 6024 {
3565                         	switch	.ftext
3566  08e2                   f_TpTxResetChannel:
3570                         ; 6028   __TpTxInit(tpChannel, kTpTxErrTxResetChannelIsCalled);
3572  08e2 cc0027            	ldd	#39
3573  08e5 4a004b4b          	call	L51f_TpTxInit
3575                         ; 6030 }
3578  08e9 0a                	rtc	
3601                         ; 6430 void TpFuncInit ( void )
3601                         ; 6431 {
3602                         	switch	.ftext
3603  08ea                   f_TpFuncInit:
3607                         ; 6432   tpFuncState_engine = kRxState_Idle;
3609  08ea 790006            	clr	L5151_tpFuncState_engine
3610                         ; 6434 }
3613  08ed 0a                	rtc	
3660                         ; 6438 canuint8 TP_INTERNAL_CALL_TYPE TpFuncPrecopy(CanRxInfoStructPtr rxStruct)
3660                         ; 6439 #   else
3660                         ; 6440 #    if defined ( C_MULTIPLE_RECEIVE_BUFFER )
3660                         ; 6441 canuint8 TP_INTERNAL_CALL_TYPE TpFuncPrecopy(CanChipDataPtr rxDataPtr)
3660                         ; 6442 #    endif
3660                         ; 6443 #    if defined ( C_SINGLE_RECEIVE_BUFFER )
3660                         ; 6444 canuint8 TP_INTERNAL_CALL_TYPE TpFuncPrecopy(CanReceiveHandle rxObject)
3660                         ; 6445 #    endif
3660                         ; 6446 #   endif
3660                         ; 6447 {
3661                         	switch	.ftext
3662  08ee                   f_TpFuncPrecopy:
3664  08ee 3b                	pshd	
3665  08ef 1b9d              	leas	-3,s
3666       00000003          OFST:	set	3
3669                         ; 6484   if (tpFuncState_engine == kRxState_Idle)
3671  08f1 f70006            	tst	L5151_tpFuncState_engine
3672  08f4 2672              	bne	L3551
3673                         ; 6507     tpFuncInfoStruct.SourceAddress = (canuint8)CAN_RX_ACTUAL_ID_EXT_LO; /* ( CAN_RX_ACTUAL_ID & 0x000000ff); */
3675  08f6 b746              	tfr	d,y
3676  08f8 ed42              	ldy	2,y
3677  08fa e643              	ldab	3,y
3678  08fc 54                	lsrb	
3679  08fd 6b80              	stab	OFST-3,s
3680  08ff ed83              	ldy	OFST+0,s
3681  0901 ed42              	ldy	2,y
3682  0903 e642              	ldab	2,y
3683  0905 56                	rorb	
3684  0906 56                	rorb	
3685  0907 c480              	andb	#128
3686  0909 ea80              	orab	OFST-3,s
3687  090b 7b0005            	stab	L7151_tpFuncInfoStruct+5
3688                         ; 6508     tpFuncInfoStruct.TargetAddress = (canuint8)CAN_RX_ACTUAL_ID_EXT_MID_LO; /* ((CAN_RX_ACTUAL_ID & 0x0000ff00) >> 8); */
3690  090e e642              	ldab	2,y
3691  0910 54                	lsrb	
3692  0911 6b80              	stab	OFST-3,s
3693  0913 e641              	ldab	1,y
3694  0915 56                	rorb	
3695  0916 56                	rorb	
3696  0917 c480              	andb	#128
3697  0919 ea80              	orab	OFST-3,s
3698  091b 7b0004            	stab	L7151_tpFuncInfoStruct+4
3699                         ; 6578       if ((*(CanChipDataPtr)(TPCI_OFFSET + FORMAT_FUNC_OFFSET + FRAME_DATA_PTR) & 0xF0) == 0x00 )
3701  091e ed83              	ldy	OFST+0,s
3702  0920 ee44              	ldx	4,y
3703  0922 e600              	ldab	0,x
3704  0924 c5f0              	bitb	#240
3705  0926 2640              	bne	L3551
3706                         ; 6580         tpFuncInfoStruct.DataLength = *(CanChipDataPtr)(TPCI_OFFSET + FORMAT_FUNC_OFFSET + FRAME_DATA_PTR); 
3708  0928 e600              	ldab	0,x
3709  092a 87                	clra	
3710  092b 7c0002            	std	L7151_tpFuncInfoStruct+2
3711                         ; 6582         if (tpFuncInfoStruct.DataLength  != 0)
3713  092e 2738              	beq	L3551
3714                         ; 6594             if (tpFuncInfoStruct.DataLength <= (FRAME_LENGTH - (SF_OFFSET + FORMAT_FUNC_OFFSET)))
3716  0930 8c0007            	cpd	#7
3717  0933 2233              	bhi	L3551
3718                         ; 6616               tpFuncInfoStruct.DataBufferPtr.DataCanBufferPtr = (CanChipDataPtr)(SF_OFFSET + FORMAT_FUNC_OFFSET + FRAME_DATA_PTR);
3720  0935 08                	inx	
3721  0936 7e0000            	stx	L7151_tpFuncInfoStruct
3722                         ; 6617               tpFuncInfoStruct.DataBufferPtr.DataApplBufferPtr = __ApplTpFuncGetBuffer(tpFuncInfoStruct.DataLength);
3724  0939 4a000000          	call	f_DescGetFuncBuffer
3726  093d 7c0000            	std	L7151_tpFuncInfoStruct
3727                         ; 6619               if(tpFuncInfoStruct.DataBufferPtr.DataApplBufferPtr != V_NULL)
3729  0940 2726              	beq	L3551
3730                         ; 6648                     for (i = (cansintCPUtype)(tpFuncInfoStruct.DataLength-1); i>=0; i--)
3732  0942 fd0002            	ldy	L7151_tpFuncInfoStruct+2
3733  0945 03                	dey	
3734  0946 6d81              	sty	OFST-2,s
3736  0948 b746              	tfr	d,y
3737  094a 200c              	bra	L1751
3738  094c                   L5651:
3739                         ; 6650                       *(tpFuncInfoStruct.DataBufferPtr.DataApplBufferPtr + i) = *((CanChipDataPtr)(SF_OFFSET + FORMAT_FUNC_OFFSET + FRAME_DATA_PTR) + i);
3741  094c ee83              	ldx	OFST+0,s
3742  094e ee04              	ldx	4,x
3743  0950 08                	inx	
3744  0951 180ae6ee          	movb	d,x,d,y
3745                         ; 6648                     for (i = (cansintCPUtype)(tpFuncInfoStruct.DataLength-1); i>=0; i--)
3747  0955 186381            	decw	OFST-2,s
3748  0958                   L1751:
3751  0958 ec81              	ldd	OFST-2,s
3752  095a 2cf0              	bge	L5651
3753                         ; 6654                   tpFuncState_engine = kRxState_ApplInformed;
3755  095c c601              	ldab	#1
3756  095e 7b0006            	stab	L5151_tpFuncState_engine
3757                         ; 6656                   __ApplTpFuncIndication(tpFuncInfoStruct.DataLength);
3759  0961 fc0002            	ldd	L7151_tpFuncInfoStruct+2
3760  0964 4a000000          	call	f_DescFuncReqInd
3762  0968                   L3551:
3763                         ; 6665   return kCanNoCopyData;
3765  0968 c7                	clrb	
3768  0969 1b85              	leas	5,s
3769  096b 0a                	rtc	
3792                         ; 6777 void TP_API_CALL_TYPE TpFuncResetChannel(void)
3792                         ; 6778 {
3793                         	switch	.ftext
3794  096c                   f_TpFuncResetChannel:
3798                         ; 6779   tpFuncState_engine = kRxState_Idle;
3800  096c 790006            	clr	L5151_tpFuncState_engine
3801                         ; 6780 }
3804  096f 0a                	rtc	
3828                         ; 6793 void TP_API_CALL_TYPE TpFuncSetResponse(void)
3828                         ; 6794 #  else
3828                         ; 6795 void TP_API_CALL_TYPE TpFuncSetResponse(canuint8 tpChannel)
3828                         ; 6796 #  endif
3828                         ; 6797 {
3829                         	switch	.ftext
3830  0970                   f_TpFuncSetResponse:
3834                         ; 6815   tpTxInfoStruct[tpChannel].TargetAddress = tpFuncInfoStruct.SourceAddress;
3836  0970 180c0005001d      	movb	L7151_tpFuncInfoStruct+5,_tpTxInfoStruct+6
3837                         ; 6833 }
3840  0976 0a                	rtc	
3864                         ; 6848 canuint8 TP_API_CALL_TYPE TpFuncGetSourceAddress(void)
3864                         ; 6849 {
3865                         	switch	.ftext
3866  0977                   f_TpFuncGetSourceAddress:
3870                         ; 6850   return tpFuncInfoStruct.SourceAddress;
3872  0977 f60005            	ldab	L7151_tpFuncInfoStruct+5
3875  097a 0a                	rtc	
3899                         ; 6866 canuint8 TP_API_CALL_TYPE TpFuncGetTargetAddress(void)
3899                         ; 6867 {
3900                         	switch	.ftext
3901  097b                   f_TpFuncGetTargetAddress:
3905                         ; 6868   return tpFuncInfoStruct.TargetAddress;
3907  097b f60004            	ldab	L7151_tpFuncInfoStruct+4
3910  097e 0a                	rtc	
3934                         ; 7036 CanChipDataPtr TP_API_CALL_TYPE TpFuncGetCanBuffer(void)
3934                         ; 7037 {
3935                         	switch	.ftext
3936  097f                   f_TpFuncGetCanBuffer:
3940                         ; 7038   return tpFuncInfoStruct.DataBufferPtr.DataCanBufferPtr;
3942  097f fc0000            	ldd	L7151_tpFuncInfoStruct
3945  0982 0a                	rtc	
4297                         	switch	.bss
4298  0000                   L7151_tpFuncInfoStruct:
4299  0000 000000000000      	ds.b	6
4300  0006                   L5151_tpFuncState_engine:
4301  0006 00                	ds.b	1
4302  0007                   L13_tpTransmitChannel:
4303  0007 00                	ds.b	1
4304                         	xdef	f_TpFuncInit
4305  0008                   L31_tpStateTaskBusy:
4306  0008 00                	ds.b	1
4307  0009                   L11_tpRxState:
4308  0009 0000000000        	ds.b	5
4309  000e                   L7_tpTxState:
4310  000e 0000000000        	ds.b	5
4311  0013                   L5_tpPGN29Bit:
4312  0013 0000              	ds.b	2
4313  0015                   L3_tpHighByte29Bit:
4314  0015 0000              	ds.b	2
4315                         	xdef	f_TpFuncGetCanBuffer
4316                         	xdef	f_TpFuncGetTargetAddress
4317                         	xdef	f_TpFuncGetSourceAddress
4318                         	xdef	f_TpFuncSetResponse
4319                         	xdef	f_TpFuncResetChannel
4320  0017                   _tpTxInfoStruct:
4321  0017 0000000000000000  	ds.b	10
4322                         	xdef	_tpTxInfoStruct
4323  0021                   _tpRxInfoStruct:
4324  0021 0000000000000000  	ds.b	9
4325                         	xdef	_tpRxInfoStruct
4326                         	xdef	f_TpRxStateTask
4327                         	xdef	f_TpTxStateTask
4328                         	xdef	f_TpRxGetFCStatus
4329                         	xdef	f_TpRxSetFCStatus
4330                         	xdef	f_TpRxSetBufferOverrun
4331                         	xdef	f_TpRxGetCanBuffer
4332                         	xdef	f_TpRxGetSourceAddress
4333                         	xdef	f_TpRxGetStatus
4334                         	xdef	f_TpRxResetChannel
4335                         	xdef	f_TpTxSetResponse
4336                         	xdef	f_TpTxSetPGN
4337                         	xdef	f_TpTxSetPriorityBits
4338                         	xdef	f_TpTxSetTargetAddress
4339                         	xdef	f_TpTxResetChannel
4340                         	xdef	f_TpTransmit
4341                         	xdef	f_TpRxTask
4342                         	xdef	f_TpTxTask
4343                         	xdef	f_TpTask
4344                         	xdef	f_TpInit
4345                         	xdef	f_TpInitPowerOn
4346                         	xdef	_kTpBugFixVersion
4347                         	xdef	_kTpSubVersion
4348                         	xdef	_kTpMainVersion
4349                         	xref	f_DescCopyToCAN
4350                         	xref	f_DescTxErrorIndication
4351                         	xref	f_DescGetBuffer
4352                         	xref	f_DescConfirmation
4353                         	xref	f_DescRxErrorIndication
4354                         	xref	f_DescPhysReqInd
4355                         	xref	f_DescFuncReqInd
4356                         	xref	f_DescGetFuncBuffer
4357                         	xdef	f_TpDrvConfirmation
4358                         	xref	_TxDynamicMsg0
4359                         	xref	f_CanDynTxObjSetDlc
4360                         	xref	f_CanDynTxObjSetExtId
4361                         	xref	f_CanGetDynTxObj
4362                         	xref	f_CanCancelTransmit
4363                         	xref	f_CanTransmit
4364                         	xdef	f_TpFuncPrecopy
4365                         	xdef	f_TpPrecopy
4366                         	xref	f_VStdResumeAllInterrupts
4367                         	xref	f_VStdSuspendAllInterrupts
4388                         	end
