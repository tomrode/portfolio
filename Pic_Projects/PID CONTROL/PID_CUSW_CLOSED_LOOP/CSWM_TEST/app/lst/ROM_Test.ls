   1                         ; C Compiler for S12XE [COSMIC Software].
   2                         ; Generator V4.7.8 - 27 Jul 2007
   3                         ; Optimizer V4.6.13 - 27 Jul 2007
  41                         ; 89 void ROMTstF_Init(void)
  41                         ; 90 {
  42                         	switch	.text
  43  0000                   _ROMTstF_Init:
  47                         ; 91 	ROMTst_status   = ROMTST_EXECUTION_INIT;            // ROM Test is initialized and ready to be started
  49  0000 c601              	ldab	#1
  50  0002 7b000f            	stab	_ROMTst_status
  51                         ; 92 	ROMTst_result   = ROMTST_RESULT_NOT_TESTED;         // The ROM Test is not executed
  53  0005 79000c            	clr	_ROMTst_result
  54                         ; 94 	ROMTst_Entry_p = (ROMTst_Entry_s *) &_ROMTstStart; // Original
  56  0008 cd0000            	ldy	#__ROMTstStart
  57  000b 7d0002            	sty	_ROMTst_Entry_p
  58                         ; 97 	ROMTst_page         = ROMTst_Entry_p->page;         // locate the page
  60  000e 180d420009        	movb	2,y,_ROMTst_page
  61                         ; 98 	ROMTst_startAddrs_p = ROMTst_Entry_p->startAddress; // locate the start address
  63  0013 1805430007        	movw	3,y,_ROMTst_startAddrs_p
  64                         ; 99 	ROMTst_endAddrs_p   = ROMTst_Entry_p->endAddress;   // locate the end address  
  66  0018 1805450005        	movw	5,y,_ROMTst_endAddrs_p
  67                         ; 100 }
  70  001d 3d                	rts	
 105                         ; 119 void ROMTstF_Calc_ChckSum(void)
 105                         ; 120 {	
 106                         	switch	.text
 107  001e                   _ROMTstF_Calc_ChckSum:
 111                         ; 122 	if ( (ROMTst_status == ROMTST_EXECUTION_INIT) || (ROMTst_status == ROMTST_EXECUTION_RUNNING) ) {
 113  001e f6000f            	ldab	_ROMTst_status
 114  0021 c101              	cmpb	#1
 115  0023 2706              	beq	L33
 117  0025 c102              	cmpb	#2
 118  0027 18260098          	bne	L13
 119  002b                   L33:
 120                         ; 124 		ROMTst_loop_cntr = 0;
 122  002b 1879000a          	clrw	_ROMTst_loop_cntr
 123                         ; 127 		if ( (ROMTst_page != ROMTST_COMMON_PAGE1) && 
 123                         ; 128 			 (ROMTst_page != ROMTST_COMMON_PAGE2) &&
 123                         ; 129 			 (!ppage_updated_b) ) {		
 125  002f f60009            	ldab	_ROMTst_page
 126  0032 c1fd              	cmpb	#253
 127  0034 2734              	beq	L34
 129  0036 c1ff              	cmpb	#255
 130  0038 2730              	beq	L34
 132  003a 1e0004012b        	brset	_ROMTst_Flag1,1,L34
 133                         ; 131 			_PPAGE.Byte = ROMTst_page;
 135  003f 7b0000            	stab	__PPAGE
 136                         ; 133 			ppage_updated_b = TRUE;
 138  0042 1c000401          	bset	_ROMTst_Flag1,1
 140  0046 2022              	bra	L34
 141  0048                   L14:
 142                         ; 152 			_asm (" sei");					// disable interrupts
 145  0048 1410              	sei	
 147                         ; 153 			_asm (" ldd _ROMTst_checksum"); // load accumulator D with ROMTst_checksum
 150  004a fc000d            	ldd	_ROMTst_checksum
 152                         ; 154 			_asm (" addd #$8000");			// bitwise OR ROMTst_checksum with 0x8000 (set MSB)
 155  004d c38000            	addd	#$8000
 157                         ; 155 			_asm (" rolb");					// Rotate left accumulator B
 160  0050 55                	rolb	
 162                         ; 156 			_asm (" rola");					// Rotate left accumulator A
 165  0051 45                	rola	
 167                         ; 157 			_asm (" std _ROMTst_checksum"); // store content of accumulator D in ROMTst_checksum
 170  0052 7c000d            	std	_ROMTst_checksum
 172                         ; 158 			_asm (" cli");
 175  0055 10ef              	cli	
 177                         ; 159 			ROMTst_checksum ^= (*ROMTst_startAddrs_p);    // XOR the checksum with the code byte
 179  0057 e640              	ldab	0,y
 180  0059 b796              	exg	b,y
 181  005b 18f8000d          	eory	_ROMTst_checksum
 182  005f 7d000d            	sty	_ROMTst_checksum
 183                         ; 163 			ROMTst_startAddrs_p++;
 185  0062 18720007          	incw	_ROMTst_startAddrs_p
 186                         ; 164 			ROMTst_loop_cntr++;    //increment the loop counter
 188  0066 1872000a          	incw	_ROMTst_loop_cntr
 189  006a                   L34:
 190                         ; 140 		while ( (ROMTst_loop_cntr < MAX_LOOP_CNT) && (ROMTst_startAddrs_p != ROMTst_endAddrs_p) ) {	
 192  006a fc000a            	ldd	_ROMTst_loop_cntr
 193  006d 8c012c            	cpd	#300
 194  0070 2408              	bhs	L74
 196  0072 fd0007            	ldy	_ROMTst_startAddrs_p
 197  0075 bd0005            	cpy	_ROMTst_endAddrs_p
 198  0078 26ce              	bne	L14
 199  007a                   L74:
 200                         ; 168 		if (ROMTst_startAddrs_p == ROMTst_endAddrs_p) {
 202  007a fc0007            	ldd	_ROMTst_startAddrs_p
 203  007d bc0005            	cpd	_ROMTst_endAddrs_p
 204  0080 264a              	bne	L56
 205                         ; 170 			ROMTst_Entry_p++;
 207  0082 fd0002            	ldy	_ROMTst_Entry_p
 208                         ; 172 			ppage_updated_b = FALSE;
 210  0085 1d000401          	bclr	_ROMTst_Flag1,1
 211                         ; 175 			if (ROMTst_Entry_p->flag == ROMTST_CONTINUE_FLAG) {
 213  0089 ec66              	ldd	7,+y
 214  008b 7d0002            	sty	_ROMTst_Entry_p
 215  008e 8c8401            	cpd	#-31743
 216  0091 2615              	bne	L35
 217                         ; 178 				ROMTst_page = ROMTst_Entry_p->page;
 219  0093 180d420009        	movb	2,y,_ROMTst_page
 220                         ; 180 				ROMTst_startAddrs_p = ROMTst_Entry_p->startAddress;
 222  0098 1805430007        	movw	3,y,_ROMTst_startAddrs_p
 223                         ; 182 				ROMTst_endAddrs_p = ROMTst_Entry_p->endAddress;
 225  009d ec45              	ldd	5,y
 226  009f 7c0005            	std	_ROMTst_endAddrs_p
 227                         ; 184 				ROMTst_status = ROMTST_EXECUTION_RUNNING;
 229  00a2 c602              	ldab	#2
 230  00a4 7b000f            	stab	_ROMTst_status
 233  00a7 3d                	rts	
 234  00a8                   L35:
 235                         ; 188 				ROMTst_checksum = ~ROMTst_checksum;
 237  00a8 1871000d          	comw	_ROMTst_checksum
 238                         ; 190 				ROMTst_Rslt_p = (ROMTst_Rslt_s *) ROMTst_Entry_p;
 240  00ac 7d0000            	sty	_ROMTst_Rslt_p
 241                         ; 192 				ROMTst_status = ROMTST_EXECUTION_FINISHED;
 243  00af c603              	ldab	#3
 244  00b1 7b000f            	stab	_ROMTst_status
 245                         ; 195 				if (ROMTst_Rslt_p->checkSum == ROMTst_checksum) {
 247  00b4 ec41              	ldd	1,y
 248  00b6 bc000d            	cpd	_ROMTst_checksum
 249  00b9 2604              	bne	L75
 250                         ; 197 					ROMTst_result = ROMTST_RESULT_OK;
 252  00bb c601              	ldab	#1
 254  00bd 200a              	bra	LC001
 255  00bf                   L75:
 256                         ; 200 					ROMTst_result = ROMTST_RESULT_NOT_OK;
 258  00bf c602              	ldab	#2
 259  00c1 2006              	bra	LC001
 260  00c3                   L13:
 261                         ; 209 		if (ROMTst_status != ROMTST_EXECUTION_FINISHED) {
 263  00c3 c103              	cmpb	#3
 264  00c5 2705              	beq	L56
 265                         ; 211 			ROMTst_result = ROMTST_RESULT_UNDEFINED;
 267  00c7 c603              	ldab	#3
 268  00c9                   LC001:
 269  00c9 7b000c            	stab	_ROMTst_result
 271  00cc                   L56:
 272                         ; 217 }
 275  00cc 3d                	rts	
 309                         ; 232 void ROMTstF_Compare_ChckSum(void)
 309                         ; 233 {
 310                         	switch	.text
 311  00cd                   _ROMTstF_Compare_ChckSum:
 313  00cd 3b                	pshd	
 314       00000002          OFST:	set	2
 317                         ; 237 	ROMTest_checksum_return = /*_checksum();*/ _checksum16();
 319  00ce 160000            	jsr	__checksum16
 321                         ; 238 }
 324  00d1 31                	puly	
 325  00d2 3d                	rts	
 353                         ; 254 void ROMTstF_Clr_Rom_Err(void)
 353                         ; 255 {
 354                         	switch	.text
 355  00d3                   _ROMTstF_Clr_Rom_Err:
 359                         ; 257 	EE_BlockRead(EE_INTERNAL_FLT_BYTE0, (u_8Bit*)&intFlt_bytes[ZERO]);
 361  00d3 cc0000            	ldd	#_intFlt_bytes
 362  00d6 3b                	pshd	
 363  00d7 cc0018            	ldd	#24
 364  00da 4a000000          	call	f_EE_BlockRead
 366  00de 1b82              	leas	2,s
 367                         ; 260 	if ( ( (intFlt_bytes[ZERO] & ROM_CHKSUM_ERR_MASK) == ROM_CHKSUM_ERR_MASK) &&
 367                         ; 261 		 (ROMTst_result == ROMTST_RESULT_OK) ) {
 369  00e0 1f00000818        	brclr	_intFlt_bytes,8,L121
 371  00e5 f6000c            	ldab	_ROMTst_result
 372  00e8 042112            	dbne	b,L121
 373                         ; 263 		IntFlt_F_Update(ZERO, CLR_ROM_CHCKSUM_ERR, INT_FLT_CLR);
 375  00eb cc0001            	ldd	#1
 376  00ee 3b                	pshd	
 377  00ef c6f7              	ldab	#247
 378  00f1 3b                	pshd	
 379  00f2 c7                	clrb	
 380  00f3 4a000000          	call	f_IntFlt_F_Update
 382  00f7 1b84              	leas	4,s
 383                         ; 265 		Rom_chksum_err_b = FALSE;
 385  00f9 1d000008          	bclr	_int_flt_byte0,8
 387  00fd                   L121:
 388                         ; 270 }
 391  00fd 3d                	rts	
 393                         	switch	.data
 394  0000                   L321_Rom_CAN_disable_cnt:
 395  0000 00                	dc.b	0
 437                         ; 289 void ROMTstF_Chck_Err(void)
 437                         ; 290 {
 438                         	switch	.text
 439  00fe                   _ROMTstF_Chck_Err:
 443                         ; 295 	if (intFlt_config_c == ENABLE_HIGH_PRIORITY_INTFLT_DET) {
 445  00fe f60000            	ldab	_intFlt_config_c
 446  0101 c155              	cmpb	#85
 447  0103 2653              	bne	L141
 448                         ; 298 		if (ROMTst_result == ROMTST_RESULT_OK) {
 450  0105 f6000c            	ldab	_ROMTst_result
 451  0108 042109            	dbne	b,L341
 452                         ; 300 			Rom_CAN_disable_cnt = 0;
 454  010b 790000            	clr	L321_Rom_CAN_disable_cnt
 455                         ; 302 			Rom_chksum_err_b = FALSE;
 457  010e 1d000008          	bclr	_int_flt_byte0,8
 458                         ; 304 			mainF_Finish_Mcu_Test(MAIN_ROM_TEST);
 462  0112 2044              	bra	L141
 463  0114                   L341:
 464                         ; 308 			if (Rom_chksum_err_b == TRUE) {
 466  0114 1e00000808        	brset	_int_flt_byte0,8,L151
 468                         ; 312 				IntFlt_F_ClearOutputs();
 470  0119 4a000000          	call	f_IntFlt_F_ClearOutputs
 472                         ; 314 				Rom_chksum_err_b = TRUE;
 474  011d 1c000008          	bset	_int_flt_byte0,8
 475  0121                   L151:
 476                         ; 318 			EE_BlockRead(EE_INTERNAL_FLT_BYTE0, (u_8Bit*)&intFlt_bytes[ZERO]);
 478  0121 cc0000            	ldd	#_intFlt_bytes
 479  0124 3b                	pshd	
 480  0125 cc0018            	ldd	#24
 481  0128 4a000000          	call	f_EE_BlockRead
 483  012c 1b82              	leas	2,s
 484                         ; 321 			if ( (intFlt_bytes[ZERO]& ROM_CHKSUM_ERR_MASK) == ROM_CHKSUM_ERR_MASK) {
 486  012e 1f00000817        	brclr	_intFlt_bytes,8,L351
 487                         ; 325 				if (Rom_CAN_disable_cnt < INTFLT_CAN_DISABLE_TM) {
 489  0133 f60000            	ldab	L321_Rom_CAN_disable_cnt
 490  0136 c114              	cmpb	#20
 491  0138 2404              	bhs	L551
 492                         ; 327 					Rom_CAN_disable_cnt++;
 494  013a 720000            	inc	L321_Rom_CAN_disable_cnt
 497  013d 3d                	rts	
 498  013e                   L551:
 499                         ; 330 					Dio_WriteChannel(CAN_STB, STD_HIGH);
 502  013e 1410              	sei	
 507  0140 fe0000            	ldx	_Dio_PortWrite_Ptr
 508  0143 0c0008            	bset	0,x,8
 512  0146 10ef              	cli	
 514                         ; 332 					mainF_Finish_Mcu_Test(MAIN_ROM_TEST);
 518  0148 200e              	bra	L141
 519  014a                   L351:
 520                         ; 336 				IntFlt_F_Update(ZERO, ROM_CHKSUM_ERR_MASK, INT_FLT_SET);
 522  014a 87                	clra	
 523  014b c7                	clrb	
 524  014c 3b                	pshd	
 525  014d c608              	ldab	#8
 526  014f 3b                	pshd	
 527  0150 c7                	clrb	
 528  0151 4a000000          	call	f_IntFlt_F_Update
 530  0155 1b84              	leas	4,s
 532  0157 3d                	rts	
 533  0158                   L141:
 534                         ; 342 		mainF_Finish_Mcu_Test(MAIN_ROM_TEST);
 536  0158 cc0001            	ldd	#1
 537  015b 160000            	jsr	_mainF_Finish_Mcu_Test
 539                         ; 344 }
 542  015e 3d                	rts	
 819                         	xref	__ROMTstStart
 820                         	xref	__checksum16
 821                         	switch	.bss
 822  0000                   _ROMTst_Rslt_p:
 823  0000 0000              	ds.b	2
 824                         	xdef	_ROMTst_Rslt_p
 825  0002                   _ROMTst_Entry_p:
 826  0002 0000              	ds.b	2
 827                         	xdef	_ROMTst_Entry_p
 828  0004                   _ROMTst_Flag1:
 829  0004 00                	ds.b	1
 830                         	xdef	_ROMTst_Flag1
 831  0005                   _ROMTst_endAddrs_p:
 832  0005 0000              	ds.b	2
 833                         	xdef	_ROMTst_endAddrs_p
 834  0007                   _ROMTst_startAddrs_p:
 835  0007 0000              	ds.b	2
 836                         	xdef	_ROMTst_startAddrs_p
 837  0009                   _ROMTst_page:
 838  0009 00                	ds.b	1
 839                         	xdef	_ROMTst_page
 840  000a                   _ROMTst_loop_cntr:
 841  000a 0000              	ds.b	2
 842                         	xdef	_ROMTst_loop_cntr
 843                         	xref	f_EE_BlockRead
 844                         	xref	f_IntFlt_F_ClearOutputs
 845                         	xref	f_IntFlt_F_Update
 846                         	xref	_intFlt_config_c
 847                         	xref	_intFlt_bytes
 848                         	xref	_int_flt_byte0
 849                         	xref	_Dio_PortWrite_Ptr
 850                         	xref	__PPAGE
 851                         	xdef	_ROMTstF_Clr_Rom_Err
 852                         	xdef	_ROMTstF_Chck_Err
 853                         	xdef	_ROMTstF_Compare_ChckSum
 854                         	xdef	_ROMTstF_Calc_ChckSum
 855                         	xdef	_ROMTstF_Init
 856  000c                   _ROMTst_result:
 857  000c 00                	ds.b	1
 858                         	xdef	_ROMTst_result
 859  000d                   _ROMTst_checksum:
 860  000d 0000              	ds.b	2
 861                         	xdef	_ROMTst_checksum
 862  000f                   _ROMTst_status:
 863  000f 00                	ds.b	1
 864                         	xdef	_ROMTst_status
 865                         	xref	_mainF_Finish_Mcu_Test
 886                         	end
