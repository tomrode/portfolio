   1                         ; C Compiler for S12XE [COSMIC Software].
   2                         ; Generator V4.7.8 - 27 Jul 2007
   3                         ; Optimizer V4.6.13 - 27 Jul 2007
 417                         ; 118 void DESC_API_CALLBACK_TYPE ApplDescRead_220101_Bussed_Outputs_Status(DescMsgContext* pMsgContext)
 417                         ; 119 {
 418                         .ftext:	section	.text
 419  0000                   f_ApplDescRead_220101_Bussed_Outputs_Status:
 421  0000 3b                	pshd	
 422       00000000          OFST:	set	0
 425                         ; 167 	temp_c = 0;
 427  0001 790001            	clr	L311_temp_c
 428                         ; 169 	temp_c = hs_output_status_c [FRONT_LEFT];
 430                         ; 170 	pMsgContext->resData[0] = temp_c;
 432  0004 b746              	tfr	d,y
 433  0006 ee44              	ldx	4,y
 434  0008 1809000000        	movb	_hs_output_status_c,0,x
 435                         ; 173 	temp_c = hs_output_status_c [FRONT_RIGHT];
 437  000d f60001            	ldab	_hs_output_status_c+1
 438  0010 58                	lslb	
 439  0011 58                	lslb	
 440  0012 58                	lslb	
 441  0013 7b0001            	stab	L311_temp_c
 442                         ; 174 	temp_c <<= 3;
 444                         ; 176 	pMsgContext->resData[0] |= temp_c;
 446  0016 ea00              	orab	0,x
 447  0018 6b00              	stab	0,x
 448                         ; 179 	if ((CSWM_config_c & CSWM_4HS_VARIANT_MASK_K) == CSWM_4HS_VARIANT_MASK_K)
 450  001a 1e00000f02        	brset	_CSWM_config_c,15,LC001
 451  001f 201d              	bra	L762
 452  0021                   LC001:
 453                         ; 181 		temp_c = 0;
 455  0021 790001            	clr	L311_temp_c
 456                         ; 184 		temp_c = hs_output_status_c [REAR_LEFT];
 458                         ; 185 		pMsgContext->resData[1] = temp_c;
 460  0024 ed44              	ldy	4,y
 461  0026 1809410002        	movb	_hs_output_status_c+2,1,y
 462                         ; 188 		temp_c = hs_output_status_c [REAR_RIGHT];
 464  002b f60003            	ldab	_hs_output_status_c+3
 465  002e 58                	lslb	
 466  002f 58                	lslb	
 467  0030 58                	lslb	
 468  0031 7b0001            	stab	L311_temp_c
 469                         ; 189 		temp_c <<= 3;
 471                         ; 190 		pMsgContext->resData[1] |= temp_c;
 473  0034 ed80              	ldy	OFST+0,s
 474  0036 ed44              	ldy	4,y
 475  0038 ea41              	orab	1,y
 476  003a 6b41              	stab	1,y
 478  003c 2004              	bra	L172
 479  003e                   L762:
 480                         ; 195 		pMsgContext->resData[1] = 0x00;
 482  003e ed44              	ldy	4,y
 483  0040 6941              	clr	1,y
 484  0042                   L172:
 485                         ; 198 	if ((CSWM_config_c & CSWM_2VS_MASK_K) == CSWM_2VS_MASK_K)
 487  0042 1e00003002        	brset	_CSWM_config_c,48,LC002
 488  0047 2028              	bra	L372
 489  0049                   LC002:
 490                         ; 200 		temp_c = 0;
 492  0049 c7                	clrb	
 493  004a 7b0001            	stab	L311_temp_c
 494                         ; 202 		temp_c = vsF_Get_Output_Stat(VS_FL);
 496  004d 87                	clra	
 497  004e 4a000000          	call	f_vsF_Get_Output_Stat
 499  0052 7b0001            	stab	L311_temp_c
 500                         ; 203 		pMsgContext->resData[2] = temp_c;
 502  0055 ed80              	ldy	OFST+0,s
 503  0057 ed44              	ldy	4,y
 504  0059 6b42              	stab	2,y
 505                         ; 205 		temp_c = vsF_Get_Output_Stat(VS_FR);
 507  005b cc0001            	ldd	#1
 508  005e 4a000000          	call	f_vsF_Get_Output_Stat
 510  0062 58                	lslb	
 511  0063 58                	lslb	
 512  0064 7b0001            	stab	L311_temp_c
 513                         ; 206 		temp_c <<= 2; 
 515                         ; 207 		pMsgContext->resData[2] |= temp_c;
 517  0067 ed80              	ldy	OFST+0,s
 518  0069 ed44              	ldy	4,y
 519  006b ea42              	orab	2,y
 520  006d 6b42              	stab	2,y
 522  006f 2006              	bra	L572
 523  0071                   L372:
 524                         ; 211 		pMsgContext->resData[2] = 0x00;
 526  0071 ed80              	ldy	OFST+0,s
 527  0073 ed44              	ldy	4,y
 528  0075 6942              	clr	2,y
 529  0077                   L572:
 530                         ; 214 	if ((CSWM_config_c & CSWM_HSW_MASK_K) == CSWM_HSW_MASK_K)
 532  0077 1f0000400c        	brclr	_CSWM_config_c,64,L772
 533                         ; 216 		pMsgContext->resData[3] = stWF_Get_Output_Stat();
 535  007c 4a000000          	call	f_stWF_Get_Output_Stat
 537  0080 ed80              	ldy	OFST+0,s
 538  0082 ed44              	ldy	4,y
 539  0084 6b43              	stab	3,y
 541  0086 2006              	bra	L103
 542  0088                   L772:
 543                         ; 220 		pMsgContext->resData[3] = 0x00;
 545  0088 ed80              	ldy	OFST+0,s
 546  008a ed44              	ldy	4,y
 547  008c 6943              	clr	3,y
 548  008e                   L103:
 549                         ; 224 	temp_c = 0;
 551  008e 790001            	clr	L311_temp_c
 552                         ; 225 	temp_c = canio_RemSt_configStat_c;
 554  0091 f60000            	ldab	_canio_RemSt_configStat_c
 555  0094 58                	lslb	
 556  0095 7b0001            	stab	L311_temp_c
 557                         ; 226 	temp_c <<= 1;
 559                         ; 227 	pMsgContext->resData[4] = temp_c;
 561  0098 ed80              	ldy	OFST+0,s
 562  009a ed44              	ldy	4,y
 563  009c 6b44              	stab	4,y
 564                         ; 229 	DataLength = 5;
 566  009e cc0005            	ldd	#5
 567  00a1 7c0002            	std	L111_DataLength
 568                         ; 230 	Response = RESPONSE_OK;
 570  00a4 c601              	ldab	#1
 571  00a6 7b0000            	stab	L511_Response
 572                         ; 231 	diagF_DiagResponse(Response, DataLength, pMsgContext);
 574  00a9 ec80              	ldd	OFST+0,s
 575  00ab 3b                	pshd	
 576  00ac cc0005            	ldd	#5
 577  00af 3b                	pshd	
 578  00b0 c601              	ldab	#1
 579  00b2 4a000000          	call	f_diagF_DiagResponse
 581  00b6 1b86              	leas	6,s
 582                         ; 232 }
 585  00b8 0a                	rtc	
 642                         ; 238 void DESC_API_CALLBACK_TYPE ApplDescRead_220102_Bussed_Input(DescMsgContext* pMsgContext)
 642                         ; 239 {
 643                         	switch	.ftext
 644  00b9                   f_ApplDescRead_220102_Bussed_Input:
 646  00b9 3b                	pshd	
 647       00000000          OFST:	set	0
 650                         ; 270 	pMsgContext->resData[0] = canio_RX_EngineSpeed_c;
 652  00ba f60000            	ldab	_canio_RX_EngineSpeed_c
 653  00bd ed80              	ldy	OFST+0,s
 654  00bf 6beb0004          	stab	[4,y]
 655                         ; 273 	temp_c = 0; //Init the value.
 657  00c3 790001            	clr	L311_temp_c
 658                         ; 274 	pMsgContext->resData[1] = temp_c; //Initialization
 660  00c6 ed44              	ldy	4,y
 661  00c8 6941              	clr	1,y
 662                         ; 275 	temp_c = IlGetRxEngineSpeedValidData();
 664  00ca f60007            	ldab	_STATUS_B_ECM+7
 665  00cd 55                	rolb	
 666  00ce 55                	rolb	
 667  00cf 55                	rolb	
 668  00d0 55                	rolb	
 669  00d1 c401              	andb	#1
 670                         ; 276 	pMsgContext->resData[1] = temp_c; //Engine Speed Valid Data Info stored.
 672  00d3 ed80              	ldy	OFST+0,s
 673  00d5 ed44              	ldy	4,y
 674  00d7 6b41              	stab	1,y
 675                         ; 278 	temp_c = IlGetRxEngineSts();
 677  00d9 f60000            	ldab	_STATUS_C_CAN
 678  00dc c4c0              	andb	#192
 679  00de 54                	lsrb	
 680  00df 54                	lsrb	
 681  00e0 54                	lsrb	
 682  00e1 54                	lsrb	
 683  00e2 54                	lsrb	
 684                         ; 279 	temp_c <<=1;
 686                         ; 280 	pMsgContext->resData[1] |= temp_c; //Engine Status stored
 688  00e3 ea41              	orab	1,y
 689  00e5 6b41              	stab	1,y
 690                         ; 282 	temp_c = canio_RX_IGN_STATUS_c;
 692  00e7 f60000            	ldab	_canio_RX_IGN_STATUS_c
 693  00ea 58                	lslb	
 694  00eb 58                	lslb	
 695  00ec 58                	lslb	
 696                         ; 283 	temp_c <<= 3;
 698                         ; 284 	pMsgContext->resData[1] |= temp_c; //Ignition Status stored
 700  00ed ea41              	orab	1,y
 701  00ef 6b41              	stab	1,y
 702                         ; 286 	temp_c = canio_RX_IgnRun_RemSt_c;
 704  00f1 f60000            	ldab	_canio_RX_IgnRun_RemSt_c
 705                         ; 287 	temp_c <<= 6;
 707  00f4 56                	rorb	
 708  00f5 56                	rorb	
 709  00f6 56                	rorb	
 710  00f7 c4c0              	andb	#192
 711                         ; 288 	pMsgContext->resData[1] |= temp_c; //Ignition Run Remote Start Active stored
 713  00f9 ea41              	orab	1,y
 714  00fb 6b41              	stab	1,y
 715                         ; 290 	temp_c = canio_RemSt_configStat_c;
 717  00fd f60000            	ldab	_canio_RemSt_configStat_c
 718                         ; 291 	temp_c<<=7;
 720  0100 56                	rorb	
 721  0101 56                	rorb	
 722  0102 c480              	andb	#128
 723  0104 7b0001            	stab	L311_temp_c
 724                         ; 292 	pMsgContext->resData[1] |= temp_c; //Last Known Remote start status stored
 726  0107 ea41              	orab	1,y
 727  0109 6b41              	stab	1,y
 728                         ; 295 	temp_c = 0; //Init the value.
 730  010b 790001            	clr	L311_temp_c
 731                         ; 296 	pMsgContext->resData[2] = temp_c;  //Initialization
 733  010e 6942              	clr	2,y
 734                         ; 297 	temp_c = canio_RX_FR_HS_RQ_TGW_c;
 736                         ; 298 	pMsgContext->resData[2] = temp_c;  //Front Right Heating Switch Request Stored
 738  0110 1809420001        	movb	_canio_hvac_heated_seat_request_c+1,2,y
 739                         ; 299 	temp_c = canio_RX_FL_HS_RQ_TGW_c;
 741  0115 f60000            	ldab	_canio_hvac_heated_seat_request_c
 742  0118 58                	lslb	
 743  0119 58                	lslb	
 744  011a 7b0001            	stab	L311_temp_c
 745                         ; 300 	temp_c <<= 2;
 747                         ; 301 	pMsgContext->resData[2] |= temp_c; //Front Left Heating Switch Request Stored
 749  011d ea42              	orab	2,y
 750  011f 6b42              	stab	2,y
 751                         ; 304 	if( ((CSWM_config_c & CSWM_2VS_MASK_K) == CSWM_2VS_MASK_K) )
 753  0121 1e00003002        	brset	_CSWM_config_c,48,LC003
 754  0126 201a              	bra	L323
 755  0128                   LC003:
 756                         ; 306 		temp_c = canio_RF_VS_RQ_c;
 758  0128 f60001            	ldab	_canio_hvac_vent_seat_request_c+1
 759                         ; 307 		temp_c <<= 4;
 761  012b 58                	lslb	
 762  012c 58                	lslb	
 763  012d 58                	lslb	
 764  012e 58                	lslb	
 765                         ; 308 		pMsgContext->resData[2] |= temp_c; //Front Right Vent Switch Request Stored.
 767  012f ea42              	orab	2,y
 768  0131 6b42              	stab	2,y
 769                         ; 309 		temp_c = canio_LF_VS_RQ_c;
 771  0133 f60000            	ldab	_canio_hvac_vent_seat_request_c
 772                         ; 310 		temp_c <<= 6;
 774  0136 56                	rorb	
 775  0137 56                	rorb	
 776  0138 56                	rorb	
 777  0139 c4c0              	andb	#192
 778  013b 7b0001            	stab	L311_temp_c
 779                         ; 311 		pMsgContext->resData[2] |= temp_c; //Front Left Vent Switch Request Stored.
 781  013e ea42              	orab	2,y
 782  0140 6b42              	stab	2,y
 783  0142                   L323:
 784                         ; 315 	temp_c = 0; //Init the value.
 786  0142 790001            	clr	L311_temp_c
 787                         ; 316 	pMsgContext->resData[3] = temp_c;  //Initialization
 789  0145 6943              	clr	3,y
 790                         ; 318 	if ((CSWM_config_c & CSWM_HSW_MASK_K) == CSWM_HSW_MASK_K)
 792  0147 1f00004008        	brclr	_CSWM_config_c,64,L523
 793                         ; 321 		temp_c = canio_RX_HSW_RQ_TGW_c;
 795  014c f60000            	ldab	_canio_RX_HSW_RQ_TGW_c
 796  014f 7b0001            	stab	L311_temp_c
 797                         ; 322 		pMsgContext->resData[3] = temp_c;
 799  0152 6b43              	stab	3,y
 800  0154                   L523:
 801                         ; 325 	if(canio_LHD_RHD_c)
 803  0154 f60000            	ldab	_canio_LHD_RHD_c
 804  0157 2704              	beq	L723
 805                         ; 327 	temp_c = 0x02;     //Table conversions  2114
 807  0159 c602              	ldab	#2
 809  015b 2002              	bra	L133
 810  015d                   L723:
 811                         ; 331 	temp_c = 0x01;     //Table conversions  2114
 813  015d c601              	ldab	#1
 814  015f                   L133:
 815  015f 58                	lslb	
 816  0160 58                	lslb	
 817  0161 7b0001            	stab	L311_temp_c
 818                         ; 333 	temp_c <<= 2;
 820                         ; 334 	pMsgContext->resData[3] |= temp_c;
 822  0164 ed80              	ldy	OFST+0,s
 823  0166 ed44              	ldy	4,y
 824  0168 ea43              	orab	3,y
 825  016a 6b43              	stab	3,y
 826                         ; 336 	if ((CSWM_config_c & CSWM_HSW_MASK_K) == CSWM_HSW_MASK_K)
 828  016c 1f0000400e        	brclr	_CSWM_config_c,64,L333
 829                         ; 339 		temp_c = canio_RX_StW_TempSens_FltSts_c;
 831  0171 f60000            	ldab	_canio_RX_StW_TempSens_FltSts_c
 832                         ; 340 		temp_c<<=4;
 834  0174 58                	lslb	
 835  0175 58                	lslb	
 836  0176 58                	lslb	
 837  0177 58                	lslb	
 838  0178 7b0001            	stab	L311_temp_c
 839                         ; 341 		pMsgContext->resData[3] |= temp_c;
 841  017b ea43              	orab	3,y
 842  017d 6b43              	stab	3,y
 843  017f                   L333:
 844                         ; 346 	pMsgContext->resData[4] = 0;
 846  017f 6944              	clr	4,y
 847                         ; 348 	if ((CSWM_config_c & CSWM_HSW_MASK_K) == CSWM_HSW_MASK_K)
 849  0181 1f00004005        	brclr	_CSWM_config_c,64,L533
 850                         ; 351 		pMsgContext->resData[4] = canio_RX_StW_TempSts_c;
 852  0186 1809440000        	movb	_canio_RX_StW_TempSts_c,4,y
 853  018b                   L533:
 854                         ; 355 	pMsgContext->resData[5] = canio_RX_BatteryVoltageLevel_c;
 856  018b 1809450000        	movb	_canio_RX_BatteryVoltageLevel_c,5,y
 857                         ; 358 	pMsgContext->resData[6] = BattVF_Get_Crt_Uint();
 859  0190 4a000000          	call	f_BattVF_Get_Crt_Uint
 861  0194 ed80              	ldy	OFST+0,s
 862  0196 ed44              	ldy	4,y
 863  0198 6b46              	stab	6,y
 864                         ; 361 	pMsgContext->resData[7] = canio_RX_ExternalTemperature_c;
 866  019a 1809470000        	movb	_canio_RX_ExternalTemperature_c,7,y
 867                         ; 363 	DataLength = 8;
 869  019f cc0008            	ldd	#8
 870  01a2 7c0002            	std	L111_DataLength
 871                         ; 364 	Response = RESPONSE_OK;
 873  01a5 c601              	ldab	#1
 874  01a7 7b0000            	stab	L511_Response
 875                         ; 365 	diagF_DiagResponse(Response, DataLength, pMsgContext);
 877  01aa ec80              	ldd	OFST+0,s
 878  01ac 3b                	pshd	
 879  01ad cc0008            	ldd	#8
 880  01b0 3b                	pshd	
 881  01b1 c601              	ldab	#1
 882  01b3 4a000000          	call	f_diagF_DiagResponse
 884  01b7 1b86              	leas	6,s
 885                         ; 366 }
 888  01b9 0a                	rtc	
 934                         ; 372 void DESC_API_CALLBACK_TYPE ApplDescRead_220103_Rear_Seat_Switch_Input_Status(DescMsgContext* pMsgContext)
 934                         ; 373 {
 935                         	switch	.ftext
 936  01ba                   f_ApplDescRead_220103_Rear_Seat_Switch_Input_Status:
 938  01ba 3b                	pshd	
 939       00000000          OFST:	set	0
 942                         ; 375 	if( ((CSWM_config_c & CSWM_4HS_VARIANT_MASK_K) == CSWM_4HS_VARIANT_MASK_K) && (main_SwReq_Rear_c == VEH_SWITCH_REAR_ONLY_K) )
 944  01bb f60000            	ldab	_CSWM_config_c
 945  01be c40f              	andb	#15
 946  01c0 c10f              	cmpb	#15
 947  01c2 2652              	bne	L753
 949  01c4 f60000            	ldab	_main_SwReq_Rear_c
 950  01c7 c103              	cmpb	#3
 951  01c9 264b              	bne	L753
 952                         ; 378 		temp_c = 0;
 954  01cb 790001            	clr	L311_temp_c
 955                         ; 379 		pMsgContext->resData[0] = temp_c;
 957  01ce ed80              	ldy	OFST+0,s
 958  01d0 ee44              	ldx	4,y
 959  01d2 6900              	clr	0,x
 960                         ; 382 		temp_c = hs_rear_swth_stat_c[RL_SWITCH];
 962  01d4 f60000            	ldab	_hs_rear_swth_stat_c
 963  01d7 7b0001            	stab	L311_temp_c
 964                         ; 384 		pMsgContext->resData[0] = temp_c;
 966  01da 6b00              	stab	0,x
 967                         ; 386 		temp_c = 0;
 969  01dc 790001            	clr	L311_temp_c
 970                         ; 388 		temp_c = hs_rear_swth_stat_c[RR_SWITCH];
 972  01df f60001            	ldab	_hs_rear_swth_stat_c+1
 973  01e2 58                	lslb	
 974  01e3 58                	lslb	
 975  01e4 7b0001            	stab	L311_temp_c
 976                         ; 390 		temp_c<<=2;
 978                         ; 391 		pMsgContext->resData[0] |= temp_c;
 980  01e7 ea00              	orab	0,x
 981  01e9 6b00              	stab	0,x
 982                         ; 393 		temp_c = 0;
 984  01eb 790001            	clr	L311_temp_c
 985                         ; 395 		temp_c = hs_rear_swpsd_stat_c[RL_SWITCH];
 987  01ee f60000            	ldab	_hs_rear_swpsd_stat_c
 988                         ; 396 		temp_c<<=4;
 990  01f1 58                	lslb	
 991  01f2 58                	lslb	
 992  01f3 58                	lslb	
 993  01f4 58                	lslb	
 994  01f5 7b0001            	stab	L311_temp_c
 995                         ; 397 		pMsgContext->resData[0] |= temp_c;
 997  01f8 ea00              	orab	0,x
 998  01fa 6b00              	stab	0,x
 999                         ; 399 		temp_c = 0;
1001  01fc 790001            	clr	L311_temp_c
1002                         ; 401 		temp_c = hs_rear_swpsd_stat_c[RR_SWITCH];
1004  01ff f60001            	ldab	_hs_rear_swpsd_stat_c+1
1005                         ; 402 		temp_c<<=5;
1007  0202 58                	lslb	
1008  0203 58                	lslb	
1009  0204 58                	lslb	
1010  0205 58                	lslb	
1011  0206 58                	lslb	
1012  0207 7b0001            	stab	L311_temp_c
1013                         ; 403 		pMsgContext->resData[0] |= temp_c;
1015  020a ea00              	orab	0,x
1016  020c 6b00              	stab	0,x
1017                         ; 405 		DataLength = 1;
1019  020e cc0001            	ldd	#1
1020  0211 7c0002            	std	L111_DataLength
1021                         ; 406 		Response = RESPONSE_OK;
1024  0214 2008              	bra	L163
1025  0216                   L753:
1026                         ; 411        DataLength = 1;
1028  0216 cc0001            	ldd	#1
1029  0219 7c0002            	std	L111_DataLength
1030                         ; 412 	   Response = SUBFUNCTION_NOT_SUPPORTED;
1032  021c c607              	ldab	#7
1033  021e                   L163:
1034  021e 7b0000            	stab	L511_Response
1035                         ; 414     diagF_DiagResponse(Response, DataLength, pMsgContext);
1037  0221 ec80              	ldd	OFST+0,s
1038  0223 3b                	pshd	
1039  0224 fc0002            	ldd	L111_DataLength
1040  0227 3b                	pshd	
1041  0228 f60000            	ldab	L511_Response
1042  022b 87                	clra	
1043  022c 4a000000          	call	f_diagF_DiagResponse
1045  0230 1b86              	leas	6,s
1046                         ; 415 }
1049  0232 0a                	rtc	
1109                         ; 421 void DESC_API_CALLBACK_TYPE ApplDescRead_220104_Conditions_to_activate_CSWM_outputs(DescMsgContext* pMsgContext)
1109                         ; 422 {
1110                         	switch	.ftext
1111  0233                   f_ApplDescRead_220104_Conditions_to_activate_CSWM_outputs:
1113  0233 3b                	pshd	
1114       00000000          OFST:	set	0
1117                         ; 423     diag_srvc_22027E_byte1 = 0;
1119  0234 79000a            	clr	_diag_srvc_22027E_byte1
1120                         ; 424 	diag_srvc_22027E_byte2 = 0;
1122  0237 790009            	clr	_diag_srvc_22027E_byte2
1123                         ; 430 	diag_ldShd_Stat_c = BattVF_Get_LdShd_Mnt_Stat();
1125  023a 4a000000          	call	f_BattVF_Get_LdShd_Mnt_Stat
1127  023e 7b0005            	stab	_diag_ldShd_Stat_c
1128                         ; 431 	diag_PCB_tempStat_c = TempF_Get_PCB_NTC_Stat();
1130  0241 4a000000          	call	f_TempF_Get_PCB_NTC_Stat
1132  0245 7b0004            	stab	_diag_PCB_tempStat_c
1133                         ; 438 	if(canio_RX_IGN_STATUS_c == IGN_RUN){
1135  0248 f60000            	ldab	_canio_RX_IGN_STATUS_c
1136  024b c104              	cmpb	#4
1137  024d 2606              	bne	L304
1138                         ; 439 		diag_srvc_22027E_byte1 |= IGN_STAT_IS_RUN;
1140  024f 1c000a01          	bset	_diag_srvc_22027E_byte1,1
1142  0253 2004              	bra	L504
1143  0255                   L304:
1144                         ; 441 		diag_srvc_22027E_byte1 &= (~IGN_STAT_IS_RUN);
1146  0255 1d000a01          	bclr	_diag_srvc_22027E_byte1,1
1147  0259                   L504:
1148                         ; 445 	if(NTC_AD_Readings_c[FRONT_LEFT] >= hs_cal_prms.hs_prms[hs_vehicle_line_c].NTC_CutOFF_c[4]){
1150  0259 f60000            	ldab	_hs_vehicle_line_c
1151  025c 860e              	ldaa	#14
1152  025e 12                	mul	
1153  025f b746              	tfr	d,y
1154  0261 e6ea000c          	ldab	_hs_cal_prms+12,y
1155  0265 f10000            	cmpb	_NTC_AD_Readings_c
1156  0268 2206              	bhi	L704
1157                         ; 446 		diag_srvc_22027E_byte1 |= FL_HS_NTC_LESS_70C;						
1159  026a 1c000a02          	bset	_diag_srvc_22027E_byte1,2
1161  026e 2004              	bra	L114
1162  0270                   L704:
1163                         ; 448 		diag_srvc_22027E_byte1 &= (~FL_HS_NTC_LESS_70C);
1165  0270 1d000a02          	bclr	_diag_srvc_22027E_byte1,2
1166  0274                   L114:
1167                         ; 452 	if(NTC_AD_Readings_c[FRONT_RIGHT] >= hs_cal_prms.hs_prms[hs_vehicle_line_c].NTC_CutOFF_c[4]){
1169  0274 f60000            	ldab	_hs_vehicle_line_c
1170  0277 860e              	ldaa	#14
1171  0279 12                	mul	
1172  027a b746              	tfr	d,y
1173  027c e6ea000c          	ldab	_hs_cal_prms+12,y
1174  0280 f10001            	cmpb	_NTC_AD_Readings_c+1
1175  0283 2206              	bhi	L314
1176                         ; 453 		diag_srvc_22027E_byte1 |= FR_HS_NTC_LESS_70C;						
1178  0285 1c000a04          	bset	_diag_srvc_22027E_byte1,4
1180  0289 2004              	bra	L514
1181  028b                   L314:
1182                         ; 455 		diag_srvc_22027E_byte1 &= (~FR_HS_NTC_LESS_70C);
1184  028b 1d000a04          	bclr	_diag_srvc_22027E_byte1,4
1185  028f                   L514:
1186                         ; 459 	if(NTC_AD_Readings_c[REAR_LEFT] >= hs_cal_prms.hs_prms[hs_vehicle_line_c].NTC_CutOFF_c[4]){
1188  028f f60000            	ldab	_hs_vehicle_line_c
1189  0292 860e              	ldaa	#14
1190  0294 12                	mul	
1191  0295 b746              	tfr	d,y
1192  0297 e6ea000c          	ldab	_hs_cal_prms+12,y
1193  029b f10002            	cmpb	_NTC_AD_Readings_c+2
1194  029e 2206              	bhi	L714
1195                         ; 460 		diag_srvc_22027E_byte1 |= RL_HS_NTC_LESS_70C;						
1197  02a0 1c000a08          	bset	_diag_srvc_22027E_byte1,8
1199  02a4 2004              	bra	L124
1200  02a6                   L714:
1201                         ; 462 		diag_srvc_22027E_byte1 &= (~RL_HS_NTC_LESS_70C);
1203  02a6 1d000a08          	bclr	_diag_srvc_22027E_byte1,8
1204  02aa                   L124:
1205                         ; 466 	if(NTC_AD_Readings_c[REAR_RIGHT] >= hs_cal_prms.hs_prms[hs_vehicle_line_c].NTC_CutOFF_c[4]){
1207  02aa f60000            	ldab	_hs_vehicle_line_c
1208  02ad 860e              	ldaa	#14
1209  02af 12                	mul	
1210  02b0 b746              	tfr	d,y
1211  02b2 e6ea000c          	ldab	_hs_cal_prms+12,y
1212  02b6 f10003            	cmpb	_NTC_AD_Readings_c+3
1213  02b9 2206              	bhi	L324
1214                         ; 467 		diag_srvc_22027E_byte1 |= RR_HS_NTC_LESS_70C;						
1216  02bb 1c000a10          	bset	_diag_srvc_22027E_byte1,16
1218  02bf 2004              	bra	L524
1219  02c1                   L324:
1220                         ; 469 		diag_srvc_22027E_byte1 &= (~RR_HS_NTC_LESS_70C);
1222  02c1 1d000a10          	bclr	_diag_srvc_22027E_byte1,16
1223  02c5                   L524:
1224                         ; 473 	if(stW_temperature_c <= stW_prms.cals[0].MaxTemp_Threshld){
1226  02c5 f60000            	ldab	_stW_temperature_c
1227  02c8 f1000e            	cmpb	_stW_prms+14
1228  02cb 2206              	bhi	L724
1229                         ; 474 		diag_srvc_22027E_byte1 |= HSW_TEMP_LESS_50C;
1231  02cd 1c000a20          	bset	_diag_srvc_22027E_byte1,32
1233  02d1 2004              	bra	L134
1234  02d3                   L724:
1235                         ; 476 		diag_srvc_22027E_byte1 &= (~HSW_TEMP_LESS_50C);
1237  02d3 1d000a20          	bclr	_diag_srvc_22027E_byte1,32
1238  02d7                   L134:
1239                         ; 480 	if( (canio_RX_EngineSpeed_c > main_EngRpm_lowLmt_c) ){
1241  02d7 f60000            	ldab	_canio_RX_EngineSpeed_c
1242  02da f10000            	cmpb	_main_EngRpm_lowLmt_c
1243  02dd 2306              	bls	L334
1244                         ; 481 		diag_srvc_22027E_byte1 |= ENG_RPM_IN_RANGE;
1246  02df 1c000a40          	bset	_diag_srvc_22027E_byte1,64
1248  02e3 2004              	bra	L534
1249  02e5                   L334:
1250                         ; 483 		diag_srvc_22027E_byte1 &= (~ENG_RPM_IN_RANGE);
1252  02e5 1d000a40          	bclr	_diag_srvc_22027E_byte1,64
1253  02e9                   L534:
1254                         ; 487 	if( (BattV_set[SYSTEM_V].crt_vltg_c <= BattV_cals.Thrshld_a[SET_OV_LMT]) &&
1254                         ; 488 		(BattV_set[SYSTEM_V].crt_vltg_c >= BattV_cals.Thrshld_a[SET_UV_LMT]) &&
1254                         ; 489 		(BattV_set[BATTERY_V].crt_vltg_c <= BattV_cals.Thrshld_a[SET_OV_LMT]) && 
1254                         ; 490 		(BattV_set[BATTERY_V].crt_vltg_c >= BattV_cals.Thrshld_a[SET_UV_LMT]) ){
1256  02e9 f60000            	ldab	_BattV_set
1257  02ec f10000            	cmpb	_BattV_cals
1258  02ef 2218              	bhi	L734
1260  02f1 f10003            	cmpb	_BattV_cals+3
1261  02f4 2513              	blo	L734
1263  02f6 f6000a            	ldab	_BattV_set+10
1264  02f9 f10000            	cmpb	_BattV_cals
1265  02fc 220b              	bhi	L734
1267  02fe f10003            	cmpb	_BattV_cals+3
1268  0301 2506              	blo	L734
1269                         ; 491 		diag_srvc_22027E_byte1 |= VOLTAGE_IN_RANGE;
1271  0303 1c000a80          	bset	_diag_srvc_22027E_byte1,128
1273  0307 2004              	bra	L144
1274  0309                   L734:
1275                         ; 493 		diag_srvc_22027E_byte1 &= (~VOLTAGE_IN_RANGE);
1277  0309 1d000a80          	bclr	_diag_srvc_22027E_byte1,128
1278  030d                   L144:
1279                         ; 501 	if(diag_PCB_tempStat_c == PCB_STAT_OVERTEMP){
1281  030d f60004            	ldab	_diag_PCB_tempStat_c
1282  0310 c103              	cmpb	#3
1283  0312 2606              	bne	L344
1284                         ; 502 		diag_srvc_22027E_byte2 |= PCB_OVER_TEMP_PRESENT;
1286  0314 1c000901          	bset	_diag_srvc_22027E_byte2,1
1288  0318 2004              	bra	L544
1289  031a                   L344:
1290                         ; 504 		diag_srvc_22027E_byte2 &= (~PCB_OVER_TEMP_PRESENT);
1292  031a 1d000901          	bclr	_diag_srvc_22027E_byte2,1
1293  031e                   L544:
1294                         ; 508 	if(diag_ldShd_Stat_c != LDSHD_MNT_GOOD){
1296  031e f60005            	ldab	_diag_ldShd_Stat_c
1297  0321 2706              	beq	L744
1298                         ; 509 		diag_srvc_22027E_byte2 |= LOAD_SHED_ACTIVE;
1300  0323 1c000902          	bset	_diag_srvc_22027E_byte2,2
1302  0327 2004              	bra	L154
1303  0329                   L744:
1304                         ; 511 		diag_srvc_22027E_byte2 &= (~LOAD_SHED_ACTIVE);
1306  0329 1d000902          	bclr	_diag_srvc_22027E_byte2,2
1307  032d                   L154:
1308                         ; 515 	if( (canio_canbus_off_b == TRUE) || (final_FCM_LOC_b == TRUE) || (final_CCN_LOC_b == TRUE) || (final_HVAC_LOC_b == TRUE) || (final_CBC_LOC_b == TRUE) || (final_TGW_LOC_b == TRUE) ){
1310  032d 1e00000119        	brset	_canio_status2_flags_c,1,L554
1312  0332 1e00000414        	brset	_canio_status2_flags_c,4,L554
1314  0337 1e0000020f        	brset	_canio_status2_flags_c,2,L554
1316  033c 1e0000080a        	brset	_canio_status2_flags_c,8,L554
1318  0341 1e00001005        	brset	_canio_status2_flags_c,16,L554
1320  0346 1f00002015        	brclr	_canio_status2_flags_c,32,L354
1321  034b                   L554:
1322                         ; 516 		diag_srvc_22027E_byte2 |= CAN_COMM_OR_LOC_FLT;
1324  034b 1c000904          	bset	_diag_srvc_22027E_byte2,4
1326  034f                   L764:
1327                         ; 522 	if( (intFlt_bytes[ZERO] != NO_INT_FLT) || (intFlt_bytes[ONE] != NO_INT_FLT) || (intFlt_bytes[TWO] != NO_INT_FLT) ){
1329  034f f60000            	ldab	_intFlt_bytes
1330  0352 2612              	bne	L374
1332  0354 f60001            	ldab	_intFlt_bytes+1
1333  0357 260d              	bne	L374
1335  0359 f60002            	ldab	_intFlt_bytes+2
1336  035c 2735              	beq	L174
1337  035e 2006              	bra	L374
1338  0360                   L354:
1339                         ; 518 		diag_srvc_22027E_byte2 &= (~CAN_COMM_OR_LOC_FLT);
1341  0360 1d000904          	bclr	_diag_srvc_22027E_byte2,4
1342  0364 20e9              	bra	L764
1343  0366                   L374:
1344                         ; 523 		diag_srvc_22027E_byte2 |= INTERNAL_FLT_PRESENT;
1346  0366 1c000908          	bset	_diag_srvc_22027E_byte2,8
1348  036a                   L774:
1349                         ; 528 	pMsgContext->resData[0] = diag_srvc_22027E_byte1;
1351  036a f6000a            	ldab	_diag_srvc_22027E_byte1
1352  036d ed80              	ldy	OFST+0,s
1353  036f 6beb0004          	stab	[4,y]
1354                         ; 529 	pMsgContext->resData[1] = diag_srvc_22027E_byte2;					
1356  0373 ed44              	ldy	4,y
1357  0375 1809410009        	movb	_diag_srvc_22027E_byte2,1,y
1358                         ; 530 	DataLength = 2;
1360  037a cc0002            	ldd	#2
1361  037d 7c0002            	std	L111_DataLength
1362                         ; 531 	Response = RESPONSE_OK;
1364  0380 53                	decb	
1365  0381 7b0000            	stab	L511_Response
1366                         ; 532     diagF_DiagResponse(Response, DataLength, pMsgContext);
1368  0384 ec80              	ldd	OFST+0,s
1369  0386 3b                	pshd	
1370  0387 cc0002            	ldd	#2
1371  038a 3b                	pshd	
1372  038b 53                	decb	
1373  038c 4a000000          	call	f_diagF_DiagResponse
1375  0390 1b86              	leas	6,s
1376                         ; 533 }
1379  0392 0a                	rtc	
1380  0393                   L174:
1381                         ; 525 		diag_srvc_22027E_byte2 &= (~INTERNAL_FLT_PRESENT);
1383  0393 1d000908          	bclr	_diag_srvc_22027E_byte2,8
1384  0397 20d1              	bra	L774
1428                         ; 539 void DESC_API_CALLBACK_TYPE ApplDescRead_220105_Heated_Seat_NTC_Feedback_Sensor_Status(DescMsgContext* pMsgContext)
1428                         ; 540 {
1429                         	switch	.ftext
1430  0399                   f_ApplDescRead_220105_Heated_Seat_NTC_Feedback_Sensor_Status:
1432  0399 3b                	pshd	
1433       00000000          OFST:	set	0
1436                         ; 543 	pMsgContext->resData[0] = NTC_AD_Readings_c[FRONT_LEFT]; // F L Seat NTC Reading
1438  039a f60000            	ldab	_NTC_AD_Readings_c
1439  039d ed80              	ldy	OFST+0,s
1440  039f 6beb0004          	stab	[4,y]
1441                         ; 544 	pMsgContext->resData[1] = NTC_AD_Readings_c[FRONT_RIGHT]; // F R Seat NTC Reading
1443  03a3 ed44              	ldy	4,y
1444  03a5 1809410001        	movb	_NTC_AD_Readings_c+1,1,y
1445                         ; 545 	pMsgContext->resData[2] = NTC_AD_Readings_c[REAR_LEFT];  // R L Seat NTC Reading
1447  03aa 1809420002        	movb	_NTC_AD_Readings_c+2,2,y
1448                         ; 546 	pMsgContext->resData[3] = NTC_AD_Readings_c[REAR_RIGHT];  // R R Seat NTC Reading
1450  03af 1809430003        	movb	_NTC_AD_Readings_c+3,3,y
1451                         ; 549 	pMsgContext->resData[4] = TempF_NtcAdStd_to_CanTemp(NTC_AD_Readings_c[FRONT_LEFT]); 
1453  03b4 87                	clra	
1454  03b5 4a000000          	call	f_TempF_NtcAdStd_to_CanTemp
1456  03b9 ed80              	ldy	OFST+0,s
1457  03bb ed44              	ldy	4,y
1458  03bd 6b44              	stab	4,y
1459                         ; 550 	pMsgContext->resData[5] = TempF_NtcAdStd_to_CanTemp(NTC_AD_Readings_c[FRONT_RIGHT]);
1461  03bf f60001            	ldab	_NTC_AD_Readings_c+1
1462  03c2 87                	clra	
1463  03c3 4a000000          	call	f_TempF_NtcAdStd_to_CanTemp
1465  03c7 ed80              	ldy	OFST+0,s
1466  03c9 ed44              	ldy	4,y
1467  03cb 6b45              	stab	5,y
1468                         ; 551 	pMsgContext->resData[6] = TempF_NtcAdStd_to_CanTemp(NTC_AD_Readings_c[REAR_LEFT]);  
1470  03cd f60002            	ldab	_NTC_AD_Readings_c+2
1471  03d0 87                	clra	
1472  03d1 4a000000          	call	f_TempF_NtcAdStd_to_CanTemp
1474  03d5 ed80              	ldy	OFST+0,s
1475  03d7 ed44              	ldy	4,y
1476  03d9 6b46              	stab	6,y
1477                         ; 552 	pMsgContext->resData[7] = TempF_NtcAdStd_to_CanTemp(NTC_AD_Readings_c[REAR_RIGHT]);
1479  03db f60003            	ldab	_NTC_AD_Readings_c+3
1480  03de 87                	clra	
1481  03df 4a000000          	call	f_TempF_NtcAdStd_to_CanTemp
1483  03e3 ed80              	ldy	OFST+0,s
1484  03e5 ed44              	ldy	4,y
1485  03e7 6b47              	stab	7,y
1486                         ; 554 	DataLength = 8;
1488  03e9 cc0008            	ldd	#8
1489  03ec 7c0002            	std	L111_DataLength
1490                         ; 555 	Response = RESPONSE_OK;
1492  03ef c601              	ldab	#1
1493  03f1 7b0000            	stab	L511_Response
1494                         ; 556 	diagF_DiagResponse(Response, DataLength, pMsgContext);
1496  03f4 ec80              	ldd	OFST+0,s
1497  03f6 3b                	pshd	
1498  03f7 cc0008            	ldd	#8
1499  03fa 3b                	pshd	
1500  03fb c601              	ldab	#1
1501  03fd 4a000000          	call	f_diagF_DiagResponse
1503  0401 1b86              	leas	6,s
1504                         ; 557 }
1507  0403 0a                	rtc	
1552                         ; 563 void DESC_API_CALLBACK_TYPE ApplDescRead_220106_HeatVentWheel_Current_Sense(DescMsgContext* pMsgContext)
1552                         ; 564 {
1553                         	switch	.ftext
1554  0404                   f_ApplDescRead_220106_HeatVentWheel_Current_Sense:
1556  0404 3b                	pshd	
1557       00000000          OFST:	set	0
1560                         ; 566     pMsgContext->resData[0] = hs_Isense_c[FRONT_LEFT]; //Front Left Heater Current Sense Reading 
1562  0405 f60000            	ldab	_hs_Isense_c
1563  0408 ed80              	ldy	OFST+0,s
1564  040a 6beb0004          	stab	[4,y]
1565                         ; 567 	pMsgContext->resData[1] = hs_Isense_c[FRONT_RIGHT]; //Front Right Heater Current Sense Reading
1567  040e ed44              	ldy	4,y
1568  0410 1809410001        	movb	_hs_Isense_c+1,1,y
1569                         ; 569 	if ((CSWM_config_c & CSWM_4HS_VARIANT_MASK_K) == CSWM_4HS_VARIANT_MASK_K)
1571  0415 1e00000f02        	brset	_CSWM_config_c,15,LC004
1572  041a 200c              	bra	L145
1573  041c                   LC004:
1574                         ; 571 		pMsgContext->resData[2] = hs_Isense_c[REAR_LEFT]; //Rear Left Heater Current Sense Reading
1576  041c 1809420002        	movb	_hs_Isense_c+2,2,y
1577                         ; 572 		pMsgContext->resData[3] = hs_Isense_c[REAR_RIGHT]; //Rear Right Heater Current Sense Reading
1579  0421 1809430003        	movb	_hs_Isense_c+3,3,y
1581  0426 2004              	bra	L345
1582  0428                   L145:
1583                         ; 576 		pMsgContext->resData[2] = 0x00;
1585                         ; 577 		pMsgContext->resData[3] = 0x00;
1587  0428 87                	clra	
1588  0429 c7                	clrb	
1589  042a 6c42              	std	2,y
1590  042c                   L345:
1591                         ; 579 	if ((CSWM_config_c & CSWM_HSW_MASK_K) == CSWM_HSW_MASK_K)
1593  042c ed80              	ldy	OFST+0,s
1594  042e ed44              	ldy	4,y
1595  0430 1f00004007        	brclr	_CSWM_config_c,64,L545
1596                         ; 581 		pMsgContext->resData[4] = stW_Isense_c; //Steering Wheel Current Sense Reading
1598  0435 1809440000        	movb	_stW_Isense_c,4,y
1600  043a 2002              	bra	L745
1601  043c                   L545:
1602                         ; 585 		pMsgContext->resData[4] = 0x00;
1604  043c 6944              	clr	4,y
1605  043e                   L745:
1606                         ; 587 	if((CSWM_config_c & CSWM_2VS_MASK_K) == CSWM_2VS_MASK_K)
1608  043e 1e00003002        	brset	_CSWM_config_c,48,LC005
1609  0443 2010              	bra	L155
1610  0445                   LC005:
1611                         ; 589 	    pMsgContext->resData[5] = vs_Isense_c[VS_FL]; // VL Isense
1613  0445 ed80              	ldy	OFST+0,s
1614  0447 ed44              	ldy	4,y
1615  0449 1809450000        	movb	_vs_Isense_c,5,y
1616                         ; 590 	    pMsgContext->resData[6] = vs_Isense_c[VS_FR]; // VR Isense
1618  044e 1809460001        	movb	_vs_Isense_c+1,6,y
1620  0453 2008              	bra	L355
1621  0455                   L155:
1622                         ; 594 		pMsgContext->resData[5] = 0x00;
1624  0455 ed80              	ldy	OFST+0,s
1625  0457 ed44              	ldy	4,y
1626                         ; 595 		pMsgContext->resData[6] = 0x00;
1628  0459 87                	clra	
1629  045a c7                	clrb	
1630  045b 6c45              	std	5,y
1631  045d                   L355:
1632                         ; 598 	DataLength = 7;
1634  045d cc0007            	ldd	#7
1635  0460 7c0002            	std	L111_DataLength
1636                         ; 599 	Response = RESPONSE_OK;
1638  0463 c601              	ldab	#1
1639  0465 7b0000            	stab	L511_Response
1640                         ; 600 	diagF_DiagResponse(Response, DataLength, pMsgContext);
1642  0468 ec80              	ldd	OFST+0,s
1643  046a 3b                	pshd	
1644  046b cc0007            	ldd	#7
1645  046e 3b                	pshd	
1646  046f c601              	ldab	#1
1647  0471 4a000000          	call	f_diagF_DiagResponse
1649  0475 1b86              	leas	6,s
1650                         ; 601 }
1653  0477 0a                	rtc	
1696                         ; 607 void DESC_API_CALLBACK_TYPE ApplDescRead_220107_Vented_Seats_Voltage_Sense(DescMsgContext* pMsgContext)
1696                         ; 608 {
1697                         	switch	.ftext
1698  0478                   f_ApplDescRead_220107_Vented_Seats_Voltage_Sense:
1700  0478 3b                	pshd	
1701       00000000          OFST:	set	0
1704                         ; 609 	if((CSWM_config_c & CSWM_2VS_MASK_K) == CSWM_2VS_MASK_K)
1706  0479 1e00003002        	brset	_CSWM_config_c,48,LC006
1707  047e 2012              	bra	L575
1708  0480                   LC006:
1709                         ; 612 		pMsgContext->resData[0] = vs_Vsense_c[VS_FL]; // VL Vsense 
1711  0480 f60000            	ldab	_vs_Vsense_c
1712  0483 ed80              	ldy	OFST+0,s
1713  0485 6beb0004          	stab	[4,y]
1714                         ; 613 		pMsgContext->resData[1] = vs_Vsense_c[VS_FR]; // VR Vsense
1716  0489 ed44              	ldy	4,y
1717  048b 1809410001        	movb	_vs_Vsense_c+1,1,y
1719  0490 200a              	bra	L775
1720  0492                   L575:
1721                         ; 617 		pMsgContext->resData[0] = 0x00;
1723  0492 b746              	tfr	d,y
1724  0494 69eb0004          	clr	[4,y]
1725                         ; 618 		pMsgContext->resData[1] = 0x00;
1727  0498 ed44              	ldy	4,y
1728  049a 6941              	clr	1,y
1729  049c                   L775:
1730                         ; 621 	DataLength = 2;
1732  049c cc0002            	ldd	#2
1733  049f 7c0002            	std	L111_DataLength
1734                         ; 622 	Response = RESPONSE_OK;
1736  04a2 53                	decb	
1737  04a3 7b0000            	stab	L511_Response
1738                         ; 623 	diagF_DiagResponse(Response, DataLength, pMsgContext);
1740  04a6 ec80              	ldd	OFST+0,s
1741  04a8 3b                	pshd	
1742  04a9 cc0002            	ldd	#2
1743  04ac 3b                	pshd	
1744  04ad 53                	decb	
1745  04ae 4a000000          	call	f_diagF_DiagResponse
1747  04b2 1b86              	leas	6,s
1748                         ; 624 }
1751  04b4 0a                	rtc	
1792                         ; 635 void DESC_API_CALLBACK_TYPE ApplDescReadECU_time_stamps_RAM(DescMsgContext* pMsgContext)
1792                         ; 636 {
1793                         	switch	.ftext
1794  04b5                   f_ApplDescReadECU_time_stamps_RAM:
1796  04b5 3b                	pshd	
1797       00000000          OFST:	set	0
1800                         ; 638 	pMsgContext->resData[0] = fmem_lifetime_l.lw.hhbyte;
1802  04b6 f60000            	ldab	_fmem_lifetime_l
1803  04b9 ed80              	ldy	OFST+0,s
1804  04bb 6beb0004          	stab	[4,y]
1805                         ; 639 	pMsgContext->resData[1] = fmem_lifetime_l.lw.hbyte;
1807  04bf ed44              	ldy	4,y
1808  04c1 1809410001        	movb	_fmem_lifetime_l+1,1,y
1809                         ; 640 	pMsgContext->resData[2] = fmem_lifetime_l.lw.lbyte;
1811  04c6 1809420002        	movb	_fmem_lifetime_l+2,2,y
1812                         ; 641 	pMsgContext->resData[3] = fmem_lifetime_l.lw.llbyte;
1814  04cb 1809430003        	movb	_fmem_lifetime_l+3,3,y
1815                         ; 643 	DataLength = 4;
1817  04d0 cc0004            	ldd	#4
1818  04d3 7c0002            	std	L111_DataLength
1819                         ; 644 	Response = RESPONSE_OK;
1821  04d6 c601              	ldab	#1
1822  04d8 7b0000            	stab	L511_Response
1823                         ; 645 	diagF_DiagResponse(Response, DataLength, pMsgContext);
1825  04db ec80              	ldd	OFST+0,s
1826  04dd 3b                	pshd	
1827  04de cc0004            	ldd	#4
1828  04e1 3b                	pshd	
1829  04e2 c601              	ldab	#1
1830  04e4 4a000000          	call	f_diagF_DiagResponse
1832  04e8 1b86              	leas	6,s
1833                         ; 646 }
1836  04ea 0a                	rtc	
1878                         ; 658 void DESC_API_CALLBACK_TYPE ApplDescReadECU_time_stamps_from_KeyOn_RAM(DescMsgContext* pMsgContext)
1878                         ; 659 {
1879                         	switch	.ftext
1880  04eb                   f_ApplDescReadECU_time_stamps_from_KeyOn_RAM:
1882  04eb 3b                	pshd	
1883       00000000          OFST:	set	0
1886                         ; 662 	pMsgContext->resData[0] = HIGHBYTE(fmem_keyon_time_w);
1888  04ec f60000            	ldab	_fmem_keyon_time_w
1889  04ef ed80              	ldy	OFST+0,s
1890  04f1 6beb0004          	stab	[4,y]
1891                         ; 663 	pMsgContext->resData[1] = LOWBYTE(fmem_keyon_time_w);
1893  04f5 ed44              	ldy	4,y
1894  04f7 1809410001        	movb	_fmem_keyon_time_w+1,1,y
1895                         ; 665 	DataLength = 2;
1897  04fc cc0002            	ldd	#2
1898  04ff 7c0002            	std	L111_DataLength
1899                         ; 666 	Response = RESPONSE_OK;
1901  0502 53                	decb	
1902  0503 7b0000            	stab	L511_Response
1903                         ; 667 	diagF_DiagResponse(Response, DataLength, pMsgContext);
1905  0506 ec80              	ldd	OFST+0,s
1906  0508 3b                	pshd	
1907  0509 cc0002            	ldd	#2
1908  050c 3b                	pshd	
1909  050d 53                	decb	
1910  050e 4a000000          	call	f_diagF_DiagResponse
1912  0512 1b86              	leas	6,s
1913                         ; 668 }
1916  0514 0a                	rtc	
1958                         ; 680 void DESC_API_CALLBACK_TYPE ApplDescRead_22102A_Check_EOL_configuration_data(DescMsgContext* pMsgContext)
1958                         ; 681 {
1959                         	switch	.ftext
1960  0515                   f_ApplDescRead_22102A_Check_EOL_configuration_data:
1962  0515 3b                	pshd	
1963       00000000          OFST:	set	0
1966                         ; 683 	EE_BlockRead(EE_PROXI_FAILURE, (u_8Bit*)&pMsgContext->resData[0]);
1968  0516 b746              	tfr	d,y
1969  0518 ec44              	ldd	4,y
1970  051a 3b                	pshd	
1971  051b cc001e            	ldd	#30
1972  051e 4a000000          	call	f_EE_BlockRead
1974  0522 1b82              	leas	2,s
1975                         ; 684 	DataLength = 9;
1977  0524 cc0009            	ldd	#9
1978  0527 7c0002            	std	L111_DataLength
1979                         ; 685 	Response = RESPONSE_OK;
1981  052a c601              	ldab	#1
1982  052c 7b0000            	stab	L511_Response
1983                         ; 686 	diagF_DiagResponse(Response, DataLength, pMsgContext);
1985  052f ec80              	ldd	OFST+0,s
1986  0531 3b                	pshd	
1987  0532 cc0009            	ldd	#9
1988  0535 3b                	pshd	
1989  0536 c601              	ldab	#1
1990  0538 4a000000          	call	f_diagF_DiagResponse
1992  053c 1b86              	leas	6,s
1993                         ; 688 }
1996  053e 0a                	rtc	
2037                         ; 695 void DESC_API_CALLBACK_TYPE ApplDescReadOdometer(DescMsgContext* pMsgContext)
2037                         ; 696 {
2038                         	switch	.ftext
2039  053f                   f_ApplDescReadOdometer:
2041  053f 3b                	pshd	
2042       00000000          OFST:	set	0
2045                         ; 701 	pMsgContext->resData[0] = canio_RX_TotalOdo_c[0];
2047  0540 f60000            	ldab	_canio_RX_TotalOdo_c
2048  0543 ed80              	ldy	OFST+0,s
2049  0545 6beb0004          	stab	[4,y]
2050                         ; 702 	pMsgContext->resData[1] = canio_RX_TotalOdo_c[1];
2052  0549 ed44              	ldy	4,y
2053  054b 1809410001        	movb	_canio_RX_TotalOdo_c+1,1,y
2054                         ; 703 	pMsgContext->resData[2] = canio_RX_TotalOdo_c[2];
2056  0550 1809420002        	movb	_canio_RX_TotalOdo_c+2,2,y
2057                         ; 705 	DataLength = 3;
2059  0555 cc0003            	ldd	#3
2060  0558 7c0002            	std	L111_DataLength
2061                         ; 706 	Response = RESPONSE_OK;
2063  055b c601              	ldab	#1
2064  055d 7b0000            	stab	L511_Response
2065                         ; 707 	diagF_DiagResponse(Response, DataLength, pMsgContext);
2067  0560 ec80              	ldd	OFST+0,s
2068  0562 3b                	pshd	
2069  0563 cc0003            	ldd	#3
2070  0566 3b                	pshd	
2071  0567 c601              	ldab	#1
2072  0569 4a000000          	call	f_diagF_DiagResponse
2074  056d 1b86              	leas	6,s
2075                         ; 708 }
2078  056f 0a                	rtc	
2121                         ; 715 void DESC_API_CALLBACK_TYPE ApplDescReadOdometer_content_to_the_last_F_ROM_updating(DescMsgContext* pMsgContext)
2121                         ; 716 {
2122                         	switch	.ftext
2123  0570                   f_ApplDescReadOdometer_content_to_the_last_F_ROM_updating:
2125  0570 3b                	pshd	
2126       00000000          OFST:	set	0
2129                         ; 718 	EE_BlockRead(EE_ODOMETER_LAST_PGM, (u_8Bit*)&pMsgContext->resData[0]);
2131  0571 b746              	tfr	d,y
2132  0573 ec44              	ldd	4,y
2133  0575 3b                	pshd	
2134  0576 cc0026            	ldd	#38
2135  0579 4a000000          	call	f_EE_BlockRead
2137  057d 1b82              	leas	2,s
2138                         ; 720 	DataLength = 3;
2140  057f cc0003            	ldd	#3
2141  0582 7c0002            	std	L111_DataLength
2142                         ; 721 	Response = RESPONSE_OK;
2144  0585 c601              	ldab	#1
2145  0587 7b0000            	stab	L511_Response
2146                         ; 722 	diagF_DiagResponse(Response, DataLength, pMsgContext);
2148  058a ec80              	ldd	OFST+0,s
2149  058c 3b                	pshd	
2150  058d cc0003            	ldd	#3
2151  0590 3b                	pshd	
2152  0591 c601              	ldab	#1
2153  0593 4a000000          	call	f_diagF_DiagResponse
2155  0597 1b86              	leas	6,s
2156                         ; 723 }
2159  0599 0a                	rtc	
2201                         ; 730 void DESC_API_CALLBACK_TYPE ApplDescReadNumber_of_FlashRom_re_writings_F_ROM(DescMsgContext* pMsgContext)
2201                         ; 731 {
2202                         	switch	.ftext
2203  059a                   f_ApplDescReadNumber_of_FlashRom_re_writings_F_ROM:
2205  059a 3b                	pshd	
2206       00000000          OFST:	set	0
2209                         ; 734 	EE_BlockRead(EE_FBL_PROG_ATTEMPTS, (u_8Bit*)&pMsgContext->resData[0]);
2211  059b b746              	tfr	d,y
2212  059d ec44              	ldd	4,y
2213  059f 3b                	pshd	
2214  05a0 cc0034            	ldd	#52
2215  05a3 4a000000          	call	f_EE_BlockRead
2217  05a7 1b82              	leas	2,s
2218                         ; 736 	DataLength = 1;
2220  05a9 cc0001            	ldd	#1
2221  05ac 7c0002            	std	L111_DataLength
2222                         ; 737 	Response = RESPONSE_OK;
2224  05af 7b0000            	stab	L511_Response
2225                         ; 738 	diagF_DiagResponse(Response, DataLength, pMsgContext);
2227  05b2 ec80              	ldd	OFST+0,s
2228  05b4 3b                	pshd	
2229  05b5 cc0001            	ldd	#1
2230  05b8 3b                	pshd	
2231  05b9 4a000000          	call	f_diagF_DiagResponse
2233  05bd 1b86              	leas	6,s
2234                         ; 739 }
2237  05bf 0a                	rtc	
2278                         ; 753 void DESC_API_CALLBACK_TYPE ApplDescReadECU_time_stamps_EEPROM(DescMsgContext* pMsgContext)
2278                         ; 754 {
2279                         	switch	.ftext
2280  05c0                   f_ApplDescReadECU_time_stamps_EEPROM:
2282  05c0 3b                	pshd	
2283       00000000          OFST:	set	0
2286                         ; 757 	EE_BlockRead(EE_ECU_TIMESTAMPS, (u_8Bit*)&pMsgContext->resData[0]); //Read the stored value of life time from EEPROM
2288  05c1 b746              	tfr	d,y
2289  05c3 ec44              	ldd	4,y
2290  05c5 3b                	pshd	
2291  05c6 cc0027            	ldd	#39
2292  05c9 4a000000          	call	f_EE_BlockRead
2294  05cd 1b82              	leas	2,s
2295                         ; 759 	DataLength = 4;
2297  05cf cc0004            	ldd	#4
2298  05d2 7c0002            	std	L111_DataLength
2299                         ; 760 	Response = RESPONSE_OK;
2301  05d5 c601              	ldab	#1
2302  05d7 7b0000            	stab	L511_Response
2303                         ; 761 	diagF_DiagResponse(Response, DataLength, pMsgContext);
2305  05da ec80              	ldd	OFST+0,s
2306  05dc 3b                	pshd	
2307  05dd cc0004            	ldd	#4
2308  05e0 3b                	pshd	
2309  05e1 c601              	ldab	#1
2310  05e3 4a000000          	call	f_diagF_DiagResponse
2312  05e7 1b86              	leas	6,s
2313                         ; 762 }
2316  05e9 0a                	rtc	
2358                         ; 776 void DESC_API_CALLBACK_TYPE ApplDescReadECU_time_stamps_from_KeyOn_EEPROM(DescMsgContext* pMsgContext)
2358                         ; 777 {
2359                         	switch	.ftext
2360  05ea                   f_ApplDescReadECU_time_stamps_from_KeyOn_EEPROM:
2362  05ea 3b                	pshd	
2363       00000000          OFST:	set	0
2366                         ; 779 	EE_BlockRead(EE_ECU_TIME_KEYON, (u_8Bit*)&pMsgContext->resData[0]);
2368  05eb b746              	tfr	d,y
2369  05ed ec44              	ldd	4,y
2370  05ef 3b                	pshd	
2371  05f0 cc0028            	ldd	#40
2372  05f3 4a000000          	call	f_EE_BlockRead
2374  05f7 1b82              	leas	2,s
2375                         ; 781 	DataLength = 2;
2377  05f9 cc0002            	ldd	#2
2378  05fc 7c0002            	std	L111_DataLength
2379                         ; 782 	Response = RESPONSE_OK;
2381  05ff 53                	decb	
2382  0600 7b0000            	stab	L511_Response
2383                         ; 783 	diagF_DiagResponse(Response, DataLength, pMsgContext);
2385  0603 ec80              	ldd	OFST+0,s
2386  0605 3b                	pshd	
2387  0606 cc0002            	ldd	#2
2388  0609 3b                	pshd	
2389  060a 53                	decb	
2390  060b 4a000000          	call	f_diagF_DiagResponse
2392  060f 1b86              	leas	6,s
2393                         ; 784 }
2396  0611 0a                	rtc	
2437                         ; 793 void DESC_API_CALLBACK_TYPE ApplDescReadKeyOn_counter_EEPROM(DescMsgContext* pMsgContext)
2437                         ; 794 {
2438                         	switch	.ftext
2439  0612                   f_ApplDescReadKeyOn_counter_EEPROM:
2441  0612 3b                	pshd	
2442       00000000          OFST:	set	0
2445                         ; 797 	EE_BlockRead(EE_ECU_KEYON_COUNTER, (u_8Bit*)&pMsgContext->resData[0]);
2447  0613 b746              	tfr	d,y
2448  0615 ec44              	ldd	4,y
2449  0617 3b                	pshd	
2450  0618 cc0029            	ldd	#41
2451  061b 4a000000          	call	f_EE_BlockRead
2453  061f 1b82              	leas	2,s
2454                         ; 799 	DataLength = 2;
2456  0621 cc0002            	ldd	#2
2457  0624 7c0002            	std	L111_DataLength
2458                         ; 800 	Response = RESPONSE_OK;
2460  0627 53                	decb	
2461  0628 7b0000            	stab	L511_Response
2462                         ; 801 	diagF_DiagResponse(Response, DataLength, pMsgContext);
2464  062b ec80              	ldd	OFST+0,s
2465  062d 3b                	pshd	
2466  062e cc0002            	ldd	#2
2467  0631 3b                	pshd	
2468  0632 53                	decb	
2469  0633 4a000000          	call	f_diagF_DiagResponse
2471  0637 1b86              	leas	6,s
2472                         ; 802 }
2475  0639 0a                	rtc	
2517                         ; 810 void DESC_API_CALLBACK_TYPE ApplDescReadECU_Time_first_DTC_detection_EEPROM(DescMsgContext* pMsgContext)
2517                         ; 811 {
2518                         	switch	.ftext
2519  063a                   f_ApplDescReadECU_Time_first_DTC_detection_EEPROM:
2521  063a 3b                	pshd	
2522       00000000          OFST:	set	0
2525                         ; 814 	EE_BlockRead(EE_ECU_TIME_FIRSTDTC, (u_8Bit*)&pMsgContext->resData[0]); 
2527  063b b746              	tfr	d,y
2528  063d ec44              	ldd	4,y
2529  063f 3b                	pshd	
2530  0640 cc002a            	ldd	#42
2531  0643 4a000000          	call	f_EE_BlockRead
2533  0647 1b82              	leas	2,s
2534                         ; 816 	DataLength = 4;
2536  0649 cc0004            	ldd	#4
2537  064c 7c0002            	std	L111_DataLength
2538                         ; 817 	Response = RESPONSE_OK;
2540  064f c601              	ldab	#1
2541  0651 7b0000            	stab	L511_Response
2542                         ; 818 	diagF_DiagResponse(Response, DataLength, pMsgContext);
2544  0654 ec80              	ldd	OFST+0,s
2545  0656 3b                	pshd	
2546  0657 cc0004            	ldd	#4
2547  065a 3b                	pshd	
2548  065b c601              	ldab	#1
2549  065d 4a000000          	call	f_diagF_DiagResponse
2551  0661 1b86              	leas	6,s
2552                         ; 819 }
2555  0663 0a                	rtc	
2598                         ; 827 void DESC_API_CALLBACK_TYPE ApplDescReadECU_Time_Stamps_from_KeyOn_first_DTC_detection_EEPROM(DescMsgContext* pMsgContext)
2598                         ; 828 {
2599                         	switch	.ftext
2600  0664                   f_ApplDescReadECU_Time_Stamps_from_KeyOn_first_DTC_detection_EEPROM:
2602  0664 3b                	pshd	
2603       00000000          OFST:	set	0
2606                         ; 830 	EE_BlockRead(EE_ECU_TIME_KEYON_FIRSTDTC, (u_8Bit*)&pMsgContext->resData[0]); 
2608  0665 b746              	tfr	d,y
2609  0667 ec44              	ldd	4,y
2610  0669 3b                	pshd	
2611  066a cc002b            	ldd	#43
2612  066d 4a000000          	call	f_EE_BlockRead
2614  0671 1b82              	leas	2,s
2615                         ; 832 	DataLength = 2;
2617  0673 cc0002            	ldd	#2
2618  0676 7c0002            	std	L111_DataLength
2619                         ; 833 	Response = RESPONSE_OK;
2621  0679 53                	decb	
2622  067a 7b0000            	stab	L511_Response
2623                         ; 834 	diagF_DiagResponse(Response, DataLength, pMsgContext);
2625  067d ec80              	ldd	OFST+0,s
2626  067f 3b                	pshd	
2627  0680 cc0002            	ldd	#2
2628  0683 3b                	pshd	
2629  0684 53                	decb	
2630  0685 4a000000          	call	f_diagF_DiagResponse
2632  0689 1b86              	leas	6,s
2633                         ; 835 }
2636  068b 0a                	rtc	
2677                         ; 844 void DESC_API_CALLBACK_TYPE ApplDescReadKeyOn_Counter_Status_EEPROM(DescMsgContext* pMsgContext)
2677                         ; 845 {
2678                         	switch	.ftext
2679  068c                   f_ApplDescReadKeyOn_Counter_Status_EEPROM:
2681  068c 3b                	pshd	
2682       00000000          OFST:	set	0
2685                         ; 851 	DataLength = 1;
2687  068d cc0001            	ldd	#1
2688  0690 7c0002            	std	L111_DataLength
2689                         ; 852 	Response = RESPONSE_OK; 
2691  0693 7b0000            	stab	L511_Response
2692                         ; 853 	diagF_DiagResponse(Response, DataLength, pMsgContext);
2694  0696 ec80              	ldd	OFST+0,s
2695  0698 3b                	pshd	
2696  0699 cc0001            	ldd	#1
2697  069c 3b                	pshd	
2698  069d 4a000000          	call	f_diagF_DiagResponse
2700  06a1 1b86              	leas	6,s
2701                         ; 854 }
2704  06a3 0a                	rtc	
2746                         ; 863 void DESC_API_CALLBACK_TYPE ApplDescReadProgrammingStatus_EEPROM_FLASH(DescMsgContext* pMsgContext)
2746                         ; 864 {
2747                         	switch	.ftext
2748  06a4                   f_ApplDescReadProgrammingStatus_EEPROM_FLASH:
2750  06a4 3b                	pshd	
2751       00000000          OFST:	set	0
2754                         ; 867 	EE_BlockRead(EE_FBL_PROG_STATUS, (u_8Bit*)&pMsgContext->resData[0]); 
2756  06a5 b746              	tfr	d,y
2757  06a7 ec44              	ldd	4,y
2758  06a9 3b                	pshd	
2759  06aa cc0035            	ldd	#53
2760  06ad 4a000000          	call	f_EE_BlockRead
2762  06b1 1b82              	leas	2,s
2763                         ; 869 	DataLength = 4;
2765  06b3 cc0004            	ldd	#4
2766  06b6 7c0002            	std	L111_DataLength
2767                         ; 870 	Response = RESPONSE_OK;
2769  06b9 c601              	ldab	#1
2770  06bb 7b0000            	stab	L511_Response
2771                         ; 871 	diagF_DiagResponse(Response, DataLength, pMsgContext);
2773  06be ec80              	ldd	OFST+0,s
2774  06c0 3b                	pshd	
2775  06c1 cc0004            	ldd	#4
2776  06c4 3b                	pshd	
2777  06c5 c601              	ldab	#1
2778  06c7 4a000000          	call	f_diagF_DiagResponse
2780  06cb 1b86              	leas	6,s
2781                         ; 872 }
2784  06cd 0a                	rtc	
2825                         ; 877 void DESC_API_CALLBACK_TYPE ApplDescRead_2E2023_System_Configuration_PROXI(DescMsgContext* pMsgContext)
2825                         ; 878 {
2826                         	switch	.ftext
2827  06ce                   f_ApplDescRead_2E2023_System_Configuration_PROXI:
2829  06ce 3b                	pshd	
2830       00000000          OFST:	set	0
2833                         ; 880 	DataLength = 255;
2835  06cf cc00ff            	ldd	#255
2836  06d2 7c0002            	std	L111_DataLength
2837                         ; 881 	Response = RESPONSE_OK;
2839  06d5 c601              	ldab	#1
2840  06d7 7b0000            	stab	L511_Response
2841                         ; 882 	diagF_DiagResponse(Response, DataLength, pMsgContext);
2843  06da ec80              	ldd	OFST+0,s
2844  06dc 3b                	pshd	
2845  06dd cc00ff            	ldd	#255
2846  06e0 3b                	pshd	
2847  06e1 c601              	ldab	#1
2848  06e3 4a000000          	call	f_diagF_DiagResponse
2850  06e7 1b86              	leas	6,s
2851                         ; 884 }
2854  06e9 0a                	rtc	
2897                         ; 889 void DESC_API_CALLBACK_TYPE ApplDescRead_222024_Received_PROXI_Information(DescMsgContext* pMsgContext)
2897                         ; 890 {
2898                         	switch	.ftext
2899  06ea                   f_ApplDescRead_222024_Received_PROXI_Information:
2901  06ea 3b                	pshd	
2902       00000000          OFST:	set	0
2905                         ; 894 	temp_c = 0;
2907  06eb 790001            	clr	L311_temp_c
2908                         ; 896 	temp_c = ru_eep_PROXI_Cfg.s.PROXI_IDVeh.s.Digit11;
2910  06ee f60000            	ldab	_ru_eep_PROXI_Cfg
2911                         ; 897     temp_c <<= 4;
2913  06f1 c4f0              	andb	#240
2914  06f3 7b0001            	stab	L311_temp_c
2915                         ; 899   	pMsgContext->resData[0] =  temp_c;	
2917  06f6 ed80              	ldy	OFST+0,s
2918  06f8 ee44              	ldx	4,y
2919  06fa 6b00              	stab	0,x
2920                         ; 901 	pMsgContext->resData[0] |= ru_eep_PROXI_Cfg.s.PROXI_IDVeh.s.Digit10;
2922  06fc f60000            	ldab	_ru_eep_PROXI_Cfg
2923  06ff c40f              	andb	#15
2924  0701 ea00              	orab	0,x
2925  0703 6b00              	stab	0,x
2926                         ; 903 	temp_c = 0;
2928  0705 790001            	clr	L311_temp_c
2929                         ; 905 	temp_c = ru_eep_PROXI_Cfg.s.PROXI_IDVeh.s.Digit9;
2931  0708 f60001            	ldab	_ru_eep_PROXI_Cfg+1
2932                         ; 906     temp_c <<= 4;
2934  070b c4f0              	andb	#240
2935  070d 7b0001            	stab	L311_temp_c
2936                         ; 908   	pMsgContext->resData[1] =  temp_c;	
2938  0710 ed44              	ldy	4,y
2939  0712 6b41              	stab	1,y
2940                         ; 910 	pMsgContext->resData[1] |= ru_eep_PROXI_Cfg.s.PROXI_IDVeh.s.Digit8;
2942  0714 ed80              	ldy	OFST+0,s
2943  0716 ed44              	ldy	4,y
2944  0718 f60001            	ldab	_ru_eep_PROXI_Cfg+1
2945  071b c40f              	andb	#15
2946  071d ea41              	orab	1,y
2947  071f 6b41              	stab	1,y
2948                         ; 913 	temp_c = 0;
2950  0721 790001            	clr	L311_temp_c
2951                         ; 915 	temp_c = ru_eep_PROXI_Cfg.s.PROXI_IDVeh.s.Digit7;
2953  0724 f60002            	ldab	_ru_eep_PROXI_Cfg+2
2954                         ; 916     temp_c <<= 4;
2956  0727 c4f0              	andb	#240
2957  0729 7b0001            	stab	L311_temp_c
2958                         ; 918   	pMsgContext->resData[2] =  temp_c;	
2960  072c 6b42              	stab	2,y
2961                         ; 920 	pMsgContext->resData[2] |= ru_eep_PROXI_Cfg.s.PROXI_IDVeh.s.Digit6;
2963  072e f60002            	ldab	_ru_eep_PROXI_Cfg+2
2964  0731 c40f              	andb	#15
2965  0733 ea42              	orab	2,y
2966  0735 6b42              	stab	2,y
2967                         ; 924 	temp_c = 0;
2969  0737 790001            	clr	L311_temp_c
2970                         ; 926 	temp_c = ru_eep_PROXI_Cfg.s.PROXI_CRC.s.Digit5;
2972  073a f60003            	ldab	_ru_eep_PROXI_Cfg+3
2973                         ; 927     temp_c <<= 4;
2975  073d c4f0              	andb	#240
2976  073f 7b0001            	stab	L311_temp_c
2977                         ; 929   	pMsgContext->resData[3] =  temp_c;	
2979  0742 6b43              	stab	3,y
2980                         ; 931 	pMsgContext->resData[3] |= ru_eep_PROXI_Cfg.s.PROXI_CRC.s.Digit4;
2982  0744 f60003            	ldab	_ru_eep_PROXI_Cfg+3
2983  0747 c40f              	andb	#15
2984  0749 ea43              	orab	3,y
2985  074b 6b43              	stab	3,y
2986                         ; 934 	temp_c = 0;
2988  074d 790001            	clr	L311_temp_c
2989                         ; 936 	temp_c = ru_eep_PROXI_Cfg.s.PROXI_CRC.s.Digit3;
2991  0750 f60004            	ldab	_ru_eep_PROXI_Cfg+4
2992                         ; 937     temp_c <<= 4;
2994  0753 c4f0              	andb	#240
2995  0755 7b0001            	stab	L311_temp_c
2996                         ; 939   	pMsgContext->resData[4] =  temp_c;	
2998  0758 6b44              	stab	4,y
2999                         ; 941 	pMsgContext->resData[4] |= ru_eep_PROXI_Cfg.s.PROXI_CRC.s.Digit2;
3001  075a f60004            	ldab	_ru_eep_PROXI_Cfg+4
3002  075d c40f              	andb	#15
3003  075f ea44              	orab	4,y
3004  0761 6b44              	stab	4,y
3005                         ; 944 	temp_c = 0;
3007  0763 790001            	clr	L311_temp_c
3008                         ; 946 	temp_c = ru_eep_PROXI_Cfg.s.PROXI_CRC.s.Digit1;
3010  0766 f60005            	ldab	_ru_eep_PROXI_Cfg+5
3011                         ; 947     temp_c <<= 4;
3013  0769 c4f0              	andb	#240
3014  076b 7b0001            	stab	L311_temp_c
3015                         ; 949   	pMsgContext->resData[5] =  temp_c;	
3017  076e 6b45              	stab	5,y
3018                         ; 951 	pMsgContext->resData[5] |= NOT_USED_VAL;
3020                         ; 956   	pMsgContext->resData[6] = ru_eep_PROXI_Cfg.s.Vehicle_Line_Configuration;
3022  0770 1809460006        	movb	_ru_eep_PROXI_Cfg+6,6,y
3023                         ; 960   	pMsgContext->resData[7] = ru_eep_PROXI_Cfg.s.Country_Code;
3025  0775 1809470007        	movb	_ru_eep_PROXI_Cfg+7,7,y
3026                         ; 964 	temp_c = 0; //Init the value.
3028  077a 790001            	clr	L311_temp_c
3029                         ; 965 	pMsgContext->resData[8] = temp_c; //Initialization
3031  077d 6948              	clr	8,y
3032                         ; 968 	temp_c = ru_eep_PROXI_Cfg.s.Heated_Seats_Variant;
3034  077f f60009            	ldab	_ru_eep_PROXI_Cfg+9
3035  0782 c406              	andb	#6
3036  0784 54                	lsrb	
3037                         ; 969 	pMsgContext->resData[8] = temp_c;
3039  0785 6b48              	stab	8,y
3040                         ; 971 	temp_c = ru_eep_PROXI_Cfg.s.Steering_Wheel;
3042  0787 f6000a            	ldab	_ru_eep_PROXI_Cfg+10
3043  078a c401              	andb	#1
3044  078c 58                	lslb	
3045  078d 58                	lslb	
3046  078e 7b0001            	stab	L311_temp_c
3047                         ; 972 	temp_c <<=2;
3049                         ; 973 	pMsgContext->resData[8] |= temp_c;
3051  0791 ea48              	orab	8,y
3052  0793 6b48              	stab	8,y
3053                         ; 976 	temp_c = NOT_USED_VAL;
3055  0795 790001            	clr	L311_temp_c
3056                         ; 977 	temp_c <<=3;
3058  0798 780001            	lsl	L311_temp_c
3059  079b 780001            	lsl	L311_temp_c
3060  079e 780001            	lsl	L311_temp_c
3061                         ; 978 	pMsgContext->resData[8] |= temp_c;
3063                         ; 981 	temp_c = ru_eep_PROXI_Cfg.s.Seat_Material;
3065  07a1 f60009            	ldab	_ru_eep_PROXI_Cfg+9
3066                         ; 982 	temp_c <<=4;
3068                         ; 983 	pMsgContext->resData[8] |= temp_c;
3070  07a4 c430              	andb	#48
3071  07a6 ea48              	orab	8,y
3072  07a8 6b48              	stab	8,y
3073                         ; 986 	temp_c = ru_eep_PROXI_Cfg.s.Wheel_Material;
3075  07aa f60009            	ldab	_ru_eep_PROXI_Cfg+9
3076                         ; 987 	temp_c <<=6;
3078  07ad c4c0              	andb	#192
3079  07af 7b0001            	stab	L311_temp_c
3080                         ; 988 	pMsgContext->resData[8] |= temp_c;
3082  07b2 ea48              	orab	8,y
3083  07b4 6b48              	stab	8,y
3084                         ; 991 	temp_c = 0; //Init the value.
3086  07b6 790001            	clr	L311_temp_c
3087                         ; 992 	pMsgContext->resData[9] = temp_c; //Initialization
3089  07b9 6949              	clr	9,y
3090                         ; 995 	temp_c = ru_eep_PROXI_Cfg.s.Driver_Side;
3092  07bb f60009            	ldab	_ru_eep_PROXI_Cfg+9
3093  07be c401              	andb	#1
3094                         ; 996 	pMsgContext->resData[9] = temp_c;
3096  07c0 6b49              	stab	9,y
3097                         ; 999 	temp_c = ru_eep_PROXI_Cfg.s.Remote_Start;
3099  07c2 f60009            	ldab	_ru_eep_PROXI_Cfg+9
3100  07c5 c408              	andb	#8
3101  07c7 54                	lsrb	
3102  07c8 54                	lsrb	
3103  07c9 7b0001            	stab	L311_temp_c
3104                         ; 1000 	temp_c <<=1;
3106                         ; 1001 	pMsgContext->resData[9] |= temp_c;
3108  07cc ea49              	orab	9,y
3109  07ce 6b49              	stab	9,y
3110                         ; 1003 	pMsgContext->resData[10] = ru_eep_PROXI_Cfg.s.Model_Year;
3112  07d0 18094a0008        	movb	_ru_eep_PROXI_Cfg+8,10,y
3113                         ; 1006 	DataLength = 11;
3115  07d5 cc000b            	ldd	#11
3116  07d8 7c0002            	std	L111_DataLength
3117                         ; 1007 	Response = RESPONSE_OK;
3119  07db c601              	ldab	#1
3120  07dd 7b0000            	stab	L511_Response
3121                         ; 1008 	diagF_DiagResponse(Response, DataLength, pMsgContext);
3123  07e0 ec80              	ldd	OFST+0,s
3124  07e2 3b                	pshd	
3125  07e3 cc000b            	ldd	#11
3126  07e6 3b                	pshd	
3127  07e7 c601              	ldab	#1
3128  07e9 4a000000          	call	f_diagF_DiagResponse
3130  07ed 1b86              	leas	6,s
3131                         ; 1010 }
3134  07ef 0a                	rtc	
3187                         ; 1017 void DESC_API_CALLBACK_TYPE ApplDescRead_222800_Heated_Seat_PWM_Calibration_Parameters_Front_Seats(DescMsgContext* pMsgContext)
3187                         ; 1018 {
3188                         	switch	.ftext
3189  07f0                   f_ApplDescRead_222800_Heated_Seat_PWM_Calibration_Parameters_Front_Seats:
3191  07f0 3b                	pshd	
3192  07f1 37                	pshb	
3193       00000001          OFST:	set	1
3196                         ; 1020 	unsigned char hs_vehicle_line_offset_c= 0;
3198  07f2 6980              	clr	OFST-1,s
3199                         ; 1026     EE_BlockRead(EE_HEAT_CAL_DATA_PF, (u_8Bit*)&hs_cal_prms);
3201  07f4 cc0000            	ldd	#_hs_cal_prms
3202  07f7 3b                	pshd	
3203  07f8 cc000e            	ldd	#14
3204  07fb 4a000000          	call	f_EE_BlockRead
3206  07ff 1b82              	leas	2,s
3207                         ; 1028 	pMsgContext->resData[0] = hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].Pwm_c[0];
3209  0801 ed81              	ldy	OFST+0,s
3210  0803 ed44              	ldy	4,y
3211  0805 e680              	ldab	OFST-1,s
3212  0807 87                	clra	
3213  0808 fb0000            	addb	_hs_seat_config_c
3214  080b 45                	rola	
3215  080c 59                	lsld	
3216  080d 3b                	pshd	
3217  080e 59                	lsld	
3218  080f 59                	lsld	
3219  0810 59                	lsld	
3220  0811 a3b1              	subd	2,s+
3221  0813 b745              	tfr	d,x
3222  0815 180ae2000040      	movb	_hs_cal_prms,x,0,y
3223                         ; 1029 	pMsgContext->resData[1] = hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].Pwm_c[1];
3225  081b ed81              	ldy	OFST+0,s
3226  081d ed44              	ldy	4,y
3227  081f 180ae2000141      	movb	_hs_cal_prms+1,x,1,y
3228                         ; 1030 	pMsgContext->resData[2] = hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].Pwm_c[2];
3230  0825 180ae2000242      	movb	_hs_cal_prms+2,x,2,y
3231                         ; 1031 	pMsgContext->resData[3] = hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].Pwm_c[3];
3233  082b 180ae2000343      	movb	_hs_cal_prms+3,x,3,y
3234                         ; 1033 	DataLength = 4;
3236  0831 cc0004            	ldd	#4
3237  0834 7c0002            	std	L111_DataLength
3238                         ; 1034 	Response = RESPONSE_OK;
3240  0837 c601              	ldab	#1
3241  0839 7b0000            	stab	L511_Response
3242                         ; 1035 	diagF_DiagResponse(Response, DataLength, pMsgContext);
3244  083c ec81              	ldd	OFST+0,s
3245  083e 3b                	pshd	
3246  083f cc0004            	ldd	#4
3247  0842 3b                	pshd	
3248  0843 c601              	ldab	#1
3249  0845 4a000000          	call	f_diagF_DiagResponse
3251                         ; 1036 }
3254  0849 1b87              	leas	7,s
3255  084b 0a                	rtc	
3299                         ; 1043 void DESC_API_CALLBACK_TYPE ApplDescRead_222801_Vented_Seat_PWM_Calibration_Parameters(DescMsgContext* pMsgContext)
3299                         ; 1044 {
3300                         	switch	.ftext
3301  084c                   f_ApplDescRead_222801_Vented_Seat_PWM_Calibration_Parameters:
3303  084c 3b                	pshd	
3304       00000000          OFST:	set	0
3307                         ; 1046 	if ( (CSWM_config_c & CSWM_2VS_MASK_K) == CSWM_2VS_MASK_K)
3309  084d 1e00003002        	brset	_CSWM_config_c,48,LC007
3310  0852 2019              	bra	L3221
3311  0854                   LC007:
3312                         ; 1050 		pMsgContext->resData[0] = Vs_cal_prms.cals[0].PWM_a[VL_LOW];
3314  0854 f60001            	ldab	_Vs_cal_prms+1
3315  0857 ed80              	ldy	OFST+0,s
3316  0859 6beb0004          	stab	[4,y]
3317                         ; 1051 		pMsgContext->resData[1] = Vs_cal_prms.cals[0].PWM_a[VL_HI - 1];
3319  085d ed44              	ldy	4,y
3320  085f 1809410002        	movb	_Vs_cal_prms+2,1,y
3321                         ; 1052 		DataLength = 2;
3323  0864 cc0002            	ldd	#2
3324  0867 7c0002            	std	L111_DataLength
3325                         ; 1053 		Response = RESPONSE_OK;
3327  086a 53                	decb	
3329  086b 2008              	bra	L5221
3330  086d                   L3221:
3331                         ; 1057 		DataLength = 1;
3333  086d cc0001            	ldd	#1
3334  0870 7c0002            	std	L111_DataLength
3335                         ; 1058 		Response = SUBFUNCTION_NOT_SUPPORTED;
3337  0873 c607              	ldab	#7
3338  0875                   L5221:
3339  0875 7b0000            	stab	L511_Response
3340                         ; 1060 	diagF_DiagResponse(Response, DataLength, pMsgContext);
3342  0878 ec80              	ldd	OFST+0,s
3343  087a 3b                	pshd	
3344  087b fc0002            	ldd	L111_DataLength
3345  087e 3b                	pshd	
3346  087f f60000            	ldab	L511_Response
3347  0882 87                	clra	
3348  0883 4a000000          	call	f_diagF_DiagResponse
3350  0887 1b86              	leas	6,s
3351                         ; 1061 }
3354  0889 0a                	rtc	
3400                         ; 1068 void DESC_API_CALLBACK_TYPE ApplDescRead_222802_Heated_Steering_Wheel_PWM_Calibration_Parameters(DescMsgContext* pMsgContext)
3400                         ; 1069 {
3401                         	switch	.ftext
3402  088a                   f_ApplDescRead_222802_Heated_Steering_Wheel_PWM_Calibration_Parameters:
3404  088a 3b                	pshd	
3405       00000000          OFST:	set	0
3408                         ; 1072 	if ( (CSWM_config_c & CSWM_HSW_MASK_K) == CSWM_HSW_MASK_K)
3410  088b 1f00004036        	brclr	_CSWM_config_c,64,L7421
3411                         ; 1075 		pMsgContext->resData[0] = stW_prms.cals[stW_cfg_c+stW_veh_offset_c].Pwm_a[WL1];
3413  0890 b746              	tfr	d,y
3414  0892 ee44              	ldx	4,y
3415  0894 f60000            	ldab	_stW_veh_offset_c
3416  0897 87                	clra	
3417  0898 fb0000            	addb	_stW_cfg_c
3418  089b 45                	rola	
3419  089c cd0013            	ldy	#19
3420  089f 13                	emul	
3421  08a0 b756              	tfr	x,y
3422  08a2 b745              	tfr	d,x
3423  08a4 180ae2000640      	movb	_stW_prms+6,x,0,y
3424                         ; 1076 		pMsgContext->resData[1] = stW_prms.cals[stW_cfg_c+stW_veh_offset_c].Pwm_a[WL2];
3426  08aa 180ae2000741      	movb	_stW_prms+7,x,1,y
3427                         ; 1077 		pMsgContext->resData[2] = stW_prms.cals[stW_cfg_c+stW_veh_offset_c].Pwm_a[WL3];
3429  08b0 180ae2000842      	movb	_stW_prms+8,x,2,y
3430                         ; 1078 		pMsgContext->resData[3] = stW_prms.cals[stW_cfg_c+stW_veh_offset_c].Pwm_a[WL4];
3432  08b6 180ae2000943      	movb	_stW_prms+9,x,3,y
3433                         ; 1079 		DataLength = 4;
3435  08bc cc0004            	ldd	#4
3436  08bf 7c0002            	std	L111_DataLength
3437                         ; 1080 		Response = RESPONSE_OK;
3439  08c2 c601              	ldab	#1
3441  08c4 2008              	bra	L1521
3442  08c6                   L7421:
3443                         ; 1084 		DataLength = 1;
3445  08c6 cc0001            	ldd	#1
3446  08c9 7c0002            	std	L111_DataLength
3447                         ; 1085 		Response = SUBFUNCTION_NOT_SUPPORTED;
3449  08cc c607              	ldab	#7
3450  08ce                   L1521:
3451  08ce 7b0000            	stab	L511_Response
3452                         ; 1087 	diagF_DiagResponse(Response, DataLength, pMsgContext);
3454  08d1 ec80              	ldd	OFST+0,s
3455  08d3 3b                	pshd	
3456  08d4 fc0002            	ldd	L111_DataLength
3457  08d7 3b                	pshd	
3458  08d8 f60000            	ldab	L511_Response
3459  08db 87                	clra	
3460  08dc 4a000000          	call	f_diagF_DiagResponse
3462  08e0 1b86              	leas	6,s
3463                         ; 1088 }
3466  08e2 0a                	rtc	
3518                         ; 1096 void DESC_API_CALLBACK_TYPE ApplDescRead_222803_Heated_Seat_Max_Timeout_Parameters_Front_Seats(DescMsgContext* pMsgContext)
3518                         ; 1097 {
3519                         	switch	.ftext
3520  08e3                   f_ApplDescRead_222803_Heated_Seat_Max_Timeout_Parameters_Front_Seats:
3522  08e3 3b                	pshd	
3523  08e4 37                	pshb	
3524       00000001          OFST:	set	1
3527                         ; 1098 	unsigned char hs_vehicle_line_offset_c= 0;
3529  08e5 6980              	clr	OFST-1,s
3530                         ; 1104 	pMsgContext->resData[0] = hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].Max_TimeOut_c[0];
3532  08e7 b746              	tfr	d,y
3533  08e9 ed44              	ldy	4,y
3534  08eb f60000            	ldab	_hs_seat_config_c
3535  08ee 860e              	ldaa	#14
3536  08f0 12                	mul	
3537  08f1 b745              	tfr	d,x
3538  08f3 180ae2000440      	movb	_hs_cal_prms+4,x,0,y
3539                         ; 1105 	pMsgContext->resData[1] = hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].Max_TimeOut_c[1];
3541  08f9 ed81              	ldy	OFST+0,s
3542  08fb ed44              	ldy	4,y
3543  08fd 180ae2000541      	movb	_hs_cal_prms+5,x,1,y
3544                         ; 1106 	pMsgContext->resData[2] = hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].Max_TimeOut_c[2];
3546  0903 180ae2000642      	movb	_hs_cal_prms+6,x,2,y
3547                         ; 1107 	pMsgContext->resData[3] = hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].Max_TimeOut_c[3];
3549  0909 180ae2000743      	movb	_hs_cal_prms+7,x,3,y
3550                         ; 1109 	DataLength = 4;
3552  090f cc0004            	ldd	#4
3553  0912 7c0002            	std	L111_DataLength
3554                         ; 1110 	Response = RESPONSE_OK;
3556  0915 c601              	ldab	#1
3557  0917 7b0000            	stab	L511_Response
3558                         ; 1111 	diagF_DiagResponse(Response, DataLength, pMsgContext);
3560  091a ec81              	ldd	OFST+0,s
3561  091c 3b                	pshd	
3562  091d cc0004            	ldd	#4
3563  0920 3b                	pshd	
3564  0921 c601              	ldab	#1
3565  0923 4a000000          	call	f_diagF_DiagResponse
3567                         ; 1112 }
3570  0927 1b87              	leas	7,s
3571  0929 0a                	rtc	
3617                         ; 1119 void DESC_API_CALLBACK_TYPE ApplDescRead_222804_Heated_Steering_Wheel_Max_Timeout_Parameters(DescMsgContext* pMsgContext)
3617                         ; 1120 {
3618                         	switch	.ftext
3619  092a                   f_ApplDescRead_222804_Heated_Steering_Wheel_Max_Timeout_Parameters:
3621  092a 3b                	pshd	
3622       00000000          OFST:	set	0
3625                         ; 1122 	if ( (CSWM_config_c & CSWM_HSW_MASK_K) == CSWM_HSW_MASK_K)
3627  092b 1f00004036        	brclr	_CSWM_config_c,64,L5131
3628                         ; 1125 		pMsgContext->resData[0] = stW_prms.cals[stW_cfg_c+stW_veh_offset_c].TimeOut_c[WL1];
3630  0930 b746              	tfr	d,y
3631  0932 ee44              	ldx	4,y
3632  0934 f60000            	ldab	_stW_veh_offset_c
3633  0937 87                	clra	
3634  0938 fb0000            	addb	_stW_cfg_c
3635  093b 45                	rola	
3636  093c cd0013            	ldy	#19
3637  093f 13                	emul	
3638  0940 b756              	tfr	x,y
3639  0942 b745              	tfr	d,x
3640  0944 180ae2000140      	movb	_stW_prms+1,x,0,y
3641                         ; 1126 		pMsgContext->resData[1] = stW_prms.cals[stW_cfg_c+stW_veh_offset_c].TimeOut_c[WL2];
3643  094a 180ae2000241      	movb	_stW_prms+2,x,1,y
3644                         ; 1127 		pMsgContext->resData[2] = stW_prms.cals[stW_cfg_c+stW_veh_offset_c].TimeOut_c[WL3];
3646  0950 180ae2000342      	movb	_stW_prms+3,x,2,y
3647                         ; 1128 		pMsgContext->resData[3] = stW_prms.cals[stW_cfg_c+stW_veh_offset_c].TimeOut_c[WL4];
3649  0956 180ae2000443      	movb	_stW_prms+4,x,3,y
3650                         ; 1129 		DataLength = 4;
3652  095c cc0004            	ldd	#4
3653  095f 7c0002            	std	L111_DataLength
3654                         ; 1130 		Response = RESPONSE_OK;
3656  0962 c601              	ldab	#1
3658  0964 2008              	bra	L7131
3659  0966                   L5131:
3660                         ; 1134 		DataLength = 1;
3662  0966 cc0001            	ldd	#1
3663  0969 7c0002            	std	L111_DataLength
3664                         ; 1135 		Response = SUBFUNCTION_NOT_SUPPORTED;
3666  096c c607              	ldab	#7
3667  096e                   L7131:
3668  096e 7b0000            	stab	L511_Response
3669                         ; 1137 	diagF_DiagResponse(Response, DataLength, pMsgContext);
3671  0971 ec80              	ldd	OFST+0,s
3672  0973 3b                	pshd	
3673  0974 fc0002            	ldd	L111_DataLength
3674  0977 3b                	pshd	
3675  0978 f60000            	ldab	L511_Response
3676  097b 87                	clra	
3677  097c 4a000000          	call	f_diagF_DiagResponse
3679  0980 1b86              	leas	6,s
3680                         ; 1138 }
3683  0982 0a                	rtc	
3724                         ; 1145 void DESC_API_CALLBACK_TYPE ApplDescRead_222805_Voltage_Level_ON(DescMsgContext* pMsgContext)
3724                         ; 1146 {
3725                         	switch	.ftext
3726  0983                   f_ApplDescRead_222805_Voltage_Level_ON:
3728  0983 3b                	pshd	
3729       00000000          OFST:	set	0
3732                         ; 1148 	pMsgContext->resData[0] = BattV_cals.Thrshld_a[2];
3734  0984 f60002            	ldab	_BattV_cals+2
3735  0987 ed80              	ldy	OFST+0,s
3736  0989 6beb0004          	stab	[4,y]
3737                         ; 1150 	DataLength = 1;
3739  098d cc0001            	ldd	#1
3740  0990 7c0002            	std	L111_DataLength
3741                         ; 1151 	Response = RESPONSE_OK;
3743  0993 7b0000            	stab	L511_Response
3744                         ; 1152 	diagF_DiagResponse(Response, DataLength, pMsgContext);
3746  0996 35                	pshy	
3747  0997 3b                	pshd	
3748  0998 4a000000          	call	f_diagF_DiagResponse
3750  099c 1b86              	leas	6,s
3751                         ; 1153 }
3754  099e 0a                	rtc	
3796                         ; 1161 void DESC_API_CALLBACK_TYPE ApplDescRead_222806_Voltage_Level_OFF(DescMsgContext* pMsgContext)
3796                         ; 1162 {
3797                         	switch	.ftext
3798  099f                   f_ApplDescRead_222806_Voltage_Level_OFF:
3800  099f 3b                	pshd	
3801       00000000          OFST:	set	0
3804                         ; 1164 	pMsgContext->resData[0] = BattV_cals.Thrshld_a[3];
3806  09a0 f60003            	ldab	_BattV_cals+3
3807  09a3 ed80              	ldy	OFST+0,s
3808  09a5 6beb0004          	stab	[4,y]
3809                         ; 1166 	DataLength = 1;
3811  09a9 cc0001            	ldd	#1
3812  09ac 7c0002            	std	L111_DataLength
3813                         ; 1167 	Response = RESPONSE_OK;
3815  09af 7b0000            	stab	L511_Response
3816                         ; 1168 	diagF_DiagResponse(Response, DataLength, pMsgContext);
3818  09b2 35                	pshy	
3819  09b3 3b                	pshd	
3820  09b4 4a000000          	call	f_diagF_DiagResponse
3822  09b8 1b86              	leas	6,s
3823                         ; 1169 }
3826  09ba 0a                	rtc	
3869                         ; 1176 void DESC_API_CALLBACK_TYPE ApplDescRead_222807_PCB_Over_Temperature_Condition_Status(DescMsgContext* pMsgContext)
3869                         ; 1177 {
3870                         	switch	.ftext
3871  09bb                   f_ApplDescRead_222807_PCB_Over_Temperature_Condition_Status:
3873  09bb 3b                	pshd	
3874       00000000          OFST:	set	0
3877                         ; 1179 	EE_BlockRead(EE_OVER_TEMP_FAILSAFE, &pMsgContext->resData[0]);
3879  09bc b746              	tfr	d,y
3880  09be ec44              	ldd	4,y
3881  09c0 3b                	pshd	
3882  09c1 cc0015            	ldd	#21
3883  09c4 4a000000          	call	f_EE_BlockRead
3885  09c8 1b82              	leas	2,s
3886                         ; 1184 	DataLength = 6;
3888  09ca cc0006            	ldd	#6
3889  09cd 7c0002            	std	L111_DataLength
3890                         ; 1185 	Response = RESPONSE_OK;
3892  09d0 c601              	ldab	#1
3893  09d2 7b0000            	stab	L511_Response
3894                         ; 1186 	diagF_DiagResponse(Response, DataLength, pMsgContext);
3896  09d5 ec80              	ldd	OFST+0,s
3897  09d7 3b                	pshd	
3898  09d8 cc0006            	ldd	#6
3899  09db 3b                	pshd	
3900  09dc c601              	ldab	#1
3901  09de 4a000000          	call	f_diagF_DiagResponse
3903  09e2 1b86              	leas	6,s
3904                         ; 1187 }
3907  09e4 0a                	rtc	
3960                         ; 1195 void DESC_API_CALLBACK_TYPE ApplDescRead_222808_Heated_Seat_NTC_Cutoff_Parameters_Front_Seats(DescMsgContext* pMsgContext)
3960                         ; 1196 {
3961                         	switch	.ftext
3962  09e5                   f_ApplDescRead_222808_Heated_Seat_NTC_Cutoff_Parameters_Front_Seats:
3964  09e5 3b                	pshd	
3965  09e6 37                	pshb	
3966       00000001          OFST:	set	1
3969                         ; 1197 	unsigned char hs_vehicle_line_offset_c= 0;
3971  09e7 6980              	clr	OFST-1,s
3972                         ; 1202 	pMsgContext->resData[0] = TempF_NtcAdStd_to_CanTemp(hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].NTC_CutOFF_c[0]); //PWM 1 No cut off
3974  09e9 f60000            	ldab	_hs_seat_config_c
3975  09ec 860e              	ldaa	#14
3976  09ee 12                	mul	
3977  09ef b746              	tfr	d,y
3978  09f1 e6ea0008          	ldab	_hs_cal_prms+8,y
3979  09f5 87                	clra	
3980  09f6 4a000000          	call	f_TempF_NtcAdStd_to_CanTemp
3982  09fa ee81              	ldx	OFST+0,s
3983  09fc 6be30004          	stab	[4,x]
3984                         ; 1203 	pMsgContext->resData[1] = TempF_NtcAdStd_to_CanTemp(hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].NTC_CutOFF_c[1]); //PWM 1 No cut off
3986  0a00 e680              	ldab	OFST-1,s
3987  0a02 87                	clra	
3988  0a03 fb0000            	addb	_hs_seat_config_c
3989  0a06 45                	rola	
3990  0a07 cd000e            	ldy	#14
3991  0a0a 13                	emul	
3992  0a0b b746              	tfr	d,y
3993  0a0d e6ea0009          	ldab	_hs_cal_prms+9,y
3994  0a11 87                	clra	
3995  0a12 4a000000          	call	f_TempF_NtcAdStd_to_CanTemp
3997  0a16 ed81              	ldy	OFST+0,s
3998  0a18 ed44              	ldy	4,y
3999  0a1a 6b41              	stab	1,y
4000                         ; 1204 	pMsgContext->resData[2] = TempF_NtcAdStd_to_CanTemp(hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].NTC_CutOFF_c[2]); //PWM 1 No cut off
4002  0a1c e680              	ldab	OFST-1,s
4003  0a1e 87                	clra	
4004  0a1f fb0000            	addb	_hs_seat_config_c
4005  0a22 45                	rola	
4006  0a23 cd000e            	ldy	#14
4007  0a26 13                	emul	
4008  0a27 b746              	tfr	d,y
4009  0a29 e6ea000a          	ldab	_hs_cal_prms+10,y
4010  0a2d 87                	clra	
4011  0a2e 4a000000          	call	f_TempF_NtcAdStd_to_CanTemp
4013  0a32 ed81              	ldy	OFST+0,s
4014  0a34 ed44              	ldy	4,y
4015  0a36 6b42              	stab	2,y
4016                         ; 1205 	pMsgContext->resData[3] = TempF_NtcAdStd_to_CanTemp(hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].NTC_CutOFF_c[3]); //PWM 1 No cut off
4018  0a38 e680              	ldab	OFST-1,s
4019  0a3a 87                	clra	
4020  0a3b fb0000            	addb	_hs_seat_config_c
4021  0a3e 45                	rola	
4022  0a3f cd000e            	ldy	#14
4023  0a42 13                	emul	
4024  0a43 b746              	tfr	d,y
4025  0a45 e6ea000b          	ldab	_hs_cal_prms+11,y
4026  0a49 87                	clra	
4027  0a4a 4a000000          	call	f_TempF_NtcAdStd_to_CanTemp
4029  0a4e ed81              	ldy	OFST+0,s
4030  0a50 ed44              	ldy	4,y
4031  0a52 6b43              	stab	3,y
4032                         ; 1206 	pMsgContext->resData[4] = TempF_NtcAdStd_to_CanTemp(hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].NTC_CutOFF_c[4]); //PWM 1 No cut off
4034  0a54 e680              	ldab	OFST-1,s
4035  0a56 87                	clra	
4036  0a57 fb0000            	addb	_hs_seat_config_c
4037  0a5a 45                	rola	
4038  0a5b cd000e            	ldy	#14
4039  0a5e 13                	emul	
4040  0a5f b746              	tfr	d,y
4041  0a61 e6ea000c          	ldab	_hs_cal_prms+12,y
4042  0a65 87                	clra	
4043  0a66 4a000000          	call	f_TempF_NtcAdStd_to_CanTemp
4045  0a6a ed81              	ldy	OFST+0,s
4046  0a6c ed44              	ldy	4,y
4047  0a6e 6b44              	stab	4,y
4048                         ; 1208 	DataLength = 5;
4050  0a70 cc0005            	ldd	#5
4051  0a73 7c0002            	std	L111_DataLength
4052                         ; 1209 	Response = RESPONSE_OK;
4054  0a76 c601              	ldab	#1
4055  0a78 7b0000            	stab	L511_Response
4056                         ; 1210 	diagF_DiagResponse(Response, DataLength, pMsgContext);
4058  0a7b ec81              	ldd	OFST+0,s
4059  0a7d 3b                	pshd	
4060  0a7e cc0005            	ldd	#5
4061  0a81 3b                	pshd	
4062  0a82 c601              	ldab	#1
4063  0a84 4a000000          	call	f_diagF_DiagResponse
4065                         ; 1211 }
4068  0a88 1b87              	leas	7,s
4069  0a8a 0a                	rtc	
4115                         ; 1218 void DESC_API_CALLBACK_TYPE ApplDescRead_222809_Steering_Wheel_Temperature_Cut_off_Parameters(DescMsgContext* pMsgContext)
4115                         ; 1219 {
4116                         	switch	.ftext
4117  0a8b                   f_ApplDescRead_222809_Steering_Wheel_Temperature_Cut_off_Parameters:
4119  0a8b 3b                	pshd	
4120       00000000          OFST:	set	0
4123                         ; 1221 	if ( (CSWM_config_c & CSWM_HSW_MASK_K) == CSWM_HSW_MASK_K)
4125  0a8c f60000            	ldab	_CSWM_config_c
4126  0a8f c440              	andb	#64
4127  0a91 c140              	cmpb	#64
4128  0a93 263c              	bne	L3441
4129                         ; 1224 		pMsgContext->resData[0] = stW_prms.cals[stW_cfg_c+stW_veh_offset_c].WL1Temp_Threshld;; 
4131  0a95 ed80              	ldy	OFST+0,s
4132  0a97 ee44              	ldx	4,y
4133  0a99 f60000            	ldab	_stW_veh_offset_c
4134  0a9c 87                	clra	
4135  0a9d fb0000            	addb	_stW_cfg_c
4136  0aa0 45                	rola	
4137  0aa1 cd0013            	ldy	#19
4138  0aa4 13                	emul	
4139  0aa5 b756              	tfr	x,y
4140  0aa7 b745              	tfr	d,x
4141  0aa9 180ae2000a40      	movb	_stW_prms+10,x,0,y
4142                         ; 1225 		pMsgContext->resData[1] = stW_prms.cals[stW_cfg_c+stW_veh_offset_c].WL2Temp_Threshld;
4145  0aaf 180ae2000b41      	movb	_stW_prms+11,x,1,y
4146                         ; 1226 		pMsgContext->resData[2] = stW_prms.cals[stW_cfg_c+stW_veh_offset_c].WL3Temp_Threshld;
4148  0ab5 180ae2000c42      	movb	_stW_prms+12,x,2,y
4149                         ; 1227 		pMsgContext->resData[3] = stW_prms.cals[stW_cfg_c+stW_veh_offset_c].WL4Temp_Threshld;
4151  0abb 180ae2000d43      	movb	_stW_prms+13,x,3,y
4152                         ; 1228 		pMsgContext->resData[4] = stW_prms.cals[stW_cfg_c+stW_veh_offset_c].MaxTemp_Threshld;
4154  0ac1 180ae2000e44      	movb	_stW_prms+14,x,4,y
4155                         ; 1229 		DataLength = 5;
4157  0ac7 cc0005            	ldd	#5
4158  0aca 7c0002            	std	L111_DataLength
4159                         ; 1230 		Response = RESPONSE_OK;
4161  0acd c601              	ldab	#1
4163  0acf 2008              	bra	L5441
4164  0ad1                   L3441:
4165                         ; 1234 		DataLength = 1;
4167  0ad1 cc0001            	ldd	#1
4168  0ad4 7c0002            	std	L111_DataLength
4169                         ; 1235 		Response = SUBFUNCTION_NOT_SUPPORTED;
4171  0ad7 c607              	ldab	#7
4172  0ad9                   L5441:
4173  0ad9 7b0000            	stab	L511_Response
4174                         ; 1237 	diagF_DiagResponse(Response, DataLength, pMsgContext);
4176  0adc ec80              	ldd	OFST+0,s
4177  0ade 3b                	pshd	
4178  0adf fc0002            	ldd	L111_DataLength
4179  0ae2 3b                	pshd	
4180  0ae3 f60000            	ldab	L511_Response
4181  0ae6 87                	clra	
4182  0ae7 4a000000          	call	f_diagF_DiagResponse
4184  0aeb 1b86              	leas	6,s
4185                         ; 1238 }
4188  0aed 0a                	rtc	
4232                         ; 1245 void DESC_API_CALLBACK_TYPE ApplDescRead_22280A_HeatVent_Auto_Activation_Temperature_Calibration_Paramet(DescMsgContext* pMsgContext)
4232                         ; 1246 {
4233                         	switch	.ftext
4234  0aee                   f_ApplDescRead_22280A_HeatVent_Auto_Activation_Temperature_Calibration_Paramet:
4236  0aee 3b                	pshd	
4237       00000000          OFST:	set	0
4240                         ; 1250 	pMsgContext->resData[0] = 0x00;
4242  0aef b746              	tfr	d,y
4243  0af1 ee44              	ldx	4,y
4244  0af3 6930              	clr	1,x+
4245                         ; 1251 	EE_BlockRead(EE_REMOTE_CTRL_HEAT_AMBTEMP, &pMsgContext->resData[1]);
4247  0af5 34                	pshx	
4248  0af6 cc0011            	ldd	#17
4249  0af9 4a000000          	call	f_EE_BlockRead
4251  0afd 1b82              	leas	2,s
4252                         ; 1252 	pMsgContext->resData[2] = 0x00;
4254  0aff ed80              	ldy	OFST+0,s
4255  0b01 ed44              	ldy	4,y
4256  0b03 6942              	clr	2,y
4257                         ; 1253 	EE_BlockRead(EE_REMOTE_CTRL_VENT_AMBTEMP, &pMsgContext->resData[3]);
4259  0b05 ed80              	ldy	OFST+0,s
4260  0b07 ee44              	ldx	4,y
4261  0b09 1a03              	leax	3,x
4262  0b0b 34                	pshx	
4263  0b0c cc0012            	ldd	#18
4264  0b0f 4a000000          	call	f_EE_BlockRead
4266  0b13 1b82              	leas	2,s
4267                         ; 1254 	DataLength = 4;
4269  0b15 cc0004            	ldd	#4
4270  0b18 7c0002            	std	L111_DataLength
4271                         ; 1255 	Response = RESPONSE_OK;
4273  0b1b c601              	ldab	#1
4274  0b1d 7b0000            	stab	L511_Response
4275                         ; 1256 	diagF_DiagResponse(Response, DataLength, pMsgContext);
4277  0b20 ec80              	ldd	OFST+0,s
4278  0b22 3b                	pshd	
4279  0b23 cc0004            	ldd	#4
4280  0b26 3b                	pshd	
4281  0b27 c601              	ldab	#1
4282  0b29 4a000000          	call	f_diagF_DiagResponse
4284  0b2d 1b86              	leas	6,s
4285                         ; 1257 }
4288  0b2f 0a                	rtc	
4333                         ; 1264 void DESC_API_CALLBACK_TYPE ApplDescRead_22280C_Heated_Steering_Wheel_Min_Timeout_Parameters(DescMsgContext* pMsgContext)
4333                         ; 1265 {
4334                         	switch	.ftext
4335  0b30                   f_ApplDescRead_22280C_Heated_Steering_Wheel_Min_Timeout_Parameters:
4337  0b30 3b                	pshd	
4338       00000000          OFST:	set	0
4341                         ; 1267 	pMsgContext->resData[0] = stW_prms.cals[stW_cfg_c+stW_veh_offset_c].WL1TempChck_Time;; 
4343  0b31 b746              	tfr	d,y
4344  0b33 ee44              	ldx	4,y
4345  0b35 f60000            	ldab	_stW_veh_offset_c
4346  0b38 87                	clra	
4347  0b39 fb0000            	addb	_stW_cfg_c
4348  0b3c 45                	rola	
4349  0b3d cd0013            	ldy	#19
4350  0b40 13                	emul	
4351  0b41 b756              	tfr	x,y
4352  0b43 b745              	tfr	d,x
4353  0b45 180ae2000f40      	movb	_stW_prms+15,x,0,y
4354                         ; 1268 	pMsgContext->resData[1] = stW_prms.cals[stW_cfg_c+stW_veh_offset_c].WL2TempChck_Time;
4357  0b4b ed80              	ldy	OFST+0,s
4358  0b4d ee44              	ldx	4,y
4359  0b4f f60000            	ldab	_stW_veh_offset_c
4360  0b52 87                	clra	
4361  0b53 fb0000            	addb	_stW_cfg_c
4362  0b56 45                	rola	
4363  0b57 cd0013            	ldy	#19
4364  0b5a 13                	emul	
4365  0b5b b756              	tfr	x,y
4366  0b5d b745              	tfr	d,x
4367  0b5f 180ae2001041      	movb	_stW_prms+16,x,1,y
4368                         ; 1269 	pMsgContext->resData[2] = stW_prms.cals[stW_cfg_c+stW_veh_offset_c].WL3TempChck_Time;
4370  0b65 180ae2001142      	movb	_stW_prms+17,x,2,y
4371                         ; 1270 	pMsgContext->resData[3] = stW_prms.cals[stW_cfg_c+stW_veh_offset_c].WL4TempChck_Time;
4373  0b6b 180ae2001243      	movb	_stW_prms+18,x,3,y
4374                         ; 1271 	DataLength = 4;
4376  0b71 cc0004            	ldd	#4
4377  0b74 7c0002            	std	L111_DataLength
4378                         ; 1272 	Response = RESPONSE_OK;
4380  0b77 c601              	ldab	#1
4381  0b79 7b0000            	stab	L511_Response
4382                         ; 1273 	diagF_DiagResponse(Response, DataLength, pMsgContext);
4384  0b7c ec80              	ldd	OFST+0,s
4385  0b7e 3b                	pshd	
4386  0b7f cc0004            	ldd	#4
4387  0b82 3b                	pshd	
4388  0b83 c601              	ldab	#1
4389  0b85 4a000000          	call	f_diagF_DiagResponse
4391  0b89 1b86              	leas	6,s
4392                         ; 1274 }
4395  0b8b 0a                	rtc	
4437                         ; 1281 void DESC_API_CALLBACK_TYPE ApplDescRead_22280D_Load_Shed_Fault_Status(DescMsgContext* pMsgContext)
4437                         ; 1282 {
4438                         	switch	.ftext
4439  0b8c                   f_ApplDescRead_22280D_Load_Shed_Fault_Status:
4441  0b8c 3b                	pshd	
4442       00000000          OFST:	set	0
4445                         ; 1289 	pMsgContext->resData[0] = canio_LOAD_SHED_c; // Load shed status from Bus
4447  0b8d f60000            	ldab	_canio_LOAD_SHED_c
4448  0b90 ed80              	ldy	OFST+0,s
4449  0b92 6beb0004          	stab	[4,y]
4450                         ; 1292 	DataLength = 5;
4452  0b96 cc0005            	ldd	#5
4453  0b99 7c0002            	std	L111_DataLength
4454                         ; 1293 	Response = RESPONSE_OK;
4456  0b9c c601              	ldab	#1
4457  0b9e 7b0000            	stab	L511_Response
4458                         ; 1294 	diagF_DiagResponse(Response, DataLength, pMsgContext);
4460  0ba1 35                	pshy	
4461  0ba2 c605              	ldab	#5
4462  0ba4 3b                	pshd	
4463  0ba5 c601              	ldab	#1
4464  0ba7 4a000000          	call	f_diagF_DiagResponse
4466  0bab 1b86              	leas	6,s
4467                         ; 1295 }
4470  0bad 0a                	rtc	
4525                         ; 1300 void DESC_API_CALLBACK_TYPE ApplDescRead_22280E_Heated_Seat_PWM_Calibration_Parameters_Rear_Seats(DescMsgContext* pMsgContext)
4525                         ; 1301 {
4526                         	switch	.ftext
4527  0bae                   f_ApplDescRead_22280E_Heated_Seat_PWM_Calibration_Parameters_Rear_Seats:
4529  0bae 3b                	pshd	
4530  0baf 37                	pshb	
4531       00000001          OFST:	set	1
4534                         ; 1302 	unsigned char hs_vehicle_line_offset_c= 1;
4536  0bb0 c601              	ldab	#1
4537  0bb2 6b80              	stab	OFST-1,s
4538                         ; 1305 	if( ((CSWM_config_c & CSWM_4HS_VARIANT_MASK_K) == CSWM_4HS_VARIANT_MASK_K) && (main_SwReq_Rear_c == VEH_SWITCH_REAR_ONLY_K) )
4540  0bb4 f60000            	ldab	_CSWM_config_c
4541  0bb7 c40f              	andb	#15
4542  0bb9 c10f              	cmpb	#15
4543  0bbb 264e              	bne	L1551
4545  0bbd f60000            	ldab	_main_SwReq_Rear_c
4546  0bc0 c103              	cmpb	#3
4547  0bc2 2647              	bne	L1551
4548                         ; 1311     	EE_BlockRead(EE_HEAT_CAL_DATA_PF, (u_8Bit*)&hs_cal_prms);
4550  0bc4 cc0000            	ldd	#_hs_cal_prms
4551  0bc7 3b                	pshd	
4552  0bc8 cc000e            	ldd	#14
4553  0bcb 4a000000          	call	f_EE_BlockRead
4555  0bcf 1b82              	leas	2,s
4556                         ; 1313 		pMsgContext->resData[0] = hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].Pwm_c[0];
4558  0bd1 ed81              	ldy	OFST+0,s
4559  0bd3 ed44              	ldy	4,y
4560  0bd5 e680              	ldab	OFST-1,s
4561  0bd7 87                	clra	
4562  0bd8 fb0000            	addb	_hs_seat_config_c
4563  0bdb 45                	rola	
4564  0bdc 59                	lsld	
4565  0bdd 3b                	pshd	
4566  0bde 59                	lsld	
4567  0bdf 59                	lsld	
4568  0be0 59                	lsld	
4569  0be1 a3b1              	subd	2,s+
4570  0be3 b745              	tfr	d,x
4571  0be5 180ae2000040      	movb	_hs_cal_prms,x,0,y
4572                         ; 1314 		pMsgContext->resData[1] = hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].Pwm_c[1];
4574  0beb ed81              	ldy	OFST+0,s
4575  0bed ed44              	ldy	4,y
4576  0bef 180ae2000141      	movb	_hs_cal_prms+1,x,1,y
4577                         ; 1315 		pMsgContext->resData[2] = hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].Pwm_c[2];
4579  0bf5 180ae2000242      	movb	_hs_cal_prms+2,x,2,y
4580                         ; 1316 		pMsgContext->resData[3] = hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].Pwm_c[3];
4582  0bfb 180ae2000343      	movb	_hs_cal_prms+3,x,3,y
4583                         ; 1318 		DataLength = 4;
4585  0c01 cc0004            	ldd	#4
4586  0c04 7c0002            	std	L111_DataLength
4587                         ; 1319 		Response = RESPONSE_OK;
4589  0c07 c601              	ldab	#1
4591  0c09 2008              	bra	L3551
4592  0c0b                   L1551:
4593                         ; 1324        	DataLength = 1;
4595  0c0b cc0001            	ldd	#1
4596  0c0e 7c0002            	std	L111_DataLength
4597                         ; 1325 	   	Response = SUBFUNCTION_NOT_SUPPORTED;
4599  0c11 c607              	ldab	#7
4600  0c13                   L3551:
4601  0c13 7b0000            	stab	L511_Response
4602                         ; 1329 	diagF_DiagResponse(Response, DataLength, pMsgContext);
4604  0c16 ec81              	ldd	OFST+0,s
4605  0c18 3b                	pshd	
4606  0c19 fc0002            	ldd	L111_DataLength
4607  0c1c 3b                	pshd	
4608  0c1d f60000            	ldab	L511_Response
4609  0c20 87                	clra	
4610  0c21 4a000000          	call	f_diagF_DiagResponse
4612                         ; 1330 }
4615  0c25 1b87              	leas	7,s
4616  0c27 0a                	rtc	
4670                         ; 1336 void DESC_API_CALLBACK_TYPE ApplDescRead_22280F_Heated_Seat_Max_Timeout_Parameters_Rear_Seats(DescMsgContext* pMsgContext)
4670                         ; 1337 {
4671                         	switch	.ftext
4672  0c28                   f_ApplDescRead_22280F_Heated_Seat_Max_Timeout_Parameters_Rear_Seats:
4674  0c28 3b                	pshd	
4675  0c29 37                	pshb	
4676       00000001          OFST:	set	1
4679                         ; 1338 	unsigned char hs_vehicle_line_offset_c= 1;
4681  0c2a c601              	ldab	#1
4682  0c2c 6b80              	stab	OFST-1,s
4683                         ; 1341 	if( ((CSWM_config_c & CSWM_4HS_VARIANT_MASK_K) == CSWM_4HS_VARIANT_MASK_K) && (main_SwReq_Rear_c == VEH_SWITCH_REAR_ONLY_K) )
4685  0c2e 1e00000f02        	brset	_CSWM_config_c,15,LC008
4686  0c33 2035              	bra	L7751
4687  0c35                   LC008:
4689  0c35 f60000            	ldab	_main_SwReq_Rear_c
4690  0c38 c103              	cmpb	#3
4691  0c3a 262e              	bne	L7751
4692                         ; 1347 		pMsgContext->resData[0] = hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].Max_TimeOut_c[0];
4694  0c3c ed81              	ldy	OFST+0,s
4695  0c3e ed44              	ldy	4,y
4696  0c40 f60000            	ldab	_hs_seat_config_c
4697  0c43 860e              	ldaa	#14
4698  0c45 12                	mul	
4699  0c46 b745              	tfr	d,x
4700  0c48 180ae2001240      	movb	_hs_cal_prms+18,x,0,y
4701                         ; 1348 		pMsgContext->resData[1] = hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].Max_TimeOut_c[1];
4703  0c4e 180ae2001341      	movb	_hs_cal_prms+19,x,1,y
4704                         ; 1349 		pMsgContext->resData[2] = hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].Max_TimeOut_c[2];
4706  0c54 180ae2001442      	movb	_hs_cal_prms+20,x,2,y
4707                         ; 1350 		pMsgContext->resData[3] = hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].Max_TimeOut_c[3];
4709  0c5a 180ae2001543      	movb	_hs_cal_prms+21,x,3,y
4710                         ; 1352 		DataLength = 4;
4712  0c60 cc0004            	ldd	#4
4713  0c63 7c0002            	std	L111_DataLength
4714                         ; 1353 		Response = RESPONSE_OK;
4716  0c66 c601              	ldab	#1
4718  0c68 2008              	bra	L1061
4719  0c6a                   L7751:
4720                         ; 1358        	DataLength = 1;
4722  0c6a cc0001            	ldd	#1
4723  0c6d 7c0002            	std	L111_DataLength
4724                         ; 1359 	   	Response = SUBFUNCTION_NOT_SUPPORTED;
4726  0c70 c607              	ldab	#7
4727  0c72                   L1061:
4728  0c72 7b0000            	stab	L511_Response
4729                         ; 1362 	diagF_DiagResponse(Response, DataLength, pMsgContext);
4731  0c75 ec81              	ldd	OFST+0,s
4732  0c77 3b                	pshd	
4733  0c78 fc0002            	ldd	L111_DataLength
4734  0c7b 3b                	pshd	
4735  0c7c f60000            	ldab	L511_Response
4736  0c7f 87                	clra	
4737  0c80 4a000000          	call	f_diagF_DiagResponse
4739                         ; 1363 }
4742  0c84 1b87              	leas	7,s
4743  0c86 0a                	rtc	
4798                         ; 1369 void DESC_API_CALLBACK_TYPE ApplDescRead_222810_Heated_Seat_NTC_Cutoff_Parameters_Rear_Seats(DescMsgContext* pMsgContext)
4798                         ; 1370 {
4799                         	switch	.ftext
4800  0c87                   f_ApplDescRead_222810_Heated_Seat_NTC_Cutoff_Parameters_Rear_Seats:
4802  0c87 3b                	pshd	
4803  0c88 37                	pshb	
4804       00000001          OFST:	set	1
4807                         ; 1371 	unsigned char hs_vehicle_line_offset_c= 1;
4809  0c89 c601              	ldab	#1
4810  0c8b 6b80              	stab	OFST-1,s
4811                         ; 1375 	if( ((CSWM_config_c & CSWM_4HS_VARIANT_MASK_K) == CSWM_4HS_VARIANT_MASK_K) && (main_SwReq_Rear_c == VEH_SWITCH_REAR_ONLY_K) )
4813  0c8d f60000            	ldab	_CSWM_config_c
4814  0c90 c40f              	andb	#15
4815  0c92 c10f              	cmpb	#15
4816  0c94 1826009a          	bne	L5261
4818  0c98 f60000            	ldab	_main_SwReq_Rear_c
4819  0c9b c103              	cmpb	#3
4820  0c9d 18260091          	bne	L5261
4821                         ; 1379 		pMsgContext->resData[0] = TempF_NtcAdStd_to_CanTemp(hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].NTC_CutOFF_c[0]); //PWM 1 No cut off
4823  0ca1 f60000            	ldab	_hs_seat_config_c
4824  0ca4 860e              	ldaa	#14
4825  0ca6 12                	mul	
4826  0ca7 b746              	tfr	d,y
4827  0ca9 e6ea0016          	ldab	_hs_cal_prms+22,y
4828  0cad 87                	clra	
4829  0cae 4a000000          	call	f_TempF_NtcAdStd_to_CanTemp
4831  0cb2 ee81              	ldx	OFST+0,s
4832  0cb4 6be30004          	stab	[4,x]
4833                         ; 1380 		pMsgContext->resData[1] = TempF_NtcAdStd_to_CanTemp(hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].NTC_CutOFF_c[1]); //PWM 1 No cut off
4835  0cb8 e680              	ldab	OFST-1,s
4836  0cba 87                	clra	
4837  0cbb fb0000            	addb	_hs_seat_config_c
4838  0cbe 45                	rola	
4839  0cbf cd000e            	ldy	#14
4840  0cc2 13                	emul	
4841  0cc3 b746              	tfr	d,y
4842  0cc5 e6ea0009          	ldab	_hs_cal_prms+9,y
4843  0cc9 87                	clra	
4844  0cca 4a000000          	call	f_TempF_NtcAdStd_to_CanTemp
4846  0cce ed81              	ldy	OFST+0,s
4847  0cd0 ed44              	ldy	4,y
4848  0cd2 6b41              	stab	1,y
4849                         ; 1381 		pMsgContext->resData[2] = TempF_NtcAdStd_to_CanTemp(hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].NTC_CutOFF_c[2]); //PWM 1 No cut off
4851  0cd4 e680              	ldab	OFST-1,s
4852  0cd6 87                	clra	
4853  0cd7 fb0000            	addb	_hs_seat_config_c
4854  0cda 45                	rola	
4855  0cdb cd000e            	ldy	#14
4856  0cde 13                	emul	
4857  0cdf b746              	tfr	d,y
4858  0ce1 e6ea000a          	ldab	_hs_cal_prms+10,y
4859  0ce5 87                	clra	
4860  0ce6 4a000000          	call	f_TempF_NtcAdStd_to_CanTemp
4862  0cea ed81              	ldy	OFST+0,s
4863  0cec ed44              	ldy	4,y
4864  0cee 6b42              	stab	2,y
4865                         ; 1382 		pMsgContext->resData[3] = TempF_NtcAdStd_to_CanTemp(hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].NTC_CutOFF_c[3]); //PWM 1 No cut off
4867  0cf0 e680              	ldab	OFST-1,s
4868  0cf2 87                	clra	
4869  0cf3 fb0000            	addb	_hs_seat_config_c
4870  0cf6 45                	rola	
4871  0cf7 cd000e            	ldy	#14
4872  0cfa 13                	emul	
4873  0cfb b746              	tfr	d,y
4874  0cfd e6ea000b          	ldab	_hs_cal_prms+11,y
4875  0d01 87                	clra	
4876  0d02 4a000000          	call	f_TempF_NtcAdStd_to_CanTemp
4878  0d06 ed81              	ldy	OFST+0,s
4879  0d08 ed44              	ldy	4,y
4880  0d0a 6b43              	stab	3,y
4881                         ; 1383 		pMsgContext->resData[4] = TempF_NtcAdStd_to_CanTemp(hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].NTC_CutOFF_c[4]); //PWM 1 No cut off
4883  0d0c e680              	ldab	OFST-1,s
4884  0d0e 87                	clra	
4885  0d0f fb0000            	addb	_hs_seat_config_c
4886  0d12 45                	rola	
4887  0d13 cd000e            	ldy	#14
4888  0d16 13                	emul	
4889  0d17 b746              	tfr	d,y
4890  0d19 e6ea000c          	ldab	_hs_cal_prms+12,y
4891  0d1d 87                	clra	
4892  0d1e 4a000000          	call	f_TempF_NtcAdStd_to_CanTemp
4894  0d22 ed81              	ldy	OFST+0,s
4895  0d24 ed44              	ldy	4,y
4896  0d26 6b44              	stab	4,y
4897                         ; 1385 		DataLength = 5;
4899                         ; 1386 		Response = RESPONSE_OK;
4901                         ; 1387 		DataLength = 4;
4903  0d28 cc0004            	ldd	#4
4904  0d2b 7c0002            	std	L111_DataLength
4905                         ; 1388 		Response = RESPONSE_OK;
4907  0d2e c601              	ldab	#1
4909  0d30 2008              	bra	L7261
4910  0d32                   L5261:
4911                         ; 1393        	DataLength = 1;
4913  0d32 cc0001            	ldd	#1
4914  0d35 7c0002            	std	L111_DataLength
4915                         ; 1394 	   	Response = SUBFUNCTION_NOT_SUPPORTED;
4917  0d38 c607              	ldab	#7
4918  0d3a                   L7261:
4919  0d3a 7b0000            	stab	L511_Response
4920                         ; 1397 	diagF_DiagResponse(Response, DataLength, pMsgContext);
4922  0d3d ec81              	ldd	OFST+0,s
4923  0d3f 3b                	pshd	
4924  0d40 fc0002            	ldd	L111_DataLength
4925  0d43 3b                	pshd	
4926  0d44 f60000            	ldab	L511_Response
4927  0d47 87                	clra	
4928  0d48 4a000000          	call	f_diagF_DiagResponse
4930                         ; 1398 }
4933  0d4c 1b87              	leas	7,s
4934  0d4e 0a                	rtc	
4978                         ; 1405 void DESC_API_CALLBACK_TYPE ApplDescRead_223000_Program_ECU_Data_at_Flash_Station_1(DescMsgContext* pMsgContext)
4978                         ; 1406 {
4979                         	switch	.ftext
4980  0d4f                   f_ApplDescRead_223000_Program_ECU_Data_at_Flash_Station_1:
4982  0d4f 3b                	pshd	
4983       00000000          OFST:	set	0
4986                         ; 1407 	if(diag_eol_io_lock_b)
4988  0d50 1f00000118        	brclr	_diag_io_lock3_flags_c,1,L1561
4989                         ; 1409 		EE_BlockRead(EE_BOARD_TEMIC_BOM_NUMBER, &pMsgContext->resData[0]);
4991  0d55 b746              	tfr	d,y
4992  0d57 ec44              	ldd	4,y
4993  0d59 3b                	pshd	
4994  0d5a cc0024            	ldd	#36
4995  0d5d 4a000000          	call	f_EE_BlockRead
4997  0d61 1b82              	leas	2,s
4998                         ; 1410 		DataLength = 15;
5000  0d63 cc000f            	ldd	#15
5001  0d66 7c0002            	std	L111_DataLength
5002                         ; 1411 		Response = RESPONSE_OK;
5004  0d69 c601              	ldab	#1
5006  0d6b 2008              	bra	L3561
5007  0d6d                   L1561:
5008                         ; 1415 		DataLength = 1;
5010  0d6d cc0001            	ldd	#1
5011  0d70 7c0002            	std	L111_DataLength
5012                         ; 1416 		Response = CONDITIONS_NOT_CORRECT;
5014  0d73 c606              	ldab	#6
5015  0d75                   L3561:
5016  0d75 7b0000            	stab	L511_Response
5017                         ; 1419 	diagF_DiagResponse(Response, DataLength, pMsgContext);
5019  0d78 ec80              	ldd	OFST+0,s
5020  0d7a 3b                	pshd	
5021  0d7b fc0002            	ldd	L111_DataLength
5022  0d7e 3b                	pshd	
5023  0d7f f60000            	ldab	L511_Response
5024  0d82 87                	clra	
5025  0d83 4a000000          	call	f_diagF_DiagResponse
5027  0d87 1b86              	leas	6,s
5028                         ; 1421 }
5031  0d89 0a                	rtc	
5075                         ; 1427 void DESC_API_CALLBACK_TYPE ApplDescRead_223001_Program_ECU_Data_at_Flash_Station_2(DescMsgContext* pMsgContext)
5075                         ; 1428 {
5076                         	switch	.ftext
5077  0d8a                   f_ApplDescRead_223001_Program_ECU_Data_at_Flash_Station_2:
5079  0d8a 3b                	pshd	
5080       00000000          OFST:	set	0
5083                         ; 1429 	if(diag_eol_io_lock_b)
5085  0d8b 1f00000116        	brclr	_diag_io_lock3_flags_c,1,L5761
5086                         ; 1431 		EE_BlockRead(EE_CTRL_BYTE_FINAL, &pMsgContext->resData[0]);
5088  0d90 b746              	tfr	d,y
5089  0d92 ec44              	ldd	4,y
5090  0d94 3b                	pshd	
5091  0d95 cc0023            	ldd	#35
5092  0d98 4a000000          	call	f_EE_BlockRead
5094  0d9c 1b82              	leas	2,s
5095                         ; 1432 		DataLength = 1;
5097  0d9e cc0001            	ldd	#1
5098  0da1 7c0002            	std	L111_DataLength
5099                         ; 1433 		Response = RESPONSE_OK;
5102  0da4 2008              	bra	L7761
5103  0da6                   L5761:
5104                         ; 1437 		DataLength = 1;
5106  0da6 cc0001            	ldd	#1
5107  0da9 7c0002            	std	L111_DataLength
5108                         ; 1438 		Response = CONDITIONS_NOT_CORRECT;
5110  0dac c606              	ldab	#6
5111  0dae                   L7761:
5112  0dae 7b0000            	stab	L511_Response
5113                         ; 1441 	diagF_DiagResponse(Response, DataLength, pMsgContext);
5115  0db1 ec80              	ldd	OFST+0,s
5116  0db3 3b                	pshd	
5117  0db4 fc0002            	ldd	L111_DataLength
5118  0db7 3b                	pshd	
5119  0db8 f60000            	ldab	L511_Response
5120  0dbb 87                	clra	
5121  0dbc 4a000000          	call	f_diagF_DiagResponse
5123  0dc0 1b86              	leas	6,s
5124                         ; 1442 }
5127  0dc2 0a                	rtc	
5201                         ; 1450 void DESC_API_CALLBACK_TYPE ApplDescReadReadWrite(DescMsgContext* pMsgContext)
5201                         ; 1451 {
5202                         	switch	.ftext
5203  0dc3                   f_ApplDescReadReadWrite:
5205  0dc3 3b                	pshd	
5206  0dc4 1b9c              	leas	-4,s
5207       00000004          OFST:	set	4
5210                         ; 1452 	if(diag_eol_io_lock_b)
5212  0dc6 f60000            	ldab	_diag_io_lock3_flags_c
5213  0dc9 c501              	bitb	#1
5214  0dcb 182700f3          	beq	L7271
5215                         ; 1456 		u_16Bit  diag_vs_StrtUpTm_w = 0;
5217                         ; 1458 		pMsgContext->resData[0] = HIGHBYTE(ROMTst_checksum); //High byte of Rom CHecksum
5219  0dcf f60000            	ldab	_ROMTst_checksum
5220  0dd2 ed84              	ldy	OFST+0,s
5221  0dd4 6beb0004          	stab	[4,y]
5222                         ; 1459 		pMsgContext->resData[1] = LOWBYTE(ROMTst_checksum);  //Low byte of Rom CHecksum
5224  0dd8 ed44              	ldy	4,y
5225  0dda 1809410001        	movb	_ROMTst_checksum+1,1,y
5226                         ; 1460 		pMsgContext->resData[2] = ROMTst_result;  	         // Rom Test Result:0-> Not tested; 1-> Result OK; 2-> Not OK; 3-> Undefined
5228  0ddf 1809420000        	movb	_ROMTst_result,2,y
5229                         ; 1462 		pMsgContext->resData[3] = main_variant_select_c; // Variant selection from Hardware pins
5231  0de4 1809430000        	movb	_main_variant_select_c,3,y
5232                         ; 1464 		pMsgContext->resData[4] = (unsigned char) Dio_ReadChannel(REL_A_MON); //Relay A  Status
5234  0de9 fe0000            	ldx	_Dio_PortRead_Ptr
5235  0dec e600              	ldab	0,x
5236  0dee c420              	andb	#32
5237  0df0 54                	lsrb	
5238  0df1 54                	lsrb	
5239  0df2 54                	lsrb	
5240  0df3 54                	lsrb	
5241  0df4 54                	lsrb	
5242  0df5 6b44              	stab	4,y
5243                         ; 1465 		pMsgContext->resData[5] = (unsigned char) Dio_ReadChannel(REL_B_MON); //Relay B  Status
5245  0df7 e600              	ldab	0,x
5246  0df9 55                	rolb	
5247  0dfa 55                	rolb	
5248  0dfb c401              	andb	#1
5249  0dfd 6b45              	stab	5,y
5250                         ; 1466 	 	pMsgContext->resData[6] = stW_Vsense_LS_c; //Vsense at LOW Side
5252  0dff 1809460000        	movb	_stW_Vsense_LS_c,6,y
5253                         ; 1467 	 	pMsgContext->resData[7] = (unsigned char) Dio_ReadChannel(REL_STW_EN); //Relay C ENable PIN)
5255  0e04 e6fbf200          	ldab	[_Dio_PortRead_Ptr+8]
5256  0e08 c408              	andb	#8
5257  0e0a 54                	lsrb	
5258  0e0b 54                	lsrb	
5259  0e0c 54                	lsrb	
5260  0e0d 6b47              	stab	7,y
5261                         ; 1469 	 	pMsgContext->resData[8] = temperature_NTC_supplyV; //Read U12S PIN
5263  0e0f 1809480000        	movb	_temperature_NTC_supplyV,8,y
5264                         ; 1470 	 	pMsgContext->resData[9] = BattVF_Get_Crt_Uint(); //Read UINIT
5266  0e14 4a000000          	call	f_BattVF_Get_Crt_Uint
5268  0e18 ed84              	ldy	OFST+0,s
5269  0e1a ed44              	ldy	4,y
5270  0e1c 6b49              	stab	9,y
5271                         ; 1471 		pMsgContext->resData[10] = (u_8Bit) (temperature_PCB_ADC>>8);    // Read PCB Temp High Byte
5273  0e1e 18094a0000        	movb	_temperature_PCB_ADC,10,y
5274                         ; 1472 		pMsgContext->resData[11] = (u_8Bit) (temperature_PCB_ADC);       // Read PCB Temp Low Byte 
5276  0e23 18094b0001        	movb	_temperature_PCB_ADC+1,11,y
5277                         ; 1474 		pMsgContext->resData[12] = hs_Vsense_c[FRONT_LEFT]; // F L Seat Vsense AD Reading
5279  0e28 18094c0000        	movb	_hs_Vsense_c,12,y
5280                         ; 1475 		pMsgContext->resData[13] = hs_Vsense_c[FRONT_RIGHT]; // F R Seat Vsense AD Reading
5282  0e2d 18094d0001        	movb	_hs_Vsense_c+1,13,y
5283                         ; 1476 		pMsgContext->resData[14] = hs_Vsense_c[REAR_LEFT];  // R L Seat Vsense AD Reading
5285  0e32 18094e0002        	movb	_hs_Vsense_c+2,14,y
5286                         ; 1477 		pMsgContext->resData[15] = hs_Vsense_c[REAR_RIGHT];  // R R Seat Vsense AD Reading
5288  0e37 18094f0003        	movb	_hs_Vsense_c+3,15,y
5289                         ; 1479 		pMsgContext->resData[16] = stW_Vsense_HS_c;         //Steering Wheel High Side Vsense
5291  0e3c 1809e8100000      	movb	_stW_Vsense_HS_c,16,y
5292                         ; 1480 		pMsgContext->resData[17] = stW_Isense_c;			//Steering Wheel High Side Isense
5294  0e42 1809e8110000      	movb	_stW_Isense_c,17,y
5295                         ; 1483 		pMsgContext->resData[18] = vs_Vsense_c[VS_FL]; // VL Vsense 
5297  0e48 1809e8120000      	movb	_vs_Vsense_c,18,y
5298                         ; 1484 		pMsgContext->resData[19] = vs_Vsense_c[VS_FR]; // VR Vsense
5300  0e4e 1809e8130001      	movb	_vs_Vsense_c+1,19,y
5301                         ; 1485 		pMsgContext->resData[20] = vs_Isense_c[VS_FL]; // VL Isense
5303  0e54 1809e8140000      	movb	_vs_Isense_c,20,y
5304                         ; 1486 		pMsgContext->resData[21] = vs_Isense_c[VS_FR]; // VR Isense
5306  0e5a 1809e8150001      	movb	_vs_Isense_c+1,21,y
5307                         ; 1489 		diag_vs_StrtUpTm_w = Vs_flt_cal_prms.start_up_tm;	
5309  0e60 fc0004            	ldd	_Vs_flt_cal_prms+4
5310  0e63 6c80              	std	OFST-4,s
5311                         ; 1490 		pMsgContext->resData[22] = (u_8Bit) HIGHBYTE(diag_vs_StrtUpTm_w); /* Store High byte */
5313  0e65 6ae816            	staa	22,y
5314                         ; 1491 		pMsgContext->resData[23] = (u_8Bit) LOWBYTE(diag_vs_StrtUpTm_w); /* Store Low Byte */
5316  0e68 6be817            	stab	23,y
5317                         ; 1493 		diag_vs_StrtUpTm_w = vsF_Modify_StartUp_Tm(); //Turn OFF Vent RampUp
5319  0e6b 4a000000          	call	f_vsF_Modify_StartUp_Tm
5321  0e6f 6c80              	std	OFST-4,s
5322                         ; 1494 		pMsgContext->resData[24] = (u_8Bit) HIGHBYTE(diag_vs_StrtUpTm_w); //Store High byte
5324  0e71 ed84              	ldy	OFST+0,s
5325  0e73 ed44              	ldy	4,y
5326  0e75 6ae818            	staa	24,y
5327                         ; 1495 		pMsgContext->resData[25] = (u_8Bit) LOWBYTE(diag_vs_StrtUpTm_w);  //Store LOW Byte
5329  0e78 6be819            	stab	25,y
5330                         ; 1497 		pMsgContext->resData[26] = (unsigned char) Dio_ReadChannel(IGN_REAR_MON);
5332  0e7b e6fbf189          	ldab	[_Dio_PortRead_Ptr+8]
5333  0e7f c404              	andb	#4
5334  0e81 54                	lsrb	
5335  0e82 54                	lsrb	
5336  0e83 6be81a            	stab	26,y
5337                         ; 1498 		pMsgContext->resData[27] = (u_8Bit) Dio_ReadChannel(IGN_WHEEL_MON);
5339  0e86 fe0000            	ldx	_Dio_PortRead_Ptr
5340  0e89 e600              	ldab	0,x
5341  0e8b 55                	rolb	
5342  0e8c 55                	rolb	
5343  0e8d 55                	rolb	
5344  0e8e c401              	andb	#1
5345  0e90 6be81b            	stab	27,y
5346                         ; 1500 		pMsgContext->resData[28] = (u_8Bit) Dio_ReadChannel(RL_LED_SW_DETECT);
5348  0e93 e600              	ldab	0,x
5349  0e95 c402              	andb	#2
5350  0e97 54                	lsrb	
5351  0e98 6be81c            	stab	28,y
5352                         ; 1501 		pMsgContext->resData[29] = (u_8Bit) Dio_ReadChannel(RR_LED_SW_DETECT);
5354  0e9b e600              	ldab	0,x
5355  0e9d c404              	andb	#4
5356  0e9f 54                	lsrb	
5357  0ea0 54                	lsrb	
5358  0ea1 6be81d            	stab	29,y
5359                         ; 1503 		pChar = (unsigned char *)((unsigned int *)(0x004000));
5361                         ; 1505 		memcpy( &pMsgContext->resData[30],pChar,5); //To store, from , total length
5363  0ea4 ed84              	ldy	OFST+0,s
5364  0ea6 ee44              	ldx	4,y
5365  0ea8 1ae01e            	leax	30,x
5366  0eab cd4000            	ldy	#16384
5367  0eae cc0005            	ldd	#5
5368  0eb1                   L621:
5369  0eb1 180a7030          	movb	1,y+,1,x+
5370  0eb5 0434f9            	dbne	d,L621
5371                         ; 1508 		DataLength = 50;
5373  0eb8 cc0032            	ldd	#50
5374  0ebb 7c0002            	std	L111_DataLength
5375                         ; 1509 		Response = RESPONSE_OK;
5377  0ebe c601              	ldab	#1
5379  0ec0 2008              	bra	L1371
5380  0ec2                   L7271:
5381                         ; 1513 		DataLength = 1;
5383  0ec2 cc0001            	ldd	#1
5384  0ec5 7c0002            	std	L111_DataLength
5385                         ; 1514 		Response = CONDITIONS_NOT_CORRECT;
5387  0ec8 c606              	ldab	#6
5388  0eca                   L1371:
5389  0eca 7b0000            	stab	L511_Response
5390                         ; 1517 	diagF_DiagResponse(Response, DataLength, pMsgContext);
5392  0ecd ec84              	ldd	OFST+0,s
5393  0ecf 3b                	pshd	
5394  0ed0 fc0002            	ldd	L111_DataLength
5395  0ed3 3b                	pshd	
5396  0ed4 f60000            	ldab	L511_Response
5397  0ed7 87                	clra	
5398  0ed8 4a000000          	call	f_diagF_DiagResponse
5400                         ; 1518 }
5403  0edc 1b8a              	leas	10,s
5404  0ede 0a                	rtc	
5447                         ; 1526 void DESC_API_CALLBACK_TYPE ApplDescRead_22F100_Active_Diagnostic_Information(DescMsgContext* pMsgContext)
5447                         ; 1527 {
5448                         	switch	.ftext
5449  0edf                   f_ApplDescRead_22F100_Active_Diagnostic_Information:
5451  0edf 3b                	pshd	
5452       00000000          OFST:	set	0
5455                         ; 1539 	pMsgContext->resData[0] = 0x00; //Active Diagnostic Status of ECU 
5457  0ee0 b746              	tfr	d,y
5458  0ee2 69eb0004          	clr	[4,y]
5459                         ; 1540 	pMsgContext->resData[1] = 0x60; //Diagnostic variant
5461  0ee6 c660              	ldab	#96
5462  0ee8 ed80              	ldy	OFST+0,s
5463  0eea ed44              	ldy	4,y
5464  0eec 6b41              	stab	1,y
5465                         ; 1541 	pMsgContext->resData[2] = 0x00; //Diagnostic version
5467  0eee 6942              	clr	2,y
5468                         ; 1542 	pMsgContext->resData[3] = DescGetSessionIdOfSessionState(DescGetStateSession());
5470  0ef0 4a000000          	call	f_DescGetStateSession
5472  0ef4 4a000000          	call	f_DescGetSessionIdOfSessionState
5474  0ef8 ed80              	ldy	OFST+0,s
5475  0efa ed44              	ldy	4,y
5476  0efc 6b43              	stab	3,y
5477                         ; 1543 	pMsgContext->resData[4] = 0x00; //Reserved byte
5479  0efe 6944              	clr	4,y
5480                         ; 1545 	DataLength = 5;
5482  0f00 cc0005            	ldd	#5
5483  0f03 7c0002            	std	L111_DataLength
5484                         ; 1546 	Response = RESPONSE_OK;
5486  0f06 c601              	ldab	#1
5487  0f08 7b0000            	stab	L511_Response
5488                         ; 1547 	diagF_DiagResponse(Response, DataLength, pMsgContext);
5490  0f0b ec80              	ldd	OFST+0,s
5491  0f0d 3b                	pshd	
5492  0f0e cc0005            	ldd	#5
5493  0f11 3b                	pshd	
5494  0f12 c601              	ldab	#1
5495  0f14 4a000000          	call	f_diagF_DiagResponse
5497  0f18 1b86              	leas	6,s
5498                         ; 1548 }
5501  0f1a 0a                	rtc	
5548                         ; 1556 void DESC_API_CALLBACK_TYPE ApplDescRead_22F10B_ECU_Configuration(DescMsgContext* pMsgContext)
5548                         ; 1557 {
5549                         	switch	.ftext
5550  0f1b                   f_ApplDescRead_22F10B_ECU_Configuration:
5552  0f1b 3b                	pshd	
5553       00000000          OFST:	set	0
5556                         ; 1576     diag_srvc_22F10B_byte0 = 0;
5558  0f1c 790008            	clr	_diag_srvc_22F10B_byte0
5559                         ; 1577     diag_srvc_22F10B_byte1 = 0;
5561  0f1f 790007            	clr	_diag_srvc_22F10B_byte1
5562                         ; 1578     diag_srvc_22F10B_byte2 = 0;
5564  0f22 790006            	clr	_diag_srvc_22F10B_byte2
5565                         ; 1583 	if (CSWM_config_c & CSWM_2HS_VARIANT_1_K) {
5567  0f25 1f00000306        	brclr	_CSWM_config_c,3,L3771
5568                         ; 1584 		 diag_srvc_22F10B_byte0 |= FRONT_HEAT_PRESENT;
5570  0f2a 1c000801          	bset	_diag_srvc_22F10B_byte0,1
5572  0f2e 2004              	bra	L5771
5573  0f30                   L3771:
5574                         ; 1586 		 diag_srvc_22F10B_byte0	&= (~FRONT_HEAT_PRESENT);
5576  0f30 1d000801          	bclr	_diag_srvc_22F10B_byte0,1
5577  0f34                   L5771:
5578                         ; 1590 	if (CSWM_config_c & REAR_HEATER_MASK_K) {
5580  0f34 1f00000c06        	brclr	_CSWM_config_c,12,L7771
5581                         ; 1591 		 diag_srvc_22F10B_byte0 |= FRONT_REAR_HEAT_PRESENT;
5583  0f39 1c000802          	bset	_diag_srvc_22F10B_byte0,2
5585  0f3d 2004              	bra	L1002
5586  0f3f                   L7771:
5587                         ; 1593 		 diag_srvc_22F10B_byte0	&= (~FRONT_REAR_HEAT_PRESENT);
5589  0f3f 1d000802          	bclr	_diag_srvc_22F10B_byte0,2
5590  0f43                   L1002:
5591                         ; 1597 	if (CSWM_config_c & CSWM_2VS_MASK_K) {
5593  0f43 1f00003006        	brclr	_CSWM_config_c,48,L3002
5594                         ; 1598 		 diag_srvc_22F10B_byte0 |= VENT_PRESENT;
5596  0f48 1c000804          	bset	_diag_srvc_22F10B_byte0,4
5598  0f4c 2004              	bra	L5002
5599  0f4e                   L3002:
5600                         ; 1600 		 diag_srvc_22F10B_byte0	&= (~VENT_PRESENT);
5602  0f4e 1d000804          	bclr	_diag_srvc_22F10B_byte0,4
5603  0f52                   L5002:
5604                         ; 1604 	if (CSWM_config_c & CSWM_HSW_MASK_K) {
5606  0f52 1f00004006        	brclr	_CSWM_config_c,64,L7002
5607                         ; 1605 		 diag_srvc_22F10B_byte0 |= STW_PRESENT;
5609  0f57 1c000808          	bset	_diag_srvc_22F10B_byte0,8
5611  0f5b 2004              	bra	L1102
5612  0f5d                   L7002:
5613                         ; 1607 		 diag_srvc_22F10B_byte0	&= (~STW_PRESENT);
5615  0f5d 1d000808          	bclr	_diag_srvc_22F10B_byte0,8
5616  0f61                   L1102:
5617                         ; 1610 	pMsgContext->resData[0] = diag_srvc_22F10B_byte0; 
5619  0f61 f60008            	ldab	_diag_srvc_22F10B_byte0
5620  0f64 ee80              	ldx	OFST+0,s
5621  0f66 6be30004          	stab	[4,x]
5622                         ; 1614 	if (ru_eep_PROXI_Cfg.s.Seat_Material == PROXI_SEAT_TYPE_CLOTH) {
5624  0f6a f60009            	ldab	_ru_eep_PROXI_Cfg+9
5625  0f6d c430              	andb	#48
5626  0f6f c110              	cmpb	#16
5627  0f71 2606              	bne	L3102
5628                         ; 1615 		 diag_srvc_22F10B_byte1 |= SEAT_TYPE_CLOTH;
5630  0f73 1c000701          	bset	_diag_srvc_22F10B_byte1,1
5632  0f77 2004              	bra	L5102
5633  0f79                   L3102:
5634                         ; 1617 		 diag_srvc_22F10B_byte1	&= (~SEAT_TYPE_CLOTH);
5636  0f79 1d000701          	bclr	_diag_srvc_22F10B_byte1,1
5637  0f7d                   L5102:
5638                         ; 1622 	if (ru_eep_PROXI_Cfg.s.Seat_Material == PROXI_SEAT_TYPE_PERF) {
5640  0f7d 1e00093002        	brset	_ru_eep_PROXI_Cfg+9,48,LC009
5641  0f82 2006              	bra	L7102
5642  0f84                   LC009:
5643                         ; 1623 		 diag_srvc_22F10B_byte1 |= SEAT_TYPE_PERF_LEATHER;
5645  0f84 1c000702          	bset	_diag_srvc_22F10B_byte1,2
5647  0f88 2004              	bra	L1202
5648  0f8a                   L7102:
5649                         ; 1625 		 diag_srvc_22F10B_byte1	&= (~SEAT_TYPE_PERF_LEATHER);
5651  0f8a 1d000702          	bclr	_diag_srvc_22F10B_byte1,2
5652  0f8e                   L1202:
5653                         ; 1629 	if (ru_eep_PROXI_Cfg.s.Seat_Material == PROXI_SEAT_TYPE_LEATHER) {
5655  0f8e f60009            	ldab	_ru_eep_PROXI_Cfg+9
5656  0f91 c430              	andb	#48
5657  0f93 c120              	cmpb	#32
5658  0f95 2606              	bne	L3202
5659                         ; 1630 		 diag_srvc_22F10B_byte1 |= SEAT_TYPE_LEATHER;
5661  0f97 1c000704          	bset	_diag_srvc_22F10B_byte1,4
5663  0f9b 2004              	bra	L5202
5664  0f9d                   L3202:
5665                         ; 1632 		 diag_srvc_22F10B_byte1	&= (~SEAT_TYPE_LEATHER);
5667  0f9d 1d000704          	bclr	_diag_srvc_22F10B_byte1,4
5668  0fa1                   L5202:
5669                         ; 1636 	if (ru_eep_PROXI_Cfg.s.Wheel_Material == PROXI_WHEEL_TYPE_WOOD) {
5671  0fa1 f60009            	ldab	_ru_eep_PROXI_Cfg+9
5672  0fa4 c4c0              	andb	#192
5673  0fa6 c140              	cmpb	#64
5674  0fa8 2606              	bne	L7202
5675                         ; 1637 		 diag_srvc_22F10B_byte1 |= WHEEL_TYPE_WOOD;
5677  0faa 1c000708          	bset	_diag_srvc_22F10B_byte1,8
5679  0fae 2004              	bra	L1302
5680  0fb0                   L7202:
5681                         ; 1639 		 diag_srvc_22F10B_byte1	&= (~WHEEL_TYPE_WOOD);
5683  0fb0 1d000708          	bclr	_diag_srvc_22F10B_byte1,8
5684  0fb4                   L1302:
5685                         ; 1643 	if (ru_eep_PROXI_Cfg.s.Wheel_Material == PROXI_WHEEL_TYPE_LEATHER) {
5687  0fb4 f60009            	ldab	_ru_eep_PROXI_Cfg+9
5688  0fb7 c4c0              	andb	#192
5689  0fb9 c180              	cmpb	#128
5690  0fbb 2606              	bne	L3302
5691                         ; 1644 		 diag_srvc_22F10B_byte1 |= WHEEL_TYPE_LEATHER;
5693  0fbd 1c000710          	bset	_diag_srvc_22F10B_byte1,16
5695  0fc1 2004              	bra	L5302
5696  0fc3                   L3302:
5697                         ; 1646 		 diag_srvc_22F10B_byte1	&= (~WHEEL_TYPE_LEATHER);
5699  0fc3 1d000710          	bclr	_diag_srvc_22F10B_byte1,16
5700  0fc7                   L5302:
5701                         ; 1650 	if ( (ru_eep_PROXI_Cfg.s.Heated_Seats_Variant == PROXI_HEATSEAT_FRONT)||  (ru_eep_PROXI_Cfg.s.Heated_Seats_Variant == PROXI_HEATSEAT_FRONT_REAR) ) {
5703  0fc7 f60009            	ldab	_ru_eep_PROXI_Cfg+9
5704  0fca c406              	andb	#6
5705  0fcc c102              	cmpb	#2
5706  0fce 2709              	beq	L1402
5708  0fd0 f60009            	ldab	_ru_eep_PROXI_Cfg+9
5709  0fd3 c406              	andb	#6
5710  0fd5 c104              	cmpb	#4
5711  0fd7 2613              	bne	L7302
5712  0fd9                   L1402:
5713                         ; 1651 		 diag_srvc_22F10B_byte2 |= HEATSEAT_FRONT_P;
5715  0fd9 1c000601          	bset	_diag_srvc_22F10B_byte2,1
5717  0fdd                   L3402:
5718                         ; 1657 	if (ru_eep_PROXI_Cfg.s.Heated_Seats_Variant == PROXI_HEATSEAT_FRONT_REAR) {
5720  0fdd f60009            	ldab	_ru_eep_PROXI_Cfg+9
5721  0fe0 c406              	andb	#6
5722  0fe2 c104              	cmpb	#4
5723  0fe4 260c              	bne	L5402
5724                         ; 1658 		 diag_srvc_22F10B_byte2 |= HEATSEAT_FRONT_REAR_P;
5726  0fe6 1c000602          	bset	_diag_srvc_22F10B_byte2,2
5728  0fea 200a              	bra	L7402
5729  0fec                   L7302:
5730                         ; 1653 		 diag_srvc_22F10B_byte2	&= (~HEATSEAT_FRONT_P);
5732  0fec 1d000601          	bclr	_diag_srvc_22F10B_byte2,1
5733  0ff0 20eb              	bra	L3402
5734  0ff2                   L5402:
5735                         ; 1660 		 diag_srvc_22F10B_byte2	&= (~HEATSEAT_FRONT_REAR_P);
5737  0ff2 1d000602          	bclr	_diag_srvc_22F10B_byte2,2
5738  0ff6                   L7402:
5739                         ; 1665 		 diag_srvc_22F10B_byte2	&= (~VENTSEAT_FRONT_P);
5741  0ff6 1d000604          	bclr	_diag_srvc_22F10B_byte2,4
5742                         ; 1668 	if (ru_eep_PROXI_Cfg.s.Steering_Wheel == PROXI_WHEEL_PRESENT) {
5744  0ffa 1f000a0106        	brclr	_ru_eep_PROXI_Cfg+10,1,L1502
5745                         ; 1669 		 diag_srvc_22F10B_byte2 |= STEERING_WHEEL_P;
5747  0fff 1c000608          	bset	_diag_srvc_22F10B_byte2,8
5749  1003 2004              	bra	L3502
5750  1005                   L1502:
5751                         ; 1671 		 diag_srvc_22F10B_byte2	&= (~STEERING_WHEEL_P);
5753  1005 1d000608          	bclr	_diag_srvc_22F10B_byte2,8
5754  1009                   L3502:
5755                         ; 1674 	pMsgContext->resData[1] = diag_srvc_22F10B_byte1;                                    
5757  1009 ed80              	ldy	OFST+0,s
5758  100b ed44              	ldy	4,y
5759  100d 1809410007        	movb	_diag_srvc_22F10B_byte1,1,y
5760                         ; 1675 	pMsgContext->resData[2] = diag_srvc_22F10B_byte2;
5762  1012 1809420006        	movb	_diag_srvc_22F10B_byte2,2,y
5763                         ; 1676 	pMsgContext->resData[3] = hardware_proxi_mismatch_c;	 	// Hardware / Proxi Mismatch
5765  1017 1809430000        	movb	_hardware_proxi_mismatch_c,3,y
5766                         ; 1680 	DataLength = 4;
5768  101c cc0004            	ldd	#4
5769  101f 7c0002            	std	L111_DataLength
5770                         ; 1681 	Response = RESPONSE_OK;
5772  1022 c601              	ldab	#1
5773  1024 7b0000            	stab	L511_Response
5774                         ; 1682 	diagF_DiagResponse(Response, DataLength, pMsgContext);}
5776  1027 ec80              	ldd	OFST+0,s
5777  1029 3b                	pshd	
5778  102a cc0004            	ldd	#4
5779  102d 3b                	pshd	
5780  102e c601              	ldab	#1
5781  1030 4a000000          	call	f_diagF_DiagResponse
5783  1034 1b86              	leas	6,s
5787  1036 0a                	rtc	
5829                         ; 1689 void DESC_API_CALLBACK_TYPE ApplDescRead_22F112_Hardware_Part_Number(DescMsgContext* pMsgContext)
5829                         ; 1690 {
5830                         	switch	.ftext
5831  1037                   f_ApplDescRead_22F112_Hardware_Part_Number:
5833  1037 3b                	pshd	
5834       00000000          OFST:	set	0
5837                         ; 1693 	EE_BlockRead(EE_FBL_CHR_ECU_PNO, &pMsgContext->resData[0]); //ECU PNO, SW PNO and HW PNO all are same.
5839  1038 b746              	tfr	d,y
5840  103a ec44              	ldd	4,y
5841  103c 3b                	pshd	
5842  103d cc0046            	ldd	#70
5843  1040 4a000000          	call	f_EE_BlockRead
5845  1044 1b82              	leas	2,s
5846                         ; 1696 	DataLength = 10;
5848  1046 cc000a            	ldd	#10
5849  1049 7c0002            	std	L111_DataLength
5850                         ; 1697 	Response = RESPONSE_OK;
5852  104c c601              	ldab	#1
5853  104e 7b0000            	stab	L511_Response
5854                         ; 1698 	diagF_DiagResponse(Response, DataLength, pMsgContext);
5856  1051 ec80              	ldd	OFST+0,s
5857  1053 3b                	pshd	
5858  1054 cc000a            	ldd	#10
5859  1057 3b                	pshd	
5860  1058 c601              	ldab	#1
5861  105a 4a000000          	call	f_diagF_DiagResponse
5863  105e 1b86              	leas	6,s
5864                         ; 1699 }
5867  1060 0a                	rtc	
5909                         ; 1707 void DESC_API_CALLBACK_TYPE ApplDescRead_22F122_Software_Part_Number(DescMsgContext* pMsgContext)
5909                         ; 1708 {
5910                         	switch	.ftext
5911  1061                   f_ApplDescRead_22F122_Software_Part_Number:
5913  1061 3b                	pshd	
5914       00000000          OFST:	set	0
5917                         ; 1711     EE_BlockRead(EE_FBL_CHR_APPL_SW_PNO, &pMsgContext->resData[0]);
5919  1062 b746              	tfr	d,y
5920  1064 ec44              	ldd	4,y
5921  1066 3b                	pshd	
5922  1067 cc0044            	ldd	#68
5923  106a 4a000000          	call	f_EE_BlockRead
5925  106e 1b82              	leas	2,s
5926                         ; 1713 	DataLength = 10;
5928  1070 cc000a            	ldd	#10
5929  1073 7c0002            	std	L111_DataLength
5930                         ; 1714 	Response = RESPONSE_OK;
5932  1076 c601              	ldab	#1
5933  1078 7b0000            	stab	L511_Response
5934                         ; 1715 	diagF_DiagResponse(Response, DataLength, pMsgContext);
5936  107b ec80              	ldd	OFST+0,s
5937  107d 3b                	pshd	
5938  107e cc000a            	ldd	#10
5939  1081 3b                	pshd	
5940  1082 c601              	ldab	#1
5941  1084 4a000000          	call	f_diagF_DiagResponse
5943  1088 1b86              	leas	6,s
5944                         ; 1716 }
5947  108a 0a                	rtc	
5988                         ; 1723 void DESC_API_CALLBACK_TYPE ApplDescRead_22F132_ECU_Part_Number(DescMsgContext* pMsgContext)
5988                         ; 1724 {
5989                         	switch	.ftext
5990  108b                   f_ApplDescRead_22F132_ECU_Part_Number:
5992  108b 3b                	pshd	
5993       00000000          OFST:	set	0
5996                         ; 1727 	EE_BlockRead(EE_FBL_CHR_ECU_PNO, &pMsgContext->resData[0]);
5998  108c b746              	tfr	d,y
5999  108e ec44              	ldd	4,y
6000  1090 3b                	pshd	
6001  1091 cc0046            	ldd	#70
6002  1094 4a000000          	call	f_EE_BlockRead
6004  1098 1b82              	leas	2,s
6005                         ; 1729 	DataLength = 10;
6007  109a cc000a            	ldd	#10
6008  109d 7c0002            	std	L111_DataLength
6009                         ; 1730 	Response = RESPONSE_OK;
6011  10a0 c601              	ldab	#1
6012  10a2 7b0000            	stab	L511_Response
6013                         ; 1731 	diagF_DiagResponse(Response, DataLength, pMsgContext);
6015  10a5 ec80              	ldd	OFST+0,s
6016  10a7 3b                	pshd	
6017  10a8 cc000a            	ldd	#10
6018  10ab 3b                	pshd	
6019  10ac c601              	ldab	#1
6020  10ae 4a000000          	call	f_diagF_DiagResponse
6022  10b2 1b86              	leas	6,s
6023                         ; 1732 }
6026  10b4 0a                	rtc	
6068                         ; 1739 void DESC_API_CALLBACK_TYPE ApplDescReadBootSoftwareIdentification(DescMsgContext* pMsgContext)
6068                         ; 1740 {
6069                         	switch	.ftext
6070  10b5                   f_ApplDescReadBootSoftwareIdentification:
6072  10b5 3b                	pshd	
6073       00000000          OFST:	set	0
6076                         ; 1747 	pMsgContext->resData[0] = 0x01; //Number of Modules
6078  10b6 c601              	ldab	#1
6079  10b8 ed80              	ldy	OFST+0,s
6080  10ba ee44              	ldx	4,y
6081  10bc 6b30              	stab	1,x+
6082                         ; 1748 	EE_BlockRead(EE_FBL_BOOT_SW_IDENT, &pMsgContext->resData[1]); //BOOT SW IDENTIFICATION.
6084  10be 34                	pshx	
6085  10bf cc0037            	ldd	#55
6086  10c2 4a000000          	call	f_EE_BlockRead
6088  10c6 1b82              	leas	2,s
6089                         ; 1750 	DataLength = 14;
6091  10c8 cc000e            	ldd	#14
6092  10cb 7c0002            	std	L111_DataLength
6093                         ; 1751 	Response = RESPONSE_OK;
6095  10ce c601              	ldab	#1
6096  10d0 7b0000            	stab	L511_Response
6097                         ; 1752 	diagF_DiagResponse(Response, DataLength, pMsgContext);
6099  10d3 ec80              	ldd	OFST+0,s
6100  10d5 3b                	pshd	
6101  10d6 cc000e            	ldd	#14
6102  10d9 3b                	pshd	
6103  10da c601              	ldab	#1
6104  10dc 4a000000          	call	f_diagF_DiagResponse
6106  10e0 1b86              	leas	6,s
6107                         ; 1753 }
6110  10e2 0a                	rtc	
6152                         ; 1760 void DESC_API_CALLBACK_TYPE ApplDescReadApplicationSoftwareIdentification(DescMsgContext* pMsgContext)
6152                         ; 1761 {
6153                         	switch	.ftext
6154  10e3                   f_ApplDescReadApplicationSoftwareIdentification:
6156  10e3 3b                	pshd	
6157       00000000          OFST:	set	0
6160                         ; 1768 	pMsgContext->resData[0] = 0x01; //Number of Modules
6162  10e4 c601              	ldab	#1
6163  10e6 ed80              	ldy	OFST+0,s
6164  10e8 ee44              	ldx	4,y
6165  10ea 6b30              	stab	1,x+
6166                         ; 1769 	EE_BlockRead(EE_FBL_APPL_SW_IDENT, &pMsgContext->resData[1]); //APPL SW IDENTIFICATION.
6168  10ec 34                	pshx	
6169  10ed cc0038            	ldd	#56
6170  10f0 4a000000          	call	f_EE_BlockRead
6172  10f4 1b82              	leas	2,s
6173                         ; 1771 	DataLength = 14;
6175  10f6 cc000e            	ldd	#14
6176  10f9 7c0002            	std	L111_DataLength
6177                         ; 1772 	Response = RESPONSE_OK;
6179  10fc c601              	ldab	#1
6180  10fe 7b0000            	stab	L511_Response
6181                         ; 1773 	diagF_DiagResponse(Response, DataLength, pMsgContext);
6183  1101 ec80              	ldd	OFST+0,s
6184  1103 3b                	pshd	
6185  1104 cc000e            	ldd	#14
6186  1107 3b                	pshd	
6187  1108 c601              	ldab	#1
6188  110a 4a000000          	call	f_diagF_DiagResponse
6190  110e 1b86              	leas	6,s
6191                         ; 1774 }
6194  1110 0a                	rtc	
6236                         ; 1781 void DESC_API_CALLBACK_TYPE ApplDescReadApplicationDataIdentification(DescMsgContext* pMsgContext)
6236                         ; 1782 {
6237                         	switch	.ftext
6238  1111                   f_ApplDescReadApplicationDataIdentification:
6240  1111 3b                	pshd	
6241       00000000          OFST:	set	0
6244                         ; 1789 	pMsgContext->resData[0] = 0x01; //Number of Modules
6246  1112 c601              	ldab	#1
6247  1114 ed80              	ldy	OFST+0,s
6248  1116 ee44              	ldx	4,y
6249  1118 6b30              	stab	1,x+
6250                         ; 1790 	EE_BlockRead(EE_FBL_APPL_DATA_IDENT, &pMsgContext->resData[1]); //APPL SW IDENTIFICATION.
6252  111a 34                	pshx	
6253  111b cc0039            	ldd	#57
6254  111e 4a000000          	call	f_EE_BlockRead
6256  1122 1b82              	leas	2,s
6257                         ; 1792 	DataLength = 14;
6259  1124 cc000e            	ldd	#14
6260  1127 7c0002            	std	L111_DataLength
6261                         ; 1793 	Response = RESPONSE_OK;
6263  112a c601              	ldab	#1
6264  112c 7b0000            	stab	L511_Response
6265                         ; 1794 	diagF_DiagResponse(Response, DataLength, pMsgContext);
6267  112f ec80              	ldd	OFST+0,s
6268  1131 3b                	pshd	
6269  1132 cc000e            	ldd	#14
6270  1135 3b                	pshd	
6271  1136 c601              	ldab	#1
6272  1138 4a000000          	call	f_diagF_DiagResponse
6274  113c 1b86              	leas	6,s
6275                         ; 1795 }
6278  113e 0a                	rtc	
6318                         ; 1802 void DESC_API_CALLBACK_TYPE ApplDescReadBootSoftwareFingerprint(DescMsgContext* pMsgContext)
6318                         ; 1803 {
6319                         	switch	.ftext
6320  113f                   f_ApplDescReadBootSoftwareFingerprint:
6322  113f 3b                	pshd	
6323       00000000          OFST:	set	0
6326                         ; 1812 	pMsgContext->resData[0] = 0x01; //Module ID
6328  1140 c601              	ldab	#1
6329  1142 ed80              	ldy	OFST+0,s
6330  1144 6beb0004          	stab	[4,y]
6331                         ; 1813 	pMsgContext->resData[1] = 0x20; 
6333  1148 c620              	ldab	#32
6334  114a ed44              	ldy	4,y
6335  114c 6b41              	stab	1,y
6336                         ; 1814 	pMsgContext->resData[2] = 0x20; 
6338  114e 6b42              	stab	2,y
6339                         ; 1815 	pMsgContext->resData[3] = 0x20; 
6341  1150 6b43              	stab	3,y
6342                         ; 1816 	pMsgContext->resData[4] = 0x20; 
6344  1152 6b44              	stab	4,y
6345                         ; 1817 	pMsgContext->resData[5] = 0x20; 
6347  1154 6b45              	stab	5,y
6348                         ; 1818 	pMsgContext->resData[6] = 0x20; 
6350  1156 6b46              	stab	6,y
6351                         ; 1819 	pMsgContext->resData[7] = 0x20; 
6353  1158 6b47              	stab	7,y
6354                         ; 1820 	pMsgContext->resData[8] = 0x20; 
6356  115a 6b48              	stab	8,y
6357                         ; 1821 	pMsgContext->resData[9] = 0x20; 
6359  115c 6b49              	stab	9,y
6360                         ; 1822 	pMsgContext->resData[10] = 0x11; //Boot Program year
6362  115e c611              	ldab	#17
6363  1160 6b4a              	stab	10,y
6364                         ; 1823 	pMsgContext->resData[11] = 0x08; //Boot Program Month 
6366  1162 c608              	ldab	#8
6367  1164 6b4b              	stab	11,y
6368                         ; 1824 	pMsgContext->resData[12] = 0x08; //Boot Program Date
6370  1166 6b4c              	stab	12,y
6371                         ; 1826 	DataLength = 13;
6373  1168 cc000d            	ldd	#13
6374  116b 7c0002            	std	L111_DataLength
6375                         ; 1827 	Response = RESPONSE_OK;
6377  116e c601              	ldab	#1
6378  1170 7b0000            	stab	L511_Response
6379                         ; 1828 	diagF_DiagResponse(Response, DataLength, pMsgContext);
6381  1173 ec80              	ldd	OFST+0,s
6382  1175 3b                	pshd	
6383  1176 cc000d            	ldd	#13
6384  1179 3b                	pshd	
6385  117a c601              	ldab	#1
6386  117c 4a000000          	call	f_diagF_DiagResponse
6388  1180 1b86              	leas	6,s
6389                         ; 1829 }
6392  1182 0a                	rtc	
6434                         ; 1836 void DESC_API_CALLBACK_TYPE ApplDescReadApplicationSoftwareFingerprint(DescMsgContext* pMsgContext)
6434                         ; 1837 {
6435                         	switch	.ftext
6436  1183                   f_ApplDescReadApplicationSoftwareFingerprint:
6438  1183 3b                	pshd	
6439       00000000          OFST:	set	0
6442                         ; 1845 	EE_BlockRead(EE_FBL_APPL_SW_FPRINT, &pMsgContext->resData[0]); //Tester Code + Pgm date
6444  1184 b746              	tfr	d,y
6445  1186 ec44              	ldd	4,y
6446  1188 3b                	pshd	
6447  1189 cc003b            	ldd	#59
6448  118c 4a000000          	call	f_EE_BlockRead
6450  1190 1b82              	leas	2,s
6451                         ; 1846 	DataLength = 13;
6453  1192 cc000d            	ldd	#13
6454  1195 7c0002            	std	L111_DataLength
6455                         ; 1847 	Response = RESPONSE_OK;
6457  1198 c601              	ldab	#1
6458  119a 7b0000            	stab	L511_Response
6459                         ; 1848 	diagF_DiagResponse(Response, DataLength, pMsgContext);
6461  119d ec80              	ldd	OFST+0,s
6462  119f 3b                	pshd	
6463  11a0 cc000d            	ldd	#13
6464  11a3 3b                	pshd	
6465  11a4 c601              	ldab	#1
6466  11a6 4a000000          	call	f_diagF_DiagResponse
6468  11aa 1b86              	leas	6,s
6469                         ; 1849 }
6472  11ac 0a                	rtc	
6514                         ; 1856 void DESC_API_CALLBACK_TYPE ApplDescReadApplicationDataFingerprint(DescMsgContext* pMsgContext)
6514                         ; 1857 {
6515                         	switch	.ftext
6516  11ad                   f_ApplDescReadApplicationDataFingerprint:
6518  11ad 3b                	pshd	
6519       00000000          OFST:	set	0
6522                         ; 1865 	EE_BlockRead(EE_FBL_APPL_DATA_FPRINT, &pMsgContext->resData[0]); //Tester Code + Pgm date
6524  11ae b746              	tfr	d,y
6525  11b0 ec44              	ldd	4,y
6526  11b2 3b                	pshd	
6527  11b3 cc003c            	ldd	#60
6528  11b6 4a000000          	call	f_EE_BlockRead
6530  11ba 1b82              	leas	2,s
6531                         ; 1866 	DataLength = 13;
6533  11bc cc000d            	ldd	#13
6534  11bf 7c0002            	std	L111_DataLength
6535                         ; 1867 	Response = RESPONSE_OK;
6537  11c2 c601              	ldab	#1
6538  11c4 7b0000            	stab	L511_Response
6539                         ; 1868 	diagF_DiagResponse(Response, DataLength, pMsgContext);
6541  11c7 ec80              	ldd	OFST+0,s
6542  11c9 3b                	pshd	
6543  11ca cc000d            	ldd	#13
6544  11cd 3b                	pshd	
6545  11ce c601              	ldab	#1
6546  11d0 4a000000          	call	f_diagF_DiagResponse
6548  11d4 1b86              	leas	6,s
6549                         ; 1869 }
6552  11d6 0a                	rtc	
6595                         ; 1876 void DESC_API_CALLBACK_TYPE ApplDescReadActiveDiagnosticSessionDataIdentifier(DescMsgContext* pMsgContext)
6595                         ; 1877 {
6596                         	switch	.ftext
6597  11d7                   f_ApplDescReadActiveDiagnosticSessionDataIdentifier:
6599  11d7 3b                	pshd	
6600       00000000          OFST:	set	0
6603                         ; 1881 	pMsgContext->resData[0] = DescGetSessionIdOfSessionState(DescGetStateSession());;
6605  11d8 4a000000          	call	f_DescGetStateSession
6607  11dc 4a000000          	call	f_DescGetSessionIdOfSessionState
6609  11e0 ed80              	ldy	OFST+0,s
6610  11e2 6beb0004          	stab	[4,y]
6611                         ; 1882 	DataLength = 1;
6614  11e6 cc0001            	ldd	#1
6615  11e9 7c0002            	std	L111_DataLength
6616                         ; 1883 	Response = RESPONSE_OK;
6618  11ec 7b0000            	stab	L511_Response
6619                         ; 1884 	diagF_DiagResponse(Response, DataLength, pMsgContext);
6621  11ef 35                	pshy	
6622  11f0 3b                	pshd	
6623  11f1 4a000000          	call	f_diagF_DiagResponse
6625  11f5 1b86              	leas	6,s
6626                         ; 1885 }
6629  11f7 0a                	rtc	
6671                         ; 1892 void DESC_API_CALLBACK_TYPE ApplDescReadVehicleManufacturerSparePartNumber(DescMsgContext* pMsgContext)
6671                         ; 1893 {
6672                         	switch	.ftext
6673  11f8                   f_ApplDescReadVehicleManufacturerSparePartNumber:
6675  11f8 3b                	pshd	
6676       00000000          OFST:	set	0
6679                         ; 1897 	EE_BlockRead(EE_FBL_SPARE_PNO, &pMsgContext->resData[0]); //spare part number
6681  11f9 b746              	tfr	d,y
6682  11fb ec44              	ldd	4,y
6683  11fd 3b                	pshd	
6684  11fe cc003d            	ldd	#61
6685  1201 4a000000          	call	f_EE_BlockRead
6687  1205 1b82              	leas	2,s
6688                         ; 1898 	DataLength = 11;
6690  1207 cc000b            	ldd	#11
6691  120a 7c0002            	std	L111_DataLength
6692                         ; 1899 	Response = RESPONSE_OK;
6694  120d c601              	ldab	#1
6695  120f 7b0000            	stab	L511_Response
6696                         ; 1900 	diagF_DiagResponse(Response, DataLength, pMsgContext);
6698  1212 ec80              	ldd	OFST+0,s
6699  1214 3b                	pshd	
6700  1215 cc000b            	ldd	#11
6701  1218 3b                	pshd	
6702  1219 c601              	ldab	#1
6703  121b 4a000000          	call	f_diagF_DiagResponse
6705  121f 1b86              	leas	6,s
6706                         ; 1901 }
6709  1221 0a                	rtc	
6750                         ; 1908 void DESC_API_CALLBACK_TYPE ApplDescReadEcuSerialNumber(DescMsgContext* pMsgContext)
6750                         ; 1909 {
6751                         	switch	.ftext
6752  1222                   f_ApplDescReadEcuSerialNumber:
6754  1222 3b                	pshd	
6755       00000000          OFST:	set	0
6758                         ; 1913 	EE_BlockRead(EE_SERIAL_NUMBER, &pMsgContext->resData[0]);
6760  1223 b746              	tfr	d,y
6761  1225 ec44              	ldd	4,y
6762  1227 3b                	pshd	
6763  1228 cc0047            	ldd	#71
6764  122b 4a000000          	call	f_EE_BlockRead
6766  122f 1b82              	leas	2,s
6767                         ; 1915 	DataLength = 14;
6769  1231 cc000e            	ldd	#14
6770  1234 7c0002            	std	L111_DataLength
6771                         ; 1916 	Response = RESPONSE_OK;
6773  1237 c601              	ldab	#1
6774  1239 7b0000            	stab	L511_Response
6775                         ; 1917 	diagF_DiagResponse(Response, DataLength, pMsgContext);
6777  123c ec80              	ldd	OFST+0,s
6778  123e 3b                	pshd	
6779  123f cc000e            	ldd	#14
6780  1242 3b                	pshd	
6781  1243 c601              	ldab	#1
6782  1245 4a000000          	call	f_diagF_DiagResponse
6784  1249 1b86              	leas	6,s
6785                         ; 1918 }
6788  124b 0a                	rtc	
6828                         ; 1924 void DESC_API_CALLBACK_TYPE ApplDescReadVIN(DescMsgContext* pMsgContext)
6828                         ; 1925 {
6829                         	switch	.ftext
6830  124c                   f_ApplDescReadVIN:
6832  124c 3b                	pshd	
6833       00000000          OFST:	set	0
6836                         ; 1931 	EE_BlockRead(EE_VIN_ORIGINAL, &pMsgContext->resData[0]);
6838  124d b746              	tfr	d,y
6839  124f ec44              	ldd	4,y
6840  1251 3b                	pshd	
6841  1252 cc0048            	ldd	#72
6842  1255 4a000000          	call	f_EE_BlockRead
6844  1259 1b82              	leas	2,s
6845                         ; 1933 	DataLength = 17;
6847  125b cc0011            	ldd	#17
6848  125e 7c0002            	std	L111_DataLength
6849                         ; 1934 	Response = RESPONSE_OK;
6851  1261 c601              	ldab	#1
6852  1263 7b0000            	stab	L511_Response
6853                         ; 1935 	diagF_DiagResponse(Response, DataLength, pMsgContext);
6855  1266 ec80              	ldd	OFST+0,s
6856  1268 3b                	pshd	
6857  1269 cc0011            	ldd	#17
6858  126c 3b                	pshd	
6859  126d c601              	ldab	#1
6860  126f 4a000000          	call	f_diagF_DiagResponse
6862  1273 1b86              	leas	6,s
6863                         ; 1936 }
6866  1275 0a                	rtc	
6908                         ; 1942 void DESC_API_CALLBACK_TYPE ApplDescReadSystemSupplierEcuHardwareNumber(DescMsgContext* pMsgContext)
6908                         ; 1943 {
6909                         	switch	.ftext
6910  1276                   f_ApplDescReadSystemSupplierEcuHardwareNumber:
6912  1276 3b                	pshd	
6913       00000000          OFST:	set	0
6916                         ; 1950 	EE_BlockRead(EE_FBL_FIAT_ECU_HW_NO, &pMsgContext->resData[0]);
6918  1277 b746              	tfr	d,y
6919  1279 ec44              	ldd	4,y
6920  127b 3b                	pshd	
6921  127c cc003e            	ldd	#62
6922  127f 4a000000          	call	f_EE_BlockRead
6924  1283 1b82              	leas	2,s
6925                         ; 1952 	DataLength = 11;
6927  1285 cc000b            	ldd	#11
6928  1288 7c0002            	std	L111_DataLength
6929                         ; 1953 	Response = RESPONSE_OK;
6931  128b c601              	ldab	#1
6932  128d 7b0000            	stab	L511_Response
6933                         ; 1954 	diagF_DiagResponse(Response, DataLength, pMsgContext);
6935  1290 ec80              	ldd	OFST+0,s
6936  1292 3b                	pshd	
6937  1293 cc000b            	ldd	#11
6938  1296 3b                	pshd	
6939  1297 c601              	ldab	#1
6940  1299 4a000000          	call	f_diagF_DiagResponse
6942  129d 1b86              	leas	6,s
6943                         ; 1955 }
6946  129f 0a                	rtc	
6988                         ; 1961 void DESC_API_CALLBACK_TYPE ApplDescReadSystemSupplierEcuHardwareVersionNumber(DescMsgContext* pMsgContext)
6988                         ; 1962 {
6989                         	switch	.ftext
6990  12a0                   f_ApplDescReadSystemSupplierEcuHardwareVersionNumber:
6992  12a0 3b                	pshd	
6993       00000000          OFST:	set	0
6996                         ; 1968 	EE_BlockRead(EE_FBL_FIAT_ECU_HW_VERSION, &pMsgContext->resData[0]);
6998  12a1 b746              	tfr	d,y
6999  12a3 ec44              	ldd	4,y
7000  12a5 3b                	pshd	
7001  12a6 cc003f            	ldd	#63
7002  12a9 4a000000          	call	f_EE_BlockRead
7004  12ad 1b82              	leas	2,s
7005                         ; 1969 	DataLength = 1;
7007  12af cc0001            	ldd	#1
7008  12b2 7c0002            	std	L111_DataLength
7009                         ; 1970 	Response = RESPONSE_OK;
7011  12b5 7b0000            	stab	L511_Response
7012                         ; 1971 	diagF_DiagResponse(Response, DataLength, pMsgContext);
7014  12b8 ec80              	ldd	OFST+0,s
7015  12ba 3b                	pshd	
7016  12bb cc0001            	ldd	#1
7017  12be 3b                	pshd	
7018  12bf 4a000000          	call	f_diagF_DiagResponse
7020  12c3 1b86              	leas	6,s
7021                         ; 1972 }
7024  12c5 0a                	rtc	
7066                         ; 1978 void DESC_API_CALLBACK_TYPE ApplDescReadSystemSupplierEcuSoftwareNumber(DescMsgContext* pMsgContext)
7066                         ; 1979 {
7067                         	switch	.ftext
7068  12c6                   f_ApplDescReadSystemSupplierEcuSoftwareNumber:
7070  12c6 3b                	pshd	
7071       00000000          OFST:	set	0
7074                         ; 1985 	EE_BlockRead(EE_FBL_FIAT_ECU_SW_NO, &pMsgContext->resData[0]);
7076  12c7 b746              	tfr	d,y
7077  12c9 ec44              	ldd	4,y
7078  12cb 3b                	pshd	
7079  12cc cc0040            	ldd	#64
7080  12cf 4a000000          	call	f_EE_BlockRead
7082  12d3 1b82              	leas	2,s
7083                         ; 1987 	DataLength = 11;
7085  12d5 cc000b            	ldd	#11
7086  12d8 7c0002            	std	L111_DataLength
7087                         ; 1988 	Response = RESPONSE_OK;
7089  12db c601              	ldab	#1
7090  12dd 7b0000            	stab	L511_Response
7091                         ; 1989 	diagF_DiagResponse(Response, DataLength, pMsgContext);
7093  12e0 ec80              	ldd	OFST+0,s
7094  12e2 3b                	pshd	
7095  12e3 cc000b            	ldd	#11
7096  12e6 3b                	pshd	
7097  12e7 c601              	ldab	#1
7098  12e9 4a000000          	call	f_diagF_DiagResponse
7100  12ed 1b86              	leas	6,s
7101                         ; 1990 }
7104  12ef 0a                	rtc	
7146                         ; 1997 void DESC_API_CALLBACK_TYPE ApplDescReadSystemSupplierEcuSoftwareVersionNumber(DescMsgContext* pMsgContext)
7146                         ; 1998 {
7147                         	switch	.ftext
7148  12f0                   f_ApplDescReadSystemSupplierEcuSoftwareVersionNumber:
7150  12f0 3b                	pshd	
7151       00000000          OFST:	set	0
7154                         ; 2004 	EE_BlockRead(EE_FBL_FIAT_ECU_SW_VERSION, &pMsgContext->resData[0]);
7156  12f1 b746              	tfr	d,y
7157  12f3 ec44              	ldd	4,y
7158  12f5 3b                	pshd	
7159  12f6 cc0041            	ldd	#65
7160  12f9 4a000000          	call	f_EE_BlockRead
7162  12fd 1b82              	leas	2,s
7163                         ; 2006 	DataLength = 2;
7165  12ff cc0002            	ldd	#2
7166  1302 7c0002            	std	L111_DataLength
7167                         ; 2007 	Response = RESPONSE_OK;
7169  1305 53                	decb	
7170  1306 7b0000            	stab	L511_Response
7171                         ; 2008 	diagF_DiagResponse(Response, DataLength, pMsgContext);
7173  1309 ec80              	ldd	OFST+0,s
7174  130b 3b                	pshd	
7175  130c cc0002            	ldd	#2
7176  130f 3b                	pshd	
7177  1310 53                	decb	
7178  1311 4a000000          	call	f_diagF_DiagResponse
7180  1315 1b86              	leas	6,s
7181                         ; 2009 }
7184  1317 0a                	rtc	
7225                         ; 2016 void DESC_API_CALLBACK_TYPE ApplDescReadExhaustRegulationOrTypeApprovalNumber(DescMsgContext* pMsgContext)
7225                         ; 2017 {
7226                         	switch	.ftext
7227  1318                   f_ApplDescReadExhaustRegulationOrTypeApprovalNumber:
7229  1318 3b                	pshd	
7230       00000000          OFST:	set	0
7233                         ; 2022 	pMsgContext->resData[0] = 0x20;
7235  1319 c620              	ldab	#32
7236  131b ed80              	ldy	OFST+0,s
7237  131d 6beb0004          	stab	[4,y]
7238                         ; 2023 	pMsgContext->resData[1] = 0x20;
7240  1321 ed44              	ldy	4,y
7241  1323 6b41              	stab	1,y
7242                         ; 2024 	pMsgContext->resData[2] = 0x20;
7244  1325 6b42              	stab	2,y
7245                         ; 2025 	pMsgContext->resData[3] = 0x20;
7247  1327 6b43              	stab	3,y
7248                         ; 2026 	pMsgContext->resData[4] = 0x20;
7250  1329 6b44              	stab	4,y
7251                         ; 2027 	pMsgContext->resData[5] = 0x20;
7253  132b 6b45              	stab	5,y
7254                         ; 2030 	DataLength = 6;
7256  132d cc0006            	ldd	#6
7257  1330 7c0002            	std	L111_DataLength
7258                         ; 2031 	Response = RESPONSE_OK;
7260  1333 c601              	ldab	#1
7261  1335 7b0000            	stab	L511_Response
7262                         ; 2032 	diagF_DiagResponse(Response, DataLength, pMsgContext);
7264  1338 ec80              	ldd	OFST+0,s
7265  133a 3b                	pshd	
7266  133b cc0006            	ldd	#6
7267  133e 3b                	pshd	
7268  133f c601              	ldab	#1
7269  1341 4a000000          	call	f_diagF_DiagResponse
7271  1345 1b86              	leas	6,s
7272                         ; 2033 }
7275  1347 0a                	rtc	
7316                         ; 2040 void DESC_API_CALLBACK_TYPE ApplDescReadIdentificationCode(DescMsgContext* pMsgContext)
7316                         ; 2041 {
7317                         	switch	.ftext
7318  1348                   f_ApplDescReadIdentificationCode:
7320  1348 3b                	pshd	
7321       00000000          OFST:	set	0
7324                         ; 2054 	EE_BlockRead(EE_FBL_SPARE_PNO, &pMsgContext->resData[0]); //spare part number
7326  1349 b746              	tfr	d,y
7327  134b ec44              	ldd	4,y
7328  134d 3b                	pshd	
7329  134e cc003d            	ldd	#61
7330  1351 4a000000          	call	f_EE_BlockRead
7332  1355 1b82              	leas	2,s
7333                         ; 2055 	EE_BlockRead(EE_VIN_ORIGINAL, &pMsgContext->resData[11]); //VIN Number
7335  1357 ed80              	ldy	OFST+0,s
7336  1359 ee44              	ldx	4,y
7337  135b 1a0b              	leax	11,x
7338  135d 34                	pshx	
7339  135e cc0048            	ldd	#72
7340  1361 4a000000          	call	f_EE_BlockRead
7342  1365 1b82              	leas	2,s
7343                         ; 2056 	EE_BlockRead(EE_FBL_FIAT_ECU_HW_NO, &pMsgContext->resData[28]); //HW Number
7345  1367 ed80              	ldy	OFST+0,s
7346  1369 ec44              	ldd	4,y
7347  136b c3001c            	addd	#28
7348  136e 3b                	pshd	
7349  136f cc003e            	ldd	#62
7350  1372 4a000000          	call	f_EE_BlockRead
7352  1376 1b82              	leas	2,s
7353                         ; 2057 	EE_BlockRead(EE_FBL_FIAT_ECU_HW_VERSION, &pMsgContext->resData[39]); //HW Version
7355  1378 ed80              	ldy	OFST+0,s
7356  137a ec44              	ldd	4,y
7357  137c c30027            	addd	#39
7358  137f 3b                	pshd	
7359  1380 cc003f            	ldd	#63
7360  1383 4a000000          	call	f_EE_BlockRead
7362  1387 1b82              	leas	2,s
7363                         ; 2058 	EE_BlockRead(EE_FBL_FIAT_ECU_SW_NO, &pMsgContext->resData[40]); //SW PArt Number
7365  1389 ed80              	ldy	OFST+0,s
7366  138b ec44              	ldd	4,y
7367  138d c30028            	addd	#40
7368  1390 3b                	pshd	
7369  1391 cc0040            	ldd	#64
7370  1394 4a000000          	call	f_EE_BlockRead
7372  1398 1b82              	leas	2,s
7373                         ; 2059 	EE_BlockRead(EE_FBL_FIAT_ECU_SW_VERSION, &pMsgContext->resData[51]); //SW Version
7375  139a ed80              	ldy	OFST+0,s
7376  139c ec44              	ldd	4,y
7377  139e c30033            	addd	#51
7378  13a1 3b                	pshd	
7379  13a2 cc0041            	ldd	#65
7380  13a5 4a000000          	call	f_EE_BlockRead
7382  13a9 1b82              	leas	2,s
7383                         ; 2060 	EE_BlockRead(EE_FBL_APPROVAL_NO, &pMsgContext->resData[53]); //Approval Number
7385  13ab ed80              	ldy	OFST+0,s
7386  13ad ec44              	ldd	4,y
7387  13af c30035            	addd	#53
7388  13b2 3b                	pshd	
7389  13b3 cc0042            	ldd	#66
7390  13b6 4a000000          	call	f_EE_BlockRead
7392  13ba 1b82              	leas	2,s
7393                         ; 2061 	pMsgContext->resData[59] = 0x20; //ISO Code Not supported by Program
7395  13bc c620              	ldab	#32
7396  13be ed80              	ldy	OFST+0,s
7397  13c0 ed44              	ldy	4,y
7398  13c2 6be83b            	stab	59,y
7399                         ; 2062 	pMsgContext->resData[60] = 0x20; //ISO Code Not supported by Program
7401  13c5 6be83c            	stab	60,y
7402                         ; 2063 	pMsgContext->resData[61] = 0x20; //ISO Code Not supported by Program
7404  13c8 6be83d            	stab	61,y
7405                         ; 2064 	pMsgContext->resData[62] = 0x20; //ISO Code Not supported by Program
7407  13cb 6be83e            	stab	62,y
7408                         ; 2065 	pMsgContext->resData[63] = 0x20; //ISO Code Not supported by Program
7410  13ce 6be83f            	stab	63,y
7411                         ; 2067 	DataLength = 64;
7413  13d1 cc0040            	ldd	#64
7414  13d4 7c0002            	std	L111_DataLength
7415                         ; 2068 	Response = RESPONSE_OK;
7417  13d7 c601              	ldab	#1
7418  13d9 7b0000            	stab	L511_Response
7419                         ; 2069 	diagF_DiagResponse(Response, DataLength, pMsgContext);			
7421  13dc ec80              	ldd	OFST+0,s
7422  13de 3b                	pshd	
7423  13df cc0040            	ldd	#64
7424  13e2 3b                	pshd	
7425  13e3 c601              	ldab	#1
7426  13e5 4a000000          	call	f_diagF_DiagResponse
7428  13e9 1b86              	leas	6,s
7429                         ; 2070 }
7432  13eb 0a                	rtc	
7472                         ; 2078 void DESC_API_CALLBACK_TYPE ApplDescReadSincomAndFactory(DescMsgContext* pMsgContext)
7472                         ; 2079 {
7473                         	switch	.ftext
7474  13ec                   f_ApplDescReadSincomAndFactory:
7476  13ec 3b                	pshd	
7477       00000000          OFST:	set	0
7480                         ; 2084 	pMsgContext->resData[0] = 0x20;
7482  13ed c620              	ldab	#32
7483  13ef ed80              	ldy	OFST+0,s
7484  13f1 6beb0004          	stab	[4,y]
7485                         ; 2085 	pMsgContext->resData[1] = 0x20;
7487  13f5 ed44              	ldy	4,y
7488  13f7 6b41              	stab	1,y
7489                         ; 2086 	pMsgContext->resData[2] = 0x20;
7491  13f9 6b42              	stab	2,y
7492                         ; 2087 	pMsgContext->resData[3] = 0x20;
7494  13fb 6b43              	stab	3,y
7495                         ; 2088 	pMsgContext->resData[4] = 0x20;
7497  13fd 6b44              	stab	4,y
7498                         ; 2089 	pMsgContext->resData[5] = 0x20;
7500  13ff 6b45              	stab	5,y
7501                         ; 2091 	DataLength = 6;
7503  1401 cc0006            	ldd	#6
7504  1404 7c0002            	std	L111_DataLength
7505                         ; 2092 	Response = RESPONSE_OK;
7507  1407 c601              	ldab	#1
7508  1409 7b0000            	stab	L511_Response
7509                         ; 2093 	diagF_DiagResponse(Response, DataLength, pMsgContext);	
7511  140c ec80              	ldd	OFST+0,s
7512  140e 3b                	pshd	
7513  140f cc0006            	ldd	#6
7514  1412 3b                	pshd	
7515  1413 c601              	ldab	#1
7516  1415 4a000000          	call	f_diagF_DiagResponse
7518  1419 1b86              	leas	6,s
7519                         ; 2094 }
7522  141b 0a                	rtc	
7562                         ; 2103 void DESC_API_CALLBACK_TYPE ApplDescReadISOCode(DescMsgContext* pMsgContext)
7562                         ; 2104 {
7563                         	switch	.ftext
7564  141c                   f_ApplDescReadISOCode:
7566  141c 3b                	pshd	
7567       00000000          OFST:	set	0
7570                         ; 2108 	pMsgContext->resData[0] = 0x20;
7572  141d c620              	ldab	#32
7573  141f ed80              	ldy	OFST+0,s
7574  1421 6beb0004          	stab	[4,y]
7575                         ; 2109 	pMsgContext->resData[1] = 0x20;
7577  1425 ed44              	ldy	4,y
7578  1427 6b41              	stab	1,y
7579                         ; 2110 	pMsgContext->resData[2] = 0x20;
7581  1429 6b42              	stab	2,y
7582                         ; 2111 	pMsgContext->resData[3] = 0x20;
7584  142b 6b43              	stab	3,y
7585                         ; 2112 	pMsgContext->resData[4] = 0x20;
7587  142d 6b44              	stab	4,y
7588                         ; 2114 	DataLength = 5;
7590  142f cc0005            	ldd	#5
7591  1432 7c0002            	std	L111_DataLength
7592                         ; 2115 	Response = RESPONSE_OK;
7594  1435 c601              	ldab	#1
7595  1437 7b0000            	stab	L511_Response
7596                         ; 2116 	diagF_DiagResponse(Response, DataLength, pMsgContext);	
7598  143a ec80              	ldd	OFST+0,s
7599  143c 3b                	pshd	
7600  143d cc0005            	ldd	#5
7601  1440 3b                	pshd	
7602  1441 c601              	ldab	#1
7603  1443 4a000000          	call	f_diagF_DiagResponse
7605  1447 1b86              	leas	6,s
7606                         ; 2117 }
7609  1449 0a                	rtc	
7808                         	xref	f_diagF_DiagResponse
7809                         	switch	.bss
7810  0000                   L511_Response:
7811  0000 00                	ds.b	1
7812  0001                   L311_temp_c:
7813  0001 00                	ds.b	1
7814  0002                   L111_DataLength:
7815  0002 0000              	ds.b	2
7816  0004                   _diag_PCB_tempStat_c:
7817  0004 00                	ds.b	1
7818                         	xdef	_diag_PCB_tempStat_c
7819  0005                   _diag_ldShd_Stat_c:
7820  0005 00                	ds.b	1
7821                         	xdef	_diag_ldShd_Stat_c
7822  0006                   _diag_srvc_22F10B_byte2:
7823  0006 00                	ds.b	1
7824                         	xdef	_diag_srvc_22F10B_byte2
7825  0007                   _diag_srvc_22F10B_byte1:
7826  0007 00                	ds.b	1
7827                         	xdef	_diag_srvc_22F10B_byte1
7828  0008                   _diag_srvc_22F10B_byte0:
7829  0008 00                	ds.b	1
7830                         	xdef	_diag_srvc_22F10B_byte0
7831  0009                   _diag_srvc_22027E_byte2:
7832  0009 00                	ds.b	1
7833                         	xdef	_diag_srvc_22027E_byte2
7834  000a                   _diag_srvc_22027E_byte1:
7835  000a 00                	ds.b	1
7836                         	xdef	_diag_srvc_22027E_byte1
7837                         	xref	_Dio_PortRead_Ptr
7838                         	xref	f_EE_BlockRead
7839                         	xref	_ROMTst_result
7840                         	xref	_ROMTst_checksum
7841                         	xref	f_BattVF_Get_Crt_Uint
7842                         	xref	f_BattVF_Get_LdShd_Mnt_Stat
7843                         	xref	_BattV_set
7844                         	xref	_BattV_cals
7845                         	xref	_intFlt_bytes
7846                         	xref	f_TempF_NtcAdStd_to_CanTemp
7847                         	xref	f_TempF_Get_PCB_NTC_Stat
7848                         	xref	_temperature_PCB_ADC
7849                         	xref	_temperature_NTC_supplyV
7850                         	xref	f_stWF_Get_Output_Stat
7851                         	xref	_stW_prms
7852                         	xref	_stW_veh_offset_c
7853                         	xref	_stW_cfg_c
7854                         	xref	_stW_temperature_c
7855                         	xref	_stW_Isense_c
7856                         	xref	_stW_Vsense_LS_c
7857                         	xref	_stW_Vsense_HS_c
7858                         	xref	f_vsF_Modify_StartUp_Tm
7859                         	xref	f_vsF_Get_Output_Stat
7860                         	xref	_Vs_flt_cal_prms
7861                         	xref	_Vs_cal_prms
7862                         	xref	_vs_Isense_c
7863                         	xref	_vs_Vsense_c
7864                         	xref	_hs_cal_prms
7865                         	xref	_NTC_AD_Readings_c
7866                         	xref	_CSWM_config_c
7867                         	xref	_hs_Isense_c
7868                         	xref	_hs_Vsense_c
7869                         	xref	_hs_output_status_c
7870                         	xref	_hs_rear_swpsd_stat_c
7871                         	xref	_hs_rear_swth_stat_c
7872                         	xref	_hs_seat_config_c
7873                         	xref	_hs_vehicle_line_c
7874                         	xref	_fmem_keyon_time_w
7875                         	xref	_fmem_lifetime_l
7876                         	xref	_canio_RemSt_configStat_c
7877                         	xref	_canio_RX_IgnRun_RemSt_c
7878                         	xref	_canio_RX_ExternalTemperature_c
7879                         	xref	_canio_LHD_RHD_c
7880                         	xref	_canio_RX_EngineSpeed_c
7881                         	xref	_canio_LOAD_SHED_c
7882                         	xref	_canio_RX_IGN_STATUS_c
7883                         	xref	_canio_RX_StW_TempSens_FltSts_c
7884                         	xref	_canio_RX_StW_TempSts_c
7885                         	xref	_canio_RX_HSW_RQ_TGW_c
7886                         	xref	_canio_hvac_vent_seat_request_c
7887                         	xref	_canio_hvac_heated_seat_request_c
7888                         	xref	_canio_RX_BatteryVoltageLevel_c
7889                         	xref	_canio_RX_TotalOdo_c
7890                         	xref	_canio_status2_flags_c
7891                         	xref	_ru_eep_PROXI_Cfg
7892  000b                   L12_diag_sec_FAA_c:
7893  000b 00                	ds.b	1
7894  000c                   L71_secMod1_var2_l:
7895  000c 00000000          	ds.b	4
7896  0010                   L51_secMod1_var1_l:
7897  0010 00000000          	ds.b	4
7898  0014                   L31_diag_10sec_timer_w:
7899  0014 0000              	ds.b	2
7900  0016                   L11_hsm_key_l:
7901  0016 00000000          	ds.b	4
7902  001a                   L7_tool_key_l:
7903  001a 00000000          	ds.b	4
7904  001e                   L5_diag_seed_generated_l:
7905  001e 00000000          	ds.b	4
7906  0022                   L3_diag_seed_l:
7907  0022 00000000          	ds.b	4
7908                         	xref	_diag_io_lock3_flags_c
7909                         	xref	_main_variant_select_c
7910                         	xref	_main_EngRpm_lowLmt_c
7911                         	xref	_hardware_proxi_mismatch_c
7912                         	xref	_main_SwReq_Rear_c
7913                         	xdef	f_ApplDescReadISOCode
7914                         	xdef	f_ApplDescReadSincomAndFactory
7915                         	xdef	f_ApplDescReadIdentificationCode
7916                         	xdef	f_ApplDescReadExhaustRegulationOrTypeApprovalNumber
7917                         	xdef	f_ApplDescReadSystemSupplierEcuSoftwareVersionNumber
7918                         	xdef	f_ApplDescReadSystemSupplierEcuSoftwareNumber
7919                         	xdef	f_ApplDescReadSystemSupplierEcuHardwareVersionNumber
7920                         	xdef	f_ApplDescReadSystemSupplierEcuHardwareNumber
7921                         	xdef	f_ApplDescReadVIN
7922                         	xdef	f_ApplDescReadEcuSerialNumber
7923                         	xdef	f_ApplDescReadVehicleManufacturerSparePartNumber
7924                         	xdef	f_ApplDescReadActiveDiagnosticSessionDataIdentifier
7925                         	xdef	f_ApplDescReadApplicationDataFingerprint
7926                         	xdef	f_ApplDescReadApplicationSoftwareFingerprint
7927                         	xdef	f_ApplDescReadBootSoftwareFingerprint
7928                         	xdef	f_ApplDescReadApplicationDataIdentification
7929                         	xdef	f_ApplDescReadApplicationSoftwareIdentification
7930                         	xdef	f_ApplDescReadBootSoftwareIdentification
7931                         	xdef	f_ApplDescRead_22F132_ECU_Part_Number
7932                         	xdef	f_ApplDescRead_22F122_Software_Part_Number
7933                         	xdef	f_ApplDescRead_22F112_Hardware_Part_Number
7934                         	xdef	f_ApplDescRead_22F10B_ECU_Configuration
7935                         	xdef	f_ApplDescRead_22F100_Active_Diagnostic_Information
7936                         	xdef	f_ApplDescReadReadWrite
7937                         	xdef	f_ApplDescRead_223001_Program_ECU_Data_at_Flash_Station_2
7938                         	xdef	f_ApplDescRead_223000_Program_ECU_Data_at_Flash_Station_1
7939                         	xdef	f_ApplDescRead_222810_Heated_Seat_NTC_Cutoff_Parameters_Rear_Seats
7940                         	xdef	f_ApplDescRead_22280F_Heated_Seat_Max_Timeout_Parameters_Rear_Seats
7941                         	xdef	f_ApplDescRead_22280E_Heated_Seat_PWM_Calibration_Parameters_Rear_Seats
7942                         	xdef	f_ApplDescRead_22280D_Load_Shed_Fault_Status
7943                         	xdef	f_ApplDescRead_22280C_Heated_Steering_Wheel_Min_Timeout_Parameters
7944                         	xdef	f_ApplDescRead_22280A_HeatVent_Auto_Activation_Temperature_Calibration_Paramet
7945                         	xdef	f_ApplDescRead_222809_Steering_Wheel_Temperature_Cut_off_Parameters
7946                         	xdef	f_ApplDescRead_222808_Heated_Seat_NTC_Cutoff_Parameters_Front_Seats
7947                         	xdef	f_ApplDescRead_222807_PCB_Over_Temperature_Condition_Status
7948                         	xdef	f_ApplDescRead_222806_Voltage_Level_OFF
7949                         	xdef	f_ApplDescRead_222805_Voltage_Level_ON
7950                         	xdef	f_ApplDescRead_222804_Heated_Steering_Wheel_Max_Timeout_Parameters
7951                         	xdef	f_ApplDescRead_222803_Heated_Seat_Max_Timeout_Parameters_Front_Seats
7952                         	xdef	f_ApplDescRead_222802_Heated_Steering_Wheel_PWM_Calibration_Parameters
7953                         	xdef	f_ApplDescRead_222801_Vented_Seat_PWM_Calibration_Parameters
7954                         	xdef	f_ApplDescRead_222800_Heated_Seat_PWM_Calibration_Parameters_Front_Seats
7955                         	xdef	f_ApplDescRead_222024_Received_PROXI_Information
7956                         	xdef	f_ApplDescRead_2E2023_System_Configuration_PROXI
7957                         	xdef	f_ApplDescReadProgrammingStatus_EEPROM_FLASH
7958                         	xdef	f_ApplDescReadKeyOn_Counter_Status_EEPROM
7959                         	xdef	f_ApplDescReadECU_Time_Stamps_from_KeyOn_first_DTC_detection_EEPROM
7960                         	xdef	f_ApplDescReadECU_Time_first_DTC_detection_EEPROM
7961                         	xdef	f_ApplDescReadKeyOn_counter_EEPROM
7962                         	xdef	f_ApplDescReadECU_time_stamps_from_KeyOn_EEPROM
7963                         	xdef	f_ApplDescReadECU_time_stamps_EEPROM
7964                         	xdef	f_ApplDescReadNumber_of_FlashRom_re_writings_F_ROM
7965                         	xdef	f_ApplDescReadOdometer_content_to_the_last_F_ROM_updating
7966                         	xdef	f_ApplDescReadOdometer
7967                         	xdef	f_ApplDescRead_22102A_Check_EOL_configuration_data
7968                         	xdef	f_ApplDescReadECU_time_stamps_from_KeyOn_RAM
7969                         	xdef	f_ApplDescReadECU_time_stamps_RAM
7970                         	xdef	f_ApplDescRead_220107_Vented_Seats_Voltage_Sense
7971                         	xdef	f_ApplDescRead_220106_HeatVentWheel_Current_Sense
7972                         	xdef	f_ApplDescRead_220105_Heated_Seat_NTC_Feedback_Sensor_Status
7973                         	xdef	f_ApplDescRead_220104_Conditions_to_activate_CSWM_outputs
7974                         	xdef	f_ApplDescRead_220103_Rear_Seat_Switch_Input_Status
7975                         	xdef	f_ApplDescRead_220102_Bussed_Input
7976                         	xdef	f_ApplDescRead_220101_Bussed_Outputs_Status
7977                         	xref	f_DescGetSessionIdOfSessionState
7978                         	xref	f_DescGetStateSession
7979                         	xref	_STATUS_B_ECM
7980                         	xref	_STATUS_C_CAN
8001                         	end
