   1                         ; C Compiler for S12XE [COSMIC Software].
   2                         ; Generator V4.7.8 - 27 Jul 2007
   3                         ; Optimizer V4.6.13 - 27 Jul 2007
 344                         ; 203 @far void diagF_Init(void)
 344                         ; 204 {
 345                         .ftext:	section	.text
 346  0000                   f_diagF_Init:
 350                         ; 205 	d_ResetReq_t = NO_RESET_REQUEST;
 352  0000 790019            	clr	_d_ResetReq_t
 353                         ; 207 	diagLF_SessionTimeout();
 355  0003 4a019191          	call	f_diagLF_SessionTimeout
 357                         ; 209 	diagSecF_Main_Init ();
 359  0007 4a000000          	call	f_diagSecF_Main_Init
 361                         ; 214 	DescInitPowerOn(kDescPowerOnInitParam);
 363  000b 87                	clra	
 364  000c c7                	clrb	
 365  000d 4a000000          	call	f_DescInitPowerOn
 367                         ; 218 }
 370  0011 0a                	rtc	
 438                         ; 236 void DESC_API_CALLBACK_TYPE ApplDescCheckSessionTransition(DESC_CONTEXT_FORMAL_PARAM_DEF_FIRST DescStateGroup newState, DescStateGroup formerState)
 438                         ; 237 {
 439                         	switch	.ftext
 440  0012                   f_ApplDescCheckSessionTransition:
 442  0012 3b                	pshd	
 443  0013 1b9c              	leas	-4,s
 444       00000004          OFST:	set	4
 447                         ; 239 	u_8Bit diag_PCBTemp_stat_c = 0;        // Stores the PCB temperature status
 449  0015 6980              	clr	OFST-4,s
 450                         ; 240 	u_8Bit diag_Cswm_actvtn_stat = 0;	   // Stored CSWM output activation status
 452  0017 6981              	clr	OFST-3,s
 453                         ; 241 	u_8Bit diag_PCB_overtemp_stat = 0;	   // stores PCB over temperature status
 455  0019 6982              	clr	OFST-2,s
 456                         ; 242 	u_8Bit temp_BootSW_verStatus = 0;	   // stores bootloader version status (valid or not valid)
 458  001b 6983              	clr	OFST-1,s
 459                         ; 245 	if (newState == kDescStateSessionProgramming)
 461  001d 8c0002            	cpd	#2
 462  0020 2610              	bne	L352
 463                         ; 249 		d_ResetReq_t = BOOTMODE_REQUEST;
 465  0022 c6aa              	ldab	#170
 466  0024 7b0019            	stab	_d_ResetReq_t
 467                         ; 251 		diag_session_active_b = 1;
 469  0027 1c003601          	bset	_diag_status_flags_c,1
 470                         ; 253 		DescSetNegResponse(kDescNrcResponsePending);
 472  002b cc0078            	ldd	#120
 473  002e 4a000000          	call	f_DescSetNegResponse
 475                         ; 254 		DescProcessingDone();
 479  0032                   L352:
 480                         ; 259 		DescSessionTransitionChecked(DESC_CONTEXT_FORMAL_PARAM_DEF_FIRST);
 483  0032 4a000000          	call	f_DescProcessingDone
 484                         ; 261 }
 487  0036 1b86              	leas	6,s
 488  0038 0a                	rtc	
 527                         ; 279 void DESC_API_CALLBACK_TYPE ApplDescOnTransitionSession(DescStateGroup newState, DescStateGroup formerState)
 527                         ; 280 {
 528                         	switch	.ftext
 529  0039                   f_ApplDescOnTransitionSession:
 533                         ; 281 	switch (newState) {
 536  0039 040409            	dbeq	d,L752
 537  003c 040413            	dbeq	d,L162
 538  003f 830002            	subd	#2
 539  0042 271f              	beq	L362
 541  0044 0a                	rtc	
 542  0045                   L752:
 543                         ; 285 			diag_session_active_b = 0;
 545  0045 1d003601          	bclr	_diag_status_flags_c,1
 546                         ; 287 			diagLF_SessionTimeout();
 548  0049 4a019191          	call	f_diagLF_SessionTimeout
 550                         ; 289 			diagSecF_DefSession_Init ();
 552  004d 4a000000          	call	f_diagSecF_DefSession_Init
 554                         ; 291 			break;
 557  0051 0a                	rtc	
 558  0052                   L162:
 559                         ; 296 			diag_session_active_b = 1;
 561  0052 1c003601          	bset	_diag_status_flags_c,1
 562                         ; 299 			diag_sec_access1_state_c = ECU_POWERUP_STATE;
 564  0056 790000            	clr	_diag_sec_access1_state_c
 565                         ; 300 			sec_acc_unlock_b = 0;
 567  0059 1d000001          	bclr	_diag_flags_c,1
 568                         ; 301 			d_ResetReq_t = BOOTMODE_REQUEST;
 570  005d c6aa              	ldab	#170
 571  005f 7b0019            	stab	_d_ResetReq_t
 572                         ; 302 			break;
 575  0062 0a                	rtc	
 576  0063                   L362:
 577                         ; 306 			diagSecF_ExtSession_Init ();
 579  0063 4a000000          	call	f_diagSecF_ExtSession_Init
 581                         ; 307 			break;
 583                         ; 314 }
 586  0067 0a                	rtc	
 840                         ; 321 void DESC_API_CALLBACK_TYPE ApplDescResetHardReset(DescMsgContext* pMsgContext)
 840                         ; 322 { 
 841                         	switch	.ftext
 842  0068                   f_ApplDescResetHardReset:
 844  0068 3b                	pshd	
 845  0069 3b                	pshd	
 846       00000002          OFST:	set	2
 849                         ; 327 	d_ResetReq_t = PREPARE_RESET_REQUEST;
 851  006a c655              	ldab	#85
 852  006c 7b0019            	stab	_d_ResetReq_t
 853                         ; 328   SubFunctionRequest = pMsgContext->reqData[0];
 855  006f edf30002          	ldy	[OFST+0,s]
 856  0073 e640              	ldab	0,y
 857  0075 87                	clra	
 858  0076 6c80              	std	OFST-2,s
 859                         ; 330   pMsgContext->resData[0] = 0x01;
 861  0078 c601              	ldab	#1
 862  007a ed82              	ldy	OFST+0,s
 863  007c 6beb0004          	stab	[4,y]
 864                         ; 332   pMsgContext->resDataLen = 1;
 866  0080 6c46              	std	6,y
 867                         ; 335   if(SubFunctionRequest & D_NO_POSITIV_RESPONSE_REQUIRED_K)
 869  0082 0e818003          	brset	OFST-1,s,128,L554
 871                         ; 342       Response = RESPONSE_OK;
 873  0086 7b0018            	stab	L702_Response
 874  0089                   L554:
 875                         ; 344   diagF_DiagResponse(Response, pMsgContext->reqDataLen, pMsgContext); 
 877  0089 35                	pshy	
 878  008a ec42              	ldd	2,y
 879  008c 3b                	pshd	
 880  008d f60018            	ldab	L702_Response
 881  0090 87                	clra	
 882  0091 4a00cccc          	call	f_diagF_DiagResponse
 884                         ; 345 }	
 887  0095 1b88              	leas	8,s
 888  0097 0a                	rtc	
 983                         ; 452 void DESC_API_CALLBACK_TYPE ApplDescCheckCommCtrl(DESC_CONTEXT_FORMAL_PARAM_DEF_FIRST DescOemCommControlInfo *commControlInfo)
 983                         ; 453 {
 984                         	switch	.ftext
 985  0098                   f_ApplDescCheckCommCtrl:
 989                         ; 499   DescCommCtrlChecked(DESC_CONTEXT_PARAM_ONLY);
 991  0098 4a000000          	call	f_DescProcessingDone
 993                         ; 500 }
 996  009c 0a                	rtc	
1020                         ; 506 void DESC_API_CALLBACK_TYPE ApplDescOnCommunicationDisable(void)
1020                         ; 507 {
1021                         	switch	.ftext
1022  009d                   f_ApplDescOnCommunicationDisable:
1026                         ; 508 	comn_ctrl_disable_b = TRUE;
1028  009d 1c003680          	bset	_diag_status_flags_c,128
1029                         ; 509 }
1032  00a1 0a                	rtc	
1056                         ; 515 void DESC_API_CALLBACK_TYPE ApplDescOnCommunicationEnable(void)
1056                         ; 516 {
1057                         	switch	.ftext
1058  00a2                   f_ApplDescOnCommunicationEnable:
1062                         ; 517 	comn_ctrl_disable_b = FALSE;
1064  00a2 1d003680          	bclr	_diag_status_flags_c,128
1065                         ; 518 }
1068  00a6 0a                	rtc	
1106                         ; 540 void DESC_API_CALLBACK_TYPE ApplDescFatalError(vuint8 errorCode, vuint16 lineNumber)
1106                         ; 541 {
1107                         	switch	.ftext
1108  00a7                   f_ApplDescFatalError:
1110       fffffffe          OFST:	set	-2
1113                         ; 544   DESC_IGNORE_UNREF_PARAM(errorCode);
1115                         ; 545   DESC_IGNORE_UNREF_PARAM(lineNumber);
1117  00a7                   L565:
1118                         ; 549   for(;;);
1120  00a7 20fe              	bra	L565
1152                         ; 566 void DESC_API_CALLBACK_TYPE ApplDescRcrRpConfirmation(vuint8 status)
1152                         ; 567 {
1153                         	switch	.ftext
1154  00a9                   f_ApplDescRcrRpConfirmation:
1158                         ; 573   if(status == kDescOk)
1161  00a9 d7                	tstb	
1163                         ; 586 }
1167  00aa 0a                	rtc	
1205                         ; 621 void DESC_API_CALLBACK_TYPE ApplDescRequestDownload(DescMsgContext* pMsgContext)
1205                         ; 622 {
1206                         	switch	.ftext
1207  00ab                   f_ApplDescRequestDownload:
1211                         ; 640   DescProcessingDone();
1213  00ab 4a000000          	call	f_DescProcessingDone
1215                         ; 641 }
1218  00af 0a                	rtc	
1256                         ; 677 void DESC_API_CALLBACK_TYPE ApplDescTransferDataDownload(DescMsgContext* pMsgContext)
1256                         ; 678 {
1257                         	switch	.ftext
1258  00b0                   f_ApplDescTransferDataDownload:
1262                         ; 705   DescProcessingDone();
1264  00b0 4a000000          	call	f_DescProcessingDone
1266                         ; 706 }
1269  00b4 0a                	rtc	
1307                         ; 720 void DESC_API_CALLBACK_TYPE ApplDescReqTransferExitDownload(DescMsgContext* pMsgContext)
1307                         ; 721 {
1308                         	switch	.ftext
1309  00b5                   f_ApplDescReqTransferExitDownload:
1313                         ; 726   DescProcessingDone();
1315  00b5 4a000000          	call	f_DescProcessingDone
1317                         ; 727 }
1320  00b9 0a                	rtc	
1358                         ; 762 void DESC_API_CALLBACK_TYPE ApplDescTester_PrresentTesterPresent(DescMsgContext* pMsgContext)
1358                         ; 763 {
1359                         	switch	.ftext
1360  00ba                   f_ApplDescTester_PrresentTesterPresent:
1364                         ; 768   DescProcessingDone();
1366  00ba 4a000000          	call	f_DescProcessingDone
1368                         ; 769 }
1371  00be 0a                	rtc	
1408                         ; 800 void DESC_API_CALLBACK_TYPE ApplDescSetCommMode(DescOemCommControlInfo *commControlInfo)
1408                         ; 801 {
1409                         	switch	.ftext
1410  00bf                   f_ApplDescSetCommMode:
1414                         ; 802   switch(commControlInfo->subNetNumber)
1416  00bf b746              	tfr	d,y
1417  00c1 e640              	ldab	0,y
1418  00c3 54                	lsrb	
1419  00c4 54                	lsrb	
1420  00c5 54                	lsrb	
1421  00c6 54                	lsrb	
1423  00c7 2702              	beq	L747
1424  00c9 c00f              	subb	#15
1425  00cb                   L747:
1426                         ; 830 }
1429  00cb 0a                	rtc	
1590                         ; 916 void diagF_DiagResponse(enum DIAG_RESPONSE Response, u_temp_l Length, DescMsgContext* pMsgContext)
1590                         ; 917 {
1591                         	switch	.ftext
1592  00cc                   f_diagF_DiagResponse:
1594       fffffffe          OFST:	set	-2
1597                         ; 918 	switch (Response) {
1600  00cc 87                	clra	
1601  00cd c10d              	cmpb	#13
1602  00cf 182400ac          	bhs	L3701
1603  00d3 59                	lsld	
1604  00d4 05ff              	jmp	[d,pc]
1605  00d6 00f0              	dc.w	L157
1606  00d8 00f1              	dc.w	L357
1607  00da 00fc              	dc.w	L557
1608  00dc 0108              	dc.w	L757
1609  00de 0114              	dc.w	L167
1610  00e0 0120              	dc.w	L367
1611  00e2 012c              	dc.w	L567
1612  00e4 0138              	dc.w	L767
1613  00e6 0144              	dc.w	L177
1614  00e8 0150              	dc.w	L377
1615  00ea 015c              	dc.w	L577
1616  00ec 0168              	dc.w	L777
1617  00ee 0174              	dc.w	L1001
1618  00f0                   L157:
1619                         ; 922 			break;
1622  00f0 0a                	rtc	
1623  00f1                   L357:
1624                         ; 927 			pMsgContext->resDataLen = Length;
1626  00f1 ed85              	ldy	OFST+7,s
1627  00f3 18028346          	movw	OFST+5,s,6,y
1628                         ; 928 			DescProcessingDone();
1630  00f7 4a000000          	call	f_DescProcessingDone
1632                         ; 929 			break;
1635  00fb 0a                	rtc	
1636  00fc                   L557:
1637                         ; 933 			DescSetNegResponse(kDescNrcResponsePending);
1639  00fc cc0078            	ldd	#120
1640  00ff 4a000000          	call	f_DescSetNegResponse
1642                         ; 934 			DescProcessingDone();
1644  0103 4a000000          	call	f_DescProcessingDone
1646                         ; 935 			break;
1649  0107 0a                	rtc	
1650  0108                   L757:
1651                         ; 939 			DescSetNegResponse(kDescNrcBusyRepeatRequest);
1653  0108 cc0021            	ldd	#33
1654  010b 4a000000          	call	f_DescSetNegResponse
1656                         ; 940 			DescProcessingDone();
1658  010f 4a000000          	call	f_DescProcessingDone
1660                         ; 941 			break;
1663  0113 0a                	rtc	
1664  0114                   L167:
1665                         ; 945 			DescSetNegResponse(kDescNrcRequestOutOfRange);
1667  0114 cc0031            	ldd	#49
1668  0117 4a000000          	call	f_DescSetNegResponse
1670                         ; 946 			DescProcessingDone();
1672  011b 4a000000          	call	f_DescProcessingDone
1674                         ; 947 			break;
1677  011f 0a                	rtc	
1678  0120                   L367:
1679                         ; 951 			DescSetNegResponse(kDescNrcInvalidFormat);
1681  0120 cc0013            	ldd	#19
1682  0123 4a000000          	call	f_DescSetNegResponse
1684                         ; 952 			DescProcessingDone();
1686  0127 4a000000          	call	f_DescProcessingDone
1688                         ; 953 			break;
1691  012b 0a                	rtc	
1692  012c                   L567:
1693                         ; 957 			DescSetNegResponse(kDescNrcConditionsNotCorrect);
1695  012c cc0022            	ldd	#34
1696  012f 4a000000          	call	f_DescSetNegResponse
1698                         ; 958 			DescProcessingDone();
1700  0133 4a000000          	call	f_DescProcessingDone
1702                         ; 959 			break;
1705  0137 0a                	rtc	
1706  0138                   L767:
1707                         ; 963 			DescSetNegResponse(kDescNrcSubfunctionNotSupported);
1709  0138 cc0012            	ldd	#18
1710  013b 4a000000          	call	f_DescSetNegResponse
1712                         ; 964 			DescProcessingDone();
1714  013f 4a000000          	call	f_DescProcessingDone
1716                         ; 965 			break;
1719  0143 0a                	rtc	
1720  0144                   L177:
1721                         ; 969 			DescSetNegResponse(kDescNrcAccessDenied);
1723  0144 cc0033            	ldd	#51
1724  0147 4a000000          	call	f_DescSetNegResponse
1726                         ; 970 			DescProcessingDone();
1728  014b 4a000000          	call	f_DescProcessingDone
1730                         ; 971 			break;
1733  014f 0a                	rtc	
1734  0150                   L377:
1735                         ; 975 			DescSetNegResponse(kDescNrcInvalidKey);
1737  0150 cc0035            	ldd	#53
1738  0153 4a000000          	call	f_DescSetNegResponse
1740                         ; 976 			DescProcessingDone();
1742  0157 4a000000          	call	f_DescProcessingDone
1744                         ; 977 			break;
1747  015b 0a                	rtc	
1748  015c                   L577:
1749                         ; 981 			DescSetNegResponse(kDescNrcExceedNumOfAttempts);
1751  015c cc0036            	ldd	#54
1752  015f 4a000000          	call	f_DescSetNegResponse
1754                         ; 982 			DescProcessingDone();
1756  0163 4a000000          	call	f_DescProcessingDone
1758                         ; 983 			break;
1761  0167 0a                	rtc	
1762  0168                   L777:
1763                         ; 987 			DescSetNegResponse(kDescNrcTimeDelayNotExpired);
1765  0168 cc0037            	ldd	#55
1766  016b 4a000000          	call	f_DescSetNegResponse
1768                         ; 988 			DescProcessingDone();
1770  016f 4a000000          	call	f_DescProcessingDone
1772                         ; 989 			break;
1775  0173 0a                	rtc	
1776  0174                   L1001:
1777                         ; 993 			DescSetNegResponse(kDescNrcRequestSequenceError);
1779  0174 cc0024            	ldd	#36
1780  0177 4a000000          	call	f_DescSetNegResponse
1782                         ; 994 			DescProcessingDone();
1784  017b 4a000000          	call	f_DescProcessingDone
1786                         ; 995 			break;
1788  017f                   L3701:
1789                         ; 1001 }
1792  017f 0a                	rtc	
1819                         ; 1013 @far void diagF_CyclicWriteTask(void)
1819                         ; 1014 {
1820                         	switch	.ftext
1821  0180                   f_diagF_CyclicWriteTask:
1825                         ; 1016 	diagWriteF_CyclicTask ();
1827  0180 4a000000          	call	f_diagWriteF_CyclicTask
1829                         ; 1018 	diagLF_CheckResetRequest();
1831  0184 4a044e4e          	call	f_diagLF_CheckResetRequest
1833                         ; 1020 	diagSecF_CyclicTask ();
1835  0188 4a000000          	call	f_diagSecF_CyclicTask
1837                         ; 1023 	diagLF_PartNo_SwVer_Update();
1839  018c 4a01f1f1          	call	f_diagLF_PartNo_SwVer_Update
1841                         ; 1032 }
1844  0190 0a                	rtc	
1846                         	xref	_diag_DCX_Test_cntr_w
1895                         ; 1042 void diagLF_SessionTimeout(void)
1895                         ; 1043 {
1896                         	switch	.ftext
1897  0191                   f_diagLF_SessionTimeout:
1899  0191 37                	pshb	
1900       00000001          OFST:	set	1
1903                         ; 1048 	d_DtcSettingModeEnabled_t = TRUE;
1905  0192 c601              	ldab	#1
1906  0194 7b0000            	stab	_d_DtcSettingModeEnabled_t
1907                         ; 1052 	for (i=0;i<4;i++)
1909  0197 c7                	clrb	
1910  0198 6b80              	stab	OFST-1,s
1911  019a                   L1211:
1912                         ; 1054 		diag_io_hs_lock_flags_c[i].cb = 0;
1914  019a b796              	exg	b,y
1915  019c 69ea0000          	clr	_diag_io_hs_lock_flags_c,y
1916                         ; 1056 		diag_io_hs_rq_c[i] = 0;
1918  01a0 69ea0000          	clr	_diag_io_hs_rq_c,y
1919                         ; 1057 		diag_io_hs_state_c[i] = 0;
1921  01a4 69ea0000          	clr	_diag_io_hs_state_c,y
1922                         ; 1052 	for (i=0;i<4;i++)
1924  01a8 6280              	inc	OFST-1,s
1927  01aa e680              	ldab	OFST-1,s
1928  01ac c104              	cmpb	#4
1929  01ae 25ea              	blo	L1211
1930                         ; 1060 	for (i=0;i<2;i++)
1932  01b0 c7                	clrb	
1933  01b1 6b80              	stab	OFST-1,s
1934  01b3                   L7211:
1935                         ; 1062 		diag_io_vs_lock_flags_c[i].cb = 0;
1937  01b3 b796              	exg	b,y
1938  01b5 69ea0000          	clr	_diag_io_vs_lock_flags_c,y
1939                         ; 1064 		diag_io_vs_rq_c [i] = 0;
1941  01b9 69ea0000          	clr	_diag_io_vs_rq_c,y
1942                         ; 1065 		diag_io_vs_state_c [i] = 0;
1944  01bd 69ea0000          	clr	_diag_io_vs_state_c,y
1945                         ; 1060 	for (i=0;i<2;i++)
1947  01c1 6280              	inc	OFST-1,s
1950  01c3 e680              	ldab	OFST-1,s
1951  01c5 c102              	cmpb	#2
1952  01c7 25ea              	blo	L7211
1953                         ; 1068 	diag_io_lock_flags_c.cb = 0;
1955  01c9 790000            	clr	_diag_io_lock_flags_c
1956                         ; 1069 	diag_io_lock2_flags_c.cb = 0;
1958  01cc 790000            	clr	_diag_io_lock2_flags_c
1959                         ; 1070 	diag_io_lock3_flags_c.cb = 0;
1961  01cf 790000            	clr	_diag_io_lock3_flags_c
1962                         ; 1073 	diag_io_load_shed_c = 0;
1964  01d2 790000            	clr	_diag_io_load_shed_c
1965                         ; 1074 	diag_io_ign_state_c = 0;
1967  01d5 790000            	clr	_diag_io_ign_state_c
1968                         ; 1075 	diag_io_vs_all_out_c = 0;
1970  01d8 790000            	clr	_diag_io_vs_all_out_c
1971                         ; 1076 	diag_io_st_whl_rq_c = 0;
1973  01db 790000            	clr	_diag_io_st_whl_rq_c
1974                         ; 1077 	diag_io_st_whl_state_c = 0;
1976  01de 790000            	clr	_diag_io_st_whl_state_c
1977                         ; 1080 	diag_rc_lock_flags_c.cb = 0;
1979  01e1 790035            	clr	_diag_rc_lock_flags_c
1980                         ; 1081 	diag_op_rc_status_c = 0;
1982  01e4 790000            	clr	_diag_op_rc_status_c
1983                         ; 1082 	diag_op_rc_preCondition_stat_c = 0;
1985  01e7 790000            	clr	_diag_op_rc_preCondition_stat_c
1986                         ; 1083 	diag_DCX_Test_cntr_w = 0;
1988  01ea 18790000          	clrw	_diag_DCX_Test_cntr_w
1989                         ; 1085 }
1992  01ee 1b81              	leas	1,s
1993  01f0 0a                	rtc	
2044                         ; 1096 void diagLF_PartNo_SwVer_Update(void)
2044                         ; 1097 {
2045                         	switch	.ftext
2046  01f1                   f_diagLF_PartNo_SwVer_Update:
2048  01f1 1b93              	leas	-13,s
2049       0000000d          OFST:	set	13
2052                         ; 1120 	switch (main_variant_select_c /*CSWM_config_c*/){
2054  01f3 f60000            	ldab	_main_variant_select_c
2056  01f6 87                	clra	
2057  01f7 c106              	cmpb	#6
2058  01f9 182400fb          	bhs	L1511
2059  01fd 59                	lsld	
2060  01fe 05ff              	jmp	[d,pc]
2061  0200 020c              	dc.w	L5311
2062  0202 027e              	dc.w	L3411
2063  0204 025a              	dc.w	L1411
2064  0206 02c8              	dc.w	L7411
2065  0208 0233              	dc.w	L7311
2066  020a 02a3              	dc.w	L5411
2067  020c                   L5311:
2068                         ; 1130 			diag_def_ecu_pno[0]  = 0x35;
2070  020c c635              	ldab	#53
2071  020e 7b000a            	stab	_diag_def_ecu_pno
2072                         ; 1131 			diag_def_ecu_pno[1]  = 0x36;
2074  0211 52                	incb	
2075  0212 7b000b            	stab	_diag_def_ecu_pno+1
2076                         ; 1132 			diag_def_ecu_pno[2]  = 0x30;
2078  0215 c630              	ldab	#48
2079  0217 7b000c            	stab	_diag_def_ecu_pno+2
2080                         ; 1133 			diag_def_ecu_pno[3]  = 0x34;
2082  021a c634              	ldab	#52
2083  021c 7b000d            	stab	_diag_def_ecu_pno+3
2084                         ; 1134 			diag_def_ecu_pno[4]  = 0x36;
2086  021f c636              	ldab	#54
2087  0221 7b000e            	stab	_diag_def_ecu_pno+4
2088                         ; 1135 			diag_def_ecu_pno[5]  = 0x35;
2090  0224 53                	decb	
2091  0225 7b000f            	stab	_diag_def_ecu_pno+5
2092                         ; 1136 			diag_def_ecu_pno[6]  = 0x39;
2094  0228 c639              	ldab	#57
2095  022a 7b0010            	stab	_diag_def_ecu_pno+6
2096                         ; 1137 			diag_def_ecu_pno[7]  = 0x32;
2098  022d c632              	ldab	#50
2099                         ; 1138 			diag_def_ecu_pno[8]  = 0x41;
2101                         ; 1139 			diag_def_ecu_pno[9]  = 0x45;
2103                         ; 1140 			break;
2105  022f 182000b9          	bra	LC001
2106  0233                   L7311:
2107                         ; 1152 			diag_def_ecu_pno[0]  = 0x35;
2109  0233 c635              	ldab	#53
2110  0235 7b000a            	stab	_diag_def_ecu_pno
2111                         ; 1153 			diag_def_ecu_pno[1]  = 0x36;
2113  0238 52                	incb	
2114  0239 7b000b            	stab	_diag_def_ecu_pno+1
2115                         ; 1154 			diag_def_ecu_pno[2]  = 0x30;
2117  023c c630              	ldab	#48
2118  023e 7b000c            	stab	_diag_def_ecu_pno+2
2119                         ; 1155 			diag_def_ecu_pno[3]  = 0x34;
2121  0241 c634              	ldab	#52
2122  0243 7b000d            	stab	_diag_def_ecu_pno+3
2123                         ; 1156 			diag_def_ecu_pno[4]  = 0x36;
2125  0246 c636              	ldab	#54
2126  0248 7b000e            	stab	_diag_def_ecu_pno+4
2127                         ; 1157 			diag_def_ecu_pno[5]  = 0x35;
2129  024b 53                	decb	
2130  024c 7b000f            	stab	_diag_def_ecu_pno+5
2131                         ; 1158 			diag_def_ecu_pno[6]  = 0x39;
2133  024f c639              	ldab	#57
2134  0251 7b0010            	stab	_diag_def_ecu_pno+6
2135                         ; 1159 			diag_def_ecu_pno[7]  = 0x33;
2137  0254 c633              	ldab	#51
2138                         ; 1160 			diag_def_ecu_pno[8]  = 0x41;
2140                         ; 1161 			diag_def_ecu_pno[9]  = 0x45;
2142                         ; 1162 			break;
2144  0256 18200092          	bra	LC001
2145  025a                   L1411:
2146                         ; 1174 			diag_def_ecu_pno[0]  = 0x36;
2148  025a c636              	ldab	#54
2149  025c 7b000a            	stab	_diag_def_ecu_pno
2150                         ; 1175 			diag_def_ecu_pno[1]  = 0x38;
2152  025f c638              	ldab	#56
2153  0261 7b000b            	stab	_diag_def_ecu_pno+1
2154                         ; 1176 			diag_def_ecu_pno[2]  = 0x31;
2156  0264 c631              	ldab	#49
2157  0266 7b000c            	stab	_diag_def_ecu_pno+2
2158                         ; 1177 			diag_def_ecu_pno[3]  = 0x33;
2160  0269 c633              	ldab	#51
2161  026b 7b000d            	stab	_diag_def_ecu_pno+3
2162                         ; 1178 			diag_def_ecu_pno[4]  = 0x37;
2164  026e c637              	ldab	#55
2165  0270 7b000e            	stab	_diag_def_ecu_pno+4
2166                         ; 1179 			diag_def_ecu_pno[5]  = 0x35;
2168  0273 c635              	ldab	#53
2169  0275 7b000f            	stab	_diag_def_ecu_pno+5
2170                         ; 1180 			diag_def_ecu_pno[6]  = 0x34;
2172  0278 53                	decb	
2173  0279 7b0010            	stab	_diag_def_ecu_pno+6
2174                         ; 1181 			diag_def_ecu_pno[7]  = 0x34;
2176                         ; 1182 			diag_def_ecu_pno[8]  = 0x41;
2178                         ; 1183 			diag_def_ecu_pno[9]  = 0x45;
2180                         ; 1184 			break;
2182  027c 206e              	bra	LC001
2183  027e                   L3411:
2184                         ; 1196 			diag_def_ecu_pno[0]  = 0x36;
2186  027e c636              	ldab	#54
2187  0280 7b000a            	stab	_diag_def_ecu_pno
2188                         ; 1197 			diag_def_ecu_pno[1]  = 0x38;
2190  0283 c638              	ldab	#56
2191  0285 7b000b            	stab	_diag_def_ecu_pno+1
2192                         ; 1198 			diag_def_ecu_pno[2]  = 0x31;
2194  0288 c631              	ldab	#49
2195  028a 7b000c            	stab	_diag_def_ecu_pno+2
2196                         ; 1199 			diag_def_ecu_pno[3]  = 0x33;
2198  028d c633              	ldab	#51
2199  028f 7b000d            	stab	_diag_def_ecu_pno+3
2200                         ; 1200 			diag_def_ecu_pno[4]  = 0x37;
2202  0292 c637              	ldab	#55
2203  0294 7b000e            	stab	_diag_def_ecu_pno+4
2204                         ; 1201 			diag_def_ecu_pno[5]  = 0x35;
2206  0297 c635              	ldab	#53
2207  0299 7b000f            	stab	_diag_def_ecu_pno+5
2208                         ; 1202 			diag_def_ecu_pno[6]  = 0x34;
2210  029c 53                	decb	
2211  029d 7b0010            	stab	_diag_def_ecu_pno+6
2212                         ; 1203 			diag_def_ecu_pno[7]  = 0x35;
2214  02a0 52                	incb	
2215                         ; 1204 			diag_def_ecu_pno[8]  = 0x41;
2217                         ; 1205 			diag_def_ecu_pno[9]  = 0x45;
2219                         ; 1206 			break;
2221  02a1 2049              	bra	LC001
2222  02a3                   L5411:
2223                         ; 1218 			diag_def_ecu_pno[0]  = 0x35;
2225  02a3 c635              	ldab	#53
2226  02a5 7b000a            	stab	_diag_def_ecu_pno
2227                         ; 1219 			diag_def_ecu_pno[1]  = 0x36;
2229  02a8 52                	incb	
2230  02a9 7b000b            	stab	_diag_def_ecu_pno+1
2231                         ; 1220 			diag_def_ecu_pno[2]  = 0x30;
2233  02ac c630              	ldab	#48
2234  02ae 7b000c            	stab	_diag_def_ecu_pno+2
2235                         ; 1221 			diag_def_ecu_pno[3]  = 0x34;
2237  02b1 c634              	ldab	#52
2238  02b3 7b000d            	stab	_diag_def_ecu_pno+3
2239                         ; 1222 			diag_def_ecu_pno[4]  = 0x36;
2241  02b6 c636              	ldab	#54
2242  02b8 7b000e            	stab	_diag_def_ecu_pno+4
2243                         ; 1223 			diag_def_ecu_pno[5]  = 0x35;
2245  02bb 53                	decb	
2246  02bc 7b000f            	stab	_diag_def_ecu_pno+5
2247                         ; 1224 			diag_def_ecu_pno[6]  = 0x39;
2249  02bf c639              	ldab	#57
2250  02c1 7b0010            	stab	_diag_def_ecu_pno+6
2251                         ; 1225 			diag_def_ecu_pno[7]  = 0x35;
2253  02c4 c635              	ldab	#53
2254                         ; 1226 			diag_def_ecu_pno[8]  = 0x41;
2256                         ; 1227 			diag_def_ecu_pno[9]  = 0x45;
2258                         ; 1228 			break;
2260  02c6 2024              	bra	LC001
2261  02c8                   L7411:
2262                         ; 1240 			diag_def_ecu_pno[0]  = 0x36;
2264  02c8 c636              	ldab	#54
2265  02ca 7b000a            	stab	_diag_def_ecu_pno
2266                         ; 1241 			diag_def_ecu_pno[1]  = 0x38;
2268  02cd c638              	ldab	#56
2269  02cf 7b000b            	stab	_diag_def_ecu_pno+1
2270                         ; 1242 			diag_def_ecu_pno[2]  = 0x31;
2272  02d2 c631              	ldab	#49
2273  02d4 7b000c            	stab	_diag_def_ecu_pno+2
2274                         ; 1243 			diag_def_ecu_pno[3]  = 0x33;
2276  02d7 c633              	ldab	#51
2277  02d9 7b000d            	stab	_diag_def_ecu_pno+3
2278                         ; 1244 			diag_def_ecu_pno[4]  = 0x37;
2280  02dc c637              	ldab	#55
2281  02de 7b000e            	stab	_diag_def_ecu_pno+4
2282                         ; 1245 			diag_def_ecu_pno[5]  = 0x35;
2284  02e1 c635              	ldab	#53
2285  02e3 7b000f            	stab	_diag_def_ecu_pno+5
2286                         ; 1246 			diag_def_ecu_pno[6]  = 0x34;
2288  02e6 53                	decb	
2289  02e7 7b0010            	stab	_diag_def_ecu_pno+6
2290                         ; 1247 			diag_def_ecu_pno[7]  = 0x36;
2292  02ea c636              	ldab	#54
2293                         ; 1248 			diag_def_ecu_pno[8]  = 0x41;
2295                         ; 1249 			diag_def_ecu_pno[9]  = 0x45;
2297  02ec                   LC001:
2298  02ec 7b0011            	stab	_diag_def_ecu_pno+7
2299  02ef c641              	ldab	#65
2300  02f1 7b0012            	stab	_diag_def_ecu_pno+8
2301  02f4 c645              	ldab	#69
2302                         ; 1250 			break;
2304  02f6 202b              	bra	L7711
2305  02f8                   L1511:
2306                         ; 1263 			diag_def_ecu_pno[0]  = 'P';//0x35;
2308  02f8 c650              	ldab	#80
2309  02fa 7b000a            	stab	_diag_def_ecu_pno
2310                         ; 1264 			diag_def_ecu_pno[1]  = 'I';//0x36;
2312  02fd c649              	ldab	#73
2313  02ff 7b000b            	stab	_diag_def_ecu_pno+1
2314                         ; 1265 			diag_def_ecu_pno[2]  = 'D';//0x30;
2316  0302 c644              	ldab	#68
2317  0304 7b000c            	stab	_diag_def_ecu_pno+2
2318                         ; 1266 			diag_def_ecu_pno[3]  = 'C';//0x34;
2320  0307 53                	decb	
2321  0308 7b000d            	stab	_diag_def_ecu_pno+3
2322                         ; 1267 			diag_def_ecu_pno[4]  = 'O';//0x36;
2324  030b c64f              	ldab	#79
2325  030d 7b000e            	stab	_diag_def_ecu_pno+4
2326                         ; 1268 			diag_def_ecu_pno[5]  = 'N';//0x35;
2328  0310 53                	decb	
2329  0311 7b000f            	stab	_diag_def_ecu_pno+5
2330                         ; 1269 			diag_def_ecu_pno[6]  = 'N';//0x39;
2332  0314 7b0010            	stab	_diag_def_ecu_pno+6
2333                         ; 1270 			diag_def_ecu_pno[7]  = 'U';//0x34;
2335  0317 c655              	ldab	#85
2336  0319 7b0011            	stab	_diag_def_ecu_pno+7
2337                         ; 1271 			diag_def_ecu_pno[8]  = 'M';//0x41;
2339  031c c64d              	ldab	#77
2340  031e 7b0012            	stab	_diag_def_ecu_pno+8
2341                         ; 1272 			diag_def_ecu_pno[9]  = '2';//0x45;
2343  0321 c632              	ldab	#50
2344  0323                   L7711:
2345  0323 7b0013            	stab	_diag_def_ecu_pno+9
2346                         ; 1277 	EE_BlockRead(EE_FBL_CHR_APPL_SW_PNO, &temp_partno_c[0]);
2348  0326 1a80              	leax	OFST-13,s
2349  0328 34                	pshx	
2350  0329 cc0044            	ldd	#68
2351  032c 4a000000          	call	f_EE_BlockRead
2353  0330 1b82              	leas	2,s
2354                         ; 1279 	if ( (temp_partno_c[5] != diag_def_ecu_pno[5]) ||
2354                         ; 1280 	    (temp_partno_c[6] != diag_def_ecu_pno[6]) ||
2354                         ; 1281 	    (temp_partno_c[7] != diag_def_ecu_pno[7]) ||
2354                         ; 1282 	    (temp_partno_c[8] != diag_def_ecu_pno[8]) ||
2354                         ; 1283 	    (temp_partno_c[9] != diag_def_ecu_pno[9]) )
2356  0332 e685              	ldab	OFST-8,s
2357  0334 f1000f            	cmpb	_diag_def_ecu_pno+5
2358  0337 261c              	bne	L3021
2360  0339 e686              	ldab	OFST-7,s
2361  033b f10010            	cmpb	_diag_def_ecu_pno+6
2362  033e 2615              	bne	L3021
2364  0340 e687              	ldab	OFST-6,s
2365  0342 f10011            	cmpb	_diag_def_ecu_pno+7
2366  0345 260e              	bne	L3021
2368  0347 e688              	ldab	OFST-5,s
2369  0349 f10012            	cmpb	_diag_def_ecu_pno+8
2370  034c 2607              	bne	L3021
2372  034e e689              	ldab	OFST-4,s
2373  0350 f10013            	cmpb	_diag_def_ecu_pno+9
2374  0353 2742              	beq	L1021
2375  0355                   L3021:
2376                         ; 1285 		temp_partno_c[0] = diag_def_ecu_pno[0];
2378  0355 180980000a        	movb	_diag_def_ecu_pno,OFST-13,s
2379                         ; 1286 		temp_partno_c[1] = diag_def_ecu_pno[1];
2381  035a 180981000b        	movb	_diag_def_ecu_pno+1,OFST-12,s
2382                         ; 1287 		temp_partno_c[2] = diag_def_ecu_pno[2];
2384  035f 180982000c        	movb	_diag_def_ecu_pno+2,OFST-11,s
2385                         ; 1288 		temp_partno_c[3] = diag_def_ecu_pno[3];
2387  0364 180983000d        	movb	_diag_def_ecu_pno+3,OFST-10,s
2388                         ; 1289 		temp_partno_c[4] = diag_def_ecu_pno[4];
2390  0369 180984000e        	movb	_diag_def_ecu_pno+4,OFST-9,s
2391                         ; 1290 		temp_partno_c[5] = diag_def_ecu_pno[5];
2393  036e 180985000f        	movb	_diag_def_ecu_pno+5,OFST-8,s
2394                         ; 1291 		temp_partno_c[6] = diag_def_ecu_pno[6];
2396  0373 1809860010        	movb	_diag_def_ecu_pno+6,OFST-7,s
2397                         ; 1292 		temp_partno_c[7] = diag_def_ecu_pno[7];
2399  0378 1809870011        	movb	_diag_def_ecu_pno+7,OFST-6,s
2400                         ; 1293 		temp_partno_c[8] = diag_def_ecu_pno[8];
2402  037d 1809880012        	movb	_diag_def_ecu_pno+8,OFST-5,s
2403                         ; 1294 		temp_partno_c[9] = diag_def_ecu_pno[9];
2405  0382 1809890013        	movb	_diag_def_ecu_pno+9,OFST-4,s
2406                         ; 1296 		EE_BlockWrite(EE_FBL_CHR_APPL_SW_PNO, &temp_partno_c[0]);
2408  0387 1a80              	leax	OFST-13,s
2409  0389 34                	pshx	
2410  038a cc0044            	ldd	#68
2411  038d 4a000000          	call	f_EE_BlockWrite
2413  0391 1b82              	leas	2,s
2414                         ; 1297 		sw_partno_update_b = TRUE;
2416  0393 1c001701          	bset	_diag_partno_updates_c,1
2417  0397                   L1021:
2418                         ; 1301 	EE_BlockRead(EE_FBL_CHR_ECU_PNO, &temp_partno_c[0]);
2420  0397 1a80              	leax	OFST-13,s
2421  0399 34                	pshx	
2422  039a cc0046            	ldd	#70
2423  039d 4a000000          	call	f_EE_BlockRead
2425  03a1 1b82              	leas	2,s
2426                         ; 1303 	if ( (temp_partno_c[5] != diag_def_ecu_pno[5]) ||
2426                         ; 1304 	    (temp_partno_c[6] != diag_def_ecu_pno[6]) ||
2426                         ; 1305 	    (temp_partno_c[7] != diag_def_ecu_pno[7]) ||
2426                         ; 1306 	    (temp_partno_c[8] != diag_def_ecu_pno[8]) ||
2426                         ; 1307 	    (temp_partno_c[9] != diag_def_ecu_pno[9]))
2428  03a3 e685              	ldab	OFST-8,s
2429  03a5 f1000f            	cmpb	_diag_def_ecu_pno+5
2430  03a8 261c              	bne	L5121
2432  03aa e686              	ldab	OFST-7,s
2433  03ac f10010            	cmpb	_diag_def_ecu_pno+6
2434  03af 2615              	bne	L5121
2436  03b1 e687              	ldab	OFST-6,s
2437  03b3 f10011            	cmpb	_diag_def_ecu_pno+7
2438  03b6 260e              	bne	L5121
2440  03b8 e688              	ldab	OFST-5,s
2441  03ba f10012            	cmpb	_diag_def_ecu_pno+8
2442  03bd 2607              	bne	L5121
2444  03bf e689              	ldab	OFST-4,s
2445  03c1 f10013            	cmpb	_diag_def_ecu_pno+9
2446  03c4 2742              	beq	L3121
2447  03c6                   L5121:
2448                         ; 1309 		temp_partno_c[0] = diag_def_ecu_pno[0];
2450  03c6 180980000a        	movb	_diag_def_ecu_pno,OFST-13,s
2451                         ; 1310 		temp_partno_c[1] = diag_def_ecu_pno[1];
2453  03cb 180981000b        	movb	_diag_def_ecu_pno+1,OFST-12,s
2454                         ; 1311 		temp_partno_c[2] = diag_def_ecu_pno[2];
2456  03d0 180982000c        	movb	_diag_def_ecu_pno+2,OFST-11,s
2457                         ; 1312 		temp_partno_c[3] = diag_def_ecu_pno[3];
2459  03d5 180983000d        	movb	_diag_def_ecu_pno+3,OFST-10,s
2460                         ; 1313 		temp_partno_c[4] = diag_def_ecu_pno[4];
2462  03da 180984000e        	movb	_diag_def_ecu_pno+4,OFST-9,s
2463                         ; 1314 		temp_partno_c[5] = diag_def_ecu_pno[5];
2465  03df 180985000f        	movb	_diag_def_ecu_pno+5,OFST-8,s
2466                         ; 1315 		temp_partno_c[6] = diag_def_ecu_pno[6];
2468  03e4 1809860010        	movb	_diag_def_ecu_pno+6,OFST-7,s
2469                         ; 1316 		temp_partno_c[7] = diag_def_ecu_pno[7];
2471  03e9 1809870011        	movb	_diag_def_ecu_pno+7,OFST-6,s
2472                         ; 1317 		temp_partno_c[8] = diag_def_ecu_pno[8];
2474  03ee 1809880012        	movb	_diag_def_ecu_pno+8,OFST-5,s
2475                         ; 1318 		temp_partno_c[9] = diag_def_ecu_pno[9];
2477  03f3 1809890013        	movb	_diag_def_ecu_pno+9,OFST-4,s
2478                         ; 1320 		EE_BlockWrite(EE_FBL_CHR_ECU_PNO, &temp_partno_c[0]);
2480  03f8 1a80              	leax	OFST-13,s
2481  03fa 34                	pshx	
2482  03fb cc0046            	ldd	#70
2483  03fe 4a000000          	call	f_EE_BlockWrite
2485  0402 1b82              	leas	2,s
2486                         ; 1321 		ecu_partno_update_b = TRUE;
2488  0404 1c001704          	bset	_diag_partno_updates_c,4
2489  0408                   L3121:
2490                         ; 1325 	EE_BlockRead(EE_FBL_FIAT_ECU_SW_VERSION, &temp_swversion_c[0]);
2492  0408 1a8a              	leax	OFST-3,s
2493  040a 34                	pshx	
2494  040b cc0041            	ldd	#65
2495  040e 4a000000          	call	f_EE_BlockRead
2497  0412 1b82              	leas	2,s
2498                         ; 1327 	if ((temp_swversion_c[0] != SW_YEAR) ||
2498                         ; 1328 	    (temp_swversion_c[1] != SW_WEEK) /*||
2498                         ; 1329 	    (temp_swversion_c[2] != SW_PATCH)*/)
2500  0414 e68a              	ldab	OFST-3,s
2501  0416 2606              	bne	L7221
2503  0418 e68b              	ldab	OFST-2,s
2504  041a c10a              	cmpb	#10
2505  041c 2716              	beq	L5221
2506  041e                   L7221:
2507                         ; 1332 		temp_swversion_c[0] = SW_YEAR;
2509  041e 698a              	clr	OFST-3,s
2510                         ; 1333 		temp_swversion_c[1] = SW_WEEK;
2512  0420 c60a              	ldab	#10
2513  0422 6b8b              	stab	OFST-2,s
2514                         ; 1336 		EE_BlockWrite(EE_FBL_FIAT_ECU_SW_VERSION, &temp_swversion_c[0]);
2516  0424 1a8a              	leax	OFST-3,s
2517  0426 34                	pshx	
2518  0427 cc0041            	ldd	#65
2519  042a 4a000000          	call	f_EE_BlockWrite
2521  042e 1b82              	leas	2,s
2522                         ; 1337 		sw_version_update_b = TRUE;
2524  0430 1c001708          	bset	_diag_partno_updates_c,8
2525  0434                   L5221:
2526                         ; 1341 	if ((sw_partno_update_b == TRUE)  ||
2526                         ; 1342 	    (ecu_partno_update_b == TRUE) ||
2526                         ; 1343 	    (sw_version_update_b == TRUE) )
2528  0434 1e0017010a        	brset	_diag_partno_updates_c,1,L3321
2530  0439 1e00170405        	brset	_diag_partno_updates_c,4,L3321
2532  043e 1f00170808        	brclr	_diag_partno_updates_c,8,L1321
2533  0443                   L3321:
2534                         ; 1347 		diag_load_bl_eep_backup_b = TRUE; //Time to take boot backup
2536  0443 1c000040          	bset	_diag_io_lock3_flags_c,64
2537                         ; 1348 		sw_partno_update_b = FALSE;
2539                         ; 1349 		ecu_partno_update_b = FALSE;
2541                         ; 1350 		sw_version_update_b = FALSE;
2543  0447 1d00170d          	bclr	_diag_partno_updates_c,13
2544  044b                   L1321:
2545                         ; 1352 } //End of function
2548  044b 1b8d              	leas	13,s
2549  044d 0a                	rtc	
2586                         ; 1362 void diagLF_CheckResetRequest(void)
2586                         ; 1363 {
2587                         	switch	.ftext
2588  044e                   f_diagLF_CheckResetRequest:
2590  044e 37                	pshb	
2591       00000001          OFST:	set	1
2594                         ; 1365 	switch (d_ResetReq_t) {
2596  044f f60019            	ldab	_d_ResetReq_t
2598  0452 c022              	subb	#34
2599  0454 2727              	beq	L3421
2600  0456 c033              	subb	#51
2601  0458 271c              	beq	L1421
2602  045a c055              	subb	#85
2603  045c 262b              	bne	L5621
2604                         ; 1369 			temp = D_FBL_BOOTMODE_VAL_K;
2606  045e c6c3              	ldab	#195
2607  0460 6b80              	stab	OFST-1,s
2608                         ; 1370             EE_BlockWrite(EE_FBL_PROGREQ_FLAGS,&temp);
2610  0462 1a80              	leax	OFST-1,s
2611  0464 34                	pshx	
2612  0465 cc0032            	ldd	#50
2613  0468 4a000000          	call	f_EE_BlockWrite
2615  046c 1b82              	leas	2,s
2616                         ; 1375   	  	    diag_load_bl_eep_backup_b = TRUE; //Time to take backup copy of boot
2618  046e 1c000040          	bset	_diag_io_lock3_flags_c,64
2619                         ; 1377   	  	    d_ResetReq_t = PREPARE_RESET_REQUEST;
2621  0472 c655              	ldab	#85
2622                         ; 1378 			break;
2624  0474 2002              	bra	LC002
2625  0476                   L1421:
2626                         ; 1386 				d_ResetReq_t = RESET_REQUEST;
2628  0476 c622              	ldab	#34
2629  0478                   LC002:
2630  0478 7b0019            	stab	_d_ResetReq_t
2631                         ; 1392 			break;
2633  047b 200c              	bra	L5621
2634  047d                   L3421:
2635                         ; 1397 			if ( EE_IsBusy() != TRUE) {
2637  047d 4a000000          	call	f_EE_IsBusy
2639  0481 040105            	dbeq	b,L5621
2640                         ; 1399 				main_ECU_reset_rq_c = TRUE;
2642  0484 c601              	ldab	#1
2643  0486 7b0000            	stab	_main_ECU_reset_rq_c
2644                         ; 1402 				break;
2646  0489                   L5621:
2647                         ; 1413 }
2650  0489 1b81              	leas	1,s
2651  048b 0a                	rtc	
2877                         	xdef	f_ApplDescSetCommMode
2878                         	xdef	f_diagF_DiagResponse
2879                         	xdef	f_diagLF_SessionTimeout
2880                         	xdef	f_diagLF_PartNo_SwVer_Update
2881                         	xdef	f_diagLF_CheckResetRequest
2882                         	switch	.bss
2883  0000                   _diag_def_sw_pno:
2884  0000 0000000000000000  	ds.b	10
2885                         	xdef	_diag_def_sw_pno
2886  000a                   _diag_def_ecu_pno:
2887  000a 0000000000000000  	ds.b	10
2888                         	xdef	_diag_def_ecu_pno
2889  0014                   _DataLength:
2890  0014 0000              	ds.b	2
2891                         	xdef	_DataLength
2892  0016                   _diag_writedata_mode_c:
2893  0016 00                	ds.b	1
2894                         	xdef	_diag_writedata_mode_c
2895  0017                   _diag_partno_updates_c:
2896  0017 00                	ds.b	1
2897                         	xdef	_diag_partno_updates_c
2898  0018                   L702_Response:
2899  0018 00                	ds.b	1
2900  0019                   _d_ResetReq_t:
2901  0019 00                	ds.b	1
2902                         	xdef	_d_ResetReq_t
2903                         	xref	f_EE_IsBusy
2904                         	xref	f_EE_BlockRead
2905                         	xref	f_EE_BlockWrite
2906                         	xref	_d_DtcSettingModeEnabled_t
2907                         	xref	_diag_io_vs_lock_flags_c
2908                         	xref	_diag_io_hs_lock_flags_c
2909                         	xref	_diag_io_lock3_flags_c
2910                         	xref	_diag_io_lock2_flags_c
2911                         	xref	_diag_io_lock_flags_c
2912                         	xref	_diag_io_vs_state_c
2913                         	xref	_diag_io_vs_rq_c
2914                         	xref	_diag_io_hs_state_c
2915                         	xref	_diag_io_hs_rq_c
2916                         	xref	_diag_op_rc_preCondition_stat_c
2917                         	xref	_diag_op_rc_status_c
2918                         	xref	_diag_io_st_whl_state_c
2919                         	xref	_diag_io_st_whl_rq_c
2920                         	xref	_diag_io_vs_all_out_c
2921                         	xref	_diag_io_ign_state_c
2922                         	xref	_diag_io_load_shed_c
2923                         	xref	f_diagWriteF_CyclicTask
2924                         	xref	f_diagSecF_CyclicTask
2925                         	xref	f_diagSecF_ExtSession_Init
2926                         	xref	f_diagSecF_DefSession_Init
2927                         	xref	f_diagSecF_Main_Init
2928  001a                   L711_diag_sec_FAA_c:
2929  001a 00                	ds.b	1
2930  001b                   L511_secMod1_var2_l:
2931  001b 00000000          	ds.b	4
2932  001f                   L311_secMod1_var1_l:
2933  001f 00000000          	ds.b	4
2934  0023                   L111_diag_10sec_timer_w:
2935  0023 0000              	ds.b	2
2936  0025                   L701_hsm_key_l:
2937  0025 00000000          	ds.b	4
2938  0029                   L501_tool_key_l:
2939  0029 00000000          	ds.b	4
2940  002d                   L301_diag_seed_generated_l:
2941  002d 00000000          	ds.b	4
2942  0031                   L101_diag_seed_l:
2943  0031 00000000          	ds.b	4
2944                         	xref	_diag_sec_access1_state_c
2945                         	xref	_diag_flags_c
2946                         	xdef	f_diagF_CyclicWriteTask
2947                         	xdef	f_diagF_Init
2948  0035                   _diag_rc_lock_flags_c:
2949  0035 00                	ds.b	1
2950                         	xdef	_diag_rc_lock_flags_c
2951  0036                   _diag_status_flags_c:
2952  0036 00                	ds.b	1
2953                         	xdef	_diag_status_flags_c
2954                         	xref	_main_variant_select_c
2955                         	xref	_main_ECU_reset_rq_c
2956                         	xdef	f_ApplDescCheckSessionTransition
2957                         	xdef	f_ApplDescOnCommunicationEnable
2958                         	xdef	f_ApplDescOnCommunicationDisable
2959                         	xdef	f_ApplDescCheckCommCtrl
2960                         	xdef	f_ApplDescTester_PrresentTesterPresent
2961                         	xdef	f_ApplDescReqTransferExitDownload
2962                         	xdef	f_ApplDescTransferDataDownload
2963                         	xdef	f_ApplDescRequestDownload
2964                         	xdef	f_ApplDescResetHardReset
2965                         	xdef	f_ApplDescOnTransitionSession
2966                         	xdef	f_ApplDescRcrRpConfirmation
2967                         	xdef	f_ApplDescFatalError
2968                         	xref	f_DescInitPowerOn
2969                         	xref	f_DescSetNegResponse
2970                         	xref	f_DescProcessingDone
2991                         	end
