   1                         ; C Compiler for S12XE [COSMIC Software].
   2                         ; Generator V4.7.8 - 27 Jul 2007
   3                         ; Optimizer V4.6.13 - 27 Jul 2007
  52                         ; 284 UINT8 EepromDriver_InitSync( void* address )
  52                         ; 285 {
  53                         .ftext:	section	.text
  54  0000                   f_EepromDriver_InitSync:
  58  0000                   L311:
  59                         ; 294    while ((FSTAT & CCIF) == 0x00)
  61  0000 1f000080fb        	brclr	__FSTAT,128,L311
  62                         ; 300    FCLKDIV = (FCLK_PRESCALER & 0x7F);
  64  0005 c60f              	ldab	#15
  65  0007 7b0000            	stab	__FCLKDIV
  66                         ; 303    FERCNFG = 0;
  68  000a 790000            	clr	__FERCNFG
  69                         ; 306    FCNFG = 0x10;
  71  000d 52                	incb	
  72  000e 7b0000            	stab	__FCNFG
  73                         ; 308    if ((FCLKDIV & FDIVLD) != FDIVLD)
  75  0011 1e00008003        	brset	__FCLKDIV,128,L711
  76                         ; 310       return(~IO_E_OK);
  78  0016 c6ff              	ldab	#255
  81  0018 0a                	rtc	
  82  0019                   L711:
  83                         ; 313    return(IO_E_OK);
  85  0019 c7                	clrb	
  88  001a 0a                	rtc	
 141                         ; 325 UINT8 EepromDriver_RWriteSync( unsigned char * writeBuffer, unsigned char writeLength, 
 141                         ; 326    unsigned long writeAddress )
 141                         ; 327 {
 142                         	switch	.ftext
 143  001b                   L34f_EepromDriver_RWriteSync:
 145  001b 3b                	pshd	
 146       00000000          OFST:	set	0
 149                         ; 328    if (writeLength > 0)
 151  001c e686              	ldab	OFST+6,s
 152  001e 2726              	beq	L341
 153                         ; 330       EE_ForcedWrite_Local(writeAddress, writeBuffer, writeLength);
 155  0020 87                	clra	
 156  0021 3b                	pshd	
 157  0022 ec82              	ldd	OFST+2,s
 158  0024 3b                	pshd	
 159  0025 ec8d              	ldd	OFST+13,s
 160  0027 ee8b              	ldx	OFST+11,s
 161  0029 4a00b9b9          	call	L33f_EE_ForcedWrite_Local
 163  002d 1b84              	leas	4,s
 165  002f 200e              	bra	L741
 166  0031                   L541:
 167                         ; 335          HWIO_PetTheDog();
 169  0031 c655              	ldab	#85
 170  0033 7b0000            	stab	__ARMCOP
 173  0036 c6aa              	ldab	#170
 174  0038 7b0000            	stab	__ARMCOP
 175                         ; 337          EE_Handler_Local();
 177  003b 4a008686          	call	L73f_EE_Handler_Local
 179  003f                   L741:
 180                         ; 333       while (EE_IsBusy_Local() == TRUE)
 182  003f 4a00aaaa          	call	L53f_EE_IsBusy_Local
 184  0043 0411eb            	dbeq	b,L541
 185  0046                   L341:
 186                         ; 341    return(IO_E_OK);
 188  0046 c7                	clrb	
 191  0047 31                	puly	
 192  0048 0a                	rtc	
 250                         ; 354 UINT8 EepromDriver_RReadSync( unsigned char * readBuffer, unsigned char readLength, 
 250                         ; 355    unsigned long readAddress )
 250                         ; 356 {
 251                         	switch	.ftext
 252  0049                   L14f_EepromDriver_RReadSync:
 254  0049 3b                	pshd	
 255  004a 37                	pshb	
 256       00000001          OFST:	set	1
 259                         ; 359    if (readLength > 0)
 261  004b e687              	ldab	OFST+6,s
 262  004d 2719              	beq	L771
 263                         ; 361       PrevGpage = GPAGE;
 265  004f f60000            	ldab	__GPAGE
 266  0052 6b80              	stab	OFST-1,s
 267                         ; 362       EepRead((void *)readBuffer, (void *)(readAddress & 0xFFFF), readLength);
 269  0054 e687              	ldab	OFST+6,s
 270  0056 87                	clra	
 271  0057 3b                	pshd	
 272  0058 ec8c              	ldd	OFST+11,s
 273                         
 274  005a 3b                	pshd	
 275  005b ec85              	ldd	OFST+4,s
 276  005d 4a006c6c          	call	L35f_EepRead
 278  0061 1b84              	leas	4,s
 279                         ; 363       GPAGE = PrevGpage;
 281  0063 180d800000        	movb	OFST-1,s,__GPAGE
 282  0068                   L771:
 283                         ; 366    return(IO_E_OK);
 285  0068 c7                	clrb	
 288  0069 1b83              	leas	3,s
 289  006b 0a                	rtc	
 323                         ; 378 static EepRead( void *dest, void *source, unsigned short count )
 323                         ; 379 {
 324                         	switch	.ftext
 325  006c                   L35f_EepRead:
 329                         ; 381          tfr d,y                  ; Y -> destination
 332  006c b746              	tfr	d,y
 334                         ; 382          ldd 4,sp                 ; D -> count
 337  006e ec84              	ldd	4,sp
 339                         ; 383          beq read_exit            ; Exit if count == 0
 342  0070 2713              	beq	read_exit
 344                         ; 384          ldx 2,sp                 ; X -> source
 347  0072 ee82              	ldx	2,sp
 349                         ; 385          movb #$10, $10           ; Init GPAGE
 352  0074 180b100010        	movb	#$10, $10
 354                         ; 386       read_loop:
 357  0079                   read_loop:
 359                         ; 387          pshd                     ; Save counter
 362  0079 3b                	pshd	
 364                         ; 388          gldaa   x
 367  007a 18a600            	gldaa	x
 369                         ; 389          staa    y
 372  007d 6a40              	staa	y
 374                         ; 390          inx
 377  007f 08                	inx	
 379                         ; 391          iny
 382  0080 02                	iny	
 384                         ; 392          puld                     ; Restore counter
 387  0081 3a                	puld	
 389                         ; 393          dbne    d,read_loop      ; Count down and loop back
 392  0082 0434f4            	dbne	d,read_loop
 394                         ; 394       read_exit:
 397  0085                   read_exit:
 399                         ; 396 }
 402  0085 0a                	rtc	
 429                         ; 411 void EE_Handler_Local(void)
 429                         ; 412 {
 430                         	switch	.ftext
 431  0086                   L73f_EE_Handler_Local:
 435                         ; 414     switch(eEECurrentState)
 437  0086 f6000c            	ldab	L16_eEECurrentState
 439  0089 04010a            	dbeq	b,L712
 440  008c 04010c            	dbeq	b,L122
 441  008f 04010e            	dbeq	b,L322
 442  0092 040110            	dbeq	b,L522
 444  0095 0a                	rtc	
 445  0096                   L712:
 446                         ; 416     case EE_WRITE_SEQUENCE_INITIATED:
 446                         ; 417         EE_WriteSequenceInitiated();
 448  0096 4a01f6f6          	call	L31f_EE_WriteSequenceInitiated
 450                         ; 418         break;
 453  009a 0a                	rtc	
 454  009b                   L122:
 455                         ; 420     case EE_WAIT_FOR_ERASE_COMPLETION:
 455                         ; 421         EE_Burst_Write();
 457  009b 4a022020          	call	L52f_EE_Burst_Write
 459                         ; 422         break;
 462  009f 0a                	rtc	
 463  00a0                   L322:
 464                         ; 424     case EE_WAIT_FOR_WRITE_COMPLETION:
 464                         ; 425         EE_WaitForWriteCompletion();
 466  00a0 4a029e9e          	call	L51f_EE_WaitForWriteCompletion
 468                         ; 426         break;
 471  00a4 0a                	rtc	
 472  00a5                   L522:
 473                         ; 428     case EE_RETRY:
 473                         ; 429         EE_Retry();
 475  00a5 4a032929          	call	L71f_EE_Retry
 477                         ; 430         break;
 479                         ; 432     case EE_IDLE:
 479                         ; 433         break;
 481                         ; 435     default:
 481                         ; 436         break;
 483                         ; 438 }
 486  00a9 0a                	rtc	
 518                         ; 449 unsigned char EE_IsBusy_Local(void)
 518                         ; 450 {
 519                         	switch	.ftext
 520  00aa                   L53f_EE_IsBusy_Local:
 522  00aa 37                	pshb	
 523       00000001          OFST:	set	1
 526                         ; 453     if(eEECurrentState == EE_IDLE)
 528  00ab f6000c            	ldab	L16_eEECurrentState
 529  00ae 2604              	bne	L362
 530                         ; 455         bystatus = FALSE;
 532  00b0 6b80              	stab	OFST-1,s
 534  00b2 2002              	bra	L562
 535  00b4                   L362:
 536                         ; 459         bystatus = TRUE;
 538  00b4 c601              	ldab	#1
 539  00b6                   L562:
 540                         ; 462     return bystatus;
 544  00b6 1b81              	leas	1,s
 545  00b8 0a                	rtc	
 597                         ; 476 unsigned char EE_ForcedWrite_Local(unsigned long ee_dest, unsigned char *pbysrc, unsigned char bysize)
 597                         ; 477 {
 598                         	switch	.ftext
 599  00b9                   L33f_EE_ForcedWrite_Local:
 601       fffffffc          OFST:	set	-4
 604                         ; 478     sEEPendingWrite.lAddress = ee_dest;
 606  00b9 7c0114            	std	L55_sEEPendingWrite+2
 607  00bc 7e0112            	stx	L55_sEEPendingWrite
 608                         ; 481     sEEPendingWrite.pbyData = pbysrc;
 610  00bf 1805830116        	movw	OFST+7,s,L55_sEEPendingWrite+4
 611                         ; 484     sEEPendingWrite.byLength = bysize;
 613  00c4 180d860118        	movb	OFST+10,s,L55_sEEPendingWrite+6
 614                         ; 486     sEEPendingWrite.byRetryCount = 0;
 616  00c9 79011a            	clr	L55_sEEPendingWrite+8
 617                         ; 489     if (EE_PrepDataForWrite())
 619  00cc 4a017474          	call	L3f_EE_PrepDataForWrite
 621  00d0 044107            	tbeq	b,L113
 622                         ; 492        EE_SetNextStateTo(EE_WRITE_SEQUENCE_INITIATED);
 624  00d3 cc0001            	ldd	#1
 625  00d6 4a00dddd          	call	L11f_EE_SetNextStateTo
 627  00da                   L113:
 628                         ; 495     return TRUE;
 630  00da c601              	ldab	#1
 633  00dc 0a                	rtc	
 712                         ; 507 void EE_SetNextStateTo(EEEState enew_state)
 712                         ; 508 {
 713                         	switch	.ftext
 714  00dd                   L11f_EE_SetNextStateTo:
 718                         ; 509     eEECurrentState = enew_state;
 720  00dd 7b000c            	stab	L16_eEECurrentState
 721                         ; 510 }
 724  00e0 0a                	rtc	
 759                         ; 525 unsigned char EE_SubmitCommand(void)
 759                         ; 526 {
 760                         	switch	.ftext
 761  00e1                   L5f_EE_SubmitCommand:
 763  00e1 37                	pshb	
 764       00000001          OFST:	set	1
 767                         ; 530     if (Flash_IsProtectionViolation())
 769  00e2 1f00001004        	brclr	__FSTAT,16,L173
 770                         ; 532         Flash_ClearProtectionViolation();
 772  00e7 1c000010          	bset	__FSTAT,16
 773  00eb                   L173:
 774                         ; 535     if (Flash_IsAccessError())
 776  00eb 1f00002004        	brclr	__FSTAT,32,L373
 777                         ; 537        Flash_ClearAccessError();
 779  00f0 1c000020          	bset	__FSTAT,32
 780  00f4                   L373:
 781                         ; 540     FCCOBIX = 0; 
 783  00f4 87                	clra	
 784  00f5 7a0000            	staa	__FCCOBIX
 785                         ; 541     FCCOB = (((unsigned int)sFCCOBBlock.bFlashCmd) << 8) + sFCCOBBlock.bGlblAddr_Bit22To16;
 787  00f8 b60000            	ldaa	L36_sFCCOBBlock
 788  00fb c7                	clrb	
 789  00fc fb0001            	addb	L36_sFCCOBBlock+1
 790  00ff 8900              	adca	#0
 791  0101 7c0000            	std	__FCCOB
 792                         ; 543     switch (sFCCOBBlock.bFlashCmd)
 794  0104 f60000            	ldab	L36_sFCCOBBlock
 796  0107 c011              	subb	#17
 797  0109 270d              	beq	L153
 798  010b 04213d            	dbne	b,L773
 799                         ; 546         case ERASE_D_FLASH_SECTOR:
 799                         ; 547             FCCOBIX = 1;
 801  010e c601              	ldab	#1
 802  0110 7b0000            	stab	__FCCOBIX
 803                         ; 548             FCCOB = sFCCOBBlock.wGlblAddr_Bit15To0;
 805  0113 fc0002            	ldd	L36_sFCCOBBlock+2
 806                         ; 549             break;
 808  0116 2030              	bra	LC001
 809  0118                   L153:
 810                         ; 552         case PROGRAM_D_FLASH_WORDS:
 810                         ; 553             FCCOBIX = 1;
 812  0118 c601              	ldab	#1
 813  011a 7b0000            	stab	__FCCOBIX
 814                         ; 554             FCCOB = sFCCOBBlock.wGlblAddr_Bit15To0;
 816  011d 180400020000      	movw	L36_sFCCOBBlock+2,__FCCOB
 817                         ; 555             FCCOBIX = 2;
 819  0123 52                	incb	
 820  0124 7b0000            	stab	__FCCOBIX
 821                         ; 556             FCCOB = sFCCOBBlock.wData0;
 823  0127 180400040000      	movw	L36_sFCCOBBlock+4,__FCCOB
 824                         ; 557             FCCOBIX = 3;
 826  012d 52                	incb	
 827  012e 7b0000            	stab	__FCCOBIX
 828                         ; 558             FCCOB = sFCCOBBlock.wData1;
 830  0131 180400060000      	movw	L36_sFCCOBBlock+6,__FCCOB
 831                         ; 559             FCCOBIX = 4;
 833  0137 52                	incb	
 834  0138 7b0000            	stab	__FCCOBIX
 835                         ; 560             FCCOB = sFCCOBBlock.wData2;
 837  013b 180400080000      	movw	L36_sFCCOBBlock+8,__FCCOB
 838                         ; 561             FCCOBIX = 5;
 840  0141 52                	incb	
 841  0142 7b0000            	stab	__FCCOBIX
 842                         ; 562             FCCOB = sFCCOBBlock.wData3;
 844  0145 fc000a            	ldd	L36_sFCCOBBlock+10
 845  0148                   LC001:
 846  0148 7c0000            	std	__FCCOB
 847                         ; 563             break;
 849                         ; 565         default:  
 849                         ; 566             break;
 851  014b                   L773:
 852                         ; 570     Flash_SetCommand();
 854  014b c680              	ldab	#128
 855  014d 7b0000            	stab	__FSTAT
 856                         ; 572     if (Flash_IsProtectionViolation() || Flash_IsAccessError())
 858  0150 1e00001005        	brset	__FSTAT,16,L304
 860  0155 1f00002016        	brclr	__FSTAT,32,L104
 861  015a                   L304:
 862                         ; 574        if (Flash_IsProtectionViolation())
 864  015a 1f00001004        	brclr	__FSTAT,16,L504
 865                         ; 576            Flash_ClearProtectionViolation();
 867  015f 1c000010          	bset	__FSTAT,16
 868  0163                   L504:
 869                         ; 579        if(Flash_IsAccessError())
 871  0163 1f00002004        	brclr	__FSTAT,32,L704
 872                         ; 581            Flash_ClearAccessError();
 874  0168 1c000020          	bset	__FSTAT,32
 875  016c                   L704:
 876                         ; 584         byresult = FALSE;
 878  016c c7                	clrb	
 880  016d                   L114:
 881                         ; 592     return byresult;
 885  016d 1b81              	leas	1,s
 886  016f 0a                	rtc	
 887  0170                   L104:
 888                         ; 589         byresult = TRUE;
 890  0170 c601              	ldab	#1
 891  0172 20f9              	bra	L114
 955                         ; 610 unsigned char EE_PrepDataForWrite(void)
 955                         ; 611 {
 956                         	switch	.ftext
 957  0174                   L3f_EE_PrepDataForWrite:
 959  0174 1b99              	leas	-7,s
 960       00000007          OFST:	set	7
 963                         ; 617     if (sEEPendingWrite.byLength != 0)
 965  0176 f60118            	ldab	L55_sEEPendingWrite+6
 966  0179 2778              	beq	L144
 967                         ; 620         EE_SetEPAGE(sEEPendingWrite.lAddress);
 969  017b fc0114            	ldd	L55_sEEPendingWrite+2
 970  017e fe0112            	ldx	L55_sEEPendingWrite
 971  0181 4a037575          	call	L72f_GetEPage
 973  0185 7b0000            	stab	__EPAGE
 974                         ; 621         pbyAddress = EE_GetLogicalAddr(sEEPendingWrite.lAddress);
 976  0188 fc0114            	ldd	L55_sEEPendingWrite+2
 977  018b fe0112            	ldx	L55_sEEPendingWrite
 978  018e 4a037e7e          	call	L13f_GetEOffset
 980  0192 c30800            	addd	#2048
 981  0195 6c84              	std	OFST-3,s
 982                         ; 624         pbysector_address = EE_SectorAddress(pbyAddress);
 984  0197 c7                	clrb	
 985  0198 6c82              	std	OFST-5,s
 986                         ; 627         for (bycopy_index = 0; bycopy_index < DFLASH_WORDS_PER_SECTOR; bycopy_index++)
 988  019a 18c7              	clry	
 989  019c 6d80              	sty	OFST-7,s
 990  019e                   L344:
 991                         ; 630            uDFlashSectorBuffer.SectorData.pbyWords[bycopy_index] = *(unsigned int *)pbysector_address;
 993  019e 1858              	lsly	
 994  01a0 ee82              	ldx	OFST-5,s
 995  01a2 180200ea0011      	movw	0,x,L75_uDFlashSectorBuffer+4,y
 996                         ; 631            pbysector_address++;
 998  01a8 ed82              	ldy	OFST-5,s
 999  01aa 1942              	leay	2,y
1000  01ac 6d82              	sty	OFST-5,s
1001                         ; 627         for (bycopy_index = 0; bycopy_index < DFLASH_WORDS_PER_SECTOR; bycopy_index++)
1003  01ae 186280            	incw	OFST-7,s
1006  01b1 ed80              	ldy	OFST-7,s
1007  01b3 8d0080            	cpy	#128
1008  01b6 25e6              	blo	L344
1009                         ; 635         sEEPendingWrite.byBytesWritten = 0;
1011  01b8 87                	clra	
1012  01b9 7a0119            	staa	L55_sEEPendingWrite+7
1013                         ; 641         for(bycopy_index = EE_AddressSectorOffset(pbyAddress); 
1015  01bc e685              	ldab	OFST-2,s
1016  01be 6c80              	std	OFST-7,s
1018  01c0 fe0116            	ldx	L55_sEEPendingWrite+4
1019  01c3 b746              	tfr	d,y
1020  01c5 200e              	bra	L554
1021  01c7                   L154:
1022                         ; 645             uDFlashSectorBuffer.SectorData.pbyBytes[bycopy_index] = *(sEEPendingWrite.pbyData);
1024  01c7 180a00ea0011      	movb	0,x,L75_uDFlashSectorBuffer+4,y
1025                         ; 650             sEEPendingWrite.pbyData++;
1027  01cd 08                	inx	
1028                         ; 653             sEEPendingWrite.byBytesWritten++;
1030  01ce 720119            	inc	L55_sEEPendingWrite+7
1031                         ; 656             sEEPendingWrite.byLength--;
1033  01d1 730118            	dec	L55_sEEPendingWrite+6
1034                         ; 642             ((bycopy_index < DFLASH_BYTES_PER_SECTOR) && (sEEPendingWrite.byLength > 0)); bycopy_index++)
1036  01d4 02                	iny	
1037  01d5                   L554:
1038                         ; 641         for(bycopy_index = EE_AddressSectorOffset(pbyAddress); 
1038                         ; 642             ((bycopy_index < DFLASH_BYTES_PER_SECTOR) && (sEEPendingWrite.byLength > 0)); bycopy_index++)
1040  01d5 8d0100            	cpy	#256
1041  01d8 2405              	bhs	LC003
1043  01da f60118            	ldab	L55_sEEPendingWrite+6
1044  01dd 26e8              	bne	L154
1045  01df                   LC003:
1046  01df 7e0116            	stx	L55_sEEPendingWrite+4
1047                         ; 660         uDFlashSectorBuffer.SectorAddr.ulWhole = DFlash_GetSectorAddr(sEEPendingWrite.lAddress);
1049  01e2 b60114            	ldaa	L55_sEEPendingWrite+2
1050  01e5 fe0112            	ldx	L55_sEEPendingWrite
1051  01e8 c7                	clrb	
1052  01e9 7c000f            	std	L75_uDFlashSectorBuffer+2
1053  01ec 7e000d            	stx	L75_uDFlashSectorBuffer
1054                         ; 663         uDFlashSectorBuffer.DFlash_buffer_index = 0;
1056  01ef 790111            	clr	L75_uDFlashSectorBuffer+260
1057                         ; 665         byresult = TRUE;
1059  01f2 52                	incb	
1061  01f3                   L144:
1062                         ; 669         byresult = FALSE;
1064                         ; 672     return byresult;
1068  01f3 1b87              	leas	7,s
1069  01f5 0a                	rtc	
1097                         ; 689 void EE_WriteSequenceInitiated(void)
1097                         ; 690 {
1098                         	switch	.ftext
1099  01f6                   L31f_EE_WriteSequenceInitiated:
1103                         ; 693     if (Flash_IsCommandComplete())
1105  01f6 1f00008024        	brclr	__FSTAT,128,L574
1106                         ; 696         sFCCOBBlock.bFlashCmd = ERASE_D_FLASH_SECTOR;
1108  01fb c612              	ldab	#18
1109  01fd 7b0000            	stab	L36_sFCCOBBlock
1110                         ; 697         sFCCOBBlock.bGlblAddr_Bit22To16 = uDFlashSectorBuffer.SectorAddr.pbyBytes[1];
1112  0200 180c000e0001      	movb	L75_uDFlashSectorBuffer+1,L36_sFCCOBBlock+1
1113                         ; 698         sFCCOBBlock.wGlblAddr_Bit15To0 = uDFlashSectorBuffer.SectorAddr.sWordWise.wLowerHalf;
1115  0206 1804000f0002      	movw	L75_uDFlashSectorBuffer+2,L36_sFCCOBBlock+2
1116                         ; 701         if (EE_SubmitCommand())
1118  020c 4a00e1e1          	call	L5f_EE_SubmitCommand
1120  0210 044105            	tbeq	b,L774
1121                         ; 705             EE_SetNextStateTo(EE_WAIT_FOR_ERASE_COMPLETION);
1123  0213 cc0002            	ldd	#2
1126  0216 2003              	bra	LC004
1127  0218                   L774:
1128                         ; 711             EE_SetNextStateTo(EE_RETRY);
1130  0218 cc0004            	ldd	#4
1131  021b                   LC004:
1132  021b 4a00dddd          	call	L11f_EE_SetNextStateTo
1134  021f                   L574:
1135                         ; 714 }
1138  021f 0a                	rtc	
1165                         ; 734 void EE_Burst_Write (void)
1165                         ; 735 {
1166                         	switch	.ftext
1167  0220                   L52f_EE_Burst_Write:
1171                         ; 737     if (Flash_IsCommandComplete())
1173  0220 f60000            	ldab	__FSTAT
1174  0223 c480              	andb	#128
1175  0225 c180              	cmpb	#128
1176  0227 2674              	bne	L315
1177                         ; 741         sFCCOBBlock.bFlashCmd = PROGRAM_D_FLASH_WORDS;
1179  0229 c611              	ldab	#17
1180  022b 7b0000            	stab	L36_sFCCOBBlock
1181                         ; 742         sFCCOBBlock.bGlblAddr_Bit22To16 = uDFlashSectorBuffer.SectorAddr.pbyBytes[1];
1183  022e 180c000e0001      	movb	L75_uDFlashSectorBuffer+1,L36_sFCCOBBlock+1
1184                         ; 745         if (uDFlashSectorBuffer.DFlash_buffer_index <= (DFLASH_WORDS_PER_SECTOR - DFLASH_WORDS_PER_WRITE))
1186  0234 f60111            	ldab	L75_uDFlashSectorBuffer+260
1187  0237 c17c              	cmpb	#124
1188  0239 225b              	bhi	L515
1189                         ; 750             sFCCOBBlock.wGlblAddr_Bit15To0 = uDFlashSectorBuffer.SectorAddr.sWordWise.wLowerHalf + (uDFlashSectorBuffer.DFlash_buffer_index << 1);
1191  023b 87                	clra	
1192  023c 59                	lsld	
1193  023d f3000f            	addd	L75_uDFlashSectorBuffer+2
1194  0240 7c0002            	std	L36_sFCCOBBlock+2
1195                         ; 751             sFCCOBBlock.wData0 = uDFlashSectorBuffer.SectorData.pbyWords[uDFlashSectorBuffer.DFlash_buffer_index];
1197  0243 f60111            	ldab	L75_uDFlashSectorBuffer+260
1198  0246 87                	clra	
1199  0247 59                	lsld	
1200  0248 b746              	tfr	d,y
1201  024a 1805ea00110004    	movw	L75_uDFlashSectorBuffer+4,y,L36_sFCCOBBlock+4
1202                         ; 752             uDFlashSectorBuffer.DFlash_buffer_index++;
1204  0251 720111            	inc	L75_uDFlashSectorBuffer+260
1205                         ; 753             sFCCOBBlock.wData1 = uDFlashSectorBuffer.SectorData.pbyWords[uDFlashSectorBuffer.DFlash_buffer_index] ;
1207  0254 f60111            	ldab	L75_uDFlashSectorBuffer+260
1208  0257 87                	clra	
1209  0258 59                	lsld	
1210  0259 b746              	tfr	d,y
1211  025b 1805ea00110006    	movw	L75_uDFlashSectorBuffer+4,y,L36_sFCCOBBlock+6
1212                         ; 754             uDFlashSectorBuffer.DFlash_buffer_index++;
1214  0262 720111            	inc	L75_uDFlashSectorBuffer+260
1215                         ; 755             sFCCOBBlock.wData2 = uDFlashSectorBuffer.SectorData.pbyWords[uDFlashSectorBuffer.DFlash_buffer_index] ;
1217  0265 f60111            	ldab	L75_uDFlashSectorBuffer+260
1218  0268 87                	clra	
1219  0269 59                	lsld	
1220  026a b746              	tfr	d,y
1221  026c 1805ea00110008    	movw	L75_uDFlashSectorBuffer+4,y,L36_sFCCOBBlock+8
1222                         ; 756             uDFlashSectorBuffer.DFlash_buffer_index++;
1224  0273 720111            	inc	L75_uDFlashSectorBuffer+260
1225                         ; 757             sFCCOBBlock.wData3 = uDFlashSectorBuffer.SectorData.pbyWords[uDFlashSectorBuffer.DFlash_buffer_index] ;
1227  0276 f60111            	ldab	L75_uDFlashSectorBuffer+260
1228  0279 87                	clra	
1229  027a 59                	lsld	
1230  027b b746              	tfr	d,y
1231  027d 1805ea0011000a    	movw	L75_uDFlashSectorBuffer+4,y,L36_sFCCOBBlock+10
1232                         ; 758             uDFlashSectorBuffer.DFlash_buffer_index++;
1234  0284 720111            	inc	L75_uDFlashSectorBuffer+260
1235                         ; 765             if (!EE_SubmitCommand())
1237  0287 4a00e1e1          	call	L5f_EE_SubmitCommand
1239  028b 04610f            	tbne	b,L315
1240                         ; 769                 uDFlashSectorBuffer.DFlash_buffer_index = 0;
1242  028e 790111            	clr	L75_uDFlashSectorBuffer+260
1243                         ; 771                 EE_SetNextStateTo(EE_RETRY);
1245  0291 cc0004            	ldd	#4
1247  0294 2003              	bra	LC005
1248  0296                   L515:
1249                         ; 779             EE_SetNextStateTo(EE_WAIT_FOR_WRITE_COMPLETION);
1251  0296 cc0003            	ldd	#3
1252  0299                   LC005:
1253  0299 4a00dddd          	call	L11f_EE_SetNextStateTo
1255  029d                   L315:
1256                         ; 782 }
1259  029d 0a                	rtc	
1319                         ; 798 void EE_WaitForWriteCompletion(void)
1319                         ; 799 {
1320                         	switch	.ftext
1321  029e                   L51f_EE_WaitForWriteCompletion:
1323  029e 1b9b              	leas	-5,s
1324       00000005          OFST:	set	5
1327                         ; 806     if(Flash_IsCommandComplete())
1329  02a0 f60000            	ldab	__FSTAT
1330  02a3 c480              	andb	#128
1331  02a5 c180              	cmpb	#128
1332  02a7 267d              	bne	L745
1333                         ; 810         EE_SetEPAGE(sEEPendingWrite.lAddress);
1335  02a9 fc0114            	ldd	L55_sEEPendingWrite+2
1336  02ac fe0112            	ldx	L55_sEEPendingWrite
1337  02af 4a037575          	call	L72f_GetEPage
1339  02b3 7b0000            	stab	__EPAGE
1340                         ; 811         pbyAddress = EE_GetLogicalAddr(sEEPendingWrite.lAddress);
1342  02b6 fc0114            	ldd	L55_sEEPendingWrite+2
1343  02b9 fe0112            	ldx	L55_sEEPendingWrite
1344  02bc 4a037e7e          	call	L13f_GetEOffset
1346  02c0 c30800            	addd	#2048
1347  02c3 6c83              	std	OFST-2,s
1348                         ; 814         pbysector_address = EE_SectorAddress(pbyAddress);
1350  02c5 c7                	clrb	
1351  02c6 6c81              	std	OFST-4,s
1352                         ; 817         endloop = FALSE;
1354  02c8 6980              	clr	OFST-5,s
1355                         ; 818         for (uDFlashSectorBuffer.DFlash_buffer_index = 0; 
1357  02ca 790111            	clr	L75_uDFlashSectorBuffer+260
1359  02cd 2019              	bra	L555
1360  02cf                   L155:
1361                         ; 822            if (uDFlashSectorBuffer.SectorData.pbyWords[uDFlashSectorBuffer.DFlash_buffer_index] != *(unsigned int *)pbysector_address)
1363  02cf 87                	clra	
1364  02d0 59                	lsld	
1365  02d1 b746              	tfr	d,y
1366  02d3 ecea0011          	ldd	L75_uDFlashSectorBuffer+4,y
1367  02d7 ed81              	ldy	OFST-4,s
1368  02d9 ac40              	cpd	0,y
1369  02db 2704              	beq	L165
1370                         ; 824               endloop = TRUE;
1372  02dd c601              	ldab	#1
1373  02df 6b80              	stab	OFST-5,s
1374  02e1                   L165:
1375                         ; 826            pbysector_address++;
1377  02e1 1942              	leay	2,y
1378  02e3 6d81              	sty	OFST-4,s
1379                         ; 819              ((uDFlashSectorBuffer.DFlash_buffer_index < DFLASH_WORDS_PER_SECTOR) && (endloop == FALSE)); 
1379                         ; 820              (uDFlashSectorBuffer.DFlash_buffer_index++))
1381  02e5 720111            	inc	L75_uDFlashSectorBuffer+260
1382  02e8                   L555:
1383                         ; 818         for (uDFlashSectorBuffer.DFlash_buffer_index = 0; 
1383                         ; 819              ((uDFlashSectorBuffer.DFlash_buffer_index < DFLASH_WORDS_PER_SECTOR) && (endloop == FALSE)); 
1385  02e8 f60111            	ldab	L75_uDFlashSectorBuffer+260
1386  02eb c180              	cmpb	#128
1387  02ed 2404              	bhs	L365
1389  02ef e780              	tst	OFST-5,s
1390  02f1 27dc              	beq	L155
1391  02f3                   L365:
1392                         ; 830         if (endloop == TRUE)
1394  02f3 e680              	ldab	OFST-5,s
1395  02f5 042105            	dbne	b,L565
1396                         ; 833             EE_SetNextStateTo(EE_RETRY);
1398  02f8 cc0004            	ldd	#4
1401  02fb 2025              	bra	LC006
1402  02fd                   L565:
1403                         ; 837             if(sEEPendingWrite.byLength != 0)
1405  02fd f60118            	ldab	L55_sEEPendingWrite+6
1406  0300 271f              	beq	L175
1407                         ; 840                 sEEPendingWrite.lAddress += sEEPendingWrite.byBytesWritten;
1409  0302 f60119            	ldab	L55_sEEPendingWrite+7
1410  0305 87                	clra	
1411  0306 1887              	clrx	
1412  0308 f30114            	addd	L55_sEEPendingWrite+2
1413  030b 18b90112          	adex	L55_sEEPendingWrite
1414  030f 7c0114            	std	L55_sEEPendingWrite+2
1415  0312 7e0112            	stx	L55_sEEPendingWrite
1416                         ; 843                 if (EE_PrepDataForWrite())
1418  0315 4a017474          	call	L3f_EE_PrepDataForWrite
1420  0319 04410a            	tbeq	b,L745
1421                         ; 845                    EE_SetNextStateTo(EE_WRITE_SEQUENCE_INITIATED);
1423  031c cc0001            	ldd	#1
1425  031f 2001              	bra	LC006
1426  0321                   L175:
1427                         ; 850                 EE_SetNextStateTo(EE_IDLE);
1429  0321 87                	clra	
1430  0322                   LC006:
1431  0322 4a00dddd          	call	L11f_EE_SetNextStateTo
1433  0326                   L745:
1434                         ; 854 }
1437  0326 1b85              	leas	5,s
1438  0328 0a                	rtc	
1464                         ; 867 void EE_Retry(void)
1464                         ; 868 {
1465                         	switch	.ftext
1466  0329                   L71f_EE_Retry:
1470                         ; 870     if (Flash_IsProtectionViolation())
1472  0329 1f00001004        	brclr	__FSTAT,16,L706
1473                         ; 872        Flash_ClearProtectionViolation();
1475  032e 1c000010          	bset	__FSTAT,16
1476  0332                   L706:
1477                         ; 875     if (Flash_IsAccessError())
1479  0332 1f00002004        	brclr	__FSTAT,32,L116
1480                         ; 877        Flash_ClearAccessError();
1482  0337 1c000020          	bset	__FSTAT,32
1483  033b                   L116:
1484                         ; 881     if(Flash_IsCommandComplete())
1486  033b 1f00008032        	brclr	__FSTAT,128,L316
1487                         ; 886         if (Flash_IsProtectionViolation())
1489  0340 1f00001004        	brclr	__FSTAT,16,L516
1490                         ; 888             Flash_ClearProtectionViolation();
1492  0345 1c000010          	bset	__FSTAT,16
1493  0349                   L516:
1494                         ; 891         if (Flash_IsAccessError())
1496  0349 1f00002004        	brclr	__FSTAT,32,L716
1497                         ; 893             Flash_ClearAccessError();
1499  034e 1c000020          	bset	__FSTAT,32
1500  0352                   L716:
1501                         ; 896         if(sEEPendingWrite.byRetryCount < EE_MAX_RETRY)
1503  0352 f6011a            	ldab	L55_sEEPendingWrite+8
1504  0355 c103              	cmpb	#3
1505  0357 240f              	bhs	L126
1506                         ; 899             sEEPendingWrite.byRetryCount++;
1508  0359 72011a            	inc	L55_sEEPendingWrite+8
1509                         ; 901             EE_ErrorHook();
1511  035c 4a037373          	call	L7f_EE_ErrorHook
1513                         ; 903             EE_SetNextStateTo(EE_WRITE_SEQUENCE_INITIATED);
1515  0360 cc0001            	ldd	#1
1516  0363 4a00dddd          	call	L11f_EE_SetNextStateTo
1520  0367 0a                	rtc	
1521  0368                   L126:
1522                         ; 908             EE_SetNextStateTo(EE_IDLE);
1524  0368 87                	clra	
1525  0369 c7                	clrb	
1526  036a 4a00dddd          	call	L11f_EE_SetNextStateTo
1528                         ; 910             EE_ErrorHook();
1530  036e 4a037373          	call	L7f_EE_ErrorHook
1532  0372                   L316:
1533                         ; 913 }
1536  0372 0a                	rtc	
1559                         ; 925 void EE_ErrorHook(void)
1559                         ; 926 {
1560                         	switch	.ftext
1561  0373                   L7f_EE_ErrorHook:
1565                         ; 927     _asm("nop");
1568  0373 a7                	nop	
1570                         ; 928 }
1573  0374 0a                	rtc	
1604                         ; 938 unsigned char GetEPage( unsigned long GlobalAddress )
1604                         ; 939 {
1605                         	switch	.ftext
1606  0375                   L72f_GetEPage:
1608  0375 3b                	pshd	
1609  0376 34                	pshx	
1610       00000000          OFST:	set	0
1613                         ; 940     return ((S12XEepromType*)&GlobalAddress)->epage;
1615  0377 ec81              	ldd	OFST+1,s
1616  0379 49                	lsrd	
1617  037a 49                	lsrd	
1620  037b 1b84              	leas	4,s
1621  037d 0a                	rtc	
1652                         ; 951 unsigned int GetEOffset( unsigned long GlobalAddress )
1652                         ; 952 {
1653                         	switch	.ftext
1654  037e                   L13f_GetEOffset:
1656       00000000          OFST:	set	0
1659                         ; 953     return ((S12XEepromType*)&GlobalAddress)->localOffset;
1661  037e 8403              	anda	#3
1664  0380 0a                	rtc	
1767                         ; 965 void WriteFBLDFlashByte(unsigned long mainCopyDFlashAddr, unsigned char* dataToWrite,unsigned char TotalSize)
1767                         ; 966 {
1768                         	switch	.ftext
1769  0381                   f_WriteFBLDFlashByte:
1771  0381 3b                	pshd	
1772  0382 34                	pshx	
1773  0383 1bf135            	leas	-203,s
1774       000000cb          OFST:	set	203
1777                         ; 973     unsigned char total_new = 0; //Harsha added
1779  0386 69f0c9            	clr	OFST-2,s
1780                         ; 974     Epage_tmp = EPAGE;
1782  0389 f60000            	ldab	__EPAGE
1783  038c 6bf0ca            	stab	OFST-1,s
1784                         ; 977     EE_SetEPAGE(mainCopyDFlashAddr);
1786  038f ecf0cd            	ldd	OFST+2,s
1787  0392 eef0cb            	ldx	OFST+0,s
1788  0395 4a037575          	call	L72f_GetEPage
1790  0399 7b0000            	stab	__EPAGE
1791                         ; 979     pbyAddress = EE_GetLogicalAddr(mainCopyDFlashAddr);
1793  039c ecf0cd            	ldd	OFST+2,s
1794  039f eef0cb            	ldx	OFST+0,s
1795  03a2 4a037e7e          	call	L13f_GetEOffset
1797  03a6 c30800            	addd	#2048
1798  03a9 6cf0c7            	std	OFST-4,s
1799                         ; 981     StartAddress = EE_GetLogicalAddr(DFlash_GetSectorAddr(mainCopyDFlashAddr));
1801  03ac a6f0cd            	ldaa	OFST+2,s
1802  03af eef0cb            	ldx	OFST+0,s
1803  03b2 c7                	clrb	
1804  03b3 4a037e7e          	call	L13f_GetEOffset
1806  03b7 c30800            	addd	#2048
1807  03ba 6cf0c5            	std	OFST-6,s
1808                         ; 984     buffer_tmp[FBL_CHECKSUM_SECTOR_OFFSET] = 0;
1810  03bd 69f0c4            	clr	OFST-7,s
1811                         ; 986     for (index_tmp = 0; index_tmp < FBL_CHECKSUM_SECTOR_OFFSET; index_tmp++)
1813  03c0 6980              	clr	OFST-203,s
1814  03c2                   L137:
1815                         ; 988         if (pbyAddress != StartAddress)
1817  03c2 ecf0c7            	ldd	OFST-4,s
1818  03c5 acf0c5            	cpd	OFST-6,s
1819  03c8 270f              	beq	L737
1820                         ; 990            buffer_tmp[index_tmp] = *(unsigned char *)StartAddress;
1822  03ca e680              	ldab	OFST-203,s
1823  03cc 87                	clra	
1824  03cd c30001            	addd	#-202+OFST
1825  03d0 edf0c5            	ldy	OFST-6,s
1826  03d3 180a40f6          	movb	0,y,d,s
1828  03d7 2027              	bra	L147
1829  03d9                   L737:
1830                         ; 995         	buffer_tmp[index_tmp] = *(unsigned char *)dataToWrite; //Harsha added 
1832  03d9 e680              	ldab	OFST-203,s
1833  03db 87                	clra	
1834  03dc c30001            	addd	#-202+OFST
1835  03df edf0d2            	ldy	OFST+7,s
1836  03e2 180a40f6          	movb	0,y,d,s
1837                         ; 996           if((TotalSize > 1) && (total_new < TotalSize))
1839  03e6 e6f0d5            	ldab	OFST+10,s
1840  03e9 c101              	cmpb	#1
1841  03eb 2313              	bls	L147
1843  03ed e6f0c9            	ldab	OFST-2,s
1844  03f0 e1f0d5            	cmpb	OFST+10,s
1845  03f3 240b              	bhs	L147
1846                         ; 998         	  pbyAddress++; 
1848  03f5 1862f0c7          	incw	OFST-4,s
1849                         ; 999         	  dataToWrite++;
1851  03f9 02                	iny	
1852  03fa 6df0d2            	sty	OFST+7,s
1853                         ; 1000         	  total_new++;
1855  03fd 62f0c9            	inc	OFST-2,s
1856  0400                   L147:
1857                         ; 1005         buffer_tmp[FBL_CHECKSUM_SECTOR_OFFSET] += buffer_tmp[index_tmp];
1859  0400 e680              	ldab	OFST-203,s
1860  0402 87                	clra	
1861  0403 c30001            	addd	#-202+OFST
1862  0406 e6f6              	ldab	d,s
1863  0408 ebf0c4            	addb	OFST-7,s
1864  040b 6bf0c4            	stab	OFST-7,s
1865                         ; 1007         StartAddress++;
1867  040e 1862f0c5          	incw	OFST-6,s
1868                         ; 986     for (index_tmp = 0; index_tmp < FBL_CHECKSUM_SECTOR_OFFSET; index_tmp++)
1870  0412 6280              	inc	OFST-203,s
1873  0414 e680              	ldab	OFST-203,s
1874  0416 c1c3              	cmpb	#195
1875  0418 25a8              	blo	L137
1876                         ; 1010     EPAGE = Epage_tmp;
1878  041a 180df0ca0000      	movb	OFST-1,s,__EPAGE
1879                         ; 1012     EE_BlockWrite(EE_FBL_TOTAL, (unsigned char *)&buffer_tmp[0]);
1881  0420 1a81              	leax	OFST-202,s
1882  0422 34                	pshx	
1883  0423 cc0031            	ldd	#49
1884  0426 4a000000          	call	f_EE_BlockWrite
1886                         ; 1018 }
1889  042a 1bf0d1            	leas	209,s
1890  042d 0a                	rtc	
1955                         .const:	section	.data
1956                         	even
1957  0000                   L25:
1958  0000 00000100          	dc.l	256
1959                         ; 1029 unsigned char ReadFBLDFlash(unsigned long dflashMainCopyAddr, unsigned char* readData, unsigned char size)
1959                         ; 1030 {
1960                         	switch	.ftext
1961  042e                   f_ReadFBLDFlash:
1963  042e 3b                	pshd	
1964  042f 34                	pshx	
1965  0430 3b                	pshd	
1966       00000002          OFST:	set	2
1969                         ; 1035     return_value = TRUE;
1971  0431 c601              	ldab	#1
1972  0433 6b81              	stab	OFST-1,s
1973                         ; 1038     GoodFBLDFlashChecksum = VerifyFBLDFlashChecksum(FBL_DFLASH_MAIN_COPY_START_ADDR);
1975  0435 ce0010            	ldx	#16
1976  0438 cc1f00            	ldd	#7936
1977  043b 4a047e7e          	call	L54f_VerifyFBLDFlashChecksum
1979  043f 6b80              	stab	OFST-2,s
1980                         ; 1041     if (GoodFBLDFlashChecksum == TRUE)
1982  0441 042108            	dbne	b,L377
1983                         ; 1044        EepromDriver_RReadSync(readData, size, dflashMainCopyAddr);
1985  0444 ec84              	ldd	OFST+2,s
1986  0446 3b                	pshd	
1987  0447 ec84              	ldd	OFST+2,s
1988  0449 3b                	pshd	
1991  044a 201c              	bra	LC007
1992  044c                   L377:
1993                         ; 1050        GoodFBLDFlashChecksum = VerifyFBLDFlashChecksum((FBL_DFLASH_MAIN_COPY_START_ADDR - FBL_DFLASH_BACKUP_OFFSET));
1995  044c ce0010            	ldx	#16
1996  044f cc1e00            	ldd	#7680
1997  0452 4a047e7e          	call	L54f_VerifyFBLDFlashChecksum
1999  0456 6b80              	stab	OFST-2,s
2000                         ; 1052        if  (GoodFBLDFlashChecksum == TRUE)
2002  0458 04211c            	dbne	b,L777
2003                         ; 1055           EepromDriver_RReadSync(readData, size, (dflashMainCopyAddr - FBL_DFLASH_BACKUP_OFFSET));
2005  045b ec84              	ldd	OFST+2,s
2006  045d ee82              	ldx	OFST+0,s
2007  045f b30002            	subd	L25+2
2008  0462 18b20000          	sbex	L25
2009  0466 3b                	pshd	
2010  0467 34                	pshx	
2012  0468                   LC007:
2013  0468 e6f010            	ldab	OFST+14,s
2014  046b 87                	clra	
2015  046c 3b                	pshd	
2016  046d ec8f              	ldd	OFST+13,s
2017  046f 4a004949          	call	L14f_EepromDriver_RReadSync
2018  0473 1b86              	leas	6,s
2020  0475 2002              	bra	L577
2021  0477                   L777:
2022                         ; 1061           return_value = FALSE;
2024  0477 6981              	clr	OFST-1,s
2025  0479                   L577:
2026                         ; 1065     return return_value;
2028  0479 e681              	ldab	OFST-1,s
2031  047b 1b86              	leas	6,s
2032  047d 0a                	rtc	
2100                         ; 1078 unsigned char VerifyFBLDFlashChecksum(unsigned long dflashSectorStartAddr)
2100                         ; 1079 {
2101                         	switch	.ftext
2102  047e                   L54f_VerifyFBLDFlashChecksum:
2104  047e 3b                	pshd	
2105  047f 34                	pshx	
2106  0480 1b9b              	leas	-5,s
2107       00000005          OFST:	set	5
2110                         ; 1085     Epage_tmp = EPAGE;
2112  0482 f60000            	ldab	__EPAGE
2113  0485 6b82              	stab	OFST-3,s
2114                         ; 1088     EE_SetEPAGE(dflashSectorStartAddr);
2116  0487 ec87              	ldd	OFST+2,s
2117  0489 ee85              	ldx	OFST+0,s
2118  048b 4a037575          	call	L72f_GetEPage
2120  048f 7b0000            	stab	__EPAGE
2121                         ; 1089     pbyAddress = EE_GetLogicalAddr(dflashSectorStartAddr);
2123  0492 ec87              	ldd	OFST+2,s
2124  0494 ee85              	ldx	OFST+0,s
2125  0496 4a037e7e          	call	L13f_GetEOffset
2127  049a c30800            	addd	#2048
2128  049d 6c80              	std	OFST-5,s
2129                         ; 1092     CalcChecksum_tmp = Calc_Checksum(pbyAddress, FBL_CHECKSUM_SECTOR_OFFSET);
2131  049f cc00c3            	ldd	#195
2132  04a2 3b                	pshd	
2133  04a3 ec82              	ldd	OFST-3,s
2134  04a5 4a04c5c5          	call	L74f_Calc_Checksum
2136  04a9 1b82              	leas	2,s
2137  04ab 6b84              	stab	OFST-1,s
2138                         ; 1094     StoredChecksum_tmp = *(unsigned char *)(pbyAddress + FBL_CHECKSUM_SECTOR_OFFSET);
2140  04ad ed80              	ldy	OFST-5,s
2141  04af 180ae8c383        	movb	195,y,OFST-2,s
2142                         ; 1096     EPAGE = Epage_tmp;
2144  04b4 180d820000        	movb	OFST-3,s,__EPAGE
2145                         ; 1098     return (CalcChecksum_tmp == StoredChecksum_tmp);
2147  04b9 e183              	cmpb	OFST-2,s
2148  04bb 2604              	bne	L65
2149  04bd c601              	ldab	#1
2150  04bf 2001              	bra	L06
2151  04c1                   L65:
2152  04c1 c7                	clrb	
2153  04c2                   L06:
2156  04c2 1b89              	leas	9,s
2157  04c4 0a                	rtc	
2212                         ; 1110 unsigned char Calc_Checksum(unsigned char * StartAddress, unsigned char Size)
2212                         ; 1111 {
2213                         	switch	.ftext
2214  04c5                   L74f_Calc_Checksum:
2216  04c5 3b                	pshd	
2217  04c6 3b                	pshd	
2218       00000002          OFST:	set	2
2221                         ; 1115     checksum_tmp = *(unsigned char *)StartAddress;
2223  04c7 b746              	tfr	d,y
2224  04c9 180a4081          	movb	0,y,OFST-1,s
2225                         ; 1116     for (index_tmp = 1; index_tmp < Size; index_tmp++)
2227  04cd c601              	ldab	#1
2228  04cf 6b80              	stab	OFST-2,s
2230  04d1 ed82              	ldy	OFST+0,s
2231  04d3 200b              	bra	L1601
2232  04d5                   L5501:
2233                         ; 1119         checksum_tmp += *(unsigned char *)(StartAddress + index_tmp);
2235  04d5 87                	clra	
2236  04d6 e6ee              	ldab	d,y
2237  04d8 eb81              	addb	OFST-1,s
2238  04da 6b81              	stab	OFST-1,s
2239                         ; 1116     for (index_tmp = 1; index_tmp < Size; index_tmp++)
2241  04dc 6280              	inc	OFST-2,s
2242  04de e680              	ldab	OFST-2,s
2243  04e0                   L1601:
2246  04e0 e188              	cmpb	OFST+6,s
2247  04e2 25f1              	blo	L5501
2248                         ; 1122     return checksum_tmp;
2250  04e4 e681              	ldab	OFST-1,s
2253  04e6 1b84              	leas	4,s
2254  04e8 0a                	rtc	
2528                         	switch	.bss
2529  0000                   L36_sFCCOBBlock:
2530  0000 0000000000000000  	ds.b	12
2531  000c                   L16_eEECurrentState:
2532  000c 00                	ds.b	1
2533  000d                   L75_uDFlashSectorBuffer:
2534  000d 0000000000000000  	ds.b	261
2535  0112                   L55_sEEPendingWrite:
2536  0112 0000000000000000  	ds.b	9
2537                         	xref	f_EE_BlockWrite
2538                         	xdef	f_ReadFBLDFlash
2539                         	xdef	f_WriteFBLDFlashByte
2540                         	xdef	f_EepromDriver_InitSync
2541                         	xref	__FCCOB
2542                         	xref	__FSTAT
2543                         	xref	__FERCNFG
2544                         	xref	__FCNFG
2545                         	xref	__FCCOBIX
2546                         	xref	__FCLKDIV
2547                         	xref	__ARMCOP
2548                         	xref	__EPAGE
2549                         	xref	__GPAGE
2570                         	end
