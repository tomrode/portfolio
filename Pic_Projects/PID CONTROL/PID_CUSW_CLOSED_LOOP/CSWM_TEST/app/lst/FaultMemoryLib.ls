   1                         ; C Compiler for S12XE [COSMIC Software].
   2                         ; Generator V4.7.8 - 27 Jul 2007
   3                         ; Optimizer V4.6.13 - 27 Jul 2007
 345                         ; 208 @far void FMemLibF_Init(void)
 345                         ; 209 {
 346                         .ftext:	section	.text
 347  0000                   f_FMemLibF_Init:
 351                         ; 211 	fault_update_case_c = 0;
 353  0000 790000            	clr	_fault_update_case_c
 354                         ; 212 	fmem_nm_status_c = 0;
 356  0003 790005            	clr	_fmem_nm_status_c
 357                         ; 214 	d_DtcSettingModeEnabled_t = TRUE;
 359  0006 c601              	ldab	#1
 360  0008 7b0015            	stab	_d_DtcSettingModeEnabled_t
 361                         ; 215 	d_HistInterrogRecordEnable_t = FALSE;
 363  000b 790004            	clr	_d_HistInterrogRecordEnable_t
 364                         ; 216 	d_OperationCycle_c = OPERATION_CYLE_ON;
 366  000e 7b0014            	stab	_d_OperationCycle_c
 367                         ; 220 	d_IgnitionStart5secTimer_c = D_5_SEC_K;
 369  0011 c610              	ldab	#16
 370  0013 7b0016            	stab	_d_IgnitionStart5secTimer_c
 371                         ; 222 	ecu_1min_counter_c = CLEAR_COUNTER; //Clear the one min increment counter for updating the  ECU Life time variable in RAM
 373  0016 79000a            	clr	_ecu_1min_counter_c
 374                         ; 223 	ecu_keyon_counter_c = CLEAR_COUNTER; //Clear the 15sec increment counter for updating the ECU KEY ON time variable in RAM
 376  0019 790009            	clr	_ecu_keyon_counter_c
 377                         ; 224 	ecu_life_counter_c = CLEAR_COUNTER; //Clear the 5 min counter for updating ECU Life time and KEY ON in EEPROM
 379  001c 790008            	clr	_ecu_life_counter_c
 380                         ; 226 	fmem_keyon_time_w = CLEAR_COUNTER; // Start fresh timer to keep the value of KEY ON TImer for each IGN cycle
 382  001f 1879000d          	clrw	_fmem_keyon_time_w
 383                         ; 227 	EE_BlockRead(EE_ECU_TIMESTAMPS, (u_8Bit*)&fmem_lifetime_l); //Read the stored value of life time from EEPROM
 385  0023 cc000f            	ldd	#_fmem_lifetime_l
 386  0026 3b                	pshd	
 387  0027 cc0027            	ldd	#39
 388  002a 4a000000          	call	f_EE_BlockRead
 390                         ; 229 	EE_BlockRead(EE_ECU_KEYON_COUNTER, (u_8Bit*)&fmem_keyon_counter_w); //Read the stored value of IGN Cycles of ECU Life time
 392  002e cc0006            	ldd	#_fmem_keyon_counter_w
 393  0031 6c80              	std	0,s
 394  0033 cc0029            	ldd	#41
 395  0036 4a000000          	call	f_EE_BlockRead
 397  003a 1b82              	leas	2,s
 398                         ; 232 	FMemApplInit();
 400  003c 4a000000          	call	f_FMemApplInit
 402                         ; 234 }
 405  0040 0a                	rtc	
 680                         ; 351 @far void FMemLibF_CyclicDtcTask(void)
 680                         ; 352 {
 681                         	switch	.ftext
 682  0041                   f_FMemLibF_CyclicDtcTask:
 684  0041 1bf1e1            	leas	-31,s
 685       0000001f          OFST:	set	31
 688                         ; 353     unsigned char temp_recent_conf_dtc, DTCMemIndex = 0; //Because of Warning 676, this variable introduced.
 690  0044 6983              	clr	OFST-28,s
 691                         ; 355 	u_temp_l   Index, SearchResult, TransferDtcToFaultMemory, NumberOfRegisteredDtc, dtc_in_chrono = 0, dtc_in_hist = 0 ;
 695                         ; 365 	if (d_DtcSettingModeEnabled_t == TRUE)
 697  0046 f60015            	ldab	_d_DtcSettingModeEnabled_t
 698  0049 53                	decb	
 699  004a 1826041e          	bne	L143
 700                         ; 368 		if (d_IgnitionStart5secTimer_c != 0)
 702  004e f60016            	ldab	_d_IgnitionStart5secTimer_c
 703  0051 18260417          	bne	L143
 705                         ; 377 			if (dtc_loop_count_c < DTC_PROCESS_TIMES)
 707  0055 f60002            	ldab	_dtc_loop_count_c
 708  0058 c104              	cmpb	#4
 709  005a 241f              	bhs	L743
 710                         ; 380 				cyclic_dtc_count_c = (unsigned char)(dtc_loop_count_c * DTC_PROCESS_EACH_TIME);
 712  005c 860c              	ldaa	#12
 713  005e 12                	mul	
 714  005f 7b0003            	stab	_cyclic_dtc_count_c
 715                         ; 382 				no_of_dtc_c = cyclic_dtc_count_c + DTC_PROCESS_EACH_TIME;
 717  0062 cb0c              	addb	#12
 718  0064 7b0001            	stab	_no_of_dtc_c
 719                         ; 384 				dtc_loop_count_c++;
 721  0067 720002            	inc	_dtc_loop_count_c
 722                         ; 388 				if(dtc_loop_count_c == DTC_PROCESS_TIMES){
 724  006a f60002            	ldab	_dtc_loop_count_c
 725  006d c104              	cmpb	#4
 726  006f 2618              	bne	L353
 727                         ; 395 					no_of_dtc_c -= 2;
 729  0071 f60001            	ldab	_no_of_dtc_c
 730  0074 c002              	subb	#2
 731  0076 7b0001            	stab	_no_of_dtc_c
 732  0079 200e              	bra	L353
 733  007b                   L743:
 734                         ; 401 				dtc_loop_count_c = 0;
 736  007b 790002            	clr	_dtc_loop_count_c
 737                         ; 402 				cyclic_dtc_count_c = (unsigned char)(dtc_loop_count_c * DTC_PROCESS_EACH_TIME);
 739  007e 790003            	clr	_cyclic_dtc_count_c
 740                         ; 403 				no_of_dtc_c = cyclic_dtc_count_c + DTC_PROCESS_EACH_TIME;
 742  0081 c60c              	ldab	#12
 743  0083 7b0001            	stab	_no_of_dtc_c
 744                         ; 404 				dtc_loop_count_c++;
 746  0086 720002            	inc	_dtc_loop_count_c
 747  0089                   L353:
 748                         ; 407 			for (Index = cyclic_dtc_count_c; Index < no_of_dtc_c; Index++)
 750  0089 f60003            	ldab	_cyclic_dtc_count_c
 751  008c 87                	clra	
 752  008d 6c81              	std	OFST-30,s
 754  008f 182003cf          	bra	L163
 755  0093                   L553:
 756                         ; 412 				switch (d_AllDtc_a[Index].reported_state) {
 758  0093 ed81              	ldy	OFST-30,s
 759  0095 1858              	lsly	
 760  0097 e6ea0000          	ldab	_d_AllDtc_a,y
 761  009b 55                	rolb	
 762  009c 55                	rolb	
 763  009d 55                	rolb	
 764  009e c403              	andb	#3
 766  00a0 2708              	beq	L771
 767  00a2 040115            	dbeq	b,L102
 768  00a5 040145            	dbeq	b,L763
 769  00a8 2043              	bra	L763
 770  00aa                   L771:
 771                         ; 417 						d_AllDtc_a[Index].dtc_on_time = 0;
 773  00aa ed81              	ldy	OFST-30,s
 774  00ac 1858              	lsly	
 775  00ae 0dea00003f        	bclr	_d_AllDtc_a,y,63
 776                         ; 419 						d_AllDtc_a[Index].DtcState.state.TestFailedThisMonitoringCycle = FALSE;
 778                         ; 420 						d_AllDtc_a[Index].DtcState.state.Pending = FALSE;
 780  00b3 0dea000106        	bclr	_d_AllDtc_a+1,y,6
 781                         ; 422 						break;
 783  00b8 2033              	bra	L763
 784  00ba                   L102:
 785                         ; 428 						d_AllDtc_a[Index].dtc_on_time = FMemLibLF_IncCounter(d_AllDtc_a[Index].dtc_on_time, fmem_AllRegisteredDtc_t[Index].dtc_error_time);
 787  00ba ec81              	ldd	OFST-30,s
 788  00bc cd0006            	ldy	#6
 789  00bf 13                	emul	
 790  00c0 b746              	tfr	d,y
 791  00c2 e6ea0004          	ldab	_fmem_AllRegisteredDtc_t+4,y
 792  00c6 c43f              	andb	#63
 793  00c8 87                	clra	
 794  00c9 3b                	pshd	
 795  00ca ed83              	ldy	OFST-28,s
 796  00cc 1858              	lsly	
 797  00ce e6ea0000          	ldab	_d_AllDtc_a,y
 798  00d2 c43f              	andb	#63
 799  00d4 4a106a6a          	call	f_FMemLibLF_IncCounter
 801  00d8 1b82              	leas	2,s
 802  00da ed81              	ldy	OFST-30,s
 803  00dc 1858              	lsly	
 804  00de 0dea00003f        	bclr	_d_AllDtc_a,y,63
 805  00e3 c43f              	andb	#63
 806  00e5 eaea0000          	orab	_d_AllDtc_a,y
 807  00e9 6bea0000          	stab	_d_AllDtc_a,y
 808                         ; 429 						break;
 810  00ed                   L763:
 811                         ; 439 				if (d_AllDtc_a[Index].dtc_on_time >=  fmem_AllRegisteredDtc_t[Index].dtc_error_time)
 813  00ed ec81              	ldd	OFST-30,s
 814  00ef cd0006            	ldy	#6
 815  00f2 13                	emul	
 816  00f3 b746              	tfr	d,y
 817  00f5 e6ea0004          	ldab	_fmem_AllRegisteredDtc_t+4,y
 818  00f9 c43f              	andb	#63
 819  00fb 6b80              	stab	OFST-31,s
 820  00fd ed81              	ldy	OFST-30,s
 821  00ff 1858              	lsly	
 822  0101 e6ea0000          	ldab	_d_AllDtc_a,y
 823  0105 c43f              	andb	#63
 824  0107 e180              	cmpb	OFST-31,s
 825  0109 2507              	blo	L173
 826                         ; 441 					TransferDtcToFaultMemory = TRUE;
 828  010b cc0001            	ldd	#1
 829  010e 6c86              	std	OFST-25,s
 831  0110 2003              	bra	L373
 832  0112                   L173:
 833                         ; 445 					TransferDtcToFaultMemory = FALSE;
 835  0112 186986            	clrw	OFST-25,s
 836  0115                   L373:
 837                         ; 448 				if ((DtcMemoryIndex = FMemLibLF_SearchDtcIndexInDtcMemory((unsigned char)Index, FAULT_MEMORY)) >= 0)
 839  0115 87                	clra	
 840  0116 c7                	clrb	
 841  0117 3b                	pshd	
 842  0118 e684              	ldab	OFST-27,s
 843  011a 4a0ebdbd          	call	f_FMemLibLF_SearchDtcIndexInDtcMemory
 845  011e 1b82              	leas	2,s
 846  0120 6c88              	std	OFST-23,s
 847  0122 2d0b              	blt	L573
 848                         ; 450 					SearchResult = D_FOUNDED_K;
 850  0124 cc0010            	ldd	#16
 851  0127 6c8a              	std	OFST-21,s
 852                         ; 451 					DTCMemIndex = (unsigned char)DtcMemoryIndex; //Because of Warning 676, this variable introduced.
 854  0129 180a8983          	movb	OFST-22,s,OFST-28,s
 856  012d 2003              	bra	L773
 857  012f                   L573:
 858                         ; 455 					SearchResult = D_NOT_FOUNDED_K;
 860  012f 18698a            	clrw	OFST-21,s
 861  0132                   L773:
 862                         ; 458 				NumberOfRegisteredDtc = FMemLibLF_GetNumberOfRegisteredDtc(FAULT_MEMORY);
 864  0132 87                	clra	
 865  0133 c7                	clrb	
 866  0134 4a0e2929          	call	f_FMemLibLF_GetNumberOfRegisteredDtc
 868  0138 6c84              	std	OFST-27,s
 869                         ; 459 				switch (SearchResult | d_AllDtc_a[Index].reported_state) {
 871  013a ed81              	ldy	OFST-30,s
 872  013c 1858              	lsly	
 873  013e e6ea0000          	ldab	_d_AllDtc_a,y
 874  0142 55                	rolb	
 875  0143 55                	rolb	
 876  0144 55                	rolb	
 877  0145 c403              	andb	#3
 878  0147 ea8b              	orab	OFST-20,s
 879  0149 a68a              	ldaa	OFST-21,s
 881  014b 044415            	tbeq	d,L502
 882  014e 04041f            	dbeq	d,L702
 883  0151 83000f            	subd	#15
 884  0154 1827013a          	beq	L112
 885  0158 830001            	subd	#1
 886  015b 1827025f          	beq	L312
 887  015f 182002fc          	bra	L304
 888  0163                   L502:
 889                         ; 464 						d_AllDtc_a[Index].DtcState.state.TestFailed = FALSE;
 891  0163 ed81              	ldy	OFST-30,s
 892  0165 1858              	lsly	
 893  0167 0dea000101        	bclr	_d_AllDtc_a+1,y,1
 894                         ; 465 						break;
 896  016c 182002ef          	bra	L304
 897  0170                   L702:
 898                         ; 471 						if (TransferDtcToFaultMemory == TRUE)
 900  0170 ec86              	ldd	OFST-25,s
 901  0172 8c0001            	cpd	#1
 902  0175 182602e6          	bne	L304
 903                         ; 475 							d_AllDtc_a[Index].DtcState.state.TestFailed = TRUE;
 905  0179 ed81              	ldy	OFST-30,s
 906  017b 1858              	lsly	
 907                         ; 476 							d_AllDtc_a[Index].DtcState.state.TestFailedThisMonitoringCycle = TRUE;
 909                         ; 477 							d_AllDtc_a[Index].DtcState.state.Pending = TRUE;
 911  017d 0cea000107        	bset	_d_AllDtc_a+1,y,7
 912                         ; 479 							if (fmem_AllRegisteredDtc_t[Index].dtc_warning_indicator == TRUE) {
 914  0182 ec81              	ldd	OFST-30,s
 915  0184 cd0006            	ldy	#6
 916  0187 13                	emul	
 917  0188 b746              	tfr	d,y
 918  018a 0fea0005020b      	brclr	_fmem_AllRegisteredDtc_t+5,y,2,L704
 919                         ; 480 								d_AllDtc_a[Index].DtcState.state.WarningIndicatorRequested = TRUE;
 921  0190 ed81              	ldy	OFST-30,s
 922  0192 1858              	lsly	
 923  0194 0cea000180        	bset	_d_AllDtc_a+1,y,128
 925  0199 2009              	bra	L114
 926  019b                   L704:
 927                         ; 484 								d_AllDtc_a[Index].DtcState.state.WarningIndicatorRequested = FALSE;
 929  019b ed81              	ldy	OFST-30,s
 930  019d 1858              	lsly	
 931  019f 0dea000180        	bclr	_d_AllDtc_a+1,y,128
 932  01a4                   L114:
 933                         ; 489 							if (NumberOfRegisteredDtc < SIZEOF_DTC_MEMORY)
 935  01a4 ec84              	ldd	OFST-27,s
 936  01a6 8c000a            	cpd	#10
 937                         ; 493 								fmemLF_MemMove((unsigned char*)&d_DtcMemory.elem[1], (unsigned char*)&d_DtcMemory.elem[0], (sizeof(struct DTC_MEMORY_ELEMENT)*NumberOfRegisteredDtc));
 941  01a9 252d              	blo	L514
 942                         ; 502 								if ((DtcMemoryIndex = FMemLibLF_SearchDtcIndexWithLowerPriority(fmem_AllRegisteredDtc_t[Index].dtc_priority)) >= 0)
 944  01ab ec81              	ldd	OFST-30,s
 945  01ad cd0006            	ldy	#6
 946  01b0 13                	emul	
 947  01b1 b746              	tfr	d,y
 948  01b3 e6ea0004          	ldab	_fmem_AllRegisteredDtc_t+4,y
 949  01b7 55                	rolb	
 950  01b8 55                	rolb	
 951  01b9 55                	rolb	
 952  01ba c403              	andb	#3
 953  01bc 87                	clra	
 954  01bd 4a0f2121          	call	L751f_FMemLibLF_SearchDtcIndexWithLowerPriority
 956  01c1 6c88              	std	OFST-23,s
 957  01c3 182d0298          	blt	L304
 958                         ; 504 									FMemLibLF_ClearSingleDtc(d_DtcMemory.elem[DtcMemoryIndex].dtc_RomIndex);
 960  01c7 cd000b            	ldy	#11
 961  01ca 13                	emul	
 962  01cb b746              	tfr	d,y
 963  01cd e6ea0073          	ldab	_d_DtcMemory+2,y
 964  01d1 87                	clra	
 965  01d2 4a0db3b3          	call	f_FMemLibLF_ClearSingleDtc
 967                         ; 506 									fmemLF_MemMove((unsigned char*)&d_DtcMemory.elem[1], (unsigned char*)&d_DtcMemory.elem[0], (sizeof(struct DTC_MEMORY_ELEMENT)* (u_temp_l)DtcMemoryIndex) );
 969  01d6 ec88              	ldd	OFST-23,s
 972  01d8                   L514:
 973  01d8 cd000b            	ldy	#11
 974  01db 13                	emul	
 975  01dc 3b                	pshd	
 976  01dd cc0071            	ldd	#_d_DtcMemory
 977  01e0 3b                	pshd	
 978  01e1 cc007c            	ldd	#_d_DtcMemory+11
 979  01e4 160000            	jsr	_memmove
 980  01e7 1b84              	leas	4,s
 981                         ; 516 							d_DtcMemory.elem[0].dtc_RomIndex = (unsigned char)Index;
 983  01e9 180d820073        	movb	OFST-29,s,_d_DtcMemory+2
 984                         ; 517 							d_AllDtc_a[Index].DtcState.state.Confirmed = TRUE;
 986  01ee ed81              	ldy	OFST-30,s
 987  01f0 1858              	lsly	
 988  01f2 0cea000108        	bset	_d_AllDtc_a+1,y,8
 989                         ; 518 							d_DtcMemory.elem[0].frequency_counter = 1;
 991  01f7 c601              	ldab	#1
 992  01f9 7b0071            	stab	_d_DtcMemory
 993                         ; 519 							d_DtcMemory.elem[0].operation_cycle_counter = D_SELF_HEALING_CRITERION_K;
 995  01fc c628              	ldab	#40
 996  01fe 7b0072            	stab	_d_DtcMemory+1
 997                         ; 521 							d_DtcMemory.elem[0].ecu_lifetime = fmem_lifetime_l;
 999  0201 1804000f0074      	movw	_fmem_lifetime_l,_d_DtcMemory+3
1000  0207 180400110076      	movw	_fmem_lifetime_l+2,_d_DtcMemory+5
1001                         ; 522 							d_DtcMemory.elem[0].ecu_time_keyon = fmem_keyon_time_w;
1003  020d 1804000d0078      	movw	_fmem_keyon_time_w,_d_DtcMemory+7
1004                         ; 523 							d_DtcMemory.elem[0].key_on_counter = fmem_keyon_counter_w; 
1006  0213 18040006007a      	movw	_fmem_keyon_counter_w,_d_DtcMemory+9
1007                         ; 526 							update_chrono_stack_b = TRUE;   
1009                         ; 528 							update_dtc_status_info_b = TRUE;
1011  0219 1c000048          	bset	_diag_status_flags_c,72
1012                         ; 531 							EE_BlockRead(EE_ECU_TIME_FIRSTDTC, (u_8Bit*)&temp_lifetime_firstdtc);
1014  021d 1a8c              	leax	OFST-19,s
1015  021f 34                	pshx	
1016  0220 cc002a            	ldd	#42
1017  0223 4a000000          	call	f_EE_BlockRead
1019  0227 1b82              	leas	2,s
1020                         ; 532 							EE_BlockRead(EE_ECU_TIME_KEYON_FIRSTDTC, (u_8Bit*)&temp_time_ecu_keyon);
1022  0229 1af011            	leax	OFST-14,s
1023  022c 34                	pshx	
1024  022d cc002b            	ldd	#43
1025  0230 4a000000          	call	f_EE_BlockRead
1027  0234 1b82              	leas	2,s
1028                         ; 536 							EE_BlockRead(EE_FIRST_CONF_DTC, &temp_first_confirmeddtc);
1030  0236 1af010            	leax	OFST-15,s
1031  0239 34                	pshx	
1032  023a cc002c            	ldd	#44
1033  023d 4a000000          	call	f_EE_BlockRead
1035  0241 1b82              	leas	2,s
1036                         ; 537 							if(temp_first_confirmeddtc == 0xFF)
1038  0243 e6f010            	ldab	OFST-15,s
1039  0246 04a113            	ibne	b,L324
1040                         ; 539 								temp_first_confirmeddtc = d_DtcMemory.elem[0].dtc_RomIndex;
1042  0249 1809f0100073      	movb	_d_DtcMemory+2,OFST-15,s
1043                         ; 540 								EE_BlockWrite(EE_FIRST_CONF_DTC, &temp_first_confirmeddtc);
1045  024f 1af010            	leax	OFST-15,s
1046  0252 34                	pshx	
1047  0253 cc002c            	ldd	#44
1048  0256 4a000000          	call	f_EE_BlockWrite
1050  025a 1b82              	leas	2,s
1051  025c                   L324:
1052                         ; 545 							EE_BlockWrite(EE_RECENT_CONF_DTC, (u_8Bit*)&d_DtcMemory.elem[0].dtc_RomIndex);
1054  025c cc0073            	ldd	#_d_DtcMemory+2
1055  025f 3b                	pshd	
1056  0260 cc002d            	ldd	#45
1057  0263 4a000000          	call	f_EE_BlockWrite
1059  0267 1b82              	leas	2,s
1060                         ; 550 							if (temp_lifetime_firstdtc.l == 0x00)
1062  0269 ec8c              	ldd	OFST-19,s
1063  026b 182601f0          	bne	L304
1064  026f ec8e              	ldd	OFST-17,s
1065  0271 182601ea          	bne	L304
1066                         ; 552 								EE_BlockWrite(EE_ECU_TIME_FIRSTDTC, (u_8Bit*)&fmem_lifetime_l);
1068  0275 cc000f            	ldd	#_fmem_lifetime_l
1069  0278 3b                	pshd	
1070  0279 cc002a            	ldd	#42
1071  027c 4a000000          	call	f_EE_BlockWrite
1073                         ; 553 								EE_BlockWrite(EE_ECU_TIME_KEYON_FIRSTDTC, (u_8Bit*)&fmem_keyon_time_w);
1075  0280 cc000d            	ldd	#_fmem_keyon_time_w
1076  0283 6c80              	std	0,s
1077  0285 cc002b            	ldd	#43
1078  0288 4a000000          	call	f_EE_BlockWrite
1080  028c 1b82              	leas	2,s
1082  028e 182001cd          	bra	L304
1083  0292                   L112:
1084                         ; 575 						if (d_AllDtc_a[Index].DtcState.state.TestFailed == TRUE)
1086  0292 ed81              	ldy	OFST-30,s
1087  0294 1858              	lsly	
1088  0296 0fea00010127      	brclr	_d_AllDtc_a+1,y,1,L334
1089                         ; 578 							d_AllDtc_a[Index].DtcState.state.TestFailed = FALSE;
1091                         ; 579 							d_AllDtc_a[Index].DtcState.state.TestFailedThisMonitoringCycle = FALSE;
1093                         ; 580 							d_AllDtc_a[Index].DtcState.state.Pending = FALSE;
1095  029c 0dea000107        	bclr	_d_AllDtc_a+1,y,7
1096                         ; 582 							if(d_OperationCycle_c == OPERATION_CYLE_ON)
1098  02a1 f60014            	ldab	_d_OperationCycle_c
1099  02a4 04210b            	dbne	b,L534
1100                         ; 588 								d_DtcMemory.elem[DTCMemIndex].operation_cycle_counter = D_SELF_HEALING_CRITERION_K;
1102  02a7 e683              	ldab	OFST-28,s
1103  02a9 860b              	ldaa	#11
1104  02ab 12                	mul	
1105  02ac b746              	tfr	d,y
1106  02ae c628              	ldab	#40
1108  02b0 2009              	bra	L734
1109  02b2                   L534:
1110                         ; 596 								d_DtcMemory.elem[DTCMemIndex].operation_cycle_counter = 39; //CUSW update
1112  02b2 e683              	ldab	OFST-28,s
1113  02b4 860b              	ldaa	#11
1114  02b6 12                	mul	
1115  02b7 b746              	tfr	d,y
1116  02b9 c627              	ldab	#39
1117  02bb                   L734:
1118  02bb 6bea0072          	stab	_d_DtcMemory+1,y
1119                         ; 600 							update_chrono_stack_b = TRUE;      
1121                         ; 602 							update_dtc_status_info_b = TRUE;
1124  02bf 18200198          	bra	LC001
1125  02c3                   L334:
1126                         ; 608 							if (fmem_AllRegisteredDtc_t[Index].dtc_priority != D_PRIO_1_K)
1128  02c3 ec81              	ldd	OFST-30,s
1129  02c5 cd0006            	ldy	#6
1130  02c8 13                	emul	
1131  02c9 b746              	tfr	d,y
1132  02cb e6ea0004          	ldab	_fmem_AllRegisteredDtc_t+4,y
1133  02cf c4c0              	andb	#192
1134  02d1 c140              	cmpb	#64
1135  02d3 18270188          	beq	L304
1136                         ; 610 								if (d_DtcMemory.elem[DTCMemIndex].operation_cycle_counter == 0)
1138  02d7 e683              	ldab	OFST-28,s
1139  02d9 860b              	ldaa	#11
1140  02db 12                	mul	
1141  02dc b746              	tfr	d,y
1142  02de e6ea0072          	ldab	_d_DtcMemory+1,y
1143  02e2 18260179          	bne	L304
1144                         ; 615 									FMemLibLF_ClearSingleDtc((unsigned char)Index);
1146  02e6 e682              	ldab	OFST-29,s
1147  02e8 87                	clra	
1148  02e9 4a0db3b3          	call	f_FMemLibLF_ClearSingleDtc
1150                         ; 616 									fmemLF_MemMove((unsigned char*)&d_DtcMemory.elem[DTCMemIndex], (unsigned char*)&d_DtcMemory.elem[DTCMemIndex+1], (sizeof(struct DTC_MEMORY_ELEMENT)*((NumberOfRegisteredDtc-1) - (u_temp_l)DtcMemoryIndex)));
1152  02ed ec84              	ldd	OFST-27,s
1153  02ef c3ffff            	addd	#-1
1154  02f2 a388              	subd	OFST-23,s
1155  02f4 cd000b            	ldy	#11
1156  02f7 13                	emul	
1157  02f8 3b                	pshd	
1158  02f9 e685              	ldab	OFST-26,s
1159  02fb 860b              	ldaa	#11
1160  02fd 12                	mul	
1161  02fe c3007c            	addd	#_d_DtcMemory+11
1162  0301 3b                	pshd	
1163  0302 e687              	ldab	OFST-24,s
1164  0304 860b              	ldaa	#11
1165  0306 12                	mul	
1166  0307 c30071            	addd	#_d_DtcMemory
1167  030a 160000            	jsr	_memmove
1169  030d 1b84              	leas	4,s
1170                         ; 618 									fmemLF_MemSet((unsigned char*)&d_DtcMemory.elem[NumberOfRegisteredDtc-1], 0xFF, sizeof(struct DTC_MEMORY_ELEMENT));
1172  030f ec84              	ldd	OFST-27,s
1173  0311 cd000b            	ldy	#11
1174  0314 13                	emul	
1175  0315 b746              	tfr	d,y
1176  0317 18cb0066          	addy	#_d_DtcMemory-11
1177  031b c6ff              	ldab	#255
1178  031d ce000b            	ldx	#11
1179  0320                   L21:
1180  0320 6b70              	stab	1,y+
1181  0322 0435fb            	dbne	x,L21
1182                         ; 621 									dtc_in_hist = FMemLibLF_GetNumberOfRegisteredDtc(HISTORICAL_MEMORY);
1184  0325 cc0001            	ldd	#1
1185  0328 4a0e2929          	call	f_FMemLibLF_GetNumberOfRegisteredDtc
1187  032c 6c86              	std	OFST-25,s
1188                         ; 624 									DtcHistIndex = FMemLibLF_SearchDtcIndexInDtcMemory((unsigned char)Index, HISTORICAL_MEMORY);
1190  032e cc0001            	ldd	#1
1191  0331 3b                	pshd	
1192  0332 e684              	ldab	OFST-27,s
1193  0334 4a0ebdbd          	call	f_FMemLibLF_SearchDtcIndexInDtcMemory
1195  0338 1b82              	leas	2,s
1196  033a 6c84              	std	OFST-27,s
1197                         ; 626 									if((DtcHistIndex >= 0) && (dtc_in_hist > 0)) /* Clear only if the searched DTC is present in historical stack */
1199  033c 2d3c              	blt	L154
1201  033e ec86              	ldd	OFST-25,s
1202  0340 2738              	beq	L154
1203                         ; 629 										fmemLF_MemMove((unsigned char*)&d_DtcHistoricalMemory.elem[DtcHistIndex], (unsigned char*)&d_DtcHistoricalMemory.elem[DtcHistIndex+1], (sizeof(struct DTC_HISTORICAL_MEMORY_ELEMENT)*((dtc_in_hist-1) - (u_temp_l)DtcHistIndex)));
1205  0342 c3ffff            	addd	#-1
1206  0345 a384              	subd	OFST-27,s
1207  0347 cd0009            	ldy	#9
1208  034a 13                	emul	
1209  034b 3b                	pshd	
1210  034c ec86              	ldd	OFST-25,s
1211  034e cd0009            	ldy	#9
1212  0351 13                	emul	
1213  0352 c30020            	addd	#_d_DtcHistoricalMemory+9
1214  0355 3b                	pshd	
1215  0356 ec88              	ldd	OFST-23,s
1216  0358 cd0009            	ldy	#9
1217  035b 13                	emul	
1218  035c c30017            	addd	#_d_DtcHistoricalMemory
1219  035f 160000            	jsr	_memmove
1221  0362 1b84              	leas	4,s
1222                         ; 630 										fmemLF_MemSet((unsigned char*)&d_DtcHistoricalMemory.elem[dtc_in_hist-1], 0xFF, sizeof(struct DTC_HISTORICAL_MEMORY_ELEMENT));
1224  0364 ec86              	ldd	OFST-25,s
1225  0366 cd0009            	ldy	#9
1226  0369 13                	emul	
1227  036a b746              	tfr	d,y
1228  036c 18cb000e          	addy	#_d_DtcHistoricalMemory-9
1229  0370 c6ff              	ldab	#255
1230  0372 ce0009            	ldx	#9
1231  0375                   L41:
1232  0375 6b70              	stab	1,y+
1233  0377 0435fb            	dbne	x,L41
1235  037a                   L154:
1236                         ; 639 									dtc_in_chrono = FMemLibLF_GetNumberOfRegisteredDtc(FAULT_MEMORY);
1238  037a 87                	clra	
1239  037b c7                	clrb	
1240  037c 4a0e2929          	call	f_FMemLibLF_GetNumberOfRegisteredDtc
1242  0380 6c8a              	std	OFST-21,s
1243                         ; 643 									if (0 == dtc_in_chrono)
1245  0382 2632              	bne	L354
1246                         ; 646 										temp_lifetime_firstdtc.l = 0x00;
1248  0384 6c8c              	std	OFST-19,s
1249  0386 6c8e              	std	OFST-17,s
1250                         ; 647 										temp_time_ecu_keyon = 0x00;
1252  0388 1869f011          	clrw	OFST-14,s
1253                         ; 648 										temp_recent_conf_dtc = 0xFF;
1255  038c c6ff              	ldab	#255
1256  038e 6bf013            	stab	OFST-12,s
1257                         ; 649 										EE_BlockWrite(EE_ECU_TIME_FIRSTDTC, (u_8Bit*)&temp_lifetime_firstdtc);
1259  0391 1a8c              	leax	OFST-19,s
1260  0393 34                	pshx	
1261  0394 c62a              	ldab	#42
1262  0396 4a000000          	call	f_EE_BlockWrite
1264  039a 1b82              	leas	2,s
1265                         ; 650 										EE_BlockWrite(EE_ECU_TIME_KEYON_FIRSTDTC, (u_8Bit*)&temp_time_ecu_keyon);
1267  039c 1af011            	leax	OFST-14,s
1268  039f 34                	pshx	
1269  03a0 cc002b            	ldd	#43
1270  03a3 4a000000          	call	f_EE_BlockWrite
1272  03a7 1b82              	leas	2,s
1273                         ; 651 										EE_BlockWrite(EE_RECENT_CONF_DTC, &temp_recent_conf_dtc);
1275  03a9 1af013            	leax	OFST-12,s
1276  03ac 34                	pshx	
1277  03ad cc002d            	ldd	#45
1278  03b0 4a000000          	call	f_EE_BlockWrite
1280  03b4 1b82              	leas	2,s
1281  03b6                   L354:
1282                         ; 656 									update_chrono_stack_b = TRUE;     
1284                         ; 659 									update_historical_stack_b = TRUE;
1286                         ; 661 									update_dtc_status_info_b = TRUE;
1288  03b6 1c000058          	bset	_diag_status_flags_c,88
1289  03ba 182000a1          	bra	L304
1290  03be                   L312:
1291                         ; 674 						if (TransferDtcToFaultMemory == TRUE)
1293  03be ec86              	ldd	OFST-25,s
1294  03c0 8c0001            	cpd	#1
1295  03c3 18260098          	bne	L304
1296                         ; 677 							if (d_AllDtc_a[Index].DtcState.state.TestFailed == FALSE)
1298  03c7 ed81              	ldy	OFST-30,s
1299  03c9 1858              	lsly	
1300  03cb e6ea0001          	ldab	_d_AllDtc_a+1,y
1301  03cf c501              	bitb	#1
1302  03d1 1826008a          	bne	L304
1303                         ; 679 								d_AllDtc_a[Index].DtcState.state.TestFailed = TRUE;
1305                         ; 680 								d_AllDtc_a[Index].DtcState.state.TestFailedThisMonitoringCycle = TRUE;
1307                         ; 681 								d_AllDtc_a[Index].DtcState.state.Pending = TRUE;
1309  03d5 0cea000107        	bset	_d_AllDtc_a+1,y,7
1310                         ; 683 								FMemLibLF_TransferFaultToHistoricalStack(DtcMemoryIndex, 1);
1312  03da cc0001            	ldd	#1
1313  03dd 3b                	pshd	
1314  03de ec8a              	ldd	OFST-21,s
1315  03e0 4a0f8f8f          	call	f_FMemLibLF_TransferFaultToHistoricalStack
1317  03e4 1b82              	leas	2,s
1318                         ; 687 								EE_BlockWrite(EE_RECENT_CONF_DTC, (u_8Bit*)&d_DtcMemory.elem[DTCMemIndex].dtc_RomIndex);
1320  03e6 e683              	ldab	OFST-28,s
1321  03e8 860b              	ldaa	#11
1322  03ea 12                	mul	
1323  03eb c30073            	addd	#_d_DtcMemory+2
1324  03ee 3b                	pshd	
1325  03ef cc002d            	ldd	#45
1326  03f2 4a000000          	call	f_EE_BlockWrite
1328  03f6 1b82              	leas	2,s
1329                         ; 689 								d_DtcMemory.elem[DTCMemIndex].operation_cycle_counter = D_SELF_HEALING_CRITERION_K;
1331  03f8 e683              	ldab	OFST-28,s
1332  03fa 860b              	ldaa	#11
1333  03fc 12                	mul	
1334  03fd b746              	tfr	d,y
1335  03ff c628              	ldab	#40
1336  0401 6bea0072          	stab	_d_DtcMemory+1,y
1337                         ; 691 								d_DtcMemory.elem[DTCMemIndex].frequency_counter = FMemLibLF_IncCounter((u_temp_l)(d_DtcMemory.elem[DTCMemIndex].frequency_counter), 0xFF);
1339  0405 35                	pshy	
1340  0406 cc00ff            	ldd	#255
1341  0409 3b                	pshd	
1342  040a e687              	ldab	OFST-24,s
1343  040c 860b              	ldaa	#11
1344  040e 12                	mul	
1345  040f b746              	tfr	d,y
1346  0411 e6ea0071          	ldab	_d_DtcMemory,y
1347  0415 87                	clra	
1348  0416 4a106a6a          	call	f_FMemLibLF_IncCounter
1350  041a 1b82              	leas	2,s
1351  041c 31                	puly	
1352  041d 6bea0071          	stab	_d_DtcMemory,y
1353                         ; 693 								fmemLF_MemCpy((unsigned char*)&TempDtcMemoryElement, (unsigned char*)&d_DtcMemory.elem[DTCMemIndex], sizeof(struct DTC_MEMORY_ELEMENT));
1355  0421 19f014            	leay	OFST-11,s
1356  0424 e683              	ldab	OFST-28,s
1357  0426 860b              	ldaa	#11
1358  0428 12                	mul	
1359  0429 b745              	tfr	d,x
1360  042b 188b0071          	addx	#_d_DtcMemory
1361  042f cc000b            	ldd	#11
1362  0432                   L61:
1363  0432 180a3070          	movb	1,x+,1,y+
1364  0436 0434f9            	dbne	d,L61
1365                         ; 695 								fmemLF_MemMove((unsigned char*)&d_DtcMemory.elem[1], (unsigned char*)&d_DtcMemory.elem[0], (sizeof(struct DTC_MEMORY_ELEMENT)* DTCMemIndex));
1367  0439 e683              	ldab	OFST-28,s
1368  043b 860b              	ldaa	#11
1369  043d 12                	mul	
1370  043e 3b                	pshd	
1371  043f cc0071            	ldd	#_d_DtcMemory
1372  0442 3b                	pshd	
1373  0443 cc007c            	ldd	#_d_DtcMemory+11
1374  0446 160000            	jsr	_memmove
1376  0449 1b84              	leas	4,s
1377                         ; 697 								fmemLF_MemCpy((unsigned char*)&d_DtcMemory, (unsigned char*)&TempDtcMemoryElement, sizeof(struct DTC_MEMORY_ELEMENT));
1379  044b cd0071            	ldy	#_d_DtcMemory
1380  044e 1af014            	leax	OFST-11,s
1381  0451 cc000b            	ldd	#11
1382  0454                   L02:
1383  0454 180a3070          	movb	1,x+,1,y+
1384  0458 0434f9            	dbne	d,L02
1385                         ; 699 								update_chrono_stack_b = TRUE;      
1387                         ; 701 								update_dtc_status_info_b = TRUE;
1389  045b                   LC001:
1390  045b 1c000048          	bset	_diag_status_flags_c,72
1391  045f                   L304:
1392                         ; 407 			for (Index = cyclic_dtc_count_c; Index < no_of_dtc_c; Index++)
1394  045f 186281            	incw	OFST-30,s
1395  0462                   L163:
1398  0462 f60001            	ldab	_no_of_dtc_c
1399  0465 87                	clra	
1400  0466 ac81              	cpd	OFST-30,s
1401  0468 1822fc27          	bhi	L553
1402  046c                   L143:
1403                         ; 716 	FMemLibF_UpdatestoEEPROM(); 
1405  046c 4a107676          	call	f_FMemLibF_UpdatestoEEPROM
1407                         ; 718 	FMemLibF_ECUTimeStamps();
1409  0470 4a10cfcf          	call	f_FMemLibF_ECUTimeStamps
1411                         ; 719 }
1414  0474 1bf01f            	leas	31,s
1415  0477 0a                	rtc	
1841                         .const:	section	.data
1842                         	even
1843  0000                   L327:
1844  0000 00000006          	dc.w	0,6
1845  0004 04ac              	dc.w	L164
1846  0006 00000000          	dc.l	0
1847  000a 04ac              	dc.w	L164
1848  000c 00000001          	dc.l	1
1849  0010 04ac              	dc.w	L164
1850  0012 00400000          	dc.l	4194304
1851  0016 04b2              	dc.w	L364
1852  0018 00800000          	dc.l	8388608
1853  001c 04b2              	dc.w	L364
1854  001e 00c00000          	dc.l	12582912
1855  0022 04b2              	dc.w	L364
1856  0024 00ffffff          	dc.l	16777215
1857  0028 04c3              	dc.w	L564
1858                         ; 721 void FMemLibF_ClearDiagnosticInformation(enum DIAG_RESPONSE* Response, DescMsgContext* pMsgContext)
1858                         ; 722 {
1859                         	switch	.ftext
1860  0478                   f_FMemLibF_ClearDiagnosticInformation:
1862  0478 3b                	pshd	
1863  0479 1b96              	leas	-10,s
1864       0000000a          OFST:	set	10
1867                         ; 727 	TempDtcCode.all = 0;
1869  047b 87                	clra	
1870  047c c7                	clrb	
1871  047d 6c80              	std	OFST-10,s
1872  047f 6c82              	std	OFST-8,s
1873                         ; 730 	TempDtcCode.byte[1] = pMsgContext->reqData[0]; //Harsha..Re arranged
1875  0481 edf3000f          	ldy	[OFST+5,s]
1876  0485 180a4081          	movb	0,y,OFST-9,s
1877                         ; 731 	TempDtcCode.byte[2] = pMsgContext->reqData[1];
1879  0489 180a4182          	movb	1,y,OFST-8,s
1880                         ; 732 	TempDtcCode.byte[3] = pMsgContext->reqData[2];
1882  048d 180a4283          	movb	2,y,OFST-7,s
1883                         ; 735 	NumberOfRegisteredDtc = FMemLibLF_GetNumberOfRegisteredDtc(FAULT_MEMORY);
1885  0491 4a0e2929          	call	f_FMemLibLF_GetNumberOfRegisteredDtc
1887  0495 6c88              	std	OFST-2,s
1888                         ; 737 	if (pMsgContext->reqDataLen == 3)
1890  0497 ed8f              	ldy	OFST+5,s
1891  0499 ec42              	ldd	2,y
1892  049b 8c0003            	cpd	#3
1893  049e 1826008c          	bne	L127
1894                         ; 739 		switch (TempDtcCode.all) {
1896  04a2 ec82              	ldd	OFST-8,s
1897  04a4 ee80              	ldx	OFST-10,s
1899  04a6 cd0000            	ldy	#L327
1900  04a9 060000            	jmp	c_jltab
1901  04ac                   L164:
1902                         ; 746 				*Response = REQUEST_OUT_OF_RANGE;
1904  04ac c604              	ldab	#4
1905                         ; 747 				break;
1907  04ae 1820007e          	bra	LC002
1908  04b2                   L364:
1909                         ; 756 				FMemLibF_ClearDtcMemory(FAULT_MEMORY);      //Clear snapshot record 1 stack
1911  04b2 87                	clra	
1912  04b3 c7                	clrb	
1913  04b4 4a053737          	call	f_FMemLibF_ClearDtcMemory
1915                         ; 758 				FMemLibF_ClearDtcMemory(HISTORICAL_MEMORY); //Clear snapshot record 2 stack
1917  04b8 cc0001            	ldd	#1
1918  04bb 4a053737          	call	f_FMemLibF_ClearDtcMemory
1920                         ; 759 				*Response = RESPONSE_OK;
1922  04bf c601              	ldab	#1
1923                         ; 760 				break;
1925  04c1 206d              	bra	LC002
1926  04c3                   L564:
1927                         ; 765 				if ((Index = FMemLibF_CheckDtcSupportOverDtc(TempDtcCode.all)) >= 0) {
1929  04c3 ec82              	ldd	OFST-8,s
1930  04c5 ee80              	ldx	OFST-10,s
1931  04c7 4a0d7373          	call	f_FMemLibF_CheckDtcSupportOverDtc
1933  04cb 6c84              	std	OFST-6,s
1934  04cd 2d5b              	blt	L727
1935                         ; 767 					if ((DtcMemoryIndex = FMemLibLF_SearchDtcIndexInDtcMemory((unsigned char)Index, FAULT_MEMORY)) >= 0)
1937  04cf 87                	clra	
1938  04d0 c7                	clrb	
1939  04d1 3b                	pshd	
1940  04d2 e687              	ldab	OFST-3,s
1941  04d4 4a0ebdbd          	call	f_FMemLibLF_SearchDtcIndexInDtcMemory
1943  04d8 1b82              	leas	2,s
1944  04da 6c86              	std	OFST-4,s
1945  04dc 2d43              	blt	L137
1946                         ; 772 						FMemLibLF_ClearSingleDtc((unsigned char)Index);
1948  04de e685              	ldab	OFST-5,s
1949  04e0 87                	clra	
1950  04e1 4a0db3b3          	call	f_FMemLibLF_ClearSingleDtc
1952                         ; 774 						fmemLF_MemMove((unsigned char*)&d_DtcMemory.elem[DtcMemoryIndex], (unsigned char*)&d_DtcMemory.elem[DtcMemoryIndex+1], (sizeof(struct DTC_MEMORY_ELEMENT)*((NumberOfRegisteredDtc-1) - (u_temp_l)DtcMemoryIndex)));
1954  04e5 ec88              	ldd	OFST-2,s
1955  04e7 c3ffff            	addd	#-1
1956  04ea a386              	subd	OFST-4,s
1957  04ec cd000b            	ldy	#11
1958  04ef 13                	emul	
1959  04f0 3b                	pshd	
1960  04f1 ec88              	ldd	OFST-2,s
1961  04f3 cd000b            	ldy	#11
1962  04f6 13                	emul	
1963  04f7 c3007c            	addd	#_d_DtcMemory+11
1964  04fa 3b                	pshd	
1965  04fb ec8a              	ldd	OFST+0,s
1966  04fd cd000b            	ldy	#11
1967  0500 13                	emul	
1968  0501 c30071            	addd	#_d_DtcMemory
1969  0504 160000            	jsr	_memmove
1971  0507 1b84              	leas	4,s
1972                         ; 776 						fmemLF_MemSet((unsigned char*)&d_DtcMemory.elem[NumberOfRegisteredDtc-1], 0xFF, sizeof(struct DTC_MEMORY_ELEMENT));
1974  0509 ec88              	ldd	OFST-2,s
1975  050b cd000b            	ldy	#11
1976  050e 13                	emul	
1977  050f b746              	tfr	d,y
1978  0511 18cb0066          	addy	#_d_DtcMemory-11
1979  0515 c6ff              	ldab	#255
1980  0517 ce000b            	ldx	#11
1981  051a                   L62:
1982  051a 6b70              	stab	1,y+
1983  051c 0435fb            	dbne	x,L62
1985  051f 2013              	bra	L737
1986  0521                   L137:
1987                         ; 782 						FMemLibLF_ClearSingleDtc((unsigned char)Index);
1989  0521 e685              	ldab	OFST-5,s
1990  0523 87                	clra	
1991  0524 4a0db3b3          	call	f_FMemLibLF_ClearSingleDtc
1993  0528 200a              	bra	L737
1994  052a                   L727:
1995                         ; 787 					*Response = REQUEST_OUT_OF_RANGE;
1997  052a c604              	ldab	#4
1998  052c 2002              	bra	LC002
2000  052e                   L127:
2001                         ; 794 		*Response = LENGTH_INVALID_FORMAT;
2003  052e c605              	ldab	#5
2004  0530                   LC002:
2005  0530 6bf3000a          	stab	[OFST+0,s]
2006  0534                   L737:
2007                         ; 796 }
2010  0534 1b8c              	leas	12,s
2011  0536 0a                	rtc	
2106                         ; 798 void FMemLibF_ClearDtcMemory(enum FAULTMEMORY_TYP FaultMemoryTyp)
2106                         ; 799 {
2107                         	switch	.ftext
2108  0537                   f_FMemLibF_ClearDtcMemory:
2110  0537 3b                	pshd	
2111  0538 1b97              	leas	-9,s
2112       00000009          OFST:	set	9
2115                         ; 805 	switch (FaultMemoryTyp) {
2118  053a 044105            	tbeq	b,L147
2119  053d 04016a            	dbeq	b,L347
2120  0540 207a              	bra	L7001
2121  0542                   L147:
2122                         ; 809 			for ( Index = 0; Index < SIZEOF_ALL_SUPPORTED_DTC; Index++)
2124  0542 186980            	clrw	OFST-9,s
2125  0545 e681              	ldab	OFST-8,s
2126  0547                   L1101:
2127                         ; 811 				FMemLibLF_ClearSingleDtc((unsigned char)Index);
2129  0547 87                	clra	
2130  0548 4a0db3b3          	call	f_FMemLibLF_ClearSingleDtc
2132                         ; 809 			for ( Index = 0; Index < SIZEOF_ALL_SUPPORTED_DTC; Index++)
2134  054c 186280            	incw	OFST-9,s
2137  054f ec80              	ldd	OFST-9,s
2138  0551 8c002e            	cpd	#46
2139  0554 25f1              	blo	L1101
2140                         ; 815 			update_dtc_status_info_b = TRUE;
2142  0556 1c000040          	bset	_diag_status_flags_c,64
2143                         ; 824 			fmemLF_MemSet((unsigned char*)&d_DtcMemory, 0xFF, sizeof(struct DTC_MEMORY));
2145  055a cd0071            	ldy	#_d_DtcMemory
2146  055d ccffff            	ldd	#-1
2147  0560 ce0037            	ldx	#55
2148  0563                   L23:
2149  0563 6c71              	std	2,y+
2150  0565 0435fb            	dbne	x,L23
2151                         ; 827 			temp_lifetime_firstdtc.l = 0x00;
2153  0568 87                	clra	
2154  0569 c7                	clrb	
2155  056a 6c85              	std	OFST-4,s
2156  056c 6c87              	std	OFST-2,s
2157                         ; 828 			temp_time_ecu_keyon = 0x00;
2159  056e 186983            	clrw	OFST-6,s
2160                         ; 829 			temp_recent_conf_dtc = 0xFF;
2162  0571 c6ff              	ldab	#255
2163  0573 6b82              	stab	OFST-7,s
2164                         ; 830 			EE_BlockWrite(EE_ECU_TIME_FIRSTDTC, (u_8Bit*)&temp_lifetime_firstdtc);
2166  0575 1a85              	leax	OFST-4,s
2167  0577 34                	pshx	
2168  0578 c62a              	ldab	#42
2169  057a 4a000000          	call	f_EE_BlockWrite
2171  057e 1b82              	leas	2,s
2172                         ; 831 			EE_BlockWrite(EE_ECU_TIME_KEYON_FIRSTDTC, (u_8Bit*)&temp_time_ecu_keyon);
2174  0580 1a83              	leax	OFST-6,s
2175  0582 34                	pshx	
2176  0583 cc002b            	ldd	#43
2177  0586 4a000000          	call	f_EE_BlockWrite
2179  058a 1b82              	leas	2,s
2180                         ; 832 			EE_BlockWrite(EE_RECENT_CONF_DTC, &temp_recent_conf_dtc);
2182  058c 1a82              	leax	OFST-7,s
2183  058e 34                	pshx	
2184  058f cc002d            	ldd	#45
2185  0592 4a000000          	call	f_EE_BlockWrite
2187  0596 1b82              	leas	2,s
2188                         ; 833 			EE_BlockWrite(EE_FIRST_CONF_DTC, &temp_recent_conf_dtc);
2190  0598 1a82              	leax	OFST-7,s
2191  059a 34                	pshx	
2192  059b cc002c            	ldd	#44
2193  059e 4a000000          	call	f_EE_BlockWrite
2195  05a2 1b82              	leas	2,s
2196                         ; 839 			update_chrono_stack_b = TRUE;   
2198  05a4 1c000008          	bset	_diag_status_flags_c,8
2199                         ; 840 			break;
2201  05a8 2012              	bra	L7001
2202  05aa                   L347:
2203                         ; 851 			fmemLF_MemSet((unsigned char*)&d_DtcHistoricalMemory, 0xFF, sizeof(struct DTC_HISTORICAL_MEMORY));
2205  05aa cd0017            	ldy	#_d_DtcHistoricalMemory
2206  05ad ccffff            	ldd	#-1
2207  05b0 ce002d            	ldx	#45
2208  05b3                   L43:
2209  05b3 6c71              	std	2,y+
2210  05b5 0435fb            	dbne	x,L43
2211                         ; 855 			update_historical_stack_b = TRUE;    
2213  05b8 1c000010          	bset	_diag_status_flags_c,16
2214                         ; 856 			break;
2216  05bc                   L7001:
2217                         ; 859 }
2220  05bc 1b8b              	leas	11,s
2221  05be 0a                	rtc	
2406                         ; 907 void FMemLibF_ReadDiagnosticInformation(enum DIAG_RESPONSE* Response, u_temp_l* DataLen, DescMsgContext* pMsgContext,  unsigned char Subservice)
2406                         ; 908 {
2407                         	switch	.ftext
2408  05bf                   f_FMemLibF_ReadDiagnosticInformation:
2410  05bf 3b                	pshd	
2411  05c0 1b90              	leas	-16,s
2412       00000010          OFST:	set	16
2415                         ; 910 	temp_l  Index = 0, NumberOfRegisteredDtc, NumberOfMaskedDtc = 0, DtcMemoryIndex, DtcSearchMask;
2417  05c2 87                	clra	
2418  05c3 c7                	clrb	
2419  05c4 6c88              	std	OFST-8,s
2420  05c6 6c86              	std	OFST-10,s
2423                         ; 916 	switch (Subservice) {
2425  05c8 e6f01a            	ldab	OFST+10,s
2427  05cb c002              	subb	#2
2428  05cd c10d              	cmpb	#13
2429  05cf 182406e7          	bhs	L7301
2430  05d3 59                	lsld	
2431  05d4 05ff              	jmp	[d,pc]
2432  05d6 05f0              	dc.w	L7101
2433  05d8 0cba              	dc.w	L7301
2434  05da 06fa              	dc.w	L1201
2435  05dc 0cba              	dc.w	L7301
2436  05de 0989              	dc.w	L3201
2437  05e0 0a31              	dc.w	L5201
2438  05e2 0a61              	dc.w	L7201
2439  05e4 0b78              	dc.w	L1301
2440  05e6 0cba              	dc.w	L7301
2441  05e8 0cba              	dc.w	L7301
2442  05ea 0be4              	dc.w	L3301
2443  05ec 0cba              	dc.w	L7301
2444  05ee 0c4f              	dc.w	L5301
2445  05f0                   L7101:
2446                         ; 921 			DtcSearchMask = pMsgContext->reqData[0];
2448  05f0 edf30017          	ldy	[OFST+7,s]
2449  05f4 e640              	ldab	0,y
2450  05f6 87                	clra	
2451  05f7 6c88              	std	OFST-8,s
2452                         ; 922 			DtcSearchMask &= FMEM_DTC_AVAILABILITY_MASK_K;
2454  05f9 c7                	clrb	
2455  05fa 6b88              	stab	OFST-8,s
2456  05fc 0d89f0            	bclr	OFST-7,s,240
2457                         ; 925 			NumberOfRegisteredDtc = (temp_l)FMemLibLF_GetNumberOfRegisteredDtc(FAULT_MEMORY);
2459  05ff 4a0e2929          	call	f_FMemLibLF_GetNumberOfRegisteredDtc
2461  0603 6c8b              	std	OFST-5,s
2462                         ; 928 			if (((FMemLibLF_GetNumberOfMaskedDtc((unsigned char)DtcSearchMask, FAULT_MEMORY)*4)+2) < kDescPrimBufferLen)
2464  0605 87                	clra	
2465  0606 c7                	clrb	
2466  0607 3b                	pshd	
2467  0608 e68b              	ldab	OFST-5,s
2468  060a 4a0dd9d9          	call	f_FMemLibLF_GetNumberOfMaskedDtc
2470  060e 1b82              	leas	2,s
2471  0610 87                	clra	
2472  0611 59                	lsld	
2473  0612 59                	lsld	
2474  0613 c30002            	addd	#2
2475  0616 8c0102            	cpd	#258
2476  0619 182c05c1          	bge	L7421
2477                         ; 931 				pMsgContext->resData[0] = FMEM_DTC_AVAILABILITY_MASK_K;
2479  061d c60f              	ldab	#15
2480  061f eef017            	ldx	OFST+7,s
2481  0622 6be30004          	stab	[4,x]
2482                         ; 933 				if (DtcSearchMask)
2484  0626 ec88              	ldd	OFST-8,s
2485  0628 182700c7          	beq	L5411
2486                         ; 936 					for (DtcMemoryIndex = 0; DtcMemoryIndex < NumberOfRegisteredDtc; DtcMemoryIndex++)
2488  062c 186984            	clrw	OFST-12,s
2490  062f 182000ad          	bra	L3511
2491  0633                   L7411:
2492                         ; 938 						if (d_AllDtc_a[d_DtcMemory.elem[DtcMemoryIndex].dtc_RomIndex].DtcState.all & DtcSearchMask)
2494  0633 cd000b            	ldy	#11
2495  0636 13                	emul	
2496  0637 b746              	tfr	d,y
2497  0639 e6ea0073          	ldab	_d_DtcMemory+2,y
2498  063d 87                	clra	
2499  063e 59                	lsld	
2500  063f b745              	tfr	d,x
2501  0641 e6e20001          	ldab	_d_AllDtc_a+1,x
2502  0645 e489              	andb	OFST-7,s
2503  0647 18270092          	beq	L7511
2504                         ; 940 							pMsgContext->resData[(NumberOfMaskedDtc*4)+1] = FMemLibLF_GetByteOfDtc(d_DtcMemory.elem[DtcMemoryIndex].dtc_RomIndex, 1);
2506  064b cc0001            	ldd	#1
2507  064e 3b                	pshd	
2508  064f ec86              	ldd	OFST-10,s
2509  0651 cd000b            	ldy	#11
2510  0654 13                	emul	
2511  0655 b746              	tfr	d,y
2512  0657 e6ea0073          	ldab	_d_DtcMemory+2,y
2513  065b 87                	clra	
2514  065c 4a0dcbcb          	call	f_FMemLibLF_GetByteOfDtc
2516  0660 1b82              	leas	2,s
2517  0662 ed86              	ldy	OFST-10,s
2518  0664 1858              	lsly	
2519  0666 1858              	lsly	
2520  0668 eef017            	ldx	OFST+7,s
2521  066b 18eb04            	addy	4,x
2522  066e 6b41              	stab	1,y
2523                         ; 941 							pMsgContext->resData[(NumberOfMaskedDtc*4)+2] = FMemLibLF_GetByteOfDtc(d_DtcMemory.elem[DtcMemoryIndex].dtc_RomIndex, 2);
2525  0670 cc0002            	ldd	#2
2526  0673 3b                	pshd	
2527  0674 ec86              	ldd	OFST-10,s
2528  0676 cd000b            	ldy	#11
2529  0679 13                	emul	
2530  067a b746              	tfr	d,y
2531  067c e6ea0073          	ldab	_d_DtcMemory+2,y
2532  0680 87                	clra	
2533  0681 4a0dcbcb          	call	f_FMemLibLF_GetByteOfDtc
2535  0685 1b82              	leas	2,s
2536  0687 ed86              	ldy	OFST-10,s
2537  0689 1858              	lsly	
2538  068b 1858              	lsly	
2539  068d eef017            	ldx	OFST+7,s
2540  0690 18eb04            	addy	4,x
2541  0693 6b42              	stab	2,y
2542                         ; 942 							pMsgContext->resData[(NumberOfMaskedDtc*4)+3] = FMemLibLF_GetByteOfDtc(d_DtcMemory.elem[DtcMemoryIndex].dtc_RomIndex, 3);
2544  0695 cc0003            	ldd	#3
2545  0698 3b                	pshd	
2546  0699 ec86              	ldd	OFST-10,s
2547  069b cd000b            	ldy	#11
2548  069e 13                	emul	
2549  069f b746              	tfr	d,y
2550  06a1 e6ea0073          	ldab	_d_DtcMemory+2,y
2551  06a5 87                	clra	
2552  06a6 4a0dcbcb          	call	f_FMemLibLF_GetByteOfDtc
2554  06aa 1b82              	leas	2,s
2555  06ac ed86              	ldy	OFST-10,s
2556  06ae 1858              	lsly	
2557  06b0 1858              	lsly	
2558  06b2 eef017            	ldx	OFST+7,s
2559  06b5 18eb04            	addy	4,x
2560  06b8 6b43              	stab	3,y
2561                         ; 945 							pMsgContext->resData[(NumberOfMaskedDtc*4)+4] = d_AllDtc_a[d_DtcMemory.elem[DtcMemoryIndex].dtc_RomIndex].DtcState.all;
2563  06ba b756              	tfr	x,y
2564  06bc ee44              	ldx	4,y
2565  06be ec86              	ldd	OFST-10,s
2566  06c0 59                	lsld	
2567  06c1 59                	lsld	
2568  06c2 1ae6              	leax	d,x
2569  06c4 ec84              	ldd	OFST-12,s
2570  06c6 cd000b            	ldy	#11
2571  06c9 13                	emul	
2572  06ca b746              	tfr	d,y
2573  06cc e6ea0073          	ldab	_d_DtcMemory+2,y
2574  06d0 87                	clra	
2575  06d1 59                	lsld	
2576  06d2 b746              	tfr	d,y
2577  06d4 180aea000104      	movb	_d_AllDtc_a+1,y,4,x
2578                         ; 947 							NumberOfMaskedDtc++;
2580  06da 186286            	incw	OFST-10,s
2581  06dd                   L7511:
2582                         ; 936 					for (DtcMemoryIndex = 0; DtcMemoryIndex < NumberOfRegisteredDtc; DtcMemoryIndex++)
2584  06dd 186284            	incw	OFST-12,s
2585  06e0                   L3511:
2588  06e0 ec84              	ldd	OFST-12,s
2589  06e2 ac8b              	cpd	OFST-5,s
2590  06e4 182dff4b          	blt	L7411
2591                         ; 951 					*DataLen = ((u_temp_l)NumberOfMaskedDtc*4)+1;
2593  06e8 ec86              	ldd	OFST-10,s
2594  06ea 59                	lsld	
2595  06eb 59                	lsld	
2596  06ec                   LC009:
2597  06ec c30001            	addd	#1
2599  06ef 1820028c          	bra	L5021
2600  06f3                   L5411:
2601                         ; 955 					*DataLen = 1;
2603  06f3 cc0001            	ldd	#1
2604                         ; 959 				*Response = RESPONSE_OK;
2607  06f6 18200285          	bra	L5021
2608                         ; 963 				*Response = REQUEST_OUT_OF_RANGE;
2610  06fa                   L1201:
2611                         ; 972 			TempDtcCode.all = 0;
2613  06fa 87                	clra	
2614  06fb c7                	clrb	
2615  06fc 6c80              	std	OFST-16,s
2616  06fe 6c82              	std	OFST-14,s
2617                         ; 973 			temp_offset_nr = 0;
2619  0700 698a              	clr	OFST-6,s
2620                         ; 974 			temp_snapshot1_flag= 0;
2622  0702 698e              	clr	OFST-2,s
2623                         ; 975 			temp_snapshot2_flag= 0;
2625                         ; 980 			TempDtcCode.byte[1] = pMsgContext->reqData[0];
2627  0704 edf30017          	ldy	[OFST+7,s]
2628  0708 180a4081          	movb	0,y,OFST-15,s
2629                         ; 981 			TempDtcCode.byte[2] = pMsgContext->reqData[1];
2631  070c 180a4182          	movb	1,y,OFST-14,s
2632                         ; 982 			TempDtcCode.byte[3] = pMsgContext->reqData[2];
2634  0710 180a4283          	movb	2,y,OFST-13,s
2635                         ; 984 			temp_snapshot_nr = pMsgContext->reqData[3];
2637  0714 180a438d          	movb	3,y,OFST-3,s
2638                         ; 987 			if ((Index = FMemLibF_CheckDtcSupportOverDtc(TempDtcCode.all)) >= 0 &&
2638                         ; 988 			   (Index = FMemLibF_CheckDtcSupportOverDtc(TempDtcCode.all)) < SIZEOF_ALL_SUPPORTED_DTC) {
2640  0718 ec82              	ldd	OFST-14,s
2641  071a ee80              	ldx	OFST-16,s
2642  071c 4a0d7373          	call	f_FMemLibF_CheckDtcSupportOverDtc
2644  0720 6c88              	std	OFST-8,s
2645  0722 182d04b8          	blt	L7421
2647  0726 ec82              	ldd	OFST-14,s
2648  0728 ee80              	ldx	OFST-16,s
2649  072a 4a0d7373          	call	f_FMemLibF_CheckDtcSupportOverDtc
2651  072e 6c88              	std	OFST-8,s
2652  0730 8c002e            	cpd	#46
2653  0733 182c04a7          	bge	L7421
2654                         ; 990 				pMsgContext->resData[0] = pMsgContext->reqData[0];
2656  0737 edf017            	ldy	OFST+7,s
2657  073a ed44              	ldy	4,y
2658  073c eef30017          	ldx	[OFST+7,s]
2659  0740 180a0040          	movb	0,x,0,y
2660                         ; 991 				pMsgContext->resData[1] = pMsgContext->reqData[1];
2662  0744 edf017            	ldy	OFST+7,s
2663  0747 ee44              	ldx	4,y
2664  0749 ed40              	ldy	0,y
2665  074b 180a4101          	movb	1,y,1,x
2666                         ; 992 				pMsgContext->resData[2] = pMsgContext->reqData[2];
2668  074f 180a4202          	movb	2,y,2,x
2669                         ; 993 				pMsgContext->resData[3] = d_AllDtc_a[Index].DtcState.all;
2671  0753 edf017            	ldy	OFST+7,s
2672  0756 ed44              	ldy	4,y
2673  0758 ee88              	ldx	OFST-8,s
2674  075a 1848              	lslx	
2675  075c 180ae2000143      	movb	_d_AllDtc_a+1,x,3,y
2676                         ; 1000 				if ( ((DtcMemoryIndex = FMemLibLF_SearchDtcIndexInDtcMemory((unsigned char)Index, FAULT_MEMORY)) >= 0) &&
2676                         ; 1001 				     ((temp_snapshot_nr == 0x00) || (temp_snapshot_nr == 0xFF))) {
2678  0762 87                	clra	
2679  0763 c7                	clrb	
2680  0764 3b                	pshd	
2681  0765 e68b              	ldab	OFST-5,s
2682  0767 4a0ebdbd          	call	f_FMemLibLF_SearchDtcIndexInDtcMemory
2684  076b 1b82              	leas	2,s
2685  076d 6c84              	std	OFST-12,s
2686  076f 182d00d0          	blt	L7611
2688  0773 e68d              	ldab	OFST-3,s
2689  0775 2705              	beq	L1711
2691  0777 52                	incb	
2692  0778 182600c7          	bne	L7611
2693  077c                   L1711:
2694                         ; 1006 					pMsgContext->resData[4] = 0x00; //DTC Snapshot Data Record Number First Record
2696  077c edf017            	ldy	OFST+7,s
2697  077f ed44              	ldy	4,y
2698  0781 6944              	clr	4,y
2699                         ; 1007 					pMsgContext->resData[5] = 4; //Number of Record Identifiers in the snapshot.
2701  0783 c604              	ldab	#4
2702  0785 6b45              	stab	5,y
2703                         ; 1010 					pMsgContext->resData[6] = 0x10;
2705  0787 c610              	ldab	#16
2706  0789 6b46              	stab	6,y
2707                         ; 1011 					pMsgContext->resData[7] = 0x08;
2709  078b c608              	ldab	#8
2710  078d 6b47              	stab	7,y
2711                         ; 1012 					pMsgContext->resData[8] = d_DtcMemory.elem[DtcMemoryIndex].ecu_lifetime.lw.hhbyte;
2713  078f ec84              	ldd	OFST-12,s
2714  0791 b765              	tfr	y,x
2715  0793 cd000b            	ldy	#11
2716  0796 13                	emul	
2717  0797 b756              	tfr	x,y
2718  0799 b745              	tfr	d,x
2719  079b 180ae2007448      	movb	_d_DtcMemory+3,x,8,y
2720                         ; 1013 					pMsgContext->resData[9] = d_DtcMemory.elem[DtcMemoryIndex].ecu_lifetime.lw.hbyte;
2722  07a1 edf017            	ldy	OFST+7,s
2723  07a4 ee44              	ldx	4,y
2724  07a6 ec84              	ldd	OFST-12,s
2725  07a8 cd000b            	ldy	#11
2726  07ab 13                	emul	
2727  07ac b756              	tfr	x,y
2728  07ae b745              	tfr	d,x
2729  07b0 180ae2007549      	movb	_d_DtcMemory+4,x,9,y
2730                         ; 1014 					pMsgContext->resData[10] = d_DtcMemory.elem[DtcMemoryIndex].ecu_lifetime.lw.lbyte;
2732  07b6 180ae200764a      	movb	_d_DtcMemory+5,x,10,y
2733                         ; 1015 					pMsgContext->resData[11] = d_DtcMemory.elem[DtcMemoryIndex].ecu_lifetime.lw.llbyte;
2735  07bc 180ae200774b      	movb	_d_DtcMemory+6,x,11,y
2736                         ; 1017 					pMsgContext->resData[12] = 0x10;
2738  07c2 c610              	ldab	#16
2739  07c4 edf017            	ldy	OFST+7,s
2740  07c7 ed44              	ldy	4,y
2741  07c9 6b4c              	stab	12,y
2742                         ; 1018 					pMsgContext->resData[13] = 0x09;
2744  07cb c609              	ldab	#9
2745  07cd 6b4d              	stab	13,y
2746                         ; 1019 					pMsgContext->resData[14] = HIGHBYTE(d_DtcMemory.elem[DtcMemoryIndex].ecu_time_keyon);
2748  07cf ec84              	ldd	OFST-12,s
2749  07d1 cd000b            	ldy	#11
2750  07d4 13                	emul	
2751  07d5 b746              	tfr	d,y
2752  07d7 e6ea0078          	ldab	_d_DtcMemory+7,y
2753  07db edf017            	ldy	OFST+7,s
2754  07de ed44              	ldy	4,y
2755  07e0 6b4e              	stab	14,y
2756                         ; 1020 					pMsgContext->resData[15] = LOWBYTE(d_DtcMemory.elem[DtcMemoryIndex].ecu_time_keyon);
2758  07e2 ec84              	ldd	OFST-12,s
2759  07e4 cd000b            	ldy	#11
2760  07e7 13                	emul	
2761  07e8 b746              	tfr	d,y
2762  07ea e6ea0079          	ldab	_d_DtcMemory+8,y
2763  07ee edf017            	ldy	OFST+7,s
2764  07f1 ed44              	ldy	4,y
2765  07f3 6b4f              	stab	15,y
2766                         ; 1022 					pMsgContext->resData[16] = 0x20;
2768  07f5 c620              	ldab	#32
2769  07f7 6be810            	stab	16,y
2770                         ; 1023 					pMsgContext->resData[17] = 0x0A;
2772  07fa c60a              	ldab	#10
2773  07fc 6be811            	stab	17,y
2774                         ; 1024 					pMsgContext->resData[18] = HIGHBYTE(d_DtcMemory.elem[DtcMemoryIndex].key_on_counter);
2776  07ff ec84              	ldd	OFST-12,s
2777  0801 cd000b            	ldy	#11
2778  0804 13                	emul	
2779  0805 b746              	tfr	d,y
2780  0807 e6ea007a          	ldab	_d_DtcMemory+9,y
2781  080b edf017            	ldy	OFST+7,s
2782  080e ed44              	ldy	4,y
2783  0810 6be812            	stab	18,y
2784                         ; 1025 					pMsgContext->resData[19] = LOWBYTE(d_DtcMemory.elem[DtcMemoryIndex].key_on_counter);
2786  0813 ec84              	ldd	OFST-12,s
2787  0815 cd000b            	ldy	#11
2788  0818 13                	emul	
2789  0819 b746              	tfr	d,y
2790  081b e6ea007b          	ldab	_d_DtcMemory+10,y
2791  081f edf017            	ldy	OFST+7,s
2792  0822 ed44              	ldy	4,y
2793  0824 6be813            	stab	19,y
2794                         ; 1027 					pMsgContext->resData[20] = 0x60;
2796  0827 c660              	ldab	#96
2797  0829 6be814            	stab	20,y
2798                         ; 1028 					pMsgContext->resData[21] = 0x82;
2800  082c c682              	ldab	#130
2801  082e 6be815            	stab	21,y
2802                         ; 1029 					pMsgContext->resData[22] = pMsgContext->resData[2];
2804  0831 180a42e816        	movb	2,y,22,y
2805                         ; 1031 					temp_offset_nr = 19;
2807  0836 c613              	ldab	#19
2808  0838 6b8a              	stab	OFST-6,s
2809                         ; 1033 					temp_snapshot1_flag = TRUE;
2811  083a c601              	ldab	#1
2812  083c 6b8e              	stab	OFST-2,s
2813                         ; 1035 					*DataLen = 23;
2815  083e cc0017            	ldd	#23
2817  0841 2003              	bra	L3711
2818  0843                   L7611:
2819                         ; 1039 					*DataLen = 4;
2821  0843 cc0004            	ldd	#4
2822  0846                   L3711:
2823  0846 6cf30015          	std	[OFST+5,s]
2824                         ; 1043                if ( ((DtcMemoryIndex = FMemLibLF_SearchDtcIndexInDtcMemory((unsigned char)Index, HISTORICAL_MEMORY)) >= 0) &&
2824                         ; 1044 					     ((temp_snapshot_nr == 0x01) || (temp_snapshot_nr == 0xFF)))
2826  084a cc0001            	ldd	#1
2827  084d 3b                	pshd	
2828  084e e68b              	ldab	OFST-5,s
2829  0850 4a0ebdbd          	call	f_FMemLibLF_SearchDtcIndexInDtcMemory
2831  0854 1b82              	leas	2,s
2832  0856 6c84              	std	OFST-12,s
2833  0858 182d0116          	blt	L5711
2835  085c e68d              	ldab	OFST-3,s
2836  085e c101              	cmpb	#1
2837  0860 2705              	beq	L7711
2839  0862 52                	incb	
2840  0863 1826010b          	bne	L5711
2841  0867                   L7711:
2842                         ; 1047 					pMsgContext->resData[4+temp_offset_nr] = 0x01; //DTC Snapshot Data Record Number First Record
2844  0867 edf017            	ldy	OFST+7,s
2845  086a ee44              	ldx	4,y
2846  086c e68a              	ldab	OFST-6,s
2847  086e 1ae5              	leax	b,x
2848  0870 c601              	ldab	#1
2849  0872 6b04              	stab	4,x
2850                         ; 1048 					pMsgContext->resData[5+temp_offset_nr] = 4; //Number of Records reported
2852  0874 c604              	ldab	#4
2853  0876 6b05              	stab	5,x
2854                         ; 1051 					pMsgContext->resData[6+temp_offset_nr] = 0x10;
2856  0878 c610              	ldab	#16
2857  087a 6b06              	stab	6,x
2858                         ; 1052 					pMsgContext->resData[7+temp_offset_nr] = 0x08;
2860  087c c608              	ldab	#8
2861  087e 6b07              	stab	7,x
2862                         ; 1053 					pMsgContext->resData[8+temp_offset_nr] = d_DtcHistoricalMemory.elem[DtcMemoryIndex].ecu_lifetime.lw.hhbyte;
2864  0880 ec84              	ldd	OFST-12,s
2865  0882 cd0009            	ldy	#9
2866  0885 13                	emul	
2867  0886 b746              	tfr	d,y
2868  0888 180aea001708      	movb	_d_DtcHistoricalMemory,y,8,x
2869                         ; 1054 					pMsgContext->resData[9+temp_offset_nr] = d_DtcHistoricalMemory.elem[DtcMemoryIndex].ecu_lifetime.lw.hbyte;
2871  088e edf017            	ldy	OFST+7,s
2872  0891 ee44              	ldx	4,y
2873  0893 e68a              	ldab	OFST-6,s
2874  0895 1ae5              	leax	b,x
2875  0897 ec84              	ldd	OFST-12,s
2876  0899 cd0009            	ldy	#9
2877  089c 13                	emul	
2878  089d b746              	tfr	d,y
2879  089f 180aea001809      	movb	_d_DtcHistoricalMemory+1,y,9,x
2880                         ; 1055 					pMsgContext->resData[10+temp_offset_nr] = d_DtcHistoricalMemory.elem[DtcMemoryIndex].ecu_lifetime.lw.lbyte;
2882  08a5 edf017            	ldy	OFST+7,s
2883  08a8 ee44              	ldx	4,y
2884  08aa e68a              	ldab	OFST-6,s
2885  08ac 1ae5              	leax	b,x
2886  08ae ec84              	ldd	OFST-12,s
2887  08b0 cd0009            	ldy	#9
2888  08b3 13                	emul	
2889  08b4 b746              	tfr	d,y
2890  08b6 180aea00190a      	movb	_d_DtcHistoricalMemory+2,y,10,x
2891                         ; 1056 					pMsgContext->resData[11+temp_offset_nr] = d_DtcHistoricalMemory.elem[DtcMemoryIndex].ecu_lifetime.lw.llbyte;
2893  08bc edf017            	ldy	OFST+7,s
2894  08bf ee44              	ldx	4,y
2895  08c1 e68a              	ldab	OFST-6,s
2896  08c3 1ae5              	leax	b,x
2897  08c5 ec84              	ldd	OFST-12,s
2898  08c7 cd0009            	ldy	#9
2899  08ca 13                	emul	
2900  08cb b746              	tfr	d,y
2901  08cd 180aea001a0b      	movb	_d_DtcHistoricalMemory+3,y,11,x
2902                         ; 1058 					pMsgContext->resData[12+temp_offset_nr] = 0x10;
2904  08d3 edf017            	ldy	OFST+7,s
2905  08d6 ee44              	ldx	4,y
2906  08d8 e68a              	ldab	OFST-6,s
2907  08da 1ae5              	leax	b,x
2908  08dc c610              	ldab	#16
2909  08de 6b0c              	stab	12,x
2910                         ; 1059 					pMsgContext->resData[13+temp_offset_nr] = 0x09;
2912  08e0 c609              	ldab	#9
2913  08e2 6b0d              	stab	13,x
2914                         ; 1060 					pMsgContext->resData[14+temp_offset_nr] = HIGHBYTE(d_DtcHistoricalMemory.elem[DtcMemoryIndex].ecu_time_keyon);
2916  08e4 ec84              	ldd	OFST-12,s
2917  08e6 cd0009            	ldy	#9
2918  08e9 13                	emul	
2919  08ea b746              	tfr	d,y
2920  08ec 180aea001b0e      	movb	_d_DtcHistoricalMemory+4,y,14,x
2921                         ; 1061 					pMsgContext->resData[15+temp_offset_nr] = LOWBYTE(d_DtcHistoricalMemory.elem[DtcMemoryIndex].ecu_time_keyon);
2923  08f2 edf017            	ldy	OFST+7,s
2924  08f5 ee44              	ldx	4,y
2925  08f7 e68a              	ldab	OFST-6,s
2926  08f9 1ae5              	leax	b,x
2927  08fb ec84              	ldd	OFST-12,s
2928  08fd cd0009            	ldy	#9
2929  0900 13                	emul	
2930  0901 b746              	tfr	d,y
2931  0903 180aea001c0f      	movb	_d_DtcHistoricalMemory+5,y,15,x
2932                         ; 1063 					pMsgContext->resData[16+temp_offset_nr] = 0x20;
2934  0909 edf017            	ldy	OFST+7,s
2935  090c ee44              	ldx	4,y
2936  090e e68a              	ldab	OFST-6,s
2937  0910 1ae5              	leax	b,x
2938  0912 c620              	ldab	#32
2939  0914 6be010            	stab	16,x
2940                         ; 1064 					pMsgContext->resData[17+temp_offset_nr] = 0x0A;
2942  0917 c60a              	ldab	#10
2943  0919 6be011            	stab	17,x
2944                         ; 1065 					pMsgContext->resData[18+temp_offset_nr] = HIGHBYTE(d_DtcHistoricalMemory.elem[DtcMemoryIndex].key_on_counter);
2946  091c ec84              	ldd	OFST-12,s
2947  091e cd0009            	ldy	#9
2948  0921 13                	emul	
2949  0922 b746              	tfr	d,y
2950  0924 180aea001de012    	movb	_d_DtcHistoricalMemory+6,y,18,x
2951                         ; 1066 					pMsgContext->resData[19+temp_offset_nr] = LOWBYTE(d_DtcHistoricalMemory.elem[DtcMemoryIndex].key_on_counter);
2953  092b edf017            	ldy	OFST+7,s
2954  092e ee44              	ldx	4,y
2955  0930 e68a              	ldab	OFST-6,s
2956  0932 1ae5              	leax	b,x
2957  0934 ec84              	ldd	OFST-12,s
2958  0936 cd0009            	ldy	#9
2959  0939 13                	emul	
2960  093a b746              	tfr	d,y
2961  093c 180aea001ee013    	movb	_d_DtcHistoricalMemory+7,y,19,x
2962                         ; 1068 					pMsgContext->resData[20+temp_offset_nr] = 0x60;
2964  0943 edf017            	ldy	OFST+7,s
2965  0946 ee44              	ldx	4,y
2966  0948 e68a              	ldab	OFST-6,s
2967  094a 1ae5              	leax	b,x
2968  094c c660              	ldab	#96
2969  094e 6be014            	stab	20,x
2970                         ; 1069 					pMsgContext->resData[21+temp_offset_nr] = 0x82;
2972  0951 c682              	ldab	#130
2973  0953 6be015            	stab	21,x
2974                         ; 1070 					pMsgContext->resData[22+temp_offset_nr] = pMsgContext->resData[2];
2976  0956 ec44              	ldd	4,y
2977  0958 3b                	pshd	
2978  0959 e68c              	ldab	OFST-4,s
2979  095b b796              	exg	b,y
2980  095d 19e816            	leay	22,y
2981  0960 ce0002            	ldx	#2
2982  0963 3a                	puld	
2983  0964 180ae6ee          	movb	d,x,d,y
2984                         ; 1072 					temp_snapshot2_flag = TRUE;
2986                         ; 1075 					if (temp_snapshot_nr == 0xFF)
2988  0968 e68d              	ldab	OFST-3,s
2989  096a 52                	incb	
2990  096b 260a              	bne	LC003
2991                         ; 1078 						*DataLen = 42;
2993  096d cc002a            	ldd	#42
2995  0970 200d              	bra	L5021
2996                         ; 1083 						*DataLen = 23;
2998  0972                   L5711:
2999                         ; 1088 					if(temp_snapshot1_flag == TRUE)
3001  0972 e68e              	ldab	OFST-2,s
3002  0974 042105            	dbne	b,L7021
3003                         ; 1090 						*DataLen = 23;
3005  0977                   LC003:
3006  0977 cc0017            	ldd	#23
3008  097a 2003              	bra	L5021
3009  097c                   L7021:
3010                         ; 1094 						*DataLen = 4;
3012  097c cc0004            	ldd	#4
3013  097f                   L5021:
3014  097f 6cf30015          	std	[OFST+5,s]
3015                         ; 1108 				*Response = RESPONSE_OK;
3017  0983                   LC005:
3018  0983 c601              	ldab	#1
3020  0985 18200333          	bra	LC004
3021                         ; 1111 				*Response = REQUEST_OUT_OF_RANGE;
3023  0989                   L3201:
3024                         ; 1119 			TempDtcCode.all = 0;
3026  0989 87                	clra	
3027  098a c7                	clrb	
3028  098b 6c80              	std	OFST-16,s
3029  098d 6c82              	std	OFST-14,s
3030                         ; 1122 			TempDtcCode.byte[1] = pMsgContext->reqData[0];
3032  098f edf30017          	ldy	[OFST+7,s]
3033  0993 180a4081          	movb	0,y,OFST-15,s
3034                         ; 1123 			TempDtcCode.byte[2] = pMsgContext->reqData[1];
3036  0997 180a4182          	movb	1,y,OFST-14,s
3037                         ; 1124 			TempDtcCode.byte[3] = pMsgContext->reqData[2];
3039  099b 180a4283          	movb	2,y,OFST-13,s
3040                         ; 1127 			if ((Index = FMemLibF_CheckDtcSupportOverDtc(TempDtcCode.all)) >= 0 &&
3040                         ; 1128 			   (Index = FMemLibF_CheckDtcSupportOverDtc(TempDtcCode.all)) < SIZEOF_ALL_SUPPORTED_DTC) {
3042  099f ec82              	ldd	OFST-14,s
3043  09a1 ee80              	ldx	OFST-16,s
3044  09a3 4a0d7373          	call	f_FMemLibF_CheckDtcSupportOverDtc
3046  09a7 6c88              	std	OFST-8,s
3047  09a9 182d0231          	blt	L7421
3049  09ad ec82              	ldd	OFST-14,s
3050  09af ee80              	ldx	OFST-16,s
3051  09b1 4a0d7373          	call	f_FMemLibF_CheckDtcSupportOverDtc
3053  09b5 6c88              	std	OFST-8,s
3054  09b7 8c002e            	cpd	#46
3055  09ba 182c0220          	bge	L7421
3056                         ; 1130 				pMsgContext->resData[0] = pMsgContext->reqData[0];
3058  09be edf017            	ldy	OFST+7,s
3059  09c1 ed44              	ldy	4,y
3060  09c3 eef30017          	ldx	[OFST+7,s]
3061  09c7 180a0040          	movb	0,x,0,y
3062                         ; 1131 				pMsgContext->resData[1] = pMsgContext->reqData[1];
3064  09cb edf017            	ldy	OFST+7,s
3065  09ce ee44              	ldx	4,y
3066  09d0 ed40              	ldy	0,y
3067  09d2 180a4101          	movb	1,y,1,x
3068                         ; 1132 				pMsgContext->resData[2] = pMsgContext->reqData[2];
3070  09d6 180a4202          	movb	2,y,2,x
3071                         ; 1135 				pMsgContext->resData[3] = d_AllDtc_a[Index].DtcState.all;
3073  09da edf017            	ldy	OFST+7,s
3074  09dd ed44              	ldy	4,y
3075  09df ee88              	ldx	OFST-8,s
3076  09e1 1848              	lslx	
3077  09e3 180ae2000143      	movb	_d_AllDtc_a+1,x,3,y
3078                         ; 1137 				if ((DtcMemoryIndex = FMemLibLF_SearchDtcIndexInDtcMemory((unsigned char)Index, FAULT_MEMORY)) >= 0) {
3080  09e9 87                	clra	
3081  09ea c7                	clrb	
3082  09eb 3b                	pshd	
3083  09ec e68b              	ldab	OFST-5,s
3084  09ee 4a0ebdbd          	call	f_FMemLibLF_SearchDtcIndexInDtcMemory
3086  09f2 1b82              	leas	2,s
3087  09f4 6c84              	std	OFST-12,s
3088  09f6 2d84              	blt	L7021
3089                         ; 1140 					pMsgContext->resData[4] = 1; //DTC Extended Data Record Number
3091  09f8 c601              	ldab	#1
3092  09fa edf017            	ldy	OFST+7,s
3093  09fd ed44              	ldy	4,y
3094  09ff 6b44              	stab	4,y
3095                         ; 1145 					pMsgContext->resData[5] = 0x60;
3097  0a01 c660              	ldab	#96
3098  0a03 6b45              	stab	5,y
3099                         ; 1146 					pMsgContext->resData[6] = 0x80;
3101  0a05 c680              	ldab	#128
3102  0a07 6b46              	stab	6,y
3103                         ; 1147 					pMsgContext->resData[7] = d_DtcMemory.elem[DtcMemoryIndex].operation_cycle_counter;
3105  0a09 ec84              	ldd	OFST-12,s
3106  0a0b b765              	tfr	y,x
3107  0a0d cd000b            	ldy	#11
3108  0a10 13                	emul	
3109  0a11 b756              	tfr	x,y
3110  0a13 b745              	tfr	d,x
3111  0a15 180ae2007247      	movb	_d_DtcMemory+1,x,7,y
3112                         ; 1149 					pMsgContext->resData[8] = 0x60;
3114  0a1b c660              	ldab	#96
3115  0a1d edf017            	ldy	OFST+7,s
3116  0a20 ed44              	ldy	4,y
3117  0a22 6b48              	stab	8,y
3118                         ; 1150 					pMsgContext->resData[9] = 0x81;
3120  0a24 c681              	ldab	#129
3121  0a26 6b49              	stab	9,y
3122                         ; 1151 					pMsgContext->resData[10] = 0x00; //Warning Lamp is OFF
3124  0a28 694a              	clr	10,y
3125                         ; 1153 					*DataLen = 11;
3127  0a2a cc000b            	ldd	#11
3129  0a2d 1820ff4e          	bra	L5021
3130                         ; 1157 					*DataLen = 4;
3132                         ; 1160 				*Response = RESPONSE_OK;
3135                         ; 1163 				*Response = REQUEST_OUT_OF_RANGE;
3137  0a31                   L5201:
3138                         ; 1173 			DtcSearchMask = pMsgContext->reqData[0];
3140  0a31 edf30017          	ldy	[OFST+7,s]
3141  0a35 e640              	ldab	0,y
3142  0a37 87                	clra	
3143  0a38 6c88              	std	OFST-8,s
3144                         ; 1174 			DtcSearchMask &= FMEM_DTC_AVAILABILITY_MASK_K;
3146  0a3a 6a88              	staa	OFST-8,s
3147  0a3c 0d89f0            	bclr	OFST-7,s,240
3148                         ; 1177 			pMsgContext->resData[0] = FMEM_DTC_AVAILABILITY_MASK_K;
3150  0a3f c60f              	ldab	#15
3151  0a41 edf017            	ldy	OFST+7,s
3152  0a44 6beb0004          	stab	[4,y]
3153                         ; 1180 			pMsgContext->resData[1] = D_ISO15031_6DTC_FORMAT_K;//D_ISO14229_1DTC_FORMAT_K;
3155  0a48 ed44              	ldy	4,y
3156                         ; 1183 			pMsgContext->resData[2] = 0x00;
3158  0a4a c7                	clrb	
3159  0a4b 6c41              	std	1,y
3160                         ; 1184 			pMsgContext->resData[3] = FMemLibLF_GetNumberOfMaskedDtc((unsigned char)DtcSearchMask, FAULT_MEMORY);
3162  0a4d 3b                	pshd	
3163  0a4e e68b              	ldab	OFST-5,s
3164  0a50 4a0dd9d9          	call	f_FMemLibLF_GetNumberOfMaskedDtc
3166  0a54 1b82              	leas	2,s
3167  0a56 edf017            	ldy	OFST+7,s
3168  0a59 ed44              	ldy	4,y
3169  0a5b 6b43              	stab	3,y
3170                         ; 1187 			*DataLen = 4;
3172                         ; 1190 			*Response = RESPONSE_OK;
3174                         ; 1195 			break;
3176  0a5d 1820ff1b          	bra	L7021
3177  0a61                   L7201:
3178                         ; 1203 			DtcSearchMask = pMsgContext->reqData[1];
3180  0a61 edf30017          	ldy	[OFST+7,s]
3181  0a65 e641              	ldab	1,y
3182  0a67 87                	clra	
3183  0a68 6c88              	std	OFST-8,s
3184                         ; 1204 			DtcSearchMask &= FMEM_DTC_AVAILABILITY_MASK_K;
3186  0a6a c7                	clrb	
3187  0a6b 6b88              	stab	OFST-8,s
3188  0a6d 0d89f0            	bclr	OFST-7,s,240
3189                         ; 1207 			NumberOfRegisteredDtc = (temp_l)FMemLibLF_GetNumberOfRegisteredDtc(FAULT_MEMORY);
3191  0a70 4a0e2929          	call	f_FMemLibLF_GetNumberOfRegisteredDtc
3193  0a74 6c8b              	std	OFST-5,s
3194                         ; 1210 			if (((FMemLibLF_GetNumberOfMaskedDtc((unsigned char)DtcSearchMask, FAULT_MEMORY)*4)+2) < kDescPrimBufferLen)
3196  0a76 87                	clra	
3197  0a77 c7                	clrb	
3198  0a78 3b                	pshd	
3199  0a79 e68b              	ldab	OFST-5,s
3200  0a7b 4a0dd9d9          	call	f_FMemLibLF_GetNumberOfMaskedDtc
3202  0a7f 1b82              	leas	2,s
3203  0a81 87                	clra	
3204  0a82 59                	lsld	
3205  0a83 59                	lsld	
3206  0a84 c30002            	addd	#2
3207  0a87 8c0102            	cpd	#258
3208  0a8a 182c0150          	bge	L7421
3209                         ; 1213 				pMsgContext->resData[0] = FMEM_DTC_AVAILABILITY_MASK_K;
3211  0a8e c60f              	ldab	#15
3212  0a90 eef017            	ldx	OFST+7,s
3213  0a93 6be30004          	stab	[4,x]
3214                         ; 1215 				if (DtcSearchMask)
3216  0a97 ec88              	ldd	OFST-8,s
3217  0a99 182700d4          	beq	L7221
3218                         ; 1218 					for (DtcMemoryIndex = 0; DtcMemoryIndex < NumberOfRegisteredDtc; DtcMemoryIndex++)
3220  0a9d 186984            	clrw	OFST-12,s
3222  0aa0 182000bb          	bra	L5321
3223  0aa4                   L1321:
3224                         ; 1220 						if (d_AllDtc_a[d_DtcMemory.elem[DtcMemoryIndex].dtc_RomIndex].DtcState.all & DtcSearchMask)
3226  0aa4 cd000b            	ldy	#11
3227  0aa7 13                	emul	
3228  0aa8 b746              	tfr	d,y
3229  0aaa e6ea0073          	ldab	_d_DtcMemory+2,y
3230  0aae 87                	clra	
3231  0aaf 59                	lsld	
3232  0ab0 b745              	tfr	d,x
3233  0ab2 e6e20001          	ldab	_d_AllDtc_a+1,x
3234  0ab6 e489              	andb	OFST-7,s
3235  0ab8 182700a0          	beq	L1421
3236                         ; 1222 							pMsgContext->resData[(NumberOfMaskedDtc*6)+1] = 0; //No severity defined for this program
3238  0abc edf017            	ldy	OFST+7,s
3239  0abf ee44              	ldx	4,y
3240  0ac1 ec86              	ldd	OFST-10,s
3241  0ac3 cd0006            	ldy	#6
3242  0ac6 13                	emul	
3243  0ac7 1ae6              	leax	d,x
3244                         ; 1223 							pMsgContext->resData[(NumberOfMaskedDtc*6)+2] = 0; //No functionality defined for this program
3246  0ac9 87                	clra	
3247  0aca c7                	clrb	
3248  0acb 6c01              	std	1,x
3249                         ; 1224 							pMsgContext->resData[(NumberOfMaskedDtc*6)+3] = FMemLibLF_GetByteOfDtc(d_DtcMemory.elem[DtcMemoryIndex].dtc_RomIndex, 1);
3251  0acd 34                	pshx	
3252  0ace 52                	incb	
3253  0acf 3b                	pshd	
3254  0ad0 ec88              	ldd	OFST-8,s
3255  0ad2 cd000b            	ldy	#11
3256  0ad5 13                	emul	
3257  0ad6 b746              	tfr	d,y
3258  0ad8 e6ea0073          	ldab	_d_DtcMemory+2,y
3259  0adc 87                	clra	
3260  0add 4a0dcbcb          	call	f_FMemLibLF_GetByteOfDtc
3262  0ae1 1b82              	leas	2,s
3263  0ae3 31                	puly	
3264  0ae4 6b43              	stab	3,y
3265                         ; 1225 							pMsgContext->resData[(NumberOfMaskedDtc*6)+4] = FMemLibLF_GetByteOfDtc(d_DtcMemory.elem[DtcMemoryIndex].dtc_RomIndex, 2);
3267  0ae6 edf017            	ldy	OFST+7,s
3268  0ae9 ee44              	ldx	4,y
3269  0aeb ec86              	ldd	OFST-10,s
3270  0aed cd0006            	ldy	#6
3271  0af0 13                	emul	
3272  0af1 1ae6              	leax	d,x
3273  0af3 34                	pshx	
3274  0af4 cc0002            	ldd	#2
3275  0af7 3b                	pshd	
3276  0af8 ec88              	ldd	OFST-8,s
3277  0afa cd000b            	ldy	#11
3278  0afd 13                	emul	
3279  0afe b746              	tfr	d,y
3280  0b00 e6ea0073          	ldab	_d_DtcMemory+2,y
3281  0b04 87                	clra	
3282  0b05 4a0dcbcb          	call	f_FMemLibLF_GetByteOfDtc
3284  0b09 1b82              	leas	2,s
3285  0b0b 31                	puly	
3286  0b0c 6b44              	stab	4,y
3287                         ; 1226 							pMsgContext->resData[(NumberOfMaskedDtc*6)+5] = FMemLibLF_GetByteOfDtc(d_DtcMemory.elem[DtcMemoryIndex].dtc_RomIndex, 3);
3289  0b0e edf017            	ldy	OFST+7,s
3290  0b11 ee44              	ldx	4,y
3291  0b13 ec86              	ldd	OFST-10,s
3292  0b15 cd0006            	ldy	#6
3293  0b18 13                	emul	
3294  0b19 1ae6              	leax	d,x
3295  0b1b 34                	pshx	
3296  0b1c cc0003            	ldd	#3
3297  0b1f 3b                	pshd	
3298  0b20 ec88              	ldd	OFST-8,s
3299  0b22 cd000b            	ldy	#11
3300  0b25 13                	emul	
3301  0b26 b746              	tfr	d,y
3302  0b28 e6ea0073          	ldab	_d_DtcMemory+2,y
3303  0b2c 87                	clra	
3304  0b2d 4a0dcbcb          	call	f_FMemLibLF_GetByteOfDtc
3306  0b31 1b82              	leas	2,s
3307  0b33 31                	puly	
3308  0b34 6b45              	stab	5,y
3309                         ; 1229 							pMsgContext->resData[(NumberOfMaskedDtc*6)+6] = d_AllDtc_a[d_DtcMemory.elem[DtcMemoryIndex].dtc_RomIndex].DtcState.all;
3311  0b36 edf017            	ldy	OFST+7,s
3312  0b39 ee44              	ldx	4,y
3313  0b3b ec86              	ldd	OFST-10,s
3314  0b3d cd0006            	ldy	#6
3315  0b40 13                	emul	
3316  0b41 1ae6              	leax	d,x
3317  0b43 ec84              	ldd	OFST-12,s
3318  0b45 cd000b            	ldy	#11
3319  0b48 13                	emul	
3320  0b49 b746              	tfr	d,y
3321  0b4b e6ea0073          	ldab	_d_DtcMemory+2,y
3322  0b4f 87                	clra	
3323  0b50 59                	lsld	
3324  0b51 b746              	tfr	d,y
3325  0b53 180aea000106      	movb	_d_AllDtc_a+1,y,6,x
3326                         ; 1231 							NumberOfMaskedDtc++;
3328  0b59 186286            	incw	OFST-10,s
3329  0b5c                   L1421:
3330                         ; 1218 					for (DtcMemoryIndex = 0; DtcMemoryIndex < NumberOfRegisteredDtc; DtcMemoryIndex++)
3332  0b5c 186284            	incw	OFST-12,s
3333  0b5f                   L5321:
3336  0b5f ec84              	ldd	OFST-12,s
3337  0b61 ac8b              	cpd	OFST-5,s
3338  0b63 182dff3d          	blt	L1321
3339                         ; 1235 					*DataLen = ((u_temp_l)NumberOfMaskedDtc*6)+1;
3341  0b67 ec86              	ldd	OFST-10,s
3342  0b69 cd0006            	ldy	#6
3343  0b6c 13                	emul	
3345  0b6d 1820fb7b          	bra	LC009
3346  0b71                   L7221:
3347                         ; 1239 					*DataLen = 1;
3349  0b71 cc0001            	ldd	#1
3350                         ; 1243 				*Response = RESPONSE_OK;
3353  0b74 1820fe07          	bra	L5021
3354                         ; 1247 				*Response = REQUEST_OUT_OF_RANGE;
3356  0b78                   L1301:
3357                         ; 1256 			TempDtcCode.all = 0;
3359  0b78 87                	clra	
3360  0b79 c7                	clrb	
3361  0b7a 6c80              	std	OFST-16,s
3362  0b7c 6c82              	std	OFST-14,s
3363                         ; 1259 			TempDtcCode.byte[1] = pMsgContext->reqData[0];
3365  0b7e eef017            	ldx	OFST+7,s
3366  0b81 ed00              	ldy	0,x
3367  0b83 180a4081          	movb	0,y,OFST-15,s
3368                         ; 1260 			TempDtcCode.byte[2] = pMsgContext->reqData[1];
3370  0b87 180a4182          	movb	1,y,OFST-14,s
3371                         ; 1261 			TempDtcCode.byte[3] = pMsgContext->reqData[2];
3373  0b8b 180a4283          	movb	2,y,OFST-13,s
3374                         ; 1263 			pMsgContext->resData[0] = FMEM_DTC_AVAILABILITY_MASK_K;
3376  0b8f c60f              	ldab	#15
3377  0b91 6be30004          	stab	[4,x]
3378                         ; 1267 			if ((Index = FMemLibF_CheckDtcSupportOverDtc(TempDtcCode.all)) >= 0 &&
3378                         ; 1268 			   (Index = FMemLibF_CheckDtcSupportOverDtc(TempDtcCode.all)) < SIZEOF_ALL_SUPPORTED_DTC) {
3380  0b95 ec82              	ldd	OFST-14,s
3381  0b97 ee80              	ldx	OFST-16,s
3382  0b99 4a0d7373          	call	f_FMemLibF_CheckDtcSupportOverDtc
3384  0b9d 6c88              	std	OFST-8,s
3385  0b9f 2d3d              	blt	L7421
3387  0ba1 ec82              	ldd	OFST-14,s
3388  0ba3 ee80              	ldx	OFST-16,s
3389  0ba5 4a0d7373          	call	f_FMemLibF_CheckDtcSupportOverDtc
3391  0ba9 6c88              	std	OFST-8,s
3392  0bab 8c002e            	cpd	#46
3393  0bae 2c2e              	bge	L7421
3394                         ; 1270 				pMsgContext->resData[1] = 0; //Severity not defined for this program 
3396  0bb0 edf017            	ldy	OFST+7,s
3397  0bb3 ed44              	ldy	4,y
3398                         ; 1272 				pMsgContext->resData[2] = 0; /* DTC Functional Unit Not defined for this program */
3400  0bb5 87                	clra	
3401  0bb6 c7                	clrb	
3402  0bb7 6c41              	std	1,y
3403                         ; 1274 				pMsgContext->resData[3] = TempDtcCode.byte[1];
3405  0bb9 180a8143          	movb	OFST-15,s,3,y
3406                         ; 1275 				pMsgContext->resData[4] = TempDtcCode.byte[2];
3408  0bbd 180a8244          	movb	OFST-14,s,4,y
3409                         ; 1276 				pMsgContext->resData[5] = TempDtcCode.byte[3];
3411  0bc1 180a8345          	movb	OFST-13,s,5,y
3412                         ; 1279 				pMsgContext->resData[6] = d_AllDtc_a[Index].DtcState.all;
3414  0bc5 ee88              	ldx	OFST-8,s
3415  0bc7 1848              	lslx	
3416  0bc9 180ae2000146      	movb	_d_AllDtc_a+1,x,6,y
3417                         ; 1281 				*Response = RESPONSE_OK;
3419  0bcf 52                	incb	
3420  0bd0 6bf30010          	stab	[OFST+0,s]
3421                         ; 1282 				*DataLen = 7;
3423  0bd4 c607              	ldab	#7
3424  0bd6 6cf30015          	std	[OFST+5,s]
3426  0bda 182000e2          	bra	L1411
3427  0bde                   L7421:
3428                         ; 1286 				*Response = REQUEST_OUT_OF_RANGE;
3430  0bde c604              	ldab	#4
3431  0be0 182000d8          	bra	LC004
3432  0be4                   L3301:
3433                         ; 1294 			unsigned char temp_dtc_index = 0;
3435  0be4 6983              	clr	OFST-13,s
3436                         ; 1295 			EE_BlockRead(EE_FIRST_CONF_DTC, &temp_dtc_index);
3438  0be6 1a83              	leax	OFST-13,s
3439  0be8 34                	pshx	
3440  0be9 cc002c            	ldd	#44
3441  0bec 4a000000          	call	f_EE_BlockRead
3443  0bf0 1b82              	leas	2,s
3444                         ; 1297 			if(temp_dtc_index >= 0 && temp_dtc_index <= SIZEOF_ALL_SUPPORTED_DTC)
3446  0bf2 e683              	ldab	OFST-13,s
3447  0bf4 c12e              	cmpb	#46
3448  0bf6 1822fd89          	bhi	LC005
3449                         ; 1300 				pMsgContext->resData[0] = FMEM_DTC_AVAILABILITY_MASK_K;
3451  0bfa c60f              	ldab	#15
3452  0bfc eef017            	ldx	OFST+7,s
3453  0bff 6be30004          	stab	[4,x]
3454                         ; 1302 				pMsgContext->resData[1] = FMemLibLF_GetByteOfDtc(temp_dtc_index, 1);
3456  0c03 cc0001            	ldd	#1
3457  0c06 3b                	pshd	
3458  0c07 e685              	ldab	OFST-11,s
3459  0c09 4a0dcbcb          	call	f_FMemLibLF_GetByteOfDtc
3461  0c0d 1b82              	leas	2,s
3462  0c0f edf017            	ldy	OFST+7,s
3463  0c12 ed44              	ldy	4,y
3464  0c14 6b41              	stab	1,y
3465                         ; 1303 				pMsgContext->resData[2] = FMemLibLF_GetByteOfDtc(temp_dtc_index, 2);
3467  0c16 cc0002            	ldd	#2
3468  0c19 3b                	pshd	
3469  0c1a e685              	ldab	OFST-11,s
3470  0c1c 4a0dcbcb          	call	f_FMemLibLF_GetByteOfDtc
3472  0c20 1b82              	leas	2,s
3473  0c22 edf017            	ldy	OFST+7,s
3474  0c25 ed44              	ldy	4,y
3475  0c27 6b42              	stab	2,y
3476                         ; 1304 				pMsgContext->resData[3] = FMemLibLF_GetByteOfDtc(temp_dtc_index, 3);
3478  0c29 cc0003            	ldd	#3
3479  0c2c 3b                	pshd	
3480  0c2d e685              	ldab	OFST-11,s
3481  0c2f 4a0dcbcb          	call	f_FMemLibLF_GetByteOfDtc
3483  0c33 1b82              	leas	2,s
3484  0c35 edf017            	ldy	OFST+7,s
3485  0c38 ed44              	ldy	4,y
3486  0c3a 6b43              	stab	3,y
3487                         ; 1306 				pMsgContext->resData[4] = d_AllDtc_a[temp_dtc_index].DtcState.all;
3489  0c3c e683              	ldab	OFST-13,s
3490  0c3e 87                	clra	
3491  0c3f b745              	tfr	d,x
3492  0c41 1848              	lslx	
3493  0c43 180ae2000144      	movb	_d_AllDtc_a+1,x,4,y
3494                         ; 1308 				*DataLen = 5;
3496  0c49 c605              	ldab	#5
3497                         ; 1309 				*Response = RESPONSE_OK;
3500  0c4b 1820fd30          	bra	L5021
3501                         ; 1314 				*Response = RESPONSE_OK;
3503  0c4f                   L5301:
3504                         ; 1342 			unsigned char temp_dtc_index = 0;
3506  0c4f 6983              	clr	OFST-13,s
3507                         ; 1343 			EE_BlockRead(EE_RECENT_CONF_DTC, &temp_dtc_index);
3509  0c51 1a83              	leax	OFST-13,s
3510  0c53 34                	pshx	
3511  0c54 cc002d            	ldd	#45
3512  0c57 4a000000          	call	f_EE_BlockRead
3514  0c5b 1b82              	leas	2,s
3515                         ; 1345 			if(temp_dtc_index >= 0 && temp_dtc_index <= SIZEOF_ALL_SUPPORTED_DTC)
3517  0c5d e683              	ldab	OFST-13,s
3518  0c5f c12e              	cmpb	#46
3519  0c61 1822fd1e          	bhi	LC005
3520                         ; 1348 				pMsgContext->resData[0] = FMEM_DTC_AVAILABILITY_MASK_K;
3522  0c65 c60f              	ldab	#15
3523  0c67 eef017            	ldx	OFST+7,s
3524  0c6a 6be30004          	stab	[4,x]
3525                         ; 1350 				pMsgContext->resData[1] = FMemLibLF_GetByteOfDtc(temp_dtc_index, 1);
3527  0c6e cc0001            	ldd	#1
3528  0c71 3b                	pshd	
3529  0c72 e685              	ldab	OFST-11,s
3530  0c74 4a0dcbcb          	call	f_FMemLibLF_GetByteOfDtc
3532  0c78 1b82              	leas	2,s
3533  0c7a edf017            	ldy	OFST+7,s
3534  0c7d ed44              	ldy	4,y
3535  0c7f 6b41              	stab	1,y
3536                         ; 1351 				pMsgContext->resData[2] = FMemLibLF_GetByteOfDtc(temp_dtc_index, 2);
3538  0c81 cc0002            	ldd	#2
3539  0c84 3b                	pshd	
3540  0c85 e685              	ldab	OFST-11,s
3541  0c87 4a0dcbcb          	call	f_FMemLibLF_GetByteOfDtc
3543  0c8b 1b82              	leas	2,s
3544  0c8d edf017            	ldy	OFST+7,s
3545  0c90 ed44              	ldy	4,y
3546  0c92 6b42              	stab	2,y
3547                         ; 1352 				pMsgContext->resData[3] = FMemLibLF_GetByteOfDtc(temp_dtc_index, 3);
3549  0c94 cc0003            	ldd	#3
3550  0c97 3b                	pshd	
3551  0c98 e685              	ldab	OFST-11,s
3552  0c9a 4a0dcbcb          	call	f_FMemLibLF_GetByteOfDtc
3554  0c9e 1b82              	leas	2,s
3555  0ca0 edf017            	ldy	OFST+7,s
3556  0ca3 ed44              	ldy	4,y
3557  0ca5 6b43              	stab	3,y
3558                         ; 1354 				pMsgContext->resData[4] = d_AllDtc_a[temp_dtc_index].DtcState.all;
3560  0ca7 e683              	ldab	OFST-13,s
3561  0ca9 87                	clra	
3562  0caa b745              	tfr	d,x
3563  0cac 1848              	lslx	
3564  0cae 180ae2000144      	movb	_d_AllDtc_a+1,x,4,y
3565                         ; 1356 				*DataLen = 5;
3567  0cb4 c605              	ldab	#5
3568                         ; 1357 				*Response = RESPONSE_OK;
3571  0cb6 1820fcc5          	bra	L5021
3572                         ; 1362 				*Response = RESPONSE_OK;
3574  0cba                   L7301:
3575                         ; 1390 			*Response = SUBFUNCTION_NOT_SUPPORTED;
3577  0cba c607              	ldab	#7
3578  0cbc                   LC004:
3579  0cbc 6bf30010          	stab	[OFST+0,s]
3580  0cc0                   L1411:
3581                         ; 1393 }
3584  0cc0 1bf012            	leas	18,s
3585  0cc3 0a                	rtc	
3669                         ; 1395 void FMemLibF_ReportDtc(unsigned long DtcCode, enum ERROR_STATES State)
3669                         ; 1396 {
3670                         	switch	.ftext
3671  0cc4                   f_FMemLibF_ReportDtc:
3673  0cc4 3b                	pshd	
3674  0cc5 34                	pshx	
3675  0cc6 1b9d              	leas	-3,s
3676       00000003          OFST:	set	3
3679                         ; 1405 	unsigned char ReportEnable = 0;
3681  0cc8 6982              	clr	OFST-1,s
3682                         ; 1412 		if ((Index = FMemLibF_CheckDtcSupportOverDtc(DtcCode)) >= 0)
3684  0cca 4a0d7373          	call	f_FMemLibF_CheckDtcSupportOverDtc
3686  0cce 6c80              	std	OFST-3,s
3687  0cd0 2d18              	blt	L1231
3688                         ; 1414 			d_AllDtc_a[Index].reported_state = State;
3690  0cd2 e68b              	ldab	OFST+8,s
3691  0cd4 ed80              	ldy	OFST-3,s
3692  0cd6 1858              	lsly	
3693  0cd8 56                	rorb	
3694  0cd9 56                	rorb	
3695  0cda 56                	rorb	
3696  0cdb 0dea0000c0        	bclr	_d_AllDtc_a,y,192
3697  0ce0 c4c0              	andb	#192
3698  0ce2 eaea0000          	orab	_d_AllDtc_a,y
3699  0ce6 6bea0000          	stab	_d_AllDtc_a,y
3700  0cea                   L1231:
3701                         ; 1417 }
3704  0cea 1b87              	leas	7,s
3705  0cec 0a                	rtc	
3707                         	switch	.data
3708  0000                   L3231_d_OperationCycle10secTimer_c:
3709  0000 00                	dc.b	0
3757                         ; 1455 @far void FMemLibF_CheckDtcConditionTimer(void)
3757                         ; 1456 {
3758                         	switch	.ftext
3759  0ced                   f_FMemLibF_CheckDtcConditionTimer:
3761  0ced 37                	pshb	
3762       00000001          OFST:	set	1
3765                         ; 1464 	IgnitionState = FMemLibF_GetIgnition();
3767  0cee 4a0da8a8          	call	f_FMemLibF_GetIgnition
3769  0cf2 6b80              	stab	OFST-1,s
3770                         ; 1466 	switch (d_OperationCycle_c) {
3772  0cf4 f60014            	ldab	_d_OperationCycle_c
3774  0cf7 2705              	beq	L5231
3775  0cf9 040119            	dbeq	b,L7231
3776                         ; 1514 			d_OperationCycle_c = OPERATION_CYLE_OFF;
3778  0cfc 205a              	bra	LC010
3779  0cfe                   L5231:
3780                         ; 1471 			d_OperationCycle10secTimer_c = 0;
3782  0cfe 790000            	clr	L3231_d_OperationCycle10secTimer_c
3783                         ; 1473 			if( (IgnitionState != IGN_RUN) && (IgnitionState != IGN_START) )
3785  0d01 e680              	ldab	OFST-1,s
3786  0d03 c104              	cmpb	#4
3787  0d05 2754              	beq	L3531
3789  0d07 c105              	cmpb	#5
3790  0d09 2750              	beq	L3531
3791                         ; 1476 				d_HistInterrogRecordEnable_t = FALSE; 
3793  0d0b 790004            	clr	_d_HistInterrogRecordEnable_t
3794                         ; 1478 				d_OperationCycle_c = OPERATION_CYLE_ON;
3796  0d0e c601              	ldab	#1
3797  0d10 7b0014            	stab	_d_OperationCycle_c
3798  0d13 2046              	bra	L3531
3799  0d15                   L7231:
3800                         ; 1484 			if (IgnitionState == IGN_RUN)
3802  0d15 e680              	ldab	OFST-1,s
3803  0d17 c104              	cmpb	#4
3804  0d19 260f              	bne	L7531
3805                         ; 1487 				d_OperationCycle10secTimer_c = FMemLibLF_IncCounter(d_OperationCycle10secTimer_c, D_10_SEC_K);
3807  0d1b cc001f            	ldd	#31
3808  0d1e 3b                	pshd	
3809  0d1f f60000            	ldab	L3231_d_OperationCycle10secTimer_c
3810  0d22 4a106a6a          	call	f_FMemLibLF_IncCounter
3812  0d26 1b82              	leas	2,s
3814  0d28 2001              	bra	L1631
3815  0d2a                   L7531:
3816                         ; 1491 				d_OperationCycle10secTimer_c = 0;
3818  0d2a c7                	clrb	
3819  0d2b                   L1631:
3820  0d2b 7b0000            	stab	L3231_d_OperationCycle10secTimer_c
3821                         ; 1494 			if (d_OperationCycle10secTimer_c == D_10_SEC_K)
3823  0d2e c11f              	cmpb	#31
3824  0d30 2629              	bne	L3531
3825                         ; 1497 				fmem_keyon_counter_w++; //Update the value of IGN Cycles of ECU Life time
3827  0d32 18720006          	incw	_fmem_keyon_counter_w
3828                         ; 1498 				EE_BlockWrite(EE_ECU_KEYON_COUNTER, (u_8Bit*)&fmem_keyon_counter_w); //Write the latest value of IGN Cycles of ECU Life time
3830  0d36 cc0006            	ldd	#_fmem_keyon_counter_w
3831  0d39 3b                	pshd	
3832  0d3a cc0029            	ldd	#41
3833  0d3d 4a000000          	call	f_EE_BlockWrite
3835                         ; 1500 				EE_BlockWrite(EE_ODOMETER, (UINT8*)&canio_RX_TotalOdo_c[0]); //$2001 service
3837  0d41 cc0000            	ldd	#_canio_RX_TotalOdo_c
3838  0d44 6c80              	std	0,s
3839  0d46 cc0025            	ldd	#37
3840  0d49 4a000000          	call	f_EE_BlockWrite
3842  0d4d 1b82              	leas	2,s
3843                         ; 1503 				FMemLibLF_DecDtcOperationCyclCounter();
3845  0d4f 4a0e6f6f          	call	f_FMemLibLF_DecDtcOperationCyclCounter
3847                         ; 1505 				d_HistInterrogRecordEnable_t = TRUE;
3849  0d53 c601              	ldab	#1
3850  0d55 7b0004            	stab	_d_HistInterrogRecordEnable_t
3851                         ; 1507 				d_OperationCycle_c = OPERATION_CYLE_OFF;
3853  0d58                   LC010:
3854  0d58 790014            	clr	_d_OperationCycle_c
3855  0d5b                   L3531:
3856                         ; 1517 	if (IgnitionState == IGN_START)
3858  0d5b e680              	ldab	OFST-1,s
3859  0d5d c105              	cmpb	#5
3860  0d5f 2607              	bne	L5631
3861                         ; 1519 		d_IgnitionStart5secTimer_c = D_5_SEC_K;
3863  0d61 c610              	ldab	#16
3864  0d63 7b0016            	stab	_d_IgnitionStart5secTimer_c
3866  0d66 2008              	bra	L7631
3867  0d68                   L5631:
3868                         ; 1523 		if (d_IgnitionStart5secTimer_c > 0)
3870  0d68 f70016            	tst	_d_IgnitionStart5secTimer_c
3871  0d6b 2703              	beq	L7631
3872                         ; 1525 			d_IgnitionStart5secTimer_c--;
3874  0d6d 730016            	dec	_d_IgnitionStart5secTimer_c
3875  0d70                   L7631:
3876                         ; 1529 }
3879  0d70 1b81              	leas	1,s
3880  0d72 0a                	rtc	
3920                         ; 1531 temp_l FMemLibF_CheckDtcSupportOverDtc(DtcType DtcCode)
3920                         ; 1532 //s_8Bit FMemLibF_CheckDtcSupportOverDtc(DtcType DtcCode)
3920                         ; 1533 {
3921                         	switch	.ftext
3922  0d73                   f_FMemLibF_CheckDtcSupportOverDtc:
3924  0d73 3b                	pshd	
3925  0d74 34                	pshx	
3926  0d75 3b                	pshd	
3927       00000002          OFST:	set	2
3930                         ; 1537 	for (Index = 0; Index < SIZEOF_ALL_SUPPORTED_DTC; Index++)
3932  0d76 87                	clra	
3933  0d77 c7                	clrb	
3934  0d78 6c80              	std	OFST-2,s
3935  0d7a                   L1141:
3936                         ; 1539 		if (fmem_AllRegisteredDtc_t[Index].dtc_code.all == DtcCode)
3938  0d7a cd0000            	ldy	#_fmem_AllRegisteredDtc_t
3939  0d7d 3b                	pshd	
3940  0d7e 59                	lsld	
3941  0d7f e3b1              	addd	2,s+
3942  0d81 59                	lsld	
3943  0d82 19ee              	leay	d,y
3944  0d84 ec40              	ldd	0,y
3945  0d86 ac82              	cpd	OFST+0,s
3946  0d88 260a              	bne	L7141
3947  0d8a ec42              	ldd	2,y
3948  0d8c ac84              	cpd	OFST+2,s
3949  0d8e 2604              	bne	L7141
3950                         ; 1544 				return Index;
3952  0d90 ec80              	ldd	OFST-2,s
3954  0d92 200d              	bra	L65
3955  0d94                   L7141:
3956                         ; 1537 	for (Index = 0; Index < SIZEOF_ALL_SUPPORTED_DTC; Index++)
3958  0d94 186280            	incw	OFST-2,s
3961  0d97 ec80              	ldd	OFST-2,s
3962  0d99 8c002e            	cpd	#46
3963  0d9c 2ddc              	blt	L1141
3964                         ; 1553 	return -1;
3966  0d9e ccffff            	ldd	#-1
3968  0da1                   L65:
3970  0da1 1b86              	leas	6,s
3971  0da3 0a                	rtc	
3995                         ; 1557 u_temp_l FMemLibF_GetOdometer(void)
3995                         ; 1558 {
3996                         	switch	.ftext
3997  0da4                   f_FMemLibF_GetOdometer:
4001                         ; 1560 	return canio_RX_TravelDistance_w;
4003  0da4 fc0000            	ldd	_canio_RX_TravelDistance_w
4006  0da7 0a                	rtc	
4030                         ; 1563 unsigned char FMemLibF_GetIgnition(void)
4030                         ; 1564 {
4031                         	switch	.ftext
4032  0da8                   f_FMemLibF_GetIgnition:
4036                         ; 1566 	return canio_RX_IGN_STATUS_c;
4038  0da8 f60000            	ldab	_canio_RX_IGN_STATUS_c
4041  0dab 0a                	rtc	
4073                         ; 1569 u_temp_l FMemLibF_IsOdometerValid(unsigned short Odometer)
4073                         ; 1570 {
4074                         	switch	.ftext
4075  0dac                   f_FMemLibF_IsOdometerValid:
4079                         ; 1571 	if (Odometer != CANBUS_KILOMETER_INVALID_K)
4081  0dac 048403            	ibeq	d,L5541
4082                         ; 1574 		return 1;
4084  0daf cc0001            	ldd	#1
4087  0db2                   L5541:
4088                         ; 1579 		return 0;
4092  0db2 0a                	rtc	
4125                         ; 1583 void FMemLibLF_ClearSingleDtc(unsigned char DtcIndex)
4125                         ; 1584 {
4126                         	switch	.ftext
4127  0db3                   f_FMemLibLF_ClearSingleDtc:
4129       00000000          OFST:	set	0
4132                         ; 1585 	d_AllDtc_a[DtcIndex].dtc_on_time = 0x00;
4134  0db3 87                	clra	
4135  0db4 59                	lsld	
4136  0db5 b746              	tfr	d,y
4137  0db7 0dea00003f        	bclr	_d_AllDtc_a,y,63
4138                         ; 1586 	d_AllDtc_a[DtcIndex].DtcState.all = 0x00;
4140  0dbc 69ea0001          	clr	_d_AllDtc_a+1,y
4141                         ; 1587 	d_AllDtc_a[DtcIndex].reported_state = ERROR_NO_CHECK_K;
4143  0dc0 0dea0000c0        	bclr	_d_AllDtc_a,y,192
4144  0dc5 0cea000080        	bset	_d_AllDtc_a,y,128
4145                         ; 1589 }
4148  0dca 0a                	rtc	
4188                         ; 1595 unsigned char FMemLibLF_GetByteOfDtc(unsigned char DtcIndex, unsigned char Byte)
4188                         ; 1596 {
4189                         	switch	.ftext
4190  0dcb                   f_FMemLibLF_GetByteOfDtc:
4192       fffffffe          OFST:	set	-2
4195                         ; 1598 	return fmem_AllRegisteredDtc_t[DtcIndex].dtc_code.byte[Byte];
4197  0dcb 8606              	ldaa	#6
4198  0dcd 12                	mul	
4199  0dce b746              	tfr	d,y
4200  0dd0 e684              	ldab	OFST+6,s
4201  0dd2 19ed              	leay	b,y
4202  0dd4 e6ea0000          	ldab	_fmem_AllRegisteredDtc_t,y
4205  0dd8 0a                	rtc	
4272                         ; 1603 unsigned char FMemLibLF_GetNumberOfMaskedDtc(unsigned char DtcSearchMask, enum FAULTMEMORY_TYP FaultMemoryTyp)
4272                         ; 1604 {
4273                         	switch	.ftext
4274  0dd9                   f_FMemLibLF_GetNumberOfMaskedDtc:
4276  0dd9 3b                	pshd	
4277  0dda 1b9d              	leas	-3,s
4278       00000003          OFST:	set	3
4281                         ; 1607   unsigned char Index, NumberOfRegisteredDtc, NumberOfMaskedDtc = 0;
4283  0ddc 6981              	clr	OFST-2,s
4284                         ; 1609 	switch (FaultMemoryTyp) {
4286  0dde e689              	ldab	OFST+6,s
4288  0de0 2705              	beq	L3151
4289  0de2 04011c            	dbeq	b,L5151
4290  0de5 203d              	bra	L7451
4291  0de7                   L3151:
4292                         ; 1612 			for (Index = 0; Index < SIZEOF_ALL_SUPPORTED_DTC; Index++)
4294  0de7 6b80              	stab	OFST-3,s
4295  0de9                   L1551:
4296                         ; 1614 				if (d_AllDtc_a[Index].DtcState.all & DtcSearchMask)
4298  0de9 87                	clra	
4299  0dea 59                	lsld	
4300  0deb b746              	tfr	d,y
4301  0ded e6ea0001          	ldab	_d_AllDtc_a+1,y
4302  0df1 e484              	andb	OFST+1,s
4303  0df3 2702              	beq	L7551
4304                         ; 1616 					NumberOfMaskedDtc++;
4306  0df5 6281              	inc	OFST-2,s
4307  0df7                   L7551:
4308                         ; 1612 			for (Index = 0; Index < SIZEOF_ALL_SUPPORTED_DTC; Index++)
4310  0df7 6280              	inc	OFST-3,s
4313  0df9 e680              	ldab	OFST-3,s
4314  0dfb c12e              	cmpb	#46
4315  0dfd 25ea              	blo	L1551
4316                         ; 1619 			break;
4318  0dff 2023              	bra	L7451
4319  0e01                   L5151:
4320                         ; 1623 			NumberOfRegisteredDtc = (unsigned char)FMemLibLF_GetNumberOfRegisteredDtc(HISTORICAL_MEMORY);
4322  0e01 cc0001            	ldd	#1
4323  0e04 4a0e2929          	call	f_FMemLibLF_GetNumberOfRegisteredDtc
4325  0e08 6b82              	stab	OFST-1,s
4326                         ; 1624 			for (Index = 0; Index < NumberOfRegisteredDtc; Index++)
4328  0e0a 6980              	clr	OFST-3,s
4330  0e0c 2010              	bra	L5651
4331  0e0e                   L1651:
4332                         ; 1627 				if(d_DtcHistoricalMemory.elem[Index].dtc_RomIndex !=  EMPTY_SECTION)
4334  0e0e 8609              	ldaa	#9
4335  0e10 12                	mul	
4336  0e11 b746              	tfr	d,y
4337  0e13 e6ea001f          	ldab	_d_DtcHistoricalMemory+8,y
4338  0e17 048102            	ibeq	b,L1751
4339                         ; 1629 					NumberOfMaskedDtc++;
4341  0e1a 6281              	inc	OFST-2,s
4342  0e1c                   L1751:
4343                         ; 1624 			for (Index = 0; Index < NumberOfRegisteredDtc; Index++)
4345  0e1c 6280              	inc	OFST-3,s
4346  0e1e                   L5651:
4349  0e1e e680              	ldab	OFST-3,s
4350  0e20 e182              	cmpb	OFST-1,s
4351  0e22 25ea              	blo	L1651
4352                         ; 1632 			break;
4354  0e24                   L7451:
4355                         ; 1636 	return NumberOfMaskedDtc;
4357  0e24 e681              	ldab	OFST-2,s
4360  0e26 1b85              	leas	5,s
4361  0e28 0a                	rtc	
4405                         ; 1641 u_temp_l FMemLibLF_GetNumberOfRegisteredDtc(enum FAULTMEMORY_TYP FaultMemoryTyp)
4405                         ; 1642 {
4406                         	switch	.ftext
4407  0e29                   f_FMemLibLF_GetNumberOfRegisteredDtc:
4409  0e29 3b                	pshd	
4410  0e2a 3b                	pshd	
4411       00000002          OFST:	set	2
4414                         ; 1643 	u_temp_l Number = 0;
4416  0e2b 186980            	clrw	OFST-2,s
4417                         ; 1645 	switch (FaultMemoryTyp) {
4420  0e2e 044108            	tbeq	b,L5261
4421  0e31 040120            	dbeq	b,L5361
4422  0e34 2034              	bra	L1261
4423  0e36                   L3261:
4424                         ; 1650 				Number++;
4426  0e36 186280            	incw	OFST-2,s
4427  0e39                   L5261:
4428                         ; 1648 			while ((d_DtcMemory.elem[Number].dtc_RomIndex != 0xFF) && (Number < (SIZEOF_DTC_MEMORY) ) )
4430  0e39 ec80              	ldd	OFST-2,s
4431  0e3b cd000b            	ldy	#11
4432  0e3e 13                	emul	
4433  0e3f b746              	tfr	d,y
4434  0e41 e6ea0073          	ldab	_d_DtcMemory+2,y
4435  0e45 048122            	ibeq	b,L1261
4437  0e48 ec80              	ldd	OFST-2,s
4438  0e4a 8c000a            	cpd	#10
4439  0e4d 25e7              	blo	L3261
4440  0e4f 2019              	bra	L1261
4441  0e51                   L3361:
4442                         ; 1658 				Number++;
4444  0e51 186280            	incw	OFST-2,s
4445  0e54                   L5361:
4446                         ; 1656 			while ((d_DtcHistoricalMemory.elem[Number].dtc_RomIndex != 0xFF) && (Number < (SIZEOF_DTC_HISTORICAL_MEMORY)))
4448  0e54 ec80              	ldd	OFST-2,s
4449  0e56 cd0009            	ldy	#9
4450  0e59 13                	emul	
4451  0e5a b746              	tfr	d,y
4452  0e5c e6ea001f          	ldab	_d_DtcHistoricalMemory+8,y
4453  0e60 048107            	ibeq	b,L1261
4455  0e63 ec80              	ldd	OFST-2,s
4456  0e65 8c000a            	cpd	#10
4457  0e68 25e7              	blo	L3361
4458  0e6a                   L1261:
4459                         ; 1664 	return Number;
4461  0e6a ec80              	ldd	OFST-2,s
4464  0e6c 1b84              	leas	4,s
4465  0e6e 0a                	rtc	
4510                         ; 1678 void FMemLibLF_DecDtcOperationCyclCounter(void)
4510                         ; 1679 {
4511                         	switch	.ftext
4512  0e6f                   f_FMemLibLF_DecDtcOperationCyclCounter:
4514  0e6f 3b                	pshd	
4515       00000002          OFST:	set	2
4518                         ; 1683 	NumberOfRegisteredDtc = (unsigned char)FMemLibLF_GetNumberOfRegisteredDtc(FAULT_MEMORY);
4520  0e70 87                	clra	
4521  0e71 c7                	clrb	
4522  0e72 4a0e2929          	call	f_FMemLibLF_GetNumberOfRegisteredDtc
4524  0e76 6b81              	stab	OFST-1,s
4525                         ; 1685 	for (Index = 0; Index < NumberOfRegisteredDtc; Index++)
4527  0e78 6980              	clr	OFST-2,s
4529  0e7a 2039              	bra	L5661
4530  0e7c                   L1661:
4531                         ; 1690 		if ( (d_AllDtc_a[d_DtcMemory.elem[Index].dtc_RomIndex].DtcState.all & FMEM_DTC_AVAILABILITY_MASK_K ) == FMEM_DTC_STORED_NOT_ACTIVE_MASK_K )
4533  0e7c 860b              	ldaa	#11
4534  0e7e 12                	mul	
4535  0e7f b746              	tfr	d,y
4536  0e81 e6ea0073          	ldab	_d_DtcMemory+2,y
4537  0e85 87                	clra	
4538  0e86 59                	lsld	
4539  0e87 b745              	tfr	d,x
4540  0e89 e6e20001          	ldab	_d_AllDtc_a+1,x
4541  0e8d c40f              	andb	#15
4542  0e8f c108              	cmpb	#8
4543  0e91 2620              	bne	L1761
4544                         ; 1692 			d_DtcMemory.elem[Index].operation_cycle_counter = FMemLibLF_DecCounter(d_DtcMemory.elem[Index].operation_cycle_counter, D_SELF_HEALING_CRITERION_K);
4546  0e93 35                	pshy	
4547  0e94 cc0028            	ldd	#40
4548  0e97 3b                	pshd	
4549  0e98 e684              	ldab	OFST+2,s
4550  0e9a 860b              	ldaa	#11
4551  0e9c 12                	mul	
4552  0e9d b746              	tfr	d,y
4553  0e9f e6ea0072          	ldab	_d_DtcMemory+1,y
4554  0ea3 87                	clra	
4555  0ea4 4a117777          	call	f_FMemLibLF_DecCounter
4557  0ea8 1b82              	leas	2,s
4558  0eaa 31                	puly	
4559  0eab 6bea0072          	stab	_d_DtcMemory+1,y
4560                         ; 1694 			update_chrono_stack_b = TRUE;   
4562  0eaf 1c000008          	bset	_diag_status_flags_c,8
4563  0eb3                   L1761:
4564                         ; 1685 	for (Index = 0; Index < NumberOfRegisteredDtc; Index++)
4566  0eb3 6280              	inc	OFST-2,s
4567  0eb5                   L5661:
4570  0eb5 e680              	ldab	OFST-2,s
4571  0eb7 e181              	cmpb	OFST-1,s
4572  0eb9 25c1              	blo	L1661
4573                         ; 1698 }
4576  0ebb 31                	puly	
4577  0ebc 0a                	rtc	
4635                         ; 1705 temp_l FMemLibLF_SearchDtcIndexInDtcMemory(unsigned char DtcIndex, enum FAULTMEMORY_TYP FaultMemoryTyp)
4635                         ; 1706 {
4636                         	switch	.ftext
4637  0ebd                   f_FMemLibLF_SearchDtcIndexInDtcMemory:
4639  0ebd 3b                	pshd	
4640  0ebe 1b9c              	leas	-4,s
4641       00000004          OFST:	set	4
4644                         ; 1710 	switch (FaultMemoryTyp) {
4646  0ec0 e68a              	ldab	OFST+6,s
4648  0ec2 2705              	beq	L3761
4649  0ec4 040130            	dbeq	b,L5761
4650  0ec7 2054              	bra	L5271
4651  0ec9                   L3761:
4652                         ; 1713 			for (Index = 0; Index < SIZEOF_DTC_MEMORY; Index++)
4654  0ec9 87                	clra	
4655  0eca 6c80              	std	OFST-4,s
4656  0ecc                   L7271:
4657                         ; 1715 				if (d_DtcMemory.elem[Index].dtc_RomIndex == DtcIndex)
4659  0ecc cd000b            	ldy	#11
4660  0ecf 13                	emul	
4661  0ed0 b746              	tfr	d,y
4662  0ed2 e6ea0073          	ldab	_d_DtcMemory+2,y
4663  0ed6 e185              	cmpb	OFST+1,s
4664  0ed8 2604              	bne	L5371
4665                         ; 1718 					return Index;
4667  0eda ec80              	ldd	OFST-4,s
4669  0edc 200a              	bra	L201
4670  0ede                   L5371:
4671                         ; 1724 					if (Index == (SIZEOF_DTC_MEMORY-1))
4673  0ede ec80              	ldd	OFST-4,s
4674  0ee0 8c0009            	cpd	#9
4675  0ee3 2606              	bne	L7371
4676                         ; 1726 						return -1;
4678  0ee5                   LC011:
4679  0ee5 ccffff            	ldd	#-1
4681  0ee8                   L201:
4683  0ee8 1b86              	leas	6,s
4684  0eea 0a                	rtc	
4685  0eeb                   L7371:
4686                         ; 1713 			for (Index = 0; Index < SIZEOF_DTC_MEMORY; Index++)
4688  0eeb 186280            	incw	OFST-4,s
4691  0eee ec80              	ldd	OFST-4,s
4692  0ef0 8c000a            	cpd	#10
4693  0ef3 25d7              	blo	L7271
4694                         ; 1735 			break;
4696  0ef5 2026              	bra	L5271
4697  0ef7                   L5761:
4698                         ; 1739 			for (Index = 0; Index < SIZEOF_DTC_HISTORICAL_MEMORY; Index++)
4700  0ef7 87                	clra	
4701  0ef8 6c80              	std	OFST-4,s
4702  0efa                   L5471:
4703                         ; 1741 				if (d_DtcHistoricalMemory.elem[Index].dtc_RomIndex == DtcIndex)
4705  0efa cd0009            	ldy	#9
4706  0efd 13                	emul	
4707  0efe b746              	tfr	d,y
4708  0f00 e6ea001f          	ldab	_d_DtcHistoricalMemory+8,y
4709  0f04 e185              	cmpb	OFST+1,s
4710  0f06 2604              	bne	L3571
4711                         ; 1744 					return Index;
4713  0f08 ec80              	ldd	OFST-4,s
4715  0f0a 20dc              	bra	L201
4716  0f0c                   L3571:
4717                         ; 1750 					if (Index == (SIZEOF_DTC_MEMORY-1))
4719  0f0c ec80              	ldd	OFST-4,s
4720  0f0e 8c0009            	cpd	#9
4721                         ; 1752 						return -1;
4724  0f11 27d2              	beq	LC011
4725                         ; 1739 			for (Index = 0; Index < SIZEOF_DTC_HISTORICAL_MEMORY; Index++)
4727  0f13 186280            	incw	OFST-4,s
4730  0f16 ec80              	ldd	OFST-4,s
4731  0f18 8c000a            	cpd	#10
4732  0f1b 25dd              	blo	L5471
4733                         ; 1761 			break;
4735  0f1d                   L5271:
4736                         ; 1764 	return (Indexvalue);
4738  0f1d ec82              	ldd	OFST-2,s
4740  0f1f 20c7              	bra	L201
4797                         ; 1767 static temp_l FMemLibLF_SearchDtcIndexWithLowerPriority(u_temp_l Priority)
4797                         ; 1768 {
4798                         	switch	.ftext
4799  0f21                   L751f_FMemLibLF_SearchDtcIndexWithLowerPriority:
4801  0f21 3b                	pshd	
4802  0f22 1b9a              	leas	-6,s
4803       00000006          OFST:	set	6
4806                         ; 1770 	temp_l Index, IndexMerker = 0, LowestPriority = 0;
4808  0f24 87                	clra	
4809  0f25 c7                	clrb	
4810  0f26 6c84              	std	OFST-2,s
4813  0f28 6c82              	std	OFST-4,s
4814                         ; 1772 	for (Index = 0; Index < SIZEOF_DTC_MEMORY; Index++) {
4816  0f2a 6c80              	std	OFST-6,s
4817  0f2c                   L5002:
4818                         ; 1773 		if (fmem_AllRegisteredDtc_t[d_DtcMemory.elem[Index].dtc_RomIndex].dtc_priority >= LowestPriority)
4820  0f2c cd000b            	ldy	#11
4821  0f2f 13                	emul	
4822  0f30 b746              	tfr	d,y
4823  0f32 e6ea0073          	ldab	_d_DtcMemory+2,y
4824  0f36 8606              	ldaa	#6
4825  0f38 12                	mul	
4826  0f39 b746              	tfr	d,y
4827  0f3b e6ea0004          	ldab	_fmem_AllRegisteredDtc_t+4,y
4828  0f3f 55                	rolb	
4829  0f40 55                	rolb	
4830  0f41 55                	rolb	
4831  0f42 c403              	andb	#3
4832  0f44 87                	clra	
4833  0f45 ac82              	cpd	OFST-4,s
4834  0f47 2d2c              	blt	L3102
4835                         ; 1776 			if (d_AllDtc_a[d_DtcMemory.elem[Index].dtc_RomIndex].DtcState.state.TestFailed == FALSE)
4837  0f49 ec80              	ldd	OFST-6,s
4838  0f4b cd000b            	ldy	#11
4839  0f4e 13                	emul	
4840  0f4f b746              	tfr	d,y
4841  0f51 e6ea0073          	ldab	_d_DtcMemory+2,y
4842  0f55 87                	clra	
4843  0f56 b745              	tfr	d,x
4844  0f58 1848              	lslx	
4845  0f5a 0ee200010115      	brset	_d_AllDtc_a+1,x,1,L3102
4846                         ; 1779 				LowestPriority = fmem_AllRegisteredDtc_t[d_DtcMemory.elem[Index].dtc_RomIndex].dtc_priority;
4848  0f60 8606              	ldaa	#6
4849  0f62 12                	mul	
4850  0f63 b746              	tfr	d,y
4851  0f65 e6ea0004          	ldab	_fmem_AllRegisteredDtc_t+4,y
4852  0f69 55                	rolb	
4853  0f6a 55                	rolb	
4854  0f6b 55                	rolb	
4855  0f6c c403              	andb	#3
4856  0f6e 87                	clra	
4857  0f6f 6c82              	std	OFST-4,s
4858                         ; 1780 				IndexMerker = Index;
4860  0f71 18028084          	movw	OFST-6,s,OFST-2,s
4861  0f75                   L3102:
4862                         ; 1772 	for (Index = 0; Index < SIZEOF_DTC_MEMORY; Index++) {
4864  0f75 186280            	incw	OFST-6,s
4867  0f78 ec80              	ldd	OFST-6,s
4868  0f7a 8c000a            	cpd	#10
4869  0f7d 2dad              	blt	L5002
4870                         ; 1784 	if (LowestPriority >= (temp_l)Priority)
4872  0f7f ec82              	ldd	OFST-4,s
4873  0f81 ac86              	cpd	OFST+0,s
4874  0f83 2d04              	blt	L7102
4875                         ; 1787 		return IndexMerker;
4877  0f85 ec84              	ldd	OFST-2,s
4879  0f87 2003              	bra	L601
4880  0f89                   L7102:
4881                         ; 1792 		return -1;
4883  0f89 ccffff            	ldd	#-1
4885  0f8c                   L601:
4887  0f8c 1b88              	leas	8,s
4888  0f8e 0a                	rtc	
5002                         ; 1802 void FMemLibLF_TransferFaultToHistoricalStack(temp_l StartIndex, temp_l Number)
5002                         ; 1803 {
5003                         	switch	.ftext
5004  0f8f                   f_FMemLibLF_TransferFaultToHistoricalStack:
5006  0f8f 3b                	pshd	
5007  0f90 1b93              	leas	-13,s
5008       0000000d          OFST:	set	13
5011                         ; 1824 	for (Index = StartIndex+Number-1; Index >= StartIndex; Index--)
5013  0f92 e3f012            	addd	OFST+5,s
5014  0f95 c3ffff            	addd	#-1
5015  0f98 6c82              	std	OFST-11,s
5017  0f9a 182000c3          	bra	L3702
5018  0f9e                   L7602:
5019                         ; 1827 		if ((DtcHistoricalMemoryIndex = FMemLibLF_SearchDtcIndexInDtcMemory(d_DtcMemory.elem[Index].dtc_RomIndex, HISTORICAL_MEMORY)) >= 0)
5021  0f9e cc0001            	ldd	#1
5022  0fa1 3b                	pshd	
5023  0fa2 ec84              	ldd	OFST-9,s
5024  0fa4 cd000b            	ldy	#11
5025  0fa7 13                	emul	
5026  0fa8 b746              	tfr	d,y
5027  0faa e6ea0073          	ldab	_d_DtcMemory+2,y
5028  0fae 87                	clra	
5029  0faf 4a0ebdbd          	call	f_FMemLibLF_SearchDtcIndexInDtcMemory
5031  0fb3 1b82              	leas	2,s
5032  0fb5 6c80              	std	OFST-13,s
5033  0fb7 2d68              	blt	L7702
5034                         ; 1832 			d_DtcHistoricalMemory.elem[DtcHistoricalMemoryIndex].ecu_lifetime = fmem_lifetime_l;
5036  0fb9 cd0009            	ldy	#9
5037  0fbc 13                	emul	
5038  0fbd b746              	tfr	d,y
5039  0fbf 18cb0017          	addy	#_d_DtcHistoricalMemory
5040  0fc3 180140000f        	movw	_fmem_lifetime_l,0,y
5041  0fc8 1801420011        	movw	_fmem_lifetime_l+2,2,y
5042                         ; 1833 			d_DtcHistoricalMemory.elem[DtcHistoricalMemoryIndex].ecu_time_keyon = fmem_keyon_time_w;
5044  0fcd ec80              	ldd	OFST-13,s
5045  0fcf cd0009            	ldy	#9
5046  0fd2 13                	emul	
5047  0fd3 b746              	tfr	d,y
5048  0fd5 1801ea001b000d    	movw	_fmem_keyon_time_w,_d_DtcHistoricalMemory+4,y
5049                         ; 1834 			d_DtcHistoricalMemory.elem[DtcHistoricalMemoryIndex].key_on_counter = fmem_keyon_counter_w;
5051  0fdc 1801ea001d0006    	movw	_fmem_keyon_counter_w,_d_DtcHistoricalMemory+6,y
5052                         ; 1836 			fmemLF_MemCpy((unsigned char*)&TempDtcHistMemoryElement, (unsigned char*)&d_DtcHistoricalMemory.elem[DtcHistoricalMemoryIndex], sizeof(struct DTC_HISTORICAL_MEMORY_ELEMENT));
5054  0fe3 1984              	leay	OFST-9,s
5055  0fe5 ec80              	ldd	OFST-13,s
5056  0fe7 3b                	pshd	
5057  0fe8 59                	lsld	
5058  0fe9 59                	lsld	
5059  0fea 59                	lsld	
5060  0feb e3b1              	addd	2,s+
5061  0fed b745              	tfr	d,x
5062  0fef 188b0017          	addx	#_d_DtcHistoricalMemory
5063  0ff3 cc0009            	ldd	#9
5064  0ff6                   L211:
5065  0ff6 180a3070          	movb	1,x+,1,y+
5066  0ffa 0434f9            	dbne	d,L211
5067                         ; 1838 			fmemLF_MemMove((unsigned char*)&d_DtcHistoricalMemory.elem[1], (unsigned char*)&d_DtcHistoricalMemory.elem[0], (sizeof(struct DTC_HISTORICAL_MEMORY_ELEMENT)* DtcHistoricalMemoryIndex));
5069  0ffd ec80              	ldd	OFST-13,s
5070  0fff cd0009            	ldy	#9
5071  1002 13                	emul	
5072  1003 3b                	pshd	
5073  1004 cc0017            	ldd	#_d_DtcHistoricalMemory
5074  1007 3b                	pshd	
5075  1008 cc0020            	ldd	#_d_DtcHistoricalMemory+9
5076  100b 160000            	jsr	_memmove
5078  100e 1b84              	leas	4,s
5079                         ; 1840 			fmemLF_MemCpy((unsigned char*)&d_DtcHistoricalMemory, (unsigned char*)&TempDtcHistMemoryElement, sizeof(struct DTC_HISTORICAL_MEMORY_ELEMENT));
5081  1010 cd0017            	ldy	#_d_DtcHistoricalMemory
5082  1013 1a84              	leax	OFST-9,s
5083  1015 cc0009            	ldd	#9
5084  1018                   L411:
5085  1018 180a3070          	movb	1,x+,1,y+
5086  101c 0434f9            	dbne	d,L411
5087                         ; 1843 			update_historical_stack_b = TRUE;
5090  101f 2037              	bra	L1012
5091  1021                   L7702:
5092                         ; 1849 			fmemLF_MemMove((unsigned char*)&d_DtcHistoricalMemory.elem[1], (unsigned char*)&d_DtcHistoricalMemory.elem[0], (sizeof(struct DTC_HISTORICAL_MEMORY_ELEMENT)*(SIZEOF_DTC_HISTORICAL_MEMORY-1)));
5094  1021 cc0051            	ldd	#81
5095  1024 3b                	pshd	
5096  1025 cc0017            	ldd	#_d_DtcHistoricalMemory
5097  1028 3b                	pshd	
5098  1029 cc0020            	ldd	#_d_DtcHistoricalMemory+9
5099  102c 160000            	jsr	_memmove
5101  102f 1b84              	leas	4,s
5102                         ; 1851 			d_DtcHistoricalMemory.elem[0].ecu_lifetime = fmem_lifetime_l;
5104  1031 1804000f0017      	movw	_fmem_lifetime_l,_d_DtcHistoricalMemory
5105  1037 180400110019      	movw	_fmem_lifetime_l+2,_d_DtcHistoricalMemory+2
5106                         ; 1852 			d_DtcHistoricalMemory.elem[0].ecu_time_keyon = fmem_keyon_time_w;
5108  103d 1804000d001b      	movw	_fmem_keyon_time_w,_d_DtcHistoricalMemory+4
5109                         ; 1853 			d_DtcHistoricalMemory.elem[0].key_on_counter = fmem_keyon_counter_w;
5111  1043 18040006001d      	movw	_fmem_keyon_counter_w,_d_DtcHistoricalMemory+6
5112                         ; 1854             d_DtcHistoricalMemory.elem[0].dtc_RomIndex = d_DtcMemory.elem[Index].dtc_RomIndex;
5114  1049 ec82              	ldd	OFST-11,s
5115  104b cd000b            	ldy	#11
5116  104e 13                	emul	
5117  104f b746              	tfr	d,y
5118  1051 180dea0073001f    	movb	_d_DtcMemory+2,y,_d_DtcHistoricalMemory+8
5119                         ; 1857 			update_historical_stack_b = TRUE;
5121  1058                   L1012:
5122  1058 1c000010          	bset	_diag_status_flags_c,16
5123                         ; 1824 	for (Index = StartIndex+Number-1; Index >= StartIndex; Index--)
5125  105c 186382            	decw	OFST-11,s
5126  105f ec82              	ldd	OFST-11,s
5127  1061                   L3702:
5130  1061 ac8d              	cpd	OFST+0,s
5131  1063 182cff37          	bge	L7602
5132                         ; 1862 }
5135  1067 1b8f              	leas	15,s
5136  1069 0a                	rtc	
5175                         ; 1866 unsigned char FMemLibLF_IncCounter(u_temp_l Counter, u_temp_l MaxValue)
5175                         ; 1867 {
5176                         	switch	.ftext
5177  106a                   f_FMemLibLF_IncCounter:
5179  106a 3b                	pshd	
5180       00000000          OFST:	set	0
5183                         ; 1868 	if (Counter < MaxValue)
5185  106b ac85              	cpd	OFST+5,s
5186  106d 2403              	bhs	L1212
5187                         ; 1870 		Counter++;
5189  106f 186280            	incw	OFST+0,s
5190  1072                   L1212:
5191                         ; 1874 	return (unsigned char)Counter;
5193  1072 e681              	ldab	OFST+1,s
5196  1074 31                	puly	
5197  1075 0a                	rtc	
5226                         ; 1880 void FMemLibF_UpdatestoEEPROM(void)
5226                         ; 1881 {
5227                         	switch	.ftext
5228  1076                   f_FMemLibF_UpdatestoEEPROM:
5232                         ; 1882 	switch (fault_update_case_c)
5234  1076 f60000            	ldab	_fault_update_case_c
5236  1079 2708              	beq	L3212
5237  107b 04011f            	dbeq	b,L5212
5238  107e 040134            	dbeq	b,L7212
5239                         ; 1927 			fault_update_case_c = 0;
5241  1081 2048              	bra	L3512
5242  1083                   L3212:
5243                         ; 1887 			if (update_dtc_status_info_b)
5245  1083 1f00004011        	brclr	_diag_status_flags_c,64,L7412
5246                         ; 1889 				EE_BlockWrite(EE_DTC_STATUS, (u_8Bit*)&d_AllDtc_a);
5248  1088 cc0000            	ldd	#_d_AllDtc_a
5249  108b 3b                	pshd	
5250  108c cc0021            	ldd	#33
5251  108f 4a000000          	call	f_EE_BlockWrite
5253  1093 1b82              	leas	2,s
5254                         ; 1891 				update_dtc_status_info_b = FALSE;
5256  1095 1d000040          	bclr	_diag_status_flags_c,64
5257  1099                   L7412:
5258                         ; 1894 			fault_update_case_c++;
5260  1099 720000            	inc	_fault_update_case_c
5261                         ; 1895 			break;
5264  109c 0a                	rtc	
5265  109d                   L5212:
5266                         ; 1900 			if (update_chrono_stack_b)
5268  109d 1f00000811        	brclr	_diag_status_flags_c,8,L1512
5269                         ; 1902 				EE_BlockWrite(EE_CHRONO_STACK, (u_8Bit*)&d_DtcMemory);
5271  10a2 cc0071            	ldd	#_d_DtcMemory
5272  10a5 3b                	pshd	
5273  10a6 cc0017            	ldd	#23
5274  10a9 4a000000          	call	f_EE_BlockWrite
5276  10ad 1b82              	leas	2,s
5277                         ; 1904 				update_chrono_stack_b = FALSE;
5279  10af 1d000008          	bclr	_diag_status_flags_c,8
5280  10b3                   L1512:
5281                         ; 1907 			fault_update_case_c++;
5283                         ; 1908 			break;
5285  10b3 20e4              	bra	L7412
5286  10b5                   L7212:
5287                         ; 1913 			if (update_historical_stack_b)
5289  10b5 1f00001011        	brclr	_diag_status_flags_c,16,L3512
5290                         ; 1915 				EE_BlockWrite(EE_HISTORICAL_STACK, (u_8Bit*)&d_DtcHistoricalMemory);
5292  10ba cc0017            	ldd	#_d_DtcHistoricalMemory
5293  10bd 3b                	pshd	
5294  10be cc0022            	ldd	#34
5295  10c1 4a000000          	call	f_EE_BlockWrite
5297  10c5 1b82              	leas	2,s
5298                         ; 1917 				update_historical_stack_b = FALSE;
5300  10c7 1d000010          	bclr	_diag_status_flags_c,16
5301  10cb                   L3512:
5302                         ; 1920 			fault_update_case_c = 0;
5304  10cb 790000            	clr	_fault_update_case_c
5305                         ; 1921 			break;
5307                         ; 1931 }
5310  10ce 0a                	rtc	
5352                         ; 1955 @far void FMemLibF_ECUTimeStamps(void)
5352                         ; 1956 {
5353                         	switch	.ftext
5354  10cf                   f_FMemLibF_ECUTimeStamps:
5356  10cf 1b9c              	leas	-4,s
5357       00000004          OFST:	set	4
5360                         ; 1958 	EE_BlockRead(EE_ECU_TIMESTAMPS, (u_8Bit*)&temp_lifetime_l); //Read the stored value of life time from EEPROM
5362  10d1 1a80              	leax	OFST-4,s
5363  10d3 34                	pshx	
5364  10d4 cc0027            	ldd	#39
5365  10d7 4a000000          	call	f_EE_BlockRead
5367  10db 1b82              	leas	2,s
5368                         ; 1963 	if (ecu_keyon_counter_c >= FIFTEEN_SEC_TIMER)
5370  10dd f60009            	ldab	_ecu_keyon_counter_c
5371  10e0 c12f              	cmpb	#47
5372  10e2 2509              	blo	L3712
5373                         ; 1965 		fmem_keyon_time_w ++; //Increment the 15sec KEY ON counter. Variable used to store info into EEPROM
5375  10e4 1872000d          	incw	_fmem_keyon_time_w
5376                         ; 1966 		ecu_keyon_counter_c = CLEAR_COUNTER;
5378  10e8 790009            	clr	_ecu_keyon_counter_c
5380  10eb 2003              	bra	L5712
5381  10ed                   L3712:
5382                         ; 1970 		ecu_keyon_counter_c++; //Not reached 15sec. Increment.
5384  10ed 720009            	inc	_ecu_keyon_counter_c
5385  10f0                   L5712:
5386                         ; 1976 	if(ecu_1min_counter_c >= ONE_MINUTE_TIMER)
5388  10f0 f6000a            	ldab	_ecu_1min_counter_c
5389  10f3 c1bb              	cmpb	#187
5390  10f5 257a              	blo	L7712
5391                         ; 1979 		fmem_lifetime_l.l++; //Increment the ECU Life time counter. Variable used to store info into EEPROM
5393  10f7 18720011          	incw	_fmem_lifetime_l+2
5394  10fb 2604              	bne	L621
5395  10fd 1872000f          	incw	_fmem_lifetime_l
5396  1101                   L621:
5397                         ; 1980 		ecu_1min_counter_c = CLEAR_COUNTER; //Clear 1min counter, so next iteration it starts fresh
5399  1101 79000a            	clr	_ecu_1min_counter_c
5400                         ; 1983 		if(temp_lifetime_l.l < 30)
5402  1104 ec82              	ldd	OFST-2,s
5403  1106 8c001e            	cpd	#30
5404  1109 ed80              	ldy	OFST-4,s
5405  110b 188d0000          	cpey	#0
5406  110f 241d              	bhs	L1022
5407                         ; 1985 			EE_BlockWrite(EE_ECU_TIMESTAMPS, (u_8Bit*)&fmem_lifetime_l);
5409  1111 cc000f            	ldd	#_fmem_lifetime_l
5410  1114 3b                	pshd	
5411  1115 cc0027            	ldd	#39
5412  1118 4a000000          	call	f_EE_BlockWrite
5414                         ; 1986 			EE_BlockWrite(EE_ECU_TIME_KEYON, (u_8Bit*)&fmem_keyon_time_w);
5416  111c cc000d            	ldd	#_fmem_keyon_time_w
5417  111f 6c80              	std	0,s
5418  1121 cc0028            	ldd	#40
5419  1124 4a000000          	call	f_EE_BlockWrite
5421  1128 1b82              	leas	2,s
5423  112a ec82              	ldd	OFST-2,s
5424  112c 2003              	bra	L3022
5425  112e                   L1022:
5426                         ; 1990 			ecu_life_counter_c ++; //Increment 5 min counter
5428  112e 720008            	inc	_ecu_life_counter_c
5429  1131                   L3022:
5430                         ; 1993 		if((temp_lifetime_l.l >= 30 && temp_lifetime_l.l < 60))
5432  1131 8c001e            	cpd	#30
5433  1134 ed80              	ldy	OFST-4,s
5434  1136 188d0000          	cpey	#0
5435  113a 2510              	blo	L5022
5437  113c 8c003c            	cpd	#60
5438  113f 188d0000          	cpey	#0
5439  1143 2407              	bhs	L5022
5440                         ; 1995 			if(ecu_life_counter_c == FIVE_MINUTE_COUNTER)
5442  1145 f60008            	ldab	_ecu_life_counter_c
5443  1148 c105              	cmpb	#5
5444                         ; 1997 				EE_BlockWrite(EE_ECU_TIMESTAMPS, (u_8Bit*)&fmem_lifetime_l);
5447                         ; 1998 				EE_BlockWrite(EE_ECU_TIME_KEYON, (u_8Bit*)&fmem_keyon_time_w);
5450                         ; 1999 				ecu_life_counter_c = CLEAR_COUNTER;
5452  114a 2005              	bra	LC014
5453  114c                   L5022:
5454                         ; 2004 			if(ecu_life_counter_c == TEN_MINUTE_COUNTER)
5456  114c f60008            	ldab	_ecu_life_counter_c
5457  114f c10a              	cmpb	#10
5458                         ; 2006 				EE_BlockWrite(EE_ECU_TIMESTAMPS, (u_8Bit*)&fmem_lifetime_l);
5461                         ; 2007 				EE_BlockWrite(EE_ECU_TIME_KEYON, (u_8Bit*)&fmem_keyon_time_w);
5464                         ; 2008 				ecu_life_counter_c = CLEAR_COUNTER;
5466  1151                   LC014:
5467  1151 2621              	bne	L5122
5468  1153 cc000f            	ldd	#_fmem_lifetime_l
5469  1156 3b                	pshd	
5470  1157 cc0027            	ldd	#39
5471  115a 4a000000          	call	f_EE_BlockWrite
5472  115e cc000d            	ldd	#_fmem_keyon_time_w
5473  1161 6c80              	std	0,s
5474  1163 cc0028            	ldd	#40
5475  1166 4a000000          	call	f_EE_BlockWrite
5476  116a 1b82              	leas	2,s
5477  116c 790008            	clr	_ecu_life_counter_c
5478  116f 2003              	bra	L5122
5479  1171                   L7712:
5480                         ; 2015 		ecu_1min_counter_c++; //Not reached 1min, increment.
5482  1171 72000a            	inc	_ecu_1min_counter_c
5483  1174                   L5122:
5484                         ; 2018 }
5487  1174 1b84              	leas	4,s
5488  1176 0a                	rtc	
5527                         ; 2024 unsigned char FMemLibLF_DecCounter(u_temp_l Counter, u_temp_l MaxValue)
5527                         ; 2025 {
5528                         	switch	.ftext
5529  1177                   f_FMemLibLF_DecCounter:
5531  1177 3b                	pshd	
5532       00000000          OFST:	set	0
5535                         ; 2026 	if ((Counter == MaxValue) || (Counter != 0x0))
5537  1178 ac85              	cpd	OFST+5,s
5538  117a 2703              	beq	L7322
5540  117c 044403            	tbeq	d,L5322
5541  117f                   L7322:
5542                         ; 2028 		Counter--;
5544  117f 186380            	decw	OFST+0,s
5545  1182                   L5322:
5546                         ; 2032 	return (unsigned char)Counter;
5548  1182 e681              	ldab	OFST+1,s
5551  1184 31                	puly	
5552  1185 0a                	rtc	
5587                         ; 2055 @far void FMemLibF_NMFailSts(void)
5587                         ; 2056 {
5588                         	switch	.ftext
5589  1186                   f_FMemLibF_NMFailSts:
5591  1186 37                	pshb	
5592       00000001          OFST:	set	1
5595                         ; 2057 	unsigned char i = 0;
5597  1187 6980              	clr	OFST-1,s
5598                         ; 2060 	switch (fmem_nm_status_c)
5600  1189 f60005            	ldab	_fmem_nm_status_c
5602  118c 270b              	beq	L1422
5603  118e 04012b            	dbeq	b,L3422
5604                         ; 2106 			fmem_genericFailSts = 0;
5606  1191 79000c            	clr	_fmem_genericFailSts
5607                         ; 2107 			fmem_currentFailSts = 0;
5609  1194 79000b            	clr	_fmem_currentFailSts
5610                         ; 2109 			fmem_nm_status_c = 0;
5612  1197 2041              	bra	L3032
5613  1199                   L1422:
5614                         ; 2065 			fmem_genericFailSts = 0;
5616  1199 7b000c            	stab	_fmem_genericFailSts
5617                         ; 2068 			for (i = 0; i < SIZEOF_ALL_SUPPORTED_DTC; i++)
5619  119c 6b80              	stab	OFST-1,s
5620  119e                   L7622:
5621                         ; 2071 				if (d_AllDtc_a[i].DtcState.state.Confirmed == TRUE)
5623  119e 87                	clra	
5624  119f 59                	lsld	
5625  11a0 b746              	tfr	d,y
5626  11a2 0fea00010807      	brclr	_d_AllDtc_a+1,y,8,L5722
5627                         ; 2074 					fmem_genericFailSts = TRUE;
5629  11a8 c601              	ldab	#1
5630  11aa 7b000c            	stab	_fmem_genericFailSts
5631                         ; 2075 					break;
5633  11ad 2008              	bra	L3722
5634  11af                   L5722:
5635                         ; 2068 			for (i = 0; i < SIZEOF_ALL_SUPPORTED_DTC; i++)
5637  11af 6280              	inc	OFST-1,s
5640  11b1 e680              	ldab	OFST-1,s
5641  11b3 c12e              	cmpb	#46
5642  11b5 25e7              	blo	L7622
5643  11b7                   L3722:
5644                         ; 2080 			fmem_nm_status_c++;
5646  11b7 720005            	inc	_fmem_nm_status_c
5647                         ; 2081 			break;
5649  11ba 2021              	bra	L5622
5650  11bc                   L3422:
5651                         ; 2087 			fmem_currentFailSts = 0;
5653  11bc 7b000b            	stab	_fmem_currentFailSts
5654                         ; 2089 			for (i = 0; i < SIZEOF_ALL_SUPPORTED_DTC; i++)
5656  11bf 6b80              	stab	OFST-1,s
5657  11c1                   L7722:
5658                         ; 2091 				if (d_AllDtc_a[i].DtcState.state.TestFailed == TRUE)
5660  11c1 87                	clra	
5661  11c2 59                	lsld	
5662  11c3 b746              	tfr	d,y
5663  11c5 0fea00010107      	brclr	_d_AllDtc_a+1,y,1,L5032
5664                         ; 2094 					fmem_currentFailSts = TRUE;
5666  11cb c601              	ldab	#1
5667  11cd 7b000b            	stab	_fmem_currentFailSts
5668                         ; 2095 					break;
5670  11d0 2008              	bra	L3032
5671  11d2                   L5032:
5672                         ; 2089 			for (i = 0; i < SIZEOF_ALL_SUPPORTED_DTC; i++)
5674  11d2 6280              	inc	OFST-1,s
5677  11d4 e680              	ldab	OFST-1,s
5678  11d6 c12e              	cmpb	#46
5679  11d8 25e7              	blo	L7722
5680  11da                   L3032:
5681                         ; 2100 			fmem_nm_status_c = 0;
5683  11da 790005            	clr	_fmem_nm_status_c
5684                         ; 2101 			break;
5686  11dd                   L5622:
5687                         ; 2114 }
5690  11dd 1b81              	leas	1,s
5691  11df 0a                	rtc	
5779                         	switch	.bss
5780  0000                   _fault_update_case_c:
5781  0000 00                	ds.b	1
5782                         	xdef	_fault_update_case_c
5783  0001                   _no_of_dtc_c:
5784  0001 00                	ds.b	1
5785                         	xdef	_no_of_dtc_c
5786  0002                   _dtc_loop_count_c:
5787  0002 00                	ds.b	1
5788                         	xdef	_dtc_loop_count_c
5789  0003                   _cyclic_dtc_count_c:
5790  0003 00                	ds.b	1
5791                         	xdef	_cyclic_dtc_count_c
5792  0004                   _d_HistInterrogRecordEnable_t:
5793  0004 00                	ds.b	1
5794                         	xdef	_d_HistInterrogRecordEnable_t
5795  0005                   _fmem_nm_status_c:
5796  0005 00                	ds.b	1
5797                         	xdef	_fmem_nm_status_c
5798  0006                   _fmem_keyon_counter_w:
5799  0006 0000              	ds.b	2
5800                         	xdef	_fmem_keyon_counter_w
5801  0008                   _ecu_life_counter_c:
5802  0008 00                	ds.b	1
5803                         	xdef	_ecu_life_counter_c
5804  0009                   _ecu_keyon_counter_c:
5805  0009 00                	ds.b	1
5806                         	xdef	_ecu_keyon_counter_c
5807  000a                   _ecu_1min_counter_c:
5808  000a 00                	ds.b	1
5809                         	xdef	_ecu_1min_counter_c
5810                         	xdef	f_FMemLibLF_DecCounter
5811                         	xdef	f_FMemLibLF_IncCounter
5812                         	xdef	f_FMemLibF_ReadDiagnosticInformation
5813                         	xdef	f_FMemLibF_ClearDiagnosticInformation
5814                         	xdef	f_FMemLibLF_DecDtcOperationCyclCounter
5815                         	xdef	f_FMemLibLF_ClearSingleDtc
5816                         	xdef	f_FMemLibLF_SearchDtcIndexInDtcMemory
5817                         	xdef	f_FMemLibLF_TransferFaultToHistoricalStack
5818                         	xdef	f_FMemLibLF_GetNumberOfRegisteredDtc
5819                         	xdef	f_FMemLibLF_GetNumberOfMaskedDtc
5820                         	xdef	f_FMemLibLF_GetByteOfDtc
5821                         	xref	f_EE_BlockRead
5822                         	xref	f_EE_BlockWrite
5823                         	xref	_memmove
5824                         	xref	_canio_RX_IGN_STATUS_c
5825                         	xref	_canio_RX_TotalOdo_c
5826                         	xref	_canio_RX_TravelDistance_w
5827                         	xref	f_FMemApplInit
5828                         	xdef	f_FMemLibF_CheckDtcSupportOverDtc
5829                         	xdef	f_FMemLibF_GetIgnition
5830                         	xdef	f_FMemLibF_IsOdometerValid
5831                         	xdef	f_FMemLibF_GetOdometer
5832                         	xdef	f_FMemLibF_ClearDtcMemory
5833                         	xdef	f_FMemLibF_CheckDtcConditionTimer
5834                         	xdef	f_FMemLibF_ReportDtc
5835                         	xdef	f_FMemLibF_NMFailSts
5836                         	xdef	f_FMemLibF_ECUTimeStamps
5837                         	xdef	f_FMemLibF_UpdatestoEEPROM
5838                         	xdef	f_FMemLibF_CyclicDtcTask
5839                         	xdef	f_FMemLibF_Init
5840  000b                   _fmem_currentFailSts:
5841  000b 00                	ds.b	1
5842                         	xdef	_fmem_currentFailSts
5843  000c                   _fmem_genericFailSts:
5844  000c 00                	ds.b	1
5845                         	xdef	_fmem_genericFailSts
5846  000d                   _fmem_keyon_time_w:
5847  000d 0000              	ds.b	2
5848                         	xdef	_fmem_keyon_time_w
5849  000f                   _fmem_lifetime_l:
5850  000f 00000000          	ds.b	4
5851                         	xdef	_fmem_lifetime_l
5852  0013                   _Subservice:
5853  0013 00                	ds.b	1
5854                         	xdef	_Subservice
5855  0014                   _d_OperationCycle_c:
5856  0014 00                	ds.b	1
5857                         	xdef	_d_OperationCycle_c
5858  0015                   _d_DtcSettingModeEnabled_t:
5859  0015 00                	ds.b	1
5860                         	xdef	_d_DtcSettingModeEnabled_t
5861  0016                   _d_IgnitionStart5secTimer_c:
5862  0016 00                	ds.b	1
5863                         	xdef	_d_IgnitionStart5secTimer_c
5864                         	xref	_d_AllDtc_a
5865                         	xref	_fmem_AllRegisteredDtc_t
5866  0017                   _d_DtcHistoricalMemory:
5867  0017 0000000000000000  	ds.b	90
5868                         	xdef	_d_DtcHistoricalMemory
5869  0071                   _d_DtcMemory:
5870  0071 0000000000000000  	ds.b	110
5871                         	xdef	_d_DtcMemory
5872                         	xref	_diag_status_flags_c
5893                         	xref	c_jltab
5894                         	end
