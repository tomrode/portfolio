   1                         ; C Compiler for S12XE [COSMIC Software].
   2                         ; Generator V4.7.8 - 27 Jul 2007
   3                         ; Optimizer V4.6.13 - 27 Jul 2007
   4                         .const:	section	.data
   5  0000                   _kIlMainVersion:
   6  0000 05                	dc.b	5
   7  0001                   _kIlSubVersion:
   8  0001 05                	dc.b	5
   9  0002                   _kIlReleaseVersion:
  10  0002 00                	dc.b	0
  57                         ; 858 static void IlSendMessage(IlTransmitHandle ilTxHnd)
  57                         ; 859 {
  58                         .ftext:	section	.text
  59  0000                   L5f_IlSendMessage:
  61  0000 3b                	pshd	
  62       00000000          OFST:	set	0
  65                         ; 861   CanInterruptDisable();
  67  0001 4a000000          	call	f_VStdSuspendAllInterrupts
  69                         ; 862   ilTxState[ilTxHnd] &= kTxNotSendRequest;
  71  0005 e681              	ldab	OFST+1,s
  72  0007 b796              	exg	b,y
  73  0009 0dea000680        	bclr	_ilTxState,y,128
  74                         ; 875   ilTxUpdateCounter[ilTxHnd] = kIlStopUpdateCounterValue;
  76  000e c6ff              	ldab	#255
  77  0010 6bea0003          	stab	_ilTxUpdateCounter,y
  78                         ; 879   CanInterruptRestore();
  80  0014 4a000000          	call	f_VStdResumeAllInterrupts
  82                         ; 881   if (CanTransmit(IlGetIlTxIndirection(ilTxHnd)) != kCanTxOk)
  84  0018 e681              	ldab	OFST+1,s
  85  001a 87                	clra	
  86  001b 59                	lsld	
  87  001c b746              	tfr	d,y
  88  001e ecea0000          	ldd	_IlTxIndirection,y
  89  0022 4a000000          	call	f_CanTransmit
  91  0026 040115            	dbeq	b,L13
  92                         ; 883     CanInterruptDisable();
  94  0029 4a000000          	call	f_VStdSuspendAllInterrupts
  96                         ; 885       ilTxState[ilTxHnd] |= (kTxSendRequest);
  98  002d e681              	ldab	OFST+1,s
  99  002f b796              	exg	b,y
 100  0031 0cea000680        	bset	_ilTxState,y,128
 101                         ; 887     ilTxUpdateCounter[ilTxHnd] = 0;
 103  0036 69ea0003          	clr	_ilTxUpdateCounter,y
 104                         ; 888     CanInterruptRestore();
 106  003a 4a000000          	call	f_VStdResumeAllInterrupts
 108  003e                   L13:
 109                         ; 904 }                               /* End of IlSendMessage() */
 112  003e 31                	puly	
 113  003f 0a                	rtc	
 136                         ; 914 void IlInitPowerOn(void)
 136                         ; 915 {
 137                         	switch	.ftext
 138  0040                   f_IlInitPowerOn:
 142                         ; 921     IlInit(IL_CHANNEL_ILPARA_ONLY);
 144  0040 4a004545          	call	f_IlInit
 146                         ; 923 }                               /* End of IlInitPowerOn() */
 149  0044 0a                	rtc	
 202                         ; 928 void IlInit(IL_CHANNEL_ILTYPE_ONLY)
 202                         ; 929 {
 203                         	switch	.ftext
 204  0045                   f_IlInit:
 206  0045 37                	pshb	
 207       00000001          OFST:	set	1
 210                         ; 939   Il_ChannelState(IL_CHANNEL_ILPARA_MACRO) = kIlIsSuspend;
 212  0046 c7                	clrb	
 213  0047 7b000d            	stab	_ilChannelState
 214                         ; 959   for (ilRxHnd = 0; ilRxHnd < kIlNumberOfRxObjects; ilRxHnd++)
 216  004a 6b80              	stab	OFST-1,s
 217  004c                   L16:
 218                         ; 964     if ((IlGetRxDataPtr(ilRxHnd)) != V_NULL)
 221  004c 87                	clra	
 222  004d b746              	tfr	d,y
 223  004f 1858              	lsly	
 224  0051 eeea0000          	ldx	_CanRxDataPtr,y
 225  0055 2748              	beq	L61
 226                         ; 967       if ((IlRxDefaultInitValue[ilRxHnd]) != V_NULL)
 228  0057 edea0000          	ldy	_IlRxDefaultInitValue,y
 229  005b 2725              	beq	L17
 230                         ; 970         VStdMemCpyRomToRam(IlGetRxDataPtr(ilRxHnd), IlRxDefaultInitValue[ilRxHnd], IlGetRxDataLen(ilRxHnd));
 232  005d b746              	tfr	d,y
 233  005f e6ea0000          	ldab	_CanRxDataLen,y
 234  0063 3b                	pshd	
 235  0064 e682              	ldab	OFST+1,s
 236  0066 b746              	tfr	d,y
 237  0068 1858              	lsly	
 238  006a edea0000          	ldy	_CanRxDataPtr,y
 239  006e 59                	lsld	
 240  006f b745              	tfr	d,x
 241  0071 eee20000          	ldx	_IlRxDefaultInitValue,x
 242  0075 3a                	puld	
 243  0076 044407            	tbeq	d,L21
 244  0079                   L41:
 245  0079 180a3070          	movb	1,x+,1,y+
 246  007d 0434f9            	dbne	d,L41
 247  0080                   L21:
 249  0080 201d              	bra	L61
 250  0082                   L17:
 251                         ; 976         VStdMemClr(IlGetRxDataPtr(ilRxHnd), IlGetRxDataLen(ilRxHnd));
 253  0082 c7                	clrb	
 254  0083 3b                	pshd	
 255  0084 e682              	ldab	OFST+1,s
 256  0086 b746              	tfr	d,y
 257  0088 1858              	lsly	
 258  008a edea0000          	ldy	_CanRxDataPtr,y
 259  008e b745              	tfr	d,x
 260  0090 e6e20000          	ldab	_CanRxDataLen,x
 261  0094 b745              	tfr	d,x
 262  0096 3a                	puld	
 263  0097 044505            	tbeq	x,L61
 264  009a                   L02:
 265  009a 6b70              	stab	1,y+
 266  009c 0435fb            	dbne	x,L02
 267  009f                   L61:
 268                         ; 959   for (ilRxHnd = 0; ilRxHnd < kIlNumberOfRxObjects; ilRxHnd++)
 270  009f 6280              	inc	OFST-1,s
 273  00a1 e680              	ldab	OFST-1,s
 274  00a3 c10c              	cmpb	#12
 275  00a5 25a5              	blo	L16
 276                         ; 995   IlResetRxTimeoutFlags(IL_CHANNEL_ILPARA_ONLY);
 278  00a7 4a038787          	call	f_IlResetRxTimeoutFlags
 280                         ; 998   ResetIndicationFlags(IL_CHANNEL_ILPARA_MACRO);
 283  00ab cd0009            	ldy	#_ilRxIndicationFlags
 284  00ae 87                	clra	
 285  00af c7                	clrb	
 286  00b0 ce0002            	ldx	#2
 287  00b3                   L22:
 288  00b3 6c71              	std	2,y+
 289  00b5 0435fb            	dbne	x,L22
 290                         ; 1016   for (ilTxHnd = 0; ilTxHnd < kIlNumberOfTxObjects; ilTxHnd++)
 294  00b8 c7                	clrb	
 295  00b9 6b80              	stab	OFST-1,s
 296  00bb                   L57:
 297                         ; 1031     if ((IlGetTxDataPtr(ilTxHnd)) != V_NULL)
 300  00bb 87                	clra	
 301  00bc b746              	tfr	d,y
 302  00be 1858              	lsly	
 303  00c0 eeea0000          	ldx	_IlTxIndirection,y
 304  00c4 1848              	lslx	
 305  00c6 eee20000          	ldx	_CanTxDataPtr,x
 306  00ca 2760              	beq	L03
 307                         ; 1034       if (IlTxDefaultInitValue[ilTxHnd] != V_NULL)
 309  00cc eeea0000          	ldx	_IlTxDefaultInitValue,y
 310  00d0 272f              	beq	L501
 311                         ; 1037         VStdMemCpyRomToRam(IlGetTxDataPtr(ilTxHnd), IlTxDefaultInitValue[ilTxHnd], IlGetTxDlc(ilTxHnd));
 313  00d2 edea0000          	ldy	_IlTxIndirection,y
 314  00d6 e6ea0000          	ldab	_CanTxDLC,y
 315  00da c40f              	andb	#15
 316  00dc 3b                	pshd	
 317  00dd e682              	ldab	OFST+1,s
 318  00df b746              	tfr	d,y
 319  00e1 1858              	lsly	
 320  00e3 eeea0000          	ldx	_IlTxIndirection,y
 321  00e7 1848              	lslx	
 322  00e9 ede20000          	ldy	_CanTxDataPtr,x
 323  00ed 59                	lsld	
 324  00ee b745              	tfr	d,x
 325  00f0 eee20000          	ldx	_IlTxDefaultInitValue,x
 326  00f4 3a                	puld	
 327  00f5 044407            	tbeq	d,L42
 328  00f8                   L62:
 329  00f8 180a3070          	movb	1,x+,1,y+
 330  00fc 0434f9            	dbne	d,L62
 331  00ff                   L42:
 333  00ff 202b              	bra	L03
 334  0101                   L501:
 335                         ; 1044         VStdMemClr(IlGetTxDataPtr(ilTxHnd), IlGetTxDlc(ilTxHnd));
 337  0101 c7                	clrb	
 338  0102 3b                	pshd	
 339  0103 e682              	ldab	OFST+1,s
 340  0105 b746              	tfr	d,y
 341  0107 1858              	lsly	
 342  0109 eeea0000          	ldx	_IlTxIndirection,y
 343  010d 1848              	lslx	
 344  010f ede20000          	ldy	_CanTxDataPtr,x
 345  0113 b745              	tfr	d,x
 346  0115 1848              	lslx	
 347  0117 eee20000          	ldx	_IlTxIndirection,x
 348  011b e6e20000          	ldab	_CanTxDLC,x
 349  011f c40f              	andb	#15
 350  0121 b745              	tfr	d,x
 351  0123 3a                	puld	
 352  0124 044505            	tbeq	x,L03
 353  0127                   L23:
 354  0127 6b70              	stab	1,y+
 355  0129 0435fb            	dbne	x,L23
 356  012c                   L03:
 357                         ; 1049     ilTxState[ilTxHnd] = 0;
 359  012c e680              	ldab	OFST-1,s
 360  012e b796              	exg	b,y
 361  0130 69ea0006          	clr	_ilTxState,y
 362                         ; 1052     ilTxUpdateCounter[ilTxHnd] = 0; /*    */
 364  0134 69ea0003          	clr	_ilTxUpdateCounter,y
 365                         ; 1065     ilTxCyclicCounter[ilTxHnd] = 0;
 367  0138 69ea0000          	clr	L3_ilTxCyclicCounter,y
 368                         ; 1016   for (ilTxHnd = 0; ilTxHnd < kIlNumberOfTxObjects; ilTxHnd++)
 370  013c 6280              	inc	OFST-1,s
 373  013e e680              	ldab	OFST-1,s
 374  0140 c103              	cmpb	#3
 375  0142 1825ff75          	blo	L57
 376                         ; 1093 }                               /* End of IlInit() */
 381  0146 1b81              	leas	1,s
 382  0148 0a                	rtc	
 408                         ; 1098 void IlRxStart(IL_CHANNEL_ILTYPE_ONLY)
 408                         ; 1099 {
 409                         	switch	.ftext
 410  0149                   f_IlRxStart:
 414                         ; 1110   if (RxEnabled(IL_CHANNEL_ILPARA_MACRO))
 416  0149 1f000d0801        	brclr	_ilChannelState,8,L121
 417                         ; 1112     return;
 420  014e 0a                	rtc	
 421  014f                   L121:
 422                         ; 1159   IlResetRxTimeoutFlags(IL_CHANNEL_ILPARA_ONLY);
 424  014f 4a038787          	call	f_IlResetRxTimeoutFlags
 426                         ; 1162   ResetIndicationFlags(IL_CHANNEL_ILPARA_MACRO);
 429  0153 cd0009            	ldy	#_ilRxIndicationFlags
 430  0156 87                	clra	
 431  0157 c7                	clrb	
 432  0158 ce0002            	ldx	#2
 433  015b                   L63:
 434  015b 6c71              	std	2,y+
 435  015d 0435fb            	dbne	x,L63
 436                         ; 1175   EnableRx(IL_CHANNEL_ILPARA_MACRO);
 440  0160 f6000d            	ldab	_ilChannelState
 441  0163 c4fb              	andb	#251
 442  0165 ca08              	orab	#8
 443  0167 7b000d            	stab	_ilChannelState
 444                         ; 1182 }                               /* End of IlRxStart() */
 447  016a 0a                	rtc	
 485                         ; 1187 void IlTxStart(IL_CHANNEL_ILTYPE_ONLY)
 485                         ; 1188 {
 486                         	switch	.ftext
 487  016b                   f_IlTxStart:
 489  016b 37                	pshb	
 490       00000001          OFST:	set	1
 493                         ; 1193   if (TxEnabled(IL_CHANNEL_ILPARA_MACRO))
 495  016c 1e000d022f        	brset	_ilChannelState,2,L24
 496                         ; 1195     return;
 498                         ; 1202   for (ilTxHnd = 0; ilTxHnd < kIlNumberOfTxObjects; ilTxHnd++)
 500  0171 c7                	clrb	
 501  0172 6b80              	stab	OFST-1,s
 502  0174                   L141:
 503                         ; 1220     ilTxState[ilTxHnd] = IlTxType[ilTxHnd];
 506  0174 b796              	exg	b,y
 507  0176 180aea0000ea0006  	movb	_IlTxType,y,_ilTxState,y
 508                         ; 1223     ilTxUpdateCounter[ilTxHnd] = 0; /*    */
 510  017e 69ea0003          	clr	_ilTxUpdateCounter,y
 511                         ; 1241       ilTxCyclicCounter[ilTxHnd] = IlTxStartCycles[ilTxHnd];    /* + 1 already done in the table */
 513  0182 180aea0000ea0000  	movb	_IlTxStartCycles,y,L3_ilTxCyclicCounter,y
 514                         ; 1202   for (ilTxHnd = 0; ilTxHnd < kIlNumberOfTxObjects; ilTxHnd++)
 516  018a 6280              	inc	OFST-1,s
 519  018c e680              	ldab	OFST-1,s
 520  018e c103              	cmpb	#3
 521  0190 25e2              	blo	L141
 522                         ; 1268   IlResetCanConfirmationFlags(IL_CHANNEL_ILPARA_ONLY);
 526  0192 4a000000          	call	f_IlResetCanConfirmationFlags
 528                         ; 1282   EnableTx(IL_CHANNEL_ILPARA_MACRO);
 530  0196 f6000d            	ldab	_ilChannelState
 531  0199 c4fe              	andb	#254
 532  019b ca02              	orab	#2
 533  019d 7b000d            	stab	_ilChannelState
 534                         ; 1289 }                               /* End of IlTxStart() */
 535  01a0                   L24:
 538  01a0 1b81              	leas	1,s
 539  01a2 0a                	rtc	
 562                         ; 1294 void IlRxStop(IL_CHANNEL_ILTYPE_ONLY)
 562                         ; 1295 {
 563                         	switch	.ftext
 564  01a3                   f_IlRxStop:
 568                         ; 1302   if (RxSuspended(IL_CHANNEL_ILPARA_MACRO)) /* Receive already disabled  */
 570  01a3 f6000d            	ldab	_ilChannelState
 571  01a6 c50c              	bitb	#12
 572  01a8 2601              	bne	L751
 573                         ; 1304     return;
 576  01aa 0a                	rtc	
 577  01ab                   L751:
 578                         ; 1307   SuspendRx(IL_CHANNEL_ILPARA_MACRO);
 580  01ab c4f3              	andb	#243
 581  01ad 7b000d            	stab	_ilChannelState
 582                         ; 1341 }                               /* End of IlRxStop() */
 585  01b0 0a                	rtc	
 608                         ; 1346 void IlTxStop(IL_CHANNEL_ILTYPE_ONLY)
 608                         ; 1347 {
 609                         	switch	.ftext
 610  01b1                   f_IlTxStop:
 614                         ; 1354   if (TxSuspended(IL_CHANNEL_ILPARA_MACRO))
 616  01b1 f6000d            	ldab	_ilChannelState
 617  01b4 c503              	bitb	#3
 618  01b6 2601              	bne	L171
 619                         ; 1356     return;
 622  01b8 0a                	rtc	
 623  01b9                   L171:
 624                         ; 1358   SuspendTx(IL_CHANNEL_ILPARA_MACRO);
 626  01b9 c4fc              	andb	#252
 627  01bb 7b000d            	stab	_ilChannelState
 628                         ; 1396 }                               /* End of IlTxStop() */
 631  01be 0a                	rtc	
 654                         ; 1401 void IlRxWait(IL_CHANNEL_ILTYPE_ONLY)
 654                         ; 1402 {
 655                         	switch	.ftext
 656  01bf                   f_IlRxWait:
 660                         ; 1404   if (RxEnabled(IL_CHANNEL_ILPARA_MACRO))
 662  01bf 1f000d080a        	brclr	_ilChannelState,8,L302
 663                         ; 1406     WaitRx(IL_CHANNEL_ILPARA_MACRO);
 665  01c4 f6000d            	ldab	_ilChannelState
 666  01c7 c4f7              	andb	#247
 667  01c9 ca04              	orab	#4
 668  01cb 7b000d            	stab	_ilChannelState
 669  01ce                   L302:
 670                         ; 1413 }                               /* End of IlRxWait() */
 673  01ce 0a                	rtc	
 696                         ; 1418 void IlTxWait(IL_CHANNEL_ILTYPE_ONLY)
 696                         ; 1419 {
 697                         	switch	.ftext
 698  01cf                   f_IlTxWait:
 702                         ; 1421   if (TxEnabled(IL_CHANNEL_ILPARA_MACRO))
 704  01cf 1f000d020a        	brclr	_ilChannelState,2,L512
 705                         ; 1423     WaitTx(IL_CHANNEL_ILPARA_MACRO);
 707  01d4 f6000d            	ldab	_ilChannelState
 708  01d7 c4fd              	andb	#253
 709  01d9 ca01              	orab	#1
 710  01db 7b000d            	stab	_ilChannelState
 711  01de                   L512:
 712                         ; 1430 }                               /* End of IlTxWait() */
 715  01de 0a                	rtc	
 738                         ; 1435 void IlRxRelease(IL_CHANNEL_ILTYPE_ONLY)
 738                         ; 1436 {
 739                         	switch	.ftext
 740  01df                   f_IlRxRelease:
 744                         ; 1443   if (RxWaiting(IL_CHANNEL_ILPARA_MACRO))
 746  01df 1f000d040a        	brclr	_ilChannelState,4,L722
 747                         ; 1474     EnableRx(IL_CHANNEL_ILPARA_MACRO);
 749  01e4 f6000d            	ldab	_ilChannelState
 750  01e7 c4fb              	andb	#251
 751  01e9 ca08              	orab	#8
 752  01eb 7b000d            	stab	_ilChannelState
 753  01ee                   L722:
 754                         ; 1482 }                               /* End of IlRxRelease() */
 757  01ee 0a                	rtc	
 780                         ; 1487 void IlTxRelease(IL_CHANNEL_ILTYPE_ONLY)
 780                         ; 1488 {
 781                         	switch	.ftext
 782  01ef                   f_IlTxRelease:
 786                         ; 1490   if (TxWaiting(IL_CHANNEL_ILPARA_MACRO))
 788  01ef 1f000d010a        	brclr	_ilChannelState,1,L142
 789                         ; 1492     EnableTx(IL_CHANNEL_ILPARA_MACRO);
 791  01f4 f6000d            	ldab	_ilChannelState
 792  01f7 c4fe              	andb	#254
 793  01f9 ca02              	orab	#2
 794  01fb 7b000d            	stab	_ilChannelState
 795  01fe                   L142:
 796                         ; 1500 }                               /* End of IlTxRelease() */
 799  01fe 0a                	rtc	
 823                         ; 1505 void IlRxTask(IL_CHANNEL_ILTYPE_ONLY)
 823                         ; 1506 {
 824                         	switch	.ftext
 825  01ff                   f_IlRxTask:
 829                         ; 1507   IlRxStateTask(IL_CHANNEL_ILPARA_ONLY);
 831  01ff 4a020808          	call	f_IlRxStateTask
 833                         ; 1508   IlRxTimerTask(IL_CHANNEL_ILPARA_ONLY);
 835  0203 4a020909          	call	f_IlRxTimerTask
 837                         ; 1509 }                               /* End of IlRxTask() */
 840  0207 0a                	rtc	
 862                         ; 1514 void IlRxStateTask(IL_CHANNEL_ILTYPE_ONLY)
 862                         ; 1515 {
 863                         	switch	.ftext
 864  0208                   f_IlRxStateTask:
 868                         ; 1572 }                               /* End of IlRxStateTask() */
 871  0208 0a                	rtc	
 894                         ; 1577 void IlRxTimerTask(IL_CHANNEL_ILTYPE_ONLY)
 894                         ; 1578 {
 895                         	switch	.ftext
 896  0209                   f_IlRxTimerTask:
 900                         ; 1591   if (!RxEnabled(IL_CHANNEL_ILPARA_MACRO))
 902                         ; 1593     return;
 905                         ; 1669 }                               /* End of IlRxTimerTask() */
 908  0209 0a                	rtc	
 932                         ; 1674 void IlTxTask(IL_CHANNEL_ILTYPE_ONLY)
 932                         ; 1675 {
 933                         	switch	.ftext
 934  020a                   f_IlTxTask:
 938                         ; 1676   IlTxStateTask(IL_CHANNEL_ILPARA_ONLY);
 940  020a 4a021313          	call	f_IlTxStateTask
 942                         ; 1677   IlTxTimerTask(IL_CHANNEL_ILPARA_ONLY);
 944  020e 4a029090          	call	f_IlTxTimerTask
 946                         ; 1678 }                               /* End of IlTxTask() */
 949  0212 0a                	rtc	
 997                         ; 1683 void IlTxStateTask(IL_CHANNEL_ILTYPE_ONLY)
 997                         ; 1684 {
 998                         	switch	.ftext
 999  0213                   f_IlTxStateTask:
1001  0213 1b9d              	leas	-3,s
1002       00000003          OFST:	set	3
1005                         ; 1690   if (TxNotSuspended(IL_CHANNEL_ILPARA_MACRO))
1007  0215 f6000d            	ldab	_ilChannelState
1008  0218 c503              	bitb	#3
1009  021a 2771              	beq	L323
1010                         ; 1699     ilTxHnd = kIlNumberOfTxObjects;
1012  021c c603              	ldab	#3
1013  021e 6b80              	stab	OFST-3,s
1014  0220                   L523:
1015                         ; 1705       ilTxHnd--;
1017  0220 6380              	dec	OFST-3,s
1018                         ; 1708       x = IlGetIlTxIndirection(ilTxHnd);
1020  0222 e680              	ldab	OFST-3,s
1021  0224 87                	clra	
1022  0225 b746              	tfr	d,y
1023  0227 1858              	lsly	
1024  0229 edea0000          	ldy	_IlTxIndirection,y
1025  022d 6d81              	sty	OFST-2,s
1026                         ; 1710       if (x != kCanTxHandleNotUsed)
1028  022f 8dffff            	cpy	#-1
1029  0232 2755              	beq	L723
1030                         ; 1713         if ((IlGetTxConfirmationFlags(IlGetTxConfirmationOffset(x)) & IlGetTxConfirmationMask(x)) != 0)
1032  0234 e6ea0000          	ldab	_CanConfirmationOffset,y
1033  0238 b746              	tfr	d,y
1034  023a ee81              	ldx	OFST-2,s
1035  023c e6e20000          	ldab	_CanConfirmationMask,x
1036  0240 e4ea0000          	andb	_CanConfirmationFlags,y
1037  0244 2743              	beq	L723
1038                         ; 1716           CanInterruptDisable();
1040  0246 4a000000          	call	f_VStdSuspendAllInterrupts
1042                         ; 1717           IlGetTxConfirmationFlags(IlGetTxConfirmationOffset(x)) &= (vuint8) ~ IlGetTxConfirmationMask(x);
1044  024a ed81              	ldy	OFST-2,s
1045  024c e6ea0000          	ldab	_CanConfirmationOffset,y
1046  0250 87                	clra	
1047  0251 b746              	tfr	d,y
1048  0253 ee81              	ldx	OFST-2,s
1049  0255 e6e20000          	ldab	_CanConfirmationMask,x
1050  0259 51                	comb	
1051  025a e4ea0000          	andb	_CanConfirmationFlags,y
1052  025e 6bea0000          	stab	_CanConfirmationFlags,y
1053                         ; 1719           ilTxUpdateCounter[ilTxHnd] = IlTxUpdateCycles[ilTxHnd];
1055  0262 e680              	ldab	OFST-3,s
1056  0264 b746              	tfr	d,y
1057  0266 180aea0000ea0003  	movb	_IlTxUpdateCycles,y,_ilTxUpdateCounter,y
1058                         ; 1720           CanInterruptRestore();
1060  026e 4a000000          	call	f_VStdResumeAllInterrupts
1062                         ; 1723           if (IlTxConfirmationFctPtr[ilTxHnd] != V_NULL)
1064  0272 e680              	ldab	OFST-3,s
1065  0274 87                	clra	
1066  0275 59                	lsld	
1067  0276 59                	lsld	
1068  0277 b746              	tfr	d,y
1069  0279 ecea0000          	ldd	_IlTxConfirmationFctPtr,y
1070  027d 2606              	bne	LC001
1071  027f ecea0002          	ldd	_IlTxConfirmationFctPtr+2,y
1072  0283 2704              	beq	L723
1073  0285                   LC001:
1074                         ; 1725             IlTxConfirmationFctPtr[ilTxHnd] ();
1076  0285 4beb0000          	call	[_IlTxConfirmationFctPtr,y]
1078  0289                   L723:
1079                         ; 1759     while (ilTxHnd != 0);
1081  0289 e680              	ldab	OFST-3,s
1082  028b 2693              	bne	L523
1083  028d                   L323:
1084                         ; 1768 }                               /* End of IlTxStateTask() */
1087  028d 1b83              	leas	3,s
1088  028f 0a                	rtc	
1127                         ; 1773 void IlTxTimerTask(IL_CHANNEL_ILTYPE_ONLY)
1127                         ; 1774 {
1128                         	switch	.ftext
1129  0290                   f_IlTxTimerTask:
1131  0290 37                	pshb	
1132       00000001          OFST:	set	1
1135                         ; 1779   if (TxEnabled(IL_CHANNEL_ILPARA_MACRO))
1137  0291 f6000d            	ldab	_ilChannelState
1138  0294 c502              	bitb	#2
1139  0296 276d              	beq	L553
1140                         ; 1792     ilTxHnd = kIlNumberOfTxObjects;
1142  0298 c603              	ldab	#3
1143  029a 6b80              	stab	OFST-1,s
1144  029c                   L753:
1145                         ; 1798       ilTxHnd--;
1147  029c 6380              	dec	OFST-1,s
1148                         ; 1813       CanInterruptDisable();
1151  029e 4a000000          	call	f_VStdSuspendAllInterrupts
1153                         ; 1815       if ((ilTxUpdateCounter[ilTxHnd] != kIlStopUpdateCounterValue) && (ilTxUpdateCounter[ilTxHnd] > 0))
1155  02a2 e680              	ldab	OFST-1,s
1156  02a4 b796              	exg	b,y
1157  02a6 e6ea0003          	ldab	_ilTxUpdateCounter,y
1158  02aa c1ff              	cmpb	#255
1159  02ac 2707              	beq	L563
1161  02ae 044104            	tbeq	b,L563
1162                         ; 1817         ilTxUpdateCounter[ilTxHnd]--;
1164  02b1 63ea0003          	dec	_ilTxUpdateCounter,y
1165  02b5                   L563:
1166                         ; 1819       CanInterruptRestore();
1168  02b5 4a000000          	call	f_VStdResumeAllInterrupts
1170                         ; 1828           if ((ilTxState[ilTxHnd] & kTxSendCyclic) != 0)
1172  02b9 e680              	ldab	OFST-1,s
1173  02bb b796              	exg	b,y
1174  02bd 0fea00060129      	brclr	_ilTxState,y,1,L763
1175                         ; 1830             if (ilTxCyclicCounter[ilTxHnd] != 0)    /* decrement timer counter      */
1177  02c3 e7ea0000          	tst	L3_ilTxCyclicCounter,y
1178  02c7 2704              	beq	L173
1179                         ; 1832               ilTxCyclicCounter[ilTxHnd]--; /*   only if not 0              */
1181  02c9 63ea0000          	dec	L3_ilTxCyclicCounter,y
1182  02cd                   L173:
1183                         ; 1836             if (ilTxCyclicCounter[ilTxHnd] == 0)
1185  02cd e6ea0000          	ldab	L3_ilTxCyclicCounter,y
1186  02d1 2619              	bne	L763
1187                         ; 1866                 ilTxCyclicCounter[ilTxHnd] = IlGetTxCyclicCycles(ilTxHnd);
1189  02d3 180aea0000ea0000  	movb	_IlTxCyclicCycles,y,L3_ilTxCyclicCounter,y
1190                         ; 1880                   CanInterruptDisable();
1192  02db 4a000000          	call	f_VStdSuspendAllInterrupts
1194                         ; 1881                   ilTxState[ilTxHnd] |= kTxSendRequest;
1196  02df e680              	ldab	OFST-1,s
1197  02e1 b796              	exg	b,y
1198  02e3 0cea000680        	bset	_ilTxState,y,128
1199                         ; 1882                   CanInterruptRestore();
1201  02e8 4a000000          	call	f_VStdResumeAllInterrupts
1203  02ec                   L763:
1204                         ; 1931           if (((ilTxState[ilTxHnd] & kTxSendRequest) != 0) && (ilTxUpdateCounter[ilTxHnd] == 0))
1206  02ec e680              	ldab	OFST-1,s
1207  02ee 87                	clra	
1208  02ef b746              	tfr	d,y
1209  02f1 0fea0006800a      	brclr	_ilTxState,y,128,L163
1211  02f7 e7ea0003          	tst	_ilTxUpdateCounter,y
1212  02fb 2604              	bne	L163
1213                         ; 1948             IlSendMessage(ilTxHnd);
1215  02fd 4a000000          	call	L5f_IlSendMessage
1217  0301                   L163:
1218                         ; 1956     while (ilTxHnd != 0);
1220  0301 e680              	ldab	OFST-1,s
1221  0303 2697              	bne	L753
1222  0305                   L553:
1223                         ; 1964 }                               /* End of IlTxTimerTask() */
1226  0305 1b81              	leas	1,s
1227  0307 0a                	rtc	
1268                         ; 1978 void IlCanCancelNotification(CanTransmitHandle txHandle)
1268                         ; 1979 {
1269                         	switch	.ftext
1270  0308                   f_IlCanCancelNotification:
1272  0308 3b                	pshd	
1273  0309 37                	pshb	
1274       00000001          OFST:	set	1
1277                         ; 1991   for (ilTxHnd = 0; ilTxHnd < kIlNumberOfTxObjects; ilTxHnd++)
1279  030a c7                	clrb	
1280  030b 6b80              	stab	OFST-1,s
1281  030d                   L514:
1282                         ; 1994     if (IlGetIlTxIndirection(ilTxHnd) == txHandle)
1284  030d 87                	clra	
1285  030e 59                	lsld	
1286  030f b746              	tfr	d,y
1287  0311 ecea0000          	ldd	_IlTxIndirection,y
1288  0315 ac81              	cpd	OFST+0,s
1289  0317 2608              	bne	L324
1290                         ; 1998       ilTxUpdateCounter[ilTxHnd] = 0;
1292  0319 e680              	ldab	OFST-1,s
1293  031b b796              	exg	b,y
1294  031d 69ea0003          	clr	_ilTxUpdateCounter,y
1295  0321                   L324:
1296                         ; 1991   for (ilTxHnd = 0; ilTxHnd < kIlNumberOfTxObjects; ilTxHnd++)
1298  0321 6280              	inc	OFST-1,s
1301  0323 e680              	ldab	OFST-1,s
1302  0325 c103              	cmpb	#3
1303  0327 25e4              	blo	L514
1304                         ; 2009 }                               /* End of IlCanCancelNotification() */
1307  0329 1b83              	leas	3,s
1308  032b 0a                	rtc	
1387                         ; 2033      vuint8 IlCanGenericPrecopy(CanRxInfoStructPtr rxStruct)
1387                         ; 2034 #  define canRxHnd  (rxStruct->Handle)
1387                         ; 2035 # endif
1387                         ; 2036 {
1388                         	switch	.ftext
1389  032c                   f_IlCanGenericPrecopy:
1393                         ; 2045   if (canRxHnd >= kIlCanNumberOfRxObjects)
1395  032c b746              	tfr	d,y
1396  032e ec46              	ldd	6,y
1397  0330 8c000c            	cpd	#12
1398  0333 2503              	blo	L564
1399                         ; 2047     return kCanCopyData;
1401  0335 c601              	ldab	#1
1404  0337 0a                	rtc	
1405  0338                   L564:
1406                         ; 2049   if (RxSuspended(IL_CHANNEL_ILPRECOPY_MACRO))
1408  0338 f6000d            	ldab	_ilChannelState
1409  033b c50c              	bitb	#12
1410  033d 2602              	bne	L764
1411                         ; 2051     return kCanNoCopyData;
1413  033f c7                	clrb	
1416  0340 0a                	rtc	
1417  0341                   L764:
1418                         ; 2065   return kCanCopyData;
1420  0341 c601              	ldab	#1
1423  0343 0a                	rtc	
1446                         ; 2087 Il_Status IlGetStatus(IL_CHANNEL_ILTYPE_ONLY)
1446                         ; 2088 {
1447                         	switch	.ftext
1448  0344                   f_IlGetStatus:
1452                         ; 2089   return (Il_Status) Il_ChannelState(IL_CHANNEL_ILPARA_MACRO);
1454  0344 f6000d            	ldab	_ilChannelState
1457  0347 0a                	rtc	
1489                         ; 2255 void IlSetEvent(IlTransmitHandle ilTxHnd) C_API_3
1489                         ; 2256 {
1490                         	switch	.ftext
1491  0348                   f_IlSetEvent:
1495                         ; 2267   ilTxState[ilTxHnd] |= kTxSendRequest;
1498  0348 b796              	exg	b,y
1499  034a 0cea000680        	bset	_ilTxState,y,128
1500                         ; 2268 }                               /* End of IlSetEvent() */
1503  034f 0a                	rtc	
1547                         ; 2300 vuint8 IlGetSignalIndicationFlag(IlReceiveFlagHandle ilRxFlagHnd)
1547                         ; 2301 {
1548                         	switch	.ftext
1549  0350                   f_IlGetSignalIndicationFlag:
1551  0350 3b                	pshd	
1552  0351 37                	pshb	
1553       00000001          OFST:	set	1
1556                         ; 2304   CanInterruptDisable();
1558  0352 4a000000          	call	f_VStdSuspendAllInterrupts
1560                         ; 2306   rc = ilRxIndicationFlags[IlIndicationOffset[ilRxFlagHnd]] & IlIndicationMask[ilRxFlagHnd];
1562  0356 e682              	ldab	OFST+1,s
1563  0358 87                	clra	
1564  0359 b746              	tfr	d,y
1565  035b e6ea0000          	ldab	_IlIndicationOffset,y
1566  035f b746              	tfr	d,y
1567  0361 e682              	ldab	OFST+1,s
1568  0363 b745              	tfr	d,x
1569  0365 e6e20000          	ldab	_IlIndicationMask,x
1570  0369 e4ea0009          	andb	_ilRxIndicationFlags,y
1571  036d 6b80              	stab	OFST-1,s
1572                         ; 2307   if (rc != 0)
1574  036f 270d              	beq	L335
1575                         ; 2310     ilRxIndicationFlags[IlIndicationOffset[ilRxFlagHnd]] &= (vuint8) ~ (IlIndicationMask[ilRxFlagHnd]);
1577  0371 e6e20000          	ldab	_IlIndicationMask,x
1578  0375 51                	comb	
1579  0376 e4ea0009          	andb	_ilRxIndicationFlags,y
1580  037a 6bea0009          	stab	_ilRxIndicationFlags,y
1581  037e                   L335:
1582                         ; 2313   CanInterruptRestore();
1584  037e 4a000000          	call	f_VStdResumeAllInterrupts
1586                         ; 2315   return (rc);
1588  0382 e680              	ldab	OFST-1,s
1591  0384 1b83              	leas	3,s
1592  0386 0a                	rtc	
1615                         ; 2323 void IlResetRxTimeoutFlags(IL_CHANNEL_ILTYPE_ONLY)
1615                         ; 2324 {
1616                         	switch	.ftext
1617  0387                   f_IlResetRxTimeoutFlags:
1621                         ; 2333 }                               /* End of IlResetRxTimeoutFlags() */
1625  0387 0a                	rtc	
1714                         	switch	.bss
1715  0000                   L3_ilTxCyclicCounter:
1716  0000 000000            	ds.b	3
1717                         	xref	f_IlResetCanConfirmationFlags
1718                         	xdef	f_IlResetRxTimeoutFlags
1719                         	xdef	f_IlGetSignalIndicationFlag
1720                         	xdef	f_IlGetStatus
1721                         	xdef	f_IlSetEvent
1722                         	xdef	f_IlTxTimerTask
1723                         	xdef	f_IlRxTimerTask
1724                         	xdef	f_IlTxStateTask
1725                         	xdef	f_IlRxStateTask
1726                         	xdef	f_IlTxTask
1727                         	xdef	f_IlRxTask
1728                         	xdef	f_IlTxRelease
1729                         	xdef	f_IlRxRelease
1730                         	xdef	f_IlTxWait
1731                         	xdef	f_IlRxWait
1732                         	xdef	f_IlTxStop
1733                         	xdef	f_IlRxStop
1734                         	xdef	f_IlTxStart
1735                         	xdef	f_IlRxStart
1736                         	xdef	f_IlInit
1737                         	xdef	f_IlInitPowerOn
1738                         	xref	_IlTxIndirection
1739                         	xref	_IlTxConfirmationFctPtr
1740                         	xref	_IlTxUpdateCycles
1741                         	xref	_IlTxStartCycles
1742                         	xref	_IlTxCyclicCycles
1743                         	xref	_IlTxType
1744                         	xref	_IlTxDefaultInitValue
1745                         	xref	_IlIndicationMask
1746                         	xref	_IlIndicationOffset
1747                         	xref	_IlRxDefaultInitValue
1748                         	xdef	_kIlReleaseVersion
1749                         	xdef	_kIlSubVersion
1750                         	xdef	_kIlMainVersion
1751  0003                   _ilTxUpdateCounter:
1752  0003 000000            	ds.b	3
1753                         	xdef	_ilTxUpdateCounter
1754  0006                   _ilTxState:
1755  0006 000000            	ds.b	3
1756                         	xdef	_ilTxState
1757  0009                   _ilRxIndicationFlags:
1758  0009 00000000          	ds.b	4
1759                         	xdef	_ilRxIndicationFlags
1760  000d                   _ilChannelState:
1761  000d 00                	ds.b	1
1762                         	xdef	_ilChannelState
1763                         	xref	f_CanTransmit
1764                         	xdef	f_IlCanCancelNotification
1765                         	xdef	f_IlCanGenericPrecopy
1766                         	xref	_CanRxDataPtr
1767                         	xref	_CanRxDataLen
1768                         	xref	_CanConfirmationMask
1769                         	xref	_CanConfirmationOffset
1770                         	xref	_CanTxDataPtr
1771                         	xref	_CanTxDLC
1772                         	xref	_CanConfirmationFlags
1773                         	xref	f_VStdResumeAllInterrupts
1774                         	xref	f_VStdSuspendAllInterrupts
1795                         	end
