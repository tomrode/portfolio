   1                         ; C Compiler for S12XE [COSMIC Software].
   2                         ; Generator V4.7.8 - 27 Jul 2007
   3                         ; Optimizer V4.6.13 - 27 Jul 2007
   4                         .const:	section	.data
   5  0000                   _CanTxId0:
   6  0000 f3f941da          	dc.l	-201768486
   7  0004 f09a8060          	dc.l	-258310048
   8  0008 c6dde384          	dc.l	-958536828
   9  000c 38fe8060          	dc.l	956203104
  10  0010 31798060          	dc.l	830046304
  11  0014 00180ffe          	dc.l	1576958
  12  0018                   _CanTxDLC:
  13  0018 08                	dc.b	8
  14  0019 06                	dc.b	6
  15  001a 08                	dc.b	8
  16  001b 02                	dc.b	2
  17  001c 02                	dc.b	2
  18  001d 08                	dc.b	8
  19  001e                   _CanTxDataPtr:
  20  001e 0000              	dc.w	_ECU_APPL_CSWM
  21  0020 0000              	dc.w	_CFG_DATA_CODE_RSP_CSWM
  22  0022 0000              	dc.w	_DIAGNOSTIC_RESPONSE_CSWM
  23  0024 0000              	dc.w	_nmSendMess
  24  0026 0000              	dc.w	_CSWM_A1
  25  0028 0000              	dc.w	_TxDynamicMsg0
  26  002a                   _CanTxApplConfirmationPtr:
  27  002a 00000000          	dc.l	0
  28  002e 00000000          	dc.l	0
  29  0032 00000000          	dc.l	0
  31  0036 0000              	dc.w	f_NmConfirmation
  32  0038 0000              	dc.b	page(f_NmConfirmation),0
  33  003a 00000000          	dc.l	0
  35  003e 0000              	dc.w	f_TpDrvConfirmation
  36  0040 0000              	dc.b	page(f_TpDrvConfirmation),0
  37  0042                   _CanTxSendMask:
  38  0042 01                	dc.b	1
  39  0043 01                	dc.b	1
  40  0044 00                	dc.b	0
  41  0045 02                	dc.b	2
  42  0046 01                	dc.b	1
  43  0047 00                	dc.b	0
  44  0048                   _CanConfirmationOffset:
  45  0048 00                	dc.b	0
  46  0049 00                	dc.b	0
  47  004a 00                	dc.b	0
  48  004b 00                	dc.b	0
  49  004c 00                	dc.b	0
  50  004d 00                	dc.b	0
  51  004e                   _CanConfirmationMask:
  52  004e 01                	dc.b	1
  53  004f 02                	dc.b	2
  54  0050 00                	dc.b	0
  55  0051 00                	dc.b	0
  56  0052 04                	dc.b	4
  57  0053 00                	dc.b	0
  58  0054                   _CanRxId0:
  59  0054 f3f941d8          	dc.l	-201768488
  60  0058 f09a8000          	dc.l	-258310144
  61  005c c6dffde2          	dc.l	-958399006
  62  0060 705a8000          	dc.l	1884979200
  63  0064 601a8006          	dc.l	1612349446
  64  0068 38fe800a          	dc.l	956203018
  65  006c 31fa8000          	dc.l	838500352
  66  0070 31798400          	dc.l	830047232
  67  0074 313a8000          	dc.l	825917440
  68  0078 21da8000          	dc.l	567967744
  69  007c 211a8002          	dc.l	555384834
  70  0080 193a8000          	dc.l	423264256
  71  0084 10faa000          	dc.l	284860416
  72  0088 10fa800a          	dc.l	284852234
  73  008c                   _CanRxMsgIndirection:
  74  008c 0000              	dc.w	0
  75  008e 0001              	dc.w	1
  76  0090 000c              	dc.w	12
  77  0092 000d              	dc.w	13
  78  0094 0002              	dc.w	2
  79  0096 0003              	dc.w	3
  80  0098 0004              	dc.w	4
  81  009a 0005              	dc.w	5
  82  009c 0006              	dc.w	6
  83  009e 0007              	dc.w	7
  84  00a0 0008              	dc.w	8
  85  00a2 0009              	dc.w	9
  86  00a4 000a              	dc.w	10
  87  00a6 000b              	dc.w	11
  88  00a8                   _CanRxDataLen:
  89  00a8 08                	dc.b	8
  90  00a9 06                	dc.b	6
  91  00aa 04                	dc.b	4
  92  00ab 02                	dc.b	2
  93  00ac 02                	dc.b	2
  94  00ad 07                	dc.b	7
  95  00ae 01                	dc.b	1
  96  00af 04                	dc.b	4
  97  00b0 08                	dc.b	8
  98  00b1 08                	dc.b	8
  99  00b2 01                	dc.b	1
 100  00b3 02                	dc.b	2
 101  00b4 08                	dc.b	8
 102  00b5 06                	dc.b	6
 103  00b6                   _CanRxDataPtr:
 104  00b6 0000              	dc.w	_APPL_ECU_CSWM
 105  00b8 0000              	dc.w	_CONFIGURATION_DATA_CODE_REQUEST
 106  00ba 0000              	dc.w	_TRIP_A_B
 107  00bc 0000              	dc.w	_TGW_A1
 108  00be 0000              	dc.w	_ENVIRONMENTAL_CONDITIONS
 109  00c0 0000              	dc.w	_StW_Actn_Rq
 110  00c2 0000              	dc.w	_STATUS_C_CAN
 111  00c4 0000              	dc.w	_VEHICLE_SPEED_ODOMETER
 112  00c6 0000              	dc.w	_STATUS_B_ECM
 113  00c8 0000              	dc.w	_VIN
 114  00ca 0000              	dc.w	_CBC_I4
 115  00cc 0000              	dc.w	_CFG_RQ
 116  00ce 0000              	dc.w	_DIAGNOSTIC_REQUEST_FUNCTIONAL
 117  00d0 0000              	dc.w	_NWM_BCM
 118  00d2                   _CanRxApplPrecopyPtr:
 119  00d2 00000000          	dc.l	0
 120  00d6 00000000          	dc.l	0
 121  00da 00000000          	dc.l	0
 122  00de 00000000          	dc.l	0
 123  00e2 00000000          	dc.l	0
 124  00e6 00000000          	dc.l	0
 125  00ea 00000000          	dc.l	0
 126  00ee 00000000          	dc.l	0
 127  00f2 00000000          	dc.l	0
 128  00f6 00000000          	dc.l	0
 129  00fa 00000000          	dc.l	0
 130  00fe 00000000          	dc.l	0
 131  0102 00000000          	dc.l	0
 133  0106 0000              	dc.w	f_NmPrecopy
 134  0108 0000              	dc.b	page(f_NmPrecopy),0
 135  010a                   _CanRxApplIndicationPtr:
 137  010a 0000              	dc.w	f_IlMsgAPPL_ECU_CSWMIndication
 138  010c 0000              	dc.b	page(f_IlMsgAPPL_ECU_CSWMIndication),0
 140  010e 0000              	dc.w	f_IlMsgCONFIGURATION_DATA_CODE_REQUESTIndication
 141  0110 0000              	dc.b	page(f_IlMsgCONFIGURATION_DATA_CODE_REQUESTIndication),0
 143  0112 0000              	dc.w	f_IlMsgTRIP_A_BIndication
 144  0114 0000              	dc.b	page(f_IlMsgTRIP_A_BIndication),0
 146  0116 0000              	dc.w	f_IlMsgTGW_A1Indication
 147  0118 0000              	dc.b	page(f_IlMsgTGW_A1Indication),0
 149  011a 0000              	dc.w	f_IlMsgENVIRONMENTAL_CONDITIONSIndication
 150  011c 0000              	dc.b	page(f_IlMsgENVIRONMENTAL_CONDITIONSIndication),0
 152  011e 0000              	dc.w	f_IlMsgStW_Actn_RqIndication
 153  0120 0000              	dc.b	page(f_IlMsgStW_Actn_RqIndication),0
 155  0122 0000              	dc.w	f_IlMsgSTATUS_C_CANIndication
 156  0124 0000              	dc.b	page(f_IlMsgSTATUS_C_CANIndication),0
 158  0126 0000              	dc.w	f_IlMsgVEHICLE_SPEED_ODOMETERIndication
 159  0128 0000              	dc.b	page(f_IlMsgVEHICLE_SPEED_ODOMETERIndication),0
 161  012a 0000              	dc.w	f_IlMsgSTATUS_B_ECMIndication
 162  012c 0000              	dc.b	page(f_IlMsgSTATUS_B_ECMIndication),0
 164  012e 0000              	dc.w	f_IlMsgVINIndication
 165  0130 0000              	dc.b	page(f_IlMsgVINIndication),0
 167  0132 0000              	dc.w	f_IlMsgCBC_I4Indication
 168  0134 0000              	dc.b	page(f_IlMsgCBC_I4Indication),0
 170  0136 0000              	dc.w	f_IlMsgCFG_RQIndication
 171  0138 0000              	dc.b	page(f_IlMsgCFG_RQIndication),0
 172  013a 00000000          	dc.l	0
 173  013e 00000000          	dc.l	0
 174  0142                   _CanIndicationOffset:
 175  0142 00                	dc.b	0
 176  0143 00                	dc.b	0
 177  0144 00                	dc.b	0
 178  0145 00                	dc.b	0
 179  0146 00                	dc.b	0
 180  0147 00                	dc.b	0
 181  0148 00                	dc.b	0
 182  0149 00                	dc.b	0
 183  014a 00                	dc.b	0
 184  014b 00                	dc.b	0
 185  014c 00                	dc.b	0
 186  014d 00                	dc.b	0
 187  014e 00                	dc.b	0
 188  014f 00                	dc.b	0
 189  0150                   _CanIndicationMask:
 190  0150 00                	dc.b	0
 191  0151 00                	dc.b	0
 192  0152 00                	dc.b	0
 193  0153 00                	dc.b	0
 194  0154 00                	dc.b	0
 195  0155 00                	dc.b	0
 196  0156 00                	dc.b	0
 197  0157 00                	dc.b	0
 198  0158 00                	dc.b	0
 199  0159 00                	dc.b	0
 200  015a 00                	dc.b	0
 201  015b 00                	dc.b	0
 202  015c 01                	dc.b	1
 203  015d 02                	dc.b	2
 204  015e                   _CanInitObjectStartIndex:
 205  015e 00                	dc.b	0
 206  015f 01                	dc.b	1
 207  0160                   _CanInitObject:
 208  0160 04                	dc.b	4
 209  0161 84                	dc.b	132
 210  0162 83                	dc.b	131
 211  0163 2b                	dc.b	43
 212  0164 85                	dc.b	133
 213  0165 00                	dc.b	0
 214  0166 00                	dc.b	0
 215  0167 08                	dc.b	8
 216  0168 00                	dc.b	0
 217  0169 00                	dc.b	0
 218  016a e7                	dc.b	231
 219  016b f7                	dc.b	247
 220  016c ff                	dc.b	255
 221  016d fe                	dc.b	254
 222  016e 00                	dc.b	0
 223  016f 08                	dc.b	8
 224  0170 00                	dc.b	0
 225  0171 00                	dc.b	0
 226  0172 ff                	dc.b	255
 227  0173 f7                	dc.b	247
 228  0174 e5                	dc.b	229
 229  0175 fe                	dc.b	254
 602                         	xdef	_CanInitObjectStartIndex
 603                         	xref	f_IlMsgCFG_RQIndication
 604                         	xref	f_IlMsgCBC_I4Indication
 605                         	xref	f_IlMsgVINIndication
 606                         	xref	f_IlMsgSTATUS_B_ECMIndication
 607                         	xref	f_IlMsgVEHICLE_SPEED_ODOMETERIndication
 608                         	xref	f_IlMsgSTATUS_C_CANIndication
 609                         	xref	f_IlMsgStW_Actn_RqIndication
 610                         	xref	f_IlMsgENVIRONMENTAL_CONDITIONSIndication
 611                         	xref	f_IlMsgTGW_A1Indication
 612                         	xref	f_IlMsgTRIP_A_BIndication
 613                         	xref	f_IlMsgCONFIGURATION_DATA_CODE_REQUESTIndication
 614                         	xref	f_IlMsgAPPL_ECU_CSWMIndication
 615                         	xref	f_TpDrvConfirmation
 616                         	xref	f_NmConfirmation
 617                         	xref	f_NmPrecopy
 618                         	xref	_nmSendMess
 619                         	xref	_TxDynamicMsg0
 620                         	xref	_APPL_ECU_CSWM
 621                         	xref	_CBC_I4
 622                         	xref	_CFG_DATA_CODE_RSP_CSWM
 623                         	xref	_CFG_RQ
 624                         	xref	_CONFIGURATION_DATA_CODE_REQUEST
 625                         	xref	_CSWM_A1
 626                         	xref	_DIAGNOSTIC_REQUEST_FUNCTIONAL
 627                         	xref	_DIAGNOSTIC_RESPONSE_CSWM
 628                         	xref	_ECU_APPL_CSWM
 629                         	xref	_ENVIRONMENTAL_CONDITIONS
 630                         	xref	_NWM_BCM
 631                         	xref	_STATUS_B_ECM
 632                         	xref	_STATUS_C_CAN
 633                         	xref	_StW_Actn_Rq
 634                         	xref	_TGW_A1
 635                         	xref	_TRIP_A_B
 636                         	xref	_VEHICLE_SPEED_ODOMETER
 637                         	xref	_VIN
 638                         	xdef	_CanInitObject
 639                         	xdef	_CanRxApplIndicationPtr
 640                         	xdef	_CanIndicationMask
 641                         	xdef	_CanIndicationOffset
 642                         	xdef	_CanRxApplPrecopyPtr
 643                         	xdef	_CanRxDataPtr
 644                         	xdef	_CanRxDataLen
 645                         	xdef	_CanRxMsgIndirection
 646                         	xdef	_CanRxId0
 647                         	xdef	_CanTxApplConfirmationPtr
 648                         	xdef	_CanConfirmationMask
 649                         	xdef	_CanConfirmationOffset
 650                         	xdef	_CanTxDataPtr
 651                         	xdef	_CanTxDLC
 652                         	xdef	_CanTxSendMask
 653                         	xdef	_CanTxId0
 672                         	end
