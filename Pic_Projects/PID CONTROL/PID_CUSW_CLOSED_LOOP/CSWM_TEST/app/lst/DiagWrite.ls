   1                         ; C Compiler for S12XE [COSMIC Software].
   2                         ; Generator V4.7.8 - 27 Jul 2007
   3                         ; Optimizer V4.6.13 - 27 Jul 2007
 687                         ; 153 void diagWriteF_CyclicTask(void)
 687                         ; 154 {
 688                         .ftext:	section	.text
 689  0000                   f_diagWriteF_CyclicTask:
 693                         ; 156 	if (diag_writedata_b)
 695  0000 1f00000208        	brclr	_diag_flags_c,2,L544
 696                         ; 158 		diagLF_WriteData(diag_writedata_mode_c/*, pMsgContext*/);
 698  0005 f60000            	ldab	_diag_writedata_mode_c
 699  0008 87                	clra	
 700  0009 4a000e0e          	call	f_diagLF_WriteData
 703  000d                   L544:
 704                         ; 164 }	
 707  000d 0a                	rtc	
 758                         ; 166 void diagLF_WriteData(unsigned char service/*, DescMsgContext* pMsgContext*/)
 758                         ; 167 {
 759                         	switch	.ftext
 760  000e                   f_diagLF_WriteData:
 762  000e 3b                	pshd	
 763  000f 3b                	pshd	
 764       00000002          OFST:	set	2
 767                         ; 171 	switch (service) {
 770  0010 87                	clra	
 771  0011 c11b              	cmpb	#27
 772  0013 182401ed          	bhs	L135
 773  0017 59                	lsld	
 774  0018 05ff              	jmp	[d,pc]
 775  001a 0050              	dc.w	L744
 776  001c 006f              	dc.w	L354
 777  001e 007f              	dc.w	L554
 778  0020 005f              	dc.w	L154
 779  0022 008f              	dc.w	L754
 780  0024 009f              	dc.w	L164
 781  0026 00af              	dc.w	L364
 782  0028 0204              	dc.w	L135
 783  002a 00eb              	dc.w	L574
 784  002c 010b              	dc.w	L105
 785  002e 012b              	dc.w	L505
 786  0030 00fb              	dc.w	L774
 787  0032 011b              	dc.w	L305
 788  0034 00bf              	dc.w	L564
 789  0036 00c7              	dc.w	L764
 790  0038 00cb              	dc.w	L174
 791  003a 019c              	dc.w	L715
 792  003c 0204              	dc.w	L135
 793  003e 00cf              	dc.w	L374
 794  0040 013b              	dc.w	L705
 795  0042 0170              	dc.w	L115
 796  0044 0181              	dc.w	L315
 797  0046 0190              	dc.w	L515
 798  0048 01cf              	dc.w	L125
 799  004a 01e2              	dc.w	L325
 800  004c 01ee              	dc.w	L525
 801  004e 01fa              	dc.w	L725
 802  0050                   L744:
 803                         ; 174 			if ( hsF_WriteData_to_ShadowEEPROM( HS_PWM))
 805  0050 87                	clra	
 806  0051 c7                	clrb	
 807  0052 4a000000          	call	f_hsF_WriteData_to_ShadowEEPROM
 809  0056 d7                	tstb	
 810  0057 182701b0          	beq	L355
 811                         ; 177 				diag_writedata_b = 0;
 813                         ; 178 				diag_writedata_mode_c = 0;
 816  005b 182001a5          	bra	L135
 817  005f                   L154:
 818                         ; 189 			if ( hsF_WriteData_to_ShadowEEPROM( HS_TIMEOUT))
 820  005f cc0001            	ldd	#1
 821  0062 4a000000          	call	f_hsF_WriteData_to_ShadowEEPROM
 823  0066 d7                	tstb	
 824  0067 182701a0          	beq	L355
 825                         ; 192 				diag_writedata_b = 0;
 827                         ; 193 				diag_writedata_mode_c = 0;
 830  006b 18200195          	bra	L135
 831  006f                   L354:
 832                         ; 203 			if ( vsF_Write_Diag_Data(VS_PWM) == TRUE)
 834  006f cc0002            	ldd	#2
 835  0072 4a000000          	call	f_vsF_Write_Diag_Data
 837  0076 53                	decb	
 838  0077 18260190          	bne	L355
 839                         ; 205 				diag_writedata_b = 0;
 841                         ; 206 				diag_writedata_mode_c = 0;
 844  007b 18200185          	bra	L135
 845  007f                   L554:
 846                         ; 216 			if ( stWF_Write_Diag_Data(HSW_PWM) == TRUE)
 848  007f cc0003            	ldd	#3
 849  0082 4a000000          	call	f_stWF_Write_Diag_Data
 851  0086 53                	decb	
 852  0087 18260180          	bne	L355
 853                         ; 219 				diag_writedata_b = 0;
 855                         ; 220 				diag_writedata_mode_c = 0;
 858  008b 18200175          	bra	L135
 859  008f                   L754:
 860                         ; 230 			if ( stWF_Write_Diag_Data(HSW_TIMEOUT) == TRUE)
 862  008f cc0004            	ldd	#4
 863  0092 4a000000          	call	f_stWF_Write_Diag_Data
 865  0096 53                	decb	
 866  0097 18260170          	bne	L355
 867                         ; 233 				diag_writedata_b = 0;
 869                         ; 234 				diag_writedata_mode_c = 0;
 872  009b 18200165          	bra	L135
 873  009f                   L164:
 874                         ; 258 			if ( BattVF_Write_Diag_Data(VOLTAGE_ON) == TRUE)
 876  009f cc0005            	ldd	#5
 877  00a2 4a000000          	call	f_BattVF_Write_Diag_Data
 879  00a6 53                	decb	
 880  00a7 18260160          	bne	L355
 881                         ; 261 				diag_writedata_b = 0;
 883                         ; 262 				diag_writedata_mode_c = 0;
 886  00ab 18200155          	bra	L135
 887  00af                   L364:
 888                         ; 272 			if ( BattVF_Write_Diag_Data(VOLTAGE_OFF) == TRUE)
 890  00af cc0006            	ldd	#6
 891  00b2 4a000000          	call	f_BattVF_Write_Diag_Data
 893  00b6 53                	decb	
 894  00b7 18260150          	bne	L355
 895                         ; 275 				diag_writedata_b = 0;
 897                         ; 276 				diag_writedata_mode_c = 0;
 900  00bb 18200145          	bra	L135
 901  00bf                   L564:
 902                         ; 289 			diag_load_bl_eep_backup_b = TRUE; //Time to take boot backup
 904  00bf 1c000040          	bset	_diag_io_lock3_flags_c,64
 905                         ; 292 			diag_writedata_b = 0;
 907                         ; 293 			diag_writedata_mode_c = 0;
 909                         ; 294 			break;
 911  00c3 1820013d          	bra	L135
 912  00c7                   L764:
 913                         ; 311 			diag_load_bl_eep_backup_b = TRUE; //Time to take boot backup
 915  00c7 1c000040          	bset	_diag_io_lock3_flags_c,64
 916                         ; 314 			diag_writedata_b = 0;
 918                         ; 315 			diag_writedata_mode_c = 0;
 920                         ; 316 			break;
 922  00cb                   L174:
 923                         ; 322 			diag_writedata_b = 0;
 925                         ; 323 			diag_writedata_mode_c = 0;
 927                         ; 324 			break;
 929  00cb 18200135          	bra	L135
 930  00cf                   L374:
 931                         ; 330 			EE_BlockWrite(EE_SERIAL_NUMBER, &diag_temp_buffer_c[ 0 ]);
 933  00cf cc0005            	ldd	#_diag_temp_buffer_c
 934  00d2 3b                	pshd	
 935  00d3 cc0047            	ldd	#71
 936  00d6 4a000000          	call	f_EE_BlockWrite
 938  00da 1b82              	leas	2,s
 939                         ; 333 			diag_writedata_b = 0;
 941  00dc 1d000002          	bclr	_diag_flags_c,2
 942                         ; 334 			diag_writedata_mode_c = 0;
 944  00e0 790000            	clr	_diag_writedata_mode_c
 945                         ; 335 			diag_load_bl_eep_backup_b = 1; //Its time to take backup for Boot data	
 947  00e3 1c000040          	bset	_diag_io_lock3_flags_c,64
 948                         ; 337 			break;
 950  00e7 18200120          	bra	L355
 951  00eb                   L574:
 952                         ; 343 			if ( hsF_WriteData_to_ShadowEEPROM( HS_NTC_CUT_OFF))
 954  00eb cc0009            	ldd	#9
 955  00ee 4a000000          	call	f_hsF_WriteData_to_ShadowEEPROM
 957  00f2 d7                	tstb	
 958  00f3 18270114          	beq	L355
 959                         ; 346 				diag_writedata_b = 0;
 961                         ; 347 				diag_writedata_mode_c = 0;
 964  00f7 18200109          	bra	L135
 965  00fb                   L774:
 966                         ; 357 			if ( hsF_WriteData_to_ShadowEEPROM( HS_MIN_TIMEOUT))
 968  00fb cc000c            	ldd	#12
 969  00fe 4a000000          	call	f_hsF_WriteData_to_ShadowEEPROM
 971  0102 d7                	tstb	
 972  0103 18270104          	beq	L355
 973                         ; 360 				diag_writedata_b = 0;
 975                         ; 361 				diag_writedata_mode_c = 0;
 978  0107 182000f9          	bra	L135
 979  010b                   L105:
 980                         ; 372 			if ( stWF_Write_Diag_Data(HSW_TEMP_CUT_OFF) == TRUE)
 982  010b cc000b            	ldd	#11
 983  010e 4a000000          	call	f_stWF_Write_Diag_Data
 985  0112 53                	decb	
 986  0113 182600f4          	bne	L355
 987                         ; 375 				diag_writedata_b = 0;
 989                         ; 376 				diag_writedata_mode_c = 0;
 992  0117 182000e9          	bra	L135
 993  011b                   L305:
 994                         ; 386 			if ( stWF_Write_Diag_Data(HSW_MIN_TIMEOUT) == TRUE)
 996  011b cc000d            	ldd	#13
 997  011e 4a000000          	call	f_stWF_Write_Diag_Data
 999  0122 53                	decb	
1000  0123 182600e4          	bne	L355
1001                         ; 389 				diag_writedata_b = 0;
1003                         ; 390 				diag_writedata_mode_c = 0;
1006  0127 182000d9          	bra	L135
1007  012b                   L505:
1008                         ; 401 			if ( hsF_WriteData_to_ShadowEEPROM( REMOTE_AMB_TEMP_LIMIT))
1010  012b cc000e            	ldd	#14
1011  012e 4a000000          	call	f_hsF_WriteData_to_ShadowEEPROM
1013  0132 d7                	tstb	
1014  0133 182700d4          	beq	L355
1015                         ; 404 				diag_writedata_b = 0;
1017                         ; 405 				diag_writedata_mode_c = 0;
1020  0137 182000c9          	bra	L135
1021  013b                   L705:
1022                         ; 418 			EE_BlockWrite(EE_INTERNAL_FLT_BYTE0, &intFlt_bytes[ZERO]);
1024  013b cc0000            	ldd	#_intFlt_bytes
1025  013e 3b                	pshd	
1026  013f cc0018            	ldd	#24
1027  0142 4a000000          	call	f_EE_BlockWrite
1029                         ; 419 			EE_BlockWrite(EE_INTERNAL_FLT_BYTE1, &intFlt_bytes[ONE]);
1031  0146 cc0001            	ldd	#_intFlt_bytes+1
1032  0149 6c80              	std	0,s
1033  014b cc0019            	ldd	#25
1034  014e 4a000000          	call	f_EE_BlockWrite
1036                         ; 420 			EE_BlockWrite(EE_INTERNAL_FLT_BYTE2, &intFlt_bytes[TWO]);
1038  0152 cc0002            	ldd	#_intFlt_bytes+2
1039  0155 6c80              	std	0,s
1040  0157 cc001a            	ldd	#26
1041  015a 4a000000          	call	f_EE_BlockWrite
1043                         ; 421 			EE_BlockWrite(EE_INTERNAL_FLT_BYTE3, &intFlt_bytes[THREE]);
1045  015e cc0003            	ldd	#_intFlt_bytes+3
1046  0161 6c80              	std	0,s
1047  0163 cc001b            	ldd	#27
1048  0166 4a000000          	call	f_EE_BlockWrite
1050  016a 1b82              	leas	2,s
1051                         ; 424 			diag_writedata_b = 0;
1053                         ; 425 			diag_writedata_mode_c = 0;			
1055                         ; 426 			break;
1057  016c 18200094          	bra	L135
1058  0170                   L115:
1059                         ; 432 			EE_BlockWrite(EE_CTRL_BYTE_FINAL, &diag_temp_buffer_c[ 0 ]);
1061  0170 cc0005            	ldd	#_diag_temp_buffer_c
1062  0173 3b                	pshd	
1063  0174 cc0023            	ldd	#35
1064  0177 4a000000          	call	f_EE_BlockWrite
1066  017b 1b82              	leas	2,s
1067                         ; 434 			diag_writedata_b = 0;
1069                         ; 435 			diag_writedata_mode_c = 0;
1071                         ; 436 			break;
1073  017d 18200083          	bra	L135
1074  0181                   L315:
1075                         ; 442 			EE_BlockWrite(EE_BOARD_TEMIC_BOM_NUMBER, &diag_temp_buffer_c[ 0 ]);
1077  0181 cc0005            	ldd	#_diag_temp_buffer_c
1078  0184 3b                	pshd	
1079  0185 cc0024            	ldd	#36
1080  0188 4a000000          	call	f_EE_BlockWrite
1082  018c 1b82              	leas	2,s
1083                         ; 444 			diag_writedata_b = 0;
1085                         ; 445 			diag_writedata_mode_c = 0;
1087                         ; 446 			break;
1089  018e 2074              	bra	L135
1090  0190                   L515:
1091                         ; 450 			if ( BattVF_Write_Diag_Data(BATTERY_COMPENSATION) == TRUE)
1093  0190 cc0007            	ldd	#7
1094  0193 4a000000          	call	f_BattVF_Write_Diag_Data
1096  0197 53                	decb	
1097  0198 2671              	bne	L355
1098                         ; 452 				diag_writedata_b = 0;
1100                         ; 453 				diag_writedata_mode_c = 0;
1103  019a 2068              	bra	L135
1104  019c                   L715:
1105                         ; 501 			EE_BlockWrite(EE_REMOTE_CTRL_STAT, &diag_temp_buffer_c[ 0 ]);
1107  019c cc0005            	ldd	#_diag_temp_buffer_c
1108  019f 3b                	pshd	
1109  01a0 cc0010            	ldd	#16
1110  01a3 4a000000          	call	f_EE_BlockWrite
1112  01a7 1b82              	leas	2,s
1113                         ; 503 			if(diag_temp_buffer_c[ 0 ] == 0x01)
1115  01a9 f60005            	ldab	_diag_temp_buffer_c
1116  01ac 042109            	dbne	b,L146
1117                         ; 506 				canio_RemSt_configStat_c = TRUE;
1119  01af c601              	ldab	#1
1120  01b1 7b0000            	stab	_canio_RemSt_configStat_c
1121                         ; 508 				canio_RemSt_stored_stat = REMST_ENABLED;
1123  01b4 c655              	ldab	#85
1124                         ; 510 				EE_BlockWrite(EE_REMOTE_CTRL_STAT, &canio_RemSt_stored_stat);
1128  01b6 2005              	bra	L346
1129  01b8                   L146:
1130                         ; 515 				canio_RemSt_configStat_c = FALSE;
1132  01b8 790000            	clr	_canio_RemSt_configStat_c
1133                         ; 517 				canio_RemSt_stored_stat = REMST_DISABLED;
1135  01bb c6aa              	ldab	#170
1136                         ; 519 				EE_BlockWrite(EE_REMOTE_CTRL_STAT, &canio_RemSt_stored_stat);
1139  01bd                   L346:
1140  01bd 7b0000            	stab	_canio_RemSt_stored_stat
1141  01c0 cc0000            	ldd	#_canio_RemSt_stored_stat
1142  01c3 3b                	pshd	
1143  01c4 cc0010            	ldd	#16
1144  01c7 4a000000          	call	f_EE_BlockWrite
1145  01cb 1b82              	leas	2,s
1146                         ; 527 			diag_writedata_b = 0;
1148                         ; 528 			diag_writedata_mode_c = 0;
1150                         ; 530 			break;
1152  01cd 2035              	bra	L135
1153  01cf                   L125:
1154                         ; 539 			mark_virginflag_fail_w = 0xA55A;
1156  01cf cca55a            	ldd	#-23206
1157  01d2 6c80              	std	OFST-2,s
1158                         ; 540 			EE_BlockWrite(EE_VRGN_FLAG_START_BLOCK, (UInt8*)&mark_virginflag_fail_w);        
1160  01d4 1a80              	leax	OFST-2,s
1161  01d6 34                	pshx	
1162  01d7 cc0001            	ldd	#1
1163  01da 4a000000          	call	f_EE_BlockWrite
1165  01de 1b82              	leas	2,s
1166                         ; 542 			diag_writedata_b = 0;
1168                         ; 543 			diag_writedata_mode_c = 0;
1170                         ; 544 			break;
1172  01e0 2022              	bra	L135
1173  01e2                   L325:
1174                         ; 549 			if ( hsF_WriteData_to_ShadowEEPROM( HS_PWM_REAR))
1176  01e2 cc0012            	ldd	#18
1177  01e5 4a000000          	call	f_hsF_WriteData_to_ShadowEEPROM
1179  01e9 04411f            	tbeq	b,L355
1180                         ; 552 				diag_writedata_b = 0;
1182                         ; 553 				diag_writedata_mode_c = 0;
1185  01ec 2016              	bra	L135
1186  01ee                   L525:
1187                         ; 564 			if ( hsF_WriteData_to_ShadowEEPROM( HS_TIMEOUT_REAR))
1189  01ee cc0013            	ldd	#19
1190  01f1 4a000000          	call	f_hsF_WriteData_to_ShadowEEPROM
1192  01f5 044113            	tbeq	b,L355
1193                         ; 567 				diag_writedata_b = 0;
1195                         ; 568 				diag_writedata_mode_c = 0;
1198  01f8 200a              	bra	L135
1199  01fa                   L725:
1200                         ; 579 			if ( hsF_WriteData_to_ShadowEEPROM( HS_NTC_CUTOFF_REAR))
1202  01fa cc0014            	ldd	#20
1203  01fd 4a000000          	call	f_hsF_WriteData_to_ShadowEEPROM
1205  0201 044107            	tbeq	b,L355
1206                         ; 582 				diag_writedata_b = 0;
1208                         ; 583 				diag_writedata_mode_c = 0;
1211  0204                   L135:
1212                         ; 596 			diag_writedata_b = 0;
1214                         ; 597 			diag_writedata_mode_c = 0;
1216  0204 1d000002          	bclr	_diag_flags_c,2
1217  0208 790000            	clr	_diag_writedata_mode_c
1218  020b                   L355:
1219                         ; 601 }
1222  020b 1b84              	leas	4,s
1223  020d 0a                	rtc	
1476                         ; 608 void DESC_API_CALLBACK_TYPE ApplDescWrite_222800_Heated_Seat_PWM_Calibration_Parameters_Front_Seats(DescMsgContext* pMsgContext)
1476                         ; 609 {
1477                         	switch	.ftext
1478  020e                   f_ApplDescWrite_222800_Heated_Seat_PWM_Calibration_Parameters_Front_Seats:
1480  020e 3b                	pshd	
1481       00000000          OFST:	set	0
1484                         ; 611 	DataLength = 1;
1486  020f cc0001            	ldd	#1
1487  0212 7c0001            	std	L324_DataLength
1488                         ; 614 	if (diag_sec_access5_state_c == ECU_UNLOCKED)
1490  0215 f60000            	ldab	_diag_sec_access5_state_c
1491  0218 c105              	cmpb	#5
1492  021a 262d              	bne	L3201
1493                         ; 616 		if (hsF_CheckDiagnoseParameter( HS_PWM,&pMsgContext->reqData[0] ))
1495  021c ecf30000          	ldd	[OFST+0,s]
1496  0220 3b                	pshd	
1497  0221 87                	clra	
1498  0222 c7                	clrb	
1499  0223 4a000000          	call	f_hsF_CheckDiagnoseParameter
1501  0227 1b82              	leas	2,s
1502  0229 044112            	tbeq	b,L5201
1503                         ; 619 			diag_writedata_b = 1;							
1505  022c 1c000002          	bset	_diag_flags_c,2
1506                         ; 620 			DataLength = 0;
1508  0230 18790001          	clrw	L324_DataLength
1509                         ; 621 			Response = RESPONSE_OK;
1511  0234 c601              	ldab	#1
1512  0236 7b0003            	stab	L124_Response
1513                         ; 622 			diag_writedata_mode_c = WRITE_DATA_FOR_HS_PWM;
1515  0239 790000            	clr	_diag_writedata_mode_c
1517  023c 2017              	bra	L1301
1518  023e                   L5201:
1519                         ; 627 			diag_writedata_b = 0;
1521  023e 1d000002          	bclr	_diag_flags_c,2
1522                         ; 628 			diag_writedata_mode_c = 0;
1524  0242 790000            	clr	_diag_writedata_mode_c
1525                         ; 630 			Response = REQUEST_OUT_OF_RANGE;
1527  0245 c604              	ldab	#4
1528  0247 2009              	bra	LC002
1529  0249                   L3201:
1530                         ; 636 		diag_writedata_b = 0;
1532  0249 1d000002          	bclr	_diag_flags_c,2
1533                         ; 637 		diag_writedata_mode_c = 0;
1535  024d 790000            	clr	_diag_writedata_mode_c
1536                         ; 639 		Response = SECURITY_ACCESS_DENIED;
1538  0250 c608              	ldab	#8
1539  0252                   LC002:
1540  0252 7b0003            	stab	L124_Response
1541  0255                   L1301:
1542                         ; 641 	diagF_DiagResponse(Response, DataLength+SIZE_OF_BLOCKNUMBER_K, pMsgContext);
1544  0255 ec80              	ldd	OFST+0,s
1545  0257 3b                	pshd	
1546  0258 fd0001            	ldy	L324_DataLength
1547  025b 1942              	leay	2,y
1548  025d 35                	pshy	
1549  025e f60003            	ldab	L124_Response
1550  0261 87                	clra	
1551  0262 4a000000          	call	f_diagF_DiagResponse
1553  0266 1b86              	leas	6,s
1554                         ; 642 }
1557  0268 0a                	rtc	
1604                         ; 647 void DESC_API_CALLBACK_TYPE ApplDescWrite_222801_Vented_Seat_PWM_Calibration_Parameters(DescMsgContext* pMsgContext)
1604                         ; 648 {
1605                         	switch	.ftext
1606  0269                   f_ApplDescWrite_222801_Vented_Seat_PWM_Calibration_Parameters:
1608  0269 3b                	pshd	
1609       00000000          OFST:	set	0
1612                         ; 650 	DataLength = 1;
1614  026a cc0001            	ldd	#1
1615  026d 7c0001            	std	L324_DataLength
1616                         ; 654 	if ((CSWM_config_c & CSWM_2VS_MASK_K) == CSWM_2VS_MASK_K) {
1618  0270 1e00003002        	brset	_CSWM_config_c,48,LC003
1619  0275 2040              	bra	L3501
1620  0277                   LC003:
1621                         ; 655 		if (diag_sec_access5_state_c == ECU_UNLOCKED)
1623  0277 f60000            	ldab	_diag_sec_access5_state_c
1624  027a c105              	cmpb	#5
1625  027c 262e              	bne	L5501
1626                         ; 657 			if ( vsF_Chck_Diag_Prms(VS_PWM, (&pMsgContext->reqData[0])) == TRUE)
1628  027e ecf30000          	ldd	[OFST+0,s]
1629  0282 3b                	pshd	
1630  0283 cc0002            	ldd	#2
1631  0286 4a000000          	call	f_vsF_Chck_Diag_Prms
1633  028a 1b82              	leas	2,s
1634  028c 042112            	dbne	b,L7501
1635                         ; 660 				diag_writedata_b = 1;
1637  028f 1c000002          	bset	_diag_flags_c,2
1638                         ; 661 				DataLength = 0;
1640  0293 18790001          	clrw	L324_DataLength
1641                         ; 662 				Response = RESPONSE_OK;
1643  0297 c601              	ldab	#1
1644  0299 7b0003            	stab	L124_Response
1645                         ; 663 				diag_writedata_mode_c = WRITE_DATA_FOR_VS_PWM;
1647  029c 7b0000            	stab	_diag_writedata_mode_c
1649  029f 2022              	bra	L5601
1650  02a1                   L7501:
1651                         ; 668 				diag_writedata_b = 0;
1653  02a1 1d000002          	bclr	_diag_flags_c,2
1654                         ; 669 				diag_writedata_mode_c = 0;
1656  02a5 790000            	clr	_diag_writedata_mode_c
1657                         ; 672 				Response = REQUEST_OUT_OF_RANGE;
1659  02a8 c604              	ldab	#4
1660  02aa 2014              	bra	LC004
1661  02ac                   L5501:
1662                         ; 678 			diag_writedata_b = 0;
1664  02ac 1d000002          	bclr	_diag_flags_c,2
1665                         ; 679 			diag_writedata_mode_c = 0;
1667  02b0 790000            	clr	_diag_writedata_mode_c
1668                         ; 682 			Response = SECURITY_ACCESS_DENIED;
1670  02b3 c608              	ldab	#8
1671  02b5 2009              	bra	LC004
1672  02b7                   L3501:
1673                         ; 688 		diag_writedata_b = 0;
1675  02b7 1d000002          	bclr	_diag_flags_c,2
1676                         ; 689 		diag_writedata_mode_c = 0;
1678  02bb 790000            	clr	_diag_writedata_mode_c
1679                         ; 692 		Response = SUBFUNCTION_NOT_SUPPORTED;
1681  02be c607              	ldab	#7
1682  02c0                   LC004:
1683  02c0 7b0003            	stab	L124_Response
1684  02c3                   L5601:
1685                         ; 694     diagF_DiagResponse(Response, DataLength+SIZE_OF_BLOCKNUMBER_K, pMsgContext);
1687  02c3 ec80              	ldd	OFST+0,s
1688  02c5 3b                	pshd	
1689  02c6 fd0001            	ldy	L324_DataLength
1690  02c9 1942              	leay	2,y
1691  02cb 35                	pshy	
1692  02cc f60003            	ldab	L124_Response
1693  02cf 87                	clra	
1694  02d0 4a000000          	call	f_diagF_DiagResponse
1696  02d4 1b86              	leas	6,s
1697                         ; 695 }
1700  02d6 0a                	rtc	
1747                         ; 700 void DESC_API_CALLBACK_TYPE ApplDescWrite_222802_Heated_Steering_Wheel_PWM_Calibration_Parameters(DescMsgContext* pMsgContext)
1747                         ; 701 {
1748                         	switch	.ftext
1749  02d7                   f_ApplDescWrite_222802_Heated_Steering_Wheel_PWM_Calibration_Parameters:
1751  02d7 3b                	pshd	
1752       00000000          OFST:	set	0
1755                         ; 703 	DataLength = 1;
1757  02d8 cc0001            	ldd	#1
1758  02db 7c0001            	std	L324_DataLength
1759                         ; 706 	if ((CSWM_config_c & CSWM_HSW_MASK_K) == CSWM_HSW_MASK_K)
1761  02de 1f00004041        	brclr	_CSWM_config_c,64,L7011
1762                         ; 708 		if (diag_sec_access5_state_c == ECU_UNLOCKED)
1764  02e3 f60000            	ldab	_diag_sec_access5_state_c
1765  02e6 c105              	cmpb	#5
1766  02e8 262f              	bne	L1111
1767                         ; 710 			if (stWF_Chck_Diag_Prms(HSW_PWM, (&pMsgContext->reqData[0])) == TRUE)
1769  02ea ecf30000          	ldd	[OFST+0,s]
1770  02ee 3b                	pshd	
1771  02ef cc0003            	ldd	#3
1772  02f2 4a000000          	call	f_stWF_Chck_Diag_Prms
1774  02f6 1b82              	leas	2,s
1775  02f8 042113            	dbne	b,L3111
1776                         ; 713 				diag_writedata_b = 1;
1778  02fb 1c000002          	bset	_diag_flags_c,2
1779                         ; 714 				DataLength = 0;
1781  02ff 18790001          	clrw	L324_DataLength
1782                         ; 715 				Response = RESPONSE_OK;
1784  0303 c601              	ldab	#1
1785  0305 7b0003            	stab	L124_Response
1786                         ; 716 				diag_writedata_mode_c = WRITE_DATA_FOR_HSW_PWM;
1788  0308 52                	incb	
1789  0309 7b0000            	stab	_diag_writedata_mode_c
1791  030c 2022              	bra	L1211
1792  030e                   L3111:
1793                         ; 721 				diag_writedata_b = 0;
1795  030e 1d000002          	bclr	_diag_flags_c,2
1796                         ; 722 				diag_writedata_mode_c = 0;
1798  0312 790000            	clr	_diag_writedata_mode_c
1799                         ; 725 				Response = REQUEST_OUT_OF_RANGE;
1801  0315 c604              	ldab	#4
1802  0317 2014              	bra	LC005
1803  0319                   L1111:
1804                         ; 731 			diag_writedata_b = 0;
1806  0319 1d000002          	bclr	_diag_flags_c,2
1807                         ; 732 			diag_writedata_mode_c = 0;
1809  031d 790000            	clr	_diag_writedata_mode_c
1810                         ; 735 			Response = SECURITY_ACCESS_DENIED;
1812  0320 c608              	ldab	#8
1813  0322 2009              	bra	LC005
1814  0324                   L7011:
1815                         ; 741 		diag_writedata_b = 0;
1817  0324 1d000002          	bclr	_diag_flags_c,2
1818                         ; 742 		diag_writedata_mode_c = 0;
1820  0328 790000            	clr	_diag_writedata_mode_c
1821                         ; 745 		Response = SUBFUNCTION_NOT_SUPPORTED;
1823  032b c607              	ldab	#7
1824  032d                   LC005:
1825  032d 7b0003            	stab	L124_Response
1826  0330                   L1211:
1827                         ; 747 	diagF_DiagResponse(Response, DataLength+SIZE_OF_BLOCKNUMBER_K, pMsgContext);
1829  0330 ec80              	ldd	OFST+0,s
1830  0332 3b                	pshd	
1831  0333 fd0001            	ldy	L324_DataLength
1832  0336 1942              	leay	2,y
1833  0338 35                	pshy	
1834  0339 f60003            	ldab	L124_Response
1835  033c 87                	clra	
1836  033d 4a000000          	call	f_diagF_DiagResponse
1838  0341 1b86              	leas	6,s
1839                         ; 748 }
1842  0343 0a                	rtc	
1888                         ; 754 void DESC_API_CALLBACK_TYPE ApplDescWrite_222803_Heated_Seat_Max_Timeout_Parameters_Front_Seats(DescMsgContext* pMsgContext)
1888                         ; 755 {
1889                         	switch	.ftext
1890  0344                   f_ApplDescWrite_222803_Heated_Seat_Max_Timeout_Parameters_Front_Seats:
1892  0344 3b                	pshd	
1893       00000000          OFST:	set	0
1896                         ; 757 	DataLength = 1;
1898  0345 cc0001            	ldd	#1
1899  0348 7c0001            	std	L324_DataLength
1900                         ; 760 	if (diag_sec_access5_state_c == ECU_UNLOCKED)
1902  034b f60000            	ldab	_diag_sec_access5_state_c
1903  034e c105              	cmpb	#5
1904  0350 2630              	bne	L3411
1905                         ; 762 		if ( hsF_CheckDiagnoseParameter( HS_TIMEOUT,&pMsgContext->reqData[0] ))
1907  0352 ecf30000          	ldd	[OFST+0,s]
1908  0356 3b                	pshd	
1909  0357 cc0001            	ldd	#1
1910  035a 4a000000          	call	f_hsF_CheckDiagnoseParameter
1912  035e 1b82              	leas	2,s
1913  0360 044114            	tbeq	b,L5411
1914                         ; 765 			diag_writedata_b = 1;
1916  0363 1c000002          	bset	_diag_flags_c,2
1917                         ; 766 			DataLength = 0;
1919  0367 18790001          	clrw	L324_DataLength
1920                         ; 767 			Response = RESPONSE_OK;
1922  036b c601              	ldab	#1
1923  036d 7b0003            	stab	L124_Response
1924                         ; 768 			diag_writedata_mode_c = WRITE_DATA_FOR_HS_TIMEOUT;
1926  0370 c603              	ldab	#3
1927  0372 7b0000            	stab	_diag_writedata_mode_c
1929  0375 2017              	bra	L1511
1930  0377                   L5411:
1931                         ; 773 			diag_writedata_b = 0;
1933  0377 1d000002          	bclr	_diag_flags_c,2
1934                         ; 774 			diag_writedata_mode_c = 0;
1936  037b 790000            	clr	_diag_writedata_mode_c
1937                         ; 776 			Response = REQUEST_OUT_OF_RANGE;
1939  037e c604              	ldab	#4
1940  0380 2009              	bra	LC006
1941  0382                   L3411:
1942                         ; 782 		diag_writedata_b = 0;
1944  0382 1d000002          	bclr	_diag_flags_c,2
1945                         ; 783 		diag_writedata_mode_c = 0;
1947  0386 790000            	clr	_diag_writedata_mode_c
1948                         ; 786 		Response = SECURITY_ACCESS_DENIED;
1950  0389 c608              	ldab	#8
1951  038b                   LC006:
1952  038b 7b0003            	stab	L124_Response
1953  038e                   L1511:
1954                         ; 788 	diagF_DiagResponse(Response, DataLength+SIZE_OF_BLOCKNUMBER_K, pMsgContext);
1956  038e ec80              	ldd	OFST+0,s
1957  0390 3b                	pshd	
1958  0391 fd0001            	ldy	L324_DataLength
1959  0394 1942              	leay	2,y
1960  0396 35                	pshy	
1961  0397 f60003            	ldab	L124_Response
1962  039a 87                	clra	
1963  039b 4a000000          	call	f_diagF_DiagResponse
1965  039f 1b86              	leas	6,s
1966                         ; 789 }
1969  03a1 0a                	rtc	
2016                         ; 794 void DESC_API_CALLBACK_TYPE ApplDescWrite_222804_Heated_Steering_Wheel_Max_Timeout_Parameters(DescMsgContext* pMsgContext)
2016                         ; 795 {
2017                         	switch	.ftext
2018  03a2                   f_ApplDescWrite_222804_Heated_Steering_Wheel_Max_Timeout_Parameters:
2020  03a2 3b                	pshd	
2021       00000000          OFST:	set	0
2024                         ; 797 	DataLength = 1;
2026  03a3 cc0001            	ldd	#1
2027  03a6 7c0001            	std	L324_DataLength
2028                         ; 800 	if ((CSWM_config_c & CSWM_HSW_MASK_K) == CSWM_HSW_MASK_K)
2030  03a9 1f00004042        	brclr	_CSWM_config_c,64,L3711
2031                         ; 802 		if (diag_sec_access5_state_c == ECU_UNLOCKED)
2033  03ae f60000            	ldab	_diag_sec_access5_state_c
2034  03b1 c105              	cmpb	#5
2035  03b3 2630              	bne	L5711
2036                         ; 804 			if ( stWF_Chck_Diag_Prms(HSW_TIMEOUT, (&pMsgContext->reqData[0])) == TRUE)
2038  03b5 ecf30000          	ldd	[OFST+0,s]
2039  03b9 3b                	pshd	
2040  03ba cc0004            	ldd	#4
2041  03bd 4a000000          	call	f_stWF_Chck_Diag_Prms
2043  03c1 1b82              	leas	2,s
2044  03c3 042114            	dbne	b,L7711
2045                         ; 807 				diag_writedata_b = 1;
2047  03c6 1c000002          	bset	_diag_flags_c,2
2048                         ; 808 				DataLength = 0;
2050  03ca 18790001          	clrw	L324_DataLength
2051                         ; 809 				Response = RESPONSE_OK;
2053  03ce c601              	ldab	#1
2054  03d0 7b0003            	stab	L124_Response
2055                         ; 810 				diag_writedata_mode_c = WRITE_DATA_FOR_HSW_TIMEOUT;
2057  03d3 c604              	ldab	#4
2058  03d5 7b0000            	stab	_diag_writedata_mode_c
2060  03d8 2022              	bra	L5021
2061  03da                   L7711:
2062                         ; 815 				diag_writedata_b = 0;
2064  03da 1d000002          	bclr	_diag_flags_c,2
2065                         ; 816 				diag_writedata_mode_c = 0;
2067  03de 790000            	clr	_diag_writedata_mode_c
2068                         ; 818 				Response = REQUEST_OUT_OF_RANGE;
2070  03e1 c604              	ldab	#4
2071  03e3 2014              	bra	LC007
2072  03e5                   L5711:
2073                         ; 824 			diag_writedata_b = 0;
2075  03e5 1d000002          	bclr	_diag_flags_c,2
2076                         ; 825 			diag_writedata_mode_c = 0;
2078  03e9 790000            	clr	_diag_writedata_mode_c
2079                         ; 828 			Response = SECURITY_ACCESS_DENIED;
2081  03ec c608              	ldab	#8
2082  03ee 2009              	bra	LC007
2083  03f0                   L3711:
2084                         ; 833 		diag_writedata_b = 0;
2086  03f0 1d000002          	bclr	_diag_flags_c,2
2087                         ; 834 		diag_writedata_mode_c = 0;
2089  03f4 790000            	clr	_diag_writedata_mode_c
2090                         ; 837 		Response = SUBFUNCTION_NOT_SUPPORTED;
2092  03f7 c607              	ldab	#7
2093  03f9                   LC007:
2094  03f9 7b0003            	stab	L124_Response
2095  03fc                   L5021:
2096                         ; 839 	diagF_DiagResponse(Response, DataLength+SIZE_OF_BLOCKNUMBER_K, pMsgContext);
2098  03fc ec80              	ldd	OFST+0,s
2099  03fe 3b                	pshd	
2100  03ff fd0001            	ldy	L324_DataLength
2101  0402 1942              	leay	2,y
2102  0404 35                	pshy	
2103  0405 f60003            	ldab	L124_Response
2104  0408 87                	clra	
2105  0409 4a000000          	call	f_diagF_DiagResponse
2107  040d 1b86              	leas	6,s
2108                         ; 840 }
2111  040f 0a                	rtc	
2156                         ; 845 void DESC_API_CALLBACK_TYPE ApplDescWrite_222805_Voltage_Level_ON(DescMsgContext* pMsgContext)
2156                         ; 846 {
2157                         	switch	.ftext
2158  0410                   f_ApplDescWrite_222805_Voltage_Level_ON:
2160  0410 3b                	pshd	
2161       00000000          OFST:	set	0
2164                         ; 848 	DataLength = 1;
2166  0411 cc0001            	ldd	#1
2167  0414 7c0001            	std	L324_DataLength
2168                         ; 851 	if (diag_sec_access5_state_c == ECU_UNLOCKED)
2170  0417 f60000            	ldab	_diag_sec_access5_state_c
2171  041a c105              	cmpb	#5
2172  041c 2630              	bne	L7221
2173                         ; 853 		if ( BattVF_Chck_Diag_Prms(VOLTAGE_ON, (&pMsgContext->reqData[0])) == TRUE)
2175  041e ecf30000          	ldd	[OFST+0,s]
2176  0422 3b                	pshd	
2177  0423 cc0005            	ldd	#5
2178  0426 4a000000          	call	f_BattVF_Chck_Diag_Prms
2180  042a 1b82              	leas	2,s
2181  042c 042114            	dbne	b,L1321
2182                         ; 856 			diag_writedata_b = 1;
2184  042f 1c000002          	bset	_diag_flags_c,2
2185                         ; 857 			DataLength = 0;
2187  0433 18790001          	clrw	L324_DataLength
2188                         ; 858 			Response = RESPONSE_OK;
2190  0437 c601              	ldab	#1
2191  0439 7b0003            	stab	L124_Response
2192                         ; 859 			diag_writedata_mode_c = WRITE_DATA_FOR_VOLTAGE_ON;
2194  043c c605              	ldab	#5
2195  043e 7b0000            	stab	_diag_writedata_mode_c
2197  0441 2017              	bra	L5321
2198  0443                   L1321:
2199                         ; 864 			diag_writedata_b = 0;
2201  0443 1d000002          	bclr	_diag_flags_c,2
2202                         ; 865 			diag_writedata_mode_c = 0;
2204  0447 790000            	clr	_diag_writedata_mode_c
2205                         ; 868 			Response = REQUEST_OUT_OF_RANGE;
2207  044a c604              	ldab	#4
2208  044c 2009              	bra	LC008
2209  044e                   L7221:
2210                         ; 874 		diag_writedata_b = 0;
2212  044e 1d000002          	bclr	_diag_flags_c,2
2213                         ; 875 		diag_writedata_mode_c = 0;
2215  0452 790000            	clr	_diag_writedata_mode_c
2216                         ; 878 		Response = SECURITY_ACCESS_DENIED;
2218  0455 c608              	ldab	#8
2219  0457                   LC008:
2220  0457 7b0003            	stab	L124_Response
2221  045a                   L5321:
2222                         ; 880 	diagF_DiagResponse(Response, DataLength+SIZE_OF_BLOCKNUMBER_K, pMsgContext);
2224  045a ec80              	ldd	OFST+0,s
2225  045c 3b                	pshd	
2226  045d fd0001            	ldy	L324_DataLength
2227  0460 1942              	leay	2,y
2228  0462 35                	pshy	
2229  0463 f60003            	ldab	L124_Response
2230  0466 87                	clra	
2231  0467 4a000000          	call	f_diagF_DiagResponse
2233  046b 1b86              	leas	6,s
2234                         ; 881 }
2237  046d 0a                	rtc	
2282                         ; 887 void DESC_API_CALLBACK_TYPE ApplDescWrite_222806_Voltage_Level_OFF(DescMsgContext* pMsgContext)
2282                         ; 888 {
2283                         	switch	.ftext
2284  046e                   f_ApplDescWrite_222806_Voltage_Level_OFF:
2286  046e 3b                	pshd	
2287       00000000          OFST:	set	0
2290                         ; 890 	DataLength = 1;
2292  046f cc0001            	ldd	#1
2293  0472 7c0001            	std	L324_DataLength
2294                         ; 893 	if (diag_sec_access5_state_c == ECU_UNLOCKED)
2296  0475 f60000            	ldab	_diag_sec_access5_state_c
2297  0478 c105              	cmpb	#5
2298  047a 2630              	bne	L7521
2299                         ; 895 		if ( BattVF_Chck_Diag_Prms(VOLTAGE_OFF, (&pMsgContext->reqData[0])) == TRUE)
2301  047c ecf30000          	ldd	[OFST+0,s]
2302  0480 3b                	pshd	
2303  0481 cc0006            	ldd	#6
2304  0484 4a000000          	call	f_BattVF_Chck_Diag_Prms
2306  0488 1b82              	leas	2,s
2307  048a 042114            	dbne	b,L1621
2308                         ; 898 			diag_writedata_b = 1;
2310  048d 1c000002          	bset	_diag_flags_c,2
2311                         ; 899 			DataLength = 0;
2313  0491 18790001          	clrw	L324_DataLength
2314                         ; 900 			Response = RESPONSE_OK;
2316  0495 c601              	ldab	#1
2317  0497 7b0003            	stab	L124_Response
2318                         ; 901 			diag_writedata_mode_c = WRITE_DATA_FOR_VOLTAGE_OFF;
2320  049a c606              	ldab	#6
2321  049c 7b0000            	stab	_diag_writedata_mode_c
2323  049f 2017              	bra	L5621
2324  04a1                   L1621:
2325                         ; 906 			diag_writedata_b = 0;
2327  04a1 1d000002          	bclr	_diag_flags_c,2
2328                         ; 907 			diag_writedata_mode_c = 0;
2330  04a5 790000            	clr	_diag_writedata_mode_c
2331                         ; 910 			Response = REQUEST_OUT_OF_RANGE;
2333  04a8 c604              	ldab	#4
2334  04aa 2009              	bra	LC009
2335  04ac                   L7521:
2336                         ; 916 		diag_writedata_b = 0;
2338  04ac 1d000002          	bclr	_diag_flags_c,2
2339                         ; 917 		diag_writedata_mode_c = 0;
2341  04b0 790000            	clr	_diag_writedata_mode_c
2342                         ; 919 		Response = SECURITY_ACCESS_DENIED;
2344  04b3 c608              	ldab	#8
2345  04b5                   LC009:
2346  04b5 7b0003            	stab	L124_Response
2347  04b8                   L5621:
2348                         ; 921 	diagF_DiagResponse(Response, DataLength+SIZE_OF_BLOCKNUMBER_K, pMsgContext);
2350  04b8 ec80              	ldd	OFST+0,s
2351  04ba 3b                	pshd	
2352  04bb fd0001            	ldy	L324_DataLength
2353  04be 1942              	leay	2,y
2354  04c0 35                	pshy	
2355  04c1 f60003            	ldab	L124_Response
2356  04c4 87                	clra	
2357  04c5 4a000000          	call	f_diagF_DiagResponse
2359  04c9 1b86              	leas	6,s
2360                         ; 922 }
2363  04cb 0a                	rtc	
2409                         ; 927 void DESC_API_CALLBACK_TYPE ApplDescWrite_222808_Heated_Seat_NTC_Cutoff_Parameters_Front_Seats(DescMsgContext* pMsgContext)
2409                         ; 928 {
2410                         	switch	.ftext
2411  04cc                   f_ApplDescWrite_222808_Heated_Seat_NTC_Cutoff_Parameters_Front_Seats:
2413  04cc 3b                	pshd	
2414       00000000          OFST:	set	0
2417                         ; 930 	DataLength = 1;
2419  04cd cc0001            	ldd	#1
2420  04d0 7c0001            	std	L324_DataLength
2421                         ; 932 	if ( diag_sec_access5_state_c == ECU_UNLOCKED)
2423  04d3 f60000            	ldab	_diag_sec_access5_state_c
2424  04d6 c105              	cmpb	#5
2425  04d8 2630              	bne	L7031
2426                         ; 934 		if ( hsF_CheckDiagnoseParameter( HS_NTC_CUT_OFF,&pMsgContext->reqData[0] ))
2428  04da ecf30000          	ldd	[OFST+0,s]
2429  04de 3b                	pshd	
2430  04df cc0009            	ldd	#9
2431  04e2 4a000000          	call	f_hsF_CheckDiagnoseParameter
2433  04e6 1b82              	leas	2,s
2434  04e8 044114            	tbeq	b,L1131
2435                         ; 937 			diag_writedata_b = 1;
2437  04eb 1c000002          	bset	_diag_flags_c,2
2438                         ; 938 			DataLength = 0;
2440  04ef 18790001          	clrw	L324_DataLength
2441                         ; 939 			Response = RESPONSE_OK;
2443  04f3 c601              	ldab	#1
2444  04f5 7b0003            	stab	L124_Response
2445                         ; 940 			diag_writedata_mode_c = WRITE_HEAT_NTC_CUTOFF;
2447  04f8 c608              	ldab	#8
2448  04fa 7b0000            	stab	_diag_writedata_mode_c
2450  04fd 2017              	bra	L5131
2451  04ff                   L1131:
2452                         ; 945 			diag_writedata_b = 0;
2454  04ff 1d000002          	bclr	_diag_flags_c,2
2455                         ; 946 			diag_writedata_mode_c = 0;
2457  0503 790000            	clr	_diag_writedata_mode_c
2458                         ; 949 			Response = REQUEST_OUT_OF_RANGE;
2460  0506 c604              	ldab	#4
2461  0508 2009              	bra	LC010
2462  050a                   L7031:
2463                         ; 955 		diag_writedata_b = 0;
2465  050a 1d000002          	bclr	_diag_flags_c,2
2466                         ; 956 		diag_writedata_mode_c = 0;
2468  050e 790000            	clr	_diag_writedata_mode_c
2469                         ; 959 		Response = SECURITY_ACCESS_DENIED;
2471  0511 c608              	ldab	#8
2472  0513                   LC010:
2473  0513 7b0003            	stab	L124_Response
2474  0516                   L5131:
2475                         ; 961 	diagF_DiagResponse(Response, DataLength+SIZE_OF_BLOCKNUMBER_K, pMsgContext);
2477  0516 ec80              	ldd	OFST+0,s
2478  0518 3b                	pshd	
2479  0519 fd0001            	ldy	L324_DataLength
2480  051c 1942              	leay	2,y
2481  051e 35                	pshy	
2482  051f f60003            	ldab	L124_Response
2483  0522 87                	clra	
2484  0523 4a000000          	call	f_diagF_DiagResponse
2486  0527 1b86              	leas	6,s
2487                         ; 962 }
2490  0529 0a                	rtc	
2537                         ; 967 void DESC_API_CALLBACK_TYPE ApplDescWrite_222809_Steering_Wheel_Temperature_Cut_off_Parameters (DescMsgContext* pMsgContext)
2537                         ; 968 {
2538                         	switch	.ftext
2539  052a                   f_ApplDescWrite_222809_Steering_Wheel_Temperature_Cut_off_Parameters:
2541  052a 3b                	pshd	
2542       00000000          OFST:	set	0
2545                         ; 970 	DataLength = 1;
2547  052b cc0001            	ldd	#1
2548  052e 7c0001            	std	L324_DataLength
2549                         ; 977 	if ((CSWM_config_c & CSWM_HSW_MASK_K) == CSWM_HSW_MASK_K)
2551  0531 1f00004042        	brclr	_CSWM_config_c,64,L7331
2552                         ; 979 		if (diag_sec_access5_state_c == ECU_UNLOCKED)
2554  0536 f60000            	ldab	_diag_sec_access5_state_c
2555  0539 c105              	cmpb	#5
2556  053b 2630              	bne	L1431
2557                         ; 981 			if ( stWF_Chck_Diag_Prms(HSW_TEMP_CUT_OFF, (&pMsgContext->reqData[0])) == TRUE)
2559  053d ecf30000          	ldd	[OFST+0,s]
2560  0541 3b                	pshd	
2561  0542 cc000b            	ldd	#11
2562  0545 4a000000          	call	f_stWF_Chck_Diag_Prms
2564  0549 1b82              	leas	2,s
2565  054b 042114            	dbne	b,L3431
2566                         ; 984 				diag_writedata_b = 1;
2568  054e 1c000002          	bset	_diag_flags_c,2
2569                         ; 985 				DataLength = 0;
2571  0552 18790001          	clrw	L324_DataLength
2572                         ; 986 				Response = RESPONSE_OK;
2574  0556 c601              	ldab	#1
2575  0558 7b0003            	stab	L124_Response
2576                         ; 987 				diag_writedata_mode_c = WRITE_HSW_TEMP_CUTOFF;
2578  055b c609              	ldab	#9
2579  055d 7b0000            	stab	_diag_writedata_mode_c
2581  0560 2022              	bra	L1531
2582  0562                   L3431:
2583                         ; 992 				diag_writedata_b = 0;
2585  0562 1d000002          	bclr	_diag_flags_c,2
2586                         ; 993 				diag_writedata_mode_c = 0;
2588  0566 790000            	clr	_diag_writedata_mode_c
2589                         ; 995 				Response = REQUEST_OUT_OF_RANGE;
2591  0569 c604              	ldab	#4
2592  056b 2014              	bra	LC011
2593  056d                   L1431:
2594                         ; 1001 			diag_writedata_b = 0;
2596  056d 1d000002          	bclr	_diag_flags_c,2
2597                         ; 1002 			diag_writedata_mode_c = 0;
2599  0571 790000            	clr	_diag_writedata_mode_c
2600                         ; 1005 			Response = SECURITY_ACCESS_DENIED;
2602  0574 c608              	ldab	#8
2603  0576 2009              	bra	LC011
2604  0578                   L7331:
2605                         ; 1010 		diag_writedata_b = 0;
2607  0578 1d000002          	bclr	_diag_flags_c,2
2608                         ; 1011 		diag_writedata_mode_c = 0;
2610  057c 790000            	clr	_diag_writedata_mode_c
2611                         ; 1014 		Response = SUBFUNCTION_NOT_SUPPORTED;
2613  057f c607              	ldab	#7
2614  0581                   LC011:
2615  0581 7b0003            	stab	L124_Response
2616  0584                   L1531:
2617                         ; 1016 	diagF_DiagResponse(Response, DataLength+SIZE_OF_BLOCKNUMBER_K, pMsgContext);
2619  0584 ec80              	ldd	OFST+0,s
2620  0586 3b                	pshd	
2621  0587 fd0001            	ldy	L324_DataLength
2622  058a 1942              	leay	2,y
2623  058c 35                	pshy	
2624  058d f60003            	ldab	L124_Response
2625  0590 87                	clra	
2626  0591 4a000000          	call	f_diagF_DiagResponse
2628  0595 1b86              	leas	6,s
2629                         ; 1017 }
2632  0597 0a                	rtc	
2679                         ; 1022 void DESC_API_CALLBACK_TYPE ApplDescWrite_22280A_HeatVent_Auto_Activation_Temperature_Calibration_Paramet(DescMsgContext* pMsgContext)
2679                         ; 1023 {
2680                         	switch	.ftext
2681  0598                   f_ApplDescWrite_22280A_HeatVent_Auto_Activation_Temperature_Calibration_Paramet:
2683  0598 3b                	pshd	
2684       00000000          OFST:	set	0
2687                         ; 1025 	DataLength = 1;
2689  0599 cc0001            	ldd	#1
2690  059c 7c0001            	std	L324_DataLength
2691                         ; 1034 	if ( diag_sec_access5_state_c == ECU_UNLOCKED)
2693  059f f60000            	ldab	_diag_sec_access5_state_c
2694  05a2 c105              	cmpb	#5
2695  05a4 2630              	bne	L3731
2696                         ; 1036 		if ( hsF_CheckDiagnoseParameter( REMOTE_AMB_TEMP_LIMIT,&pMsgContext->reqData[0] ))
2698  05a6 ecf30000          	ldd	[OFST+0,s]
2699  05aa 3b                	pshd	
2700  05ab cc000e            	ldd	#14
2701  05ae 4a000000          	call	f_hsF_CheckDiagnoseParameter
2703  05b2 1b82              	leas	2,s
2704  05b4 044114            	tbeq	b,L5731
2705                         ; 1039 			diag_writedata_b = 1;
2707  05b7 1c000002          	bset	_diag_flags_c,2
2708                         ; 1040 			DataLength = 0;
2710  05bb 18790001          	clrw	L324_DataLength
2711                         ; 1041 			Response = RESPONSE_OK;
2713  05bf c601              	ldab	#1
2714  05c1 7b0003            	stab	L124_Response
2715                         ; 1042 			diag_writedata_mode_c = WRITE_AMB_TEMP_LIMITS_REMOTEST;
2717  05c4 c60a              	ldab	#10
2718  05c6 7b0000            	stab	_diag_writedata_mode_c
2720  05c9 2017              	bra	L1041
2721  05cb                   L5731:
2722                         ; 1047 			diag_writedata_b = 0;
2724  05cb 1d000002          	bclr	_diag_flags_c,2
2725                         ; 1048 			diag_writedata_mode_c = 0;
2727  05cf 790000            	clr	_diag_writedata_mode_c
2728                         ; 1051 			Response = REQUEST_OUT_OF_RANGE;
2730  05d2 c604              	ldab	#4
2731  05d4 2009              	bra	LC012
2732  05d6                   L3731:
2733                         ; 1057 		diag_writedata_b = 0;
2735  05d6 1d000002          	bclr	_diag_flags_c,2
2736                         ; 1058 		diag_writedata_mode_c = 0;
2738  05da 790000            	clr	_diag_writedata_mode_c
2739                         ; 1061 		Response = SECURITY_ACCESS_DENIED;
2741  05dd c608              	ldab	#8
2742  05df                   LC012:
2743  05df 7b0003            	stab	L124_Response
2744  05e2                   L1041:
2745                         ; 1063 	diagF_DiagResponse(Response, DataLength+SIZE_OF_BLOCKNUMBER_K, pMsgContext);
2747  05e2 ec80              	ldd	OFST+0,s
2748  05e4 3b                	pshd	
2749  05e5 fd0001            	ldy	L324_DataLength
2750  05e8 1942              	leay	2,y
2751  05ea 35                	pshy	
2752  05eb f60003            	ldab	L124_Response
2753  05ee 87                	clra	
2754  05ef 4a000000          	call	f_diagF_DiagResponse
2756  05f3 1b86              	leas	6,s
2757                         ; 1064 }
2760  05f5 0a                	rtc	
2807                         ; 1069 void DESC_API_CALLBACK_TYPE ApplDescWrite_22280C_Heated_Steering_Wheel_Min_Timeout_Parameters(DescMsgContext* pMsgContext)
2807                         ; 1070 {
2808                         	switch	.ftext
2809  05f6                   f_ApplDescWrite_22280C_Heated_Steering_Wheel_Min_Timeout_Parameters:
2811  05f6 3b                	pshd	
2812       00000000          OFST:	set	0
2815                         ; 1072 	DataLength = 1;
2817  05f7 cc0001            	ldd	#1
2818  05fa 7c0001            	std	L324_DataLength
2819                         ; 1075 	if ((CSWM_config_c & CSWM_HSW_MASK_K) == CSWM_HSW_MASK_K)
2821  05fd 1f00004042        	brclr	_CSWM_config_c,64,L3241
2822                         ; 1077 		if (diag_sec_access5_state_c == ECU_UNLOCKED)
2824  0602 f60000            	ldab	_diag_sec_access5_state_c
2825  0605 c105              	cmpb	#5
2826  0607 2630              	bne	L5241
2827                         ; 1079 			if ( stWF_Chck_Diag_Prms(HSW_MIN_TIMEOUT, (&pMsgContext->reqData[0])) == TRUE)
2829  0609 ecf30000          	ldd	[OFST+0,s]
2830  060d 3b                	pshd	
2831  060e cc000d            	ldd	#13
2832  0611 4a000000          	call	f_stWF_Chck_Diag_Prms
2834  0615 1b82              	leas	2,s
2835  0617 042114            	dbne	b,L7241
2836                         ; 1082 				diag_writedata_b = 1;
2838  061a 1c000002          	bset	_diag_flags_c,2
2839                         ; 1083 				DataLength = 0;
2841  061e 18790001          	clrw	L324_DataLength
2842                         ; 1084 				Response = RESPONSE_OK;
2844  0622 c601              	ldab	#1
2845  0624 7b0003            	stab	L124_Response
2846                         ; 1085 				diag_writedata_mode_c = WRITE_HSW_MIN_TIMEOUT;
2848  0627 c60c              	ldab	#12
2849  0629 7b0000            	stab	_diag_writedata_mode_c
2851  062c 2022              	bra	L5341
2852  062e                   L7241:
2853                         ; 1090 				diag_writedata_b = 0;
2855  062e 1d000002          	bclr	_diag_flags_c,2
2856                         ; 1091 				diag_writedata_mode_c = 0;
2858  0632 790000            	clr	_diag_writedata_mode_c
2859                         ; 1093 				Response = REQUEST_OUT_OF_RANGE;
2861  0635 c604              	ldab	#4
2862  0637 2014              	bra	LC013
2863  0639                   L5241:
2864                         ; 1099 			diag_writedata_b = 0;
2866  0639 1d000002          	bclr	_diag_flags_c,2
2867                         ; 1100 			diag_writedata_mode_c = 0;
2869  063d 790000            	clr	_diag_writedata_mode_c
2870                         ; 1103 			Response = SECURITY_ACCESS_DENIED;
2872  0640 c608              	ldab	#8
2873  0642 2009              	bra	LC013
2874  0644                   L3241:
2875                         ; 1108 		diag_writedata_b = 0;
2877  0644 1d000002          	bclr	_diag_flags_c,2
2878                         ; 1109 		diag_writedata_mode_c = 0;
2880  0648 790000            	clr	_diag_writedata_mode_c
2881                         ; 1112 		Response = SUBFUNCTION_NOT_SUPPORTED;
2883  064b c607              	ldab	#7
2884  064d                   LC013:
2885  064d 7b0003            	stab	L124_Response
2886  0650                   L5341:
2887                         ; 1114 	diagF_DiagResponse(Response, DataLength+SIZE_OF_BLOCKNUMBER_K, pMsgContext);
2889  0650 ec80              	ldd	OFST+0,s
2890  0652 3b                	pshd	
2891  0653 fd0001            	ldy	L324_DataLength
2892  0656 1942              	leay	2,y
2893  0658 35                	pshy	
2894  0659 f60003            	ldab	L124_Response
2895  065c 87                	clra	
2896  065d 4a000000          	call	f_diagF_DiagResponse
2898  0661 1b86              	leas	6,s
2899                         ; 1115 }
2902  0663 0a                	rtc	
2949                         ; 1120 void DESC_API_CALLBACK_TYPE ApplDescWrite_22280E_Heated_Seat_PWM_Calibration_Parameters_Rear_Seats(DescMsgContext* pMsgContext)
2949                         ; 1121 {
2950                         	switch	.ftext
2951  0664                   f_ApplDescWrite_22280E_Heated_Seat_PWM_Calibration_Parameters_Rear_Seats:
2953  0664 3b                	pshd	
2954       00000000          OFST:	set	0
2957                         ; 1123 	DataLength = 1;
2959  0665 cc0001            	ldd	#1
2960  0668 7c0001            	std	L324_DataLength
2961                         ; 1125 	if ((CSWM_config_c & CSWM_4HS_VARIANT_MASK_K) == CSWM_4HS_VARIANT_MASK_K)
2963  066b 1e00000f02        	brset	_CSWM_config_c,15,LC014
2964  0670 2042              	bra	L7541
2965  0672                   LC014:
2966                         ; 1127 		if (diag_sec_access5_state_c == ECU_UNLOCKED)
2968  0672 f60000            	ldab	_diag_sec_access5_state_c
2969  0675 c105              	cmpb	#5
2970  0677 2630              	bne	L1641
2971                         ; 1129 			if (hsF_CheckDiagnoseParameter( HS_PWM_REAR,&pMsgContext->reqData[0] ))
2973  0679 ecf30000          	ldd	[OFST+0,s]
2974  067d 3b                	pshd	
2975  067e cc0012            	ldd	#18
2976  0681 4a000000          	call	f_hsF_CheckDiagnoseParameter
2978  0685 1b82              	leas	2,s
2979  0687 044114            	tbeq	b,L3641
2980                         ; 1132 				diag_writedata_b = 1;
2982  068a 1c000002          	bset	_diag_flags_c,2
2983                         ; 1133 				DataLength = 0;
2985  068e 18790001          	clrw	L324_DataLength
2986                         ; 1134 				Response = RESPONSE_OK;
2988  0692 c601              	ldab	#1
2989  0694 7b0003            	stab	L124_Response
2990                         ; 1135 				diag_writedata_mode_c = WRITE_HS_PWM_REAR;
2992  0697 c618              	ldab	#24
2993  0699 7b0000            	stab	_diag_writedata_mode_c
2995  069c 2022              	bra	L1741
2996  069e                   L3641:
2997                         ; 1140 				diag_writedata_b = 0;
2999  069e 1d000002          	bclr	_diag_flags_c,2
3000                         ; 1141 				diag_writedata_mode_c = 0;
3002  06a2 790000            	clr	_diag_writedata_mode_c
3003                         ; 1144 				Response = REQUEST_OUT_OF_RANGE;
3005  06a5 c604              	ldab	#4
3006  06a7 2014              	bra	LC015
3007  06a9                   L1641:
3008                         ; 1150 			diag_writedata_b = 0;
3010  06a9 1d000002          	bclr	_diag_flags_c,2
3011                         ; 1151 			diag_writedata_mode_c = 0;
3013  06ad 790000            	clr	_diag_writedata_mode_c
3014                         ; 1154 			Response = SECURITY_ACCESS_DENIED;
3016  06b0 c608              	ldab	#8
3017  06b2 2009              	bra	LC015
3018  06b4                   L7541:
3019                         ; 1160 		diag_writedata_b = 0;
3021  06b4 1d000002          	bclr	_diag_flags_c,2
3022                         ; 1161 		diag_writedata_mode_c = 0;
3024  06b8 790000            	clr	_diag_writedata_mode_c
3025                         ; 1164 		Response = SUBFUNCTION_NOT_SUPPORTED;
3027  06bb c607              	ldab	#7
3028  06bd                   LC015:
3029  06bd 7b0003            	stab	L124_Response
3030  06c0                   L1741:
3031                         ; 1166 	diagF_DiagResponse(Response, DataLength+SIZE_OF_BLOCKNUMBER_K, pMsgContext);
3033  06c0 ec80              	ldd	OFST+0,s
3034  06c2 3b                	pshd	
3035  06c3 fd0001            	ldy	L324_DataLength
3036  06c6 1942              	leay	2,y
3037  06c8 35                	pshy	
3038  06c9 f60003            	ldab	L124_Response
3039  06cc 87                	clra	
3040  06cd 4a000000          	call	f_diagF_DiagResponse
3042  06d1 1b86              	leas	6,s
3043                         ; 1168 }
3046  06d3 0a                	rtc	
3093                         ; 1174 void DESC_API_CALLBACK_TYPE ApplDescWrite_22280F_Heated_Seat_Max_Timeout_Parameters_Rear_Seats(DescMsgContext* pMsgContext)
3093                         ; 1175 {
3094                         	switch	.ftext
3095  06d4                   f_ApplDescWrite_22280F_Heated_Seat_Max_Timeout_Parameters_Rear_Seats:
3097  06d4 3b                	pshd	
3098       00000000          OFST:	set	0
3101                         ; 1177 	DataLength = 1;
3103  06d5 cc0001            	ldd	#1
3104  06d8 7c0001            	std	L324_DataLength
3105                         ; 1179 	if ((CSWM_config_c & CSWM_4HS_VARIANT_MASK_K) == CSWM_4HS_VARIANT_MASK_K)
3107  06db 1e00000f02        	brset	_CSWM_config_c,15,LC016
3108  06e0 2042              	bra	L3151
3109  06e2                   LC016:
3110                         ; 1181 		if (diag_sec_access5_state_c == ECU_UNLOCKED)
3112  06e2 f60000            	ldab	_diag_sec_access5_state_c
3113  06e5 c105              	cmpb	#5
3114  06e7 2630              	bne	L5151
3115                         ; 1183 			if (hsF_CheckDiagnoseParameter( HS_TIMEOUT_REAR,&pMsgContext->reqData[0] ))
3117  06e9 ecf30000          	ldd	[OFST+0,s]
3118  06ed 3b                	pshd	
3119  06ee cc0013            	ldd	#19
3120  06f1 4a000000          	call	f_hsF_CheckDiagnoseParameter
3122  06f5 1b82              	leas	2,s
3123  06f7 044114            	tbeq	b,L7151
3124                         ; 1186 				diag_writedata_b = 1;
3126  06fa 1c000002          	bset	_diag_flags_c,2
3127                         ; 1187 				DataLength = 0;
3129  06fe 18790001          	clrw	L324_DataLength
3130                         ; 1188 				Response = RESPONSE_OK;
3132  0702 c601              	ldab	#1
3133  0704 7b0003            	stab	L124_Response
3134                         ; 1189 				diag_writedata_mode_c = WRITE_HS_TIMEOUT_REAR;
3136  0707 c619              	ldab	#25
3137  0709 7b0000            	stab	_diag_writedata_mode_c
3139  070c 2022              	bra	L5251
3140  070e                   L7151:
3141                         ; 1194 				diag_writedata_b = 0;
3143  070e 1d000002          	bclr	_diag_flags_c,2
3144                         ; 1195 				diag_writedata_mode_c = 0;
3146  0712 790000            	clr	_diag_writedata_mode_c
3147                         ; 1198 				Response = REQUEST_OUT_OF_RANGE;
3149  0715 c604              	ldab	#4
3150  0717 2014              	bra	LC017
3151  0719                   L5151:
3152                         ; 1204 			diag_writedata_b = 0;
3154  0719 1d000002          	bclr	_diag_flags_c,2
3155                         ; 1205 			diag_writedata_mode_c = 0;
3157  071d 790000            	clr	_diag_writedata_mode_c
3158                         ; 1208 			Response = SECURITY_ACCESS_DENIED;
3160  0720 c608              	ldab	#8
3161  0722 2009              	bra	LC017
3162  0724                   L3151:
3163                         ; 1214 		diag_writedata_b = 0;
3165  0724 1d000002          	bclr	_diag_flags_c,2
3166                         ; 1215 		diag_writedata_mode_c = 0;
3168  0728 790000            	clr	_diag_writedata_mode_c
3169                         ; 1218 		Response = SUBFUNCTION_NOT_SUPPORTED;
3171  072b c607              	ldab	#7
3172  072d                   LC017:
3173  072d 7b0003            	stab	L124_Response
3174  0730                   L5251:
3175                         ; 1220 	diagF_DiagResponse(Response, DataLength+SIZE_OF_BLOCKNUMBER_K, pMsgContext);
3177  0730 ec80              	ldd	OFST+0,s
3178  0732 3b                	pshd	
3179  0733 fd0001            	ldy	L324_DataLength
3180  0736 1942              	leay	2,y
3181  0738 35                	pshy	
3182  0739 f60003            	ldab	L124_Response
3183  073c 87                	clra	
3184  073d 4a000000          	call	f_diagF_DiagResponse
3186  0741 1b86              	leas	6,s
3187                         ; 1222 }
3190  0743 0a                	rtc	
3237                         ; 1228 void DESC_API_CALLBACK_TYPE ApplDescWrite_222810_Heated_Seat_NTC_Cutoff_Parameters_Rear_Seats(DescMsgContext* pMsgContext)
3237                         ; 1229 {
3238                         	switch	.ftext
3239  0744                   f_ApplDescWrite_222810_Heated_Seat_NTC_Cutoff_Parameters_Rear_Seats:
3241  0744 3b                	pshd	
3242       00000000          OFST:	set	0
3245                         ; 1231 	DataLength = 1;
3247  0745 cc0001            	ldd	#1
3248  0748 7c0001            	std	L324_DataLength
3249                         ; 1233 	if ((CSWM_config_c & CSWM_4HS_VARIANT_MASK_K) == CSWM_4HS_VARIANT_MASK_K)
3251  074b 1e00000f02        	brset	_CSWM_config_c,15,LC018
3252  0750 2042              	bra	L7451
3253  0752                   LC018:
3254                         ; 1235 		if (diag_sec_access5_state_c == ECU_UNLOCKED)
3256  0752 f60000            	ldab	_diag_sec_access5_state_c
3257  0755 c105              	cmpb	#5
3258  0757 2630              	bne	L1551
3259                         ; 1237 			if (hsF_CheckDiagnoseParameter( HS_NTC_CUTOFF_REAR,&pMsgContext->reqData[0] ))
3261  0759 ecf30000          	ldd	[OFST+0,s]
3262  075d 3b                	pshd	
3263  075e cc0014            	ldd	#20
3264  0761 4a000000          	call	f_hsF_CheckDiagnoseParameter
3266  0765 1b82              	leas	2,s
3267  0767 044114            	tbeq	b,L3551
3268                         ; 1240 				diag_writedata_b = 1;
3270  076a 1c000002          	bset	_diag_flags_c,2
3271                         ; 1241 				DataLength = 0;
3273  076e 18790001          	clrw	L324_DataLength
3274                         ; 1242 				Response = RESPONSE_OK;
3276  0772 c601              	ldab	#1
3277  0774 7b0003            	stab	L124_Response
3278                         ; 1243 				diag_writedata_mode_c = WRITE_HS_NTC_CUTOFF_REAR;
3280  0777 c61a              	ldab	#26
3281  0779 7b0000            	stab	_diag_writedata_mode_c
3283  077c 2022              	bra	L1651
3284  077e                   L3551:
3285                         ; 1248 				diag_writedata_b = 0;
3287  077e 1d000002          	bclr	_diag_flags_c,2
3288                         ; 1249 				diag_writedata_mode_c = 0;
3290  0782 790000            	clr	_diag_writedata_mode_c
3291                         ; 1252 				Response = REQUEST_OUT_OF_RANGE;
3293  0785 c604              	ldab	#4
3294  0787 2014              	bra	LC019
3295  0789                   L1551:
3296                         ; 1258 			diag_writedata_b = 0;
3298  0789 1d000002          	bclr	_diag_flags_c,2
3299                         ; 1259 			diag_writedata_mode_c = 0;
3301  078d 790000            	clr	_diag_writedata_mode_c
3302                         ; 1262 			Response = SECURITY_ACCESS_DENIED;
3304  0790 c608              	ldab	#8
3305  0792 2009              	bra	LC019
3306  0794                   L7451:
3307                         ; 1268 		diag_writedata_b = 0;
3309  0794 1d000002          	bclr	_diag_flags_c,2
3310                         ; 1269 		diag_writedata_mode_c = 0;
3312  0798 790000            	clr	_diag_writedata_mode_c
3313                         ; 1272 		Response = SUBFUNCTION_NOT_SUPPORTED;
3315  079b c607              	ldab	#7
3316  079d                   LC019:
3317  079d 7b0003            	stab	L124_Response
3318  07a0                   L1651:
3319                         ; 1274 	diagF_DiagResponse(Response, DataLength+SIZE_OF_BLOCKNUMBER_K, pMsgContext);
3321  07a0 ec80              	ldd	OFST+0,s
3322  07a2 3b                	pshd	
3323  07a3 fd0001            	ldy	L324_DataLength
3324  07a6 1942              	leay	2,y
3325  07a8 35                	pshy	
3326  07a9 f60003            	ldab	L124_Response
3327  07ac 87                	clra	
3328  07ad 4a000000          	call	f_diagF_DiagResponse
3330  07b1 1b86              	leas	6,s
3331                         ; 1276 }
3334  07b3 0a                	rtc	
3387                         ; 1283 void DESC_API_CALLBACK_TYPE ApplDescWrite_223000_Program_ECU_Data_at_Flash_Station_1(DescMsgContext* pMsgContext)
3387                         ; 1284 {
3388                         	switch	.ftext
3389  07b4                   f_ApplDescWrite_223000_Program_ECU_Data_at_Flash_Station_1:
3391  07b4 3b                	pshd	
3392  07b5 37                	pshb	
3393       00000001          OFST:	set	1
3396                         ; 1286 	for ( i = 0; i < 20; i++ )
3398  07b6 c7                	clrb	
3399  07b7 6b80              	stab	OFST-1,s
3400  07b9 cd0005            	ldy	#_diag_temp_buffer_c
3401  07bc                   L5061:
3402                         ; 1289 		diag_temp_buffer_c[ i ] = pMsgContext->reqData[ i ];
3404  07bc 87                	clra	
3405  07bd eef30001          	ldx	[OFST+0,s]
3406  07c1 180ae6ee          	movb	d,x,d,y
3407                         ; 1286 	for ( i = 0; i < 20; i++ )
3409  07c5 6280              	inc	OFST-1,s
3412  07c7 e680              	ldab	OFST-1,s
3413  07c9 c114              	cmpb	#20
3414  07cb 25ef              	blo	L5061
3415                         ; 1292 	if(diag_eol_io_lock_b)
3417  07cd 1f00000111        	brclr	_diag_io_lock3_flags_c,1,L3161
3418                         ; 1295 		diag_writedata_b = 1;
3420  07d2 1c000002          	bset	_diag_flags_c,2
3421                         ; 1297 		diag_writedata_mode_c = WRITE_TEMIC_BOM_NUMBER;
3423  07d6 c615              	ldab	#21
3424  07d8 7b0000            	stab	_diag_writedata_mode_c
3425                         ; 1298 		DataLength = 0;
3427  07db 18790001          	clrw	L324_DataLength
3428                         ; 1299 		Response = RESPONSE_OK;
3430  07df c601              	ldab	#1
3432  07e1 2008              	bra	L5161
3433  07e3                   L3161:
3434                         ; 1303 		DataLength = 1;
3436  07e3 cc0001            	ldd	#1
3437  07e6 7c0001            	std	L324_DataLength
3438                         ; 1304 		Response = CONDITIONS_NOT_CORRECT;
3440  07e9 c606              	ldab	#6
3441  07eb                   L5161:
3442  07eb 7b0003            	stab	L124_Response
3443                         ; 1306 	diagF_DiagResponse(Response, DataLength+SIZE_OF_BLOCKNUMBER_K, pMsgContext);
3445  07ee ec81              	ldd	OFST+0,s
3446  07f0 3b                	pshd	
3447  07f1 fd0001            	ldy	L324_DataLength
3448  07f4 1942              	leay	2,y
3449  07f6 35                	pshy	
3450  07f7 f60003            	ldab	L124_Response
3451  07fa 87                	clra	
3452  07fb 4a000000          	call	f_diagF_DiagResponse
3454                         ; 1308 }
3457  07ff 1b87              	leas	7,s
3458  0801 0a                	rtc	
3511                         ; 1315 void DESC_API_CALLBACK_TYPE ApplDescWrite_223001_Program_ECU_Data_at_Flash_Station_2(DescMsgContext* pMsgContext)
3511                         ; 1316 {
3512                         	switch	.ftext
3513  0802                   f_ApplDescWrite_223001_Program_ECU_Data_at_Flash_Station_2:
3515  0802 3b                	pshd	
3516  0803 37                	pshb	
3517       00000001          OFST:	set	1
3520                         ; 1318 	for ( i = 0; i < 20; i++ )
3522  0804 c7                	clrb	
3523  0805 6b80              	stab	OFST-1,s
3524  0807 cd0005            	ldy	#_diag_temp_buffer_c
3525  080a                   L1461:
3526                         ; 1321 		diag_temp_buffer_c[ i ] = pMsgContext->reqData[ i ];
3528  080a 87                	clra	
3529  080b eef30001          	ldx	[OFST+0,s]
3530  080f 180ae6ee          	movb	d,x,d,y
3531                         ; 1318 	for ( i = 0; i < 20; i++ )
3533  0813 6280              	inc	OFST-1,s
3536  0815 e680              	ldab	OFST-1,s
3537  0817 c114              	cmpb	#20
3538  0819 25ef              	blo	L1461
3539                         ; 1324 	if(diag_eol_io_lock_b)
3541  081b 1f00000111        	brclr	_diag_io_lock3_flags_c,1,L7461
3542                         ; 1327 		diag_writedata_b = 1;
3544  0820 1c000002          	bset	_diag_flags_c,2
3545                         ; 1329 		diag_writedata_mode_c = WRITE_CTRL_BYTE_FINAL;
3547  0824 c614              	ldab	#20
3548  0826 7b0000            	stab	_diag_writedata_mode_c
3549                         ; 1330 		DataLength = 0;
3551  0829 18790001          	clrw	L324_DataLength
3552                         ; 1331 		Response = RESPONSE_OK;
3554  082d c601              	ldab	#1
3556  082f 2008              	bra	L1561
3557  0831                   L7461:
3558                         ; 1335 		DataLength = 1;
3560  0831 cc0001            	ldd	#1
3561  0834 7c0001            	std	L324_DataLength
3562                         ; 1336 		Response = CONDITIONS_NOT_CORRECT;
3564  0837 c606              	ldab	#6
3565  0839                   L1561:
3566  0839 7b0003            	stab	L124_Response
3567                         ; 1338 	diagF_DiagResponse(Response, DataLength+SIZE_OF_BLOCKNUMBER_K, pMsgContext);
3569  083c ec81              	ldd	OFST+0,s
3570  083e 3b                	pshd	
3571  083f fd0001            	ldy	L324_DataLength
3572  0842 1942              	leay	2,y
3573  0844 35                	pshy	
3574  0845 f60003            	ldab	L124_Response
3575  0848 87                	clra	
3576  0849 4a000000          	call	f_diagF_DiagResponse
3578                         ; 1340 }
3581  084d 1b87              	leas	7,s
3582  084f 0a                	rtc	
3633                         ; 1345 void DESC_API_CALLBACK_TYPE ApplDescWriteEcuSerialNumber(DescMsgContext* pMsgContext)
3633                         ; 1346 {
3634                         	switch	.ftext
3635  0850                   f_ApplDescWriteEcuSerialNumber:
3637  0850 3b                	pshd	
3638  0851 37                	pshb	
3639       00000001          OFST:	set	1
3642                         ; 1348 	for ( i = 0; i < 20; i++ )
3644  0852 c7                	clrb	
3645  0853 6b80              	stab	OFST-1,s
3646  0855 cd0005            	ldy	#_diag_temp_buffer_c
3647  0858                   L5761:
3648                         ; 1351 		diag_temp_buffer_c[ i ] = pMsgContext->reqData[ i ];
3650  0858 87                	clra	
3651  0859 eef30001          	ldx	[OFST+0,s]
3652  085d 180ae6ee          	movb	d,x,d,y
3653                         ; 1348 	for ( i = 0; i < 20; i++ )
3655  0861 6280              	inc	OFST-1,s
3658  0863 e680              	ldab	OFST-1,s
3659  0865 c114              	cmpb	#20
3660  0867 25ef              	blo	L5761
3661                         ; 1354 	if(diag_eol_io_lock_b)
3663  0869 1f00000111        	brclr	_diag_io_lock3_flags_c,1,L3071
3664                         ; 1357 		diag_writedata_b = TRUE;
3666  086e 1c000002          	bset	_diag_flags_c,2
3667                         ; 1359 		diag_writedata_mode_c = WRITE_ECU_SERIAL_NUMBER;
3669  0872 c612              	ldab	#18
3670  0874 7b0000            	stab	_diag_writedata_mode_c
3671                         ; 1360 		DataLength = 0;
3673  0877 18790001          	clrw	L324_DataLength
3674                         ; 1361 		Response = RESPONSE_OK;
3676  087b c601              	ldab	#1
3678  087d 2008              	bra	L5071
3679  087f                   L3071:
3680                         ; 1365 		DataLength = 1;
3682  087f cc0001            	ldd	#1
3683  0882 7c0001            	std	L324_DataLength
3684                         ; 1366 		Response = CONDITIONS_NOT_CORRECT;
3686  0885 c606              	ldab	#6
3687  0887                   L5071:
3688  0887 7b0003            	stab	L124_Response
3689                         ; 1368 	diagF_DiagResponse(Response, DataLength+SIZE_OF_BLOCKNUMBER_K, pMsgContext);
3691  088a ec81              	ldd	OFST+0,s
3692  088c 3b                	pshd	
3693  088d fd0001            	ldy	L324_DataLength
3694  0890 1942              	leay	2,y
3695  0892 35                	pshy	
3696  0893 f60003            	ldab	L124_Response
3697  0896 87                	clra	
3698  0897 4a000000          	call	f_diagF_DiagResponse
3700                         ; 1370 }
3703  089b 1b87              	leas	7,s
3704  089d 0a                	rtc	
3742                         ; 1378 void DESC_API_CALLBACK_TYPE ApplDescWriteVIN(DescMsgContext* pMsgContext)
3742                         ; 1379 {
3743                         	switch	.ftext
3744  089e                   f_ApplDescWriteVIN:
3748                         ; 1391     DescSetNegResponse(kDescNrcRequestOutOfRange);
3750  089e cc0031            	ldd	#49
3751  08a1 4a000000          	call	f_DescSetNegResponse
3753                         ; 1394   DescProcessingDone();
3755  08a5 4a000000          	call	f_DescProcessingDone
3757                         ; 1395 }
3760  08a9 0a                	rtc	
4217                         ; 1404 void DESC_API_CALLBACK_TYPE ApplDescWrite_2E2023_System_Configuration_PROXI(DescMsgContext* pMsgContext)
4217                         ; 1405 {
4218                         	switch	.ftext
4219  08aa                   f_ApplDescWrite_2E2023_System_Configuration_PROXI:
4221  08aa 3b                	pshd	
4222  08ab 1bf1e1            	leas	-31,s
4223       0000001f          OFST:	set	31
4226                         ; 1406 	DescNegResCode  lu_NRC = kDescNrcNone; 
4228  08ae 698c              	clr	OFST-19,s
4229                         ; 1409 	u_8Bit lub_index = 0u;
4231                         ; 1416 	if(re_PROXI_state != E_PROXI_LRN_START)
4233  08b0 f60041            	ldab	_re_PROXI_state
4234  08b3 2709              	beq	L5222
4235                         ; 1435 			lu_NRC = kDescNrcGeneralProgrammingFailure;
4237  08b5 c672              	ldab	#114
4238  08b7 6b8c              	stab	OFST-19,s
4239                         ; 1436 			re_PROXI_state = E_PROXI_NEG_RSP;
4241  08b9 c604              	ldab	#4
4242  08bb 7b0041            	stab	_re_PROXI_state
4243  08be                   L5222:
4244                         ; 1444 		lub_PROXIautoTrigger = FALSE;
4246  08be 87                	clra	
4247  08bf 6a8e              	staa	OFST-17,s
4248                         ; 1448 		lu_PROXI_Data_Temp.s.PROXI_IDVeh.s.Digit11 = didwrite_rfhub_ReturnPROXISignal(&pMsgContext->reqData[0], PROXI_DIGIT_11_BYTE, PROXI_DIGIT_MASK, PROXI_DIGIT_OFFSET);
4250  08c1 c7                	clrb	
4251  08c2 3b                	pshd	
4252  08c3 c60f              	ldab	#15
4253  08c5 3b                	pshd	
4254  08c6 c7                	clrb	
4255  08c7 3b                	pshd	
4256  08c8 ecf30025          	ldd	[OFST+6,s]
4257  08cc 4a0d8b8b          	call	f_didwrite_rfhub_ReturnPROXISignal
4259  08d0 1b86              	leas	6,s
4260  08d2 58                	lslb	
4261  08d3 58                	lslb	
4262  08d4 58                	lslb	
4263  08d5 58                	lslb	
4264  08d6 0d80f0            	bclr	OFST-31,s,240
4265  08d9 c4f0              	andb	#240
4266  08db ea80              	orab	OFST-31,s
4267  08dd 6b80              	stab	OFST-31,s
4268                         ; 1449 		lu_PROXI_Data_Temp.s.PROXI_IDVeh.s.Digit10 = didwrite_rfhub_ReturnPROXISignal(&pMsgContext->reqData[0], PROXI_DIGIT_10_BYTE, PROXI_DIGIT_MASK, PROXI_DIGIT_OFFSET);
4270  08df 87                	clra	
4271  08e0 c7                	clrb	
4272  08e1 3b                	pshd	
4273  08e2 c60f              	ldab	#15
4274  08e4 3b                	pshd	
4275  08e5 c601              	ldab	#1
4276  08e7 3b                	pshd	
4277  08e8 ecf30025          	ldd	[OFST+6,s]
4278  08ec 4a0d8b8b          	call	f_didwrite_rfhub_ReturnPROXISignal
4280  08f0 1b86              	leas	6,s
4281  08f2 0d800f            	bclr	OFST-31,s,15
4282  08f5 c40f              	andb	#15
4283  08f7 ea80              	orab	OFST-31,s
4284  08f9 6b80              	stab	OFST-31,s
4285                         ; 1450 		lu_PROXI_Data_Temp.s.PROXI_IDVeh.s.Digit9 = didwrite_rfhub_ReturnPROXISignal(&pMsgContext->reqData[0], PROXI_DIGIT_9_BYTE, PROXI_DIGIT_MASK, PROXI_DIGIT_OFFSET);
4287  08fb 87                	clra	
4288  08fc c7                	clrb	
4289  08fd 3b                	pshd	
4290  08fe c60f              	ldab	#15
4291  0900 3b                	pshd	
4292  0901 c602              	ldab	#2
4293  0903 3b                	pshd	
4294  0904 ecf30025          	ldd	[OFST+6,s]
4295  0908 4a0d8b8b          	call	f_didwrite_rfhub_ReturnPROXISignal
4297  090c 1b86              	leas	6,s
4298  090e 58                	lslb	
4299  090f 58                	lslb	
4300  0910 58                	lslb	
4301  0911 58                	lslb	
4302  0912 0d81f0            	bclr	OFST-30,s,240
4303  0915 c4f0              	andb	#240
4304  0917 ea81              	orab	OFST-30,s
4305  0919 6b81              	stab	OFST-30,s
4306                         ; 1451 		lu_PROXI_Data_Temp.s.PROXI_IDVeh.s.Digit8 = didwrite_rfhub_ReturnPROXISignal(&pMsgContext->reqData[0], PROXI_DIGIT_8_BYTE, PROXI_DIGIT_MASK, PROXI_DIGIT_OFFSET);
4308  091b 87                	clra	
4309  091c c7                	clrb	
4310  091d 3b                	pshd	
4311  091e c60f              	ldab	#15
4312  0920 3b                	pshd	
4313  0921 c603              	ldab	#3
4314  0923 3b                	pshd	
4315  0924 ecf30025          	ldd	[OFST+6,s]
4316  0928 4a0d8b8b          	call	f_didwrite_rfhub_ReturnPROXISignal
4318  092c 1b86              	leas	6,s
4319  092e 0d810f            	bclr	OFST-30,s,15
4320  0931 c40f              	andb	#15
4321  0933 ea81              	orab	OFST-30,s
4322  0935 6b81              	stab	OFST-30,s
4323                         ; 1452 		lu_PROXI_Data_Temp.s.PROXI_IDVeh.s.Digit7 = didwrite_rfhub_ReturnPROXISignal(&pMsgContext->reqData[0], PROXI_DIGIT_7_BYTE, PROXI_DIGIT_MASK, PROXI_DIGIT_OFFSET);
4325  0937 87                	clra	
4326  0938 c7                	clrb	
4327  0939 3b                	pshd	
4328  093a c60f              	ldab	#15
4329  093c 3b                	pshd	
4330  093d c604              	ldab	#4
4331  093f 3b                	pshd	
4332  0940 ecf30025          	ldd	[OFST+6,s]
4333  0944 4a0d8b8b          	call	f_didwrite_rfhub_ReturnPROXISignal
4335  0948 1b86              	leas	6,s
4336  094a 58                	lslb	
4337  094b 58                	lslb	
4338  094c 58                	lslb	
4339  094d 58                	lslb	
4340  094e 0d82f0            	bclr	OFST-29,s,240
4341  0951 c4f0              	andb	#240
4342  0953 ea82              	orab	OFST-29,s
4343  0955 6b82              	stab	OFST-29,s
4344                         ; 1453 		lu_PROXI_Data_Temp.s.PROXI_IDVeh.s.Digit6 = didwrite_rfhub_ReturnPROXISignal(&pMsgContext->reqData[0], PROXI_DIGIT_6_BYTE, PROXI_DIGIT_MASK, PROXI_DIGIT_OFFSET);
4346  0957 87                	clra	
4347  0958 c7                	clrb	
4348  0959 3b                	pshd	
4349  095a c60f              	ldab	#15
4350  095c 3b                	pshd	
4351  095d c605              	ldab	#5
4352  095f 3b                	pshd	
4353  0960 ecf30025          	ldd	[OFST+6,s]
4354  0964 4a0d8b8b          	call	f_didwrite_rfhub_ReturnPROXISignal
4356  0968 1b86              	leas	6,s
4357  096a 0d820f            	bclr	OFST-29,s,15
4358  096d c40f              	andb	#15
4359  096f ea82              	orab	OFST-29,s
4360  0971 6b82              	stab	OFST-29,s
4361                         ; 1456 		lu_PROXI_Data_Temp.s.PROXI_CRC.s.Digit5 = didwrite_rfhub_ReturnPROXISignal(&pMsgContext->reqData[0], PROXI_DIGIT_5_BYTE, PROXI_DIGIT_MASK, PROXI_DIGIT_OFFSET);
4363  0973 87                	clra	
4364  0974 c7                	clrb	
4365  0975 3b                	pshd	
4366  0976 c60f              	ldab	#15
4367  0978 3b                	pshd	
4368  0979 c606              	ldab	#6
4369  097b 3b                	pshd	
4370  097c ecf30025          	ldd	[OFST+6,s]
4371  0980 4a0d8b8b          	call	f_didwrite_rfhub_ReturnPROXISignal
4373  0984 1b86              	leas	6,s
4374  0986 58                	lslb	
4375  0987 58                	lslb	
4376  0988 58                	lslb	
4377  0989 58                	lslb	
4378  098a 0d83f0            	bclr	OFST-28,s,240
4379  098d c4f0              	andb	#240
4380  098f ea83              	orab	OFST-28,s
4381  0991 6b83              	stab	OFST-28,s
4382                         ; 1457 		lu_PROXI_Data_Temp.s.PROXI_CRC.s.Digit4 = didwrite_rfhub_ReturnPROXISignal(&pMsgContext->reqData[0], PROXI_DIGIT_4_BYTE, PROXI_DIGIT_MASK, PROXI_DIGIT_OFFSET);
4384  0993 87                	clra	
4385  0994 c7                	clrb	
4386  0995 3b                	pshd	
4387  0996 c60f              	ldab	#15
4388  0998 3b                	pshd	
4389  0999 c607              	ldab	#7
4390  099b 3b                	pshd	
4391  099c ecf30025          	ldd	[OFST+6,s]
4392  09a0 4a0d8b8b          	call	f_didwrite_rfhub_ReturnPROXISignal
4394  09a4 1b86              	leas	6,s
4395  09a6 0d830f            	bclr	OFST-28,s,15
4396  09a9 c40f              	andb	#15
4397  09ab ea83              	orab	OFST-28,s
4398  09ad 6b83              	stab	OFST-28,s
4399                         ; 1458 		lu_PROXI_Data_Temp.s.PROXI_CRC.s.Digit3 = didwrite_rfhub_ReturnPROXISignal(&pMsgContext->reqData[0], PROXI_DIGIT_3_BYTE, PROXI_DIGIT_MASK, PROXI_DIGIT_OFFSET);
4401  09af 87                	clra	
4402  09b0 c7                	clrb	
4403  09b1 3b                	pshd	
4404  09b2 c60f              	ldab	#15
4405  09b4 3b                	pshd	
4406  09b5 c608              	ldab	#8
4407  09b7 3b                	pshd	
4408  09b8 ecf30025          	ldd	[OFST+6,s]
4409  09bc 4a0d8b8b          	call	f_didwrite_rfhub_ReturnPROXISignal
4411  09c0 1b86              	leas	6,s
4412  09c2 58                	lslb	
4413  09c3 58                	lslb	
4414  09c4 58                	lslb	
4415  09c5 58                	lslb	
4416  09c6 0d84f0            	bclr	OFST-27,s,240
4417  09c9 c4f0              	andb	#240
4418  09cb ea84              	orab	OFST-27,s
4419  09cd 6b84              	stab	OFST-27,s
4420                         ; 1459 		lu_PROXI_Data_Temp.s.PROXI_CRC.s.Digit2 = didwrite_rfhub_ReturnPROXISignal(&pMsgContext->reqData[0], PROXI_DIGIT_2_BYTE, PROXI_DIGIT_MASK, PROXI_DIGIT_OFFSET);
4422  09cf 87                	clra	
4423  09d0 c7                	clrb	
4424  09d1 3b                	pshd	
4425  09d2 c60f              	ldab	#15
4426  09d4 3b                	pshd	
4427  09d5 c609              	ldab	#9
4428  09d7 3b                	pshd	
4429  09d8 ecf30025          	ldd	[OFST+6,s]
4430  09dc 4a0d8b8b          	call	f_didwrite_rfhub_ReturnPROXISignal
4432  09e0 1b86              	leas	6,s
4433  09e2 0d840f            	bclr	OFST-27,s,15
4434  09e5 c40f              	andb	#15
4435  09e7 ea84              	orab	OFST-27,s
4436  09e9 6b84              	stab	OFST-27,s
4437                         ; 1460 		lu_PROXI_Data_Temp.s.PROXI_CRC.s.Digit1 = didwrite_rfhub_ReturnPROXISignal(&pMsgContext->reqData[0], PROXI_DIGIT_1_BYTE, PROXI_DIGIT_MASK, PROXI_DIGIT_OFFSET);
4439  09eb 87                	clra	
4440  09ec c7                	clrb	
4441  09ed 3b                	pshd	
4442  09ee c60f              	ldab	#15
4443  09f0 3b                	pshd	
4444  09f1 c60a              	ldab	#10
4445  09f3 3b                	pshd	
4446  09f4 ecf30025          	ldd	[OFST+6,s]
4447  09f8 4a0d8b8b          	call	f_didwrite_rfhub_ReturnPROXISignal
4449  09fc 1b86              	leas	6,s
4450  09fe 58                	lslb	
4451  09ff 58                	lslb	
4452  0a00 58                	lslb	
4453  0a01 58                	lslb	
4454  0a02 0d85f0            	bclr	OFST-26,s,240
4455  0a05 c4f0              	andb	#240
4456  0a07 ea85              	orab	OFST-26,s
4457  0a09 c4f0              	andb	#240
4458  0a0b 6b85              	stab	OFST-26,s
4459                         ; 1461 		lu_PROXI_Data_Temp.s.PROXI_CRC.s.unused0 = NOT_USED_VAL; /* Last byte do not care - always zero */
4461                         ; 1464 		lu_PROXI_Data_Temp.s.Driver_Side = didwrite_rfhub_ReturnPROXISignal(&pMsgContext->reqData[0], PROXI_DS_BYTE, PROXI_DS_MASK, PROXI_DS_OFFSET);
4463  0a0d cc0003            	ldd	#3
4464  0a10 3b                	pshd	
4465  0a11 c608              	ldab	#8
4466  0a13 3b                	pshd	
4467  0a14 c639              	ldab	#57
4468  0a16 3b                	pshd	
4469  0a17 ecf30025          	ldd	[OFST+6,s]
4470  0a1b 4a0d8b8b          	call	f_didwrite_rfhub_ReturnPROXISignal
4472  0a1f 1b86              	leas	6,s
4473  0a21 c501              	bitb	#1
4474  0a23 2705              	beq	L26
4475  0a25 0c8901            	bset	OFST-22,s,1
4476  0a28 2003              	bra	L46
4477  0a2a                   L26:
4478  0a2a 0d8901            	bclr	OFST-22,s,1
4479  0a2d                   L46:
4480                         ; 1465 		lu_PROXI_Data_Temp.s.Heated_Seats_Variant = didwrite_rfhub_ReturnPROXISignal(&pMsgContext->reqData[0], PROXI_HEATSEAT_BYTE, PROXI_HEATSEAT_MASK, PROXI_HEATSEAT_OFFSET);
4482  0a2d 87                	clra	
4483  0a2e c7                	clrb	
4484  0a2f 3b                	pshd	
4485  0a30 c603              	ldab	#3
4486  0a32 3b                	pshd	
4487  0a33 c63d              	ldab	#61
4488  0a35 3b                	pshd	
4489  0a36 ecf30025          	ldd	[OFST+6,s]
4490  0a3a 4a0d8b8b          	call	f_didwrite_rfhub_ReturnPROXISignal
4492  0a3e 1b86              	leas	6,s
4493  0a40 58                	lslb	
4494  0a41 0d8906            	bclr	OFST-22,s,6
4495  0a44 c406              	andb	#6
4496  0a46 ea89              	orab	OFST-22,s
4497  0a48 6b89              	stab	OFST-22,s
4498                         ; 1466 		lu_PROXI_Data_Temp.s.Remote_Start = didwrite_rfhub_ReturnPROXISignal(&pMsgContext->reqData[0], PROXI_RS_BYTE, PROXI_RS_MASK, PROXI_RS_OFFSET);
4500  0a4a 87                	clra	
4501  0a4b c7                	clrb	
4502  0a4c 3b                	pshd	
4503  0a4d 52                	incb	
4504  0a4e 3b                	pshd	
4505  0a4f c66b              	ldab	#107
4506  0a51 3b                	pshd	
4507  0a52 ecf30025          	ldd	[OFST+6,s]
4508  0a56 4a0d8b8b          	call	f_didwrite_rfhub_ReturnPROXISignal
4510  0a5a 1b86              	leas	6,s
4511  0a5c c501              	bitb	#1
4512  0a5e 2705              	beq	L66
4513  0a60 0c8908            	bset	OFST-22,s,8
4514  0a63 2003              	bra	L07
4515  0a65                   L66:
4516  0a65 0d8908            	bclr	OFST-22,s,8
4517  0a68                   L07:
4518                         ; 1467 		lu_PROXI_Data_Temp.s.Seat_Material = didwrite_rfhub_ReturnPROXISignal(&pMsgContext->reqData[0], PROXI_SEAT_TYPE_BYTE, PROXI_SEAT_TYPE_MASK, PROXI_SEAT_TYPE_OFFSET);
4520  0a68 cc0002            	ldd	#2
4521  0a6b 3b                	pshd	
4522  0a6c c60c              	ldab	#12
4523  0a6e 3b                	pshd	
4524  0a6f c67b              	ldab	#123
4525  0a71 3b                	pshd	
4526  0a72 ecf30025          	ldd	[OFST+6,s]
4527  0a76 4a0d8b8b          	call	f_didwrite_rfhub_ReturnPROXISignal
4529  0a7a 1b86              	leas	6,s
4530  0a7c 58                	lslb	
4531  0a7d 58                	lslb	
4532  0a7e 58                	lslb	
4533  0a7f 58                	lslb	
4534  0a80 0d8930            	bclr	OFST-22,s,48
4535  0a83 c430              	andb	#48
4536  0a85 ea89              	orab	OFST-22,s
4537  0a87 6b89              	stab	OFST-22,s
4538                         ; 1468 		lu_PROXI_Data_Temp.s.Wheel_Material = didwrite_rfhub_ReturnPROXISignal(&pMsgContext->reqData[0], PROXI_WHEEL_TYPE_BYTE, PROXI_WHEEL_TYPE_MASK, PROXI_WHEEL_TYPE_OFFSET);
4540  0a89 cc0005            	ldd	#5
4541  0a8c 3b                	pshd	
4542  0a8d c660              	ldab	#96
4543  0a8f 3b                	pshd	
4544  0a90 c67b              	ldab	#123
4545  0a92 3b                	pshd	
4546  0a93 ecf30025          	ldd	[OFST+6,s]
4547  0a97 4a0d8b8b          	call	f_didwrite_rfhub_ReturnPROXISignal
4549  0a9b 1b86              	leas	6,s
4550  0a9d 56                	rorb	
4551  0a9e 56                	rorb	
4552  0a9f 56                	rorb	
4553  0aa0 0d89c0            	bclr	OFST-22,s,192
4554  0aa3 c4c0              	andb	#192
4555  0aa5 ea89              	orab	OFST-22,s
4556  0aa7 6b89              	stab	OFST-22,s
4557                         ; 1469 		lu_PROXI_Data_Temp.s.Steering_Wheel = didwrite_rfhub_ReturnPROXISignal(&pMsgContext->reqData[0], PROXI_WHEEL_BYTE, PROXI_WHEEL_MASK, PROXI_WHEEL_OFFSET);
4559  0aa9 cc0004            	ldd	#4
4560  0aac 3b                	pshd	
4561  0aad c610              	ldab	#16
4562  0aaf 3b                	pshd	
4563  0ab0 c67b              	ldab	#123
4564  0ab2 3b                	pshd	
4565  0ab3 ecf30025          	ldd	[OFST+6,s]
4566  0ab7 4a0d8b8b          	call	f_didwrite_rfhub_ReturnPROXISignal
4568  0abb 1b86              	leas	6,s
4569  0abd c501              	bitb	#1
4570  0abf 2705              	beq	L27
4571  0ac1 0c8a01            	bset	OFST-21,s,1
4572  0ac4 2003              	bra	L47
4573  0ac6                   L27:
4574  0ac6 0d8a01            	bclr	OFST-21,s,1
4575  0ac9                   L47:
4576                         ; 1470 		lu_PROXI_Data_Temp.s.Country_Code = didwrite_rfhub_ReturnPROXISignal(&pMsgContext->reqData[0], PROXI_COUNTRY_BYTE, PROXI_COUNTRY_MASK, PROXI_COUNTRY_OFFSET);
4578  0ac9 87                	clra	
4579  0aca c7                	clrb	
4580  0acb 3b                	pshd	
4581  0acc c6ff              	ldab	#255
4582  0ace 3b                	pshd	
4583  0acf c66a              	ldab	#106
4584  0ad1 3b                	pshd	
4585  0ad2 ecf30025          	ldd	[OFST+6,s]
4586  0ad6 4a0d8b8b          	call	f_didwrite_rfhub_ReturnPROXISignal
4588  0ada 1b86              	leas	6,s
4589  0adc 6b87              	stab	OFST-24,s
4590                         ; 1471 		lu_PROXI_Data_Temp.s.Vehicle_Line_Configuration = didwrite_rfhub_ReturnPROXISignal(&pMsgContext->reqData[0], PROXI_VL_BYTE, PROXI_VL_MASK, PROXI_VL_OFFSET);
4592  0ade 87                	clra	
4593  0adf c7                	clrb	
4594  0ae0 3b                	pshd	
4595  0ae1 c6ff              	ldab	#255
4596  0ae3 3b                	pshd	
4597  0ae4 c668              	ldab	#104
4598  0ae6 3b                	pshd	
4599  0ae7 ecf30025          	ldd	[OFST+6,s]
4600  0aeb 4a0d8b8b          	call	f_didwrite_rfhub_ReturnPROXISignal
4602  0aef 1b86              	leas	6,s
4603  0af1 6b86              	stab	OFST-25,s
4604                         ; 1472 		lu_PROXI_Data_Temp.s.Model_Year = didwrite_rfhub_ReturnPROXISignal(&pMsgContext->reqData[0], PROXI_MY_BYTE, PROXI_MY_MASK, PROXI_MY_OFFSET);
4606  0af3 87                	clra	
4607  0af4 c7                	clrb	
4608  0af5 3b                	pshd	
4609  0af6 c6ff              	ldab	#255
4610  0af8 3b                	pshd	
4611  0af9 c67c              	ldab	#124
4612  0afb 3b                	pshd	
4613  0afc ecf30025          	ldd	[OFST+6,s]
4614  0b00 4a0d8b8b          	call	f_didwrite_rfhub_ReturnPROXISignal
4616  0b04 1b86              	leas	6,s
4617  0b06 6b88              	stab	OFST-23,s
4618                         ; 1473 		lu_PROXI_Data_Temp.s.unused0 = NOT_USED_VAL; /* Unused bits */
4620  0b08 0d8afe            	bclr	OFST-21,s,254
4621                         ; 1475 		switch( re_PROXI_state )
4623  0b0b f60041            	ldab	_re_PROXI_state
4625  0b0e 2718              	beq	L7271
4626  0b10 53                	decb	
4627  0b11 1827019e          	beq	L1371
4628  0b15 53                	decb	
4629  0b16 182701c3          	beq	L3371
4630  0b1a 53                	decb	
4631  0b1b 182701db          	beq	L5371
4632  0b1f 53                	decb	
4633  0b20 182701dc          	beq	L7371
4634                         ; 1731 			default:
4634                         ; 1732 				/* Must not enter this state */
4634                         ; 1733 				re_PROXI_state = E_PROXI_NEG_RSP;
4636                         ; 1734 				lub_PROXIautoTrigger = TRUE;
4638                         ; 1736 				break;
4640  0b24 182001c7          	bra	LC022
4641  0b28                   L7271:
4642                         ; 1477 			case E_PROXI_LRN_START:
4642                         ; 1478 				/* Clear error flag */
4642                         ; 1479 				lub_errCnt = 0;
4644  0b28 87                	clra	
4645  0b29 6a8b              	staa	OFST-20,s
4646                         ; 1485 				memset((u_8Bit *)ras_Rejection_Feedback_Tbl, REJECTION_FEEDBACK_DEFAULT, PROXI_REJ_FEEDBACK_TBL_SIZE);
4648  0b2b cd0038            	ldy	#_ras_Rejection_Feedback_Tbl
4649  0b2e ce0009            	ldx	#9
4650  0b31                   L67:
4651  0b31 6b70              	stab	1,y+
4652  0b33 0435fb            	dbne	x,L67
4653                         ; 1486 				EE_BlockWrite(EE_PROXI_FAILURE, (UINT8*)&ras_Rejection_Feedback_Tbl[0]);
4655  0b36 cc0038            	ldd	#_ras_Rejection_Feedback_Tbl
4656  0b39 3b                	pshd	
4657  0b3a cc001e            	ldd	#30
4658  0b3d 4a000000          	call	f_EE_BlockWrite
4660  0b41 1b82              	leas	2,s
4661                         ; 1490 				for( lub_index = 0; lub_index < CFG_CODE_LEN; lub_index++ )
4663  0b43 c7                	clrb	
4664  0b44 6b8d              	stab	OFST-18,s
4665  0b46                   L7322:
4666                         ; 1492 					if( ((pMsgContext->reqData[lub_index]) >= LOW_CFG_CODE_LIMIT ) &&
4666                         ; 1493 						((pMsgContext->reqData[lub_index]) <= UP_CFG_CODE_LIMIT) )
4668  0b46 edf3001f          	ldy	[OFST+0,s]
4669  0b4a 87                	clra	
4670  0b4b e6ee              	ldab	d,y
4671  0b4d c130              	cmpb	#48
4672  0b4f 2508              	blo	L5422
4674  0b51 e68d              	ldab	OFST-18,s
4675  0b53 e6ee              	ldab	d,y
4676  0b55 c139              	cmpb	#57
4677  0b57 231e              	bls	L7422
4679  0b59                   L5422:
4680                         ; 1500 						if( (lub_errCnt < PROXI_MAX_NUM_ERRORS) )
4682  0b59 e68b              	ldab	OFST-20,s
4683  0b5b c103              	cmpb	#3
4684  0b5d 2418              	bhs	L7422
4685                         ; 1502 							lu_NRC = kDescNrcInvalidFormat;
4687  0b5f c613              	ldab	#19
4688  0b61 6b8c              	stab	OFST-19,s
4689                         ; 1504 							didwrite_rfhub_SetPROXIError(lub_errCnt, lub_index, PROXI_CFG_CODE_MASK, lu_NRC);
4691  0b63 87                	clra	
4692  0b64 3b                	pshd	
4693  0b65 c6ff              	ldab	#255
4694  0b67 3b                	pshd	
4695  0b68 e6f011            	ldab	OFST-14,s
4696  0b6b 3b                	pshd	
4697  0b6c e6f011            	ldab	OFST-14,s
4698  0b6f 4a0d9d9d          	call	f_didwrite_rfhub_SetPROXIError
4700  0b73 1b86              	leas	6,s
4701                         ; 1505 							lub_errCnt++;
4703  0b75 628b              	inc	OFST-20,s
4704  0b77                   L7422:
4705                         ; 1490 				for( lub_index = 0; lub_index < CFG_CODE_LEN; lub_index++ )
4707  0b77 628d              	inc	OFST-18,s
4710  0b79 e68d              	ldab	OFST-18,s
4711  0b7b c10b              	cmpb	#11
4712  0b7d 25c7              	blo	L7322
4713                         ; 1512 				if(	(lub_errCnt < PROXI_MAX_NUM_ERRORS) &&
4713                         ; 1513 					((u_8Bit)lu_PROXI_Data_Temp.s.Heated_Seats_Variant == FALSE ) )
4715  0b7f e68b              	ldab	OFST-20,s
4716  0b81 c103              	cmpb	#3
4717  0b83 241e              	bhs	L3522
4719  0b85 e689              	ldab	OFST-22,s
4720  0b87 c406              	andb	#6
4721  0b89 54                	lsrb	
4722  0b8a 2617              	bne	L3522
4723                         ; 1515 					lu_NRC = kDescNrcInvalidFormat;
4725  0b8c c613              	ldab	#19
4726  0b8e 6b8c              	stab	OFST-19,s
4727                         ; 1516 					didwrite_rfhub_SetPROXIError(lub_errCnt, PROXI_HEATSEAT_BYTE, PROXI_HEATSEAT_MASK, lu_NRC);
4729  0b90 87                	clra	
4730  0b91 3b                	pshd	
4731  0b92 c603              	ldab	#3
4732  0b94 3b                	pshd	
4733  0b95 c63d              	ldab	#61
4734  0b97 3b                	pshd	
4735  0b98 e6f011            	ldab	OFST-14,s
4736  0b9b 4a0d9d9d          	call	f_didwrite_rfhub_SetPROXIError
4738  0b9f 1b86              	leas	6,s
4739                         ; 1517 					lub_errCnt++;
4741  0ba1 628b              	inc	OFST-20,s
4742  0ba3                   L3522:
4743                         ; 1521 				if( (lub_errCnt < PROXI_MAX_NUM_ERRORS) &&
4743                         ; 1522 					((u_8Bit)lu_PROXI_Data_Temp.s.Seat_Material == FALSE ) )
4745  0ba3 e68b              	ldab	OFST-20,s
4746  0ba5 c103              	cmpb	#3
4747  0ba7 2421              	bhs	L5522
4749  0ba9 e689              	ldab	OFST-22,s
4750  0bab c430              	andb	#48
4751  0bad 54                	lsrb	
4752  0bae 54                	lsrb	
4753  0baf 54                	lsrb	
4754  0bb0 54                	lsrb	
4755  0bb1 2617              	bne	L5522
4756                         ; 1525 					lu_NRC = kDescNrcInvalidFormat;
4758  0bb3 c613              	ldab	#19
4759  0bb5 6b8c              	stab	OFST-19,s
4760                         ; 1526 					didwrite_rfhub_SetPROXIError(lub_errCnt, PROXI_SEAT_TYPE_BYTE, PROXI_SEAT_TYPE_MASK, lu_NRC);
4762  0bb7 87                	clra	
4763  0bb8 3b                	pshd	
4764  0bb9 c60c              	ldab	#12
4765  0bbb 3b                	pshd	
4766  0bbc c67b              	ldab	#123
4767  0bbe 3b                	pshd	
4768  0bbf e6f011            	ldab	OFST-14,s
4769  0bc2 4a0d9d9d          	call	f_didwrite_rfhub_SetPROXIError
4771  0bc6 1b86              	leas	6,s
4772                         ; 1527 					lub_errCnt++;
4774  0bc8 628b              	inc	OFST-20,s
4775  0bca                   L5522:
4776                         ; 1531 				if( (lub_errCnt < PROXI_MAX_NUM_ERRORS) &&
4776                         ; 1532 					((u_8Bit)lu_PROXI_Data_Temp.s.Steering_Wheel == PROXI_WHEEL_ABSENT ) )
4778  0bca e68b              	ldab	OFST-20,s
4779  0bcc c103              	cmpb	#3
4780  0bce 241d              	bhs	L7522
4782  0bd0 0e8a0119          	brset	OFST-21,s,1,L7522
4783                         ; 1534 					lu_NRC = kDescNrcInvalidFormat;
4785  0bd4 c613              	ldab	#19
4786  0bd6 6b8c              	stab	OFST-19,s
4787                         ; 1535 					didwrite_rfhub_SetPROXIError(lub_errCnt, PROXI_WHEEL_BYTE, PROXI_WHEEL_MASK, lu_NRC);
4789  0bd8 87                	clra	
4790  0bd9 3b                	pshd	
4791  0bda c610              	ldab	#16
4792  0bdc 3b                	pshd	
4793  0bdd c67b              	ldab	#123
4794  0bdf 3b                	pshd	
4795  0be0 e6f011            	ldab	OFST-14,s
4796  0be3 4a0d9d9d          	call	f_didwrite_rfhub_SetPROXIError
4798  0be7 1b86              	leas	6,s
4799                         ; 1536 					lub_errCnt++;
4801  0be9 628b              	inc	OFST-20,s
4802  0beb e68b              	ldab	OFST-20,s
4803  0bed                   L7522:
4804                         ; 1540 				if( (lub_errCnt < PROXI_MAX_NUM_ERRORS) &&
4804                         ; 1541 					((u_8Bit)lu_PROXI_Data_Temp.s.Wheel_Material == PROXI_WHEEL_TYPE_INVALID ) )
4806  0bed c103              	cmpb	#3
4807  0bef 241e              	bhs	L1622
4809  0bf1 e689              	ldab	OFST-22,s
4810  0bf3 55                	rolb	
4811  0bf4 55                	rolb	
4812  0bf5 55                	rolb	
4813  0bf6 c403              	andb	#3
4814  0bf8 2615              	bne	L1622
4815                         ; 1543 					lu_NRC = kDescNrcInvalidFormat;
4817  0bfa c613              	ldab	#19
4818  0bfc 6b8c              	stab	OFST-19,s
4819                         ; 1544 					didwrite_rfhub_SetPROXIError(lub_errCnt, PROXI_WHEEL_TYPE_BYTE, PROXI_WHEEL_TYPE_BYTE, lu_NRC);
4821  0bfe 87                	clra	
4822  0bff 3b                	pshd	
4823  0c00 c67b              	ldab	#123
4824  0c02 3b                	pshd	
4825  0c03 3b                	pshd	
4826  0c04 e6f011            	ldab	OFST-14,s
4827  0c07 4a0d9d9d          	call	f_didwrite_rfhub_SetPROXIError
4829  0c0b 1b86              	leas	6,s
4830                         ; 1545 					lub_errCnt++;
4832  0c0d 628b              	inc	OFST-20,s
4833  0c0f                   L1622:
4834                         ; 1549 				if( (lub_errCnt < PROXI_MAX_NUM_ERRORS) &&
4834                         ; 1550 					(lu_PROXI_Data_Temp.s.Vehicle_Line_Configuration == INVALID_DATA ) )
4836  0c0f e68b              	ldab	OFST-20,s
4837  0c11 c103              	cmpb	#3
4838  0c13 241b              	bhs	L3622
4840  0c15 e686              	ldab	OFST-25,s
4841  0c17 2617              	bne	L3622
4842                         ; 1552 					lu_NRC = kDescNrcInvalidFormat;
4844  0c19 c613              	ldab	#19
4845  0c1b 6b8c              	stab	OFST-19,s
4846                         ; 1553 					didwrite_rfhub_SetPROXIError(lub_errCnt, PROXI_VL_BYTE, PROXI_VL_MASK, lu_NRC);
4848  0c1d 87                	clra	
4849  0c1e 3b                	pshd	
4850  0c1f c6ff              	ldab	#255
4851  0c21 3b                	pshd	
4852  0c22 c668              	ldab	#104
4853  0c24 3b                	pshd	
4854  0c25 e6f011            	ldab	OFST-14,s
4855  0c28 4a0d9d9d          	call	f_didwrite_rfhub_SetPROXIError
4857  0c2c 1b86              	leas	6,s
4858                         ; 1554 					lub_errCnt++;
4860  0c2e 628b              	inc	OFST-20,s
4861  0c30                   L3622:
4862                         ; 1568 				if( lub_errCnt < PROXI_MAX_NUM_ERRORS )
4864  0c30 e68b              	ldab	OFST-20,s
4865  0c32 c103              	cmpb	#3
4866  0c34 2471              	bhs	L5622
4867                         ; 1571 					luw_CalculatedCRC = 0;
4869  0c36 18698f            	clrw	OFST-16,s
4870                         ; 1572 					luw_ReceivedCRC = 0;
4872                         ; 1574 					for (lub_index = START_CRC_CALC_INDEX; lub_index < PROXI_FILE_LEN ; lub_index++)
4874  0c39 c619              	ldab	#25
4875  0c3b 6b8d              	stab	OFST-18,s
4876  0c3d                   L7622:
4877                         ; 1576 						luw_CalculatedCRC = didwrite_rfhub_reversed_crc16_mmc_upd( luw_CalculatedCRC, pMsgContext->reqData[lub_index]);
4879  0c3d edf3001f          	ldy	[OFST+0,s]
4880  0c41 87                	clra	
4881  0c42 e6ee              	ldab	d,y
4882  0c44 3b                	pshd	
4883  0c45 ecf011            	ldd	OFST-14,s
4884  0c48 4a0d6464          	call	f_didwrite_rfhub_reversed_crc16_mmc_upd
4886  0c4c 1b82              	leas	2,s
4887  0c4e 6c8f              	std	OFST-16,s
4888                         ; 1574 					for (lub_index = START_CRC_CALC_INDEX; lub_index < PROXI_FILE_LEN ; lub_index++)
4890  0c50 628d              	inc	OFST-18,s
4893  0c52 e68d              	ldab	OFST-18,s
4894  0c54 c1ff              	cmpb	#255
4895  0c56 25e5              	blo	L7622
4896                         ; 1579 					luw_ReceivedCRC = didwrite_rfhub_convert_ascii_string_to_num(&pMsgContext->reqData[CRC_INDEX_PROXI_FILE], CRC_NUM_OF_DIGITS);
4898  0c58 cc0005            	ldd	#5
4899  0c5b 3b                	pshd	
4900  0c5c ecf30021          	ldd	[OFST+2,s]
4901  0c60 c30006            	addd	#6
4902  0c63 4a0d2020          	call	f_didwrite_rfhub_convert_ascii_string_to_num
4904  0c67 1b82              	leas	2,s
4905  0c69 6cf011            	std	OFST-14,s
4906                         ; 1584 					lub_errCnt = didwrite_rfhub_check_crc_byte_by_byte(luw_CalculatedCRC, luw_ReceivedCRC, lub_errCnt);
4908  0c6c e68b              	ldab	OFST-20,s
4909  0c6e 87                	clra	
4910  0c6f 3b                	pshd	
4911  0c70 ecf013            	ldd	OFST-12,s
4912  0c73 3b                	pshd	
4913  0c74 ecf013            	ldd	OFST-12,s
4914  0c77 4a0dbebe          	call	f_didwrite_rfhub_check_crc_byte_by_byte
4916  0c7b 1b84              	leas	4,s
4917  0c7d 6b8b              	stab	OFST-20,s
4918                         ; 1587 					if( lub_errCnt == 0 )
4920  0c7f 2626              	bne	L5622
4921                         ; 1590 						memcpy((u_8Bit *)lu_PROXI_Data_OrigBackup.byte,(u_8Bit *)ru_eep_PROXI_Cfg.byte, PROXI_DATA_SIZE);
4923  0c81 19f014            	leay	OFST-11,s
4924  0c84 ce0024            	ldx	#_ru_eep_PROXI_Cfg
4925  0c87 cc000b            	ldd	#11
4926  0c8a                   L001:
4927  0c8a 180a3070          	movb	1,x+,1,y+
4928  0c8e 0434f9            	dbne	d,L001
4929                         ; 1591 						memcpy((u_8Bit *)ru_eep_PROXI_Cfg.byte,(u_8Bit *)lu_PROXI_Data_Temp.byte, PROXI_DATA_SIZE);
4931  0c91 cd0024            	ldy	#_ru_eep_PROXI_Cfg
4932  0c94 1a80              	leax	OFST-31,s
4933  0c96 cc000b            	ldd	#11
4934  0c99                   L201:
4935  0c99 180a3070          	movb	1,x+,1,y+
4936  0c9d 0434f9            	dbne	d,L201
4937                         ; 1611 							re_PROXI_state = E_PROXI_CFG_UPDATE;
4939  0ca0 c601              	ldab	#1
4940  0ca2 7b0041            	stab	_re_PROXI_state
4941                         ; 1613 							lub_PROXIautoTrigger = TRUE;
4943  0ca5 6b8e              	stab	OFST-17,s
4944  0ca7                   L5622:
4945                         ; 1636 				if(lub_errCnt > 0)
4947  0ca7 e68b              	ldab	OFST-20,s
4948  0ca9 276b              	beq	L7222
4949                         ; 1653 						re_PROXI_state = E_REJ_TABLE_UPDATE;
4951  0cab c602              	ldab	#2
4952  0cad 7b0041            	stab	_re_PROXI_state
4953                         ; 1655 						lub_PROXIautoTrigger = TRUE;
4955  0cb0 53                	decb	
4956  0cb1 2043              	bra	LC021
4957  0cb3                   L1371:
4958                         ; 1661 			case E_PROXI_CFG_UPDATE:
4958                         ; 1662 //				/* Check if update operation */
4958                         ; 1663 //				if((T_UWORD)EEPBLK_IDLE_OK != EEPROMBlock_Status(PROXI_CFG))
4958                         ; 1664 //				{
4958                         ; 1665 //					/* Block is not yet updated */
4958                         ; 1666 //				}
4958                         ; 1667 //				else
4958                         ; 1668 //				{
4958                         ; 1669 					/* Block updated */
4958                         ; 1670 					re_PROXI_state = E_PROXI_POS_RSP;
4960  0cb3 c603              	ldab	#3
4961  0cb5 7b0041            	stab	_re_PROXI_state
4962                         ; 1671 					lub_PROXIautoTrigger = TRUE;
4964  0cb8 c601              	ldab	#1
4965  0cba 6b8e              	stab	OFST-17,s
4966                         ; 1674 					EE_BlockWrite(EE_PROXI_CFG, (UINT8*)&ru_eep_PROXI_Cfg);
4968  0cbc cc0024            	ldd	#_ru_eep_PROXI_Cfg
4969  0cbf 3b                	pshd	
4970  0cc0 cc001d            	ldd	#29
4971  0cc3 4a000000          	call	f_EE_BlockWrite
4973  0cc7 1b82              	leas	2,s
4974                         ; 1675 					temp_PROXI_learned = 0xAA;
4976  0cc9 c6aa              	ldab	#170
4977  0ccb 6bf013            	stab	OFST-12,s
4978                         ; 1676 					EE_BlockWrite(EE_PROXI_LEARNED, &temp_PROXI_learned);
4980  0cce 1af013            	leax	OFST-12,s
4981  0cd1 34                	pshx	
4982  0cd2 cc001f            	ldd	#31
4983  0cd5 4a000000          	call	f_EE_BlockWrite
4985  0cd9 1b82              	leas	2,s
4986                         ; 1682 				break;
4988  0cdb 2039              	bra	L7222
4989  0cdd                   L3371:
4990                         ; 1684 			case E_REJ_TABLE_UPDATE:
4990                         ; 1685 //				/* Check if update operation */
4990                         ; 1686 //				if((T_UWORD)EEPBLK_IDLE_OK != EEPROMBlock_Status(PROXI_REJECTION_FEEDBACK_TBL))
4990                         ; 1687 //				{
4990                         ; 1688 //					/* Block is not yet updated */
4990                         ; 1689 //				}
4990                         ; 1690 //				else
4990                         ; 1691 //				{
4990                         ; 1692 					lu_NRC = GET_FIRST_NRC();
4992  0cdd 18098c0038        	movb	_ras_Rejection_Feedback_Tbl,OFST-19,s
4993                         ; 1693 				    EE_BlockWrite(EE_PROXI_FAILURE, (UINT8*)&ras_Rejection_Feedback_Tbl[0]);
4995  0ce2 cc0038            	ldd	#_ras_Rejection_Feedback_Tbl
4996  0ce5 3b                	pshd	
4997  0ce6 cc001e            	ldd	#30
4998  0ce9 4a000000          	call	f_EE_BlockWrite
5000  0ced 1b82              	leas	2,s
5001                         ; 1694 					re_PROXI_state = E_PROXI_NEG_RSP;
5003                         ; 1695 					lub_PROXIautoTrigger = TRUE;
5005  0cef                   LC022:
5006  0cef c604              	ldab	#4
5007  0cf1 7b0041            	stab	_re_PROXI_state
5008  0cf4 c601              	ldab	#1
5009  0cf6                   LC021:
5010  0cf6 6b8e              	stab	OFST-17,s
5011                         ; 1698 				break;
5013  0cf8 201c              	bra	L7222
5014  0cfa                   L5371:
5015                         ; 1700 			case E_PROXI_POS_RSP:
5015                         ; 1701 				re_PROXI_state = E_PROXI_LRN_START;
5017  0cfa 7b0041            	stab	_re_PROXI_state
5018                         ; 1705 				DescSetNegResponse(kDescNrcNone);
5020  0cfd 87                	clra	
5022                         ; 1707 				DescProcessingDone();
5025                         ; 1709 				lub_PROXIautoTrigger = FALSE;
5027                         ; 1711 				break;
5029  0cfe 200c              	bra	LC020
5030  0d00                   L7371:
5031                         ; 1713 			case E_PROXI_NEG_RSP:
5031                         ; 1714 				/* Must never reach with no NRC set */
5031                         ; 1715 				if(lu_NRC == kDescNrcNone)
5033  0d00 e68c              	ldab	OFST-19,s
5034  0d02 2604              	bne	L1032
5035                         ; 1717 					lu_NRC = kDescNrcGeneralProgrammingFailure;
5037  0d04 c672              	ldab	#114
5038  0d06 6b8c              	stab	OFST-19,s
5039  0d08                   L1032:
5040                         ; 1720 				re_PROXI_state = E_PROXI_LRN_START;
5042  0d08 87                	clra	
5043  0d09 7a0041            	staa	_re_PROXI_state
5044                         ; 1724 				DescSetNegResponse(lu_NRC);
5047                         ; 1726 				DescProcessingDone();
5050                         ; 1728 				lub_PROXIautoTrigger = FALSE;
5052  0d0c                   LC020:
5053  0d0c 4a000000          	call	f_DescSetNegResponse
5054  0d10 4a000000          	call	f_DescProcessingDone
5055  0d14 698e              	clr	OFST-17,s
5056                         ; 1729 				break;
5058  0d16                   L7222:
5059                         ; 1739 	while(lub_PROXIautoTrigger);
5061  0d16 e68e              	ldab	OFST-17,s
5062  0d18 1826fba2          	bne	L5222
5063                         ; 1740 }
5066  0d1c 1bf021            	leas	33,s
5067  0d1f 0a                	rtc	
5132                         ; 1753 u_16Bit didwrite_rfhub_convert_ascii_string_to_num(DescMsgItem * pub_CRCStr, u_8Bit lub_CRCStrLen)
5132                         ; 1754 {
5133                         	switch	.ftext
5134  0d20                   f_didwrite_rfhub_convert_ascii_string_to_num:
5136  0d20 3b                	pshd	
5137  0d21 1b9b              	leas	-5,s
5138       00000005          OFST:	set	5
5141                         ; 1759 	luw_CRCReturn = 0;
5143  0d23 186983            	clrw	OFST-2,s
5144                         ; 1760 	luw_Unit = 1;
5146  0d26 cc0001            	ldd	#1
5147  0d29 6c81              	std	OFST-4,s
5148                         ; 1761     for(lub_StrIndex = lub_CRCStrLen; lub_StrIndex; lub_StrIndex--)
5150  0d2b e68b              	ldab	OFST+6,s
5151  0d2d 6b80              	stab	OFST-5,s
5153  0d2f 202c              	bra	L5332
5154  0d31                   L1332:
5155                         ; 1763     	luw_CRCReturn += multiply_u16_u16_u16((u_16Bit)(pub_CRCStr[lub_StrIndex-1] & 0x0F), luw_Unit);
5157  0d31 ec81              	ldd	OFST-4,s
5158  0d33 3b                	pshd	
5159  0d34 ed87              	ldy	OFST+2,s
5160  0d36 e682              	ldab	OFST-3,s
5161  0d38 87                	clra	
5162  0d39 c3ffff            	addd	#-1
5163  0d3c e6ee              	ldab	d,y
5164  0d3e c40f              	andb	#15
5165  0d40 87                	clra	
5166  0d41 4a0e2626          	call	f_multiply_u16_u16_u16
5168  0d45 1b82              	leas	2,s
5169  0d47 e383              	addd	OFST-2,s
5170  0d49 6c83              	std	OFST-2,s
5171                         ; 1764     	luw_Unit = multiply_u16_u16_u16(luw_Unit, 10);
5173  0d4b cc000a            	ldd	#10
5174  0d4e 3b                	pshd	
5175  0d4f ec83              	ldd	OFST-2,s
5176  0d51 4a0e2626          	call	f_multiply_u16_u16_u16
5178  0d55 1b82              	leas	2,s
5179  0d57 6c81              	std	OFST-4,s
5180                         ; 1761     for(lub_StrIndex = lub_CRCStrLen; lub_StrIndex; lub_StrIndex--)
5182  0d59 6380              	dec	OFST-5,s
5183  0d5b e680              	ldab	OFST-5,s
5184  0d5d                   L5332:
5187  0d5d 26d2              	bne	L1332
5188                         ; 1767     return luw_CRCReturn;
5190  0d5f ec83              	ldd	OFST-2,s
5193  0d61 1b87              	leas	7,s
5194  0d63 0a                	rtc	
5248                         ; 1777 u_16Bit didwrite_rfhub_reversed_crc16_mmc_upd(u_16Bit luw_crc_old, u_8Bit lub_data)
5248                         ; 1778 {
5249                         	switch	.ftext
5250  0d64                   f_didwrite_rfhub_reversed_crc16_mmc_upd:
5252  0d64 3b                	pshd	
5253  0d65 3b                	pshd	
5254       00000002          OFST:	set	2
5257                         ; 1780     for (lub_cnt_bits = 8; lub_cnt_bits; lub_cnt_bits--)
5259  0d66 c608              	ldab	#8
5260  0d68 6b80              	stab	OFST-2,s
5261  0d6a                   L3632:
5262                         ; 1782         lub_flag_xor = (luw_crc_old ^ lub_data) & 0x01;
5264  0d6a e683              	ldab	OFST+1,s
5265  0d6c e888              	eorb	OFST+6,s
5266  0d6e c401              	andb	#1
5267  0d70 6b81              	stab	OFST-1,s
5268                         ; 1783         lub_data >>= 1;
5270  0d72 6488              	lsr	OFST+6,s
5271                         ; 1784         luw_crc_old >>= 1;
5273  0d74 186482            	lsrw	OFST+0,s
5274                         ; 1785         if (lub_flag_xor) luw_crc_old ^= 0x8408;
5276  0d77 044108            	tbeq	b,L1732
5279  0d7a cd8408            	ldy	#-31736
5280  0d7d 18e882            	eory	OFST+0,s
5281  0d80 6d82              	sty	OFST+0,s
5282  0d82                   L1732:
5283                         ; 1780     for (lub_cnt_bits = 8; lub_cnt_bits; lub_cnt_bits--)
5285  0d82 6380              	dec	OFST-2,s
5288  0d84 26e4              	bne	L3632
5289                         ; 1787     return(luw_crc_old);
5291  0d86 ec82              	ldd	OFST+0,s
5294  0d88 1b84              	leas	4,s
5295  0d8a 0a                	rtc	
5351                         ; 1797 u_8Bit didwrite_rfhub_ReturnPROXISignal(DescMsgItem * data, u_8Bit lub_BytePos, u_8Bit lub_SignalMask, u_8Bit lub_Offset)
5351                         ; 1798 {
5352                         	switch	.ftext
5353  0d8b                   f_didwrite_rfhub_ReturnPROXISignal:
5355       fffffffe          OFST:	set	-2
5358                         ; 1799 	return (u_8Bit)((data[lub_BytePos] & lub_SignalMask) >> lub_Offset );
5360  0d8b b746              	tfr	d,y
5361  0d8d e684              	ldab	OFST+6,s
5362  0d8f 87                	clra	
5363  0d90 e6ee              	ldab	d,y
5364  0d92 e486              	andb	OFST+8,s
5365  0d94 a688              	ldaa	OFST+10,s
5366  0d96 2704              	beq	L211
5367  0d98                   L411:
5368  0d98 54                	lsrb	
5369  0d99 0430fc            	dbne	a,L411
5370  0d9c                   L211:
5373  0d9c 0a                	rtc	
5427                         ; 1809 void didwrite_rfhub_SetPROXIError(u_8Bit lub_RecordPos, u_8Bit lub_ErrorBytePos, u_8Bit lub_ErrorMask, DescNegResCode lub_ErrorType)
5427                         ; 1810 {
5428                         	switch	.ftext
5429  0d9d                   f_didwrite_rfhub_SetPROXIError:
5431  0d9d 3b                	pshd	
5432       00000000          OFST:	set	0
5435                         ; 1811 	if( lub_RecordPos < PROXI_MAX_NUM_ERRORS )
5437  0d9e c103              	cmpb	#3
5438  0da0 241a              	bhs	L1442
5439                         ; 1813 		ras_Rejection_Feedback_Tbl[lub_RecordPos].Error_Type = lub_ErrorType;
5441  0da2 87                	clra	
5442  0da3 cd0003            	ldy	#3
5443  0da6 13                	emul	
5444  0da7 b746              	tfr	d,y
5445  0da9 180a8aea0038      	movb	OFST+10,s,_ras_Rejection_Feedback_Tbl,y
5446                         ; 1814 		ras_Rejection_Feedback_Tbl[lub_RecordPos].Error_BytePos = lub_ErrorBytePos + 1;
5448  0daf e686              	ldab	OFST+6,s
5449  0db1 52                	incb	
5450  0db2 6bea0039          	stab	_ras_Rejection_Feedback_Tbl+1,y
5451                         ; 1815 		ras_Rejection_Feedback_Tbl[lub_RecordPos].Error_Mask = lub_ErrorMask;
5453  0db6 180a88ea003a      	movb	OFST+8,s,_ras_Rejection_Feedback_Tbl+2,y
5454  0dbc                   L1442:
5455                         ; 1817 }
5458  0dbc 31                	puly	
5459  0dbd 0a                	rtc	
5536                         ; 1827 u_8Bit didwrite_rfhub_check_crc_byte_by_byte(u_16Bit luw_ReceivedCRC, u_16Bit luw_CalculatedCRC, u_8Bit lub_ErrCnt)
5536                         ; 1828 {
5537                         	switch	.ftext
5538  0dbe                   f_didwrite_rfhub_check_crc_byte_by_byte:
5540  0dbe 3b                	pshd	
5541  0dbf 1b9c              	leas	-4,s
5542       00000004          OFST:	set	4
5545                         ; 1832 	DescNegResCode lu_NRC = kDescNrcRequestOutOfRange;
5547  0dc1 c631              	ldab	#49
5548  0dc3 6b83              	stab	OFST-1,s
5549                         ; 1836 	for(lub_ByteNum = CRC_LOW_BYTE_LOC; lub_ByteNum >= CRC_HIGH_BYTE_LOC; lub_ByteNum--)
5551  0dc5 c60a              	ldab	#10
5552  0dc7 6b80              	stab	OFST-4,s
5553  0dc9                   L3742:
5554                         ; 1838 		lub_ReceivedChar = luw_ReceivedCRC % CRC_DECIMAL_FACTOR + CRC_CHARACTER_OFFSET;
5556  0dc9 ec84              	ldd	OFST+0,s
5557  0dcb ce000a            	ldx	#10
5558  0dce 1810              	idiv	
5559  0dd0 cb30              	addb	#48
5560  0dd2 6b81              	stab	OFST-3,s
5561                         ; 1839 		lub_CalculatedChar = luw_CalculatedCRC % CRC_DECIMAL_FACTOR + CRC_CHARACTER_OFFSET;
5563  0dd4 ec89              	ldd	OFST+5,s
5564  0dd6 ce000a            	ldx	#10
5565  0dd9 1810              	idiv	
5566  0ddb cb30              	addb	#48
5567  0ddd 6b82              	stab	OFST-2,s
5568                         ; 1840 		if(lub_ReceivedChar != lub_CalculatedChar)
5570  0ddf e681              	ldab	OFST-3,s
5571  0de1 e182              	cmpb	OFST-2,s
5572  0de3 2721              	beq	L1052
5573                         ; 1842 			rub_Test = (lub_ReceivedChar ^ lub_CalculatedChar);
5575  0de5 e882              	eorb	OFST-2,s
5576  0de7 7b0000            	stab	_rub_Test
5577                         ; 1844 			didwrite_rfhub_SetPROXIError(lub_ErrCnt, lub_ByteNum, rub_Test, lu_NRC);
5579  0dea e683              	ldab	OFST-1,s
5580  0dec 87                	clra	
5581  0ded 3b                	pshd	
5582  0dee f60000            	ldab	_rub_Test
5583  0df1 3b                	pshd	
5584  0df2 e684              	ldab	OFST+0,s
5585  0df4 3b                	pshd	
5586  0df5 e6f012            	ldab	OFST+14,s
5587  0df8 4a0d9d9d          	call	f_didwrite_rfhub_SetPROXIError
5589  0dfc 1b86              	leas	6,s
5590                         ; 1845 			lub_ErrCnt++;
5592  0dfe 628c              	inc	OFST+8,s
5593                         ; 1846 			if(lub_ErrCnt == PROXI_MAX_NUM_ERRORS)
5595  0e00 e68c              	ldab	OFST+8,s
5596  0e02 c103              	cmpb	#3
5597                         ; 1848 				return lub_ErrCnt;
5600  0e04 271d              	beq	L221
5601  0e06                   L1052:
5602                         ; 1851 		luw_ReceivedCRC /= CRC_DECIMAL_FACTOR;
5604  0e06 ec84              	ldd	OFST+0,s
5605  0e08 cd000a            	ldy	#10
5606  0e0b b765              	tfr	y,x
5607  0e0d 1810              	idiv	
5608  0e0f 6e84              	stx	OFST+0,s
5609                         ; 1852 		luw_CalculatedCRC /= CRC_DECIMAL_FACTOR;
5611  0e11 ec89              	ldd	OFST+5,s
5612  0e13 b765              	tfr	y,x
5613  0e15 1810              	idiv	
5614  0e17 6e89              	stx	OFST+5,s
5615                         ; 1836 	for(lub_ByteNum = CRC_LOW_BYTE_LOC; lub_ByteNum >= CRC_HIGH_BYTE_LOC; lub_ByteNum--)
5617  0e19 6380              	dec	OFST-4,s
5620  0e1b e680              	ldab	OFST-4,s
5621  0e1d c106              	cmpb	#6
5622  0e1f 24a8              	bhs	L3742
5623                         ; 1855 	return lub_ErrCnt;
5625  0e21 e68c              	ldab	OFST+8,s
5627  0e23                   L221:
5629  0e23 1b86              	leas	6,s
5630  0e25 0a                	rtc	
5669                         ; 1868 u_16Bit multiply_u16_u16_u16 (u_16Bit ruw_op1, u_16Bit ruw_op2)
5669                         ; 1869 {
5670                         	switch	.ftext
5671  0e26                   f_multiply_u16_u16_u16:
5673       fffffffe          OFST:	set	-2
5676                         ; 1870   return ((u_16Bit)ruw_op1*ruw_op2);
5678  0e26 ed83              	ldy	OFST+5,s
5679  0e28 13                	emul	
5682  0e29 0a                	rtc	
5877                         	xdef	f_didwrite_rfhub_check_crc_byte_by_byte
5878                         	xdef	f_multiply_u16_u16_u16
5879                         	xdef	f_didwrite_rfhub_SetPROXIError
5880                         	xdef	f_didwrite_rfhub_ReturnPROXISignal
5881                         	xdef	f_didwrite_rfhub_reversed_crc16_mmc_upd
5882                         	xdef	f_didwrite_rfhub_convert_ascii_string_to_num
5883                         	xref	f_diagF_DiagResponse
5884                         	switch	.bss
5885  0000                   _rub_Test:
5886  0000 00                	ds.b	1
5887                         	xdef	_rub_Test
5888  0001                   L324_DataLength:
5889  0001 0000              	ds.b	2
5890  0003                   L124_Response:
5891  0003 00                	ds.b	1
5892                         	xref	_diag_writedata_mode_c
5893                         	xref	f_EE_BlockWrite
5894                         	xref	f_BattVF_Write_Diag_Data
5895                         	xref	f_BattVF_Chck_Diag_Prms
5896                         	xref	_intFlt_bytes
5897                         	xref	f_stWF_Write_Diag_Data
5898                         	xref	f_stWF_Chck_Diag_Prms
5899                         	xref	f_vsF_Write_Diag_Data
5900                         	xref	f_vsF_Chck_Diag_Prms
5901                         	xref	f_hsF_WriteData_to_ShadowEEPROM
5902                         	xref	f_hsF_CheckDiagnoseParameter
5903                         	xref	_CSWM_config_c
5904                         	xref	_canio_RemSt_configStat_c
5905                         	xref	_canio_RemSt_stored_stat
5906                         	xref	_diag_io_lock3_flags_c
5907                         	xdef	f_diagLF_WriteData
5908                         	xdef	f_diagWriteF_CyclicTask
5909  0004                   _PROXI_Learned_c:
5910  0004 00                	ds.b	1
5911                         	xdef	_PROXI_Learned_c
5912  0005                   _diag_temp_buffer_c:
5913  0005 0000000000000000  	ds.b	20
5914                         	xdef	_diag_temp_buffer_c
5915  0019                   _ru_def_eep_map:
5916  0019 0000000000000000  	ds.b	11
5917                         	xdef	_ru_def_eep_map
5918  0024                   _ru_eep_PROXI_Cfg:
5919  0024 0000000000000000  	ds.b	11
5920                         	xdef	_ru_eep_PROXI_Cfg
5921  002f                   _def_Feedback_Tbl:
5922  002f 0000000000000000  	ds.b	9
5923                         	xdef	_def_Feedback_Tbl
5924  0038                   _ras_Rejection_Feedback_Tbl:
5925  0038 0000000000000000  	ds.b	9
5926                         	xdef	_ras_Rejection_Feedback_Tbl
5927  0041                   _re_PROXI_state:
5928  0041 00                	ds.b	1
5929                         	xdef	_re_PROXI_state
5930  0042                   L12_diag_sec_FAA_c:
5931  0042 00                	ds.b	1
5932  0043                   L71_secMod1_var2_l:
5933  0043 00000000          	ds.b	4
5934  0047                   L51_secMod1_var1_l:
5935  0047 00000000          	ds.b	4
5936  004b                   L31_diag_10sec_timer_w:
5937  004b 0000              	ds.b	2
5938  004d                   L11_hsm_key_l:
5939  004d 00000000          	ds.b	4
5940  0051                   L7_tool_key_l:
5941  0051 00000000          	ds.b	4
5942  0055                   L5_diag_seed_generated_l:
5943  0055 00000000          	ds.b	4
5944  0059                   L3_diag_seed_l:
5945  0059 00000000          	ds.b	4
5946                         	xref	_diag_sec_access5_state_c
5947                         	xref	_diag_flags_c
5948                         	xdef	f_ApplDescWriteVIN
5949                         	xdef	f_ApplDescWriteEcuSerialNumber
5950                         	xdef	f_ApplDescWrite_223001_Program_ECU_Data_at_Flash_Station_2
5951                         	xdef	f_ApplDescWrite_223000_Program_ECU_Data_at_Flash_Station_1
5952                         	xdef	f_ApplDescWrite_222810_Heated_Seat_NTC_Cutoff_Parameters_Rear_Seats
5953                         	xdef	f_ApplDescWrite_22280F_Heated_Seat_Max_Timeout_Parameters_Rear_Seats
5954                         	xdef	f_ApplDescWrite_22280E_Heated_Seat_PWM_Calibration_Parameters_Rear_Seats
5955                         	xdef	f_ApplDescWrite_22280C_Heated_Steering_Wheel_Min_Timeout_Parameters
5956                         	xdef	f_ApplDescWrite_22280A_HeatVent_Auto_Activation_Temperature_Calibration_Paramet
5957                         	xdef	f_ApplDescWrite_222809_Steering_Wheel_Temperature_Cut_off_Parameters
5958                         	xdef	f_ApplDescWrite_222808_Heated_Seat_NTC_Cutoff_Parameters_Front_Seats
5959                         	xdef	f_ApplDescWrite_222806_Voltage_Level_OFF
5960                         	xdef	f_ApplDescWrite_222805_Voltage_Level_ON
5961                         	xdef	f_ApplDescWrite_222804_Heated_Steering_Wheel_Max_Timeout_Parameters
5962                         	xdef	f_ApplDescWrite_222803_Heated_Seat_Max_Timeout_Parameters_Front_Seats
5963                         	xdef	f_ApplDescWrite_222802_Heated_Steering_Wheel_PWM_Calibration_Parameters
5964                         	xdef	f_ApplDescWrite_222801_Vented_Seat_PWM_Calibration_Parameters
5965                         	xdef	f_ApplDescWrite_222800_Heated_Seat_PWM_Calibration_Parameters_Front_Seats
5966                         	xdef	f_ApplDescWrite_2E2023_System_Configuration_PROXI
5967                         	xref	f_DescSetNegResponse
5968                         	xref	f_DescProcessingDone
5989                         	end
