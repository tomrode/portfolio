   1                         ; C Compiler for S12XE [COSMIC Software].
   2                         ; Generator V4.7.8 - 27 Jul 2007
   3                         ; Optimizer V4.6.13 - 27 Jul 2007
  35                         ; 68 interrupt void near Ect_3_Isr(void)
  35                         ; 69 {
  36                         	switch	.text
  37  0000                   _Ect_3_Isr:
  42                         ; 70     Pwm_TIM_Channel_3_Isr();
  44  0000 160000            	jsr	_Pwm_TIM_Channel_3_Isr
  46                         ; 71 }
  49  0003 0b                	rti	
  72                         ; 72 interrupt void near Ect_4_Isr(void)
  72                         ; 73 {
  73                         	switch	.text
  74  0004                   _Ect_4_Isr:
  79                         ; 74     Pwm_TIM_Channel_4_Isr();
  81  0004 160000            	jsr	_Pwm_TIM_Channel_4_Isr
  83                         ; 75 }
  86  0007 0b                	rti	
 109                         ; 76 interrupt void near Ect_5_Isr(void)
 109                         ; 77 {
 110                         	switch	.text
 111  0008                   _Ect_5_Isr:
 116                         ; 78     Pwm_TIM_Channel_5_Isr();
 118  0008 160000            	jsr	_Pwm_TIM_Channel_5_Isr
 120                         ; 79 }
 123  000b 0b                	rti	
 146                         ; 80 interrupt void near  Ect_6_Isr(void)
 146                         ; 81 {
 147                         	switch	.text
 148  000c                   _Ect_6_Isr:
 153                         ; 82     Pwm_TIM_Channel_6_Isr();
 155  000c 160000            	jsr	_Pwm_TIM_Channel_6_Isr
 157                         ; 83 }
 160  000f 0b                	rti	
 183                         ; 84 interrupt void near  Ect_7_Isr(void)
 183                         ; 85 {
 184                         	switch	.text
 185  0010                   _Ect_7_Isr:
 190                         ; 86     Pwm_TIM_Channel_7_Isr();
 192  0010 160000            	jsr	_Pwm_TIM_Channel_7_Isr
 194                         ; 87 }
 197  0013 0b                	rti	
 209                         	xdef	_Ect_7_Isr
 210                         	xdef	_Ect_6_Isr
 211                         	xdef	_Ect_5_Isr
 212                         	xdef	_Ect_4_Isr
 213                         	xdef	_Ect_3_Isr
 214                         	xref	_Pwm_TIM_Channel_7_Isr
 215                         	xref	_Pwm_TIM_Channel_6_Isr
 216                         	xref	_Pwm_TIM_Channel_5_Isr
 217                         	xref	_Pwm_TIM_Channel_4_Isr
 218                         	xref	_Pwm_TIM_Channel_3_Isr
 238                         	end
