   1                         ; C Compiler for S12XE [COSMIC Software].
   2                         ; Generator V4.7.8 - 27 Jul 2007
   3                         ; Optimizer V4.6.13 - 27 Jul 2007
 540                         ; 521 @far void hsF_Init(void)
 540                         ; 522 {
 541                         .ftext:	section	.text
 542  0000                   f_hsF_Init:
 546                         ; 525 	EE_BlockRead(EE_HEAT_CAL_DATA_PF, (u_8Bit*)&hs_cal_prms); //MKS 61288
 548  0000 cc0077            	ldd	#_hs_cal_prms
 549  0003 3b                	pshd	
 550  0004 cc000e            	ldd	#14
 551  0007 4a000000          	call	f_EE_BlockRead
 553  000b 1b82              	leas	2,s
 554                         ; 529 	if(ru_eep_PROXI_Cfg.s.Vehicle_Line_Configuration == PROXI_VEH_LINE_PF)
 556                         ; 531 		hs_vehicle_line_c = HS_VEH_PF;
 559                         ; 536 		hs_vehicle_line_c = HS_VEH_PF;
 561  000d 79010e            	clr	_hs_vehicle_line_c
 562                         ; 540 	if(ru_eep_PROXI_Cfg.s.Seat_Material == PROXI_SEAT_TYPE_CLOTH)
 564  0010 f60009            	ldab	_ru_eep_PROXI_Cfg+9
 565  0013 c430              	andb	#48
 566  0015 c110              	cmpb	#16
 567  0017 2605              	bne	L333
 568                         ; 543 	    hs_seat_config_c = HS_VEH_CLOTH;
 570  0019 79010d            	clr	_hs_seat_config_c
 572  001c 2012              	bra	L533
 573  001e                   L333:
 574                         ; 545 	else if(ru_eep_PROXI_Cfg.s.Seat_Material == PROXI_SEAT_TYPE_LEATHER)
 576  001e f60009            	ldab	_ru_eep_PROXI_Cfg+9
 577  0021 c430              	andb	#48
 578  0023 c120              	cmpb	#32
 579  0025 2604              	bne	L733
 580                         ; 548 	    hs_seat_config_c = HS_VEH_BASE_LEATHER;
 582  0027 c602              	ldab	#2
 584  0029 2002              	bra	LC001
 585  002b                   L733:
 586                         ; 553 	    hs_seat_config_c = HS_VEH_PERF_LEATHER;
 588  002b c604              	ldab	#4
 589  002d                   LC001:
 590  002d 7b010d            	stab	_hs_seat_config_c
 591  0030                   L533:
 592                         ; 556 	EE_BlockRead(EE_HEAT_FAULT_THRESHOLDS, (u_8Bit*)&hs_flt_dtc_c); //MKS 50111
 594  0030 cc00cb            	ldd	#_hs_flt_dtc_c
 595  0033 3b                	pshd	
 596  0034 cc0005            	ldd	#5
 597  0037 4a000000          	call	f_EE_BlockRead
 599  003b 1b82              	leas	2,s
 600                         ; 559 	hs_rear_mature_time_w = /*hs_rear_stksw_mat_time_c*/ hs_flt_dtc_c[HS_REAR_STUCK_MATURE_TIME] * 25; //Called in 40ms time slice to detect stuck switch
 602  003d f600d0            	ldab	_hs_flt_dtc_c+5
 603  0040 8619              	ldaa	#25
 604  0042 12                	mul	
 605  0043 7c0035            	std	_hs_rear_mature_time_w
 606                         ; 560 	hs_rear_demature_time_c = /*hs_rear_stksw_demat_time_c*/ (unsigned char)(hs_flt_dtc_c[HS_REAR_STUCK_DEMATURE_TIME]* 25); //Called in 40ms time slice to demature stuck switch
 608  0046 f600d1            	ldab	_hs_flt_dtc_c+6
 609  0049 8619              	ldaa	#25
 610  004b 12                	mul	
 611  004c 7b0034            	stab	_hs_rear_demature_time_c
 612                         ; 563 	hsLF_ClearHeatSequence();
 614  004f 4a0b5a5a          	call	f_hsLF_ClearHeatSequence
 616                         ; 564 }
 619  0053 0a                	rtc	
 707                         ; 671 @far void hsF_HeatManager(void)
 707                         ; 672 {
 708                         	switch	.ftext
 709  0054                   f_hsF_HeatManager:
 711  0054 1b9d              	leas	-3,s
 712       00000003          OFST:	set	3
 715                         ; 678 	hs_LED_U12S_stat_c = Dio_ReadChannel(LED_U12S_EN);
 717  0056 e6fbffa6          	ldab	[_Dio_PortRead_Ptr]
 718  005a c401              	andb	#1
 719  005c 6b80              	stab	OFST-3,s
 720                         ; 690 	if (((CSWM_config_c & CSWM_4HS_VARIANT_MASK_K) == CSWM_4HS_VARIANT_MASK_K) && 
 720                         ; 691 		((main_SwReq_Rear_c == VEH_SWITCH_REAR_ONLY_K) || (diag_eol_io_lock_b == TRUE)) && 
 720                         ; 692 		(hs_LED_U12S_stat_c == STD_LOW) )
 722  005e 1e00e80f02        	brset	_CSWM_config_c,15,LC002
 723  0063 201a              	bra	L363
 724  0065                   LC002:
 726  0065 f60000            	ldab	_main_SwReq_Rear_c
 727  0068 c103              	cmpb	#3
 728  006a 2705              	beq	L563
 730  006c 1f0000010e        	brclr	_diag_io_lock3_flags_c,1,L363
 731  0071                   L563:
 733  0071 e680              	ldab	OFST-3,s
 734  0073 260a              	bne	L363
 735                         ; 695 			Dio_WriteChannel(LED_U12S_EN, STD_HIGH);
 738  0075 1410              	sei	
 743  0077 fe0000            	ldx	_Dio_PortWrite_Ptr
 744  007a 0c0001            	bset	0,x,1
 748  007d 10ef              	cli	
 751  007f                   L363:
 752                         ; 701 	if ( (canio_RX_IGN_STATUS_c == IGN_RUN) || (canio_RX_IGN_STATUS_c == IGN_START) )
 754  007f f60000            	ldab	_canio_RX_IGN_STATUS_c
 755  0082 c104              	cmpb	#4
 756  0084 2706              	beq	L173
 758  0086 c105              	cmpb	#5
 759  0088 182601e3          	bne	L763
 760  008c                   L173:
 761                         ; 703 		for (hs_nr = FRONT_LEFT; hs_nr < ALL_HEAT_SEATS; hs_nr++)
 763  008c c7                	clrb	
 764  008d 6b80              	stab	OFST-3,s
 765  008f                   L373:
 766                         ; 706 			NTC_AD_Readings_c[hs_nr] = TempF_Get_NTC_Rdg(hs_nr);
 768  008f 87                	clra	
 769  0090 3b                	pshd	
 770  0091 e682              	ldab	OFST-1,s
 771  0093 4a000000          	call	f_TempF_Get_NTC_Rdg
 773  0097 31                	puly	
 774  0098 6bea00e4          	stab	_NTC_AD_Readings_c,y
 775                         ; 711 			if (temperature_HS_NTC_status[hs_nr] != NTC_STAT_UNDEFINED)
 777  009c e680              	ldab	OFST-3,s
 778  009e 87                	clra	
 779  009f b746              	tfr	d,y
 780  00a1 e7ea0000          	tst	_temperature_HS_NTC_status,y
 781  00a5 18270185          	beq	L574
 782                         ; 719 				if ((!diag_io_hs_lock_flags_c[hs_nr].b.switch_rq_lock_b) && (!diag_io_all_hs_lock_b) && (diag_rc_all_op_lock_b == FALSE) )
 784  00a9 0eea00000110      	brset	_diag_io_hs_lock_flags_c,y,1,L304
 786  00af 1e0000100b        	brset	_diag_io_lock2_flags_c,16,L304
 788  00b4 1e00000106        	brset	_diag_rc_lock_flags_c,1,L304
 789                         ; 722 						hsLF_Catch_Signals (hs_nr);
 791  00b9 4a042f2f          	call	f_hsLF_Catch_Signals
 794  00bd 2005              	bra	L504
 795  00bf                   L304:
 796                         ; 727 						hsLF_Process_DiagIO_RC_Outputs (hs_nr);
 798  00bf 87                	clra	
 799  00c0 4a027575          	call	f_hsLF_Process_DiagIO_RC_Outputs
 801  00c4                   L504:
 802                         ; 733 				hsLF_Process_DiagIO_LEDStatus (hs_nr);
 804  00c4 e680              	ldab	OFST-3,s
 805  00c6 87                	clra	
 806  00c7 4a03b3b3          	call	f_hsLF_Process_DiagIO_LEDStatus
 808                         ; 739 				if (main_state_c == NORMAL)
 810  00cb f60000            	ldab	_main_state_c
 811  00ce 2672              	bne	L704
 812                         ; 742 					if (hs_control_flags_c[ hs_nr ].b.remote_user_b)
 814  00d0 e680              	ldab	OFST-3,s
 815  00d2 87                	clra	
 816  00d3 b746              	tfr	d,y
 817  00d5 0fea011d4005      	brclr	_hs_control_flags_c,y,64,L314
 818                         ; 762 						hs_control_flags_c[ hs_nr ].b.remote_user_b = FALSE;
 820  00db 0dea011d40        	bclr	_hs_control_flags_c,y,64
 822  00e0                   L314:
 823                         ; 776 					if (((hs_nr == REAR_LEFT)||(hs_nr == REAR_RIGHT)) && (main_SwReq_Rear_c == VEH_SWITCH_REAR_ONLY_K) && (canio_RX_IgnRun_RemSt_c == FALSE))
 825  00e0 c102              	cmpb	#2
 826  00e2 2704              	beq	L714
 828  00e4 c103              	cmpb	#3
 829  00e6 263c              	bne	L514
 830  00e8                   L714:
 832  00e8 f60000            	ldab	_main_SwReq_Rear_c
 833  00eb c103              	cmpb	#3
 834  00ed 2635              	bne	L514
 836  00ef f60000            	ldab	_canio_RX_IgnRun_RemSt_c
 837  00f2 2630              	bne	L514
 838                         ; 783 						if(hs_nr == REAR_LEFT && 
 838                         ; 784 						   hs_rear_seat_flags_c[ RL_SWITCH ].b.stksw_psd == FALSE &&
 838                         ; 785 						   d_AllDtc_a[REAR_LEFT_STUCK_INDEX].DtcState.state.TestFailed == FALSE)
 840  00f4 e680              	ldab	OFST-3,s
 841  00f6 c102              	cmpb	#2
 842  00f8 2613              	bne	L124
 844  00fa 1e010f010e        	brset	_hs_rear_seat_flags_c,1,L124
 846  00ff 1e00510109        	brset	_d_AllDtc_a+81,1,L124
 847                         ; 788 							hsLF_Process_Switch_Requests_ECU (REAR_LEFT);
 849  0104 cc0002            	ldd	#2
 850  0107 4a060d0d          	call	f_hsLF_Process_Switch_Requests_ECU
 852  010b e680              	ldab	OFST-3,s
 853  010d                   L124:
 854                         ; 791 						if(hs_nr == REAR_RIGHT && 
 854                         ; 792 						   hs_rear_seat_flags_c[ RR_SWITCH ].b.stksw_psd == FALSE &&
 854                         ; 793 						   d_AllDtc_a[REAR_RIGHT_STUCK_INDEX].DtcState.state.TestFailed == FALSE)
 856  010d c103              	cmpb	#3
 857  010f 2635              	bne	L734
 859  0111 1e01100130        	brset	_hs_rear_seat_flags_c+1,1,L734
 861  0116 1e0053012b        	brset	_d_AllDtc_a+83,1,L734
 862                         ; 796 							hsLF_Process_Switch_Requests_ECU (REAR_RIGHT);
 864  011b cc0003            	ldd	#3
 865  011e 4a060d0d          	call	f_hsLF_Process_Switch_Requests_ECU
 867  0122 2022              	bra	L734
 868  0124                   L514:
 869                         ; 806 					  	if(((hs_nr==  FRONT_LEFT) || (hs_nr==FRONT_RIGHT)) ||  (((hs_nr == REAR_LEFT)||(hs_nr == REAR_RIGHT))&& (canio_RX_IgnRun_RemSt_c == FALSE)))
 871  0124 e680              	ldab	OFST-3,s
 872  0126 2713              	beq	L134
 874  0128 c101              	cmpb	#1
 875  012a 270f              	beq	L134
 877  012c c102              	cmpb	#2
 878  012e 2704              	beq	L334
 880  0130 c103              	cmpb	#3
 881  0132 2612              	bne	L734
 882  0134                   L334:
 884  0134 f60000            	ldab	_canio_RX_IgnRun_RemSt_c
 885  0137 260d              	bne	L734
 886  0139 e680              	ldab	OFST-3,s
 887  013b                   L134:
 888                         ; 809 							hsLF_Process_Switch_Requests_Bus (hs_nr);
 890  013b 87                	clra	
 891  013c 4a056767          	call	f_hsLF_Process_Switch_Requests_Bus
 894  0140 2004              	bra	L734
 895  0142                   L704:
 896                         ; 820 					hsLF_RemoteStart_Control();
 898  0142 4a068787          	call	f_hsLF_RemoteStart_Control
 900  0146                   L734:
 901                         ; 826 				if ((hs_nr == REAR_LEFT)||(hs_nr == REAR_RIGHT))
 903  0146 e680              	ldab	OFST-3,s
 904  0148 c102              	cmpb	#2
 905  014a 2704              	beq	L344
 907  014c c103              	cmpb	#3
 908  014e 262d              	bne	L144
 909  0150                   L344:
 910                         ; 828 					hs_vehicle_line_offset_c= VEH_LINE_REAROFFSET;	
 912  0150 c601              	ldab	#1
 913  0152 7b0018            	stab	_hs_vehicle_line_offset_c
 915  0155 e680              	ldab	OFST-3,s
 916  0157                   L544:
 917                         ; 836 				if (((NTC_AD_Readings_c[hs_nr] != 0) && (NTC_AD_Readings_c[hs_nr] < /*hs_NTC_feedback_values_c[4]*/hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].NTC_CutOFF_c[4])) ||
 917                         ; 837 			    /*(NTC_Supply_Voltage_Flt_b) ||*/
 917                         ; 838 			    (temperature_HS_NTC_status[hs_nr]!= NTC_STAT_GOOD)) {
 919  0157 87                	clra	
 920  0158 b746              	tfr	d,y
 921  015a e6ea00e4          	ldab	_NTC_AD_Readings_c,y
 922  015e 2722              	beq	L354
 924  0160 f60018            	ldab	_hs_vehicle_line_offset_c
 925  0163 fb010d            	addb	_hs_seat_config_c
 926  0166 45                	rola	
 927  0167 cd000e            	ldy	#14
 928  016a 13                	emul	
 929  016b b746              	tfr	d,y
 930  016d e680              	ldab	OFST-3,s
 931  016f b795              	exg	b,x
 932  0171 e6e200e4          	ldab	_NTC_AD_Readings_c,x
 933  0175 e1ea0083          	cmpb	_hs_cal_prms+12,y
 934  0179 2516              	blo	L154
 935  017b 2005              	bra	L354
 936  017d                   L144:
 937                         ; 832 					hs_vehicle_line_offset_c= VEH_LINE_FRONTOFFSET;
 939  017d 790018            	clr	_hs_vehicle_line_offset_c
 940  0180 20d5              	bra	L544
 941  0182                   L354:
 942                         ; 836 				if (((NTC_AD_Readings_c[hs_nr] != 0) && (NTC_AD_Readings_c[hs_nr] < /*hs_NTC_feedback_values_c[4]*/hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].NTC_CutOFF_c[4])) ||
 942                         ; 837 			    /*(NTC_Supply_Voltage_Flt_b) ||*/
 942                         ; 838 			    (temperature_HS_NTC_status[hs_nr]!= NTC_STAT_GOOD)) {
 943  0182 e680              	ldab	OFST-3,s
 944  0184 87                	clra	
 945  0185 b746              	tfr	d,y
 946  0187 e6ea0000          	ldab	_temperature_HS_NTC_status,y
 947  018b c103              	cmpb	#3
 948  018d 18270090          	beq	L744
 949  0191                   L154:
 950                         ; 841 				TempOdo = FMemLibF_GetOdometer();
 952  0191 4a000000          	call	f_FMemLibF_GetOdometer
 954  0195 6c81              	std	OFST-2,s
 955                         ; 845 				if (hs_control_flags_c[ hs_nr ].b.ntc_failure_first_time_b == FALSE) {
 957  0197 e680              	ldab	OFST-3,s
 958  0199 87                	clra	
 959  019a b746              	tfr	d,y
 960  019c e6ea011d          	ldab	_hs_control_flags_c,y
 961  01a0 c580              	bitb	#128
 962  01a2 18260088          	bne	L574
 963                         ; 848 					hs_control_flags_c[ hs_nr ].b.ntc_greater_70c_b = TRUE;
 965  01a6 0cea011d10        	bset	_hs_control_flags_c,y,16
 966                         ; 853 					if((NTC_AD_Readings_c[hs_nr] != 0) && (NTC_AD_Readings_c[hs_nr] < hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].NTC_CutOFF_c[4]))
 968  01ab e6ea00e4          	ldab	_NTC_AD_Readings_c,y
 969  01af 2758              	beq	L754
 971  01b1 f60018            	ldab	_hs_vehicle_line_offset_c
 972  01b4 fb010d            	addb	_hs_seat_config_c
 973  01b7 45                	rola	
 974  01b8 cd000e            	ldy	#14
 975  01bb 13                	emul	
 976  01bc b746              	tfr	d,y
 977  01be e680              	ldab	OFST-3,s
 978  01c0 b795              	exg	b,x
 979  01c2 e6e200e4          	ldab	_NTC_AD_Readings_c,x
 980  01c6 e1ea0083          	cmpb	_hs_cal_prms+12,y
 981  01ca 243d              	bhs	L754
 982                         ; 856 						EE_BlockRead(EE_HEAT_NTC_FAILSAFE, (u_8Bit*)&hs_NTC_FailSafe_prms);
 984  01cc cc0072            	ldd	#_hs_NTC_FailSafe_prms
 985  01cf 3b                	pshd	
 986  01d0 cc0016            	ldd	#22
 987  01d3 4a000000          	call	f_EE_BlockRead
 989  01d7 1b82              	leas	2,s
 990                         ; 859 						if(hs_ntc_updated_b == FALSE)
 992  01d9 1e0019012b        	brset	_hs_temp_set_c,1,L754
 993                         ; 862 							if(hs_NTC_FailSafe_prms.First_odostamp_w == 0xFFFF)
 995  01de fc0072            	ldd	_hs_NTC_FailSafe_prms
 996  01e1 04a405            	ibne	d,L364
 997                         ; 865 								hs_NTC_FailSafe_prms.First_odostamp_w = TempOdo;
 999  01e4 1805810072        	movw	OFST-2,s,_hs_NTC_FailSafe_prms
1000  01e9                   L364:
1001                         ; 869 							hs_NTC_FailSafe_prms.recent_odostamp_w = TempOdo;
1003  01e9 1805810074        	movw	OFST-2,s,_hs_NTC_FailSafe_prms+2
1004                         ; 872 							if(hs_NTC_FailSafe_prms.failure_counter_c != 254)
1006  01ee f60076            	ldab	_hs_NTC_FailSafe_prms+4
1007  01f1 c1fe              	cmpb	#254
1008  01f3 2703              	beq	L564
1009                         ; 874 								hs_NTC_FailSafe_prms.failure_counter_c++;
1011  01f5 720076            	inc	_hs_NTC_FailSafe_prms+4
1012  01f8                   L564:
1013                         ; 878 							EE_BlockWrite(EE_HEAT_NTC_FAILSAFE, (u_8Bit*)&hs_NTC_FailSafe_prms);
1015  01f8 cc0072            	ldd	#_hs_NTC_FailSafe_prms
1016  01fb 3b                	pshd	
1017  01fc cc0016            	ldd	#22
1018  01ff 4a000000          	call	f_EE_BlockWrite
1020  0203 1b82              	leas	2,s
1021                         ; 881 							hs_ntc_updated_b = TRUE;
1023  0205 1c001901          	bset	_hs_temp_set_c,1
1024  0209                   L754:
1025                         ; 889 					if(hs_rem_seat_req_c[hs_nr] != SET_OFF_REQUEST)
1027  0209 e680              	ldab	OFST-3,s
1028  020b b796              	exg	b,y
1029  020d e6ea00fd          	ldab	_hs_rem_seat_req_c,y
1030  0211 2707              	beq	L764
1031                         ; 892 					hs_monitor_2sec_led_counter_c[hs_nr] = /*mature_2sec_time_c*/hs_flt_dtc_c[HS_2SEC_MATURE_TIME];
1033  0213 1809ea00f900cf    	movb	_hs_flt_dtc_c+4,_hs_monitor_2sec_led_counter_c,y
1034  021a                   L764:
1035                         ; 896 					hs_control_flags_c[ hs_nr ].b.ntc_failure_first_time_b = TRUE;
1037  021a 0cea011d80        	bset	_hs_control_flags_c,y,128
1039  021f 200d              	bra	L574
1040  0221                   L744:
1041                         ; 907 				hs_control_flags_c[ hs_nr ].b.ntc_greater_70c_b = FALSE;
1043  0221 e680              	ldab	OFST-3,s
1044  0223 b746              	tfr	d,y
1045                         ; 910 				hs_control_flags_c[ hs_nr ].b.ntc_failure_first_time_b = FALSE;
1047  0225 0dea011d90        	bclr	_hs_control_flags_c,y,144
1048                         ; 912 				hs_ntc_updated_b = FALSE;
1050  022a 1d001901          	bclr	_hs_temp_set_c,1
1051  022e                   L574:
1052                         ; 703 		for (hs_nr = FRONT_LEFT; hs_nr < ALL_HEAT_SEATS; hs_nr++)
1054  022e 6280              	inc	OFST-3,s
1057  0230 e680              	ldab	OFST-3,s
1058  0232 c104              	cmpb	#4
1059  0234 1825fe57          	blo	L373
1060                         ; 929             hsLF_diag_pid_flag = FALSE; 
1062  0238 c7                	clrb	
1063  0239 7b0067            	stab	_hsLF_diag_pid_flag
1064                         ; 932             for (hs_nr = FRONT_LEFT; hs_nr < ALL_HEAT_SEATS; hs_nr++)
1066  023c 6b80              	stab	OFST-3,s
1067  023e                   L774:
1068                         ; 935               if(SWAPPED_NTC_AD_READING[ hs_nr ] < hs_pid_setpoint[ hs_nr ])        /* Make Diagnostics active while pwm is > 0%*/ 
1070  023e 87                	clra	
1071  023f b746              	tfr	d,y
1072  0241 b745              	tfr	d,x
1073  0243 e6e2006a          	ldab	_SWAPPED_NTC_AD_READING,x
1074  0247 e1ea006e          	cmpb	_hs_pid_setpoint,y
1075  024b 2405              	bhs	L505
1076                         ; 937                      hsLF_diag_pid_flag = TRUE;
1078  024d c601              	ldab	#1
1079  024f 7b0067            	stab	_hsLF_diag_pid_flag
1080  0252                   L505:
1081                         ; 932             for (hs_nr = FRONT_LEFT; hs_nr < ALL_HEAT_SEATS; hs_nr++)
1083  0252 6280              	inc	OFST-3,s
1086  0254 e680              	ldab	OFST-3,s
1087  0256 c104              	cmpb	#4
1088  0258 25e4              	blo	L774
1089                         ; 942               if(hsLF_diag_pid_flag == TRUE)
1091  025a f60067            	ldab	_hsLF_diag_pid_flag
1092  025d 042104            	dbne	b,L705
1093                         ; 944                   hsLF_HeatDiagnostics();
1095  0260 4a087070          	call	f_hsLF_HeatDiagnostics
1097  0264                   L705:
1098                         ; 953 		hsLF_2SecLEDDisplay();
1100  0264 4a0a5555          	call	f_hsLF_2SecLEDDisplay
1103  0268                   L115:
1104                         ; 962     pid_heat_setting();
1106  0268 4a1ceeee          	call	f_pid_heat_setting
1108                         ; 964 } //End Of Heat Manager
1111  026c 1b83              	leas	3,s
1112  026e 0a                	rtc	
1113  026f                   L763:
1114                         ; 959 		hsLF_ClearHeatSequence();
1116  026f 4a0b5a5a          	call	f_hsLF_ClearHeatSequence
1118  0273 20f3              	bra	L115
1159                         ; 1030 void hsLF_Process_DiagIO_RC_Outputs(unsigned char hs_seats)
1159                         ; 1031 {
1160                         	switch	.ftext
1161  0275                   f_hsLF_Process_DiagIO_RC_Outputs:
1163  0275 3b                	pshd	
1164       00000000          OFST:	set	0
1167                         ; 1033 	if ((diag_io_hs_lock_flags_c[hs_seats].b.switch_rq_lock_b == TRUE) || (diag_io_all_hs_lock_b == TRUE) || (diag_rc_all_heaters_lock_b == TRUE))
1169  0276 87                	clra	
1170  0277 b746              	tfr	d,y
1171  0279 0eea0000010e      	brset	_diag_io_hs_lock_flags_c,y,1,L135
1173  027f 1e00001009        	brset	_diag_io_lock2_flags_c,16,L135
1175  0284 f60000            	ldab	_diag_rc_lock_flags_c
1176  0287 c502              	bitb	#2
1177  0289 182700df          	beq	L725
1178  028d                   L135:
1179                         ; 1040 		if(diag_rc_all_heaters_lock_b == FALSE)
1181  028d e681              	ldab	OFST+1,s
1182  028f 87                	clra	
1183  0290 b746              	tfr	d,y
1184  0292 1e00000217        	brset	_diag_rc_lock_flags_c,2,L535
1185                         ; 1044 			hs_control_flags_c [hs_seats].b.hs_io_rq_control_b = TRUE;
1187  0297 0cea011d04        	bset	_hs_control_flags_c,y,4
1188                         ; 1047 			hs_IO_seat_req_c[ hs_seats ] = diag_io_hs_rq_c [hs_seats];
1190  029c 180aea0000ea005b  	movb	_diag_io_hs_rq_c,y,_hs_IO_seat_req_c,y
1191                         ; 1050 			diag_io_hs_rq_c[hs_seats] = 7;
1193  02a4 c607              	ldab	#7
1194  02a6 6bea0000          	stab	_diag_io_hs_rq_c,y
1196  02aa e681              	ldab	OFST+1,s
1197  02ac 2005              	bra	L735
1198  02ae                   L535:
1199                         ; 1057 			hs_status3_flags_c [hs_seats].b.self_tests_active_b = TRUE;
1201  02ae 0cea011120        	bset	_hs_status3_flags_c,y,32
1202  02b3                   L735:
1203                         ; 1062 		hs_control_flags_c[ hs_seats ].b.hs_can_msg_flow_ctrl_b = TRUE;
1205  02b3 87                	clra	
1206  02b4 b746              	tfr	d,y
1207  02b6 0cea011d02        	bset	_hs_control_flags_c,y,2
1208                         ; 1065 		if ((hs_IO_seat_req_c[ hs_seats ] == HS_DIAG_IO_RQ_HL3) || (diag_rc_all_heaters_lock_b == TRUE) )
1210  02bb e6ea005b          	ldab	_hs_IO_seat_req_c,y
1211  02bf c104              	cmpb	#4
1212  02c1 2705              	beq	L345
1214  02c3 1f00000253        	brclr	_diag_rc_lock_flags_c,2,L145
1215  02c8                   L345:
1216                         ; 1070 			if ( ((hs_status3_flags_c [hs_seats].b.tests_started_b == FALSE) && (diag_rc_all_heaters_lock_b == TRUE)) ||
1216                         ; 1071 				(hs_IO_seat_req_c[ hs_seats ] == HS_DIAG_IO_RQ_HL3) )
1218  02c8 0eea01114005      	brset	_hs_status3_flags_c,y,64,L155
1220  02ce 1e00000204        	brset	_diag_rc_lock_flags_c,2,L745
1221  02d3                   L155:
1223  02d3 182600da          	bne	L506
1224  02d7                   L745:
1225                         ; 1074 				if(hs_seats == FRONT_LEFT || hs_seats == FRONT_RIGHT)
1227  02d7 e681              	ldab	OFST+1,s
1228  02d9 2703              	beq	L555
1230  02db 042119            	dbne	b,L355
1231  02de                   L555:
1232                         ; 1077 					hsLF_UpdateHeatParameters(hs_seats, HEAT_LEVEL_HL3);
1234  02de cc0003            	ldd	#3
1235  02e1 3b                	pshd	
1236  02e2 e683              	ldab	OFST+3,s
1237  02e4 4a0d5050          	call	f_hsLF_UpdateHeatParameters
1239  02e8 1b82              	leas	2,s
1240                         ; 1079 					hs_status3_flags_c [hs_seats].b.tests_started_b = TRUE;
1242  02ea e681              	ldab	OFST+1,s
1243  02ec b796              	exg	b,y
1244  02ee 0cea011140        	bset	_hs_status3_flags_c,y,64
1246  02f3 182000ba          	bra	L506
1247  02f7                   L355:
1248                         ; 1084 					if((CSWM_config_c & CSWM_4HS_VARIANT_MASK_K) == CSWM_4HS_VARIANT_MASK_K)
1250  02f7 f600e8            	ldab	_CSWM_config_c
1251  02fa c40f              	andb	#15
1252  02fc c10f              	cmpb	#15
1253  02fe 182600af          	bne	L506
1254                         ; 1087 						hsLF_UpdateHeatParameters(hs_seats, HEAT_LEVEL_HL3);
1256  0302 cc0003            	ldd	#3
1257  0305 3b                	pshd	
1258  0306 e683              	ldab	OFST+3,s
1259  0308 4a0d5050          	call	f_hsLF_UpdateHeatParameters
1261  030c 1b82              	leas	2,s
1262                         ; 1089 						hs_status3_flags_c [hs_seats].b.tests_started_b = TRUE;
1264  030e e681              	ldab	OFST+1,s
1265  0310 b796              	exg	b,y
1266  0312 0cea011140        	bset	_hs_status3_flags_c,y,64
1267  0317 18200096          	bra	L506
1268  031b                   L145:
1269                         ; 1098 			if (hs_IO_seat_req_c[ hs_seats ] == HS_DIAG_IO_RQ_HL2)
1271  031b e681              	ldab	OFST+1,s
1272  031d b746              	tfr	d,y
1273  031f e6ea005b          	ldab	_hs_IO_seat_req_c,y
1274  0323 c103              	cmpb	#3
1275  0325 2605              	bne	L565
1276                         ; 1101 				hsLF_UpdateHeatParameters(hs_seats, HEAT_LEVEL_HL2);
1278  0327 cc0002            	ldd	#2
1281  032a 201c              	bra	LC004
1282  032c                   L565:
1283                         ; 1106 				if (hs_IO_seat_req_c[ hs_seats ] == HS_DIAG_IO_RQ_HL1)
1285  032c e681              	ldab	OFST+1,s
1286  032e b746              	tfr	d,y
1287  0330 e6ea005b          	ldab	_hs_IO_seat_req_c,y
1288  0334 c102              	cmpb	#2
1289  0336 2605              	bne	L175
1290                         ; 1109 					hsLF_UpdateHeatParameters(hs_seats, HEAT_LEVEL_HL1);
1292  0338 cc0001            	ldd	#1
1295  033b 200b              	bra	LC004
1296  033d                   L175:
1297                         ; 1114 					if (hs_IO_seat_req_c[ hs_seats ] == HS_DIAG_IO_RQ_LL1)
1299  033d e681              	ldab	OFST+1,s
1300  033f b746              	tfr	d,y
1301  0341 e6ea005b          	ldab	_hs_IO_seat_req_c,y
1302  0345 04210b            	dbne	b,L575
1303                         ; 1117 						hsLF_UpdateHeatParameters(hs_seats, HEAT_LEVEL_LL1);
1306  0348                   LC004:
1307  0348 3b                	pshd	
1308  0349 e683              	ldab	OFST+3,s
1309  034b 4a0d5050          	call	f_hsLF_UpdateHeatParameters
1310  034f 1b82              	leas	2,s
1312  0351 205e              	bra	L506
1313  0353                   L575:
1314                         ; 1122 						if (hs_IO_seat_req_c[ hs_seats ] == HS_DIAG_IO_RQ_OFF)
1316  0353 e681              	ldab	OFST+1,s
1317  0355 b746              	tfr	d,y
1318  0357 e6ea005b          	ldab	_hs_IO_seat_req_c,y
1319  035b 2654              	bne	L506
1320                         ; 1125 							hsLF_UpdateHeatParameters(hs_seats, HEAT_LEVEL_OFF);
1322  035d c607              	ldab	#7
1323  035f 3b                	pshd	
1324  0360 e683              	ldab	OFST+3,s
1325  0362 4a0d5050          	call	f_hsLF_UpdateHeatParameters
1327  0366 1b82              	leas	2,s
1328                         ; 1127 							hs_status3_flags_c [hs_seats].b.tests_started_b = FALSE;
1330  0368 e681              	ldab	OFST+1,s
1332  036a 203e              	bra	L516
1333  036c                   L725:
1334                         ; 1143 		if( (hs_control_flags_c [hs_seats].b.hs_io_rq_control_b == TRUE) || (hs_status3_flags_c [hs_seats].b.self_tests_active_b == TRUE) )
1336  036c e681              	ldab	OFST+1,s
1337  036e b746              	tfr	d,y
1338  0370 0eea011d0406      	brset	_hs_control_flags_c,y,4,L116
1340  0376 0fea01112035      	brclr	_hs_status3_flags_c,y,32,L506
1341  037c                   L116:
1342                         ; 1149 			hs_control_flags_c[ hs_seats ].b.hs_can_msg_flow_ctrl_b = FALSE;
1344  037c 0dea011d02        	bclr	_hs_control_flags_c,y,2
1345                         ; 1152 			hsLF_UpdateHeatParameters(hs_seats, HEAT_LEVEL_OFF);
1347  0381 cc0007            	ldd	#7
1348  0384 3b                	pshd	
1349  0385 e683              	ldab	OFST+3,s
1350  0387 4a0d5050          	call	f_hsLF_UpdateHeatParameters
1352  038b 1b82              	leas	2,s
1353                         ; 1154 			if(hs_control_flags_c [hs_seats].b.hs_io_rq_control_b == TRUE)
1355  038d e681              	ldab	OFST+1,s
1356  038f 87                	clra	
1357  0390 b746              	tfr	d,y
1358  0392 0fea011d040b      	brclr	_hs_control_flags_c,y,4,L316
1359                         ; 1157 				hs_control_flags_c [hs_seats].b.hs_io_rq_control_b = FALSE;
1361  0398 0dea011d04        	bclr	_hs_control_flags_c,y,4
1362                         ; 1160 				hs_IO_seat_req_c[ hs_seats ] = 0;
1364  039d 69ea005b          	clr	_hs_IO_seat_req_c,y
1366  03a1 2007              	bra	L516
1367  03a3                   L316:
1368                         ; 1166 				hs_status3_flags_c [hs_seats].b.self_tests_active_b = FALSE;
1370  03a3 b746              	tfr	d,y
1371  03a5 0dea011120        	bclr	_hs_status3_flags_c,y,32
1372  03aa                   L516:
1373                         ; 1171 			hs_status3_flags_c [hs_seats].b.tests_started_b = FALSE;
1375  03aa b796              	exg	b,y
1376  03ac 0dea011140        	bclr	_hs_status3_flags_c,y,64
1377  03b1                   L506:
1378                         ; 1176 } //End of Diag IO processing for Outputs
1381  03b1 31                	puly	
1382  03b2 0a                	rtc	
1420                         ; 1201 void hsLF_Process_DiagIO_LEDStatus (unsigned char hs_seat)
1420                         ; 1202 {
1421                         	switch	.ftext
1422  03b3                   f_hsLF_Process_DiagIO_LEDStatus:
1424  03b3 3b                	pshd	
1425       00000000          OFST:	set	0
1428                         ; 1205 	if (!diag_io_hs_lock_flags_c[hs_seat].b.led_status_lock_b)
1430  03b4 87                	clra	
1431  03b5 b746              	tfr	d,y
1432  03b7 0eea0000021d      	brset	_diag_io_hs_lock_flags_c,y,2,L336
1433                         ; 1209 		if (!hs_control_flags_c [hs_seat].b.hs_io_stat_control_b)
1435  03bd e6ea011d          	ldab	_hs_control_flags_c,y
1436  03c1 c508              	bitb	#8
1437  03c3 2768              	beq	L746
1439                         ; 1220 			if (((hs_seat == REAR_LEFT)||(hs_seat == REAR_RIGHT)) && (main_SwReq_Rear_c == VEH_SWITCH_REAR_ONLY_K))
1443                         ; 1225 				hsLF_SendHeatedSeatCanStatus(hs_seat, CAN_LED_OFF);
1447                         ; 1231 				hsLF_SendHeatedSeatCanStatus(hs_seat, CAN_LED_OFF);
1450  03c5 c7                	clrb	
1451  03c6 3b                	pshd	
1452  03c7 e683              	ldab	OFST+3,s
1453  03c9 4a0c0909          	call	f_hsLF_SendHeatedSeatCanStatus
1454  03cd 1b82              	leas	2,s
1455                         ; 1235 			hs_control_flags_c [hs_seat].b.hs_io_stat_control_b = FALSE;
1457  03cf e681              	ldab	OFST+1,s
1458  03d1 b796              	exg	b,y
1459                         ; 1239 			hs_control_flags_c[ hs_seat ].b.hs_can_msg_flow_ctrl_b = FALSE;
1461  03d3 0dea011d0a        	bclr	_hs_control_flags_c,y,10
1462  03d8 2053              	bra	L746
1463  03da                   L336:
1464                         ; 1247 		hs_control_flags_c [hs_seat].b.hs_io_stat_control_b = TRUE;
1466  03da e681              	ldab	OFST+1,s
1467  03dc b746              	tfr	d,y
1468  03de 0cea011d08        	bset	_hs_control_flags_c,y,8
1469                         ; 1251 		hs_IO_seat_stat_c [hs_seat] = diag_io_hs_state_c [hs_seat];
1471  03e3 e6ea0000          	ldab	_diag_io_hs_state_c,y
1472  03e7 6bea0057          	stab	_hs_IO_seat_stat_c,y
1473                         ; 1253 		if (hs_IO_seat_stat_c [hs_seat] == HS_DIAG_IO_STATUS_HI)
1475  03eb c103              	cmpb	#3
1476  03ed 261b              	bne	L156
1477                         ; 1256 			if (((hs_seat == REAR_LEFT)||(hs_seat == REAR_RIGHT)) && (main_SwReq_Rear_c == VEH_SWITCH_REAR_ONLY_K))
1479  03ef e681              	ldab	OFST+1,s
1480  03f1 c102              	cmpb	#2
1481  03f3 2704              	beq	L556
1483  03f5 c103              	cmpb	#3
1484  03f7 260c              	bne	L356
1485  03f9                   L556:
1487  03f9 f60000            	ldab	_main_SwReq_Rear_c
1488  03fc c103              	cmpb	#3
1489  03fe 2605              	bne	L356
1490                         ; 1261 				hsLF_SendHeatedSeatCanStatus(hs_seat, CAN_LED_HI);
1492  0400 cc0003            	ldd	#3
1495  0403 2016              	bra	L166
1496  0405                   L356:
1497                         ; 1267 				hsLF_SendHeatedSeatCanStatus(hs_seat, CAN_LED_HI);
1499  0405 cc0003            	ldd	#3
1501  0408 2011              	bra	L166
1502  040a                   L156:
1503                         ; 1273 			if (hs_IO_seat_stat_c [hs_seat] == HS_DIAG_IO_STATUS_LOW)
1505  040a e681              	ldab	OFST+1,s
1506  040c b746              	tfr	d,y
1507  040e e6ea0057          	ldab	_hs_IO_seat_stat_c,y
1508  0412 042105            	dbne	b,L366
1509                         ; 1276 				if (((hs_seat == REAR_LEFT)||(hs_seat == REAR_RIGHT)) && (main_SwReq_Rear_c == VEH_SWITCH_REAR_ONLY_K))
1513                         ; 1281 					hsLF_SendHeatedSeatCanStatus(hs_seat, CAN_LED_LOW);
1517                         ; 1286 					hsLF_SendHeatedSeatCanStatus(hs_seat, CAN_LED_LOW);
1519  0415 cc0001            	ldd	#1
1521  0418 2001              	bra	L166
1522  041a                   L366:
1523                         ; 1292 				if (((hs_seat == REAR_LEFT)||(hs_seat == REAR_RIGHT)) && (main_SwReq_Rear_c == VEH_SWITCH_REAR_ONLY_K))
1527                         ; 1297 					hsLF_SendHeatedSeatCanStatus(hs_seat, CAN_LED_OFF);
1531                         ; 1303 					hsLF_SendHeatedSeatCanStatus(hs_seat, CAN_LED_OFF);
1533  041a c7                	clrb	
1535  041b                   L166:
1536  041b 3b                	pshd	
1537  041c e683              	ldab	OFST+3,s
1538  041e 4a0c0909          	call	f_hsLF_SendHeatedSeatCanStatus
1539  0422 1b82              	leas	2,s
1540                         ; 1309 		hs_control_flags_c[ hs_seat ].b.hs_can_msg_flow_ctrl_b = TRUE;
1542  0424 e681              	ldab	OFST+1,s
1543  0426 b796              	exg	b,y
1544  0428 0cea011d02        	bset	_hs_control_flags_c,y,2
1545  042d                   L746:
1546                         ; 1313 }
1549  042d 31                	puly	
1550  042e 0a                	rtc	
1591                         ; 1328 void hsLF_Catch_Signals(unsigned char hs_side)
1591                         ; 1329 {
1592                         	switch	.ftext
1593  042f                   f_hsLF_Catch_Signals:
1595  042f 3b                	pshd	
1596       00000000          OFST:	set	0
1599                         ; 1332 	if ((hs_control_flags_c [hs_side].b.hs_io_rq_control_b == FALSE) && (hs_status3_flags_c [hs_side].b.self_tests_active_b == FALSE))
1601  0430 87                	clra	
1602  0431 b746              	tfr	d,y
1603  0433 0eea011d0450      	brset	_hs_control_flags_c,y,4,L137
1605  0439 0eea0111204a      	brset	_hs_status3_flags_c,y,32,L137
1606                         ; 1334 		switch (hs_side) {
1608  043f e681              	ldab	OFST+1,s
1610  0441 270d              	beq	L307
1611  0443 04011e            	dbeq	b,L507
1612  0446 04012f            	dbeq	b,L707
1613  0449 04012c            	dbeq	b,L707
1614  044c 1820008f          	bra	L357
1615  0450                   L307:
1616                         ; 1339 				if (!diag_io_vs_lock_flags_c[hs_side].b.switch_rq_lock_b)
1618  0450 b746              	tfr	d,y
1619  0452 e6ea0000          	ldab	_diag_io_vs_lock_flags_c,y
1620  0456 c501              	bitb	#1
1621  0458 18260083          	bne	L357
1622                         ; 1343 					hs_temp_seat_req_fl_c = canio_RX_FL_HS_RQ_TGW_c;
1624  045c 180c0000003f      	movb	_canio_hvac_heated_seat_request_c,_hs_temp_seat_req_c
1626  0462 207b              	bra	L357
1627  0464                   L507:
1628                         ; 1380 				if (!diag_io_vs_lock_flags_c[hs_side].b.switch_rq_lock_b)
1630  0464 e681              	ldab	OFST+1,s
1631  0466 b746              	tfr	d,y
1632  0468 e6ea0000          	ldab	_diag_io_vs_lock_flags_c,y
1633  046c c501              	bitb	#1
1634  046e 266f              	bne	L357
1635                         ; 1384 					hs_temp_seat_req_fr_c = canio_RX_FR_HS_RQ_TGW_c;
1637  0470 180c00010040      	movb	_canio_hvac_heated_seat_request_c+1,_hs_temp_seat_req_c+1
1639  0476 2067              	bra	L357
1640  0478                   L707:
1641                         ; 1416 				if ((CSWM_config_c & CSWM_4HS_VARIANT_MASK_K) == CSWM_4HS_VARIANT_MASK_K)
1643                         ; 1419 					hsLF_Catch_Rear_Switch_Requests(hs_side);
1646                         ; 1451 				if ((CSWM_config_c & CSWM_4HS_VARIANT_MASK_K) == CSWM_4HS_VARIANT_MASK_K)
1648                         ; 1455 					hsLF_Catch_Rear_Switch_Requests(hs_side);
1650  0478 f600e8            	ldab	_CSWM_config_c
1651  047b c40f              	andb	#15
1652  047d c10f              	cmpb	#15
1653  047f 265e              	bne	L357
1654  0481 e681              	ldab	OFST+1,s
1655  0483 4a04e1e1          	call	f_hsLF_Catch_Rear_Switch_Requests
1657  0487 2056              	bra	L357
1658                         ; 1479 				break;
1659  0489                   L137:
1660                         ; 1492 		hs_control_flags_c[ hs_side ].b.hs_can_msg_flow_ctrl_b = FALSE;
1662  0489 e681              	ldab	OFST+1,s
1663  048b 87                	clra	
1664  048c b746              	tfr	d,y
1665  048e 0dea011d02        	bclr	_hs_control_flags_c,y,2
1666                         ; 1495 		hsLF_UpdateHeatParameters(hs_side, HEAT_LEVEL_OFF);
1668  0493 c607              	ldab	#7
1669  0495 3b                	pshd	
1670  0496 e683              	ldab	OFST+3,s
1671  0498 4a0d5050          	call	f_hsLF_UpdateHeatParameters
1673  049c 1b82              	leas	2,s
1674                         ; 1503 		if(hs_control_flags_c [hs_side].b.hs_io_rq_control_b == TRUE)
1676  049e e681              	ldab	OFST+1,s
1677  04a0 87                	clra	
1678  04a1 b746              	tfr	d,y
1679  04a3 0fea011d040b      	brclr	_hs_control_flags_c,y,4,L557
1680                         ; 1506 			hs_control_flags_c [hs_side].b.hs_io_rq_control_b = FALSE;
1682  04a9 0dea011d04        	bclr	_hs_control_flags_c,y,4
1683                         ; 1509 			hs_IO_seat_req_c[ hs_side ] = 0;
1685  04ae 69ea005b          	clr	_hs_IO_seat_req_c,y
1687  04b2 2007              	bra	L757
1688  04b4                   L557:
1689                         ; 1514 			hs_status3_flags_c [hs_side].b.self_tests_active_b = FALSE;
1691  04b4 b746              	tfr	d,y
1692  04b6 0dea011120        	bclr	_hs_status3_flags_c,y,32
1693  04bb                   L757:
1694                         ; 1518 		hs_status3_flags_c [hs_side].b.tests_started_b = FALSE;
1696  04bb 87                	clra	
1697  04bc b746              	tfr	d,y
1698  04be 0dea011140        	bclr	_hs_status3_flags_c,y,64
1699                         ; 1522 		if ((CSWM_config_c & CSWM_4HS_VARIANT_MASK_K) == CSWM_4HS_VARIANT_MASK_K)
1701  04c3 1e00e80f02        	brset	_CSWM_config_c,15,LC007
1702  04c8 2015              	bra	L357
1703  04ca                   LC007:
1704                         ; 1527 			if (((hs_side == REAR_LEFT)||(hs_side == REAR_RIGHT))&& (main_SwReq_Rear_c == VEH_SWITCH_REAR_ONLY_K))
1706  04ca c102              	cmpb	#2
1707  04cc 2704              	beq	L567
1709  04ce c103              	cmpb	#3
1710  04d0 260d              	bne	L357
1711  04d2                   L567:
1713  04d2 f60000            	ldab	_main_SwReq_Rear_c
1714  04d5 c103              	cmpb	#3
1715  04d7 2606              	bne	L357
1716                         ; 1530 				hs_temp_seat_req_c[hs_side] = 0x01;
1718  04d9 c601              	ldab	#1
1719  04db 6bea003f          	stab	_hs_temp_seat_req_c,y
1720  04df                   L357:
1721                         ; 1537 } //End of Catch Signals function
1724  04df 31                	puly	
1725  04e0 0a                	rtc	
1763                         ; 1549 void hsLF_Catch_Rear_Switch_Requests(unsigned char hs_rear)
1763                         ; 1550 {
1764                         	switch	.ftext
1765  04e1                   f_hsLF_Catch_Rear_Switch_Requests:
1767  04e1 3b                	pshd	
1768       00000000          OFST:	set	0
1771                         ; 1552 	hs_rear_U12S_stat_c = (unsigned char) Dio_ReadChannel(LED_U12S_EN);
1773  04e2 e6fbfb1a          	ldab	[_Dio_PortRead_Ptr]
1774  04e6 c401              	andb	#1
1775  04e8 7b00d2            	stab	_hs_rear_U12S_stat_c
1776                         ; 1554 	switch (hs_rear) {
1778  04eb e681              	ldab	OFST+1,s
1780  04ed c002              	subb	#2
1781  04ef 2705              	beq	L767
1782  04f1 04013a            	dbeq	b,L177
1783  04f4 206f              	bra	L3101
1784  04f6                   L767:
1785                         ; 1558 			if (hs_rear_U12S_stat_c)
1787  04f6 f600d2            	ldab	_hs_rear_U12S_stat_c
1788  04f9 276a              	beq	L3101
1789                         ; 1562 				hs_rear_swth_stat_c[RL_SWITCH] = (unsigned char) Dio_ReadChannel(RL_LED_SW_DETECT);
1791  04fb e6fbfb01          	ldab	[_Dio_PortRead_Ptr]
1792  04ff c402              	andb	#2
1793  0501 54                	lsrb	
1794  0502 7b010b            	stab	_hs_rear_swth_stat_c
1795                         ; 1563 				if(hs_rear_swth_stat_c[RL_SWITCH] == STD_LOW)
1797  0505 2616              	bne	L7101
1798                         ; 1565 					if(hs_bounce_counter_c[RL_SWITCH] >= 5)
1800  0507 f6002c            	ldab	_hs_bounce_counter_c
1801  050a c105              	cmpb	#5
1802  050c 250a              	blo	L1201
1803                         ; 1567 						hs_temp_seat_req_rl_c = STD_LOW;
1805  050e 790041            	clr	_hs_temp_seat_req_c+2
1806                         ; 1569 						hs_rear_swpsd_stat_c[RL_SWITCH] = 1; //newly added.
1808  0511 c601              	ldab	#1
1809  0513 7b0109            	stab	_hs_rear_swpsd_stat_c
1811  0516 204d              	bra	L3101
1812  0518                   L1201:
1813                         ; 1573 						hs_bounce_counter_c[RL_SWITCH]++;
1815  0518 72002c            	inc	_hs_bounce_counter_c
1816  051b 2048              	bra	L3101
1817  051d                   L7101:
1818                         ; 1579 					if(hs_bounce_counter_c[RL_SWITCH] > 0)
1820  051d f7002c            	tst	_hs_bounce_counter_c
1821  0520 2705              	beq	L7201
1822                         ; 1581 						hs_bounce_counter_c[RL_SWITCH]--;
1824  0522 73002c            	dec	_hs_bounce_counter_c
1826  0525 203e              	bra	L3101
1827  0527                   L7201:
1828                         ; 1585 						hs_temp_seat_req_rl_c = STD_HIGH;
1830  0527 c601              	ldab	#1
1831  0529 7b0041            	stab	_hs_temp_seat_req_c+2
1832  052c 2037              	bra	L3101
1833  052e                   L177:
1834                         ; 1597 			if (hs_rear_U12S_stat_c)
1836  052e f600d2            	ldab	_hs_rear_U12S_stat_c
1837  0531 2732              	beq	L3101
1838                         ; 1601 				hs_rear_swth_stat_c[RR_SWITCH] = (unsigned char) Dio_ReadChannel(RR_LED_SW_DETECT);
1840  0533 e6fbfac9          	ldab	[_Dio_PortRead_Ptr]
1841  0537 c404              	andb	#4
1842  0539 54                	lsrb	
1843  053a 54                	lsrb	
1844  053b 7b010c            	stab	_hs_rear_swth_stat_c+1
1845                         ; 1602 				if(hs_rear_swth_stat_c[RR_SWITCH] == STD_LOW)
1847  053e 2616              	bne	L5301
1848                         ; 1604 					if(hs_bounce_counter_c[RR_SWITCH] >= 5)
1850  0540 f6002d            	ldab	_hs_bounce_counter_c+1
1851  0543 c105              	cmpb	#5
1852  0545 250a              	blo	L7301
1853                         ; 1606 						hs_temp_seat_req_rr_c = STD_LOW;
1855  0547 790042            	clr	_hs_temp_seat_req_c+3
1856                         ; 1608 						hs_rear_swpsd_stat_c[RR_SWITCH] = 1;
1858  054a c601              	ldab	#1
1859  054c 7b010a            	stab	_hs_rear_swpsd_stat_c+1
1861  054f 2014              	bra	L3101
1862  0551                   L7301:
1863                         ; 1612 						hs_bounce_counter_c[RR_SWITCH]++;
1865  0551 72002d            	inc	_hs_bounce_counter_c+1
1866  0554 200f              	bra	L3101
1867  0556                   L5301:
1868                         ; 1618 					if(hs_bounce_counter_c[RR_SWITCH] > 0)
1870  0556 f7002d            	tst	_hs_bounce_counter_c+1
1871  0559 2705              	beq	L5401
1872                         ; 1620 						hs_bounce_counter_c[RR_SWITCH]--;
1874  055b 73002d            	dec	_hs_bounce_counter_c+1
1876  055e 2005              	bra	L3101
1877  0560                   L5401:
1878                         ; 1624 						hs_temp_seat_req_rr_c = STD_HIGH;
1880  0560 c601              	ldab	#1
1881  0562 7b0042            	stab	_hs_temp_seat_req_c+3
1882  0565                   L3101:
1883                         ; 1638 }
1886  0565 31                	puly	
1887  0566 0a                	rtc	
1928                         ; 1674 void hsLF_Process_Switch_Requests_Bus (unsigned char hs_pos)
1928                         ; 1675 {
1929                         	switch	.ftext
1930  0567                   f_hsLF_Process_Switch_Requests_Bus:
1932  0567 3b                	pshd	
1933       00000000          OFST:	set	0
1936                         ; 1679 	if ((hs_control_flags_c[ hs_pos ].b.hs_can_msg_flow_ctrl_b == FALSE) &&
1936                         ; 1680 	    ((hs_temp_seat_req_c [hs_pos] == HS_PSD) ||
1936                         ; 1681 	    (hs_temp_seat_req_c [hs_pos] == HS_OFF) ||
1936                         ; 1682 	    (hs_temp_seat_req_c [hs_pos] == HS_SNA)))
1938  0568 b796              	exg	b,y
1939  056a e6ea011d          	ldab	_hs_control_flags_c,y
1940  056e c502              	bitb	#2
1941  0570 267d              	bne	L3701
1943  0572 e6ea003f          	ldab	_hs_temp_seat_req_c,y
1944  0576 c101              	cmpb	#1
1945  0578 2708              	beq	L5701
1947  057a c107              	cmpb	#7
1948  057c 2704              	beq	L5701
1950  057e c103              	cmpb	#3
1951  0580 266d              	bne	L3701
1952  0582                   L5701:
1953                         ; 1686 		hs_control_flags_c[ hs_pos ].b.hs_can_msg_flow_ctrl_b = TRUE;
1955  0582 e681              	ldab	OFST+1,s
1956  0584 87                	clra	
1957  0585 b746              	tfr	d,y
1958  0587 0cea011d02        	bset	_hs_control_flags_c,y,2
1959                         ; 1689 		hs_switch_request_c [hs_pos] = hs_temp_seat_req_c [hs_pos];
1961  058c 180aea003fea005f  	movb	_hs_temp_seat_req_c,y,_hs_switch_request_c,y
1962                         ; 1692 		hsLF_GetHeatedSeatCanStatus(hs_pos);
1964  0594 4a0cebeb          	call	f_hsLF_GetHeatedSeatCanStatus
1966                         ; 1695 		hsLF_Timer(hs_pos, TIMER_CLEAR);
1968  0598 cc0002            	ldd	#2
1969  059b 3b                	pshd	
1970  059c e683              	ldab	OFST+3,s
1971  059e 4a0d1111          	call	f_hsLF_Timer
1973  05a2 1b82              	leas	2,s
1974                         ; 1699 		switch (hs_switch_request_c [hs_pos]) {
1976  05a4 e681              	ldab	OFST+1,s
1977  05a6 87                	clra	
1978  05a7 b746              	tfr	d,y
1979  05a9 e6ea005f          	ldab	_hs_switch_request_c,y
1981  05ad 04010a            	dbeq	b,L3501
1982  05b0 c002              	subb	#2
1983  05b2 2721              	beq	L1111
1984  05b4 c004              	subb	#4
1985  05b6 2629              	bne	L3011
1986                         ; 1707 				hsLF_UpdateHeatParameters(hs_pos, HEAT_LEVEL_OFF);
1989                         ; 1709 				break;
1991  05b8 201b              	bra	L1111
1992  05ba                   L3501:
1993                         ; 1716 				if (hs_temp_seat_stat_c [hs_pos] == CAN_LED_OFF)
1995  05ba e681              	ldab	OFST+1,s
1996  05bc b746              	tfr	d,y
1997  05be e7ea003b          	tst	_hs_temp_seat_stat_c,y
1998  05c2 2604              	bne	L5011
1999                         ; 1728 					hsLF_UpdateHeatParameters(hs_pos, HEAT_LEVEL_HL3);
2001  05c4 c603              	ldab	#3
2004  05c6 2010              	bra	LC009
2005  05c8                   L5011:
2006                         ; 1733 					if (hs_temp_seat_stat_c [hs_pos] == CAN_LED_HI)
2008  05c8 b746              	tfr	d,y
2009  05ca e6ea003b          	ldab	_hs_temp_seat_stat_c,y
2010  05ce c103              	cmpb	#3
2011  05d0 2603              	bne	L1111
2012                         ; 1736 						hsLF_UpdateHeatParameters(hs_pos, HEAT_LEVEL_LL1);
2014  05d2 c7                	clrb	
2017  05d3 2003              	bra	LC009
2018  05d5                   L1111:
2019                         ; 1741 						hsLF_UpdateHeatParameters(hs_pos, HEAT_LEVEL_OFF);
2021  05d5 cc0007            	ldd	#7
2023  05d8                   LC009:
2024  05d8 3b                	pshd	
2025  05d9 e683              	ldab	OFST+3,s
2026  05db 4a0d5050          	call	f_hsLF_UpdateHeatParameters
2027  05df 1b82              	leas	2,s
2028  05e1                   L3011:
2029                         ; 1754 		hs_rem_seat_req_c[hs_pos] = hs_switch_request_to_fet_c[hs_pos];
2031  05e1 e681              	ldab	OFST+1,s
2032  05e3 b796              	exg	b,y
2033  05e5 180aea0053ea00fd  	movb	_hs_switch_request_to_fet_c,y,_hs_rem_seat_req_c,y
2035  05ed 201c              	bra	L5111
2036  05ef                   L3701:
2037                         ; 1760 		if (hs_control_flags_c[ hs_pos ].b.hs_can_msg_flow_ctrl_b &&
2037                         ; 1761 		   hs_switch_request_c [hs_pos] != hs_temp_seat_req_c [hs_pos])
2039  05ef e681              	ldab	OFST+1,s
2040  05f1 87                	clra	
2041  05f2 b746              	tfr	d,y
2042  05f4 0fea011d0211      	brclr	_hs_control_flags_c,y,2,L5111
2044  05fa b745              	tfr	d,x
2045  05fc e6e2005f          	ldab	_hs_switch_request_c,x
2046  0600 e1ea003f          	cmpb	_hs_temp_seat_req_c,y
2047  0604 2705              	beq	L5111
2048                         ; 1764 			hs_control_flags_c[ hs_pos ].b.hs_can_msg_flow_ctrl_b = FALSE;
2050  0606 0dea011d02        	bclr	_hs_control_flags_c,y,2
2051  060b                   L5111:
2052                         ; 1767 } //End of Switch request processing from BUS
2055  060b 31                	puly	
2056  060c 0a                	rtc	
2096                         ; 1801 void hsLF_Process_Switch_Requests_ECU (unsigned char hs_rears)
2096                         ; 1802 {
2097                         	switch	.ftext
2098  060d                   f_hsLF_Process_Switch_Requests_ECU:
2100  060d 3b                	pshd	
2101       00000000          OFST:	set	0
2104                         ; 1805 	if ((hs_control_flags_c[ hs_rears ].b.hs_can_msg_flow_ctrl_b == FALSE) &&
2104                         ; 1806 	    (hs_temp_seat_req_c [hs_rears] == STD_LOW) )
2106  060e b796              	exg	b,y
2107  0610 e6ea011d          	ldab	_hs_control_flags_c,y
2108  0614 c502              	bitb	#2
2109  0616 2651              	bne	L5311
2111  0618 e6ea003f          	ldab	_hs_temp_seat_req_c,y
2112  061c 264b              	bne	L5311
2113                         ; 1810 		hs_control_flags_c[ hs_rears ].b.hs_can_msg_flow_ctrl_b = TRUE;
2115  061e 0cea011d02        	bset	_hs_control_flags_c,y,2
2116                         ; 1813 		hs_switch_request_c [hs_rears] = hs_temp_seat_req_c [hs_rears];
2118  0623 6bea005f          	stab	_hs_switch_request_c,y
2119                         ; 1816 		hsLF_Timer(hs_rears, TIMER_CLEAR);
2121  0627 cc0002            	ldd	#2
2122  062a 3b                	pshd	
2123  062b e683              	ldab	OFST+3,s
2124  062d 4a0d1111          	call	f_hsLF_Timer
2126  0631 1b82              	leas	2,s
2127                         ; 1820 		if (hs_rear_seat_led_status_c[hs_rears] == CAN_LED_OFF)
2129  0633 e681              	ldab	OFST+1,s
2130  0635 87                	clra	
2131  0636 b746              	tfr	d,y
2132  0638 e7ea0063          	tst	_hs_rear_seat_led_status_c,y
2133  063c 2604              	bne	L7311
2134                         ; 1832 					hsLF_UpdateHeatParameters(hs_rears, HEAT_LEVEL_HL3);
2136  063e c603              	ldab	#3
2139  0640 2010              	bra	L1411
2140  0642                   L7311:
2141                         ; 1836 			if (hs_rear_seat_led_status_c[hs_rears] == CAN_LED_HI)
2143  0642 b746              	tfr	d,y
2144  0644 e6ea0063          	ldab	_hs_rear_seat_led_status_c,y
2145  0648 c103              	cmpb	#3
2146  064a 2603              	bne	L3411
2147                         ; 1839 				hsLF_UpdateHeatParameters(hs_rears, HEAT_LEVEL_LL1);
2149  064c c7                	clrb	
2152  064d 2003              	bra	L1411
2153  064f                   L3411:
2154                         ; 1844 				hsLF_UpdateHeatParameters(hs_rears, HEAT_LEVEL_OFF);
2156  064f cc0007            	ldd	#7
2158  0652                   L1411:
2159  0652 3b                	pshd	
2160  0653 e683              	ldab	OFST+3,s
2161  0655 4a0d5050          	call	f_hsLF_UpdateHeatParameters
2162  0659 1b82              	leas	2,s
2163                         ; 1848 		hs_rem_seat_req_c[hs_rears] = hs_switch_request_to_fet_c[hs_rears];
2165  065b e681              	ldab	OFST+1,s
2166  065d b796              	exg	b,y
2167  065f 180aea0053ea00fd  	movb	_hs_switch_request_to_fet_c,y,_hs_rem_seat_req_c,y
2169  0667 201c              	bra	L7411
2170  0669                   L5311:
2171                         ; 1854 		if (hs_control_flags_c[ hs_rears ].b.hs_can_msg_flow_ctrl_b &&
2171                         ; 1855 		   hs_switch_request_c [hs_rears] != hs_temp_seat_req_c [hs_rears])
2173  0669 e681              	ldab	OFST+1,s
2174  066b 87                	clra	
2175  066c b746              	tfr	d,y
2176  066e 0fea011d0211      	brclr	_hs_control_flags_c,y,2,L7411
2178  0674 b745              	tfr	d,x
2179  0676 e6e2005f          	ldab	_hs_switch_request_c,x
2180  067a e1ea003f          	cmpb	_hs_temp_seat_req_c,y
2181  067e 2705              	beq	L7411
2182                         ; 1858 			hs_control_flags_c[ hs_rears ].b.hs_can_msg_flow_ctrl_b = FALSE;
2184  0680 0dea011d02        	bclr	_hs_control_flags_c,y,2
2185  0685                   L7411:
2186                         ; 1861 } //End of Switch request processing from ECU
2189  0685 31                	puly	
2190  0686 0a                	rtc	
2245                         ; 1908 void hsLF_RemoteStart_Control(void)
2245                         ; 1909 {
2246                         	switch	.ftext
2247  0687                   f_hsLF_RemoteStart_Control:
2249  0687 3b                	pshd	
2250       00000002          OFST:	set	2
2253                         ; 1910 	unsigned char hs_auto_rem_c = 0; //pc-lint warning 644 eliminated.
2255                         ; 1911 	unsigned char hs_switch_rem_c = 0;
2257  0688 87                	clra	
2258  0689 c7                	clrb	
2259  068a 6c80              	std	OFST-2,s
2260                         ; 1920 	if ((canio_LHD_RHD_c == LHD) ||(canio_LHD_RHD_c == 0x03))
2262  068c f60000            	ldab	_canio_LHD_RHD_c
2263  068f c101              	cmpb	#1
2264  0691 2704              	beq	L5711
2266  0693 c103              	cmpb	#3
2267  0695 2613              	bne	L3711
2268  0697                   L5711:
2269                         ; 1923 		hs_auto_rem_c = FRONT_LEFT;
2271  0697 6980              	clr	OFST-2,s
2272                         ; 1927 		hs_switch_rem_c = FRONT_RIGHT;
2274  0699 c601              	ldab	#1
2275  069b 6b81              	stab	OFST-1,s
2277  069d                   L7711:
2278                         ; 1952 	if ((main_state_c == REM_STRT_HEATING) || (main_state_c == AUTO_STRT_HEATING) )
2280  069d f60000            	ldab	_main_state_c
2281  06a0 c101              	cmpb	#1
2282  06a2 2712              	beq	L7021
2284  06a4 c104              	cmpb	#4
2285  06a6 2647              	bne	L7121
2286  06a8 200c              	bra	L7021
2287  06aa                   L3711:
2288                         ; 1931 		if (canio_LHD_RHD_c == RHD)
2290  06aa c102              	cmpb	#2
2291  06ac 26ef              	bne	L7711
2292                         ; 1934 			hs_auto_rem_c = FRONT_RIGHT;
2294  06ae c601              	ldab	#1
2295  06b0 6b80              	stab	OFST-2,s
2296                         ; 1938 			hs_switch_rem_c = FRONT_LEFT;
2298  06b2 6981              	clr	OFST-1,s
2300  06b4 20e7              	bra	L7711
2301  06b6                   L7021:
2302                         ; 1956 		if (hs_control_flags_c[ hs_auto_rem_c ].b.remote_user_b == FALSE)
2304  06b6 e680              	ldab	OFST-2,s
2305  06b8 87                	clra	
2306  06b9 b746              	tfr	d,y
2307  06bb 0eea011d401f      	brset	_hs_control_flags_c,y,64,L3121
2308                         ; 1959 			hsLF_Timer(hs_auto_rem_c, TIMER_CLEAR);
2310  06c1 c602              	ldab	#2
2311  06c3 3b                	pshd	
2312  06c4 e682              	ldab	OFST+0,s
2313  06c6 4a0d1111          	call	f_hsLF_Timer
2315                         ; 1975 				hsLF_UpdateHeatParameters(hs_auto_rem_c, HEAT_LEVEL_HL3);
2317  06ca cc0003            	ldd	#3
2318  06cd 6c80              	std	0,s
2319  06cf e682              	ldab	OFST+0,s
2320  06d1 4a0d5050          	call	f_hsLF_UpdateHeatParameters
2322  06d5 1b82              	leas	2,s
2323                         ; 1979 			hs_control_flags_c[ hs_auto_rem_c ].b.remote_user_b = TRUE;
2325  06d7 e680              	ldab	OFST-2,s
2326  06d9 b796              	exg	b,y
2327  06db 0cea011d40        	bset	_hs_control_flags_c,y,64
2329  06e0                   L3121:
2330                         ; 1988 		if (hs_control_flags_c[ hs_switch_rem_c ].b.remote_user_b == FALSE)
2332  06e0 e681              	ldab	OFST-1,s
2333  06e2 b796              	exg	b,y
2334  06e4 0eea011d4005      	brset	_hs_control_flags_c,y,64,L7121
2335                         ; 1991 			hs_control_flags_c[ hs_switch_rem_c ].b.remote_user_b = TRUE;
2337  06ea 0cea011d40        	bset	_hs_control_flags_c,y,64
2338  06ef                   L7121:
2339                         ; 2007 	if ((main_state_c == REM_STRT_VENTING)  || (main_state_c == AUTO_STRT_VENTING))
2341  06ef f60000            	ldab	_main_state_c
2342  06f2 c102              	cmpb	#2
2343  06f4 2704              	beq	L3221
2345  06f6 c105              	cmpb	#5
2346  06f8 261e              	bne	L3321
2347  06fa                   L3221:
2348                         ; 2010 		if (hs_control_flags_c[ hs_auto_rem_c ].b.remote_user_b == FALSE)
2350  06fa e680              	ldab	OFST-2,s
2351  06fc b796              	exg	b,y
2352  06fe 0eea011d4005      	brset	_hs_control_flags_c,y,64,L7221
2353                         ; 2012 			hs_control_flags_c[ hs_auto_rem_c ].b.remote_user_b = TRUE;
2355  0704 0cea011d40        	bset	_hs_control_flags_c,y,64
2357  0709                   L7221:
2358                         ; 2021 		if (hs_control_flags_c[ hs_switch_rem_c ].b.remote_user_b == FALSE)
2360  0709 e681              	ldab	OFST-1,s
2361  070b b796              	exg	b,y
2362  070d 0eea011d4005      	brset	_hs_control_flags_c,y,64,L3321
2363                         ; 2024 			hs_control_flags_c[ hs_switch_rem_c ].b.remote_user_b = TRUE;
2365  0713 0cea011d40        	bset	_hs_control_flags_c,y,64
2366  0718                   L3321:
2367                         ; 2039 	if ( (main_state_c == REM_STRT_USER_SWTCH)||
2367                         ; 2040 		 (main_state_c == AUTO_STRT_USER_SWTCH) || 
2367                         ; 2041 	     (hs_control_flags_c[ hs_auto_rem_c ].b.remote_user_b == TRUE) ||
2367                         ; 2042 	     (hs_control_flags_c[ hs_switch_rem_c ].b.remote_user_b == TRUE) )
2369  0718 f60000            	ldab	_main_state_c
2370  071b c103              	cmpb	#3
2371  071d 2719              	beq	L7321
2373  071f c106              	cmpb	#6
2374  0721 2715              	beq	L7321
2376  0723 e680              	ldab	OFST-2,s
2377  0725 87                	clra	
2378  0726 b746              	tfr	d,y
2379  0728 0eea011d400a      	brset	_hs_control_flags_c,y,64,L7321
2381  072e e681              	ldab	OFST-1,s
2382  0730 b746              	tfr	d,y
2383  0732 0fea011d404d      	brclr	_hs_control_flags_c,y,64,L5321
2384  0738                   L7321:
2385                         ; 2046 		for (hs_seat = FRONT_LEFT; hs_seat <=  FRONT_RIGHT; hs_seat++)
2387  0738 c7                	clrb	
2388  0739 6b80              	stab	OFST-2,s
2389  073b                   L5421:
2390                         ; 2048 			hsLF_Process_Switch_Requests_Bus(hs_seat);
2392  073b 87                	clra	
2393  073c 4a056767          	call	f_hsLF_Process_Switch_Requests_Bus
2395                         ; 2046 		for (hs_seat = FRONT_LEFT; hs_seat <=  FRONT_RIGHT; hs_seat++)
2397  0740 6280              	inc	OFST-2,s
2400  0742 e680              	ldab	OFST-2,s
2401  0744 c101              	cmpb	#1
2402  0746 23f3              	bls	L5421
2403                         ; 2053 		if ( (main_state_c == AUTO_STRT_USER_SWTCH) || (main_state_c == AUTO_STRT_VENTING) || (main_state_c == AUTO_STRT_HEATING) )
2405  0748 f60000            	ldab	_main_state_c
2406  074b c106              	cmpb	#6
2407  074d 2708              	beq	L5521
2409  074f c105              	cmpb	#5
2410  0751 2704              	beq	L5521
2412  0753 c104              	cmpb	#4
2413  0755 262e              	bne	L5321
2414  0757                   L5521:
2415                         ; 2071 			if(hs_seat == REAR_LEFT && 
2415                         ; 2072 			   hs_rear_seat_flags_c[ RL_SWITCH ].b.stksw_psd == FALSE &&
2415                         ; 2073 			   d_AllDtc_a[REAR_LEFT_STUCK_INDEX].DtcState.state.TestFailed == FALSE)
2417  0757 e680              	ldab	OFST-2,s
2418  0759 c102              	cmpb	#2
2419  075b 2613              	bne	L1621
2421  075d 1e010f010e        	brset	_hs_rear_seat_flags_c,1,L1621
2423  0762 1e00510109        	brset	_d_AllDtc_a+81,1,L1621
2424                         ; 2076 				hsLF_Process_Switch_Requests_ECU (REAR_LEFT);
2426  0767 cc0002            	ldd	#2
2427  076a 4a060d0d          	call	f_hsLF_Process_Switch_Requests_ECU
2429  076e e680              	ldab	OFST-2,s
2430  0770                   L1621:
2431                         ; 2079 			if(hs_seat == REAR_RIGHT && 
2431                         ; 2080 			   hs_rear_seat_flags_c[ RR_SWITCH ].b.stksw_psd == FALSE &&
2431                         ; 2081 			   d_AllDtc_a[REAR_RIGHT_STUCK_INDEX].DtcState.state.TestFailed == FALSE)
2433  0770 c103              	cmpb	#3
2434  0772 2611              	bne	L5321
2436  0774 1e0110010c        	brset	_hs_rear_seat_flags_c+1,1,L5321
2438  0779 1e00530107        	brset	_d_AllDtc_a+83,1,L5321
2439                         ; 2084 				hsLF_Process_Switch_Requests_ECU (REAR_RIGHT);
2441  077e cc0003            	ldd	#3
2442  0781 4a060d0d          	call	f_hsLF_Process_Switch_Requests_ECU
2444  0785                   L5321:
2445                         ; 2095 } //End of Remote Start Control function
2448  0785 31                	puly	
2449  0786 0a                	rtc	
2493                         ; 2138 @far void hsF_MonitorHeatTimes(void)
2493                         ; 2139 {
2494                         	switch	.ftext
2495  0787                   f_hsF_MonitorHeatTimes:
2497  0787 1b9d              	leas	-3,s
2498       00000003          OFST:	set	3
2501                         ; 2143 	for (hs_seat = FRONT_LEFT; hs_seat < ALL_HEAT_SEATS; hs_seat++)
2503  0789 6982              	clr	OFST-1,s
2504  078b                   L3031:
2505                         ; 2148 		if ((main_CSWM_Status_c == GOOD) &&
2505                         ; 2149 		    (!hs_status_flags_c[ hs_seat ].b.hs_prelim_short_gnd_b) &&
2505                         ; 2150 		    (!hs_status_flags_c[ hs_seat ].b.hs_prelim_short_bat_b) &&
2505                         ; 2151 		    (!hs_status_flags_c[ hs_seat ].b.hs_prelim_open_b)    /*  &&
2505                         ; 2152 		    (!hs_status_flags_c[ hs_seat ].b.hs_prelim_partial_open_b)*/)
2507  078b f60000            	ldab	_main_CSWM_Status_c
2508  078e 53                	decb	
2509  078f 182600d0          	bne	L7431
2511  0793 e682              	ldab	OFST-1,s
2512  0795 b796              	exg	b,y
2513  0797 e6ea0119          	ldab	_hs_status_flags_c,y
2514  079b c501              	bitb	#1
2515  079d 182600c2          	bne	L7431
2517  07a1 c510              	bitb	#16
2518  07a3 182600bc          	bne	L7431
2520  07a7 c504              	bitb	#4
2521  07a9 182600b6          	bne	L7431
2522                         ; 2155 			if (hs_control_flags_c[ hs_seat ].b.hs_start_timer_b)
2524  07ad e6ea011d          	ldab	_hs_control_flags_c,y
2525  07b1 c501              	bitb	#1
2526  07b3 182700ac          	beq	L7431
2527                         ; 2164 					if ( (hs_time_counter_w[ hs_seat ] >= (hs_wait_time_check_NTC_c[ hs_seat ] * 187)) && 
2527                         ; 2165 					     (NTC_AD_Readings_c[hs_seat] <= hs_allowed_NTC_c[hs_seat]) )
2529  07b7 e6ea0047          	ldab	_hs_wait_time_check_NTC_c,y
2530  07bb 86bb              	ldaa	#187
2531  07bd 12                	mul	
2532  07be 6c80              	std	OFST-3,s
2533  07c0 1858              	lsly	
2534  07c2 ecea00dc          	ldd	_hs_time_counter_w,y
2535  07c6 ac80              	cpd	OFST-3,s
2536  07c8 251e              	blo	L7131
2538  07ca e682              	ldab	OFST-1,s
2539  07cc 87                	clra	
2540  07cd b746              	tfr	d,y
2541  07cf b745              	tfr	d,x
2542  07d1 e6e200e4          	ldab	_NTC_AD_Readings_c,x
2543  07d5 e1ea0043          	cmpb	_hs_allowed_NTC_c,y
2544  07d9 220d              	bhi	L7131
2545                         ; 2170 						hs_time_counter_w[ hs_seat ] = (hs_allowed_heat_time_c[ hs_seat ] * 187);
2547  07db 1858              	lsly	
2548  07dd e6e2004b          	ldab	_hs_allowed_heat_time_c,x
2549  07e1 86bb              	ldaa	#187
2550  07e3 12                	mul	
2551  07e4 6cea00dc          	std	_hs_time_counter_w,y
2553  07e8                   L7131:
2554                         ; 2184 				if (hs_time_counter_w[ hs_seat ] >= (hs_allowed_heat_time_c[ hs_seat ] * /*3 *60*/187))
2556  07e8 e682              	ldab	OFST-1,s
2557  07ea b796              	exg	b,y
2558  07ec e6ea004b          	ldab	_hs_allowed_heat_time_c,y
2559  07f0 86bb              	ldaa	#187
2560  07f2 12                	mul	
2561  07f3 6c80              	std	OFST-3,s
2562  07f5 1858              	lsly	
2563  07f7 ecea00dc          	ldd	_hs_time_counter_w,y
2564  07fb ac80              	cpd	OFST-3,s
2565  07fd e682              	ldab	OFST-1,s
2566  07ff 2559              	blo	L1231
2567                         ; 2187 					if (hs_status2_flags_c[hs_seat].b.hs_HL3_set_b)
2569  0801 87                	clra	
2570  0802 b746              	tfr	d,y
2571  0804 0fea01150109      	brclr	_hs_status2_flags_c,y,1,L3231
2572                         ; 2190 						hs_status2_flags_c[hs_seat].b.hs_HL3_set_b = FALSE;
2574  080a 0dea011501        	bclr	_hs_status2_flags_c,y,1
2575                         ; 2194 						hsLF_UpdateHeatParameters(hs_seat, HEAT_LEVEL_HL2);
2577  080f c602              	ldab	#2
2580  0811 2030              	bra	LC011
2581  0813                   L3231:
2582                         ; 2199 						if (hs_status2_flags_c[hs_seat].b.hs_HL2_set_b)
2584  0813 b746              	tfr	d,y
2585  0815 0fea01150209      	brclr	_hs_status2_flags_c,y,2,L7231
2586                         ; 2202 							hs_status2_flags_c[hs_seat].b.hs_HL2_set_b = FALSE;
2588  081b 0dea011502        	bclr	_hs_status2_flags_c,y,2
2589                         ; 2205 							hsLF_UpdateHeatParameters(hs_seat, HEAT_LEVEL_HL1);
2591  0820 c601              	ldab	#1
2594  0822 201f              	bra	LC011
2595  0824                   L7231:
2596                         ; 2210 							if (hs_status2_flags_c[hs_seat].b.hs_HL1_set_b)
2598  0824 b746              	tfr	d,y
2599  0826 0fea01150408      	brclr	_hs_status2_flags_c,y,4,L3331
2600                         ; 2213 								hs_status2_flags_c[hs_seat].b.hs_HL1_set_b = FALSE;
2602  082c 0dea011504        	bclr	_hs_status2_flags_c,y,4
2603                         ; 2216 								hsLF_UpdateHeatParameters(hs_seat, HEAT_LEVEL_LL1);
2605  0831 c7                	clrb	
2608  0832 200f              	bra	LC011
2609  0834                   L3331:
2610                         ; 2221 								if (hs_status2_flags_c[hs_seat].b.hs_LL1_set_b)
2612  0834 b746              	tfr	d,y
2613  0836 0fea01151012      	brclr	_hs_status2_flags_c,y,16,L5231
2614                         ; 2224 									hs_status2_flags_c[hs_seat].b.hs_LL1_set_b = FALSE;
2616  083c 0dea011510        	bclr	_hs_status2_flags_c,y,16
2617                         ; 2227 									hsLF_UpdateHeatParameters(hs_seat, HEAT_LEVEL_OFF);
2619  0841 c607              	ldab	#7
2621  0843                   LC011:
2622  0843 3b                	pshd	
2623  0844 e684              	ldab	OFST+1,s
2624  0846 4a0d5050          	call	f_hsLF_UpdateHeatParameters
2625  084a 1b82              	leas	2,s
2627  084c e682              	ldab	OFST-1,s
2628  084e                   L5231:
2629                         ; 2238 					hs_rem_seat_req_c[hs_seat] = hs_switch_request_to_fet_c[ hs_seat ];
2631  084e b796              	exg	b,y
2632  0850 180aea0053ea00fd  	movb	_hs_switch_request_to_fet_c,y,_hs_rem_seat_req_c,y
2634  0858 2009              	bra	L7431
2635  085a                   L1231:
2636                         ; 2243 					hs_time_counter_w[ hs_seat ]++;
2638  085a 87                	clra	
2639  085b 59                	lsld	
2640  085c b746              	tfr	d,y
2641  085e 1862ea00dc        	incw	_hs_time_counter_w,y
2642  0863                   L7431:
2643                         ; 2143 	for (hs_seat = FRONT_LEFT; hs_seat < ALL_HEAT_SEATS; hs_seat++)
2645  0863 6282              	inc	OFST-1,s
2648  0865 e682              	ldab	OFST-1,s
2649  0867 c104              	cmpb	#4
2650  0869 1825ff1e          	blo	L3031
2651                         ; 2256 }
2654  086d 1b83              	leas	3,s
2655  086f 0a                	rtc	
2704                         ; 2301 void hsLF_HeatDiagnostics(void)
2704                         ; 2302 {
2705                         	switch	.ftext
2706  0870                   f_hsLF_HeatDiagnostics:
2708  0870 37                	pshb	
2709       00000001          OFST:	set	1
2712                         ; 2307 	if (main_CSWM_Status_c == GOOD)
2714  0871 f60000            	ldab	_main_CSWM_Status_c
2715  0874 53                	decb	
2716  0875 1826018b          	bne	L5631
2717                         ; 2309 		for (hs_seat = FRONT_LEFT; hs_seat < ALL_HEAT_SEATS; hs_seat++)
2719  0879 6b80              	stab	OFST-1,s
2720  087b                   L7631:
2721                         ; 2312 			hs_status3_flags_c[ hs_seat ].b.first_time_ugly_b = FALSE;
2723  087b b796              	exg	b,y
2724  087d 0dea011110        	bclr	_hs_status3_flags_c,y,16
2725                         ; 2317 				if ( (hs_status2_flags_c[ hs_seat ].b.hs_heater_state_b)  && 
2725                         ; 2318 				     (!hs_status_flags_c[ hs_seat ].b.hs_final_short_gnd_b) &&
2725                         ; 2319 				     (!hs_status_flags_c[ hs_seat ].b.hs_final_open_b)      &&
2725                         ; 2320 				     (!hs_status_flags_c[ hs_seat ].b.hs_final_partial_open_b) )
2727  0882 e6ea0115          	ldab	_hs_status2_flags_c,y
2728  0886 c540              	bitb	#64
2729  0888 18270113          	beq	L5731
2731  088c e6ea0119          	ldab	_hs_status_flags_c,y
2732  0890 c502              	bitb	#2
2733  0892 18260109          	bne	L5731
2735  0896 c508              	bitb	#8
2736  0898 18260103          	bne	L5731
2738  089c c580              	bitb	#128
2739  089e 182600fd          	bne	L5731
2740                         ; 2324 					if ((((hs_seat == FRONT_LEFT) || (hs_seat == FRONT_RIGHT)) && (!relay_A_internal_Fault_b)) ||
2740                         ; 2325 					   (((hs_seat == REAR_LEFT) || (hs_seat == REAR_RIGHT)) && (!relay_B_internal_Fault_b && !Rear_Seats_no_power_prelim_b)))
2742  08a2 e680              	ldab	OFST-1,s
2743  08a4 2704              	beq	L5041
2745  08a6 c101              	cmpb	#1
2746  08a8 2605              	bne	L3041
2747  08aa                   L5041:
2749  08aa 1f0000101e        	brclr	_relay_control_c,16,L1041
2750  08af                   L3041:
2752  08af c102              	cmpb	#2
2753  08b1 2706              	beq	L7041
2755  08b3 c103              	cmpb	#3
2756  08b5 182600d7          	bne	L7731
2757  08b9                   L7041:
2759  08b9 f60000            	ldab	_relay_control_c
2760  08bc c520              	bitb	#32
2761  08be 182600ce          	bne	L7731
2763  08c2 f60000            	ldab	_relay_control2_c
2764  08c5 c501              	bitb	#1
2765  08c7 182600c5          	bne	L7731
2766  08cb e680              	ldab	OFST-1,s
2767  08cd                   L1041:
2768                         ; 2328 						if ((hs_Vsense_c[hs_seat] >= hs_flt_dtc_c[HS_STG_VSENSE]) &&
2768                         ; 2329 						    (hs_Isense_c[hs_seat] < hs_flt_dtc_c[HS_OVERLOAD_ISENSE]))
2770  08cd 87                	clra	
2771  08ce b746              	tfr	d,y
2772  08d0 e6ea00f1          	ldab	_hs_Vsense_c,y
2773  08d4 f100ce            	cmpb	_hs_flt_dtc_c+3
2774  08d7 182500a5          	blo	L1141
2776  08db e6ea00ed          	ldab	_hs_Isense_c,y
2777  08df f100cc            	cmpb	_hs_flt_dtc_c+1
2778  08e2 1824009a          	bhs	L1141
2779                         ; 2332 							if (hs_Isense_c[hs_seat] < hs_flt_dtc_c[HS_FULL_OPEN_ISENSE])
2781  08e6 f100cb            	cmpb	_hs_flt_dtc_c
2782  08e9 240e              	bhs	L3141
2783                         ; 2336 								hs_status_flags_c[ hs_seat ].b.hs_prelim_open_b = TRUE;
2785  08eb 0dea011941        	bclr	_hs_status_flags_c,y,65
2786  08f0 0cea011904        	bset	_hs_status_flags_c,y,4
2787                         ; 2339 								hs_status_flags_c[ hs_seat ].b.hs_prelim_partial_open_b = FALSE;
2789                         ; 2342 								hs_status_flags_c[ hs_seat ].b.hs_prelim_short_gnd_b = FALSE;
2792  08f5 182000f8          	bra	L1541
2793  08f9                   L3141:
2794                         ; 2349 							   if ((hs_seat == FRONT_LEFT) || (hs_seat == FRONT_RIGHT))
2796  08f9 e680              	ldab	OFST-1,s
2797  08fb 2704              	beq	L1241
2799  08fd c101              	cmpb	#1
2800  08ff 2642              	bne	L7141
2801  0901                   L1241:
2802                         ; 2352 									if (hs_Zsense_c[hs_seat] >= 70)
2804  0901 87                	clra	
2805  0902 b746              	tfr	d,y
2806  0904 e6ea00e9          	ldab	_hs_Zsense_c,y
2807  0908 c146              	cmpb	#70
2808  090a 252a              	blo	L3241
2809                         ; 2356 										hs_status_flags_c[ hs_seat ].b.hs_prelim_partial_open_b = TRUE;
2811  090c 0dea011905        	bclr	_hs_status_flags_c,y,5
2812  0911 0cea011940        	bset	_hs_status_flags_c,y,64
2813                         ; 2359 										hs_status_flags_c[ hs_seat ].b.hs_prelim_open_b = FALSE;
2815                         ; 2362 										hs_status_flags_c[ hs_seat ].b.hs_prelim_short_gnd_b = FALSE;
2817                         ; 2366 										if( (hs_status2_flags_c[hs_seat].b.hs_HL3_set_b) ||
2817                         ; 2367 											(hs_status2_flags_c[hs_seat].b.hs_HL2_set_b)	)
2819  0916 0eea0115010a      	brset	_hs_status2_flags_c,y,1,L7241
2821  091c e6ea0115          	ldab	_hs_status2_flags_c,y
2822  0920 c502              	bitb	#2
2823  0922 182700cb          	beq	L1541
2824  0926                   L7241:
2825                         ; 2369 											hsLF_UpdateHeatParameters(hs_seat, HEAT_LEVEL_HL1);
2827  0926 cc0001            	ldd	#1
2828  0929 3b                	pshd	
2829  092a e682              	ldab	OFST+1,s
2830  092c 4a0d5050          	call	f_hsLF_UpdateHeatParameters
2832  0930 1b82              	leas	2,s
2833  0932 182000bb          	bra	L1541
2834  0936                   L3241:
2835                         ; 2377 										hs_status_flags_c[ hs_seat ].b.hs_prelim_short_gnd_b = FALSE;
2837  0936 e680              	ldab	OFST-1,s
2838  0938 b746              	tfr	d,y
2839                         ; 2378 										hs_status_flags_c[ hs_seat ].b.hs_prelim_open_b = FALSE;
2841                         ; 2379 										hs_status_flags_c[ hs_seat ].b.hs_prelim_partial_open_b = FALSE;
2843                         ; 2380 										hs_status_flags_c[ hs_seat ].b.hs_prelim_short_bat_b = FALSE;
2845  093a 0dea011955        	bclr	_hs_status_flags_c,y,85
2846  093f 182000ae          	bra	L1541
2847  0943                   L7141:
2848                         ; 2387 									if (hs_Zsense_c[hs_seat] >= 80)
2850  0943 b746              	tfr	d,y
2851  0945 e6ea00e9          	ldab	_hs_Zsense_c,y
2852  0949 c150              	cmpb	#80
2853  094b 2528              	blo	L5341
2854                         ; 2391 										hs_status_flags_c[ hs_seat ].b.hs_prelim_partial_open_b = TRUE;
2856  094d 0dea011905        	bclr	_hs_status_flags_c,y,5
2857  0952 0cea011940        	bset	_hs_status_flags_c,y,64
2858                         ; 2394 										hs_status_flags_c[ hs_seat ].b.hs_prelim_open_b = FALSE;
2860                         ; 2397 										hs_status_flags_c[ hs_seat ].b.hs_prelim_short_gnd_b = FALSE;
2862                         ; 2401 										if( (hs_status2_flags_c[hs_seat].b.hs_HL3_set_b) ||
2862                         ; 2402 											(hs_status2_flags_c[hs_seat].b.hs_HL2_set_b)	)
2864  0957 0eea0115010a      	brset	_hs_status2_flags_c,y,1,L1441
2866  095d e6ea0115          	ldab	_hs_status2_flags_c,y
2867  0961 c502              	bitb	#2
2868  0963 1827008a          	beq	L1541
2869  0967                   L1441:
2870                         ; 2404 											hsLF_UpdateHeatParameters(hs_seat, HEAT_LEVEL_HL1);
2872  0967 cc0001            	ldd	#1
2873  096a 3b                	pshd	
2874  096b e682              	ldab	OFST+1,s
2875  096d 4a0d5050          	call	f_hsLF_UpdateHeatParameters
2877  0971 1b82              	leas	2,s
2878  0973 207c              	bra	L1541
2879  0975                   L5341:
2880                         ; 2412 										hs_status_flags_c[ hs_seat ].b.hs_prelim_short_gnd_b = FALSE;
2882  0975 e680              	ldab	OFST-1,s
2883  0977 b746              	tfr	d,y
2884                         ; 2413 										hs_status_flags_c[ hs_seat ].b.hs_prelim_open_b = FALSE;
2886                         ; 2414 										hs_status_flags_c[ hs_seat ].b.hs_prelim_partial_open_b = FALSE;
2888                         ; 2415 										hs_status_flags_c[ hs_seat ].b.hs_prelim_short_bat_b = FALSE;
2890  0979 0dea011955        	bclr	_hs_status_flags_c,y,85
2891  097e 2071              	bra	L1541
2892  0980                   L1141:
2893                         ; 2429 							hs_status_flags_c[ hs_seat ].b.hs_prelim_short_gnd_b = TRUE;
2895  0980 e680              	ldab	OFST-1,s
2896  0982 b796              	exg	b,y
2897  0984 0dea011904        	bclr	_hs_status_flags_c,y,4
2898  0989 0cea011901        	bset	_hs_status_flags_c,y,1
2899                         ; 2432 							hs_status_flags_c[ hs_seat ].b.hs_prelim_open_b = FALSE;
2901  098e 2061              	bra	L1541
2902  0990                   L7731:
2903                         ; 2439 						hs_monitor_prelim_fault_counter_c[hs_seat] = 0;
2905  0990 e680              	ldab	OFST-1,s
2906  0992 b796              	exg	b,y
2907  0994 69ea0037          	clr	_hs_monitor_prelim_fault_counter_c,y
2908                         ; 2442 						hs_status_flags_c[ hs_seat ].b.hs_prelim_short_gnd_b = FALSE;
2910                         ; 2443 						hs_status_flags_c[ hs_seat ].b.hs_prelim_open_b = FALSE;
2912                         ; 2444 						hs_status_flags_c[ hs_seat ].b.hs_prelim_partial_open_b = FALSE;
2914                         ; 2445 						hs_status_flags_c[ hs_seat ].b.hs_prelim_short_bat_b = FALSE;
2916  0998 0dea011955        	bclr	_hs_status_flags_c,y,85
2917  099d 2052              	bra	L1541
2918  099f                   L5731:
2919                         ; 2452 					if ((hs_seat == FRONT_LEFT) || (hs_seat == FRONT_RIGHT))
2921  099f e680              	ldab	OFST-1,s
2922  09a1 2704              	beq	L5541
2924  09a3 c101              	cmpb	#1
2925  09a5 261f              	bne	L3541
2926  09a7                   L5541:
2927                         ; 2455 						if ((!hs_status_flags_c[ hs_seat ].b.hs_final_short_bat_b) && (!relay_A_internal_Fault_b))
2929  09a7 b796              	exg	b,y
2930  09a9 0eea01192042      	brset	_hs_status_flags_c,y,32,L1541
2932  09af 1e0000103d        	brset	_relay_control_c,16,L1541
2933                         ; 2460 							if ((hs_Vsense_c[hs_seat] > /*hs_STB_Vsense_cutoff_c*/hs_flt_dtc_c[HS_STB_VSENSE]) && (!relay_F_FET_fault_b))
2935  09b4 e6ea00f1          	ldab	_hs_Vsense_c,y
2936  09b8 f100cd            	cmpb	_hs_flt_dtc_c+2
2937  09bb 232b              	bls	L3741
2939  09bd 1e00001002        	brset	_relay_fault_status_c,16,L1641
2940                         ; 2463 								hs_status_flags_c[ hs_seat ].b.hs_prelim_short_bat_b = TRUE;
2943  09c2 201d              	bra	LC013
2944  09c4                   L1641:
2945                         ; 2468 								hs_status_flags_c[ hs_seat ].b.hs_prelim_short_bat_b = FALSE;
2947  09c4 2022              	bra	L3741
2948  09c6                   L3541:
2949                         ; 2481 						if ((!hs_status_flags_c[ hs_seat ].b.hs_final_short_bat_b) && (!relay_B_internal_Fault_b))
2951  09c6 b796              	exg	b,y
2952  09c8 0eea01192023      	brset	_hs_status_flags_c,y,32,L1541
2954  09ce 1e0000201e        	brset	_relay_control_c,32,L1541
2955                         ; 2484 							if ( (hs_Vsense_c[hs_seat] > /*hs_STB_Vsense_cutoff_c*/hs_flt_dtc_c[HS_STB_VSENSE]) && (!relay_R_FET_fault_b))
2957  09d3 e6ea00f1          	ldab	_hs_Vsense_c,y
2958  09d7 f100cd            	cmpb	_hs_flt_dtc_c+2
2959  09da 230c              	bls	L3741
2961  09dc 1e00002007        	brset	_relay_fault_status_c,32,L3741
2962                         ; 2487 								hs_status_flags_c[ hs_seat ].b.hs_prelim_short_bat_b = TRUE;
2964  09e1                   LC013:
2965  09e1 0cea011910        	bset	_hs_status_flags_c,y,16
2967  09e6 2009              	bra	L1541
2968  09e8                   L3741:
2969                         ; 2492 								hs_status_flags_c[ hs_seat ].b.hs_prelim_short_bat_b = FALSE;
2971  09e8 e680              	ldab	OFST-1,s
2972  09ea b796              	exg	b,y
2973  09ec 0dea011910        	bclr	_hs_status_flags_c,y,16
2974  09f1                   L1541:
2975                         ; 2504 				hsLF_2SecMonitorTime_FaultDetection(hs_seat);
2977  09f1 e680              	ldab	OFST-1,s
2978  09f3 87                	clra	
2979  09f4 4a152e2e          	call	f_hsLF_2SecMonitorTime_FaultDetection
2981                         ; 2309 		for (hs_seat = FRONT_LEFT; hs_seat < ALL_HEAT_SEATS; hs_seat++)
2983  09f8 6280              	inc	OFST-1,s
2986  09fa e680              	ldab	OFST-1,s
2987  09fc c104              	cmpb	#4
2988  09fe 1825fe79          	blo	L7631
2990  0a02 204e              	bra	L1051
2991  0a04                   L5631:
2992                         ; 2519     	for (hs_seat = FRONT_LEFT; hs_seat < ALL_HEAT_SEATS; hs_seat++)
2994  0a04 c7                	clrb	
2995  0a05 6b80              	stab	OFST-1,s
2996  0a07                   L3051:
2997                         ; 2522     		hs_monitor_prelim_fault_counter_c[hs_seat] = 0;
2999  0a07 87                	clra	
3000  0a08 b746              	tfr	d,y
3001  0a0a 6aea0037          	staa	_hs_monitor_prelim_fault_counter_c,y
3002                         ; 2524     		if (main_CSWM_Status_c == UGLY) 
3004  0a0e f60000            	ldab	_main_CSWM_Status_c
3005  0a11 c103              	cmpb	#3
3006  0a13 262c              	bne	L1151
3007                         ; 2527 				if(hs_status3_flags_c[ hs_seat ].b.first_time_ugly_b == FALSE /*||
3007                         ; 2528 				  (hs_status3_flags_c[ hs_seat ].b.first_time_ugly_b == TRUE && hs_PWM_off_counter_c < 10)*/ )
3009  0a15 0eea01111004      	brset	_hs_status3_flags_c,y,16,L3151
3010                         ; 2532                 hs_pid_setpoint[hs_seat] = SET_OFF_REQUEST;
3012  0a1b 69ea006e          	clr	_hs_pid_setpoint,y
3013  0a1f                   L3151:
3014                         ; 2539 				if((hs_rem_seat_req_c[hs_seat] != SET_OFF_REQUEST) &&
3014                         ; 2540 					(hs_status3_flags_c[ hs_seat ].b.first_time_ugly_b == FALSE) )
3016  0a1f e680              	ldab	OFST-1,s
3017  0a21 b796              	exg	b,y
3018  0a23 e6ea00fd          	ldab	_hs_rem_seat_req_c,y
3019  0a27 270d              	beq	L5151
3021  0a29 0eea01111007      	brset	_hs_status3_flags_c,y,16,L5151
3022                         ; 2542 					hs_monitor_2sec_led_counter_c[hs_seat] = /*mature_2sec_time_c*/hs_flt_dtc_c[HS_2SEC_MATURE_TIME];
3024  0a2f 1809ea00f900cf    	movb	_hs_flt_dtc_c+4,_hs_monitor_2sec_led_counter_c,y
3025  0a36                   L5151:
3026                         ; 2544 				hs_status3_flags_c[ hs_seat ].b.first_time_ugly_b = TRUE;
3028  0a36 e680              	ldab	OFST-1,s
3029  0a38 b796              	exg	b,y
3030  0a3a 0cea011110        	bset	_hs_status3_flags_c,y,16
3032  0a3f 2009              	bra	L7151
3033  0a41                   L1151:
3034                         ; 2549 				hs_status3_flags_c[ hs_seat ].b.first_time_ugly_b = FALSE;
3036  0a41 e680              	ldab	OFST-1,s
3037  0a43 b746              	tfr	d,y
3038  0a45 0dea011110        	bclr	_hs_status3_flags_c,y,16
3039  0a4a                   L7151:
3040                         ; 2519     	for (hs_seat = FRONT_LEFT; hs_seat < ALL_HEAT_SEATS; hs_seat++)
3042  0a4a 6280              	inc	OFST-1,s
3045  0a4c e680              	ldab	OFST-1,s
3046  0a4e c104              	cmpb	#4
3047  0a50 25b5              	blo	L3051
3048  0a52                   L1051:
3049                         ; 2556 }
3052  0a52 1b81              	leas	1,s
3053  0a54 0a                	rtc	
3097                         ; 2578 void hsLF_2SecLEDDisplay(void)
3097                         ; 2579 {
3098                         	switch	.ftext
3099  0a55                   f_hsLF_2SecLEDDisplay:
3101  0a55 37                	pshb	
3102       00000001          OFST:	set	1
3105                         ; 2583 	for (hs_seat = FRONT_LEFT; hs_seat < ALL_HEAT_SEATS; hs_seat++) {
3107  0a56 6980              	clr	OFST-1,s
3108  0a58                   L5351:
3109                         ; 2589 		if ((main_CSWM_Status_c == UGLY) ||
3109                         ; 2590 		   (hs_status_flags_c[ hs_seat ].b.hs_final_open_b) ||
3109                         ; 2591 		   (hs_status_flags_c[ hs_seat ].b.hs_final_partial_open_b) ||
3109                         ; 2592 		   (hs_status_flags_c[ hs_seat ].b.hs_final_short_gnd_b) ||
3109                         ; 2593 		   (hs_control_flags_c[ hs_seat ].b.ntc_greater_70c_b) ||
3109                         ; 2594 		   (hs_seat == REAR_LEFT && hs_rear_seat_flags_c[ RL_SWITCH ].b.stksw_psd) ||
3109                         ; 2595 		   (hs_seat == REAR_RIGHT && hs_rear_seat_flags_c[ RR_SWITCH ].b.stksw_psd)   )
3111  0a58 f60000            	ldab	_main_CSWM_Status_c
3112  0a5b c103              	cmpb	#3
3113  0a5d 272f              	beq	L5451
3115  0a5f e680              	ldab	OFST-1,s
3116  0a61 87                	clra	
3117  0a62 b746              	tfr	d,y
3118  0a64 0eea01190824      	brset	_hs_status_flags_c,y,8,L5451
3120  0a6a 0eea0119801e      	brset	_hs_status_flags_c,y,128,L5451
3122  0a70 0eea01190218      	brset	_hs_status_flags_c,y,2,L5451
3124  0a76 0eea011d1012      	brset	_hs_control_flags_c,y,16,L5451
3126  0a7c c102              	cmpb	#2
3127  0a7e 2605              	bne	L1651
3129  0a80 1e010f0109        	brset	_hs_rear_seat_flags_c,1,L5451
3130  0a85                   L1651:
3132  0a85 c103              	cmpb	#3
3133  0a87 261f              	bne	L3451
3135  0a89 1f0110011a        	brclr	_hs_rear_seat_flags_c+1,1,L3451
3136  0a8e                   L5451:
3137                         ; 2598 			if (hs_rem_seat_req_c[hs_seat] != SET_OFF_REQUEST)
3139  0a8e e680              	ldab	OFST-1,s
3140  0a90 87                	clra	
3141  0a91 b746              	tfr	d,y
3142  0a93 e6ea00fd          	ldab	_hs_rem_seat_req_c,y
3143  0a97 182700b2          	beq	L3751
3144                         ; 2600 				if (hs_monitor_2sec_led_counter_c[hs_seat] >= /*mature_2sec_time_c*/hs_flt_dtc_c[HS_2SEC_MATURE_TIME])
3146  0a9b e6ea00f9          	ldab	_hs_monitor_2sec_led_counter_c,y
3147  0a9f f100cf            	cmpb	_hs_flt_dtc_c+4
3148  0aa2 18250086          	blo	L3461
3149                         ; 2603 					hsLF_UpdateHeatParameters(hs_seat, HEAT_LEVEL_OFF);
3152                         ; 2606 					hs_rem_seat_req_c[hs_seat] = hs_switch_request_to_fet_c[hs_seat];
3154                         ; 2609 					hs_monitor_2sec_led_counter_c[hs_seat] = 0;
3157  0aa6 2066              	bra	LC018
3158                         ; 2614 					hs_monitor_2sec_led_counter_c[hs_seat]++;
3160                         ; 2616 					hs_switch_request_to_fet_c[hs_seat] = SET_OFF_REQUEST;
3162  0aa8                   L3451:
3163                         ; 2659 				if ((hs_seat == FRONT_LEFT) ||(hs_seat == FRONT_RIGHT))
3165  0aa8 044104            	tbeq	b,L7751
3167  0aab c101              	cmpb	#1
3168  0aad 2639              	bne	L5751
3169  0aaf                   L7751:
3170                         ; 2661 					if (relay_A_internal_Fault_b || (hs_status_flags_c[ FRONT_LEFT ].b.hs_final_short_bat_b) ||
3170                         ; 2662 					   (hs_status_flags_c[ FRONT_RIGHT ].b.hs_final_short_bat_b) ||(vs_shrtTo_batt_mntr_c == VS_SHRT_TO_BATT))
3172  0aaf 1e00001013        	brset	_relay_control_c,16,L3061
3174  0ab4 1e0119200e        	brset	_hs_status_flags_c,32,L3061
3176  0ab9 1e011a2009        	brset	_hs_status_flags_c+1,32,L3061
3178  0abe f60000            	ldab	_vs_shrtTo_batt_mntr_c
3179  0ac1 c102              	cmpb	#2
3180  0ac3 2616              	bne	L1061
3181  0ac5 e680              	ldab	OFST-1,s
3182  0ac7                   L3061:
3183                         ; 2665 						if (hs_rem_seat_req_c[hs_seat] != SET_OFF_REQUEST)
3185  0ac7 87                	clra	
3186  0ac8 b746              	tfr	d,y
3187  0aca e6ea00fd          	ldab	_hs_rem_seat_req_c,y
3188  0ace 277d              	beq	L3751
3189                         ; 2667 							if (hs_monitor_2sec_led_counter_c[hs_seat] >= /*mature_2sec_time_c*/hs_flt_dtc_c[HS_2SEC_MATURE_TIME])
3191  0ad0 e6ea00f9          	ldab	_hs_monitor_2sec_led_counter_c,y
3192  0ad4 f100cf            	cmpb	_hs_flt_dtc_c+4
3193  0ad7 2553              	blo	L3461
3194                         ; 2670 								hsLF_UpdateHeatParameters(hs_seat, HEAT_LEVEL_OFF);
3197                         ; 2673 								hs_rem_seat_req_c[hs_seat] = hs_switch_request_to_fet_c[hs_seat];
3199                         ; 2676 								hs_monitor_2sec_led_counter_c[hs_seat] = 0;
3202  0ad9 2033              	bra	LC018
3203                         ; 2682 								hs_monitor_2sec_led_counter_c[hs_seat]++;
3205                         ; 2684 								hs_switch_request_to_fet_c[hs_seat] = SET_OFF_REQUEST;
3207  0adb                   L1061:
3208                         ; 2696 						if (main_CSWM_Status_c == BAD)
3210  0adb f60000            	ldab	_main_CSWM_Status_c
3211  0ade c102              	cmpb	#2
3212  0ae0 265f              	bne	L3561
3213                         ; 2699 							hs_switch_request_to_fet_c[hs_seat] = SET_OFF_REQUEST;
3215  0ae2                   LC017:
3216  0ae2 e680              	ldab	OFST-1,s
3217  0ae4 b796              	exg	b,y
3219  0ae6 204c              	bra	LC015
3220                         ; 2705 							hs_switch_request_to_fet_c[ hs_seat ] = hs_rem_seat_req_c[hs_seat];
3222  0ae8                   L5751:
3223                         ; 2715 					if (relay_B_internal_Fault_b || (hs_status_flags_c[ REAR_LEFT ].b.hs_final_short_bat_b) ||
3223                         ; 2716 					   (hs_status_flags_c[ REAR_RIGHT ].b.hs_final_short_bat_b) || Rear_Seats_no_power_final_b)
3225  0ae8 1e0000200f        	brset	_relay_control_c,32,L3361
3227  0aed 1e011b200a        	brset	_hs_status_flags_c+2,32,L3361
3229  0af2 1e011c2005        	brset	_hs_status_flags_c+3,32,L3361
3231  0af7 1f0000023e        	brclr	_relay_control2_c,2,L1361
3232  0afc                   L3361:
3233                         ; 2718 						if (hs_rem_seat_req_c[hs_seat] != SET_OFF_REQUEST)
3235  0afc 87                	clra	
3236  0afd b746              	tfr	d,y
3237  0aff e6ea00fd          	ldab	_hs_rem_seat_req_c,y
3238  0b03 2748              	beq	L3751
3239                         ; 2720 							if (hs_monitor_2sec_led_counter_c[hs_seat] >= /*mature_2sec_time_c*/hs_flt_dtc_c[HS_2SEC_MATURE_TIME])
3241  0b05 e6ea00f9          	ldab	_hs_monitor_2sec_led_counter_c,y
3242  0b09 f100cf            	cmpb	_hs_flt_dtc_c+4
3243  0b0c 251e              	blo	L3461
3244                         ; 2723 								hsLF_UpdateHeatParameters(hs_seat, HEAT_LEVEL_OFF);
3247                         ; 2726 								hs_rem_seat_req_c[hs_seat] = hs_switch_request_to_fet_c[hs_seat];
3249                         ; 2729 								hs_monitor_2sec_led_counter_c[hs_seat] = 0;
3251  0b0e                   LC018:
3252  0b0e cc0007            	ldd	#7
3253  0b11 3b                	pshd	
3254  0b12 e682              	ldab	OFST+1,s
3255  0b14 4a0d5050          	call	f_hsLF_UpdateHeatParameters
3256  0b18 1b82              	leas	2,s
3257  0b1a e680              	ldab	OFST-1,s
3258  0b1c b796              	exg	b,y
3259  0b1e 180aea0053ea00fd  	movb	_hs_switch_request_to_fet_c,y,_hs_rem_seat_req_c,y
3260  0b26 69ea00f9          	clr	_hs_monitor_2sec_led_counter_c,y
3262  0b2a 2021              	bra	L3751
3263  0b2c                   L3461:
3264                         ; 2735 								hs_monitor_2sec_led_counter_c[hs_seat]++;
3266  0b2c e680              	ldab	OFST-1,s
3267  0b2e b746              	tfr	d,y
3268  0b30 62ea00f9          	inc	_hs_monitor_2sec_led_counter_c,y
3269                         ; 2737 								hs_switch_request_to_fet_c[hs_seat] = SET_OFF_REQUEST;
3271  0b34                   LC015:
3272  0b34 69ea0053          	clr	_hs_switch_request_to_fet_c,y
3273  0b38 2013              	bra	L3751
3274  0b3a                   L1361:
3275                         ; 2749 						if (main_CSWM_Status_c == BAD)
3277  0b3a f60000            	ldab	_main_CSWM_Status_c
3278  0b3d c102              	cmpb	#2
3279                         ; 2752 							hs_switch_request_to_fet_c[hs_seat] = SET_OFF_REQUEST;
3282  0b3f 27a1              	beq	LC017
3283  0b41                   L3561:
3284                         ; 2758 							hs_switch_request_to_fet_c[ hs_seat ] = hs_rem_seat_req_c[hs_seat];
3286  0b41 e680              	ldab	OFST-1,s
3287  0b43 b796              	exg	b,y
3288  0b45 180aea00fdea0053  	movb	_hs_rem_seat_req_c,y,_hs_switch_request_to_fet_c,y
3289  0b4d                   L3751:
3290                         ; 2583 	for (hs_seat = FRONT_LEFT; hs_seat < ALL_HEAT_SEATS; hs_seat++) {
3292  0b4d 6280              	inc	OFST-1,s
3295  0b4f e680              	ldab	OFST-1,s
3296  0b51 c104              	cmpb	#4
3297  0b53 1825ff01          	blo	L5351
3298                         ; 2768 }
3301  0b57 1b81              	leas	1,s
3302  0b59 0a                	rtc	
3367                         ; 2793 void hsLF_ClearHeatSequence(void)
3367                         ; 2794 {
3368                         	switch	.ftext
3369  0b5a                   f_hsLF_ClearHeatSequence:
3371  0b5a 37                	pshb	
3372       00000001          OFST:	set	1
3375                         ; 2797 	for (hs_seat = FRONT_LEFT; hs_seat < ALL_HEAT_SEATS; hs_seat++ )
3377  0b5b c7                	clrb	
3378  0b5c 6b80              	stab	OFST-1,s
3379  0b5e                   L3761:
3380                         ; 2800         hs_pid_setpoint[hs_seat] = SET_OFF_REQUEST;
3382  0b5e 87                	clra	
3383  0b5f b746              	tfr	d,y
3384  0b61 6aea006e          	staa	_hs_pid_setpoint,y
3385                         ; 2802 		hs_control_flags_c[ hs_seat ].cb = 0; //Holds the Heat start timer bit, Pre Heat Bit and Message flow ctrl bit
3387  0b65 6aea011d          	staa	_hs_control_flags_c,y
3388                         ; 2803 		hs_status_flags_c[ hs_seat ].cb = 0;  // Holds the status bits
3390  0b69 6aea0119          	staa	_hs_status_flags_c,y
3391                         ; 2804 		hs_status2_flags_c[ hs_seat ].cb = 0;  // Holds the status bits
3393  0b6d 6aea0115          	staa	_hs_status2_flags_c,y
3394                         ; 2805 		hs_status3_flags_c[ hs_seat ].cb = 0;  // Holds the status bits
3396  0b71 6aea0111          	staa	_hs_status3_flags_c,y
3397                         ; 2806 		hs_switch_request_to_fet_c [hs_seat] = 0; //Holds the actual request to FET,Initially SET_OFF_REQUEST
3399  0b75 6aea0053          	staa	_hs_switch_request_to_fet_c,y
3400                         ; 2807 		hs_allowed_heat_pwm_c [hs_seat] = 0; //Holds the current PWM for the current level
3402  0b79 6aea0105          	staa	_hs_allowed_heat_pwm_c,y
3403                         ; 2808 		hs_allowed_heat_time_c [hs_seat] = 0; //Holds the current Timeout value for the PWM.
3405  0b7d 6aea004b          	staa	_hs_allowed_heat_time_c,y
3406                         ; 2809 		hs_wait_time_check_NTC_c[hs_seat] = 0; //Holds the wait time period to check the NTC feedback for current level
3408  0b81 6aea0047          	staa	_hs_wait_time_check_NTC_c,y
3409                         ; 2810 		hs_allowed_NTC_c[hs_seat] = 0; //Holds the NTC Thresholds for current Level
3411  0b85 6aea0043          	staa	_hs_allowed_NTC_c,y
3412                         ; 2812 		hs_temp_seat_stat_c [hs_seat] = 0; //Holds the current seat stat
3414  0b89 6aea003b          	staa	_hs_temp_seat_stat_c,y
3415                         ; 2813 		hs_rem_seat_req_c [hs_seat] = 0;   // Holds the current seat request state and can be used when faults were removed
3417  0b8d 6aea00fd          	staa	_hs_rem_seat_req_c,y
3418                         ; 2814 		hs_output_status_c [hs_seat] = 0; // Output Heat seat status display using KWP
3420  0b91 6aea0101          	staa	_hs_output_status_c,y
3421                         ; 2815 		hs_time_counter_w[ hs_seat ] = 0x00000; //Clear the timeout counter
3423  0b95 1858              	lsly	
3424  0b97 1869ea00dc        	clrw	_hs_time_counter_w,y
3425                         ; 2816 		hs_status2_flags_c[ hs_seat ].b.hs_heat_level_off_b = TRUE; //Holds the heater status.
3427  0b9c b746              	tfr	d,y
3428  0b9e 0cea011520        	bset	_hs_status2_flags_c,y,32
3429                         ; 2817 		hs_rear_seat_led_status_c [hs_seat] = CAN_LED_OFF; //Rear Seat LED status set to Zero
3431  0ba3 69ea0063          	clr	_hs_rear_seat_led_status_c,y
3432                         ; 2821 		hs_Vsense_c [hs_seat] = 0;
3434  0ba7 69ea00f1          	clr	_hs_Vsense_c,y
3435                         ; 2822 		hs_Isense_c [hs_seat] = 0;
3437  0bab 69ea00ed          	clr	_hs_Isense_c,y
3438                         ; 2823 		hs_Zsense_c [hs_seat] = 0;
3440  0baf 69ea00e9          	clr	_hs_Zsense_c,y
3441                         ; 2797 	for (hs_seat = FRONT_LEFT; hs_seat < ALL_HEAT_SEATS; hs_seat++ )
3443  0bb3 6280              	inc	OFST-1,s
3446  0bb5 e680              	ldab	OFST-1,s
3447  0bb7 c104              	cmpb	#4
3448  0bb9 25a3              	blo	L3761
3449                         ; 2828 	hs_temp_set_c.cb = 0; //WHen IGN is Not in Run/Start clear the NTC Fault data flag.
3451  0bbb 790019            	clr	_hs_temp_set_c
3452                         ; 2830 	hsLF_Timer(ALL_HEAT_SEATS, TIMER_CLEAR);  //Clear the Time
3454  0bbe cc0002            	ldd	#2
3455  0bc1 3b                	pshd	
3456  0bc2 c604              	ldab	#4
3457  0bc4 4a0d1111          	call	f_hsLF_Timer
3459                         ; 2831 	hsLF_SendHeatedSeatCanStatus(ALL_HEAT_SEATS, CAN_LED_OFF); //Initially set the CAN_LED_OFF status on Bus
3461  0bc8 87                	clra	
3462  0bc9 c7                	clrb	
3463  0bca 6c80              	std	0,s
3464  0bcc c604              	ldab	#4
3465  0bce 4a0c0909          	call	f_hsLF_SendHeatedSeatCanStatus
3467                         ; 2835 	Dio_WriteChannel(LED1_RL_EN, STD_LOW);
3470  0bd2 1410              	sei	
3475  0bd4 fd0012            	ldy	_Dio_PortWrite_Ptr+18
3476  0bd7 0d4020            	bclr	0,y,32
3480  0bda 10ef              	cli	
3482                         ; 2837 	Dio_WriteChannel(LED2_RL_EN, STD_LOW);
3486  0bdc 1410              	sei	
3491  0bde 0d4010            	bclr	0,y,16
3495  0be1 10ef              	cli	
3497                         ; 2840 	Dio_WriteChannel(LED1_RR_EN, STD_LOW);
3501  0be3 1410              	sei	
3506  0be5 0d4001            	bclr	0,y,1
3510  0be8 10ef              	cli	
3512                         ; 2842 	Dio_WriteChannel(LED2_RR_EN, STD_LOW);
3516  0bea 1410              	sei	
3521  0bec 0d4080            	bclr	0,y,128
3525  0bef 10ef              	cli	
3527                         ; 2846 	RemSt_activated_b = FALSE;
3530  0bf1 1d000040          	bclr	_main_flag_c,64
3531                         ; 2848 	hs_rear_swpsd_stat_c [RL_SWITCH] = 0; //Rear seat switch press status shall be cleared.
3533                         ; 2849 	hs_rear_swpsd_stat_c [RR_SWITCH] = 0; //Rear seat switch press status shall be cleared.
3535  0bf5 87                	clra	
3536  0bf6 c7                	clrb	
3537  0bf7 7c0109            	std	_hs_rear_swpsd_stat_c
3538                         ; 2854     hs_temp_seat_req_c[FRONT_LEFT]=0;
3540                         ; 2855 	hs_temp_seat_req_c[FRONT_RIGHT]=0;
3542  0bfa 7c003f            	std	_hs_temp_seat_req_c
3543                         ; 2856 	hs_temp_seat_req_c[REAR_LEFT]=1;
3545  0bfd 52                	incb	
3546  0bfe 7b0041            	stab	_hs_temp_seat_req_c+2
3547                         ; 2857 	hs_temp_seat_req_c[REAR_RIGHT]=1;
3549  0c01 c601              	ldab	#1
3550  0c03 7b0042            	stab	_hs_temp_seat_req_c+3
3551                         ; 2859 }
3554  0c06 1b83              	leas	3,s
3555  0c08 0a                	rtc	
3603                         ; 2890 void hsLF_SendHeatedSeatCanStatus(unsigned char hs_side,unsigned char action_c)
3603                         ; 2891 {
3604                         	switch	.ftext
3605  0c09                   f_hsLF_SendHeatedSeatCanStatus:
3607  0c09 3b                	pshd	
3608  0c0a 37                	pshb	
3609       00000001          OFST:	set	1
3612                         ; 2895 	switch (hs_side) {
3615  0c0b 04410e            	tbeq	b,L1071
3616  0c0e 04010f            	dbeq	b,L3071
3617  0c11 040110            	dbeq	b,L5071
3618  0c14 040111            	dbeq	b,L7071
3619  0c17 040112            	dbeq	b,L1171
3620  0c1a 2014              	bra	L7371
3621  0c1c                   L1071:
3622                         ; 2900 			hs_seat_mask_c = HS_FL_SEAT_MASK;
3624  0c1c c601              	ldab	#1
3625                         ; 2901 			break;
3627  0c1e 200e              	bra	LC019
3628  0c20                   L3071:
3629                         ; 2907 			hs_seat_mask_c = HS_FR_SEAT_MASK;
3631  0c20 c602              	ldab	#2
3632                         ; 2908 			break;
3634  0c22 200a              	bra	LC019
3635  0c24                   L5071:
3636                         ; 2914 			hs_seat_mask_c = HS_RL_SEAT_MASK;
3638  0c24 c604              	ldab	#4
3639                         ; 2915 			break;
3641  0c26 2006              	bra	LC019
3642  0c28                   L7071:
3643                         ; 2921 			hs_seat_mask_c = HS_RR_SEAT_MASK;
3645  0c28 c608              	ldab	#8
3646                         ; 2922 			break;
3648  0c2a 2002              	bra	LC019
3649  0c2c                   L1171:
3650                         ; 2928 			hs_seat_mask_c = HS_ALL_SEAT_MASK;
3652  0c2c c60f              	ldab	#15
3653  0c2e                   LC019:
3654  0c2e 6b80              	stab	OFST-1,s
3655                         ; 2929 			break;
3657  0c30                   L7371:
3658                         ; 2940 	if (HS_FL_SEAT_MASK & hs_seat_mask_c)
3660  0c30 0f800107          	brclr	OFST-1,s,1,L1471
3661                         ; 2943 		IlPutTxFL_HS_STATSts(action_c);
3663  0c34 e687              	ldab	OFST+6,s
3664  0c36 87                	clra	
3665  0c37 4a000000          	call	f_IlPutTxFL_HS_STATSts
3667  0c3b                   L1471:
3668                         ; 2948 	if (HS_FR_SEAT_MASK & hs_seat_mask_c)
3670  0c3b 0f800207          	brclr	OFST-1,s,2,L3471
3671                         ; 2951 		IlPutTxFR_HS_STATSts(action_c);
3673  0c3f e687              	ldab	OFST+6,s
3674  0c41 87                	clra	
3675  0c42 4a000000          	call	f_IlPutTxFR_HS_STATSts
3677  0c46                   L3471:
3678                         ; 2954 }
3681  0c46 1b83              	leas	3,s
3682  0c48 0a                	rtc	
3747                         ; 2977 void hsLF_SendRearSeat_HWStatus(unsigned char hs_rear,unsigned char action_c)
3747                         ; 2978 {
3748                         	switch	.ftext
3749  0c49                   f_hsLF_SendRearSeat_HWStatus:
3751  0c49 3b                	pshd	
3752       00000000          OFST:	set	0
3755                         ; 2980 	switch (hs_rear) {
3758  0c4a c002              	subb	#2
3759  0c4c 2707              	beq	L5471
3760  0c4e 53                	decb	
3761  0c4f 2748              	beq	L7471
3762  0c51 18200094          	bra	L3771
3763  0c55                   L5471:
3764                         ; 2985 			if (action_c == CAN_LED_HI) {
3766  0c55 e686              	ldab	OFST+6,s
3767  0c57 c103              	cmpb	#3
3768  0c59 2611              	bne	L5771
3769                         ; 2988 				Dio_WriteChannel(LED1_RL_EN, STD_HIGH);
3772  0c5b 1410              	sei	
3777  0c5d fd0012            	ldy	_Dio_PortWrite_Ptr+18
3778  0c60 0c4020            	bset	0,y,32
3782  0c63 10ef              	cli	
3784                         ; 2990 				Dio_WriteChannel(LED2_RL_EN, STD_HIGH);
3788  0c65 1410              	sei	
3793  0c67 0c4010            	bset	0,y,16
3798                         ; 2992 				hs_rear_seat_led_status_c [hs_rear] = CAN_LED_HI;
3802  0c6a 2042              	bra	LC022
3803  0c6c                   L5771:
3804                         ; 2994 			} else if (action_c == CAN_LED_LOW){
3806  0c6c 042119            	dbne	b,L1002
3807                         ; 2997 					Dio_WriteChannel(LED1_RL_EN, STD_HIGH);
3810  0c6f 1410              	sei	
3815  0c71 fd0012            	ldy	_Dio_PortWrite_Ptr+18
3816  0c74 0c4020            	bset	0,y,32
3820  0c77 10ef              	cli	
3822                         ; 2999 					Dio_WriteChannel(LED2_RL_EN, STD_LOW);
3826  0c79 1410              	sei	
3831  0c7b 0d4010            	bclr	0,y,16
3836                         ; 3001 					hs_rear_seat_led_status_c [hs_rear] = CAN_LED_LOW;
3839  0c7e                   LC023:
3840  0c7e 10ef              	cli	
3841  0c80 e681              	ldab	OFST+1,s
3842  0c82 b796              	exg	b,y
3843  0c84 c601              	ldab	#1
3845  0c86 202e              	bra	LC021
3846  0c88                   L1002:
3847                         ; 3006 					Dio_WriteChannel(LED1_RL_EN, STD_LOW);
3850  0c88 1410              	sei	
3855  0c8a fd0012            	ldy	_Dio_PortWrite_Ptr+18
3856  0c8d 0d4020            	bclr	0,y,32
3860  0c90 10ef              	cli	
3862                         ; 3008 					Dio_WriteChannel(LED2_RL_EN, STD_LOW);
3866  0c92 1410              	sei	
3871  0c94 0d4010            	bclr	0,y,16
3876                         ; 3010 					hs_rear_seat_led_status_c [hs_rear] = CAN_LED_OFF;
3879  0c97 2046              	bra	LC020
3880  0c99                   L7471:
3881                         ; 3018 			if (action_c == CAN_LED_HI) {
3883  0c99 e686              	ldab	OFST+6,s
3884  0c9b c103              	cmpb	#3
3885  0c9d 261d              	bne	L5002
3886                         ; 3021 				Dio_WriteChannel(LED1_RR_EN, STD_HIGH);
3889  0c9f 1410              	sei	
3894  0ca1 fd0012            	ldy	_Dio_PortWrite_Ptr+18
3895  0ca4 0c4001            	bset	0,y,1
3899  0ca7 10ef              	cli	
3901                         ; 3023 				Dio_WriteChannel(LED2_RR_EN, STD_HIGH);
3905  0ca9 1410              	sei	
3910  0cab 0c4080            	bset	0,y,128
3915                         ; 3025 				hs_rear_seat_led_status_c [hs_rear] = CAN_LED_HI;
3918  0cae                   LC022:
3919  0cae 10ef              	cli	
3920  0cb0 e681              	ldab	OFST+1,s
3921  0cb2 b796              	exg	b,y
3922  0cb4 c603              	ldab	#3
3923  0cb6                   LC021:
3924  0cb6 6bea0063          	stab	_hs_rear_seat_led_status_c,y
3926  0cba 202d              	bra	L3771
3927  0cbc                   L5002:
3928                         ; 3027 			} else if (action_c == CAN_LED_LOW){
3930  0cbc 042111            	dbne	b,L1102
3931                         ; 3030 					Dio_WriteChannel(LED1_RR_EN, STD_HIGH);
3934  0cbf 1410              	sei	
3939  0cc1 fd0012            	ldy	_Dio_PortWrite_Ptr+18
3940  0cc4 0c4001            	bset	0,y,1
3944  0cc7 10ef              	cli	
3946                         ; 3032 					Dio_WriteChannel(LED2_RR_EN, STD_LOW);
3950  0cc9 1410              	sei	
3955  0ccb 0d4080            	bclr	0,y,128
3960                         ; 3034 					hs_rear_seat_led_status_c [hs_rear] = CAN_LED_LOW;
3964  0cce 20ae              	bra	LC023
3965  0cd0                   L1102:
3966                         ; 3039 					Dio_WriteChannel(LED1_RR_EN, STD_LOW);
3969  0cd0 1410              	sei	
3974  0cd2 fd0012            	ldy	_Dio_PortWrite_Ptr+18
3975  0cd5 0d4001            	bclr	0,y,1
3979  0cd8 10ef              	cli	
3981                         ; 3041 					Dio_WriteChannel(LED2_RR_EN, STD_LOW);
3985  0cda 1410              	sei	
3990  0cdc 0d4080            	bclr	0,y,128
3995                         ; 3043 					hs_rear_seat_led_status_c [hs_rear] = CAN_LED_OFF;
3998  0cdf                   LC020:
3999  0cdf 10ef              	cli	
4000  0ce1 e681              	ldab	OFST+1,s
4001  0ce3 b796              	exg	b,y
4002  0ce5 69ea0063          	clr	_hs_rear_seat_led_status_c,y
4003  0ce9                   L3771:
4004                         ; 3054 } //End of hsLF_SendRearSeat_HWStatus
4007  0ce9 31                	puly	
4008  0cea 0a                	rtc	
4044                         ; 3075 void hsLF_GetHeatedSeatCanStatus(unsigned char hs_pstn)
4044                         ; 3076 {
4045                         	switch	.ftext
4046  0ceb                   f_hsLF_GetHeatedSeatCanStatus:
4050                         ; 3086 	switch (hs_pstn) {
4053  0ceb 044104            	tbeq	b,L5102
4054  0cee 04010f            	dbeq	b,L7102
4056  0cf1 0a                	rtc	
4057  0cf2                   L5102:
4058                         ; 3095 			IlEnterCriticalFL_HS_STATSts();
4060  0cf2 4a000000          	call	f_VStdSuspendAllInterrupts
4062                         ; 3096 			hs_temp_seat_stat_c[FRONT_LEFT] = CSWM_A1.CSWM_A1.FL_HS_STATSts;
4064  0cf6 f60000            	ldab	_CSWM_A1
4065  0cf9 c403              	andb	#3
4066  0cfb 7b003b            	stab	_hs_temp_seat_stat_c
4067                         ; 3097 			IlLeaveCriticalFL_HS_STATSts();
4070                         ; 3101 			break;
4072  0cfe 200c              	bra	LC024
4073  0d00                   L7102:
4074                         ; 3112 			IlEnterCriticalFR_HS_STATSts();
4076  0d00 4a000000          	call	f_VStdSuspendAllInterrupts
4078                         ; 3113 			hs_temp_seat_stat_c[FRONT_RIGHT] = CSWM_A1.CSWM_A1.FR_HS_STATSts;
4080  0d04 f60001            	ldab	_CSWM_A1+1
4081  0d07 c403              	andb	#3
4082  0d09 7b003c            	stab	_hs_temp_seat_stat_c+1
4083                         ; 3114 			IlLeaveCriticalFR_HS_STATSts();
4085  0d0c                   LC024:
4086  0d0c 4a000000          	call	f_VStdResumeAllInterrupts
4088                         ; 3117 			break;
4090                         ; 3149 }
4093  0d10 0a                	rtc	
4133                         ; 3165 void hsLF_Timer(unsigned char hs_pos,unsigned char action)
4133                         ; 3166 {
4134                         	switch	.ftext
4135  0d11                   f_hsLF_Timer:
4137  0d11 3b                	pshd	
4138       00000000          OFST:	set	0
4141                         ; 3178 	if (action == TIMER_START) {
4143  0d12 a686              	ldaa	OFST+6,s
4144  0d14 2609              	bne	L1602
4145                         ; 3179 		hs_control_flags_c[ hs_pos ].b.hs_start_timer_b = TRUE;
4147  0d16 b746              	tfr	d,y
4148  0d18 0cea011d01        	bset	_hs_control_flags_c,y,1
4149                         ; 3180 		hs_time_counter_w[hs_pos] = 0x0000;
4152  0d1d 2028              	bra	LC025
4153  0d1f                   L1602:
4154                         ; 3184 		if (hs_pos == ALL_HEAT_SEATS) {
4156  0d1f e681              	ldab	OFST+1,s
4157  0d21 c104              	cmpb	#4
4158  0d23 261b              	bne	L5602
4159                         ; 3186 			for ( hs_pos = FRONT_LEFT; hs_pos < ALL_HEAT_SEATS; hs_pos++ ) {
4161  0d25 c7                	clrb	
4162  0d26 6b81              	stab	OFST+1,s
4163  0d28                   L7602:
4164                         ; 3188 				hs_control_flags_c[ hs_pos ].b.hs_start_timer_b = FALSE;
4166  0d28 b796              	exg	b,y
4167  0d2a 0dea011d01        	bclr	_hs_control_flags_c,y,1
4168                         ; 3189 				hs_time_counter_w[ hs_pos ] = 0x0000;
4170  0d2f 1858              	lsly	
4171  0d31 1869ea00dc        	clrw	_hs_time_counter_w,y
4172                         ; 3186 			for ( hs_pos = FRONT_LEFT; hs_pos < ALL_HEAT_SEATS; hs_pos++ ) {
4174  0d36 6281              	inc	OFST+1,s
4177  0d38 e681              	ldab	OFST+1,s
4178  0d3a c104              	cmpb	#4
4179  0d3c 25ea              	blo	L7602
4181  0d3e 200e              	bra	L3602
4182  0d40                   L5602:
4183                         ; 3193 			hs_control_flags_c[ hs_pos ].b.hs_start_timer_b = FALSE;
4185  0d40 b796              	exg	b,y
4186  0d42 0dea011d01        	bclr	_hs_control_flags_c,y,1
4187                         ; 3194 			hs_time_counter_w[hs_pos] = 0x0000;
4189  0d47                   LC025:
4190  0d47 1858              	lsly	
4191  0d49 1869ea00dc        	clrw	_hs_time_counter_w,y
4192  0d4e                   L3602:
4193                         ; 3197 }
4196  0d4e 31                	puly	
4197  0d4f 0a                	rtc	
4256                         ; 3222 @far void hsLF_UpdateHeatParameters(unsigned char hs_seatpos, unsigned char value_c)
4256                         ; 3223 {
4257                         	switch	.ftext
4258  0d50                   f_hsLF_UpdateHeatParameters:
4260  0d50 3b                	pshd	
4261       00000000          OFST:	set	0
4264                         ; 3226 	hs_status2_flags_c[hs_seatpos].b.hs_HL3_set_b = FALSE;
4266  0d51 b796              	exg	b,y
4267                         ; 3229 	hs_status2_flags_c[hs_seatpos].b.hs_HL2_set_b = FALSE;
4269                         ; 3232 	hs_status2_flags_c[hs_seatpos].b.hs_HL1_set_b = FALSE;
4271                         ; 3235 	hs_status2_flags_c[hs_seatpos].b.hs_LL1_set_b = FALSE;
4273  0d53 0dea011517        	bclr	_hs_status2_flags_c,y,23
4274                         ; 3237 	if (value_c == HEAT_LEVEL_OFF)
4276  0d58 e686              	ldab	OFST+6,s
4277  0d5a c107              	cmpb	#7
4278  0d5c 267d              	bne	L7212
4279                         ; 3240 		hs_allowed_heat_pwm_c[ hs_seatpos ] = 0; 
4281  0d5e 69ea0105          	clr	_hs_allowed_heat_pwm_c,y
4282                         ; 3241 		hs_allowed_heat_time_c[ hs_seatpos ] = 0;
4284  0d62 69ea004b          	clr	_hs_allowed_heat_time_c,y
4285                         ; 3242 		hs_wait_time_check_NTC_c[hs_seatpos] = 0;
4287  0d66 69ea0047          	clr	_hs_wait_time_check_NTC_c,y
4288                         ; 3243 		hs_allowed_NTC_c[hs_seatpos] = 0;
4290  0d6a 69ea0043          	clr	_hs_allowed_NTC_c,y
4291                         ; 3247         hs_pid_setpoint[hs_seatpos] = SET_OFF_REQUEST;
4293  0d6e 69ea006e          	clr	_hs_pid_setpoint,y
4294                         ; 3250 		hs_switch_request_to_fet_c[hs_seatpos] = SET_OFF_REQUEST;
4296  0d72 69ea0053          	clr	_hs_switch_request_to_fet_c,y
4297                         ; 3253 		if (((hs_seatpos == REAR_LEFT)||(hs_seatpos == REAR_RIGHT)) && 
4297                         ; 3254 			((main_SwReq_Rear_c == VEH_SWITCH_REAR_ONLY_K) || (diag_eol_io_lock_b == TRUE)) )
4299  0d76 e681              	ldab	OFST+1,s
4300  0d78 c102              	cmpb	#2
4301  0d7a 2704              	beq	L3312
4303  0d7c c103              	cmpb	#3
4304  0d7e 2656              	bne	L1312
4305  0d80                   L3312:
4307  0d80 f60000            	ldab	_main_SwReq_Rear_c
4308  0d83 c103              	cmpb	#3
4309  0d85 2707              	beq	L5312
4311  0d87 f60000            	ldab	_diag_io_lock3_flags_c
4312  0d8a c501              	bitb	#1
4313  0d8c 2748              	beq	L1312
4314  0d8e                   L5312:
4315                         ; 3257 			hsLF_SendRearSeat_HWStatus(hs_seatpos, CAN_LED_OFF);
4317  0d8e 87                	clra	
4318  0d8f c7                	clrb	
4319  0d90 3b                	pshd	
4320  0d91 e683              	ldab	OFST+3,s
4321  0d93 4a0c4949          	call	f_hsLF_SendRearSeat_HWStatus
4323                         ; 3259 			hsLF_SendHeatedSeatCanStatus(hs_seatpos, CAN_LED_OFF);
4325  0d97 87                	clra	
4326  0d98 c7                	clrb	
4327  0d99 6c80              	std	0,s
4330  0d9b                   L7312:
4331  0d9b e683              	ldab	OFST+3,s
4332  0d9d 4a0c0909          	call	f_hsLF_SendHeatedSeatCanStatus
4333  0da1 1b82              	leas	2,s
4334                         ; 3269 		hs_output_status_c [hs_seatpos] = HS_READ_STATUS_OFF;
4336  0da3 e681              	ldab	OFST+1,s
4337  0da5 87                	clra	
4338  0da6 b746              	tfr	d,y
4339  0da8 69ea0101          	clr	_hs_output_status_c,y
4340                         ; 3272 		hsLF_Timer(hs_seatpos, TIMER_CLEAR);
4342  0dac c602              	ldab	#2
4343  0dae 3b                	pshd	
4344  0daf e683              	ldab	OFST+3,s
4345  0db1 4a0d1111          	call	f_hsLF_Timer
4347  0db5 1b82              	leas	2,s
4348                         ; 3275 		hs_status2_flags_c[hs_seatpos].b.hs_heat_level_off_b = TRUE;
4350  0db7 e681              	ldab	OFST+1,s
4351  0db9 b796              	exg	b,y
4352  0dbb 0dea011540        	bclr	_hs_status2_flags_c,y,64
4353  0dc0 0cea011520        	bset	_hs_status2_flags_c,y,32
4354                         ; 3278 		hs_status2_flags_c[hs_seatpos].b.hs_heater_state_b = FALSE;
4356                         ; 3281 		hs_monitor_prelim_fault_counter_c[hs_seatpos] = 0;
4358  0dc5 69ea0037          	clr	_hs_monitor_prelim_fault_counter_c,y
4359                         ; 3284 		hs_monitor_2sec_led_counter_c[hs_seatpos] = 0;
4361  0dc9 69ea00f9          	clr	_hs_monitor_2sec_led_counter_c,y
4362                         ; 3288 		hs_status_flags_c[ hs_seatpos ].b.hs_prelim_short_bat_b = FALSE;
4364                         ; 3291 		hs_status_flags_c[ hs_seatpos ].b.hs_prelim_short_gnd_b = FALSE;
4366                         ; 3293 		hs_status_flags_c[ hs_seatpos ].b.hs_prelim_open_b = FALSE;
4368                         ; 3295 		hs_status_flags_c[ hs_seatpos ].b.hs_prelim_partial_open_b = FALSE;
4370  0dcd 0dea011955        	bclr	_hs_status_flags_c,y,85
4372  0dd2 18200122          	bra	L1412
4373  0dd6                   L1312:
4374                         ; 3265 			hsLF_SendHeatedSeatCanStatus(hs_seatpos, CAN_LED_OFF);
4376  0dd6 87                	clra	
4377  0dd7 c7                	clrb	
4378  0dd8 3b                	pshd	
4380  0dd9 20c0              	bra	L7312
4381  0ddb                   L7212:
4382                         ; 3304 		if ((hs_seatpos == REAR_LEFT)||(hs_seatpos == REAR_RIGHT))
4384  0ddb e681              	ldab	OFST+1,s
4385  0ddd c102              	cmpb	#2
4386  0ddf 2704              	beq	L5412
4388  0de1 c103              	cmpb	#3
4389  0de3 2646              	bne	L3412
4390  0de5                   L5412:
4391                         ; 3306 				hs_vehicle_line_offset_c= VEH_LINE_REAROFFSET;	
4393  0de5 c601              	ldab	#1
4394  0de7 7b0018            	stab	_hs_vehicle_line_offset_c
4396  0dea e681              	ldab	OFST+1,s
4397  0dec                   L7412:
4398                         ; 3314 		hs_allowed_heat_pwm_c[ hs_seatpos ] = /*hs_heat_pwm_values_c[value_c]*/hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].Pwm_c[value_c];
4400  0dec 87                	clra	
4401  0ded b746              	tfr	d,y
4402  0def f60018            	ldab	_hs_vehicle_line_offset_c
4403  0df2 fb010d            	addb	_hs_seat_config_c
4404  0df5 45                	rola	
4405  0df6 59                	lsld	
4406  0df7 3b                	pshd	
4407  0df8 59                	lsld	
4408  0df9 59                	lsld	
4409  0dfa 59                	lsld	
4410  0dfb a3b1              	subd	2,s+
4411  0dfd b745              	tfr	d,x
4412  0dff e686              	ldab	OFST+6,s
4413  0e01 87                	clra	
4414  0e02 1ae5              	leax	b,x
4415  0e04 180ae20077ea0105  	movb	_hs_cal_prms,x,_hs_allowed_heat_pwm_c,y
4416                         ; 3316 		hs_allowed_heat_time_c[ hs_seatpos ] = /*hs_heat_timeout_values_c[value_c]*/hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].Max_TimeOut_c[value_c];
4418  0e0c 180ae2007bea004b  	movb	_hs_cal_prms+4,x,_hs_allowed_heat_time_c,y
4419                         ; 3320 		hs_allowed_NTC_c[hs_seatpos] = /*hs_NTC_feedback_values_c[value_c]*/hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].NTC_CutOFF_c[value_c];
4421  0e14 180ae2007fea0043  	movb	_hs_cal_prms+8,x,_hs_allowed_NTC_c,y
4422                         ; 3322 		if (value_c == HEAT_LEVEL_LL1)
4424  0e1c 046142            	tbne	b,L1512
4425                         ; 3325 			if (((hs_seatpos == REAR_LEFT)||(hs_seatpos == REAR_RIGHT)) && 
4425                         ; 3326 				((main_SwReq_Rear_c == VEH_SWITCH_REAR_ONLY_K) || (diag_eol_io_lock_b == TRUE)) )
4427  0e1f e681              	ldab	OFST+1,s
4428  0e21 c102              	cmpb	#2
4429  0e23 270b              	beq	L5512
4431  0e25 c103              	cmpb	#3
4432  0e27 2632              	bne	L3512
4433  0e29 2005              	bra	L5512
4434  0e2b                   L3412:
4435                         ; 3310 				hs_vehicle_line_offset_c= VEH_LINE_FRONTOFFSET;   
4437  0e2b 790018            	clr	_hs_vehicle_line_offset_c
4438  0e2e 20bc              	bra	L7412
4439  0e30                   L5512:
4440                         ; 3325 			if (((hs_seatpos == REAR_LEFT)||(hs_seatpos == REAR_RIGHT)) && 
4440                         ; 3326 				((main_SwReq_Rear_c == VEH_SWITCH_REAR_ONLY_K) || (diag_eol_io_lock_b == TRUE)) )
4441  0e30 f60000            	ldab	_main_SwReq_Rear_c
4442  0e33 c103              	cmpb	#3
4443  0e35 2705              	beq	L7512
4445  0e37 1f0000011f        	brclr	_diag_io_lock3_flags_c,1,L3512
4446  0e3c                   L7512:
4447                         ; 3329 				hsLF_SendRearSeat_HWStatus(hs_seatpos, CAN_LED_LOW);
4449  0e3c cc0001            	ldd	#1
4450  0e3f 3b                	pshd	
4451  0e40 e683              	ldab	OFST+3,s
4452  0e42 4a0c4949          	call	f_hsLF_SendRearSeat_HWStatus
4454                         ; 3331 				hsLF_SendHeatedSeatCanStatus(hs_seatpos, CAN_LED_LOW);
4456  0e46 cc0001            	ldd	#1
4457  0e49 6c80              	std	0,s
4460  0e4b                   L1612:
4461  0e4b e683              	ldab	OFST+3,s
4462  0e4d 4a0c0909          	call	f_hsLF_SendHeatedSeatCanStatus
4463  0e51 1b82              	leas	2,s
4464                         ; 3340 			hs_switch_request_to_fet_c[hs_seatpos] = SET_LO_REQUEST;
4466  0e53 e681              	ldab	OFST+1,s
4467  0e55 b796              	exg	b,y
4468  0e57 c601              	ldab	#1
4470  0e59 2039              	bra	L3612
4471  0e5b                   L3512:
4472                         ; 3337 				hsLF_SendHeatedSeatCanStatus(hs_seatpos, CAN_LED_LOW);
4474  0e5b cc0001            	ldd	#1
4475  0e5e 3b                	pshd	
4477  0e5f 20ea              	bra	L1612
4478  0e61                   L1512:
4479                         ; 3345 			if (((hs_seatpos == REAR_LEFT)||(hs_seatpos == REAR_RIGHT)) && 
4479                         ; 3346 				((main_SwReq_Rear_c == VEH_SWITCH_REAR_ONLY_K) || (diag_eol_io_lock_b == TRUE)) )
4481  0e61 e681              	ldab	OFST+1,s
4482  0e63 c102              	cmpb	#2
4483  0e65 2704              	beq	L7612
4485  0e67 c103              	cmpb	#3
4486  0e69 2651              	bne	L5612
4487  0e6b                   L7612:
4489  0e6b f60000            	ldab	_main_SwReq_Rear_c
4490  0e6e c103              	cmpb	#3
4491  0e70 2705              	beq	L1712
4493  0e72 1f00000145        	brclr	_diag_io_lock3_flags_c,1,L5612
4494  0e77                   L1712:
4495                         ; 3349 				hsLF_SendRearSeat_HWStatus(hs_seatpos, CAN_LED_HI);
4497  0e77 cc0003            	ldd	#3
4498  0e7a 3b                	pshd	
4499  0e7b e683              	ldab	OFST+3,s
4500  0e7d 4a0c4949          	call	f_hsLF_SendRearSeat_HWStatus
4502                         ; 3351 				hsLF_SendHeatedSeatCanStatus(hs_seatpos, CAN_LED_HI);
4504  0e81 cc0003            	ldd	#3
4505  0e84 6c80              	std	0,s
4508  0e86                   L3712:
4509  0e86 e683              	ldab	OFST+3,s
4510  0e88 4a0c0909          	call	f_hsLF_SendHeatedSeatCanStatus
4511  0e8c 1b82              	leas	2,s
4512                         ; 3361 			hs_switch_request_to_fet_c[hs_seatpos] = SET_HI_REQUEST;
4514  0e8e e681              	ldab	OFST+1,s
4515  0e90 b796              	exg	b,y
4516  0e92 c602              	ldab	#2
4517  0e94                   L3612:
4518  0e94 6bea0053          	stab	_hs_switch_request_to_fet_c,y
4519                         ; 3366 		hsLF_Timer(hs_seatpos, TIMER_START);
4521  0e98 87                	clra	
4522  0e99 c7                	clrb	
4523  0e9a 3b                	pshd	
4524  0e9b e683              	ldab	OFST+3,s
4525  0e9d 4a0d1111          	call	f_hsLF_Timer
4527  0ea1 1b82              	leas	2,s
4528                         ; 3369 		hs_status2_flags_c[hs_seatpos].b.hs_heat_level_off_b = FALSE;
4530  0ea3 e681              	ldab	OFST+1,s
4531  0ea5 87                	clra	
4532  0ea6 b746              	tfr	d,y
4533  0ea8 0dea011520        	bclr	_hs_status2_flags_c,y,32
4534                         ; 3371 		switch (value_c) {
4536  0ead e686              	ldab	OFST+6,s
4538  0eaf 2738              	beq	L5012
4539  0eb1 040128            	dbeq	b,L3012
4540  0eb4 040118            	dbeq	b,L1012
4541  0eb7 040108            	dbeq	b,L7702
4542  0eba 203c              	bra	L1412
4543  0ebc                   L5612:
4544                         ; 3357 				hsLF_SendHeatedSeatCanStatus(hs_seatpos, CAN_LED_HI);
4546  0ebc cc0003            	ldd	#3
4547  0ebf 3b                	pshd	
4549  0ec0 20c4              	bra	L3712
4550  0ec2                   L7702:
4551                         ; 3376 				hs_status2_flags_c[hs_seatpos].b.hs_HL3_set_b = TRUE;
4553  0ec2 e681              	ldab	OFST+1,s
4554  0ec4 b746              	tfr	d,y
4555  0ec6 0cea011501        	bset	_hs_status2_flags_c,y,1
4556                         ; 3379 				hs_output_status_c [hs_seatpos] = HS_READ_STATUS_HL3; 
4558  0ecb c605              	ldab	#5
4559                         ; 3381 				break;
4561  0ecd 2025              	bra	LC026
4562  0ecf                   L1012:
4563                         ; 3387 				hs_status2_flags_c[hs_seatpos].b.hs_HL2_set_b = TRUE;
4565  0ecf e681              	ldab	OFST+1,s
4566  0ed1 b746              	tfr	d,y
4567  0ed3 0cea011502        	bset	_hs_status2_flags_c,y,2
4568                         ; 3390 				hs_output_status_c [hs_seatpos] = HS_READ_STATUS_HL2; 
4570  0ed8 c604              	ldab	#4
4571                         ; 3392 				break;
4573  0eda 2018              	bra	LC026
4574  0edc                   L3012:
4575                         ; 3398 				hs_status2_flags_c[hs_seatpos].b.hs_HL1_set_b = TRUE;
4577  0edc e681              	ldab	OFST+1,s
4578  0ede b746              	tfr	d,y
4579  0ee0 0cea011504        	bset	_hs_status2_flags_c,y,4
4580                         ; 3401 				hs_output_status_c [hs_seatpos] = HS_READ_STATUS_HL1; 
4582  0ee5 c603              	ldab	#3
4583                         ; 3403 				break;
4585  0ee7 200b              	bra	LC026
4586  0ee9                   L5012:
4587                         ; 3410 				hs_status2_flags_c[hs_seatpos].b.hs_LL1_set_b = TRUE;
4589  0ee9 e681              	ldab	OFST+1,s
4590  0eeb b746              	tfr	d,y
4591  0eed 0cea011510        	bset	_hs_status2_flags_c,y,16
4592                         ; 3413 				hs_output_status_c [hs_seatpos] = HS_READ_STATUS_LL1; 
4594  0ef2 c601              	ldab	#1
4595  0ef4                   LC026:
4596  0ef4 6bea0101          	stab	_hs_output_status_c,y
4597                         ; 3415 				break;
4598  0ef8                   L1412:
4599                         ; 3425 	hs_rem_seat_req_c[hs_seatpos] = hs_switch_request_to_fet_c[hs_seatpos];
4601  0ef8 e681              	ldab	OFST+1,s
4602  0efa b796              	exg	b,y
4603  0efc 180aea0053ea00fd  	movb	_hs_switch_request_to_fet_c,y,_hs_rem_seat_req_c,y
4604                         ; 3426 } //End of Update Parameters function
4607  0f04 31                	puly	
4608  0f05 0a                	rtc	
4646                         ; 3443 @far void hsF_HeatRequestToFet(void)
4646                         ; 3444 {
4647                         	switch	.ftext
4648  0f06                   f_hsF_HeatRequestToFet:
4650  0f06 37                	pshb	
4651       00000001          OFST:	set	1
4654                         ; 3448 	for (hs_position = FRONT_LEFT; hs_position < ALL_HEAT_SEATS; hs_position++ ) {
4656  0f07 6980              	clr	OFST-1,s
4657  0f09                   L1322:
4658                         ; 3452 		if ((hs_position == FRONT_LEFT || hs_position == FRONT_RIGHT) && relay_A_Ok_b)
4660  0f09 e680              	ldab	OFST-1,s
4661  0f0b 2704              	beq	L1422
4663  0f0d c101              	cmpb	#1
4664  0f0f 263f              	bne	L3522
4665  0f11                   L1422:
4667  0f11 1f0000403a        	brclr	_relay_status_c,64,L3522
4668                         ; 3454 			switch (hs_switch_request_to_fet_c[hs_position]) {
4670  0f16 87                	clra	
4671  0f17 b746              	tfr	d,y
4672  0f19 e6ea0053          	ldab	_hs_switch_request_to_fet_c,y
4674  0f1d 2717              	beq	L3022
4675  0f1f 040109            	dbeq	b,L1022
4676  0f22 040106            	dbeq	b,L1022
4677                         ; 3490 					hs_pid_setpoint[hs_position] = SET_OFF_REQUEST;
4679  0f25 69ea006e          	clr	_hs_pid_setpoint,y
4680  0f29 2025              	bra	L3522
4681  0f2b                   L1022:
4682                         ; 3465 					hs_status3_flags_c[hs_position].b.hs_setoff_rq_b = FALSE;
4684  0f2b e680              	ldab	OFST-1,s
4685  0f2d b796              	exg	b,y
4686  0f2f 0dea011104        	bclr	_hs_status3_flags_c,y,4
4687                         ; 3469 					break;
4689  0f34 201a              	bra	L3522
4690  0f36                   L3022:
4691                         ; 3476 					if((hs_pwm_state_c[hs_position] == PWM_HIGH) || (hs_status3_flags_c[hs_position].b.hs_setoff_rq_b == FALSE))
4693  0f36 e680              	ldab	OFST-1,s
4694  0f38 b746              	tfr	d,y
4695  0f3a e6ea00f5          	ldab	_hs_pwm_state_c,y
4696  0f3e 040106            	dbeq	b,L1522
4698  0f41 0eea01110409      	brset	_hs_status3_flags_c,y,4,L3522
4699  0f47                   L1522:
4700                         ; 3479                         hs_pid_setpoint[hs_position] = SET_OFF_REQUEST;
4702  0f47 69ea006e          	clr	_hs_pid_setpoint,y
4703                         ; 3482 						hs_status3_flags_c[hs_position].b.hs_setoff_rq_b = TRUE; 
4705  0f4b 0cea011104        	bset	_hs_status3_flags_c,y,4
4706                         ; 3490 					hs_pid_setpoint[hs_position] = SET_OFF_REQUEST;
4707  0f50                   L3522:
4708                         ; 3499 		if (((CSWM_config_c & CSWM_4HS_VARIANT_MASK_K) == CSWM_4HS_VARIANT_MASK_K))
4710  0f50 1e00e80f02        	brset	_CSWM_config_c,15,LC027
4711  0f55 2049              	bra	L5722
4712  0f57                   LC027:
4713                         ; 3502 			if ((hs_position == REAR_LEFT || hs_position == REAR_RIGHT) && relay_B_Ok_b)
4715  0f57 e680              	ldab	OFST-1,s
4716  0f59 c102              	cmpb	#2
4717  0f5b 2704              	beq	L1622
4719  0f5d c103              	cmpb	#3
4720  0f5f 263f              	bne	L5722
4721  0f61                   L1622:
4723  0f61 1f0000803a        	brclr	_relay_status_c,128,L5722
4724                         ; 3504 				switch (hs_switch_request_to_fet_c[hs_position]) {
4726  0f66 87                	clra	
4727  0f67 b746              	tfr	d,y
4728  0f69 e6ea0053          	ldab	_hs_switch_request_to_fet_c,y
4730  0f6d 2717              	beq	L1122
4731  0f6f 040109            	dbeq	b,L7022
4732  0f72 040106            	dbeq	b,L7022
4733                         ; 3539 						hs_pid_setpoint[hs_position] = SET_OFF_REQUEST;
4735  0f75 69ea006e          	clr	_hs_pid_setpoint,y
4736  0f79 2025              	bra	L5722
4737  0f7b                   L7022:
4738                         ; 3515 						hs_status3_flags_c[hs_position].b.hs_setoff_rq_b = FALSE;
4740  0f7b e680              	ldab	OFST-1,s
4741  0f7d b796              	exg	b,y
4742  0f7f 0dea011104        	bclr	_hs_status3_flags_c,y,4
4743                         ; 3518 						break;
4745  0f84 201a              	bra	L5722
4746  0f86                   L1122:
4747                         ; 3525 						if((hs_pwm_state_c[hs_position] == PWM_HIGH) || (hs_status3_flags_c[hs_position].b.hs_setoff_rq_b == FALSE) )
4749  0f86 e680              	ldab	OFST-1,s
4750  0f88 b746              	tfr	d,y
4751  0f8a e6ea00f5          	ldab	_hs_pwm_state_c,y
4752  0f8e 040106            	dbeq	b,L1722
4754  0f91 0eea01110409      	brset	_hs_status3_flags_c,y,4,L5722
4755  0f97                   L1722:
4756                         ; 3528 							hs_pid_setpoint[hs_position] = SET_OFF_REQUEST;
4758  0f97 69ea006e          	clr	_hs_pid_setpoint,y
4759                         ; 3531 							hs_status3_flags_c[hs_position].b.hs_setoff_rq_b = TRUE;
4761  0f9b 0cea011104        	bset	_hs_status3_flags_c,y,4
4762                         ; 3539 						hs_pid_setpoint[hs_position] = SET_OFF_REQUEST;
4763  0fa0                   L5722:
4764                         ; 3448 	for (hs_position = FRONT_LEFT; hs_position < ALL_HEAT_SEATS; hs_position++ ) {
4766  0fa0 6280              	inc	OFST-1,s
4769  0fa2 e680              	ldab	OFST-1,s
4770  0fa4 c104              	cmpb	#4
4771  0fa6 1825ff5f          	blo	L1322
4772                         ; 3553 }
4775  0faa 1b81              	leas	1,s
4776  0fac 0a                	rtc	
4803                         ; 3566 @far void hsF_HeatDTC(void)
4803                         ; 3567 {
4804                         	switch	.ftext
4805  0fad                   f_hsF_HeatDTC:
4809                         ; 3568 	switch (hs_seat_fault_counter_c) {
4811  0fad f60026            	ldab	_hs_seat_fault_counter_c
4813  0fb0 2718              	beq	L7722
4814  0fb2 53                	decb	
4815  0fb3 182700b3          	beq	L1032
4816  0fb7 53                	decb	
4817  0fb8 1827014e          	beq	L3032
4818  0fbc 53                	decb	
4819  0fbd 182701f4          	beq	L5032
4820  0fc1 53                	decb	
4821  0fc2 1827029a          	beq	L7032
4822                         ; 3891 			hs_seat_fault_counter_c = 0;
4824  0fc6 182002b6          	bra	L7342
4825  0fca                   L7722:
4826                         ; 3574 			if ( hs_status_flags_c[ FRONT_LEFT ].b.hs_final_short_bat_b ) {
4828  0fca 1f01192005        	brclr	_hs_status_flags_c,32,L7232
4829                         ; 3578 				FMemLibF_ReportDtc(DTC_HS_FL_SHORT_TO_BAT_K, ERROR_ON_K);
4831  0fcf cc0001            	ldd	#1
4833                         ; 3581 				FMemLibF_ReportDtc(DTC_HS_FL_SHORT_TO_GND_K, ERROR_OFF_K);
4836                         ; 3582 				FMemLibF_ReportDtc(DTC_HS_FL_OPEN_FULL_K, ERROR_OFF_K);
4839                         ; 3583 				FMemLibF_ReportDtc(DTC_HS_FL_OPEN_PARTIAL_K, ERROR_OFF_K);
4843  0fd2 2059              	bra	LC031
4844  0fd4                   L7232:
4845                         ; 3587 				if (hs_status_flags_c[ FRONT_LEFT ].b.hs_final_short_gnd_b) {
4847  0fd4 1f01190206        	brclr	_hs_status_flags_c,2,L3332
4848                         ; 3591 					FMemLibF_ReportDtc(DTC_HS_FL_SHORT_TO_GND_K, ERROR_ON_K);
4850  0fd9 cc0001            	ldd	#1
4851  0fdc 3b                	pshd	
4853                         ; 3594 					FMemLibF_ReportDtc(DTC_HS_FL_OPEN_FULL_K, ERROR_OFF_K);
4856                         ; 3595 					FMemLibF_ReportDtc(DTC_HS_FL_OPEN_PARTIAL_K, ERROR_OFF_K);
4860  0fdd 205d              	bra	LC030
4861  0fdf                   L3332:
4862                         ; 3599 					if ( hs_status_flags_c[ FRONT_LEFT ].b.hs_final_open_b ) {
4864  0fdf 1f0119081a        	brclr	_hs_status_flags_c,8,L7332
4865                         ; 3603 						FMemLibF_ReportDtc(DTC_HS_FL_OPEN_FULL_K, ERROR_ON_K);
4867  0fe4 cc0001            	ldd	#1
4868  0fe7 3b                	pshd	
4869  0fe8 ce009e            	ldx	#158
4870  0feb cc9913            	ldd	#-26349
4871  0fee 4a000000          	call	f_FMemLibF_ReportDtc
4873                         ; 3606 						FMemLibF_ReportDtc(DTC_HS_FL_SHORT_TO_GND_K, ERROR_OFF_K);
4875  0ff2 87                	clra	
4876  0ff3 c7                	clrb	
4877  0ff4 6c80              	std	0,s
4878  0ff6 ce009e            	ldx	#158
4879  0ff9 cc9911            	ldd	#-26351
4881                         ; 3608 						FMemLibF_ReportDtc(DTC_HS_FL_OPEN_PARTIAL_K, ERROR_OFF_K);
4885  0ffc 2052              	bra	LC029
4886  0ffe                   L7332:
4887                         ; 3612 						if (hs_status_flags_c[ FRONT_LEFT ].b.hs_final_partial_open_b) {
4889  0ffe 1f01198028        	brclr	_hs_status_flags_c,128,L3432
4890                         ; 3616 							FMemLibF_ReportDtc(DTC_HS_FL_OPEN_PARTIAL_K, ERROR_ON_K);
4892  1003 cc0001            	ldd	#1
4893  1006 3b                	pshd	
4894  1007 ce009e            	ldx	#158
4895  100a cc991e            	ldd	#-26338
4896  100d 4a000000          	call	f_FMemLibF_ReportDtc
4898                         ; 3619 							FMemLibF_ReportDtc(DTC_HS_FL_OPEN_FULL_K, ERROR_OFF_K);
4900  1011 87                	clra	
4901  1012 c7                	clrb	
4902  1013 6c80              	std	0,s
4903  1015 ce009e            	ldx	#158
4904  1018 cc9913            	ldd	#-26349
4905  101b 4a000000          	call	f_FMemLibF_ReportDtc
4907                         ; 3620 							FMemLibF_ReportDtc(DTC_HS_FL_SHORT_TO_GND_K, ERROR_OFF_K);
4909  101f 87                	clra	
4910  1020 c7                	clrb	
4911  1021 6c80              	std	0,s
4912  1023 ce009e            	ldx	#158
4913  1026 cc9911            	ldd	#-26351
4916  1029 2033              	bra	L1332
4917  102b                   L3432:
4918                         ; 3625 							FMemLibF_ReportDtc(DTC_HS_FL_SHORT_TO_BAT_K, ERROR_OFF_K);
4920  102b 87                	clra	
4921  102c c7                	clrb	
4923                         ; 3626 							FMemLibF_ReportDtc(DTC_HS_FL_SHORT_TO_GND_K, ERROR_OFF_K);
4925  102d                   LC031:
4926  102d 3b                	pshd	
4927  102e ce009e            	ldx	#158
4928  1031 cc9912            	ldd	#-26350
4929  1034 4a000000          	call	f_FMemLibF_ReportDtc
4930  1038 87                	clra	
4931  1039 c7                	clrb	
4932  103a 6c80              	std	0,s
4934                         ; 3627 							FMemLibF_ReportDtc(DTC_HS_FL_OPEN_FULL_K, ERROR_OFF_K);
4936  103c                   LC030:
4937  103c ce009e            	ldx	#158
4938  103f cc9911            	ldd	#-26351
4939  1042 4a000000          	call	f_FMemLibF_ReportDtc
4940  1046 87                	clra	
4941  1047 c7                	clrb	
4942  1048 6c80              	std	0,s
4943  104a ce009e            	ldx	#158
4944  104d cc9913            	ldd	#-26349
4946                         ; 3628 							FMemLibF_ReportDtc(DTC_HS_FL_OPEN_PARTIAL_K, ERROR_OFF_K);
4948  1050                   LC029:
4949  1050 4a000000          	call	f_FMemLibF_ReportDtc
4950  1054 87                	clra	
4951  1055 c7                	clrb	
4952  1056 6c80              	std	0,s
4953  1058 ce009e            	ldx	#158
4954  105b cc991e            	ldd	#-26338
4956  105e                   L1332:
4957  105e 4a000000          	call	f_FMemLibF_ReportDtc
4958  1062 1b82              	leas	2,s
4959                         ; 3633 			hs_seat_fault_counter_c = 1;
4961  1064 c601              	ldab	#1
4962                         ; 3634 			break;
4964  1066 18200147          	bra	LC043
4965  106a                   L1032:
4966                         ; 3642 			if ( hs_status_flags_c[ FRONT_RIGHT ].b.hs_final_short_bat_b ) {
4968  106a 1f011a2005        	brclr	_hs_status_flags_c+1,32,L7432
4969                         ; 3646 				FMemLibF_ReportDtc(DTC_HS_FR_SHORT_TO_BAT_K, ERROR_ON_K);
4971  106f cc0001            	ldd	#1
4973                         ; 3649 				FMemLibF_ReportDtc(DTC_HS_FR_SHORT_TO_GND_K, ERROR_OFF_K);
4976                         ; 3650 				FMemLibF_ReportDtc(DTC_HS_FR_OPEN_FULL_K, ERROR_OFF_K);
4979                         ; 3651 				FMemLibF_ReportDtc(DTC_HS_FR_OPEN_PARTIAL_K, ERROR_OFF_K);
4983  1072 2059              	bra	LC034
4984  1074                   L7432:
4985                         ; 3655 				if (hs_status_flags_c[ FRONT_RIGHT ].b.hs_final_short_gnd_b) {
4987  1074 1f011a0206        	brclr	_hs_status_flags_c+1,2,L3532
4988                         ; 3659 					FMemLibF_ReportDtc(DTC_HS_FR_SHORT_TO_GND_K, ERROR_ON_K);
4990  1079 cc0001            	ldd	#1
4991  107c 3b                	pshd	
4993                         ; 3662 					FMemLibF_ReportDtc(DTC_HS_FR_OPEN_FULL_K, ERROR_OFF_K);
4996                         ; 3663 					FMemLibF_ReportDtc(DTC_HS_FR_OPEN_PARTIAL_K, ERROR_OFF_K);
5000  107d 205d              	bra	LC033
5001  107f                   L3532:
5002                         ; 3667 					if ( hs_status_flags_c[ FRONT_RIGHT ].b.hs_final_open_b ) {
5004  107f 1f011a081a        	brclr	_hs_status_flags_c+1,8,L7532
5005                         ; 3671 						FMemLibF_ReportDtc(DTC_HS_FR_OPEN_FULL_K, ERROR_ON_K);
5007  1084 cc0001            	ldd	#1
5008  1087 3b                	pshd	
5009  1088 ce009e            	ldx	#158
5010  108b cc9a13            	ldd	#-26093
5011  108e 4a000000          	call	f_FMemLibF_ReportDtc
5013                         ; 3674 						FMemLibF_ReportDtc(DTC_HS_FR_SHORT_TO_GND_K, ERROR_OFF_K);
5015  1092 87                	clra	
5016  1093 c7                	clrb	
5017  1094 6c80              	std	0,s
5018  1096 ce009e            	ldx	#158
5019  1099 cc9a11            	ldd	#-26095
5021                         ; 3675 						FMemLibF_ReportDtc(DTC_HS_FR_OPEN_PARTIAL_K, ERROR_OFF_K);
5025  109c 2052              	bra	LC032
5026  109e                   L7532:
5027                         ; 3679 						if (hs_status_flags_c[ FRONT_RIGHT ].b.hs_final_partial_open_b) {
5029  109e 1f011a8028        	brclr	_hs_status_flags_c+1,128,L3632
5030                         ; 3682 							FMemLibF_ReportDtc(DTC_HS_FR_OPEN_PARTIAL_K, ERROR_ON_K);
5032  10a3 cc0001            	ldd	#1
5033  10a6 3b                	pshd	
5034  10a7 ce009e            	ldx	#158
5035  10aa cc9a1e            	ldd	#-26082
5036  10ad 4a000000          	call	f_FMemLibF_ReportDtc
5038                         ; 3685 							FMemLibF_ReportDtc(DTC_HS_FR_OPEN_FULL_K, ERROR_OFF_K);
5040  10b1 87                	clra	
5041  10b2 c7                	clrb	
5042  10b3 6c80              	std	0,s
5043  10b5 ce009e            	ldx	#158
5044  10b8 cc9a13            	ldd	#-26093
5045  10bb 4a000000          	call	f_FMemLibF_ReportDtc
5047                         ; 3686 							FMemLibF_ReportDtc(DTC_HS_FR_SHORT_TO_GND_K, ERROR_OFF_K);
5049  10bf 87                	clra	
5050  10c0 c7                	clrb	
5051  10c1 6c80              	std	0,s
5052  10c3 ce009e            	ldx	#158
5053  10c6 cc9a11            	ldd	#-26095
5056  10c9 2033              	bra	L1532
5057  10cb                   L3632:
5058                         ; 3691 							FMemLibF_ReportDtc(DTC_HS_FR_SHORT_TO_BAT_K, ERROR_OFF_K);
5060  10cb 87                	clra	
5061  10cc c7                	clrb	
5063                         ; 3692 							FMemLibF_ReportDtc(DTC_HS_FR_SHORT_TO_GND_K, ERROR_OFF_K);
5065  10cd                   LC034:
5066  10cd 3b                	pshd	
5067  10ce ce009e            	ldx	#158
5068  10d1 cc9a12            	ldd	#-26094
5069  10d4 4a000000          	call	f_FMemLibF_ReportDtc
5070  10d8 87                	clra	
5071  10d9 c7                	clrb	
5072  10da 6c80              	std	0,s
5074                         ; 3693 							FMemLibF_ReportDtc(DTC_HS_FR_OPEN_FULL_K, ERROR_OFF_K);
5076  10dc                   LC033:
5077  10dc ce009e            	ldx	#158
5078  10df cc9a11            	ldd	#-26095
5079  10e2 4a000000          	call	f_FMemLibF_ReportDtc
5080  10e6 87                	clra	
5081  10e7 c7                	clrb	
5082  10e8 6c80              	std	0,s
5083  10ea ce009e            	ldx	#158
5084  10ed cc9a13            	ldd	#-26093
5086                         ; 3694 							FMemLibF_ReportDtc(DTC_HS_FR_OPEN_PARTIAL_K, ERROR_OFF_K);
5088  10f0                   LC032:
5089  10f0 4a000000          	call	f_FMemLibF_ReportDtc
5090  10f4 87                	clra	
5091  10f5 c7                	clrb	
5092  10f6 6c80              	std	0,s
5093  10f8 ce009e            	ldx	#158
5094  10fb cc9a1e            	ldd	#-26082
5096  10fe                   L1532:
5097  10fe 4a000000          	call	f_FMemLibF_ReportDtc
5098  1102 1b82              	leas	2,s
5099                         ; 3700 			hs_seat_fault_counter_c = 2;
5101  1104 c602              	ldab	#2
5102                         ; 3701 			break;
5104  1106 182000a7          	bra	LC043
5105  110a                   L3032:
5106                         ; 3707 			if ((CSWM_config_c & CSWM_4HS_VARIANT_MASK_K) == CSWM_4HS_VARIANT_MASK_K)
5108  110a f600e8            	ldab	_CSWM_config_c
5109  110d c40f              	andb	#15
5110  110f c10f              	cmpb	#15
5111  1111 1826016b          	bne	L7342
5112                         ; 3711 				if ( hs_status_flags_c[ REAR_LEFT ].b.hs_final_short_bat_b )
5114  1115 1f011b2005        	brclr	_hs_status_flags_c+2,32,L1732
5115                         ; 3715 					FMemLibF_ReportDtc(DTC_HS_RL_SHORT_TO_BAT_K, ERROR_ON_K);
5117  111a cc0001            	ldd	#1
5119                         ; 3718 					FMemLibF_ReportDtc(DTC_HS_RL_SHORT_TO_GND_K, ERROR_OFF_K);
5122                         ; 3719 					FMemLibF_ReportDtc(DTC_HS_RL_OPEN_FULL_K, ERROR_OFF_K);
5125                         ; 3720 					FMemLibF_ReportDtc(DTC_HS_RL_OPEN_PARTIAL_K, ERROR_OFF_K);
5129  111d 2059              	bra	LC037
5130  111f                   L1732:
5131                         ; 3725 					if (hs_status_flags_c[ REAR_LEFT ].b.hs_final_short_gnd_b)
5133  111f 1f011b0206        	brclr	_hs_status_flags_c+2,2,L5732
5134                         ; 3729 						FMemLibF_ReportDtc(DTC_HS_RL_SHORT_TO_GND_K, ERROR_ON_K);
5136  1124 cc0001            	ldd	#1
5137  1127 3b                	pshd	
5139                         ; 3732 						FMemLibF_ReportDtc(DTC_HS_RL_OPEN_FULL_K, ERROR_OFF_K);
5142                         ; 3733 						FMemLibF_ReportDtc(DTC_HS_RL_OPEN_PARTIAL_K, ERROR_OFF_K);
5146  1128 205d              	bra	LC036
5147  112a                   L5732:
5148                         ; 3738 						if ( hs_status_flags_c[ REAR_LEFT ].b.hs_final_open_b )
5150  112a 1f011b081a        	brclr	_hs_status_flags_c+2,8,L1042
5151                         ; 3742 							FMemLibF_ReportDtc(DTC_HS_RL_OPEN_FULL_K, ERROR_ON_K);
5153  112f cc0001            	ldd	#1
5154  1132 3b                	pshd	
5155  1133 ce009e            	ldx	#158
5156  1136 cc9b13            	ldd	#-25837
5157  1139 4a000000          	call	f_FMemLibF_ReportDtc
5159                         ; 3745 							FMemLibF_ReportDtc(DTC_HS_RL_SHORT_TO_GND_K, ERROR_OFF_K);
5161  113d 87                	clra	
5162  113e c7                	clrb	
5163  113f 6c80              	std	0,s
5164  1141 ce009e            	ldx	#158
5165  1144 cc9b11            	ldd	#-25839
5167                         ; 3746 							FMemLibF_ReportDtc(DTC_HS_RL_OPEN_PARTIAL_K, ERROR_OFF_K);
5171  1147 2052              	bra	LC035
5172  1149                   L1042:
5173                         ; 3751 							if (hs_status_flags_c[ REAR_LEFT ].b.hs_final_partial_open_b)
5175  1149 1f011b8028        	brclr	_hs_status_flags_c+2,128,L5042
5176                         ; 3755 								FMemLibF_ReportDtc(DTC_HS_RL_OPEN_PARTIAL_K, ERROR_ON_K);
5178  114e cc0001            	ldd	#1
5179  1151 3b                	pshd	
5180  1152 ce009e            	ldx	#158
5181  1155 cc9b1e            	ldd	#-25826
5182  1158 4a000000          	call	f_FMemLibF_ReportDtc
5184                         ; 3758 								FMemLibF_ReportDtc(DTC_HS_RL_OPEN_FULL_K, ERROR_OFF_K);
5186  115c 87                	clra	
5187  115d c7                	clrb	
5188  115e 6c80              	std	0,s
5189  1160 ce009e            	ldx	#158
5190  1163 cc9b13            	ldd	#-25837
5191  1166 4a000000          	call	f_FMemLibF_ReportDtc
5193                         ; 3759 								FMemLibF_ReportDtc(DTC_HS_RL_SHORT_TO_GND_K, ERROR_OFF_K);
5195  116a 87                	clra	
5196  116b c7                	clrb	
5197  116c 6c80              	std	0,s
5198  116e ce009e            	ldx	#158
5199  1171 cc9b11            	ldd	#-25839
5202  1174 2033              	bra	L3732
5203  1176                   L5042:
5204                         ; 3765 								FMemLibF_ReportDtc(DTC_HS_RL_SHORT_TO_BAT_K, ERROR_OFF_K);
5206  1176 87                	clra	
5207  1177 c7                	clrb	
5209                         ; 3766 								FMemLibF_ReportDtc(DTC_HS_RL_SHORT_TO_GND_K, ERROR_OFF_K);
5211  1178                   LC037:
5212  1178 3b                	pshd	
5213  1179 ce009e            	ldx	#158
5214  117c cc9b12            	ldd	#-25838
5215  117f 4a000000          	call	f_FMemLibF_ReportDtc
5216  1183 87                	clra	
5217  1184 c7                	clrb	
5218  1185 6c80              	std	0,s
5220                         ; 3767 								FMemLibF_ReportDtc(DTC_HS_RL_OPEN_FULL_K, ERROR_OFF_K);
5222  1187                   LC036:
5223  1187 ce009e            	ldx	#158
5224  118a cc9b11            	ldd	#-25839
5225  118d 4a000000          	call	f_FMemLibF_ReportDtc
5226  1191 87                	clra	
5227  1192 c7                	clrb	
5228  1193 6c80              	std	0,s
5229  1195 ce009e            	ldx	#158
5230  1198 cc9b13            	ldd	#-25837
5232                         ; 3768 								FMemLibF_ReportDtc(DTC_HS_RL_OPEN_PARTIAL_K, ERROR_OFF_K);
5234  119b                   LC035:
5235  119b 4a000000          	call	f_FMemLibF_ReportDtc
5236  119f 87                	clra	
5237  11a0 c7                	clrb	
5238  11a1 6c80              	std	0,s
5239  11a3 ce009e            	ldx	#158
5240  11a6 cc9b1e            	ldd	#-25826
5242  11a9                   L3732:
5243  11a9 4a000000          	call	f_FMemLibF_ReportDtc
5244  11ad 1b82              	leas	2,s
5245                         ; 3773 				hs_seat_fault_counter_c = 3;
5247  11af c603              	ldab	#3
5248  11b1                   LC043:
5249  11b1 7b0026            	stab	_hs_seat_fault_counter_c
5252  11b4 0a                	rtc	
5253                         ; 3777 				hs_seat_fault_counter_c = 0;
5255  11b5                   L5032:
5256                         ; 3787 			if ((CSWM_config_c & CSWM_4HS_VARIANT_MASK_K) == CSWM_4HS_VARIANT_MASK_K)
5258  11b5 f600e8            	ldab	_CSWM_config_c
5259  11b8 c40f              	andb	#15
5260  11ba c10f              	cmpb	#15
5261  11bc 182600c0          	bne	L7342
5262                         ; 3791 				if ( hs_status_flags_c[ REAR_RIGHT ].b.hs_final_short_bat_b )
5264  11c0 1f011c2005        	brclr	_hs_status_flags_c+3,32,L5142
5265                         ; 3795 					FMemLibF_ReportDtc(DTC_HS_RR_SHORT_TO_BAT_K, ERROR_ON_K);
5267  11c5 cc0001            	ldd	#1
5269                         ; 3798 					FMemLibF_ReportDtc(DTC_HS_RR_SHORT_TO_GND_K, ERROR_OFF_K);
5272                         ; 3799 					FMemLibF_ReportDtc(DTC_HS_RR_OPEN_FULL_K, ERROR_OFF_K);
5275                         ; 3800 					FMemLibF_ReportDtc(DTC_HS_RR_OPEN_PARTIAL_K, ERROR_OFF_K);
5279  11c8 2059              	bra	LC040
5280  11ca                   L5142:
5281                         ; 3805 					if (hs_status_flags_c[ REAR_RIGHT ].b.hs_final_short_gnd_b)
5283  11ca 1f011c0206        	brclr	_hs_status_flags_c+3,2,L1242
5284                         ; 3809 						FMemLibF_ReportDtc(DTC_HS_RR_SHORT_TO_GND_K, ERROR_ON_K);
5286  11cf cc0001            	ldd	#1
5287  11d2 3b                	pshd	
5289                         ; 3814 						FMemLibF_ReportDtc(DTC_HS_RR_OPEN_FULL_K, ERROR_OFF_K);
5292                         ; 3815 						FMemLibF_ReportDtc(DTC_HS_RR_OPEN_PARTIAL_K, ERROR_OFF_K);
5296  11d3 205d              	bra	LC039
5297  11d5                   L1242:
5298                         ; 3820 						if ( hs_status_flags_c[ REAR_RIGHT ].b.hs_final_open_b )
5300  11d5 1f011c081a        	brclr	_hs_status_flags_c+3,8,L5242
5301                         ; 3824 							FMemLibF_ReportDtc(DTC_HS_RR_OPEN_FULL_K, ERROR_ON_K);
5303  11da cc0001            	ldd	#1
5304  11dd 3b                	pshd	
5305  11de ce009e            	ldx	#158
5306  11e1 cc9c13            	ldd	#-25581
5307  11e4 4a000000          	call	f_FMemLibF_ReportDtc
5309                         ; 3827 							FMemLibF_ReportDtc(DTC_HS_RR_SHORT_TO_GND_K, ERROR_OFF_K);
5311  11e8 87                	clra	
5312  11e9 c7                	clrb	
5313  11ea 6c80              	std	0,s
5314  11ec ce009e            	ldx	#158
5315  11ef cc9c11            	ldd	#-25583
5317                         ; 3829 							FMemLibF_ReportDtc(DTC_HS_RR_OPEN_PARTIAL_K, ERROR_OFF_K);
5321  11f2 2052              	bra	LC038
5322  11f4                   L5242:
5323                         ; 3834 							if (hs_status_flags_c[ REAR_RIGHT ].b.hs_final_partial_open_b)
5325  11f4 1f011c8028        	brclr	_hs_status_flags_c+3,128,L1342
5326                         ; 3838 								FMemLibF_ReportDtc(DTC_HS_RR_OPEN_PARTIAL_K, ERROR_ON_K);
5328  11f9 cc0001            	ldd	#1
5329  11fc 3b                	pshd	
5330  11fd ce009e            	ldx	#158
5331  1200 cc9c1e            	ldd	#-25570
5332  1203 4a000000          	call	f_FMemLibF_ReportDtc
5334                         ; 3843 								FMemLibF_ReportDtc(DTC_HS_RR_OPEN_FULL_K, ERROR_OFF_K);
5336  1207 87                	clra	
5337  1208 c7                	clrb	
5338  1209 6c80              	std	0,s
5339  120b ce009e            	ldx	#158
5340  120e cc9c13            	ldd	#-25581
5341  1211 4a000000          	call	f_FMemLibF_ReportDtc
5343                         ; 3844 								FMemLibF_ReportDtc(DTC_HS_RR_SHORT_TO_GND_K, ERROR_OFF_K);
5345  1215 87                	clra	
5346  1216 c7                	clrb	
5347  1217 6c80              	std	0,s
5348  1219 ce009e            	ldx	#158
5349  121c cc9c11            	ldd	#-25583
5352  121f 2033              	bra	L7142
5353  1221                   L1342:
5354                         ; 3850 								FMemLibF_ReportDtc(DTC_HS_RR_SHORT_TO_BAT_K, ERROR_OFF_K);
5356  1221 87                	clra	
5357  1222 c7                	clrb	
5359                         ; 3851 								FMemLibF_ReportDtc(DTC_HS_RR_SHORT_TO_GND_K, ERROR_OFF_K);
5361  1223                   LC040:
5362  1223 3b                	pshd	
5363  1224 ce009e            	ldx	#158
5364  1227 cc9c12            	ldd	#-25582
5365  122a 4a000000          	call	f_FMemLibF_ReportDtc
5366  122e 87                	clra	
5367  122f c7                	clrb	
5368  1230 6c80              	std	0,s
5370                         ; 3852 								FMemLibF_ReportDtc(DTC_HS_RR_OPEN_FULL_K, ERROR_OFF_K);
5372  1232                   LC039:
5373  1232 ce009e            	ldx	#158
5374  1235 cc9c11            	ldd	#-25583
5375  1238 4a000000          	call	f_FMemLibF_ReportDtc
5376  123c 87                	clra	
5377  123d c7                	clrb	
5378  123e 6c80              	std	0,s
5379  1240 ce009e            	ldx	#158
5380  1243 cc9c13            	ldd	#-25581
5382                         ; 3853 								FMemLibF_ReportDtc(DTC_HS_RR_OPEN_PARTIAL_K, ERROR_OFF_K);
5384  1246                   LC038:
5385  1246 4a000000          	call	f_FMemLibF_ReportDtc
5386  124a 87                	clra	
5387  124b c7                	clrb	
5388  124c 6c80              	std	0,s
5389  124e ce009e            	ldx	#158
5390  1251 cc9c1e            	ldd	#-25570
5392  1254                   L7142:
5393  1254 4a000000          	call	f_FMemLibF_ReportDtc
5394  1258 1b82              	leas	2,s
5395                         ; 3858 				hs_seat_fault_counter_c = 4;
5397  125a c604              	ldab	#4
5399  125c 1820ff51          	bra	LC043
5400                         ; 3862 				hs_seat_fault_counter_c = 0;
5402  1260                   L7032:
5403                         ; 3871 			if ((CSWM_config_c & CSWM_4HS_VARIANT_MASK_K) == CSWM_4HS_VARIANT_MASK_K)
5405  1260 1e00e80f02        	brset	_CSWM_config_c,15,LC028
5406  1265 2019              	bra	L7342
5407  1267                   LC028:
5408                         ; 3874 				if (Rear_Seats_no_power_final_b)
5410  1267 1f00000205        	brclr	_relay_control2_c,2,L1442
5411                         ; 3877 					FMemLibF_ReportDtc(DTC_NO_BAT_FOR_REAR_SEATS_K, ERROR_ON_K);
5413  126c cc0001            	ldd	#1
5416  126f 2002              	bra	LC041
5417  1271                   L1442:
5418                         ; 3882 					FMemLibF_ReportDtc(DTC_NO_BAT_FOR_REAR_SEATS_K, ERROR_OFF_K);
5420  1271 87                	clra	
5421  1272 c7                	clrb	
5423  1273                   LC041:
5424  1273 3b                	pshd	
5425  1274 ce0091            	ldx	#145
5426  1277 ccdc13            	ldd	#-9197
5427  127a 4a000000          	call	f_FMemLibF_ReportDtc
5428  127e 1b82              	leas	2,s
5429  1280                   L7342:
5430                         ; 3885 			hs_seat_fault_counter_c = 0;
5432  1280 790026            	clr	_hs_seat_fault_counter_c
5433                         ; 3886 			break;
5435                         ; 3894 }
5438  1283 0a                	rtc	
5465                         ; 3924 @far void hsF_NTCDTC(void)
5465                         ; 3925 {
5466                         	switch	.ftext
5467  1284                   f_hsF_NTCDTC:
5471                         ; 3927 	if (!NTC_Supply_Voltage_Flt_b)
5473  1284 f60000            	ldab	_int_flt_byte2
5474  1287 c504              	bitb	#4
5475  1289 1826014f          	bne	L7642
5476                         ; 3929 		switch (hs_ntc_fault_counter_c) {
5478  128d f60025            	ldab	_hs_ntc_fault_counter_c
5480  1290 2711              	beq	L5442
5481  1292 53                	decb	
5482  1293 2758              	beq	L7442
5483  1295 53                	decb	
5484  1296 1827009f          	beq	L1542
5485  129a 53                	decb	
5486  129b 182700ed          	beq	L3542
5487                         ; 4087 				hs_ntc_fault_counter_c = 0;
5489  129f 18200136          	bra	L1352
5490  12a3                   L5442:
5491                         ; 3935 				if (temperature_HS_NTC_status[FRONT_LEFT] == /*NTC_MTRD_OPEN_MASK*/ NTC_OPEN_CKT)
5493  12a3 f60000            	ldab	_temperature_HS_NTC_status
5494  12a6 c102              	cmpb	#2
5495  12a8 261a              	bne	L5742
5496                         ; 3938 					FMemLibF_ReportDtc(DTC_FL_HEAT_NTC_HIGH_K, ERROR_ON_K);
5498  12aa cc0001            	ldd	#1
5499  12ad 3b                	pshd	
5500  12ae ce009e            	ldx	#158
5501  12b1 ccb11b            	ldd	#-20197
5502  12b4 4a000000          	call	f_FMemLibF_ReportDtc
5504                         ; 3941 					FMemLibF_ReportDtc(DTC_FL_HEAT_NTC_LOW_K, ERROR_OFF_K);
5506  12b8 87                	clra	
5507  12b9 c7                	clrb	
5508  12ba 6c80              	std	0,s
5509  12bc ce009e            	ldx	#158
5510  12bf ccb11a            	ldd	#-20198
5513  12c2 201f              	bra	L7742
5514  12c4                   L5742:
5515                         ; 3945 					if (temperature_HS_NTC_status[FRONT_LEFT] == /*NTC_MTRD_GND_MASK*/ NTC_SHRT_TO_GND)
5517  12c4 042105            	dbne	b,L1052
5518                         ; 3948 						FMemLibF_ReportDtc(DTC_FL_HEAT_NTC_LOW_K, ERROR_ON_K);
5520  12c7 cc0001            	ldd	#1
5522                         ; 3951 						FMemLibF_ReportDtc(DTC_FL_HEAT_NTC_HIGH_K, ERROR_OFF_K);
5526  12ca 2002              	bra	LC046
5527  12cc                   L1052:
5528                         ; 3956 						FMemLibF_ReportDtc(DTC_FL_HEAT_NTC_LOW_K, ERROR_OFF_K);
5530  12cc 87                	clra	
5531  12cd c7                	clrb	
5533                         ; 3959 						FMemLibF_ReportDtc(DTC_FL_HEAT_NTC_HIGH_K, ERROR_OFF_K);
5535  12ce                   LC046:
5536  12ce 3b                	pshd	
5537  12cf ce009e            	ldx	#158
5538  12d2 ccb11a            	ldd	#-20198
5539  12d5 4a000000          	call	f_FMemLibF_ReportDtc
5540  12d9 87                	clra	
5541  12da c7                	clrb	
5542  12db 6c80              	std	0,s
5543  12dd ce009e            	ldx	#158
5544  12e0 ccb11b            	ldd	#-20197
5546  12e3                   L7742:
5547  12e3 4a000000          	call	f_FMemLibF_ReportDtc
5548  12e7 1b82              	leas	2,s
5549                         ; 3962 				hs_ntc_fault_counter_c = 1;
5551  12e9 c601              	ldab	#1
5552                         ; 3964 				break;
5554  12eb 2048              	bra	LC052
5555  12ed                   L7442:
5556                         ; 3971 				if (temperature_HS_NTC_status[FRONT_RIGHT] == /*NTC_MTRD_OPEN_MASK*/ NTC_OPEN_CKT)
5558  12ed f60001            	ldab	_temperature_HS_NTC_status+1
5559  12f0 c102              	cmpb	#2
5560  12f2 261a              	bne	L5052
5561                         ; 3974 					FMemLibF_ReportDtc(DTC_FR_HEAT_NTC_HIGH_K, ERROR_ON_K);
5563  12f4 cc0001            	ldd	#1
5564  12f7 3b                	pshd	
5565  12f8 ce009e            	ldx	#158
5566  12fb ccb21b            	ldd	#-19941
5567  12fe 4a000000          	call	f_FMemLibF_ReportDtc
5569                         ; 3977 					FMemLibF_ReportDtc(DTC_FR_HEAT_NTC_LOW_K, ERROR_OFF_K);
5571  1302 87                	clra	
5572  1303 c7                	clrb	
5573  1304 6c80              	std	0,s
5574  1306 ce009e            	ldx	#158
5575  1309 ccb21a            	ldd	#-19942
5578  130c 201f              	bra	L7052
5579  130e                   L5052:
5580                         ; 3981 					if (temperature_HS_NTC_status[FRONT_RIGHT] == /*NTC_MTRD_GND_MASK*/NTC_SHRT_TO_GND)
5582  130e 042105            	dbne	b,L1152
5583                         ; 3984 						FMemLibF_ReportDtc(DTC_FR_HEAT_NTC_LOW_K, ERROR_ON_K);
5585  1311 cc0001            	ldd	#1
5587                         ; 3987 						FMemLibF_ReportDtc(DTC_FR_HEAT_NTC_HIGH_K, ERROR_OFF_K);
5591  1314 2002              	bra	LC047
5592  1316                   L1152:
5593                         ; 3992 						FMemLibF_ReportDtc(DTC_FR_HEAT_NTC_LOW_K, ERROR_OFF_K);
5595  1316 87                	clra	
5596  1317 c7                	clrb	
5598                         ; 3995 						FMemLibF_ReportDtc(DTC_FR_HEAT_NTC_HIGH_K, ERROR_OFF_K);
5600  1318                   LC047:
5601  1318 3b                	pshd	
5602  1319 ce009e            	ldx	#158
5603  131c ccb21a            	ldd	#-19942
5604  131f 4a000000          	call	f_FMemLibF_ReportDtc
5605  1323 87                	clra	
5606  1324 c7                	clrb	
5607  1325 6c80              	std	0,s
5608  1327 ce009e            	ldx	#158
5609  132a ccb21b            	ldd	#-19941
5611  132d                   L7052:
5612  132d 4a000000          	call	f_FMemLibF_ReportDtc
5613  1331 1b82              	leas	2,s
5614                         ; 3998 				hs_ntc_fault_counter_c = 2;
5616  1333 c602              	ldab	#2
5617  1335                   LC052:
5618  1335 7b0025            	stab	_hs_ntc_fault_counter_c
5619                         ; 4000 				break;
5622  1338 0a                	rtc	
5623  1339                   L1542:
5624                         ; 4006 				if ((CSWM_config_c & CSWM_4HS_VARIANT_MASK_K) == CSWM_4HS_VARIANT_MASK_K)
5626  1339 1e00e80f04        	brset	_CSWM_config_c,15,LC044
5627  133e 18200097          	bra	L1352
5628  1342                   LC044:
5629                         ; 4009 					if (temperature_HS_NTC_status[REAR_LEFT] == /*NTC_MTRD_OPEN_MASK*/ NTC_OPEN_CKT)
5631  1342 f60002            	ldab	_temperature_HS_NTC_status+2
5632  1345 c102              	cmpb	#2
5633  1347 261a              	bne	L7152
5634                         ; 4012 						FMemLibF_ReportDtc(DTC_RL_HEAT_NTC_HIGH_K, ERROR_ON_K);
5636  1349 cc0001            	ldd	#1
5637  134c 3b                	pshd	
5638  134d ce009e            	ldx	#158
5639  1350 ccb31b            	ldd	#-19685
5640  1353 4a000000          	call	f_FMemLibF_ReportDtc
5642                         ; 4015 						FMemLibF_ReportDtc(DTC_RL_HEAT_NTC_LOW_K, ERROR_OFF_K);
5644  1357 87                	clra	
5645  1358 c7                	clrb	
5646  1359 6c80              	std	0,s
5647  135b ce009e            	ldx	#158
5648  135e ccb31a            	ldd	#-19686
5651  1361 201f              	bra	L1252
5652  1363                   L7152:
5653                         ; 4019 						if (temperature_HS_NTC_status[REAR_LEFT] == /*NTC_MTRD_GND_MASK*/ NTC_SHRT_TO_GND)
5655  1363 042105            	dbne	b,L3252
5656                         ; 4022 							FMemLibF_ReportDtc(DTC_RL_HEAT_NTC_LOW_K, ERROR_ON_K);
5658  1366 cc0001            	ldd	#1
5660                         ; 4025 							FMemLibF_ReportDtc(DTC_RL_HEAT_NTC_HIGH_K, ERROR_OFF_K);
5664  1369 2002              	bra	LC048
5665  136b                   L3252:
5666                         ; 4030 							FMemLibF_ReportDtc(DTC_RL_HEAT_NTC_LOW_K, ERROR_OFF_K);
5668  136b 87                	clra	
5669  136c c7                	clrb	
5671                         ; 4033 							FMemLibF_ReportDtc(DTC_RL_HEAT_NTC_HIGH_K, ERROR_OFF_K);
5673  136d                   LC048:
5674  136d 3b                	pshd	
5675  136e ce009e            	ldx	#158
5676  1371 ccb31a            	ldd	#-19686
5677  1374 4a000000          	call	f_FMemLibF_ReportDtc
5678  1378 87                	clra	
5679  1379 c7                	clrb	
5680  137a 6c80              	std	0,s
5681  137c ce009e            	ldx	#158
5682  137f ccb31b            	ldd	#-19685
5684  1382                   L1252:
5685  1382 4a000000          	call	f_FMemLibF_ReportDtc
5686  1386 1b82              	leas	2,s
5687                         ; 4036 					hs_ntc_fault_counter_c = 3;
5689  1388 c603              	ldab	#3
5691  138a 20a9              	bra	LC052
5692                         ; 4040 					hs_ntc_fault_counter_c = 0;
5694  138c                   L3542:
5695                         ; 4049 				if ((CSWM_config_c & CSWM_4HS_VARIANT_MASK_K) == CSWM_4HS_VARIANT_MASK_K)
5697  138c 1e00e80f02        	brset	_CSWM_config_c,15,LC045
5698  1391 2046              	bra	L1352
5699  1393                   LC045:
5700                         ; 4052 					if (temperature_HS_NTC_status[REAR_RIGHT] == /*NTC_MTRD_OPEN_MASK*/ NTC_OPEN_CKT)
5702  1393 f60003            	ldab	_temperature_HS_NTC_status+3
5703  1396 c102              	cmpb	#2
5704  1398 261a              	bne	L3352
5705                         ; 4055 						FMemLibF_ReportDtc(DTC_RR_HEAT_NTC_HIGH_K, ERROR_ON_K);
5707  139a cc0001            	ldd	#1
5708  139d 3b                	pshd	
5709  139e ce009e            	ldx	#158
5710  13a1 ccb41b            	ldd	#-19429
5711  13a4 4a000000          	call	f_FMemLibF_ReportDtc
5713                         ; 4058 						FMemLibF_ReportDtc(DTC_RR_HEAT_NTC_LOW_K, ERROR_OFF_K);
5715  13a8 87                	clra	
5716  13a9 c7                	clrb	
5717  13aa 6c80              	std	0,s
5718  13ac ce009e            	ldx	#158
5719  13af ccb41a            	ldd	#-19430
5722  13b2 201f              	bra	LC049
5723  13b4                   L3352:
5724                         ; 4062 						if (temperature_HS_NTC_status[REAR_RIGHT] == /*NTC_MTRD_GND_MASK*/ NTC_SHRT_TO_GND)
5726  13b4 042105            	dbne	b,L7352
5727                         ; 4065 							FMemLibF_ReportDtc(DTC_RR_HEAT_NTC_LOW_K, ERROR_ON_K);
5729  13b7 cc0001            	ldd	#1
5731                         ; 4068 							FMemLibF_ReportDtc(DTC_RR_HEAT_NTC_HIGH_K, ERROR_OFF_K);
5735  13ba 2002              	bra	LC050
5736  13bc                   L7352:
5737                         ; 4073 							FMemLibF_ReportDtc(DTC_RR_HEAT_NTC_LOW_K, ERROR_OFF_K);
5739  13bc 87                	clra	
5740  13bd c7                	clrb	
5742                         ; 4076 							FMemLibF_ReportDtc(DTC_RR_HEAT_NTC_HIGH_K, ERROR_OFF_K);
5744  13be                   LC050:
5745  13be 3b                	pshd	
5746  13bf ce009e            	ldx	#158
5747  13c2 ccb41a            	ldd	#-19430
5748  13c5 4a000000          	call	f_FMemLibF_ReportDtc
5749  13c9 87                	clra	
5750  13ca c7                	clrb	
5751  13cb 6c80              	std	0,s
5752  13cd ce009e            	ldx	#158
5753  13d0 ccb41b            	ldd	#-19429
5755  13d3                   LC049:
5756  13d3 4a000000          	call	f_FMemLibF_ReportDtc
5757  13d7 1b82              	leas	2,s
5758  13d9                   L1352:
5759                         ; 4080 				hs_ntc_fault_counter_c = 0;
5761  13d9 790025            	clr	_hs_ntc_fault_counter_c
5762                         ; 4082 				break;
5764  13dc                   L7642:
5765                         ; 4091 }
5768  13dc 0a                	rtc	
5812                         ; 4105 @far void hsF_RearSwitchStuckDTC (void)
5812                         ; 4106 {
5813                         	switch	.ftext
5814  13dd                   f_hsF_RearSwitchStuckDTC:
5816  13dd 37                	pshb	
5817       00000001          OFST:	set	1
5820                         ; 4123 	if((canio_RX_IGN_STATUS_c == IGN_RUN) || (canio_RX_IGN_STATUS_c == IGN_START))
5822  13de f60000            	ldab	_canio_RX_IGN_STATUS_c
5823  13e1 c104              	cmpb	#4
5824  13e3 2706              	beq	L1652
5826  13e5 c105              	cmpb	#5
5827  13e7 182600cd          	bne	L5262
5828  13eb                   L1652:
5829                         ; 4128 		hs_rear_swth_stat_c[RL_SWITCH] = (unsigned char) Dio_ReadChannel(RL_LED_SW_DETECT);
5831  13eb fd0000            	ldy	_Dio_PortRead_Ptr
5832  13ee e640              	ldab	0,y
5833  13f0 c402              	andb	#2
5834  13f2 54                	lsrb	
5835  13f3 7b010b            	stab	_hs_rear_swth_stat_c
5836                         ; 4129 		hs_rear_swth_stat_c[RR_SWITCH] = (unsigned char) Dio_ReadChannel(RR_LED_SW_DETECT);
5838  13f6 e640              	ldab	0,y
5839  13f8 c404              	andb	#4
5840  13fa 54                	lsrb	
5841  13fb 54                	lsrb	
5842  13fc 7b010c            	stab	_hs_rear_swth_stat_c+1
5843                         ; 4131 		for (hs_rearseat = RL_SWITCH; hs_rearseat < ALL_REAR_SWITCH; hs_rearseat++) {
5845  13ff c7                	clrb	
5846  1400 6b80              	stab	OFST-1,s
5847  1402                   L3652:
5848                         ; 4135 			if(hs_rear_swth_stat_c[hs_rearseat] == STK_PRESS_K)
5850  1402 87                	clra	
5851  1403 b746              	tfr	d,y
5852  1405 e7ea010b          	tst	_hs_rear_swth_stat_c,y
5853  1409 2664              	bne	L1752
5854                         ; 4138 				hs_rear_demature_counter_c[hs_rearseat] = 0;
5856  140b 69ea002e          	clr	_hs_rear_demature_counter_c,y
5857                         ; 4141 				if (hs_rear_seat_flags_c[ hs_rearseat ].b.stksw_psd == FALSE)
5859  140f 0eea010f014f      	brset	_hs_rear_seat_flags_c,y,1,L3752
5860                         ; 4144 					hs_rear_demature_counter_c[hs_rearseat] = 0;
5862  1415 69ea002e          	clr	_hs_rear_demature_counter_c,y
5863                         ; 4147 					if(hs_rear_mature_counter_w[hs_rearseat] >= hs_rear_mature_time_w)
5865  1419 1858              	lsly	
5866  141b ecea0030          	ldd	_hs_rear_mature_counter_w,y
5867  141f bc0035            	cpd	_hs_rear_mature_time_w
5868  1422 e680              	ldab	OFST-1,s
5869  1424 2533              	blo	L5752
5870                         ; 4149 						if(hs_rearseat == RL_SWITCH)
5872  1426 2614              	bne	L7752
5873                         ; 4153 							hsLF_UpdateHeatParameters(REAR_LEFT, HEAT_LEVEL_OFF);
5875  1428 cc0007            	ldd	#7
5876  142b 3b                	pshd	
5877  142c c602              	ldab	#2
5878  142e 4a0d5050          	call	f_hsLF_UpdateHeatParameters
5880  1432 1b82              	leas	2,s
5881                         ; 4156 							hs_rem_seat_req_c[REAR_LEFT] = hs_switch_request_to_fet_c[REAR_LEFT];
5883  1434 180c005500ff      	movb	_hs_switch_request_to_fet_c+2,_hs_rem_seat_req_c+2
5885  143a 2012              	bra	L1062
5886  143c                   L7752:
5887                         ; 4162 							hsLF_UpdateHeatParameters(REAR_RIGHT, HEAT_LEVEL_OFF);
5889  143c cc0007            	ldd	#7
5890  143f 3b                	pshd	
5891  1440 c603              	ldab	#3
5892  1442 4a0d5050          	call	f_hsLF_UpdateHeatParameters
5894  1446 1b82              	leas	2,s
5895                         ; 4164 							hs_rem_seat_req_c[REAR_RIGHT] = hs_switch_request_to_fet_c[REAR_RIGHT];
5897  1448 180c00560100      	movb	_hs_switch_request_to_fet_c+3,_hs_rem_seat_req_c+3
5898  144e                   L1062:
5899                         ; 4168 						hs_rear_seat_flags_c[ hs_rearseat ].b.stksw_psd = TRUE;
5901  144e e680              	ldab	OFST-1,s
5902  1450 b796              	exg	b,y
5903  1452 0cea010f01        	bset	_hs_rear_seat_flags_c,y,1
5905  1457 2055              	bra	L3162
5906  1459                   L5752:
5907                         ; 4173 						hs_rear_mature_counter_w[hs_rearseat]++;
5909  1459 87                	clra	
5910  145a 59                	lsld	
5911  145b b746              	tfr	d,y
5912  145d 1862ea0030        	incw	_hs_rear_mature_counter_w,y
5913  1462 204a              	bra	L3162
5914  1464                   L3752:
5915                         ; 4181 						if (hs_rearseat == RL_SWITCH)
5917  1464 046104            	tbne	b,L7062
5918                         ; 4184 							FMemLibF_ReportDtc(DTC_HS_RL_SWITCH_STUCK_PSD_K, ERROR_ON_K);
5920  1467 c601              	ldab	#1
5923  1469 2022              	bra	LC055
5924  146b                   L7062:
5925                         ; 4189 							FMemLibF_ReportDtc(DTC_HS_RR_SWITCH_STUCK_PSD_K, ERROR_ON_K);
5927  146b c601              	ldab	#1
5929  146d 2028              	bra	LC054
5930  146f                   L1752:
5931                         ; 4199 				hs_rear_mature_counter_w[hs_rearseat] = 0;
5933  146f b746              	tfr	d,y
5934  1471 1858              	lsly	
5935  1473 1869ea0030        	clrw	_hs_rear_mature_counter_w,y
5936                         ; 4201 				if(hs_rear_demature_counter_c[hs_rearseat] >= hs_rear_demature_time_c)
5938  1478 b746              	tfr	d,y
5939  147a e6ea002e          	ldab	_hs_rear_demature_counter_c,y
5940  147e f10034            	cmpb	_hs_rear_demature_time_c
5941  1481 2523              	blo	L5162
5942                         ; 4203 					hs_rear_seat_flags_c[ hs_rearseat ].b.stksw_psd = FALSE;
5944  1483 0dea010f01        	bclr	_hs_rear_seat_flags_c,y,1
5945                         ; 4207 					if(hs_rearseat == RL_SWITCH)
5947  1488 e780              	tst	OFST-1,s
5948  148a 260a              	bne	L7162
5949                         ; 4210 							FMemLibF_ReportDtc(DTC_HS_RL_SWITCH_STUCK_PSD_K, ERROR_OFF_K);
5951  148c c7                	clrb	
5952  148d                   LC055:
5953  148d 3b                	pshd	
5954  148e ce0091            	ldx	#145
5955  1491 cc482a            	ldd	#18474
5958  1494 2008              	bra	LC053
5959  1496                   L7162:
5960                         ; 4215 						FMemLibF_ReportDtc(DTC_HS_RR_SWITCH_STUCK_PSD_K, ERROR_OFF_K);
5962  1496 c7                	clrb	
5963  1497                   LC054:
5964  1497 3b                	pshd	
5965  1498 ce0091            	ldx	#145
5966  149b cc4d2a            	ldd	#19754
5968  149e                   LC053:
5969  149e 4a000000          	call	f_FMemLibF_ReportDtc
5970  14a2 1b82              	leas	2,s
5971  14a4 2008              	bra	L3162
5972  14a6                   L5162:
5973                         ; 4222 					hs_rear_demature_counter_c[hs_rearseat]++;
5975  14a6 e680              	ldab	OFST-1,s
5976  14a8 b746              	tfr	d,y
5977  14aa 62ea002e          	inc	_hs_rear_demature_counter_c,y
5978  14ae                   L3162:
5979                         ; 4131 		for (hs_rearseat = RL_SWITCH; hs_rearseat < ALL_REAR_SWITCH; hs_rearseat++) {
5981  14ae 6280              	inc	OFST-1,s
5984  14b0 e680              	ldab	OFST-1,s
5985  14b2 c102              	cmpb	#2
5986  14b4 1825ff4a          	blo	L3652
5988  14b8                   L5262:
5989                         ; 4239 } //Function call end
5992  14b8 1b81              	leas	1,s
5993  14ba 0a                	rtc	
6036                         ; 4275 void hsLF_ControlFET(unsigned char seatpos, unsigned char duty_cycle)
6036                         ; 4276 {
6037                         	switch	.ftext
6038  14bb                   f_hsLF_ControlFET:
6040  14bb 3b                	pshd	
6041       00000000          OFST:	set	0
6044                         ; 4279 	if ((hs_temp_heat_pwm_c[seatpos] != duty_cycle) || (duty_cycle == SET_OFF_REQUEST) ) {
6046  14bc b796              	exg	b,y
6047  14be e6ea004f          	ldab	_hs_temp_heat_pwm_c,y
6048  14c2 e186              	cmpb	OFST+6,s
6049  14c4 2604              	bne	L1662
6051  14c6 e686              	ldab	OFST+6,s
6052  14c8 2647              	bne	L3762
6053  14ca                   L1662:
6054                         ; 4282 		hs_temp_heat_pwm_c[seatpos] = duty_cycle;
6056  14ca e686              	ldab	OFST+6,s
6057  14cc 6bea004f          	stab	_hs_temp_heat_pwm_c,y
6058                         ; 4285 		if (duty_cycle != SET_OFF_REQUEST) {
6060  14d0 2709              	beq	L3662
6061                         ; 4288 			hs_status2_flags_c[seatpos].b.hs_heater_state_b = TRUE;
6063  14d2 0cea011540        	bset	_hs_status2_flags_c,y,64
6065  14d7 e681              	ldab	OFST+1,s
6066  14d9 200a              	bra	L5662
6067  14db                   L3662:
6068                         ; 4292 			hs_status2_flags_c[seatpos].b.hs_heater_state_b = FALSE;
6070  14db e681              	ldab	OFST+1,s
6071  14dd 87                	clra	
6072  14de b746              	tfr	d,y
6073  14e0 0dea011540        	bclr	_hs_status2_flags_c,y,64
6074  14e5                   L5662:
6075                         ; 4297 		hs_autosar_pwm_w[seatpos] = mainF_Calc_PWM(duty_cycle);
6077  14e5 87                	clra	
6078  14e6 b746              	tfr	d,y
6079  14e8 1858              	lsly	
6080  14ea 35                	pshy	
6081  14eb e688              	ldab	OFST+8,s
6082  14ed 160000            	jsr	_mainF_Calc_PWM
6084  14f0 31                	puly	
6085  14f1 6cea00d4          	std	_hs_autosar_pwm_w,y
6086                         ; 4300 		switch (seatpos) {
6088  14f5 e681              	ldab	OFST+1,s
6090  14f7 270b              	beq	L7262
6091  14f9 040117            	dbeq	b,L1362
6092  14fc 04011d            	dbeq	b,L3362
6093  14ff 040123            	dbeq	b,L5362
6094  1502 200d              	bra	L3762
6095  1504                   L7262:
6096                         ; 4306 				StaggerF_Manager(FL_TIM_CH, hs_autosar_pwm_w[FRONT_LEFT]);				
6098  1504 fc00d4            	ldd	_hs_autosar_pwm_w
6099  1507 3b                	pshd	
6100  1508 cc0003            	ldd	#3
6102  150b                   LC056:
6103  150b 4a000000          	call	f_StaggerF_Manager
6104  150f 1b82              	leas	2,s
6105                         ; 4307 				break;
6106  1511                   L3762:
6107                         ; 4345 }
6110  1511 31                	puly	
6111  1512 0a                	rtc	
6112  1513                   L1362:
6113                         ; 4314 				StaggerF_Manager(FR_TIM_CH, hs_autosar_pwm_w[FRONT_RIGHT]);
6115  1513 fc00d6            	ldd	_hs_autosar_pwm_w+2
6116  1516 3b                	pshd	
6117  1517 cc0004            	ldd	#4
6119                         ; 4315 				break;
6121  151a 20ef              	bra	LC056
6122  151c                   L3362:
6123                         ; 4323 				StaggerF_Manager(RL_TIM_CH, hs_autosar_pwm_w[REAR_LEFT]);
6125  151c fc00d8            	ldd	_hs_autosar_pwm_w+4
6126  151f 3b                	pshd	
6127  1520 cc0005            	ldd	#5
6129                         ; 4324 				break;
6131  1523 20e6              	bra	LC056
6132  1525                   L5362:
6133                         ; 4331 				StaggerF_Manager(RR_TIM_CH, hs_autosar_pwm_w[REAR_RIGHT]);
6135  1525 fc00da            	ldd	_hs_autosar_pwm_w+6
6136  1528 3b                	pshd	
6137  1529 cc0006            	ldd	#6
6139                         ; 4332 				break;
6141  152c 20dd              	bra	LC056
6186                         ; 4386 void hsLF_2SecMonitorTime_FaultDetection(unsigned char hs_seatpos)
6186                         ; 4387 {
6187                         	switch	.ftext
6188  152e                   f_hsLF_2SecMonitorTime_FaultDetection:
6190  152e 3b                	pshd	
6191       00000000          OFST:	set	0
6194                         ; 4392 	if (hs_status_flags_c[ hs_seatpos ].b.hs_prelim_short_bat_b ||
6194                         ; 4393 	    hs_status_flags_c[ hs_seatpos ].b.hs_prelim_short_gnd_b ||
6194                         ; 4394 	    hs_status_flags_c[ hs_seatpos ].b.hs_prelim_open_b      ||
6194                         ; 4395 	    hs_status_flags_c[ hs_seatpos ].b.hs_prelim_partial_open_b ||
6194                         ; 4396 	    hs_status3_flags_c[ hs_seatpos ].b.final_STB_or_OPEN_b) {
6196  152f 87                	clra	
6197  1530 b746              	tfr	d,y
6198  1532 0eea0119101c      	brset	_hs_status_flags_c,y,16,L3172
6200  1538 0eea01190116      	brset	_hs_status_flags_c,y,1,L3172
6202  153e 0eea01190410      	brset	_hs_status_flags_c,y,4,L3172
6204  1544 0eea0119400a      	brset	_hs_status_flags_c,y,64,L3172
6206  154a e6ea0111          	ldab	_hs_status3_flags_c,y
6207  154e c501              	bitb	#1
6208  1550 1827010d          	beq	L1172
6209  1554                   L3172:
6210                         ; 4399 		if (hs_monitor_prelim_fault_counter_c[hs_seatpos] >= /*mature_2sec_time_c*/hs_flt_dtc_c[HS_2SEC_MATURE_TIME]) {
6212  1554 e681              	ldab	OFST+1,s
6213  1556 87                	clra	
6214  1557 b746              	tfr	d,y
6215  1559 e6ea0037          	ldab	_hs_monitor_prelim_fault_counter_c,y
6216  155d f100cf            	cmpb	_hs_flt_dtc_c+4
6217  1560 182500f3          	blo	L3272
6218                         ; 4402 			if (hs_status_flags_c[ hs_seatpos ].b.hs_prelim_short_bat_b) {
6220  1564 0fea01191015      	brclr	_hs_status_flags_c,y,16,L5272
6221                         ; 4405 				hs_status_flags_c[ hs_seatpos ].b.hs_final_short_bat_b = TRUE;
6223  156a 0dea011955        	bclr	_hs_status_flags_c,y,85
6224  156f 0cea011920        	bset	_hs_status_flags_c,y,32
6225                         ; 4408 				hs_status_flags_c[ hs_seatpos ].b.hs_prelim_short_bat_b = FALSE;
6227                         ; 4411 				hs_status_flags_c[ hs_seatpos ].b.hs_prelim_short_gnd_b = FALSE;
6229                         ; 4412 				hs_status_flags_c[ hs_seatpos ].b.hs_prelim_open_b = FALSE;
6231                         ; 4413 				hs_status_flags_c[ hs_seatpos ].b.hs_prelim_partial_open_b = FALSE;
6233                         ; 4416 				hs_status3_flags_c[ hs_seatpos ].b.final_STB_or_OPEN_b = FALSE;
6235  1574 0dea011101        	bclr	_hs_status3_flags_c,y,1
6237  1579 e681              	ldab	OFST+1,s
6238  157b 1820008b          	bra	L7272
6239  157f                   L5272:
6240                         ; 4421 				if (hs_status_flags_c[ hs_seatpos ].b.hs_prelim_short_gnd_b) {
6242  157f e681              	ldab	OFST+1,s
6243  1581 b746              	tfr	d,y
6244  1583 0fea0119010c      	brclr	_hs_status_flags_c,y,1,L1372
6245                         ; 4423 					hs_status_flags_c[ hs_seatpos ].b.hs_final_short_gnd_b = TRUE;
6247  1589 0dea011955        	bclr	_hs_status_flags_c,y,85
6248  158e 0cea011902        	bset	_hs_status_flags_c,y,2
6249                         ; 4426 					hs_status_flags_c[ hs_seatpos ].b.hs_prelim_short_gnd_b = FALSE;
6251                         ; 4429 					hs_status_flags_c[ hs_seatpos ].b.hs_prelim_short_bat_b = FALSE;
6253                         ; 4430 					hs_status_flags_c[ hs_seatpos ].b.hs_prelim_open_b = FALSE;
6255                         ; 4431 					hs_status_flags_c[ hs_seatpos ].b.hs_prelim_partial_open_b = FALSE;
6258  1593 2075              	bra	L7272
6259  1595                   L1372:
6260                         ; 4436 					if (hs_status_flags_c[ hs_seatpos ].b.hs_prelim_open_b) {
6262  1595 b746              	tfr	d,y
6263  1597 0fea0119040c      	brclr	_hs_status_flags_c,y,4,L5372
6264                         ; 4441 						hs_status3_flags_c[ hs_seatpos ].b.final_STB_or_OPEN_b = TRUE;
6266  159d 0cea011101        	bset	_hs_status3_flags_c,y,1
6267                         ; 4444 						hs_status_flags_c[ hs_seatpos ].b.hs_prelim_open_b = FALSE;
6269                         ; 4447 						hs_status_flags_c[ hs_seatpos ].b.hs_prelim_short_bat_b = FALSE;
6271                         ; 4448 						hs_status_flags_c[ hs_seatpos ].b.hs_prelim_short_gnd_b = FALSE;
6273                         ; 4449 						hs_status_flags_c[ hs_seatpos ].b.hs_prelim_partial_open_b = FALSE;
6275  15a2 0dea011955        	bclr	_hs_status_flags_c,y,85
6277  15a7 2061              	bra	L7272
6278  15a9                   L5372:
6279                         ; 4455 						if (hs_status3_flags_c[ hs_seatpos ].b.final_STB_or_OPEN_b) {
6281  15a9 b746              	tfr	d,y
6282  15ab 0fea0111010c      	brclr	_hs_status3_flags_c,y,1,L1472
6283                         ; 4457 							hs_status_flags_c[ hs_seatpos ].b.hs_final_open_b = TRUE;
6285  15b1 0cea011908        	bset	_hs_status_flags_c,y,8
6286                         ; 4460 							hs_status3_flags_c[ hs_seatpos ].b.final_STB_or_OPEN_b = FALSE;
6288  15b6 0dea011101        	bclr	_hs_status3_flags_c,y,1
6290  15bb 204d              	bra	L7272
6291  15bd                   L1472:
6292                         ; 4468 							if ( (diag_io_hs_lock_flags_c[hs_seatpos].b.switch_rq_lock_b == TRUE) || 
6292                         ; 4469 								 (diag_io_all_hs_lock_b == TRUE) || 
6292                         ; 4470 								 (diag_rc_all_heaters_lock_b == TRUE) )
6294  15bd b746              	tfr	d,y
6295  15bf 0eea0000010a      	brset	_diag_io_hs_lock_flags_c,y,1,L7472
6297  15c5 1e00001005        	brset	_diag_io_lock2_flags_c,16,L7472
6299  15ca 1f0000020d        	brclr	_diag_rc_lock_flags_c,2,L5472
6300  15cf                   L7472:
6301                         ; 4473 								hs_monitor_loop_counter_w [hs_seatpos] = HS_PART_OPEN_LOOP_K;
6303  15cf 87                	clra	
6304  15d0 59                	lsld	
6305  15d1 b746              	tfr	d,y
6306  15d3 cc01c1            	ldd	#449
6307  15d6 6cea001d          	std	_hs_monitor_loop_counter_w,y
6308  15da e681              	ldab	OFST+1,s
6309  15dc                   L5472:
6310                         ; 4481 							if(hs_monitor_loop_counter_w [hs_seatpos] >= HS_PART_OPEN_LOOP_K)
6312  15dc 87                	clra	
6313  15dd 59                	lsld	
6314  15de b746              	tfr	d,y
6315  15e0 ecea001d          	ldd	_hs_monitor_loop_counter_w,y
6316  15e4 8c01c1            	cpd	#449
6317  15e7 e681              	ldab	OFST+1,s
6318  15e9 250f              	blo	L3572
6319                         ; 4485 								hs_status_flags_c[ hs_seatpos ].b.hs_final_partial_open_b = TRUE;
6321  15eb 87                	clra	
6322  15ec b746              	tfr	d,y
6323  15ee 0dea011955        	bclr	_hs_status_flags_c,y,85
6324  15f3 0cea011980        	bset	_hs_status_flags_c,y,128
6325                         ; 4488 								hs_status_flags_c[ hs_seatpos ].b.hs_prelim_partial_open_b = FALSE;
6327                         ; 4491 								hs_status_flags_c[ hs_seatpos ].b.hs_prelim_short_bat_b = FALSE;
6329                         ; 4492 								hs_status_flags_c[ hs_seatpos ].b.hs_prelim_short_gnd_b = FALSE;
6331                         ; 4493 								hs_status_flags_c[ hs_seatpos ].b.hs_prelim_open_b = FALSE;
6334  15f8 2010              	bra	L7272
6335  15fa                   L3572:
6336                         ; 4499 								hs_monitor_loop_counter_w [hs_seatpos]++;
6338  15fa 87                	clra	
6339  15fb b746              	tfr	d,y
6340  15fd 1858              	lsly	
6341  15ff 1862ea001d        	incw	_hs_monitor_loop_counter_w,y
6342                         ; 4502 								hs_monitor_prelim_fault_counter_c[hs_seatpos] = 0;
6344  1604 b746              	tfr	d,y
6345  1606 69ea0037          	clr	_hs_monitor_prelim_fault_counter_c,y
6346  160a                   L7272:
6347                         ; 4523 			if(hs_status_flags_c[ hs_seatpos ].b.hs_final_short_bat_b ||
6347                         ; 4524 			   hs_status_flags_c[ hs_seatpos ].b.hs_final_short_gnd_b ||
6347                         ; 4525 			   hs_status_flags_c[ hs_seatpos ].b.hs_final_open_b      ||
6347                         ; 4526 			   hs_status_flags_c[ hs_seatpos ].b.hs_final_partial_open_b ||
6347                         ; 4527 			   hs_status3_flags_c[ hs_seatpos ].b.final_STB_or_OPEN_b )
6349  160a 87                	clra	
6350  160b b746              	tfr	d,y
6351  160d 0eea01192018      	brset	_hs_status_flags_c,y,32,L1672
6353  1613 0eea01190212      	brset	_hs_status_flags_c,y,2,L1672
6355  1619 0eea0119080c      	brset	_hs_status_flags_c,y,8,L1672
6357  161f 0eea01198006      	brset	_hs_status_flags_c,y,128,L1672
6359  1625 0fea01110134      	brclr	_hs_status3_flags_c,y,1,L3772
6360  162b                   L1672:
6361                         ; 4529 				hs_status2_flags_c[ hs_seatpos ].b.hs_output_fault_set_b = TRUE;
6363  162b 87                	clra	
6364  162c b746              	tfr	d,y
6365  162e 0cea011580        	bset	_hs_status2_flags_c,y,128
6366                         ; 4532 				hsLF_UpdateHeatParameters(hs_seatpos, HEAT_LEVEL_OFF);
6368  1633 c607              	ldab	#7
6369  1635 3b                	pshd	
6370  1636 e683              	ldab	OFST+3,s
6371  1638 4a0d5050          	call	f_hsLF_UpdateHeatParameters
6373  163c 1b82              	leas	2,s
6374                         ; 4535 				hs_rem_seat_req_c[hs_seatpos] = hs_switch_request_to_fet_c[hs_seatpos];
6376  163e e681              	ldab	OFST+1,s
6377  1640 b796              	exg	b,y
6378  1642 180aea0053ea00fd  	movb	_hs_switch_request_to_fet_c,y,_hs_rem_seat_req_c,y
6379                         ; 4538 				hs_monitor_prelim_fault_counter_c[hs_seatpos] = 0;
6381  164a 69ea0037          	clr	_hs_monitor_prelim_fault_counter_c,y
6382                         ; 4539 				hs_monitor_loop_counter_w [hs_seatpos] = 0;
6384  164e 1858              	lsly	
6385  1650 1869ea001d        	clrw	_hs_monitor_loop_counter_w,y
6386  1655 2008              	bra	L3772
6387  1657                   L3272:
6388                         ; 4544 			hs_monitor_prelim_fault_counter_c[hs_seatpos]++;
6390  1657 e681              	ldab	OFST+1,s
6391  1659 b746              	tfr	d,y
6392  165b 62ea0037          	inc	_hs_monitor_prelim_fault_counter_c,y
6393  165f                   L3772:
6394                         ; 4573 }
6397  165f 31                	puly	
6398  1660 0a                	rtc	
6399  1661                   L1172:
6400                         ; 4549 		hs_monitor_prelim_fault_counter_c[hs_seatpos] = 0;
6402  1661 e681              	ldab	OFST+1,s
6403  1663 b746              	tfr	d,y
6404  1665 6aea0037          	staa	_hs_monitor_prelim_fault_counter_c,y
6405                         ; 4550 		hs_monitor_loop_counter_w[hs_seatpos] = 0;
6407  1669 1858              	lsly	
6408  166b 1869ea001d        	clrw	_hs_monitor_loop_counter_w,y
6409                         ; 4552 		hs_status_flags_c[ hs_seatpos ].b.hs_prelim_short_gnd_b = FALSE;
6411  1670 b746              	tfr	d,y
6412                         ; 4553 		hs_status_flags_c[ hs_seatpos ].b.hs_prelim_open_b = FALSE;
6414                         ; 4554 		hs_status_flags_c[ hs_seatpos ].b.hs_prelim_partial_open_b = FALSE;
6416                         ; 4555 		hs_status_flags_c[ hs_seatpos ].b.hs_prelim_short_bat_b = FALSE;
6418  1672 0dea011955        	bclr	_hs_status_flags_c,y,85
6419  1677 20e6              	bra	L3772
6466                         ; 4605 @far unsigned char hsF_FrontHeatSeatStatus(unsigned char hs_position)
6466                         ; 4606 {
6467                         	switch	.ftext
6468  1679                   f_hsF_FrontHeatSeatStatus:
6470  1679 3b                	pshd	
6471  167a 3b                	pshd	
6472       00000002          OFST:	set	2
6475                         ; 4612 	hs_pos = hs_position;
6477  167b 6b81              	stab	OFST-1,s
6478                         ; 4614 	switch (hs_pos) {
6481  167d 2705              	beq	L5772
6482  167f 04010b            	dbeq	b,L7772
6483                         ; 4642 			hs_status = HS_NOT_ACTIVE;
6485  1682 2005              	bra	L7203
6486  1684                   L5772:
6487                         ; 4618 			if (hs_rem_seat_req_c[FRONT_LEFT] != SET_OFF_REQUEST) {
6489  1684 f600fd            	ldab	_hs_rem_seat_req_c
6490                         ; 4620 				hs_status = HS_ACTIVE;
6493  1687 2609              	bne	LC058
6494  1689                   L7203:
6495                         ; 4623 				hs_status = HS_NOT_ACTIVE;
6497  1689 c6aa              	ldab	#170
6498  168b 2007              	bra	LC057
6499  168d                   L7772:
6500                         ; 4630 			if (hs_rem_seat_req_c[FRONT_RIGHT] != SET_OFF_REQUEST) {
6502  168d f600fe            	ldab	_hs_rem_seat_req_c+1
6503  1690 2706              	beq	L3303
6504                         ; 4632 				hs_status = HS_ACTIVE;
6506  1692                   LC058:
6507  1692 c655              	ldab	#85
6508  1694                   LC057:
6509  1694 6b80              	stab	OFST-2,s
6511  1696 2002              	bra	L5203
6512  1698                   L3303:
6513                         ; 4635 				hs_status = HS_NOT_ACTIVE;
6515  1698 c6aa              	ldab	#170
6516  169a                   L5203:
6517                         ; 4646 	return (hs_status);
6521  169a 1b84              	leas	4,s
6522  169c 0a                	rtc	
6561                         ; 4722 @far void hsF_AD_Readings(void)
6561                         ; 4723 {
6562                         	switch	.ftext
6563  169d                   f_hsF_AD_Readings:
6565  169d 37                	pshb	
6566       00000001          OFST:	set	1
6569                         ; 4726 	for (hs_pos = FRONT_LEFT; hs_pos < ALL_HEAT_SEATS; hs_pos++) {
6571  169e c7                	clrb	
6572  169f 6b80              	stab	OFST-1,s
6573  16a1                   L3503:
6574                         ; 4729 		hsLF_GetPWM_State(hs_pos);
6576  16a1 87                	clra	
6577  16a2 4a186060          	call	f_hsLF_GetPWM_State
6579                         ; 4733 		if (hs_status2_flags_c[hs_pos].b.hs_heater_state_b ) {
6581  16a6 e680              	ldab	OFST-1,s
6582  16a8 87                	clra	
6583  16a9 b746              	tfr	d,y
6584  16ab 0fea0115401d      	brclr	_hs_status2_flags_c,y,64,L1603
6585                         ; 4736 			if (hs_pwm_state_c[hs_pos] == PWM_HIGH) {
6587  16b1 e6ea00f5          	ldab	_hs_pwm_state_c,y
6588  16b5 042126            	dbne	b,L7603
6589                         ; 4741 				hsLF_Vsense_AD_Readings(hs_pos);
6591  16b8 e680              	ldab	OFST-1,s
6592  16ba 4a172727          	call	f_hsLF_Vsense_AD_Readings
6594                         ; 4744 				hsLF_Isense_AD_Readings(hs_pos);
6596  16be e680              	ldab	OFST-1,s
6597  16c0 87                	clra	
6598  16c1 4a16e9e9          	call	f_hsLF_Isense_AD_Readings
6600                         ; 4747 				hsLF_Zsense_AD_Readings(hs_pos);
6602  16c5 e680              	ldab	OFST-1,s
6603  16c7 87                	clra	
6604  16c8 4a17a6a6          	call	f_hsLF_Zsense_AD_Readings
6607  16cc 2010              	bra	L7603
6608  16ce                   L1603:
6609                         ; 4756 			hsLF_Vsense_AD_Readings(hs_pos);
6611  16ce 4a172727          	call	f_hsLF_Vsense_AD_Readings
6613                         ; 4759 			hs_Isense_c[hs_pos] = 0;
6615  16d2 e680              	ldab	OFST-1,s
6616  16d4 b796              	exg	b,y
6617  16d6 69ea00ed          	clr	_hs_Isense_c,y
6618                         ; 4760 			hs_Zsense_c[hs_pos] = 0;
6620  16da 69ea00e9          	clr	_hs_Zsense_c,y
6621  16de                   L7603:
6622                         ; 4726 	for (hs_pos = FRONT_LEFT; hs_pos < ALL_HEAT_SEATS; hs_pos++) {
6624  16de 6280              	inc	OFST-1,s
6627  16e0 e680              	ldab	OFST-1,s
6628  16e2 c104              	cmpb	#4
6629  16e4 25bb              	blo	L3503
6630                         ; 4763 }
6633  16e6 1b81              	leas	1,s
6634  16e8 0a                	rtc	
6671                         ; 4792 void hsLF_Isense_AD_Readings(unsigned char seat)
6671                         ; 4793 {
6672                         	switch	.ftext
6673  16e9                   f_hsLF_Isense_AD_Readings:
6675  16e9 3b                	pshd	
6676       00000000          OFST:	set	0
6679                         ; 4795 	switch (seat) {
6682  16ea 04410b            	tbeq	b,L1703
6683  16ed 04010c            	dbeq	b,L3703
6684  16f0 04010d            	dbeq	b,L5703
6685  16f3 04010e            	dbeq	b,L7703
6686  16f6 2011              	bra	L1213
6687  16f8                   L1703:
6688                         ; 4799 			hs_AD_parameters.slctd_adCH_c = CH3;        // set selected AD channel to CH3 - AN03/PAD03
6690  16f8 c603              	ldab	#3
6691                         ; 4800 			break;
6693  16fa 200a              	bra	LC060
6694  16fc                   L3703:
6695                         ; 4806 			hs_AD_parameters.slctd_adCH_c = CH4;        // set selected AD channel to CH4 - AN04/PAD04
6697  16fc c604              	ldab	#4
6698                         ; 4807 			break;
6700  16fe 2006              	bra	LC060
6701  1700                   L5703:
6702                         ; 4813 			hs_AD_parameters.slctd_adCH_c = CH1;        // set selected AD channel to CH1 - AN01/PAD01
6704  1700 c601              	ldab	#1
6705                         ; 4814 			break;
6707  1702 2002              	bra	LC060
6708  1704                   L7703:
6709                         ; 4820 			hs_AD_parameters.slctd_adCH_c = CH2;        // set selected AD channel to CH2 - AN02/PAD02
6711  1704 c602              	ldab	#2
6712  1706                   LC060:
6713  1706 7b001b            	stab	_hs_AD_parameters+1
6714                         ; 4821 			break;
6716  1709                   L1213:
6717                         ; 4831 	hsLF_Isense_AD_Conversion();
6719  1709 4a184a4a          	call	f_hsLF_Isense_AD_Conversion
6721                         ; 4834 	hs_Isense_c[seat] = (unsigned int) ADF_Nrmlz_Isense(AD_Filter_set[hs_AD_parameters.slctd_adCH_c].flt_rslt_w);
6723  170d e681              	ldab	OFST+1,s
6724  170f 87                	clra	
6725  1710 3b                	pshd	
6726  1711 f6001b            	ldab	_hs_AD_parameters+1
6727  1714 8606              	ldaa	#6
6728  1716 12                	mul	
6729  1717 b746              	tfr	d,y
6730  1719 ecea0004          	ldd	_AD_Filter_set+4,y
6731  171d 160000            	jsr	_ADF_Nrmlz_Isense
6733  1720 31                	puly	
6734  1721 6bea00ed          	stab	_hs_Isense_c,y
6735                         ; 4836 }
6738  1725 31                	puly	
6739  1726 0a                	rtc	
6777                         ; 4865 void hsLF_Vsense_AD_Readings(unsigned char seatpos)
6777                         ; 4866 {
6778                         	switch	.ftext
6779  1727                   f_hsLF_Vsense_AD_Readings:
6781  1727 3b                	pshd	
6782  1728 37                	pshb	
6783       00000001          OFST:	set	1
6786                         ; 4869 	switch (seatpos) {
6789  1729 04410b            	tbeq	b,L3213
6790  172c 040114            	dbeq	b,L5213
6791  172f 04011d            	dbeq	b,L7213
6792  1732 040128            	dbeq	b,L1313
6793  1735 2034              	bra	L3513
6794  1737                   L3213:
6795                         ; 4873 			hs_AD_parameters.slctd_adCH_c = CH6;        // set selected AD channel to Channel 6 
6797  1737 c606              	ldab	#6
6798  1739 7b001b            	stab	_hs_AD_parameters+1
6799                         ; 4874 			hs_AD_parameters.slctd_mux_c  = MUX1;       // set selected mux to MUX1
6801  173c 79001c            	clr	_hs_AD_parameters+2
6802                         ; 4875 			hs_AD_parameters.slctd_muxCH_c = CH3;       // Set selected MUX Channel to CH3
6804  173f c603              	ldab	#3
6805                         ; 4876 			break;
6807  1741 2025              	bra	LC061
6808  1743                   L5213:
6809                         ; 4882 			hs_AD_parameters.slctd_adCH_c = CH6;        // set selected AD channel to Channel 6 
6811  1743 c606              	ldab	#6
6812  1745 7b001b            	stab	_hs_AD_parameters+1
6813                         ; 4883 			hs_AD_parameters.slctd_mux_c  = MUX1;       // set selected mux to MUX1
6815  1748 79001c            	clr	_hs_AD_parameters+2
6816                         ; 4884 			hs_AD_parameters.slctd_muxCH_c = CH2;       // Set selected MUX Channel to CH2
6818  174b c602              	ldab	#2
6819                         ; 4885 			break;
6821  174d 2019              	bra	LC061
6822  174f                   L7213:
6823                         ; 4891 			hs_AD_parameters.slctd_adCH_c = CH5;        // set selected AD channel to Channel 5 
6825  174f c605              	ldab	#5
6826  1751 7b001b            	stab	_hs_AD_parameters+1
6827                         ; 4892 			hs_AD_parameters.slctd_mux_c  = MUX2;       // set selected mux to MUX2
6829  1754 c601              	ldab	#1
6830  1756 7b001c            	stab	_hs_AD_parameters+2
6831                         ; 4893 			hs_AD_parameters.slctd_muxCH_c = CH3;       // Set selected MUX Channel to CH3
6833  1759 c603              	ldab	#3
6834                         ; 4894 			break;
6836  175b 200b              	bra	LC061
6837  175d                   L1313:
6838                         ; 4900 			hs_AD_parameters.slctd_adCH_c = CH5;        // set selected AD channel to Channel 5 
6840  175d c605              	ldab	#5
6841  175f 7b001b            	stab	_hs_AD_parameters+1
6842                         ; 4901 			hs_AD_parameters.slctd_mux_c  = MUX2;       // set selected mux to MUX2
6844  1762 c601              	ldab	#1
6845  1764 7b001c            	stab	_hs_AD_parameters+2
6846                         ; 4902 			hs_AD_parameters.slctd_muxCH_c = CH2;       // Set selected MUX Channel to CH2
6848  1767 52                	incb	
6849  1768                   LC061:
6850  1768 7b001a            	stab	_hs_AD_parameters
6851                         ; 4903 			break;
6853  176b                   L3513:
6854                         ; 4912 	Dio_WriteChannelGroup(MuxSelGroup, hs_AD_parameters.slctd_muxCH_c);
6857  176b 1410              	sei	
6862  176d f60000            	ldab	_DioConfigData
6863  1770 87                	clra	
6864  1771 59                	lsld	
6865  1772 b746              	tfr	d,y
6866  1774 f6001a            	ldab	_hs_AD_parameters
6867  1777 b60001            	ldaa	_DioConfigData+1
6868  177a 2704              	beq	L231
6869  177c                   L431:
6870  177c 58                	lslb	
6871  177d 0430fc            	dbne	a,L431
6872  1780                   L231:
6873  1780 f40002            	andb	_DioConfigData+2
6874  1783 6b80              	stab	OFST-1,s
6875  1785 f60000            	ldab	_DioConfigData
6876  1788 87                	clra	
6877  1789 b745              	tfr	d,x
6878  178b 1848              	lslx	
6879  178d f60002            	ldab	_DioConfigData+2
6880  1790 51                	comb	
6881  1791 e4e30000          	andb	[_Dio_PortWrite_Ptr,x]
6882  1795 ea80              	orab	OFST-1,s
6883  1797 6beb0000          	stab	[_Dio_PortWrite_Ptr,y]
6887  179b 10ef              	cli	
6889                         ; 4915 	hsLF_Vsense_AD_Conversion(seatpos);
6892  179d e682              	ldab	OFST+1,s
6893  179f 4a17efef          	call	f_hsLF_Vsense_AD_Conversion
6895                         ; 4916 }
6898  17a3 1b83              	leas	3,s
6899  17a5 0a                	rtc	
6971                         ; 4945 void hsLF_Zsense_AD_Readings(unsigned char seat)
6971                         ; 4946 {
6972                         	switch	.ftext
6973  17a6                   f_hsLF_Zsense_AD_Readings:
6975  17a6 3b                	pshd	
6976  17a7 1b99              	leas	-7,s
6977       00000007          OFST:	set	7
6980                         ; 4949 	unsigned char hs_mux = hs_AD_parameters.slctd_mux_c;
6982  17a9 180980001c        	movb	_hs_AD_parameters+2,OFST-7,s
6983                         ; 4950 	unsigned char hs_mux_ch = hs_AD_parameters.slctd_muxCH_c;
6985  17ae 180981001a        	movb	_hs_AD_parameters,OFST-6,s
6986                         ; 4951 	unsigned char hs_ad_ch = hs_AD_parameters.slctd_adCH_c;
6988  17b3 180982001b        	movb	_hs_AD_parameters+1,OFST-5,s
6989                         ; 4953 	unsigned short hs_fltrd_Vsense_w = AD_mux_Filter[hs_mux][hs_mux_ch].flt_rslt_w;
6991  17b8 e681              	ldab	OFST-6,s
6992  17ba 8606              	ldaa	#6
6993  17bc 12                	mul	
6994  17bd b746              	tfr	d,y
6995  17bf e680              	ldab	OFST-7,s
6996  17c1 8630              	ldaa	#48
6997  17c3 12                	mul	
6998  17c4 19ee              	leay	d,y
6999  17c6 1802ea000483      	movw	_AD_mux_Filter+4,y,OFST-4,s
7000                         ; 4954 	unsigned short hs_fltrd_Isense_w = AD_Filter_set[hs_ad_ch].flt_rslt_w;
7002  17cc e682              	ldab	OFST-5,s
7003  17ce 8606              	ldaa	#6
7004  17d0 12                	mul	
7005  17d1 b746              	tfr	d,y
7006  17d3 1802ea000485      	movw	_AD_Filter_set+4,y,OFST-2,s
7007                         ; 4956 	hs_Zsense_c[seat] = ADF_Hs_PrtlOpen_Z_Cnvrsn(hs_fltrd_Vsense_w, hs_fltrd_Isense_w);
7009  17d9 e688              	ldab	OFST+1,s
7010  17db 87                	clra	
7011  17dc 3b                	pshd	
7012  17dd ec87              	ldd	OFST+0,s
7013  17df 3b                	pshd	
7014  17e0 ec87              	ldd	OFST+0,s
7015  17e2 160000            	jsr	_ADF_Hs_PrtlOpen_Z_Cnvrsn
7017  17e5 1b82              	leas	2,s
7018  17e7 31                	puly	
7019  17e8 6bea00e9          	stab	_hs_Zsense_c,y
7020                         ; 4957 }
7023  17ec 1b89              	leas	9,s
7024  17ee 0a                	rtc	
7066                         ; 4989 void hsLF_Vsense_AD_Conversion(unsigned char heater)
7066                         ; 4990 {
7067                         	switch	.ftext
7068  17ef                   f_hsLF_Vsense_AD_Conversion:
7070  17ef 3b                	pshd	
7071       00000000          OFST:	set	0
7074                         ; 4992 	ADF_AtoD_Cnvrsn(hs_AD_parameters.slctd_adCH_c);
7076  17f0 f6001b            	ldab	_hs_AD_parameters+1
7077  17f3 87                	clra	
7078  17f4 160000            	jsr	_ADF_AtoD_Cnvrsn
7080                         ; 4995 	hsLF_GetPWM_State(heater);
7082  17f7 e681              	ldab	OFST+1,s
7083  17f9 87                	clra	
7084  17fa 4a186060          	call	f_hsLF_GetPWM_State
7086                         ; 5000 	if (hs_status2_flags_c[heater].b.hs_heater_state_b ) {
7088  17fe e681              	ldab	OFST+1,s
7089  1800 87                	clra	
7090  1801 b746              	tfr	d,y
7091  1803 0fea01154007      	brclr	_hs_status2_flags_c,y,64,L7123
7092                         ; 5002 		if (hs_pwm_state_c[heater] == PWM_HIGH) {
7094  1809 e6ea00f5          	ldab	_hs_pwm_state_c,y
7095  180d 042138            	dbne	b,L5223
7096                         ; 5005 			ADF_Str_UnFlt_Mux_Rslt(hs_AD_parameters.slctd_adCH_c, hs_AD_parameters.slctd_muxCH_c);
7099                         ; 5008 			ADF_Mux_Filter(hs_AD_parameters.slctd_mux_c, hs_AD_parameters.slctd_muxCH_c);
7102                         ; 5011 			hs_Vsense_c[heater] = ADF_Nrmlz_Vsense(AD_mux_Filter[hs_AD_parameters.slctd_mux_c][hs_AD_parameters.slctd_muxCH_c].flt_rslt_w);
7106  1810                   L7123:
7107                         ; 5019 		ADF_Str_UnFlt_Mux_Rslt(hs_AD_parameters.slctd_adCH_c, hs_AD_parameters.slctd_muxCH_c);
7110                         ; 5022 		ADF_Mux_Filter(hs_AD_parameters.slctd_mux_c, hs_AD_parameters.slctd_muxCH_c);
7113                         ; 5025 		hs_Vsense_c[heater] = ADF_Nrmlz_Vsense(AD_mux_Filter[hs_AD_parameters.slctd_mux_c][hs_AD_parameters.slctd_muxCH_c].flt_rslt_w);
7116  1810 f6001a            	ldab	_hs_AD_parameters
7117  1813 3b                	pshd	
7118  1814 f6001b            	ldab	_hs_AD_parameters+1
7119  1817 160000            	jsr	_ADF_Str_UnFlt_Mux_Rslt
7120  181a f6001a            	ldab	_hs_AD_parameters
7121  181d 87                	clra	
7122  181e 6c80              	std	0,s
7123  1820 f6001c            	ldab	_hs_AD_parameters+2
7124  1823 160000            	jsr	_ADF_Mux_Filter
7125  1826 1b82              	leas	2,s
7126  1828 e681              	ldab	OFST+1,s
7127  182a 87                	clra	
7128  182b 3b                	pshd	
7129  182c f6001a            	ldab	_hs_AD_parameters
7130  182f 8606              	ldaa	#6
7131  1831 12                	mul	
7132  1832 b746              	tfr	d,y
7133  1834 f6001c            	ldab	_hs_AD_parameters+2
7134  1837 8630              	ldaa	#48
7135  1839 12                	mul	
7136  183a 19ee              	leay	d,y
7137  183c ecea0004          	ldd	_AD_mux_Filter+4,y
7138  1840 160000            	jsr	_ADF_Nrmlz_Vsense
7139  1843 31                	puly	
7140  1844 6bea00f1          	stab	_hs_Vsense_c,y
7141  1848                   L5223:
7142                         ; 5027 }
7145  1848 31                	puly	
7146  1849 0a                	rtc	
7173                         ; 5058 void hsLF_Isense_AD_Conversion(void)
7173                         ; 5059 {
7174                         	switch	.ftext
7175  184a                   f_hsLF_Isense_AD_Conversion:
7179                         ; 5060 	ADF_AtoD_Cnvrsn(hs_AD_parameters.slctd_adCH_c);
7181  184a f6001b            	ldab	_hs_AD_parameters+1
7182  184d 87                	clra	
7183  184e 160000            	jsr	_ADF_AtoD_Cnvrsn
7185                         ; 5062 	ADF_Str_UnFlt_Rslt(hs_AD_parameters.slctd_adCH_c);
7187  1851 f6001b            	ldab	_hs_AD_parameters+1
7188  1854 87                	clra	
7189  1855 160000            	jsr	_ADF_Str_UnFlt_Rslt
7191                         ; 5064 	ADF_Filter(hs_AD_parameters.slctd_adCH_c);
7193  1858 f6001b            	ldab	_hs_AD_parameters+1
7194  185b 87                	clra	
7195  185c 160000            	jsr	_ADF_Filter
7197                         ; 5065 }
7200  185f 0a                	rtc	
7233                         ; 5095 void hsLF_GetPWM_State(unsigned char heater)
7233                         ; 5096 {
7234                         	switch	.ftext
7235  1860                   f_hsLF_GetPWM_State:
7239                         ; 5100 	switch (heater) {
7242  1860 04410a            	tbeq	b,L7323
7243  1863 040112            	dbeq	b,L1423
7244  1866 04011a            	dbeq	b,L3423
7245  1869 040122            	dbeq	b,L5423
7247  186c 0a                	rtc	
7248  186d                   L7323:
7249                         ; 5104 			hs_pwm_state_c[FRONT_LEFT] = (unsigned char) Pwm_GetOutputState(PWM_HS_FL);
7251  186d cc0003            	ldd	#3
7252  1870 4a000000          	call	f_Pwm_GetOutputState
7254  1874 7b00f5            	stab	_hs_pwm_state_c
7255                         ; 5105 			break;
7258  1877 0a                	rtc	
7259  1878                   L1423:
7260                         ; 5110 			hs_pwm_state_c[FRONT_RIGHT] = (unsigned char) Pwm_GetOutputState(PWM_HS_FR);
7262  1878 cc0004            	ldd	#4
7263  187b 4a000000          	call	f_Pwm_GetOutputState
7265  187f 7b00f6            	stab	_hs_pwm_state_c+1
7266                         ; 5111 			break;
7269  1882 0a                	rtc	
7270  1883                   L3423:
7271                         ; 5116 			hs_pwm_state_c[REAR_LEFT] = (unsigned char) Pwm_GetOutputState(PWM_HS_RL);
7273  1883 cc0005            	ldd	#5
7274  1886 4a000000          	call	f_Pwm_GetOutputState
7276  188a 7b00f7            	stab	_hs_pwm_state_c+2
7277                         ; 5117 			break;
7280  188d 0a                	rtc	
7281  188e                   L5423:
7282                         ; 5122 			hs_pwm_state_c[REAR_RIGHT] = (unsigned char) Pwm_GetOutputState(PWM_HS_RR);
7284  188e cc0006            	ldd	#6
7285  1891 4a000000          	call	f_Pwm_GetOutputState
7287  1895 7b00f8            	stab	_hs_pwm_state_c+3
7288                         ; 5123 			break;
7290                         ; 5130 }
7293  1898 0a                	rtc	
7349                         ; 5165 @far unsigned char hsF_CheckDiagnoseParameter( unsigned char service, unsigned char *data_p_c )
7349                         ; 5166 {
7350                         	switch	.ftext
7351  1899                   f_hsF_CheckDiagnoseParameter:
7353  1899 3b                	pshd	
7354  189a 37                	pshb	
7355       00000001          OFST:	set	1
7358                         ; 5170 	switch (service) {
7361  189b 87                	clra	
7362  189c c008              	subb	#8
7363  189e c10d              	cmpb	#13
7364  18a0 241d              	bhs	L251
7365  18a2 59                	lsld	
7366  18a3 05ff              	jmp	[d,pc]
7367  18a5 19a6              	dc.w	L7723
7368  18a7 19ca              	dc.w	L1033
7369  18a9 1a85              	dc.w	L1133
7370  18ab 1a85              	dc.w	L1133
7371  18ad 1a1c              	dc.w	L3033
7372  18af 1a85              	dc.w	L1133
7373  18b1 1a38              	dc.w	L5033
7374  18b3 1a40              	dc.w	L7033
7375  18b5 1a85              	dc.w	L1133
7376  18b7 19a2              	dc.w	L5723
7377  18b9 18ca              	dc.w	L1723
7378  18bb 193b              	dc.w	L3723
7379  18bd 19ca              	dc.w	L1033
7380  18bf                   L251:
7381  18bf c0f8              	subb	#-8
7382  18c1 2707              	beq	L1723
7383  18c3 53                	decb	
7384  18c4 2775              	beq	L3723
7385  18c6 182001bb          	bra	L1133
7386  18ca                   L1723:
7387                         ; 5183 		if ((*data_p_c < 7  || *data_p_c > 100 ) ||
7387                         ; 5184 		    (*(data_p_c + 1) < 7  || *(data_p_c + 1) > 100) ||
7387                         ; 5185 		    (*(data_p_c + 2) < 7  || *(data_p_c + 2) > 100) ||
7387                         ; 5186 		    (*(data_p_c + 3) < 7  || *(data_p_c + 3) > 100) ) {
7389  18ca ed86              	ldy	OFST+5,s
7390  18cc e640              	ldab	0,y
7391  18ce c107              	cmpb	#7
7392  18d0 182501b1          	blo	L1133
7394  18d4 c164              	cmpb	#100
7395  18d6 182201ab          	bhi	L1133
7397  18da e641              	ldab	1,y
7398  18dc c107              	cmpb	#7
7399  18de 182501a3          	blo	L1133
7401  18e2 c164              	cmpb	#100
7402  18e4 1822019d          	bhi	L1133
7404  18e8 e642              	ldab	2,y
7405  18ea c107              	cmpb	#7
7406  18ec 18250195          	blo	L1133
7408  18f0 c164              	cmpb	#100
7409  18f2 1822018f          	bhi	L1133
7411  18f6 e643              	ldab	3,y
7412  18f8 c107              	cmpb	#7
7413  18fa 18250187          	blo	L1133
7415  18fe c164              	cmpb	#100
7416                         ; 5189 			temp_return_c = 0;
7419  1900 18220181          	bhi	L1133
7420                         ; 5198 			if (service == HS_PWM_REAR)
7422  1904 e682              	ldab	OFST+1,s
7423  1906 c112              	cmpb	#18
7424  1908 2604              	bne	L5533
7425                         ; 5200 				hs_vehicle_line_offset_c= VEH_LINE_REAROFFSET;	
7427  190a c601              	ldab	#1
7429  190c 2001              	bra	L7533
7430  190e                   L5533:
7431                         ; 5204 				hs_vehicle_line_offset_c= VEH_LINE_FRONTOFFSET;
7433  190e c7                	clrb	
7434  190f                   L7533:
7435  190f 7b0018            	stab	_hs_vehicle_line_offset_c
7436                         ; 5208 			hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].Pwm_c[0] = *data_p_c;
7438  1912 87                	clra	
7439  1913 fb010d            	addb	_hs_seat_config_c
7440  1916 45                	rola	
7441  1917 cd000e            	ldy	#14
7442  191a 13                	emul	
7443  191b b746              	tfr	d,y
7444  191d ee86              	ldx	OFST+5,s
7445  191f 180a00ea0077      	movb	0,x,_hs_cal_prms,y
7446                         ; 5209 			hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].Pwm_c[1] = *(data_p_c + 1);
7448  1925 180a01ea0078      	movb	1,x,_hs_cal_prms+1,y
7449                         ; 5210 			hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].Pwm_c[2] = *(data_p_c + 2);
7451  192b 180a02ea0079      	movb	2,x,_hs_cal_prms+2,y
7452                         ; 5211 			hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].Pwm_c[3] = *(data_p_c + 3);
7454  1931 180a03ea007a      	movb	3,x,_hs_cal_prms+3,y
7455                         ; 5214 			temp_return_c = 1;
7457  1937 182000dd          	bra	LC064
7458  193b                   L3723:
7459                         ; 5230 		if ((*data_p_c < 1  || *data_p_c > 60 ) ||
7459                         ; 5231 		    (*(data_p_c + 1) < 1  || *(data_p_c + 1) > 60) ||
7459                         ; 5232 		    (*(data_p_c + 2) < 1  || *(data_p_c + 2) > 60) ||
7459                         ; 5233 		    (*(data_p_c + 3) < 1  || *(data_p_c + 3) > 60) ) {
7461  193b ed86              	ldy	OFST+5,s
7462  193d e640              	ldab	0,y
7463  193f 18270142          	beq	L1133
7465  1943 c13c              	cmpb	#60
7466  1945 1822013c          	bhi	L1133
7468  1949 e641              	ldab	1,y
7469  194b 18270136          	beq	L1133
7471  194f c13c              	cmpb	#60
7472  1951 18220130          	bhi	L1133
7474  1955 e642              	ldab	2,y
7475  1957 1827012a          	beq	L1133
7477  195b c13c              	cmpb	#60
7478  195d 18220124          	bhi	L1133
7480  1961 e643              	ldab	3,y
7481  1963 1827011e          	beq	L1133
7483  1967 c13c              	cmpb	#60
7484                         ; 5236 			temp_return_c = 0;
7487  1969 18220118          	bhi	L1133
7488                         ; 5245 			if (service == HS_TIMEOUT_REAR)
7490  196d e682              	ldab	OFST+1,s
7491  196f c113              	cmpb	#19
7492  1971 2604              	bne	L5733
7493                         ; 5247 				hs_vehicle_line_offset_c = VEH_LINE_REAROFFSET; 
7495  1973 c601              	ldab	#1
7497  1975 2001              	bra	L7733
7498  1977                   L5733:
7499                         ; 5252 				hs_vehicle_line_offset_c = VEH_LINE_FRONTOFFSET;
7501  1977 c7                	clrb	
7502  1978                   L7733:
7503  1978 7b0018            	stab	_hs_vehicle_line_offset_c
7504                         ; 5257 			hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].Max_TimeOut_c[0] = *data_p_c;
7506  197b 87                	clra	
7507  197c fb010d            	addb	_hs_seat_config_c
7508  197f 45                	rola	
7509  1980 cd000e            	ldy	#14
7510  1983 13                	emul	
7511  1984 b746              	tfr	d,y
7512  1986 ee86              	ldx	OFST+5,s
7513  1988 180a00ea007b      	movb	0,x,_hs_cal_prms+4,y
7514                         ; 5258 			hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].Max_TimeOut_c[1] = *(data_p_c + 1);
7516  198e 180a01ea007c      	movb	1,x,_hs_cal_prms+5,y
7517                         ; 5259 			hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].Max_TimeOut_c[2] = *(data_p_c + 2);
7519  1994 180a02ea007d      	movb	2,x,_hs_cal_prms+6,y
7520                         ; 5260 			hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].Max_TimeOut_c[3] = *(data_p_c + 3);
7522  199a 180a03ea007e      	movb	3,x,_hs_cal_prms+7,y
7523                         ; 5263 			temp_return_c = 1;
7525  19a0 2076              	bra	LC064
7526  19a2                   L5723:
7527                         ; 5275 				hsLF_CopyDiagnoseParameter( data_p_c );
7530                         ; 5278 				temp_return_c = 1; 
7532                         ; 5285 			break;
7534  19a2 18200092          	bra	L5033
7535  19a6                   L7723:
7536                         ; 5300 			if ((*data_p_c == 0x03) || (*data_p_c == 0x0F) || (*data_p_c == 0x73) || (*data_p_c == 0x43) ||
7536                         ; 5301 			    (*data_p_c == 0x33) || (*data_p_c == 0x4F) || (*data_p_c == 0x7F) )
7538  19a6 e6f30006          	ldab	[OFST+5,s]
7539  19aa c103              	cmpb	#3
7540  19ac 276a              	beq	LC064
7542  19ae c10f              	cmpb	#15
7543  19b0 2766              	beq	LC064
7545  19b2 c173              	cmpb	#115
7546  19b4 2762              	beq	LC064
7548  19b6 c143              	cmpb	#67
7549  19b8 275e              	beq	LC064
7551  19ba c133              	cmpb	#51
7552  19bc 275a              	beq	LC064
7554  19be c14f              	cmpb	#79
7555  19c0 2756              	beq	LC064
7557  19c2 c17f              	cmpb	#127
7558  19c4 182600bd          	bne	L1133
7559                         ; 5307 				temp_return_c = 1;
7562  19c8 204e              	bra	LC064
7563                         ; 5311 				temp_return_c = 0;
7565  19ca                   L1033:
7566                         ; 5327 			if ((*data_p_c > 235  || *data_p_c < 20 ) ||
7566                         ; 5328 			    (*(data_p_c + 1) > 235  || *(data_p_c + 1) < 20) ||
7566                         ; 5329 			    (*(data_p_c + 2) > 235  || *(data_p_c + 2) < 20) ||
7566                         ; 5330 			    (*(data_p_c + 3) > 235  || *(data_p_c + 3) < 20) ||
7566                         ; 5331 			    (*(data_p_c + 4) > 235  || *(data_p_c + 4) < 20) ) {
7568  19ca ed86              	ldy	OFST+5,s
7569  19cc e640              	ldab	0,y
7570  19ce c1eb              	cmpb	#235
7571  19d0 182200b1          	bhi	L1133
7573  19d4 c114              	cmpb	#20
7574  19d6 182500ab          	blo	L1133
7576  19da e641              	ldab	1,y
7577  19dc c1eb              	cmpb	#235
7578  19de 182200a3          	bhi	L1133
7580  19e2 c114              	cmpb	#20
7581  19e4 1825009d          	blo	L1133
7583  19e8 e642              	ldab	2,y
7584  19ea c1eb              	cmpb	#235
7585  19ec 18220095          	bhi	L1133
7587  19f0 c114              	cmpb	#20
7588  19f2 1825008f          	blo	L1133
7590  19f6 e643              	ldab	3,y
7591  19f8 c1eb              	cmpb	#235
7592  19fa 18220087          	bhi	L1133
7594  19fe c114              	cmpb	#20
7595  1a00 18250081          	blo	L1133
7597  1a04 e644              	ldab	4,y
7598  1a06 c1eb              	cmpb	#235
7599  1a08 227b              	bhi	L1133
7601  1a0a c114              	cmpb	#20
7602                         ; 5334 				temp_return_c = 0;
7605  1a0c 2577              	blo	L1133
7606                         ; 5339 				hsLF_ConvertDegToAD( service, data_p_c );
7608  1a0e 35                	pshy	
7609  1a0f e684              	ldab	OFST+3,s
7610  1a11 87                	clra	
7611  1a12 4a1aa3a3          	call	f_hsLF_ConvertDegToAD
7613  1a16 1b82              	leas	2,s
7614                         ; 5344 				temp_return_c = 1;
7616  1a18                   LC064:
7617  1a18 c601              	ldab	#1
7618  1a1a 206a              	bra	L7333
7619  1a1c                   L3033:
7620                         ; 5357 			if ((*data_p_c > 45 ) ||
7620                         ; 5358 			    (*(data_p_c + 1) > 30) ||
7620                         ; 5359 			    (*(data_p_c + 2) > 20) ||
7620                         ; 5360 			    (*(data_p_c + 3) > 10) ) {
7622  1a1c ed86              	ldy	OFST+5,s
7623  1a1e e640              	ldab	0,y
7624  1a20 c12d              	cmpb	#45
7625  1a22 2261              	bhi	L1133
7627  1a24 e641              	ldab	1,y
7628  1a26 c11e              	cmpb	#30
7629  1a28 225b              	bhi	L1133
7631  1a2a e642              	ldab	2,y
7632  1a2c c114              	cmpb	#20
7633  1a2e 2255              	bhi	L1133
7635  1a30 e643              	ldab	3,y
7636  1a32 c10a              	cmpb	#10
7637  1a34 23e2              	bls	LC064
7638                         ; 5363 				temp_return_c = 0;
7641  1a36 204d              	bra	L1133
7642                         ; 5376 				temp_return_c = 1;
7644  1a38                   L5033:
7645                         ; 5405 				hsLF_CopyDiagnoseParameter( data_p_c );
7647  1a38 ec86              	ldd	OFST+5,s
7648  1a3a 4a1a8989          	call	f_hsLF_CopyDiagnoseParameter
7650                         ; 5408 				temp_return_c = 1;
7652                         ; 5410 			break;
7654  1a3e 20d8              	bra	LC064
7655  1a40                   L7033:
7656                         ; 5422 			if( (*data_p_c < 0x01 || *data_p_c > 0x04) ||					// Iref Check
7656                         ; 5423 				(*(data_p_c+1) < 0x1 ) ||									// Vref Low range Check1 < 0x1xx
7656                         ; 5424 				( (*(data_p_c+1) == 0x01 && *(data_p_c+2) < 0x2C) ) ||		// Vref Low range Check2 < 0x12C
7656                         ; 5425 				( *(data_p_c+1) > 0x05 ) ||									// Vref high range Check1 > 0x5xx
7656                         ; 5426 				( (*(data_p_c+1) == 0x05 && *(data_p_c+2) > 0x8F) ) ){		// Vref high range Check2 > 0x58F
7658  1a40 ed86              	ldy	OFST+5,s
7659  1a42 e640              	ldab	0,y
7660  1a44 273f              	beq	L1133
7662  1a46 c104              	cmpb	#4
7663  1a48 223b              	bhi	L1133
7665  1a4a e641              	ldab	1,y
7666  1a4c 2737              	beq	L1133
7668  1a4e c101              	cmpb	#1
7669  1a50 2608              	bne	L1643
7671  1a52 e642              	ldab	2,y
7672  1a54 c12c              	cmpb	#44
7673  1a56 252d              	blo	L1133
7674  1a58 e641              	ldab	1,y
7675  1a5a                   L1643:
7677  1a5a c105              	cmpb	#5
7678  1a5c 2227              	bhi	L1133
7680  1a5e 2606              	bne	L1543
7682  1a60 e642              	ldab	2,y
7683  1a62 c18f              	cmpb	#143
7684                         ; 5429 				temp_return_c = 0;				
7687  1a64 221f              	bhi	L1133
7688  1a66                   L1543:
7689                         ; 5433 				AD_Iref_seatH_c = *(data_p_c);
7691  1a66 180d400000        	movb	0,y,_AD_Iref_seatH_c
7692                         ; 5434 				AD_Vref_seatH_w = *(data_p_c+1);
7694  1a6b e641              	ldab	1,y
7695  1a6d 87                	clra	
7696  1a6e 7c0000            	std	_AD_Vref_seatH_w
7697                         ; 5435 				AD_Vref_seatH_w = (AD_Vref_seatH_w<<8);		// shift data_p_c+1 information to high byte
7699  1a71 b60001            	ldaa	_AD_Vref_seatH_w+1
7700  1a74 c7                	clrb	
7701  1a75 7c0000            	std	_AD_Vref_seatH_w
7702                         ; 5436 				AD_Vref_seatH_w |= *(data_p_c+2);			// or with low byte (data_p_c+2) 
7704  1a78 e642              	ldab	2,y
7705  1a7a b796              	exg	b,y
7706  1a7c 18fa0000          	ory	_AD_Vref_seatH_w
7707  1a80 7d0000            	sty	_AD_Vref_seatH_w
7708                         ; 5439 				temp_return_c = 1;				
7710  1a83 2093              	bra	LC064
7711  1a85                   L1133:
7712                         ; 5447 			temp_return_c = 0;
7714  1a85 c7                	clrb	
7715                         ; 5449 			break;
7717  1a86                   L7333:
7718                         ; 5453 	return( temp_return_c );
7722  1a86 1b83              	leas	3,s
7723  1a88 0a                	rtc	
7766                         ; 5483 void hsLF_CopyDiagnoseParameter( unsigned char *data_p_c )
7766                         ; 5484 {
7767                         	switch	.ftext
7768  1a89                   f_hsLF_CopyDiagnoseParameter:
7770  1a89 3b                	pshd	
7771  1a8a 37                	pshb	
7772       00000001          OFST:	set	1
7775                         ; 5488 	for ( i = 0; i < 5; i++ ) {
7777  1a8b c7                	clrb	
7778  1a8c 6b80              	stab	OFST-1,s
7779  1a8e cd0027            	ldy	#_hs_diag_prog_data_c
7780  1a91 ee81              	ldx	OFST+0,s
7781  1a93                   L7053:
7782                         ; 5489 		hs_diag_prog_data_c[ i ] = *(data_p_c + i);
7784  1a93 87                	clra	
7785  1a94 180ae6ee          	movb	d,x,d,y
7786                         ; 5488 	for ( i = 0; i < 5; i++ ) {
7788  1a98 6280              	inc	OFST-1,s
7791  1a9a e680              	ldab	OFST-1,s
7792  1a9c c105              	cmpb	#5
7793  1a9e 25f3              	blo	L7053
7794                         ; 5491 }
7797  1aa0 1b83              	leas	3,s
7798  1aa2 0a                	rtc	
7851                         ; 5509 void hsLF_ConvertDegToAD(unsigned char serv, unsigned char *data_p_c)
7851                         ; 5510 {
7852                         	switch	.ftext
7853  1aa3                   f_hsLF_ConvertDegToAD:
7855  1aa3 3b                	pshd	
7856  1aa4 37                	pshb	
7857       00000001          OFST:	set	1
7860                         ; 5515 	if (serv == HS_NTC_CUTOFF_REAR)
7862  1aa5 c114              	cmpb	#20
7863  1aa7 2607              	bne	L7353
7864                         ; 5517 		hs_vehicle_line_offset_c= VEH_LINE_REAROFFSET;	
7866  1aa9 c601              	ldab	#1
7867  1aab 7b0018            	stab	_hs_vehicle_line_offset_c
7869  1aae 2003              	bra	L1453
7870  1ab0                   L7353:
7871                         ; 5521 		hs_vehicle_line_offset_c= VEH_LINE_FRONTOFFSET;
7873  1ab0 790018            	clr	_hs_vehicle_line_offset_c
7874  1ab3                   L1453:
7875                         ; 5525 	for ( i = 0; i < 5; i++ ) {
7877  1ab3 6980              	clr	OFST-1,s
7878  1ab5                   L3453:
7879                         ; 5534 		hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].NTC_CutOFF_c[i] = TempF_CanTemp_to_NtcAdStd( *(data_p_c + i) ); //MKS 50039 & MKS 54427 
7881  1ab5 f60018            	ldab	_hs_vehicle_line_offset_c
7882  1ab8 87                	clra	
7883  1ab9 fb010d            	addb	_hs_seat_config_c
7884  1abc 45                	rola	
7885  1abd cd000e            	ldy	#14
7886  1ac0 13                	emul	
7887  1ac1 b746              	tfr	d,y
7888  1ac3 e680              	ldab	OFST-1,s
7889  1ac5 87                	clra	
7890  1ac6 19ed              	leay	b,y
7891  1ac8 35                	pshy	
7892  1ac9 ed88              	ldy	OFST+7,s
7893  1acb e6ee              	ldab	d,y
7894  1acd 4a000000          	call	f_TempF_CanTemp_to_NtcAdStd
7896  1ad1 31                	puly	
7897  1ad2 6bea007f          	stab	_hs_cal_prms+8,y
7898                         ; 5525 	for ( i = 0; i < 5; i++ ) {
7900  1ad6 6280              	inc	OFST-1,s
7903  1ad8 e680              	ldab	OFST-1,s
7904  1ada c105              	cmpb	#5
7905  1adc 25d7              	blo	L3453
7906                         ; 5537 }
7909  1ade 1b83              	leas	3,s
7910  1ae0 0a                	rtc	
7960                         ; 5569 @far unsigned char hsF_WriteData_to_ShadowEEPROM(unsigned char service)
7960                         ; 5570 {
7961                         	switch	.ftext
7962  1ae1                   f_hsF_WriteData_to_ShadowEEPROM:
7964  1ae1 3b                	pshd	
7965  1ae2 1b9b              	leas	-5,s
7966       00000005          OFST:	set	5
7969                         ; 5572 	u_8Bit hs_result_c = FALSE;   // write result (success or fail)
7971  1ae4 87                	clra	
7972  1ae5 6a84              	staa	OFST-1,s
7973                         ; 5574 	switch (service) {
7976  1ae7 c009              	subb	#9
7977  1ae9 c109              	cmpb	#9
7978  1aeb 2415              	bhs	L461
7979  1aed 59                	lsld	
7980  1aee 05ff              	jmp	[d,pc]
7981  1af0 1b48              	dc.w	L7553
7982  1af2 1b74              	dc.w	L7653
7983  1af4 1b74              	dc.w	L7653
7984  1af6 1b55              	dc.w	L1653
7985  1af8 1b74              	dc.w	L7653
7986  1afa 1b57              	dc.w	L3653
7987  1afc 1b70              	dc.w	L5653
7988  1afe 1b74              	dc.w	L7653
7989  1b00 1b29              	dc.w	L5553
7990  1b02                   L461:
7991  1b02 c0f7              	subb	#-9
7992  1b04 2705              	beq	L1553
7993  1b06 040111            	dbeq	b,L3553
7994  1b09 2069              	bra	L7653
7995  1b0b                   L1553:
7996                         ; 5582 			EE_BlockWrite(EE_HEAT_CAL_DATA_PF, (u_8Bit*)&hs_cal_prms);
7998  1b0b cc0077            	ldd	#_hs_cal_prms
7999  1b0e 3b                	pshd	
8000  1b0f cc000e            	ldd	#14
8001  1b12 4a000000          	call	f_EE_BlockWrite
8003  1b16 1b82              	leas	2,s
8004                         ; 5583 			hs_result_c = TRUE;
8006                         ; 5584 			break;
8008  1b18 2056              	bra	L5653
8009  1b1a                   L3553:
8010                         ; 5593 			EE_BlockWrite(EE_HEAT_CAL_DATA_PF, (u_8Bit*)&hs_cal_prms);
8012  1b1a cc0077            	ldd	#_hs_cal_prms
8013  1b1d 3b                	pshd	
8014  1b1e cc000e            	ldd	#14
8015  1b21 4a000000          	call	f_EE_BlockWrite
8017  1b25 1b82              	leas	2,s
8018                         ; 5594 			hs_result_c = TRUE;
8020                         ; 5595 			break;
8022  1b27 2047              	bra	L5653
8023  1b29                   L5553:
8024                         ; 5603 			dest = (*(unsigned long*)(&hs_diag_prog_data_c[ 0 ]));
8026  1b29 1801820029        	movw	_hs_diag_prog_data_c+2,OFST-3,s
8027  1b2e 1801800027        	movw	_hs_diag_prog_data_c,OFST-5,s
8028                         ; 5604 			if (EE_DirectWrite(dest,&hs_diag_prog_data_c[ 5 ],hs_diag_prog_data_c[ 4 ]) == 0) {
8030  1b33 f6002b            	ldab	_hs_diag_prog_data_c+4
8031  1b36 87                	clra	
8032  1b37 3b                	pshd	
8033  1b38 cc002c            	ldd	#_hs_diag_prog_data_c+5
8034  1b3b 3b                	pshd	
8035  1b3c ec86              	ldd	OFST+1,s
8036  1b3e ee84              	ldx	OFST-1,s
8037  1b40 4a000000          	call	f_EE_DirectWrite
8039  1b44 1b84              	leas	4,s
8040                         ; 5608 			hs_result_c = TRUE;
8043                         ; 5612 			hs_result_c = TRUE;
8045  1b46 2028              	bra	L5653
8046  1b48                   L7553:
8047                         ; 5639 			EE_BlockWrite(EE_HEAT_CAL_DATA_PF, (u_8Bit*)&hs_cal_prms);
8049  1b48 cc0077            	ldd	#_hs_cal_prms
8050  1b4b 3b                	pshd	
8051  1b4c cc000e            	ldd	#14
8052  1b4f 4a000000          	call	f_EE_BlockWrite
8054  1b53 1b82              	leas	2,s
8055                         ; 5640 			hs_result_c = TRUE;
8057                         ; 5641 			break;
8059  1b55                   L1653:
8060                         ; 5648 			hs_result_c = TRUE;
8062                         ; 5649 			break;
8064  1b55 2019              	bra	L5653
8065  1b57                   L3653:
8066                         ; 5654 			EE_BlockWrite(EE_REMOTE_CTRL_HEAT_AMBTEMP, &hs_diag_prog_data_c[1]);
8068  1b57 cc0028            	ldd	#_hs_diag_prog_data_c+1
8069  1b5a 3b                	pshd	
8070  1b5b cc0011            	ldd	#17
8071  1b5e 4a000000          	call	f_EE_BlockWrite
8073                         ; 5655 			EE_BlockWrite(EE_REMOTE_CTRL_VENT_AMBTEMP, &hs_diag_prog_data_c[3]);
8075  1b62 cc002a            	ldd	#_hs_diag_prog_data_c+3
8076  1b65 6c80              	std	0,s
8077  1b67 cc0012            	ldd	#18
8078  1b6a 4a000000          	call	f_EE_BlockWrite
8080  1b6e 1b82              	leas	2,s
8081                         ; 5657 			hs_result_c = TRUE;
8083                         ; 5658 			break;
8085  1b70                   L5653:
8086                         ; 5668 			hs_result_c = TRUE;		
8088  1b70 c601              	ldab	#1
8089                         ; 5669 			break;
8091  1b72 2001              	bra	L3163
8092  1b74                   L7653:
8093                         ; 5675 			hs_result_c = FALSE;
8095  1b74 c7                	clrb	
8096  1b75                   L3163:
8097                         ; 5678 	return (hs_result_c);
8101  1b75 1b87              	leas	7,s
8102  1b77 0a                	rtc	
8142                         ; 5696 @far unsigned char hsF_GetActv_OutputNumber(void)
8142                         ; 5697 {
8143                         	switch	.ftext
8144  1b78                   f_hsF_GetActv_OutputNumber:
8146  1b78 3b                	pshd	
8147       00000002          OFST:	set	2
8150                         ; 5698 	unsigned char hs_actv_number_c = 0;	// Number of active heats
8152                         ; 5699 	unsigned char cnt_c = 0;			// loop counter
8154                         ; 5701 	for(cnt_c=0; cnt_c<ALL_HEAT_SEATS; cnt_c++)
8156  1b79 87                	clra	
8157  1b7a c7                	clrb	
8158  1b7b 6c80              	std	OFST-2,s
8159  1b7d 180e              	tab	
8160  1b7f                   L7363:
8161                         ; 5704 		if( (hs_status2_flags_c[cnt_c].b.hs_heater_state_b == TRUE) && (hs_status2_flags_c[cnt_c].b.hs_HL3_set_b == TRUE) )
8163  1b7f b796              	exg	b,y
8164  1b81 0fea01154008      	brclr	_hs_status2_flags_c,y,64,L7463
8166  1b87 0fea01150102      	brclr	_hs_status2_flags_c,y,1,L7463
8167                         ; 5707 			hs_actv_number_c++;
8169  1b8d 6281              	inc	OFST-1,s
8171  1b8f                   L7463:
8172                         ; 5701 	for(cnt_c=0; cnt_c<ALL_HEAT_SEATS; cnt_c++)
8174  1b8f 6280              	inc	OFST-2,s
8177  1b91 e680              	ldab	OFST-2,s
8178  1b93 c104              	cmpb	#4
8179  1b95 25e8              	blo	L7363
8180                         ; 5712 	return(hs_actv_number_c);
8182  1b97 e681              	ldab	OFST-1,s
8185  1b99 31                	puly	
8186  1b9a 0a                	rtc	
8214                         ; 5731 @far void hsF_RearSeat_TurnOFF(void)
8214                         ; 5732 {
8215                         	switch	.ftext
8216  1b9b                   f_hsF_RearSeat_TurnOFF:
8220                         ; 5733 		if(hs_rem_seat_req_c[REAR_LEFT] != SET_OFF_REQUEST)
8222  1b9b f600ff            	ldab	_hs_rem_seat_req_c+2
8223  1b9e 270c              	beq	L1663
8224                         ; 5736 			hsLF_UpdateHeatParameters(REAR_LEFT, HEAT_LEVEL_OFF);
8226  1ba0 cc0007            	ldd	#7
8227  1ba3 3b                	pshd	
8228  1ba4 c602              	ldab	#2
8229  1ba6 4a0d5050          	call	f_hsLF_UpdateHeatParameters
8231  1baa 1b82              	leas	2,s
8232  1bac                   L1663:
8233                         ; 5742 		if(hs_rem_seat_req_c[REAR_RIGHT] != SET_OFF_REQUEST)
8235  1bac f60100            	ldab	_hs_rem_seat_req_c+3
8236  1baf 270c              	beq	L3663
8237                         ; 5745 			hsLF_UpdateHeatParameters(REAR_RIGHT, HEAT_LEVEL_OFF);
8239  1bb1 cc0007            	ldd	#7
8240  1bb4 3b                	pshd	
8241  1bb5 c603              	ldab	#3
8242  1bb7 4a0d5050          	call	f_hsLF_UpdateHeatParameters
8244  1bbb 1b82              	leas	2,s
8245  1bbd                   L3663:
8246                         ; 5752 			Dio_WriteChannel(LED_U12S_EN, STD_LOW);
8249  1bbd 1410              	sei	
8254  1bbf fe0000            	ldx	_Dio_PortWrite_Ptr
8255  1bc2 0d0001            	bclr	0,x,1
8259  1bc5 10ef              	cli	
8261                         ; 5753 }
8265  1bc7 0a                	rtc	
8267                         	switch	.bss
8268  0000                   L5663_pre_error:
8269  0000 0000000000000000  	ds.b	8
8270  0008                   L7663_i_term:
8271  0008 0000000000000000  	ds.b	16
8361                         .const:	section	.data
8362                         	even
8363  0000                   L471:
8364  0000 001e0000          	dc.l	1966080
8365                         	even
8366  0004                   L671:
8367  0004 00050000          	dc.l	327680
8368                         ; 5771 void pid_cal(void)
8368                         ; 5772 {
8369                         	switch	.ftext
8370  1bc8                   f_pid_cal:
8372  1bc8 1b95              	leas	-11,s
8373       0000000b          OFST:	set	11
8376                         ; 5785   for (hs_position = 0; hs_position < ALL_HEAT_SEATS; hs_position++)
8378  1bca c7                	clrb	
8379  1bcb 6b84              	stab	OFST-7,s
8380  1bcd                   L7273:
8381                         ; 5788     SWAPPED_NTC_AD_READING[hs_position] = (u_8Bit)(PID_NTC_MAX) - NTC_AD_Readings_c[hs_position];      
8383  1bcd 87                	clra	
8384  1bce b746              	tfr	d,y
8385  1bd0 b745              	tfr	d,x
8386  1bd2 e6e200e4          	ldab	_NTC_AD_Readings_c,x
8387  1bd6 c0ff              	subb	#255
8388  1bd8 50                	negb	
8389  1bd9 6bea006a          	stab	_SWAPPED_NTC_AD_READING,y
8390                         ; 5790     if (hs_pid_setpoint[hs_position] != 0)
8392  1bdd e7ea006e          	tst	_hs_pid_setpoint,y
8393  1be1 182700d3          	beq	L5373
8394                         ; 5793       actual = (u_8Bit)(PID_NTC_MAX) - NTC_AD_Readings_c[hs_position];
8396  1be5 e6ea00e4          	ldab	_NTC_AD_Readings_c,y
8397  1be9 c0ff              	subb	#255
8398  1beb 50                	negb	
8399  1bec 7b0069            	stab	_actual
8400                         ; 5796       error = (s_16Bit)(hs_pid_setpoint[hs_position] - actual);
8402  1bef e6ea006e          	ldab	_hs_pid_setpoint,y
8403  1bf3 f00069            	subb	_actual
8404  1bf6 8200              	sbca	#0
8405  1bf8 6c85              	std	OFST-6,s
8406                         ; 5799       p_term = (s_32Bit)(PID_KP)*(s_32Bit)(error);
8408  1bfa b74d              	sex	d,x
8409  1bfc cd0000            	ldy	#L471
8410  1bff 160000            	jsr	c_lmul
8412  1c02 6c89              	std	OFST-2,s
8413  1c04 6e87              	stx	OFST-4,s
8414                         ; 5802       i_term_scalar = i_term[hs_position];
8416  1c06 cd0008            	ldy	#L7663_i_term
8417  1c09 e684              	ldab	OFST-7,s
8418  1c0b 87                	clra	
8419  1c0c 59                	lsld	
8420  1c0d 59                	lsld	
8421  1c0e 19ee              	leay	d,y
8422  1c10 18024282          	movw	2,y,OFST-9,s
8423  1c14 18024080          	movw	0,y,OFST-11,s
8424                         ; 5804       i_term_scalar = i_term_scalar + (s_32Bit)(PID_KI*PID_DT)*(s_32Bit)(error);
8426  1c18 ec85              	ldd	OFST-6,s
8427  1c1a b74d              	sex	d,x
8428  1c1c cd0004            	ldy	#L671
8429  1c1f 160000            	jsr	c_lmul
8431  1c22 e382              	addd	OFST-9,s
8432  1c24 18a980            	adex	OFST-11,s
8433  1c27 6c82              	std	OFST-9,s
8434  1c29 6e80              	stx	OFST-11,s
8435                         ; 5806       if (i_term_scalar > PID_MAX)
8437  1c2b 8c0000            	cpd	#0
8438  1c2e 188e0064          	cpex	#100
8439  1c32 2f0a              	ble	L7373
8440                         ; 5808         i_term_scalar = PID_MAX;
8442  1c34 87                	clra	
8443  1c35 c7                	clrb	
8444  1c36 6c82              	std	OFST-9,s
8445  1c38 c664              	ldab	#100
8446  1c3a 6c80              	std	OFST-11,s
8448  1c3c 200a              	bra	L1473
8449  1c3e                   L7373:
8450                         ; 5810       else if (i_term_scalar < PID_MIN)
8452  1c3e e680              	ldab	OFST-11,s
8453  1c40 2c06              	bge	L1473
8454                         ; 5812         i_term_scalar = PID_MIN;
8456  1c42 87                	clra	
8457  1c43 c7                	clrb	
8458  1c44 6c80              	std	OFST-11,s
8459  1c46 6c82              	std	OFST-9,s
8460  1c48                   L1473:
8461                         ; 5815       i_term[hs_position] = i_term_scalar;
8463  1c48 cd0008            	ldy	#L7663_i_term
8464  1c4b e684              	ldab	OFST-7,s
8465  1c4d 87                	clra	
8466  1c4e 59                	lsld	
8467  1c4f 59                	lsld	
8468  1c50 19ee              	leay	d,y
8469  1c52 18028242          	movw	OFST-9,s,2,y
8470  1c56 18028040          	movw	OFST-11,s,0,y
8471                         ; 5818       d_term = (s_32Bit)(PID_KD/PID_DT)*(s_32Bit)(error - pre_error[hs_position]);
8473  1c5a e684              	ldab	OFST-7,s
8474  1c5c 87                	clra	
8475  1c5d 59                	lsld	
8476  1c5e b746              	tfr	d,y
8477  1c60 ec85              	ldd	OFST-6,s
8478  1c62 a3ea0000          	subd	L5663_pre_error,y
8479  1c66 cd1999            	ldy	#6553
8480  1c69 1813              	emuls	
8481  1c6b 6c82              	std	OFST-9,s
8482  1c6d 6d80              	sty	OFST-11,s
8483                         ; 5821       output = p_term + i_term[hs_position] + d_term;
8485  1c6f cd0008            	ldy	#L7663_i_term
8486  1c72 e684              	ldab	OFST-7,s
8487  1c74 87                	clra	
8488  1c75 59                	lsld	
8489  1c76 59                	lsld	
8490  1c77 19ee              	leay	d,y
8491  1c79 ec42              	ldd	2,y
8492  1c7b ee40              	ldx	0,y
8493  1c7d e389              	addd	OFST-2,s
8494  1c7f 18a987            	adex	OFST-4,s
8495  1c82 e382              	addd	OFST-9,s
8496  1c84 18a980            	adex	OFST-11,s
8497  1c87 6c82              	std	OFST-9,s
8498  1c89 6e80              	stx	OFST-11,s
8499                         ; 5823       if (output > PID_MAX)
8501  1c8b 8c0000            	cpd	#0
8502  1c8e 188e0064          	cpex	#100
8503  1c92 2f0a              	ble	L5473
8504                         ; 5825         output = PID_MAX;
8506  1c94 87                	clra	
8507  1c95 c7                	clrb	
8508  1c96 6c82              	std	OFST-9,s
8509  1c98 c664              	ldab	#100
8510  1c9a 6c80              	std	OFST-11,s
8512  1c9c 200a              	bra	L7473
8513  1c9e                   L5473:
8514                         ; 5827       else if (output < PID_MIN)
8516  1c9e e680              	ldab	OFST-11,s
8517  1ca0 2c06              	bge	L7473
8518                         ; 5829         output = PID_MIN;
8520  1ca2 87                	clra	
8521  1ca3 c7                	clrb	
8522  1ca4 6c80              	std	OFST-11,s
8523  1ca6 6c82              	std	OFST-9,s
8524  1ca8                   L7473:
8525                         ; 5833       pre_error[hs_position] = error;
8527  1ca8 e684              	ldab	OFST-7,s
8528  1caa 87                	clra	
8529  1cab 59                	lsld	
8530  1cac b746              	tfr	d,y
8531  1cae 180285ea0000      	movw	OFST-6,s,L5663_pre_error,y
8533  1cb4 e681              	ldab	OFST-10,s
8534  1cb6 201f              	bra	L3573
8535  1cb8                   L5373:
8536                         ; 5838       i_term[hs_position] = 0;
8538  1cb8 cd0008            	ldy	#L7663_i_term
8539  1cbb e684              	ldab	OFST-7,s
8540  1cbd 59                	lsld	
8541  1cbe 59                	lsld	
8542  1cbf 19ee              	leay	d,y
8543  1cc1 87                	clra	
8544  1cc2 c7                	clrb	
8545  1cc3 6c40              	std	0,y
8546  1cc5 6c42              	std	2,y
8547                         ; 5839       pre_error[hs_position] = 0;
8549  1cc7 e684              	ldab	OFST-7,s
8550  1cc9 b746              	tfr	d,y
8551  1ccb 1858              	lsly	
8552  1ccd c7                	clrb	
8553  1cce 6cea0000          	std	L5663_pre_error,y
8554  1cd2 6c80              	std	OFST-11,s
8555                         ; 5840       output = 0;
8557  1cd4 186982            	clrw	OFST-9,s
8558  1cd7                   L3573:
8559                         ; 5844     hsLF_ControlFET(hs_position, ((u_8Bit)(output >> PID_FRAC_BITS)));
8561  1cd7 87                	clra	
8562  1cd8 3b                	pshd	
8563  1cd9 e686              	ldab	OFST-5,s
8564  1cdb 4a14bbbb          	call	f_hsLF_ControlFET
8566  1cdf 1b82              	leas	2,s
8567                         ; 5785   for (hs_position = 0; hs_position < ALL_HEAT_SEATS; hs_position++)
8569  1ce1 6284              	inc	OFST-7,s
8572  1ce3 e684              	ldab	OFST-7,s
8573  1ce5 c104              	cmpb	#4
8574  1ce7 1825fee2          	blo	L7273
8575                         ; 5846 }
8578  1ceb 1b8b              	leas	11,s
8579  1ced 0a                	rtc	
8581                         	switch	.data
8582  0000                   L5573_wait_time:
8583  0000 0000              	dc.w	0
8624                         ; 5866 void pid_heat_setting(void)
8624                         ; 5867 { 
8625                         	switch	.ftext
8626  1cee                   f_pid_heat_setting:
8628  1cee 37                	pshb	
8629       00000001          OFST:	set	1
8632                         ; 5873   if (wait_time >= PID_WAIT_TIME)
8634  1cef fc0000            	ldd	L5573_wait_time
8635  1cf2 8c0400            	cpd	#1024
8636  1cf5 2555              	blo	L5773
8637                         ; 5876      for(hs_position = 0; hs_position < ALL_HEAT_SEATS; hs_position++)
8639  1cf7 c7                	clrb	
8640  1cf8 6b80              	stab	OFST-1,s
8641  1cfa                   L7773:
8642                         ; 5878       if (hs_status2_flags_c[hs_position].b.hs_HL3_set_b == TRUE)              /* In HL3 now */
8644  1cfa 87                	clra	
8645  1cfb b746              	tfr	d,y
8646  1cfd 0fea01150104      	brclr	_hs_status2_flags_c,y,1,L5004
8647                         ; 5880         hs_pid_setpoint[hs_position] = PID_HEAT_HL3;
8649  1d03 c67d              	ldab	#125
8651  1d05 2022              	bra	LC067
8652  1d07                   L5004:
8653                         ; 5882       else if(hs_status2_flags_c[hs_position].b.hs_HL2_set_b == TRUE)          /* In HL2 now */
8655  1d07 b746              	tfr	d,y
8656  1d09 0fea01150204      	brclr	_hs_status2_flags_c,y,2,L1104
8657                         ; 5884         hs_pid_setpoint[hs_position] = PID_HEAT_HL2;
8659  1d0f c66d              	ldab	#109
8661  1d11 2016              	bra	LC067
8662  1d13                   L1104:
8663                         ; 5886       else if (hs_status2_flags_c[hs_position].b.hs_HL1_set_b == TRUE)         /* In HL1 now */
8665  1d13 b746              	tfr	d,y
8666  1d15 0fea01150404      	brclr	_hs_status2_flags_c,y,4,L5104
8667                         ; 5888         hs_pid_setpoint[hs_position] = PID_HEAT_HL1;
8669  1d1b c663              	ldab	#99
8671  1d1d 200a              	bra	LC067
8672  1d1f                   L5104:
8673                         ; 5890       else if (hs_status2_flags_c[hs_position].b.hs_LL1_set_b == TRUE)         /* In LL1 now */
8675  1d1f b746              	tfr	d,y
8676  1d21 0fea01151008      	brclr	_hs_status2_flags_c,y,16,L1204
8677                         ; 5892         hs_pid_setpoint[hs_position] = PID_HEAT_LL1;
8679  1d27 c657              	ldab	#87
8680  1d29                   LC067:
8681  1d29 6bea006e          	stab	_hs_pid_setpoint,y
8683  1d2d 2006              	bra	L7004
8684  1d2f                   L1204:
8685                         ; 5896         hs_pid_setpoint[hs_position] = SET_OFF_REQUEST;
8687  1d2f b746              	tfr	d,y
8688  1d31 69ea006e          	clr	_hs_pid_setpoint,y
8689  1d35                   L7004:
8690                         ; 5876      for(hs_position = 0; hs_position < ALL_HEAT_SEATS; hs_position++)
8692  1d35 6280              	inc	OFST-1,s
8695  1d37 e680              	ldab	OFST-1,s
8696  1d39 c104              	cmpb	#4
8697  1d3b 25bd              	blo	L7773
8698                         ; 5900     setpoint_read = hs_pid_setpoint[hs_position];                             /* Current set point*/
8700  1d3d b796              	exg	b,y
8701  1d3f 180dea006e0068    	movb	_hs_pid_setpoint,y,_setpoint_read
8702                         ; 5903     pid_cal();
8704  1d46 4a1bc8c8          	call	f_pid_cal
8707  1d4a 2004              	bra	L5204
8708  1d4c                   L5773:
8709                         ; 5909     wait_time++;
8711  1d4c 18720000          	incw	L5573_wait_time
8712  1d50                   L5204:
8713                         ; 5911 }  
8716  1d50 1b81              	leas	1,s
8717  1d52 0a                	rtc	
9154                         	xdef	f_pid_heat_setting
9155                         	xdef	f_pid_cal
9156                         	xdef	f_hsLF_SendRearSeat_HWStatus
9157                         	xdef	f_hsLF_Process_Switch_Requests_ECU
9158                         	xdef	f_hsLF_Process_Switch_Requests_Bus
9159                         	xdef	f_hsLF_Process_DiagIO_LEDStatus
9160                         	xdef	f_hsLF_Process_DiagIO_RC_Outputs
9161                         	xdef	f_hsLF_Catch_Rear_Switch_Requests
9162                         	xdef	f_hsLF_Catch_Signals
9163                         	xdef	f_hsLF_RemoteStart_Control
9164                         	xdef	f_hsLF_ConvertDegToAD
9165                         	xdef	f_hsLF_CopyDiagnoseParameter
9166                         	xdef	f_hsLF_GetPWM_State
9167                         	xdef	f_hsLF_Vsense_AD_Conversion
9168                         	xdef	f_hsLF_Isense_AD_Conversion
9169                         	xdef	f_hsLF_Zsense_AD_Readings
9170                         	xdef	f_hsLF_Vsense_AD_Readings
9171                         	xdef	f_hsLF_Isense_AD_Readings
9172                         	xdef	f_hsLF_2SecMonitorTime_FaultDetection
9173                         	xdef	f_hsLF_Timer
9174                         	xdef	f_hsLF_GetHeatedSeatCanStatus
9175                         	xdef	f_hsLF_SendHeatedSeatCanStatus
9176                         	xdef	f_hsLF_2SecLEDDisplay
9177                         	xdef	f_hsLF_HeatDiagnostics
9178                         	switch	.bss
9179  0018                   _hs_vehicle_line_offset_c:
9180  0018 00                	ds.b	1
9181                         	xdef	_hs_vehicle_line_offset_c
9182  0019                   _hs_temp_set_c:
9183  0019 00                	ds.b	1
9184                         	xdef	_hs_temp_set_c
9185  001a                   _hs_AD_parameters:
9186  001a 000000            	ds.b	3
9187                         	xdef	_hs_AD_parameters
9188  001d                   _hs_monitor_loop_counter_w:
9189  001d 0000000000000000  	ds.b	8
9190                         	xdef	_hs_monitor_loop_counter_w
9191  0025                   _hs_ntc_fault_counter_c:
9192  0025 00                	ds.b	1
9193                         	xdef	_hs_ntc_fault_counter_c
9194  0026                   _hs_seat_fault_counter_c:
9195  0026 00                	ds.b	1
9196                         	xdef	_hs_seat_fault_counter_c
9197  0027                   _hs_diag_prog_data_c:
9198  0027 0000000000        	ds.b	5
9199                         	xdef	_hs_diag_prog_data_c
9200  002c                   _hs_bounce_counter_c:
9201  002c 0000              	ds.b	2
9202                         	xdef	_hs_bounce_counter_c
9203  002e                   _hs_rear_demature_counter_c:
9204  002e 0000              	ds.b	2
9205                         	xdef	_hs_rear_demature_counter_c
9206  0030                   _hs_rear_mature_counter_w:
9207  0030 00000000          	ds.b	4
9208                         	xdef	_hs_rear_mature_counter_w
9209  0034                   _hs_rear_demature_time_c:
9210  0034 00                	ds.b	1
9211                         	xdef	_hs_rear_demature_time_c
9212  0035                   _hs_rear_mature_time_w:
9213  0035 0000              	ds.b	2
9214                         	xdef	_hs_rear_mature_time_w
9215  0037                   _hs_monitor_prelim_fault_counter_c:
9216  0037 00000000          	ds.b	4
9217                         	xdef	_hs_monitor_prelim_fault_counter_c
9218  003b                   _hs_temp_seat_stat_c:
9219  003b 00000000          	ds.b	4
9220                         	xdef	_hs_temp_seat_stat_c
9221  003f                   _hs_temp_seat_req_c:
9222  003f 00000000          	ds.b	4
9223                         	xdef	_hs_temp_seat_req_c
9224  0043                   _hs_allowed_NTC_c:
9225  0043 00000000          	ds.b	4
9226                         	xdef	_hs_allowed_NTC_c
9227  0047                   _hs_wait_time_check_NTC_c:
9228  0047 00000000          	ds.b	4
9229                         	xdef	_hs_wait_time_check_NTC_c
9230  004b                   _hs_allowed_heat_time_c:
9231  004b 00000000          	ds.b	4
9232                         	xdef	_hs_allowed_heat_time_c
9233  004f                   _hs_temp_heat_pwm_c:
9234  004f 00000000          	ds.b	4
9235                         	xdef	_hs_temp_heat_pwm_c
9236  0053                   _hs_switch_request_to_fet_c:
9237  0053 00000000          	ds.b	4
9238                         	xdef	_hs_switch_request_to_fet_c
9239  0057                   _hs_IO_seat_stat_c:
9240  0057 00000000          	ds.b	4
9241                         	xdef	_hs_IO_seat_stat_c
9242  005b                   _hs_IO_seat_req_c:
9243  005b 00000000          	ds.b	4
9244                         	xdef	_hs_IO_seat_req_c
9245  005f                   _hs_switch_request_c:
9246  005f 00000000          	ds.b	4
9247                         	xdef	_hs_switch_request_c
9248  0063                   _hs_rear_seat_led_status_c:
9249  0063 00000000          	ds.b	4
9250                         	xdef	_hs_rear_seat_led_status_c
9251  0067                   _hsLF_diag_pid_flag:
9252  0067 00                	ds.b	1
9253                         	xdef	_hsLF_diag_pid_flag
9254  0068                   _setpoint_read:
9255  0068 00                	ds.b	1
9256                         	xdef	_setpoint_read
9257  0069                   _actual:
9258  0069 00                	ds.b	1
9259                         	xdef	_actual
9260  006a                   _SWAPPED_NTC_AD_READING:
9261  006a 00000000          	ds.b	4
9262                         	xdef	_SWAPPED_NTC_AD_READING
9263  006e                   _hs_pid_setpoint:
9264  006e 00000000          	ds.b	4
9265                         	xdef	_hs_pid_setpoint
9266                         	xref	f_Pwm_GetOutputState
9267                         	xref	_DioConfigData
9268                         	xref	_Dio_PortRead_Ptr
9269                         	xref	_Dio_PortWrite_Ptr
9270                         	xref	f_EE_DirectWrite
9271                         	xref	f_EE_BlockRead
9272                         	xref	f_EE_BlockWrite
9273                         	xref	_int_flt_byte2
9274                         	xref	f_StaggerF_Manager
9275                         	xref	_vs_shrtTo_batt_mntr_c
9276                         	xref	_ADF_Hs_PrtlOpen_Z_Cnvrsn
9277                         	xref	_ADF_Nrmlz_Isense
9278                         	xref	_ADF_Nrmlz_Vsense
9279                         	xref	_ADF_AtoD_Cnvrsn
9280                         	xref	_ADF_Str_UnFlt_Rslt
9281                         	xref	_ADF_Str_UnFlt_Mux_Rslt
9282                         	xref	_ADF_Mux_Filter
9283                         	xref	_ADF_Filter
9284                         	xref	_AD_Filter_set
9285                         	xref	_AD_mux_Filter
9286                         	xref	_AD_Vref_seatH_w
9287                         	xref	_AD_Iref_seatH_c
9288                         	xref	f_TempF_CanTemp_to_NtcAdStd
9289                         	xref	f_TempF_Get_NTC_Rdg
9290                         	xref	_temperature_HS_NTC_status
9291                         	xref	f_FMemLibF_GetOdometer
9292                         	xref	f_FMemLibF_ReportDtc
9293                         	xref	_d_AllDtc_a
9294                         	xref	_relay_control2_c
9295                         	xref	_relay_control_c
9296                         	xref	_relay_fault_status_c
9297                         	xref	_relay_status_c
9298                         	xdef	f_hsF_RearSeat_TurnOFF
9299                         	xdef	f_hsF_GetActv_OutputNumber
9300                         	xdef	f_hsLF_ClearHeatSequence
9301                         	xdef	f_hsLF_UpdateHeatParameters
9302                         	xdef	f_hsF_WriteData_to_ShadowEEPROM
9303                         	xdef	f_hsF_CheckDiagnoseParameter
9304                         	xdef	f_hsF_AD_Readings
9305                         	xdef	f_hsF_RearSwitchStuckDTC
9306                         	xdef	f_hsF_NTCDTC
9307                         	xdef	f_hsF_HeatDTC
9308                         	xdef	f_hsLF_ControlFET
9309                         	xdef	f_hsF_HeatRequestToFet
9310                         	xdef	f_hsF_FrontHeatSeatStatus
9311                         	xdef	f_hsF_MonitorHeatTimes
9312                         	xdef	f_hsF_HeatManager
9313                         	xdef	f_hsF_Init
9314  0072                   _hs_NTC_FailSafe_prms:
9315  0072 0000000000        	ds.b	5
9316                         	xdef	_hs_NTC_FailSafe_prms
9317  0077                   _hs_cal_prms:
9318  0077 0000000000000000  	ds.b	84
9319                         	xdef	_hs_cal_prms
9320  00cb                   _hs_flt_dtc_c:
9321  00cb 00000000000000    	ds.b	7
9322                         	xdef	_hs_flt_dtc_c
9323  00d2                   _hs_rear_U12S_stat_c:
9324  00d2 00                	ds.b	1
9325                         	xdef	_hs_rear_U12S_stat_c
9326  00d3                   _hs_remote_start_cfg_c:
9327  00d3 00                	ds.b	1
9328                         	xdef	_hs_remote_start_cfg_c
9329  00d4                   _hs_autosar_pwm_w:
9330  00d4 0000000000000000  	ds.b	8
9331                         	xdef	_hs_autosar_pwm_w
9332  00dc                   _hs_time_counter_w:
9333  00dc 0000000000000000  	ds.b	8
9334                         	xdef	_hs_time_counter_w
9335  00e4                   _NTC_AD_Readings_c:
9336  00e4 00000000          	ds.b	4
9337                         	xdef	_NTC_AD_Readings_c
9338  00e8                   _CSWM_config_c:
9339  00e8 00                	ds.b	1
9340                         	xdef	_CSWM_config_c
9341  00e9                   _hs_Zsense_c:
9342  00e9 00000000          	ds.b	4
9343                         	xdef	_hs_Zsense_c
9344  00ed                   _hs_Isense_c:
9345  00ed 00000000          	ds.b	4
9346                         	xdef	_hs_Isense_c
9347  00f1                   _hs_Vsense_c:
9348  00f1 00000000          	ds.b	4
9349                         	xdef	_hs_Vsense_c
9350  00f5                   _hs_pwm_state_c:
9351  00f5 00000000          	ds.b	4
9352                         	xdef	_hs_pwm_state_c
9353  00f9                   _hs_monitor_2sec_led_counter_c:
9354  00f9 00000000          	ds.b	4
9355                         	xdef	_hs_monitor_2sec_led_counter_c
9356  00fd                   _hs_rem_seat_req_c:
9357  00fd 00000000          	ds.b	4
9358                         	xdef	_hs_rem_seat_req_c
9359  0101                   _hs_output_status_c:
9360  0101 00000000          	ds.b	4
9361                         	xdef	_hs_output_status_c
9362  0105                   _hs_allowed_heat_pwm_c:
9363  0105 00000000          	ds.b	4
9364                         	xdef	_hs_allowed_heat_pwm_c
9365  0109                   _hs_rear_swpsd_stat_c:
9366  0109 0000              	ds.b	2
9367                         	xdef	_hs_rear_swpsd_stat_c
9368  010b                   _hs_rear_swth_stat_c:
9369  010b 0000              	ds.b	2
9370                         	xdef	_hs_rear_swth_stat_c
9371  010d                   _hs_seat_config_c:
9372  010d 00                	ds.b	1
9373                         	xdef	_hs_seat_config_c
9374  010e                   _hs_vehicle_line_c:
9375  010e 00                	ds.b	1
9376                         	xdef	_hs_vehicle_line_c
9377  010f                   _hs_rear_seat_flags_c:
9378  010f 0000              	ds.b	2
9379                         	xdef	_hs_rear_seat_flags_c
9380  0111                   _hs_status3_flags_c:
9381  0111 00000000          	ds.b	4
9382                         	xdef	_hs_status3_flags_c
9383  0115                   _hs_status2_flags_c:
9384  0115 00000000          	ds.b	4
9385                         	xdef	_hs_status2_flags_c
9386  0119                   _hs_status_flags_c:
9387  0119 00000000          	ds.b	4
9388                         	xdef	_hs_status_flags_c
9389  011d                   _hs_control_flags_c:
9390  011d 00000000          	ds.b	4
9391                         	xdef	_hs_control_flags_c
9392  0121                   _main_internal_fault_w:
9393  0121 0000              	ds.b	2
9394                         	xdef	_main_internal_fault_w
9395                         	xref	_mainF_Calc_PWM
9396                         	xref	_main_flag_c
9397                         	xref	_main_SwReq_Rear_c
9398                         	xref	_main_state_c
9399                         	xref	_main_CSWM_Status_c
9400                         	xref	_canio_RX_IgnRun_RemSt_c
9401                         	xref	_canio_LHD_RHD_c
9402                         	xref	_canio_RX_IGN_STATUS_c
9403                         	xref	_canio_hvac_heated_seat_request_c
9404                         	xref	_ru_eep_PROXI_Cfg
9405                         	xref	_diag_io_vs_lock_flags_c
9406                         	xref	_diag_io_hs_lock_flags_c
9407                         	xref	_diag_io_lock3_flags_c
9408                         	xref	_diag_io_lock2_flags_c
9409                         	xref	_diag_io_hs_state_c
9410                         	xref	_diag_io_hs_rq_c
9411                         	xref	_diag_rc_lock_flags_c
9412                         	xref	f_IlPutTxFR_HS_STATSts
9413                         	xref	f_IlPutTxFL_HS_STATSts
9414                         	xref	_CSWM_A1
9415                         	xref	f_VStdResumeAllInterrupts
9416                         	xref	f_VStdSuspendAllInterrupts
9437                         	xref	c_lmul
9438                         	end
