   1                         ; C Compiler for S12XE [COSMIC Software].
   2                         ; Generator V4.7.8 - 27 Jul 2007
   3                         ; Optimizer V4.6.13 - 27 Jul 2007
   4                         .const:	section	.data
   5  0000                   L3_Pwm_MapPwmIndex:
   6  0000 01                	dc.b	1
   7  0001 02                	dc.b	2
   8  0002 03                	dc.b	3
   9  0003                   L5_Pwm_MapEctIndex:
  10  0003 03                	dc.b	3
  11  0004 04                	dc.b	4
  12  0005 05                	dc.b	5
  13  0006 06                	dc.b	6
  14  0007 07                	dc.b	7
  15                         	switch	.data
  16  0000                   L71_Pwm_On_Off_Current_Stat:
  17  0000 00                	dc.b	0
  18  0001                   L12_Pwm_Base_Period_Reg:
  19  0001 0000              	dc.w	__PWMPER01
  20  0003                   L32_Pwm_Base_Duty_Reg:
  21  0003 0000              	dc.w	__PWMDTY01
  22  0005                   L52_Pwm_Base_Cntr_Reg:
  23  0005 0000              	dc.w	__PWMCNT01
  24                         	switch	.const
  25  0008                   L72_Pwm_BaseAddr_TC:
  26  0008 0000              	dc.w	__TC0
  27  000a                   L33_Pwm_ECT_IDLE:
  28  000a 00                	dc.b	0
  29                         	switch	.data
  30  0007                   _Pwm_Calculated_POLARITY_1:
  31  0007 00                	dc.b	0
  32  0008                   _Pwm_Calculated_POLARITY_2:
  33  0008 00                	dc.b	0
 310                         ; 329 void Pwm_Init(const Pwm_ConfigType *ConfigPtr)
 310                         ; 330 {
 311                         	switch	.text
 312  0000                   _Pwm_Init:
 314  0000 3b                	pshd	
 315  0001 1b98              	leas	-8,s
 316       00000008          OFST:	set	8
 319                         ; 337     Pwm_CfgPtr = (Pwm_ConfigType *)ConfigPtr;
 321                         ; 341     PWMCTL |= ConfigPtr->PWM_PWMHW.POWER_MODE;
 323  0003 b746              	tfr	d,y
 324  0005 7d0001            	sty	L13_Pwm_CfgPtr
 325  0008 e640              	ldab	0,y
 326  000a fa0000            	orab	__PWMCTL
 327  000d 7b0000            	stab	__PWMCTL
 328                         ; 344     PWMPRCLK_PCKA = (ConfigPtr->PWM_PWMHW.PRESCALE_B_A & 0x07);
 330  0010 ed88              	ldy	OFST+0,s
 331  0012 e645              	ldab	5,y
 332  0014 c407              	andb	#7
 333  0016 37                	pshb	
 334  0017 f60000            	ldab	__PWMPRCLK
 335  001a c4f8              	andb	#248
 336  001c eab0              	orab	1,s+
 337  001e 7b0000            	stab	__PWMPRCLK
 338                         ; 345     PWMPRCLK_PCKB = (ConfigPtr->PWM_PWMHW.PRESCALE_B_A & 0x70)>>4;
 340  0021 ed88              	ldy	OFST+0,s
 341  0023 e645              	ldab	5,y
 342  0025 c470              	andb	#112
 343  0027 37                	pshb	
 344  0028 f60000            	ldab	__PWMPRCLK
 345  002b c48f              	andb	#143
 346  002d eab0              	orab	1,s+
 347  002f 7b0000            	stab	__PWMPRCLK
 348                         ; 347     PWMCLK |= ConfigPtr->PWM_PWMHW.CLOCK_SELECT;
 350  0032 ed88              	ldy	OFST+0,s
 351  0034 e646              	ldab	6,y
 352  0036 fa0000            	orab	__PWMCLK
 353  0039 7b0000            	stab	__PWMCLK
 354                         ; 351     PWMCAE |= ConfigPtr->PWM_PWMHW.CHANNEL_ALIGNED;
 356  003c e641              	ldab	1,y
 357  003e fa0000            	orab	__PWMCAE
 358  0041 7b0000            	stab	__PWMCAE
 359                         ; 355     PWMPOL |= ConfigPtr->PWM_PWMHW.POLARITY;
 361  0044 e642              	ldab	2,y
 362  0046 fa0000            	orab	__PWMPOL
 363  0049 7b0000            	stab	__PWMPOL
 364                         ; 357     for(index=0;index<PWM_PWMHW_CONFIGURED_CHANNELS;index++)
 366  004c c7                	clrb	
 367  004d 6b82              	stab	OFST-6,s
 368  004f                   L771:
 369                         ; 362         actual_channel=Pwm_MapPwmIndex[index];
 371  004f 87                	clra	
 372  0050 b746              	tfr	d,y
 373  0052 180aea000087      	movb	L3_Pwm_MapPwmIndex,y,OFST-1,s
 374                         ; 365             (*(Pwm_Base_Period_Reg+actual_channel))=(uint8)
 374                         ; 366                          ConfigPtr->PWM_PWMHW.CHANNEL[index].PERIOD_DEFAULT;
 376  0058 ed88              	ldy	OFST+0,s
 377  005a 59                	lsld	
 378  005b 59                	lsld	
 379  005c 19ee              	leay	d,y
 380  005e fe0001            	ldx	L12_Pwm_Base_Period_Reg
 381  0061 e687              	ldab	OFST-1,s
 382  0063 87                	clra	
 383  0064 180a48e6          	movb	8,y,d,x
 384                         ; 368             if(ConfigPtr->PWM_PWMHW.CHANNEL[index].DUTYCYCLE_DEFAULT <
 384                         ; 369                                                          PWM_HUNDRED_PERCENT)
 386  0068 ed88              	ldy	OFST+0,s
 387  006a e682              	ldab	OFST-6,s
 388  006c 59                	lsld	
 389  006d 59                	lsld	
 390  006e 19ee              	leay	d,y
 391  0070 ec49              	ldd	9,y
 392  0072 8c8000            	cpd	#-32768
 393  0075 2434              	bhs	L502
 394                         ; 371                  DutyCycle =ConfigPtr->PWM_PWMHW.CHANNEL[index].\
 394                         ; 372                             DUTYCYCLE_DEFAULT;
 396  0077 ed88              	ldy	OFST+0,s
 397  0079 e682              	ldab	OFST-6,s
 398  007b 87                	clra	
 399  007c 59                	lsld	
 400  007d 59                	lsld	
 401  007e 19ee              	leay	d,y
 402  0080 18024985          	movw	9,y,OFST-3,s
 403  0084 1887              	clrx	
 404  0086 6e83              	stx	OFST-5,s
 405                         ; 373                  DutyCycle *=(uint32)(*(Pwm_Base_Period_Reg+actual_channel));
 407  0088 fd0001            	ldy	L12_Pwm_Base_Period_Reg
 408  008b e687              	ldab	OFST-1,s
 409  008d 87                	clra	
 410  008e e6ee              	ldab	d,y
 411  0090 1983              	leay	OFST-5,s
 412  0092 160000            	jsr	c_lgmul
 414                         ; 374                  (*(Pwm_Base_Duty_Reg+actual_channel))= (uint8)
 414                         ; 375                             ((DutyCycle<<1)>>16);
 416  0095 fd0003            	ldy	L32_Pwm_Base_Duty_Reg
 417  0098 e687              	ldab	OFST-1,s
 418  009a 19ed              	leay	b,y
 419  009c ec85              	ldd	OFST-3,s
 420  009e ee83              	ldx	OFST-5,s
 421  00a0 59                	lsld	
 422  00a1 1845              	rolx	
 423  00a3 b754              	tfr	x,d
 424  00a5 1887              	clrx	
 425  00a7 6b40              	stab	0,y
 427  00a9 200a              	bra	L702
 428  00ab                   L502:
 429                         ; 379                 (*(Pwm_Base_Duty_Reg+actual_channel))=
 429                         ; 380                                 (*(Pwm_Base_Period_Reg+actual_channel));
 431  00ab e687              	ldab	OFST-1,s
 432  00ad 87                	clra	
 433  00ae fd0003            	ldy	L32_Pwm_Base_Duty_Reg
 434  00b1 180ae6ee          	movb	d,x,d,y
 435  00b5                   L702:
 436                         ; 357     for(index=0;index<PWM_PWMHW_CONFIGURED_CHANNELS;index++)
 438  00b5 6282              	inc	OFST-6,s
 441  00b7 e682              	ldab	OFST-6,s
 442  00b9 c103              	cmpb	#3
 443  00bb 2592              	blo	L771
 444                         ; 384     PWME |= PWM_CHANNEL_USED_PWM;
 446  00bd 1c00000e          	bset	__PWME,14
 447                         ; 387     DisableAllInterrupts();
 450  00c1 1410              	sei	
 452                         ; 389     Pwm_Confg_Count =  ConfigPtr - Pwm_Config;
 455  00c3 ec88              	ldd	OFST+0,s
 456  00c5 830000            	subd	#_Pwm_Config
 457  00c8 ce0036            	ldx	#54
 458  00cb 1815              	idivs	
 459  00cd b754              	tfr	x,d
 460  00cf 7b0000            	stab	L53_Pwm_Confg_Count
 461                         ; 391     PTT  = ((~PWM_CHANNEL_USED_ECT)&Port_PTT_Level) | Pwm_ECT_IDLE[Pwm_Confg_Count]; 
 463  00d2 b796              	exg	b,y
 464  00d4 f60000            	ldab	_Port_PTT_Level
 465  00d7 c407              	andb	#7
 466  00d9 eaea000a          	orab	L33_Pwm_ECT_IDLE,y
 467  00dd 7b0000            	stab	__PTT
 468                         ; 392     for(index=0;index<PWM_ECTHW_CONFIGURED_CHANNELS;index++) 
 470  00e0 c7                	clrb	
 471  00e1 6b82              	stab	OFST-6,s
 472  00e3                   L112:
 473                         ; 394         Pwm_Ect_Period[index]=
 473                         ; 395                             ConfigPtr->PWM_ECTHW.CHANNEL[index].PERIOD_DEFAULT;
 475  00e3 87                	clra	
 476  00e4 b746              	tfr	d,y
 477  00e6 1858              	lsly	
 478  00e8 ee88              	ldx	OFST+0,s
 479  00ea 59                	lsld	
 480  00eb 59                	lsld	
 481  00ec 1ae6              	leax	d,x
 482  00ee 1802e022ea0017    	movw	34,x,L11_Pwm_Ect_Period,y
 483                         ; 397         if(ConfigPtr->PWM_ECTHW.CHANNEL[index].DUTYCYCLE_DEFAULT < 
 483                         ; 398                                                      PWM_HUNDRED_PERCENT)
 485  00f5 ed88              	ldy	OFST+0,s
 486  00f7 19ee              	leay	d,y
 487  00f9 ece824            	ldd	36,y
 488  00fc 8c8000            	cpd	#-32768
 489  00ff 2428              	bhs	L712
 490                         ; 400             DutyCycle =ConfigPtr->PWM_ECTHW.CHANNEL[index].DUTYCYCLE_DEFAULT;
 492  0101 6c85              	std	OFST-3,s
 493  0103 1887              	clrx	
 494  0105 6e83              	stx	OFST-5,s
 495                         ; 401             DutyCycle *=(uint32)Pwm_Ect_Period[index];
 497  0107 e682              	ldab	OFST-6,s
 498  0109 87                	clra	
 499  010a 59                	lsld	
 500  010b b746              	tfr	d,y
 501  010d ecea0017          	ldd	L11_Pwm_Ect_Period,y
 502  0111 1983              	leay	OFST-5,s
 503  0113 160000            	jsr	c_lgmul
 505                         ; 402             Pwm_Ect_On_Time[index]=(uint16)((DutyCycle<<1)>>16);
 507  0116 e682              	ldab	OFST-6,s
 508  0118 87                	clra	
 509  0119 59                	lsld	
 510  011a b746              	tfr	d,y
 511  011c ec85              	ldd	OFST-3,s
 512  011e ee83              	ldx	OFST-5,s
 513  0120 59                	lsld	
 514  0121 1845              	rolx	
 515  0123 b754              	tfr	x,d
 516  0125 1887              	clrx	
 517                         
 519  0127 200a              	bra	L122
 520  0129                   L712:
 521                         ; 406             Pwm_Ect_On_Time[index]=Pwm_Ect_Period[index];
 523  0129 e682              	ldab	OFST-6,s
 524  012b 87                	clra	
 525  012c 59                	lsld	
 526  012d b746              	tfr	d,y
 527  012f ecea0017          	ldd	L11_Pwm_Ect_Period,y
 528  0133                   L122:
 529  0133 6cea000d          	std	L31_Pwm_Ect_On_Time,y
 530                         ; 409         Pwm_Ect_Off_Time[index]=Pwm_Ect_Period[index]-Pwm_Ect_On_Time[index];
 532  0137 e682              	ldab	OFST-6,s
 533  0139 87                	clra	
 534  013a b746              	tfr	d,y
 535  013c 1858              	lsly	
 536  013e 59                	lsld	
 537  013f b745              	tfr	d,x
 538  0141 1802e2000d80      	movw	L31_Pwm_Ect_On_Time,x,OFST-8,s
 539  0147 ece20017          	ldd	L11_Pwm_Ect_Period,x
 540  014b a380              	subd	OFST-8,s
 541  014d 6cea0003          	std	L51_Pwm_Ect_Off_Time,y
 542                         ; 411         (*(Pwm_BaseAddr_TC+index))=(ECT_TCNT+(uint16)Pwm_Ect_On_Time[index]);
 544  0151 ece2000d          	ldd	L31_Pwm_Ect_On_Time,x
 545  0155 f30000            	addd	__TCNT
 546  0158 6cea0000          	std	__TC0,y
 547                         ; 413         Pwm_Ena_Notif_Ect[index]= MCAL_CLEAR;
 549  015c e682              	ldab	OFST-6,s
 550  015e b796              	exg	b,y
 551  0160 69ea0021          	clr	L7_Pwm_Ena_Notif_Ect,y
 552                         ; 392     for(index=0;index<PWM_ECTHW_CONFIGURED_CHANNELS;index++) 
 554  0164 6282              	inc	OFST-6,s
 557  0166 e682              	ldab	OFST-6,s
 558  0168 c105              	cmpb	#5
 559  016a 1825ff75          	blo	L112
 560                         ; 417     Pwm_Calculated_POLARITY_2.all = Pwm_CfgPtr->PWM_ECTHW.POLARITY_2;
 562  016e cd0008            	ldy	#_Pwm_Calculated_POLARITY_2
 563  0171 fe0001            	ldx	L13_Pwm_CfgPtr
 564  0174 180ae01440        	movb	20,x,0,y
 565                         ; 418     Pwm_Calculated_POLARITY_1.all = Pwm_CfgPtr->PWM_ECTHW.POLARITY_1;
 567  0179 cd0007            	ldy	#_Pwm_Calculated_POLARITY_1
 568  017c 180ae01340        	movb	19,x,0,y
 569                         ; 423     ECT_TCTL2 |= ConfigPtr->PWM_ECTHW.POLARITY_2;
 571  0181 ed88              	ldy	OFST+0,s
 572  0183 e6e814            	ldab	20,y
 573  0186 fa0000            	orab	__TCTL2
 574  0189 7b0000            	stab	__TCTL2
 575                         ; 424     ECT_TCTL1 |= ConfigPtr->PWM_ECTHW.POLARITY_1;
 577  018c e6e813            	ldab	19,y
 578  018f fa0000            	orab	__TCTL1
 579  0192 7b0000            	stab	__TCTL1
 580                         ; 427     ECT_TIOS |= PWM_CHANNEL_USED_ECT;
 582  0195 1c0000f8          	bset	__TIOS,248
 583                         ; 429     ECT_TIE |= PWM_CHANNEL_USED_ECT;
 585  0199 1c0000f8          	bset	__TIE,248
 586                         ; 431     EnableAllInterrupts();
 589  019d 10ef              	cli	
 591                         ; 433 }
 595  019f 1b8a              	leas	10,s
 596  01a1 3d                	rts	
 672                         ; 452 void Pwm_SetDutyCycle(Pwm_ChannelType ChannelNumber,uint16 DutyCycle)
 672                         ; 453 {
 673                         	switch	.text
 674  01a2                   f_Pwm_SetDutyCycle:
 676  01a2 3b                	pshd	
 677  01a3 1b95              	leas	-11,s
 678       0000000b          OFST:	set	11
 681                         ; 455     uint8 ect_logical=0;
 683  01a5 87                	clra	
 684  01a6 6a89              	staa	OFST-2,s
 685                         ; 456     volatile uint8 Check_Var=0;
 687  01a8 6a8a              	staa	OFST-1,s
 688                         ; 458     if(ChannelNumber>=PWM_PWMHW_CONFIGURED_CHANNELS)
 690  01aa c103              	cmpb	#3
 691  01ac 250c              	blo	L152
 692                         ; 460         ect_logical=ChannelNumber-PWM_PWMHW_CONFIGURED_CHANNELS;
 694  01ae c003              	subb	#3
 695  01b0 6b89              	stab	OFST-2,s
 696                         ; 461         actual_channel= Pwm_MapEctIndex[ect_logical];
 698  01b2 b746              	tfr	d,y
 699  01b4 e6ea0003          	ldab	L5_Pwm_MapEctIndex,y
 701  01b8 2008              	bra	L352
 702  01ba                   L152:
 703                         ; 465         actual_channel=Pwm_MapPwmIndex[ChannelNumber];
 705  01ba e68c              	ldab	OFST+1,s
 706  01bc b746              	tfr	d,y
 707  01be e6ea0000          	ldab	L3_Pwm_MapPwmIndex,y
 708  01c2                   L352:
 709  01c2 6b88              	stab	OFST-3,s
 710                         ; 468     if(ChannelNumber>=PWM_PWMHW_CONFIGURED_CHANNELS)
 712  01c4 e68c              	ldab	OFST+1,s
 713  01c6 c103              	cmpb	#3
 714  01c8 ecf010            	ldd	OFST+5,s
 715  01cb 18250263          	blo	L552
 716                         ; 471         if(DutyCycle > 0x8000)
 718  01cf 8c8000            	cpd	#-32768
 719  01d2 2306              	bls	L752
 720                         ; 473             DutyCycle = 0x8000;
 722  01d4 cc8000            	ldd	#-32768
 723  01d7 6cf010            	std	OFST+5,s
 724  01da                   L752:
 725                         ; 477         if(((DutyCycle > 0)&&(DutyCycle < 0x8000))||((DutyCycle == 0)&&\
 725                         ; 478         (0 != Pwm_Ect_On_Time[ect_logical])) 
 725                         ; 479         ||((DutyCycle == 0x8000)&&(0 != Pwm_Ect_Off_Time[ect_logical]))) 
 727  01da edf010            	ldy	OFST+5,s
 728  01dd 2705              	beq	L562
 730  01df 8c8000            	cpd	#-32768
 731  01e2 2524              	blo	L362
 732  01e4                   L562:
 734  01e4 ecf010            	ldd	OFST+5,s
 735  01e7 260e              	bne	L172
 737  01e9 e689              	ldab	OFST-2,s
 738  01eb 59                	lsld	
 739  01ec b746              	tfr	d,y
 740  01ee ecea000d          	ldd	L31_Pwm_Ect_On_Time,y
 741  01f2 2614              	bne	L362
 742  01f4 ecf010            	ldd	OFST+5,s
 743  01f7                   L172:
 745  01f7 8c8000            	cpd	#-32768
 746  01fa 2610              	bne	L162
 748  01fc e689              	ldab	OFST-2,s
 749  01fe 87                	clra	
 750  01ff 59                	lsld	
 751  0200 b746              	tfr	d,y
 752  0202 ecea0003          	ldd	L51_Pwm_Ect_Off_Time,y
 753  0206 2704              	beq	L162
 754  0208                   L362:
 755                         ; 481             Check_Var = ON;
 757  0208 c601              	ldab	#1
 758  020a 6b8a              	stab	OFST-1,s
 759  020c                   L162:
 760                         ; 491         if(DutyCycle == 0)
 762  020c ecf010            	ldd	OFST+5,s
 763  020f 182600c0          	bne	L372
 764                         ; 493             if(actual_channel<4)
 766  0213 e688              	ldab	OFST-3,s
 767  0215 c104              	cmpb	#4
 768  0217 2459              	bhs	L572
 769                         ; 495                 ECT_TCTL2 = (ECT_TCTL2 & ~(1u<<(actual_channel*2))|
 769                         ; 496                 (2u<<(actual_channel*2)))|(~(Pwm_CfgPtr->PWM_ECTHW.POLARITY_2)&
 769                         ; 497                 (1u<<(actual_channel*2))|(2u<<(actual_channel*2)));
 771  0219 58                	lslb	
 772  021a 6b87              	stab	OFST-4,s
 773  021c c602              	ldab	#2
 774  021e a687              	ldaa	OFST-4,s
 775  0220 2704              	beq	L01
 776  0222                   L21:
 777  0222 58                	lslb	
 778  0223 0430fc            	dbne	a,L21
 779  0226                   L01:
 780  0226 6b86              	stab	OFST-5,s
 781  0228 e688              	ldab	OFST-3,s
 782  022a 58                	lslb	
 783  022b 6b85              	stab	OFST-6,s
 784  022d c601              	ldab	#1
 785  022f a685              	ldaa	OFST-6,s
 786  0231 2704              	beq	L41
 787  0233                   L61:
 788  0233 58                	lslb	
 789  0234 0430fc            	dbne	a,L61
 790  0237                   L41:
 791  0237 6b84              	stab	OFST-7,s
 792  0239 fd0001            	ldy	L13_Pwm_CfgPtr
 793  023c e6e814            	ldab	20,y
 794  023f 51                	comb	
 795  0240 e484              	andb	OFST-7,s
 796  0242 ea86              	orab	OFST-5,s
 797  0244 6b83              	stab	OFST-8,s
 798  0246 e688              	ldab	OFST-3,s
 799  0248 58                	lslb	
 800  0249 6b82              	stab	OFST-9,s
 801  024b c602              	ldab	#2
 802  024d a682              	ldaa	OFST-9,s
 803  024f 2704              	beq	L02
 804  0251                   L22:
 805  0251 58                	lslb	
 806  0252 0430fc            	dbne	a,L22
 807  0255                   L02:
 808  0255 6b81              	stab	OFST-10,s
 809  0257 e688              	ldab	OFST-3,s
 810  0259 58                	lslb	
 811  025a 6b80              	stab	OFST-11,s
 812  025c c601              	ldab	#1
 813  025e a680              	ldaa	OFST-11,s
 814  0260 2704              	beq	L42
 815  0262                   L62:
 816  0262 58                	lslb	
 817  0263 0430fc            	dbne	a,L62
 818  0266                   L42:
 819  0266 51                	comb	
 820  0267 f40000            	andb	__TCTL2
 821  026a ea81              	orab	OFST-10,s
 822  026c ea83              	orab	OFST-8,s
 824  026e 182000d0          	bra	LC002
 825  0272                   L572:
 826                         ; 501                 ECT_TCTL1 = (ECT_TCTL1 & ~(1u<<((actual_channel-4)*2))|
 826                         ; 502                 (2u<<((actual_channel-4)*2)))|
 826                         ; 503                 (~(Pwm_CfgPtr->PWM_ECTHW.POLARITY_1)&
 826                         ; 504                 (1u<<((actual_channel-4)*2))|(2u<<((actual_channel-4)*2)));
 828  0272 c004              	subb	#4
 829  0274 58                	lslb	
 830  0275 6b87              	stab	OFST-4,s
 831  0277 c602              	ldab	#2
 832  0279 a687              	ldaa	OFST-4,s
 833  027b 2704              	beq	L03
 834  027d                   L23:
 835  027d 58                	lslb	
 836  027e 0430fc            	dbne	a,L23
 837  0281                   L03:
 838  0281 6b86              	stab	OFST-5,s
 839  0283 e688              	ldab	OFST-3,s
 840  0285 c004              	subb	#4
 841  0287 58                	lslb	
 842  0288 6b85              	stab	OFST-6,s
 843  028a c601              	ldab	#1
 844  028c a685              	ldaa	OFST-6,s
 845  028e 2704              	beq	L43
 846  0290                   L63:
 847  0290 58                	lslb	
 848  0291 0430fc            	dbne	a,L63
 849  0294                   L43:
 850  0294 6b84              	stab	OFST-7,s
 851  0296 fd0001            	ldy	L13_Pwm_CfgPtr
 852  0299 e6e813            	ldab	19,y
 853  029c 51                	comb	
 854  029d e484              	andb	OFST-7,s
 855  029f ea86              	orab	OFST-5,s
 856  02a1 6b83              	stab	OFST-8,s
 857  02a3 e688              	ldab	OFST-3,s
 858  02a5 c004              	subb	#4
 859  02a7 58                	lslb	
 860  02a8 6b82              	stab	OFST-9,s
 861  02aa c602              	ldab	#2
 862  02ac a682              	ldaa	OFST-9,s
 863  02ae 2704              	beq	L04
 864  02b0                   L24:
 865  02b0 58                	lslb	
 866  02b1 0430fc            	dbne	a,L24
 867  02b4                   L04:
 868  02b4 6b81              	stab	OFST-10,s
 869  02b6 e688              	ldab	OFST-3,s
 870  02b8 c004              	subb	#4
 871  02ba 58                	lslb	
 872  02bb 6b80              	stab	OFST-11,s
 873  02bd c601              	ldab	#1
 874  02bf a680              	ldaa	OFST-11,s
 875  02c1 2704              	beq	L44
 876  02c3                   L64:
 877  02c3 58                	lslb	
 878  02c4 0430fc            	dbne	a,L64
 879  02c7                   L44:
 880  02c7 51                	comb	
 881  02c8 f40000            	andb	__TCTL1
 882  02cb ea81              	orab	OFST-10,s
 883  02cd ea83              	orab	OFST-8,s
 884  02cf 182000cc          	bra	LC001
 885  02d3                   L372:
 886                         ; 508         else if((DutyCycle == 0x8000)||((DutyCycle != 0)&&(0 == Pwm_Ect_On_Time[ect_logical])))
 888  02d3 8c8000            	cpd	#-32768
 889  02d6 2714              	beq	L503
 891  02d8 6c9e              	std	-2,s
 892  02da 182700c4          	beq	L103
 894  02de e689              	ldab	OFST-2,s
 895  02e0 87                	clra	
 896  02e1 59                	lsld	
 897  02e2 b746              	tfr	d,y
 898  02e4 ecea000d          	ldd	L31_Pwm_Ect_On_Time,y
 899  02e8 182600b6          	bne	L103
 900  02ec                   L503:
 901                         ; 510             if(actual_channel<4)
 903  02ec e688              	ldab	OFST-3,s
 904  02ee c104              	cmpb	#4
 905  02f0 2455              	bhs	L703
 906                         ; 512                 ECT_TCTL2 = (ECT_TCTL2 & ~(1u<<(actual_channel*2))|
 906                         ; 513                 (2u<<(actual_channel*2)))|((Pwm_CfgPtr->PWM_ECTHW.POLARITY_2)&
 906                         ; 514                 (1u<<(actual_channel*2))|(2u<<(actual_channel*2)));
 908  02f2 58                	lslb	
 909  02f3 6b87              	stab	OFST-4,s
 910  02f5 c602              	ldab	#2
 911  02f7 a687              	ldaa	OFST-4,s
 912  02f9 2704              	beq	L05
 913  02fb                   L25:
 914  02fb 58                	lslb	
 915  02fc 0430fc            	dbne	a,L25
 916  02ff                   L05:
 917  02ff 6b86              	stab	OFST-5,s
 918  0301 e688              	ldab	OFST-3,s
 919  0303 58                	lslb	
 920  0304 6b85              	stab	OFST-6,s
 921  0306 c601              	ldab	#1
 922  0308 a685              	ldaa	OFST-6,s
 923  030a 2704              	beq	L45
 924  030c                   L65:
 925  030c 58                	lslb	
 926  030d 0430fc            	dbne	a,L65
 927  0310                   L45:
 928  0310 fd0001            	ldy	L13_Pwm_CfgPtr
 929  0313 e4e814            	andb	20,y
 930  0316 ea86              	orab	OFST-5,s
 931  0318 6b84              	stab	OFST-7,s
 932  031a e688              	ldab	OFST-3,s
 933  031c 58                	lslb	
 934  031d 6b83              	stab	OFST-8,s
 935  031f c602              	ldab	#2
 936  0321 a683              	ldaa	OFST-8,s
 937  0323 2704              	beq	L06
 938  0325                   L26:
 939  0325 58                	lslb	
 940  0326 0430fc            	dbne	a,L26
 941  0329                   L06:
 942  0329 6b82              	stab	OFST-9,s
 943  032b e688              	ldab	OFST-3,s
 944  032d 58                	lslb	
 945  032e 6b81              	stab	OFST-10,s
 946  0330 c601              	ldab	#1
 947  0332 a681              	ldaa	OFST-10,s
 948  0334 2704              	beq	L46
 949  0336                   L66:
 950  0336 58                	lslb	
 951  0337 0430fc            	dbne	a,L66
 952  033a                   L46:
 953  033a 51                	comb	
 954  033b f40000            	andb	__TCTL2
 955  033e ea82              	orab	OFST-9,s
 956  0340 ea84              	orab	OFST-7,s
 957  0342                   LC002:
 958  0342 7b0000            	stab	__TCTL2
 960  0345 205b              	bra	L103
 961  0347                   L703:
 962                         ; 518                 ECT_TCTL1 = (ECT_TCTL1 & ~(1u<<((actual_channel-4)*2))|
 962                         ; 519                 (2u<<((actual_channel-4)*2)))|
 962                         ; 520                 ((Pwm_CfgPtr->PWM_ECTHW.POLARITY_1)&
 962                         ; 521                 (1u<<((actual_channel-4)*2))|(2u<<((actual_channel-4)*2)));
 964  0347 c004              	subb	#4
 965  0349 58                	lslb	
 966  034a 6b87              	stab	OFST-4,s
 967  034c c602              	ldab	#2
 968  034e a687              	ldaa	OFST-4,s
 969  0350 2704              	beq	L07
 970  0352                   L27:
 971  0352 58                	lslb	
 972  0353 0430fc            	dbne	a,L27
 973  0356                   L07:
 974  0356 6b86              	stab	OFST-5,s
 975  0358 e688              	ldab	OFST-3,s
 976  035a c004              	subb	#4
 977  035c 58                	lslb	
 978  035d 6b85              	stab	OFST-6,s
 979  035f c601              	ldab	#1
 980  0361 a685              	ldaa	OFST-6,s
 981  0363 2704              	beq	L47
 982  0365                   L67:
 983  0365 58                	lslb	
 984  0366 0430fc            	dbne	a,L67
 985  0369                   L47:
 986  0369 fd0001            	ldy	L13_Pwm_CfgPtr
 987  036c e4e813            	andb	19,y
 988  036f ea86              	orab	OFST-5,s
 989  0371 6b84              	stab	OFST-7,s
 990  0373 e688              	ldab	OFST-3,s
 991  0375 c004              	subb	#4
 992  0377 58                	lslb	
 993  0378 6b83              	stab	OFST-8,s
 994  037a c602              	ldab	#2
 995  037c a683              	ldaa	OFST-8,s
 996  037e 2704              	beq	L001
 997  0380                   L201:
 998  0380 58                	lslb	
 999  0381 0430fc            	dbne	a,L201
1000  0384                   L001:
1001  0384 6b82              	stab	OFST-9,s
1002  0386 e688              	ldab	OFST-3,s
1003  0388 c004              	subb	#4
1004  038a 58                	lslb	
1005  038b 6b81              	stab	OFST-10,s
1006  038d c601              	ldab	#1
1007  038f a681              	ldaa	OFST-10,s
1008  0391 2704              	beq	L401
1009  0393                   L601:
1010  0393 58                	lslb	
1011  0394 0430fc            	dbne	a,L601
1012  0397                   L401:
1013  0397 51                	comb	
1014  0398 f40000            	andb	__TCTL1
1015  039b ea82              	orab	OFST-9,s
1016  039d ea84              	orab	OFST-7,s
1017  039f                   LC001:
1018  039f 7b0000            	stab	__TCTL1
1019  03a2                   L103:
1020                         ; 525         DisableAllInterrupts();       
1023  03a2 1410              	sei	
1025                         ; 528         if(((DutyCycle != 0)&&(0 == Pwm_Ect_On_Time[ect_logical])) 
1025                         ; 529         ||((DutyCycle != 0x8000)&&(0 == Pwm_Ect_Off_Time[ect_logical])))
1028  03a4 ecf010            	ldd	OFST+5,s
1029  03a7 270f              	beq	L713
1031  03a9 e689              	ldab	OFST-2,s
1032  03ab 87                	clra	
1033  03ac 59                	lsld	
1034  03ad b746              	tfr	d,y
1035  03af ecea000d          	ldd	L31_Pwm_Ect_On_Time,y
1036  03b3 2714              	beq	L513
1037  03b5 ecf010            	ldd	OFST+5,s
1038  03b8                   L713:
1040  03b8 8c8000            	cpd	#-32768
1041  03bb 2723              	beq	L313
1043  03bd e689              	ldab	OFST-2,s
1044  03bf 87                	clra	
1045  03c0 59                	lsld	
1046  03c1 b746              	tfr	d,y
1047  03c3 ecea0003          	ldd	L51_Pwm_Ect_Off_Time,y
1048  03c7 2617              	bne	L313
1049  03c9                   L513:
1050                         ; 531             (*(Pwm_BaseAddr_TC+actual_channel)) = 
1050                         ; 532                                       ECT_TCNT + Pwm_Ect_Period[ect_logical];
1052  03c9 e688              	ldab	OFST-3,s
1053  03cb 87                	clra	
1054  03cc b746              	tfr	d,y
1055  03ce 1858              	lsly	
1056  03d0 e689              	ldab	OFST-2,s
1057  03d2 59                	lsld	
1058  03d3 b745              	tfr	d,x
1059  03d5 ece20017          	ldd	L11_Pwm_Ect_Period,x
1060  03d9 f30000            	addd	__TCNT
1061  03dc 6cea0000          	std	__TC0,y
1062  03e0                   L313:
1063                         ; 537         Pwm_Ect_On_Time[ect_logical]=(uint16)
1063                         ; 538             ((((uint32)DutyCycle*(uint32)Pwm_Ect_Period[ect_logical])<<1)>>16);
1065  03e0 e689              	ldab	OFST-2,s
1066  03e2 87                	clra	
1067  03e3 b746              	tfr	d,y
1068  03e5 1858              	lsly	
1069  03e7 59                	lsld	
1070  03e8 b745              	tfr	d,x
1071  03ea eee20017          	ldx	L11_Pwm_Ect_Period,x
1072  03ee ecf010            	ldd	OFST+5,s
1073  03f1 b7d6              	exg	x,y
1074  03f3 13                	emul	
1075  03f4 b7d6              	exg	x,y
1076  03f6 59                	lsld	
1077  03f7 1845              	rolx	
1078                         
1079  03f9 6eea000d          	stx	L31_Pwm_Ect_On_Time,y
1080                         ; 539         Pwm_Ect_Off_Time[ect_logical]=(uint16)
1080                         ; 540                     (Pwm_Ect_Period[ect_logical]-Pwm_Ect_On_Time[ect_logical]);
1082  03fd e689              	ldab	OFST-2,s
1083  03ff 87                	clra	
1084  0400 b746              	tfr	d,y
1085  0402 1858              	lsly	
1086  0404 59                	lsld	
1087  0405 b745              	tfr	d,x
1088  0407 1802e2000d86      	movw	L31_Pwm_Ect_On_Time,x,OFST-5,s
1089  040d ece20017          	ldd	L11_Pwm_Ect_Period,x
1090  0411 a386              	subd	OFST-5,s
1091  0413 6cea0003          	std	L51_Pwm_Ect_Off_Time,y
1092                         ; 546         if(ON == Check_Var ) 
1094  0417 e68a              	ldab	OFST-1,s
1095  0419 042112            	dbne	b,L123
1096                         ; 549             ECT_TIE |= ((uint8)(1<<actual_channel));
1098  041c c601              	ldab	#1
1099  041e a688              	ldaa	OFST-3,s
1100  0420 2704              	beq	L011
1101  0422                   L211:
1102  0422 58                	lslb	
1103  0423 0430fc            	dbne	a,L211
1104  0426                   L011:
1105  0426 fa0000            	orab	__TIE
1106  0429 7b0000            	stab	__TIE
1107                         ; 550             Check_Var = OFF;
1109  042c 698a              	clr	OFST-1,s
1110  042e                   L123:
1111                         ; 552         EnableAllInterrupts();
1114  042e 10ef              	cli	
1118  0430 2034              	bra	L323
1119  0432                   L552:
1120                         ; 558             if(DutyCycle < PWM_HUNDRED_PERCENT)/* If DutyCycle < 100% */
1122  0432 8c8000            	cpd	#-32768
1123  0435 2422              	bhs	L523
1124                         ; 560                (*(Pwm_Base_Duty_Reg+actual_channel))=(uint8)
1124                         ; 561                ((((uint32)DutyCycle*(uint32)(*(Pwm_Base_Period_Reg+\
1124                         ; 562                actual_channel)))<<1)>>16);
1126  0437 fd0003            	ldy	L32_Pwm_Base_Duty_Reg
1127  043a e688              	ldab	OFST-3,s
1128  043c 87                	clra	
1129  043d 19ed              	leay	b,y
1130  043f fe0001            	ldx	L12_Pwm_Base_Period_Reg
1131  0442 e6e6              	ldab	d,x
1132  0444 b745              	tfr	d,x
1133  0446 ecf010            	ldd	OFST+5,s
1134  0449 b7d6              	exg	x,y
1135  044b 13                	emul	
1136  044c b7d6              	exg	x,y
1137  044e 59                	lsld	
1138  044f 1845              	rolx	
1139  0451 b754              	tfr	x,d
1140  0453 1887              	clrx	
1141  0455 6b40              	stab	0,y
1143  0457 200d              	bra	L323
1144  0459                   L523:
1145                         ; 566                 (*(Pwm_Base_Duty_Reg+actual_channel))=
1145                         ; 567                                     (*(Pwm_Base_Period_Reg+actual_channel));
1147  0459 e688              	ldab	OFST-3,s
1148  045b 87                	clra	
1149  045c fd0003            	ldy	L32_Pwm_Base_Duty_Reg
1150  045f fe0001            	ldx	L12_Pwm_Base_Period_Reg
1151  0462 180ae6ee          	movb	d,x,d,y
1152  0466                   L323:
1153                         ; 571 }
1156  0466 1b8d              	leas	13,s
1157  0468 0a                	rtc	
1220                         ; 586 void Pwm_SetOutputToIdle(Pwm_ChannelType ChannelNumber) 
1220                         ; 587 {       
1221                         	switch	.text
1222  0469                   f_Pwm_SetOutputToIdle:
1224  0469 3b                	pshd	
1225  046a 1b97              	leas	-9,s
1226       00000009          OFST:	set	9
1229                         ; 589     uint8 ect_logical=0;
1231  046c 87                	clra	
1232  046d 6a88              	staa	OFST-1,s
1233                         ; 592     if(ChannelNumber>=PWM_PWMHW_CONFIGURED_CHANNELS)
1235  046f c103              	cmpb	#3
1236  0471 250e              	blo	L153
1237                         ; 594         ect_logical=ChannelNumber-PWM_PWMHW_CONFIGURED_CHANNELS;
1239  0473 c003              	subb	#3
1240  0475 6b88              	stab	OFST-1,s
1241                         ; 595         actual_channel=
1241                         ; 596         Pwm_MapEctIndex[ChannelNumber-PWM_PWMHW_CONFIGURED_CHANNELS];
1243  0477 e68a              	ldab	OFST+1,s
1244  0479 b746              	tfr	d,y
1245  047b e6ea0000          	ldab	L5_Pwm_MapEctIndex-3,y
1247  047f 2008              	bra	L353
1248  0481                   L153:
1249                         ; 600         actual_channel=Pwm_MapPwmIndex[ChannelNumber];
1251  0481 e68a              	ldab	OFST+1,s
1252  0483 b746              	tfr	d,y
1253  0485 e6ea0000          	ldab	L3_Pwm_MapPwmIndex,y
1254  0489                   L353:
1255  0489 6b87              	stab	OFST-2,s
1256                         ; 604     if((ChannelNumber>=PWM_PWMHW_CONFIGURED_CHANNELS)&&
1256                         ; 605        (PWM_VARIABLE_PERIOD== 
1256                         ; 606               ((Pwm_CfgPtr->PWM_ECTHW.CHANNEL_CLASS>>actual_channel)&1)))
1258  048b e68a              	ldab	OFST+1,s
1259  048d c103              	cmpb	#3
1260  048f 18250119          	blo	L553
1262  0493 fd0001            	ldy	L13_Pwm_CfgPtr
1263  0496 e6e817            	ldab	23,y
1264  0499 b795              	exg	b,x
1265  049b e687              	ldab	OFST-2,s
1266  049d 2705              	beq	L611
1267  049f                   L021:
1268  049f 1847              	asrx	
1269  04a1 0431fb            	dbne	b,L021
1270  04a4                   L611:
1271  04a4 b754              	tfr	x,d
1272  04a6 c501              	bitb	#1
1273  04a8 18260100          	bne	L553
1274                         ; 608         if(actual_channel<4)
1276  04ac e687              	ldab	OFST-2,s
1277  04ae c104              	cmpb	#4
1278  04b0 2452              	bhs	L753
1279                         ; 610             ECT_TCTL2 = (ECT_TCTL2 & ~(1u<<(actual_channel*2u))|
1279                         ; 611             (2u<<(actual_channel*2u)))|((Pwm_CfgPtr->PWM_ECTHW.IDLE_STATE_2)&
1279                         ; 612             (1u<<(actual_channel*2u))|(2u<<(actual_channel*2u)));
1281  04b2 58                	lslb	
1282  04b3 6b86              	stab	OFST-3,s
1283  04b5 c602              	ldab	#2
1284  04b7 a686              	ldaa	OFST-3,s
1285  04b9 2704              	beq	L221
1286  04bb                   L421:
1287  04bb 58                	lslb	
1288  04bc 0430fc            	dbne	a,L421
1289  04bf                   L221:
1290  04bf 6b85              	stab	OFST-4,s
1291  04c1 e687              	ldab	OFST-2,s
1292  04c3 58                	lslb	
1293  04c4 6b84              	stab	OFST-5,s
1294  04c6 c601              	ldab	#1
1295  04c8 a684              	ldaa	OFST-5,s
1296  04ca 2704              	beq	L621
1297  04cc                   L031:
1298  04cc 58                	lslb	
1299  04cd 0430fc            	dbne	a,L031
1300  04d0                   L621:
1301  04d0 e4e816            	andb	22,y
1302  04d3 ea85              	orab	OFST-4,s
1303  04d5 6b83              	stab	OFST-6,s
1304  04d7 e687              	ldab	OFST-2,s
1305  04d9 58                	lslb	
1306  04da 6b82              	stab	OFST-7,s
1307  04dc c602              	ldab	#2
1308  04de a682              	ldaa	OFST-7,s
1309  04e0 2704              	beq	L231
1310  04e2                   L431:
1311  04e2 58                	lslb	
1312  04e3 0430fc            	dbne	a,L431
1313  04e6                   L231:
1314  04e6 6b81              	stab	OFST-8,s
1315  04e8 e687              	ldab	OFST-2,s
1316  04ea 58                	lslb	
1317  04eb 6b80              	stab	OFST-9,s
1318  04ed c601              	ldab	#1
1319  04ef a680              	ldaa	OFST-9,s
1320  04f1 2704              	beq	L631
1321  04f3                   L041:
1322  04f3 58                	lslb	
1323  04f4 0430fc            	dbne	a,L041
1324  04f7                   L631:
1325  04f7 51                	comb	
1326  04f8 f40000            	andb	__TCTL2
1327  04fb ea81              	orab	OFST-8,s
1328  04fd ea83              	orab	OFST-6,s
1329  04ff 7b0000            	stab	__TCTL2
1331  0502 205b              	bra	L163
1332  0504                   L753:
1333                         ; 616             ECT_TCTL1 = (ECT_TCTL1 & ~(1u<<((actual_channel-4)*2u))|
1333                         ; 617             (2u<<((actual_channel-4)*2u)))|((Pwm_CfgPtr->PWM_ECTHW.IDLE_STATE_1)&
1333                         ; 618             (1u<<((actual_channel-4)*2u))|(2u<<((actual_channel-4)*2u)));
1335  0504 c004              	subb	#4
1336  0506 58                	lslb	
1337  0507 6b86              	stab	OFST-3,s
1338  0509 c602              	ldab	#2
1339  050b a686              	ldaa	OFST-3,s
1340  050d 2704              	beq	L241
1341  050f                   L441:
1342  050f 58                	lslb	
1343  0510 0430fc            	dbne	a,L441
1344  0513                   L241:
1345  0513 6b85              	stab	OFST-4,s
1346  0515 e687              	ldab	OFST-2,s
1347  0517 c004              	subb	#4
1348  0519 58                	lslb	
1349  051a 6b84              	stab	OFST-5,s
1350  051c c601              	ldab	#1
1351  051e a684              	ldaa	OFST-5,s
1352  0520 2704              	beq	L641
1353  0522                   L051:
1354  0522 58                	lslb	
1355  0523 0430fc            	dbne	a,L051
1356  0526                   L641:
1357  0526 fd0001            	ldy	L13_Pwm_CfgPtr
1358  0529 e4e815            	andb	21,y
1359  052c ea85              	orab	OFST-4,s
1360  052e 6b83              	stab	OFST-6,s
1361  0530 e687              	ldab	OFST-2,s
1362  0532 c004              	subb	#4
1363  0534 58                	lslb	
1364  0535 6b82              	stab	OFST-7,s
1365  0537 c602              	ldab	#2
1366  0539 a682              	ldaa	OFST-7,s
1367  053b 2704              	beq	L251
1368  053d                   L451:
1369  053d 58                	lslb	
1370  053e 0430fc            	dbne	a,L451
1371  0541                   L251:
1372  0541 6b81              	stab	OFST-8,s
1373  0543 e687              	ldab	OFST-2,s
1374  0545 c004              	subb	#4
1375  0547 58                	lslb	
1376  0548 6b80              	stab	OFST-9,s
1377  054a c601              	ldab	#1
1378  054c a680              	ldaa	OFST-9,s
1379  054e 2704              	beq	L651
1380  0550                   L061:
1381  0550 58                	lslb	
1382  0551 0430fc            	dbne	a,L061
1383  0554                   L651:
1384  0554 51                	comb	
1385  0555 f40000            	andb	__TCTL1
1386  0558 ea81              	orab	OFST-8,s
1387  055a ea83              	orab	OFST-6,s
1388  055c 7b0000            	stab	__TCTL1
1389  055f                   L163:
1390                         ; 621         DisableAllInterrupts();
1393  055f 1410              	sei	
1395                         ; 622         Pwm_Ect_On_Time[ect_logical]=0;
1398  0561 e688              	ldab	OFST-1,s
1399  0563 87                	clra	
1400  0564 59                	lsld	
1401  0565 b746              	tfr	d,y
1402  0567 1869ea000d        	clrw	L31_Pwm_Ect_On_Time,y
1403                         ; 623         Pwm_Ect_Off_Time[ect_logical]=Pwm_Ect_Period[ect_logical];
1405  056c 1802ea0017ea0003  	movw	L11_Pwm_Ect_Period,y,L51_Pwm_Ect_Off_Time,y
1406                         ; 625         ECT_CFORC |=((uint8)(1u<<actual_channel));
1408  0574 c601              	ldab	#1
1409  0576 a687              	ldaa	OFST-2,s
1410  0578 2704              	beq	L261
1411  057a                   L461:
1412  057a 58                	lslb	
1413  057b 0430fc            	dbne	a,L461
1414  057e                   L261:
1415  057e fa0000            	orab	__CFORC
1416  0581 7b0000            	stab	__CFORC
1417                         ; 627         ECT_TIE &= (~(uint8)(1u<<actual_channel));
1419  0584 c601              	ldab	#1
1420  0586 a687              	ldaa	OFST-2,s
1421  0588 2704              	beq	L661
1422  058a                   L071:
1423  058a 58                	lslb	
1424  058b 0430fc            	dbne	a,L071
1425  058e                   L661:
1426  058e 51                	comb	
1427  058f f40000            	andb	__TIE
1428  0592 7b0000            	stab	__TIE
1429                         ; 629         ECT_TIOS &= (~(uint8)(1u<<actual_channel));
1431  0595 c601              	ldab	#1
1432  0597 a687              	ldaa	OFST-2,s
1433  0599 2704              	beq	L271
1434  059b                   L471:
1435  059b 58                	lslb	
1436  059c 0430fc            	dbne	a,L471
1437  059f                   L271:
1438  059f 51                	comb	
1439  05a0 f40000            	andb	__TIOS
1440  05a3 7b0000            	stab	__TIOS
1441                         ; 630         EnableAllInterrupts();
1444  05a6 10ef              	cli	
1448  05a8 18200085          	bra	L363
1449  05ac                   L553:
1450                         ; 634         if(PWM_VARIABLE_PERIOD==
1450                         ; 635                   ((Pwm_CfgPtr->PWM_PWMHW.CHANNEL_CLASS>>actual_channel)&1))
1452  05ac fd0001            	ldy	L13_Pwm_CfgPtr
1453  05af e644              	ldab	4,y
1454  05b1 b795              	exg	b,x
1455  05b3 e687              	ldab	OFST-2,s
1456  05b5 2705              	beq	L671
1457  05b7                   L002:
1458  05b7 1847              	asrx	
1459  05b9 0431fb            	dbne	b,L002
1460  05bc                   L671:
1461  05bc b754              	tfr	x,d
1462  05be c501              	bitb	#1
1463  05c0 266f              	bne	L363
1464                         ; 637             if(TRUE==((Pwm_CfgPtr->PWM_PWMHW.IDLE_STATE>>actual_channel)&1))
1466  05c2 e643              	ldab	3,y
1467  05c4 b795              	exg	b,x
1468  05c6 e687              	ldab	OFST-2,s
1469  05c8 2705              	beq	L202
1470  05ca                   L402:
1471  05ca 1847              	asrx	
1472  05cc 0431fb            	dbne	b,L402
1473  05cf                   L202:
1474  05cf b754              	tfr	x,d
1475  05d1 c401              	andb	#1
1476  05d3 87                	clra	
1477  05d4 8c0001            	cpd	#1
1478  05d7 2621              	bne	L763
1479                         ; 640                 *(Pwm_Base_Period_Reg+actual_channel)=1;
1481  05d9 fd0001            	ldy	L12_Pwm_Base_Period_Reg
1482  05dc e687              	ldab	OFST-2,s
1483  05de 19ed              	leay	b,y
1484  05e0 c601              	ldab	#1
1485  05e2 6b40              	stab	0,y
1486                         ; 641                 *(Pwm_Base_Duty_Reg+actual_channel)=
1486                         ; 642                             ((PWMPOL>>actual_channel)&1);
1488  05e4 fd0003            	ldy	L32_Pwm_Base_Duty_Reg
1489  05e7 e687              	ldab	OFST-2,s
1490  05e9 19ed              	leay	b,y
1491  05eb f60000            	ldab	__PWMPOL
1492  05ee a687              	ldaa	OFST-2,s
1493  05f0 2704              	beq	L602
1494  05f2                   L012:
1495  05f2 54                	lsrb	
1496  05f3 0430fc            	dbne	a,L012
1497  05f6                   L602:
1498  05f6 c401              	andb	#1
1500  05f8 202d              	bra	L173
1501  05fa                   L763:
1502                         ; 647                 *(Pwm_Base_Period_Reg+actual_channel)=1;
1504  05fa fd0001            	ldy	L12_Pwm_Base_Period_Reg
1505  05fd e687              	ldab	OFST-2,s
1506  05ff 19ed              	leay	b,y
1507  0601 c601              	ldab	#1
1508  0603 6b40              	stab	0,y
1509                         ; 648                 *(Pwm_Base_Duty_Reg+actual_channel)=
1509                         ; 649                                             (((PWMPOL>>actual_channel)&1)?0:1);
1511  0605 fd0003            	ldy	L32_Pwm_Base_Duty_Reg
1512  0608 e687              	ldab	OFST-2,s
1513  060a 19ed              	leay	b,y
1514  060c 35                	pshy	
1515  060d f60000            	ldab	__PWMPOL
1516  0610 b746              	tfr	d,y
1517  0612 e689              	ldab	OFST+0,s
1518  0614 2705              	beq	L412
1519  0616                   L612:
1520  0616 1857              	asry	
1521  0618 0431fb            	dbne	b,L612
1522  061b                   L412:
1523  061b b764              	tfr	y,d
1524  061d c501              	bitb	#1
1525  061f 2703              	beq	L212
1526  0621 c7                	clrb	
1527  0622 2002              	bra	L022
1528  0624                   L212:
1529  0624 c601              	ldab	#1
1530  0626                   L022:
1531  0626 31                	puly	
1532  0627                   L173:
1533  0627 6b40              	stab	0,y
1534                         ; 652             *((Pwm_Base_Cntr_Reg+actual_channel)) = MCAL_CLEAR;
1536  0629 fd0005            	ldy	L52_Pwm_Base_Cntr_Reg
1537  062c e687              	ldab	OFST-2,s
1538  062e 87                	clra	
1539  062f 6aee              	staa	d,y
1540  0631                   L363:
1541                         ; 656 }
1544  0631 1b8b              	leas	11,s
1545  0633 0a                	rtc	
1596                         ; 673 Pwm_OutputStateType Pwm_GetOutputState(Pwm_ChannelType ChannelNumber) 
1596                         ; 674 {                
1597                         	switch	.text
1598  0634                   f_Pwm_GetOutputState:
1600  0634 3b                	pshd	
1601  0635 3b                	pshd	
1602       00000002          OFST:	set	2
1605                         ; 675     uint8 output_state=PWM_LOW;
1607  0636 87                	clra	
1608  0637 6a81              	staa	OFST-1,s
1609                         ; 679     if(ChannelNumber>=PWM_PWMHW_CONFIGURED_CHANNELS)
1611  0639 c103              	cmpb	#3
1612  063b 2508              	blo	L314
1613                         ; 681         actual_channel=
1613                         ; 682         Pwm_MapEctIndex[ChannelNumber-PWM_PWMHW_CONFIGURED_CHANNELS];
1615  063d b746              	tfr	d,y
1616  063f e6ea0000          	ldab	L5_Pwm_MapEctIndex-3,y
1618  0643 2008              	bra	L514
1619  0645                   L314:
1620                         ; 686         actual_channel=Pwm_MapPwmIndex[ChannelNumber];
1622  0645 e683              	ldab	OFST+1,s
1623  0647 b746              	tfr	d,y
1624  0649 e6ea0000          	ldab	L3_Pwm_MapPwmIndex,y
1625  064d                   L514:
1626  064d 6b80              	stab	OFST-2,s
1627                         ; 689     if(ChannelNumber>=PWM_PWMHW_CONFIGURED_CHANNELS)
1629  064f e683              	ldab	OFST+1,s
1630  0651 c103              	cmpb	#3
1631  0653 250f              	blo	L714
1632                         ; 692         output_state=((PTIT>>(actual_channel))&1);
1634  0655 f60000            	ldab	__PTIT
1635  0658 a680              	ldaa	OFST-2,s
1636  065a 2704              	beq	L422
1637  065c                   L622:
1638  065c 54                	lsrb	
1639  065d 0430fc            	dbne	a,L622
1640  0660                   L422:
1641  0660 c401              	andb	#1
1643  0662 2054              	bra	LC003
1644  0664                   L714:
1645                         ; 699             if((*(Pwm_Base_Cntr_Reg+actual_channel))<
1645                         ; 700                (*(Pwm_Base_Duty_Reg+actual_channel))&&
1645                         ; 701                  (TRUE==((Pwm_CfgPtr->PWM_PWMHW.POLARITY>>actual_channel)&1)))
1647  0664 fd0003            	ldy	L32_Pwm_Base_Duty_Reg
1648  0667 e680              	ldab	OFST-2,s
1649  0669 87                	clra	
1650  066a 19ed              	leay	b,y
1651  066c fe0005            	ldx	L52_Pwm_Base_Cntr_Reg
1652  066f e6e6              	ldab	d,x
1653  0671 e140              	cmpb	0,y
1654  0673 241d              	bhs	L324
1656  0675 fd0001            	ldy	L13_Pwm_CfgPtr
1657  0678 e642              	ldab	2,y
1658  067a b745              	tfr	d,x
1659  067c e680              	ldab	OFST-2,s
1660  067e 2705              	beq	L032
1661  0680                   L232:
1662  0680 1847              	asrx	
1663  0682 0431fb            	dbne	b,L232
1664  0685                   L032:
1665  0685 b754              	tfr	x,d
1666  0687 c401              	andb	#1
1667  0689 87                	clra	
1668  068a 8c0001            	cpd	#1
1669                         ; 703                 output_state = PWM_HIGH;
1672  068d 2727              	beq	LC004
1673  068f fe0005            	ldx	L52_Pwm_Base_Cntr_Reg
1674  0692                   L324:
1675                         ; 705             else if((*(Pwm_Base_Cntr_Reg+actual_channel))>=
1675                         ; 706                (*(Pwm_Base_Duty_Reg+actual_channel))&&
1675                         ; 707                  (FALSE==((Pwm_CfgPtr->PWM_PWMHW.POLARITY>>actual_channel)&1)))
1677  0692 fd0003            	ldy	L32_Pwm_Base_Duty_Reg
1678  0695 e680              	ldab	OFST-2,s
1679  0697 87                	clra	
1680  0698 19ed              	leay	b,y
1681  069a e6e6              	ldab	d,x
1682  069c e140              	cmpb	0,y
1683  069e 251a              	blo	L124
1685  06a0 fd0001            	ldy	L13_Pwm_CfgPtr
1686  06a3 e642              	ldab	2,y
1687  06a5 b745              	tfr	d,x
1688  06a7 e680              	ldab	OFST-2,s
1689  06a9 2705              	beq	L432
1690  06ab                   L632:
1691  06ab 1847              	asrx	
1692  06ad 0431fb            	dbne	b,L632
1693  06b0                   L432:
1694  06b0 b754              	tfr	x,d
1695  06b2 c501              	bitb	#1
1696  06b4 2604              	bne	L124
1697                         ; 709                 output_state = PWM_HIGH;
1699  06b6                   LC004:
1700  06b6 c601              	ldab	#1
1701  06b8                   LC003:
1702  06b8 6b81              	stab	OFST-1,s
1703  06ba                   L124:
1704                         ; 713     return (output_state);
1706  06ba e681              	ldab	OFST-1,s
1709  06bc 1b84              	leas	4,s
1710  06be 0a                	rtc	
1743                         ; 730 void Pwm_DisableNotification(Pwm_ChannelType ChannelNumber) 
1743                         ; 731 {                
1744                         	switch	.text
1745  06bf                   f_Pwm_DisableNotification:
1749                         ; 733     Pwm_Ena_Notif_Ect[ChannelNumber-PWM_PWMHW_CONFIGURED_CHANNELS]= MCAL_CLEAR;
1751  06bf b796              	exg	b,y
1752  06c1 69ea001e          	clr	L7_Pwm_Ena_Notif_Ect-3,y
1753                         ; 735 }
1756  06c5 0a                	rtc	
1826                         ; 752 void Pwm_EnableNotification(Pwm_ChannelType ChannelNumber,
1826                         ; 753                                Pwm_EdgeNotificationType Notification)
1826                         ; 754 {
1827                         	switch	.text
1828  06c6                   f_Pwm_EnableNotification:
1830       fffffffe          OFST:	set	-2
1833                         ; 756     Pwm_Ena_Notif_Ect[ChannelNumber-PWM_PWMHW_CONFIGURED_CHANNELS]=
1833                         ; 757                                                            (uint8)Notification;
1835  06c6 b796              	exg	b,y
1836  06c8 180a84ea001e      	movb	OFST+6,s,L7_Pwm_Ena_Notif_Ect-3,y
1837                         ; 759 }
1840  06ce 0a                	rtc	
1918                         ; 779 void Pwm_GetVersionInfo(Std_VersionInfoType *versioninfo)
1918                         ; 780 {
1919                         	switch	.text
1920  06cf                   _Pwm_GetVersionInfo:
1922       00000000          OFST:	set	0
1925                         ; 782     if (NULL_PTR != versioninfo)
1927  06cf 6cae              	std	2,-s
1928  06d1 2715              	beq	L535
1929                         ; 785         versioninfo->moduleID=PWM_MODULE_ID;
1931  06d3 c679              	ldab	#121
1932  06d5 ed80              	ldy	OFST+0,s
1933  06d7 6b42              	stab	2,y
1934                         ; 787         versioninfo->vendorID=PWM_VENDOR_ID;
1936  06d9 cc0028            	ldd	#40
1937  06dc 6c40              	std	0,y
1938                         ; 789         versioninfo->sw_major_version=PWM_SW_MAJOR_VERSION_C;
1940  06de c603              	ldab	#3
1941  06e0 6b43              	stab	3,y
1942                         ; 790         versioninfo->sw_minor_version=PWM_SW_MINOR_VERSION_C;
1944  06e2 52                	incb	
1945  06e3 6b44              	stab	4,y
1946                         ; 791         versioninfo->sw_patch_version=PWM_SW_PATCH_VERSION_C;
1948  06e5 52                	incb	
1949  06e6 6b45              	stab	5,y
1950  06e8                   L535:
1951                         ; 794 }
1954  06e8 31                	puly	
1955  06e9 3d                	rts	
1988                         ; 814 void  Pwm_TIM_Channel_3_Isr(void)
1988                         ; 815 {
1989                         	switch	.text
1990  06ea                   _Pwm_TIM_Channel_3_Isr:
1994                         ; 817     ECT_TFLG1 =MCAL_BIT3_MASK;
1996  06ea c608              	ldab	#8
1997  06ec 7b0000            	stab	__TFLG1
1998                         ; 820     if(0==Pwm_Ect_Off_Time[PWM_ECT_CH_3]) /* 100% duty cycle */
2000  06ef fc0003            	ldd	L51_Pwm_Ect_Off_Time
2001  06f2 2606              	bne	L745
2002                         ; 823         Pwm_On_Off_Current_Stat.all |= 0x08;
2004  06f4 1c000008          	bset	L71_Pwm_On_Off_Current_Stat,8
2005                         ; 825         ECT_TIE_C3I = 0;
2008  06f8 2009              	bra	LC007
2009  06fa                   L745:
2010                         ; 827     else if(0==Pwm_Ect_On_Time[PWM_ECT_CH_3])/* 0% duty cycle */
2012  06fa fc000d            	ldd	L31_Pwm_Ect_On_Time
2013  06fd 260a              	bne	L355
2014                         ; 830         Pwm_On_Off_Current_Stat.all = (Pwm_On_Off_Current_Stat.all & 0xF7);
2016  06ff 1d000008          	bclr	L71_Pwm_On_Off_Current_Stat,8
2017                         ; 832         ECT_TIE_C3I = 0;
2019  0703                   LC007:
2020  0703 1d000008          	bclr	__TIE,8
2022  0707 203c              	bra	L652
2023  0709                   L355:
2024                         ; 837         if(Pwm_Calculated_POLARITY_2.bit.b6==ECT_TCTL2_OL3)
2026  0709 f60008            	ldab	_Pwm_Calculated_POLARITY_2
2027  070c f80000            	eorb	__TCTL2
2028  070f c540              	bitb	#64
2029  0711 261a              	bne	L755
2030                         ; 840             ECT_TC3 +=(uint16)Pwm_Ect_On_Time[PWM_ECT_CH_3];
2032  0713 fc0000            	ldd	__TC3
2033  0716 f3000d            	addd	L31_Pwm_Ect_On_Time
2034  0719 7c0000            	std	__TC3
2035                         ; 842             Pwm_On_Off_Current_Stat.all |= 0x08;
2037  071c 1c000008          	bset	L71_Pwm_On_Off_Current_Stat,8
2038                         ; 843             ECT_TCTL2_OL3= ~Pwm_Calculated_POLARITY_2.bit.b6;
2040  0720 1f00084006        	brclr	_Pwm_Calculated_POLARITY_2,64,L052
2041  0725                   LC006:
2042  0725 1d000040          	bclr	__TCTL2,64
2043  0729 201a              	bra	L652
2044  072b                   L052:
2046  072b 2014              	bra	L452
2047  072d                   L755:
2048                         ; 848             ECT_TC3 +=(uint16)Pwm_Ect_Off_Time[PWM_ECT_CH_3];
2050  072d fc0000            	ldd	__TC3
2051  0730 f30003            	addd	L51_Pwm_Ect_Off_Time
2052  0733 7c0000            	std	__TC3
2053                         ; 850             Pwm_On_Off_Current_Stat.all = (Pwm_On_Off_Current_Stat.all & 0xF7);
2055  0736 1d000008          	bclr	L71_Pwm_On_Off_Current_Stat,8
2056                         ; 851             ECT_TCTL2_OL3= Pwm_Calculated_POLARITY_2.bit.b6;
2058  073a 1e00084002        	brset	_Pwm_Calculated_POLARITY_2,64,L452
2059  073f 20e4              	bra	LC006
2060  0741                   L452:
2061  0741 1c000040          	bset	__TCTL2,64
2062  0745                   L652:
2063                         ; 856     if(NULL_PTR != Pwm_CfgPtr->PWM_ECTHW.NOTIFICATION[PWM_ECT_CH_3])
2065  0745 fd0001            	ldy	L13_Pwm_CfgPtr
2066  0748 ece818            	ldd	24,y
2067  074b 2724              	beq	L365
2068                         ; 858         if((uint8)PWM_BOTH_EDGES == Pwm_Ena_Notif_Ect[PWM_ECT_CH_3])
2070  074d f60021            	ldab	L7_Pwm_Ena_Notif_Ect
2071  0750 c103              	cmpb	#3
2072                         ; 861             (Pwm_CfgPtr->PWM_ECTHW.NOTIFICATION[PWM_ECT_CH_3])();
2076  0752 2719              	beq	LC008
2077                         ; 863         else if((uint8)PWM_RISING_EDGE==Pwm_Ena_Notif_Ect[PWM_ECT_CH_3])
2079  0754 c101              	cmpb	#1
2080  0756 2607              	bne	L175
2081                         ; 866             if((TRUE==Pwm_On_Off_Current_Stat.bit.b3)&&
2081                         ; 867                (TRUE==Pwm_Calculated_POLARITY_2.bit.b6))
2083  0758 1f00000814        	brclr	L71_Pwm_On_Off_Current_Stat,8,L365
2085                         ; 870                 (Pwm_CfgPtr->PWM_ECTHW.NOTIFICATION[PWM_ECT_CH_3])();
2088  075d 2009              	bra	LC009
2089  075f                   L175:
2090                         ; 873         else if((uint8)PWM_FALLING_EDGE==Pwm_Ena_Notif_Ect[PWM_ECT_CH_3])
2092  075f c102              	cmpb	#2
2093  0761 260e              	bne	L365
2094                         ; 876             if((FALSE==Pwm_On_Off_Current_Stat.bit.b3)&&
2094                         ; 877                (TRUE==Pwm_Calculated_POLARITY_2.bit.b6))
2096  0763 1e00000809        	brset	L71_Pwm_On_Off_Current_Stat,8,L365
2098  0768                   LC009:
2099  0768 1f00084004        	brclr	_Pwm_Calculated_POLARITY_2,64,L365
2100                         ; 880                 (Pwm_CfgPtr->PWM_ECTHW.NOTIFICATION[PWM_ECT_CH_3])();
2102  076d                   LC008:
2103  076d 15eb0018          	jsr	[24,y]
2105  0771                   L365:
2106                         ; 884 }
2109  0771 3d                	rts	
2142                         ; 901 void  Pwm_TIM_Channel_4_Isr(void)
2142                         ; 902 {
2143                         	switch	.text
2144  0772                   _Pwm_TIM_Channel_4_Isr:
2148                         ; 904     ECT_TFLG1 =MCAL_BIT4_MASK;
2150  0772 c610              	ldab	#16
2151  0774 7b0000            	stab	__TFLG1
2152                         ; 906     if(0==Pwm_Ect_Off_Time[PWM_ECT_CH_4]) /* 100% duty cycle */
2154  0777 fc0005            	ldd	L51_Pwm_Ect_Off_Time+2
2155  077a 2606              	bne	L316
2156                         ; 909         Pwm_On_Off_Current_Stat.all |= 0x10;
2158  077c 1c000010          	bset	L71_Pwm_On_Off_Current_Stat,16
2159                         ; 911         ECT_TIE_C4I = 0;
2162  0780 2009              	bra	LC012
2163  0782                   L316:
2164                         ; 913     else if(0==Pwm_Ect_On_Time[PWM_ECT_CH_4])/* 0% duty cycle */
2166  0782 fc000f            	ldd	L31_Pwm_Ect_On_Time+2
2167  0785 260a              	bne	L716
2168                         ; 916         Pwm_On_Off_Current_Stat.all = (Pwm_On_Off_Current_Stat.all & 0xEF);
2170  0787 1d000010          	bclr	L71_Pwm_On_Off_Current_Stat,16
2171                         ; 918         ECT_TIE_C4I = 0;
2173  078b                   LC012:
2174  078b 1d000010          	bclr	__TIE,16
2176  078f 203c              	bra	L072
2177  0791                   L716:
2178                         ; 923         if(Pwm_Calculated_POLARITY_1.bit.b0==ECT_TCTL1_OL4)
2180  0791 f60007            	ldab	_Pwm_Calculated_POLARITY_1
2181  0794 f80000            	eorb	__TCTL1
2182  0797 c501              	bitb	#1
2183  0799 261a              	bne	L326
2184                         ; 926             ECT_TC4 +=(uint16)Pwm_Ect_On_Time[PWM_ECT_CH_4];
2186  079b fc0000            	ldd	__TC4
2187  079e f3000f            	addd	L31_Pwm_Ect_On_Time+2
2188  07a1 7c0000            	std	__TC4
2189                         ; 928             Pwm_On_Off_Current_Stat.all |= 0x10;
2191  07a4 1c000010          	bset	L71_Pwm_On_Off_Current_Stat,16
2192                         ; 929             ECT_TCTL1_OL4= ~Pwm_Calculated_POLARITY_1.bit.b0;
2194  07a8 1f00070106        	brclr	_Pwm_Calculated_POLARITY_1,1,L262
2195  07ad                   LC011:
2196  07ad 1d000001          	bclr	__TCTL1,1
2197  07b1 201a              	bra	L072
2198  07b3                   L262:
2200  07b3 2014              	bra	L662
2201  07b5                   L326:
2202                         ; 934             ECT_TC4 +=(uint16)Pwm_Ect_Off_Time[PWM_ECT_CH_4];
2204  07b5 fc0000            	ldd	__TC4
2205  07b8 f30005            	addd	L51_Pwm_Ect_Off_Time+2
2206  07bb 7c0000            	std	__TC4
2207                         ; 936             Pwm_On_Off_Current_Stat.all = (Pwm_On_Off_Current_Stat.all & 0xEF);
2209  07be 1d000010          	bclr	L71_Pwm_On_Off_Current_Stat,16
2210                         ; 937             ECT_TCTL1_OL4= Pwm_Calculated_POLARITY_1.bit.b0;
2212  07c2 1e00070102        	brset	_Pwm_Calculated_POLARITY_1,1,L662
2213  07c7 20e4              	bra	LC011
2214  07c9                   L662:
2215  07c9 1c000001          	bset	__TCTL1,1
2216  07cd                   L072:
2217                         ; 942     if(NULL_PTR != Pwm_CfgPtr->PWM_ECTHW.NOTIFICATION[PWM_ECT_CH_4])
2219  07cd fd0001            	ldy	L13_Pwm_CfgPtr
2220  07d0 ece81a            	ldd	26,y
2221  07d3 2724              	beq	L726
2222                         ; 944         if((uint8)PWM_BOTH_EDGES == Pwm_Ena_Notif_Ect[PWM_ECT_CH_4])
2224  07d5 f60022            	ldab	L7_Pwm_Ena_Notif_Ect+1
2225  07d8 c103              	cmpb	#3
2226                         ; 947             (Pwm_CfgPtr->PWM_ECTHW.NOTIFICATION[PWM_ECT_CH_4])();
2230  07da 2719              	beq	LC013
2231                         ; 949         else if((uint8)PWM_RISING_EDGE==Pwm_Ena_Notif_Ect[PWM_ECT_CH_4])
2233  07dc c101              	cmpb	#1
2234  07de 2607              	bne	L536
2235                         ; 952             if((TRUE==Pwm_On_Off_Current_Stat.bit.b4)&&
2235                         ; 953                (TRUE==Pwm_Calculated_POLARITY_1.bit.b0))
2237  07e0 1f00001014        	brclr	L71_Pwm_On_Off_Current_Stat,16,L726
2239                         ; 956                 (Pwm_CfgPtr->PWM_ECTHW.NOTIFICATION[PWM_ECT_CH_4])();
2242  07e5 2009              	bra	LC014
2243  07e7                   L536:
2244                         ; 959         else if((uint8)PWM_FALLING_EDGE==Pwm_Ena_Notif_Ect[PWM_ECT_CH_4])
2246  07e7 c102              	cmpb	#2
2247  07e9 260e              	bne	L726
2248                         ; 962             if((FALSE==Pwm_On_Off_Current_Stat.bit.b4)&&
2248                         ; 963                (TRUE==Pwm_Calculated_POLARITY_1.bit.b0))
2250  07eb 1e00001009        	brset	L71_Pwm_On_Off_Current_Stat,16,L726
2252  07f0                   LC014:
2253  07f0 1f00070104        	brclr	_Pwm_Calculated_POLARITY_1,1,L726
2254                         ; 966                 (Pwm_CfgPtr->PWM_ECTHW.NOTIFICATION[PWM_ECT_CH_4])();
2256  07f5                   LC013:
2257  07f5 15eb001a          	jsr	[26,y]
2259  07f9                   L726:
2260                         ; 970 }
2263  07f9 3d                	rts	
2296                         ; 987 void  Pwm_TIM_Channel_5_Isr(void)
2296                         ; 988 {
2297                         	switch	.text
2298  07fa                   _Pwm_TIM_Channel_5_Isr:
2302                         ; 990     ECT_TFLG1 =MCAL_BIT5_MASK;
2304  07fa c620              	ldab	#32
2305  07fc 7b0000            	stab	__TFLG1
2306                         ; 992     if(0==Pwm_Ect_Off_Time[PWM_ECT_CH_5]) /* 100% duty cycle */
2308  07ff fc0007            	ldd	L51_Pwm_Ect_Off_Time+4
2309  0802 2606              	bne	L756
2310                         ; 995         Pwm_On_Off_Current_Stat.all |= 0x20;
2312  0804 1c000020          	bset	L71_Pwm_On_Off_Current_Stat,32
2313                         ; 997         ECT_TIE_C5I = 0;
2316  0808 2009              	bra	LC017
2317  080a                   L756:
2318                         ; 999     else if(0==Pwm_Ect_On_Time[PWM_ECT_CH_5])/* 0% duty cycle */
2320  080a fc0011            	ldd	L31_Pwm_Ect_On_Time+4
2321  080d 260a              	bne	L366
2322                         ; 1002         Pwm_On_Off_Current_Stat.all = (Pwm_On_Off_Current_Stat.all & 0xDF);
2324  080f 1d000020          	bclr	L71_Pwm_On_Off_Current_Stat,32
2325                         ; 1004         ECT_TIE_C5I = 0;
2327  0813                   LC017:
2328  0813 1d000020          	bclr	__TIE,32
2330  0817 203c              	bra	L203
2331  0819                   L366:
2332                         ; 1009         if(Pwm_Calculated_POLARITY_1.bit.b2==ECT_TCTL1_OL5)
2334  0819 f60007            	ldab	_Pwm_Calculated_POLARITY_1
2335  081c f80000            	eorb	__TCTL1
2336  081f c504              	bitb	#4
2337  0821 261a              	bne	L766
2338                         ; 1012             ECT_TC5 +=(uint16)Pwm_Ect_On_Time[PWM_ECT_CH_5];
2340  0823 fc0000            	ldd	__TC5
2341  0826 f30011            	addd	L31_Pwm_Ect_On_Time+4
2342  0829 7c0000            	std	__TC5
2343                         ; 1014             Pwm_On_Off_Current_Stat.all |= 0x20;
2345  082c 1c000020          	bset	L71_Pwm_On_Off_Current_Stat,32
2346                         ; 1015             ECT_TCTL1_OL5= ~Pwm_Calculated_POLARITY_1.bit.b2;
2348  0830 1f00070406        	brclr	_Pwm_Calculated_POLARITY_1,4,L472
2349  0835                   LC016:
2350  0835 1d000004          	bclr	__TCTL1,4
2351  0839 201a              	bra	L203
2352  083b                   L472:
2354  083b 2014              	bra	L003
2355  083d                   L766:
2356                         ; 1020             ECT_TC5 +=(uint16)Pwm_Ect_Off_Time[PWM_ECT_CH_5];
2358  083d fc0000            	ldd	__TC5
2359  0840 f30007            	addd	L51_Pwm_Ect_Off_Time+4
2360  0843 7c0000            	std	__TC5
2361                         ; 1022             Pwm_On_Off_Current_Stat.all = (Pwm_On_Off_Current_Stat.all & 0xDF);
2363  0846 1d000020          	bclr	L71_Pwm_On_Off_Current_Stat,32
2364                         ; 1023             ECT_TCTL1_OL5= Pwm_Calculated_POLARITY_1.bit.b2;
2366  084a 1e00070402        	brset	_Pwm_Calculated_POLARITY_1,4,L003
2367  084f 20e4              	bra	LC016
2368  0851                   L003:
2369  0851 1c000004          	bset	__TCTL1,4
2370  0855                   L203:
2371                         ; 1028     if(NULL_PTR != Pwm_CfgPtr->PWM_ECTHW.NOTIFICATION[PWM_ECT_CH_5])
2373  0855 fd0001            	ldy	L13_Pwm_CfgPtr
2374  0858 ece81c            	ldd	28,y
2375  085b 2724              	beq	L376
2376                         ; 1030         if((uint8)PWM_BOTH_EDGES == Pwm_Ena_Notif_Ect[PWM_ECT_CH_5])
2378  085d f60023            	ldab	L7_Pwm_Ena_Notif_Ect+2
2379  0860 c103              	cmpb	#3
2380                         ; 1033             (Pwm_CfgPtr->PWM_ECTHW.NOTIFICATION[PWM_ECT_CH_5])();
2384  0862 2719              	beq	LC018
2385                         ; 1035         else if((uint8)PWM_RISING_EDGE==Pwm_Ena_Notif_Ect[PWM_ECT_CH_5])
2387  0864 c101              	cmpb	#1
2388  0866 2607              	bne	L107
2389                         ; 1038             if((TRUE==Pwm_On_Off_Current_Stat.bit.b5)&&
2389                         ; 1039                (TRUE==Pwm_Calculated_POLARITY_1.bit.b2))
2391  0868 1f00002014        	brclr	L71_Pwm_On_Off_Current_Stat,32,L376
2393                         ; 1042                 (Pwm_CfgPtr->PWM_ECTHW.NOTIFICATION[PWM_ECT_CH_5])();
2396  086d 2009              	bra	LC019
2397  086f                   L107:
2398                         ; 1045         else if((uint8)PWM_FALLING_EDGE==Pwm_Ena_Notif_Ect[PWM_ECT_CH_5])
2400  086f c102              	cmpb	#2
2401  0871 260e              	bne	L376
2402                         ; 1048             if((FALSE==Pwm_On_Off_Current_Stat.bit.b5)&&
2402                         ; 1049                (TRUE==Pwm_Calculated_POLARITY_1.bit.b2))
2404  0873 1e00002009        	brset	L71_Pwm_On_Off_Current_Stat,32,L376
2406  0878                   LC019:
2407  0878 1f00070404        	brclr	_Pwm_Calculated_POLARITY_1,4,L376
2408                         ; 1052                 (Pwm_CfgPtr->PWM_ECTHW.NOTIFICATION[PWM_ECT_CH_5])();
2410  087d                   LC018:
2411  087d 15eb001c          	jsr	[28,y]
2413  0881                   L376:
2414                         ; 1056 }
2417  0881 3d                	rts	
2450                         ; 1073 void  Pwm_TIM_Channel_6_Isr(void)
2450                         ; 1074 {
2451                         	switch	.text
2452  0882                   _Pwm_TIM_Channel_6_Isr:
2456                         ; 1076     ECT_TFLG1 =MCAL_BIT6_MASK;
2458  0882 c640              	ldab	#64
2459  0884 7b0000            	stab	__TFLG1
2460                         ; 1078     if(0==Pwm_Ect_Off_Time[PWM_ECT_CH_6]) /* 100% duty cycle */
2462  0887 fc0009            	ldd	L51_Pwm_Ect_Off_Time+6
2463  088a 2606              	bne	L327
2464                         ; 1081         Pwm_On_Off_Current_Stat.all |= 0x40;
2466  088c 1c000040          	bset	L71_Pwm_On_Off_Current_Stat,64
2467                         ; 1083         ECT_TIE_C6I = 0;
2470  0890 2009              	bra	LC022
2471  0892                   L327:
2472                         ; 1085     else if(0==Pwm_Ect_On_Time[PWM_ECT_CH_6])/* 0% duty cycle */
2474  0892 fc0013            	ldd	L31_Pwm_Ect_On_Time+6
2475  0895 260a              	bne	L727
2476                         ; 1088         Pwm_On_Off_Current_Stat.all = (Pwm_On_Off_Current_Stat.all & 0xBF);
2478  0897 1d000040          	bclr	L71_Pwm_On_Off_Current_Stat,64
2479                         ; 1090         ECT_TIE_C6I = 0;
2481  089b                   LC022:
2482  089b 1d000040          	bclr	__TIE,64
2484  089f 203c              	bra	L413
2485  08a1                   L727:
2486                         ; 1095         if(Pwm_Calculated_POLARITY_1.bit.b4==ECT_TCTL1_OL6)
2488  08a1 f60007            	ldab	_Pwm_Calculated_POLARITY_1
2489  08a4 f80000            	eorb	__TCTL1
2490  08a7 c510              	bitb	#16
2491  08a9 261a              	bne	L337
2492                         ; 1098             ECT_TC6 +=(uint16)Pwm_Ect_On_Time[PWM_ECT_CH_6];
2494  08ab fc0000            	ldd	__TC6
2495  08ae f30013            	addd	L31_Pwm_Ect_On_Time+6
2496  08b1 7c0000            	std	__TC6
2497                         ; 1100             Pwm_On_Off_Current_Stat.all |= 0x40;
2499  08b4 1c000040          	bset	L71_Pwm_On_Off_Current_Stat,64
2500                         ; 1101             ECT_TCTL1_OL6= ~Pwm_Calculated_POLARITY_1.bit.b4;
2502  08b8 1f00071006        	brclr	_Pwm_Calculated_POLARITY_1,16,L603
2503  08bd                   LC021:
2504  08bd 1d000010          	bclr	__TCTL1,16
2505  08c1 201a              	bra	L413
2506  08c3                   L603:
2508  08c3 2014              	bra	L213
2509  08c5                   L337:
2510                         ; 1106             ECT_TC6 +=(uint16)Pwm_Ect_Off_Time[PWM_ECT_CH_6];
2512  08c5 fc0000            	ldd	__TC6
2513  08c8 f30009            	addd	L51_Pwm_Ect_Off_Time+6
2514  08cb 7c0000            	std	__TC6
2515                         ; 1108             Pwm_On_Off_Current_Stat.all = (Pwm_On_Off_Current_Stat.all & 0xBF);
2517  08ce 1d000040          	bclr	L71_Pwm_On_Off_Current_Stat,64
2518                         ; 1109             ECT_TCTL1_OL6= Pwm_Calculated_POLARITY_1.bit.b4;
2520  08d2 1e00071002        	brset	_Pwm_Calculated_POLARITY_1,16,L213
2521  08d7 20e4              	bra	LC021
2522  08d9                   L213:
2523  08d9 1c000010          	bset	__TCTL1,16
2524  08dd                   L413:
2525                         ; 1114     if(NULL_PTR != Pwm_CfgPtr->PWM_ECTHW.NOTIFICATION[PWM_ECT_CH_6])
2527  08dd fd0001            	ldy	L13_Pwm_CfgPtr
2528  08e0 ece81e            	ldd	30,y
2529  08e3 2724              	beq	L737
2530                         ; 1116         if((uint8)PWM_BOTH_EDGES == Pwm_Ena_Notif_Ect[PWM_ECT_CH_6])
2532  08e5 f60024            	ldab	L7_Pwm_Ena_Notif_Ect+3
2533  08e8 c103              	cmpb	#3
2534                         ; 1119             (Pwm_CfgPtr->PWM_ECTHW.NOTIFICATION[PWM_ECT_CH_6])();
2538  08ea 2719              	beq	LC023
2539                         ; 1121         else if((uint8)PWM_RISING_EDGE==Pwm_Ena_Notif_Ect[PWM_ECT_CH_6])
2541  08ec c101              	cmpb	#1
2542  08ee 2607              	bne	L547
2543                         ; 1124             if((TRUE==Pwm_On_Off_Current_Stat.bit.b6)&&
2543                         ; 1125                (TRUE==Pwm_Calculated_POLARITY_1.bit.b4))
2545  08f0 1f00004014        	brclr	L71_Pwm_On_Off_Current_Stat,64,L737
2547                         ; 1128                 (Pwm_CfgPtr->PWM_ECTHW.NOTIFICATION[PWM_ECT_CH_6])();
2550  08f5 2009              	bra	LC024
2551  08f7                   L547:
2552                         ; 1131         else if((uint8)PWM_FALLING_EDGE==Pwm_Ena_Notif_Ect[PWM_ECT_CH_6])
2554  08f7 c102              	cmpb	#2
2555  08f9 260e              	bne	L737
2556                         ; 1134             if((FALSE==Pwm_On_Off_Current_Stat.bit.b6)&&
2556                         ; 1135                (TRUE==Pwm_Calculated_POLARITY_1.bit.b4))
2558  08fb 1e00004009        	brset	L71_Pwm_On_Off_Current_Stat,64,L737
2560  0900                   LC024:
2561  0900 1f00071004        	brclr	_Pwm_Calculated_POLARITY_1,16,L737
2562                         ; 1138                 (Pwm_CfgPtr->PWM_ECTHW.NOTIFICATION[PWM_ECT_CH_6])();
2564  0905                   LC023:
2565  0905 15eb001e          	jsr	[30,y]
2567  0909                   L737:
2568                         ; 1142 }
2571  0909 3d                	rts	
2604                         ; 1159 void  Pwm_TIM_Channel_7_Isr(void)
2604                         ; 1160 {
2605                         	switch	.text
2606  090a                   _Pwm_TIM_Channel_7_Isr:
2610                         ; 1162     ECT_TFLG1 =MCAL_BIT7_MASK;
2612  090a c680              	ldab	#128
2613  090c 7b0000            	stab	__TFLG1
2614                         ; 1164     if(0==Pwm_Ect_Off_Time[PWM_ECT_CH_7]) /* 100% duty cycle */
2616  090f fc000b            	ldd	L51_Pwm_Ect_Off_Time+8
2617  0912 2606              	bne	L767
2618                         ; 1167         Pwm_On_Off_Current_Stat.all |= 0x80;
2620  0914 1c000080          	bset	L71_Pwm_On_Off_Current_Stat,128
2621                         ; 1169         ECT_TIE_C7I = 0;
2624  0918 2009              	bra	LC027
2625  091a                   L767:
2626                         ; 1171     else if(0==Pwm_Ect_On_Time[PWM_ECT_CH_7])/* 0% duty cycle */
2628  091a fc0015            	ldd	L31_Pwm_Ect_On_Time+8
2629  091d 260a              	bne	L377
2630                         ; 1174         Pwm_On_Off_Current_Stat.all = (Pwm_On_Off_Current_Stat.all & 0x7F);
2632  091f 1d000080          	bclr	L71_Pwm_On_Off_Current_Stat,128
2633                         ; 1176         ECT_TIE_C7I = 0;
2635  0923                   LC027:
2636  0923 1d000080          	bclr	__TIE,128
2638  0927 203c              	bra	L623
2639  0929                   L377:
2640                         ; 1181         if(Pwm_Calculated_POLARITY_1.bit.b6==ECT_TCTL1_OL7)
2642  0929 f60007            	ldab	_Pwm_Calculated_POLARITY_1
2643  092c f80000            	eorb	__TCTL1
2644  092f c540              	bitb	#64
2645  0931 261a              	bne	L777
2646                         ; 1184             ECT_TC7 +=(uint16)Pwm_Ect_On_Time[PWM_ECT_CH_7];
2648  0933 fc0000            	ldd	__TC7
2649  0936 f30015            	addd	L31_Pwm_Ect_On_Time+8
2650  0939 7c0000            	std	__TC7
2651                         ; 1186             Pwm_On_Off_Current_Stat.all |= 0x80;
2653  093c 1c000080          	bset	L71_Pwm_On_Off_Current_Stat,128
2654                         ; 1187             ECT_TCTL1_OL7= ~Pwm_Calculated_POLARITY_1.bit.b6;
2656  0940 1f00074006        	brclr	_Pwm_Calculated_POLARITY_1,64,L023
2657  0945                   LC026:
2658  0945 1d000040          	bclr	__TCTL1,64
2659  0949 201a              	bra	L623
2660  094b                   L023:
2662  094b 2014              	bra	L423
2663  094d                   L777:
2664                         ; 1192             ECT_TC7 +=(uint16)Pwm_Ect_Off_Time[PWM_ECT_CH_7];
2666  094d fc0000            	ldd	__TC7
2667  0950 f3000b            	addd	L51_Pwm_Ect_Off_Time+8
2668  0953 7c0000            	std	__TC7
2669                         ; 1194             Pwm_On_Off_Current_Stat.all = (Pwm_On_Off_Current_Stat.all & 0x7F);
2671  0956 1d000080          	bclr	L71_Pwm_On_Off_Current_Stat,128
2672                         ; 1195             ECT_TCTL1_OL7= Pwm_Calculated_POLARITY_1.bit.b6;
2674  095a 1e00074002        	brset	_Pwm_Calculated_POLARITY_1,64,L423
2675  095f 20e4              	bra	LC026
2676  0961                   L423:
2677  0961 1c000040          	bset	__TCTL1,64
2678  0965                   L623:
2679                         ; 1200         if(NULL_PTR != Pwm_CfgPtr->PWM_ECTHW.NOTIFICATION[PWM_ECT_CH_7])
2681  0965 fd0001            	ldy	L13_Pwm_CfgPtr
2682  0968 ece820            	ldd	32,y
2683  096b 2724              	beq	L3001
2684                         ; 1202         if((uint8)PWM_BOTH_EDGES == Pwm_Ena_Notif_Ect[PWM_ECT_CH_7])
2686  096d f60025            	ldab	L7_Pwm_Ena_Notif_Ect+4
2687  0970 c103              	cmpb	#3
2688                         ; 1205             (Pwm_CfgPtr->PWM_ECTHW.NOTIFICATION[PWM_ECT_CH_7])();
2692  0972 2719              	beq	LC028
2693                         ; 1207         else if((uint8)PWM_RISING_EDGE==Pwm_Ena_Notif_Ect[PWM_ECT_CH_7])
2695  0974 c101              	cmpb	#1
2696  0976 2607              	bne	L1101
2697                         ; 1210             if((TRUE==Pwm_On_Off_Current_Stat.bit.b7)&&
2697                         ; 1211                (TRUE==Pwm_Calculated_POLARITY_1.bit.b6))
2699  0978 1f00008014        	brclr	L71_Pwm_On_Off_Current_Stat,128,L3001
2701                         ; 1214                 (Pwm_CfgPtr->PWM_ECTHW.NOTIFICATION[PWM_ECT_CH_7])();
2704  097d 2009              	bra	LC029
2705  097f                   L1101:
2706                         ; 1217         else if((uint8)PWM_FALLING_EDGE==Pwm_Ena_Notif_Ect[PWM_ECT_CH_7])
2708  097f c102              	cmpb	#2
2709  0981 260e              	bne	L3001
2710                         ; 1220             if((FALSE==Pwm_On_Off_Current_Stat.bit.b7)&&
2710                         ; 1221                (TRUE==Pwm_Calculated_POLARITY_1.bit.b6))
2712  0983 1e00008009        	brset	L71_Pwm_On_Off_Current_Stat,128,L3001
2714  0988                   LC029:
2715  0988 1f00074004        	brclr	_Pwm_Calculated_POLARITY_1,64,L3001
2716                         ; 1224                 (Pwm_CfgPtr->PWM_ECTHW.NOTIFICATION[PWM_ECT_CH_7])();
2718  098d                   LC028:
2719  098d 15eb0020          	jsr	[32,y]
2721  0991                   L3001:
2722                         ; 1228 }
2725  0991 3d                	rts	
3107                         	xdef	_Pwm_Calculated_POLARITY_2
3108                         	xdef	_Pwm_Calculated_POLARITY_1
3109                         	switch	.bss
3110  0000                   L53_Pwm_Confg_Count:
3111  0000 00                	ds.b	1
3112  0001                   L13_Pwm_CfgPtr:
3113  0001 0000              	ds.b	2
3114  0003                   L51_Pwm_Ect_Off_Time:
3115  0003 0000000000000000  	ds.b	10
3116  000d                   L31_Pwm_Ect_On_Time:
3117  000d 0000000000000000  	ds.b	10
3118  0017                   L11_Pwm_Ect_Period:
3119  0017 0000000000000000  	ds.b	10
3120  0021                   L7_Pwm_Ena_Notif_Ect:
3121  0021 0000000000        	ds.b	5
3122                         	xref	_Port_PTT_Level
3123                         	xdef	_Pwm_TIM_Channel_7_Isr
3124                         	xdef	_Pwm_TIM_Channel_6_Isr
3125                         	xdef	_Pwm_TIM_Channel_5_Isr
3126                         	xdef	_Pwm_TIM_Channel_4_Isr
3127                         	xdef	_Pwm_TIM_Channel_3_Isr
3128                         	xdef	_Pwm_GetVersionInfo
3129                         	xdef	f_Pwm_EnableNotification
3130                         	xdef	f_Pwm_DisableNotification
3131                         	xdef	f_Pwm_GetOutputState
3132                         	xdef	f_Pwm_SetOutputToIdle
3133                         	xdef	f_Pwm_SetDutyCycle
3134                         	xdef	_Pwm_Init
3135                         	xref	_Pwm_Config
3136                         	xref	__PWMDTY01
3137                         	xref	__PWMPER01
3138                         	xref	__PWMCNT01
3139                         	xref	__PWMCTL
3140                         	xref	__PWMCAE
3141                         	xref	__PWMPRCLK
3142                         	xref	__PWMCLK
3143                         	xref	__PWMPOL
3144                         	xref	__PWME
3145                         	xref	__PTIT
3146                         	xref	__PTT
3147                         	xref	__TC7
3148                         	xref	__TC6
3149                         	xref	__TC5
3150                         	xref	__TC4
3151                         	xref	__TC3
3152                         	xref	__TC0
3153                         	xref	__TFLG1
3154                         	xref	__TIE
3155                         	xref	__TCTL2
3156                         	xref	__TCTL1
3157                         	xref	__TCNT
3158                         	xref	__CFORC
3159                         	xref	__TIOS
3180                         	xref	c_lgmul
3181                         	end
