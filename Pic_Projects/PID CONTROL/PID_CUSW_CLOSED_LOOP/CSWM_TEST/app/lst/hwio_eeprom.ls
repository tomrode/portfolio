   1                         ; C Compiler for S12XE [COSMIC Software].
   2                         ; Generator V4.7.8 - 27 Jul 2007
   3                         ; Optimizer V4.6.13 - 27 Jul 2007
  40                         ; 221 void EE_DRIVER_CALL EE_DriverPowerupInit(void)
  40                         ; 222 {
  41                         .ftext:	section	.text
  42  0000                   f_EE_DriverPowerupInit:
  46  0000                   L55:
  47                         ; 226     while (!Flash_IsCommandComplete());
  49  0000 1f000080fb        	brclr	__FSTAT,128,L55
  50                         ; 229     FCLKDIV = FLASH_CLOCK_DIV;
  52  0005 c607              	ldab	#7
  53  0007 7b0000            	stab	__FCLKDIV
  54                         ; 232     FERCNFG = 0;
  56  000a 790000            	clr	__FERCNFG
  57                         ; 235     FCNFG = IGNSF;
  59  000d c610              	ldab	#16
  60  000f 7b0000            	stab	__FCNFG
  61                         ; 287 }
  64  0012 0a                	rtc	
  90                         ; 300 void EE_DRIVER_CALL EE_Handler(void)
  90                         ; 301 {
  91                         	switch	.ftext
  92  0013                   f_EE_Handler:
  96                         ; 303     switch(eEECurrentState)
  98  0013 f6000c            	ldab	L13_eEECurrentState
 100  0016 040108            	dbeq	b,L16
 101  0019 c002              	subb	#2
 102  001b 2709              	beq	L56
 103  001d 04010b            	dbeq	b,L76
 105  0020 0a                	rtc	
 106  0021                   L16:
 107                         ; 305     case EE_WRITE_SEQUENCE_INITIATED:
 107                         ; 306         EE_WriteSequenceInitiated();
 109  0021 4a019696          	call	L31f_EE_WriteSequenceInitiated
 111                         ; 307         break;
 114  0025 0a                	rtc	
 115                         ; 309     case EE_WAIT_FOR_ERASE_COMPLETION:
 115                         ; 310         /* move to interrupt service routine to speedup eeprom write */
 115                         ; 311         // EE_Burst_Write();
 115                         ; 312         break;
 117  0026                   L56:
 118                         ; 314     case EE_WAIT_FOR_WRITE_COMPLETION:
 118                         ; 315         EE_WaitForWriteCompletion();
 120  0026 4a024a4a          	call	L51f_EE_WaitForWriteCompletion
 122                         ; 316         break;
 125  002a 0a                	rtc	
 126  002b                   L76:
 127                         ; 318     case EE_RETRY:
 127                         ; 319         EE_Retry();
 129  002b 4a02d5d5          	call	L71f_EE_Retry
 131                         ; 320         break;
 133                         ; 322     case EE_IDLE:
 133                         ; 323         break;
 135                         ; 325     default:
 135                         ; 326         break;
 137                         ; 328 }
 140  002f 0a                	rtc	
 172                         ; 340 unsigned char EE_DRIVER_CALL EE_IsBusy(void)
 172                         ; 341 {
 173                         	switch	.ftext
 174  0030                   f_EE_IsBusy:
 176  0030 37                	pshb	
 177       00000001          OFST:	set	1
 180                         ; 344     if(eEECurrentState == EE_IDLE)
 182  0031 f6000c            	ldab	L13_eEECurrentState
 183  0034 2604              	bne	L521
 184                         ; 346         bystatus = ITBM_FALSE;
 186  0036 6b80              	stab	OFST-1,s
 188  0038 2002              	bra	L721
 189  003a                   L521:
 190                         ; 350         bystatus = ITBM_TRUE;
 192  003a c601              	ldab	#1
 193  003c                   L721:
 194                         ; 353     return bystatus;
 198  003c 1b81              	leas	1,s
 199  003e 0a                	rtc	
 250                         ; 367 unsigned char EE_DRIVER_CALL EE_ForcedWrite(unsigned long ee_dest, unsigned char *pbysrc, unsigned char bysize)
 250                         ; 368 {
 251                         	switch	.ftext
 252  003f                   f_EE_ForcedWrite:
 254       fffffffc          OFST:	set	-4
 257                         ; 371     sEEPendingWrite.lAddress = ee_dest;
 259  003f 7c0114            	std	L52_sEEPendingWrite+2
 260  0042 7e0112            	stx	L52_sEEPendingWrite
 261                         ; 374     sEEPendingWrite.pbyData = pbysrc;
 263  0045 1805830116        	movw	OFST+7,s,L52_sEEPendingWrite+4
 264                         ; 377     sEEPendingWrite.byLength = bysize;
 266  004a 180d860118        	movb	OFST+10,s,L52_sEEPendingWrite+6
 267                         ; 379     sEEPendingWrite.byRetryCount = 0;
 269  004f 79011a            	clr	L52_sEEPendingWrite+8
 270                         ; 382     if (EE_PrepDataForWrite())
 272  0052 4a011414          	call	L3f_EE_PrepDataForWrite
 274  0056 044107            	tbeq	b,L351
 275                         ; 385        EE_SetNextStateTo(EE_WRITE_SEQUENCE_INITIATED);
 277  0059 cc0001            	ldd	#1
 278  005c 4a006363          	call	L11f_EE_SetNextStateTo
 280  0060                   L351:
 281                         ; 388     return 1;
 283  0060 c601              	ldab	#1
 286  0062 0a                	rtc	
 365                         ; 400 void EE_SetNextStateTo(EEEState enew_state)
 365                         ; 401 {
 366                         	switch	.ftext
 367  0063                   L11f_EE_SetNextStateTo:
 371                         ; 402     eEECurrentState = enew_state;
 373  0063 7b000c            	stab	L13_eEECurrentState
 374                         ; 403 }
 377  0066 0a                	rtc	
 436                         ; 418 ITBM_BOOLEAN EE_SubmitCommand(void)
 436                         ; 419 {
 437                         	switch	.ftext
 438  0067                   L5f_EE_SubmitCommand:
 440  0067 37                	pshb	
 441       00000001          OFST:	set	1
 444                         ; 423     if (Flash_IsProtectionViolation())
 446  0068 1f00001004        	brclr	__FSTAT,16,L152
 447                         ; 425         Flash_ClearProtectionViolation();
 449  006d 1c000010          	bset	__FSTAT,16
 450  0071                   L152:
 451                         ; 428     if (Flash_IsAccessError())
 453  0071 1f00002004        	brclr	__FSTAT,32,L352
 454                         ; 430        Flash_ClearAccessError();
 456  0076 1c000020          	bset	__FSTAT,32
 457  007a                   L352:
 458                         ; 433     FCCOBIX = 0; 
 460  007a 87                	clra	
 461  007b 7a0000            	staa	__FCCOBIX
 462                         ; 434     FCCOB = (((UInt16)sFCCOBBlock.bFlashCmd) << 8) + sFCCOBBlock.bGlblAddr_Bit22To16;
 464  007e b60000            	ldaa	L33_sFCCOBBlock
 465  0081 c7                	clrb	
 466  0082 fb0001            	addb	L33_sFCCOBBlock+1
 467  0085 8900              	adca	#0
 468  0087 7c0000            	std	__FCCOB
 469                         ; 436     switch (sFCCOBBlock.bFlashCmd)
 471  008a f60000            	ldab	L33_sFCCOBBlock
 473  008d c011              	subb	#17
 474  008f 2713              	beq	L312
 475  0091 040106            	dbeq	b,L112
 476  0094 c00e              	subb	#14
 477  0096 273e              	beq	L712
 478  0098 2051              	bra	L752
 479  009a                   L112:
 480                         ; 439         case ERASE_D_FLASH_SECTOR:
 480                         ; 440             FCCOBIX = 1;
 482  009a c601              	ldab	#1
 483  009c 7b0000            	stab	__FCCOBIX
 484                         ; 441             FCCOB = sFCCOBBlock.wGlblAddr_Bit15To0;
 486  009f fc0002            	ldd	L33_sFCCOBBlock+2
 487                         ; 442             break;
 489  00a2 2044              	bra	LC001
 490  00a4                   L312:
 491                         ; 445         case PROGRAM_D_FLASH_WORDS:
 491                         ; 446             FCCOBIX = 1;
 493  00a4 c601              	ldab	#1
 494  00a6 7b0000            	stab	__FCCOBIX
 495                         ; 448             FCCOB = sFCCOBBlock.wGlblAddr_Bit15To0;
 497  00a9 180400020000      	movw	L33_sFCCOBBlock+2,__FCCOB
 498                         ; 449             FCCOBIX = 2;
 500  00af 52                	incb	
 501  00b0 7b0000            	stab	__FCCOBIX
 502                         ; 450             FCCOB = sFCCOBBlock.wData0;
 504  00b3 180400040000      	movw	L33_sFCCOBBlock+4,__FCCOB
 505                         ; 451             FCCOBIX = 3;
 507  00b9 52                	incb	
 508  00ba 7b0000            	stab	__FCCOBIX
 509                         ; 452             FCCOB = sFCCOBBlock.wData1;
 511  00bd 180400060000      	movw	L33_sFCCOBBlock+6,__FCCOB
 512                         ; 453             FCCOBIX = 4;
 514  00c3 52                	incb	
 515  00c4 7b0000            	stab	__FCCOBIX
 516                         ; 454             FCCOB = sFCCOBBlock.wData2;
 518  00c7 180400080000      	movw	L33_sFCCOBBlock+8,__FCCOB
 519                         ; 455             FCCOBIX = 5;
 521  00cd 52                	incb	
 522  00ce 7b0000            	stab	__FCCOBIX
 523                         ; 456             FCCOB = sFCCOBBlock.wData3;
 525  00d1 fc000a            	ldd	L33_sFCCOBBlock+10
 526                         ; 457             break;
 528  00d4 2012              	bra	LC001
 529                         ; 460         case EMULATION_QUERY:
 529                         ; 461             break;
 531  00d6                   L712:
 532                         ; 464         case PARTITION_DFLASH:
 532                         ; 465             FCCOBIX = 1;
 534  00d6 c601              	ldab	#1
 535  00d8 7b0000            	stab	__FCCOBIX
 536                         ; 466             FCCOB = sFCCOBBlock.wGlblAddr_Bit15To0;
 538  00db 180400020000      	movw	L33_sFCCOBBlock+2,__FCCOB
 539                         ; 467             FCCOBIX = 2;
 541  00e1 52                	incb	
 542  00e2 7b0000            	stab	__FCCOBIX
 543                         ; 468             FCCOB = sFCCOBBlock.wData0;
 545  00e5 fc0004            	ldd	L33_sFCCOBBlock+4
 546  00e8                   LC001:
 547  00e8 7c0000            	std	__FCCOB
 548                         ; 469             break;
 550                         ; 471         default:  
 550                         ; 472             break;
 552  00eb                   L752:
 553                         ; 476     Flash_SetCommand();
 555  00eb c680              	ldab	#128
 556  00ed 7b0000            	stab	__FSTAT
 557                         ; 478     if (Flash_IsProtectionViolation() || Flash_IsAccessError())
 559  00f0 1e00001005        	brset	__FSTAT,16,L362
 561  00f5 1f00002016        	brclr	__FSTAT,32,L162
 562  00fa                   L362:
 563                         ; 480        if (Flash_IsProtectionViolation())
 565  00fa 1f00001004        	brclr	__FSTAT,16,L562
 566                         ; 482            Flash_ClearProtectionViolation();
 568  00ff 1c000010          	bset	__FSTAT,16
 569  0103                   L562:
 570                         ; 485        if(Flash_IsAccessError())
 572  0103 1f00002004        	brclr	__FSTAT,32,L762
 573                         ; 487            Flash_ClearAccessError();
 575  0108 1c000020          	bset	__FSTAT,32
 576  010c                   L762:
 577                         ; 490         byresult = ITBM_FALSE;
 579  010c c7                	clrb	
 581  010d                   L172:
 582                         ; 498     return byresult;
 586  010d 1b81              	leas	1,s
 587  010f 0a                	rtc	
 588  0110                   L162:
 589                         ; 495         byresult = ITBM_TRUE;
 591  0110 c601              	ldab	#1
 592  0112 20f9              	bra	L172
 660                         ; 516 ITBM_BOOLEAN EE_PrepDataForWrite(void)
 660                         ; 517 {
 661                         	switch	.ftext
 662  0114                   L3f_EE_PrepDataForWrite:
 664  0114 1b99              	leas	-7,s
 665       00000007          OFST:	set	7
 668                         ; 523     if (sEEPendingWrite.byLength != 0)
 670  0116 f60118            	ldab	L52_sEEPendingWrite+6
 671  0119 2778              	beq	L323
 672                         ; 526         EE_SetEPAGE(sEEPendingWrite.lAddress);
 674  011b fc0114            	ldd	L52_sEEPendingWrite+2
 675  011e fe0112            	ldx	L52_sEEPendingWrite
 676  0121 4a000000          	call	f_GetEPage
 678  0125 7b0000            	stab	__EPAGE
 679                         ; 527         pbyAddress = EE_GetLogicalAddr(sEEPendingWrite.lAddress);
 681  0128 fc0114            	ldd	L52_sEEPendingWrite+2
 682  012b fe0112            	ldx	L52_sEEPendingWrite
 683  012e 4a000000          	call	f_GetEOffset
 685  0132 c30800            	addd	#2048
 686  0135 6c84              	std	OFST-3,s
 687                         ; 530         pbysector_address = EE_SectorAddress(pbyAddress);
 689  0137 c7                	clrb	
 690  0138 6c82              	std	OFST-5,s
 691                         ; 533         for (bycopy_index = 0; bycopy_index < DFLASH_WORDS_PER_SECTOR; bycopy_index++)
 693  013a 18c7              	clry	
 694  013c 6d80              	sty	OFST-7,s
 695  013e                   L523:
 696                         ; 536            uDFlashSectorBuffer.SectorData.pbyWords[bycopy_index] = *(UInt16 *)pbysector_address;
 698  013e 1858              	lsly	
 699  0140 ee82              	ldx	OFST-5,s
 700  0142 180200ea0011      	movw	0,x,L72_uDFlashSectorBuffer+4,y
 701                         ; 537            pbysector_address++;
 703  0148 ed82              	ldy	OFST-5,s
 704  014a 1942              	leay	2,y
 705  014c 6d82              	sty	OFST-5,s
 706                         ; 533         for (bycopy_index = 0; bycopy_index < DFLASH_WORDS_PER_SECTOR; bycopy_index++)
 708  014e 186280            	incw	OFST-7,s
 711  0151 ed80              	ldy	OFST-7,s
 712  0153 8d0080            	cpy	#128
 713  0156 25e6              	blo	L523
 714                         ; 541         sEEPendingWrite.byBytesWritten = 0;
 716  0158 87                	clra	
 717  0159 7a0119            	staa	L52_sEEPendingWrite+7
 718                         ; 547         for(bycopy_index = EE_AddressSectorOffset(pbyAddress); 
 720  015c e685              	ldab	OFST-2,s
 721  015e 6c80              	std	OFST-7,s
 723  0160 fe0116            	ldx	L52_sEEPendingWrite+4
 724  0163 b746              	tfr	d,y
 725  0165 200e              	bra	L733
 726  0167                   L333:
 727                         ; 551             uDFlashSectorBuffer.SectorData.pbyBytes[bycopy_index] = *(sEEPendingWrite.pbyData);
 729  0167 180a00ea0011      	movb	0,x,L72_uDFlashSectorBuffer+4,y
 730                         ; 556             sEEPendingWrite.pbyData++;
 732  016d 08                	inx	
 733                         ; 559             sEEPendingWrite.byBytesWritten++;
 735  016e 720119            	inc	L52_sEEPendingWrite+7
 736                         ; 562             sEEPendingWrite.byLength--;
 738  0171 730118            	dec	L52_sEEPendingWrite+6
 739                         ; 548             ((bycopy_index < DFLASH_BYTES_PER_SECTOR) && (sEEPendingWrite.byLength > 0)); bycopy_index++)
 741  0174 02                	iny	
 742  0175                   L733:
 743                         ; 547         for(bycopy_index = EE_AddressSectorOffset(pbyAddress); 
 743                         ; 548             ((bycopy_index < DFLASH_BYTES_PER_SECTOR) && (sEEPendingWrite.byLength > 0)); bycopy_index++)
 745  0175 8d0100            	cpy	#256
 746  0178 2405              	bhs	LC003
 748  017a f60118            	ldab	L52_sEEPendingWrite+6
 749  017d 26e8              	bne	L333
 750  017f                   LC003:
 751  017f 7e0116            	stx	L52_sEEPendingWrite+4
 752                         ; 566         uDFlashSectorBuffer.SectorAddr.ulWhole = DFlash_GetSectorAddr(sEEPendingWrite.lAddress);
 754  0182 b60114            	ldaa	L52_sEEPendingWrite+2
 755  0185 fe0112            	ldx	L52_sEEPendingWrite
 756  0188 c7                	clrb	
 757  0189 7c000f            	std	L72_uDFlashSectorBuffer+2
 758  018c 7e000d            	stx	L72_uDFlashSectorBuffer
 759                         ; 569         uDFlashSectorBuffer.DFlash_buffer_index = 0;
 761  018f 790111            	clr	L72_uDFlashSectorBuffer+260
 762                         ; 571         byresult = ITBM_TRUE;
 764  0192 52                	incb	
 766  0193                   L323:
 767                         ; 575         byresult = ITBM_FALSE;
 769                         ; 578     return byresult;
 773  0193 1b87              	leas	7,s
 774  0195 0a                	rtc	
 803                         ; 595 void EE_WriteSequenceInitiated(void)
 803                         ; 596 {
 804                         	switch	.ftext
 805  0196                   L31f_EE_WriteSequenceInitiated:
 809                         ; 599     if (Flash_IsCommandComplete())
 811  0196 1f00008028        	brclr	__FSTAT,128,L753
 812                         ; 602         sFCCOBBlock.bFlashCmd = ERASE_D_FLASH_SECTOR;
 814  019b c612              	ldab	#18
 815  019d 7b0000            	stab	L33_sFCCOBBlock
 816                         ; 603         sFCCOBBlock.bGlblAddr_Bit22To16 = uDFlashSectorBuffer.SectorAddr.pbyBytes[1];
 818  01a0 180c000e0001      	movb	L72_uDFlashSectorBuffer+1,L33_sFCCOBBlock+1
 819                         ; 604         sFCCOBBlock.wGlblAddr_Bit15To0 = uDFlashSectorBuffer.SectorAddr.sWordWise.wLowerHalf;
 821  01a6 1804000f0002      	movw	L72_uDFlashSectorBuffer+2,L33_sFCCOBBlock+2
 822                         ; 607         if (EE_SubmitCommand())
 824  01ac 4a006767          	call	L5f_EE_SubmitCommand
 826  01b0 044109            	tbeq	b,L163
 827                         ; 610             Flash_EnableCmdCompleteInterrupt();
 829  01b3 1c000080          	bset	__FCNFG,128
 830                         ; 613             EE_SetNextStateTo(EE_WAIT_FOR_ERASE_COMPLETION);
 832  01b7 cc0002            	ldd	#2
 835  01ba 2003              	bra	LC004
 836  01bc                   L163:
 837                         ; 619             EE_SetNextStateTo(EE_RETRY);
 839  01bc cc0004            	ldd	#4
 840  01bf                   LC004:
 841  01bf 4a006363          	call	L11f_EE_SetNextStateTo
 843  01c3                   L753:
 844                         ; 622 }
 847  01c3 0a                	rtc	
 875                         ; 642 void EE_DRIVER_CALL EE_Burst_Write(void)
 875                         ; 643 {
 876                         	switch	.ftext
 877  01c4                   f_EE_Burst_Write:
 881                         ; 645     if (Flash_IsCommandComplete())
 883  01c4 f60000            	ldab	__FSTAT
 884  01c7 c480              	andb	#128
 885  01c9 c180              	cmpb	#128
 886  01cb 267c              	bne	L573
 887                         ; 649         sFCCOBBlock.bFlashCmd = PROGRAM_D_FLASH_WORDS;
 889  01cd c611              	ldab	#17
 890  01cf 7b0000            	stab	L33_sFCCOBBlock
 891                         ; 650         sFCCOBBlock.bGlblAddr_Bit22To16 = uDFlashSectorBuffer.SectorAddr.pbyBytes[1];
 893  01d2 180c000e0001      	movb	L72_uDFlashSectorBuffer+1,L33_sFCCOBBlock+1
 894                         ; 653         if (uDFlashSectorBuffer.DFlash_buffer_index <= (DFLASH_WORDS_PER_SECTOR - DFLASH_WORDS_PER_WRITE))
 896  01d8 f60111            	ldab	L72_uDFlashSectorBuffer+260
 897  01db c17c              	cmpb	#124
 898  01dd 225f              	bhi	L773
 899                         ; 658             sFCCOBBlock.wGlblAddr_Bit15To0 = uDFlashSectorBuffer.SectorAddr.sWordWise.wLowerHalf + (uDFlashSectorBuffer.DFlash_buffer_index << 1);
 901  01df 87                	clra	
 902  01e0 59                	lsld	
 903  01e1 f3000f            	addd	L72_uDFlashSectorBuffer+2
 904  01e4 7c0002            	std	L33_sFCCOBBlock+2
 905                         ; 659             sFCCOBBlock.wData0 = uDFlashSectorBuffer.SectorData.pbyWords[uDFlashSectorBuffer.DFlash_buffer_index];
 907  01e7 f60111            	ldab	L72_uDFlashSectorBuffer+260
 908  01ea 87                	clra	
 909  01eb 59                	lsld	
 910  01ec b746              	tfr	d,y
 911  01ee 1805ea00110004    	movw	L72_uDFlashSectorBuffer+4,y,L33_sFCCOBBlock+4
 912                         ; 660             uDFlashSectorBuffer.DFlash_buffer_index++;
 914  01f5 720111            	inc	L72_uDFlashSectorBuffer+260
 915                         ; 661             sFCCOBBlock.wData1 = uDFlashSectorBuffer.SectorData.pbyWords[uDFlashSectorBuffer.DFlash_buffer_index] ;
 917  01f8 f60111            	ldab	L72_uDFlashSectorBuffer+260
 918  01fb 87                	clra	
 919  01fc 59                	lsld	
 920  01fd b746              	tfr	d,y
 921  01ff 1805ea00110006    	movw	L72_uDFlashSectorBuffer+4,y,L33_sFCCOBBlock+6
 922                         ; 662             uDFlashSectorBuffer.DFlash_buffer_index++;
 924  0206 720111            	inc	L72_uDFlashSectorBuffer+260
 925                         ; 663             sFCCOBBlock.wData2 = uDFlashSectorBuffer.SectorData.pbyWords[uDFlashSectorBuffer.DFlash_buffer_index] ;
 927  0209 f60111            	ldab	L72_uDFlashSectorBuffer+260
 928  020c 87                	clra	
 929  020d 59                	lsld	
 930  020e b746              	tfr	d,y
 931  0210 1805ea00110008    	movw	L72_uDFlashSectorBuffer+4,y,L33_sFCCOBBlock+8
 932                         ; 664             uDFlashSectorBuffer.DFlash_buffer_index++;
 934  0217 720111            	inc	L72_uDFlashSectorBuffer+260
 935                         ; 665             sFCCOBBlock.wData3 = uDFlashSectorBuffer.SectorData.pbyWords[uDFlashSectorBuffer.DFlash_buffer_index] ;
 937  021a f60111            	ldab	L72_uDFlashSectorBuffer+260
 938  021d 87                	clra	
 939  021e 59                	lsld	
 940  021f b746              	tfr	d,y
 941  0221 1805ea0011000a    	movw	L72_uDFlashSectorBuffer+4,y,L33_sFCCOBBlock+10
 942                         ; 666             uDFlashSectorBuffer.DFlash_buffer_index++;
 944  0228 720111            	inc	L72_uDFlashSectorBuffer+260
 945                         ; 673             if (!EE_SubmitCommand())
 947  022b 4a006767          	call	L5f_EE_SubmitCommand
 949  022f 046117            	tbne	b,L573
 950                         ; 676                 Flash_DisableCmdCompleteInterrupt();
 952  0232 1d000080          	bclr	__FCNFG,128
 953                         ; 679                 uDFlashSectorBuffer.DFlash_buffer_index = 0;
 955  0236 790111            	clr	L72_uDFlashSectorBuffer+260
 956                         ; 681                 EE_SetNextStateTo(EE_RETRY);
 958  0239 cc0004            	ldd	#4
 960  023c 2007              	bra	LC005
 961  023e                   L773:
 962                         ; 689             Flash_DisableCmdCompleteInterrupt();
 964  023e 1d000080          	bclr	__FCNFG,128
 965                         ; 692             EE_SetNextStateTo(EE_WAIT_FOR_WRITE_COMPLETION);
 967  0242 cc0003            	ldd	#3
 968  0245                   LC005:
 969  0245 4a006363          	call	L11f_EE_SetNextStateTo
 971  0249                   L573:
 972                         ; 695 }
 975  0249 0a                	rtc	
1038                         ; 711 void EE_WaitForWriteCompletion(void)
1038                         ; 712 {
1039                         	switch	.ftext
1040  024a                   L51f_EE_WaitForWriteCompletion:
1042  024a 1b9b              	leas	-5,s
1043       00000005          OFST:	set	5
1046                         ; 719     if(Flash_IsCommandComplete())
1048  024c f60000            	ldab	__FSTAT
1049  024f c480              	andb	#128
1050  0251 c180              	cmpb	#128
1051  0253 267d              	bne	L334
1052                         ; 723         EE_SetEPAGE(sEEPendingWrite.lAddress);
1054  0255 fc0114            	ldd	L52_sEEPendingWrite+2
1055  0258 fe0112            	ldx	L52_sEEPendingWrite
1056  025b 4a000000          	call	f_GetEPage
1058  025f 7b0000            	stab	__EPAGE
1059                         ; 724         pbyAddress = EE_GetLogicalAddr(sEEPendingWrite.lAddress);
1061  0262 fc0114            	ldd	L52_sEEPendingWrite+2
1062  0265 fe0112            	ldx	L52_sEEPendingWrite
1063  0268 4a000000          	call	f_GetEOffset
1065  026c c30800            	addd	#2048
1066  026f 6c83              	std	OFST-2,s
1067                         ; 727         pbysector_address = EE_SectorAddress(pbyAddress);
1069  0271 c7                	clrb	
1070  0272 6c81              	std	OFST-4,s
1071                         ; 730         endloop = ITBM_FALSE;
1073  0274 6980              	clr	OFST-5,s
1074                         ; 731         for (uDFlashSectorBuffer.DFlash_buffer_index = 0; 
1076  0276 790111            	clr	L72_uDFlashSectorBuffer+260
1078  0279 2019              	bra	L144
1079  027b                   L534:
1080                         ; 735            if (uDFlashSectorBuffer.SectorData.pbyWords[uDFlashSectorBuffer.DFlash_buffer_index] != *(UInt16 *)pbysector_address)
1082  027b 87                	clra	
1083  027c 59                	lsld	
1084  027d b746              	tfr	d,y
1085  027f ecea0011          	ldd	L72_uDFlashSectorBuffer+4,y
1086  0283 ed81              	ldy	OFST-4,s
1087  0285 ac40              	cpd	0,y
1088  0287 2704              	beq	L544
1089                         ; 737               endloop = ITBM_TRUE;
1091  0289 c601              	ldab	#1
1092  028b 6b80              	stab	OFST-5,s
1093  028d                   L544:
1094                         ; 739            pbysector_address++;
1096  028d 1942              	leay	2,y
1097  028f 6d81              	sty	OFST-4,s
1098                         ; 732              ((uDFlashSectorBuffer.DFlash_buffer_index < DFLASH_WORDS_PER_SECTOR) && (endloop == ITBM_FALSE)); 
1098                         ; 733              (uDFlashSectorBuffer.DFlash_buffer_index++))
1100  0291 720111            	inc	L72_uDFlashSectorBuffer+260
1101  0294                   L144:
1102                         ; 731         for (uDFlashSectorBuffer.DFlash_buffer_index = 0; 
1102                         ; 732              ((uDFlashSectorBuffer.DFlash_buffer_index < DFLASH_WORDS_PER_SECTOR) && (endloop == ITBM_FALSE)); 
1104  0294 f60111            	ldab	L72_uDFlashSectorBuffer+260
1105  0297 c180              	cmpb	#128
1106  0299 2404              	bhs	L744
1108  029b e780              	tst	OFST-5,s
1109  029d 27dc              	beq	L534
1110  029f                   L744:
1111                         ; 743         if (endloop == ITBM_TRUE)
1113  029f e680              	ldab	OFST-5,s
1114  02a1 042105            	dbne	b,L154
1115                         ; 746             EE_SetNextStateTo(EE_RETRY);
1117  02a4 cc0004            	ldd	#4
1120  02a7 2025              	bra	LC006
1121  02a9                   L154:
1122                         ; 750             if(sEEPendingWrite.byLength != 0)
1124  02a9 f60118            	ldab	L52_sEEPendingWrite+6
1125  02ac 271f              	beq	L554
1126                         ; 753                 sEEPendingWrite.lAddress += sEEPendingWrite.byBytesWritten;
1128  02ae f60119            	ldab	L52_sEEPendingWrite+7
1129  02b1 87                	clra	
1130  02b2 1887              	clrx	
1131  02b4 f30114            	addd	L52_sEEPendingWrite+2
1132  02b7 18b90112          	adex	L52_sEEPendingWrite
1133  02bb 7c0114            	std	L52_sEEPendingWrite+2
1134  02be 7e0112            	stx	L52_sEEPendingWrite
1135                         ; 756                 if (EE_PrepDataForWrite())
1137  02c1 4a011414          	call	L3f_EE_PrepDataForWrite
1139  02c5 04410a            	tbeq	b,L334
1140                         ; 758                    EE_SetNextStateTo(EE_WRITE_SEQUENCE_INITIATED);
1142  02c8 cc0001            	ldd	#1
1144  02cb 2001              	bra	LC006
1145  02cd                   L554:
1146                         ; 763                 EE_SetNextStateTo(EE_IDLE);
1148  02cd 87                	clra	
1149  02ce                   LC006:
1150  02ce 4a006363          	call	L11f_EE_SetNextStateTo
1152  02d2                   L334:
1153                         ; 767 }
1156  02d2 1b85              	leas	5,s
1157  02d4 0a                	rtc	
1183                         ; 780 void EE_Retry(void)
1183                         ; 781 {
1184                         	switch	.ftext
1185  02d5                   L71f_EE_Retry:
1189                         ; 783     if (Flash_IsProtectionViolation())
1191  02d5 1f00001004        	brclr	__FSTAT,16,L374
1192                         ; 785        Flash_ClearProtectionViolation();
1194  02da 1c000010          	bset	__FSTAT,16
1195  02de                   L374:
1196                         ; 788     if (Flash_IsAccessError())
1198  02de 1f00002004        	brclr	__FSTAT,32,L574
1199                         ; 790        Flash_ClearAccessError();
1201  02e3 1c000020          	bset	__FSTAT,32
1202  02e7                   L574:
1203                         ; 794     if(Flash_IsCommandComplete())
1205  02e7 1f00008032        	brclr	__FSTAT,128,L774
1206                         ; 799         if (Flash_IsProtectionViolation())
1208  02ec 1f00001004        	brclr	__FSTAT,16,L105
1209                         ; 801             Flash_ClearProtectionViolation();
1211  02f1 1c000010          	bset	__FSTAT,16
1212  02f5                   L105:
1213                         ; 804         if (Flash_IsAccessError())
1215  02f5 1f00002004        	brclr	__FSTAT,32,L305
1216                         ; 806             Flash_ClearAccessError();
1218  02fa 1c000020          	bset	__FSTAT,32
1219  02fe                   L305:
1220                         ; 809         if(sEEPendingWrite.byRetryCount < EE_MAX_RETRY)
1222  02fe f6011a            	ldab	L52_sEEPendingWrite+8
1223  0301 c103              	cmpb	#3
1224  0303 240f              	bhs	L505
1225                         ; 812             sEEPendingWrite.byRetryCount++;
1227  0305 72011a            	inc	L52_sEEPendingWrite+8
1228                         ; 814             EE_ErrorHook();
1230  0308 4a031f1f          	call	L7f_EE_ErrorHook
1232                         ; 816             EE_SetNextStateTo(EE_WRITE_SEQUENCE_INITIATED);
1234  030c cc0001            	ldd	#1
1235  030f 4a006363          	call	L11f_EE_SetNextStateTo
1239  0313 0a                	rtc	
1240  0314                   L505:
1241                         ; 821             EE_SetNextStateTo(EE_IDLE);
1243  0314 87                	clra	
1244  0315 c7                	clrb	
1245  0316 4a006363          	call	L11f_EE_SetNextStateTo
1247                         ; 823             EE_ErrorHook();
1249  031a 4a031f1f          	call	L7f_EE_ErrorHook
1251  031e                   L774:
1252                         ; 826 }
1255  031e 0a                	rtc	
1278                         ; 838 void EE_ErrorHook(void)
1278                         ; 839 {
1279                         	switch	.ftext
1280  031f                   L7f_EE_ErrorHook:
1284                         ; 840     _asm("nop");
1287  031f a7                	nop	
1289                         ; 841 }
1292  0320 0a                	rtc	
1316                         ; 913 void EE_DRIVER_CALL EE_Driver_Static_Init (void)
1316                         ; 914 {
1317                         	switch	.ftext
1318  0321                   f_EE_Driver_Static_Init:
1322                         ; 915    eEECurrentState = EE_IDLE; 
1324  0321 79000c            	clr	L13_eEECurrentState
1325                         ; 916 }
1328  0324 0a                	rtc	
1352                         ; 932 interrupt near void IsrFlashCmdCompleteInterrupt(void)
1352                         ; 933 {
1353                         	switch	.text
1354  0000                   _IsrFlashCmdCompleteInterrupt:
1359                         ; 934     EE_Burst_Write();
1361  0000 4a01c4c4          	call	f_EE_Burst_Write
1363                         ; 935 }
1366  0004 0b                	rti	
1640                         	xdef	_IsrFlashCmdCompleteInterrupt
1641                         	switch	.bss
1642  0000                   L33_sFCCOBBlock:
1643  0000 0000000000000000  	ds.b	12
1644  000c                   L13_eEECurrentState:
1645  000c 00                	ds.b	1
1646  000d                   L72_uDFlashSectorBuffer:
1647  000d 0000000000000000  	ds.b	261
1648  0112                   L52_sEEPendingWrite:
1649  0112 0000000000000000  	ds.b	9
1650                         	xdef	f_EE_Driver_Static_Init
1651                         	xdef	f_EE_Burst_Write
1652                         	xdef	f_EE_ForcedWrite
1653                         	xdef	f_EE_DriverPowerupInit
1654                         	xdef	f_EE_IsBusy
1655                         	xdef	f_EE_Handler
1656                         	xref	f_GetEOffset
1657                         	xref	f_GetEPage
1658                         	xref	__FCCOB
1659                         	xref	__FSTAT
1660                         	xref	__FERCNFG
1661                         	xref	__FCNFG
1662                         	xref	__FCCOBIX
1663                         	xref	__FCLKDIV
1664                         	xref	__EPAGE
1685                         	end
