   1                         ; C Compiler for S12XE [COSMIC Software].
   2                         ; Generator V4.7.8 - 27 Jul 2007
   3                         ; Optimizer V4.6.13 - 27 Jul 2007
   4                         .const:	section	.data
   5  0000                   L3_CanShiftLookUp:
   6  0000 0001              	dc.w	1
   7  0002 0002              	dc.w	2
   8  0004 0004              	dc.w	4
   9  0006 0008              	dc.w	8
  10  0008 0010              	dc.w	16
  11  000a 0020              	dc.w	32
  12  000c 0040              	dc.w	64
  13  000e 0080              	dc.w	128
  14  0010 0100              	dc.w	256
  15  0012 0200              	dc.w	512
  16  0014 0400              	dc.w	1024
  17  0016 0800              	dc.w	2048
  18  0018 1000              	dc.w	4096
  19  001a 2000              	dc.w	8192
  20  001c 4000              	dc.w	16384
  21  001e 8000              	dc.w	-32768
  22  0020                   L5_CanGetHighestFlagFromNibble:
  23  0020 ff                	dc.b	255
  24  0021 00                	dc.b	0
  25  0022 01                	dc.b	1
  26  0023 01                	dc.b	1
  27  0024 02                	dc.b	2
  28  0025 02                	dc.b	2
  29  0026 02                	dc.b	2
  30  0027 02                	dc.b	2
  31  0028 03                	dc.b	3
  32  0029 03                	dc.b	3
  33  002a 03                	dc.b	3
  34  002b 03                	dc.b	3
  35  002c 03                	dc.b	3
  36  002d 03                	dc.b	3
  37  002e 03                	dc.b	3
  38  002f 03                	dc.b	3
  39  0030                   _kCanMainVersion:
  40  0030 05                	dc.b	5
  41  0031                   _kCanSubVersion:
  42  0031 11                	dc.b	17
  43  0032                   _kCanBugFixVersion:
  44  0032 02                	dc.b	2
  45  0033                   L7_CanMailboxSelect:
  46  0033 01                	dc.b	1
  47  0034 02                	dc.b	2
  48  0035 04                	dc.b	4
  49  0036                   L11_CanFirstMailbox:
  50  0036 ff                	dc.b	255
  51  0037 00                	dc.b	0
  52  0038 01                	dc.b	1
  53  0039 00                	dc.b	0
  54  003a 02                	dc.b	2
  55  003b 00                	dc.b	0
  56  003c 01                	dc.b	1
  57  003d 00                	dc.b	0
 136                         ; 1538 static void CanCanInterruptDisableInternal(CAN_CHANNEL_CANTYPE_FIRST tCanLLCanIntOld* pOldFlagPtr)
 136                         ; 1539 {
 137                         .ftext:	section	.text
 138  0000                   L36f_CanCanInterruptDisableInternal:
 140  0000 3b                	pshd	
 141  0001 37                	pshb	
 142       00000001          OFST:	set	1
 145                         ; 1541   CanNestedGlobalInterruptDisable();
 147  0002 b774              	tfr	s,d
 148  0004 4a000000          	call	f_VStdLL_GlobalInterruptDisable
 150                         ; 1543   pOldFlagPtr -> oldCanCRIER = CRIER;
 153  0008 ed81              	ldy	OFST+0,s
 154  000a ce0145            	ldx	#325
 155  000d 180a0040          	movb	0,x,0,y
 156                         ; 1544   pOldFlagPtr -> oldCanCTIER = CTIER;
 158  0011 ce0147            	ldx	#327
 159  0014 180a0041          	movb	0,x,1,y
 160                         ; 1545   CRIER = kCanCRIERInterruptsDisabledVal;
 162  0018 790145            	clr	325
 163                         ; 1546   CTIER = (vuint8)0;
 165  001b 790147            	clr	327
 166                         ; 1548   canCanIrqDisabled[channel] = kCanTrue;
 169  001e cc0001            	ldd	#1
 170  0021 7c0000            	std	L14_canCanIrqDisabled
 171                         ; 1550   CanNestedGlobalInterruptRestore();
 174  0024 e680              	ldab	OFST-1,s
 175  0026 4a000000          	call	f_VStdLL_GlobalInterruptRestore
 177                         ; 1551 }
 180  002a 1b83              	leas	3,s
 181  002c 0a                	rtc	
 227                         ; 1565 static void CanCanInterruptRestoreInternal(CAN_CHANNEL_CANTYPE_FIRST tCanLLCanIntOld nOldFlag)
 227                         ; 1566 {
 228                         	switch	.ftext
 229  002d                   L56f_CanCanInterruptRestoreInternal:
 231  002d 37                	pshb	
 232       00000001          OFST:	set	1
 235                         ; 1568   CanNestedGlobalInterruptDisable();
 237  002e b774              	tfr	s,d
 238  0030 4a000000          	call	f_VStdLL_GlobalInterruptDisable
 240                         ; 1570   CRIER = nOldFlag.oldCanCRIER;
 243  0034 cd0145            	ldy	#325
 244  0037 180a8440          	movb	OFST+3,s,0,y
 245                         ; 1571   CTIER = nOldFlag.oldCanCTIER;
 247  003b cd0147            	ldy	#327
 248  003e 180a8540          	movb	OFST+4,s,0,y
 249                         ; 1573   canCanIrqDisabled[channel] = kCanFalse;
 252  0042 18790000          	clrw	L14_canCanIrqDisabled
 253                         ; 1575   CanNestedGlobalInterruptRestore();
 256  0046 e680              	ldab	OFST-1,s
 257  0048 87                	clra	
 258  0049 4a000000          	call	f_VStdLL_GlobalInterruptRestore
 260                         ; 1576 }
 263  004d 1b81              	leas	1,s
 264  004f 0a                	rtc	
 295                         ; 1668 vuint8 CanGetDriverStatus(CAN_CHANNEL_CANTYPE_ONLY)
 295                         ; 1669 {
 296                         	switch	.ftext
 297  0050                   f_CanGetDriverStatus:
 299  0050 37                	pshb	
 300       00000001          OFST:	set	1
 303                         ; 1674   if((CRFLG & BOFFIF) == BOFFIF)
 305  0051 1e01440c02        	brset	324,12,LC001
 306  0056 200f              	bra	L361
 307  0058                   LC001:
 308                         ; 1677     if((CRFLG & CSCIF) == CSCIF)
 310  0058 1f01444004        	brclr	324,64,L561
 311                         ; 1680       retVal = kCanDriverBusoff;
 313  005d c601              	ldab	#1
 315  005f 2002              	bra	LC002
 316  0061                   L561:
 317                         ; 1685       retVal = kCanDriverBusoffInit;
 319  0061 c602              	ldab	#2
 320  0063                   LC002:
 321  0063 6b80              	stab	OFST-1,s
 322  0065 2002              	bra	L171
 323  0067                   L361:
 324                         ; 1691     retVal = kCanDriverNormal;
 326  0067 c603              	ldab	#3
 327  0069                   L171:
 328                         ; 1694   return retVal;
 332  0069 1b81              	leas	1,s
 333  006b 0a                	rtc	
 358                         ; 1720 static void CanLL_WakeUpHandling(CAN_CHANNEL_CANTYPE_ONLY)
 358                         ; 1721 {
 359                         	switch	.ftext
 360  006c                   L16f_CanLL_WakeUpHandling:
 364                         ; 1725   (void)CanWakeUp(CAN_CHANNEL_CANPARA_ONLY);   /* if hardware does not wake up automatically */
 366  006c 4a09c3c3          	call	f_CanWakeUp
 368                         ; 1726   CRFLG = WUPIF; /* acknowledge wakeup flag */
 370  0070 c680              	ldab	#128
 371  0072 7b0144            	stab	324
 372                         ; 1727   APPL_CAN_WAKEUP( channel );
 374  0075 4a000000          	call	f_ApplCanWakeUp
 376                         ; 1728 }
 379  0079 0a                	rtc	
 402                         ; 1975 CAN_RX_IRQ_FUNC(CanRxInterrupt_0)
 402                         ; 1976 #      endif /* C_ENABLE_OSEK_OS */
 402                         ; 1977 {
 403                         	switch	.text
 404  0000                   _CanRxInterrupt_0:
 409                         ; 1979   CanBasicCanMsgReceived(0);
 411  0000 87                	clra	
 412  0001 c7                	clrb	
 413  0002 4a05c5c5          	call	L74f_CanBasicCanMsgReceived
 415                         ; 1983 }
 418  0006 0b                	rti	
 442                         ; 2010 CAN_TX_IRQ_FUNC(CanTxInterrupt_0)
 442                         ; 2011 #       endif /* C_ENABLE_OSEK_OS */
 442                         ; 2012 {
 443                         	switch	.text
 444  0007                   _CanTxInterrupt_0:
 449                         ; 2014   CanHL_TxConfirmation(CanFirstMailbox[CTIER & CANTFLG & kCanPendingTxObjMask]);
 451  0007 f60146            	ldab	326
 452  000a f40147            	andb	327
 453  000d c406              	andb	#6
 454  000f 87                	clra	
 455  0010 b746              	tfr	d,y
 456  0012 e6ea0036          	ldab	L11_CanFirstMailbox,y
 457  0016 4a077070          	call	L15f_CanHL_TxConfirmation
 459                         ; 2020 }
 462  001a 0b                	rti	
 486                         ; 2047 CAN_WAKEUP_IRQ_FUNC(CanWakeUpInterrupt_0)
 486                         ; 2048 #      endif /* C_ENABLE_OSEK_OS */
 486                         ; 2049 {
 487                         	switch	.text
 488  001b                   _CanWakeUpInterrupt_0:
 493                         ; 2051   CanLL_WakeUpHandling();
 495  001b 4a006c6c          	call	L16f_CanLL_WakeUpHandling
 497                         ; 2055 }
 500  001f 0b                	rti	
 524                         ; 2082 CAN_ERROR_IRQ_FUNC(CanErrorInterrupt_0)
 524                         ; 2083 #      endif /* C_ENABLE_OSEK_OS */
 524                         ; 2084 {
 525                         	switch	.text
 526  0020                   _CanErrorInterrupt_0:
 531                         ; 2086   CanHL_ErrorHandling();
 533  0020 4a05afaf          	call	L75f_CanHL_ErrorHandling
 535                         ; 2090 }
 538  0024 0b                	rti	
 604                         ; 2705 C_API_1 void C_API_2 CanInit( CAN_CHANNEL_CANTYPE_FIRST CanInitHandle initObject )     /* PRQA S 1505 */
 604                         ; 2706 {
 605                         	switch	.ftext
 606  007a                   f_CanInit:
 608  007a 3b                	pshd	
 609  007b 1b99              	leas	-7,s
 610       00000007          OFST:	set	7
 613                         ; 2725   lastInitObject[channel] = initObject;
 615  007d 7c000e            	std	L72_lastInitObject
 616                         ; 2747     CTL0 = INITRQ;
 620  0080 c601              	ldab	#1
 621  0082 7b0140            	stab	320
 622                         ; 2748     APPLCANTIMERSTART(kCanLoopInitReq);
 624  0085                   L172:
 625                         ; 2749     while( ((CTL1 & INITAK) == 0) && APPLCANTIMERLOOP(kCanLoopInitReq) )
 627  0085 1f014101fb        	brclr	321,1,L172
 628                         ; 2755     CTL1  = (vuint8)(CanInitObject[initObject].CanInitCTL1 | CANE);
 631  008a ec87              	ldd	OFST+0,s
 632  008c cd0016            	ldy	#22
 633  008f 13                	emul	
 634  0090 b746              	tfr	d,y
 635  0092 e6ea0001          	ldab	_CanInitObject+1,y
 636  0096 ca80              	orab	#128
 637  0098 7b0141            	stab	321
 638                         ; 2760     CBTR0 = CanInitObject[initObject].CanInitCBTR0;     /* set baudrate */
 640  009b ce0142            	ldx	#322
 641  009e ec87              	ldd	OFST+0,s
 642  00a0 cd0016            	ldy	#22
 643  00a3 13                	emul	
 644  00a4 b756              	tfr	x,y
 645  00a6 b745              	tfr	d,x
 646  00a8 180ae2000240      	movb	_CanInitObject+2,x,0,y
 647                         ; 2761     CBTR1 = CanInitObject[initObject].CanInitCBTR1;
 649  00ae ce0143            	ldx	#323
 650  00b1 ec87              	ldd	OFST+0,s
 651  00b3 cd0016            	ldy	#22
 652  00b6 13                	emul	
 653  00b7 b756              	tfr	x,y
 654  00b9 b745              	tfr	d,x
 655  00bb 180ae2000340      	movb	_CanInitObject+3,x,0,y
 656                         ; 2762     CIDAC = CanInitObject[initObject].CanInitCIDAC;     /* set filter mode */
 658  00c1 ce014b            	ldx	#331
 659  00c4 ec87              	ldd	OFST+0,s
 660  00c6 cd0016            	ldy	#22
 661  00c9 13                	emul	
 662  00ca b756              	tfr	x,y
 663  00cc b745              	tfr	d,x
 664  00ce 180ae2000540      	movb	_CanInitObject+5,x,0,y
 665                         ; 2765     CIDAR0 = CanInitObject[initObject].CanInitCIDAR0;
 667  00d4 ce0150            	ldx	#336
 668  00d7 ec87              	ldd	OFST+0,s
 669  00d9 cd0016            	ldy	#22
 670  00dc 13                	emul	
 671  00dd b756              	tfr	x,y
 672  00df b745              	tfr	d,x
 673  00e1 180ae2000640      	movb	_CanInitObject+6,x,0,y
 674                         ; 2766     CIDAR1 = CanInitObject[initObject].CanInitCIDAR1;
 676  00e7 ce0151            	ldx	#337
 677  00ea ec87              	ldd	OFST+0,s
 678  00ec cd0016            	ldy	#22
 679  00ef 13                	emul	
 680  00f0 b756              	tfr	x,y
 681  00f2 b745              	tfr	d,x
 682  00f4 180ae2000740      	movb	_CanInitObject+7,x,0,y
 683                         ; 2767     CIDAR2 = CanInitObject[initObject].CanInitCIDAR2;
 685  00fa ce0152            	ldx	#338
 686  00fd ec87              	ldd	OFST+0,s
 687  00ff cd0016            	ldy	#22
 688  0102 13                	emul	
 689  0103 b756              	tfr	x,y
 690  0105 b745              	tfr	d,x
 691  0107 180ae2000840      	movb	_CanInitObject+8,x,0,y
 692                         ; 2768     CIDAR3 = CanInitObject[initObject].CanInitCIDAR3;
 694  010d ce0153            	ldx	#339
 695  0110 ec87              	ldd	OFST+0,s
 696  0112 cd0016            	ldy	#22
 697  0115 13                	emul	
 698  0116 b756              	tfr	x,y
 699  0118 b745              	tfr	d,x
 700  011a 180ae2000940      	movb	_CanInitObject+9,x,0,y
 701                         ; 2769     CIDMR0 = CanInitObject[initObject].CanInitCIDMR0;
 703  0120 ce0154            	ldx	#340
 704  0123 ec87              	ldd	OFST+0,s
 705  0125 cd0016            	ldy	#22
 706  0128 13                	emul	
 707  0129 b756              	tfr	x,y
 708  012b b745              	tfr	d,x
 709  012d 180ae2000a40      	movb	_CanInitObject+10,x,0,y
 710                         ; 2770     CIDMR1 = CanInitObject[initObject].CanInitCIDMR1;
 712  0133 ce0155            	ldx	#341
 713  0136 ec87              	ldd	OFST+0,s
 714  0138 cd0016            	ldy	#22
 715  013b 13                	emul	
 716  013c b756              	tfr	x,y
 717  013e b745              	tfr	d,x
 718  0140 180ae2000b40      	movb	_CanInitObject+11,x,0,y
 719                         ; 2771     CIDMR2 = CanInitObject[initObject].CanInitCIDMR2;
 721  0146 ce0156            	ldx	#342
 722  0149 ec87              	ldd	OFST+0,s
 723  014b cd0016            	ldy	#22
 724  014e 13                	emul	
 725  014f b756              	tfr	x,y
 726  0151 b745              	tfr	d,x
 727  0153 180ae2000c40      	movb	_CanInitObject+12,x,0,y
 728                         ; 2772     CIDMR3 = CanInitObject[initObject].CanInitCIDMR3;
 730  0159 ce0157            	ldx	#343
 731  015c ec87              	ldd	OFST+0,s
 732  015e cd0016            	ldy	#22
 733  0161 13                	emul	
 734  0162 b756              	tfr	x,y
 735  0164 b745              	tfr	d,x
 736  0166 180ae2000d40      	movb	_CanInitObject+13,x,0,y
 737                         ; 2773     CIDAR4 = CanInitObject[initObject].CanInitCIDAR4;
 739  016c ce0158            	ldx	#344
 740  016f ec87              	ldd	OFST+0,s
 741  0171 cd0016            	ldy	#22
 742  0174 13                	emul	
 743  0175 b756              	tfr	x,y
 744  0177 b745              	tfr	d,x
 745  0179 180ae2000e40      	movb	_CanInitObject+14,x,0,y
 746                         ; 2774     CIDAR5 = CanInitObject[initObject].CanInitCIDAR5;
 748  017f ce0159            	ldx	#345
 749  0182 ec87              	ldd	OFST+0,s
 750  0184 cd0016            	ldy	#22
 751  0187 13                	emul	
 752  0188 b756              	tfr	x,y
 753  018a b745              	tfr	d,x
 754  018c 180ae2000f40      	movb	_CanInitObject+15,x,0,y
 755                         ; 2775     CIDAR6 = CanInitObject[initObject].CanInitCIDAR6;
 757  0192 ce015a            	ldx	#346
 758  0195 ec87              	ldd	OFST+0,s
 759  0197 cd0016            	ldy	#22
 760  019a 13                	emul	
 761  019b b756              	tfr	x,y
 762  019d b745              	tfr	d,x
 763  019f 180ae2001040      	movb	_CanInitObject+16,x,0,y
 764                         ; 2776     CIDAR7 = CanInitObject[initObject].CanInitCIDAR7;
 766  01a5 ce015b            	ldx	#347
 767  01a8 ec87              	ldd	OFST+0,s
 768  01aa cd0016            	ldy	#22
 769  01ad 13                	emul	
 770  01ae b756              	tfr	x,y
 771  01b0 b745              	tfr	d,x
 772  01b2 180ae2001140      	movb	_CanInitObject+17,x,0,y
 773                         ; 2777     CIDMR4 = CanInitObject[initObject].CanInitCIDMR4;
 775  01b8 ce015c            	ldx	#348
 776  01bb ec87              	ldd	OFST+0,s
 777  01bd cd0016            	ldy	#22
 778  01c0 13                	emul	
 779  01c1 b756              	tfr	x,y
 780  01c3 b745              	tfr	d,x
 781  01c5 180ae2001240      	movb	_CanInitObject+18,x,0,y
 782                         ; 2778     CIDMR5 = CanInitObject[initObject].CanInitCIDMR5;
 784  01cb ce015d            	ldx	#349
 785  01ce ec87              	ldd	OFST+0,s
 786  01d0 cd0016            	ldy	#22
 787  01d3 13                	emul	
 788  01d4 b756              	tfr	x,y
 789  01d6 b745              	tfr	d,x
 790  01d8 180ae2001340      	movb	_CanInitObject+19,x,0,y
 791                         ; 2779     CIDMR6 = CanInitObject[initObject].CanInitCIDMR6;
 793  01de ce015e            	ldx	#350
 794  01e1 ec87              	ldd	OFST+0,s
 795  01e3 cd0016            	ldy	#22
 796  01e6 13                	emul	
 797  01e7 b756              	tfr	x,y
 798  01e9 b745              	tfr	d,x
 799  01eb 180ae2001440      	movb	_CanInitObject+20,x,0,y
 800                         ; 2780     CIDMR7 = CanInitObject[initObject].CanInitCIDMR7;
 802  01f1 ce015f            	ldx	#351
 803  01f4 ec87              	ldd	OFST+0,s
 804  01f6 cd0016            	ldy	#22
 805  01f9 13                	emul	
 806  01fa b756              	tfr	x,y
 807  01fc b745              	tfr	d,x
 808  01fe 180ae2001540      	movb	_CanInitObject+21,x,0,y
 809                         ; 2784     if(canCanIrqDisabled[channel] == kCanTrue) /* disable CAN interrupts? */
 811  0204 fc0000            	ldd	L14_canCanIrqDisabled
 812  0207 042405            	dbne	d,L572
 813                         ; 2786       canCanInterruptOldStatus[channel].oldCanCTIER = (vuint8)0x00; /* disable all Tx interrupts */
 815  020a 790011            	clr	L52_canCanInterruptOldStatus+1
 817  020d 2003              	bra	L772
 818  020f                   L572:
 819                         ; 2791       CTIER = (vuint8)0x00; /* disable all Tx interrupts */
 822  020f 790147            	clr	327
 823  0212                   L772:
 824                         ; 2884     CTL0 &= (vuint8)~INITRQ;
 826  0212 1d014001          	bclr	320,1
 827                         ; 2885     CTL0 = (vuint8)(CanInitObject[initObject].CanInitCTL0 & ((vuint8)~INITRQ));
 829  0216 ec87              	ldd	OFST+0,s
 830  0218 cd0016            	ldy	#22
 831  021b 13                	emul	
 832  021c b746              	tfr	d,y
 833  021e e6ea0000          	ldab	_CanInitObject,y
 834  0222 c4fe              	andb	#254
 835  0224 7b0140            	stab	320
 836                         ; 2886     APPLCANTIMERSTART(kCanLoopExitInit);
 838  0227                   L303:
 839                         ; 2887     while(((CTL1 & INITAK) == INITAK) && APPLCANTIMERLOOP(kCanLoopExitInit))
 841  0227 1e014101fb        	brset	321,1,L303
 842                         ; 2906       for (hwObjHandle=CAN_HL_HW_TX_STARTINDEX(canHwChannel); hwObjHandle<CAN_HL_HW_TX_STOPINDEX(canHwChannel); hwObjHandle++ )     /*lint !e661*/
 845  022c cd0001            	ldy	#1
 846  022f 6d80              	sty	OFST-7,s
 847  0231                   L703:
 848                         ; 2908         logTxObjHandle = (CanObjectHandle)((vsintx)hwObjHandle + CAN_HL_TX_OFFSET_HW_TO_LOG(canHwChannel));     /*lint !e661*/
 850  0231 03                	dey	
 851  0232 6d84              	sty	OFST-3,s
 852                         ; 2912         if((canStatus[channel] & kCanHwIsInit) == kCanHwIsInit)                    /*lint !e661*/
 854  0234 1f00020411        	brclr	L73_canStatus,4,L513
 855                         ; 2915           txHandle = canHandleCurTxObj[logTxObjHandle];
 857  0239 1858              	lsly	
 858  023b ecea001a          	ldd	L31_canHandleCurTxObj,y
 859  023f 6c82              	std	OFST-5,s
 860                         ; 2918           if( txHandle < kCanNumberOfTxObjects )
 862  0241 8c0006            	cpd	#6
 863  0244 2404              	bhs	L513
 864                         ; 2920             APPLCANCANCELNOTIFICATION(channel, txHandle);
 866  0246 4a000000          	call	f_IlCanCancelNotification
 868  024a                   L513:
 869                         ; 2932         canHandleCurTxObj[logTxObjHandle] = kCanBufferFree;                   /* MsgObj is free */
 871  024a ccffff            	ldd	#-1
 872  024d ed84              	ldy	OFST-3,s
 873  024f 1858              	lsly	
 874  0251 6cea001a          	std	L31_canHandleCurTxObj,y
 875                         ; 2906       for (hwObjHandle=CAN_HL_HW_TX_STARTINDEX(canHwChannel); hwObjHandle<CAN_HL_HW_TX_STOPINDEX(canHwChannel); hwObjHandle++ )     /*lint !e661*/
 877  0255 186280            	incw	OFST-7,s
 880  0258 ed80              	ldy	OFST-7,s
 881  025a 8d0002            	cpy	#2
 882  025d 25d2              	blo	L703
 883                         ; 2949     for (hwObjHandle=CAN_HL_HW_UNUSED_STARTINDEX(canHwChannel); hwObjHandle<CAN_HL_HW_UNUSED_STOPINDEX(canHwChannel); hwObjHandle++ )  /*lint !e661 !e681*/
 885  025f cc0002            	ldd	#2
 886  0262 6c80              	std	OFST-7,s
 887  0264                   L123:
 890  0264 186280            	incw	OFST-7,s
 893  0267 ec80              	ldd	OFST-7,s
 894  0269 8c0004            	cpd	#4
 895  026c 25f6              	blo	L123
 896                         ; 2957     for (hwObjHandle=CAN_HL_HW_RX_BASIC_STARTINDEX(canHwChannel); hwObjHandle<CAN_HL_HW_RX_BASIC_STOPINDEX(canHwChannel); hwObjHandle++ )
 898  026e 186980            	clrw	OFST-7,s
 899  0271                   L723:
 902  0271 186280            	incw	OFST-7,s
 905  0274 27fb              	beq	L723
 906                         ; 2979     tmpCRIER = (vuint8)0x00; /* disable all possible interrupt sources - then enable the used one */
 908  0276 6986              	clr	OFST-1,s
 909                         ; 2984     tmpCRIER |= (vuint8)(CSCIE | TSTAT0E); /* at least we want bus-off notification */
 911                         ; 2995       tmpCRIER |= RXE; /* enable Rx interrupt */
 913                         ; 3002     tmpCRIER |= WUPIE; /* enable wakeup interrupt */
 915  0278 0c86c5            	bset	OFST-1,s,197
 916                         ; 3006     if(canCanIrqDisabled[channel] == kCanTrue) /* disable CAN interrupts? */
 918  027b fc0000            	ldd	L14_canCanIrqDisabled
 919  027e 042407            	dbne	d,L533
 920                         ; 3008       canCanInterruptOldStatus[channel].oldCanCRIER = tmpCRIER;
 922  0281 c6c5              	ldab	#197
 923  0283 7b0010            	stab	L52_canCanInterruptOldStatus
 925  0286 2005              	bra	L733
 926  0288                   L533:
 927                         ; 3012       CRIER = tmpCRIER;
 929  0288 c6c5              	ldab	#197
 930  028a 7b0145            	stab	325
 931  028d                   L733:
 932                         ; 3036 } /* END OF CanInit */
 935  028d 1b89              	leas	9,s
 936  028f 0a                	rtc	
 988                         ; 3051 C_API_1 void C_API_2 CanInitPowerOn( void )
 988                         ; 3052 { 
 989                         	switch	.ftext
 990  0290                   f_CanInitPowerOn:
 992  0290 3b                	pshd	
 993       00000002          OFST:	set	2
 996                         ; 3066   VStdInitPowerOn();
 998  0291 4a000000          	call	f_VStdInitPowerOn
1000                         ; 3078   for (txHandle = 0; txHandle < kCanNumberOfTxDynObjects; txHandle++)
1002  0295 18c7              	clry	
1003  0297                   L753:
1004                         ; 3081     canTxDynObjReservedFlag[txHandle] = 0;
1006  0297 69ea0019          	clr	L51_canTxDynObjReservedFlag,y
1007                         ; 3078   for (txHandle = 0; txHandle < kCanNumberOfTxDynObjects; txHandle++)
1009  029b 02                	iny	
1012  029c 27f9              	beq	L753
1013  029e 6d80              	sty	OFST-2,s
1014                         ; 3094   for (txHandle = 0; txHandle < kCanNumberOfIndBytes; txHandle++) {
1016  02a0 18c7              	clry	
1017  02a2                   L563:
1018                         ; 3095     CanIndicationFlags._c[txHandle] = 0;
1020  02a2 69ea0022          	clr	_CanIndicationFlags,y
1021                         ; 3094   for (txHandle = 0; txHandle < kCanNumberOfIndBytes; txHandle++) {
1023  02a6 02                	iny	
1026  02a7 27f9              	beq	L563
1027  02a9 6d80              	sty	OFST-2,s
1028                         ; 3100   for (txHandle = 0; txHandle < kCanNumberOfConfBytes; txHandle++) {
1030  02ab 18c7              	clry	
1031  02ad                   L373:
1032                         ; 3101     CanConfirmationFlags._c[txHandle] = 0;
1034  02ad 69ea0023          	clr	_CanConfirmationFlags,y
1035                         ; 3100   for (txHandle = 0; txHandle < kCanNumberOfConfBytes; txHandle++) {
1037  02b1 02                	iny	
1040  02b2 27f9              	beq	L373
1041                         ; 3116     for(ch = 0; ch < kCanNumberOfChannels; ch++)
1043  02b4 18c7              	clry	
1044  02b6 6d80              	sty	OFST-2,s
1045  02b8                   L104:
1046                         ; 3118       canCanIrqDisabled[ch] = kCanFalse;
1048  02b8 ed80              	ldy	OFST-2,s
1049  02ba 1858              	lsly	
1050  02bc 1869ea0000        	clrw	L14_canCanIrqDisabled,y
1051                         ; 3127       canRDSRxPtr[ch] = (vuint8*)&(CAN_CNTRL_BASIS_ADR(ch) -> CanRxBuf.DataFld[0]);
1053  02c1 cc0164            	ldd	#356
1054  02c4 6cea001e          	std	_canRDSRxPtr,y
1055                         ; 3116     for(ch = 0; ch < kCanNumberOfChannels; ch++)
1057  02c8 186280            	incw	OFST-2,s
1060  02cb 27eb              	beq	L104
1061                         ; 3149     canStatus[channel]              = kCanStatusInit;
1063  02cd 87                	clra	
1064  02ce 7a0002            	staa	L73_canStatus
1065                         ; 3156     canRxInfoStruct[channel].Channel  = channel;
1067  02d1 c7                	clrb	
1068  02d2 7c0006            	std	L13_canRxInfoStruct
1069  02d5 7c0012            	std	L32_canCanInterruptCounter
1070                         ; 3158     canCanInterruptCounter[channel] = 0; 
1072                         ; 3171     confirmHandle[channel] = kCanBufferFree;
1074  02d8 ccffff            	ldd	#-1
1075  02db 7c0020            	std	_confirmHandle
1076                         ; 3195     canTxPartOffline[channel]       = kCanTxPartInit;
1078  02de 790003            	clr	L53_canTxPartOffline
1079                         ; 3201     canRxHandle[channel] = kCanRxHandleNotUsed;
1081  02e1 7c0024            	std	_canRxHandle
1082                         ; 3206       CanDelQueuedObj( CAN_CHANNEL_CANPARA_ONLY );
1084  02e4 4a02f4f4          	call	L55f_CanDelQueuedObj
1086                         ; 3209       CanInit( CAN_CHANNEL_CANPARA_FIRST 0 );
1088  02e8 87                	clra	
1089  02e9 c7                	clrb	
1090  02ea 4a007a7a          	call	f_CanInit
1092                         ; 3212       canStatus[channel]              |= (kCanHwIsInit | kCanTxOn);
1094  02ee 1c000205          	bset	L73_canStatus,5
1095                         ; 3217 } /* END OF CanInitPowerOn */
1098  02f2 31                	puly	
1099  02f3 0a                	rtc	
1157                         ; 3231 static void CanDelQueuedObj( CAN_CHANNEL_CANTYPE_ONLY )
1157                         ; 3232 { 
1158                         	switch	.ftext
1159  02f4                   L55f_CanDelQueuedObj:
1161  02f4 1b98              	leas	-8,s
1162       00000008          OFST:	set	8
1165                         ; 3245   if((canStatus[channel] & kCanHwIsInit) == kCanHwIsInit)
1167  02f6 1f00020452        	brclr	L73_canStatus,4,L134
1168                         ; 3247     CAN_CAN_INTERRUPT_DISABLE(CAN_CHANNEL_CANPARA_ONLY);        /* avoid interruption by CanHL_TxConfirmation */
1170  02fb 4a0a0909          	call	f_CanCanInterruptDisable
1172                         ; 3248     for(queueElementIdx = CAN_HL_TXQUEUE_STARTINDEX(channel); queueElementIdx < CAN_HL_TXQUEUE_STOPINDEX(channel); queueElementIdx++)
1174  02ff 18c7              	clry	
1175  0301 6d80              	sty	OFST-8,s
1176  0303                   L334:
1177                         ; 3250       elem = canTxQueueFlags[queueElementIdx];
1179  0303 ed80              	ldy	OFST-8,s
1180  0305 1858              	lsly	
1181  0307 ecea0004          	ldd	L33_canTxQueueFlags,y
1182  030b 6c84              	std	OFST-4,s
1183                         ; 3251       if(elem != (tCanQueueElementType)0) /* is there any flag set in the queue element? */
1185  030d 272e              	beq	L144
1186                         ; 3254         for(elementBitIdx = ((CanSignedTxHandle)1 << kCanTxQueueShift) - (CanSignedTxHandle)1; elementBitIdx >= (CanSignedTxHandle)0; elementBitIdx--)
1188  030f cc000f            	ldd	#15
1189  0312 6c82              	std	OFST-6,s
1190  0314                   L344:
1191                         ; 3256           if( ( elem & CanShiftLookUp[elementBitIdx] ) != (tCanQueueElementType)0)
1193  0314 ed82              	ldy	OFST-6,s
1194  0316 1858              	lsly	
1195  0318 eeea0000          	ldx	L3_CanShiftLookUp,y
1196  031c 18a484            	andx	OFST-4,s
1197  031f 270e              	beq	L154
1198                         ; 3258             txHandle = (((CanTransmitHandle)queueElementIdx << kCanTxQueueShift) + (CanTransmitHandle)elementBitIdx) - CAN_HL_TXQUEUE_PADBITS(channel);
1200  0321 ec80              	ldd	OFST-8,s
1201  0323 59                	lsld	
1202  0324 59                	lsld	
1203  0325 59                	lsld	
1204  0326 59                	lsld	
1205  0327 e382              	addd	OFST-6,s
1206  0329 6c86              	std	OFST-2,s
1207                         ; 3259             APPLCANCANCELNOTIFICATION(channel, txHandle);
1209  032b 4a000000          	call	f_IlCanCancelNotification
1211  032f                   L154:
1212                         ; 3254         for(elementBitIdx = ((CanSignedTxHandle)1 << kCanTxQueueShift) - (CanSignedTxHandle)1; elementBitIdx >= (CanSignedTxHandle)0; elementBitIdx--)
1214  032f 186382            	decw	OFST-6,s
1217  0332 2ce0              	bge	L344
1218                         ; 3262         canTxQueueFlags[queueElementIdx] = (tCanQueueElementType)0;
1220  0334 ed80              	ldy	OFST-8,s
1221  0336 1858              	lsly	
1222  0338 1869ea0004        	clrw	L33_canTxQueueFlags,y
1223  033d                   L144:
1224                         ; 3248     for(queueElementIdx = CAN_HL_TXQUEUE_STARTINDEX(channel); queueElementIdx < CAN_HL_TXQUEUE_STOPINDEX(channel); queueElementIdx++)
1226  033d 186280            	incw	OFST-8,s
1229  0340 ec80              	ldd	OFST-8,s
1230  0342 8c0001            	cpd	#1
1231  0345 2dbc              	blt	L334
1232                         ; 3265     CAN_CAN_INTERRUPT_RESTORE(CAN_CHANNEL_CANPARA_ONLY);
1234  0347 4a0a2a2a          	call	f_CanCanInterruptRestore
1237  034b 2015              	bra	L354
1238  034d                   L134:
1239                         ; 3270     for(queueElementIdx = CAN_HL_TXQUEUE_STARTINDEX(channel); queueElementIdx < CAN_HL_TXQUEUE_STOPINDEX(channel); queueElementIdx++)
1241  034d 18c7              	clry	
1242  034f 6d80              	sty	OFST-8,s
1243  0351                   L554:
1244                         ; 3272       canTxQueueFlags[queueElementIdx] = (tCanQueueElementType)0;
1246  0351 1858              	lsly	
1247  0353 1869ea0004        	clrw	L33_canTxQueueFlags,y
1248                         ; 3270     for(queueElementIdx = CAN_HL_TXQUEUE_STARTINDEX(channel); queueElementIdx < CAN_HL_TXQUEUE_STOPINDEX(channel); queueElementIdx++)
1250  0358 186280            	incw	OFST-8,s
1253  035b ed80              	ldy	OFST-8,s
1254  035d 8d0001            	cpy	#1
1255  0360 2def              	blt	L554
1256  0362                   L354:
1257                         ; 3289 }
1260  0362 1b88              	leas	8,s
1261  0364 0a                	rtc	
1334                         ; 3304 C_API_1 void C_API_2 CanCancelTransmit( CanTransmitHandle txHandle )
1334                         ; 3305 {
1335                         	switch	.ftext
1336  0365                   f_CanCancelTransmit:
1338  0365 3b                	pshd	
1339  0366 1b99              	leas	-7,s
1340       00000007          OFST:	set	7
1343                         ; 3320   if (txHandle < kCanNumberOfTxObjects)         /* legal txHandle ? */
1345  0368 8c0006            	cpd	#6
1346  036b 2463              	bhs	L115
1347                         ; 3330     CanNestedGlobalInterruptDisable();
1349  036d b774              	tfr	s,d
1350  036f c30006            	addd	#-1+OFST
1351  0372 4a000000          	call	f_VStdLL_GlobalInterruptDisable
1353                         ; 3343     queueBitPos  = txHandle + CAN_HL_TXQUEUE_PADBITS(channel);
1355  0376 ed87              	ldy	OFST+0,s
1356                         ; 3344     queueElementIdx = (CanSignedTxHandle)(queueBitPos >> kCanTxQueueShift); /* get the queue element where to set the flag */
1358  0378 b764              	tfr	y,d
1359  037a 49                	lsrd	
1360  037b 49                	lsrd	
1361  037c 49                	lsrd	
1362  037d 49                	lsrd	
1363  037e 6c84              	std	OFST-3,s
1364                         ; 3345     elementBitIdx  = (CanSignedTxHandle)(queueBitPos & kCanTxQueueMask);   /* get the flag index wihtin the queue element */
1366  0380 b764              	tfr	y,d
1367  0382 c40f              	andb	#15
1368  0384 87                	clra	
1369                         ; 3346     if( (canTxQueueFlags[queueElementIdx] & CanShiftLookUp[elementBitIdx]) != (tCanQueueElementType)0 )
1371  0385 b746              	tfr	d,y
1372  0387 6d80              	sty	OFST-7,s
1373  0389 1858              	lsly	
1374  038b eeea0000          	ldx	L3_CanShiftLookUp,y
1375  038f ed84              	ldy	OFST-3,s
1376  0391 1858              	lsly	
1377  0393 18a4ea0004        	andx	L33_canTxQueueFlags,y
1378  0398 271c              	beq	L315
1379                         ; 3348       canTxQueueFlags[queueElementIdx] &= ~CanShiftLookUp[elementBitIdx]; /* clear flag from the queue */
1381  039a ed84              	ldy	OFST-3,s
1382  039c 1858              	lsly	
1383  039e 59                	lsld	
1384  039f b745              	tfr	d,x
1385  03a1 eee20000          	ldx	L3_CanShiftLookUp,x
1386  03a5 1841              	comx	
1387  03a7 18a4ea0004        	andx	L33_canTxQueueFlags,y
1388  03ac 6eea0004          	stx	L33_canTxQueueFlags,y
1389                         ; 3349       APPLCANCANCELNOTIFICATION(channel, txHandle);
1391  03b0 ec87              	ldd	OFST+0,s
1392  03b2 4a000000          	call	f_IlCanCancelNotification
1394  03b6                   L315:
1395                         ; 3353     logTxObjHandle = (CanObjectHandle)((vsintx)CAN_HL_HW_TX_NORMAL_INDEX(canHwChannel) + CAN_HL_TX_OFFSET_HW_TO_LOG(canHwChannel));
1397                         ; 3354     if (canHandleCurTxObj[logTxObjHandle] == txHandle)
1399  03b6 fc001a            	ldd	L31_canHandleCurTxObj
1400  03b9 ac87              	cpd	OFST+0,s
1401  03bb 260c              	bne	L515
1402                         ; 3356       canHandleCurTxObj[logTxObjHandle] = kCanBufferCancel;
1404  03bd ccfffe            	ldd	#-2
1405  03c0 7c001a            	std	L31_canHandleCurTxObj
1406                         ; 3367       APPLCANCANCELNOTIFICATION(channel, txHandle);
1408  03c3 ec87              	ldd	OFST+0,s
1409  03c5 4a000000          	call	f_IlCanCancelNotification
1411  03c9                   L515:
1412                         ; 3370     CanNestedGlobalInterruptRestore();
1414  03c9 e686              	ldab	OFST-1,s
1415  03cb 87                	clra	
1416  03cc 4a000000          	call	f_VStdLL_GlobalInterruptRestore
1418  03d0                   L115:
1419                         ; 3372 }
1422  03d0 1b89              	leas	9,s
1423  03d2 0a                	rtc	
1514                         ; 3463 C_API_1 vuint8 C_API_2 CanTransmit(CanTransmitHandle txHandle)  C_API_3   /*lint !e14 !e31*/
1514                         ; 3464 {
1515                         	switch	.ftext
1516  03d3                   f_CanTransmit:
1518  03d3 3b                	pshd	
1519  03d4 1b9a              	leas	-6,s
1520       00000006          OFST:	set	6
1523                         ; 3513   if ( (canStatus[channel] & kCanTxOn) != kCanTxOn )           /* transmit path switched off */
1526  03d6 1e00020103        	brset	L73_canStatus,1,L155
1527                         ; 3515     return kCanTxFailed;
1529  03db c7                	clrb	
1531  03dc 200d              	bra	L63
1532  03de                   L155:
1533                         ; 3519   if ( (canTxPartOffline[channel] & CanTxSendMask[txHandle]) != (vuint8)0)   /*lint !e661*/ /* CAN off ? */
1535  03de ed86              	ldy	OFST+0,s
1536  03e0 e6ea0000          	ldab	_CanTxSendMask,y
1537  03e4 f40003            	andb	L53_canTxPartOffline
1538  03e7 2705              	beq	L355
1539                         ; 3521     return (kCanTxPartOffline);
1541  03e9 c602              	ldab	#2
1543  03eb                   L63:
1545  03eb 1b88              	leas	8,s
1546  03ed 0a                	rtc	
1547  03ee                   L355:
1548                         ; 3596   txObjHandle = CAN_HL_HW_TX_NORMAL_INDEX(canHwChannel);                                          /* msg in object 0 */
1552  03ee cc0001            	ldd	#1
1553  03f1 6c80              	std	OFST-6,s
1554                         ; 3598   logTxObjHandle = (CanObjectHandle)((vsintx)txObjHandle + CAN_HL_TX_OFFSET_HW_TO_LOG(canHwChannel));
1556  03f3 186982            	clrw	OFST-4,s
1557                         ; 3601   CanNestedGlobalInterruptDisable();
1559  03f6 b774              	tfr	s,d
1560  03f8 c30004            	addd	#-2+OFST
1561  03fb 4a000000          	call	f_VStdLL_GlobalInterruptDisable
1563                         ; 3604   if ( (canStatus[channel] & kCanTxOn) != kCanTxOn )                /* transmit path switched off */
1565  03ff 1e0002010a        	brset	L73_canStatus,1,L555
1566                         ; 3606     CanNestedGlobalInterruptRestore();
1568  0404 e684              	ldab	OFST-2,s
1569  0406 87                	clra	
1570  0407 4a000000          	call	f_VStdLL_GlobalInterruptRestore
1572                         ; 3607     return kCanTxFailed;
1574  040b c7                	clrb	
1576  040c 20dd              	bra	L63
1577  040e                   L555:
1578                         ; 3613   if (
1578                         ; 3614 
1578                         ; 3615 #   if defined( C_ENABLE_TX_POLLING )
1578                         ; 3616 #    if defined( C_ENABLE_INDIVIDUAL_POLLING )
1578                         ; 3617        ( ( CanHwObjIndivPolling[CAN_HWOBJINDIVPOLLING_INDEX0][txObjHandle] != (vuint8)0 )       /* Object is used in polling mode! */
1578                         ; 3618                          || (canHandleCurTxObj[logTxObjHandle] != kCanBufferFree) )    /* MsgObj used?  */
1578                         ; 3619 #    else
1578                         ; 3620         /* write always to queue; transmission is started out of TxTask */
1578                         ; 3621 #    endif
1578                         ; 3622 #   else
1578                         ; 3623        ( canHandleCurTxObj[logTxObjHandle] != kCanBufferFree)    /* MsgObj used?  */
1578                         ; 3624 #   endif
1578                         ; 3625      )
1580  040e ed82              	ldy	OFST-4,s
1581  0410 1858              	lsly	
1582  0412 ecea001a          	ldd	L31_canHandleCurTxObj,y
1583  0416 048430            	ibeq	d,L755
1584                         ; 3630       queueBitPos  = txHandle + CAN_HL_TXQUEUE_PADBITS(channel);
1586  0419 ee86              	ldx	OFST+0,s
1587                         ; 3631       queueElementIdx = (CanSignedTxHandle)(queueBitPos >> kCanTxQueueShift); /* get the queue element where to set the flag */
1589  041b b754              	tfr	x,d
1590  041d 49                	lsrd	
1591  041e 49                	lsrd	
1592  041f 49                	lsrd	
1593  0420 49                	lsrd	
1594  0421 6c82              	std	OFST-4,s
1595                         ; 3632       elementBitIdx  = (CanSignedTxHandle)(queueBitPos & kCanTxQueueMask);   /* get the flag index wihtin the queue element */
1597  0423 b754              	tfr	x,d
1598  0425 c40f              	andb	#15
1599  0427 87                	clra	
1600  0428 6c80              	std	OFST-6,s
1601                         ; 3633       canTxQueueFlags[queueElementIdx] |= CanShiftLookUp[elementBitIdx];
1603  042a ed82              	ldy	OFST-4,s
1604  042c 1858              	lsly	
1605  042e b745              	tfr	d,x
1606  0430 1848              	lslx	
1607  0432 eee20000          	ldx	L3_CanShiftLookUp,x
1608  0436 18aaea0004        	orx	L33_canTxQueueFlags,y
1609  043b 6eea0004          	stx	L33_canTxQueueFlags,y
1610                         ; 3634       CanNestedGlobalInterruptRestore();
1612  043f e684              	ldab	OFST-2,s
1613  0441 4a000000          	call	f_VStdLL_GlobalInterruptRestore
1615                         ; 3635       return kCanTxOk;                          
1617  0445 c601              	ldab	#1
1619  0447 20a2              	bra	L63
1620  0449                   L755:
1621                         ; 3650     if (( canHandleCurTxObj[logTxObjHandle] != kCanBufferFree)    /* MsgObj used?  */
1621                         ; 3651        || ( !CanLL_TxIsHWObjFree( canHwChannel, txObjHandle ) )
1621                         ; 3652 
1621                         ; 3653       /* hareware-txObject is not free --------------------------------------*/
1621                         ; 3654        )  /* end of if question */
1623  0449 ed82              	ldy	OFST-4,s
1624  044b 1858              	lsly	
1625  044d ecea001a          	ldd	L31_canHandleCurTxObj,y
1626  0451 04a40b            	ibne	d,L565
1628  0454 ed80              	ldy	OFST-6,s
1629  0456 e6ea0033          	ldab	L7_CanMailboxSelect,y
1630  045a f40146            	andb	326
1631  045d 260a              	bne	L165
1632  045f                   L565:
1633                         ; 3658       CanNestedGlobalInterruptRestore();
1635  045f e684              	ldab	OFST-2,s
1636  0461 87                	clra	
1637  0462 4a000000          	call	f_VStdLL_GlobalInterruptRestore
1639                         ; 3659       return kCanTxFailed;
1641  0466 c7                	clrb	
1643  0467 2026              	bra	L04
1644  0469                   L165:
1645                         ; 3664   canHandleCurTxObj[logTxObjHandle] = txHandle;/* Save hdl of msgObj to be transmitted*/
1647  0469 ed82              	ldy	OFST-4,s
1648  046b 1858              	lsly	
1649  046d 180286ea001a      	movw	OFST+0,s,L31_canHandleCurTxObj,y
1650                         ; 3665   CanNestedGlobalInterruptRestore();
1652  0473 e684              	ldab	OFST-2,s
1653  0475 87                	clra	
1654  0476 4a000000          	call	f_VStdLL_GlobalInterruptRestore
1656                         ; 3667   rc = CanCopyDataAndStartTransmission( CAN_CHANNEL_CANPARA_FIRST txObjHandle, txHandle);
1658  047a ec86              	ldd	OFST+0,s
1659  047c 3b                	pshd	
1660  047d ec82              	ldd	OFST-4,s
1661  047f 4a049292          	call	L35f_CanCopyDataAndStartTransmission
1663  0483 1b82              	leas	2,s
1664  0485 6b85              	stab	OFST-1,s
1665                         ; 3670   if ( rc == kCanTxNotify)
1667  0487 c105              	cmpb	#5
1668  0489 2602              	bne	L765
1669                         ; 3672     rc = kCanTxFailed;      /* ignore notification if calls of CanCopy.. is performed within CanTransmit */
1671  048b 6985              	clr	OFST-1,s
1672  048d                   L765:
1673                         ; 3677   return(rc);
1675  048d e685              	ldab	OFST-1,s
1677  048f                   L04:
1679  048f 1b88              	leas	8,s
1680  0491 0a                	rtc	
1797                         ; 3701 static vuint8 CanCopyDataAndStartTransmission( CAN_CHANNEL_CANTYPE_FIRST CanObjectHandle txObjHandle, CanTransmitHandle txHandle) C_API_3   /*lint !e14 !e31*/
1797                         ; 3702 {
1798                         	switch	.ftext
1799  0492                   L35f_CanCopyDataAndStartTransmission:
1801  0492 3b                	pshd	
1802  0493 1b96              	leas	-10,s
1803       0000000a          OFST:	set	10
1806                         ; 3734    if (txHandle >= CAN_HL_TX_DYN_ROM_STARTINDEX(channel))
1809  0495 ed8f              	ldy	OFST+5,s
1810  0497 8d0005            	cpy	#5
1811  049a 2506              	blo	L536
1812                         ; 3736      dynTxObj = (txHandle - CAN_HL_TX_DYN_ROM_STARTINDEX(channel)) + CAN_HL_TX_DYN_RAM_STARTINDEX(channel);    /* PRQA S 3382,0291 */
1814  049c 195b              	leay	-5,y
1815  049e 6d82              	sty	OFST-8,s
1817  04a0 2003              	bra	L736
1818  04a2                   L536:
1819                         ; 3740      dynTxObj = (CanTransmitHandle)0;
1821  04a2 186982            	clrw	OFST-8,s
1822  04a5                   L736:
1823                         ; 3745    logTxObjHandle = (CanObjectHandle)((vsintx)txObjHandle + CAN_HL_TX_OFFSET_HW_TO_LOG(canHwChannel));
1826  04a5 ed8a              	ldy	OFST+0,s
1827  04a7 03                	dey	
1828  04a8 6d88              	sty	OFST-2,s
1829                         ; 3751    CTBSEL = CanMailboxSelect[txObjHandle]; /* select the appropriate Tx buffer */
1833  04aa cd014a            	ldy	#330
1834  04ad ee8a              	ldx	OFST+0,s
1835  04af 180ae2003340      	movb	L7_CanMailboxSelect,x,0,y
1836                         ; 3760      if (txHandle >= CAN_HL_TX_DYN_ROM_STARTINDEX(channel))
1839  04b5 ec8f              	ldd	OFST+5,s
1840  04b7 8c0005            	cpd	#5
1841  04ba cd0170            	ldy	#368
1842  04bd 2523              	blo	L146
1843                         ; 3764         CAN_TX_MAILBOX_BASIS_ADR(channel) -> Id   = canDynTxId0[dynTxObj];
1845  04bf ce0015            	ldx	#L71_canDynTxId0
1846  04c2 ec82              	ldd	OFST-8,s
1847  04c4 59                	lsld	
1848  04c5 59                	lsld	
1849  04c6 1ae6              	leax	d,x
1850  04c8 18020040          	movw	0,x,0,y
1851  04cc 18020242          	movw	2,x,2,y
1852                         ; 3765         CAN_TX_MAILBOX_BASIS_ADR(channel) -> Prio = TX1; /* Prio Std Tx > Prio LowLevel Tx */
1854  04d0 c602              	ldab	#2
1855  04d2 7b017d            	stab	381
1856                         ; 3773         CAN_TX_MAILBOX_BASIS_ADR(channel) -> Dlc = (canDynTxDLC[dynTxObj]);
1858  04d5 cd017c            	ldy	#380
1859  04d8 ee82              	ldx	OFST-8,s
1860  04da 180ae2001440      	movb	L12_canDynTxDLC,x,0,y
1862  04e0 201f              	bra	L346
1863  04e2                   L146:
1864                         ; 3805         CAN_TX_MAILBOX_BASIS_ADR(channel) -> Id   = CanGetTxId0(txHandle);
1866  04e2 ce0000            	ldx	#_CanTxId0
1867  04e5 59                	lsld	
1868  04e6 59                	lsld	
1869  04e7 1ae6              	leax	d,x
1870  04e9 18020040          	movw	0,x,0,y
1871  04ed 18020242          	movw	2,x,2,y
1872                         ; 3806         CAN_TX_MAILBOX_BASIS_ADR(channel) -> Prio = TX1; /* Prio Std Tx > Prio LowLevel Tx */
1874  04f1 c602              	ldab	#2
1875  04f3 7b017d            	stab	381
1876                         ; 3823         CAN_TX_MAILBOX_BASIS_ADR(channel) -> Dlc = CanGetTxDlc(txHandle);
1878  04f6 cd017c            	ldy	#380
1879  04f9 ee8f              	ldx	OFST+5,s
1880  04fb 180ae2000040      	movb	_CanTxDLC,x,0,y
1881  0501                   L346:
1882                         ; 3944      CanMemCopySrcPtr = CanGetTxDataPtr(txHandle);  
1884  0501 ed8f              	ldy	OFST+5,s
1885  0503 1858              	lsly	
1886  0505 ecea0000          	ldd	_CanTxDataPtr,y
1887  0509 6c84              	std	OFST-6,s
1888                         ; 3947    if ( CanMemCopySrcPtr != V_NULL )   /* copy if buffer exists */
1890  050b 2729              	beq	L546
1891                         ; 3950      CanNestedGlobalInterruptDisable();  
1893  050d b774              	tfr	s,d
1894  050f c30006            	addd	#-4+OFST
1895  0512 4a000000          	call	f_VStdLL_GlobalInterruptDisable
1897                         ; 3954      pDestData = (vuint8*)&(CAN_TX_MAILBOX_BASIS_ADR(channel) -> DataFld[0]);
1899  0516 cd0174            	ldy	#372
1900  0519 6d82              	sty	OFST-8,s
1901                         ; 3958      pSrcData  = (vuint8 *)CanMemCopySrcPtr;
1903  051b ee84              	ldx	OFST-6,s
1904  051d 6e84              	stx	OFST-6,s
1905                         ; 3960      for(i = 7; i >= 0; i--)
1907  051f cc0007            	ldd	#7
1908  0522 6c80              	std	OFST-10,s
1909  0524                   L746:
1910                         ; 3962        pDestData[i] = pSrcData[i];
1912  0524 ec80              	ldd	OFST-10,s
1913  0526 180ae6ee          	movb	d,x,d,y
1914                         ; 3960      for(i = 7; i >= 0; i--)
1916  052a 186380            	decw	OFST-10,s
1919  052d 2cf5              	bge	L746
1920                         ; 3968      CanNestedGlobalInterruptRestore();  
1922  052f e686              	ldab	OFST-4,s
1923  0531 87                	clra	
1924  0532 4a000000          	call	f_VStdLL_GlobalInterruptRestore
1926  0536                   L546:
1927                         ; 3981    CanNestedGlobalInterruptDisable();  
1930  0536 b774              	tfr	s,d
1931  0538 c30006            	addd	#-4+OFST
1932  053b 4a000000          	call	f_VStdLL_GlobalInterruptDisable
1934                         ; 3984    if ((canHandleCurTxObj[logTxObjHandle] == txHandle) && ((canStatus[channel] & kCanTxOn) == kCanTxOn))
1936  053f ed88              	ldy	OFST-2,s
1937  0541 1858              	lsly	
1938  0543 ecea001a          	ldd	L31_canHandleCurTxObj,y
1939  0547 ac8f              	cpd	OFST+5,s
1940  0549 2639              	bne	L556
1942  054b 1f00020134        	brclr	L73_canStatus,1,L556
1943                         ; 3997      CANTFLG = CanMailboxSelect[txObjHandle]; /* transmit message    */
1946  0550 cd0146            	ldy	#326
1947  0553 ee8a              	ldx	OFST+0,s
1948  0555 180ae2003340      	movb	L7_CanMailboxSelect,x,0,y
1949                         ; 3998      CTBSEL = TX2; /* ESCAN00022867, the assignment is relevant if normal transmission interrupts low-level transmission */
1951  055b c604              	ldab	#4
1952  055d 7b014a            	stab	330
1953                         ; 4005        if(canCanIrqDisabled[channel] == kCanTrue)
1955  0560 fc0000            	ldd	L14_canCanIrqDisabled
1956  0563 ed8a              	ldy	OFST+0,s
1957  0565 04240c            	dbne	d,L756
1958                         ; 4007          canCanInterruptOldStatus[channel].oldCanCTIER |= CanMailboxSelect[txObjHandle]; /* enable Tx interrupt */
1960  0568 e6ea0033          	ldab	L7_CanMailboxSelect,y
1961  056c fa0011            	orab	L52_canCanInterruptOldStatus+1
1962  056f 7b0011            	stab	L52_canCanInterruptOldStatus+1
1964  0572 200a              	bra	L166
1965  0574                   L756:
1966                         ; 4011          CTIER |= CanMailboxSelect[txObjHandle]; /* enable Tx interrupt */
1968  0574 e6ea0033          	ldab	L7_CanMailboxSelect,y
1969  0578 fa0147            	orab	327
1970  057b 7b0147            	stab	327
1971  057e                   L166:
1972                         ; 4022      rc = kCanTxOk;                                    
1975  057e c601              	ldab	#1
1976  0580 6b87              	stab	OFST-3,s
1978  0582 201f              	bra	L366
1979  0584                   L556:
1980                         ; 4027      if (canHandleCurTxObj[logTxObjHandle] == txHandle)
1982  0584 ed88              	ldy	OFST-2,s
1983  0586 1858              	lsly	
1984  0588 ecea001a          	ldd	L31_canHandleCurTxObj,y
1985  058c ac8f              	cpd	OFST+5,s
1986  058e 2606              	bne	L566
1987                         ; 4030        rc = kCanTxNotify;
1989  0590 c605              	ldab	#5
1990  0592 6b87              	stab	OFST-3,s
1992  0594 2002              	bra	L766
1993  0596                   L566:
1994                         ; 4035        rc = kCanTxFailed;   
1996  0596 6987              	clr	OFST-3,s
1997  0598                   L766:
1998                         ; 4039      canHandleCurTxObj[logTxObjHandle] = kCanBufferFree;  /* release TxHandle (CanOffline) */
2001  0598 ccffff            	ldd	#-1
2002  059b ed88              	ldy	OFST-2,s
2003  059d 1858              	lsly	
2004  059f 6cea001a          	std	L31_canHandleCurTxObj,y
2005  05a3                   L366:
2006                         ; 4042    CanNestedGlobalInterruptRestore();
2008  05a3 e686              	ldab	OFST-4,s
2009  05a5 87                	clra	
2010  05a6 4a000000          	call	f_VStdLL_GlobalInterruptRestore
2012                         ; 4045    return (rc);   
2014  05aa e687              	ldab	OFST-3,s
2017  05ac 1b8c              	leas	12,s
2018  05ae 0a                	rtc	
2042                         ; 4439 static void CanHL_ErrorHandling( CAN_HW_CHANNEL_CANTYPE_ONLY )
2042                         ; 4440 {
2043                         	switch	.ftext
2044  05af                   L75f_CanHL_ErrorHandling:
2048                         ; 4450   if( ((CRFLG & BOFFIF) == BOFFIF) && ((CRFLG & CSCIF) == CSCIF) )
2050  05af 1e01440c02        	brset	324,12,LC003
2051  05b4 2009              	bra	L107
2052  05b6                   LC003:
2054  05b6 1f01444004        	brclr	324,64,L107
2055                         ; 4453     APPL_CAN_BUSOFF( CAN_CHANNEL_CANPARA_ONLY );            /* call application specific function */
2057  05bb 4a000000          	call	f_NmCanError
2059  05bf                   L107:
2060                         ; 4465   CRFLG = (OVRIF | CSCIF); /* clear overrun flag and status changed */
2062  05bf c642              	ldab	#66
2063  05c1 7b0144            	stab	324
2064                         ; 4467 } /* END OF CanHL_ErrorHandling */
2067  05c4 0a                	rtc	
2184                         ; 4616 static void CanBasicCanMsgReceived( CAN_HW_CHANNEL_CANTYPE_FIRST CanObjectHandle rxObjHandle)
2184                         ; 4617 {
2185                         	switch	.ftext
2186  05c5                   L74f_CanBasicCanMsgReceived:
2188  05c5 3b                	pshd	
2189  05c6 1b98              	leas	-8,s
2190       00000008          OFST:	set	8
2193                         ; 4693   rxHandle = kCanRxHandleNotUsed;
2195                         ; 4708   pId = (vuint8*)CAN_RX_MAILBOX_BASIS_ADR(channel);
2198  05c8 cd0160            	ldy	#352
2199  05cb 6d82              	sty	OFST-6,s
2200                         ; 4715   if((pId[1] & (vuint8)0x08) == (vuint8)0x08)
2202  05cd 0f410809          	brclr	1,y,8,L757
2203                         ; 4718     if((pId[3] & (vuint8)0x01) == (vuint8)0x01)
2205  05d1 e643              	ldab	3,y
2206  05d3 c401              	andb	#1
2207  05d5 53                	decb	
2208  05d6 182700b2          	beq	L307
2209                         ; 4720       goto finishBasicCan;
2211  05da                   L757:
2212                         ; 4743   pCanRxInfoStruct =  &canRxInfoStruct[channel];
2214  05da cc0006            	ldd	#L13_canRxInfoStruct
2215  05dd 6c82              	std	OFST-6,s
2216                         ; 4748   (pCanRxInfoStruct->pChipMsgObj) = (CanChipMsgPtr)CAN_RX_MAILBOX_BASIS_ADR(channel);
2218  05df 7d0008            	sty	L13_canRxInfoStruct+2
2219                         ; 4757   (pCanRxInfoStruct->pChipData) = (CanChipDataPtr)&(CAN_RX_MAILBOX_BASIS_ADR(channel) -> DataFld[0]);
2221  05e2 cc0164            	ldd	#356
2222  05e5 7c000a            	std	L13_canRxInfoStruct+4
2223                         ; 4762   canRDSRxPtr[channel] = pCanRxInfoStruct->pChipData;
2225  05e8 7c001e            	std	_canRDSRxPtr
2226                         ; 4784   CAN_HL_P_RX_INFO_STRUCT_HANDLE(channel)      = kCanRxHandleNotUsed;  
2228  05eb ccffff            	ldd	#-1
2229  05ee 7c000c            	std	L13_canRxInfoStruct+6
2230                         ; 4810   if (CanRxActualIdType( CAN_HL_P_RX_INFO_STRUCT(channel) ) != kCanIdTypeExt)
2232  05f1 ee40              	ldx	0,y
2233  05f3 18840018          	andx	#24
2234  05f7 87                	clra	
2235  05f8 c7                	clrb	
2236  05f9 8e0018            	cpx	#24
2237  05fc 1826008c          	bne	L307
2238  0600 6c9e              	std	-2,s
2239  0602 18260086          	bne	L307
2240                         ; 4812     goto finishBasicCan;
2242                         ; 4868     idRaw0 = CanRxActualIdRaw0( CAN_HL_P_RX_INFO_STRUCT(channel) ) & MK_EXTID0(0x1FFFFFFF);
2244  0606 ec42              	ldd	2,y
2245  0608 ee40              	ldx	0,y
2246  060a c4fe              	andb	#254
2247  060c 6c86              	std	OFST-2,s
2248  060e 6e84              	stx	OFST-4,s
2249                         ; 5070         if ( C_RANGE_MATCH_EXT( CAN_RX_IDRAW_PARA, CANRANGE0ACCMASK(channel), CANRANGE0ACCCODE(channel)) )
2251  0610 84fe              	anda	#254
2252  0612 c401              	andb	#1
2253  0614 8ec6dd            	cpx	#-14627
2254  0617 2611              	bne	L567
2255  0619 8c8400            	cpd	#-31744
2256  061c 260c              	bne	L567
2257                         ; 5084             if (APPLCANRANGE0PRECOPY( CAN_HL_P_RX_INFO_STRUCT(channel) ) == kCanNoCopyData)
2259  061e cc0006            	ldd	#L13_canRxInfoStruct
2260  0621 4a000000          	call	f_TpPrecopy
2262  0625 044164            	tbeq	b,L307
2263                         ; 5086               goto finishBasicCan;
2265  0628 ee84              	ldx	OFST-4,s
2266  062a                   L567:
2267                         ; 5116         if ( C_RANGE_MATCH_EXT( CAN_RX_IDRAW_PARA, CANRANGE1ACCMASK(channel), CANRANGE1ACCCODE(channel)) )
2269  062a e687              	ldab	OFST-1,s
2270  062c 1884fffe          	andx	#-2
2271  0630 87                	clra	
2272  0631 c401              	andb	#1
2273  0633 8ec6de            	cpx	#-14626
2274  0636 260c              	bne	L177
2275  0638 046409            	tbne	d,L177
2276                         ; 5130             if (APPLCANRANGE1PRECOPY( CAN_HL_P_RX_INFO_STRUCT(channel) ) == kCanNoCopyData)
2278  063b ec82              	ldd	OFST-6,s
2279  063d 4a000000          	call	f_TpFuncPrecopy
2281  0641 044148            	tbeq	b,L307
2282                         ; 5132               goto finishBasicCan;
2284  0644                   L177:
2285                         ; 5255   for (rxHandle = CAN_HL_RX_BASIC_STARTINDEX(channel); rxHandle < CAN_HL_RX_BASIC_STOPINDEX(channel) ;rxHandle++)
2287  0644 87                	clra	
2288  0645 c7                	clrb	
2289  0646 6c80              	std	OFST-8,s
2290  0648                   L577:
2291                         ; 5257     if( idRaw0 == CanGetRxId0(rxHandle) )
2293  0648 cd0000            	ldy	#_CanRxId0
2294  064b 59                	lsld	
2295  064c 59                	lsld	
2296  064d 19ee              	leay	d,y
2297  064f ec40              	ldd	0,y
2298  0651 ac84              	cpd	OFST-4,s
2299  0653 2606              	bne	LC004
2300  0655 ec42              	ldd	2,y
2301  0657 ac86              	cpd	OFST-2,s
2302  0659 270a              	beq	L1001
2303  065b                   LC004:
2304                         ; 5283                 break;    /*exit loop with index rxHandle */
2306                         ; 5255   for (rxHandle = CAN_HL_RX_BASIC_STARTINDEX(channel); rxHandle < CAN_HL_RX_BASIC_STOPINDEX(channel) ;rxHandle++)
2308  065b 186280            	incw	OFST-8,s
2311  065e ec80              	ldd	OFST-8,s
2312  0660 8c000e            	cpd	#14
2313  0663 25e3              	blo	L577
2314  0665                   L1001:
2315                         ; 5426   if ( rxHandle < CAN_HL_RX_BASIC_STOPINDEX(channel)) 
2317  0665 ed80              	ldy	OFST-8,s
2318  0667 8d000e            	cpy	#14
2319  066a 2420              	bhs	L307
2320                         ; 5433     rxHandle = CanRxMsgIndirection[rxHandle];       /* indirection for special sort-algoritms */
2322  066c 1858              	lsly	
2323  066e ecea0000          	ldd	_CanRxMsgIndirection,y
2324  0672 6c80              	std	OFST-8,s
2325                         ; 5437     CAN_HL_P_RX_INFO_STRUCT_HANDLE(channel) = rxHandle;  
2327  0674 ee82              	ldx	OFST-6,s
2328  0676 6c06              	std	6,x
2329                         ; 5442     if (CanHL_ReceivedRxHandle( CAN_CHANNEL_CANPARA_ONLY ) == kCanHlContinueRx)
2331  0678 4a069494          	call	L34f_CanHL_ReceivedRxHandle
2333  067c 04210d            	dbne	b,L307
2334                         ; 5460       CRFLG = RXF; /* clear receive pending flag */
2336  067f c601              	ldab	#1
2337  0681 7b0144            	stab	324
2338                         ; 5465       CanHL_IndRxHandle( rxHandle );
2340  0684 ec80              	ldd	OFST-8,s
2341  0686 4a072a2a          	call	L54f_CanHL_IndRxHandle
2343                         ; 5468       return;
2345  068a 2005              	bra	L06
2346  068c                   L307:
2347                         ; 5492 finishBasicCan:
2347                         ; 5493 
2347                         ; 5494   /* make receive buffer free*/
2347                         ; 5495   #if defined(C_COMP_COSMIC_MCS12X_MSCAN12)  
2347                         ; 5496   # if defined(C_ENABLE_XGATE_USED)
2347                         ; 5497   #   if defined(C_SINGLE_RECEIVE_CHANNEL)
2347                         ; 5498   index = (kCanBasisAdr >> 6) & 0x07;
2347                         ; 5499   #   else
2347                         ; 5500   index = (CanBasisAdr[channel] >> 6) & 0x07;
2347                         ; 5501   #   endif
2347                         ; 5502   nInfoIndex = CanXGateChannelToInfo[index];
2347                         ; 5503   *CanXGateIrqChannelInfo[nInfoIndex].pXGif = CanXGateIrqChannelInfo[nInfoIndex].nRxMask;
2347                         ; 5504   CanBeginCriticalXGateSection();
2347                         ; 5505   canRxSyncObj[channel] = 0; 
2347                         ; 5506   CanEndCriticalXGateSection();
2347                         ; 5507   # else
2347                         ; 5508   CRFLG = RXF; /* clear receive pending flag */
2349  068c c601              	ldab	#1
2350  068e 7b0144            	stab	324
2351                         ; 5514   return;    /* to avoid compiler warnings about label without code */
2352  0691                   L06:
2355  0691 1b8a              	leas	10,s
2356  0693 0a                	rtc	
2410                         ; 5537 static vuint8 CanHL_ReceivedRxHandle( CAN_CHANNEL_CANTYPE_ONLY )
2410                         ; 5538 {
2411                         	switch	.ftext
2412  0694                   L34f_CanHL_ReceivedRxHandle:
2414  0694 1b9c              	leas	-4,s
2415       00000004          OFST:	set	4
2418                         ; 5557   pCanRxInfoStruct =  &canRxInfoStruct[channel];
2420  0696 cc0006            	ldd	#L13_canRxInfoStruct
2421  0699 6c81              	std	OFST-3,s
2422                         ; 5574   if ( (CanGetRxDataLen(CAN_HL_P_RX_INFO_STRUCT_HANDLE(channel))) > CanRxActualDLC( CAN_HL_P_RX_INFO_STRUCT(channel) ) )
2424  069b fd0008            	ldy	L13_canRxInfoStruct+2
2425  069e e64c              	ldab	12,y
2426  06a0 c40f              	andb	#15
2427  06a2 6b80              	stab	OFST-4,s
2428  06a4 fd000c            	ldy	L13_canRxInfoStruct+6
2429  06a7 e6ea0000          	ldab	_CanRxDataLen,y
2430  06ab e180              	cmpb	OFST-4,s
2431                         ; 5581     return  kCanHlFinishRx;
2434  06ad 220a              	bhi	LC006
2435                         ; 5590   if ( APPL_CAN_GENERIC_PRECOPY( CAN_HL_P_RX_INFO_STRUCT(channel) ) != kCanCopyData)
2437  06af cc0006            	ldd	#L13_canRxInfoStruct
2438  06b2 4a000000          	call	f_IlCanGenericPrecopy
2440  06b6 040104            	dbeq	b,L5301
2441                         ; 5592     return kCanHlFinishRx;  
2443  06b9                   LC006:
2444  06b9 c7                	clrb	
2446  06ba                   L27:
2448  06ba 1b84              	leas	4,s
2449  06bc 0a                	rtc	
2450  06bd                   L5301:
2451                         ; 5599   if ( CanGetApplPrecopyPtr(CAN_HL_P_RX_INFO_STRUCT_HANDLE(channel)) != V_NULL )    /*precopy routine */
2453  06bd ed81              	ldy	OFST-3,s
2454  06bf ee46              	ldx	6,y
2455  06c1 1848              	lslx	
2456  06c3 1848              	lslx	
2457  06c5 ece20000          	ldd	_CanRxApplPrecopyPtr,x
2458  06c9 2606              	bne	LC005
2459  06cb ece20002          	ldd	_CanRxApplPrecopyPtr+2,x
2460  06cf 271c              	beq	L7301
2461  06d1                   LC005:
2462                         ; 5603     canRxHandle[channel] = CAN_HL_P_RX_INFO_STRUCT_HANDLE(channel);
2464  06d1 cd0024            	ldy	#_canRxHandle
2465  06d4 ee81              	ldx	OFST-3,s
2466  06d6 18020640          	movw	6,x,0,y
2467                         ; 5605     if ( CanGetApplPrecopyPtr(CAN_HL_P_RX_INFO_STRUCT_HANDLE(channel))( CAN_HL_P_RX_INFO_STRUCT(channel) )==kCanNoCopyData )
2469  06da ec81              	ldd	OFST-3,s
2470  06dc b746              	tfr	d,y
2471  06de ee46              	ldx	6,y
2472  06e0 1848              	lslx	
2473  06e2 1848              	lslx	
2474  06e4 4be30000          	call	[_CanRxApplPrecopyPtr,x]
2476  06e8 046102            	tbne	b,L7301
2477                         ; 5607       return  kCanHlFinishRx;
2480  06eb 20cc              	bra	LC006
2481  06ed                   L7301:
2482                         ; 5615   if ( CanGetRxDataPtr(CAN_HL_P_RX_INFO_STRUCT_HANDLE(channel)) != V_NULL )      /* copy if buffer exists */
2484  06ed ed81              	ldy	OFST-3,s
2485  06ef ee46              	ldx	6,y
2486  06f1 1848              	lslx	
2487  06f3 ece20000          	ldd	_CanRxDataPtr,x
2488  06f7 272d              	beq	L3401
2489                         ; 5619     CanNestedGlobalInterruptDisable();
2491  06f9 b774              	tfr	s,d
2492  06fb c30003            	addd	#-1+OFST
2493  06fe 4a000000          	call	f_VStdLL_GlobalInterruptDisable
2495                         ; 5651       VStdMemCpyRamToRam(CanGetRxDataPtr(CAN_HL_P_RX_INFO_STRUCT_HANDLE(channel)), pCanRxInfoStruct->pChipData, CanGetRxDataLen(CAN_HL_P_RX_INFO_STRUCT_HANDLE(channel)));
2498  0702 ed81              	ldy	OFST-3,s
2499  0704 ee46              	ldx	6,y
2500  0706 e6e20000          	ldab	_CanRxDataLen,x
2501  070a 87                	clra	
2502  070b 1848              	lslx	
2503  070d ede20000          	ldy	_CanRxDataPtr,x
2504  0711 ee81              	ldx	OFST-3,s
2505  0713 ee04              	ldx	4,x
2506  0715 044407            	tbeq	d,L66
2507  0718                   L07:
2508  0718 180a3070          	movb	1,x+,1,y+
2509  071c 0434f9            	dbne	d,L07
2510  071f                   L66:
2511                         ; 5665     CanNestedGlobalInterruptRestore();
2513  071f e683              	ldab	OFST-1,s
2514  0721 87                	clra	
2515  0722 4a000000          	call	f_VStdLL_GlobalInterruptRestore
2517  0726                   L3401:
2518                         ; 5671   return kCanHlContinueRx;
2521  0726 c601              	ldab	#1
2523  0728 2090              	bra	L27
2568                         ; 5686 static void CanHL_IndRxHandle( CanReceiveHandle rxHandle )
2568                         ; 5687 {
2569                         	switch	.ftext
2570  072a                   L54f_CanHL_IndRxHandle:
2572  072a 3b                	pshd	
2573  072b 37                	pshb	
2574       00000001          OFST:	set	1
2577                         ; 5696   CanNestedGlobalInterruptDisable();
2579  072c b774              	tfr	s,d
2580  072e 4a000000          	call	f_VStdLL_GlobalInterruptDisable
2582                         ; 5698   CanIndicationFlags._c[CanGetIndicationOffset(rxHandle)] |= CanGetIndicationMask(rxHandle);
2584  0732 ed81              	ldy	OFST+0,s
2585  0734 e6ea0000          	ldab	_CanIndicationOffset,y
2586  0738 87                	clra	
2587  0739 b746              	tfr	d,y
2588  073b ee81              	ldx	OFST+0,s
2589  073d e6e20000          	ldab	_CanIndicationMask,x
2590  0741 eaea0022          	orab	_CanIndicationFlags,y
2591  0745 6bea0022          	stab	_CanIndicationFlags,y
2592                         ; 5700   CanNestedGlobalInterruptRestore();
2594  0749 e680              	ldab	OFST-1,s
2595  074b 4a000000          	call	f_VStdLL_GlobalInterruptRestore
2597                         ; 5707   if ( CanGetApplIndicationPtr(rxHandle) != V_NULL )
2599  074f ed81              	ldy	OFST+0,s
2600  0751 1858              	lsly	
2601  0753 1858              	lsly	
2602  0755 ecea0000          	ldd	_CanRxApplIndicationPtr,y
2603  0759 2606              	bne	LC007
2604  075b ecea0002          	ldd	_CanRxApplIndicationPtr+2,y
2605  075f 270c              	beq	L3601
2606  0761                   LC007:
2607                         ; 5710     CanGetApplIndicationPtr(rxHandle)(rxHandle);  /* call IndicationRoutine */
2609  0761 ec81              	ldd	OFST+0,s
2610  0763 b746              	tfr	d,y
2611  0765 1858              	lsly	
2612  0767 1858              	lsly	
2613  0769 4beb0000          	call	[_CanRxApplIndicationPtr,y]
2615  076d                   L3601:
2616                         ; 5713 }
2619  076d 1b83              	leas	3,s
2620  076f 0a                	rtc	
2715                         ; 5731 static void CanHL_TxConfirmation( CAN_HW_CHANNEL_CANTYPE_FIRST CanObjectHandle txObjHandle)
2715                         ; 5732 {
2716                         	switch	.ftext
2717  0770                   L15f_CanHL_TxConfirmation:
2719  0770 3b                	pshd	
2720  0771 1b96              	leas	-10,s
2721       0000000a          OFST:	set	10
2724                         ; 5764   logTxObjHandle = (CanObjectHandle)((vsintx)txObjHandle + CAN_HL_TX_OFFSET_HW_TO_LOG(canHwChannel));
2726  0773 b746              	tfr	d,y
2727  0775 03                	dey	
2728  0776 6d87              	sty	OFST-3,s
2729                         ; 5767   if((vuint8)0xFF == txObjHandle)
2731  0778 ec8a              	ldd	OFST+0,s
2732  077a 8c00ff            	cpd	#255
2733  077d 1827015b          	beq	L5601
2734                         ; 5769     goto finishCanHL_TxConfirmation;
2736                         ; 5776     CTIER &= (vuint8)~CanMailboxSelect[txObjHandle]; /* deactivate Tx interrupt, might redundant in case of individual polling but is faster without a check */
2738  0781 b746              	tfr	d,y
2739  0783 e6ea0033          	ldab	L7_CanMailboxSelect,y
2740  0787 51                	comb	
2741  0788 f40147            	andb	327
2742  078b 7b0147            	stab	327
2743                         ; 5790   CTBSEL = CanMailboxSelect[txObjHandle]; /* ESCAN00023651, make the mailbox visible that transmitted */
2745  078e cd014a            	ldy	#330
2746  0791 ee8a              	ldx	OFST+0,s
2747  0793 180ae2003340      	movb	L7_CanMailboxSelect,x,0,y
2748                         ; 5793   txHandle = canHandleCurTxObj[logTxObjHandle];           /* get saved handle */
2750  0799 ed87              	ldy	OFST-3,s
2751  079b 1858              	lsly	
2752  079d ecea001a          	ldd	L31_canHandleCurTxObj,y
2753  07a1 6c82              	std	OFST-8,s
2754                         ; 5796   if (txHandle == kCanBufferFree)
2756  07a3 8cffff            	cpd	#-1
2757  07a6 18270132          	beq	L5601
2758                         ; 5799     goto finishCanHL_TxConfirmation;
2761                         ; 5838   if (txHandle != kCanBufferCancel)
2763  07aa 8cfffe            	cpd	#-2
2764  07ad 2744              	beq	L5211
2765                         ; 5858     CanNestedGlobalInterruptDisable();
2767  07af b774              	tfr	s,d
2768  07b1 c30006            	addd	#-4+OFST
2769  07b4 4a000000          	call	f_VStdLL_GlobalInterruptDisable
2771                         ; 5860     CanConfirmationFlags._c[CanGetConfirmationOffset(txHandle)] |= CanGetConfirmationMask(txHandle);
2773  07b8 ed82              	ldy	OFST-8,s
2774  07ba e6ea0000          	ldab	_CanConfirmationOffset,y
2775  07be 87                	clra	
2776  07bf b746              	tfr	d,y
2777  07c1 ee82              	ldx	OFST-8,s
2778  07c3 e6e20000          	ldab	_CanConfirmationMask,x
2779  07c7 eaea0023          	orab	_CanConfirmationFlags,y
2780  07cb 6bea0023          	stab	_CanConfirmationFlags,y
2781                         ; 5862     CanNestedGlobalInterruptRestore();
2783  07cf e686              	ldab	OFST-4,s
2784  07d1 4a000000          	call	f_VStdLL_GlobalInterruptRestore
2786                         ; 5870       if ( CanGetApplConfirmationPtr(txHandle) != V_NULL )
2788  07d5 ed82              	ldy	OFST-8,s
2789  07d7 1858              	lsly	
2790  07d9 1858              	lsly	
2791  07db ecea0000          	ldd	_CanTxApplConfirmationPtr,y
2792  07df 2606              	bne	LC008
2793  07e1 ecea0002          	ldd	_CanTxApplConfirmationPtr+2,y
2794  07e5 270c              	beq	L5211
2795  07e7                   LC008:
2796                         ; 5873         (CanGetApplConfirmationPtr(txHandle))(txHandle);   /* call completion routine  */
2798  07e7 ec82              	ldd	OFST-8,s
2799  07e9 b746              	tfr	d,y
2800  07eb 1858              	lsly	
2801  07ed 1858              	lsly	
2802  07ef 4beb0000          	call	[_CanTxApplConfirmationPtr,y]
2804  07f3                   L5211:
2805                         ; 5885     CanNestedGlobalInterruptDisable();                /* ESCAN00008914 */
2807  07f3 b774              	tfr	s,d
2808  07f5 c30006            	addd	#-4+OFST
2809  07f8 4a000000          	call	f_VStdLL_GlobalInterruptDisable
2811                         ; 5887     for(queueElementIdx = CAN_HL_TXQUEUE_STOPINDEX(channel) - (CanSignedTxHandle)1; 
2813  07fc 18c7              	clry	
2814  07fe 6d84              	sty	OFST-6,s
2815  0800                   L1311:
2816                         ; 5890       elem = canTxQueueFlags[queueElementIdx];
2818  0800 ed84              	ldy	OFST-6,s
2819  0802 1858              	lsly	
2820  0804 ecea0004          	ldd	L33_canTxQueueFlags,y
2821  0808 6c80              	std	OFST-10,s
2822                         ; 5891       if(elem != (tCanQueueElementType)0) /* is there any flag set in the queue element? */
2824  080a 182700b5          	beq	L7311
2825                         ; 5894         CanNestedGlobalInterruptRestore();
2827  080e e686              	ldab	OFST-4,s
2828  0810 87                	clra	
2829  0811 4a000000          	call	f_VStdLL_GlobalInterruptRestore
2831                         ; 5899           if((elem & (tCanQueueElementType)0x0000FF00) != (tCanQueueElementType)0)
2833  0815 ec80              	ldd	OFST-10,s
2834  0817 85ff              	bita	#255
2835  0819 2724              	beq	L1411
2836                         ; 5901             if((elem & (tCanQueueElementType)0x0000F000) != (tCanQueueElementType)0)
2838  081b 85f0              	bita	#240
2839  081d b784              	exg	a,d
2840  081f 2711              	beq	L3411
2841                         ; 5903               elementBitIdx = (CanSignedTxHandle)CanGetHighestFlagFromNibble[elem >> 12] + 12;
2843  0821 54                	lsrb	
2844  0822 54                	lsrb	
2845  0823 54                	lsrb	
2846  0824 54                	lsrb	
2847  0825 b746              	tfr	d,y
2848  0827 e6ea0020          	ldab	L5_CanGetHighestFlagFromNibble,y
2849  082b b714              	sex	b,d
2850  082d c3000c            	addd	#12
2852  0830 202a              	bra	L7411
2853  0832                   L3411:
2854                         ; 5907               elementBitIdx = (CanSignedTxHandle)CanGetHighestFlagFromNibble[elem >> 8] + 8;
2856  0832 b746              	tfr	d,y
2857  0834 e6ea0020          	ldab	L5_CanGetHighestFlagFromNibble,y
2858  0838 b714              	sex	b,d
2859  083a c30008            	addd	#8
2860  083d 201d              	bra	L7411
2861  083f                   L1411:
2862                         ; 5913             if((elem & (tCanQueueElementType)0x000000F0) != (tCanQueueElementType)0)
2864  083f c5f0              	bitb	#240
2865  0841 2711              	beq	L1511
2866                         ; 5915               elementBitIdx = (CanSignedTxHandle)CanGetHighestFlagFromNibble[elem >> 4] + 4;
2868  0843 49                	lsrd	
2869  0844 49                	lsrd	
2870  0845 49                	lsrd	
2871  0846 49                	lsrd	
2872  0847 b746              	tfr	d,y
2873  0849 e6ea0020          	ldab	L5_CanGetHighestFlagFromNibble,y
2874  084d b714              	sex	b,d
2875  084f c30004            	addd	#4
2877  0852 2008              	bra	L7411
2878  0854                   L1511:
2879                         ; 5919               elementBitIdx = (CanSignedTxHandle)CanGetHighestFlagFromNibble[elem];
2881  0854 b746              	tfr	d,y
2882  0856 e6ea0020          	ldab	L5_CanGetHighestFlagFromNibble,y
2883  085a b714              	sex	b,d
2884  085c                   L7411:
2885  085c 6c80              	std	OFST-10,s
2886                         ; 5923         txHandle = (((CanTransmitHandle)queueElementIdx << kCanTxQueueShift) + (CanTransmitHandle)elementBitIdx) - CAN_HL_TXQUEUE_PADBITS(channel);
2888  085e ec84              	ldd	OFST-6,s
2889  0860 59                	lsld	
2890  0861 59                	lsld	
2891  0862 59                	lsld	
2892  0863 59                	lsld	
2893  0864 e380              	addd	OFST-10,s
2894  0866 6c82              	std	OFST-8,s
2895                         ; 5927             CanNestedGlobalInterruptDisable();
2897  0868 b774              	tfr	s,d
2898  086a c30006            	addd	#-4+OFST
2899  086d 4a000000          	call	f_VStdLL_GlobalInterruptDisable
2901                         ; 5929             if ( (canTxQueueFlags[queueElementIdx] & CanShiftLookUp[elementBitIdx]) != (tCanQueueElementType)0 )
2903  0871 ed80              	ldy	OFST-10,s
2904  0873 1858              	lsly	
2905  0875 eeea0000          	ldx	L3_CanShiftLookUp,y
2906  0879 ed84              	ldy	OFST-6,s
2907  087b 1858              	lsly	
2908  087d 18a4ea0004        	andx	L33_canTxQueueFlags,y
2909  0882 2746              	beq	LC009
2910                         ; 5931               canTxQueueFlags[queueElementIdx] &= ~CanShiftLookUp[elementBitIdx]; /* clear flag from the queue */
2912  0884 ed84              	ldy	OFST-6,s
2913  0886 1858              	lsly	
2914  0888 ee80              	ldx	OFST-10,s
2915  088a 1848              	lslx	
2916  088c eee20000          	ldx	L3_CanShiftLookUp,x
2917  0890 1841              	comx	
2918  0892 18a4ea0004        	andx	L33_canTxQueueFlags,y
2919  0897 6eea0004          	stx	L33_canTxQueueFlags,y
2920                         ; 5933               CanNestedGlobalInterruptRestore();  
2922  089b e686              	ldab	OFST-4,s
2923  089d 87                	clra	
2924  089e 4a000000          	call	f_VStdLL_GlobalInterruptRestore
2926                         ; 5934               canHandleCurTxObj[logTxObjHandle] = txHandle;/* Save hdl of msgObj to be transmitted*/
2928  08a2 ec82              	ldd	OFST-8,s
2929  08a4 ed87              	ldy	OFST-3,s
2930  08a6 1858              	lsly	
2931  08a8 6cea001a          	std	L31_canHandleCurTxObj,y
2932                         ; 5936               rc = CanCopyDataAndStartTransmission( CAN_CHANNEL_CANPARA_FIRST txObjHandle,txHandle); 
2934  08ac 3b                	pshd	
2935  08ad ec8c              	ldd	OFST+2,s
2936  08af 4a049292          	call	L35f_CanCopyDataAndStartTransmission
2938  08b3 1b82              	leas	2,s
2939  08b5 6b89              	stab	OFST-1,s
2940                         ; 5937               if ( rc == kCanTxNotify)
2942  08b7 c105              	cmpb	#5
2943  08b9 2621              	bne	L5601
2944                         ; 5939                 APPLCANCANCELNOTIFICATION(channel, txHandle);
2946  08bb ec82              	ldd	OFST-8,s
2947  08bd 4a000000          	call	f_IlCanCancelNotification
2949  08c1 2019              	bra	L5601
2950                         ; 5948             canHandleCurTxObj[logTxObjHandle] = kCanBufferFree;  /* free msg object if queue is empty */
2952                         ; 5949             CanNestedGlobalInterruptRestore();  
2955                         ; 5950             goto finishCanHL_TxConfirmation;
2957  08c3                   L7311:
2958                         ; 5888                              queueElementIdx >= CAN_HL_TXQUEUE_STARTINDEX(channel); queueElementIdx--)
2960  08c3 186384            	decw	OFST-6,s
2961                         ; 5887     for(queueElementIdx = CAN_HL_TXQUEUE_STOPINDEX(channel) - (CanSignedTxHandle)1; 
2961                         ; 5888                              queueElementIdx >= CAN_HL_TXQUEUE_STARTINDEX(channel); queueElementIdx--)
2963  08c6 182cff36          	bge	L1311
2964                         ; 5962     canHandleCurTxObj[logTxObjHandle] = kCanBufferFree;  /* free msg object if queue is empty */
2966                         ; 5963     CanNestedGlobalInterruptRestore();                 /* ESCAN00008914 */
2968  08ca                   LC009:
2969  08ca ccffff            	ldd	#-1
2970  08cd ed87              	ldy	OFST-3,s
2971  08cf 1858              	lsly	
2972  08d1 6cea001a          	std	L31_canHandleCurTxObj,y
2973  08d5 e686              	ldab	OFST-4,s
2974  08d7 87                	clra	
2975  08d8 4a000000          	call	f_VStdLL_GlobalInterruptRestore
2977  08dc                   L5601:
2978                         ; 5969 finishCanHL_TxConfirmation:
2978                         ; 5970 
2978                         ; 5971   # if defined(C_COMP_COSMIC_MCS12X_MSCAN12)  
2978                         ; 5972   CTBSEL = (vuint8)((vuint8)~CTBSEL & kCanPendingTxObjMask); /* ESCAN00023651, make visible the mailbox that was probably changed (if it was not changed, this is not a risk) */
2980  08dc f6014a            	ldab	330
2981  08df 51                	comb	
2982  08e0 c406              	andb	#6
2983  08e2 7b014a            	stab	330
2984                         ; 5975   return;
2987  08e5 1b8c              	leas	12,s
2988  08e7 0a                	rtc	
3023                         ; 6040 C_API_1 void C_API_2 CanOnline(CAN_CHANNEL_CANTYPE_ONLY)
3023                         ; 6041 {
3024                         	switch	.ftext
3025  08e8                   f_CanOnline:
3027  08e8 37                	pshb	
3028       00000001          OFST:	set	1
3031                         ; 6048   CanNestedGlobalInterruptDisable();
3033  08e9 b774              	tfr	s,d
3034  08eb 4a000000          	call	f_VStdLL_GlobalInterruptDisable
3036                         ; 6050   canStatus[channel] |= kCanTxOn;
3038  08ef 1c000201          	bset	L73_canStatus,1
3039                         ; 6055   CanNestedGlobalInterruptRestore();
3041  08f3 e680              	ldab	OFST-1,s
3042  08f5 87                	clra	
3043  08f6 4a000000          	call	f_VStdLL_GlobalInterruptRestore
3045                         ; 6057 }
3048  08fa 1b81              	leas	1,s
3049  08fc 0a                	rtc	
3085                         ; 6070 C_API_1 void C_API_2 CanOffline(CAN_CHANNEL_CANTYPE_ONLY)
3085                         ; 6071 {
3086                         	switch	.ftext
3087  08fd                   f_CanOffline:
3089  08fd 37                	pshb	
3090       00000001          OFST:	set	1
3093                         ; 6078   CanNestedGlobalInterruptDisable();
3095  08fe b774              	tfr	s,d
3096  0900 4a000000          	call	f_VStdLL_GlobalInterruptDisable
3098                         ; 6080   canStatus[channel] &= kCanTxNotOn;
3100  0904 1d000201          	bclr	L73_canStatus,1
3101                         ; 6085   CanNestedGlobalInterruptRestore();
3103  0908 e680              	ldab	OFST-1,s
3104  090a 87                	clra	
3105  090b 4a000000          	call	f_VStdLL_GlobalInterruptRestore
3107                         ; 6088   CanDelQueuedObj( CAN_CHANNEL_CANPARA_ONLY );
3109  090f 4a02f4f4          	call	L55f_CanDelQueuedObj
3111                         ; 6091 }
3114  0913 1b81              	leas	1,s
3115  0915 0a                	rtc	
3157                         ; 6106 C_API_1 void C_API_2 CanSetPartOffline(CAN_CHANNEL_CANTYPE_FIRST vuint8 sendGroup)
3157                         ; 6107 {
3158                         	switch	.ftext
3159  0916                   f_CanSetPartOffline:
3161  0916 3b                	pshd	
3162  0917 37                	pshb	
3163       00000001          OFST:	set	1
3166                         ; 6114   CanNestedGlobalInterruptDisable();
3168  0918 b774              	tfr	s,d
3169  091a 4a000000          	call	f_VStdLL_GlobalInterruptDisable
3171                         ; 6115   canTxPartOffline[channel] |= sendGroup;   /* set offlinestate and Save for use of CanOnline/CanOffline */
3173  091e e682              	ldab	OFST+1,s
3174  0920 fa0003            	orab	L53_canTxPartOffline
3175  0923 7b0003            	stab	L53_canTxPartOffline
3176                         ; 6116   CanNestedGlobalInterruptRestore();
3178  0926 e680              	ldab	OFST-1,s
3179  0928 87                	clra	
3180  0929 4a000000          	call	f_VStdLL_GlobalInterruptRestore
3182                         ; 6117 }
3185  092d 1b83              	leas	3,s
3186  092f 0a                	rtc	
3228                         ; 6130 C_API_1 void C_API_2 CanSetPartOnline(CAN_CHANNEL_CANTYPE_FIRST vuint8 invSendGroup)
3228                         ; 6131 {
3229                         	switch	.ftext
3230  0930                   f_CanSetPartOnline:
3232  0930 3b                	pshd	
3233  0931 37                	pshb	
3234       00000001          OFST:	set	1
3237                         ; 6138   CanNestedGlobalInterruptDisable();
3239  0932 b774              	tfr	s,d
3240  0934 4a000000          	call	f_VStdLL_GlobalInterruptDisable
3242                         ; 6139   canTxPartOffline[channel] &= invSendGroup;
3244  0938 e682              	ldab	OFST+1,s
3245  093a f40003            	andb	L53_canTxPartOffline
3246  093d 7b0003            	stab	L53_canTxPartOffline
3247                         ; 6140   CanNestedGlobalInterruptRestore();
3249  0940 e680              	ldab	OFST-1,s
3250  0942 87                	clra	
3251  0943 4a000000          	call	f_VStdLL_GlobalInterruptRestore
3253                         ; 6141 }
3256  0947 1b83              	leas	3,s
3257  0949 0a                	rtc	
3280                         ; 6154 C_API_1 vuint8 C_API_2 CanGetPartMode(CAN_CHANNEL_CANTYPE_ONLY)
3280                         ; 6155 {
3281                         	switch	.ftext
3282  094a                   f_CanGetPartMode:
3286                         ; 6160   return canTxPartOffline[channel];
3288  094a f60003            	ldab	L53_canTxPartOffline
3291  094d 0a                	rtc	
3314                         ; 6185 C_API_1 vuint8 C_API_2 CanGetStatus( CAN_CHANNEL_CANTYPE_ONLY )
3314                         ; 6186 {
3315                         	switch	.ftext
3316  094e                   f_CanGetStatus:
3320                         ; 6199   if ( CanLL_HwIsSleep(CAN_HW_CHANNEL_CANPARA_ONLY)   )  { return ( VUINT8_CAST ( canStatus[channel] | kCanHwIsSleep ) ); }
3322  094e 1f01410206        	brclr	321,2,L5621
3325  0953 f60002            	ldab	L73_canStatus
3326  0956 ca80              	orab	#128
3329  0958 0a                	rtc	
3330  0959                   L5621:
3331                         ; 6204   if ( CanLL_HwIsStop(CAN_HW_CHANNEL_CANPARA_ONLY)    )  { return ( VUINT8_CAST ( canStatus[channel] | kCanHwIsStop ) ); }
3333  0959 1f01410106        	brclr	321,1,L7621
3336  095e f60002            	ldab	L73_canStatus
3337  0961 ca02              	orab	#2
3340  0963 0a                	rtc	
3341  0964                   L7621:
3342                         ; 6207   if ( CanLL_HwIsBusOff(CAN_HW_CHANNEL_CANPARA_ONLY)  )  { return ( VUINT8_CAST ( canStatus[channel] | kCanHwIsBusOff ) ); }
3344  0964 1e01440c02        	brset	324,12,LC010
3345  0969 2006              	bra	L1721
3346  096b                   LC010:
3349  096b f60002            	ldab	L73_canStatus
3350  096e ca40              	orab	#64
3353  0970 0a                	rtc	
3354  0971                   L1721:
3355                         ; 6211     if ( CanLL_HwIsPassive(CAN_HW_CHANNEL_CANPARA_ONLY) )  { return ( VUINT8_CAST ( canStatus[channel] | kCanHwIsPassive ) ); }
3357  0971 f60144            	ldab	324
3358  0974 c40c              	andb	#12
3359  0976 c108              	cmpb	#8
3360  0978 2709              	beq	L5721
3362  097a f60144            	ldab	324
3363  097d c430              	andb	#48
3364  097f c120              	cmpb	#32
3365  0981 2606              	bne	L3721
3366  0983                   L5721:
3369  0983 f60002            	ldab	L73_canStatus
3370  0986 ca20              	orab	#32
3373  0988 0a                	rtc	
3374  0989                   L3721:
3375                         ; 6216     if ( CanLL_HwIsWarning(CAN_HW_CHANNEL_CANPARA_ONLY) )  { return ( VUINT8_CAST ( canStatus[channel] | kCanHwIsWarning ) ); }
3377  0989 f60144            	ldab	324
3378  098c c40c              	andb	#12
3379  098e c104              	cmpb	#4
3380  0990 2709              	beq	L1031
3382  0992 f60144            	ldab	324
3383  0995 c430              	andb	#48
3384  0997 c110              	cmpb	#16
3385  0999 2606              	bne	L7721
3386  099b                   L1031:
3389  099b f60002            	ldab	L73_canStatus
3390  099e ca10              	orab	#16
3393  09a0 0a                	rtc	
3394  09a1                   L7721:
3395                         ; 6219   return ( VUINT8_CAST (canStatus[channel] & kCanTxOn) );
3397  09a1 f60002            	ldab	L73_canStatus
3398  09a4 c401              	andb	#1
3401  09a6 0a                	rtc	
3432                         ; 6236 C_API_1 vuint8 C_API_2 CanSleep(CAN_CHANNEL_CANTYPE_ONLY)
3432                         ; 6237 {
3433                         	switch	.ftext
3434  09a7                   f_CanSleep:
3436  09a7 37                	pshb	
3437       00000001          OFST:	set	1
3440                         ; 6260       CTL0 |= SLPRQ;    /* set sleep request bit */
3445  09a8 1c014002          	bset	320,2
3446                         ; 6261       APPLCANTIMERSTART(kCanLoopCanSleep);
3448  09ac                   L1231:
3449                         ; 6262       while(((CTL0 & SLPRQ) == SLPRQ) && ((CTL1 & SLPAK) != SLPAK) && APPLCANTIMERLOOP(kCanLoopCanSleep))
3451  09ac 1f01400205        	brclr	320,2,L5231
3453  09b1 1f014102f6        	brclr	321,2,L1231
3454  09b6                   L5231:
3455                         ; 6268       if(SLPAK == (CTL1 & SLPAK)) /* check if sleep request was successful */
3458  09b6 1f01410204        	brclr	321,2,L7231
3459                         ; 6270         canReturnCode = kCanOk;
3461  09bb c601              	ldab	#1
3463  09bd 2001              	bra	L1331
3464  09bf                   L7231:
3465                         ; 6274         canReturnCode = kCanFailed;
3467  09bf c7                	clrb	
3468  09c0                   L1331:
3469                         ; 6278   return canReturnCode;
3473  09c0 1b81              	leas	1,s
3474  09c2 0a                	rtc	
3505                         ; 6300 C_API_1 vuint8 C_API_2 CanWakeUp( CAN_CHANNEL_CANTYPE_ONLY )
3505                         ; 6301 {
3506                         	switch	.ftext
3507  09c3                   f_CanWakeUp:
3509  09c3 37                	pshb	
3510       00000001          OFST:	set	1
3513                         ; 6313       CTL0 &= (vuint8)~SLPRQ; /* clear sleep request */
3516  09c4 1d014002          	bclr	320,2
3517                         ; 6315       APPLCANTIMERSTART(kCanLoopCanWakeup);
3519  09c8                   L1531:
3520                         ; 6316       while(((CTL1 & SLPAK) == SLPAK) && APPLCANTIMERLOOP(kCanLoopCanWakeup))
3522  09c8 1e014102fb        	brset	321,2,L1531
3523                         ; 6322       if(SLPAK == (CTL1 & SLPAK))
3526  09cd 1f01410205        	brclr	321,2,L5531
3527                         ; 6324         canReturnCode = kCanFailed; /* CAN controller still sleeping */
3529  09d2 c7                	clrb	
3530  09d3 6b80              	stab	OFST-1,s
3532  09d5 2002              	bra	L7531
3533  09d7                   L5531:
3534                         ; 6328         canReturnCode = kCanOk;
3536  09d7 c601              	ldab	#1
3537  09d9                   L7531:
3538                         ; 6332   return canReturnCode;
3542  09d9 1b81              	leas	1,s
3543  09db 0a                	rtc	
3575                         ; 6356 C_API_1 vuint8 C_API_2 CanStop( CAN_CHANNEL_CANTYPE_ONLY )
3575                         ; 6357 {
3576                         	switch	.ftext
3577  09dc                   f_CanStop:
3579  09dc 37                	pshb	
3580       00000001          OFST:	set	1
3583                         ; 6371       CTL0 = INITRQ;
3587  09dd c601              	ldab	#1
3588  09df 7b0140            	stab	320
3589                         ; 6372       APPLCANTIMERSTART(kCanLoopInitReq);
3591  09e2                   L7731:
3592                         ; 6373       while( ((CTL1 & INITAK) != INITAK) && APPLCANTIMERLOOP(kCanLoopInitReq) )
3594  09e2 1f014101fb        	brclr	321,1,L7731
3595                         ; 6380       if(INITAK == (CTL1 & INITAK))
3598  09e7 1f01410108        	brclr	321,1,L3041
3599                         ; 6382         canReturnCode = kCanOk;
3601  09ec 6b80              	stab	OFST-1,s
3602                         ; 6383         canStatus[channel] |= kCanHwIsStop;
3604  09ee 1c000202          	bset	L73_canStatus,2
3606  09f2 2001              	bra	L5041
3607  09f4                   L3041:
3608                         ; 6387         canReturnCode = kCanFailed;
3610  09f4 c7                	clrb	
3611  09f5                   L5041:
3612                         ; 6391   return canReturnCode;
3616  09f5 1b81              	leas	1,s
3617  09f7 0a                	rtc	
3651                         ; 6406 C_API_1 vuint8 C_API_2 CanStart( CAN_CHANNEL_CANTYPE_ONLY )
3651                         ; 6407 {
3652                         	switch	.ftext
3653  09f8                   f_CanStart:
3655  09f8 37                	pshb	
3656       00000001          OFST:	set	1
3659                         ; 6415     CanInit(CAN_CHANNEL_CANPARA_FIRST lastInitObject[channel]);
3661  09f9 fc000e            	ldd	L72_lastInitObject
3662  09fc 4a007a7a          	call	f_CanInit
3664                         ; 6416     canStatus[channel] &= (vuint8)~kCanHwIsStop;
3666  0a00 1d000202          	bclr	L73_canStatus,2
3667                         ; 6417     canReturnCode = kCanOk;
3669                         ; 6419   return canReturnCode;
3671  0a04 c601              	ldab	#1
3674  0a06 1b81              	leas	1,s
3675  0a08 0a                	rtc	
3713                         ; 6434 C_API_1 void C_API_2 CanCanInterruptDisable( CAN_CHANNEL_CANTYPE_ONLY )
3713                         ; 6435 {
3714                         	switch	.ftext
3715  0a09                   f_CanCanInterruptDisable:
3717  0a09 37                	pshb	
3718       00000001          OFST:	set	1
3721                         ; 6455   CanNestedGlobalInterruptDisable();
3724  0a0a b774              	tfr	s,d
3725  0a0c 4a000000          	call	f_VStdLL_GlobalInterruptDisable
3727                         ; 6456   if (canCanInterruptCounter[channel] == (vsintx)0)  /* if 0 then save old interrupt status */
3729  0a10 fc0012            	ldd	L32_canCanInterruptCounter
3730  0a13 2607              	bne	L7341
3731                         ; 6464       CanLL_CanInterruptDisable(canHwChannel, &canCanInterruptOldStatus[canHwChannel]);
3734  0a15 cc0010            	ldd	#L52_canCanInterruptOldStatus
3735  0a18 4a000000          	call	L36f_CanCanInterruptDisableInternal
3737  0a1c                   L7341:
3738                         ; 6471   canCanInterruptCounter[channel]++;               /* common for all platforms */
3740  0a1c 18720012          	incw	L32_canCanInterruptCounter
3741                         ; 6473   CanNestedGlobalInterruptRestore();
3743  0a20 e680              	ldab	OFST-1,s
3744  0a22 87                	clra	
3745  0a23 4a000000          	call	f_VStdLL_GlobalInterruptRestore
3747                         ; 6475 } /* END OF CanCanInterruptDisable */
3750  0a27 1b81              	leas	1,s
3751  0a29 0a                	rtc	
3789                         ; 6488 C_API_1 void C_API_2 CanCanInterruptRestore( CAN_CHANNEL_CANTYPE_ONLY )
3789                         ; 6489 {
3790                         	switch	.ftext
3791  0a2a                   f_CanCanInterruptRestore:
3793  0a2a 37                	pshb	
3794       00000001          OFST:	set	1
3797                         ; 6503   CanNestedGlobalInterruptDisable();
3800  0a2b b774              	tfr	s,d
3801  0a2d 4a000000          	call	f_VStdLL_GlobalInterruptDisable
3803                         ; 6505   canCanInterruptCounter[channel]--;
3805  0a31 18730012          	decw	L32_canCanInterruptCounter
3806                         ; 6507   if (canCanInterruptCounter[channel] == (vsintx)0)         /* restore interrupt if canCanInterruptCounter=0 */
3808  0a35 260a              	bne	L5541
3809                         ; 6515       CanLL_CanInterruptRestore(canHwChannel, canCanInterruptOldStatus[canHwChannel]);
3812  0a37 fc0010            	ldd	L52_canCanInterruptOldStatus
3813  0a3a 3b                	pshd	
3814  0a3b 4a002d2d          	call	L56f_CanCanInterruptRestoreInternal
3816  0a3f 1b82              	leas	2,s
3817  0a41                   L5541:
3818                         ; 6523   CanNestedGlobalInterruptRestore();
3820  0a41 e680              	ldab	OFST-1,s
3821  0a43 87                	clra	
3822  0a44 4a000000          	call	f_VStdLL_GlobalInterruptRestore
3824                         ; 6525 } /* END OF CanCanInterruptRestore */
3827  0a48 1b81              	leas	1,s
3828  0a4a 0a                	rtc	
3880                         ; 6724 C_API_1 CanTransmitHandle C_API_2 CanGetDynTxObj(CanTransmitHandle txHandle )
3880                         ; 6725 {
3881                         	switch	.ftext
3882  0a4b                   f_CanGetDynTxObj:
3884  0a4b 3b                	pshd	
3885  0a4c 1b9d              	leas	-3,s
3886       00000003          OFST:	set	3
3889                         ; 6739   nTxDynObj = (txHandle - CAN_HL_TX_DYN_ROM_STARTINDEX(channel)) + CAN_HL_TX_DYN_RAM_STARTINDEX(channel); /* PRQA S 3382,0291 */
3894  0a4e b746              	tfr	d,y
3895  0a50 195b              	leay	-5,y
3896  0a52 6d80              	sty	OFST-3,s
3897                         ; 6741   CanNestedGlobalInterruptDisable();
3899  0a54 b774              	tfr	s,d
3900  0a56 c30002            	addd	#-1+OFST
3901  0a59 4a000000          	call	f_VStdLL_GlobalInterruptDisable
3903                         ; 6742   if ( canTxDynObjReservedFlag[nTxDynObj] != (vuint8)0)
3905  0a5d ed80              	ldy	OFST-3,s
3906  0a5f e7ea0019          	tst	L51_canTxDynObjReservedFlag,y
3907  0a63 270c              	beq	L7741
3908                         ; 6744     CanNestedGlobalInterruptRestore();
3910  0a65 e682              	ldab	OFST-1,s
3911  0a67 87                	clra	
3912  0a68 4a000000          	call	f_VStdLL_GlobalInterruptRestore
3914                         ; 6745     return kCanNoTxDynObjAvailable;
3916  0a6c ccffff            	ldd	#-1
3918  0a6f 2026              	bra	L631
3919  0a71                   L7741:
3920                         ; 6748   canTxDynObjReservedFlag[nTxDynObj] = 1;
3922  0a71 c601              	ldab	#1
3923  0a73 6bea0019          	stab	L51_canTxDynObjReservedFlag,y
3924                         ; 6751   CanConfirmationFlags._c[CanGetConfirmationOffset(txHandle)] &= 
3924                         ; 6752                             (vuint8)(~CanGetConfirmationMask(txHandle));
3926  0a77 ed83              	ldy	OFST+0,s
3927  0a79 e6ea0000          	ldab	_CanConfirmationOffset,y
3928  0a7d 87                	clra	
3929  0a7e b746              	tfr	d,y
3930  0a80 ee83              	ldx	OFST+0,s
3931  0a82 e6e20000          	ldab	_CanConfirmationMask,x
3932  0a86 51                	comb	
3933  0a87 e4ea0023          	andb	_CanConfirmationFlags,y
3934  0a8b 6bea0023          	stab	_CanConfirmationFlags,y
3935                         ; 6754   CanNestedGlobalInterruptRestore();
3937  0a8f e682              	ldab	OFST-1,s
3938  0a91 4a000000          	call	f_VStdLL_GlobalInterruptRestore
3940                         ; 6762   return (txHandle);
3942  0a95 ec83              	ldd	OFST+0,s
3944  0a97                   L631:
3946  0a97 1b85              	leas	5,s
3947  0a99 0a                	rtc	
4011                         ; 6783 C_API_1 vuint8 C_API_2 CanReleaseDynTxObj(CanTransmitHandle txHandle)
4011                         ; 6784 {
4012                         	switch	.ftext
4013  0a9a                   f_CanReleaseDynTxObj:
4015  0a9a 3b                	pshd	
4016  0a9b 1b9a              	leas	-6,s
4017       00000006          OFST:	set	6
4020                         ; 6802   dynTxObj = (txHandle - CAN_HL_TX_DYN_ROM_STARTINDEX(channel)) + CAN_HL_TX_DYN_RAM_STARTINDEX(channel);  /* PRQA S 3382,0291 */
4025  0a9d b746              	tfr	d,y
4026  0a9f 195b              	leay	-5,y
4027  0aa1 6d82              	sty	OFST-4,s
4028                         ; 6818   queueBitPos  = txHandle + CAN_HL_TXQUEUE_PADBITS(channel);
4031  0aa3 ed86              	ldy	OFST+0,s
4032                         ; 6819   queueElementIdx = (CanSignedTxHandle)(queueBitPos >> kCanTxQueueShift); /* get the queue element where to set the flag */
4034  0aa5 b764              	tfr	y,d
4035  0aa7 49                	lsrd	
4036  0aa8 49                	lsrd	
4037  0aa9 49                	lsrd	
4038  0aaa 49                	lsrd	
4039  0aab 6c84              	std	OFST-2,s
4040                         ; 6820   elementBitIdx  = (CanSignedTxHandle)(queueBitPos & kCanTxQueueMask);   /* get the flag index wihtin the queue element */
4042  0aad b764              	tfr	y,d
4043  0aaf c40f              	andb	#15
4044                         ; 6821   if( (canTxQueueFlags[queueElementIdx] & CanShiftLookUp[elementBitIdx]) != (tCanQueueElementType)0 )
4046  0ab1 b796              	exg	b,y
4047  0ab3 6d80              	sty	OFST-6,s
4048  0ab5 1858              	lsly	
4049  0ab7 eeea0000          	ldx	L3_CanShiftLookUp,y
4050  0abb ed84              	ldy	OFST-2,s
4051  0abd 1858              	lsly	
4052  0abf 18a4ea0004        	andx	L33_canTxQueueFlags,y
4053  0ac4 2617              	bne	L7251
4055                         ; 6827     if (
4055                         ; 6828 # if defined( C_ENABLE_CONFIRMATION_FCT ) && \
4055                         ; 6829     defined( C_ENABLE_TRANSMIT_QUEUE )
4055                         ; 6830          (confirmHandle[channel] == txHandle) ||       /* confirmation active ? */
4055                         ; 6831 # endif
4055                         ; 6832          (canHandleCurTxObj[(vsintx)CAN_HL_HW_TX_NORMAL_INDEX(canHwChannel) + CAN_HL_TX_OFFSET_HW_TO_LOG(canHwChannel)] != txHandle) )
4057  0ac6 fc0020            	ldd	_confirmHandle
4058  0ac9 ac86              	cpd	OFST+0,s
4059  0acb 2707              	beq	L3351
4061  0acd fc001a            	ldd	L31_canHandleCurTxObj
4062  0ad0 ac86              	cpd	OFST+0,s
4063  0ad2 2709              	beq	L7251
4064  0ad4                   L3351:
4065                         ; 6835       canTxDynObjReservedFlag[dynTxObj] = 0;
4067  0ad4 ed82              	ldy	OFST-4,s
4068  0ad6 c7                	clrb	
4069  0ad7 6bea0019          	stab	L51_canTxDynObjReservedFlag,y
4070                         ; 6836       return(kCanDynReleased);
4073  0adb 2002              	bra	L241
4074  0add                   L7251:
4075                         ; 6839   return(kCanDynNotReleased);
4077  0add c601              	ldab	#1
4079  0adf                   L241:
4081  0adf 1b88              	leas	8,s
4082  0ae1 0a                	rtc	
4136                         ; 6915 C_API_1 void C_API_2 CanDynTxObjSetExtId(CanTransmitHandle txHandle, vuint16 idExtHi, vuint16 idExtLo)
4136                         ; 6916 {
4137                         	switch	.ftext
4138  0ae2                   f_CanDynTxObjSetExtId:
4140  0ae2 3b                	pshd	
4141  0ae3 1b92              	leas	-14,s
4142       0000000e          OFST:	set	14
4145                         ; 6930   dynTxObj = (txHandle - CAN_HL_TX_DYN_ROM_STARTINDEX(channel)) + CAN_HL_TX_DYN_RAM_STARTINDEX(channel); /* PRQA S 3382,0291 */
4151  0ae5 b746              	tfr	d,y
4152  0ae7 195b              	leay	-5,y
4153  0ae9 6d8c              	sty	OFST-2,s
4154                         ; 6939   canDynTxId0[dynTxObj]      = MK_EXTID0( ((vuint32)idExtHi<<16) | (vuint32)idExtLo );
4156  0aeb ecf015            	ldd	OFST+7,s
4157  0aee 1887              	clrx	
4158  0af0 6c8a              	std	OFST-4,s
4159  0af2 6e88              	stx	OFST-6,s
4160  0af4 18aaf013          	orx	OFST+5,s
4161  0af8 59                	lsld	
4162  0af9 1845              	rolx	
4163  0afb 18840007          	andx	#7
4164  0aff c4fe              	andb	#254
4165  0b01 6c86              	std	OFST-8,s
4166  0b03 6e84              	stx	OFST-10,s
4167  0b05 ecf015            	ldd	OFST+7,s
4168  0b08 1887              	clrx	
4169  0b0a 6c82              	std	OFST-12,s
4170  0b0c 6e80              	stx	OFST-14,s
4171  0b0e 18aaf013          	orx	OFST+5,s
4172  0b12 59                	lsld	
4173  0b13 1845              	rolx	
4174  0b15 59                	lsld	
4175  0b16 1845              	rolx	
4176  0b18 59                	lsld	
4177  0b19 1845              	rolx	
4178  0b1b 1884ffe0          	andx	#-32
4179  0b1f 18aa84            	orx	OFST-10,s
4180  0b22 a686              	ldaa	OFST-8,s
4181  0b24 e687              	ldab	OFST-7,s
4182  0b26 188a0018          	orx	#24
4183  0b2a 1858              	lsly	
4184  0b2c 1858              	lsly	
4185  0b2e 18cb0015          	addy	#L71_canDynTxId0
4186  0b32 6c42              	std	2,y
4187  0b34 6e40              	stx	0,y
4188                         ; 6952 }
4191  0b36 1bf010            	leas	16,s
4192  0b39 0a                	rtc	
4238                         ; 6969 C_API_1 void C_API_2 CanDynTxObjSetDlc(CanTransmitHandle txHandle, vuint8 dlc)
4238                         ; 6970 {
4239                         	switch	.ftext
4240  0b3a                   f_CanDynTxObjSetDlc:
4242  0b3a 3b                	pshd	
4243  0b3b 3b                	pshd	
4244       00000002          OFST:	set	2
4247                         ; 6984   dynTxObj = (txHandle - CAN_HL_TX_DYN_ROM_STARTINDEX(channel)) + CAN_HL_TX_DYN_RAM_STARTINDEX(channel); /* PRQA S 3382,0291 */
4253  0b3c b746              	tfr	d,y
4254  0b3e 195b              	leay	-5,y
4255  0b40 6d80              	sty	OFST-2,s
4256                         ; 6987   canDynTxDLC[dynTxObj] = MK_TX_DLC_EXT(dlc);
4258  0b42 e688              	ldab	OFST+6,s
4259  0b44 c40f              	andb	#15
4260  0b46 6bea0014          	stab	L12_canDynTxDLC,y
4261                         ; 6991 }
4264  0b4a 1b84              	leas	4,s
4265  0b4c 0a                	rtc	
4683                         	switch	.bss
4684  0000                   L14_canCanIrqDisabled:
4685  0000 0000              	ds.b	2
4686  0002                   L73_canStatus:
4687  0002 00                	ds.b	1
4688  0003                   L53_canTxPartOffline:
4689  0003 00                	ds.b	1
4690  0004                   L33_canTxQueueFlags:
4691  0004 0000              	ds.b	2
4692  0006                   L13_canRxInfoStruct:
4693  0006 0000000000000000  	ds.b	8
4694  000e                   L72_lastInitObject:
4695  000e 0000              	ds.b	2
4696  0010                   L52_canCanInterruptOldStatus:
4697  0010 0000              	ds.b	2
4698  0012                   L32_canCanInterruptCounter:
4699  0012 0000              	ds.b	2
4700  0014                   L12_canDynTxDLC:
4701  0014 00                	ds.b	1
4702  0015                   L71_canDynTxId0:
4703  0015 00000000          	ds.b	4
4704  0019                   L51_canTxDynObjReservedFlag:
4705  0019 00                	ds.b	1
4706  001a                   L31_canHandleCurTxObj:
4707  001a 0000              	ds.b	2
4708                         	xdef	_CanErrorInterrupt_0
4709                         	xdef	_CanWakeUpInterrupt_0
4710                         	xdef	_CanTxInterrupt_0
4711                         	xdef	_CanRxInterrupt_0
4712                         	xdef	f_CanGetDriverStatus
4713                         	xdef	f_CanDynTxObjSetDlc
4714                         	xdef	f_CanDynTxObjSetExtId
4715                         	xdef	f_CanReleaseDynTxObj
4716                         	xdef	f_CanGetDynTxObj
4717                         	xdef	f_CanCanInterruptRestore
4718                         	xdef	f_CanCanInterruptDisable
4719                         	xdef	f_CanStop
4720                         	xdef	f_CanStart
4721                         	xdef	f_CanWakeUp
4722                         	xdef	f_CanSleep
4723                         	xdef	f_CanGetStatus
4724                         	xdef	f_CanGetPartMode
4725                         	xdef	f_CanSetPartOnline
4726                         	xdef	f_CanSetPartOffline
4727                         	xdef	f_CanOffline
4728                         	xdef	f_CanOnline
4729                         	xdef	f_CanCancelTransmit
4730                         	xdef	f_CanTransmit
4731                         	xdef	f_CanInit
4732                         	xdef	f_CanInitPowerOn
4733                         	xref	f_IlCanCancelNotification
4734                         	xref	f_IlCanGenericPrecopy
4735                         	xref	f_TpFuncPrecopy
4736                         	xref	f_TpPrecopy
4737                         	xref	f_ApplCanWakeUp
4738                         	xref	f_NmCanError
4739                         	xref	_CanInitObject
4740                         	xref	_CanRxApplIndicationPtr
4741                         	xref	_CanIndicationMask
4742                         	xref	_CanIndicationOffset
4743                         	xref	_CanRxApplPrecopyPtr
4744                         	xref	_CanRxDataPtr
4745                         	xref	_CanRxDataLen
4746                         	xref	_CanRxMsgIndirection
4747                         	xref	_CanRxId0
4748                         	xref	_CanTxApplConfirmationPtr
4749                         	xref	_CanConfirmationMask
4750                         	xref	_CanConfirmationOffset
4751                         	xref	_CanTxDataPtr
4752                         	xref	_CanTxDLC
4753                         	xref	_CanTxSendMask
4754                         	xref	_CanTxId0
4755  001c                   _canRDSTxPtr:
4756  001c 0000              	ds.b	2
4757                         	xdef	_canRDSTxPtr
4758  001e                   _canRDSRxPtr:
4759  001e 0000              	ds.b	2
4760                         	xdef	_canRDSRxPtr
4761  0020                   _confirmHandle:
4762  0020 0000              	ds.b	2
4763                         	xdef	_confirmHandle
4764  0022                   _CanIndicationFlags:
4765  0022 00                	ds.b	1
4766                         	xdef	_CanIndicationFlags
4767  0023                   _CanConfirmationFlags:
4768  0023 00                	ds.b	1
4769                         	xdef	_CanConfirmationFlags
4770  0024                   _canRxHandle:
4771  0024 0000              	ds.b	2
4772                         	xdef	_canRxHandle
4773                         	xdef	_kCanBugFixVersion
4774                         	xdef	_kCanSubVersion
4775                         	xdef	_kCanMainVersion
4776                         	xref	f_VStdLL_GlobalInterruptRestore
4777                         	xref	f_VStdLL_GlobalInterruptDisable
4778                         	xref	f_VStdInitPowerOn
4799                         	end
