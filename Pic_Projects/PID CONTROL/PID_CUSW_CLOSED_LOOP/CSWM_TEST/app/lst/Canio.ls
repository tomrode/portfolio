   1                         ; C Compiler for S12XE [COSMIC Software].
   2                         ; Generator V4.7.8 - 27 Jul 2007
   3                         ; Optimizer V4.6.13 - 27 Jul 2007
 340                         ; 215 @far void canF_Init(void)
 340                         ; 216 {
 341                         .ftext:	section	.text
 342  0000                   f_canF_Init:
 346                         ; 219 	EE_BlockRead(EE_REMOTE_CTRL_STAT, &canio_RemSt_stored_stat);
 348  0000 cc0050            	ldd	#_canio_RemSt_stored_stat
 349  0003 3b                	pshd	
 350  0004 cc0010            	ldd	#16
 351  0007 4a000000          	call	f_EE_BlockRead
 353  000b 1b82              	leas	2,s
 354                         ; 249 	canio_status_flags_c.cb = 0;
 356  000d 790052            	clr	_canio_status_flags_c
 357                         ; 250 	canio_RX_TravelDistance_w = CANBUS_KILOMETER_INVALID_K; //Initialize with the invalid default odo
 359  0010 ccffff            	ldd	#-1
 360  0013 7c004e            	std	_canio_RX_TravelDistance_w
 361                         ; 251 	update_veh_state_c = 0;
 363  0016 87                	clra	
 364  0017 7a000a            	staa	L531_update_veh_state_c
 365                         ; 253 	canio_Debounce_Time_c = 0;
 367  001a 7a0007            	staa	_canio_Debounce_Time_c
 368                         ; 255 	canio_LHD_RHD_c = ru_eep_PROXI_Cfg.s.Driver_Side; //LHD;
 370  001d f60009            	ldab	_ru_eep_PROXI_Cfg+9
 371  0020 c401              	andb	#1
 372  0022 7b0035            	stab	_canio_LHD_RHD_c
 373                         ; 258 	main_SwReq_stW_c=0;
 375  0025 7a0000            	staa	_main_SwReq_stW_c
 376                         ; 261 	if (canio_RemSt_stored_stat == REMST_ENABLED)
 378  0028 f60050            	ldab	_canio_RemSt_stored_stat
 379  002b c155              	cmpb	#85
 380  002d 2604              	bne	L551
 381                         ; 264 		canio_RemSt_configStat_c = TRUE;
 383  002f c601              	ldab	#1
 384                         ; 269 		IlPutTxRemSt_DCSSts(TRUE);
 388  0031 2001              	bra	L751
 389  0033                   L551:
 390                         ; 275 		canio_RemSt_configStat_c = FALSE;
 392  0033 c7                	clrb	
 393                         ; 280 		IlPutTxRemSt_DCSSts(FALSE);
 396  0034                   L751:
 397  0034 7b000e            	stab	_canio_RemSt_configStat_c
 398  0037 4a000000          	call	f_IlPutTxRemSt_DCSSts
 399                         ; 285 	main_SwReq_Front_c = VEH_BUS_HVAC_FRONT_ONLY_K;
 401  003b c602              	ldab	#2
 402  003d 7b0000            	stab	_main_SwReq_Front_c
 403                         ; 287 	main_SwReq_Rear_c = VEH_SWITCH_REAR_ONLY_K;
 405  0040 52                	incb	
 406  0041 7b0000            	stab	_main_SwReq_Rear_c
 407                         ; 289 }
 410  0044 0a                	rtc	
 434                         ; 308 @far void canF_CyclicTask(void)
 434                         ; 309 {
 435                         	switch	.ftext
 436  0045                   f_canF_CyclicTask:
 440                         ; 315 	canLF_LOCTimeout_Condition();
 442  0045 4a01d1d1          	call	f_canLF_LOCTimeout_Condition
 444                         ; 317 	canLF_WriteOriginalVIN();
 446  0049 4a018989          	call	f_canLF_WriteOriginalVIN
 448                         ; 319 }
 451  004d 0a                	rtc	
 491                         ; 342 @far void canF_CyclicRemoteStart(void)
 491                         ; 343 {
 492                         	switch	.ftext
 493  004e                   f_canF_CyclicRemoteStart:
 495  004e 37                	pshb	
 496       00000001          OFST:	set	1
 499                         ; 348 	EE_BlockRead(EE_REMOTE_CTRL_STAT,&temp_remote_stat_c);
 501  004f 1a80              	leax	OFST-1,s
 502  0051 34                	pshx	
 503  0052 cc0010            	ldd	#16
 504  0055 4a000000          	call	f_EE_BlockRead
 506  0059 1b82              	leas	2,s
 507                         ; 350 	if (canio_RX_Rem_CFG_FEATURE_c == MAIN_CFG_RemSt_CSWM) {
 509  005b f60011            	ldab	_canio_RX_Rem_CFG_FEATURE_c
 510  005e c128              	cmpb	#40
 511  0060 2649              	bne	L132
 512                         ; 356 		if ( (canio_RX_Rem_CFG_STAT_RQ_c == CFG_DEFAULT) || (canio_RX_Rem_CFG_STAT_RQ_c == CFG_ENABLE) ) {
 514  0062 f60010            	ldab	_canio_RX_Rem_CFG_STAT_RQ_c
 515  0065 2704              	beq	L112
 517  0067 c102              	cmpb	#2
 518  0069 2617              	bne	L702
 519  006b                   L112:
 520                         ; 358 			canio_RemSt_stored_stat = REMST_ENABLED;
 522  006b c655              	ldab	#85
 523  006d 7b0050            	stab	_canio_RemSt_stored_stat
 524                         ; 361 		    IlPutTxRemSt_DCSSts(TRUE);
 526  0070 cc0001            	ldd	#1
 527  0073 4a000000          	call	f_IlPutTxRemSt_DCSSts
 529                         ; 365 			canio_RemSt_configStat_c = TRUE;
 531  0077 c601              	ldab	#1
 532  0079 7b000e            	stab	_canio_RemSt_configStat_c
 533                         ; 368 			if (temp_remote_stat_c == REMST_ENABLED) {
 535  007c e680              	ldab	OFST-1,s
 536  007e c155              	cmpb	#85
 538                         ; 374 				EE_BlockWrite(EE_REMOTE_CTRL_STAT,&canio_RemSt_stored_stat);
 541                         ; 375 				canio_RemSt_stat_updated = TRUE;
 543  0080 2015              	bra	LC001
 544  0082                   L702:
 545                         ; 380 			if (canio_RX_Rem_CFG_STAT_RQ_c == CFG_DISABLE) {
 547  0082 042126            	dbne	b,L132
 548                         ; 382 				canio_RemSt_stored_stat = REMST_DISABLED;
 550  0085 c6aa              	ldab	#170
 551  0087 7b0050            	stab	_canio_RemSt_stored_stat
 552                         ; 385 				IlPutTxRemSt_DCSSts(FALSE);
 554  008a 87                	clra	
 555  008b c7                	clrb	
 556  008c 4a000000          	call	f_IlPutTxRemSt_DCSSts
 558                         ; 389 				canio_RemSt_configStat_c = FALSE;
 560  0090 79000e            	clr	_canio_RemSt_configStat_c
 561                         ; 391 				if (temp_remote_stat_c == REMST_DISABLED) {
 563  0093 e680              	ldab	OFST-1,s
 564  0095 c1aa              	cmpb	#170
 566                         ; 397 					EE_BlockWrite(EE_REMOTE_CTRL_STAT,&canio_RemSt_stored_stat);
 569                         ; 398 					canio_RemSt_stat_updated = TRUE;
 571  0097                   LC001:
 572  0097 2712              	beq	L132
 573  0099 cc0050            	ldd	#_canio_RemSt_stored_stat
 574  009c 3b                	pshd	
 575  009d cc0010            	ldd	#16
 576  00a0 4a000000          	call	f_EE_BlockWrite
 577  00a4 1b82              	leas	2,s
 578  00a6 c601              	ldab	#1
 579  00a8 7b0000            	stab	_canio_RemSt_stat_updated
 580  00ab                   L132:
 581                         ; 413 }
 584  00ab 1b81              	leas	1,s
 585  00ad 0a                	rtc	
 622                         ; 438 @far void canF_CyclicManIndFunc(void)
 622                         ; 439 {
 623                         	switch	.ftext
 624  00ae                   f_canF_CyclicManIndFunc:
 628                         ; 443 	CSWM_VehStart_PKT_ManIndFunc(); //IGN Status and RemoteStartActive Info
 630  00ae 4a022222          	call	f_CSWM_VehStart_PKT_ManIndFunc
 632                         ; 444 	CSWM_EngRPM_ManIndFunc();		//Engine RPM
 634  00b2 4a025a5a          	call	f_CSWM_EngRPM_ManIndFunc
 636                         ; 445 	CSWM_FL_HS_RQ_TGW_ManIndFunc();	//F L Seat Heater Request
 638  00b6 4a026e6e          	call	f_CSWM_FL_HS_RQ_TGW_ManIndFunc
 640                         ; 446 	CSWM_FR_HS_RQ_TGW_ManIndFunc();	//F L Seat Heater Request
 642  00ba 4a028b8b          	call	f_CSWM_FR_HS_RQ_TGW_ManIndFunc
 644                         ; 447 	CSWM_HSW_RQ_TGW_ManIndFunc();	//Steering Wheel Request
 646  00be 4a02a1a1          	call	f_CSWM_HSW_RQ_TGW_ManIndFunc
 648                         ; 448 	CSWM_ExtTemp_ManIndFunc();		//External TEMP (AMB TEMP) Info
 650  00c2 4a02b8b8          	call	f_CSWM_ExtTemp_ManIndFunc
 652                         ; 449 	CSWM_BatVolt_ManIndFunc();		//System voltage Info
 654  00c6 4a02cccc          	call	f_CSWM_BatVolt_ManIndFunc
 656                         ; 450 	CSWM_ODO_ManIndFunc();			//Odometer Info
 658  00ca 4a02ecec          	call	f_CSWM_ODO_ManIndFunc
 660                         ; 451 	CSWM_CFG_RQ_ManIndFunc();		//Remote Start Info
 662  00ce 4a031717          	call	f_CSWM_CFG_RQ_ManIndFunc
 664                         ; 452 	CSWM_StW_Actn_Rq_ManIndFunc();	//StWheel Temp and SnsFlt Ingo
 666  00d2 4a033d3d          	call	f_CSWM_StW_Actn_Rq_ManIndFunc
 668                         ; 453 	CSWM_VIN_ManIndFunc ();			//VIN	
 670  00d6 4a04a2a2          	call	f_CSWM_VIN_ManIndFunc
 672                         ; 454 	CSWM_EngineSts_ManIndFunc();	//Engine Status
 674  00da 4a036565          	call	f_CSWM_EngineSts_ManIndFunc
 676                         ; 455 	CSWM_EngineSpeedValidData_ManIndFunc();//Engine Speed Valid
 678  00de 4a037e7e          	call	f_CSWM_EngineSpeedValidData_ManIndFunc
 680                         ; 456 	CSWM_CFG_DATA_RESP_ManIndFunc ();
 682  00e2 4a039898          	call	f_CSWM_CFG_DATA_RESP_ManIndFunc
 684                         ; 457 }
 687  00e6 0a                	rtc	
 714                         ; 479 void canF_CBC_LOC_DTC(void)
 714                         ; 480 {
 715                         	switch	.ftext
 716  00e7                   f_canF_CBC_LOC_DTC:
 720                         ; 483 	if ((diag_failsafe_cond_b == FALSE) &&
 720                         ; 484 		(canio_canbus_off_b == FALSE))
 722  00e7 1e0000104b        	brset	_diag_io_lock3_flags_c,16,L172
 724  00ec 1e00510146        	brset	_canio_status2_flags_c,1,L172
 725                         ; 489 		if(canio_IgnRun_RemSt_Timeout_c > CBC_TIMEOUT_TIME_K)
 727  00f1 f6000c            	ldab	_canio_IgnRun_RemSt_Timeout_c
 728  00f4 c119              	cmpb	#25
 729  00f6 2306              	bls	L552
 730                         ; 491 			final_CBC_LOC_b = TRUE;
 732  00f8 1c005110          	bset	_canio_status2_flags_c,16
 734  00fc 2007              	bra	L752
 735  00fe                   L552:
 736                         ; 495 			canio_IgnRun_RemSt_Timeout_c++;
 738  00fe 72000c            	inc	_canio_IgnRun_RemSt_Timeout_c
 739                         ; 496 			final_CBC_LOC_b = FALSE;
 741  0101 1d005110          	bclr	_canio_status2_flags_c,16
 742  0105                   L752:
 743                         ; 499 		if ( final_CBC_LOC_b )
 745  0105 1f0051101c        	brclr	_canio_status2_flags_c,16,L162
 746                         ; 502 			if (canio_CBCTimeOut_Additional_WaitTime_c >= LOC_ENABLE_WAIT_TIME_K)
 748  010a f60002            	ldab	_canio_CBCTimeOut_Additional_WaitTime_c
 749  010d c17d              	cmpb	#125
 750  010f 2511              	blo	L362
 751                         ; 504 				FMemLibF_ReportDtc(DTC_NWM_LOC_WITH_CBC_K, ERROR_ON_K);
 753  0111 cc0001            	ldd	#1
 754  0114 3b                	pshd	
 755  0115 ce00c1            	ldx	#193
 756  0118 cc4000            	ldd	#16384
 757  011b 4a000000          	call	f_FMemLibF_ReportDtc
 759  011f 1b82              	leas	2,s
 762  0121 0a                	rtc	
 763  0122                   L362:
 764                         ; 508 				canio_CBCTimeOut_Additional_WaitTime_c++;
 766  0122 720002            	inc	_canio_CBCTimeOut_Additional_WaitTime_c
 768  0125 0a                	rtc	
 769  0126                   L162:
 770                         ; 514 			FMemLibF_ReportDtc(DTC_NWM_LOC_WITH_CBC_K, ERROR_OFF_K);
 772  0126 87                	clra	
 773  0127 c7                	clrb	
 774  0128 3b                	pshd	
 775  0129 ce00c1            	ldx	#193
 776  012c 8640              	ldaa	#64
 777  012e 4a000000          	call	f_FMemLibF_ReportDtc
 779  0132 1b82              	leas	2,s
 780                         ; 515 			canio_CBCTimeOut_Additional_WaitTime_c = 0;
 782  0134 790002            	clr	_canio_CBCTimeOut_Additional_WaitTime_c
 783  0137                   L172:
 784                         ; 522 }
 787  0137 0a                	rtc	
 814                         ; 546 void canF_TGW_LOC_DTC(void)
 814                         ; 547 {
 815                         	switch	.ftext
 816  0138                   f_canF_TGW_LOC_DTC:
 820                         ; 551 	if ((diag_failsafe_cond_b == FALSE) &&
 820                         ; 552 		(canio_canbus_off_b == FALSE))
 822  0138 1e0000104b        	brset	_diag_io_lock3_flags_c,16,L123
 824  013d 1e00510146        	brset	_canio_status2_flags_c,1,L123
 825                         ; 557 		if(canio_FL_HS_RQ_TGW_Timeout_c > TGW_TIMEOUT_TIME_K)
 827  0142 f6000d            	ldab	_canio_FL_HS_RQ_TGW_Timeout_c
 828  0145 c1fa              	cmpb	#250
 829  0147 2306              	bls	L503
 830                         ; 559 			final_TGW_LOC_b = TRUE;
 832  0149 1c005120          	bset	_canio_status2_flags_c,32
 834  014d 2007              	bra	L703
 835  014f                   L503:
 836                         ; 563 			canio_FL_HS_RQ_TGW_Timeout_c++;
 838  014f 72000d            	inc	_canio_FL_HS_RQ_TGW_Timeout_c
 839                         ; 564 			final_TGW_LOC_b = FALSE;
 841  0152 1d005120          	bclr	_canio_status2_flags_c,32
 842  0156                   L703:
 843                         ; 567 		if (final_TGW_LOC_b)
 845  0156 1f0051201c        	brclr	_canio_status2_flags_c,32,L113
 846                         ; 570 			if (canio_TGWTimeOut_Additional_WaitTime_c >= LOC_ENABLE_WAIT_TIME_K)
 848  015b f60001            	ldab	_canio_TGWTimeOut_Additional_WaitTime_c
 849  015e c17d              	cmpb	#125
 850  0160 2511              	blo	L313
 851                         ; 572 				FMemLibF_ReportDtc(DTC_NWM_LOC_WITH_TGW_K, ERROR_ON_K);
 853  0162 cc0001            	ldd	#1
 854  0165 3b                	pshd	
 855  0166 ce00c1            	ldx	#193
 856  0169 cc4700            	ldd	#18176
 857  016c 4a000000          	call	f_FMemLibF_ReportDtc
 859  0170 1b82              	leas	2,s
 862  0172 0a                	rtc	
 863  0173                   L313:
 864                         ; 576 				canio_TGWTimeOut_Additional_WaitTime_c++;
 866  0173 720001            	inc	_canio_TGWTimeOut_Additional_WaitTime_c
 868  0176 0a                	rtc	
 869  0177                   L113:
 870                         ; 582 			FMemLibF_ReportDtc(DTC_NWM_LOC_WITH_TGW_K, ERROR_OFF_K);
 872  0177 87                	clra	
 873  0178 c7                	clrb	
 874  0179 3b                	pshd	
 875  017a ce00c1            	ldx	#193
 876  017d 8647              	ldaa	#71
 877  017f 4a000000          	call	f_FMemLibF_ReportDtc
 879  0183 1b82              	leas	2,s
 880                         ; 583 			canio_TGWTimeOut_Additional_WaitTime_c = 0;
 882  0185 790001            	clr	_canio_TGWTimeOut_Additional_WaitTime_c
 883  0188                   L123:
 884                         ; 590 }
 887  0188 0a                	rtc	
 927                         ; 993 void canLF_WriteOriginalVIN(void)
 927                         ; 994 {
 928                         	switch	.ftext
 929  0189                   f_canLF_WriteOriginalVIN:
 931  0189 1b9d              	leas	-3,s
 932       00000003          OFST:	set	3
 935                         ; 996 	EE_BlockRead(EE_ODOMETER, (UInt8*)&temp_ODO_c[0]); 
 937  018b 1a80              	leax	OFST-3,s
 938  018d 34                	pshx	
 939  018e cc0025            	ldd	#37
 940  0191 4a000000          	call	f_EE_BlockRead
 942  0195 1b82              	leas	2,s
 943                         ; 1001 	if (update_original_vin_b)
 945  0197 1f00520132        	brclr	_canio_status_flags_c,1,L163
 946                         ; 1003 		if ((temp_ODO_c[0] == 0xFF) || (temp_ODO_c[0] == 0x00) && 
 946                         ; 1004 			(temp_ODO_c[1] == 0xFF) || (temp_ODO_c[1] == 0x00) &&
 946                         ; 1005 			(temp_ODO_c[2] == 0xFF) || (temp_ODO_c[2] <= 0x05))
 948  019c e680              	ldab	OFST-3,s
 949  019e c1ff              	cmpb	#255
 950  01a0 2717              	beq	L543
 952  01a2 046105            	tbne	b,L153
 954  01a5 e681              	ldab	OFST-2,s
 955  01a7 04810f            	ibeq	b,L543
 956  01aa                   L153:
 958  01aa e681              	ldab	OFST-2,s
 959  01ac 2605              	bne	L553
 961  01ae e682              	ldab	OFST-1,s
 962  01b0 048106            	ibeq	b,L543
 963  01b3                   L553:
 965  01b3 e682              	ldab	OFST-1,s
 966  01b5 c105              	cmpb	#5
 967  01b7 2215              	bhi	L163
 968  01b9                   L543:
 969                         ; 1011 			EE_BlockWrite(EE_VIN_ORIGINAL, (u_8Bit*)&canio_CurrentVIN[0]);
 971  01b9 cc0013            	ldd	#_canio_CurrentVIN
 972  01bc 3b                	pshd	
 973  01bd cc0048            	ldd	#72
 974  01c0 4a000000          	call	f_EE_BlockWrite
 976  01c4 1b82              	leas	2,s
 977                         ; 1012 			diag_load_bl_eep_backup_b = 1; //Its time to take backup for Boot data
 979  01c6 1c000040          	bset	_diag_io_lock3_flags_c,64
 980                         ; 1014 			update_original_vin_b = 0;
 982  01ca 1d005201          	bclr	_canio_status_flags_c,1
 984  01ce                   L163:
 985                         ; 1026 }
 988  01ce 1b83              	leas	3,s
 989  01d0 0a                	rtc	
1029                         ; 1045 void canLF_LOCTimeout_Condition(void)
1029                         ; 1046 {
1030                         	switch	.ftext
1031  01d1                   f_canLF_LOCTimeout_Condition:
1033  01d1 37                	pshb	
1034       00000001          OFST:	set	1
1037                         ; 1048 	unsigned char temp_result_c = 0;
1039  01d2 6980              	clr	OFST-1,s
1040                         ; 1050 	if (canio_RX_IGN_STATUS_c == IGN_RUN)
1042  01d4 f6003c            	ldab	_canio_RX_IGN_STATUS_c
1043  01d7 c104              	cmpb	#4
1044  01d9 2704              	beq	L104
1046                         ; 1057 		temp_result_c = 1;
1048  01db c601              	ldab	#1
1049  01dd 6b80              	stab	OFST-1,s
1050  01df                   L104:
1051                         ; 1061 	if ((canio_RX_BatteryVoltageLevel_c >= 90) && (canio_RX_BatteryVoltageLevel_c <= 160))
1053  01df f6004a            	ldab	_canio_RX_BatteryVoltageLevel_c
1054  01e2 c15a              	cmpb	#90
1055  01e4 2504              	blo	L304
1057  01e6 c1a0              	cmpb	#160
1058  01e8 2304              	bls	L504
1060  01ea                   L304:
1061                         ; 1068 		temp_result_c = 1;
1063  01ea c601              	ldab	#1
1064  01ec 6b80              	stab	OFST-1,s
1065  01ee                   L504:
1066                         ; 1097 	if (canio_canbus_off_b == FALSE)
1068  01ee 1f00510104        	brclr	_canio_status2_flags_c,1,L114
1070                         ; 1104 		temp_result_c = 1;
1072  01f3 c601              	ldab	#1
1073  01f5 6b80              	stab	OFST-1,s
1074  01f7                   L114:
1075                         ; 1109 	if (temp_result_c)
1077  01f7 e680              	ldab	OFST-1,s
1078  01f9 270b              	beq	L314
1079                         ; 1111 		canio_LOC_Timeout_Count_c = 0;
1081  01fb 790008            	clr	_canio_LOC_Timeout_Count_c
1082                         ; 1113 		canio_CBCTimeOut_Additional_WaitTime_c = 0;
1084  01fe 790002            	clr	_canio_CBCTimeOut_Additional_WaitTime_c
1085                         ; 1114 		canio_TGWTimeOut_Additional_WaitTime_c = 0;
1087  0201 790001            	clr	_canio_TGWTimeOut_Additional_WaitTime_c
1089  0204 200a              	bra	L514
1090  0206                   L314:
1091                         ; 1120 		if (canio_LOC_Timeout_Count_c < LOC_ENABLE_WAIT_TIME_K)
1093  0206 f60008            	ldab	_canio_LOC_Timeout_Count_c
1094  0209 c17d              	cmpb	#125
1095  020b 2403              	bhs	L514
1096                         ; 1122 			canio_LOC_Timeout_Count_c++;
1098  020d 720008            	inc	_canio_LOC_Timeout_Count_c
1099  0210                   L514:
1100                         ; 1127 	if (canio_LOC_Timeout_Count_c >= LOC_ENABLE_WAIT_TIME_K)
1102  0210 f60008            	ldab	_canio_LOC_Timeout_Count_c
1103  0213 c17d              	cmpb	#125
1104  0215 2508              	blo	L124
1105                         ; 1129 	    canF_CBC_LOC_DTC ();
1107  0217 4a00e7e7          	call	f_canF_CBC_LOC_DTC
1109                         ; 1130 	    canF_TGW_LOC_DTC ();
1111  021b 4a013838          	call	f_canF_TGW_LOC_DTC
1113  021f                   L124:
1114                         ; 1134 }
1117  021f 1b81              	leas	1,s
1118  0221 0a                	rtc	
1165                         ; 1239 void  CSWM_VehStart_PKT_ManIndFunc(void)
1165                         ; 1240 {
1166                         	switch	.ftext
1167  0222                   f_CSWM_VehStart_PKT_ManIndFunc:
1169  0222 3b                	pshd	
1170       00000002          OFST:	set	2
1173                         ; 1244 	local_flag_CmdIgnSts_c = IlGetClrCmdIgnStsIndication();
1175  0223 cc001a            	ldd	#26
1176  0226 4a000000          	call	f_IlGetSignalIndicationFlag
1178  022a 6b80              	stab	OFST-2,s
1179                         ; 1245 	local_flag_RemStActvSts_c = IlGetClrRemStActvStsIndication();
1181  022c cc001b            	ldd	#27
1182  022f 4a000000          	call	f_IlGetSignalIndicationFlag
1184  0233 6b81              	stab	OFST-1,s
1185                         ; 1247 	if(local_flag_CmdIgnSts_c)
1187  0235 e680              	ldab	OFST-2,s
1188  0237 270d              	beq	L544
1189                         ; 1249 		if (diag_io_ign_lock_b == FALSE)
1191  0239 1e00000408        	brset	_diag_io_lock_flags_c,4,L544
1192                         ; 1251 			canio_RX_IGN_STATUS_c = IlGetRxCmdIgnSts();			
1194  023e f60000            	ldab	_CBC_I4
1195  0241 c407              	andb	#7
1196  0243 7b003c            	stab	_canio_RX_IGN_STATUS_c
1197  0246                   L544:
1198                         ; 1260 	if(local_flag_RemStActvSts_c)
1200  0246 e681              	ldab	OFST-1,s
1201  0248 270e              	beq	L154
1202                         ; 1263 		canio_RX_IgnRun_RemSt_c = IlGetRxRemStActvSts();
1204  024a f60000            	ldab	_CBC_I4
1205  024d 55                	rolb	
1206  024e 55                	rolb	
1207  024f 55                	rolb	
1208  0250 c401              	andb	#1
1209  0252 7b000f            	stab	_canio_RX_IgnRun_RemSt_c
1210                         ; 1265 		canio_IgnRun_RemSt_Timeout_c = 0;
1212  0255 79000c            	clr	_canio_IgnRun_RemSt_Timeout_c
1214  0258                   L154:
1215                         ; 1272 }
1218  0258 31                	puly	
1219  0259 0a                	rtc	
1255                         ; 1287 void  CSWM_EngRPM_ManIndFunc(void)
1255                         ; 1288 {
1256                         	switch	.ftext
1257  025a                   f_CSWM_EngRPM_ManIndFunc:
1259  025a 37                	pshb	
1260       00000001          OFST:	set	1
1263                         ; 1291 	local_flag_EngineSpeed_c = IlGetClrEngineSpeedIndication();
1265  025b cc0016            	ldd	#22
1266  025e 4a000000          	call	f_IlGetSignalIndicationFlag
1268                         ; 1293 	if(local_flag_EngineSpeed_c)
1270  0262 044106            	tbeq	b,L174
1271                         ; 1296 		canio_RX_EngineSpeed_c = IlGetRxEngineSpeed();
1273  0265 180c0006003a      	movb	_STATUS_B_ECM+6,_canio_RX_EngineSpeed_c
1275  026b                   L174:
1276                         ; 1302 }
1279  026b 1b81              	leas	1,s
1280  026d 0a                	rtc	
1317                         ; 1318 void  CSWM_FL_HS_RQ_TGW_ManIndFunc(void)
1317                         ; 1319 {
1318                         	switch	.ftext
1319  026e                   f_CSWM_FL_HS_RQ_TGW_ManIndFunc:
1321  026e 37                	pshb	
1322       00000001          OFST:	set	1
1325                         ; 1322 	local_FL_HS_RQ_flag_c = IlGetClrFL_HS_RQ_TGWStsIndication();
1327  026f cc000f            	ldd	#15
1328  0272 4a000000          	call	f_IlGetSignalIndicationFlag
1330                         ; 1324 	if(local_FL_HS_RQ_flag_c)
1332  0276 04410f            	tbeq	b,L115
1333                         ; 1327 		canio_RX_FL_HS_RQ_TGW_c = IlGetRxFL_HS_RQ_TGWSts(); 
1335  0279 f60001            	ldab	_TGW_A1+1
1336  027c c430              	andb	#48
1337  027e 54                	lsrb	
1338  027f 54                	lsrb	
1339  0280 54                	lsrb	
1340  0281 54                	lsrb	
1341  0282 7b0046            	stab	_canio_hvac_heated_seat_request_c
1342                         ; 1329 		canio_FL_HS_RQ_TGW_Timeout_c = 0;
1344  0285 79000d            	clr	_canio_FL_HS_RQ_TGW_Timeout_c
1346  0288                   L115:
1347                         ; 1335 } 
1350  0288 1b81              	leas	1,s
1351  028a 0a                	rtc	
1387                         ; 1350 void  CSWM_FR_HS_RQ_TGW_ManIndFunc(void)
1387                         ; 1351 {
1388                         	switch	.ftext
1389  028b                   f_CSWM_FR_HS_RQ_TGW_ManIndFunc:
1391  028b 37                	pshb	
1392       00000001          OFST:	set	1
1395                         ; 1354 	local_FR_HS_RQ_flag_c = IlGetClrFR_HS_RQ_TGWStsIndication();
1397  028c cc000e            	ldd	#14
1398  028f 4a000000          	call	f_IlGetSignalIndicationFlag
1400                         ; 1356 	if(local_FR_HS_RQ_flag_c)
1402  0293 044108            	tbeq	b,L135
1403                         ; 1359 		canio_RX_FR_HS_RQ_TGW_c = IlGetRxFR_HS_RQ_TGWSts();
1405  0296 f60001            	ldab	_TGW_A1+1
1406  0299 c403              	andb	#3
1407  029b 7b0047            	stab	_canio_hvac_heated_seat_request_c+1
1409  029e                   L135:
1410                         ; 1365 } 
1413  029e 1b81              	leas	1,s
1414  02a0 0a                	rtc	
1450                         ; 1380 void  CSWM_HSW_RQ_TGW_ManIndFunc(void)
1450                         ; 1381 { 
1451                         	switch	.ftext
1452  02a1                   f_CSWM_HSW_RQ_TGW_ManIndFunc:
1454  02a1 37                	pshb	
1455       00000001          OFST:	set	1
1458                         ; 1384 	local_HSW_RQ_flag_c = IlGetClrHSW_RQ_TGWStsIndication();
1460  02a2 cc000d            	ldd	#13
1461  02a5 4a000000          	call	f_IlGetSignalIndicationFlag
1463                         ; 1386 	if(local_HSW_RQ_flag_c)
1465  02a9 044109            	tbeq	b,L155
1466                         ; 1389 		canio_RX_HSW_RQ_TGW_c = IlGetRxHSW_RQ_TGWSts();
1468  02ac f60000            	ldab	_TGW_A1
1469  02af c406              	andb	#6
1470  02b1 54                	lsrb	
1471  02b2 7b0041            	stab	_canio_RX_HSW_RQ_TGW_c
1473  02b5                   L155:
1474                         ; 1398 }
1477  02b5 1b81              	leas	1,s
1478  02b7 0a                	rtc	
1514                         ; 1413 void  CSWM_ExtTemp_ManIndFunc(void)
1514                         ; 1414 {
1515                         	switch	.ftext
1516  02b8                   f_CSWM_ExtTemp_ManIndFunc:
1518  02b8 37                	pshb	
1519       00000001          OFST:	set	1
1522                         ; 1417 	local_ExtTemp_flag_c = IlGetClrExternalTemperatureIndication();
1524  02b9 cc0010            	ldd	#16
1525  02bc 4a000000          	call	f_IlGetSignalIndicationFlag
1527                         ; 1419 	if(local_ExtTemp_flag_c)
1529  02c0 044106            	tbeq	b,L175
1530                         ; 1422 		canio_RX_ExternalTemperature_c = IlGetRxExternalTemperature();
1532  02c3 180c00000012      	movb	_ENVIRONMENTAL_CONDITIONS,_canio_RX_ExternalTemperature_c
1534  02c9                   L175:
1535                         ; 1428 }
1538  02c9 1b81              	leas	1,s
1539  02cb 0a                	rtc	
1575                         ; 1443 void   CSWM_BatVolt_ManIndFunc(void)
1575                         ; 1444 {
1576                         	switch	.ftext
1577  02cc                   f_CSWM_BatVolt_ManIndFunc:
1579  02cc 37                	pshb	
1580       00000001          OFST:	set	1
1583                         ; 1447 	local_BatVolt_flag_c = IlGetClrBatteryVoltageLevelIndication();
1585  02cd cc0011            	ldd	#17
1586  02d0 4a000000          	call	f_IlGetSignalIndicationFlag
1588                         ; 1449 	if(local_BatVolt_flag_c)
1590  02d4 044112            	tbeq	b,L116
1591                         ; 1452 		canio_RX_BatteryVoltageLevel_c = IlGetRxBatteryVoltageLevel();
1593  02d7 f60001            	ldab	_ENVIRONMENTAL_CONDITIONS+1
1594  02da c47f              	andb	#127
1595                         ; 1454 		canio_RX_BatteryVoltageLevel_c = (u_8Bit)( (canio_RX_BatteryVoltageLevel_c * 16) / 10);
1597  02dc 8610              	ldaa	#16
1598  02de 12                	mul	
1599  02df ce000a            	ldx	#10
1600  02e2 1815              	idivs	
1601  02e4 b754              	tfr	x,d
1602  02e6 7b004a            	stab	_canio_RX_BatteryVoltageLevel_c
1604  02e9                   L116:
1605                         ; 1460 }
1608  02e9 1b81              	leas	1,s
1609  02eb 0a                	rtc	
1714                         .const:	section	.data
1715                         	even
1716  0000                   L44:
1717  0000 0000000a          	dc.l	10
1718                         ; 1475 void   CSWM_ODO_ManIndFunc(void)
1718                         ; 1476 {
1719                         	switch	.ftext
1720  02ec                   f_CSWM_ODO_ManIndFunc:
1722  02ec 1b9b              	leas	-5,s
1723       00000005          OFST:	set	5
1726                         ; 1480 	local_TotalOdo_flag_c = IlGetClrTotalOdometerIndication();
1728  02ee cc000c            	ldd	#12
1729  02f1 4a000000          	call	f_IlGetSignalIndicationFlag
1731  02f5 6b84              	stab	OFST-1,s
1732                         ; 1482 	if(local_TotalOdo_flag_c)
1734  02f7 271b              	beq	L366
1735                         ; 1486 		temp_TotalOdo_l.l = ( (unsigned long) IlGetRxTotalOdometer() * 10);
1737  02f9 4a000000          	call	f_IlGetRxTotalOdometer
1739  02fd cd0000            	ldy	#L44
1740  0300 160000            	jsr	c_lmul
1742  0303 6c82              	std	OFST-3,s
1743  0305 6e80              	stx	OFST-5,s
1744                         ; 1488 		canio_RX_TotalOdo_c[0] = temp_TotalOdo_l.lw.hbyte;
1746  0307 180d81004b        	movb	OFST-4,s,_canio_RX_TotalOdo_c
1747                         ; 1489 		canio_RX_TotalOdo_c[1] = temp_TotalOdo_l.lw.lbyte;
1749  030c 180d82004c        	movb	OFST-3,s,_canio_RX_TotalOdo_c+1
1750                         ; 1490 		canio_RX_TotalOdo_c[2] = temp_TotalOdo_l.lw.llbyte;
1752  0311 7b004d            	stab	_canio_RX_TotalOdo_c+2
1754  0314                   L366:
1755                         ; 1497 }
1758  0314 1b85              	leas	5,s
1759  0316 0a                	rtc	
1796                         ; 1513 void   CSWM_CFG_RQ_ManIndFunc(void)
1796                         ; 1514 {
1797                         	switch	.ftext
1798  0317                   f_CSWM_CFG_RQ_ManIndFunc:
1800  0317 37                	pshb	
1801       00000001          OFST:	set	1
1804                         ; 1517 	local_CFG_RQ_flag_c = IlGetClrCFG_FeatureCntrlIndication();
1806  0318 cc001c            	ldd	#28
1807  031b 4a000000          	call	f_IlGetSignalIndicationFlag
1809  031f 6b80              	stab	OFST-1,s
1810                         ; 1518 	local_CFG_RQ_flag_c |= IlGetClrCFG_STAT_RQCntrlIndication();
1812  0321 cc001d            	ldd	#29
1813  0324 4a000000          	call	f_IlGetSignalIndicationFlag
1815  0328 ea80              	orab	OFST-1,s
1816                         ; 1520 	if(local_CFG_RQ_flag_c)
1818  032a 270e              	beq	L307
1819                         ; 1523 		canio_RX_Rem_CFG_FEATURE_c = IlGetRxCFG_FeatureCntrl();
1821  032c 180c00000011      	movb	_CFG_RQ,_canio_RX_Rem_CFG_FEATURE_c
1822                         ; 1525 		canio_RX_Rem_CFG_STAT_RQ_c = IlGetRxCFG_STAT_RQCntrl();
1824  0332 f60001            	ldab	_CFG_RQ+1
1825  0335 c403              	andb	#3
1826  0337 7b0010            	stab	_canio_RX_Rem_CFG_STAT_RQ_c
1828  033a                   L307:
1829                         ; 1531 }
1832  033a 1b81              	leas	1,s
1833  033c 0a                	rtc	
1870                         ; 1547 void   CSWM_StW_Actn_Rq_ManIndFunc(void)
1870                         ; 1548 {
1871                         	switch	.ftext
1872  033d                   f_CSWM_StW_Actn_Rq_ManIndFunc:
1874  033d 37                	pshb	
1875       00000001          OFST:	set	1
1878                         ; 1552 	local_StW_Actn_Rq_flag_c = IlGetClrStW_TempSens_FltStsIndication();
1880  033e cc0012            	ldd	#18
1881  0341 4a000000          	call	f_IlGetSignalIndicationFlag
1883  0345 6b80              	stab	OFST-1,s
1884                         ; 1553 	local_StW_Actn_Rq_flag_c |= IlGetClrStW_TempStsIndication();
1886  0347 cc0013            	ldd	#19
1887  034a 4a000000          	call	f_IlGetSignalIndicationFlag
1889  034e ea80              	orab	OFST-1,s
1890                         ; 1555 	if(local_StW_Actn_Rq_flag_c)
1892  0350 2710              	beq	L327
1893                         ; 1558 	canio_RX_StW_TempSts_c = IlGetRxStW_TempSts();
1895  0352 180c00060040      	movb	_StW_Actn_Rq+6,_canio_RX_StW_TempSts_c
1896                         ; 1560 	canio_RX_StW_TempSens_FltSts_c = IlGetRxStW_TempSens_FltSts();
1898  0358 f60005            	ldab	_StW_Actn_Rq+5
1899  035b c40c              	andb	#12
1900  035d 54                	lsrb	
1901  035e 54                	lsrb	
1902  035f 7b003f            	stab	_canio_RX_StW_TempSens_FltSts_c
1904  0362                   L327:
1905                         ; 1566 }
1908  0362 1b81              	leas	1,s
1909  0364 0a                	rtc	
1945                         ; 1581 void   CSWM_EngineSts_ManIndFunc(void)
1945                         ; 1582 {
1946                         	switch	.ftext
1947  0365                   f_CSWM_EngineSts_ManIndFunc:
1949  0365 37                	pshb	
1950       00000001          OFST:	set	1
1953                         ; 1585 	local_EngineSts_flag_c = IlGetClrEngineStsIndication();
1955  0366 cc0014            	ldd	#20
1956  0369 4a000000          	call	f_IlGetSignalIndicationFlag
1958  036d d7                	tstb	
1959                         ; 1587 	if(local_EngineSts_flag_c)
1961  036e 270b              	beq	L347
1962                         ; 1590 		canio_RX_EngineSts_c = IlGetRxEngineSts();
1964  0370 f60000            	ldab	_STATUS_C_CAN
1965  0373 55                	rolb	
1966  0374 55                	rolb	
1967  0375 55                	rolb	
1968  0376 c403              	andb	#3
1969  0378 7b003e            	stab	_canio_RX_EngineSts_c
1971  037b                   L347:
1972                         ; 1596 }
1975  037b 1b81              	leas	1,s
1976  037d 0a                	rtc	
2012                         ; 1612 void   CSWM_EngineSpeedValidData_ManIndFunc(void)
2012                         ; 1613 {
2013                         	switch	.ftext
2014  037e                   f_CSWM_EngineSpeedValidData_ManIndFunc:
2016  037e 37                	pshb	
2017       00000001          OFST:	set	1
2020                         ; 1616 	local_EngSpeed_flag_c = IlGetClrEngineSpeedValidDataIndication();
2022  037f cc0017            	ldd	#23
2023  0382 4a000000          	call	f_IlGetSignalIndicationFlag
2025  0386 d7                	tstb	
2026                         ; 1618 	if(local_EngSpeed_flag_c)
2028  0387 270c              	beq	L367
2029                         ; 1621 	canio_RX_EngineSpeedValidData_c = IlGetRxEngineSpeedValidData();
2031  0389 f60007            	ldab	_STATUS_B_ECM+7
2032  038c 55                	rolb	
2033  038d 55                	rolb	
2034  038e 55                	rolb	
2035  038f 55                	rolb	
2036  0390 c401              	andb	#1
2037  0392 7b003d            	stab	_canio_RX_EngineSpeedValidData_c
2039  0395                   L367:
2040                         ; 1627 }
2043  0395 1b81              	leas	1,s
2044  0397 0a                	rtc	
2083                         ; 1642 void   CSWM_CFG_DATA_RESP_ManIndFunc(void)
2083                         ; 1643 {
2084                         	switch	.ftext
2085  0398                   f_CSWM_CFG_DATA_RESP_ManIndFunc:
2087  0398 37                	pshb	
2088       00000001          OFST:	set	1
2091                         ; 1647 	local_Config_c = IlGetClrDigit_01Indication();
2093  0399 cc000b            	ldd	#11
2094  039c 4a000000          	call	f_IlGetSignalIndicationFlag
2096  03a0 6b80              	stab	OFST-1,s
2097                         ; 1649 	if(local_Config_c)
2099  03a2 182700f9          	beq	L3001
2100                         ; 1653 		IlPutTxDigit_01(ru_eep_PROXI_Cfg.s.PROXI_CRC.s.Digit1);
2102  03a6 f60005            	ldab	_ru_eep_PROXI_Cfg+5
2103  03a9 54                	lsrb	
2104  03aa 54                	lsrb	
2105  03ab 54                	lsrb	
2106  03ac 54                	lsrb	
2107  03ad 87                	clra	
2108  03ae 4a000000          	call	f_IlPutTxDigit_01
2110                         ; 1654 		IlPutTxDigit_02(ru_eep_PROXI_Cfg.s.PROXI_CRC.s.Digit2);
2112  03b2 4a000000          	call	f_VStdSuspendAllInterrupts
2116  03b6 f60004            	ldab	_ru_eep_PROXI_Cfg+4
2117  03b9 1d00040f          	bclr	_CFG_DATA_CODE_RSP_CSWM+4,15
2118  03bd c40f              	andb	#15
2119  03bf fa0004            	orab	_CFG_DATA_CODE_RSP_CSWM+4
2120  03c2 7b0004            	stab	_CFG_DATA_CODE_RSP_CSWM+4
2123  03c5 4a000000          	call	f_VStdResumeAllInterrupts
2125                         ; 1655 		IlPutTxDigit_03(ru_eep_PROXI_Cfg.s.PROXI_CRC.s.Digit3);
2128  03c9 4a000000          	call	f_VStdSuspendAllInterrupts
2132  03cd f60004            	ldab	_ru_eep_PROXI_Cfg+4
2133  03d0 1d0004f0          	bclr	_CFG_DATA_CODE_RSP_CSWM+4,240
2134  03d4 c4f0              	andb	#240
2135  03d6 fa0004            	orab	_CFG_DATA_CODE_RSP_CSWM+4
2136  03d9 7b0004            	stab	_CFG_DATA_CODE_RSP_CSWM+4
2139  03dc 4a000000          	call	f_VStdResumeAllInterrupts
2141                         ; 1656 		IlPutTxDigit_04(ru_eep_PROXI_Cfg.s.PROXI_CRC.s.Digit4);
2144  03e0 4a000000          	call	f_VStdSuspendAllInterrupts
2148  03e4 f60003            	ldab	_ru_eep_PROXI_Cfg+3
2149  03e7 1d00030f          	bclr	_CFG_DATA_CODE_RSP_CSWM+3,15
2150  03eb c40f              	andb	#15
2151  03ed fa0003            	orab	_CFG_DATA_CODE_RSP_CSWM+3
2152  03f0 7b0003            	stab	_CFG_DATA_CODE_RSP_CSWM+3
2155  03f3 4a000000          	call	f_VStdResumeAllInterrupts
2157                         ; 1657 		IlPutTxDigit_05(ru_eep_PROXI_Cfg.s.PROXI_CRC.s.Digit5);
2160  03f7 4a000000          	call	f_VStdSuspendAllInterrupts
2164  03fb f60003            	ldab	_ru_eep_PROXI_Cfg+3
2165  03fe 1d0003f0          	bclr	_CFG_DATA_CODE_RSP_CSWM+3,240
2166  0402 c4f0              	andb	#240
2167  0404 fa0003            	orab	_CFG_DATA_CODE_RSP_CSWM+3
2168  0407 7b0003            	stab	_CFG_DATA_CODE_RSP_CSWM+3
2171  040a 4a000000          	call	f_VStdResumeAllInterrupts
2173                         ; 1658 		IlPutTxDigit_06(ru_eep_PROXI_Cfg.s.PROXI_IDVeh.s.Digit6);
2176  040e 4a000000          	call	f_VStdSuspendAllInterrupts
2180  0412 f60002            	ldab	_ru_eep_PROXI_Cfg+2
2181  0415 1d00020f          	bclr	_CFG_DATA_CODE_RSP_CSWM+2,15
2182  0419 c40f              	andb	#15
2183  041b fa0002            	orab	_CFG_DATA_CODE_RSP_CSWM+2
2184  041e 7b0002            	stab	_CFG_DATA_CODE_RSP_CSWM+2
2187  0421 4a000000          	call	f_VStdResumeAllInterrupts
2189                         ; 1659 		IlPutTxDigit_07(ru_eep_PROXI_Cfg.s.PROXI_IDVeh.s.Digit7);
2192  0425 4a000000          	call	f_VStdSuspendAllInterrupts
2196  0429 f60002            	ldab	_ru_eep_PROXI_Cfg+2
2197  042c 1d0002f0          	bclr	_CFG_DATA_CODE_RSP_CSWM+2,240
2198  0430 c4f0              	andb	#240
2199  0432 fa0002            	orab	_CFG_DATA_CODE_RSP_CSWM+2
2200  0435 7b0002            	stab	_CFG_DATA_CODE_RSP_CSWM+2
2203  0438 4a000000          	call	f_VStdResumeAllInterrupts
2205                         ; 1660 		IlPutTxDigit_08(ru_eep_PROXI_Cfg.s.PROXI_IDVeh.s.Digit8);
2208  043c 4a000000          	call	f_VStdSuspendAllInterrupts
2212  0440 f60001            	ldab	_ru_eep_PROXI_Cfg+1
2213  0443 1d00010f          	bclr	_CFG_DATA_CODE_RSP_CSWM+1,15
2214  0447 c40f              	andb	#15
2215  0449 fa0001            	orab	_CFG_DATA_CODE_RSP_CSWM+1
2216  044c 7b0001            	stab	_CFG_DATA_CODE_RSP_CSWM+1
2219  044f 4a000000          	call	f_VStdResumeAllInterrupts
2221                         ; 1661 		IlPutTxDigit_09(ru_eep_PROXI_Cfg.s.PROXI_IDVeh.s.Digit9);
2224  0453 4a000000          	call	f_VStdSuspendAllInterrupts
2228  0457 f60001            	ldab	_ru_eep_PROXI_Cfg+1
2229  045a 1d0001f0          	bclr	_CFG_DATA_CODE_RSP_CSWM+1,240
2230  045e c4f0              	andb	#240
2231  0460 fa0001            	orab	_CFG_DATA_CODE_RSP_CSWM+1
2232  0463 7b0001            	stab	_CFG_DATA_CODE_RSP_CSWM+1
2235  0466 4a000000          	call	f_VStdResumeAllInterrupts
2237                         ; 1662 		IlPutTxDigit_10(ru_eep_PROXI_Cfg.s.PROXI_IDVeh.s.Digit10);
2240  046a 4a000000          	call	f_VStdSuspendAllInterrupts
2244  046e f60000            	ldab	_ru_eep_PROXI_Cfg
2245  0471 1d00000f          	bclr	_CFG_DATA_CODE_RSP_CSWM,15
2246  0475 c40f              	andb	#15
2247  0477 fa0000            	orab	_CFG_DATA_CODE_RSP_CSWM
2248  047a 7b0000            	stab	_CFG_DATA_CODE_RSP_CSWM
2251  047d 4a000000          	call	f_VStdResumeAllInterrupts
2253                         ; 1663 		IlPutTxDigit_11(ru_eep_PROXI_Cfg.s.PROXI_IDVeh.s.Digit11);
2256  0481 4a000000          	call	f_VStdSuspendAllInterrupts
2260  0485 f60000            	ldab	_ru_eep_PROXI_Cfg
2261  0488 1d0000f0          	bclr	_CFG_DATA_CODE_RSP_CSWM,240
2262  048c c4f0              	andb	#240
2263  048e fa0000            	orab	_CFG_DATA_CODE_RSP_CSWM
2264  0491 7b0000            	stab	_CFG_DATA_CODE_RSP_CSWM
2267  0494 4a000000          	call	f_VStdResumeAllInterrupts
2269                         ; 1665 		CanTransmit(CanTxCFG_DATA_CODE_RSP_CSWM);
2272  0498 cc0001            	ldd	#1
2273  049b 4a000000          	call	f_CanTransmit
2276  049f                   L3001:
2277                         ; 1671 }
2280  049f 1b81              	leas	1,s
2281  04a1 0a                	rtc	
2351                         ; 1688 void  CSWM_VIN_ManIndFunc (void)
2351                         ; 1689 {
2352                         	switch	.ftext
2353  04a2                   f_CSWM_VIN_ManIndFunc:
2355  04a2 1b90              	leas	-16,s
2356       00000010          OFST:	set	16
2359                         ; 1694 	local_VIN_MSG_flag_c = IlGetClrVIN_MSGIndication();
2361  04a4 cc0018            	ldd	#24
2362  04a7 4a000000          	call	f_IlGetSignalIndicationFlag
2364  04ab 6b8f              	stab	OFST-1,s
2365                         ; 1696 	if(local_VIN_MSG_flag_c)
2367  04ad 272c              	beq	L5301
2368                         ; 1698 		canio_Rx_VIN_MSG_c = IlGetRxVIN_MSG();
2370  04af f60000            	ldab	_VIN
2371  04b2 c403              	andb	#3
2372  04b4 6b8e              	stab	OFST-2,s
2373                         ; 1700 		IlGetRxVIN_DATA(local_VIN_DATA);
2375  04b6 b774              	tfr	s,d
2376  04b8 c30007            	addd	#-9+OFST
2377  04bb 4a000000          	call	f_IlGetRxVIN_DATA
2379                         ; 1702 		local_buffer[0] = local_VIN_DATA[6];
2381  04bf 180a8d80          	movb	OFST-3,s,OFST-16,s
2382                         ; 1703 		local_buffer[1] = local_VIN_DATA[5];
2384  04c3 180a8c81          	movb	OFST-4,s,OFST-15,s
2385                         ; 1704 		local_buffer[2] = local_VIN_DATA[4];
2387  04c7 180a8b82          	movb	OFST-5,s,OFST-14,s
2388                         ; 1705 		local_buffer[3] = local_VIN_DATA[3];
2390  04cb 180a8a83          	movb	OFST-6,s,OFST-13,s
2391                         ; 1706 		local_buffer[4] = local_VIN_DATA[2];
2393  04cf 180a8984          	movb	OFST-7,s,OFST-12,s
2394                         ; 1707 		local_buffer[5] = local_VIN_DATA[1];
2396  04d3 180a8885          	movb	OFST-8,s,OFST-11,s
2397                         ; 1708 		local_buffer[6] = local_VIN_DATA[0];
2399  04d7 180a8786          	movb	OFST-9,s,OFST-10,s
2401  04db                   L5301:
2402                         ; 1717 	if(canio_Rx_VIN_MSG_c == 0x00)
2404  04db e68e              	ldab	OFST-2,s
2405  04dd 2624              	bne	L7301
2406                         ; 1720 		if (!memcmp((canio_CurrentVIN), (&local_buffer[0]),7) )
2408  04df cc0007            	ldd	#7
2409  04e2 3b                	pshd	
2410  04e3 1a82              	leax	OFST-14,s
2411  04e5 34                	pshx	
2412  04e6 cc0013            	ldd	#_canio_CurrentVIN
2413  04e9 160000            	jsr	_memcmp
2415  04ec 1b84              	leas	4,s
2416  04ee 1827008d          	beq	L5401
2418                         ; 1727 			memcpy(canio_CurrentVIN, (&local_buffer[0]), 7);
2420  04f2 cd0013            	ldy	#_canio_CurrentVIN
2421  04f5 1a80              	leax	OFST-16,s
2422  04f7 cc0007            	ldd	#7
2423  04fa                   L26:
2424  04fa 180a3070          	movb	1,x+,1,y+
2425  04fe 0434f9            	dbne	d,L26
2426                         ; 1729 			Canio_VehIdNumberCounter_c = 0;
2428  0501 2079              	bra	LC002
2429  0503                   L7301:
2430                         ; 1733 	else if(canio_Rx_VIN_MSG_c == 0x01)
2432  0503 c101              	cmpb	#1
2433  0505 2622              	bne	L7401
2434                         ; 1736 		if (!memcmp((canio_CurrentVIN+7), (&local_buffer[0]),7) )
2436  0507 cc0007            	ldd	#7
2437  050a 3b                	pshd	
2438  050b 1a82              	leax	OFST-14,s
2439  050d 34                	pshx	
2440  050e cc001a            	ldd	#_canio_CurrentVIN+7
2441  0511 160000            	jsr	_memcmp
2443  0514 1b84              	leas	4,s
2444  0516 2767              	beq	L5401
2446                         ; 1743 			memcpy((canio_CurrentVIN+7), (&local_buffer[0]), 7);
2448  0518 cd001a            	ldy	#_canio_CurrentVIN+7
2449  051b 1a80              	leax	OFST-16,s
2450  051d cc0007            	ldd	#7
2451  0520                   L46:
2452  0520 180a3070          	movb	1,x+,1,y+
2453  0524 0434f9            	dbne	d,L46
2454                         ; 1745 			Canio_VehIdNumberCounter_c = 0;
2456  0527 2053              	bra	LC002
2457  0529                   L7401:
2458                         ; 1750 	else if(canio_Rx_VIN_MSG_c == 0x02)
2460  0529 c102              	cmpb	#2
2461  052b 2652              	bne	L5401
2462                         ; 1753 		if (!memcmp((canio_CurrentVIN+14), (&local_buffer[0]), 3))
2464  052d cc0003            	ldd	#3
2465  0530 3b                	pshd	
2466  0531 1a82              	leax	OFST-14,s
2467  0533 34                	pshx	
2468  0534 cc0021            	ldd	#_canio_CurrentVIN+14
2469  0537 160000            	jsr	_memcmp
2471  053a 1b84              	leas	4,s
2472  053c 2631              	bne	L1601
2473                         ; 1756 			Canio_VehIdNumberCounter_c++;
2475  053e 72000b            	inc	_Canio_VehIdNumberCounter_c
2476                         ; 1757 			if (Canio_VehIdNumberCounter_c >= VIN_MAX_COUNT_K)
2478  0541 f6000b            	ldab	_Canio_VehIdNumberCounter_c
2479  0544 c10a              	cmpb	#10
2480  0546 2537              	blo	L5401
2481                         ; 1760 				Canio_VehIdNumberCounter_c = 0;
2483  0548 79000b            	clr	_Canio_VehIdNumberCounter_c
2484                         ; 1763 				EE_BlockRead(EE_VIN_ORIGINAL, (u_8Bit*)&canio_Vehicle_ID_Number[0]);
2486  054b cc0024            	ldd	#_canio_Vehicle_ID_Number
2487  054e 3b                	pshd	
2488  054f cc0048            	ldd	#72
2489  0552 4a000000          	call	f_EE_BlockRead
2491                         ; 1765 				if (memcmp((canio_Vehicle_ID_Number), canio_CurrentVIN, 17))
2493  0556 cc0011            	ldd	#17
2494  0559 6c80              	std	0,s
2495  055b cc0013            	ldd	#_canio_CurrentVIN
2496  055e 3b                	pshd	
2497  055f cc0024            	ldd	#_canio_Vehicle_ID_Number
2498  0562 160000            	jsr	_memcmp
2500  0565 1b84              	leas	4,s
2501  0567 2716              	beq	L5401
2502                         ; 1768 					update_original_vin_b = 1;
2504  0569 1c005201          	bset	_canio_status_flags_c,1
2505  056d 2010              	bra	L5401
2506  056f                   L1601:
2507                         ; 1775 			memcpy((canio_CurrentVIN+14), (&local_buffer[0]), 3);
2509  056f cd0021            	ldy	#_canio_CurrentVIN+14
2510  0572 1a80              	leax	OFST-16,s
2511  0574 18023171          	movw	2,x+,2,y+
2512  0578 180a0040          	movb	0,x,0,y
2513                         ; 1777 			Canio_VehIdNumberCounter_c = 0;
2515  057c                   LC002:
2516  057c 79000b            	clr	_Canio_VehIdNumberCounter_c
2517  057f                   L5401:
2518                         ; 1871 }
2521  057f 1bf010            	leas	16,s
2522  0582 0a                	rtc	
2630                         	xdef	f_canLF_LOCTimeout_Condition
2631                         	xdef	f_canLF_WriteOriginalVIN
2632                         	switch	.bss
2633  0000                   _canio_RemSt_stat_updated:
2634  0000 00                	ds.b	1
2635                         	xdef	_canio_RemSt_stat_updated
2636  0001                   _canio_TGWTimeOut_Additional_WaitTime_c:
2637  0001 00                	ds.b	1
2638                         	xdef	_canio_TGWTimeOut_Additional_WaitTime_c
2639  0002                   _canio_CBCTimeOut_Additional_WaitTime_c:
2640  0002 00                	ds.b	1
2641                         	xdef	_canio_CBCTimeOut_Additional_WaitTime_c
2642  0003                   _EcuCfg5_counter_c:
2643  0003 00                	ds.b	1
2644                         	xdef	_EcuCfg5_counter_c
2645  0004                   _canio_current_EcuCfg5_stat_c:
2646  0004 00                	ds.b	1
2647                         	xdef	_canio_current_EcuCfg5_stat_c
2648  0005                   _aux_c:
2649  0005 00                	ds.b	1
2650                         	xdef	_aux_c
2651  0006                   _canio_current_veh_line_c:
2652  0006 00                	ds.b	1
2653                         	xdef	_canio_current_veh_line_c
2654  0007                   _canio_Debounce_Time_c:
2655  0007 00                	ds.b	1
2656                         	xdef	_canio_Debounce_Time_c
2657  0008                   _canio_LOC_Timeout_Count_c:
2658  0008 00                	ds.b	1
2659                         	xdef	_canio_LOC_Timeout_Count_c
2660  0009                   _temp_odo_c:
2661  0009 00                	ds.b	1
2662                         	xdef	_temp_odo_c
2663  000a                   L531_update_veh_state_c:
2664  000a 00                	ds.b	1
2665  000b                   _Canio_VehIdNumberCounter_c:
2666  000b 00                	ds.b	1
2667                         	xdef	_Canio_VehIdNumberCounter_c
2668                         	xref	f_EE_BlockRead
2669                         	xref	f_EE_BlockWrite
2670                         	xref	f_FMemLibF_ReportDtc
2671                         	xref	_ru_eep_PROXI_Cfg
2672                         	xref	_diag_io_lock3_flags_c
2673                         	xref	_diag_io_lock_flags_c
2674                         	xref	_main_SwReq_stW_c
2675                         	xref	_main_SwReq_Rear_c
2676                         	xref	_main_SwReq_Front_c
2677                         	xdef	f_CSWM_CFG_DATA_RESP_ManIndFunc
2678                         	xdef	f_CSWM_EngineSpeedValidData_ManIndFunc
2679                         	xdef	f_CSWM_EngineSts_ManIndFunc
2680                         	xdef	f_CSWM_VIN_ManIndFunc
2681                         	xdef	f_CSWM_StW_Actn_Rq_ManIndFunc
2682                         	xdef	f_CSWM_CFG_RQ_ManIndFunc
2683                         	xdef	f_CSWM_ODO_ManIndFunc
2684                         	xdef	f_CSWM_BatVolt_ManIndFunc
2685                         	xdef	f_CSWM_ExtTemp_ManIndFunc
2686                         	xdef	f_CSWM_HSW_RQ_TGW_ManIndFunc
2687                         	xdef	f_CSWM_FR_HS_RQ_TGW_ManIndFunc
2688                         	xdef	f_CSWM_FL_HS_RQ_TGW_ManIndFunc
2689                         	xdef	f_CSWM_EngRPM_ManIndFunc
2690                         	xdef	f_CSWM_VehStart_PKT_ManIndFunc
2691                         	xdef	f_canF_TGW_LOC_DTC
2692                         	xdef	f_canF_CBC_LOC_DTC
2693                         	xdef	f_canF_CyclicManIndFunc
2694                         	xdef	f_canF_CyclicRemoteStart
2695                         	xdef	f_canF_CyclicTask
2696                         	xdef	f_canF_Init
2697  000c                   _canio_IgnRun_RemSt_Timeout_c:
2698  000c 00                	ds.b	1
2699                         	xdef	_canio_IgnRun_RemSt_Timeout_c
2700  000d                   _canio_FL_HS_RQ_TGW_Timeout_c:
2701  000d 00                	ds.b	1
2702                         	xdef	_canio_FL_HS_RQ_TGW_Timeout_c
2703  000e                   _canio_RemSt_configStat_c:
2704  000e 00                	ds.b	1
2705                         	xdef	_canio_RemSt_configStat_c
2706  000f                   _canio_RX_IgnRun_RemSt_c:
2707  000f 00                	ds.b	1
2708                         	xdef	_canio_RX_IgnRun_RemSt_c
2709  0010                   _canio_RX_Rem_CFG_STAT_RQ_c:
2710  0010 00                	ds.b	1
2711                         	xdef	_canio_RX_Rem_CFG_STAT_RQ_c
2712  0011                   _canio_RX_Rem_CFG_FEATURE_c:
2713  0011 00                	ds.b	1
2714                         	xdef	_canio_RX_Rem_CFG_FEATURE_c
2715  0012                   _canio_RX_ExternalTemperature_c:
2716  0012 00                	ds.b	1
2717                         	xdef	_canio_RX_ExternalTemperature_c
2718  0013                   _canio_CurrentVIN:
2719  0013 0000000000000000  	ds.b	17
2720                         	xdef	_canio_CurrentVIN
2721  0024                   _canio_Vehicle_ID_Number:
2722  0024 0000000000000000  	ds.b	17
2723                         	xdef	_canio_Vehicle_ID_Number
2724  0035                   _canio_LHD_RHD_c:
2725  0035 00                	ds.b	1
2726                         	xdef	_canio_LHD_RHD_c
2727  0036                   _canio_COUNTRY_c:
2728  0036 00                	ds.b	1
2729                         	xdef	_canio_COUNTRY_c
2730  0037                   _canio_BODY_STYLE_c:
2731  0037 00                	ds.b	1
2732                         	xdef	_canio_BODY_STYLE_c
2733  0038                   _canio_MODEL_YEAR_c:
2734  0038 00                	ds.b	1
2735                         	xdef	_canio_MODEL_YEAR_c
2736  0039                   _canio_VEH_LINE_c:
2737  0039 00                	ds.b	1
2738                         	xdef	_canio_VEH_LINE_c
2739  003a                   _canio_RX_EngineSpeed_c:
2740  003a 00                	ds.b	1
2741                         	xdef	_canio_RX_EngineSpeed_c
2742  003b                   _canio_LOAD_SHED_c:
2743  003b 00                	ds.b	1
2744                         	xdef	_canio_LOAD_SHED_c
2745  003c                   _canio_RX_IGN_STATUS_c:
2746  003c 00                	ds.b	1
2747                         	xdef	_canio_RX_IGN_STATUS_c
2748  003d                   _canio_RX_EngineSpeedValidData_c:
2749  003d 00                	ds.b	1
2750                         	xdef	_canio_RX_EngineSpeedValidData_c
2751  003e                   _canio_RX_EngineSts_c:
2752  003e 00                	ds.b	1
2753                         	xdef	_canio_RX_EngineSts_c
2754  003f                   _canio_RX_StW_TempSens_FltSts_c:
2755  003f 00                	ds.b	1
2756                         	xdef	_canio_RX_StW_TempSens_FltSts_c
2757  0040                   _canio_RX_StW_TempSts_c:
2758  0040 00                	ds.b	1
2759                         	xdef	_canio_RX_StW_TempSts_c
2760  0041                   _canio_RX_HSW_RQ_TGW_c:
2761  0041 00                	ds.b	1
2762                         	xdef	_canio_RX_HSW_RQ_TGW_c
2763  0042                   _canio_hvac_vent_seat_request_c:
2764  0042 0000              	ds.b	2
2765                         	xdef	_canio_hvac_vent_seat_request_c
2766  0044                   _canio_vent_seat_request_c:
2767  0044 0000              	ds.b	2
2768                         	xdef	_canio_vent_seat_request_c
2769  0046                   _canio_hvac_heated_seat_request_c:
2770  0046 00000000          	ds.b	4
2771                         	xdef	_canio_hvac_heated_seat_request_c
2772  004a                   _canio_RX_BatteryVoltageLevel_c:
2773  004a 00                	ds.b	1
2774                         	xdef	_canio_RX_BatteryVoltageLevel_c
2775  004b                   _canio_RX_TotalOdo_c:
2776  004b 000000            	ds.b	3
2777                         	xdef	_canio_RX_TotalOdo_c
2778  004e                   _canio_RX_TravelDistance_w:
2779  004e 0000              	ds.b	2
2780                         	xdef	_canio_RX_TravelDistance_w
2781  0050                   _canio_RemSt_stored_stat:
2782  0050 00                	ds.b	1
2783                         	xdef	_canio_RemSt_stored_stat
2784  0051                   _canio_status2_flags_c:
2785  0051 00                	ds.b	1
2786                         	xdef	_canio_status2_flags_c
2787  0052                   _canio_status_flags_c:
2788  0052 00                	ds.b	1
2789                         	xdef	_canio_status_flags_c
2790                         	xref	f_IlGetSignalIndicationFlag
2791                         	xref	f_IlPutTxRemSt_DCSSts
2792                         	xref	f_IlPutTxDigit_01
2793                         	xref	f_IlGetRxVIN_DATA
2794                         	xref	f_IlGetRxTotalOdometer
2795                         	xref	_CBC_I4
2796                         	xref	_CFG_DATA_CODE_RSP_CSWM
2797                         	xref	_CFG_RQ
2798                         	xref	_ENVIRONMENTAL_CONDITIONS
2799                         	xref	_STATUS_B_ECM
2800                         	xref	_STATUS_C_CAN
2801                         	xref	_StW_Actn_Rq
2802                         	xref	_TGW_A1
2803                         	xref	_VIN
2804                         	xref	f_CanTransmit
2805                         	xref	f_VStdResumeAllInterrupts
2806                         	xref	f_VStdSuspendAllInterrupts
2807                         	xref	_memcmp
2828                         	xref	c_lmul
2829                         	end
