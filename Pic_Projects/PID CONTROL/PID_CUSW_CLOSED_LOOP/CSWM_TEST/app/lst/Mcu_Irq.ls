   1                         ; C Compiler for S12XE [COSMIC Software].
   2                         ; Generator V4.7.8 - 27 Jul 2007
   3                         ; Optimizer V4.6.13 - 27 Jul 2007
  35                         ; 98 interrupt void near CrgScm_Isr(void)
  35                         ; 99 {
  36                         	switch	.text
  37  0000                   _CrgScm_Isr:
  42                         ; 100     Mcu_SelfClockMode_Isr();
  44  0000 160000            	jsr	_Mcu_SelfClockMode_Isr
  46                         ; 101 }
  49  0003 0b                	rti	
  72                         ; 102 interrupt  void near CrgPllLck_Isr(void)
  72                         ; 103 {
  73                         	switch	.text
  74  0004                   _CrgPllLck_Isr:
  79                         ; 104     Mcu_PllLockInterrupt_Isr();
  81  0004 160000            	jsr	_Mcu_PllLockInterrupt_Isr
  83                         ; 105 }
  86  0007 0b                	rti	
  98                         	xdef	_CrgPllLck_Isr
  99                         	xdef	_CrgScm_Isr
 100                         	xref	_Mcu_PllLockInterrupt_Isr
 101                         	xref	_Mcu_SelfClockMode_Isr
 121                         	end
