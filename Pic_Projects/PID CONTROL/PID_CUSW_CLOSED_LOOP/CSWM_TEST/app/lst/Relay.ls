   1                         ; C Compiler for S12XE [COSMIC Software].
   2                         ; Generator V4.7.8 - 27 Jul 2007
   3                         ; Optimizer V4.6.13 - 27 Jul 2007
 243                         ; 308 @far void rlF_InitRelay(void)
 243                         ; 309 {
 244                         .ftext:	section	.text
 245  0000                   f_rlF_InitRelay:
 249                         ; 310 	relay_UBATT_current_value_c = 120;       //Initially taking the value as 12v
 251  0000 c678              	ldab	#120
 252  0002 7b000d            	stab	_relay_UBATT_current_value_c
 253                         ; 312 	EE_BlockRead(EE_AUTOSAR_100P_PWM, (u_8Bit*)&relay_autosar_pwm_w);
 255  0005 cc0009            	ldd	#_relay_autosar_pwm_w
 256  0008 3b                	pshd	
 257  0009 cc0004            	ldd	#4
 258  000c 4a000000          	call	f_EE_BlockRead
 260                         ; 321 	EE_BlockRead(EE_RELAY_THRESHOLDS, (u_8Bit*)&relay_calibs_c);
 262  0010 cc0016            	ldd	#_relay_calibs_c
 263  0013 6c80              	std	0,s
 264  0015 cc0006            	ldd	#6
 265  0018 4a000000          	call	f_EE_BlockRead
 267  001c 1b82              	leas	2,s
 268                         ; 336 	relay_fault_detection_type_c = HSW_PHASE1_TEST_VOLT_OFF;
 270  001e c601              	ldab	#1
 271  0020 7b0005            	stab	_relay_fault_detection_type_c
 272                         ; 337 }
 275  0023 0a                	rtc	
 344                         ; 349 @far void rlF_RelayManager(void)
 344                         ; 350 {
 345                         	switch	.ftext
 346  0024                   f_rlF_RelayManager:
 350                         ; 351 	if ( main_CSWM_Status_c != UGLY )
 352  0024 f60000            	ldab	_main_CSWM_Status_c
 353  0027 c103              	cmpb	#3
 354  0029 2731              	beq	L351
 355                         ; 354 		rlLF_BaseRelay_For_FrontSeats_OFF();
 357  002b 4a00f4f4          	call	f_rlLF_BaseRelay_For_FrontSeats_OFF
 359                         ; 357 		rlLF_BaseRelay_For_RearSeats_OFF();
 361  002f 4a011212          	call	f_rlLF_BaseRelay_For_RearSeats_OFF
 363                         ; 360 		rlLF_BaseRelay_For_Steering_OFF();
 365  0033 4a012626          	call	f_rlLF_BaseRelay_For_Steering_OFF
 367                         ; 363 		rlLF_BaseRelay_ON_With100PWM_For_RelayA();
 369  0037 4a014545          	call	f_rlLF_BaseRelay_ON_With100PWM_For_RelayA
 371                         ; 366 		rlLF_BaseRelay_ON_With100PWM_For_RelayB();
 373  003b 4a017c7c          	call	f_rlLF_BaseRelay_ON_With100PWM_For_RelayB
 375                         ; 369 		rlLF_BaseRelay_ON_With100PWM_For_RelayC();
 377  003f 4a01a7a7          	call	f_rlLF_BaseRelay_ON_With100PWM_For_RelayC
 379                         ; 372 		rlLF_Control_BaseRelay();
 381  0043 4a01cfcf          	call	f_rlLF_Control_BaseRelay
 383                         ; 375 		rlLF_Control_RelayA_Port();
 385  0047 4a027171          	call	f_rlLF_Control_RelayA_Port
 387                         ; 378 		rlLF_Control_RelayB_Port();
 389  004b 4a02aeae          	call	f_rlLF_Control_RelayB_Port
 391                         ; 381 		rlLF_Control_RelayC_Port();
 393  004f 4a02ebeb          	call	f_rlLF_Control_RelayC_Port
 395                         ; 384 		rlLF_TurnOFF_BaseRelay();
 397  0053 4a032e2e          	call	f_rlLF_TurnOFF_BaseRelay
 399                         ; 387 		rlF_RelayDiagnostics();
 401  0057 4a037676          	call	f_rlF_RelayDiagnostics
 405  005b 0a                	rtc	
 406  005c                   L351:
 407                         ; 396 		Dio_WriteChannel(REL_A_EN, STD_HIGH);
 410  005c 1410              	sei	
 415  005e fd0014            	ldy	_Dio_PortWrite_Ptr+20
 416  0061 0c4002            	bset	0,y,2
 420  0064 10ef              	cli	
 422                         ; 397 		relay_A_status_c = RELAY_MON_LOW;
 425  0066 c7                	clrb	
 426  0067 7b0011            	stab	_relay_A_status_c
 427                         ; 400 		Dio_WriteChannel(REL_B_EN, STD_HIGH);
 430  006a 1410              	sei	
 435  006c 0c4001            	bset	0,y,1
 439  006f 10ef              	cli	
 441                         ; 401 		relay_B_status_c = RELAY_MON_LOW;
 444  0071 87                	clra	
 445  0072 7a0010            	staa	_relay_B_status_c
 446                         ; 404 		Dio_WriteChannel(REL_STW_EN, STD_HIGH);
 449  0075 1410              	sei	
 454  0077 fe0008            	ldx	_Dio_PortWrite_Ptr+8
 455  007a 0c0008            	bset	0,x,8
 459  007d 10ef              	cli	
 461                         ; 407 		Pwm_SetDutyCycle(PWM_BASE_REL, 0x0000);
 464  007f 3b                	pshd	
 465  0080 c602              	ldab	#2
 466  0082 4a000000          	call	f_Pwm_SetDutyCycle
 468  0086 1b82              	leas	2,s
 469                         ; 414 		relay_status_c.cb = 0;
 471  0088 790021            	clr	_relay_status_c
 472                         ; 416 		if( (canio_RX_IGN_STATUS_c != IGN_RUN) && (canio_RX_IGN_STATUS_c != IGN_START) ){
 474  008b f60000            	ldab	_canio_RX_IGN_STATUS_c
 475  008e c104              	cmpb	#4
 476  0090 2715              	beq	L751
 478  0092 c105              	cmpb	#5
 479  0094 2711              	beq	L751
 480                         ; 418 			relay_status2_c.cb = 0;
 482  0096 790020            	clr	_relay_status2_c
 483                         ; 419 			relay_fault_status_c.cb = 0;
 485  0099 79001e            	clr	_relay_fault_status_c
 486                         ; 420 			relay_fault_status2_c.cb = 0;
 488  009c 79001f            	clr	_relay_fault_status2_c
 489                         ; 423 			relay_control_c.cb = 0;
 491  009f 79001d            	clr	_relay_control_c
 492                         ; 424 			relay_control2_c.cb = 0;			
 494  00a2 79001c            	clr	_relay_control2_c
 496  00a5 2010              	bra	L161
 497  00a7                   L751:
 498                         ; 427 			base_relay_active_for_steering_wheel_b = FALSE;	// bit0
 500                         ; 428 			relay_C_enabled_b = FALSE;						// bit1
 502                         ; 429 			relay_C_Ok_b = FALSE;							// bit2
 504                         ; 430 			relay_C_phase1_ok_b = FALSE;					// bit3
 506                         ; 431 			relay_C_phase2_ok_b = FALSE;					// bit4
 508  00a7 1d00201f          	bclr	_relay_status2_c,31
 509                         ; 437 			relay_C_stuck_HI_b = FALSE;				// bit1 Not Used
 511                         ; 438 			relay_C_short_at_FET_b = FALSE;			// bit3 Not Used
 513                         ; 439 			Base_relay_always_100P_PWM_b = FALSE;	// bit4
 515                         ; 440 			Relay_A_Test_b = FALSE;					// bit5 Not Used
 517                         ; 441 			Relay_B_Test_b = FALSE;					// bit6 Not Used
 519                         ; 442 			Relay_C_Test_b = FALSE;					// bit7 Not Used
 521  00ab 1d001ffa          	bclr	_relay_fault_status2_c,250
 522                         ; 446 			relay_A_phase1_ok_b = FALSE; 	// bit0
 524                         ; 447 			relay_B_phase1_ok_b = FALSE;	// bit1
 526                         ; 448 			relay_A_phase2_ok_b = FALSE;	// bit2
 528                         ; 449 			relay_B_phase2_ok_b = FALSE;	// bit3
 530  00af 1d001d0f          	bclr	_relay_control_c,15
 531                         ; 453 			relay_B_no_power_b = FALSE; 		// bit2 Not Used
 533                         ; 454 			relay_C_no_power_b = FALSE; 		// bit3 Not Used
 535                         ; 455 			relay_C_keep_U12S_ON_b = FALSE;		// bit4
 537  00b3 1d001c1c          	bclr	_relay_control2_c,28
 538  00b7                   L161:
 539                         ; 465 		relay_fault_detection_type_c = HSW_PHASE1_TEST_VOLT_OFF;
 541  00b7 c601              	ldab	#1
 542  00b9 7b0005            	stab	_relay_fault_detection_type_c
 543                         ; 468 		relay_1sec_100pwm_counter_c = 0;
 545  00bc 790015            	clr	_relay_1sec_100pwm_counter_c
 546                         ; 471 		relay_A_2000ms_fault_counter_c = 0;
 548  00bf 790014            	clr	_relay_A_2000ms_fault_counter_c
 549                         ; 472 		relay_B_2000ms_fault_counter_c = 0;
 551  00c2 790013            	clr	_relay_B_2000ms_fault_counter_c
 552                         ; 473 		relay_C_2000ms_fault_counter_c = 0;
 554  00c5 790012            	clr	_relay_C_2000ms_fault_counter_c
 555                         ; 477 		relay_delay_time_phase1_c = 0;
 557  00c8 790008            	clr	_relay_delay_time_phase1_c
 558                         ; 478 		relay_delay_time_phase2_c = 0;
 560  00cb 790007            	clr	_relay_delay_time_phase2_c
 561                         ; 479 		rear_seat_no_power_delay_c = 0;
 563  00ce 790002            	clr	_rear_seat_no_power_delay_c
 564                         ; 482 		relay_A_stuck_HI_delay_c = 0;
 566  00d1 790004            	clr	_relay_A_stuck_HI_delay_c
 567                         ; 483 		relay_B_stuck_HI_delay_c = 0;
 569  00d4 790003            	clr	_relay_B_stuck_HI_delay_c
 570                         ; 485 		relay_base_pwm_value_c = 0;
 572  00d7 79000c            	clr	_relay_base_pwm_value_c
 573                         ; 486 		relay_base_prev_pwm_value_c = 0;
 575  00da 79000b            	clr	_relay_base_prev_pwm_value_c
 576                         ; 488 		EE_BlockRead(EE_AUTOSAR_100P_PWM, (u_8Bit*)&relay_autosar_pwm_w);
 578  00dd cc0009            	ldd	#_relay_autosar_pwm_w
 579  00e0 3b                	pshd	
 580  00e1 cc0004            	ldd	#4
 581  00e4 4a000000          	call	f_EE_BlockRead
 583  00e8 1b82              	leas	2,s
 584                         ; 491 		relay_C_STB_delay_c = 0;
 586  00ea 790001            	clr	_relay_C_STB_delay_c
 587                         ; 492 		relay_delay_time_for_steering_c = 0;
 589  00ed 790006            	clr	_relay_delay_time_for_steering_c
 590                         ; 495 		relay_stw_LO_Side_Off_cntr_c = 0;
 592  00f0 790000            	clr	_relay_stw_LO_Side_Off_cntr_c
 593                         ; 498 }
 596  00f3 0a                	rtc	
 622                         ; 517 void rlLF_BaseRelay_For_FrontSeats_OFF (void)
 622                         ; 518 {
 623                         	switch	.ftext
 624  00f4                   f_rlLF_BaseRelay_For_FrontSeats_OFF:
 628                         ; 521 	if ((hs_rem_seat_req_c[FRONT_LEFT] == SET_OFF_REQUEST)  &&
 628                         ; 522 	    (hs_rem_seat_req_c[FRONT_RIGHT] == SET_OFF_REQUEST) &&
 628                         ; 523 	    (vs_Flag1_c[VS_FL].b.sw_rq_b == FALSE)              &&
 628                         ; 524 	    (vs_Flag1_c[VS_FR].b.sw_rq_b == FALSE) ) {
 630  00f4 f60000            	ldab	_hs_rem_seat_req_c
 631  00f7 2618              	bne	L102
 633  00f9 f60001            	ldab	_hs_rem_seat_req_c+1
 634  00fc 2613              	bne	L102
 636  00fe 1e0000020e        	brset	_vs_Flag1_c,2,L102
 638  0103 1e00010209        	brset	_vs_Flag1_c+1,2,L102
 639                         ; 527 		if (base_relay_active_for_front_seats_b) {
 641  0108 1f00210104        	brclr	_relay_status_c,1,L102
 642                         ; 529 			base_relay_active_for_front_seats_b = FALSE;
 644  010d 1d002101          	bclr	_relay_status_c,1
 646  0111                   L102:
 647                         ; 544 }
 650  0111 0a                	rtc	
 675                         ; 565 void rlLF_BaseRelay_For_RearSeats_OFF (void)
 675                         ; 566 {
 676                         	switch	.ftext
 677  0112                   f_rlLF_BaseRelay_For_RearSeats_OFF:
 681                         ; 569 	if ((hs_rem_seat_req_c[REAR_LEFT] == SET_OFF_REQUEST) &&
 681                         ; 570 	    (hs_rem_seat_req_c[REAR_RIGHT] == SET_OFF_REQUEST) ) {
 683  0112 f60002            	ldab	_hs_rem_seat_req_c+2
 684  0115 260e              	bne	L122
 686  0117 f60003            	ldab	_hs_rem_seat_req_c+3
 687  011a 2609              	bne	L122
 688                         ; 573 		if (base_relay_active_for_rear_seats_b) {
 690  011c 1f00210204        	brclr	_relay_status_c,2,L122
 691                         ; 575 			base_relay_active_for_rear_seats_b = FALSE;
 693  0121 1d002102          	bclr	_relay_status_c,2
 695  0125                   L122:
 696                         ; 590 }
 699  0125 0a                	rtc	
 726                         ; 610 void rlLF_BaseRelay_For_Steering_OFF (void)
 726                         ; 611 {
 727                         	switch	.ftext
 728  0126                   f_rlLF_BaseRelay_For_Steering_OFF:
 732                         ; 614 	if  (stW_sw_rq_b == FALSE)
 734  0126 1e00000416        	brset	_stW_Flag1_c,4,L532
 735                         ; 617 		if (base_relay_active_for_steering_wheel_b)
 737  012b 1f00200111        	brclr	_relay_status2_c,1,L532
 738                         ; 620 			if(relay_stw_LO_Side_Off_cntr_c >= /*relay_stw_LO_Side_Off_time_lmt_c*/relay_calibs_c[RL_HSW_LOSIDE_OFF_DELAY]){
 740  0130 f60000            	ldab	_relay_stw_LO_Side_Off_cntr_c
 741  0133 f1001b            	cmpb	_relay_calibs_c+5
 742  0136 2505              	blo	L732
 743                         ; 622 				base_relay_active_for_steering_wheel_b = FALSE;	
 745  0138 1d002001          	bclr	_relay_status2_c,1
 748  013c 0a                	rtc	
 749  013d                   L732:
 750                         ; 624 				relay_stw_LO_Side_Off_cntr_c++;	//increment low side off delay counter
 752  013d 720000            	inc	_relay_stw_LO_Side_Off_cntr_c
 754  0140 0a                	rtc	
 755  0141                   L532:
 756                         ; 630 			relay_stw_LO_Side_Off_cntr_c = 0;
 758                         ; 637 		relay_stw_LO_Side_Off_cntr_c = 0;
 760  0141 790000            	clr	_relay_stw_LO_Side_Off_cntr_c
 761                         ; 639 }
 764  0144 0a                	rtc	
 795                         ; 658 void rlLF_BaseRelay_ON_With100PWM_For_RelayA(void)
 795                         ; 659 {
 796                         	switch	.ftext
 797  0145                   f_rlLF_BaseRelay_ON_With100PWM_For_RelayA:
 801                         ; 664 	if ((base_relay_active_for_front_seats_b == FALSE) &&
 801                         ; 665 	    (relay_A_internal_Fault_b == FALSE) &&
 801                         ; 666 	    (hs_status_flags_c[ FRONT_LEFT ].b.hs_final_short_bat_b == FALSE) &&
 801                         ; 667 	    (hs_status_flags_c[ FRONT_RIGHT ].b.hs_final_short_bat_b == FALSE)&&
 801                         ; 668 	    (vs_shrtTo_batt_mntr_c!= VS_SHRT_TO_BATT)) {
 803  0145 1e00210131        	brset	_relay_status_c,1,L372
 805  014a 1e001d102c        	brset	_relay_control_c,16,L372
 807  014f 1e00002027        	brset	_hs_status_flags_c,32,L372
 809  0154 1e00012022        	brset	_hs_status_flags_c+1,32,L372
 811  0159 f60000            	ldab	_vs_shrtTo_batt_mntr_c
 812  015c c102              	cmpb	#2
 813  015e 271b              	beq	L372
 814                         ; 672 		if ((hs_rem_seat_req_c[FRONT_LEFT] != SET_OFF_REQUEST)  ||
 814                         ; 673 		    (hs_rem_seat_req_c[FRONT_RIGHT] != SET_OFF_REQUEST) ||
 814                         ; 674 		    (vs_Flag1_c[VS_FL].b.sw_rq_b == TRUE)               ||
 814                         ; 675 		    (vs_Flag1_c[VS_FR].b.sw_rq_b == TRUE)) {
 816  0160 f60000            	ldab	_hs_rem_seat_req_c
 817  0163 260f              	bne	L362
 819  0165 f60001            	ldab	_hs_rem_seat_req_c+1
 820  0168 260a              	bne	L362
 822  016a 1e00000205        	brset	_vs_Flag1_c,2,L362
 824  016f 1f00010207        	brclr	_vs_Flag1_c+1,2,L372
 825  0174                   L362:
 826                         ; 679 			base_relay_active_for_front_seats_b = TRUE;
 828                         ; 682 			base_relay_100_pwm_b = TRUE;
 830  0174 1c002105          	bset	_relay_status_c,5
 831                         ; 685 			relay_1sec_100pwm_counter_c = 0;
 833  0178 790015            	clr	_relay_1sec_100pwm_counter_c
 835  017b                   L372:
 836                         ; 701 }
 839  017b 0a                	rtc	
 869                         ; 720 void rlLF_BaseRelay_ON_With100PWM_For_RelayB(void)
 869                         ; 721 {
 870                         	switch	.ftext
 871  017c                   f_rlLF_BaseRelay_ON_With100PWM_For_RelayB:
 875                         ; 725 	if ((base_relay_active_for_rear_seats_b == FALSE) &&
 875                         ; 726 	    (relay_B_internal_Fault_b == FALSE) &&
 875                         ; 727 	    (hs_status_flags_c[ REAR_LEFT ].b.hs_final_short_bat_b == FALSE) &&
 875                         ; 728 	    (hs_status_flags_c[ REAR_RIGHT ].b.hs_final_short_bat_b == FALSE) &&
 875                         ; 729 	   (Rear_Seats_no_power_final_b == FALSE)) {
 877  017c 1e00210225        	brset	_relay_status_c,2,L513
 879  0181 1e001d2020        	brset	_relay_control_c,32,L513
 881  0186 1e0002201b        	brset	_hs_status_flags_c+2,32,L513
 883  018b 1e00032016        	brset	_hs_status_flags_c+3,32,L513
 885  0190 1e001c0211        	brset	_relay_control2_c,2,L513
 886                         ; 732 		if ((hs_rem_seat_req_c[REAR_LEFT] != SET_OFF_REQUEST) ||
 886                         ; 733 		    (hs_rem_seat_req_c[REAR_RIGHT] != SET_OFF_REQUEST)) {
 888  0195 f60002            	ldab	_hs_rem_seat_req_c+2
 889  0198 2605              	bne	L113
 891  019a f60003            	ldab	_hs_rem_seat_req_c+3
 892  019d 2707              	beq	L513
 893  019f                   L113:
 894                         ; 737 			base_relay_active_for_rear_seats_b = TRUE;
 896                         ; 740 			base_relay_100_pwm_b = TRUE;
 898  019f 1c002106          	bset	_relay_status_c,6
 899                         ; 743 			relay_1sec_100pwm_counter_c = 0;
 901  01a3 790015            	clr	_relay_1sec_100pwm_counter_c
 903  01a6                   L513:
 904                         ; 760 }
 907  01a6 0a                	rtc	
 937                         ; 779 void rlLF_BaseRelay_ON_With100PWM_For_RelayC(void)
 937                         ; 780 {
 938                         	switch	.ftext
 939  01a7                   f_rlLF_BaseRelay_ON_With100PWM_For_RelayC:
 943                         ; 784 	if ((base_relay_active_for_steering_wheel_b == FALSE) &&
 943                         ; 785 	    (relay_C_internal_Fault_b == FALSE)&&
 943                         ; 786 	    (stW_Flt_status_w <= STW_PRL_FLT_MAX))
 945  01a7 1e00200122        	brset	_relay_status2_c,1,L533
 947  01ac 1e001f011d        	brset	_relay_fault_status2_c,1,L533
 949  01b1 fc0000            	ldd	_stW_Flt_status_w
 950  01b4 8c003f            	cpd	#63
 951  01b7 2215              	bhi	L533
 952                         ; 790 		if ((stW_sw_rq_b == TRUE) && (relay_C_phase1_ok_b == TRUE))
 954  01b9 1f00000410        	brclr	_stW_Flag1_c,4,L533
 956  01be 1f0020080b        	brclr	_relay_status2_c,8,L533
 957                         ; 793 			base_relay_active_for_steering_wheel_b = TRUE;
 959  01c3 1c002001          	bset	_relay_status2_c,1
 960                         ; 796 			base_relay_100_pwm_b = TRUE;
 962  01c7 1c002104          	bset	_relay_status_c,4
 963                         ; 799 			relay_1sec_100pwm_counter_c = 0;
 965  01cb 790015            	clr	_relay_1sec_100pwm_counter_c
 967  01ce                   L533:
 968                         ; 812 }
 971  01ce 0a                	rtc	
1007                         ; 833 void rlLF_Control_BaseRelay(void)
1007                         ; 834 {
1008                         	switch	.ftext
1009  01cf                   f_rlLF_Control_BaseRelay:
1013                         ; 837 	if ((base_relay_active_for_front_seats_b) ||
1013                         ; 838 	    (base_relay_active_for_rear_seats_b) ||
1013                         ; 839 	    (base_relay_active_for_steering_wheel_b))
1015  01cf 1e0021010e        	brset	_relay_status_c,1,L153
1017  01d4 1e00210209        	brset	_relay_status_c,2,L153
1019  01d9 f60020            	ldab	_relay_status2_c
1020  01dc c501              	bitb	#1
1021  01de 1827008e          	beq	L773
1022  01e2                   L153:
1023                         ; 842 		if (base_relay_100_pwm_b)
1025  01e2 1f0021042e        	brclr	_relay_status_c,4,L553
1026                         ; 848 			EE_BlockRead(EE_AUTOSAR_100P_PWM, (u_8Bit*)&relay_autosar_pwm_w);
1028  01e7 cc0009            	ldd	#_relay_autosar_pwm_w
1029  01ea 3b                	pshd	
1030  01eb cc0004            	ldd	#4
1031  01ee 4a000000          	call	f_EE_BlockRead
1033                         ; 851 			Pwm_SetDutyCycle(PWM_BASE_REL, relay_autosar_pwm_w);
1035  01f2 1801800009        	movw	_relay_autosar_pwm_w,0,s
1036  01f7 cc0002            	ldd	#2
1037  01fa 4a000000          	call	f_Pwm_SetDutyCycle
1039  01fe 1b82              	leas	2,s
1040                         ; 854 			if (relay_1sec_100pwm_counter_c > 100)
1042  0200 f60015            	ldab	_relay_1sec_100pwm_counter_c
1043  0203 c164              	cmpb	#100
1044  0205 2309              	bls	L753
1045                         ; 858 				base_relay_100_pwm_b = FALSE;
1047  0207 1d002104          	bclr	_relay_status_c,4
1048                         ; 861 				relay_1sec_100pwm_counter_c = 0;
1050  020b 790015            	clr	_relay_1sec_100pwm_counter_c
1052  020e 205c              	bra	L363
1053  0210                   L753:
1054                         ; 866 				relay_1sec_100pwm_counter_c++;
1056  0210 720015            	inc	_relay_1sec_100pwm_counter_c
1057  0213 2057              	bra	L363
1058  0215                   L553:
1059                         ; 873 			if (!Base_relay_always_100P_PWM_b)
1061  0215 1e001f1052        	brset	_relay_fault_status2_c,16,L363
1062                         ; 878 				relay_UBATT_current_value_c = BattVF_Get_Crt_Uint();
1064  021a 4a000000          	call	f_BattVF_Get_Crt_Uint
1066  021e 7b000d            	stab	_relay_UBATT_current_value_c
1067                         ; 882 				if ( relay_UBATT_current_value_c >  /*relay_UBatt_cutoff_value_c*/relay_calibs_c[RL_UBAT_CUTOFF] )
1069  0221 f10016            	cmpb	_relay_calibs_c
1070  0224 232d              	bls	L763
1071                         ; 888 					relay_base_pwm_value_c = (unsigned char) ( ((unsigned int)(/*relay_UBatt_cutoff_value_c*/relay_calibs_c[RL_UBAT_CUTOFF]*100)) / relay_UBATT_current_value_c );
1073  0226 b795              	exg	b,x
1074  0228 f60016            	ldab	_relay_calibs_c
1075  022b 8664              	ldaa	#100
1076  022d 12                	mul	
1077  022e 1810              	idiv	
1078  0230 b754              	tfr	x,d
1079  0232 7b000c            	stab	_relay_base_pwm_value_c
1080                         ; 891 					if (relay_base_pwm_value_c != relay_base_prev_pwm_value_c)
1082  0235 f1000b            	cmpb	_relay_base_prev_pwm_value_c
1083  0238 2732              	beq	L363
1084                         ; 894 						relay_autosar_pwm_w = mainF_Calc_PWM(relay_base_pwm_value_c);
1086  023a 87                	clra	
1087  023b 160000            	jsr	_mainF_Calc_PWM
1089  023e 7c0009            	std	_relay_autosar_pwm_w
1090                         ; 897 						Pwm_SetDutyCycle(PWM_BASE_REL, relay_autosar_pwm_w);
1092  0241 3b                	pshd	
1093  0242 cc0002            	ldd	#2
1094  0245 4a000000          	call	f_Pwm_SetDutyCycle
1096  0249 1b82              	leas	2,s
1097                         ; 901 						relay_base_prev_pwm_value_c = relay_base_pwm_value_c;
1099  024b 180c000c000b      	movb	_relay_base_pwm_value_c,_relay_base_prev_pwm_value_c
1100  0251 2019              	bra	L363
1101  0253                   L763:
1102                         ; 908 					EE_BlockRead(EE_AUTOSAR_100P_PWM, (u_8Bit*)&relay_autosar_pwm_w);
1104  0253 cc0009            	ldd	#_relay_autosar_pwm_w
1105  0256 3b                	pshd	
1106  0257 cc0004            	ldd	#4
1107  025a 4a000000          	call	f_EE_BlockRead
1109                         ; 911 					Pwm_SetDutyCycle(PWM_BASE_REL, /*mirror_eeprom.autosar_100percent_pwm*/relay_autosar_pwm_w );
1111  025e 1801800009        	movw	_relay_autosar_pwm_w,0,s
1112  0263 cc0002            	ldd	#2
1113  0266 4a000000          	call	f_Pwm_SetDutyCycle
1115  026a 1b82              	leas	2,s
1116  026c                   L363:
1117                         ; 920 		relay_pwm_is_active_b = TRUE;
1119  026c 1c002120          	bset	_relay_status_c,32
1121  0270                   L773:
1122                         ; 926 }
1125  0270 0a                	rtc	
1157                         ; 939 void rlLF_Control_RelayA_Port(void)
1157                         ; 940 {
1158                         	switch	.ftext
1159  0271                   f_rlLF_Control_RelayA_Port:
1163                         ; 943 	if (base_relay_active_for_front_seats_b) {
1165  0271 1f0021011a        	brclr	_relay_status_c,1,L114
1166                         ; 946 		if (relay_A_phase1_ok_b) {
1168  0276 1f001d0132        	brclr	_relay_control_c,1,L324
1169                         ; 951 			if (relay_A_enabled_b) {
1171  027b 1e0021082d        	brset	_relay_status_c,8,L324
1173                         ; 957 				Dio_WriteChannel(REL_A_EN, STD_LOW);
1176  0280 1410              	sei	
1181  0282 fe0014            	ldx	_Dio_PortWrite_Ptr+20
1182  0285 0d0002            	bclr	0,x,2
1186  0288 10ef              	cli	
1188                         ; 960 				relay_A_enabled_b = TRUE;
1191  028a 1c002108          	bset	_relay_status_c,8
1192                         ; 963 				relay_delay_time_phase2_c = 0;
1194  028e 201a              	bra	LC002
1195  0290                   L114:
1196                         ; 978 		if (relay_A_enabled_b) {
1198  0290 1f00210818        	brclr	_relay_status_c,8,L324
1199                         ; 980 			Dio_WriteChannel(REL_A_EN, STD_HIGH);
1202  0295 1410              	sei	
1207  0297 fe0014            	ldx	_Dio_PortWrite_Ptr+20
1208  029a 0c0002            	bset	0,x,2
1212  029d 10ef              	cli	
1214                         ; 983 			relay_A_enabled_b = FALSE;
1217                         ; 986 			relay_A_Ok_b = FALSE;
1219  029f 1d002148          	bclr	_relay_status_c,72
1220                         ; 989 			relay_A_phase1_ok_b = FALSE;
1222                         ; 992 			relay_A_phase2_ok_b = FALSE;
1224  02a3 1d001d05          	bclr	_relay_control_c,5
1225                         ; 995 			relay_delay_time_phase1_c = 0;
1227  02a7 790008            	clr	_relay_delay_time_phase1_c
1228                         ; 996 			relay_delay_time_phase2_c = 0;
1230  02aa                   LC002:
1231  02aa 790007            	clr	_relay_delay_time_phase2_c
1233  02ad                   L324:
1234                         ; 1006 }
1237  02ad 0a                	rtc	
1269                         ; 1018 void rlLF_Control_RelayB_Port(void)
1269                         ; 1019 {
1270                         	switch	.ftext
1271  02ae                   f_rlLF_Control_RelayB_Port:
1275                         ; 1021 	if (base_relay_active_for_rear_seats_b) {
1277  02ae 1f0021021a        	brclr	_relay_status_c,2,L144
1278                         ; 1023 		if (relay_B_phase1_ok_b) {
1280  02b3 1f001d0232        	brclr	_relay_control_c,2,L354
1281                         ; 1026 			if (relay_B_enabled_b) {
1283  02b8 1e0021102d        	brset	_relay_status_c,16,L354
1285                         ; 1032 				Dio_WriteChannel(REL_B_EN, STD_LOW);
1288  02bd 1410              	sei	
1293  02bf fe0014            	ldx	_Dio_PortWrite_Ptr+20
1294  02c2 0d0001            	bclr	0,x,1
1298  02c5 10ef              	cli	
1300                         ; 1035 				relay_B_enabled_b = TRUE;
1303  02c7 1c002110          	bset	_relay_status_c,16
1304                         ; 1038 				relay_delay_time_phase2_c = 0;
1306  02cb 201a              	bra	LC003
1307  02cd                   L144:
1308                         ; 1054 		if (relay_B_enabled_b) {
1310  02cd 1f00211018        	brclr	_relay_status_c,16,L354
1311                         ; 1056 			Dio_WriteChannel(REL_B_EN, STD_HIGH);
1314  02d2 1410              	sei	
1319  02d4 fe0014            	ldx	_Dio_PortWrite_Ptr+20
1320  02d7 0c0001            	bset	0,x,1
1324  02da 10ef              	cli	
1326                         ; 1059 			relay_B_enabled_b = FALSE;
1329                         ; 1062 			relay_B_Ok_b = FALSE;
1331  02dc 1d002190          	bclr	_relay_status_c,144
1332                         ; 1065 			relay_B_phase1_ok_b = FALSE;
1334                         ; 1068 			relay_B_phase2_ok_b = FALSE;
1336  02e0 1d001d0a          	bclr	_relay_control_c,10
1337                         ; 1071 			relay_delay_time_phase1_c = 0;
1339  02e4 790008            	clr	_relay_delay_time_phase1_c
1340                         ; 1072 			relay_delay_time_phase2_c = 0;
1342  02e7                   LC003:
1343  02e7 790007            	clr	_relay_delay_time_phase2_c
1345  02ea                   L354:
1346                         ; 1082 }
1349  02ea 0a                	rtc	
1382                         ; 1094 void rlLF_Control_RelayC_Port(void)
1382                         ; 1095 {
1383                         	switch	.ftext
1384  02eb                   f_rlLF_Control_RelayC_Port:
1388                         ; 1097 	if (base_relay_active_for_steering_wheel_b)
1390  02eb 1f00200121        	brclr	_relay_status2_c,1,L174
1391                         ; 1100 		if (relay_C_phase1_ok_b)
1393  02f0 1f00200838        	brclr	_relay_status2_c,8,L305
1394                         ; 1104 			if (relay_C_enabled_b)
1396  02f5 1e00200233        	brset	_relay_status2_c,2,L305
1398                         ; 1111 				Dio_WriteChannel(REL_STW_EN, STD_LOW);
1401  02fa 1410              	sei	
1406  02fc fd0008            	ldy	_Dio_PortWrite_Ptr+8
1407  02ff 0d4008            	bclr	0,y,8
1411  0302 10ef              	cli	
1413                         ; 1114 				relay_C_enabled_b = TRUE;
1416  0304 1c002002          	bset	_relay_status2_c,2
1417                         ; 1117 				relay_fault_detection_type_c = HSW_PHASE1_TEST_VOLT_OFF;
1419  0308 c601              	ldab	#1
1420  030a 7b0005            	stab	_relay_fault_detection_type_c
1421                         ; 1120 				relay_delay_time_phase2_c = 0;
1423  030d 790007            	clr	_relay_delay_time_phase2_c
1425  0310 0a                	rtc	
1426  0311                   L174:
1427                         ; 1133 		if (relay_C_enabled_b)
1429  0311 1f00200217        	brclr	_relay_status2_c,2,L305
1430                         ; 1136 			Dio_WriteChannel(REL_STW_EN, STD_HIGH);
1433  0316 1410              	sei	
1438  0318 fe0008            	ldx	_Dio_PortWrite_Ptr+8
1439  031b 0c0008            	bset	0,x,8
1443  031e 10ef              	cli	
1445                         ; 1139 			relay_C_enabled_b = FALSE;
1448                         ; 1142 			relay_C_Ok_b = FALSE;
1450                         ; 1145 			relay_C_phase1_ok_b = FALSE;
1452                         ; 1148 			relay_C_phase2_ok_b = FALSE;
1454  0320 1d00201e          	bclr	_relay_status2_c,30
1455                         ; 1151 			relay_delay_time_phase1_c = 0;
1457  0324 790008            	clr	_relay_delay_time_phase1_c
1458                         ; 1152 			relay_delay_time_phase2_c = 0;
1460  0327 790007            	clr	_relay_delay_time_phase2_c
1461                         ; 1155 			relay_C_2000ms_fault_counter_c = 0;
1463  032a 790012            	clr	_relay_C_2000ms_fault_counter_c
1465  032d                   L305:
1466                         ; 1162 }
1469  032d 0a                	rtc	
1507                         ; 1173 void rlLF_TurnOFF_BaseRelay(void)
1507                         ; 1174 {
1508                         	switch	.ftext
1509  032e                   f_rlLF_TurnOFF_BaseRelay:
1513                         ; 1176 	if (!base_relay_active_for_front_seats_b &&
1513                         ; 1177 	    !base_relay_active_for_rear_seats_b &&
1513                         ; 1178 	    !base_relay_active_for_steering_wheel_b)
1515  032e 1e00210142        	brset	_relay_status_c,1,L725
1517  0333 1e0021023d        	brset	_relay_status_c,2,L725
1519  0338 1e00200138        	brset	_relay_status2_c,1,L725
1520                         ; 1181 		if (relay_pwm_is_active_b)
1522  033d 1f00212033        	brclr	_relay_status_c,32,L725
1523                         ; 1184 			relay_autosar_pwm_w = 0x0000;
1525  0342 87                	clra	
1526  0343 c7                	clrb	
1527  0344 7c0009            	std	_relay_autosar_pwm_w
1528                         ; 1187 			Pwm_SetDutyCycle(PWM_BASE_REL, relay_autosar_pwm_w);
1530  0347 3b                	pshd	
1531  0348 c602              	ldab	#2
1532  034a 4a000000          	call	f_Pwm_SetDutyCycle
1534  034e 1b82              	leas	2,s
1535                         ; 1190 			relay_pwm_is_active_b = 0;
1537  0350 1d002120          	bclr	_relay_status_c,32
1538                         ; 1194 			relay_1sec_100pwm_counter_c = 0;
1540  0354 790015            	clr	_relay_1sec_100pwm_counter_c
1541                         ; 1197 			relay_A_2000ms_fault_counter_c = 0;
1543  0357 790014            	clr	_relay_A_2000ms_fault_counter_c
1544                         ; 1198 			relay_B_2000ms_fault_counter_c = 0;
1546  035a 790013            	clr	_relay_B_2000ms_fault_counter_c
1547                         ; 1199 			relay_C_2000ms_fault_counter_c = 0;
1549  035d 790012            	clr	_relay_C_2000ms_fault_counter_c
1550                         ; 1203 			relay_delay_time_phase1_c = 0;
1552  0360 790008            	clr	_relay_delay_time_phase1_c
1553                         ; 1204 			relay_delay_time_phase2_c = 0;
1555  0363 790007            	clr	_relay_delay_time_phase2_c
1556                         ; 1205 			rear_seat_no_power_delay_c = 0;
1558  0366 790002            	clr	_rear_seat_no_power_delay_c
1559                         ; 1208 			relay_A_stuck_HI_delay_c = 0;
1561  0369 790004            	clr	_relay_A_stuck_HI_delay_c
1562                         ; 1209 			relay_B_stuck_HI_delay_c = 0;
1564  036c 790003            	clr	_relay_B_stuck_HI_delay_c
1565                         ; 1211 			relay_base_pwm_value_c = 0;
1567  036f 79000c            	clr	_relay_base_pwm_value_c
1568                         ; 1212 			relay_base_prev_pwm_value_c = 0;
1570  0372 79000b            	clr	_relay_base_prev_pwm_value_c
1572  0375                   L725:
1573                         ; 1223 }
1576  0375 0a                	rtc	
1629                         ; 1297 @far void rlF_RelayDiagnostics(void)
1629                         ; 1298 {
1630                         	switch	.ftext
1631  0376                   f_rlF_RelayDiagnostics:
1635                         ; 1300 	relay_A_status_c = (unsigned char) Dio_ReadChannel(REL_A_MON);
1637  0376 fd0000            	ldy	_Dio_PortRead_Ptr
1638  0379 e640              	ldab	0,y
1639  037b c420              	andb	#32
1640  037d 54                	lsrb	
1641  037e 54                	lsrb	
1642  037f 54                	lsrb	
1643  0380 54                	lsrb	
1644  0381 54                	lsrb	
1645  0382 7b0011            	stab	_relay_A_status_c
1646                         ; 1303 	relay_B_status_c = (unsigned char) Dio_ReadChannel(REL_B_MON);
1648  0385 e640              	ldab	0,y
1649  0387 55                	rolb	
1650  0388 55                	rolb	
1651  0389 c401              	andb	#1
1652  038b 7b0010            	stab	_relay_B_status_c
1653                         ; 1306 	relay_C_Test_Voltage_Pin_status_c = (unsigned char) Dio_ReadChannel(U12T_EN);
1655  038e e640              	ldab	0,y
1656  0390 c410              	andb	#16
1657  0392 54                	lsrb	
1658  0393 54                	lsrb	
1659  0394 54                	lsrb	
1660  0395 54                	lsrb	
1661  0396 7b000e            	stab	_relay_C_Test_Voltage_Pin_status_c
1662                         ; 1309 	if ( main_CSWM_Status_c == GOOD )
1664  0399 f60000            	ldab	_main_CSWM_Status_c
1665  039c 042148            	dbne	b,L145
1666                         ; 1312 		if (!diag_eol_no_fault_detection_b)
1668  039f 1e0000026d        	brset	_diag_io_lock3_flags_c,2,L755
1669                         ; 1315 			rlLF_NoPower_To_RearSeats_Detection();
1671  03a4 4a041212          	call	f_rlLF_NoPower_To_RearSeats_Detection
1673                         ; 1319 			if (relay_delay_time_phase1_c >=  /*relay_phase1_delay_c*/relay_calibs_c[RL_PHASE1_DELAY])
1675  03a8 f60008            	ldab	_relay_delay_time_phase1_c
1676  03ab f10017            	cmpb	_relay_calibs_c+1
1677  03ae 250e              	blo	L545
1678                         ; 1322 				rlLF_RelayAPortOFF_FETOFF_Phase1_Detection();
1680  03b0 4a044242          	call	f_rlLF_RelayAPortOFF_FETOFF_Phase1_Detection
1682                         ; 1325 				rlLF_RelayBPortOFF_FETOFF_Phase1_Detection();
1684  03b4 4a04f4f4          	call	f_rlLF_RelayBPortOFF_FETOFF_Phase1_Detection
1686                         ; 1328 				rlLF_RelayCPortOFF_FETOFF_Phase1_Detection();
1688  03b8 4a058383          	call	f_rlLF_RelayCPortOFF_FETOFF_Phase1_Detection
1691  03bc 2003              	bra	L745
1692  03be                   L545:
1693                         ; 1333 				relay_delay_time_phase1_c++;
1695  03be 720008            	inc	_relay_delay_time_phase1_c
1696  03c1                   L745:
1697                         ; 1338 			if (relay_delay_time_phase2_c >=  /*relay_phase2_delay_c*/relay_calibs_c[RL_PHASE2_DELAY])
1699  03c1 f60007            	ldab	_relay_delay_time_phase2_c
1700  03c4 f10018            	cmpb	_relay_calibs_c+2
1701  03c7 250e              	blo	L155
1702                         ; 1341 				rlLF_RelayAPortON_FETOFF_Phase2_Detection();
1704  03c9 4a068e8e          	call	f_rlLF_RelayAPortON_FETOFF_Phase2_Detection
1706                         ; 1344 				rlLF_RelayBPortON_FETOFF_Phase2_Detection();
1708  03cd 4a076565          	call	f_rlLF_RelayBPortON_FETOFF_Phase2_Detection
1710                         ; 1347 				rlLF_RelayCPortON_FETOFF_Phase2_Detection();
1712  03d1 4a081111          	call	f_rlLF_RelayCPortON_FETOFF_Phase2_Detection
1715  03d5 2003              	bra	L355
1716  03d7                   L155:
1717                         ; 1352 				relay_delay_time_phase2_c++;
1719  03d7 720007            	inc	_relay_delay_time_phase2_c
1720  03da                   L355:
1721                         ; 1356 			rlLF_MatureTime_For_RelayA_InternalFault();
1723  03da 4a086a6a          	call	f_rlLF_MatureTime_For_RelayA_InternalFault
1725                         ; 1359 			rlLF_MatureTime_For_RelayB_InternalFault();
1727  03de 4a08c2c2          	call	f_rlLF_MatureTime_For_RelayB_InternalFault
1729                         ; 1362 			rlLF_MatureTime_For_RelayC_InternalFault();
1731  03e2 4a093939          	call	f_rlLF_MatureTime_For_RelayC_InternalFault
1735  03e6 0a                	rtc	
1736  03e7                   L145:
1737                         ; 1372 		relay_delay_time_phase1_c = 0;
1739  03e7 790008            	clr	_relay_delay_time_phase1_c
1740                         ; 1373 		relay_delay_time_phase2_c = 0;
1742  03ea 790007            	clr	_relay_delay_time_phase2_c
1743                         ; 1374 		rear_seat_no_power_delay_c = 0;
1745  03ed 790002            	clr	_rear_seat_no_power_delay_c
1746                         ; 1377 		relay_A_stuck_HI_delay_c = 0;
1748  03f0 790004            	clr	_relay_A_stuck_HI_delay_c
1749                         ; 1378 		relay_B_stuck_HI_delay_c = 0;
1751  03f3 790003            	clr	_relay_B_stuck_HI_delay_c
1752                         ; 1381 		relay_A_2000ms_fault_counter_c = 0;
1754  03f6 790014            	clr	_relay_A_2000ms_fault_counter_c
1755                         ; 1382 		relay_B_2000ms_fault_counter_c = 0;
1757  03f9 790013            	clr	_relay_B_2000ms_fault_counter_c
1758                         ; 1383 		relay_C_2000ms_fault_counter_c = 0;
1760  03fc 790012            	clr	_relay_C_2000ms_fault_counter_c
1761                         ; 1385 		Rear_Seats_no_power_prelim_b = FALSE;
1763                         ; 1387 		relay_C_keep_U12S_ON_b = FALSE;
1765  03ff 1d001c11          	bclr	_relay_control2_c,17
1766                         ; 1389 		relay_C_STB_delay_c = 0;		
1768  0403 790001            	clr	_relay_C_STB_delay_c
1769                         ; 1391 		relay_fault_detection_type_c = HSW_PHASE1_TEST_VOLT_OFF;
1771  0406 c601              	ldab	#1
1772  0408 7b0005            	stab	_relay_fault_detection_type_c
1773                         ; 1392 		relay_delay_time_for_steering_c = 0;
1775  040b 790006            	clr	_relay_delay_time_for_steering_c
1776                         ; 1393 		relay_stw_LO_Side_Off_cntr_c = 0;
1778  040e 790000            	clr	_relay_stw_LO_Side_Off_cntr_c
1779  0411                   L755:
1780                         ; 1395 }
1783  0411 0a                	rtc	
1812                         ; 1412 void rlLF_NoPower_To_RearSeats_Detection(void)
1812                         ; 1413 {
1813                         	switch	.ftext
1814  0412                   f_rlLF_NoPower_To_RearSeats_Detection:
1818                         ; 1415 	if ((CSWM_config_c & CSWM_4HS_VARIANT_MASK_K) == CSWM_4HS_VARIANT_MASK_K)
1820  0412 1e00000f01        	brset	_CSWM_config_c,15,LC004
1822  0417 0a                	rtc	
1823  0418                   LC004:
1824                         ; 1418 		if (!Rear_Seats_no_power_final_b)
1826  0418 1e001c0224        	brset	_relay_control2_c,2,L175
1827                         ; 1421 			if (rear_seat_no_power_delay_c >= /*relay_phase1_delay_c*/relay_calibs_c[RL_PHASE1_DELAY])
1829  041d f60002            	ldab	_rear_seat_no_power_delay_c
1830  0420 f10017            	cmpb	_relay_calibs_c+1
1831  0423 2519              	blo	L575
1832                         ; 1424 				relay_B_power_status_c = (unsigned char) Dio_ReadChannel(IGN_REAR_MON);
1834  0425 e6fbfbdf          	ldab	[_Dio_PortRead_Ptr+8]
1835  0429 c404              	andb	#4
1836  042b 54                	lsrb	
1837  042c 54                	lsrb	
1838  042d 7b000f            	stab	_relay_B_power_status_c
1839                         ; 1427 				if (relay_B_power_status_c == RELAY_MON_HIGH)
1841  0430 c101              	cmpb	#1
1842  0432 2605              	bne	L775
1843                         ; 1430 					Rear_Seats_no_power_prelim_b = FALSE;
1845  0434 1d001c01          	bclr	_relay_control2_c,1
1848  0438 0a                	rtc	
1849  0439                   L775:
1850                         ; 1435 					Rear_Seats_no_power_prelim_b = TRUE;
1852  0439 1c001c01          	bset	_relay_control2_c,1
1854  043d 0a                	rtc	
1855  043e                   L575:
1856                         ; 1441 				rear_seat_no_power_delay_c++;
1858  043e 720002            	inc	_rear_seat_no_power_delay_c
1859  0441                   L175:
1860                         ; 1447 } //End of Function
1863  0441 0a                	rtc	
1900                         ; 1483 void rlLF_RelayAPortOFF_FETOFF_Phase1_Detection(void)
1900                         ; 1484 {
1901                         	switch	.ftext
1902  0442                   f_rlLF_RelayAPortOFF_FETOFF_Phase1_Detection:
1906                         ; 1488 	if ((relay_A_internal_Fault_b == FALSE) &&
1906                         ; 1489 	    (hs_status_flags_c[ FRONT_LEFT ].b.hs_final_short_bat_b == FALSE) &&
1906                         ; 1490 	    (hs_status_flags_c[ FRONT_RIGHT ].b.hs_final_short_bat_b == FALSE)&&
1906                         ; 1491 	    (vs_shrtTo_batt_mntr_c != VS_SHRT_TO_BATT) ) {
1908  0442 f6001d            	ldab	_relay_control_c
1909  0445 c510              	bitb	#16
1910  0447 182600a8          	bne	L736
1912  044b f60000            	ldab	_hs_status_flags_c
1913  044e c520              	bitb	#32
1914  0450 1826009f          	bne	L736
1916  0454 f60001            	ldab	_hs_status_flags_c+1
1917  0457 c520              	bitb	#32
1918  0459 18260096          	bne	L736
1920  045d f60000            	ldab	_vs_shrtTo_batt_mntr_c
1921  0460 c102              	cmpb	#2
1922  0462 1827008d          	beq	L736
1923                         ; 1494 		if (!relay_A_enabled_b ) {
1925  0466 f60021            	ldab	_relay_status_c
1926  0469 c508              	bitb	#8
1927  046b 18260084          	bne	L736
1928                         ; 1497 			if ( (relay_A_status_c == RELAY_MON_LOW) &&
1928                         ; 1498 			   ((hs_Vsense_c[FRONT_LEFT] < /*hs_STG_Vsense_cutoff_c*/hs_flt_dtc_c[HS_STG_VSENSE])&&
1928                         ; 1499 			   (hs_Vsense_c[FRONT_RIGHT] < /*hs_STG_Vsense_cutoff_c*/hs_flt_dtc_c[HS_STG_VSENSE])) &&
1928                         ; 1500 			   ((vs_Vsense_c[VS_FL] < Vs_flt_cal_prms.Flt_Dtc[V_SHRT_TO_GND])&&
1928                         ; 1501 			   (vs_Vsense_c[VS_FR] < Vs_flt_cal_prms.Flt_Dtc[V_SHRT_TO_GND]))) {
1930  046f f60011            	ldab	_relay_A_status_c
1931  0472 2633              	bne	L126
1933  0474 f60000            	ldab	_hs_Vsense_c
1934  0477 f10003            	cmpb	_hs_flt_dtc_c+3
1935  047a 242b              	bhs	L126
1937  047c f60001            	ldab	_hs_Vsense_c+1
1938  047f f10003            	cmpb	_hs_flt_dtc_c+3
1939  0482 2423              	bhs	L126
1941  0484 f60000            	ldab	_vs_Vsense_c
1942  0487 f10001            	cmpb	_Vs_flt_cal_prms+1
1943  048a 241b              	bhs	L126
1945  048c f60001            	ldab	_vs_Vsense_c+1
1946  048f f10001            	cmpb	_Vs_flt_cal_prms+1
1947  0492 2413              	bhs	L126
1948                         ; 1505 				relay_A_phase1_ok_b = TRUE;
1950  0494 1c001d01          	bset	_relay_control_c,1
1951                         ; 1508 				relay_A_stuck_HI_b = FALSE;
1953  0498 1d001e01          	bclr	_relay_fault_status_c,1
1954                         ; 1511 				relay_A_short_at_FET_b = FALSE;
1956  049c 1d001d40          	bclr	_relay_control_c,64
1957                         ; 1514 				relay_A_2000ms_fault_counter_c = 0;
1959  04a0 790014            	clr	_relay_A_2000ms_fault_counter_c
1960                         ; 1516 				relay_A_stuck_HI_delay_c = 0;
1962  04a3 790004            	clr	_relay_A_stuck_HI_delay_c
1965  04a6 0a                	rtc	
1966  04a7                   L126:
1967                         ; 1521 				if (relay_A_stuck_HI_delay_c >= /*relay_phase1_delay_c*/relay_calibs_c[RL_PHASE1_DELAY] ) {
1969  04a7 f60004            	ldab	_relay_A_stuck_HI_delay_c
1970  04aa f10017            	cmpb	_relay_calibs_c+1
1971  04ad 2541              	blo	L526
1972                         ; 1525 					if ((relay_A_status_c == RELAY_MON_HIGH) && 
1972                         ; 1526 					   ((hs_Vsense_c[FRONT_LEFT] < /*hs_STG_Vsense_cutoff_c*/hs_flt_dtc_c[HS_STG_VSENSE])&&
1972                         ; 1527 					    (hs_Vsense_c[FRONT_RIGHT] < /*hs_STG_Vsense_cutoff_c*/hs_flt_dtc_c[HS_STG_VSENSE]))&&
1972                         ; 1528 					   ((vs_Vsense_c[VS_FL] < Vs_flt_cal_prms.Flt_Dtc[V_SHRT_TO_GND])&&
1972                         ; 1529 					   (vs_Vsense_c[VS_FR] < Vs_flt_cal_prms.Flt_Dtc[V_SHRT_TO_GND]))) {
1974  04af f60011            	ldab	_relay_A_status_c
1975  04b2 c101              	cmpb	#1
1976  04b4 2631              	bne	L726
1978  04b6 f60000            	ldab	_hs_Vsense_c
1979  04b9 f10003            	cmpb	_hs_flt_dtc_c+3
1980  04bc 2429              	bhs	L726
1982  04be f60001            	ldab	_hs_Vsense_c+1
1983  04c1 f10003            	cmpb	_hs_flt_dtc_c+3
1984  04c4 2421              	bhs	L726
1986  04c6 f60000            	ldab	_vs_Vsense_c
1987  04c9 f10001            	cmpb	_Vs_flt_cal_prms+1
1988  04cc 2419              	bhs	L726
1990  04ce f60001            	ldab	_vs_Vsense_c+1
1991  04d1 f10001            	cmpb	_Vs_flt_cal_prms+1
1992  04d4 2411              	bhs	L726
1993                         ; 1532 						relay_A_phase1_ok_b = FALSE;
1995  04d6 1d001d01          	bclr	_relay_control_c,1
1996                         ; 1535 						relay_A_Ok_b = FALSE;
1998  04da 1d002140          	bclr	_relay_status_c,64
1999                         ; 1538 						relay_A_stuck_HI_b = TRUE;
2001  04de 1c001e01          	bset	_relay_fault_status_c,1
2002                         ; 1541 						relay_A_short_at_FET_b = FALSE;
2004  04e2 1d001d40          	bclr	_relay_control_c,64
2007  04e6 0a                	rtc	
2008  04e7                   L726:
2009                         ; 1546 						relay_A_short_at_FET_b = TRUE;
2011  04e7 1c001d40          	bset	_relay_control_c,64
2012                         ; 1549 						relay_A_stuck_HI_b = FALSE;
2014  04eb 1d001e01          	bclr	_relay_fault_status_c,1
2016  04ef 0a                	rtc	
2017  04f0                   L526:
2018                         ; 1556 					relay_A_stuck_HI_delay_c++;
2020  04f0 720004            	inc	_relay_A_stuck_HI_delay_c
2021  04f3                   L736:
2022                         ; 1574 }
2025  04f3 0a                	rtc	
2061                         ; 1611 void rlLF_RelayBPortOFF_FETOFF_Phase1_Detection(void)
2061                         ; 1612 {
2062                         	switch	.ftext
2063  04f4                   f_rlLF_RelayBPortOFF_FETOFF_Phase1_Detection:
2067                         ; 1614 	if ((CSWM_config_c & CSWM_4HS_VARIANT_MASK_K) == CSWM_4HS_VARIANT_MASK_K)
2069  04f4 f60000            	ldab	_CSWM_config_c
2070  04f7 c40f              	andb	#15
2071  04f9 c10f              	cmpb	#15
2072  04fb 18260083          	bne	L107
2073                         ; 1617 		if ((!Rear_Seats_no_power_prelim_b) && (!Rear_Seats_no_power_final_b))
2075  04ff f6001c            	ldab	_relay_control2_c
2076  0502 c501              	bitb	#1
2077  0504 267c              	bne	L107
2079  0506 c502              	bitb	#2
2080  0508 2678              	bne	L107
2081                         ; 1620 			if ((relay_B_internal_Fault_b == FALSE)&&
2081                         ; 1621 			    (hs_status_flags_c[ REAR_LEFT ].b.hs_final_short_bat_b == FALSE) &&
2081                         ; 1622 			    (hs_status_flags_c[ REAR_RIGHT ].b.hs_final_short_bat_b == FALSE))
2083  050a 1e001d2073        	brset	_relay_control_c,32,L107
2085  050f 1e0002206e        	brset	_hs_status_flags_c+2,32,L107
2087  0514 1e00032069        	brset	_hs_status_flags_c+3,32,L107
2088                         ; 1625 				if (!relay_B_enabled_b)
2090  0519 1e00211064        	brset	_relay_status_c,16,L107
2091                         ; 1628 					if ((relay_B_status_c == RELAY_MON_LOW) && 
2091                         ; 1629 					   ((hs_Vsense_c[REAR_LEFT]<  /*hs_STG_Vsense_cutoff_c*/hs_flt_dtc_c[HS_STG_VSENSE]) && 
2091                         ; 1630 							   (hs_Vsense_c[REAR_RIGHT]<  /*hs_STG_Vsense_cutoff_c*/hs_flt_dtc_c[HS_STG_VSENSE])))
2093  051e f60010            	ldab	_relay_B_status_c
2094  0521 2623              	bne	L166
2096  0523 f60002            	ldab	_hs_Vsense_c+2
2097  0526 f10003            	cmpb	_hs_flt_dtc_c+3
2098  0529 241b              	bhs	L166
2100  052b f60003            	ldab	_hs_Vsense_c+3
2101  052e f10003            	cmpb	_hs_flt_dtc_c+3
2102  0531 2413              	bhs	L166
2103                         ; 1634 						relay_B_phase1_ok_b = 1;
2105  0533 1c001d02          	bset	_relay_control_c,2
2106                         ; 1637 						relay_B_stuck_HI_b = FALSE;
2108  0537 1d001e04          	bclr	_relay_fault_status_c,4
2109                         ; 1640 						relay_B_short_at_FET_b = FALSE;
2111  053b 1d001d80          	bclr	_relay_control_c,128
2112                         ; 1643 						relay_B_2000ms_fault_counter_c = 0;
2114  053f 790013            	clr	_relay_B_2000ms_fault_counter_c
2115                         ; 1645 						relay_B_stuck_HI_delay_c = 0;
2117  0542 790003            	clr	_relay_B_stuck_HI_delay_c
2120  0545 0a                	rtc	
2121  0546                   L166:
2122                         ; 1650 						if (relay_B_stuck_HI_delay_c >= /*relay_phase1_delay_c*/relay_calibs_c[RL_PHASE1_DELAY] )
2124  0546 f60003            	ldab	_relay_B_stuck_HI_delay_c
2125  0549 f10017            	cmpb	_relay_calibs_c+1
2126  054c 2531              	blo	L566
2127                         ; 1654 							if ((relay_B_status_c == RELAY_MON_HIGH) && 
2127                         ; 1655 							   ((hs_Vsense_c[REAR_LEFT] <  /*hs_STG_Vsense_cutoff_c*/hs_flt_dtc_c[HS_STG_VSENSE])&&
2127                         ; 1656 							   (hs_Vsense_c[REAR_RIGHT] <  /*hs_STG_Vsense_cutoff_c*/hs_flt_dtc_c[HS_STG_VSENSE])))
2129  054e f60010            	ldab	_relay_B_status_c
2130  0551 c101              	cmpb	#1
2131  0553 2621              	bne	L766
2133  0555 f60002            	ldab	_hs_Vsense_c+2
2134  0558 f10003            	cmpb	_hs_flt_dtc_c+3
2135  055b 2419              	bhs	L766
2137  055d f60003            	ldab	_hs_Vsense_c+3
2138  0560 f10003            	cmpb	_hs_flt_dtc_c+3
2139  0563 2411              	bhs	L766
2140                         ; 1659 								relay_B_phase1_ok_b = FALSE;
2142  0565 1d001d02          	bclr	_relay_control_c,2
2143                         ; 1662 								relay_B_Ok_b = FALSE;
2145  0569 1d002180          	bclr	_relay_status_c,128
2146                         ; 1665 								relay_B_stuck_HI_b = TRUE;
2148  056d 1c001e04          	bset	_relay_fault_status_c,4
2149                         ; 1668 								relay_B_short_at_FET_b = FALSE;
2151  0571 1d001d80          	bclr	_relay_control_c,128
2154  0575 0a                	rtc	
2155  0576                   L766:
2156                         ; 1673 								relay_B_short_at_FET_b = TRUE;
2158  0576 1c001d80          	bset	_relay_control_c,128
2159                         ; 1676 								relay_B_stuck_HI_b = FALSE;
2161  057a 1d001e04          	bclr	_relay_fault_status_c,4
2163  057e 0a                	rtc	
2164  057f                   L566:
2165                         ; 1681 							relay_B_stuck_HI_delay_c++;
2167  057f 720003            	inc	_relay_B_stuck_HI_delay_c
2168  0582                   L107:
2169                         ; 1700 }//End of Function
2172  0582 0a                	rtc	
2212                         ; 1732 void rlLF_RelayCPortOFF_FETOFF_Phase1_Detection(void)
2212                         ; 1733 {
2213                         	switch	.ftext
2214  0583                   f_rlLF_RelayCPortOFF_FETOFF_Phase1_Detection:
2218                         ; 1735 	if ((CSWM_config_c & CSWM_HSW_MASK_K) == CSWM_HSW_MASK_K)
2220  0583 f60000            	ldab	_CSWM_config_c
2221  0586 c440              	andb	#64
2222  0588 c140              	cmpb	#64
2223  058a 182600f3          	bne	L317
2224                         ; 1743 		if ((relay_C_internal_Fault_b == FALSE) &&
2224                         ; 1744 		    (stW_Flt_status_w != STW_MTRD_BATT_MASK) &&
2224                         ; 1745 		    (stW_Flt_status_w != STW_MTRD_GND_MASK) )
2226  058e f6001f            	ldab	_relay_fault_status2_c
2227  0591 c501              	bitb	#1
2228  0593 182600df          	bne	L517
2230  0597 fc0000            	ldd	_stW_Flt_status_w
2231  059a 8c0200            	cpd	#512
2232  059d 182700d5          	beq	L517
2234  05a1 8c0100            	cpd	#256
2235  05a4 182700ce          	beq	L517
2236                         ; 1748 			if (!relay_C_enabled_b)
2238  05a8 f60020            	ldab	_relay_status2_c
2239  05ab c502              	bitb	#2
2240  05ad 182600b3          	bne	L717
2241                         ; 1751 				if (relay_fault_detection_type_c == HSW_PHASE1_TEST_VOLT_OFF)
2243  05b1 f60005            	ldab	_relay_fault_detection_type_c
2244  05b4 53                	decb	
2245  05b5 2668              	bne	L127
2246                         ; 1754 					if (relay_delay_time_for_steering_c >=  /*relay_phase1_delay_c*/relay_calibs_c[RL_PHASE1_DELAY])
2248  05b7 f60006            	ldab	_relay_delay_time_for_steering_c
2249  05ba f10017            	cmpb	_relay_calibs_c+1
2250  05bd 1825009f          	blo	L357
2251                         ; 1757 						relay_C_Test_Voltage_Pin_status_c = (unsigned char) Dio_ReadChannel(U12T_EN);
2253  05c1 e6fbfa3b          	ldab	[_Dio_PortRead_Ptr]
2254  05c5 c410              	andb	#16
2255  05c7 54                	lsrb	
2256  05c8 54                	lsrb	
2257  05c9 54                	lsrb	
2258  05ca 54                	lsrb	
2259  05cb 7b000e            	stab	_relay_C_Test_Voltage_Pin_status_c
2260                         ; 1760 						if(relay_C_Test_Voltage_Pin_status_c == STD_LOW){
2262  05ce 263c              	bne	L527
2263                         ; 1764 							if(relay_C_STB_delay_c >= /*relay_stw_stb_delay_c*/relay_calibs_c[RL_HSW_STB_DELAY]){
2265  05d0 f60001            	ldab	_relay_C_STB_delay_c
2266  05d3 f10019            	cmpb	_relay_calibs_c+3
2267  05d6 2530              	blo	L727
2268                         ; 1766 								if( (stW_Vsense_HS_c > stW_flt_calibs_c[STW_SHRT_TO_BATT_VSENSE]) || 
2268                         ; 1767 									(stW_Vsense_LS_c > stW_flt_calibs_c[STW_SHRT_TO_BATT_VSENSE]) ){
2270  05d8 f60000            	ldab	_stW_Vsense_HS_c
2271  05db f10004            	cmpb	_stW_flt_calibs_c+4
2272  05de 2208              	bhi	L337
2274  05e0 f60000            	ldab	_stW_Vsense_LS_c
2275  05e3 f10004            	cmpb	_stW_flt_calibs_c+4
2276  05e6 2305              	bls	L137
2277  05e8                   L337:
2278                         ; 1769 									relay_C_phase1_Test_Voltage_OFF_fault_b = TRUE;								
2280  05e8 1c002040          	bset	_relay_status2_c,64
2283  05ec 0a                	rtc	
2284  05ed                   L137:
2285                         ; 1772 									if ((stW_sw_rq_b == TRUE) && (stW_state_b == FALSE))
2287  05ed 1f00000411        	brclr	_stW_Flag1_c,4,L737
2289  05f2 1e0000010c        	brset	_stW_Flag1_c,1,L737
2290                         ; 1775 										relay_delay_time_for_steering_c = 0;
2292  05f7 790006            	clr	_relay_delay_time_for_steering_c
2293                         ; 1777 										relay_fault_detection_type_c = HSW_PHASE1_TEST_VOLT_ON;
2295  05fa c602              	ldab	#2
2296  05fc 7b0005            	stab	_relay_fault_detection_type_c
2297                         ; 1779 										relay_C_keep_U12S_ON_b = TRUE;
2299  05ff 1c001c10          	bset	_relay_control2_c,16
2300  0603                   L737:
2301                         ; 1782 									relay_C_phase1_Test_Voltage_OFF_fault_b = FALSE;
2303  0603 1d002040          	bclr	_relay_status2_c,64
2305  0607 0a                	rtc	
2306  0608                   L727:
2307                         ; 1786 								relay_C_STB_delay_c++;
2309  0608 720001            	inc	_relay_C_STB_delay_c
2311  060b 0a                	rtc	
2312  060c                   L527:
2313                         ; 1791 							relay_C_STB_delay_c = 0;
2315  060c 790001            	clr	_relay_C_STB_delay_c
2316                         ; 1794 							if( (stW_sw_rq_b == FALSE) && (relay_C_keep_U12S_ON_b == TRUE) ){
2318  060f f60000            	ldab	_stW_Flag1_c
2319  0612 c504              	bitb	#4
2320  0614 2677              	bne	L3001
2322  0616 f6001c            	ldab	_relay_control2_c
2323  0619 c510              	bitb	#16
2324  061b 2770              	beq	L3001
2325                         ; 1795 								relay_C_keep_U12S_ON_b = FALSE;
2327  061d 2052              	bra	LC006
2328                         ; 1802 						relay_delay_time_for_steering_c++;
2330  061f                   L127:
2331                         ; 1808 					relay_C_STB_delay_c = 0;
2333  061f 790001            	clr	_relay_C_STB_delay_c
2334                         ; 1810 					if (relay_delay_time_for_steering_c >= /*relay_phase1_delay_c*/relay_calibs_c[RL_PHASE1_DELAY])
2336  0622 f60006            	ldab	_relay_delay_time_for_steering_c
2337  0625 f10017            	cmpb	_relay_calibs_c+1
2338  0628 2536              	blo	L357
2339                         ; 1816 						if(main_kept_U12S_ON_b == TRUE){
2341  062a 1f0000045e        	brclr	_main_flag2_c,4,L3001
2342                         ; 1818 							if(stW_Vsense_LS_c < stW_flt_calibs_c[STW_SHRT_TO_GND_VSENSE]){
2344  062f f60000            	ldab	_stW_Vsense_LS_c
2345  0632 f10003            	cmpb	_stW_flt_calibs_c+3
2346  0635 2406              	bhs	L757
2347                         ; 1820 								relay_C_phase1_Test_Voltage_ON_fault_b = TRUE;	
2349  0637 1c002020          	bset	_relay_status2_c,32
2351  063b 2004              	bra	L167
2352  063d                   L757:
2353                         ; 1823 								relay_C_phase1_Test_Voltage_ON_fault_b = FALSE;
2355  063d 1d002020          	bclr	_relay_status2_c,32
2356  0641                   L167:
2357                         ; 1826 							if ( (!relay_C_phase1_Test_Voltage_OFF_fault_b) && 
2357                         ; 1827 								    ((!relay_C_phase1_Test_Voltage_ON_fault_b) &&
2357                         ; 1828 								    (relay_fault_detection_type_c == HSW_PHASE1_TEST_VOLT_ON)) )
2359  0641 1e00204012        	brset	_relay_status2_c,64,L367
2361  0646 1e0020200d        	brset	_relay_status2_c,32,L367
2363  064b f60005            	ldab	_relay_fault_detection_type_c
2364  064e c102              	cmpb	#2
2365  0650 2606              	bne	L367
2366                         ; 1831 								relay_C_phase1_ok_b = TRUE;
2368  0652 1c002008          	bset	_relay_status2_c,8
2370  0656 2004              	bra	L567
2371  0658                   L367:
2372                         ; 1834 								relay_C_phase1_ok_b = FALSE;
2374  0658 1d002008          	bclr	_relay_status2_c,8
2375  065c                   L567:
2376                         ; 1836 							relay_delay_time_for_steering_c = 0;
2378  065c 790006            	clr	_relay_delay_time_for_steering_c
2381  065f 0a                	rtc	
2382  0660                   L357:
2383                         ; 1844 						relay_delay_time_for_steering_c++;
2385  0660 720006            	inc	_relay_delay_time_for_steering_c
2387  0663 0a                	rtc	
2388  0664                   L717:
2389                         ; 1853 				relay_C_STB_delay_c = 0;
2391  0664 790001            	clr	_relay_C_STB_delay_c
2392                         ; 1856 				if( (stW_sw_rq_b == FALSE) && (relay_C_keep_U12S_ON_b == TRUE) ) {
2394  0667 1e00000421        	brset	_stW_Flag1_c,4,L3001
2396  066c 1f001c101c        	brclr	_relay_control2_c,16,L3001
2397                         ; 1857 					relay_C_keep_U12S_ON_b = FALSE;
2399  0671                   LC006:
2400  0671 1d001c10          	bclr	_relay_control2_c,16
2402  0675 0a                	rtc	
2403  0676                   L517:
2404                         ; 1866 			if(relay_C_keep_U12S_ON_b == TRUE){
2406  0676 1f001c1004        	brclr	_relay_control2_c,16,L1001
2407                         ; 1867 				relay_C_keep_U12S_ON_b = FALSE;
2409  067b 1d001c10          	bclr	_relay_control2_c,16
2410  067f                   L1001:
2411                         ; 1870 			relay_C_STB_delay_c = 0;
2413  067f 2009              	bra	L5001
2414  0681                   L317:
2415                         ; 1876 		if(relay_C_keep_U12S_ON_b == TRUE){
2417  0681 1f001c1004        	brclr	_relay_control2_c,16,L5001
2418                         ; 1877 			relay_C_keep_U12S_ON_b = FALSE;
2420  0686 1d001c10          	bclr	_relay_control2_c,16
2421  068a                   L5001:
2422                         ; 1880 		relay_C_STB_delay_c = 0;
2424  068a 790001            	clr	_relay_C_STB_delay_c
2425  068d                   L3001:
2426                         ; 1882 }
2429  068d 0a                	rtc	
2464                         ; 1915 void rlLF_RelayAPortON_FETOFF_Phase2_Detection(void)
2464                         ; 1916 {
2465                         	switch	.ftext
2466  068e                   f_rlLF_RelayAPortON_FETOFF_Phase2_Detection:
2470                         ; 1918 	if ((relay_A_internal_Fault_b == FALSE) &&
2470                         ; 1919 	    (hs_status_flags_c[ FRONT_LEFT ].b.hs_final_short_bat_b == FALSE) &&
2470                         ; 1920 	    (hs_status_flags_c[ FRONT_RIGHT ].b.hs_final_short_bat_b == FALSE)&&
2470                         ; 1921 	    (vs_shrtTo_batt_mntr_c != VS_SHRT_TO_BATT)) {
2472  068e f6001d            	ldab	_relay_control_c
2473  0691 c510              	bitb	#16
2474  0693 182600cd          	bne	L1601
2476  0697 f60000            	ldab	_hs_status_flags_c
2477  069a c520              	bitb	#32
2478  069c 182600c4          	bne	L1601
2480  06a0 f60001            	ldab	_hs_status_flags_c+1
2481  06a3 c520              	bitb	#32
2482  06a5 182600bb          	bne	L1601
2484  06a9 f60000            	ldab	_vs_shrtTo_batt_mntr_c
2485  06ac c102              	cmpb	#2
2486  06ae 182700b2          	beq	L1601
2487                         ; 1924 		if (relay_A_enabled_b && !relay_A_phase2_ok_b && relay_A_phase1_ok_b && !relay_A_short_at_FET_b) {
2489  06b2 f60021            	ldab	_relay_status_c
2490  06b5 c508              	bitb	#8
2491  06b7 182700a9          	beq	L1601
2493  06bb f6001d            	ldab	_relay_control_c
2494  06be c504              	bitb	#4
2495  06c0 182600a0          	bne	L1601
2497  06c4 c501              	bitb	#1
2498  06c6 1827009a          	beq	L1601
2500  06ca c540              	bitb	#64
2501  06cc 18260094          	bne	L1601
2502                         ; 1928 			if ((relay_A_status_c == RELAY_MON_LOW) && 
2502                         ; 1929 			   ((hs_Vsense_c[FRONT_LEFT] <  /*hs_STG_Vsense_cutoff_c*/hs_flt_dtc_c[HS_STG_VSENSE])||
2502                         ; 1930 			   (hs_Vsense_c[FRONT_RIGHT] <  /*hs_STG_Vsense_cutoff_c*/hs_flt_dtc_c[HS_STG_VSENSE])||
2502                         ; 1931 			   (vs_Vsense_c[VS_FL] <  Vs_flt_cal_prms.Flt_Dtc[V_SHRT_TO_GND])|| 
2502                         ; 1932 			   (vs_Vsense_c[VS_FR] <  Vs_flt_cal_prms.Flt_Dtc[V_SHRT_TO_GND]))) {
2504  06d0 f60011            	ldab	_relay_A_status_c
2505  06d3 262d              	bne	L3201
2507  06d5 f60000            	ldab	_hs_Vsense_c
2508  06d8 f10003            	cmpb	_hs_flt_dtc_c+3
2509  06db 2518              	blo	L5201
2511  06dd f60001            	ldab	_hs_Vsense_c+1
2512  06e0 f10003            	cmpb	_hs_flt_dtc_c+3
2513  06e3 2510              	blo	L5201
2515  06e5 f60000            	ldab	_vs_Vsense_c
2516  06e8 f10001            	cmpb	_Vs_flt_cal_prms+1
2517  06eb 2508              	blo	L5201
2519  06ed f60001            	ldab	_vs_Vsense_c+1
2520  06f0 f10001            	cmpb	_Vs_flt_cal_prms+1
2521  06f3 240d              	bhs	L3201
2522  06f5                   L5201:
2523                         ; 1935 				relay_A_stuck_LOW_b = TRUE;
2525  06f5 1c001e02          	bset	_relay_fault_status_c,2
2526                         ; 1938 				relay_A_phase2_ok_b = FALSE;
2528  06f9 1d001d04          	bclr	_relay_control_c,4
2529                         ; 1941 				relay_A_Ok_b = FALSE;
2531  06fd 1d002140          	bclr	_relay_status_c,64
2534  0701 0a                	rtc	
2535  0702                   L3201:
2536                         ; 1945 				if (relay_A_status_c == RELAY_MON_HIGH) {
2538  0702 f60011            	ldab	_relay_A_status_c
2539  0705 04215c            	dbne	b,L1601
2540                         ; 1948 					if ((hs_Vsense_c[FRONT_LEFT] <  /*hs_STB_Vsense_cutoff_c*/hs_flt_dtc_c[HS_STB_VSENSE]) &&
2540                         ; 1949 						(hs_Vsense_c[FRONT_RIGHT] <  /*hs_STB_Vsense_cutoff_c*/hs_flt_dtc_c[HS_STB_VSENSE]) &&
2540                         ; 1950 						(vs_Vsense_c[VS_FL] <  Vs_flt_cal_prms.Flt_Dtc[V_SHRT_TO_BATT]) && 
2540                         ; 1951 					    (vs_Vsense_c[VS_FR] <  Vs_flt_cal_prms.Flt_Dtc[V_SHRT_TO_BATT]))
2542  0708 f60000            	ldab	_hs_Vsense_c
2543  070b f10002            	cmpb	_hs_flt_dtc_c+2
2544  070e 2428              	bhs	L7301
2546  0710 f60001            	ldab	_hs_Vsense_c+1
2547  0713 f10002            	cmpb	_hs_flt_dtc_c+2
2548  0716 2420              	bhs	L7301
2550  0718 f60000            	ldab	_vs_Vsense_c
2551  071b f10002            	cmpb	_Vs_flt_cal_prms+2
2552  071e 2418              	bhs	L7301
2554  0720 f60001            	ldab	_vs_Vsense_c+1
2555  0723 f10002            	cmpb	_Vs_flt_cal_prms+2
2556  0726 2410              	bhs	L7301
2557                         ; 1955 						relay_A_phase2_ok_b = TRUE;
2559  0728 1c001d04          	bset	_relay_control_c,4
2560                         ; 1958 						relay_A_stuck_LOW_b = FALSE;
2562  072c 1d001e02          	bclr	_relay_fault_status_c,2
2563                         ; 1961 						relay_A_Ok_b = TRUE;
2565  0730 1c002140          	bset	_relay_status_c,64
2566                         ; 1964 						relay_A_2000ms_fault_counter_c = 0;
2568  0734 790014            	clr	_relay_A_2000ms_fault_counter_c
2571  0737 0a                	rtc	
2572  0738                   L7301:
2573                         ; 1969 						if ( (hs_Vsense_c[FRONT_LEFT] >= /*hs_STB_Vsense_cutoff_c*/hs_flt_dtc_c[HS_STB_VSENSE]) ||
2573                         ; 1970 							 (hs_Vsense_c[FRONT_RIGHT] >= /*hs_STB_Vsense_cutoff_c*/hs_flt_dtc_c[HS_STB_VSENSE]) ||
2573                         ; 1971 							 (vs_Vsense_c[VS_FL] >= Vs_flt_cal_prms.Flt_Dtc[V_SHRT_TO_BATT]) || 
2573                         ; 1972 							 (vs_Vsense_c[VS_FR] >= Vs_flt_cal_prms.Flt_Dtc[V_SHRT_TO_BATT]) ){
2575  0738 f60000            	ldab	_hs_Vsense_c
2576  073b f10002            	cmpb	_hs_flt_dtc_c+2
2577  073e 2418              	bhs	L5401
2579  0740 f60001            	ldab	_hs_Vsense_c+1
2580  0743 f10002            	cmpb	_hs_flt_dtc_c+2
2581  0746 2410              	bhs	L5401
2583  0748 f60000            	ldab	_vs_Vsense_c
2584  074b f10002            	cmpb	_Vs_flt_cal_prms+2
2585  074e 2408              	bhs	L5401
2587  0750 f60001            	ldab	_vs_Vsense_c+1
2588  0753 f10002            	cmpb	_Vs_flt_cal_prms+2
2589  0756 250c              	blo	L1601
2590  0758                   L5401:
2591                         ; 1974 							relay_A_phase2_ok_b = FALSE;
2593  0758 1d001d04          	bclr	_relay_control_c,4
2594                         ; 1977 							relay_A_Ok_b = FALSE;
2596  075c 1d002140          	bclr	_relay_status_c,64
2597                         ; 1980 							relay_F_FET_fault_b = TRUE;
2599  0760 1c001e10          	bset	_relay_fault_status_c,16
2601  0764                   L1601:
2602                         ; 2075 } //End of function
2605  0764 0a                	rtc	
2639                         ; 2109 void rlLF_RelayBPortON_FETOFF_Phase2_Detection(void)
2639                         ; 2110 {
2640                         	switch	.ftext
2641  0765                   f_rlLF_RelayBPortON_FETOFF_Phase2_Detection:
2645                         ; 2112 	if ((CSWM_config_c & CSWM_4HS_VARIANT_MASK_K) == CSWM_4HS_VARIANT_MASK_K) {
2647  0765 f60000            	ldab	_CSWM_config_c
2648  0768 c40f              	andb	#15
2649  076a c10f              	cmpb	#15
2650  076c 182600a0          	bne	L3311
2651                         ; 2115 		if ((!Rear_Seats_no_power_prelim_b) && (!Rear_Seats_no_power_final_b))  {
2653  0770 f6001c            	ldab	_relay_control2_c
2654  0773 c501              	bitb	#1
2655  0775 18260097          	bne	L3311
2657  0779 c502              	bitb	#2
2658  077b 18260091          	bne	L3311
2659                         ; 2117 			if ((relay_B_internal_Fault_b == FALSE)&&
2659                         ; 2118 			    (hs_status_flags_c[ REAR_LEFT ].b.hs_final_short_bat_b == FALSE) &&
2659                         ; 2119 			    (hs_status_flags_c[ REAR_RIGHT ].b.hs_final_short_bat_b == FALSE)) {
2661  077f f6001d            	ldab	_relay_control_c
2662  0782 c520              	bitb	#32
2663  0784 18260088          	bne	L3311
2665  0788 f60002            	ldab	_hs_status_flags_c+2
2666  078b c520              	bitb	#32
2667  078d 1826007f          	bne	L3311
2669  0791 f60003            	ldab	_hs_status_flags_c+3
2670  0794 c520              	bitb	#32
2671  0796 2678              	bne	L3311
2672                         ; 2122 				if (relay_B_enabled_b && !relay_B_phase2_ok_b && relay_B_phase1_ok_b && !relay_B_short_at_FET_b) {
2674  0798 1f00211073        	brclr	_relay_status_c,16,L3311
2676  079d 1e001d086e        	brset	_relay_control_c,8,L3311
2678  07a2 1f001d0269        	brclr	_relay_control_c,2,L3311
2680  07a7 1e001d8064        	brset	_relay_control_c,128,L3311
2681                         ; 2126 					if ((relay_B_status_c == RELAY_MON_LOW) && 
2681                         ; 2127 					   ((hs_Vsense_c[REAR_LEFT] <  /*hs_STG_Vsense_cutoff_c*/hs_flt_dtc_c[HS_STG_VSENSE])|| 
2681                         ; 2128 					   (hs_Vsense_c[REAR_RIGHT] <  /*hs_STG_Vsense_cutoff_c*/hs_flt_dtc_c[HS_STG_VSENSE]))) {
2683  07ac f60010            	ldab	_relay_B_status_c
2684  07af 261d              	bne	L3011
2686  07b1 f60002            	ldab	_hs_Vsense_c+2
2687  07b4 f10003            	cmpb	_hs_flt_dtc_c+3
2688  07b7 2508              	blo	L5011
2690  07b9 f60003            	ldab	_hs_Vsense_c+3
2691  07bc f10003            	cmpb	_hs_flt_dtc_c+3
2692  07bf 240d              	bhs	L3011
2693  07c1                   L5011:
2694                         ; 2131 						relay_B_stuck_LOW_b = TRUE;
2696  07c1 1c001e08          	bset	_relay_fault_status_c,8
2697                         ; 2137 						relay_B_phase2_ok_b = FALSE;
2699  07c5 1d001d08          	bclr	_relay_control_c,8
2700                         ; 2140 						relay_B_Ok_b = FALSE;
2702  07c9 1d002180          	bclr	_relay_status_c,128
2705  07cd 0a                	rtc	
2706  07ce                   L3011:
2707                         ; 2145 						if (relay_B_status_c == RELAY_MON_HIGH) {
2709  07ce f60010            	ldab	_relay_B_status_c
2710  07d1 04213c            	dbne	b,L3311
2711                         ; 2148 							if ((hs_Vsense_c[REAR_LEFT] <  /*hs_STB_Vsense_cutoff_c*/hs_flt_dtc_c[HS_STB_VSENSE])&& 
2711                         ; 2149 							   (hs_Vsense_c[REAR_RIGHT] <  /*hs_STB_Vsense_cutoff_c*/hs_flt_dtc_c[HS_STB_VSENSE])) {
2713  07d4 f60002            	ldab	_hs_Vsense_c+2
2714  07d7 f10002            	cmpb	_hs_flt_dtc_c+2
2715  07da 2418              	bhs	L3111
2717  07dc f60003            	ldab	_hs_Vsense_c+3
2718  07df f10002            	cmpb	_hs_flt_dtc_c+2
2719  07e2 2410              	bhs	L3111
2720                         ; 2152 								relay_B_phase2_ok_b = TRUE;
2722  07e4 1c001d08          	bset	_relay_control_c,8
2723                         ; 2155 								relay_B_stuck_LOW_b = FALSE;
2725  07e8 1d001e08          	bclr	_relay_fault_status_c,8
2726                         ; 2158 								relay_B_Ok_b = TRUE;
2728  07ec 1c002180          	bset	_relay_status_c,128
2729                         ; 2161 								relay_B_2000ms_fault_counter_c = 0;
2731  07f0 790013            	clr	_relay_B_2000ms_fault_counter_c
2734  07f3 0a                	rtc	
2735  07f4                   L3111:
2736                         ; 2167 								if ( (hs_Vsense_c[REAR_LEFT] >= /*hs_STB_Vsense_cutoff_c*/hs_flt_dtc_c[HS_STB_VSENSE]) ||
2736                         ; 2168 									 (hs_Vsense_c[REAR_RIGHT] >= /*hs_STB_Vsense_cutoff_c*/hs_flt_dtc_c[HS_STB_VSENSE]) ){
2738  07f4 f60002            	ldab	_hs_Vsense_c+2
2739  07f7 f10002            	cmpb	_hs_flt_dtc_c+2
2740  07fa 2408              	bhs	L1211
2742  07fc f60003            	ldab	_hs_Vsense_c+3
2743  07ff f10002            	cmpb	_hs_flt_dtc_c+2
2744  0802 250c              	blo	L3311
2745  0804                   L1211:
2746                         ; 2171 									relay_B_phase2_ok_b = 0;
2748  0804 1d001d08          	bclr	_relay_control_c,8
2749                         ; 2174 									relay_B_Ok_b = FALSE;
2751  0808 1d002180          	bclr	_relay_status_c,128
2752                         ; 2177 									relay_R_FET_fault_b = TRUE;
2754  080c 1c001e20          	bset	_relay_fault_status_c,32
2756  0810                   L3311:
2757                         ; 2253 } //End of function
2760  0810 0a                	rtc	
2793                         ; 2284 void rlLF_RelayCPortON_FETOFF_Phase2_Detection(void)
2793                         ; 2285 {
2794                         	switch	.ftext
2795  0811                   f_rlLF_RelayCPortON_FETOFF_Phase2_Detection:
2799                         ; 2287 	if ((CSWM_config_c & CSWM_HSW_MASK_K) == CSWM_HSW_MASK_K)
2801  0811 1f0000404a        	brclr	_CSWM_config_c,64,L7411
2802                         ; 2294 		if ((relay_C_internal_Fault_b == FALSE)&&
2802                         ; 2295 		    (stW_Flt_status_w != STW_MTRD_BATT_MASK) &&
2802                         ; 2296 		    (stW_Flt_status_w != STW_MTRD_GND_MASK) )
2804  0816 1e001f0145        	brset	_relay_fault_status2_c,1,L7411
2806  081b fc0000            	ldd	_stW_Flt_status_w
2807  081e 8c0200            	cpd	#512
2808  0821 273d              	beq	L7411
2810  0823 8c0100            	cpd	#256
2811  0826 2738              	beq	L7411
2812                         ; 2305 			if ( (relay_C_enabled_b == TRUE) && (relay_C_phase1_ok_b == TRUE) && 
2812                         ; 2306 				 (relay_C_phase2_ok_b == FALSE) && (relay_C_internal_Fault_b == FALSE) )
2814  0828 1f0020023c        	brclr	_relay_status2_c,2,L1711
2816  082d 1f00200837        	brclr	_relay_status2_c,8,L1711
2818  0832 1e00201032        	brset	_relay_status2_c,16,L1711
2820  0837 1e001f012d        	brset	_relay_fault_status2_c,1,L1711
2821                         ; 2312 				if(main_kept_U12S_ON_b == TRUE){
2823  083c 1f00000428        	brclr	_main_flag2_c,4,L1711
2824                         ; 2314 					if(stW_Vsense_LS_c > /*relay_stw_stk_open_c*/relay_calibs_c[RL_HSW_STUCK_OPEN]){
2826  0841 f60000            	ldab	_stW_Vsense_LS_c
2827  0844 f1001a            	cmpb	_relay_calibs_c+4
2828  0847 230a              	bls	L5511
2829                         ; 2316 						relay_C_stuck_LOW_b = TRUE;
2831  0849 1c001f04          	bset	_relay_fault_status2_c,4
2832                         ; 2318 						relay_C_phase2_ok_b = FALSE;
2834                         ; 2320 						relay_C_Ok_b = FALSE;						
2836  084d 1d002014          	bclr	_relay_status2_c,20
2838  0851 2012              	bra	LC008
2839  0853                   L5511:
2840                         ; 2323 						relay_C_stuck_LOW_b = FALSE;
2842  0853 1d001f04          	bclr	_relay_fault_status2_c,4
2843                         ; 2325 						relay_C_phase2_ok_b = TRUE;
2845                         ; 2327 						relay_C_Ok_b = TRUE;								
2847  0857 1c002014          	bset	_relay_status2_c,20
2848                         ; 2330 						relay_C_2000ms_fault_counter_c = 0;
2850  085b 790012            	clr	_relay_C_2000ms_fault_counter_c
2851                         ; 2333 					relay_C_keep_U12S_ON_b = FALSE;
2854  085e 2005              	bra	LC008
2855  0860                   L7411:
2856                         ; 2349 			if(relay_C_keep_U12S_ON_b == TRUE){
2858                         ; 2350 				relay_C_keep_U12S_ON_b = FALSE;
2860                         ; 2358 		if(relay_C_keep_U12S_ON_b == TRUE){
2862  0860 1f001c1004        	brclr	_relay_control2_c,16,L1711
2863                         ; 2359 			relay_C_keep_U12S_ON_b = FALSE;
2865  0865                   LC008:
2866  0865 1d001c10          	bclr	_relay_control2_c,16
2867  0869                   L1711:
2868                         ; 2362 }
2871  0869 0a                	rtc	
2903                         ; 2384 void rlLF_MatureTime_For_RelayA_InternalFault(void)
2903                         ; 2385 {
2904                         	switch	.ftext
2905  086a                   f_rlLF_MatureTime_For_RelayA_InternalFault:
2909                         ; 2387 	if ((relay_A_short_at_FET_b == FALSE) &&
2909                         ; 2388 	    (hs_status_flags_c[ FRONT_LEFT ].b.hs_final_short_bat_b == FALSE) &&
2909                         ; 2389 	    (hs_status_flags_c[ FRONT_RIGHT ].b.hs_final_short_bat_b == FALSE)&&
2909                         ; 2390 	    (vs_shrtTo_batt_mntr_c != VS_SHRT_TO_BATT) &&
2909                         ; 2391 	    (!relay_A_internal_Fault_b)) {
2911  086a 1e001d404f        	brset	_relay_control_c,64,L5021
2913  086f 1e0000204a        	brset	_hs_status_flags_c,32,L5021
2915  0874 1e00012045        	brset	_hs_status_flags_c+1,32,L5021
2917  0879 f60000            	ldab	_vs_shrtTo_batt_mntr_c
2918  087c c102              	cmpb	#2
2919  087e 273e              	beq	L5021
2921  0880 1e001d1039        	brset	_relay_control_c,16,L5021
2922                         ; 2394 		if ((relay_A_enabled_b && !relay_A_phase2_ok_b) ||  !relay_A_phase1_ok_b) {
2924  0885 1f00210805        	brclr	_relay_status_c,8,L3121
2926  088a 1f001d0405        	brclr	_relay_control_c,4,L1121
2927  088f                   L3121:
2929  088f 1e001d012d        	brset	_relay_control_c,1,L7221
2930  0894                   L1121:
2931                         ; 2397 			if (relay_A_2000ms_fault_counter_c >= /*mature_2sec_time_c*/hs_flt_dtc_c[HS_2SEC_MATURE_TIME]) {
2933  0894 f60014            	ldab	_relay_A_2000ms_fault_counter_c
2934  0897 f10004            	cmpb	_hs_flt_dtc_c+4
2935  089a 251e              	blo	L5121
2936                         ; 2400 				relay_A_internal_Fault_b = TRUE;
2938  089c 1c001d10          	bset	_relay_control_c,16
2939                         ; 2403 				relay_A_2000ms_fault_counter_c = 0;
2941  08a0 790014            	clr	_relay_A_2000ms_fault_counter_c
2942                         ; 2408 				if(hs_rem_seat_req_c[FRONT_LEFT] != SET_OFF_REQUEST) 
2944  08a3 f60000            	ldab	_hs_rem_seat_req_c
2945  08a6 2706              	beq	L7121
2946                         ; 2412 					hs_monitor_2sec_led_counter_c[ FRONT_LEFT ] = /*mature_2sec_time_c*/hs_flt_dtc_c[HS_2SEC_MATURE_TIME];
2948  08a8 180c00040000      	movb	_hs_flt_dtc_c+4,_hs_monitor_2sec_led_counter_c
2949  08ae                   L7121:
2950                         ; 2415 				if(hs_rem_seat_req_c[FRONT_RIGHT] != SET_OFF_REQUEST) 
2952  08ae f60001            	ldab	_hs_rem_seat_req_c+1
2953  08b1 270e              	beq	L7221
2954                         ; 2419 					hs_monitor_2sec_led_counter_c[ FRONT_RIGHT ] = /*mature_2sec_time_c*/hs_flt_dtc_c[HS_2SEC_MATURE_TIME];
2956  08b3 f60004            	ldab	_hs_flt_dtc_c+4
2957  08b6 7b0001            	stab	_hs_monitor_2sec_led_counter_c+1
2959  08b9 0a                	rtc	
2960  08ba                   L5121:
2961                         ; 2425 				relay_A_2000ms_fault_counter_c++;
2963  08ba 720014            	inc	_relay_A_2000ms_fault_counter_c
2965  08bd 0a                	rtc	
2966  08be                   L5021:
2967                         ; 2438 		relay_A_2000ms_fault_counter_c = 0;
2969  08be 790014            	clr	_relay_A_2000ms_fault_counter_c
2970  08c1                   L7221:
2971                         ; 2442 } //End of Function
2974  08c1 0a                	rtc	
3007                         ; 2468 void rlLF_MatureTime_For_RelayB_InternalFault(void)
3007                         ; 2469 {
3008                         	switch	.ftext
3009  08c2                   f_rlLF_MatureTime_For_RelayB_InternalFault:
3013                         ; 2471 	if ((CSWM_config_c & CSWM_4HS_VARIANT_MASK_K) == CSWM_4HS_VARIANT_MASK_K) {
3015  08c2 f60000            	ldab	_CSWM_config_c
3016  08c5 c40f              	andb	#15
3017  08c7 c10f              	cmpb	#15
3018  08c9 266d              	bne	L7721
3019                         ; 2474 		if ((relay_B_short_at_FET_b == FALSE) &&
3019                         ; 2475 		    (hs_status_flags_c[ REAR_LEFT ].b.hs_final_short_bat_b == FALSE) &&
3019                         ; 2476 		    (hs_status_flags_c[ REAR_RIGHT ].b.hs_final_short_bat_b == FALSE)&&
3019                         ; 2477 		    (!relay_B_internal_Fault_b) &&
3019                         ; 2478 		    (!Rear_Seats_no_power_final_b)) {
3021  08cb f6001d            	ldab	_relay_control_c
3022  08ce c580              	bitb	#128
3023  08d0 2663              	bne	L3421
3025  08d2 1e0002205e        	brset	_hs_status_flags_c+2,32,L3421
3027  08d7 1e00032059        	brset	_hs_status_flags_c+3,32,L3421
3029  08dc c520              	bitb	#32
3030  08de 2655              	bne	L3421
3032  08e0 1e001c0250        	brset	_relay_control2_c,2,L3421
3033                         ; 2481 			if ((!Rear_Seats_no_power_prelim_b) && (!Rear_Seats_no_power_final_b)) {
3035  08e5 1e001c0139        	brset	_relay_control2_c,1,L5421
3037  08ea 1e001c0234        	brset	_relay_control2_c,2,L5421
3038                         ; 2484 				if ((relay_B_enabled_b && !relay_B_phase2_ok_b) ||  !relay_B_phase1_ok_b) {
3040  08ef 1f00211004        	brclr	_relay_status_c,16,L3521
3042  08f4 c508              	bitb	#8
3043  08f6 2705              	beq	L1521
3044  08f8                   L3521:
3046  08f8 1e001d023b        	brset	_relay_control_c,2,L7721
3047  08fd                   L1521:
3048                         ; 2487 					if (relay_B_2000ms_fault_counter_c >= /*mature_2sec_time_c*/hs_flt_dtc_c[HS_2SEC_MATURE_TIME]) {
3050  08fd f60013            	ldab	_relay_B_2000ms_fault_counter_c
3051  0900 f10004            	cmpb	_hs_flt_dtc_c+4
3052  0903 252c              	blo	L1721
3053                         ; 2490 						relay_B_internal_Fault_b = TRUE;
3055  0905 1c001d20          	bset	_relay_control_c,32
3056                         ; 2493 						relay_B_2000ms_fault_counter_c = 0;
3058  0909 790013            	clr	_relay_B_2000ms_fault_counter_c
3059                         ; 2498 						if(hs_rem_seat_req_c[REAR_LEFT] != SET_OFF_REQUEST) 
3061  090c f60002            	ldab	_hs_rem_seat_req_c+2
3062  090f 2706              	beq	L7521
3063                         ; 2502 							hs_monitor_2sec_led_counter_c[ REAR_LEFT ] = /*mature_2sec_time_c*/hs_flt_dtc_c[HS_2SEC_MATURE_TIME];
3065  0911 180c00040002      	movb	_hs_flt_dtc_c+4,_hs_monitor_2sec_led_counter_c+2
3066  0917                   L7521:
3067                         ; 2508 						if(hs_rem_seat_req_c[REAR_RIGHT] != SET_OFF_REQUEST) 
3069  0917 f60003            	ldab	_hs_rem_seat_req_c+3
3070  091a 271c              	beq	L7721
3071                         ; 2512 							hs_monitor_2sec_led_counter_c[ REAR_RIGHT ] = /*mature_2sec_time_c*/hs_flt_dtc_c[HS_2SEC_MATURE_TIME];
3073  091c f60004            	ldab	_hs_flt_dtc_c+4
3074  091f 7b0003            	stab	_hs_monitor_2sec_led_counter_c+3
3076  0922 0a                	rtc	
3077                         ; 2518 						relay_B_2000ms_fault_counter_c++;
3079  0923                   L5421:
3080                         ; 2531 				if (relay_B_2000ms_fault_counter_c >= /*mature_2sec_time_c*/hs_flt_dtc_c[HS_2SEC_MATURE_TIME] ) {
3082  0923 f60013            	ldab	_relay_B_2000ms_fault_counter_c
3083  0926 f10004            	cmpb	_hs_flt_dtc_c+4
3084  0929 2506              	blo	L1721
3085                         ; 2534 					Rear_Seats_no_power_final_b = TRUE;
3087  092b 1c001c02          	bset	_relay_control2_c,2
3088                         ; 2537 					relay_B_2000ms_fault_counter_c = 0;
3091  092f 2004              	bra	L3421
3092  0931                   L1721:
3093                         ; 2542 					relay_B_2000ms_fault_counter_c++;
3095  0931 720013            	inc	_relay_B_2000ms_fault_counter_c
3097  0934 0a                	rtc	
3098  0935                   L3421:
3099                         ; 2551 			relay_B_2000ms_fault_counter_c = 0;
3101  0935 790013            	clr	_relay_B_2000ms_fault_counter_c
3102  0938                   L7721:
3103                         ; 2561 } //End of Function
3106  0938 0a                	rtc	
3135                         ; 2581 void rlLF_MatureTime_For_RelayC_InternalFault(void)
3135                         ; 2582 {
3136                         	switch	.ftext
3137  0939                   f_rlLF_MatureTime_For_RelayC_InternalFault:
3141                         ; 2584 	if ((relay_C_internal_Fault_b == FALSE)&&
3141                         ; 2585 	    (stW_Flt_status_w != STW_MTRD_BATT_MASK) &&
3141                         ; 2586 	    (stW_Flt_status_w != STW_MTRD_GND_MASK))
3143  0939 1e001f012e        	brset	_relay_fault_status2_c,1,L1131
3145  093e fc0000            	ldd	_stW_Flt_status_w
3146  0941 8c0200            	cpd	#512
3147  0944 2726              	beq	L1131
3149  0946 8c0100            	cpd	#256
3150  0949 2721              	beq	L1131
3151                         ; 2589 		if ( (relay_C_enabled_b == TRUE) && 
3151                         ; 2590 		     (relay_C_phase2_ok_b == FALSE) && 
3151                         ; 2591 		     (relay_C_stuck_LOW_b == TRUE) )
3153  094b 1f0020021f        	brclr	_relay_status2_c,2,L3231
3155  0950 1e0020101a        	brset	_relay_status2_c,16,L3231
3157  0955 1f001f0415        	brclr	_relay_fault_status2_c,4,L3231
3158                         ; 2594 			if (relay_C_2000ms_fault_counter_c >=  /*mature_2sec_time_c*/hs_flt_dtc_c[HS_2SEC_MATURE_TIME])
3160  095a f60012            	ldab	_relay_C_2000ms_fault_counter_c
3161  095d f10004            	cmpb	_hs_flt_dtc_c+4
3162  0960 2506              	blo	L5131
3163                         ; 2597 				relay_C_internal_Fault_b = TRUE;
3165  0962 1c001f01          	bset	_relay_fault_status2_c,1
3166                         ; 2600 				relay_C_2000ms_fault_counter_c = 0;
3169  0966 2004              	bra	L1131
3170  0968                   L5131:
3171                         ; 2605 				relay_C_2000ms_fault_counter_c++;
3173  0968 720012            	inc	_relay_C_2000ms_fault_counter_c
3175  096b 0a                	rtc	
3176  096c                   L1131:
3177                         ; 2616 		relay_C_2000ms_fault_counter_c = 0;
3179  096c 790012            	clr	_relay_C_2000ms_fault_counter_c
3180  096f                   L3231:
3181                         ; 2618 } //End of function
3184  096f 0a                	rtc	
3211                         ; 2635 @far void rlF_RelayDTC(void)
3211                         ; 2636 {
3212                         	switch	.ftext
3213  0970                   f_rlF_RelayDTC:
3217                         ; 2642 	if(relay_A_internal_Fault_b == TRUE){
3219  0970 1f001d1023        	brclr	_relay_control_c,16,L5331
3220                         ; 2645 		if(relay_A_stuck_HI_b == TRUE){
3222  0975 1f001e0106        	brclr	_relay_fault_status_c,1,L7331
3223                         ; 2647 			rlLF_Set_Relay_Err(RELAY_A_STUCK_HI_MASK, INT_FLT_SET);				
3225  097a 87                	clra	
3226  097b c7                	clrb	
3227  097c 3b                	pshd	
3228  097d 52                	incb	
3231  097e 2040              	bra	LC014
3232  0980                   L7331:
3233                         ; 2648 		}else if(relay_A_stuck_LOW_b == TRUE ){
3235  0980 1f001e0207        	brclr	_relay_fault_status_c,2,L3431
3236                         ; 2650 			rlLF_Set_Relay_Err(RELAY_A_STUCK_LO_MASK, INT_FLT_SET);				
3238  0985 87                	clra	
3239  0986 c7                	clrb	
3240  0987 3b                	pshd	
3241  0988 c602              	ldab	#2
3244  098a 2034              	bra	LC014
3245  098c                   L3431:
3246                         ; 2651 		}else if(relay_F_FET_fault_b == TRUE){
3248  098c 1f001e1035        	brclr	_relay_fault_status_c,16,L1531
3249                         ; 2653 			rlLF_Set_Relay_Err(F_HS_DRIVER_FLT_MASK, INT_FLT_SET);
3251  0991 87                	clra	
3252  0992 c7                	clrb	
3253  0993 3b                	pshd	
3254  0994 c640              	ldab	#64
3256  0996 2028              	bra	LC014
3257  0998                   L5331:
3258                         ; 2658 		RelayA_Stuck_HI_b = FALSE;
3260  0998 1d000001          	bclr	_int_flt_byte1,1
3261                         ; 2659 		rlLF_Set_Relay_Err(CLR_RELAYA_STUCK_HI, INT_FLT_CLR);
3263  099c cc0001            	ldd	#1
3264  099f 3b                	pshd	
3265  09a0 c6fe              	ldab	#254
3266  09a2 4a0a2424          	call	f_rlLF_Set_Relay_Err
3268                         ; 2661 		RelayA_Stuck_LO_b = FALSE;
3270  09a6 1d000002          	bclr	_int_flt_byte1,2
3271                         ; 2662 		rlLF_Set_Relay_Err(CLR_RELAYA_STUCK_LO, INT_FLT_CLR);
3273  09aa cc0001            	ldd	#1
3274  09ad 6c80              	std	0,s
3275  09af c6fd              	ldab	#253
3276  09b1 4a0a2424          	call	f_rlLF_Set_Relay_Err
3278                         ; 2664 		relay_F_FET_fault_b = FALSE; 		
3280  09b5 1d001e10          	bclr	_relay_fault_status_c,16
3281                         ; 2665 		rlLF_Set_Relay_Err(CLR_F_DRIVER_FAULT, INT_FLT_CLR);
3283  09b9 cc0001            	ldd	#1
3284  09bc 6c80              	std	0,s
3285  09be c6bf              	ldab	#191
3287  09c0                   LC014:
3288  09c0 4a0a2424          	call	f_rlLF_Set_Relay_Err
3289  09c4 1b82              	leas	2,s
3290  09c6                   L1531:
3291                         ; 2670 	if ((CSWM_config_c & CSWM_4HS_VARIANT_MASK_K) == CSWM_4HS_VARIANT_MASK_K)
3293  09c6 1e00000f01        	brset	_CSWM_config_c,15,LC013
3295  09cb 0a                	rtc	
3296  09cc                   LC013:
3297                         ; 2673 		if(relay_B_internal_Fault_b == TRUE){
3299  09cc 1f001d2024        	brclr	_relay_control_c,32,L5531
3300                         ; 2676 			if(relay_B_stuck_HI_b == TRUE){
3302  09d1 1f001e0407        	brclr	_relay_fault_status_c,4,L7531
3303                         ; 2678 				rlLF_Set_Relay_Err(RELAY_B_STUCK_HI_MASK, INT_FLT_SET);				
3305  09d6 87                	clra	
3306  09d7 c7                	clrb	
3307  09d8 3b                	pshd	
3308  09d9 c604              	ldab	#4
3311  09db 2040              	bra	LC015
3312  09dd                   L7531:
3313                         ; 2679 			}else if(relay_B_stuck_LOW_b == TRUE ){
3315  09dd 1f001e0807        	brclr	_relay_fault_status_c,8,L3631
3316                         ; 2681 				rlLF_Set_Relay_Err(RELAY_B_STUCK_LO_MASK, INT_FLT_SET);				
3318  09e2 87                	clra	
3319  09e3 c7                	clrb	
3320  09e4 3b                	pshd	
3321  09e5 c608              	ldab	#8
3324  09e7 2034              	bra	LC015
3325  09e9                   L3631:
3326                         ; 2682 			}else if(relay_R_FET_fault_b == TRUE){
3328  09e9 1f001e2035        	brclr	_relay_fault_status_c,32,L3731
3329                         ; 2684 				rlLF_Set_Relay_Err(R_HS_DRIVER_FLT_MASK, INT_FLT_SET);
3331  09ee 87                	clra	
3332  09ef c7                	clrb	
3333  09f0 3b                	pshd	
3334  09f1 c680              	ldab	#128
3336  09f3 2028              	bra	LC015
3337  09f5                   L5531:
3338                         ; 2689 			RelayB_Stuck_HI_b = FALSE;
3340  09f5 1d000004          	bclr	_int_flt_byte1,4
3341                         ; 2690 			rlLF_Set_Relay_Err(CLR_RELAYB_STUCK_HI, INT_FLT_CLR);
3343  09f9 cc0001            	ldd	#1
3344  09fc 3b                	pshd	
3345  09fd c6fb              	ldab	#251
3346  09ff 4a0a2424          	call	f_rlLF_Set_Relay_Err
3348                         ; 2692 			RelayB_Stuck_LO_b = FALSE;
3350  0a03 1d000008          	bclr	_int_flt_byte1,8
3351                         ; 2693 			rlLF_Set_Relay_Err(CLR_RELAYB_STUCK_LO, INT_FLT_CLR);
3353  0a07 cc0001            	ldd	#1
3354  0a0a 6c80              	std	0,s
3355  0a0c c6f7              	ldab	#247
3356  0a0e 4a0a2424          	call	f_rlLF_Set_Relay_Err
3358                         ; 2695 			relay_R_FET_fault_b = FALSE;
3360  0a12 1d001e20          	bclr	_relay_fault_status_c,32
3361                         ; 2696 			rlLF_Set_Relay_Err(CLR_R_DRIVER_FAULT, INT_FLT_CLR);
3363  0a16 cc0001            	ldd	#1
3364  0a19 6c80              	std	0,s
3365  0a1b c67f              	ldab	#127
3367  0a1d                   LC015:
3368  0a1d 4a0a2424          	call	f_rlLF_Set_Relay_Err
3369  0a21 1b82              	leas	2,s
3370  0a23                   L3731:
3371                         ; 2729 } //End of function
3374  0a23 0a                	rtc	
3453                         ; 2746 void rlLF_Set_Relay_Err(unsigned char relay_bit_mask, Int_Flt_Op_Type_e Operation)
3453                         ; 2747 {
3454                         	switch	.ftext
3455  0a24                   f_rlLF_Set_Relay_Err:
3457  0a24 3b                	pshd	
3458  0a25 3b                	pshd	
3459       00000002          OFST:	set	2
3462                         ; 2748 	u_8Bit relay_bit = relay_bit_mask;        // bit mask (to set or clear the fault)
3464  0a26 6b80              	stab	OFST-2,s
3465                         ; 2749 	u_8Bit relay_set_mask = 0;                // set bit mask
3467                         ; 2752 	EE_BlockRead(EE_INTERNAL_FLT_BYTE1, (u_8Bit*)&intFlt_bytes[ONE]);
3469  0a28 cc0001            	ldd	#_intFlt_bytes+1
3470  0a2b 3b                	pshd	
3471  0a2c cc0019            	ldd	#25
3472  0a2f 4a000000          	call	f_EE_BlockRead
3474  0a33 1b82              	leas	2,s
3475                         ; 2755 	if (Operation == INT_FLT_SET) {
3477  0a35 e688              	ldab	OFST+6,s
3478  0a37 261f              	bne	L1341
3479                         ; 2758 		if ( (intFlt_bytes[ONE] & relay_bit_mask) == relay_bit_mask) {			
3481  0a39 f60001            	ldab	_intFlt_bytes+1
3482  0a3c e483              	andb	OFST+1,s
3483  0a3e e183              	cmpb	OFST+1,s
3484  0a40 273a              	beq	L7341
3486                         ; 2763 			IntFlt_F_Update(ONE, relay_bit, INT_FLT_SET);
3488  0a42 87                	clra	
3489  0a43 c7                	clrb	
3490  0a44 3b                	pshd	
3491  0a45 e682              	ldab	OFST+0,s
3492  0a47 3b                	pshd	
3493  0a48 cc0001            	ldd	#1
3494  0a4b 4a000000          	call	f_IntFlt_F_Update
3496  0a4f 1b84              	leas	4,s
3497                         ; 2766 			int_flt_byte1.cb |= relay_bit;
3499  0a51 e680              	ldab	OFST-2,s
3500  0a53 fa0000            	orab	_int_flt_byte1
3501  0a56 2021              	bra	LC016
3502  0a58                   L1341:
3503                         ; 2771 		relay_set_mask = (u_8Bit)(~relay_bit);
3505  0a58 e680              	ldab	OFST-2,s
3506  0a5a 51                	comb	
3507  0a5b 6b81              	stab	OFST-1,s
3508                         ; 2774 		if ( (intFlt_bytes[ONE] & relay_set_mask) == relay_set_mask) {
3510  0a5d f40001            	andb	_intFlt_bytes+1
3511  0a60 e181              	cmpb	OFST-1,s
3512  0a62 2618              	bne	L7341
3513                         ; 2777 			IntFlt_F_Update(ONE, relay_bit, INT_FLT_CLR);
3515  0a64 cc0001            	ldd	#1
3516  0a67 3b                	pshd	
3517  0a68 e682              	ldab	OFST+0,s
3518  0a6a 3b                	pshd	
3519  0a6b cc0001            	ldd	#1
3520  0a6e 4a000000          	call	f_IntFlt_F_Update
3522  0a72 1b84              	leas	4,s
3523                         ; 2780 			int_flt_byte1.cb &= relay_bit;
3525  0a74 e680              	ldab	OFST-2,s
3526  0a76 f40000            	andb	_int_flt_byte1
3527  0a79                   LC016:
3528  0a79 7b0000            	stab	_int_flt_byte1
3530  0a7c                   L7341:
3531                         ; 2786 } //End of Function
3534  0a7c 1b84              	leas	4,s
3535  0a7e 0a                	rtc	
3715                         	xdef	f_rlLF_Set_Relay_Err
3716                         	xdef	f_rlLF_MatureTime_For_RelayC_InternalFault
3717                         	xdef	f_rlLF_MatureTime_For_RelayB_InternalFault
3718                         	xdef	f_rlLF_MatureTime_For_RelayA_InternalFault
3719                         	xdef	f_rlLF_RelayCPortON_FETOFF_Phase2_Detection
3720                         	xdef	f_rlLF_RelayBPortON_FETOFF_Phase2_Detection
3721                         	xdef	f_rlLF_RelayAPortON_FETOFF_Phase2_Detection
3722                         	xdef	f_rlLF_RelayCPortOFF_FETOFF_Phase1_Detection
3723                         	xdef	f_rlLF_RelayBPortOFF_FETOFF_Phase1_Detection
3724                         	xdef	f_rlLF_RelayAPortOFF_FETOFF_Phase1_Detection
3725                         	xdef	f_rlLF_NoPower_To_RearSeats_Detection
3726                         	xdef	f_rlLF_TurnOFF_BaseRelay
3727                         	xdef	f_rlLF_Control_RelayC_Port
3728                         	xdef	f_rlLF_Control_RelayB_Port
3729                         	xdef	f_rlLF_Control_RelayA_Port
3730                         	xdef	f_rlLF_Control_BaseRelay
3731                         	xdef	f_rlLF_BaseRelay_ON_With100PWM_For_RelayC
3732                         	xdef	f_rlLF_BaseRelay_ON_With100PWM_For_RelayB
3733                         	xdef	f_rlLF_BaseRelay_ON_With100PWM_For_RelayA
3734                         	xdef	f_rlLF_BaseRelay_For_Steering_OFF
3735                         	xdef	f_rlLF_BaseRelay_For_RearSeats_OFF
3736                         	xdef	f_rlLF_BaseRelay_For_FrontSeats_OFF
3737                         	switch	.bss
3738  0000                   _relay_stw_LO_Side_Off_cntr_c:
3739  0000 00                	ds.b	1
3740                         	xdef	_relay_stw_LO_Side_Off_cntr_c
3741  0001                   _relay_C_STB_delay_c:
3742  0001 00                	ds.b	1
3743                         	xdef	_relay_C_STB_delay_c
3744  0002                   _rear_seat_no_power_delay_c:
3745  0002 00                	ds.b	1
3746                         	xdef	_rear_seat_no_power_delay_c
3747  0003                   _relay_B_stuck_HI_delay_c:
3748  0003 00                	ds.b	1
3749                         	xdef	_relay_B_stuck_HI_delay_c
3750  0004                   _relay_A_stuck_HI_delay_c:
3751  0004 00                	ds.b	1
3752                         	xdef	_relay_A_stuck_HI_delay_c
3753  0005                   _relay_fault_detection_type_c:
3754  0005 00                	ds.b	1
3755                         	xdef	_relay_fault_detection_type_c
3756  0006                   _relay_delay_time_for_steering_c:
3757  0006 00                	ds.b	1
3758                         	xdef	_relay_delay_time_for_steering_c
3759  0007                   _relay_delay_time_phase2_c:
3760  0007 00                	ds.b	1
3761                         	xdef	_relay_delay_time_phase2_c
3762  0008                   _relay_delay_time_phase1_c:
3763  0008 00                	ds.b	1
3764                         	xdef	_relay_delay_time_phase1_c
3765  0009                   _relay_autosar_pwm_w:
3766  0009 0000              	ds.b	2
3767                         	xdef	_relay_autosar_pwm_w
3768  000b                   _relay_base_prev_pwm_value_c:
3769  000b 00                	ds.b	1
3770                         	xdef	_relay_base_prev_pwm_value_c
3771  000c                   _relay_base_pwm_value_c:
3772  000c 00                	ds.b	1
3773                         	xdef	_relay_base_pwm_value_c
3774  000d                   _relay_UBATT_current_value_c:
3775  000d 00                	ds.b	1
3776                         	xdef	_relay_UBATT_current_value_c
3777  000e                   _relay_C_Test_Voltage_Pin_status_c:
3778  000e 00                	ds.b	1
3779                         	xdef	_relay_C_Test_Voltage_Pin_status_c
3780  000f                   _relay_B_power_status_c:
3781  000f 00                	ds.b	1
3782                         	xdef	_relay_B_power_status_c
3783  0010                   _relay_B_status_c:
3784  0010 00                	ds.b	1
3785                         	xdef	_relay_B_status_c
3786  0011                   _relay_A_status_c:
3787  0011 00                	ds.b	1
3788                         	xdef	_relay_A_status_c
3789  0012                   _relay_C_2000ms_fault_counter_c:
3790  0012 00                	ds.b	1
3791                         	xdef	_relay_C_2000ms_fault_counter_c
3792  0013                   _relay_B_2000ms_fault_counter_c:
3793  0013 00                	ds.b	1
3794                         	xdef	_relay_B_2000ms_fault_counter_c
3795  0014                   _relay_A_2000ms_fault_counter_c:
3796  0014 00                	ds.b	1
3797                         	xdef	_relay_A_2000ms_fault_counter_c
3798  0015                   _relay_1sec_100pwm_counter_c:
3799  0015 00                	ds.b	1
3800                         	xdef	_relay_1sec_100pwm_counter_c
3801                         	xref	f_Pwm_SetDutyCycle
3802                         	xref	_Dio_PortRead_Ptr
3803                         	xref	_Dio_PortWrite_Ptr
3804                         	xref	f_EE_BlockRead
3805                         	xref	_diag_io_lock3_flags_c
3806                         	xref	f_IntFlt_F_Update
3807                         	xref	_intFlt_bytes
3808                         	xref	_int_flt_byte1
3809                         	xref	_stW_flt_calibs_c
3810                         	xref	_stW_Flt_status_w
3811                         	xref	_stW_Vsense_LS_c
3812                         	xref	_stW_Vsense_HS_c
3813                         	xref	_stW_Flag1_c
3814                         	xref	f_BattVF_Get_Crt_Uint
3815                         	xref	_Vs_flt_cal_prms
3816                         	xref	_vs_shrtTo_batt_mntr_c
3817                         	xref	_vs_Vsense_c
3818                         	xref	_vs_Flag1_c
3819                         	xdef	f_rlF_RelayDTC
3820                         	xdef	f_rlF_RelayDiagnostics
3821                         	xdef	f_rlF_RelayManager
3822                         	xdef	f_rlF_InitRelay
3823  0016                   _relay_calibs_c:
3824  0016 000000000000      	ds.b	6
3825                         	xdef	_relay_calibs_c
3826  001c                   _relay_control2_c:
3827  001c 00                	ds.b	1
3828                         	xdef	_relay_control2_c
3829  001d                   _relay_control_c:
3830  001d 00                	ds.b	1
3831                         	xdef	_relay_control_c
3832  001e                   _relay_fault_status_c:
3833  001e 00                	ds.b	1
3834                         	xdef	_relay_fault_status_c
3835  001f                   _relay_fault_status2_c:
3836  001f 00                	ds.b	1
3837                         	xdef	_relay_fault_status2_c
3838  0020                   _relay_status2_c:
3839  0020 00                	ds.b	1
3840                         	xdef	_relay_status2_c
3841  0021                   _relay_status_c:
3842  0021 00                	ds.b	1
3843                         	xdef	_relay_status_c
3844                         	xref	_hs_flt_dtc_c
3845                         	xref	_CSWM_config_c
3846                         	xref	_hs_Vsense_c
3847                         	xref	_hs_monitor_2sec_led_counter_c
3848                         	xref	_hs_rem_seat_req_c
3849                         	xref	_hs_status_flags_c
3850                         	xref	_mainF_Calc_PWM
3851                         	xref	_main_flag2_c
3852                         	xref	_main_CSWM_Status_c
3853                         	xref	_canio_RX_IGN_STATUS_c
3874                         	end
