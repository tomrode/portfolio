   1                         ; C Compiler for S12XE [COSMIC Software].
   2                         ; Generator V4.7.8 - 27 Jul 2007
   3                         ; Optimizer V4.6.13 - 27 Jul 2007
  59                         ; 282 @far void stWF_Clear(void)
  59                         ; 283 {
  60                         .ftext:	section	.text
  61  0000                   f_stWF_Clear:
  65                         ; 285 	if ( (canio_RX_IGN_STATUS_c != IGN_RUN) && (canio_RX_IGN_STATUS_c != IGN_START) )
  67  0000 f60000            	ldab	_canio_RX_IGN_STATUS_c
  68  0003 c104              	cmpb	#4
  69  0005 2739              	beq	L32
  71  0007 c105              	cmpb	#5
  72  0009 2735              	beq	L32
  73                         ; 287 		stW_lst_sw_rq_c        = 0;            // clear the last switch request
  75  000b c7                	clrb	
  76  000c 7b0028            	stab	_stW_lst_sw_rq_c
  77                         ; 288 		stW_Flt_Mtr_cnt        = 0;            // clear fault mature time
  79  000f 87                	clra	
  80  0010 7c0020            	std	_stW_Flt_Mtr_cnt
  81                         ; 289 		stW_Isense_dly_cnt_c   = 0;            // clear Isense analysis delay
  83  0013 7a0016            	staa	_stW_Isense_dly_cnt_c
  84                         ; 290 		stW_TimeOut_Cycles_w   = 0;            // clear timeout cycles
  86  0016 7c0022            	std	_stW_TimeOut_Cycles_w
  87  0019 7c00f2            	std	_stW_Flt_status_w
  88                         ; 292 		stW_Flt_status_w    = STW_NO_FLT_MASK; //no stw faults
  90                         ; 294 		stW_status_c           = GOOD;         //Stw status is good
  92  001c 52                	incb	
  93  001d 7b0029            	stab	_stW_status_c
  94                         ; 296 		stW_diag_cntrl_act_b   = FALSE;        // diagnostics control is inactive
  96                         ; 297 		stW_not_vld_swtch_rq_b = FALSE;        // clear the invalid switch request
  98  0020 1d00fa12          	bclr	_stW_Flag1_c,18
  99                         ; 299 		stW_RemSt_auto_b       = FALSE;        // Clear remote start automatic activation bit		
 101                         ; 301 		stW_Open_or_STB_b      = FALSE;        // Clear the bit that is used to determine Open or STB fault on HS driver
 103  0024 1d001003          	bclr	_stW_Flag2_c,3
 104                         ; 303 		diag_io_st_whl_rq_c    = 0;            // diagnostic stW request is off
 106  0028 790000            	clr	_diag_io_st_whl_rq_c
 107                         ; 304 		diag_io_st_whl_state_c = 0;            // diagnostic stW LED state is off
 109  002b 790000            	clr	_diag_io_st_whl_state_c
 110                         ; 306 		stW_Vsense_HS_c        = 0;            // clear steering wheel voltage reading high side
 112  002e 7900f9            	clr	_stW_Vsense_HS_c
 113                         ; 307 		stW_Vsense_LS_c        = 0;            // Clear steering wheel voltage reading low side
 115  0031 7900f8            	clr	_stW_Vsense_LS_c
 116                         ; 308 		stW_Isense_c           = 0;            // Clear steering wheel current reading low side
 118  0034 7900f7            	clr	_stW_Isense_c
 119                         ; 310 		stW_Ign_stat           = 0;            // Clear steering wheel ignition (battery power) status
 121  0037 790017            	clr	_stW_Ign_stat
 122                         ; 312 		stW_lst_main_state_c = NORMAL;         // Last main state is defaulted to normal
 124  003a 79000f            	clr	_stW_lst_main_state_c
 125                         ; 314 		stW_chck_Open_or_STB_dly_c = 0;		   // Clear Open or STB detection delay counter 
 127  003d 790015            	clr	_stW_chck_Open_or_STB_dly_c
 129  0040                   L32:
 130                         ; 321 	stW_level_c            = 0;        // current wheel level is WL0 
 132  0040 87                	clra	
 133  0041 7a002a            	staa	_stW_level_c
 134                         ; 322 	stW_onTime_w           = 0;        // clear Amount of time that stW has been On
 136  0044 c7                	clrb	
 137  0045 7c0024            	std	_stW_onTime_w
 138  0048 7c001c            	std	_stW_pwm_w
 139                         ; 323 	stW_pwm_w              = 0;        // PWM is 0
 141                         ; 324 	stW_LED_stat_c         = 0;        // LED status is off
 143  004b 7b0027            	stab	_stW_LED_stat_c
 144                         ; 325 	stW_pwm_state_c        = 0;        // pwm state is PWM_LOW
 146  004e 7a0018            	staa	_stW_pwm_state_c
 147                         ; 330 	stW_sw_rq_b            = FALSE;    // software request off
 149                         ; 331 	stW_LED_off_b          = FALSE;    // clear LED off flag
 151                         ; 332 	stW_swtch_rq_prps_b    = FALSE;    // switch request purpose is to turn off
 153                         ; 334 	stW_clr_ontime_b       = FALSE;    // reset ontime to zero flag
 155  0051 1d00fa6c          	bclr	_stW_Flag1_c,108
 156                         ; 336 	stW_Isense_limit_b         = FALSE;        // Clear the 8A limit set flag.
 158  0055 1d001004          	bclr	_stW_Flag2_c,4
 159                         ; 339 	stW_LED_Dsply_Tm_cnt   = 0;        // clear LED display time
 161  0059 7c001e            	std	_stW_LED_Dsply_Tm_cnt
 162  005c 7c0003            	std	_stW_Isense_timer_w
 163                         ; 341 	stW_Isense_timer_w         = 0;        //8A current limit timer.
 165                         ; 343 }
 168  005f 0a                	rtc	
 208                         ; 360 @far void stWF_Init(void)
 208                         ; 361 {
 209                         	switch	.ftext
 210  0060                   f_stWF_Init:
 212  0060 3b                	pshd	
 213       00000002          OFST:	set	2
 216                         ; 363 	EE_BlockRead(EE_PROXI_CFG, (UINT8*)&ru_eep_PROXI_Cfg);
 218  0061 cc0000            	ldd	#_ru_eep_PROXI_Cfg
 219  0064 3b                	pshd	
 220  0065 cc001d            	ldd	#29
 221  0068 4a000000          	call	f_EE_BlockRead
 223                         ; 365 	EE_BlockRead(EE_WHEEL_CAL_DATA, (u_8Bit*)&stW_prms); 
 225  006c cc002b            	ldd	#_stW_prms
 226  006f 6c80              	std	0,s
 227  0071 cc000c            	ldd	#12
 228  0074 4a000000          	call	f_EE_BlockRead
 230  0078 1b82              	leas	2,s
 231                         ; 367 	if(ru_eep_PROXI_Cfg.s.Vehicle_Line_Configuration == PROXI_VEH_LINE_PF)
 233                         ; 369 		stW_veh_offset_c = STW_VEH_PF;
 236                         ; 374 		stW_veh_offset_c = STW_VEH_PF;
 238  007a 7900ef            	clr	_stW_veh_offset_c
 239                         ; 377 	if (ru_eep_PROXI_Cfg.s.Wheel_Material == PROXI_WHEEL_TYPE_LEATHER) 
 241  007d f60009            	ldab	_ru_eep_PROXI_Cfg+9
 242  0080 c4c0              	andb	#192
 243  0082 c180              	cmpb	#128
 244  0084 2605              	bne	L14
 245                         ; 380 		stW_cfg_c = STW_LEATHER; 
 247  0086 7900f0            	clr	_stW_cfg_c
 249  0089 2005              	bra	L34
 250  008b                   L14:
 251                         ; 385 		stW_cfg_c = STW_WOOD; 
 253  008b c601              	ldab	#1
 254  008d 7b00f0            	stab	_stW_cfg_c
 255  0090                   L34:
 256                         ; 402 	EE_BlockRead(EE_WHEEL_FAULT_THRESHOLDS, (u_8Bit*)&stW_flt_calibs_c);   
 258  0090 cc00e9            	ldd	#_stW_flt_calibs_c
 259  0093 3b                	pshd	
 260  0094 cc000d            	ldd	#13
 261  0097 4a000000          	call	f_EE_BlockRead
 263  009b 1b82              	leas	2,s
 264                         ; 405 	stW_Flt_status_w = STW_NO_FLT_MASK;    //no stw faults
 266  009d 187900f2          	clrw	_stW_Flt_status_w
 267                         ; 408 	stW_status_c = GOOD;                   //Stw status is good
 269  00a1 c601              	ldab	#1
 270  00a3 7b0029            	stab	_stW_status_c
 271                         ; 411 	stW_Flt_Mtr_Tm = (MAIN_FLT_MTR_TM * STW_MTR_FLT_TM_MDLTN);       // Fault mature time (2*50=100) in 20ms tm
 273  00a6 cc0064            	ldd	#100
 274  00a9 7c0013            	std	_stW_Flt_Mtr_Tm
 275                         ; 412 	stW_LED_Dsply_Tm = (MAIN_LED_DSPLY_TM * STW_LED_DSPLY_TM_MDLTN); // LED Display time (2*100=200) in 10ms tm
 277  00ac c6c8              	ldab	#200
 278  00ae 7c0011            	std	_stW_LED_Dsply_Tm
 279                         ; 427 	stW_WL1TempChck_lmt_w = ((stW_prms.cals[stW_veh_offset_c+stW_cfg_c].WL1TempChck_Time * STW_TM_SLICE_MLTPLR) + (stW_prms.cals[stW_veh_offset_c+stW_cfg_c].WL1TempChck_Time>>1));
 281  00b1 f600f0            	ldab	_stW_cfg_c
 282  00b4 fb00ef            	addb	_stW_veh_offset_c
 283  00b7 8900              	adca	#0
 284  00b9 cd0013            	ldy	#19
 285  00bc 13                	emul	
 286  00bd b746              	tfr	d,y
 287  00bf e6ea003a          	ldab	_stW_prms+15,y
 288  00c3 87                	clra	
 289  00c4 54                	lsrb	
 290  00c5 6c80              	std	OFST-2,s
 291  00c7 f600f0            	ldab	_stW_cfg_c
 292  00ca fb00ef            	addb	_stW_veh_offset_c
 293  00cd 45                	rola	
 294  00ce cd0013            	ldy	#19
 295  00d1 13                	emul	
 296  00d2 b746              	tfr	d,y
 297  00d4 e6ea003a          	ldab	_stW_prms+15,y
 298  00d8 86bb              	ldaa	#187
 299  00da 12                	mul	
 300  00db e380              	addd	OFST-2,s
 301  00dd 7c000d            	std	_stW_WL1TempChck_lmt_w
 302                         ; 430 	stW_WL2TempChck_lmt_w = ((stW_prms.cals[stW_veh_offset_c+stW_cfg_c].WL2TempChck_Time * STW_TM_SLICE_MLTPLR) + (stW_prms.cals[stW_veh_offset_c+stW_cfg_c].WL2TempChck_Time>>1));
 304  00e0 e6ea003b          	ldab	_stW_prms+16,y
 305  00e4 87                	clra	
 306  00e5 54                	lsrb	
 307  00e6 6c80              	std	OFST-2,s
 308  00e8 f600f0            	ldab	_stW_cfg_c
 309  00eb fb00ef            	addb	_stW_veh_offset_c
 310  00ee 45                	rola	
 311  00ef cd0013            	ldy	#19
 312  00f2 13                	emul	
 313  00f3 b746              	tfr	d,y
 314  00f5 e6ea003b          	ldab	_stW_prms+16,y
 315  00f9 86bb              	ldaa	#187
 316  00fb 12                	mul	
 317  00fc e380              	addd	OFST-2,s
 318  00fe 7c000b            	std	_stW_WL2TempChck_lmt_w
 319                         ; 433 	stW_WL3TempChck_lmt_w = ((stW_prms.cals[stW_veh_offset_c+stW_cfg_c].WL3TempChck_Time * STW_TM_SLICE_MLTPLR) + (stW_prms.cals[stW_veh_offset_c+stW_cfg_c].WL3TempChck_Time>>1));
 321  0101 e6ea003c          	ldab	_stW_prms+17,y
 322  0105 87                	clra	
 323  0106 54                	lsrb	
 324  0107 6c80              	std	OFST-2,s
 325  0109 f600f0            	ldab	_stW_cfg_c
 326  010c fb00ef            	addb	_stW_veh_offset_c
 327  010f 45                	rola	
 328  0110 cd0013            	ldy	#19
 329  0113 13                	emul	
 330  0114 b746              	tfr	d,y
 331  0116 e6ea003c          	ldab	_stW_prms+17,y
 332  011a 86bb              	ldaa	#187
 333  011c 12                	mul	
 334  011d e380              	addd	OFST-2,s
 335  011f 7c0009            	std	_stW_WL3TempChck_lmt_w
 336                         ; 436 	stW_WL4TempChck_lmt_w = ((stW_prms.cals[stW_veh_offset_c+stW_cfg_c].WL4TempChck_Time * STW_TM_SLICE_MLTPLR) + (stW_prms.cals[stW_veh_offset_c+stW_cfg_c].WL4TempChck_Time>>1));
 338  0122 e6ea003d          	ldab	_stW_prms+18,y
 339  0126 87                	clra	
 340  0127 54                	lsrb	
 341  0128 6c80              	std	OFST-2,s
 342  012a f600f0            	ldab	_stW_cfg_c
 343  012d fb00ef            	addb	_stW_veh_offset_c
 344  0130 45                	rola	
 345  0131 cd0013            	ldy	#19
 346  0134 13                	emul	
 347  0135 b746              	tfr	d,y
 348  0137 e6ea003d          	ldab	_stW_prms+18,y
 349  013b 86bb              	ldaa	#187
 350  013d 12                	mul	
 351  013e e380              	addd	OFST-2,s
 352  0140 7c0007            	std	_stW_WL4TempChck_lmt_w
 353                         ; 441 	stW_ontimeLmt_w = ( (stW_flt_calibs_c[STW_SNSR_FLT_RANGE_ONTIME] * STW_TM_SLICE_MLTPLR) + (stW_flt_calibs_c[STW_SNSR_FLT_RANGE_ONTIME]>>1) );
 355  0143 f600ee            	ldab	_stW_flt_calibs_c+5
 356  0146 87                	clra	
 357  0147 54                	lsrb	
 358  0148 6c80              	std	OFST-2,s
 359  014a f600ee            	ldab	_stW_flt_calibs_c+5
 360  014d 86bb              	ldaa	#187
 361  014f 12                	mul	
 362  0150 e380              	addd	OFST-2,s
 363  0152 7c0005            	std	_stW_ontimeLmt_w
 364                         ; 444 	stW_lst_main_state_c = NORMAL;
 366  0155 79000f            	clr	_stW_lst_main_state_c
 367                         ; 450 	stW_temperature_c = STW_TEMP_22C;	// steering wheel temperature is 22.5 C	
 369  0158 c67d              	ldab	#125
 370  015a 7b00f1            	stab	_stW_temperature_c
 371                         ; 451 	stW_tmprtr_sensor_flt_c = (u_8Bit) STW_TEMPERATURE_SENSOR_OK; // steering wheel sensor fault status is o.k.
 373  015d 790019            	clr	_stW_tmprtr_sensor_flt_c
 374                         ; 452 }
 377  0160 31                	puly	
 378  0161 0a                	rtc	
 404                         ; 475 @far void stWF_AD_Cnvrn(void)
 404                         ; 476 {
 405                         	switch	.ftext
 406  0162                   f_stWF_AD_Cnvrn:
 410                         ; 478 	ADF_AtoD_Cnvrsn(stW_AD_prms.slctd_adCH_c);
 412  0162 f60001            	ldab	_stW_AD_prms+1
 413  0165 87                	clra	
 414  0166 160000            	jsr	_ADF_AtoD_Cnvrsn
 416                         ; 481 	ADF_Str_UnFlt_Mux_Rslt(stW_AD_prms.slctd_adCH_c, stW_AD_prms.slctd_muxCH_c);
 418  0169 f60000            	ldab	_stW_AD_prms
 419  016c 87                	clra	
 420  016d 3b                	pshd	
 421  016e f60001            	ldab	_stW_AD_prms+1
 422  0171 160000            	jsr	_ADF_Str_UnFlt_Mux_Rslt
 424                         ; 484 	ADF_Mux_Filter(stW_AD_prms.slctd_mux_c, stW_AD_prms.slctd_muxCH_c);
 426  0174 f60000            	ldab	_stW_AD_prms
 427  0177 87                	clra	
 428  0178 6c80              	std	0,s
 429  017a f60002            	ldab	_stW_AD_prms+2
 430  017d 160000            	jsr	_ADF_Mux_Filter
 432  0180 1b82              	leas	2,s
 433                         ; 485 }
 436  0182 0a                	rtc	
 477                         ; 508 @far void stWF_Vsense_AD_Rdg(void)
 477                         ; 509 {
 478                         	switch	.ftext
 479  0183                   f_stWF_Vsense_AD_Rdg:
 481  0183 3b                	pshd	
 482       00000002          OFST:	set	2
 485                         ; 511 	for (i=0; i<WHEEL_SIDE_LMT; i++)
 487  0184 6981              	clr	OFST-1,s
 488  0186                   L17:
 489                         ; 514 		if (i == WHEEL_HIGH_SIDE)
 491  0186 e681              	ldab	OFST-1,s
 492  0188 2604              	bne	L77
 493                         ; 517 			stW_AD_prms.slctd_muxCH_c = CH6;
 495  018a c606              	ldab	#6
 497  018c 2002              	bra	L101
 498  018e                   L77:
 499                         ; 522 			stW_AD_prms.slctd_muxCH_c = CH5;
 501  018e c605              	ldab	#5
 502  0190                   L101:
 503  0190 7b0000            	stab	_stW_AD_prms
 504                         ; 526 		Dio_WriteChannelGroup(MuxSelGroup, stW_AD_prms.slctd_muxCH_c);
 507  0193 1410              	sei	
 512  0195 f60000            	ldab	_DioConfigData
 513  0198 87                	clra	
 514  0199 59                	lsld	
 515  019a b746              	tfr	d,y
 516  019c f60000            	ldab	_stW_AD_prms
 517  019f b60001            	ldaa	_DioConfigData+1
 518  01a2 2704              	beq	L41
 519  01a4                   L61:
 520  01a4 58                	lslb	
 521  01a5 0430fc            	dbne	a,L61
 522  01a8                   L41:
 523  01a8 f40002            	andb	_DioConfigData+2
 524  01ab 6b80              	stab	OFST-2,s
 525  01ad f60000            	ldab	_DioConfigData
 526  01b0 87                	clra	
 527  01b1 59                	lsld	
 528  01b2 b745              	tfr	d,x
 529  01b4 f60002            	ldab	_DioConfigData+2
 530  01b7 51                	comb	
 531  01b8 e4e30000          	andb	[_Dio_PortWrite_Ptr,x]
 532  01bc ea80              	orab	OFST-2,s
 533  01be 6beb0000          	stab	[_Dio_PortWrite_Ptr,y]
 537  01c2 10ef              	cli	
 539                         ; 529 		stWF_AD_Cnvrn();
 542  01c4 4a016262          	call	f_stWF_AD_Cnvrn
 544                         ; 532 		if (i == WHEEL_HIGH_SIDE)
 546  01c8 e681              	ldab	OFST-1,s
 547  01ca 261c              	bne	L301
 548                         ; 535 			stW_Vsense_HS_c = ADF_Nrmlz_Vsense(AD_mux_Filter[stW_AD_prms.slctd_mux_c][stW_AD_prms.slctd_muxCH_c].flt_rslt_w);
 550  01cc f60000            	ldab	_stW_AD_prms
 551  01cf 8606              	ldaa	#6
 552  01d1 12                	mul	
 553  01d2 b746              	tfr	d,y
 554  01d4 f60002            	ldab	_stW_AD_prms+2
 555  01d7 8630              	ldaa	#48
 556  01d9 12                	mul	
 557  01da 19ee              	leay	d,y
 558  01dc ecea0004          	ldd	_AD_mux_Filter+4,y
 559  01e0 160000            	jsr	_ADF_Nrmlz_Vsense
 561  01e3 7b00f9            	stab	_stW_Vsense_HS_c
 563  01e6 201a              	bra	L501
 564  01e8                   L301:
 565                         ; 540 			stW_Vsense_LS_c = ADF_Nrmlz_Vsense(AD_mux_Filter[stW_AD_prms.slctd_mux_c][stW_AD_prms.slctd_muxCH_c].flt_rslt_w);
 567  01e8 f60000            	ldab	_stW_AD_prms
 568  01eb 8606              	ldaa	#6
 569  01ed 12                	mul	
 570  01ee b746              	tfr	d,y
 571  01f0 f60002            	ldab	_stW_AD_prms+2
 572  01f3 8630              	ldaa	#48
 573  01f5 12                	mul	
 574  01f6 19ee              	leay	d,y
 575  01f8 ecea0004          	ldd	_AD_mux_Filter+4,y
 576  01fc 160000            	jsr	_ADF_Nrmlz_Vsense
 578  01ff 7b00f8            	stab	_stW_Vsense_LS_c
 579  0202                   L501:
 580                         ; 511 	for (i=0; i<WHEEL_SIDE_LMT; i++)
 582  0202 6281              	inc	OFST-1,s
 585  0204 e681              	ldab	OFST-1,s
 586  0206 c102              	cmpb	#2
 587  0208 1825ff7a          	blo	L17
 588                         ; 543 }
 591  020c 31                	puly	
 592  020d 0a                	rtc	
 655                         ; 568 @far void stWF_Isense_AD_Rdg(stW_Isense_type_e Isense_Rdg_Type)
 655                         ; 569 {
 656                         	switch	.ftext
 657  020e                   f_stWF_Isense_AD_Rdg:
 659  020e 3b                	pshd	
 660  020f 37                	pshb	
 661       00000001          OFST:	set	1
 664                         ; 574 	stW_AD_prms.slctd_muxCH_c = CH4;
 666  0210 c604              	ldab	#4
 667  0212 7b0000            	stab	_stW_AD_prms
 668                         ; 577 	Dio_WriteChannelGroup(MuxSelGroup, stW_AD_prms.slctd_muxCH_c);
 671  0215 1410              	sei	
 676  0217 f60000            	ldab	_DioConfigData
 677  021a 87                	clra	
 678  021b 59                	lsld	
 679  021c b746              	tfr	d,y
 680  021e f60000            	ldab	_stW_AD_prms
 681  0221 b60001            	ldaa	_DioConfigData+1
 682  0224 2704              	beq	L22
 683  0226                   L42:
 684  0226 58                	lslb	
 685  0227 0430fc            	dbne	a,L42
 686  022a                   L22:
 687  022a f40002            	andb	_DioConfigData+2
 688  022d 6b80              	stab	OFST-1,s
 689  022f f60000            	ldab	_DioConfigData
 690  0232 87                	clra	
 691  0233 59                	lsld	
 692  0234 b745              	tfr	d,x
 693  0236 f60002            	ldab	_DioConfigData+2
 694  0239 51                	comb	
 695  023a e4e30000          	andb	[_Dio_PortWrite_Ptr,x]
 696  023e ea80              	orab	OFST-1,s
 697  0240 6beb0000          	stab	[_Dio_PortWrite_Ptr,y]
 701  0244 10ef              	cli	
 703                         ; 580 	stWF_AD_Cnvrn();
 706  0246 4a016262          	call	f_stWF_AD_Cnvrn
 708                         ; 584 	if(Isense_Rdg_Type == ON_ISENSE){
 710  024a e682              	ldab	OFST+1,s
 711  024c 04211c            	dbne	b,L531
 712                         ; 586 		stW_Isense_c = ADF_Nrmlz_StW_Isense(AD_mux_Filter[stW_AD_prms.slctd_mux_c][stW_AD_prms.slctd_muxCH_c].flt_rslt_w);
 714  024f f60000            	ldab	_stW_AD_prms
 715  0252 8606              	ldaa	#6
 716  0254 12                	mul	
 717  0255 b746              	tfr	d,y
 718  0257 f60002            	ldab	_stW_AD_prms+2
 719  025a 8630              	ldaa	#48
 720  025c 12                	mul	
 721  025d 19ee              	leay	d,y
 722  025f ecea0004          	ldd	_AD_mux_Filter+4,y
 723  0263 160000            	jsr	_ADF_Nrmlz_StW_Isense
 725  0266 7b00f7            	stab	_stW_Isense_c
 727  0269 2003              	bra	L731
 728  026b                   L531:
 729                         ; 588 		stW_Isense_c = 0;
 731  026b 7900f7            	clr	_stW_Isense_c
 732  026e                   L731:
 733                         ; 616 }
 736  026e 1b83              	leas	3,s
 737  0270 0a                	rtc	
 767                         ; 645 @far void stWF_AD_Rdng(void)
 767                         ; 646 {
 768                         	switch	.ftext
 769  0271                   f_stWF_AD_Rdng:
 773                         ; 649 	stW_AD_prms.slctd_mux_c = MUX1;        // select the mux1 for stWheel feedbacks (Isense and Vsense)
 775  0271 790002            	clr	_stW_AD_prms+2
 776                         ; 650 	stW_AD_prms.slctd_adCH_c = CH6;        // select AD channel 6 in order to read the mux1 output
 778  0274 c606              	ldab	#6
 779  0276 7b0001            	stab	_stW_AD_prms+1
 780                         ; 653 	stW_pwm_state_c = (u_8Bit) Pwm_GetOutputState(PWM_STW);
 782  0279 cc0007            	ldd	#7
 783  027c 4a000000          	call	f_Pwm_GetOutputState
 785  0280 7b0018            	stab	_stW_pwm_state_c
 786                         ; 657 	if (stW_state_b == TRUE)
 788  0283 1f00fa010f        	brclr	_stW_Flag1_c,1,L151
 789                         ; 660 		if (stW_pwm_state_c == PWM_HIGH)
 791  0288 042117            	dbne	b,L751
 792                         ; 663 			stWF_Vsense_AD_Rdg();
 794  028b 4a018383          	call	f_stWF_Vsense_AD_Rdg
 796                         ; 666 			stWF_Isense_AD_Rdg(ON_ISENSE);
 798  028f cc0001            	ldd	#1
 799  0292 4a020e0e          	call	f_stWF_Isense_AD_Rdg
 803  0296 0a                	rtc	
 804  0297                   L151:
 805                         ; 676 		stWF_Vsense_AD_Rdg();
 807  0297 4a018383          	call	f_stWF_Vsense_AD_Rdg
 809                         ; 681 		stW_Isense_offset_w = 0; /* Offset compensation disabled for BTS6143D */
 811  029b 187900f5          	clrw	_stW_Isense_offset_w
 812                         ; 684 		stW_Isense_c = 0;
 814  029f 7900f7            	clr	_stW_Isense_c
 815  02a2                   L751:
 816                         ; 686 }
 819  02a2 0a                	rtc	
 846                         ; 707 @far void stWF_Cal_TmOut(void)
 846                         ; 708 {
 847                         	switch	.ftext
 848  02a3                   f_stWF_Cal_TmOut:
 850  02a3 3b                	pshd	
 851       00000002          OFST:	set	2
 854                         ; 710 	if ( (stW_level_c > WL0) && (stW_level_c <= WL4) ) {
 856  02a4 f6002a            	ldab	_stW_level_c
 857  02a7 272d              	beq	L171
 859  02a9 c104              	cmpb	#4
 860  02ab 2229              	bhi	L171
 861                         ; 712 		stW_TimeOut_Cycles_w = ( (stW_prms.cals[stW_veh_offset_c+stW_cfg_c].TimeOut_c[stW_level_c] * STW_TM_SLICE_MLTPLR) + (stW_prms.cals[stW_veh_offset_c+stW_cfg_c].TimeOut_c[stW_level_c]>>1) );
 863  02ad f600f0            	ldab	_stW_cfg_c
 864  02b0 87                	clra	
 865  02b1 fb00ef            	addb	_stW_veh_offset_c
 866  02b4 45                	rola	
 867  02b5 cd0013            	ldy	#19
 868  02b8 13                	emul	
 869  02b9 b746              	tfr	d,y
 870  02bb f6002a            	ldab	_stW_level_c
 871  02be 87                	clra	
 872  02bf 19ed              	leay	b,y
 873  02c1 e6ea002b          	ldab	_stW_prms,y
 874  02c5 54                	lsrb	
 875  02c6 6c80              	std	OFST-2,s
 876  02c8 e6ea002b          	ldab	_stW_prms,y
 877  02cc 86bb              	ldaa	#187
 878  02ce 12                	mul	
 879  02cf e380              	addd	OFST-2,s
 880  02d1 7c0022            	std	_stW_TimeOut_Cycles_w
 882  02d4 2004              	bra	L371
 883  02d6                   L171:
 884                         ; 716 		stW_TimeOut_Cycles_w = 0;
 886  02d6 18790022          	clrw	_stW_TimeOut_Cycles_w
 887  02da                   L371:
 888                         ; 718 }
 891  02da 31                	puly	
 892  02db 0a                	rtc	
 920                         ; 734 @far void stWF_Rdg_Temperature(void)
 920                         ; 735 {
 921                         	switch	.ftext
 922  02dc                   f_stWF_Rdg_Temperature:
 926                         ; 740 	if(Allow_stWheel_mngmnt_b == TRUE){
 928  02dc 1f0000200c        	brclr	_main_flag_c,32,L702
 929                         ; 743 		stW_temperature_c = canio_RX_StW_TempSts_c;		
 931  02e1 180c000000f1      	movb	_canio_RX_StW_TempSts_c,_stW_temperature_c
 932                         ; 744 		stW_tmprtr_sensor_flt_c = canio_RX_StW_TempSens_FltSts_c;
 934  02e7 180c00000019      	movb	_canio_RX_StW_TempSens_FltSts_c,_stW_tmprtr_sensor_flt_c
 936  02ed                   L702:
 937                         ; 755 }
 940  02ed 0a                	rtc	
 964                         ; 784 @far void stWF_Upt_Lvl(void)
 964                         ; 785 {
 965                         	switch	.ftext
 966  02ee                   f_stWF_Upt_Lvl:
 970                         ; 793 		switch (stW_level_c) {
 972  02ee f6002a            	ldab	_stW_level_c
 974  02f1 270f              	beq	L112
 975  02f3 c002              	subb	#2
 976  02f5 270f              	beq	L312
 977  02f7 040110            	dbeq	b,L512
 978  02fa 040111            	dbeq	b,L712
 979                         ; 826 			stWF_Clear();
 981  02fd 4a000000          	call	f_stWF_Clear
 984  0301 0a                	rtc	
 985  0302                   L112:
 986                         ; 798 			stW_level_c = WL4;
 988  0302 c604              	ldab	#4
 989                         ; 799 			break;
 991  0304 200a              	bra	LC001
 992  0306                   L312:
 993                         ; 805 			stW_level_c = WL1;
 995  0306 c601              	ldab	#1
 996                         ; 806 			break;
 998  0308 2006              	bra	LC001
 999  030a                   L512:
1000                         ; 812 			stW_level_c = WL2;
1002  030a c602              	ldab	#2
1003                         ; 813 			break;
1005  030c 2002              	bra	LC001
1006  030e                   L712:
1007                         ; 819 			stW_level_c = WL3;
1009  030e c603              	ldab	#3
1010  0310                   LC001:
1011  0310 7b002a            	stab	_stW_level_c
1012                         ; 820 			break;
1014                         ; 847 }
1017  0313 0a                	rtc	
1045                         ; 871 @far void stWF_Updt_Pwm(void)
1045                         ; 872 {
1046                         	switch	.ftext
1047  0314                   f_stWF_Updt_Pwm:
1051                         ; 874 	if ( (stW_level_c > WL0) && (stW_level_c <= WL4) ) {
1053  0314 f6002a            	ldab	_stW_level_c
1054  0317 2723              	beq	L742
1056  0319 c104              	cmpb	#4
1057  031b 221f              	bhi	L742
1058                         ; 876 		stW_pwm_w = mainF_Calc_PWM(stW_prms.cals[stW_veh_offset_c+stW_cfg_c].Pwm_a[stW_level_c]);
1060  031d f600f0            	ldab	_stW_cfg_c
1061  0320 87                	clra	
1062  0321 fb00ef            	addb	_stW_veh_offset_c
1063  0324 45                	rola	
1064  0325 cd0013            	ldy	#19
1065  0328 13                	emul	
1066  0329 b746              	tfr	d,y
1067  032b f6002a            	ldab	_stW_level_c
1068  032e 87                	clra	
1069  032f 19ed              	leay	b,y
1070  0331 e6ea0030          	ldab	_stW_prms+5,y
1071  0335 160000            	jsr	_mainF_Calc_PWM
1073  0338 7c001c            	std	_stW_pwm_w
1076  033b 0a                	rtc	
1077  033c                   L742:
1078                         ; 880 		stW_pwm_w = 0;
1080  033c 1879001c          	clrw	_stW_pwm_w
1081                         ; 882 }
1084  0340 0a                	rtc	
1109                         ; 896 @far void stWF_Diag_IO_Updt_Lvl(void)
1109                         ; 897 {
1110                         	switch	.ftext
1111  0341                   f_stWF_Diag_IO_Updt_Lvl:
1115                         ; 899 	if ( (diag_io_st_whl_rq_c > WL0) && (diag_io_st_whl_rq_c <= WL4) ) {
1117  0341 f60000            	ldab	_diag_io_st_whl_rq_c
1118  0344 2708              	beq	L362
1120  0346 c104              	cmpb	#4
1121  0348 2204              	bhi	L362
1122                         ; 901 		stW_level_c = diag_io_st_whl_rq_c;
1124  034a 7b002a            	stab	_stW_level_c
1127  034d 0a                	rtc	
1128  034e                   L362:
1129                         ; 905 		stW_level_c = WL0;
1131  034e 79002a            	clr	_stW_level_c
1132                         ; 907 }
1135  0351 0a                	rtc	
1159                         ; 921 @far void stWF_Diag_Timeout_Updt_Lvl(void)
1159                         ; 922 {
1160                         	switch	.ftext
1161  0352                   f_stWF_Diag_Timeout_Updt_Lvl:
1165                         ; 924 	if ( (diag_io_st_whl_rq_c > WL0) && (diag_io_st_whl_rq_c <= WL4) ) {
1167  0352 f60000            	ldab	_diag_io_st_whl_rq_c
1168  0355 2708              	beq	L772
1170  0357 c104              	cmpb	#4
1171  0359 2204              	bhi	L772
1172                         ; 926 		diag_io_st_whl_rq_c -= 1;
1174  035b 730000            	dec	_diag_io_st_whl_rq_c
1177  035e 0a                	rtc	
1178  035f                   L772:
1179                         ; 930 		diag_io_st_whl_rq_c = WL0;
1181  035f 790000            	clr	_diag_io_st_whl_rq_c
1182                         ; 932 }
1185  0362 0a                	rtc	
1211                         ; 947 @far void stWLF_Chck_Diag_TurnOff(void)
1211                         ; 948 {
1212                         	switch	.ftext
1213  0363                   f_stWLF_Chck_Diag_TurnOff:
1217                         ; 950 	if ( (diag_io_st_whl_rq_lock_b == TRUE) && (stW_level_c == WL1) ) {
1219  0363 1f00008009        	brclr	_diag_io_lock_flags_c,128,L513
1221  0368 f6002a            	ldab	_stW_level_c
1222  036b 042103            	dbne	b,L513
1223                         ; 952 		diag_io_st_whl_rq_c = 0;
1225  036e 790000            	clr	_diag_io_st_whl_rq_c
1227  0371                   L513:
1228                         ; 957 }
1231  0371 0a                	rtc	
1265                         ; 1022 @far void stWF_Timer(void)
1265                         ; 1023 {
1266                         	switch	.ftext
1267  0372                   f_stWF_Timer:
1271                         ; 1025 	stWF_Cal_TmOut();
1273  0372 4a02a3a3          	call	f_stWF_Cal_TmOut
1275                         ; 1028 	if (stW_onTime_w < stW_TimeOut_Cycles_w) {
1277  0376 fc0024            	ldd	_stW_onTime_w
1278  0379 bc0022            	cpd	_stW_TimeOut_Cycles_w
1279  037c 241b              	bhs	L723
1280                         ; 1030 		if ( (stW_state_b == TRUE) && (stW_Flt_status_w == STW_NO_FLT_MASK) ) {
1282  037e 1f00fa010a        	brclr	_stW_Flag1_c,1,L133
1284  0383 fc00f2            	ldd	_stW_Flt_status_w
1285  0386 2605              	bne	L133
1286                         ; 1032 			stW_onTime_w++;
1288  0388 18720024          	incw	_stW_onTime_w
1291  038c 0a                	rtc	
1292  038d                   L133:
1293                         ; 1036 			if (stW_status_c != UGLY) {
1295  038d f60029            	ldab	_stW_status_c
1296  0390 c103              	cmpb	#3
1297  0392 2630              	bne	L143
1299                         ; 1041 				stW_onTime_w = 0;
1301  0394 18790024          	clrw	_stW_onTime_w
1303  0398 0a                	rtc	
1304  0399                   L723:
1305                         ; 1047 		stW_onTime_w = 0;
1307  0399 18790024          	clrw	_stW_onTime_w
1308                         ; 1050 		if ( (stW_level_c > WL1) && (stW_level_c <= WL4) ) {
1310  039d f6002a            	ldab	_stW_level_c
1311  03a0 c101              	cmpb	#1
1312  03a2 2318              	bls	L343
1314  03a4 c104              	cmpb	#4
1315  03a6 2214              	bhi	L343
1316                         ; 1052 			if (stW_diag_cntrl_act_b == FALSE) {
1318  03a8 1e00fa1006        	brset	_stW_Flag1_c,16,L543
1319                         ; 1054 				stWF_Upt_Lvl();
1321  03ad 4a02eeee          	call	f_stWF_Upt_Lvl
1324  03b1 2004              	bra	L743
1325  03b3                   L543:
1326                         ; 1058 				stWF_Diag_Timeout_Updt_Lvl();
1328  03b3 4a035252          	call	f_stWF_Diag_Timeout_Updt_Lvl
1330  03b7                   L743:
1331                         ; 1061 			stWF_Updt_Pwm();
1333  03b7 4a031414          	call	f_stWF_Updt_Pwm
1337  03bb 0a                	rtc	
1338  03bc                   L343:
1339                         ; 1066 			stWLF_Chck_Diag_TurnOff();
1341  03bc 4a036363          	call	f_stWLF_Chck_Diag_TurnOff
1343                         ; 1069 			stWF_Clear();
1345  03c0 4a000000          	call	f_stWF_Clear
1347  03c4                   L143:
1348                         ; 1072 }
1351  03c4 0a                	rtc	
1392                         ; 1101 @far void stWF_Updt_Status(void)
1392                         ; 1102 {
1393                         	switch	.ftext
1394  03c5                   f_stWF_Updt_Status:
1396  03c5 37                	pshb	
1397       00000001          OFST:	set	1
1400                         ; 1107 	stW_prv_stat_c = stW_status_c;
1402  03c6 1809800029        	movb	_stW_status_c,OFST-1,s
1403                         ; 1110 	stWF_Rdg_Temperature();
1405  03cb 4a02dcdc          	call	f_stWF_Rdg_Temperature
1407                         ; 1114 	if ( (stW_Flt_status_w <= STW_PRL_FLT_MAX) && (relay_C_internal_Fault_b == FALSE) && 
1407                         ; 1115 		 (stW_temperature_c <= stW_prms.cals[stW_veh_offset_c+stW_cfg_c].MaxTemp_Threshld) && (stW_temperature_c != STW_TEMP_SNA) ) {
1409  03cf fc00f2            	ldd	_stW_Flt_status_w
1410  03d2 8c003f            	cpd	#63
1411  03d5 2227              	bhi	L763
1413  03d7 1e00000122        	brset	_relay_fault_status2_c,1,L763
1415  03dc f600f0            	ldab	_stW_cfg_c
1416  03df 87                	clra	
1417  03e0 fb00ef            	addb	_stW_veh_offset_c
1418  03e3 45                	rola	
1419  03e4 cd0013            	ldy	#19
1420  03e7 13                	emul	
1421  03e8 b746              	tfr	d,y
1422  03ea e6ea0039          	ldab	_stW_prms+14,y
1423  03ee f100f1            	cmpb	_stW_temperature_c
1424  03f1 250b              	blo	L763
1426  03f3 f600f1            	ldab	_stW_temperature_c
1427  03f6 048105            	ibeq	b,L763
1428                         ; 1117 		stW_status_c = main_CSWM_Status_c;
1430  03f9 f60000            	ldab	_main_CSWM_Status_c
1432  03fc 2002              	bra	L173
1433  03fe                   L763:
1434                         ; 1121 		stW_status_c = UGLY;
1436  03fe c603              	ldab	#3
1437  0400                   L173:
1438  0400 7b0029            	stab	_stW_status_c
1439                         ; 1125 	if ( (stW_prv_stat_c != UGLY) && (stW_status_c == UGLY) ) {
1441  0403 e680              	ldab	OFST-1,s
1442  0405 c103              	cmpb	#3
1443  0407 270b              	beq	L373
1445  0409 f60029            	ldab	_stW_status_c
1446  040c c103              	cmpb	#3
1447  040e 2604              	bne	L373
1448                         ; 1127 		stW_LED_off_b = TRUE;
1450  0410 1c00fa20          	bset	_stW_Flag1_c,32
1451  0414                   L373:
1452                         ; 1129 }
1455  0414 1b81              	leas	1,s
1456  0416 0a                	rtc	
1488                         ; 1147 @far u_8Bit stWLF_Get_Swtch_Rq(void)
1488                         ; 1148 {
1489                         	switch	.ftext
1490  0417                   f_stWLF_Get_Swtch_Rq:
1492       00000001          OFST:	set	1
1495                         ; 1152 	u_8Bit swtch_rq = STW_SNA;
1497                         ; 1154 	swtch_rq = canio_RX_HSW_RQ_TGW_c;	
1499  0417 f60000            	ldab	_canio_RX_HSW_RQ_TGW_c
1500                         ; 1168 	return(swtch_rq);
1504  041a 0a                	rtc	
1544                         ; 1191 @far void stWF_Swtch_Mngmnt(void)
1544                         ; 1192 {
1545                         	switch	.ftext
1546  041b                   f_stWF_Swtch_Mngmnt:
1548  041b 37                	pshb	
1549       00000001          OFST:	set	1
1552                         ; 1193 	u_8Bit stW_rq = 0;           // current stw request
1554  041c 6980              	clr	OFST-1,s
1555                         ; 1196 	if ( (diag_io_st_whl_rq_lock_b == FALSE) && (diag_io_st_whl_state_lock_b == FALSE) && (diag_rc_all_op_lock_b == FALSE) ) {
1557  041e 1e00008041        	brset	_diag_io_lock_flags_c,128,L524
1559  0423 1e0000043c        	brset	_diag_io_lock3_flags_c,4,L524
1561  0428 1e00000137        	brset	_diag_rc_lock_flags_c,1,L524
1562                         ; 1199 		stW_rq = stWLF_Get_Swtch_Rq();
1564  042d 4a041717          	call	f_stWLF_Get_Swtch_Rq
1566  0431 6b80              	stab	OFST-1,s
1567                         ; 1203 		if ( (stW_rq == STW_PSD) || (stW_rq == STW_NOT_PSD) ) {
1569  0433 c101              	cmpb	#1
1570  0435 2705              	beq	LC002
1572  0437 04611b            	tbne	b,L724
1573                         ; 1205 			if ( (stW_rq == STW_PSD) && (stW_lst_sw_rq_c == STW_NOT_PSD) ) {
1575  043a 2621              	bne	L344
1576  043c                   LC002:
1578  043c f60028            	ldab	_stW_lst_sw_rq_c
1579  043f 261c              	bne	L344
1580                         ; 1207 				if (stW_swtch_rq_prps_b == FALSE) {
1582  0441 1e00fa080d        	brset	_stW_Flag1_c,8,L534
1583                         ; 1209 					stW_swtch_rq_prps_b = TRUE;
1585  0446 1c00fa08          	bset	_stW_Flag1_c,8
1586                         ; 1212 					stW_onTime_w = 0;
1588  044a 87                	clra	
1589  044b 7c0024            	std	_stW_onTime_w
1590  044e 7c001e            	std	_stW_LED_Dsply_Tm_cnt
1591                         ; 1213 					stW_LED_Dsply_Tm_cnt = 0;
1594  0451 200a              	bra	L344
1595  0453                   L534:
1596                         ; 1217 					stW_swtch_rq_prps_b = FALSE;
1598  0453 2004              	bra	LC003
1599  0455                   L724:
1600                         ; 1226 			if(stW_rq == STW_SNA){
1602  0455 c103              	cmpb	#3
1603  0457 2604              	bne	L344
1604                         ; 1228 				stW_swtch_rq_prps_b = FALSE;
1606  0459                   LC003:
1607  0459 1d00fa08          	bclr	_stW_Flag1_c,8
1609  045d                   L344:
1610                         ; 1237 		stW_lst_sw_rq_c = stW_rq;
1612  045d 180d800028        	movb	OFST-1,s,_stW_lst_sw_rq_c
1614  0462 2026              	bra	L154
1615  0464                   L524:
1616                         ; 1243 		if(diag_rc_all_op_lock_b == TRUE){			
1618  0464 1f00000109        	brclr	_diag_rc_lock_flags_c,1,L354
1619                         ; 1245 			if(diag_rc_all_heaters_lock_b == TRUE){
1621  0469 1f00000202        	brclr	_diag_rc_lock_flags_c,2,L554
1622                         ; 1247 				stW_swtch_rq_prps_b = TRUE;					
1625  046e 2010              	bra	LC005
1626  0470                   L554:
1627                         ; 1250 				stW_swtch_rq_prps_b = FALSE;
1629  0470 2014              	bra	L564
1630  0472                   L354:
1631                         ; 1254 			if (diag_io_st_whl_state_lock_b == FALSE) {
1633  0472 1e00000413        	brset	_diag_io_lock3_flags_c,4,L154
1634                         ; 1256 				if ( (diag_io_st_whl_rq_c > WL0) && (diag_io_st_whl_rq_c <= WL4) ) {
1636  0477 f60000            	ldab	_diag_io_st_whl_rq_c
1637  047a 270a              	beq	L564
1639  047c c104              	cmpb	#4
1640  047e 2206              	bhi	L564
1641                         ; 1258 					stW_swtch_rq_prps_b = TRUE;
1643  0480                   LC005:
1644  0480 1c00fa08          	bset	_stW_Flag1_c,8
1646  0484 2004              	bra	L154
1647  0486                   L564:
1648                         ; 1262 					stW_swtch_rq_prps_b = FALSE;
1650  0486 1d00fa08          	bclr	_stW_Flag1_c,8
1651  048a                   L154:
1652                         ; 1270 }
1655  048a 1b81              	leas	1,s
1656  048c 0a                	rtc	
1684                         ; 1288 @far void stWF_Fnsh_LED_Dslpy(void)
1684                         ; 1289 {
1685                         	switch	.ftext
1686  048d                   f_stWF_Fnsh_LED_Dslpy:
1690                         ; 1291 	stWF_Clear();
1692  048d 4a000000          	call	f_stWF_Clear
1694                         ; 1294 	if (diag_io_st_whl_rq_lock_b == TRUE) {
1696  0491 1f00008003        	brclr	_diag_io_lock_flags_c,128,L305
1697                         ; 1296 		diag_io_st_whl_rq_c = 0;
1699  0496 790000            	clr	_diag_io_st_whl_rq_c
1700  0499                   L305:
1701                         ; 1300 	if (diag_io_st_whl_state_lock_b == TRUE) {
1703  0499 1f00000403        	brclr	_diag_io_lock3_flags_c,4,L505
1704                         ; 1302 		diag_io_st_whl_state_c = 0;
1706  049e 790000            	clr	_diag_io_st_whl_state_c
1707  04a1                   L505:
1708                         ; 1304 }
1711  04a1 0a                	rtc	
1746                         ; 1379 @far void stWF_LED_Display(void)
1746                         ; 1380 {
1747                         	switch	.ftext
1748  04a2                   f_stWF_LED_Display:
1752                         ; 1382 	if (stW_LED_off_b == TRUE) {
1754  04a2 1f00fa200a        	brclr	_stW_Flag1_c,32,L125
1755                         ; 1385 		stW_LED_Dsply_Tm_cnt = stW_LED_Dsply_Tm;
1757  04a7 18040011001e      	movw	_stW_LED_Dsply_Tm,_stW_LED_Dsply_Tm_cnt
1758                         ; 1388 		stW_LED_off_b = FALSE;
1760  04ad 1d00fa20          	bclr	_stW_Flag1_c,32
1762  04b1                   L125:
1763                         ; 1396 	if ( (canio_RX_IGN_STATUS_c == IGN_RUN) || (canio_RX_IGN_STATUS_c == IGN_START) ) {
1765  04b1 f60000            	ldab	_canio_RX_IGN_STATUS_c
1766  04b4 c104              	cmpb	#4
1767  04b6 2704              	beq	L525
1769  04b8 c105              	cmpb	#5
1770  04ba 2641              	bne	L745
1771  04bc                   L525:
1772                         ; 1399 		if (stW_swtch_rq_prps_b == TRUE) {
1774  04bc 1f00fa0822        	brclr	_stW_Flag1_c,8,L725
1775                         ; 1402 			if (stW_status_c != UGLY) {
1777  04c1 f60029            	ldab	_stW_status_c
1778  04c4 c103              	cmpb	#3
1779                         ; 1404 				stW_LED_stat_c = TRUE;
1782  04c6 2629              	bne	LC008
1783                         ; 1408 				if (stW_LED_Dsply_Tm_cnt < stW_LED_Dsply_Tm) {
1785  04c8 fc001e            	ldd	_stW_LED_Dsply_Tm_cnt
1786  04cb bc0011            	cpd	_stW_LED_Dsply_Tm
1787  04ce 2406              	bhs	L535
1788                         ; 1410 					stW_LED_Dsply_Tm_cnt++;
1790  04d0 1872001e          	incw	_stW_LED_Dsply_Tm_cnt
1791                         ; 1413 					stW_LED_stat_c = TRUE;
1794  04d4 201b              	bra	LC008
1795  04d6                   L535:
1796                         ; 1417 					if(diag_rc_all_op_lock_b == FALSE){
1798  04d6 1e00000106        	brset	_diag_rc_lock_flags_c,1,L145
1799                         ; 1420 						stWF_Fnsh_LED_Dslpy();						
1801  04db 4a048d8d          	call	f_stWF_Fnsh_LED_Dslpy
1804  04df 2020              	bra	L755
1805  04e1                   L145:
1806                         ; 1424 						stW_LED_stat_c = FALSE;
1808  04e1 2015              	bra	L155
1809  04e3                   L725:
1810                         ; 1431 			if (diag_io_st_whl_state_lock_b == TRUE) {
1812  04e3 1f00000415        	brclr	_diag_io_lock3_flags_c,4,L745
1813                         ; 1437 				if ( (diag_io_st_whl_state_c > WL0) && (diag_io_st_whl_state_c <= WL4) ) {
1815  04e8 f60000            	ldab	_diag_io_st_whl_state_c
1816  04eb 270b              	beq	L155
1818  04ed c104              	cmpb	#4
1819  04ef 2207              	bhi	L155
1820                         ; 1439 					stW_LED_stat_c = TRUE;
1822  04f1                   LC008:
1823  04f1 c601              	ldab	#1
1824  04f3 7b0027            	stab	_stW_LED_stat_c
1826  04f6 2009              	bra	L755
1827  04f8                   L155:
1828                         ; 1443 					stW_LED_stat_c = FALSE;
1830  04f8 790027            	clr	_stW_LED_stat_c
1831  04fb 2004              	bra	L755
1832  04fd                   L745:
1833                         ; 1448 				stWF_Clear();
1835  04fd 4a000000          	call	f_stWF_Clear
1837  0501                   L755:
1838                         ; 1461 	if(stW_prev_LED_stat_c != stW_LED_stat_c){
1840  0501 f60026            	ldab	_stW_prev_LED_stat_c
1841  0504 f10027            	cmpb	_stW_LED_stat_c
1842  0507 270e              	beq	L365
1843                         ; 1470 		IlPutTxHSW_StatSts(stW_LED_stat_c);
1845  0509 f60027            	ldab	_stW_LED_stat_c
1846  050c 87                	clra	
1847  050d 4a000000          	call	f_IlPutTxHSW_StatSts
1849                         ; 1473 		stW_prev_LED_stat_c = stW_LED_stat_c;
1851  0511 180c00270026      	movb	_stW_LED_stat_c,_stW_prev_LED_stat_c
1853                         ; 1454 		stWF_Clear();
1856  0517                   L365:
1857                         ; 1478 }
1860  0517 0a                	rtc	
1894                         ; 1519 @far void stWF_Output_Control(void)
1894                         ; 1520 {	
1895                         	switch	.ftext
1896  0518                   f_stWF_Output_Control:
1900                         ; 1522 	if ( (stW_sw_rq_b == TRUE) && (relay_C_Ok_b == TRUE) )
1902  0518 1f00fa0428        	brclr	_stW_Flag1_c,4,L575
1904  051d 1f00000423        	brclr	_relay_status2_c,4,L575
1905                         ; 1526 		if(stW_prev_pwm != stW_pwm_w){
1907  0522 fc001a            	ldd	_stW_prev_pwm
1908  0525 bc001c            	cpd	_stW_pwm_w
1909  0528 270d              	beq	L106
1910                         ; 1527 			Pwm_SetDutyCycle(PWM_STW, stW_pwm_w);
1912  052a fc001c            	ldd	_stW_pwm_w
1913  052d 3b                	pshd	
1914  052e cc0007            	ldd	#7
1915  0531 4a000000          	call	f_Pwm_SetDutyCycle
1917  0535 1b82              	leas	2,s
1919  0537                   L106:
1920                         ; 1532 		stW_prev_pwm = stW_pwm_w;	// do not use stW_prev_pwm when calling Pwm_SetDutyCycle
1922  0537 fc001c            	ldd	_stW_pwm_w
1923  053a 7c001a            	std	_stW_prev_pwm
1924                         ; 1535 		stW_state_b = TRUE;
1926  053d 1c00fa01          	bset	_stW_Flag1_c,1
1927                         ; 1538 		stW_chck_Open_or_STB_dly_c = 0;
1929  0541 790015            	clr	_stW_chck_Open_or_STB_dly_c
1932  0544 0a                	rtc	
1933  0545                   L575:
1934                         ; 1543 		if( (stW_state_b == TRUE) || (stW_prev_pwm != 0) ){
1936  0545 1e00fa0105        	brset	_stW_Flag1_c,1,L706
1938  054a fc001a            	ldd	_stW_prev_pwm
1939  054d 2713              	beq	L116
1940  054f                   L706:
1941                         ; 1545 			Pwm_SetDutyCycle(PWM_STW, 0);	
1943  054f 87                	clra	
1944  0550 c7                	clrb	
1945  0551 3b                	pshd	
1946  0552 c607              	ldab	#7
1947  0554 4a000000          	call	f_Pwm_SetDutyCycle
1949  0558 1b82              	leas	2,s
1950                         ; 1550 			DisableAllInterrupts();
1953  055a 1410              	sei	
1955                         ; 1553 			CFORC_FOC7 = TRUE;		// Force output compare action for channel 7 while turning off (for immediate affect) 
1958  055c 1c000080          	bset	__CFORC,128
1959                         ; 1555 			EnableAllInterrupts();
1962  0560 10ef              	cli	
1965  0562                   L116:
1966                         ; 1573 		if(PTIT_PTIT7 == TRUE){
1968  0562 1f0000800b        	brclr	__PTIT,128,L516
1969                         ; 1575 			Pwm_SetDutyCycle(PWM_STW, 0);
1971  0567 87                	clra	
1972  0568 c7                	clrb	
1973  0569 3b                	pshd	
1974  056a c607              	ldab	#7
1975  056c 4a000000          	call	f_Pwm_SetDutyCycle
1977  0570 1b82              	leas	2,s
1979  0572                   L516:
1980                         ; 1584 		stW_prev_pwm = 0;
1982  0572 1879001a          	clrw	_stW_prev_pwm
1983                         ; 1587 		stW_Isense_dly_cnt_c = 0;
1985  0576 790016            	clr	_stW_Isense_dly_cnt_c
1986                         ; 1590 		stW_state_b = FALSE;
1988  0579 1d00fa01          	bclr	_stW_Flag1_c,1
1989                         ; 1592 }
1992  057d 0a                	rtc	
2019                         ; 1615 @far void stWF_Chck_Swtch_Prps(void)
2019                         ; 1616 {
2020                         	switch	.ftext
2021  057e                   f_stWF_Chck_Swtch_Prps:
2025                         ; 1618 	if (stW_swtch_rq_prps_b == TRUE)
2027  057e 1f00fa081e        	brclr	_stW_Flag1_c,8,L736
2028                         ; 1621 		if (stW_status_c == GOOD)
2030  0583 f60029            	ldab	_stW_status_c
2031  0586 c101              	cmpb	#1
2032  0588 2609              	bne	L136
2033                         ; 1624 			stWF_Updt_Pwm();
2035  058a 4a031414          	call	f_stWF_Updt_Pwm
2037                         ; 1627 			stW_sw_rq_b = TRUE;
2039  058e 1c00fa04          	bset	_stW_Flag1_c,4
2042  0592 0a                	rtc	
2043  0593                   L136:
2044                         ; 1632 			if (stW_status_c == BAD)
2046  0593 c102              	cmpb	#2
2047  0595 2606              	bne	L536
2048                         ; 1635 				stWF_Updt_Pwm();
2050  0597 4a031414          	call	f_stWF_Updt_Pwm
2053  059b 2004              	bra	L736
2054  059d                   L536:
2055                         ; 1640 				stW_pwm_w = 0;
2057  059d 1879001c          	clrw	_stW_pwm_w
2058  05a1                   L736:
2059                         ; 1643 			stW_sw_rq_b = FALSE;
2061                         ; 1649 		stW_sw_rq_b = FALSE;
2063  05a1 1d00fa04          	bclr	_stW_Flag1_c,4
2064                         ; 1651 }
2067  05a5 0a                	rtc	
2093                         ; 1669 @far void stWF_Rtrn_Cntrl_toECU(void)
2093                         ; 1670 {
2094                         	switch	.ftext
2095  05a6                   f_stWF_Rtrn_Cntrl_toECU:
2099                         ; 1671 	stW_diag_cntrl_act_b   = FALSE;    // diagnostics is inactive
2101  05a6 1d00fa10          	bclr	_stW_Flag1_c,16
2102                         ; 1673 	stW_Flt_Mtr_cnt        = 0;        // clear fault mature count
2104  05aa 18790020          	clrw	_stW_Flt_Mtr_cnt
2105                         ; 1676 	stWF_Clear();                      // clear stW variables
2107  05ae 4a000000          	call	f_stWF_Clear
2109                         ; 1677 }
2112  05b2 0a                	rtc	
2147                         ; 1699 @far void stWF_Swtch_Cntrl(void)
2147                         ; 1700 {
2148                         	switch	.ftext
2149  05b3                   f_stWF_Swtch_Cntrl:
2151  05b3 37                	pshb	
2152       00000001          OFST:	set	1
2155                         ; 1701 	u_8Bit stW_crt_rq = 0;           // current stw request
2157  05b4 6980              	clr	OFST-1,s
2158                         ; 1704 	stW_crt_rq = stWLF_Get_Swtch_Rq();
2160  05b6 4a041717          	call	f_stWLF_Get_Swtch_Rq
2162  05ba 6b80              	stab	OFST-1,s
2163                         ; 1707 	if ( (stW_crt_rq == STW_PSD) && (stW_not_vld_swtch_rq_b == FALSE) ) {
2165  05bc c101              	cmpb	#1
2166  05be 2613              	bne	L766
2168  05c0 1e00fa020e        	brset	_stW_Flag1_c,2,L766
2169                         ; 1709 		stW_not_vld_swtch_rq_b = TRUE;
2171  05c5 1c00fa02          	bset	_stW_Flag1_c,2
2172                         ; 1712 		stWF_Upt_Lvl();
2174  05c9 4a02eeee          	call	f_stWF_Upt_Lvl
2176                         ; 1715 		stWF_Chck_Swtch_Prps();
2178  05cd 4a057e7e          	call	f_stWF_Chck_Swtch_Prps
2181  05d1 200d              	bra	L176
2182  05d3                   L766:
2183                         ; 1719 		if (stW_crt_rq == STW_NOT_PSD) {
2185  05d3 046106            	tbne	b,L376
2186                         ; 1721 			stW_not_vld_swtch_rq_b = FALSE;
2188  05d6 1d00fa02          	bclr	_stW_Flag1_c,2
2190  05da 2004              	bra	L176
2191  05dc                   L376:
2192                         ; 1725 			stW_not_vld_swtch_rq_b = TRUE;
2194  05dc 1c00fa02          	bset	_stW_Flag1_c,2
2195  05e0                   L176:
2196                         ; 1728 }
2199  05e0 1b81              	leas	1,s
2200  05e2 0a                	rtc	
2231                         ; 1748 @far void stWF_Diag_IO_Cntrl(void)
2231                         ; 1749 {
2232                         	switch	.ftext
2233  05e3                   f_stWF_Diag_IO_Cntrl:
2237                         ; 1751 	stW_diag_cntrl_act_b = TRUE;
2239  05e3 1c00fa10          	bset	_stW_Flag1_c,16
2240                         ; 1754 	if (stW_clr_ontime_b == TRUE)
2242  05e7 1f00fa400c        	brclr	_stW_Flag1_c,64,L117
2243                         ; 1757 		stW_onTime_w = 0;
2245  05ec 87                	clra	
2246  05ed c7                	clrb	
2247  05ee 7c0024            	std	_stW_onTime_w
2248  05f1 7c001e            	std	_stW_LED_Dsply_Tm_cnt
2249                         ; 1758 		stW_LED_Dsply_Tm_cnt = 0;
2251                         ; 1761 		stW_clr_ontime_b = FALSE;
2253  05f4 1d00fa40          	bclr	_stW_Flag1_c,64
2255  05f8                   L117:
2256                         ; 1769 	if(diag_rc_all_op_lock_b == TRUE){
2258  05f8 1f00000111        	brclr	_diag_rc_lock_flags_c,1,L317
2259                         ; 1772 		if(diag_rc_all_heaters_lock_b == TRUE){
2261  05fd 1f00000207        	brclr	_diag_rc_lock_flags_c,2,L517
2262                         ; 1775 			stW_level_c = WL4;
2264  0602 c604              	ldab	#4
2265  0604 7b002a            	stab	_stW_level_c
2266                         ; 1778 			stWF_Chck_Swtch_Prps();
2270  0607 200e              	bra	LC010
2271  0609                   L517:
2272                         ; 1782 			stWF_Clear();
2274  0609 4a000000          	call	f_stWF_Clear
2277  060d 0a                	rtc	
2278  060e                   L317:
2279                         ; 1787 		if (diag_io_st_whl_state_lock_b == FALSE)
2281  060e 1e00000409        	brset	_diag_io_lock3_flags_c,4,L327
2282                         ; 1790 			stWF_Diag_IO_Updt_Lvl();
2284  0613 4a034141          	call	f_stWF_Diag_IO_Updt_Lvl
2286                         ; 1793 			stWF_Chck_Swtch_Prps();
2288  0617                   LC010:
2289  0617 4a057e7e          	call	f_stWF_Chck_Swtch_Prps
2293  061b 0a                	rtc	
2294  061c                   L327:
2295                         ; 1798 			stW_sw_rq_b = FALSE;
2297                         ; 1801 			stW_swtch_rq_prps_b = FALSE;
2299  061c 1d00fa0c          	bclr	_stW_Flag1_c,12
2300                         ; 1804 }
2303  0620 0a                	rtc	
2331                         ; 1902 @far void stWLF_RemSt_Chck(void)
2331                         ; 1903 {
2332                         	switch	.ftext
2333  0621                   f_stWLF_RemSt_Chck:
2337                         ; 1905 	if (main_state_c == REM_STRT_HEATING) {
2339  0621 f60000            	ldab	_main_state_c
2340  0624 c101              	cmpb	#1
2341  0626 2619              	bne	L547
2342                         ; 1907 		if (stW_RemSt_auto_b == TRUE) {
2344  0628 1e00100114        	brset	_stW_Flag2_c,1,L547
2346                         ; 1912 			stW_swtch_rq_prps_b = TRUE;
2348  062d 1c00fa08          	bset	_stW_Flag1_c,8
2349                         ; 1917 			stW_level_c = WL4;
2351  0631 c604              	ldab	#4
2352  0633 7b002a            	stab	_stW_level_c
2353                         ; 1920 			stWF_Chck_Swtch_Prps();
2355  0636 4a057e7e          	call	f_stWF_Chck_Swtch_Prps
2357                         ; 1923 			stW_RemSt_auto_b = TRUE;
2359  063a 1c001001          	bset	_stW_Flag2_c,1
2360  063e f60000            	ldab	_main_state_c
2361  0641                   L547:
2362                         ; 1932 	if (main_state_c == AUTO_STRT_HEATING) {
2364  0641 c104              	cmpb	#4
2365  0643 2616              	bne	L557
2366                         ; 1934 		if (stW_RemSt_auto_b == TRUE) {
2368  0645 1e00100111        	brset	_stW_Flag2_c,1,L557
2370                         ; 1939 			stW_swtch_rq_prps_b = TRUE;
2372  064a 1c00fa08          	bset	_stW_Flag1_c,8
2373                         ; 1942 			stW_level_c = WL4;
2375  064e c604              	ldab	#4
2376  0650 7b002a            	stab	_stW_level_c
2377                         ; 1945 			stWF_Chck_Swtch_Prps();
2379  0653 4a057e7e          	call	f_stWF_Chck_Swtch_Prps
2381                         ; 1948 			stW_RemSt_auto_b = TRUE;
2383  0657 1c001001          	bset	_stW_Flag2_c,1
2384  065b                   L557:
2385                         ; 1955 	autostart_check_complete_b = TRUE; //Make sure Auto start complete flag check in main remote start check.
2387  065b 1c000008          	bset	_main_flag2_c,8
2388                         ; 1957 }
2391  065f 0a                	rtc	
2419                         ; 1978 @far void stWLF_Chck_mainState(void)
2419                         ; 1979 {
2420                         	switch	.ftext
2421  0660                   f_stWLF_Chck_mainState:
2425                         ; 1984 	if ( (stW_lst_main_state_c != NORMAL) && (main_state_c == NORMAL) ) {
2427  0660 f6000f            	ldab	_stW_lst_main_state_c
2428  0663 2709              	beq	L177
2430  0665 f60000            	ldab	_main_state_c
2431  0668 2604              	bne	L177
2432                         ; 1986 		stW_RemSt_auto_b = FALSE;   // Clear remote start automatic activation bit
2434  066a 1d001001          	bclr	_stW_Flag2_c,1
2436  066e                   L177:
2437                         ; 2018 	if ( (main_state_c != NORMAL) && (stW_lst_main_state_c == NORMAL) ) {
2439  066e f60000            	ldab	_main_state_c
2440  0671 2726              	beq	L1101
2442  0673 f6000f            	ldab	_stW_lst_main_state_c
2443  0676 2621              	bne	L1101
2444                         ; 2021 		if ((main_state_c == REM_STRT_HEATING) || (main_state_c == REM_STRT_USER_SWTCH) ) {
2446  0678 f60000            	ldab	_main_state_c
2447  067b c101              	cmpb	#1
2448  067d 270c              	beq	L5001
2450  067f c103              	cmpb	#3
2451                         ; 2023 			stW_onTime_w = 0;
2453                         ; 2027 			stW_level_c = WL4;
2456  0681 2708              	beq	L5001
2457                         ; 2031 				if ((main_state_c == AUTO_STRT_HEATING) || (main_state_c == AUTO_STRT_USER_SWTCH)){
2459  0683 c104              	cmpb	#4
2460  0685 2704              	beq	L5001
2462  0687 c106              	cmpb	#6
2463  0689 260b              	bne	L3001
2464  068b                   L5001:
2465                         ; 2034 					stW_onTime_w = 0;
2467                         ; 2036 					stW_level_c = WL4;
2469  068b 18790024          	clrw	_stW_onTime_w
2470  068f c604              	ldab	#4
2471  0691 7b002a            	stab	_stW_level_c
2473  0694 2003              	bra	L1101
2474  0696                   L3001:
2475                         ; 2041 					stW_level_c = WL0;
2477  0696 79002a            	clr	_stW_level_c
2478  0699                   L1101:
2479                         ; 2051 	stW_lst_main_state_c = main_state_c;
2481  0699 180c0000000f      	movb	_main_state_c,_stW_lst_main_state_c
2482                         ; 2052 }
2485  069f 0a                	rtc	
2520                         ; 2085 @far void stWF_Chck_Auto_Lvl_Updt(void)
2520                         ; 2086 {
2521                         	switch	.ftext
2522  06a0                   f_stWF_Chck_Auto_Lvl_Updt:
2526                         ; 2092 		if ( ((stW_level_c == WL4) && (stW_onTime_w >= stW_WL4TempChck_lmt_w) && (stW_temperature_c >= stW_prms.cals[stW_veh_offset_c+stW_cfg_c].WL4Temp_Threshld)) || 
2526                         ; 2093 			 ((stW_level_c == WL4) && (stW_RearHeat_Isense_c >= STW_REARSEATB_ISENSE_THRHLD ))  ||
2526                         ; 2094 			 ((stW_level_c == WL4) && (stW_Isense_limit_b == TRUE)) ) {
2528  06a0 f6002a            	ldab	_stW_level_c
2529  06a3 c104              	cmpb	#4
2530  06a5 261f              	bne	L7201
2532  06a7 fc0024            	ldd	_stW_onTime_w
2533  06aa bc0007            	cpd	_stW_WL4TempChck_lmt_w
2534  06ad 2517              	blo	L7201
2536  06af f600f0            	ldab	_stW_cfg_c
2537  06b2 87                	clra	
2538  06b3 fb00ef            	addb	_stW_veh_offset_c
2539  06b6 45                	rola	
2540  06b7 cd0013            	ldy	#19
2541  06ba 13                	emul	
2542  06bb b746              	tfr	d,y
2543  06bd e6ea0038          	ldab	_stW_prms+13,y
2544  06c1 f100f1            	cmpb	_stW_temperature_c
2545  06c4 231a              	bls	L5201
2546  06c6                   L7201:
2548  06c6 f6002a            	ldab	_stW_level_c
2549  06c9 c104              	cmpb	#4
2550  06cb 260a              	bne	L5301
2552  06cd f600f4            	ldab	_stW_RearHeat_Isense_c
2553  06d0 c18c              	cmpb	#140
2554  06d2 240c              	bhs	L5201
2555  06d4 f6002a            	ldab	_stW_level_c
2556  06d7                   L5301:
2558  06d7 c104              	cmpb	#4
2559  06d9 260d              	bne	L3201
2561  06db 1f00100408        	brclr	_stW_Flag2_c,4,L3201
2562  06e0                   L5201:
2563                         ; 2096 			stW_onTime_w = 0;
2565  06e0 18790024          	clrw	_stW_onTime_w
2566                         ; 2099 			stW_level_c = WL3;
2568  06e4 c603              	ldab	#3
2569                         ; 2102 			stWF_Updt_Pwm();
2573  06e6 205b              	bra	LC012
2574  06e8                   L3201:
2575                         ; 2108 			stW_Isense_limit_b = FALSE;
2577  06e8 1d001004          	bclr	_stW_Flag2_c,4
2578                         ; 2111 			if ( (stW_level_c == WL3) && (stW_onTime_w >= stW_WL3TempChck_lmt_w) && (stW_temperature_c >= stW_prms.cals[stW_veh_offset_c+stW_cfg_c].WL3Temp_Threshld) ) {
2580  06ec c103              	cmpb	#3
2581  06ee 2627              	bne	L1401
2583  06f0 fc0024            	ldd	_stW_onTime_w
2584  06f3 bc0009            	cpd	_stW_WL3TempChck_lmt_w
2585  06f6 251f              	blo	L1401
2587  06f8 f600f0            	ldab	_stW_cfg_c
2588  06fb 87                	clra	
2589  06fc fb00ef            	addb	_stW_veh_offset_c
2590  06ff 45                	rola	
2591  0700 cd0013            	ldy	#19
2592  0703 13                	emul	
2593  0704 b746              	tfr	d,y
2594  0706 e6ea0037          	ldab	_stW_prms+12,y
2595  070a f100f1            	cmpb	_stW_temperature_c
2596  070d 2208              	bhi	L1401
2597                         ; 2113 				stW_onTime_w = 0;
2599  070f 18790024          	clrw	_stW_onTime_w
2600                         ; 2116 				stW_level_c = WL2;
2602  0713 c602              	ldab	#2
2603                         ; 2119 				stWF_Updt_Pwm();
2607  0715 202c              	bra	LC012
2608  0717                   L1401:
2609                         ; 2125 				if ( (stW_level_c == WL2) && (stW_onTime_w >= stW_WL2TempChck_lmt_w) && (stW_temperature_c >= stW_prms.cals[stW_veh_offset_c+stW_cfg_c].WL2Temp_Threshld) ) {
2611  0717 f6002a            	ldab	_stW_level_c
2612  071a c102              	cmpb	#2
2613  071c 262c              	bne	L7301
2615  071e fc0024            	ldd	_stW_onTime_w
2616  0721 bc000b            	cpd	_stW_WL2TempChck_lmt_w
2617  0724 2524              	blo	L7301
2619  0726 f600f0            	ldab	_stW_cfg_c
2620  0729 87                	clra	
2621  072a fb00ef            	addb	_stW_veh_offset_c
2622  072d 45                	rola	
2623  072e cd0013            	ldy	#19
2624  0731 13                	emul	
2625  0732 b746              	tfr	d,y
2626  0734 e6ea0036          	ldab	_stW_prms+11,y
2627  0738 f100f1            	cmpb	_stW_temperature_c
2628  073b 220d              	bhi	L7301
2629                         ; 2127 					stW_onTime_w = 0;
2631  073d 18790024          	clrw	_stW_onTime_w
2632                         ; 2130 					stW_level_c = WL1;
2634  0741 c601              	ldab	#1
2635                         ; 2133 					stWF_Updt_Pwm();
2637  0743                   LC012:
2638  0743 7b002a            	stab	_stW_level_c
2639  0746 4a031414          	call	f_stWF_Updt_Pwm
2642  074a                   L7301:
2643                         ; 2158 }
2646  074a 0a                	rtc	
2686                         ; 2243 @far void stWF_Manager(void)
2686                         ; 2244 {
2687                         	switch	.ftext
2688  074b                   f_stWF_Manager:
2692                         ; 2247 	stWLF_Chck_mainState();
2694  074b 4a066060          	call	f_stWLF_Chck_mainState
2696                         ; 2250 	stWF_Updt_Status();
2698  074f 4a03c5c5          	call	f_stWF_Updt_Status
2700                         ; 2253 	stWF_Swtch_Mngmnt();
2702  0753 4a041b1b          	call	f_stWF_Swtch_Mngmnt
2704                         ; 2257 	if ( (diag_io_st_whl_rq_lock_b == FALSE) && (diag_io_st_whl_state_lock_b == FALSE) && (diag_rc_all_op_lock_b == FALSE) )
2706  0757 1e0000801f        	brset	_diag_io_lock_flags_c,128,L1601
2708  075c 1e0000041a        	brset	_diag_io_lock3_flags_c,4,L1601
2710  0761 1e00000115        	brset	_diag_rc_lock_flags_c,1,L1601
2711                         ; 2260 		if (stW_diag_cntrl_act_b == FALSE)
2713  0766 1e00fa100a        	brset	_stW_Flag1_c,16,L3601
2714                         ; 2263 			stWF_Swtch_Cntrl();
2716  076b 4a05b3b3          	call	f_stWF_Swtch_Cntrl
2718                         ; 2266 			stWLF_RemSt_Chck();
2720  076f 4a062121          	call	f_stWLF_RemSt_Chck
2723  0773 200a              	bra	L7601
2724  0775                   L3601:
2725                         ; 2273 			stWF_Rtrn_Cntrl_toECU();
2727  0775 4a05a6a6          	call	f_stWF_Rtrn_Cntrl_toECU
2729  0779 2004              	bra	L7601
2730  077b                   L1601:
2731                         ; 2279 		stWF_Diag_IO_Cntrl();
2733  077b 4a05e3e3          	call	f_stWF_Diag_IO_Cntrl
2735  077f                   L7601:
2736                         ; 2283 	stWF_Chck_Auto_Lvl_Updt();
2738  077f 4a06a0a0          	call	f_stWF_Chck_Auto_Lvl_Updt
2740                         ; 2286 	if (stW_status_c != GOOD)
2742  0783 f60029            	ldab	_stW_status_c
2743  0786 53                	decb	
2744                         ; 2289 		stW_sw_rq_b = FALSE;
2747  0787 260f              	bne	L5701
2748                         ; 2294 		if (stW_swtch_rq_prps_b == TRUE)
2750  0789 1f00fa080a        	brclr	_stW_Flag1_c,8,L5701
2751                         ; 2297 			stW_sw_rq_b = TRUE;
2753  078e 1c00fa04          	bset	_stW_Flag1_c,4
2754                         ; 2302 			stWF_Updt_Pwm();
2756  0792 4a031414          	call	f_stWF_Updt_Pwm
2759  0796 2004              	bra	L3701
2760  0798                   L5701:
2761                         ; 2307 			stW_sw_rq_b = FALSE;
2763  0798 1d00fa04          	bclr	_stW_Flag1_c,4
2764  079c                   L3701:
2765                         ; 2312 	stWF_LED_Display();
2767  079c 4a04a2a2          	call	f_stWF_LED_Display
2769                         ; 2315 	stW_RearHeat_Isense_c = stW_Isense_c + hs_Isense_c[REAR_LEFT]+hs_Isense_c[REAR_RIGHT];
2771  07a0 f600f7            	ldab	_stW_Isense_c
2772  07a3 fb0002            	addb	_hs_Isense_c+2
2773  07a6 fb0003            	addb	_hs_Isense_c+3
2774  07a9 7b00f4            	stab	_stW_RearHeat_Isense_c
2775                         ; 2316 }
2778  07ac 0a                	rtc	
2865                         ; 2340 @far void stWF_Updt_Flt_Stat(stW_Flt_Type_e Wheel_Fault)
2865                         ; 2341 {
2866                         	switch	.ftext
2867  07ad                   f_stWF_Updt_Flt_Stat:
2869  07ad 3b                	pshd	
2870       00000000          OFST:	set	0
2873                         ; 2343 	if (stW_Flt_Mtr_cnt < (stW_Flt_Mtr_Tm-1))
2875  07ae fd0013            	ldy	_stW_Flt_Mtr_Tm
2876  07b1 03                	dey	
2877  07b2 bd0020            	cpy	_stW_Flt_Mtr_cnt
2878  07b5 2342              	bls	L3711
2879                         ; 2346 		stW_Flt_Mtr_cnt++;
2881  07b7 18720020          	incw	_stW_Flt_Mtr_cnt
2882                         ; 2349 		switch (Wheel_Fault) {
2885  07bb 87                	clra	
2886  07bc c106              	cmpb	#6
2887  07be 2433              	bhs	L5111
2888  07c0 59                	lsld	
2889  07c1 05ff              	jmp	[d,pc]
2890  07c3 07cf              	dc.w	L1011
2891  07c5 07d5              	dc.w	L3011
2892  07c7 07e1              	dc.w	L7011
2893  07c9 07db              	dc.w	L5011
2894  07cb 07e7              	dc.w	L1111
2895  07cd 07ed              	dc.w	L3111
2896  07cf                   L1011:
2897                         ; 2354 				stW_Flt_status_w |= STW_PRLM_FULL_OPEN_MASK;
2899  07cf 1c00f301          	bset	_stW_Flt_status_w+1,1
2900                         ; 2355 				break;
2901  07d3                   L1021:
2902                         ; 2459 }
2905  07d3 31                	puly	
2906  07d4 0a                	rtc	
2907  07d5                   L3011:
2908                         ; 2361 				stW_Flt_status_w |= STW_PRLM_PARTIAL_OPEN_MASK;
2910  07d5 1c00f302          	bset	_stW_Flt_status_w+1,2
2911                         ; 2362 				break;
2913  07d9 20f8              	bra	L1021
2914  07db                   L5011:
2915                         ; 2368 				stW_Flt_status_w |= STW_PRLM_GND_MASK;
2917  07db 1c00f304          	bset	_stW_Flt_status_w+1,4
2918                         ; 2369 				break;
2920  07df 20f2              	bra	L1021
2921  07e1                   L7011:
2922                         ; 2375 				stW_Flt_status_w |= STW_PRLM_BATT_MASK;
2924  07e1 1c00f308          	bset	_stW_Flt_status_w+1,8
2925                         ; 2376 				break;
2927  07e5 20ec              	bra	L1021
2928  07e7                   L1111:
2929                         ; 2382 				stW_Flt_status_w |= STW_PRLM_IGN_WHEEL_MASK;
2931  07e7 1c00f310          	bset	_stW_Flt_status_w+1,16
2932                         ; 2383 				break;
2934  07eb 20e6              	bra	L1021
2935  07ed                   L3111:
2936                         ; 2389 				stW_Flt_status_w |= STW_PRELIM_TEMP_SENSOR_FLT;
2938  07ed 1c00f320          	bset	_stW_Flt_status_w+1,32
2939                         ; 2390 				break;
2941  07f1 20e0              	bra	L1021
2942  07f3                   L5111:
2943                         ; 2396 				stW_Flt_status_w = STW_NO_FLT_MASK;
2945  07f3 187900f2          	clrw	_stW_Flt_status_w
2946  07f7 20da              	bra	L1021
2948  07f9                   L3711:
2949                         ; 2403 		switch (Wheel_Fault) {
2952  07f9 87                	clra	
2953  07fa c106              	cmpb	#6
2954  07fc 2434              	bhs	L3311
2955  07fe 59                	lsld	
2956  07ff 05ff              	jmp	[d,pc]
2957  0801 080d              	dc.w	L7111
2958  0803 0816              	dc.w	L1211
2959  0805 0820              	dc.w	L5211
2960  0807 081b              	dc.w	L3211
2961  0809 0825              	dc.w	L7211
2962  080b 082a              	dc.w	L1311
2963  080d                   L7111:
2964                         ; 2409 				stW_Open_or_STB_b = TRUE;
2966  080d 1c001002          	bset	_stW_Flag2_c,2
2967                         ; 2411 				stW_Flt_status_w = STW_MTRD_FULL_OPEN_MASK;	
2969  0811 cc0040            	ldd	#64
2970                         ; 2412 				break;
2972  0814 2017              	bra	LC014
2973  0816                   L1211:
2974                         ; 2418 				stW_Flt_status_w = STW_MTRD_PARTIAL_OPEN_MASK;
2976  0816 cc0080            	ldd	#128
2977                         ; 2419 				break;
2979  0819 2012              	bra	LC014
2980  081b                   L3211:
2981                         ; 2425 				stW_Flt_status_w = STW_MTRD_GND_MASK;
2983  081b cc0100            	ldd	#256
2984                         ; 2426 				break;
2986  081e 200d              	bra	LC014
2987  0820                   L5211:
2988                         ; 2432 				stW_Flt_status_w = STW_MTRD_BATT_MASK;
2990  0820 cc0200            	ldd	#512
2991                         ; 2433 				break;
2993  0823 2008              	bra	LC014
2994  0825                   L7211:
2995                         ; 2439 				stW_Flt_status_w = STW_MTRD_IGN_WHEEL_MASK;
2997  0825 cc0400            	ldd	#1024
2998                         ; 2440 				break;
3000  0828 2003              	bra	LC014
3001  082a                   L1311:
3002                         ; 2446 				stW_Flt_status_w = STW_MTRD_TEMP_SENS_FLT_MASK;
3004  082a cc0800            	ldd	#2048
3005  082d                   LC014:
3006  082d 7c00f2            	std	_stW_Flt_status_w
3007                         ; 2447 				break;
3009  0830 2004              	bra	L5021
3010  0832                   L3311:
3011                         ; 2453 				stW_Flt_status_w = STW_NO_FLT_MASK;
3013  0832 187900f2          	clrw	_stW_Flt_status_w
3014  0836                   L5021:
3015                         ; 2457 		stW_Flt_Mtr_cnt = 0;
3017  0836 18790020          	clrw	_stW_Flt_Mtr_cnt
3018  083a 2097              	bra	L1021
3071                         ; 2560 @far void stWF_Flt_Mntr(void)
3071                         ; 2561 {
3072                         	switch	.ftext
3073  083c                   f_stWF_Flt_Mntr:
3075  083c 3b                	pshd	
3076       00000002          OFST:	set	2
3079                         ; 2563 	u_16Bit stW_prv_flt_typ = stW_Flt_status_w;     // stores the previous fault type
3081  083d 18018000f2        	movw	_stW_Flt_status_w,OFST-2,s
3082                         ; 2566 	stW_Ign_stat = (u_8Bit) Dio_ReadChannel(IGN_WHEEL_MON);
3084  0842 e6fbf7ba          	ldab	[_Dio_PortRead_Ptr]
3085  0846 55                	rolb	
3086  0847 55                	rolb	
3087  0848 55                	rolb	
3088  0849 c401              	andb	#1
3089  084b 7b0017            	stab	_stW_Ign_stat
3090                         ; 2571 	if ( (stW_Ign_stat == STW_IGN_STAT_ON) &&
3090                         ; 2572 		 (stW_Flt_status_w != STW_MTRD_IGN_WHEEL_MASK) )
3092  084e 53                	decb	
3093  084f 1826015e          	bne	L3221
3095  0853 fc00f2            	ldd	_stW_Flt_status_w
3096  0856 8c0400            	cpd	#1024
3097  0859 18270154          	beq	L3221
3098                         ; 2575 		stW_Flt_status_w &= STW_CLR_PRLM_IGN_WHEEL;
3100  085d 1d00f310          	bclr	_stW_Flt_status_w+1,16
3101                         ; 2578 		if ((stW_tmprtr_sensor_flt_c == STW_TEMPERATURE_SENSOR_OK) &&
3101                         ; 2579 			(stW_Flt_status_w != STW_MTRD_TEMP_SENS_FLT_MASK))
3103  0861 f60019            	ldab	_stW_tmprtr_sensor_flt_c
3104  0864 1826010b          	bne	L5221
3106  0868 fc00f2            	ldd	_stW_Flt_status_w
3107  086b 8c0800            	cpd	#2048
3108  086e 18270101          	beq	L5221
3109                         ; 2582 			stW_Flt_status_w &= STW_CLR_PRLM_TEMP_SENSOR;
3111  0872 1d00f320          	bclr	_stW_Flt_status_w+1,32
3112                         ; 2584 			if ( (stW_Flt_status_w != STW_MTRD_BATT_MASK) &&
3112                         ; 2585 				 (relay_C_phase1_Test_Voltage_OFF_fault_b == FALSE) )
3114  0876 fc00f2            	ldd	_stW_Flt_status_w
3115  0879 8c0200            	cpd	#512
3116  087c 182700e9          	beq	L7221
3118  0880 f60000            	ldab	_relay_status2_c
3119  0883 c540              	bitb	#64
3120  0885 182600e0          	bne	L7221
3121                         ; 2588 				stW_Flt_status_w &= STW_CLR_PRLM_BATT;
3123  0889 1d00f308          	bclr	_stW_Flt_status_w+1,8
3124                         ; 2591 				if ( (main_CSWM_Status_c == GOOD) &&
3124                         ; 2592 					 (relay_C_internal_Fault_b == FALSE) )
3126  088d f60000            	ldab	_main_CSWM_Status_c
3127  0890 53                	decb	
3128  0891 182600b8          	bne	L1321
3130  0895 f60000            	ldab	_relay_fault_status2_c
3131  0898 c501              	bitb	#1
3132  089a 182600af          	bne	L1321
3133                         ; 2595 					if (stW_state_b == TRUE)
3135  089e 1f00fa0141        	brclr	_stW_Flag1_c,1,L3321
3136                         ; 2598 						if (stW_Isense_dly_cnt_c < STW_ISENSE_DELAY)
3138  08a3 f60016            	ldab	_stW_Isense_dly_cnt_c
3139  08a6 c10f              	cmpb	#15
3140  08a8 2407              	bhs	L5321
3141                         ; 2601 							stW_Isense_dly_cnt_c++;
3143  08aa 720016            	inc	_stW_Isense_dly_cnt_c
3145  08ad 1820010f          	bra	L5431
3146  08b1                   L5321:
3147                         ; 2606 							if ((stW_Vsense_HS_c < stW_flt_calibs_c[STW_SHRT_TO_GND_VSENSE]) ||
3147                         ; 2607 								(stW_Isense_c > stW_flt_calibs_c[STW_OVR_CURRENT_ISENSE]) )
3149  08b1 f600f9            	ldab	_stW_Vsense_HS_c
3150  08b4 f100ec            	cmpb	_stW_flt_calibs_c+3
3151  08b7 2508              	blo	L3421
3153  08b9 f600f7            	ldab	_stW_Isense_c
3154  08bc f100eb            	cmpb	_stW_flt_calibs_c+2
3155  08bf 2308              	bls	L1421
3156  08c1                   L3421:
3157                         ; 2610 								stW_Open_or_STB_b = FALSE;
3159  08c1 1d001002          	bclr	_stW_Flag2_c,2
3160                         ; 2612 								stWF_Updt_Flt_Stat(STW_SHRT_TO_GND);
3164  08c5 18200091          	bra	LC016
3165  08c9                   L1421:
3166                         ; 2617 								stW_Flt_status_w &= STW_CLR_PRLM_GND;
3168  08c9 1d00f304          	bclr	_stW_Flt_status_w+1,4
3169                         ; 2620 								if (stW_Isense_c < stW_flt_calibs_c[STW_FULL_OPEN_ISENSE])
3171  08cd f100ea            	cmpb	_stW_flt_calibs_c+1
3172  08d0 2406              	bhs	L7421
3173                         ; 2623 									stWF_Updt_Flt_Stat(STW_FULL_OPEN);
3175  08d2 87                	clra	
3176  08d3 c7                	clrb	
3179  08d4 182000e4          	bra	LC015
3180  08d8                   L7421:
3181                         ; 2628 									stW_Flt_status_w = STW_NO_FLT_MASK;
3183  08d8 187900f2          	clrw	_stW_Flt_status_w
3184                         ; 2630 									stW_Open_or_STB_b = FALSE;										
3186  08dc 1d001002          	bclr	_stW_Flag2_c,2
3187  08e0 182000dc          	bra	L5431
3188  08e4                   L3321:
3189                         ; 2638 						if ( (relay_C_phase1_Test_Voltage_ON_fault_b == TRUE) && (stW_Flt_status_w != STW_MTRD_GND_MASK) )
3191  08e4 1f00002008        	brclr	_relay_status2_c,32,L5521
3193  08e9 fc00f2            	ldd	_stW_Flt_status_w
3194  08ec 8c0100            	cpd	#256
3195                         ; 2641 							stWF_Updt_Flt_Stat(STW_SHRT_TO_GND);
3199  08ef 2669              	bne	LC016
3200  08f1                   L5521:
3201                         ; 2646 							stW_Flt_status_w &= STW_CLR_PRLM_GND;
3203  08f1 1d00f304          	bclr	_stW_Flt_status_w+1,4
3204                         ; 2649 							if(stW_chck_Open_or_STB_dly_c >= STW_OPEN_or_STB_DELAY){
3206  08f5 f60015            	ldab	_stW_chck_Open_or_STB_dly_c
3207  08f8 c107              	cmpb	#7
3208  08fa 254c              	blo	L1621
3209                         ; 2652 								if( (stW_Open_or_STB_b == TRUE) && (stW_Vsense_HS_c > stW_flt_calibs_c[STW_SHRT_TO_BATT_VSENSE]) ){
3211  08fc 1f0010021d        	brclr	_stW_Flag2_c,2,L3621
3213  0901 f600f9            	ldab	_stW_Vsense_HS_c
3214  0904 f100ed            	cmpb	_stW_flt_calibs_c+4
3215  0907 2315              	bls	L3621
3216                         ; 2654 									stW_Flt_Mtr_cnt = stW_Flt_Mtr_Tm;
3218  0909 180400130020      	movw	_stW_Flt_Mtr_Tm,_stW_Flt_Mtr_cnt
3219                         ; 2656 									stWF_Updt_Flt_Stat(STW_SHRT_TO_BATT);
3221  090f cc0002            	ldd	#2
3222  0912 4a07adad          	call	f_stWF_Updt_Flt_Stat
3224                         ; 2658 									stW_Open_or_STB_b = FALSE;
3226  0916 1d001002          	bclr	_stW_Flag2_c,2
3228  091a 182000a2          	bra	L5431
3229  091e                   L3621:
3230                         ; 2662 									if(stW_Open_or_STB_b == TRUE){													
3232  091e 1f00100214        	brclr	_stW_Flag2_c,2,L7621
3233                         ; 2664 										stW_Flt_Mtr_cnt = stW_Flt_Mtr_Tm;
3235  0923 180400130020      	movw	_stW_Flt_Mtr_Tm,_stW_Flt_Mtr_cnt
3236                         ; 2666 										stWF_Updt_Flt_Stat(STW_FULL_OPEN);
3238  0929 87                	clra	
3239  092a c7                	clrb	
3240  092b 4a07adad          	call	f_stWF_Updt_Flt_Stat
3242                         ; 2668 										stW_Open_or_STB_b = FALSE;	
3244  092f 1d001002          	bclr	_stW_Flag2_c,2
3246  0933 18200089          	bra	L5431
3247  0937                   L7621:
3248                         ; 2671 										stW_Flt_status_w &= STW_CLR_PRLM_FULL_OPEN;
3250                         ; 2673 										stW_Flt_status_w &= STW_CLR_PRLM_PARTIAL_OPEN;
3252  0937 1d00f303          	bclr	_stW_Flt_status_w+1,3
3253                         ; 2675 										if(stW_Vsense_HS_c > stW_flt_calibs_c[STW_SHRT_TO_BATT_VSENSE]){
3255  093b f600f9            	ldab	_stW_Vsense_HS_c
3256  093e f100ed            	cmpb	_stW_flt_calibs_c+4
3257  0941 237d              	bls	L5431
3258                         ; 2677 											stWF_Updt_Flt_Stat(STW_SHRT_TO_BATT);
3260  0943                   LC017:
3261  0943 cc0002            	ldd	#2
3264  0946 2074              	bra	LC015
3265  0948                   L1621:
3266                         ; 2686 								stW_chck_Open_or_STB_dly_c++;
3268  0948 720015            	inc	_stW_chck_Open_or_STB_dly_c
3269  094b 2073              	bra	L5431
3270  094d                   L1321:
3271                         ; 2692 					if( (relay_C_internal_Fault_b == TRUE) && (stW_Flt_status_w != STW_MTRD_GND_MASK) ){
3273  094d 1f0000010d        	brclr	_relay_fault_status2_c,1,L3031
3275  0952 fc00f2            	ldd	_stW_Flt_status_w
3276  0955 8c0100            	cpd	#256
3277  0958 2705              	beq	L3031
3278                         ; 2694 						stWF_Updt_Flt_Stat(STW_SHRT_TO_GND);					
3280  095a                   LC016:
3281  095a cc0003            	ldd	#3
3284  095d 205d              	bra	LC015
3285  095f                   L3031:
3286                         ; 2697 						stW_Flt_status_w &= STW_CLR_PRLM_GND;
3288  095f 1d00f304          	bclr	_stW_Flt_status_w+1,4
3289                         ; 2699 						stW_Flt_Mtr_cnt = 0;
3291  0963 18790020          	clrw	_stW_Flt_Mtr_cnt
3292  0967 2057              	bra	L5431
3293  0969                   L7221:
3294                         ; 2706 				if(stW_Flt_status_w != STW_MTRD_BATT_MASK){
3296  0969 fc00f2            	ldd	_stW_Flt_status_w
3297  096c 8c0200            	cpd	#512
3298  096f 274f              	beq	L5431
3299                         ; 2708 					stWF_Updt_Flt_Stat(STW_SHRT_TO_BATT);						
3303  0971 20d0              	bra	LC017
3304  0973                   L5221:
3305                         ; 2717 			if (stW_Flt_status_w != STW_MTRD_TEMP_SENS_FLT_MASK)
3307  0973 fc00f2            	ldd	_stW_Flt_status_w
3308  0976 8c0800            	cpd	#2048
3309  0979 2745              	beq	L5431
3310                         ; 2720 				if((main_SwReq_Front_c == VEH_BUS_CCN_FRONT_REAR_K) || (main_SwReq_Rear_c == VEH_BUS_DM_REAR_K)){
3312  097b f60000            	ldab	_main_SwReq_Front_c
3313  097e 040107            	dbeq	b,L3231
3315  0981 f60000            	ldab	_main_SwReq_Rear_c
3316  0984 c105              	cmpb	#5
3317  0986 2624              	bne	L1231
3318  0988                   L3231:
3319                         ; 2722 					if ((stW_tmprtr_sensor_flt_c == STW_TEMPERATURE_SENSOR_SHRT))
3321  0988 f60019            	ldab	_stW_tmprtr_sensor_flt_c
3322  098b 042119            	dbne	b,L5231
3323                         ; 2726 						if (canio_RX_ExternalTemperature_c < STW_SNSR_FLT_AMBIENT_LMT)
3325  098e f60000            	ldab	_canio_RX_ExternalTemperature_c
3326  0991 c15a              	cmpb	#90
3327  0993 240d              	bhs	L7231
3328                         ; 2729 							if (stW_onTime_w > stW_ontimeLmt_w)
3330  0995 fc0024            	ldd	_stW_onTime_w
3331  0998 bc0005            	cpd	_stW_ontimeLmt_w
3332  099b 2323              	bls	L5431
3333                         ; 2732 								stWF_Updt_Flt_Stat(STW_TEMP_SENSOR_FLT);
3335  099d cc0005            	ldd	#5
3338  09a0 201a              	bra	LC015
3339  09a2                   L7231:
3340                         ; 2743 							stWF_Updt_Flt_Stat(STW_TEMP_SENSOR_FLT);
3342  09a2 cc0005            	ldd	#5
3344  09a5 2015              	bra	LC015
3345  09a7                   L5231:
3346                         ; 2750 						stWF_Updt_Flt_Stat(STW_TEMP_SENSOR_FLT);
3348  09a7 cc0005            	ldd	#5
3350  09aa 2010              	bra	LC015
3351  09ac                   L1231:
3352                         ; 2755 					stWF_Updt_Flt_Stat(STW_TEMP_SENSOR_FLT);						
3354  09ac cc0005            	ldd	#5
3356  09af 200b              	bra	LC015
3357  09b1                   L3221:
3358                         ; 2767 		if (stW_Flt_status_w != STW_MTRD_IGN_WHEEL_MASK)
3360  09b1 fc00f2            	ldd	_stW_Flt_status_w
3361  09b4 8c0400            	cpd	#1024
3362  09b7 2707              	beq	L5431
3363                         ; 2770 			stWF_Updt_Flt_Stat(STW_IGN_WHEEL);
3365  09b9 cc0004            	ldd	#4
3366  09bc                   LC015:
3367  09bc 4a07adad          	call	f_stWF_Updt_Flt_Stat
3370  09c0                   L5431:
3371                         ; 2779 	if ( (stW_prv_flt_typ != stW_Flt_status_w) ||
3371                         ; 2780 		 (stW_Flt_status_w == STW_NO_FLT_MASK) )
3373  09c0 ec80              	ldd	OFST-2,s
3374  09c2 bc00f2            	cpd	_stW_Flt_status_w
3375  09c5 2605              	bne	L5531
3377  09c7 fc00f2            	ldd	_stW_Flt_status_w
3378  09ca 2607              	bne	L3531
3379  09cc                   L5531:
3380                         ; 2782 		stW_Flt_Mtr_cnt = 0;
3382  09cc 18790020          	clrw	_stW_Flt_Mtr_cnt
3383  09d0 f600f3            	ldab	_stW_Flt_status_w+1
3384  09d3                   L3531:
3385                         ; 2788 	if( ( (stW_Flt_status_w & STW_PRLM_FULL_OPEN_MASK) != STW_PRLM_FULL_OPEN_MASK) && 
3385                         ; 2789 		( (stW_Flt_status_w & STW_MTRD_FULL_OPEN_MASK) != STW_MTRD_FULL_OPEN_MASK) &&
3385                         ; 2790 		(stW_Open_or_STB_b == TRUE) ){
3387  09d3 c401              	andb	#1
3388  09d5 87                	clra	
3389  09d6 8c0001            	cpd	#1
3390  09d9 2713              	beq	L7531
3392  09db f600f3            	ldab	_stW_Flt_status_w+1
3393  09de c440              	andb	#64
3394  09e0 8c0040            	cpd	#64
3395  09e3 2709              	beq	L7531
3397  09e5 1f00100204        	brclr	_stW_Flag2_c,2,L7531
3398                         ; 2792 		stW_Open_or_STB_b = FALSE;
3400  09ea 1d001002          	bclr	_stW_Flag2_c,2
3401  09ee                   L7531:
3402                         ; 2794 }
3405  09ee 31                	puly	
3406  09ef 0a                	rtc	
3431                         ; 2813 @far void stWF_DTC_Manager(void)
3431                         ; 2814 {
3432                         	switch	.ftext
3433  09f0                   f_stWF_DTC_Manager:
3437                         ; 2816 	if ( (stW_Flt_status_w & STW_MTRD_FULL_OPEN_MASK) == STW_MTRD_FULL_OPEN_MASK)
3439  09f0 f600f3            	ldab	_stW_Flt_status_w+1
3440  09f3 c440              	andb	#64
3441  09f5 87                	clra	
3442  09f6 8c0040            	cpd	#64
3443  09f9 2609              	bne	L1731
3444                         ; 2819 		if(stW_Open_or_STB_b == FALSE){
3446  09fb 1e00100212        	brset	_stW_Flag2_c,2,L5731
3447                         ; 2820 			FMemLibF_ReportDtc(DTC_HSW_OPEN_FULL_K, ERROR_ON_K);
3449  0a00 c601              	ldab	#1
3451  0a02 2001              	bra	LC018
3452  0a04                   L1731:
3453                         ; 2826 		FMemLibF_ReportDtc(DTC_HSW_OPEN_FULL_K, ERROR_OFF_K);
3455  0a04 c7                	clrb	
3457  0a05                   LC018:
3458  0a05 3b                	pshd	
3459  0a06 ce0090            	ldx	#144
3460  0a09 ccc413            	ldd	#-15341
3461  0a0c 4a000000          	call	f_FMemLibF_ReportDtc
3462  0a10 1b82              	leas	2,s
3463  0a12                   L5731:
3464                         ; 2843 	if ( (stW_Flt_status_w & STW_MTRD_GND_MASK) == STW_MTRD_GND_MASK)
3466  0a12 b600f2            	ldaa	_stW_Flt_status_w
3467  0a15 8401              	anda	#1
3468  0a17 c7                	clrb	
3469  0a18 8c0100            	cpd	#256
3470  0a1b 2605              	bne	L7731
3471                         ; 2846 		FMemLibF_ReportDtc(DTC_HSW_SHORT_TO_GND_K, ERROR_ON_K);
3473  0a1d cc0001            	ldd	#1
3476  0a20 2001              	bra	L1041
3477  0a22                   L7731:
3478                         ; 2851 		FMemLibF_ReportDtc(DTC_HSW_SHORT_TO_GND_K, ERROR_OFF_K);
3480  0a22 87                	clra	
3482  0a23                   L1041:
3483  0a23 3b                	pshd	
3484  0a24 ce0090            	ldx	#144
3485  0a27 ccc411            	ldd	#-15343
3486  0a2a 4a000000          	call	f_FMemLibF_ReportDtc
3487  0a2e 1b82              	leas	2,s
3488                         ; 2855 	if ( (stW_Flt_status_w & STW_MTRD_BATT_MASK) == STW_MTRD_BATT_MASK)
3490  0a30 b600f2            	ldaa	_stW_Flt_status_w
3491  0a33 8402              	anda	#2
3492  0a35 c7                	clrb	
3493  0a36 8c0200            	cpd	#512
3494  0a39 2605              	bne	L3041
3495                         ; 2858 		FMemLibF_ReportDtc(DTC_HSW_SHORT_TO_BAT_K, ERROR_ON_K);
3497  0a3b cc0001            	ldd	#1
3500  0a3e 2001              	bra	L5041
3501  0a40                   L3041:
3502                         ; 2863 		FMemLibF_ReportDtc(DTC_HSW_SHORT_TO_BAT_K, ERROR_OFF_K);
3504  0a40 87                	clra	
3506  0a41                   L5041:
3507  0a41 3b                	pshd	
3508  0a42 ce0090            	ldx	#144
3509  0a45 ccc412            	ldd	#-15342
3510  0a48 4a000000          	call	f_FMemLibF_ReportDtc
3511  0a4c 1b82              	leas	2,s
3512                         ; 2867 	if ( (stW_Flt_status_w & STW_MTRD_IGN_WHEEL_MASK) == STW_MTRD_IGN_WHEEL_MASK)
3514  0a4e b600f2            	ldaa	_stW_Flt_status_w
3515  0a51 8404              	anda	#4
3516  0a53 c7                	clrb	
3517  0a54 8c0400            	cpd	#1024
3518  0a57 2605              	bne	L7041
3519                         ; 2870 		FMemLibF_ReportDtc(DTC_NO_BAT_FOR_STEERING_WHEEL_K, ERROR_ON_K);
3521  0a59 cc0001            	ldd	#1
3524  0a5c 2001              	bra	L1141
3525  0a5e                   L7041:
3526                         ; 2875 		FMemLibF_ReportDtc(DTC_NO_BAT_FOR_STEERING_WHEEL_K, ERROR_OFF_K);
3528  0a5e 87                	clra	
3530  0a5f                   L1141:
3531  0a5f 3b                	pshd	
3532  0a60 ce0091            	ldx	#145
3533  0a63 ccc113            	ldd	#-16109
3534  0a66 4a000000          	call	f_FMemLibF_ReportDtc
3535  0a6a 1b82              	leas	2,s
3536                         ; 2879 	if ( (stW_Flt_status_w & STW_MTRD_TEMP_SENS_FLT_MASK) == STW_MTRD_TEMP_SENS_FLT_MASK)
3538  0a6c b600f2            	ldaa	_stW_Flt_status_w
3539  0a6f 8408              	anda	#8
3540  0a71 c7                	clrb	
3541  0a72 8c0800            	cpd	#2048
3542  0a75 260c              	bne	L3141
3543                         ; 2882 		FMemLibF_ReportDtc(DTC_HSW_TEMP_SENS_FLT_SNA_K, ERROR_ON_K);
3545  0a77 cc0001            	ldd	#1
3546  0a7a 3b                	pshd	
3547  0a7b ce00d4            	ldx	#212
3548  0a7e cc4600            	ldd	#17920
3551  0a81 2007              	bra	L5141
3552  0a83                   L3141:
3553                         ; 2887 		FMemLibF_ReportDtc(DTC_HSW_TEMP_SENS_FLT_SNA_K, ERROR_OFF_K);
3555  0a83 87                	clra	
3556  0a84 3b                	pshd	
3557  0a85 ce00d4            	ldx	#212
3558  0a88 8646              	ldaa	#70
3560  0a8a                   L5141:
3561  0a8a 4a000000          	call	f_FMemLibF_ReportDtc
3562  0a8e 1b82              	leas	2,s
3563                         ; 2889 }
3566  0a90 0a                	rtc	
3637                         ; 2943 @far u_8Bit stWF_Chck_Diag_Prms(u_8Bit stw_LID_serv, u_8Bit *stw_data_p)
3637                         ; 2944 {
3638                         	switch	.ftext
3639  0a91                   f_stWF_Chck_Diag_Prms:
3641  0a91 3b                	pshd	
3642  0a92 1b9c              	leas	-4,s
3643       00000004          OFST:	set	4
3646                         ; 2948 	u_8Bit stW_chck_rslt = FALSE;     // check result
3648  0a94 87                	clra	
3649  0a95 6a82              	staa	OFST-2,s
3650                         ; 2950 	stW_LID_service = stw_LID_serv;   // store the LID service 
3652  0a97 6b83              	stab	OFST-1,s
3653                         ; 2951 	stW_data_p_c = stw_data_p;        // point to the data that needs to be checked
3655  0a99 18028980          	movw	OFST+5,s,OFST-4,s
3656                         ; 2953 	switch (stW_LID_service) {
3659  0a9d c003              	subb	#3
3660  0a9f 2719              	beq	L7141
3661  0aa1 53                	decb	
3662  0aa2 277b              	beq	L1241
3663  0aa4 c007              	subb	#7
3664  0aa6 182700c4          	beq	L3241
3665  0aaa c002              	subb	#2
3666  0aac 18270122          	beq	L5241
3667  0ab0 c003              	subb	#3
3668  0ab2 1827015f          	beq	L7241
3669                         ; 3130 			stW_chck_rslt = FALSE;
3671  0ab6 182000e2          	bra	L1251
3672  0aba                   L7141:
3673                         ; 2962 		if ( ( *stW_data_p_c < 10 || *stW_data_p_c > 100 ) ||
3673                         ; 2963 		   ( ( *(stW_data_p_c + 1) < 10 || *(stW_data_p_c + 1) > 100) ) ||
3673                         ; 2964 		   ( ( *(stW_data_p_c + 2) < 10 || *(stW_data_p_c + 2) > 100) ) ||
3673                         ; 2965 		   ( ( *(stW_data_p_c + 3) < 10 || *(stW_data_p_c + 3) > 100) ) ) {
3675  0aba ed80              	ldy	OFST-4,s
3676  0abc e640              	ldab	0,y
3677  0abe c10a              	cmpb	#10
3678  0ac0 182500d8          	blo	L1251
3680  0ac4 c164              	cmpb	#100
3681  0ac6 182200d2          	bhi	L1251
3683  0aca e641              	ldab	1,y
3684  0acc c10a              	cmpb	#10
3685  0ace 182500ca          	blo	L1251
3687  0ad2 c164              	cmpb	#100
3688  0ad4 182200c4          	bhi	L1251
3690  0ad8 e642              	ldab	2,y
3691  0ada c10a              	cmpb	#10
3692  0adc 182500bc          	blo	L1251
3694  0ae0 c164              	cmpb	#100
3695  0ae2 182200b6          	bhi	L1251
3697  0ae6 e643              	ldab	3,y
3698  0ae8 c10a              	cmpb	#10
3699  0aea 182500ae          	blo	L1251
3701  0aee c164              	cmpb	#100
3702                         ; 2968 			stW_chck_rslt = FALSE;
3705  0af0 182200a8          	bhi	L1251
3706                         ; 2974 			stW_prms.cals[stW_veh_offset_c+stW_cfg_c].Pwm_a[WL1] = *stW_data_p_c;
3708  0af4 f600f0            	ldab	_stW_cfg_c
3709  0af7 fb00ef            	addb	_stW_veh_offset_c
3710  0afa 45                	rola	
3711  0afb cd0013            	ldy	#19
3712  0afe 13                	emul	
3713  0aff b746              	tfr	d,y
3714  0b01 ee80              	ldx	OFST-4,s
3715  0b03 180a00ea0031      	movb	0,x,_stW_prms+6,y
3716                         ; 2975 			stW_prms.cals[stW_veh_offset_c+stW_cfg_c].Pwm_a[WL2] = *(stW_data_p_c + 1);
3718  0b09 180a01ea0032      	movb	1,x,_stW_prms+7,y
3719                         ; 2976 			stW_prms.cals[stW_veh_offset_c+stW_cfg_c].Pwm_a[WL3] = *(stW_data_p_c + 2);
3721  0b0f 180a02ea0033      	movb	2,x,_stW_prms+8,y
3722                         ; 2977 			stW_prms.cals[stW_veh_offset_c+stW_cfg_c].Pwm_a[WL4] = *(stW_data_p_c + 3);
3724  0b15 180a03ea0034      	movb	3,x,_stW_prms+9,y
3725                         ; 2981 			stW_chck_rslt = TRUE;
3727  0b1b 18200147          	bra	LC019
3728  0b1f                   L1241:
3729                         ; 2993 		if ( ( *stW_data_p_c < 1 || *stW_data_p_c > 60 ) ||
3729                         ; 2994 		   ( ( *(stW_data_p_c + 1) < 1 || *(stW_data_p_c + 1) > 60) ) ||
3729                         ; 2995 		   ( ( *(stW_data_p_c + 2) < 1 || *(stW_data_p_c + 2) > 60) ) ||
3729                         ; 2996 		   ( ( *(stW_data_p_c + 3) < 1 || *(stW_data_p_c + 3) > 60) )) {
3731  0b1f ee80              	ldx	OFST-4,s
3732  0b21 e600              	ldab	0,x
3733  0b23 2777              	beq	L1251
3735  0b25 b756              	tfr	x,y
3736  0b27 e640              	ldab	0,y
3737  0b29 c13c              	cmpb	#60
3738  0b2b 226f              	bhi	L1251
3740  0b2d e641              	ldab	1,y
3741  0b2f 276b              	beq	L1251
3743  0b31 c13c              	cmpb	#60
3744  0b33 2267              	bhi	L1251
3746  0b35 e642              	ldab	2,y
3747  0b37 2763              	beq	L1251
3749  0b39 c13c              	cmpb	#60
3750  0b3b 225f              	bhi	L1251
3752  0b3d e643              	ldab	3,y
3753  0b3f 275b              	beq	L1251
3755  0b41 c13c              	cmpb	#60
3756                         ; 2999 			stW_chck_rslt = FALSE;
3759  0b43 2257              	bhi	L1251
3760                         ; 3005 			stW_prms.cals[stW_veh_offset_c+stW_cfg_c].TimeOut_c[WL1] = *stW_data_p_c;
3762  0b45 f600f0            	ldab	_stW_cfg_c
3763  0b48 fb00ef            	addb	_stW_veh_offset_c
3764  0b4b 45                	rola	
3765  0b4c cd0013            	ldy	#19
3766  0b4f 13                	emul	
3767  0b50 b746              	tfr	d,y
3768  0b52 180a00ea002c      	movb	0,x,_stW_prms+1,y
3769                         ; 3006 			stW_prms.cals[stW_veh_offset_c+stW_cfg_c].TimeOut_c[WL2] = *(stW_data_p_c + 1);
3771  0b58 180a01ea002d      	movb	1,x,_stW_prms+2,y
3772                         ; 3007 			stW_prms.cals[stW_veh_offset_c+stW_cfg_c].TimeOut_c[WL3] = *(stW_data_p_c + 2);
3774  0b5e 180a02ea002e      	movb	2,x,_stW_prms+3,y
3775                         ; 3008 			stW_prms.cals[stW_veh_offset_c+stW_cfg_c].TimeOut_c[WL4] = *(stW_data_p_c + 3);
3777  0b64 180a03ea002f      	movb	3,x,_stW_prms+4,y
3778                         ; 3012 			stW_chck_rslt = TRUE;
3780  0b6a 182000f8          	bra	LC019
3781  0b6e                   L3241:
3782                         ; 3024 		if ( ( *stW_data_p_c < 1 || *stW_data_p_c > 200 ) ||
3782                         ; 3025 		   ( ( *(stW_data_p_c + 1) < 1 || *(stW_data_p_c + 1) > 200) ) ||
3782                         ; 3026 		   ( ( *(stW_data_p_c + 2) < 1 || *(stW_data_p_c + 2) > 200) ) ||
3782                         ; 3027 		   ( ( *(stW_data_p_c + 3) < 1 || *(stW_data_p_c + 3) > 200) ) ||
3782                         ; 3028 		   ( ( *(stW_data_p_c + 4) < 1 || *(stW_data_p_c + 4) > 200) ) ) {
3784  0b6e ee80              	ldx	OFST-4,s
3785  0b70 e600              	ldab	0,x
3786  0b72 2728              	beq	L1251
3788  0b74 b756              	tfr	x,y
3789  0b76 e640              	ldab	0,y
3790  0b78 c1c8              	cmpb	#200
3791  0b7a 2220              	bhi	L1251
3793  0b7c e641              	ldab	1,y
3794  0b7e 271c              	beq	L1251
3796  0b80 c1c8              	cmpb	#200
3797  0b82 2218              	bhi	L1251
3799  0b84 e642              	ldab	2,y
3800  0b86 2714              	beq	L1251
3802  0b88 c1c8              	cmpb	#200
3803  0b8a 2210              	bhi	L1251
3805  0b8c e643              	ldab	3,y
3806  0b8e 270c              	beq	L1251
3808  0b90 c1c8              	cmpb	#200
3809  0b92 2208              	bhi	L1251
3811  0b94 e644              	ldab	4,y
3812  0b96 2704              	beq	L1251
3814  0b98 c1c8              	cmpb	#200
3815  0b9a 2307              	bls	L7151
3816  0b9c                   L1251:
3817                         ; 3031 			stW_chck_rslt = FALSE;
3819  0b9c c7                	clrb	
3820  0b9d 6b82              	stab	OFST-2,s
3822  0b9f 182000c5          	bra	L5641
3823  0ba3                   L7151:
3824                         ; 3037 			stW_prms.cals[stW_veh_offset_c+stW_cfg_c].WL1Temp_Threshld = *stW_data_p_c ; //Added in case If decides to have new value
3826  0ba3 f600f0            	ldab	_stW_cfg_c
3827  0ba6 fb00ef            	addb	_stW_veh_offset_c
3828  0ba9 45                	rola	
3829  0baa cd0013            	ldy	#19
3830  0bad 13                	emul	
3831  0bae b746              	tfr	d,y
3832  0bb0 180a00ea0035      	movb	0,x,_stW_prms+10,y
3833                         ; 3038 			stW_prms.cals[stW_veh_offset_c+stW_cfg_c].WL2Temp_Threshld = *(stW_data_p_c + 1);
3835  0bb6 180a01ea0036      	movb	1,x,_stW_prms+11,y
3836                         ; 3039 			stW_prms.cals[stW_veh_offset_c+stW_cfg_c].WL3Temp_Threshld = *(stW_data_p_c + 2);
3838  0bbc 180a02ea0037      	movb	2,x,_stW_prms+12,y
3839                         ; 3040 			stW_prms.cals[stW_veh_offset_c+stW_cfg_c].WL4Temp_Threshld = *(stW_data_p_c + 3);
3841  0bc2 180a03ea0038      	movb	3,x,_stW_prms+13,y
3842                         ; 3041 			stW_prms.cals[stW_veh_offset_c+stW_cfg_c].MaxTemp_Threshld = *(stW_data_p_c + 4);
3844  0bc8 180a04ea0039      	movb	4,x,_stW_prms+14,y
3845                         ; 3045 			stW_chck_rslt = TRUE;
3847  0bce 18200094          	bra	LC019
3848  0bd2                   L5241:
3849                         ; 3057 		if ( ( *stW_data_p_c < 0 || *stW_data_p_c > 60 ) ||
3849                         ; 3058 		   ( ( *(stW_data_p_c + 1) < 0 || *(stW_data_p_c + 1) > 60) ) ||
3849                         ; 3059 		   ( ( *(stW_data_p_c + 2) < 0 || *(stW_data_p_c + 2) > 60) ) ||
3849                         ; 3060 		   ( ( *(stW_data_p_c + 3) < 0 || *(stW_data_p_c + 3) > 60) ) ) {
3851  0bd2 ed80              	ldy	OFST-4,s
3852  0bd4 e640              	ldab	0,y
3853  0bd6 c13c              	cmpb	#60
3854  0bd8 22c2              	bhi	L1251
3856  0bda e641              	ldab	1,y
3857  0bdc c13c              	cmpb	#60
3858  0bde 22bc              	bhi	L1251
3860  0be0 e642              	ldab	2,y
3861  0be2 c13c              	cmpb	#60
3862  0be4 22b6              	bhi	L1251
3864  0be6 e643              	ldab	3,y
3865  0be8 c13c              	cmpb	#60
3866                         ; 3063 			stW_chck_rslt = FALSE;
3869  0bea 22b0              	bhi	L1251
3870                         ; 3068 			stW_prms.cals[stW_veh_offset_c+stW_cfg_c].WL1TempChck_Time = *(stW_data_p_c);
3872  0bec f600f0            	ldab	_stW_cfg_c
3873  0bef fb00ef            	addb	_stW_veh_offset_c
3874  0bf2 45                	rola	
3875  0bf3 cd0013            	ldy	#19
3876  0bf6 13                	emul	
3877  0bf7 b746              	tfr	d,y
3878  0bf9 ee80              	ldx	OFST-4,s
3879  0bfb 180a00ea003a      	movb	0,x,_stW_prms+15,y
3880                         ; 3069 			stW_prms.cals[stW_veh_offset_c+stW_cfg_c].WL2TempChck_Time = *(stW_data_p_c + 1);
3882  0c01 180a01ea003b      	movb	1,x,_stW_prms+16,y
3883                         ; 3070 			stW_prms.cals[stW_veh_offset_c+stW_cfg_c].WL3TempChck_Time = *(stW_data_p_c + 2);
3885  0c07 180a02ea003c      	movb	2,x,_stW_prms+17,y
3886                         ; 3071 			stW_prms.cals[stW_veh_offset_c+stW_cfg_c].WL4TempChck_Time = *(stW_data_p_c + 3);
3888  0c0d 180a03ea003d      	movb	3,x,_stW_prms+18,y
3889                         ; 3075 			stW_chck_rslt = TRUE;
3891  0c13 2051              	bra	LC019
3892  0c15                   L7241:
3893                         ; 3105 			if( ( *stW_data_p_c < 0x07 || *stW_data_p_c > 0x0C ) ||						// Iref Check
3893                         ; 3106 				( *(stW_data_p_c+1) < 0x02 ) || 										// Vref Low range Check1 < 0x2xx
3893                         ; 3107 				( ( *(stW_data_p_c + 1) == 0x02 && *(stW_data_p_c + 2) < 0xBC) ) ||     // Vref Low range Check2 < 0x2BC
3893                         ; 3108 				( *(stW_data_p_c+1) > 0x05 ) ||											// Vref high range Check1 > 0x5xx
3893                         ; 3109 				( ( *(stW_data_p_c + 1) == 0x05 && *(stW_data_p_c + 2) > 0x14) ) ){     // Vref high range Check2 > 0x514
3895  0c15 ed80              	ldy	OFST-4,s
3896  0c17 e640              	ldab	0,y
3897  0c19 c107              	cmpb	#7
3898  0c1b 1825ff7d          	blo	L1251
3900  0c1f c10c              	cmpb	#12
3901  0c21 1822ff77          	bhi	L1251
3903  0c25 e641              	ldab	1,y
3904  0c27 c102              	cmpb	#2
3905  0c29 1825ff6f          	blo	L1251
3907  0c2d 260a              	bne	L7551
3909  0c2f e642              	ldab	2,y
3910  0c31 c1bc              	cmpb	#188
3911  0c33 1825ff65          	blo	L1251
3912  0c37 e641              	ldab	1,y
3913  0c39                   L7551:
3915  0c39 c105              	cmpb	#5
3916  0c3b 1822ff5d          	bhi	L1251
3918  0c3f 2608              	bne	L7451
3920  0c41 e642              	ldab	2,y
3921  0c43 c114              	cmpb	#20
3922                         ; 3112 				stW_chck_rslt = FALSE;				
3925  0c45 1822ff53          	bhi	L1251
3926  0c49                   L7451:
3927                         ; 3116 				AD_Iref_STW_c = *(stW_data_p_c);
3929  0c49 180d400000        	movb	0,y,_AD_Iref_STW_c
3930                         ; 3117 				AD_Vref_STW_w = *(stW_data_p_c+1);
3932  0c4e e641              	ldab	1,y
3933  0c50 87                	clra	
3934  0c51 7c0000            	std	_AD_Vref_STW_w
3935                         ; 3118 				AD_Vref_STW_w = (AD_Vref_STW_w<<8);		// shift information to high byte
3937  0c54 b60001            	ldaa	_AD_Vref_STW_w+1
3938  0c57 c7                	clrb	
3939  0c58 7c0000            	std	_AD_Vref_STW_w
3940                         ; 3119 				AD_Vref_STW_w |= *(stW_data_p_c+2);		// or with low byte
3942  0c5b e642              	ldab	2,y
3943  0c5d b796              	exg	b,y
3944  0c5f 18fa0000          	ory	_AD_Vref_STW_w
3945  0c63 7d0000            	sty	_AD_Vref_STW_w
3946                         ; 3123 				stW_chck_rslt = TRUE;				
3948  0c66                   LC019:
3949  0c66 c601              	ldab	#1
3950  0c68                   L5641:
3951                         ; 3135 	return(stW_chck_rslt);
3955  0c68 1b86              	leas	6,s
3956  0c6a 0a                	rtc	
4004                         ; 3155 @far u_8Bit stWF_Write_Diag_Data(u_8Bit stW_LID)
4004                         ; 3156 {
4005                         	switch	.ftext
4006  0c6b                   f_stWF_Write_Diag_Data:
4008  0c6b 3b                	pshd	
4009  0c6c 37                	pshb	
4010       00000001          OFST:	set	1
4013                         ; 3158 	u_8Bit stW_LID_s = stW_LID;    // store the LID request
4015  0c6d 6b80              	stab	OFST-1,s
4016                         ; 3159 	u_8Bit stW_write_rslt = FALSE; // Write result (Success or Fail)
4018                         ; 3163 	if ( (stW_LID_s == HSW_PWM) || 
4018                         ; 3164 		(stW_LID_s == HSW_TIMEOUT) || 
4018                         ; 3165 		(stW_LID_s == HSW_TEMP_CUT_OFF) || 
4018                         ; 3166 		(stW_LID_s == HSW_MIN_TIMEOUT) ){
4020  0c6f c103              	cmpb	#3
4021  0c71 270c              	beq	L7061
4023  0c73 c104              	cmpb	#4
4024  0c75 2708              	beq	L7061
4026  0c77 c10b              	cmpb	#11
4027  0c79 2704              	beq	L7061
4029  0c7b c10d              	cmpb	#13
4030  0c7d 2612              	bne	L5061
4031  0c7f                   L7061:
4032                         ; 3172 			EE_BlockWrite(EE_WHEEL_CAL_DATA, (u_8Bit*)&stW_prms); 
4034  0c7f cc002b            	ldd	#_stW_prms
4035  0c82 3b                	pshd	
4036  0c83 cc000c            	ldd	#12
4037  0c86 4a000000          	call	f_EE_BlockWrite
4039  0c8a 1b82              	leas	2,s
4040                         ; 3180 		stW_write_rslt = TRUE;
4042  0c8c                   LC021:
4043  0c8c c601              	ldab	#1
4045  0c8e                   L5161:
4046                         ; 3194 	return(stW_write_rslt);
4050  0c8e 1b83              	leas	3,s
4051  0c90 0a                	rtc	
4052  0c91                   L5061:
4053                         ; 3183 		if(stW_LID_s == STW_IREF_VREF){
4055  0c91 c110              	cmpb	#16
4056                         ; 3188 			stW_write_rslt = TRUE;			
4059  0c93 27f7              	beq	LC021
4060                         ; 3191 			stW_write_rslt = FALSE;			
4062  0c95 c7                	clrb	
4063  0c96 6b80              	stab	OFST-1,s
4064  0c98 20f4              	bra	L5161
4088                         ; 3208 @far u_8Bit stWF_Get_Output_Stat(void)
4088                         ; 3209 {
4089                         	switch	.ftext
4090  0c9a                   f_stWF_Get_Output_Stat:
4094                         ; 3210 	return(stW_level_c);
4096  0c9a f6002a            	ldab	_stW_level_c
4099  0c9d 0a                	rtc	
4170                         ; 3230 @far void stWF_Diag_DTC_Control(stW_DTC_Control_e stW_DTC_cntrl, u_16Bit stW_DTC)
4170                         ; 3231 {
4171                         	switch	.ftext
4172  0c9e                   f_stWF_Diag_DTC_Control:
4174  0c9e 3b                	pshd	
4175  0c9f 3b                	pshd	
4176       00000002          OFST:	set	2
4179                         ; 3233 	u_16Bit stW_chosen_DTC = stW_DTC;
4181  0ca0 18028780          	movw	OFST+5,s,OFST-2,s
4182                         ; 3236 	if (stW_DTC_cntrl == STW_SET_DTC) {
4184  0ca4 e683              	ldab	OFST+1,s
4185  0ca6 042107            	dbne	b,L5661
4186                         ; 3238 		stW_Flt_status_w = stW_chosen_DTC;
4188  0ca9 18058000f2        	movw	OFST-2,s,_stW_Flt_status_w
4190  0cae 2008              	bra	L7661
4191  0cb0                   L5661:
4192                         ; 3242 		stW_Flt_status_w = STW_NO_FLT_MASK;
4194  0cb0 87                	clra	
4195  0cb1 c7                	clrb	
4196  0cb2 7c00f2            	std	_stW_Flt_status_w
4197  0cb5 7c0020            	std	_stW_Flt_Mtr_cnt
4198                         ; 3245 		stW_Flt_Mtr_cnt = 0;
4200  0cb8                   L7661:
4201                         ; 3247 }
4204  0cb8 1b84              	leas	4,s
4205  0cba 0a                	rtc	
4238                         ; 3266 @far u_8Bit stWF_Actvtn_Stat(void)
4238                         ; 3267 {
4239                         	switch	.ftext
4240  0cbb                   f_stWF_Actvtn_Stat:
4242  0cbb 37                	pshb	
4243       00000001          OFST:	set	1
4246                         ; 3268 	u_8Bit stW_actvtn_stat_c = FALSE;	// local variable to store steering wheel activation status
4248  0cbc 6980              	clr	OFST-1,s
4249                         ; 3271 	if( (stW_state_b == TRUE) && (stW_level_c != WL0) ){
4251  0cbe 1f00fa0109        	brclr	_stW_Flag1_c,1,L5071
4253  0cc3 f6002a            	ldab	_stW_level_c
4254  0cc6 2704              	beq	L5071
4255                         ; 3272 		stW_actvtn_stat_c = TRUE;	// output is active (turned ON)
4257  0cc8 c601              	ldab	#1
4259  0cca 2001              	bra	L7071
4260  0ccc                   L5071:
4261                         ; 3274 		stW_actvtn_stat_c = FALSE;	// output is not active (turned OFF)
4263  0ccc c7                	clrb	
4264  0ccd                   L7071:
4265                         ; 3276 	return(stW_actvtn_stat_c);	
4269  0ccd 1b81              	leas	1,s
4270  0ccf 0a                	rtc	
4306                         ; 3290 @far void stWF_CurrentLimit_Timer(void)
4306                         ; 3291 {
4307                         	switch	.ftext
4308  0cd0                   f_stWF_CurrentLimit_Timer:
4310  0cd0 37                	pshb	
4311       00000001          OFST:	set	1
4314                         ; 3292 	u_8Bit stW_Isense_limit_c = 80; 
4316  0cd1 c650              	ldab	#80
4317  0cd3 6b80              	stab	OFST-1,s
4318                         ; 3299 	if( (stW_level_c == WL4) && (stW_Isense_c >= stW_Isense_limit_c ) )
4320  0cd5 f6002a            	ldab	_stW_level_c
4321  0cd8 c104              	cmpb	#4
4322  0cda 2620              	bne	L5271
4324  0cdc f600f7            	ldab	_stW_Isense_c
4325  0cdf c150              	cmpb	#80
4326  0ce1 2519              	blo	L5271
4327                         ; 3302 		if ((stW_Isense_limit_b == FALSE) && (stW_Isense_timer_w < STW_ISENSE_CNTR))
4329  0ce3 1e0010040e        	brset	_stW_Flag2_c,4,L7271
4331  0ce8 fc0003            	ldd	_stW_Isense_timer_w
4332  0ceb 8c05dc            	cpd	#1500
4333  0cee 2406              	bhs	L7271
4334                         ; 3304 			stW_Isense_timer_w++;
4336  0cf0 18720003          	incw	_stW_Isense_timer_w
4338  0cf4 200a              	bra	L3371
4339  0cf6                   L7271:
4340                         ; 3309 			stW_Isense_limit_b = TRUE;
4342  0cf6 1c001004          	bset	_stW_Flag2_c,4
4343  0cfa 2004              	bra	L3371
4344  0cfc                   L5271:
4345                         ; 3315 		stW_Isense_timer_w = 0;
4347  0cfc 18790003          	clrw	_stW_Isense_timer_w
4348  0d00                   L3371:
4349                         ; 3318 }
4352  0d00 1b81              	leas	1,s
4353  0d02 0a                	rtc	
4916                         	xdef	f_stWF_Updt_Flt_Stat
4917                         	xdef	f_stWF_Chck_Auto_Lvl_Updt
4918                         	xdef	f_stWLF_Chck_mainState
4919                         	xdef	f_stWLF_RemSt_Chck
4920                         	xdef	f_stWF_Diag_IO_Cntrl
4921                         	xdef	f_stWF_Swtch_Cntrl
4922                         	xdef	f_stWF_Rtrn_Cntrl_toECU
4923                         	xdef	f_stWF_Chck_Swtch_Prps
4924                         	xdef	f_stWF_Fnsh_LED_Dslpy
4925                         	xdef	f_stWF_Swtch_Mngmnt
4926                         	xdef	f_stWLF_Get_Swtch_Rq
4927                         	xdef	f_stWF_Updt_Status
4928                         	xdef	f_stWLF_Chck_Diag_TurnOff
4929                         	xdef	f_stWF_Diag_Timeout_Updt_Lvl
4930                         	xdef	f_stWF_Diag_IO_Updt_Lvl
4931                         	xdef	f_stWF_Updt_Pwm
4932                         	xdef	f_stWF_Upt_Lvl
4933                         	xdef	f_stWF_Rdg_Temperature
4934                         	xdef	f_stWF_Cal_TmOut
4935                         	xdef	f_stWF_Isense_AD_Rdg
4936                         	xdef	f_stWF_Vsense_AD_Rdg
4937                         	xdef	f_stWF_AD_Cnvrn
4938                         	switch	.bss
4939  0000                   _stW_AD_prms:
4940  0000 000000            	ds.b	3
4941                         	xdef	_stW_AD_prms
4942  0003                   _stW_Isense_timer_w:
4943  0003 0000              	ds.b	2
4944                         	xdef	_stW_Isense_timer_w
4945  0005                   _stW_ontimeLmt_w:
4946  0005 0000              	ds.b	2
4947                         	xdef	_stW_ontimeLmt_w
4948  0007                   _stW_WL4TempChck_lmt_w:
4949  0007 0000              	ds.b	2
4950                         	xdef	_stW_WL4TempChck_lmt_w
4951  0009                   _stW_WL3TempChck_lmt_w:
4952  0009 0000              	ds.b	2
4953                         	xdef	_stW_WL3TempChck_lmt_w
4954  000b                   _stW_WL2TempChck_lmt_w:
4955  000b 0000              	ds.b	2
4956                         	xdef	_stW_WL2TempChck_lmt_w
4957  000d                   _stW_WL1TempChck_lmt_w:
4958  000d 0000              	ds.b	2
4959                         	xdef	_stW_WL1TempChck_lmt_w
4960  000f                   _stW_lst_main_state_c:
4961  000f 00                	ds.b	1
4962                         	xdef	_stW_lst_main_state_c
4963  0010                   _stW_Flag2_c:
4964  0010 00                	ds.b	1
4965                         	xdef	_stW_Flag2_c
4966  0011                   _stW_LED_Dsply_Tm:
4967  0011 0000              	ds.b	2
4968                         	xdef	_stW_LED_Dsply_Tm
4969  0013                   _stW_Flt_Mtr_Tm:
4970  0013 0000              	ds.b	2
4971                         	xdef	_stW_Flt_Mtr_Tm
4972  0015                   _stW_chck_Open_or_STB_dly_c:
4973  0015 00                	ds.b	1
4974                         	xdef	_stW_chck_Open_or_STB_dly_c
4975  0016                   _stW_Isense_dly_cnt_c:
4976  0016 00                	ds.b	1
4977                         	xdef	_stW_Isense_dly_cnt_c
4978  0017                   _stW_Ign_stat:
4979  0017 00                	ds.b	1
4980                         	xdef	_stW_Ign_stat
4981  0018                   _stW_pwm_state_c:
4982  0018 00                	ds.b	1
4983                         	xdef	_stW_pwm_state_c
4984  0019                   _stW_tmprtr_sensor_flt_c:
4985  0019 00                	ds.b	1
4986                         	xdef	_stW_tmprtr_sensor_flt_c
4987  001a                   _stW_prev_pwm:
4988  001a 0000              	ds.b	2
4989                         	xdef	_stW_prev_pwm
4990  001c                   _stW_pwm_w:
4991  001c 0000              	ds.b	2
4992                         	xdef	_stW_pwm_w
4993  001e                   _stW_LED_Dsply_Tm_cnt:
4994  001e 0000              	ds.b	2
4995                         	xdef	_stW_LED_Dsply_Tm_cnt
4996  0020                   _stW_Flt_Mtr_cnt:
4997  0020 0000              	ds.b	2
4998                         	xdef	_stW_Flt_Mtr_cnt
4999  0022                   _stW_TimeOut_Cycles_w:
5000  0022 0000              	ds.b	2
5001                         	xdef	_stW_TimeOut_Cycles_w
5002  0024                   _stW_onTime_w:
5003  0024 0000              	ds.b	2
5004                         	xdef	_stW_onTime_w
5005  0026                   _stW_prev_LED_stat_c:
5006  0026 00                	ds.b	1
5007                         	xdef	_stW_prev_LED_stat_c
5008  0027                   _stW_LED_stat_c:
5009  0027 00                	ds.b	1
5010                         	xdef	_stW_LED_stat_c
5011  0028                   _stW_lst_sw_rq_c:
5012  0028 00                	ds.b	1
5013                         	xdef	_stW_lst_sw_rq_c
5014  0029                   _stW_status_c:
5015  0029 00                	ds.b	1
5016                         	xdef	_stW_status_c
5017  002a                   _stW_level_c:
5018  002a 00                	ds.b	1
5019                         	xdef	_stW_level_c
5020                         	xref	_hs_Isense_c
5021                         	xref	f_EE_BlockRead
5022                         	xref	f_EE_BlockWrite
5023                         	xref	_relay_fault_status2_c
5024                         	xref	_relay_status2_c
5025                         	xref	f_FMemLibF_ReportDtc
5026                         	xref	f_Pwm_GetOutputState
5027                         	xref	f_Pwm_SetDutyCycle
5028                         	xref	_DioConfigData
5029                         	xref	_Dio_PortRead_Ptr
5030                         	xref	_Dio_PortWrite_Ptr
5031                         	xref	__PTIT
5032                         	xref	__CFORC
5033                         	xref	_ADF_Nrmlz_StW_Isense
5034                         	xref	_ADF_Nrmlz_Vsense
5035                         	xref	_ADF_AtoD_Cnvrsn
5036                         	xref	_ADF_Str_UnFlt_Mux_Rslt
5037                         	xref	_ADF_Mux_Filter
5038                         	xref	_AD_mux_Filter
5039                         	xref	_AD_Vref_STW_w
5040                         	xref	_AD_Iref_STW_c
5041                         	xref	_ru_eep_PROXI_Cfg
5042                         	xref	_diag_io_lock3_flags_c
5043                         	xref	_diag_io_lock_flags_c
5044                         	xref	_diag_io_st_whl_state_c
5045                         	xref	_diag_io_st_whl_rq_c
5046                         	xref	_diag_rc_lock_flags_c
5047                         	xref	_canio_RX_ExternalTemperature_c
5048                         	xref	_canio_RX_IGN_STATUS_c
5049                         	xref	_canio_RX_StW_TempSens_FltSts_c
5050                         	xref	_canio_RX_StW_TempSts_c
5051                         	xref	_canio_RX_HSW_RQ_TGW_c
5052                         	xref	_mainF_Calc_PWM
5053                         	xref	_main_flag2_c
5054                         	xref	_main_flag_c
5055                         	xref	_main_SwReq_Rear_c
5056                         	xref	_main_SwReq_Front_c
5057                         	xref	_main_state_c
5058                         	xref	_main_CSWM_Status_c
5059                         	xdef	f_stWF_CurrentLimit_Timer
5060                         	xdef	f_stWF_Actvtn_Stat
5061                         	xdef	f_stWF_Diag_DTC_Control
5062                         	xdef	f_stWF_Get_Output_Stat
5063                         	xdef	f_stWF_Write_Diag_Data
5064                         	xdef	f_stWF_Chck_Diag_Prms
5065                         	xdef	f_stWF_LED_Display
5066                         	xdef	f_stWF_Clear
5067                         	xdef	f_stWF_DTC_Manager
5068                         	xdef	f_stWF_AD_Rdng
5069                         	xdef	f_stWF_Flt_Mntr
5070                         	xdef	f_stWF_Timer
5071                         	xdef	f_stWF_Init
5072                         	xdef	f_stWF_Output_Control
5073                         	xdef	f_stWF_Manager
5074  002b                   _stW_prms:
5075  002b 0000000000000000  	ds.b	190
5076                         	xdef	_stW_prms
5077  00e9                   _stW_flt_calibs_c:
5078  00e9 000000000000      	ds.b	6
5079                         	xdef	_stW_flt_calibs_c
5080  00ef                   _stW_veh_offset_c:
5081  00ef 00                	ds.b	1
5082                         	xdef	_stW_veh_offset_c
5083  00f0                   _stW_cfg_c:
5084  00f0 00                	ds.b	1
5085                         	xdef	_stW_cfg_c
5086  00f1                   _stW_temperature_c:
5087  00f1 00                	ds.b	1
5088                         	xdef	_stW_temperature_c
5089  00f2                   _stW_Flt_status_w:
5090  00f2 0000              	ds.b	2
5091                         	xdef	_stW_Flt_status_w
5092  00f4                   _stW_RearHeat_Isense_c:
5093  00f4 00                	ds.b	1
5094                         	xdef	_stW_RearHeat_Isense_c
5095  00f5                   _stW_Isense_offset_w:
5096  00f5 0000              	ds.b	2
5097                         	xdef	_stW_Isense_offset_w
5098  00f7                   _stW_Isense_c:
5099  00f7 00                	ds.b	1
5100                         	xdef	_stW_Isense_c
5101  00f8                   _stW_Vsense_LS_c:
5102  00f8 00                	ds.b	1
5103                         	xdef	_stW_Vsense_LS_c
5104  00f9                   _stW_Vsense_HS_c:
5105  00f9 00                	ds.b	1
5106                         	xdef	_stW_Vsense_HS_c
5107  00fa                   _stW_Flag1_c:
5108  00fa 00                	ds.b	1
5109                         	xdef	_stW_Flag1_c
5110                         	xref	f_IlPutTxHSW_StatSts
5131                         	end
