   1                         ; C Compiler for S12XE [COSMIC Software].
   2                         ; Generator V4.7.8 - 27 Jul 2007
   3                         ; Optimizer V4.6.13 - 27 Jul 2007
   4                         	switch	.data
   5  0000                   _Mcu_Reset_Reason:
   6  0000 00                	dc.b	0
   7  0001                   _Mcu_SWResetFlag:
   8  0001 00                	dc.b	0
   9  0002                   L3_Mcu_COPResetFlag:
  10  0002 00                	dc.b	0
  11  0003                   L5_Mcu_ClockMonitorResetFlag:
  12  0003 00                	dc.b	0
  13  0004                   L31_Mcu_PowerOnWakeUp:
  14  0004 00                	dc.b	0
 775                         ; 313 void Mcu_Init(const Mcu_ConfigType *ConfigPtr )
 775                         ; 314 {
 776                         	switch	.text
 777  0000                   _Mcu_Init:
 781                         ; 319     Mcu_UserConfig_Ptr = ConfigPtr;
 783  0000 7c0001            	std	L7_Mcu_UserConfig_Ptr
 784                         ; 323     if(TRUE== CRGFLG_PORF)
 786  0003 1f00004017        	brclr	__CRGFLG,64,L174
 787                         ; 325         Mcu_Reset_Reason = MCU_POWER_ON_RESET;
 789  0008 790000            	clr	_Mcu_Reset_Reason
 790                         ; 326         CRGFLG = CRGFLG_PORF_MASK;
 792  000b c640              	ldab	#64
 793  000d 7b0000            	stab	__CRGFLG
 794                         ; 327         Mcu_PowerOnWakeUp = TRUE;
 796  0010 c601              	ldab	#1
 797  0012 7b0004            	stab	L31_Mcu_PowerOnWakeUp
 798                         ; 329         Mcu_ClockMonitorResetFlag = FALSE;
 800  0015 790003            	clr	L5_Mcu_ClockMonitorResetFlag
 801                         ; 330         Mcu_COPResetFlag          = FALSE;
 803  0018 790002            	clr	L3_Mcu_COPResetFlag
 804                         ; 331         Mcu_SWResetFlag           = FALSE;
 806  001b 790001            	clr	_Mcu_SWResetFlag
 809  001e 3d                	rts	
 810  001f                   L174:
 811                         ; 337         if(TRUE == Mcu_ClockMonitorResetFlag)
 813  001f f60003            	ldab	L5_Mcu_ClockMonitorResetFlag
 814  0022 042108            	dbne	b,L574
 815                         ; 339             Mcu_Reset_Reason = MCU_CLOCK_MONITOR_RESET;
 817  0025 c605              	ldab	#5
 818  0027 7b0000            	stab	_Mcu_Reset_Reason
 819                         ; 340             Mcu_ClockMonitorResetFlag = FALSE;
 821  002a 790003            	clr	L5_Mcu_ClockMonitorResetFlag
 822  002d                   L574:
 823                         ; 343 	     if(TRUE == Mcu_COPResetFlag)
 825  002d f60002            	ldab	L3_Mcu_COPResetFlag
 826  0030 042108            	dbne	b,L774
 827                         ; 345             Mcu_Reset_Reason = MCU_WATCHDOG_RESET;
 829  0033 c601              	ldab	#1
 830  0035 7b0000            	stab	_Mcu_Reset_Reason
 831                         ; 346             Mcu_COPResetFlag = FALSE;
 833  0038 790002            	clr	L3_Mcu_COPResetFlag
 834  003b                   L774:
 835                         ; 349         if((TRUE == Mcu_SWResetFlag) && (TRUE == CRGFLG_ILAF))
 837  003b f60001            	ldab	_Mcu_SWResetFlag
 838  003e 04210f            	dbne	b,L105
 840  0041 1f0000040a        	brclr	__CRGFLG,4,L105
 841                         ; 351             Mcu_Reset_Reason = MCU_SW_RESET;
 843  0046 c602              	ldab	#2
 844  0048 7b0000            	stab	_Mcu_Reset_Reason
 845                         ; 352             Mcu_SWResetFlag = FALSE;
 847  004b 790001            	clr	_Mcu_SWResetFlag
 848                         ; 353             CRGFLG_ILAF = TRUE;
 851  004e 200a              	bra	LC001
 852  0050                   L105:
 853                         ; 355         else if(TRUE == CRGFLG_ILAF)
 855  0050 1f00000409        	brclr	__CRGFLG,4,L305
 856                         ; 357             Mcu_Reset_Reason = MCU_ILLEGAL_ADDRESS_RESET;
 858  0055 c604              	ldab	#4
 859  0057 7b0000            	stab	_Mcu_Reset_Reason
 860                         ; 358             CRGFLG_ILAF = TRUE;
 862  005a                   LC001:
 863  005a 1c000004          	bset	__CRGFLG,4
 864  005e                   L305:
 865                         ; 362         if(TRUE== CRGFLG_LVRF)
 867  005e 1f0000200a        	brclr	__CRGFLG,32,L374
 868                         ; 364             Mcu_Reset_Reason =MCU_LOW_VOLTAGE_RESET;
 870  0063 c603              	ldab	#3
 871  0065 7b0000            	stab	_Mcu_Reset_Reason
 872                         ; 365             CRGFLG = CRGFLG_LVRF_MASK;
 874  0068 c620              	ldab	#32
 875  006a 7b0000            	stab	__CRGFLG
 876  006d                   L374:
 877                         ; 370 }
 880  006d 3d                	rts	
 931                         ; 388 Std_ReturnType Mcu_InitRamSection(const Mcu_RamSectionType RamSection )
 931                         ; 389 {
 932                         	switch	.text
 933  006e                   _Mcu_InitRamSection:
 935  006e 3b                	pshd	
 936  006f 1b9a              	leas	-6,s
 937       00000006          OFST:	set	6
 940                         ; 396     index = 0;
 942  0071 18c7              	clry	
 943  0073 6d80              	sty	OFST-6,s
 944  0075 6d82              	sty	OFST-4,s
 945                         ; 397     ramptr = (uint8 *__far)(Mcu_UserConfig_Ptr->MCU_RAM_SECTOR_SETTING
 945                         ; 398                                            [RamSection].Mcu_RamSect_BaseAddr);
 947  0077 fd0001            	ldy	L7_Mcu_UserConfig_Ptr
 948  007a 8609              	ldaa	#9
 949  007c 12                	mul	
 950  007d 19ee              	leay	d,y
 951  007f 18024584          	movw	5,y,OFST-2,s
 953  0083 201a              	bra	L01
 954  0085                   L535:
 955                         ; 405         *(ramptr++) = Mcu_UserConfig_Ptr->MCU_RAM_SECTOR_SETTING
 955                         ; 406                                           [RamSection].Mcu_RamSect_Preset;
 957  0085 fd0001            	ldy	L7_Mcu_UserConfig_Ptr
 958  0088 e687              	ldab	OFST+1,s
 959  008a 8609              	ldaa	#9
 960  008c 12                	mul	
 961  008d 19ee              	leay	d,y
 962  008f e64b              	ldab	11,y
 963  0091 ed84              	ldy	OFST-2,s
 964  0093 6b70              	stab	1,y+
 965  0095 6d84              	sty	OFST-2,s
 966                         ; 407         index++;
 968  0097 186282            	incw	OFST-4,s
 969  009a 2603              	bne	L01
 970  009c 08                	inx	
 971  009d 6e80              	stx	OFST-6,s
 972  009f                   L01:
 973                         ; 402     while(index < (Mcu_UserConfig_Ptr->MCU_RAM_SECTOR_SETTING
 973                         ; 403                                        [RamSection].Mcu_RamSect_Size))
 975  009f fd0001            	ldy	L7_Mcu_UserConfig_Ptr
 976  00a2 e687              	ldab	OFST+1,s
 977  00a4 8609              	ldaa	#9
 978  00a6 12                	mul	
 979  00a7 19ee              	leay	d,y
 980  00a9 ec82              	ldd	OFST-4,s
 981  00ab ac49              	cpd	9,y
 982  00ad ee80              	ldx	OFST-6,s
 983  00af 18ae47            	cpex	7,y
 984  00b2 25d1              	blo	L535
 985                         ; 411     return E_OK;
 987  00b4 c7                	clrb	
 990  00b5 1b88              	leas	8,s
 991  00b7 3d                	rts	
1042                         ; 432 Std_ReturnType Mcu_InitClock(const Mcu_ClockType ClockSetting )
1042                         ; 433 {
1043                         	switch	.text
1044  00b8                   _Mcu_InitClock:
1046  00b8 3b                	pshd	
1047  00b9 3b                	pshd	
1048       00000002          OFST:	set	2
1051                         ; 435     uint16 mcu_count = 0xFFFF; 
1053  00ba ccffff            	ldd	#-1
1054  00bd 6c80              	std	OFST-2,s
1055  00bf                   L565:
1056                         ; 439         mcu_count--;
1058  00bf 186380            	decw	OFST-2,s
1059                         ; 440         if (0 == mcu_count)
1061  00c2 260a              	bne	L765
1062                         ; 443             Mcu_PerformReset();
1064  00c4 c601              	ldab	#1
1065  00c6 7b0001            	stab	_Mcu_SWResetFlag
1068  00c9 c6aa              	ldab	#170
1069  00cb 7b3fff            	stab	16383
1070  00ce                   L765:
1071                         ; 446     while (0 != (CRGFLG & 0x01));
1073  00ce 1e000001ec        	brset	__CRGFLG,1,L565
1074                         ; 449     Mcu_ClkSettings =ClockSetting;
1076  00d3 e683              	ldab	OFST+1,s
1077  00d5 7b0000            	stab	L11_Mcu_ClkSettings
1078                         ; 453     Mcu_TimerInit(ClockSetting);
1080  00d8 87                	clra	
1081  00d9 16021c            	jsr	_Mcu_TimerInit
1083                         ; 456     CLKSEL_PLLSEL = FALSE;
1085  00dc 1d000080          	bclr	__CLKSEL,128
1086                         ; 460     PLLCTL_FM = Mcu_UserConfig_Ptr->MCU_CLOCK_SETTING[ClockSetting].Mcu_PLLCTLFM_Value;
1088  00e0 fd0001            	ldy	L7_Mcu_UserConfig_Ptr
1089  00e3 e683              	ldab	OFST+1,s
1090  00e5 87                	clra	
1091  00e6 59                	lsld	
1092  00e7 59                	lsld	
1093  00e8 19ee              	leay	d,y
1094  00ea e6e814            	ldab	20,y
1095  00ed 58                	lslb	
1096  00ee 58                	lslb	
1097  00ef 58                	lslb	
1098  00f0 58                	lslb	
1099  00f1 c430              	andb	#48
1100  00f3 37                	pshb	
1101  00f4 f60000            	ldab	__PLLCTL
1102  00f7 c4cf              	andb	#207
1103  00f9 eab0              	orab	1,s+
1104  00fb 7b0000            	stab	__PLLCTL
1105                         ; 463     CRGINT_LOCKIE = FALSE;
1107  00fe 1d000010          	bclr	__CRGINT,16
1108                         ; 466     PLLCTL_PLLON =TRUE;
1110  0102 1c000040          	bset	__PLLCTL,64
1111                         ; 469     SYNR =Mcu_UserConfig_Ptr->MCU_CLOCK_SETTING[ClockSetting].Mcu_SYNR_RegValue;
1113  0106 cd0000            	ldy	#__SYNR
1114  0109 fe0001            	ldx	L7_Mcu_UserConfig_Ptr
1115  010c e683              	ldab	OFST+1,s
1116  010e 87                	clra	
1117  010f 59                	lsld	
1118  0110 59                	lsld	
1119  0111 1ae6              	leax	d,x
1120  0113 180ae01140        	movb	17,x,0,y
1121                         ; 472     REFDV=Mcu_UserConfig_Ptr->MCU_CLOCK_SETTING
1121                         ; 473     [ClockSetting].Mcu_REFDV_RegValue;
1123  0118 cd0000            	ldy	#__REFDV
1124  011b fe0001            	ldx	L7_Mcu_UserConfig_Ptr
1125  011e 1ae6              	leax	d,x
1126  0120 180ae01240        	movb	18,x,0,y
1127                         ; 476     POSTDIV=Mcu_UserConfig_Ptr->MCU_CLOCK_SETTING
1127                         ; 477     [ClockSetting].Mcu_POSTDIV_RegValue;
1129  0125 cd0000            	ldy	#__POSTDIV
1130  0128 180ae01340        	movb	19,x,0,y
1131                         ; 480     CRGINT =  Mcu_UserConfig_Ptr->MCU_CRGINT.Byte;
1133  012d cd0000            	ldy	#__CRGINT
1134  0130 fe0001            	ldx	L7_Mcu_UserConfig_Ptr
1135  0133 180ae01640        	movb	22,x,0,y
1136                         ; 481     return E_OK;
1138  0138 c7                	clrb	
1141  0139 1b84              	leas	4,s
1142  013b 3d                	rts	
1183                         ; 500 void Mcu_SetMode(const Mcu_ModeType McuMode)
1183                         ; 501 {
1184                         	switch	.text
1185  013c                   _Mcu_SetMode:
1187  013c 3b                	pshd	
1188       00000000          OFST:	set	0
1191                         ; 507     switch(Mcu_UserConfig_Ptr->MCU_MODE_SETTING[McuMode].Mcu_PowerMode)
1193  013d fd0001            	ldy	L7_Mcu_UserConfig_Ptr
1194  0140 8605              	ldaa	#5
1195  0142 12                	mul	
1196  0143 19ee              	leay	d,y
1197  0145 e64c              	ldab	12,y
1199  0147 270c              	beq	L575
1200  0149 04015c            	dbeq	b,L775
1201  014c 53                	decb	
1202  014d 1827008e          	beq	L106
1203  0151 18200096          	bra	L526
1204  0155                   L575:
1205                         ; 510         case  MCU_POWER_MODE_WAIT:
1205                         ; 511 
1205                         ; 512                    /*Set Low power mode in  CLKSEL register ( wait mode for
1205                         ; 513                    PLL/RTI ->these could be enabled/disabled during wait
1205                         ; 514                    mode,based on the configuration) */
1205                         ; 515                     CLKSEL_PLLWAI =  ~(Mcu_UserConfig_Ptr->MCU_MODE_SETTING
1205                         ; 516                                      [McuMode].Mcu_PLL_Enable_WaitMode);
1207  0155 fd0001            	ldy	L7_Mcu_UserConfig_Ptr
1208  0158 e681              	ldab	OFST+1,s
1209  015a 8605              	ldaa	#5
1210  015c 12                	mul	
1211  015d 19ee              	leay	d,y
1212  015f e64e              	ldab	14,y
1213  0161 51                	comb	
1214  0162 c501              	bitb	#1
1215  0164 2706              	beq	L61
1216  0166 1c000008          	bset	__CLKSEL,8
1217  016a 2004              	bra	L02
1218  016c                   L61:
1219  016c 1d000008          	bclr	__CLKSEL,8
1220  0170                   L02:
1221                         ; 518                     CLKSEL_RTIWAI =  ~(Mcu_UserConfig_Ptr->MCU_MODE_SETTING
1221                         ; 519                                      [McuMode].Mcu_RTI_Enable_WaitMode);
1223  0170 fd0001            	ldy	L7_Mcu_UserConfig_Ptr
1224  0173 e681              	ldab	OFST+1,s
1225  0175 8605              	ldaa	#5
1226  0177 12                	mul	
1227  0178 19ee              	leay	d,y
1228  017a e64d              	ldab	13,y
1229  017c 51                	comb	
1230  017d c501              	bitb	#1
1231  017f 2706              	beq	L22
1232  0181 1c000002          	bset	__CLKSEL,2
1233  0185 2004              	bra	L42
1234  0187                   L22:
1235  0187 1d000002          	bclr	__CLKSEL,2
1236  018b                   L42:
1237                         ; 522                     if(MCAL_DISABLE == Mcu_UserConfig_Ptr->MCU_MODE_SETTING
1237                         ; 523                             [McuMode].Mcu_PLL_Enable_WaitMode)
1239  018b fd0001            	ldy	L7_Mcu_UserConfig_Ptr
1240  018e e681              	ldab	OFST+1,s
1241  0190 8605              	ldaa	#5
1242  0192 12                	mul	
1243  0193 19ee              	leay	d,y
1244  0195 e64e              	ldab	14,y
1245  0197 2604              	bne	L726
1246                         ; 525                         CRGINT_LOCKIE = FALSE;  
1248  0199 1d000010          	bclr	__CRGINT,16
1249  019d                   L726:
1250                         ; 530             if(MCAL_DISABLE == Mcu_UserConfig_Ptr->MCU_MODE_SETTING
1250                         ; 531                                      [McuMode].Mcu_RTI_Enable_WaitMode)
1252  019d e64d              	ldab	13,y
1253  019f 2604              	bne	L136
1254                         ; 533                 CRGINT_RTIE = FALSE;  
1256  01a1 1d000080          	bclr	__CRGINT,128
1257  01a5                   L136:
1258                         ; 537                     StartWaitMode ;
1261  01a5 3e                	wai	
1263                         ; 539         break;
1266  01a6 2043              	bra	L526
1267  01a8                   L775:
1268                         ; 542         case  MCU_POWER_MODE_PSEUDO_STOP:
1268                         ; 543 
1268                         ; 544                     /*Set mode in PLLCTL register (Stop mode for RTI and COP ->
1268                         ; 545                     these could be enabled/disabled during Pseudo stop  mode,
1268                         ; 546                     based on the configuration) */
1268                         ; 547                     PLLCTL_PCE =  Mcu_UserConfig_Ptr->MCU_MODE_SETTING
1268                         ; 548                                   [McuMode].Mcu_COP_Enable_PseudoStopMode;
1270  01a8 fd0001            	ldy	L7_Mcu_UserConfig_Ptr
1271  01ab e681              	ldab	OFST+1,s
1272  01ad 8605              	ldaa	#5
1273  01af 12                	mul	
1274  01b0 19ee              	leay	d,y
1275  01b2 0f4f0106          	brclr	15,y,1,L62
1276  01b6 1c000002          	bset	__PLLCTL,2
1277  01ba 2004              	bra	L03
1278  01bc                   L62:
1279  01bc 1d000002          	bclr	__PLLCTL,2
1280  01c0                   L03:
1281                         ; 550                     PLLCTL_PRE =  Mcu_UserConfig_Ptr->MCU_MODE_SETTING
1281                         ; 551                                   [McuMode].Mcu_RTI_Enable_PseudoStopMode;
1283  01c0 fd0001            	ldy	L7_Mcu_UserConfig_Ptr
1284  01c3 e681              	ldab	OFST+1,s
1285  01c5 8605              	ldaa	#5
1286  01c7 12                	mul	
1287  01c8 19ee              	leay	d,y
1288  01ca 0fe8100106        	brclr	16,y,1,L23
1289  01cf 1c000004          	bset	__PLLCTL,4
1290  01d3 2004              	bra	L43
1291  01d5                   L23:
1292  01d5 1d000004          	bclr	__PLLCTL,4
1293  01d9                   L43:
1294                         ; 553                     CLKSEL_PSTP = TRUE;       /* Oscillator keeps running */
1296  01d9 1c000040          	bset	__CLKSEL,64
1297                         ; 555                     CRGINT_LOCKIE = FALSE;
1299                         ; 557                     StartStopMode ;
1307                         ; 559         break;
1310  01dd 2004              	bra	LC002
1311  01df                   L106:
1312                         ; 562         case  MCU_POWER_MODE_STOP:
1312                         ; 563 
1312                         ; 564                     CLKSEL_PSTP = FALSE;       /* Oscillator disabled */
1314  01df 1d000040          	bclr	__CLKSEL,64
1315                         ; 566                     CRGINT_LOCKIE = FALSE;
1317                         ; 568                     StartStopMode;
1324  01e3                   LC002:
1325  01e3 1d000010          	bclr	__CRGINT,16
1326  01e7 107f              	andcc	#127
1327  01e9 183e              	stop	
1329                         ; 570         break;
1332                         ; 573         default :
1332                         ; 574         break;
1334  01eb                   L526:
1335                         ; 580         if(TRUE == Mcu_PowerOnWakeUp)
1337  01eb f60004            	ldab	L31_Mcu_PowerOnWakeUp
1338  01ee 04210d            	dbne	b,L336
1339                         ; 582             ECUM_WKSOURCE_POWER = 1;
1341  01f1 1c000101          	bset	_WakeupSource+1,1
1342                         ; 583             EcuM_SetWakeUpEvent(WakeupSource);
1344  01f5 fc0000            	ldd	_WakeupSource
1345  01f8 3b                	pshd	
1346  01f9 160000            	jsr	_EcuM_SetWakeUpEvent
1348  01fc 1b82              	leas	2,s
1349  01fe                   L336:
1350                         ; 588 }
1353  01fe 31                	puly	
1354  01ff 3d                	rts	
1432                         ; 608 void Mcu_GetVersionInfo(Std_VersionInfoType *versioninfo)
1432                         ; 609 {
1433                         	switch	.text
1434  0200                   _Mcu_GetVersionInfo:
1436       00000000          OFST:	set	0
1439                         ; 611     if(NULL_PTR != versioninfo)
1441  0200 6cae              	std	2,-s
1442  0202 2716              	beq	L376
1443                         ; 614     	versioninfo->moduleID=MCU_MODULE_ID;
1445  0204 c665              	ldab	#101
1446  0206 ed80              	ldy	OFST+0,s
1447  0208 6b42              	stab	2,y
1448                         ; 616     	versioninfo->vendorID=MCU_VENDOR_ID;
1450  020a cc0028            	ldd	#40
1451  020d 6c40              	std	0,y
1452                         ; 618     	versioninfo->sw_major_version=MCU_SW_MAJOR_VERSION_C;
1454  020f c604              	ldab	#4
1455  0211 6b43              	stab	3,y
1456                         ; 619     	versioninfo->sw_minor_version=MCU_SW_MINOR_VERSION_C;
1458  0213 53                	decb	
1459  0214 6b44              	stab	4,y
1460                         ; 620     	versioninfo->sw_patch_version=MCU_SW_PATCH_VERSION_C;
1462  0216 c609              	ldab	#9
1463  0218 6b45              	stab	5,y
1464  021a                   L376:
1465                         ; 624 }
1468  021a 31                	puly	
1469  021b 3d                	rts	
1506                         ; 640 void Mcu_TimerInit(const Mcu_ClockType ClockSetting) 
1506                         ; 641 {
1507                         	switch	.text
1508  021c                   _Mcu_TimerInit:
1510  021c 3b                	pshd	
1511       00000000          OFST:	set	0
1514                         ; 647 if(ECT_LEGACY_TIMER ==
1514                         ; 648   (Mcu_UserConfig_Ptr->Mcu_ExtSetting[ClockSetting].Mcu_EctHwSettings.all
1514                         ; 649   & (MCAL_BIT3_MASK)))
1516  021d fd0001            	ldy	L7_Mcu_UserConfig_Ptr
1517  0220 87                	clra	
1518  0221 59                	lsld	
1519  0222 19ee              	leay	d,y
1520  0224 0ee8170814        	brset	23,y,8,L317
1521                         ; 651         ECT_TSCR2 &= ~ECT_TSCR2_PR_MASK;
1523  0229 1d000007          	bclr	__TSCR2,7
1524                         ; 653 ECT_TSCR2|=(Mcu_UserConfig_Ptr->Mcu_ExtSetting[ClockSetting].Mcu_Prescaler);
1526  022d fd0001            	ldy	L7_Mcu_UserConfig_Ptr
1527  0230 19ee              	leay	d,y
1528  0232 e6e818            	ldab	24,y
1529  0235 fa0000            	orab	__TSCR2
1530  0238 7b0000            	stab	__TSCR2
1532  023b 2011              	bra	L517
1533  023d                   L317:
1534                         ; 658 ECT_PTPSR=Mcu_UserConfig_Ptr->Mcu_ExtSetting[ClockSetting].Mcu_Prescaler;
1536  023d cd0000            	ldy	#__PTPSR
1537  0240 fe0001            	ldx	L7_Mcu_UserConfig_Ptr
1538  0243 e681              	ldab	OFST+1,s
1539  0245 87                	clra	
1540  0246 59                	lsld	
1541  0247 1ae6              	leax	d,x
1542  0249 180ae01840        	movb	24,x,0,y
1543  024e                   L517:
1544                         ; 668 ECT_TSCR1|=Mcu_UserConfig_Ptr->Mcu_ExtSetting[ClockSetting].Mcu_EctHwSettings.
1544                         ; 669      all;
1546  024e fd0001            	ldy	L7_Mcu_UserConfig_Ptr
1547  0251 e681              	ldab	OFST+1,s
1548  0253 87                	clra	
1549  0254 59                	lsld	
1550  0255 19ee              	leay	d,y
1551  0257 e6e817            	ldab	23,y
1552  025a fa0000            	orab	__TSCR1
1553  025d 7b0000            	stab	__TSCR1
1554                         ; 671 ECT_TSCR2_TCRE = FALSE;
1556  0260 1d000008          	bclr	__TSCR2,8
1557                         ; 674 }
1560  0264 31                	puly	
1561  0265 3d                	rts	
1586                         ; 690 void Mcu_SelfClockMode_Isr(void)
1586                         ; 691 {
1587                         	switch	.text
1588  0266                   _Mcu_SelfClockMode_Isr:
1592                         ; 694     CRGFLG = CRGFLG_SCMIF_MASK;
1594  0266 c602              	ldab	#2
1595  0268 7b0000            	stab	__CRGFLG
1596                         ; 697     IntFlt_F_Set_Osci_Err();
1598  026b 4a000000          	call	f_IntFlt_F_Set_Osci_Err
1600                         ; 698 }
1603  026f 3d                	rts	
1630                         ; 714 void Mcu_PllLockInterrupt_Isr (void)     
1630                         ; 715 {
1631                         	switch	.text
1632  0270                   _Mcu_PllLockInterrupt_Isr:
1636                         ; 716     if (!CRGFLG_LOCK) /* if PLL not locked */
1638  0270 1e00000809        	brset	__CRGFLG,8,L737
1639                         ; 718         (void)Mcu_InitClock(Mcu_ClkSettings); /* start PLL again */
1641  0275 f60000            	ldab	L11_Mcu_ClkSettings
1642  0278 87                	clra	
1643  0279 1600b8            	jsr	_Mcu_InitClock
1646  027c 2004              	bra	L147
1647  027e                   L737:
1648                         ; 724         Mcu_DistributePllClock();
1650  027e 1c000080          	bset	__CLKSEL,128
1651  0282                   L147:
1652                         ; 728     CRGFLG = CRGFLG_LOCKIF_MASK; 
1654  0282 c610              	ldab	#16
1655  0284 7b0000            	stab	__CRGFLG
1656                         ; 729 }
1659  0287 3d                	rts	
1683                         ; 744 void Mcu_COPWatchdog (void)     
1683                         ; 745 {
1684                         	switch	.text
1685  0288                   _Mcu_COPWatchdog:
1689                         ; 747     Mcu_COPResetFlag = TRUE; 
1691  0288 c601              	ldab	#1
1692  028a 7b0002            	stab	L3_Mcu_COPResetFlag
1693                         ; 748     _Startup();
1695  028d 160000            	jsr	__Startup
1697                         ; 750 }
1700  0290 3d                	rts	
1724                         ; 765 void Mcu_ClkMonitor (void)     
1724                         ; 766 {
1725                         	switch	.text
1726  0291                   _Mcu_ClkMonitor:
1730                         ; 768     Mcu_ClockMonitorResetFlag = TRUE; 
1732  0291 c601              	ldab	#1
1733  0293 7b0003            	stab	L5_Mcu_ClockMonitorResetFlag
1734                         ; 769     _Startup();
1736  0296 160000            	jsr	__Startup
1738                         ; 771 }
1741  0299 3d                	rts	
1823                         	xref	__Startup
1824                         	xref	f_IntFlt_F_Set_Osci_Err
1825                         	switch	.bss
1826  0000                   L11_Mcu_ClkSettings:
1827  0000 00                	ds.b	1
1828  0001                   L7_Mcu_UserConfig_Ptr:
1829  0001 0000              	ds.b	2
1830                         	xdef	_Mcu_ClkMonitor
1831                         	xdef	_Mcu_COPWatchdog
1832                         	xdef	_Mcu_PllLockInterrupt_Isr
1833                         	xdef	_Mcu_SelfClockMode_Isr
1834                         	xdef	_Mcu_GetVersionInfo
1835                         	xdef	_Mcu_SetMode
1836                         	xdef	_Mcu_InitClock
1837                         	xdef	_Mcu_InitRamSection
1838                         	xdef	_Mcu_Init
1839                         	xdef	_Mcu_TimerInit
1840                         	xdef	_Mcu_SWResetFlag
1841                         	xdef	_Mcu_Reset_Reason
1842                         	xref	_EcuM_SetWakeUpEvent
1843                         	xref	_WakeupSource
1844                         	xref	__PTPSR
1845                         	xref	__TSCR2
1846                         	xref	__TSCR1
1847                         	xref	__PLLCTL
1848                         	xref	__CLKSEL
1849                         	xref	__CRGINT
1850                         	xref	__CRGFLG
1851                         	xref	__POSTDIV
1852                         	xref	__REFDV
1853                         	xref	__SYNR
1874                         	end
