   1                         ; C Compiler for S12XE [COSMIC Software].
   2                         ; Generator V4.7.8 - 27 Jul 2007
   3                         ; Optimizer V4.6.13 - 27 Jul 2007
  37                         ; 63 void Pwm_Notification_HFL(void)
  37                         ; 64 {
  38                         	switch	.text
  39  0000                   _Pwm_Notification_HFL:
  43                         ; 66     StaggerF_Enable_Next_Ch(FL_TIM_CH);  
  45  0000 cc0003            	ldd	#3
  46  0003 4a000000          	call	f_StaggerF_Enable_Next_Ch
  48                         ; 67 }
  51  0007 3d                	rts	
  75                         ; 69 void Pwm_Notification_HFR(void)
  75                         ; 70 {
  76                         	switch	.text
  77  0008                   _Pwm_Notification_HFR:
  81                         ; 72     StaggerF_Enable_Next_Ch(FR_TIM_CH);  
  83  0008 cc0004            	ldd	#4
  84  000b 4a000000          	call	f_StaggerF_Enable_Next_Ch
  86                         ; 73 }
  89  000f 3d                	rts	
 113                         ; 75 void Pwm_Notification_HRL(void)
 113                         ; 76 {
 114                         	switch	.text
 115  0010                   _Pwm_Notification_HRL:
 119                         ; 78     StaggerF_Enable_Next_Ch(RL_TIM_CH);  
 121  0010 cc0005            	ldd	#5
 122  0013 4a000000          	call	f_StaggerF_Enable_Next_Ch
 124                         ; 79 }
 127  0017 3d                	rts	
 151                         ; 81 void Pwm_Notification_HRR(void)
 151                         ; 82 {
 152                         	switch	.text
 153  0018                   _Pwm_Notification_HRR:
 157                         ; 84     StaggerF_Enable_Next_Ch(RR_TIM_CH);  
 159  0018 cc0006            	ldd	#6
 160  001b 4a000000          	call	f_StaggerF_Enable_Next_Ch
 162                         ; 85 }
 165  001f 3d                	rts	
 177                         	xref	f_StaggerF_Enable_Next_Ch
 178                         	xdef	_Pwm_Notification_HRR
 179                         	xdef	_Pwm_Notification_HRL
 180                         	xdef	_Pwm_Notification_HFR
 181                         	xdef	_Pwm_Notification_HFL
 201                         	end
