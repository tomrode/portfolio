   1                         ; C Compiler for S12XE [COSMIC Software].
   2                         ; Generator V4.7.8 - 27 Jul 2007
   3                         ; Optimizer V4.6.13 - 27 Jul 2007
  79                         ; 232 @far void vsF_Clear(u_8Bit vs_position)
  79                         ; 233 {
  80                         .ftext:	section	.text
  81  0000                   f_vsF_Clear:
  83  0000 3b                	pshd	
  84  0001 3b                	pshd	
  85       00000002          OFST:	set	2
  88                         ; 235 	u_8Bit vs_pstn = vs_position;    // store vent position
  90  0002 6b80              	stab	OFST-2,s
  91                         ; 236 	u_8Bit hs_stat = 0;              // stores the heated seat status (HS_ACTIVE or HS_NOT_ACTIVE)
  93                         ; 239 	hs_stat = (u_8Bit) hsF_FrontHeatSeatStatus(vs_pstn);
  95  0004 87                	clra	
  96  0005 4a000000          	call	f_hsF_FrontHeatSeatStatus
  98  0009 6b81              	stab	OFST-1,s
  99                         ; 243 	if(main_SwReq_Front_c == VEH_BUS_CCN_FRONT_REAR_K){
 101  000b f60000            	ldab	_main_SwReq_Front_c
 102  000e 042110            	dbne	b,L13
 103                         ; 246 		if ( (hs_stat == HS_ACTIVE) && (canio_vent_seat_request_c[vs_pstn] == VS_NOT_PSD) )
 105  0011 e681              	ldab	OFST-1,s
 106  0013 c155              	cmpb	#85
 107  0015 261f              	bne	L53
 109  0017 e680              	ldab	OFST-2,s
 110  0019 b796              	exg	b,y
 111  001b e6ea0000          	ldab	_canio_vent_seat_request_c,y
 112                         ; 249 			vs_Flag2_c[vs_pstn].b.invld_swtch_rq_b = FALSE;
 114  001f 200e              	bra	LC001
 115  0021                   L13:
 116                         ; 254 		if ( (hs_stat == HS_ACTIVE) && (canio_hvac_vent_seat_request_c[vs_pstn] == VS_NOT_PSD) )
 118  0021 e681              	ldab	OFST-1,s
 119  0023 c155              	cmpb	#85
 120  0025 260f              	bne	L53
 122  0027 e680              	ldab	OFST-2,s
 123  0029 b796              	exg	b,y
 124  002b e6ea0000          	ldab	_canio_hvac_vent_seat_request_c,y
 125                         ; 257 			vs_Flag2_c[vs_pstn].b.invld_swtch_rq_b = FALSE;
 127  002f                   LC001:
 128  002f 2605              	bne	L53
 129  0031 0dea004e02        	bclr	_vs_Flag2_c,y,2
 130  0036                   L53:
 131                         ; 262 	if ( (diag_io_vs_lock_flags_c[vs_pstn].b.switch_rq_lock_b == FALSE) && (diag_io_vs_lock_flags_c[vs_pstn].b.led_status_lock_b == FALSE) && (diag_io_all_vs_lock_b == FALSE) )
 133  0036 e680              	ldab	OFST-2,s
 134  0038 87                	clra	
 135  0039 b746              	tfr	d,y
 136  003b 0eea00000110      	brset	_diag_io_vs_lock_flags_c,y,1,L34
 138  0041 0eea0000020a      	brset	_diag_io_vs_lock_flags_c,y,2,L34
 140  0047 1e00000105        	brset	_diag_io_lock2_flags_c,1,L34
 141                         ; 266 		vs_Flag2_c[vs_pstn].b.diag_cntrl_actv_b = FALSE; 
 143  004c 0dea004e04        	bclr	_vs_Flag2_c,y,4
 145  0051                   L34:
 146                         ; 274 	vs_prm_set[vs_pstn].crt_lv_c            = 0;        // vs current level is 0
 148  0051 860f              	ldaa	#15
 149  0053 12                	mul	
 150  0054 b746              	tfr	d,y
 151  0056 87                	clra	
 152  0057 6aea0003          	staa	_vs_prm_set,y
 153                         ; 276 	vs_prm_set[vs_pstn].LED_dsply_cnt       = 0;        // LED display time
 155  005b c7                	clrb	
 156  005c 6cea000b          	std	_vs_prm_set+8,y
 157  0060 6cea000d          	std	_vs_prm_set+10,y
 158                         ; 277 	vs_prm_set[vs_pstn].pwm_w               = 0;        // 0 PWM
 160                         ; 278 	vs_prm_set[vs_pstn].strt_up_cnt         = 0;        // clear vs start up counter
 162  0064 1869ea0007        	clrw	_vs_prm_set+4,y
 163                         ; 283 	vs_Flag1_c[vs_pstn].b.sw_rq_b           = FALSE;    // vs software request is off
 165  0069 e680              	ldab	OFST-2,s
 166  006b b746              	tfr	d,y
 167                         ; 284 	vs_Flag1_c[vs_pstn].b.LED_off_b         = FALSE;    // clear LED off flag
 169                         ; 285 	vs_Flag1_c[vs_pstn].b.start_up_cmplt_b  = FALSE;    // Start up complete falg is cleared
 171  006d 0dea00500e        	bclr	_vs_Flag1_c,y,14
 172                         ; 287 	vs_Flag2_c[vs_pstn].b.swtch_rq_prps_b   = FALSE;    // switch purpose is to turn off
 174                         ; 289 	vs_Flag2_c[vs_pstn].b.diag_clr_ontime_b = FALSE;    // resets the ontime to zero
 176  0072 0dea004e09        	bclr	_vs_Flag2_c,y,9
 177                         ; 292 	if ( (canio_RX_IGN_STATUS_c != IGN_RUN) && (canio_RX_IGN_STATUS_c != IGN_START) )
 179  0077 f60000            	ldab	_canio_RX_IGN_STATUS_c
 180  007a c104              	cmpb	#4
 181  007c 2754              	beq	L74
 183  007e c105              	cmpb	#5
 184  0080 2750              	beq	L74
 185                         ; 294 		vs_prm_set[vs_pstn].lst_swtch_rq_c      = 0;           // clear the last switch request
 187  0082 e680              	ldab	OFST-2,s
 188  0084 860f              	ldaa	#15
 189  0086 12                	mul	
 190  0087 b746              	tfr	d,y
 191  0089 87                	clra	
 192  008a 6aea000f          	staa	_vs_prm_set+12,y
 193                         ; 295 		vs_prm_set[vs_pstn].flt_mtr_cnt         = 0;           // clear vs fault mature time
 195  008e c7                	clrb	
 196  008f 6cea0009          	std	_vs_prm_set+6,y
 197                         ; 296 		vs_prm_set[vs_pstn].Vsense_dly_cnt      = 0;           // clear vs Vsense delay time
 199                         ; 297 		vs_prm_set[vs_pstn].flt_stat_c          = VS_NO_FLT_MASK; // no vs faults (open, short etc.)
 201  0093 6cea0005          	std	_vs_prm_set+2,y
 202                         ; 302 		vs_prm_set[vs_pstn].stat_c              = GOOD;        // vs status is good
 204  0097 52                	incb	
 205  0098 6bea0004          	stab	_vs_prm_set+1,y
 206                         ; 304 		vs_Flag1_c[vs_pstn].b.RemSt_auto_b      = FALSE;       // Clear remote start automatic activation bit
 208  009c e680              	ldab	OFST-2,s
 209  009e b746              	tfr	d,y
 210  00a0 0dea005010        	bclr	_vs_Flag1_c,y,16
 211                         ; 305 		vs_Flag2_c[vs_pstn].b.diag_cntrl_actv_b = FALSE;       // diagnostic IO control is inactive
 213                         ; 306 		vs_Flag2_c[vs_pstn].b.invld_swtch_rq_b  = FALSE;       // clear invalid switch flag
 215  00a5 0dea004e06        	bclr	_vs_Flag2_c,y,6
 216                         ; 308 		diag_io_vs_rq_c[vs_pstn]                = 0;           // clear diagnostic request  
 218  00aa 69ea0000          	clr	_diag_io_vs_rq_c,y
 219                         ; 309 		diag_io_vs_state_c[vs_pstn]             = 0;           // clear diagnostic LED state
 221  00ae 69ea0000          	clr	_diag_io_vs_state_c,y
 222                         ; 310 		diag_io_vs_all_out_c                    = 0;           // clear diagnostic all vs request
 224  00b2 790000            	clr	_diag_io_vs_all_out_c
 225                         ; 312 		vs_Vsense_c[vs_pstn]                    = 0;           // Clear Vent voltage reading
 227  00b5 69ea004c          	clr	_vs_Vsense_c,y
 228                         ; 313 		vs_Isense_c[vs_pstn]                    = 0;           // Clear Vent current reading
 230  00b9 69ea004a          	clr	_vs_Isense_c,y
 231                         ; 315 		vs_shrtTo_batt_mntr_c                   = 0;           // turn off the shrt_to_batt monitor
 233  00bd 790049            	clr	_vs_shrtTo_batt_mntr_c
 234                         ; 317 		vs_slctd_drive_c                        = VS_LMT;      // Select Both vents (No remote start)
 236  00c0 c602              	ldab	#2
 237  00c2 7b0025            	stab	_vs_slctd_drive_c
 238                         ; 319 		vs_lst_main_state_c                     = NORMAL;      // Last main state is defaulted to normal
 240  00c5 790023            	clr	_vs_lst_main_state_c
 241                         ; 322 		Dio_WriteChannel(VENT_EN, STD_LOW);
 244  00c8 1410              	sei	
 249  00ca fe0014            	ldx	_Dio_PortWrite_Ptr+20
 250  00cd 0d0004            	bclr	0,x,4
 254  00d0 10ef              	cli	
 258  00d2                   L74:
 259                         ; 328 } //End of function
 262  00d2 1b84              	leas	4,s
 263  00d4 0a                	rtc	
 303                         ; 346 @far void vsF_Init(void)
 303                         ; 347 {
 304                         	switch	.ftext
 305  00d5                   f_vsF_Init:
 307  00d5 37                	pshb	
 308       00000001          OFST:	set	1
 311                         ; 352 	for (a=0; a <VS_LMT; a++)
 313  00d6 c7                	clrb	
 314  00d7 6b80              	stab	OFST-1,s
 315  00d9                   L56:
 316                         ; 354 		vs_prm_set[a].flt_stat_c = VS_NO_FLT_MASK;     // no vs faults
 318  00d9 860f              	ldaa	#15
 319  00db 12                	mul	
 320  00dc b746              	tfr	d,y
 321  00de 69ea0005          	clr	_vs_prm_set+2,y
 322                         ; 355 		vs_prm_set[a].stat_c     = GOOD;               // vs status is good
 324  00e2 c601              	ldab	#1
 325  00e4 6bea0004          	stab	_vs_prm_set+1,y
 326                         ; 352 	for (a=0; a <VS_LMT; a++)
 328  00e8 6280              	inc	OFST-1,s
 331  00ea e680              	ldab	OFST-1,s
 332  00ec c102              	cmpb	#2
 333  00ee 25e9              	blo	L56
 334                         ; 361 	EE_BlockRead(EE_VENT_PWM_DATA, (u_8Bit*)&Vs_cal_prms);
 336  00f0 cc0037            	ldd	#_Vs_cal_prms
 337  00f3 3b                	pshd	
 338  00f4 cc0007            	ldd	#7
 339  00f7 4a000000          	call	f_EE_BlockRead
 341                         ; 364 	EE_BlockRead(EE_VENT_FLT_CAL_DATA, (u_8Bit*)&Vs_flt_cal_prms);
 343  00fb cc002f            	ldd	#_Vs_flt_cal_prms
 344  00fe 6c80              	std	0,s
 345  0100 cc0008            	ldd	#8
 346  0103 4a000000          	call	f_EE_BlockRead
 348                         ; 368 	vs_LED_dsply_tm = (MAIN_LED_DSPLY_TM * VS_TM_MDLTN); // vs shortTerm LED display (2 seconds in 10ms tmslc)
 350  0107 cc00c8            	ldd	#200
 351  010a 7c002d            	std	_vs_LED_dsply_tm
 352                         ; 369 	vs_flt_mtr_tm =   Vs_flt_cal_prms.vs_2sec_mature_time; //vs fault mature time (2 seconds in 80ms tmslc)
 354  010d f60035            	ldab	_Vs_flt_cal_prms+6
 355  0110 7c002b            	std	_vs_flt_mtr_tm
 356                         ; 372 	vs_slct = VS_FR;
 358  0113 c601              	ldab	#1
 359  0115 7b0026            	stab	_vs_slct
 360                         ; 375 	vs_slctd_drive_c = VS_LMT;
 362  0118 52                	incb	
 363  0119 7b0025            	stab	_vs_slctd_drive_c
 364                         ; 378 	vs_lst_main_state_c = NORMAL;
 366  011c 790023            	clr	_vs_lst_main_state_c
 367                         ; 379 }
 370  011f 1b83              	leas	3,s
 371  0121 0a                	rtc	
 397                         ; 402 @far void vsF_AD_Cnvrsn(void)
 397                         ; 403 {
 398                         	switch	.ftext
 399  0122                   f_vsF_AD_Cnvrsn:
 403                         ; 405 	ADF_AtoD_Cnvrsn(vs_AD_prms.slctd_adCH_c);
 405  0122 f60001            	ldab	_vs_AD_prms+1
 406  0125 87                	clra	
 407  0126 160000            	jsr	_ADF_AtoD_Cnvrsn
 409                         ; 408 	ADF_Str_UnFlt_Mux_Rslt(vs_AD_prms.slctd_adCH_c, vs_AD_prms.slctd_muxCH_c);
 411  0129 f60000            	ldab	_vs_AD_prms
 412  012c 87                	clra	
 413  012d 3b                	pshd	
 414  012e f60001            	ldab	_vs_AD_prms+1
 415  0131 160000            	jsr	_ADF_Str_UnFlt_Mux_Rslt
 417                         ; 411 	ADF_Mux_Filter(vs_AD_prms.slctd_mux_c, vs_AD_prms.slctd_muxCH_c);
 419  0134 f60000            	ldab	_vs_AD_prms
 420  0137 87                	clra	
 421  0138 6c80              	std	0,s
 422  013a f60002            	ldab	_vs_AD_prms+2
 423  013d 160000            	jsr	_ADF_Mux_Filter
 425  0140 1b82              	leas	2,s
 426                         ; 412 }
 429  0142 0a                	rtc	
 476                         ; 437 @far void vsF_Vsense_AD_Rdg(u_8Bit vs_postn)
 476                         ; 438 {
 477                         	switch	.ftext
 478  0143                   f_vsF_Vsense_AD_Rdg:
 480  0143 3b                	pshd	
 481  0144 3b                	pshd	
 482       00000002          OFST:	set	2
 485                         ; 440 	u_8Bit vs_pos = vs_postn;    // store vent position
 487  0145 6b81              	stab	OFST-1,s
 488                         ; 443 	if (vs_pos == VS_FL) {		
 490  0147 2604              	bne	L121
 491                         ; 445 		vs_AD_prms.slctd_muxCH_c = CH4;
 493  0149 c604              	ldab	#4
 495  014b 2002              	bra	L321
 496  014d                   L121:
 497                         ; 449 		vs_AD_prms.slctd_muxCH_c = CH5;
 499  014d c605              	ldab	#5
 500  014f                   L321:
 501  014f 7b0000            	stab	_vs_AD_prms
 502                         ; 452 	Dio_WriteChannelGroup(MuxSelGroup, vs_AD_prms.slctd_muxCH_c);
 505  0152 1410              	sei	
 510  0154 f60000            	ldab	_DioConfigData
 511  0157 87                	clra	
 512  0158 59                	lsld	
 513  0159 b746              	tfr	d,y
 514  015b f60000            	ldab	_vs_AD_prms
 515  015e b60001            	ldaa	_DioConfigData+1
 516  0161 2704              	beq	L41
 517  0163                   L61:
 518  0163 58                	lslb	
 519  0164 0430fc            	dbne	a,L61
 520  0167                   L41:
 521  0167 f40002            	andb	_DioConfigData+2
 522  016a 6b80              	stab	OFST-2,s
 523  016c f60000            	ldab	_DioConfigData
 524  016f 87                	clra	
 525  0170 59                	lsld	
 526  0171 b745              	tfr	d,x
 527  0173 f60002            	ldab	_DioConfigData+2
 528  0176 51                	comb	
 529  0177 e4e30000          	andb	[_Dio_PortWrite_Ptr,x]
 530  017b ea80              	orab	OFST-2,s
 531  017d 6beb0000          	stab	[_Dio_PortWrite_Ptr,y]
 535  0181 10ef              	cli	
 537                         ; 455 	vsF_AD_Cnvrsn();
 540  0183 4a012222          	call	f_vsF_AD_Cnvrsn
 542                         ; 458 	vs_Vsense_c[vs_pos] = ADF_Nrmlz_Vs_Vsense(AD_mux_Filter[vs_AD_prms.slctd_mux_c][vs_AD_prms.slctd_muxCH_c].flt_rslt_w);
 544  0187 e681              	ldab	OFST-1,s
 545  0189 87                	clra	
 546  018a 3b                	pshd	
 547  018b f60000            	ldab	_vs_AD_prms
 548  018e 8606              	ldaa	#6
 549  0190 12                	mul	
 550  0191 b746              	tfr	d,y
 551  0193 f60002            	ldab	_vs_AD_prms+2
 552  0196 8630              	ldaa	#48
 553  0198 12                	mul	
 554  0199 19ee              	leay	d,y
 555  019b ecea0004          	ldd	_AD_mux_Filter+4,y
 556  019f 160000            	jsr	_ADF_Nrmlz_Vs_Vsense
 558  01a2 31                	puly	
 559  01a3 6bea004c          	stab	_vs_Vsense_c,y
 560                         ; 459 }
 563  01a7 1b84              	leas	4,s
 564  01a9 0a                	rtc	
 650                         ; 493 @far void vsF_Isense_AD_Rdg(u_8Bit vs_pstion, Vs_Isense_type_e Vs_Isense_Rdg_type)
 650                         ; 494 {
 651                         	switch	.ftext
 652  01aa                   f_vsF_Isense_AD_Rdg:
 654  01aa 3b                	pshd	
 655  01ab 1b9c              	leas	-4,s
 656       00000004          OFST:	set	4
 659                         ; 496 	u_8Bit  vs_ps = vs_pstion;       // store vent position
 661  01ad 6b81              	stab	OFST-3,s
 662                         ; 497 	u_16Bit vs_interim_Isense_w = 0; // Intermediate result for the On Isense
 664                         ; 500 	if (vs_ps == VS_FL) {
 666  01af 2604              	bne	L161
 667                         ; 502 		vs_AD_prms.slctd_muxCH_c = CH6;
 669  01b1 c606              	ldab	#6
 671  01b3 2002              	bra	L361
 672  01b5                   L161:
 673                         ; 506 		vs_AD_prms.slctd_muxCH_c = CH7;
 675  01b5 c607              	ldab	#7
 676  01b7                   L361:
 677  01b7 7b0000            	stab	_vs_AD_prms
 678                         ; 509 	Dio_WriteChannelGroup(MuxSelGroup, vs_AD_prms.slctd_muxCH_c);
 681  01ba 1410              	sei	
 686  01bc f60000            	ldab	_DioConfigData
 687  01bf 87                	clra	
 688  01c0 59                	lsld	
 689  01c1 b746              	tfr	d,y
 690  01c3 f60000            	ldab	_vs_AD_prms
 691  01c6 b60001            	ldaa	_DioConfigData+1
 692  01c9 2704              	beq	L22
 693  01cb                   L42:
 694  01cb 58                	lslb	
 695  01cc 0430fc            	dbne	a,L42
 696  01cf                   L22:
 697  01cf f40002            	andb	_DioConfigData+2
 698  01d2 6b80              	stab	OFST-4,s
 699  01d4 f60000            	ldab	_DioConfigData
 700  01d7 87                	clra	
 701  01d8 59                	lsld	
 702  01d9 b745              	tfr	d,x
 703  01db f60002            	ldab	_DioConfigData+2
 704  01de 51                	comb	
 705  01df e4e30000          	andb	[_Dio_PortWrite_Ptr,x]
 706  01e3 ea80              	orab	OFST-4,s
 707  01e5 6beb0000          	stab	[_Dio_PortWrite_Ptr,y]
 711  01e9 10ef              	cli	
 713                         ; 512 	vsF_AD_Cnvrsn();
 716  01eb 4a012222          	call	f_vsF_AD_Cnvrsn
 718                         ; 515 	if (Vs_Isense_Rdg_type == VS_ON_ISENSE) {
 720  01ef e68a              	ldab	OFST+6,s
 721  01f1 042146            	dbne	b,L561
 722                         ; 517 		vs_interim_Isense_w = AD_mux_Filter[vs_AD_prms.slctd_mux_c][vs_AD_prms.slctd_muxCH_c].flt_rslt_w;
 724  01f4 f60000            	ldab	_vs_AD_prms
 725  01f7 8606              	ldaa	#6
 726  01f9 12                	mul	
 727  01fa b746              	tfr	d,y
 728  01fc f60002            	ldab	_vs_AD_prms+2
 729  01ff 8630              	ldaa	#48
 730  0201 12                	mul	
 731  0202 19ee              	leay	d,y
 732  0204 1802ea000482      	movw	_AD_mux_Filter+4,y,OFST-2,s
 733                         ; 520 		if (vs_interim_Isense_w > vs_Isense_offset_w[vs_ps]) {
 735  020a e681              	ldab	OFST-3,s
 736  020c 87                	clra	
 737  020d 59                	lsld	
 738  020e b746              	tfr	d,y
 739  0210 ecea0027          	ldd	_vs_Isense_offset_w,y
 740  0214 ac82              	cpd	OFST-2,s
 741  0216 2418              	bhs	L761
 742                         ; 523 			vs_interim_Isense_w -= vs_Isense_offset_w[vs_ps];
 744  0218 ec82              	ldd	OFST-2,s
 745  021a a3ea0027          	subd	_vs_Isense_offset_w,y
 746  021e 6c82              	std	OFST-2,s
 747                         ; 526 			vs_Isense_c[vs_ps] = ADF_Nrmlz_Vs_Isense(vs_interim_Isense_w);
 749  0220 e681              	ldab	OFST-3,s
 750  0222 87                	clra	
 751  0223 3b                	pshd	
 752  0224 ec84              	ldd	OFST+0,s
 753  0226 160000            	jsr	_ADF_Nrmlz_Vs_Isense
 755  0229 31                	puly	
 756  022a 6bea004a          	stab	_vs_Isense_c,y
 758  022e 2028              	bra	L371
 759  0230                   L761:
 760                         ; 530 			vs_Isense_c[vs_ps] = 0;
 762  0230 e681              	ldab	OFST-3,s
 763  0232 b796              	exg	b,y
 764  0234 69ea004a          	clr	_vs_Isense_c,y
 765  0238 201e              	bra	L371
 766  023a                   L561:
 767                         ; 535 		vs_Isense_offset_w[vs_ps] = AD_mux_Filter[vs_AD_prms.slctd_mux_c][vs_AD_prms.slctd_muxCH_c].flt_rslt_w;
 769  023a e681              	ldab	OFST-3,s
 770  023c 87                	clra	
 771  023d 59                	lsld	
 772  023e b746              	tfr	d,y
 773  0240 f60000            	ldab	_vs_AD_prms
 774  0243 8606              	ldaa	#6
 775  0245 12                	mul	
 776  0246 b745              	tfr	d,x
 777  0248 f60002            	ldab	_vs_AD_prms+2
 778  024b 8630              	ldaa	#48
 779  024d 12                	mul	
 780  024e 1ae6              	leax	d,x
 781  0250 1802e20004ea0027  	movw	_AD_mux_Filter+4,x,_vs_Isense_offset_w,y
 782  0258                   L371:
 783                         ; 537 }
 786  0258 1b86              	leas	6,s
 787  025a 0a                	rtc	
 824                         ; 561 @far void vsF_ADC_Monitor(void)
 824                         ; 562 {
 825                         	switch	.ftext
 826  025b                   f_vsF_ADC_Monitor:
 828  025b 37                	pshb	
 829       00000001          OFST:	set	1
 832                         ; 567 	vs_AD_prms.slctd_adCH_c = CH5;        // set selected AD channel to CH5
 834  025c c605              	ldab	#5
 835  025e 7b0001            	stab	_vs_AD_prms+1
 836                         ; 568 	vs_AD_prms.slctd_mux_c  = MUX2;       // set selected mux to MUX2
 838  0261 c601              	ldab	#1
 839  0263 7b0002            	stab	_vs_AD_prms+2
 840                         ; 571 	for (dd=0; dd<VS_LMT; dd++) {
 842  0266 c7                	clrb	
 843  0267 6b80              	stab	OFST-1,s
 844  0269                   L112:
 845                         ; 573 		vsF_Vsense_AD_Rdg(dd);
 847  0269 87                	clra	
 848  026a 4a014343          	call	f_vsF_Vsense_AD_Rdg
 850                         ; 576 		if ( (vs_Flag1_c[dd].b.sw_rq_b == TRUE) && (relay_A_Ok_b == TRUE) ) {
 852  026e e680              	ldab	OFST-1,s
 853  0270 87                	clra	
 854  0271 b746              	tfr	d,y
 855  0273 0fea00500212      	brclr	_vs_Flag1_c,y,2,L712
 857  0279 1f0000400d        	brclr	_relay_status_c,64,L712
 858                         ; 579 			vsF_Isense_AD_Rdg(dd, VS_ON_ISENSE);
 860  027e c601              	ldab	#1
 861  0280 3b                	pshd	
 862  0281 e682              	ldab	OFST+1,s
 863  0283 4a01aaaa          	call	f_vsF_Isense_AD_Rdg
 865  0287 1b82              	leas	2,s
 867  0289 2013              	bra	L122
 868  028b                   L712:
 869                         ; 583 			vsF_Isense_AD_Rdg(dd, VS_OFFSET_ISENSE);
 871  028b 87                	clra	
 872  028c c7                	clrb	
 873  028d 3b                	pshd	
 874  028e e682              	ldab	OFST+1,s
 875  0290 4a01aaaa          	call	f_vsF_Isense_AD_Rdg
 877  0294 1b82              	leas	2,s
 878                         ; 586 			vs_Isense_c[dd] = 0;
 880  0296 e680              	ldab	OFST-1,s
 881  0298 b796              	exg	b,y
 882  029a 69ea004a          	clr	_vs_Isense_c,y
 883  029e                   L122:
 884                         ; 571 	for (dd=0; dd<VS_LMT; dd++) {
 886  029e 6280              	inc	OFST-1,s
 889  02a0 e680              	ldab	OFST-1,s
 890  02a2 c102              	cmpb	#2
 891  02a4 25c3              	blo	L112
 892                         ; 589 }
 895  02a6 1b81              	leas	1,s
 896  02a8 0a                	rtc	
 947                         ; 640 @far void vsF_Updt_Status(u_8Bit vs_location)
 947                         ; 641 {
 948                         	switch	.ftext
 949  02a9                   f_vsF_Updt_Status:
 951  02a9 3b                	pshd	
 952  02aa 3b                	pshd	
 953       00000002          OFST:	set	2
 956                         ; 643 	u_8Bit vs_p = vs_location;   // store vent position
 958  02ab 6b80              	stab	OFST-2,s
 959                         ; 647 	vs_prv_stat_c = vs_prm_set[vs_p].stat_c;
 961  02ad 860f              	ldaa	#15
 962  02af 12                	mul	
 963  02b0 b746              	tfr	d,y
 964  02b2 180aea000481      	movb	_vs_prm_set+1,y,OFST-1,s
 965                         ; 651 	if ( (relay_A_internal_Fault_b == FALSE) && (vs_shrtTo_batt_mntr_c != VS_SHRT_TO_BATT) && (vs_prm_set[vs_p].flt_stat_c <= VS_PRL_FLT_MAX) && (hs_status_flags_c[FRONT_LEFT].b.hs_final_short_bat_b == FALSE) && (hs_status_flags_c[FRONT_RIGHT].b.hs_final_short_bat_b == FALSE) ) {
 967  02b8 1e00001025        	brset	_relay_control_c,16,L342
 969  02bd f60049            	ldab	_vs_shrtTo_batt_mntr_c
 970  02c0 c102              	cmpb	#2
 971  02c2 271e              	beq	L342
 973  02c4 e680              	ldab	OFST-2,s
 974  02c6 860f              	ldaa	#15
 975  02c8 12                	mul	
 976  02c9 b746              	tfr	d,y
 977  02cb e6ea0005          	ldab	_vs_prm_set+2,y
 978  02cf c107              	cmpb	#7
 979  02d1 220f              	bhi	L342
 981  02d3 1e0000200a        	brset	_hs_status_flags_c,32,L342
 983  02d8 1e00012005        	brset	_hs_status_flags_c+1,32,L342
 984                         ; 654 		vs_prm_set[vs_p].stat_c = main_CSWM_Status_c;
 986  02dd f60000            	ldab	_main_CSWM_Status_c
 988  02e0 2009              	bra	L542
 989  02e2                   L342:
 990                         ; 658 		vs_prm_set[vs_p].stat_c = UGLY;
 992  02e2 e680              	ldab	OFST-2,s
 993  02e4 860f              	ldaa	#15
 994  02e6 12                	mul	
 995  02e7 b746              	tfr	d,y
 996  02e9 c603              	ldab	#3
 997  02eb                   L542:
 998  02eb 6bea0004          	stab	_vs_prm_set+1,y
 999                         ; 661 	if ( (vs_prv_stat_c != UGLY) && (vs_prm_set[vs_p].stat_c == UGLY)) {
1001  02ef e681              	ldab	OFST-1,s
1002  02f1 c103              	cmpb	#3
1003  02f3 2718              	beq	L742
1005  02f5 e680              	ldab	OFST-2,s
1006  02f7 860f              	ldaa	#15
1007  02f9 12                	mul	
1008  02fa b746              	tfr	d,y
1009  02fc e6ea0004          	ldab	_vs_prm_set+1,y
1010  0300 c103              	cmpb	#3
1011  0302 2609              	bne	L742
1012                         ; 663 		vs_Flag1_c[vs_p].b.LED_off_b = TRUE;
1014  0304 e680              	ldab	OFST-2,s
1015  0306 b796              	exg	b,y
1016  0308 0cea005004        	bset	_vs_Flag1_c,y,4
1017  030d                   L742:
1018                         ; 665 }
1021  030d 1b84              	leas	4,s
1022  030f 0a                	rtc	
1078                         ; 687 @far void vsF_Nrmlz_PWM(u_8Bit vs_spot)
1078                         ; 688 {
1079                         	switch	.ftext
1080  0310                   f_vsF_Nrmlz_PWM:
1082  0310 3b                	pshd	
1083  0311 3b                	pshd	
1084       00000002          OFST:	set	2
1087                         ; 690 	u_8Bit vs_spt = vs_spot;    // store vent spot
1089  0312 6b81              	stab	OFST-1,s
1090                         ; 692 	u_8Bit vs_duty_cycle_c = 0; // local variable to store the vent pwm duty cycle
1092                         ; 695 	vs_pwm_lvl_c = vs_prm_set[vs_spt].crt_lv_c;
1094  0314 860f              	ldaa	#15
1095  0316 12                	mul	
1096  0317 b746              	tfr	d,y
1097  0319 e6ea0003          	ldab	_vs_prm_set,y
1098  031d 6b80              	stab	OFST-2,s
1099                         ; 698 	if (vs_pwm_lvl_c == VL_LOW) {
1101  031f c101              	cmpb	#1
1102  0321 2610              	bne	L372
1103                         ; 700 		vs_duty_cycle_c = Vs_cal_prms.cals[hs_vehicle_line_c].PWM_a[1];
1105  0323 f60000            	ldab	_hs_vehicle_line_c
1106  0326 87                	clra	
1107  0327 cd0003            	ldy	#3
1108  032a 13                	emul	
1109  032b b746              	tfr	d,y
1110  032d e6ea0038          	ldab	_Vs_cal_prms+1,y
1112  0331 2012              	bra	LC002
1113  0333                   L372:
1114                         ; 703 		if (vs_pwm_lvl_c == VL_HI) {
1116  0333 c103              	cmpb	#3
1117  0335 2612              	bne	L772
1118                         ; 705 			vs_duty_cycle_c = Vs_cal_prms.cals[hs_vehicle_line_c].PWM_a[2];
1120  0337 f60000            	ldab	_hs_vehicle_line_c
1121  033a 87                	clra	
1122  033b cd0003            	ldy	#3
1123  033e 13                	emul	
1124  033f b746              	tfr	d,y
1125  0341 e6ea0039          	ldab	_Vs_cal_prms+2,y
1126  0345                   LC002:
1127  0345 6b80              	stab	OFST-2,s
1129  0347 2002              	bra	L572
1130  0349                   L772:
1131                         ; 709 			vs_duty_cycle_c = 0;
1133  0349 6980              	clr	OFST-2,s
1134  034b                   L572:
1135                         ; 713 	vs_prm_set[vs_spt].pwm_w = mainF_Calc_PWM(vs_duty_cycle_c);
1137  034b e681              	ldab	OFST-1,s
1138  034d 860f              	ldaa	#15
1139  034f 12                	mul	
1140  0350 3b                	pshd	
1141  0351 e682              	ldab	OFST+0,s
1142  0353 87                	clra	
1143  0354 160000            	jsr	_mainF_Calc_PWM
1145  0357 31                	puly	
1146  0358 6cea000d          	std	_vs_prm_set+10,y
1147                         ; 714 }
1150  035c 1b84              	leas	4,s
1151  035e 0a                	rtc	
1191                         ; 758 @far void vsF_Updt_Crrt_Lvl(u_8Bit vs_spot_c)
1191                         ; 759 {
1192                         	switch	.ftext
1193  035f                   f_vsF_Updt_Crrt_Lvl:
1195  035f 3b                	pshd	
1196  0360 37                	pshb	
1197       00000001          OFST:	set	1
1200                         ; 761 	u_8Bit vs_st = vs_spot_c;    // store vent position
1202  0361 6b80              	stab	OFST-1,s
1203                         ; 764 	switch (vs_prm_set[vs_st].crt_lv_c) {
1205  0363 860f              	ldaa	#15
1206  0365 12                	mul	
1207  0366 b746              	tfr	d,y
1208  0368 e6ea0003          	ldab	_vs_prm_set,y
1210  036c 270d              	beq	L303
1211  036e c003              	subb	#3
1212  0370 2714              	beq	L503
1213                         ; 782 			vsF_Clear(vs_st);
1215  0372 e680              	ldab	OFST-1,s
1216  0374 87                	clra	
1217  0375 4a000000          	call	f_vsF_Clear
1219  0379 2018              	bra	L133
1220  037b                   L303:
1221                         ; 769 			vs_prm_set[vs_st].crt_lv_c = VL_HI;
1223  037b e680              	ldab	OFST-1,s
1224  037d 860f              	ldaa	#15
1225  037f 12                	mul	
1226  0380 b746              	tfr	d,y
1227  0382 c603              	ldab	#3
1228                         ; 770 			break;
1230  0384 2009              	bra	LC003
1231  0386                   L503:
1232                         ; 776 			vs_prm_set[vs_st].crt_lv_c = VL_LOW;
1234  0386 e680              	ldab	OFST-1,s
1235  0388 860f              	ldaa	#15
1236  038a 12                	mul	
1237  038b b746              	tfr	d,y
1238  038d c601              	ldab	#1
1239  038f                   LC003:
1240  038f 6bea0003          	stab	_vs_prm_set,y
1241                         ; 777 			break;
1243  0393                   L133:
1244                         ; 785 }
1247  0393 1b83              	leas	3,s
1248  0395 0a                	rtc	
1288                         ; 805 @far u_8Bit vsF_Get_Output_Stat(u_8Bit vs_site)
1288                         ; 806 {
1289                         	switch	.ftext
1290  0396                   f_vsF_Get_Output_Stat:
1292       00000001          OFST:	set	1
1295                         ; 808 	u_8Bit vs_ste = vs_site;        // store vent position
1297                         ; 811 	return(vs_prm_set[vs_ste].crt_lv_c);
1299  0396 860f              	ldaa	#15
1300  0398 12                	mul	
1301  0399 b746              	tfr	d,y
1302  039b e6ea0003          	ldab	_vs_prm_set,y
1305  039f 0a                	rtc	
1346                         ; 834 @far void vsF_Motor_StrtUp(u_8Bit vs_motor_pos)
1346                         ; 835 {
1347                         	switch	.ftext
1348  03a0                   f_vsF_Motor_StrtUp:
1350  03a0 3b                	pshd	
1351  03a1 37                	pshb	
1352       00000001          OFST:	set	1
1355                         ; 837 	u_8Bit vs_motor_loc = vs_motor_pos;      // motor activation location
1357  03a2 6b80              	stab	OFST-1,s
1358                         ; 840 	if (vs_Flag1_c[vs_motor_loc].b.start_up_cmplt_b == FALSE) {
1360  03a4 87                	clra	
1361  03a5 b746              	tfr	d,y
1362  03a7 860f              	ldaa	#15
1363  03a9 12                	mul	
1364  03aa 0eea00500824      	brset	_vs_Flag1_c,y,8,L763
1365                         ; 843 		if (vs_prm_set[vs_motor_loc].strt_up_cnt < Vs_flt_cal_prms.start_up_tm) {
1367  03b0 b746              	tfr	d,y
1368  03b2 ecea0007          	ldd	_vs_prm_set+4,y
1369  03b6 bc0033            	cpd	_Vs_flt_cal_prms+4
1370  03b9 240e              	bhs	L173
1371                         ; 846 			vs_prm_set[vs_motor_loc].strt_up_cnt++;
1373  03bb 1862ea0007        	incw	_vs_prm_set+4,y
1374                         ; 849 			vs_prm_set[vs_motor_loc].pwm_w = MAIN_MAX_PWM;
1376  03c0 cc8000            	ldd	#-32768
1377  03c3 6cea000d          	std	_vs_prm_set+10,y
1379  03c7 201b              	bra	L573
1380  03c9                   L173:
1381                         ; 853 			vs_Flag1_c[vs_motor_loc].b.start_up_cmplt_b = TRUE;
1383  03c9 e680              	ldab	OFST-1,s
1384  03cb b796              	exg	b,y
1385  03cd 0cea005008        	bset	_vs_Flag1_c,y,8
1386  03d2 2010              	bra	L573
1387  03d4                   L763:
1388                         ; 858 		vs_prm_set[vs_motor_loc].strt_up_cnt = 0; 
1390  03d4 b746              	tfr	d,y
1391  03d6 1869ea0007        	clrw	_vs_prm_set+4,y
1392                         ; 861 		vs_Flag1_c[vs_motor_loc].b.state_b = TRUE;
1394  03db e680              	ldab	OFST-1,s
1395  03dd b796              	exg	b,y
1396  03df 0cea005001        	bset	_vs_Flag1_c,y,1
1397  03e4                   L573:
1398                         ; 863 }
1401  03e4 1b83              	leas	3,s
1402  03e6 0a                	rtc	
1444                         ; 885 @far void vsF_Diag_IO_Mngmnt(u_8Bit vs_site_c)
1444                         ; 886 {
1445                         	switch	.ftext
1446  03e7                   f_vsF_Diag_IO_Mngmnt:
1448  03e7 3b                	pshd	
1449  03e8 37                	pshb	
1450       00000001          OFST:	set	1
1453                         ; 888 	u_8Bit vs_po = vs_site_c;        // store vent position
1455  03e9 6b80              	stab	OFST-1,s
1456                         ; 891 	if(diag_rc_all_op_lock_b == TRUE){
1458  03eb 87                	clra	
1459  03ec 1f0000010b        	brclr	_diag_rc_lock_flags_c,1,L514
1460                         ; 893 		if(diag_rc_all_vents_lock_b == TRUE){			
1462  03f1 1f00000404        	brclr	_diag_rc_lock_flags_c,4,L714
1463                         ; 895 			vs_Flag2_c[vs_po].b.swtch_rq_prps_b = TRUE;
1465  03f6 b746              	tfr	d,y
1467  03f8 2016              	bra	L134
1468  03fa                   L714:
1469                         ; 899 			vs_Flag2_c[vs_po].b.swtch_rq_prps_b = FALSE;
1471  03fa 201d              	bra	LC004
1472  03fc                   L514:
1473                         ; 904 		if (diag_io_vs_lock_flags_c[vs_po].b.led_status_lock_b == FALSE) {
1475  03fc b746              	tfr	d,y
1476  03fe 0eea0000021c      	brset	_diag_io_vs_lock_flags_c,y,2,L324
1477                         ; 907 			if ( (diag_io_vs_rq_c[vs_po] == VL_LOW) || (diag_io_vs_rq_c[vs_po] == VL_HI) ) {
1479  0404 e6ea0000          	ldab	_diag_io_vs_rq_c,y
1480  0408 c101              	cmpb	#1
1481  040a 2704              	beq	L134
1483  040c c103              	cmpb	#3
1484  040e 2607              	bne	L724
1485  0410                   L134:
1486                         ; 910 				vs_Flag2_c[vs_po].b.swtch_rq_prps_b = TRUE;
1488  0410 0cea004e01        	bset	_vs_Flag2_c,y,1
1490  0415 2009              	bra	L324
1491  0417                   L724:
1492                         ; 914 				vs_Flag2_c[vs_po].b.swtch_rq_prps_b = FALSE;
1494  0417 e680              	ldab	OFST-1,s
1495  0419                   LC004:
1496  0419 b746              	tfr	d,y
1497  041b 0dea004e01        	bclr	_vs_Flag2_c,y,1
1498  0420                   L324:
1499                         ; 921 }
1502  0420 1b83              	leas	3,s
1503  0422 0a                	rtc	
1545                         ; 938 @far void vsLF_RemSt_Check(u_8Bit vs_start_side)
1545                         ; 939 {
1546                         	switch	.ftext
1547  0423                   f_vsLF_RemSt_Check:
1549  0423 3b                	pshd	
1550  0424 37                	pshb	
1551       00000001          OFST:	set	1
1554                         ; 941 	u_8Bit vs_loc = vs_start_side;        // Vent side
1556  0425 6b80              	stab	OFST-1,s
1557                         ; 945 	if ((main_state_c == REM_STRT_VENTING) || (main_state_c == AUTO_STRT_VENTING)) {
1559  0427 f60000            	ldab	_main_state_c
1560  042a c102              	cmpb	#2
1561  042c 2704              	beq	L754
1563  042e c105              	cmpb	#5
1564  0430 2661              	bne	L774
1565  0432                   L754:
1566                         ; 948 		if (vs_Flag1_c[vs_loc].b.RemSt_auto_b == TRUE) {
1568  0432 e680              	ldab	OFST-1,s
1569  0434 87                	clra	
1570  0435 b746              	tfr	d,y
1571  0437 0eea00501056      	brset	_vs_Flag1_c,y,16,L774
1573                         ; 953 			if (main_state_c == REM_STRT_VENTING)
1575  043d f60000            	ldab	_main_state_c
1576  0440 c102              	cmpb	#2
1577  0442 260b              	bne	L564
1578                         ; 956 				vs_prm_set[vs_loc].crt_lv_c = VL_HI;
1580  0444 e680              	ldab	OFST-1,s
1581  0446 860f              	ldaa	#15
1582  0448 12                	mul	
1583  0449 b746              	tfr	d,y
1584  044b c603              	ldab	#3
1586  044d 2011              	bra	LC006
1587  044f                   L564:
1588                         ; 962 				if (vs_Flag1_c[vs_loc].b.sw_rq_b == FALSE)
1590  044f e680              	ldab	OFST-1,s
1591  0451 b746              	tfr	d,y
1592  0453 0eea0050020d      	brset	_vs_Flag1_c,y,2,L764
1593                         ; 965 					vs_prm_set[vs_loc].crt_lv_c = VL_LOW;
1595  0459 860f              	ldaa	#15
1596  045b 12                	mul	
1597  045c b746              	tfr	d,y
1598  045e c601              	ldab	#1
1599  0460                   LC006:
1600  0460 6bea0003          	stab	_vs_prm_set,y
1601  0464 e680              	ldab	OFST-1,s
1602  0466                   L764:
1603                         ; 971 			vs_Flag2_c[vs_loc].b.swtch_rq_prps_b = TRUE;
1605  0466 87                	clra	
1606  0467 b746              	tfr	d,y
1607  0469 0cea004e01        	bset	_vs_Flag2_c,y,1
1608                         ; 974 			vs_Flag1_c[vs_loc].b.RemSt_auto_b = TRUE;
1610  046e 0cea005010        	bset	_vs_Flag1_c,y,16
1611                         ; 977 			if (vs_prm_set[vs_loc].stat_c == GOOD) {
1613  0473 860f              	ldaa	#15
1614  0475 12                	mul	
1615  0476 b746              	tfr	d,y
1616  0478 e6ea0004          	ldab	_vs_prm_set+1,y
1617  047c 04210b            	dbne	b,L374
1618                         ; 979 				vs_Flag1_c[vs_loc].b.sw_rq_b = TRUE;
1620  047f e680              	ldab	OFST-1,s
1621  0481 b796              	exg	b,y
1622  0483 0cea005002        	bset	_vs_Flag1_c,y,2
1624  0488 2009              	bra	L774
1625  048a                   L374:
1626                         ; 983 				vs_Flag1_c[vs_loc].b.sw_rq_b = FALSE;
1628  048a e680              	ldab	OFST-1,s
1629  048c b796              	exg	b,y
1630  048e 0dea005002        	bclr	_vs_Flag1_c,y,2
1631  0493                   L774:
1632                         ; 990 }
1635  0493 1b83              	leas	3,s
1636  0495 0a                	rtc	
1691                         ; 1016 @far void vsF_swtch_Mngmnt(u_8Bit vs_point)
1691                         ; 1017 {
1692                         	switch	.ftext
1693  0496                   f_vsF_swtch_Mngmnt:
1695  0496 3b                	pshd	
1696  0497 3b                	pshd	
1697       00000002          OFST:	set	2
1700                         ; 1019 	u_8Bit vs_l = vs_point;     // store vent location
1702  0498 6b80              	stab	OFST-2,s
1703                         ; 1020 	u_8Bit vs_rq = 0;           // current vs request
1705                         ; 1023 	if ( (diag_io_hs_lock_flags_c[vs_l].b.switch_rq_lock_b == FALSE) && (diag_io_hs_lock_flags_c[vs_l].b.led_status_lock_b == FALSE) && (diag_io_all_hs_lock_b == FALSE) && (diag_rc_all_op_lock_b == FALSE) )
1707  049a b796              	exg	b,y
1708  049c e6ea0000          	ldab	_diag_io_hs_lock_flags_c,y
1709  04a0 c501              	bitb	#1
1710  04a2 18260112          	bne	L125
1712  04a6 c502              	bitb	#2
1713  04a8 1826010c          	bne	L125
1715  04ac f60000            	ldab	_diag_io_lock2_flags_c
1716  04af c510              	bitb	#16
1717  04b1 18260103          	bne	L125
1719  04b5 f60000            	ldab	_diag_rc_lock_flags_c
1720  04b8 c501              	bitb	#1
1721  04ba 182600fa          	bne	L125
1722                         ; 1026 		if ( (diag_io_vs_lock_flags_c[vs_l].b.switch_rq_lock_b == FALSE) && (diag_io_vs_lock_flags_c[vs_l].b.led_status_lock_b == FALSE) && (diag_io_all_vs_lock_b == FALSE) && 
1722                         ; 1027 			 (diag_rc_all_op_lock_b == FALSE)	)
1724  04be e6ea0000          	ldab	_diag_io_vs_lock_flags_c,y
1725  04c2 c501              	bitb	#1
1726  04c4 182600f5          	bne	LC013
1728  04c8 c502              	bitb	#2
1729  04ca 182600ef          	bne	LC013
1731  04ce f60000            	ldab	_diag_io_lock2_flags_c
1732  04d1 c501              	bitb	#1
1733  04d3 182600e6          	bne	LC013
1735  04d7 f60000            	ldab	_diag_rc_lock_flags_c
1736  04da c501              	bitb	#1
1737  04dc 182600dd          	bne	LC013
1738                         ; 1031 			if (vs_l == VS_FL)
1740  04e0 e680              	ldab	OFST-2,s
1741  04e2 2610              	bne	L525
1742                         ; 1033 				if (main_SwReq_Front_c == VEH_BUS_CCN_FRONT_REAR_K)
1744  04e4 f60000            	ldab	_main_SwReq_Front_c
1745  04e7 042105            	dbne	b,L725
1746                         ; 1036 					vs_rq = canio_L_F_VS_RQ_c;
1748  04ea f60000            	ldab	_canio_vent_seat_request_c
1750  04ed 2013              	bra	L335
1751  04ef                   L725:
1752                         ; 1041 					vs_rq = canio_LF_VS_RQ_c;
1754  04ef f60000            	ldab	_canio_hvac_vent_seat_request_c
1755  04f2 200e              	bra	L335
1756  04f4                   L525:
1757                         ; 1046 				if (main_SwReq_Front_c == VEH_BUS_CCN_FRONT_REAR_K)
1759  04f4 f60000            	ldab	_main_SwReq_Front_c
1760  04f7 042105            	dbne	b,L535
1761                         ; 1049 					vs_rq = canio_R_F_VS_RQ_c;
1763  04fa f60001            	ldab	_canio_vent_seat_request_c+1
1765  04fd 2003              	bra	L335
1766  04ff                   L535:
1767                         ; 1054 					vs_rq = canio_RF_VS_RQ_c;	
1769  04ff f60001            	ldab	_canio_hvac_vent_seat_request_c+1
1770  0502                   L335:
1771  0502 6b81              	stab	OFST-1,s
1772                         ; 1063 			if (main_SwReq_Front_c == VEH_BUS_CCN_FRONT_REAR_K)
1774  0504 f60000            	ldab	_main_SwReq_Front_c
1775  0507 53                	decb	
1776  0508 264b              	bne	L145
1777                         ; 1065 				if ( (vs_rq == VS_PSD) || (vs_rq == VS_NOT_PSD) )
1779  050a e681              	ldab	OFST-1,s
1780  050c c104              	cmpb	#4
1781  050e 2707              	beq	LC007
1783  0510 046138            	tbne	b,L345
1784                         ; 1068 					if ( (vs_rq == VS_PSD) && (vs_prm_set[vs_l].lst_swtch_rq_c == VS_NOT_PSD) )
1786  0513 18260092          	bne	L375
1787  0517                   LC007:
1789  0517 e680              	ldab	OFST-2,s
1790  0519 860f              	ldaa	#15
1791  051b 12                	mul	
1792  051c b746              	tfr	d,y
1793  051e e6ea000f          	ldab	_vs_prm_set+12,y
1794  0522 18260083          	bne	L375
1795                         ; 1071 						if (vs_Flag2_c[vs_l].b.swtch_rq_prps_b == FALSE)
1797  0526 e680              	ldab	OFST-2,s
1798  0528 87                	clra	
1799  0529 b746              	tfr	d,y
1800  052b 0eea004e0102      	brset	_vs_Flag2_c,y,1,L155
1801                         ; 1074 							vs_Flag2_c[vs_l].b.swtch_rq_prps_b = TRUE;
1804  0531 2011              	bra	LC009
1805  0533                   L155:
1806                         ; 1079 							if (vs_prm_set[vs_l].crt_lv_c != VL_LOW)
1808  0533 860f              	ldaa	#15
1809  0535 12                	mul	
1810  0536 b746              	tfr	d,y
1811  0538 e6ea0003          	ldab	_vs_prm_set,y
1812  053c 53                	decb	
1813  053d 2747              	beq	L706
1814                         ; 1082 								vs_Flag2_c[vs_l].b.swtch_rq_prps_b = TRUE;
1816  053f                   LC016:
1817  053f e680              	ldab	OFST-2,s
1818  0541 87                	clra	
1819  0542 b746              	tfr	d,y
1820  0544                   LC009:
1821  0544 0cea004e01        	bset	_vs_Flag2_c,y,1
1823  0549 2045              	bra	L506
1824                         ; 1087 								vs_Flag2_c[vs_l].b.swtch_rq_prps_b = FALSE;
1826                         ; 1091 						vs_prm_set[vs_l].LED_dsply_cnt = 0;
1829  054b                   L345:
1830                         ; 1101 					if ( (vs_rq == VS_OFF) || (vs_rq == VS_SNA) )
1832  054b c101              	cmpb	#1
1833  054d 2751              	beq	LC011
1835  054f c107              	cmpb	#7
1836  0551 2656              	bne	L375
1837                         ; 1104 						vs_Flag2_c[vs_l].b.swtch_rq_prps_b = FALSE;
1840  0553 204b              	bra	LC011
1841  0555                   L145:
1842                         ; 1121 				if ( (vs_rq == VS_HVAC_PSD) || (vs_rq == VS_HVAC_NOT_PSD) )
1844  0555 e681              	ldab	OFST-1,s
1845  0557 c101              	cmpb	#1
1846  0559 2705              	beq	LC008
1848  055b 04613e            	tbne	b,L575
1849                         ; 1124 					if ( (vs_rq == VS_HVAC_PSD) && (vs_prm_set[vs_l].lst_swtch_rq_c == VS_HVAC_NOT_PSD) )
1851  055e 2649              	bne	L375
1852  0560                   LC008:
1854  0560 e680              	ldab	OFST-2,s
1855  0562 860f              	ldaa	#15
1856  0564 12                	mul	
1857  0565 b746              	tfr	d,y
1858  0567 e6ea000f          	ldab	_vs_prm_set+12,y
1859  056b 263c              	bne	L375
1860                         ; 1127 						if (vs_Flag2_c[vs_l].b.swtch_rq_prps_b == FALSE)
1862  056d e680              	ldab	OFST-2,s
1863  056f 87                	clra	
1864  0570 b746              	tfr	d,y
1865  0572 0eea004e0102      	brset	_vs_Flag2_c,y,1,L306
1866                         ; 1130 							vs_Flag2_c[vs_l].b.swtch_rq_prps_b = TRUE;
1869  0578 20ca              	bra	LC009
1870  057a                   L306:
1871                         ; 1135 							if (vs_prm_set[vs_l].crt_lv_c != VL_LOW)
1873  057a 860f              	ldaa	#15
1874  057c 12                	mul	
1875  057d b746              	tfr	d,y
1876  057f e6ea0003          	ldab	_vs_prm_set,y
1877  0583 53                	decb	
1878                         ; 1138 								vs_Flag2_c[vs_l].b.swtch_rq_prps_b = TRUE;
1881  0584 26b9              	bne	LC016
1882  0586                   L706:
1883                         ; 1143 								vs_Flag2_c[vs_l].b.swtch_rq_prps_b = FALSE;
1885  0586 e680              	ldab	OFST-2,s
1886  0588 87                	clra	
1887  0589 b746              	tfr	d,y
1888  058b 0dea004e01        	bclr	_vs_Flag2_c,y,1
1889  0590                   L506:
1890                         ; 1147 						vs_prm_set[vs_l].LED_dsply_cnt = 0;
1892  0590 860f              	ldaa	#15
1893  0592 12                	mul	
1894  0593 b746              	tfr	d,y
1895  0595 1869ea000b        	clrw	_vs_prm_set+8,y
1897  059a 200d              	bra	L375
1898  059c                   L575:
1899                         ; 1157 					if (vs_rq == VS_HVAC_SNA)
1901  059c c103              	cmpb	#3
1902  059e 2609              	bne	L375
1903                         ; 1160 						vs_Flag2_c[vs_l].b.swtch_rq_prps_b = FALSE;
1905  05a0                   LC011:
1906  05a0 e680              	ldab	OFST-2,s
1907  05a2 b796              	exg	b,y
1908  05a4 0dea004e01        	bclr	_vs_Flag2_c,y,1
1910  05a9                   L375:
1911                         ; 1173 			vs_prm_set[vs_l].lst_swtch_rq_c = vs_rq;
1913  05a9 e680              	ldab	OFST-2,s
1914  05ab 860f              	ldaa	#15
1915  05ad 12                	mul	
1916  05ae b746              	tfr	d,y
1917  05b0 180a81ea000f      	movb	OFST-1,s,_vs_prm_set+12,y
1919  05b6 200c              	bra	L526
1920                         ; 1178 			vsF_Diag_IO_Mngmnt(vs_l);
1923  05b8                   L125:
1924                         ; 1184 		if(diag_rc_all_op_lock_b == TRUE){
1926  05b8 1f00000107        	brclr	_diag_rc_lock_flags_c,1,L526
1927                         ; 1186 			vsF_Diag_IO_Mngmnt(vs_l);			
1929  05bd                   LC013:
1930  05bd e680              	ldab	OFST-2,s
1931  05bf 87                	clra	
1932  05c0 4a03e7e7          	call	f_vsF_Diag_IO_Mngmnt
1935  05c4                   L526:
1936                         ; 1194 } //End Function
1939  05c4 1b84              	leas	4,s
1940  05c6 0a                	rtc	
1981                         ; 1234 @far void vsF_Output_Cntrl(void)
1981                         ; 1235 {
1982                         	switch	.ftext
1983  05c7                   f_vsF_Output_Cntrl:
1985  05c7 37                	pshb	
1986       00000001          OFST:	set	1
1989                         ; 1239 	for (c=0; c <VS_LMT; c++) {
1991  05c8 c7                	clrb	
1992  05c9 6b80              	stab	OFST-1,s
1993  05cb                   L746:
1994                         ; 1241 		if ( (vs_Flag1_c[c].b.sw_rq_b == TRUE) && (relay_A_Ok_b == TRUE) ) {
1996  05cb 87                	clra	
1997  05cc b746              	tfr	d,y
1998  05ce e6ea0050          	ldab	_vs_Flag1_c,y
1999  05d2 c502              	bitb	#2
2000  05d4 2779              	beq	L556
2002  05d6 f60000            	ldab	_relay_status_c
2003  05d9 c540              	bitb	#64
2004  05db 2772              	beq	L556
2005                         ; 1244 			if (vs_Flag1_c[c].b.state_b == TRUE) {
2007  05dd 0eea00500110      	brset	_vs_Flag1_c,y,1,L166
2009                         ; 1250 				Dio_WriteChannel(VENT_EN, STD_HIGH);
2012  05e3 1410              	sei	
2017  05e5 fe0014            	ldx	_Dio_PortWrite_Ptr+20
2018  05e8 0c0004            	bset	0,x,4
2022  05eb 10ef              	cli	
2024                         ; 1253 				vsF_Motor_StrtUp(c);
2027  05ed e680              	ldab	OFST-1,s
2028  05ef 4a03a0a0          	call	f_vsF_Motor_StrtUp
2030  05f3                   L166:
2031                         ; 1257 			if (c == VS_FL) {
2033  05f3 e680              	ldab	OFST-1,s
2034  05f5 2620              	bne	L366
2035                         ; 1261 				if( (vs_prm_set[c].prev_pwm_w != vs_prm_set[c].pwm_w) ){
2037  05f7 860f              	ldaa	#15
2038  05f9 12                	mul	
2039  05fa b746              	tfr	d,y
2040  05fc e680              	ldab	OFST-1,s
2041  05fe 860f              	ldaa	#15
2042  0600 12                	mul	
2043  0601 b745              	tfr	d,x
2044  0603 ece20010          	ldd	_vs_prm_set+13,x
2045  0607 acea000d          	cpd	_vs_prm_set+10,y
2046  060b 272d              	beq	L176
2047                         ; 1263 					Pwm_SetDutyCycle(PWM_VS_FL, vs_prm_set[c].pwm_w);					
2049  060d ecea000d          	ldd	_vs_prm_set+10,y
2050  0611 3b                	pshd	
2051  0612 cc0001            	ldd	#1
2054  0615 201d              	bra	LC017
2055  0617                   L366:
2056                         ; 1271 				if( (vs_prm_set[c].prev_pwm_w != vs_prm_set[c].pwm_w) ){
2058  0617 860f              	ldaa	#15
2059  0619 12                	mul	
2060  061a b746              	tfr	d,y
2061  061c e680              	ldab	OFST-1,s
2062  061e 860f              	ldaa	#15
2063  0620 12                	mul	
2064  0621 b745              	tfr	d,x
2065  0623 ece20010          	ldd	_vs_prm_set+13,x
2066  0627 acea000d          	cpd	_vs_prm_set+10,y
2067  062b 270d              	beq	L176
2068                         ; 1273 					Pwm_SetDutyCycle(PWM_VS_FR, vs_prm_set[c].pwm_w);					
2070  062d ecea000d          	ldd	_vs_prm_set+10,y
2071  0631 3b                	pshd	
2072  0632 87                	clra	
2073  0633 c7                	clrb	
2075  0634                   LC017:
2076  0634 4a000000          	call	f_Pwm_SetDutyCycle
2077  0638 1b82              	leas	2,s
2079  063a                   L176:
2080                         ; 1279 			vs_prm_set[c].prev_pwm_w = vs_prm_set[c].pwm_w;
2082  063a e680              	ldab	OFST-1,s
2083  063c 860f              	ldaa	#15
2084  063e 12                	mul	
2085  063f b746              	tfr	d,y
2086  0641 1802ea000dea0010  	movw	_vs_prm_set+10,y,_vs_prm_set+13,y
2087                         ; 1282 			vs_prm_set[c].Vsense_dly_cnt = 0;
2089  0649 69ea0006          	clr	_vs_prm_set+3,y
2091  064d 2068              	bra	L776
2092  064f                   L556:
2093                         ; 1287 			if (c == VS_FL) {	
2095  064f e680              	ldab	OFST-1,s
2096  0651 261a              	bne	L107
2097                         ; 1289 				if( (vs_Flag1_c[c].b.state_b == TRUE) || (vs_prm_set[c].prev_pwm_w != 0) ){
2099  0653 87                	clra	
2100  0654 b746              	tfr	d,y
2101  0656 0eea0050010b      	brset	_vs_Flag1_c,y,1,L507
2103  065c 860f              	ldaa	#15
2104  065e 12                	mul	
2105  065f b746              	tfr	d,y
2106  0661 ecea0010          	ldd	_vs_prm_set+13,y
2107  0665 2723              	beq	L117
2108  0667                   L507:
2109                         ; 1291 					Pwm_SetDutyCycle(PWM_VS_FL, 0);					
2111  0667 87                	clra	
2112  0668 c7                	clrb	
2113  0669 3b                	pshd	
2114  066a 52                	incb	
2117  066b 2017              	bra	LC018
2118  066d                   L107:
2119                         ; 1298 				if( (vs_Flag1_c[c].b.state_b == TRUE) || (vs_prm_set[c].prev_pwm_w != 0) ){
2121  066d 87                	clra	
2122  066e b746              	tfr	d,y
2123  0670 0eea0050010b      	brset	_vs_Flag1_c,y,1,L517
2125  0676 860f              	ldaa	#15
2126  0678 12                	mul	
2127  0679 b746              	tfr	d,y
2128  067b ecea0010          	ldd	_vs_prm_set+13,y
2129  067f 2709              	beq	L117
2130  0681                   L517:
2131                         ; 1300 					Pwm_SetDutyCycle(PWM_VS_FR, 0);
2133  0681 87                	clra	
2134  0682 c7                	clrb	
2135  0683 3b                	pshd	
2137  0684                   LC018:
2138  0684 4a000000          	call	f_Pwm_SetDutyCycle
2139  0688 1b82              	leas	2,s
2141  068a                   L117:
2142                         ; 1307 			vs_prm_set[c].prev_pwm_w = 0;
2144  068a e680              	ldab	OFST-1,s
2145  068c 860f              	ldaa	#15
2146  068e 12                	mul	
2147  068f b746              	tfr	d,y
2148  0691 87                	clra	
2149  0692 c7                	clrb	
2150  0693 6cea0010          	std	_vs_prm_set+13,y
2151  0697 6cea000d          	std	_vs_prm_set+10,y
2152                         ; 1310 			vs_prm_set[c].pwm_w = 0;
2154                         ; 1313 			vs_Flag1_c[c].b.state_b = FALSE;
2156  069b e680              	ldab	OFST-1,s
2157  069d b746              	tfr	d,y
2158  069f 0dea005001        	bclr	_vs_Flag1_c,y,1
2159                         ; 1316 			vs_prm_set[c].strt_up_cnt = 0;
2161  06a4 860f              	ldaa	#15
2162  06a6 12                	mul	
2163  06a7 b746              	tfr	d,y
2164  06a9 1869ea0007        	clrw	_vs_prm_set+4,y
2165                         ; 1317 			vs_Flag1_c[c].b.start_up_cmplt_b = FALSE;
2167  06ae e680              	ldab	OFST-1,s
2168  06b0 b796              	exg	b,y
2169  06b2 0dea005008        	bclr	_vs_Flag1_c,y,8
2170  06b7                   L776:
2171                         ; 1239 	for (c=0; c <VS_LMT; c++) {
2173  06b7 6280              	inc	OFST-1,s
2176  06b9 e680              	ldab	OFST-1,s
2177  06bb c102              	cmpb	#2
2178  06bd 1825ff0a          	blo	L746
2179                         ; 1323 	if( (vs_Flag1_c[VS_FL].b.sw_rq_b == FALSE) && (vs_Flag1_c[VS_FR].b.sw_rq_b == FALSE) ){
2181  06c1 1e0050020f        	brset	_vs_Flag1_c,2,L127
2183  06c6 1e0051020a        	brset	_vs_Flag1_c+1,2,L127
2184                         ; 1325 		Dio_WriteChannel(VENT_EN, STD_LOW);
2187  06cb 1410              	sei	
2192  06cd fe0014            	ldx	_Dio_PortWrite_Ptr+20
2193  06d0 0d0004            	bclr	0,x,4
2197  06d3 10ef              	cli	
2200  06d5                   L127:
2201                         ; 1328 }
2204  06d5 1b81              	leas	1,s
2205  06d7 0a                	rtc	
2248                         ; 1345 @far void vsF_Fnsh_LED_Dsply(u_8Bit vs_point_c)
2248                         ; 1346 {
2249                         	switch	.ftext
2250  06d8                   f_vsF_Fnsh_LED_Dsply:
2252  06d8 3b                	pshd	
2253  06d9 37                	pshb	
2254       00000001          OFST:	set	1
2257                         ; 1348 	u_8Bit vs_pnt = vs_point_c;    // store vent position
2259  06da 6b80              	stab	OFST-1,s
2260                         ; 1351 	vsF_Clear(vs_pnt);
2262  06dc 87                	clra	
2263  06dd 4a000000          	call	f_vsF_Clear
2265                         ; 1354 	if ( (diag_io_vs_lock_flags_c[vs_pnt].b.switch_rq_lock_b == TRUE) || (diag_io_all_vs_lock_b == TRUE) ) {
2267  06e1 e680              	ldab	OFST-1,s
2268  06e3 87                	clra	
2269  06e4 b746              	tfr	d,y
2270  06e6 0eea00000105      	brset	_diag_io_vs_lock_flags_c,y,1,L347
2272  06ec 1f00000104        	brclr	_diag_io_lock2_flags_c,1,L147
2273  06f1                   L347:
2274                         ; 1356 		diag_io_vs_rq_c[vs_pnt] = 0;
2276  06f1 69ea0000          	clr	_diag_io_vs_rq_c,y
2277  06f5                   L147:
2278                         ; 1359 	if (diag_io_vs_lock_flags_c[vs_pnt].b.led_status_lock_b == TRUE) {
2280  06f5 b796              	exg	b,y
2281  06f7 0fea00000204      	brclr	_diag_io_vs_lock_flags_c,y,2,L547
2282                         ; 1361 		diag_io_vs_state_c[vs_pnt] = 0;
2284  06fd 69ea0000          	clr	_diag_io_vs_state_c,y
2285  0701                   L547:
2286                         ; 1363 }
2289  0701 1b83              	leas	3,s
2290  0703 0a                	rtc	
2347                         ; 1436 @far void vsF_LED_Display(u_8Bit vs_mark)
2347                         ; 1437 {
2348                         	switch	.ftext
2349  0704                   f_vsF_LED_Display:
2351  0704 3b                	pshd	
2352  0705 3b                	pshd	
2353       00000002          OFST:	set	2
2356                         ; 1439 	u_8Bit vs_m = vs_mark;    // marks the selected vent
2358  0706 6b80              	stab	OFST-2,s
2359                         ; 1440 	u_8Bit vs_crt_rq = 0;     // current request status
2361                         ; 1443 	if (vs_Flag1_c[vs_m].b.LED_off_b == TRUE) {
2363  0708 87                	clra	
2364  0709 b746              	tfr	d,y
2365  070b 0fea00500411      	brclr	_vs_Flag1_c,y,4,L767
2366                         ; 1445 		vs_Flag1_c[vs_m].b.LED_off_b = FALSE;
2368  0711 0dea005004        	bclr	_vs_Flag1_c,y,4
2369                         ; 1448 		vs_prm_set[vs_m].LED_dsply_cnt = vs_LED_dsply_tm;
2371  0716 860f              	ldaa	#15
2372  0718 12                	mul	
2373  0719 b746              	tfr	d,y
2374  071b 1801ea000b002d    	movw	_vs_LED_dsply_tm,_vs_prm_set+8,y
2375  0722                   L767:
2376                         ; 1453 	if ( (canio_RX_IGN_STATUS_c == IGN_RUN) || (canio_RX_IGN_STATUS_c == IGN_START) ) {
2378  0722 f60000            	ldab	_canio_RX_IGN_STATUS_c
2379  0725 c104              	cmpb	#4
2380  0727 2704              	beq	L377
2382  0729 c105              	cmpb	#5
2383  072b 2659              	bne	L177
2384  072d                   L377:
2385                         ; 1456 		if (vs_Flag2_c[vs_m].b.swtch_rq_prps_b == TRUE) {
2387  072d e680              	ldab	OFST-2,s
2388  072f 87                	clra	
2389  0730 b746              	tfr	d,y
2390  0732 0fea004e012b      	brclr	_vs_Flag2_c,y,1,L577
2391                         ; 1458 			if (vs_prm_set[vs_m].stat_c != UGLY) {
2393  0738 860f              	ldaa	#15
2394  073a 12                	mul	
2395  073b b746              	tfr	d,y
2396  073d e6ea0004          	ldab	_vs_prm_set+1,y
2397  0741 c103              	cmpb	#3
2398  0743 262a              	bne	L1201
2400                         ; 1463 				if (vs_prm_set[vs_m].LED_dsply_cnt < vs_LED_dsply_tm) {
2402  0745 ecea000b          	ldd	_vs_prm_set+8,y
2403  0749 bc002d            	cpd	_vs_LED_dsply_tm
2404  074c 2407              	bhs	L3001
2405                         ; 1465 					vs_prm_set[vs_m].LED_dsply_cnt++;
2407  074e 1862ea000b        	incw	_vs_prm_set+8,y
2409  0753 201a              	bra	L1201
2410  0755                   L3001:
2411                         ; 1469 					if(diag_rc_all_op_lock_b == FALSE){
2413  0755 1e00000115        	brset	_diag_rc_lock_flags_c,1,L1201
2414                         ; 1472 						vsF_Fnsh_LED_Dsply(vs_m);						
2416  075a e680              	ldab	OFST-2,s
2417  075c 87                	clra	
2418  075d 4a06d8d8          	call	f_vsF_Fnsh_LED_Dsply
2421  0761 200c              	bra	L1201
2422  0763                   L577:
2423                         ; 1481 			if (diag_io_vs_lock_flags_c[vs_m].b.led_status_lock_b == TRUE) {
2425  0763 b746              	tfr	d,y
2426  0765 0eea00000204      	brset	_diag_io_vs_lock_flags_c,y,2,L1201
2428                         ; 1486 				vsF_Clear(vs_m);
2430  076b                   LC019:
2431  076b 4a000000          	call	f_vsF_Clear
2433  076f                   L1201:
2434                         ; 1497 	if (diag_io_vs_lock_flags_c[vs_m].b.led_status_lock_b == FALSE) {
2436  076f e680              	ldab	OFST-2,s
2437  0771 87                	clra	
2438  0772 b746              	tfr	d,y
2439  0774 0eea00000224      	brset	_diag_io_vs_lock_flags_c,y,2,L3201
2440                         ; 1500 		if(diag_rc_all_op_lock_b == FALSE){
2442  077a 860f              	ldaa	#15
2443  077c 12                	mul	
2444  077d b746              	tfr	d,y
2445  077f 1e00000107        	brset	_diag_rc_lock_flags_c,1,L5201
2446                         ; 1502 			vs_crt_rq = vs_prm_set[vs_m].crt_lv_c;			
2449  0784 200e              	bra	LC021
2450  0786                   L177:
2451                         ; 1492 		vsF_Clear(vs_m);
2453  0786 e680              	ldab	OFST-2,s
2454  0788 87                	clra	
2456  0789 20e0              	bra	LC019
2457  078b                   L5201:
2458                         ; 1505 			if(vs_prm_set[vs_m].LED_dsply_cnt < vs_LED_dsply_tm){
2460  078b ecea000b          	ldd	_vs_prm_set+8,y
2461  078f bc002d            	cpd	_vs_LED_dsply_tm
2462  0792 2406              	bhs	L1301
2463                         ; 1507 				vs_crt_rq = vs_prm_set[vs_m].crt_lv_c;				
2465  0794                   LC021:
2466  0794 e6ea0003          	ldab	_vs_prm_set,y
2468  0798 200a              	bra	LC020
2469  079a                   L1301:
2470                         ; 1511 				vs_crt_rq = VL0;
2472  079a 6981              	clr	OFST-1,s
2473  079c 2008              	bra	L5301
2474  079e                   L3201:
2475                         ; 1517 		vs_crt_rq = diag_io_vs_state_c[vs_m];
2477  079e b746              	tfr	d,y
2478  07a0 e6ea0000          	ldab	_diag_io_vs_state_c,y
2479  07a4                   LC020:
2480  07a4 6b81              	stab	OFST-1,s
2481  07a6                   L5301:
2482                         ; 1521 	if (vs_m == VS_FL) {
2484  07a6 e680              	ldab	OFST-2,s
2485  07a8 260e              	bne	L7301
2486                         ; 1525 		if(vs_lst_fl_led_stat_c != vs_crt_rq){
2488  07aa f60022            	ldab	_vs_lst_fl_led_stat_c
2489  07ad e181              	cmpb	OFST-1,s
2490  07af 2713              	beq	L5401
2491                         ; 1532 			vs_lst_fl_led_stat_c = vs_crt_rq;			
2493  07b1 180d810022        	movb	OFST-1,s,_vs_lst_fl_led_stat_c
2495  07b6 200c              	bra	L5401
2496  07b8                   L7301:
2497                         ; 1541 		if(vs_lst_fr_led_stat_c != vs_crt_rq){
2499  07b8 f60021            	ldab	_vs_lst_fr_led_stat_c
2500  07bb e181              	cmpb	OFST-1,s
2501  07bd 2705              	beq	L5401
2502                         ; 1547 			vs_lst_fr_led_stat_c = vs_crt_rq;			
2504  07bf 180d810021        	movb	OFST-1,s,_vs_lst_fr_led_stat_c
2506  07c4                   L5401:
2507                         ; 1552 }
2510  07c4 1b84              	leas	4,s
2511  07c6 0a                	rtc	
2553                         ; 1575 @far void vsF_Chck_Swtch_Prps(u_8Bit vs_mark_c)
2553                         ; 1576 {
2554                         	switch	.ftext
2555  07c7                   f_vsF_Chck_Swtch_Prps:
2557  07c7 3b                	pshd	
2558  07c8 37                	pshb	
2559       00000001          OFST:	set	1
2562                         ; 1578 	u_8Bit vs_ms = vs_mark_c;         // marks the selected vent
2564  07c9 6b80              	stab	OFST-1,s
2565                         ; 1581 	if (vs_Flag2_c[vs_ms].b.swtch_rq_prps_b == TRUE) {
2567  07cb 87                	clra	
2568  07cc b746              	tfr	d,y
2569  07ce 0fea004e011a      	brclr	_vs_Flag2_c,y,1,L1701
2570                         ; 1584 		if (vs_prm_set[vs_ms].stat_c == GOOD) {
2572  07d4 860f              	ldaa	#15
2573  07d6 12                	mul	
2574  07d7 b746              	tfr	d,y
2575  07d9 e6ea0004          	ldab	_vs_prm_set+1,y
2576  07dd 04210b            	dbne	b,L3701
2577                         ; 1587 			vs_Flag1_c[vs_ms].b.sw_rq_b = TRUE;
2579  07e0 e680              	ldab	OFST-1,s
2580  07e2 b796              	exg	b,y
2581  07e4 0cea005002        	bset	_vs_Flag1_c,y,2
2583  07e9 200a              	bra	L7701
2584  07eb                   L3701:
2585                         ; 1591 			vs_Flag1_c[vs_ms].b.sw_rq_b = FALSE;
2587  07eb e680              	ldab	OFST-1,s
2588  07ed 87                	clra	
2589  07ee                   L1701:
2590                         ; 1596 		vs_Flag1_c[vs_ms].b.sw_rq_b = FALSE;
2592  07ee b746              	tfr	d,y
2593  07f0 0dea005002        	bclr	_vs_Flag1_c,y,2
2594  07f5                   L7701:
2595                         ; 1598 }
2598  07f5 1b83              	leas	3,s
2599  07f7 0a                	rtc	
2643                         ; 1623 @far void vsF_swtch_Cntrl(u_8Bit vs_switch_pst)
2643                         ; 1624 {
2644                         	switch	.ftext
2645  07f8                   f_vsF_swtch_Cntrl:
2647  07f8 3b                	pshd	
2648  07f9 37                	pshb	
2649       00000001          OFST:	set	1
2652                         ; 1626 	u_8Bit vs_s = vs_switch_pst;        // marks the selected vent
2654  07fa 6b80              	stab	OFST-1,s
2655                         ; 1629 	if (main_SwReq_Front_c == VEH_BUS_CCN_FRONT_REAR_K)
2657  07fc f60000            	ldab	_main_SwReq_Front_c
2658  07ff 042122            	dbne	b,L7111
2659                         ; 1632 		if ( (canio_vent_seat_request_c[vs_s] == VS_PSD) && (vs_Flag2_c[vs_s].b.invld_swtch_rq_b == FALSE) )
2661  0802 e680              	ldab	OFST-1,s
2662  0804 87                	clra	
2663  0805 b746              	tfr	d,y
2664  0807 e6ea0000          	ldab	_canio_vent_seat_request_c,y
2665  080b c104              	cmpb	#4
2666  080d 2608              	bne	L1211
2668  080f 0eea004e0202      	brset	_vs_Flag2_c,y,2,L1211
2669                         ; 1635 			vs_Flag2_c[vs_s].b.invld_swtch_rq_b = TRUE;
2671                         ; 1638 			vsF_Updt_Crrt_Lvl(vs_s);
2674                         ; 1641 			vsF_Chck_Swtch_Prps(vs_s);
2678  0815 201f              	bra	LC024
2679  0817                   L1211:
2680                         ; 1646 			if (canio_vent_seat_request_c[vs_s] == VS_NOT_PSD)
2682  0817 e680              	ldab	OFST-1,s
2683  0819 87                	clra	
2684  081a b746              	tfr	d,y
2685  081c e7ea0000          	tst	_canio_vent_seat_request_c,y
2686  0820 263a              	bne	L7311
2687                         ; 1649 				vs_Flag2_c[vs_s].b.invld_swtch_rq_b = FALSE;
2690  0822 2031              	bra	LC025
2691                         ; 1654 				vs_Flag2_c[vs_s].b.invld_swtch_rq_b = TRUE;
2693  0824                   L7111:
2694                         ; 1661 		if ( (canio_hvac_vent_seat_request_c[vs_s] == VS_HVAC_PSD) && (vs_Flag2_c[vs_s].b.invld_swtch_rq_b == FALSE) )
2696  0824 e680              	ldab	OFST-1,s
2697  0826 87                	clra	
2698  0827 b746              	tfr	d,y
2699  0829 e6ea0000          	ldab	_canio_hvac_vent_seat_request_c,y
2700  082d 04211a            	dbne	b,L3311
2702  0830 0eea004e0214      	brset	_vs_Flag2_c,y,2,L3311
2703                         ; 1664 			vs_Flag2_c[vs_s].b.invld_swtch_rq_b = TRUE;
2705                         ; 1667 			vsF_Updt_Crrt_Lvl(vs_s);
2708                         ; 1670 			vsF_Chck_Swtch_Prps(vs_s);
2710  0836                   LC024:
2711  0836 0cea004e02        	bset	_vs_Flag2_c,y,2
2712  083b e680              	ldab	OFST-1,s
2713  083d 4a035f5f          	call	f_vsF_Updt_Crrt_Lvl
2714  0841 e680              	ldab	OFST-1,s
2715  0843 87                	clra	
2716  0844 4a07c7c7          	call	f_vsF_Chck_Swtch_Prps
2719  0848 2019              	bra	L1311
2720  084a                   L3311:
2721                         ; 1676 			if (canio_hvac_vent_seat_request_c[vs_s] == VS_NOT_PSD)
2723  084a e680              	ldab	OFST-1,s
2724  084c 87                	clra	
2725  084d b746              	tfr	d,y
2726  084f e7ea0000          	tst	_canio_hvac_vent_seat_request_c,y
2727  0853 2607              	bne	L7311
2728                         ; 1679 				vs_Flag2_c[vs_s].b.invld_swtch_rq_b = FALSE;
2730  0855                   LC025:
2731  0855 0dea004e02        	bclr	_vs_Flag2_c,y,2
2733  085a 2007              	bra	L1311
2734  085c                   L7311:
2735                         ; 1684 				vs_Flag2_c[vs_s].b.invld_swtch_rq_b = TRUE;
2737  085c b746              	tfr	d,y
2738  085e 0cea004e02        	bset	_vs_Flag2_c,y,2
2739  0863                   L1311:
2740                         ; 1688 }
2743  0863 1b83              	leas	3,s
2744  0865 0a                	rtc	
2784                         ; 1712 @far void vsF_Rqst_Drg_Hs_Cycl(u_8Bit vs_lokation)
2784                         ; 1713 {
2785                         	switch	.ftext
2786  0866                   f_vsF_Rqst_Drg_Hs_Cycl:
2788  0866 3b                	pshd	
2789  0867 37                	pshb	
2790       00000001          OFST:	set	1
2793                         ; 1715 	u_8Bit vs_lk = vs_lokation;        // marks the selected vent
2795  0868 6b80              	stab	OFST-1,s
2796                         ; 1718 	hsLF_UpdateHeatParameters(vs_lk, HEAT_LEVEL_OFF);
2798  086a cc0007            	ldd	#7
2799  086d 3b                	pshd	
2800  086e e682              	ldab	OFST+1,s
2801  0870 4a000000          	call	f_hsLF_UpdateHeatParameters
2803                         ; 1719 }
2806  0874 1b85              	leas	5,s
2807  0876 0a                	rtc	
2849                         ; 1740 @far void vsF_Rtrn_Cntrl_toECU(u_8Bit vs_lktn)
2849                         ; 1741 {
2850                         	switch	.ftext
2851  0877                   f_vsF_Rtrn_Cntrl_toECU:
2853  0877 3b                	pshd	
2854  0878 37                	pshb	
2855       00000001          OFST:	set	1
2858                         ; 1743 	u_8Bit vs_lkt = vs_lktn;     // selected vent location
2860  0879 6b80              	stab	OFST-1,s
2861                         ; 1745 	vs_Flag2_c[vs_lkt].b.diag_cntrl_actv_b = FALSE;      // diagnostics is inactive
2863  087b 87                	clra	
2864  087c b746              	tfr	d,y
2865  087e 0dea004e04        	bclr	_vs_Flag2_c,y,4
2866                         ; 1747 	vs_prm_set[vs_lkt].flt_mtr_cnt         = 0;          // clear vs fault mature time
2868  0883 860f              	ldaa	#15
2869  0885 12                	mul	
2870  0886 b746              	tfr	d,y
2871  0888 1869ea0009        	clrw	_vs_prm_set+6,y
2872                         ; 1750 	vsF_Clear(vs_lkt);
2874  088d e680              	ldab	OFST-1,s
2875  088f 87                	clra	
2876  0890 4a000000          	call	f_vsF_Clear
2878                         ; 1751 }
2881  0894 1b83              	leas	3,s
2882  0896 0a                	rtc	
2928                         ; 1777 @far void vsF_Diag_IO_Cntrl(u_8Bit vs_diag_lct)
2928                         ; 1778 {
2929                         	switch	.ftext
2930  0897                   f_vsF_Diag_IO_Cntrl:
2932  0897 3b                	pshd	
2933  0898 37                	pshb	
2934       00000001          OFST:	set	1
2937                         ; 1780 	u_8Bit vs_k = vs_diag_lct;     // selected vent location
2939  0899 6b80              	stab	OFST-1,s
2940                         ; 1783 	vs_Flag2_c[vs_k].b.diag_cntrl_actv_b = TRUE;
2942  089b 87                	clra	
2943  089c b746              	tfr	d,y
2944  089e 0cea004e04        	bset	_vs_Flag2_c,y,4
2945                         ; 1786 	if (vs_Flag2_c[vs_k].b.diag_clr_ontime_b == TRUE) {
2947  08a3 0fea004e0814      	brclr	_vs_Flag2_c,y,8,L7121
2948                         ; 1788 		vs_prm_set[vs_k].LED_dsply_cnt = 0;
2950  08a9 860f              	ldaa	#15
2951  08ab 12                	mul	
2952  08ac b746              	tfr	d,y
2953  08ae 1869ea000b        	clrw	_vs_prm_set+8,y
2954                         ; 1791 		vs_Flag2_c[vs_k].b.diag_clr_ontime_b = FALSE;
2956  08b3 e680              	ldab	OFST-1,s
2957  08b5 87                	clra	
2958  08b6 b746              	tfr	d,y
2959  08b8 0dea004e08        	bclr	_vs_Flag2_c,y,8
2961  08bd                   L7121:
2962                         ; 1798 	if(diag_rc_all_op_lock_b == TRUE){
2964  08bd 1f0000011b        	brclr	_diag_rc_lock_flags_c,1,L1221
2965                         ; 1801 		if(diag_rc_all_vents_lock_b == TRUE){	
2967  08c2 1f0000040f        	brclr	_diag_rc_lock_flags_c,4,L3221
2968                         ; 1804 			vs_prm_set[vs_k].crt_lv_c = VL_HI;
2970  08c7 860f              	ldaa	#15
2971  08c9 12                	mul	
2972  08ca b746              	tfr	d,y
2973  08cc c603              	ldab	#3
2974  08ce 6bea0003          	stab	_vs_prm_set,y
2975                         ; 1807 			vsF_Chck_Swtch_Prps(vs_k);
2977  08d2                   LC027:
2978  08d2 e680              	ldab	OFST-1,s
2981  08d4 2030              	bra	L7321
2982  08d6                   L3221:
2983                         ; 1811 			vsF_Clear(vs_k);	
2985  08d6 87                	clra	
2986  08d7 4a000000          	call	f_vsF_Clear
2988  08db 2052              	bra	L7221
2989  08dd                   L1221:
2990                         ; 1815 		if (diag_io_vs_lock_flags_c[vs_k].b.led_status_lock_b == FALSE) {
2992  08dd 87                	clra	
2993  08de b746              	tfr	d,y
2994  08e0 0eea00000234      	brset	_diag_io_vs_lock_flags_c,y,2,L1321
2995                         ; 1818 			if ( (diag_io_vs_rq_c[vs_k] == VL_LOW) || (diag_io_vs_rq_c[vs_k] == VL_HI) ) {
2997  08e6 e6ea0000          	ldab	_diag_io_vs_rq_c,y
2998  08ea c101              	cmpb	#1
2999  08ec 2704              	beq	L5321
3001  08ee c103              	cmpb	#3
3002  08f0 261b              	bne	L3321
3003  08f2                   L5321:
3004                         ; 1820 				vs_prm_set[vs_k].crt_lv_c = diag_io_vs_rq_c[vs_k];
3006  08f2 e680              	ldab	OFST-1,s
3007  08f4 860f              	ldaa	#15
3008  08f6 12                	mul	
3009  08f7 b746              	tfr	d,y
3010  08f9 e680              	ldab	OFST-1,s
3011  08fb 87                	clra	
3012  08fc b745              	tfr	d,x
3013  08fe 180ae20000ea0003  	movb	_diag_io_vs_rq_c,x,_vs_prm_set,y
3015  0906                   L7321:
3016                         ; 1828 			vsF_Chck_Swtch_Prps(vs_k);
3018  0906 87                	clra	
3019  0907 4a07c7c7          	call	f_vsF_Chck_Swtch_Prps
3022  090b 2022              	bra	L7221
3023  090d                   L3321:
3024                         ; 1824 				vs_prm_set[vs_k].crt_lv_c = VL0;
3026  090d e680              	ldab	OFST-1,s
3027  090f 860f              	ldaa	#15
3028  0911 12                	mul	
3029  0912 b746              	tfr	d,y
3030  0914 69ea0003          	clr	_vs_prm_set,y
3031  0918 20b8              	bra	LC027
3032  091a                   L1321:
3033                         ; 1832 			vs_Flag1_c[vs_k].b.sw_rq_b = FALSE;
3035  091a b746              	tfr	d,y
3036  091c 0dea005002        	bclr	_vs_Flag1_c,y,2
3037                         ; 1835 			vs_Flag2_c[vs_k].b.swtch_rq_prps_b = FALSE;
3039  0921 0dea004e01        	bclr	_vs_Flag2_c,y,1
3040                         ; 1838 			vs_prm_set[vs_k].crt_lv_c = VL0;
3042  0926 860f              	ldaa	#15
3043  0928 12                	mul	
3044  0929 b746              	tfr	d,y
3045  092b 69ea0003          	clr	_vs_prm_set,y
3046  092f                   L7221:
3047                         ; 1841 }
3050  092f 1b83              	leas	3,s
3051  0931 0a                	rtc	
3081                         ; 1871 @far void vsLF_Chck_mainState(void)
3081                         ; 1872 {
3082                         	switch	.ftext
3083  0932                   f_vsLF_Chck_mainState:
3087                         ; 1874 	if ( (vs_lst_main_state_c != NORMAL) && (main_state_c == NORMAL) ) {
3089  0932 f60023            	ldab	_vs_lst_main_state_c
3090  0935 2726              	beq	L1621
3092  0937 f60000            	ldab	_main_state_c
3093  093a 2621              	bne	L1621
3094                         ; 1877 		vs_Flag1_c[VS_FL].b.RemSt_auto_b = FALSE;
3096  093c 1d005010          	bclr	_vs_Flag1_c,16
3097                         ; 1878 		vs_Flag1_c[VS_FR].b.RemSt_auto_b = FALSE;  
3099  0940 1d005110          	bclr	_vs_Flag1_c+1,16
3100                         ; 1888 		if((vs_lst_main_state_c == REM_STRT_VENTING) || (vs_lst_main_state_c == REM_STRT_USER_SWTCH))
3102  0944 f60023            	ldab	_vs_lst_main_state_c
3103  0947 c102              	cmpb	#2
3104  0949 2704              	beq	L7521
3106  094b c103              	cmpb	#3
3107  094d 260e              	bne	L1621
3108  094f                   L7521:
3109                         ; 1893 			vs_prm_set[vs_slctd_drive_c].crt_lv_c = VL_LOW;
3111  094f f60025            	ldab	_vs_slctd_drive_c
3112  0952 860f              	ldaa	#15
3113  0954 12                	mul	
3114  0955 b746              	tfr	d,y
3115  0957 c601              	ldab	#1
3116  0959 6bea0003          	stab	_vs_prm_set,y
3117  095d                   L1621:
3118                         ; 1903 	if (main_state_c == NORMAL) {
3120  095d f60000            	ldab	_main_state_c
3121  0960 2607              	bne	L3621
3122                         ; 1905 		vs_slctd_drive_c = VS_LMT;
3124  0962 c602              	ldab	#2
3125  0964 7b0025            	stab	_vs_slctd_drive_c
3127  0967 2021              	bra	L5621
3128  0969                   L3621:
3129                         ; 1909 		if (canio_LHD_RHD_c == RHD) {
3131  0969 f60000            	ldab	_canio_LHD_RHD_c
3132  096c c102              	cmpb	#2
3133  096e 260e              	bne	L7621
3134                         ; 1911 			vs_slctd_drive_c = VS_FR;
3136  0970 c601              	ldab	#1
3137  0972 7b0025            	stab	_vs_slctd_drive_c
3138                         ; 1915 			vs_Flag1_c[VS_FL].b.RemSt_auto_b = TRUE;
3140  0975 1c005010          	bset	_vs_Flag1_c,16
3141                         ; 1918 			vs_other_drive_c = VS_FL;
3143  0979 790024            	clr	_vs_other_drive_c
3145  097c 200c              	bra	L5621
3146  097e                   L7621:
3147                         ; 1923 			vs_slctd_drive_c = VS_FL;
3149  097e 790025            	clr	_vs_slctd_drive_c
3150                         ; 1927 			vs_Flag1_c[VS_FR].b.RemSt_auto_b = TRUE;
3152  0981 1c005110          	bset	_vs_Flag1_c+1,16
3153                         ; 1930 			vs_other_drive_c = VS_FR;
3155  0985 c601              	ldab	#1
3156  0987 7b0024            	stab	_vs_other_drive_c
3157  098a                   L5621:
3158                         ; 1937 	if ( (main_state_c != NORMAL) && (vs_lst_main_state_c == NORMAL) ) {
3160  098a f60000            	ldab	_main_state_c
3161  098d 2749              	beq	L3131
3163  098f f60023            	ldab	_vs_lst_main_state_c
3164  0992 2644              	bne	L3131
3165                         ; 1941 		if ((main_state_c == REM_STRT_VENTING) || (main_state_c == REM_STRT_USER_SWTCH) /*|| (main_state_c == AUTO_STRT_VENTING) || (main_state_c == AUTO_STRT_USER_SWTCH)*/) {
3167  0994 f60000            	ldab	_main_state_c
3168  0997 c102              	cmpb	#2
3169  0999 2704              	beq	L7721
3171  099b c103              	cmpb	#3
3172  099d 260c              	bne	L5721
3173  099f                   L7721:
3174                         ; 1943 			vs_prm_set[vs_slctd_drive_c].crt_lv_c = VL_HI;
3176  099f f60025            	ldab	_vs_slctd_drive_c
3177  09a2 860f              	ldaa	#15
3178  09a4 12                	mul	
3179  09a5 b746              	tfr	d,y
3180  09a7 c603              	ldab	#3
3182  09a9 201b              	bra	LC028
3183  09ab                   L5721:
3184                         ; 1945 	   	else if((main_state_c == AUTO_STRT_VENTING) || (main_state_c == AUTO_STRT_USER_SWTCH)){
3186  09ab c105              	cmpb	#5
3187  09ad 2704              	beq	L5031
3189  09af c106              	cmpb	#6
3190  09b1 2619              	bne	L3031
3191  09b3                   L5031:
3192                         ; 1948 	   		if(vs_Flag1_c[vs_slctd_drive_c].b.sw_rq_b == FALSE)
3194  09b3 f60025            	ldab	_vs_slctd_drive_c
3195  09b6 87                	clra	
3196  09b7 b746              	tfr	d,y
3197  09b9 0eea00500219      	brset	_vs_Flag1_c,y,2,L3131
3198                         ; 1952 	   			vs_prm_set[vs_slctd_drive_c].crt_lv_c = VL_LOW;
3200  09bf 860f              	ldaa	#15
3201  09c1 12                	mul	
3202  09c2 b746              	tfr	d,y
3203  09c4 c601              	ldab	#1
3204  09c6                   LC028:
3205  09c6 6bea0003          	stab	_vs_prm_set,y
3206  09ca 200c              	bra	L3131
3207  09cc                   L3031:
3208                         ; 1957 		   	vs_prm_set[vs_slctd_drive_c].crt_lv_c = VL0;
3210  09cc f60025            	ldab	_vs_slctd_drive_c
3211  09cf 860f              	ldaa	#15
3212  09d1 12                	mul	
3213  09d2 b746              	tfr	d,y
3214  09d4 69ea0003          	clr	_vs_prm_set,y
3215  09d8                   L3131:
3216                         ; 1965 	vs_lst_main_state_c = main_state_c;
3218  09d8 180c00000023      	movb	_main_state_c,_vs_lst_main_state_c
3219                         ; 1966 }
3222  09de 0a                	rtc	
3284                         ; 2049 @far void vsF_Manager(void)
3284                         ; 2050 {
3285                         	switch	.ftext
3286  09df                   f_vsF_Manager:
3288  09df 3b                	pshd	
3289       00000002          OFST:	set	2
3292                         ; 2056 	vsLF_Chck_mainState();
3294  09e0 4a093232          	call	f_vsLF_Chck_mainState
3296                         ; 2058 	for (e=0; e<VS_LMT; e++) {
3298  09e4 6980              	clr	OFST-2,s
3299  09e6                   L3331:
3300                         ; 2064 		if ( (vs_slctd_drive_c == VS_LMT) || (e == vs_slctd_drive_c) || (e == vs_other_drive_c)) {
3302  09e6 f60025            	ldab	_vs_slctd_drive_c
3303  09e9 c102              	cmpb	#2
3304  09eb 270e              	beq	L3431
3306  09ed e680              	ldab	OFST-2,s
3307  09ef f10025            	cmpb	_vs_slctd_drive_c
3308  09f2 2707              	beq	L3431
3310  09f4 f10024            	cmpb	_vs_other_drive_c
3311  09f7 18260139          	bne	L1431
3312  09fb                   L3431:
3313                         ; 2067 			vsF_Updt_Status(e);
3315  09fb e680              	ldab	OFST-2,s
3316  09fd 87                	clra	
3317  09fe 4a02a9a9          	call	f_vsF_Updt_Status
3319                         ; 2070 			vsF_swtch_Mngmnt(e);
3321  0a02 e680              	ldab	OFST-2,s
3322  0a04 87                	clra	
3323  0a05 4a049696          	call	f_vsF_swtch_Mngmnt
3325                         ; 2073 			crt_hs_stat = (u_8Bit) hsF_FrontHeatSeatStatus(e);
3327  0a09 e680              	ldab	OFST-2,s
3328  0a0b 87                	clra	
3329  0a0c 4a000000          	call	f_hsF_FrontHeatSeatStatus
3331  0a10 6b81              	stab	OFST-1,s
3332                         ; 2076 			if (vs_Flag1_c[e].b.start_up_cmplt_b == TRUE) {
3334  0a12 e680              	ldab	OFST-2,s
3335  0a14 87                	clra	
3336  0a15 b746              	tfr	d,y
3337  0a17 0fea00500804      	brclr	_vs_Flag1_c,y,8,L1531
3338                         ; 2078 				vsF_Nrmlz_PWM(e);
3340  0a1d 4a031010          	call	f_vsF_Nrmlz_PWM
3343  0a21                   L1531:
3344                         ; 2086 			if (crt_hs_stat == HS_NOT_ACTIVE) {
3346  0a21 e681              	ldab	OFST-1,s
3347  0a23 c1aa              	cmpb	#170
3348  0a25 2636              	bne	L3531
3349                         ; 2088 				if ( (diag_io_vs_lock_flags_c[e].b.switch_rq_lock_b == FALSE) && (diag_io_vs_lock_flags_c[e].b.led_status_lock_b == FALSE) && (diag_io_all_vs_lock_b == FALSE) && (diag_rc_all_op_lock_b == FALSE) ) {
3351  0a27 e680              	ldab	OFST-2,s
3352  0a29 87                	clra	
3353  0a2a b746              	tfr	d,y
3354  0a2c 0eea00000129      	brset	_diag_io_vs_lock_flags_c,y,1,L5531
3356  0a32 0eea00000223      	brset	_diag_io_vs_lock_flags_c,y,2,L5531
3358  0a38 1e0000011e        	brset	_diag_io_lock2_flags_c,1,L5531
3360  0a3d 1e00000119        	brset	_diag_rc_lock_flags_c,1,L5531
3361                         ; 2090 					if (vs_Flag2_c[e].b.diag_cntrl_actv_b == FALSE) {
3363  0a42 0eea004e0406      	brset	_vs_Flag2_c,y,4,L7531
3364                         ; 2092 						vsF_swtch_Cntrl(e);
3366  0a48 4a07f8f8          	call	f_vsF_swtch_Cntrl
3369  0a4c 2004              	bra	L1631
3370  0a4e                   L7531:
3371                         ; 2096 						vsF_Rtrn_Cntrl_toECU(e);
3373  0a4e 4a087777          	call	f_vsF_Rtrn_Cntrl_toECU
3375  0a52                   L1631:
3376                         ; 2100 					vsLF_RemSt_Check(e);
3378  0a52 e680              	ldab	OFST-2,s
3379  0a54 87                	clra	
3380  0a55 4a042323          	call	f_vsLF_RemSt_Check
3383  0a59 202e              	bra	L5631
3384  0a5b                   L5531:
3385                         ; 2107 					vsF_Diag_IO_Cntrl(e);
3388  0a5b 2027              	bra	L1041
3389  0a5d                   L3531:
3390                         ; 2113 				if ( (diag_io_vs_lock_flags_c[e].b.switch_rq_lock_b == TRUE) || (diag_io_all_vs_lock_b == TRUE) || (diag_io_vs_lock_flags_c[e].b.led_status_lock_b == TRUE) || (diag_rc_all_op_lock_b == TRUE) ) {
3392  0a5d e680              	ldab	OFST-2,s
3393  0a5f 87                	clra	
3394  0a60 b746              	tfr	d,y
3395  0a62 0eea00000110      	brset	_diag_io_vs_lock_flags_c,y,1,L1731
3397  0a68 1e0000010b        	brset	_diag_io_lock2_flags_c,1,L1731
3399  0a6d 0eea00000205      	brset	_diag_io_vs_lock_flags_c,y,2,L1731
3401  0a73 1f00000126        	brclr	_diag_rc_lock_flags_c,1,L7631
3402  0a78                   L1731:
3403                         ; 2116 					if(diag_rc_all_op_lock_b == FALSE){
3405  0a78 1e00000107        	brset	_diag_rc_lock_flags_c,1,L1041
3406                         ; 2120 						vsF_Rqst_Drg_Hs_Cycl(e);
3408  0a7d 87                	clra	
3409  0a7e 4a086666          	call	f_vsF_Rqst_Drg_Hs_Cycl
3412  0a82 e680              	ldab	OFST-2,s
3413  0a84                   L1041:
3414                         ; 2128 					vsF_Diag_IO_Cntrl(e);
3416  0a84 87                	clra	
3417  0a85 4a089797          	call	f_vsF_Diag_IO_Cntrl
3420  0a89                   L5631:
3421                         ; 2168 			if (vs_prm_set[e].stat_c != GOOD) {
3423  0a89 e680              	ldab	OFST-2,s
3424  0a8b 860f              	ldaa	#15
3425  0a8d 12                	mul	
3426  0a8e b746              	tfr	d,y
3427  0a90 e6ea0004          	ldab	_vs_prm_set+1,y
3428  0a94 53                	decb	
3429  0a95 2773              	beq	L1241
3430                         ; 2170 				vs_Flag1_c[e].b.sw_rq_b = FALSE;
3432  0a97 e680              	ldab	OFST-2,s
3433  0a99 87                	clra	
3435  0a9a 1820007e          	bra	L5241
3436  0a9e                   L7631:
3437                         ; 2132 					if ((main_SwReq_Front_c == VEH_BUS_CCN_FRONT_REAR_K)) {
3439  0a9e f60000            	ldab	_main_SwReq_Front_c
3440  0aa1 04212e            	dbne	b,L5041
3441                         ; 2135 						if ( (canio_vent_seat_request_c[e] == VS_PSD) && (vs_Flag2_c[e].b.invld_swtch_rq_b == FALSE) && (diag_io_hs_lock_flags_c[e].b.switch_rq_lock_b == FALSE) && (diag_io_hs_lock_flags_c[e].b.led_status_lock_b == FALSE) && (diag_io_all_hs_lock_b == FALSE) ) {
3443  0aa4 e680              	ldab	OFST-2,s
3444  0aa6 b746              	tfr	d,y
3445  0aa8 e6ea0000          	ldab	_canio_vent_seat_request_c,y
3446  0aac c104              	cmpb	#4
3447  0aae 2619              	bne	L7041
3449  0ab0 0eea004e0213      	brset	_vs_Flag2_c,y,2,L7041
3451  0ab6 0eea0000010d      	brset	_diag_io_hs_lock_flags_c,y,1,L7041
3453  0abc 0eea00000207      	brset	_diag_io_hs_lock_flags_c,y,2,L7041
3455  0ac2 1e00001002        	brset	_diag_io_lock2_flags_c,16,L7041
3456                         ; 2138 							vsF_Rqst_Drg_Hs_Cycl(e);
3459                         ; 2141 							vsF_swtch_Cntrl(e);
3463  0ac7 2030              	bra	LC030
3464  0ac9                   L7041:
3465                         ; 2145 							vsF_Clear(e);
3467  0ac9 e680              	ldab	OFST-2,s
3468  0acb 87                	clra	
3469  0acc 4a000000          	call	f_vsF_Clear
3471  0ad0 20b7              	bra	L5631
3472  0ad2                   L5041:
3473                         ; 2151 						if ( (canio_hvac_vent_seat_request_c[e] == VS_HVAC_PSD) && (vs_Flag2_c[e].b.invld_swtch_rq_b == FALSE) && (diag_io_hs_lock_flags_c[e].b.switch_rq_lock_b == FALSE) && (diag_io_hs_lock_flags_c[e].b.led_status_lock_b == FALSE) && (diag_io_all_hs_lock_b == FALSE) && (diag_rc_all_op_lock_b == FALSE) ) {
3475  0ad2 e680              	ldab	OFST-2,s
3476  0ad4 b746              	tfr	d,y
3477  0ad6 e6ea0000          	ldab	_canio_hvac_vent_seat_request_c,y
3478  0ada 53                	decb	
3479  0adb 26ec              	bne	L7041
3481  0add 0eea004e0225      	brset	_vs_Flag2_c,y,2,L5141
3483  0ae3 0eea0000011f      	brset	_diag_io_hs_lock_flags_c,y,1,L5141
3485  0ae9 0eea00000219      	brset	_diag_io_hs_lock_flags_c,y,2,L5141
3487  0aef 1e00001014        	brset	_diag_io_lock2_flags_c,16,L5141
3489  0af4 1e0000010f        	brset	_diag_rc_lock_flags_c,1,L5141
3490                         ; 2154 							vsF_Rqst_Drg_Hs_Cycl(e);
3493                         ; 2157 							vsF_swtch_Cntrl(e);
3495  0af9                   LC030:
3496  0af9 e680              	ldab	OFST-2,s
3497  0afb 4a086666          	call	f_vsF_Rqst_Drg_Hs_Cycl
3498  0aff e680              	ldab	OFST-2,s
3499  0b01 87                	clra	
3500  0b02 4a07f8f8          	call	f_vsF_swtch_Cntrl
3503  0b06 2081              	bra	L5631
3504  0b08                   L5141:
3505                         ; 2161 							vsF_Clear(e);
3508  0b08 20bf              	bra	L7041
3509  0b0a                   L1241:
3510                         ; 2174 				if (vs_Flag2_c[e].b.swtch_rq_prps_b == TRUE) {
3512  0b0a e680              	ldab	OFST-2,s
3513  0b0c 87                	clra	
3514  0b0d b746              	tfr	d,y
3515  0b0f 0fea004e0107      	brclr	_vs_Flag2_c,y,1,L5241
3516                         ; 2177 					vs_Flag1_c[e].b.sw_rq_b = TRUE;
3518  0b15 0cea005002        	bset	_vs_Flag1_c,y,2
3520  0b1a 2007              	bra	L3241
3521  0b1c                   L5241:
3522                         ; 2181 					vs_Flag1_c[e].b.sw_rq_b = FALSE;
3524  0b1c b746              	tfr	d,y
3525  0b1e 0dea005002        	bclr	_vs_Flag1_c,y,2
3526  0b23                   L3241:
3527                         ; 2186 			vsF_LED_Display(e);
3531  0b23 87                	clra	
3532  0b24 4a070404          	call	f_vsF_LED_Display
3533                         ; 2058 	for (e=0; e<VS_LMT; e++) {
3535  0b28 6280              	inc	OFST-2,s
3538  0b2a e680              	ldab	OFST-2,s
3539  0b2c c102              	cmpb	#2
3540  0b2e 1825feb4          	blo	L3331
3541                         ; 2197 }
3544  0b32 31                	puly	
3545  0b33 0a                	rtc	
3546  0b34                   L1431:
3547                         ; 2191 			vsF_Clear(e);
3549  0b34 87                	clra	
3550  0b35 4a000000          	call	f_vsF_Clear
3552                         ; 2194 			vsF_LED_Display(e);
3554  0b39 e680              	ldab	OFST-2,s
3556  0b3b 20e6              	bra	L3241
3635                         ; 2227 @far void vsF_Updt_Flt_Stat(u_8Bit vs_flt_pstn, Vs_Flt_Type_e vs_fault)
3635                         ; 2228 {
3636                         	switch	.ftext
3637  0b3d                   f_vsF_Updt_Flt_Stat:
3639  0b3d 3b                	pshd	
3640  0b3e 1b9d              	leas	-3,s
3641       00000003          OFST:	set	3
3644                         ; 2230 	u_8Bit vs_flt_p = vs_flt_pstn;       // stores faultly vent position
3646  0b40 6b82              	stab	OFST-1,s
3647                         ; 2233 	if (vs_prm_set[vs_flt_p].flt_mtr_cnt < (vs_flt_mtr_tm-1)) {
3649  0b42 fd002b            	ldy	_vs_flt_mtr_tm
3650  0b45 03                	dey	
3651  0b46 6d80              	sty	OFST-3,s
3652  0b48 860f              	ldaa	#15
3653  0b4a 12                	mul	
3654  0b4b b746              	tfr	d,y
3655  0b4d ecea0009          	ldd	_vs_prm_set+6,y
3656  0b51 ac80              	cpd	OFST-3,s
3657  0b53 2446              	bhs	L7051
3658                         ; 2235 		vs_prm_set[vs_flt_p].flt_mtr_cnt++;
3660  0b55 e682              	ldab	OFST-1,s
3661  0b57 860f              	ldaa	#15
3662  0b59 12                	mul	
3663  0b5a b746              	tfr	d,y
3664  0b5c 1862ea0009        	incw	_vs_prm_set+6,y
3665                         ; 2238 		switch (vs_fault) {
3667  0b61 e689              	ldab	OFST+6,s
3669  0b63 270c              	beq	L3341
3670  0b65 040117            	dbeq	b,L5341
3671  0b68 040122            	dbeq	b,L7341
3672                         ; 2264 				vs_prm_set[vs_flt_p].flt_stat_c = VS_NO_FLT_MASK;
3674  0b6b 69ea0005          	clr	_vs_prm_set+2,y
3675  0b6f 2062              	bra	L5151
3676  0b71                   L3341:
3677                         ; 2243 				vs_prm_set[vs_flt_p].flt_stat_c |= VS_PRLM_OPEN_MASK;
3679  0b71 e682              	ldab	OFST-1,s
3680  0b73 860f              	ldaa	#15
3681  0b75 12                	mul	
3682  0b76 b746              	tfr	d,y
3683  0b78 0cea000501        	bset	_vs_prm_set+2,y,1
3684                         ; 2244 				break;
3686  0b7d 2054              	bra	L5151
3687  0b7f                   L5341:
3688                         ; 2250 				vs_prm_set[vs_flt_p].flt_stat_c |= VS_PRLM_GND_MASK;
3690  0b7f e682              	ldab	OFST-1,s
3691  0b81 860f              	ldaa	#15
3692  0b83 12                	mul	
3693  0b84 b746              	tfr	d,y
3694  0b86 0cea000502        	bset	_vs_prm_set+2,y,2
3695                         ; 2251 				break;
3697  0b8b 2046              	bra	L5151
3698  0b8d                   L7341:
3699                         ; 2257 				vs_prm_set[vs_flt_p].flt_stat_c |= VS_PRLM_BATT_MASK;
3701  0b8d e682              	ldab	OFST-1,s
3702  0b8f 860f              	ldaa	#15
3703  0b91 12                	mul	
3704  0b92 b746              	tfr	d,y
3705  0b94 0cea000504        	bset	_vs_prm_set+2,y,4
3706                         ; 2258 				break;
3708  0b99 2038              	bra	L5151
3709                         ; 2264 				vs_prm_set[vs_flt_p].flt_stat_c = VS_NO_FLT_MASK;
3710  0b9b                   L7051:
3711                         ; 2271 		switch (vs_fault) {
3713  0b9b e689              	ldab	OFST+6,s
3715  0b9d 2713              	beq	L3441
3716  0b9f 040134            	dbeq	b,L5441
3717  0ba2 040145            	dbeq	b,L7441
3718                         ; 2319 				vs_prm_set[vs_flt_p].flt_stat_c = VS_NO_FLT_MASK;
3720  0ba5 e682              	ldab	OFST-1,s
3721  0ba7 860f              	ldaa	#15
3722  0ba9 12                	mul	
3723  0baa b746              	tfr	d,y
3724  0bac 69ea0005          	clr	_vs_prm_set+2,y
3725                         ; 2322 				vs_prm_set[vs_flt_p].flt_mtr_cnt = 0;
3727  0bb0 2031              	bra	LC033
3728  0bb2                   L3441:
3729                         ; 2277 				if (vs_prm_set[vs_flt_p].flt_mtr_cnt >= (Vs_flt_cal_prms.vs_10sec_mature_time)) {
3731  0bb2 e682              	ldab	OFST-1,s
3732  0bb4 860f              	ldaa	#15
3733  0bb6 12                	mul	
3734  0bb7 b746              	tfr	d,y
3735  0bb9 f60036            	ldab	_Vs_flt_cal_prms+7
3736  0bbc 87                	clra	
3737  0bbd acea0009          	cpd	_vs_prm_set+6,y
3738  0bc1 2204              	bhi	L3251
3739                         ; 2281 					vs_prm_set[vs_flt_p].flt_stat_c = VS_MTRD_OPEN_MASK;
3741  0bc3 c608              	ldab	#8
3742                         ; 2284 					vs_prm_set[vs_flt_p].flt_mtr_cnt = 0;
3745  0bc5 2018              	bra	LC034
3746  0bc7                   L3251:
3747                         ; 2288 					vs_prm_set[vs_flt_p].flt_mtr_cnt++;
3749  0bc7 e682              	ldab	OFST-1,s
3750  0bc9 860f              	ldaa	#15
3751  0bcb 12                	mul	
3752  0bcc b746              	tfr	d,y
3753  0bce 1862ea0009        	incw	_vs_prm_set+6,y
3754  0bd3                   L5151:
3755                         ; 2329 }
3758  0bd3 1b85              	leas	5,s
3759  0bd5 0a                	rtc	
3760  0bd6                   L5441:
3761                         ; 2296 				vs_prm_set[vs_flt_p].flt_stat_c = VS_MTRD_GND_MASK;
3763  0bd6 e682              	ldab	OFST-1,s
3764  0bd8 860f              	ldaa	#15
3765  0bda 12                	mul	
3766  0bdb b746              	tfr	d,y
3767  0bdd c610              	ldab	#16
3768  0bdf                   LC034:
3769  0bdf 6bea0005          	stab	_vs_prm_set+2,y
3770                         ; 2299 				vs_prm_set[vs_flt_p].flt_mtr_cnt = 0;
3772  0be3                   LC033:
3773  0be3 1869ea0009        	clrw	_vs_prm_set+6,y
3774                         ; 2300 				break;
3776  0be8 20e9              	bra	L5151
3777  0bea                   L7441:
3778                         ; 2306 				vs_prm_set[vs_flt_p].flt_stat_c = VS_MTRD_BATT_MASK;
3780  0bea e682              	ldab	OFST-1,s
3781  0bec 860f              	ldaa	#15
3782  0bee 12                	mul	
3783  0bef b746              	tfr	d,y
3784  0bf1 c620              	ldab	#32
3785  0bf3 6bea0005          	stab	_vs_prm_set+2,y
3786                         ; 2309 				vs_shrtTo_batt_mntr_c = VS_SHRT_TO_BATT;
3788  0bf7 c602              	ldab	#2
3789  0bf9 7b0049            	stab	_vs_shrtTo_batt_mntr_c
3790                         ; 2312 				vs_prm_set[vs_flt_p].flt_mtr_cnt = 0;
3792                         ; 2313 				break;
3794  0bfc 20e5              	bra	LC033
3840                         ; 2392 @far void vsF_Fault_Mntr(void)
3840                         ; 2393 {
3841                         	switch	.ftext
3842  0bfe                   f_vsF_Fault_Mntr:
3844  0bfe 3b                	pshd	
3845       00000002          OFST:	set	2
3848                         ; 2397 	for (f=0; f < VS_LMT; f++) {
3850  0bff c7                	clrb	
3851  0c00 6b80              	stab	OFST-2,s
3852  0c02                   L5451:
3853                         ; 2400 		vs_prv_flt_typ_c = vs_prm_set[f].flt_stat_c;
3855  0c02 860f              	ldaa	#15
3856  0c04 12                	mul	
3857  0c05 b746              	tfr	d,y
3858  0c07 180aea000581      	movb	_vs_prm_set+2,y,OFST-1,s
3859                         ; 2404 		if ( (main_CSWM_Status_c == GOOD) && (relay_A_internal_Fault_b == FALSE) && (vs_prm_set[f].flt_stat_c != VS_MTRD_BATT_MASK) ) {
3861  0c0d f60000            	ldab	_main_CSWM_Status_c
3862  0c10 53                	decb	
3863  0c11 182600b9          	bne	L3551
3865  0c15 f60000            	ldab	_relay_control_c
3866  0c18 c510              	bitb	#16
3867  0c1a 182600b0          	bne	L3551
3869  0c1e e6ea0005          	ldab	_vs_prm_set+2,y
3870  0c22 c120              	cmpb	#32
3871  0c24 182700a6          	beq	L3551
3872                         ; 2407 			if (vs_Flag1_c[f].b.state_b == TRUE) {
3874  0c28 e680              	ldab	OFST-2,s
3875  0c2a 87                	clra	
3876  0c2b b746              	tfr	d,y
3877  0c2d 0fea0050014b      	brclr	_vs_Flag1_c,y,1,L5551
3878                         ; 2410 				if (vs_Vsense_c[f] >= Vs_flt_cal_prms.Flt_Dtc[V_SHRT_TO_GND]) {
3880  0c33 e6ea004c          	ldab	_vs_Vsense_c,y
3881  0c37 f10030            	cmpb	_Vs_flt_cal_prms+1
3882  0c3a 253d              	blo	L7551
3883                         ; 2412 					vs_prm_set[f].flt_stat_c &= VS_CLR_PRLM_GND;
3885  0c3c e680              	ldab	OFST-2,s
3886  0c3e 860f              	ldaa	#15
3887  0c40 12                	mul	
3888  0c41 b746              	tfr	d,y
3889  0c43 0dea000502        	bclr	_vs_prm_set+2,y,2
3890                         ; 2415 					if (vs_Vsense_c[f] >= Vs_flt_cal_prms.Flt_Dtc[V_EN_OPEN_DEC]) {
3892  0c48 e680              	ldab	OFST-2,s
3893  0c4a 87                	clra	
3894  0c4b b746              	tfr	d,y
3895  0c4d e6ea004c          	ldab	_vs_Vsense_c,y
3896  0c51 f10032            	cmpb	_Vs_flt_cal_prms+3
3897  0c54 2515              	blo	L1651
3898                         ; 2418 						if (vs_Isense_c[f] < Vs_flt_cal_prms.Flt_Dtc[V_OPEN]) {
3900  0c56 e6ea004a          	ldab	_vs_Isense_c,y
3901  0c5a f1002f            	cmpb	_Vs_flt_cal_prms
3902  0c5d 2403              	bhs	L3651
3903                         ; 2420 							vsF_Updt_Flt_Stat(f, VS_OPEN);
3905  0c5f c7                	clrb	
3908  0c60 2047              	bra	LC036
3909  0c62                   L3651:
3910                         ; 2424 							vs_prm_set[f].flt_stat_c = VS_NO_FLT_MASK;
3912  0c62 e680              	ldab	OFST-2,s
3913  0c64 860f              	ldaa	#15
3914  0c66 12                	mul	
3915  0c67 b746              	tfr	d,y
3916  0c69 205d              	bra	LC035
3917  0c6b                   L1651:
3918                         ; 2430 						vs_prm_set[f].flt_stat_c &= VS_CLR_PRLM_OPEN;
3920  0c6b e680              	ldab	OFST-2,s
3921  0c6d 860f              	ldaa	#15
3922  0c6f 12                	mul	
3923  0c70 b746              	tfr	d,y
3924  0c72 0dea000501        	bclr	_vs_prm_set+2,y,1
3925  0c77 2061              	bra	L5161
3926  0c79                   L7551:
3927                         ; 2436 					vsF_Updt_Flt_Stat(f, VS_SHRT_TO_GND);
3929  0c79 cc0001            	ldd	#1
3931  0c7c 202b              	bra	LC036
3932  0c7e                   L5551:
3933                         ; 2441 				if (vs_Flag1_c[f].b.sw_rq_b == FALSE) {
3935  0c7e b746              	tfr	d,y
3936  0c80 0eea00500254      	brset	_vs_Flag1_c,y,2,L5161
3937                         ; 2444 					if (vs_prm_set[f].Vsense_dly_cnt < VS_VSENSE_DLY)  {
3939  0c86 860f              	ldaa	#15
3940  0c88 12                	mul	
3941  0c89 b746              	tfr	d,y
3942  0c8b e6ea0006          	ldab	_vs_prm_set+3,y
3943  0c8f c102              	cmpb	#2
3944  0c91 2406              	bhs	L7751
3945                         ; 2447 						vs_prm_set[f].Vsense_dly_cnt++;
3947  0c93 62ea0006          	inc	_vs_prm_set+3,y
3949  0c97 2041              	bra	L5161
3950  0c99                   L7751:
3951                         ; 2451 						if (vs_Vsense_c[f] >= Vs_flt_cal_prms.Flt_Dtc[V_SHRT_TO_BATT]) {
3953  0c99 e680              	ldab	OFST-2,s
3954  0c9b b796              	exg	b,y
3955  0c9d e6ea004c          	ldab	_vs_Vsense_c,y
3956  0ca1 f10031            	cmpb	_Vs_flt_cal_prms+2
3957  0ca4 250e              	blo	L3061
3958                         ; 2454 							vsF_Updt_Flt_Stat(f, VS_SHRT_TO_BATT);
3960  0ca6 cc0002            	ldd	#2
3962  0ca9                   LC036:
3963  0ca9 3b                	pshd	
3964  0caa e682              	ldab	OFST+0,s
3965  0cac 4a0b3d3d          	call	f_vsF_Updt_Flt_Stat
3966  0cb0 1b82              	leas	2,s
3968  0cb2 2026              	bra	L5161
3969  0cb4                   L3061:
3970                         ; 2458 							vs_prm_set[f].flt_stat_c &= VS_CLR_PRLM_BATT;
3972  0cb4 e680              	ldab	OFST-2,s
3973  0cb6 860f              	ldaa	#15
3974  0cb8 12                	mul	
3975  0cb9 b746              	tfr	d,y
3976  0cbb 0dea000504        	bclr	_vs_prm_set+2,y,4
3977                         ; 2461 							if (vs_prm_set[f].flt_stat_c > VS_PRL_FLT_MAX) {
3979  0cc0 e6ea0005          	ldab	_vs_prm_set+2,y
3980  0cc4 c107              	cmpb	#7
3981  0cc6 2212              	bhi	L5161
3983                         ; 2467 								vs_prm_set[f].flt_stat_c = VS_NO_FLT_MASK;
3985  0cc8                   LC035:
3986  0cc8 69ea0005          	clr	_vs_prm_set+2,y
3987  0ccc 200c              	bra	L5161
3988  0cce                   L3551:
3989                         ; 2481 			vs_prm_set[f].flt_mtr_cnt = 0;
3991  0cce e680              	ldab	OFST-2,s
3992  0cd0 860f              	ldaa	#15
3993  0cd2 12                	mul	
3994  0cd3 b746              	tfr	d,y
3995  0cd5 1869ea0009        	clrw	_vs_prm_set+6,y
3996  0cda                   L5161:
3997                         ; 2485 		if ( (vs_prv_flt_typ_c != vs_prm_set[f].flt_stat_c) || (vs_prm_set[f].flt_stat_c == VS_NO_FLT_MASK) ) {
3999  0cda e680              	ldab	OFST-2,s
4000  0cdc 860f              	ldaa	#15
4001  0cde 12                	mul	
4002  0cdf b746              	tfr	d,y
4003  0ce1 e6ea0005          	ldab	_vs_prm_set+2,y
4004  0ce5 e181              	cmpb	OFST-1,s
4005  0ce7 2603              	bne	L1261
4007  0ce9 046105            	tbne	b,L7161
4008  0cec                   L1261:
4009                         ; 2487 			vs_prm_set[f].flt_mtr_cnt = 0;
4011  0cec 1869ea0009        	clrw	_vs_prm_set+6,y
4012  0cf1                   L7161:
4013                         ; 2397 	for (f=0; f < VS_LMT; f++) {
4015  0cf1 6280              	inc	OFST-2,s
4018  0cf3 e680              	ldab	OFST-2,s
4019  0cf5 c102              	cmpb	#2
4020  0cf7 1825ff07          	blo	L5451
4021                         ; 2492 } //End of function
4024  0cfb 31                	puly	
4025  0cfc 0a                	rtc	
4050                         ; 2520 @far void vsF_DTC_Manager(void)
4050                         ; 2521 {
4051                         	switch	.ftext
4052  0cfd                   f_vsF_DTC_Manager:
4056                         ; 2523 	if (vs_slct == VS_FL)
4058  0cfd f60026            	ldab	_vs_slct
4059  0d00 2604              	bne	L3361
4060                         ; 2526 		vs_slct = VS_FR;
4062  0d02 c601              	ldab	#1
4064  0d04 2001              	bra	L5361
4065  0d06                   L3361:
4066                         ; 2531 		vs_slct = VS_FL;
4068  0d06 c7                	clrb	
4069  0d07                   L5361:
4070  0d07 7b0026            	stab	_vs_slct
4071                         ; 2536 	if ( (vs_prm_set[vs_slct].flt_stat_c & VS_MTRD_OPEN_MASK) == VS_MTRD_OPEN_MASK)
4073  0d0a 860f              	ldaa	#15
4074  0d0c 12                	mul	
4075  0d0d b746              	tfr	d,y
4076  0d0f f60026            	ldab	_vs_slct
4077  0d12 0fea0005080c      	brclr	_vs_prm_set+2,y,8,L7361
4078                         ; 2539 		if (vs_slct == VS_FL)
4080  0d18 2605              	bne	L1461
4081                         ; 2542 			FMemLibF_ReportDtc(DTC_VS_FL_OPEN_K, ERROR_ON_K);
4083  0d1a cc0001            	ldd	#1
4086  0d1d 2008              	bra	LC038
4087  0d1f                   L1461:
4088                         ; 2547 			FMemLibF_ReportDtc(DTC_VS_FR_OPEN_K, ERROR_ON_K);
4090  0d1f cc0001            	ldd	#1
4092  0d22 200e              	bra	LC037
4093  0d24                   L7361:
4094                         ; 2553 		if (vs_slct == VS_FL)
4096  0d24 260a              	bne	L7461
4097                         ; 2556 			FMemLibF_ReportDtc(DTC_VS_FL_OPEN_K, ERROR_OFF_K);
4099  0d26 87                	clra	
4100  0d27                   LC038:
4101  0d27 3b                	pshd	
4102  0d28 ce009e            	ldx	#158
4103  0d2b cc9d13            	ldd	#-25325
4106  0d2e 2009              	bra	L5461
4107  0d30                   L7461:
4108                         ; 2561 			FMemLibF_ReportDtc(DTC_VS_FR_OPEN_K, ERROR_OFF_K);
4110  0d30 87                	clra	
4111  0d31 c7                	clrb	
4112  0d32                   LC037:
4113  0d32 3b                	pshd	
4114  0d33 ce009e            	ldx	#158
4115  0d36 cc9e13            	ldd	#-25069
4117  0d39                   L5461:
4118  0d39 4a000000          	call	f_FMemLibF_ReportDtc
4119  0d3d 1b82              	leas	2,s
4120                         ; 2568 	if ( (vs_prm_set[vs_slct].flt_stat_c & VS_MTRD_GND_MASK) == VS_MTRD_GND_MASK)
4122  0d3f f60026            	ldab	_vs_slct
4123  0d42 860f              	ldaa	#15
4124  0d44 12                	mul	
4125  0d45 b746              	tfr	d,y
4126  0d47 f60026            	ldab	_vs_slct
4127  0d4a 0fea0005100c      	brclr	_vs_prm_set+2,y,16,L3561
4128                         ; 2571 		if (vs_slct == VS_FL)
4130  0d50 2605              	bne	L5561
4131                         ; 2574 			FMemLibF_ReportDtc(DTC_VS_FL_SHORT_TO_GND_K, ERROR_ON_K);
4133  0d52 cc0001            	ldd	#1
4136  0d55 2008              	bra	LC040
4137  0d57                   L5561:
4138                         ; 2579 			FMemLibF_ReportDtc(DTC_VS_FR_SHORT_TO_GND_K, ERROR_ON_K);
4140  0d57 cc0001            	ldd	#1
4142  0d5a 200e              	bra	LC039
4143  0d5c                   L3561:
4144                         ; 2585 		if (vs_slct == VS_FL)
4146  0d5c 260a              	bne	L3661
4147                         ; 2588 			FMemLibF_ReportDtc(DTC_VS_FL_SHORT_TO_GND_K, ERROR_OFF_K);
4149  0d5e 87                	clra	
4150  0d5f                   LC040:
4151  0d5f 3b                	pshd	
4152  0d60 ce009e            	ldx	#158
4153  0d63 cc9d11            	ldd	#-25327
4156  0d66 2009              	bra	L1661
4157  0d68                   L3661:
4158                         ; 2593 			FMemLibF_ReportDtc(DTC_VS_FR_SHORT_TO_GND_K, ERROR_OFF_K);
4160  0d68 87                	clra	
4161  0d69 c7                	clrb	
4162  0d6a                   LC039:
4163  0d6a 3b                	pshd	
4164  0d6b ce009e            	ldx	#158
4165  0d6e cc9e11            	ldd	#-25071
4167  0d71                   L1661:
4168  0d71 4a000000          	call	f_FMemLibF_ReportDtc
4169  0d75 1b82              	leas	2,s
4170                         ; 2599 	if ( (vs_prm_set[vs_slct].flt_stat_c & VS_MTRD_BATT_MASK) == VS_MTRD_BATT_MASK)
4172  0d77 f60026            	ldab	_vs_slct
4173  0d7a 860f              	ldaa	#15
4174  0d7c 12                	mul	
4175  0d7d b746              	tfr	d,y
4176  0d7f f60026            	ldab	_vs_slct
4177  0d82 0fea0005200c      	brclr	_vs_prm_set+2,y,32,L7661
4178                         ; 2602 		if (vs_slct == VS_FL)
4180  0d88 2605              	bne	L1761
4181                         ; 2605 			FMemLibF_ReportDtc(DTC_VS_FL_SHORT_TO_BAT_K, ERROR_ON_K);
4183  0d8a cc0001            	ldd	#1
4186  0d8d 2008              	bra	LC042
4187  0d8f                   L1761:
4188                         ; 2610 			FMemLibF_ReportDtc(DTC_VS_FR_SHORT_TO_BAT_K, ERROR_ON_K);
4190  0d8f cc0001            	ldd	#1
4192  0d92 200e              	bra	LC041
4193  0d94                   L7661:
4194                         ; 2616 		if (vs_slct == VS_FL)
4196  0d94 260a              	bne	L7761
4197                         ; 2619 			FMemLibF_ReportDtc(DTC_VS_FL_SHORT_TO_BAT_K, ERROR_OFF_K);
4199  0d96 87                	clra	
4200  0d97                   LC042:
4201  0d97 3b                	pshd	
4202  0d98 ce009e            	ldx	#158
4203  0d9b cc9d12            	ldd	#-25326
4206  0d9e 2009              	bra	L5761
4207  0da0                   L7761:
4208                         ; 2624 			FMemLibF_ReportDtc(DTC_VS_FR_SHORT_TO_BAT_K, ERROR_OFF_K);
4210  0da0 87                	clra	
4211  0da1 c7                	clrb	
4212  0da2                   LC041:
4213  0da2 3b                	pshd	
4214  0da3 ce009e            	ldx	#158
4215  0da6 cc9e12            	ldd	#-25070
4217  0da9                   L5761:
4218  0da9 4a000000          	call	f_FMemLibF_ReportDtc
4219  0dad 1b82              	leas	2,s
4220                         ; 2627 }
4223  0daf 0a                	rtc	
4290                         ; 2684 @far u_8Bit vsF_Chck_Diag_Prms(u_8Bit vs_LID_ser, u_8Bit *vs_data_p)
4290                         ; 2685 {
4291                         	switch	.ftext
4292  0db0                   f_vsF_Chck_Diag_Prms:
4294  0db0 3b                	pshd	
4295  0db1 1b9d              	leas	-3,s
4296       00000003          OFST:	set	3
4299                         ; 2687 	u_8Bit vs_LID_service = vs_LID_ser;       // LID service
4301  0db3 6b82              	stab	OFST-1,s
4302                         ; 2688 	u_8Bit *vs_data_p_c = vs_data_p;          // pointer to start address of data needs to be checked
4304  0db5 18028880          	movw	OFST+5,s,OFST-3,s
4305                         ; 2689 	u_8Bit vs_chck_rslt = FALSE;              // check result
4307                         ; 2692 	if (vs_LID_service == VS_PWM) {
4309  0db9 c102              	cmpb	#2
4310  0dbb 2632              	bne	L3371
4311                         ; 2696 		if ( ( *vs_data_p_c < 50 || *vs_data_p_c > 100 ) ||
4311                         ; 2697 		   ( ( *(vs_data_p_c + 1) < 50 || *(vs_data_p_c + 1) > 100) ) ) {
4313  0dbd ed80              	ldy	OFST-3,s
4314  0dbf e640              	ldab	0,y
4315  0dc1 c132              	cmpb	#50
4316  0dc3 252a              	blo	L3371
4318  0dc5 c164              	cmpb	#100
4319  0dc7 2226              	bhi	L3371
4321  0dc9 e641              	ldab	1,y
4322  0dcb c132              	cmpb	#50
4323  0dcd 2520              	blo	L3371
4325  0dcf c164              	cmpb	#100
4326                         ; 2700 			vs_chck_rslt = FALSE;
4329  0dd1 221c              	bhi	L3371
4330                         ; 2706 			Vs_cal_prms.cals[hs_vehicle_line_c].PWM_a[VL_LOW] = *vs_data_p_c;
4332  0dd3 f60000            	ldab	_hs_vehicle_line_c
4333  0dd6 87                	clra	
4334  0dd7 cd0003            	ldy	#3
4335  0dda 13                	emul	
4336  0ddb b746              	tfr	d,y
4337  0ddd ee80              	ldx	OFST-3,s
4338  0ddf 180a00ea0038      	movb	0,x,_Vs_cal_prms+1,y
4339                         ; 2707 			Vs_cal_prms.cals[hs_vehicle_line_c].PWM_a[VL_HI - 1] = *(vs_data_p_c + 1);			
4341  0de5 180a01ea0039      	movb	1,x,_Vs_cal_prms+2,y
4342                         ; 2711 			vs_chck_rslt = TRUE;
4344  0deb c601              	ldab	#1
4345  0ded 2001              	bra	L5471
4346  0def                   L3371:
4347                         ; 2716 		vs_chck_rslt = FALSE;
4349  0def c7                	clrb	
4350  0df0                   L5471:
4351                         ; 2718 	return(vs_chck_rslt);
4355  0df0 1b85              	leas	5,s
4356  0df2 0a                	rtc	
4404                         ; 2741 @far u_8Bit vsF_Write_Diag_Data(u_8Bit vs_LID)
4404                         ; 2742 {
4405                         	switch	.ftext
4406  0df3                   f_vsF_Write_Diag_Data:
4408  0df3 3b                	pshd	
4409  0df4 37                	pshb	
4410       00000001          OFST:	set	1
4413                         ; 2744 	u_8Bit vs_LID_s = vs_LID;       // LID service
4415  0df5 6b80              	stab	OFST-1,s
4416                         ; 2745 	u_8Bit vs_write_rslt = FALSE;   // write result (success or fail)
4418                         ; 2748 	if (vs_LID_s == VS_PWM) {
4420  0df7 c102              	cmpb	#2
4421  0df9 2611              	bne	L7671
4422                         ; 2750 		EE_BlockWrite(EE_VENT_PWM_DATA, (u_8Bit*)&Vs_cal_prms);
4424  0dfb cc0037            	ldd	#_Vs_cal_prms
4425  0dfe 3b                	pshd	
4426  0dff cc0007            	ldd	#7
4427  0e02 4a000000          	call	f_EE_BlockWrite
4429  0e06 1b82              	leas	2,s
4430                         ; 2752 		vs_write_rslt = TRUE;
4432  0e08 c601              	ldab	#1
4434  0e0a 2001              	bra	L1771
4435  0e0c                   L7671:
4436                         ; 2756 		vs_write_rslt = FALSE;
4438  0e0c c7                	clrb	
4439  0e0d                   L1771:
4440                         ; 2758 	return(vs_write_rslt);
4444  0e0d 1b83              	leas	3,s
4445  0e0f 0a                	rtc	
4552                         ; 2783 @far void vsF_Diag_DTC_Control(Vs_DTC_Control_e Vs_DTC_cntrl, u_8Bit vs_DTC, Vs_Location_e Vent_Location)
4552                         ; 2784 {
4553                         	switch	.ftext
4554  0e10                   f_vsF_Diag_DTC_Control:
4556  0e10 3b                	pshd	
4557  0e11 37                	pshb	
4558       00000001          OFST:	set	1
4561                         ; 2786 	u_8Bit vs_set_DTC = vs_DTC;    // DTC to be set
4563  0e12 180a8780          	movb	OFST+6,s,OFST-1,s
4564                         ; 2788 	if (Vs_DTC_cntrl == VS_SET_DTC) {
4566  0e16 e682              	ldab	OFST+1,s
4567  0e18 04210f            	dbne	b,L3402
4568                         ; 2790 		vs_prm_set[Vent_Location].flt_stat_c = vs_set_DTC;
4570  0e1b e689              	ldab	OFST+8,s
4571  0e1d 860f              	ldaa	#15
4572  0e1f 12                	mul	
4573  0e20 b746              	tfr	d,y
4574  0e22 180a80ea0005      	movb	OFST-1,s,_vs_prm_set+2,y
4576  0e28 2010              	bra	L5402
4577  0e2a                   L3402:
4578                         ; 2794 		vs_prm_set[Vent_Location].flt_stat_c = VS_NO_FLT_MASK;
4580  0e2a e689              	ldab	OFST+8,s
4581  0e2c 860f              	ldaa	#15
4582  0e2e 12                	mul	
4583  0e2f b746              	tfr	d,y
4584  0e31 69ea0005          	clr	_vs_prm_set+2,y
4585                         ; 2797 		vs_prm_set[Vent_Location].flt_mtr_cnt = 0;
4587  0e35 1869ea0009        	clrw	_vs_prm_set+6,y
4588  0e3a                   L5402:
4589                         ; 2799 }
4592  0e3a 1b83              	leas	3,s
4593  0e3c 0a                	rtc	
4627                         ; 2812 @far u_16Bit vsF_Modify_StartUp_Tm(void)
4627                         ; 2813 {
4628                         	switch	.ftext
4629  0e3d                   f_vsF_Modify_StartUp_Tm:
4631  0e3d 3b                	pshd	
4632       00000002          OFST:	set	2
4635                         ; 2817 	if (diag_eol_io_lock_b == TRUE) {
4637  0e3e 1f00000109        	brclr	_diag_io_lock3_flags_c,1,L3602
4638                         ; 2819 		Vs_flt_cal_prms.start_up_tm = 0;
4640  0e43 87                	clra	
4641  0e44 c7                	clrb	
4642  0e45 7c0033            	std	_Vs_flt_cal_prms+4
4643                         ; 2822 		vs_result_c = Vs_flt_cal_prms.start_up_tm ;
4645  0e48 6c80              	std	OFST-2,s
4647  0e4a 2003              	bra	L5602
4648  0e4c                   L3602:
4649                         ; 2826 		vs_result_c = VS_FAILED_RSLT;
4651  0e4c cc0001            	ldd	#1
4652  0e4f                   L5602:
4653                         ; 2828 	return(vs_result_c);
4657  0e4f 31                	puly	
4658  0e50 0a                	rtc	
4699                         ; 2847 @far u_8Bit vsF_Get_Actv_outputNumber(void)
4699                         ; 2848 {
4700                         	switch	.ftext
4701  0e51                   f_vsF_Get_Actv_outputNumber:
4703  0e51 3b                	pshd	
4704       00000002          OFST:	set	2
4707                         ; 2849 	u_8Bit vs_actv_number_c = VS_ZERO_ACTV;	// Number of active vents
4709                         ; 2850 	u_8Bit lc1 = 0;							// loop counter
4711                         ; 2852 	for (lc1=0; lc1 <VS_LMT; lc1++)
4713  0e52 87                	clra	
4714  0e53 c7                	clrb	
4715  0e54 6c80              	std	OFST-2,s
4716  0e56 180e              	tab	
4717  0e58                   L5012:
4718                         ; 2855 		if(  (vs_Flag1_c[lc1].b.state_b == TRUE) && (vs_prm_set[lc1].crt_lv_c != VL0) ){
4720  0e58 87                	clra	
4721  0e59 b746              	tfr	d,y
4722  0e5b 0fea0050010d      	brclr	_vs_Flag1_c,y,1,L5112
4724  0e61 860f              	ldaa	#15
4725  0e63 12                	mul	
4726  0e64 b746              	tfr	d,y
4727  0e66 e6ea0003          	ldab	_vs_prm_set,y
4728  0e6a 2702              	beq	L5112
4729                         ; 2857 			vs_actv_number_c++;			
4731  0e6c 6281              	inc	OFST-1,s
4733  0e6e                   L5112:
4734                         ; 2852 	for (lc1=0; lc1 <VS_LMT; lc1++)
4736  0e6e 6280              	inc	OFST-2,s
4739  0e70 e680              	ldab	OFST-2,s
4740  0e72 c102              	cmpb	#2
4741  0e74 25e2              	blo	L5012
4742                         ; 2862 	return(vs_actv_number_c);
4744  0e76 e681              	ldab	OFST-1,s
4747  0e78 31                	puly	
4748  0e79 0a                	rtc	
4788                         ; 2879 @far u_8Bit vsF_Get_Fault_Status(u_8Bit vs_choice)
4788                         ; 2880 {
4789                         	switch	.ftext
4790  0e7a                   f_vsF_Get_Fault_Status:
4792       00000001          OFST:	set	1
4795                         ; 2882 	u_8Bit vs_chosen = vs_choice;
4797                         ; 2885 	return(vs_prm_set[vs_choice].flt_stat_c);
4799  0e7a 860f              	ldaa	#15
4800  0e7c 12                	mul	
4801  0e7d b746              	tfr	d,y
4802  0e7f e6ea0005          	ldab	_vs_prm_set+2,y
4805  0e83 0a                	rtc	
5274                         	xdef	f_vsF_Updt_Flt_Stat
5275                         	xdef	f_vsLF_Chck_mainState
5276                         	xdef	f_vsF_Diag_IO_Cntrl
5277                         	xdef	f_vsF_Rtrn_Cntrl_toECU
5278                         	xdef	f_vsF_Rqst_Drg_Hs_Cycl
5279                         	xdef	f_vsF_swtch_Cntrl
5280                         	xdef	f_vsF_Chck_Swtch_Prps
5281                         	xdef	f_vsF_Fnsh_LED_Dsply
5282                         	xdef	f_vsF_swtch_Mngmnt
5283                         	xdef	f_vsLF_RemSt_Check
5284                         	xdef	f_vsF_Diag_IO_Mngmnt
5285                         	xdef	f_vsF_Motor_StrtUp
5286                         	xdef	f_vsF_Updt_Crrt_Lvl
5287                         	xdef	f_vsF_Nrmlz_PWM
5288                         	xdef	f_vsF_Updt_Status
5289                         	xdef	f_vsF_Isense_AD_Rdg
5290                         	xdef	f_vsF_Vsense_AD_Rdg
5291                         	xdef	f_vsF_AD_Cnvrsn
5292                         	switch	.bss
5293  0000                   _vs_AD_prms:
5294  0000 000000            	ds.b	3
5295                         	xdef	_vs_AD_prms
5296  0003                   _vs_prm_set:
5297  0003 0000000000000000  	ds.b	30
5298                         	xdef	_vs_prm_set
5299  0021                   _vs_lst_fr_led_stat_c:
5300  0021 00                	ds.b	1
5301                         	xdef	_vs_lst_fr_led_stat_c
5302  0022                   _vs_lst_fl_led_stat_c:
5303  0022 00                	ds.b	1
5304                         	xdef	_vs_lst_fl_led_stat_c
5305  0023                   _vs_lst_main_state_c:
5306  0023 00                	ds.b	1
5307                         	xdef	_vs_lst_main_state_c
5308  0024                   _vs_other_drive_c:
5309  0024 00                	ds.b	1
5310                         	xdef	_vs_other_drive_c
5311  0025                   _vs_slctd_drive_c:
5312  0025 00                	ds.b	1
5313                         	xdef	_vs_slctd_drive_c
5314  0026                   _vs_slct:
5315  0026 00                	ds.b	1
5316                         	xdef	_vs_slct
5317  0027                   _vs_Isense_offset_w:
5318  0027 00000000          	ds.b	4
5319                         	xdef	_vs_Isense_offset_w
5320  002b                   _vs_flt_mtr_tm:
5321  002b 0000              	ds.b	2
5322                         	xdef	_vs_flt_mtr_tm
5323  002d                   _vs_LED_dsply_tm:
5324  002d 0000              	ds.b	2
5325                         	xdef	_vs_LED_dsply_tm
5326                         	xref	f_EE_BlockRead
5327                         	xref	f_EE_BlockWrite
5328                         	xref	f_hsLF_UpdateHeatParameters
5329                         	xref	f_hsF_FrontHeatSeatStatus
5330                         	xref	_hs_vehicle_line_c
5331                         	xref	_hs_status_flags_c
5332                         	xref	_relay_control_c
5333                         	xref	_relay_status_c
5334                         	xref	f_FMemLibF_ReportDtc
5335                         	xref	f_Pwm_SetDutyCycle
5336                         	xref	_DioConfigData
5337                         	xref	_Dio_PortWrite_Ptr
5338                         	xref	_ADF_Nrmlz_Vs_Isense
5339                         	xref	_ADF_Nrmlz_Vs_Vsense
5340                         	xref	_ADF_AtoD_Cnvrsn
5341                         	xref	_ADF_Str_UnFlt_Mux_Rslt
5342                         	xref	_ADF_Mux_Filter
5343                         	xref	_AD_mux_Filter
5344                         	xref	_diag_io_vs_lock_flags_c
5345                         	xref	_diag_io_hs_lock_flags_c
5346                         	xref	_diag_io_lock3_flags_c
5347                         	xref	_diag_io_lock2_flags_c
5348                         	xref	_diag_io_vs_state_c
5349                         	xref	_diag_io_vs_rq_c
5350                         	xref	_diag_io_vs_all_out_c
5351                         	xref	_diag_rc_lock_flags_c
5352                         	xref	_canio_LHD_RHD_c
5353                         	xref	_canio_RX_IGN_STATUS_c
5354                         	xref	_canio_hvac_vent_seat_request_c
5355                         	xref	_canio_vent_seat_request_c
5356                         	xref	_mainF_Calc_PWM
5357                         	xref	_main_SwReq_Front_c
5358                         	xref	_main_state_c
5359                         	xref	_main_CSWM_Status_c
5360                         	xdef	f_vsF_Get_Actv_outputNumber
5361                         	xdef	f_vsF_Get_Fault_Status
5362                         	xdef	f_vsF_Modify_StartUp_Tm
5363                         	xdef	f_vsF_Diag_DTC_Control
5364                         	xdef	f_vsF_Get_Output_Stat
5365                         	xdef	f_vsF_Write_Diag_Data
5366                         	xdef	f_vsF_Chck_Diag_Prms
5367                         	xdef	f_vsF_LED_Display
5368                         	xdef	f_vsF_Clear
5369                         	xdef	f_vsF_DTC_Manager
5370                         	xdef	f_vsF_Fault_Mntr
5371                         	xdef	f_vsF_Manager
5372                         	xdef	f_vsF_Output_Cntrl
5373                         	xdef	f_vsF_ADC_Monitor
5374                         	xdef	f_vsF_Init
5375  002f                   _Vs_flt_cal_prms:
5376  002f 0000000000000000  	ds.b	8
5377                         	xdef	_Vs_flt_cal_prms
5378  0037                   _Vs_cal_prms:
5379  0037 0000000000000000  	ds.b	18
5380                         	xdef	_Vs_cal_prms
5381  0049                   _vs_shrtTo_batt_mntr_c:
5382  0049 00                	ds.b	1
5383                         	xdef	_vs_shrtTo_batt_mntr_c
5384  004a                   _vs_Isense_c:
5385  004a 0000              	ds.b	2
5386                         	xdef	_vs_Isense_c
5387  004c                   _vs_Vsense_c:
5388  004c 0000              	ds.b	2
5389                         	xdef	_vs_Vsense_c
5390  004e                   _vs_Flag2_c:
5391  004e 0000              	ds.b	2
5392                         	xdef	_vs_Flag2_c
5393  0050                   _vs_Flag1_c:
5394  0050 0000              	ds.b	2
5395                         	xdef	_vs_Flag1_c
5416                         	end
