   1                         ; C Compiler for S12XE [COSMIC Software].
   2                         ; Generator V4.7.8 - 27 Jul 2007
   3                         ; Optimizer V4.6.13 - 27 Jul 2007
 706                         ; 116 void DESC_API_CALLBACK_TYPE ApplDescControl_2F500103_Actuate_all_heaters(DescMsgContext* pMsgContext)
 706                         ; 117 {
 707                         .ftext:	section	.text
 708  0000                   f_ApplDescControl_2F500103_Actuate_all_heaters:
 710  0000 3b                	pshd	
 711       00000000          OFST:	set	0
 714                         ; 120 	DataLength = 1;
 716  0001 cc0001            	ldd	#1
 717  0004 7c0000            	std	L762_DataLength
 718                         ; 123 	if(diag_rc_all_op_lock_b == FALSE)
 720  0007 1e00000139        	brset	_diag_rc_lock_flags_c,1,L144
 721                         ; 127 		if (pMsgContext->reqData[0] <= 4)
 723  000c edf30000          	ldy	[OFST+0,s]
 724  0010 e640              	ldab	0,y
 725  0012 c104              	cmpb	#4
 726  0014 222b              	bhi	L344
 727                         ; 130 			diag_io_all_hs_lock_b = 1; // TRUE;
 729  0016 1c000a10          	bset	_diag_io_lock2_flags_c,16
 730                         ; 133 			diag_io_active_b = 1; // TRUE;
 732  001a 1c000908          	bset	_diag_io_lock3_flags_c,8
 733                         ; 136 			diag_io_hs_rq_c[FRONT_LEFT] = pMsgContext->reqData[0];
 735  001e 7b0014            	stab	_diag_io_hs_rq_c
 736                         ; 137 			diag_io_hs_rq_c[FRONT_RIGHT] = pMsgContext->reqData[0];
 738  0021 7b0015            	stab	_diag_io_hs_rq_c+1
 739                         ; 140 			if ((CSWM_config_c & CSWM_4HS_VARIANT_MASK_K) == CSWM_4HS_VARIANT_MASK_K)
 741  0024 1e00000f02        	brset	_CSWM_config_c,15,LC001
 742  0029 2006              	bra	L544
 743  002b                   LC001:
 744                         ; 142 				diag_io_hs_rq_c[REAR_LEFT] = pMsgContext->reqData[0];
 746  002b 7b0016            	stab	_diag_io_hs_rq_c+2
 747                         ; 143 				diag_io_hs_rq_c[REAR_RIGHT] = pMsgContext->reqData[0];
 749  002e 7b0017            	stab	_diag_io_hs_rq_c+3
 750  0031                   L544:
 751                         ; 146 				diag_io_all_vs_lock_b = 0;
 753  0031 1d000a01          	bclr	_diag_io_lock2_flags_c,1
 754                         ; 147 				diag_io_vs_lock_flags_c[LEFT_FRONT].b.switch_rq_lock_b  = 0;
 756  0035 1d000301          	bclr	_diag_io_vs_lock_flags_c,1
 757                         ; 148 	 			diag_io_vs_lock_flags_c[RIGHT_FRONT].b.switch_rq_lock_b = 0;
 759  0039 1d000401          	bclr	_diag_io_vs_lock_flags_c+1,1
 760                         ; 150 				Response = RESPONSE_OK;
 762  003d c601              	ldab	#1
 764  003f 2006              	bra	L154
 765  0041                   L344:
 766                         ; 155 			Response = REQUEST_OUT_OF_RANGE;
 768  0041 c604              	ldab	#4
 769  0043 2002              	bra	L154
 770  0045                   L144:
 771                         ; 162 		Response = CONDITIONS_NOT_CORRECT;	// Response with conditions not correct
 773  0045 c606              	ldab	#6
 774  0047                   L154:
 775  0047 7b0002            	stab	L562_Response
 776                         ; 165 	 if(diag_io_active_b == TRUE)
 778  004a 1f00090803        	brclr	_diag_io_lock3_flags_c,8,L554
 779                         ; 169 		 mainF_CSWM_Status();
 781  004f 160000            	jsr	_mainF_CSWM_Status
 784  0052                   L554:
 785                         ; 175 	diagF_DiagResponse(Response, DataLength/*+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext);   
 787  0052 ec80              	ldd	OFST+0,s
 788  0054 3b                	pshd	
 789  0055 fc0000            	ldd	L762_DataLength
 790  0058 3b                	pshd	
 791  0059 f60002            	ldab	L562_Response
 792  005c 87                	clra	
 793  005d 4a000000          	call	f_diagF_DiagResponse
 795  0061 1b86              	leas	6,s
 796                         ; 176 }
 799  0063 0a                	rtc	
 848                         ; 181 void DESC_API_CALLBACK_TYPE ApplDescControl_2F500203_Actuate_all_Vents(DescMsgContext* pMsgContext)
 848                         ; 182 {
 849                         	switch	.ftext
 850  0064                   f_ApplDescControl_2F500203_Actuate_all_Vents:
 852  0064 3b                	pshd	
 853       00000000          OFST:	set	0
 856                         ; 185 	DataLength = 1;
 858  0065 cc0001            	ldd	#1
 859  0068 7c0000            	std	L762_DataLength
 860                         ; 188 	if(diag_rc_all_op_lock_b == FALSE)
 862  006b 1e00000147        	brset	_diag_rc_lock_flags_c,1,L774
 863                         ; 191 		if ((CSWM_config_c & CSWM_2VS_MASK_K) == CSWM_2VS_MASK_K)
 865  0070 1e00003002        	brset	_CSWM_config_c,48,LC002
 866  0075 203c              	bra	L105
 867  0077                   LC002:
 868                         ; 194 			if (pMsgContext->reqData[0] <= VL_HI)
 870  0077 edf30000          	ldy	[OFST+0,s]
 871  007b e640              	ldab	0,y
 872  007d c103              	cmpb	#3
 873  007f 222e              	bhi	L305
 874                         ; 197 					diag_io_all_vs_lock_b = 1; // TRUE;
 876  0081 1c000a01          	bset	_diag_io_lock2_flags_c,1
 877                         ; 201 					diag_io_active_b = 1; // TRUE;
 879  0085 1c000908          	bset	_diag_io_lock3_flags_c,8
 880                         ; 204 					diag_io_vs_rq_c[LEFT_FRONT] = pMsgContext->reqData[0];
 882  0089 7b000e            	stab	_diag_io_vs_rq_c
 883                         ; 205 					diag_io_vs_rq_c[RIGHT_FRONT] = pMsgContext->reqData[0];
 885  008c 7b000f            	stab	_diag_io_vs_rq_c+1
 886                         ; 207 					vs_Flag2_c[VS_FL].b.diag_clr_ontime_b = TRUE;
 888  008f 1c000008          	bset	_vs_Flag2_c,8
 889                         ; 208 					vs_Flag2_c[VS_FR].b.diag_clr_ontime_b = TRUE;
 891  0093 1c000108          	bset	_vs_Flag2_c+1,8
 892                         ; 210 					diag_io_all_hs_lock_b = 0;
 894  0097 1d000a10          	bclr	_diag_io_lock2_flags_c,16
 895                         ; 211 					diag_io_hs_lock_flags_c[FRONT_LEFT].b.switch_rq_lock_b = 0;
 897  009b 1d000501          	bclr	_diag_io_hs_lock_flags_c,1
 898                         ; 212 					diag_io_hs_lock_flags_c[FRONT_RIGHT].b.switch_rq_lock_b = 0;
 900  009f 1d000601          	bclr	_diag_io_hs_lock_flags_c+1,1
 901                         ; 213 					diag_io_hs_lock_flags_c[REAR_LEFT].b.switch_rq_lock_b = 0;
 903  00a3 1d000701          	bclr	_diag_io_hs_lock_flags_c+2,1
 904                         ; 214 					diag_io_hs_lock_flags_c[REAR_RIGHT].b.switch_rq_lock_b = 0;
 906  00a7 1d000801          	bclr	_diag_io_hs_lock_flags_c+3,1
 907                         ; 217 					Response = RESPONSE_OK;								
 909  00ab c601              	ldab	#1
 911  00ad 200a              	bra	L115
 912  00af                   L305:
 913                         ; 223 					Response = REQUEST_OUT_OF_RANGE;
 915  00af c604              	ldab	#4
 916  00b1 2006              	bra	L115
 917  00b3                   L105:
 918                         ; 229 			Response = SUBFUNCTION_NOT_SUPPORTED;
 920  00b3 c607              	ldab	#7
 921  00b5 2002              	bra	L115
 922  00b7                   L774:
 923                         ; 236 		Response = CONDITIONS_NOT_CORRECT;	// Response with conditions not correct
 925  00b7 c606              	ldab	#6
 926  00b9                   L115:
 927  00b9 7b0002            	stab	L562_Response
 928                         ; 239     if(diag_io_active_b == TRUE)
 930  00bc 1f00090803        	brclr	_diag_io_lock3_flags_c,8,L515
 931                         ; 243 		mainF_CSWM_Status();
 933  00c1 160000            	jsr	_mainF_CSWM_Status
 936  00c4                   L515:
 937                         ; 249   diagF_DiagResponse(Response, DataLength/*+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext); 
 939  00c4 ec80              	ldd	OFST+0,s
 940  00c6 3b                	pshd	
 941  00c7 fc0000            	ldd	L762_DataLength
 942  00ca 3b                	pshd	
 943  00cb f60002            	ldab	L562_Response
 944  00ce 87                	clra	
 945  00cf 4a000000          	call	f_diagF_DiagResponse
 947  00d3 1b86              	leas	6,s
 948                         ; 250 }
 951  00d5 0a                	rtc	
 999                         ; 255 void DESC_API_CALLBACK_TYPE ApplDescControl_2F500303_F_L_Heat_Seat_Request_BI(DescMsgContext* pMsgContext)
 999                         ; 256 {
1000                         	switch	.ftext
1001  00d6                   f_ApplDescControl_2F500303_F_L_Heat_Seat_Request_BI:
1003  00d6 3b                	pshd	
1004       00000000          OFST:	set	0
1007                         ; 258 	DataLength = 1;
1009  00d7 cc0001            	ldd	#1
1010  00da 7c0000            	std	L762_DataLength
1011                         ; 261 	if(diag_rc_all_op_lock_b == FALSE)
1013  00dd 1e00000139        	brset	_diag_rc_lock_flags_c,1,L545
1014                         ; 264 		if ((!diag_io_hs_lock_flags_c[FRONT_LEFT].b.led_status_lock_b) && (!diag_io_all_hs_lock_b) && (pMsgContext->reqData[0] <= 4))
1016  00e2 1e00050226        	brset	_diag_io_hs_lock_flags_c,2,L145
1018  00e7 1e000a1021        	brset	_diag_io_lock2_flags_c,16,L145
1020  00ec edf30000          	ldy	[OFST+0,s]
1021  00f0 e640              	ldab	0,y
1022  00f2 c104              	cmpb	#4
1023  00f4 2217              	bhi	L145
1024                         ; 267 			diag_io_hs_lock_flags_c[FRONT_LEFT].b.switch_rq_lock_b = 1; //TRUE;
1026  00f6 1c000501          	bset	_diag_io_hs_lock_flags_c,1
1027                         ; 271 			diag_io_active_b = 1; // TRUE;
1029  00fa 1c000908          	bset	_diag_io_lock3_flags_c,8
1030                         ; 274 			diag_io_vs_lock_flags_c[LEFT_FRONT].b.switch_rq_lock_b = 0; // FALSE;
1032  00fe 1d000301          	bclr	_diag_io_vs_lock_flags_c,1
1033                         ; 276 			diag_io_hs_rq_c[FRONT_LEFT] = pMsgContext->reqData[0];
1035  0102 7b0014            	stab	_diag_io_hs_rq_c
1036                         ; 277 			DataLength = 0;
1038  0105 18790000          	clrw	L762_DataLength
1039                         ; 278 			Response = RESPONSE_OK;
1041  0109 c601              	ldab	#1
1043  010b 2010              	bra	L155
1044  010d                   L145:
1045                         ; 283 			if (pMsgContext->reqData[0] > 5)
1047  010d edf30000          	ldy	[OFST+0,s]
1048  0111 e640              	ldab	0,y
1049  0113 c105              	cmpb	#5
1050  0115 2304              	bls	L545
1051                         ; 286 				Response = REQUEST_OUT_OF_RANGE;
1053  0117 c604              	ldab	#4
1055  0119 2002              	bra	L155
1056  011b                   L545:
1057                         ; 291 				Response = CONDITIONS_NOT_CORRECT;
1059                         ; 299 		Response = CONDITIONS_NOT_CORRECT;	// Response with conditions not correct
1061  011b c606              	ldab	#6
1062  011d                   L155:
1063  011d 7b0002            	stab	L562_Response
1064                         ; 302 	if(diag_io_active_b == TRUE)
1066  0120 1f00090803        	brclr	_diag_io_lock3_flags_c,8,L555
1067                         ; 306 		mainF_CSWM_Status();
1069  0125 160000            	jsr	_mainF_CSWM_Status
1072  0128                   L555:
1073                         ; 312      diagF_DiagResponse(Response, DataLength/*+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext);	
1075  0128 ec80              	ldd	OFST+0,s
1076  012a 3b                	pshd	
1077  012b fc0000            	ldd	L762_DataLength
1078  012e 3b                	pshd	
1079  012f f60002            	ldab	L562_Response
1080  0132 87                	clra	
1081  0133 4a000000          	call	f_diagF_DiagResponse
1083  0137 1b86              	leas	6,s
1084                         ; 313 }
1087  0139 0a                	rtc	
1135                         ; 318 void DESC_API_CALLBACK_TYPE ApplDescControl_2F500403_F_R_Heat_Seat_Request_BI(DescMsgContext* pMsgContext)
1135                         ; 319 {
1136                         	switch	.ftext
1137  013a                   f_ApplDescControl_2F500403_F_R_Heat_Seat_Request_BI:
1139  013a 3b                	pshd	
1140       00000000          OFST:	set	0
1143                         ; 321 	DataLength = 1;
1145  013b cc0001            	ldd	#1
1146  013e 7c0000            	std	L762_DataLength
1147                         ; 324 	if(diag_rc_all_op_lock_b == FALSE)
1149  0141 1e00000139        	brset	_diag_rc_lock_flags_c,1,L506
1150                         ; 327 		if ((!diag_io_hs_lock_flags_c[FRONT_RIGHT].b.led_status_lock_b) && (!diag_io_all_hs_lock_b) && (pMsgContext->reqData[0] <= 4))
1152  0146 1e00060226        	brset	_diag_io_hs_lock_flags_c+1,2,L106
1154  014b 1e000a1021        	brset	_diag_io_lock2_flags_c,16,L106
1156  0150 edf30000          	ldy	[OFST+0,s]
1157  0154 e640              	ldab	0,y
1158  0156 c104              	cmpb	#4
1159  0158 2217              	bhi	L106
1160                         ; 330 			diag_io_hs_lock_flags_c[FRONT_RIGHT].b.switch_rq_lock_b = 1; //TRUE;
1162  015a 1c000601          	bset	_diag_io_hs_lock_flags_c+1,1
1163                         ; 334 			diag_io_active_b = 1; // TRUE;
1165  015e 1c000908          	bset	_diag_io_lock3_flags_c,8
1166                         ; 337 			diag_io_vs_lock_flags_c[RIGHT_FRONT].b.switch_rq_lock_b = 0; // FALSE;	
1168  0162 1d000401          	bclr	_diag_io_vs_lock_flags_c+1,1
1169                         ; 339 			diag_io_hs_rq_c[FRONT_RIGHT] = pMsgContext->reqData[0];
1171  0166 7b0015            	stab	_diag_io_hs_rq_c+1
1172                         ; 340 			DataLength = 0;
1174  0169 18790000          	clrw	L762_DataLength
1175                         ; 341 			Response = RESPONSE_OK;
1177  016d c601              	ldab	#1
1179  016f 2010              	bra	L116
1180  0171                   L106:
1181                         ; 346 			if (pMsgContext->reqData[0] > 5)
1183  0171 edf30000          	ldy	[OFST+0,s]
1184  0175 e640              	ldab	0,y
1185  0177 c105              	cmpb	#5
1186  0179 2304              	bls	L506
1187                         ; 349 				Response = REQUEST_OUT_OF_RANGE;
1189  017b c604              	ldab	#4
1191  017d 2002              	bra	L116
1192  017f                   L506:
1193                         ; 354 				Response = CONDITIONS_NOT_CORRECT;
1195                         ; 362 		Response = CONDITIONS_NOT_CORRECT;	// Response with conditions not correct
1197  017f c606              	ldab	#6
1198  0181                   L116:
1199  0181 7b0002            	stab	L562_Response
1200                         ; 365 	if(diag_io_active_b == TRUE)
1202  0184 1f00090803        	brclr	_diag_io_lock3_flags_c,8,L516
1203                         ; 369 		mainF_CSWM_Status();
1205  0189 160000            	jsr	_mainF_CSWM_Status
1208  018c                   L516:
1209                         ; 375     diagF_DiagResponse(Response, DataLength/*+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext);
1211  018c ec80              	ldd	OFST+0,s
1212  018e 3b                	pshd	
1213  018f fc0000            	ldd	L762_DataLength
1214  0192 3b                	pshd	
1215  0193 f60002            	ldab	L562_Response
1216  0196 87                	clra	
1217  0197 4a000000          	call	f_diagF_DiagResponse
1219  019b 1b86              	leas	6,s
1220                         ; 376 }
1223  019d 0a                	rtc	
1271                         ; 381 void DESC_API_CALLBACK_TYPE ApplDescControl_2F500503_R_L_Heat_Seat_Request_BI(DescMsgContext* pMsgContext)
1271                         ; 382 {
1272                         	switch	.ftext
1273  019e                   f_ApplDescControl_2F500503_R_L_Heat_Seat_Request_BI:
1275  019e 3b                	pshd	
1276       00000000          OFST:	set	0
1279                         ; 384 	DataLength = 1;
1281  019f cc0001            	ldd	#1
1282  01a2 7c0000            	std	L762_DataLength
1283                         ; 387 	if(diag_rc_all_op_lock_b == FALSE)
1285  01a5 1e00000140        	brset	_diag_rc_lock_flags_c,1,L736
1286                         ; 391 		if ((CSWM_config_c & CSWM_4HS_VARIANT_MASK_K) == CSWM_4HS_VARIANT_MASK_K)
1288  01aa 1e00000f02        	brset	_CSWM_config_c,15,LC005
1289  01af 2035              	bra	L146
1290  01b1                   LC005:
1291                         ; 394 			if ((!diag_io_hs_lock_flags_c[REAR_LEFT].b.led_status_lock_b) && (!diag_io_all_hs_lock_b) && (pMsgContext->reqData[0] <= 4))
1293  01b1 1e00070222        	brset	_diag_io_hs_lock_flags_c+2,2,L346
1295  01b6 1e000a101d        	brset	_diag_io_lock2_flags_c,16,L346
1297  01bb edf30000          	ldy	[OFST+0,s]
1298  01bf e640              	ldab	0,y
1299  01c1 c104              	cmpb	#4
1300  01c3 2213              	bhi	L346
1301                         ; 397 				diag_io_hs_lock_flags_c[REAR_LEFT].b.switch_rq_lock_b = 1; // TRUE;
1303  01c5 1c000701          	bset	_diag_io_hs_lock_flags_c+2,1
1304                         ; 401 				diag_io_active_b = 1; // TRUE;
1306  01c9 1c000908          	bset	_diag_io_lock3_flags_c,8
1307                         ; 404 				diag_io_hs_rq_c[REAR_LEFT] = pMsgContext->reqData[0];
1309  01cd 7b0016            	stab	_diag_io_hs_rq_c+2
1310                         ; 405 				DataLength = 0;
1312  01d0 18790000          	clrw	L762_DataLength
1313                         ; 406 				Response = RESPONSE_OK;
1315  01d4 c601              	ldab	#1
1317  01d6 2014              	bra	L556
1318  01d8                   L346:
1319                         ; 411 				if (pMsgContext->reqData[0] > 5)
1321  01d8 edf30000          	ldy	[OFST+0,s]
1322  01dc e640              	ldab	0,y
1323  01de c105              	cmpb	#5
1324  01e0 2308              	bls	L736
1325                         ; 414 					Response = REQUEST_OUT_OF_RANGE;
1327  01e2 c604              	ldab	#4
1329  01e4 2006              	bra	L556
1330                         ; 419 					Response = CONDITIONS_NOT_CORRECT;
1332  01e6                   L146:
1333                         ; 426 			Response = SUBFUNCTION_NOT_SUPPORTED;
1335  01e6 c607              	ldab	#7
1336  01e8 2002              	bra	L556
1337  01ea                   L736:
1338                         ; 433 		Response = CONDITIONS_NOT_CORRECT;	// Response with conditions not correct
1340  01ea c606              	ldab	#6
1341  01ec                   L556:
1342  01ec 7b0002            	stab	L562_Response
1343                         ; 436 	if(diag_io_active_b == TRUE)
1345  01ef 1f00090803        	brclr	_diag_io_lock3_flags_c,8,L166
1346                         ; 440 		mainF_CSWM_Status();
1348  01f4 160000            	jsr	_mainF_CSWM_Status
1351  01f7                   L166:
1352                         ; 446     diagF_DiagResponse(Response, DataLength/*+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext);
1354  01f7 ec80              	ldd	OFST+0,s
1355  01f9 3b                	pshd	
1356  01fa fc0000            	ldd	L762_DataLength
1357  01fd 3b                	pshd	
1358  01fe f60002            	ldab	L562_Response
1359  0201 87                	clra	
1360  0202 4a000000          	call	f_diagF_DiagResponse
1362  0206 1b86              	leas	6,s
1363                         ; 447 }
1366  0208 0a                	rtc	
1414                         ; 452 void DESC_API_CALLBACK_TYPE ApplDescControl_2F500603_R_R_Heat_Seat_Request_BI(DescMsgContext* pMsgContext)
1414                         ; 453 {
1415                         	switch	.ftext
1416  0209                   f_ApplDescControl_2F500603_R_R_Heat_Seat_Request_BI:
1418  0209 3b                	pshd	
1419       00000000          OFST:	set	0
1422                         ; 455 	DataLength = 1;
1424  020a cc0001            	ldd	#1
1425  020d 7c0000            	std	L762_DataLength
1426                         ; 458 	if(diag_rc_all_op_lock_b == FALSE)
1428  0210 1e00000140        	brset	_diag_rc_lock_flags_c,1,L307
1429                         ; 462 		if ((CSWM_config_c & CSWM_4HS_VARIANT_MASK_K) == CSWM_4HS_VARIANT_MASK_K)
1431  0215 1e00000f02        	brset	_CSWM_config_c,15,LC007
1432  021a 2035              	bra	L507
1433  021c                   LC007:
1434                         ; 465 			if ((!diag_io_hs_lock_flags_c[REAR_RIGHT].b.led_status_lock_b) && (!diag_io_all_hs_lock_b) && (pMsgContext->reqData[0] <= 4))
1436  021c 1e00080222        	brset	_diag_io_hs_lock_flags_c+3,2,L707
1438  0221 1e000a101d        	brset	_diag_io_lock2_flags_c,16,L707
1440  0226 edf30000          	ldy	[OFST+0,s]
1441  022a e640              	ldab	0,y
1442  022c c104              	cmpb	#4
1443  022e 2213              	bhi	L707
1444                         ; 468 				diag_io_hs_lock_flags_c[REAR_RIGHT].b.switch_rq_lock_b = 1; // TRUE;
1446  0230 1c000801          	bset	_diag_io_hs_lock_flags_c+3,1
1447                         ; 472 				diag_io_active_b = 1; // TRUE;
1449  0234 1c000908          	bset	_diag_io_lock3_flags_c,8
1450                         ; 475 				diag_io_hs_rq_c[REAR_RIGHT] = pMsgContext->reqData[0];
1452  0238 7b0017            	stab	_diag_io_hs_rq_c+3
1453                         ; 476 				DataLength = 0;
1455  023b 18790000          	clrw	L762_DataLength
1456                         ; 477 				Response = RESPONSE_OK;
1458  023f c601              	ldab	#1
1460  0241 2014              	bra	L127
1461  0243                   L707:
1462                         ; 482 				if (pMsgContext->reqData[0] > 5)
1464  0243 edf30000          	ldy	[OFST+0,s]
1465  0247 e640              	ldab	0,y
1466  0249 c105              	cmpb	#5
1467  024b 2308              	bls	L307
1468                         ; 485 					Response = REQUEST_OUT_OF_RANGE;
1470  024d c604              	ldab	#4
1472  024f 2006              	bra	L127
1473                         ; 490 					Response = CONDITIONS_NOT_CORRECT;
1475  0251                   L507:
1476                         ; 497 			Response = SUBFUNCTION_NOT_SUPPORTED;
1478  0251 c607              	ldab	#7
1479  0253 2002              	bra	L127
1480  0255                   L307:
1481                         ; 504 		Response = CONDITIONS_NOT_CORRECT;	// Response with conditions not correct
1483  0255 c606              	ldab	#6
1484  0257                   L127:
1485  0257 7b0002            	stab	L562_Response
1486                         ; 507 	if(diag_io_active_b == TRUE)
1488  025a 1f00090803        	brclr	_diag_io_lock3_flags_c,8,L527
1489                         ; 511 		mainF_CSWM_Status();
1491  025f 160000            	jsr	_mainF_CSWM_Status
1494  0262                   L527:
1495                         ; 517     diagF_DiagResponse(Response, DataLength/*+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext);
1497  0262 ec80              	ldd	OFST+0,s
1498  0264 3b                	pshd	
1499  0265 fc0000            	ldd	L762_DataLength
1500  0268 3b                	pshd	
1501  0269 f60002            	ldab	L562_Response
1502  026c 87                	clra	
1503  026d 4a000000          	call	f_diagF_DiagResponse
1505  0271 1b86              	leas	6,s
1506                         ; 518 }
1509  0273 0a                	rtc	
1559                         ; 523 void DESC_API_CALLBACK_TYPE ApplDescControl_2F500703_F_L_Vent_Seat_Request_BI(DescMsgContext* pMsgContext)
1559                         ; 524 {
1560                         	switch	.ftext
1561  0274                   f_ApplDescControl_2F500703_F_L_Vent_Seat_Request_BI:
1563  0274 3b                	pshd	
1564       00000000          OFST:	set	0
1567                         ; 526 	DataLength = 1;
1569  0275 cc0001            	ldd	#1
1570  0278 7c0000            	std	L762_DataLength
1571                         ; 529 	if(diag_rc_all_op_lock_b == FALSE)
1573  027b 1e0000014a        	brset	_diag_rc_lock_flags_c,1,L747
1574                         ; 532 		if ((CSWM_config_c & CSWM_2VS_MASK_K) == CSWM_2VS_MASK_K)
1576  0280 1e00003002        	brset	_CSWM_config_c,48,LC009
1577  0285 203f              	bra	L157
1578  0287                   LC009:
1579                         ; 535 			if ( (diag_io_vs_lock_flags_c[LEFT_FRONT].b.led_status_lock_b == FALSE) && 
1579                         ; 536 				 (diag_io_all_vs_lock_b == FALSE) && 
1579                         ; 537 				 (pMsgContext->reqData[0] <= VL_HI) )
1581  0287 1e0003022a        	brset	_diag_io_vs_lock_flags_c,2,L357
1583  028c 1e000a0125        	brset	_diag_io_lock2_flags_c,1,L357
1585  0291 edf30000          	ldy	[OFST+0,s]
1586  0295 e640              	ldab	0,y
1587  0297 c103              	cmpb	#3
1588  0299 221b              	bhi	L357
1589                         ; 540 				diag_io_vs_lock_flags_c[LEFT_FRONT].b.switch_rq_lock_b = 1; // TRUE;
1591  029b 1c000301          	bset	_diag_io_vs_lock_flags_c,1
1592                         ; 544 				diag_io_active_b = 1; // TRUE;
1594  029f 1c000908          	bset	_diag_io_lock3_flags_c,8
1595                         ; 547 				vs_Flag2_c[VS_FL].b.diag_clr_ontime_b = TRUE;
1597  02a3 1c000008          	bset	_vs_Flag2_c,8
1598                         ; 549 				diag_io_hs_lock_flags_c[FRONT_LEFT].b.switch_rq_lock_b = 0; // FALSE;
1600  02a7 1d000501          	bclr	_diag_io_hs_lock_flags_c,1
1601                         ; 551 				diag_io_vs_rq_c[LEFT_FRONT] = pMsgContext->reqData[0];
1603  02ab 7b000e            	stab	_diag_io_vs_rq_c
1604                         ; 552 				DataLength = 0;
1606  02ae 18790000          	clrw	L762_DataLength
1607                         ; 553 				Response = RESPONSE_OK;
1609  02b2 c601              	ldab	#1
1611  02b4 2016              	bra	L767
1612  02b6                   L357:
1613                         ; 559 				if ( (diag_io_vs_lock_flags_c[LEFT_FRONT].b.led_status_lock_b == TRUE) || (diag_io_all_vs_lock_b == TRUE) )
1615  02b6 1e00030205        	brset	_diag_io_vs_lock_flags_c,2,L167
1617  02bb 1f000a0102        	brclr	_diag_io_lock2_flags_c,1,L757
1618  02c0                   L167:
1619                         ; 562 					Response = CONDITIONS_NOT_CORRECT;
1622  02c0 2008              	bra	L747
1623  02c2                   L757:
1624                         ; 567 					Response = REQUEST_OUT_OF_RANGE;
1626  02c2 c604              	ldab	#4
1627  02c4 2006              	bra	L767
1628  02c6                   L157:
1629                         ; 574 			Response = SUBFUNCTION_NOT_SUPPORTED;
1631  02c6 c607              	ldab	#7
1632  02c8 2002              	bra	L767
1633  02ca                   L747:
1634                         ; 581 				Response = CONDITIONS_NOT_CORRECT;	// Response with conditions not correct
1636  02ca c606              	ldab	#6
1637  02cc                   L767:
1638  02cc 7b0002            	stab	L562_Response
1639                         ; 584 	 if(diag_io_active_b == TRUE)
1641  02cf 1f00090803        	brclr	_diag_io_lock3_flags_c,8,L377
1642                         ; 588 		mainF_CSWM_Status();
1644  02d4 160000            	jsr	_mainF_CSWM_Status
1647  02d7                   L377:
1648                         ; 594       diagF_DiagResponse(Response, DataLength/*+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext);	
1650  02d7 ec80              	ldd	OFST+0,s
1651  02d9 3b                	pshd	
1652  02da fc0000            	ldd	L762_DataLength
1653  02dd 3b                	pshd	
1654  02de f60002            	ldab	L562_Response
1655  02e1 87                	clra	
1656  02e2 4a000000          	call	f_diagF_DiagResponse
1658  02e6 1b86              	leas	6,s
1659                         ; 595 }
1662  02e8 0a                	rtc	
1712                         ; 600 void DESC_API_CALLBACK_TYPE ApplDescControl_2F500803_F_R_Vent_Seat_Request_BI(DescMsgContext* pMsgContext)
1712                         ; 601 {
1713                         	switch	.ftext
1714  02e9                   f_ApplDescControl_2F500803_F_R_Vent_Seat_Request_BI:
1716  02e9 3b                	pshd	
1717       00000000          OFST:	set	0
1720                         ; 604 	DataLength = 1;
1722  02ea cc0001            	ldd	#1
1723  02ed 7c0000            	std	L762_DataLength
1724                         ; 607 	if(diag_rc_all_op_lock_b == FALSE)
1726  02f0 1e0000014a        	brset	_diag_rc_lock_flags_c,1,L5101
1727                         ; 610 		if ((CSWM_config_c & CSWM_2VS_MASK_K) == CSWM_2VS_MASK_K)
1729  02f5 1e00003002        	brset	_CSWM_config_c,48,LC011
1730  02fa 203f              	bra	L7101
1731  02fc                   LC011:
1732                         ; 613 			if ( (diag_io_vs_lock_flags_c[RIGHT_FRONT].b.led_status_lock_b == FALSE) && 
1732                         ; 614 				 (diag_io_all_vs_lock_b == FALSE) && 
1732                         ; 615 				 (pMsgContext->reqData[0] <= VL_HI) )
1734  02fc 1e0004022a        	brset	_diag_io_vs_lock_flags_c+1,2,L1201
1736  0301 1e000a0125        	brset	_diag_io_lock2_flags_c,1,L1201
1738  0306 edf30000          	ldy	[OFST+0,s]
1739  030a e640              	ldab	0,y
1740  030c c103              	cmpb	#3
1741  030e 221b              	bhi	L1201
1742                         ; 618 				diag_io_vs_lock_flags_c[RIGHT_FRONT].b.switch_rq_lock_b = 1; // TRUE;
1744  0310 1c000401          	bset	_diag_io_vs_lock_flags_c+1,1
1745                         ; 622 				diag_io_active_b = 1; // TRUE;							
1747  0314 1c000908          	bset	_diag_io_lock3_flags_c,8
1748                         ; 625 				vs_Flag2_c[VS_FR].b.diag_clr_ontime_b = TRUE;
1750  0318 1c000108          	bset	_vs_Flag2_c+1,8
1751                         ; 627 				diag_io_hs_lock_flags_c[FRONT_RIGHT].b.switch_rq_lock_b = 0; // FALSE;
1753  031c 1d000601          	bclr	_diag_io_hs_lock_flags_c+1,1
1754                         ; 629 				diag_io_vs_rq_c[RIGHT_FRONT] = pMsgContext->reqData[0];
1756  0320 7b000f            	stab	_diag_io_vs_rq_c+1
1757                         ; 630 				DataLength = 0;
1759  0323 18790000          	clrw	L762_DataLength
1760                         ; 631 				Response = RESPONSE_OK;
1762  0327 c601              	ldab	#1
1764  0329 2016              	bra	L5301
1765  032b                   L1201:
1766                         ; 637 				if ( (diag_io_vs_lock_flags_c[RIGHT_FRONT].b.led_status_lock_b == TRUE) || (diag_io_all_vs_lock_b == TRUE) ) 
1768  032b 1e00040205        	brset	_diag_io_vs_lock_flags_c+1,2,L7201
1770  0330 1f000a0102        	brclr	_diag_io_lock2_flags_c,1,L5201
1771  0335                   L7201:
1772                         ; 640 					Response = CONDITIONS_NOT_CORRECT;
1775  0335 2008              	bra	L5101
1776  0337                   L5201:
1777                         ; 645 					Response = REQUEST_OUT_OF_RANGE;
1779  0337 c604              	ldab	#4
1780  0339 2006              	bra	L5301
1781  033b                   L7101:
1782                         ; 652 			Response = SUBFUNCTION_NOT_SUPPORTED;
1784  033b c607              	ldab	#7
1785  033d 2002              	bra	L5301
1786  033f                   L5101:
1787                         ; 659 		Response = CONDITIONS_NOT_CORRECT;	// Response with conditions not correct
1789  033f c606              	ldab	#6
1790  0341                   L5301:
1791  0341 7b0002            	stab	L562_Response
1792                         ; 662 	if(diag_io_active_b == TRUE)
1794  0344 1f00090803        	brclr	_diag_io_lock3_flags_c,8,L1401
1795                         ; 666 		mainF_CSWM_Status();
1797  0349 160000            	jsr	_mainF_CSWM_Status
1800  034c                   L1401:
1801                         ; 672     diagF_DiagResponse(Response, DataLength/*+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext);		
1803  034c ec80              	ldd	OFST+0,s
1804  034e 3b                	pshd	
1805  034f fc0000            	ldd	L762_DataLength
1806  0352 3b                	pshd	
1807  0353 f60002            	ldab	L562_Response
1808  0356 87                	clra	
1809  0357 4a000000          	call	f_diagF_DiagResponse
1811  035b 1b86              	leas	6,s
1812                         ; 673 }
1815  035d 0a                	rtc	
1864                         ; 678 void DESC_API_CALLBACK_TYPE ApplDescControl_2F500903_Heated_Steering_Wheel_Request_BI(DescMsgContext* pMsgContext)
1864                         ; 679 {
1865                         	switch	.ftext
1866  035e                   f_ApplDescControl_2F500903_Heated_Steering_Wheel_Request_BI:
1868  035e 3b                	pshd	
1869       00000000          OFST:	set	0
1872                         ; 681 	DataLength = 1;
1874  035f cc0001            	ldd	#1
1875  0362 7c0000            	std	L762_DataLength
1876                         ; 684 	if(diag_rc_all_op_lock_b == FALSE)
1878  0365 1e0000013d        	brset	_diag_rc_lock_flags_c,1,L3601
1879                         ; 686 		if ((CSWM_config_c & CSWM_HSW_MASK_K) == CSWM_HSW_MASK_K)
1881  036a 1f00004034        	brclr	_CSWM_config_c,64,L5601
1882                         ; 689 			if ( (!diag_io_st_whl_state_lock_b) && (pMsgContext->reqData[0] <= 4) )
1884  036f 1e00090421        	brset	_diag_io_lock3_flags_c,4,L7601
1886  0374 edf30000          	ldy	[OFST+0,s]
1887  0378 e640              	ldab	0,y
1888  037a c104              	cmpb	#4
1889  037c 2217              	bhi	L7601
1890                         ; 692 				diag_io_st_whl_rq_lock_b = 1; // TRUE;				
1892  037e 1c000b80          	bset	_diag_io_lock_flags_c,128
1893                         ; 695 				diag_io_active_b = 1; // TRUE;
1895  0382 1c000908          	bset	_diag_io_lock3_flags_c,8
1896                         ; 698 				stW_clr_ontime_b = TRUE;
1898  0386 1c000040          	bset	_stW_Flag1_c,64
1899                         ; 700 				diag_io_st_whl_rq_c = pMsgContext->reqData[0];
1901  038a 7b001b            	stab	_diag_io_st_whl_rq_c
1902                         ; 701 				DataLength = 0;
1904  038d 18790000          	clrw	L762_DataLength
1905                         ; 702 				Response = RESPONSE_OK;
1907  0391 c601              	ldab	#1
1909  0393 2014              	bra	L1011
1910  0395                   L7601:
1911                         ; 707 				if (pMsgContext->reqData[0] <= 4)
1913  0395 edf30000          	ldy	[OFST+0,s]
1914  0399 e640              	ldab	0,y
1915  039b c104              	cmpb	#4
1916                         ; 710 					Response = CONDITIONS_NOT_CORRECT;
1919  039d 2308              	bls	L3601
1920                         ; 715 					Response = REQUEST_OUT_OF_RANGE;
1922  039f c604              	ldab	#4
1923  03a1 2006              	bra	L1011
1924  03a3                   L5601:
1925                         ; 722 			Response = SUBFUNCTION_NOT_SUPPORTED;
1927  03a3 c607              	ldab	#7
1928  03a5 2002              	bra	L1011
1929  03a7                   L3601:
1930                         ; 729 		Response = CONDITIONS_NOT_CORRECT;	// Response with conditions not correct
1932  03a7 c606              	ldab	#6
1933  03a9                   L1011:
1934  03a9 7b0002            	stab	L562_Response
1935                         ; 732 	if(diag_io_active_b == TRUE)
1937  03ac 1f00090803        	brclr	_diag_io_lock3_flags_c,8,L5011
1938                         ; 736 		mainF_CSWM_Status();
1940  03b1 160000            	jsr	_mainF_CSWM_Status
1943  03b4                   L5011:
1944                         ; 742     diagF_DiagResponse(Response, DataLength/*+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext);
1946  03b4 ec80              	ldd	OFST+0,s
1947  03b6 3b                	pshd	
1948  03b7 fc0000            	ldd	L762_DataLength
1949  03ba 3b                	pshd	
1950  03bb f60002            	ldab	L562_Response
1951  03be 87                	clra	
1952  03bf 4a000000          	call	f_diagF_DiagResponse
1954  03c3 1b86              	leas	6,s
1955                         ; 743 }
1958  03c5 0a                	rtc	
2004                         ; 748 void DESC_API_CALLBACK_TYPE ApplDescControl_2F500A03_F_L_Heat_Seat_Status_BO(DescMsgContext* pMsgContext)
2004                         ; 749 {
2005                         	switch	.ftext
2006  03c6                   f_ApplDescControl_2F500A03_F_L_Heat_Seat_Status_BO:
2008  03c6 3b                	pshd	
2009       00000000          OFST:	set	0
2012                         ; 751 	DataLength = 1;
2014  03c7 cc0001            	ldd	#1
2015  03ca 7c0000            	std	L762_DataLength
2016                         ; 754 	if(diag_rc_all_op_lock_b == FALSE)
2018  03cd 1e0000013d        	brset	_diag_rc_lock_flags_c,1,L7311
2019                         ; 757 		if ((!diag_io_hs_lock_flags_c[FRONT_LEFT].b.switch_rq_lock_b) && (!diag_io_all_hs_lock_b) && ((pMsgContext->reqData[0] <= 1)||(pMsgContext->reqData[0] == 3)))
2021  03d2 1e00050126        	brset	_diag_io_hs_lock_flags_c,1,L1311
2023  03d7 1e000a1021        	brset	_diag_io_lock2_flags_c,16,L1311
2025  03dc edf30000          	ldy	[OFST+0,s]
2026  03e0 e640              	ldab	0,y
2027  03e2 c101              	cmpb	#1
2028  03e4 2304              	bls	L3311
2030  03e6 c103              	cmpb	#3
2031  03e8 2613              	bne	L1311
2032  03ea                   L3311:
2033                         ; 760 			diag_io_hs_lock_flags_c[FRONT_LEFT].b.led_status_lock_b = 1; // TRUE;
2035  03ea 1c000502          	bset	_diag_io_hs_lock_flags_c,2
2036                         ; 762 			diag_io_vs_lock_flags_c[LEFT_FRONT].b.led_status_lock_b = 0; // FALSE;
2038  03ee 1d000302          	bclr	_diag_io_vs_lock_flags_c,2
2039                         ; 764 			diag_io_hs_state_c[FRONT_LEFT] = pMsgContext->reqData[0];
2041  03f2 7b0010            	stab	_diag_io_hs_state_c
2042                         ; 765 			DataLength = 0;
2044  03f5 18790000          	clrw	L762_DataLength
2045                         ; 766 			Response = RESPONSE_OK;
2047  03f9 c601              	ldab	#1
2049  03fb 2014              	bra	L5411
2050  03fd                   L1311:
2051                         ; 772 			if ((pMsgContext->reqData[0] > 3) || (pMsgContext->reqData[0] == 2) )
2053  03fd edf30000          	ldy	[OFST+0,s]
2054  0401 e640              	ldab	0,y
2055  0403 c103              	cmpb	#3
2056  0405 2204              	bhi	L1411
2058  0407 c102              	cmpb	#2
2059  0409 2604              	bne	L7311
2060  040b                   L1411:
2061                         ; 775 				Response = REQUEST_OUT_OF_RANGE;
2063  040b c604              	ldab	#4
2065  040d 2002              	bra	L5411
2066  040f                   L7311:
2067                         ; 780 				Response = CONDITIONS_NOT_CORRECT;
2069                         ; 788 		Response = CONDITIONS_NOT_CORRECT;	// Response with conditions not correct
2071  040f c606              	ldab	#6
2072  0411                   L5411:
2073  0411 7b0002            	stab	L562_Response
2074                         ; 791 	diagF_DiagResponse(Response, DataLength/*+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext);
2076  0414 ec80              	ldd	OFST+0,s
2077  0416 3b                	pshd	
2078  0417 fc0000            	ldd	L762_DataLength
2079  041a 3b                	pshd	
2080  041b f60002            	ldab	L562_Response
2081  041e 87                	clra	
2082  041f 4a000000          	call	f_diagF_DiagResponse
2084  0423 1b86              	leas	6,s
2085                         ; 792 }
2088  0425 0a                	rtc	
2134                         ; 797 void DESC_API_CALLBACK_TYPE ApplDescControl_2F500B03_F_R_Heat_Seat_Status_BO(DescMsgContext* pMsgContext)
2134                         ; 798 {
2135                         	switch	.ftext
2136  0426                   f_ApplDescControl_2F500B03_F_R_Heat_Seat_Status_BO:
2138  0426 3b                	pshd	
2139       00000000          OFST:	set	0
2142                         ; 800 	DataLength = 1;
2144  0427 cc0001            	ldd	#1
2145  042a 7c0000            	std	L762_DataLength
2146                         ; 803 	if(diag_rc_all_op_lock_b == FALSE)
2148  042d 1e0000013d        	brset	_diag_rc_lock_flags_c,1,L7711
2149                         ; 805         if ((!diag_io_hs_lock_flags_c[FRONT_RIGHT].b.switch_rq_lock_b) && (!diag_io_all_hs_lock_b)&& ((pMsgContext->reqData[0] <= 1)||(pMsgContext->reqData[0] == 3)))
2151  0432 1e00060126        	brset	_diag_io_hs_lock_flags_c+1,1,L1711
2153  0437 1e000a1021        	brset	_diag_io_lock2_flags_c,16,L1711
2155  043c edf30000          	ldy	[OFST+0,s]
2156  0440 e640              	ldab	0,y
2157  0442 c101              	cmpb	#1
2158  0444 2304              	bls	L3711
2160  0446 c103              	cmpb	#3
2161  0448 2613              	bne	L1711
2162  044a                   L3711:
2163                         ; 808 			diag_io_hs_lock_flags_c[FRONT_RIGHT].b.led_status_lock_b = 1; // TRUE;
2165  044a 1c000602          	bset	_diag_io_hs_lock_flags_c+1,2
2166                         ; 810 			diag_io_vs_lock_flags_c[RIGHT_FRONT].b.led_status_lock_b = 0; // FALSE;
2168  044e 1d000402          	bclr	_diag_io_vs_lock_flags_c+1,2
2169                         ; 812 			diag_io_hs_state_c[FRONT_RIGHT] = pMsgContext->reqData[0];
2171  0452 7b0011            	stab	_diag_io_hs_state_c+1
2172                         ; 814 			DataLength = 0;
2174  0455 18790000          	clrw	L762_DataLength
2175                         ; 815 			Response = RESPONSE_OK;
2177  0459 c601              	ldab	#1
2179  045b 2014              	bra	L5021
2180  045d                   L1711:
2181                         ; 820 			if ((pMsgContext->reqData[0] > 3) || (pMsgContext->reqData[0] == 2) )
2183  045d edf30000          	ldy	[OFST+0,s]
2184  0461 e640              	ldab	0,y
2185  0463 c103              	cmpb	#3
2186  0465 2204              	bhi	L1021
2188  0467 c102              	cmpb	#2
2189  0469 2604              	bne	L7711
2190  046b                   L1021:
2191                         ; 823 				Response = REQUEST_OUT_OF_RANGE;
2193  046b c604              	ldab	#4
2195  046d 2002              	bra	L5021
2196  046f                   L7711:
2197                         ; 828 				Response = CONDITIONS_NOT_CORRECT;
2199                         ; 836 		Response = CONDITIONS_NOT_CORRECT;	// Response with conditions not correct
2201  046f c606              	ldab	#6
2202  0471                   L5021:
2203  0471 7b0002            	stab	L562_Response
2204                         ; 839 	diagF_DiagResponse(Response, DataLength/*+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext);	
2206  0474 ec80              	ldd	OFST+0,s
2207  0476 3b                	pshd	
2208  0477 fc0000            	ldd	L762_DataLength
2209  047a 3b                	pshd	
2210  047b f60002            	ldab	L562_Response
2211  047e 87                	clra	
2212  047f 4a000000          	call	f_diagF_DiagResponse
2214  0483 1b86              	leas	6,s
2215                         ; 840 }
2218  0485 0a                	rtc	
2264                         ; 845 void DESC_API_CALLBACK_TYPE ApplDescControl_2F500C03_R_L_Heat_Seat_Status_BO(DescMsgContext* pMsgContext)
2264                         ; 846 {
2265                         	switch	.ftext
2266  0486                   f_ApplDescControl_2F500C03_R_L_Heat_Seat_Status_BO:
2268  0486 3b                	pshd	
2269       00000000          OFST:	set	0
2272                         ; 848 	DataLength = 1;
2274  0487 cc0001            	ldd	#1
2275  048a 7c0000            	std	L762_DataLength
2276                         ; 851 	if(diag_rc_all_op_lock_b == FALSE)
2278  048d 1e00000144        	brset	_diag_rc_lock_flags_c,1,L7221
2279                         ; 855 		if ((CSWM_config_c & CSWM_4HS_VARIANT_MASK_K) == CSWM_4HS_VARIANT_MASK_K)
2281  0492 1e00000f02        	brset	_CSWM_config_c,15,LC016
2282  0497 2039              	bra	L1321
2283  0499                   LC016:
2284                         ; 858 			if ((!diag_io_hs_lock_flags_c[REAR_LEFT].b.switch_rq_lock_b) && (!diag_io_all_hs_lock_b) && ((pMsgContext->reqData[0] <= 1)||(pMsgContext->reqData[0] == 3)))
2286  0499 1e00070122        	brset	_diag_io_hs_lock_flags_c+2,1,L3321
2288  049e 1e000a101d        	brset	_diag_io_lock2_flags_c,16,L3321
2290  04a3 edf30000          	ldy	[OFST+0,s]
2291  04a7 e640              	ldab	0,y
2292  04a9 c101              	cmpb	#1
2293  04ab 2304              	bls	L5321
2295  04ad c103              	cmpb	#3
2296  04af 260f              	bne	L3321
2297  04b1                   L5321:
2298                         ; 861 				diag_io_hs_lock_flags_c[REAR_LEFT].b.led_status_lock_b = 1; // TRUE;
2300  04b1 1c000702          	bset	_diag_io_hs_lock_flags_c+2,2
2301                         ; 863 				diag_io_hs_state_c[REAR_LEFT] = pMsgContext->reqData[0];
2303  04b5 7b0012            	stab	_diag_io_hs_state_c+2
2304                         ; 864 				DataLength = 0;
2306  04b8 18790000          	clrw	L762_DataLength
2307                         ; 865 				Response = RESPONSE_OK;
2309  04bc c601              	ldab	#1
2311  04be 201b              	bra	L1521
2312  04c0                   L3321:
2313                         ; 870 				if ((pMsgContext->reqData[0] > 3) || (pMsgContext->reqData[0] == 2) )
2315  04c0 edf30000          	ldy	[OFST+0,s]
2316  04c4 e640              	ldab	0,y
2317  04c6 c103              	cmpb	#3
2318  04c8 2204              	bhi	L3421
2320  04ca c102              	cmpb	#2
2321  04cc 260b              	bne	LC017
2322  04ce                   L3421:
2323                         ; 873 					Response = REQUEST_OUT_OF_RANGE;
2325  04ce c604              	ldab	#4
2327  04d0 2009              	bra	L1521
2328                         ; 878 					Response = CONDITIONS_NOT_CORRECT;
2330  04d2                   L1321:
2331                         ; 885 			Response = SUBFUNCTION_NOT_SUPPORTED;
2333  04d2 c607              	ldab	#7
2334  04d4 2005              	bra	L1521
2335  04d6                   L7221:
2336                         ; 891 		DataLength = 1;						// Set data lenght to 1
2338  04d6 7c0000            	std	L762_DataLength
2339                         ; 892 		Response = CONDITIONS_NOT_CORRECT;	// Response with conditions not correct
2341  04d9                   LC017:
2342  04d9 c606              	ldab	#6
2343  04db                   L1521:
2344  04db 7b0002            	stab	L562_Response
2345                         ; 895 	diagF_DiagResponse(Response, DataLength/*+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext);	
2347  04de ec80              	ldd	OFST+0,s
2348  04e0 3b                	pshd	
2349  04e1 fc0000            	ldd	L762_DataLength
2350  04e4 3b                	pshd	
2351  04e5 f60002            	ldab	L562_Response
2352  04e8 87                	clra	
2353  04e9 4a000000          	call	f_diagF_DiagResponse
2355  04ed 1b86              	leas	6,s
2356                         ; 896 }
2359  04ef 0a                	rtc	
2405                         ; 901 void DESC_API_CALLBACK_TYPE ApplDescControl_2F500D03_R_R_Heat_Seat_Status_BO(DescMsgContext* pMsgContext)
2405                         ; 902 {
2406                         	switch	.ftext
2407  04f0                   f_ApplDescControl_2F500D03_R_R_Heat_Seat_Status_BO:
2409  04f0 3b                	pshd	
2410       00000000          OFST:	set	0
2413                         ; 904 	DataLength = 1;
2415  04f1 cc0001            	ldd	#1
2416  04f4 7c0000            	std	L762_DataLength
2417                         ; 907 	if(diag_rc_all_op_lock_b == FALSE)
2419  04f7 1e00000144        	brset	_diag_rc_lock_flags_c,1,L3721
2420                         ; 911 		if ((CSWM_config_c & CSWM_4HS_VARIANT_MASK_K) == CSWM_4HS_VARIANT_MASK_K)
2422  04fc 1e00000f02        	brset	_CSWM_config_c,15,LC018
2423  0501 2039              	bra	L5721
2424  0503                   LC018:
2425                         ; 914 			if ((!diag_io_hs_lock_flags_c[REAR_RIGHT].b.switch_rq_lock_b) && (!diag_io_all_hs_lock_b) && ((pMsgContext->reqData[0] <= 1)||(pMsgContext->reqData[0] == 3)))
2427  0503 1e00080122        	brset	_diag_io_hs_lock_flags_c+3,1,L7721
2429  0508 1e000a101d        	brset	_diag_io_lock2_flags_c,16,L7721
2431  050d edf30000          	ldy	[OFST+0,s]
2432  0511 e640              	ldab	0,y
2433  0513 c101              	cmpb	#1
2434  0515 2304              	bls	L1031
2436  0517 c103              	cmpb	#3
2437  0519 260f              	bne	L7721
2438  051b                   L1031:
2439                         ; 917 				diag_io_hs_lock_flags_c[REAR_RIGHT].b.led_status_lock_b = 1; // TRUE;
2441  051b 1c000802          	bset	_diag_io_hs_lock_flags_c+3,2
2442                         ; 919 				diag_io_hs_state_c[REAR_RIGHT] = pMsgContext->reqData[0];
2444  051f 7b0013            	stab	_diag_io_hs_state_c+3
2445                         ; 920 				DataLength = 0;
2447  0522 18790000          	clrw	L762_DataLength
2448                         ; 921 				Response = RESPONSE_OK;
2450  0526 c601              	ldab	#1
2452  0528 2018              	bra	L5131
2453  052a                   L7721:
2454                         ; 926 				if ((pMsgContext->reqData[0] > 3) || (pMsgContext->reqData[0] == 2) )
2456  052a edf30000          	ldy	[OFST+0,s]
2457  052e e640              	ldab	0,y
2458  0530 c103              	cmpb	#3
2459  0532 2204              	bhi	L7031
2461  0534 c102              	cmpb	#2
2462  0536 2608              	bne	L3721
2463  0538                   L7031:
2464                         ; 929 					Response = REQUEST_OUT_OF_RANGE;
2466  0538 c604              	ldab	#4
2468  053a 2006              	bra	L5131
2469                         ; 934 					Response = CONDITIONS_NOT_CORRECT;
2471  053c                   L5721:
2472                         ; 941 			Response = SUBFUNCTION_NOT_SUPPORTED;
2474  053c c607              	ldab	#7
2475  053e 2002              	bra	L5131
2476  0540                   L3721:
2477                         ; 948 		Response = CONDITIONS_NOT_CORRECT;	// Response with conditions not correct
2479  0540 c606              	ldab	#6
2480  0542                   L5131:
2481  0542 7b0002            	stab	L562_Response
2482                         ; 951 	diagF_DiagResponse(Response, DataLength/*+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext);	
2484  0545 ec80              	ldd	OFST+0,s
2485  0547 3b                	pshd	
2486  0548 fc0000            	ldd	L762_DataLength
2487  054b 3b                	pshd	
2488  054c f60002            	ldab	L562_Response
2489  054f 87                	clra	
2490  0550 4a000000          	call	f_diagF_DiagResponse
2492  0554 1b86              	leas	6,s
2493                         ; 952 }
2496  0556 0a                	rtc	
2543                         ; 957 void DESC_API_CALLBACK_TYPE ApplDescControl_2F500E03_F_L_Vent_Seat_Status_BO(DescMsgContext* pMsgContext)
2543                         ; 958 {
2544                         	switch	.ftext
2545  0557                   f_ApplDescControl_2F500E03_F_L_Vent_Seat_Status_BO:
2547  0557 3b                	pshd	
2548       00000000          OFST:	set	0
2551                         ; 960 	DataLength = 1;
2553  0558 cc0001            	ldd	#1
2554  055b 7c0000            	std	L762_DataLength
2555                         ; 963 	if(diag_rc_all_op_lock_b == FALSE)
2557  055e 1e00000148        	brset	_diag_rc_lock_flags_c,1,L7331
2558                         ; 966 		if ((CSWM_config_c & CSWM_2VS_MASK_K) == CSWM_2VS_MASK_K)
2560  0563 1e00003002        	brset	_CSWM_config_c,48,LC020
2561  0568 203d              	bra	L1431
2562  056a                   LC020:
2563                         ; 969 			if ( (diag_io_vs_lock_flags_c[LEFT_FRONT].b.switch_rq_lock_b == FALSE) && (diag_io_all_vs_lock_b == FALSE) && ( (pMsgContext->reqData[0] == VL0) || (pMsgContext->reqData[0] == VL_LOW) || (pMsgContext->reqData[0] == VL_HI) ) )
2565  056a 1e00030128        	brset	_diag_io_vs_lock_flags_c,1,L3431
2567  056f 1e000a0123        	brset	_diag_io_lock2_flags_c,1,L3431
2569  0574 edf30000          	ldy	[OFST+0,s]
2570  0578 e640              	ldab	0,y
2571  057a 2708              	beq	L5431
2573  057c c101              	cmpb	#1
2574  057e 2704              	beq	L5431
2576  0580 c103              	cmpb	#3
2577  0582 2613              	bne	L3431
2578  0584                   L5431:
2579                         ; 972 				diag_io_vs_lock_flags_c[LEFT_FRONT].b.led_status_lock_b = 1; // TRUE;
2581  0584 1c000302          	bset	_diag_io_vs_lock_flags_c,2
2582                         ; 974 				diag_io_hs_lock_flags_c[FRONT_LEFT].b.led_status_lock_b = 0; // FALSE;
2584  0588 1d000502          	bclr	_diag_io_hs_lock_flags_c,2
2585                         ; 976 				diag_io_vs_state_c[LEFT_FRONT] = pMsgContext->reqData[0];
2587  058c 7b000c            	stab	_diag_io_vs_state_c
2588                         ; 977 				DataLength = 0;
2590  058f 18790000          	clrw	L762_DataLength
2591                         ; 978 				Response = RESPONSE_OK;
2593  0593 c601              	ldab	#1
2595  0595 2016              	bra	L3631
2596  0597                   L3431:
2597                         ; 984 				if ( (diag_io_vs_lock_flags_c[LEFT_FRONT].b.switch_rq_lock_b == TRUE) || (diag_io_all_vs_lock_b == TRUE) )
2599  0597 1e00030105        	brset	_diag_io_vs_lock_flags_c,1,L5531
2601  059c 1f000a0102        	brclr	_diag_io_lock2_flags_c,1,L3531
2602  05a1                   L5531:
2603                         ; 987 					Response = CONDITIONS_NOT_CORRECT;
2606  05a1 2008              	bra	L7331
2607  05a3                   L3531:
2608                         ; 992 					Response = REQUEST_OUT_OF_RANGE;
2610  05a3 c604              	ldab	#4
2611  05a5 2006              	bra	L3631
2612  05a7                   L1431:
2613                         ; 999 			Response = SUBFUNCTION_NOT_SUPPORTED;
2615  05a7 c607              	ldab	#7
2616  05a9 2002              	bra	L3631
2617  05ab                   L7331:
2618                         ; 1006 		Response = CONDITIONS_NOT_CORRECT;	// Response with conditions not correct
2620  05ab c606              	ldab	#6
2621  05ad                   L3631:
2622  05ad 7b0002            	stab	L562_Response
2623                         ; 1009 	diagF_DiagResponse(Response, DataLength/*+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext);	
2625  05b0 ec80              	ldd	OFST+0,s
2626  05b2 3b                	pshd	
2627  05b3 fc0000            	ldd	L762_DataLength
2628  05b6 3b                	pshd	
2629  05b7 f60002            	ldab	L562_Response
2630  05ba 87                	clra	
2631  05bb 4a000000          	call	f_diagF_DiagResponse
2633  05bf 1b86              	leas	6,s
2634                         ; 1010 }
2637  05c1 0a                	rtc	
2684                         ; 1015 void DESC_API_CALLBACK_TYPE ApplDescControl_2F500F03_F_R_Vent_Seat_Status_BO(DescMsgContext* pMsgContext)
2684                         ; 1016 {
2685                         	switch	.ftext
2686  05c2                   f_ApplDescControl_2F500F03_F_R_Vent_Seat_Status_BO:
2688  05c2 3b                	pshd	
2689       00000000          OFST:	set	0
2692                         ; 1018 	DataLength = 1;
2694  05c3 cc0001            	ldd	#1
2695  05c6 7c0000            	std	L762_DataLength
2696                         ; 1021 	if(diag_rc_all_op_lock_b == FALSE)
2698  05c9 1e00000148        	brset	_diag_rc_lock_flags_c,1,L5041
2699                         ; 1024 		if ((CSWM_config_c & CSWM_2VS_MASK_K) == CSWM_2VS_MASK_K) 
2701  05ce 1e00003002        	brset	_CSWM_config_c,48,LC022
2702  05d3 203d              	bra	L7041
2703  05d5                   LC022:
2704                         ; 1027 			if ( (diag_io_vs_lock_flags_c[RIGHT_FRONT].b.switch_rq_lock_b == FALSE) && (diag_io_all_vs_lock_b == FALSE) && ( (pMsgContext->reqData[0] == VL0) || (pMsgContext->reqData[0] == VL_LOW) || (pMsgContext->reqData[0] ==VL_HI) ) )
2706  05d5 1e00040128        	brset	_diag_io_vs_lock_flags_c+1,1,L1141
2708  05da 1e000a0123        	brset	_diag_io_lock2_flags_c,1,L1141
2710  05df edf30000          	ldy	[OFST+0,s]
2711  05e3 e640              	ldab	0,y
2712  05e5 2708              	beq	L3141
2714  05e7 c101              	cmpb	#1
2715  05e9 2704              	beq	L3141
2717  05eb c103              	cmpb	#3
2718  05ed 2613              	bne	L1141
2719  05ef                   L3141:
2720                         ; 1030 				diag_io_vs_lock_flags_c[RIGHT_FRONT].b.led_status_lock_b = 1; // TRUE;
2722  05ef 1c000402          	bset	_diag_io_vs_lock_flags_c+1,2
2723                         ; 1032 				diag_io_hs_lock_flags_c[FRONT_RIGHT].b.led_status_lock_b = 0; // FALSE;
2725  05f3 1d000602          	bclr	_diag_io_hs_lock_flags_c+1,2
2726                         ; 1034 				diag_io_vs_state_c[RIGHT_FRONT] = pMsgContext->reqData[0];
2728  05f7 7b000d            	stab	_diag_io_vs_state_c+1
2729                         ; 1035 				DataLength = 0;
2731  05fa 18790000          	clrw	L762_DataLength
2732                         ; 1036 				Response = RESPONSE_OK;
2734  05fe c601              	ldab	#1
2736  0600 2016              	bra	L1341
2737  0602                   L1141:
2738                         ; 1042 				if ( (diag_io_vs_lock_flags_c[RIGHT_FRONT].b.switch_rq_lock_b == TRUE) || (diag_io_all_vs_lock_b == TRUE) )
2740  0602 1e00040105        	brset	_diag_io_vs_lock_flags_c+1,1,L3241
2742  0607 1f000a0102        	brclr	_diag_io_lock2_flags_c,1,L1241
2743  060c                   L3241:
2744                         ; 1045 					Response = CONDITIONS_NOT_CORRECT;
2747  060c 2008              	bra	L5041
2748  060e                   L1241:
2749                         ; 1050 					Response = REQUEST_OUT_OF_RANGE;
2751  060e c604              	ldab	#4
2752  0610 2006              	bra	L1341
2753  0612                   L7041:
2754                         ; 1057 			Response = SUBFUNCTION_NOT_SUPPORTED;
2756  0612 c607              	ldab	#7
2757  0614 2002              	bra	L1341
2758  0616                   L5041:
2759                         ; 1064 		Response = CONDITIONS_NOT_CORRECT;	// Response with conditions not correct
2761  0616 c606              	ldab	#6
2762  0618                   L1341:
2763  0618 7b0002            	stab	L562_Response
2764                         ; 1067 	diagF_DiagResponse(Response, DataLength/*+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext);	
2766  061b ec80              	ldd	OFST+0,s
2767  061d 3b                	pshd	
2768  061e fc0000            	ldd	L762_DataLength
2769  0621 3b                	pshd	
2770  0622 f60002            	ldab	L562_Response
2771  0625 87                	clra	
2772  0626 4a000000          	call	f_diagF_DiagResponse
2774  062a 1b86              	leas	6,s
2775                         ; 1068 }
2778  062c 0a                	rtc	
2825                         ; 1073 void DESC_API_CALLBACK_TYPE ApplDescControl_2F501003_Heated_Steering_Wheel_Status_BO(DescMsgContext* pMsgContext)
2825                         ; 1074 {
2826                         	switch	.ftext
2827  062d                   f_ApplDescControl_2F501003_Heated_Steering_Wheel_Status_BO:
2829  062d 3b                	pshd	
2830       00000000          OFST:	set	0
2833                         ; 1076 	DataLength = 1;
2835  062e cc0001            	ldd	#1
2836  0631 7c0000            	std	L762_DataLength
2837                         ; 1079 	if(diag_rc_all_op_lock_b == FALSE)
2839  0634 1e00000135        	brset	_diag_rc_lock_flags_c,1,L3541
2840                         ; 1081 		if ((CSWM_config_c & CSWM_HSW_MASK_K) == CSWM_HSW_MASK_K)
2842  0639 1f0000402c        	brclr	_CSWM_config_c,64,L5541
2843                         ; 1084 			if ( (!diag_io_st_whl_rq_lock_b) && (pMsgContext->reqData[0] <= 4) )
2845  063e 1e000b8019        	brset	_diag_io_lock_flags_c,128,L7541
2847  0643 edf30000          	ldy	[OFST+0,s]
2848  0647 e640              	ldab	0,y
2849  0649 c104              	cmpb	#4
2850  064b 220f              	bhi	L7541
2851                         ; 1087 				diag_io_st_whl_state_lock_b = 1; // TRUE;
2853  064d 1c000904          	bset	_diag_io_lock3_flags_c,4
2854                         ; 1089 				diag_io_st_whl_state_c = pMsgContext->reqData[0];
2856  0651 7b001a            	stab	_diag_io_st_whl_state_c
2857                         ; 1090 				DataLength = 0;
2859  0654 18790000          	clrw	L762_DataLength
2860                         ; 1091 				Response = RESPONSE_OK;
2862  0658 c601              	ldab	#1
2864  065a 2014              	bra	L1741
2865  065c                   L7541:
2866                         ; 1096 				if (pMsgContext->reqData[0] <= 3)
2868  065c edf30000          	ldy	[OFST+0,s]
2869  0660 e640              	ldab	0,y
2870  0662 c103              	cmpb	#3
2871                         ; 1099 					Response = CONDITIONS_NOT_CORRECT;
2874  0664 2308              	bls	L3541
2875                         ; 1104 					Response = REQUEST_OUT_OF_RANGE;
2877  0666 c604              	ldab	#4
2878  0668 2006              	bra	L1741
2879  066a                   L5541:
2880                         ; 1111 			Response = SUBFUNCTION_NOT_SUPPORTED;
2882  066a c607              	ldab	#7
2883  066c 2002              	bra	L1741
2884  066e                   L3541:
2885                         ; 1118 		Response = CONDITIONS_NOT_CORRECT;	// Response with conditions not correct
2887  066e c606              	ldab	#6
2888  0670                   L1741:
2889  0670 7b0002            	stab	L562_Response
2890                         ; 1121 	diagF_DiagResponse(Response, DataLength/*+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext);	
2892  0673 ec80              	ldd	OFST+0,s
2893  0675 3b                	pshd	
2894  0676 fc0000            	ldd	L762_DataLength
2895  0679 3b                	pshd	
2896  067a f60002            	ldab	L562_Response
2897  067d 87                	clra	
2898  067e 4a000000          	call	f_diagF_DiagResponse
2900  0682 1b86              	leas	6,s
2901                         ; 1122 }
2904  0684 0a                	rtc	
2948                         ; 1127 void DESC_API_CALLBACK_TYPE ApplDescControl_2F501103_Load_shedding_status_BI(DescMsgContext* pMsgContext)
2948                         ; 1128 {
2949                         	switch	.ftext
2950  0685                   f_ApplDescControl_2F501103_Load_shedding_status_BI:
2952  0685 3b                	pshd	
2953       00000000          OFST:	set	0
2956                         ; 1129 	if (pMsgContext->reqData[0] <= 4)
2958  0686 b746              	tfr	d,y
2959  0688 e6eb0000          	ldab	[0,y]
2960  068c c104              	cmpb	#4
2961  068e 2218              	bhi	L3151
2962                         ; 1132 		diag_io_load_shed_lock_b = 1; // TRUE;
2964  0690 1c000b01          	bset	_diag_io_lock_flags_c,1
2965                         ; 1134 		diag_io_load_shed_c = pMsgContext->reqData[0];
2967  0694 edf30000          	ldy	[OFST+0,s]
2968  0698 e640              	ldab	0,y
2969  069a 7b001e            	stab	_diag_io_load_shed_c
2970                         ; 1136 		canio_LOAD_SHED_c = diag_io_load_shed_c;
2972  069d 7b0000            	stab	_canio_LOAD_SHED_c
2973                         ; 1137 		DataLength = 0;
2975  06a0 18790000          	clrw	L762_DataLength
2976                         ; 1138 		Response = RESPONSE_OK;
2978  06a4 c601              	ldab	#1
2980  06a6 2008              	bra	L5151
2981  06a8                   L3151:
2982                         ; 1142 		DataLength = 1;
2984  06a8 cc0001            	ldd	#1
2985  06ab 7c0000            	std	L762_DataLength
2986                         ; 1144 		Response = REQUEST_OUT_OF_RANGE;
2988  06ae c604              	ldab	#4
2989  06b0                   L5151:
2990  06b0 7b0002            	stab	L562_Response
2991                         ; 1147 	diagF_DiagResponse(Response, DataLength/*+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext);	
2993  06b3 ec80              	ldd	OFST+0,s
2994  06b5 3b                	pshd	
2995  06b6 fc0000            	ldd	L762_DataLength
2996  06b9 3b                	pshd	
2997  06ba f60002            	ldab	L562_Response
2998  06bd 87                	clra	
2999  06be 4a000000          	call	f_diagF_DiagResponse
3001  06c2 1b86              	leas	6,s
3002                         ; 1148 }
3005  06c4 0a                	rtc	
3051                         ; 1153 void DESC_API_CALLBACK_TYPE ApplDescControl_2F501203_Ignition_Status(DescMsgContext* pMsgContext)
3051                         ; 1154 {
3052                         	switch	.ftext
3053  06c5                   f_ApplDescControl_2F501203_Ignition_Status:
3055  06c5 3b                	pshd	
3056       00000000          OFST:	set	0
3059                         ; 1155     if (pMsgContext->reqData[0] <= 6)
3061  06c6 b746              	tfr	d,y
3062  06c8 e6eb0000          	ldab	[0,y]
3063  06cc c106              	cmpb	#6
3064  06ce 2218              	bhi	L7351
3065                         ; 1158 		diag_io_ign_lock_b = 1; // TRUE;
3067  06d0 1c000b04          	bset	_diag_io_lock_flags_c,4
3068                         ; 1160 		diag_io_ign_state_c = pMsgContext->reqData[0];
3070  06d4 edf30000          	ldy	[OFST+0,s]
3071  06d8 e640              	ldab	0,y
3072  06da 7b001d            	stab	_diag_io_ign_state_c
3073                         ; 1162 		canio_RX_IGN_STATUS_c = diag_io_ign_state_c;
3075  06dd 7b0000            	stab	_canio_RX_IGN_STATUS_c
3076                         ; 1163 		DataLength = 0;
3078  06e0 18790000          	clrw	L762_DataLength
3079                         ; 1164 		Response = RESPONSE_OK;
3081  06e4 c601              	ldab	#1
3083  06e6 2008              	bra	L1451
3084  06e8                   L7351:
3085                         ; 1168 		DataLength = 1;
3087  06e8 cc0001            	ldd	#1
3088  06eb 7c0000            	std	L762_DataLength
3089                         ; 1170 		Response = REQUEST_OUT_OF_RANGE;
3091  06ee c604              	ldab	#4
3092  06f0                   L1451:
3093  06f0 7b0002            	stab	L562_Response
3094                         ; 1173 	if(diag_io_active_b == TRUE)
3096  06f3 1f00090803        	brclr	_diag_io_lock3_flags_c,8,L5451
3097                         ; 1177 		mainF_CSWM_Status();
3099  06f8 160000            	jsr	_mainF_CSWM_Status
3102  06fb                   L5451:
3103                         ; 1183     diagF_DiagResponse(Response, DataLength/*+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext);	
3105  06fb ec80              	ldd	OFST+0,s
3106  06fd 3b                	pshd	
3107  06fe fc0000            	ldd	L762_DataLength
3108  0701 3b                	pshd	
3109  0702 f60002            	ldab	L562_Response
3110  0705 87                	clra	
3111  0706 4a000000          	call	f_diagF_DiagResponse
3113  070a 1b86              	leas	6,s
3114                         ; 1184 }
3117  070c 0a                	rtc	
3159                         ; 1191 void DESC_API_CALLBACK_TYPE ApplDescControl_2F501303_Flash_Program_Data_1(DescMsgContext* pMsgContext)
3159                         ; 1192 {
3160                         	switch	.ftext
3161  070d                   f_ApplDescControl_2F501303_Flash_Program_Data_1:
3163  070d 3b                	pshd	
3164       00000000          OFST:	set	0
3167                         ; 1193 	if (pMsgContext->reqData[0] == 1)
3169  070e b746              	tfr	d,y
3170  0710 e6eb0000          	ldab	[0,y]
3171  0714 042106            	dbne	b,L7651
3172                         ; 1196 		diag_eol_io_lock_b = TRUE;
3174  0717 1c000901          	bset	_diag_io_lock3_flags_c,1
3176  071b 2004              	bra	L1751
3177  071d                   L7651:
3178                         ; 1201 		diag_eol_io_lock_b = FALSE;
3180  071d 1d000901          	bclr	_diag_io_lock3_flags_c,1
3181  0721                   L1751:
3182                         ; 1204 	DataLength = 0;
3184  0721 18790000          	clrw	L762_DataLength
3185                         ; 1205 	Response = RESPONSE_OK;
3187  0725 c601              	ldab	#1
3188  0727 7b0002            	stab	L562_Response
3189                         ; 1207     diagF_DiagResponse(Response, DataLength/*+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext);	
3191  072a ec80              	ldd	OFST+0,s
3192  072c 3b                	pshd	
3193  072d 87                	clra	
3194  072e c7                	clrb	
3195  072f 3b                	pshd	
3196  0730 52                	incb	
3197  0731 4a000000          	call	f_diagF_DiagResponse
3199  0735 1b86              	leas	6,s
3200                         ; 1209 }
3203  0737 0a                	rtc	
3251                         ; 1215 void DESC_API_CALLBACK_TYPE ApplDescControl_2F501403_Flash_Program_Data_2(DescMsgContext* pMsgContext)
3251                         ; 1216 {
3252                         	switch	.ftext
3253  0738                   f_ApplDescControl_2F501403_Flash_Program_Data_2:
3255  0738 3b                	pshd	
3256       00000000          OFST:	set	0
3259                         ; 1217 	if (diag_eol_io_lock_b)
3261  0739 1f00090147        	brclr	_diag_io_lock3_flags_c,1,L3161
3262                         ; 1219 		if (pMsgContext->reqData[0] == 1)
3264  073e edf30000          	ldy	[OFST+0,s]
3265  0742 e640              	ldab	0,y
3266  0744 04211d            	dbne	b,L5161
3267                         ; 1222 			diag_eol_no_fault_detection_b = TRUE;
3269  0747 1c000902          	bset	_diag_io_lock3_flags_c,2
3270                         ; 1225 			Pwm_SetDutyCycle(PWM_BASE_REL, 0x8000);
3272  074b cc8000            	ldd	#-32768
3273  074e 3b                	pshd	
3274  074f cc0002            	ldd	#2
3275  0752 4a000000          	call	f_Pwm_SetDutyCycle
3277  0756 1b82              	leas	2,s
3278                         ; 1228 			Dio_WriteChannel(REL_A_EN, STD_LOW);
3281  0758 1410              	sei	
3286  075a fe0014            	ldx	_Dio_PortWrite_Ptr+20
3287  075d 0d0002            	bclr	0,x,2
3291  0760 10ef              	cli	
3295  0762 2019              	bra	L7161
3296  0764                   L5161:
3297                         ; 1234 			diag_eol_no_fault_detection_b = FALSE;
3299  0764 1d000902          	bclr	_diag_io_lock3_flags_c,2
3300                         ; 1237 			Dio_WriteChannel(REL_A_EN, STD_HIGH);
3303  0768 1410              	sei	
3308  076a fe0014            	ldx	_Dio_PortWrite_Ptr+20
3309  076d 0c0002            	bset	0,x,2
3313  0770 10ef              	cli	
3315                         ; 1240 			Pwm_SetDutyCycle(PWM_BASE_REL, 0x0000);
3318  0772 87                	clra	
3319  0773 c7                	clrb	
3320  0774 3b                	pshd	
3321  0775 c602              	ldab	#2
3322  0777 4a000000          	call	f_Pwm_SetDutyCycle
3324  077b 1b82              	leas	2,s
3325  077d                   L7161:
3326                         ; 1243 		DataLength = 0;
3328  077d 18790000          	clrw	L762_DataLength
3329                         ; 1244 		Response = RESPONSE_OK;
3331  0781 c601              	ldab	#1
3333  0783 2008              	bra	L1261
3334  0785                   L3161:
3335                         ; 1248 		DataLength = 1;
3337  0785 cc0001            	ldd	#1
3338  0788 7c0000            	std	L762_DataLength
3339                         ; 1249 		Response = CONDITIONS_NOT_CORRECT;
3341  078b c606              	ldab	#6
3342  078d                   L1261:
3343  078d 7b0002            	stab	L562_Response
3344                         ; 1252     diagF_DiagResponse(Response, DataLength/*+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext);	
3346  0790 ec80              	ldd	OFST+0,s
3347  0792 3b                	pshd	
3348  0793 fc0000            	ldd	L762_DataLength
3349  0796 3b                	pshd	
3350  0797 f60002            	ldab	L562_Response
3351  079a 87                	clra	
3352  079b 4a000000          	call	f_diagF_DiagResponse
3354  079f 1b86              	leas	6,s
3355                         ; 1254 }
3358  07a1 0a                	rtc	
3406                         ; 1260 void DESC_API_CALLBACK_TYPE ApplDescControl_2F501503_Flash_Program_Data_3(DescMsgContext* pMsgContext)
3406                         ; 1261 {
3407                         	switch	.ftext
3408  07a2                   f_ApplDescControl_2F501503_Flash_Program_Data_3:
3410  07a2 3b                	pshd	
3411       00000000          OFST:	set	0
3414                         ; 1262 	if (diag_eol_io_lock_b) {
3416  07a3 1f00090147        	brclr	_diag_io_lock3_flags_c,1,L3461
3417                         ; 1263 		if (pMsgContext->reqData[0] == 1)
3419  07a8 edf30000          	ldy	[OFST+0,s]
3420  07ac e640              	ldab	0,y
3421  07ae 04211d            	dbne	b,L5461
3422                         ; 1266 			diag_eol_no_fault_detection_b = TRUE;
3424  07b1 1c000902          	bset	_diag_io_lock3_flags_c,2
3425                         ; 1269 			Pwm_SetDutyCycle(PWM_BASE_REL, 0x8000);
3427  07b5 cc8000            	ldd	#-32768
3428  07b8 3b                	pshd	
3429  07b9 cc0002            	ldd	#2
3430  07bc 4a000000          	call	f_Pwm_SetDutyCycle
3432  07c0 1b82              	leas	2,s
3433                         ; 1272 			Dio_WriteChannel(REL_B_EN, STD_LOW);
3436  07c2 1410              	sei	
3441  07c4 fe0014            	ldx	_Dio_PortWrite_Ptr+20
3442  07c7 0d0001            	bclr	0,x,1
3446  07ca 10ef              	cli	
3450  07cc 2019              	bra	L7461
3451  07ce                   L5461:
3452                         ; 1278 			diag_eol_no_fault_detection_b = FALSE;
3454  07ce 1d000902          	bclr	_diag_io_lock3_flags_c,2
3455                         ; 1281 			Dio_WriteChannel(REL_B_EN, STD_HIGH);
3458  07d2 1410              	sei	
3463  07d4 fe0014            	ldx	_Dio_PortWrite_Ptr+20
3464  07d7 0c0001            	bset	0,x,1
3468  07da 10ef              	cli	
3470                         ; 1284 			Pwm_SetDutyCycle(PWM_BASE_REL, 0x0000);
3473  07dc 87                	clra	
3474  07dd c7                	clrb	
3475  07de 3b                	pshd	
3476  07df c602              	ldab	#2
3477  07e1 4a000000          	call	f_Pwm_SetDutyCycle
3479  07e5 1b82              	leas	2,s
3480  07e7                   L7461:
3481                         ; 1287 		DataLength = 0;
3483  07e7 18790000          	clrw	L762_DataLength
3484                         ; 1288 		Response = RESPONSE_OK;
3486  07eb c601              	ldab	#1
3488  07ed 2008              	bra	L1561
3489  07ef                   L3461:
3490                         ; 1292 		DataLength = 1;
3492  07ef cc0001            	ldd	#1
3493  07f2 7c0000            	std	L762_DataLength
3494                         ; 1293 		Response = CONDITIONS_NOT_CORRECT;
3496  07f5 c606              	ldab	#6
3497  07f7                   L1561:
3498  07f7 7b0002            	stab	L562_Response
3499                         ; 1295     diagF_DiagResponse(Response, DataLength/*+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext);	
3501  07fa ec80              	ldd	OFST+0,s
3502  07fc 3b                	pshd	
3503  07fd fc0000            	ldd	L762_DataLength
3504  0800 3b                	pshd	
3505  0801 f60002            	ldab	L562_Response
3506  0804 87                	clra	
3507  0805 4a000000          	call	f_diagF_DiagResponse
3509  0809 1b86              	leas	6,s
3510                         ; 1297 }
3513  080b 0a                	rtc	
3561                         ; 1303 void DESC_API_CALLBACK_TYPE ApplDescControl_2F501603_Flash_Program_Data_4(DescMsgContext* pMsgContext)
3561                         ; 1304 {
3562                         	switch	.ftext
3563  080c                   f_ApplDescControl_2F501603_Flash_Program_Data_4:
3565  080c 3b                	pshd	
3566       00000000          OFST:	set	0
3569                         ; 1305 	if (diag_eol_io_lock_b)
3571  080d 1f00090147        	brclr	_diag_io_lock3_flags_c,1,L3761
3572                         ; 1307 		if (pMsgContext->reqData[0] == 1)
3574  0812 edf30000          	ldy	[OFST+0,s]
3575  0816 e640              	ldab	0,y
3576  0818 04211d            	dbne	b,L5761
3577                         ; 1310 			diag_eol_no_fault_detection_b = TRUE;
3579  081b 1c000902          	bset	_diag_io_lock3_flags_c,2
3580                         ; 1313 			Pwm_SetDutyCycle(PWM_BASE_REL, 0x8000);
3582  081f cc8000            	ldd	#-32768
3583  0822 3b                	pshd	
3584  0823 cc0002            	ldd	#2
3585  0826 4a000000          	call	f_Pwm_SetDutyCycle
3587  082a 1b82              	leas	2,s
3588                         ; 1316 			Dio_WriteChannel(REL_STW_EN, STD_LOW);
3591  082c 1410              	sei	
3596  082e fe0008            	ldx	_Dio_PortWrite_Ptr+8
3597  0831 0d0008            	bclr	0,x,8
3601  0834 10ef              	cli	
3605  0836 2019              	bra	L7761
3606  0838                   L5761:
3607                         ; 1321 			diag_eol_no_fault_detection_b = FALSE;
3609  0838 1d000902          	bclr	_diag_io_lock3_flags_c,2
3610                         ; 1324 			Dio_WriteChannel(REL_STW_EN, STD_HIGH);
3613  083c 1410              	sei	
3618  083e fe0008            	ldx	_Dio_PortWrite_Ptr+8
3619  0841 0c0008            	bset	0,x,8
3623  0844 10ef              	cli	
3625                         ; 1327 			Pwm_SetDutyCycle(PWM_BASE_REL, 0x0000);
3628  0846 87                	clra	
3629  0847 c7                	clrb	
3630  0848 3b                	pshd	
3631  0849 c602              	ldab	#2
3632  084b 4a000000          	call	f_Pwm_SetDutyCycle
3634  084f 1b82              	leas	2,s
3635  0851                   L7761:
3636                         ; 1329 		DataLength = 0;
3638  0851 18790000          	clrw	L762_DataLength
3639                         ; 1330 		Response = RESPONSE_OK;
3641  0855 c601              	ldab	#1
3643  0857 2008              	bra	L1071
3644  0859                   L3761:
3645                         ; 1334 		DataLength = 1;
3647  0859 cc0001            	ldd	#1
3648  085c 7c0000            	std	L762_DataLength
3649                         ; 1335 		Response = CONDITIONS_NOT_CORRECT;
3651  085f c606              	ldab	#6
3652  0861                   L1071:
3653  0861 7b0002            	stab	L562_Response
3654                         ; 1337     diagF_DiagResponse(Response, DataLength/*+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext);	
3656  0864 ec80              	ldd	OFST+0,s
3657  0866 3b                	pshd	
3658  0867 fc0000            	ldd	L762_DataLength
3659  086a 3b                	pshd	
3660  086b f60002            	ldab	L562_Response
3661  086e 87                	clra	
3662  086f 4a000000          	call	f_diagF_DiagResponse
3664  0873 1b86              	leas	6,s
3665                         ; 1339 }
3668  0875 0a                	rtc	
3711                         ; 1345 void DESC_API_CALLBACK_TYPE ApplDescReturnControl_2F500103_Actuate_all_heaters(DescMsgContext* pMsgContext)
3711                         ; 1346 {
3712                         	switch	.ftext
3713  0876                   f_ApplDescReturnControl_2F500103_Actuate_all_heaters:
3715  0876 3b                	pshd	
3716       00000000          OFST:	set	0
3719                         ; 1348 	diag_io_all_hs_lock_b = 0; //FALSE;
3721  0877 1d000a10          	bclr	_diag_io_lock2_flags_c,16
3722                         ; 1350 	diag_io_active_b = 0; //FALSE;
3724  087b 1d000908          	bclr	_diag_io_lock3_flags_c,8
3725                         ; 1352 	diag_io_hs_rq_c[FRONT_LEFT] = 0;
3727                         ; 1353 	diag_io_hs_rq_c[FRONT_RIGHT] = 0;
3729  087f 87                	clra	
3730  0880 c7                	clrb	
3731  0881 7c0014            	std	_diag_io_hs_rq_c
3732                         ; 1354 	diag_io_hs_rq_c[REAR_LEFT] = 0;
3734                         ; 1355 	diag_io_hs_rq_c[REAR_RIGHT] = 0;
3736  0884 7c0016            	std	_diag_io_hs_rq_c+2
3737                         ; 1357 	Response = RESPONSE_OK;
3739  0887 52                	incb	
3740  0888 7b0002            	stab	L562_Response
3741                         ; 1358 	diagF_DiagResponse(Response, 0/*DataLength+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext); 
3743  088b ec80              	ldd	OFST+0,s
3744  088d 3b                	pshd	
3745  088e 87                	clra	
3746  088f c7                	clrb	
3747  0890 3b                	pshd	
3748  0891 52                	incb	
3749  0892 4a000000          	call	f_diagF_DiagResponse
3751  0896 1b86              	leas	6,s
3752                         ; 1359 }
3755  0898 0a                	rtc	
3798                         ; 1364 void DESC_API_CALLBACK_TYPE ApplDescReturnControl_2F500203_Actuate_all_Vents(DescMsgContext* pMsgContext)
3798                         ; 1365 {
3799                         	switch	.ftext
3800  0899                   f_ApplDescReturnControl_2F500203_Actuate_all_Vents:
3802  0899 3b                	pshd	
3803       00000000          OFST:	set	0
3806                         ; 1367 	diag_io_all_vs_lock_b = 0; // FALSE;
3808  089a 1d000a01          	bclr	_diag_io_lock2_flags_c,1
3809                         ; 1368 	diag_io_vs_rq_c[LEFT_FRONT] = 0;	// added by can
3811                         ; 1369 	diag_io_vs_rq_c[RIGHT_FRONT] = 0;	// added by can				
3813  089e 87                	clra	
3814  089f c7                	clrb	
3815  08a0 7c000e            	std	_diag_io_vs_rq_c
3816                         ; 1371 	diag_io_active_b = 0; // FALSE;
3818  08a3 1d000908          	bclr	_diag_io_lock3_flags_c,8
3819                         ; 1373 	Response = RESPONSE_OK;
3821  08a7 52                	incb	
3822  08a8 7b0002            	stab	L562_Response
3823                         ; 1374 	diagF_DiagResponse(Response, 0/*DataLength+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext); 				
3825  08ab ec80              	ldd	OFST+0,s
3826  08ad 3b                	pshd	
3827  08ae 87                	clra	
3828  08af c7                	clrb	
3829  08b0 3b                	pshd	
3830  08b1 52                	incb	
3831  08b2 4a000000          	call	f_diagF_DiagResponse
3833  08b6 1b86              	leas	6,s
3834                         ; 1375 }
3837  08b8 0a                	rtc	
3881                         ; 1380 void DESC_API_CALLBACK_TYPE ApplDescReturnControl_2F500303_F_L_Heat_Seat_Request_BI(DescMsgContext* pMsgContext)
3881                         ; 1381 {
3882                         	switch	.ftext
3883  08b9                   f_ApplDescReturnControl_2F500303_F_L_Heat_Seat_Request_BI:
3885  08b9 3b                	pshd	
3886       00000000          OFST:	set	0
3889                         ; 1383 	diag_io_hs_lock_flags_c[FRONT_LEFT].b.switch_rq_lock_b = 0; // FALSE;					
3891  08ba 1d000501          	bclr	_diag_io_hs_lock_flags_c,1
3892                         ; 1385 	diag_io_active_b = 0; // FALSE;
3894  08be 1d000908          	bclr	_diag_io_lock3_flags_c,8
3895                         ; 1387 	diag_io_hs_rq_c[FRONT_LEFT] = 0;
3897  08c2 790014            	clr	_diag_io_hs_rq_c
3898                         ; 1389 	Response = RESPONSE_OK;
3900  08c5 c601              	ldab	#1
3901  08c7 7b0002            	stab	L562_Response
3902                         ; 1390     diagF_DiagResponse(Response, 0/*DataLength*/, pMsgContext); 					
3904  08ca ec80              	ldd	OFST+0,s
3905  08cc 3b                	pshd	
3906  08cd 87                	clra	
3907  08ce c7                	clrb	
3908  08cf 3b                	pshd	
3909  08d0 52                	incb	
3910  08d1 4a000000          	call	f_diagF_DiagResponse
3912  08d5 1b86              	leas	6,s
3913                         ; 1391 }
3916  08d7 0a                	rtc	
3960                         ; 1396 void DESC_API_CALLBACK_TYPE ApplDescReturnControl_2F500403_F_R_Heat_Seat_Request_BI(DescMsgContext* pMsgContext)
3960                         ; 1397 {				
3961                         	switch	.ftext
3962  08d8                   f_ApplDescReturnControl_2F500403_F_R_Heat_Seat_Request_BI:
3964  08d8 3b                	pshd	
3965       00000000          OFST:	set	0
3968                         ; 1399 	diag_io_hs_lock_flags_c[FRONT_RIGHT].b.switch_rq_lock_b = 0; // FALSE;
3970  08d9 1d000601          	bclr	_diag_io_hs_lock_flags_c+1,1
3971                         ; 1401 	diag_io_active_b = 0; // FALSE;
3973  08dd 1d000908          	bclr	_diag_io_lock3_flags_c,8
3974                         ; 1403 	diag_io_hs_rq_c[FRONT_RIGHT] = 0;
3976  08e1 790015            	clr	_diag_io_hs_rq_c+1
3977                         ; 1405 	Response = RESPONSE_OK;
3979  08e4 c601              	ldab	#1
3980  08e6 7b0002            	stab	L562_Response
3981                         ; 1406 	diagF_DiagResponse(Response, 0/*DataLength+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext); 					
3983  08e9 ec80              	ldd	OFST+0,s
3984  08eb 3b                	pshd	
3985  08ec 87                	clra	
3986  08ed c7                	clrb	
3987  08ee 3b                	pshd	
3988  08ef 52                	incb	
3989  08f0 4a000000          	call	f_diagF_DiagResponse
3991  08f4 1b86              	leas	6,s
3992                         ; 1407 }
3995  08f6 0a                	rtc	
4039                         ; 1412 void DESC_API_CALLBACK_TYPE ApplDescReturnControl_2F500503_R_L_Heat_Seat_Request_BI(DescMsgContext* pMsgContext)
4039                         ; 1413 {			
4040                         	switch	.ftext
4041  08f7                   f_ApplDescReturnControl_2F500503_R_L_Heat_Seat_Request_BI:
4043  08f7 3b                	pshd	
4044       00000000          OFST:	set	0
4047                         ; 1415 	diag_io_hs_lock_flags_c[REAR_LEFT].b.switch_rq_lock_b = 0; // FALSE;
4049  08f8 1d000701          	bclr	_diag_io_hs_lock_flags_c+2,1
4050                         ; 1417 	diag_io_active_b = 0; // FALSE;
4052  08fc 1d000908          	bclr	_diag_io_lock3_flags_c,8
4053                         ; 1419 	diag_io_hs_rq_c[REAR_LEFT] = 0;
4055  0900 790016            	clr	_diag_io_hs_rq_c+2
4056                         ; 1421 	Response = RESPONSE_OK;
4058  0903 c601              	ldab	#1
4059  0905 7b0002            	stab	L562_Response
4060                         ; 1422 	diagF_DiagResponse(Response, 0/*DataLength+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext); 			
4062  0908 ec80              	ldd	OFST+0,s
4063  090a 3b                	pshd	
4064  090b 87                	clra	
4065  090c c7                	clrb	
4066  090d 3b                	pshd	
4067  090e 52                	incb	
4068  090f 4a000000          	call	f_diagF_DiagResponse
4070  0913 1b86              	leas	6,s
4071                         ; 1423 }
4074  0915 0a                	rtc	
4118                         ; 1428 void DESC_API_CALLBACK_TYPE ApplDescReturnControl_2F500603_R_R_Heat_Seat_Request_BI(DescMsgContext* pMsgContext)
4118                         ; 1429 {					
4119                         	switch	.ftext
4120  0916                   f_ApplDescReturnControl_2F500603_R_R_Heat_Seat_Request_BI:
4122  0916 3b                	pshd	
4123       00000000          OFST:	set	0
4126                         ; 1431 	diag_io_hs_lock_flags_c[REAR_RIGHT].b.switch_rq_lock_b = 0; // FALSE;
4128  0917 1d000801          	bclr	_diag_io_hs_lock_flags_c+3,1
4129                         ; 1433 	diag_io_active_b = 0; // FALSE;
4131  091b 1d000908          	bclr	_diag_io_lock3_flags_c,8
4132                         ; 1435 	diag_io_hs_rq_c[REAR_RIGHT] = 0;
4134  091f 790017            	clr	_diag_io_hs_rq_c+3
4135                         ; 1437 	Response = RESPONSE_OK;
4137  0922 c601              	ldab	#1
4138  0924 7b0002            	stab	L562_Response
4139                         ; 1438 	diagF_DiagResponse(Response, 0/*DataLength+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext); 					
4141  0927 ec80              	ldd	OFST+0,s
4142  0929 3b                	pshd	
4143  092a 87                	clra	
4144  092b c7                	clrb	
4145  092c 3b                	pshd	
4146  092d 52                	incb	
4147  092e 4a000000          	call	f_diagF_DiagResponse
4149  0932 1b86              	leas	6,s
4150                         ; 1439 }
4153  0934 0a                	rtc	
4197                         ; 1444 void DESC_API_CALLBACK_TYPE ApplDescReturnControl_2F500703_F_L_Vent_Seat_Request_BI(DescMsgContext* pMsgContext)
4197                         ; 1445 {
4198                         	switch	.ftext
4199  0935                   f_ApplDescReturnControl_2F500703_F_L_Vent_Seat_Request_BI:
4201  0935 3b                	pshd	
4202       00000000          OFST:	set	0
4205                         ; 1447 	diag_io_vs_lock_flags_c[LEFT_FRONT].b.switch_rq_lock_b = 0; // FALSE;
4207  0936 1d000301          	bclr	_diag_io_vs_lock_flags_c,1
4208                         ; 1448 	diag_io_vs_rq_c[LEFT_FRONT] = 0;	//added by can 11.05.2008
4210  093a 79000e            	clr	_diag_io_vs_rq_c
4211                         ; 1450 	diag_io_active_b = 0; // FALSE;
4213  093d 1d000908          	bclr	_diag_io_lock3_flags_c,8
4214                         ; 1452 	Response = RESPONSE_OK;
4216  0941 c601              	ldab	#1
4217  0943 7b0002            	stab	L562_Response
4218                         ; 1453 	diagF_DiagResponse(Response, 0/*DataLength+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext); 			
4220  0946 ec80              	ldd	OFST+0,s
4221  0948 3b                	pshd	
4222  0949 87                	clra	
4223  094a c7                	clrb	
4224  094b 3b                	pshd	
4225  094c 52                	incb	
4226  094d 4a000000          	call	f_diagF_DiagResponse
4228  0951 1b86              	leas	6,s
4229                         ; 1454 }
4232  0953 0a                	rtc	
4276                         ; 1459 void DESC_API_CALLBACK_TYPE ApplDescReturnControl_2F500803_F_R_Vent_Seat_Request_BI(DescMsgContext* pMsgContext)
4276                         ; 1460 {
4277                         	switch	.ftext
4278  0954                   f_ApplDescReturnControl_2F500803_F_R_Vent_Seat_Request_BI:
4280  0954 3b                	pshd	
4281       00000000          OFST:	set	0
4284                         ; 1462 	diag_io_vs_lock_flags_c[RIGHT_FRONT].b.switch_rq_lock_b = 0; // FALSE;
4286  0955 1d000401          	bclr	_diag_io_vs_lock_flags_c+1,1
4287                         ; 1463 	diag_io_vs_rq_c[RIGHT_FRONT] = 0;	//added by can 11.05.2008
4289  0959 79000f            	clr	_diag_io_vs_rq_c+1
4290                         ; 1465 	diag_io_active_b = 0; // FALSE;
4292  095c 1d000908          	bclr	_diag_io_lock3_flags_c,8
4293                         ; 1467 	Response = RESPONSE_OK;
4295  0960 c601              	ldab	#1
4296  0962 7b0002            	stab	L562_Response
4297                         ; 1468 	diagF_DiagResponse(Response, 0/*DataLength+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext); 					
4299  0965 ec80              	ldd	OFST+0,s
4300  0967 3b                	pshd	
4301  0968 87                	clra	
4302  0969 c7                	clrb	
4303  096a 3b                	pshd	
4304  096b 52                	incb	
4305  096c 4a000000          	call	f_diagF_DiagResponse
4307  0970 1b86              	leas	6,s
4308                         ; 1469 }
4311  0972 0a                	rtc	
4355                         ; 1474 void DESC_API_CALLBACK_TYPE ApplDescReturnControl_2F500903_Heated_Steering_Wheel_Request_BI(DescMsgContext* pMsgContext)
4355                         ; 1475 {
4356                         	switch	.ftext
4357  0973                   f_ApplDescReturnControl_2F500903_Heated_Steering_Wheel_Request_BI:
4359  0973 3b                	pshd	
4360       00000000          OFST:	set	0
4363                         ; 1477 	diag_io_st_whl_rq_lock_b = 0; // FALSE;
4365  0974 1d000b80          	bclr	_diag_io_lock_flags_c,128
4366                         ; 1478 	diag_io_st_whl_rq_c = 0; // added by Can 11.06.2008
4368  0978 79001b            	clr	_diag_io_st_whl_rq_c
4369                         ; 1480 	diag_io_active_b = 0; // FALSE;
4371  097b 1d000908          	bclr	_diag_io_lock3_flags_c,8
4372                         ; 1482 	Response = RESPONSE_OK;
4374  097f c601              	ldab	#1
4375  0981 7b0002            	stab	L562_Response
4376                         ; 1483 	diagF_DiagResponse(Response, 0/*DataLength+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext); 					
4378  0984 ec80              	ldd	OFST+0,s
4379  0986 3b                	pshd	
4380  0987 87                	clra	
4381  0988 c7                	clrb	
4382  0989 3b                	pshd	
4383  098a 52                	incb	
4384  098b 4a000000          	call	f_diagF_DiagResponse
4386  098f 1b86              	leas	6,s
4387                         ; 1484 }
4390  0991 0a                	rtc	
4431                         ; 1489 void DESC_API_CALLBACK_TYPE ApplDescReturnControl_2F500A03_F_L_Heat_Seat_Status_BO(DescMsgContext* pMsgContext)
4431                         ; 1490 {
4432                         	switch	.ftext
4433  0992                   f_ApplDescReturnControl_2F500A03_F_L_Heat_Seat_Status_BO:
4435  0992 3b                	pshd	
4436       00000000          OFST:	set	0
4439                         ; 1492 	diag_io_hs_lock_flags_c[FRONT_LEFT].b.led_status_lock_b = 0; // FALSE;			
4441  0993 1d000502          	bclr	_diag_io_hs_lock_flags_c,2
4442                         ; 1494 	Response = RESPONSE_OK;
4444  0997 c601              	ldab	#1
4445  0999 7b0002            	stab	L562_Response
4446                         ; 1495 	diagF_DiagResponse(Response, /*DataLength+SIZE_OF_BLOCKNUMBER_K*/0, pMsgContext); 		          
4448  099c ec80              	ldd	OFST+0,s
4449  099e 3b                	pshd	
4450  099f 87                	clra	
4451  09a0 c7                	clrb	
4452  09a1 3b                	pshd	
4453  09a2 52                	incb	
4454  09a3 4a000000          	call	f_diagF_DiagResponse
4456  09a7 1b86              	leas	6,s
4457                         ; 1496 }
4460  09a9 0a                	rtc	
4501                         ; 1501 void DESC_API_CALLBACK_TYPE ApplDescReturnControl_2F500B03_F_R_Heat_Seat_Status_BO(DescMsgContext* pMsgContext)
4501                         ; 1502 {
4502                         	switch	.ftext
4503  09aa                   f_ApplDescReturnControl_2F500B03_F_R_Heat_Seat_Status_BO:
4505  09aa 3b                	pshd	
4506       00000000          OFST:	set	0
4509                         ; 1504 	diag_io_hs_lock_flags_c[FRONT_RIGHT].b.led_status_lock_b = 0; // FALSE;
4511  09ab 1d000602          	bclr	_diag_io_hs_lock_flags_c+1,2
4512                         ; 1506 	Response = RESPONSE_OK;
4514  09af c601              	ldab	#1
4515  09b1 7b0002            	stab	L562_Response
4516                         ; 1507 	diagF_DiagResponse(Response, 0/*DataLength+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext); 
4518  09b4 ec80              	ldd	OFST+0,s
4519  09b6 3b                	pshd	
4520  09b7 87                	clra	
4521  09b8 c7                	clrb	
4522  09b9 3b                	pshd	
4523  09ba 52                	incb	
4524  09bb 4a000000          	call	f_diagF_DiagResponse
4526  09bf 1b86              	leas	6,s
4527                         ; 1508 }
4530  09c1 0a                	rtc	
4571                         ; 1513 void DESC_API_CALLBACK_TYPE ApplDescReturnControl_2F500C03_R_L_Heat_Seat_Status_BO(DescMsgContext* pMsgContext)
4571                         ; 1514 {
4572                         	switch	.ftext
4573  09c2                   f_ApplDescReturnControl_2F500C03_R_L_Heat_Seat_Status_BO:
4575  09c2 3b                	pshd	
4576       00000000          OFST:	set	0
4579                         ; 1516 	diag_io_hs_lock_flags_c[REAR_LEFT].b.led_status_lock_b = 0; // FALSE;
4581  09c3 1d000702          	bclr	_diag_io_hs_lock_flags_c+2,2
4582                         ; 1518 	Response = RESPONSE_OK;
4584  09c7 c601              	ldab	#1
4585  09c9 7b0002            	stab	L562_Response
4586                         ; 1519 	diagF_DiagResponse(Response, 0/*DataLength+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext); 
4588  09cc ec80              	ldd	OFST+0,s
4589  09ce 3b                	pshd	
4590  09cf 87                	clra	
4591  09d0 c7                	clrb	
4592  09d1 3b                	pshd	
4593  09d2 52                	incb	
4594  09d3 4a000000          	call	f_diagF_DiagResponse
4596  09d7 1b86              	leas	6,s
4597                         ; 1520 }
4600  09d9 0a                	rtc	
4641                         ; 1525 void DESC_API_CALLBACK_TYPE ApplDescReturnControl_2F500D03_R_R_Heat_Seat_Status_BO(DescMsgContext* pMsgContext)
4641                         ; 1526 {
4642                         	switch	.ftext
4643  09da                   f_ApplDescReturnControl_2F500D03_R_R_Heat_Seat_Status_BO:
4645  09da 3b                	pshd	
4646       00000000          OFST:	set	0
4649                         ; 1528 	diag_io_hs_lock_flags_c[REAR_RIGHT].b.led_status_lock_b = 0; // FALSE;
4651  09db 1d000802          	bclr	_diag_io_hs_lock_flags_c+3,2
4652                         ; 1530 	Response = RESPONSE_OK;
4654  09df c601              	ldab	#1
4655  09e1 7b0002            	stab	L562_Response
4656                         ; 1531 	diagF_DiagResponse(Response, 0/*DataLength+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext); 
4658  09e4 ec80              	ldd	OFST+0,s
4659  09e6 3b                	pshd	
4660  09e7 87                	clra	
4661  09e8 c7                	clrb	
4662  09e9 3b                	pshd	
4663  09ea 52                	incb	
4664  09eb 4a000000          	call	f_diagF_DiagResponse
4666  09ef 1b86              	leas	6,s
4667                         ; 1532 }
4670  09f1 0a                	rtc	
4712                         ; 1537 void DESC_API_CALLBACK_TYPE ApplDescReturnControl_2F500E03_F_L_Vent_Seat_Status_BO(DescMsgContext* pMsgContext)
4712                         ; 1538 {
4713                         	switch	.ftext
4714  09f2                   f_ApplDescReturnControl_2F500E03_F_L_Vent_Seat_Status_BO:
4716  09f2 3b                	pshd	
4717       00000000          OFST:	set	0
4720                         ; 1540 	diag_io_vs_lock_flags_c[LEFT_FRONT].b.led_status_lock_b = 0; // FALSE;
4722  09f3 1d000302          	bclr	_diag_io_vs_lock_flags_c,2
4723                         ; 1541 	diag_io_vs_state_c[LEFT_FRONT] = 0;	/// added by Can 11.06.2008
4725  09f7 79000c            	clr	_diag_io_vs_state_c
4726                         ; 1543 	Response = RESPONSE_OK;
4728  09fa c601              	ldab	#1
4729  09fc 7b0002            	stab	L562_Response
4730                         ; 1544 	diagF_DiagResponse(Response, 0/*DataLength+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext); 
4732  09ff ec80              	ldd	OFST+0,s
4733  0a01 3b                	pshd	
4734  0a02 87                	clra	
4735  0a03 c7                	clrb	
4736  0a04 3b                	pshd	
4737  0a05 52                	incb	
4738  0a06 4a000000          	call	f_diagF_DiagResponse
4740  0a0a 1b86              	leas	6,s
4741                         ; 1545 }					
4744  0a0c 0a                	rtc	
4786                         ; 1550 void DESC_API_CALLBACK_TYPE ApplDescReturnControl_2F500F03_F_R_Vent_Seat_Status_BO(DescMsgContext* pMsgContext)
4786                         ; 1551 {
4787                         	switch	.ftext
4788  0a0d                   f_ApplDescReturnControl_2F500F03_F_R_Vent_Seat_Status_BO:
4790  0a0d 3b                	pshd	
4791       00000000          OFST:	set	0
4794                         ; 1553 	diag_io_vs_lock_flags_c[RIGHT_FRONT].b.led_status_lock_b = 0; // FALSE;
4796  0a0e 1d000402          	bclr	_diag_io_vs_lock_flags_c+1,2
4797                         ; 1554 	diag_io_vs_state_c[RIGHT_FRONT] = 0; // added by Can 11.06.2008
4799  0a12 79000d            	clr	_diag_io_vs_state_c+1
4800                         ; 1556 	Response = RESPONSE_OK;
4802  0a15 c601              	ldab	#1
4803  0a17 7b0002            	stab	L562_Response
4804                         ; 1557 	diagF_DiagResponse(Response, 0/*DataLength+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext); 
4806  0a1a ec80              	ldd	OFST+0,s
4807  0a1c 3b                	pshd	
4808  0a1d 87                	clra	
4809  0a1e c7                	clrb	
4810  0a1f 3b                	pshd	
4811  0a20 52                	incb	
4812  0a21 4a000000          	call	f_diagF_DiagResponse
4814  0a25 1b86              	leas	6,s
4815                         ; 1558 }
4818  0a27 0a                	rtc	
4861                         ; 1563 void DESC_API_CALLBACK_TYPE ApplDescReturnControl_2F501003_Heated_Steering_Wheel_Status_BO(DescMsgContext* pMsgContext)
4861                         ; 1564 {
4862                         	switch	.ftext
4863  0a28                   f_ApplDescReturnControl_2F501003_Heated_Steering_Wheel_Status_BO:
4865  0a28 3b                	pshd	
4866       00000000          OFST:	set	0
4869                         ; 1566 	diag_io_st_whl_state_lock_b = 0; // FALSE;
4871  0a29 1d000904          	bclr	_diag_io_lock3_flags_c,4
4872                         ; 1567 	diag_io_st_whl_state_c = 0; //added by Can 11.06.2008
4874  0a2d 79001a            	clr	_diag_io_st_whl_state_c
4875                         ; 1569 	Response = RESPONSE_OK;
4877  0a30 c601              	ldab	#1
4878  0a32 7b0002            	stab	L562_Response
4879                         ; 1570 	diagF_DiagResponse(Response, 0/*DataLength+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext); 
4881  0a35 ec80              	ldd	OFST+0,s
4882  0a37 3b                	pshd	
4883  0a38 87                	clra	
4884  0a39 c7                	clrb	
4885  0a3a 3b                	pshd	
4886  0a3b 52                	incb	
4887  0a3c 4a000000          	call	f_diagF_DiagResponse
4889  0a40 1b86              	leas	6,s
4890                         ; 1571 }
4893  0a42 0a                	rtc	
4934                         ; 1576 void DESC_API_CALLBACK_TYPE ApplDescReturnControl_2F501103_Load_shedding_status_BI(DescMsgContext* pMsgContext)
4934                         ; 1577 {
4935                         	switch	.ftext
4936  0a43                   f_ApplDescReturnControl_2F501103_Load_shedding_status_BI:
4938  0a43 3b                	pshd	
4939       00000000          OFST:	set	0
4942                         ; 1579 	diag_io_load_shed_lock_b = 0; // FALSE:
4944  0a44 1d000b01          	bclr	_diag_io_lock_flags_c,1
4945                         ; 1581 	Response = RESPONSE_OK;
4947  0a48 c601              	ldab	#1
4948  0a4a 7b0002            	stab	L562_Response
4949                         ; 1582 	diagF_DiagResponse(Response, 0/*DataLength+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext); 
4951  0a4d ec80              	ldd	OFST+0,s
4952  0a4f 3b                	pshd	
4953  0a50 87                	clra	
4954  0a51 c7                	clrb	
4955  0a52 3b                	pshd	
4956  0a53 52                	incb	
4957  0a54 4a000000          	call	f_diagF_DiagResponse
4959  0a58 1b86              	leas	6,s
4960                         ; 1583 }
4963  0a5a 0a                	rtc	
5005                         ; 1588 void DESC_API_CALLBACK_TYPE ApplDescReturnControl_2F501203_Ignition_Status(DescMsgContext* pMsgContext)
5005                         ; 1589 {				
5006                         	switch	.ftext
5007  0a5b                   f_ApplDescReturnControl_2F501203_Ignition_Status:
5009  0a5b 3b                	pshd	
5010       00000000          OFST:	set	0
5013                         ; 1591 	diag_io_ign_lock_b = 0; // FALSE;
5015  0a5c 1d000b04          	bclr	_diag_io_lock_flags_c,4
5016                         ; 1593 	diag_io_active_b = 0; //FALSE;
5018  0a60 1d000908          	bclr	_diag_io_lock3_flags_c,8
5019                         ; 1595 	Response = RESPONSE_OK;
5021  0a64 c601              	ldab	#1
5022  0a66 7b0002            	stab	L562_Response
5023                         ; 1596 	diagF_DiagResponse(Response, 0/*DataLength+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext); 
5025  0a69 ec80              	ldd	OFST+0,s
5026  0a6b 3b                	pshd	
5027  0a6c 87                	clra	
5028  0a6d c7                	clrb	
5029  0a6e 3b                	pshd	
5030  0a6f 52                	incb	
5031  0a70 4a000000          	call	f_diagF_DiagResponse
5033  0a74 1b86              	leas	6,s
5034                         ; 1597 }
5037  0a76 0a                	rtc	
5078                         ; 1603 void DESC_API_CALLBACK_TYPE ApplDescReturnControl_2F501303_Flash_Program_Data_1(DescMsgContext* pMsgContext)
5078                         ; 1604 {				
5079                         	switch	.ftext
5080  0a77                   f_ApplDescReturnControl_2F501303_Flash_Program_Data_1:
5082  0a77 3b                	pshd	
5083       00000000          OFST:	set	0
5086                         ; 1606 	diag_eol_io_lock_b = FALSE;
5088  0a78 1d000901          	bclr	_diag_io_lock3_flags_c,1
5089                         ; 1608 	Response = RESPONSE_OK;
5091  0a7c c601              	ldab	#1
5092  0a7e 7b0002            	stab	L562_Response
5093                         ; 1609 	diagF_DiagResponse(Response, 0/*DataLength+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext); 
5095  0a81 ec80              	ldd	OFST+0,s
5096  0a83 3b                	pshd	
5097  0a84 87                	clra	
5098  0a85 c7                	clrb	
5099  0a86 3b                	pshd	
5100  0a87 52                	incb	
5101  0a88 4a000000          	call	f_diagF_DiagResponse
5103  0a8c 1b86              	leas	6,s
5104                         ; 1610 }
5107  0a8e 0a                	rtc	
5152                         ; 1616 void DESC_API_CALLBACK_TYPE ApplDescReturnControl_2F501403_Flash_Program_Data_2(DescMsgContext* pMsgContext)
5152                         ; 1617 {
5153                         	switch	.ftext
5154  0a8f                   f_ApplDescReturnControl_2F501403_Flash_Program_Data_2:
5156  0a8f 3b                	pshd	
5157       00000000          OFST:	set	0
5160                         ; 1619 	diag_eol_no_fault_detection_b = FALSE;
5162  0a90 1d000902          	bclr	_diag_io_lock3_flags_c,2
5163                         ; 1622 	Dio_WriteChannel(REL_A_EN, STD_HIGH);
5166  0a94 1410              	sei	
5171  0a96 fe0014            	ldx	_Dio_PortWrite_Ptr+20
5172  0a99 0c0002            	bset	0,x,2
5176  0a9c 10ef              	cli	
5178                         ; 1625 	Pwm_SetDutyCycle(PWM_BASE_REL, 0x0000);
5181  0a9e 87                	clra	
5182  0a9f c7                	clrb	
5183  0aa0 3b                	pshd	
5184  0aa1 c602              	ldab	#2
5185  0aa3 4a000000          	call	f_Pwm_SetDutyCycle
5187  0aa7 1b82              	leas	2,s
5188                         ; 1628 	Response = RESPONSE_OK;
5190  0aa9 c601              	ldab	#1
5191  0aab 7b0002            	stab	L562_Response
5192                         ; 1629 	diagF_DiagResponse(Response, 0/*DataLength+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext); 
5194  0aae ec80              	ldd	OFST+0,s
5195  0ab0 3b                	pshd	
5196  0ab1 87                	clra	
5197  0ab2 c7                	clrb	
5198  0ab3 3b                	pshd	
5199  0ab4 52                	incb	
5200  0ab5 4a000000          	call	f_diagF_DiagResponse
5202  0ab9 1b86              	leas	6,s
5203                         ; 1630 }
5206  0abb 0a                	rtc	
5251                         ; 1636 void DESC_API_CALLBACK_TYPE ApplDescReturnControl_2F501503_Flash_Program_Data_3(DescMsgContext* pMsgContext)
5251                         ; 1637 {				
5252                         	switch	.ftext
5253  0abc                   f_ApplDescReturnControl_2F501503_Flash_Program_Data_3:
5255  0abc 3b                	pshd	
5256       00000000          OFST:	set	0
5259                         ; 1639 	diag_eol_no_fault_detection_b = FALSE;
5261  0abd 1d000902          	bclr	_diag_io_lock3_flags_c,2
5262                         ; 1642 	Dio_WriteChannel(REL_B_EN, STD_HIGH);
5265  0ac1 1410              	sei	
5270  0ac3 fe0014            	ldx	_Dio_PortWrite_Ptr+20
5271  0ac6 0c0001            	bset	0,x,1
5275  0ac9 10ef              	cli	
5277                         ; 1645 	Pwm_SetDutyCycle(PWM_BASE_REL, 0x0000);
5280  0acb 87                	clra	
5281  0acc c7                	clrb	
5282  0acd 3b                	pshd	
5283  0ace c602              	ldab	#2
5284  0ad0 4a000000          	call	f_Pwm_SetDutyCycle
5286  0ad4 1b82              	leas	2,s
5287                         ; 1648 	Response = RESPONSE_OK;
5289  0ad6 c601              	ldab	#1
5290  0ad8 7b0002            	stab	L562_Response
5291                         ; 1649 	diagF_DiagResponse(Response, 0/*DataLength+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext); 
5293  0adb ec80              	ldd	OFST+0,s
5294  0add 3b                	pshd	
5295  0ade 87                	clra	
5296  0adf c7                	clrb	
5297  0ae0 3b                	pshd	
5298  0ae1 52                	incb	
5299  0ae2 4a000000          	call	f_diagF_DiagResponse
5301  0ae6 1b86              	leas	6,s
5302                         ; 1650 }
5305  0ae8 0a                	rtc	
5350                         ; 1656 void DESC_API_CALLBACK_TYPE ApplDescReturnControl_2F501603_Flash_Program_Data_4(DescMsgContext* pMsgContext)
5350                         ; 1657 {				
5351                         	switch	.ftext
5352  0ae9                   f_ApplDescReturnControl_2F501603_Flash_Program_Data_4:
5354  0ae9 3b                	pshd	
5355       00000000          OFST:	set	0
5358                         ; 1659 	diag_eol_no_fault_detection_b = FALSE;
5360  0aea 1d000902          	bclr	_diag_io_lock3_flags_c,2
5361                         ; 1662 	Dio_WriteChannel(REL_STW_EN, STD_HIGH);
5364  0aee 1410              	sei	
5369  0af0 fe0008            	ldx	_Dio_PortWrite_Ptr+8
5370  0af3 0c0008            	bset	0,x,8
5374  0af6 10ef              	cli	
5376                         ; 1665 	Pwm_SetDutyCycle(PWM_BASE_REL, 0x0000);
5379  0af8 87                	clra	
5380  0af9 c7                	clrb	
5381  0afa 3b                	pshd	
5382  0afb c602              	ldab	#2
5383  0afd 4a000000          	call	f_Pwm_SetDutyCycle
5385  0b01 1b82              	leas	2,s
5386                         ; 1667 	Response = RESPONSE_OK;
5388  0b03 c601              	ldab	#1
5389  0b05 7b0002            	stab	L562_Response
5390                         ; 1668 	diagF_DiagResponse(Response, 0/*DataLength+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext); 
5392  0b08 ec80              	ldd	OFST+0,s
5393  0b0a 3b                	pshd	
5394  0b0b 87                	clra	
5395  0b0c c7                	clrb	
5396  0b0d 3b                	pshd	
5397  0b0e 52                	incb	
5398  0b0f 4a000000          	call	f_diagF_DiagResponse
5400  0b13 1b86              	leas	6,s
5401                         ; 1669 }
5404  0b15 0a                	rtc	
5541                         	xref	f_diagF_DiagResponse
5542                         	switch	.bss
5543  0000                   L762_DataLength:
5544  0000 0000              	ds.b	2
5545  0002                   L562_Response:
5546  0002 00                	ds.b	1
5547                         	xref	f_Pwm_SetDutyCycle
5548                         	xref	_Dio_PortWrite_Ptr
5549                         	xref	_stW_Flag1_c
5550                         	xref	_vs_Flag2_c
5551                         	xref	_CSWM_config_c
5552                         	xref	_canio_LOAD_SHED_c
5553                         	xref	_canio_RX_IGN_STATUS_c
5554  0003                   _diag_io_vs_lock_flags_c:
5555  0003 0000              	ds.b	2
5556                         	xdef	_diag_io_vs_lock_flags_c
5557  0005                   _diag_io_hs_lock_flags_c:
5558  0005 00000000          	ds.b	4
5559                         	xdef	_diag_io_hs_lock_flags_c
5560  0009                   _diag_io_lock3_flags_c:
5561  0009 00                	ds.b	1
5562                         	xdef	_diag_io_lock3_flags_c
5563  000a                   _diag_io_lock2_flags_c:
5564  000a 00                	ds.b	1
5565                         	xdef	_diag_io_lock2_flags_c
5566  000b                   _diag_io_lock_flags_c:
5567  000b 00                	ds.b	1
5568                         	xdef	_diag_io_lock_flags_c
5569  000c                   _diag_io_vs_state_c:
5570  000c 0000              	ds.b	2
5571                         	xdef	_diag_io_vs_state_c
5572  000e                   _diag_io_vs_rq_c:
5573  000e 0000              	ds.b	2
5574                         	xdef	_diag_io_vs_rq_c
5575  0010                   _diag_io_hs_state_c:
5576  0010 00000000          	ds.b	4
5577                         	xdef	_diag_io_hs_state_c
5578  0014                   _diag_io_hs_rq_c:
5579  0014 00000000          	ds.b	4
5580                         	xdef	_diag_io_hs_rq_c
5581  0018                   _diag_op_rc_preCondition_stat_c:
5582  0018 00                	ds.b	1
5583                         	xdef	_diag_op_rc_preCondition_stat_c
5584  0019                   _diag_op_rc_status_c:
5585  0019 00                	ds.b	1
5586                         	xdef	_diag_op_rc_status_c
5587  001a                   _diag_io_st_whl_state_c:
5588  001a 00                	ds.b	1
5589                         	xdef	_diag_io_st_whl_state_c
5590  001b                   _diag_io_st_whl_rq_c:
5591  001b 00                	ds.b	1
5592                         	xdef	_diag_io_st_whl_rq_c
5593  001c                   _diag_io_vs_all_out_c:
5594  001c 00                	ds.b	1
5595                         	xdef	_diag_io_vs_all_out_c
5596  001d                   _diag_io_ign_state_c:
5597  001d 00                	ds.b	1
5598                         	xdef	_diag_io_ign_state_c
5599  001e                   _diag_io_load_shed_c:
5600  001e 00                	ds.b	1
5601                         	xdef	_diag_io_load_shed_c
5602  001f                   L12_diag_sec_FAA_c:
5603  001f 00                	ds.b	1
5604  0020                   L71_secMod1_var2_l:
5605  0020 00000000          	ds.b	4
5606  0024                   L51_secMod1_var1_l:
5607  0024 00000000          	ds.b	4
5608  0028                   L31_diag_10sec_timer_w:
5609  0028 0000              	ds.b	2
5610  002a                   L11_hsm_key_l:
5611  002a 00000000          	ds.b	4
5612  002e                   L7_tool_key_l:
5613  002e 00000000          	ds.b	4
5614  0032                   L5_diag_seed_generated_l:
5615  0032 00000000          	ds.b	4
5616  0036                   L3_diag_seed_l:
5617  0036 00000000          	ds.b	4
5618                         	xref	_diag_rc_lock_flags_c
5619                         	xref	_mainF_CSWM_Status
5620                         	xdef	f_ApplDescControl_2F501603_Flash_Program_Data_4
5621                         	xdef	f_ApplDescReturnControl_2F501603_Flash_Program_Data_4
5622                         	xdef	f_ApplDescControl_2F501503_Flash_Program_Data_3
5623                         	xdef	f_ApplDescReturnControl_2F501503_Flash_Program_Data_3
5624                         	xdef	f_ApplDescControl_2F501403_Flash_Program_Data_2
5625                         	xdef	f_ApplDescReturnControl_2F501403_Flash_Program_Data_2
5626                         	xdef	f_ApplDescControl_2F501303_Flash_Program_Data_1
5627                         	xdef	f_ApplDescReturnControl_2F501303_Flash_Program_Data_1
5628                         	xdef	f_ApplDescControl_2F501203_Ignition_Status
5629                         	xdef	f_ApplDescReturnControl_2F501203_Ignition_Status
5630                         	xdef	f_ApplDescControl_2F501103_Load_shedding_status_BI
5631                         	xdef	f_ApplDescReturnControl_2F501103_Load_shedding_status_BI
5632                         	xdef	f_ApplDescControl_2F501003_Heated_Steering_Wheel_Status_BO
5633                         	xdef	f_ApplDescReturnControl_2F501003_Heated_Steering_Wheel_Status_BO
5634                         	xdef	f_ApplDescControl_2F500F03_F_R_Vent_Seat_Status_BO
5635                         	xdef	f_ApplDescReturnControl_2F500F03_F_R_Vent_Seat_Status_BO
5636                         	xdef	f_ApplDescControl_2F500E03_F_L_Vent_Seat_Status_BO
5637                         	xdef	f_ApplDescReturnControl_2F500E03_F_L_Vent_Seat_Status_BO
5638                         	xdef	f_ApplDescControl_2F500D03_R_R_Heat_Seat_Status_BO
5639                         	xdef	f_ApplDescReturnControl_2F500D03_R_R_Heat_Seat_Status_BO
5640                         	xdef	f_ApplDescControl_2F500C03_R_L_Heat_Seat_Status_BO
5641                         	xdef	f_ApplDescReturnControl_2F500C03_R_L_Heat_Seat_Status_BO
5642                         	xdef	f_ApplDescControl_2F500B03_F_R_Heat_Seat_Status_BO
5643                         	xdef	f_ApplDescReturnControl_2F500B03_F_R_Heat_Seat_Status_BO
5644                         	xdef	f_ApplDescControl_2F500A03_F_L_Heat_Seat_Status_BO
5645                         	xdef	f_ApplDescReturnControl_2F500A03_F_L_Heat_Seat_Status_BO
5646                         	xdef	f_ApplDescControl_2F500903_Heated_Steering_Wheel_Request_BI
5647                         	xdef	f_ApplDescReturnControl_2F500903_Heated_Steering_Wheel_Request_BI
5648                         	xdef	f_ApplDescControl_2F500803_F_R_Vent_Seat_Request_BI
5649                         	xdef	f_ApplDescReturnControl_2F500803_F_R_Vent_Seat_Request_BI
5650                         	xdef	f_ApplDescControl_2F500703_F_L_Vent_Seat_Request_BI
5651                         	xdef	f_ApplDescReturnControl_2F500703_F_L_Vent_Seat_Request_BI
5652                         	xdef	f_ApplDescControl_2F500603_R_R_Heat_Seat_Request_BI
5653                         	xdef	f_ApplDescReturnControl_2F500603_R_R_Heat_Seat_Request_BI
5654                         	xdef	f_ApplDescControl_2F500503_R_L_Heat_Seat_Request_BI
5655                         	xdef	f_ApplDescReturnControl_2F500503_R_L_Heat_Seat_Request_BI
5656                         	xdef	f_ApplDescControl_2F500403_F_R_Heat_Seat_Request_BI
5657                         	xdef	f_ApplDescReturnControl_2F500403_F_R_Heat_Seat_Request_BI
5658                         	xdef	f_ApplDescControl_2F500303_F_L_Heat_Seat_Request_BI
5659                         	xdef	f_ApplDescReturnControl_2F500303_F_L_Heat_Seat_Request_BI
5660                         	xdef	f_ApplDescControl_2F500203_Actuate_all_Vents
5661                         	xdef	f_ApplDescReturnControl_2F500203_Actuate_all_Vents
5662                         	xdef	f_ApplDescControl_2F500103_Actuate_all_heaters
5663                         	xdef	f_ApplDescReturnControl_2F500103_Actuate_all_heaters
5684                         	end
