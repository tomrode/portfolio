   1                         ; C Compiler for S12XE [COSMIC Software].
   2                         ; Generator V4.7.8 - 27 Jul 2007
   3                         ; Optimizer V4.6.13 - 27 Jul 2007
   4                         .const:	section	.data
   5  0000                   _NTC_READ:
   6  0000 e5                	dc.b	229
   7  0001 e4                	dc.b	228
   8  0002 e3                	dc.b	227
   9  0003 e1                	dc.b	225
  10  0004 df                	dc.b	223
  11  0005 dc                	dc.b	220
  12  0006 da                	dc.b	218
  13  0007 d6                	dc.b	214
  14  0008 d2                	dc.b	210
  15  0009 cc                	dc.b	204
  16  000a c6                	dc.b	198
  17  000b bf                	dc.b	191
  18  000c b6                	dc.b	182
  19  000d ad                	dc.b	173
  20  000e a4                	dc.b	164
  21  000f 99                	dc.b	153
  22  0010 8e                	dc.b	142
  23  0011 83                	dc.b	131
  24  0012 78                	dc.b	120
  25  0013 6d                	dc.b	109
  26  0014 63                	dc.b	99
  27  0015 59                	dc.b	89
  28  0016 4f                	dc.b	79
  29  0017 47                	dc.b	71
  30  0018 3f                	dc.b	63
  31  0019 38                	dc.b	56
  68                         ; 190 @far void TempF_Init(void)
  68                         ; 191 {
  69                         .ftext:	section	.text
  70  0000                   f_TempF_Init:
  72  0000 3b                	pshd	
  73       00000002          OFST:	set	2
  76                         ; 193 	EE_BlockRead(EE_TEMPERATURE_CALIB_DATA, (u_8Bit*)&temperature_s);
  78  0001 cc0013            	ldd	#_temperature_s
  79  0004 3b                	pshd	
  80  0005 cc0009            	ldd	#9
  81  0008 4a000000          	call	f_EE_BlockRead
  83  000c 1b82              	leas	2,s
  84                         ; 196 	temperature_PCB_NTC_status_c = PCB_STAT_GOOD;
  86  000e c604              	ldab	#4
  87  0010 7b0008            	stab	_temperature_PCB_NTC_status_c
  88                         ; 199 	temperature_HS_NTC_status[0] = NTC_STAT_GOOD;
  90  0013 53                	decb	
  91  0014 7b001d            	stab	_temperature_HS_NTC_status
  92                         ; 200 	temperature_HS_NTC_status[1] = NTC_STAT_GOOD;
  94  0017 7b001e            	stab	_temperature_HS_NTC_status+1
  95                         ; 201 	temperature_HS_NTC_status[2] = NTC_STAT_GOOD;
  97  001a 7b001f            	stab	_temperature_HS_NTC_status+2
  98                         ; 202 	temperature_HS_NTC_status[3] = NTC_STAT_GOOD;
 100  001d 7b0020            	stab	_temperature_HS_NTC_status+3
 101                         ; 204 	temperature_HS_ontimeLmt_w = ( (temperature_s.NTCOpen_OutputOntime_lmt * TEMPERATURE_HS_1MIN) + (temperature_s.NTCOpen_OutputOntime_lmt>>1) );
 103  0020 f60016            	ldab	_temperature_s+3
 104  0023 87                	clra	
 105  0024 54                	lsrb	
 106  0025 6c80              	std	OFST-2,s
 107  0027 f60016            	ldab	_temperature_s+3
 108  002a 86bb              	ldaa	#187
 109  002c 12                	mul	
 110  002d e380              	addd	OFST-2,s
 111  002f 7c0005            	std	_temperature_HS_ontimeLmt_w
 112                         ; 205 }
 115  0032 31                	puly	
 116  0033 0a                	rtc	
 156                         ; 234 @far void TempF_ADC_SetUp(u_8Bit temperature_thrmstr_sel_c)
 156                         ; 235 {
 157                         	switch	.ftext
 158  0034                   f_TempF_ADC_SetUp:
 160  0034 3b                	pshd	
 161  0035 37                	pshb	
 162       00000001          OFST:	set	1
 165                         ; 240 	temperature_hs_sel = temperature_thrmstr_sel_c;
 167  0036 6b80              	stab	OFST-1,s
 168                         ; 242 	switch (temperature_hs_sel) {
 171  0038 270b              	beq	L12
 172  003a 040114            	dbeq	b,L32
 173  003d 04011b            	dbeq	b,L52
 174  0040 040127            	dbeq	b,L72
 175  0043 2032              	bra	L35
 176  0045                   L12:
 177                         ; 246 			temperature_AD_prms.slctd_adCH_c  = CH6;     // selected AD channel is Channel 6
 179  0045 c606              	ldab	#6
 180  0047 7b0001            	stab	_temperature_AD_prms+1
 181                         ; 247 			temperature_AD_prms.slctd_mux_c   = MUX1;    // selected mux for front left seat thermistor is MUX1
 183  004a 790002            	clr	_temperature_AD_prms+2
 184                         ; 248 			temperature_AD_prms.slctd_muxCH_c = CH1;     // selected mux channel is Channel 1
 186  004d c601              	ldab	#1
 187                         ; 249 			break;
 189  004f 2014              	bra	LC002
 190  0051                   L32:
 191                         ; 255 			temperature_AD_prms.slctd_adCH_c  = CH6;     // selected AD channel is Channel 6
 193  0051 c606              	ldab	#6
 194  0053 7b0001            	stab	_temperature_AD_prms+1
 195                         ; 256 			temperature_AD_prms.slctd_mux_c   = MUX1;    // selected mux for front right seat thermistor is MUX1
 197  0056 790002            	clr	_temperature_AD_prms+2
 198                         ; 257 			temperature_AD_prms.slctd_muxCH_c = CH0;     // selected mux channel is Channel 0
 200                         ; 258 			break;
 202  0059 2019              	bra	LC001
 203  005b                   L52:
 204                         ; 264 			temperature_AD_prms.slctd_adCH_c  = CH5;     // selected AD channel is Channel 5
 206  005b c605              	ldab	#5
 207  005d 7b0001            	stab	_temperature_AD_prms+1
 208                         ; 265 			temperature_AD_prms.slctd_mux_c   = MUX2;    // selected mux for rear left seat thermistor is MUX2
 210  0060 c601              	ldab	#1
 211  0062 7b0002            	stab	_temperature_AD_prms+2
 212                         ; 266 			temperature_AD_prms.slctd_muxCH_c = CH1;     // selected mux channel is Channel 1
 214  0065                   LC002:
 215  0065 7b0000            	stab	_temperature_AD_prms
 216                         ; 268 			break;
 218  0068 200d              	bra	L35
 219  006a                   L72:
 220                         ; 274 			temperature_AD_prms.slctd_adCH_c  = CH5;     // selected AD channel is Channel 5
 222  006a c605              	ldab	#5
 223  006c 7b0001            	stab	_temperature_AD_prms+1
 224                         ; 275 			temperature_AD_prms.slctd_mux_c   = MUX2;    // selected mux for rear right seat thermistor is MUX2
 226  006f c601              	ldab	#1
 227  0071 7b0002            	stab	_temperature_AD_prms+2
 228                         ; 276 			temperature_AD_prms.slctd_muxCH_c = CH0;     // selected mux channel is Channel 0
 230  0074                   LC001:
 231  0074 790000            	clr	_temperature_AD_prms
 232                         ; 277 			break;
 234  0077                   L35:
 235                         ; 285 }
 238  0077 1b83              	leas	3,s
 239  0079 0a                	rtc	
 270                         ; 320 @far void TempF_PCB_ADC(void)
 270                         ; 321 {
 271                         	switch	.ftext
 272  007a                   f_TempF_PCB_ADC:
 274  007a 37                	pshb	
 275       00000001          OFST:	set	1
 278                         ; 323 	Dio_WriteChannelGroup(MuxSelGroup, CH7);
 281  007b 1410              	sei	
 286  007d f60000            	ldab	_DioConfigData
 287  0080 87                	clra	
 288  0081 59                	lsld	
 289  0082 b746              	tfr	d,y
 290  0084 c607              	ldab	#7
 291  0086 b60001            	ldaa	_DioConfigData+1
 292  0089 2704              	beq	L41
 293  008b                   L61:
 294  008b 58                	lslb	
 295  008c 0430fc            	dbne	a,L61
 296  008f                   L41:
 297  008f f40002            	andb	_DioConfigData+2
 298  0092 6b80              	stab	OFST-1,s
 299  0094 f60000            	ldab	_DioConfigData
 300  0097 87                	clra	
 301  0098 59                	lsld	
 302  0099 b745              	tfr	d,x
 303  009b f60002            	ldab	_DioConfigData+2
 304  009e 51                	comb	
 305  009f e4e30000          	andb	[_Dio_PortWrite_Ptr,x]
 306  00a3 ea80              	orab	OFST-1,s
 307  00a5 6beb0000          	stab	[_Dio_PortWrite_Ptr,y]
 311  00a9 10ef              	cli	
 313                         ; 326 	ADF_AtoD_Cnvrsn(CH6);
 316  00ab cc0006            	ldd	#6
 317  00ae 160000            	jsr	_ADF_AtoD_Cnvrsn
 319                         ; 329 	ADF_Str_UnFlt_Mux_Rslt(CH6, CH7);
 321  00b1 cc0007            	ldd	#7
 322  00b4 3b                	pshd	
 323  00b5 53                	decb	
 324  00b6 160000            	jsr	_ADF_Str_UnFlt_Mux_Rslt
 326                         ; 332 	ADF_Mux_Filter(MUX1, CH7);
 328  00b9 cc0007            	ldd	#7
 329  00bc 6c80              	std	0,s
 330  00be c7                	clrb	
 331  00bf 160000            	jsr	_ADF_Mux_Filter
 333                         ; 335 	temperature_PCB_ADC = AD_mux_Filter[MUX1][CH7].flt_rslt_w;
 335  00c2 1804002e001b      	movw	_AD_mux_Filter+46,_temperature_PCB_ADC
 336                         ; 336 }
 339  00c8 1b83              	leas	3,s
 340  00ca 0a                	rtc	
 387                         ; 360 @far void TempF_Single_Thrmstr_ADC(u_8Bit temperature_sel_c)
 387                         ; 361 {
 388                         	switch	.ftext
 389  00cb                   f_TempF_Single_Thrmstr_ADC:
 391  00cb 3b                	pshd	
 392  00cc 3b                	pshd	
 393       00000002          OFST:	set	2
 396                         ; 366 	temperature_pick = temperature_sel_c;
 398  00cd 6b81              	stab	OFST-1,s
 399                         ; 369 	TempF_ADC_SetUp(temperature_pick);
 401  00cf 87                	clra	
 402  00d0 4a003434          	call	f_TempF_ADC_SetUp
 404                         ; 372 	Dio_WriteChannelGroup(MuxSelGroup, temperature_AD_prms.slctd_muxCH_c);
 407  00d4 1410              	sei	
 412  00d6 f60000            	ldab	_DioConfigData
 413  00d9 87                	clra	
 414  00da 59                	lsld	
 415  00db b746              	tfr	d,y
 416  00dd f60000            	ldab	_temperature_AD_prms
 417  00e0 b60001            	ldaa	_DioConfigData+1
 418  00e3 2704              	beq	L22
 419  00e5                   L42:
 420  00e5 58                	lslb	
 421  00e6 0430fc            	dbne	a,L42
 422  00e9                   L22:
 423  00e9 f40002            	andb	_DioConfigData+2
 424  00ec 6b80              	stab	OFST-2,s
 425  00ee f60000            	ldab	_DioConfigData
 426  00f1 87                	clra	
 427  00f2 b745              	tfr	d,x
 428  00f4 1848              	lslx	
 429  00f6 f60002            	ldab	_DioConfigData+2
 430  00f9 51                	comb	
 431  00fa e4e30000          	andb	[_Dio_PortWrite_Ptr,x]
 432  00fe ea80              	orab	OFST-2,s
 433  0100 6beb0000          	stab	[_Dio_PortWrite_Ptr,y]
 437  0104 10ef              	cli	
 439                         ; 375 	ADF_AtoD_Cnvrsn(temperature_AD_prms.slctd_adCH_c);
 442  0106 f60001            	ldab	_temperature_AD_prms+1
 443  0109 160000            	jsr	_ADF_AtoD_Cnvrsn
 445                         ; 378 	ADF_Str_UnFlt_Mux_Rslt(temperature_AD_prms.slctd_adCH_c, temperature_AD_prms.slctd_muxCH_c);
 447  010c f60000            	ldab	_temperature_AD_prms
 448  010f 87                	clra	
 449  0110 3b                	pshd	
 450  0111 f60001            	ldab	_temperature_AD_prms+1
 451  0114 160000            	jsr	_ADF_Str_UnFlt_Mux_Rslt
 453                         ; 379 }
 456  0117 1b86              	leas	6,s
 457  0119 0a                	rtc	
 538                         ; 415 @far void TempF_Stndrdz_Trmstr_Rdg(u_8Bit temperature_mux, u_8Bit temperature_mux_ch)
 538                         ; 416 {
 539                         	switch	.ftext
 540  011a                   f_TempF_Stndrdz_Trmstr_Rdg:
 542  011a 3b                	pshd	
 543  011b 1b9a              	leas	-6,s
 544       00000006          OFST:	set	6
 547                         ; 425 	temperature_slctd_mux = temperature_mux;
 549  011d 6b82              	stab	OFST-4,s
 550                         ; 426 	temperature_slctd_mux_ch = temperature_mux_ch;
 552  011f e68c              	ldab	OFST+6,s
 553  0121 6b83              	stab	OFST-3,s
 554                         ; 427 	temperature_AD_ulf_w = AD_mux_Filter[temperature_slctd_mux][temperature_slctd_mux_ch].unflt_rslt_w;
 556  0123 8606              	ldaa	#6
 557  0125 12                	mul	
 558  0126 b746              	tfr	d,y
 559  0128 e682              	ldab	OFST-4,s
 560  012a 8630              	ldaa	#48
 561  012c 12                	mul	
 562  012d 19ee              	leay	d,y
 563  012f ecea0000          	ldd	_AD_mux_Filter,y
 564  0133 6c84              	std	OFST-2,s
 565                         ; 428 	temperature_U12s_local_w = AD_Filter_set[CH0].unflt_rslt_w;
 567  0135 fe0000            	ldx	_AD_Filter_set
 568                         ; 431 	temperature_stndrdz_thrmstr_w = ( (temperature_AD_ulf_w<<6) / (temperature_U12s_local_w>>2) );
 570  0138 59                	lsld	
 571  0139 59                	lsld	
 572  013a 59                	lsld	
 573  013b 59                	lsld	
 574  013c 59                	lsld	
 575  013d 59                	lsld	
 576  013e 1844              	lsrx	
 577  0140 1844              	lsrx	
 578  0142 1810              	idiv	
 579  0144 6e80              	stx	OFST-6,s
 580                         ; 434 	if (temperature_stndrdz_thrmstr_w <= TEMPERATURE_NTC_LMT) {
 582  0146 8e00ff            	cpx	#255
 583  0149 2212              	bhi	L331
 584                         ; 436 		AD_mux_Filter[temperature_slctd_mux][temperature_slctd_mux_ch].unflt_rslt_w = temperature_stndrdz_thrmstr_w;
 586  014b e683              	ldab	OFST-3,s
 587  014d 8606              	ldaa	#6
 588  014f 12                	mul	
 589  0150 b746              	tfr	d,y
 590  0152 e682              	ldab	OFST-4,s
 591  0154 8630              	ldaa	#48
 592  0156 12                	mul	
 593  0157 19ee              	leay	d,y
 594  0159 b754              	tfr	x,d
 596  015b 2011              	bra	L531
 597  015d                   L331:
 598                         ; 440 		AD_mux_Filter[temperature_slctd_mux][temperature_slctd_mux_ch].unflt_rslt_w = TEMPERATURE_NTC_LMT;
 600  015d e683              	ldab	OFST-3,s
 601  015f 8606              	ldaa	#6
 602  0161 12                	mul	
 603  0162 b746              	tfr	d,y
 604  0164 e682              	ldab	OFST-4,s
 605  0166 8630              	ldaa	#48
 606  0168 12                	mul	
 607  0169 19ee              	leay	d,y
 608  016b cc00ff            	ldd	#255
 609  016e                   L531:
 610  016e 6cea0000          	std	_AD_mux_Filter,y
 611                         ; 442 }
 614  0172 1b88              	leas	8,s
 615  0174 0a                	rtc	
 647                         ; 471 @far void TempF_Chck_NTC_SupplyV(void)
 647                         ; 472 {
 648                         	switch	.ftext
 649  0175                   f_TempF_Chck_NTC_SupplyV:
 653                         ; 474 	temperature_NTC_supplyV = BattVF_AD_to_100mV_Cnvrsn(AD_Filter_set[CH0].flt_rslt_w);
 655  0175 fc0004            	ldd	_AD_Filter_set+4
 656  0178 4a000000          	call	f_BattVF_AD_to_100mV_Cnvrsn
 658  017c 7b0021            	stab	_temperature_NTC_supplyV
 659                         ; 477 	EE_BlockRead(EE_INTERNAL_FLT_BYTE2, (u_8Bit*)&intFlt_bytes[TWO]);
 661  017f cc0002            	ldd	#_intFlt_bytes+2
 662  0182 3b                	pshd	
 663  0183 cc001a            	ldd	#26
 664  0186 4a000000          	call	f_EE_BlockRead
 666  018a 1b82              	leas	2,s
 667                         ; 480 	if (temperature_NTC_supplyV > temperature_s.NTC_Supply_V_Low_Thrshld) {
 669  018c f60021            	ldab	_temperature_NTC_supplyV
 670  018f f10015            	cmpb	_temperature_s+2
 671  0192 231a              	bls	L741
 672                         ; 483 		if ( (intFlt_bytes[TWO] & NTC_SUPPLY_LOW_MASK) == NTC_SUPPLY_LOW_MASK ) {
 674  0194 1f0002043f        	brclr	_intFlt_bytes+2,4,L551
 675                         ; 486 			IntFlt_F_Update(TWO, CLR_NTC_SUPPLY_LOW, INT_FLT_CLR);
 677  0199 cc0001            	ldd	#1
 678  019c 3b                	pshd	
 679  019d c6fb              	ldab	#251
 680  019f 3b                	pshd	
 681  01a0 c602              	ldab	#2
 682  01a2 4a000000          	call	f_IntFlt_F_Update
 684  01a6 1b84              	leas	4,s
 685                         ; 489 			NTC_Supply_Voltage_Flt_b = FALSE;
 687  01a8 1d000004          	bclr	_int_flt_byte2,4
 688                         ; 492 			temperature_NTC_SupplyV_flt_mtr_cnt = 0;
 691  01ac 2027              	bra	LC003
 692  01ae                   L741:
 693                         ; 501 		if ( (temperature_NTC_SupplyV_flt_mtr_cnt <= TEMPERATURE_FLT_MTR_TM) && (NTC_Supply_Voltage_Flt_b == FALSE) ) {
 695  01ae f60007            	ldab	_temperature_NTC_SupplyV_flt_mtr_cnt
 696  01b1 c10c              	cmpb	#12
 697  01b3 2209              	bhi	L751
 699  01b5 1e00000404        	brset	_int_flt_byte2,4,L751
 700                         ; 504 			temperature_NTC_SupplyV_flt_mtr_cnt++;
 702  01ba 720007            	inc	_temperature_NTC_SupplyV_flt_mtr_cnt
 705  01bd 0a                	rtc	
 706  01be                   L751:
 707                         ; 508 			if ( (intFlt_bytes[TWO] & NTC_SUPPLY_LOW_MASK) == NTC_SUPPLY_LOW_MASK) {
 709  01be 1e00020415        	brset	_intFlt_bytes+2,4,L551
 711                         ; 513 				IntFlt_F_Update(TWO, NTC_SUPPLY_LOW_MASK, INT_FLT_SET);
 713  01c3 87                	clra	
 714  01c4 c7                	clrb	
 715  01c5 3b                	pshd	
 716  01c6 c604              	ldab	#4
 717  01c8 3b                	pshd	
 718  01c9 c602              	ldab	#2
 719  01cb 4a000000          	call	f_IntFlt_F_Update
 721  01cf 1b84              	leas	4,s
 722                         ; 516 				NTC_Supply_Voltage_Flt_b = TRUE;
 724  01d1 1c000004          	bset	_int_flt_byte2,4
 725                         ; 519 				temperature_NTC_SupplyV_flt_mtr_cnt = 0;
 727  01d5                   LC003:
 728  01d5 790007            	clr	_temperature_NTC_SupplyV_flt_mtr_cnt
 729  01d8                   L551:
 730                         ; 523 }
 733  01d8 0a                	rtc	
 758                         ; 536 @far void TempF_U12S_ADC(void)
 758                         ; 537 {
 759                         	switch	.ftext
 760  01d9                   f_TempF_U12S_ADC:
 764                         ; 539 	ADF_AtoD_Cnvrsn(CH0);
 766  01d9 87                	clra	
 767  01da c7                	clrb	
 768  01db 160000            	jsr	_ADF_AtoD_Cnvrsn
 770                         ; 542 	ADF_Str_UnFlt_Rslt(CH0);
 772  01de 87                	clra	
 773  01df c7                	clrb	
 774  01e0 160000            	jsr	_ADF_Str_UnFlt_Rslt
 776                         ; 545 	ADF_Filter(CH0);	
 778  01e3 87                	clra	
 779  01e4 c7                	clrb	
 780  01e5 160000            	jsr	_ADF_Filter
 782                         ; 546 }
 785  01e8 0a                	rtc	
 830                         ; 585 @far void TempF_Monitor_ADC(void)
 830                         ; 586 {
 831                         	switch	.ftext
 832  01e9                   f_TempF_Monitor_ADC:
 834  01e9 37                	pshb	
 835       00000001          OFST:	set	1
 838                         ; 602 	TempF_Chck_NTC_SupplyV();
 840  01ea 4a017575          	call	f_TempF_Chck_NTC_SupplyV
 842                         ; 606 	if ( ((intFlt_bytes[TWO] & NTC_SUPPLY_LOW_MASK) != NTC_SUPPLY_LOW_MASK) && (temperature_NTC_supplyV > temperature_s.NTC_Supply_V_Low_Thrshld) ) {
 844  01ee 1e0002044e        	brset	_intFlt_bytes+2,4,L322
 846  01f3 f60021            	ldab	_temperature_NTC_supplyV
 847  01f6 f10015            	cmpb	_temperature_s+2
 848  01f9 2346              	bls	L322
 849                         ; 608 		for (i=0; i<TEMPERATURE_HS_LMT; i++) {
 851  01fb c7                	clrb	
 852  01fc 6b80              	stab	OFST-1,s
 853  01fe                   L512:
 854                         ; 611 			TempF_Single_Thrmstr_ADC(i);
 856  01fe 87                	clra	
 857  01ff 4a00cbcb          	call	f_TempF_Single_Thrmstr_ADC
 859                         ; 614 			TempF_Stndrdz_Trmstr_Rdg(temperature_AD_prms.slctd_mux_c, temperature_AD_prms.slctd_muxCH_c);
 861  0203 f60000            	ldab	_temperature_AD_prms
 862  0206 87                	clra	
 863  0207 3b                	pshd	
 864  0208 f60002            	ldab	_temperature_AD_prms+2
 865  020b 4a011a1a          	call	f_TempF_Stndrdz_Trmstr_Rdg
 867                         ; 617 			ADF_Mux_Filter(temperature_AD_prms.slctd_mux_c, temperature_AD_prms.slctd_muxCH_c);
 869  020f f60000            	ldab	_temperature_AD_prms
 870  0212 87                	clra	
 871  0213 6c80              	std	0,s
 872  0215 f60002            	ldab	_temperature_AD_prms+2
 873  0218 160000            	jsr	_ADF_Mux_Filter
 875  021b 1b82              	leas	2,s
 876                         ; 620 			temperature_hs_thrmstr_ADC[i] = (u_8Bit) AD_mux_Filter[temperature_AD_prms.slctd_mux_c][temperature_AD_prms.slctd_muxCH_c].flt_rslt_w;
 878  021d e680              	ldab	OFST-1,s
 879  021f b796              	exg	b,y
 880  0221 f60000            	ldab	_temperature_AD_prms
 881  0224 8606              	ldaa	#6
 882  0226 12                	mul	
 883  0227 b745              	tfr	d,x
 884  0229 f60002            	ldab	_temperature_AD_prms+2
 885  022c 8630              	ldaa	#48
 886  022e 12                	mul	
 887  022f 1ae6              	leax	d,x
 888  0231 180ae20005ea0009  	movb	_AD_mux_Filter+5,x,_temperature_hs_thrmstr_ADC,y
 889                         ; 608 		for (i=0; i<TEMPERATURE_HS_LMT; i++) {
 891  0239 6280              	inc	OFST-1,s
 894  023b e680              	ldab	OFST-1,s
 895  023d c104              	cmpb	#4
 896  023f 25bd              	blo	L512
 898  0241                   L322:
 899                         ; 628 	if(main_U12S_keep_ON_b == TRUE){		
 901  0241 1f00000206        	brclr	_main_flag2_c,2,L522
 902                         ; 630 		main_kept_U12S_ON_b = TRUE;
 904  0246 1c000004          	bset	_main_flag2_c,4
 906  024a 200e              	bra	L722
 907  024c                   L522:
 908                         ; 633 		Dio_WriteChannel(U12T_EN, STD_LOW);
 911  024c 1410              	sei	
 916  024e fe0000            	ldx	_Dio_PortWrite_Ptr
 917  0251 0d0010            	bclr	0,x,16
 921  0254 10ef              	cli	
 923                         ; 635 		main_kept_U12S_ON_b = FALSE;
 926  0256 1d000004          	bclr	_main_flag2_c,4
 927  025a                   L722:
 928                         ; 637 }
 931  025a 1b81              	leas	1,s
 932  025c 0a                	rtc	
 973                         ; 679 @far void TempF_HS_NTC_Flt_Monitor(void)
 973                         ; 680 {
 974                         	switch	.ftext
 975  025d                   f_TempF_HS_NTC_Flt_Monitor:
 977  025d 37                	pshb	
 978       00000001          OFST:	set	1
 981                         ; 686 	if ( ((intFlt_bytes[TWO] & NTC_SUPPLY_LOW_MASK) != NTC_SUPPLY_LOW_MASK) &&
 981                         ; 687 			(temperature_NTC_supplyV > temperature_s.NTC_Supply_V_Low_Thrshld) &&
 981                         ; 688 			(canio_RX_IGN_STATUS_c == IGN_RUN) )
 983  025e f60002            	ldab	_intFlt_bytes+2
 984  0261 c404              	andb	#4
 985  0263 c104              	cmpb	#4
 986  0265 276d              	beq	L103
 988  0267 f60021            	ldab	_temperature_NTC_supplyV
 989  026a f10015            	cmpb	_temperature_s+2
 990  026d 2365              	bls	L103
 992  026f f60000            	ldab	_canio_RX_IGN_STATUS_c
 993  0272 c104              	cmpb	#4
 994  0274 265e              	bne	L103
 995                         ; 691 		for (j=0; j<TEMPERATURE_HS_LMT; j++) {
 997  0276 c7                	clrb	
 998  0277 6b80              	stab	OFST-1,s
 999  0279                   L742:
1000                         ; 693 			if (temperature_hs_thrmstr_ADC[j] < temperature_s.HS_NTC_Lw_Thrshld)
1002  0279 87                	clra	
1003  027a b746              	tfr	d,y
1004  027c e6ea0009          	ldab	_temperature_hs_thrmstr_ADC,y
1005  0280 f10013            	cmpb	_temperature_s
1006  0283 2404              	bhs	L552
1007                         ; 696 				temperature_HS_NTC_status[j] = NTC_SHRT_TO_GND;
1009  0285 c601              	ldab	#1
1011  0287 203f              	bra	LC004
1012  0289                   L552:
1013                         ; 700 				if (temperature_hs_thrmstr_ADC[j] == TEMPERATURE_NTC_LMT)
1015  0289 e680              	ldab	OFST-1,s
1016  028b b746              	tfr	d,y
1017  028d e6ea0009          	ldab	_temperature_hs_thrmstr_ADC,y
1018  0291 04a104            	ibne	b,L162
1019                         ; 703 					temperature_HS_NTC_status[j] = NTC_OPEN_CKT;
1021  0294                   LC005:
1022  0294 c602              	ldab	#2
1024  0296 2030              	bra	LC004
1025  0298                   L162:
1026                         ; 708 					if (temperature_hs_thrmstr_ADC[j] > temperature_s.HS_NTC_Hgh_Thrshld)
1028  0298 e680              	ldab	OFST-1,s
1029  029a b746              	tfr	d,y
1030  029c e6ea0009          	ldab	_temperature_hs_thrmstr_ADC,y
1031  02a0 f10014            	cmpb	_temperature_s+1
1032  02a3 231d              	bls	L562
1033                         ; 711 						if (canio_RX_ExternalTemperature_c < TEMPERATURE_COLD_AMBIENT_LMT)
1035  02a5 f60000            	ldab	_canio_RX_ExternalTemperature_c
1036  02a8 c13c              	cmpb	#60
1037  02aa 2412              	bhs	L762
1038                         ; 714 							if (hs_time_counter_w[j] > temperature_HS_ontimeLmt_w)
1040  02ac 1858              	lsly	
1041  02ae ecea0000          	ldd	_hs_time_counter_w,y
1042  02b2 bc0005            	cpd	_temperature_HS_ontimeLmt_w
1043  02b5 2315              	bls	L752
1044                         ; 717 								temperature_HS_NTC_status[j] = NTC_OPEN_CKT;
1046  02b7 e680              	ldab	OFST-1,s
1047  02b9 87                	clra	
1048  02ba                   LC006:
1049  02ba b746              	tfr	d,y
1051  02bc 20d6              	bra	LC005
1052  02be                   L762:
1053                         ; 727 							temperature_HS_NTC_status[j] = NTC_OPEN_CKT;
1055  02be e680              	ldab	OFST-1,s
1056  02c0 20f8              	bra	LC006
1057  02c2                   L562:
1058                         ; 733 						temperature_HS_NTC_status[j] = NTC_STAT_GOOD;
1060  02c2 e680              	ldab	OFST-1,s
1061  02c4 b746              	tfr	d,y
1062  02c6 c603              	ldab	#3
1063  02c8                   LC004:
1064  02c8 6bea001d          	stab	_temperature_HS_NTC_status,y
1065  02cc                   L752:
1066                         ; 691 		for (j=0; j<TEMPERATURE_HS_LMT; j++) {
1068  02cc 6280              	inc	OFST-1,s
1071  02ce e680              	ldab	OFST-1,s
1072  02d0 c104              	cmpb	#4
1073  02d2 25a5              	blo	L742
1075  02d4                   L103:
1076                         ; 744 }
1079  02d4 1b81              	leas	1,s
1080  02d6 0a                	rtc	
1121                         ; 761 @far u_8Bit TempF_Get_NTC_Rdg(u_8Bit temperature_hs_position)
1121                         ; 762 {
1122                         	switch	.ftext
1123  02d7                   f_TempF_Get_NTC_Rdg:
1125       00000001          OFST:	set	1
1128                         ; 767 	temperature_hs_location = temperature_hs_position;
1130                         ; 770 	return (temperature_hs_thrmstr_ADC[temperature_hs_location]);
1132  02d7 b796              	exg	b,y
1133  02d9 e6ea0009          	ldab	_temperature_hs_thrmstr_ADC,y
1136  02dd 0a                	rtc	
1172                         ; 795 @far void TempF_Set_PCB_OverTemp(void)
1172                         ; 796 {
1173                         	switch	.ftext
1174  02de                   f_TempF_Set_PCB_OverTemp:
1176  02de 3b                	pshd	
1177       00000002          OFST:	set	2
1180                         ; 806 	EE_BlockRead(EE_OVER_TEMP_FAILSAFE,(u_8Bit*)&Temp_FailSafe_prms);
1182  02df cc000d            	ldd	#_Temp_FailSafe_prms
1183  02e2 3b                	pshd	
1184  02e3 cc0015            	ldd	#21
1185  02e6 4a000000          	call	f_EE_BlockRead
1187  02ea 1b82              	leas	2,s
1188                         ; 809 	TempOdo = FMemLibF_GetOdometer();
1190  02ec 4a000000          	call	f_FMemLibF_GetOdometer
1192  02f0 6c80              	std	OFST-2,s
1193                         ; 814 	if (Temp_FailSafe_prms.PCB_OverTemp_OdoStamp_First_w == 0xFFFF) {
1195  02f2 fc000d            	ldd	_Temp_FailSafe_prms
1196  02f5 04a405            	ibne	d,L733
1197                         ; 818 		Temp_FailSafe_prms.PCB_OverTemp_OdoStamp_First_w = TempOdo;
1199  02f8 180580000d        	movw	OFST-2,s,_Temp_FailSafe_prms
1201  02fd                   L733:
1202                         ; 829 	Temp_FailSafe_prms.PCB_OverTemp_OdoStamp_Latest_w = TempOdo;
1204  02fd 180580000f        	movw	OFST-2,s,_Temp_FailSafe_prms+2
1205                         ; 834 	if (Temp_FailSafe_prms.PCB_OverTemp_Counter_c < OVER_TEMP_COUNTER_LMT) {
1207  0302 f60011            	ldab	_Temp_FailSafe_prms+4
1208  0305 c1fe              	cmpb	#254
1209  0307 2403              	bhs	L343
1210                         ; 837 		Temp_FailSafe_prms.PCB_OverTemp_Counter_c++;
1212  0309 720011            	inc	_Temp_FailSafe_prms+4
1214  030c                   L343:
1215                         ; 845 	Temp_FailSafe_prms.PCB_OverTemp_Status_c = OVER_TEMP_SET;
1217  030c c601              	ldab	#1
1218  030e 7b0012            	stab	_Temp_FailSafe_prms+5
1219                         ; 847 	EE_BlockWrite(EE_OVER_TEMP_FAILSAFE,(u_8Bit*)&Temp_FailSafe_prms);
1221  0311 cc000d            	ldd	#_Temp_FailSafe_prms
1222  0314 3b                	pshd	
1223  0315 cc0015            	ldd	#21
1224  0318 4a000000          	call	f_EE_BlockWrite
1226  031c 1b84              	leas	4,s
1227                         ; 849 }
1230  031e 0a                	rtc	
1256                         ; 868 @far void TempF_Clr_PCB_OverTemp(void)
1256                         ; 869 {
1257                         	switch	.ftext
1258  031f                   f_TempF_Clr_PCB_OverTemp:
1262                         ; 871 	EE_BlockRead(EE_OVER_TEMP_FAILSAFE,(u_8Bit*)&Temp_FailSafe_prms);
1264  031f cc000d            	ldd	#_Temp_FailSafe_prms
1265  0322 3b                	pshd	
1266  0323 cc0015            	ldd	#21
1267  0326 4a000000          	call	f_EE_BlockRead
1269  032a 1b82              	leas	2,s
1270                         ; 874 	if (Temp_FailSafe_prms.PCB_OverTemp_Status_c == OVER_TEMP_SET) {
1272  032c f60012            	ldab	_Temp_FailSafe_prms+5
1273  032f 042110            	dbne	b,L753
1274                         ; 877 		Temp_FailSafe_prms.PCB_OverTemp_Status_c = OVER_TEMP_CLEAR;
1276  0332 790012            	clr	_Temp_FailSafe_prms+5
1277                         ; 879 		EE_BlockWrite(EE_OVER_TEMP_FAILSAFE,(u_8Bit*)&Temp_FailSafe_prms);		
1279  0335 cc000d            	ldd	#_Temp_FailSafe_prms
1280  0338 3b                	pshd	
1281  0339 cc0015            	ldd	#21
1282  033c 4a000000          	call	f_EE_BlockWrite
1284  0340 1b82              	leas	2,s
1286  0342                   L753:
1287                         ; 884 }
1290  0342 0a                	rtc	
1332                         ; 1170 @far void TempF_PCB_NTC_Flt_Monitor(void)
1332                         ; 1171 {
1333                         	switch	.ftext
1334  0343                   f_TempF_PCB_NTC_Flt_Monitor:
1336  0343 37                	pshb	
1337       00000001          OFST:	set	1
1340                         ; 1175 	temperature_BatteryV_stat_c = BattVF_Get_Status(BATTERY_V);
1342  0344 cc0001            	ldd	#1
1343  0347 4a000000          	call	f_BattVF_Get_Status
1345  034b 6b80              	stab	OFST-1,s
1346                         ; 1179 	if(temperature_BatteryV_stat_c == V_STAT_GOOD){
1348  034d 042155            	dbne	b,L573
1349                         ; 1185 		EE_BlockRead(EE_OVER_TEMP_FAILSAFE,(u_8Bit*)&Temp_FailSafe_prms);
1351  0350 cc000d            	ldd	#_Temp_FailSafe_prms
1352  0353 3b                	pshd	
1353  0354 cc0015            	ldd	#21
1354  0357 4a000000          	call	f_EE_BlockRead
1356  035b 1b82              	leas	2,s
1357                         ; 1188 		if (temperature_PCB_ADC <= temperature_s.PCB_Set_OverTemp) {
1359  035d fc001b            	ldd	_temperature_PCB_ADC
1360  0360 bc0017            	cpd	_temperature_s+4
1361  0363 221e              	bhi	L773
1362                         ; 1191 			if (Temp_FailSafe_prms.PCB_OverTemp_Status_c == OVER_TEMP_CLEAR) {
1364  0365 f60012            	ldab	_Temp_FailSafe_prms+5
1365  0368 262c              	bne	LC009
1366                         ; 1195 				if (temperature_PCB_OT_cnt <TEMPERATURE_PCB_OT_TM_LMT) {					
1368  036a fc0003            	ldd	_temperature_PCB_OT_cnt
1369  036d 8c05dc            	cpd	#1500
1370  0370 2406              	bhs	L304
1371                         ; 1197 					temperature_PCB_OT_cnt++;
1373  0372 18720003          	incw	_temperature_PCB_OT_cnt
1375  0376 2031              	bra	L324
1376  0378                   L304:
1377                         ; 1201 					temperature_PCB_NTC_status_c = PCB_STAT_OVERTEMP;
1379  0378 c603              	ldab	#3
1380  037a 7b0008            	stab	_temperature_PCB_NTC_status_c
1381                         ; 1204 					TempF_Set_PCB_OverTemp();
1383  037d 4a02dede          	call	f_TempF_Set_PCB_OverTemp
1385  0381 2026              	bra	L324
1386                         ; 1209 				temperature_PCB_NTC_status_c = PCB_STAT_OVERTEMP;
1388  0383                   L773:
1389                         ; 1214 			temperature_PCB_OT_cnt = 0;
1391  0383 18790003          	clrw	_temperature_PCB_OT_cnt
1392                         ; 1217 			if (Temp_FailSafe_prms.PCB_OverTemp_Status_c == OVER_TEMP_SET) {				
1394  0387 f60012            	ldab	_Temp_FailSafe_prms+5
1395  038a c101              	cmpb	#1
1396  038c 260c              	bne	L514
1397                         ; 1219 				if (temperature_PCB_ADC < temperature_s.PCB_Clear_OverTemp) {
1399  038e fc001b            	ldd	_temperature_PCB_ADC
1400  0391 bc0019            	cpd	_temperature_s+6
1401  0394 2404              	bhs	L514
1402                         ; 1226 					temperature_PCB_NTC_status_c = PCB_STAT_OVERTEMP;
1404  0396                   LC009:
1405  0396 c603              	ldab	#3
1407  0398 2006              	bra	LC007
1408  039a                   L514:
1409                         ; 1230 					TempF_Clr_PCB_OverTemp();
1412                         ; 1232 					temperature_PCB_NTC_status_c = PCB_STAT_GOOD;
1414                         ; 1237 				TempF_Clr_PCB_OverTemp();
1417                         ; 1240 				temperature_PCB_NTC_status_c = PCB_STAT_GOOD;
1419  039a 4a031f1f          	call	f_TempF_Clr_PCB_OverTemp
1420  039e c604              	ldab	#4
1421  03a0                   LC007:
1422  03a0 7b0008            	stab	_temperature_PCB_NTC_status_c
1423  03a3 2004              	bra	L324
1424  03a5                   L573:
1425                         ; 1246 		temperature_PCB_OT_cnt = 0;		
1427  03a5 18790003          	clrw	_temperature_PCB_OT_cnt
1428  03a9                   L324:
1429                         ; 1249 }
1432  03a9 1b81              	leas	1,s
1433  03ab 0a                	rtc	
1457                         ; 1273 @far u_8Bit TempF_Get_PCB_NTC_Stat(void)
1457                         ; 1274 {
1458                         	switch	.ftext
1459  03ac                   f_TempF_Get_PCB_NTC_Stat:
1463                         ; 1275 	return(temperature_PCB_NTC_status_c);
1465  03ac f60008            	ldab	_temperature_PCB_NTC_status_c
1468  03af 0a                	rtc	
1529                         ; 1293 @far u_8Bit TempF_NtcAdStd_to_CanTemp(u_8Bit NtcAd_c)
1529                         ; 1294 {
1530                         	switch	.ftext
1531  03b0                   f_TempF_NtcAdStd_to_CanTemp:
1533  03b0 3b                	pshd	
1534  03b1 1b9b              	leas	-5,s
1535       00000005          OFST:	set	5
1538                         ; 1296 	u_8Bit k = 0;		   				// stores the found index of the ntcAd reading in our lookup table 
1540  03b3 6980              	clr	OFST-5,s
1541                         ; 1297 	u_16Bit interim_canTemp = 0;		// intermidiate value of the calculated CAN temperature
1543                         ; 1298 	u_8Bit canTemp = 0;					// final result (8-bit value) CAN temp equivalent of ntcAd reading
1545  03b5 6983              	clr	OFST-2,s
1546                         ; 1299 	u_8Bit NtcAd_rdg_c = 0;             // Current NtcAd value
1548                         ; 1301 	NtcAd_rdg_c = NtcAd_c;				// store the current standarized NTC reading
1550  03b7 e686              	ldab	OFST+1,s
1551  03b9 6b84              	stab	OFST-1,s
1552                         ; 1304 	if(NtcAd_rdg_c < NTC_READ[T_REFPOINTS-1]){	
1554  03bb c138              	cmpb	#56
1555  03bd 2406              	bhs	L764
1556                         ; 1307 		canTemp = T_LIMIT;	// return the canTemp as it is 85 C
1558  03bf c6fa              	ldab	#250
1560  03c1 2057              	bra	LC010
1561  03c3                   L564:
1562                         ; 1315 			k++;
1564  03c3 6280              	inc	OFST-5,s
1565  03c5                   L764:
1566                         ; 1312 		while ((NTC_READ[k] > NtcAd_c) && (k < T_REFPOINTS-2))
1568  03c5 e680              	ldab	OFST-5,s
1569  03c7 b796              	exg	b,y
1570  03c9 e6ea0000          	ldab	_NTC_READ,y
1571  03cd e186              	cmpb	OFST+1,s
1572  03cf 2306              	bls	L374
1574  03d1 e680              	ldab	OFST-5,s
1575  03d3 c118              	cmpb	#24
1576  03d5 25ec              	blo	L564
1577  03d7                   L374:
1578                         ; 1319 		if(k != FALSE){
1580  03d7 e780              	tst	OFST-5,s
1581  03d9 2741              	beq	L364
1582                         ; 1321 			k = (k-T_N1);											// adjust the index
1584  03db 6380              	dec	OFST-5,s
1585                         ; 1322 			interim_canTemp = (NTC_READ[k]-NtcAd_c);				// find the difference between table entry and current ntcAd
1587  03dd e680              	ldab	OFST-5,s
1588  03df 87                	clra	
1589  03e0 b746              	tfr	d,y
1590  03e2 e6ea0000          	ldab	_NTC_READ,y
1591  03e6 e086              	subb	OFST+1,s
1592  03e8 8200              	sbca	#0
1593                         ; 1323 			interim_canTemp = (interim_canTemp*T_N10);			    // 16-bit unsigned int for divison
1595  03ea cd000a            	ldy	#10
1596  03ed 13                	emul	
1597  03ee 6c81              	std	OFST-4,s
1598                         ; 1324 			canTemp = (NTC_READ[k] - NTC_READ[k+T_N1]);				// find the difference between next table entry and current ntcAd
1600  03f0 e680              	ldab	OFST-5,s
1601  03f2 87                	clra	
1602  03f3 b746              	tfr	d,y
1603  03f5 b745              	tfr	d,x
1604  03f7 e6e20001          	ldab	_NTC_READ+1,x
1605  03fb e0ea0000          	subb	_NTC_READ,y
1606  03ff 50                	negb	
1607  0400 6b83              	stab	OFST-2,s
1608                         ; 1325 			interim_canTemp = (interim_canTemp + (canTemp/T_N2) );	// add denominator/2 for rounding
1610  0402 54                	lsrb	
1611  0403 e381              	addd	OFST-4,s
1612  0405 6c81              	std	OFST-4,s
1613                         ; 1326 			interim_canTemp = (interim_canTemp/canTemp);			// div: 16-bit/8-bit
1615  0407 e683              	ldab	OFST-2,s
1616  0409 b795              	exg	b,x
1617  040b ec81              	ldd	OFST-4,s
1618  040d 1810              	idiv	
1619  040f 6e81              	stx	OFST-4,s
1620                         ; 1327 			canTemp = (u_8Bit) (k*T_N10);							// next lower CAN temp = index*10 (Maximum 250)
1622  0411 e680              	ldab	OFST-5,s
1623  0413 860a              	ldaa	#10
1624  0415 12                	mul	
1625  0416 6b83              	stab	OFST-2,s
1626                         ; 1328 			canTemp = (u_8Bit) (canTemp+interim_canTemp);			// final canTemp result
1628  0418 eb82              	addb	OFST-3,s
1629  041a                   LC010:
1630  041a 6b83              	stab	OFST-2,s
1632  041c                   L364:
1633                         ; 1335 	return(canTemp);
1635  041c e683              	ldab	OFST-2,s
1638  041e 1b87              	leas	7,s
1639  0420 0a                	rtc	
1700                         ; 1355 @far u_8Bit TempF_CanTemp_to_NtcAdStd(u_8Bit CanTemp_c)
1700                         ; 1356 {
1701                         	switch	.ftext
1702  0421                   f_TempF_CanTemp_to_NtcAdStd:
1704  0421 3b                	pshd	
1705  0422 1b9b              	leas	-5,s
1706       00000005          OFST:	set	5
1709                         ; 1358 	u_8Bit j = 0;		   				// stores the found index of the canTemp reading in our lookup table 
1711                         ; 1359 	u_16Bit interim_ntcAd = 0;			// intermidiate value of the calculated ntcAD temperature
1713                         ; 1360 	u_8Bit ntcAd = 0;					// final result (8-bit value) ntcAd reading equivalent of CAN temp
1715                         ; 1361 	u_8Bit canTemp_rdg_c = 0;           // Current canTemp value
1717                         ; 1363 	canTemp_rdg_c = CanTemp_c;			// store the current canTemp
1719  0424 e686              	ldab	OFST+1,s
1720  0426 6b84              	stab	OFST-1,s
1721                         ; 1365 	j = (u_8Bit) (canTemp_rdg_c/T_N10);	// Find a reference index of the current canTemp
1723  0428 87                	clra	
1724  0429 ce000a            	ldx	#10
1725  042c 1815              	idivs	
1726  042e b754              	tfr	x,d
1727  0430 6b80              	stab	OFST-5,s
1728                         ; 1368 	if(j <= T_REFPOINTS-2){
1730  0432 c118              	cmpb	#24
1731  0434 223b              	bhi	L525
1732                         ; 1370 		ntcAd = (u_8Bit) ( (j+T_N1)*T_N10 );     					// next higher CAN temp Reference point (Maximum 250)
1734  0436 52                	incb	
1735  0437 860a              	ldaa	#10
1736  0439 12                	mul	
1737  043a 6b83              	stab	OFST-2,s
1738                         ; 1371 		interim_ntcAd = (NTC_READ[j]-NTC_READ[j+T_N1]);				// find the difference between the two table lookup values
1740  043c e680              	ldab	OFST-5,s
1741  043e 87                	clra	
1742  043f b746              	tfr	d,y
1743  0441 b745              	tfr	d,x
1744  0443 e6e20000          	ldab	_NTC_READ,x
1745  0447 e0ea0001          	subb	_NTC_READ+1,y
1746  044b 8200              	sbca	#0
1747  044d 6c81              	std	OFST-4,s
1748                         ; 1372 		interim_ntcAd = (interim_ntcAd*(ntcAd - canTemp_rdg_c));    // 16-bit result
1750  044f e683              	ldab	OFST-2,s
1751  0451 87                	clra	
1752  0452 e084              	subb	OFST-1,s
1753  0454 8200              	sbca	#0
1754  0456 ed81              	ldy	OFST-4,s
1755  0458 13                	emul	
1756                         ; 1373 		interim_ntcAd = ((interim_ntcAd+T_N5)/T_N10);         		// for rounding	and making an 8-bit result                                  
1758  0459 c30005            	addd	#5
1759  045c ce000a            	ldx	#10
1760  045f 1810              	idiv	
1761  0461 6e81              	stx	OFST-4,s
1762                         ; 1374 		ntcAd = (u_8Bit) ((NTC_READ[j+1] + interim_ntcAd)) ;		// final ntcAd result
1764  0463 e680              	ldab	OFST-5,s
1765  0465 b796              	exg	b,y
1766  0467 e6ea0001          	ldab	_NTC_READ+1,y
1767  046b eb82              	addb	OFST-3,s
1768  046d 6b83              	stab	OFST-2,s
1770  046f 2002              	bra	L725
1771  0471                   L525:
1772                         ; 1377 		ntcAd = NTC_READ[T_REFPOINTS-1];
1774  0471 c638              	ldab	#56
1775  0473                   L725:
1776                         ; 1379 	return(ntcAd);	
1780  0473 1b87              	leas	7,s
1781  0475 0a                	rtc	
2024                         	xdef	f_TempF_Clr_PCB_OverTemp
2025                         	xdef	f_TempF_Set_PCB_OverTemp
2026                         	xdef	f_TempF_Chck_NTC_SupplyV
2027                         	xdef	f_TempF_Stndrdz_Trmstr_Rdg
2028                         	xdef	f_TempF_Single_Thrmstr_ADC
2029                         	xdef	f_TempF_ADC_SetUp
2030                         	switch	.bss
2031  0000                   _temperature_AD_prms:
2032  0000 000000            	ds.b	3
2033                         	xdef	_temperature_AD_prms
2034                         	xdef	_NTC_READ
2035  0003                   _temperature_PCB_OT_cnt:
2036  0003 0000              	ds.b	2
2037                         	xdef	_temperature_PCB_OT_cnt
2038  0005                   _temperature_HS_ontimeLmt_w:
2039  0005 0000              	ds.b	2
2040                         	xdef	_temperature_HS_ontimeLmt_w
2041  0007                   _temperature_NTC_SupplyV_flt_mtr_cnt:
2042  0007 00                	ds.b	1
2043                         	xdef	_temperature_NTC_SupplyV_flt_mtr_cnt
2044  0008                   _temperature_PCB_NTC_status_c:
2045  0008 00                	ds.b	1
2046                         	xdef	_temperature_PCB_NTC_status_c
2047  0009                   _temperature_hs_thrmstr_ADC:
2048  0009 00000000          	ds.b	4
2049                         	xdef	_temperature_hs_thrmstr_ADC
2050                         	xref	f_EE_BlockRead
2051                         	xref	f_EE_BlockWrite
2052                         	xref	f_FMemLibF_GetOdometer
2053                         	xref	f_IntFlt_F_Update
2054                         	xref	_intFlt_bytes
2055                         	xref	_int_flt_byte2
2056                         	xref	_hs_time_counter_w
2057                         	xref	f_BattVF_AD_to_100mV_Cnvrsn
2058                         	xref	f_BattVF_Get_Status
2059                         	xref	_DioConfigData
2060                         	xref	_Dio_PortWrite_Ptr
2061                         	xref	_ADF_AtoD_Cnvrsn
2062                         	xref	_ADF_Str_UnFlt_Rslt
2063                         	xref	_ADF_Str_UnFlt_Mux_Rslt
2064                         	xref	_ADF_Mux_Filter
2065                         	xref	_ADF_Filter
2066                         	xref	_AD_Filter_set
2067                         	xref	_AD_mux_Filter
2068                         	xref	_main_flag2_c
2069                         	xref	_canio_RX_ExternalTemperature_c
2070                         	xref	_canio_RX_IGN_STATUS_c
2071                         	xdef	f_TempF_CanTemp_to_NtcAdStd
2072                         	xdef	f_TempF_NtcAdStd_to_CanTemp
2073                         	xdef	f_TempF_Get_NTC_Rdg
2074                         	xdef	f_TempF_U12S_ADC
2075                         	xdef	f_TempF_Get_PCB_NTC_Stat
2076                         	xdef	f_TempF_PCB_NTC_Flt_Monitor
2077                         	xdef	f_TempF_HS_NTC_Flt_Monitor
2078                         	xdef	f_TempF_Monitor_ADC
2079                         	xdef	f_TempF_PCB_ADC
2080                         	xdef	f_TempF_Init
2081  000d                   _Temp_FailSafe_prms:
2082  000d 000000000000      	ds.b	6
2083                         	xdef	_Temp_FailSafe_prms
2084  0013                   _temperature_s:
2085  0013 0000000000000000  	ds.b	8
2086                         	xdef	_temperature_s
2087  001b                   _temperature_PCB_ADC:
2088  001b 0000              	ds.b	2
2089                         	xdef	_temperature_PCB_ADC
2090  001d                   _temperature_HS_NTC_status:
2091  001d 00000000          	ds.b	4
2092                         	xdef	_temperature_HS_NTC_status
2093  0021                   _temperature_NTC_supplyV:
2094  0021 00                	ds.b	1
2095                         	xdef	_temperature_NTC_supplyV
2116                         	end
