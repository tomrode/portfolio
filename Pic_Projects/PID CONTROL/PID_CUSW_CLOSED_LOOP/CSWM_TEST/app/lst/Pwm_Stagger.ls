   1                         ; C Compiler for S12XE [COSMIC Software].
   2                         ; Generator V4.7.8 - 27 Jul 2007
   3                         ; Optimizer V4.6.13 - 27 Jul 2007
   4                         	switch	.data
   5  0000                   L3_waited:
   6  0000 00                	dc.b	0
 100                         ; 98 @far u_8Bit StaggerF_Chck_Existing_Ch(Stagger_Channel_Type Channel_ID)
 100                         ; 99 {
 101                         .ftext:	section	.text
 102  0000                   f_StaggerF_Chck_Existing_Ch:
 104  0000 3b                	pshd	
 105  0001 3b                	pshd	
 106       00000002          OFST:	set	2
 109                         ; 101 	u_8Bit chck_result = FALSE;        // existing channel check result
 111                         ; 104 	for (i=0; i<HS_SIZE; i++) {
 113  0002 87                	clra	
 114  0003 c7                	clrb	
 115  0004 6c80              	std	OFST-2,s
 116  0006 180e              	tab	
 117  0008                   L15:
 118                         ; 107 		if (stagger_s[i].channel == Channel_ID) {
 120  0008 87                	clra	
 121  0009 cd0003            	ldy	#3
 122  000c 13                	emul	
 123  000d b746              	tfr	d,y
 124  000f e6ea0004          	ldab	_stagger_s,y
 125  0013 e183              	cmpb	OFST+1,s
 126  0015 2608              	bne	L16
 127                         ; 110 			chck_result = TRUE;			
 129  0017 c601              	ldab	#1
 130  0019 6b81              	stab	OFST-1,s
 131                         ; 113 			i = HS_SIZE;
 133  001b c604              	ldab	#4
 134  001d 6b80              	stab	OFST-2,s
 136  001f                   L16:
 137                         ; 104 	for (i=0; i<HS_SIZE; i++) {
 139  001f 6280              	inc	OFST-2,s
 142  0021 e680              	ldab	OFST-2,s
 143  0023 c104              	cmpb	#4
 144  0025 25e1              	blo	L15
 145                         ; 119 	return(chck_result);
 147  0027 e681              	ldab	OFST-1,s
 150  0029 1b84              	leas	4,s
 151  002b 0a                	rtc	
 201                         ; 144 @far u_8Bit StaggerF_Find_Index(Stagger_Channel_Type Channel_ID)
 201                         ; 145 {
 202                         	switch	.ftext
 203  002c                   f_StaggerF_Find_Index:
 205  002c 3b                	pshd	
 206  002d 3b                	pshd	
 207       00000002          OFST:	set	2
 210                         ; 146 	u_8Bit location = INVALID_CH_INDEX;   // found index
 212  002e c604              	ldab	#4
 213  0030 6b81              	stab	OFST-1,s
 214                         ; 149 	for (p=0; p<HS_SIZE; p++) {
 216  0032 c7                	clrb	
 217  0033 6b80              	stab	OFST-2,s
 218  0035                   L501:
 219                         ; 152 		if (stagger_s[p].channel == Channel_ID) {
 221  0035 87                	clra	
 222  0036 cd0003            	ldy	#3
 223  0039 13                	emul	
 224  003a b746              	tfr	d,y
 225  003c e6ea0004          	ldab	_stagger_s,y
 226  0040 e183              	cmpb	OFST+1,s
 227  0042 2608              	bne	L511
 228                         ; 155 			location = p;
 230  0044 180a8081          	movb	OFST-2,s,OFST-1,s
 231                         ; 158 			p = HS_SIZE;
 233  0048 c604              	ldab	#4
 234  004a 6b80              	stab	OFST-2,s
 236  004c                   L511:
 237                         ; 149 	for (p=0; p<HS_SIZE; p++) {
 239  004c 6280              	inc	OFST-2,s
 242  004e e680              	ldab	OFST-2,s
 243  0050 c104              	cmpb	#4
 244  0052 25e1              	blo	L501
 245                         ; 164 	return(location);
 247  0054 e681              	ldab	OFST-1,s
 250  0056 1b84              	leas	4,s
 251  0058 0a                	rtc	
 276                         ; 190 @far void StaggerF_Turn_Off_All_Ch(void)
 276                         ; 191 {
 277                         	switch	.ftext
 278  0059                   f_StaggerF_Turn_Off_All_Ch:
 282                         ; 193 	if (hs_allowed_heat_pwm_c[FRONT_LEFT] != N100) {
 284  0059 f60000            	ldab	_hs_allowed_heat_pwm_c
 285  005c c164              	cmpb	#100
 286  005e 2707              	beq	L131
 287                         ; 196 		Pwm_DisableNotification(PWM_HS_FL);
 289  0060 cc0003            	ldd	#3
 290  0063 4a000000          	call	f_Pwm_DisableNotification
 293  0067                   L131:
 294                         ; 204 	if (hs_allowed_heat_pwm_c[FRONT_RIGHT] != N100) {
 296  0067 f60001            	ldab	_hs_allowed_heat_pwm_c+1
 297  006a c164              	cmpb	#100
 298  006c 2707              	beq	L531
 299                         ; 207 		Pwm_DisableNotification(PWM_HS_FR);
 301  006e cc0004            	ldd	#4
 302  0071 4a000000          	call	f_Pwm_DisableNotification
 305  0075                   L531:
 306                         ; 215 	if (hs_allowed_heat_pwm_c[REAR_LEFT] != N100) {
 308  0075 f60002            	ldab	_hs_allowed_heat_pwm_c+2
 309  0078 c164              	cmpb	#100
 310  007a 2707              	beq	L141
 311                         ; 218 		Pwm_DisableNotification(PWM_HS_RL);
 313  007c cc0005            	ldd	#5
 314  007f 4a000000          	call	f_Pwm_DisableNotification
 317  0083                   L141:
 318                         ; 226 	if (hs_allowed_heat_pwm_c[REAR_RIGHT] != N100) {
 320  0083 f60003            	ldab	_hs_allowed_heat_pwm_c+3
 321  0086 c164              	cmpb	#100
 322  0088 2707              	beq	L541
 323                         ; 229 		Pwm_DisableNotification(PWM_HS_RR);
 325  008a cc0006            	ldd	#6
 326  008d 4a000000          	call	f_Pwm_DisableNotification
 329  0091                   L541:
 330                         ; 235 }
 333  0091 0a                	rtc	
 398                         ; 261 @far void StaggerF_Add_Channel(Stagger_Channel_Type Channel_ID, u_16Bit duty_cycle)
 398                         ; 262 {
 399                         	switch	.ftext
 400  0092                   f_StaggerF_Add_Channel:
 402  0092 3b                	pshd	
 403  0093 1b9c              	leas	-4,s
 404       00000004          OFST:	set	4
 407                         ; 264 	u_16Bit  pwm_cycle = duty_cycle; // new channel pwm duty cycle
 409  0095 18028982          	movw	OFST+5,s,OFST-2,s
 410                         ; 265 	u_8Bit added = FALSE;            // channel is succesfully added indicator
 412                         ; 267 	for (b=0; b<HS_SIZE; b++) {
 414  0099 87                	clra	
 415  009a c7                	clrb	
 416  009b 6c80              	std	OFST-4,s
 417  009d 180e              	tab	
 418  009f                   L571:
 419                         ; 270 		if ( (stagger_flags1[b].b.ch_occupied == FALSE) && (added == FALSE) ) {
 421  009f 87                	clra	
 422  00a0 b746              	tfr	d,y
 423  00a2 0eea0000012a      	brset	_stagger_flags1,y,1,L302
 425  00a8 e781              	tst	OFST-3,s
 426  00aa 2626              	bne	L302
 427                         ; 273 			stagger_s[b].channel = Channel_ID;
 429  00ac cd0003            	ldy	#3
 430  00af 13                	emul	
 431  00b0 b746              	tfr	d,y
 432  00b2 180a85ea0004      	movb	OFST+1,s,_stagger_s,y
 433                         ; 274 			stagger_s[b].pwm = pwm_cycle;
 435  00b8 180282ea0005      	movw	OFST-2,s,_stagger_s+1,y
 436                         ; 277 			stagger_flags1[b].b.ch_occupied = TRUE;
 438  00be e680              	ldab	OFST-4,s
 439  00c0 b796              	exg	b,y
 440  00c2 0dea000002        	bclr	_stagger_flags1,y,2
 441  00c7 0cea000001        	bset	_stagger_flags1,y,1
 442                         ; 278 			stagger_flags1[b].b.ch_activated = FALSE;
 444                         ; 281 			added = TRUE;
 446  00cc c601              	ldab	#1
 447  00ce 6b81              	stab	OFST-3,s
 449  00d0 2007              	bra	L502
 450  00d2                   L302:
 451                         ; 285 			stagger_flags1[b].b.ch_activated = FALSE;
 453  00d2 b796              	exg	b,y
 454  00d4 0dea000002        	bclr	_stagger_flags1,y,2
 455  00d9                   L502:
 456                         ; 267 	for (b=0; b<HS_SIZE; b++) {
 458  00d9 6280              	inc	OFST-4,s
 461  00db e680              	ldab	OFST-4,s
 462  00dd c104              	cmpb	#4
 463  00df 25be              	blo	L571
 464                         ; 288 }
 467  00e1 1b86              	leas	6,s
 468  00e3 0a                	rtc	
 532                         ; 311 @far void StaggerF_Update_Ch(Stagger_Channel_Type Channel_ID, u_16Bit new_pwm)
 532                         ; 312 {
 533                         	switch	.ftext
 534  00e4                   f_StaggerF_Update_Ch:
 536  00e4 3b                	pshd	
 537  00e5 1b9c              	leas	-4,s
 538       00000004          OFST:	set	4
 541                         ; 313 	u_16Bit  updated_pwm = new_pwm;  // new pwm duty cycle
 543  00e7 18028982          	movw	OFST+5,s,OFST-2,s
 544                         ; 314 	u_8Bit updated = FALSE;          // channel is updated succesfully
 546                         ; 317 	for (c=0; c<HS_SIZE; c++) {
 548  00eb 87                	clra	
 549  00ec c7                	clrb	
 550  00ed 6c80              	std	OFST-4,s
 551  00ef 180e              	tab	
 552  00f1                   L532:
 553                         ; 320 		if ( (stagger_s[c].channel == Channel_ID) && (updated == FALSE) ) {
 555  00f1 87                	clra	
 556  00f2 cd0003            	ldy	#3
 557  00f5 13                	emul	
 558  00f6 b746              	tfr	d,y
 559  00f8 e6ea0004          	ldab	_stagger_s,y
 560  00fc e185              	cmpb	OFST+1,s
 561  00fe 261e              	bne	L342
 563  0100 e681              	ldab	OFST-3,s
 564  0102 261a              	bne	L342
 565                         ; 323 			stagger_s[c].pwm = updated_pwm;
 567  0104 180282ea0005      	movw	OFST-2,s,_stagger_s+1,y
 568                         ; 326 			stagger_flags1[c].b.ch_occupied = TRUE;
 570  010a e680              	ldab	OFST-4,s
 571  010c b796              	exg	b,y
 572  010e 0dea000002        	bclr	_stagger_flags1,y,2
 573  0113 0cea000001        	bset	_stagger_flags1,y,1
 574                         ; 327 			stagger_flags1[c].b.ch_activated = FALSE;
 576                         ; 330 			updated = TRUE;
 578  0118 c601              	ldab	#1
 579  011a 6b81              	stab	OFST-3,s
 581  011c 2009              	bra	L542
 582  011e                   L342:
 583                         ; 334 			stagger_flags1[c].b.ch_activated = FALSE;
 585  011e e680              	ldab	OFST-4,s
 586  0120 b796              	exg	b,y
 587  0122 0dea000002        	bclr	_stagger_flags1,y,2
 588  0127                   L542:
 589                         ; 317 	for (c=0; c<HS_SIZE; c++) {
 591  0127 6280              	inc	OFST-4,s
 594  0129 e680              	ldab	OFST-4,s
 595  012b c104              	cmpb	#4
 596  012d 25c2              	blo	L532
 597                         ; 337 }
 600  012f 1b86              	leas	6,s
 601  0131 0a                	rtc	
 655                         ; 365 @far void StaggerF_Delete_Ch(Stagger_Channel_Type Channel_ID)
 655                         ; 366 {
 656                         	switch	.ftext
 657  0132                   f_StaggerF_Delete_Ch:
 659  0132 3b                	pshd	
 660  0133 37                	pshb	
 661       00000001          OFST:	set	1
 664                         ; 367 	u_8Bit existing_channel = 0;         // existing channel result (true or false)
 666                         ; 371 	existing_channel = StaggerF_Chck_Existing_Ch(Channel_ID);
 668  0134 87                	clra	
 669  0135 4a000000          	call	f_StaggerF_Chck_Existing_Ch
 671  0139 6b80              	stab	OFST-1,s
 672                         ; 374 	if (existing_channel == TRUE) {
 674  013b 53                	decb	
 675  013c 18260094          	bne	L172
 676                         ; 377 		StaggerF_Turn_Off_All_Ch();
 678  0140 4a005959          	call	f_StaggerF_Turn_Off_All_Ch
 680                         ; 380 		for (k=0; k<(HS_SIZE-1); k++) {
 682  0144 c7                	clrb	
 683  0145 6b80              	stab	OFST-1,s
 684  0147                   L372:
 685                         ; 383 			if (stagger_s[k].channel == Channel_ID) {
 687  0147 87                	clra	
 688  0148 cd0003            	ldy	#3
 689  014b 13                	emul	
 690  014c b746              	tfr	d,y
 691  014e e6ea0004          	ldab	_stagger_s,y
 692  0152 e182              	cmpb	OFST+1,s
 693  0154 2643              	bne	L103
 695  0156 2039              	bra	L703
 696  0158                   L303:
 697                         ; 388 					stagger_s[k].channel = stagger_s[k+1].channel;
 699  0158 87                	clra	
 700  0159 cd0003            	ldy	#3
 701  015c 13                	emul	
 702  015d b746              	tfr	d,y
 703  015f 180aea0007ea0004  	movb	_stagger_s+3,y,_stagger_s,y
 704                         ; 389 					stagger_s[k].pwm = stagger_s[k+1].pwm;
 706  0167 1802ea0008ea0005  	movw	_stagger_s+4,y,_stagger_s+1,y
 707                         ; 392 					stagger_flags1[k].b.ch_occupied = stagger_flags1[k+1].b.ch_occupied;
 709  016f e680              	ldab	OFST-1,s
 710  0171 87                	clra	
 711  0172 b746              	tfr	d,y
 712  0174 b745              	tfr	d,x
 713  0176 0eea00010107      	brset	_stagger_flags1+1,y,1,L02
 714  017c 0de2000001        	bclr	_stagger_flags1,x,1
 715  0181 2005              	bra	L22
 716  0183                   L02:
 717  0183 0ce2000001        	bset	_stagger_flags1,x,1
 718  0188                   L22:
 719                         ; 393 					stagger_flags1[k].b.ch_activated = FALSE;
 721  0188 b796              	exg	b,y
 722  018a 0dea000002        	bclr	_stagger_flags1,y,2
 723                         ; 385 				for (; k<(HS_SIZE-1); k++) {
 725  018f 6280              	inc	OFST-1,s
 726  0191                   L703:
 729  0191 e680              	ldab	OFST-1,s
 730  0193 c103              	cmpb	#3
 731  0195 25c1              	blo	L303
 733  0197 2009              	bra	L313
 734  0199                   L103:
 735                         ; 398 				stagger_flags1[k].b.ch_activated = FALSE;
 737  0199 e680              	ldab	OFST-1,s
 738  019b b796              	exg	b,y
 739  019d 0dea000002        	bclr	_stagger_flags1,y,2
 740  01a2                   L313:
 741                         ; 380 		for (k=0; k<(HS_SIZE-1); k++) {
 743  01a2 6280              	inc	OFST-1,s
 746  01a4 e680              	ldab	OFST-1,s
 747  01a6 c103              	cmpb	#3
 748  01a8 259d              	blo	L372
 749                         ; 403 		stagger_s[HS_SIZE-1].channel = 0;
 751  01aa 79000d            	clr	_stagger_s+9
 752                         ; 404 		stagger_s[HS_SIZE-1].pwm = 0;
 754  01ad 1879000e          	clrw	_stagger_s+10
 755                         ; 407 		stagger_flags1[HS_SIZE-1].b.ch_occupied = FALSE;
 757                         ; 408 		stagger_flags1[HS_SIZE-1].b.ch_activated = FALSE;
 759  01b1 1d000303          	bclr	_stagger_flags1+3,3
 760                         ; 411 		if (stagger_flags1[0].b.ch_occupied == TRUE) {
 762  01b5 1f00000125        	brclr	_stagger_flags1,1,L123
 763                         ; 414 			stagger_flags1[0].b.ch_activated = TRUE;
 765  01ba 1c000002          	bset	_stagger_flags1,2
 766                         ; 415 			Pwm_EnableNotification(stagger_s[0].channel, PWM_FALLING_EDGE);
 768  01be cc0002            	ldd	#2
 769  01c1 3b                	pshd	
 770  01c2 f60004            	ldab	_stagger_s
 771  01c5 4a000000          	call	f_Pwm_EnableNotification
 773                         ; 416 			Pwm_SetDutyCycle(stagger_s[0].channel, stagger_s[0].pwm);
 775  01c9 1801800005        	movw	_stagger_s+1,0,s
 776  01ce f60004            	ldab	_stagger_s
 777  01d1 87                	clra	
 780  01d2 2005              	bra	LC001
 781  01d4                   L172:
 782                         ; 424 		Pwm_SetDutyCycle(Channel_ID, 0);
 784  01d4 87                	clra	
 785  01d5 c7                	clrb	
 786  01d6 3b                	pshd	
 787  01d7 e684              	ldab	OFST+3,s
 789  01d9                   LC001:
 790  01d9 4a000000          	call	f_Pwm_SetDutyCycle
 791  01dd 1b82              	leas	2,s
 792  01df                   L123:
 793                         ; 426 }
 796  01df 1b83              	leas	3,s
 797  01e1 0a                	rtc	
 874                         ; 455 @far u_16Bit StaggerF_Get_EctTime(u_8Bit pwm_channel, Stagger_EctTime_Type ectTime_type)
 874                         ; 456 {
 875                         	switch	.ftext
 876  01e2                   f_StaggerF_Get_EctTime:
 878  01e2 3b                	pshd	
 879  01e3 1b9b              	leas	-5,s
 880       00000005          OFST:	set	5
 883                         ; 457 	u_8Bit selected_channel = pwm_channel;  // selected channel number (heated seat output)
 885  01e5 6b84              	stab	OFST-1,s
 886                         ; 458 	u_16Bit Time_w = 0;                     // return value of the function (On or Off time)
 888  01e7 186982            	clrw	OFST-3,s
 889                         ; 460 	switch (selected_channel) {
 892  01ea c003              	subb	#3
 893  01ec 270d              	beq	L323
 894  01ee 040119            	dbeq	b,L523
 895  01f1 040125            	dbeq	b,L723
 896  01f4 040134            	dbeq	b,L133
 897  01f7 ec82              	ldd	OFST-3,s
 898  01f9 2047              	bra	LC002
 899  01fb                   L323:
 900                         ; 464 			if (ectTime_type == ON_TIME) {				
 902  01fb e68b              	ldab	OFST+6,s
 903  01fd 042105            	dbne	b,L573
 904                         ; 466 				Time_w = (N250 * hs_allowed_heat_pwm_c[FRONT_LEFT]);
 906  0200 f60000            	ldab	_hs_allowed_heat_pwm_c
 908  0203 201c              	bra	LC004
 909  0205                   L573:
 910                         ; 470 				Time_w = (CH_PERIOD - (N250 * hs_allowed_heat_pwm_c[FRONT_LEFT]));
 912  0205 f60000            	ldab	_hs_allowed_heat_pwm_c
 913  0208 202e              	bra	LC003
 914  020a                   L523:
 915                         ; 477 			if (ectTime_type == ON_TIME) {				
 917  020a e68b              	ldab	OFST+6,s
 918  020c 042105            	dbne	b,L104
 919                         ; 479 				Time_w = (N250 * hs_allowed_heat_pwm_c[FRONT_RIGHT]);
 921  020f f60001            	ldab	_hs_allowed_heat_pwm_c+1
 923  0212 200d              	bra	LC004
 924  0214                   L104:
 925                         ; 483 				Time_w = (CH_PERIOD - (N250 * hs_allowed_heat_pwm_c[FRONT_RIGHT]));
 927  0214 f60001            	ldab	_hs_allowed_heat_pwm_c+1
 928  0217 201f              	bra	LC003
 929  0219                   L723:
 930                         ; 490 			if (ectTime_type == ON_TIME) {				
 932  0219 e68b              	ldab	OFST+6,s
 933  021b 042108            	dbne	b,L504
 934                         ; 492 				Time_w = (N250 * hs_allowed_heat_pwm_c[REAR_LEFT]);
 936  021e f60002            	ldab	_hs_allowed_heat_pwm_c+2
 937  0221                   LC004:
 938  0221 86fa              	ldaa	#250
 939  0223 12                	mul	
 941  0224 201c              	bra	LC002
 942  0226                   L504:
 943                         ; 496 				Time_w = (CH_PERIOD - (N250 * hs_allowed_heat_pwm_c[REAR_LEFT]));
 945  0226 f60002            	ldab	_hs_allowed_heat_pwm_c+2
 946  0229 200d              	bra	LC003
 947  022b                   L133:
 948                         ; 503 			if (ectTime_type == ON_TIME) {				
 950  022b e68b              	ldab	OFST+6,s
 951  022d 042105            	dbne	b,L114
 952                         ; 505 				Time_w = (N250 * hs_allowed_heat_pwm_c[REAR_RIGHT]);
 954  0230 f60003            	ldab	_hs_allowed_heat_pwm_c+3
 956  0233 20ec              	bra	LC004
 957  0235                   L114:
 958                         ; 509 				Time_w = (CH_PERIOD - (N250 * hs_allowed_heat_pwm_c[REAR_RIGHT]));
 960  0235 f60003            	ldab	_hs_allowed_heat_pwm_c+3
 961  0238                   LC003:
 962  0238 86fa              	ldaa	#250
 963  023a 12                	mul	
 964  023b 6c80              	std	OFST-5,s
 965  023d cc61a8            	ldd	#25000
 966  0240 a380              	subd	OFST-5,s
 967  0242                   LC002:
 968                         ; 520 	return(Time_w);
 972  0242 1b87              	leas	7,s
 973  0244 0a                	rtc	
1078                         ; 549 @far void StaggerF_Adjust_Tcx(u_8Bit present_index, u_16Bit time_difference, Stagger_Operation_Type TCx_Operation)
1078                         ; 550 {
1079                         	switch	.ftext
1080  0245                   f_StaggerF_Adjust_Tcx:
1082  0245 3b                	pshd	
1083  0246 1b97              	leas	-9,s
1084       00000009          OFST:	set	9
1087                         ; 551 	u_8Bit this_index = present_index;     // channel index in the stagger data structure array
1089  0248 6b88              	stab	OFST-1,s
1090                         ; 552 	u_16Bit tm_offset = time_difference;     // time offset calculated in calling function 
1092  024a 18028e84          	movw	OFST+5,s,OFST-5,s
1093                         ; 553 	volatile u_16Bit *TC_This_Ch_p = (u_16Bit*)(&(TC2)); // Current TC Register Pointer
1095  024e cc0000            	ldd	#__TC2
1096  0251 6c82              	std	OFST-7,s
1097                         ; 554 	volatile u_16Bit *TC_Next_Ch_p = (u_16Bit*)(&(TC2)); // Next TC Register Pointer
1099  0253 6c86              	std	OFST-3,s
1100                         ; 557 	if (this_index < (HS_SIZE-1)) {
1102  0255 e688              	ldab	OFST-1,s
1103  0257 c103              	cmpb	#3
1104  0259 2450              	bhs	L574
1105                         ; 560 		TC_This_Ch_p += ((u_8Bit)stagger_s[this_index].channel - TC_REG_OFFSET);
1107  025b 87                	clra	
1108  025c cd0003            	ldy	#3
1109  025f 13                	emul	
1110  0260 b746              	tfr	d,y
1111  0262 e6ea0004          	ldab	_stagger_s,y
1112  0266 87                	clra	
1113  0267 b745              	tfr	d,x
1114  0269 1a1e              	leax	-2,x
1115  026b 1848              	lslx	
1116  026d 18ab82            	addx	OFST-7,s
1117  0270 6e82              	stx	OFST-7,s
1118                         ; 561 		TC_Next_Ch_p += ((u_8Bit)stagger_s[this_index+1].channel - TC_REG_OFFSET);
1120  0272 e6ea0007          	ldab	_stagger_s+3,y
1121  0276 b745              	tfr	d,x
1122  0278 1a1e              	leax	-2,x
1123  027a 1848              	lslx	
1124  027c 18ab86            	addx	OFST-3,s
1125  027f 6e86              	stx	OFST-3,s
1126                         ; 564 		if (TCx_Operation == SUBTRACT) {
1128  0281 e6f011            	ldab	OFST+8,s
1129  0284 261d              	bne	L564
1130                         ; 567 			if ((*TC_This_Ch_p) >= tm_offset) {				
1132  0286 ed82              	ldy	OFST-7,s
1133  0288 ec40              	ldd	0,y
1134  028a ac84              	cpd	OFST-5,s
1135  028c 2506              	blo	L764
1136                         ; 569 				(*TC_Next_Ch_p) = (u_16Bit) (((*TC_This_Ch_p) - tm_offset));
1138  028e ec40              	ldd	0,y
1139  0290 a384              	subd	OFST-5,s
1141  0292 2015              	bra	LC005
1142  0294                   L764:
1143                         ; 573 				(*TC_Next_Ch_p) = (u_16Bit)(MAX_COUNT - (tm_offset-(*TC_This_Ch_p)));
1145  0294 ec84              	ldd	OFST-5,s
1146  0296 a3f30002          	subd	[OFST-7,s]
1147  029a 6c80              	std	OFST-9,s
1148  029c ccffff            	ldd	#-1
1149  029f a380              	subd	OFST-9,s
1150  02a1 2006              	bra	LC005
1151  02a3                   L564:
1152                         ; 578 			(*TC_Next_Ch_p) = (u_16Bit) ((*TC_This_Ch_p) + tm_offset);
1154  02a3 ecf30002          	ldd	[OFST-7,s]
1155  02a7 e384              	addd	OFST-5,s
1156  02a9                   LC005:
1157  02a9 6c00              	std	0,x
1158  02ab                   L574:
1159                         ; 584 }
1162  02ab 1b8b              	leas	11,s
1163  02ad 0a                	rtc	
1245                         ; 637 @far void StaggerF_Enable_Next_Ch(Stagger_Channel_Type Channel_ID)
1245                         ; 638 {	
1246                         	switch	.ftext
1247  02ae                   f_StaggerF_Enable_Next_Ch:
1249  02ae 3b                	pshd	
1250  02af 1b98              	leas	-8,s
1251       00000008          OFST:	set	8
1254                         ; 640 	u_8Bit found_index = INVALID_CH_INDEX;        // channel index
1256                         ; 641 	u_16Bit present_ch_OffTime  = 0;              // Present channel Ect off Time
1258                         ; 642 	u_16Bit next_ch_OffTime     = 0;              // Next channel Ect off Time
1260                         ; 643 	u_16Bit timeDifference      = 0;              // TCx offset
1262                         ; 644 	Stagger_Operation_Type Operation = SUBTRACT;  // TCx Operation
1264  02b1 87                	clra	
1265  02b2 6a87              	staa	OFST-1,s
1266                         ; 647 	found_index = StaggerF_Find_Index(Channel_ID);
1268  02b4 e689              	ldab	OFST+1,s
1269  02b6 4a002c2c          	call	f_StaggerF_Find_Index
1271  02ba 6b82              	stab	OFST-6,s
1272                         ; 650 	if (waited == TRUE) {
1274  02bc f60000            	ldab	L3_waited
1275  02bf 53                	decb	
1276  02c0 18260109          	bne	L135
1277                         ; 653 		waited = FALSE;
1279  02c4 87                	clra	
1280  02c5 7a0000            	staa	L3_waited
1281                         ; 656 		if ( (stagger_flags1[found_index+1].b.ch_occupied == TRUE) && (stagger_flags1[found_index+1].b.ch_activated == FALSE) && (found_index < (HS_SIZE-1)) ) {
1283  02c8 e682              	ldab	OFST-6,s
1284  02ca b746              	tfr	d,y
1285  02cc e6ea0001          	ldab	_stagger_flags1+1,y
1286  02d0 c501              	bitb	#1
1287  02d2 182700db          	beq	L335
1289  02d6 c502              	bitb	#2
1290  02d8 182600d5          	bne	L335
1292  02dc e682              	ldab	OFST-6,s
1293  02de c103              	cmpb	#3
1294  02e0 182400cd          	bhs	L335
1295                         ; 659 			present_ch_OffTime = StaggerF_Get_EctTime((u_8Bit)stagger_s[found_index].channel, OFF_TIME);
1297  02e4 c7                	clrb	
1298  02e5 3b                	pshd	
1299  02e6 e684              	ldab	OFST-4,s
1300  02e8 cd0003            	ldy	#3
1301  02eb 13                	emul	
1302  02ec b746              	tfr	d,y
1303  02ee e6ea0004          	ldab	_stagger_s,y
1304  02f2 87                	clra	
1305  02f3 4a01e2e2          	call	f_StaggerF_Get_EctTime
1307  02f7 1b82              	leas	2,s
1308  02f9 6c83              	std	OFST-5,s
1309                         ; 662 			next_ch_OffTime = StaggerF_Get_EctTime((u_8Bit)stagger_s[found_index+1].channel, OFF_TIME);
1311  02fb 87                	clra	
1312  02fc c7                	clrb	
1313  02fd 3b                	pshd	
1314  02fe e684              	ldab	OFST-4,s
1315  0300 cd0003            	ldy	#3
1316  0303 13                	emul	
1317  0304 b746              	tfr	d,y
1318  0306 e6ea0007          	ldab	_stagger_s+3,y
1319  030a 87                	clra	
1320  030b 4a01e2e2          	call	f_StaggerF_Get_EctTime
1322  030f 1b82              	leas	2,s
1323  0311 6c85              	std	OFST-3,s
1324                         ; 665 			if ( (present_ch_OffTime >= CH_HALF_PERIOD) && (next_ch_OffTime >= CH_HALF_PERIOD) ) {
1326  0313 ec83              	ldd	OFST-5,s
1327  0315 8c30d4            	cpd	#12500
1328  0318 2509              	blo	L535
1330  031a ec85              	ldd	OFST-3,s
1331  031c 8c30d4            	cpd	#12500
1332                         ; 670 				timeDifference = ( present_ch_OffTime - (CH_PERIOD-next_ch_OffTime) );
1335  031f 242f              	bhs	L545
1336  0321 ec83              	ldd	OFST-5,s
1337  0323                   L535:
1338                         ; 674 				if ( (present_ch_OffTime < CH_HALF_PERIOD) && (next_ch_OffTime < CH_HALF_PERIOD) ) {
1340  0323 8c30d4            	cpd	#12500
1341  0326 2416              	bhs	L145
1343  0328 ec85              	ldd	OFST-3,s
1344  032a 8c30d4            	cpd	#12500
1345  032d 240f              	bhs	L145
1346                         ; 679 					timeDifference = ((CH_PERIOD-present_ch_OffTime)- next_ch_OffTime);
1348  032f cc61a8            	ldd	#25000
1349  0332 a383              	subd	OFST-5,s
1350  0334 a385              	subd	OFST-3,s
1351                         ; 682 					Operation = ADD;
1353  0336                   LC007:
1354  0336 6c83              	std	OFST-5,s
1355  0338 c601              	ldab	#1
1356  033a 6b87              	stab	OFST-1,s
1358  033c 2021              	bra	L735
1359  033e                   L145:
1360                         ; 687 					if (present_ch_OffTime < (CH_PERIOD-next_ch_OffTime) ) {
1362  033e cc61a8            	ldd	#25000
1363  0341 a385              	subd	OFST-3,s
1364  0343 ac83              	cpd	OFST-5,s
1365  0345 2309              	bls	L545
1366                         ; 693 						timeDifference = ( (CH_PERIOD-next_ch_OffTime) - present_ch_OffTime );
1368  0347 cc61a8            	ldd	#25000
1369  034a a385              	subd	OFST-3,s
1370  034c a383              	subd	OFST-5,s
1371                         ; 696 						Operation = ADD;
1374  034e 20e6              	bra	LC007
1375  0350                   L545:
1376                         ; 702 						timeDifference = ( present_ch_OffTime - (CH_PERIOD-next_ch_OffTime) );
1378  0350 cc61a8            	ldd	#25000
1379  0353 a385              	subd	OFST-3,s
1380  0355 6c80              	std	OFST-8,s
1381  0357 ec83              	ldd	OFST-5,s
1382  0359 a380              	subd	OFST-8,s
1383  035b 6c83              	std	OFST-5,s
1384  035d e687              	ldab	OFST-1,s
1385  035f                   L735:
1386                         ; 707 			StaggerF_Adjust_Tcx(found_index, timeDifference, Operation);
1388  035f 87                	clra	
1389  0360 3b                	pshd	
1390  0361 ec85              	ldd	OFST-3,s
1391  0363 3b                	pshd	
1392  0364 e686              	ldab	OFST-2,s
1393  0366 87                	clra	
1394  0367 4a024545          	call	f_StaggerF_Adjust_Tcx
1396  036b 1b84              	leas	4,s
1397                         ; 710 			stagger_flags1[found_index+1].b.ch_activated = TRUE;
1399  036d e682              	ldab	OFST-6,s
1400  036f 87                	clra	
1401  0370 b746              	tfr	d,y
1402  0372 0cea000102        	bset	_stagger_flags1+1,y,2
1403                         ; 711 			Pwm_EnableNotification(stagger_s[found_index+1].channel, PWM_FALLING_EDGE);
1405  0377 c602              	ldab	#2
1406  0379 3b                	pshd	
1407  037a e684              	ldab	OFST-4,s
1408  037c cd0003            	ldy	#3
1409  037f 13                	emul	
1410  0380 b746              	tfr	d,y
1411  0382 e6ea0007          	ldab	_stagger_s+3,y
1412  0386 87                	clra	
1413  0387 4a000000          	call	f_Pwm_EnableNotification
1415  038b 1b82              	leas	2,s
1416                         ; 714 			Pwm_SetDutyCycle(stagger_s[found_index+1].channel, stagger_s[found_index+1].pwm);
1418  038d e682              	ldab	OFST-6,s
1419  038f 87                	clra	
1420  0390 cd0003            	ldy	#3
1421  0393 13                	emul	
1422  0394 b746              	tfr	d,y
1423  0396 ecea0008          	ldd	_stagger_s+4,y
1424  039a 3b                	pshd	
1425  039b e684              	ldab	OFST-4,s
1426  039d 87                	clra	
1427  039e cd0003            	ldy	#3
1428  03a1 13                	emul	
1429  03a2 b746              	tfr	d,y
1430  03a4 e6ea0007          	ldab	_stagger_s+3,y
1431  03a8 87                	clra	
1432  03a9 4a000000          	call	f_Pwm_SetDutyCycle
1434  03ad 1b82              	leas	2,s
1436  03af 2021              	bra	L555
1437  03b1                   L335:
1438                         ; 718 			if (stagger_flags1[found_index].b.ch_activated == TRUE) {
1440  03b1 e682              	ldab	OFST-6,s
1441  03b3 87                	clra	
1442  03b4 b746              	tfr	d,y
1443  03b6 0fea00000216      	brclr	_stagger_flags1,y,2,L555
1444                         ; 721 				Pwm_DisableNotification(stagger_s[found_index].channel);
1446  03bc cd0003            	ldy	#3
1447  03bf 13                	emul	
1448  03c0 b746              	tfr	d,y
1449  03c2 e6ea0004          	ldab	_stagger_s,y
1450  03c6 87                	clra	
1451  03c7 4a000000          	call	f_Pwm_DisableNotification
1453  03cb 2005              	bra	L555
1454  03cd                   L135:
1455                         ; 727 		waited = TRUE;
1457  03cd c601              	ldab	#1
1458  03cf 7b0000            	stab	L3_waited
1459  03d2                   L555:
1460                         ; 729 }
1463  03d2 1b8a              	leas	10,s
1464  03d4 0a                	rtc	
1528                         ; 758 @far void StaggerF_Manager(Stagger_Channel_Type Channel_ID, u_16Bit this_duty_cycle)
1528                         ; 759 {
1529                         	switch	.ftext
1530  03d5                   f_StaggerF_Manager:
1532  03d5 3b                	pshd	
1533  03d6 1b9d              	leas	-3,s
1534       00000003          OFST:	set	3
1537                         ; 760 	u_16Bit current_pwm = this_duty_cycle;   // current duty cycle
1539  03d8 ec88              	ldd	OFST+5,s
1540  03da 6c80              	std	OFST-3,s
1541                         ; 761 	u_8Bit  existing_ch = FALSE;             // existing channel check result
1543                         ; 764 	if ( (current_pwm != 0) && (current_pwm != MAIN_MAX_PWM) ) {
1545  03dc 2751              	beq	L306
1547  03de 1887              	clrx	
1548  03e0 2605              	bne	LC008
1549  03e2 8c8000            	cpd	#-32768
1550  03e5 2748              	beq	L306
1551  03e7                   LC008:
1552                         ; 767 		existing_ch = StaggerF_Chck_Existing_Ch(Channel_ID);
1554  03e7 e684              	ldab	OFST+1,s
1555  03e9 87                	clra	
1556  03ea 4a000000          	call	f_StaggerF_Chck_Existing_Ch
1558  03ee 6b82              	stab	OFST-1,s
1559                         ; 770 		StaggerF_Turn_Off_All_Ch();
1561  03f0 4a005959          	call	f_StaggerF_Turn_Off_All_Ch
1563                         ; 773 		if (existing_ch == FALSE) {
1565  03f4 e682              	ldab	OFST-1,s
1566  03f6 260c              	bne	L506
1567                         ; 776 			StaggerF_Add_Channel(Channel_ID, current_pwm);
1569  03f8 ec80              	ldd	OFST-3,s
1570  03fa 3b                	pshd	
1571  03fb e686              	ldab	OFST+3,s
1572  03fd 87                	clra	
1573  03fe 4a009292          	call	f_StaggerF_Add_Channel
1576  0402 200a              	bra	L706
1577  0404                   L506:
1578                         ; 780 			StaggerF_Update_Ch(Channel_ID, current_pwm);
1580  0404 ec80              	ldd	OFST-3,s
1581  0406 3b                	pshd	
1582  0407 e686              	ldab	OFST+3,s
1583  0409 87                	clra	
1584  040a 4a00e4e4          	call	f_StaggerF_Update_Ch
1586  040e                   L706:
1587                         ; 784 		stagger_flags1[0].b.ch_activated = TRUE;
1589  040e 1c000002          	bset	_stagger_flags1,2
1590                         ; 785 		Pwm_EnableNotification(stagger_s[0].channel, PWM_FALLING_EDGE);
1592  0412 cc0002            	ldd	#2
1593  0415 6c80              	std	0,s
1594  0417 f60004            	ldab	_stagger_s
1595  041a 4a000000          	call	f_Pwm_EnableNotification
1597                         ; 786 		Pwm_SetDutyCycle(stagger_s[0].channel, stagger_s[0].pwm);
1599  041e 1801800005        	movw	_stagger_s+1,0,s
1600  0423 f60004            	ldab	_stagger_s
1601  0426 87                	clra	
1602  0427 4a000000          	call	f_Pwm_SetDutyCycle
1604  042b 1b82              	leas	2,s
1606  042d 2021              	bra	L116
1607  042f                   L306:
1608                         ; 790 		if (current_pwm == 0) {
1610  042f 046402            	tbne	d,L316
1611                         ; 793 			StaggerF_Delete_Ch(Channel_ID);
1615  0432 2015              	bra	LC009
1616  0434                   L316:
1617                         ; 797 			Pwm_SetDutyCycle(Channel_ID, current_pwm);
1619  0434 3b                	pshd	
1620  0435 e686              	ldab	OFST+3,s
1621  0437 87                	clra	
1622  0438 4a000000          	call	f_Pwm_SetDutyCycle
1624  043c 1b82              	leas	2,s
1625                         ; 800 			existing_ch = StaggerF_Chck_Existing_Ch(Channel_ID);
1627  043e e684              	ldab	OFST+1,s
1628  0440 87                	clra	
1629  0441 4a000000          	call	f_StaggerF_Chck_Existing_Ch
1631  0445 6b82              	stab	OFST-1,s
1632                         ; 803 			if (existing_ch == FALSE) {				
1634  0447 2707              	beq	L116
1636                         ; 808 				StaggerF_Delete_Ch(Channel_ID);
1638  0449                   LC009:
1639  0449 e684              	ldab	OFST+1,s
1640  044b 87                	clra	
1641  044c 4a013232          	call	f_StaggerF_Delete_Ch
1643  0450                   L116:
1644                         ; 812 }
1647  0450 1b85              	leas	5,s
1648  0452 0a                	rtc	
1825                         	xdef	f_StaggerF_Adjust_Tcx
1826                         	xdef	f_StaggerF_Get_EctTime
1827                         	xdef	f_StaggerF_Delete_Ch
1828                         	xdef	f_StaggerF_Update_Ch
1829                         	xdef	f_StaggerF_Add_Channel
1830                         	xdef	f_StaggerF_Turn_Off_All_Ch
1831                         	xdef	f_StaggerF_Find_Index
1832                         	xdef	f_StaggerF_Chck_Existing_Ch
1833                         	switch	.bss
1834  0000                   _stagger_flags1:
1835  0000 00000000          	ds.b	4
1836                         	xdef	_stagger_flags1
1837  0004                   _stagger_s:
1838  0004 0000000000000000  	ds.b	12
1839                         	xdef	_stagger_s
1840                         	xref	_hs_allowed_heat_pwm_c
1841                         	xdef	f_StaggerF_Enable_Next_Ch
1842                         	xdef	f_StaggerF_Manager
1843                         	xref	f_Pwm_EnableNotification
1844                         	xref	f_Pwm_DisableNotification
1845                         	xref	f_Pwm_SetDutyCycle
1846                         	xref	__TC2
1867                         	end
