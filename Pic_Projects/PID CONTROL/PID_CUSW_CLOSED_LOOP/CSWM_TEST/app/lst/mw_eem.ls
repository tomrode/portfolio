   1                         ; C Compiler for S12XE [COSMIC Software].
   2                         ; Generator V4.7.8 - 27 Jul 2007
   3                         ; Optimizer V4.6.13 - 27 Jul 2007
   4                         .const:	section	.data
   5  0000                   L3_abyEEMaskTable:
   6  0000 01                	dc.b	1
   7  0001 02                	dc.b	2
   8  0002 04                	dc.b	4
   9  0003 08                	dc.b	8
  10  0004 10                	dc.b	16
  11  0005 20                	dc.b	32
  12  0006 40                	dc.b	64
  13  0007 80                	dc.b	128
  14  0008                   _sEEPConstants:
  15  0008 ff                	dc.b	255
  16  0009 0b2a              	dc.w	2858
  17  000b 8000              	dc.w	-32768
  18  000d 05                	dc.b	5
  19  000e 64                	dc.b	100
  20  000f 32                	dc.b	50
  21  0010 0a                	dc.b	10
  22  0011 c8                	dc.b	200
  23  0012 3c                	dc.b	60
  24  0013 02                	dc.b	2
  25  0014 50                	dc.b	80
  26  0015 04                	dc.b	4
  27  0016 04                	dc.b	4
  28  0017 08                	dc.b	8
  29  0018 19                	dc.b	25
  30  0019 08                	dc.b	8
  31  001a 00                	dc.b	0
  32  001b 50                	dc.b	80
  33  001c 64                	dc.b	100
  34  001d 00                	dc.b	0
  35  001e 50                	dc.b	80
  36  001f 64                	dc.b	100
  37  0020 00                	dc.b	0
  38  0021 50                	dc.b	80
  39  0022 64                	dc.b	100
  40  0023 00                	dc.b	0
  41  0024 50                	dc.b	80
  42  0025 64                	dc.b	100
  43  0026 00                	dc.b	0
  44  0027 50                	dc.b	80
  45  0028 64                	dc.b	100
  46  0029 00                	dc.b	0
  47  002a 50                	dc.b	80
  48  002b 64                	dc.b	100
  49  002c 02                	dc.b	2
  50  002d 0a                	dc.b	10
  51  002e 32                	dc.b	50
  52  002f 5f                	dc.b	95
  53  0030 0190              	dc.w	400
  54  0032 19                	dc.b	25
  55  0033 7d                	dc.b	125
  56  0034 0a                	dc.b	10
  57  0035 e2                	dc.b	226
  58  0036 32                	dc.b	50
  59  0037 04                	dc.b	4
  60  0038 00ad              	dc.w	173
  61  003a 00d5              	dc.w	213
  62  003c a0                	dc.b	160
  63  003d 9b                	dc.b	155
  64  003e 5f                	dc.b	95
  65  003f 5a                	dc.b	90
  66  0040 19                	dc.b	25
  67  0041 03                	dc.b	3
  68  0042 3c                	dc.b	60
  69  0043 0f                	dc.b	15
  70  0044 00                	dc.b	0
  71  0045 04                	dc.b	4
  72  0046 07                	dc.b	7
  73  0047 05                	dc.b	5
  74  0048 0b                	dc.b	11
  75  0049 0d                	dc.b	13
  76  004a 00                	dc.b	0
  77  004b 3c                	dc.b	60
  78  004c 3c                	dc.b	60
  79  004d 1e                	dc.b	30
  80  004e 0f                	dc.b	15
  81  004f 00                	dc.b	0
  82  0050 23                	dc.b	35
  83  0051 2d                	dc.b	45
  84  0052 41                	dc.b	65
  85  0053 64                	dc.b	100
  86  0054 ac                	dc.b	172
  87  0055 a8                	dc.b	168
  88  0056 9e                	dc.b	158
  89  0057 9a                	dc.b	154
  90  0058 ac                	dc.b	172
  91  0059 00                	dc.b	0
  92  005a 00                	dc.b	0
  93  005b 00                	dc.b	0
  94  005c 00                	dc.b	0
  95  005d 00                	dc.b	0
  96  005e 3c                	dc.b	60
  97  005f 3c                	dc.b	60
  98  0060 1e                	dc.b	30
  99  0061 0f                	dc.b	15
 100  0062 00                	dc.b	0
 101  0063 23                	dc.b	35
 102  0064 2d                	dc.b	45
 103  0065 41                	dc.b	65
 104  0066 64                	dc.b	100
 105  0067 ac                	dc.b	172
 106  0068 a8                	dc.b	168
 107  0069 9e                	dc.b	158
 108  006a 9a                	dc.b	154
 109  006b ac                	dc.b	172
 110  006c 00                	dc.b	0
 111  006d 00                	dc.b	0
 112  006e 00                	dc.b	0
 113  006f 00                	dc.b	0
 114  0070 00                	dc.b	0
 115  0071 1e                	dc.b	30
 116  0072 1e                	dc.b	30
 117  0073 14                	dc.b	20
 118  0074 0f                	dc.b	15
 119  0075 00                	dc.b	0
 120  0076 0f                	dc.b	15
 121  0077 23                	dc.b	35
 122  0078 41                	dc.b	65
 123  0079 64                	dc.b	100
 124  007a a8                	dc.b	168
 125  007b a6                	dc.b	166
 126  007c 9e                	dc.b	158
 127  007d 98                	dc.b	152
 128  007e a8                	dc.b	168
 129  007f 00                	dc.b	0
 130  0080 00                	dc.b	0
 131  0081 00                	dc.b	0
 132  0082 00                	dc.b	0
 133  0083 00                	dc.b	0
 134  0084 1e                	dc.b	30
 135  0085 1e                	dc.b	30
 136  0086 14                	dc.b	20
 137  0087 0f                	dc.b	15
 138  0088 00                	dc.b	0
 139  0089 0f                	dc.b	15
 140  008a 23                	dc.b	35
 141  008b 41                	dc.b	65
 142  008c 64                	dc.b	100
 143  008d a8                	dc.b	168
 144  008e a6                	dc.b	166
 145  008f 9e                	dc.b	158
 146  0090 98                	dc.b	152
 147  0091 a8                	dc.b	168
 148  0092 00                	dc.b	0
 149  0093 00                	dc.b	0
 150  0094 00                	dc.b	0
 151  0095 00                	dc.b	0
 152  0096 00                	dc.b	0
 153  0097 1e                	dc.b	30
 154  0098 1e                	dc.b	30
 155  0099 14                	dc.b	20
 156  009a 0f                	dc.b	15
 157  009b 00                	dc.b	0
 158  009c 0f                	dc.b	15
 159  009d 23                	dc.b	35
 160  009e 41                	dc.b	65
 161  009f 64                	dc.b	100
 162  00a0 a8                	dc.b	168
 163  00a1 a6                	dc.b	166
 164  00a2 9e                	dc.b	158
 165  00a3 98                	dc.b	152
 166  00a4 a8                	dc.b	168
 167  00a5 00                	dc.b	0
 168  00a6 00                	dc.b	0
 169  00a7 00                	dc.b	0
 170  00a8 00                	dc.b	0
 171  00a9 00                	dc.b	0
 172  00aa 1e                	dc.b	30
 173  00ab 1e                	dc.b	30
 174  00ac 14                	dc.b	20
 175  00ad 0f                	dc.b	15
 176  00ae 00                	dc.b	0
 177  00af 0f                	dc.b	15
 178  00b0 23                	dc.b	35
 179  00b1 41                	dc.b	65
 180  00b2 64                	dc.b	100
 181  00b3 a8                	dc.b	168
 182  00b4 a6                	dc.b	166
 183  00b5 9e                	dc.b	158
 184  00b6 98                	dc.b	152
 185  00b7 a8                	dc.b	168
 186  00b8 00                	dc.b	0
 187  00b9 00                	dc.b	0
 188  00ba 00                	dc.b	0
 189  00bb 00                	dc.b	0
 190  00bc 00                	dc.b	0
 191  00bd 1e                	dc.b	30
 192  00be 1e                	dc.b	30
 193  00bf 14                	dc.b	20
 194  00c0 0f                	dc.b	15
 195  00c1 00                	dc.b	0
 196  00c2 0f                	dc.b	15
 197  00c3 23                	dc.b	35
 198  00c4 41                	dc.b	65
 199  00c5 64                	dc.b	100
 200  00c6 a8                	dc.b	168
 201  00c7 a6                	dc.b	166
 202  00c8 9e                	dc.b	158
 203  00c9 98                	dc.b	152
 204  00ca a8                	dc.b	168
 205  00cb 00                	dc.b	0
 206  00cc 00                	dc.b	0
 207  00cd 00                	dc.b	0
 208  00ce 00                	dc.b	0
 209  00cf 00                	dc.b	0
 210  00d0 1e                	dc.b	30
 211  00d1 1e                	dc.b	30
 212  00d2 14                	dc.b	20
 213  00d3 0f                	dc.b	15
 214  00d4 00                	dc.b	0
 215  00d5 0f                	dc.b	15
 216  00d6 23                	dc.b	35
 217  00d7 41                	dc.b	65
 218  00d8 64                	dc.b	100
 219  00d9 a8                	dc.b	168
 220  00da a6                	dc.b	166
 221  00db 9e                	dc.b	158
 222  00dc 98                	dc.b	152
 223  00dd a8                	dc.b	168
 224  00de 00                	dc.b	0
 225  00df 00                	dc.b	0
 226  00e0 00                	dc.b	0
 227  00e1 00                	dc.b	0
 228  00e2 00                	dc.b	0
 229  00e3 1e                	dc.b	30
 230  00e4 1e                	dc.b	30
 231  00e5 14                	dc.b	20
 232  00e6 0f                	dc.b	15
 233  00e7 00                	dc.b	0
 234  00e8 0f                	dc.b	15
 235  00e9 23                	dc.b	35
 236  00ea 41                	dc.b	65
 237  00eb 64                	dc.b	100
 238  00ec a8                	dc.b	168
 239  00ed a6                	dc.b	166
 240  00ee 9e                	dc.b	158
 241  00ef 98                	dc.b	152
 242  00f0 a8                	dc.b	168
 243  00f1 00                	dc.b	0
 244  00f2 00                	dc.b	0
 245  00f3 00                	dc.b	0
 246  00f4 00                	dc.b	0
 247  00f5 00                	dc.b	0
 248  00f6 1e                	dc.b	30
 249  00f7 1e                	dc.b	30
 250  00f8 14                	dc.b	20
 251  00f9 0f                	dc.b	15
 252  00fa 00                	dc.b	0
 253  00fb 0f                	dc.b	15
 254  00fc 23                	dc.b	35
 255  00fd 41                	dc.b	65
 256  00fe 64                	dc.b	100
 257  00ff a8                	dc.b	168
 258  0100 a6                	dc.b	166
 259  0101 9e                	dc.b	158
 260  0102 98                	dc.b	152
 261  0103 a8                	dc.b	168
 262  0104 00                	dc.b	0
 263  0105 00                	dc.b	0
 264  0106 00                	dc.b	0
 265  0107 00                	dc.b	0
 266  0108 05                	dc.b	5
 267  0109 0a                	dc.b	10
 268  010a 64                	dc.b	100
 269  010b 0f                	dc.b	15
 270  010c 32                	dc.b	50
 271  010d 04                	dc.b	4
 272  010e 0f                	dc.b	15
 273  010f 19                	dc.b	25
 274  0110 28                	dc.b	40
 275  0111 64                	dc.b	100
 276  0112 2d                	dc.b	45
 277  0113 14                	dc.b	20
 278  0114 14                	dc.b	20
 279  0115 0f                	dc.b	15
 280  0116 76                	dc.b	118
 281  0117 78                	dc.b	120
 282  0118 79                	dc.b	121
 283  0119 77                	dc.b	119
 284  011a 76                	dc.b	118
 285  011b 46                	dc.b	70
 286  011c 0f                	dc.b	15
 287  011d 19                	dc.b	25
 288  011e 28                	dc.b	40
 289  011f 64                	dc.b	100
 290  0120 2d                	dc.b	45
 291  0121 14                	dc.b	20
 292  0122 14                	dc.b	20
 293  0123 0f                	dc.b	15
 294  0124 76                	dc.b	118
 295  0125 78                	dc.b	120
 296  0126 79                	dc.b	121
 297  0127 77                	dc.b	119
 298  0128 76                	dc.b	118
 299  0129 46                	dc.b	70
 300  012a 0f                	dc.b	15
 301  012b 19                	dc.b	25
 302  012c 28                	dc.b	40
 303  012d 64                	dc.b	100
 304  012e 2d                	dc.b	45
 305  012f 14                	dc.b	20
 306  0130 14                	dc.b	20
 307  0131 0f                	dc.b	15
 308  0132 76                	dc.b	118
 309  0133 78                	dc.b	120
 310  0134 79                	dc.b	121
 311  0135 77                	dc.b	119
 312  0136 76                	dc.b	118
 313  0137 46                	dc.b	70
 314  0138 0f                	dc.b	15
 315  0139 19                	dc.b	25
 316  013a 28                	dc.b	40
 317  013b 64                	dc.b	100
 318  013c 2d                	dc.b	45
 319  013d 14                	dc.b	20
 320  013e 14                	dc.b	20
 321  013f 0f                	dc.b	15
 322  0140 76                	dc.b	118
 323  0141 78                	dc.b	120
 324  0142 79                	dc.b	121
 325  0143 77                	dc.b	119
 326  0144 76                	dc.b	118
 327  0145 46                	dc.b	70
 328  0146 0f                	dc.b	15
 329  0147 19                	dc.b	25
 330  0148 28                	dc.b	40
 331  0149 64                	dc.b	100
 332  014a 2d                	dc.b	45
 333  014b 14                	dc.b	20
 334  014c 14                	dc.b	20
 335  014d 0f                	dc.b	15
 336  014e 76                	dc.b	118
 337  014f 78                	dc.b	120
 338  0150 79                	dc.b	121
 339  0151 77                	dc.b	119
 340  0152 76                	dc.b	118
 341  0153 46                	dc.b	70
 342  0154 0f                	dc.b	15
 343  0155 19                	dc.b	25
 344  0156 28                	dc.b	40
 345  0157 64                	dc.b	100
 346  0158 2d                	dc.b	45
 347  0159 14                	dc.b	20
 348  015a 14                	dc.b	20
 349  015b 0f                	dc.b	15
 350  015c 76                	dc.b	118
 351  015d 78                	dc.b	120
 352  015e 79                	dc.b	121
 353  015f 77                	dc.b	119
 354  0160 76                	dc.b	118
 355  0161 46                	dc.b	70
 356  0162 32                	dc.b	50
 357  0163 55                	dc.b	85
 358  0164 59                	dc.b	89
 359  0165 85                	dc.b	133
 360  0166 0d                	dc.b	13
 361  0167 00                	dc.b	0
 362  0168 ffff              	dc.w	-1
 363  016a ffff              	dc.w	-1
 364  016c 00                	dc.b	0
 365  016d 00                	dc.b	0
 366  016e ffff              	dc.w	-1
 367  0170 ffff              	dc.w	-1
 368  0172 00                	dc.b	0
 369  0173 ff                	dc.b	255
 370  0174 ff                	dc.b	255
 371  0175 ff                	dc.b	255
 372  0176 ff                	dc.b	255
 373  0177 ff                	dc.b	255
 374  0178 ff                	dc.b	255
 375  0179 ff                	dc.b	255
 376  017a ffff              	dc.w	-1
 377  017c ffff              	dc.w	-1
 378  017e ff                	dc.b	255
 379  017f ff                	dc.b	255
 380  0180 ff                	dc.b	255
 381  0181 ff                	dc.b	255
 382  0182 ff                	dc.b	255
 383  0183 ff                	dc.b	255
 384  0184 ff                	dc.b	255
 385  0185 ffff              	dc.w	-1
 386  0187 ffff              	dc.w	-1
 387  0189 ff                	dc.b	255
 388  018a ff                	dc.b	255
 389  018b ff                	dc.b	255
 390  018c ff                	dc.b	255
 391  018d ff                	dc.b	255
 392  018e ff                	dc.b	255
 393  018f ff                	dc.b	255
 394  0190 ffff              	dc.w	-1
 395  0192 ffff              	dc.w	-1
 396  0194 ff                	dc.b	255
 397  0195 ff                	dc.b	255
 398  0196 ff                	dc.b	255
 399  0197 ff                	dc.b	255
 400  0198 ff                	dc.b	255
 401  0199 ff                	dc.b	255
 402  019a ff                	dc.b	255
 403  019b ffff              	dc.w	-1
 404  019d ffff              	dc.w	-1
 405  019f ff                	dc.b	255
 406  01a0 ff                	dc.b	255
 407  01a1 ff                	dc.b	255
 408  01a2 ff                	dc.b	255
 409  01a3 ff                	dc.b	255
 410  01a4 ff                	dc.b	255
 411  01a5 ff                	dc.b	255
 412  01a6 ffff              	dc.w	-1
 413  01a8 ffff              	dc.w	-1
 414  01aa ff                	dc.b	255
 415  01ab ff                	dc.b	255
 416  01ac ff                	dc.b	255
 417  01ad ff                	dc.b	255
 418  01ae ff                	dc.b	255
 419  01af ff                	dc.b	255
 420  01b0 ff                	dc.b	255
 421  01b1 ffff              	dc.w	-1
 422  01b3 ffff              	dc.w	-1
 423  01b5 ff                	dc.b	255
 424  01b6 ff                	dc.b	255
 425  01b7 ff                	dc.b	255
 426  01b8 ff                	dc.b	255
 427  01b9 ff                	dc.b	255
 428  01ba ff                	dc.b	255
 429  01bb ff                	dc.b	255
 430  01bc ffff              	dc.w	-1
 431  01be ffff              	dc.w	-1
 432  01c0 ff                	dc.b	255
 433  01c1 ff                	dc.b	255
 434  01c2 ff                	dc.b	255
 435  01c3 ff                	dc.b	255
 436  01c4 ff                	dc.b	255
 437  01c5 ff                	dc.b	255
 438  01c6 ff                	dc.b	255
 439  01c7 ffff              	dc.w	-1
 440  01c9 ffff              	dc.w	-1
 441  01cb ff                	dc.b	255
 442  01cc ff                	dc.b	255
 443  01cd ff                	dc.b	255
 444  01ce ff                	dc.b	255
 445  01cf ff                	dc.b	255
 446  01d0 ff                	dc.b	255
 447  01d1 ff                	dc.b	255
 448  01d2 ffff              	dc.w	-1
 449  01d4 ffff              	dc.w	-1
 450  01d6 ff                	dc.b	255
 451  01d7 ff                	dc.b	255
 452  01d8 ff                	dc.b	255
 453  01d9 ff                	dc.b	255
 454  01da ff                	dc.b	255
 455  01db ff                	dc.b	255
 456  01dc ff                	dc.b	255
 457  01dd ffff              	dc.w	-1
 458  01df ffff              	dc.w	-1
 459  01e1 00                	dc.b	0
 460  01e2 00                	dc.b	0
 461  01e3 00                	dc.b	0
 462  01e4 00                	dc.b	0
 463  01e5 55                	dc.b	85
 464  01e6 ff                	dc.b	255
 465  01e7 ff                	dc.b	255
 466  01e8 ff                	dc.b	255
 467  01e9 ff                	dc.b	255
 468  01ea ff                	dc.b	255
 469  01eb ff                	dc.b	255
 470  01ec ff                	dc.b	255
 471  01ed ff                	dc.b	255
 472  01ee ff                	dc.b	255
 473  01ef ff                	dc.b	255
 474  01f0 ff                	dc.b	255
 475  01f1 00                	dc.b	0
 476  01f2 00                	dc.b	0
 477  01f3 00                	dc.b	0
 478  01f4 00                	dc.b	0
 479  01f5 00                	dc.b	0
 480  01f6 00                	dc.b	0
 481  01f7 00                	dc.b	0
 482  01f8 00                	dc.b	0
 483  01f9 00                	dc.b	0
 484  01fa 55                	dc.b	85
 485  01fb ff                	dc.b	255
 486  01fc ff                	dc.b	255
 487  01fd ff                	dc.b	255
 488  01fe ff                	dc.b	255
 489  01ff ff                	dc.b	255
 490  0200 ff                	dc.b	255
 491  0201 ff                	dc.b	255
 492  0202 ff                	dc.b	255
 493  0203 ff                	dc.b	255
 494  0204 ff                	dc.b	255
 495  0205 ff                	dc.b	255
 496  0206 ff                	dc.b	255
 497  0207 ff                	dc.b	255
 498  0208 00                	dc.b	0
 499  0209 00                	dc.b	0
 500  020a 00                	dc.b	0
 501  020b 00                	dc.b	0
 502  020c 00                	dc.b	0
 503  020d 00                	dc.b	0
 504  020e 00                	dc.b	0
 505  020f 00                	dc.b	0
 506  0210 00                	dc.b	0
 507  0211 00                	dc.b	0
 508  0212 00                	dc.b	0
 509  0213 00                	dc.b	0
 510  0214 00                	dc.b	0
 511  0215 00                	dc.b	0
 512  0216 00                	dc.b	0
 513  0217 00                	dc.b	0
 514  0218 00                	dc.b	0
 515  0219 00                	dc.b	0
 516  021a 00                	dc.b	0
 517  021b 00                	dc.b	0
 518  021c 00                	dc.b	0
 519  021d 00                	dc.b	0
 520  021e 00                	dc.b	0
 521  021f 00                	dc.b	0
 522  0220 00                	dc.b	0
 523  0221 00                	dc.b	0
 524  0222 00                	dc.b	0
 525  0223 00                	dc.b	0
 526  0224 00                	dc.b	0
 527  0225 00                	dc.b	0
 528  0226 00                	dc.b	0
 529  0227 00                	dc.b	0
 530  0228 00                	dc.b	0
 531  0229 00                	dc.b	0
 532  022a 00                	dc.b	0
 533  022b 00                	dc.b	0
 534  022c 00                	dc.b	0
 535  022d 00                	dc.b	0
 536  022e 00                	dc.b	0
 537  022f 00                	dc.b	0
 538  0230 00                	dc.b	0
 539  0231 00                	dc.b	0
 540  0232 00                	dc.b	0
 541  0233 00                	dc.b	0
 542  0234 00                	dc.b	0
 543  0235 00                	dc.b	0
 544  0236 00                	dc.b	0
 545  0237 00                	dc.b	0
 546  0238 00                	dc.b	0
 547  0239 00                	dc.b	0
 548  023a 00                	dc.b	0
 549  023b 00                	dc.b	0
 550  023c 00                	dc.b	0
 551  023d 00                	dc.b	0
 552  023e 00                	dc.b	0
 553  023f 00                	dc.b	0
 554  0240 00                	dc.b	0
 555  0241 00                	dc.b	0
 556  0242 00                	dc.b	0
 557  0243 00                	dc.b	0
 558  0244 00                	dc.b	0
 559  0245 00                	dc.b	0
 560  0246 00                	dc.b	0
 561  0247 00                	dc.b	0
 562  0248 00                	dc.b	0
 563  0249 00                	dc.b	0
 564  024a 00                	dc.b	0
 565  024b 00                	dc.b	0
 566  024c 00                	dc.b	0
 567  024d 00                	dc.b	0
 568  024e 00                	dc.b	0
 569  024f 00                	dc.b	0
 570  0250 00                	dc.b	0
 571  0251 00                	dc.b	0
 572  0252 00                	dc.b	0
 573  0253 00                	dc.b	0
 574  0254 00                	dc.b	0
 575  0255 00                	dc.b	0
 576  0256 00                	dc.b	0
 577  0257 00                	dc.b	0
 578  0258 00                	dc.b	0
 579  0259 00                	dc.b	0
 580  025a 00                	dc.b	0
 581  025b 00                	dc.b	0
 582  025c 00                	dc.b	0
 583  025d 00                	dc.b	0
 584  025e 00                	dc.b	0
 585  025f 00                	dc.b	0
 586  0260 00                	dc.b	0
 587  0261 00                	dc.b	0
 588  0262 00                	dc.b	0
 589  0263 00                	dc.b	0
 590  0264 ff                	dc.b	255
 591  0265 ff                	dc.b	255
 592  0266 ff                	dc.b	255
 593  0267 ff                	dc.b	255
 594  0268 ffff              	dc.w	-1
 595  026a ffff              	dc.w	-1
 596  026c ff                	dc.b	255
 597  026d ff                	dc.b	255
 598  026e ff                	dc.b	255
 599  026f ff                	dc.b	255
 600  0270 ff                	dc.b	255
 601  0271 ffff              	dc.w	-1
 602  0273 ffff              	dc.w	-1
 603  0275 ff                	dc.b	255
 604  0276 ff                	dc.b	255
 605  0277 ff                	dc.b	255
 606  0278 ff                	dc.b	255
 607  0279 ff                	dc.b	255
 608  027a ffff              	dc.w	-1
 609  027c ffff              	dc.w	-1
 610  027e ff                	dc.b	255
 611  027f ff                	dc.b	255
 612  0280 ff                	dc.b	255
 613  0281 ff                	dc.b	255
 614  0282 ff                	dc.b	255
 615  0283 ffff              	dc.w	-1
 616  0285 ffff              	dc.w	-1
 617  0287 ff                	dc.b	255
 618  0288 ff                	dc.b	255
 619  0289 ff                	dc.b	255
 620  028a ff                	dc.b	255
 621  028b ff                	dc.b	255
 622  028c ffff              	dc.w	-1
 623  028e ffff              	dc.w	-1
 624  0290 ff                	dc.b	255
 625  0291 ff                	dc.b	255
 626  0292 ff                	dc.b	255
 627  0293 ff                	dc.b	255
 628  0294 ff                	dc.b	255
 629  0295 ffff              	dc.w	-1
 630  0297 ffff              	dc.w	-1
 631  0299 ff                	dc.b	255
 632  029a ff                	dc.b	255
 633  029b ff                	dc.b	255
 634  029c ff                	dc.b	255
 635  029d ff                	dc.b	255
 636  029e ffff              	dc.w	-1
 637  02a0 ffff              	dc.w	-1
 638  02a2 ff                	dc.b	255
 639  02a3 ff                	dc.b	255
 640  02a4 ff                	dc.b	255
 641  02a5 ff                	dc.b	255
 642  02a6 ff                	dc.b	255
 643  02a7 ffff              	dc.w	-1
 644  02a9 ffff              	dc.w	-1
 645  02ab ff                	dc.b	255
 646  02ac ff                	dc.b	255
 647  02ad ff                	dc.b	255
 648  02ae ff                	dc.b	255
 649  02af ff                	dc.b	255
 650  02b0 ffff              	dc.w	-1
 651  02b2 ffff              	dc.w	-1
 652  02b4 ff                	dc.b	255
 653  02b5 ff                	dc.b	255
 654  02b6 ff                	dc.b	255
 655  02b7 ff                	dc.b	255
 656  02b8 ff                	dc.b	255
 657  02b9 ffff              	dc.w	-1
 658  02bb ffff              	dc.w	-1
 659  02bd ff                	dc.b	255
 660  02be 00                	dc.b	0
 661  02bf ff                	dc.b	255
 662  02c0 ff                	dc.b	255
 663  02c1 ff                	dc.b	255
 664  02c2 ff                	dc.b	255
 665  02c3 ff                	dc.b	255
 666  02c4 ff                	dc.b	255
 667  02c5 ff                	dc.b	255
 668  02c6 ff                	dc.b	255
 669  02c7 ff                	dc.b	255
 670  02c8 ff                	dc.b	255
 671  02c9 ff                	dc.b	255
 672  02ca ff                	dc.b	255
 673  02cb ff                	dc.b	255
 674  02cc ff                	dc.b	255
 675  02cd ff                	dc.b	255
 676  02ce 00                	dc.b	0
 677  02cf 00                	dc.b	0
 678  02d0 00                	dc.b	0
 679  02d1 00                	dc.b	0
 680  02d2 00                	dc.b	0
 681  02d3 00                	dc.b	0
 682  02d4 00                	dc.b	0
 683  02d5 00                	dc.b	0
 684  02d6 00                	dc.b	0
 685  02d7 00                	dc.b	0
 686  02d8 0000              	dc.w	0
 687  02da 0000              	dc.w	0
 688  02dc 00                	dc.b	0
 689  02dd 00                	dc.b	0
 690  02de 00                	dc.b	0
 691  02df 00                	dc.b	0
 692  02e0 0000              	dc.w	0
 693  02e2 ff                	dc.b	255
 694  02e3 ff                	dc.b	255
 695  02e4 ff                	dc.b	255
 696  02e5 ff                	dc.b	255
 697  02e6 ff                	dc.b	255
 698  02e7 ff                	dc.b	255
 699  02e8 ff                	dc.b	255
 700  02e9 ff                	dc.b	255
 701  02ea ff                	dc.b	255
 702  02eb ff                	dc.b	255
 703  02ec ff                	dc.b	255
 704  02ed ff                	dc.b	255
 705  02ee ff                	dc.b	255
 706  02ef ff                	dc.b	255
 707  02f0 ff                	dc.b	255
 708  02f1 ff                	dc.b	255
 709  02f2 ff                	dc.b	255
 710  02f3 ff                	dc.b	255
 711  02f4 ff                	dc.b	255
 712  02f5 ff                	dc.b	255
 713  02f6 ff                	dc.b	255
 714  02f7 ff                	dc.b	255
 715  02f8 ff                	dc.b	255
 716  02f9 ff                	dc.b	255
 717  02fa ff                	dc.b	255
 718  02fb ff                	dc.b	255
 719  02fc ff                	dc.b	255
 720  02fd ff                	dc.b	255
 721  02fe ff                	dc.b	255
 722  02ff ff                	dc.b	255
 723  0300 ff                	dc.b	255
 724  0301 ff                	dc.b	255
 725  0302 ff                	dc.b	255
 726  0303 ff                	dc.b	255
 727  0304 ff                	dc.b	255
 728  0305 ff                	dc.b	255
 729  0306 ff                	dc.b	255
 730  0307 ff                	dc.b	255
 731  0308 0a                	dc.b	10
 732  0309 12                	dc.b	18
 733  030a 00                	dc.b	0
 734  030b 20                	dc.b	32
 735  030c 20                	dc.b	32
 736  030d 20                	dc.b	32
 737  030e 20                	dc.b	32
 738  030f 20                	dc.b	32
 739  0310 20                	dc.b	32
 740  0311 20                	dc.b	32
 741  0312 20                	dc.b	32
 742  0313 01                	dc.b	1
 743  0314 0b                	dc.b	11
 744  0315 27                	dc.b	39
 745  0316 00                	dc.b	0
 746  0317 20                	dc.b	32
 747  0318 20                	dc.b	32
 748  0319 20                	dc.b	32
 749  031a 20                	dc.b	32
 750  031b 20                	dc.b	32
 751  031c 20                	dc.b	32
 752  031d 20                	dc.b	32
 753  031e 20                	dc.b	32
 808                         ; 666 ITBM_BOOLEAN EE_MANAGER_CALL EE_IsChecksumsValid(void)
 808                         ; 667 {
 809                         .ftext:	section	.text
 810  0000                   f_EE_IsChecksumsValid:
 814                         ; 670 	return checksums_valid;
 816  0000 f607da            	ldab	L71_checksums_valid
 819  0003 0a                	rtc	
 843                         ; 688 ITBM_BOOLEAN EE_MANAGER_CALL EE_IsVersionValid(void)
 843                         ; 689 {
 844                         	switch	.ftext
 845  0004                   f_EE_IsVersionValid:
 849                         ; 692 	return version_valid;
 851  0004 f607d9            	ldab	L12_version_valid
 854  0007 0a                	rtc	
 889                         ; 710 void MEM_FAR EE_WaitPendingWrites(void)
 889                         ; 711 {
 890                         	switch	.ftext
 891  0008                   f_EE_WaitPendingWrites:
 893  0008 1b9c              	leas	-4,s
 894       00000004          OFST:	set	4
 897                         ; 716 	ITBM_tmrmSetTimer(TIMER_20MS_EE_WRITE_HOLDOFF, 0);
 899  000a 87                	clra	
 900  000b c7                	clrb	
 901  000c 3b                	pshd	
 902  000d 3b                	pshd	
 903  000e c604              	ldab	#4
 904  0010 4a000000          	call	f_ITBM_tmrmSetTimer
 906  0014 1b84              	leas	4,s
 907  0016                   L531:
 908                         ; 725 		EE_Manager();
 910  0016 4a02e8e8          	call	f_EE_Manager
 912                         ; 731 	} while (EE_IsBusy());
 914  001a 4a000000          	call	f_EE_IsBusy
 916  001e 0471f5            	tbne	b,L531
 917                         ; 738 	for ( i = 0; i < 10000; i++ ) {
 919  0021 87                	clra	
 920  0022 6c80              	std	OFST-4,s
 921  0024 6c82              	std	OFST-2,s
 922  0026                   L341:
 923                         ; 741 		EE_Manager();
 925  0026 4a02e8e8          	call	f_EE_Manager
 927                         ; 738 	for ( i = 0; i < 10000; i++ ) {
 929  002a 186282            	incw	OFST-2,s
 930  002d 2603              	bne	L21
 931  002f 186280            	incw	OFST-4,s
 932  0032                   L21:
 935  0032 ec82              	ldd	OFST-2,s
 936  0034 8c2710            	cpd	#10000
 937  0037 ee80              	ldx	OFST-4,s
 938  0039 188e0000          	cpex	#0
 939  003d 25e7              	blo	L341
 940                         ; 749 }
 943  003f 1b84              	leas	4,s
 944  0041 0a                	rtc	
1026                         ; 765 @far void /*EE_MANAGER_CALL*/ EE_PowerupInit(void)
1026                         ; 766 {
1027                         	switch	.ftext
1028  0042                   f_EE_PowerupInit:
1030  0042 1b95              	leas	-11,s
1031       0000000b          OFST:	set	11
1034                         ; 776 	EE_Manager_Static_Init();
1036  0044 4a0e0c0c          	call	L16f_EE_Manager_Static_Init
1038                         ; 777 	EE_Driver_Static_Init();
1040  0048 4a000000          	call	f_EE_Driver_Static_Init
1042                         ; 779 	EE_DriverPowerupInit();
1044  004c 4a000000          	call	f_EE_DriverPowerupInit
1046                         ; 793 	for (byindex=0; byindex < EE_NUM_BYTES_IN_DIRTY_ARRAY; byindex++) {
1048  0050 c7                	clrb	
1049  0051 6b82              	stab	OFST-9,s
1050  0053                   L571:
1051                         ; 796 		abyEEDirtyBits[byindex] = 0;
1053  0053 b796              	exg	b,y
1054  0055 69ea08a6          	clr	L5_abyEEDirtyBits,y
1055                         ; 793 	for (byindex=0; byindex < EE_NUM_BYTES_IN_DIRTY_ARRAY; byindex++) {
1057  0059 6282              	inc	OFST-9,s
1060  005b e682              	ldab	OFST-9,s
1061  005d c114              	cmpb	#20
1062  005f 25f2              	blo	L571
1063                         ; 807 	for (byindex=0; byindex < EE_NUM_BYTES_IN_CXSUM_FLAGS_ARRAY; byindex++) {
1065  0061 6982              	clr	OFST-9,s
1066  0063                   L302:
1067                         ; 810 		abyEECxsumUpdateReq[byindex] = 0;
1069  0063 e682              	ldab	OFST-9,s
1070  0065 b796              	exg	b,y
1071  0067 69ea07dc          	clr	L31_abyEECxsumUpdateReq,y
1072                         ; 811 		abyEECxsumFailedFlags[byindex] = 0;
1074  006b 69ea07db          	clr	L51_abyEECxsumFailedFlags,y
1075                         ; 807 	for (byindex=0; byindex < EE_NUM_BYTES_IN_CXSUM_FLAGS_ARRAY; byindex++) {
1077  006f 6282              	inc	OFST-9,s
1080  0071 27f0              	beq	L302
1081                         ; 819 	sEEStatusFlags.eeInitialized = ITBM_FALSE;
1083  0073 1d08a504          	bclr	L7_sEEStatusFlags,4
1084                         ; 822 	sEEStatusFlags.eeCxsumIsValid = ITBM_TRUE;
1086  0077 1c08a501          	bset	L7_sEEStatusFlags,1
1087                         ; 825 	EE_RefreshEEToRAM();
1089  007b 4a0a6262          	call	L75f_EE_RefreshEEToRAM
1091                         ; 828 	EE_BlockRead(EE_VRGN_FLAG_START_BLOCK, (UInt8*)&lLongBlockRead);
1093  007f 1a83              	leax	OFST-8,s
1094  0081 34                	pshx	
1095  0082 cc0001            	ldd	#1
1096  0085 4a06bfbf          	call	f_EE_BlockRead
1098  0089 1b82              	leas	2,s
1099                         ; 829 	wEEStartFlag = (UInt16)lLongBlockRead;
1101  008b 18028587          	movw	OFST-6,s,OFST-4,s
1102                         ; 832 	EE_BlockRead(EE_VRGN_FLAG_END_BLOCK, (UInt8*)&lLongBlockRead);
1104  008f 1a83              	leax	OFST-8,s
1105  0091 34                	pshx	
1106  0092 cc004d            	ldd	#77
1107  0095 4a06bfbf          	call	f_EE_BlockRead
1109  0099 1b82              	leas	2,s
1110                         ; 833 	wEEEndFlag = (UInt16)lLongBlockRead;
1112  009b 18028589          	movw	OFST-6,s,OFST-2,s
1113                         ; 839 	if (!((wEEStartFlag == EE_IS_VIRGIN_MARKER) && (wEEEndFlag == EE_IS_VIRGIN_MARKER))) {
1115  009f ec87              	ldd	OFST-4,s
1116  00a1 8c5aa5            	cpd	#23205
1117  00a4 2609              	bne	L312
1119  00a6 ec89              	ldd	OFST-2,s
1120  00a8 8c5aa5            	cpd	#23205
1121  00ab 18270092          	beq	L112
1122  00af                   L312:
1123                         ; 843 		EE_UpdateAllCxsum();
1125  00af 4a0c9494          	call	L54f_EE_UpdateAllCxsum
1127                         ; 847 		sEEStatusFlags.eeBkupInitializeReq = ITBM_TRUE;
1129  00b3 1c08a508          	bset	L7_sEEStatusFlags,8
1130                         ; 853 		for (byindex=0; byindex < EE_CXSUM_NUM_SECTIONS; byindex++) {
1132  00b7 6982              	clr	OFST-9,s
1133  00b9                   L512:
1134                         ; 856 			EE_SectionCopy(byindex, EE_COPY_PRIMARY2BACKUP);
1136  00b9 87                	clra	
1137  00ba c7                	clrb	
1138  00bb 3b                	pshd	
1139  00bc e684              	ldab	OFST-7,s
1140  00be 4a0afcfc          	call	L14f_EE_SectionCopy
1142  00c2 1b82              	leas	2,s
1143                         ; 853 		for (byindex=0; byindex < EE_CXSUM_NUM_SECTIONS; byindex++) {
1145  00c4 6282              	inc	OFST-9,s
1148  00c6 27f1              	beq	L512
1149                         ; 867 		for (byindex=0; byindex < EE_NUM_BLOCKS; byindex++) {
1151  00c8 c7                	clrb	
1152  00c9 6b82              	stab	OFST-9,s
1153  00cb                   L322:
1154                         ; 870 			if (aEEBlockTable[byindex].eDefaultLoc != EE_NO_DEFAULT) {
1156  00cb 860b              	ldaa	#11
1157  00cd 12                	mul	
1158  00ce b746              	tfr	d,y
1159  00d0 e6ea0002          	ldab	_aEEBlockTable+2,y
1160  00d4 2712              	beq	L132
1161                         ; 874 				EE_BlockWrite(byindex, aEEDefaultsTable[aEEBlockTable[byindex].eDefaultLoc].pRomAddr);
1163  00d6 87                	clra	
1164  00d7 59                	lsld	
1165  00d8 b745              	tfr	d,x
1166  00da ece20000          	ldd	_aEEDefaultsTable,x
1167  00de 3b                	pshd	
1168  00df e684              	ldab	OFST-7,s
1169  00e1 87                	clra	
1170  00e2 4a056b6b          	call	f_EE_BlockWrite
1172  00e6 1b82              	leas	2,s
1173  00e8                   L132:
1174                         ; 867 		for (byindex=0; byindex < EE_NUM_BLOCKS; byindex++) {
1176  00e8 6282              	inc	OFST-9,s
1179  00ea e682              	ldab	OFST-9,s
1180  00ec c14e              	cmpb	#78
1181  00ee 25db              	blo	L322
1182                         ; 885 		lLongBlockRead = EE_IS_VIRGIN_MARKER;
1184  00f0 cc5aa5            	ldd	#23205
1185  00f3 6c85              	std	OFST-6,s
1186  00f5 87                	clra	
1187  00f6 c7                	clrb	
1188  00f7 6c83              	std	OFST-8,s
1189                         ; 886 		EE_BlockWrite(EE_VRGN_FLAG_START_BLOCK, (UInt8*)&lLongBlockRead);        
1191  00f9 1a83              	leax	OFST-8,s
1192  00fb 34                	pshx	
1193  00fc 52                	incb	
1194  00fd 4a056b6b          	call	f_EE_BlockWrite
1196  0101 1b82              	leas	2,s
1197                         ; 887 		EE_BlockWrite(EE_VRGN_FLAG_END_BLOCK, (UInt8*)&lLongBlockRead);
1199  0103 1a83              	leax	OFST-8,s
1200  0105 34                	pshx	
1201  0106 cc004d            	ldd	#77
1202  0109 4a056b6b          	call	f_EE_BlockWrite
1204  010d 1b82              	leas	2,s
1205                         ; 890 		EE_WriteFailedFlags();
1207  010f 4a0d4646          	call	L35f_EE_WriteFailedFlags
1210  0113                   L332:
1211                         ; 955 		version_valid = ITBM_TRUE;
1213  0113 c601              	ldab	#1
1214  0115 7b07d9            	stab	L12_version_valid
1215                         ; 957 		EE_BlockRead(EE_EEPROM_VERSION, (UINT8*)&version);
1217  0118 1a80              	leax	OFST-11,s
1218  011a 34                	pshx	
1219  011b cc0003            	ldd	#3
1220  011e 4a06bfbf          	call	f_EE_BlockRead
1222  0122 1b82              	leas	2,s
1223                         ; 961 		if (version != CURRENT_EEPROM_VERSION) {
1225  0124 ec80              	ldd	OFST-11,s
1226  0126 8c0b22            	cpd	#2850
1227  0129 277d              	beq	L342
1228                         ; 964 			version = CURRENT_EEPROM_VERSION;
1230  012b cc0b22            	ldd	#2850
1231  012e 6c80              	std	OFST-11,s
1232                         ; 966 			version_valid = ITBM_FALSE;
1234  0130 7907d9            	clr	L12_version_valid
1235                         ; 968 			EE_BlockWrite(EE_EEPROM_VERSION, (UINT8*)&version);
1237  0133 1a80              	leax	OFST-11,s
1238  0135 34                	pshx	
1239  0136 cc0003            	ldd	#3
1240  0139 4a056b6b          	call	f_EE_BlockWrite
1242  013d 1b82              	leas	2,s
1243  013f 2067              	bra	L342
1244  0141                   L112:
1245                         ; 899 		EE_ReadFailedFlags();
1247  0141 4a0d8484          	call	L55f_EE_ReadFailedFlags
1249                         ; 902 		sEEStatusFlags.eeBkupInitializeReq = ITBM_FALSE;
1251  0145 1d08a508          	bclr	L7_sEEStatusFlags,8
1252                         ; 903 		EE_BlockRead(EE_BACKUPS_INITALIZED_BLOCK, (UInt8*)&lLongBlockRead);
1254  0149 1a83              	leax	OFST-8,s
1255  014b 34                	pshx	
1256  014c cc004c            	ldd	#76
1257  014f 4a06bfbf          	call	f_EE_BlockRead
1259  0153 1b82              	leas	2,s
1260                         ; 907 		if ( lLongBlockRead != EE_BACKUPS_INITALIZED ) {
1262  0155 ec83              	ldd	OFST-8,s
1263  0157 8c5aa5            	cpd	#23205
1264  015a 2607              	bne	LC001
1265  015c ec85              	ldd	OFST-6,s
1266  015e 8ca55a            	cpd	#-23206
1267  0161 270a              	beq	L532
1268  0163                   LC001:
1269                         ; 911 			sEEStatusFlags.eeBkupInitializeReq = ITBM_TRUE;
1271  0163 1c08a508          	bset	L7_sEEStatusFlags,8
1272                         ; 915 			EE_SID31_FACallback();
1274  0167 4a098b8b          	call	f_EE_SID31_FACallback
1277  016b 20a6              	bra	L332
1278  016d                   L532:
1279                         ; 929 			EPAGE = GetEPage(EE_CXSUM_INVALID_FLAG_ADDRESS);
1281  016d ce0010            	ldx	#16
1282  0170 cc0c00            	ldd	#3072
1283  0173 4a000000          	call	f_GetEPage
1285  0177 7b0000            	stab	__EPAGE
1286                         ; 930 			lLongBlockRead = *((unsigned long*)(EEPROM_LOGICAL_START + GetEOffset(EE_CXSUM_INVALID_FLAG_ADDRESS)));
1288  017a ce0010            	ldx	#16
1289  017d cc0c00            	ldd	#3072
1290  0180 4a000000          	call	f_GetEOffset
1292  0184 b746              	tfr	d,y
1293  0186 1802ea080285      	movw	2050,y,OFST-6,s
1294  018c ecea0800          	ldd	2048,y
1295  0190 6c83              	std	OFST-8,s
1296                         ; 934 			if ( lLongBlockRead != EE_CXSUM_IS_VALID_DATA ) {
1298  0192 8c5aa5            	cpd	#23205
1299  0195 2609              	bne	LC002
1300  0197 ec85              	ldd	OFST-6,s
1301  0199 8ca55a            	cpd	#-23206
1302  019c 1827ff73          	beq	L332
1303  01a0                   LC002:
1304                         ; 937 				EE_PowerFailureRecovery();
1306  01a0 4a0c1212          	call	L34f_EE_PowerFailureRecovery
1308  01a4 1820ff6b          	bra	L332
1309  01a8                   L342:
1310                         ; 979 	ITBM_tmrmSetTimer(TIMER_20MS_EE_WRITE_HOLDOFF, EE_HOLDOFF_TIME);
1312  01a8 cc0032            	ldd	#50
1313  01ab 3b                	pshd	
1314  01ac c7                	clrb	
1315  01ad 3b                	pshd	
1316  01ae c604              	ldab	#4
1317  01b0 4a000000          	call	f_ITBM_tmrmSetTimer
1319                         ; 981 }
1322  01b4 1b8f              	leas	15,s
1323  01b6 0a                	rtc	
1410                         ; 990 @far void /*EE_MANAGER_CALL*/ EE_LoadDefaults(void)
1410                         ; 991 {
1411                         	switch	.ftext
1412  01b7                   f_EE_LoadDefaults:
1414  01b7 1b92              	leas	-14,s
1415       0000000e          OFST:	set	14
1418                         ; 1002 	EE_BlockRead(EE_LOAD_DEFAULTS, &temp_load_data_c);
1420  01b9 1a83              	leax	OFST-11,s
1421  01bb 34                	pshx	
1422  01bc cc0002            	ldd	#2
1423  01bf 4a06bfbf          	call	f_EE_BlockRead
1425  01c3 1b82              	leas	2,s
1426                         ; 1003 	EE_BlockRead(EE_FBL_REPROG_REQ, &temp_reprog_req_c);
1428  01c5 1a8b              	leax	OFST-3,s
1429  01c7 34                	pshx	
1430  01c8 cc0036            	ldd	#54
1431  01cb 4a06bfbf          	call	f_EE_BlockRead
1433  01cf 1b82              	leas	2,s
1434                         ; 1007 	if ( (temp_load_data_c != 0xAA) || (temp_reprog_req_c != 0xAA) )  {
1436  01d1 e683              	ldab	OFST-11,s
1437  01d3 c1aa              	cmpb	#170
1438  01d5 2608              	bne	L503
1440  01d7 e68b              	ldab	OFST-3,s
1441  01d9 c1aa              	cmpb	#170
1442  01db 18270106          	beq	L303
1443  01df                   L503:
1444                         ; 1010 		TempAddr = (UINT8*)&sEEPConstants.eeprom_version_w;
1446  01df cc0009            	ldd	#_sEEPConstants+1
1447  01e2 6c81              	std	OFST-13,s
1448                         ; 1012 		for (EeBlockLocation = EE_EEPROM_VERSION; EeBlockLocation <= EE_RESERVE_DATAS2; EeBlockLocation++) {
1450  01e4 c603              	ldab	#3
1451  01e6 6b80              	stab	OFST-14,s
1452  01e8                   L703:
1453                         ; 1014 			EE_BlockWrite(EeBlockLocation,  TempAddr); //Harsha Added
1455  01e8 ec81              	ldd	OFST-13,s
1456  01ea 3b                	pshd	
1457  01eb e682              	ldab	OFST-12,s
1458  01ed 87                	clra	
1459  01ee 4a056b6b          	call	f_EE_BlockWrite
1461  01f2 1b82              	leas	2,s
1462                         ; 1015 			TempAddr += (aEEBlockAddrTable[EeBlockLocation].wSize);
1464  01f4 e680              	ldab	OFST-14,s
1465  01f6 8605              	ldaa	#5
1466  01f8 12                	mul	
1467  01f9 b746              	tfr	d,y
1468  01fb ec81              	ldd	OFST-13,s
1469  01fd e3ea0003          	addd	_aEEBlockAddrTable+3,y
1470  0201 6c81              	std	OFST-13,s
1471                         ; 1012 		for (EeBlockLocation = EE_EEPROM_VERSION; EeBlockLocation <= EE_RESERVE_DATAS2; EeBlockLocation++) {
1473  0203 6280              	inc	OFST-14,s
1476  0205 e680              	ldab	OFST-14,s
1477  0207 c120              	cmpb	#32
1478  0209 23dd              	bls	L703
1479                         ; 1018 		EE_BlockWrite(EE_DTC_STATUS, (UInt8*)&sEEPConstants.Dtcstat[0]);
1481  020b cc0208            	ldd	#_sEEPConstants+512
1482  020e 3b                	pshd	
1483  020f cc0021            	ldd	#33
1484  0212 4a056b6b          	call	f_EE_BlockWrite
1486  0216 1b82              	leas	2,s
1487                         ; 1020 		EE_BlockRead(EE_ECU_TIMESTAMPS, (UInt8*)&temp_ecu_time[0]); 
1489  0218 1a84              	leax	OFST-10,s
1490  021a 34                	pshx	
1491  021b cc0027            	ldd	#39
1492  021e 4a06bfbf          	call	f_EE_BlockRead
1494  0222 1b82              	leas	2,s
1495                         ; 1022 		if(temp_ecu_time[0] == 0xFF && temp_ecu_time[1] == 0xFF && temp_ecu_time[2] == 0xFF && temp_ecu_time[3] == 0xFF)
1497  0224 e684              	ldab	OFST-10,s
1498  0226 04a11c            	ibne	b,L713
1500  0229 e685              	ldab	OFST-9,s
1501  022b 04a117            	ibne	b,L713
1503  022e e686              	ldab	OFST-8,s
1504  0230 04a112            	ibne	b,L713
1506  0233 e687              	ldab	OFST-7,s
1507  0235 04a10d            	ibne	b,L713
1508                         ; 1024 			EE_BlockWrite(EE_ECU_TIMESTAMPS, (UInt8*)&sEEPConstants.ecu_timestamps_l[0]);
1510  0238 cc02d4            	ldd	#_sEEPConstants+716
1511  023b 3b                	pshd	
1512  023c cc0027            	ldd	#39
1513  023f 4a056b6b          	call	f_EE_BlockWrite
1515  0243 1b82              	leas	2,s
1517  0245                   L713:
1518                         ; 1031 		EE_BlockRead(EE_ECU_TIME_KEYON, (UInt8*)&temp_key_on_w); 
1520  0245 1a8c              	leax	OFST-2,s
1521  0247 34                	pshx	
1522  0248 cc0028            	ldd	#40
1523  024b 4a06bfbf          	call	f_EE_BlockRead
1525  024f 1b82              	leas	2,s
1526                         ; 1033 		if (temp_key_on_w == 0xFFFF) {
1528  0251 ec8c              	ldd	OFST-2,s
1529  0253 04a419            	ibne	d,L323
1530                         ; 1035 			EE_BlockWrite(EE_ECU_TIME_KEYON, (UINT8*)&sEEPConstants.ecu_timekeyon_w);
1532  0256 cc02d8            	ldd	#_sEEPConstants+720
1533  0259 3b                	pshd	
1534  025a cc0028            	ldd	#40
1535  025d 4a056b6b          	call	f_EE_BlockWrite
1537                         ; 1036 			EE_BlockWrite(EE_ECU_KEYON_COUNTER, (UINT8*)&sEEPConstants.ecu_keyon_counter_w);
1539  0261 cc02da            	ldd	#_sEEPConstants+722
1540  0264 6c80              	std	0,s
1541  0266 cc0029            	ldd	#41
1542  0269 4a056b6b          	call	f_EE_BlockWrite
1544  026d 1b82              	leas	2,s
1546  026f                   L323:
1547                         ; 1042 		EE_BlockWrite(EE_ECU_TIME_FIRSTDTC, (UInt8*)&sEEPConstants.ecu_time_firstdtc_l[0]);
1549  026f cc02dc            	ldd	#_sEEPConstants+724
1550  0272 3b                	pshd	
1551  0273 cc002a            	ldd	#42
1552  0276 4a056b6b          	call	f_EE_BlockWrite
1554                         ; 1043 		EE_BlockWrite(EE_ECU_TIME_KEYON_FIRSTDTC, (UINT8*)&sEEPConstants.ecu_timekeyon_firstdtc_w);
1556  027a cc02e0            	ldd	#_sEEPConstants+728
1557  027d 6c80              	std	0,s
1558  027f cc002b            	ldd	#43
1559  0282 4a056b6b          	call	f_EE_BlockWrite
1561  0286 1b82              	leas	2,s
1562                         ; 1047 		EE_BlockRead(EE_ODOMETER, (UInt8*)&temp_TotalOdo_c[0]); //$2001 service
1564  0288 1a88              	leax	OFST-6,s
1565  028a 34                	pshx	
1566  028b cc0025            	ldd	#37
1567  028e 4a06bfbf          	call	f_EE_BlockRead
1569  0292 1b82              	leas	2,s
1570                         ; 1049 		if(temp_TotalOdo_c[0] == 0xFF && temp_TotalOdo_c[1] == 0xFF && temp_TotalOdo_c[2] == 0xFF)
1572  0294 e688              	ldab	OFST-6,s
1573  0296 04a11c            	ibne	b,L523
1575  0299 e689              	ldab	OFST-5,s
1576  029b 04a117            	ibne	b,L523
1578  029e e68a              	ldab	OFST-4,s
1579  02a0 04a112            	ibne	b,L523
1580                         ; 1052 			EE_BlockWrite(EE_ODOMETER, (UINT8*)&sEEPConstants.odometer_c[0]); //$2001 service Initial value
1582  02a3 cc02ce            	ldd	#_sEEPConstants+710
1583  02a6 3b                	pshd	
1584  02a7 cc0025            	ldd	#37
1585  02aa 4a056b6b          	call	f_EE_BlockWrite
1587                         ; 1053 			EE_BlockWrite(EE_ODOMETER_LAST_PGM, (UINT8*)&sEEPConstants.odometer_last_pgm_c[0]); //$2002 service Initial value
1589  02ae cc02d1            	ldd	#_sEEPConstants+713
1590  02b1 6c80              	std	0,s
1593  02b3 2003              	bra	L723
1594  02b5                   L523:
1595                         ; 1058 			EE_BlockWrite(EE_ODOMETER_LAST_PGM, (UINT8*)&temp_TotalOdo_c[0]); //$2002 service
1597  02b5 1a88              	leax	OFST-6,s
1598  02b7 34                	pshx	
1600  02b8                   L723:
1601  02b8 cc0026            	ldd	#38
1602  02bb 4a056b6b          	call	f_EE_BlockWrite
1603  02bf 1b82              	leas	2,s
1604                         ; 1075 		temp_load_data_c = 0xAA;
1606  02c1 c6aa              	ldab	#170
1607  02c3 6b83              	stab	OFST-11,s
1608                         ; 1076 		EE_BlockWrite(EE_LOAD_DEFAULTS, &temp_load_data_c);
1610  02c5 1a83              	leax	OFST-11,s
1611  02c7 34                	pshx	
1612  02c8 cc0002            	ldd	#2
1613  02cb 4a056b6b          	call	f_EE_BlockWrite
1615  02cf 1b82              	leas	2,s
1616                         ; 1077 		EE_BlockWrite(EE_FBL_REPROG_REQ, &temp_load_data_c);
1618  02d1 1a83              	leax	OFST-11,s
1619  02d3 34                	pshx	
1620  02d4 cc0036            	ldd	#54
1621  02d7 4a056b6b          	call	f_EE_BlockWrite
1623  02db 1b82              	leas	2,s
1624                         ; 1079 	   	diag_load_def_eemap_b = 0;
1626  02dd 1d000020          	bclr	_diag_io_lock3_flags_c,32
1627                         ; 1080 		diag_load_bl_eep_backup_b = 1; //Its time to take backup for Boot data	
1629  02e1 1c000040          	bset	_diag_io_lock3_flags_c,64
1630  02e5                   L303:
1631                         ; 1085 }
1634  02e5 1b8e              	leas	14,s
1635  02e7 0a                	rtc	
1743                         ; 1102 @far void /*EE_MANAGER_CALL*/ EE_Manager(void)
1743                         ; 1103 {
1744                         	switch	.ftext
1745  02e8                   f_EE_Manager:
1747  02e8 1b91              	leas	-15,s
1748       0000000f          OFST:	set	15
1751                         ; 1120 	bEEWriteHandled = ITBM_FALSE;
1753  02ea 6984              	clr	OFST-11,s
1754                         ; 1126 	if (EE_IsBusy() != ITBM_TRUE /*&& ITBM_tmrmExpired(TIMER_20MS_EE_WRITE_HOLDOFF)*/ ) {
1756  02ec 4a000000          	call	f_EE_IsBusy
1758  02f0 53                	decb	
1759  02f1 18270224          	beq	L763
1760                         ; 1134 		if ( sEEStatusFlags.eeWriteChecksumFlag == ITBM_TRUE ) {
1762  02f5 1f08a51029        	brclr	L7_sEEStatusFlags,16,L173
1763                         ; 1138 			*((unsigned long *)abyEEWriteBuff) = (UInt32)(EE_CXSUM_IS_INVALID_DATA);
1765  02fa ccffff            	ldd	#-1
1766  02fd 7c07df            	std	L11_abyEEWriteBuff+2
1767  0300 7c07dd            	std	L11_abyEEWriteBuff
1768                         ; 1141 			EE_ForcedWrite((UInt32)EE_CXSUM_INVALID_FLAG_ADDRESS, abyEEWriteBuff, EE_SECTOR_SIZE);
1770  0303 cc0004            	ldd	#4
1771  0306 3b                	pshd	
1772  0307 cc07dd            	ldd	#L11_abyEEWriteBuff
1773  030a 3b                	pshd	
1774  030b ce0010            	ldx	#16
1775  030e cc0c00            	ldd	#3072
1776  0311 4a000000          	call	f_EE_ForcedWrite
1778  0315 1b84              	leas	4,s
1779                         ; 1143 			sEEStatusFlags.eeWriteChecksumFlag = ITBM_FALSE;
1781                         ; 1145 			sEEStatusFlags.eeCxsumIsValid = ITBM_FALSE;
1783  0317 1d08a511          	bclr	L7_sEEStatusFlags,17
1784                         ; 1148 			sEEStatusFlags.eeManagerBusy = ITBM_TRUE;
1786  031b 1c08a502          	bset	L7_sEEStatusFlags,2
1788  031f 182001f6          	bra	L763
1789  0323                   L173:
1790                         ; 1163 			for (byindex = 0x00; byindex < EE_NUM_BYTES_IN_DIRTY_ARRAY && !bEEWriteHandled; byindex++) {
1792  0323 6983              	clr	OFST-12,s
1794  0325 18200183          	bra	L104
1795  0329                   L573:
1796                         ; 1170 				if (abyEEDirtyBits[byindex] != 0x00) {
1798  0329 87                	clra	
1799  032a b746              	tfr	d,y
1800  032c e7ea08a6          	tst	L5_abyEEDirtyBits,y
1801  0330 182700e3          	beq	L504
1802                         ; 1173 					byEEDirtyBitRead = abyEEDirtyBits[byindex];
1804  0334 180aea08a682      	movb	L5_abyEEDirtyBits,y,OFST-13,s
1805                         ; 1174 					byDirtyBitOffset = 0;
1807  033a 6984              	clr	OFST-11,s
1809  033c 2004              	bra	L314
1810  033e                   L704:
1811                         ; 1183 						byDirtyBitOffset++;
1813  033e 6284              	inc	OFST-11,s
1814                         ; 1184 						byEEDirtyBitRead >>= 1;
1816  0340 6482              	lsr	OFST-13,s
1817  0342                   L314:
1818                         ; 1180 					while (!(byEEDirtyBitRead & 0x01)) {
1820  0342 0f8201f8          	brclr	OFST-13,s,1,L704
1821                         ; 1193 					EELocation_temp = (byindex << 3) + byDirtyBitOffset;
1823  0346 e683              	ldab	OFST-12,s
1824  0348 58                	lslb	
1825  0349 58                	lslb	
1826  034a 58                	lslb	
1827  034b eb84              	addb	OFST-11,s
1828  034d 6b82              	stab	OFST-13,s
1829                         ; 1196 					EE_MAKE_SECTOR_CLEAN(EELocation_temp);
1831  034f 87                	clra	
1832  0350 b746              	tfr	d,y
1833  0352 1857              	asry	
1834  0354 1857              	asry	
1835  0356 1857              	asry	
1836  0358 c407              	andb	#7
1837  035a b745              	tfr	d,x
1838  035c e6e20000          	ldab	L3_abyEEMaskTable,x
1839  0360 51                	comb	
1840  0361 e4ea08a6          	andb	L5_abyEEDirtyBits,y
1841  0365 6bea08a6          	stab	L5_abyEEDirtyBits,y
1842                         ; 1202 					if ( (EELocation_temp < EE_NUM_BLOCKS)  
1842                         ; 1203 					   ||((EELocation_temp >= EE_NUM_PRIMARY_IDS) && ((EELocation_temp - EE_NUM_PRIMARY_IDS) < EE_NUM_BLOCKS))) {
1844  0369 e682              	ldab	OFST-13,s
1845  036b c14e              	cmpb	#78
1846  036d 2512              	blo	L124
1848  036f c150              	cmpb	#80
1849  0371 18250098          	blo	L714
1851  0375 c3ffb0            	addd	#-80
1852  0378 8c004e            	cpd	#78
1853  037b 182c008e          	bge	L714
1854  037f e682              	ldab	OFST-13,s
1855  0381                   L124:
1856                         ; 1210 						if (EELocation_temp < EE_NUM_PRIMARY_IDS ) {
1858  0381 c150              	cmpb	#80
1859  0383 2411              	bhs	L324
1860                         ; 1214 							ShadowOffst = aEEBlockTable[EELocation_temp].wRAMOffset;
1862  0385 860b              	ldaa	#11
1863  0387 12                	mul	
1864  0388 b746              	tfr	d,y
1865  038a 1802ea000989      	movw	_aEEBlockTable+9,y,OFST-6,s
1866                         ; 1215 							lLongBlockWrite = aEEBlockTable[EELocation_temp].wOffset + EE_EEPROM_START_PHY_ADDRESS;
1868  0390 ecea0003          	ldd	_aEEBlockTable+3,y
1870  0394 2024              	bra	LC003
1871  0396                   L324:
1872                         ; 1228 							EELocation_temp = EELocation_temp - EE_NUM_PRIMARY_IDS;
1874  0396 c050              	subb	#80
1875  0398 6b82              	stab	OFST-13,s
1876                         ; 1234 							if (aEEBlockTable[EELocation_temp].wBackupOffset != EE_NO_BKUP_OFST) {
1878  039a 860b              	ldaa	#11
1879  039c 12                	mul	
1880  039d b746              	tfr	d,y
1881  039f ecea0005          	ldd	_aEEBlockTable+5,y
1882  03a3 048422            	ibeq	d,L724
1883                         ; 1238 								ShadowOffst = aEEBlockTable[EELocation_temp].wRAMOffset + SHADOW_BACKUP_START_OFFSET;
1885  03a6 e682              	ldab	OFST-13,s
1886  03a8 860b              	ldaa	#11
1887  03aa 12                	mul	
1888  03ab b746              	tfr	d,y
1889  03ad ecea0009          	ldd	_aEEBlockTable+9,y
1890  03b1 c303e8            	addd	#1000
1891  03b4 6c89              	std	OFST-6,s
1892                         ; 1239 								lLongBlockWrite = aEEBlockTable[EELocation_temp].wBackupOffset + EE_EEPROM_START_PHY_ADDRESS;
1894  03b6 ecea0005          	ldd	_aEEBlockTable+5,y
1895  03ba                   LC003:
1896  03ba 1887              	clrx	
1897  03bc 188b0010          	addx	#16
1898  03c0 6c87              	std	OFST-8,s
1899  03c2 6e85              	stx	OFST-10,s
1901  03c4 e682              	ldab	OFST-13,s
1902  03c6 2004              	bra	L524
1903  03c8                   L724:
1904                         ; 1252 								EELocation_temp = EE_INVALID_BLOCK_ID;
1906  03c8 c6ff              	ldab	#255
1907  03ca 6b82              	stab	OFST-13,s
1908  03cc                   L524:
1909                         ; 1266 						if (EELocation_temp != EE_INVALID_BLOCK_ID) {
1911  03cc 04813e            	ibeq	b,L714
1912                         ; 1274 							for (byindex = 0; byindex < aEEBlockTable[EELocation_temp].wSize; byindex++) {
1914  03cf 6983              	clr	OFST-12,s
1916  03d1 2013              	bra	L144
1917  03d3                   L534:
1918                         ; 1277 								abyEEWriteBuff[byindex]  = byShadowRam[ShadowOffst + byindex];
1920  03d3 e683              	ldab	OFST-12,s
1921  03d5 87                	clra	
1922  03d6 b746              	tfr	d,y
1923  03d8 ee89              	ldx	OFST-6,s
1924  03da 1ae5              	leax	b,x
1925  03dc 180ae20008ea07dd  	movb	L52_byShadowRam,x,L11_abyEEWriteBuff,y
1926                         ; 1274 							for (byindex = 0; byindex < aEEBlockTable[EELocation_temp].wSize; byindex++) {
1928  03e4 6283              	inc	OFST-12,s
1929  03e6                   L144:
1932  03e6 e683              	ldab	OFST-12,s
1933  03e8 87                	clra	
1934  03e9 6c80              	std	OFST-15,s
1935  03eb e682              	ldab	OFST-13,s
1936  03ed 860b              	ldaa	#11
1937  03ef 12                	mul	
1938  03f0 b746              	tfr	d,y
1939  03f2 ecea0007          	ldd	_aEEBlockTable+7,y
1940  03f6 ac80              	cpd	OFST-15,s
1941  03f8 22d9              	bhi	L534
1942                         ; 1292 								EE_ForcedWrite(lLongBlockWrite, abyEEWriteBuff, aEEBlockTable[EELocation_temp].wSize);
1944  03fa 3b                	pshd	
1945  03fb cc07dd            	ldd	#L11_abyEEWriteBuff
1946  03fe 3b                	pshd	
1947  03ff ec8b              	ldd	OFST-4,s
1948  0401 ee89              	ldx	OFST-6,s
1949  0403 4a000000          	call	f_EE_ForcedWrite
1951  0407 1b84              	leas	4,s
1952                         ; 1308 							sEEStatusFlags.eeManagerBusy = ITBM_TRUE;
1954  0409 1c08a502          	bset	L7_sEEStatusFlags,2
1955  040d                   L714:
1956                         ; 1322 					bEEWriteHandled = ITBM_TRUE;
1958  040d c601              	ldab	#1
1959  040f 6b84              	stab	OFST-11,s
1961  0411                   L544:
1962                         ; 1163 			for (byindex = 0x00; byindex < EE_NUM_BYTES_IN_DIRTY_ARRAY && !bEEWriteHandled; byindex++) {
1964  0411 6283              	inc	OFST-12,s
1965  0413 18200095          	bra	L104
1966  0417                   L504:
1967                         ; 1334 					if ((byindex == (EE_NUM_BYTES_IN_PRIMARY_DIRTY_ARRAY - 1)) ||
1967                         ; 1335 					   (byindex == (EE_NUM_BYTES_IN_DIRTY_ARRAY - 1))) {
1969  0417 c109              	cmpb	#9
1970  0419 2704              	beq	L154
1972  041b c113              	cmpb	#19
1973  041d 26f2              	bne	L544
1974  041f                   L154:
1975                         ; 1338 						if (byindex == (EE_NUM_BYTES_IN_PRIMARY_DIRTY_ARRAY - 1)) {
1977  041f c109              	cmpb	#9
1978  0421 2604              	bne	L354
1979                         ; 1341 							byChecksumIndex = EE_CXSUM_FIRST_LOCATION;
1981  0423 c601              	ldab	#1
1983  0425 207b              	bra	L164
1984  0427                   L354:
1985                         ; 1349 							byChecksumIndex = EE_CXSUM_FIRST_LOCATION + 1;
1987  0427 c602              	ldab	#2
1988  0429 2077              	bra	L164
1989  042b                   L754:
1990                         ; 1367 							if ( EE_CXSUM_FLAG_IS_SET(abyEECxsumUpdateReq, byChecksumIndex) ) {
1992  042b 87                	clra	
1993  042c b746              	tfr	d,y
1994  042e 1857              	asry	
1995  0430 1857              	asry	
1996  0432 1857              	asry	
1997  0434 c407              	andb	#7
1998  0436 b745              	tfr	d,x
1999  0438 e6e20000          	ldab	L3_abyEEMaskTable,x
2000  043c e4ea07dc          	andb	L31_abyEECxsumUpdateReq,y
2001  0440 275c              	beq	L564
2002                         ; 1371 								lCxsumResult = (UInt32)((UInt16)(-(EE_BlockCxsum(aEECxsumTable[byChecksumIndex].wStartOffset, aEECxsumTable[byChecksumIndex].wEndOffset))));
2004  0442 e682              	ldab	OFST-13,s
2005  0444 8606              	ldaa	#6
2006  0446 12                	mul	
2007  0447 b746              	tfr	d,y
2008  0449 ecea0003          	ldd	_aEECxsumTable+3,y
2009  044d 3b                	pshd	
2010  044e e684              	ldab	OFST-11,s
2011  0450 8606              	ldaa	#6
2012  0452 12                	mul	
2013  0453 b746              	tfr	d,y
2014  0455 ecea0001          	ldd	_aEECxsumTable+1,y
2015  0459 4a0cc6c6          	call	L74f_EE_BlockCxsum
2017  045d 1b82              	leas	2,s
2018  045f 40                	nega	
2019  0460 50                	negb	
2020  0461 8200              	sbca	#0
2021  0463 1887              	clrx	
2022  0465 6c8d              	std	OFST-2,s
2023  0467 6e8b              	stx	OFST-4,s
2024                         ; 1373 								EE_BlockWrite(aEECxsumTable[byChecksumIndex].eCxsumBlockLoc, (UInt8*)(&lCxsumResult));
2026  0469 1a8b              	leax	OFST-4,s
2027  046b 34                	pshx	
2028  046c e684              	ldab	OFST-11,s
2029  046e 8606              	ldaa	#6
2030  0470 12                	mul	
2031  0471 b746              	tfr	d,y
2032  0473 e6ea0000          	ldab	_aEECxsumTable,y
2033  0477 87                	clra	
2034  0478 4a056b6b          	call	f_EE_BlockWrite
2036  047c 1b82              	leas	2,s
2037                         ; 1375 								EE_CLR_CXSUM_FLAG(abyEECxsumUpdateReq, byChecksumIndex);
2039  047e e682              	ldab	OFST-13,s
2040  0480 87                	clra	
2041  0481 b746              	tfr	d,y
2042  0483 1857              	asry	
2043  0485 1857              	asry	
2044  0487 1857              	asry	
2045  0489 c407              	andb	#7
2046  048b b745              	tfr	d,x
2047  048d e6e20000          	ldab	L3_abyEEMaskTable,x
2048  0491 51                	comb	
2049  0492 e4ea07dc          	andb	L31_abyEECxsumUpdateReq,y
2050  0496 6bea07dc          	stab	L31_abyEECxsumUpdateReq,y
2051                         ; 1377 								bEEWriteHandled = ITBM_TRUE;
2053  049a c601              	ldab	#1
2054  049c 6b84              	stab	OFST-11,s
2055  049e                   L564:
2056                         ; 1384 							byChecksumIndex+=2;
2058  049e e682              	ldab	OFST-13,s
2059  04a0 cb02              	addb	#2
2060  04a2                   L164:
2061  04a2 6b82              	stab	OFST-13,s
2062                         ; 1360 						while (byChecksumIndex < EE_CXSUM_NUM_BLOCKS) {
2064  04a4 c103              	cmpb	#3
2065  04a6 2583              	blo	L754
2066  04a8 1820ff65          	bra	L544
2067  04ac                   L104:
2068                         ; 1163 			for (byindex = 0x00; byindex < EE_NUM_BYTES_IN_DIRTY_ARRAY && !bEEWriteHandled; byindex++) {
2070  04ac e683              	ldab	OFST-12,s
2071  04ae c114              	cmpb	#20
2072  04b0 2406              	bhs	L764
2074  04b2 e784              	tst	OFST-11,s
2075  04b4 1827fe71          	beq	L573
2076  04b8                   L764:
2077                         ; 1405 			if ( bEEWriteHandled == ITBM_FALSE ) {
2079  04b8 e684              	ldab	OFST-11,s
2080  04ba 265d              	bne	L763
2081                         ; 1412 				if ( sEEStatusFlags.eeCxsumIsValid == ITBM_FALSE ) {
2083  04bc 1e08a50126        	brset	L7_sEEStatusFlags,1,L374
2084                         ; 1416 					sEEStatusFlags.eeCxsumIsValid = ITBM_TRUE;
2086  04c1 1c08a501          	bset	L7_sEEStatusFlags,1
2087                         ; 1419 					*((unsigned long *)abyEEWriteBuff) = (UInt32)(EE_CXSUM_IS_VALID_DATA);
2089  04c5 cca55a            	ldd	#-23206
2090  04c8 7c07df            	std	L11_abyEEWriteBuff+2
2091  04cb cc5aa5            	ldd	#23205
2092  04ce 7c07dd            	std	L11_abyEEWriteBuff
2093                         ; 1421 					EE_ForcedWrite(EE_CXSUM_INVALID_FLAG_ADDRESS, abyEEWriteBuff, EE_SECTOR_SIZE);
2095  04d1 cc0004            	ldd	#4
2096  04d4 3b                	pshd	
2097  04d5 cc07dd            	ldd	#L11_abyEEWriteBuff
2098  04d8 3b                	pshd	
2099  04d9 ce0010            	ldx	#16
2100  04dc cc0c00            	ldd	#3072
2101  04df 4a000000          	call	f_EE_ForcedWrite
2103  04e3 1b84              	leas	4,s
2105  04e5 2032              	bra	L763
2106  04e7                   L374:
2107                         ; 1433 					if ( sEEStatusFlags.eeBkupInitializeReq == ITBM_TRUE ) {
2109  04e7 1f08a5081c        	brclr	L7_sEEStatusFlags,8,L774
2110                         ; 1436 						lLongBlockWrite = EE_BACKUPS_INITALIZED;
2112  04ec cca55a            	ldd	#-23206
2113  04ef 6c87              	std	OFST-8,s
2114  04f1 cc5aa5            	ldd	#23205
2115  04f4 6c85              	std	OFST-10,s
2116                         ; 1437 						EE_BlockWrite(EE_BACKUPS_INITALIZED_BLOCK, (UInt8*)&lLongBlockWrite);
2118  04f6 1a85              	leax	OFST-10,s
2119  04f8 34                	pshx	
2120  04f9 cc004c            	ldd	#76
2121  04fc 4a056b6b          	call	f_EE_BlockWrite
2123  0500 1b82              	leas	2,s
2124                         ; 1438 						sEEStatusFlags.eeBkupInitializeReq = ITBM_FALSE;
2126  0502 1d08a508          	bclr	L7_sEEStatusFlags,8
2128  0506 2011              	bra	L763
2129  0508                   L774:
2130                         ; 1451 						if ( sEEStatusFlags.eeInitialized == ITBM_FALSE ) {
2132  0508 1e08a50408        	brset	L7_sEEStatusFlags,4,L305
2133                         ; 1455 							sEEStatusFlags.eeInitialized = ITBM_TRUE;
2135  050d 1c08a504          	bset	L7_sEEStatusFlags,4
2136                         ; 1458 							EE_VerifyAllCxsum();
2138  0511 4a0d0a0a          	call	L15f_EE_VerifyAllCxsum
2140  0515                   L305:
2141                         ; 1465 						sEEStatusFlags.eeManagerBusy = ITBM_FALSE;
2143  0515 1d08a502          	bclr	L7_sEEStatusFlags,2
2144  0519                   L763:
2145                         ; 1493 	EE_Handler(); 
2147  0519 4a000000          	call	f_EE_Handler
2149                         ; 1500 	if (EE_IsBusy() != ITBM_TRUE) {
2151  051d 4a000000          	call	f_EE_IsBusy
2153  0521 04012b            	dbeq	b,L505
2154                         ; 1503 		if (diag_load_bl_eep_backup_b == 1) {
2156  0524 1f00004026        	brclr	_diag_io_lock3_flags_c,64,L505
2157                         ; 1506 			if (ee_wait_counter_c > 250) {
2159  0529 fc0006            	ldd	_ee_wait_counter_c
2160  052c 8c00fa            	cpd	#250
2161  052f 231a              	bls	L115
2162                         ; 1510 				WriteFBLDFlashByte((kEepAddressEcuHwVerNumber),  (UINT8*)&sEEPConstants.fiat_ecu_hw_version_c,1);
2164  0531 cc0001            	ldd	#1
2165  0534 3b                	pshd	
2166  0535 cc0313            	ldd	#_sEEPConstants+779
2167  0538 3b                	pshd	
2168  0539 ce0000            	ldx	#0
2169  053c cc1f6c            	ldd	#8044
2170  053f 4a000000          	call	f_WriteFBLDFlashByte
2172  0543 1b84              	leas	4,s
2173                         ; 1511 				diag_load_bl_eep_backup_b = 0;
2175  0545 1d000040          	bclr	_diag_io_lock3_flags_c,64
2177  0549 2004              	bra	L505
2178  054b                   L115:
2179                         ; 1514 				ee_wait_counter_c++;
2181  054b 18720006          	incw	_ee_wait_counter_c
2182  054f                   L505:
2183                         ; 1524 }
2186  054f 1b8f              	leas	15,s
2187  0551 0a                	rtc	
2280                         ; 1539 SEEStatusFlags_t EE_MANAGER_CALL EE_GetStatusFlags(void)
2280                         ; 1540 {
2281                         	switch	.ftext
2282  0552                   f_EE_GetStatusFlags:
2284       00000000          OFST:	set	0
2287                         ; 1544 	return sEEStatusFlags;
2289  0552 ed83              	ldy	OFST+3,s
2290  0554 18094008a5        	movb	L7_sEEStatusFlags,0,y
2293  0559 0a                	rtc	
2317                         ; 1562 UInt8 EE_MANAGER_CALL EE_ManagerIsBusy(void)
2317                         ; 1563 {
2318                         	switch	.ftext
2319  055a                   f_EE_ManagerIsBusy:
2323                         ; 1570 	if ( sEEStatusFlags.eeManagerBusy == ITBM_TRUE || EE_IsBusy() == ITBM_TRUE ) {
2325  055a 1e08a50207        	brset	L7_sEEStatusFlags,2,L375
2327  055f 4a000000          	call	f_EE_IsBusy
2329  0563 042103            	dbne	b,L175
2330  0566                   L375:
2331                         ; 1573 		return ITBM_TRUE;
2333  0566 c601              	ldab	#1
2336  0568 0a                	rtc	
2337  0569                   L175:
2338                         ; 1581 		return ITBM_FALSE;
2340  0569 c7                	clrb	
2343  056a 0a                	rtc	
3045                         ; 1603 @far void /*EE_MANAGER_CALL*/ EE_BlockWrite(EEBlockLocation elocation, UInt8 *pbysrc)
3045                         ; 1604 {
3046                         	switch	.ftext
3047  056b                   f_EE_BlockWrite:
3049  056b 3b                	pshd	
3050  056c 1b99              	leas	-7,s
3051       00000007          OFST:	set	7
3054                         ; 1613 	wsize = EE_LOCATION_SIZE(elocation);
3056  056e 860b              	ldaa	#11
3057  0570 12                	mul	
3058  0571 b746              	tfr	d,y
3059  0573 1802ea000785      	movw	_aEEBlockTable+7,y,OFST-2,s
3060                         ; 1616 	wDataOffset = (UInt16)(aEEBlockTable[elocation].wRAMOffset);
3062  0579 e688              	ldab	OFST+1,s
3063  057b 860b              	ldaa	#11
3064  057d 12                	mul	
3065  057e b746              	tfr	d,y
3066  0580 1802ea000982      	movw	_aEEBlockTable+9,y,OFST-5,s
3067                         ; 1622 	bUpdateNeeded = ITBM_FALSE;
3069  0586 6984              	clr	OFST-3,s
3070                         ; 1626 	for (windex = 0; windex < wsize; windex++) {
3072  0588 186980            	clrw	OFST-7,s
3074  058b 2047              	bra	L5011
3075  058d                   L1011:
3076                         ; 1633 		if ( byShadowRam[wDataOffset + windex] != pbysrc[windex] ) {
3078  058d b746              	tfr	d,y
3079  058f ec82              	ldd	OFST-5,s
3080  0591 19ee              	leay	d,y
3081  0593 e6ea0008          	ldab	L52_byShadowRam,y
3082  0597 ed8c              	ldy	OFST+5,s
3083  0599 18eb80            	addy	OFST-7,s
3084  059c e140              	cmpb	0,y
3085  059e 272f              	beq	L1111
3086                         ; 1637 			bUpdateNeeded = ITBM_TRUE;
3088  05a0 c601              	ldab	#1
3089  05a2 6b84              	stab	OFST-3,s
3090                         ; 1640 			EE_MAKE_SECTOR_DIRTY(elocation);
3092  05a4 e688              	ldab	OFST+1,s
3093  05a6 87                	clra	
3094  05a7 b746              	tfr	d,y
3095  05a9 1857              	asry	
3096  05ab 1857              	asry	
3097  05ad 1857              	asry	
3098  05af c407              	andb	#7
3099  05b1 b745              	tfr	d,x
3100  05b3 e6e20000          	ldab	L3_abyEEMaskTable,x
3101  05b7 eaea08a6          	orab	L5_abyEEDirtyBits,y
3102  05bb 6bea08a6          	stab	L5_abyEEDirtyBits,y
3103                         ; 1643 			byShadowRam[wDataOffset + windex] = pbysrc[windex];
3105  05bf ed80              	ldy	OFST-7,s
3106  05c1 ec82              	ldd	OFST-5,s
3107  05c3 19ee              	leay	d,y
3108  05c5 ee80              	ldx	OFST-7,s
3109  05c7 ec8c              	ldd	OFST+5,s
3110  05c9 180ae6ea0008      	movb	d,x,L52_byShadowRam,y
3111  05cf                   L1111:
3112                         ; 1626 	for (windex = 0; windex < wsize; windex++) {
3114  05cf ee80              	ldx	OFST-7,s
3115  05d1 08                	inx	
3116  05d2 6e80              	stx	OFST-7,s
3117  05d4                   L5011:
3120  05d4 ec80              	ldd	OFST-7,s
3121  05d6 ac85              	cpd	OFST-2,s
3122  05d8 25b3              	blo	L1011
3123                         ; 1657 	if ( aEEBlockTable[elocation].wBackupOffset != EE_NO_BKUP_OFST ) {
3125  05da e688              	ldab	OFST+1,s
3126  05dc 860b              	ldaa	#11
3127  05de 12                	mul	
3128  05df b746              	tfr	d,y
3129  05e1 ecea0005          	ldd	_aEEBlockTable+5,y
3130  05e5 048462            	ibeq	d,L3111
3131                         ; 1660 		wDataOffset += SHADOW_BACKUP_START_OFFSET;
3133  05e8 ec82              	ldd	OFST-5,s
3134  05ea c303e8            	addd	#1000
3135  05ed 6c82              	std	OFST-5,s
3136                         ; 1664 		for (windex = 0; windex < wsize; windex++) {
3138  05ef 186980            	clrw	OFST-7,s
3140  05f2 2050              	bra	L1211
3141  05f4                   L5111:
3142                         ; 1671 			if ( byShadowRam[wDataOffset + windex] != pbysrc[windex] ) {
3144  05f4 ec82              	ldd	OFST-5,s
3145  05f6 19ee              	leay	d,y
3146  05f8 e6ea0008          	ldab	L52_byShadowRam,y
3147  05fc ed8c              	ldy	OFST+5,s
3148  05fe 18eb80            	addy	OFST-7,s
3149  0601 e140              	cmpb	0,y
3150  0603 273a              	beq	L5211
3151                         ; 1675 				bUpdateNeeded = ITBM_TRUE;
3153  0605 c601              	ldab	#1
3154  0607 6b84              	stab	OFST-3,s
3155                         ; 1678 				EE_MAKE_SECTOR_DIRTY(elocation + EE_NUM_PRIMARY_IDS);
3157  0609 e688              	ldab	OFST+1,s
3158  060b 87                	clra	
3159  060c c30050            	addd	#80
3160  060f ce0008            	ldx	#8
3161  0612 1815              	idivs	
3162  0614 b756              	tfr	x,y
3163  0616 e688              	ldab	OFST+1,s
3164  0618 87                	clra	
3165  0619 c30050            	addd	#80
3166  061c ce0008            	ldx	#8
3167  061f 1815              	idivs	
3168  0621 b745              	tfr	d,x
3169  0623 e6e20000          	ldab	L3_abyEEMaskTable,x
3170  0627 eaea08a6          	orab	L5_abyEEDirtyBits,y
3171  062b 6bea08a6          	stab	L5_abyEEDirtyBits,y
3172                         ; 1681 				byShadowRam[wDataOffset + windex] = pbysrc[windex];
3174  062f ed80              	ldy	OFST-7,s
3175  0631 ec82              	ldd	OFST-5,s
3176  0633 19ee              	leay	d,y
3177  0635 ee80              	ldx	OFST-7,s
3178  0637 ec8c              	ldd	OFST+5,s
3179  0639 180ae6ea0008      	movb	d,x,L52_byShadowRam,y
3180  063f                   L5211:
3181                         ; 1664 		for (windex = 0; windex < wsize; windex++) {
3183  063f ee80              	ldx	OFST-7,s
3184  0641 08                	inx	
3185  0642 6e80              	stx	OFST-7,s
3186  0644                   L1211:
3189  0644 ed80              	ldy	OFST-7,s
3190  0646 ad85              	cpy	OFST-2,s
3191  0648 25aa              	blo	L5111
3192  064a                   L3111:
3193                         ; 1699 	if (bUpdateNeeded == ITBM_TRUE) {
3195  064a e684              	ldab	OFST-3,s
3196  064c 53                	decb	
3197  064d 266d              	bne	L7211
3198                         ; 1706 		if ( aEEBlockTable[elocation].sChecksumLoc.byChecksumBlock != EE_CXSUM_NONE ) {
3200  064f e688              	ldab	OFST+1,s
3201  0651 860b              	ldaa	#11
3202  0653 12                	mul	
3203  0654 b746              	tfr	d,y
3204  0656 e6ea0000          	ldab	_aEEBlockTable,y
3205  065a c50f              	bitb	#15
3206  065c 275e              	beq	L7211
3207                         ; 1710 			EE_SET_CXSUM_FLAG(abyEECxsumUpdateReq, aEEBlockTable[elocation].sChecksumLoc.byChecksumBlock);
3209  065e c40f              	andb	#15
3210  0660 87                	clra	
3211  0661 b745              	tfr	d,x
3212  0663 1844              	lsrx	
3213  0665 1844              	lsrx	
3214  0667 1844              	lsrx	
3215  0669 e6ea0000          	ldab	_aEEBlockTable,y
3216  066d c407              	andb	#7
3217  066f b746              	tfr	d,y
3218  0671 e6ea0000          	ldab	L3_abyEEMaskTable,y
3219  0675 eae207dc          	orab	L31_abyEECxsumUpdateReq,x
3220  0679 6be207dc          	stab	L31_abyEECxsumUpdateReq,x
3221                         ; 1714 			if ( aEEBlockTable[elocation].sChecksumLoc.byBkupChecksumBlock != EE_CXSUM_NONE ) {
3223  067d e688              	ldab	OFST+1,s
3224  067f 860b              	ldaa	#11
3225  0681 12                	mul	
3226  0682 b746              	tfr	d,y
3227  0684 0fea0000f029      	brclr	_aEEBlockTable,y,240,L3311
3228                         ; 1718 				EE_SET_CXSUM_FLAG(abyEECxsumUpdateReq, aEEBlockTable[elocation].sChecksumLoc.byBkupChecksumBlock);
3230  068a e6ea0000          	ldab	_aEEBlockTable,y
3231  068e 54                	lsrb	
3232  068f 54                	lsrb	
3233  0690 54                	lsrb	
3234  0691 54                	lsrb	
3235  0692 87                	clra	
3236  0693 b745              	tfr	d,x
3237  0695 1844              	lsrx	
3238  0697 1844              	lsrx	
3239  0699 1844              	lsrx	
3240  069b e6ea0000          	ldab	_aEEBlockTable,y
3241  069f c470              	andb	#112
3242  06a1 54                	lsrb	
3243  06a2 54                	lsrb	
3244  06a3 54                	lsrb	
3245  06a4 54                	lsrb	
3246  06a5 b746              	tfr	d,y
3247  06a7 e6ea0000          	ldab	L3_abyEEMaskTable,y
3248  06ab eae207dc          	orab	L31_abyEECxsumUpdateReq,x
3249  06af 6be207dc          	stab	L31_abyEECxsumUpdateReq,x
3250  06b3                   L3311:
3251                         ; 1725 			if ( sEEStatusFlags.eeCxsumIsValid == ITBM_TRUE ) {
3253  06b3 1f08a50104        	brclr	L7_sEEStatusFlags,1,L7211
3254                         ; 1729 				sEEStatusFlags.eeWriteChecksumFlag = ITBM_TRUE;
3256  06b8 1c08a510          	bset	L7_sEEStatusFlags,16
3257  06bc                   L7211:
3258                         ; 1742 }
3261  06bc 1b89              	leas	9,s
3262  06be 0a                	rtc	
3331                         ; 1914 void EE_MANAGER_CALL EE_BlockRead(EEBlockLocation elocation, UInt8 *pbydest)
3331                         ; 1915 {
3332                         	switch	.ftext
3333  06bf                   f_EE_BlockRead:
3335  06bf 3b                	pshd	
3336  06c0 1b9a              	leas	-6,s
3337       00000006          OFST:	set	6
3340                         ; 1923 	wsize = EE_LOCATION_SIZE(elocation);
3342  06c2 860b              	ldaa	#11
3343  06c4 12                	mul	
3344  06c5 b746              	tfr	d,y
3345  06c7 1802ea000784      	movw	_aEEBlockTable+7,y,OFST-2,s
3346                         ; 1933 	if ( (aEEBlockTable[elocation].wBackupOffset != EE_NO_BKUP_OFST) &&
3346                         ; 1934 	   (EE_CXSUM_FLAG_IS_SET(abyEECxsumFailedFlags, aEEBlockTable[elocation].sChecksumLoc.byChecksumBlock)) ) {
3348  06cd e687              	ldab	OFST+1,s
3349  06cf 860b              	ldaa	#11
3350  06d1 12                	mul	
3351  06d2 b746              	tfr	d,y
3352  06d4 ecea0005          	ldd	_aEEBlockTable+5,y
3353  06d8 048431            	ibeq	d,L7611
3355  06db e6ea0000          	ldab	_aEEBlockTable,y
3356  06df c40f              	andb	#15
3357  06e1 87                	clra	
3358  06e2 b745              	tfr	d,x
3359  06e4 1844              	lsrx	
3360  06e6 1844              	lsrx	
3361  06e8 1844              	lsrx	
3362  06ea e6ea0000          	ldab	_aEEBlockTable,y
3363  06ee c407              	andb	#7
3364  06f0 b746              	tfr	d,y
3365  06f2 e6ea0000          	ldab	L3_abyEEMaskTable,y
3366  06f6 e4e207db          	andb	L51_abyEECxsumFailedFlags,x
3367  06fa 2710              	beq	L7611
3368                         ; 1939 		wDataOffset = (UInt16)(aEEBlockTable[elocation].wRAMOffset + SHADOW_BACKUP_START_OFFSET);
3370  06fc e687              	ldab	OFST+1,s
3371  06fe 860b              	ldaa	#11
3372  0700 12                	mul	
3373  0701 b746              	tfr	d,y
3374  0703 ecea0009          	ldd	_aEEBlockTable+9,y
3375  0707 c303e8            	addd	#1000
3377  070a 200b              	bra	L1711
3378  070c                   L7611:
3379                         ; 1948 		wDataOffset = (UInt16)(aEEBlockTable[elocation].wRAMOffset);
3381  070c e687              	ldab	OFST+1,s
3382  070e 860b              	ldaa	#11
3383  0710 12                	mul	
3384  0711 b746              	tfr	d,y
3385  0713 ecea0009          	ldd	_aEEBlockTable+9,y
3386  0717                   L1711:
3387  0717 6c82              	std	OFST-4,s
3388                         ; 1962 	for (windex = 0; windex < wsize; windex++) {
3390  0719 186980            	clrw	OFST-6,s
3392  071c ec8b              	ldd	OFST+5,s
3393  071e 200e              	bra	L7711
3394  0720                   L3711:
3395                         ; 1965 		pbydest[windex] = byShadowRam[wDataOffset + windex];
3397  0720 ee82              	ldx	OFST-4,s
3398  0722 18ab80            	addx	OFST-6,s
3399  0725 180ae20008ee      	movb	L52_byShadowRam,x,d,y
3400                         ; 1962 	for (windex = 0; windex < wsize; windex++) {
3402  072b 186280            	incw	OFST-6,s
3403  072e                   L7711:
3406  072e ed80              	ldy	OFST-6,s
3407  0730 ad84              	cpy	OFST-2,s
3408  0732 25ec              	blo	L3711
3409                         ; 1977 }
3412  0734 1b88              	leas	8,s
3413  0736 0a                	rtc	
3537                         ; 2072 UInt8 EE_MANAGER_CALL EE_DirectWrite(UInt32 eedestaddr, UInt8 *pbysrc, UInt8 bysize)
3537                         ; 2073 {
3538                         	switch	.ftext
3539  0737                   f_EE_DirectWrite:
3541  0737 3b                	pshd	
3542  0738 34                	pshx	
3543  0739 1b92              	leas	-14,s
3544       0000000e          OFST:	set	14
3547                         ; 2091 	if ( (eedestaddr < EE_EEPROM_START_PHY_ADDRESS) || ((eedestaddr+bysize) >= EE_EEPROM_END_PHY_ADDRESS) ) {
3549  073b 8c0000            	cpd	#0
3550  073e 188e0010          	cpex	#16
3551  0742 18250241          	blo	L5031
3553  0746 e6f018            	ldab	OFST+10,s
3554  0749 87                	clra	
3555  074a 1887              	clrx	
3556  074c e3f010            	addd	OFST+2,s
3557  074f 18a98e            	adex	OFST+0,s
3558  0752 8c2000            	cpd	#8192
3559  0755 188e0010          	cpex	#16
3560                         ; 2095 		return 1;
3563  0759 1824022a          	bhs	L5031
3564                         ; 2106 	wLogicalStartOffst = eedestaddr - EE_EEPROM_START_PHY_ADDRESS;
3566  075d 1802f01086        	movw	OFST+2,s,OFST-8,s
3567                         ; 2107 	wLogicalEndOffst = (eedestaddr+(bysize-1)) - EE_EEPROM_START_PHY_ADDRESS;
3569  0762 e6f018            	ldab	OFST+10,s
3570  0765 b796              	exg	b,y
3571  0767 03                	dey	
3572  0768 18ebf010          	addy	OFST+2,s
3573  076c 1940              	leay	0,y
3574  076e 6d8c              	sty	OFST-2,s
3575                         ; 2110 	EEBlockIndex_temp = EE_INVALID_BLOCK_ID;
3577  0770 c6ff              	ldab	#255
3578  0772 6b85              	stab	OFST-9,s
3579                         ; 2116 	for (byBlockIndex = 1; ((byBlockIndex < EEADDR_TAB_SIZE) && (EEBlockIndex_temp == EE_INVALID_BLOCK_ID)); byBlockIndex++) {
3581  0774 c601              	ldab	#1
3582  0776 6b84              	stab	OFST-10,s
3584  0778 182000b4          	bra	L1621
3585  077c                   L5521:
3586                         ; 2123 		if ((wLogicalStartOffst >= aEEBlockAddrTable[byBlockIndex].wOffset ) &&
3586                         ; 2124 		   (wLogicalStartOffst < (aEEBlockAddrTable[byBlockIndex].wOffset + aEEBlockAddrTable[byBlockIndex].wSize))) {
3588  077c e684              	ldab	OFST-10,s
3589  077e 8605              	ldaa	#5
3590  0780 12                	mul	
3591  0781 b746              	tfr	d,y
3592  0783 ecea0001          	ldd	_aEEBlockAddrTable+1,y
3593  0787 ac86              	cpd	OFST-8,s
3594  0789 224a              	bhi	L5621
3596  078b e684              	ldab	OFST-10,s
3597  078d 8605              	ldaa	#5
3598  078f 12                	mul	
3599  0790 b745              	tfr	d,x
3600  0792 ece20003          	ldd	_aEEBlockAddrTable+3,x
3601  0796 e3ea0001          	addd	_aEEBlockAddrTable+1,y
3602  079a ac86              	cpd	OFST-8,s
3603  079c 2337              	bls	L5621
3604                         ; 2131 			if (aEEBlockAddrTable[byBlockIndex].sEEBlockLoc < EE_NUM_PRIMARY_IDS) {
3606  079e e6ea0000          	ldab	_aEEBlockAddrTable,y
3607  07a2 c150              	cmpb	#80
3608  07a4 2407              	bhs	L7621
3609                         ; 2134 				EEBlockIndex_temp = aEEBlockAddrTable[byBlockIndex].sEEBlockLoc;
3611  07a6 6b85              	stab	OFST-9,s
3612                         ; 2135 				ShadowBackupOffset_temp = 0;
3614  07a8 186988            	clrw	OFST-6,s
3616  07ab 2014              	bra	L1721
3617  07ad                   L7621:
3618                         ; 2147 				EEBlockIndex_temp = aEEBlockAddrTable[byBlockIndex].sEEBlockLoc - EE_NUM_PRIMARY_IDS;
3620  07ad e684              	ldab	OFST-10,s
3621  07af 8605              	ldaa	#5
3622  07b1 12                	mul	
3623  07b2 b746              	tfr	d,y
3624  07b4 e6ea0000          	ldab	_aEEBlockAddrTable,y
3625  07b8 c050              	subb	#80
3626  07ba 6b85              	stab	OFST-9,s
3627                         ; 2148 				ShadowBackupOffset_temp = SHADOW_BACKUP_START_OFFSET;
3629  07bc cc03e8            	ldd	#1000
3630  07bf 6c88              	std	OFST-6,s
3631  07c1                   L1721:
3632                         ; 2155 			ShadowAddrOffset_temp = wLogicalStartOffst - aEEBlockAddrTable[byBlockIndex].wOffset;
3634  07c1 e684              	ldab	OFST-10,s
3635  07c3 8605              	ldaa	#5
3636  07c5 12                	mul	
3637  07c6 b746              	tfr	d,y
3638  07c8 ec86              	ldd	OFST-8,s
3639  07ca a3ea0001          	subd	_aEEBlockAddrTable+1,y
3640  07ce 6c8a              	std	OFST-4,s
3641                         ; 2156 			srcAddrOffst = 0;
3643  07d0 186982            	clrw	OFST-12,s
3645  07d3 2057              	bra	L3721
3646  07d5                   L5621:
3647                         ; 2168 			if ((wLogicalStartOffst < aEEBlockAddrTable[byBlockIndex].wOffset) &&
3647                         ; 2169 			   (wLogicalStartOffst >= (aEEBlockAddrTable[byBlockIndex-1].wOffset + aEEBlockAddrTable[byBlockIndex - 1].wSize))) {
3649  07d5 e684              	ldab	OFST-10,s
3650  07d7 8605              	ldaa	#5
3651  07d9 12                	mul	
3652  07da b746              	tfr	d,y
3653  07dc ecea0001          	ldd	_aEEBlockAddrTable+1,y
3654  07e0 ac86              	cpd	OFST-8,s
3655  07e2 2348              	bls	L3721
3657  07e4 e684              	ldab	OFST-10,s
3658  07e6 8605              	ldaa	#5
3659  07e8 12                	mul	
3660  07e9 b745              	tfr	d,x
3661  07eb ece2fffe          	ldd	_aEEBlockAddrTable-2,x
3662  07ef e3eafffc          	addd	_aEEBlockAddrTable-4,y
3663  07f3 ac86              	cpd	OFST-8,s
3664  07f5 2235              	bhi	L3721
3665                         ; 2176 				if (aEEBlockAddrTable[byBlockIndex].sEEBlockLoc < EE_NUM_PRIMARY_IDS) {
3667  07f7 e6ea0000          	ldab	_aEEBlockAddrTable,y
3668  07fb c150              	cmpb	#80
3669  07fd 2407              	bhs	L7721
3670                         ; 2179 					EEBlockIndex_temp = aEEBlockAddrTable[byBlockIndex].sEEBlockLoc;
3672  07ff 6b85              	stab	OFST-9,s
3673                         ; 2180 					ShadowBackupOffset_temp = 0;
3675  0801 186988            	clrw	OFST-6,s
3677  0804 2014              	bra	L1031
3678  0806                   L7721:
3679                         ; 2192 					EEBlockIndex_temp = aEEBlockAddrTable[byBlockIndex].sEEBlockLoc - EE_NUM_PRIMARY_IDS;
3681  0806 e684              	ldab	OFST-10,s
3682  0808 8605              	ldaa	#5
3683  080a 12                	mul	
3684  080b b746              	tfr	d,y
3685  080d e6ea0000          	ldab	_aEEBlockAddrTable,y
3686  0811 c050              	subb	#80
3687  0813 6b85              	stab	OFST-9,s
3688                         ; 2193 					ShadowBackupOffset_temp = SHADOW_BACKUP_START_OFFSET;
3690  0815 cc03e8            	ldd	#1000
3691  0818 6c88              	std	OFST-6,s
3692  081a                   L1031:
3693                         ; 2200 				ShadowAddrOffset_temp = 0;
3695  081a 18698a            	clrw	OFST-4,s
3696                         ; 2201 				srcAddrOffst =  aEEBlockAddrTable[byBlockIndex].wOffset - wLogicalStartOffst;
3698  081d e684              	ldab	OFST-10,s
3699  081f 8605              	ldaa	#5
3700  0821 12                	mul	
3701  0822 b746              	tfr	d,y
3702  0824 ecea0001          	ldd	_aEEBlockAddrTable+1,y
3703  0828 a386              	subd	OFST-8,s
3704  082a 6c82              	std	OFST-12,s
3705  082c                   L3721:
3706                         ; 2116 	for (byBlockIndex = 1; ((byBlockIndex < EEADDR_TAB_SIZE) && (EEBlockIndex_temp == EE_INVALID_BLOCK_ID)); byBlockIndex++) {
3708  082c 6284              	inc	OFST-10,s
3709  082e e684              	ldab	OFST-10,s
3710  0830                   L1621:
3713  0830 c14c              	cmpb	#76
3714  0832 2407              	bhs	L3031
3716  0834 e685              	ldab	OFST-9,s
3717  0836 52                	incb	
3718  0837 1827ff41          	beq	L5521
3719  083b                   L3031:
3720                         ; 2218 	if ((EEBlockIndex_temp != EE_INVALID_BLOCK_ID) && (srcAddrOffst < bysize)) {
3722  083b e685              	ldab	OFST-9,s
3723  083d 52                	incb	
3724  083e 18270145          	beq	L5031
3726  0842 e6f018            	ldab	OFST+10,s
3727  0845 87                	clra	
3728  0846 ac82              	cpd	OFST-12,s
3729  0848 1823013b          	bls	L5031
3730                         ; 2222 		bUpdateNeeded = ITBM_FALSE;
3732  084c 6984              	clr	OFST-10,s
3733                         ; 2227 		byindex = srcAddrOffst;
3736  084e 182000bc          	bra	L3131
3737  0852                   L7031:
3738                         ; 2238 			if ( byShadowRam[aEEBlockTable[EEBlockIndex_temp].wRAMOffset + ShadowAddrOffset_temp + ShadowBackupOffset_temp] != pbysrc[byindex] ) {
3740  0852 ed8a              	ldy	OFST-4,s
3741  0854 e685              	ldab	OFST-9,s
3742  0856 860b              	ldaa	#11
3743  0858 12                	mul	
3744  0859 b745              	tfr	d,x
3745  085b ece20009          	ldd	_aEEBlockTable+9,x
3746  085f 19ee              	leay	d,y
3747  0861 ec88              	ldd	OFST-6,s
3748  0863 19ee              	leay	d,y
3749  0865 e6ea0008          	ldab	L52_byShadowRam,y
3750  0869 edf015            	ldy	OFST+7,s
3751  086c 18eb82            	addy	OFST-12,s
3752  086f e140              	cmpb	0,y
3753  0871 275c              	beq	L7131
3754                         ; 2242 				bUpdateNeeded = ITBM_TRUE;
3756  0873 c601              	ldab	#1
3757  0875 6b84              	stab	OFST-10,s
3758                         ; 2248 				if (ShadowBackupOffset_temp < SHADOW_BACKUP_START_OFFSET) {
3760  0877 ec88              	ldd	OFST-6,s
3761  0879 8c03e8            	cpd	#1000
3762  087c e685              	ldab	OFST-9,s
3763  087e 240d              	bhs	L1231
3764                         ; 2251 					EE_MAKE_SECTOR_DIRTY(EEBlockIndex_temp);
3766  0880 87                	clra	
3767  0881 b746              	tfr	d,y
3768  0883 1857              	asry	
3769  0885 1857              	asry	
3770  0887 1857              	asry	
3771  0889 c407              	andb	#7
3773  088b 2016              	bra	L3231
3774  088d                   L1231:
3775                         ; 2259 					EE_MAKE_SECTOR_DIRTY(EEBlockIndex_temp + EE_NUM_PRIMARY_IDS);
3777  088d 87                	clra	
3778  088e c30050            	addd	#80
3779  0891 ce0008            	ldx	#8
3780  0894 1815              	idivs	
3781  0896 b756              	tfr	x,y
3782  0898 e685              	ldab	OFST-9,s
3783  089a 87                	clra	
3784  089b c30050            	addd	#80
3785  089e ce0008            	ldx	#8
3786  08a1 1815              	idivs	
3787  08a3                   L3231:
3788  08a3 b745              	tfr	d,x
3789  08a5 e6e20000          	ldab	L3_abyEEMaskTable,x
3790  08a9 eaea08a6          	orab	L5_abyEEDirtyBits,y
3791  08ad 6bea08a6          	stab	L5_abyEEDirtyBits,y
3792                         ; 2267 				byShadowRam[aEEBlockTable[EEBlockIndex_temp].wRAMOffset + ShadowAddrOffset_temp + ShadowBackupOffset_temp] = pbysrc[byindex];
3794  08b1 ed8a              	ldy	OFST-4,s
3795  08b3 e685              	ldab	OFST-9,s
3796  08b5 860b              	ldaa	#11
3797  08b7 12                	mul	
3798  08b8 b745              	tfr	d,x
3799  08ba ece20009          	ldd	_aEEBlockTable+9,x
3800  08be 19ee              	leay	d,y
3801  08c0 ec88              	ldd	OFST-6,s
3802  08c2 19ee              	leay	d,y
3803  08c4 ee82              	ldx	OFST-12,s
3804  08c6 ecf015            	ldd	OFST+7,s
3805  08c9 180ae6ea0008      	movb	d,x,L52_byShadowRam,y
3806  08cf                   L7131:
3807                         ; 2274 			ShadowAddrOffset_temp++;
3809  08cf 18628a            	incw	OFST-4,s
3810                         ; 2280 			if (ShadowAddrOffset_temp >= aEEBlockTable[EEBlockIndex_temp].wSize) {
3812  08d2 e685              	ldab	OFST-9,s
3813  08d4 860b              	ldaa	#11
3814  08d6 12                	mul	
3815  08d7 b746              	tfr	d,y
3816  08d9 ecea0007          	ldd	_aEEBlockTable+7,y
3817  08dd ac8a              	cpd	OFST-4,s
3818  08df 2227              	bhi	L5231
3819                         ; 2283 				ShadowAddrOffset_temp = 0;
3821  08e1 18698a            	clrw	OFST-4,s
3822                         ; 2284 				EEBlockIndex_temp++;
3824  08e4 6285              	inc	OFST-9,s
3825                         ; 2287 				byindex += (aEEBlockTable[EEBlockIndex_temp].wOffset -
3825                         ; 2288 				            aEEBlockTable[EEBlockIndex_temp - 1].wOffset - aEEBlockTable[EEBlockIndex_temp - 1].wSize);
3827  08e6 e685              	ldab	OFST-9,s
3828  08e8 860b              	ldaa	#11
3829  08ea 12                	mul	
3830  08eb b746              	tfr	d,y
3831  08ed e685              	ldab	OFST-9,s
3832  08ef 860b              	ldaa	#11
3833  08f1 12                	mul	
3834  08f2 b745              	tfr	d,x
3835  08f4 1802e2fff880      	movw	_aEEBlockTable-8,x,OFST-14,s
3836  08fa ece20003          	ldd	_aEEBlockTable+3,x
3837  08fe a380              	subd	OFST-14,s
3838  0900 a3eafffc          	subd	_aEEBlockTable-4,y
3839  0904 e382              	addd	OFST-12,s
3840  0906 6c82              	std	OFST-12,s
3841  0908                   L5231:
3842                         ; 2295 			byindex++;
3844  0908 186282            	incw	OFST-12,s
3845  090b e6f018            	ldab	OFST+10,s
3846  090e                   L3131:
3847                         ; 2231 		while (byindex < bysize) {
3849  090e 87                	clra	
3850  090f ac82              	cpd	OFST-12,s
3851  0911 1822ff3d          	bhi	L7031
3852                         ; 2306 		if (bUpdateNeeded == ITBM_TRUE) {
3854  0915 e684              	ldab	OFST-10,s
3855  0917 53                	decb	
3856  0918 2668              	bne	L7231
3857                         ; 2313 			for (byindex = EE_CXSUM_FIRST_LOCATION; byindex < EE_CXSUM_NUM_BLOCKS; byindex++) {
3859  091a cc0001            	ldd	#1
3860  091d 6c82              	std	OFST-12,s
3861  091f                   L1331:
3862                         ; 2323 				if ( ((wLogicalStartOffst >= aEECxsumTable[byindex].wStartOffset)
3862                         ; 2324 				     && (wLogicalStartOffst <= aEECxsumTable[byindex].wEndOffset))
3862                         ; 2325 				     || ((wLogicalEndOffst >= aEECxsumTable[byindex].wStartOffset)
3862                         ; 2326 				     && (wLogicalEndOffst <= aEECxsumTable[byindex].wEndOffset))
3862                         ; 2327 				     || ((wLogicalStartOffst < aEECxsumTable[byindex].wStartOffset)
3862                         ; 2328 				     && (wLogicalEndOffst > aEECxsumTable[byindex].wEndOffset)) ) {
3864  091f cd0006            	ldy	#6
3865  0922 13                	emul	
3866  0923 b746              	tfr	d,y
3867  0925 ecea0001          	ldd	_aEECxsumTable+1,y
3868  0929 ac86              	cpd	OFST-8,s
3869  092b 220c              	bhi	L3431
3871  092d ecea0003          	ldd	_aEECxsumTable+3,y
3872  0931 ac86              	cpd	OFST-8,s
3873  0933 2420              	bhs	L1431
3874  0935 ecea0001          	ldd	_aEECxsumTable+1,y
3875  0939                   L3431:
3877  0939 ac8c              	cpd	OFST-2,s
3878  093b 220c              	bhi	L7431
3880  093d ecea0003          	ldd	_aEECxsumTable+3,y
3881  0941 ac8c              	cpd	OFST-2,s
3882  0943 2410              	bhs	L1431
3883  0945 ecea0001          	ldd	_aEECxsumTable+1,y
3884  0949                   L7431:
3886  0949 ac86              	cpd	OFST-8,s
3887  094b 232b              	bls	L7331
3889  094d ecea0003          	ldd	_aEECxsumTable+3,y
3890  0951 ac8c              	cpd	OFST-2,s
3891  0953 2423              	bhs	L7331
3892  0955                   L1431:
3893                         ; 2332 					EE_SET_CXSUM_FLAG(abyEECxsumUpdateReq, byindex);
3895  0955 ed82              	ldy	OFST-12,s
3896  0957 1854              	lsry	
3897  0959 1854              	lsry	
3898  095b 1854              	lsry	
3899  095d e683              	ldab	OFST-11,s
3900  095f c407              	andb	#7
3901  0961 b795              	exg	b,x
3902  0963 e6e20000          	ldab	L3_abyEEMaskTable,x
3903  0967 eaea07dc          	orab	L31_abyEECxsumUpdateReq,y
3904  096b 6bea07dc          	stab	L31_abyEECxsumUpdateReq,y
3905                         ; 2336 					if ( sEEStatusFlags.eeCxsumIsValid == ITBM_TRUE ) {
3907  096f 1f08a50104        	brclr	L7_sEEStatusFlags,1,L7331
3908                         ; 2339 						sEEStatusFlags.eeWriteChecksumFlag = ITBM_TRUE;
3910  0974 1c08a510          	bset	L7_sEEStatusFlags,16
3911  0978                   L7331:
3912                         ; 2313 			for (byindex = EE_CXSUM_FIRST_LOCATION; byindex < EE_CXSUM_NUM_BLOCKS; byindex++) {
3914  0978 186282            	incw	OFST-12,s
3917  097b ec82              	ldd	OFST-12,s
3918  097d 8c0003            	cpd	#3
3919  0980 259d              	blo	L1331
3920  0982                   L7231:
3921                         ; 2362 		return 0;
3923  0982 c7                	clrb	
3925  0983                   L04:
3927  0983 1bf012            	leas	18,s
3928  0986 0a                	rtc	
3929  0987                   L5031:
3930                         ; 2371 		return 1;
3932  0987 c601              	ldab	#1
3934  0989 20f8              	bra	L04
3971                         ; 2391 void EE_MANAGER_CALL EE_SID31_FACallback(void)
3971                         ; 2392 {
3972                         	switch	.ftext
3973  098b                   f_EE_SID31_FACallback:
3975  098b 37                	pshb	
3976       00000001          OFST:	set	1
3979                         ; 2401 	for (byindex=0; byindex < EE_CXSUM_NUM_SECTIONS; byindex++) {
3981  098c 6980              	clr	OFST-1,s
3982  098e                   L1731:
3983                         ; 2404 		EE_SectionCopy(byindex, EE_COPY_PRIMARY2BACKUP);
3985  098e 87                	clra	
3986  098f c7                	clrb	
3987  0990 3b                	pshd	
3988  0991 e682              	ldab	OFST+1,s
3989  0993 4a0afcfc          	call	L14f_EE_SectionCopy
3991  0997 1b82              	leas	2,s
3992                         ; 2401 	for (byindex=0; byindex < EE_CXSUM_NUM_SECTIONS; byindex++) {
3994  0999 6280              	inc	OFST-1,s
3997  099b 27f1              	beq	L1731
3998                         ; 2415 	for (byindex=EE_CXSUM_FIRST_LOCATION; byindex < EE_CXSUM_NUM_BLOCKS; byindex++) {
4000  099d c601              	ldab	#1
4001  099f 6b80              	stab	OFST-1,s
4002  09a1                   L7731:
4003                         ; 2419 		EE_SET_CXSUM_FLAG(abyEECxsumUpdateReq, byindex);
4005  09a1 87                	clra	
4006  09a2 b746              	tfr	d,y
4007  09a4 1857              	asry	
4008  09a6 1857              	asry	
4009  09a8 1857              	asry	
4010  09aa c407              	andb	#7
4011  09ac b745              	tfr	d,x
4012  09ae e6e20000          	ldab	L3_abyEEMaskTable,x
4013  09b2 eaea07dc          	orab	L31_abyEECxsumUpdateReq,y
4014  09b6 6bea07dc          	stab	L31_abyEECxsumUpdateReq,y
4015                         ; 2422 		EE_CLR_CXSUM_FLAG(abyEECxsumFailedFlags, byindex);
4017  09ba e680              	ldab	OFST-1,s
4018  09bc b746              	tfr	d,y
4019  09be 1857              	asry	
4020  09c0 1857              	asry	
4021  09c2 1857              	asry	
4022  09c4 c407              	andb	#7
4023  09c6 b745              	tfr	d,x
4024  09c8 e6e20000          	ldab	L3_abyEEMaskTable,x
4025  09cc 51                	comb	
4026  09cd e4ea07db          	andb	L51_abyEECxsumFailedFlags,y
4027  09d1 6bea07db          	stab	L51_abyEECxsumFailedFlags,y
4028                         ; 2415 	for (byindex=EE_CXSUM_FIRST_LOCATION; byindex < EE_CXSUM_NUM_BLOCKS; byindex++) {
4030  09d5 6280              	inc	OFST-1,s
4033  09d7 e680              	ldab	OFST-1,s
4034  09d9 c103              	cmpb	#3
4035  09db 25c4              	blo	L7731
4036                         ; 2430 	EE_WriteFailedFlags();
4038  09dd 4a0d4646          	call	L35f_EE_WriteFailedFlags
4040                         ; 2434 }
4043  09e1 1b81              	leas	1,s
4044  09e3 0a                	rtc	
4144                         ; 2449 @far u_16Bit /*EE_MANAGER_CALL*/ EE_ValidateCxsum(EEECxsumBlock eCxsumBlock)
4144                         ; 2450 {
4145                         	switch	.ftext
4146  09e4                   f_EE_ValidateCxsum:
4148  09e4 3b                	pshd	
4149  09e5 1b9a              	leas	-6,s
4150       00000006          OFST:	set	6
4153                         ; 2460 	if ( EE_IsBusy() != ITBM_TRUE ) {
4155  09e7 4a000000          	call	f_EE_IsBusy
4157  09eb 040170            	dbeq	b,L5441
4158                         ; 2465 		EPAGE = GetEPage(EE_EEPROM_START_PHY_ADDRESS + aEEBlockTable[aEECxsumTable[eCxsumBlock].eCxsumBlockLoc].wOffset);
4160  09ee e687              	ldab	OFST+1,s
4161  09f0 8606              	ldaa	#6
4162  09f2 12                	mul	
4163  09f3 b746              	tfr	d,y
4164  09f5 e6ea0000          	ldab	_aEECxsumTable,y
4165  09f9 860b              	ldaa	#11
4166  09fb 12                	mul	
4167  09fc b746              	tfr	d,y
4168  09fe ecea0003          	ldd	_aEEBlockTable+3,y
4169  0a02 1887              	clrx	
4170  0a04 188b0010          	addx	#16
4171  0a08 4a000000          	call	f_GetEPage
4173  0a0c 7b0000            	stab	__EPAGE
4174                         ; 2466 		lCxsumval = *((UInt32*)(EEPROM_LOGICAL_START + GetEOffset(EE_EEPROM_START_PHY_ADDRESS + aEEBlockTable[aEECxsumTable[eCxsumBlock].eCxsumBlockLoc].wOffset)));
4176  0a0f e687              	ldab	OFST+1,s
4177  0a11 8606              	ldaa	#6
4178  0a13 12                	mul	
4179  0a14 b746              	tfr	d,y
4180  0a16 e6ea0000          	ldab	_aEECxsumTable,y
4181  0a1a 860b              	ldaa	#11
4182  0a1c 12                	mul	
4183  0a1d b746              	tfr	d,y
4184  0a1f ecea0003          	ldd	_aEEBlockTable+3,y
4185  0a23 1887              	clrx	
4186  0a25 188b0010          	addx	#16
4187  0a29 4a000000          	call	f_GetEOffset
4189  0a2d b746              	tfr	d,y
4190  0a2f 1802ea080284      	movw	2050,y,OFST-2,s
4191  0a35 1802ea080082      	movw	2048,y,OFST-4,s
4192                         ; 2468 		wResult = (EE_BlockCxsum(aEECxsumTable[eCxsumBlock].wStartOffset, aEECxsumTable[eCxsumBlock].wEndOffset));
4194  0a3b e687              	ldab	OFST+1,s
4195  0a3d 8606              	ldaa	#6
4196  0a3f 12                	mul	
4197  0a40 b746              	tfr	d,y
4198  0a42 ecea0003          	ldd	_aEECxsumTable+3,y
4199  0a46 3b                	pshd	
4200  0a47 e689              	ldab	OFST+3,s
4201  0a49 8606              	ldaa	#6
4202  0a4b 12                	mul	
4203  0a4c b746              	tfr	d,y
4204  0a4e ecea0001          	ldd	_aEECxsumTable+1,y
4205  0a52 4a0cc6c6          	call	L74f_EE_BlockCxsum
4207  0a56 1b82              	leas	2,s
4208                         ; 2470 		wResult += (UInt16)(lCxsumval);
4210  0a58 e384              	addd	OFST-2,s
4211  0a5a 6c80              	std	OFST-6,s
4213  0a5c 2001              	bra	L7441
4214  0a5e                   L5441:
4215                         ; 2479 		wResult = 0;
4217  0a5e 87                	clra	
4218  0a5f                   L7441:
4219                         ; 2486 	return wResult;
4223  0a5f 1b88              	leas	8,s
4224  0a61 0a                	rtc	
4280                         	switch	.const
4281  031f 00                	even
4282  0320                   L05:
4283  0320 000fc000          	dc.l	1032192
4284                         	even
4285  0324                   L25:
4286  0324 000fc3e8          	dc.l	1033192
4287                         ; 2504 void EE_RefreshEEToRAM(void)
4287                         ; 2505 {
4288                         	switch	.ftext
4289  0a62                   L75f_EE_RefreshEEToRAM:
4291  0a62 1b95              	leas	-11,s
4292       0000000b          OFST:	set	11
4295                         ; 2517 	if ( EE_IsBusy() != ITBM_TRUE ) {
4297  0a64 4a000000          	call	f_EE_IsBusy
4299  0a68 53                	decb	
4300  0a69 1827008c          	beq	L3741
4301                         ; 2524 		for (byindex=0; byindex < EE_NUM_BLOCKS; byindex++) {
4303  0a6d c7                	clrb	
4304  0a6e 6b80              	stab	OFST-11,s
4305  0a70                   L5741:
4306                         ; 2539 				wBlockSize = aEEBlockTable[byindex].wSize;
4308  0a70 860b              	ldaa	#11
4309  0a72 12                	mul	
4310  0a73 b746              	tfr	d,y
4311  0a75 1802ea000789      	movw	_aEEBlockTable+7,y,OFST-2,s
4312                         ; 2541 				lDestAddr = (UInt32)(aEEBlockTable[byindex].wRAMOffset + (UInt16)(&byShadowRam)) + 0xFC000L;
4314  0a7b ecea0009          	ldd	_aEEBlockTable+9,y
4315  0a7f c30008            	addd	#L52_byShadowRam
4316  0a82 1887              	clrx	
4317  0a84 f30322            	addd	L05+2
4318  0a87 18b90320          	adex	L05
4319  0a8b 6c83              	std	OFST-8,s
4320  0a8d 6e81              	stx	OFST-10,s
4321                         ; 2542 				lSrcAddr = (aEEBlockTable[byindex].wOffset + EE_EEPROM_START_PHY_ADDRESS);
4323  0a8f 1887              	clrx	
4324  0a91 188b0010          	addx	#16
4325  0a95 1802ea000387      	movw	_aEEBlockTable+3,y,OFST-4,s
4326  0a9b 6e85              	stx	OFST-6,s
4327                         ; 2543 				UniversalMemCopy(lDestAddr, lSrcAddr, wBlockSize);
4329  0a9d ec89              	ldd	OFST-2,s
4330  0a9f 3b                	pshd	
4331  0aa0 ec89              	ldd	OFST-2,s
4332  0aa2 3b                	pshd	
4333  0aa3 34                	pshx	
4334  0aa4 ec89              	ldd	OFST-2,s
4335  0aa6 ee87              	ldx	OFST-4,s
4336  0aa8 4a000000          	call	f_UniversalMemCopy
4338  0aac 1b86              	leas	6,s
4339                         ; 2549 				if ( aEEBlockTable[byindex].wBackupOffset != EE_NO_BKUP_OFST ) {
4341  0aae e680              	ldab	OFST-11,s
4342  0ab0 860b              	ldaa	#11
4343  0ab2 12                	mul	
4344  0ab3 b746              	tfr	d,y
4345  0ab5 ecea0005          	ldd	_aEEBlockTable+5,y
4346  0ab9 048433            	ibeq	d,L3051
4347                         ; 2553 					                    lDestAddr = (UInt32)(aEEBlockTable[byindex].wRAMOffset + (UInt16)(&byShadowRam)) + 0xFC000L + SHADOW_BACKUP_START_OFFSET;
4349  0abc ecea0009          	ldd	_aEEBlockTable+9,y
4350  0ac0 c30008            	addd	#L52_byShadowRam
4351  0ac3 1887              	clrx	
4352  0ac5 f30326            	addd	L25+2
4353  0ac8 18b90324          	adex	L25
4354  0acc 6c83              	std	OFST-8,s
4355  0ace 6e81              	stx	OFST-10,s
4356                         ; 2554 					                    lSrcAddr = (aEEBlockTable[byindex].wBackupOffset + EE_EEPROM_START_PHY_ADDRESS);
4358  0ad0 1887              	clrx	
4359  0ad2 188b0010          	addx	#16
4360  0ad6 1802ea000587      	movw	_aEEBlockTable+5,y,OFST-4,s
4361  0adc 6e85              	stx	OFST-6,s
4362                         ; 2555 					                    UniversalMemCopy(lDestAddr, lSrcAddr, wBlockSize);
4364  0ade ec89              	ldd	OFST-2,s
4365  0ae0 3b                	pshd	
4366  0ae1 ec89              	ldd	OFST-2,s
4367  0ae3 3b                	pshd	
4368  0ae4 34                	pshx	
4369  0ae5 ec89              	ldd	OFST-2,s
4370  0ae7 ee87              	ldx	OFST-4,s
4371  0ae9 4a000000          	call	f_UniversalMemCopy
4373  0aed 1b86              	leas	6,s
4374  0aef                   L3051:
4375                         ; 2524 		for (byindex=0; byindex < EE_NUM_BLOCKS; byindex++) {
4377  0aef 6280              	inc	OFST-11,s
4380  0af1 e680              	ldab	OFST-11,s
4381  0af3 c14e              	cmpb	#78
4382  0af5 1825ff77          	blo	L5741
4383  0af9                   L3741:
4384                         ; 2576 }
4387  0af9 1b8b              	leas	11,s
4388  0afb 0a                	rtc	
4536                         ; 2588 void EE_SectionCopy(EECxsumSection_t eSection, EESectionCopyType_t eCopyType)
4536                         ; 2589 {
4537                         	switch	.ftext
4538  0afc                   L14f_EE_SectionCopy:
4540  0afc 3b                	pshd	
4541  0afd 1b96              	leas	-10,s
4542       0000000a          OFST:	set	10
4545                         ; 2601 	EEBlock_StartID = aEECxsumTable[aEECxsumSectionTable[eSection].eCxsumPrimaryBlock].EEBlockStart;
4547  0aff 87                	clra	
4548  0b00 59                	lsld	
4549  0b01 b746              	tfr	d,y
4550  0b03 e6ea0000          	ldab	_aEECxsumSectionTable,y
4551  0b07 8606              	ldaa	#6
4552  0b09 12                	mul	
4553  0b0a b746              	tfr	d,y
4554  0b0c 180aea000584      	movb	_aEECxsumTable+5,y,OFST-6,s
4555                         ; 2602 	EEBlock_EndID = aEECxsumTable[aEECxsumSectionTable[eSection].eCxsumPrimaryBlock].eCxsumBlockLoc;
4557  0b12 e68b              	ldab	OFST+1,s
4558  0b14 87                	clra	
4559  0b15 59                	lsld	
4560  0b16 b746              	tfr	d,y
4561  0b18 e6ea0000          	ldab	_aEECxsumSectionTable,y
4562  0b1c 8606              	ldaa	#6
4563  0b1e 12                	mul	
4564  0b1f b746              	tfr	d,y
4565  0b21 180aea000089      	movb	_aEECxsumTable,y,OFST-1,s
4566                         ; 2608 	for (EEID_index = EEBlock_StartID; EEID_index < EEBlock_EndID; EEID_index++) {
4568  0b27 e684              	ldab	OFST-6,s
4569  0b29 6b83              	stab	OFST-7,s
4571  0b2b 1820009b          	bra	L3751
4572  0b2f                   L7651:
4573                         ; 2615 		if ( eCopyType == EE_COPY_BACKUP2PRIMARY ) {
4575  0b2f e6f010            	ldab	OFST+6,s
4576  0b32 04211a            	dbne	b,L7751
4577                         ; 2619 			wSrcOffst = aEEBlockTable[EEID_index].wRAMOffset + SHADOW_BACKUP_START_OFFSET;
4579  0b35 e683              	ldab	OFST-7,s
4580  0b37 860b              	ldaa	#11
4581  0b39 12                	mul	
4582  0b3a b746              	tfr	d,y
4583  0b3c ecea0009          	ldd	_aEEBlockTable+9,y
4584  0b40 c303e8            	addd	#1000
4585  0b43 6c85              	std	OFST-5,s
4586                         ; 2622 			wDestOffst = aEEBlockTable[EEID_index].wRAMOffset;
4588  0b45 1802ea000987      	movw	_aEEBlockTable+9,y,OFST-3,s
4589                         ; 2625 			EELocation_temp = EEID_index;
4591  0b4b e683              	ldab	OFST-7,s
4593  0b4d 2016              	bra	L1061
4594  0b4f                   L7751:
4595                         ; 2638 			wSrcOffst = aEEBlockTable[EEID_index].wRAMOffset;
4597  0b4f e683              	ldab	OFST-7,s
4598  0b51 860b              	ldaa	#11
4599  0b53 12                	mul	
4600  0b54 b746              	tfr	d,y
4601  0b56 ecea0009          	ldd	_aEEBlockTable+9,y
4602  0b5a 6c85              	std	OFST-5,s
4603                         ; 2641 			wDestOffst = aEEBlockTable[EEID_index].wRAMOffset + SHADOW_BACKUP_START_OFFSET;
4605  0b5c c303e8            	addd	#1000
4606  0b5f 6c87              	std	OFST-3,s
4607                         ; 2644 			EELocation_temp = EEID_index + EE_NUM_PRIMARY_IDS ;
4609  0b61 e683              	ldab	OFST-7,s
4610  0b63 cb50              	addb	#80
4611  0b65                   L1061:
4612  0b65 6b84              	stab	OFST-6,s
4613                         ; 2658 		for (windex = 0; windex < aEEBlockTable[EEID_index].wSize; windex++) {
4615  0b67 6982              	clr	OFST-8,s
4617  0b69 2047              	bra	L7061
4618  0b6b                   L3061:
4619                         ; 2665 			if ( byShadowRam[wDestOffst + windex] != byShadowRam[wSrcOffst + windex] ) {
4621  0b6b ed85              	ldy	OFST-5,s
4622  0b6d e682              	ldab	OFST-8,s
4623  0b6f 87                	clra	
4624  0b70 19ed              	leay	b,y
4625  0b72 ee87              	ldx	OFST-3,s
4626  0b74 1ae5              	leax	b,x
4627  0b76 e6e20008          	ldab	L52_byShadowRam,x
4628  0b7a e1ea0008          	cmpb	L52_byShadowRam,y
4629  0b7e 2730              	beq	L3161
4630                         ; 2669 				EE_MAKE_SECTOR_DIRTY(EELocation_temp);
4632  0b80 e684              	ldab	OFST-6,s
4633  0b82 b746              	tfr	d,y
4634  0b84 1857              	asry	
4635  0b86 1857              	asry	
4636  0b88 1857              	asry	
4637  0b8a c407              	andb	#7
4638  0b8c b745              	tfr	d,x
4639  0b8e e6e20000          	ldab	L3_abyEEMaskTable,x
4640  0b92 eaea08a6          	orab	L5_abyEEDirtyBits,y
4641  0b96 6bea08a6          	stab	L5_abyEEDirtyBits,y
4642                         ; 2672 				byShadowRam[wDestOffst + windex] = byShadowRam[wSrcOffst + windex];
4644  0b9a cc0008            	ldd	#L52_byShadowRam
4645  0b9d 3b                	pshd	
4646  0b9e e684              	ldab	OFST-6,s
4647  0ba0 87                	clra	
4648  0ba1 b746              	tfr	d,y
4649  0ba3 18eb89            	addy	OFST-1,s
4650  0ba6 b745              	tfr	d,x
4651  0ba8 18ab87            	addx	OFST-3,s
4652  0bab 3a                	puld	
4653  0bac 180ae6ee          	movb	d,x,d,y
4654  0bb0                   L3161:
4655                         ; 2658 		for (windex = 0; windex < aEEBlockTable[EEID_index].wSize; windex++) {
4657  0bb0 6282              	inc	OFST-8,s
4658  0bb2                   L7061:
4661  0bb2 e682              	ldab	OFST-8,s
4662  0bb4 87                	clra	
4663  0bb5 6c80              	std	OFST-10,s
4664  0bb7 e683              	ldab	OFST-7,s
4665  0bb9 860b              	ldaa	#11
4666  0bbb 12                	mul	
4667  0bbc b746              	tfr	d,y
4668  0bbe ecea0007          	ldd	_aEEBlockTable+7,y
4669  0bc2 ac80              	cpd	OFST-10,s
4670  0bc4 22a5              	bhi	L3061
4671                         ; 2608 	for (EEID_index = EEBlock_StartID; EEID_index < EEBlock_EndID; EEID_index++) {
4673  0bc6 6283              	inc	OFST-7,s
4674  0bc8 e683              	ldab	OFST-7,s
4675  0bca                   L3751:
4678  0bca e189              	cmpb	OFST-1,s
4679  0bcc 1825ff5f          	blo	L7651
4680                         ; 2695 	if ( sEEStatusFlags.eeCxsumIsValid == ITBM_TRUE ) {
4682  0bd0 1f08a50104        	brclr	L7_sEEStatusFlags,1,L5161
4683                         ; 2698 		sEEStatusFlags.eeWriteChecksumFlag = ITBM_TRUE;
4685  0bd5 1c08a510          	bset	L7_sEEStatusFlags,16
4686  0bd9                   L5161:
4687                         ; 2705 	if ( eCopyType == EE_COPY_BACKUP2PRIMARY ) {
4689  0bd9 e6f010            	ldab	OFST+6,s
4690  0bdc 04210d            	dbne	b,L7161
4691                         ; 2709 		EE_SET_CXSUM_FLAG(abyEECxsumUpdateReq, aEECxsumSectionTable[eSection].eCxsumPrimaryBlock);
4693  0bdf e68b              	ldab	OFST+1,s
4694  0be1 87                	clra	
4695  0be2 b746              	tfr	d,y
4696  0be4 1858              	lsly	
4697  0be6 e6ea0000          	ldab	_aEECxsumSectionTable,y
4699  0bea 200b              	bra	L1261
4700  0bec                   L7161:
4701                         ; 2718 		EE_SET_CXSUM_FLAG(abyEECxsumUpdateReq, aEECxsumSectionTable[eSection].eCxsumBackupBlock);
4703  0bec e68b              	ldab	OFST+1,s
4704  0bee 87                	clra	
4705  0bef b746              	tfr	d,y
4706  0bf1 1858              	lsly	
4707  0bf3 e6ea0001          	ldab	_aEECxsumSectionTable+1,y
4708  0bf7                   L1261:
4709  0bf7 b745              	tfr	d,x
4710  0bf9 1847              	asrx	
4711  0bfb 1847              	asrx	
4712  0bfd 1847              	asrx	
4713  0bff c407              	andb	#7
4714  0c01 b746              	tfr	d,y
4715  0c03 e6ea0000          	ldab	L3_abyEEMaskTable,y
4716  0c07 eae207dc          	orab	L31_abyEECxsumUpdateReq,x
4717  0c0b 6be207dc          	stab	L31_abyEECxsumUpdateReq,x
4718                         ; 2725 }
4721  0c0f 1b8c              	leas	12,s
4722  0c11 0a                	rtc	
4790                         ; 2739 void EE_PowerFailureRecovery(void)
4790                         ; 2740 {
4791                         	switch	.ftext
4792  0c12                   L34f_EE_PowerFailureRecovery:
4794  0c12 1b9c              	leas	-4,s
4795       00000004          OFST:	set	4
4798                         ; 2752 	for (byindex=0; byindex < EE_CXSUM_NUM_SECTIONS; byindex++) {
4800  0c14 6980              	clr	OFST-4,s
4801  0c16                   L3561:
4802                         ; 2755 		bCanRecover = ITBM_FALSE;
4804  0c16 87                	clra	
4805  0c17 6a83              	staa	OFST-1,s
4806                         ; 2758 		eCxsumPrimaryBlock = aEECxsumSectionTable[byindex].eCxsumPrimaryBlock;
4808  0c19 e680              	ldab	OFST-4,s
4809  0c1b b746              	tfr	d,y
4810  0c1d 1858              	lsly	
4811  0c1f 180aea000081      	movb	_aEECxsumSectionTable,y,OFST-3,s
4812                         ; 2759 		eCxsumBackupBlock = aEECxsumSectionTable[byindex].eCxsumBackupBlock;
4814  0c25 180aea000182      	movb	_aEECxsumSectionTable+1,y,OFST-2,s
4815                         ; 2764 		if ( EE_ValidateCxsum(eCxsumPrimaryBlock) ) {
4817  0c2b e681              	ldab	OFST-3,s
4818  0c2d 4a09e4e4          	call	f_EE_ValidateCxsum
4820  0c31 04442f            	tbeq	d,L1661
4821                         ; 2771 			if ( !(EE_ValidateCxsum(eCxsumBackupBlock)) ) {
4823  0c34 e682              	ldab	OFST-2,s
4824  0c36 87                	clra	
4825  0c37 4a09e4e4          	call	f_EE_ValidateCxsum
4827  0c3b 046425            	tbne	d,L1661
4828                         ; 2780 				if ( !(EE_CXSUM_FLAG_IS_SET(abyEECxsumFailedFlags, eCxsumBackupBlock)) ) {
4830  0c3e e682              	ldab	OFST-2,s
4831  0c40 b746              	tfr	d,y
4832  0c42 1857              	asry	
4833  0c44 1857              	asry	
4834  0c46 1857              	asry	
4835  0c48 c407              	andb	#7
4836  0c4a b745              	tfr	d,x
4837  0c4c e6e20000          	ldab	L3_abyEEMaskTable,x
4838  0c50 e4ea07db          	andb	L51_abyEECxsumFailedFlags,y
4839  0c54 260d              	bne	L1661
4840                         ; 2784 					bCanRecover = ITBM_TRUE;
4842  0c56 c601              	ldab	#1
4843  0c58 6b83              	stab	OFST-1,s
4844                         ; 2786 					EE_SectionCopy(byindex, EE_COPY_BACKUP2PRIMARY);
4846  0c5a 3b                	pshd	
4847  0c5b e682              	ldab	OFST-2,s
4848  0c5d 4a0afcfc          	call	L14f_EE_SectionCopy
4850  0c61 1b82              	leas	2,s
4851  0c63                   L1661:
4852                         ; 2799 		if ( bCanRecover == ITBM_FALSE ) {
4854  0c63 e683              	ldab	OFST-1,s
4855  0c65 2622              	bne	L7661
4856                         ; 2807 			if ( !(EE_CXSUM_FLAG_IS_SET(abyEECxsumFailedFlags, eCxsumPrimaryBlock)) ) {
4858  0c67 e681              	ldab	OFST-3,s
4859  0c69 87                	clra	
4860  0c6a b746              	tfr	d,y
4861  0c6c 1857              	asry	
4862  0c6e 1857              	asry	
4863  0c70 1857              	asry	
4864  0c72 c407              	andb	#7
4865  0c74 b745              	tfr	d,x
4866  0c76 e6e20000          	ldab	L3_abyEEMaskTable,x
4867  0c7a e4ea07db          	andb	L51_abyEECxsumFailedFlags,y
4868  0c7e 2609              	bne	L7661
4869                         ; 2811 				EE_SectionCopy(byindex, EE_COPY_PRIMARY2BACKUP);
4871  0c80 3b                	pshd	
4872  0c81 e682              	ldab	OFST-2,s
4873  0c83 4a0afcfc          	call	L14f_EE_SectionCopy
4875  0c87 1b82              	leas	2,s
4877  0c89                   L7661:
4878                         ; 2752 	for (byindex=0; byindex < EE_CXSUM_NUM_SECTIONS; byindex++) {
4880  0c89 6280              	inc	OFST-4,s
4883  0c8b 2789              	beq	L3561
4884                         ; 2834 	EE_UpdateAllCxsum();
4886  0c8d 4a0c9494          	call	L54f_EE_UpdateAllCxsum
4888                         ; 2838 }
4891  0c91 1b84              	leas	4,s
4892  0c93 0a                	rtc	
4926                         ; 2852 void EE_UpdateAllCxsum(void)
4926                         ; 2853 {
4927                         	switch	.ftext
4928  0c94                   L54f_EE_UpdateAllCxsum:
4930  0c94 37                	pshb	
4931       00000001          OFST:	set	1
4934                         ; 2862 	if ( sEEStatusFlags.eeCxsumIsValid == ITBM_TRUE ) {
4936  0c95 1f08a50104        	brclr	L7_sEEStatusFlags,1,L1171
4937                         ; 2865 		sEEStatusFlags.eeWriteChecksumFlag = ITBM_TRUE;
4939  0c9a 1c08a510          	bset	L7_sEEStatusFlags,16
4940  0c9e                   L1171:
4941                         ; 2876 	for (byindex=EE_CXSUM_FIRST_LOCATION; byindex<EE_CXSUM_NUM_BLOCKS; byindex++) {
4943  0c9e c601              	ldab	#1
4944  0ca0 6b80              	stab	OFST-1,s
4945  0ca2                   L3171:
4946                         ; 2880 		EE_SET_CXSUM_FLAG(abyEECxsumUpdateReq, byindex);
4948  0ca2 87                	clra	
4949  0ca3 b746              	tfr	d,y
4950  0ca5 1857              	asry	
4951  0ca7 1857              	asry	
4952  0ca9 1857              	asry	
4953  0cab c407              	andb	#7
4954  0cad b745              	tfr	d,x
4955  0caf e6e20000          	ldab	L3_abyEEMaskTable,x
4956  0cb3 eaea07dc          	orab	L31_abyEECxsumUpdateReq,y
4957  0cb7 6bea07dc          	stab	L31_abyEECxsumUpdateReq,y
4958                         ; 2876 	for (byindex=EE_CXSUM_FIRST_LOCATION; byindex<EE_CXSUM_NUM_BLOCKS; byindex++) {
4960  0cbb 6280              	inc	OFST-1,s
4963  0cbd e680              	ldab	OFST-1,s
4964  0cbf c103              	cmpb	#3
4965  0cc1 25df              	blo	L3171
4966                         ; 2887 }
4969  0cc3 1b81              	leas	1,s
4970  0cc5 0a                	rtc	
5035                         ; 2901 UInt16 EE_BlockCxsum(UInt16 wStartOffset, UInt16 wEndOffset)
5035                         ; 2902 {
5036                         	switch	.ftext
5037  0cc6                   L74f_EE_BlockCxsum:
5039  0cc6 3b                	pshd	
5040  0cc7 1b9a              	leas	-6,s
5041       00000006          OFST:	set	6
5044                         ; 2910 	EPAGE = GetEPage(EE_EEPROM_START_PHY_ADDRESS + wStartOffset);
5046  0cc9 1887              	clrx	
5047  0ccb 188b0010          	addx	#16
5048  0ccf 4a000000          	call	f_GetEPage
5050  0cd3 7b0000            	stab	__EPAGE
5051                         ; 2911 	pbysrc = (unsigned char *)(EEPROM_LOGICAL_START + GetEOffset(EE_EEPROM_START_PHY_ADDRESS + wStartOffset)); 
5053  0cd6 ec86              	ldd	OFST+0,s
5054  0cd8 1887              	clrx	
5055  0cda 188b0010          	addx	#16
5056  0cde 4a000000          	call	f_GetEOffset
5058  0ce2 c30800            	addd	#2048
5059  0ce5 6c80              	std	OFST-6,s
5060                         ; 2914 	wResult = 0;
5062  0ce7 87                	clra	
5063  0ce8 c7                	clrb	
5064  0ce9 6c84              	std	OFST-2,s
5065  0ceb 6c82              	std	OFST-4,s
5066                         ; 2920 	for (windex=0; windex<=(wEndOffset-wStartOffset); windex++) {
5069  0ced ed80              	ldy	OFST-6,s
5070  0cef 200a              	bra	L3571
5071  0cf1                   L7471:
5072                         ; 2924 		wResult += *(pbysrc);
5074  0cf1 e670              	ldab	1,y+
5075  0cf3 87                	clra	
5076  0cf4 e384              	addd	OFST-2,s
5077  0cf6 6c84              	std	OFST-2,s
5078                         ; 2926 		++pbysrc;
5080                         ; 2920 	for (windex=0; windex<=(wEndOffset-wStartOffset); windex++) {
5082  0cf8 186282            	incw	OFST-4,s
5083  0cfb                   L3571:
5086  0cfb ec8b              	ldd	OFST+5,s
5087  0cfd a386              	subd	OFST+0,s
5088  0cff ac82              	cpd	OFST-4,s
5089  0d01 24ee              	bhs	L7471
5090  0d03 6d80              	sty	OFST-6,s
5091                         ; 2933 	return wResult;
5093  0d05 ec84              	ldd	OFST-2,s
5096  0d07 1b88              	leas	8,s
5097  0d09 0a                	rtc	
5143                         ; 2951 void EE_VerifyAllCxsum(void)
5143                         ; 2952 {
5144                         	switch	.ftext
5145  0d0a                   L15f_EE_VerifyAllCxsum:
5147  0d0a 3b                	pshd	
5148       00000002          OFST:	set	2
5151                         ; 2958 	checksums_valid = ITBM_TRUE;
5153  0d0b c601              	ldab	#1
5154  0d0d 7b07da            	stab	L71_checksums_valid
5155                         ; 2962 	for (byindex=EE_CXSUM_FIRST_LOCATION; byindex < EE_CXSUM_NUM_BLOCKS; byindex++) {
5157  0d10 6b80              	stab	OFST-2,s
5158  0d12                   L7771:
5159                         ; 2965 		if ( EE_ValidateCxsum(byindex) ) {
5161  0d12 87                	clra	
5162  0d13 4a09e4e4          	call	f_EE_ValidateCxsum
5164  0d17 044422            	tbeq	d,L5002
5165                         ; 2969 			EE_SET_CXSUM_FLAG(abyEECxsumFailedFlags, byindex);
5167  0d1a e680              	ldab	OFST-2,s
5168  0d1c 87                	clra	
5169  0d1d b746              	tfr	d,y
5170  0d1f 1857              	asry	
5171  0d21 1857              	asry	
5172  0d23 1857              	asry	
5173  0d25 c407              	andb	#7
5174  0d27 b745              	tfr	d,x
5175  0d29 e6e20000          	ldab	L3_abyEEMaskTable,x
5176  0d2d eaea07db          	orab	L51_abyEECxsumFailedFlags,y
5177  0d31 6bea07db          	stab	L51_abyEECxsumFailedFlags,y
5178                         ; 2971 			checksums_valid = ITBM_FALSE;
5180  0d35 7907da            	clr	L71_checksums_valid
5181                         ; 2974 			EE_WriteFailedFlags();
5183  0d38 4a0d4646          	call	L35f_EE_WriteFailedFlags
5185  0d3c                   L5002:
5186                         ; 2962 	for (byindex=EE_CXSUM_FIRST_LOCATION; byindex < EE_CXSUM_NUM_BLOCKS; byindex++) {
5188  0d3c 6280              	inc	OFST-2,s
5191  0d3e e680              	ldab	OFST-2,s
5192  0d40 c103              	cmpb	#3
5193  0d42 25ce              	blo	L7771
5194                         ; 2984 	temp = checksums_valid ? ITBM_FALSE : ITBM_TRUE;
5196                         ; 2991 }
5199  0d44 31                	puly	
5200  0d45 0a                	rtc	
5245                         ; 3005 void EE_WriteFailedFlags(void)
5245                         ; 3006 {
5246                         	switch	.ftext
5247  0d46                   L35f_EE_WriteFailedFlags:
5249  0d46 1b9b              	leas	-5,s
5250       00000005          OFST:	set	5
5253                         ; 3012 	*(UInt32 *)&aFailedFlagsSectorCopy = 0;
5255  0d48 87                	clra	
5256  0d49 c7                	clrb	
5257  0d4a 6c81              	std	OFST-4,s
5258  0d4c 6c83              	std	OFST-2,s
5259                         ; 3016 	for (byindex=0; byindex < EE_NUM_BYTES_IN_CXSUM_FLAGS_ARRAY; byindex++) {
5261  0d4e 6980              	clr	OFST-5,s
5262  0d50 ce07db            	ldx	#L51_abyEECxsumFailedFlags
5263  0d53                   L7202:
5264                         ; 3019 		aFailedFlagsSectorCopy[byindex] = abyEECxsumFailedFlags[byindex];
5266  0d53 e680              	ldab	OFST-5,s
5267  0d55 87                	clra	
5268  0d56 1981              	leay	OFST-4,s
5269  0d58 180ae6ee          	movb	d,x,d,y
5270                         ; 3016 	for (byindex=0; byindex < EE_NUM_BYTES_IN_CXSUM_FLAGS_ARRAY; byindex++) {
5272  0d5c 6280              	inc	OFST-5,s
5275  0d5e 27f3              	beq	L7202
5276                         ; 3026 	EE_BlockWrite(EE_CXSUM_FAILED_FLAGS_BLOCK,  (UInt8*)&aFailedFlagsSectorCopy);
5278  0d60 1a81              	leax	OFST-4,s
5279  0d62 34                	pshx	
5280  0d63 c649              	ldab	#73
5281  0d65 4a056b6b          	call	f_EE_BlockWrite
5283  0d69 1b82              	leas	2,s
5284                         ; 3027 	EE_BlockWrite(EE_CXSUM_FAILED_FLAGS2_BLOCK, (UInt8*)&aFailedFlagsSectorCopy);
5286  0d6b 1a81              	leax	OFST-4,s
5287  0d6d 34                	pshx	
5288  0d6e cc004a            	ldd	#74
5289  0d71 4a056b6b          	call	f_EE_BlockWrite
5291  0d75 1b82              	leas	2,s
5292                         ; 3028 	EE_BlockWrite(EE_CXSUM_FAILED_FLAGS3_BLOCK, (UInt8*)&aFailedFlagsSectorCopy);
5294  0d77 1a81              	leax	OFST-4,s
5295  0d79 34                	pshx	
5296  0d7a cc004b            	ldd	#75
5297  0d7d 4a056b6b          	call	f_EE_BlockWrite
5299                         ; 3032 }
5302  0d81 1b87              	leas	7,s
5303  0d83 0a                	rtc	
5359                         ; 3046 void EE_ReadFailedFlags(void)
5359                         ; 3047 {
5360                         	switch	.ftext
5361  0d84                   L55f_EE_ReadFailedFlags:
5363  0d84 1b92              	leas	-14,s
5364       0000000e          OFST:	set	14
5367                         ; 3055 	bFailureDetected = ITBM_FALSE;
5369  0d86 698d              	clr	OFST-1,s
5370                         ; 3057 	EE_BlockRead(EE_CXSUM_FAILED_FLAGS_BLOCK,  (UInt8*)&abyEECXsumFailedFlagsBuffer[0]);
5372  0d88 1a80              	leax	OFST-14,s
5373  0d8a 34                	pshx	
5374  0d8b cc0049            	ldd	#73
5375  0d8e 4a06bfbf          	call	f_EE_BlockRead
5377  0d92 1b82              	leas	2,s
5378                         ; 3058 	EE_BlockRead(EE_CXSUM_FAILED_FLAGS2_BLOCK, (UInt8*)&abyEECXsumFailedFlagsBuffer[1]);
5380  0d94 1a84              	leax	OFST-10,s
5381  0d96 34                	pshx	
5382  0d97 cc004a            	ldd	#74
5383  0d9a 4a06bfbf          	call	f_EE_BlockRead
5385  0d9e 1b82              	leas	2,s
5386                         ; 3059 	EE_BlockRead(EE_CXSUM_FAILED_FLAGS3_BLOCK, (UInt8*)&abyEECXsumFailedFlagsBuffer[2]);
5388  0da0 1a88              	leax	OFST-6,s
5389  0da2 34                	pshx	
5390  0da3 cc004b            	ldd	#75
5391  0da6 4a06bfbf          	call	f_EE_BlockRead
5393  0daa 1b82              	leas	2,s
5394                         ; 3063 	if ( (*(UInt32*)&abyEECXsumFailedFlagsBuffer[0] == *(UInt32*)&abyEECXsumFailedFlagsBuffer[1])
5394                         ; 3064 	   && (*(UInt32*)&abyEECXsumFailedFlagsBuffer[1] == *(UInt32*)&abyEECXsumFailedFlagsBuffer[2]) ) {
5396  0dac ec80              	ldd	OFST-14,s
5397  0dae ac84              	cpd	OFST-10,s
5398  0db0 2612              	bne	LC005
5399  0db2 ec82              	ldd	OFST-12,s
5400  0db4 ac86              	cpd	OFST-8,s
5401  0db6 260c              	bne	LC005
5403  0db8 ec84              	ldd	OFST-10,s
5404  0dba ac88              	cpd	OFST-6,s
5405  0dbc 2606              	bne	LC005
5406  0dbe ec86              	ldd	OFST-8,s
5407  0dc0 ac8a              	cpd	OFST-4,s
5408  0dc2 272a              	beq	L3602
5409  0dc4                   LC005:
5411                         ; 3075 		if ( *(UInt32*)&abyEECXsumFailedFlagsBuffer[1] == *(UInt32*)&abyEECXsumFailedFlagsBuffer[2] ) {
5413  0dc4 ec84              	ldd	OFST-10,s
5414  0dc6 ac88              	cpd	OFST-6,s
5415  0dc8 2612              	bne	L5602
5416  0dca ec86              	ldd	OFST-8,s
5417  0dcc ac8a              	cpd	OFST-4,s
5418  0dce 260c              	bne	L5602
5419                         ; 3079 			*(UInt32 *)&abyEECXsumFailedFlagsBuffer[0] = *(UInt32 *)&abyEECXsumFailedFlagsBuffer[1];
5421  0dd0 6c82              	std	OFST-12,s
5422  0dd2 18028480          	movw	OFST-10,s,OFST-14,s
5423                         ; 3080 			bFailureDetected = ITBM_TRUE;
5425  0dd6 c601              	ldab	#1
5426  0dd8 6b8d              	stab	OFST-1,s
5428  0dda 2012              	bra	L3602
5429  0ddc                   L5602:
5430                         ; 3089 			bFailureDetected = ITBM_TRUE;
5432  0ddc c601              	ldab	#1
5433  0dde 6b8d              	stab	OFST-1,s
5434                         ; 3095 			if ( *(UInt32*)(&abyEECXsumFailedFlagsBuffer[0]) == 0xFFFFFFFF ) {
5436  0de0 ec80              	ldd	OFST-14,s
5437  0de2 04a409            	ibne	d,L3602
5438  0de5 ec82              	ldd	OFST-12,s
5439  0de7 04a404            	ibne	d,L3602
5440                         ; 3098 				*(UInt32 *)&abyEECXsumFailedFlagsBuffer[0] = 0;
5442  0dea 6c80              	std	OFST-14,s
5443  0dec 6c82              	std	OFST-12,s
5444  0dee                   L3602:
5445                         ; 3111 	for (byindex=0; byindex < EE_NUM_BYTES_IN_CXSUM_FLAGS_ARRAY; byindex++) {
5447  0dee 698c              	clr	OFST-2,s
5448  0df0 cd07db            	ldy	#L51_abyEECxsumFailedFlags
5449  0df3                   L3702:
5450                         ; 3114 		abyEECxsumFailedFlags[byindex] = *(UInt8 *)&abyEECXsumFailedFlagsBuffer[0][byindex];
5452  0df3 e68c              	ldab	OFST-2,s
5453  0df5 87                	clra	
5454  0df6 1a80              	leax	OFST-14,s
5455  0df8 180ae6ee          	movb	d,x,d,y
5456                         ; 3111 	for (byindex=0; byindex < EE_NUM_BYTES_IN_CXSUM_FLAGS_ARRAY; byindex++) {
5458  0dfc 628c              	inc	OFST-2,s
5461  0dfe 27f3              	beq	L3702
5462                         ; 3121 	if ( bFailureDetected == ITBM_TRUE ) {
5464  0e00 e68d              	ldab	OFST-1,s
5465  0e02 042104            	dbne	b,L1012
5466                         ; 3125 		EE_WriteFailedFlags();
5468  0e05 4a0d4646          	call	L35f_EE_WriteFailedFlags
5470  0e09                   L1012:
5471                         ; 3132 }
5474  0e09 1b8e              	leas	14,s
5475  0e0b 0a                	rtc	
5499                         ; 3148 void EE_Manager_Static_Init (void)
5499                         ; 3149 {
5501                         	switch	.ftext
5502  0e0c                   L16f_EE_Manager_Static_Init:
5506                         ; 3152 	*(@far UInt8 *)&sEEStatusFlags = 0;
5508  0e0c ce08a5            	ldx	#L7_sEEStatusFlags
5509  0e0f 86a5              	ldaa	#page(L7_sEEStatusFlags)
5510  0e11 c7                	clrb	
5511  0e12 7a0016            	staa	RPAGE
5512  0e15 6b00              	stab	0,x
5513                         ; 3159 }
5516  0e17 0a                	rtc	
6836                         	xdef	_sEEPConstants
6837                         	switch	.bss
6838  0000                   L53_ee_Vref_STW_w:
6839  0000 0000              	ds.b	2
6840  0002                   L33_ee_Iref_STW_c:
6841  0002 00                	ds.b	1
6842  0003                   L13_ee_Vref_seatH_w:
6843  0003 0000              	ds.b	2
6844  0005                   L72_ee_Iref_seatH_c:
6845  0005 00                	ds.b	1
6846  0006                   _ee_wait_counter_c:
6847  0006 0000              	ds.b	2
6848                         	xdef	_ee_wait_counter_c
6849  0008                   L52_byShadowRam:
6850  0008 0000000000000000  	ds.b	2000
6851  07d8                   L32_byVTABlockOfst:
6852  07d8 00                	ds.b	1
6853  07d9                   L12_version_valid:
6854  07d9 00                	ds.b	1
6855  07da                   L71_checksums_valid:
6856  07da 00                	ds.b	1
6857  07db                   L51_abyEECxsumFailedFlags:
6858  07db 00                	ds.b	1
6859  07dc                   L31_abyEECxsumUpdateReq:
6860  07dc 00                	ds.b	1
6861  07dd                   L11_abyEEWriteBuff:
6862  07dd 0000000000000000  	ds.b	200
6863  08a5                   L7_sEEStatusFlags:
6864  08a5 00                	ds.b	1
6865  08a6                   L5_abyEEDirtyBits:
6866  08a6 0000000000000000  	ds.b	20
6867                         	xref	f_WriteFBLDFlashByte
6868                         	xref	_diag_io_lock3_flags_c
6869                         	xref	_aEEBlockAddrTable
6870                         	xref	_aEECxsumSectionTable
6871                         	xref	_aEEDefaultsTable
6872                         	xref	_aEECxsumTable
6873                         	xref	_aEEBlockTable
6874                         	xdef	f_EE_WaitPendingWrites
6875                         	xdef	f_EE_IsVersionValid
6876                         	xdef	f_EE_IsChecksumsValid
6877                         	xdef	f_EE_ValidateCxsum
6878                         	xdef	f_EE_SID31_FACallback
6879                         	xdef	f_EE_DirectWrite
6880                         	xdef	f_EE_LoadDefaults
6881                         	xdef	f_EE_BlockRead
6882                         	xdef	f_EE_BlockWrite
6883                         	xdef	f_EE_ManagerIsBusy
6884                         	xdef	f_EE_GetStatusFlags
6885                         	xdef	f_EE_Manager
6886                         	xdef	f_EE_PowerupInit
6887                         	xref	f_EE_Driver_Static_Init
6888                         	xref	f_EE_ForcedWrite
6889                         	xref	f_EE_DriverPowerupInit
6890                         	xref	f_EE_IsBusy
6891                         	xref	f_EE_Handler
6892                         	xref	f_UniversalMemCopy
6893                         	xref	f_GetEOffset
6894                         	xref	f_GetEPage
6895                         	xref	f_ITBM_tmrmSetTimer
6896                         	xref	__EPAGE
6917                         	end
