   1                         ; C Compiler for S12XE [COSMIC Software].
   2                         ; Generator V4.7.8 - 27 Jul 2007
   3                         ; Optimizer V4.6.13 - 27 Jul 2007
  38                         ; 92 void near ADF_Init(void)
  38                         ; 93 {	
  39                         	switch	.text
  40  0000                   _ADF_Init:
  44                         ; 106 	AD_Iref_seatH_c = 2;
  46  0000 c602              	ldab	#2
  47  0002 7b00b5            	stab	_AD_Iref_seatH_c
  48                         ; 107 	AD_Vref_seatH_w = 712;
  50  0005 cc02c8            	ldd	#712
  51  0008 7c00b3            	std	_AD_Vref_seatH_w
  52                         ; 108 	AD_Iref_STW_c = 8;
  54  000b c608              	ldab	#8
  55  000d 7b00b2            	stab	_AD_Iref_STW_c
  56                         ; 109 	AD_Vref_STW_w = 825;
  58  0010 cc0339            	ldd	#825
  59  0013 7c00b0            	std	_AD_Vref_STW_w
  60                         ; 110 }
  63  0016 3d                	rts	
 103                         ; 145 void near ADF_Filter(u_8Bit AD_Filter_ch)
 103                         ; 146 {
 104                         	switch	.text
 105  0017                   _ADF_Filter:
 107  0017 3b                	pshd	
 108  0018 37                	pshb	
 109       00000001          OFST:	set	1
 112                         ; 151 	AD_ch = AD_Filter_ch;
 114  0019 6b80              	stab	OFST-1,s
 115                         ; 154 	if (AD_Filter_flg[AD_ch].b.Loaded_b) {
 117  001b 87                	clra	
 118  001c b746              	tfr	d,y
 119  001e 8606              	ldaa	#6
 120  0020 12                	mul	
 121  0021 0fea0018012f      	brclr	_AD_Filter_flg,y,1,L73
 122                         ; 156 		AD_Filter_set[AD_ch].avg_w -= AD_Filter_set[AD_ch].flt_rslt_w;
 124  0027 b746              	tfr	d,y
 125  0029 e680              	ldab	OFST-1,s
 126  002b 8606              	ldaa	#6
 127  002d 12                	mul	
 128  002e b745              	tfr	d,x
 129  0030 ecea0022          	ldd	_AD_Filter_set+2,y
 130  0034 a3e20024          	subd	_AD_Filter_set+4,x
 131                         ; 159 		AD_Filter_set[AD_ch].avg_w +=AD_Filter_set[AD_ch].unflt_rslt_w;
 133  0038 e3e20020          	addd	_AD_Filter_set,x
 134  003c 6cea0022          	std	_AD_Filter_set+2,y
 135                         ; 162 		AD_Filter_set[AD_ch].flt_rslt_w = (AD_Filter_set[AD_ch].avg_w>>3);
 137  0040 ece20022          	ldd	_AD_Filter_set+2,x
 138  0044 49                	lsrd	
 139  0045 49                	lsrd	
 140  0046 49                	lsrd	
 141  0047 6cea0024          	std	_AD_Filter_set+4,y
 142                         ; 165 		AD_Filter_flg[AD_ch].b.Vld_b = TRUE;
 144  004b e680              	ldab	OFST-1,s
 145  004d b796              	exg	b,y
 146  004f 0cea001802        	bset	_AD_Filter_flg,y,2
 148  0054 2025              	bra	L14
 149  0056                   L73:
 150                         ; 169 		AD_Filter_set[AD_ch].avg_w = (AD_Filter_set[AD_ch].unflt_rslt_w<<3);
 152  0056 b746              	tfr	d,y
 153  0058 e680              	ldab	OFST-1,s
 154  005a 8606              	ldaa	#6
 155  005c 12                	mul	
 156  005d b745              	tfr	d,x
 157  005f ece20020          	ldd	_AD_Filter_set,x
 158  0063 59                	lsld	
 159  0064 59                	lsld	
 160  0065 59                	lsld	
 161  0066 6cea0022          	std	_AD_Filter_set+2,y
 162                         ; 172 		AD_Filter_set[AD_ch].flt_rslt_w = AD_Filter_set[AD_ch].unflt_rslt_w;
 164  006a 1802ea0020ea0024  	movw	_AD_Filter_set,y,_AD_Filter_set+4,y
 165                         ; 175 		AD_Filter_flg[AD_ch].b.Loaded_b = TRUE;
 167  0072 e680              	ldab	OFST-1,s
 168  0074 b796              	exg	b,y
 169  0076 0cea001801        	bset	_AD_Filter_flg,y,1
 170  007b                   L14:
 171                         ; 177 }
 174  007b 1b83              	leas	3,s
 175  007d 3d                	rts	
 230                         ; 211 void near ADF_Mux_Filter(u_8Bit AD_Filter_slctd_mux, u_8Bit AD_Filter_mux_ch)
 230                         ; 212 {
 231                         	switch	.text
 232  007e                   _ADF_Mux_Filter:
 234  007e 3b                	pshd	
 235  007f 3b                	pshd	
 236       00000002          OFST:	set	2
 239                         ; 218 	AD_mux = AD_Filter_slctd_mux;
 241  0080 6b80              	stab	OFST-2,s
 242                         ; 219 	AD_mux_ch = AD_Filter_mux_ch;
 244  0082 180a8781          	movb	OFST+5,s,OFST-1,s
 245                         ; 222 	if (AD_Filter_mux_flg[AD_mux][AD_mux_ch].b.mux_Loaded_b) {
 247  0086 8608              	ldaa	#8
 248  0088 12                	mul	
 249  0089 b746              	tfr	d,y
 250  008b e681              	ldab	OFST-1,s
 251  008d 19ed              	leay	b,y
 252  008f e6ea0008          	ldab	_AD_Filter_mux_flg,y
 253  0093 c501              	bitb	#1
 254  0095 274a              	beq	L56
 255                         ; 224 		AD_mux_Filter[AD_mux][AD_mux_ch].avg_w -=AD_mux_Filter[AD_mux][AD_mux_ch].flt_rslt_w;
 257  0097 e681              	ldab	OFST-1,s
 258  0099 8606              	ldaa	#6
 259  009b 12                	mul	
 260  009c b746              	tfr	d,y
 261  009e e680              	ldab	OFST-2,s
 262  00a0 8630              	ldaa	#48
 263  00a2 12                	mul	
 264  00a3 19ee              	leay	d,y
 265  00a5 e681              	ldab	OFST-1,s
 266  00a7 8606              	ldaa	#6
 267  00a9 12                	mul	
 268  00aa b745              	tfr	d,x
 269  00ac e680              	ldab	OFST-2,s
 270  00ae 8630              	ldaa	#48
 271  00b0 12                	mul	
 272  00b1 1ae6              	leax	d,x
 273  00b3 ecea0052          	ldd	_AD_mux_Filter+2,y
 274  00b7 a3e20054          	subd	_AD_mux_Filter+4,x
 275                         ; 227 		AD_mux_Filter[AD_mux][AD_mux_ch].avg_w += AD_mux_Filter[AD_mux][AD_mux_ch].unflt_rslt_w;
 277  00bb e3e20050          	addd	_AD_mux_Filter,x
 278  00bf 6cea0052          	std	_AD_mux_Filter+2,y
 279                         ; 230 		AD_mux_Filter[AD_mux][AD_mux_ch].flt_rslt_w = (AD_mux_Filter[AD_mux][AD_mux_ch].avg_w>>3);
 281  00c3 ece20052          	ldd	_AD_mux_Filter+2,x
 282  00c7 49                	lsrd	
 283  00c8 49                	lsrd	
 284  00c9 49                	lsrd	
 285  00ca 6cea0054          	std	_AD_mux_Filter+4,y
 286                         ; 233 		AD_Filter_mux_flg[AD_mux][AD_mux_ch].b.mux_Vld_b = TRUE;
 288  00ce e680              	ldab	OFST-2,s
 289  00d0 8608              	ldaa	#8
 290  00d2 12                	mul	
 291  00d3 b746              	tfr	d,y
 292  00d5 e681              	ldab	OFST-1,s
 293  00d7 87                	clra	
 294  00d8 19ed              	leay	b,y
 295  00da 0cea000802        	bset	_AD_Filter_mux_flg,y,2
 297  00df 203f              	bra	L76
 298  00e1                   L56:
 299                         ; 238 		AD_mux_Filter[AD_mux][AD_mux_ch].avg_w = (AD_mux_Filter[AD_mux][AD_mux_ch].unflt_rslt_w<<3);
 301  00e1 e681              	ldab	OFST-1,s
 302  00e3 8606              	ldaa	#6
 303  00e5 12                	mul	
 304  00e6 b746              	tfr	d,y
 305  00e8 e680              	ldab	OFST-2,s
 306  00ea 8630              	ldaa	#48
 307  00ec 12                	mul	
 308  00ed 19ee              	leay	d,y
 309  00ef e681              	ldab	OFST-1,s
 310  00f1 8606              	ldaa	#6
 311  00f3 12                	mul	
 312  00f4 b745              	tfr	d,x
 313  00f6 e680              	ldab	OFST-2,s
 314  00f8 8630              	ldaa	#48
 315  00fa 12                	mul	
 316  00fb 1ae6              	leax	d,x
 317  00fd ece20050          	ldd	_AD_mux_Filter,x
 318  0101 59                	lsld	
 319  0102 59                	lsld	
 320  0103 59                	lsld	
 321  0104 6cea0052          	std	_AD_mux_Filter+2,y
 322                         ; 241 		AD_mux_Filter[AD_mux][AD_mux_ch].flt_rslt_w = AD_mux_Filter[AD_mux][AD_mux_ch].unflt_rslt_w;
 324  0108 1802ea0050ea0054  	movw	_AD_mux_Filter,y,_AD_mux_Filter+4,y
 325                         ; 244 		AD_Filter_mux_flg[AD_mux][AD_mux_ch].b.mux_Loaded_b = TRUE;
 327  0110 e680              	ldab	OFST-2,s
 328  0112 8608              	ldaa	#8
 329  0114 12                	mul	
 330  0115 b746              	tfr	d,y
 331  0117 e681              	ldab	OFST-1,s
 332  0119 19ed              	leay	b,y
 333  011b 0cea000801        	bset	_AD_Filter_mux_flg,y,1
 334  0120                   L76:
 335                         ; 247 }
 338  0120 1b84              	leas	4,s
 339  0122 3d                	rts	
 394                         ; 277 void near ADF_Str_UnFlt_Mux_Rslt(u_8Bit AD_Filter_feed, u_8Bit AD_Filter_mux_feed)
 394                         ; 278 {
 395                         	switch	.text
 396  0123                   _ADF_Str_UnFlt_Mux_Rslt:
 398  0123 3b                	pshd	
 399  0124 3b                	pshd	
 400       00000002          OFST:	set	2
 403                         ; 284 	AD_feed = AD_Filter_feed;
 405  0125 6b81              	stab	OFST-1,s
 406                         ; 285 	AD_mux_feed = AD_Filter_mux_feed;
 408  0127 180a8780          	movb	OFST+5,s,OFST-2,s
 409                         ; 287 	switch (AD_feed) {
 412  012b c005              	subb	#5
 413  012d 2705              	beq	L17
 414  012f 040115            	dbeq	b,L37
 415  0132 2024              	bra	L321
 416  0134                   L17:
 417                         ; 291 			AD_mux_Filter[MUX2][AD_mux_feed].unflt_rslt_w = Adc_SingleValueReadChannel(VAD_MUX2_OUT);
 419  0134 e680              	ldab	OFST-2,s
 420  0136 8606              	ldaa	#6
 421  0138 12                	mul	
 422  0139 3b                	pshd	
 423  013a cc0005            	ldd	#5
 424  013d 160000            	jsr	_Adc_SingleValueReadChannel
 426  0140 31                	puly	
 427  0141 6cea0080          	std	_AD_mux_Filter+48,y
 428                         ; 292 			break;
 430  0145 2011              	bra	L321
 431  0147                   L37:
 432                         ; 297 			AD_mux_Filter[MUX1][AD_mux_feed].unflt_rslt_w = Adc_SingleValueReadChannel(VAD_MUX1_OUT);
 434  0147 e680              	ldab	OFST-2,s
 435  0149 8606              	ldaa	#6
 436  014b 12                	mul	
 437  014c 3b                	pshd	
 438  014d cc0006            	ldd	#6
 439  0150 160000            	jsr	_Adc_SingleValueReadChannel
 441  0153 31                	puly	
 442  0154 6cea0050          	std	_AD_mux_Filter,y
 443                         ; 298 			break;
 445  0158                   L321:
 446                         ; 305 }
 449  0158 1b84              	leas	4,s
 450  015a 3d                	rts	
 490                         ; 338 void near ADF_Str_UnFlt_Rslt(u_8Bit AD_Filter_control)
 490                         ; 339 {
 491                         	switch	.text
 492  015b                   _ADF_Str_UnFlt_Rslt:
 494  015b 3b                	pshd	
 495  015c 37                	pshb	
 496       00000001          OFST:	set	1
 499                         ; 344 	AD_control = AD_Filter_control;
 501  015d 6b80              	stab	OFST-1,s
 502                         ; 346 	switch (AD_control) {
 505  015f 87                	clra	
 506  0160 c108              	cmpb	#8
 507  0162 245a              	bhs	L141
 508  0164 59                	lsld	
 509  0165 05ff              	jmp	[d,pc]
 510  0167 0177              	dc.w	L521
 511  0169 0181              	dc.w	L721
 512  016b 018c              	dc.w	L131
 513  016d 0197              	dc.w	L331
 514  016f 01a2              	dc.w	L531
 515  0171 01be              	dc.w	L141
 516  0173 01be              	dc.w	L141
 517  0175 01ad              	dc.w	L731
 518  0177                   L521:
 519                         ; 351 		    AD_Filter_set[AD_control].unflt_rslt_w = Adc_SingleValueReadChannel(VAD_U12T);
 521  0177 e680              	ldab	OFST-1,s
 522  0179 8606              	ldaa	#6
 523  017b 12                	mul	
 524  017c 3b                	pshd	
 525  017d 87                	clra	
 526  017e c7                	clrb	
 528                         ; 352 		    break;
 530  017f 2035              	bra	LC001
 531  0181                   L721:
 532                         ; 357 			AD_Filter_set[AD_control].unflt_rslt_w = Adc_SingleValueReadChannel(VAD_ISENSE_HS_RL);
 534  0181 e680              	ldab	OFST-1,s
 535  0183 8606              	ldaa	#6
 536  0185 12                	mul	
 537  0186 3b                	pshd	
 538  0187 cc0001            	ldd	#1
 540                         ; 358 			break;
 542  018a 202a              	bra	LC001
 543  018c                   L131:
 544                         ; 363 			AD_Filter_set[AD_control].unflt_rslt_w = Adc_SingleValueReadChannel(VAD_ISENSE_HS_RR);
 546  018c e680              	ldab	OFST-1,s
 547  018e 8606              	ldaa	#6
 548  0190 12                	mul	
 549  0191 3b                	pshd	
 550  0192 cc0002            	ldd	#2
 552                         ; 364 			break;
 554  0195 201f              	bra	LC001
 555  0197                   L331:
 556                         ; 369 			AD_Filter_set[AD_control].unflt_rslt_w = Adc_SingleValueReadChannel(VAD_ISENSE_HS_FL);
 558  0197 e680              	ldab	OFST-1,s
 559  0199 8606              	ldaa	#6
 560  019b 12                	mul	
 561  019c 3b                	pshd	
 562  019d cc0003            	ldd	#3
 564                         ; 371 			break;
 566  01a0 2014              	bra	LC001
 567  01a2                   L531:
 568                         ; 377 			AD_Filter_set[AD_control].unflt_rslt_w = Adc_SingleValueReadChannel(VAD_ISENSE_HS_FR);
 570  01a2 e680              	ldab	OFST-1,s
 571  01a4 8606              	ldaa	#6
 572  01a6 12                	mul	
 573  01a7 3b                	pshd	
 574  01a8 cc0004            	ldd	#4
 576                         ; 378 			break;
 578  01ab 2009              	bra	LC001
 579  01ad                   L731:
 580                         ; 383 			AD_Filter_set[AD_control].unflt_rslt_w = Adc_SingleValueReadChannel(VAD_UINT);
 582  01ad e680              	ldab	OFST-1,s
 583  01af 8606              	ldaa	#6
 584  01b1 12                	mul	
 585  01b2 3b                	pshd	
 586  01b3 cc0007            	ldd	#7
 588  01b6                   LC001:
 589  01b6 160000            	jsr	_Adc_SingleValueReadChannel
 590  01b9 31                	puly	
 591  01ba 6cea0020          	std	_AD_Filter_set,y
 592                         ; 384 			break;
 594  01be                   L141:
 595                         ; 391 }
 598  01be 1b83              	leas	3,s
 599  01c0 3d                	rts	
 640                         ; 430 void near ADF_AtoD_Cnvrsn(u_8Bit AD_Filter_supply)
 640                         ; 431 {
 641                         	switch	.text
 642  01c1                   _ADF_AtoD_Cnvrsn:
 644  01c1 3b                	pshd	
 645  01c2 37                	pshb	
 646       00000001          OFST:	set	1
 649                         ; 436 	AD_supply = AD_Filter_supply;
 651  01c3 6b80              	stab	OFST-1,s
 652                         ; 439 	Adc_StartGroupConversion(AD_supply);
 654  01c5 87                	clra	
 655  01c6 160000            	jsr	_Adc_StartGroupConversion
 658  01c9 200b              	bra	L502
 659  01cb                   L302:
 660                         ; 445 		AD_Filter_cnvrsn_stat_c[AD_supply] = Adc_GetGroupStatus(AD_supply);
 662  01cb e680              	ldab	OFST-1,s
 663  01cd 3b                	pshd	
 664  01ce 160000            	jsr	_Adc_GetGroupStatus
 666  01d1 31                	puly	
 667  01d2 6bea0000          	stab	_AD_Filter_cnvrsn_stat_c,y
 668  01d6                   L502:
 669                         ; 443 	while (AD_Filter_cnvrsn_stat_c[AD_supply] != ADC_COMPLETED) {
 671  01d6 e680              	ldab	OFST-1,s
 672  01d8 87                	clra	
 673  01d9 b746              	tfr	d,y
 674  01db e6ea0000          	ldab	_AD_Filter_cnvrsn_stat_c,y
 675  01df c103              	cmpb	#3
 676  01e1 26e8              	bne	L302
 677                         ; 448 	AD_Filter_cnvrsn_stat_c[AD_supply] = ADC_IDLE;
 679  01e3 69ea0000          	clr	_AD_Filter_cnvrsn_stat_c,y
 680                         ; 449 }
 683  01e7 1b83              	leas	3,s
 684  01e9 3d                	rts	
 729                         ; 473 u_8Bit near ADF_Nrmlz_Vsense(u_16Bit AD_Vsense_value)
 729                         ; 474 {
 730                         	switch	.text
 731  01ea                   _ADF_Nrmlz_Vsense:
 733  01ea 3b                	pshd	
 734  01eb 1b9b              	leas	-5,s
 735       00000005          OFST:	set	5
 738                         ; 476 	u_16Bit AD_Vsense = AD_Vsense_value;       // filtered Vsense AD value
 740  01ed 6c83              	std	OFST-2,s
 741                         ; 480 	AD_nrmlzd_Vsense = (u_8Bit) ( ( (AD_Vsense<<4) + ((AD_Vsense*AD_N4)/AD_N10) ) / AD_N100);
 743  01ef 59                	lsld	
 744  01f0 59                	lsld	
 745  01f1 ce000a            	ldx	#10
 746  01f4 1810              	idiv	
 747  01f6 6e80              	stx	OFST-5,s
 748  01f8 ec83              	ldd	OFST-2,s
 749  01fa 59                	lsld	
 750  01fb 59                	lsld	
 751  01fc 59                	lsld	
 752  01fd 59                	lsld	
 753  01fe e380              	addd	OFST-5,s
 754  0200 ce0064            	ldx	#100
 755  0203 1810              	idiv	
 756  0205 b754              	tfr	x,d
 757                         ; 483 	return(AD_nrmlzd_Vsense);
 761  0207 1b87              	leas	7,s
 762  0209 3d                	rts	
 825                         ; 509 u_8Bit near ADF_Nrmlz_Isense(u_16Bit AD_Isense_value)
 825                         ; 510 {
 826                         	switch	.text
 827  020a                   _ADF_Nrmlz_Isense:
 829  020a 3b                	pshd	
 830  020b 1b9c              	leas	-4,s
 831       00000004          OFST:	set	4
 834                         ; 511 	u_16Bit AD_Isense = AD_Isense_value;         // Filtered Isense AD value
 836  020d 6c82              	std	OFST-2,s
 837                         ; 512 	u_8Bit  AD_nrmlzd_Isense = 0;                // local variable for converted Isense (in 100mA)
 839                         ; 513 	u_8Bit AD_nrmlzd_Isense_interim = 0;		 // interimRresult: ROUNDUP ((Iref_seatH*49)/10)
 841                         ; 514 	u_8Bit AD_nrmlzd_Isense_interim2 = 0;		 // interimResult2: ROUNDUP (Vref_seatH/10)
 843                         ; 524 	if(AD_Vref_seatH_w >= AD_N10){
 845  020f fc00b3            	ldd	_AD_Vref_seatH_w
 846  0212 8c000a            	cpd	#10
 847  0215 252e              	blo	L552
 848                         ; 525 		AD_nrmlzd_Isense_interim = (u_8Bit) (((AD_Iref_seatH_c*AD_N49)/AD_N10)+AD_N1);
 850  0217 f600b5            	ldab	_AD_Iref_seatH_c
 851  021a 8631              	ldaa	#49
 852  021c 12                	mul	
 853  021d ce000a            	ldx	#10
 854  0220 1815              	idivs	
 855  0222 b754              	tfr	x,d
 856  0224 c30001            	addd	#1
 857  0227 6b81              	stab	OFST-3,s
 858                         ; 526 		AD_nrmlzd_Isense_interim2 = (u_8Bit) ((AD_Vref_seatH_w/AD_N10)+AD_N1);
 860  0229 fc00b3            	ldd	_AD_Vref_seatH_w
 861  022c ce000a            	ldx	#10
 862  022f 1810              	idiv	
 863  0231 188b0001          	addx	#1
 864  0235 b754              	tfr	x,d
 865                         ; 527 		AD_nrmlzd_Isense = (u_8Bit) ((AD_Isense*AD_nrmlzd_Isense_interim)/AD_nrmlzd_Isense_interim2);
 867  0237 87                	clra	
 868  0238 b745              	tfr	d,x
 869  023a e681              	ldab	OFST-3,s
 870  023c ed82              	ldy	OFST-2,s
 871  023e 13                	emul	
 872  023f 1810              	idiv	
 873  0241 b754              	tfr	x,d
 875  0243 2001              	bra	L752
 876  0245                   L552:
 877                         ; 529 		AD_nrmlzd_Isense = 0;
 879  0245 c7                	clrb	
 880  0246                   L752:
 881                         ; 533 	return(AD_nrmlzd_Isense);
 885  0246 1b86              	leas	6,s
 886  0248 3d                	rts	
 933                         ; 555 u_8Bit near ADF_Nrmlz_Vs_Vsense(u_16Bit AD_vs_Vsense_value)
 933                         ; 556 {
 934                         	switch	.text
 935  0249                   _ADF_Nrmlz_Vs_Vsense:
 937  0249 3b                	pshd	
 938  024a 1b9b              	leas	-5,s
 939       00000005          OFST:	set	5
 942                         ; 558 	u_16Bit AD_vs_Vsense = (AD_vs_Vsense_value+AD_N3);  // Filtered Vsense AD value
 944  024c c30003            	addd	#3
 945  024f 6c83              	std	OFST-2,s
 946                         ; 562 	AD_nrmlzd_vs_Vsense = (u_8Bit) ( ( (AD_vs_Vsense*AD_N42)+(AD_vs_Vsense>>AD_N1) ) >> AD_N8);
 948  0251 49                	lsrd	
 949  0252 6c80              	std	OFST-5,s
 950  0254 ec83              	ldd	OFST-2,s
 951  0256 cd002a            	ldy	#42
 952  0259 13                	emul	
 953  025a e380              	addd	OFST-5,s
 954                         ; 565 	return(AD_nrmlzd_vs_Vsense);
 956  025c b701              	tfr	a,b
 959  025e 1b87              	leas	7,s
 960  0260 3d                	rts	
1007                         ; 587 u_8Bit near ADF_Nrmlz_Vs_Isense(u_16Bit AD_vs_Isense_value)
1007                         ; 588 {
1008                         	switch	.text
1009  0261                   _ADF_Nrmlz_Vs_Isense:
1011  0261 3b                	pshd	
1012  0262 1b9b              	leas	-5,s
1013       00000005          OFST:	set	5
1016                         ; 590 	u_16Bit AD_vs_Isense = (AD_vs_Isense_value+AD_N3); // Isense AD value
1018  0264 c30003            	addd	#3
1019  0267 6c82              	std	OFST-3,s
1020                         ; 591 	u_8Bit AD_vs_nrmlzd_Isense = 0;                    // local variable for normalized Isense (in 100mA)
1022                         ; 595 	AD_vs_nrmlzd_Isense = (u_8Bit) ( ( (AD_vs_Isense*AD_N13)-(AD_vs_Isense>>AD_N2) ) / AD_N100 );
1024  0269 49                	lsrd	
1025  026a 49                	lsrd	
1026  026b 6c80              	std	OFST-5,s
1027  026d ec82              	ldd	OFST-3,s
1028  026f cd000d            	ldy	#13
1029  0272 13                	emul	
1030  0273 a380              	subd	OFST-5,s
1031  0275 ce0064            	ldx	#100
1032  0278 1810              	idiv	
1033  027a b754              	tfr	x,d
1034                         ; 598 	return(AD_vs_nrmlzd_Isense);
1038  027c 1b87              	leas	7,s
1039  027e 3d                	rts	
1105                         ; 624 u_8Bit near ADF_Nrmlz_StW_Isense(u_16Bit AD_Stw_Isense_value)
1105                         ; 625 {
1106                         	switch	.text
1107  027f                   _ADF_Nrmlz_StW_Isense:
1109  027f 3b                	pshd	
1110  0280 1b9c              	leas	-4,s
1111       00000004          OFST:	set	4
1114                         ; 627 	u_16Bit AD_Stw_Isense = AD_Stw_Isense_value;  // Filtered Isense AD value
1116  0282 6c82              	std	OFST-2,s
1117                         ; 628 	u_8Bit  AD_Stw_nrmlzd_Isense = 0;             // local variable for converted Isense (in 100mA)
1119                         ; 629 	u_8Bit AD_Stw_nrmlzd_Isense_interim = 0;	  // interimResult: ROUNDDOWN (Iref_STW*49)/10
1121                         ; 630 	u_8Bit AD_Stw_nrmlzd_Isense_interim2 = 0;	  // interimResult2: ROUNDDOWN (Vref_STW/10)
1123                         ; 641 	if(AD_Vref_STW_w >= AD_N10){
1125  0284 fc00b0            	ldd	_AD_Vref_STW_w
1126  0287 8c000a            	cpd	#10
1127  028a 2527              	blo	L543
1128                         ; 642 		AD_Stw_nrmlzd_Isense_interim = (u_8Bit) ((AD_Iref_STW_c*AD_N49)/AD_N10);
1130  028c f600b2            	ldab	_AD_Iref_STW_c
1131  028f 8631              	ldaa	#49
1132  0291 12                	mul	
1133  0292 ce000a            	ldx	#10
1134  0295 1815              	idivs	
1135  0297 b754              	tfr	x,d
1136  0299 6b81              	stab	OFST-3,s
1137                         ; 643 		AD_Stw_nrmlzd_Isense_interim2 = (u_8Bit) (AD_Vref_STW_w/AD_N10);
1139  029b fc00b0            	ldd	_AD_Vref_STW_w
1140  029e ce000a            	ldx	#10
1141  02a1 1810              	idiv	
1142  02a3 b754              	tfr	x,d
1143                         ; 644 		AD_Stw_nrmlzd_Isense = (u_8Bit) ((AD_Stw_Isense*AD_Stw_nrmlzd_Isense_interim)/AD_Stw_nrmlzd_Isense_interim2);
1145  02a5 87                	clra	
1146  02a6 b745              	tfr	d,x
1147  02a8 e681              	ldab	OFST-3,s
1148  02aa ed82              	ldy	OFST-2,s
1149  02ac 13                	emul	
1150  02ad 1810              	idiv	
1151  02af b754              	tfr	x,d
1153  02b1 2001              	bra	L743
1154  02b3                   L543:
1155                         ; 646 		AD_Stw_nrmlzd_Isense = 0;
1157  02b3 c7                	clrb	
1158  02b4                   L743:
1159                         ; 650 	return(AD_Stw_nrmlzd_Isense);
1163  02b4 1b86              	leas	6,s
1164  02b6 3d                	rts	
1232                         ; 671 u_8Bit near ADF_Hs_PrtlOpen_Z_Cnvrsn(u_16Bit Vsense_AD, u_16Bit Isense_AD)
1232                         ; 672 {
1233                         	switch	.text
1234  02b7                   _ADF_Hs_PrtlOpen_Z_Cnvrsn:
1236  02b7 3b                	pshd	
1237  02b8 1b9b              	leas	-5,s
1238       00000005          OFST:	set	5
1241                         ; 674 	u_16Bit AD_hs_Vsense_w = Vsense_AD;                 // stores 10-bit Heated seat Vsense reading
1243  02ba 6c83              	std	OFST-2,s
1244                         ; 675 	u_16Bit AD_hs_Isense_w = Isense_AD;                 // stores 10-bit Heated seat Isense reading
1246  02bc ec89              	ldd	OFST+4,s
1247  02be 6c80              	std	OFST-5,s
1248                         ; 676 	u_16Bit AD_hs_interim_rslt_w = 0;                   // 16-bit Interim Impedence result
1250                         ; 677 	u_8Bit  AD_hs_Z_rslt_c = 0;                         // 8-bit Heated seat impedence result
1252                         ; 680 	if (AD_hs_Isense_w > AD_INVL_ISENSE) {
1254  02c0 271d              	beq	L773
1255                         ; 683 		AD_hs_interim_rslt_w = ( (AD_hs_Vsense_w*AD_N61) / (AD_hs_Isense_w*AD_N5) );
1257  02c2 cd0005            	ldy	#5
1258  02c5 13                	emul	
1259  02c6 b745              	tfr	d,x
1260  02c8 ec83              	ldd	OFST-2,s
1261  02ca cd003d            	ldy	#61
1262  02cd 13                	emul	
1263  02ce 1810              	idiv	
1264  02d0 6e80              	stx	OFST-5,s
1265                         ; 686 		if (AD_hs_interim_rslt_w <= AD_MAX_Z_RSLT) {
1267  02d2 b754              	tfr	x,d
1268  02d4 8c00ff            	cpd	#255
1269                         ; 688 			AD_hs_Z_rslt_c = (u_8Bit) AD_hs_interim_rslt_w; 
1272  02d7 2302              	bls	LC002
1273                         ; 692 			AD_hs_Z_rslt_c = (u_8Bit) AD_MAX_Z_RSLT;
1275  02d9 c6ff              	ldab	#255
1276  02db                   LC002:
1277  02db 6b82              	stab	OFST-3,s
1278  02dd 2002              	bra	L504
1279  02df                   L773:
1280                         ; 697 		AD_hs_Z_rslt_c = (u_8Bit) AD_MAX_Z_RSLT;
1282  02df c6ff              	ldab	#255
1283  02e1                   L504:
1284                         ; 700 	return(AD_hs_Z_rslt_c);
1288  02e1 1b87              	leas	7,s
1289  02e3 3d                	rts	
1531                         	switch	.bss
1532  0000                   _AD_Filter_cnvrsn_stat_c:
1533  0000 0000000000000000  	ds.b	8
1534                         	xdef	_AD_Filter_cnvrsn_stat_c
1535  0008                   _AD_Filter_mux_flg:
1536  0008 0000000000000000  	ds.b	16
1537                         	xdef	_AD_Filter_mux_flg
1538  0018                   _AD_Filter_flg:
1539  0018 0000000000000000  	ds.b	8
1540                         	xdef	_AD_Filter_flg
1541                         	xref	_Adc_GetGroupStatus
1542                         	xref	_Adc_SingleValueReadChannel
1543                         	xref	_Adc_StartGroupConversion
1544                         	xdef	_ADF_Nrmlz_Vs_Isense
1545                         	xdef	_ADF_Nrmlz_Vs_Vsense
1546                         	xdef	_ADF_Hs_PrtlOpen_Z_Cnvrsn
1547                         	xdef	_ADF_Nrmlz_StW_Isense
1548                         	xdef	_ADF_Nrmlz_Isense
1549                         	xdef	_ADF_Nrmlz_Vsense
1550                         	xdef	_ADF_AtoD_Cnvrsn
1551                         	xdef	_ADF_Str_UnFlt_Rslt
1552                         	xdef	_ADF_Str_UnFlt_Mux_Rslt
1553                         	xdef	_ADF_Mux_Filter
1554                         	xdef	_ADF_Filter
1555                         	xdef	_ADF_Init
1556  0020                   _AD_Filter_set:
1557  0020 0000000000000000  	ds.b	48
1558                         	xdef	_AD_Filter_set
1559  0050                   _AD_mux_Filter:
1560  0050 0000000000000000  	ds.b	96
1561                         	xdef	_AD_mux_Filter
1562  00b0                   _AD_Vref_STW_w:
1563  00b0 0000              	ds.b	2
1564                         	xdef	_AD_Vref_STW_w
1565  00b2                   _AD_Iref_STW_c:
1566  00b2 00                	ds.b	1
1567                         	xdef	_AD_Iref_STW_c
1568  00b3                   _AD_Vref_seatH_w:
1569  00b3 0000              	ds.b	2
1570                         	xdef	_AD_Vref_seatH_w
1571  00b5                   _AD_Iref_seatH_c:
1572  00b5 00                	ds.b	1
1573                         	xdef	_AD_Iref_seatH_c
1594                         	end
