   1                         ;	C STARTUP FOR S12X
   2                         ;	WITH AUTOMATIC DATA/CODE INITIALISATION
   3                         ;	Copyright (c) 2006 by COSMIC Software
   4                         ;
   5                         	xdef	_exit, __stext
   6                         	xdef    __Startup		; Added
   7                         	xref	_main
   8                         ;	xref	f_main
   9                         	xref	__sbss, __memory, __idesc__, __stack, __sgdata, __sdirect
  10                         	xref	__subsct, __eubsct, __sgbss, __egbss, __sgdflt
  11                         	xref	_GPAGE, _RPAGE, _PPAGE, _DIRECT
  12                         ;
  13                         ;__stext:
  14  0000                   __Startup:				; Rename the subroutine
  15                         ;	lds	#__stack		; initialize stack pointer	; Stack pointer is initiliazed before crtsx
  16  0000 180bfd0000        	movb	#$FD,_RPAGE		; default ram configuration
  17  0005 180b000000        	movb	#high(__sdirect),_DIRECT ; direct page register
  18                         ;	movb	#page(__idesc__),_PPAGE	; allows banked descriptor	; Commented. With it, we failed to return to main.
  19  000a ce0000            	ldx	#__idesc__		; descriptor address
  20  000d ed31              	ldy	2,x+			; start address of prom data
  21  000f                   ibcl:
  22  000f a634              	ldaa	5,x+			; test flag byte
  23  0011 2737              	beq	zbss			; no more segment
  24  0013 2a02              	bpl	nopg			; page indicator
  25  0015 1a02              	leax	2,x			; skip it
  26  0017                   nopg:
  27  0017 8560              	bita	#$60			; test for moveable code segment
  28  0019 2604              	bne	dseg			; no, copy it
  29  001b ed1e              	ldy	-2,x			; reload code address
  30  001d 20f0              	bra	ibcl			; and continue with next segment
  31  001f                   dseg:
  32  001f 34                	pshx				; save pointer
  33  0020 97                	tsta				; global addresses
  34  0021 2b10              	bmi	glob			; yes, copy
  35  0023 b764              	tfr	y,d			; start address
  36  0025 a31e              	subd	-2,x			; minus end address
  37  0027 ee1c              	ldx	-4,x			; destination address
  38  0029                   dbcl:
  39  0029 180a7030          	movb	1,y+,1,x+		; copy from prom to ram
  40  002d 04b4f9            	ibne	d,dbcl			; count up and loop
  41  0030 30                	pulx				; reload pointer to desc
  42  0031 20dc              	bra	ibcl			; and loop
  43  0033                   glob:
  44  0033 ec1e              	ldd	-2,x			; end address
  45  0035 3b                	pshd				; on the stack
  46  0036 180d1b0000        	movb	-5,x,_GPAGE		; set page register
  47  003b ee1c              	ldx	-4,x			; destination address
  48  003d                   xbcl:
  49  003d e670              	ldab	1,y+			; read data
  50  003f 186b30            	gstab	1,x+			; and write to ram
  51  0042 ad80              	cpy	0,s			; compare with end address
  52  0044 25f7              	blo	xbcl			; and loop back
  53  0046 30                	pulx				; clean stack
  54  0047 30                	pulx				; reload pointer to desc
  55  0048 20c5              	bra	ibcl			; and loop
  56  004a                   zbss:
  57  004a ce0000            	ldx	#__sbss			; start of bss
  58  004d c7                	clrb				; complete zero
  59  004e 2002              	bra	loop			; start loop
  60  0050                   zbcl:
  61  0050 6c31              	std	2,x+			; clear word
  62  0052                   loop:
  63  0052 8e0000            	cpx	#__memory		; end of bss
  64  0055 25f9              	blo	zbcl			; no, continue
  65  0057 ce0000            	ldx	#__subsct		; start of ubsct
  66  005a 2002              	bra	uloop			; start loop
  67  005c                   zubs:
  68  005c 6b30              	stab	1,x+			; clear byte
  69  005e                   uloop:
  70  005e 8e0000            	cpx	#__eubsct		; end of ubsct
  71  0061 25f9              	blo	zubs			; no continue
  72  0063 180b000000        	movb	#phigh(__sgdata),_GPAGE ; set global page register
  73  0068 ce0000            	ldx	#__sgbss		; start of gbss
  74  006b 2003              	bra	gloop			; start loop
  75  006d                   zgbss:
  76  006d 186c31            	gstd	2,x+			; clear word
  77  0070                   gloop:
  78  0070 8e0000            	cpx	#__egbss		; end of gbss
  79  0073 25f8              	blo	zgbss			; no, continue
  80  0075 180b000000        	movb	#phigh(__sgdflt),_GPAGE ; set global page default
  81                         ;	jsr	_main			; execute main			; This subrotine is called from main
  82  007a 3d                	rts								; Return to main			
  83                         ;	call	f_main			; in case main is banked
  84  007b                   _exit:
  85  007b 20fe              	bra	_exit			; stay here
  86                         ;
  87                         	end
