   1                         ; C Compiler for S12XE [COSMIC Software].
   2                         ; Generator V4.7.8 - 27 Jul 2007
   3                         ; Optimizer V4.6.13 - 27 Jul 2007
   4                         	switch	.data
   5  0000                   _RamTst_ExecutionStatus:
   6  0000 00                	dc.b	0
   7  0001                   _RamTst_PresentAlgorithm:
   8  0001 00                	dc.b	0
   9  0002                   L3_RamTst_AlgorithmFunctionPtr:
  10  0002 0000              	dc.w	0
  67                         ; 184 uint8 DataSaveLocationCheck(void)
  67                         ; 185 {
  68                         	switch	.text
  69  0000                   _DataSaveLocationCheck:
  71  0000 1b9d              	leas	-3,s
  72       00000003          OFST:	set	3
  75                         ; 192     Data_0x55 = (uint8)0x55;
  77                         ; 193     Data_0xAA = (uint8)0xAA;
  79                         ; 194     DataSave_Result = NO_CONTENT;
  81                         ; 197     RamTst_DataSave1 =  Data_0x55;
  83  0002 c655              	ldab	#85
  84  0004 7b0000            	stab	_RamTst_DataSave1
  85                         ; 200     if(0x55 == RamTst_DataSave1)
  87  0007 f60000            	ldab	_RamTst_DataSave1
  88  000a c155              	cmpb	#85
  89  000c 262e              	bne	L33
  90                         ; 203         RamTst_DataSave1 =  Data_0xAA;
  92  000e c6aa              	ldab	#170
  93  0010 7b0000            	stab	_RamTst_DataSave1
  94                         ; 206         if(0xAA == RamTst_DataSave1) 
  96  0013 f60000            	ldab	_RamTst_DataSave1
  97  0016 c1aa              	cmpb	#170
  98  0018 261c              	bne	L14
  99                         ; 209             RamTst_DataSave2 =  Data_0x55;
 101  001a c655              	ldab	#85
 102  001c 7b0000            	stab	_RamTst_DataSave2
 103                         ; 212             if(0x55 == RamTst_DataSave2) 
 105  001f f60000            	ldab	_RamTst_DataSave2
 106  0022 c155              	cmpb	#85
 107  0024 2610              	bne	L14
 108                         ; 215                 RamTst_DataSave2 =  Data_0xAA;
 110  0026 c6aa              	ldab	#170
 111  0028 7b0000            	stab	_RamTst_DataSave2
 112                         ; 218                 if(0xAA == RamTst_DataSave2) 
 114  002b f60000            	ldab	_RamTst_DataSave2
 115  002e c1aa              	cmpb	#170
 116  0030 2604              	bne	L14
 117                         ; 220                     DataSave_Result =DATA_SAVE_LOCATION_OK; 
 119  0032 c601              	ldab	#1
 121  0034 2002              	bra	LC001
 122  0036                   L14:
 123                         ; 226                     DataSave_Result =DATA_SAVE_LOCATION_NOT_OK;
 125                         ; 233                 DataSave_Result =DATA_SAVE_LOCATION_NOT_OK;
 127                         ; 240             DataSave_Result =DATA_SAVE_LOCATION_NOT_OK;
 129  0036 c602              	ldab	#2
 130  0038                   LC001:
 131  0038 6b80              	stab	OFST-3,s
 132  003a 2002              	bra	L15
 133  003c                   L33:
 134                         ; 247         DataSave_Result =DATA_SAVE_LOCATION_NOT_OK;
 136  003c c602              	ldab	#2
 137  003e                   L15:
 138                         ; 251     return DataSave_Result;
 142  003e 1b83              	leas	3,s
 143  0040 3d                	rts	
 191                         ; 275 void RamTst_Init(void) 
 191                         ; 276 
 191                         ; 277 {
 192                         	switch	.text
 193  0041                   _RamTst_Init:
 195       00000002          OFST:	set	2
 198                         ; 279     RamTst_BlockIDType Counter_BlockID = NO_CONTENT;
 200  0041 18c7              	clry	
 201                         ; 282     RamTst_PresentAlgorithm =  RAMTST_DEFAULT_TEST_ALGORITHM;
 203  0043 c601              	ldab	#1
 204  0045 7b0001            	stab	_RamTst_PresentAlgorithm
 205                         ; 283     RamTst_PresentBlock = NO_CONTENT;
 207  0048 7d000c            	sty	_RamTst_PresentBlock
 208                         ; 284     RamTst_PresentAddress = PUT_NEW_ADDRESS;
 210  004b 87                	clra	
 211  004c 7c0008            	std	_RamTst_PresentAddress+2
 212  004f c7                	clrb	
 213  0050 7c0006            	std	_RamTst_PresentAddress
 214                         ; 285     RamTst_TestedCellCounter = NO_CONTENT;   
 216  0053 7d0004            	sty	_RamTst_TestedCellCounter
 217                         ; 286     RamTst_BlockIDForAlgo = NO_CONTENT;
 219  0056 7d0000            	sty	_RamTst_BlockIDForAlgo
 220                         ; 287     RamTst_BlockStartAddressForAlgo = 0;
 222  0059 7d0000            	sty	_RamTst_BlockStartAddressForAlgo
 223  005c 7d0002            	sty	_RamTst_BlockStartAddressForAlgo+2
 224                         ; 288     RamTst_BlockEndAddressForAlgo = 0;
 226  005f 7d0000            	sty	_RamTst_BlockEndAddressForAlgo
 227  0062 7d0002            	sty	_RamTst_BlockEndAddressForAlgo+2
 228                         ; 289     RamTst_Result = NULL_PTR;
 230  0065 7d0000            	sty	_RamTst_Result
 231                         ; 290     RamTst_PresentNumberOfTestedCell = 0;
 233  0068 7d000a            	sty	_RamTst_PresentNumberOfTestedCell
 234                         ; 292     RamTst_NumberOfNewTestedCells = RAMTST_PRESENT_NUMBER_OF_TESTED_CELLS;
 236  006b 18040008000e      	movw	_RamTst_Config+8,_RamTst_NumberOfNewTestedCells
 237                         ; 293     RamTst_NewAlgorithm = RAMTST_PRESENT_DEFAULT_TEST_ALGORITHM;
 239  0071 52                	incb	
 240  0072 7b0010            	stab	_RamTst_NewAlgorithm
 241                         ; 296     for(Counter_BlockID = 0;Counter_BlockID<RAMTST_TEST_TOTAL_NUMBER_OF_BLOCKS;\
 243  0075                   L76:
 244                         ; 299         RamTst_TestResult[Counter_BlockID]= RAMTST_RESULT_NOT_TESTED;
 246  0075 69ea0011          	clr	_RamTst_TestResult,y
 247                         ; 296     for(Counter_BlockID = 0;Counter_BlockID<RAMTST_TEST_TOTAL_NUMBER_OF_BLOCKS;\
 247                         ; 297         ++Counter_BlockID) 
 249  0079 02                	iny	
 250                         ; 296     for(Counter_BlockID = 0;Counter_BlockID<RAMTST_TEST_TOTAL_NUMBER_OF_BLOCKS;\
 252  007a 27f9              	beq	L76
 253                         ; 303     RamTst_AlgorithmFunctionPtr[(RamTst_DataType)RAMTST_CHECKERBOARD_TEST-\
 253                         ; 304         (RamTst_DataType)1] = CheckerboardTest;
 255  007c cc0000            	ldd	#_CheckerboardTest
 256  007f 7c0002            	std	L3_RamTst_AlgorithmFunctionPtr
 257                         ; 312     RamTst_ExecutionStatus = RAMTST_EXECUTION_INIT;
 259  0082 c601              	ldab	#1
 260  0084 7b0000            	stab	_RamTst_ExecutionStatus
 261                         ; 314     return;
 264  0087 3d                	rts	
 327                         ; 329 RamTst_TestResultType RamTst_GetTestResult ( void )
 327                         ; 330 
 327                         ; 331 {
 328                         	switch	.text
 329  0088                   _RamTst_GetTestResult:
 333                         ; 334     return RamTst_TestResult[PRESENT_BLOCK_ID];
 335  0088 fd0000            	ldy	_RamTst_BlockIDForAlgo
 336  008b e6ea0011          	ldab	_RamTst_TestResult,y
 339  008f 3d                	rts	
 418                         ; 355 void RamTst_GetVersionInfo ( Std_VersionInfoType *versioninfo )
 418                         ; 356 
 418                         ; 357 {
 419                         	switch	.text
 420  0090                   _RamTst_GetVersionInfo:
 422       00000000          OFST:	set	0
 425                         ; 359     if(NULL_PTR != versioninfo )
 427  0090 6cae              	std	2,-s
 428  0092 2715              	beq	L751
 429                         ; 367         versioninfo->moduleID         = RAMTST_MODULE_ID;
 431  0094 c65d              	ldab	#93
 432  0096 ed80              	ldy	OFST+0,s
 433  0098 6b42              	stab	2,y
 434                         ; 368         versioninfo->vendorID         = RAMTST_VENDOR_ID;
 436  009a cc0028            	ldd	#40
 437  009d 6c40              	std	0,y
 438                         ; 369         versioninfo->sw_major_version = RAMTST_SW_MAJOR_VERSION_C;
 440  009f c601              	ldab	#1
 441  00a1 6b43              	stab	3,y
 442                         ; 370         versioninfo->sw_minor_version = RAMTST_SW_MINOR_VERSION_C;
 444  00a3 6944              	clr	4,y
 445                         ; 371         versioninfo->sw_patch_version = RAMTST_SW_PATCH_VERSION_C;
 447  00a5 c603              	ldab	#3
 448  00a7 6b45              	stab	5,y
 449  00a9                   L751:
 450                         ; 374 }
 453  00a9 31                	puly	
 454  00aa 3d                	rts	
 496                         ; 394 void RamTst_MainFunction ( void )
 496                         ; 395 
 496                         ; 396 {
 497                         	switch	.text
 498  00ab                   _RamTst_MainFunction:
 502                         ; 401     if(DATA_SAVE_LOCATION_NOT_OK == DataSaveLocationCheck())
 504  00ab 160000            	jsr	_DataSaveLocationCheck
 506  00ae c102              	cmpb	#2
 507  00b0 260a              	bne	L171
 508                         ; 404         if( NULL_PTR != RamTst_Config.RamTst_ErrorNotification_Ptr)
 510  00b2 fc0005            	ldd	_RamTst_Config+5
 511  00b5 2704              	beq	L371
 512                         ; 407             RamTst_Config.RamTst_ErrorNotification_Ptr();
 514  00b7 15fbff4a          	jsr	[_RamTst_Config+5]
 516  00bb                   L371:
 517                         ; 409         return;
 520  00bb 3d                	rts	
 521  00bc                   L171:
 522                         ; 413     if(RAMTST_EXECUTION_STOPPED == RamTst_ExecutionStatus) 
 524  00bc f60000            	ldab	_RamTst_ExecutionStatus
 525  00bf c103              	cmpb	#3
 526  00c1 260a              	bne	L571
 527                         ; 415         if( NULL_PTR != RamTst_Config.RamTst_ErrorNotification_Ptr)
 529  00c3 fc0005            	ldd	_RamTst_Config+5
 530  00c6 2704              	beq	L771
 531                         ; 418             RamTst_Config.RamTst_ErrorNotification_Ptr();
 533  00c8 15fbff39          	jsr	[_RamTst_Config+5]
 535  00cc                   L771:
 536                         ; 420         return;
 539  00cc 3d                	rts	
 540  00cd                   L571:
 541                         ; 424     ((RamTst_ExecutionStatus == RAMTST_EXECUTION_INIT)||
 541                         ; 425     (RamTst_ExecutionStatus == RAMTST_EXECUTION_CONTINUE))?
 541                         ; 426     (RamTst_ExecutionStatus = RAMTST_EXECUTION_RUNNING):\
 541                         ; 427     (RamTst_ExecutionStatusType)NO_OPERATION;
 543  00cd c101              	cmpb	#1
 544  00cf 2704              	beq	L02
 545  00d1 c104              	cmpb	#4
 546  00d3 2605              	bne	L61
 547  00d5                   L02:
 548  00d5 c602              	ldab	#2
 549  00d7 7b0000            	stab	_RamTst_ExecutionStatus
 550  00da                   L61:
 551                         ; 430     RamTst_PresentNumberOfTestedCell = RamTst_NumberOfNewTestedCells; 
 553  00da 1804000e000a      	movw	_RamTst_NumberOfNewTestedCells,_RamTst_PresentNumberOfTestedCell
 554                         ; 433     (RAMTST_ALGORITHM_UNDEFINED == RamTst_PresentAlgorithm)?
 554                         ; 434     (RamTst_PresentAlgorithm = RamTst_NewAlgorithm):\
 554                         ; 435     (RamTst_AlgorithmType)NO_OPERATION;
 556  00e0 f60001            	ldab	_RamTst_PresentAlgorithm
 557  00e3 2606              	bne	L42
 558  00e5 f60010            	ldab	_RamTst_NewAlgorithm
 559  00e8 7b0001            	stab	_RamTst_PresentAlgorithm
 560  00eb                   L42:
 561                         ; 438     RamTst_BlockIDForAlgo           = 
 561                         ; 439         ((RamTst_Config.RamTst_AlgParams[RamTst_PresentAlgorithm-1].\
 561                         ; 440         RamTst_BlockParams+(RamTst_PresentBlock))->RamTst_BlockID - \
 561                         ; 441             (RamTst_DataType)1);
 563  00eb 8607              	ldaa	#7
 564  00ed 12                	mul	
 565  00ee b746              	tfr	d,y
 566  00f0 eeea0005          	ldx	_RamTst_Config+5,y
 567  00f4 fc000c            	ldd	_RamTst_PresentBlock
 568  00f7 cd000a            	ldy	#10
 569  00fa 13                	emul	
 570  00fb eee6              	ldx	d,x
 571  00fd 09                	dex	
 572  00fe 7e0000            	stx	_RamTst_BlockIDForAlgo
 573                         ; 443     if((RamTst_PresentAddress == (RamTst_BlockAddressType)PUT_NEW_ADDRESS)\
 573                         ; 444       &&(RAMTST_RESULT_NOT_TESTED != RamTst_TestResult[PRESENT_BLOCK_ID]))
 575  0101 fc0006            	ldd	_RamTst_PresentAddress
 576  0104 2612              	bne	L102
 577  0106 fc0008            	ldd	_RamTst_PresentAddress+2
 578  0109 04240c            	dbne	d,L102
 580  010c b756              	tfr	x,y
 581  010e e7ea0011          	tst	_RamTst_TestResult,y
 582  0112 2704              	beq	L102
 583                         ; 446         RamTst_TestResult[PRESENT_BLOCK_ID] = RAMTST_RESULT_NOT_TESTED;
 585  0114 69ea0011          	clr	_RamTst_TestResult,y
 586  0118                   L102:
 587                         ; 450     (RamTst_PresentAddress == (RamTst_BlockAddressType)PUT_NEW_ADDRESS)\
 587                         ; 451         ?(RamTst_PresentAddress = PRESENT_BLOCK_STARTING_ADDRESS)\
 587                         ; 452         :(RamTst_BlockAddressType)NO_OPERATION;
 589  0118 fc0006            	ldd	_RamTst_PresentAddress
 590  011b 2625              	bne	L23
 591  011d fc0008            	ldd	_RamTst_PresentAddress+2
 592  0120 04241f            	dbne	d,L23
 593  0123 f60001            	ldab	_RamTst_PresentAlgorithm
 594  0126 8607              	ldaa	#7
 595  0128 12                	mul	
 596  0129 b746              	tfr	d,y
 597  012b eeea0005          	ldx	_RamTst_Config+5,y
 598  012f fc000c            	ldd	_RamTst_PresentBlock
 599  0132 cd000a            	ldy	#10
 600  0135 13                	emul	
 601  0136 1ae6              	leax	d,x
 602  0138 1805040008        	movw	4,x,_RamTst_PresentAddress+2
 603  013d ee02              	ldx	2,x
 604  013f 7e0006            	stx	_RamTst_PresentAddress
 605  0142                   L23:
 606                         ; 455     RamTst_BlockStartAddressForAlgo = 
 606                         ; 456         (RamTst_Config.RamTst_AlgParams[RamTst_PresentAlgorithm-1].\
 606                         ; 457         RamTst_BlockParams+(RamTst_PresentBlock))->RamTst_StartAddress;
 608  0142 f60001            	ldab	_RamTst_PresentAlgorithm
 609  0145 8607              	ldaa	#7
 610  0147 12                	mul	
 611  0148 b746              	tfr	d,y
 612  014a eeea0005          	ldx	_RamTst_Config+5,y
 613  014e fc000c            	ldd	_RamTst_PresentBlock
 614  0151 cd000a            	ldy	#10
 615  0154 13                	emul	
 616  0155 1ae6              	leax	d,x
 617  0157 1805040002        	movw	4,x,_RamTst_BlockStartAddressForAlgo+2
 618  015c 1805020000        	movw	2,x,_RamTst_BlockStartAddressForAlgo
 619                         ; 459     RamTst_BlockEndAddressForAlgo   = 
 619                         ; 460         (RamTst_Config.RamTst_AlgParams[RamTst_PresentAlgorithm-1].\
 619                         ; 461         RamTst_BlockParams+(RamTst_PresentBlock))->RamTst_EndAddress;
 621  0161 1805080002        	movw	8,x,_RamTst_BlockEndAddressForAlgo+2
 622  0166 1805060000        	movw	6,x,_RamTst_BlockEndAddressForAlgo
 623                         ; 463     RamTst_Result = RamTst_TestResult;
 625  016b cc0011            	ldd	#_RamTst_TestResult
 626  016e 7c0000            	std	_RamTst_Result
 627                         ; 466     RamTst_SP_1 = RAM_SP_ADDRESS;
 629  0171 cc3dde            	ldd	#15838
 630  0174 7c0002            	std	_RamTst_SP_1
 631                         ; 469     {_asm ("STS _RamTst_SP_2");} /* Save the present SP in a DATSAVE_RAM variable */ /* MODIFIED */
 634  0177 7f0000            	sts	_RamTst_SP_2
 636                         ; 470     {_asm ("LDS _RamTst_SP_1");} /* Load the SP with an address located in the DATASAVE_RAM*/ /* MODIFIED */
 639  017a ff0002            	lds	_RamTst_SP_1
 641                         ; 474     (*RamTst_AlgorithmFunctionPtr[(RamTst_DataType)RamTst_PresentAlgorithm-\
 641                         ; 475         (RamTst_DataType)1])();
 643  017d f60001            	ldab	_RamTst_PresentAlgorithm
 644  0180 87                	clra	
 645  0181 59                	lsld	
 646  0182 b746              	tfr	d,y
 647  0184 15eb0000          	jsr	[L3_RamTst_AlgorithmFunctionPtr-2,y]
 649                         ; 479     {_asm ("LDS _RamTst_SP_2");} /* MODIFIED */
 652  0188 ff0000            	lds	_RamTst_SP_2
 654                         ; 482     if(RAMTST_RESULT_NOT_OK == RamTst_TestResult[PRESENT_BLOCK_ID]) 
 656  018b fd0000            	ldy	_RamTst_BlockIDForAlgo
 657  018e e6ea0011          	ldab	_RamTst_TestResult,y
 658  0192 c102              	cmpb	#2
 659  0194 2615              	bne	L302
 660                         ; 484         if( NULL_PTR != RamTst_Config.RamTst_ErrorNotification_Ptr)
 662  0196 fc0005            	ldd	_RamTst_Config+5
 663  0199 2704              	beq	L502
 664                         ; 487             RamTst_Config.RamTst_ErrorNotification_Ptr();
 666  019b 15fbfe66          	jsr	[_RamTst_Config+5]
 668  019f                   L502:
 669                         ; 490         RamTst_PresentAddress = END_ADDRESS_OF_THE_PRESENT_BLOCK;
 671  019f 180400020008      	movw	_RamTst_BlockEndAddressForAlgo+2,_RamTst_PresentAddress+2
 672  01a5 180400000006      	movw	_RamTst_BlockEndAddressForAlgo,_RamTst_PresentAddress
 673  01ab                   L302:
 674                         ; 494     if(RamTst_PresentAddress >= END_ADDRESS_OF_THE_PRESENT_BLOCK)
 676  01ab fc0008            	ldd	_RamTst_PresentAddress+2
 677  01ae bc0002            	cpd	_RamTst_BlockEndAddressForAlgo+2
 678  01b1 fe0006            	ldx	_RamTst_PresentAddress
 679  01b4 18be0000          	cpex	_RamTst_BlockEndAddressForAlgo
 680  01b8 255e              	blo	L702
 681                         ; 497         if(RamTst_PresentBlock == NUMBER_OF_BLOCKS_FOR_PRESENT_ALGO -\
 681                         ; 498           (RamTst_BlockIDType)1)
 683  01ba f60001            	ldab	_RamTst_PresentAlgorithm
 684  01bd 8607              	ldaa	#7
 685  01bf 12                	mul	
 686  01c0 b746              	tfr	d,y
 687  01c2 eeea0003          	ldx	_RamTst_Config+3,y
 688  01c6 09                	dex	
 689  01c7 be000c            	cpx	_RamTst_PresentBlock
 690  01ca 2625              	bne	L112
 691                         ; 501             if(RAMTST_RESULT_NOT_TESTED == RamTst_TestResult[PRESENT_BLOCK_ID])
 693  01cc fd0000            	ldy	_RamTst_BlockIDForAlgo
 694  01cf e6ea0011          	ldab	_RamTst_TestResult,y
 695  01d3 2606              	bne	L312
 696                         ; 503                 RamTst_TestResult[PRESENT_BLOCK_ID] = RAMTST_RESULT_OK;
 698  01d5 c601              	ldab	#1
 699  01d7 6bea0011          	stab	_RamTst_TestResult,y
 700  01db                   L312:
 701                         ; 506             RamTst_PresentBlock = PUT_NEW_BLOCK;
 703  01db 1879000c          	clrw	_RamTst_PresentBlock
 704                         ; 508             RamTst_PresentAddress = PUT_NEW_ADDRESS;
 706  01df cc0001            	ldd	#1
 707  01e2 7c0008            	std	_RamTst_PresentAddress+2
 708  01e5 c7                	clrb	
 709  01e6 7c0006            	std	_RamTst_PresentAddress
 710                         ; 510             RamTst_PresentAlgorithm = RamTst_NewAlgorithm;
 712  01e9 180c00100001      	movb	_RamTst_NewAlgorithm,_RamTst_PresentAlgorithm
 714  01ef 201d              	bra	L512
 715  01f1                   L112:
 716                         ; 515             if(RAMTST_RESULT_NOT_TESTED == RamTst_TestResult[PRESENT_BLOCK_ID])
 718  01f1 fd0000            	ldy	_RamTst_BlockIDForAlgo
 719  01f4 e6ea0011          	ldab	_RamTst_TestResult,y
 720  01f8 2606              	bne	L712
 721                         ; 517                 RamTst_TestResult[PRESENT_BLOCK_ID] = RAMTST_RESULT_OK;
 723  01fa c601              	ldab	#1
 724  01fc 6bea0011          	stab	_RamTst_TestResult,y
 725  0200                   L712:
 726                         ; 520             ++RamTst_PresentBlock;
 728  0200 1872000c          	incw	_RamTst_PresentBlock
 729                         ; 522             RamTst_PresentAddress = PUT_NEW_ADDRESS;
 731  0204 cc0001            	ldd	#1
 732  0207 7c0008            	std	_RamTst_PresentAddress+2
 733  020a c7                	clrb	
 734  020b 7c0006            	std	_RamTst_PresentAddress
 735  020e                   L512:
 736                         ; 524         if( NULL_PTR != RamTst_Config.RamTst_CompleteNotification_Ptr)
 738  020e fc0003            	ldd	_RamTst_Config+3
 739  0211 270f              	beq	L04
 740                         ; 527             RamTst_Config.RamTst_CompleteNotification_Ptr();
 742  0213 15fbfdec          	jsr	[_RamTst_Config+3]
 745  0217 3d                	rts	
 746  0218                   L702:
 747                         ; 533         ++RamTst_PresentAddress;
 749  0218 18720008          	incw	_RamTst_PresentAddress+2
 750  021c 2604              	bne	L04
 751  021e 08                	inx	
 752  021f 7e0006            	stx	_RamTst_PresentAddress
 753  0222                   L04:
 754                         ; 535     return;
 757  0222 3d                	rts	
 759                         .safe_ram:	section		.bss
 760  0000                   _RamTst_SP_2:
 761  0000 0000              	ds.b	2
 762  0002                   _RamTst_SP_1:
 763  0002 0000              	ds.b	2
 764  0004                   _RamTst_TestedCellCounter:
 765  0004 0000              	ds.b	2
 766  0006                   _RamTst_PresentAddress:
 767  0006 00000000          	ds.b	4
 768  000a                   _RamTst_PresentNumberOfTestedCell:
 769  000a 0000              	ds.b	2
 770  000c                   _RamTst_PresentBlock:
 771  000c 0000              	ds.b	2
 772  000e                   _RamTst_NumberOfNewTestedCells:
 773  000e 0000              	ds.b	2
 774  0010                   _RamTst_NewAlgorithm:
 775  0010 00                	ds.b	1
 776  0011                   _RamTst_TestResult:
 777  0011 00                	ds.b	1
 778  0012                   _RamTst_Data:
 779  0012 0000              	ds.b	2
 987                         	xdef	_DataSaveLocationCheck
 988                         	xdef	_RamTst_SP_2
 989                         	xdef	_RamTst_SP_1
 990                         	xref	_CheckerboardTest
 991                         	xref	_RamTst_DataSave2
 992                         	xref	_RamTst_DataSave1
 993                         	xref	_RamTst_Result
 994                         	xdef	_RamTst_TestedCellCounter
 995                         	xdef	_RamTst_PresentAddress
 996                         	xdef	_RamTst_PresentNumberOfTestedCell
 997                         	xref	_RamTst_BlockEndAddressForAlgo
 998                         	xref	_RamTst_BlockStartAddressForAlgo
 999                         	xdef	_RamTst_MainFunction
1000                         	xdef	_RamTst_GetVersionInfo
1001                         	xdef	_RamTst_GetTestResult
1002                         	xdef	_RamTst_Init
1003                         	xdef	_RamTst_PresentBlock
1004                         	xdef	_RamTst_NumberOfNewTestedCells
1005                         	xdef	_RamTst_PresentAlgorithm
1006                         	xdef	_RamTst_NewAlgorithm
1007                         	xref	_RamTst_BlockIDForAlgo
1008                         	xdef	_RamTst_TestResult
1009                         	xdef	_RamTst_Data
1010                         	xdef	_RamTst_ExecutionStatus
1011                         	xref	_RamTst_Config
1031                         	end
