   1                         ; C Compiler for S12XE [COSMIC Software].
   2                         ; Generator V4.7.8 - 27 Jul 2007
   3                         ; Optimizer V4.6.13 - 27 Jul 2007
   4                         	switch	.data
   5  0000                   _ITBM_TMR_EXEC_PERIOD:
   6  0000 000a              	dc.w	10
 199                         ; 98 void MEM_FAR ITBM_tmrmPowerupInit(void)
 199                         ; 99 {
 200                         .ftext:	section	.text
 201  0000                   f_ITBM_tmrmPowerupInit:
 203  0000 37                	pshb	
 204       00000001          OFST:	set	1
 207                         ; 103     for(byindex=TIMER_FIRST_10MS; byindex<NUM_EVENT_TIMERS; byindex++)
 209  0001 c7                	clrb	
 210  0002 6b80              	stab	OFST-1,s
 211  0004                   L301:
 212                         ; 105         aTimerValues[byindex] = TIMER_DISABLED;
 214  0004 cd0002            	ldy	#_aTimerValues
 215  0007 87                	clra	
 216  0008 59                	lsld	
 217  0009 59                	lsld	
 218  000a 19ee              	leay	d,y
 219  000c ccffff            	ldd	#-1
 220  000f 6c42              	std	2,y
 221  0011 6c40              	std	0,y
 222                         ; 103     for(byindex=TIMER_FIRST_10MS; byindex<NUM_EVENT_TIMERS; byindex++)
 224  0013 6280              	inc	OFST-1,s
 227  0015 e680              	ldab	OFST-1,s
 228  0017 c112              	cmpb	#18
 229  0019 25e9              	blo	L301
 230                         ; 108     wCurrTime = 0;
 232  001b 18790000          	clrw	_wCurrTime
 233                         ; 114 }
 236  001f 1b81              	leas	1,s
 237  0021 0a                	rtc	
 260                         ; 123 void MEM_FAR ITBM_tmrmMain(void)
 260                         ; 124 {
 261                         	switch	.ftext
 262  0022                   f_ITBM_tmrmMain:
 266                         ; 138     wCurrTime++;
 268  0022 18720000          	incw	_wCurrTime
 269                         ; 140     if(wCurrTime >= ITBM_TMR_MAX_TIMER_TIME)
 271  0026 fc0000            	ldd	_wCurrTime
 272  0029 8c03e8            	cpd	#1000
 273  002c 2504              	blo	L121
 274                         ; 142         wCurrTime = 0;
 276  002e 18790000          	clrw	_wCurrTime
 277  0032                   L121:
 278                         ; 163 }
 281  0032 0a                	rtc	
 316                         ; 173 static void TMR_DecTimer(TimerID Tindex)
 316                         ; 174 {
 317                         	switch	.ftext
 318  0033                   L3f_TMR_DecTimer:
 320  0033 3b                	pshd	
 321       00000000          OFST:	set	0
 324                         ; 175     if(IS_TIMER_RUNNING(Tindex))
 326  0034 87                	clra	
 327  0035 59                	lsld	
 328  0036 59                	lsld	
 329  0037 b746              	tfr	d,y
 330  0039 ecea0002          	ldd	_aTimerValues,y
 331  003d 2606              	bne	LC001
 332  003f ecea0004          	ldd	_aTimerValues+2,y
 333  0043 271e              	beq	L141
 334  0045                   LC001:
 336  0045 cd0002            	ldy	#_aTimerValues
 337  0048 e681              	ldab	OFST+1,s
 338  004a 87                	clra	
 339  004b 59                	lsld	
 340  004c 59                	lsld	
 341  004d 19ee              	leay	d,y
 342  004f ec40              	ldd	0,y
 343  0051 04a405            	ibne	d,LC002
 344  0054 ec42              	ldd	2,y
 345  0056 04840a            	ibeq	d,L141
 346  0059                   LC002:
 347                         ; 177         aTimerValues[Tindex]--;
 349  0059 ec42              	ldd	2,y
 350  005b 2603              	bne	L61
 351  005d 186340            	decw	0,y
 352  0060                   L61:
 353  0060 186342            	decw	2,y
 354  0063                   L141:
 355                         ; 180 }
 358  0063 31                	puly	
 359  0064 0a                	rtc	
 422                         ; 189 ITBM_BOOLEAN MEM_FAR ITBM_tmrmSetTimer(TimerID Tindex, Timer32 byCounts)
 422                         ; 190 {
 423                         	switch	.ftext
 424  0065                   f_ITBM_tmrmSetTimer:
 426  0065 3b                	pshd	
 427       00000000          OFST:	set	0
 430                         ; 192     if(byCounts > TIMER_MAX_VALUE)
 432  0066 ec87              	ldd	OFST+7,s
 433  0068 ee85              	ldx	OFST+5,s
 434  006a 8cffff            	cpd	#-1
 435  006d 188e7fff          	cpex	#32767
 436  0071 2303              	bls	L371
 437                         ; 194         return ITBM_FALSE;
 439  0073 c7                	clrb	
 442  0074 31                	puly	
 443  0075 0a                	rtc	
 444  0076                   L371:
 445                         ; 197     SET_TIMER(Tindex, byCounts);
 447  0076 cd0002            	ldy	#_aTimerValues
 448  0079 e681              	ldab	OFST+1,s
 449  007b 87                	clra	
 450  007c 59                	lsld	
 451  007d 59                	lsld	
 452  007e 19ee              	leay	d,y
 453  0080 18028742          	movw	OFST+7,s,2,y
 454  0084 6e40              	stx	0,y
 455                         ; 199     return ITBM_TRUE;
 457  0086 c601              	ldab	#1
 460  0088 31                	puly	
 461  0089 0a                	rtc	
 497                         ; 209 void MEM_FAR ITBM_tmrmDisableTimer(TimerID Tindex)
 497                         ; 210 {
 498                         	switch	.ftext
 499  008a                   f_ITBM_tmrmDisableTimer:
 503                         ; 211     SET_TIMER(Tindex, TIMER_DISABLED);
 505  008a cd0002            	ldy	#_aTimerValues
 506  008d 87                	clra	
 507  008e 59                	lsld	
 508  008f 59                	lsld	
 509  0090 19ee              	leay	d,y
 510  0092 ccffff            	ldd	#-1
 511  0095 6c42              	std	2,y
 512  0097 6c40              	std	0,y
 513                         ; 212 }
 516  0099 0a                	rtc	
 552                         ; 222 static UINT8 ITBM_tmrmGetTimerValues(TimerID Tindex)
 552                         ; 223 {
 553                         	switch	.ftext
 554  009a                   L312f_ITBM_tmrmGetTimerValues:
 558                         ; 224     return GET_TIMER_VAL(Tindex);
 560  009a 87                	clra	
 561  009b 59                	lsld	
 562  009c 59                	lsld	
 563  009d b746              	tfr	d,y
 564  009f e6ea0005          	ldab	_aTimerValues+3,y
 567  00a3 0a                	rtc	
 603                         ; 235 ITBM_BOOLEAN MEM_FAR ITBM_tmrmExpired(TimerID Tindex)
 603                         ; 236 {
 604                         	switch	.ftext
 605  00a4                   f_ITBM_tmrmExpired:
 609                         ; 237     return IS_TIMER_EXPIRED(Tindex);
 611  00a4 87                	clra	
 612  00a5 59                	lsld	
 613  00a6 59                	lsld	
 614  00a7 b746              	tfr	d,y
 615  00a9 ecea0002          	ldd	_aTimerValues,y
 616  00ad 2609              	bne	L03
 617  00af ecea0004          	ldd	_aTimerValues+2,y
 618  00b3 2603              	bne	L03
 619  00b5 c601              	ldab	#1
 621  00b7 0a                	rtc	
 622  00b8                   L03:
 623  00b8 c7                	clrb	
 626  00b9 0a                	rtc	
 663                         ; 247 ITBM_BOOLEAN MEM_FAR ITBM_tmrmIsDisabled(TimerID Tindex)
 663                         ; 248 {
 664                         	switch	.ftext
 665  00ba                   f_ITBM_tmrmIsDisabled:
 669                         ; 249     return IS_TIMER_DISABLED(Tindex);
 671  00ba cd0002            	ldy	#_aTimerValues
 672  00bd 87                	clra	
 673  00be 59                	lsld	
 674  00bf 59                	lsld	
 675  00c0 19ee              	leay	d,y
 676  00c2 ec40              	ldd	0,y
 677  00c4 04a40a            	ibne	d,L04
 678  00c7 ec42              	ldd	2,y
 679  00c9 8cffff            	cpd	#-1
 680  00cc 2603              	bne	L04
 681  00ce c601              	ldab	#1
 683  00d0 0a                	rtc	
 684  00d1                   L04:
 685  00d1 c7                	clrb	
 688  00d2 0a                	rtc	
 724                         ; 259 ITBM_BOOLEAN MEM_FAR ITBM_tmrmIsRunning(TimerID Tindex)
 724                         ; 260 {
 725                         	switch	.ftext
 726  00d3                   f_ITBM_tmrmIsRunning:
 728  00d3 3b                	pshd	
 729       00000000          OFST:	set	0
 732                         ; 261     return IS_TIMER_RUNNING(Tindex);
 734  00d4 87                	clra	
 735  00d5 59                	lsld	
 736  00d6 59                	lsld	
 737  00d7 b746              	tfr	d,y
 738  00d9 ecea0002          	ldd	_aTimerValues,y
 739  00dd 2606              	bne	LC003
 740  00df ecea0004          	ldd	_aTimerValues+2,y
 741  00e3 2718              	beq	L05
 742  00e5                   LC003:
 743  00e5 cd0002            	ldy	#_aTimerValues
 744  00e8 e681              	ldab	OFST+1,s
 745  00ea 87                	clra	
 746  00eb 59                	lsld	
 747  00ec 59                	lsld	
 748  00ed 19ee              	leay	d,y
 749  00ef ec40              	ldd	0,y
 750  00f1 04a405            	ibne	d,LC004
 751  00f4 ec42              	ldd	2,y
 752  00f6 048404            	ibeq	d,L05
 753  00f9                   LC004:
 754  00f9 c601              	ldab	#1
 755  00fb 2001              	bra	L65
 756  00fd                   L05:
 757  00fd c7                	clrb	
 758  00fe                   L65:
 761  00fe 31                	puly	
 762  00ff 0a                	rtc	
 798                         ; 271 Timer32 MEM_FAR ITBM_tmrmGetRemaining(TimerID Tindex)
 798                         ; 272 {
 799                         	switch	.ftext
 800  0100                   f_ITBM_tmrmGetRemaining:
 804                         ; 273     return GET_TIMER_VAL(Tindex);
 806  0100 cd0002            	ldy	#_aTimerValues
 807  0103 87                	clra	
 808  0104 59                	lsld	
 809  0105 59                	lsld	
 810  0106 19ee              	leay	d,y
 811  0108 ec42              	ldd	2,y
 812  010a ee40              	ldx	0,y
 815  010c 0a                	rtc	
 854                         	switch	.bss
 855  0000                   _wCurrTime:
 856  0000 0000              	ds.b	2
 857                         	xdef	_wCurrTime
 858  0002                   _aTimerValues:
 859  0002 0000000000000000  	ds.b	72
 860                         	xdef	_aTimerValues
 861                         	xdef	_ITBM_TMR_EXEC_PERIOD
 862                         	xdef	f_ITBM_tmrmGetRemaining
 863                         	xdef	f_ITBM_tmrmIsRunning
 864                         	xdef	f_ITBM_tmrmIsDisabled
 865                         	xdef	f_ITBM_tmrmExpired
 866                         	xdef	f_ITBM_tmrmDisableTimer
 867                         	xdef	f_ITBM_tmrmSetTimer
 868                         	xdef	f_ITBM_tmrmMain
 869                         	xdef	f_ITBM_tmrmPowerupInit
 890                         	end
