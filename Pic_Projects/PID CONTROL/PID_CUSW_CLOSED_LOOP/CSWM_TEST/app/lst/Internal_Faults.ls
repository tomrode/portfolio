   1                         ; C Compiler for S12XE [COSMIC Software].
   2                         ; Generator V4.7.8 - 27 Jul 2007
   3                         ; Optimizer V4.6.13 - 27 Jul 2007
  46                         ; 93 @far void IntFlt_F_Init(void)
  46                         ; 94 {
  47                         .ftext:	section	.text
  48  0000                   f_IntFlt_F_Init:
  52                         ; 96 	int_flt_byte0.cb = 0;    
  54  0000 87                	clra	
  55  0001 7a000c            	staa	_int_flt_byte0
  56                         ; 97 	int_flt_byte1.cb = 0;
  58  0004 c7                	clrb	
  59  0005 7b000b            	stab	_int_flt_byte1
  60                         ; 98 	int_flt_byte2.cb = 0;
  62  0008 7b000a            	stab	_int_flt_byte2
  63                         ; 99 	int_flt_byte3.cb = 0;
  65  000b 7a0009            	staa	_int_flt_byte3
  66                         ; 101 	intFlt_bytes[ZERO] = 0;
  68                         ; 102 	intFlt_bytes[ONE] = 0;
  70  000e 7c0005            	std	_intFlt_bytes
  71                         ; 103 	intFlt_bytes[TWO] = 0;
  73                         ; 104 	intFlt_bytes[THREE] = 0;
  75  0011 7c0007            	std	_intFlt_bytes+2
  76                         ; 107 	intFlt_self_clck_mode = INTFLT_NORMAL_CLK_MODE;
  78  0014 790003            	clr	_intFlt_self_clck_mode
  79                         ; 108 	PLL_status = MCU_PLL_LOCKED;
  81  0017 52                	incb	
  82  0018 7b0002            	stab	_PLL_status
  83                         ; 111 	intFlt_PLL_unlock_cnt_c = 0;
  85  001b 790001            	clr	_intFlt_PLL_unlock_cnt_c
  86                         ; 114 	EE_BlockRead(EE_INTERNAL_FLT_CFG, (u_8Bit*)&intFlt_config_c);
  88  001e cc0004            	ldd	#_intFlt_config_c
  89  0021 3b                	pshd	
  90  0022 cc001c            	ldd	#28
  91  0025 4a000000          	call	f_EE_BlockRead
  93                         ; 118 	EE_BlockRead(EE_PLL_FAULT_TIME_LMT, (u_8Bit*)&intFlt_PLL_err_tm_lmt_c);
  95  0029 cc0000            	ldd	#_intFlt_PLL_err_tm_lmt_c
  96  002c 6c80              	std	0,s
  97  002e cc000f            	ldd	#15
  98  0031 4a000000          	call	f_EE_BlockRead
 100  0035 1b82              	leas	2,s
 101                         ; 119 }
 104  0037 0a                	rtc	
 129                         ; 132 @far void IntFlt_F_Read_Fault_Bytes(void)
 129                         ; 133 {
 130                         	switch	.ftext
 131  0038                   f_IntFlt_F_Read_Fault_Bytes:
 135                         ; 135 	EE_BlockRead(EE_INTERNAL_FLT_BYTE0, (u_8Bit*)&intFlt_bytes[ZERO]);
 137  0038 cc0005            	ldd	#_intFlt_bytes
 138  003b 3b                	pshd	
 139  003c cc0018            	ldd	#24
 140  003f 4a000000          	call	f_EE_BlockRead
 142                         ; 136 	EE_BlockRead(EE_INTERNAL_FLT_BYTE1, (u_8Bit*)&intFlt_bytes[ONE]);
 144  0043 cc0006            	ldd	#_intFlt_bytes+1
 145  0046 6c80              	std	0,s
 146  0048 cc0019            	ldd	#25
 147  004b 4a000000          	call	f_EE_BlockRead
 149                         ; 137 	EE_BlockRead(EE_INTERNAL_FLT_BYTE2, (u_8Bit*)&intFlt_bytes[TWO]);
 151  004f cc0007            	ldd	#_intFlt_bytes+2
 152  0052 6c80              	std	0,s
 153  0054 cc001a            	ldd	#26
 154  0057 4a000000          	call	f_EE_BlockRead
 156                         ; 138 	EE_BlockRead(EE_INTERNAL_FLT_BYTE3, (u_8Bit*)&intFlt_bytes[THREE]);	
 158  005b cc0008            	ldd	#_intFlt_bytes+3
 159  005e 6c80              	std	0,s
 160  0060 cc001b            	ldd	#27
 161  0063 4a000000          	call	f_EE_BlockRead
 163  0067 1b82              	leas	2,s
 164                         ; 139 }
 167  0069 0a                	rtc	
 193                         ; 159 @far void IntFlt_F_Chck_Unused_Bits(void)
 193                         ; 160 {
 194                         	switch	.ftext
 195  006a                   f_IntFlt_F_Chck_Unused_Bits:
 199                         ; 162 	IntFlt_F_Read_Fault_Bytes();	
 201  006a 4a003838          	call	f_IntFlt_F_Read_Fault_Bytes
 203                         ; 165 	if ( (intFlt_bytes[ZERO] | BYTE0_USED_BITS) == BYTE0_USED_BITS) {
 205  006e f60005            	ldab	_intFlt_bytes
 206  0071 ca1b              	orab	#27
 207  0073 c11b              	cmpb	#27
 208  0075 270e              	beq	L34
 210                         ; 170 		IntFlt_F_Update(ZERO, BYTE0_USED_BITS, INT_FLT_CLR);
 212  0077 cc0001            	ldd	#1
 213  007a 3b                	pshd	
 214  007b c61b              	ldab	#27
 215  007d 3b                	pshd	
 216  007e c7                	clrb	
 217  007f 4a015858          	call	f_IntFlt_F_Update
 219  0083 1b84              	leas	4,s
 220  0085                   L34:
 221                         ; 174 	if ( (intFlt_bytes[ONE] | BYTE1_USED_BITS) == BYTE1_USED_BITS) {
 223  0085 f60006            	ldab	_intFlt_bytes+1
 224  0088 cacf              	orab	#207
 225  008a c1cf              	cmpb	#207
 226  008c 270f              	beq	L74
 228                         ; 179 		IntFlt_F_Update(ONE, BYTE1_USED_BITS, INT_FLT_CLR);
 230  008e cc0001            	ldd	#1
 231  0091 3b                	pshd	
 232  0092 c6cf              	ldab	#207
 233  0094 3b                	pshd	
 234  0095 c601              	ldab	#1
 235  0097 4a015858          	call	f_IntFlt_F_Update
 237  009b 1b84              	leas	4,s
 238  009d                   L74:
 239                         ; 183 	if ( (intFlt_bytes[TWO] | BYTE2_USED_BITS) == BYTE2_USED_BITS) {
 241  009d f60007            	ldab	_intFlt_bytes+2
 242  00a0 ca04              	orab	#4
 243  00a2 c104              	cmpb	#4
 244  00a4 270f              	beq	L35
 246                         ; 188 		IntFlt_F_Update(TWO, BYTE2_USED_BITS, INT_FLT_CLR);
 248  00a6 cc0001            	ldd	#1
 249  00a9 3b                	pshd	
 250  00aa c604              	ldab	#4
 251  00ac 3b                	pshd	
 252  00ad c602              	ldab	#2
 253  00af 4a015858          	call	f_IntFlt_F_Update
 255  00b3 1b84              	leas	4,s
 256  00b5                   L35:
 257                         ; 192 	if ( (intFlt_bytes[THREE] | BYTE3_USED_BITS) == BYTE3_USED_BITS) {
 259  00b5 f60008            	ldab	_intFlt_bytes+3
 260  00b8 ca01              	orab	#1
 261  00ba 04010d            	dbeq	b,L75
 263                         ; 197 		IntFlt_F_Update(THREE, BYTE3_USED_BITS, INT_FLT_CLR);
 265  00bd cc0001            	ldd	#1
 266  00c0 3b                	pshd	
 267  00c1 3b                	pshd	
 268  00c2 c603              	ldab	#3
 269  00c4 4a015858          	call	f_IntFlt_F_Update
 271  00c8 1b84              	leas	4,s
 272  00ca                   L75:
 273                         ; 199 }
 276  00ca 0a                	rtc	
 314                         ; 217 @far void IntFlt_F_Chck_CAN_Comm(void)
 314                         ; 218 {
 315                         	switch	.ftext
 316  00cb                   f_IntFlt_F_Chck_CAN_Comm:
 318  00cb 37                	pshb	
 319       00000001          OFST:	set	1
 322                         ; 220 	u_8Bit CAN_Stdy_pin_stat = 0;
 324  00cc 6980              	clr	OFST-1,s
 325                         ; 225 	CAN_Stdy_pin_stat = (u_8Bit) Dio_ReadChannel(CAN_STB);
 327  00ce e6fbff2e          	ldab	[_Dio_PortRead_Ptr]
 328  00d2 c408              	andb	#8
 329  00d4 54                	lsrb	
 330  00d5 54                	lsrb	
 331  00d6 54                	lsrb	
 332  00d7 6b80              	stab	OFST-1,s
 333                         ; 232 	if ( (CAN_Stdy_pin_stat == STD_HIGH) && (intFlt_bytes[ZERO] == NO_INT_FLT) && (intFlt_self_clck_mode == INTFLT_NORMAL_CLK_MODE) ) {
 335  00d9 042114            	dbne	b,L77
 337  00dc f60005            	ldab	_intFlt_bytes
 338  00df 260f              	bne	L77
 340  00e1 f70003            	tst	_intFlt_self_clck_mode
 341  00e4 260a              	bne	L77
 342                         ; 235 		Dio_WriteChannel(CAN_STB, STD_LOW);
 345  00e6 1410              	sei	
 350  00e8 fe0000            	ldx	_Dio_PortWrite_Ptr
 351  00eb 0d0008            	bclr	0,x,8
 355  00ee 10ef              	cli	
 359  00f0                   L77:
 360                         ; 240 }
 363  00f0 1b81              	leas	1,s
 364  00f2 0a                	rtc	
 400                         ; 258 @far void IntFlt_F_DTC_Monitor(void)
 400                         ; 259 {
 401                         	switch	.ftext
 402  00f3                   f_IntFlt_F_DTC_Monitor:
 404  00f3 37                	pshb	
 405       00000001          OFST:	set	1
 408                         ; 261 	u_8Bit intFlt_active = FALSE;    // internal fault active indicator
 410  00f4 6980              	clr	OFST-1,s
 411                         ; 264 	IntFlt_F_Chck_Unused_Bits();
 413  00f6 4a006a6a          	call	f_IntFlt_F_Chck_Unused_Bits
 415                         ; 268 	if ( (intFlt_bytes[ZERO] == NO_INT_FLT) && (intFlt_bytes[ONE] == NO_INT_FLT) && (intFlt_bytes[TWO] == NO_INT_FLT) ) {
 417  00fa f60005            	ldab	_intFlt_bytes
 418  00fd 260a              	bne	L511
 420  00ff f60006            	ldab	_intFlt_bytes+1
 421  0102 2605              	bne	L511
 423  0104 f60007            	ldab	_intFlt_bytes+2
 424                         ; 270 		intFlt_active = FALSE;
 427  0107 2702              	beq	L711
 428  0109                   L511:
 429                         ; 274 		intFlt_active = TRUE;
 431  0109 c601              	ldab	#1
 432  010b                   L711:
 433  010b 6b80              	stab	OFST-1,s
 434                         ; 279 	if (intFlt_active == TRUE) {
 436  010d 04210c            	dbne	b,L121
 437                         ; 281 		FMemLibF_ReportDtc(DTC_HARDWARE_ERROR_K, ERROR_ON_K);
 439  0110 cc0001            	ldd	#1
 440  0113 3b                	pshd	
 441  0114 ce00a2            	ldx	#162
 442  0117 cc1a00            	ldd	#6656
 445  011a 2008              	bra	L321
 446  011c                   L121:
 447                         ; 285 		FMemLibF_ReportDtc(DTC_HARDWARE_ERROR_K, ERROR_OFF_K);
 449  011c 87                	clra	
 450  011d c7                	clrb	
 451  011e 3b                	pshd	
 452  011f ce00a2            	ldx	#162
 453  0122 861a              	ldaa	#26
 455  0124                   L321:
 456  0124 4a000000          	call	f_FMemLibF_ReportDtc
 457  0128 1b82              	leas	2,s
 458                         ; 289 	IntFlt_F_Chck_CAN_Comm();
 460  012a 4a00cbcb          	call	f_IntFlt_F_Chck_CAN_Comm
 462                         ; 290 }
 465  012e 1b81              	leas	1,s
 466  0130 0a                	rtc	
1137                         ; 304 @far EEBlockLocation IntFltLF_Get_Location(u_8Bit sel_byte)
1137                         ; 305 {
1138                         	switch	.ftext
1139  0131                   f_IntFltLF_Get_Location:
1141  0131 3b                	pshd	
1142  0132 3b                	pshd	
1143       00000002          OFST:	set	2
1146                         ; 307 	u_8Bit fault_byte = sel_byte;
1148  0133 6b81              	stab	OFST-1,s
1149                         ; 308 	EEBlockLocation my_location = (EEBlockLocation)0;
1151  0135 6980              	clr	OFST-2,s
1152                         ; 311 	switch(fault_byte)
1155  0137 04410d            	tbeq	b,L521
1156  013a 04010e            	dbeq	b,L721
1157  013d 04010f            	dbeq	b,L131
1158  0140 040110            	dbeq	b,L331
1159  0143 e680              	ldab	OFST-2,s
1160  0145 200e              	bra	LC001
1161  0147                   L521:
1162                         ; 313 		case ZERO:
1162                         ; 314 			my_location = EE_INTERNAL_FLT_BYTE0;
1164  0147 c618              	ldab	#24
1165                         ; 315 			break;
1167  0149 200a              	bra	LC001
1168  014b                   L721:
1169                         ; 316 		case ONE:
1169                         ; 317 			my_location = EE_INTERNAL_FLT_BYTE1;
1171  014b c619              	ldab	#25
1172                         ; 318 			break;
1174  014d 2006              	bra	LC001
1175  014f                   L131:
1176                         ; 319 		case TWO:
1176                         ; 320 			my_location = EE_INTERNAL_FLT_BYTE2;
1178  014f c61a              	ldab	#26
1179                         ; 321 			break;
1181  0151 2002              	bra	LC001
1182  0153                   L331:
1183                         ; 322 		case THREE:
1183                         ; 323 			my_location = EE_INTERNAL_FLT_BYTE3;
1185  0153 c61b              	ldab	#27
1186  0155                   LC001:
1187                         ; 324 			break;
1189                         ; 325 		default: 
1189                         ; 326 			break;
1191                         ; 330 	return(my_location);
1195  0155 1b84              	leas	4,s
1196  0157 0a                	rtc	
1299                         ; 381 @far void IntFlt_F_Update(u_8Bit Byte_number, u_8Bit Bit_mask, Int_Flt_Op_Type_e intFlt_Op)
1299                         ; 382 {	
1300                         	switch	.ftext
1301  0158                   f_IntFlt_F_Update:
1303  0158 3b                	pshd	
1304  0159 1b9d              	leas	-3,s
1305       00000003          OFST:	set	3
1308                         ; 384 	u_8Bit intFlt_byte = Byte_number;    // Byte number
1310  015b 6b80              	stab	OFST-3,s
1311                         ; 385 	u_8Bit intFlt_mask = Bit_mask;       // Bit mask
1313  015d 180a8982          	movb	OFST+6,s,OFST-1,s
1314                         ; 386 	u_8Bit intFlt_temp = 0;              // stores the selected internal byte
1316  0161 6981              	clr	OFST-2,s
1317                         ; 387 	EEBlockLocation flt_location = (EEBlockLocation) 0;	 // Internal Fault location in EE Block
1319                         ; 390 	if (intFlt_byte <= THREE) {
1321  0163 c103              	cmpb	#3
1322  0165 2233              	bhi	L115
1323                         ; 393 		IntFlt_F_Read_Fault_Bytes();
1325  0167 4a003838          	call	f_IntFlt_F_Read_Fault_Bytes
1327                         ; 398 		intFlt_temp = intFlt_bytes[intFlt_byte];
1329  016b e680              	ldab	OFST-3,s
1330  016d b796              	exg	b,y
1331  016f 180aea000581      	movb	_intFlt_bytes,y,OFST-2,s
1332                         ; 401 		if (intFlt_Op == INT_FLT_SET) {
1334  0175 e68b              	ldab	OFST+8,s
1335  0177 2606              	bne	L105
1336                         ; 403 			intFlt_temp |= intFlt_mask;
1338  0179 e682              	ldab	OFST-1,s
1339  017b ea81              	orab	OFST-2,s
1341  017d 2004              	bra	L305
1342  017f                   L105:
1343                         ; 407 			intFlt_temp &= intFlt_mask;
1345  017f e682              	ldab	OFST-1,s
1346  0181 e481              	andb	OFST-2,s
1347  0183                   L305:
1348  0183 6b81              	stab	OFST-2,s
1349                         ; 411 		flt_location = IntFltLF_Get_Location(intFlt_byte);
1351  0185 e680              	ldab	OFST-3,s
1352  0187 87                	clra	
1353  0188 4a013131          	call	f_IntFltLF_Get_Location
1355  018c 6b80              	stab	OFST-3,s
1356                         ; 414 		if(flt_location != FALSE){
1358  018e 270a              	beq	L115
1359                         ; 416 			EE_BlockWrite(flt_location, &intFlt_temp);
1361  0190 1a81              	leax	OFST-2,s
1362  0192 34                	pshx	
1363  0193 87                	clra	
1364  0194 4a000000          	call	f_EE_BlockWrite
1366  0198 1b82              	leas	2,s
1368  019a                   L115:
1369                         ; 424 }
1372  019a 1b85              	leas	5,s
1373  019c 0a                	rtc	
1404                         ; 450 @far void IntFlt_F_ClearOutputs(void)
1404                         ; 451 {
1405                         	switch	.ftext
1406  019d                   f_IntFlt_F_ClearOutputs:
1410                         ; 454 	if ((CSWM_config_c & CSWM_2VS_MASK_K) == CSWM_2VS_MASK_K)
1412  019d 1e00003002        	brset	_CSWM_config_c,48,LC002
1413  01a2 201e              	bra	L525
1414  01a4                   LC002:
1415                         ; 457 		vsF_Clear( (u_8Bit) VS_FL);
1417  01a4 87                	clra	
1418  01a5 c7                	clrb	
1419  01a6 4a000000          	call	f_vsF_Clear
1421                         ; 458 		vsF_LED_Display ( (u_8Bit) VS_FL);
1423  01aa 87                	clra	
1424  01ab c7                	clrb	
1425  01ac 4a000000          	call	f_vsF_LED_Display
1427                         ; 459 		vsF_Clear( (u_8Bit) VS_FR);
1429  01b0 cc0001            	ldd	#1
1430  01b3 4a000000          	call	f_vsF_Clear
1432                         ; 460 		vsF_LED_Display ( (u_8Bit) VS_FR);
1434  01b7 cc0001            	ldd	#1
1435  01ba 4a000000          	call	f_vsF_LED_Display
1437                         ; 461 		vsF_Output_Cntrl();
1439  01be 4a000000          	call	f_vsF_Output_Cntrl
1442  01c2                   L525:
1443                         ; 469 	if ((CSWM_config_c & CSWM_HSW_MASK_K) == CSWM_HSW_MASK_K)
1445  01c2 1f0000400c        	brclr	_CSWM_config_c,64,L135
1446                         ; 472 		stWF_Clear();
1448  01c7 4a000000          	call	f_stWF_Clear
1450                         ; 473 		stWF_LED_Display();
1452  01cb 4a000000          	call	f_stWF_LED_Display
1454                         ; 474 		stWF_Output_Control();
1456  01cf 4a000000          	call	f_stWF_Output_Control
1459  01d3                   L135:
1460                         ; 482 	hsLF_ClearHeatSequence();
1462  01d3 4a000000          	call	f_hsLF_ClearHeatSequence
1464                         ; 483 }
1467  01d7 0a                	rtc	
1494                         ; 502 @far void IntFlt_F_Clr_PLL_Err(void)
1494                         ; 503 {
1495                         	switch	.ftext
1496  01d8                   f_IntFlt_F_Clr_PLL_Err:
1500                         ; 505 	EE_BlockRead(EE_INTERNAL_FLT_BYTE0, (u_8Bit*)&intFlt_bytes[ZERO]);
1502  01d8 cc0005            	ldd	#_intFlt_bytes
1503  01db 3b                	pshd	
1504  01dc cc0018            	ldd	#24
1505  01df 4a000000          	call	f_EE_BlockRead
1507  01e3 1b82              	leas	2,s
1508                         ; 508 	if ( (intFlt_bytes[ZERO] & PLL_ERR_MASK) == PLL_ERR_MASK) {
1510  01e5 1f0005020e        	brclr	_intFlt_bytes,2,L545
1511                         ; 510 		IntFlt_F_Update(ZERO, CLR_PLL_ERR, INT_FLT_CLR);
1513  01ea cc0001            	ldd	#1
1514  01ed 3b                	pshd	
1515  01ee c6fd              	ldab	#253
1516  01f0 3b                	pshd	
1517  01f1 c7                	clrb	
1518  01f2 4a015858          	call	f_IntFlt_F_Update
1520  01f6 1b84              	leas	4,s
1522  01f8                   L545:
1523                         ; 516 	PLL_Flt_b = FALSE;
1525  01f8 1d000c02          	bclr	_int_flt_byte0,2
1526                         ; 517 }
1529  01fc 0a                	rtc	
1531                         	switch	.data
1532  0000                   L745_PLL_CAN_disable_cnt:
1533  0000 00                	dc.b	0
1578                         ; 539 @far void IntFlt_F_Chck_PLL(void)
1578                         ; 540 {	
1579                         	switch	.ftext
1580  01fd                   f_IntFlt_F_Chck_PLL:
1584                         ; 545 	if (intFlt_config_c == ENABLE_HIGH_PRIORITY_INTFLT_DET) {
1586  01fd f60004            	ldab	_intFlt_config_c
1587  0200 c155              	cmpb	#85
1588  0202 266e              	bne	L316
1589                         ; 548 		PLL_status = Mcu_GetPllStatus();
1591  0204 1f00000804        	brclr	__CRGFLG,8,L03
1592  0209 c601              	ldab	#1
1593  020b 2001              	bra	L23
1594  020d                   L03:
1595  020d c7                	clrb	
1596  020e                   L23:
1597  020e 7b0002            	stab	_PLL_status
1598                         ; 551 		if (PLL_status == MCU_PLL_LOCKED) {
1600  0211 04210b            	dbne	b,L765
1601                         ; 553 			intFlt_PLL_unlock_cnt_c = 0;
1603  0214 790001            	clr	_intFlt_PLL_unlock_cnt_c
1604                         ; 556 			PLL_CAN_disable_cnt = 0;
1606  0217 790000            	clr	L745_PLL_CAN_disable_cnt
1607                         ; 559 			IntFlt_F_Clr_PLL_Err();
1609  021a 4a01d8d8          	call	f_IntFlt_F_Clr_PLL_Err
1613  021e 0a                	rtc	
1614  021f                   L765:
1615                         ; 563 			if ( (intFlt_PLL_unlock_cnt_c < intFlt_PLL_err_tm_lmt_c) && (PLL_Flt_b == FALSE) ) {
1617  021f f60001            	ldab	_intFlt_PLL_unlock_cnt_c
1618  0222 f10000            	cmpb	_intFlt_PLL_err_tm_lmt_c
1619  0225 2409              	bhs	L375
1621  0227 1e000c0204        	brset	_int_flt_byte0,2,L375
1622                         ; 566 				intFlt_PLL_unlock_cnt_c++;
1624  022c 720001            	inc	_intFlt_PLL_unlock_cnt_c
1627  022f 0a                	rtc	
1628  0230                   L375:
1629                         ; 570 				if (PLL_Flt_b == TRUE) {
1631  0230 1e000c0208        	brset	_int_flt_byte0,2,L106
1633                         ; 575 					IntFlt_F_ClearOutputs();
1635  0235 4a019d9d          	call	f_IntFlt_F_ClearOutputs
1637                         ; 578 					PLL_Flt_b = TRUE;
1639  0239 1c000c02          	bset	_int_flt_byte0,2
1640  023d                   L106:
1641                         ; 581 				EE_BlockRead(EE_INTERNAL_FLT_BYTE0, (u_8Bit*)&intFlt_bytes[ZERO]);
1643  023d cc0005            	ldd	#_intFlt_bytes
1644  0240 3b                	pshd	
1645  0241 cc0018            	ldd	#24
1646  0244 4a000000          	call	f_EE_BlockRead
1648  0248 1b82              	leas	2,s
1649                         ; 584 				if ( (intFlt_bytes[ZERO] & PLL_ERR_MASK) == PLL_ERR_MASK) {
1651  024a 1f00050216        	brclr	_intFlt_bytes,2,L306
1652                         ; 587 					if (PLL_CAN_disable_cnt < INTFLT_CAN_DISABLE_TM) {
1654  024f f60000            	ldab	L745_PLL_CAN_disable_cnt
1655  0252 c114              	cmpb	#20
1656  0254 2404              	bhs	L506
1657                         ; 589 						PLL_CAN_disable_cnt++;
1659  0256 720000            	inc	L745_PLL_CAN_disable_cnt
1662  0259 0a                	rtc	
1663  025a                   L506:
1664                         ; 593 						Dio_WriteChannel(CAN_STB, STD_HIGH);
1667  025a 1410              	sei	
1672  025c fd0000            	ldy	_Dio_PortWrite_Ptr
1673  025f 0c4008            	bset	0,y,8
1677  0262 10ef              	cli	
1681  0264 0a                	rtc	
1682  0265                   L306:
1683                         ; 598 					IntFlt_F_Update(ZERO, PLL_ERR_MASK, INT_FLT_SET);
1685  0265 87                	clra	
1686  0266 c7                	clrb	
1687  0267 3b                	pshd	
1688  0268 c602              	ldab	#2
1689  026a 3b                	pshd	
1690  026b c7                	clrb	
1691  026c 4a015858          	call	f_IntFlt_F_Update
1693  0270 1b84              	leas	4,s
1694  0272                   L316:
1695                         ; 606 }
1698  0272 0a                	rtc	
1722                         ; 623 @far void IntFlt_F_Set_Osci_Err(void)
1722                         ; 624 {	
1723                         	switch	.ftext
1724  0273                   f_IntFlt_F_Set_Osci_Err:
1728                         ; 631 	if (intFlt_self_clck_mode == INTFLT_NORMAL_CLK_MODE) {
1730  0273 f70003            	tst	_intFlt_self_clck_mode
1731  0276 2606              	bne	L526
1732                         ; 633 		intFlt_self_clck_mode = INTFLT_SELF_CLK_MODE;
1734  0278 c6aa              	ldab	#170
1735  027a 7b0003            	stab	_intFlt_self_clck_mode
1738  027d 0a                	rtc	
1739  027e                   L526:
1740                         ; 637 		intFlt_self_clck_mode = INTFLT_NORMAL_CLK_MODE;
1742  027e 790003            	clr	_intFlt_self_clck_mode
1743                         ; 640 }
1746  0281 0a                	rtc	
1774                         ; 657 @far void IntFlt_F_Clr_Osci_Err(void)
1774                         ; 658 {	
1775                         	switch	.ftext
1776  0282                   f_IntFlt_F_Clr_Osci_Err:
1780                         ; 660 	if (intFlt_self_clck_mode == INTFLT_NORMAL_CLK_MODE) {
1782  0282 f70003            	tst	_intFlt_self_clck_mode
1783  0285 2624              	bne	L746
1784                         ; 663 		EE_BlockRead(EE_INTERNAL_FLT_BYTE0, (u_8Bit*)&intFlt_bytes[ZERO]);
1786  0287 cc0005            	ldd	#_intFlt_bytes
1787  028a 3b                	pshd	
1788  028b cc0018            	ldd	#24
1789  028e 4a000000          	call	f_EE_BlockRead
1791  0292 1b82              	leas	2,s
1792                         ; 666 		if ( (intFlt_bytes[ZERO] & CRYSTAL_OSCILLATOR_MASK) == CRYSTAL_OSCILLATOR_MASK) {
1794  0294 1f0005010e        	brclr	_intFlt_bytes,1,L546
1795                         ; 668 			IntFlt_F_Update(ZERO, CLR_OSC_ERR, INT_FLT_CLR);
1797  0299 cc0001            	ldd	#1
1798  029c 3b                	pshd	
1799  029d c6fe              	ldab	#254
1800  029f 3b                	pshd	
1801  02a0 c7                	clrb	
1802  02a1 4a015858          	call	f_IntFlt_F_Update
1804  02a5 1b84              	leas	4,s
1806  02a7                   L546:
1807                         ; 674 		Crystal_Osci_Flt_b = FALSE;
1809  02a7 1d000c01          	bclr	_int_flt_byte0,1
1811  02ab                   L746:
1812                         ; 679 }
1815  02ab 0a                	rtc	
1849                         ; 705 @far void IntFlt_F_Chck_Osci_Err(void)
1849                         ; 706 {	
1850                         	switch	.ftext
1851  02ac                   f_IntFlt_F_Chck_Osci_Err:
1855                         ; 708 	if (intFlt_config_c == ENABLE_HIGH_PRIORITY_INTFLT_DET) {
1857  02ac f60004            	ldab	_intFlt_config_c
1858  02af c155              	cmpb	#85
1859  02b1 2641              	bne	L776
1860                         ; 710 		if (intFlt_self_clck_mode == INTFLT_SELF_CLK_MODE) {
1862  02b3 f60003            	ldab	_intFlt_self_clck_mode
1863  02b6 c1aa              	cmpb	#170
1864  02b8 2636              	bne	L366
1865                         ; 712 			Dio_WriteChannel(CAN_STB, STD_HIGH);
1868  02ba 1410              	sei	
1873  02bc fe0000            	ldx	_Dio_PortWrite_Ptr
1874  02bf 0c0008            	bset	0,x,8
1878  02c2 10ef              	cli	
1880                         ; 715 			if (Crystal_Osci_Flt_b == TRUE) {
1883  02c4 1e000c0108        	brset	_int_flt_byte0,1,L766
1885                         ; 720 				IntFlt_F_ClearOutputs();
1887  02c9 4a019d9d          	call	f_IntFlt_F_ClearOutputs
1889                         ; 723 				Crystal_Osci_Flt_b = TRUE;
1891  02cd 1c000c01          	bset	_int_flt_byte0,1
1892  02d1                   L766:
1893                         ; 726 			EE_BlockRead(EE_INTERNAL_FLT_BYTE0, (u_8Bit*)&intFlt_bytes[ZERO]);
1895  02d1 cc0005            	ldd	#_intFlt_bytes
1896  02d4 3b                	pshd	
1897  02d5 cc0018            	ldd	#24
1898  02d8 4a000000          	call	f_EE_BlockRead
1900  02dc 1b82              	leas	2,s
1901                         ; 729 			if ( (intFlt_bytes[ZERO] & CRYSTAL_OSCILLATOR_MASK) == CRYSTAL_OSCILLATOR_MASK) {
1903  02de 1e00050111        	brset	_intFlt_bytes,1,L776
1905                         ; 734 				IntFlt_F_Update(ZERO, CRYSTAL_OSCILLATOR_MASK, INT_FLT_SET);
1907  02e3 87                	clra	
1908  02e4 c7                	clrb	
1909  02e5 3b                	pshd	
1910  02e6 52                	incb	
1911  02e7 3b                	pshd	
1912  02e8 c7                	clrb	
1913  02e9 4a015858          	call	f_IntFlt_F_Update
1915  02ed 1b84              	leas	4,s
1917  02ef 0a                	rtc	
1918  02f0                   L366:
1919                         ; 739 			IntFlt_F_Clr_Osci_Err();
1921  02f0 4a028282          	call	f_IntFlt_F_Clr_Osci_Err
1923  02f4                   L776:
1924                         ; 745 }
1927  02f4 0a                	rtc	
1955                         ; 760 @far void IntFlt_F_Clr_Ram_Err(void)
1955                         ; 761 {
1956                         	switch	.ftext
1957  02f5                   f_IntFlt_F_Clr_Ram_Err:
1961                         ; 763 	EE_BlockRead(EE_INTERNAL_FLT_BYTE0, (u_8Bit*)&intFlt_bytes[ZERO]);
1963  02f5 cc0005            	ldd	#_intFlt_bytes
1964  02f8 3b                	pshd	
1965  02f9 cc0018            	ldd	#24
1966  02fc 4a000000          	call	f_EE_BlockRead
1968  0300 1b82              	leas	2,s
1969                         ; 766 	if ( ( (intFlt_bytes[ZERO] & RAM_ERR_MASK) == RAM_ERR_MASK) && (main_RAM_test_rslt == RAMTST_RESULT_OK) ){
1971  0302 1f00051018        	brclr	_intFlt_bytes,16,L317
1973  0307 f60000            	ldab	_main_RAM_test_rslt
1974  030a 042112            	dbne	b,L317
1975                         ; 768 		IntFlt_F_Update(ZERO, CLR_RAM_ERR, INT_FLT_CLR);
1977  030d cc0001            	ldd	#1
1978  0310 3b                	pshd	
1979  0311 c6ef              	ldab	#239
1980  0313 3b                	pshd	
1981  0314 c7                	clrb	
1982  0315 4a015858          	call	f_IntFlt_F_Update
1984  0319 1b84              	leas	4,s
1985                         ; 771 		Ram_err_b = FALSE;
1987  031b 1d000c10          	bclr	_int_flt_byte0,16
1989  031f                   L317:
1990                         ; 776 }
1993  031f 0a                	rtc	
1995                         	switch	.data
1996  0001                   L517_Ram_CAN_disable_cnt:
1997  0001 00                	dc.b	0
2040                         ; 798 @far void IntFlt_F_Chck_Ram_Err(void)
2040                         ; 799 {	
2041                         	switch	.ftext
2042  0320                   f_IntFlt_F_Chck_Ram_Err:
2046                         ; 804 	if (intFlt_config_c == ENABLE_HIGH_PRIORITY_INTFLT_DET) {
2048  0320 f60004            	ldab	_intFlt_config_c
2049  0323 c155              	cmpb	#85
2050  0325 2654              	bne	L337
2051                         ; 807 		if (main_RAM_test_rslt == RAMTST_RESULT_OK) {
2053  0327 f60000            	ldab	_main_RAM_test_rslt
2054  032a 04210a            	dbne	b,L537
2055                         ; 809 			Ram_CAN_disable_cnt = 0;			
2057  032d 7b0001            	stab	L517_Ram_CAN_disable_cnt
2058                         ; 811 			Ram_err_b = FALSE;			
2060  0330 1d000c10          	bclr	_int_flt_byte0,16
2061                         ; 813 			mainF_Finish_Mcu_Test(MAIN_RAM_TEST);
2063  0334 87                	clra	
2066  0335 2046              	bra	LC003
2067  0337                   L537:
2068                         ; 817 			if (Ram_err_b == TRUE) {
2070  0337 1e000c1008        	brset	_int_flt_byte0,16,L347
2072                         ; 822 				IntFlt_F_ClearOutputs();				
2074  033c 4a019d9d          	call	f_IntFlt_F_ClearOutputs
2076                         ; 824 				Ram_err_b = TRUE;
2078  0340 1c000c10          	bset	_int_flt_byte0,16
2079  0344                   L347:
2080                         ; 827 			EE_BlockRead(EE_INTERNAL_FLT_BYTE0, (u_8Bit*)&intFlt_bytes[ZERO]);
2082  0344 cc0005            	ldd	#_intFlt_bytes
2083  0347 3b                	pshd	
2084  0348 cc0018            	ldd	#24
2085  034b 4a000000          	call	f_EE_BlockRead
2087  034f 1b82              	leas	2,s
2088                         ; 830 			if ( (intFlt_bytes[ZERO] & RAM_ERR_MASK) == RAM_ERR_MASK) {
2090  0351 1f00051017        	brclr	_intFlt_bytes,16,L547
2091                         ; 833 				if (Ram_CAN_disable_cnt < INTFLT_CAN_DISABLE_TM) {
2093  0356 f60001            	ldab	L517_Ram_CAN_disable_cnt
2094  0359 c114              	cmpb	#20
2095  035b 2404              	bhs	L747
2096                         ; 835 					Ram_CAN_disable_cnt++;
2098  035d 720001            	inc	L517_Ram_CAN_disable_cnt
2101  0360 0a                	rtc	
2102  0361                   L747:
2103                         ; 839 					Dio_WriteChannel(CAN_STB, STD_HIGH);					
2106  0361 1410              	sei	
2111  0363 fe0000            	ldx	_Dio_PortWrite_Ptr
2112  0366 0c0008            	bset	0,x,8
2116  0369 10ef              	cli	
2118                         ; 841 					mainF_Finish_Mcu_Test(MAIN_RAM_TEST);
2122  036b 200e              	bra	L337
2123  036d                   L547:
2124                         ; 846 				IntFlt_F_Update(ZERO, RAM_ERR_MASK, INT_FLT_SET);
2126  036d 87                	clra	
2127  036e c7                	clrb	
2128  036f 3b                	pshd	
2129  0370 c610              	ldab	#16
2130  0372 3b                	pshd	
2131  0373 c7                	clrb	
2132  0374 4a015858          	call	f_IntFlt_F_Update
2134  0378 1b84              	leas	4,s
2136  037a 0a                	rtc	
2137  037b                   L337:
2138                         ; 852 		mainF_Finish_Mcu_Test(MAIN_RAM_TEST);
2140  037b 87                	clra	
2141  037c c7                	clrb	
2142  037d                   LC003:
2143  037d 160000            	jsr	_mainF_Finish_Mcu_Test
2145                         ; 854 }
2148  0380 0a                	rtc	
2386                         	xdef	f_IntFlt_F_Clr_Osci_Err
2387                         	xdef	f_IntFlt_F_Clr_PLL_Err
2388                         	xdef	f_IntFltLF_Get_Location
2389                         	xdef	f_IntFlt_F_Chck_CAN_Comm
2390                         	xdef	f_IntFlt_F_Chck_Unused_Bits
2391                         	xdef	f_IntFlt_F_Read_Fault_Bytes
2392                         	switch	.bss
2393  0000                   _intFlt_PLL_err_tm_lmt_c:
2394  0000 00                	ds.b	1
2395                         	xdef	_intFlt_PLL_err_tm_lmt_c
2396  0001                   _intFlt_PLL_unlock_cnt_c:
2397  0001 00                	ds.b	1
2398                         	xdef	_intFlt_PLL_unlock_cnt_c
2399  0002                   _PLL_status:
2400  0002 00                	ds.b	1
2401                         	xdef	_PLL_status
2402  0003                   _intFlt_self_clck_mode:
2403  0003 00                	ds.b	1
2404                         	xdef	_intFlt_self_clck_mode
2405                         	xref	f_hsLF_ClearHeatSequence
2406                         	xref	_CSWM_config_c
2407                         	xref	f_stWF_LED_Display
2408                         	xref	f_stWF_Clear
2409                         	xref	f_stWF_Output_Control
2410                         	xref	f_vsF_LED_Display
2411                         	xref	f_vsF_Clear
2412                         	xref	f_vsF_Output_Cntrl
2413                         	xref	_Dio_PortRead_Ptr
2414                         	xref	_Dio_PortWrite_Ptr
2415                         	xref	__CRGFLG
2416                         	xref	f_FMemLibF_ReportDtc
2417                         	xref	_mainF_Finish_Mcu_Test
2418                         	xref	_main_RAM_test_rslt
2419                         	xdef	f_IntFlt_F_Set_Osci_Err
2420                         	xdef	f_IntFlt_F_Chck_Osci_Err
2421                         	xdef	f_IntFlt_F_Clr_Ram_Err
2422                         	xdef	f_IntFlt_F_ClearOutputs
2423                         	xdef	f_IntFlt_F_Chck_Ram_Err
2424                         	xdef	f_IntFlt_F_Chck_PLL
2425                         	xdef	f_IntFlt_F_Update
2426                         	xdef	f_IntFlt_F_DTC_Monitor
2427                         	xdef	f_IntFlt_F_Init
2428  0004                   _intFlt_config_c:
2429  0004 00                	ds.b	1
2430                         	xdef	_intFlt_config_c
2431  0005                   _intFlt_bytes:
2432  0005 00000000          	ds.b	4
2433                         	xdef	_intFlt_bytes
2434  0009                   _int_flt_byte3:
2435  0009 00                	ds.b	1
2436                         	xdef	_int_flt_byte3
2437  000a                   _int_flt_byte2:
2438  000a 00                	ds.b	1
2439                         	xdef	_int_flt_byte2
2440  000b                   _int_flt_byte1:
2441  000b 00                	ds.b	1
2442                         	xdef	_int_flt_byte1
2443  000c                   _int_flt_byte0:
2444  000c 00                	ds.b	1
2445                         	xdef	_int_flt_byte0
2446                         	xref	f_EE_BlockRead
2447                         	xref	f_EE_BlockWrite
2468                         	end
