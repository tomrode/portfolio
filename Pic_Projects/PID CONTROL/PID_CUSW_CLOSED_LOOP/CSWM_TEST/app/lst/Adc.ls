   1                         ; C Compiler for S12XE [COSMIC Software].
   2                         ; Generator V4.7.8 - 27 Jul 2007
   3                         ; Optimizer V4.6.13 - 27 Jul 2007
  49                         ; 289 void Init_Variables(void)
  49                         ; 290 {
  50                         	switch	.text
  51  0000                   _Init_Variables:
  53  0000 37                	pshb	
  54       00000001          OFST:	set	1
  57                         ; 293     for (Temp=0;Temp<NO_OF_HW_UNITS;Temp++)
  59  0001 6980              	clr	OFST-1,s
  60  0003                   L54:
  61                         ; 296   		Adc_Starting_Channel[Temp]= FALSE;
  63  0003 e680              	ldab	OFST-1,s
  64  0005 b796              	exg	b,y
  65  0007 c7                	clrb	
  66  0008 6bea0013          	stab	L11_Adc_Starting_Channel,y
  67                         ; 298   		Adc_Seq_Length[Temp]= FALSE;
  69  000c 6bea0012          	stab	L31_Adc_Seq_Length,y
  70                         ; 301   		Prev_Group=FALSE;
  72  0010 7b001c            	stab	L5_Prev_Group
  73                         ; 293     for (Temp=0;Temp<NO_OF_HW_UNITS;Temp++)
  75  0013 6280              	inc	OFST-1,s
  78  0015 27ec              	beq	L54
  79                         ; 307     for (Temp=0;Temp<ADC_CONFIGURED_GROUPS;Temp++)
  81  0017 6b80              	stab	OFST-1,s
  82  0019                   L35:
  83                         ; 310         Adc_Conversion_StartFlag[Temp] = ADC_CONV_NOT_STARTED;
  85  0019 b796              	exg	b,y
  86  001b 69ea0014          	clr	L7_Adc_Conversion_StartFlag,y
  87                         ; 307     for (Temp=0;Temp<ADC_CONFIGURED_GROUPS;Temp++)
  89  001f 6280              	inc	OFST-1,s
  92  0021 e680              	ldab	OFST-1,s
  93  0023 c108              	cmpb	#8
  94  0025 25f2              	blo	L35
  95                         ; 314     for (Temp=0;Temp<ADC_NO_OF_CONVCOMPLETE_REG;Temp++)
  97  0027 6980              	clr	OFST-1,s
  98  0029                   L16:
  99                         ; 316         Adc_Converted_Channels[Temp]  = FALSE;
 101  0029 e680              	ldab	OFST-1,s
 102  002b b796              	exg	b,y
 103  002d c7                	clrb	
 104  002e 6bea0011          	stab	L51_Adc_Converted_Channels,y
 105                         ; 314     for (Temp=0;Temp<ADC_NO_OF_CONVCOMPLETE_REG;Temp++)
 107  0032 6280              	inc	OFST-1,s
 110  0034 27f3              	beq	L16
 111                         ; 320     for(Temp=0;Temp<ADC_MAX_NO_OF_CHANNELS;Temp++)
 113  0036 6b80              	stab	OFST-1,s
 114  0038                   L76:
 115                         ; 322         Adc_Converted_Value[Temp]= OUT_OF_RANGE;
 117  0038 87                	clra	
 118  0039 59                	lsld	
 119  003a b746              	tfr	d,y
 120  003c ccffff            	ldd	#-1
 121  003f 6cea0000          	std	L12_Adc_Converted_Value,y
 122                         ; 320     for(Temp=0;Temp<ADC_MAX_NO_OF_CHANNELS;Temp++)
 124  0043 6280              	inc	OFST-1,s
 127  0045 e680              	ldab	OFST-1,s
 128  0047 c108              	cmpb	#8
 129  0049 25ed              	blo	L76
 130                         ; 325 }
 133  004b 1b81              	leas	1,s
 134  004d 3d                	rts	
 160                         ; 350 void Init_HW0(void)
 160                         ; 351 {
 161                         	switch	.text
 162  004e                   _Init_HW0:
 164  004e 3b                	pshd	
 165       00000002          OFST:	set	2
 168                         ; 354     ATD0CTL0 = ADC_0_CTL0_DEFAULT_VALUE;
 170  004f c607              	ldab	#7
 171  0051 7b0000            	stab	__ATD0CTL01
 172                         ; 357     ATD0CTL1 = ADC_0_CTL1_DEFAULT_VALUE;
 174  0054 c627              	ldab	#39
 175  0056 7b0001            	stab	__ATD0CTL01+1
 176                         ; 362     ATD0CTL3 = ADC_0_CTL3_DEFAULT_VALUE |(uint8)Adc_UserConfig->
 176                         ; 363                                                    Adc_Freeze_Mode[ADC_0];
 178  0059 fd001d            	ldy	L3_Adc_UserConfig
 179  005c e641              	ldab	1,y
 180  005e ca20              	orab	#32
 181  0060 7b0001            	stab	__ATD0CTL23+1
 182                         ; 366     ATD0CTL4 = (uint8)Adc_UserConfig->ADC_PRESCALE[ADC_0];
 184  0063 cd0000            	ldy	#__ATD0CTL45
 185  0066 fe001d            	ldx	L3_Adc_UserConfig
 186  0069 180a0040          	movb	0,x,0,y
 187                         ; 370     ATD0CTL2 = (uint8) (ADC_0_CTL2_DEFAULT_VALUE | ADC_Set_RegisterBit(\
 187                         ; 371          ATD0CTL2, Adc_UserConfig->Adc_StopConv[ADC_0],ATD0CTL2_ICLKSTP) \
 187                         ; 372          | ATD0CTL2_ASCIE_MASK);
 189  006d fd001d            	ldy	L3_Adc_UserConfig
 190  0070 e642              	ldab	2,y
 191  0072 270f              	beq	L01
 192  0074 f60000            	ldab	__ATD0CTL23
 193  0077 55                	rolb	
 194  0078 55                	rolb	
 195  0079 55                	rolb	
 196  007a 55                	rolb	
 197  007b c401              	andb	#1
 198  007d fa0000            	orab	__ATD0CTL23
 199  0080 87                	clra	
 200  0081 2018              	bra	L21
 201  0083                   L01:
 202  0083 f60000            	ldab	__ATD0CTL23
 203  0086 87                	clra	
 204  0087 6c80              	std	OFST-2,s
 205  0089 f60000            	ldab	__ATD0CTL23
 206  008c 55                	rolb	
 207  008d 55                	rolb	
 208  008e 55                	rolb	
 209  008f 55                	rolb	
 210  0090 c401              	andb	#1
 211  0092 b746              	tfr	d,y
 212  0094 1851              	comy	
 213  0096 18e480            	andy	OFST-2,s
 214  0099 b764              	tfr	y,d
 215  009b                   L21:
 216  009b ca02              	orab	#2
 217  009d 7b0000            	stab	__ATD0CTL23
 218                         ; 375 }
 221  00a0 31                	puly	
 222  00a1 3d                	rts	
 644                         ; 407 void Adc_Init(const Adc_ConfigType *ConfigPtr)
 644                         ; 408 {
 645                         	switch	.text
 646  00a2                   _Adc_Init:
 648  00a2 3b                	pshd	
 649       00000000          OFST:	set	0
 652                         ; 414     Init_Variables();
 654  00a3 160000            	jsr	_Init_Variables
 656                         ; 417     Adc_UserConfig = ConfigPtr;
 658  00a6 180580001d        	movw	OFST+0,s,L3_Adc_UserConfig
 659                         ; 421     Init_HW0();
 661  00ab 07a1              	jsr	_Init_HW0
 663                         ; 426 }
 666  00ad 31                	puly	
 667  00ae 3d                	rts	
 738                         ; 441 void Adc_StartGroupConversion(Adc_GroupType group)
 738                         ; 442 {
 739                         	switch	.text
 740  00af                   _Adc_StartGroupConversion:
 742  00af 3b                	pshd	
 743  00b0 1b9c              	leas	-4,s
 744       00000004          OFST:	set	4
 747                         ; 446     uint8 conversion_mode=0;
 749                         ; 465             ATD0CTL0 =(uint8) Adc_UserConfig->Adc_GrpCfg[group].
 749                         ; 466                             ADC_GROUP_DEFINITION.Adc_Wrap_around;
 751  00b2 cd0000            	ldy	#__ATD0CTL01
 752  00b5 fe001d            	ldx	L3_Adc_UserConfig
 753  00b8 ee03              	ldx	3,x
 754  00ba e685              	ldab	OFST+1,s
 755  00bc 8609              	ldaa	#9
 756  00be 12                	mul	
 757  00bf 1ae6              	leax	d,x
 758  00c1 180a0740          	movb	7,x,0,y
 759                         ; 469             Adc_Seq_Length[ADC_0] = Adc_UserConfig->Adc_GrpCfg[group] \
 759                         ; 470                                               .ADC_GROUP_DEFINITION.Adc_TotalCh;
 761  00c5 fd001d            	ldy	L3_Adc_UserConfig
 762  00c8 ee43              	ldx	3,y
 763  00ca e685              	ldab	OFST+1,s
 764  00cc 8609              	ldaa	#9
 765  00ce 12                	mul	
 766  00cf 1ae6              	leax	d,x
 767  00d1 180d030012        	movb	3,x,L31_Adc_Seq_Length
 768                         ; 471             ControlByte = ATD0CTL3;
 770  00d6 f60001            	ldab	__ATD0CTL23+1
 771  00d9 c487              	andb	#135
 772  00db 6b81              	stab	OFST-3,s
 773                         ; 474             ControlByte = (uint8)(ControlByte & (~ADC_ATDCTL3_SC_MASK));
 775                         ; 476             ControlByte =(uint8)(ControlByte |ATD0CTL3_DJM_MASK \
 775                         ; 477                            |ADC_Set_RegisterMultipleBits \
 775                         ; 478                      (ControlByte,Adc_Seq_Length[ADC_0],ATD0CTL3_S1C_MASK));
 777  00dd f60012            	ldab	L31_Adc_Seq_Length
 778  00e0 58                	lslb	
 779  00e1 58                	lslb	
 780  00e2 58                	lslb	
 781  00e3 ea81              	orab	OFST-3,s
 782  00e5 6b80              	stab	OFST-4,s
 783  00e7 e681              	ldab	OFST-3,s
 784  00e9 ca80              	orab	#128
 785  00eb ea80              	orab	OFST-4,s
 786                         ; 480             ATD0CTL3 =  ControlByte;
 788  00ed 7b0001            	stab	__ATD0CTL23+1
 789                         ; 484             ControlByte = ATD0CTL1;
 791  00f0 f60001            	ldab	__ATD0CTL01+1
 792  00f3 c49f              	andb	#159
 793  00f5 6b81              	stab	OFST-3,s
 794                         ; 487             ControlByte = ControlByte & (~ATD0CTL1_SRES_MASK);
 796                         ; 489             ControlByte =(uint8)(ControlByte |ADC_Set_RegisterMultipleBits(\
 796                         ; 490                             ControlByte, Adc_UserConfig->Adc_GrpCfg[group].\
 796                         ; 491                             ADC_GROUP_DEFINITION.ADC_CHANNEL_RESOLUTION,\
 796                         ; 492                             ATD0CTL1_SRES0_MASK));
 798  00f7 e605              	ldab	5,x
 799  00f9 58                	lslb	
 800  00fa 58                	lslb	
 801  00fb 58                	lslb	
 802  00fc 58                	lslb	
 803  00fd 58                	lslb	
 804  00fe ea81              	orab	OFST-3,s
 805  0100 6b81              	stab	OFST-3,s
 806                         ; 495             ControlByte |=ADC_Set_RegisterBit(ControlByte,
 806                         ; 496                             Adc_UserConfig->Adc_GrpCfg[group].ADC_GROUP_DEFINITION.\
 806                         ; 497                             Adc_Discharge,ATD0CTL1_SMP_DIS_MASK);
 808  0102 e708              	tst	8,x
 809  0104 2704              	beq	L02
 810  0106 ca10              	orab	#16
 811  0108 2002              	bra	L22
 812  010a                   L02:
 813  010a c4ef              	andb	#239
 814  010c                   L22:
 815  010c ea81              	orab	OFST-3,s
 816                         ; 498             ATD0CTL1 = ControlByte;
 818  010e 7b0001            	stab	__ATD0CTL01+1
 819                         ; 500             ControlByte = ATD0CTL4;
 821  0111 f60000            	ldab	__ATD0CTL45
 822  0114 c41f              	andb	#31
 823  0116 6b81              	stab	OFST-3,s
 824                         ; 501                         ControlByte = ControlByte &(~ATD0CTL4_SMP_MASK);
 826                         ; 503             ControlByte = ControlByte |ADC_Set_RegisterMultipleBits( \
 826                         ; 504                            ControlByte,Adc_UserConfig->Adc_GrpCfg[group]. \
 826                         ; 505                            ADC_GROUP_DEFINITION.ADC_CHANNEL_CONV_TIME, \
 826                         ; 506                            ATD0CTL4_SMP0_MASK);
 828  0118 ee43              	ldx	3,y
 829  011a e685              	ldab	OFST+1,s
 830  011c 8609              	ldaa	#9
 831  011e 12                	mul	
 832  011f 1ae6              	leax	d,x
 833  0121 e606              	ldab	6,x
 834  0123 58                	lslb	
 835  0124 58                	lslb	
 836  0125 58                	lslb	
 837  0126 58                	lslb	
 838  0127 58                	lslb	
 839  0128 ea81              	orab	OFST-3,s
 840  012a 6b81              	stab	OFST-3,s
 841                         ; 508             ATD0CTL4 = ControlByte;
 843  012c 7b0000            	stab	__ATD0CTL45
 844                         ; 511             Adc_Starting_Channel[ADC_0] =(Adc_UserConfig->Adc_GrpCfg[group]
 844                         ; 512                                        .ADC_GROUP_DEFINITION.Adc_Start_Channel);
 846  012f 180d040013        	movb	4,x,L11_Adc_Starting_Channel
 847                         ; 520 		   Adc_StartedGroup[ADC_0] = group;
 849  0134 e685              	ldab	OFST+1,s
 850  0136 7b0010            	stab	L71_Adc_StartedGroup
 851                         ; 522 		   Adc_Converted_Channels[ADC_0_CHANNEL_RESULT_REGISTER_L] =MCAL_CLEAR;
 853  0139 87                	clra	
 854  013a 7a0011            	staa	L51_Adc_Converted_Channels
 855                         ; 523            Adc_Conversion_StartFlag[group] = ADC_CONV_STARTED;
 857  013d b746              	tfr	d,y
 858  013f c601              	ldab	#1
 859  0141 6bea0014          	stab	L7_Adc_Conversion_StartFlag,y
 860                         ; 525 		    ControlByte = MCAL_CLEAR;
 862  0145 6981              	clr	OFST-3,s
 863                         ; 526 			if((Adc_UserConfig->Adc_GrpCfg[group].ADC_GROUP_CONV_MODE == ADC_CONV_MODE_ON_DEMAND) ||
 863                         ; 527 									   (Adc_UserConfig->Adc_GrpCfg[group].ADC_GROUP_CONV_MODE == ADC_CONV_MODE_ONESHOT))
 865  0147 fd001d            	ldy	L3_Adc_UserConfig
 866  014a ee43              	ldx	3,y
 867  014c e685              	ldab	OFST+1,s
 868  014e 8609              	ldaa	#9
 869  0150 12                	mul	
 870  0151 1ae6              	leax	d,x
 871  0153 e602              	ldab	2,x
 872  0155 c102              	cmpb	#2
 873  0157 2703              	beq	L373
 875  0159 046129            	tbne	b,L173
 876  015c                   L373:
 877                         ; 530 			  conversion_mode = 0;
 879  015c c7                	clrb	
 881  015d                   L573:
 882  015d 6b83              	stab	OFST-1,s
 883                         ; 536 			   ControlByte =ControlByte |ADC_Set_RegisterBit(ControlByte,\
 883                         ; 537 						  conversion_mode, ATD0CTL5_SCAN_MASK)\
 883                         ; 538 						  | ATD0CTL5_MULT_MASK | Adc_Starting_Channel[ADC_0];
 885  015f 2706              	beq	L42
 886  0161 e681              	ldab	OFST-3,s
 887  0163 ca20              	orab	#32
 888  0165 2004              	bra	L62
 889  0167                   L42:
 890  0167 e681              	ldab	OFST-3,s
 891  0169 c4df              	andb	#223
 892  016b                   L62:
 893  016b ea81              	orab	OFST-3,s
 894  016d ca10              	orab	#16
 895  016f fa0013            	orab	L11_Adc_Starting_Channel
 896  0172 6b81              	stab	OFST-3,s
 897                         ; 540         channel = Adc_UserConfig->Adc_GrpCfg[group].ADC_GROUP_DEFINITION.Adc_Start_Channel; 
 899  0174 ee43              	ldx	3,y
 900  0176 e685              	ldab	OFST+1,s
 901  0178 8609              	ldaa	#9
 902  017a 12                	mul	
 903  017b 1ae6              	leax	d,x
 904  017d 180a0482          	movb	4,x,OFST-2,s
 905                         ; 544         for(index=0;
 907  0181 6983              	clr	OFST-1,s
 909  0183 202e              	bra	L304
 910  0185                   L173:
 911                         ; 534 			   conversion_mode = 1;
 913  0185 c601              	ldab	#1
 914  0187 20d4              	bra	L573
 915  0189                   L773:
 916                         ; 549             Adc_Converted_Value[channel]=OUT_OF_RANGE;
 918  0189 e682              	ldab	OFST-2,s
 919  018b 87                	clra	
 920  018c 59                	lsld	
 921  018d b746              	tfr	d,y
 922  018f ccffff            	ldd	#-1
 923  0192 6cea0000          	std	L12_Adc_Converted_Value,y
 924                         ; 551             channel = ((channel < Adc_UserConfig->Adc_GrpCfg[group].ADC_GROUP_DEFINITION.Adc_Wrap_around))?++channel:ADC_0_BEGIN_CHANNEL_INDEX;
 926  0196 fd001d            	ldy	L3_Adc_UserConfig
 927  0199 ee43              	ldx	3,y
 928  019b e685              	ldab	OFST+1,s
 929  019d 8609              	ldaa	#9
 930  019f 12                	mul	
 931  01a0 1ae6              	leax	d,x
 932  01a2 e682              	ldab	OFST-2,s
 933  01a4 e107              	cmpb	7,x
 934  01a6 2406              	bhs	L03
 935  01a8 6282              	inc	OFST-2,s
 936  01aa e682              	ldab	OFST-2,s
 937  01ac 2001              	bra	L23
 938  01ae                   L03:
 939  01ae c7                	clrb	
 940  01af                   L23:
 941  01af 6b82              	stab	OFST-2,s
 942                         ; 545             index<Adc_UserConfig->Adc_GrpCfg[group].ADC_GROUP_DEFINITION.Adc_TotalCh;
 942                         ; 546             index++)
 944  01b1 6283              	inc	OFST-1,s
 945  01b3                   L304:
 946                         ; 544         for(index=0;
 946                         ; 545             index<Adc_UserConfig->Adc_GrpCfg[group].ADC_GROUP_DEFINITION.Adc_TotalCh;
 948  01b3 ee43              	ldx	3,y
 949  01b5 e685              	ldab	OFST+1,s
 950  01b7 8609              	ldaa	#9
 951  01b9 12                	mul	
 952  01ba 1ae6              	leax	d,x
 953  01bc e683              	ldab	OFST-1,s
 954  01be e103              	cmpb	3,x
 955  01c0 25c7              	blo	L773
 956                         ; 557             if(Adc_Seq_Length[ADC_0]> 1)
 958  01c2 f60012            	ldab	L31_Adc_Seq_Length
 959  01c5 c101              	cmpb	#1
 960  01c7 2304              	bls	L704
 961                         ; 559                 ATD0CTL5 = ControlByte;
 963  01c9 e681              	ldab	OFST-3,s
 965  01cb 2004              	bra	L114
 966  01cd                   L704:
 967                         ; 563                 ATD0CTL5 = ControlByte & (~ATD0CTL5_MULT_MASK);
 969  01cd e681              	ldab	OFST-3,s
 970  01cf c4ef              	andb	#239
 971  01d1                   L114:
 972  01d1 7b0001            	stab	__ATD0CTL45+1
 973                         ; 569     Prev_Group = group;
 975  01d4 180d85001c        	movb	OFST+1,s,L5_Prev_Group
 976                         ; 572 }
 979  01d9 1b86              	leas	6,s
 980  01db 3d                	rts	
1013                         ; 589 Adc_ValueType Adc_SingleValueReadChannel(Adc_ChannelType channel)
1013                         ; 590 {
1014                         	switch	.text
1015  01dc                   _Adc_SingleValueReadChannel:
1019                         ; 596     return(Adc_Converted_Value[channel]);
1021  01dc 87                	clra	
1022  01dd 59                	lsld	
1023  01de b746              	tfr	d,y
1024  01e0 ecea0000          	ldd	L12_Adc_Converted_Value,y
1027  01e4 3d                	rts	
1106                         ; 615 Adc_StatusType Adc_GetGroupStatus (Adc_GroupType group)
1106                         ; 616 {
1107                         	switch	.text
1108  01e5                   _Adc_GetGroupStatus:
1110  01e5 3b                	pshd	
1111  01e6 37                	pshb	
1112       00000001          OFST:	set	1
1115                         ; 620     AdcStatus = ADC_IDLE;
1117  01e7 87                	clra	
1118  01e8 6a80              	staa	OFST-1,s
1119                         ; 627     if( ADC_CONV_NOT_STARTED == Adc_Conversion_StartFlag[group])
1121  01ea b746              	tfr	d,y
1122  01ec e7ea0014          	tst	L7_Adc_Conversion_StartFlag,y
1123  01f0 2604              	bne	L364
1124                         ; 629         AdcStatus = ADC_IDLE;
1126  01f2 6980              	clr	OFST-1,s
1128  01f4 203a              	bra	L564
1129  01f6                   L364:
1130                         ; 631     else if(ADC_CONV_FINISHED ==Adc_Conversion_StartFlag[group])
1132  01f6 e682              	ldab	OFST+1,s
1133  01f8 b746              	tfr	d,y
1134  01fa e6ea0014          	ldab	L7_Adc_Conversion_StartFlag,y
1135  01fe c102              	cmpb	#2
1136  0200 2604              	bne	L764
1137                         ; 635         AdcStatus = ADC_COMPLETED;
1139  0202 c603              	ldab	#3
1141  0204 2028              	bra	LC001
1142  0206                   L764:
1143                         ; 643 			  else if (Adc_UserConfig->Adc_GrpCfg[group].\
1143                         ; 644 			   ADC_GROUP_DEFINITION.Adc_Start_Channel < ADC_CHANNELS_IN_ADC_0)
1145  0206 fd001d            	ldy	L3_Adc_UserConfig
1146  0209 ee43              	ldx	3,y
1147  020b e682              	ldab	OFST+1,s
1148  020d 8609              	ldaa	#9
1149  020f 12                	mul	
1150  0210 1ae6              	leax	d,x
1151  0212 e604              	ldab	4,x
1152  0214 c108              	cmpb	#8
1153  0216 2418              	bhs	L564
1154                         ; 649 					 if(MCAL_CLEAR==\
1154                         ; 650 					  Adc_Converted_Channels[ADC_0_CHANNEL_RESULT_REGISTER_L])
1156  0218 f60011            	ldab	L51_Adc_Converted_Channels
1157  021b 2604              	bne	L574
1158                         ; 652 					     AdcStatus = ADC_BUSY_NO_VALUE;
1160  021d c601              	ldab	#1
1162  021f 200d              	bra	LC001
1163  0221                   L574:
1164                         ; 656     			     else if(ADC_CONV_STARTED == Adc_Conversion_StartFlag[group])
1166  0221 e682              	ldab	OFST+1,s
1167  0223 b796              	exg	b,y
1168  0225 e6ea0014          	ldab	L7_Adc_Conversion_StartFlag,y
1169  0229 042104            	dbne	b,L564
1170                         ; 658 					     AdcStatus = ADC_BUSY;
1172  022c c602              	ldab	#2
1173  022e                   LC001:
1174  022e 6b80              	stab	OFST-1,s
1176  0230                   L564:
1177                         ; 663 			  return AdcStatus;
1179  0230 e680              	ldab	OFST-1,s
1182  0232 1b83              	leas	3,s
1183  0234 3d                	rts	
1261                         ; 686 void Adc_GetVersionInfo(Std_VersionInfoType *versioninfo)
1261                         ; 687 {
1262                         	switch	.text
1263  0235                   _Adc_GetVersionInfo:
1265       00000000          OFST:	set	0
1268                         ; 690      if(NULL_PTR != versioninfo)
1270  0235 6cae              	std	2,-s
1271  0237 2714              	beq	L345
1272                         ; 693 	        versioninfo->moduleID=ADC_MODULE_ID;
1274  0239 c67b              	ldab	#123
1275  023b ed80              	ldy	OFST+0,s
1276  023d 6b42              	stab	2,y
1277                         ; 695 	        versioninfo->vendorID=ADC_VENDOR_ID;
1279  023f cc0028            	ldd	#40
1280  0242 6c40              	std	0,y
1281                         ; 697 	        versioninfo->sw_major_version=ADC_SW_MAJOR_VERSION_C;
1283  0244 c605              	ldab	#5
1284  0246 6b43              	stab	3,y
1285                         ; 698 	        versioninfo->sw_minor_version=ADC_SW_MINOR_VERSION_C;
1287  0248 6944              	clr	4,y
1288                         ; 699 	        versioninfo->sw_patch_version=ADC_SW_PATCH_VERSION_C;
1290  024a 52                	incb	
1291  024b 6b45              	stab	5,y
1292  024d                   L345:
1293                         ; 702 }
1296  024d 31                	puly	
1297  024e 3d                	rts	
1344                         ; 720 void Adc0_ConversionCompleteIsr(void)
1344                         ; 721 {
1345                         	switch	.text
1346  024f                   _Adc0_ConversionCompleteIsr:
1348  024f 3b                	pshd	
1349       00000002          OFST:	set	2
1352                         ; 728     Adc_Converted_Channels[ADC_0_CHANNEL_RESULT_REGISTER_L] = ATD0STAT2L;
1354  0250 f60001            	ldab	__ATD0STAT2+1
1355  0253 7b0011            	stab	L51_Adc_Converted_Channels
1356                         ; 731     ATD0STAT0_SCF  =TRUE;
1358  0256 1c000080          	bset	__ATD0STAT0,128
1359                         ; 734     if( (ADC_CONV_MODE_ONESHOT==Adc_UserConfig->Adc_GrpCfg[Adc_StartedGroup[ADC_0]].ADC_GROUP_CONV_MODE) || 
1359                         ; 735         (ADC_CONV_MODE_CONTINUOUS==Adc_UserConfig->Adc_GrpCfg[Adc_StartedGroup[ADC_0]].ADC_GROUP_CONV_MODE))
1361  025a fd001d            	ldy	L3_Adc_UserConfig
1362  025d ee43              	ldx	3,y
1363  025f f60010            	ldab	L71_Adc_StartedGroup
1364  0262 8609              	ldaa	#9
1365  0264 12                	mul	
1366  0265 1ae6              	leax	d,x
1367  0267 e602              	ldab	2,x
1368  0269 2703              	beq	L565
1370  026b 04214a            	dbne	b,L365
1371  026e                   L565:
1372                         ; 737         channel = Adc_UserConfig->Adc_GrpCfg[Adc_StartedGroup[ADC_0]].ADC_GROUP_DEFINITION.Adc_Start_Channel;
1374  026e 180a0480          	movb	4,x,OFST-2,s
1375                         ; 741         for(index=0;
1377  0272 6981              	clr	OFST-1,s
1379  0274 2032              	bra	L375
1380  0276                   L765:
1381                         ; 745             Adc_Converted_Value[channel]=(uint16)(*(&ATD0DR0+index));
1383  0276 e680              	ldab	OFST-2,s
1384  0278 87                	clra	
1385  0279 b746              	tfr	d,y
1386  027b 1858              	lsly	
1387  027d e681              	ldab	OFST-1,s
1388  027f 59                	lsld	
1389  0280 b745              	tfr	d,x
1390  0282 1802e20000ea0000  	movw	__ATD0DR0,x,L12_Adc_Converted_Value,y
1391                         ; 746             channel = ((channel < Adc_UserConfig->Adc_GrpCfg[Adc_StartedGroup[ADC_0]].ADC_GROUP_DEFINITION.Adc_Wrap_around))?++channel:ADC_0_BEGIN_CHANNEL_INDEX;
1393  028a fd001d            	ldy	L3_Adc_UserConfig
1394  028d ee43              	ldx	3,y
1395  028f f60010            	ldab	L71_Adc_StartedGroup
1396  0292 8609              	ldaa	#9
1397  0294 12                	mul	
1398  0295 1ae6              	leax	d,x
1399  0297 e680              	ldab	OFST-2,s
1400  0299 e107              	cmpb	7,x
1401  029b 2406              	bhs	L44
1402  029d 6280              	inc	OFST-2,s
1403  029f e680              	ldab	OFST-2,s
1404  02a1 2001              	bra	L64
1405  02a3                   L44:
1406  02a3 c7                	clrb	
1407  02a4                   L64:
1408  02a4 6b80              	stab	OFST-2,s
1409                         ; 742             index<Adc_UserConfig->Adc_GrpCfg[Adc_StartedGroup[ADC_0]].ADC_GROUP_DEFINITION.Adc_TotalCh;
1409                         ; 743             index++)
1411  02a6 6281              	inc	OFST-1,s
1412  02a8                   L375:
1413                         ; 741         for(index=0;
1413                         ; 742             index<Adc_UserConfig->Adc_GrpCfg[Adc_StartedGroup[ADC_0]].ADC_GROUP_DEFINITION.Adc_TotalCh;
1415  02a8 ee43              	ldx	3,y
1416  02aa f60010            	ldab	L71_Adc_StartedGroup
1417  02ad 8609              	ldaa	#9
1418  02af 12                	mul	
1419  02b0 1ae6              	leax	d,x
1420  02b2 e681              	ldab	OFST-1,s
1421  02b4 e103              	cmpb	3,x
1422  02b6 25be              	blo	L765
1423  02b8                   L365:
1424                         ; 757   	if(ADC_CONV_MODE_ONESHOT==Adc_UserConfig->Adc_GrpCfg\
1424                         ; 758   		[Adc_StartedGroup[ADC_0]].\
1424                         ; 759   		ADC_GROUP_CONV_MODE)
1426  02b8 ee43              	ldx	3,y
1427  02ba f60010            	ldab	L71_Adc_StartedGroup
1428  02bd 8609              	ldaa	#9
1429  02bf 12                	mul	
1430  02c0 1ae6              	leax	d,x
1431  02c2 e602              	ldab	2,x
1432  02c4 2612              	bne	L775
1433                         ; 761   		if(ADC_CONV_STARTED ==\
1433                         ; 762   		   Adc_Conversion_StartFlag[Adc_StartedGroup[ADC_0]])
1435  02c6 f60010            	ldab	L71_Adc_StartedGroup
1436  02c9 b796              	exg	b,y
1437  02cb e6ea0014          	ldab	L7_Adc_Conversion_StartFlag,y
1438  02cf 042106            	dbne	b,L775
1439                         ; 764   			Adc_Conversion_StartFlag[Adc_StartedGroup[ADC_0]] =\
1439                         ; 765   				ADC_CONV_FINISHED;
1441  02d2 c602              	ldab	#2
1442  02d4 6bea0014          	stab	L7_Adc_Conversion_StartFlag,y
1443  02d8                   L775:
1444                         ; 778 }//end of ADC0 ISR
1447  02d8 31                	puly	
1448  02d9 3d                	rts	
1575                         	xdef	_Init_HW0
1576                         	xdef	_Init_Variables
1577                         	switch	.bss
1578  0000                   L12_Adc_Converted_Value:
1579  0000 0000000000000000  	ds.b	16
1580  0010                   L71_Adc_StartedGroup:
1581  0010 00                	ds.b	1
1582  0011                   L51_Adc_Converted_Channels:
1583  0011 00                	ds.b	1
1584  0012                   L31_Adc_Seq_Length:
1585  0012 00                	ds.b	1
1586  0013                   L11_Adc_Starting_Channel:
1587  0013 00                	ds.b	1
1588  0014                   L7_Adc_Conversion_StartFlag:
1589  0014 0000000000000000  	ds.b	8
1590  001c                   L5_Prev_Group:
1591  001c 00                	ds.b	1
1592  001d                   L3_Adc_UserConfig:
1593  001d 0000              	ds.b	2
1594                         	xdef	_Adc0_ConversionCompleteIsr
1595                         	xdef	_Adc_GetVersionInfo
1596                         	xdef	_Adc_GetGroupStatus
1597                         	xdef	_Adc_SingleValueReadChannel
1598                         	xdef	_Adc_StartGroupConversion
1599                         	xdef	_Adc_Init
1600                         	xref	__ATD0DR0
1601                         	xref	__ATD0STAT2
1602                         	xref	__ATD0STAT0
1603                         	xref	__ATD0CTL45
1604                         	xref	__ATD0CTL23
1605                         	xref	__ATD0CTL01
1626                         	end
