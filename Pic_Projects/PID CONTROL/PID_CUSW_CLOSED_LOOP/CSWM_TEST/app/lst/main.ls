   1                         ; C Compiler for S12XE [COSMIC Software].
   2                         ; Generator V4.7.8 - 27 Jul 2007
   3                         ; Optimizer V4.6.13 - 27 Jul 2007
 309                         	switch	.bss
 310  0000                   _main_variant_select_c:
 311  0000 00                	ds.b	1
 312  0001                   _main_ecu_seat_cfg_c:
 313  0001 00                	ds.b	1
 314  0002                   _main_ecu_cfg_for_v7_c:
 315  0002 00                	ds.b	1
 316  0003                   _main_EngRpm_lowLmt_c:
 317  0003 00                	ds.b	1
 318  0004                   _main_RAM_test_rslt:
 319  0004 00                	ds.b	1
 320  0005                   _main_flag2_c:
 321  0005 00                	ds.b	1
 322  0006                   _main_flag_c:
 323  0006 00                	ds.b	1
 324  0007                   _hardware_proxi_mismatch_c:
 325  0007 00                	ds.b	1
 326  0008                   _main_SwReq_stW_c:
 327  0008 00                	ds.b	1
 328  0009                   _main_RemSt_delay_c:
 329  0009 00                	ds.b	1
 330  000a                   _main_SwReq_Rear_c:
 331  000a 00                	ds.b	1
 332  000b                   _main_SwReq_Front_c:
 333  000b 00                	ds.b	1
 334  000c                   _main_state_c:
 335  000c 00                	ds.b	1
 336  000d                   _main_RearSeat_Status_c:
 337  000d 00                	ds.b	1
 338  000e                   _main_FrontSeat_Status_c:
 339  000e 00                	ds.b	1
 340  000f                   _main_ECU_reset_rq_c:
 341  000f 00                	ds.b	1
 342  0010                   _main_CSWM_Func_State_c:
 343  0010 00                	ds.b	1
 344  0011                   _main_CSWM_Status_c:
 345  0011 00                	ds.b	1
 346  0012                   _main_prms:
 347  0012 0000              	ds.b	2
 399                         ; 228 void near mainLF_Init(void)
 399                         ; 229 {
 400                         	switch	.text
 401  0000                   _mainLF_Init:
 405                         ; 230 	main_timeSlice_counter_c.cb = 0;		// Time slice counter is cleared
 407  0000 79001d            	clr	_main_timeSlice_counter_c
 408                         ; 231 	main_timeSlice_request_c.cb = 0;		// Time slice requests are cleared
 410  0003 79001e            	clr	_main_timeSlice_request_c
 411                         ; 232 	main_InitClock_stat = 0;				// Init clock state cleared	
 413  0006 79001c            	clr	_main_InitClock_stat
 414                         ; 234 	main_prms.Flt_Mtr_Tm   = MAIN_LED_DSPLY_TM;  // Default: 2 second short-term LED Display time
 416  0009 c602              	ldab	#2
 417  000b 7b0012            	stab	_main_prms
 418                         ; 235 	main_prms.LED_Dsply_Tm = MAIN_FLT_MTR_TM;    // Default: 2 second fault mature time
 420  000e 7b0013            	stab	_main_prms+1
 421                         ; 237 	main_CSWM_Status_c = GOOD;              // every power on reset start with CSWM status as GOOD
 423  0011 53                	decb	
 424  0012 7b0011            	stab	_main_CSWM_Status_c
 425                         ; 238 	main_state_c = NORMAL;                  // CSWM default state: Normal functinality
 427  0015 79000c            	clr	_main_state_c
 428                         ; 241 	main_CSWM_stat_delay_c = 0;             // 800ms delay after powerOnReset to update main_CSWM_status 
 430  0018 790017            	clr	_main_CSWM_stat_delay_c
 431                         ; 242 	main_wheel_delay_c = 0;                 // 2 second delay to start wheel management and fault detection
 433  001b 790016            	clr	_main_wheel_delay_c
 434                         ; 244 	main_ECU_reset_rq_c = 0;                // Clear Diagnostic Service $11 $01 ECU Reset request
 436  001e 79000f            	clr	_main_ECU_reset_rq_c
 437                         ; 245 	main_RemSt_delay_c = 0;                 // 4 second Remote start delay (LHD_RHD cyclic 2000)
 439  0021 790009            	clr	_main_RemSt_delay_c
 440                         ; 246 	main_U12S_keep_ON_b = 0;                // clear flag to keep U12S on
 442                         ; 247 	main_kept_U12S_ON_b = 0;                // clear kept on flag
 444  0024 1d000506          	bclr	_main_flag2_c,6
 445                         ; 249 	mainLF_Variant_Select(); //Select the variant
 447  0028 0779              	jsr	_mainLF_Variant_Select
 449                         ; 251 	EE_BlockRead(EE_ENG_RPM_LIMIT,  &main_EngRpm_lowLmt_c);	// Store the Engine RPM low limit (disables CSWM ouputs)
 451  002a cc0003            	ldd	#_main_EngRpm_lowLmt_c
 452  002d 3b                	pshd	
 453  002e cc0013            	ldd	#19
 454  0031 4a000000          	call	f_EE_BlockRead
 456                         ; 252 	EE_BlockRead(EE_REMOTE_CTRL_HEAT_AMBTEMP, (u_8Bit*)&main_AmbTemp_RemStart_Heat_c);
 458  0035 cc0015            	ldd	#_main_AmbTemp_RemStart_Heat_c
 459  0038 6c80              	std	0,s
 460  003a cc0011            	ldd	#17
 461  003d 4a000000          	call	f_EE_BlockRead
 463                         ; 253 	EE_BlockRead(EE_REMOTE_CTRL_VENT_AMBTEMP, (u_8Bit*)&main_AmbTemp_RemStart_Vent_c);
 465  0041 cc0014            	ldd	#_main_AmbTemp_RemStart_Vent_c
 466  0044 6c80              	std	0,s
 467  0046 cc0012            	ldd	#18
 468  0049 4a000000          	call	f_EE_BlockRead
 470                         ; 255 	main_SystemV_stat_c = V_STAT_GOOD;      // every power on reset start with System voltage status as GOOD
 472  004d c601              	ldab	#1
 473  004f 7b001b            	stab	_main_SystemV_stat_c
 474                         ; 256 	main_BatteryV_stat_c = V_STAT_GOOD;     // every power on reset start with Battery voltage status as GOOD
 476  0052 7b001a            	stab	_main_BatteryV_stat_c
 477                         ; 257 	main_ldShd_mnt_stat_c = LDSHD_MNT_GOOD; // Load shed monitor status is good
 479  0055 87                	clra	
 480  0056 7a0019            	staa	_main_ldShd_mnt_stat_c
 481                         ; 258 	main_PCB_temp_stat_c = PCB_STAT_GOOD;   // every power on reset start with PCB temperature status as GOOD
 483  0059 c604              	ldab	#4
 484  005b 7b0018            	stab	_main_PCB_temp_stat_c
 485                         ; 260 	Pwm_SetDutyCycle(PWM_HS_FL, 0x0000); 
 487  005e c7                	clrb	
 488  005f 6c80              	std	0,s
 489  0061 c603              	ldab	#3
 490  0063 4a000000          	call	f_Pwm_SetDutyCycle
 492                         ; 261 	Pwm_SetDutyCycle(PWM_HS_FR, 0x0000); 
 494  0067 87                	clra	
 495  0068 c7                	clrb	
 496  0069 6c80              	std	0,s
 497  006b c604              	ldab	#4
 498  006d 4a000000          	call	f_Pwm_SetDutyCycle
 500                         ; 262 	Pwm_SetDutyCycle(PWM_HS_RL, 0x0000); 
 502  0071 87                	clra	
 503  0072 c7                	clrb	
 504  0073 6c80              	std	0,s
 505  0075 c605              	ldab	#5
 506  0077 4a000000          	call	f_Pwm_SetDutyCycle
 508                         ; 263 	Pwm_SetDutyCycle(PWM_HS_RR, 0x0000); 
 510  007b 87                	clra	
 511  007c c7                	clrb	
 512  007d 6c80              	std	0,s
 513  007f c606              	ldab	#6
 514  0081 4a000000          	call	f_Pwm_SetDutyCycle
 516                         ; 264 	Pwm_SetDutyCycle(PWM_STW, 0x0000);
 518  0085 87                	clra	
 519  0086 c7                	clrb	
 520  0087 6c80              	std	0,s
 521  0089 c607              	ldab	#7
 522  008b 4a000000          	call	f_Pwm_SetDutyCycle
 524                         ; 265 	Pwm_SetDutyCycle(PWM_VS_FL, 0x0000);
 526  008f 87                	clra	
 527  0090 c7                	clrb	
 528  0091 6c80              	std	0,s
 529  0093 52                	incb	
 530  0094 4a000000          	call	f_Pwm_SetDutyCycle
 532                         ; 266 	Pwm_SetDutyCycle(PWM_VS_FR, 0x0000);
 534  0098 87                	clra	
 535  0099 c7                	clrb	
 536  009a 6c80              	std	0,s
 537  009c 4a000000          	call	f_Pwm_SetDutyCycle
 539  00a0 1b82              	leas	2,s
 540                         ; 267 }
 543  00a2 3d                	rts	
 583                         ; 273 void near mainLF_Variant_Select(void)
 583                         ; 274 {
 584                         	switch	.text
 585  00a3                   _mainLF_Variant_Select:
 587  00a3 37                	pshb	
 588       00000001          OFST:	set	1
 591                         ; 320 	main_variant_select_c = Dio_ReadChannelGroup(VarReadGrp);
 593  00a4 f60003            	ldab	_DioConfigData+3
 594  00a7 87                	clra	
 595  00a8 59                	lsld	
 596  00a9 b746              	tfr	d,y
 597  00ab f60005            	ldab	_DioConfigData+5
 598  00ae e4eb0000          	andb	[_Dio_PortRead_Ptr,y]
 599  00b2 b60004            	ldaa	_DioConfigData+4
 600  00b5 2704              	beq	L01
 601  00b7                   L21:
 602  00b7 54                	lsrb	
 603  00b8 0430fc            	dbne	a,L21
 604  00bb                   L01:
 605  00bb 7b0000            	stab	_main_variant_select_c
 606                         ; 326 	if(main_variant_select_c == CSWM_V4)
 608                         ; 328 		temp_hw_cfg_c = CSWM_2HS_HSW_VARIANT_4_K;
 611                         ; 333 		temp_hw_cfg_c = CSWM_2HS_HSW_VARIANT_4_K;
 613  00be c643              	ldab	#67
 614  00c0 6b80              	stab	OFST-1,s
 615                         ; 366 		EE_BlockRead(EE_PROXI_CFG, (UINT8*)&ru_eep_PROXI_Cfg);
 617  00c2 cc0000            	ldd	#_ru_eep_PROXI_Cfg
 618  00c5 3b                	pshd	
 619  00c6 cc001d            	ldd	#29
 620  00c9 4a000000          	call	f_EE_BlockRead
 622  00cd 1b82              	leas	2,s
 623                         ; 367 		hardware_proxi_mismatch_c = FALSE;
 625  00cf 790007            	clr	_hardware_proxi_mismatch_c
 626                         ; 370 	if(ru_eep_PROXI_Cfg.byte[0] == 0xFF)
 628  00d2 f60000            	ldab	_ru_eep_PROXI_Cfg
 629  00d5 04a146            	ibne	b,L312
 630                         ; 373 		ru_eep_PROXI_Cfg.byte[0] = PROXI_CFG_CODE_DEFAULT;
 632  00d8 c6aa              	ldab	#170
 633  00da 7b0000            	stab	_ru_eep_PROXI_Cfg
 634                         ; 374 		ru_eep_PROXI_Cfg.byte[1] = PROXI_CFG_CODE_DEFAULT;
 636  00dd 7b0001            	stab	_ru_eep_PROXI_Cfg+1
 637                         ; 375 		ru_eep_PROXI_Cfg.byte[2] = PROXI_CFG_CODE_DEFAULT;
 639  00e0 7b0002            	stab	_ru_eep_PROXI_Cfg+2
 640                         ; 376 		ru_eep_PROXI_Cfg.byte[3] = PROXI_CFG_CODE_DEFAULT;
 642  00e3 7b0003            	stab	_ru_eep_PROXI_Cfg+3
 643                         ; 377 		ru_eep_PROXI_Cfg.byte[4] = PROXI_CFG_CODE_DEFAULT;
 645  00e6 7b0004            	stab	_ru_eep_PROXI_Cfg+4
 646                         ; 378 		ru_eep_PROXI_Cfg.byte[5] = PROXI_CFG_CODE_DEFAULT_LAST_BYTE;
 648  00e9 c6a0              	ldab	#160
 649  00eb 7b0005            	stab	_ru_eep_PROXI_Cfg+5
 650                         ; 380 		ru_eep_PROXI_Cfg.s.Driver_Side = PROXI_DS_DEFAULT;
 652  00ee 1d000901          	bclr	_ru_eep_PROXI_Cfg+9,1
 653                         ; 381 		ru_eep_PROXI_Cfg.s.Remote_Start = PROXI_RS_DEFAULT;
 655  00f2 1c000908          	bset	_ru_eep_PROXI_Cfg+9,8
 656                         ; 382 		ru_eep_PROXI_Cfg.s.Vehicle_Line_Configuration = PROXI_VEH_LINE_DEFAULT;
 658  00f6 c650              	ldab	#80
 659  00f8 7b0006            	stab	_ru_eep_PROXI_Cfg+6
 660                         ; 383 		ru_eep_PROXI_Cfg.s.Country_Code = PROXI_COUNTRY_CODE_US;
 662  00fb c6cd              	ldab	#205
 663  00fd 7b0007            	stab	_ru_eep_PROXI_Cfg+7
 664                         ; 384 		ru_eep_PROXI_Cfg.s.Model_Year = PROXI_MY_DEFAULT;
 666  0100 c60b              	ldab	#11
 667  0102 7b0008            	stab	_ru_eep_PROXI_Cfg+8
 668                         ; 385 		ru_eep_PROXI_Cfg.s.Heated_Seats_Variant = PROXI_HEATSEAT_DEFAULT;
 670                         ; 386 		ru_eep_PROXI_Cfg.s.Seat_Material = PROXI_SEAT_TYPE_CLOTH;
 672  0105 1d0009f6          	bclr	_ru_eep_PROXI_Cfg+9,246
 673                         ; 387 		ru_eep_PROXI_Cfg.s.Wheel_Material = PROXI_WHEEL_TYPE_WOOD;
 675  0109 1c000952          	bset	_ru_eep_PROXI_Cfg+9,82
 676                         ; 388 		ru_eep_PROXI_Cfg.s.Steering_Wheel = PROXI_WHEEL_PRESENT;
 678  010d 1c000a01          	bset	_ru_eep_PROXI_Cfg+10,1
 679                         ; 390 		EE_BlockWrite(EE_PROXI_CFG, (UINT8*)&ru_eep_PROXI_Cfg);
 681  0111 cc0000            	ldd	#_ru_eep_PROXI_Cfg
 682  0114 3b                	pshd	
 683  0115 cc001d            	ldd	#29
 684  0118 4a000000          	call	f_EE_BlockWrite
 686  011c 1b82              	leas	2,s
 687  011e                   L312:
 688                         ; 395 	if(ru_eep_PROXI_Cfg.s.Heated_Seats_Variant == PROXI_HEATSEAT_FRONT)
 690  011e f60009            	ldab	_ru_eep_PROXI_Cfg+9
 691  0121 c406              	andb	#6
 692  0123 c102              	cmpb	#2
 693  0125 2604              	bne	L512
 694                         ; 398 		CSWM_config_c = CSWM_2HS_VARIANT_1_K;
 696  0127 c603              	ldab	#3
 698  0129 2002              	bra	L712
 699  012b                   L512:
 700                         ; 403 		CSWM_config_c = CSWM_4HS_VARIANT_2_K;
 702  012b c60f              	ldab	#15
 703  012d                   L712:
 704  012d 7b0000            	stab	_CSWM_config_c
 705                         ; 407 	if(ru_eep_PROXI_Cfg.s.Steering_Wheel == PROXI_WHEEL_PRESENT)
 707  0130 1f000a0104        	brclr	_ru_eep_PROXI_Cfg+10,1,L122
 708                         ; 410 		CSWM_config_c |= CSWM_HSW_MASK_K;
 710  0135 1c000040          	bset	_CSWM_config_c,64
 711  0139                   L122:
 712                         ; 414 	if( (ru_eep_PROXI_Cfg.byte[0] == PROXI_CFG_CODE_DEFAULT) &&
 712                         ; 415 		(ru_eep_PROXI_Cfg.byte[1] == PROXI_CFG_CODE_DEFAULT) &&
 712                         ; 416 		(ru_eep_PROXI_Cfg.byte[2] == PROXI_CFG_CODE_DEFAULT))
 714  0139 f60000            	ldab	_ru_eep_PROXI_Cfg
 715  013c c1aa              	cmpb	#170
 716  013e 2613              	bne	L322
 718  0140 f60001            	ldab	_ru_eep_PROXI_Cfg+1
 719  0143 c1aa              	cmpb	#170
 720  0145 260c              	bne	L322
 722  0147 f60002            	ldab	_ru_eep_PROXI_Cfg+2
 723  014a c1aa              	cmpb	#170
 724  014c 2605              	bne	L322
 725                         ; 418 		hardware_proxi_mismatch_c = PROXI_NOT_PRGMED;
 727  014e 790007            	clr	_hardware_proxi_mismatch_c
 729  0151 2015              	bra	L522
 730  0153                   L322:
 731                         ; 423 		if (CSWM_config_c == temp_hw_cfg_c ) 
 733  0153 f60000            	ldab	_CSWM_config_c
 734  0156 e180              	cmpb	OFST-1,s
 735  0158 2604              	bne	L722
 736                         ; 426 			hardware_proxi_mismatch_c = PROXI_PRGMED_OK;
 738  015a c602              	ldab	#2
 740  015c 2007              	bra	LC001
 741  015e                   L722:
 742                         ; 432 			CSWM_config_c = temp_hw_cfg_c;
 744  015e 180d800000        	movb	OFST-1,s,_CSWM_config_c
 745                         ; 433 			hardware_proxi_mismatch_c = PROXI_HW_MISMATCH;
 747  0163 c601              	ldab	#1
 748  0165                   LC001:
 749  0165 7b0007            	stab	_hardware_proxi_mismatch_c
 750  0168                   L522:
 751                         ; 438 } //End of Variant selection function.
 754  0168 1b81              	leas	1,s
 755  016a 3d                	rts	
 779                         ; 479 void near mainF_TimeSliceGen()
 779                         ; 480 {
 780                         	switch	.text
 781  016b                   _mainF_TimeSliceGen:
 785                         ; 481 	main_timeSlice_counter_c.cb++;	// increment the time slice counter
 787  016b 72001d            	inc	_main_timeSlice_counter_c
 788                         ; 484 	if(main_timeSlice_counter_c.b.b0 == TRUE)
 790  016e 1f001d0105        	brclr	_main_timeSlice_counter_c,1,L342
 791                         ; 486 		main_timeSlice_request_c.b.b0 = TRUE;	// Set 2.5ms request
 793  0173 1c001e01          	bset	_main_timeSlice_request_c,1
 796  0177 3d                	rts	
 797  0178                   L342:
 798                         ; 488 	}else if(main_timeSlice_counter_c.b.b1 == TRUE){
 800  0178 1f001d0205        	brclr	_main_timeSlice_counter_c,2,L742
 801                         ; 490 		main_timeSlice_request_c.b.b1 = TRUE;	// Set 5ms request	
 803  017d 1c001e02          	bset	_main_timeSlice_request_c,2
 806  0181 3d                	rts	
 807  0182                   L742:
 808                         ; 492 	}else if(main_timeSlice_counter_c.b.b2 == TRUE){
 810  0182 1f001d0405        	brclr	_main_timeSlice_counter_c,4,L352
 811                         ; 494 		main_timeSlice_request_c.b.b2 = TRUE;	// Set 10ms request
 813  0187 1c001e04          	bset	_main_timeSlice_request_c,4
 816  018b 3d                	rts	
 817  018c                   L352:
 818                         ; 496 	}else if(main_timeSlice_counter_c.b.b3 == TRUE){
 820  018c 1f001d0805        	brclr	_main_timeSlice_counter_c,8,L752
 821                         ; 498 		main_timeSlice_request_c.b.b3 = TRUE;	// Set 20ms request
 823  0191 1c001e08          	bset	_main_timeSlice_request_c,8
 826  0195 3d                	rts	
 827  0196                   L752:
 828                         ; 500 	}else if(main_timeSlice_counter_c.b.b4 == TRUE){
 830  0196 1f001d1005        	brclr	_main_timeSlice_counter_c,16,L362
 831                         ; 502 		main_timeSlice_request_c.b.b4 = TRUE;	// Set 40ms request
 833  019b 1c001e10          	bset	_main_timeSlice_request_c,16
 836  019f 3d                	rts	
 837  01a0                   L362:
 838                         ; 504 	}else if(main_timeSlice_counter_c.b.b5 == TRUE){
 840  01a0 1f001d2005        	brclr	_main_timeSlice_counter_c,32,L762
 841                         ; 506 		main_timeSlice_request_c.b.b5 = TRUE;	// Set 80ms request
 843  01a5 1c001e20          	bset	_main_timeSlice_request_c,32
 846  01a9 3d                	rts	
 847  01aa                   L762:
 848                         ; 508 	}else if(main_timeSlice_counter_c.b.b6 == TRUE){
 850  01aa 1f001d4005        	brclr	_main_timeSlice_counter_c,64,L372
 851                         ; 510 		main_timeSlice_request_c.b.b6 = TRUE;	// Set 160ms request
 853  01af 1c001e40          	bset	_main_timeSlice_request_c,64
 856  01b3 3d                	rts	
 857  01b4                   L372:
 858                         ; 512 	}else if(main_timeSlice_counter_c.b.b7 == TRUE){
 860  01b4 1f001d8004        	brclr	_main_timeSlice_counter_c,128,L542
 861                         ; 514 		main_timeSlice_request_c.b.b7 = TRUE;	// Set 320ms request		
 863  01b9 1c001e80          	bset	_main_timeSlice_request_c,128
 864  01bd                   L542:
 865                         ; 516 }
 868  01bd 3d                	rts	
 897                         ; 538 void near mainLF_App_RamTst(void)
 897                         ; 539 {
 898                         	switch	.text
 899  01be                   _mainLF_App_RamTst:
 903                         ; 541 	if(main_RAM_test_rslt == RAMTST_RESULT_NOT_TESTED){
 905  01be f60004            	ldab	_main_RAM_test_rslt
 906  01c1 260a              	bne	L113
 907                         ; 544 		RamTst_MainFunction();
 909  01c3 160000            	jsr	_RamTst_MainFunction
 911                         ; 546 		main_RAM_test_rslt = RamTst_GetTestResult();
 913  01c6 160000            	jsr	_RamTst_GetTestResult
 915  01c9 7b0004            	stab	_main_RAM_test_rslt
 918  01cc 3d                	rts	
 919  01cd                   L113:
 920                         ; 549 		if(RAM_is_checked_b == FALSE){
 922  01cd 1e0006040a        	brset	_main_flag_c,4,L513
 923                         ; 551 			RamTst_Stop();
 925  01d2 c603              	ldab	#3
 926  01d4 7b0000            	stab	_RamTst_ExecutionStatus
 927                         ; 553 			IntFlt_F_Chck_Ram_Err();
 929  01d7 4a000000          	call	f_IntFlt_F_Chck_Ram_Err
 933  01db 3d                	rts	
 934  01dc                   L513:
 935                         ; 556 			IntFlt_F_Clr_Ram_Err();
 937  01dc 4a000000          	call	f_IntFlt_F_Clr_Ram_Err
 939                         ; 574 }
 942  01e0 3d                	rts	
1067                         ; 600 void near main(void)
1067                         ; 601 {
1068                         	switch	.text
1069  01e1                   _main:
1073                         ; 603 	DisableAllInterrupts();	// Disable all interrupts for initilizations	
1076  01e1 1410              	sei	
1078                         ; 607 	LoadStack_Pointer();	// load stack pointer
1082                         xref __stack
1087  01e3 cf0000            	lds	#__stack
1089                         ; 608 	Modified_StartUp();	// Jump to modified start-up sub-routine
1093                         xref __Startup
1098  01e6 160000            	jsr	__Startup
1100                         ; 614 	Mcu_Init(&Mcu_Config);	
1103  01e9 cc0000            	ldd	#_Mcu_Config
1104  01ec 160000            	jsr	_Mcu_Init
1106                         ; 615 	main_InitClock_stat = Mcu_InitClock(CLK_SETTING_0);
1108  01ef 87                	clra	
1109  01f0 c7                	clrb	
1110  01f1 160000            	jsr	_Mcu_InitClock
1112  01f4 7b001c            	stab	_main_InitClock_stat
1113                         ; 616 	Port_Init(&Port_Config[PORT_config_0]);
1115  01f7 cc0000            	ldd	#_Port_Config
1116  01fa 160000            	jsr	_Port_Init
1118                         ; 620     RamTst_Init();	  
1120  01fd 160000            	jsr	_RamTst_Init
1122                         ; 621 	Gpt_Init(&Gpt_Config[GPT_config_0]);	
1124  0200 cc0000            	ldd	#_Gpt_Config
1125  0203 160000            	jsr	_Gpt_Init
1127                         ; 622 	DisableAllInterrupts();		// Gpt_Init Enables all interrupts	
1130  0206 1410              	sei	
1132                         ; 623 	Gpt_StartTimer(TimeSlice_CH2,(Gpt_ValueType)MAIN_TMSLICE_PERIOD);	
1135  0208 cc0271            	ldd	#625
1136  020b 3b                	pshd	
1137  020c 87                	clra	
1138  020d c7                	clrb	
1139  020e 3b                	pshd	
1140  020f 160000            	jsr	_Gpt_StartTimer
1142  0212 1b84              	leas	4,s
1143                         ; 624 	DisableAllInterrupts();		// Gpt_StartTimer Enables all interrupts	
1146  0214 1410              	sei	
1148                         ; 625 	Gpt_EnableNotification(TimeSlice_CH2);	
1151  0216 87                	clra	
1152  0217 c7                	clrb	
1153  0218 160000            	jsr	_Gpt_EnableNotification
1155                         ; 626 	Pwm_Init(&Pwm_Config[PWM_config_0]);	
1157  021b cc0000            	ldd	#_Pwm_Config
1158  021e 160000            	jsr	_Pwm_Init
1160                         ; 627 	DisableAllInterrupts();		// Pwm_Init Enables all interrupts
1163  0221 1410              	sei	
1165                         ; 628 	Adc_Init(&Adc_Config[ADC_CONFIG_0]);	
1168  0223 cc0000            	ldd	#_Adc_Config
1169  0226 160000            	jsr	_Adc_Init
1171                         ; 630 	DisableAllInterrupts();
1174  0229 1410              	sei	
1176                         ; 638 	nwmngF_Init();
1179  022b 4a000000          	call	f_nwmngF_Init
1181                         ; 639 	EE_PowerupInit(); 	
1183  022f 4a000000          	call	f_EE_PowerupInit
1185                         ; 640 	diagF_Init();
1187  0233 4a000000          	call	f_diagF_Init
1189                         ; 641 	EE_LoadDefaults();  
1191  0237 4a000000          	call	f_EE_LoadDefaults
1193                         ; 643 	mainLF_Init();		// main Init
1195  023b 160000            	jsr	_mainLF_Init
1197                         ; 644 	canF_Init();
1199  023e 4a000000          	call	f_canF_Init
1201                         ; 646 	ADF_Init();         // ADC module init
1203  0242 160000            	jsr	_ADF_Init
1205                         ; 647     IntFlt_F_Init();    // internal faults init
1207  0245 4a000000          	call	f_IntFlt_F_Init
1209                         ; 648     ROMTstF_Init();		// rom test int
1211  0249 160000            	jsr	_ROMTstF_Init
1213                         ; 649 	BattVF_Init();	    // Voltage monitor init	
1215  024c 4a000000          	call	f_BattVF_Init
1217                         ; 650 	rlF_InitRelay();	// relay init
1219  0250 4a000000          	call	f_rlF_InitRelay
1221                         ; 651 	hsF_Init ();		// heaters init
1223  0254 4a000000          	call	f_hsF_Init
1225                         ; 652 	stWF_Init();		// steering wheel init
1227  0258 4a000000          	call	f_stWF_Init
1229                         ; 653 	vsF_Init();			// vents init
1231  025c 4a000000          	call	f_vsF_Init
1233                         ; 654 	FMemLibF_Init();	// DTC Fault Memory Library Init
1235  0260 4a000000          	call	f_FMemLibF_Init
1237                         ; 655 	TempF_Init ();		// Temperature module init
1239  0264 4a000000          	call	f_TempF_Init
1241                         ; 663 	EnableAllInterrupts();	// Initilizations are completed. Enabling all interrupts		
1244  0268 10ef              	cli	
1246                         ; 664   nmCan_Start_IL();     // Start IL Rx and Tx
1249  026a 4a000000          	call	f_nmCan_Start_IL
1251  026e                   L133:
1252                         ; 668 		if(main_timeSlice_request_c.b.b0 == TRUE)
1254  026e 1f001e0122        	brclr	_main_timeSlice_request_c,1,L533
1255                         ; 687                         TempF_U12S_ADC();
1257  0273 4a000000          	call	f_TempF_U12S_ADC
1259                         ; 690 			hsF_AD_Readings();
1261  0277 4a000000          	call	f_hsF_AD_Readings
1263                         ; 693 			if ( (CSWM_config_c & CSWM_HSW_MASK_K) == CSWM_HSW_MASK_K)
1265  027b 1f00004004        	brclr	_CSWM_config_c,64,L143
1266                         ; 696 				stWF_AD_Rdng();
1268  0280 4a000000          	call	f_stWF_AD_Rdng
1271  0284                   L143:
1272                         ; 704 			if ( (CSWM_config_c & CSWM_2VS_MASK_K) == CSWM_2VS_MASK_K)
1274  0284 1e00003002        	brset	_CSWM_config_c,48,LC002
1275  0289 2004              	bra	L543
1276  028b                   LC002:
1277                         ; 707 				vsF_ADC_Monitor();
1279  028b 4a000000          	call	f_vsF_ADC_Monitor
1282  028f                   L543:
1283                         ; 716 			main_timeSlice_request_c.b.b0 = FALSE;
1285  028f 1d001e01          	bclr	_main_timeSlice_request_c,1
1287  0293 20d9              	bra	L133
1288  0295                   L533:
1289                         ; 719 		}else if(main_timeSlice_request_c.b.b1 == TRUE){
1291  0295 1f001e022a        	brclr	_main_timeSlice_request_c,2,L153
1292                         ; 724 			IntFlt_F_Chck_Osci_Err();
1294  029a 4a000000          	call	f_IntFlt_F_Chck_Osci_Err
1296                         ; 728 			BattVF_AD_Rdg();
1298  029e 4a000000          	call	f_BattVF_AD_Rdg
1300                         ; 745 			hsF_HeatRequestToFet();
1302  02a2 4a000000          	call	f_hsF_HeatRequestToFet
1304                         ; 747 			if ((CSWM_config_c & CSWM_HSW_MASK_K) == CSWM_HSW_MASK_K)
1306  02a6 1f00004004        	brclr	_CSWM_config_c,64,L553
1307                         ; 750 				stWF_Output_Control();
1309  02ab 4a000000          	call	f_stWF_Output_Control
1312  02af                   L553:
1313                         ; 758 			if ((CSWM_config_c & CSWM_2VS_MASK_K) == CSWM_2VS_MASK_K)
1315  02af 1e00003002        	brset	_CSWM_config_c,48,LC003
1316  02b4 2004              	bra	L163
1317  02b6                   LC003:
1318                         ; 761 				vsF_Output_Cntrl();
1320  02b6 4a000000          	call	f_vsF_Output_Cntrl
1323  02ba                   L163:
1324                         ; 769 			EE_Manager();
1326  02ba 4a000000          	call	f_EE_Manager
1328                         ; 772 			main_timeSlice_request_c.b.b1 = FALSE;
1330  02be 1d001e02          	bclr	_main_timeSlice_request_c,2
1332  02c2 20aa              	bra	L133
1333  02c4                   L153:
1334                         ; 775 		}else if(main_timeSlice_request_c.b.b2 == TRUE){
1336  02c4 1f001e0453        	brclr	_main_timeSlice_request_c,4,L563
1337                         ; 781                          IntFlt_F_Chck_PLL();
1339  02c9 4a000000          	call	f_IntFlt_F_Chck_PLL
1341                         ; 782                          nmCan_Cyclic_Tasks();
1343  02cd 4a000000          	call	f_nmCan_Cyclic_Tasks
1345                         ; 784 			if (Allow_CSWM_status_Update_b == TRUE) {
1347  02d1 1f00061003        	brclr	_main_flag_c,16,L173
1348                         ; 786 				mainF_CSWM_Status();
1350  02d6 160458            	jsr	_mainF_CSWM_Status
1353  02d9                   L173:
1354                         ; 794 			rlF_RelayManager ();
1356  02d9 4a000000          	call	f_rlF_RelayManager
1358                         ; 796 			hsF_HeatManager ();
1360  02dd 4a000000          	call	f_hsF_HeatManager
1362                         ; 799 			if ( (CSWM_config_c & CSWM_HSW_MASK_K) == CSWM_HSW_MASK_K)
1364  02e1 1f00004014        	brclr	_CSWM_config_c,64,L104
1365                         ; 802 				if (main_wheel_delay_c < MAIN_2SEC_DELAY)
1367  02e6 f60016            	ldab	_main_wheel_delay_c
1368  02e9 c1c8              	cmpb	#200
1369  02eb 2405              	bhs	L573
1370                         ; 805 					main_wheel_delay_c++;
1372  02ed 720016            	inc	_main_wheel_delay_c
1374  02f0 2004              	bra	L773
1375  02f2                   L573:
1376                         ; 814 					Allow_stWheel_mngmnt_b = TRUE;
1378  02f2 1c000620          	bset	_main_flag_c,32
1379  02f6                   L773:
1380                         ; 819 				stWF_Manager();
1382  02f6 4a000000          	call	f_stWF_Manager
1385  02fa                   L104:
1386                         ; 827 			if ((CSWM_config_c & CSWM_2VS_MASK_K) == CSWM_2VS_MASK_K)
1388  02fa 1e00003002        	brset	_CSWM_config_c,48,LC004
1389  02ff 2004              	bra	L504
1390  0301                   LC004:
1391                         ; 830 				vsF_Manager();
1393  0301 4a000000          	call	f_vsF_Manager
1396  0305                   L504:
1397                         ; 837 			diagF_CyclicWriteTask();
1399  0305 4a000000          	call	f_diagF_CyclicWriteTask
1401                         ; 840 			canF_CyclicRemoteStart();
1403  0309 4a000000          	call	f_canF_CyclicRemoteStart
1405                         ; 843 			canF_CyclicManIndFunc();
1407  030d 4a000000          	call	f_canF_CyclicManIndFunc
1409                         ; 849 			mainLF_App_RamTst();
1411  0311 1601be            	jsr	_mainLF_App_RamTst
1413                         ; 853 			main_timeSlice_request_c.b.b2 = FALSE;
1415  0314 1d001e04          	bclr	_main_timeSlice_request_c,4
1417  0318 1820ff52          	bra	L133
1418  031c                   L563:
1419                         ; 856 		}else if(main_timeSlice_request_c.b.b3 == TRUE){
1421  031c 1f001e083d        	brclr	_main_timeSlice_request_c,8,L114
1422                         ; 861 			if (main_RemSt_delay_c < MAIN_REMST_DELAY)
1424  0321 f60009            	ldab	_main_RemSt_delay_c
1425  0324 c1c8              	cmpb	#200
1426  0326 2405              	bhs	L314
1427                         ; 864 				main_RemSt_delay_c++;
1429  0328 720009            	inc	_main_RemSt_delay_c
1431  032b 2003              	bra	L514
1432  032d                   L314:
1433                         ; 869 				mainF_RemSt_Manager();
1435  032d 1604ef            	jsr	_mainF_RemSt_Manager
1437  0330                   L514:
1438                         ; 873 			if ( (CSWM_config_c & CSWM_HSW_MASK_K) == CSWM_HSW_MASK_K)
1440  0330 1f00004004        	brclr	_CSWM_config_c,64,L124
1441                         ; 889 				stWF_Flt_Mntr();				
1443  0335 4a000000          	call	f_stWF_Flt_Mntr
1446  0339                   L124:
1447                         ; 897 			if((ROMTst_status != ROMTST_EXECUTION_FINISHED)){
1449  0339 f60000            	ldab	_ROMTst_status
1450  033c c103              	cmpb	#3
1451  033e 2705              	beq	L324
1452                         ; 899 				ROMTstF_Calc_ChckSum();
1454  0340 160000            	jsr	_ROMTstF_Calc_ChckSum
1457  0343 200d              	bra	L524
1458  0345                   L324:
1459                         ; 903 				if(ROM_is_checked_b == FALSE) {
1461  0345 1e00060805        	brset	_main_flag_c,8,L724
1462                         ; 905 					ROMTstF_Chck_Err();
1464  034a 160000            	jsr	_ROMTstF_Chck_Err
1467  034d 2003              	bra	L524
1468  034f                   L724:
1469                         ; 908 					ROMTstF_Clr_Rom_Err();
1471  034f 160000            	jsr	_ROMTstF_Clr_Rom_Err
1473  0352                   L524:
1474                         ; 914 			diagF_DCX_OutputTest();		
1476  0352 4a000000          	call	f_diagF_DCX_OutputTest
1478                         ; 917 			main_timeSlice_request_c.b.b3 = FALSE;
1480  0356 1d001e08          	bclr	_main_timeSlice_request_c,8
1482  035a 1820ff10          	bra	L133
1483  035e                   L114:
1484                         ; 920 		}else if(main_timeSlice_request_c.b.b4 == TRUE){
1486  035e 1f001e1042        	brclr	_main_timeSlice_request_c,16,L534
1487                         ; 923 			nwmngF_State_Transition();			
1489  0363 4a000000          	call	f_nwmngF_State_Transition
1491                         ; 926 			if (main_CSWM_stat_delay_c < MAIN_CSWM_STAT_DELAY)
1493  0367 f60017            	ldab	_main_CSWM_stat_delay_c
1494  036a c114              	cmpb	#20
1495  036c 2405              	bhs	L734
1496                         ; 929 				main_CSWM_stat_delay_c++;
1498  036e 720017            	inc	_main_CSWM_stat_delay_c
1500  0371 2008              	bra	L144
1501  0373                   L734:
1502                         ; 934 				BattVF_Manager();
1504  0373 4a000000          	call	f_BattVF_Manager
1506                         ; 937 				Allow_CSWM_status_Update_b = TRUE;
1508  0377 1c000610          	bset	_main_flag_c,16
1509  037b                   L144:
1510                         ; 941 			canF_CyclicTask();
1512  037b 4a000000          	call	f_canF_CyclicTask
1514                         ; 954 			if (((CSWM_config_c & CSWM_4HS_VARIANT_MASK_K) == CSWM_4HS_VARIANT_MASK_K) && (main_SwReq_Rear_c == VEH_SWITCH_REAR_ONLY_K))
1516  037f 1e00000f02        	brset	_CSWM_config_c,15,LC005
1517  0384 200b              	bra	L344
1518  0386                   LC005:
1520  0386 f6000a            	ldab	_main_SwReq_Rear_c
1521  0389 c103              	cmpb	#3
1522  038b 2604              	bne	L344
1523                         ; 956 				hsF_RearSwitchStuckDTC();
1525  038d 4a000000          	call	f_hsF_RearSwitchStuckDTC
1527  0391                   L344:
1528                         ; 960 			mainF_OFF_NonSupported_Outputs_ServiceParts();
1530  0391 1605b9            	jsr	_mainF_OFF_NonSupported_Outputs_ServiceParts
1532                         ; 963 			if ( (CSWM_config_c & CSWM_HSW_MASK_K) == CSWM_HSW_MASK_K)
1534  0394 1f00004004        	brclr	_CSWM_config_c,64,L744
1535                         ; 967 				stWF_CurrentLimit_Timer();				
1537  0399 4a000000          	call	f_stWF_CurrentLimit_Timer
1540  039d                   L744:
1541                         ; 975 			main_timeSlice_request_c.b.b4 = FALSE;
1543  039d 1d001e10          	bclr	_main_timeSlice_request_c,16
1545  03a1 1820fec9          	bra	L133
1546  03a5                   L534:
1547                         ; 977 		}else if(main_timeSlice_request_c.b.b5 == TRUE){
1549  03a5 1f001e205b        	brclr	_main_timeSlice_request_c,32,L354
1550                         ; 989 			if(relay_C_keep_U12S_ON_b == TRUE){
1552  03aa 1f00001010        	brclr	_relay_control2_c,16,L554
1553                         ; 991 				Dio_WriteChannel(U12T_EN, STD_HIGH);
1556  03af 1410              	sei	
1561  03b1 fe0000            	ldx	_Dio_PortWrite_Ptr
1562  03b4 0c0010            	bset	0,x,16
1566  03b7 10ef              	cli	
1568                         ; 993 				main_U12S_keep_ON_b = TRUE;				
1571  03b9 1c000502          	bset	_main_flag2_c,2
1573  03bd 201a              	bra	L754
1574  03bf                   L554:
1575                         ; 996 				main_U12S_keep_ON_b = FALSE;
1577  03bf 1d000502          	bclr	_main_flag2_c,2
1578                         ; 999 				U12S_enable_b = ~U12S_enable_b;
1580  03c3 f60006            	ldab	_main_flag_c
1581  03c6 c802              	eorb	#2
1582  03c8 7b0006            	stab	_main_flag_c
1583                         ; 1001 				if (U12S_enable_b == TRUE)
1585  03cb c502              	bitb	#2
1586  03cd 270a              	beq	L754
1587                         ; 1004 					Dio_WriteChannel(U12T_EN, STD_HIGH);
1590  03cf 1410              	sei	
1595  03d1 fe0000            	ldx	_Dio_PortWrite_Ptr
1596  03d4 0c0010            	bset	0,x,16
1600  03d7 10ef              	cli	
1603  03d9                   L754:
1604                         ; 1008 			rlF_RelayDTC();
1606  03d9 4a000000          	call	f_rlF_RelayDTC
1608                         ; 1010 			hsF_HeatDTC();
1610  03dd 4a000000          	call	f_hsF_HeatDTC
1612                         ; 1012 			IntFlt_F_DTC_Monitor();
1614  03e1 4a000000          	call	f_IntFlt_F_DTC_Monitor
1616                         ; 1015 			if ( (CSWM_config_c & CSWM_2VS_MASK_K) == CSWM_2VS_MASK_K)
1618  03e5 1e00003002        	brset	_CSWM_config_c,48,LC006
1619  03ea 2008              	bra	L564
1620  03ec                   LC006:
1621                         ; 1018 				vsF_Fault_Mntr();
1623  03ec 4a000000          	call	f_vsF_Fault_Mntr
1625                         ; 1021 				vsF_DTC_Manager();
1627  03f0 4a000000          	call	f_vsF_DTC_Manager
1630  03f4                   L564:
1631                         ; 1029 			if ( (CSWM_config_c & CSWM_HSW_MASK_K) == CSWM_HSW_MASK_K)
1633  03f4 1f00004004        	brclr	_CSWM_config_c,64,L174
1634                         ; 1032 				stWF_DTC_Manager();
1636  03f9 4a000000          	call	f_stWF_DTC_Manager
1639  03fd                   L174:
1640                         ; 1040 			main_timeSlice_request_c.b.b5 = FALSE;
1642  03fd 1d001e20          	bclr	_main_timeSlice_request_c,32
1644  0401 1820fe69          	bra	L133
1645  0405                   L354:
1646                         ; 1042 		}else if(main_timeSlice_request_c.b.b6 == TRUE){
1648  0405 1f001e4024        	brclr	_main_timeSlice_request_c,64,L574
1649                         ; 1045 			U12S_enable_b = TRUE;
1651  040a 1c000602          	bset	_main_flag_c,2
1652                         ; 1048 			TempF_Monitor_ADC();
1654  040e 4a000000          	call	f_TempF_Monitor_ADC
1656                         ; 1052 			TempF_HS_NTC_Flt_Monitor();
1658  0412 4a000000          	call	f_TempF_HS_NTC_Flt_Monitor
1660                         ; 1055 			TempF_PCB_ADC();
1662  0416 4a000000          	call	f_TempF_PCB_ADC
1664                         ; 1059 			TempF_PCB_NTC_Flt_Monitor();
1666  041a 4a000000          	call	f_TempF_PCB_NTC_Flt_Monitor
1668                         ; 1062 			BattVF_DTC_Manager();
1670  041e 4a000000          	call	f_BattVF_DTC_Manager
1672                         ; 1065 			hsF_NTCDTC();
1674  0422 4a000000          	call	f_hsF_NTCDTC
1676                         ; 1068 			main_timeSlice_request_c.b.b6 = FALSE;
1678  0426 1d001e40          	bclr	_main_timeSlice_request_c,64
1680  042a 1820fe40          	bra	L133
1681  042e                   L574:
1682                         ; 1070 		}else if(main_timeSlice_request_c.b.b7 == TRUE){
1684  042e f6001e            	ldab	_main_timeSlice_request_c
1685  0431 c580              	bitb	#128
1686  0433 1827fe37          	beq	L133
1687                         ; 1074 			hsF_MonitorHeatTimes();
1689  0437 4a000000          	call	f_hsF_MonitorHeatTimes
1691                         ; 1077 			if ((CSWM_config_c & CSWM_HSW_MASK_K) == CSWM_HSW_MASK_K)
1693  043b 1f00004004        	brclr	_CSWM_config_c,64,L505
1694                         ; 1080 				stWF_Timer();
1696  0440 4a000000          	call	f_stWF_Timer
1699  0444                   L505:
1700                         ; 1087 			FMemLibF_CheckDtcConditionTimer();
1702  0444 4a000000          	call	f_FMemLibF_CheckDtcConditionTimer
1704                         ; 1089 			FMemLibF_NMFailSts();
1706  0448 4a000000          	call	f_FMemLibF_NMFailSts
1708                         ; 1092 			FMemLibF_CyclicDtcTask();
1710  044c 4a000000          	call	f_FMemLibF_CyclicDtcTask
1712                         ; 1095 			main_timeSlice_request_c.b.b7 = FALSE;
1714  0450 1d001e80          	bclr	_main_timeSlice_request_c,128
1715  0454 1820fe16          	bra	L133
1752                         ; 1128 void near mainF_CSWM_Status(void)
1752                         ; 1129 {
1753                         	switch	.text
1754  0458                   _mainF_CSWM_Status:
1758                         ; 1131 	main_SystemV_stat_c = BattVF_Get_Status(SYSTEM_V);
1760  0458 87                	clra	
1761  0459 c7                	clrb	
1762  045a 4a000000          	call	f_BattVF_Get_Status
1764  045e 7b001b            	stab	_main_SystemV_stat_c
1765                         ; 1133 	main_BatteryV_stat_c = BattVF_Get_Status(BATTERY_V);
1767  0461 cc0001            	ldd	#1
1768  0464 4a000000          	call	f_BattVF_Get_Status
1770  0468 7b001a            	stab	_main_BatteryV_stat_c
1771                         ; 1135 	main_ldShd_mnt_stat_c = BattVF_Get_LdShd_Mnt_Stat();
1773  046b 4a000000          	call	f_BattVF_Get_LdShd_Mnt_Stat
1775  046f 7b0019            	stab	_main_ldShd_mnt_stat_c
1776                         ; 1137 	main_PCB_temp_stat_c = TempF_Get_PCB_NTC_Stat();
1778  0472 4a000000          	call	f_TempF_Get_PCB_NTC_Stat
1780  0476 7b0018            	stab	_main_PCB_temp_stat_c
1781                         ; 1142 	if ( (final_CBC_LOC_b == FALSE) && 
1781                         ; 1143 		(final_TGW_LOC_b == FALSE) &&
1781                         ; 1144 		(main_PCB_temp_stat_c != PCB_STAT_OVERTEMP) && 
1781                         ; 1145 		(int_flt_byte0.cb == NO_INT_FLT) &&
1781                         ; 1146 		(canio_canbus_off_b == FALSE))
1783  0479 f60000            	ldab	_canio_status2_flags_c
1784  047c c510              	bitb	#16
1785  047e 2669              	bne	L715
1787  0480 c520              	bitb	#32
1788  0482 2665              	bne	L715
1790  0484 f60018            	ldab	_main_PCB_temp_stat_c
1791  0487 c103              	cmpb	#3
1792  0489 275e              	beq	L715
1794  048b f60000            	ldab	_int_flt_byte0
1795  048e 2659              	bne	L715
1797  0490 f60000            	ldab	_canio_status2_flags_c
1798  0493 c501              	bitb	#1
1799  0495 2652              	bne	L715
1800                         ; 1150 		if ((canio_RX_EngineSpeed_c <  main_EngRpm_lowLmt_c ) && 
1800                         ; 1151 		    (diag_io_active_b == FALSE) && (diag_rc_all_op_lock_b == FALSE) )
1802  0497 f60000            	ldab	_canio_RX_EngineSpeed_c
1803  049a f10003            	cmpb	_main_EngRpm_lowLmt_c
1804  049d 240c              	bhs	L125
1806  049f 1e00000807        	brset	_diag_io_lock3_flags_c,8,L125
1808  04a4 1e00000102        	brset	_diag_rc_lock_flags_c,1,L125
1809                         ; 1154 			main_CSWM_Status_c = UGLY;
1812  04a9 203e              	bra	L715
1813  04ab                   L125:
1814                         ; 1159 			if (canio_RX_IGN_STATUS_c != IGN_RUN)
1816  04ab f60000            	ldab	_canio_RX_IGN_STATUS_c
1817  04ae c104              	cmpb	#4
1818  04b0 2710              	beq	L525
1819                         ; 1162 				if (canio_RX_IGN_STATUS_c == IGN_START)
1821  04b2 c105              	cmpb	#5
1822  04b4 2633              	bne	L715
1823                         ; 1165 					if ( (main_ldShd_mnt_stat_c != LDSHD_MNT_UGLY) &&
1823                         ; 1166 						(main_SystemV_stat_c != V_STAT_UGLY) &&
1823                         ; 1167 						(main_BatteryV_stat_c != V_STAT_UGLY) )
1825  04b6 f60019            	ldab	_main_ldShd_mnt_stat_c
1826  04b9 c102              	cmpb	#2
1827  04bb 272c              	beq	L715
1829  04bd f6001b            	ldab	_main_SystemV_stat_c
1831                         ; 1170 						main_CSWM_Status_c = BAD;
1834  04c0 2007              	bra	LC009
1835                         ; 1175 						main_CSWM_Status_c = UGLY;
1837                         ; 1181 					main_CSWM_Status_c = UGLY;
1839  04c2                   L525:
1840                         ; 1221 					if (main_SystemV_stat_c != V_STAT_GOOD)
1842  04c2 f6001b            	ldab	_main_SystemV_stat_c
1843  04c5 c101              	cmpb	#1
1844  04c7 270f              	beq	L145
1845                         ; 1224 						if ((main_SystemV_stat_c != V_STAT_UGLY) &&
1845                         ; 1225 							(main_BatteryV_stat_c != V_STAT_UGLY))
1848  04c9                   LC009:
1849  04c9 c103              	cmpb	#3
1850  04cb 271c              	beq	L715
1851  04cd f6001a            	ldab	_main_BatteryV_stat_c
1852  04d0 c103              	cmpb	#3
1853  04d2 2715              	beq	L715
1854                         ; 1228 							main_CSWM_Status_c = BAD;
1856  04d4                   LC008:
1857  04d4 c602              	ldab	#2
1859  04d6 2013              	bra	L165
1860                         ; 1233 							main_CSWM_Status_c = UGLY;
1862  04d8                   L145:
1863                         ; 1239 						if (main_BatteryV_stat_c != V_STAT_GOOD)
1865  04d8 f6001a            	ldab	_main_BatteryV_stat_c
1866  04db c101              	cmpb	#1
1867  04dd 2706              	beq	L155
1868                         ; 1242 							if (main_BatteryV_stat_c == V_STAT_BAD)
1870  04df c102              	cmpb	#2
1871  04e1 2606              	bne	L715
1872                         ; 1245 								main_CSWM_Status_c = BAD;
1875  04e3 20ef              	bra	LC008
1876                         ; 1250 								main_CSWM_Status_c = UGLY;
1878  04e5                   L155:
1879                         ; 1256 							main_CSWM_Status_c = GOOD;
1881  04e5 c601              	ldab	#1
1882  04e7 2002              	bra	L165
1883  04e9                   L715:
1884                         ; 1266 		main_CSWM_Status_c = UGLY;
1886  04e9 c603              	ldab	#3
1887  04eb                   L165:
1888  04eb 7b0011            	stab	_main_CSWM_Status_c
1889                         ; 1269 } //Function End
1892  04ee 3d                	rts	
1894                         	switch	.data
1895  0000                   L365_main_RemSt_configDelay_c:
1896  0000 00                	dc.b	0
1937                         ; 1288 void near mainF_RemSt_Manager(void)
1937                         ; 1289 {
1938                         	switch	.text
1939  04ef                   _mainF_RemSt_Manager:
1943                         ; 1297 		if ( (canio_RX_IgnRun_RemSt_c == TRUE) && (RemSt_activated_b == FALSE) )
1945  04ef f60000            	ldab	_canio_RX_IgnRun_RemSt_c
1946  04f2 c101              	cmpb	#1
1947  04f4 2632              	bne	L106
1949  04f6 1e0006402d        	brset	_main_flag_c,64,L106
1950                         ; 1300 			if (main_RemSt_configDelay_c < MAIN_REMST_CONFIG_DELAY)
1952  04fb f60000            	ldab	L365_main_RemSt_configDelay_c
1953  04fe c118              	cmpb	#24
1954  0500 2404              	bhs	L306
1955                         ; 1303 				main_RemSt_configDelay_c++;
1957  0502 720000            	inc	L365_main_RemSt_configDelay_c
1960  0505 3d                	rts	
1961  0506                   L306:
1962                         ; 1310 				if ( (canio_RemSt_configStat_c == TRUE) && (canio_RX_ExternalTemperature_c != EXTERNAL_TEMP_SNA) )
1964  0506 f60000            	ldab	_canio_RemSt_configStat_c
1965  0509 c101              	cmpb	#1
1966  050b 2617              	bne	L516
1968  050d f60000            	ldab	_canio_RX_ExternalTemperature_c
1969  0510 2712              	beq	L516
1970                         ; 1313 					if (canio_RX_ExternalTemperature_c <=  main_AmbTemp_RemStart_Heat_c)
1972  0512 f10015            	cmpb	_main_AmbTemp_RemStart_Heat_c
1973  0515 2204              	bhi	L116
1974                         ; 1316 						main_state_c = REM_STRT_HEATING;
1976  0517 c601              	ldab	#1
1978  0519 2032              	bra	L336
1979  051b                   L116:
1980                         ; 1321 						if (canio_RX_ExternalTemperature_c >=  main_AmbTemp_RemStart_Vent_c)
1982  051b f10014            	cmpb	_main_AmbTemp_RemStart_Vent_c
1983  051e 2504              	blo	L516
1984                         ; 1324 							main_state_c = REM_STRT_VENTING;
1986  0520 c602              	ldab	#2
1988  0522 2029              	bra	L336
1989  0524                   L516:
1990                         ; 1329 							main_state_c = REM_STRT_USER_SWTCH;
1992                         ; 1336 					main_state_c = REM_STRT_USER_SWTCH;
1994  0524 c603              	ldab	#3
1995                         ; 1339 				RemSt_activated_b = TRUE;
1997  0526 2025              	bra	L336
1998  0528                   L106:
1999                         ; 1345 			if (canio_RX_IgnRun_RemSt_c == FALSE)
2001  0528 04614c            	tbne	b,L326
2002                         ; 1349 				if ((canio_RemSt_configStat_c == TRUE) && (RemSt_activated_b == FALSE) )
2004  052b f60000            	ldab	_canio_RemSt_configStat_c
2005  052e 042124            	dbne	b,L726
2007  0531 1e0006401f        	brset	_main_flag_c,64,L726
2008                         ; 1353 						if (canio_RX_ExternalTemperature_c <=  main_AmbTemp_RemStart_Heat_c )
2010  0536 f60000            	ldab	_canio_RX_ExternalTemperature_c
2011  0539 f10015            	cmpb	_main_AmbTemp_RemStart_Heat_c
2012  053c 2204              	bhi	L136
2013                         ; 1356 							main_state_c = AUTO_STRT_HEATING;
2015  053e c604              	ldab	#4
2017  0540 200b              	bra	L336
2018  0542                   L136:
2019                         ; 1362 							if (canio_RX_ExternalTemperature_c >=  main_AmbTemp_RemStart_Vent_c)
2021  0542 f10014            	cmpb	_main_AmbTemp_RemStart_Vent_c
2022  0545 2504              	blo	L536
2023                         ; 1365 								main_state_c = AUTO_STRT_VENTING;
2025  0547 c605              	ldab	#5
2027  0549 2002              	bra	L336
2028  054b                   L536:
2029                         ; 1370 								main_state_c = AUTO_STRT_USER_SWTCH;
2031  054b c606              	ldab	#6
2032  054d                   L336:
2033                         ; 1374 					RemSt_activated_b = TRUE;
2035  054d 7b000c            	stab	_main_state_c
2036  0550 1c000640          	bset	_main_flag_c,64
2039  0554 3d                	rts	
2040  0555                   L726:
2041                         ; 1383 					if((CSWM_config_c == CSWM_2HS_VARIANT_1_K) || (CSWM_config_c == CSWM_4HS_VARIANT_2_K) || (CSWM_config_c == CSWM_2HS_2VS_VARIANT_5_K) || (CSWM_config_c == CSWM_4HS_2VS_VARIANT_8_K) )
2043  0555 f60000            	ldab	_CSWM_config_c
2044  0558 c103              	cmpb	#3
2045  055a 270c              	beq	L546
2047  055c c10f              	cmpb	#15
2048  055e 2708              	beq	L546
2050  0560 c133              	cmpb	#51
2051  0562 2704              	beq	L546
2053  0564 c13f              	cmpb	#63
2054  0566 2604              	bne	L346
2055  0568                   L546:
2056                         ; 1385 						autostart_check_complete_b = TRUE;
2058  0568 1c000508          	bset	_main_flag2_c,8
2059  056c                   L346:
2060                         ; 1394 					if (/*canio_RemSt_configStat_c == FALSE*/autostart_check_complete_b == TRUE)
2062  056c 1f00050806        	brclr	_main_flag2_c,8,L326
2063                         ; 1397 						main_state_c = NORMAL;
2065  0571 79000c            	clr	_main_state_c
2066                         ; 1403 						main_RemSt_configDelay_c = 0;
2068  0574 790000            	clr	L365_main_RemSt_configDelay_c
2070  0577                   L326:
2071                         ; 1421 }
2074  0577 3d                	rts	
2130                         ; 1443 void near mainF_Finish_Mcu_Test(main_mcu_test_type_e test_type)
2130                         ; 1444 {
2131                         	switch	.text
2132  0578                   _mainF_Finish_Mcu_Test:
2134  0578 3b                	pshd	
2135       00000000          OFST:	set	0
2138                         ; 1446 	if (test_type == MAIN_RAM_TEST) {
2140  0579 046106            	tbne	b,L707
2141                         ; 1448 		RAM_is_checked_b = TRUE;
2143  057c 1c000604          	bset	_main_flag_c,4
2145  0580 2007              	bra	L117
2146  0582                   L707:
2147                         ; 1452 		if (test_type == MAIN_ROM_TEST) {
2149  0582 042104            	dbne	b,L117
2150                         ; 1454 			ROM_is_checked_b = TRUE;
2152  0585 1c000608          	bset	_main_flag_c,8
2154  0589                   L117:
2155                         ; 1460 }
2158  0589 31                	puly	
2159  058a 3d                	rts	
2204                         ; 1484 u_16Bit near mainF_Calc_PWM(u_8Bit Duty_cycle_c)
2204                         ; 1485 {
2205                         	switch	.text
2206  058b                   _mainF_Calc_PWM:
2208  058b 3b                	pshd	
2209  058c 1b9b              	leas	-5,s
2210       00000005          OFST:	set	5
2213                         ; 1491 	main_duty_cycle_c = Duty_cycle_c;
2215  058e 6b82              	stab	OFST-3,s
2216                         ; 1494 	if ( (main_duty_cycle_c != MAIN_ZERO_PWM) && (main_duty_cycle_c <= MAIN_N100) ) {
2218  0590 2722              	beq	L737
2220  0592 c164              	cmpb	#100
2221  0594 221e              	bhi	L737
2222                         ; 1497 		if (main_duty_cycle_c != MAIN_N100) {
2224  0596 2715              	beq	L147
2225                         ; 1502 			main_pwm_w = ( (MAIN_N327*main_duty_cycle_c) + ((main_duty_cycle_c*MAIN_N68)/MAIN_N100) );
2227  0598 8644              	ldaa	#68
2228  059a 12                	mul	
2229  059b ce0064            	ldx	#100
2230  059e 1815              	idivs	
2231  05a0 6e80              	stx	OFST-5,s
2232  05a2 e682              	ldab	OFST-3,s
2233  05a4 87                	clra	
2234  05a5 cd0147            	ldy	#327
2235  05a8 13                	emul	
2236  05a9 e380              	addd	OFST-5,s
2238  05ab 2003              	bra	LC012
2239  05ad                   L147:
2240                         ; 1506 			main_pwm_w = MAIN_MAX_PWM;
2242  05ad cc8000            	ldd	#-32768
2243  05b0                   LC012:
2244  05b0 6c83              	std	OFST-2,s
2245  05b2 2002              	bra	L547
2246  05b4                   L737:
2247                         ; 1511 		main_pwm_w = MAIN_ZERO_PWM;
2249  05b4 87                	clra	
2250  05b5 c7                	clrb	
2251  05b6                   L547:
2252                         ; 1513 	return(main_pwm_w);
2256  05b6 1b87              	leas	7,s
2257  05b8 3d                	rts	
2295                         ; 1527 void near mainF_OFF_NonSupported_Outputs_ServiceParts(void)
2295                         ; 1528 {
2296                         	switch	.text
2297  05b9                   _mainF_OFF_NonSupported_Outputs_ServiceParts:
2301                         ; 1530 	if(ecucfg_update_b == TRUE)
2303  05b9 1f00004056        	brclr	_canio_status_flags_c,64,L757
2304                         ; 1533 		if((CSWM_config_c & CSWM_4HS_VARIANT_MASK_K) != CSWM_4HS_VARIANT_MASK_K)
2306  05be 1e00000f04        	brset	_CSWM_config_c,15,L167
2307                         ; 1535 			hsF_RearSeat_TurnOFF();
2309  05c3 4a000000          	call	f_hsF_RearSeat_TurnOFF
2311  05c7                   L167:
2312                         ; 1538 		if ((CSWM_config_c & CSWM_2VS_MASK_K) != CSWM_2VS_MASK_K)
2314  05c7 1e0000302a        	brset	_CSWM_config_c,48,L367
2315                         ; 1541 			vsF_Clear( (u_8Bit) VS_FL);
2317  05cc 87                	clra	
2318  05cd c7                	clrb	
2319  05ce 4a000000          	call	f_vsF_Clear
2321                         ; 1542 			vs_Vsense_c[VS_FL] = 0;           // Clear Vent voltage reading
2323  05d2 c7                	clrb	
2324  05d3 7b0000            	stab	_vs_Vsense_c
2325                         ; 1543 			vs_Isense_c[VS_FL] = 0;           // Clear Vent current reading
2327  05d6 7b0000            	stab	_vs_Isense_c
2328                         ; 1544 			vsF_LED_Display ( (u_8Bit) VS_FL);
2330  05d9 87                	clra	
2331  05da 4a000000          	call	f_vsF_LED_Display
2333                         ; 1546 			vsF_Clear( (u_8Bit) VS_FR);
2335  05de cc0001            	ldd	#1
2336  05e1 4a000000          	call	f_vsF_Clear
2338                         ; 1547 			vs_Vsense_c[VS_FR] = 0;           // Clear Vent voltage reading
2340  05e5 790001            	clr	_vs_Vsense_c+1
2341                         ; 1548 			vs_Isense_c[VS_FR] = 0;           // Clear Vent current reading
2343  05e8 790001            	clr	_vs_Isense_c+1
2344                         ; 1550 			vsF_LED_Display ( (u_8Bit) VS_FR);
2346  05eb cc0001            	ldd	#1
2347  05ee 4a000000          	call	f_vsF_LED_Display
2349                         ; 1552 			vsF_Output_Cntrl();
2351  05f2 4a000000          	call	f_vsF_Output_Cntrl
2353  05f6                   L367:
2354                         ; 1555 		if ((CSWM_config_c & CSWM_HSW_MASK_K) != CSWM_HSW_MASK_K)
2356  05f6 1e00004015        	brset	_CSWM_config_c,64,L567
2357                         ; 1558 			stWF_Clear();
2359  05fb 4a000000          	call	f_stWF_Clear
2361                         ; 1560 			stW_Vsense_HS_c = 0;         // clear steering wheel voltage reading high side
2363  05ff 790000            	clr	_stW_Vsense_HS_c
2364                         ; 1561 			stW_Vsense_LS_c = 0;         // Clear steering wheel voltage reading low side
2366  0602 790000            	clr	_stW_Vsense_LS_c
2367                         ; 1562 			stW_Isense_c = 0;            // Clear steering wheel current reading low side
2369  0605 790000            	clr	_stW_Isense_c
2370                         ; 1564 			stWF_LED_Display();
2372  0608 4a000000          	call	f_stWF_LED_Display
2374                         ; 1566 			stWF_Output_Control();
2376  060c 4a000000          	call	f_stWF_Output_Control
2378  0610                   L567:
2379                         ; 1569 		ecucfg_update_b = FALSE;
2381  0610 1d000040          	bclr	_canio_status_flags_c,64
2382  0614                   L757:
2383                         ; 1572 }
2386  0614 3d                	rts	
2604                         	xdef	_main
2605                         	xdef	_mainLF_App_RamTst
2606                         	xdef	_mainF_TimeSliceGen
2607                         	switch	.bss
2608  0014                   _main_AmbTemp_RemStart_Vent_c:
2609  0014 00                	ds.b	1
2610                         	xdef	_main_AmbTemp_RemStart_Vent_c
2611  0015                   _main_AmbTemp_RemStart_Heat_c:
2612  0015 00                	ds.b	1
2613                         	xdef	_main_AmbTemp_RemStart_Heat_c
2614  0016                   _main_wheel_delay_c:
2615  0016 00                	ds.b	1
2616                         	xdef	_main_wheel_delay_c
2617  0017                   _main_CSWM_stat_delay_c:
2618  0017 00                	ds.b	1
2619                         	xdef	_main_CSWM_stat_delay_c
2620  0018                   _main_PCB_temp_stat_c:
2621  0018 00                	ds.b	1
2622                         	xdef	_main_PCB_temp_stat_c
2623  0019                   _main_ldShd_mnt_stat_c:
2624  0019 00                	ds.b	1
2625                         	xdef	_main_ldShd_mnt_stat_c
2626  001a                   _main_BatteryV_stat_c:
2627  001a 00                	ds.b	1
2628                         	xdef	_main_BatteryV_stat_c
2629  001b                   _main_SystemV_stat_c:
2630  001b 00                	ds.b	1
2631                         	xdef	_main_SystemV_stat_c
2632  001c                   _main_InitClock_stat:
2633  001c 00                	ds.b	1
2634                         	xdef	_main_InitClock_stat
2635  001d                   _main_timeSlice_counter_c:
2636  001d 00                	ds.b	1
2637                         	xdef	_main_timeSlice_counter_c
2638  001e                   _main_timeSlice_request_c:
2639  001e 00                	ds.b	1
2640                         	xdef	_main_timeSlice_request_c
2641                         	xdef	_mainLF_Variant_Select
2642                         	xdef	_mainF_RemSt_Manager
2643                         	xdef	_mainLF_Init
2644                         	xref	_RamTst_MainFunction
2645                         	xref	_RamTst_GetTestResult
2646                         	xref	_RamTst_Init
2647                         	xref	_RamTst_ExecutionStatus
2648                         	xref	_Port_Init
2649                         	xref	_Port_Config
2650                         	xref	f_Pwm_SetDutyCycle
2651                         	xref	_Pwm_Init
2652                         	xref	_Pwm_Config
2653                         	xref	_Mcu_Config
2654                         	xref	_Mcu_InitClock
2655                         	xref	_Mcu_Init
2656                         	xref	_Gpt_EnableNotification
2657                         	xref	_Gpt_StartTimer
2658                         	xref	_Gpt_Init
2659                         	xref	_Gpt_Config
2660                         	xref	_DioConfigData
2661                         	xref	_Dio_PortRead_Ptr
2662                         	xref	_Dio_PortWrite_Ptr
2663                         	xref	_Adc_Init
2664                         	xref	_Adc_Config
2665                         	xref	f_EE_LoadDefaults
2666                         	xref	f_EE_BlockRead
2667                         	xref	f_EE_BlockWrite
2668                         	xref	f_EE_Manager
2669                         	xref	f_EE_PowerupInit
2670                         	xref	f_nwmngF_State_Transition
2671                         	xref	f_nmCan_Start_IL
2672                         	xref	f_nmCan_Cyclic_Tasks
2673                         	xref	f_nwmngF_Init
2674                         	xref	f_IntFlt_F_Chck_Osci_Err
2675                         	xref	f_IntFlt_F_Clr_Ram_Err
2676                         	xref	f_IntFlt_F_Chck_Ram_Err
2677                         	xref	f_IntFlt_F_Chck_PLL
2678                         	xref	f_IntFlt_F_DTC_Monitor
2679                         	xref	f_IntFlt_F_Init
2680                         	xref	_int_flt_byte0
2681                         	xref	f_TempF_U12S_ADC
2682                         	xref	f_TempF_Get_PCB_NTC_Stat
2683                         	xref	f_TempF_PCB_NTC_Flt_Monitor
2684                         	xref	f_TempF_HS_NTC_Flt_Monitor
2685                         	xref	f_TempF_Monitor_ADC
2686                         	xref	f_TempF_PCB_ADC
2687                         	xref	f_TempF_Init
2688                         	xref	_ROMTstF_Clr_Rom_Err
2689                         	xref	_ROMTstF_Chck_Err
2690                         	xref	_ROMTstF_Calc_ChckSum
2691                         	xref	_ROMTstF_Init
2692                         	xref	_ROMTst_status
2693                         	xref	_ADF_Init
2694                         	xref	f_vsF_LED_Display
2695                         	xref	f_vsF_Clear
2696                         	xref	f_vsF_DTC_Manager
2697                         	xref	f_vsF_Fault_Mntr
2698                         	xref	f_vsF_Manager
2699                         	xref	f_vsF_Output_Cntrl
2700                         	xref	f_vsF_ADC_Monitor
2701                         	xref	f_vsF_Init
2702                         	xref	_vs_Isense_c
2703                         	xref	_vs_Vsense_c
2704                         	xref	f_stWF_CurrentLimit_Timer
2705                         	xref	f_stWF_LED_Display
2706                         	xref	f_stWF_Clear
2707                         	xref	f_stWF_DTC_Manager
2708                         	xref	f_stWF_AD_Rdng
2709                         	xref	f_stWF_Flt_Mntr
2710                         	xref	f_stWF_Timer
2711                         	xref	f_stWF_Init
2712                         	xref	f_stWF_Output_Control
2713                         	xref	f_stWF_Manager
2714                         	xref	_stW_Isense_c
2715                         	xref	_stW_Vsense_LS_c
2716                         	xref	_stW_Vsense_HS_c
2717                         	xref	f_BattVF_Get_LdShd_Mnt_Stat
2718                         	xref	f_BattVF_DTC_Manager
2719                         	xref	f_BattVF_Get_Status
2720                         	xref	f_BattVF_AD_Rdg
2721                         	xref	f_BattVF_Manager
2722                         	xref	f_BattVF_Init
2723                         	xref	f_FMemLibF_CheckDtcConditionTimer
2724                         	xref	f_FMemLibF_NMFailSts
2725                         	xref	f_FMemLibF_CyclicDtcTask
2726                         	xref	f_FMemLibF_Init
2727                         	xref	f_rlF_RelayDTC
2728                         	xref	f_rlF_RelayManager
2729                         	xref	f_rlF_InitRelay
2730                         	xref	_relay_control2_c
2731                         	xref	f_hsF_RearSeat_TurnOFF
2732                         	xref	f_hsF_AD_Readings
2733                         	xref	f_hsF_RearSwitchStuckDTC
2734                         	xref	f_hsF_NTCDTC
2735                         	xref	f_hsF_HeatDTC
2736                         	xref	f_hsF_HeatRequestToFet
2737                         	xref	f_hsF_MonitorHeatTimes
2738                         	xref	f_hsF_HeatManager
2739                         	xref	f_hsF_Init
2740                         	xref	_CSWM_config_c
2741                         	xref	f_diagF_DCX_OutputTest
2742                         	xref	_diag_io_lock3_flags_c
2743                         	xref	_ru_eep_PROXI_Cfg
2744                         	xref	f_diagF_CyclicWriteTask
2745                         	xref	f_diagF_Init
2746                         	xref	_diag_rc_lock_flags_c
2747                         	xref	f_canF_CyclicManIndFunc
2748                         	xref	f_canF_CyclicRemoteStart
2749                         	xref	f_canF_CyclicTask
2750                         	xref	f_canF_Init
2751                         	xref	_canio_RemSt_configStat_c
2752                         	xref	_canio_RX_IgnRun_RemSt_c
2753                         	xref	_canio_RX_ExternalTemperature_c
2754                         	xref	_canio_RX_EngineSpeed_c
2755                         	xref	_canio_RX_IGN_STATUS_c
2756                         	xref	_canio_status2_flags_c
2757                         	xref	_canio_status_flags_c
2758                         	xdef	_mainF_OFF_NonSupported_Outputs_ServiceParts
2759                         	xdef	_mainF_CSWM_Status
2760                         	xdef	_mainF_Finish_Mcu_Test
2761                         	xdef	_mainF_Calc_PWM
2762                         	xdef	_main_variant_select_c
2763                         	xdef	_main_ecu_seat_cfg_c
2764                         	xdef	_main_ecu_cfg_for_v7_c
2765                         	xdef	_main_EngRpm_lowLmt_c
2766                         	xdef	_main_RAM_test_rslt
2767                         	xdef	_main_flag2_c
2768                         	xdef	_main_flag_c
2769                         	xdef	_hardware_proxi_mismatch_c
2770                         	xdef	_main_SwReq_stW_c
2771                         	xdef	_main_RemSt_delay_c
2772                         	xdef	_main_SwReq_Rear_c
2773                         	xdef	_main_SwReq_Front_c
2774                         	xdef	_main_state_c
2775                         	xdef	_main_RearSeat_Status_c
2776                         	xdef	_main_FrontSeat_Status_c
2777                         	xdef	_main_ECU_reset_rq_c
2778                         	xdef	_main_CSWM_Func_State_c
2779                         	xdef	_main_CSWM_Status_c
2780                         	xdef	_main_prms
2801                         	end
