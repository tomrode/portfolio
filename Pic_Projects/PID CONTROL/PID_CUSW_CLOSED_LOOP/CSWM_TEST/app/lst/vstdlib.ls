   1                         ; C Compiler for S12XE [COSMIC Software].
   2                         ; Generator V4.7.8 - 27 Jul 2007
   3                         ; Optimizer V4.6.13 - 27 Jul 2007
   4                         .const:	section	.data
   5  0000                   _kVStdMainVersion:
   6  0000 01                	dc.b	1
   7  0001                   _kVStdSubVersion:
   8  0001 17                	dc.b	23
   9  0002                   _kVStdReleaseVersion:
  10  0002 00                	dc.b	0
  44                         ; 310 VSTD_API_0 void VSTD_API_1 VStdInitPowerOn(void)
  44                         ; 311 {
  45                         .ftext:	section	.text
  46  0000                   f_VStdInitPowerOn:
  50                         ; 315   vstdInterruptOldStatus = (tVStdIrqStateType)0;
  52  0000 790002            	clr	_vstdInterruptOldStatus
  53                         ; 318   vstdInterruptCounter = (vsintx)0;
  55  0003 18790000          	clrw	L3_vstdInterruptCounter
  56                         ; 322 }
  59  0007 0a                	rtc	
 110                         ; 412 VSTD_API_0 void VSTD_API_1 VStdRomMemCpy(void *pDest, V_MEMROM1 void V_MEMROM2 V_MEMROM3 *pSrc, vuint16 nCnt) VSTD_API_2
 110                         ; 413 {  
 111                         	switch	.ftext
 112  0008                   f_VStdRomMemCpy:
 114  0008 3b                	pshd	
 115       00000000          OFST:	set	0
 118                         ; 462     VStdAssert(pSrc!=V_NULL, kVStdErrorMemCpyInvalidParameter);
 121  0009 200d              	bra	L15
 122  000b                   L74:
 123                         ; 465       nCnt--;
 125  000b 186387            	decw	OFST+7,s
 126                         ; 466       ((vuint8*)pDest)[nCnt] = ((V_MEMROM1 vuint8 V_MEMROM2 V_MEMROM3 *)pSrc)[nCnt];
 128  000e ec87              	ldd	OFST+7,s
 129  0010 ed80              	ldy	OFST+0,s
 130  0012 ee85              	ldx	OFST+5,s
 131  0014 180ae6ee          	movb	d,x,d,y
 132  0018                   L15:
 133                         ; 463     while(nCnt > (vuint16)0x0000)
 135  0018 ec87              	ldd	OFST+7,s
 136  001a 26ef              	bne	L74
 137                         ; 469 }
 140  001c 31                	puly	
 141  001d 0a                	rtc	
 192                         ; 485 VSTD_API_0 void VSTD_API_1 VStdRamMemCpy16(void *pDest, void *pSrc, vuint16 nWordCnt) VSTD_API_2
 192                         ; 486 {
 193                         	switch	.ftext
 194  001e                   f_VStdRamMemCpy16:
 196  001e 3b                	pshd	
 197       00000000          OFST:	set	0
 200                         ; 489     VStdAssert(pSrc!=V_NULL, kVStdErrorMemCpyInvalidParameter);
 203  001f 200e              	bra	L301
 204  0021                   L101:
 205                         ; 492       nWordCnt--;
 207  0021 186387            	decw	OFST+7,s
 208                         ; 493       ((vuint16*)pDest)[nWordCnt] = ((vuint16*)pSrc)[nWordCnt];
 210  0024 ec87              	ldd	OFST+7,s
 211  0026 59                	lsld	
 212  0027 ed80              	ldy	OFST+0,s
 213  0029 ee85              	ldx	OFST+5,s
 214  002b 1802e6ee          	movw	d,x,d,y
 215  002f                   L301:
 216                         ; 490     while(nWordCnt > (vuint16)0x0000)
 218  002f ec87              	ldd	OFST+7,s
 219  0031 26ee              	bne	L101
 220                         ; 495 }
 223  0033 31                	puly	
 224  0034 0a                	rtc	
 275                         ; 511 VSTD_API_0 void VSTD_API_1 VStdRamMemCpy32(void *pDest, void *pSrc, vuint16 nDWordCnt) VSTD_API_2
 275                         ; 512 {
 276                         	switch	.ftext
 277  0035                   f_VStdRamMemCpy32:
 279  0035 3b                	pshd	
 280       00000000          OFST:	set	0
 283                         ; 515     VStdAssert(pSrc!=V_NULL, kVStdErrorMemCpyInvalidParameter);
 286  0036 2017              	bra	L531
 287  0038                   L331:
 288                         ; 518       nDWordCnt--;
 290  0038 186387            	decw	OFST+7,s
 291                         ; 519       ((vuint32*)pDest)[nDWordCnt] = ((vuint32*)pSrc)[nDWordCnt];
 293  003b ed80              	ldy	OFST+0,s
 294  003d ec87              	ldd	OFST+7,s
 295  003f 59                	lsld	
 296  0040 59                	lsld	
 297  0041 19ee              	leay	d,y
 298  0043 ee85              	ldx	OFST+5,s
 299  0045 1ae6              	leax	d,x
 300  0047 18020040          	movw	0,x,0,y
 301  004b 18020242          	movw	2,x,2,y
 302  004f                   L531:
 303                         ; 516     while(nDWordCnt > (vuint16)0x0000)
 305  004f ec87              	ldd	OFST+7,s
 306  0051 26e5              	bne	L331
 307                         ; 521 }
 310  0053 31                	puly	
 311  0054 0a                	rtc	
 347                         ; 767 VSTD_API_0 void VSTD_API_1 VStdSuspendAllInterrupts(void) VSTD_API_2
 347                         ; 768 {
 348                         	switch	.ftext
 349  0055                   f_VStdSuspendAllInterrupts:
 351  0055 37                	pshb	
 352       00000001          OFST:	set	1
 355                         ; 775   if(vstdInterruptCounter == 0) 
 358  0056 fc0000            	ldd	L3_vstdInterruptCounter
 359  0059 260b              	bne	L551
 360                         ; 777     VStdGlobalInterruptDisable();
 362  005b b774              	tfr	s,d
 363  005d 4a008181          	call	f_VStdLL_GlobalInterruptDisable
 365                         ; 778     VStdGetGlobalInterruptOldStatus(vstdInterruptOldStatus);
 367  0061 180d800002        	movb	OFST-1,s,_vstdInterruptOldStatus
 368  0066                   L551:
 369                         ; 780   vstdInterruptCounter++;
 371  0066 18720000          	incw	L3_vstdInterruptCounter
 372                         ; 782 }
 375  006a 1b81              	leas	1,s
 376  006c 0a                	rtc	
 412                         ; 794 VSTD_API_0 void VSTD_API_1 VStdResumeAllInterrupts(void) VSTD_API_2
 412                         ; 795 {
 413                         	switch	.ftext
 414  006d                   f_VStdResumeAllInterrupts:
 416  006d 37                	pshb	
 417       00000001          OFST:	set	1
 420                         ; 802   vstdInterruptCounter--;
 423  006e 18730000          	decw	L3_vstdInterruptCounter
 424                         ; 803   if(vstdInterruptCounter == 0)
 426  0072 260a              	bne	L371
 427                         ; 805     VStdPutGlobalInterruptOldStatus(vstdInterruptOldStatus);
 429  0074 f60002            	ldab	_vstdInterruptOldStatus
 430  0077 6b80              	stab	OFST-1,s
 431                         ; 806     VStdGlobalInterruptRestore();
 433  0079 87                	clra	
 434  007a 4a008c8c          	call	f_VStdLL_GlobalInterruptRestore
 436  007e                   L371:
 437                         ; 809 }
 440  007e 1b81              	leas	1,s
 441  0080 0a                	rtc	
 476                         ; 831 VSTD_API_0 void VSTD_API_1 VStdLL_GlobalInterruptDisable(tVStdIrqStateType* pOldState) VSTD_API_2
 476                         ; 832 {
 477                         	switch	.ftext
 478  0081                   f_VStdLL_GlobalInterruptDisable:
 482                         ; 838   pshy      ; save index register
 485  0081 35                	pshy	
 487                         ; 839   tfr d,y   ; transfer pointer to index register
 490  0082 b746              	tfr	d,y
 492                         ; 840   tpa       ; load ccr to accumulator
 495  0084 b720              	tpa	
 497                         ; 841   sei       ; disable global interrupt
 500  0086 1410              	sei	
 502                         ; 842   staa 0,y  ; store the value of ccr into loction pointed by pOldState
 505  0088 6a40              	staa	0,y
 507                         ; 843   puly      ; restore index register
 510  008a 31                	puly	
 512                         ; 848 }
 515  008b 0a                	rtc	
 547                         ; 858 VSTD_API_0 void VSTD_API_1 VStdLL_GlobalInterruptRestore(tVStdIrqStateType nOldState) VSTD_API_2
 547                         ; 859 {
 548                         	switch	.ftext
 549  008c                   f_VStdLL_GlobalInterruptRestore:
 553                         ; 863   tba       ; b to a
 556  008c 180f              	tba	
 558                         ; 864   tap       ; load accumulator to ccr
 561  008e b702              	tap	
 563                         ; 868 }
 566  0090 0a                	rtc	
 624                         	switch	.bss
 625  0000                   L3_vstdInterruptCounter:
 626  0000 0000              	ds.b	2
 627  0002                   _vstdInterruptOldStatus:
 628  0002 00                	ds.b	1
 629                         	xdef	_vstdInterruptOldStatus
 630                         	xdef	f_VStdLL_GlobalInterruptRestore
 631                         	xdef	f_VStdLL_GlobalInterruptDisable
 632                         	xdef	f_VStdResumeAllInterrupts
 633                         	xdef	f_VStdSuspendAllInterrupts
 634                         	xdef	f_VStdRamMemCpy32
 635                         	xdef	f_VStdRamMemCpy16
 636                         	xdef	f_VStdRomMemCpy
 637                         	xdef	f_VStdInitPowerOn
 638                         	xdef	_kVStdReleaseVersion
 639                         	xdef	_kVStdSubVersion
 640                         	xdef	_kVStdMainVersion
 661                         	end
