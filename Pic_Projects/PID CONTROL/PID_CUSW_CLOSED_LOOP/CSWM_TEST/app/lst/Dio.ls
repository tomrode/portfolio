   1                         ; C Compiler for S12XE [COSMIC Software].
   2                         ; Generator V4.7.8 - 27 Jul 2007
   3                         ; Optimizer V4.6.13 - 27 Jul 2007
   4                         .const:	section	.data
   5  0000                   _Dio_PortWrite_Ptr:
   6  0000 0000              	dc.w	__PORTAB
   7  0002 0001              	dc.w	__PORTAB+1
   8  0004 0000              	dc.w	_Dio_Dummy
   9  0006 0000              	dc.w	_Dio_Dummy
  10  0008 0000              	dc.w	__PORTE
  11  000a 0000              	dc.w	__PTH
  12  000c 0000              	dc.w	__PTJ
  13  000e 0000              	dc.w	__PORTK
  14  0010 0000              	dc.w	__PTM
  15  0012 0000              	dc.w	__PTP
  16  0014 0000              	dc.w	__PTS
  17  0016 0000              	dc.w	__PTT
  18  0018 0001              	dc.w	__PT01AD0+1
  19  001a 0000              	dc.w	__PT01AD0
  20  001c 0000              	dc.w	_Dio_Dummy
  21  001e                   _Dio_PortRead_Ptr:
  22  001e 0000              	dc.w	__PORTAB
  23  0020 0001              	dc.w	__PORTAB+1
  24  0022 0000              	dc.w	_Dio_Dummy
  25  0024 0000              	dc.w	_Dio_Dummy
  26  0026 0000              	dc.w	__PORTE
  27  0028 0000              	dc.w	__PTIH
  28  002a 0000              	dc.w	__PTIJ
  29  002c 0000              	dc.w	__PORTK
  30  002e 0000              	dc.w	__PTIM
  31  0030 0000              	dc.w	__PTIP
  32  0032 0000              	dc.w	__PTIS
  33  0034 0000              	dc.w	__PTIT
  34  0036 0001              	dc.w	__PT01AD0+1
  35  0038 0000              	dc.w	__PT01AD0
  36  003a 0000              	dc.w	_Dio_Dummy
 123                         ; 206 void Dio_GetVersionInfo(Std_VersionInfoType *versioninfo)
 123                         ; 207 {
 124                         	switch	.text
 125  0000                   _Dio_GetVersionInfo:
 127       00000000          OFST:	set	0
 130                         ; 209     if(NULL_PTR != versioninfo)
 132  0000 6cae              	std	2,-s
 133  0002 2713              	beq	L74
 134                         ; 212     versioninfo->moduleID       = DIO_MODULE_ID;
 136  0004 c678              	ldab	#120
 137  0006 ed80              	ldy	OFST+0,s
 138  0008 6b42              	stab	2,y
 139                         ; 213     versioninfo->vendorID       = DIO_VENDOR_ID;
 141  000a cc0028            	ldd	#40
 142  000d 6c40              	std	0,y
 143                         ; 214     versioninfo->sw_major_version= DIO_SW_MAJOR_VERSION_C;
 145  000f c603              	ldab	#3
 146  0011 6b43              	stab	3,y
 147                         ; 215     versioninfo->sw_minor_version= DIO_SW_MINOR_VERSION_C;
 149  0013 6944              	clr	4,y
 150                         ; 216     versioninfo->sw_patch_version= DIO_SW_PATCH_VERSION_C;
 152  0015 6b45              	stab	5,y
 153  0017                   L74:
 154                         ; 219 }
 157  0017 31                	puly	
 158  0018 3d                	rts	
 201                         	xdef	_Dio_GetVersionInfo
 202                         	xdef	_Dio_PortRead_Ptr
 203                         	xdef	_Dio_PortWrite_Ptr
 204                         	switch	.bss
 205  0000                   _Dio_Dummy:
 206  0000 00                	ds.b	1
 207                         	xdef	_Dio_Dummy
 208                         	xref	__PT01AD0
 209                         	xref	__PTIJ
 210                         	xref	__PTJ
 211                         	xref	__PTIH
 212                         	xref	__PTH
 213                         	xref	__PTIP
 214                         	xref	__PTP
 215                         	xref	__PTIM
 216                         	xref	__PTM
 217                         	xref	__PTIS
 218                         	xref	__PTS
 219                         	xref	__PTIT
 220                         	xref	__PTT
 221                         	xref	__PORTK
 222                         	xref	__PORTE
 223                         	xref	__PORTAB
 244                         	end
