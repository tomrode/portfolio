   1                         ; C Compiler for S12XE [COSMIC Software].
   2                         ; Generator V4.7.8 - 27 Jul 2007
   3                         ; Optimizer V4.6.13 - 27 Jul 2007
  65                         ; 112 void MEMFUNC_CALL MemSet( unsigned int destination, unsigned int size, unsigned char value )
  65                         ; 113 {
  66                         .ftext:	section	.text
  67  0000                   f_MemSet:
  69  0000 3b                	pshd	
  70  0001 3b                	pshd	
  71       00000002          OFST:	set	2
  74                         ; 116     for (index=0; index < size; index++)
  76  0002 186980            	clrw	OFST-2,s
  78  0005 b746              	tfr	d,y
  79  0007 2007              	bra	L73
  80  0009                   L33:
  81                         ; 118         *((unsigned char*)destination) = value;
  83  0009 180a8a70          	movb	OFST+8,s,1,y+
  84                         ; 119         destination++; 
  86                         ; 116     for (index=0; index < size; index++)
  88  000d 186280            	incw	OFST-2,s
  89  0010                   L73:
  92  0010 ec80              	ldd	OFST-2,s
  93  0012 ac87              	cpd	OFST+5,s
  94  0014 25f3              	blo	L33
  95                         ; 121 }
  98  0016 1b84              	leas	4,s
  99  0018 0a                	rtc	
 130                         ; 132 unsigned char MEMFUNC_CALL GetGPage( unsigned long GlobalAddress )
 130                         ; 133 {
 131                         	switch	.ftext
 132  0019                   f_GetGPage:
 134  0019 3b                	pshd	
 135  001a 34                	pshx	
 136       00000000          OFST:	set	0
 139                         ; 134     return ((S12XGlobalType*)&GlobalAddress)->gpage;
 141  001b e681              	ldab	OFST+1,s
 142  001d c47f              	andb	#127
 145  001f 1b84              	leas	4,s
 146  0021 0a                	rtc	
 177                         ; 145 unsigned char MEMFUNC_CALL GetPPage( unsigned long GlobalAddress )
 177                         ; 146 {
 178                         	switch	.ftext
 179  0022                   f_GetPPage:
 181  0022 3b                	pshd	
 182  0023 34                	pshx	
 183       00000000          OFST:	set	0
 186                         ; 147     return ((S12XFlashType*)&GlobalAddress)->ppage;
 188  0024 ec81              	ldd	OFST+1,s
 189  0026 59                	lsld	
 190  0027 59                	lsld	
 191  0028 b701              	tfr	a,b
 194  002a 1b84              	leas	4,s
 195  002c 0a                	rtc	
 226                         ; 158 unsigned char MEMFUNC_CALL GetRPage( unsigned long GlobalAddress )
 226                         ; 159 {
 227                         	switch	.ftext
 228  002d                   f_GetRPage:
 230  002d 3b                	pshd	
 231  002e 34                	pshx	
 232       00000000          OFST:	set	0
 235                         ; 160     return ((S12XRAMType*)&GlobalAddress)->rpage;
 237  002f ec81              	ldd	OFST+1,s
 238  0031 49                	lsrd	
 239  0032 49                	lsrd	
 240  0033 49                	lsrd	
 241  0034 49                	lsrd	
 244  0035 1b84              	leas	4,s
 245  0037 0a                	rtc	
 276                         ; 171 unsigned char MEMFUNC_CALL GetEPage( unsigned long GlobalAddress )
 276                         ; 172 {
 277                         	switch	.ftext
 278  0038                   f_GetEPage:
 280  0038 3b                	pshd	
 281  0039 34                	pshx	
 282       00000000          OFST:	set	0
 285                         ; 173     return ((S12XEepromType*)&GlobalAddress)->epage;
 287  003a ec81              	ldd	OFST+1,s
 288  003c 49                	lsrd	
 289  003d 49                	lsrd	
 292  003e 1b84              	leas	4,s
 293  0040 0a                	rtc	
 324                         ; 184 unsigned int MEMFUNC_CALL GetGOffset( unsigned long GlobalAddress )
 324                         ; 185 {
 325                         	switch	.ftext
 326  0041                   f_GetGOffset:
 328       00000000          OFST:	set	0
 331                         ; 186     return ((S12XGlobalType*)&GlobalAddress)->localAddr;
 335  0041 0a                	rtc	
 366                         ; 197 unsigned int MEMFUNC_CALL GetPOffset( unsigned long GlobalAddress )
 366                         ; 198 {
 367                         	switch	.ftext
 368  0042                   f_GetPOffset:
 370       00000000          OFST:	set	0
 373                         ; 199     return ((S12XFlashType*)&GlobalAddress)->localOffset;
 375  0042 843f              	anda	#63
 378  0044 0a                	rtc	
 409                         ; 210 unsigned int GetROffset( unsigned long GlobalAddress )
 409                         ; 211 {
 410                         	switch	.ftext
 411  0045                   f_GetROffset:
 413       00000000          OFST:	set	0
 416                         ; 212     return ((S12XRAMType*)&GlobalAddress)->localOffset;
 418  0045 840f              	anda	#15
 421  0047 0a                	rtc	
 452                         ; 223 unsigned int MEMFUNC_CALL GetEOffset( unsigned long GlobalAddress )
 452                         ; 224 {
 453                         	switch	.ftext
 454  0048                   f_GetEOffset:
 456       00000000          OFST:	set	0
 459                         ; 225     return ((S12XEepromType*)&GlobalAddress)->localOffset;
 461  0048 8403              	anda	#3
 464  004a 0a                	rtc	
 513                         ; 236 void MEMFUNC_CALL UniversalMemCopy( unsigned long dest, unsigned long src, unsigned int size )
 513                         ; 237 {
 514                         	switch	.ftext
 515  004b                   f_UniversalMemCopy:
 517  004b 3b                	pshd	
 518  004c 34                	pshx	
 519       00000000          OFST:	set	0
 522                         ; 239     if ( (dest <= LOGICAL_MEMORY_END) &&
 522                         ; 240          (src <= LOGICAL_MEMORY_END) )
 524  004d 8cffff            	cpd	#-1
 525  0050 188e0000          	cpex	#0
 526  0054 221b              	bhi	L322
 528  0056 ec89              	ldd	OFST+9,s
 529  0058 ee87              	ldx	OFST+7,s
 530  005a 8cffff            	cpd	#-1
 531  005d 188e0000          	cpex	#0
 532  0061 220e              	bhi	L322
 533                         ; 242         LogicalMemCopy( (unsigned int)dest, (unsigned int)src, size );
 535  0063 ec8b              	ldd	OFST+11,s
 536  0065 3b                	pshd	
 537  0066 ec8b              	ldd	OFST+11,s
 538  0068 3b                	pshd	
 539  0069 ec86              	ldd	OFST+6,s
 540  006b 4a00dada          	call	f_LogicalMemCopy
 543  006f 2064              	bra	LC001
 544  0071                   L322:
 545                         ; 244     else if ( (dest > LOGICAL_MEMORY_END) &&
 545                         ; 245             (src > LOGICAL_MEMORY_END) )
 547  0071 ec82              	ldd	OFST+2,s
 548  0073 ee80              	ldx	OFST+0,s
 549  0075 8cffff            	cpd	#-1
 550  0078 188e0000          	cpex	#0
 551  007c 2320              	bls	L722
 553  007e ec89              	ldd	OFST+9,s
 554  0080 ee87              	ldx	OFST+7,s
 555  0082 8cffff            	cpd	#-1
 556  0085 188e0000          	cpex	#0
 557  0089 2313              	bls	L722
 558                         ; 248         GlobalMemCopy( dest, src, size );
 560  008b ec8b              	ldd	OFST+11,s
 561  008d 3b                	pshd	
 562  008e ec8b              	ldd	OFST+11,s
 563  0090 3b                	pshd	
 564  0091 34                	pshx	
 565  0092 ec88              	ldd	OFST+8,s
 566  0094 ee86              	ldx	OFST+6,s
 567  0096 4a01d6d6          	call	f_GlobalMemCopy
 569  009a                   LC002:
 570  009a 1b86              	leas	6,s
 572  009c 2039              	bra	L522
 573  009e                   L722:
 574                         ; 250     else if ( (dest < LOGICAL_MEMORY_END) &&
 574                         ; 251          (src > LOGICAL_MEMORY_END) )
 576  009e ec82              	ldd	OFST+2,s
 577  00a0 8cffff            	cpd	#-1
 578  00a3 ee80              	ldx	OFST+0,s
 579  00a5 188e0000          	cpex	#0
 580  00a9 241c              	bhs	L332
 582  00ab ec89              	ldd	OFST+9,s
 583  00ad ee87              	ldx	OFST+7,s
 584  00af 8cffff            	cpd	#-1
 585  00b2 188e0000          	cpex	#0
 586  00b6 230f              	bls	L332
 587                         ; 254         Global2LogicalMemCopy( (unsigned int)dest, src, size );
 589  00b8 ec8b              	ldd	OFST+11,s
 590  00ba 3b                	pshd	
 591  00bb ec8b              	ldd	OFST+11,s
 592  00bd 3b                	pshd	
 593  00be 34                	pshx	
 594  00bf ec88              	ldd	OFST+8,s
 595  00c1 4a011a1a          	call	f_Global2LogicalMemCopy
 598  00c5 20d3              	bra	LC002
 599  00c7                   L332:
 600                         ; 259         Logical2GlobalMemCopy( dest, (unsigned int)src, size );
 602  00c7 ec8b              	ldd	OFST+11,s
 603  00c9 3b                	pshd	
 604  00ca ec8b              	ldd	OFST+11,s
 605  00cc 3b                	pshd	
 606  00cd ec86              	ldd	OFST+6,s
 607  00cf ee84              	ldx	OFST+4,s
 608  00d1 4a017777          	call	f_Logical2GlobalMemCopy
 610  00d5                   LC001:
 611  00d5 1b84              	leas	4,s
 612  00d7                   L522:
 613                         ; 261 }
 616  00d7 1b84              	leas	4,s
 617  00d9 0a                	rtc	
 690                         ; 271 void MEMFUNC_CALL LogicalMemCopy( unsigned int dest, unsigned int src, unsigned int size )
 690                         ; 272 {
 691                         	switch	.ftext
 692  00da                   f_LogicalMemCopy:
 694  00da 3b                	pshd	
 695  00db 1b9a              	leas	-6,s
 696       00000006          OFST:	set	6
 699                         ; 277     destination = (unsigned char*)dest;
 701  00dd 6c82              	std	OFST-4,s
 702                         ; 278     source = (unsigned char*)src;
 704  00df ec8b              	ldd	OFST+5,s
 705  00e1 6c84              	std	OFST-2,s
 706                         ; 280     MEM_SUSPEND_INTERRUPTS
 709                         xref _MemFuncInterrupt
 714                         xref _RPAGE
 719  00e3 3b                	pshd	
 724  00e4 b720              	tpa	
 729  00e6 1410              	sei	
 734  00e8 c600              	ldab	#page(_MemFuncInterrupt)
 739  00ea 7b0000            	stab	_RPAGE
 744  00ed 7a0000            	staa	_MemFuncInterrupt
 749  00f0 3a                	puld	
 751                         ; 281     for (byteindex=0; byteindex<size; byteindex++)
 754  00f1 186980            	clrw	OFST-6,s
 756  00f4 ee84              	ldx	OFST-2,s
 757  00f6 ed82              	ldy	OFST-4,s
 758  00f8 2007              	bra	L572
 759  00fa                   L172:
 760                         ; 283         *destination++ = *source++;
 762  00fa 180a3070          	movb	1,x+,1,y+
 763                         ; 281     for (byteindex=0; byteindex<size; byteindex++)
 765  00fe 186280            	incw	OFST-6,s
 766  0101                   L572:
 769  0101 ec80              	ldd	OFST-6,s
 770  0103 ac8d              	cpd	OFST+7,s
 771  0105 25f3              	blo	L172
 772  0107 6d82              	sty	OFST-4,s
 773  0109 6e84              	stx	OFST-2,s
 774                         ; 285     MEM_RESUME_INTERRUPTS
 777  010b 36                	psha	
 782  010c 8600              	ldaa	#page(_MemFuncInterrupt)
 787  010e 7a0000            	staa	_RPAGE
 792  0111 b60000            	ldaa	_MemFuncInterrupt
 797  0114 b702              	tap	
 802  0116 32                	pula	
 804                         ; 286 }
 808  0117 1b88              	leas	8,s
 809  0119 0a                	rtc	
 878                         ; 295 void MEMFUNC_CALL Global2LogicalMemCopy( unsigned int destination, unsigned long source, unsigned int size )
 878                         ; 296 {
 880                         	switch	.ftext
 881  011a                   f_Global2LogicalMemCopy:
 883  011a 3b                	pshd	
 884  011b 1b9c              	leas	-4,s
 885       00000004          OFST:	set	4
 888                         ; 301     MEM_SUSPEND_INTERRUPTS
 891                         xref _MemFuncInterrupt
 896                         xref _RPAGE
 901  011d 3b                	pshd	
 906  011e b720              	tpa	
 911  0120 1410              	sei	
 916  0122 c600              	ldab	#page(_MemFuncInterrupt)
 921  0124 7b0000            	stab	_RPAGE
 926  0127 7a0000            	staa	_MemFuncInterrupt
 931  012a 3a                	puld	
 933                         ; 303     previousgpage = GPAGE;
 936  012b f60000            	ldab	__GPAGE
 937  012e 6b83              	stab	OFST-1,s
 938                         ; 306     for (byteindex = 0; byteindex < size; byteindex++)
 940  0130 186980            	clrw	OFST-4,s
 942  0133 2028              	bra	L333
 943  0135                   L723:
 944                         ; 308         srcgpage = ((S12XGlobalType*)(&source))->gpage;
 946  0135 e68a              	ldab	OFST+6,s
 947  0137 c47f              	andb	#127
 948  0139 6b82              	stab	OFST-2,s
 949                         ; 310         GPAGE = srcgpage;
 951  013b 7b0000            	stab	__GPAGE
 952                         ; 312         *((unsigned char*)destination) = *((@far @gpage unsigned char *)source);
 954  013e ee8b              	ldx	OFST+7,s
 955  0140 180d8a0010        	movb	OFST+6,s,GPAGE
 956  0145 18e630            	gldab	1,x+
 957  0148 180b000010        	movb	#phigh(__sgconst),GPAGE
 958  014d ed84              	ldy	OFST+0,s
 959  014f 6b70              	stab	1,y+
 960                         ; 313         destination++;
 962  0151 6d84              	sty	OFST+0,s
 963                         ; 314         source++;
 965  0153 6e8b              	stx	OFST+7,s
 966  0155 2603              	bne	L43
 967  0157 186289            	incw	OFST+5,s
 968  015a                   L43:
 969                         ; 306     for (byteindex = 0; byteindex < size; byteindex++)
 971  015a 186280            	incw	OFST-4,s
 972  015d                   L333:
 975  015d ec80              	ldd	OFST-4,s
 976  015f ac8d              	cpd	OFST+9,s
 977  0161 25d2              	blo	L723
 978                         ; 318     GPAGE = previousgpage;
 980  0163 180d830000        	movb	OFST-1,s,__GPAGE
 981                         ; 320     MEM_RESUME_INTERRUPTS
 984  0168 36                	psha	
 989  0169 8600              	ldaa	#page(_MemFuncInterrupt)
 994  016b 7a0000            	staa	_RPAGE
 999  016e b60000            	ldaa	_MemFuncInterrupt
1004  0171 b702              	tap	
1009  0173 32                	pula	
1011                         ; 321 }
1015  0174 1b86              	leas	6,s
1016  0176 0a                	rtc	
1085                         ; 331 void MEMFUNC_CALL Logical2GlobalMemCopy( unsigned long destination, unsigned int source, unsigned int size )
1085                         ; 332 {
1086                         	switch	.ftext
1087  0177                   f_Logical2GlobalMemCopy:
1089  0177 3b                	pshd	
1090  0178 34                	pshx	
1091  0179 1b9c              	leas	-4,s
1092       00000004          OFST:	set	4
1095                         ; 337     MEM_SUSPEND_INTERRUPTS
1098                         xref _MemFuncInterrupt
1103                         xref _RPAGE
1108  017b 3b                	pshd	
1113  017c b720              	tpa	
1118  017e 1410              	sei	
1123  0180 c600              	ldab	#page(_MemFuncInterrupt)
1128  0182 7b0000            	stab	_RPAGE
1133  0185 7a0000            	staa	_MemFuncInterrupt
1138  0188 3a                	puld	
1140                         ; 339     previousgpage = GPAGE;
1143  0189 f60000            	ldab	__GPAGE
1144  018c 6b83              	stab	OFST-1,s
1145                         ; 342     for (byteindex = 0; byteindex < size; byteindex++)
1147  018e 186980            	clrw	OFST-4,s
1149  0191 2029              	bra	L173
1150  0193                   L563:
1151                         ; 344         destgpage = ((S12XGlobalType*)(&destination))->gpage;
1153  0193 e685              	ldab	OFST+1,s
1154  0195 c47f              	andb	#127
1155  0197 6b82              	stab	OFST-2,s
1156                         ; 346         GPAGE = destgpage;
1158  0199 7b0000            	stab	__GPAGE
1159                         ; 348         *((@far @gpage unsigned char*)destination) = *((unsigned char *)source);
1161  019c ee86              	ldx	OFST+2,s
1162  019e 180d850010        	movb	OFST+1,s,GPAGE
1163  01a3 ed8b              	ldy	OFST+7,s
1164  01a5 e640              	ldab	0,y
1165  01a7 186b30            	gstab	1,x+
1166  01aa 180b000010        	movb	#phigh(__sgconst),GPAGE
1167                         ; 349         destination++;
1169  01af 6e86              	stx	OFST+2,s
1170  01b1 2603              	bne	L04
1171  01b3 186284            	incw	OFST+0,s
1172  01b6                   L04:
1173                         ; 350         source++;
1175  01b6 02                	iny	
1176  01b7 6d8b              	sty	OFST+7,s
1177                         ; 342     for (byteindex = 0; byteindex < size; byteindex++)
1179  01b9 186280            	incw	OFST-4,s
1180  01bc                   L173:
1183  01bc ec80              	ldd	OFST-4,s
1184  01be ac8d              	cpd	OFST+9,s
1185  01c0 25d1              	blo	L563
1186                         ; 354     GPAGE = previousgpage;
1188  01c2 180d830000        	movb	OFST-1,s,__GPAGE
1189                         ; 356     MEM_RESUME_INTERRUPTS
1192  01c7 36                	psha	
1197  01c8 8600              	ldaa	#page(_MemFuncInterrupt)
1202  01ca 7a0000            	staa	_RPAGE
1207  01cd b60000            	ldaa	_MemFuncInterrupt
1212  01d0 b702              	tap	
1217  01d2 32                	pula	
1219                         ; 358 }
1223  01d3 1b88              	leas	8,s
1224  01d5 0a                	rtc	
1306                         ; 369 void MEMFUNC_CALL GlobalMemCopy( unsigned long destination, unsigned long source, unsigned int size )
1306                         ; 370 {
1307                         	switch	.ftext
1308  01d6                   f_GlobalMemCopy:
1310  01d6 3b                	pshd	
1311  01d7 34                	pshx	
1312  01d8 1b9b              	leas	-5,s
1313       00000005          OFST:	set	5
1316                         ; 377     MEM_SUSPEND_INTERRUPTS
1319                         xref _MemFuncInterrupt
1324                         xref _RPAGE
1329  01da 3b                	pshd	
1334  01db b720              	tpa	
1339  01dd 1410              	sei	
1344  01df c600              	ldab	#page(_MemFuncInterrupt)
1349  01e1 7b0000            	stab	_RPAGE
1354  01e4 7a0000            	staa	_MemFuncInterrupt
1359  01e7 3a                	puld	
1361                         ; 379     previousgpage = GPAGE;
1364  01e8 f60000            	ldab	__GPAGE
1365  01eb 6b84              	stab	OFST-1,s
1366                         ; 381     for (byteindex = 0; byteindex < size; byteindex++)
1368  01ed 186981            	clrw	OFST-4,s
1370  01f0 204a              	bra	L334
1371  01f2                   L724:
1372                         ; 383         srcgpage = ((S12XGlobalType*)(&source))->gpage;
1374  01f2 e68d              	ldab	OFST+8,s
1375  01f4 c47f              	andb	#127
1376  01f6 6b80              	stab	OFST-5,s
1377                         ; 384         destgpage = ((S12XGlobalType*)(&destination))->gpage;
1379  01f8 e686              	ldab	OFST+1,s
1380  01fa c47f              	andb	#127
1381  01fc 6b83              	stab	OFST-2,s
1382                         ; 386         GPAGE = srcgpage;
1384  01fe 180d800000        	movb	OFST-5,s,__GPAGE
1385                         ; 388         tempbyte = *((@far @gpage unsigned char *)source++);
1387  0203 ec8e              	ldd	OFST+9,s
1388  0205 ee8c              	ldx	OFST+7,s
1389  0207 18628e            	incw	OFST+9,s
1390  020a 2603              	bne	L44
1391  020c 18628c            	incw	OFST+7,s
1392  020f                   L44:
1393  020f b7c5              	exg	d,x
1394  0211 7b0010            	stab	GPAGE
1395  0214 18e600            	gldab	0,x
1396  0217 6b80              	stab	OFST-5,s
1397                         ; 390         GPAGE = destgpage;
1399  0219 180d830000        	movb	OFST-2,s,__GPAGE
1400                         ; 391         *(@far @gpage unsigned char *) destination++ = tempbyte;
1402  021e ec87              	ldd	OFST+2,s
1403  0220 ee85              	ldx	OFST+0,s
1404  0222 186287            	incw	OFST+2,s
1405  0225 2603              	bne	L64
1406  0227 186285            	incw	OFST+0,s
1407  022a                   L64:
1408  022a b7c5              	exg	d,x
1409  022c 7b0010            	stab	GPAGE
1410  022f e680              	ldab	OFST-5,s
1411  0231 186b00            	gstab	0,x
1412  0234 180b000010        	movb	#phigh(__sgconst),GPAGE
1413                         ; 381     for (byteindex = 0; byteindex < size; byteindex++)
1415  0239 186281            	incw	OFST-4,s
1416  023c                   L334:
1419  023c ec81              	ldd	OFST-4,s
1420  023e acf010            	cpd	OFST+11,s
1421  0241 25af              	blo	L724
1422                         ; 395     GPAGE = previousgpage;
1424  0243 180d840000        	movb	OFST-1,s,__GPAGE
1425                         ; 397     MEM_RESUME_INTERRUPTS
1428  0248 36                	psha	
1433  0249 8600              	ldaa	#page(_MemFuncInterrupt)
1438  024b 7a0000            	staa	_RPAGE
1443  024e b60000            	ldaa	_MemFuncInterrupt
1448  0251 b702              	tap	
1453  0253 32                	pula	
1455                         ; 398 }
1459  0254 1b89              	leas	9,s
1460  0256 0a                	rtc	
1514                         ; 408 unsigned int MEMFUNC_CALL GlobalRead16( unsigned long longaddr )
1514                         ; 409 {
1515                         	switch	.ftext
1516  0257                   f_GlobalRead16:
1518  0257 3b                	pshd	
1519  0258 34                	pshx	
1520  0259 1b9c              	leas	-4,s
1521       00000004          OFST:	set	4
1524                         ; 413     returnval = 0;
1526  025b a680              	ldaa	OFST-4,s
1527                         ; 414     tempgpage = ((S12XGlobalType*)(&longaddr))->gpage;
1529  025d e685              	ldab	OFST+1,s
1530  025f c47f              	andb	#127
1531  0261 6b83              	stab	OFST-1,s
1532                         ; 416     MEM_SUSPEND_INTERRUPTS
1535                         xref _MemFuncInterrupt
1540                         xref _RPAGE
1545  0263 3b                	pshd	
1550  0264 b720              	tpa	
1555  0266 1410              	sei	
1560  0268 c600              	ldab	#page(_MemFuncInterrupt)
1565  026a 7b0000            	stab	_RPAGE
1570  026d 7a0000            	staa	_MemFuncInterrupt
1575  0270 3a                	puld	
1577                         ; 419     previousgpage = GPAGE;
1580  0271 f60000            	ldab	__GPAGE
1581  0274 6b82              	stab	OFST-2,s
1582                         ; 421     GPAGE = tempgpage;
1584  0276 180d830000        	movb	OFST-1,s,__GPAGE
1585                         ; 422     returnval = *((@far @gpage unsigned int *)longaddr);
1587  027b ee86              	ldx	OFST+2,s
1588  027d 180d850010        	movb	OFST+1,s,GPAGE
1589  0282 18ec00            	gldd	0,x
1590  0285 180b000010        	movb	#phigh(__sgconst),GPAGE
1591  028a 6c80              	std	OFST-4,s
1592                         ; 425     GPAGE = previousgpage;
1594  028c 180d820000        	movb	OFST-2,s,__GPAGE
1595                         ; 426     MEM_RESUME_INTERRUPTS
1598  0291 36                	psha	
1603  0292 8600              	ldaa	#page(_MemFuncInterrupt)
1608  0294 7a0000            	staa	_RPAGE
1613  0297 b60000            	ldaa	_MemFuncInterrupt
1618  029a b702              	tap	
1623  029c 32                	pula	
1625                         ; 428     return returnval;
1628  029d ec80              	ldd	OFST-4,s
1631  029f 1b88              	leas	8,s
1632  02a1 0a                	rtc	
1686                         ; 439 unsigned char MEMFUNC_CALL GlobalRead8( unsigned long longaddr )
1686                         ; 440 {
1687                         	switch	.ftext
1688  02a2                   f_GlobalRead8:
1690  02a2 3b                	pshd	
1691  02a3 34                	pshx	
1692  02a4 3b                	pshd	
1693       00000002          OFST:	set	2
1696                         ; 444     returnval = 0;
1698                         ; 445     tempgpage = ((S12XGlobalType*)(&longaddr))->gpage;
1700  02a5 e683              	ldab	OFST+1,s
1701  02a7 c47f              	andb	#127
1702  02a9 6b80              	stab	OFST-2,s
1703                         ; 447     MEM_SUSPEND_INTERRUPTS
1706                         xref _MemFuncInterrupt
1711                         xref _RPAGE
1716  02ab 3b                	pshd	
1721  02ac b720              	tpa	
1726  02ae 1410              	sei	
1731  02b0 c600              	ldab	#page(_MemFuncInterrupt)
1736  02b2 7b0000            	stab	_RPAGE
1741  02b5 7a0000            	staa	_MemFuncInterrupt
1746  02b8 3a                	puld	
1748                         ; 450     previousgpage = GPAGE;
1751  02b9 f60000            	ldab	__GPAGE
1752  02bc 6b81              	stab	OFST-1,s
1753                         ; 452     GPAGE = tempgpage;
1755  02be 180d800000        	movb	OFST-2,s,__GPAGE
1756                         ; 453     returnval = *((@far @gpage unsigned char *)longaddr);
1758  02c3 ee84              	ldx	OFST+2,s
1759  02c5 a683              	ldaa	OFST+1,s
1760  02c7 7a0010            	staa	GPAGE
1761  02ca 18e600            	gldab	0,x
1762  02cd 180b000010        	movb	#phigh(__sgconst),GPAGE
1763  02d2 6b80              	stab	OFST-2,s
1764                         ; 456     GPAGE = previousgpage;
1766  02d4 180d810000        	movb	OFST-1,s,__GPAGE
1767                         ; 457     MEM_RESUME_INTERRUPTS
1770  02d9 36                	psha	
1775  02da 8600              	ldaa	#page(_MemFuncInterrupt)
1780  02dc 7a0000            	staa	_RPAGE
1785  02df b60000            	ldaa	_MemFuncInterrupt
1790  02e2 b702              	tap	
1795  02e4 32                	pula	
1797                         ; 459     return returnval;
1800  02e5 e680              	ldab	OFST-2,s
1803  02e7 1b86              	leas	6,s
1804  02e9 0a                	rtc	
1921                         .const:	section	.data
1922                         	even
1923  0000                   L65:
1924  0000 0013ff30          	dc.l	1310512
1925                         	even
1926  0004                   L06:
1927  0004 0013ff34          	dc.l	1310516
1928                         ; 470 ITBM_BOOLEAN MEMFUNC_CALL FlashMemCheck(void)
1928                         ; 471 {
1929                         	switch	.ftext
1930  02ea                   f_FlashMemCheck:
1932  02ea 1bf1eb            	leas	-21,s
1933       00000015          OFST:	set	21
1936                         ; 483     if( EE_IsBusy() == ITBM_TRUE )
1938  02ed 4a000000          	call	f_EE_IsBusy
1940  02f1 53                	decb	
1941                         ; 486         return ITBM_TRUE;
1944  02f2 1827010d          	beq	L355
1945                         ; 489     checksum = 0;
1947  02f6 87                	clra	
1948  02f7 c7                	clrb	
1949  02f8 6c8d              	std	OFST-8,s
1950  02fa 6c8f              	std	OFST-6,s
1951                         ; 490     SegmentNr = (UInt8)GlobalRead16(kEepAddressSegmentNumber); 
1953  02fc ce0013            	ldx	#19
1954  02ff ccff2e            	ldd	#-210
1955  0302 4a025757          	call	f_GlobalRead16
1957  0306 6b8c              	stab	OFST-9,s
1958                         ; 492     if( SegmentNr < 32 ) /* we can't have more than 32 segments */
1960  0308 c120              	cmpb	#32
1961  030a 182400f5          	bhs	L355
1962                         ; 494         for(i=0;i<SegmentNr;i++)         /* Loop for all memory segments */
1964  030e 6984              	clr	OFST-17,s
1966  0310 182000b7          	bra	L165
1967  0314                   L555:
1968                         ; 498             UniversalMemCopy((UInt32)&pCurrentByte,(kEepAddressSegmentStartAddr+kEepSizeSegmentInfo*i),kEepSizeSegmentStartAddr);
1971  0314 3b                	pshd	
1972  0315 e686              	ldab	OFST-15,s
1973  0317 8608              	ldaa	#8
1974  0319 12                	mul	
1975  031a b74d              	sex	d,x
1976  031c f30002            	addd	L65+2
1977  031f 18b90000          	adex	L65
1978  0323 3b                	pshd	
1979  0324 34                	pshx	
1980  0325 1986              	leay	OFST-15,s
1981  0327 b764              	tfr	y,d
1982  0329 1887              	clrx	
1983  032b 4a004b4b          	call	f_UniversalMemCopy
1985  032f 1b86              	leas	6,s
1986                         ; 499             UniversalMemCopy((UInt32)&SegmentLength,(kEepAddressSegmentLength+kEepSizeSegmentInfo*i),kEepSizeSegmentLength);
1988  0331 cc0004            	ldd	#4
1989  0334 3b                	pshd	
1990  0335 e686              	ldab	OFST-15,s
1991  0337 8608              	ldaa	#8
1992  0339 12                	mul	
1993  033a b74d              	sex	d,x
1994  033c f30006            	addd	L06+2
1995  033f 18b90004          	adex	L06
1996  0343 3b                	pshd	
1997  0344 34                	pshx	
1998  0345 198d              	leay	OFST-8,s
1999  0347 b764              	tfr	y,d
2000  0349 1887              	clrx	
2001  034b 4a004b4b          	call	f_UniversalMemCopy
2003  034f 1b86              	leas	6,s
2004                         ; 501             if(i==0)     /* Strip 4 bytes checksum */
2006  0351 e684              	ldab	OFST-17,s
2007  0353 260f              	bne	L565
2008                         ; 503                 SegmentLength -= 4;
2010  0355 ec89              	ldd	OFST-12,s
2011  0357 830004            	subd	#4
2012  035a 6c89              	std	OFST-12,s
2013  035c ec87              	ldd	OFST-14,s
2014  035e 18830000          	sbed	#0
2015  0362 6c87              	std	OFST-14,s
2016  0364                   L565:
2017                         ; 508             for(j=0;j<SegmentLength;j++)
2020  0364 186985            	clrw	OFST-16,s
2022  0367 2055              	bra	L375
2023  0369                   L765:
2024                         ; 511                 MEM_SUSPEND_INTERRUPTS
2027                         xref _MemFuncInterrupt
2032                         xref _RPAGE
2037  0369 3b                	pshd	
2042  036a b720              	tpa	
2047  036c 1410              	sei	
2052  036e c600              	ldab	#page(_MemFuncInterrupt)
2057  0370 7b0000            	stab	_RPAGE
2062  0373 7a0000            	staa	_MemFuncInterrupt
2067  0376 3a                	puld	
2069                         ; 512                 previouspage = GPAGE;         
2072  0377 f60000            	ldab	__GPAGE
2073  037a 6b8b              	stab	OFST-10,s
2074                         ; 513                 GPAGE = GetGPage(pCurrentByte);
2076  037c ec82              	ldd	OFST-19,s
2077  037e ee80              	ldx	OFST-21,s
2078  0380 4a001919          	call	f_GetGPage
2080  0384 7b0000            	stab	__GPAGE
2081                         ; 515                 checksum += *((@far @gpage UInt8*)pCurrentByte);
2083  0387 ee82              	ldx	OFST-19,s
2084  0389 180d810010        	movb	OFST-20,s,GPAGE
2085  038e 18e600            	gldab	0,x
2086  0391 180b000010        	movb	#phigh(__sgconst),GPAGE
2087  0396 87                	clra	
2088  0397 1887              	clrx	
2089  0399 e38f              	addd	OFST-6,s
2090  039b 18a98d            	adex	OFST-8,s
2091  039e 6c8f              	std	OFST-6,s
2092  03a0 6e8d              	stx	OFST-8,s
2093                         ; 516                 pCurrentByte++; 
2095  03a2 186282            	incw	OFST-19,s
2096  03a5 2603              	bne	L26
2097  03a7 186280            	incw	OFST-21,s
2098  03aa                   L26:
2099                         ; 519                 GPAGE = previouspage;
2101  03aa 180d8b0000        	movb	OFST-10,s,__GPAGE
2102                         ; 520                 MEM_RESUME_INTERRUPTS
2105  03af 36                	psha	
2110  03b0 8600              	ldaa	#page(_MemFuncInterrupt)
2115  03b2 7a0000            	staa	_RPAGE
2120  03b5 b60000            	ldaa	_MemFuncInterrupt
2125  03b8 b702              	tap	
2130  03ba 32                	pula	
2132                         ; 508             for(j=0;j<SegmentLength;j++)
2135  03bb 186285            	incw	OFST-16,s
2136  03be                   L375:
2139  03be ec85              	ldd	OFST-16,s
2140  03c0 1887              	clrx	
2141  03c2 ac89              	cpd	OFST-12,s
2142  03c4 18ae87            	cpex	OFST-14,s
2143  03c7 25a0              	blo	L765
2144                         ; 494         for(i=0;i<SegmentNr;i++)         /* Loop for all memory segments */
2147  03c9 6284              	inc	OFST-17,s
2148  03cb                   L165:
2151  03cb e684              	ldab	OFST-17,s
2152  03cd e18c              	cmpb	OFST-9,s
2153  03cf cc0004            	ldd	#4
2154  03d2 1825ff3e          	blo	L555
2155                         ; 526         UniversalMemCopy((UInt32)&StoredCRC,kEepAddressCRCLocal,kEepSizeCRCLocal);
2157  03d6 3b                	pshd	
2158  03d7 ccff2a            	ldd	#-214
2159  03da 3b                	pshd	
2160  03db cc0013            	ldd	#19
2161  03de 3b                	pshd	
2162  03df 19f017            	leay	OFST+2,s
2163  03e2 b764              	tfr	y,d
2164  03e4 1887              	clrx	
2165  03e6 4a004b4b          	call	f_UniversalMemCopy
2167  03ea 1b86              	leas	6,s
2168                         ; 528         bReturnVal = (ITBM_BOOLEAN)(StoredCRC==checksum);
2170  03ec ecf011            	ldd	OFST-4,s
2171  03ef ac8d              	cpd	OFST-8,s
2172  03f1 260b              	bne	L46
2173  03f3 ecf013            	ldd	OFST-2,s
2174  03f6 ac8f              	cpd	OFST-6,s
2175  03f8 2604              	bne	L46
2176  03fa c601              	ldab	#1
2177  03fc 2001              	bra	L07
2178  03fe                   L46:
2179  03fe c7                	clrb	
2180  03ff                   L07:
2181  03ff 6b8c              	stab	OFST-9,s
2183  0401 2002              	bra	L775
2184  0403                   L355:
2185                         ; 533         bReturnVal = ITBM_TRUE;
2187  0403 c601              	ldab	#1
2188  0405                   L775:
2189                         ; 536     return bReturnVal;   
2193  0405 1bf015            	leas	21,s
2194  0408 0a                	rtc	
2256                         ; 547 unsigned long MEMFUNC_CALL MMRTFlashMemCheck(void)
2256                         ; 548 {
2257                         	switch	.ftext
2258  0409                   f_MMRTFlashMemCheck:
2260  0409 1b94              	leas	-12,s
2261       0000000c          OFST:	set	12
2264                         ; 555     checksum = 0;
2266  040b 87                	clra	
2267  040c c7                	clrb	
2268  040d 6c87              	std	OFST-5,s
2269  040f 6c89              	std	OFST-3,s
2270                         ; 556     pCurrentByte = FLASH_GLOBAL_START;
2272  0411 6c82              	std	OFST-10,s
2273  0413 c678              	ldab	#120
2274  0415 6c80              	std	OFST-12,s
2275                         ; 558     for(byindex=0; byindex<NUM_FLASH_PAGES; byindex++)         /* Loop for all memory segments */
2277  0417 698b              	clr	OFST-1,s
2278  0419                   L526:
2279                         ; 563         for(byPageIndex=0; byPageIndex<SIZE_OF_FLASH_PAGE; byPageIndex++)
2282  0419 186984            	clrw	OFST-8,s
2283  041c                   L336:
2284                         ; 566             MEM_SUSPEND_INTERRUPTS
2287                         xref _MemFuncInterrupt
2292                         xref _RPAGE
2297  041c 3b                	pshd	
2302  041d b720              	tpa	
2307  041f 1410              	sei	
2312  0421 c600              	ldab	#page(_MemFuncInterrupt)
2317  0423 7b0000            	stab	_RPAGE
2322  0426 7a0000            	staa	_MemFuncInterrupt
2327  0429 3a                	puld	
2329                         ; 567             previouspage = GPAGE;         
2332  042a f60000            	ldab	__GPAGE
2333  042d 6b86              	stab	OFST-6,s
2334                         ; 568             GPAGE = GetGPage(pCurrentByte);
2336  042f ec82              	ldd	OFST-10,s
2337  0431 ee80              	ldx	OFST-12,s
2338  0433 4a001919          	call	f_GetGPage
2340  0437 7b0000            	stab	__GPAGE
2341                         ; 570             checksum += *((@far @gpage UInt8*)pCurrentByte);
2343  043a ee82              	ldx	OFST-10,s
2344  043c 180d810010        	movb	OFST-11,s,GPAGE
2345  0441 18e600            	gldab	0,x
2346  0444 180b000010        	movb	#phigh(__sgconst),GPAGE
2347  0449 87                	clra	
2348  044a 1887              	clrx	
2349  044c e389              	addd	OFST-3,s
2350  044e 18a987            	adex	OFST-5,s
2351  0451 6c89              	std	OFST-3,s
2352  0453 6e87              	stx	OFST-5,s
2353                         ; 571             pCurrentByte++; 
2355  0455 186282            	incw	OFST-10,s
2356  0458 2603              	bne	L67
2357  045a 186280            	incw	OFST-12,s
2358  045d                   L67:
2359                         ; 574             GPAGE = previouspage;
2361  045d 180d860000        	movb	OFST-6,s,__GPAGE
2362                         ; 575             MEM_RESUME_INTERRUPTS
2365  0462 36                	psha	
2370  0463 8600              	ldaa	#page(_MemFuncInterrupt)
2375  0465 7a0000            	staa	_RPAGE
2380  0468 b60000            	ldaa	_MemFuncInterrupt
2385  046b b702              	tap	
2390  046d 32                	pula	
2392                         ; 563         for(byPageIndex=0; byPageIndex<SIZE_OF_FLASH_PAGE; byPageIndex++)
2395  046e 186284            	incw	OFST-8,s
2398  0471 ec84              	ldd	OFST-8,s
2399  0473 8c4000            	cpd	#16384
2400  0476 25a4              	blo	L336
2401                         ; 558     for(byindex=0; byindex<NUM_FLASH_PAGES; byindex++)         /* Loop for all memory segments */
2404  0478 628b              	inc	OFST-1,s
2407  047a e68b              	ldab	OFST-1,s
2408  047c c120              	cmpb	#32
2409  047e 2599              	blo	L526
2410                         ; 581     return checksum;
2412  0480 ec89              	ldd	OFST-3,s
2413  0482 ee87              	ldx	OFST-5,s
2416  0484 1b8c              	leas	12,s
2417  0486 0a                	rtc	
2451                         ; 597 UInt8 ram_test_word(RAM_TO_TEST_PTR test_addr)
2451                         ; 598 {
2452                         	switch	.ftext
2453  0487                   f_ram_test_word:
2457                         ; 601     xref _save_data
2460                         xref _save_data
2462                         ; 603     tfr d,y             ;put address in y so it doesn't get blown away   
2465  0487 b746              	tfr	d,y
2467                         ; 605     ldd 0,y             ;save_data = *test_addr;
2470  0489 ec40              	ldd	0,y
2472                         ; 606     std _save_data
2475  048b 7c0001            	std	_save_data
2477                         ; 608     ldd #0xAA55         ;*test_addr = 0xAA55;
2480  048e ccaa55            	ldd	#0xAA55
2482                         ; 609     staa 0,y            ;write test pattern 1 byte at a time to address under test 
2485  0491 6a40              	staa	0,y
2487                         ; 610     stab 1,y            
2490  0493 6b41              	stab	1,y
2492                         ; 612     ldaa 0,y            ; test first byte
2495  0495 a640              	ldaa	0,y
2497                         ; 613     cmpa #0xAA
2500  0497 81aa              	cmpa	#0xAA
2502                         ; 614     bne  ram_test_fail  ; signal fail if non zero
2505  0499 2622              	bne	ram_test_fail
2507                         ; 616     ldab 1,y            ; test second byte
2510  049b e641              	ldab	1,y
2512                         ; 617     cmpb #0x55
2515  049d c155              	cmpb	#0x55
2517                         ; 618     bne  ram_test_fail  ; signal fail if non zero
2520  049f 261c              	bne	ram_test_fail
2522                         ; 620     ldd #0x55AA         ;*test_addr = 0x55AA;
2525  04a1 cc55aa            	ldd	#0x55AA
2527                         ; 621     staa 0,y            ;write test pattern 1 byte at a time to address under test 
2530  04a4 6a40              	staa	0,y
2532                         ; 622     stab 1,y             
2535  04a6 6b41              	stab	1,y
2537                         ; 624     ldaa 0,y            ; test first byte
2540  04a8 a640              	ldaa	0,y
2542                         ; 625     cmpa #0x55
2545  04aa 8155              	cmpa	#0x55
2547                         ; 626     bne  ram_test_fail  ; signal fail if non zero
2550  04ac 260f              	bne	ram_test_fail
2552                         ; 628     ldab 1,y            ; test second byte
2555  04ae e641              	ldab	1,y
2557                         ; 629     cmpb #0xAA
2560  04b0 c1aa              	cmpb	#0xAA
2562                         ; 630     bne  ram_test_fail  ; signal fail if non zero
2565  04b2 2609              	bne	ram_test_fail
2567                         ; 632 ram_test_pass:
2570  04b4                   ram_test_pass:
2572                         ; 633     ldd _save_data      ;restore data
2575  04b4 fc0001            	ldd	_save_data
2577                         ; 634     std 0,y
2580  04b7 6c40              	std	0,y
2582                         ; 636     ldab #0             ;set return value in b
2585  04b9 c600              	ldab	#0
2587                         ; 638     bra ram_test_end
2590  04bb 2007              	bra	ram_test_end
2592                         ; 640 ram_test_fail:
2595  04bd                   ram_test_fail:
2597                         ; 641     ldd _save_data      ;restore data
2600  04bd fc0001            	ldd	_save_data
2602                         ; 642     std 0,y
2605  04c0 6c40              	std	0,y
2607                         ; 644     ldab #1             ;set return value in b
2610  04c2 c601              	ldab	#1
2612                         ; 646 ram_test_end:
2615  04c4                   ram_test_end:
2617                         ; 649 }
2620  04c4 0a                	rtc	
2686                         ; 651 ITBM_BOOLEAN	RamPageCheck(UInt8  RamPage)
2686                         ; 652 {
2687                         	switch	.ftext
2688  04c5                   f_RamPageCheck:
2690  04c5 3b                	pshd	
2691  04c6 1b9a              	leas	-6,s
2692       00000006          OFST:	set	6
2695                         ; 654     UInt8   Status = 0;
2697  04c8 6982              	clr	OFST-4,s
2698                         ; 659     RAMEND = 0x2000;
2700  04ca cc2000            	ldd	#8192
2701  04cd 6c84              	std	OFST-2,s
2702                         ; 661     pCurrentWord = (UInt16*)0x1000;
2704  04cf 8610              	ldaa	#16
2705  04d1 6c80              	std	OFST-6,s
2707  04d3 2042              	bra	L117
2708  04d5                   L507:
2709                         ; 665         MEM_SUSPEND_INTERRUPTS
2712                         xref _MemFuncInterrupt
2717                         xref _RPAGE
2722  04d5 3b                	pshd	
2727  04d6 b720              	tpa	
2732  04d8 1410              	sei	
2737  04da c600              	ldab	#page(_MemFuncInterrupt)
2742  04dc 7b0000            	stab	_RPAGE
2747  04df 7a0000            	staa	_MemFuncInterrupt
2752  04e2 3a                	puld	
2754                         ; 666         RPAGETemp = RPAGE;      /* Store RPAGE */
2757  04e3 f60000            	ldab	__RPAGE
2758  04e6 6b83              	stab	OFST-3,s
2759                         ; 667         RPAGE = RamPage;        /* Go to the Ram Page to be tested.*/
2761  04e8 180d870000        	movb	OFST+1,s,__RPAGE
2762                         ; 669         if(ram_test_word(pCurrentWord))
2764  04ed ec80              	ldd	OFST-6,s
2765  04ef 4a048787          	call	f_ram_test_word
2767  04f3 044108            	tbeq	b,L517
2768                         ; 671             Status = ram_test_word(pCurrentWord);    
2770  04f6 ec80              	ldd	OFST-6,s
2771  04f8 4a048787          	call	f_ram_test_word
2773  04fc 6b82              	stab	OFST-4,s
2774  04fe                   L517:
2775                         ; 673         pCurrentWord++;        
2777  04fe ed80              	ldy	OFST-6,s
2778  0500 1942              	leay	2,y
2779  0502 6d80              	sty	OFST-6,s
2780                         ; 675 	    RPAGE = RPAGETemp;
2782  0504 180d830000        	movb	OFST-3,s,__RPAGE
2783                         ; 676         MEM_RESUME_INTERRUPTS
2786  0509 36                	psha	
2791  050a 8600              	ldaa	#page(_MemFuncInterrupt)
2796  050c 7a0000            	staa	_RPAGE
2801  050f b60000            	ldaa	_MemFuncInterrupt
2806  0512 b702              	tap	
2811  0514 32                	pula	
2814  0515 ec80              	ldd	OFST-6,s
2815  0517                   L117:
2816                         ; 663     while( ((UInt16)pCurrentWord < RAMEND) && (Status == 0) )
2818  0517 ac84              	cpd	OFST-2,s
2819  0519 2404              	bhs	L717
2821  051b e782              	tst	OFST-4,s
2822  051d 27b6              	beq	L507
2823  051f                   L717:
2824                         ; 679     return ( Status == 0 );
2826  051f e682              	ldab	OFST-4,s
2827  0521 2604              	bne	L401
2828  0523 c601              	ldab	#1
2829  0525 2001              	bra	L601
2830  0527                   L401:
2831  0527 c7                	clrb	
2832  0528                   L601:
2835  0528 1b88              	leas	8,s
2836  052a 0a                	rtc	
2879                         ; 691 ITBM_BOOLEAN MEMFUNC_CALL RamMemCheck(void)
2879                         ; 692 {
2880                         	switch	.ftext
2881  052b                   f_RamMemCheck:
2883  052b 3b                	pshd	
2884       00000002          OFST:	set	2
2887                         ; 696     status = ITBM_TRUE; 
2889  052c c601              	ldab	#1
2890  052e 6b81              	stab	OFST-1,s
2891                         ; 698     for(bCurRamPage=LASTRAMPAGE; bCurRamPage>=FIRSTRAMPAGE; bCurRamPage--)
2893  0530 c6ff              	ldab	#255
2894  0532 6b80              	stab	OFST-2,s
2895  0534                   L147:
2896                         ; 701         status &=   RamPageCheck(bCurRamPage);     
2899  0534 87                	clra	
2900  0535 4a04c5c5          	call	f_RamPageCheck
2902  0539 e481              	andb	OFST-1,s
2903  053b 6b81              	stab	OFST-1,s
2904                         ; 698     for(bCurRamPage=LASTRAMPAGE; bCurRamPage>=FIRSTRAMPAGE; bCurRamPage--)
2907  053d 6380              	dec	OFST-2,s
2910  053f e680              	ldab	OFST-2,s
2911  0541 c1fb              	cmpb	#251
2912  0543 24ef              	bhs	L147
2913                         ; 706     return  status;
2915  0545 e681              	ldab	OFST-1,s
2918  0547 31                	puly	
2919  0548 0a                	rtc	
2949                         	xdef	f_RamPageCheck
2950                         	xdef	f_ram_test_word
2951                         	switch	.bss
2952  0000                   _MemFuncInterrupt:
2953  0000 00                	ds.b	1
2954                         	xdef	_MemFuncInterrupt
2955  0001                   _save_data:
2956  0001 0000              	ds.b	2
2957                         	xdef	_save_data
2958                         	xdef	f_MMRTFlashMemCheck
2959                         	xdef	f_RamMemCheck
2960                         	xdef	f_FlashMemCheck
2961                         	xdef	f_GlobalRead8
2962                         	xdef	f_GlobalRead16
2963                         	xdef	f_GlobalMemCopy
2964                         	xdef	f_Logical2GlobalMemCopy
2965                         	xdef	f_Global2LogicalMemCopy
2966                         	xdef	f_LogicalMemCopy
2967                         	xdef	f_UniversalMemCopy
2968                         	xdef	f_GetEOffset
2969                         	xdef	f_GetROffset
2970                         	xdef	f_GetPOffset
2971                         	xdef	f_GetGOffset
2972                         	xdef	f_GetEPage
2973                         	xdef	f_GetRPage
2974                         	xdef	f_GetPPage
2975                         	xdef	f_GetGPage
2976                         	xdef	f_MemSet
2977                         	xref	f_EE_IsBusy
2978                         	xref	__RPAGE
2979                         	xref	__GPAGE
2980                         	xref	__sgconst
3001                         	end
