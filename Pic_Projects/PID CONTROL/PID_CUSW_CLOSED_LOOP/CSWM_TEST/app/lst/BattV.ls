   1                         ; C Compiler for S12XE [COSMIC Software].
   2                         ; Generator V4.7.8 - 27 Jul 2007
   3                         ; Optimizer V4.6.13 - 27 Jul 2007
  43                         ; 163 @far void BattVF_Init(void)
  43                         ; 164 {
  44                         .ftext:	section	.text
  45  0000                   f_BattVF_Init:
  49                         ; 168 	EE_BlockRead(EE_BATTERY_CALIB_DATA, (u_8Bit*)&BattV_cals.Thrshld_a[SET_OV_LMT]);
  51  0000 cc002e            	ldd	#_BattV_cals
  52  0003 3b                	pshd	
  53  0004 cc000a            	ldd	#10
  54  0007 4a000000          	call	f_EE_BlockRead
  56                         ; 171 	EE_BlockRead(EE_BATT_COMPENSATION_DATA, (u_8Bit*)&BattV_compensation_a[NO_OUTPUT_ON]);
  58  000b cc0001            	ldd	#_BattV_compensation_a
  59  000e 6c80              	std	0,s
  60  0010 cc000b            	ldd	#11
  61  0013 4a000000          	call	f_EE_BlockRead
  63  0017 1b82              	leas	2,s
  64                         ; 174 	BattV_set[SYSTEM_V].status_c    = V_STAT_GOOD;
  66  0019 c601              	ldab	#1
  67  001b 7b001d            	stab	_BattV_set+3
  68                         ; 175 	BattV_set[BATTERY_V].status_c   = V_STAT_GOOD;
  70  001e 7b0027            	stab	_BattV_set+13
  71                         ; 178 	BattV_set[SYSTEM_V].flt_stat_c  = V_NO_FLT;
  73  0021 c610              	ldab	#16
  74  0023 7b001b            	stab	_BattV_set+1
  75                         ; 179 	BattV_set[BATTERY_V].flt_stat_c = V_NO_FLT;
  77  0026 7b0025            	stab	_BattV_set+11
  78                         ; 182 	BattV_set[SYSTEM_V].prvs_flt_stat_c  = V_NO_FLT;
  80  0029 7b001c            	stab	_BattV_set+2
  81                         ; 183 	BattV_set[BATTERY_V].prvs_flt_stat_c = V_NO_FLT;
  83  002c 7b0026            	stab	_BattV_set+12
  84                         ; 186 	BattV_ldShd_Mnt_stat_c = LDSHD_MNT_GOOD;
  86  002f 790000            	clr	_BattV_ldShd_Mnt_stat_c
  87                         ; 189 	BattV_set_UV_tm = BattV_cals.Timing_a[SET_UV_DLY_TM] * BATTV_MDLTN; // Set UV delay time (3*25)
  89  0032 f60033            	ldab	_BattV_cals+5
  90  0035 8619              	ldaa	#25
  91  0037 12                	mul	
  92  0038 7c0018            	std	_BattV_set_UV_tm
  93                         ; 190 	BattV_flt_mtr_tm = BattV_cals.Timing_a[FLT_MTR_TM] * BATTV_MDLTN;   // Fault mature time (60*25)
  95  003b f60034            	ldab	_BattV_cals+6
  96  003e 8619              	ldaa	#25
  97  0040 12                	mul	
  98  0041 7c0016            	std	_BattV_flt_mtr_tm
  99                         ; 191 	BattV_de_mtr_tm = BattV_cals.Timing_a[FLT_DE_MTR_TM] * BATTV_MDLTN; // Fault de-mature time (15*25)
 101  0044 f60035            	ldab	_BattV_cals+7
 102  0047 8619              	ldaa	#25
 103  0049 12                	mul	
 104  004a 7c0014            	std	_BattV_de_mtr_tm
 105                         ; 193 	BattV_slct = BATTERY_V;        // Select voltage type for DTC management
 107  004d c601              	ldab	#1
 108  004f 7b000b            	stab	_BattV_slct
 109                         ; 194 }
 112  0052 0a                	rtc	
 175                         ; 211 @far void BattVF_Clr_Flt_Times(BattV_type_e Voltage_type)
 175                         ; 212 {
 176                         	switch	.ftext
 177  0053                   f_BattVF_Clr_Flt_Times:
 179       00000000          OFST:	set	0
 182                         ; 214 	BattV_set[Voltage_type].set_UV_cnt_w     = 0;
 184  0053 860a              	ldaa	#10
 185  0055 12                	mul	
 186  0056 b746              	tfr	d,y
 187  0058 87                	clra	
 188  0059 c7                	clrb	
 189  005a 6cea001e          	std	_BattV_set+4,y
 190  005e 6cea0020          	std	_BattV_set+6,y
 191                         ; 215 	BattV_set[Voltage_type].flt_mtr_cnt_w    = 0;
 193                         ; 216 	BattV_set[Voltage_type].flt_de_mtr_cnt_w = 0;
 195  0062 1869ea0022        	clrw	_BattV_set+8,y
 196                         ; 217 }
 199  0067 0a                	rtc	
 227                         ; 234 @far void BattVF_Clear(void)
 227                         ; 235 {
 228                         	switch	.ftext
 229  0068                   f_BattVF_Clear:
 233                         ; 238 	BattV_set[SYSTEM_V].status_c    = V_STAT_GOOD;
 235  0068 c601              	ldab	#1
 236  006a 7b001d            	stab	_BattV_set+3
 237                         ; 239 	BattV_set[BATTERY_V].status_c   = V_STAT_GOOD;
 239  006d 7b0027            	stab	_BattV_set+13
 240                         ; 242 	BattV_set[SYSTEM_V].flt_stat_c  = V_NO_FLT;
 242  0070 c610              	ldab	#16
 243  0072 7b001b            	stab	_BattV_set+1
 244                         ; 243 	BattV_set[BATTERY_V].flt_stat_c = V_NO_FLT;
 246  0075 7b0025            	stab	_BattV_set+11
 247                         ; 246 	BattV_set[SYSTEM_V].prvs_flt_stat_c  = V_NO_FLT;
 249  0078 7b001c            	stab	_BattV_set+2
 250                         ; 247 	BattV_set[BATTERY_V].prvs_flt_stat_c = V_NO_FLT;
 252  007b 7b0026            	stab	_BattV_set+12
 253                         ; 250 	BattV_ldShd_Mnt_stat_c = LDSHD_MNT_GOOD;
 255  007e c7                	clrb	
 256  007f 7b0000            	stab	_BattV_ldShd_Mnt_stat_c
 257                         ; 252 	BattV_ldShed_flt_cnt_w = 0;       // Clear Load shed fault mature count
 259  0082 18c7              	clry	
 260  0084 7d0010            	sty	_BattV_ldShed_flt_cnt_w
 261  0087 7d000e            	sty	_BattV_ldShed4_2secDelay_cnt_w
 262                         ; 253 	BattV_ldShed4_2secDelay_cnt_w = 0; // Clear load Shed_4 2 sec delay count
 264                         ; 255 	BattV_Erratic_V_cnt_w = 0;        // Clear Erratic voltage reading mature time
 266  008a 87                	clra	
 267  008b 7c000c            	std	_BattV_Erratic_V_cnt_w
 268                         ; 258 	BattVF_Clr_Flt_Times(SYSTEM_V);
 270  008e 4a005353          	call	f_BattVF_Clr_Flt_Times
 272                         ; 261 	BattVF_Clr_Flt_Times(BATTERY_V);
 274  0092 cc0001            	ldd	#1
 275  0095 4a005353          	call	f_BattVF_Clr_Flt_Times
 277                         ; 262 }
 280  0099 0a                	rtc	
 307                         ; 282 @far void BattVF_AD_Rdg(void)
 307                         ; 283 {
 308                         	switch	.ftext
 309  009a                   f_BattVF_AD_Rdg:
 313                         ; 285 	ADF_AtoD_Cnvrsn(CH7);
 315  009a cc0007            	ldd	#7
 316  009d 160000            	jsr	_ADF_AtoD_Cnvrsn
 318                         ; 288 	ADF_Str_UnFlt_Rslt(CH7);
 320  00a0 cc0007            	ldd	#7
 321  00a3 160000            	jsr	_ADF_Str_UnFlt_Rslt
 323                         ; 291 	ADF_Filter(CH7);
 325  00a6 cc0007            	ldd	#7
 326  00a9 160000            	jsr	_ADF_Filter
 328                         ; 294 	BattV_ADC_Uint = AD_Filter_set[CH7].flt_rslt_w;
 330  00ac 1804002e0012      	movw	_AD_Filter_set+46,_BattV_ADC_Uint
 331                         ; 295 }
 334  00b2 0a                	rtc	
 380                         ; 320 @far u_8Bit BattVF_AD_to_100mV_Cnvrsn(u_16Bit BattV_AD_value)
 380                         ; 321 {
 381                         	switch	.ftext
 382  00b3                   f_BattVF_AD_to_100mV_Cnvrsn:
 384  00b3 3b                	pshd	
 385  00b4 1b9b              	leas	-5,s
 386       00000005          OFST:	set	5
 389                         ; 327 	BattV_Uint_AD = BattV_AD_value;
 391  00b6 6c82              	std	OFST-3,s
 392                         ; 332 	BattV_100mV_c = (u_8Bit) ( ( ( (BattV_Uint_AD*BATTV_N42)+(BattV_Uint_AD>>1) ) >> 8) + /*BATTV_N7*/ BATTV_N8);
 394  00b8 49                	lsrd	
 395  00b9 6c80              	std	OFST-5,s
 396  00bb ec82              	ldd	OFST-3,s
 397  00bd cd002a            	ldy	#42
 398  00c0 13                	emul	
 399  00c1 e380              	addd	OFST-5,s
 400  00c3 b784              	exg	a,d
 401  00c5 c30008            	addd	#8
 402                         ; 340 	return(BattV_100mV_c);
 406  00c8 1b87              	leas	7,s
 407  00ca 0a                	rtc	
 476                         ; 368 @far void BattVF_Updt_Flt_Stat(BattV_type_e Voltage_type, BattV_Flt_Type_e Volt_Fault_type)
 476                         ; 369 {
 477                         	switch	.ftext
 478  00cb                   f_BattVF_Updt_Flt_Stat:
 480  00cb 3b                	pshd	
 481  00cc 3b                	pshd	
 482       00000002          OFST:	set	2
 485                         ; 371 	BattV_set[Voltage_type].prvs_flt_stat_c = BattV_set[Voltage_type].flt_stat_c;
 487  00cd 860a              	ldaa	#10
 488  00cf 12                	mul	
 489  00d0 b746              	tfr	d,y
 490  00d2 180aea001bea001c  	movb	_BattV_set+1,y,_BattV_set+2,y
 491                         ; 375 	if ( (BattV_set[Voltage_type].flt_mtr_cnt_w < (BattV_flt_mtr_tm-1)) && (BattV_set[Voltage_type].status_c != V_STAT_UGLY) ) {
 493  00da fd0016            	ldy	_BattV_flt_mtr_tm
 494  00dd 03                	dey	
 495  00de 6d80              	sty	OFST-2,s
 496  00e0 e683              	ldab	OFST+1,s
 497  00e2 860a              	ldaa	#10
 498  00e4 12                	mul	
 499  00e5 b746              	tfr	d,y
 500  00e7 ecea0020          	ldd	_BattV_set+6,y
 501  00eb ac80              	cpd	OFST-2,s
 502  00ed 2426              	bhs	L341
 504  00ef e6ea001d          	ldab	_BattV_set+3,y
 505  00f3 c103              	cmpb	#3
 506  00f5 271e              	beq	L341
 507                         ; 380 		BattV_set[Voltage_type].flt_mtr_cnt_w++;
 509  00f7 1862ea0020        	incw	_BattV_set+6,y
 510                         ; 383 		BattV_set[Voltage_type].flt_de_mtr_cnt_w = 0;
 512  00fc 1869ea0022        	clrw	_BattV_set+8,y
 513                         ; 386 		if (Volt_Fault_type == OVER_VOLTAGE_FAULT) {
 515  0101 e688              	ldab	OFST+6,s
 516  0103 042104            	dbne	b,L541
 517                         ; 388 			BattV_set[Voltage_type].flt_stat_c = PRL_OV_FLT;
 519  0106 c601              	ldab	#1
 521  0108 2024              	bra	L151
 522  010a                   L541:
 523                         ; 392 			BattV_set[Voltage_type].flt_stat_c = PRL_UV_FLT;
 525  010a e683              	ldab	OFST+1,s
 526  010c 860a              	ldaa	#10
 527  010e 12                	mul	
 528  010f b746              	tfr	d,y
 529  0111 c602              	ldab	#2
 530  0113 2019              	bra	L151
 531  0115                   L341:
 532                         ; 398 		if (Volt_Fault_type == OVER_VOLTAGE_FAULT) {
 534  0115 e688              	ldab	OFST+6,s
 535  0117 04210b            	dbne	b,L351
 536                         ; 400 			BattV_set[Voltage_type].flt_stat_c = MTR_OV_FLT;
 538  011a e683              	ldab	OFST+1,s
 539  011c 860a              	ldaa	#10
 540  011e 12                	mul	
 541  011f b746              	tfr	d,y
 542  0121 c604              	ldab	#4
 544  0123 2009              	bra	L151
 545  0125                   L351:
 546                         ; 404 			BattV_set[Voltage_type].flt_stat_c = MTR_UV_FLT;
 548  0125 e683              	ldab	OFST+1,s
 549  0127 860a              	ldaa	#10
 550  0129 12                	mul	
 551  012a b746              	tfr	d,y
 552  012c c608              	ldab	#8
 553  012e                   L151:
 554  012e 6bea001b          	stab	_BattV_set+1,y
 555                         ; 410 	if (BattV_set[Voltage_type].prvs_flt_stat_c != BattV_set[Voltage_type].flt_stat_c) {
 557  0132 e683              	ldab	OFST+1,s
 558  0134 860a              	ldaa	#10
 559  0136 12                	mul	
 560  0137 b746              	tfr	d,y
 561  0139 e683              	ldab	OFST+1,s
 562  013b 860a              	ldaa	#10
 563  013d 12                	mul	
 564  013e b745              	tfr	d,x
 565  0140 e6e2001c          	ldab	_BattV_set+2,x
 566  0144 e1ea001b          	cmpb	_BattV_set+1,y
 567  0148 2707              	beq	L751
 568                         ; 412 		BattVF_Clr_Flt_Times(Voltage_type);
 570  014a e683              	ldab	OFST+1,s
 571  014c 87                	clra	
 572  014d 4a005353          	call	f_BattVF_Clr_Flt_Times
 574  0151                   L751:
 575                         ; 414 }
 578  0151 1b84              	leas	4,s
 579  0153 0a                	rtc	
 617                         ; 437 @far void BattVF_Rcv_UV(BattV_type_e Voltage_type)
 617                         ; 438 {
 618                         	switch	.ftext
 619  0154                   f_BattVF_Rcv_UV:
 621  0154 3b                	pshd	
 622  0155 3b                	pshd	
 623       00000002          OFST:	set	2
 626                         ; 440 	if (BattV_set[Voltage_type].crt_vltg_c > BattV_cals.Thrshld_a[CLR_UV_LMT]) {
 628  0156 860a              	ldaa	#10
 629  0158 12                	mul	
 630  0159 b746              	tfr	d,y
 631  015b e6ea001a          	ldab	_BattV_set,y
 632  015f f10030            	cmpb	_BattV_cals+2
 633  0162 2330              	bls	L771
 634                         ; 443 		if (BattV_set[Voltage_type].flt_de_mtr_cnt_w < (BattV_de_mtr_tm-1)) {
 636  0164 fd0014            	ldy	_BattV_de_mtr_tm
 637  0167 03                	dey	
 638  0168 6d80              	sty	OFST-2,s
 639  016a e683              	ldab	OFST+1,s
 640  016c 860a              	ldaa	#10
 641  016e 12                	mul	
 642  016f b746              	tfr	d,y
 643  0171 ecea0022          	ldd	_BattV_set+8,y
 644  0175 ac80              	cpd	OFST-2,s
 645  0177 240c              	bhs	L102
 646                         ; 445 			BattV_set[Voltage_type].flt_de_mtr_cnt_w++;
 648  0179 1862ea0022        	incw	_BattV_set+8,y
 649                         ; 447 			BattV_set[Voltage_type].flt_mtr_cnt_w = 0;
 651  017e 1869ea0020        	clrw	_BattV_set+6,y
 653  0183 2026              	bra	L502
 654  0185                   L102:
 655                         ; 451 			BattV_set[Voltage_type].flt_stat_c = V_NO_FLT;
 657  0185 e683              	ldab	OFST+1,s
 658  0187 860a              	ldaa	#10
 659  0189 12                	mul	
 660  018a b746              	tfr	d,y
 661  018c c610              	ldab	#16
 662  018e 6bea001b          	stab	_BattV_set+1,y
 663  0192 2017              	bra	L502
 664  0194                   L771:
 665                         ; 456 		BattVF_Updt_Flt_Stat(Voltage_type, UNDER_VOLTAGE_FAULT); 
 667  0194 87                	clra	
 668  0195 c7                	clrb	
 669  0196 3b                	pshd	
 670  0197 e685              	ldab	OFST+3,s
 671  0199 4a00cbcb          	call	f_BattVF_Updt_Flt_Stat
 673  019d 1b82              	leas	2,s
 674                         ; 458 		BattV_set[Voltage_type].flt_de_mtr_cnt_w = 0;
 676  019f e683              	ldab	OFST+1,s
 677  01a1 860a              	ldaa	#10
 678  01a3 12                	mul	
 679  01a4 b746              	tfr	d,y
 680  01a6 1869ea0022        	clrw	_BattV_set+8,y
 681  01ab                   L502:
 682                         ; 460 }
 685  01ab 1b84              	leas	4,s
 686  01ad 0a                	rtc	
 724                         ; 482 @far void BattVF_Rcv_OV(BattV_type_e Voltage_type)
 724                         ; 483 {
 725                         	switch	.ftext
 726  01ae                   f_BattVF_Rcv_OV:
 728  01ae 3b                	pshd	
 729  01af 3b                	pshd	
 730       00000002          OFST:	set	2
 733                         ; 485 	if (BattV_set[Voltage_type].crt_vltg_c < BattV_cals.Thrshld_a[CLR_OV_LMT]) {
 735  01b0 860a              	ldaa	#10
 736  01b2 12                	mul	
 737  01b3 b746              	tfr	d,y
 738  01b5 e6ea001a          	ldab	_BattV_set,y
 739  01b9 f1002f            	cmpb	_BattV_cals+1
 740  01bc 2430              	bhs	L522
 741                         ; 487 		if (BattV_set[Voltage_type].flt_de_mtr_cnt_w < (BattV_de_mtr_tm-1)) {
 743  01be fd0014            	ldy	_BattV_de_mtr_tm
 744  01c1 03                	dey	
 745  01c2 6d80              	sty	OFST-2,s
 746  01c4 e683              	ldab	OFST+1,s
 747  01c6 860a              	ldaa	#10
 748  01c8 12                	mul	
 749  01c9 b746              	tfr	d,y
 750  01cb ecea0022          	ldd	_BattV_set+8,y
 751  01cf ac80              	cpd	OFST-2,s
 752  01d1 240c              	bhs	L722
 753                         ; 489 			BattV_set[Voltage_type].flt_de_mtr_cnt_w++;
 755  01d3 1862ea0022        	incw	_BattV_set+8,y
 756                         ; 492 			BattV_set[Voltage_type].flt_mtr_cnt_w =0;
 758  01d8 1869ea0020        	clrw	_BattV_set+6,y
 760  01dd 2027              	bra	L332
 761  01df                   L722:
 762                         ; 496 			BattV_set[Voltage_type].flt_stat_c = V_NO_FLT;
 764  01df e683              	ldab	OFST+1,s
 765  01e1 860a              	ldaa	#10
 766  01e3 12                	mul	
 767  01e4 b746              	tfr	d,y
 768  01e6 c610              	ldab	#16
 769  01e8 6bea001b          	stab	_BattV_set+1,y
 770  01ec 2018              	bra	L332
 771  01ee                   L522:
 772                         ; 501 		BattVF_Updt_Flt_Stat(Voltage_type, OVER_VOLTAGE_FAULT); 
 774  01ee cc0001            	ldd	#1
 775  01f1 3b                	pshd	
 776  01f2 e685              	ldab	OFST+3,s
 777  01f4 4a00cbcb          	call	f_BattVF_Updt_Flt_Stat
 779  01f8 1b82              	leas	2,s
 780                         ; 504 		BattV_set[Voltage_type].flt_de_mtr_cnt_w = 0;
 782  01fa e683              	ldab	OFST+1,s
 783  01fc 860a              	ldaa	#10
 784  01fe 12                	mul	
 785  01ff b746              	tfr	d,y
 786  0201 1869ea0022        	clrw	_BattV_set+8,y
 787  0206                   L332:
 788                         ; 506 }
 791  0206 1b84              	leas	4,s
 792  0208 0a                	rtc	
 847                         ; 533 @far void BattVLF_CalcCompensation(void)
 847                         ; 534 {
 848                         	switch	.ftext
 849  0209                   f_BattVLF_CalcCompensation:
 851  0209 1b9d              	leas	-3,s
 852       00000003          OFST:	set	3
 855                         ; 536 	u_8Bit nmbr_of_actv_vs = 0;
 857                         ; 537 	u_8Bit nmbr_of_actv_hs = 0;     
 859                         ; 538 	u_8Bit stW_active = 0;
 861                         ; 541 	BattV_nmbr_of_actv_outputs = 0;
 863  020b 790007            	clr	_BattV_nmbr_of_actv_outputs
 864                         ; 546 	nmbr_of_actv_vs = vsF_Get_Actv_outputNumber();  // get number of active vents at VS_LOW or VS_HI
 866  020e 4a000000          	call	f_vsF_Get_Actv_outputNumber
 868  0212 6b80              	stab	OFST-3,s
 869                         ; 547 	nmbr_of_actv_hs = hsF_GetActv_OutputNumber();   // get number of active heats at HL3
 871  0214 4a000000          	call	f_hsF_GetActv_OutputNumber
 873  0218 6b81              	stab	OFST-2,s
 874                         ; 548 	stW_active = stWF_Actvtn_Stat();                                // Check if steering wheel is turned ON (at any level WL1, WL2, or WL3)
 876  021a 4a000000          	call	f_stWF_Actvtn_Stat
 878  021e 6b82              	stab	OFST-1,s
 879                         ; 551 	BattV_nmbr_of_actv_outputs = (nmbr_of_actv_vs + nmbr_of_actv_hs);
 881  0220 e680              	ldab	OFST-3,s
 882  0222 eb81              	addb	OFST-2,s
 883  0224 7b0007            	stab	_BattV_nmbr_of_actv_outputs
 884                         ; 554 	if (stW_active == TRUE) {
 886  0227 e682              	ldab	OFST-1,s
 887  0229 042125            	dbne	b,L572
 888                         ; 557 		switch (BattV_nmbr_of_actv_outputs) {
 890  022c f60007            	ldab	_BattV_nmbr_of_actv_outputs
 892  022f 2711              	beq	L532
 893  0231 040113            	dbeq	b,L732
 894  0234 040110            	dbeq	b,L732
 895  0237 040112            	dbeq	b,L142
 896  023a 04010f            	dbeq	b,L142
 897                         ; 587 				BattV_compensation_c = BattV_compensation_a[ONLY_STW_OUTPUT_ON];
 899  023d f60004            	ldab	_BattV_compensation_a+3
 900  0240 2023              	bra	L303
 901  0242                   L532:
 902                         ; 563 				BattV_compensation_c = BattV_compensation_a[ONLY_STW_OUTPUT_ON];
 904  0242 f60004            	ldab	_BattV_compensation_a+3
 905                         ; 564 				break;
 907  0245 201e              	bra	L303
 908  0247                   L732:
 909                         ; 572 				BattV_compensation_c = BattV_compensation_a[STW_PLUS_TWO_OUTPUTS_ON];
 911  0247 f60005            	ldab	_BattV_compensation_a+4
 912                         ; 573 				break;
 914  024a 2019              	bra	L303
 915  024c                   L142:
 916                         ; 581 				BattV_compensation_c = BattV_compensation_a[STW_PLUS_FOUR_OUTPUTS_ON];
 918  024c f60006            	ldab	_BattV_compensation_a+5
 919                         ; 582 				break;
 921  024f 2014              	bra	L303
 922                         ; 587 				BattV_compensation_c = BattV_compensation_a[ONLY_STW_OUTPUT_ON];
 923  0251                   L572:
 924                         ; 594 		switch (BattV_nmbr_of_actv_outputs) {
 926  0251 f60007            	ldab	_BattV_nmbr_of_actv_outputs
 928  0254 270c              	beq	L542
 929  0256 040125            	dbeq	b,L742
 930  0259 040127            	dbeq	b,L152
 931  025c 040124            	dbeq	b,L152
 932  025f 040121            	dbeq	b,L152
 933                         ; 625 				BattV_compensation_c = BattV_compensation_a[NO_OUTPUT_ON];
 935  0262                   L542:
 936                         ; 600 				BattV_compensation_c = BattV_compensation_a[NO_OUTPUT_ON];
 938  0262 f60001            	ldab	_BattV_compensation_a
 939                         ; 601 				break;
 940  0265                   L303:
 941  0265 7b0009            	stab	_BattV_compensation_c
 942                         ; 632 	if (BattV_compensation_c != BattV_prev_compnsation_c) {
 944  0268 f10008            	cmpb	_BattV_prev_compnsation_c
 945  026b 271b              	beq	L713
 946                         ; 642 		if (BattV_set[BATTERY_V].set_UV_cnt_w == BattV_set_UV_tm) {
 948  026d fc0028            	ldd	_BattV_set+14
 949  0270 bc0018            	cpd	_BattV_set_UV_tm
 950  0273 2613              	bne	L713
 951                         ; 644 			BattV_set[BATTERY_V].set_UV_cnt_w = (BattV_set[BATTERY_V].set_UV_cnt_w - BATTV_N4);
 953  0275 b746              	tfr	d,y
 954  0277 195c              	leay	-4,y
 955  0279 7d0028            	sty	_BattV_set+14
 957  027c 200a              	bra	L713
 958  027e                   L742:
 959                         ; 608 				BattV_compensation_c = BattV_compensation_a[ONE_OUTPUT_ON];
 961  027e f60002            	ldab	_BattV_compensation_a+1
 962                         ; 609 				break;
 964  0281 20e2              	bra	L303
 965  0283                   L152:
 966                         ; 618 				BattV_compensation_c = BattV_compensation_a[MULTIPLE_OUTPUTS_ON];
 968  0283 f60003            	ldab	_BattV_compensation_a+2
 969                         ; 619 				break;
 971  0286 20dd              	bra	L303
 972  0288                   L713:
 973                         ; 656 	BattV_prev_compnsation_c = BattV_compensation_c;
 975  0288 180c00090008      	movb	_BattV_compensation_c,_BattV_prev_compnsation_c
 976                         ; 657 }
 979  028e 1b83              	leas	3,s
 980  0290 0a                	rtc	
1025                         ; 686 @far void BattVF_Mntr_Stat(BattV_type_e Voltage_type)
1025                         ; 687 {
1026                         	switch	.ftext
1027  0291                   f_BattVF_Mntr_Stat:
1029  0291 3b                	pshd	
1030  0292 3b                	pshd	
1031       00000002          OFST:	set	2
1034                         ; 689 	if ( (BattV_set[Voltage_type].crt_vltg_c > BattV_cals.Thrshld_a[SET_OV_LMT]) || (BattV_set[Voltage_type].crt_vltg_c < BattV_cals.Thrshld_a[SET_UV_LMT]) ) {
1036  0293 860a              	ldaa	#10
1037  0295 12                	mul	
1038  0296 b746              	tfr	d,y
1039  0298 e6ea001a          	ldab	_BattV_set,y
1040  029c f1002e            	cmpb	_BattV_cals
1041  029f 2212              	bhi	L743
1043  02a1 e683              	ldab	OFST+1,s
1044  02a3 860a              	ldaa	#10
1045  02a5 12                	mul	
1046  02a6 b746              	tfr	d,y
1047  02a8 e6ea001a          	ldab	_BattV_set,y
1048  02ac f10031            	cmpb	_BattV_cals+3
1049  02af 1824008c          	bhs	L543
1050  02b3                   L743:
1051                         ; 691 		if ( (BattV_set[Voltage_type].set_UV_cnt_w < BattV_set_UV_tm) && (BattV_set[Voltage_type].status_c == V_STAT_GOOD) ) {
1053  02b3 e683              	ldab	OFST+1,s
1054  02b5 860a              	ldaa	#10
1055  02b7 12                	mul	
1056  02b8 b746              	tfr	d,y
1057  02ba ecea001e          	ldd	_BattV_set+4,y
1058  02be bc0018            	cpd	_BattV_set_UV_tm
1059  02c1 2410              	bhs	L153
1061  02c3 e6ea001d          	ldab	_BattV_set+3,y
1062  02c7 042109            	dbne	b,L153
1063                         ; 693 			BattV_set[Voltage_type].set_UV_cnt_w++;
1065  02ca 1862ea001e        	incw	_BattV_set+4,y
1067  02cf 182000bf          	bra	L373
1068  02d3                   L153:
1069                         ; 697 			if (BattV_set[Voltage_type].crt_vltg_c > BattV_cals.Thrshld_a[SET_OV_LMT]) {
1071  02d3 e683              	ldab	OFST+1,s
1072  02d5 860a              	ldaa	#10
1073  02d7 12                	mul	
1074  02d8 b746              	tfr	d,y
1075  02da e6ea001a          	ldab	_BattV_set,y
1076  02de f1002e            	cmpb	_BattV_cals
1077  02e1 2310              	bls	L553
1078                         ; 699 				BattVF_Updt_Flt_Stat(Voltage_type, OVER_VOLTAGE_FAULT);
1080  02e3 cc0001            	ldd	#1
1081  02e6 3b                	pshd	
1082  02e7 e685              	ldab	OFST+3,s
1083  02e9 4a00cbcb          	call	f_BattVF_Updt_Flt_Stat
1085  02ed 1b82              	leas	2,s
1087  02ef 1820009f          	bra	L373
1088  02f3                   L553:
1089                         ; 703 				if (Voltage_type == SYSTEM_V) {
1091  02f3 e683              	ldab	OFST+1,s
1092  02f5 260c              	bne	L163
1093                         ; 705 					BattVF_Updt_Flt_Stat(Voltage_type, UNDER_VOLTAGE_FAULT);
1095  02f7 87                	clra	
1096  02f8 3b                	pshd	
1097  02f9 4a00cbcb          	call	f_BattVF_Updt_Flt_Stat
1099  02fd 1b82              	leas	2,s
1101  02ff 1820008f          	bra	L373
1102  0303                   L163:
1103                         ; 709 					BattVLF_CalcCompensation();                                             
1105  0303 4a020909          	call	f_BattVLF_CalcCompensation
1107                         ; 713 					if ( (BattV_set[Voltage_type].crt_vltg_c < (BattV_cals.Thrshld_a[SET_UV_LMT] - BattV_compensation_c) ) && 
1107                         ; 714 					   ( (BattV_set[Voltage_type].set_UV_cnt_w == BattV_set_UV_tm) || (BattV_set[Voltage_type].status_c != V_STAT_GOOD) ) ) {
1109  0307 f60031            	ldab	_BattV_cals+3
1110  030a 87                	clra	
1111  030b f00009            	subb	_BattV_compensation_c
1112  030e 8200              	sbca	#0
1113  0310 6c80              	std	OFST-2,s
1114  0312 e683              	ldab	OFST+1,s
1115  0314 860a              	ldaa	#10
1116  0316 12                	mul	
1117  0317 b746              	tfr	d,y
1118  0319 e6ea001a          	ldab	_BattV_set,y
1119  031d 87                	clra	
1120  031e ac80              	cpd	OFST-2,s
1121  0320 2c70              	bge	L373
1123  0322 ecea001e          	ldd	_BattV_set+4,y
1124  0326 bc0018            	cpd	_BattV_set_UV_tm
1125  0329 2707              	beq	L763
1127  032b e6ea001d          	ldab	_BattV_set+3,y
1128  032f 040160            	dbeq	b,L373
1129  0332                   L763:
1130                         ; 716 						BattVF_Updt_Flt_Stat(Voltage_type, UNDER_VOLTAGE_FAULT);
1132  0332 87                	clra	
1133  0333 c7                	clrb	
1134  0334 3b                	pshd	
1135  0335 e685              	ldab	OFST+3,s
1136  0337 4a00cbcb          	call	f_BattVF_Updt_Flt_Stat
1138  033b 1b82              	leas	2,s
1140  033d 2053              	bra	L373
1141  033f                   L543:
1142                         ; 727 		switch (BattV_set[Voltage_type].flt_stat_c) {
1144  033f e683              	ldab	OFST+1,s
1145  0341 860a              	ldaa	#10
1146  0343 12                	mul	
1147  0344 b746              	tfr	d,y
1148  0346 e6ea001b          	ldab	_BattV_set+1,y
1150  034a 040135            	dbeq	b,L123
1151  034d 04013b            	dbeq	b,L323
1152  0350 c002              	subb	#2
1153  0352 272e              	beq	L123
1154  0354 c004              	subb	#4
1155  0356 2733              	beq	L323
1156                         ; 748 				BattV_set[Voltage_type].flt_stat_c = V_NO_FLT;
1158  0358 c610              	ldab	#16
1159  035a 6bea001b          	stab	_BattV_set+1,y
1160                         ; 751 				BattVF_Clr_Flt_Times(Voltage_type);
1162  035e e683              	ldab	OFST+1,s
1163  0360 87                	clra	
1164  0361 4a005353          	call	f_BattVF_Clr_Flt_Times
1166                         ; 754 				BattV_set[Voltage_type].prvs_flt_stat_c = V_NO_FLT;
1168  0365 e683              	ldab	OFST+1,s
1169  0367 860a              	ldaa	#10
1170  0369 12                	mul	
1171  036a b746              	tfr	d,y
1172  036c c610              	ldab	#16
1173  036e 6bea001c          	stab	_BattV_set+2,y
1174                         ; 757 				if (Voltage_type == BATTERY_V) {
1176  0372 e683              	ldab	OFST+1,s
1177  0374 04211b            	dbne	b,L373
1178                         ; 758 					BattV_compensation_c = 0;
1180  0377 790009            	clr	_BattV_compensation_c
1181                         ; 759 					BattV_prev_compnsation_c = 0;
1183  037a 790008            	clr	_BattV_prev_compnsation_c
1184                         ; 760 					BattV_nmbr_of_actv_outputs = 0;         // Added on 12.08.2008
1186  037d 790007            	clr	_BattV_nmbr_of_actv_outputs
1188  0380 2010              	bra	L373
1189  0382                   L123:
1190                         ; 733 				BattVF_Rcv_OV(Voltage_type);
1192  0382 e683              	ldab	OFST+1,s
1193  0384 87                	clra	
1194  0385 4a01aeae          	call	f_BattVF_Rcv_OV
1196                         ; 734 				break;
1198  0389 2007              	bra	L373
1199  038b                   L323:
1200                         ; 741 				BattVF_Rcv_UV(Voltage_type);
1202  038b e683              	ldab	OFST+1,s
1203  038d 87                	clra	
1204  038e 4a015454          	call	f_BattVF_Rcv_UV
1206                         ; 742 				break;
1208  0392                   L373:
1209                         ; 768 }
1212  0392 1b84              	leas	4,s
1213  0394 0a                	rtc	
1250                         ; 793 @far u_8Bit BattVF_Get_Status(BattV_type_e Voltage_type)
1250                         ; 794 {
1251                         	switch	.ftext
1252  0395                   f_BattVF_Get_Status:
1254  0395 3b                	pshd	
1255       00000000          OFST:	set	0
1258                         ; 796 	switch (BattV_set[Voltage_type].flt_stat_c) {
1260  0396 860a              	ldaa	#10
1261  0398 12                	mul	
1262  0399 b746              	tfr	d,y
1263  039b e6ea001b          	ldab	_BattV_set+1,y
1265  039f 04012b            	dbeq	b,L704
1266  03a2 040128            	dbeq	b,L704
1267  03a5 c002              	subb	#2
1268  03a7 2738              	beq	L114
1269  03a9 c004              	subb	#4
1270  03ab 2734              	beq	L114
1271  03ad c008              	subb	#8
1272  03af 270d              	beq	L504
1273                         ; 824 			BattV_set[Voltage_type].status_c = V_STAT_UNDEFINED;
1275  03b1 87                	clra	
1276  03b2 6aea001d          	staa	_BattV_set+3,y
1277                         ; 825 			BattVF_Clr_Flt_Times(Voltage_type);
1279  03b6 e681              	ldab	OFST+1,s
1280  03b8 4a005353          	call	f_BattVF_Clr_Flt_Times
1282  03bc 203a              	bra	L534
1283  03be                   L504:
1284                         ; 800 			BattV_set[Voltage_type].status_c = V_STAT_GOOD;
1286  03be e681              	ldab	OFST+1,s
1287  03c0 860a              	ldaa	#10
1288  03c2 12                	mul	
1289  03c3 b746              	tfr	d,y
1290  03c5 c601              	ldab	#1
1291  03c7 6bea001d          	stab	_BattV_set+3,y
1292                         ; 801 			break;
1294  03cb 202b              	bra	L534
1295  03cd                   L704:
1296                         ; 807 			BattV_set[Voltage_type].status_c = V_STAT_BAD;
1298  03cd e681              	ldab	OFST+1,s
1299  03cf 860a              	ldaa	#10
1300  03d1 12                	mul	
1301  03d2 b746              	tfr	d,y
1302  03d4 c602              	ldab	#2
1303  03d6 6bea001d          	stab	_BattV_set+3,y
1304                         ; 808 			BattV_set[Voltage_type].set_UV_cnt_w = 0;      // prelim. fault is set - clear delay time to set UV
1306  03da 1869ea001e        	clrw	_BattV_set+4,y
1307                         ; 809 			break;
1309  03df 2017              	bra	L534
1310  03e1                   L114:
1311                         ; 816 			BattV_set[Voltage_type].status_c = V_STAT_UGLY;
1313  03e1 e681              	ldab	OFST+1,s
1314  03e3 860a              	ldaa	#10
1315  03e5 12                	mul	
1316  03e6 b746              	tfr	d,y
1317  03e8 c603              	ldab	#3
1318  03ea 6bea001d          	stab	_BattV_set+3,y
1319                         ; 817 			BattV_set[Voltage_type].set_UV_cnt_w = 0;       // clear set UV delay time
1321  03ee 87                	clra	
1322  03ef c7                	clrb	
1323  03f0 6cea001e          	std	_BattV_set+4,y
1324  03f4 6cea0020          	std	_BattV_set+6,y
1325                         ; 818 			BattV_set[Voltage_type].flt_mtr_cnt_w = 0;      // clear fault mature time
1327                         ; 819 			break;
1329  03f8                   L534:
1330                         ; 830 	if (Voltage_type == SYSTEM_V) {
1332  03f8 e681              	ldab	OFST+1,s
1333  03fa 260c              	bne	L544
1334                         ; 832 		if (load_shed_fault_active_b == TRUE) {
1336  03fc 1f000a0107        	brclr	_BattV_Flags1_c,1,L544
1337                         ; 834 			BattV_set[SYSTEM_V].status_c = V_STAT_UGLY;
1339  0401 c603              	ldab	#3
1340  0403 7b001d            	stab	_BattV_set+3
1342  0406 e681              	ldab	OFST+1,s
1343  0408                   L544:
1344                         ; 843 	return(BattV_set[Voltage_type].status_c);
1346  0408 860a              	ldaa	#10
1347  040a 12                	mul	
1348  040b b746              	tfr	d,y
1349  040d e6ea001d          	ldab	_BattV_set+3,y
1352  0411 31                	puly	
1353  0412 0a                	rtc	
1380                         ; 863 @far void BattVF_Str_Crt_Vltg_Rdg(void)
1380                         ; 864 {
1381                         	switch	.ftext
1382  0413                   f_BattVF_Str_Crt_Vltg_Rdg:
1386                         ; 867 	BattV_set[BATTERY_V].crt_vltg_c = BattVF_AD_to_100mV_Cnvrsn(BattV_ADC_Uint);
1388  0413 fc0012            	ldd	_BattV_ADC_Uint
1389  0416 4a00b3b3          	call	f_BattVF_AD_to_100mV_Cnvrsn
1391  041a 7b0024            	stab	_BattV_set+10
1392                         ; 875 		BattV_set[SYSTEM_V].crt_vltg_c = canio_RX_BatteryVoltageLevel_c;
1394  041d 180c0000001a      	movb	_canio_RX_BatteryVoltageLevel_c,_BattV_set
1395                         ; 877 }
1398  0423 0a                	rtc	
1437                         ; 899 @far void BattV_Chck_ErraticV_Rdg(void)
1437                         ; 900 {
1438                         	switch	.ftext
1439  0424                   f_BattV_Chck_ErraticV_Rdg:
1441  0424 1b9d              	leas	-3,s
1442       00000003          OFST:	set	3
1445                         ; 905 	EE_BlockRead(EE_INTERNAL_FLT_BYTE3, (u_8Bit*)&intFlt_bytes[THREE]);
1447  0426 cc0003            	ldd	#_intFlt_bytes+3
1448  0429 3b                	pshd	
1449  042a cc001b            	ldd	#27
1450  042d 4a000000          	call	f_EE_BlockRead
1452  0431 1b82              	leas	2,s
1453                         ; 908 	if (BattV_set[SYSTEM_V].crt_vltg_c >= BattV_set[BATTERY_V].crt_vltg_c) {
1455  0433 f6001a            	ldab	_BattV_set
1456  0436 f10024            	cmpb	_BattV_set+10
1457  0439 2505              	blo	L374
1458                         ; 910 		BattV_difference_c = BattV_set[SYSTEM_V].crt_vltg_c - BattV_set[BATTERY_V].crt_vltg_c;
1460  043b f00024            	subb	_BattV_set+10
1462  043e 2006              	bra	L574
1463  0440                   L374:
1464                         ; 914 		BattV_difference_c = BattV_set[BATTERY_V].crt_vltg_c - BattV_set[SYSTEM_V].crt_vltg_c;
1466  0440 f60024            	ldab	_BattV_set+10
1467  0443 f0001a            	subb	_BattV_set
1468  0446                   L574:
1469  0446 6b82              	stab	OFST-1,s
1470                         ; 919 	if (BattV_difference_c < BattV_cals.Thrshld_a[INTERNAL_FLT_LMT]) {
1472  0448 f10032            	cmpb	_BattV_cals+4
1473  044b 2434              	bhs	L774
1474                         ; 921 		BattV_Erratic_V_cnt_w = 0;
1476  044d 1879000c          	clrw	_BattV_Erratic_V_cnt_w
1477                         ; 925 		if ( (intFlt_bytes[THREE] & ERRATIC_LOCAL_VOLTAGE_MASK) == ERRATIC_LOCAL_VOLTAGE_MASK) {
1479  0451 1f00030158        	brclr	_intFlt_bytes+3,1,L515
1480                         ; 927 			if (BattV_cals.Thrshld_a[INTERNAL_FLT_LMT] >= BATTV_HALF_A_VOLT) {
1482  0456 f60032            	ldab	_BattV_cals+4
1483  0459 c105              	cmpb	#5
1484  045b 2551              	blo	L515
1485                         ; 929 				if (BattV_difference_c < (BattV_cals.Thrshld_a[INTERNAL_FLT_LMT] - BATTV_HALF_A_VOLT) ) {
1487  045d e682              	ldab	OFST-1,s
1488  045f 87                	clra	
1489  0460 6c80              	std	OFST-3,s
1490  0462 f60032            	ldab	_BattV_cals+4
1491  0465 b746              	tfr	d,y
1492  0467 195b              	leay	-5,y
1493  0469 ad80              	cpy	OFST-3,s
1494  046b 2f41              	ble	L515
1495                         ; 931 					IntFlt_F_Update(THREE, CLR_ERRATIC_VOLTAGE_RDG, INT_FLT_CLR);
1497  046d c601              	ldab	#1
1498  046f 3b                	pshd	
1499  0470 c6fe              	ldab	#254
1500  0472 3b                	pshd	
1501  0473 c603              	ldab	#3
1502  0475 4a000000          	call	f_IntFlt_F_Update
1504  0479 1b84              	leas	4,s
1505                         ; 934 					Erratic_Local_Voltage_b = FALSE;
1507  047b 1d000001          	bclr	_int_flt_byte3,1
1509  047f 202d              	bra	L515
1510  0481                   L774:
1511                         ; 951 		if ( (BattV_Erratic_V_cnt_w < BATTV_ERRATIC_V_TM) && (Erratic_Local_Voltage_b == FALSE) ) {
1513  0481 fc000c            	ldd	_BattV_Erratic_V_cnt_w
1514  0484 8c004b            	cpd	#75
1515  0487 240b              	bhs	L715
1517  0489 1e00000106        	brset	_int_flt_byte3,1,L715
1518                         ; 953 			BattV_Erratic_V_cnt_w++;
1520  048e 1872000c          	incw	_BattV_Erratic_V_cnt_w
1522  0492 201a              	bra	L515
1523  0494                   L715:
1524                         ; 957 			if ( (intFlt_bytes[THREE] & ERRATIC_LOCAL_VOLTAGE_MASK) == ERRATIC_LOCAL_VOLTAGE_MASK) {
1526  0494 1e00030115        	brset	_intFlt_bytes+3,1,L515
1528                         ; 962 				IntFlt_F_Update(THREE, ERRATIC_LOCAL_VOLTAGE_MASK, INT_FLT_SET);
1530  0499 87                	clra	
1531  049a c7                	clrb	
1532  049b 3b                	pshd	
1533  049c 52                	incb	
1534  049d 3b                	pshd	
1535  049e c603              	ldab	#3
1536  04a0 4a000000          	call	f_IntFlt_F_Update
1538  04a4 1b84              	leas	4,s
1539                         ; 965 				BattV_Erratic_V_cnt_w = 0;
1541  04a6 1879000c          	clrw	_BattV_Erratic_V_cnt_w
1542                         ; 968 				Erratic_Local_Voltage_b = TRUE;
1544  04aa 1c000001          	bset	_int_flt_byte3,1
1545  04ae                   L515:
1546                         ; 972 }
1549  04ae 1b83              	leas	3,s
1550  04b0 0a                	rtc	
1577                         ; 1038 @far void BattVF_Manager(void)
1577                         ; 1039 {
1578                         	switch	.ftext
1579  04b1                   f_BattVF_Manager:
1583                         ; 1041 	if ( (canio_RX_IGN_STATUS_c == IGN_RUN) || (canio_RX_IGN_STATUS_c == IGN_START) ) {
1585  04b1 f60000            	ldab	_canio_RX_IGN_STATUS_c
1586  04b4 c104              	cmpb	#4
1587  04b6 2704              	beq	L145
1589  04b8 c105              	cmpb	#5
1590  04ba 2616              	bne	L735
1591  04bc                   L145:
1592                         ; 1043 		BattVF_Str_Crt_Vltg_Rdg();
1594  04bc 4a041313          	call	f_BattVF_Str_Crt_Vltg_Rdg
1596                         ; 1047 		BattV_Chck_ErraticV_Rdg();
1598  04c0 4a042424          	call	f_BattV_Chck_ErraticV_Rdg
1600                         ; 1053 		BattVF_Mntr_Stat(SYSTEM_V);
1602  04c4 87                	clra	
1603  04c5 c7                	clrb	
1604  04c6 4a029191          	call	f_BattVF_Mntr_Stat
1606                         ; 1056 		BattVF_Mntr_Stat(BATTERY_V);
1608  04ca cc0001            	ldd	#1
1609  04cd 4a029191          	call	f_BattVF_Mntr_Stat
1612                         ; 1063 }
1615  04d1 0a                	rtc	
1616  04d2                   L735:
1617                         ; 1060 		BattVF_Clear();
1619  04d2 4a006868          	call	f_BattVF_Clear
1622  04d6 0a                	rtc	
1656                         ; 1079 @far u_8Bit BattVF_Get_Crt_Uint(void)
1656                         ; 1080 {
1657                         	switch	.ftext
1658  04d7                   f_BattVF_Get_Crt_Uint:
1660  04d7 37                	pshb	
1661       00000001          OFST:	set	1
1664                         ; 1084 	BattV_crt_Uint_c = BattVF_AD_to_100mV_Cnvrsn(BattV_ADC_Uint);
1666  04d8 fc0012            	ldd	_BattV_ADC_Uint
1667  04db 4a00b3b3          	call	f_BattVF_AD_to_100mV_Cnvrsn
1669                         ; 1086 	return(BattV_crt_Uint_c);
1673  04df 1b81              	leas	1,s
1674  04e1 0a                	rtc	
1699                         ; 1106 @far void BattVF_DTC_Manager(void)
1699                         ; 1107 {
1700                         	switch	.ftext
1701  04e2                   f_BattVF_DTC_Manager:
1705                         ; 1110 	if (BattV_slct == SYSTEM_V) {
1707  04e2 f6000b            	ldab	_BattV_slct
1708  04e5 261c              	bne	L175
1709                         ; 1113 		BattV_slct = BATTERY_V;
1711  04e7 c601              	ldab	#1
1712  04e9 7b000b            	stab	_BattV_slct
1713                         ; 1116 		if (BattV_set[BATTERY_V].flt_stat_c == MTR_UV_FLT) {
1715  04ec f60025            	ldab	_BattV_set+11
1716  04ef c108              	cmpb	#8
1717  04f1 2605              	bne	L375
1718                         ; 1118 			FMemLibF_ReportDtc(DTC_BATTERY_VOLTAGE_MIN_ERROR_K, ERROR_ON_K);
1720  04f3 cc0001            	ldd	#1
1723  04f6 2002              	bra	LC003
1724  04f8                   L375:
1725                         ; 1122 			FMemLibF_ReportDtc(DTC_BATTERY_VOLTAGE_MIN_ERROR_K, ERROR_OFF_K);
1727  04f8 87                	clra	
1728  04f9 c7                	clrb	
1729  04fa                   LC003:
1730  04fa 3b                	pshd	
1731  04fb ce00a1            	ldx	#161
1732  04fe cc0c18            	ldd	#3096
1734  0501 2018              	bra	L775
1735  0503                   L175:
1736                         ; 1127 		BattV_slct = SYSTEM_V;  
1738  0503 87                	clra	
1739  0504 7a000b            	staa	_BattV_slct
1740                         ; 1134 		if ( (BattV_set[SYSTEM_V].flt_stat_c == MTR_UV_FLT) ) {
1742  0507 f6001b            	ldab	_BattV_set+1
1743  050a c108              	cmpb	#8
1744  050c 2605              	bne	L106
1745                         ; 1136 			FMemLibF_ReportDtc(DTC_SYSTEM_VOLTAGE_MIN_ERROR_K, ERROR_ON_K);
1747  050e cc0001            	ldd	#1
1750  0511 2001              	bra	LC002
1751  0513                   L106:
1752                         ; 1140 			FMemLibF_ReportDtc(DTC_SYSTEM_VOLTAGE_MIN_ERROR_K, ERROR_OFF_K);
1754  0513 c7                	clrb	
1755  0514                   LC002:
1756  0514 3b                	pshd	
1757  0515 ce00a1            	ldx	#161
1758  0518 ccdd84            	ldd	#-8828
1760  051b                   L775:
1761  051b 4a000000          	call	f_FMemLibF_ReportDtc
1762  051f 1b82              	leas	2,s
1763                         ; 1151 	if (BattV_set[BattV_slct].flt_stat_c == MTR_OV_FLT) {
1765  0521 f6000b            	ldab	_BattV_slct
1766  0524 860a              	ldaa	#10
1767  0526 12                	mul	
1768  0527 b746              	tfr	d,y
1769  0529 e6ea001b          	ldab	_BattV_set+1,y
1770  052d c104              	cmpb	#4
1771  052f 260f              	bne	L506
1772                         ; 1154 		if (BattV_slct == SYSTEM_V) {
1774  0531 f6000b            	ldab	_BattV_slct
1775  0534 2605              	bne	L706
1776                         ; 1157 			FMemLibF_ReportDtc(DTC_SYSTEM_VOLTAGE_MAX_ERROR_K, ERROR_ON_K);
1778  0536 cc0001            	ldd	#1
1781  0539 200b              	bra	LC005
1782  053b                   L706:
1783                         ; 1161 			FMemLibF_ReportDtc(DTC_BATTERY_VOLTAGE_MAX_ERROR_K, ERROR_ON_K);
1785  053b cc0001            	ldd	#1
1787  053e 2011              	bra	LC004
1788  0540                   L506:
1789                         ; 1166 		if (BattV_slct == SYSTEM_V) {
1791  0540 f6000b            	ldab	_BattV_slct
1792  0543 260a              	bne	L516
1793                         ; 1168 			FMemLibF_ReportDtc(DTC_SYSTEM_VOLTAGE_MAX_ERROR_K, ERROR_OFF_K);
1795  0545 87                	clra	
1796  0546                   LC005:
1797  0546 3b                	pshd	
1798  0547 ce00a1            	ldx	#161
1799  054a ccdd85            	ldd	#-8827
1802  054d 2009              	bra	L316
1803  054f                   L516:
1804                         ; 1172 			FMemLibF_ReportDtc(DTC_BATTERY_VOLTAGE_MAX_ERROR_K, ERROR_OFF_K);
1806  054f 87                	clra	
1807  0550 c7                	clrb	
1808  0551                   LC004:
1809  0551 3b                	pshd	
1810  0552 ce00a1            	ldx	#161
1811  0555 cc0c17            	ldd	#3095
1813  0558                   L316:
1814  0558 4a000000          	call	f_FMemLibF_ReportDtc
1815  055c 1b82              	leas	2,s
1816                         ; 1175 }
1819  055e 0a                	rtc	
1887                         ; 1229 @far u_8Bit BattVF_Chck_Diag_Prms(u_8Bit BattV_LID_ser, u_8Bit *BattV_data_pc)
1887                         ; 1230 {
1888                         	switch	.ftext
1889  055f                   f_BattVF_Chck_Diag_Prms:
1891  055f 3b                	pshd	
1892  0560 1b9c              	leas	-4,s
1893       00000004          OFST:	set	4
1896                         ; 1234 	u_8Bit BattV_chck_rslt_c = FALSE;    // Check result
1898  0562 6982              	clr	OFST-2,s
1899                         ; 1237 	BattV_LID_service = BattV_LID_ser;
1901  0564 6b83              	stab	OFST-1,s
1902                         ; 1238 	BattV_data_p_c = BattV_data_pc;
1904  0566 18028980          	movw	OFST+5,s,OFST-4,s
1905                         ; 1240 	switch (BattV_LID_service) {
1908  056a c005              	subb	#5
1909  056c 2708              	beq	L126
1910  056e 04011b            	dbeq	b,L326
1911  0571 040133            	dbeq	b,L526
1912                         ; 1323 			BattV_chck_rslt_c = FALSE;
1914  0574 2027              	bra	L776
1915  0576                   L126:
1916                         ; 1246 			if ( (*BattV_data_p_c < 85) || (*BattV_data_p_c > 125) || (*BattV_data_p_c <= BattV_cals.Thrshld_a[SET_UV_LMT]) ) {
1918  0576 e6f30000          	ldab	[OFST-4,s]
1919  057a c155              	cmpb	#85
1920  057c 251f              	blo	L776
1922  057e c17d              	cmpb	#125
1923  0580 221b              	bhi	L776
1925  0582 f10031            	cmpb	_BattV_cals+3
1926                         ; 1249 				BattV_chck_rslt_c = FALSE;
1929  0585 2316              	bls	L776
1930                         ; 1254 				BattV_cals.Thrshld_a[CLR_UV_LMT] = *BattV_data_p_c; //Clear UV condition for new values.                                
1932  0587 7b0030            	stab	_BattV_cals+2
1933                         ; 1258 				BattV_chck_rslt_c = TRUE;
1935  058a 205d              	bra	LC006
1936  058c                   L326:
1937                         ; 1267 			if ( (*BattV_data_p_c < 80) || (*BattV_data_p_c > 120) || (*BattV_data_p_c >= BattV_cals.Thrshld_a[CLR_UV_LMT]) ) {
1939  058c e6f30000          	ldab	[OFST-4,s]
1940  0590 c150              	cmpb	#80
1941  0592 2509              	blo	L776
1943  0594 c178              	cmpb	#120
1944  0596 2205              	bhi	L776
1946  0598 f10030            	cmpb	_BattV_cals+2
1947  059b 2505              	blo	L576
1948  059d                   L776:
1949                         ; 1270 				BattV_chck_rslt_c = FALSE;
1951  059d c7                	clrb	
1952  059e 6b82              	stab	OFST-2,s
1954  05a0 2049              	bra	L366
1955  05a2                   L576:
1956                         ; 1275 				BattV_cals.Thrshld_a[SET_UV_LMT] = *BattV_data_p_c; //Set UV condition for new values.
1958  05a2 7b0031            	stab	_BattV_cals+3
1959                         ; 1279 				BattV_chck_rslt_c = TRUE;
1961  05a5 2042              	bra	LC006
1962  05a7                   L526:
1963                         ; 1294 			if ( ( *BattV_data_p_c > 30 ) ||
1963                         ; 1295 			   ( *(BattV_data_p_c + 1) > 30) ||
1963                         ; 1296 			   ( *(BattV_data_p_c + 2) > 30) ||   
1963                         ; 1297 			   ( *(BattV_data_p_c + 3) > 30) ||
1963                         ; 1298 			   ( *(BattV_data_p_c + 4) > 30) ||
1963                         ; 1299 			   ( *(BattV_data_p_c + 5) > 30) ) {
1965  05a7 ed80              	ldy	OFST-4,s
1966  05a9 e640              	ldab	0,y
1967  05ab c11e              	cmpb	#30
1968  05ad 22ee              	bhi	L776
1970  05af e641              	ldab	1,y
1971  05b1 c11e              	cmpb	#30
1972  05b3 22e8              	bhi	L776
1974  05b5 e642              	ldab	2,y
1975  05b7 c11e              	cmpb	#30
1976  05b9 22e2              	bhi	L776
1978  05bb e643              	ldab	3,y
1979  05bd c11e              	cmpb	#30
1980  05bf 22dc              	bhi	L776
1982  05c1 e644              	ldab	4,y
1983  05c3 c11e              	cmpb	#30
1984  05c5 22d6              	bhi	L776
1986  05c7 e645              	ldab	5,y
1987  05c9 c11e              	cmpb	#30
1988                         ; 1303 				BattV_chck_rslt_c = FALSE;
1991  05cb 22d0              	bhi	L776
1992                         ; 1307 				BattV_compensation_a[0] = *BattV_data_p_c;              // No Outputs are ON (no compensation)
1994  05cd 180d400001        	movb	0,y,_BattV_compensation_a
1995                         ; 1308 				BattV_compensation_a[1] = *(BattV_data_p_c+1);  // Only one heater or vent output is ON
1997  05d2 180d410002        	movb	1,y,_BattV_compensation_a+1
1998                         ; 1309 				BattV_compensation_a[2] = *(BattV_data_p_c+2);  // Two, three or four outputs are ON (heater and/or vent)
2000  05d7 180d420003        	movb	2,y,_BattV_compensation_a+2
2001                         ; 1310 				BattV_compensation_a[3] = *(BattV_data_p_c+3);  // Only steering wheel ON
2003  05dc 180d430004        	movb	3,y,_BattV_compensation_a+3
2004                         ; 1311 				BattV_compensation_a[4] = *(BattV_data_p_c+4);  // Steering wheel + 1/2 heater/vent outputs are ON 
2006  05e1 180d440005        	movb	4,y,_BattV_compensation_a+4
2007                         ; 1312 				BattV_compensation_a[5] = *(BattV_data_p_c+5);  // Steering wheel + 3/4 heater/vent outputs are ON
2009  05e6 7b0006            	stab	_BattV_compensation_a+5
2010                         ; 1316 				BattV_chck_rslt_c = TRUE;
2012  05e9                   LC006:
2013  05e9 c601              	ldab	#1
2014  05eb                   L366:
2015                         ; 1326 	return(BattV_chck_rslt_c);
2019  05eb 1b86              	leas	6,s
2020  05ed 0a                	rtc	
2069                         ; 1349 @far u_8Bit BattVF_Write_Diag_Data(u_8Bit BattV_service)
2069                         ; 1350 {
2070                         	switch	.ftext
2071  05ee                   f_BattVF_Write_Diag_Data:
2073  05ee 3b                	pshd	
2074  05ef 37                	pshb	
2075       00000001          OFST:	set	1
2078                         ; 1353 	u_8Bit BattV_write_rslt_c = FALSE;   // Write result
2080                         ; 1356 	BattV_LID = BattV_service;
2082  05f0 6b80              	stab	OFST-1,s
2083                         ; 1360 	if ( (BattV_LID == VOLTAGE_ON) || (BattV_LID == VOLTAGE_OFF) || (BattV_LID == BATTERY_COMPENSATION) ) {
2085  05f2 c105              	cmpb	#5
2086  05f4 2708              	beq	L547
2088  05f6 c106              	cmpb	#6
2089  05f8 2704              	beq	L547
2091  05fa c107              	cmpb	#7
2092  05fc 261f              	bne	L347
2093  05fe                   L547:
2094                         ; 1362 		if (BattV_LID == BATTERY_COMPENSATION) {
2096  05fe c107              	cmpb	#7
2097  0600 2609              	bne	L157
2098                         ; 1364 			EE_BlockWrite(EE_BATT_COMPENSATION_DATA, (u_8Bit*)&BattV_compensation_a[NO_OUTPUT_ON]);
2100  0602 cc0001            	ldd	#_BattV_compensation_a
2101  0605 3b                	pshd	
2102  0606 cc000b            	ldd	#11
2105  0609 2007              	bra	L357
2106  060b                   L157:
2107                         ; 1368 			EE_BlockWrite(EE_BATTERY_CALIB_DATA, (u_8Bit*)&BattV_cals);
2109  060b cc002e            	ldd	#_BattV_cals
2110  060e 3b                	pshd	
2111  060f cc000a            	ldd	#10
2113  0612                   L357:
2114  0612 4a000000          	call	f_EE_BlockWrite
2115  0616 1b82              	leas	2,s
2116                         ; 1371 		BattV_write_rslt_c = TRUE;
2118  0618 c601              	ldab	#1
2120  061a                   L557:
2121                         ; 1378 	return(BattV_write_rslt_c);
2125  061a 1b83              	leas	3,s
2126  061c 0a                	rtc	
2127  061d                   L347:
2128                         ; 1376 		BattV_write_rslt_c = FALSE;
2130  061d c7                	clrb	
2131  061e 6b80              	stab	OFST-1,s
2132  0620 20f8              	bra	L557
2156                         ; 1395 @far u_8Bit BattVF_Get_LdShd_Mnt_Stat(void)
2156                         ; 1396 {
2157                         	switch	.ftext
2158  0622                   f_BattVF_Get_LdShd_Mnt_Stat:
2162                         ; 1398 	return(BattV_ldShd_Mnt_stat_c);
2164  0622 f60000            	ldab	_BattV_ldShd_Mnt_stat_c
2167  0625 0a                	rtc	
2509                         	xdef	f_BattV_Chck_ErraticV_Rdg
2510                         	xdef	f_BattVF_Str_Crt_Vltg_Rdg
2511                         	xdef	f_BattVF_Mntr_Stat
2512                         	xdef	f_BattVLF_CalcCompensation
2513                         	xdef	f_BattVF_Rcv_OV
2514                         	xdef	f_BattVF_Rcv_UV
2515                         	xdef	f_BattVF_Updt_Flt_Stat
2516                         	xdef	f_BattVF_Clear
2517                         	xdef	f_BattVF_Clr_Flt_Times
2518                         	switch	.bss
2519  0000                   _BattV_ldShd_Mnt_stat_c:
2520  0000 00                	ds.b	1
2521                         	xdef	_BattV_ldShd_Mnt_stat_c
2522  0001                   _BattV_compensation_a:
2523  0001 000000000000      	ds.b	6
2524                         	xdef	_BattV_compensation_a
2525  0007                   _BattV_nmbr_of_actv_outputs:
2526  0007 00                	ds.b	1
2527                         	xdef	_BattV_nmbr_of_actv_outputs
2528  0008                   _BattV_prev_compnsation_c:
2529  0008 00                	ds.b	1
2530                         	xdef	_BattV_prev_compnsation_c
2531  0009                   _BattV_compensation_c:
2532  0009 00                	ds.b	1
2533                         	xdef	_BattV_compensation_c
2534  000a                   _BattV_Flags1_c:
2535  000a 00                	ds.b	1
2536                         	xdef	_BattV_Flags1_c
2537  000b                   _BattV_slct:
2538  000b 00                	ds.b	1
2539                         	xdef	_BattV_slct
2540  000c                   _BattV_Erratic_V_cnt_w:
2541  000c 0000              	ds.b	2
2542                         	xdef	_BattV_Erratic_V_cnt_w
2543  000e                   _BattV_ldShed4_2secDelay_cnt_w:
2544  000e 0000              	ds.b	2
2545                         	xdef	_BattV_ldShed4_2secDelay_cnt_w
2546  0010                   _BattV_ldShed_flt_cnt_w:
2547  0010 0000              	ds.b	2
2548                         	xdef	_BattV_ldShed_flt_cnt_w
2549  0012                   _BattV_ADC_Uint:
2550  0012 0000              	ds.b	2
2551                         	xdef	_BattV_ADC_Uint
2552  0014                   _BattV_de_mtr_tm:
2553  0014 0000              	ds.b	2
2554                         	xdef	_BattV_de_mtr_tm
2555  0016                   _BattV_flt_mtr_tm:
2556  0016 0000              	ds.b	2
2557                         	xdef	_BattV_flt_mtr_tm
2558  0018                   _BattV_set_UV_tm:
2559  0018 0000              	ds.b	2
2560                         	xdef	_BattV_set_UV_tm
2561                         	xref	f_stWF_Actvtn_Stat
2562                         	xref	f_hsF_GetActv_OutputNumber
2563                         	xref	f_vsF_Get_Actv_outputNumber
2564                         	xref	f_IntFlt_F_Update
2565                         	xref	_intFlt_bytes
2566                         	xref	_int_flt_byte3
2567                         	xref	f_EE_BlockRead
2568                         	xref	f_EE_BlockWrite
2569                         	xref	f_FMemLibF_ReportDtc
2570                         	xref	_ADF_AtoD_Cnvrsn
2571                         	xref	_ADF_Str_UnFlt_Rslt
2572                         	xref	_ADF_Filter
2573                         	xref	_AD_Filter_set
2574                         	xref	_canio_RX_IGN_STATUS_c
2575                         	xref	_canio_RX_BatteryVoltageLevel_c
2576                         	xdef	f_BattVF_AD_to_100mV_Cnvrsn
2577                         	xdef	f_BattVF_Get_Crt_Uint
2578                         	xdef	f_BattVF_Write_Diag_Data
2579                         	xdef	f_BattVF_Chck_Diag_Prms
2580                         	xdef	f_BattVF_Get_LdShd_Mnt_Stat
2581                         	xdef	f_BattVF_DTC_Manager
2582                         	xdef	f_BattVF_Get_Status
2583                         	xdef	f_BattVF_AD_Rdg
2584                         	xdef	f_BattVF_Manager
2585                         	xdef	f_BattVF_Init
2586  001a                   _BattV_set:
2587  001a 0000000000000000  	ds.b	20
2588                         	xdef	_BattV_set
2589  002e                   _BattV_cals:
2590  002e 0000000000000000  	ds.b	8
2591                         	xdef	_BattV_cals
2612                         	end
