   1                         ; C Compiler for S12XE [COSMIC Software].
   2                         ; Generator V4.7.8 - 27 Jul 2007
   3                         ; Optimizer V4.6.13 - 27 Jul 2007
   4                         .const:	section	.data
   5  0000                   _g_descMainVersion:
   6  0000 05                	dc.b	5
   7  0001                   _g_descSubVersion:
   8  0001 07                	dc.b	7
   9  0002                   _g_descBugFixVersion:
  10  0002 32                	dc.b	50
  11  0003                   L342_g_descP2TicksTable:
  12  0003 0004              	dc.w	4
  13  0005 01f3              	dc.w	499
  14  0007 0004              	dc.w	4
  15  0009 01f3              	dc.w	499
  16  000b 0004              	dc.w	4
  17  000d 01f3              	dc.w	499
  18  000f 0001              	dc.w	1
  19  0011 00c7              	dc.w	199
  20  0013 0001              	dc.w	1
  21  0015 00c7              	dc.w	199
  22  0017 0001              	dc.w	1
  23  0019 00c7              	dc.w	199
  24  001b                   L542_g_descP2TimingsTable:
  25  001b 0032              	dc.w	50
  26  001d 01f4              	dc.w	500
  27  001f 0032              	dc.w	50
  28  0021 01f4              	dc.w	500
  29  0023 0032              	dc.w	50
  30  0025 01f4              	dc.w	500
  31  0027 0014              	dc.w	20
  32  0029 00c8              	dc.w	200
  33  002b 0014              	dc.w	20
  34  002d 00c8              	dc.w	200
  35  002f 0014              	dc.w	20
  36  0031 00c8              	dc.w	200
  37  0033                   L742_g_descStateGroupTransition:
  38  0033 0607              	dc.w	1543
  39  0035 0101              	dc.w	257
  40  0037 0607              	dc.w	1543
  41  0039 0102              	dc.w	258
  42  003b 0607              	dc.w	1543
  43  003d 0104              	dc.w	260
  44  003f 0300              	dc.w	768
  45  0041 0400              	dc.w	1024
  46  0043 0500              	dc.w	1280
  47  0045 0200              	dc.w	512
  48  0047                   L152_g_descSidMap:
  49  0047 ff                	dc.b	255
  50  0048 ff                	dc.b	255
  51  0049 ff                	dc.b	255
  52  004a ff                	dc.b	255
  53  004b ff                	dc.b	255
  54  004c ff                	dc.b	255
  55  004d ff                	dc.b	255
  56  004e ff                	dc.b	255
  57  004f ff                	dc.b	255
  58  0050 ff                	dc.b	255
  59  0051 ff                	dc.b	255
  60  0052 ff                	dc.b	255
  61  0053 ff                	dc.b	255
  62  0054 ff                	dc.b	255
  63  0055 ff                	dc.b	255
  64  0056 ff                	dc.b	255
  65  0057 00                	dc.b	0
  66  0058 01                	dc.b	1
  67  0059 ff                	dc.b	255
  68  005a ff                	dc.b	255
  69  005b 02                	dc.b	2
  70  005c ff                	dc.b	255
  71  005d ff                	dc.b	255
  72  005e ff                	dc.b	255
  73  005f ff                	dc.b	255
  74  0060 03                	dc.b	3
  75  0061 ff                	dc.b	255
  76  0062 ff                	dc.b	255
  77  0063 ff                	dc.b	255
  78  0064 ff                	dc.b	255
  79  0065 ff                	dc.b	255
  80  0066 ff                	dc.b	255
  81  0067 ff                	dc.b	255
  82  0068 ff                	dc.b	255
  83  0069 04                	dc.b	4
  84  006a ff                	dc.b	255
  85  006b ff                	dc.b	255
  86  006c ff                	dc.b	255
  87  006d ff                	dc.b	255
  88  006e 05                	dc.b	5
  89  006f 06                	dc.b	6
  90  0070 ff                	dc.b	255
  91  0071 ff                	dc.b	255
  92  0072 ff                	dc.b	255
  93  0073 ff                	dc.b	255
  94  0074 ff                	dc.b	255
  95  0075 07                	dc.b	7
  96  0076 08                	dc.b	8
  97  0077 ff                	dc.b	255
  98  0078 09                	dc.b	9
  99  0079 ff                	dc.b	255
 100  007a ff                	dc.b	255
 101  007b 0a                	dc.b	10
 102  007c ff                	dc.b	255
 103  007d 0b                	dc.b	11
 104  007e 0c                	dc.b	12
 105  007f ff                	dc.b	255
 106  0080 ff                	dc.b	255
 107  0081 ff                	dc.b	255
 108  0082 ff                	dc.b	255
 109  0083 ff                	dc.b	255
 110  0084 ff                	dc.b	255
 111  0085 0d                	dc.b	13
 112  0086 ff                	dc.b	255
 113  0087 ff                	dc.b	255
 114  0088 ff                	dc.b	255
 115  0089 ff                	dc.b	255
 116  008a ff                	dc.b	255
 117  008b ff                	dc.b	255
 118  008c 0e                	dc.b	14
 119  008d                   L352_g_descSvcHead:
 120  008d 01                	dc.b	1
 121  008e 11                	dc.b	17
 122  008f ff                	dc.b	255
 123  0090 3f                	dc.b	63
 124  0091 00                	dc.b	0
 125  0092 02                	dc.b	2
 126  0093 00                	dc.b	0
 127  0094 00                	dc.b	0
 128  0095 01                	dc.b	1
 129  0096 11                	dc.b	17
 130  0097 ff                	dc.b	255
 131  0098 3f                	dc.b	63
 132  0099 00                	dc.b	0
 133  009a 02                	dc.b	2
 134  009b 03                	dc.b	3
 135  009c 03                	dc.b	3
 136  009d 01                	dc.b	1
 137  009e 00                	dc.b	0
 138  009f 2f                	dc.b	47
 139  00a0 3d                	dc.b	61
 140  00a1 00                	dc.b	0
 141  00a2 01                	dc.b	1
 142  00a3 04                	dc.b	4
 143  00a4 04                	dc.b	4
 144  00a5 01                	dc.b	1
 145  00a6 11                	dc.b	17
 146  00a7 ff                	dc.b	255
 147  00a8 3d                	dc.b	61
 148  00a9 00                	dc.b	0
 149  00aa 02                	dc.b	2
 150  00ab 05                	dc.b	5
 151  00ac 04                	dc.b	4
 152  00ad 01                	dc.b	1
 153  00ae 00                	dc.b	0
 154  00af 2f                	dc.b	47
 155  00b0 3f                	dc.b	63
 156  00b1 00                	dc.b	0
 157  00b2 01                	dc.b	1
 158  00b3 0d                	dc.b	13
 159  00b4 0c                	dc.b	12
 160  00b5 01                	dc.b	1
 161  00b6 11                	dc.b	17
 162  00b7 f5                	dc.b	245
 163  00b8 3e                	dc.b	62
 164  00b9 00                	dc.b	0
 165  00ba 02                	dc.b	2
 166  00bb 0e                	dc.b	14
 167  00bc 0c                	dc.b	12
 168  00bd 01                	dc.b	1
 169  00be 11                	dc.b	17
 170  00bf ff                	dc.b	255
 171  00c0 3e                	dc.b	62
 172  00c1 00                	dc.b	0
 173  00c2 02                	dc.b	2
 174  00c3 12                	dc.b	18
 175  00c4 10                	dc.b	16
 176  00c5 07                	dc.b	7
 177  00c6 22                	dc.b	34
 178  00c7 2f                	dc.b	47
 179  00c8 3f                	dc.b	63
 180  00c9 00                	dc.b	0
 181  00ca 04                	dc.b	4
 182  00cb 14                	dc.b	20
 183  00cc 12                	dc.b	18
 184  00cd 07                	dc.b	7
 185  00ce 33                	dc.b	51
 186  00cf a5                	dc.b	165
 187  00d0 3f                	dc.b	63
 188  00d1 00                	dc.b	0
 189  00d2 04                	dc.b	4
 190  00d3 27                	dc.b	39
 191  00d4 38                	dc.b	56
 192  00d5 0d                	dc.b	13
 193  00d6 33                	dc.b	51
 194  00d7 bf                	dc.b	191
 195  00d8 3f                	dc.b	63
 196  00d9 00                	dc.b	0
 197  00da 04                	dc.b	4
 198  00db 53                	dc.b	83
 199  00dc bc                	dc.b	188
 200  00dd 01                	dc.b	1
 201  00de 00                	dc.b	0
 202  00df 25                	dc.b	37
 203  00e0 3a                	dc.b	58
 204  00e1 00                	dc.b	0
 205  00e2 01                	dc.b	1
 206  00e3 5e                	dc.b	94
 207  00e4 dd                	dc.b	221
 208  00e5 01                	dc.b	1
 209  00e6 00                	dc.b	0
 210  00e7 25                	dc.b	37
 211  00e8 3a                	dc.b	58
 212  00e9 00                	dc.b	0
 213  00ea 01                	dc.b	1
 214  00eb 5f                	dc.b	95
 215  00ec dd                	dc.b	221
 216  00ed 01                	dc.b	1
 217  00ee 00                	dc.b	0
 218  00ef 25                	dc.b	37
 219  00f0 3a                	dc.b	58
 220  00f1 00                	dc.b	0
 221  00f2 01                	dc.b	1
 222  00f3 60                	dc.b	96
 223  00f4 dd                	dc.b	221
 224  00f5 01                	dc.b	1
 225  00f6 11                	dc.b	17
 226  00f7 ff                	dc.b	255
 227  00f8 3f                	dc.b	63
 228  00f9 00                	dc.b	0
 229  00fa 02                	dc.b	2
 230  00fb 61                	dc.b	97
 231  00fc dd                	dc.b	221
 232  00fd 01                	dc.b	1
 233  00fe 11                	dc.b	17
 234  00ff ff                	dc.b	255
 235  0100 3e                	dc.b	62
 236  0101 00                	dc.b	0
 237  0102 02                	dc.b	2
 238  0103 62                	dc.b	98
 239  0104 de                	dc.b	222
 240  0105 00                	dc.b	0
 241  0106 00                	dc.b	0
 242  0107 20                	dc.b	32
 243  0108 00                	dc.b	0
 244  0109 00                	dc.b	0
 245  010a 01                	dc.b	1
 246  010b 64                	dc.b	100
 247  010c e0                	dc.b	224
 248  010d                   L552_g_descPostHandlerTable:
 250  010d 0d05              	dc.w	L731f_DescDummyPostHandler
 251  010f 0500              	dc.b	page(L731f_DescDummyPostHandler),0
 253  0111 0cdb              	dc.w	L521f_DescOemPostStartDefault
 254  0113 db00              	dc.b	page(L521f_DescOemPostStartDefault),0
 256  0115 0ce9              	dc.w	L721f_DescOemPostStartProgramming
 257  0117 e900              	dc.b	page(L721f_DescOemPostStartProgramming),0
 259  0119 0cf7              	dc.w	L131f_DescOemPostStartExtendedDiagnostic
 260  011b f700              	dc.b	page(L131f_DescOemPostStartExtendedDiagnostic),0
 262  011d 0db2              	dc.w	L531f_DescOemPostCommonCommCtrlProcess
 263  011f b200              	dc.b	page(L531f_DescOemPostCommonCommCtrlProcess),0
 265  0121 1031              	dc.w	L331f_DescPostReadDataByIdentifier
 266  0123 3100              	dc.b	page(L331f_DescPostReadDataByIdentifier),0
 267  0125                   L752_g_descSvcInst:
 268  0125 0002              	dc.w	2
 269  0127 0f                	dc.b	15
 270  0128 073f              	dc.w	1855
 271  012a 00                	dc.b	0
 272  012b 01                	dc.b	1
 274  012c 0c9c              	dc.w	L111f_DescOemStartDefault
 275  012e 9c00              	dc.b	page(L111f_DescOemStartDefault),0
 276  0130 0002              	dc.w	2
 277  0132 0f                	dc.b	15
 278  0133 073f              	dc.w	1855
 279  0135 01                	dc.b	1
 280  0136 02                	dc.b	2
 282  0137 0caa              	dc.w	L311f_DescOemStartProgramming
 283  0139 aa00              	dc.b	page(L311f_DescOemStartProgramming),0
 284  013b 0002              	dc.w	2
 285  013d 0f                	dc.b	15
 286  013e 073f              	dc.w	1855
 287  0140 02                	dc.b	2
 288  0141 03                	dc.b	3
 290  0142 0cb8              	dc.w	L511f_DescOemStartExtendedDiagnostic
 291  0144 b800              	dc.b	page(L511f_DescOemStartExtendedDiagnostic),0
 292  0146 0002              	dc.w	2
 293  0148 0f                	dc.b	15
 294  0149 073f              	dc.w	1855
 295  014b 05                	dc.b	5
 296  014c 00                	dc.b	0
 298  014d 0000              	dc.w	f_ApplDescResetHardReset
 299  014f 0000              	dc.b	page(f_ApplDescResetHardReset),0
 300  0151 0004              	dc.w	4
 301  0153 0f                	dc.b	15
 302  0154 073d              	dc.w	1853
 303  0156 05                	dc.b	5
 304  0157 00                	dc.b	0
 306  0158 0000              	dc.w	f_ApplDescClearFaultMem
 307  015a 0000              	dc.b	page(f_ApplDescClearFaultMem),0
 308  015c 0003              	dc.w	3
 309  015e 0f                	dc.b	15
 310  015f 073d              	dc.w	1853
 311  0161 05                	dc.b	5
 312  0162 00                	dc.b	0
 314  0163 0000              	dc.w	f_ApplDescSUBF_02FaultMem
 315  0165 0000              	dc.b	page(f_ApplDescSUBF_02FaultMem),0
 316  0167 0006              	dc.w	6
 317  0169 0f                	dc.b	15
 318  016a 073d              	dc.w	1853
 319  016c 05                	dc.b	5
 320  016d 00                	dc.b	0
 322  016e 0000              	dc.w	f_ApplDescSUBF_04FaultMem
 323  0170 0000              	dc.b	page(f_ApplDescSUBF_04FaultMem),0
 324  0172 0006              	dc.w	6
 325  0174 0f                	dc.b	15
 326  0175 073d              	dc.w	1853
 327  0177 05                	dc.b	5
 328  0178 00                	dc.b	0
 330  0179 0000              	dc.w	f_ApplDescSUBF_06FaultMem
 331  017b 0000              	dc.b	page(f_ApplDescSUBF_06FaultMem),0
 332  017d 0004              	dc.w	4
 333  017f 0f                	dc.b	15
 334  0180 073d              	dc.w	1853
 335  0182 05                	dc.b	5
 336  0183 00                	dc.b	0
 338  0184 0000              	dc.w	f_ApplDescSUBF_07FaultMem
 339  0186 0000              	dc.b	page(f_ApplDescSUBF_07FaultMem),0
 340  0188 0004              	dc.w	4
 341  018a 0f                	dc.b	15
 342  018b 073d              	dc.w	1853
 343  018d 05                	dc.b	5
 344  018e 00                	dc.b	0
 346  018f 0000              	dc.w	f_ApplDescSUBF_08FaultMem
 347  0191 0000              	dc.b	page(f_ApplDescSUBF_08FaultMem),0
 348  0193 0005              	dc.w	5
 349  0195 0f                	dc.b	15
 350  0196 073d              	dc.w	1853
 351  0198 05                	dc.b	5
 352  0199 00                	dc.b	0
 354  019a 0000              	dc.w	f_ApplDescSUBF_09FaultMem
 355  019c 0000              	dc.b	page(f_ApplDescSUBF_09FaultMem),0
 356  019e 0002              	dc.w	2
 357  01a0 0f                	dc.b	15
 358  01a1 073d              	dc.w	1853
 359  01a3 05                	dc.b	5
 360  01a4 00                	dc.b	0
 362  01a5 0000              	dc.w	f_ApplDescSUBF_0CFaultMem
 363  01a7 0000              	dc.b	page(f_ApplDescSUBF_0CFaultMem),0
 364  01a9 0002              	dc.w	2
 365  01ab 0f                	dc.b	15
 366  01ac 073d              	dc.w	1853
 367  01ae 05                	dc.b	5
 368  01af 00                	dc.b	0
 370  01b0 0000              	dc.w	f_ApplDescSUBF_0EFaultMem
 371  01b2 0000              	dc.b	page(f_ApplDescSUBF_0EFaultMem),0
 372  01b4 0000              	dc.w	0
 373  01b6 0f                	dc.b	15
 374  01b7 073f              	dc.w	1855
 375  01b9 05                	dc.b	5
 376  01ba 05                	dc.b	5
 378  01bb 1032              	dc.w	L711f_DescReadDataByIdentifier
 379  01bd 3200              	dc.b	page(L711f_DescReadDataByIdentifier),0
 380  01bf 0003              	dc.w	3
 381  01c1 05                	dc.b	5
 382  01c2 073a              	dc.w	1850
 383  01c4 05                	dc.b	5
 384  01c5 00                	dc.b	0
 386  01c6 0000              	dc.w	f_ApplDescRequestLevel1Seed
 387  01c8 0000              	dc.b	page(f_ApplDescRequestLevel1Seed),0
 388  01ca 0006              	dc.w	6
 389  01cc 05                	dc.b	5
 390  01cd 073a              	dc.w	1850
 391  01cf 03                	dc.b	3
 392  01d0 00                	dc.b	0
 394  01d1 0000              	dc.w	f_ApplDescSendLevel1Key
 395  01d3 0000              	dc.b	page(f_ApplDescSendLevel1Key),0
 396  01d5 0002              	dc.w	2
 397  01d7 05                	dc.b	5
 398  01d8 073e              	dc.w	1854
 399  01da 05                	dc.b	5
 400  01db 00                	dc.b	0
 402  01dc 0000              	dc.w	f_ApplDescRequestVINRequestSeed
 403  01de 0000              	dc.b	page(f_ApplDescRequestVINRequestSeed),0
 404  01e0 0006              	dc.w	6
 405  01e2 05                	dc.b	5
 406  01e3 073e              	dc.w	1854
 407  01e5 04                	dc.b	4
 408  01e6 00                	dc.b	0
 410  01e7 0000              	dc.w	f_ApplDescSendVINSendKey
 411  01e9 0000              	dc.b	page(f_ApplDescSendVINSendKey),0
 412  01eb 0003              	dc.w	3
 413  01ed 0f                	dc.b	15
 414  01ee 073e              	dc.w	1854
 415  01f0 05                	dc.b	5
 416  01f1 04                	dc.b	4
 418  01f2 0cc6              	dc.w	L121f_DescOemCtrlEnableRxAndEnableTx
 419  01f4 c600              	dc.b	page(L121f_DescOemCtrlEnableRxAndEnableTx),0
 420  01f6 0003              	dc.w	3
 421  01f8 0f                	dc.b	15
 422  01f9 073e              	dc.w	1854
 423  01fb 05                	dc.b	5
 424  01fc 04                	dc.b	4
 426  01fd 0cd1              	dc.w	L321f_DescOemCtrlEnableRxAndDisableTx
 427  01ff d100              	dc.b	page(L321f_DescOemCtrlEnableRxAndDisableTx),0
 428  0201 0102              	dc.w	258
 429  0203 0f                	dc.b	15
 430  0204 073f              	dc.w	1855
 431  0206 05                	dc.b	5
 432  0207 00                	dc.b	0
 434  0208 0000              	dc.w	f_ApplDescWrite_2E2023_System_Configuration_PROXI
 435  020a 0000              	dc.b	page(f_ApplDescWrite_2E2023_System_Configuration_PROXI),0
 436  020c 0007              	dc.w	7
 437  020e 0f                	dc.b	15
 438  020f 073f              	dc.w	1855
 439  0211 05                	dc.b	5
 440  0212 00                	dc.b	0
 442  0213 0000              	dc.w	f_ApplDescWrite_222800_Heated_Seat_PWM_Calibration_Parameters_Front_Seats
 443  0215 0000              	dc.b	page(f_ApplDescWrite_222800_Heated_Seat_PWM_Calibration_Parameters_Front_Seats),0
 444  0217 0005              	dc.w	5
 445  0219 0f                	dc.b	15
 446  021a 073f              	dc.w	1855
 447  021c 05                	dc.b	5
 448  021d 00                	dc.b	0
 450  021e 0000              	dc.w	f_ApplDescWrite_222801_Vented_Seat_PWM_Calibration_Parameters
 451  0220 0000              	dc.b	page(f_ApplDescWrite_222801_Vented_Seat_PWM_Calibration_Parameters),0
 452  0222 0007              	dc.w	7
 453  0224 0f                	dc.b	15
 454  0225 073f              	dc.w	1855
 455  0227 05                	dc.b	5
 456  0228 00                	dc.b	0
 458  0229 0000              	dc.w	f_ApplDescWrite_222802_Heated_Steering_Wheel_PWM_Calibration_Parameters
 459  022b 0000              	dc.b	page(f_ApplDescWrite_222802_Heated_Steering_Wheel_PWM_Calibration_Parameters),0
 460  022d 0007              	dc.w	7
 461  022f 0f                	dc.b	15
 462  0230 073f              	dc.w	1855
 463  0232 05                	dc.b	5
 464  0233 00                	dc.b	0
 466  0234 0000              	dc.w	f_ApplDescWrite_222803_Heated_Seat_Max_Timeout_Parameters_Front_Seats
 467  0236 0000              	dc.b	page(f_ApplDescWrite_222803_Heated_Seat_Max_Timeout_Parameters_Front_Seats),0
 468  0238 0007              	dc.w	7
 469  023a 0f                	dc.b	15
 470  023b 073f              	dc.w	1855
 471  023d 05                	dc.b	5
 472  023e 00                	dc.b	0
 474  023f 0000              	dc.w	f_ApplDescWrite_222804_Heated_Steering_Wheel_Max_Timeout_Parameters
 475  0241 0000              	dc.b	page(f_ApplDescWrite_222804_Heated_Steering_Wheel_Max_Timeout_Parameters),0
 476  0243 0004              	dc.w	4
 477  0245 0f                	dc.b	15
 478  0246 073f              	dc.w	1855
 479  0248 05                	dc.b	5
 480  0249 00                	dc.b	0
 482  024a 0000              	dc.w	f_ApplDescWrite_222805_Voltage_Level_ON
 483  024c 0000              	dc.b	page(f_ApplDescWrite_222805_Voltage_Level_ON),0
 484  024e 0004              	dc.w	4
 485  0250 0f                	dc.b	15
 486  0251 073f              	dc.w	1855
 487  0253 05                	dc.b	5
 488  0254 00                	dc.b	0
 490  0255 0000              	dc.w	f_ApplDescWrite_222806_Voltage_Level_OFF
 491  0257 0000              	dc.b	page(f_ApplDescWrite_222806_Voltage_Level_OFF),0
 492  0259 0008              	dc.w	8
 493  025b 0f                	dc.b	15
 494  025c 073f              	dc.w	1855
 495  025e 05                	dc.b	5
 496  025f 00                	dc.b	0
 498  0260 0000              	dc.w	f_ApplDescWrite_222808_Heated_Seat_NTC_Cutoff_Parameters_Front_Seats
 499  0262 0000              	dc.b	page(f_ApplDescWrite_222808_Heated_Seat_NTC_Cutoff_Parameters_Front_Seats),0
 500  0264 0008              	dc.w	8
 501  0266 0f                	dc.b	15
 502  0267 073f              	dc.w	1855
 503  0269 05                	dc.b	5
 504  026a 00                	dc.b	0
 506  026b 0000              	dc.w	f_ApplDescWrite_222809_Steering_Wheel_Temperature_Cut_off_Parameters
 507  026d 0000              	dc.b	page(f_ApplDescWrite_222809_Steering_Wheel_Temperature_Cut_off_Parameters),0
 508  026f 0007              	dc.w	7
 509  0271 0f                	dc.b	15
 510  0272 073f              	dc.w	1855
 511  0274 05                	dc.b	5
 512  0275 00                	dc.b	0
 514  0276 0000              	dc.w	f_ApplDescWrite_22280A_HeatVent_Auto_Activation_Temperature_Calibration_Paramet
 515  0278 0000              	dc.b	page(f_ApplDescWrite_22280A_HeatVent_Auto_Activation_Temperature_Calibration_Paramet),0
 516  027a 0007              	dc.w	7
 517  027c 0f                	dc.b	15
 518  027d 073f              	dc.w	1855
 519  027f 05                	dc.b	5
 520  0280 00                	dc.b	0
 522  0281 0000              	dc.w	f_ApplDescWrite_22280C_Heated_Steering_Wheel_Min_Timeout_Parameters
 523  0283 0000              	dc.b	page(f_ApplDescWrite_22280C_Heated_Steering_Wheel_Min_Timeout_Parameters),0
 524  0285 0007              	dc.w	7
 525  0287 0f                	dc.b	15
 526  0288 073f              	dc.w	1855
 527  028a 05                	dc.b	5
 528  028b 00                	dc.b	0
 530  028c 0000              	dc.w	f_ApplDescWrite_22280E_Heated_Seat_PWM_Calibration_Parameters_Rear_Seats
 531  028e 0000              	dc.b	page(f_ApplDescWrite_22280E_Heated_Seat_PWM_Calibration_Parameters_Rear_Seats),0
 532  0290 0007              	dc.w	7
 533  0292 0f                	dc.b	15
 534  0293 073f              	dc.w	1855
 535  0295 05                	dc.b	5
 536  0296 00                	dc.b	0
 538  0297 0000              	dc.w	f_ApplDescWrite_22280F_Heated_Seat_Max_Timeout_Parameters_Rear_Seats
 539  0299 0000              	dc.b	page(f_ApplDescWrite_22280F_Heated_Seat_Max_Timeout_Parameters_Rear_Seats),0
 540  029b 0008              	dc.w	8
 541  029d 0f                	dc.b	15
 542  029e 073f              	dc.w	1855
 543  02a0 05                	dc.b	5
 544  02a1 00                	dc.b	0
 546  02a2 0000              	dc.w	f_ApplDescWrite_222810_Heated_Seat_NTC_Cutoff_Parameters_Rear_Seats
 547  02a4 0000              	dc.b	page(f_ApplDescWrite_222810_Heated_Seat_NTC_Cutoff_Parameters_Rear_Seats),0
 548  02a6 0012              	dc.w	18
 549  02a8 0f                	dc.b	15
 550  02a9 073f              	dc.w	1855
 551  02ab 05                	dc.b	5
 552  02ac 00                	dc.b	0
 554  02ad 0000              	dc.w	f_ApplDescWrite_223000_Program_ECU_Data_at_Flash_Station_1
 555  02af 0000              	dc.b	page(f_ApplDescWrite_223000_Program_ECU_Data_at_Flash_Station_1),0
 556  02b1 0004              	dc.w	4
 557  02b3 0f                	dc.b	15
 558  02b4 073f              	dc.w	1855
 559  02b6 05                	dc.b	5
 560  02b7 00                	dc.b	0
 562  02b8 0000              	dc.w	f_ApplDescWrite_223001_Program_ECU_Data_at_Flash_Station_2
 563  02ba 0000              	dc.b	page(f_ApplDescWrite_223001_Program_ECU_Data_at_Flash_Station_2),0
 564  02bc 0011              	dc.w	17
 565  02be 0f                	dc.b	15
 566  02bf 073f              	dc.w	1855
 567  02c1 05                	dc.b	5
 568  02c2 00                	dc.b	0
 570  02c3 0000              	dc.w	f_ApplDescWriteEcuSerialNumber
 571  02c5 0000              	dc.b	page(f_ApplDescWriteEcuSerialNumber),0
 572  02c7 0014              	dc.w	20
 573  02c9 05                	dc.b	5
 574  02ca 023c              	dc.w	572
 575  02cc 05                	dc.b	5
 576  02cd 00                	dc.b	0
 578  02ce 0000              	dc.w	f_ApplDescWriteVIN
 579  02d0 0000              	dc.b	page(f_ApplDescWriteVIN),0
 580  02d2 0004              	dc.w	4
 581  02d4 05                	dc.b	5
 582  02d5 073c              	dc.w	1852
 583  02d7 05                	dc.b	5
 584  02d8 00                	dc.b	0
 586  02d9 0000              	dc.w	f_ApplDescReturnControl_2F500103_Actuate_all_heaters
 587  02db 0000              	dc.b	page(f_ApplDescReturnControl_2F500103_Actuate_all_heaters),0
 588  02dd 0005              	dc.w	5
 589  02df 05                	dc.b	5
 590  02e0 073c              	dc.w	1852
 591  02e2 05                	dc.b	5
 592  02e3 00                	dc.b	0
 594  02e4 0000              	dc.w	f_ApplDescControl_2F500103_Actuate_all_heaters
 595  02e6 0000              	dc.b	page(f_ApplDescControl_2F500103_Actuate_all_heaters),0
 596  02e8 0004              	dc.w	4
 597  02ea 05                	dc.b	5
 598  02eb 073c              	dc.w	1852
 599  02ed 05                	dc.b	5
 600  02ee 00                	dc.b	0
 602  02ef 0000              	dc.w	f_ApplDescReturnControl_2F500203_Actuate_all_Vents
 603  02f1 0000              	dc.b	page(f_ApplDescReturnControl_2F500203_Actuate_all_Vents),0
 604  02f3 0005              	dc.w	5
 605  02f5 05                	dc.b	5
 606  02f6 073c              	dc.w	1852
 607  02f8 05                	dc.b	5
 608  02f9 00                	dc.b	0
 610  02fa 0000              	dc.w	f_ApplDescControl_2F500203_Actuate_all_Vents
 611  02fc 0000              	dc.b	page(f_ApplDescControl_2F500203_Actuate_all_Vents),0
 612  02fe 0004              	dc.w	4
 613  0300 05                	dc.b	5
 614  0301 073c              	dc.w	1852
 615  0303 05                	dc.b	5
 616  0304 00                	dc.b	0
 618  0305 0000              	dc.w	f_ApplDescReturnControl_2F500303_F_L_Heat_Seat_Request_BI
 619  0307 0000              	dc.b	page(f_ApplDescReturnControl_2F500303_F_L_Heat_Seat_Request_BI),0
 620  0309 0005              	dc.w	5
 621  030b 05                	dc.b	5
 622  030c 073c              	dc.w	1852
 623  030e 05                	dc.b	5
 624  030f 00                	dc.b	0
 626  0310 0000              	dc.w	f_ApplDescControl_2F500303_F_L_Heat_Seat_Request_BI
 627  0312 0000              	dc.b	page(f_ApplDescControl_2F500303_F_L_Heat_Seat_Request_BI),0
 628  0314 0004              	dc.w	4
 629  0316 05                	dc.b	5
 630  0317 073c              	dc.w	1852
 631  0319 05                	dc.b	5
 632  031a 00                	dc.b	0
 634  031b 0000              	dc.w	f_ApplDescReturnControl_2F500403_F_R_Heat_Seat_Request_BI
 635  031d 0000              	dc.b	page(f_ApplDescReturnControl_2F500403_F_R_Heat_Seat_Request_BI),0
 636  031f 0005              	dc.w	5
 637  0321 05                	dc.b	5
 638  0322 073c              	dc.w	1852
 639  0324 05                	dc.b	5
 640  0325 00                	dc.b	0
 642  0326 0000              	dc.w	f_ApplDescControl_2F500403_F_R_Heat_Seat_Request_BI
 643  0328 0000              	dc.b	page(f_ApplDescControl_2F500403_F_R_Heat_Seat_Request_BI),0
 644  032a 0004              	dc.w	4
 645  032c 05                	dc.b	5
 646  032d 073c              	dc.w	1852
 647  032f 05                	dc.b	5
 648  0330 00                	dc.b	0
 650  0331 0000              	dc.w	f_ApplDescReturnControl_2F500503_R_L_Heat_Seat_Request_BI
 651  0333 0000              	dc.b	page(f_ApplDescReturnControl_2F500503_R_L_Heat_Seat_Request_BI),0
 652  0335 0005              	dc.w	5
 653  0337 05                	dc.b	5
 654  0338 073c              	dc.w	1852
 655  033a 05                	dc.b	5
 656  033b 00                	dc.b	0
 658  033c 0000              	dc.w	f_ApplDescControl_2F500503_R_L_Heat_Seat_Request_BI
 659  033e 0000              	dc.b	page(f_ApplDescControl_2F500503_R_L_Heat_Seat_Request_BI),0
 660  0340 0004              	dc.w	4
 661  0342 05                	dc.b	5
 662  0343 073c              	dc.w	1852
 663  0345 05                	dc.b	5
 664  0346 00                	dc.b	0
 666  0347 0000              	dc.w	f_ApplDescReturnControl_2F500603_R_R_Heat_Seat_Request_BI
 667  0349 0000              	dc.b	page(f_ApplDescReturnControl_2F500603_R_R_Heat_Seat_Request_BI),0
 668  034b 0005              	dc.w	5
 669  034d 05                	dc.b	5
 670  034e 073c              	dc.w	1852
 671  0350 05                	dc.b	5
 672  0351 00                	dc.b	0
 674  0352 0000              	dc.w	f_ApplDescControl_2F500603_R_R_Heat_Seat_Request_BI
 675  0354 0000              	dc.b	page(f_ApplDescControl_2F500603_R_R_Heat_Seat_Request_BI),0
 676  0356 0004              	dc.w	4
 677  0358 05                	dc.b	5
 678  0359 073c              	dc.w	1852
 679  035b 05                	dc.b	5
 680  035c 00                	dc.b	0
 682  035d 0000              	dc.w	f_ApplDescReturnControl_2F500703_F_L_Vent_Seat_Request_BI
 683  035f 0000              	dc.b	page(f_ApplDescReturnControl_2F500703_F_L_Vent_Seat_Request_BI),0
 684  0361 0005              	dc.w	5
 685  0363 05                	dc.b	5
 686  0364 073c              	dc.w	1852
 687  0366 05                	dc.b	5
 688  0367 00                	dc.b	0
 690  0368 0000              	dc.w	f_ApplDescControl_2F500703_F_L_Vent_Seat_Request_BI
 691  036a 0000              	dc.b	page(f_ApplDescControl_2F500703_F_L_Vent_Seat_Request_BI),0
 692  036c 0004              	dc.w	4
 693  036e 05                	dc.b	5
 694  036f 073c              	dc.w	1852
 695  0371 05                	dc.b	5
 696  0372 00                	dc.b	0
 698  0373 0000              	dc.w	f_ApplDescReturnControl_2F500803_F_R_Vent_Seat_Request_BI
 699  0375 0000              	dc.b	page(f_ApplDescReturnControl_2F500803_F_R_Vent_Seat_Request_BI),0
 700  0377 0005              	dc.w	5
 701  0379 05                	dc.b	5
 702  037a 073c              	dc.w	1852
 703  037c 05                	dc.b	5
 704  037d 00                	dc.b	0
 706  037e 0000              	dc.w	f_ApplDescControl_2F500803_F_R_Vent_Seat_Request_BI
 707  0380 0000              	dc.b	page(f_ApplDescControl_2F500803_F_R_Vent_Seat_Request_BI),0
 708  0382 0004              	dc.w	4
 709  0384 05                	dc.b	5
 710  0385 073f              	dc.w	1855
 711  0387 05                	dc.b	5
 712  0388 00                	dc.b	0
 714  0389 0000              	dc.w	f_ApplDescReturnControl_2F500903_Heated_Steering_Wheel_Request_BI
 715  038b 0000              	dc.b	page(f_ApplDescReturnControl_2F500903_Heated_Steering_Wheel_Request_BI),0
 716  038d 0005              	dc.w	5
 717  038f 05                	dc.b	5
 718  0390 073c              	dc.w	1852
 719  0392 05                	dc.b	5
 720  0393 00                	dc.b	0
 722  0394 0000              	dc.w	f_ApplDescControl_2F500903_Heated_Steering_Wheel_Request_BI
 723  0396 0000              	dc.b	page(f_ApplDescControl_2F500903_Heated_Steering_Wheel_Request_BI),0
 724  0398 0004              	dc.w	4
 725  039a 05                	dc.b	5
 726  039b 073c              	dc.w	1852
 727  039d 05                	dc.b	5
 728  039e 00                	dc.b	0
 730  039f 0000              	dc.w	f_ApplDescReturnControl_2F500A03_F_L_Heat_Seat_Status_BO
 731  03a1 0000              	dc.b	page(f_ApplDescReturnControl_2F500A03_F_L_Heat_Seat_Status_BO),0
 732  03a3 0005              	dc.w	5
 733  03a5 05                	dc.b	5
 734  03a6 073c              	dc.w	1852
 735  03a8 05                	dc.b	5
 736  03a9 00                	dc.b	0
 738  03aa 0000              	dc.w	f_ApplDescControl_2F500A03_F_L_Heat_Seat_Status_BO
 739  03ac 0000              	dc.b	page(f_ApplDescControl_2F500A03_F_L_Heat_Seat_Status_BO),0
 740  03ae 0004              	dc.w	4
 741  03b0 05                	dc.b	5
 742  03b1 073c              	dc.w	1852
 743  03b3 05                	dc.b	5
 744  03b4 00                	dc.b	0
 746  03b5 0000              	dc.w	f_ApplDescReturnControl_2F500B03_F_R_Heat_Seat_Status_BO
 747  03b7 0000              	dc.b	page(f_ApplDescReturnControl_2F500B03_F_R_Heat_Seat_Status_BO),0
 748  03b9 0005              	dc.w	5
 749  03bb 05                	dc.b	5
 750  03bc 073c              	dc.w	1852
 751  03be 05                	dc.b	5
 752  03bf 00                	dc.b	0
 754  03c0 0000              	dc.w	f_ApplDescControl_2F500B03_F_R_Heat_Seat_Status_BO
 755  03c2 0000              	dc.b	page(f_ApplDescControl_2F500B03_F_R_Heat_Seat_Status_BO),0
 756  03c4 0004              	dc.w	4
 757  03c6 05                	dc.b	5
 758  03c7 073c              	dc.w	1852
 759  03c9 05                	dc.b	5
 760  03ca 00                	dc.b	0
 762  03cb 0000              	dc.w	f_ApplDescReturnControl_2F500C03_R_L_Heat_Seat_Status_BO
 763  03cd 0000              	dc.b	page(f_ApplDescReturnControl_2F500C03_R_L_Heat_Seat_Status_BO),0
 764  03cf 0005              	dc.w	5
 765  03d1 05                	dc.b	5
 766  03d2 073c              	dc.w	1852
 767  03d4 05                	dc.b	5
 768  03d5 00                	dc.b	0
 770  03d6 0000              	dc.w	f_ApplDescControl_2F500C03_R_L_Heat_Seat_Status_BO
 771  03d8 0000              	dc.b	page(f_ApplDescControl_2F500C03_R_L_Heat_Seat_Status_BO),0
 772  03da 0004              	dc.w	4
 773  03dc 05                	dc.b	5
 774  03dd 073c              	dc.w	1852
 775  03df 05                	dc.b	5
 776  03e0 00                	dc.b	0
 778  03e1 0000              	dc.w	f_ApplDescReturnControl_2F500D03_R_R_Heat_Seat_Status_BO
 779  03e3 0000              	dc.b	page(f_ApplDescReturnControl_2F500D03_R_R_Heat_Seat_Status_BO),0
 780  03e5 0005              	dc.w	5
 781  03e7 05                	dc.b	5
 782  03e8 073c              	dc.w	1852
 783  03ea 05                	dc.b	5
 784  03eb 00                	dc.b	0
 786  03ec 0000              	dc.w	f_ApplDescControl_2F500D03_R_R_Heat_Seat_Status_BO
 787  03ee 0000              	dc.b	page(f_ApplDescControl_2F500D03_R_R_Heat_Seat_Status_BO),0
 788  03f0 0004              	dc.w	4
 789  03f2 05                	dc.b	5
 790  03f3 073c              	dc.w	1852
 791  03f5 05                	dc.b	5
 792  03f6 00                	dc.b	0
 794  03f7 0000              	dc.w	f_ApplDescReturnControl_2F500E03_F_L_Vent_Seat_Status_BO
 795  03f9 0000              	dc.b	page(f_ApplDescReturnControl_2F500E03_F_L_Vent_Seat_Status_BO),0
 796  03fb 0005              	dc.w	5
 797  03fd 05                	dc.b	5
 798  03fe 073c              	dc.w	1852
 799  0400 05                	dc.b	5
 800  0401 00                	dc.b	0
 802  0402 0000              	dc.w	f_ApplDescControl_2F500E03_F_L_Vent_Seat_Status_BO
 803  0404 0000              	dc.b	page(f_ApplDescControl_2F500E03_F_L_Vent_Seat_Status_BO),0
 804  0406 0004              	dc.w	4
 805  0408 05                	dc.b	5
 806  0409 073c              	dc.w	1852
 807  040b 05                	dc.b	5
 808  040c 00                	dc.b	0
 810  040d 0000              	dc.w	f_ApplDescReturnControl_2F500F03_F_R_Vent_Seat_Status_BO
 811  040f 0000              	dc.b	page(f_ApplDescReturnControl_2F500F03_F_R_Vent_Seat_Status_BO),0
 812  0411 0005              	dc.w	5
 813  0413 05                	dc.b	5
 814  0414 073c              	dc.w	1852
 815  0416 05                	dc.b	5
 816  0417 00                	dc.b	0
 818  0418 0000              	dc.w	f_ApplDescControl_2F500F03_F_R_Vent_Seat_Status_BO
 819  041a 0000              	dc.b	page(f_ApplDescControl_2F500F03_F_R_Vent_Seat_Status_BO),0
 820  041c 0004              	dc.w	4
 821  041e 05                	dc.b	5
 822  041f 073c              	dc.w	1852
 823  0421 05                	dc.b	5
 824  0422 00                	dc.b	0
 826  0423 0000              	dc.w	f_ApplDescReturnControl_2F501003_Heated_Steering_Wheel_Status_BO
 827  0425 0000              	dc.b	page(f_ApplDescReturnControl_2F501003_Heated_Steering_Wheel_Status_BO),0
 828  0427 0005              	dc.w	5
 829  0429 05                	dc.b	5
 830  042a 073c              	dc.w	1852
 831  042c 05                	dc.b	5
 832  042d 00                	dc.b	0
 834  042e 0000              	dc.w	f_ApplDescControl_2F501003_Heated_Steering_Wheel_Status_BO
 835  0430 0000              	dc.b	page(f_ApplDescControl_2F501003_Heated_Steering_Wheel_Status_BO),0
 836  0432 0004              	dc.w	4
 837  0434 05                	dc.b	5
 838  0435 073c              	dc.w	1852
 839  0437 05                	dc.b	5
 840  0438 00                	dc.b	0
 842  0439 0000              	dc.w	f_ApplDescReturnControl_2F501103_Load_shedding_status_BI
 843  043b 0000              	dc.b	page(f_ApplDescReturnControl_2F501103_Load_shedding_status_BI),0
 844  043d 0005              	dc.w	5
 845  043f 05                	dc.b	5
 846  0440 073c              	dc.w	1852
 847  0442 05                	dc.b	5
 848  0443 00                	dc.b	0
 850  0444 0000              	dc.w	f_ApplDescControl_2F501103_Load_shedding_status_BI
 851  0446 0000              	dc.b	page(f_ApplDescControl_2F501103_Load_shedding_status_BI),0
 852  0448 0004              	dc.w	4
 853  044a 05                	dc.b	5
 854  044b 073c              	dc.w	1852
 855  044d 05                	dc.b	5
 856  044e 00                	dc.b	0
 858  044f 0000              	dc.w	f_ApplDescReturnControl_2F501203_Ignition_Status
 859  0451 0000              	dc.b	page(f_ApplDescReturnControl_2F501203_Ignition_Status),0
 860  0453 0005              	dc.w	5
 861  0455 05                	dc.b	5
 862  0456 073c              	dc.w	1852
 863  0458 05                	dc.b	5
 864  0459 00                	dc.b	0
 866  045a 0000              	dc.w	f_ApplDescControl_2F501203_Ignition_Status
 867  045c 0000              	dc.b	page(f_ApplDescControl_2F501203_Ignition_Status),0
 868  045e 0004              	dc.w	4
 869  0460 05                	dc.b	5
 870  0461 073c              	dc.w	1852
 871  0463 05                	dc.b	5
 872  0464 00                	dc.b	0
 874  0465 0000              	dc.w	f_ApplDescReturnControl_2F501303_Flash_Program_Data_1
 875  0467 0000              	dc.b	page(f_ApplDescReturnControl_2F501303_Flash_Program_Data_1),0
 876  0469 0005              	dc.w	5
 877  046b 05                	dc.b	5
 878  046c 073c              	dc.w	1852
 879  046e 05                	dc.b	5
 880  046f 00                	dc.b	0
 882  0470 0000              	dc.w	f_ApplDescControl_2F501303_Flash_Program_Data_1
 883  0472 0000              	dc.b	page(f_ApplDescControl_2F501303_Flash_Program_Data_1),0
 884  0474 0004              	dc.w	4
 885  0476 05                	dc.b	5
 886  0477 073c              	dc.w	1852
 887  0479 05                	dc.b	5
 888  047a 00                	dc.b	0
 890  047b 0000              	dc.w	f_ApplDescReturnControl_2F501403_Flash_Program_Data_2
 891  047d 0000              	dc.b	page(f_ApplDescReturnControl_2F501403_Flash_Program_Data_2),0
 892  047f 0005              	dc.w	5
 893  0481 05                	dc.b	5
 894  0482 073c              	dc.w	1852
 895  0484 05                	dc.b	5
 896  0485 00                	dc.b	0
 898  0486 0000              	dc.w	f_ApplDescControl_2F501403_Flash_Program_Data_2
 899  0488 0000              	dc.b	page(f_ApplDescControl_2F501403_Flash_Program_Data_2),0
 900  048a 0004              	dc.w	4
 901  048c 05                	dc.b	5
 902  048d 073c              	dc.w	1852
 903  048f 05                	dc.b	5
 904  0490 00                	dc.b	0
 906  0491 0000              	dc.w	f_ApplDescReturnControl_2F501503_Flash_Program_Data_3
 907  0493 0000              	dc.b	page(f_ApplDescReturnControl_2F501503_Flash_Program_Data_3),0
 908  0495 0005              	dc.w	5
 909  0497 05                	dc.b	5
 910  0498 073c              	dc.w	1852
 911  049a 05                	dc.b	5
 912  049b 00                	dc.b	0
 914  049c 0000              	dc.w	f_ApplDescControl_2F501503_Flash_Program_Data_3
 915  049e 0000              	dc.b	page(f_ApplDescControl_2F501503_Flash_Program_Data_3),0
 916  04a0 0004              	dc.w	4
 917  04a2 05                	dc.b	5
 918  04a3 073c              	dc.w	1852
 919  04a5 05                	dc.b	5
 920  04a6 00                	dc.b	0
 922  04a7 0000              	dc.w	f_ApplDescReturnControl_2F501603_Flash_Program_Data_4
 923  04a9 0000              	dc.b	page(f_ApplDescReturnControl_2F501603_Flash_Program_Data_4),0
 924  04ab 0005              	dc.w	5
 925  04ad 05                	dc.b	5
 926  04ae 073c              	dc.w	1852
 927  04b0 05                	dc.b	5
 928  04b1 00                	dc.b	0
 930  04b2 0000              	dc.w	f_ApplDescControl_2F501603_Flash_Program_Data_4
 931  04b4 0000              	dc.b	page(f_ApplDescControl_2F501603_Flash_Program_Data_4),0
 932  04b6 0008              	dc.w	8
 933  04b8 0f                	dc.b	15
 934  04b9 073f              	dc.w	1855
 935  04bb 05                	dc.b	5
 936  04bc 00                	dc.b	0
 938  04bd 0000              	dc.w	f_ApplDescStart_31010201_Set_Fault_Status
 939  04bf 0000              	dc.b	page(f_ApplDescStart_31010201_Set_Fault_Status),0
 940  04c1 0005              	dc.w	5
 941  04c3 0f                	dc.b	15
 942  04c4 073f              	dc.w	1855
 943  04c6 05                	dc.b	5
 944  04c7 00                	dc.b	0
 946  04c8 0000              	dc.w	f_ApplDescStart_31010202_Request_Echo_Routine
 947  04ca 0000              	dc.b	page(f_ApplDescStart_31010202_Request_Echo_Routine),0
 948  04cc 0004              	dc.w	4
 949  04ce 0f                	dc.b	15
 950  04cf 073f              	dc.w	1855
 951  04d1 05                	dc.b	5
 952  04d2 00                	dc.b	0
 954  04d3 0000              	dc.w	f_ApplDescStart_31010203_CSWM_Ouput_Test
 955  04d5 0000              	dc.b	page(f_ApplDescStart_31010203_CSWM_Ouput_Test),0
 956  04d7 000a              	dc.w	10
 957  04d9 0f                	dc.b	15
 958  04da 043a              	dc.w	1082
 959  04dc 05                	dc.b	5
 960  04dd 00                	dc.b	0
 962  04de 0000              	dc.w	f_ApplDescStartFlashErase
 963  04e0 0000              	dc.b	page(f_ApplDescStartFlashErase),0
 964  04e2 000c              	dc.w	12
 965  04e4 0f                	dc.b	15
 966  04e5 043a              	dc.w	1082
 967  04e7 05                	dc.b	5
 968  04e8 00                	dc.b	0
 970  04e9 0000              	dc.w	f_ApplDescStartcheckProgrammingDependencies_FlashChecksum
 971  04eb 0000              	dc.b	page(f_ApplDescStartcheckProgrammingDependencies_FlashChecksum),0
 972  04ed 0004              	dc.w	4
 973  04ef 0f                	dc.b	15
 974  04f0 073f              	dc.w	1855
 975  04f2 05                	dc.b	5
 976  04f3 00                	dc.b	0
 978  04f4 0000              	dc.w	f_ApplDescStop_31010203_CSWM_Ouput_Test
 979  04f6 0000              	dc.b	page(f_ApplDescStop_31010203_CSWM_Ouput_Test),0
 980  04f8 0004              	dc.w	4
 981  04fa 0f                	dc.b	15
 982  04fb 043a              	dc.w	1082
 983  04fd 05                	dc.b	5
 984  04fe 00                	dc.b	0
 986  04ff 0000              	dc.w	f_ApplDescStopFlashErase
 987  0501 0000              	dc.b	page(f_ApplDescStopFlashErase),0
 988  0503 0004              	dc.w	4
 989  0505 0f                	dc.b	15
 990  0506 043a              	dc.w	1082
 991  0508 05                	dc.b	5
 992  0509 00                	dc.b	0
 994  050a 0000              	dc.w	f_ApplDescStopcheckProgrammingDependencies_FlashChecksum
 995  050c 0000              	dc.b	page(f_ApplDescStopcheckProgrammingDependencies_FlashChecksum),0
 996  050e 0004              	dc.w	4
 997  0510 0f                	dc.b	15
 998  0511 073f              	dc.w	1855
 999  0513 05                	dc.b	5
1000  0514 00                	dc.b	0
1002  0515 0000              	dc.w	f_ApplDescReqResults_31010203_CSWM_Ouput_Test
1003  0517 0000              	dc.b	page(f_ApplDescReqResults_31010203_CSWM_Ouput_Test),0
1004  0519 0004              	dc.w	4
1005  051b 0f                	dc.b	15
1006  051c 043a              	dc.w	1082
1007  051e 05                	dc.b	5
1008  051f 00                	dc.b	0
1010  0520 0000              	dc.w	f_ApplDescReqResultsFlashErase
1011  0522 0000              	dc.b	page(f_ApplDescReqResultsFlashErase),0
1012  0524 0004              	dc.w	4
1013  0526 0f                	dc.b	15
1014  0527 043a              	dc.w	1082
1015  0529 05                	dc.b	5
1016  052a 00                	dc.b	0
1018  052b 0000              	dc.w	f_ApplDescReqResultscheckProgrammingDependencies_FlashChecksum
1019  052d 0000              	dc.b	page(f_ApplDescReqResultscheckProgrammingDependencies_FlashChecksum),0
1020  052f 0009              	dc.w	9
1021  0531 05                	dc.b	5
1022  0532 073a              	dc.w	1850
1023  0534 05                	dc.b	5
1024  0535 00                	dc.b	0
1026  0536 0000              	dc.w	f_ApplDescRequestDownload
1027  0538 0000              	dc.b	page(f_ApplDescRequestDownload),0
1028  053a 0000              	dc.w	0
1029  053c 05                	dc.b	5
1030  053d 073a              	dc.w	1850
1031  053f 05                	dc.b	5
1032  0540 00                	dc.b	0
1034  0541 0000              	dc.w	f_ApplDescTransferDataDownload
1035  0543 0000              	dc.b	page(f_ApplDescTransferDataDownload),0
1036  0545 0001              	dc.w	1
1037  0547 05                	dc.b	5
1038  0548 073a              	dc.w	1850
1039  054a 05                	dc.b	5
1040  054b 00                	dc.b	0
1042  054c 0000              	dc.w	f_ApplDescReqTransferExitDownload
1043  054e 0000              	dc.b	page(f_ApplDescReqTransferExitDownload),0
1044  0550 0002              	dc.w	2
1045  0552 0f                	dc.b	15
1046  0553 073f              	dc.w	1855
1047  0555 05                	dc.b	5
1048  0556 00                	dc.b	0
1050  0557 0000              	dc.w	f_ApplDescTester_PrresentTesterPresent
1051  0559 0000              	dc.b	page(f_ApplDescTester_PrresentTesterPresent),0
1052  055b 0002              	dc.w	2
1053  055d 0f                	dc.b	15
1054  055e 073e              	dc.w	1854
1055  0560 05                	dc.b	5
1056  0561 00                	dc.b	0
1058  0562 0000              	dc.w	f_ApplDescCtrlDtcSettingOn
1059  0564 0000              	dc.b	page(f_ApplDescCtrlDtcSettingOn),0
1060  0566 0002              	dc.w	2
1061  0568 0f                	dc.b	15
1062  0569 073e              	dc.w	1854
1063  056b 05                	dc.b	5
1064  056c 00                	dc.b	0
1066  056d 0000              	dc.w	f_ApplDescCtrlDtcSettingOff
1067  056f 0000              	dc.b	page(f_ApplDescCtrlDtcSettingOff),0
1068  0571                   L162_g_descSvcInstHeadExt:
1069  0571 01                	dc.b	1
1070  0572 02                	dc.b	2
1071  0573 03                	dc.b	3
1072  0574 01                	dc.b	1
1073  0575 02                	dc.b	2
1074  0576 04                	dc.b	4
1075  0577 06                	dc.b	6
1076  0578 07                	dc.b	7
1077  0579 08                	dc.b	8
1078  057a 09                	dc.b	9
1079  057b 0c                	dc.b	12
1080  057c 0e                	dc.b	14
1081  057d 01                	dc.b	1
1082  057e 02                	dc.b	2
1083  057f 05                	dc.b	5
1084  0580 06                	dc.b	6
1085  0581 00                	dc.b	0
1086  0582 01                	dc.b	1
1087  0583 20                	dc.b	32
1088  0584 23                	dc.b	35
1089  0585 28                	dc.b	40
1090  0586 00                	dc.b	0
1091  0587 28                	dc.b	40
1092  0588 01                	dc.b	1
1093  0589 28                	dc.b	40
1094  058a 02                	dc.b	2
1095  058b 28                	dc.b	40
1096  058c 03                	dc.b	3
1097  058d 28                	dc.b	40
1098  058e 04                	dc.b	4
1099  058f 28                	dc.b	40
1100  0590 05                	dc.b	5
1101  0591 28                	dc.b	40
1102  0592 06                	dc.b	6
1103  0593 28                	dc.b	40
1104  0594 08                	dc.b	8
1105  0595 28                	dc.b	40
1106  0596 09                	dc.b	9
1107  0597 28                	dc.b	40
1108  0598 0a                	dc.b	10
1109  0599 28                	dc.b	40
1110  059a 0c                	dc.b	12
1111  059b 28                	dc.b	40
1112  059c 0e                	dc.b	14
1113  059d 28                	dc.b	40
1114  059e 0f                	dc.b	15
1115  059f 28                	dc.b	40
1116  05a0 10                	dc.b	16
1117  05a1 30                	dc.b	48
1118  05a2 00                	dc.b	0
1119  05a3 30                	dc.b	48
1120  05a4 01                	dc.b	1
1121  05a5 f1                	dc.b	241
1122  05a6 8c                	dc.b	140
1123  05a7 f1                	dc.b	241
1124  05a8 90                	dc.b	144
1125  05a9 50                	dc.b	80
1126  05aa 01                	dc.b	1
1127  05ab 00                	dc.b	0
1128  05ac 50                	dc.b	80
1129  05ad 01                	dc.b	1
1130  05ae 03                	dc.b	3
1131  05af 50                	dc.b	80
1132  05b0 02                	dc.b	2
1133  05b1 00                	dc.b	0
1134  05b2 50                	dc.b	80
1135  05b3 02                	dc.b	2
1136  05b4 03                	dc.b	3
1137  05b5 50                	dc.b	80
1138  05b6 03                	dc.b	3
1139  05b7 00                	dc.b	0
1140  05b8 50                	dc.b	80
1141  05b9 03                	dc.b	3
1142  05ba 03                	dc.b	3
1143  05bb 50                	dc.b	80
1144  05bc 04                	dc.b	4
1145  05bd 00                	dc.b	0
1146  05be 50                	dc.b	80
1147  05bf 04                	dc.b	4
1148  05c0 03                	dc.b	3
1149  05c1 50                	dc.b	80
1150  05c2 05                	dc.b	5
1151  05c3 00                	dc.b	0
1152  05c4 50                	dc.b	80
1153  05c5 05                	dc.b	5
1154  05c6 03                	dc.b	3
1155  05c7 50                	dc.b	80
1156  05c8 06                	dc.b	6
1157  05c9 00                	dc.b	0
1158  05ca 50                	dc.b	80
1159  05cb 06                	dc.b	6
1160  05cc 03                	dc.b	3
1161  05cd 50                	dc.b	80
1162  05ce 07                	dc.b	7
1163  05cf 00                	dc.b	0
1164  05d0 50                	dc.b	80
1165  05d1 07                	dc.b	7
1166  05d2 03                	dc.b	3
1167  05d3 50                	dc.b	80
1168  05d4 08                	dc.b	8
1169  05d5 00                	dc.b	0
1170  05d6 50                	dc.b	80
1171  05d7 08                	dc.b	8
1172  05d8 03                	dc.b	3
1173  05d9 50                	dc.b	80
1174  05da 09                	dc.b	9
1175  05db 00                	dc.b	0
1176  05dc 50                	dc.b	80
1177  05dd 09                	dc.b	9
1178  05de 03                	dc.b	3
1179  05df 50                	dc.b	80
1180  05e0 0a                	dc.b	10
1181  05e1 00                	dc.b	0
1182  05e2 50                	dc.b	80
1183  05e3 0a                	dc.b	10
1184  05e4 03                	dc.b	3
1185  05e5 50                	dc.b	80
1186  05e6 0b                	dc.b	11
1187  05e7 00                	dc.b	0
1188  05e8 50                	dc.b	80
1189  05e9 0b                	dc.b	11
1190  05ea 03                	dc.b	3
1191  05eb 50                	dc.b	80
1192  05ec 0c                	dc.b	12
1193  05ed 00                	dc.b	0
1194  05ee 50                	dc.b	80
1195  05ef 0c                	dc.b	12
1196  05f0 03                	dc.b	3
1197  05f1 50                	dc.b	80
1198  05f2 0d                	dc.b	13
1199  05f3 00                	dc.b	0
1200  05f4 50                	dc.b	80
1201  05f5 0d                	dc.b	13
1202  05f6 03                	dc.b	3
1203  05f7 50                	dc.b	80
1204  05f8 0e                	dc.b	14
1205  05f9 00                	dc.b	0
1206  05fa 50                	dc.b	80
1207  05fb 0e                	dc.b	14
1208  05fc 03                	dc.b	3
1209  05fd 50                	dc.b	80
1210  05fe 0f                	dc.b	15
1211  05ff 00                	dc.b	0
1212  0600 50                	dc.b	80
1213  0601 0f                	dc.b	15
1214  0602 03                	dc.b	3
1215  0603 50                	dc.b	80
1216  0604 10                	dc.b	16
1217  0605 00                	dc.b	0
1218  0606 50                	dc.b	80
1219  0607 10                	dc.b	16
1220  0608 03                	dc.b	3
1221  0609 50                	dc.b	80
1222  060a 11                	dc.b	17
1223  060b 00                	dc.b	0
1224  060c 50                	dc.b	80
1225  060d 11                	dc.b	17
1226  060e 03                	dc.b	3
1227  060f 50                	dc.b	80
1228  0610 12                	dc.b	18
1229  0611 00                	dc.b	0
1230  0612 50                	dc.b	80
1231  0613 12                	dc.b	18
1232  0614 03                	dc.b	3
1233  0615 50                	dc.b	80
1234  0616 13                	dc.b	19
1235  0617 00                	dc.b	0
1236  0618 50                	dc.b	80
1237  0619 13                	dc.b	19
1238  061a 03                	dc.b	3
1239  061b 50                	dc.b	80
1240  061c 14                	dc.b	20
1241  061d 00                	dc.b	0
1242  061e 50                	dc.b	80
1243  061f 14                	dc.b	20
1244  0620 03                	dc.b	3
1245  0621 50                	dc.b	80
1246  0622 15                	dc.b	21
1247  0623 00                	dc.b	0
1248  0624 50                	dc.b	80
1249  0625 15                	dc.b	21
1250  0626 03                	dc.b	3
1251  0627 50                	dc.b	80
1252  0628 16                	dc.b	22
1253  0629 00                	dc.b	0
1254  062a 50                	dc.b	80
1255  062b 16                	dc.b	22
1256  062c 03                	dc.b	3
1257  062d 01                	dc.b	1
1258  062e 02                	dc.b	2
1259  062f 01                	dc.b	1
1260  0630 01                	dc.b	1
1261  0631 02                	dc.b	2
1262  0632 02                	dc.b	2
1263  0633 01                	dc.b	1
1264  0634 02                	dc.b	2
1265  0635 03                	dc.b	3
1266  0636 01                	dc.b	1
1267  0637 ff                	dc.b	255
1268  0638 00                	dc.b	0
1269  0639 01                	dc.b	1
1270  063a ff                	dc.b	255
1271  063b 01                	dc.b	1
1272  063c 02                	dc.b	2
1273  063d 02                	dc.b	2
1274  063e 03                	dc.b	3
1275  063f 02                	dc.b	2
1276  0640 ff                	dc.b	255
1277  0641 00                	dc.b	0
1278  0642 02                	dc.b	2
1279  0643 ff                	dc.b	255
1280  0644 01                	dc.b	1
1281  0645 03                	dc.b	3
1282  0646 02                	dc.b	2
1283  0647 03                	dc.b	3
1284  0648 03                	dc.b	3
1285  0649 ff                	dc.b	255
1286  064a 00                	dc.b	0
1287  064b 03                	dc.b	3
1288  064c ff                	dc.b	255
1289  064d 01                	dc.b	1
1290  064e 00                	dc.b	0
1291  064f 01                	dc.b	1
1292  0650 02                	dc.b	2
1293  0651                   L362_g_descPIDInfo:
1294  0651 0101              	dc.w	257
1295  0653 0005              	dc.w	5
1296  0655 073f              	dc.w	1855
1297  0657 0f                	dc.b	15
1299  0658 0000              	dc.w	f_ApplDescRead_220101_Bussed_Outputs_Status
1300  065a 0000              	dc.b	page(f_ApplDescRead_220101_Bussed_Outputs_Status),0
1301  065c 0102              	dc.w	258
1302  065e 0008              	dc.w	8
1303  0660 073f              	dc.w	1855
1304  0662 0f                	dc.b	15
1306  0663 0000              	dc.w	f_ApplDescRead_220102_Bussed_Input
1307  0665 0000              	dc.b	page(f_ApplDescRead_220102_Bussed_Input),0
1308  0667 0103              	dc.w	259
1309  0669 0001              	dc.w	1
1310  066b 073f              	dc.w	1855
1311  066d 0f                	dc.b	15
1313  066e 0000              	dc.w	f_ApplDescRead_220103_Rear_Seat_Switch_Input_Status
1314  0670 0000              	dc.b	page(f_ApplDescRead_220103_Rear_Seat_Switch_Input_Status),0
1315  0672 0104              	dc.w	260
1316  0674 0002              	dc.w	2
1317  0676 073f              	dc.w	1855
1318  0678 0f                	dc.b	15
1320  0679 0000              	dc.w	f_ApplDescRead_220104_Conditions_to_activate_CSWM_outputs
1321  067b 0000              	dc.b	page(f_ApplDescRead_220104_Conditions_to_activate_CSWM_outputs),0
1322  067d 0105              	dc.w	261
1323  067f 0008              	dc.w	8
1324  0681 073f              	dc.w	1855
1325  0683 0f                	dc.b	15
1327  0684 0000              	dc.w	f_ApplDescRead_220105_Heated_Seat_NTC_Feedback_Sensor_Status
1328  0686 0000              	dc.b	page(f_ApplDescRead_220105_Heated_Seat_NTC_Feedback_Sensor_Status),0
1329  0688 0106              	dc.w	262
1330  068a 0007              	dc.w	7
1331  068c 073f              	dc.w	1855
1332  068e 0f                	dc.b	15
1334  068f 0000              	dc.w	f_ApplDescRead_220106_HeatVentWheel_Current_Sense
1335  0691 0000              	dc.b	page(f_ApplDescRead_220106_HeatVentWheel_Current_Sense),0
1336  0693 0107              	dc.w	263
1337  0695 0002              	dc.w	2
1338  0697 073f              	dc.w	1855
1339  0699 0f                	dc.b	15
1341  069a 0000              	dc.w	f_ApplDescRead_220107_Vented_Seats_Voltage_Sense
1342  069c 0000              	dc.b	page(f_ApplDescRead_220107_Vented_Seats_Voltage_Sense),0
1343  069e 1008              	dc.w	4104
1344  06a0 0004              	dc.w	4
1345  06a2 073d              	dc.w	1853
1346  06a4 0f                	dc.b	15
1348  06a5 0000              	dc.w	f_ApplDescReadECU_time_stamps_RAM
1349  06a7 0000              	dc.b	page(f_ApplDescReadECU_time_stamps_RAM),0
1350  06a9 1009              	dc.w	4105
1351  06ab 0002              	dc.w	2
1352  06ad 073d              	dc.w	1853
1353  06af 0f                	dc.b	15
1355  06b0 0000              	dc.w	f_ApplDescReadECU_time_stamps_from_KeyOn_RAM
1356  06b2 0000              	dc.b	page(f_ApplDescReadECU_time_stamps_from_KeyOn_RAM),0
1357  06b4 102a              	dc.w	4138
1358  06b6 0009              	dc.w	9
1359  06b8 073f              	dc.w	1855
1360  06ba 0f                	dc.b	15
1362  06bb 0000              	dc.w	f_ApplDescRead_22102A_Check_EOL_configuration_data
1363  06bd 0000              	dc.b	page(f_ApplDescRead_22102A_Check_EOL_configuration_data),0
1364  06bf 2001              	dc.w	8193
1365  06c1 0003              	dc.w	3
1366  06c3 073d              	dc.w	1853
1367  06c5 0f                	dc.b	15
1369  06c6 0000              	dc.w	f_ApplDescReadOdometer
1370  06c8 0000              	dc.b	page(f_ApplDescReadOdometer),0
1371  06ca 2002              	dc.w	8194
1372  06cc 0003              	dc.w	3
1373  06ce 073d              	dc.w	1853
1374  06d0 0f                	dc.b	15
1376  06d1 0000              	dc.w	f_ApplDescReadOdometer_content_to_the_last_F_ROM_updating
1377  06d3 0000              	dc.b	page(f_ApplDescReadOdometer_content_to_the_last_F_ROM_updating),0
1378  06d5 2003              	dc.w	8195
1379  06d7 0001              	dc.w	1
1380  06d9 073d              	dc.w	1853
1381  06db 0f                	dc.b	15
1383  06dc 0000              	dc.w	f_ApplDescReadNumber_of_FlashRom_re_writings_F_ROM
1384  06de 0000              	dc.b	page(f_ApplDescReadNumber_of_FlashRom_re_writings_F_ROM),0
1385  06e0 2008              	dc.w	8200
1386  06e2 0004              	dc.w	4
1387  06e4 073d              	dc.w	1853
1388  06e6 0f                	dc.b	15
1390  06e7 0000              	dc.w	f_ApplDescReadECU_time_stamps_EEPROM
1391  06e9 0000              	dc.b	page(f_ApplDescReadECU_time_stamps_EEPROM),0
1392  06eb 2009              	dc.w	8201
1393  06ed 0002              	dc.w	2
1394  06ef 073d              	dc.w	1853
1395  06f1 0f                	dc.b	15
1397  06f2 0000              	dc.w	f_ApplDescReadECU_time_stamps_from_KeyOn_EEPROM
1398  06f4 0000              	dc.b	page(f_ApplDescReadECU_time_stamps_from_KeyOn_EEPROM),0
1399  06f6 200a              	dc.w	8202
1400  06f8 0002              	dc.w	2
1401  06fa 073d              	dc.w	1853
1402  06fc 0f                	dc.b	15
1404  06fd 0000              	dc.w	f_ApplDescReadKeyOn_counter_EEPROM
1405  06ff 0000              	dc.b	page(f_ApplDescReadKeyOn_counter_EEPROM),0
1406  0701 200b              	dc.w	8203
1407  0703 0004              	dc.w	4
1408  0705 073d              	dc.w	1853
1409  0707 0f                	dc.b	15
1411  0708 0000              	dc.w	f_ApplDescReadECU_Time_first_DTC_detection_EEPROM
1412  070a 0000              	dc.b	page(f_ApplDescReadECU_Time_first_DTC_detection_EEPROM),0
1413  070c 200c              	dc.w	8204
1414  070e 0002              	dc.w	2
1415  0710 073d              	dc.w	1853
1416  0712 0f                	dc.b	15
1418  0713 0000              	dc.w	f_ApplDescReadECU_Time_Stamps_from_KeyOn_first_DTC_detection_EEPROM
1419  0715 0000              	dc.b	page(f_ApplDescReadECU_Time_Stamps_from_KeyOn_first_DTC_detection_EEPROM),0
1420  0717 200f              	dc.w	8207
1421  0719 0001              	dc.w	1
1422  071b 073d              	dc.w	1853
1423  071d 0f                	dc.b	15
1425  071e 0000              	dc.w	f_ApplDescReadKeyOn_Counter_Status_EEPROM
1426  0720 0000              	dc.b	page(f_ApplDescReadKeyOn_Counter_Status_EEPROM),0
1427  0722 2010              	dc.w	8208
1428  0724 0004              	dc.w	4
1429  0726 073f              	dc.w	1855
1430  0728 0f                	dc.b	15
1432  0729 0000              	dc.w	f_ApplDescReadProgrammingStatus_EEPROM_FLASH
1433  072b 0000              	dc.b	page(f_ApplDescReadProgrammingStatus_EEPROM_FLASH),0
1434  072d 2023              	dc.w	8227
1435  072f 00ff              	dc.w	255
1436  0731 073f              	dc.w	1855
1437  0733 0f                	dc.b	15
1439  0734 0000              	dc.w	f_ApplDescRead_2E2023_System_Configuration_PROXI
1440  0736 0000              	dc.b	page(f_ApplDescRead_2E2023_System_Configuration_PROXI),0
1441  0738 2024              	dc.w	8228
1442  073a 000b              	dc.w	11
1443  073c 073f              	dc.w	1855
1444  073e 0f                	dc.b	15
1446  073f 0000              	dc.w	f_ApplDescRead_222024_Received_PROXI_Information
1447  0741 0000              	dc.b	page(f_ApplDescRead_222024_Received_PROXI_Information),0
1448  0743 2800              	dc.w	10240
1449  0745 0004              	dc.w	4
1450  0747 073f              	dc.w	1855
1451  0749 0f                	dc.b	15
1453  074a 0000              	dc.w	f_ApplDescRead_222800_Heated_Seat_PWM_Calibration_Parameters_Front_Seats
1454  074c 0000              	dc.b	page(f_ApplDescRead_222800_Heated_Seat_PWM_Calibration_Parameters_Front_Seats),0
1455  074e 2801              	dc.w	10241
1456  0750 0002              	dc.w	2
1457  0752 073f              	dc.w	1855
1458  0754 0f                	dc.b	15
1460  0755 0000              	dc.w	f_ApplDescRead_222801_Vented_Seat_PWM_Calibration_Parameters
1461  0757 0000              	dc.b	page(f_ApplDescRead_222801_Vented_Seat_PWM_Calibration_Parameters),0
1462  0759 2802              	dc.w	10242
1463  075b 0004              	dc.w	4
1464  075d 073f              	dc.w	1855
1465  075f 0f                	dc.b	15
1467  0760 0000              	dc.w	f_ApplDescRead_222802_Heated_Steering_Wheel_PWM_Calibration_Parameters
1468  0762 0000              	dc.b	page(f_ApplDescRead_222802_Heated_Steering_Wheel_PWM_Calibration_Parameters),0
1469  0764 2803              	dc.w	10243
1470  0766 0004              	dc.w	4
1471  0768 073f              	dc.w	1855
1472  076a 0f                	dc.b	15
1474  076b 0000              	dc.w	f_ApplDescRead_222803_Heated_Seat_Max_Timeout_Parameters_Front_Seats
1475  076d 0000              	dc.b	page(f_ApplDescRead_222803_Heated_Seat_Max_Timeout_Parameters_Front_Seats),0
1476  076f 2804              	dc.w	10244
1477  0771 0004              	dc.w	4
1478  0773 073f              	dc.w	1855
1479  0775 0f                	dc.b	15
1481  0776 0000              	dc.w	f_ApplDescRead_222804_Heated_Steering_Wheel_Max_Timeout_Parameters
1482  0778 0000              	dc.b	page(f_ApplDescRead_222804_Heated_Steering_Wheel_Max_Timeout_Parameters),0
1483  077a 2805              	dc.w	10245
1484  077c 0001              	dc.w	1
1485  077e 073f              	dc.w	1855
1486  0780 0f                	dc.b	15
1488  0781 0000              	dc.w	f_ApplDescRead_222805_Voltage_Level_ON
1489  0783 0000              	dc.b	page(f_ApplDescRead_222805_Voltage_Level_ON),0
1490  0785 2806              	dc.w	10246
1491  0787 0001              	dc.w	1
1492  0789 073f              	dc.w	1855
1493  078b 0f                	dc.b	15
1495  078c 0000              	dc.w	f_ApplDescRead_222806_Voltage_Level_OFF
1496  078e 0000              	dc.b	page(f_ApplDescRead_222806_Voltage_Level_OFF),0
1497  0790 2807              	dc.w	10247
1498  0792 0006              	dc.w	6
1499  0794 073f              	dc.w	1855
1500  0796 0f                	dc.b	15
1502  0797 0000              	dc.w	f_ApplDescRead_222807_PCB_Over_Temperature_Condition_Status
1503  0799 0000              	dc.b	page(f_ApplDescRead_222807_PCB_Over_Temperature_Condition_Status),0
1504  079b 2808              	dc.w	10248
1505  079d 0005              	dc.w	5
1506  079f 073f              	dc.w	1855
1507  07a1 0f                	dc.b	15
1509  07a2 0000              	dc.w	f_ApplDescRead_222808_Heated_Seat_NTC_Cutoff_Parameters_Front_Seats
1510  07a4 0000              	dc.b	page(f_ApplDescRead_222808_Heated_Seat_NTC_Cutoff_Parameters_Front_Seats),0
1511  07a6 2809              	dc.w	10249
1512  07a8 0005              	dc.w	5
1513  07aa 073f              	dc.w	1855
1514  07ac 0f                	dc.b	15
1516  07ad 0000              	dc.w	f_ApplDescRead_222809_Steering_Wheel_Temperature_Cut_off_Parameters
1517  07af 0000              	dc.b	page(f_ApplDescRead_222809_Steering_Wheel_Temperature_Cut_off_Parameters),0
1518  07b1 280a              	dc.w	10250
1519  07b3 0004              	dc.w	4
1520  07b5 073f              	dc.w	1855
1521  07b7 0f                	dc.b	15
1523  07b8 0000              	dc.w	f_ApplDescRead_22280A_HeatVent_Auto_Activation_Temperature_Calibration_Paramet
1524  07ba 0000              	dc.b	page(f_ApplDescRead_22280A_HeatVent_Auto_Activation_Temperature_Calibration_Paramet),0
1525  07bc 280c              	dc.w	10252
1526  07be 0004              	dc.w	4
1527  07c0 073f              	dc.w	1855
1528  07c2 0f                	dc.b	15
1530  07c3 0000              	dc.w	f_ApplDescRead_22280C_Heated_Steering_Wheel_Min_Timeout_Parameters
1531  07c5 0000              	dc.b	page(f_ApplDescRead_22280C_Heated_Steering_Wheel_Min_Timeout_Parameters),0
1532  07c7 280d              	dc.w	10253
1533  07c9 0006              	dc.w	6
1534  07cb 073f              	dc.w	1855
1535  07cd 0f                	dc.b	15
1537  07ce 0000              	dc.w	f_ApplDescRead_22280D_Load_Shed_Fault_Status
1538  07d0 0000              	dc.b	page(f_ApplDescRead_22280D_Load_Shed_Fault_Status),0
1539  07d2 280e              	dc.w	10254
1540  07d4 0004              	dc.w	4
1541  07d6 073f              	dc.w	1855
1542  07d8 0f                	dc.b	15
1544  07d9 0000              	dc.w	f_ApplDescRead_22280E_Heated_Seat_PWM_Calibration_Parameters_Rear_Seats
1545  07db 0000              	dc.b	page(f_ApplDescRead_22280E_Heated_Seat_PWM_Calibration_Parameters_Rear_Seats),0
1546  07dd 280f              	dc.w	10255
1547  07df 0004              	dc.w	4
1548  07e1 073f              	dc.w	1855
1549  07e3 0f                	dc.b	15
1551  07e4 0000              	dc.w	f_ApplDescRead_22280F_Heated_Seat_Max_Timeout_Parameters_Rear_Seats
1552  07e6 0000              	dc.b	page(f_ApplDescRead_22280F_Heated_Seat_Max_Timeout_Parameters_Rear_Seats),0
1553  07e8 2810              	dc.w	10256
1554  07ea 0005              	dc.w	5
1555  07ec 073f              	dc.w	1855
1556  07ee 0f                	dc.b	15
1558  07ef 0000              	dc.w	f_ApplDescRead_222810_Heated_Seat_NTC_Cutoff_Parameters_Rear_Seats
1559  07f1 0000              	dc.b	page(f_ApplDescRead_222810_Heated_Seat_NTC_Cutoff_Parameters_Rear_Seats),0
1560  07f3 3000              	dc.w	12288
1561  07f5 000f              	dc.w	15
1562  07f7 073f              	dc.w	1855
1563  07f9 0f                	dc.b	15
1565  07fa 0000              	dc.w	f_ApplDescRead_223000_Program_ECU_Data_at_Flash_Station_1
1566  07fc 0000              	dc.b	page(f_ApplDescRead_223000_Program_ECU_Data_at_Flash_Station_1),0
1567  07fe 3001              	dc.w	12289
1568  0800 0001              	dc.w	1
1569  0802 073f              	dc.w	1855
1570  0804 0f                	dc.b	15
1572  0805 0000              	dc.w	f_ApplDescRead_223001_Program_ECU_Data_at_Flash_Station_2
1573  0807 0000              	dc.b	page(f_ApplDescRead_223001_Program_ECU_Data_at_Flash_Station_2),0
1574  0809 3002              	dc.w	12290
1575  080b 0032              	dc.w	50
1576  080d 073f              	dc.w	1855
1577  080f 0f                	dc.b	15
1579  0810 0000              	dc.w	f_ApplDescReadReadWrite
1580  0812 0000              	dc.b	page(f_ApplDescReadReadWrite),0
1581  0814 f100              	dc.w	-3840
1582  0816 0005              	dc.w	5
1583  0818 073f              	dc.w	1855
1584  081a 0f                	dc.b	15
1586  081b 0000              	dc.w	f_ApplDescRead_22F100_Active_Diagnostic_Information
1587  081d 0000              	dc.b	page(f_ApplDescRead_22F100_Active_Diagnostic_Information),0
1588  081f f10b              	dc.w	-3829
1589  0821 0004              	dc.w	4
1590  0823 073f              	dc.w	1855
1591  0825 0f                	dc.b	15
1593  0826 0000              	dc.w	f_ApplDescRead_22F10B_ECU_Configuration
1594  0828 0000              	dc.b	page(f_ApplDescRead_22F10B_ECU_Configuration),0
1595  082a f112              	dc.w	-3822
1596  082c 000a              	dc.w	10
1597  082e 073f              	dc.w	1855
1598  0830 0f                	dc.b	15
1600  0831 0000              	dc.w	f_ApplDescRead_22F112_Hardware_Part_Number
1601  0833 0000              	dc.b	page(f_ApplDescRead_22F112_Hardware_Part_Number),0
1602  0835 f122              	dc.w	-3806
1603  0837 000a              	dc.w	10
1604  0839 073f              	dc.w	1855
1605  083b 0f                	dc.b	15
1607  083c 0000              	dc.w	f_ApplDescRead_22F122_Software_Part_Number
1608  083e 0000              	dc.b	page(f_ApplDescRead_22F122_Software_Part_Number),0
1609  0840 f132              	dc.w	-3790
1610  0842 000a              	dc.w	10
1611  0844 073f              	dc.w	1855
1612  0846 0f                	dc.b	15
1614  0847 0000              	dc.w	f_ApplDescRead_22F132_ECU_Part_Number
1615  0849 0000              	dc.b	page(f_ApplDescRead_22F132_ECU_Part_Number),0
1616  084b f180              	dc.w	-3712
1617  084d 000e              	dc.w	14
1618  084f 073f              	dc.w	1855
1619  0851 0f                	dc.b	15
1621  0852 0000              	dc.w	f_ApplDescReadBootSoftwareIdentification
1622  0854 0000              	dc.b	page(f_ApplDescReadBootSoftwareIdentification),0
1623  0856 f181              	dc.w	-3711
1624  0858 000e              	dc.w	14
1625  085a 073f              	dc.w	1855
1626  085c 0f                	dc.b	15
1628  085d 0000              	dc.w	f_ApplDescReadApplicationSoftwareIdentification
1629  085f 0000              	dc.b	page(f_ApplDescReadApplicationSoftwareIdentification),0
1630  0861 f182              	dc.w	-3710
1631  0863 000e              	dc.w	14
1632  0865 073f              	dc.w	1855
1633  0867 0f                	dc.b	15
1635  0868 0000              	dc.w	f_ApplDescReadApplicationDataIdentification
1636  086a 0000              	dc.b	page(f_ApplDescReadApplicationDataIdentification),0
1637  086c f183              	dc.w	-3709
1638  086e 000d              	dc.w	13
1639  0870 073f              	dc.w	1855
1640  0872 0f                	dc.b	15
1642  0873 0000              	dc.w	f_ApplDescReadBootSoftwareFingerprint
1643  0875 0000              	dc.b	page(f_ApplDescReadBootSoftwareFingerprint),0
1644  0877 f184              	dc.w	-3708
1645  0879 000d              	dc.w	13
1646  087b 073f              	dc.w	1855
1647  087d 0f                	dc.b	15
1649  087e 0000              	dc.w	f_ApplDescReadApplicationSoftwareFingerprint
1650  0880 0000              	dc.b	page(f_ApplDescReadApplicationSoftwareFingerprint),0
1651  0882 f185              	dc.w	-3707
1652  0884 000d              	dc.w	13
1653  0886 073f              	dc.w	1855
1654  0888 0f                	dc.b	15
1656  0889 0000              	dc.w	f_ApplDescReadApplicationDataFingerprint
1657  088b 0000              	dc.b	page(f_ApplDescReadApplicationDataFingerprint),0
1658  088d f186              	dc.w	-3706
1659  088f 0001              	dc.w	1
1660  0891 073f              	dc.w	1855
1661  0893 0f                	dc.b	15
1663  0894 0000              	dc.w	f_ApplDescReadActiveDiagnosticSessionDataIdentifier
1664  0896 0000              	dc.b	page(f_ApplDescReadActiveDiagnosticSessionDataIdentifier),0
1665  0898 f187              	dc.w	-3705
1666  089a 000b              	dc.w	11
1667  089c 073f              	dc.w	1855
1668  089e 0f                	dc.b	15
1670  089f 0000              	dc.w	f_ApplDescReadVehicleManufacturerSparePartNumber
1671  08a1 0000              	dc.b	page(f_ApplDescReadVehicleManufacturerSparePartNumber),0
1672  08a3 f18c              	dc.w	-3700
1673  08a5 000e              	dc.w	14
1674  08a7 073f              	dc.w	1855
1675  08a9 0f                	dc.b	15
1677  08aa 0000              	dc.w	f_ApplDescReadEcuSerialNumber
1678  08ac 0000              	dc.b	page(f_ApplDescReadEcuSerialNumber),0
1679  08ae f190              	dc.w	-3696
1680  08b0 0011              	dc.w	17
1681  08b2 073f              	dc.w	1855
1682  08b4 0f                	dc.b	15
1684  08b5 0000              	dc.w	f_ApplDescReadVIN
1685  08b7 0000              	dc.b	page(f_ApplDescReadVIN),0
1686  08b9 f192              	dc.w	-3694
1687  08bb 000b              	dc.w	11
1688  08bd 073f              	dc.w	1855
1689  08bf 0f                	dc.b	15
1691  08c0 0000              	dc.w	f_ApplDescReadSystemSupplierEcuHardwareNumber
1692  08c2 0000              	dc.b	page(f_ApplDescReadSystemSupplierEcuHardwareNumber),0
1693  08c4 f193              	dc.w	-3693
1694  08c6 0001              	dc.w	1
1695  08c8 073f              	dc.w	1855
1696  08ca 0f                	dc.b	15
1698  08cb 0000              	dc.w	f_ApplDescReadSystemSupplierEcuHardwareVersionNumber
1699  08cd 0000              	dc.b	page(f_ApplDescReadSystemSupplierEcuHardwareVersionNumber),0
1700  08cf f194              	dc.w	-3692
1701  08d1 000b              	dc.w	11
1702  08d3 073f              	dc.w	1855
1703  08d5 0f                	dc.b	15
1705  08d6 0000              	dc.w	f_ApplDescReadSystemSupplierEcuSoftwareNumber
1706  08d8 0000              	dc.b	page(f_ApplDescReadSystemSupplierEcuSoftwareNumber),0
1707  08da f195              	dc.w	-3691
1708  08dc 0002              	dc.w	2
1709  08de 073f              	dc.w	1855
1710  08e0 0f                	dc.b	15
1712  08e1 0000              	dc.w	f_ApplDescReadSystemSupplierEcuSoftwareVersionNumber
1713  08e3 0000              	dc.b	page(f_ApplDescReadSystemSupplierEcuSoftwareVersionNumber),0
1714  08e5 f196              	dc.w	-3690
1715  08e7 0006              	dc.w	6
1716  08e9 073f              	dc.w	1855
1717  08eb 0f                	dc.b	15
1719  08ec 0000              	dc.w	f_ApplDescReadExhaustRegulationOrTypeApprovalNumber
1720  08ee 0000              	dc.b	page(f_ApplDescReadExhaustRegulationOrTypeApprovalNumber),0
1721  08f0 f1a0              	dc.w	-3680
1722  08f2 0040              	dc.w	64
1723  08f4 073f              	dc.w	1855
1724  08f6 0f                	dc.b	15
1726  08f7 0000              	dc.w	f_ApplDescReadIdentificationCode
1727  08f9 0000              	dc.b	page(f_ApplDescReadIdentificationCode),0
1728  08fb f1a4              	dc.w	-3676
1729  08fd 0006              	dc.w	6
1730  08ff 073f              	dc.w	1855
1731  0901 0f                	dc.b	15
1733  0902 0000              	dc.w	f_ApplDescReadSincomAndFactory
1734  0904 0000              	dc.b	page(f_ApplDescReadSincomAndFactory),0
1735  0906 f1a5              	dc.w	-3675
1736  0908 0007              	dc.w	7
1737  090a 073f              	dc.w	1855
1738  090c 0f                	dc.b	15
1740  090d 0000              	dc.w	f_ApplDescReadISOCode
1741  090f 0000              	dc.b	page(f_ApplDescReadISOCode),0
1794                         ; 1889 static void CheckTableConsistency(void)
1794                         ; 1890 {
1795                         .ftext:	section	.text
1796  0000                   L3f_CheckTableConsistency:
1798  0000 1b9d              	leas	-3,s
1799       00000003          OFST:	set	3
1802                         ; 1906   sidCnt = 0;
1804  0002 6982              	clr	OFST-1,s
1805                         ; 1907   for (i = 0; i < kDescSvcIdMapSize; i++)
1807  0004 18c7              	clry	
1808  0006 6d80              	sty	OFST-3,s
1809  0008                   L513:
1810                         ; 1909     if (g_descSidMap[i] != 0xff)
1812  0008 e6ea0047          	ldab	L152_g_descSidMap,y
1813  000c c1ff              	cmpb	#255
1814  000e 2715              	beq	L323
1815                         ; 1912        sidCnt++;
1817  0010 6282              	inc	OFST-1,s
1818                         ; 1914        DescAssertInternal((g_descSidMap[i] < (kDescSvcHeadNumItems - 1)),
1820  0012 c10f              	cmpb	#15
1821  0014 250f              	blo	L323
1824  0016 cc077c            	ldd	#1916
1825  0019 3b                	pshd	
1826  001a cc0002            	ldd	#2
1827  001d 4a000000          	call	f_ApplDescFatalError
1829  0021 1b82              	leas	2,s
1830  0023 ed80              	ldy	OFST-3,s
1831  0025                   L323:
1832                         ; 1907   for (i = 0; i < kDescSvcIdMapSize; i++)
1834  0025 02                	iny	
1835  0026 6d80              	sty	OFST-3,s
1838  0028 8d0046            	cpy	#70
1839  002b 25db              	blo	L513
1840                         ; 1919   DescAssertInternal((sidCnt == (kDescSvcHeadNumItems - 1)),
1842  002d e682              	ldab	OFST-1,s
1843  002f c10f              	cmpb	#15
1844  0031 270d              	beq	L723
1847  0033 cc0781            	ldd	#1921
1848  0036 3b                	pshd	
1849  0037 cc0003            	ldd	#3
1850  003a 4a000000          	call	f_ApplDescFatalError
1852  003e 1b82              	leas	2,s
1853  0040                   L723:
1854                         ; 1925   for (i = 0; i < (kDescSvcHeadNumItems - 1); i++)
1857  0040 18c7              	clry	
1858  0042 6d80              	sty	OFST-3,s
1859  0044                   L133:
1860                         ; 1927     DescAssertInternal((g_descSvcHead[i].svcInstFirstItem < kDescSvcInstNumItems),
1862  0044 1858              	lsly	
1863  0046 1858              	lsly	
1864  0048 1858              	lsly	
1865  004a e6ea0093          	ldab	L352_g_descSvcHead+6,y
1866  004e c164              	cmpb	#100
1867  0050 250d              	blo	L733
1870  0052 cc0789            	ldd	#1929
1871  0055 3b                	pshd	
1872  0056 cc0004            	ldd	#4
1873  0059 4a000000          	call	f_ApplDescFatalError
1875  005d 1b82              	leas	2,s
1876  005f                   L733:
1877                         ; 1925   for (i = 0; i < (kDescSvcHeadNumItems - 1); i++)
1880  005f 186280            	incw	OFST-3,s
1883  0062 ed80              	ldy	OFST-3,s
1884  0064 8d000f            	cpy	#15
1885  0067 25db              	blo	L133
1886                         ; 1938   DescAssertInternal((g_descSvcHead[i].svcInstFirstItem == kDescSvcInstNumItems),
1888  0069 1858              	lsly	
1889  006b 1858              	lsly	
1890  006d 1858              	lsly	
1891  006f e6ea0093          	ldab	L352_g_descSvcHead+6,y
1892  0073 c164              	cmpb	#100
1893  0075 270d              	beq	L143
1896  0077 cc0794            	ldd	#1940
1897  007a 3b                	pshd	
1898  007b cc0004            	ldd	#4
1899  007e 4a000000          	call	f_ApplDescFatalError
1901  0082 1b82              	leas	2,s
1902  0084                   L143:
1903                         ; 1945   for (i = 0; i < kDescSvcInstNumItems; i++)
1906  0084 87                	clra	
1907  0085 c7                	clrb	
1908  0086 6c80              	std	OFST-3,s
1909  0088                   L343:
1910                         ; 1947     DescAssertInternal((g_descSvcInst[i].mainHandler != V_NULL), kDescAssertMissingMainHandler);
1912  0088 cd000b            	ldy	#11
1913  008b 13                	emul	
1914  008c b746              	tfr	d,y
1915  008e ecea012c          	ldd	L752_g_descSvcInst+7,y
1916  0092 2613              	bne	L153
1917  0094 ecea012e          	ldd	L752_g_descSvcInst+9,y
1918  0098 260d              	bne	L153
1921  009a cc079b            	ldd	#1947
1922  009d 3b                	pshd	
1923  009e cc0006            	ldd	#6
1924  00a1 4a000000          	call	f_ApplDescFatalError
1926  00a5 1b82              	leas	2,s
1927  00a7                   L153:
1928                         ; 1945   for (i = 0; i < kDescSvcInstNumItems; i++)
1931  00a7 186280            	incw	OFST-3,s
1934  00aa ec80              	ldd	OFST-3,s
1935  00ac 8c0064            	cpd	#100
1936  00af 25d7              	blo	L343
1937                         ; 1949 }
1940  00b1 1b83              	leas	3,s
1941  00b3 0a                	rtc	
1963                         ; 1963 static void DescDebugIterInit(DESC_CONTEXT_FORMAL_PARAM_DEF_ONLY)
1963                         ; 1964 {
1964                         	switch	.ftext
1965  00b4                   L5f_DescDebugIterInit:
1969                         ; 1967 }
1973  00b4 0a                	rtc	
1997                         ; 2012 DESCNET_USDT_STATIC void DescUsdtNetIsoTpInitPowerOn(void)
1997                         ; 2013 {
1998                         	switch	.ftext
1999  00b5                   L7f_DescUsdtNetIsoTpInitPowerOn:
2003                         ; 2014   DescUsdtNetIsoTpInit();
2005  00b5 4a00cccc          	call	L11f_DescUsdtNetIsoTpInit
2007                         ; 2015 }
2010  00b9 0a                	rtc	
2035                         ; 2027 static void DescUsdtNetIsoTpInitContext(DESC_TPCONTEXT_FORMAL_PARAM_DEF_ONLY)
2035                         ; 2028 {
2036                         	switch	.ftext
2037  00ba                   L32f_DescUsdtNetIsoTpInitContext:
2041                         ; 2059   g_descUsdtNetInfoPoolIsoTp[DESC_TPCONTEXT_PARAM_VALUE].descHandle = kDescUsdtNetInvalidDescContext;
2043  00ba c6ff              	ldab	#255
2044  00bc 7b0150            	stab	L371_g_descUsdtNetInfoPoolIsoTp+9
2045                         ; 2060   g_descUsdtNetInfoPoolIsoTp[DESC_TPCONTEXT_PARAM_VALUE].reqDataPtr = g_descBuffer[DESC_TPCONTEXT_PARAM_VALUE];
2047  00bf cc0043            	ldd	#L771_g_descBuffer
2048  00c2 7c0153            	std	L371_g_descUsdtNetInfoPoolIsoTp+12
2049                         ; 2061   g_descUsdtNetInfoPoolIsoTp[DESC_TPCONTEXT_PARAM_VALUE].resDataPtr = g_descBuffer[DESC_TPCONTEXT_PARAM_VALUE];
2051  00c5 7c0155            	std	L371_g_descUsdtNetInfoPoolIsoTp+14
2052                         ; 2062   g_descUsdtNetInfoPoolIsoTp[DESC_TPCONTEXT_PARAM_VALUE].resType = kDescUsdtResponseNone;
2054  00c8 79014e            	clr	L371_g_descUsdtNetInfoPoolIsoTp+7
2055                         ; 2063 }
2058  00cb 0a                	rtc	
2092                         ; 2075 DESCNET_USDT_STATIC void DescUsdtNetIsoTpInit(void)
2092                         ; 2076 {
2093                         	switch	.ftext
2094  00cc                   L11f_DescUsdtNetIsoTpInit:
2096  00cc 3b                	pshd	
2097       00000002          OFST:	set	2
2100                         ; 2078     vuint8_least resIter = kTpTxChannelCount;
2102  00cd cc0001            	ldd	#1
2103  00d0 6c80              	std	OFST-2,s
2104  00d2                   L714:
2105                         ; 2081       resIter--;
2107  00d2 186380            	decw	OFST-2,s
2108                         ; 2082       g_busInfoPoolTxRef[resIter] = V_NULL;
2110  00d5 ed80              	ldy	OFST-2,s
2111  00d7 1858              	lsly	
2112  00d9 1869ea0145        	clrw	L571_g_busInfoPoolTxRef,y
2113                         ; 2079     while(resIter != 0)
2115  00de ec80              	ldd	OFST-2,s
2116  00e0 26f0              	bne	L714
2117                         ; 2099   DescUsdtNetIsoTpInitContext(DESC_TPCONTEXT_PARAM_WRAPPER_ONLY(kDescPrimContext));
2119  00e2 4a00baba          	call	L32f_DescUsdtNetIsoTpInitContext
2121                         ; 2114 }
2124  00e6 31                	puly	
2125  00e7 0a                	rtc	
2148                         ; 2125 DESCNET_USDT_STATIC void DescUsdtNetIsoTpStateTask(void)
2148                         ; 2126 {
2149                         	switch	.ftext
2150  00e8                   L31f_DescUsdtNetIsoTpStateTask:
2154                         ; 2131 }
2157  00e8 0a                	rtc	
2257                         ; 2196 TP_MEMORY_MODEL_DATA vuint8* DescGetBuffer(TP_CHANNEL_RX_FORMAL_PARAM_DEF_FIRST vuint16 dataLength)
2257                         ; 2197 {
2258                         	switch	.ftext
2259  00e9                   f_DescGetBuffer:
2261  00e9 3b                	pshd	
2262  00ea 1b9b              	leas	-5,s
2263       00000005          OFST:	set	5
2266                         ; 2198   DescUsdtNetMsg returnValue = V_NULL;
2268  00ec 186981            	clrw	OFST-4,s
2269                         ; 2209     addressCheckResult = DescOemCheckPhysSourceAddress(TpRxGetSourceAddress(TP_CHANNEL_RX_PARAM_ONLY));
2271  00ef 4a000000          	call	f_TpRxGetSourceAddress
2273  00f3 c1f1              	cmpb	#241
2274  00f5 2605              	bne	L42
2275  00f7 cc0001            	ldd	#1
2276  00fa 2002              	bra	L62
2277  00fc                   L42:
2278  00fc 87                	clra	
2279  00fd c7                	clrb	
2280  00fe                   L62:
2281  00fe 6c83              	std	OFST-2,s
2282                         ; 2213   if(addressCheckResult == kDescTrue)
2284  0100 042447            	dbne	d,L774
2285                         ; 2221     if (g_descUsdtNetInfoPoolIsoTp[DESC_TPCONTEXT_PARAM_VALUE].descHandle == kDescUsdtNetInvalidDescContext)
2287  0103 f60150            	ldab	L371_g_descUsdtNetInfoPoolIsoTp+9
2288  0106 04a141            	ibne	b,L774
2289                         ; 2223       if(dataLength > kDescPrimBufferLen)
2291  0109 ec85              	ldd	OFST+0,s
2292  010b 8c0102            	cpd	#258
2293  010e 2309              	bls	L305
2294                         ; 2233         TpRxSetFCStatus(TP_CHANNEL_RX_PARAM_FIRST kTpFCStatusOverflow);
2296  0110 cc0003            	ldd	#3
2297  0113 4a000000          	call	f_TpRxSetFCStatus
2300  0117 2031              	bra	L774
2301  0119                   L305:
2302                         ; 2250         g_descUsdtNetInfoPoolIsoTp[DESC_TPCONTEXT_PARAM_VALUE].busInfo.busType   = kDescUsdtNetBusTypeIsoTp;
2304  0119 c601              	ldab	#1
2305  011b 7b0149            	stab	L371_g_descUsdtNetInfoPoolIsoTp+2
2306                         ; 2254         g_descUsdtNetInfoPoolIsoTp[DESC_TPCONTEXT_PARAM_VALUE].busInfo.addressingInfo.isoTp.SourceAddress = TpRxGetSourceAddress(TP_CHANNEL_RX_PARAM_ONLY);
2308  011e 4a000000          	call	f_TpRxGetSourceAddress
2310  0122 7b014b            	stab	L371_g_descUsdtNetInfoPoolIsoTp+4
2311                         ; 2256         g_descUsdtNetInfoPoolIsoTp[DESC_TPCONTEXT_PARAM_VALUE].busInfo.testerId = g_descUsdtNetInfoPoolIsoTp[DESC_TPCONTEXT_PARAM_VALUE].busInfo.addressingInfo.isoTp.SourceAddress;
2313  0125 7b0148            	stab	L371_g_descUsdtNetInfoPoolIsoTp+1
2314                         ; 2280           g_descUsdtNetInfoPoolIsoTp[DESC_TPCONTEXT_PARAM_VALUE].reqType         = kDescUsdtNetReqTypePhysical;
2316  0128 79014d            	clr	L371_g_descUsdtNetInfoPoolIsoTp+6
2317                         ; 2290         g_descUsdtNetInfoPoolIsoTp[DESC_TPCONTEXT_PARAM_VALUE].dataLength        = dataLength;
2319  012b 1805850151        	movw	OFST+0,s,L371_g_descUsdtNetInfoPoolIsoTp+10
2320                         ; 2291         g_descUsdtNetInfoPoolIsoTp[DESC_TPCONTEXT_PARAM_VALUE].descHandle        = DESC_TPCONTEXT_PARAM_VALUE; /* Write used context */
2322  0130 790150            	clr	L371_g_descUsdtNetInfoPoolIsoTp+9
2323                         ; 2294         result = DescUsdtNetStartReception(&g_descUsdtNetInfoPoolIsoTp[DESC_TPCONTEXT_PARAM_VALUE]);
2325  0133 cc0147            	ldd	#L371_g_descUsdtNetInfoPoolIsoTp
2326  0136 4a036a6a          	call	L33f_DescUsdtNetAbsStartReception
2328  013a 6b80              	stab	OFST-5,s
2329                         ; 2295         if(result == kDescUsdtNetworkOk)
2331  013c 2607              	bne	L705
2332                         ; 2305           returnValue = g_descUsdtNetInfoPoolIsoTp[DESC_TPCONTEXT_PARAM_VALUE].reqDataPtr;
2334  013e 1801810153        	movw	L371_g_descUsdtNetInfoPoolIsoTp+12,OFST-4,s
2336  0143 2005              	bra	L774
2337  0145                   L705:
2338                         ; 2309           g_descUsdtNetInfoPoolIsoTp[DESC_TPCONTEXT_PARAM_VALUE].descHandle = kDescUsdtNetInvalidDescContext; /* Write used context */
2340  0145 c6ff              	ldab	#255
2341  0147 7b0150            	stab	L371_g_descUsdtNetInfoPoolIsoTp+9
2342  014a                   L774:
2343                         ; 2336   return returnValue;
2345  014a ec81              	ldd	OFST-4,s
2348  014c 1b87              	leas	7,s
2349  014e 0a                	rtc	
2383                         ; 2349 void TP_API_CALLBACK_TYPE DescPhysReqInd(TP_CHANNEL_RX_FORMAL_PARAM_DEF_FIRST vuint16 dataLen)
2383                         ; 2350 {
2384                         	switch	.ftext
2385  014f                   f_DescPhysReqInd:
2389                         ; 2351   DESC_IGNORE_UNREF_PARAM(dataLen);
2391                         ; 2395       g_descUsdtNetInfoPoolIsoTp[DESC_TPCONTEXT_PARAM_VALUE].busInfo.addressingInfo.isoTp.TargetAddress = kDescEcuNumber;
2393  014f c6c2              	ldab	#194
2394  0151 7b014c            	stab	L371_g_descUsdtNetInfoPoolIsoTp+5
2395                         ; 2404       DescUsdtNetFinishReception(&g_descUsdtNetInfoPoolIsoTp[DESC_TPCONTEXT_PARAM_VALUE], kDescUsdtNetworkOk);
2397  0154 87                	clra	
2398  0155 c7                	clrb	
2399  0156 3b                	pshd	
2400  0157 cc0147            	ldd	#L371_g_descUsdtNetInfoPoolIsoTp
2401  015a 4a038b8b          	call	L72f_DescUsdtNetAbsFinishReception
2403  015e 1b82              	leas	2,s
2404                         ; 2406       TpRxResetChannel(TP_CHANNEL_RX_PARAM_ONLY);
2406  0160 4a000000          	call	f_TpRxResetChannel
2408                         ; 2452 }
2411  0164 0a                	rtc	
2445                         ; 2465 void TP_API_CALLBACK_TYPE DescRxErrorIndication(TP_CHANNEL_RX_FORMAL_PARAM_DEF_FIRST vuint8 status)
2445                         ; 2466 {
2446                         	switch	.ftext
2447  0165                   f_DescRxErrorIndication:
2451                         ; 2468   DESC_IGNORE_UNREF_PARAM(status);
2453                         ; 2485         DescUsdtNetFinishReception(&g_descUsdtNetInfoPoolIsoTp[DESC_TPCONTEXT_PARAM_VALUE], kDescUsdtNetworkAbort);
2456  0165 cc0003            	ldd	#3
2457  0168 3b                	pshd	
2458  0169 cc0147            	ldd	#L371_g_descUsdtNetInfoPoolIsoTp
2459  016c 4a038b8b          	call	L72f_DescUsdtNetAbsFinishReception
2461  0170 1b82              	leas	2,s
2462                         ; 2499 }
2465  0172 0a                	rtc	
2806                         ; 2511 DESCNET_USDT_STATIC void DescUsdtNetIsoTpPrepareResponse(t_descUsdtNetInfoPoolPtr infoPool)
2806                         ; 2512 {
2807                         	switch	.ftext
2808  0173                   L51f_DescUsdtNetIsoTpPrepareResponse:
2810  0173 3b                	pshd	
2811  0174 37                	pshb	
2812       00000001          OFST:	set	1
2815                         ; 2518   DescUsdtNetIsoTpAssertUser(infoPool->busInfo.busType == kDescUsdtNetBusTypeIsoTp, kDescNetAssertWrongBusType);
2817  0175 b746              	tfr	d,y
2818  0177 e642              	ldab	2,y
2819  0179 04010d            	dbeq	b,L537
2822  017c cc09d6            	ldd	#2518
2823  017f 3b                	pshd	
2824  0180 cc0042            	ldd	#66
2825  0183 4a000000          	call	f_ApplDescFatalError
2827  0187 1b82              	leas	2,s
2828  0189                   L537:
2829                         ; 2521   sourceAddress = infoPool->busInfo.addressingInfo.isoTp.SourceAddress;
2832  0189 ed81              	ldy	OFST+0,s
2833  018b 180a4480          	movb	4,y,OFST-1,s
2834                         ; 2522   infoPool->busInfo.addressingInfo.isoTp.SourceAddress  = infoPool->busInfo.addressingInfo.isoTp.TargetAddress;
2836  018f 180a4544          	movb	5,y,4,y
2837                         ; 2523   infoPool->busInfo.addressingInfo.isoTp.TargetAddress = sourceAddress;
2839  0193 180a8045          	movb	OFST-1,s,5,y
2840                         ; 2528 }
2843  0197 1b83              	leas	3,s
2844  0199 0a                	rtc	
2921                         ; 2540 DESCNET_USDT_STATIC void DescUsdtNetIsoTpTransmitResponse(t_descUsdtNetInfoPoolPtr infoPool)
2921                         ; 2541 {
2922                         	switch	.ftext
2923  019a                   L71f_DescUsdtNetIsoTpTransmitResponse:
2925  019a 3b                	pshd	
2926  019b 1b9d              	leas	-3,s
2927       00000003          OFST:	set	3
2930                         ; 2553   DescUsdtNetIsoTpAssertUser(infoPool->busInfo.busType == kDescUsdtNetBusTypeIsoTp, kDescNetAssertWrongBusType);
2932  019d b746              	tfr	d,y
2933  019f e642              	ldab	2,y
2934  01a1 04010d            	dbeq	b,L577
2937  01a4 cc09f9            	ldd	#2553
2938  01a7 3b                	pshd	
2939  01a8 cc0042            	ldd	#66
2940  01ab 4a000000          	call	f_ApplDescFatalError
2942  01af 1b82              	leas	2,s
2943  01b1                   L577:
2944                         ; 2572   lBusInfoPoolTxRef = &g_busInfoPoolTxRef[TP_CHANNEL_TX_PARAM_VALUE];
2947  01b1 cc0145            	ldd	#L571_g_busInfoPoolTxRef
2948  01b4 6c80              	std	OFST-3,s
2949                         ; 2575   if(*lBusInfoPoolTxRef != V_NULL)
2951  01b6 fc0145            	ldd	L571_g_busInfoPoolTxRef
2952  01b9 272b              	beq	L777
2953                         ; 2577     DescInterruptDisable();
2955  01bb 4a000000          	call	f_VStdSuspendAllInterrupts
2957                         ; 2579     if(*lBusInfoPoolTxRef != V_NULL)
2959  01bf 18e7f30000        	tstw	[OFST-3,s]
2960  01c4 271c              	beq	L1001
2961                         ; 2583       lBusInfoPoolTxRefBackup = g_busInfoPoolTxRef[TP_CHANNEL_TX_PARAM_VALUE];
2963  01c6 1801800145        	movw	L571_g_busInfoPoolTxRef,OFST-3,s
2964                         ; 2585       g_busInfoPoolTxRef[TP_CHANNEL_TX_PARAM_VALUE] = infoPool;
2966  01cb 1805830145        	movw	OFST+0,s,L571_g_busInfoPoolTxRef
2967                         ; 2587       DescConfirmation(TP_CHANNEL_TX_PARAM_FIRST kTpBusy);
2969  01d0 cc0003            	ldd	#3
2970  01d3 4a023333          	call	f_DescConfirmation
2972                         ; 2589       g_busInfoPoolTxRef[TP_CHANNEL_TX_PARAM_VALUE] = lBusInfoPoolTxRefBackup;
2974  01d7 1805800145        	movw	OFST-3,s,L571_g_busInfoPoolTxRef
2975                         ; 2590       DescInterruptRestore();
2977  01dc 4a000000          	call	f_VStdResumeAllInterrupts
2979                         ; 2591       return;
2981  01e0 2026              	bra	L3001
2982  01e2                   L1001:
2983                         ; 2593     DescInterruptRestore();
2985  01e2 4a000000          	call	f_VStdResumeAllInterrupts
2987  01e6                   L777:
2988                         ; 2596   g_busInfoPoolTxRef[TP_CHANNEL_TX_PARAM_VALUE] = infoPool;
2990  01e6 ed83              	ldy	OFST+0,s
2991  01e8 7d0145            	sty	L571_g_busInfoPoolTxRef
2992                         ; 2601   TpTxSetTargetAddress (TP_CHANNEL_TX_PARAM_FIRST infoPool->busInfo.testerId); /* use the abstract info for spontaneus responses */
2994  01eb e641              	ldab	1,y
2995  01ed 87                	clra	
2996  01ee 4a000000          	call	f_TpTxSetTargetAddress
2998                         ; 2646   result = DanisIsoTpTransmit(TP_CHANNEL_TX_PARAM_VALUE, infoPool->resDataPtr, infoPool->dataLength);
3000  01f2 ed83              	ldy	OFST+0,s
3001  01f4 ec4a              	ldd	10,y
3002  01f6 3b                	pshd	
3003  01f7 ec4e              	ldd	14,y
3004  01f9 4a000000          	call	f_TpTransmit
3006  01fd 1b82              	leas	2,s
3007  01ff 6b82              	stab	OFST-1,s
3008                         ; 2648   if (result != kTpSuccess)
3010  0201 2705              	beq	L3001
3011                         ; 2650     DescConfirmation(TP_CHANNEL_TX_PARAM_FIRST result);
3013  0203 87                	clra	
3014  0204 4a023333          	call	f_DescConfirmation
3016  0208                   L3001:
3017                         ; 2652 }
3020  0208 1b85              	leas	5,s
3021  020a 0a                	rtc	
3107                         ; 2680 static void DescUsdtNetIsoTpCopyToCan(TpCopyToCanInfoStructPtr infoStruct)
3107                         ; 2681 {
3108                         	switch	.ftext
3109  020b                   L52f_DescUsdtNetIsoTpCopyToCan:
3111  020b 3b                	pshd	
3112  020c 3b                	pshd	
3113       00000002          OFST:	set	2
3116                         ; 2682   vuint8_least i = infoStruct->Length;
3118  020d b746              	tfr	d,y
3119  020f 18024580          	movw	5,y,OFST-2,s
3121  0213 200c              	bra	L3501
3122  0215                   L7401:
3123                         ; 2685     infoStruct->pDestination[i] = infoStruct->pSource[i];
3125  0215 b754              	tfr	x,d
3126  0217 ed82              	ldy	OFST+0,s
3127  0219 ee41              	ldx	1,y
3128  021b ed43              	ldy	3,y
3129  021d 180aeee6          	movb	d,y,d,x
3130  0221                   L3501:
3131                         ; 2683   while(i--)
3133  0221 ed80              	ldy	OFST-2,s
3134  0223 1a5f              	leax	-1,y
3135  0225 6e80              	stx	OFST-2,s
3136  0227 0476eb            	tbne	y,L7401
3137                         ; 2687 }
3140  022a 1b84              	leas	4,s
3141  022c 0a                	rtc	
3178                         ; 2699 vuint8 TP_API_CALLBACK_TYPE DescCopyToCAN(TpCopyToCanInfoStructPtr infoStruct)
3178                         ; 2700 {
3179                         	switch	.ftext
3180  022d                   f_DescCopyToCAN:
3184                         ; 2746         DescUsdtNetIsoTpCopyToCan(infoStruct);
3186  022d 4a020b0b          	call	L52f_DescUsdtNetIsoTpCopyToCan
3188                         ; 2747         return kTpSuccess;
3190  0231 c7                	clrb	
3193  0232 0a                	rtc	
3236                         ; 2774 void TP_API_CALLBACK_TYPE DescConfirmation(TP_CHANNEL_TX_FORMAL_PARAM_DEF_FIRST vuint8 status)
3236                         ; 2775 {
3237                         	switch	.ftext
3238  0233                   f_DescConfirmation:
3240  0233 3b                	pshd	
3241  0234 37                	pshb	
3242       00000001          OFST:	set	1
3245                         ; 2789         result = (status != kTpSuccess) ? kDescUsdtNetworkAbort:kDescUsdtNetworkOk;
3248  0235 044102            	tbeq	b,L05
3249  0238 c603              	ldab	#3
3250  023a                   L05:
3251  023a 6b80              	stab	OFST-1,s
3252                         ; 2791         DescUsdtNetFinishTransmission(g_busInfoPoolTxRef[TP_CHANNEL_TX_PARAM_VALUE], result);
3254  023c 87                	clra	
3255  023d 3b                	pshd	
3256  023e fc0145            	ldd	L571_g_busInfoPoolTxRef
3257  0241 4a03c6c6          	call	L13f_DescUsdtNetAbsFinishTransmission
3259                         ; 2814   g_busInfoPoolTxRef[TP_CHANNEL_TX_PARAM_VALUE] = V_NULL;
3261  0245 18790145          	clrw	L571_g_busInfoPoolTxRef
3262                         ; 2815 }
3265  0249 1b85              	leas	5,s
3266  024b 0a                	rtc	
3299                         ; 2827 vuint8 TP_API_CALLBACK_TYPE DescTxErrorIndication(TP_CHANNEL_TX_FORMAL_PARAM_DEF_FIRST vuint8 status)
3299                         ; 2828 {
3300                         	switch	.ftext
3301  024c                   f_DescTxErrorIndication:
3305                         ; 2830   DESC_IGNORE_UNREF_PARAM(status);
3307                         ; 2838     DescConfirmation(TP_CHANNEL_TX_PARAM_FIRST status);
3310  024c 87                	clra	
3311  024d 4a023333          	call	f_DescConfirmation
3313                         ; 2844         return kTpHoldChannel;
3315  0251 c7                	clrb	
3318  0252 0a                	rtc	
3356                         ; 2873 DESCNET_USDT_STATIC void DescUsdtNetIsoTpReleaseInfoPool(t_descUsdtNetInfoPoolPtr infoPool)
3356                         ; 2874 {
3357                         	switch	.ftext
3358  0253                   L12f_DescUsdtNetIsoTpReleaseInfoPool:
3360  0253 3b                	pshd	
3361       00000000          OFST:	set	0
3364                         ; 2875   DescUsdtNetIsoTpAssertUser(infoPool->busInfo.busType == kDescUsdtNetBusTypeIsoTp, kDescNetAssertWrongBusType);
3366  0254 b746              	tfr	d,y
3367  0256 e642              	ldab	2,y
3368  0258 04010d            	dbeq	b,L3511
3371  025b cc0b3b            	ldd	#2875
3372  025e 3b                	pshd	
3373  025f cc0042            	ldd	#66
3374  0262 4a000000          	call	f_ApplDescFatalError
3376  0266 1b82              	leas	2,s
3377  0268                   L3511:
3378                         ; 2876   infoPool->descHandle = kDescUsdtNetInvalidDescContext;
3381  0268 c6ff              	ldab	#255
3382  026a ee80              	ldx	OFST+0,s
3383  026c 6b09              	stab	9,x
3384                         ; 2877 }
3387  026e 31                	puly	
3388  026f 0a                	rtc	
3452                         ; 2943 TP_MEMORY_MODEL_DATA vuint8* TP_API_CALLBACK_TYPE DescGetFuncBuffer(vuint16 dataLength)
3452                         ; 2944 {
3453                         	switch	.ftext
3454  0270                   f_DescGetFuncBuffer:
3456  0270 3b                	pshd	
3457  0271 1b9b              	leas	-5,s
3458       00000005          OFST:	set	5
3461                         ; 2945   DescUsdtNetMsg returnValue = V_NULL;
3463  0273 186983            	clrw	OFST-2,s
3464                         ; 2951   addressCheckResult = DescOemCheckFuncTargetAddress(TpFuncGetTargetAddress());
3466  0276 4a000000          	call	f_TpFuncGetTargetAddress
3468  027a c1fe              	cmpb	#254
3469  027c 2605              	bne	L26
3470  027e cc0001            	ldd	#1
3471  0281 2002              	bra	L46
3472  0283                   L26:
3473  0283 87                	clra	
3474  0284 c7                	clrb	
3475  0285                   L46:
3476  0285 6c81              	std	OFST-4,s
3477                         ; 2952   if(addressCheckResult == kDescTrue)
3479  0287 8c0001            	cpd	#1
3480  028a 2611              	bne	L3021
3481                         ; 2956     addressCheckResult = DescOemCheckFuncSourceAddress(TpFuncGetSourceAddress());
3483  028c 4a000000          	call	f_TpFuncGetSourceAddress
3485  0290 c1f1              	cmpb	#241
3486  0292 2605              	bne	L66
3487  0294 cc0001            	ldd	#1
3488  0297 2002              	bra	L07
3489  0299                   L66:
3490  0299 87                	clra	
3491  029a c7                	clrb	
3492  029b                   L07:
3493  029b 6c81              	std	OFST-4,s
3494  029d                   L3021:
3495                         ; 2960   if(addressCheckResult == kDescTrue)
3497  029d 042445            	dbne	d,L5021
3498                         ; 2967     if (g_descUsdtNetInfoPoolIsoTp[DESC_TPCONTEXT_PARAM_VALUE].descHandle == kDescUsdtNetInvalidDescContext)
3500  02a0 f60150            	ldab	L371_g_descUsdtNetInfoPoolIsoTp+9
3501  02a3 04a13f            	ibne	b,L5021
3502                         ; 2971       g_descUsdtNetInfoPoolIsoTp[DESC_TPCONTEXT_PARAM_VALUE].busInfo.busType   = kDescUsdtNetBusTypeIsoTp;
3504  02a6 c601              	ldab	#1
3505  02a8 7b0149            	stab	L371_g_descUsdtNetInfoPoolIsoTp+2
3506                         ; 2980       g_descUsdtNetInfoPoolIsoTp[DESC_TPCONTEXT_PARAM_VALUE].busInfo.addressingInfo.isoTp.SourceAddress = TpFuncGetSourceAddress();
3508  02ab 4a000000          	call	f_TpFuncGetSourceAddress
3510  02af 7b014b            	stab	L371_g_descUsdtNetInfoPoolIsoTp+4
3511                         ; 2982       g_descUsdtNetInfoPoolIsoTp[DESC_TPCONTEXT_PARAM_VALUE].busInfo.testerId = g_descUsdtNetInfoPoolIsoTp[DESC_TPCONTEXT_PARAM_VALUE].busInfo.addressingInfo.isoTp.SourceAddress;
3513  02b2 7b0148            	stab	L371_g_descUsdtNetInfoPoolIsoTp+1
3514                         ; 2989       g_descUsdtNetInfoPoolIsoTp[DESC_TPCONTEXT_PARAM_VALUE].reqType           = kDescUsdtNetReqTypeFunctional;
3516  02b5 c601              	ldab	#1
3517  02b7 7b014d            	stab	L371_g_descUsdtNetInfoPoolIsoTp+6
3518                         ; 2990       g_descUsdtNetInfoPoolIsoTp[DESC_TPCONTEXT_PARAM_VALUE].resType           = kDescUsdtResponseNone;
3520  02ba 79014e            	clr	L371_g_descUsdtNetInfoPoolIsoTp+7
3521                         ; 2991       g_descUsdtNetInfoPoolIsoTp[DESC_TPCONTEXT_PARAM_VALUE].reqDataPtr        = g_descBuffer[DESC_TPCONTEXT_PARAM_VALUE];
3523  02bd cc0043            	ldd	#L771_g_descBuffer
3524  02c0 7c0153            	std	L371_g_descUsdtNetInfoPoolIsoTp+12
3525                         ; 2992       g_descUsdtNetInfoPoolIsoTp[DESC_TPCONTEXT_PARAM_VALUE].resDataPtr        = g_descBuffer[DESC_TPCONTEXT_PARAM_VALUE];
3527  02c3 7c0155            	std	L371_g_descUsdtNetInfoPoolIsoTp+14
3528                         ; 2993       g_descUsdtNetInfoPoolIsoTp[DESC_TPCONTEXT_PARAM_VALUE].dataLength        = dataLength;
3530  02c6 1805850151        	movw	OFST+0,s,L371_g_descUsdtNetInfoPoolIsoTp+10
3531                         ; 2994       g_descUsdtNetInfoPoolIsoTp[DESC_TPCONTEXT_PARAM_VALUE].descHandle        = DESC_TPCONTEXT_PARAM_VALUE; /* Write used context */
3533  02cb 790150            	clr	L371_g_descUsdtNetInfoPoolIsoTp+9
3534                         ; 2996       result = DescUsdtNetStartReception(&g_descUsdtNetInfoPoolIsoTp[DESC_TPCONTEXT_PARAM_VALUE]);
3536  02ce cc0147            	ldd	#L371_g_descUsdtNetInfoPoolIsoTp
3537  02d1 4a036a6a          	call	L33f_DescUsdtNetAbsStartReception
3539  02d5 6b80              	stab	OFST-5,s
3540                         ; 2997       if(result == kDescUsdtNetworkOk)
3542  02d7 2607              	bne	L1121
3543                         ; 2999         returnValue = g_descUsdtNetInfoPoolIsoTp[DESC_TPCONTEXT_PARAM_VALUE].reqDataPtr;
3545  02d9 1801830153        	movw	L371_g_descUsdtNetInfoPoolIsoTp+12,OFST-2,s
3547  02de 2005              	bra	L5021
3548  02e0                   L1121:
3549                         ; 3007         g_descUsdtNetInfoPoolIsoTp[DESC_TPCONTEXT_PARAM_VALUE].descHandle = kDescUsdtNetInvalidDescContext;
3551  02e0 c6ff              	ldab	#255
3552  02e2 7b0150            	stab	L371_g_descUsdtNetInfoPoolIsoTp+9
3553  02e5                   L5021:
3554                         ; 3012   return returnValue;
3556  02e5 ec83              	ldd	OFST-2,s
3559  02e7 1b87              	leas	7,s
3560  02e9 0a                	rtc	
3594                         ; 3025 void TP_API_CALLBACK_TYPE DescFuncReqInd(vuint16 dataLen)
3594                         ; 3026 {
3595                         	switch	.ftext
3596  02ea                   f_DescFuncReqInd:
3600                         ; 3032   DESC_IGNORE_UNREF_PARAM(dataLen);
3602                         ; 3060     g_descUsdtNetInfoPoolIsoTp[DESC_TPCONTEXT_PARAM_VALUE].busInfo.addressingInfo.isoTp.TargetAddress = kDescEcuNumber;
3604  02ea c6c2              	ldab	#194
3605  02ec 7b014c            	stab	L371_g_descUsdtNetInfoPoolIsoTp+5
3606                         ; 3069     DescUsdtNetFinishReception(&g_descUsdtNetInfoPoolIsoTp[DESC_TPCONTEXT_PARAM_VALUE], kDescUsdtNetworkOk);
3608  02ef 87                	clra	
3609  02f0 c7                	clrb	
3610  02f1 3b                	pshd	
3611  02f2 cc0147            	ldd	#L371_g_descUsdtNetInfoPoolIsoTp
3612  02f5 4a038b8b          	call	L72f_DescUsdtNetAbsFinishReception
3614  02f9 1b82              	leas	2,s
3615                         ; 3071     TpFuncResetChannel();
3617  02fb 4a000000          	call	f_TpFuncResetChannel
3619                         ; 3073 }
3622  02ff 0a                	rtc	
3648                         ; 3128 static void DescNetworkIterInit(DESC_CONTEXT_FORMAL_PARAM_DEF_ONLY)
3648                         ; 3129 {
3649                         	switch	.ftext
3650  0300                   L53f_DescNetworkIterInit:
3654                         ; 3131   g_descContextCtrl[DESC_CONTEXT_PARAM_VALUE].txState = kDescUsdtNetworkOk;
3656  0300 79003e            	clr	L302_g_descContextCtrl
3657                         ; 3132   g_descContextCtrl[DESC_CONTEXT_PARAM_VALUE].isApplError = 0;
3659                         ; 3133   g_descContextCtrl[DESC_CONTEXT_PARAM_VALUE].contextMode = kDescContextModeNormal;
3661  0303 1d003f0f          	bclr	L302_g_descContextCtrl+1,15
3662                         ; 3136   g_descInterruptContextCtrl[DESC_CONTEXT_PARAM_VALUE].activity = kDescContextIdle;
3664  0307 79003c            	clr	L502_g_descInterruptContextCtrl+2
3665                         ; 3137   g_descInterruptContextCtrl[DESC_CONTEXT_PARAM_VALUE].isContextLocked = 0;
3667                         ; 3139   g_descInterruptContextCtrl[DESC_CONTEXT_PARAM_VALUE].forcedRcrRpState = kDescForcedRcrRpIdle;
3669  030a 1d003d07          	bclr	L502_g_descInterruptContextCtrl+3,7
3670                         ; 3144   g_descRcrrpBuffer[DESC_CONTEXT_PARAM_VALUE][0] = kDescNegResSId;
3672  030e c67f              	ldab	#127
3673  0310 7b0040            	stab	L102_g_descRcrrpBuffer
3674                         ; 3145   g_descRcrrpBuffer[DESC_CONTEXT_PARAM_VALUE][2] = kDescNrcResponsePending;
3676  0313 c678              	ldab	#120
3677  0315 7b0042            	stab	L102_g_descRcrrpBuffer+2
3678                         ; 3151 }
3681  0318 0a                	rtc	
3706                         ; 3161 static void DescReleaseContext(DESC_CONTEXT_FORMAL_PARAM_DEF_ONLY)
3706                         ; 3162 {
3707                         	switch	.ftext
3708  0319                   L74f_DescReleaseContext:
3712                         ; 3169     g_descDoReloadS1Timer = kDescTrue;
3714  0319 cc0001            	ldd	#1
3715  031c 7c0032            	std	L712_g_descDoReloadS1Timer
3716                         ; 3176   g_descInterruptContextCtrl[DESC_CONTEXT_PARAM_VALUE].isContextLocked = 0;
3720  031f 1d003d04          	bclr	L502_g_descInterruptContextCtrl+3,4
3721                         ; 3178   DescUsdtNetAbsReleaseInfoPool(g_descInterruptContextCtrl[DESC_CONTEXT_PARAM_VALUE].infoPoolPtr );
3723  0323 fc003a            	ldd	L502_g_descInterruptContextCtrl
3724  0326 4a025353          	call	L12f_DescUsdtNetIsoTpReleaseInfoPool
3726                         ; 3179 }
3729  032a 0a                	rtc	
3773                         ; 3322 static DescBool DescIsTesterPresent(t_descUsdtNetInfoPoolPtr infoPool)
3773                         ; 3323 {
3774                         	switch	.ftext
3775  032b                   L73f_DescIsTesterPresent:
3777  032b 3b                	pshd	
3778  032c 3b                	pshd	
3779       00000002          OFST:	set	2
3782                         ; 3324   DescBool returnValue = kDescFalse;
3784  032d 186980            	clrw	OFST-2,s
3785                         ; 3326   if ((infoPool->reqType == kDescUsdtNetReqTypeFunctional) && /* Only for functional request */
3785                         ; 3327       (infoPool->reqDataPtr[0] == 0x3e) &&                /* Is it a TesterPresent? */
3785                         ; 3328       (infoPool->reqDataPtr[1] == 0x80) &&                /* Is the SPRIMB set?*/
3785                         ; 3329       (infoPool->dataLength == 0x02))                     /* Do little format check */
3787  0330 b746              	tfr	d,y
3788  0332 e646              	ldab	6,y
3789  0334 042120            	dbne	b,L3721
3791  0337 ed82              	ldy	OFST+0,s
3792  0339 e6eb000c          	ldab	[12,y]
3793  033d c13e              	cmpb	#62
3794  033f 2616              	bne	L3721
3796  0341 ed4c              	ldy	12,y
3797  0343 e641              	ldab	1,y
3798  0345 c180              	cmpb	#128
3799  0347 260e              	bne	L3721
3801  0349 ed82              	ldy	OFST+0,s
3802  034b ec4a              	ldd	10,y
3803  034d 8c0002            	cpd	#2
3804  0350 2605              	bne	L3721
3805                         ; 3332     returnValue = kDescTrue;
3807  0352 cc0001            	ldd	#1
3808  0355 6c80              	std	OFST-2,s
3809  0357                   L3721:
3810                         ; 3334   return returnValue;
3812  0357 ec80              	ldd	OFST-2,s
3815  0359 1b84              	leas	4,s
3816  035b 0a                	rtc	
3840                         ; 3366 static void DescNetworkInitPowerOn(void)
3840                         ; 3367 {
3841                         	switch	.ftext
3842  035c                   L14f_DescNetworkInitPowerOn:
3846                         ; 3368   DescUsdtNetAbsInitPowerOn();
3848  035c 4a00b5b5          	call	L7f_DescUsdtNetIsoTpInitPowerOn
3850                         ; 3370 }
3854  0360 0a                	rtc	
3877                         ; 3382 static void DescNetworkInit(void)
3877                         ; 3383 {
3878                         	switch	.ftext
3879  0361                   L34f_DescNetworkInit:
3883                         ; 3384   DescUsdtNetAbsInit();
3885  0361 4a00cccc          	call	L11f_DescUsdtNetIsoTpInit
3887                         ; 3386 }
3891  0365 0a                	rtc	
3915                         ; 3397 vuint8 DESC_API_CALL_TYPE DescGetActivityState(DESC_CONTEXT_FORMAL_PARAM_DEF_ONLY)
3915                         ; 3398 {
3916                         	switch	.ftext
3917  0366                   f_DescGetActivityState:
3921                         ; 3399   return g_descInterruptContextCtrl[DESC_CONTEXT_PARAM_VALUE].activity;
3923  0366 f6003c            	ldab	L502_g_descInterruptContextCtrl+2
3926  0369 0a                	rtc	
3976                         ; 3450 DESCNET_USDT_STATIC t_descUsdtNetResult DescUsdtNetAbsStartReception(t_descUsdtNetInfoPoolPtr infoPool)
3976                         ; 3451 {
3977                         	switch	.ftext
3978  036a                   L33f_DescUsdtNetAbsStartReception:
3980  036a 3b                	pshd	
3981  036b 37                	pshb	
3982       00000001          OFST:	set	1
3985                         ; 3452   t_descUsdtNetResult returnValue = kDescUsdtNetworkFailed;
3987  036c c601              	ldab	#1
3988  036e 6b80              	stab	OFST-1,s
3989                         ; 3454   if(g_descInterruptContextCtrl[DESC_CONTEXT_PARAM_WRAPPER_INDEX(infoPool->descHandle)].isContextLocked == 0)
3991  0370 1e003d0413        	brset	L502_g_descInterruptContextCtrl+3,4,L1531
3992                         ; 3461       g_descInterruptContextCtrl[DESC_CONTEXT_PARAM_WRAPPER_INDEX(infoPool->descHandle)].isContextLocked = 1;
3994  0375 1c003d04          	bset	L502_g_descInterruptContextCtrl+3,4
3995                         ; 3462       g_descInterruptContextCtrl[DESC_CONTEXT_PARAM_WRAPPER_INDEX(infoPool->descHandle)].activity |= kDescContextActiveRxBegin;
3997  0379 1c003c01          	bset	L502_g_descInterruptContextCtrl+2,1
3998                         ; 3465       g_descInterruptContextCtrl[DESC_CONTEXT_PARAM_WRAPPER_INDEX(infoPool->descHandle)].infoPoolPtr = infoPool;
4000  037d ed81              	ldy	OFST+0,s
4001  037f 7d003a            	sty	L502_g_descInterruptContextCtrl
4002                         ; 3475       g_descTesterAddress[DESC_CONTEXT_PARAM_WRAPPER_INDEX(infoPool->descHandle)] = infoPool->busInfo.testerId;
4005  0382 180d410039        	movb	1,y,L702_g_descTesterAddress
4006                         ; 3487       returnValue = kDescUsdtNetworkOk;
4010  0387 c7                	clrb	
4011  0388                   L1531:
4012                         ; 3490   return returnValue;
4016  0388 1b83              	leas	3,s
4017  038a 0a                	rtc	
4076                         ; 3504 DESCNET_USDT_STATIC void DescUsdtNetAbsFinishReception(t_descUsdtNetInfoPoolPtr infoPool, t_descUsdtNetResult status)
4076                         ; 3505 {
4077                         	switch	.ftext
4078  038b                   L72f_DescUsdtNetAbsFinishReception:
4080  038b 3b                	pshd	
4081  038c 3b                	pshd	
4082       00000002          OFST:	set	2
4085                         ; 3507   g_descInterruptContextCtrl[DESC_CONTEXT_PARAM_WRAPPER_INDEX(infoPool->descHandle)].activity &= (DescContextActivity)(~kDescContextActiveRxBegin);
4087  038d 1d003c01          	bclr	L502_g_descInterruptContextCtrl+2,1
4088                         ; 3508   if ((status == kDescUsdtNetworkOk) &&
4088                         ; 3509       (g_descInterruptContextCtrl[DESC_CONTEXT_PARAM_WRAPPER_INDEX(infoPool->descHandle)].infoPoolPtr == infoPool))
4090  0391 e788              	tst	OFST+6,s
4091  0393 262a              	bne	L5041
4093  0395 bc003a            	cpd	L502_g_descInterruptContextCtrl
4094  0398 2625              	bne	L5041
4095                         ; 3512     DescBool result = DescIsTesterPresent(infoPool);
4097  039a 4a032b2b          	call	L73f_DescIsTesterPresent
4099  039e 6c80              	std	OFST-2,s
4100                         ; 3513     if (result == kDescFalse)
4102  03a0 261d              	bne	L5041
4103                         ; 3518       g_descMsgContext[DESC_CONTEXT_PARAM_WRAPPER_INDEX(infoPool->descHandle)].busInfo = infoPool->busInfo;
4106  03a2 ed82              	ldy	OFST+0,s
4107  03a4 ce0028            	ldx	#L722_g_descMsgContext+9
4108  03a7 18027131          	movw	2,y+,2,x+
4109  03ab 18027131          	movw	2,y+,2,x+
4110  03af 18024000          	movw	0,y,0,x
4111                         ; 3520       DescUsdtNetAbsPrepareResponse(infoPool);
4113  03b3 ec82              	ldd	OFST+0,s
4114  03b5 4a017373          	call	L51f_DescUsdtNetIsoTpPrepareResponse
4116                         ; 3523       g_descInterruptContextCtrl[DESC_CONTEXT_PARAM_WRAPPER_INDEX(infoPool->descHandle)].activity |= kDescContextActiveRxEnd;
4118  03b9 1c003c02          	bset	L502_g_descInterruptContextCtrl+2,2
4119                         ; 3525       return;
4121  03bd 2004              	bra	L411
4122  03bf                   L5041:
4123                         ; 3532   DescReleaseContext(DESC_CONTEXT_PARAM_WRAPPER_ONLY(infoPool->descHandle));
4125  03bf 4a031919          	call	L74f_DescReleaseContext
4127                         ; 3533 }
4128  03c3                   L411:
4131  03c3 1b84              	leas	4,s
4132  03c5 0a                	rtc	
4188                         ; 3545 DESCNET_USDT_STATIC void DescUsdtNetAbsFinishTransmission(t_descUsdtNetInfoPoolPtr infoPool, t_descUsdtNetResult status)
4188                         ; 3546 {
4189                         	switch	.ftext
4190  03c6                   L13f_DescUsdtNetAbsFinishTransmission:
4192  03c6 3b                	pshd	
4193  03c7 37                	pshb	
4194       00000001          OFST:	set	1
4197                         ; 3553   if (status == kDescUsdtNetworkOk)
4200                         ; 3562   switch (infoPool->resType)
4202  03c8 b746              	tfr	d,y
4203  03ca e647              	ldab	7,y
4205  03cc 04010c            	dbeq	b,L7041
4206  03cf 040109            	dbeq	b,L7041
4207  03d2 040106            	dbeq	b,L7041
4208  03d5 c002              	subb	#2
4209  03d7 270b              	beq	L1141
4210  03d9 2016              	bra	L3541
4211  03db                   L7041:
4212                         ; 3564   case kDescUsdtResponsePositive:
4212                         ; 3565     /* fall through */
4212                         ; 3566   case kDescUsdtResponseNegative:
4212                         ; 3567     /* fall through */
4212                         ; 3568   case kDescUsdtResponseRingBuffer:
4212                         ; 3569     /* Make post processing not always success (if called from the error indication)*/
4212                         ; 3570     DescDoPostProcessing(DESC_CONTEXT_PARAM_FIRST status);
4214  03db e687              	ldab	OFST+6,s
4215  03dd 87                	clra	
4216  03de 4a0ac3c3          	call	L36f_DescDoPostProcessing
4218                         ; 3594     break;
4220  03e2 200d              	bra	L3541
4221  03e4                   L1141:
4222                         ; 3600       result = (vuint8)((status == kDescUsdtNetworkOk)?kDescOk:kDescFailed);
4224  03e4 e687              	ldab	OFST+6,s
4225  03e6 2702              	beq	L221
4226  03e8 c601              	ldab	#1
4227  03ea                   L221:
4228  03ea 6b80              	stab	OFST-1,s
4229                         ; 3602       ApplDescRcrRpConfirmation(DESC_CONTEXT_PARAM_FIRST result);
4231  03ec 87                	clra	
4232  03ed 4a000000          	call	f_ApplDescRcrRpConfirmation
4234                         ; 3607   case kDescUsdtResponseNegativeRCR_RP:
4234                         ; 3608 #if defined (DESC_USDTNET_ENABLE_VECTOR_ISO_TP)
4234                         ; 3609   /* Save code */
4234                         ; 3610 #else
4234                         ; 3611     if (infoPool->resDataPtr != g_descRcrrpBuffer[DESC_CONTEXT_PARAM_VALUE])
4234                         ; 3612     {
4234                         ; 3613       infoPool->resDataPtr = V_NULL;
4234                         ; 3614       DescUsdtNetReleaseTransmissionObj(infoPool);
4234                         ; 3615     }
4234                         ; 3616 #endif
4234                         ; 3617     break;
4236                         ; 3628   default:
4236                         ; 3629     break;
4238  03f1                   L3541:
4239                         ; 3632   infoPool->resType = kDescUsdtResponseNone;
4241  03f1 ee81              	ldx	OFST+0,s
4242  03f3 6907              	clr	7,x
4243                         ; 3633 }
4246  03f5 1b83              	leas	3,s
4247  03f7 0a                	rtc	
4275                         ; 3644 static void DescTransmitRcrRp(DESC_CONTEXT_FORMAL_PARAM_DEF_ONLY)
4275                         ; 3645 {
4276                         	switch	.ftext
4277  03f8                   L54f_DescTransmitRcrRp:
4281                         ; 3658     g_descInterruptContextCtrl[DESC_CONTEXT_PARAM_VALUE].infoPoolPtr->resDataPtr = g_descRcrrpBuffer[DESC_CONTEXT_PARAM_VALUE];
4283  03f8 cc0040            	ldd	#L102_g_descRcrrpBuffer
4284  03fb fd003a            	ldy	L502_g_descInterruptContextCtrl
4285  03fe 6c4e              	std	14,y
4286                         ; 3660   g_descInterruptContextCtrl[DESC_CONTEXT_PARAM_VALUE].infoPoolPtr->dataLength = kDescNegResLen;
4288  0400 cc0003            	ldd	#3
4289  0403 6c4a              	std	10,y
4290                         ; 3662   DescUsdtNetAbsTransmitResponse(g_descInterruptContextCtrl[DESC_CONTEXT_PARAM_VALUE].infoPoolPtr);
4292  0405 b764              	tfr	y,d
4293  0407 4a019a9a          	call	L71f_DescUsdtNetIsoTpTransmitResponse
4295                         ; 3663   DescReloadT2TimerWithP3maxTime(DESC_CONTEXT_PARAM_VALUE);
4297  040b f60034            	ldab	L512_g_descCurrSessionNumber
4298  040e 87                	clra	
4299  040f 59                	lsld	
4300  0410 59                	lsld	
4301  0411 b746              	tfr	d,y
4302  0413 1805ea00050037    	movw	L342_g_descP2TicksTable+2,y,L112_g_descT2Timer
4303                         ; 3669 }
4307  041a 0a                	rtc	
4331                         ; 3680 vuint8 DESC_API_CALL_TYPE DescGetTesterAddress(DESC_CONTEXT_FORMAL_PARAM_DEF_ONLY)
4331                         ; 3681 {
4332                         	switch	.ftext
4333  041b                   f_DescGetTesterAddress:
4337                         ; 3682   return g_descTesterAddress[DESC_CONTEXT_PARAM_VALUE];
4339  041b f60039            	ldab	L702_g_descTesterAddress
4342  041e 0a                	rtc	
4369                         ; 3694 t_descUsdtNetBus* DESC_API_CALL_TYPE DescGetCurrentBusInfo (DESC_CONTEXT_FORMAL_PARAM_DEF_ONLY)
4369                         ; 3695 {
4370                         	switch	.ftext
4371  041f                   f_DescGetCurrentBusInfo:
4375                         ; 3696   return &(g_descMsgContext[DESC_CONTEXT_PARAM_VALUE].busInfo);
4377  041f cc0028            	ldd	#L722_g_descMsgContext+9
4380  0422 0a                	rtc	
4403                         ; 3708 static void DescTimingIterInit(DESC_CONTEXT_FORMAL_PARAM_DEF_ONLY)
4403                         ; 3709 {
4404                         	switch	.ftext
4405  0423                   L35f_DescTimingIterInit:
4409                         ; 3710   DescDeactivateT2Timer(DESC_CONTEXT_PARAM_VALUE);
4411  0423 18790037          	clrw	L112_g_descT2Timer
4412                         ; 3712 }
4416  0427 0a                	rtc	
4442                         ; 3724 void DESC_API_CALL_TYPE DescForceRcrRpResponse(DESC_CONTEXT_FORMAL_PARAM_DEF_ONLY)
4442                         ; 3725 {
4443                         	switch	.ftext
4444  0428                   f_DescForceRcrRpResponse:
4446  0428 37                	pshb	
4447       00000001          OFST:	set	1
4450                         ; 3730   DescAssertUser(((g_descMsgContext[DESC_CONTEXT_PARAM_VALUE].msgAddInfo.resOnReq & g_descMsgContext[DESC_CONTEXT_PARAM_VALUE].msgAddInfo.reqType) != 0), kDescAssertInvalidUsageOfForceRcrRpApi);
4453  0429 f60027            	ldab	L722_g_descMsgContext+8
4454  042c c403              	andb	#3
4455  042e 6b80              	stab	OFST-1,s
4456  0430 f60027            	ldab	L722_g_descMsgContext+8
4457  0433 c40c              	andb	#12
4458  0435 54                	lsrb	
4459  0436 54                	lsrb	
4460  0437 e480              	andb	OFST-1,s
4461  0439 260d              	bne	L7251
4464  043b cc0e92            	ldd	#3730
4465  043e 3b                	pshd	
4466  043f cc0024            	ldd	#36
4467  0442 4a000000          	call	f_ApplDescFatalError
4469  0446 1b82              	leas	2,s
4470  0448                   L7251:
4471                         ; 3732   g_descInterruptContextCtrl[DESC_CONTEXT_PARAM_VALUE].forcedRcrRpState = kDescForcedRcrRpCharged;
4474  0448 1d003d03          	bclr	L502_g_descInterruptContextCtrl+3,3
4475  044c 1c003d01          	bset	L502_g_descInterruptContextCtrl+3,1
4476                         ; 3733 }
4479  0450 1b81              	leas	1,s
4480  0452 0a                	rtc	
4505                         ; 3746 static void DescTimingOnceInit(void)
4505                         ; 3747 {
4506                         	switch	.ftext
4507  0453                   L15f_DescTimingOnceInit:
4511                         ; 3750   g_descCurrSessionNumber = 0;
4513  0453 87                	clra	
4514  0454 7a0034            	staa	L512_g_descCurrSessionNumber
4515                         ; 3754   g_descDoReloadS1Timer = kDescFalse;
4517  0457 c7                	clrb	
4518  0458 7c0032            	std	L712_g_descDoReloadS1Timer
4519  045b 7c0035            	std	L312_g_descS1Timer
4520                         ; 3756   DescDeactivateS1Timer();
4522                         ; 3757 }
4525  045e 0a                	rtc	
4548                         ; 3909 static void DescStateOnceInit(void)
4548                         ; 3910 {
4549                         	switch	.ftext
4550  045f                   L16f_DescStateOnceInit:
4554                         ; 3911   DescStateInit();
4556  045f 4a046464          	call	f_DescStateInit
4558                         ; 3912 }
4561  0463 0a                	rtc	
4584                         ; 3914 void DESC_API_CALL_TYPE DescStateInit(void)
4584                         ; 3915 {
4585                         	switch	.ftext
4586  0464                   f_DescStateInit:
4590                         ; 3916   g_descCurState.stateSession = kDescStateSessionDefault;
4592  0464 1d00313f          	bclr	L122_g_descCurState+1,63
4593  0468 1c003101          	bset	L122_g_descCurState+1,1
4594                         ; 3917   g_descCurState.stateSecurityAccess = kDescStateSecurityAccessLocked;
4596  046c 1d003007          	bclr	L122_g_descCurState,7
4597  0470 1c003001          	bset	L122_g_descCurState,1
4598                         ; 3918 }
4601  0474 0a                	rtc	
4625                         ; 3921 DescStateGroup DESC_API_CALL_TYPE DescGetStateSession(void)
4625                         ; 3922 {
4626                         	switch	.ftext
4627  0475                   f_DescGetStateSession:
4631                         ; 3923   return g_descCurState.stateSession;
4633  0475 f60031            	ldab	L122_g_descCurState+1
4634  0478 c43f              	andb	#63
4635  047a 87                	clra	
4638  047b 0a                	rtc	
4674                         ; 3927 void DESC_API_CALL_TYPE DescSetStateSession(DescStateGroup descState)
4674                         ; 3928 {
4675                         	switch	.ftext
4676  047c                   f_DescSetStateSession:
4678       00000000          OFST:	set	0
4681                         ; 3930   DescAssertCommon((descState != 0), kDescAssertZeroStateValue);
4683  047c 6cae              	std	2,-s
4684  047e 260d              	bne	L5061
4687  0480 cc0f5a            	ldd	#3930
4688  0483 3b                	pshd	
4689  0484 cc0013            	ldd	#19
4690  0487 4a000000          	call	f_ApplDescFatalError
4692  048b 1b82              	leas	2,s
4693  048d                   L5061:
4694                         ; 3931   DescOemOnTransitionSession(descState, g_descCurState.stateSession);
4697  048d f60031            	ldab	L122_g_descCurState+1
4698  0490 c43f              	andb	#63
4699  0492 87                	clra	
4700  0493 3b                	pshd	
4701  0494 ec82              	ldd	OFST+2,s
4702  0496 4a058383          	call	L75f_DescOnTransitionStateSession
4704                         ; 3932   ApplDescOnTransitionSession(descState, g_descCurState.stateSession);
4706  049a f60031            	ldab	L122_g_descCurState+1
4707  049d c43f              	andb	#63
4708  049f 87                	clra	
4709  04a0 6c80              	std	0,s
4710  04a2 ec82              	ldd	OFST+2,s
4711  04a4 4a000000          	call	f_ApplDescOnTransitionSession
4713  04a8 1b82              	leas	2,s
4714                         ; 3933   g_descCurState.stateSession = CANBITTYPE_CAST descState;
4716  04aa e681              	ldab	OFST+1,s
4717  04ac 1d00313f          	bclr	L122_g_descCurState+1,63
4718  04b0 c43f              	andb	#63
4719  04b2 fa0031            	orab	L122_g_descCurState+1
4720  04b5 7b0031            	stab	L122_g_descCurState+1
4721                         ; 3934 }
4724  04b8 31                	puly	
4725  04b9 0a                	rtc	
4749                         ; 3937 DescStateGroup DESC_API_CALL_TYPE DescGetStateSecurityAccess(void)
4749                         ; 3938 {
4750                         	switch	.ftext
4751  04ba                   f_DescGetStateSecurityAccess:
4755                         ; 3939   return g_descCurState.stateSecurityAccess;
4757  04ba f60030            	ldab	L122_g_descCurState
4758  04bd c407              	andb	#7
4759  04bf 87                	clra	
4762  04c0 0a                	rtc	
4797                         ; 3943 void DESC_API_CALL_TYPE DescSetStateSecurityAccess(DescStateGroup descState)
4797                         ; 3944 {
4798                         	switch	.ftext
4799  04c1                   f_DescSetStateSecurityAccess:
4801       00000000          OFST:	set	0
4804                         ; 3946   DescAssertCommon((descState != 0), kDescAssertZeroStateValue);
4806  04c1 6cae              	std	2,-s
4807  04c3 260d              	bne	L3361
4810  04c5 cc0f6a            	ldd	#3946
4811  04c8 3b                	pshd	
4812  04c9 cc0013            	ldd	#19
4813  04cc 4a000000          	call	f_ApplDescFatalError
4815  04d0 1b82              	leas	2,s
4816  04d2                   L3361:
4817                         ; 3948   ApplDescOnTransitionSecurityAccess(descState, g_descCurState.stateSecurityAccess);
4821  04d2 f60030            	ldab	L122_g_descCurState
4822  04d5 c407              	andb	#7
4823  04d7 87                	clra	
4824  04d8 3b                	pshd	
4825  04d9 ec82              	ldd	OFST+2,s
4826  04db 4a000000          	call	f_ApplDescOnTransitionSecurityAccess
4828  04df 1b82              	leas	2,s
4829                         ; 3949   g_descCurState.stateSecurityAccess = CANBITTYPE_CAST descState;
4831  04e1 e681              	ldab	OFST+1,s
4832  04e3 1d003007          	bclr	L122_g_descCurState,7
4833  04e7 c407              	andb	#7
4834  04e9 fa0030            	orab	L122_g_descCurState
4835  04ec 7b0030            	stab	L122_g_descCurState
4836                         ; 3950 }
4839  04ef 31                	puly	
4840  04f0 0a                	rtc	
4925                         ; 3953 static DescNegResCode DescCheckState(V_MEMROM1 DescStateInfo V_MEMROM2 V_MEMROM3* refState)
4925                         ; 3954 {
4926                         	switch	.ftext
4927  04f1                   L762f_DescCheckState:
4929  04f1 3b                	pshd	
4930  04f2 37                	pshb	
4931       00000001          OFST:	set	1
4934                         ; 3955   if((refState->stateSession & g_descCurState.stateSession) == 0)
4936  04f3 f60031            	ldab	L122_g_descCurState+1
4937  04f6 c43f              	andb	#63
4938  04f8 6b80              	stab	OFST-1,s
4939  04fa ed81              	ldy	OFST+0,s
4940  04fc e641              	ldab	1,y
4941  04fe c43f              	andb	#63
4942  0500 e480              	andb	OFST-1,s
4943  0502 2604              	bne	L1071
4944                         ; 3957     return kDescNrcRejectStateSession;
4946  0504 c611              	ldab	#17
4948  0506 2011              	bra	L651
4949  0508                   L1071:
4950                         ; 3959   if((refState->stateSecurityAccess & g_descCurState.stateSecurityAccess) == 0)
4952  0508 f60030            	ldab	L122_g_descCurState
4953  050b c407              	andb	#7
4954  050d 6b80              	stab	OFST-1,s
4955  050f e640              	ldab	0,y
4956  0511 c407              	andb	#7
4957  0513 e480              	andb	OFST-1,s
4958  0515 2605              	bne	L3071
4959                         ; 3961     return kDescNrcRejectStateSecurityAccess;
4961  0517 c633              	ldab	#51
4963  0519                   L651:
4965  0519 1b83              	leas	3,s
4966  051b 0a                	rtc	
4967  051c                   L3071:
4968                         ; 3963   return kDescNrcNone;
4970  051c c7                	clrb	
4972  051d 20fa              	bra	L651
5022                         ; 3967 static void DescSetState(DescSvcInstIndex svcInstHandle)
5022                         ; 3968 {
5023                         	switch	.ftext
5024  051f                   L562f_DescSetState:
5026  051f 3b                	pshd	
5027  0520 1b9d              	leas	-3,s
5028       00000003          OFST:	set	3
5031                         ; 3970   if(g_descSvcInst[svcInstHandle].setStateIndex != kDescStateNoTransition)
5033  0522 cd000b            	ldy	#11
5034  0525 13                	emul	
5035  0526 b746              	tfr	d,y
5036  0528 e6ea012a          	ldab	L752_g_descSvcInst+5,y
5037  052c c105              	cmpb	#5
5038  052e 2750              	beq	L7271
5039                         ; 3972     curRefState = &g_descStateGroupTransition[g_descSvcInst[svcInstHandle].setStateIndex][0];
5041  0530 ec83              	ldd	OFST+0,s
5042  0532 cd000b            	ldy	#11
5043  0535 13                	emul	
5044  0536 b746              	tfr	d,y
5045  0538 e6ea012a          	ldab	L752_g_descSvcInst+5,y
5046  053c 87                	clra	
5047  053d 59                	lsld	
5048  053e 59                	lsld	
5049  053f c30033            	addd	#L742_g_descStateGroupTransition
5050  0542 6c81              	std	OFST-2,s
5051                         ; 3973     DescInterruptDisable();
5053  0544 4a000000          	call	f_VStdSuspendAllInterrupts
5055                         ; 3974     if((g_descCurState.stateSession & curRefState->stateSession) != 0)
5057  0548 f60031            	ldab	L122_g_descCurState+1
5058  054b c43f              	andb	#63
5059  054d 6b80              	stab	OFST-3,s
5060  054f ed81              	ldy	OFST-2,s
5061  0551 e641              	ldab	1,y
5062  0553 c43f              	andb	#63
5063  0555 e480              	andb	OFST-3,s
5064  0557 2709              	beq	L1371
5065                         ; 3976       DescSetStateSession((curRefState+1)->stateSession);
5067  0559 e643              	ldab	3,y
5068  055b c43f              	andb	#63
5069  055d 87                	clra	
5070  055e 4a047c7c          	call	f_DescSetStateSession
5072  0562                   L1371:
5073                         ; 3978     if((g_descCurState.stateSecurityAccess & curRefState->stateSecurityAccess) != 0)
5075  0562 f60030            	ldab	L122_g_descCurState
5076  0565 c407              	andb	#7
5077  0567 6b80              	stab	OFST-3,s
5078  0569 ed81              	ldy	OFST-2,s
5079  056b e640              	ldab	0,y
5080  056d c407              	andb	#7
5081  056f e480              	andb	OFST-3,s
5082  0571 2709              	beq	L3371
5083                         ; 3980       DescSetStateSecurityAccess((curRefState+1)->stateSecurityAccess);
5085  0573 e642              	ldab	2,y
5086  0575 c407              	andb	#7
5087  0577 87                	clra	
5088  0578 4a04c1c1          	call	f_DescSetStateSecurityAccess
5090  057c                   L3371:
5091                         ; 3982     DescInterruptRestore();
5093  057c 4a000000          	call	f_VStdResumeAllInterrupts
5095  0580                   L7271:
5096                         ; 3984 }
5099  0580 1b85              	leas	5,s
5100  0582 0a                	rtc	
5143                         ; 3997 static void DescOnTransitionStateSession(DescStateGroup newState, DescStateGroup currentState)
5143                         ; 3998 {
5144                         	switch	.ftext
5145  0583                   L75f_DescOnTransitionStateSession:
5147  0583 3b                	pshd	
5148       00000000          OFST:	set	0
5151                         ; 4011   if(currentState == kDescStateSessionDefault)
5154  0584 ec85              	ldd	OFST+5,s
5155  0586 04240d            	dbne	d,L3571
5156                         ; 4013     if(newState != kDescStateSessionDefault)
5158  0589 ec80              	ldd	OFST+0,s
5159  058b 040415            	dbeq	d,L7571
5160                         ; 4015       DescActivateS1Timer();
5162  058e cc01f4            	ldd	#500
5163  0591 7c0035            	std	L312_g_descS1Timer
5165  0594 200d              	bra	L7571
5166  0596                   L3571:
5167                         ; 4024     if(newState == kDescStateSessionDefault)
5169  0596 ec80              	ldd	OFST+0,s
5170  0598 042408            	dbne	d,L7571
5171                         ; 4026       DescDeactivateS1Timer();
5173  059b 18790035          	clrw	L312_g_descS1Timer
5174                         ; 4035       DescOemOnTransitionToDefaultSession();
5176  059f 4a0d4949          	call	f_DescEnableCommunication
5178  05a3                   L7571:
5179                         ; 4040   g_descCurrSessionNumber = (vuint8)DescGetSessionStateBitPosition(newState);
5181  05a3 ec80              	ldd	OFST+0,s
5182  05a5 4a063636          	call	L55f_DescGetSessionStateBitPosition
5184  05a9 7b0034            	stab	L512_g_descCurrSessionNumber
5185                         ; 4042 }
5188  05ac 31                	puly	
5189  05ad 0a                	rtc	
5265                         ; 4055 DescStateGroup DESC_API_CALL_TYPE DescGetSessionStateOfSessionId(DescMsgItem sessionId)
5265                         ; 4056 {
5266                         	switch	.ftext
5267  05ae                   f_DescGetSessionStateOfSessionId:
5269  05ae 3b                	pshd	
5270  05af 1b98              	leas	-8,s
5271       00000008          OFST:	set	8
5274                         ; 4062   step = DescGetSvcInstHeadExtEntrySize(&g_descSvcHead[kDescSvcHeadOffsetSDS]);
5276  05b1 cc008d            	ldd	#L352_g_descSvcHead
5277  05b4 4a06fdfd          	call	L57f_DescGetSvcInstHeadExtEntrySize
5279  05b8 6c84              	std	OFST-4,s
5280                         ; 4065   pReqHeadExt = DescGetSvcInstReqHeadExt(&g_descSvcHead[kDescSvcHeadOffsetSDS], kDescSvcInstOffsetSDS);
5282  05ba 87                	clra	
5283  05bb c7                	clrb	
5284  05bc 3b                	pshd	
5285  05bd cc008d            	ldd	#L352_g_descSvcHead
5286  05c0 4a074343          	call	L77f_DescGetSvcInstReqHeadExt
5288  05c4 1b82              	leas	2,s
5289  05c6 6c86              	std	OFST-2,s
5290                         ; 4067   iter = 0;
5292  05c8 87                	clra	
5293  05c9 c7                	clrb	
5294  05ca 6c80              	std	OFST-8,s
5295  05cc 6c82              	std	OFST-6,s
5296                         ; 4068   scanner = 0;
5299  05ce ed86              	ldy	OFST-2,s
5300  05d0 2029              	bra	L7102
5301  05d2                   L3102:
5302                         ; 4071     if(pReqHeadExt[scanner] == sessionId)
5304  05d2 ec82              	ldd	OFST-6,s
5305  05d4 e6ee              	ldab	d,y
5306  05d6 e189              	cmpb	OFST+1,s
5307  05d8 2616              	bne	L3202
5308                         ; 4073       break;
5309  05da ec80              	ldd	OFST-8,s
5310  05dc                   L1202:
5311                         ; 4080   DescAssertCommon((iter < kDescNumStateSession), kDescAssertInvalidStateParameterValue);
5313  05dc 8c0006            	cpd	#6
5314  05df 2521              	blo	L5202
5317  05e1 cc0ff0            	ldd	#4080
5318  05e4 3b                	pshd	
5319  05e5 cc002a            	ldd	#42
5320  05e8 4a000000          	call	f_ApplDescFatalError
5322  05ec 1b82              	leas	2,s
5323  05ee 2012              	bra	L5202
5324  05f0                   L3202:
5325                         ; 4075     scanner+=step;
5327  05f0 ec82              	ldd	OFST-6,s
5328  05f2 e384              	addd	OFST-4,s
5329  05f4 6c82              	std	OFST-6,s
5330                         ; 4076     iter++;
5332  05f6 186280            	incw	OFST-8,s
5333  05f9 ec80              	ldd	OFST-8,s
5334  05fb                   L7102:
5335                         ; 4069   while(iter < kDescNumStateSession)
5337  05fb 8c0006            	cpd	#6
5338  05fe 25d2              	blo	L3102
5339  0600 20da              	bra	L1202
5340  0602                   L5202:
5341                         ; 4083   if(g_descSvcInst[iter + kDescSvcInstOffsetSDS].setStateIndex != kDescStateNoTransition)
5344  0602 ec80              	ldd	OFST-8,s
5345  0604 cd000b            	ldy	#11
5346  0607 13                	emul	
5347  0608 b746              	tfr	d,y
5348  060a e6ea012a          	ldab	L752_g_descSvcInst+5,y
5349  060e c105              	cmpb	#5
5350  0610 2711              	beq	L7202
5351                         ; 4085     returnValue = g_descStateGroupTransition[g_descSvcInst[iter + kDescSvcInstOffsetSDS].setStateIndex][1].stateSession;
5353  0612 87                	clra	
5354  0613 b745              	tfr	d,x
5355  0615 1848              	lslx	
5356  0617 1848              	lslx	
5357  0619 e6e20036          	ldab	L742_g_descStateGroupTransition+3,x
5358  061d c43f              	andb	#63
5359  061f 6c84              	std	OFST-4,s
5361  0621 2010              	bra	L1302
5362  0623                   L7202:
5363                         ; 4090     DescAssertCommonAlways(kDescAssertInvalidStateParameterValue);
5365  0623 cc0ffa            	ldd	#4090
5366  0626 3b                	pshd	
5367  0627 cc002a            	ldd	#42
5368  062a 4a000000          	call	f_ApplDescFatalError
5370  062e 1b82              	leas	2,s
5371                         ; 4092     returnValue = kDescStateValueInvalid;
5373  0630 ccffff            	ldd	#-1
5374  0633                   L1302:
5375                         ; 4094   return returnValue;
5379  0633 1b8a              	leas	10,s
5380  0635 0a                	rtc	
5420                         ; 4108 static vuint8_least DescGetSessionStateBitPosition(DescStateGroup sessionState)
5420                         ; 4109 {
5421                         	switch	.ftext
5422  0636                   L55f_DescGetSessionStateBitPosition:
5424  0636 3b                	pshd	
5425  0637 3b                	pshd	
5426       00000002          OFST:	set	2
5429                         ; 4110   vuint8_least stateNumber = 1;
5431  0638 cc0001            	ldd	#1
5432  063b 6c80              	std	OFST-2,s
5433                         ; 4113   DescAssertCommon((sessionState != 0), kDescAssertZeroStateValue);
5435  063d ec82              	ldd	OFST+0,s
5436  063f 2612              	bne	L5502
5439  0641 cc1011            	ldd	#4113
5440  0644 3b                	pshd	
5441  0645 cc0013            	ldd	#19
5442  0648 4a000000          	call	f_ApplDescFatalError
5444  064c 1b82              	leas	2,s
5445  064e 2003              	bra	L5502
5446  0650                   L3502:
5447                         ; 4116     stateNumber++;
5449  0650 186280            	incw	OFST-2,s
5450  0653                   L5502:
5451                         ; 4114   while((sessionState>>stateNumber)!= 0)
5453  0653 ec82              	ldd	OFST+0,s
5454  0655 ed80              	ldy	OFST-2,s
5455  0657 2704              	beq	L071
5456  0659                   L271:
5457  0659 49                	lsrd	
5458  065a 0436fc            	dbne	y,L271
5459  065d                   L071:
5460  065d 0474f0            	tbne	d,L3502
5461                         ; 4119   stateNumber--;
5463  0660 186380            	decw	OFST-2,s
5464                         ; 4122   DescAssertCommon((stateNumber < kDescNumStateSession), kDescAssertInvalidStateParameterValue);
5466  0663 ec80              	ldd	OFST-2,s
5467  0665 8c0006            	cpd	#6
5468  0668 250f              	blo	L1602
5471  066a cc101a            	ldd	#4122
5472  066d 3b                	pshd	
5473  066e cc002a            	ldd	#42
5474  0671 4a000000          	call	f_ApplDescFatalError
5476  0675 1b82              	leas	2,s
5477  0677 ec80              	ldd	OFST-2,s
5478  0679                   L1602:
5479                         ; 4123   return stateNumber;
5484  0679 1b84              	leas	4,s
5485  067b 0a                	rtc	
5529                         ; 4137 DescMsgItem DESC_API_CALL_TYPE DescGetSessionIdOfSessionState(DescStateGroup sessionState)
5529                         ; 4138 {
5530                         	switch	.ftext
5531  067c                   f_DescGetSessionIdOfSessionState:
5533  067c 3b                	pshd	
5534  067d 3b                	pshd	
5535       00000002          OFST:	set	2
5538                         ; 4141   iter = kDescSvcInstOffsetSDS;
5540  067e 87                	clra	
5541  067f c7                	clrb	
5542  0680 6c80              	std	OFST-2,s
5543  0682                   L1012:
5544                         ; 4144     if(g_descSvcInst[iter].setStateIndex != kDescStateNoTransition)
5546  0682 cd000b            	ldy	#11
5547  0685 13                	emul	
5548  0686 b746              	tfr	d,y
5549  0688 e6ea012a          	ldab	L752_g_descSvcInst+5,y
5550  068c c105              	cmpb	#5
5551  068e 2723              	beq	L7012
5552                         ; 4146       if((DescStateGroup) (g_descStateGroupTransition[g_descSvcInst[iter].setStateIndex][1].stateSession) == sessionState)
5554  0690 87                	clra	
5555  0691 b745              	tfr	d,x
5556  0693 1848              	lslx	
5557  0695 1848              	lslx	
5558  0697 e6e20036          	ldab	L742_g_descStateGroupTransition+3,x
5559  069b c43f              	andb	#63
5560  069d ac82              	cpd	OFST+0,s
5561  069f 2612              	bne	L7012
5562                         ; 4148         return *DescGetSvcInstReqHeadExt(&g_descSvcHead[kDescSvcHeadOffsetSDS], iter);
5564  06a1 ec80              	ldd	OFST-2,s
5565  06a3 3b                	pshd	
5566  06a4 cc008d            	ldd	#L352_g_descSvcHead
5567  06a7 4a074343          	call	L77f_DescGetSvcInstReqHeadExt
5569  06ab 1b82              	leas	2,s
5570  06ad b746              	tfr	d,y
5571  06af e640              	ldab	0,y
5573  06b1 2019              	bra	L671
5574  06b3                   L7012:
5575                         ; 4151     iter++;
5577  06b3 186280            	incw	OFST-2,s
5578                         ; 4142   while (iter < (kDescSvcInstOffsetSDS + kDescNumStateSession))
5580  06b6 ec80              	ldd	OFST-2,s
5581  06b8 8c0006            	cpd	#6
5582  06bb 25c5              	blo	L1012
5583                         ; 4155   DescAssertCommonAlways(kDescAssertInvalidStateParameterValue);
5585  06bd cc103b            	ldd	#4155
5586  06c0 3b                	pshd	
5587  06c1 cc002a            	ldd	#42
5588  06c4 4a000000          	call	f_ApplDescFatalError
5590  06c8 1b82              	leas	2,s
5591                         ; 4157   return (DescMsgItem)0xFF;
5593  06ca c6ff              	ldab	#255
5595  06cc                   L671:
5597  06cc 1b84              	leas	4,s
5598  06ce 0a                	rtc	
5659                         ; 4171 void DESC_API_CALL_TYPE DescGetSessionTimings(DescStateGroup sessionState, vuint16* p2Time_1ms, vuint16* p2ExTime_10ms)
5659                         ; 4172 {
5660                         	switch	.ftext
5661  06cf                   f_DescGetSessionTimings:
5663  06cf 3b                	pshd	
5664  06d0 3b                	pshd	
5665       00000002          OFST:	set	2
5668                         ; 4178   bitPos = DescGetSessionStateBitPosition(sessionState);
5670  06d1 4a063636          	call	L55f_DescGetSessionStateBitPosition
5672  06d5 6c80              	std	OFST-2,s
5673                         ; 4180   *p2Time_1ms = g_descP2TimingsTable[bitPos][kDescTimingRefP2];
5675  06d7 ed87              	ldy	OFST+5,s
5676  06d9 59                	lsld	
5677  06da 59                	lsld	
5678  06db b745              	tfr	d,x
5679  06dd 1802e2001b40      	movw	L542_g_descP2TimingsTable,x,0,y
5680                         ; 4182   *p2ExTime_10ms = g_descP2TimingsTable[bitPos][kDescTimingRefP2Star];
5682  06e3 ed89              	ldy	OFST+7,s
5683  06e5 ee80              	ldx	OFST-2,s
5684  06e7 1848              	lslx	
5685  06e9 1848              	lslx	
5686  06eb 1802e2001d40      	movw	L542_g_descP2TimingsTable+2,x,0,y
5687                         ; 4190 }
5690  06f1 1b84              	leas	4,s
5691  06f3 0a                	rtc	
5723                         ; 4219 void DESC_API_CALL_TYPE DescSetNegResponse(DESC_CONTEXT_FORMAL_PARAM_DEF_FIRST DescNegResCode errorCode)
5723                         ; 4220 {
5724                         	switch	.ftext
5725  06f4                   f_DescSetNegResponse:
5729                         ; 4223   if (g_descNegResCode[DESC_CONTEXT_PARAM_VALUE] == kDescNrcNone)
5732  06f4 f7001e            	tst	L132_g_descNegResCode
5733  06f7 2603              	bne	L5512
5734                         ; 4225     g_descNegResCode[DESC_CONTEXT_PARAM_VALUE] = errorCode;
5736  06f9 7b001e            	stab	L132_g_descNegResCode
5737  06fc                   L5512:
5738                         ; 4227 }
5741  06fc 0a                	rtc	
5917                         ; 4239 static vuint8_least DescGetSvcInstHeadExtEntrySize(V_MEMROM1 DescSvcHead V_MEMROM2 V_MEMROM3 * pSvcHead)
5917                         ; 4240 {
5918                         	switch	.ftext
5919  06fd                   L57f_DescGetSvcInstHeadExtEntrySize:
5921  06fd 3b                	pshd	
5922  06fe 37                	pshb	
5923       00000001          OFST:	set	1
5926                         ; 4241   return (vuint8_least)((pSvcHead->isReqHeadExtEchoed != 0)?
5926                         ; 4242                   (pSvcHead->reqHeadExLen):(pSvcHead->reqHeadExLen + pSvcHead->resHeadExLen));
5928  06ff b746              	tfr	d,y
5929  0701 0f422007          	brclr	2,y,32,L602
5930  0705 e641              	ldab	1,y
5931  0707 c40f              	andb	#15
5932  0709 87                	clra	
5933  070a 2012              	bra	L012
5934  070c                   L602:
5935  070c b746              	tfr	d,y
5936  070e e641              	ldab	1,y
5937  0710 c40f              	andb	#15
5938  0712 6b80              	stab	OFST-1,s
5939  0714 e641              	ldab	1,y
5940  0716 54                	lsrb	
5941  0717 54                	lsrb	
5942  0718 54                	lsrb	
5943  0719 54                	lsrb	
5944  071a 87                	clra	
5945  071b eb80              	addb	OFST-1,s
5946  071d 45                	rola	
5947  071e                   L012:
5950  071e 1b83              	leas	3,s
5951  0720 0a                	rtc	
6004                         ; 4259 static V_MEMROM1 DescMsgItem V_MEMROM2 V_MEMROM3 * DescGetSvcInstResHeadExt(V_MEMROM1 DescSvcHead V_MEMROM2 V_MEMROM3 * pSvcHead, DescSvcInstIndex svcInstAbsRef)
6004                         ; 4260 # endif
6004                         ; 4261 {
6005                         	switch	.ftext
6006  0721                   L101f_DescGetSvcInstResHeadExt:
6008  0721 3b                	pshd	
6009  0722 3b                	pshd	
6010       00000002          OFST:	set	2
6013                         ; 4262   vuint8_least offset = 0;
6015  0723 186980            	clrw	OFST-2,s
6016                         ; 4263   if(pSvcHead->isReqHeadExtEchoed == 0)
6018  0726 b746              	tfr	d,y
6019  0728 0e422007          	brset	2,y,32,L1132
6020                         ; 4265     offset = pSvcHead->reqHeadExLen;
6022  072c e641              	ldab	1,y
6023  072e c40f              	andb	#15
6024  0730 87                	clra	
6025  0731 6c80              	std	OFST-2,s
6026  0733                   L1132:
6027                         ; 4267   return (DescGetSvcInstReqHeadExt(pSvcHead, svcInstAbsRef) + offset);
6029  0733 ec87              	ldd	OFST+5,s
6030  0735 3b                	pshd	
6031  0736 ec84              	ldd	OFST+2,s
6032  0738 4a074343          	call	L77f_DescGetSvcInstReqHeadExt
6034  073c 1b82              	leas	2,s
6035  073e e380              	addd	OFST-2,s
6038  0740 1b84              	leas	4,s
6039  0742 0a                	rtc	
6093                         ; 4284 static V_MEMROM1 DescMsgItem V_MEMROM2 V_MEMROM3 * DescGetSvcInstReqHeadExt(V_MEMROM1 DescSvcHead V_MEMROM2 V_MEMROM3 * pSvcHead, DescSvcInstIndex svcInstAbsRef)
6093                         ; 4285 # endif
6093                         ; 4286 {
6094                         	switch	.ftext
6095  0743                   L77f_DescGetSvcInstReqHeadExt:
6097  0743 3b                	pshd	
6098  0744 3b                	pshd	
6099       00000002          OFST:	set	2
6102                         ; 4287   vuint16_least offset = DescGetSvcInstHeadExtEntrySize(pSvcHead);
6104  0745 4a06fdfd          	call	L57f_DescGetSvcInstHeadExtEntrySize
6106  0749 6c80              	std	OFST-2,s
6107                         ; 4288   offset *= (vuint16)(svcInstAbsRef - pSvcHead->svcInstFirstItem);
6109  074b ec87              	ldd	OFST+5,s
6110  074d ed82              	ldy	OFST+0,s
6111  074f e046              	subb	6,y
6112  0751 8200              	sbca	#0
6113  0753 ed80              	ldy	OFST-2,s
6114  0755 13                	emul	
6115  0756 6c80              	std	OFST-2,s
6116                         ; 4290   return &g_descSvcInstHeadExt[pSvcHead->svcInstHeadExtFirstItem + offset];
6118  0758 ed82              	ldy	OFST+0,s
6119  075a e647              	ldab	7,y
6120  075c 87                	clra	
6121  075d e380              	addd	OFST-2,s
6122  075f c30571            	addd	#L162_g_descSvcInstHeadExt
6125  0762 1b84              	leas	4,s
6126  0764 0a                	rtc	
6226                         ; 4392 static DescSvcInstIndex DescFindSvcInst(DescConstPtr reqHeadPtr, V_MEMROM1 DescSvcHead V_MEMROM2 V_MEMROM3 * pSvcHead, vuint8_least* failedByteMask)
6226                         ; 4393 {
6227                         	switch	.ftext
6228  0765                   L76f_DescFindSvcInst:
6230  0765 3b                	pshd	
6231  0766 1b92              	leas	-14,s
6232       0000000e          OFST:	set	14
6235                         ; 4402   incStep = DescGetSvcInstHeadExtEntrySize(pSvcHead);
6237  0768 ecf013            	ldd	OFST+5,s
6238  076b 4a06fdfd          	call	L57f_DescGetSvcInstHeadExtEntrySize
6240  076f 6c8c              	std	OFST-2,s
6241                         ; 4405   reqSvcInstHandle = kDescInvalidSvcInstHandle;
6243  0771 cc0064            	ldd	#100
6244  0774 6c8a              	std	OFST-4,s
6245                         ; 4407   *failedByteMask = 0;
6247  0776 18c7              	clry	
6248  0778 6df30015          	sty	[OFST+7,s]
6249  077c 6d88              	sty	OFST-6,s
6250                         ; 4408   offset = 0;
6252                         ; 4410   for(iter = pSvcHead->svcInstHeadExtFirstItem;
6254  077e edf013            	ldy	OFST+5,s
6255  0781 e647              	ldab	7,y
6257  0783 207c              	bra	L7042
6258  0785                   L3042:
6259                         ; 4414     currColFound = 0;
6261  0785 18c7              	clry	
6262  0787 6d82              	sty	OFST-12,s
6263  0789                   L3142:
6264                         ; 4417       isEqual = V_BOOL_EXPR(reqHeadPtr[currColFound] == g_descSvcInstHeadExt[iter + currColFound]);
6266  0789 ec8e              	ldd	OFST+0,s
6267  078b e6ee              	ldab	d,y
6268  078d ed84              	ldy	OFST-10,s
6269  078f 18eb82            	addy	OFST-12,s
6270  0792 e1ea0571          	cmpb	L162_g_descSvcInstHeadExt,y
6271  0796 2605              	bne	L022
6272  0798 cc0001            	ldd	#1
6273  079b 2002              	bra	L222
6274  079d                   L022:
6275  079d 87                	clra	
6276  079e c7                	clrb	
6277  079f                   L222:
6278  079f 6c86              	std	OFST-8,s
6279                         ; 4418       currColFound++;
6281  07a1 186282            	incw	OFST-12,s
6282                         ; 4420     while ((currColFound < pSvcHead->reqHeadExLen)&&(isEqual != kDescFalse));
6284  07a4 edf013            	ldy	OFST+5,s
6285  07a7 e641              	ldab	1,y
6286  07a9 c40f              	andb	#15
6287  07ab 87                	clra	
6288  07ac 6c80              	std	OFST-14,s
6289  07ae ed82              	ldy	OFST-12,s
6290  07b0 ad80              	cpy	OFST-14,s
6291  07b2 2404              	bhs	L1242
6293  07b4 ec86              	ldd	OFST-8,s
6294  07b6 26d1              	bne	L3142
6295  07b8                   L1242:
6296                         ; 4423     if(isEqual != kDescFalse)
6298  07b8 ec86              	ldd	OFST-8,s
6299  07ba 270c              	beq	L3242
6300                         ; 4425       reqSvcInstHandle = (DescSvcInstIndex)(offset + pSvcHead->svcInstFirstItem);
6302  07bc edf013            	ldy	OFST+5,s
6303  07bf e646              	ldab	6,y
6304  07c1 87                	clra	
6305  07c2 e388              	addd	OFST-6,s
6306  07c4 6c8a              	std	OFST-4,s
6307                         ; 4426       break;
6309  07c6 201b              	bra	L1142
6310  07c8                   L3242:
6311                         ; 4431       currColFound--;
6313  07c8 03                	dey	
6314  07c9 6d82              	sty	OFST-12,s
6315                         ; 4433       if(currColFound > *failedByteMask)
6317  07cb eef015            	ldx	OFST+7,s
6318  07ce ad00              	cpy	0,x
6319  07d0 2302              	bls	L7242
6320                         ; 4436         *failedByteMask = currColFound;
6322  07d2 6d00              	sty	0,x
6323  07d4                   L7242:
6324                         ; 4439       if(reqHeadPtr[currColFound] < g_descSvcInstHeadExt[iter + currColFound])
6326  07d4 ec8e              	ldd	OFST+0,s
6327  07d6 e6ee              	ldab	d,y
6328  07d8 ed84              	ldy	OFST-10,s
6329  07da 18eb82            	addy	OFST-12,s
6330  07dd e1ea0571          	cmpb	L162_g_descSvcInstHeadExt,y
6331  07e1 2417              	bhs	L5242
6332                         ; 4442         break;
6333  07e3                   L1142:
6334                         ; 4450   *failedByteMask = (vuint8)(0x02 << *failedByteMask);
6336  07e3 cc0002            	ldd	#2
6337  07e6 eef015            	ldx	OFST+7,s
6338  07e9 ed00              	ldy	0,x
6339  07eb 2704              	beq	L422
6340  07ed                   L622:
6341  07ed 59                	lsld	
6342  07ee 0436fc            	dbne	y,L622
6343  07f1                   L422:
6344  07f1 87                	clra	
6345  07f2 6c00              	std	0,x
6346                         ; 4451   return reqSvcInstHandle;
6348  07f4 ec8a              	ldd	OFST-4,s
6351  07f6 1bf010            	leas	16,s
6352  07f9 0a                	rtc	
6353  07fa                   L5242:
6354                         ; 4446     offset++;
6356  07fa 186288            	incw	OFST-6,s
6357                         ; 4411       iter < (pSvcHead + 1)->svcInstHeadExtFirstItem;
6357                         ; 4412       iter+= incStep)
6359  07fd ec84              	ldd	OFST-10,s
6360  07ff e38c              	addd	OFST-2,s
6361  0801                   L7042:
6362  0801 6c84              	std	OFST-10,s
6363                         ; 4410   for(iter = pSvcHead->svcInstHeadExtFirstItem;
6363                         ; 4411       iter < (pSvcHead + 1)->svcInstHeadExtFirstItem;
6365  0803 edf013            	ldy	OFST+5,s
6366  0806 e64f              	ldab	15,y
6367  0808 87                	clra	
6368  0809 ac84              	cpd	OFST-10,s
6369  080b 1822ff76          	bhi	L3042
6370  080f 20d2              	bra	L1142
6416                         ; 4465 static DescSvcHeadIndex DescFindSvc(DescMsgItem reqSvcId)
6416                         ; 4466 {
6417                         	switch	.ftext
6418  0811                   L17f_DescFindSvc:
6420  0811 3b                	pshd	
6421  0812 1b9d              	leas	-3,s
6422       00000003          OFST:	set	3
6425                         ; 4467   DescSvcHeadIndex result  = kDescInvalidSvcHandle;
6427  0814 c610              	ldab	#16
6428  0816 6b80              	stab	OFST-3,s
6429                         ; 4468   vuint8_least     byteVar = 0;
6431  0818 186981            	clrw	OFST-2,s
6432                         ; 4472   if (((reqSvcId & kDescPosResIdOffset) == 0)&&
6432                         ; 4473        (reqSvcId <= kDescMaxReqSid))
6434  081b 0e84401c          	brset	OFST+1,s,64,L3542
6436  081f e684              	ldab	OFST+1,s
6437  0821 c185              	cmpb	#133
6438  0823 2216              	bhi	L3542
6439                         ; 4476     if ((reqSvcId & 0x80) != 0)
6441  0825 c580              	bitb	#128
6442  0827 2707              	beq	L5542
6443                         ; 4478       byteVar = kDescPosResIdOffset;
6445  0829 cc0040            	ldd	#64
6446  082c 6c81              	std	OFST-2,s
6447  082e e684              	ldab	OFST+1,s
6448  0830                   L5542:
6449                         ; 4480     result = g_descSidMap[reqSvcId - byteVar];
6451  0830 b796              	exg	b,y
6452  0832 18e081            	suby	OFST-2,s
6453  0835 180aea004780      	movb	L152_g_descSidMap,y,OFST-3,s
6454  083b                   L3542:
6455                         ; 4493   return result;
6457  083b e680              	ldab	OFST-3,s
6460  083d 1b85              	leas	5,s
6461  083f 0a                	rtc	
6484                         ; 4536 static void DescDispatcherIterInit(DESC_CONTEXT_FORMAL_PARAM_DEF_ONLY)
6484                         ; 4537 {
6485                         	switch	.ftext
6486  0840                   L56f_DescDispatcherIterInit:
6490                         ; 4550 }
6493  0840 0a                	rtc	
6675                         ; 4561 static void DescDispatcher(DESC_CONTEXT_FORMAL_PARAM_DEF_ONLY)
6675                         ; 4562 {
6676                         	switch	.ftext
6677  0841                   L37f_DescDispatcher:
6679  0841 1b96              	leas	-10,s
6680       0000000a          OFST:	set	10
6683                         ; 4566   msg = g_descInterruptContextCtrl[DESC_CONTEXT_PARAM_VALUE].infoPoolPtr->reqDataPtr;
6685  0843 fd003a            	ldy	L502_g_descInterruptContextCtrl
6686  0846 18024c88          	movw	12,y,OFST-2,s
6687                         ; 4575   g_descContextCtrl[DESC_CONTEXT_PARAM_VALUE].isApplError = 0;
6689  084a 1d003f08          	bclr	L302_g_descContextCtrl+1,8
6690                         ; 4577   g_descMsgContext[DESC_CONTEXT_PARAM_VALUE].msgAddInfo.resOnReq = kDescDefaultResOnReq;
6692  084e 1d00270c          	bclr	L722_g_descMsgContext+8,12
6693  0852 1c002704          	bset	L722_g_descMsgContext+8,4
6694                         ; 4579   g_descInterruptContextCtrl[DESC_CONTEXT_PARAM_VALUE].activity = kDescContextActiveProcess;
6696  0856 c604              	ldab	#4
6697  0858 7b003c            	stab	L502_g_descInterruptContextCtrl+2
6698                         ; 4586   g_descCurReqSvc[DESC_CONTEXT_PARAM_VALUE] = DescFindSvc(msg[0]);
6700  085b e6f30008          	ldab	[OFST-2,s]
6701  085f 87                	clra	
6702  0860 4a081111          	call	L17f_DescFindSvc
6704  0864 7b002f            	stab	L322_g_descCurReqSvc
6705                         ; 4587   if(g_descCurReqSvc[DESC_CONTEXT_PARAM_VALUE] < kDescInvalidSvcHandle)
6707  0867 c110              	cmpb	#16
6708  0869 18240142          	bhs	L3752
6709                         ; 4591     refDescSvcHead = &g_descSvcHead[g_descCurReqSvc[DESC_CONTEXT_PARAM_VALUE]];
6711  086d 8608              	ldaa	#8
6712  086f 12                	mul	
6713  0870 c3008d            	addd	#L352_g_descSvcHead
6714                         ; 4596     g_descMsgContext[DESC_CONTEXT_PARAM_VALUE].msgAddInfo.resOnReq = refDescSvcHead->resOnReq;
6716  0873 b746              	tfr	d,y
6717  0875 6d86              	sty	OFST-4,s
6718  0877 e642              	ldab	2,y
6719  0879 1d00270c          	bclr	L722_g_descMsgContext+8,12
6720  087d c40c              	andb	#12
6721  087f fa0027            	orab	L722_g_descMsgContext+8
6722  0882 7b0027            	stab	L722_g_descMsgContext+8
6723                         ; 4609       if ((refDescSvcHead->checkSessionState & g_descCurState.stateSession) != 0)
6725  0885 f60031            	ldab	L122_g_descCurState+1
6726  0888 c43f              	andb	#63
6727  088a 6b81              	stab	OFST-9,s
6728  088c e643              	ldab	3,y
6729  088e c43f              	andb	#63
6730  0890 e481              	andb	OFST-9,s
6731  0892 18270115          	beq	L5752
6732                         ; 4617         if(g_descMsgContext[DESC_CONTEXT_PARAM_VALUE].reqDataLen >= refDescSvcHead->minReqLength)
6734  0896 e645              	ldab	5,y
6735  0898 87                	clra	
6736  0899 bc0021            	cpd	L722_g_descMsgContext+2
6737  089c 182200e9          	bhi	L3162
6738                         ; 4626           vuint8_least svcInstFailedBytePosMask = 0;
6740  08a0 186984            	clrw	OFST-6,s
6741                         ; 4630           msg++;
6743  08a3 186288            	incw	OFST-2,s
6744                         ; 4634           supPosResBit = refDescSvcHead->suppPosRes;
6746  08a6 e642              	ldab	2,y
6747  08a8 c410              	andb	#16
6748  08aa 54                	lsrb	
6749  08ab 54                	lsrb	
6750  08ac 54                	lsrb	
6751  08ad 54                	lsrb	
6752                         ; 4636           supPosResBit <<= 7;
6754  08ae 49                	lsrd	
6755  08af b710              	tfr	b,a
6756  08b1 56                	rorb	
6757  08b2 c480              	andb	#128
6758  08b4 6c82              	std	OFST-8,s
6759                         ; 4638           g_descMsgContext[DESC_CONTEXT_PARAM_VALUE].msgAddInfo.suppPosRes = CANBITTYPE_CAST (((supPosResBit & *msg)!= 0)?0x01:0x00);
6761  08b6 e6f30008          	ldab	[OFST-2,s]
6762  08ba e483              	andb	OFST-7,s
6763  08bc 2706              	beq	L632
6764  08be 1c002710          	bset	L722_g_descMsgContext+8,16
6765  08c2 2004              	bra	L042
6766  08c4                   L632:
6767  08c4 1d002710          	bclr	L722_g_descMsgContext+8,16
6768  08c8                   L042:
6769                         ; 4640           *msg &= (DescMsgItem)(~supPosResBit);/* compose 0x80 or 0x00 depending on the CDD info */
6771  08c8 e683              	ldab	OFST-7,s
6772  08ca 51                	comb	
6773  08cb e4f30008          	andb	[OFST-2,s]
6774  08cf 6bf30008          	stab	[OFST-2,s]
6775                         ; 4647           if(refDescSvcHead->reqHeadExLen > 0)
6777  08d3 0f410f10          	brclr	1,y,15,L1062
6778                         ; 4649             g_descCurReqSvcInst[DESC_CONTEXT_PARAM_VALUE] = (DescMemSvcInstIndex)DescFindSvcInst(msg, refDescSvcHead, &svcInstFailedBytePosMask);
6780  08d7 1a84              	leax	OFST-6,s
6781  08d9 34                	pshx	
6782  08da ec88              	ldd	OFST-2,s
6783  08dc 3b                	pshd	
6784  08dd ec8c              	ldd	OFST+2,s
6785  08df 4a076565          	call	L76f_DescFindSvcInst
6787  08e3 1b84              	leas	4,s
6789  08e5 2002              	bra	L3062
6790  08e7                   L1062:
6791                         ; 4654             g_descCurReqSvcInst[DESC_CONTEXT_PARAM_VALUE] = refDescSvcHead->svcInstFirstItem;
6793  08e7 e646              	ldab	6,y
6794  08e9                   L3062:
6795  08e9 7b002e            	stab	L522_g_descCurReqSvcInst
6796                         ; 4657           if((g_descCurReqSvcInst[DESC_CONTEXT_PARAM_VALUE]) < kDescInvalidSvcInstHandle)
6798  08ec c164              	cmpb	#100
6799  08ee 182400ad          	bhs	L5062
6800                         ; 4662             refDescSvcInst = &g_descSvcInst[g_descCurReqSvcInst[DESC_CONTEXT_PARAM_VALUE]];
6802  08f2 860b              	ldaa	#11
6803  08f4 12                	mul	
6804  08f5 c30125            	addd	#L752_g_descSvcInst
6805                         ; 4668             g_descMsgContext[DESC_CONTEXT_PARAM_VALUE].msgAddInfo.resOnReq = refDescSvcInst->msgAddInfo.resOnReq;
6807  08f8 b746              	tfr	d,y
6808  08fa 6d82              	sty	OFST-8,s
6809  08fc e642              	ldab	2,y
6810  08fe 1d00270c          	bclr	L722_g_descMsgContext+8,12
6811  0902 c40c              	andb	#12
6812  0904 fa0027            	orab	L722_g_descMsgContext+8
6813  0907 7b0027            	stab	L722_g_descMsgContext+8
6814                         ; 4673             if ((refDescSvcInst->msgAddInfo.reqType &
6814                         ; 4674                  g_descMsgContext[DESC_CONTEXT_PARAM_VALUE].msgAddInfo.reqType) != 0)
6816  090a c403              	andb	#3
6817  090c 6b81              	stab	OFST-9,s
6818  090e e642              	ldab	2,y
6819  0910 c403              	andb	#3
6820  0912 e481              	andb	OFST-9,s
6821  0914 18270083          	beq	L7062
6822                         ; 4686               if ((refDescSvcInst->checkState.stateSession & g_descCurState.stateSession) != 0)
6824  0918 f60031            	ldab	L122_g_descCurState+1
6825  091b c43f              	andb	#63
6826  091d 6b81              	stab	OFST-9,s
6827  091f e644              	ldab	4,y
6828  0921 c43f              	andb	#63
6829  0923 e481              	andb	OFST-9,s
6830  0925 2766              	beq	L1162
6831                         ; 4692                 if((refDescSvcInst->reqLen == 0) ||
6831                         ; 4693                   (refDescSvcInst->reqLen == g_descMsgContext[DESC_CONTEXT_PARAM_VALUE].reqDataLen))
6833  0927 ec40              	ldd	0,y
6834  0929 2705              	beq	L5162
6836  092b bc0021            	cpd	L722_g_descMsgContext+2
6837  092e 2659              	bne	L3162
6838  0930                   L5162:
6839                         ; 4697                   g_descNegResCode[DESC_CONTEXT_PARAM_VALUE] = DescCheckState(&(refDescSvcInst->checkState));
6841  0930 b764              	tfr	y,d
6842  0932 c30003            	addd	#3
6843  0935 4a04f1f1          	call	L762f_DescCheckState
6845  0939 7b001e            	stab	L132_g_descNegResCode
6846                         ; 4700                   if(g_descNegResCode[DESC_CONTEXT_PARAM_VALUE] == kDescNrcNone)
6848  093c 263f              	bne	L7162
6849                         ; 4704                     g_descContextCtrl[DESC_CONTEXT_PARAM_VALUE].isApplError = 1;
6851  093e 1c003f08          	bset	L302_g_descContextCtrl+1,8
6852                         ; 4711                     if(g_descNegResCode[DESC_CONTEXT_PARAM_VALUE] == kDescNrcNone)
6855  0942 d7                	tstb	
6856  0943 266f              	bne	L3562
6857                         ; 4724                         g_descMsgContext[DESC_CONTEXT_PARAM_VALUE].reqData    = (DescMsg)(msg + DescExtractReqExtHeadLen(refDescSvcHead->reqHeadExLen));
6859  0945 ed86              	ldy	OFST-4,s
6860  0947 e641              	ldab	1,y
6861  0949 c40f              	andb	#15
6862  094b 87                	clra	
6863  094c e388              	addd	OFST-2,s
6864  094e 7c001f            	std	L722_g_descMsgContext
6865                         ; 4726                         g_descMsgContext[DESC_CONTEXT_PARAM_VALUE].resData    = (DescMsg)(msg + DescExtractResExtHeadLen(refDescSvcHead->resHeadExLen));
6867  0951 e641              	ldab	1,y
6868  0953 54                	lsrb	
6869  0954 54                	lsrb	
6870  0955 54                	lsrb	
6871  0956 54                	lsrb	
6872  0957 87                	clra	
6873  0958 e388              	addd	OFST-2,s
6874  095a 7c0023            	std	L722_g_descMsgContext+4
6875                         ; 4728                         g_descMsgContext[DESC_CONTEXT_PARAM_VALUE].reqDataLen -= (DescMsgLen)(DescExtractReqExtHeadLen(refDescSvcHead->reqHeadExLen) + 1);
6877  095d e641              	ldab	1,y
6878  095f c40f              	andb	#15
6879  0961 b795              	exg	b,x
6880  0963 08                	inx	
6881  0964 6e80              	stx	OFST-10,s
6882  0966 fc0021            	ldd	L722_g_descMsgContext+2
6883  0969 a380              	subd	OFST-10,s
6884  096b 7c0021            	std	L722_g_descMsgContext+2
6885                         ; 4730                         g_descMsgContext[DESC_CONTEXT_PARAM_VALUE].resDataLen = 0;
6887  096e 18790025          	clrw	L722_g_descMsgContext+6
6888                         ; 4739                         refDescSvcInst->mainHandler(&g_descMsgContext[DESC_CONTEXT_PARAM_VALUE]);
6890  0972 cc001f            	ldd	#L722_g_descMsgContext
6891  0975 ed82              	ldy	OFST-8,s
6892  0977 4beb0007          	call	[7,y]
6894                         ; 4744                         return;
6898  097b 203b              	bra	L242
6899  097d                   L7162:
6900                         ; 4752                     if((g_descNegResCode[DESC_CONTEXT_PARAM_VALUE] == kDescNrcSubfunctionNotSupportedInActiveSession)
6900                         ; 4753                       &&(refDescSvcHead->isSubFuncInstanced == 0))
6902  097d c17e              	cmpb	#126
6903  097f 2633              	bne	L3562
6905  0981 ed86              	ldy	OFST-4,s
6906  0983 0e42402d          	brset	2,y,64,L3562
6907                         ; 4755                       g_descNegResCode[DESC_CONTEXT_PARAM_VALUE] = kDescOemNrcParamIdNotSupportedInSession;
6909  0987 200a              	bra	LC003
6910  0989                   L3162:
6911                         ; 4766                   g_descNegResCode[DESC_CONTEXT_PARAM_VALUE] = kDescNrcInvalidFormat;
6913  0989 c613              	ldab	#19
6916  098b 2024              	bra	LC001
6917  098d                   L1162:
6918                         ; 4776                 if(refDescSvcHead->isSubFuncInstanced == 0)
6920  098d ed86              	ldy	OFST-4,s
6921  098f 0e424004          	brset	2,y,64,L3362
6922                         ; 4778                   g_descNegResCode[DESC_CONTEXT_PARAM_VALUE] = kDescOemNrcParamIdNotSupportedInSession;
6924  0993                   LC003:
6925  0993 c631              	ldab	#49
6927  0995 201a              	bra	LC001
6928  0997                   L3362:
6929                         ; 4782                   g_descNegResCode[DESC_CONTEXT_PARAM_VALUE] = kDescNrcSubfunctionNotSupportedInActiveSession;
6931  0997 c67e              	ldab	#126
6932  0999 2016              	bra	LC001
6933  099b                   L7062:
6934                         ; 4792               g_descNegResCode[DESC_CONTEXT_PARAM_VALUE] = kDescOemNrcInvalidAddrMethod;
6936  099b c622              	ldab	#34
6939  099d 2012              	bra	LC001
6940  099f                   L5062:
6941                         ; 4806             if((vuintx)(refDescSvcHead->reqHeadByteSpec & svcInstFailedBytePosMask) != (vuintx)0)
6943  099f ed86              	ldy	OFST-4,s
6944  09a1 e640              	ldab	0,y
6945  09a3 e485              	andb	OFST-5,s
6946                         ; 4808               g_descNegResCode[DESC_CONTEXT_PARAM_VALUE] = kDescNrcRequestOutOfRange;
6949  09a5 26ec              	bne	LC003
6950                         ; 4813               g_descNegResCode[DESC_CONTEXT_PARAM_VALUE] = kDescNrcSubfunctionNotSupported;
6952  09a7 c612              	ldab	#18
6953  09a9 2006              	bra	LC001
6954                         ; 4824           g_descNegResCode[DESC_CONTEXT_PARAM_VALUE] = kDescNrcInvalidFormat;
6958  09ab                   L5752:
6959                         ; 4833         g_descNegResCode[DESC_CONTEXT_PARAM_VALUE] = kDescNrcServiceNotSupportedInActiveSession;
6961  09ab c67f              	ldab	#127
6964  09ad 2002              	bra	LC001
6965  09af                   L3752:
6966                         ; 4861     g_descNegResCode[DESC_CONTEXT_PARAM_VALUE] = kDescNrcServiceNotSupported;
6969  09af c611              	ldab	#17
6970  09b1                   LC001:
6971  09b1 7b001e            	stab	L132_g_descNegResCode
6972  09b4                   L3562:
6973                         ; 4863   DescProcessingDone(DESC_CONTEXT_PARAM_ONLY);
6975  09b4 4a09bbbb          	call	f_DescProcessingDone
6977                         ; 4864 }
6978  09b8                   L242:
6981  09b8 1b8a              	leas	10,s
6982  09ba 0a                	rtc	
7008                         ; 4876 void DESC_API_CALL_TYPE DescProcessingDone(DESC_CONTEXT_FORMAL_PARAM_DEF_ONLY)
7008                         ; 4877 {
7009                         	switch	.ftext
7010  09bb                   f_DescProcessingDone:
7014                         ; 4880   switch(g_descContextCtrl[DESC_CONTEXT_PARAM_VALUE].contextMode)
7017  09bb f6003f            	ldab	L302_g_descContextCtrl+1
7018  09be c407              	andb	#7
7020  09c0 2711              	beq	L5562
7021  09c2 040113            	dbeq	b,L7562
7022                         ; 4911     default:
7022                         ; 4912       /* Unknown context mode */
7022                         ; 4913       DescAssertCommonAlways(kDescAssertInvalidContextMode);
7024  09c5 cc1331            	ldd	#4913
7025  09c8 3b                	pshd	
7026  09c9 cc0016            	ldd	#22
7027  09cc 4a000000          	call	f_ApplDescFatalError
7029  09d0 1b82              	leas	2,s
7030                         ; 4914       break;
7033  09d2 0a                	rtc	
7034  09d3                   L5562:
7035                         ; 4882     case kDescContextModeNormal:
7035                         ; 4883       /* Finalize the request processing */
7035                         ; 4884       DescFinalProcessingDone(DESC_CONTEXT_PARAM_ONLY);
7037  09d3 4a09dddd          	call	L301f_DescFinalProcessingDone
7039                         ; 4885       break;
7042  09d7 0a                	rtc	
7043  09d8                   L7562:
7044                         ; 4888     case kDescContextModePidList:
7044                         ; 4889       /* Current Pid list process finalization */
7044                         ; 4890       DescPidProcessingDone(DESC_CONTEXT_PARAM_ONLY);
7046  09d8 4a0ee6e6          	call	L761f_DescPidProcessingDone
7048                         ; 4891       break;
7050                         ; 4916 }
7053  09dc 0a                	rtc	
7106                         ; 4929 static void DescFinalProcessingDone(DESC_CONTEXT_FORMAL_PARAM_DEF_ONLY)
7106                         ; 4930 #else
7106                         ; 4931 void DESC_API_CALL_TYPE DescProcessingDone(DESC_CONTEXT_FORMAL_PARAM_DEF_ONLY)
7106                         ; 4932 #endif
7106                         ; 4933 {
7107                         	switch	.ftext
7108  09dd                   L301f_DescFinalProcessingDone:
7110  09dd 1b9b              	leas	-5,s
7111       00000005          OFST:	set	5
7114                         ; 4941   DescAssertCommon((g_descInterruptContextCtrl[DESC_CONTEXT_PARAM_VALUE].activity == kDescContextActiveProcess),
7117  09df f6003c            	ldab	L502_g_descInterruptContextCtrl+2
7118  09e2 c104              	cmpb	#4
7119  09e4 2710              	beq	L3272
7122  09e6 cc134e            	ldd	#4942
7123  09e9 3b                	pshd	
7124  09ea cc000e            	ldd	#14
7125  09ed 4a000000          	call	f_ApplDescFatalError
7127  09f1 1b82              	leas	2,s
7128  09f3 f6003c            	ldab	L502_g_descInterruptContextCtrl+2
7129  09f6                   L3272:
7130                         ; 4944   if((g_descInterruptContextCtrl[DESC_CONTEXT_PARAM_VALUE].activity & kDescContextActiveProcess) != 0)
7133  09f6 c504              	bitb	#4
7134  09f8 182700c4          	beq	L5272
7135                         ; 4948     g_descContextCtrl[DESC_CONTEXT_PARAM_VALUE].contextMode = kDescContextModeNormal;
7137  09fc 1d003f07          	bclr	L302_g_descContextCtrl+1,7
7138                         ; 4965     if ((g_descMsgContext[DESC_CONTEXT_PARAM_VALUE].msgAddInfo.resOnReq &
7138                         ; 4966          g_descMsgContext[DESC_CONTEXT_PARAM_VALUE].msgAddInfo.reqType) !=0 )
7142  0a00 f60027            	ldab	L722_g_descMsgContext+8
7143  0a03 c403              	andb	#3
7144  0a05 6b80              	stab	OFST-5,s
7145  0a07 f60027            	ldab	L722_g_descMsgContext+8
7146  0a0a c40c              	andb	#12
7147  0a0c 54                	lsrb	
7148  0a0d 54                	lsrb	
7149  0a0e e480              	andb	OFST-5,s
7150  0a10 182700a6          	beq	L3472
7151                         ; 4969       msg = g_descInterruptContextCtrl[DESC_CONTEXT_PARAM_VALUE].infoPoolPtr->reqDataPtr;
7153  0a14 fd003a            	ldy	L502_g_descInterruptContextCtrl
7154  0a17 18024c81          	movw	12,y,OFST-4,s
7155                         ; 4971       if(g_descNegResCode[DESC_CONTEXT_PARAM_VALUE] != kDescNrcNone)
7157  0a1b f6001e            	ldab	L132_g_descNegResCode
7158  0a1e 272f              	beq	L1372
7159                         ; 4976         if((g_descMsgContext[DESC_CONTEXT_PARAM_VALUE].msgAddInfo.reqType & kDescFuncReq) != 0)
7161  0a20 1f00270214        	brclr	L722_g_descMsgContext+8,2,L3372
7162                         ; 4979           switch(g_descNegResCode[DESC_CONTEXT_PARAM_VALUE])
7165  0a25 c011              	subb	#17
7166  0a27 2707              	beq	L7762
7167  0a29 040104            	dbeq	b,L7762
7168  0a2c c01f              	subb	#31
7169  0a2e 2609              	bne	L3372
7170  0a30                   L7762:
7171                         ; 4981             case 0x11:/* fall through */
7171                         ; 4982             case 0x12:/* fall through */
7171                         ; 4983             case 0x31:/* fall through */
7171                         ; 4984             DESC_OEM_SUPPRESSED_FUNC_NRC
7171                         ; 4985               /* fall through */
7171                         ; 4986               /* Simulate success confirmation */
7171                         ; 4987               DescDoPostProcessing(DESC_CONTEXT_PARAM_FIRST kDescUsdtNetworkOk);
7173  0a30 87                	clra	
7174  0a31 c7                	clrb	
7175  0a32 4a0ac3c3          	call	L36f_DescDoPostProcessing
7177                         ; 4988               return;/* Leave the function */
7180  0a36 1b85              	leas	5,s
7181  0a38 0a                	rtc	
7182                         ; 4989             default:break;/* Send negative response onto the comm bus */
7184  0a39                   L3372:
7185                         ; 5014         msg[1] = msg[0]; /* Copy the SID */
7187  0a39 ed81              	ldy	OFST-4,s
7188  0a3b b765              	tfr	y,x
7189  0a3d 180a0041          	movb	0,x,1,y
7190                         ; 5015         msg[0] = kDescNegResSId;/* Place 0x7F ...*/
7192  0a41 c67f              	ldab	#127
7193  0a43 6b40              	stab	0,y
7194                         ; 5016         msg[2] = g_descNegResCode[DESC_CONTEXT_PARAM_VALUE];/*... and the current error code*/
7196  0a45 180942001e        	movb	L132_g_descNegResCode,2,y
7197                         ; 5017         g_descMsgContext[DESC_CONTEXT_PARAM_VALUE].resDataLen = kDescNegResLen;
7199  0a4a cc0003            	ldd	#3
7201  0a4d 2061              	bra	L1472
7202  0a4f                   L1372:
7203                         ; 5028         if(g_descMsgContext[DESC_CONTEXT_PARAM_VALUE].msgAddInfo.suppPosRes == 0)
7205  0a4f 1e00271066        	brset	L722_g_descMsgContext+8,16,L3472
7206                         ; 5032           msg[0] += kDescPosResIdOffset;
7208  0a54 ed81              	ldy	OFST-4,s
7209  0a56 e640              	ldab	0,y
7210  0a58 cb40              	addb	#64
7211  0a5a 6b70              	stab	1,y+
7212                         ; 5033           msg++;
7214  0a5c 6d81              	sty	OFST-4,s
7215                         ; 5035           DescAssertCommon((g_descCurReqSvc[DESC_CONTEXT_PARAM_VALUE] < kDescSvcHeadNumItems),kDescAssertSvcTableIndexOutOfRange);
7217  0a5e f6002f            	ldab	L322_g_descCurReqSvc
7218  0a61 c110              	cmpb	#16
7219  0a63 2510              	blo	L5472
7222  0a65 cc13ab            	ldd	#5035
7223  0a68 3b                	pshd	
7224  0a69 cc0009            	ldd	#9
7225  0a6c 4a000000          	call	f_ApplDescFatalError
7227  0a70 1b82              	leas	2,s
7228  0a72 f6002f            	ldab	L322_g_descCurReqSvc
7229  0a75                   L5472:
7230                         ; 5038           resHeadLen = DescExtractResExtHeadLen((vuintx)(g_descSvcHead[g_descCurReqSvc[DESC_CONTEXT_PARAM_VALUE]].resHeadExLen));
7233  0a75 8608              	ldaa	#8
7234  0a77 12                	mul	
7235  0a78 b746              	tfr	d,y
7236  0a7a e6ea008e          	ldab	L352_g_descSvcHead+1,y
7237  0a7e 54                	lsrb	
7238  0a7f 54                	lsrb	
7239  0a80 54                	lsrb	
7240  0a81 54                	lsrb	
7241  0a82 87                	clra	
7242  0a83 6c83              	std	OFST-2,s
7243                         ; 5040           DescMsgCopyRomToFarRam(msg,
7243                         ; 5041                                  DescGetSvcInstResHeadExt(&g_descSvcHead[g_descCurReqSvc[DESC_CONTEXT_PARAM_VALUE]],
7243                         ; 5042                                                            g_descCurReqSvcInst[DESC_CONTEXT_PARAM_VALUE]),
7243                         ; 5043                                  (vuint16)resHeadLen);
7245  0a85 3b                	pshd	
7246  0a86 f6002e            	ldab	L522_g_descCurReqSvcInst
7247  0a89 3b                	pshd	
7248  0a8a f6002f            	ldab	L322_g_descCurReqSvc
7249  0a8d 8608              	ldaa	#8
7250  0a8f 12                	mul	
7251  0a90 c3008d            	addd	#L352_g_descSvcHead
7252  0a93 4a072121          	call	L101f_DescGetSvcInstResHeadExt
7254  0a97 1b82              	leas	2,s
7255  0a99 b746              	tfr	d,y
7256  0a9b ee83              	ldx	OFST-2,s
7257  0a9d 3a                	puld	
7258  0a9e 044407            	tbeq	d,L052
7259  0aa1                   L252:
7260  0aa1 180a7030          	movb	1,y+,1,x+
7261  0aa5 0434f9            	dbne	d,L252
7262  0aa8                   L052:
7263                         ; 5046           resHeadLen++;
7265  0aa8 186283            	incw	OFST-2,s
7266                         ; 5049           g_descMsgContext[DESC_CONTEXT_PARAM_VALUE].resDataLen += (DescMsgLen)resHeadLen;
7268  0aab fc0025            	ldd	L722_g_descMsgContext+6
7269  0aae e383              	addd	OFST-2,s
7271  0ab0                   L1472:
7272  0ab0 7c0025            	std	L722_g_descMsgContext+6
7273                         ; 5082         g_descInterruptContextCtrl[DESC_CONTEXT_PARAM_VALUE].activity = kDescContextActiveTxReady;
7275  0ab3 c610              	ldab	#16
7276  0ab5 7b003c            	stab	L502_g_descInterruptContextCtrl+2
7278  0ab8 2006              	bra	L5272
7279  0aba                   L3472:
7280                         ; 5063           DescDoPostProcessing(DESC_CONTEXT_PARAM_FIRST kDescUsdtNetworkOk);
7283                         ; 5064           return;
7285                         ; 5088       DescDoPostProcessing(DESC_CONTEXT_PARAM_FIRST kDescUsdtNetworkOk);
7287  0aba 87                	clra	
7288  0abb c7                	clrb	
7289  0abc 4a0ac3c3          	call	L36f_DescDoPostProcessing
7291  0ac0                   L5272:
7292                         ; 5091 }
7295  0ac0 1b85              	leas	5,s
7296  0ac2 0a                	rtc	
7335                         ; 5102 static void DescDoPostProcessing(DESC_CONTEXT_FORMAL_PARAM_DEF_FIRST t_descUsdtNetResult status)
7335                         ; 5103 {
7336                         	switch	.ftext
7337  0ac3                   L36f_DescDoPostProcessing:
7341                         ; 5107   g_descContextCtrl[DESC_CONTEXT_PARAM_VALUE].txState = status;
7344  0ac3 7b003e            	stab	L302_g_descContextCtrl
7345                         ; 5110   DescDeactivateT2Timer(DESC_CONTEXT_PARAM_VALUE);
7347  0ac6 18790037          	clrw	L112_g_descT2Timer
7348                         ; 5113   g_descInterruptContextCtrl[DESC_CONTEXT_PARAM_VALUE].activity = kDescContextActivePostProcess;
7351  0aca c640              	ldab	#64
7352  0acc 7b003c            	stab	L502_g_descInterruptContextCtrl+2
7353                         ; 5121   DescReleaseContext(DESC_CONTEXT_PARAM_ONLY);
7357  0acf 4a031919          	call	L74f_DescReleaseContext
7359                         ; 5122 }
7362  0ad3 0a                	rtc	
7396                         ; 5133 void DESC_API_CALL_TYPE DescInitPowerOn(DescInitParam initParam)
7396                         ; 5134 {
7397                         	switch	.ftext
7398  0ad4                   f_DescInitPowerOn:
7400  0ad4 3b                	pshd	
7401       00000000          OFST:	set	0
7404                         ; 5138   DescSubcompOnceInitPowerOnDebug();
7406  0ad5 4a000000          	call	L3f_CheckTableConsistency
7408                         ; 5139   DescSubcompOnceInitPowerOnNetwork();
7410  0ad9 4a035c5c          	call	L14f_DescNetworkInitPowerOn
7412                         ; 5157   DescInit(initParam);
7429  0add e681              	ldab	OFST+1,s
7430  0adf 87                	clra	
7431  0ae0 4a0ae6e6          	call	f_DescInit
7433                         ; 5158 }
7436  0ae4 31                	puly	
7437  0ae5 0a                	rtc	
7476                         ; 5169 void DESC_API_CALL_TYPE DescInit(DescInitParam initParam)
7476                         ; 5170 {
7477                         	switch	.ftext
7478  0ae6                   f_DescInit:
7482                         ; 5175   DESC_IGNORE_UNREF_PARAM(initParam);
7484                         ; 5181   DescSubcompOnceInitNetwork();
7487  0ae6 4a036161          	call	L34f_DescNetworkInit
7489                         ; 5182   DescSubcompOnceInitTiming();
7491  0aea 4a045353          	call	L15f_DescTimingOnceInit
7493                         ; 5183   DescSubcompOnceInitState();
7495  0aee 4a045f5f          	call	L16f_DescStateOnceInit
7497                         ; 5206     DescSubcompIterInitDebug(DESC_CONTEXT_PARAM_VALUE);
7513  0af2 4a00b4b4          	call	L5f_DescDebugIterInit
7515                         ; 5207     DescSubcompIterInitNetwork(DESC_CONTEXT_PARAM_VALUE);
7517  0af6 4a030000          	call	L53f_DescNetworkIterInit
7519                         ; 5208     DescSubcompIterInitTiming(DESC_CONTEXT_PARAM_VALUE);
7521  0afa 4a042323          	call	L35f_DescTimingIterInit
7523                         ; 5210     DescSubcompIterInitDispatcher(DESC_CONTEXT_PARAM_VALUE);
7526  0afe 4a084040          	call	L56f_DescDispatcherIterInit
7528                         ; 5218     DescSubcompIterInitPidListProcessor(DESC_CONTEXT_PARAM_VALUE);
7537  0b02 4a0ee1e1          	call	L361f_DescIterInitPidListProcessor
7539                         ; 5227 }
7546  0b06 0a                	rtc	
7573                         ; 5238 void DESC_API_CALL_TYPE DescTimerTask(void)
7573                         ; 5239 {
7574                         	switch	.ftext
7575  0b07                   f_DescTimerTask:
7579                         ; 5250     if((g_descInterruptContextCtrl[DESC_CONTEXT_PARAM_VALUE].activity & (kDescContextActiveProcess|kDescContextActiveProcessEnd)) != 0)
7581  0b07 1f003c0c04        	brclr	L502_g_descInterruptContextCtrl+2,12,L1303
7582                         ; 5252       DescResponseTimeoutTask(DESC_CONTEXT_PARAM_ONLY);
7584  0b0c 4a0b3737          	call	L701f_DescResponseTimeoutTask
7586  0b10                   L1303:
7587                         ; 5261   if(DescIsNoConnectionActive())
7589  0b10 1e003d0421        	brset	L502_g_descInterruptContextCtrl+3,4,L3303
7590                         ; 5268     if(g_descS1Timer != 0)
7592  0b15 fc0035            	ldd	L312_g_descS1Timer
7593  0b18 271c              	beq	L3303
7594                         ; 5270       if(g_descDoReloadS1Timer == kDescTrue)
7596  0b1a fc0032            	ldd	L712_g_descDoReloadS1Timer
7597  0b1d 04240a            	dbne	d,L7303
7598                         ; 5275         g_descDoReloadS1Timer = kDescFalse;
7600  0b20 18790032          	clrw	L712_g_descDoReloadS1Timer
7601                         ; 5276         DescReloadS1Timer();
7603  0b24 cc01f4            	ldd	#500
7604  0b27 7c0035            	std	L312_g_descS1Timer
7605  0b2a                   L7303:
7606                         ; 5279       g_descS1Timer--;
7608  0b2a 18730035          	decw	L312_g_descS1Timer
7609                         ; 5281       if(g_descS1Timer == 0)
7611  0b2e 2606              	bne	L3303
7612                         ; 5288         DescSetState(kDescDefaultSessionSvcInstEntry);
7614  0b30 87                	clra	
7615  0b31 c7                	clrb	
7616  0b32 4a051f1f          	call	L562f_DescSetState
7619  0b36                   L3303:
7620                         ; 5323 }
7626  0b36 0a                	rtc	
7654                         ; 5334 static void DescResponseTimeoutTask(DESC_CONTEXT_FORMAL_PARAM_DEF_ONLY)
7654                         ; 5335 {
7655                         	switch	.ftext
7656  0b37                   L701f_DescResponseTimeoutTask:
7658  0b37 37                	pshb	
7659       00000001          OFST:	set	1
7662                         ; 5385   if(g_descT2Timer[DESC_CONTEXT_PARAM_VALUE] != 0)
7664  0b38 fc0037            	ldd	L112_g_descT2Timer
7665  0b3b 2765              	beq	L5503
7666                         ; 5389     if(g_descInterruptContextCtrl[DESC_CONTEXT_PARAM_VALUE].forcedRcrRpState == kDescForcedRcrRpCharged)
7668  0b3d f6003d            	ldab	L502_g_descInterruptContextCtrl+3
7669  0b40 c403              	andb	#3
7670  0b42 04211d            	dbne	b,L7503
7671                         ; 5392       if(g_descInterruptContextCtrl[DESC_CONTEXT_PARAM_VALUE].infoPoolPtr->resType == kDescUsdtResponseNone)
7673  0b45 fd003a            	ldy	L502_g_descInterruptContextCtrl
7674  0b48 e647              	ldab	7,y
7675  0b4a 260e              	bne	L1603
7676                         ; 5402           g_descInterruptContextCtrl[DESC_CONTEXT_PARAM_VALUE].infoPoolPtr->resType = kDescUsdtResponseNegativeApplRCR_RP;
7678  0b4c c605              	ldab	#5
7679  0b4e 6b47              	stab	7,y
7680                         ; 5404           g_descInterruptContextCtrl[DESC_CONTEXT_PARAM_VALUE].forcedRcrRpState = kDescForcedRcrRpIdle;
7682  0b50 1d003d03          	bclr	L502_g_descInterruptContextCtrl+3,3
7683                         ; 5406           DescTransmitRcrRp(DESC_CONTEXT_PARAM_WRAPPER_ONLY(DESC_CONTEXT_PARAM_VALUE));
7685  0b54 4a03f8f8          	call	L54f_DescTransmitRcrRp
7688  0b58 2048              	bra	L5503
7689  0b5a                   L1603:
7690                         ; 5411         g_descT2Timer[DESC_CONTEXT_PARAM_VALUE] = 1;
7692  0b5a cc0001            	ldd	#1
7693  0b5d 7c0037            	std	L112_g_descT2Timer
7694  0b60 2040              	bra	L5503
7695  0b62                   L7503:
7696                         ; 5418       g_descT2Timer[DESC_CONTEXT_PARAM_VALUE]--;
7698  0b62 18730037          	decw	L112_g_descT2Timer
7699                         ; 5420       if(g_descT2Timer[DESC_CONTEXT_PARAM_VALUE] == 0)
7701  0b66 263a              	bne	L5503
7702                         ; 5427           if((g_descMsgContext[DESC_CONTEXT_PARAM_VALUE].msgAddInfo.resOnReq &
7702                         ; 5428               g_descMsgContext[DESC_CONTEXT_PARAM_VALUE].msgAddInfo.reqType) != 0)
7704  0b68 f60027            	ldab	L722_g_descMsgContext+8
7705  0b6b c403              	andb	#3
7706  0b6d 6b80              	stab	OFST-1,s
7707  0b6f f60027            	ldab	L722_g_descMsgContext+8
7708  0b72 c40c              	andb	#12
7709  0b74 54                	lsrb	
7710  0b75 54                	lsrb	
7711  0b76 e480              	andb	OFST-1,s
7712  0b78 271b              	beq	L1703
7713                         ; 5431             g_descMsgContext[DESC_CONTEXT_PARAM_VALUE].msgAddInfo.suppPosRes = 0;
7715  0b7a 1d002710          	bclr	L722_g_descMsgContext+8,16
7716                         ; 5433             if(g_descInterruptContextCtrl[DESC_CONTEXT_PARAM_VALUE].infoPoolPtr->resType == kDescUsdtResponseNone)
7718  0b7e fd003a            	ldy	L502_g_descInterruptContextCtrl
7719  0b81 e647              	ldab	7,y
7720  0b83 260a              	bne	L3703
7721                         ; 5443                 g_descInterruptContextCtrl[DESC_CONTEXT_PARAM_VALUE].infoPoolPtr->resType = kDescUsdtResponseNegativeRCR_RP;
7723  0b85 c604              	ldab	#4
7724  0b87 6b47              	stab	7,y
7725                         ; 5445                 DescTransmitRcrRp(DESC_CONTEXT_PARAM_WRAPPER_ONLY(DESC_CONTEXT_PARAM_VALUE));
7727  0b89 4a03f8f8          	call	L54f_DescTransmitRcrRp
7730  0b8d 2013              	bra	L5503
7731  0b8f                   L3703:
7732                         ; 5451               g_descT2Timer[DESC_CONTEXT_PARAM_VALUE]++;
7734  0b8f 18720037          	incw	L112_g_descT2Timer
7735  0b93 200d              	bra	L5503
7736  0b95                   L1703:
7737                         ; 5459             DescAssertUserAlways(kDescAssertApplLackOfConfirmation);
7739  0b95 cc1553            	ldd	#5459
7740  0b98 3b                	pshd	
7741  0b99 cc0011            	ldd	#17
7742  0b9c 4a000000          	call	f_ApplDescFatalError
7744  0ba0 1b82              	leas	2,s
7745  0ba2                   L5503:
7746                         ; 5499 }
7749  0ba2 1b81              	leas	1,s
7750  0ba4 0a                	rtc	
7774                         ; 5511 void DESC_API_CALL_TYPE DescTask(void)
7774                         ; 5512 {
7775                         	switch	.ftext
7776  0ba5                   f_DescTask:
7780                         ; 5516   DescStateTask();
7782  0ba5 4a0baeae          	call	f_DescStateTask
7784                         ; 5518   DescTimerTask();
7786  0ba9 4a0b0707          	call	f_DescTimerTask
7788                         ; 5519 }
7791  0bad 0a                	rtc	
7816                         ; 5531 void DESC_API_CALL_TYPE DescStateTask(void)
7816                         ; 5532 {
7817                         	switch	.ftext
7818  0bae                   f_DescStateTask:
7822                         ; 5560     if(g_descInterruptContextCtrl[DESC_CONTEXT_PARAM_VALUE].activity != kDescContextIdle)
7825  0bae f6003c            	ldab	L502_g_descInterruptContextCtrl+2
7826  0bb1 2704              	beq	L1213
7827                         ; 5562       DescContextStateTask(DESC_CONTEXT_PARAM_ONLY);
7829  0bb3 4a0bbcbc          	call	L501f_DescContextStateTask
7831  0bb7                   L1213:
7832                         ; 5577   DescUsdtNetAbsStateTask();
7835  0bb7 4a00e8e8          	call	L31f_DescUsdtNetIsoTpStateTask
7837                         ; 5578 }
7840  0bbb 0a                	rtc	
7889                         ; 5589 static void DescContextStateTask(DESC_CONTEXT_FORMAL_PARAM_DEF_ONLY)
7889                         ; 5590 {
7890                         	switch	.ftext
7891  0bbc                   L501f_DescContextStateTask:
7893  0bbc 37                	pshb	
7894       00000001          OFST:	set	1
7897                         ; 5594   if((g_descInterruptContextCtrl[DESC_CONTEXT_PARAM_VALUE].activity & kDescContextActivePostProcess) != 0)
7899  0bbd 1f003c404f        	brclr	L502_g_descInterruptContextCtrl+2,64,L7313
7900                         ; 5598     DescInterruptDisable();
7902  0bc2 4a000000          	call	f_VStdSuspendAllInterrupts
7904                         ; 5599     g_descInterruptContextCtrl[DESC_CONTEXT_PARAM_VALUE].activity &= (DescContextActivity)(~kDescContextActivePostProcess);
7906  0bc6 1d003c40          	bclr	L502_g_descInterruptContextCtrl+2,64
7907                         ; 5600     DescInterruptRestore();
7909  0bca 4a000000          	call	f_VStdResumeAllInterrupts
7911                         ; 5603     postProcessState = kDescPostHandlerStateOk;
7913  0bce c601              	ldab	#1
7914  0bd0 6b80              	stab	OFST-1,s
7915                         ; 5606     if(g_descContextCtrl[DESC_CONTEXT_PARAM_VALUE].txState != kDescUsdtNetworkOk)
7917  0bd2 f6003e            	ldab	L302_g_descContextCtrl
7918  0bd5 2704              	beq	L1413
7919                         ; 5609       postProcessState = kDescPostHandlerStateTxFailed;
7921  0bd7 c604              	ldab	#4
7922  0bd9 6b80              	stab	OFST-1,s
7923  0bdb                   L1413:
7924                         ; 5613     if(g_descNegResCode[DESC_CONTEXT_PARAM_VALUE] != kDescNrcNone)
7926  0bdb f6001e            	ldab	L132_g_descNegResCode
7927  0bde 2706              	beq	L3413
7928                         ; 5616       postProcessState &= (vuint8)(~kDescPostHandlerStateOk);
7930  0be0 0d8001            	bclr	OFST-1,s,1
7931                         ; 5617       postProcessState |= kDescPostHandlerStateNegResSent;
7933  0be3 0c8002            	bset	OFST-1,s,2
7934  0be6                   L3413:
7935                         ; 5631     if(g_descContextCtrl[DESC_CONTEXT_PARAM_VALUE].isApplError != 0)
7938  0be6 1f003f081a        	brclr	L302_g_descContextCtrl+1,8,L5413
7939                         ; 5634       g_descPostHandlerTable[g_descSvcInst[g_descCurReqSvcInst[DESC_CONTEXT_PARAM_VALUE]].postHandlerRef](DESC_CONTEXT_PARAM_FIRST postProcessState);
7941  0beb e680              	ldab	OFST-1,s
7942  0bed 87                	clra	
7943  0bee 3b                	pshd	
7944  0bef f6002e            	ldab	L522_g_descCurReqSvcInst
7945  0bf2 860b              	ldaa	#11
7946  0bf4 12                	mul	
7947  0bf5 b746              	tfr	d,y
7948  0bf7 e6ea012b          	ldab	L752_g_descSvcInst+6,y
7949  0bfb 87                	clra	
7950  0bfc 59                	lsld	
7951  0bfd 59                	lsld	
7952  0bfe b745              	tfr	d,x
7953  0c00 3a                	puld	
7954  0c01 4be3010d          	call	[L552_g_descPostHandlerTable,x]
7956  0c05                   L5413:
7957                         ; 5644     if((postProcessState & kDescPostHandlerStateOk) != 0)
7959  0c05 0f800108          	brclr	OFST-1,s,1,L7313
7960                         ; 5646       DescSetState(g_descCurReqSvcInst[DESC_CONTEXT_PARAM_VALUE]);
7962  0c09 f6002e            	ldab	L522_g_descCurReqSvcInst
7963  0c0c 87                	clra	
7964  0c0d 4a051f1f          	call	L562f_DescSetState
7966  0c11                   L7313:
7967                         ; 5654   if(g_descInterruptContextCtrl[DESC_CONTEXT_PARAM_VALUE].activity == kDescContextActiveRxEnd)
7969  0c11 f6003c            	ldab	L502_g_descInterruptContextCtrl+2
7970  0c14 c102              	cmpb	#2
7971  0c16 2644              	bne	L1513
7972                         ; 5657     g_descInterruptContextCtrl[DESC_CONTEXT_PARAM_VALUE].activity = kDescContextActiveProcess;
7974  0c18 c604              	ldab	#4
7975  0c1a 7b003c            	stab	L502_g_descInterruptContextCtrl+2
7976                         ; 5665       if (g_descInterruptContextCtrl[DESC_CONTEXT_PARAM_VALUE].infoPoolPtr->reqType == kDescUsdtNetReqTypeFunctional)
7978  0c1d fd003a            	ldy	L502_g_descInterruptContextCtrl
7979  0c20 e646              	ldab	6,y
7980  0c22 04210a            	dbne	b,L3513
7981                         ; 5668         g_descMsgContext[DESC_CONTEXT_PARAM_VALUE].msgAddInfo.reqType = kDescFuncReq;
7983  0c25 1d002703          	bclr	L722_g_descMsgContext+8,3
7984  0c29 1c002702          	bset	L722_g_descMsgContext+8,2
7986  0c2d 2008              	bra	L5513
7987  0c2f                   L3513:
7988                         ; 5673         g_descMsgContext[DESC_CONTEXT_PARAM_VALUE].msgAddInfo.reqType = kDescPhysReq;
7990  0c2f 1d002703          	bclr	L722_g_descMsgContext+8,3
7991  0c33 1c002701          	bset	L722_g_descMsgContext+8,1
7992  0c37                   L5513:
7993                         ; 5678       g_descRcrrpBuffer[DESC_CONTEXT_PARAM_VALUE][1] = *g_descInterruptContextCtrl[DESC_CONTEXT_PARAM_VALUE].infoPoolPtr->reqDataPtr;
7995  0c37 e6eb000c          	ldab	[12,y]
7996  0c3b 7b0041            	stab	L102_g_descRcrrpBuffer+1
7997                         ; 5683       DescActivateT2Timer(DESC_CONTEXT_PARAM_VALUE);
7999  0c3e f60034            	ldab	L512_g_descCurrSessionNumber
8000  0c41 87                	clra	
8001  0c42 59                	lsld	
8002  0c43 59                	lsld	
8003  0c44 b746              	tfr	d,y
8004  0c46 1805ea00030037    	movw	L342_g_descP2TicksTable,y,L112_g_descT2Timer
8005                         ; 5694     g_descMsgContext[DESC_CONTEXT_PARAM_VALUE].reqDataLen = g_descInterruptContextCtrl[DESC_CONTEXT_PARAM_VALUE].infoPoolPtr->dataLength;
8008  0c4d fd003a            	ldy	L502_g_descInterruptContextCtrl
8009  0c50 18054a0021        	movw	10,y,L722_g_descMsgContext+2
8010                         ; 5697     g_descNegResCode[DESC_CONTEXT_PARAM_VALUE] = kDescNrcNone;
8012  0c55 79001e            	clr	L132_g_descNegResCode
8013                         ; 5699     DescDispatcher(DESC_CONTEXT_PARAM_ONLY);
8015  0c58 4a084141          	call	L37f_DescDispatcher
8017  0c5c                   L1513:
8018                         ; 5709   if((g_descInterruptContextCtrl[DESC_CONTEXT_PARAM_VALUE].activity & (kDescContextActiveProcess |
8018                         ; 5710                                                                        kDescContextActiveTxReady |
8018                         ; 5711                                                                        kDescContextActiveTx |
8018                         ; 5712                                                                        kDescContextActiveProcessEnd)) != 0)
8020  0c5c 1f003c3c04        	brclr	L502_g_descInterruptContextCtrl+2,60,L7513
8021                         ; 5734     DescPidProcessorTask(DESC_CONTEXT_PARAM_ONLY);
8023  0c61 4a0f2a2a          	call	L561f_DescPidProcessorTask
8025  0c65                   L7513:
8026                         ; 5742   if(g_descInterruptContextCtrl[DESC_CONTEXT_PARAM_VALUE].activity == kDescContextActiveTxReady)
8028  0c65 f6003c            	ldab	L502_g_descInterruptContextCtrl+2
8029  0c68 c110              	cmpb	#16
8030  0c6a 262d              	bne	L1613
8031                         ; 5752       if(g_descInterruptContextCtrl[DESC_CONTEXT_PARAM_VALUE].infoPoolPtr->resType == kDescUsdtResponseNone)
8033  0c6c fd003a            	ldy	L502_g_descInterruptContextCtrl
8034  0c6f e647              	ldab	7,y
8035  0c71 2626              	bne	L1613
8036                         ; 5758         DescDeactivateT2Timer(DESC_CONTEXT_PARAM_VALUE);
8038  0c73 18790037          	clrw	L112_g_descT2Timer
8039                         ; 5762         g_descInterruptContextCtrl[DESC_CONTEXT_PARAM_VALUE].activity = kDescContextActiveTx;
8042  0c77 c620              	ldab	#32
8043  0c79 7b003c            	stab	L502_g_descInterruptContextCtrl+2
8044                         ; 5765         g_descInterruptContextCtrl[DESC_CONTEXT_PARAM_VALUE].infoPoolPtr->dataLength = g_descMsgContext[DESC_CONTEXT_PARAM_VALUE].resDataLen;
8046  0c7c 18014a0025        	movw	L722_g_descMsgContext+6,10,y
8047                         ; 5775           g_descInterruptContextCtrl[DESC_CONTEXT_PARAM_VALUE].infoPoolPtr->resType    = (g_descNegResCode[DESC_CONTEXT_PARAM_VALUE] == kDescNrcNone)?kDescUsdtResponsePositive:kDescUsdtResponseNegative;
8049  0c81 f6001e            	ldab	L132_g_descNegResCode
8050  0c84 2604              	bne	L672
8051  0c86 c601              	ldab	#1
8052  0c88 2002              	bra	L003
8053  0c8a                   L672:
8054  0c8a c603              	ldab	#3
8055  0c8c                   L003:
8056  0c8c 6b47              	stab	7,y
8057                         ; 5788           g_descInterruptContextCtrl[DESC_CONTEXT_PARAM_VALUE].infoPoolPtr->resDataPtr = g_descInterruptContextCtrl[DESC_CONTEXT_PARAM_VALUE].infoPoolPtr->reqDataPtr;
8059  0c8e 18024c4e          	movw	12,y,14,y
8060                         ; 5790         DescUsdtNetAbsTransmitResponse(g_descInterruptContextCtrl[DESC_CONTEXT_PARAM_VALUE].infoPoolPtr);
8062  0c92 fc003a            	ldd	L502_g_descInterruptContextCtrl
8063  0c95 4a019a9a          	call	L71f_DescUsdtNetIsoTpTransmitResponse
8065  0c99                   L1613:
8066                         ; 5794 }
8069  0c99 1b81              	leas	1,s
8070  0c9b 0a                	rtc	
8169                         ; 5807 static void DESC_API_CALLBACK_TYPE DescOemStartDefault(DescMsgContext* pMsgContext)
8169                         ; 5808 {
8170                         	switch	.ftext
8171  0c9c                   L111f_DescOemStartDefault:
8173  0c9c 3b                	pshd	
8174       00000000          OFST:	set	0
8177                         ; 5809   DescOemPrepareSessionControl(pMsgContext, kDescStateSessionDefault);
8179  0c9d cc0001            	ldd	#1
8180  0ca0 3b                	pshd	
8181  0ca1 ec82              	ldd	OFST+2,s
8182  0ca3 4a0df4f4          	call	L741f_DescOemPrepareSessionControl
8184  0ca7 1b84              	leas	4,s
8185                         ; 5810 }
8188  0ca9 0a                	rtc	
8226                         ; 5824 static void DESC_API_CALLBACK_TYPE DescOemStartProgramming(DescMsgContext* pMsgContext)
8226                         ; 5825 {
8227                         	switch	.ftext
8228  0caa                   L311f_DescOemStartProgramming:
8230  0caa 3b                	pshd	
8231       00000000          OFST:	set	0
8234                         ; 5826   DescOemPrepareSessionControl(pMsgContext, kDescStateSessionProgramming);
8236  0cab cc0002            	ldd	#2
8237  0cae 3b                	pshd	
8238  0caf ec82              	ldd	OFST+2,s
8239  0cb1 4a0df4f4          	call	L741f_DescOemPrepareSessionControl
8241  0cb5 1b84              	leas	4,s
8242                         ; 5827 }
8245  0cb7 0a                	rtc	
8283                         ; 5841 static void DESC_API_CALLBACK_TYPE DescOemStartExtendedDiagnostic(DescMsgContext* pMsgContext)
8283                         ; 5842 {
8284                         	switch	.ftext
8285  0cb8                   L511f_DescOemStartExtendedDiagnostic:
8287  0cb8 3b                	pshd	
8288       00000000          OFST:	set	0
8291                         ; 5843   DescOemPrepareSessionControl(pMsgContext, kDescStateSessionExtendedDiagnostic);
8293  0cb9 cc0004            	ldd	#4
8294  0cbc 3b                	pshd	
8295  0cbd ec82              	ldd	OFST+2,s
8296  0cbf 4a0df4f4          	call	L741f_DescOemPrepareSessionControl
8298  0cc3 1b84              	leas	4,s
8299                         ; 5844 }
8302  0cc5 0a                	rtc	
8340                         ; 5857 static void DESC_API_CALLBACK_TYPE DescOemCtrlEnableRxAndEnableTx(DescMsgContext* pMsgContext)
8340                         ; 5858 {
8341                         	switch	.ftext
8342  0cc6                   L121f_DescOemCtrlEnableRxAndEnableTx:
8346                         ; 5859   DescOemCommonCommCtrlProcess(DescMakeCommCtrlParam(kDescCommControlStateEnable, kDescCommControlStateEnable), pMsgContext);
8348  0cc6 3b                	pshd	
8349  0cc7 cc0001            	ldd	#1
8350  0cca 4a0d5959          	call	L141f_DescOemCommonCommCtrlProcess
8352  0cce 1b82              	leas	2,s
8353                         ; 5860 }
8356  0cd0 0a                	rtc	
8394                         ; 5873 static void DESC_API_CALLBACK_TYPE DescOemCtrlEnableRxAndDisableTx(DescMsgContext* pMsgContext)
8394                         ; 5874 {
8395                         	switch	.ftext
8396  0cd1                   L321f_DescOemCtrlEnableRxAndDisableTx:
8400                         ; 5875   DescOemCommonCommCtrlProcess(DescMakeCommCtrlParam(kDescCommControlStateDisable, kDescCommControlStateEnable), pMsgContext);
8402  0cd1 3b                	pshd	
8403  0cd2 87                	clra	
8404  0cd3 c7                	clrb	
8405  0cd4 4a0d5959          	call	L141f_DescOemCommonCommCtrlProcess
8407  0cd8 1b82              	leas	2,s
8408                         ; 5876 }
8411  0cda 0a                	rtc	
8444                         ; 5893 static void DESC_API_CALLBACK_TYPE DescOemPostStartDefault(vuint8 status)
8444                         ; 5894 {
8445                         	switch	.ftext
8446  0cdb                   L521f_DescOemPostStartDefault:
8448  0cdb 3b                	pshd	
8449       00000000          OFST:	set	0
8452                         ; 5895   DescOemCommonSessionPostProcessing(status, kDescStateSessionDefault);
8454  0cdc cc0001            	ldd	#1
8455  0cdf 3b                	pshd	
8456  0ce0 e683              	ldab	OFST+3,s
8457  0ce2 4a0e3434          	call	L151f_DescOemCommonSessionPostProcessing
8459  0ce6 1b84              	leas	4,s
8460                         ; 5896 }
8463  0ce8 0a                	rtc	
8496                         ; 5913 static void DESC_API_CALLBACK_TYPE DescOemPostStartProgramming(vuint8 status)
8496                         ; 5914 {
8497                         	switch	.ftext
8498  0ce9                   L721f_DescOemPostStartProgramming:
8500  0ce9 3b                	pshd	
8501       00000000          OFST:	set	0
8504                         ; 5915   DescOemCommonSessionPostProcessing(status, kDescStateSessionProgramming);
8506  0cea cc0002            	ldd	#2
8507  0ced 3b                	pshd	
8508  0cee e683              	ldab	OFST+3,s
8509  0cf0 4a0e3434          	call	L151f_DescOemCommonSessionPostProcessing
8511  0cf4 1b84              	leas	4,s
8512                         ; 5916 }
8515  0cf6 0a                	rtc	
8548                         ; 5933 static void DESC_API_CALLBACK_TYPE DescOemPostStartExtendedDiagnostic(vuint8 status)
8548                         ; 5934 {
8549                         	switch	.ftext
8550  0cf7                   L131f_DescOemPostStartExtendedDiagnostic:
8552  0cf7 3b                	pshd	
8553       00000000          OFST:	set	0
8556                         ; 5935   DescOemCommonSessionPostProcessing(status, kDescStateSessionExtendedDiagnostic);
8558  0cf8 cc0004            	ldd	#4
8559  0cfb 3b                	pshd	
8560  0cfc e683              	ldab	OFST+3,s
8561  0cfe 4a0e3434          	call	L151f_DescOemCommonSessionPostProcessing
8563  0d02 1b84              	leas	4,s
8564                         ; 5936 }
8567  0d04 0a                	rtc	
8599                         ; 5966 static void DESC_API_CALL_TYPE DescDummyPostHandler(DESC_CONTEXT_FORMAL_PARAM_DEF_FIRST vuint8 status)
8599                         ; 5967 {
8600                         	switch	.ftext
8601  0d05                   L731f_DescDummyPostHandler:
8605                         ; 5970   DESC_IGNORE_UNREF_PARAM(status);
8608                         ; 5971 }
8611  0d05 0a                	rtc	
8635                         ; 5983 DescBool DESC_API_CALL_TYPE DescIsSuppressPosResBitSet(DESC_CONTEXT_FORMAL_PARAM_DEF_ONLY)
8635                         ; 5984 {
8636                         	switch	.ftext
8637  0d06                   f_DescIsSuppressPosResBitSet:
8641                         ; 5985   return V_BOOL_EXPR(g_descMsgContext[DESC_CONTEXT_PARAM_VALUE].msgAddInfo.suppPosRes != 0);
8643  0d06 1f00271004        	brclr	L722_g_descMsgContext+8,16,L623
8644  0d0b cc0001            	ldd	#1
8646  0d0e 0a                	rtc	
8647  0d0f                   L623:
8648  0d0f 87                	clra	
8649  0d10 c7                	clrb	
8652  0d11 0a                	rtc	
8760                         ; 5998 static DescBool DescOemCheckAndExtractCommTypeParam(DescOemCommControlInfo * pCommControlInfo, DescMsgItem comType)
8760                         ; 5999 {
8761                         	switch	.ftext
8762  0d12                   L541f_DescOemCheckAndExtractCommTypeParam:
8764  0d12 3b                	pshd	
8765  0d13 3b                	pshd	
8766       00000002          OFST:	set	2
8769                         ; 6000   DescBool returnValue = kDescFalse;
8771  0d14 186980            	clrw	OFST-2,s
8772                         ; 6002   if(((comType & 0x0F) <= kDescCommControlMsgTypeAll) &&
8772                         ; 6003      ((comType & 0x0F) != 0))
8774  0d17 e688              	ldab	OFST+6,s
8775  0d19 c40f              	andb	#15
8776  0d1b c103              	cmpb	#3
8777  0d1d 2225              	bhi	L1053
8779  0d1f 0f880f21          	brclr	OFST+6,s,15,L1053
8780                         ; 6006     pCommControlInfo->msgTypes = (DescBitType)(comType & 0x03);
8782  0d23 e688              	ldab	OFST+6,s
8783  0d25 c403              	andb	#3
8784  0d27 ed82              	ldy	OFST+0,s
8785  0d29 58                	lslb	
8786  0d2a 58                	lslb	
8787  0d2b 0d400c            	bclr	0,y,12
8788  0d2e c40c              	andb	#12
8789  0d30 ea40              	orab	0,y
8790  0d32 6b40              	stab	0,y
8791                         ; 6007     pCommControlInfo->subNetNumber = (DescBitType)((comType & 0xF0) >> 4);
8793  0d34 e688              	ldab	OFST+6,s
8794  0d36 0d40f0            	bclr	0,y,240
8795  0d39 c4f0              	andb	#240
8796  0d3b ea40              	orab	0,y
8797  0d3d 6b40              	stab	0,y
8798                         ; 6009     returnValue = kDescTrue;
8800  0d3f cc0001            	ldd	#1
8801  0d42 6c80              	std	OFST-2,s
8802  0d44                   L1053:
8803                         ; 6011   return returnValue;
8805  0d44 ec80              	ldd	OFST-2,s
8808  0d46 1b84              	leas	4,s
8809  0d48 0a                	rtc	
8834                         ; 6024 void DESC_API_CALL_TYPE DescEnableCommunication(void)
8834                         ; 6025 {
8835                         	switch	.ftext
8836  0d49                   f_DescEnableCommunication:
8840                         ; 6027   g_descCommControlInfo.subNetNumber   = kDescCommControlSubNetNumAll;
8842                         ; 6028   g_descCommControlInfo.txPathState    = kDescCommControlStateEnable;
8844  0d49 1d001cf3          	bclr	L332_g_descCommControlInfo,243
8845                         ; 6032   g_descCommControlInfo.msgTypes       = kDescCommControlMsgTypeAll;
8847  0d4d 1c001c0d          	bset	L332_g_descCommControlInfo,13
8848                         ; 6035   DescOemPostCommonCommCtrlProcess(DESC_CONTEXT_PARAM_WRAPPER_FIRST(0) kDescPostHandlerStateOk);
8850  0d51 cc0001            	ldd	#1
8851  0d54 4a0db2b2          	call	L531f_DescOemPostCommonCommCtrlProcess
8853                         ; 6036 }
8856  0d58 0a                	rtc	
8919                         ; 6047 static void DescOemCommonCommCtrlProcess(vuint8 reqInfo, DescMsgContext *pMsgContext)
8919                         ; 6048 {
8920                         	switch	.ftext
8921  0d59                   L141f_DescOemCommonCommCtrlProcess:
8923  0d59 3b                	pshd	
8924  0d5a 1b9d              	leas	-3,s
8925       00000003          OFST:	set	3
8928                         ; 6049   DescNegResCode errorCode = kDescNrcNone;
8930  0d5c 87                	clra	
8931  0d5d 6a80              	staa	OFST-3,s
8932                         ; 6055   DESC_IGNORE_UNREF_PARAM(pMsgContext);
8934                         ; 6067     result = DescOemCheckAndExtractCommTypeParam(&g_descCommControlInfo, pMsgContext->reqData[0]);
8936  0d5f edf30008          	ldy	[OFST+5,s]
8937  0d63 e640              	ldab	0,y
8938  0d65 3b                	pshd	
8939  0d66 cc001c            	ldd	#L332_g_descCommControlInfo
8940  0d69 4a0d1212          	call	L541f_DescOemCheckAndExtractCommTypeParam
8942  0d6d 1b82              	leas	2,s
8943  0d6f 6c81              	std	OFST-2,s
8944                         ; 6068     if(kDescFalse == result)
8946  0d71 2604              	bne	L1453
8947                         ; 6070       errorCode = kDescNrcRequestOutOfRange;
8949  0d73 c631              	ldab	#49
8950  0d75 6b80              	stab	OFST-3,s
8951  0d77                   L1453:
8952                         ; 6085     if(kDescNrcNone == errorCode)
8954  0d77 e680              	ldab	OFST-3,s
8955  0d79 261c              	bne	L3453
8956                         ; 6088       g_descCommControlInfo.txPathState = reqInfo;
8958  0d7b e684              	ldab	OFST+1,s
8959  0d7d 1d001c03          	bclr	L332_g_descCommControlInfo,3
8960  0d81 c403              	andb	#3
8961  0d83 fa001c            	orab	L332_g_descCommControlInfo
8962  0d86 7b001c            	stab	L332_g_descCommControlInfo
8963                         ; 6103       if((g_descCommControlInfo.subNetNumber == kDescCommControlSubNetNumAll) ||
8963                         ; 6104         (g_descCommControlInfo.subNetNumber == kDescCommControlSubNetNumRx))
8966  0d89 c5f0              	bitb	#240
8967  0d8b 270a              	beq	L3453
8969  0d8d c4f0              	andb	#240
8970  0d8f c1f0              	cmpb	#240
8971  0d91 2704              	beq	L3453
8972                         ; 6122         errorCode = kDescNrcRequestOutOfRange;
8974  0d93 c631              	ldab	#49
8975  0d95 6b80              	stab	OFST-3,s
8976  0d97                   L3453:
8977                         ; 6133   if(errorCode == kDescNrcNone)
8979  0d97 e680              	ldab	OFST-3,s
8980  0d99 2609              	bne	L3553
8981                         ; 6136     ApplDescCheckCommCtrl(DESC_CONTEXT_PARAM_WRAPPER_FIRST(pMsgContext->iContext) &g_descCommControlInfo);
8983  0d9b cc001c            	ldd	#L332_g_descCommControlInfo
8984  0d9e 4a000000          	call	f_ApplDescCheckCommCtrl
8987  0da2 200b              	bra	L5553
8988  0da4                   L3553:
8989                         ; 6141     DescSetNegResponse(DESC_CONTEXT_PARAM_WRAPPER_FIRST(pMsgContext->iContext) kDescNrcRequestOutOfRange);
8991  0da4 cc0031            	ldd	#49
8992  0da7 4a06f4f4          	call	f_DescSetNegResponse
8994                         ; 6142     DescProcessingDone(DESC_CONTEXT_PARAM_WRAPPER_ONLY(pMsgContext->iContext));
8996  0dab 4a09bbbb          	call	f_DescProcessingDone
8998  0daf                   L5553:
8999                         ; 6144 }
9002  0daf 1b85              	leas	5,s
9003  0db1 0a                	rtc	
9039                         ; 6155 static void DESC_API_CALLBACK_TYPE DescOemPostCommonCommCtrlProcess(DESC_CONTEXT_FORMAL_PARAM_DEF_FIRST vuint8 status)
9039                         ; 6156 {
9040                         	switch	.ftext
9041  0db2                   L531f_DescOemPostCommonCommCtrlProcess:
9045                         ; 6162   if((status & kDescPostHandlerStateOk) != 0)
9048  0db2 c501              	bitb	#1
9049  0db4 2724              	beq	L1063
9050                         ; 6164     switch(g_descCommControlInfo.subNetNumber)
9052  0db6 f6001c            	ldab	L332_g_descCommControlInfo
9053  0db9 54                	lsrb	
9054  0dba 54                	lsrb	
9055  0dbb 54                	lsrb	
9056  0dbc 54                	lsrb	
9058  0dbd 2709              	beq	L7553
9059  0dbf c00f              	subb	#15
9060  0dc1 2705              	beq	L7553
9061                         ; 6181       default:
9061                         ; 6182         if(g_descCommControlInfo.commCtrlChannel != kDescCommControlCanChNone)
9063  0dc3 1e001df004        	brset	L332_g_descCommControlInfo+1,240,L5063
9064                         ; 6184           DescCommCtrlManipulateCAN(DESC_COMM_CHANNEL_PARAM_WRAP_ONLY(g_descCommControlInfo.commCtrlChannel));
9067  0dc8                   L7553:
9068                         ; 6171           DescCommCtrlManipulateCAN(DESC_COMM_CHANNEL_PARAM_ONLY);
9073                         ; 6177         break;
9075                         ; 6178       case kDescCommControlSubNetNumRx:/* Disable only the request RX comm channel (CAN)*/
9075                         ; 6179         DescCommCtrlManipulateCAN(DESC_COMM_CHANNEL_PARAM_WRAP_ONLY(g_descCommControlInfo.reqCommChannel));
9077  0dc8 4a0ddbdb          	call	L341f_DescCommCtrlManipulateCAN
9079                         ; 6180         break;
9081  0dcc                   L5063:
9082                         ; 6195     if((g_descCommControlInfo.txPathState & kDescCommControlStateEnable) != 0)
9084  0dcc 1f001c0105        	brclr	L332_g_descCommControlInfo,1,L1163
9085                         ; 6198       ApplDescOnCommunicationEnable();
9087  0dd1 4a000000          	call	f_ApplDescOnCommunicationEnable
9091  0dd5 0a                	rtc	
9092  0dd6                   L1163:
9093                         ; 6203       ApplDescOnCommunicationDisable();
9095  0dd6 4a000000          	call	f_ApplDescOnCommunicationDisable
9097  0dda                   L1063:
9098                         ; 6210 }
9101  0dda 0a                	rtc	
9127                         ; 6221 static void DescCommCtrlManipulateCAN(DESC_COMM_CHANNEL_FORMAL_PARAM_DEF_ONLY)
9127                         ; 6222 {
9128                         	switch	.ftext
9129  0ddb                   L341f_DescCommCtrlManipulateCAN:
9133                         ; 6223   if((g_descCommControlInfo.txPathState & kDescCommControlStateEnable) != 0)
9135  0ddb f6001c            	ldab	L332_g_descCommControlInfo
9136  0dde c40c              	andb	#12
9137  0de0 54                	lsrb	
9138  0de1 54                	lsrb	
9139  0de2 1f001c0107        	brclr	L332_g_descCommControlInfo,1,L5263
9140                         ; 6225     DescCommSetTxOnline(DESC_COMM_CHANNEL_PARAM_VALUE, g_descCommControlInfo.msgTypes);
9142  0de7 51                	comb	
9143  0de8 87                	clra	
9144  0de9 4a000000          	call	f_CanSetPartOnline
9148  0ded 0a                	rtc	
9149  0dee                   L5263:
9150                         ; 6229     DescCommSetTxOffline(DESC_COMM_CHANNEL_PARAM_VALUE, g_descCommControlInfo.msgTypes);
9152  0dee 87                	clra	
9153  0def 4a000000          	call	f_CanSetPartOffline
9155                         ; 6240 }
9158  0df3 0a                	rtc	
9229                         ; 6251 static void DescOemPrepareSessionControl(DescMsgContext *pMsgContext, DescStateGroup targetSession)
9229                         ; 6252 {
9230                         	switch	.ftext
9231  0df4                   L741f_DescOemPrepareSessionControl:
9233  0df4 3b                	pshd	
9234  0df5 1b9a              	leas	-6,s
9235       00000006          OFST:	set	6
9238                         ; 6258   DESC_IGNORE_UNREF_PARAM(pMsgContext);/* Not always used - ignore per default! */
9240                         ; 6268     resData = pMsgContext->resData;
9242  0df7 b746              	tfr	d,y
9243  0df9 18024480          	movw	4,y,OFST-6,s
9244                         ; 6270     pMsgContext->resDataLen = 4;
9246  0dfd cc0004            	ldd	#4
9247  0e00 ee86              	ldx	OFST+0,s
9248  0e02 6c06              	std	6,x
9249                         ; 6272     DescGetSessionTimings(targetSession, &p2Time, &p2ExTime);
9251  0e04 1a84              	leax	OFST-2,s
9252  0e06 34                	pshx	
9253  0e07 1a84              	leax	OFST-2,s
9254  0e09 34                	pshx	
9255  0e0a ec8f              	ldd	OFST+9,s
9256  0e0c 4a06cfcf          	call	f_DescGetSessionTimings
9258  0e10 1b84              	leas	4,s
9259                         ; 6274     resData[0] = DescGetHiByte(p2Time);
9261  0e12 ed80              	ldy	OFST-6,s
9262  0e14 180a8240          	movb	OFST-4,s,0,y
9263                         ; 6275     resData[1] = DescGetLoByte(p2Time);
9265  0e18 180a8341          	movb	OFST-3,s,1,y
9266                         ; 6277     resData[2] = DescGetHiByte(p2ExTime);
9268  0e1c 180a8442          	movb	OFST-2,s,2,y
9269                         ; 6278     resData[3] = DescGetLoByte(p2ExTime);
9271  0e20 180a8543          	movb	OFST-1,s,3,y
9272                         ; 6287   ApplDescCheckSessionTransition(DESC_CONTEXT_PARAM_WRAPPER_FIRST(pMsgContext->iContext) targetSession, g_descCurState.stateSession);
9274  0e24 f60031            	ldab	L122_g_descCurState+1
9275  0e27 c43f              	andb	#63
9276  0e29 87                	clra	
9277  0e2a 3b                	pshd	
9278  0e2b ec8d              	ldd	OFST+7,s
9279  0e2d 4a000000          	call	f_ApplDescCheckSessionTransition
9281                         ; 6288 }
9284  0e31 1b8a              	leas	10,s
9285  0e33 0a                	rtc	
9324                         ; 6301 static void DescOemCommonSessionPostProcessing(DESC_CONTEXT_FORMAL_PARAM_DEF_FIRST vuint8 status, DescStateGroup newSession)
9324                         ; 6302 {
9325                         	switch	.ftext
9326  0e34                   L151f_DescOemCommonSessionPostProcessing:
9328       fffffffe          OFST:	set	-2
9331                         ; 6306   DESC_IGNORE_UNREF_PARAM(status);
9334                         ; 6307   DESC_IGNORE_UNREF_PARAM(newSession);
9336                         ; 6329 }
9339  0e34 0a                	rtc	
9379                         ; 6362 static DescMsgLen DescPmGetPidResponseLen(DescPidInstIndex pidHandle)
9379                         ; 6363 {
9380                         	switch	.ftext
9381  0e35                   L161f_DescPmGetPidResponseLen:
9383  0e35 3b                	pshd	
9384  0e36 3b                	pshd	
9385       00000002          OFST:	set	2
9388                         ; 6366   result = DescPmClientGetResLength(pidHandle);
9390  0e37 186980            	clrw	OFST-2,s
9391                         ; 6371     result = g_descPIDInfo[pidHandle].resDataLen;
9393  0e3a cd000b            	ldy	#11
9394  0e3d 13                	emul	
9395  0e3e b746              	tfr	d,y
9396  0e40 ecea0653          	ldd	L362_g_descPIDInfo+2,y
9397                         ; 6374   return result;
9401  0e44 1b84              	leas	4,s
9402  0e46 0a                	rtc	
9517                         ; 6387 static DescPidAnalyseFailureReason DescPmAnalysePid(DESC_CONTEXT_FORMAL_PARAM_DEF_FIRST V_MEMROM1 DescPidTinyInfo V_MEMROM2 V_MEMROM3 * pRefTinyInfo)
9517                         ; 6388 {
9518                         	switch	.ftext
9519  0e47                   L751f_DescPmAnalysePid:
9521  0e47 3b                	pshd	
9522  0e48 3b                	pshd	
9523       00000002          OFST:	set	2
9526                         ; 6389   DescPidAnalyseFailureReason reason = pmAnalyseReasonOther;
9528  0e49 c603              	ldab	#3
9529  0e4b 6b81              	stab	OFST-1,s
9530                         ; 6393   if ((pRefTinyInfo->msgAddInfo.reqType &
9530                         ; 6394        g_descMsgContext[DESC_CONTEXT_PARAM_VALUE].msgAddInfo.reqType) != 0)
9532  0e4d f60027            	ldab	L722_g_descMsgContext+8
9533  0e50 c403              	andb	#3
9534  0e52 6b80              	stab	OFST-2,s
9535  0e54 ed82              	ldy	OFST+0,s
9536  0e56 e642              	ldab	2,y
9537  0e58 c403              	andb	#3
9538  0e5a e480              	andb	OFST-2,s
9539  0e5c 2722              	beq	L3773
9540                         ; 6398     g_descNegResCode[DESC_CONTEXT_PARAM_VALUE] = DescCheckState(&(pRefTinyInfo->checkState));
9542  0e5e b764              	tfr	y,d
9543  0e60 4a04f1f1          	call	L762f_DescCheckState
9545  0e64 7b001e            	stab	L132_g_descNegResCode
9546                         ; 6399     if(g_descNegResCode[DESC_CONTEXT_PARAM_VALUE] == kDescNrcNone)
9548  0e67 271c              	beq	L5004
9550                         ; 6417       if(g_descNegResCode[DESC_CONTEXT_PARAM_VALUE] == kDescNrcSubfunctionNotSupportedInActiveSession)
9552  0e69 c17e              	cmpb	#126
9553  0e6b 2609              	bne	L1004
9554                         ; 6420         reason = pmAnalyseReasonSession;
9556  0e6d c601              	ldab	#1
9557  0e6f 6b81              	stab	OFST-1,s
9558                         ; 6421         g_descNegResCode[DESC_CONTEXT_PARAM_VALUE] = kDescOemNrcParamIdNotSupportedInSession;
9560  0e71 c631              	ldab	#49
9561  0e73 7b001e            	stab	L132_g_descNegResCode
9562  0e76                   L1004:
9563                         ; 6424       if(g_descNegResCode[DESC_CONTEXT_PARAM_VALUE] ==  kDescOemNrcParamIdNotSupportedInSecurityState)
9565  0e76 c133              	cmpb	#51
9566  0e78 260b              	bne	L5004
9567                         ; 6426         reason = pmAnalyseReasonSecurityState;
9569  0e7a c602              	ldab	#2
9570  0e7c 6b81              	stab	OFST-1,s
9571  0e7e 2005              	bra	L5004
9572  0e80                   L3773:
9573                         ; 6435     g_descNegResCode[DESC_CONTEXT_PARAM_VALUE] = kDescOemNrcInvalidAddrMethod;
9575  0e80 c622              	ldab	#34
9576  0e82 7b001e            	stab	L132_g_descNegResCode
9577  0e85                   L5004:
9578                         ; 6437   return reason;
9580  0e85 e681              	ldab	OFST-1,s
9583  0e87 1b84              	leas	4,s
9584  0e89 0a                	rtc	
9624                         ; 6481 static DescPidInstIndex DescPmGetAvailablePidHandle(vuint16 pid)
9624                         ; 6482 {
9625                         	switch	.ftext
9626  0e8a                   L551f_DescPmGetAvailablePidHandle:
9628  0e8a 3b                	pshd	
9629  0e8b 3b                	pshd	
9630       00000002          OFST:	set	2
9633                         ; 6485   result = DescPmGetPidPoolHandle(pid);
9635  0e8c 4a0e9393          	call	L351f_DescPmGetPidPoolHandle
9637                         ; 6486   result = DescPmClientCheckPid(result, pid);
9639                         ; 6488   return result;
9643  0e90 1b84              	leas	4,s
9644  0e92 0a                	rtc	
9698                         ; 6520 static DescPidInstIndex DescPmGetPidPoolHandle(vuint16 pid)
9698                         ; 6521 {
9699                         	switch	.ftext
9700  0e93                   L351f_DescPmGetPidPoolHandle:
9702  0e93 3b                	pshd	
9703  0e94 1b9a              	leas	-6,s
9704       00000006          OFST:	set	6
9707                         ; 6527   loIdx= 0;
9709  0e96 186982            	clrw	OFST-4,s
9710                         ; 6528   hiIdx= (DescPidInstIndex)(kDescNumPids - 1);
9712  0e99 cc003f            	ldd	#63
9713  0e9c 6c84              	std	OFST-2,s
9714  0e9e                   L7404:
9715                         ; 6534     idx = (DescPidInstIndex)(((vuint16)((vuint16)hiIdx + (vuint16)loIdx)) >> 1);
9717  0e9e ec84              	ldd	OFST-2,s
9718  0ea0 e382              	addd	OFST-4,s
9719  0ea2 49                	lsrd	
9720  0ea3 6c80              	std	OFST-6,s
9721                         ; 6537     if(g_descPIDInfo[idx].reqPid == pid)
9723  0ea5 cd000b            	ldy	#11
9724  0ea8 13                	emul	
9725  0ea9 b746              	tfr	d,y
9726  0eab ecea0651          	ldd	L362_g_descPIDInfo,y
9727  0eaf ac86              	cpd	OFST+0,s
9728  0eb1 2604              	bne	L5504
9729                         ; 6560           return idx;
9731  0eb3 ec80              	ldd	OFST-6,s
9733  0eb5 2027              	bra	L263
9734  0eb7                   L5504:
9735                         ; 6565     if (pid < g_descPIDInfo[idx].reqPid)
9737  0eb7 ec80              	ldd	OFST-6,s
9738  0eb9 cd000b            	ldy	#11
9739  0ebc 13                	emul	
9740  0ebd b746              	tfr	d,y
9741  0ebf ecea0651          	ldd	L362_g_descPIDInfo,y
9742  0ec3 ac86              	cpd	OFST+0,s
9743  0ec5 2309              	bls	L7504
9744                         ; 6568       if(idx == 0)
9746  0ec7 ed80              	ldy	OFST-6,s
9747  0ec9 2710              	beq	L3504
9748                         ; 6570         break;
9750                         ; 6572       hiIdx = (DescPidInstIndex)(idx - 1);
9752  0ecb 03                	dey	
9753  0ecc 6d84              	sty	OFST-2,s
9755  0ece 2005              	bra	L1504
9756  0ed0                   L7504:
9757                         ; 6577       loIdx = (DescPidInstIndex)(idx + 1);
9759  0ed0 ed80              	ldy	OFST-6,s
9760  0ed2 02                	iny	
9761  0ed3 6d82              	sty	OFST-4,s
9762  0ed5                   L1504:
9763                         ; 6580   while (loIdx <= hiIdx);
9765  0ed5 ec82              	ldd	OFST-4,s
9766  0ed7 ac84              	cpd	OFST-2,s
9767  0ed9 23c3              	bls	L7404
9768  0edb                   L3504:
9769                         ; 6582   return kDescInvalidPidHandle;
9771  0edb cc0040            	ldd	#64
9773  0ede                   L263:
9775  0ede 1b88              	leas	8,s
9776  0ee0 0a                	rtc	
9800                         ; 6593 static void DescIterInitPidListProcessor(DESC_CONTEXT_FORMAL_PARAM_DEF_ONLY)
9800                         ; 6594 {
9801                         	switch	.ftext
9802  0ee1                   L361f_DescIterInitPidListProcessor:
9806                         ; 6600   g_descPidProcessorState[DESC_CONTEXT_PARAM_VALUE].isPidReady = 0;
9808  0ee1 1d000c01          	bclr	L732_g_descPidProcessorState+2,1
9809                         ; 6602 }
9812  0ee5 0a                	rtc	
9840                         ; 6613 static void DescPidProcessingDone(DESC_CONTEXT_FORMAL_PARAM_DEF_ONLY)
9840                         ; 6614 {
9841                         	switch	.ftext
9842  0ee6                   L761f_DescPidProcessingDone:
9844  0ee6 37                	pshb	
9845       00000001          OFST:	set	1
9848                         ; 6724   if(g_descNegResCode[DESC_CONTEXT_PARAM_VALUE] == kDescNrcNone)
9852  0ee7 f6001e            	ldab	L132_g_descNegResCode
9853  0eea 2637              	bne	L5014
9854                         ; 6727     g_descMsgContext[DESC_CONTEXT_PARAM_VALUE].resDataLen += g_descPidMsgContext[DESC_CONTEXT_PARAM_VALUE].resDataLen + kDescReservedBytesForPid;
9856  0eec fd0013            	ldy	L532_g_descPidMsgContext+6
9857  0eef 1942              	leay	2,y
9858  0ef1 18fb0025          	addy	L722_g_descMsgContext+6
9859  0ef5 7d0025            	sty	L722_g_descMsgContext+6
9860                         ; 6730     if(g_descMsgContext[DESC_CONTEXT_PARAM_VALUE].resDataLen > (DescMsgLen)(4095 - 1))
9862  0ef8 8d0ffe            	cpy	#4094
9863  0efb 2307              	bls	L7014
9864                         ; 6733       g_descNegResCode[DESC_CONTEXT_PARAM_VALUE] = kDescOemNrcResponseTooLong;
9866  0efd c614              	ldab	#20
9867  0eff 7b001e            	stab	L132_g_descNegResCode
9869  0f02 201f              	bra	L5014
9870  0f04                   L7014:
9871                         ; 6737       g_descPidMsgContext[DESC_CONTEXT_PARAM_VALUE].resData += g_descPidMsgContext[DESC_CONTEXT_PARAM_VALUE].resDataLen;
9873  0f04 fc0011            	ldd	L532_g_descPidMsgContext+4
9874  0f07 f30013            	addd	L532_g_descPidMsgContext+6
9875  0f0a 7c0011            	std	L532_g_descPidMsgContext+4
9876                         ; 6739       g_descPidProcessorState[DESC_CONTEXT_PARAM_VALUE].curPid++;
9878  0f0d f6000b            	ldab	L732_g_descPidProcessorState+1
9879  0f10 52                	incb	
9880  0f11 7b000b            	stab	L732_g_descPidProcessorState+1
9881                         ; 6741       if(g_descPidProcessorState[DESC_CONTEXT_PARAM_VALUE].curPid <
9881                         ; 6742          g_descPidProcessorState[DESC_CONTEXT_PARAM_VALUE].pidCount)
9883  0f14 180980000a        	movb	L732_g_descPidProcessorState,OFST-1,s
9884  0f19 e180              	cmpb	OFST-1,s
9885  0f1b 2406              	bhs	L5014
9886                         ; 6745         g_descPidProcessorState[DESC_CONTEXT_PARAM_VALUE].isPidReady = 1;
9888  0f1d 1c000c01          	bset	L732_g_descPidProcessorState+2,1
9889                         ; 6747         return;
9891  0f21 2004              	bra	L073
9892  0f23                   L5014:
9893                         ; 6752   DescFinalProcessingDone(DESC_CONTEXT_PARAM_ONLY);
9895  0f23 4a09dddd          	call	L301f_DescFinalProcessingDone
9897                         ; 6754 }
9898  0f27                   L073:
9901  0f27 1b81              	leas	1,s
9902  0f29 0a                	rtc	
9940                         ; 6765 static void DescPidProcessorTask(DESC_CONTEXT_FORMAL_PARAM_DEF_ONLY)
9940                         ; 6766 {
9941                         	switch	.ftext
9942  0f2a                   L561f_DescPidProcessorTask:
9944  0f2a 3b                	pshd	
9945       00000002          OFST:	set	2
9948                         ; 6770   if(g_descContextCtrl[DESC_CONTEXT_PARAM_VALUE].contextMode == kDescContextModePidList)
9950  0f2b f6003f            	ldab	L302_g_descContextCtrl+1
9951  0f2e c407              	andb	#7
9952  0f30 042167            	dbne	b,L1314
9953                         ; 6773     if(g_descPidProcessorState[DESC_CONTEXT_PARAM_VALUE].isPidReady !=0)
9955  0f33 1f000c0162        	brclr	L732_g_descPidProcessorState+2,1,L1314
9956                         ; 6775       g_descPidProcessorState[DESC_CONTEXT_PARAM_VALUE].isPidReady = 0;
9958  0f38 1d000c01          	bclr	L732_g_descPidProcessorState+2,1
9959                         ; 6788       pidHandle = g_descPidList[DESC_CONTEXT_PARAM_VALUE][g_descPidProcessorState[DESC_CONTEXT_PARAM_VALUE].curPid];
9961  0f3c f6000b            	ldab	L732_g_descPidProcessorState+1
9962  0f3f 87                	clra	
9963  0f40 b746              	tfr	d,y
9964  0f42 e6ea0000          	ldab	L142_g_descPidList,y
9965  0f46 6c80              	std	OFST-2,s
9966                         ; 6800         g_descPidMsgContext[DESC_CONTEXT_PARAM_VALUE].msgAddInfo.resOnReq = g_descPIDInfo[pidHandle].tinyInfo.msgAddInfo.resOnReq;
9968  0f48 cd000b            	ldy	#11
9969  0f4b 13                	emul	
9970  0f4c b746              	tfr	d,y
9971  0f4e e6ea0657          	ldab	L362_g_descPIDInfo+6,y
9972  0f52 1d00150c          	bclr	L532_g_descPidMsgContext+8,12
9973  0f56 c40c              	andb	#12
9974  0f58 fa0015            	orab	L532_g_descPidMsgContext+8
9975  0f5b 7b0015            	stab	L532_g_descPidMsgContext+8
9976                         ; 6802         g_descPidMsgContext[DESC_CONTEXT_PARAM_VALUE].resData[0] = DescGetHiByte(g_descPIDInfo[pidHandle].reqPid);
9978  0f5e e6ea0651          	ldab	L362_g_descPIDInfo,y
9979  0f62 fd0011            	ldy	L532_g_descPidMsgContext+4
9980  0f65 6b40              	stab	0,y
9981                         ; 6803         g_descPidMsgContext[DESC_CONTEXT_PARAM_VALUE].resData[1] = DescGetLoByte(g_descPIDInfo[pidHandle].reqPid);
9983  0f67 ec80              	ldd	OFST-2,s
9984  0f69 b765              	tfr	y,x
9985  0f6b cd000b            	ldy	#11
9986  0f6e 13                	emul	
9987  0f6f b756              	tfr	x,y
9988  0f71 b745              	tfr	d,x
9989  0f73 180ae2065241      	movb	L362_g_descPIDInfo+1,x,1,y
9990                         ; 6805         g_descPidMsgContext[DESC_CONTEXT_PARAM_VALUE].resData += kDescReservedBytesForPid;
9992  0f79 fc0011            	ldd	L532_g_descPidMsgContext+4
9993  0f7c c30002            	addd	#2
9994  0f7f 7c0011            	std	L532_g_descPidMsgContext+4
9995                         ; 6808         g_descMsgContext[DESC_CONTEXT_PARAM_VALUE].resData = g_descPidMsgContext[DESC_CONTEXT_PARAM_VALUE].resData;
9997  0f82 7c0023            	std	L722_g_descMsgContext+4
9998                         ; 6811         g_descPidMsgContext[DESC_CONTEXT_PARAM_VALUE].resDataLen = 0;
10000  0f85 18790013          	clrw	L532_g_descPidMsgContext+6
10001                         ; 6833         g_descPIDInfo[pidHandle].mainHandler(&g_descPidMsgContext[DESC_CONTEXT_PARAM_VALUE]);
10003  0f89 cc000d            	ldd	#L532_g_descPidMsgContext
10004  0f8c 3b                	pshd	
10005  0f8d ec82              	ldd	OFST+0,s
10006  0f8f cd000b            	ldy	#11
10007  0f92 13                	emul	
10008  0f93 b746              	tfr	d,y
10009  0f95 3a                	puld	
10010  0f96 4beb0658          	call	[L362_g_descPIDInfo+7,y]
10012  0f9a                   L1314:
10013                         ; 6838 }
10016  0f9a 31                	puly	
10017  0f9b 0a                	rtc	
10072                         ; 6849 static void DescPidDispatcher(DESC_CONTEXT_FORMAL_PARAM_DEF_ONLY)
10072                         ; 6850 {
10073                         	switch	.ftext
10074  0f9c                   L171f_DescPidDispatcher:
10076  0f9c 1b98              	leas	-8,s
10077       00000008          OFST:	set	8
10080                         ; 6859   g_descPidMsgContext[DESC_CONTEXT_PARAM_VALUE].reqDataLen = 0;
10082  0f9e 87                	clra	
10083  0f9f c7                	clrb	
10084  0fa0 7c000f            	std	L532_g_descPidMsgContext+2
10085                         ; 6861   g_descMsgContext[DESC_CONTEXT_PARAM_VALUE].reqDataLen >>= 1; /* (length div 2), since the PID are 2 Byte */
10087  0fa3 18740021          	lsrw	L722_g_descMsgContext+2
10088                         ; 6874   g_descPidProcessorState[DESC_CONTEXT_PARAM_VALUE].pidCount = 0;
10090                         ; 6876   g_descPidProcessorState[DESC_CONTEXT_PARAM_VALUE].curPid = 0;
10092  0fa7 7c000a            	std	L732_g_descPidProcessorState
10093                         ; 6880   iter = g_descMsgContext[DESC_CONTEXT_PARAM_VALUE].reqDataLen;
10095  0faa 1801860021        	movw	L722_g_descMsgContext+2,OFST-2,s
10097  0faf 2050              	bra	L1614
10098  0fb1                   L5514:
10099                         ; 6891     pidHandle = DescPmGetAvailablePidHandle(DescMake16Bit(g_descMsgContext[DESC_CONTEXT_PARAM_VALUE].reqData[0],
10099                         ; 6892                                                           g_descMsgContext[DESC_CONTEXT_PARAM_VALUE].reqData[1]));
10101  0fb1 fd001f            	ldy	L722_g_descMsgContext
10102  0fb4 e641              	ldab	1,y
10103  0fb6 87                	clra	
10104  0fb7 6c80              	std	OFST-8,s
10105  0fb9 aa40              	oraa	0,y
10106  0fbb 4a0e8a8a          	call	L551f_DescPmGetAvailablePidHandle
10108  0fbf 6c84              	std	OFST-4,s
10109                         ; 6895     iter--;
10111  0fc1 186386            	decw	OFST-2,s
10112                         ; 6897     g_descMsgContext[DESC_CONTEXT_PARAM_VALUE].reqData +=2;
10114  0fc4 fd001f            	ldy	L722_g_descMsgContext
10115  0fc7 1942              	leay	2,y
10116  0fc9 7d001f            	sty	L722_g_descMsgContext
10117                         ; 6903     if(pidHandle < kDescInvalidPidHandle)
10119  0fcc 8c0040            	cpd	#64
10120  0fcf 2430              	bhs	L1614
10121                         ; 6913       pidResLen = DescPmGetPidResponseLen(pidHandle);
10123  0fd1 4a0e3535          	call	L161f_DescPmGetPidResponseLen
10125  0fd5 6c82              	std	OFST-6,s
10126                         ; 6932         g_descPidList[DESC_CONTEXT_PARAM_VALUE][g_descPidProcessorState[DESC_CONTEXT_PARAM_VALUE].pidCount] = (DescMemPidInstIndex)pidHandle;
10128  0fd7 f6000a            	ldab	L732_g_descPidProcessorState
10129  0fda b796              	exg	b,y
10130  0fdc 180a85ea0000      	movb	OFST-3,s,L142_g_descPidList,y
10131                         ; 6940         (void)DescPmAnalysePid(DESC_CONTEXT_PARAM_FIRST &g_descPIDInfo[pidHandle].tinyInfo);
10133  0fe2 ec84              	ldd	OFST-4,s
10134  0fe4 cd000b            	ldy	#11
10135  0fe7 13                	emul	
10136  0fe8 c30655            	addd	#L362_g_descPIDInfo+4
10137  0feb 4a0e4747          	call	L751f_DescPmAnalysePid
10139                         ; 6990             g_descPidProcessorState[DESC_CONTEXT_PARAM_VALUE].pidCount++;
10141  0fef f6000a            	ldab	L732_g_descPidProcessorState
10142  0ff2 52                	incb	
10143  0ff3 7b000a            	stab	L732_g_descPidProcessorState
10144                         ; 6992             g_descMsgContext[DESC_CONTEXT_PARAM_VALUE].resDataLen += (DescMsgLen)(pidResLen + 2);
10146  0ff6 ed82              	ldy	OFST-6,s
10147  0ff8 1942              	leay	2,y
10148  0ffa 18fb0025          	addy	L722_g_descMsgContext+6
10149  0ffe 7d0025            	sty	L722_g_descMsgContext+6
10150  1001                   L1614:
10151                         ; 6881   while((iter != 0)&&
10151                         ; 6882         (g_descNegResCode[DESC_CONTEXT_PARAM_VALUE] == kDescNrcNone))
10153  1001 ec86              	ldd	OFST-2,s
10154  1003 2705              	beq	L7614
10156  1005 f6001e            	ldab	L132_g_descNegResCode
10157  1008 27a7              	beq	L5514
10158  100a                   L7614:
10159                         ; 7007   if(g_descPidProcessorState[DESC_CONTEXT_PARAM_VALUE].pidCount == 0)
10161  100a f6000a            	ldab	L732_g_descPidProcessorState
10162  100d 2607              	bne	L1714
10163                         ; 7020       DescSetNegResponse(DESC_CONTEXT_PARAM_FIRST kDescNrcRequestOutOfRange);
10165  100f cc0031            	ldd	#49
10166  1012 4a06f4f4          	call	f_DescSetNegResponse
10168  1016                   L1714:
10169                         ; 7025   if(g_descMsgContext[DESC_CONTEXT_PARAM_VALUE].resDataLen > (DescMsgLen)(4095 - 1)
10169                         ; 7026 #if !defined (DESC_ENABLE_RES_RINGBUFFER)
10169                         ; 7027     /* If linear buffer - check for overflow */
10169                         ; 7028      || g_descMsgContext[DESC_CONTEXT_PARAM_VALUE].resDataLen > (DescMsgLen)(DescGetAvailBufferLenByMsgContext(&g_descMsgContext[DESC_CONTEXT_PARAM_VALUE]) - 1)
10169                         ; 7029 #endif
10169                         ; 7030     )
10171  1016 fc0025            	ldd	L722_g_descMsgContext+6
10172  1019 8c0ffe            	cpd	#4094
10173  101c 2205              	bhi	L5714
10175  101e 8c0101            	cpd	#257
10176  1021 2307              	bls	L3714
10177  1023                   L5714:
10178                         ; 7034     DescSetNegResponse(DESC_CONTEXT_PARAM_FIRST kDescOemNrcResponseTooLong);
10180  1023 cc0014            	ldd	#20
10181  1026 4a06f4f4          	call	f_DescSetNegResponse
10183  102a                   L3714:
10184                         ; 7038   g_descMsgContext[DESC_CONTEXT_PARAM_VALUE].resDataLen = 0;
10186  102a 18790025          	clrw	L722_g_descMsgContext+6
10187                         ; 7040 }
10190  102e 1b88              	leas	8,s
10191  1030 0a                	rtc	
10223                         ; 7051 static void DESC_API_CALLBACK_TYPE DescPostReadDataByIdentifier(DESC_CONTEXT_FORMAL_PARAM_DEF_FIRST vuint8 status)
10223                         ; 7052 {
10224                         	switch	.ftext
10225  1031                   L331f_DescPostReadDataByIdentifier:
10229                         ; 7078   DESC_IGNORE_UNREF_PARAM(status);
10231                         ; 7082 }
10234  1031 0a                	rtc	
10278                         ; 7093 static void DESC_API_CALLBACK_TYPE DescReadDataByIdentifier(DescMsgContext *pMsgContext)
10278                         ; 7094 {
10279                         	switch	.ftext
10280  1032                   L711f_DescReadDataByIdentifier:
10282  1032 3b                	pshd	
10283       00000000          OFST:	set	0
10286                         ; 7111   if((g_descMsgContext[DESC_CONTEXT_PARAM_VALUE].reqDataLen > 1)&&
10286                         ; 7112     ((g_descMsgContext[DESC_CONTEXT_PARAM_VALUE].reqDataLen & 0x01) == 0))
10288  1033 fc0021            	ldd	L722_g_descMsgContext+2
10289  1036 8c0001            	cpd	#1
10290  1039 2337              	bls	L3324
10292  103b c501              	bitb	#1
10293  103d 2633              	bne	L3324
10294                         ; 7115     if((g_descMsgContext[DESC_CONTEXT_PARAM_VALUE].reqDataLen >> 1) <= kDescNumMaxPidList)
10296  103f 49                	lsrd	
10297  1040 8c000a            	cpd	#10
10298  1043 2229              	bhi	L5324
10299                         ; 7118       g_descContextCtrl[DESC_CONTEXT_PARAM_VALUE].contextMode = kDescContextModePidList;
10301  1045 1d003f07          	bclr	L302_g_descContextCtrl+1,7
10302  1049 1c003f01          	bset	L302_g_descContextCtrl+1,1
10303                         ; 7120       g_descPidMsgContext[DESC_CONTEXT_PARAM_VALUE] = *pMsgContext;
10305  104d ed80              	ldy	OFST+0,s
10306  104f ce000d            	ldx	#L532_g_descPidMsgContext
10307  1052 c607              	ldab	#7
10308  1054                   L204:
10309  1054 18027131          	movw	2,y+,2,x+
10310  1058 0431f9            	dbne	b,L204
10311  105b 180a4000          	movb	0,y,0,x
10312                         ; 7122       DescPidDispatcher(DESC_CONTEXT_PARAM_ONLY);
10314  105f 4a0f9c9c          	call	L171f_DescPidDispatcher
10316                         ; 7124       if(g_descNegResCode[DESC_CONTEXT_PARAM_VALUE] == kDescNrcNone)
10318  1063 f6001e            	ldab	L132_g_descNegResCode
10319  1066 260f              	bne	L3424
10320                         ; 7140         g_descPidProcessorState[DESC_CONTEXT_PARAM_VALUE].isPidReady = 1;
10322  1068 1c000c01          	bset	L732_g_descPidProcessorState+2,1
10323                         ; 7141         return;
10326  106c 31                	puly	
10327  106d 0a                	rtc	
10328  106e                   L5324:
10329                         ; 7147       g_descNegResCode[DESC_CONTEXT_PARAM_VALUE] = kDescOemNrcTooManyPIDs;
10331  106e c631              	ldab	#49
10332  1070 2002              	bra	LC006
10333  1072                   L3324:
10334                         ; 7153     g_descNegResCode[DESC_CONTEXT_PARAM_VALUE] = kDescNrcInvalidFormat;
10336  1072 c613              	ldab	#19
10337  1074                   LC006:
10338  1074 7b001e            	stab	L132_g_descNegResCode
10339  1077                   L3424:
10340                         ; 7172   DescFinalProcessingDone(DESC_CONTEXT_PARAM_ONLY);
10342  1077 4a09dddd          	call	L301f_DescFinalProcessingDone
10344                         ; 7173 }
10347  107b 31                	puly	
10348  107c 0a                	rtc	
10907                         	switch	.bss
10908  0000                   L142_g_descPidList:
10909  0000 0000000000000000  	ds.b	10
10910  000a                   L732_g_descPidProcessorState:
10911  000a 000000            	ds.b	3
10912  000d                   L532_g_descPidMsgContext:
10913  000d 0000000000000000  	ds.b	15
10914  001c                   L332_g_descCommControlInfo:
10915  001c 0000              	ds.b	2
10916  001e                   L132_g_descNegResCode:
10917  001e 00                	ds.b	1
10918  001f                   L722_g_descMsgContext:
10919  001f 0000000000000000  	ds.b	15
10920  002e                   L522_g_descCurReqSvcInst:
10921  002e 00                	ds.b	1
10922  002f                   L322_g_descCurReqSvc:
10923  002f 00                	ds.b	1
10924  0030                   L122_g_descCurState:
10925  0030 0000              	ds.b	2
10926  0032                   L712_g_descDoReloadS1Timer:
10927  0032 0000              	ds.b	2
10928  0034                   L512_g_descCurrSessionNumber:
10929  0034 00                	ds.b	1
10930  0035                   L312_g_descS1Timer:
10931  0035 0000              	ds.b	2
10932  0037                   L112_g_descT2Timer:
10933  0037 0000              	ds.b	2
10934  0039                   L702_g_descTesterAddress:
10935  0039 00                	ds.b	1
10936  003a                   L502_g_descInterruptContextCtrl:
10937  003a 00000000          	ds.b	4
10938  003e                   L302_g_descContextCtrl:
10939  003e 0000              	ds.b	2
10940  0040                   L102_g_descRcrrpBuffer:
10941  0040 000000            	ds.b	3
10942  0043                   L771_g_descBuffer:
10943  0043 0000000000000000  	ds.b	258
10944  0145                   L571_g_busInfoPoolTxRef:
10945  0145 0000              	ds.b	2
10946  0147                   L371_g_descUsdtNetInfoPoolIsoTp:
10947  0147 0000000000000000  	ds.b	16
10948                         	xref	f_ApplDescReadISOCode
10949                         	xref	f_ApplDescReadSincomAndFactory
10950                         	xref	f_ApplDescReadIdentificationCode
10951                         	xref	f_ApplDescReadExhaustRegulationOrTypeApprovalNumber
10952                         	xref	f_ApplDescReadSystemSupplierEcuSoftwareVersionNumber
10953                         	xref	f_ApplDescReadSystemSupplierEcuSoftwareNumber
10954                         	xref	f_ApplDescReadSystemSupplierEcuHardwareVersionNumber
10955                         	xref	f_ApplDescReadSystemSupplierEcuHardwareNumber
10956                         	xref	f_ApplDescReadVIN
10957                         	xref	f_ApplDescReadEcuSerialNumber
10958                         	xref	f_ApplDescReadVehicleManufacturerSparePartNumber
10959                         	xref	f_ApplDescReadActiveDiagnosticSessionDataIdentifier
10960                         	xref	f_ApplDescReadApplicationDataFingerprint
10961                         	xref	f_ApplDescReadApplicationSoftwareFingerprint
10962                         	xref	f_ApplDescReadBootSoftwareFingerprint
10963                         	xref	f_ApplDescReadApplicationDataIdentification
10964                         	xref	f_ApplDescReadApplicationSoftwareIdentification
10965                         	xref	f_ApplDescReadBootSoftwareIdentification
10966                         	xref	f_ApplDescRead_22F132_ECU_Part_Number
10967                         	xref	f_ApplDescRead_22F122_Software_Part_Number
10968                         	xref	f_ApplDescRead_22F112_Hardware_Part_Number
10969                         	xref	f_ApplDescRead_22F10B_ECU_Configuration
10970                         	xref	f_ApplDescRead_22F100_Active_Diagnostic_Information
10971                         	xref	f_ApplDescReadReadWrite
10972                         	xref	f_ApplDescRead_223001_Program_ECU_Data_at_Flash_Station_2
10973                         	xref	f_ApplDescRead_223000_Program_ECU_Data_at_Flash_Station_1
10974                         	xref	f_ApplDescRead_222810_Heated_Seat_NTC_Cutoff_Parameters_Rear_Seats
10975                         	xref	f_ApplDescRead_22280F_Heated_Seat_Max_Timeout_Parameters_Rear_Seats
10976                         	xref	f_ApplDescRead_22280E_Heated_Seat_PWM_Calibration_Parameters_Rear_Seats
10977                         	xref	f_ApplDescRead_22280D_Load_Shed_Fault_Status
10978                         	xref	f_ApplDescRead_22280C_Heated_Steering_Wheel_Min_Timeout_Parameters
10979                         	xref	f_ApplDescRead_22280A_HeatVent_Auto_Activation_Temperature_Calibration_Paramet
10980                         	xref	f_ApplDescRead_222809_Steering_Wheel_Temperature_Cut_off_Parameters
10981                         	xref	f_ApplDescRead_222808_Heated_Seat_NTC_Cutoff_Parameters_Front_Seats
10982                         	xref	f_ApplDescRead_222807_PCB_Over_Temperature_Condition_Status
10983                         	xref	f_ApplDescRead_222806_Voltage_Level_OFF
10984                         	xref	f_ApplDescRead_222805_Voltage_Level_ON
10985                         	xref	f_ApplDescRead_222804_Heated_Steering_Wheel_Max_Timeout_Parameters
10986                         	xref	f_ApplDescRead_222803_Heated_Seat_Max_Timeout_Parameters_Front_Seats
10987                         	xref	f_ApplDescRead_222802_Heated_Steering_Wheel_PWM_Calibration_Parameters
10988                         	xref	f_ApplDescRead_222801_Vented_Seat_PWM_Calibration_Parameters
10989                         	xref	f_ApplDescRead_222800_Heated_Seat_PWM_Calibration_Parameters_Front_Seats
10990                         	xref	f_ApplDescRead_222024_Received_PROXI_Information
10991                         	xref	f_ApplDescRead_2E2023_System_Configuration_PROXI
10992                         	xref	f_ApplDescReadProgrammingStatus_EEPROM_FLASH
10993                         	xref	f_ApplDescReadKeyOn_Counter_Status_EEPROM
10994                         	xref	f_ApplDescReadECU_Time_Stamps_from_KeyOn_first_DTC_detection_EEPROM
10995                         	xref	f_ApplDescReadECU_Time_first_DTC_detection_EEPROM
10996                         	xref	f_ApplDescReadKeyOn_counter_EEPROM
10997                         	xref	f_ApplDescReadECU_time_stamps_from_KeyOn_EEPROM
10998                         	xref	f_ApplDescReadECU_time_stamps_EEPROM
10999                         	xref	f_ApplDescReadNumber_of_FlashRom_re_writings_F_ROM
11000                         	xref	f_ApplDescReadOdometer_content_to_the_last_F_ROM_updating
11001                         	xref	f_ApplDescReadOdometer
11002                         	xref	f_ApplDescRead_22102A_Check_EOL_configuration_data
11003                         	xref	f_ApplDescReadECU_time_stamps_from_KeyOn_RAM
11004                         	xref	f_ApplDescReadECU_time_stamps_RAM
11005                         	xref	f_ApplDescRead_220107_Vented_Seats_Voltage_Sense
11006                         	xref	f_ApplDescRead_220106_HeatVentWheel_Current_Sense
11007                         	xref	f_ApplDescRead_220105_Heated_Seat_NTC_Feedback_Sensor_Status
11008                         	xref	f_ApplDescRead_220104_Conditions_to_activate_CSWM_outputs
11009                         	xref	f_ApplDescRead_220103_Rear_Seat_Switch_Input_Status
11010                         	xref	f_ApplDescRead_220102_Bussed_Input
11011                         	xref	f_ApplDescRead_220101_Bussed_Outputs_Status
11012                         	xref	f_ApplDescCheckSessionTransition
11013                         	xref	f_ApplDescOnCommunicationEnable
11014                         	xref	f_ApplDescOnCommunicationDisable
11015                         	xref	f_ApplDescCheckCommCtrl
11016                         	xref	f_ApplDescCtrlDtcSettingOff
11017                         	xref	f_ApplDescCtrlDtcSettingOn
11018                         	xref	f_ApplDescTester_PrresentTesterPresent
11019                         	xref	f_ApplDescReqTransferExitDownload
11020                         	xref	f_ApplDescTransferDataDownload
11021                         	xref	f_ApplDescRequestDownload
11022                         	xref	f_ApplDescReqResultscheckProgrammingDependencies_FlashChecksum
11023                         	xref	f_ApplDescReqResultsFlashErase
11024                         	xref	f_ApplDescReqResults_31010203_CSWM_Ouput_Test
11025                         	xref	f_ApplDescStopcheckProgrammingDependencies_FlashChecksum
11026                         	xref	f_ApplDescStopFlashErase
11027                         	xref	f_ApplDescStop_31010203_CSWM_Ouput_Test
11028                         	xref	f_ApplDescStartcheckProgrammingDependencies_FlashChecksum
11029                         	xref	f_ApplDescStartFlashErase
11030                         	xref	f_ApplDescStart_31010203_CSWM_Ouput_Test
11031                         	xref	f_ApplDescStart_31010202_Request_Echo_Routine
11032                         	xref	f_ApplDescStart_31010201_Set_Fault_Status
11033                         	xref	f_ApplDescControl_2F501603_Flash_Program_Data_4
11034                         	xref	f_ApplDescReturnControl_2F501603_Flash_Program_Data_4
11035                         	xref	f_ApplDescControl_2F501503_Flash_Program_Data_3
11036                         	xref	f_ApplDescReturnControl_2F501503_Flash_Program_Data_3
11037                         	xref	f_ApplDescControl_2F501403_Flash_Program_Data_2
11038                         	xref	f_ApplDescReturnControl_2F501403_Flash_Program_Data_2
11039                         	xref	f_ApplDescControl_2F501303_Flash_Program_Data_1
11040                         	xref	f_ApplDescReturnControl_2F501303_Flash_Program_Data_1
11041                         	xref	f_ApplDescControl_2F501203_Ignition_Status
11042                         	xref	f_ApplDescReturnControl_2F501203_Ignition_Status
11043                         	xref	f_ApplDescControl_2F501103_Load_shedding_status_BI
11044                         	xref	f_ApplDescReturnControl_2F501103_Load_shedding_status_BI
11045                         	xref	f_ApplDescControl_2F501003_Heated_Steering_Wheel_Status_BO
11046                         	xref	f_ApplDescReturnControl_2F501003_Heated_Steering_Wheel_Status_BO
11047                         	xref	f_ApplDescControl_2F500F03_F_R_Vent_Seat_Status_BO
11048                         	xref	f_ApplDescReturnControl_2F500F03_F_R_Vent_Seat_Status_BO
11049                         	xref	f_ApplDescControl_2F500E03_F_L_Vent_Seat_Status_BO
11050                         	xref	f_ApplDescReturnControl_2F500E03_F_L_Vent_Seat_Status_BO
11051                         	xref	f_ApplDescControl_2F500D03_R_R_Heat_Seat_Status_BO
11052                         	xref	f_ApplDescReturnControl_2F500D03_R_R_Heat_Seat_Status_BO
11053                         	xref	f_ApplDescControl_2F500C03_R_L_Heat_Seat_Status_BO
11054                         	xref	f_ApplDescReturnControl_2F500C03_R_L_Heat_Seat_Status_BO
11055                         	xref	f_ApplDescControl_2F500B03_F_R_Heat_Seat_Status_BO
11056                         	xref	f_ApplDescReturnControl_2F500B03_F_R_Heat_Seat_Status_BO
11057                         	xref	f_ApplDescControl_2F500A03_F_L_Heat_Seat_Status_BO
11058                         	xref	f_ApplDescReturnControl_2F500A03_F_L_Heat_Seat_Status_BO
11059                         	xref	f_ApplDescControl_2F500903_Heated_Steering_Wheel_Request_BI
11060                         	xref	f_ApplDescReturnControl_2F500903_Heated_Steering_Wheel_Request_BI
11061                         	xref	f_ApplDescControl_2F500803_F_R_Vent_Seat_Request_BI
11062                         	xref	f_ApplDescReturnControl_2F500803_F_R_Vent_Seat_Request_BI
11063                         	xref	f_ApplDescControl_2F500703_F_L_Vent_Seat_Request_BI
11064                         	xref	f_ApplDescReturnControl_2F500703_F_L_Vent_Seat_Request_BI
11065                         	xref	f_ApplDescControl_2F500603_R_R_Heat_Seat_Request_BI
11066                         	xref	f_ApplDescReturnControl_2F500603_R_R_Heat_Seat_Request_BI
11067                         	xref	f_ApplDescControl_2F500503_R_L_Heat_Seat_Request_BI
11068                         	xref	f_ApplDescReturnControl_2F500503_R_L_Heat_Seat_Request_BI
11069                         	xref	f_ApplDescControl_2F500403_F_R_Heat_Seat_Request_BI
11070                         	xref	f_ApplDescReturnControl_2F500403_F_R_Heat_Seat_Request_BI
11071                         	xref	f_ApplDescControl_2F500303_F_L_Heat_Seat_Request_BI
11072                         	xref	f_ApplDescReturnControl_2F500303_F_L_Heat_Seat_Request_BI
11073                         	xref	f_ApplDescControl_2F500203_Actuate_all_Vents
11074                         	xref	f_ApplDescReturnControl_2F500203_Actuate_all_Vents
11075                         	xref	f_ApplDescControl_2F500103_Actuate_all_heaters
11076                         	xref	f_ApplDescReturnControl_2F500103_Actuate_all_heaters
11077                         	xref	f_ApplDescWriteVIN
11078                         	xref	f_ApplDescWriteEcuSerialNumber
11079                         	xref	f_ApplDescWrite_223001_Program_ECU_Data_at_Flash_Station_2
11080                         	xref	f_ApplDescWrite_223000_Program_ECU_Data_at_Flash_Station_1
11081                         	xref	f_ApplDescWrite_222810_Heated_Seat_NTC_Cutoff_Parameters_Rear_Seats
11082                         	xref	f_ApplDescWrite_22280F_Heated_Seat_Max_Timeout_Parameters_Rear_Seats
11083                         	xref	f_ApplDescWrite_22280E_Heated_Seat_PWM_Calibration_Parameters_Rear_Seats
11084                         	xref	f_ApplDescWrite_22280C_Heated_Steering_Wheel_Min_Timeout_Parameters
11085                         	xref	f_ApplDescWrite_22280A_HeatVent_Auto_Activation_Temperature_Calibration_Paramet
11086                         	xref	f_ApplDescWrite_222809_Steering_Wheel_Temperature_Cut_off_Parameters
11087                         	xref	f_ApplDescWrite_222808_Heated_Seat_NTC_Cutoff_Parameters_Front_Seats
11088                         	xref	f_ApplDescWrite_222806_Voltage_Level_OFF
11089                         	xref	f_ApplDescWrite_222805_Voltage_Level_ON
11090                         	xref	f_ApplDescWrite_222804_Heated_Steering_Wheel_Max_Timeout_Parameters
11091                         	xref	f_ApplDescWrite_222803_Heated_Seat_Max_Timeout_Parameters_Front_Seats
11092                         	xref	f_ApplDescWrite_222802_Heated_Steering_Wheel_PWM_Calibration_Parameters
11093                         	xref	f_ApplDescWrite_222801_Vented_Seat_PWM_Calibration_Parameters
11094                         	xref	f_ApplDescWrite_222800_Heated_Seat_PWM_Calibration_Parameters_Front_Seats
11095                         	xref	f_ApplDescWrite_2E2023_System_Configuration_PROXI
11096                         	xref	f_ApplDescSendVINSendKey
11097                         	xref	f_ApplDescRequestVINRequestSeed
11098                         	xref	f_ApplDescSendLevel1Key
11099                         	xref	f_ApplDescRequestLevel1Seed
11100                         	xref	f_ApplDescSUBF_0EFaultMem
11101                         	xref	f_ApplDescSUBF_0CFaultMem
11102                         	xref	f_ApplDescSUBF_09FaultMem
11103                         	xref	f_ApplDescSUBF_08FaultMem
11104                         	xref	f_ApplDescSUBF_07FaultMem
11105                         	xref	f_ApplDescSUBF_06FaultMem
11106                         	xref	f_ApplDescSUBF_04FaultMem
11107                         	xref	f_ApplDescSUBF_02FaultMem
11108                         	xref	f_ApplDescClearFaultMem
11109                         	xref	f_ApplDescResetHardReset
11110                         	xref	f_ApplDescOnTransitionSecurityAccess
11111                         	xref	f_ApplDescOnTransitionSession
11112                         	xref	f_ApplDescRcrRpConfirmation
11113                         	xref	f_ApplDescFatalError
11114                         	xdef	f_DescEnableCommunication
11115                         	xdef	_g_descBugFixVersion
11116                         	xdef	_g_descSubVersion
11117                         	xdef	_g_descMainVersion
11118                         	xdef	f_DescIsSuppressPosResBitSet
11119                         	xdef	f_DescStateTask
11120                         	xdef	f_DescTimerTask
11121                         	xdef	f_DescTask
11122                         	xdef	f_DescInit
11123                         	xdef	f_DescInitPowerOn
11124                         	xdef	f_DescSetNegResponse
11125                         	xdef	f_DescProcessingDone
11126                         	xdef	f_DescGetSessionTimings
11127                         	xdef	f_DescGetSessionIdOfSessionState
11128                         	xdef	f_DescGetSessionStateOfSessionId
11129                         	xdef	f_DescSetStateSecurityAccess
11130                         	xdef	f_DescGetStateSecurityAccess
11131                         	xdef	f_DescSetStateSession
11132                         	xdef	f_DescGetStateSession
11133                         	xdef	f_DescStateInit
11134                         	xdef	f_DescForceRcrRpResponse
11135                         	xdef	f_DescGetCurrentBusInfo
11136                         	xdef	f_DescGetTesterAddress
11137                         	xdef	f_DescGetActivityState
11138                         	xref	f_TpFuncGetTargetAddress
11139                         	xref	f_TpFuncGetSourceAddress
11140                         	xref	f_TpFuncResetChannel
11141                         	xref	f_TpRxSetFCStatus
11142                         	xref	f_TpRxGetSourceAddress
11143                         	xref	f_TpRxResetChannel
11144                         	xref	f_TpTxSetTargetAddress
11145                         	xref	f_TpTransmit
11146                         	xdef	f_DescCopyToCAN
11147                         	xdef	f_DescTxErrorIndication
11148                         	xdef	f_DescGetBuffer
11149                         	xdef	f_DescConfirmation
11150                         	xdef	f_DescRxErrorIndication
11151                         	xdef	f_DescPhysReqInd
11152                         	xdef	f_DescFuncReqInd
11153                         	xdef	f_DescGetFuncBuffer
11154                         	xref	f_CanSetPartOnline
11155                         	xref	f_CanSetPartOffline
11156                         	xref	f_VStdResumeAllInterrupts
11157                         	xref	f_VStdSuspendAllInterrupts
11178                         	end
