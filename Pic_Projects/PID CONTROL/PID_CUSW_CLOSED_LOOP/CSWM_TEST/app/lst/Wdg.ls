   1                         ; C Compiler for S12XE [COSMIC Software].
   2                         ; Generator V4.7.8 - 27 Jul 2007
   3                         ; Optimizer V4.6.13 - 27 Jul 2007
  93                         ; 206 void Wdg_Init(const Wdg_ConfigType *ConfigPtr)
  93                         ; 207 {
  94                         	switch	.text
  95  0000                   _Wdg_Init:
  99                         ; 210     if(WDG_OFF_MODE==ConfigPtr->Wdg_Default_Mode)
 101  0000 b746              	tfr	d,y
 102  0002 e640              	ldab	0,y
 103  0004 2601              	bne	L35
 104                         ; 212         return;
 107  0006 3d                	rts	
 108  0007                   L35:
 109                         ; 215     Wdg_Trigger(); 
 111  0007 c655              	ldab	#85
 112  0009 7b0000            	stab	__ARMCOP
 115  000c c6aa              	ldab	#170
 116  000e 7b0000            	stab	__ARMCOP
 117                         ; 217 }
 120  0011 3d                	rts	
 182                         ; 238 Std_ReturnType Wdg_SetMode(WdgIf_ModeType Mode)
 182                         ; 239 {
 183                         	switch	.text
 184  0012                   _Wdg_SetMode:
 186  0012 3b                	pshd	
 187       00000000          OFST:	set	0
 190                         ; 245     if(WDGIF_OFF_MODE==Mode)
 192  0013 046104            	tbne	b,L501
 193                         ; 250          return(E_NOT_OK);
 195  0016 c601              	ldab	#1
 198  0018 31                	puly	
 199  0019 3d                	rts	
 200  001a                   L501:
 201                         ; 254     else if(WDGIF_FAST_MODE==Mode)
 203  001a c102              	cmpb	#2
 204  001c 2604              	bne	L111
 205                         ; 257          COPCTL=WDG_SETTINGS_FAST;
 207  001e c602              	ldab	#2
 209  0020 2005              	bra	LC001
 210  0022                   L111:
 211                         ; 261     else if(WDGIF_SLOW_MODE==Mode)
 213  0022 042105            	dbne	b,L701
 214                         ; 264          COPCTL=WDG_SETTINGS_SLOW;
 216  0025 c606              	ldab	#6
 217  0027                   LC001:
 218  0027 7b0000            	stab	__COPCTL
 219  002a                   L701:
 220                         ; 269     return(E_OK);
 222  002a c7                	clrb	
 225  002b 31                	puly	
 226  002c 3d                	rts	
 304                         ; 287 void Wdg_GetVersionInfo(Std_VersionInfoType *versionInfo)
 304                         ; 288 {
 305                         	switch	.text
 306  002d                   _Wdg_GetVersionInfo:
 308       00000000          OFST:	set	0
 311                         ; 291      if(NULL_PTR != versionInfo)
 313  002d 6cae              	std	2,-s
 314  002f 2715              	beq	L551
 315                         ; 295         versionInfo -> moduleID = WDG_MODULE_ID;
 317  0031 c666              	ldab	#102
 318  0033 ed80              	ldy	OFST+0,s
 319  0035 6b42              	stab	2,y
 320                         ; 298         versionInfo -> vendorID = WDG_VENDOR_ID;
 322  0037 cc0028            	ldd	#40
 323  003a 6c40              	std	0,y
 324                         ; 301         versionInfo -> sw_major_version = WDG_SW_MAJOR_VERSION_C;
 326  003c c601              	ldab	#1
 327  003e 6b43              	stab	3,y
 328                         ; 304         versionInfo -> sw_minor_version = WDG_SW_MINOR_VERSION_C;
 330  0040 6b44              	stab	4,y
 331                         ; 307         versionInfo -> sw_patch_version = WDG_SW_PATCH_VERSION_C;
 333  0042 c60a              	ldab	#10
 334  0044 6b45              	stab	5,y
 335  0046                   L551:
 336                         ; 310 }
 339  0046 31                	puly	
 340  0047 3d                	rts	
 352                         	xdef	_Wdg_GetVersionInfo
 353                         	xdef	_Wdg_SetMode
 354                         	xdef	_Wdg_Init
 355                         	xref	__ARMCOP
 356                         	xref	__COPCTL
 376                         	end
