   1                         ; C Compiler for S12XE [COSMIC Software].
   2                         ; Generator V4.7.8 - 27 Jul 2007
   3                         ; Optimizer V4.6.13 - 27 Jul 2007
  40                         ; 63 void nwmngF_Init(void)
  40                         ; 64 {
  41                         .ftext:	section	.text
  42  0000                   f_nwmngF_Init:
  46                         ; 69   CanInitPowerOn();
  48  0000 4a000000          	call	f_CanInitPowerOn
  50                         ; 71   NmInitPowerOn();
  52  0004 4a000000          	call	f_NmInitPowerOn
  54                         ; 75   TpInitPowerOn();
  57  0008 4a000000          	call	f_TpInitPowerOn
  59                         ; 77   IlInitPowerOn();
  61  000c 4a000000          	call	f_IlInitPowerOn
  63                         ; 79   NmSetNewMode(kNmNetOn);
  65  0010 cc0002            	ldd	#2
  66  0013 4a000000          	call	f_NmSetNewMode
  68                         ; 80 }
  71  0017 0a                	rtc	
  98                         ; 82 void nmCan_Cyclic_Tasks(void)
  98                         ; 83 {
  99                         	switch	.ftext
 100  0018                   f_nmCan_Cyclic_Tasks:
 104                         ; 85   DescTask();
 106  0018 4a000000          	call	f_DescTask
 108                         ; 87   TpTask();
 110  001c 4a000000          	call	f_TpTask
 112                         ; 89   IlRxTask();
 114  0020 4a000000          	call	f_IlRxTask
 116                         ; 90   IlTxTask();
 118  0024 4a000000          	call	f_IlTxTask
 120                         ; 92   NmTask();
 122  0028 4a000000          	call	f_NmTask
 124                         ; 93 }
 127  002c 0a                	rtc	
 151                         ; 94 void nmCan_Start_IL(void)
 151                         ; 95 {
 152                         	switch	.ftext
 153  002d                   f_nmCan_Start_IL:
 157                         ; 97   IlRxStart();
 159  002d 4a000000          	call	f_IlRxStart
 161                         ; 98   IlTxStart();
 163  0031 4a000000          	call	f_IlTxStart
 165                         ; 99 }
 168  0035 0a                	rtc	
 193                         ; 137 void nwmngF_State_Transition(void)
 193                         ; 138 {
 194                         	switch	.ftext
 195  0036                   f_nwmngF_State_Transition:
 199                         ; 154 	nwmngF_Get_NmStatus_ValidateBusOFF ();
 201  0036 4a005e5e          	call	f_nwmngF_Get_NmStatus_ValidateBusOFF
 203                         ; 156 	nwmngLF_NWM_CSWM_MsgUpdate ();
 205  003a 4a003f3f          	call	f_nwmngLF_NWM_CSWM_MsgUpdate
 207                         ; 157 }
 210  003e 0a                	rtc	
 247                         ; 197 void nwmngLF_NWM_CSWM_MsgUpdate (void)
 247                         ; 198 {
 248                         	switch	.ftext
 249  003f                   f_nwmngLF_NWM_CSWM_MsgUpdate:
 251  003f 37                	pshb	
 252       00000001          OFST:	set	1
 255                         ; 199 	unsigned char Nm_status = 0x00; //By default status is false. Update only in functions If the status changes.
 257  0040 87                	clra	
 258  0041 6a80              	staa	OFST-1,s
 259                         ; 202 	NmSetCurrentFailState (fmem_currentFailSts);
 261  0043 f60000            	ldab	_fmem_currentFailSts
 262  0046 4a000000          	call	f_NmSetCurrentFailState
 264                         ; 204 	NmSetGenericFailState (fmem_genericFailSts);
 266  004a f60000            	ldab	_fmem_genericFailSts
 267  004d 87                	clra	
 268  004e 4a000000          	call	f_NmSetGenericFailState
 270                         ; 205 	Nm_status = 0x01; //EOL is to be present for CUSW.
 272  0052 c601              	ldab	#1
 273  0054 6b80              	stab	OFST-1,s
 274                         ; 206 	NmSetEolState (Nm_status);
 276  0056 87                	clra	
 277  0057 4a000000          	call	f_NmSetEolState
 279                         ; 208 }
 282  005b 1b81              	leas	1,s
 283  005d 0a                	rtc	
 332                         ; 216 void nwmngF_Get_NmStatus_ValidateBusOFF(void)
 332                         ; 217 {
 333                         	switch	.ftext
 334  005e                   f_nwmngF_Get_NmStatus_ValidateBusOFF:
 336  005e 3b                	pshd	
 337       00000002          OFST:	set	2
 340                         ; 224     DisableAllInterrupts();
 343  005f 1410              	sei	
 345                         ; 226     nwm_bus_off_start_event = can_bus_off_start_b;
 348  0061 f60000            	ldab	_canio_status2_flags_c
 349  0064 55                	rolb	
 350  0065 55                	rolb	
 351  0066 55                	rolb	
 352  0067 c401              	andb	#1
 353  0069 6b80              	stab	OFST-2,s
 354                         ; 227     nwm_bus_off_end_event = can_bus_off_end_b;
 356  006b f60000            	ldab	_canio_status2_flags_c
 357  006e 55                	rolb	
 358  006f 55                	rolb	
 359  0070 c401              	andb	#1
 360  0072 6b81              	stab	OFST-1,s
 361                         ; 229     can_bus_off_start_b = FALSE;
 363                         ; 230     can_bus_off_end_b = FALSE;
 365  0074 1d0000c0          	bclr	_canio_status2_flags_c,192
 366                         ; 233     EnableAllInterrupts();
 369  0078 10ef              	cli	
 371                         ; 236     if(nwm_bus_off_start_event == TRUE)
 374  007a e680              	ldab	OFST-2,s
 375  007c 042108            	dbne	b,L501
 376                         ; 238     	if(nwm_BusOFF_counter_c < C_BUS_OFF_MAX)
 378  007f f70000            	tst	L3_nwm_BusOFF_counter_c
 379  0082 2603              	bne	L501
 380                         ; 240     		nwm_BusOFF_counter_c++;
 382  0084 720000            	inc	L3_nwm_BusOFF_counter_c
 383  0087                   L501:
 384                         ; 245     if (nwm_bus_off_end_event == TRUE)
 386  0087 e681              	ldab	OFST-1,s
 387  0089 04210e            	dbne	b,L111
 388                         ; 249 		Dio_WriteChannel(CAN_STB, STD_LOW);
 391  008c 1410              	sei	
 396  008e fe0000            	ldx	_Dio_PortWrite_Ptr
 397  0091 0d0008            	bclr	0,x,8
 401  0094 10ef              	cli	
 403                         ; 250 		canio_canbus_off_b = FALSE;
 406  0096 1d000001          	bclr	_canio_status2_flags_c,1
 407  009a                   L111:
 408                         ; 253     if(nwm_BusOFF_counter_c >= C_BUS_OFF_MAX)
 410  009a f60000            	ldab	L3_nwm_BusOFF_counter_c
 411  009d 2714              	beq	L311
 412                         ; 255     	FMemLibF_ReportDtc(DTC_CAN_INTERIOR_BUSOFF_K, ERROR_ON_K);
 414  009f cc0001            	ldd	#1
 415  00a2 3b                	pshd	
 416  00a3 ce00c0            	ldx	#192
 417  00a6 cc2000            	ldd	#8192
 418  00a9 4a000000          	call	f_FMemLibF_ReportDtc
 420  00ad 1b82              	leas	2,s
 421                         ; 256     	canio_canbus_off_b = TRUE;
 423  00af 1c000001          	bset	_canio_status2_flags_c,1
 424  00b3                   L311:
 425                         ; 259     if(canio_canbus_off_b == FALSE)
 427  00b3 1e0000010e        	brset	_canio_status2_flags_c,1,L511
 428                         ; 261     	FMemLibF_ReportDtc(DTC_CAN_INTERIOR_BUSOFF_K, ERROR_OFF_K);
 430  00b8 87                	clra	
 431  00b9 c7                	clrb	
 432  00ba 3b                	pshd	
 433  00bb ce00c0            	ldx	#192
 434  00be 8620              	ldaa	#32
 435  00c0 4a000000          	call	f_FMemLibF_ReportDtc
 437  00c4 1b82              	leas	2,s
 438  00c6                   L511:
 439                         ; 265 }
 442  00c6 31                	puly	
 443  00c7 0a                	rtc	
 467                         ; 267 void ApplNmCanNormal (void)
 467                         ; 268 {
 468                         	switch	.ftext
 469  00c8                   f_ApplNmCanNormal:
 473                         ; 270 	canio_canbus_off_b = FALSE;
 475  00c8 1d000001          	bclr	_canio_status2_flags_c,1
 476                         ; 271     nwm_BusOFF_counter_c = 0;
 478  00cc 790000            	clr	L3_nwm_BusOFF_counter_c
 479                         ; 272 }
 482  00cf 0a                	rtc	
 504                         ; 273 void ApplNmCanSleep (void)
 504                         ; 274 {
 505                         	switch	.ftext
 506  00d0                   f_ApplNmCanSleep:
 510                         ; 276 }
 513  00d0 0a                	rtc	
 538                         ; 277 void ApplNmBusOff (void)
 538                         ; 278 {
 539                         	switch	.ftext
 540  00d1                   f_ApplNmBusOff:
 544                         ; 279     IlTxWait();
 546  00d1 4a000000          	call	f_IlTxWait
 548                         ; 280     IlRxWait();
 550  00d5 4a000000          	call	f_IlRxWait
 552                         ; 281     can_bus_off_start_b = TRUE;
 554  00d9 1c000040          	bset	_canio_status2_flags_c,64
 555                         ; 284 }
 558  00dd 0a                	rtc	
 584                         ; 285 void ApplNmBusOffEnd (void)
 584                         ; 286 {
 585                         	switch	.ftext
 586  00de                   f_ApplNmBusOffEnd:
 590                         ; 287     IlTxRelease();
 592  00de 4a000000          	call	f_IlTxRelease
 594                         ; 288     IlRxRelease();
 596  00e2 4a000000          	call	f_IlRxRelease
 598                         ; 289     can_bus_off_end_b = TRUE;
 600  00e6 1c000080          	bset	_canio_status2_flags_c,128
 601                         ; 290     nwm_BusOFF_counter_c = 0;
 603  00ea 790000            	clr	L3_nwm_BusOFF_counter_c
 604                         ; 293 }
 607  00ed 0a                	rtc	
 629                         ; 298 void ApplCanWakeUp(void)
 629                         ; 299 {
 630                         	switch	.ftext
 631  00ee                   f_ApplCanWakeUp:
 635                         ; 301 }
 638  00ee 0a                	rtc	
 660                         	xdef	f_nwmngLF_NWM_CSWM_MsgUpdate
 661                         	switch	.bss
 662  0000                   L3_nwm_BusOFF_counter_c:
 663  0000 00                	ds.b	1
 664                         	xref	_Dio_PortWrite_Ptr
 665                         	xref	f_NmSetCurrentFailState
 666                         	xref	f_NmSetGenericFailState
 667                         	xref	f_NmSetEolState
 668                         	xref	f_NmSetNewMode
 669                         	xref	f_NmTask
 670                         	xref	f_NmInitPowerOn
 671                         	xref	f_FMemLibF_ReportDtc
 672                         	xref	_fmem_currentFailSts
 673                         	xref	_fmem_genericFailSts
 674                         	xref	_canio_status2_flags_c
 675                         	xdef	f_ApplNmBusOffEnd
 676                         	xdef	f_ApplNmBusOff
 677                         	xdef	f_ApplNmCanSleep
 678                         	xdef	f_ApplNmCanNormal
 679                         	xdef	f_nwmngF_Get_NmStatus_ValidateBusOFF
 680                         	xdef	f_nwmngF_State_Transition
 681                         	xdef	f_nmCan_Start_IL
 682                         	xdef	f_nmCan_Cyclic_Tasks
 683                         	xdef	f_nwmngF_Init
 684                         	xref	f_DescTask
 685                         	xref	f_IlTxTask
 686                         	xref	f_IlRxTask
 687                         	xref	f_IlTxRelease
 688                         	xref	f_IlRxRelease
 689                         	xref	f_IlTxWait
 690                         	xref	f_IlRxWait
 691                         	xref	f_IlTxStart
 692                         	xref	f_IlRxStart
 693                         	xref	f_IlInitPowerOn
 694                         	xref	f_TpTask
 695                         	xref	f_TpInitPowerOn
 696                         	xref	f_CanInitPowerOn
 697                         	xdef	f_ApplCanWakeUp
 718                         	end
