@echo off

if %ERROR_FLAG%==1 goto done

echo ***********
echo * Autosar *
echo ***********

set BASEPATH=c:\cosmic_projects\CSWM_TEST\app
set SRCPATH=%BASEPATH%\src\autosar

set SRCFILES=^
%SRCPATH%\Adc.c ^
%SRCPATH%\Adc_Cfg.c ^
%SRCPATH%\Adc_Irq.c ^
%SRCPATH%\AdcIf_Cbk.c ^
%SRCPATH%\Dio.c ^
%SRCPATH%\Dio_Cfg.c ^
%SRCPATH%\EcuM.c ^
%SRCPATH%\Gpt.c ^
%SRCPATH%\Gpt_Cfg.c ^
%SRCPATH%\Gpt_Irq.c ^
%SRCPATH%\GptIf_Cbk.c ^
%SRCPATH%\Mcu.c ^
%SRCPATH%\Mcu_Cfg.c ^
%SRCPATH%\Mcu_Irq.c ^
%SRCPATH%\Port.c ^
%SRCPATH%\Port_Cfg.c ^
%SRCPATH%\Pwm.c ^
%SRCPATH%\Pwm_Cfg.c ^
%SRCPATH%\Pwm_Irq.c ^
%SRCPATH%\PwmIf_Cbk.c ^
%SRCPATH%\RamTst.c ^
%SRCPATH%\RamTst_Algo.c ^
%SRCPATH%\RamTst_Cfg.c ^
%SRCPATH%\RamTstIf_Cbk.c ^
%SRCPATH%\Wdg.c ^
%SRCPATH%\Wdg_Cfg.c ^
%SRCPATH%\WdgIf.c

set OPTLIST=^
-f%BASEPATH%\cswm_MY12_compiler.cxf ^
-i%BASEPATH%\src\app ^
-i%BASEPATH%\src\app_paged ^
-i%BASEPATH%\src\can_nwm_pwrnet ^
-i%BASEPATH%\src\autosar ^
-i%BASEPATH%\src\flash_drv ^
-i%COSMIC_PATH%\Hs12x ^
-e -l +xe +debug +nofts -pp ^
-ce%BASEPATH%\err ^
-cl%BASEPATH%\lst ^
-co%BASEPATH%\obj

%COSMIC_PATH%\cxs12x %OPTLIST% %SRCFILES%

if ERRORLEVEL 1 set ERROR_FLAG=1

:done
