@echo off

if %ERROR_FLAG%==1 goto done

echo *********************
echo * Application Paged *
echo *********************

set BASEPATH=c:\cosmic_projects\CSWM_TEST\app
set SRCPATH=%BASEPATH%\src\app_paged

set SRCFILES=^
%SRCPATH%\BattV.c ^
%SRCPATH%\Canio.c ^
%SRCPATH%\DiagIOCtrl.c ^
%SRCPATH%\DiagMain.c ^
%SRCPATH%\DiagRead.c ^
%SRCPATH%\DiagRtnCtrl.c ^
%SRCPATH%\DiagSec.c ^
%SRCPATH%\DiagWrite.c ^
%SRCPATH%\FaultMemory.c ^
%SRCPATH%\FaultMemoryLib.c ^
%SRCPATH%\Heating.c ^
%SRCPATH%\Internal_Faults.c ^
%SRCPATH%\Network_Management.c ^
%SRCPATH%\Pwm_Stagger.c ^
%SRCPATH%\Relay.c ^
%SRCPATH%\StWheel_Heating.c ^
%SRCPATH%\Temperature.c ^
%SRCPATH%\Venting.c

set OPTLIST=^
-f%BASEPATH%\cswm_MY12_compiler.cxf ^
-i%BASEPATH%\src\app ^
-i%BASEPATH%\src\app_paged ^
-i%BASEPATH%\src\autosar ^
-i%BASEPATH%\src\can_nwm_pwrnet ^
-i%BASEPATH%\src\flash_drv ^
-i%COSMIC_PATH%\Hs12x ^
-dPWR_NET -e -l +xe +debug +modf -pp ^
-ce%BASEPATH%\err ^
-cl%BASEPATH%\lst ^
-co%BASEPATH%\obj

%COSMIC_PATH%\cxs12x %OPTLIST% %SRCFILES%

if ERRORLEVEL 1 set ERROR_FLAG=1

:done
