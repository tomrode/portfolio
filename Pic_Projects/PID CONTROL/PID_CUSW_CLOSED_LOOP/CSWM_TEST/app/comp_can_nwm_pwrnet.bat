@echo off

if %ERROR_FLAG%==1 goto done

echo ****************
echo * CAN PowerNet *
echo ****************

set BASEPATH=c:\cosmic_projects\CSWM_TEST\app
set SRCPATH=%BASEPATH%\src\can_nwm_pwrnet

set SRCFILES=^
%SRCPATH%\appdescdev.c ^
%SRCPATH%\can_drv.c ^
%SRCPATH%\can_par.c ^
%SRCPATH%\desc.c ^
%SRCPATH%\drv_par.c ^
%SRCPATH%\FNmBSl15.c ^
%SRCPATH%\il.c ^
%SRCPATH%\il_par.c ^
%SRCPATH%\nm_par.c ^
%SRCPATH%\sip_vers.c ^
%SRCPATH%\tp_par.c ^
%SRCPATH%\tpmc.c ^
%SRCPATH%\v_par.c ^
%SRCPATH%\vstdlib.c ^
%SRCPATH%\xcan_drv.c ^
%SRCPATH%\xcanshared.c 

set OPTLIST=^
-f%BASEPATH%\cswm_MY12_compiler.cxf ^
-i%BASEPATH%\src\app ^
-i%BASEPATH%\src\app_paged ^
-i%BASEPATH%\src\autosar ^
-i%BASEPATH%\src\can_nwm_pwrnet ^
-i%BASEPATH%\src\flash_drv ^
-i%COSMIC_PATH%\Hs12x ^
-e -l +xe +debug +modf -pp ^
-ce%BASEPATH%\err ^
-cl%BASEPATH%\lst ^
-co%BASEPATH%\obj

%COSMIC_PATH%\cxs12x %OPTLIST% %SRCFILES%

if ERRORLEVEL 1 set ERROR_FLAG=1

:done
