@echo off

if %ERROR_FLAG%==1 goto done

echo ***************
echo * Application *
echo ***************

set BASEPATH=c:\cosmic_projects\CSWM_TEST\app
set SRCPATH=%BASEPATH%\src\app

set SRCFILES=^
%SRCPATH%\AD_Filter.c ^
%SRCPATH%\can_isr.c ^
%SRCPATH%\main.c ^
%SRCPATH%\mc9s12xs128.c ^
%SRCPATH%\ROM_Test.c ^
%SRCPATH%\Vector.c ^
%SRCPATH%\crtsx.s

set OPTLIST=^
-f%BASEPATH%\cswm_MY12_compiler.cxf ^
-i%BASEPATH%\src\app ^
-i%BASEPATH%\src\app_paged ^
-i%BASEPATH%\src\autosar ^
-i%BASEPATH%\src\can_nwm_pwrnet ^
-i%BASEPATH%\src\flash_drv ^
-i%COSMIC_PATH%\Hs12x ^
-dRAM_TEST -dPWR_NET -e -l +xe +debug +nofts -pp ^
-ce%BASEPATH%\err ^
-cl%BASEPATH%\lst ^
-co%BASEPATH%\obj

%COSMIC_PATH%\cxs12x %OPTLIST% %SRCFILES%

if ERRORLEVEL 1 set ERROR_FLAG=1

:done

