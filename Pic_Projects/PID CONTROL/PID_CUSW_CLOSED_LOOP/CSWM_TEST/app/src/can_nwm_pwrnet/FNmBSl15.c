/*  *****   STARTSINGLE_OF_MULTIPLE    *****  */


/******************************************************************************
| Project Name: Nm_ClassB_Fiat
|
|  Description: Implementation of Fiat ClassB Network Management
|
|------------------------------------------------------------------------------
|               C O P Y R I G H T
|------------------------------------------------------------------------------
| Copyright (c) 2000-2010 by Vector Informatik GmbH.      All rights reserved.
|
|       This software is copyright protected and 
|       proprietary to Vector Informatik GmbH.
|       Vector Informatik GmbH grants to you only
|       those rights as set out in the license conditions.
|       All other rights remain with Vector Informatik GmbH.
|
|       Diese Software ist urheberrechtlich geschuetzt. 
|       Vector Informatik GmbH raeumt Ihnen an dieser Software nur 
|       die in den Lizenzbedingungen ausdruecklich genannten Rechte ein.
|       Alle anderen Rechte verbleiben bei Vector Informatik GmbH.
|
|------------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|------------------------------------------------------------------------------
| Initials     Name                      Company
| --------     ---------------------     --------------------------------------
| Bs           Thomas Balster            Vector Informatik GmbH
| dH           Gunnar de Haan            Vector Informatik GmbH
| Et           Thomas Ebert              Vector Informatik GmbH
| Th           Jochen Theuer             Vector Informatik GmbH
| Wr           Hans Waaser               Vector Informatik GmbH
| Krt          Kerstin Thim              Vector Informatik GmbH
| visoh        Oliver Hornung            Vector Informatik GmbH
|------------------------------------------------------------------------------
|               R E V I S I O N   H I S T O R Y
|------------------------------------------------------------------------------
| Date       Ver         Author   Description
| ---------  ---         ------   ----------------------------------------------------
| 09-May-00  1.00        dH       Creation
| 26-Jun-00  1.01        dH       added functions NmCanError, NmConfirmation
| 23-Oct-00  1.02        dH       added current failstate
| 30-Nov-00  1.03a       dH       added some comments(according to FIAT spec.)
|                                 changed bittype from unsigned to unsigned int
|                                 removed definition of silent time
|                                 added version constants
|                                 added ApplNmBusOff() callback
|                                 changed switch to if-else statement
|                                 moved time configuration to config header
| 14-SEP-2001  1.04      Th       adaption on VECTOR standard
| 20-NOV-2001  1.05      Th       little adaptions against compiler warnings
| 11-DEC-2001  1.06      Th       ESCAN00001902 FIAT NM15 can now be configured via GenTool
| 15-JAN-2002  1.07      Et,Th    ESCAN00002058 merge network managemnt +15 and +30,
|                                 ESCAN00002059 VECTOR standard data types
|                                 ESCAN00002019 GenTool support for +30 NM
|                                 ESCAN00002029 no check of compiler switches
|                                 ESCAN00001916 fix of SA and WU problem
|                                 ESCAN00002018 access on NM message bytes changed
|                        Bs       ESCAN00002075 assertion in event 7 deleted
|                        Bs       ESCAN00002076 event 17 added
| 24-JAN-2002  1.08      Et       ESCAN00002131 distance between NM message < SA timeout
| 25-FEB-2002  1.08.01   Th       ESCAN00002246 added support for single receive buffer
|                                 ESCAN00002373 added support for indexed CAN driver
| 07-MAR-2002  1.09      Et       ESCAN00002452 WU instead of SA (event 6)
| 11-MAR-2002            Et       ESCAN00002456 support SINGLE_RECEIVE_BUFFER
| 11-MAR-2002            Et       ESCAN00002498 #define kNmScMask 0x03  needed for clamp 15
| 15-MAR-2002  1.10      Wr       ESCAN00002501 Change bit OR to logical OR
| 17-APR-2002  1.11      Th       merging several versions
| 14-JUN-2002  1.12      Bs       ESCAN00002170 place reset of variables for slave appl 
|                                  net request and master net request in the state machine
| 07-MAY-2003  1.13      Th       ESCAN00005392 adaption on new bus off behaviour (+15 slave)
| 23-JUN-2003  1.14      Th       ESCAN00006176 calling of ApplNmBusOffEnd( ) only if state BusOff was left
| 2003-09-01   1.15      Et       ESCAN00006521 adaption on new bus off behaviour (+30 slave)
| 2003-09-10   1.16      Et       ESCAN00006598 Tx RQs in state WAIT NETWORK SILENT
|                                 ESCAN00006599 replace NM_CHANNEL_CAN... by NM_CHANNEL_NM...
| 2003-09-17   1.17.00   Bs       ESCAN00006589 master algorithm added
|                                 ESCAN00006620 pending Tx requests are cleared in state WAIT_NETWORK_SILENT
|                                 ESCAN00006621 change datatypes canuint to vuint
| 2003-10-09   1.18.00   Bs       ESCAN00006775 rename customer switch for Siemens VDO
|                                 ESCAN00006683 slave messages as response to master message must be sent within 50 ms or never
|                        Et       ESCAN00006776 support of master message AC signals
|                                 ESCAN00006781 time condition for slave response on master request
| 2003-10-20   1.19.00   Bs       ESCAN00006835 little changes to support indexed CANdriver systems
|                                 ESCAN00006785 change CAN driver API function like macro calls
|                                 ESCAN00013339 suppress not expected call of the BusOffEnd callback function
| 2003-12-01   1.20.00   Bs       ESCAN00007100 delete assertion about NmConfirmation in generated table
|                                 ESCAN00007101 encapsulation of assertion error defines has to be corrected
| 2003-12-10   1.21.00   Bs       ESCAN00007142 after reception of SLEEP command 
|                                  the slave has to stop Tx handling at once
| 2004-03-29   1.22.00   Bs       ESCAN00007035 use range precopy function for NM message reception
|                                 ESCAN00008002 support ECU addresses higher than 31
|                                 ESCAN00008043 change macro access to serve MISRA rules
|                                 ESCAN00008056 nmSendMess must be visible externally (MISRA message)
|                                 ESCAN00008162 add callback function for transistion E4
| 2004-04-21   1.23.00   Bs       ESCAN00008240 NM_TYPE_FIAT_30_MASTER define in comment can not be handled 
|                                  by Vector parser tool 'organi'
|                                 ESCAN00008243 changes just in header file
|                                 ESCAN00008244 change precopy function for slaves from range to message precopy
|                                 ESCAN00008312 encapsulate range defines in master switches
| 2006-04-04   1.24.00   Bs       file header updated
|                                 assertion in precopy function: 'kNmNoCommand' added as possible value
|                                 variable 'nmResponseTimer' is just used for Slave30 and Master algorithm
|                                 check if 'kNmSleepTime' < 'kNmBusOffRecoveryTime' for correct BusOff on sleep message
|                                 ESCAN00009956 perform BusOff recovery in state 'kNmWaitNetworkStartup' and 
|                                  'kNmWaitNetworkSilent' as well
|                                 ESCAN00010895 Naming Conventions: change version defines to serve Vector naming rules
|                                 ESCAN00013338 Clear pending Tx requests when changing into sleep mode
|                                 ESCAN00014645 call CanResetBusOffEnd when CAN is in offline mode
|                                 ESCAN00014719 rework of organi switches _COMMENT
|                                 ESCAN00015971 Master: Set AL signal in state changes and 
|                                  delete function 'NmSetMasterActiveLoad'
|                                 ESCAN00015972 BusOff timer value incremented by '1'
|                                 ESCAN00015984 Slave15: Call 'NmClrBusOffState()' function before 'CanSleep()'
|                                 ESCAN00015985 Call 'CanResetBusSleep' instead of 'CanResetBusOffStart' before 
|                                  changing into sleep mode
|                                 ESCAN00015990 call CanOnline before re-starting NM (Slave: E13 & E14, Master: E4)
|                                 ESCAN00015997 Slave15: Add callback functions 'ApplNmCanNormal' and 'ApplNmCanSleep'
| 2006-07-06   1.25.00   Bs       ESCAN00016717 Master-Algo: physical failure detection changed
|                                 ESCAN00016889 Slave15: call of CanSleep() and CanWakeup() removed
|                                 ESCAN00016890 Slave30: clear Tx-queue in event 17
| 2006-08-25   1.25.01   Krt      ESCAN00017438 Compile error for code replicated systems
| 2008-01-07   1.26.00   visoh    ESCAN00018751 Reworked assertion in precopy
|                                 ESCAN00022957 BusOff handling in NetworkSilent for Slave 30
|                                 ESCAN00023955 Optimized Slave 30 transition in case of state is Net On, a GoSleep command
|                                               is received from the Master and AL == true (Net Request is pending)
| 2008-09-16   1.27.00   visoh    ESCAN00015996 Changed memory qualifier
|                                 ESCAN00026385 Added error directive for partial offline groups
|                                 ESCAN00026888 Corrected timer handling
|                                 ESCAN00026893 Added configurable Interrupt Handling
|                                 ESCAN00030061 Adapted Fiat NM for GENy support
| 2008-12-08   1.28.00   visoh    ESCAN00031881 Added Support for "Physical Multiple ECU"
| 2009-01-30   1.29.00   visoh    ESCAN00032692 Corrected compile error for Slave15
|                                 ESCAN00032717 Improved MISRA compliance
|                                 ESCAN00032775 Corrected BusOffEnd Handling during transition to sleep (NetOff)
|                                 ESCAN00032820 Improved CAN Driver Handling
| 2010-01-26   1.30.00   visoh    ESCAN00040188 Added state transition for wakeup by NM message (Slave30)
|                                 ESCAN00040387 Corrected Interrupt Handling
| 2010-06-21   1.30.01   visoh    ESCAN00043044 Corrected CAN Interrupt Handling
|*****************************************************************************/


/*****************************************************************************/
/* Include files                                                             */
/*****************************************************************************/
#include "v_cfg.h"                    /* Include common configuration header */
#include "can_inc.h"                    /* Include CAN Driver include header */
#include "nm_cfg.h"                       /* Include NM configuration header */
#if defined ( VGEN_GENY )
#include "nm_par.h"                       /* Include NM configuration header */
#endif

#if defined( NM_TYPE_FIAT_15_SLAVE )
#include "FNmBSl15.h"                              /* Include NM header file */
#endif /* #if defined( NM_TYPE_FIAT_15_SLAVE ) */

/*****************************************************************************/
/* Version check                                                             */
/*****************************************************************************/
#if( NM_CLASSB_FIAT_VERSION != 0x0130 )
# error: "Main and/or Sub version of header and source file are inconsistent!"
#endif

#if( NM_CLASSB_FIAT_RELEASE_VERSION != 0x01 )
# error: "BugFix version of header and source file are inconsistent!"
#endif


/***************************************************************************/
/* check plausibility of driver configuration in can_cfg.h                 */
/***************************************************************************/
#if defined( C_ENABLE_EXTENDED_STATUS )
#else
# error: "NmFiatB configuration issue: CAN driver Feature Extended Status (C_ENABLE_EXTENDED_STATUS) must be enabled!"
#endif



/*****************************************************************************/
/* check tool generated defines                                              */
/*****************************************************************************/
/* check handle of NM transmit message */
#if defined ( NM_ENABLE_MULTI_ECU_PHYS )
#else
# if ( (kNmTxObj < 0) || (kNmTxObj >= kCanNumberOfTxObjects) )
#  error: "NmFiatB configuration issue: Generated handle of NM transmit message is out of range (0..kCanNumberOfTxObjects)"
# endif
#endif


/* check re-initalization object */
#if ( kNmCanPara != 0x00 )
# error: "NmFiatB configuration issue: The configured initialization object is unequal to zero,"
# error: "but Fiat supports only one baudrate on the CAN class B bus."
#endif

/* check configuration of NM call cycle */
#if defined( kNmCallCycle )
#else
# error: "NmFiatB configuration issue: No valid configuration of the NM call cycle!"
# error: "Please check configuration in generation tool and the generated configuration file"
#endif

/* task call cycle must be unequal to zero */
#if ( kNmCallCycle == 0x00 )
# error: "NmFiatB configuration issue: Configured call cycle of NmTask is zero; please correct the setting in generation tool"
#endif

/* check, if configured times can be reached by the specified call cycle */
#if( NM_TIME_STAY_ACTIVE != ((NM_TIME_STAY_ACTIVE/kNmCallCycle) * kNmCallCycle) )
# error: "NmFiatB configuration issue: StayActive time must be a multiple of the call cycle!"
# error: "Please check configuration in generation tool and the generated configuration file"
#endif

#if( NM_TIME_BUSOFF_REINIT_WAIT != ((NM_TIME_BUSOFF_REINIT_WAIT/kNmCallCycle) * kNmCallCycle) )
# error: "NmFiatB configuration issue: BusOff re-initialization time must be a multiple of the NM call cycle!"
# error: "Please check configuration in generation tool and the generated configuration file"
#endif

#if defined( NM_TYPE_FIAT_15_SLAVE )
#else
# if( NM_TIME_SLEEP != ((NM_TIME_SLEEP/kNmCallCycle) * kNmCallCycle) )
#  error: "NmFiatB configuration issue: Sleep time must be a multiple of the NM call cycle!"
#  error: "Please check configuration in generation tool and the generated configuration file"
# endif
#endif


/* check configuration of BusOff start function */
#if defined( NM_ENABLE_BUSOFF_FCT ) || defined( NM_DISABLE_BUSOFF_FCT )
#else
# error: "NmFiatB configuration issue: No valid configuration of BusOff start function - neither enabled nor disabled!"
# error: "Please check configuration in generation tool and the generated configuration file"
#endif

/* check configuration of BusOff end function */
#if defined( NM_ENABLE_BUSOFFEND_FCT ) || defined( NM_DISABLE_BUSOFFEND_FCT )
#else
# error: "NmFiatB configuration issue: No valid configuration of BusOff end function - neither enabled nor disabled!"
# error: "Please check configuration in generation tool and the generated configuration file"
#endif

/* check configuration of self testing code */
#if defined( NM_ENABLE_SOFTWARE_CHECK ) || defined( NM_DISABLE_SOFTWARE_CHECK )
#else
# error: "NmFiatB configuration issue: No valid configuration of software check / assertions - neither enabled nor disabled!"
# error: "Please check configuration in generation tool and the generated configuration file"
#endif

#if defined ( VGEN_GENY )
/* check configuration of interrupt handling */
# if defined( NM_ENABLE_NON_PREEMPTIVE ) || defined( NM_DISABLE_NON_PREEMPTIVE )
# else
#  error: "NmFiatB configuration issue: No valid configuration of non pre-emptive interrupt handling - neither enabled nor disabled!"
#  error: "Please check configuration in generation tool and the generated configuration file"
# endif
#endif



/****************************************************************************/
/* Constants                                                                */
/****************************************************************************/
/* global constants for main-, sub- and bugfix version */
V_MEMROM0 V_MEMROM1 vuint8 V_MEMROM2 kNmMainVersion   = (vuint8)(( NM_CLASSB_FIAT_VERSION ) >> 8 );
V_MEMROM0 V_MEMROM1 vuint8 V_MEMROM2 kNmSubVersion    = (vuint8)(  NM_CLASSB_FIAT_VERSION & 0x00FF );
V_MEMROM0 V_MEMROM1 vuint8 V_MEMROM2 kNmReleaseVersion = (vuint8)(  NM_CLASSB_FIAT_RELEASE_VERSION );


/*****************************************************************************/
/* Defines                                                                   */
/*****************************************************************************/
/* mask to access signals in NM messages */
#define kNmScMask        0x03  /* signal "SC" (system command) */

/* content of signal "system command" in NM message of master/slave ECUs */
#define kNmNoCommand          0xFF
#define kNmWakeUpCommand      0 /* WakeUp request */
#define kNmStayActiveCommand  2 /* StayActive request */
#define kNmGoSleepCommand     3 /* GoToSleep request */

/* value to stop network timer, busoff timer and response timer */
#define kNmTimerOff           0

/* defines for CAN HW checks */
#define kNmCanIsOk 0x01
#define kNmCanIsBusOff 0x01

/* defines for precopy functions */
#if defined( C_SINGLE_RECEIVE_BUFFER )
# define NM_PRECOPY_PARAMETER_TYPE  CanReceiveHandle
# define NM_PRECOPY_PARAMETER_NAME  rcvObject
# define NM_PRECOPY_MSG_DATA        *((vuint8*)CanRDSPtr+1)
#endif

#if defined( C_MULTIPLE_RECEIVE_BUFFER )
# define NM_PRECOPY_PARAMETER_TYPE  pChipDataPtr
# define NM_PRECOPY_PARAMETER_NAME  rxDataPtr
# define NM_PRECOPY_MSG_DATA        *(rxDataPtr+1)
#endif

#if defined( C_MULTIPLE_RECEIVE_CHANNEL )  || \
    defined( C_SINGLE_RECEIVE_CHANNEL )
# define NM_PRECOPY_PARAMETER_TYPE  CanRxInfoStructPtr
# define NM_PRECOPY_PARAMETER_NAME  rxStruct
# define NM_PRECOPY_MSG_DATA        CanRxActualData(rxStruct,1)
#endif


/***************************************************************************/
/* channel specific defines                                                */
/***************************************************************************/
#if defined( NM_TYPE_FIAT_15_SLAVE )
#else
/* calculate "Sleep Timer" value depending on the NM task call cycle */
# define kNmSleepTime (NM_TIME_SLEEP/kNmCallCycle)

#endif /* #if defined( NM_TYPE_FIAT_15_SLAVE ) */

/* calculate "Stay Active Timer" value depending on the NM task call cycle */
#if defined( NM_TYPE_FIAT_15_SLAVE )
/* => default 2000ms/kNmCallCycle according to FIAT NM specification */
#endif
#define kNmStayActiveTime (NM_TIME_STAY_ACTIVE/kNmCallCycle)


/* calculate "BusOff Recovery Timer" value depending on the NM task call cycle */
/* => default 1000ms/kNmCallCycle according to FIAT NM specification */
/* ESCAN00015972: timer must not be below 1000 ms; therfore add one call cycle */
#define kNmBusOffRecoveryTime ((NM_TIME_BUSOFF_REINIT_WAIT/kNmCallCycle)+1)

#if defined( NM_TYPE_FIAT_15_SLAVE )
#else
/* calculate "Response Timer" value depending on the NM task call cycle */
# define kNm10msResponseTime (10/kNmCallCycle)
#endif


#if defined( NM_TYPE_FIAT_15_SLAVE )
#else
/* check "Sleep Timer" value */
# if ( (kNmSleepTime > 255) || (kNmSleepTime == 0) )
#  error: "NmFiatB configuration issue: 'Sleep' Timer is out of range (1..255)!"
# endif

#endif /* #if defined( NM_TYPE_FIAT_15_SLAVE ) */

/* check "Stay Active Timer" value */
#if ( (kNmStayActiveTime > 255) || (kNmStayActiveTime == 0) )
# error: "NmFiatB configuration issue: 'Stay Alive' Timer is out of range (1..255)!"
#endif


/* check "BusOff Recovery Timer" value */
#if ( (kNmBusOffRecoveryTime > 255) || (kNmBusOffRecoveryTime == 0) )
# error: "NmFiatB configuration issue: 'BusOff Recovery' Timer is out of range (1..255)!"
#endif


#if defined ( NM_ENABLE_MULTI_ECU_PHYS )
# define kNmTxObj                     kNmTxObjField[V_ACTIVE_IDENTITY_LOG]
#endif


/***************************************************************************/
/* macros                                                                  */
/***************************************************************************/

/***************************************************************************/
/* interrupt handling                                                      */
/***************************************************************************/

/* CAN Interrupt Locking */
#if ( C_VERSION_REF_IMPLEMENTATION < 0x130 )
# define NmInterruptDisable()       CanInterruptDisable()
# define NmInterruptRestore()       CanInterruptRestore()
# define NmApiInterruptDisable()    CanInterruptDisable()
# define NmApiInterruptRestore()    CanInterruptRestore()
#else
# define NmInterruptDisable()       CanGlobalInterruptDisable()
# define NmInterruptRestore()       CanGlobalInterruptRestore()
# if defined( NM_ENABLE_NON_PREEMPTIVE )
#  define NmApiInterruptDisable()   CanCanInterruptDisable( NM_NMTOCAN_CHANNEL_IND )
#  define NmApiInterruptRestore()   CanCanInterruptRestore( NM_NMTOCAN_CHANNEL_IND )
# else
#  define NmApiInterruptDisable()   CanGlobalInterruptDisable()
#  define NmApiInterruptRestore()   CanGlobalInterruptRestore()
# endif
#endif


/*****************************************************************************/
/* Data types / enumerations                                                 */
/*****************************************************************************/
/* bit field to access copy of NM message byte 1 */
typedef struct {
#if defined( C_CPUTYPE_BITORDER_LSB2MSB )
   /* bitorder from LSB to MSB depending on microcontroller and compiler */
   canbittype SystemCommand:2;
   canbittype ActiveLoads:1;
   canbittype EndOfLineSts:1;
   canbittype GenericFailSts:1;
   canbittype CurrentFailSts:1;
   canbittype DllErrorSts:2;
#else
# if defined( C_CPUTYPE_BITORDER_MSB2LSB )
   /* bitorder from MSB to LSB depending on microcontroller and compiler */
   canbittype DllErrorSts:2;
   canbittype CurrentFailSts:1;
   canbittype GenericFailSts:1;
   canbittype EndOfLineSts:1;
   canbittype ActiveLoads:1;
   canbittype SystemCommand:2;
# else
#  error: "NmFiatB configuration issue: No bitorder (neither LSB2MSB nor MSB2LSB) is defined!"
# endif
#endif
} tsStatusInfo;

/* union to access copy of NM message byte 1 */
typedef union
/* PRQA S 0750 1 */ /* 0750 Union used for optimization reason */
{
   vuint8 value;    /* byte (array) access */
   tsStatusInfo flag; /* bit field access */
} tuStatusInfo;

/*****************************************************************************/
/* External Declarations                                                     */
/*****************************************************************************/

/*****************************************************************************/
/* global data definitions                                                   */
/*****************************************************************************/
/* data buffer for NM message of slave ECU */
#  if defined ( VGEN_GENY )
V_MEMRAM0 V_MEMRAM1_NEAR vuint8 V_MEMRAM2_NEAR nmSendMess[2];
#  else
vuint8 nmSendMess[2];
#  endif

/*****************************************************************************/
/* local data definitions                                                    */
/*****************************************************************************/
#if defined( NM_TYPE_FIAT_15_SLAVE )
/* downto counter to handle "Stay Active" counter event(s) */
#endif /* #if defined( NM_TYPE_FIAT_15_SLAVE ) */
static vuint8 nmNetworkTimer;


/* downto counter to handle "BusOff Recovery" timer event(s) */
static vuint8 nmBusOffTimer;

/* internal network management state of slave node: */
/*   - kNmPowerOff */
/*   - kNmNetOff */
/*   - kNmNetOn */
#if defined( NM_TYPE_FIAT_15_SLAVE )
#else
/*   - kNmWaitNetworkStartup */
/*   - kNmWaitNetworkSilent */
#endif
static vuint8 nmNodeState;

/* copy of NM message byte 1 for indirect signal accesses */
/* PRQA S 0759 1 */ /* 0759 Union used for optimization reason */
static tuStatusInfo nmStatusInfo;

/* reason of application network request: */
/*   - kNmPowerOff */
/*   - kNmNetOff */
/*   - kNmNetOn */
static vuint8 nmInternalStateReq;

/* reason of received message network request: */
/*   - kNmNoCommand */
/*   - kNmWakeUpCommand */
/*   - kNmStayActiveCommand */
#if defined( NM_TYPE_FIAT_15_SLAVE )
/*   - kNmGoSleepCommand */
#endif
static vuint8 nmExternalStateReq;

/* state of BusOff recovery algorithm: */
/*   - CLEAR (=0) by Tx-Int (Confirmation) */
/*   - SET (=1) by Error-Int (BusOff) */
static vuint8 nmBusOffState;

/* Tx request for NM message */
static vuint8 nmTxMsgReq;



/*****************************************************************************/
/* Constants                                                                 */
/*****************************************************************************/

/*****************************************************************************/
/* Local Function Prototypes                                                 */
/*****************************************************************************/
static void NmSetTxMsgReq( vuint8 command );

/*****************************************************************************/
/* Functions                                                                 */
/*****************************************************************************/

/*****************************************************************************/
/* local functions                                                           */
/*****************************************************************************/
/************************************************************************
| NAME:               NmSetTxMsgReq
| PROTOTYPE:          void NmSetTxMsgReq( vuint8 command )
| CALLED BY:          FIAT network management
| PRECONDITIONS:      NM intialization
| INPUT PARAMETERS:   signal "system command"
|                     - kNmWakeUpCommand
|                     - kNmStayActiveCommand
|                     - kNmGoSleepCommand
| RETURN VALUE:       none
|*************************************************************************/
static void NmSetTxMsgReq( vuint8 command )
{
  /* set data link layer (error) status: ACTIVE, WARNING or BUSOFF in NM message data bytes */
  nmStatusInfo.flag.DllErrorSts = NmGetDllState();

  /* set system command: WakeUp request or StayActive response */
  nmStatusInfo.flag.SystemCommand = command;

  nmSendMess[0] = 0; /* zero byte */
  nmSendMess[1] = nmStatusInfo.value; /* copy from INT to EXT */

  /* set Tx request of NM message */
  nmTxMsgReq = 1;
}



/*****************************************************************************/
/* global, exported functions                                                */
/*****************************************************************************/

/************************************************************************
| NAME:               NmInitPowerOn
| PROTOTYPE:          void NmInitPowerOn (void)
| CALLED BY:          application
| PRECONDITIONS:      disabled CAN (Tx, Rx and Error) interrupts
| INPUT PARAMETERS:   none
| RETURN VALUE:       none
|*************************************************************************/
void NmInitPowerOn ( void )
{
#if defined( NM_TYPE_FIAT_15_SLAVE )
  /*---------------------------------------------*/
  /* event 4:  power on intialisation (+15 ON)   */
  /* action 4: - ActiveLoads = FALSE             */
  /*           - NM_IND(NET_OFF)                 */
  /*---------------------------------------------*/
#endif

  /* call init function */
  NmInit();
}


/************************************************************************
| NAME:               NmInit
| PROTOTYPE:          void NmInit (void)
| CALLED BY:          application
| PRECONDITIONS:      disabled CAN (Tx, Rx and Error) interrupts
| INPUT PARAMETERS:   none
| RETURN VALUE:       none
|*************************************************************************/
void NmInit ( void )
{

#if defined( NM_TYPE_FIAT_15_SLAVE )
  /*---------------------------------------------*/
  /* event 4:  power on intialisation (+15 ON)   */
  /* action 4: - ActiveLoads = FALSE             */
  /*           - NM_IND(NET_OFF)                 */
  /*---------------------------------------------*/
#endif

  /* Initialize internal network management states */
  nmNodeState = kNmNetOff;
  nmInternalStateReq = kNmPowerOff; /* no application network request */
  nmExternalStateReq = kNmNoCommand; /* no external message network request */

  /* Initialize BusOff state */
  nmBusOffState = 0;

  /* Initialize Tx request state */
  nmTxMsgReq = 0;

  /* Initialize all timers */
  nmNetworkTimer = kNmTimerOff;   /* init network timer */
  nmBusOffTimer = kNmTimerOff;    /* init busoff timer */

  /* Initialize signals of NM message */
  nmStatusInfo.flag.SystemCommand = kNmGoSleepCommand;
  nmStatusInfo.flag.ActiveLoads = kNmNoActiveLoads;
  nmStatusInfo.flag.EndOfLineSts = kNmNoEolProgAtFIAT;
  nmStatusInfo.flag.GenericFailSts = kNmNoFunctionFail;
  nmStatusInfo.flag.CurrentFailSts = kNmNoFunctionFail;
  nmStatusInfo.flag.DllErrorSts = kNmErrorActive;



  /* set CAN driver to offline mode to disable Tx messages */
  CanOffline(NM_NMTOCAN_CHANNEL_IND);
  /* reset CAN hardware */
#if defined ( C_MULTIPLE_RECEIVE_CHANNEL )
  CanResetBusSleep( NM_NMTOCAN_CHANNEL_IND, kNmCanPara );
#else
  CanResetBusSleep( kNmCanPara );
#endif

  /* notify application off 'BusSleep' transition to CAN_SLEEP */
  ApplNmCanSleep();
}


#if defined( NM_TYPE_FIAT_15_SLAVE ) 
/************************************************************************
| NAME:               NmPrecopy
| PROTOTYPE:
|   SINGLE_RECEIVE_BUFFER:    vuint8 NmPrecopy ( CanReceiveHandle rcvObject )
|   MULTIPLE_RECEIVE_BUFFER:  vuint8 NmPrecopy ( pChipDataPtr ReceivedMessage )
|   XXX_RECEIVE_CHANNEL:      vuint8 NmPrecopy ( CanRxInfoStructPtr rxStruct )
| CALLED BY:          CAN driver (Rx-Interrupt)
| PRECONDITIONS:      NM initialization + enabled CAN (Rx) interrupt
| INPUT PARAMETERS:
|   SINGLE_RECEIVE_BUFFER:    index (handle) to Rx message object
|   MULTIPLE_RECEIVE_BUFFER:  pointer to Rx communication object
|   XXX_RECEIVE_CHANNEL:      pointer to info struct (CanRxInfoStructPtr)
| RETURN VALUE:       kCanNoCopyData (=> no further CAN driver handling)
|*************************************************************************/
#endif
#if defined( NM_TYPE_FIAT_15_SLAVE ) 
/* PRQA S 3673 2 */ /* 3673 API parameter is not declared const due to API is defined by another component */
/* Note that declaring API parameter to const could lead to compiler warnings / problems for some compilers */
vuint8 NmPrecopy ( NM_PRECOPY_PARAMETER_TYPE NM_PRECOPY_PARAMETER_NAME )
#endif
{
  /* local variables */
  vuint8 bLocalRxNmByte;

# if defined( V_ENABLE_USE_DUMMY_STATEMENT )
  /* PRQA S 3112 1 */ /* 3112 Dummy statement to avoid compiler warnings */
  (void)NM_PRECOPY_PARAMETER_NAME;
# endif

  /* reception of NM messages is forbidden while NM is recovering from BusOff */
  if( nmBusOffTimer == 0 )
  {
    /* get received NM command ... */
#if defined( C_MULTIPLE_RECEIVE_CHANNEL ) || defined( C_SINGLE_RECEIVE_CHANNEL )
    /* PRQA S 0488 2 */ /* 0488 Macro provided by another component */
#endif
    bLocalRxNmByte = NM_PRECOPY_MSG_DATA;
    /* set external request */
    nmExternalStateReq = bLocalRxNmByte & kNmScMask;

#if defined( NM_ENABLE_SOFTWARE_CHECK )
    /* Check for unused values */
    if( (nmExternalStateReq != kNmWakeUpCommand) && (nmExternalStateReq != kNmStayActiveCommand)
# if defined( NM_TYPE_FIAT_15_SLAVE )
      && (nmExternalStateReq != kNmGoSleepCommand)
# endif
      )
    {
      ApplNmFatalError( kNmErrorExternalStateReq );
    }
#endif


  }

  return (kCanNoCopyData); /* no further CAN driver handling */
}


/************************************************************************
| NAME:               NmCanError
| PROTOTYPE:          void NmCanError ( NM_CHANNEL_CANTYPE_ONLY )
| CALLED BY:          CAN driver (Error-Interrupt)
| PRECONDITIONS:      NM initialization + enabled CAN (Error) interrupt
| INPUT PARAMETERS:   none
| RETURN VALUE:       none
|*************************************************************************/
void NmCanError( NM_CHANNEL_CANTYPE_ONLY )
{
  /*----------------------------------------*/
  /* event 20:  BusOff detected             */
  /* action 20: - start BusOff handling     */
  /*            - set CAN driver offline    */
  /*            - initialize CAN controller */
  /*            - notify application        */
  /*----------------------------------------*/

#if defined( NM_ENABLE_SOFTWARE_CHECK )
# if defined ( C_MULTIPLE_RECEIVE_CHANNEL )
#  if defined( NM_ENABLE_INDEXED_NM )
#  else
  /* check parameter handle */
  if( NM_CHANNEL_CANPARA_ONLY != NM_NMTOCAN_CHANNEL_IND )
  {
    ApplNmFatalError( kNmErrorIllCanChnlHdl );
  }
#  endif
# endif
#endif

  /* start of BusOff recovery algorithm */
  nmBusOffState = 1;
  /* stop alive timer */
  nmNetworkTimer = kNmTimerOff;
  /* set busoff timer */
  nmBusOffTimer = kNmBusOffRecoveryTime;
  /* clear pending Tx request of slave NM message NMmXXX */
  nmTxMsgReq = 0;
  /* set data link layer (DLL) status to "BusOff" */
  nmStatusInfo.flag.DllErrorSts = kNmBusOff;

  /* set CAN driver to offline mode to disable Tx messages */
  CanOffline( NM_CHANNEL_CANPARA_ONLY );
  /* start of can driver busoff handling */
#if defined ( C_MULTIPLE_RECEIVE_CHANNEL )
  CanResetBusOffStart( NM_CHANNEL_CANPARA_ONLY, kNmCanPara );
#else
  CanResetBusOffStart( kNmCanPara );
#endif

#if defined( NM_ENABLE_BUSOFF_FCT )
  ApplNmBusOff();
#endif
}


/************************************************************************
| NAME:               NmConfirmation
| PROTOTYPE:          void NmConfirmation ( CanTransmitHandle txObject )
| CALLED BY:          CAN driver (Tx-Interrupt)
| PRECONDITIONS:      NM initialization + enabled CAN (Tx) interrupt
| INPUT PARAMETERS:   index (handle) to Tx message object
| RETURN VALUE:       none
|*************************************************************************/
void NmConfirmation ( CanTransmitHandle txObject )
{

#if defined( NM_ENABLE_SOFTWARE_CHECK )
  /* check parameter Tx handle */
  if( txObject != kNmTxObj )
  {
    ApplNmFatalError( kNmErrorIllTxConfHdl );
  }
#else
# if defined( V_ENABLE_USE_DUMMY_STATEMENT )
  /* PRQA S 3112 1 */ /* 3112 Dummy statement to avoid compiler warnings */
  (void)txObject;
# endif
#endif

#if defined( NM_ENABLE_BUSOFFEND_FCT )
  if(nmBusOffState != 0x00)
  {
    /* notify application on busoff event */
    ApplNmBusOffEnd();
  }
#endif
  /* end of BusOff recovery algorithm */
  nmBusOffState = 0;


}


/************************************************************************
| NAME:               NmTask
| PROTOTYPE:          void NmTask(void)
| CALLED BY:          Station Management / Application
| PRECONDITIONS:      NM has to be initialized
| INPUT PARAMETERS:   none
| RETURN VALUE:       none
|*************************************************************************/
void NmTask ( void )
{
  /* local variable */
  vuint8 bLocalReturnValue;


  /* ESCAN00009956 */
  /* ESCAN00022957 */
  /* node state NET_ON? */
  if( nmNodeState == kNmNetOn )
  {
    /* disable interrupts */
    NmInterruptDisable();
    /* busoff timer activated? */
    if( nmBusOffTimer != 0x00 )
    {
      /* decrement busoff timer */
      nmBusOffTimer--;

      /* busoff timer expired? */
      if( nmBusOffTimer == 0x00 )
      {
        /* end of can driver busoff handling */
#if defined ( C_MULTIPLE_RECEIVE_CHANNEL )
        CanResetBusOffEnd( NM_NMTOCAN_CHANNEL_IND,  kNmCanPara );
#else
        CanResetBusOffEnd( kNmCanPara );
#endif


        {
          /* restart network timer (stay active time) after busoff */
          nmNetworkTimer = kNmStayActiveTime + (vuint8)1;
        }
      }
      else
      {
        /* Misra violation: return function execution */
        /*  while in busoff state. Reduces if-else    */
        NmInterruptRestore();
        return;
      }
    }
    /* restore interrupts */
    NmInterruptRestore();
  }

  /* disable interrupts */
  NmInterruptDisable();


#if defined( NM_TYPE_FIAT_15_SLAVE )
  /* node state NET_ON */
  if( nmNodeState == kNmNetOn )
  {
    /*-----------------------------------------------*/
    /* event 2:  application request NM_REQ(NET_OFF) */
    /* action 2: - shutdown CAN-Controller           */
    /*           - NM_IND(NET_OFF)                   */
    /*-----------------------------------------------*/
    /* application network request? */
    if( nmInternalStateReq == kNmNetOff )
    {
      /* clear pending Tx request of NM message */  /* ESCAN00013338 */
      nmTxMsgReq = 0;
      /* clear network timer */
      nmNetworkTimer = kNmTimerOff;
      /* clear active loads (AL=FALSE) */
      nmStatusInfo.flag.ActiveLoads = kNmNoActiveLoads;
      /* set internal network management state to NET_OFF */
      nmNodeState = kNmNetOff;

      /* transition to NetOff while BusOff */  /* ESCAN00015984 */
      if( nmBusOffTimer != 0x00 )
      {
        /* clear busoff re-init timer */
        nmBusOffTimer = kNmTimerOff;
        /* end of can driver busoff handling */
# if defined ( C_MULTIPLE_RECEIVE_CHANNEL )
        CanResetBusOffEnd( NM_NMTOCAN_CHANNEL_IND,  kNmCanPara );
# else
        CanResetBusOffEnd( kNmCanPara );
# endif
# if defined( NM_ENABLE_BUSOFFEND_FCT )
      }
      if( nmBusOffState != 0 )
      {
        /* notify application on end of busoff state */
        ApplNmBusOffEnd();
# endif
        /* end of BusOff recovery algorithm */
        nmBusOffState = 0;
      }

      /* set CAN driver to offline mode */
      CanOffline(NM_NMTOCAN_CHANNEL_IND);
      /* Reset CAN Driver */
#  if defined ( C_MULTIPLE_RECEIVE_CHANNEL )
      /* ESCAN00032692 */
      CanResetBusSleep( NM_NMTOCAN_CHANNEL_IND, kNmCanPara );
#  else
      CanResetBusSleep( kNmCanPara );
#  endif

      /* notify application off 'BusSleep' transition to CAN_SLEEP */
      ApplNmCanSleep();
    }
    else
    {
      /*----------------------------------------------*/
      /* event 1:  Rx master NM message NMmBC(WU)     */
      /* action 1: - Tx slave NM message NMmXXX(SA)   */
      /*           - start 2s (StayActive) SA-Timer   */
      /*           - NM_IND(NET_ON)                   */
      /*----------------------------------------------*/
      /* application network request? */
      if( nmExternalStateReq == kNmStayActiveCommand )
      {
        /* set Tx request of slave NM message NMmXXX (StayActive request) */
        NmSetTxMsgReq( kNmStayActiveCommand );
        /* set network timer (stay active time) */
        nmNetworkTimer = kNmStayActiveTime + (vuint8)1;
      }

      /*----------------------------------------------*/
      /* event 1:  (StayActive) SA-Timer Timeout      */
      /* action 1: - Tx slave NM message NMmXXX(SA)   */
      /*           - start 2s (StayActive) SA-Timer   */
      /*           - NM_IND(NET_ON)                   */
      /*----------------------------------------------*/
      /* decrement network timer (is always active) */
      nmNetworkTimer--;
      /* network timer expired? */
      if( nmNetworkTimer == 0x00 )
      {
        /* set Tx request of slave NM message NMmXXX (StayActive request) */
        NmSetTxMsgReq( kNmStayActiveCommand );
        /* set network timer (stay active time) */
        nmNetworkTimer = kNmStayActiveTime;
      }
    }
  }
  else
  {
    /*----------------------------------------------*/
    /* event 3:  application request NM_REQ(NET_ON) */
    /* action 3: - initialise CAN-Controller        */
    /*           - ActiveLoads = TRUE               */
    /*           - Tx slave NM message NMmXXX(SA)   */
    /*           - start 2s (StayActive) SA-Timer   */
    /*           - NM_IND(NET_STARTUP)              */
    /*----------------------------------------------*/
    /* node state NET_ON */
    if( nmInternalStateReq == kNmNetOn )
    {
      /* set network timer (stay active time) */
      nmNetworkTimer = kNmStayActiveTime;
      /* set active loads (AL=TRUE) */
      nmStatusInfo.flag.ActiveLoads = kNmActiveLoads;
      /* set internal network management state to NET_On */
      nmNodeState = kNmNetOn;

      /* set CAN driver to online mode to enable Tx path */
      CanOnline(NM_NMTOCAN_CHANNEL_IND);
      /* set Tx request of slave NM message NMmXXX (WakeUp request) */
      NmSetTxMsgReq( kNmWakeUpCommand );

      /* notify application of 'WakeUp' transition to CAN_NORMAL */
      ApplNmCanNormal();
    }
  }
  nmInternalStateReq = kNmPowerOff; /* no slave application network request */
  nmExternalStateReq = kNmNoCommand; /* no master message network request */
#endif /* #if defined( NM_TYPE_FIAT_15_SLAVE ) */

  /* get Tx request of NM message */
  if( nmTxMsgReq != 0x00 )
  {
    if(nmBusOffState != 0)
    {
      /* set CAN driver to online mode to enable Tx path */
      /* This happens after a busoff when the slave response */
      /* message must be sent first. CanPartOffline inhibits the */
      /* transmission of application messages in the "after- */
      /* busoff" time.                                       */
      CanOnline(NM_NMTOCAN_CHANNEL_IND);
    }
    /* send slave NM message NMmXXX */    
    bLocalReturnValue = CanTransmit(kNmTxObj);

    if( bLocalReturnValue == kCanTxOk )
    {
      /* clear Tx request of NM message */
      nmTxMsgReq = 0;
    }
  }

  /* restore interrupts */
  NmInterruptRestore();

  /* PRQA S 2006 1 */ /* 2006 Using second return path for optimization reasons */
} /* PRQA S 4700 */ /* 4700 Metric data for information only */
  /* PRQA S 4700 */ /* 4700 Metric data for information only */

#if defined( NM_TYPE_FIAT_15_SLAVE ) 
/************************************************************************
| NAME:               NmSetNewMode
| PROTOTYPE:          void NmSetNewMode ( vuint8 state )
| CALLED BY:          application
| PRECONDITIONS:      NM initialization
| INPUT PARAMETERS:   changed node status:
|                     - kNmNetOn
|                     - kNmNetOff
| RETURN VALUE:       none
|*************************************************************************/
#endif
void NmSetNewMode ( vuint8 state )
{

#if defined( NM_ENABLE_SOFTWARE_CHECK )
  /* check parameter 'state' */
  if( (state != kNmNetOn) && (state != kNmNetOff)
    )
  {
    ApplNmFatalError( kNmErrorInternalStateReq );
  }
#endif

#if defined( NM_ENABLE_NON_PREEMPTIVE )
#else
  /* disable global interrupts */
  NmApiInterruptDisable();
#endif

  /* set internal request by application interface */
  nmInternalStateReq = state;

#if defined( NM_ENABLE_NON_PREEMPTIVE )
#else
  /* restore global interrupts */
  NmApiInterruptRestore();
#endif
}

/************************************************************************
| NAME:               NmSetEolState
| PROTOTYPE:          void NmSetEolState ( vuint8 state )
| CALLED BY:          application
| PRECONDITIONS:      NM initialization
| INPUT PARAMETERS:   changed end of line status:
|                     - kNmNoEolProgAtFIAT
|                     - kNmEolProgAtFIAT
| RETURN VALUE:       none
|*************************************************************************/
void NmSetEolState ( vuint8 state )
{
#if defined( NM_ENABLE_SOFTWARE_CHECK )
  /* check parameter 'state' */
  if( (state != kNmNoEolProgAtFIAT)
    && (state != kNmEolProgAtFIAT) )
  {
    ApplNmFatalError( kNmErrorSetEolState );
  }
#endif

  /* disable interrupts */
  NmApiInterruptDisable();

  /* set bit in NM message */
  nmStatusInfo.flag.EndOfLineSts = state;

  /* restore interrupts */
  NmApiInterruptRestore();
}

/************************************************************************
| NAME:               NmSetGenericFailState
| PROTOTYPE:          void NmSetGenericFailState ( vuint8 state )
| CALLED BY:          application
| PRECONDITIONS:      NM initialization
| INPUT PARAMETERS:   changed generic fail state:
|                     - kNmNoFunctionFail
|                     - kNmFunctionFail
| RETURN VALUE:       none
|*************************************************************************/
void NmSetGenericFailState ( vuint8 state )
{
#if defined( NM_ENABLE_SOFTWARE_CHECK )
  /* check parameter 'state' */
  if( (state != kNmNoFunctionFail)
    && (state != kNmFunctionFail) )
  {
    ApplNmFatalError( kNmErrorSetGenFailState );
  }
#endif

  /* disable interrupts */
  NmApiInterruptDisable();

  /* set bit in NM message */
  nmStatusInfo.flag.GenericFailSts = state;

  /* restore interrupts */
  NmApiInterruptRestore();
}

/************************************************************************
| NAME:               NmSetCurrentFailState
| PROTOTYPE:          void NmSetCurrentFailState ( vuint8 state )
| CALLED BY:          application
| PRECONDITIONS:      NM initialization
| INPUT PARAMETERS:   changed current fail state:
|                     - kNmNoFunctionFail
|                     - kNmFunctionFail
| RETURN VALUE:       none
|*************************************************************************/
void NmSetCurrentFailState ( vuint8 state )
{
#if defined( NM_ENABLE_SOFTWARE_CHECK )
  /* check parameter 'state' */
  if( (state != kNmNoFunctionFail) && (state != kNmFunctionFail) )
  {
    ApplNmFatalError( kNmErrorSetCurrFailState );
  }
#endif

  /* disable interrupts */
  NmApiInterruptDisable();

  /* set bit in NM message */
  nmStatusInfo.flag.CurrentFailSts = state;

  /* restore interrupts */
  NmApiInterruptRestore();
}

/************************************************************************
| NAME:               NmGetDllState
| PROTOTYPE:          vuint8 NmGetDllState ( void )
| CALLED BY:          application, NM Task
| PRECONDITIONS:      NM initialization
| INPUT PARAMETERS:   none
| RETURN VALUE:       data link layer (DLL) status
|                     - kNmErrorActive
|                     - kNmErrorPassive
|                     - kNmBusOff
|*************************************************************************/
vuint8 NmGetDllState ( void )
{
  vuint8 dllStatus;

  /* disable interrupts */
  NmApiInterruptDisable();

  /* active BusOff recovery algorithm? */
  if( nmBusOffState != 0 )
  {
    /* set data link layer (DLL) status of NM to "BusOff" */
    dllStatus = kNmBusOff;
  }
  else
  {
    /* get data link layer (DLL) status of CAN */
    dllStatus = CanGetStatus(NM_NMTOCAN_CHANNEL_IND);

    /* PRQA S 3344 1 */ /* 3344 Macro provided by another component */
    if( CanHwIsOk(dllStatus) == kNmCanIsOk )
    {
      /* set data link layer (DLL) status of NM to "Active" */
      dllStatus = kNmErrorActive;
    }
    else
    {
      /* PRQA S 3344 1 */ /* 3344 Macro provided by another component */
      if( CanHwIsBusOff(dllStatus) == kNmCanIsBusOff )
      {
        /* set data link layer (DLL) status of NM to "BusOff" */
        dllStatus = kNmBusOff;
      }
      else
      {
        /* set data link layer (DLL) status of NM to "Passive" */
        dllStatus = kNmErrorPassive;
      }
    }
  }

  /* restore interrupts */
  NmApiInterruptRestore();

  return dllStatus;
}

#if defined( NM_TYPE_FIAT_15_SLAVE )
/************************************************************************
| NAME:               NmGetNetState
| PROTOTYPE:          vuint8 NmGetNetState ( void )
| CALLED BY:          application
| PRECONDITIONS:      initialization
| INPUT PARAMETERS:   none
| RETURN VALUE:       current node state:
|                     - kNmNetOff
|                     - kNmNetOn
|*************************************************************************/
#endif
vuint8 NmGetNetState ( void )
{
  return nmNodeState;
}











/*******************************************************************************
* Organi check
*******************************************************************************/

/*  *****   STOPSINGLE_OF_MULTIPLE    *****  */
/************   Organi, Version 3.9.0 Vector-Informatik GmbH  ************/
