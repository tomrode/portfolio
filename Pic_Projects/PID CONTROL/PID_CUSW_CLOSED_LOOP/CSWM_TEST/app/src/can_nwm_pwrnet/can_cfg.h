/* -----------------------------------------------------------------------------
  Filename:    can_cfg.h
  Description: Toolversion: 03.01.09.01.01.01.51.01.00.00
               
               Serial Number: CBD1010151
               Customer Info: Continental_Temic
                              Package: Chrysler Fiat_Sgl_SLP3_ClassB_Clmp15 
                              Micro: Freescale MC9S12XS128 
                              Compiler: Cosmic 4.7.8
               
               
               Generator Fwk   : GENy 
               Generator Module: DrvCan__base
               
               Configuration   : H:\SCM_DCX_CSWM_MY_12\Can_Nwm_PwrNet\cmd\CUSW_SLP3_1138DBC_DDT60_009_101211.gny
               
               ECU: 
                       TargetSystem: Hw_Mcs12xCpu
                       Compiler:     Cosmic
                       Derivates:    MCS12X
               
               Channel "Channel0":
                       Databasefile: H:\SCM_DCX_CSWM_MY_12\Documents\chrysler-Data_dict\PPF_E2B_1138_BCAN.dbc
                       Bussystem:    CAN
                       Manufacturer: Fiat
                       Node:         CSWM

  Generated by , 2011-12-01  08:37:04
 ----------------------------------------------------------------------------- */
/* -----------------------------------------------------------------------------
  C O P Y R I G H T
 -------------------------------------------------------------------------------
  Copyright (c) 2001-2010 by Vector Informatik GmbH. All rights reserved.
 
  This software is copyright protected and proprietary to Vector Informatik 
  GmbH.
  
  Vector Informatik GmbH grants to you only those rights as set out in the 
  license conditions.
  
  All other rights remain with Vector Informatik GmbH.
 -------------------------------------------------------------------------------
 ----------------------------------------------------------------------------- */

#if !defined(__CAN_CFG_H__)
#define __CAN_CFG_H__

#include "v_cfg.h"
#define HW_MCS12XMSCANCPUCANDLL_VERSION      0x0213
#define HW_MCS12XMSCANCPUCANDLL_RELEASE_VERSION 0x00

#define HW__BASECPUCANDLL_VERSION            0x0222
#define HW__BASECPUCANDLL_RELEASE_VERSION    0x04

#define DRVCAN__BASEDLL_VERSION              0x0321
#define DRVCAN__BASEDLL_RELEASE_VERSION      0x01

#define DRVCAN__BASERI14DLL_VERSION          0x0209
#define DRVCAN__BASERI14DLL_RELEASE_VERSION  0x01

#define DRVCAN__BASERI15DLL_VERSION          0x0104
#define DRVCAN__BASERI15DLL_RELEASE_VERSION  0x06

#define DRVCAN__BASEHLLDLL_VERSION           0x0305
#define DRVCAN__BASEHLLDLL_RELEASE_VERSION   0x01

#define DRVCAN__BASERI14HLLDLL_VERSION       0x0205
#define DRVCAN__BASERI14HLLDLL_RELEASE_VERSION 0x01

#define DRVCAN__BASERI15HLLDLL_VERSION       0x0101
#define DRVCAN__BASERI15HLLDLL_RELEASE_VERSION 0x03

#define CAN_DRV_MCS12X_MSCANDLL_VERSION      0x0301
#define CAN_DRV_MCS12X_MSCANDLL_RELEASE_VERSION 0x02


#define kCanNumberOfChannels                 1
#define kCanNumberOfHwChannels               1
#define kCanNumberOfPhysChannels             1
#define C_DISABLE_MEMCOPY_SUPPORT
#define C_DISABLE_OSEK_OS
#define C_DISABLE_VARIABLE_DLC
#define C_DISABLE_DLC_FAILED_FCT
#define C_DISABLE_VARIABLE_RX_DATALEN
#define C_DISABLE_MULTI_ECU_CONFIG
#define C_DISABLE_MULTI_ECU_PHYS
#define C_ENABLE_EXTENDED_ID
#define C_DISABLE_MIXED_ID
#define C_DISABLE_RECEIVE_FCT

#define C_DISABLE_ECU_SWITCH_PASS
#define C_ENABLE_TRANSMIT_QUEUE
#define C_DISABLE_OVERRUN
#define C_DISABLE_INTCTRL_BY_APPL
#define C_DISABLE_COMMON_CAN
#define C_DISABLE_USER_CHECK
#define C_DISABLE_HARDWARE_CHECK
#define C_DISABLE_GEN_CHECK
#define C_DISABLE_INTERNAL_CHECK
#define C_DISABLE_DYN_RX_OBJECTS
#define C_ENABLE_DYN_TX_OBJECTS
#define C_ENABLE_DYN_TX_ID
#define C_ENABLE_DYN_TX_DLC
#define C_DISABLE_DYN_TX_DATAPTR
#define C_DISABLE_DYN_TX_PRETRANS_FCT
#define C_DISABLE_DYN_TX_CONF_FCT
#define C_ENABLE_EXTENDED_STATUS
#define C_DISABLE_TX_OBSERVE
#define C_DISABLE_HW_LOOP_TIMER
#define C_DISABLE_NOT_MATCHED_FCT
#define C_SECURITY_LEVEL                     30

#define C_DISABLE_MULTICHANNEL_API
#define C_ENABLE_PART_OFFLINE
#define C_ENABLE_RANGE_0
#define C_ENABLE_RANGE_1
#define C_DISABLE_RANGE_2
#define C_DISABLE_RANGE_3
#define ApplCanBusOff                        NmCanError

#define ApplCanRange0Precopy                 TpPrecopy
#define ApplCanRange1Precopy                 TpFuncPrecopy
#define kCanNumberOfTxObjects                6
#define kCanNumberOfTxStatObjects            5
#define kCanNumberOfTxDynObjects             1
#define kCanNumberOfRxObjects                14
#define kCanNumberOfRxStatFullCANObjects     0
#define kCanNumberOfRxStatBasicCANObjects    14
#define kCanNumberOfRxDynFullCANObjects      0
#define kCanNumberOfRxDynBasicCANObjects     0
#define kCanNumberOfRxDynObjects             0
#define kCanNumberOfRxStatObjects            14
#define kCanNumberOfConfFlags                3
#define kCanNumberOfIndFlags                 2
#define kCanNumberOfConfirmationFlags        1
#define kCanNumberOfIndicationFlags          1
#define kCanNumberOfInitObjects              1
#define kCanExtNumberOfInitObjects           0
#define kCanHwRxDynFullStartIndex            4
#define C_SEARCH_LINEAR

#define C_ENABLE_RX_MSG_INDIRECTION

#define C_ENABLE_CONFIRMATION_FLAG
#define C_ENABLE_INDICATION_FLAG
#define C_DISABLE_PRETRANSMIT_FCT
#define C_ENABLE_CONFIRMATION_FCT
#define C_ENABLE_INDICATION_FCT
#define C_ENABLE_PRECOPY_FCT
#define C_ENABLE_COPY_TX_DATA
#define C_ENABLE_COPY_RX_DATA
#define C_ENABLE_DLC_CHECK
#define C_DISABLE_DLC_CHECK_MIN_DATALEN

#define C_ENABLE_GENERIC_PRECOPY
#define APPL_CAN_GENERIC_PRECOPY             IlCanGenericPrecopy

#define C_SEND_GRP_NONE                      0x00
#define C_SEND_GRP_ALL                       0xFF
#define C_SEND_GRP_APPLICATION               0x01
#define C_SEND_GRP_NETMANAGEMENT             0x02
#define C_SEND_GRP_USER2                     0x04
#define C_SEND_GRP_USER3                     0x08
#define C_SEND_GRP_USER4                     0x10
#define C_SEND_GRP_USER5                     0x20
#define C_SEND_GRP_USER6                     0x40
#define C_SEND_GRP_USER7                     0x80
#define C_ENABLE_CAN_CANCEL_NOTIFICATION
#define APPL_CAN_CANCELNOTIFICATION          IlCanCancelNotification

#define kCanPhysToLogChannelIndex_0
#define C_RANGE0_ACC_MASK                    0x0FF
#define C_RANGE1_ACC_MASK                    0xFFFF

#define C_RANGE0_ACC_CODE                    0x18DAC200
#define C_RANGE1_ACC_CODE                    0x18DB0000

#define C_DISABLE_RX_FULLCAN_OBJECTS
#define C_ENABLE_RX_BASICCAN_OBJECTS
#define kCanNumberOfRxFullCANObjects         0

#define kCanNumberOfRxBasicCANObjects        14
#define kCanNumberOfUsedRxBasicCANObjects    1

#define kCanInitObj1                         0
#define C_DISABLE_TX_MASK_EXT_ID
#define C_DISABLE_RX_MASK_EXT_ID
#define C_MASK_EXT_ID                        0xFFFFFFFF

#define C_ENABLE_CAN_CAN_INTERRUPT_CONTROL
#define C_DISABLE_CAN_TX_CONF_FCT

#define C_DISABLE_TX_POLLING
#define C_DISABLE_RX_BASICCAN_POLLING
#define C_DISABLE_RX_FULLCAN_POLLING
#define C_DISABLE_ERROR_POLLING
#define C_DISABLE_WAKEUP_POLLING
#define C_DISABLE_FULLCAN_OVERRUN
#define C_DISABLE_OSEK_OS_INTCAT2
#define C_DISABLE_COPY_RX_DATA_WITH_DLC
#define kCanTxQueueBytes                     2
#define kCanNumberOfMaxBasicCAN              2
#define kCanNumberOfHwObjPerBasicCan         1
#define C_DISABLE_CAN_RAM_CHECK
#define C_ENABLE_SLEEP_WAKEUP
#define C_DISABLE_CANCEL_IN_HW
#define C_DISABLE_ONLINE_OFFLINE_CALLBACK_FCT

#define C_RANGE0_IDTYPE                      kCanIdTypeExt
#define C_RANGE1_IDTYPE                      kCanIdTypeExt
#define kCanChannel_Channel0                 0
#define C_DISABLE_INTCTRL_ADD_CAN_FCT
#if defined(C_SINGLE_RECEIVE_BUFFER) || defined(C_MULTIPLE_RECEIVE_BUFFER)
#error "DrvCan__baseRI1.5 doesn't support Single/Multiple Receive Buffer API for the callback 'ApplCanMsgReceived'!"
#endif

#define kCanNumberOfUsedCanTxIdTables        1
#define kCanNumberOfUsedCanRxIdTables        1
#define kCanHwTxStartIndex                   1

#define kCanHwUnusedStartIndex               2

#define kCanHwRxFullStartIndex               4

#define kCanHwRxBasicStartIndex              0

#define kCanNumberOfUsedTxCANObjects         1

#define kCanNumberOfUnusedObjects            2

#define kCanNumberOfTxDirectObjects          0

#define C_DISABLE_TX_FULLCAN_OBJECTS


#define kCanHwTxNormalIndex                  1

#define C_DISABLE_INDIVIDUAL_POLLING
#define kCanNumberOfHwObjIndivPolling        2

#define C_ENABLE_HW_CHANNEL_0
#define kCanBasisAdr                         0x0140
#define kCanNumberOfHwMaxTxCANObjects        1
#define kCanNumberOfHwMaxReceiveObjects      1
#define C_DISABLE_XGATE_USED
#define C_DISABLE_ISR_SPLIT
#define C_DISABLE_XGATE_PRECOPY_FCT



/* begin Fileversion check */
#ifndef SKIP_MAGIC_NUMBER
#ifdef MAGIC_NUMBER
  #if MAGIC_NUMBER != 82318335
      #error "The magic number of the generated file <H:\SCM_DCX_CSWM_MY_12\Documents\GENy Generated\1138_DDT_009\can_cfg.h> is different. Please check time and date of generated files!"
  #endif
#else
  #define MAGIC_NUMBER 82318335
#endif  /* MAGIC_NUMBER */
#endif  /* SKIP_MAGIC_NUMBER */

/* end Fileversion check */

#endif /* __CAN_CFG_H__ */
