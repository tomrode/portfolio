/* Kernbauer Version: 1.16 Konfiguration: can_driver Erzeugungsgangnummer: 1 */

/* STARTSINGLE_OF_MULTIPLE */


/*****************************************************************************
| Project Name: DrvCan_Mcs12xMscanHll
|    File Name: CAN_DRV.C
|
|  Description: Implementation of the CAN driver
|               Target systems: Freescale MCS12
|                               Freescale MCS12X
|                               Freescale MCS08
|                               Freescale MPC5200
|                               
|               Compiler:       Cosmic
|                               Metrowerks
|                               DiabData
|                               IAR
|                               QCC
|
|-----------------------------------------------------------------------------
|               C O P Y R I G H T
|-----------------------------------------------------------------------------
| Copyright (c) 1996-2009 by Vector Informatik GmbH.       All rights reserved.
|
| This software is copyright protected and proprietary 
| to Vector Informatik GmbH. Vector Informatik GmbH 
| grants to you only those rights as set out in the 
| license conditions. All other rights remain with 
| Vector Informatik GmbH.
|
|-----------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-----------------------------------------------------------------------------
| Initials     Name                      Company
| --------     ---------------------     -------------------------------------
| Ml           Patrick Markl             Vector Informatik GmbH
| Ces          Senol Cendere             Vector Informatik GmbH
| Ou           Mihai Olariu              Vector Informatik GmbH
| Ht           Heike Honert              vector Informatik GmbH
|-----------------------------------------------------------------------------
|               R E V I S I O N   H I S T O R Y
|-----------------------------------------------------------------------------
| Date       Version   Author  Description
| ---------  --------  ------  -----------------------------------------------
| 2004-05-27 00.01.00    Ml     Creation 
| 2004-10-14 01.00.00    Ml     Added support for Metrowerks
| 2004-11-21 01.01.00    Ml     ESCAN00010294: temporary CIDAC is stored channel specific
|                        Ml     ESCAN00010295: no changes
| 2004-11-28 01.02.00    Ml     ESCAN00010461: IRQ request is restored properly
|                        Ml     ESCAN00010572: Resolved corrupted Rx data
| 2005-05-26 01.03.00    Ml     ESCAN00013824: Resolved compiler error
| 2005-08-26             Msr/Ml ESCAN00013351: Added synchronization
| 2005-11-01 02.00.00    Ml     ESCAN00014262: Resolved extended range handling
| 2005-12-14 02.01.00    Ml     ESCAN00014687: no changes
|                        Ml     ESCAN00014414: Fixed setting of CTL0 register
|                        Ml     ESCAN00014769: Unneeded bits are masked out for std ranges
|                        Ml     ESCAN00014791: no changes
| 2006-01-14 02.02.00    Ml     ESCAN00014907: no changes
| 2006-05-03 02.03.00    Ml     ESCAN00016237: Changed names of ISRs
| 2006-07-10 02.04.00    Ml     ESCAN00016840: Added support for cancel in hardware
| 2006-07-17 03.00.00    Ml     ESCAN00017189: Added support for Mcs08 family
|                        Ml     ESCAN00017134: Corrected  register setting for wakeup and overrun polling
|                        Ml     ESCAN00017257: no changes
|                        Ml     ESCAN00017166: Substituted missing CAN_GET_RANGESTATUS macro
| 2006-08-13 04.00.00    Ml     ESCAN00017700: Added Mpc5200 platform
| 2006-09-19 04.01.00    Ml     ESCAN00017435: mailbox is not switched in case of passive mode
| 2006-11-30 04.02.00    Ces    ESCAN00018590: Support Autosar
| 2006-12-06 04.03.00    Ml     ESCAN00018636: no changes
|                        Ml     ESCAN00018770: Corrected support for indexed single channel API
|                        Ces    ESCAN00018927: Support IAR compiler
|                        Ces    ESCAN00018928: Support Multi ECU configuration
|                        Ces    ESCAN00018930: Support RAM check
|                        Ces    ESCAN00018931: Wrong Tx data pointer used in pretransmit function
| 2007-02-13 04.04.00    Ml     ESCAN00019569: Changed macros of ISRs in order to work with Autosar OS
|                        Ml     ESCAN00019678: Remote frames are checked in the correct message buffer
|                        Ml     ESCAN00019688: ApplCanTxConfirmation passes valid data in case of passive mode
|                        Ces    ESCAN00019730: Adaptions for Autosar
| 2007-03-06 04.05.00    Ou     ESCAN00019728: Update for the multi ECU configuration support in XGate
| 2007-03-09 04.06.00    Ml     ESCAN00019894: Removed parts for XGate transmission
| 2007-03-15 04.07.00    Ml     ESCAN00019949: Change the handling of interrupts
| 2007-04-03 04.08.00    Ces    ESCAN00020177: Adaptions for Autosar
| 2007-04-09 05.00.00    Ou     ESCAN00020104: Upgrade to RI 1.5
|                        Ou     ESCAN00020163: Apply the erratum workaround regarding "SSEM instruction"
| 2007-08-10 05.01.00    Ces    Support Microsar Revision 2.1
| 2007-09-04 05.02.00    Ou     ESCAN00020861: Error handling MCS12: wrong report of warning/ error passive state
|                        Ou     ESCAN00021695: Messages received in range(s) are not processed
|                        Ou     ESCAN00021724: Unexpected call to application CAN Tx object confirmed/ CAN Tx confirmation
| 2007-09-13 05.03.00    Ou     Add XGate support for Microsar
| 2008-01-09 05.04.00    Ou     ESCAN00022514: Interrupts don't work with OSEK OS
|                        Ou     ESCAN00022567: Message not yet sent is confirmed (CanRxInterrupt() interrupts CanTxTask())
|                        Ou     ESCAN00022855: Message not yet sent is confirmed ((CanTxTask() interrupts CanTransmit())
|                        Ou     ESCAN00022867: Message is corrupted when normal and low-level transmission interrupts each other
|                        Ou     ESCAN00023651: Message is corrupted when Tx confirmation interrupts either normal or low-level transmission (MCS12X HLL)
|                        Ou     ESCAN00023567: ISRs should not be mapped in the default section for non-banked routines
|                        Ou     ESCAN00025429: CAN interrupts are not correctly disabled
| 2008-04-15 05.05.00    Ou     ESCAN00026446: Support AUTOSAR multiplexed Tx feature
| 2008-08-22 05.06.00    Ou     ESCAN00029451: Support XGate for ASR3.0
| 2008-09-22 05.07.00    Ou     ESCAN00030206: Wrong Tx confirmation call after startup
|                        Ou     ESCAN00030207: Wrong hardware handle is passed to the CAN interface on indication
|                        Ou     ESCAN00030948: CAN interrupts are not correctly and fully disabled/ enabled
| 2008-10-20 05.08.00    Ou     ESCAN00031712: Use code template version 2.09
| 2009-01-26 05.09.00    Ou     ESCAN00032566: CANbedded only: Upgrade Mpc5x00 to RI 1.5
|                        Ou     ESCAN00032660: CANbedded only: CAN ISRs do not secure the GPAGE register when the global variables are activated
|                        Ou     ESCAN00032661: CAN ISRs do not secure the GPAGE register when the global constants are activated
| 2009-04-07 05.10.00    Ou     ESCAN00034463: CANbedded only: The XGate Cosmic compiler does not recognize the "@svpage" keyword
| 2009-05-05 05.11.00    Ou     ESCAN00034899: CANbedded only: Provide support for global variables (RI 1.5)
| 2009-06-04 05.11.01    Ht     support new core version
| 2009-07-31 05.11.02    Ou     ESCAN00034572: The default CAN clock input should be the quartz/ oscillator
|                        Ou     ESCAN00036785: CANbedded only: Support QNX
|                        Ou     ESCAN00036786: CANbedded only: Support QCC compiler for Mpc5x00
|                        Ou     ESCAN00036787: CANbedded only: compiler failure because missing of CANRANGExIDTYPE
|***************************************************************************/
/*****************************************************************************
|
|    ************    Version and change information of      **********        
|    ************    high level part only                   **********        
|
|    Please find the version number of the whole module in the previous 
|    File header.
|
|-----------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-----------------------------------------------------------------------------
| Initials     Name                      Company
| --------     ---------------------     -------------------------------------
| Ht           Heike Honert              Vector Informatik GmbH
| Pl           Georg Pfluegel            Vector Informatik GmbH
| Vg           Frank Voorburg            Vector CANtech, Inc.
| An           Ahmad Nasser              Vector CANtech, Inc.
| Ml           Patrick Markl             Vector Informatik GmbH
|-----------------------------------------------------------------------------
|               R E V I S I O N   H I S T O R Y
|-----------------------------------------------------------------------------
| Date       Ver  Author  Description
| ---------  ---  ------  ----------------------------------------------------
| 19-Jan-01  0.02  Ht     - derived form C16x V3.3
| 18-Apr-01  1.00  Pl     - derived for ARM7 TDMI
| 02-May-01  1.01  Ht     - adaption to LI1.2
|                         - change from code doupling to indexed
| 31-Oct-01  1.02  Ht     - support hash search
|                  Ht     - optimisation for message access (hardware index)
|                  Vg     - adaption for PowerPC
| 07-Nov-01  1.03  Ht     - remove some comments
|                         - support of basicCAN polling extended
| 12-Dez-01  1.04  Ht     - avoid compiler warnings for KEIL C166
|                         - ESCAN00001881: warning in CanInitPowerOn
|                         - ESCAN00001913: call of CanLL_TxEnd()
|                  Fz     - ESCAN00001914: CanInterruptRestore changed for M32C
| 02-Jan-02  1.05  Ht     - ESCAN00002009: support of polling mode improved
|                         - ESCAN00002010: Prototype of CanHL_TxConfirmation() 
|                                          not available in every case.
| 12-Feb-02  1.06 Pl      - ESCAN00002279: - now it is possible to use only the error-task without the tx-task
|                                          - support of the makro  REENTRANT
|                                          - support of the makro C_HL_ENABLE_RX_INFO_STRUCT_PTR
|                                          - For better performance for the T89C51C there is a switch-case
|                                            instruction for direct call of the PreTransmitfunction
|                                          - add C_ENABLE_RX_BASICCAN_POLLING in CanInitPowerOn
| 18-Mai-02  1.07 Ht      - ESCAN....    : support Hash search with FullCAN controller
|                         - ESCAN00002707: Reception could went wrong if IL and Hash Search
|                         - ESCAN00002893: adaption to LI 1.3
| 29-Mai-02  1.08 Ht      - ESCAN00003028: Transmission could fail in Polling mode
|                         - ESCAN00003082: call Can_LL_TxEnd() in CanMsgTransmit()
|                         - ESCAN00003083: Hash search with extended ID
|                         - ESCAN00003084: Support C_COMP_METROWERKS_PPC
|                         - ESCAN00002164: Temporary vaiable "i" not defined in function CanBasicCanMsgReceived
|                         - ESCAN00003085: support C_HL_ENABLE_IDTYPE_IN_ID
| 25-Jun     1.08.01 Vg   - Declared localInterruptOldFlag in CanRxTask()
|                         - Corrected call to CanWakeUp for multichannel
| 11-Jul-02  1.08.02 Ht   - ESCAN00003203: Hash Search routine does not work will with extended IDs
|                         - ESCAN00003205: Support of ranges could went wrong on some platforms
| 08-Aug-02  1.08.03 Ht   - ESCAN00003447: Transmission without databuffer and pretransmit-function 
| 08-Aug-02  1.08.04 An   - Added support to Green Hills
| 09-Sep-02  1.09    Ht   - ESCAN00003837: code optimication with KernelBuilder
|                         - ESCAN00004479: change the place oft the functioncall of CanLL_TxCopyMsgTransmit
|                                          in CanMsgTransmit 
| 2002-12-06 1.10    Ht   -                Support consistancy for polling tasks
|                         - ESCAN00004567: Definiton of V_NULL pointer
|                         -                remove include of string.h
|                         -                support Dummy functions for indirect function call 
|                         -                optimization for one single Tx mail box
| 2003-02-04 1.11    Ht   -                optimization for polling mode
| 2003-03-19 1.12    Ht   - ESCAN00005152: optimization of CanInit() in case of Direct Tx Objects
|                         - ESCAN00005143: CompilerWarning about function prototype 
|                                          CanHL_ReceivedRxHandle() and CanHL_IndRxHandle
|                         - ESCAN00005130: Wrong result of Heash Search on second or higher channel               
| 2003-05-12 1.13    Ht   - ESCAN00005624: support CantxMsgDestroyed for multichannel system
|                         - ESCAN00005209: Support confirmation and indication flags for EasyCAN4
|                         - ESCAN00004721: Change data type of Handle in CanRxInfoStruct
| 2003-06-18 1.20   Ht    - ESCAN00005908: support features of RI1.4
|                         - ESCAN: Support FJ16LX-Workaround for multichannel system
|                         - ESCAN00005671: Dynamic Transmit Objects: ID in extended ID frames is wrong
|                         - ESCAN00005863: Notification about cancelation failes in case of CanTxMsgDestroyed
| 2003-06-30 1.21   Ht   - ESCAN00006117: Common Confirmation Function: Write access to wrong memory location
|                        - ESCAN00006008: CanCanInterruptDisable in case of polling
|                        - ESCAN00006118: Optimization for Mixed ID and ID type in Own Register or ID type in ID Register
|                        - ESCAN00006100: transmission with wrong ID in mixed ID mode
|                        - ESCAN00006063: Undesirable hardware dependency for 
|                                         CAN_HL (CanLL_RxBasicTxIdReceived)
| 2003-09-10 1.22   Ht   - ESCAN00006853: Support V_MEMROM0
|                        - ESCAN00006854: suppress some Compiler warnings
|                        - ESCAN00006856: support CanTask if only Wakeup in polling mode
|                        - ESCAN00006857: variable newDLC not defined in case of Variable DataLen
| 2003-10-14 1.23   Ht   - ESCAN00006858: support BrsTime for internal runtime measurement
|                        - ESCAN00006860: support Conditional Msg Receive
|                        - ESCAN00006865: support "Cancel in HW" with CanTask
|                        - ESCAN00006866: support Direct Tx Objects
|                        - ESCAN00007109: support new memory qualifier for const data and pointer to const
| 2004-01-05 1.24   Ml   - ESCAN00007206: resolved preprocessor error for Hitachi compiler
|                   Ml   - ESCAN00007254: several changes
| 2004-02-06 1.25   Ml   - ESCAN00007281: solved compilerwarning
|                   Ml   - removed compiler warnings
| 2004-02-21 1.26   Ml   - ESCAN00007670: CAN RAM check
|                   Ml   - ESCAN00007671: fixed dyn Tx object issue
|                   Ml   - ESCAN00007764: added possibility to adjust Rx handle in LL drv
|                   Ml   - ESCAN00007681: solved compilerwarning in CanHL_IndRxHandle
|                   Ml   - ESCAN00007272: solved queue transmission out of LowLevel object
|                   Ml   - ESCAN00008064: no changes
| 2004-04-16 1.27   Ml   - ESCAN00008204: no changes
|                   Ml   - ESCAN00008160: no changes
|                   Ml   - ESCAN00008266: changed name of parameter of function CanTxGetActHandle
|                   Fz   - ESCAN00008272: Compiler error due to missing array canPollingTaskActive
| 2004-05-10 1.28   Fz   - ESCAN00008328: Compiler error if cancel in hardware is active
|                        - ESCAN00008363: Hole closed when TX in interrupt and cancel in HW is used
|                        - ESCAN00008365: Switch C_ENABLE_APPLCANPREWAKEUP_FCT added
|                        - ESCAN00008391: Wrong parameter macro used in call of 
|                                         CanLL_WakeUpHandling
| 2004-05-24 1.29   Ht   - ESCAN00008441: Interrupt not restored in case of internal error if TX Polling is used
| 2004-09-21 1.30   Ht   - ESCAN00008914: CAN channel may stop transmission for a certain time
|                        - ESCAN00008824: check of reference implementation version added
|                        - ESCAN00008825: No call of ApplCanMsgCancelNotification during CanInit()
|                        - ESCAN00008826: Support asssertions for "Conditional Message Received"  
|                   Ml   - ESCAN00008752: Added function qualifier macros
|                   Ht   - ESCAN00008823: compiler error due to array size 0
|                        - ESCAN00008977: label without instructions
|                        - ESCAN00009485: Message via Normal Tx Object will not be sent  
|                        - ESCAN00009497: support of CommonCAN and RX queue added
|                        - ESCAN00009521: Inconsitancy in total polling mode
| 2004-09-28 1.31   Ht   - ESCAN00009703: unresolved functions CAN_POLLING_IRQ_DISABLE/RESTORE()
| 2004-11-25 1.32   Ht   - move fix for ESCAN00007671 to CAN-LL of DrvCan_MpcToucanHll
|                        - ESCAN00010350: Dynamic Tx messages are send always with Std. ID
|                        - ESCAN00010388: ApplCanMsgConfirmed will only be called if realy transmitted
|                    Ml  - ESCAN00009931: The HardwareLoopCheck should have a channelparameter in multichannel systems.
|                    Ml  - ESCAN00010093: lint warning: function type inconsistent "CanCheckMemory"
|                    Ht  - ESCAN00010811: remove Misra and compiler warnings
|                        - ESCAN00010812: support Multi ECU
|                        - ESCAN00010526: CAN interrupts will be disabled accidently
|                        - ESCAN00010584: ECU may crash or behave strange with Rx queue active
| 2005-01-20 1.33    Ht  - ESCAN00010877: ApplCanMsgTransmitConf() is called erronemous
| 2005-03-03 1.34    Ht  - ESCAN00011139: Improvement/Correction of C_ENABLE_MULTI_ECU_CONFIG
|                        - ESCAN00011511: avoid PC-Lint warnings
|                        - ESCAN00011512: copy DLC in case of variable Rx Datalen
|                        - ESCAN00010847: warning due to missing brakets in can_par.c at CanChannelObject
| 2005-05-23 1.35   Ht   - ESCAN00012445: compiler error "V_MEMROMO undefined"in case of multi ECU
|                        - ESCAN00012350: Compiler Error "Illegal token channel"
| 2005-07-06 1.36   Ht   - ESCAN00012153: Compile Error: missing declaration of variable i
|                        - ESCAN00012460: Confirmation of LowLevel message will run into assertion (C_ENABLE_MULTI_ECU_PHYS enabled)
|                        - support Testpoints for CanTestKit
| 2005-07-14 1.37   Ht   - ESCAN00012892: compile error due to missing logTxObjHandle
|                        - ESCAN00012998: Compile Error: missing declaration of txHandle in CanInit()
|                        - support Testpoints for CanTestKit for FullCAN controller
| 2005-09-21 1.38   Ht   - ESCAN00013597: Linker error: Undefined symbol 'CanHL_IndRxHandle'
| 2005-11-10 1.39.00 Ht  - ESCAN00014331: Compile error due to missing 'else' in function CanTransmit
|
| 2005-04-26 2.00.00  Ht - ESCAN00016698: support RI1.5
|                        - ESCAN00014770: Cosmic compiler reports truncating assignement
|                        - ESCAN00016191: Compiler warning about unused variable in CanTxTask
|
| 2007-01-23 2.01.00  Ht - ESCAN00017279: Usage of SingleGlobalInterruptDisable lead to assertion in OSEK
|                        - ESCAN00017148: Compile error in higher layer due to missing declaration of CanTxMsgHandleToChannel
| 2007-03-14 2.02.00  Ht - ESCAN00019825: error directives added and misra changes
|                        - ESCAN00019827: adaption to never version of VStdLib.
|                        - ESCAN00019619: V_CALLBACK_1 and V_CALLBACK_2 not defined
|                        - ESCAN00019953: Handling of FullCAN reception in interrupt instead of polling or vice versa.
|                        - ESCAN00019958: CanDynTxObjSetId() or CanDynTxObjSetExtId() will run into assertion
| 2007-03-26 2.03.00  Ht - ESCAN00019988: Compile warnings in can_drv.c
|                        - ESCAN00018831: polling mode: function prototype without function implemenation (CanRxFullCANTask + CanRxBasicCANTask)
| 2007-04-20 2.04.00  dH - ESCAN00020299: user assertion fired irregularly due to unknown parameter (in case of CommonCAN)
| 2007-05-02 2.05.00  Ht - ESCAN00021069: Handling of canStatus improved, usage of preprocessor defines unified
|                        - ESCAN00021070: support relocation of HW objects in case of Multiple configuration
|                        - ESCAN00021166: Compiler Warnings: canHwChannel & canReturnCode not used in CanGetStatus
|                        - ESCAN00021223: CanCanInterruptDisabled called during Sleepmode in CanWakeupTask
|                        - ESCAN00022048: Parameter of ApplCancorruptMailbox is hardware channel instead of logical channel - Error directive added
| 2007-11-12 2.06.00  Ht - ESCAN00023188: support CAN Controller specific polling sequence for BasicCAN objects
|                        - ESCAN00023208: Compile issue about undefined variable kCanTxQueuePadBits in the CAN driver in Bit based Tx queue
| 2008-10-20 2.07.00  Ht - ESCAN00023010: support disabled mailboxes in case of extended RAM check
|                        - ESCAN00025706: provide C_SUPPORTS_MULTI_ECU_PHYS
|                        - ESCAN00026120: compiler warnings found on DrvCan_V85xAfcanHll RI 1.5     ##Ht: reviewed 2008-09-03
|                        - ESCAN00026322: ApplCanMsgNotMatched not called in special configuration
|                        - ESCAN00026413: Add possibility to reject remote frames received by FullCAN message objects
|                        - ESCAN00028758: CAN HL must support QNX
|                        - ESCAN00029788: CommonCAN for Driver which support only one Tx object improved (CanInit()).
|                        - ESCAN00029889: Compiler warning about uninitialized variable canHwChannel in CanCanInterruptDisable/Restore()
|                        - ESCAN00029891: Compiler warning: variable "rxHandle" was set but never used
|                        - ESCAN00029929: Support Extended ID Masking for Tx Fullcan messages 
|                        - ESCAN00030371: Improvements (assertions, misra)
|                        - ESCAN00027931: Wrong check of "queueBitPos" size
| 2009-04-08 2.08.00  Ht - ESCAN00034492: no automatic remove of CanCanInterruptDisable/Restore
|                        - ESCAN00031816: CANRANGExIDTYPE can be removed and direct expression used
|                        - ESCAN00032027: CanMsgTransmit shall support tCanMsgTransmitStruct pointer accesses to far RAM
|                        - ESCAN00034488: Postfix for unsigned const in perprocessor directives are not supported by all Compiler (ARM-Compiler 1.2)
| 2009-06-04 2.08.01  Ht - ESCAN00035426: Compiler warning about truncation in CanGetStatus removed
|
|
|
|
|    ************    Version and change information of      **********        
|    ************    high level part only                   **********        
|
|    Please find the version number of the whole module in the previous 
|    File header.
|
|***************************************************************************/

#define C_DRV_INTERNAL

/* PRQA S 3453 EOF */      /* suppress messages: a function should probably used instead of function like macro. misra 19.7 */
/* PRQA S 3109 EOF */      /* suppress messages about empty statements. misra 14.3 */
/* PRQA S 2006 EOF */      /* suppress misra message about multiple return. misra 14.7 */


/*lint -function(exit,ApplCanFatalError)*/

/***************************************************************************/
/* Include files                                                           */
/***************************************************************************/

#include "can_inc.h"


/***************************************************************************/
/* Version check                                                           */
/***************************************************************************/
#if( DRVCAN_MCS12XMSCANHLL_VERSION         != 0x0511u)
# error "Source and Header file are inconsistent!"
#endif

#if( DRVCAN_MCS12XMSCANHLL_RELEASE_VERSION != 0x02u)
# error "Source and Header file are inconsistent!"
#endif


#if( C_VERSION_REF_IMPLEMENTATION != 0x150)
# error "Generated Data and CAN driver source file are inconsistent!"
#endif

#if( DRVCAN__COREHLL_VERSION != 0x0208)
# error "Source and Header file are inconsistent!"
#endif
#if( DRVCAN__COREHLL_RELEASE_VERSION != 0x01)
# error "Source and Header file are inconsistent!"
#endif

#if ( ( DRVCAN__HLLTXQUEUEBIT_VERSION != 0x0106) )
# error "TxQueue Source and Header Version inconsistent!"
#endif
#if ( ( DRVCAN__HLLTXQUEUEBIT_RELEASE_VERSION != 0x00) )
# error "TxQueue Source and Header Version inconsistent!"
#endif


#if defined( DRVCAN__HLLTXQUEUEBIT_VERSION )
# if ( ( DRVCAN__HLLTXQUEUEBIT_VERSION != 0x0106) || \
       ( DRVCAN__HLLTXQUEUEBIT_RELEASE_VERSION != 0x00)  )
#  error "TxQueue Version inconsistent!"
# endif

/* defines to satisfy MISRA checker tool */
# define DRVCAN__HLLTXQUEUEBYTE_VERSION 0x0000
# define DRVCAN__HLLTXQUEUEBYTE_RELEASE_VERSION 0x00

#else
# if defined( DRVCAN__HLLTXQUEUEBYTE_VERSION )
#  if ( ( DRVCAN__HLLTXQUEUEBYTE_VERSION != 0x0103) || \
       ( DRVCAN__HLLTXQUEUEBYTE_RELEASE_VERSION != 0x00)  )
#   error "TxQueue Version inconsistent!"
#  endif
# else
#  error "No TxQueue available"
# endif

/* defines to satisfy MISRA checker tool */
# define DRVCAN__HLLTXQUEUEBIT_VERSION 0x0000
# define DRVCAN__HLLTXQUEUEBIT_RELEASE_VERSION 0x00

#endif


/***************************************************************************/
/* Defines                                                                 */
/***************************************************************************/


/* ##RI-1.1: Object parameters for Tx-Observe functions */
/* status of transmit objects */
#define kCanBufferFree                                     ((CanTransmitHandle)0xFFFFFFFFU)   /* mark a transmit object is free */
#define kCanBufferCancel                                   ((CanTransmitHandle)0xFFFFFFFEU)   /* mark a transmit object as canceled */
#define kCanBufferMsgDestroyed                             ((CanTransmitHandle)0xFFFFFFFDU)   /* mark a transmit object as destroyed */
/* reserved value because of definition in header:    0xFFFFFFFCU */
/* valid transmit message handle:   0x0 to kCanNumberOfTxObjects   */

/* return values */ 
#define kCanHlFinishRx                                     ((vuint8)0x00)
#define kCanHlContinueRx                                   ((vuint8)0x01)

#define  CANHL_TX_QUEUE_BIT
/* Define chiphardware                     */
/* Constants concerning can chip registers */
/* control registers for msg objects       */
#if defined(C_COMP_COSMIC_MCS12X_MSCAN12)  
/* Bit mask of CAN-Register CTL0:                                                                                                */
# define INITRQ                                                ((vuint8)0x01)    /* initialized mode request                     */
# define SLPRQ                                                 ((vuint8)0x02)    /* sleep request, go to internal sleep mode     */
# define WUPE                                                  ((vuint8)0x04)    /* wakeup enable                                */
# define TIMER                                                 ((vuint8)0x08)    /* timer enable                                 */
# define SYNCH                                                 ((vuint8)0x10)    /* synchronized status                          */
# define CSWAI                                                 ((vuint8)0x20)    /* CAN stops in wait mode                       */
# define RXACT                                                 ((vuint8)0x40)    /* receiver active                              */
# define RXFRM                                                 ((vuint8)0x80)    /* received frame flag                          */

/* Bit mask of CAN-Register CTL1:                                                                                                */
# define INITAK                                                ((vuint8)0x01)    /* initialized mode ack                         */
# define SLPAK                                                 ((vuint8)0x02)    /* sleep mode acknowledge                       */
# define WUPM                                                  ((vuint8)0x04)    /* wakeup mode                                  */
# define LISTEN                                                ((vuint8)0x10)    /* listen only                                  */
# define LOOPB                                                 ((vuint8)0x20)    /* loopback mode                                */
# define CLKSRC                                                ((vuint8)0x40)    /* clock source                                 */
# define CANE                                                  ((vuint8)0x80)    /* CAN enabled                                  */

/* Bit masks of CAN receiver flag register CRFLG:                                                                                */
# define WUPIF                                                 ((vuint8)0x80)    /* wakeup interrupt flag                        */
# define CSCIF                                                 ((vuint8)0x40)    /* CAN status change interrupt flag             */
# define RSTAT1                                                ((vuint8)0x20)    /* receiver status bit 1 flag                   */
# define RSTAT0                                                ((vuint8)0x10)    /* receiver status bit 0 flag                   */
# define TSTAT1                                                ((vuint8)0x08)    /* transmitter status bit 1 flag                */
# define TSTAT0                                                ((vuint8)0x04)    /* transmitter status bit 0 flag                */
# define OVRIF                                                 ((vuint8)0x02)    /* overrun interrupt flag                       */
# define RXF                                                   ((vuint8)0x01)    /* receive buffer full flag                     */
# define BOFFIF                                                ((vuint8)(TSTAT0 | TSTAT1))  /* Status : bus-off */

/* Bit masks of CAN receiver interrupt enable register CRIER:                                                                    */
# define WUPIE                                                 ((vuint8)0x80)    /* wakeup interrupt enable                      */
# define CSCIE                                                 ((vuint8)0x40)    /* CAN status change interrupt enable           */
# define RSTAT1E                                               ((vuint8)0x20)    /* receiver status bit 1 enable                 */
# define RSTAT0E                                               ((vuint8)0x10)    /* receiver status bit 0 enable                 */
# define TSTAT1E                                               ((vuint8)0x08)    /* transmitter status bit 1 enable              */
# define TSTAT0E                                               ((vuint8)0x04)    /* zransmitter status bit 0 enable              */
# define OVRIE                                                 ((vuint8)0x02)    /* overrun interrupt enable                     */
# define RXE                                                   ((vuint8)0x01)    /* receive buffer full enable                   */

/* Bit masks of CAN transmitter abort request register CTARQ:                                                                    */
# define ABTRQ2                                                ((vuint8)0x04)    /* abort request on Tx buffer 2                 */
# define ABTRQ1                                                ((vuint8)0x02)    /* abort request on Tx buffer 1                 */
# define ABTRQ0                                                ((vuint8)0x01)    /* abort request on Tx buffer 0                 */

/* Bit masks of CAN transmitter abort request register CTAAK:                                                                    */
# define ABTAK2                                                ((vuint8)0x04)    /* abort ack on Tx buffer 2                     */
# define ABTAK1                                                ((vuint8)0x02)    /* abort ack on Tx buffer 1                     */
# define ABTAK0                                                ((vuint8)0x01)    /* abort ack on Tx buffer 0                     */

/* Bit masks of CAN transmit buffer selection register CTBSEL:                                                                   */
# define TX2                                                   ((vuint8)0x04)    /* select Tx buffer 2                           */
# define TX1                                                   ((vuint8)0x02)    /* select Tx buffer 1                           */
# define TX0                                                   ((vuint8)0x01)    /* select Tx buffer 0                           */

/* Bit masks of CAN miscellaneous Register CMISC:                                                                                */
# define BOHOLD                                                ((vuint8)0x01)    /* bus-off on user request                      */

/* Bit masks in CRIER */
# define CRIER_RX_MASK                                         ((vuint8)0x01)
# define CRIER_ERROR_MASK                                      ((vuint8)0x7E)
# define CRIER_WAKEUP_MASK                                     ((vuint8)0x80)

/* Bit masks needed by XGate */
# if defined(C_COMP_COSMIC_MCS12X_MSCAN12)  
#  define CANXGSEM                                             (*(volatile vuint16*)(V_REG_BLOCK_ADR + 0x380 + 0x1A)) /* XGATE semaphore register */
#  define CANXGMCTL                                            (*(volatile vuint16*)(V_REG_BLOCK_ADR + 0x380 + 0x00)) /* XGATE module control register */


#  define kCanCanSemaphoreMask                                 ((vuint16)0x0001)
#  define kCanCheckCanSemaphore                                ((vuint16)0x0000)
#  define kCanSetCanSemaphore                                  ((vuint16)0x0101)
#  define kCanClearCanSemaphore                                ((vuint16)0x0100)
#  define kCanDisableXGateInterrupts                           ((vuint16)0x0100)
#  define kCanRestoreXGateInterrupts                           ((vuint16)0x0101)

#  if defined(C_ENABLE_XGATE_USED)
#   define kCanCRIERInterruptsDisabledVal                      ((vuint8)1) /* let the interrupt be processed in XGate when the message is received */
#   define CanBeginCriticalXGateSection()                      do{ CANXGSEM = kCanSetCanSemaphore; } while((CANXGSEM & kCanCanSemaphoreMask) == kCanCheckCanSemaphore)
#   define CanEndCriticalXGateSection()                        CANXGSEM = kCanClearCanSemaphore
#   define CanDisableXGateInterrupts()                         if(canCanXGateIrqtCount == 0) { CANXGMCTL = kCanDisableXGateInterrupts; } canCanXGateIrqtCount++
#   define CanRestoreXGateInterrupts()                         canCanXGateIrqtCount--; if(canCanXGateIrqtCount == 0) { CANXGMCTL = kCanRestoreXGateInterrupts; }
#  else
#   define kCanCRIERInterruptsDisabledVal                      ((vuint8)0)
#   define CanBeginCriticalXGateSection()
#   define CanEndCriticalXGateSection()
#   define CanDisableXGateInterrupts()
#   define CanRestoreXGateInterrupts()
#  endif
# else
#  define kCanCRIERInterruptsDisabledVal                       ((vuint8)0)
#  define CanBeginCriticalXGateSection()
#  define CanEndCriticalXGateSection()
#  define CanDisableXGateInterrupts()
#  define CanRestoreXGateInterrupts()
# endif

/* Miscellaneous */
#  if defined(C_COMP_COSMIC_MCS12X_MSCAN12)  
#   define kCanCellSize                                        ((vuint16)0x40)
#  endif
#  define kCanCRIEROnlyRx                                      ((vuint8)0x01)

#endif

/***************************************************************************/
/* macros                                                                  */
/***************************************************************************/

#if !(defined( C_HL_DISABLE_RX_INFO_STRUCT_PTR ) || defined( C_HL_ENABLE_RX_INFO_STRUCT_PTR ))
# define C_HL_ENABLE_RX_INFO_STRUCT_PTR
#endif

#if defined( C_HL_ENABLE_RX_INFO_STRUCT_PTR )
# define CAN_HL_P_RX_INFO_STRUCT(channel)                  (pCanRxInfoStruct)  
# define CAN_HL_P_RX_INFO_STRUCT_HANDLE(channel)           (pCanRxInfoStruct->Handle)  
#else
# define CAN_HL_P_RX_INFO_STRUCT(channel)                  (&canRxInfoStruct[channel])
# define CAN_HL_P_RX_INFO_STRUCT_HANDLE(channel)           (canRxInfoStruct[channel].Handle)  
#endif 


/*disabled - lint -emacro( (572,778), C_RANGE_MATCH) */


#if defined( C_SINGLE_RECEIVE_CHANNEL )
# if (kCanNumberOfUsedCanRxIdTables == 1)
/* Msg(4:3410) Macro parameter not enclosed in (). MISRA Rule 96 - no change */
#  define C_RANGE_MATCH_STD( CAN_RX_IDRAW_PARA, mask, code) \
                  (  ((idRaw0) & (tCanRxId0)~MK_RX_RANGE_MASK_IDSTD0(mask)) == MK_RX_RANGE_CODE_IDSTD0(code) )
#  define C_RANGE_MATCH_EXT( CAN_RX_IDRAW_PARA, mask, code) \
                  (  ((idRaw0) & (tCanRxId0)~MK_RX_RANGE_MASK_IDEXT0(mask)) == MK_RX_RANGE_CODE_IDEXT0(code) )
# endif
# if (kCanNumberOfUsedCanRxIdTables == 2)
/* Msg(4:3410) Macro parameter not enclosed in (). MISRA Rule 96 - no change */
#  define C_RANGE_MATCH_STD( CAN_RX_IDRAW_PARA, mask, code) \
                  ( ( ((idRaw0) & (tCanRxId0)~ MK_RX_RANGE_MASK_IDSTD0(mask)) == MK_RX_RANGE_CODE_IDSTD0(code) ) && \
                    ( ((idRaw1) & (tCanRxId1)~ MK_RX_RANGE_MASK_IDSTD1(mask)) == MK_RX_RANGE_CODE_IDSTD1(code) ) )
#  define C_RANGE_MATCH_EXT( CAN_RX_IDRAW_PARA, mask, code) \
                  ( ( ((idRaw0) & (tCanRxId0)~ MK_RX_RANGE_MASK_IDEXT0(mask)) == MK_RX_RANGE_CODE_IDEXT0(code) ) && \
                    ( ((idRaw1) & (tCanRxId1)~ MK_RX_RANGE_MASK_IDEXT1(mask)) == MK_RX_RANGE_CODE_IDEXT1(code) ) )
# endif
# if (kCanNumberOfUsedCanRxIdTables == 3)
/* Msg(4:3410) Macro parameter not enclosed in (). MISRA Rule 96 - no change */
#  define C_RANGE_MATCH_STD( CAN_RX_IDRAW_PARA, mask, code) \
                  ( ( ((idRaw0) & (tCanRxId0)~ MK_RX_RANGE_MASK_IDSTD0(mask)) == MK_RX_RANGE_CODE_IDSTD0(code) ) && \
                    ( ((idRaw1) & (tCanRxId1)~ MK_RX_RANGE_MASK_IDSTD1(mask)) == MK_RX_RANGE_CODE_IDSTD1(code) ) && \
                    ( ((idRaw2) & (tCanRxId2)~ MK_RX_RANGE_MASK_IDSTD2(mask)) == MK_RX_RANGE_CODE_IDSTD2(code) ) )
#  define C_RANGE_MATCH_EXT( CAN_RX_IDRAW_PARA, mask, code) \
                  ( ( ((idRaw0) & (tCanRxId0)~ MK_RX_RANGE_MASK_IDEXT0(mask)) == MK_RX_RANGE_CODE_IDEXT0(code) ) && \
                    ( ((idRaw1) & (tCanRxId1)~ MK_RX_RANGE_MASK_IDEXT1(mask)) == MK_RX_RANGE_CODE_IDEXT1(code) ) && \
                    ( ((idRaw2) & (tCanRxId2)~ MK_RX_RANGE_MASK_IDEXT2(mask)) == MK_RX_RANGE_CODE_IDEXT2(code) ) )
# endif
# if (kCanNumberOfUsedCanRxIdTables == 4)
/* Msg(4:3410) Macro parameter not enclosed in (). MISRA Rule 96 - no change */
#  define C_RANGE_MATCH_STD( CAN_RX_IDRAW_PARA, mask, code) \
                  ( ( ((idRaw0) & (tCanRxId0)~ MK_RX_RANGE_MASK_IDSTD0(mask)) == MK_RX_RANGE_CODE_IDSTD0(code) ) && \
                    ( ((idRaw1) & (tCanRxId1)~ MK_RX_RANGE_MASK_IDSTD1(mask)) == MK_RX_RANGE_CODE_IDSTD1(code) ) && \
                    ( ((idRaw2) & (tCanRxId2)~ MK_RX_RANGE_MASK_IDSTD2(mask)) == MK_RX_RANGE_CODE_IDSTD2(code) ) && \
                    ( ((idRaw3) & (tCanRxId3)~ MK_RX_RANGE_MASK_IDSTD3(mask)) == MK_RX_RANGE_CODE_IDSTD3(code) ) )
#  define C_RANGE_MATCH_EXT( CAN_RX_IDRAW_PARA, mask, code) \
                  ( ( ((idRaw0) & (tCanRxId0)~ MK_RX_RANGE_MASK_IDEXT0(mask)) == MK_RX_RANGE_CODE_IDEXT0(code) ) && \
                    ( ((idRaw1) & (tCanRxId1)~ MK_RX_RANGE_MASK_IDEXT1(mask)) == MK_RX_RANGE_CODE_IDEXT1(code) ) && \
                    ( ((idRaw2) & (tCanRxId2)~ MK_RX_RANGE_MASK_IDEXT2(mask)) == MK_RX_RANGE_CODE_IDEXT2(code) ) && \
                    ( ((idRaw3) & (tCanRxId3)~ MK_RX_RANGE_MASK_IDEXT3(mask)) == MK_RX_RANGE_CODE_IDEXT3(code) ) )
# endif
# if (kCanNumberOfUsedCanRxIdTables == 5)
/* Msg(4:3410) Macro parameter not enclosed in (). MISRA Rule 96 - no change */
#  define C_RANGE_MATCH_STD( CAN_RX_IDRAW_PARA, mask, code) \
                  ( ( ((idRaw0) & (tCanRxId0)~ MK_RX_RANGE_MASK_IDSTD0(mask)) == MK_RX_RANGE_CODE_IDSTD0(code) ) && \
                    ( ((idRaw1) & (tCanRxId1)~ MK_RX_RANGE_MASK_IDSTD1(mask)) == MK_RX_RANGE_CODE_IDSTD1(code) ) && \
                    ( ((idRaw2) & (tCanRxId2)~ MK_RX_RANGE_MASK_IDSTD2(mask)) == MK_RX_RANGE_CODE_IDSTD2(code) ) && \
                    ( ((idRaw3) & (tCanRxId3)~ MK_RX_RANGE_MASK_IDSTD3(mask)) == MK_RX_RANGE_CODE_IDSTD3(code) ) && \
                    ( ((idRaw4) & (tCanRxId4)~ MK_RX_RANGE_MASK_IDSTD4(mask)) == MK_RX_RANGE_CODE_IDSTD4(code) ) )
#  define C_RANGE_MATCH_EXT( CAN_RX_IDRAW_PARA, mask, code) \
                  ( ( ((idRaw0) & (tCanRxId0)~ MK_RX_RANGE_MASK_IDEXT0(mask)) == MK_RX_RANGE_CODE_IDEXT0(code) ) && \
                    ( ((idRaw1) & (tCanRxId1)~ MK_RX_RANGE_MASK_IDEXT1(mask)) == MK_RX_RANGE_CODE_IDEXT1(code) ) && \
                    ( ((idRaw2) & (tCanRxId2)~ MK_RX_RANGE_MASK_IDEXT2(mask)) == MK_RX_RANGE_CODE_IDEXT2(code) ) && \
                    ( ((idRaw3) & (tCanRxId3)~ MK_RX_RANGE_MASK_IDEXT3(mask)) == MK_RX_RANGE_CODE_IDEXT3(code) ) && \
                    ( ((idRaw4) & (tCanRxId4)~ MK_RX_RANGE_MASK_IDEXT4(mask)) == MK_RX_RANGE_CODE_IDEXT4(code) ) )
# endif
#else     /* C_MULTIPLE_RECEIVE_CHANNEL */

# if (kCanNumberOfUsedCanRxIdTables == 1)
/* Msg(4:3410) Macro parameter not enclosed in (). MISRA Rule 96 - no change */
#  define C_RANGE_MATCH( CAN_RX_IDRAW_PARA, mask, code)    \
                                (  ((idRaw0) & (tCanRxId0)~((mask).Id0)) == ((code).Id0) )
# endif
# if (kCanNumberOfUsedCanRxIdTables == 2)
/* Msg(4:3410) Macro parameter not enclosed in (). MISRA Rule 96 - no change */
#  define C_RANGE_MATCH( CAN_RX_IDRAW_PARA, mask, code)    \
                                ( ( ((idRaw0) & (tCanRxId0)~((mask).Id0)) == ((code).Id0) ) &&\
                                  ( ((idRaw1) & (tCanRxId1)~((mask).Id1)) == ((code).Id1) ) )
# endif
# if (kCanNumberOfUsedCanRxIdTables == 3)
/* Msg(4:3410) Macro parameter not enclosed in (). MISRA Rule 96 - no change */
#  define C_RANGE_MATCH( CAN_RX_IDRAW_PARA, mask, code)    \
                                ( ( ((idRaw0) & (tCanRxId0)~((mask).Id0)) == ((code).Id0) ) &&\
                                  ( ((idRaw1) & (tCanRxId1)~((mask).Id1)) == ((code).Id1) ) &&\
                                  ( ((idRaw2) & (tCanRxId2)~((mask).Id2)) == ((code).Id2) ) )
# endif
# if (kCanNumberOfUsedCanRxIdTables == 4)
/* Msg(4:3410) Macro parameter not enclosed in (). MISRA Rule 96 - no change */
#  define C_RANGE_MATCH( CAN_RX_IDRAW_PARA, mask, code)    \
                                ( ( ((idRaw0) & (tCanRxId0)~((mask).Id0)) == ((code).Id0) ) &&\
                                  ( ((idRaw1) & (tCanRxId1)~((mask).Id1)) == ((code).Id1) ) &&\
                                  ( ((idRaw2) & (tCanRxId2)~((mask).Id2)) == ((code).Id2) ) &&\
                                  ( ((idRaw3) & (tCanRxId3)~((mask).Id3)) == ((code).Id3) ) )
# endif
# if (kCanNumberOfUsedCanRxIdTables == 5)
/* Msg(4:3410) Macro parameter not enclosed in (). MISRA Rule 96 - no change */
#  define C_RANGE_MATCH( CAN_RX_IDRAW_PARA, mask, code)    \
                                ( ( ((idRaw0) & (tCanRxId0)~((mask).Id0)) == ((code).Id0) ) &&\
                                  ( ((idRaw1) & (tCanRxId1)~((mask).Id1)) == ((code).Id1) ) &&\
                                  ( ((idRaw2) & (tCanRxId2)~((mask).Id2)) == ((code).Id2) ) &&\
                                  ( ((idRaw3) & (tCanRxId3)~((mask).Id3)) == ((code).Id3) ) &&\
                                  ( ((idRaw4) & (tCanRxId4)~((mask).Id4)) == ((code).Id4) ) )
# endif
#endif


#if (kCanNumberOfUsedCanRxIdTables == 1)
# define CAN_RX_IDRAW_PARA                                 idRaw0
#endif
#if (kCanNumberOfUsedCanRxIdTables == 2)
# define CAN_RX_IDRAW_PARA                                 idRaw0,idRaw1
#endif
#if (kCanNumberOfUsedCanRxIdTables == 3)
# define CAN_RX_IDRAW_PARA                                 idRaw0,idRaw1,idRaw2
#endif
#if (kCanNumberOfUsedCanRxIdTables == 4)
# define CAN_RX_IDRAW_PARA                                 idRaw0,idRaw1,idRaw2,idRaw3
#endif
#if (kCanNumberOfUsedCanRxIdTables == 5)
# define CAN_RX_IDRAW_PARA                                 idRaw0,idRaw1,idRaw2,idRaw3,idRaw4
#endif


#if defined( C_SINGLE_RECEIVE_CHANNEL )
# define channel                                           ((CanChannelHandle)0)
# define canHwChannel                                      ((CanChannelHandle)0)
# define CAN_HL_HW_CHANNEL_STARTINDEX(channel)             ((CanChannelHandle)0)
# define CAN_HL_HW_CHANNEL_STOPINDEX(channel)              ((CanChannelHandle)0)
# define CAN_HL_HW_MSG_TRANSMIT_INDEX(canHwChannel)        (kCanMsgTransmitObj)
# define CAN_HL_HW_TX_NORMAL_INDEX(canHwChannel)           (kCanHwTxNormalIndex)

/* Offset which has to be added to change the hardware Tx handle into a logical handle, which is unique over all channels */
/*        Tx-Hardware-Handle - CAN_HL_HW_TX_STARTINDEX(canHwChannel) + CAN_HL_LOG_HW_TX_STARTINDEX(canHwChannel) */
# define CAN_HL_TX_OFFSET_HW_TO_LOG(canHwChannel)          ((vsintx)0-kCanHwTxStartIndex)

# define CAN_HL_TX_STARTINDEX(channel)                     ((CanTransmitHandle)0)
# define CAN_HL_TX_STAT_STARTINDEX(channel)                ((CanTransmitHandle)0)
# define CAN_HL_TX_DYN_ROM_STARTINDEX(channel)             (kCanNumberOfTxStatObjects)
# define CAN_HL_TX_DYN_RAM_STARTINDEX(channel)             ((CanTransmitHandle)0)
/* # define CAN_HL_RX_STARTINDEX(channel)                     ((CanReceiveHandle)0) */
/* index to access the ID tables - Basic index only for linear search 
   for hash search this is the start index of the ??? */
# define CAN_HL_RX_BASIC_STARTINDEX(channel)               ((CanReceiveHandle)0)
# if defined( C_SEARCH_HASH ) 
#  define CAN_HL_RX_FULL_STARTINDEX(canHwChannel)          ((CanReceiveHandle)0)
# else
#  define CAN_HL_RX_FULL_STARTINDEX(canHwChannel)          (kCanNumberOfRxBasicCANObjects)
# endif
# define CAN_HL_INIT_OBJ_STARTINDEX(channel)               ((vuint8)0)
# define CAN_HL_LOG_HW_TX_STARTINDEX(canHwChannel)         ((CanObjectHandle)0)
# define CAN_HL_HW_TX_STARTINDEX(canHwChannel)             (kCanHwTxStartIndex)
# define CAN_HL_HW_RX_FULL_STARTINDEX(canHwChannel)        (kCanHwRxFullStartIndex)
# define CAN_HL_HW_RX_BASIC_STARTINDEX(canHwChannel)       (kCanHwRxBasicStartIndex)
# define CAN_HL_HW_UNUSED_STARTINDEX(canHwChannel)         (kCanHwUnusedStartIndex)

# define CAN_HL_TX_STOPINDEX(channel)                      (kCanNumberOfTxObjects)
# define CAN_HL_TX_STAT_STOPINDEX(channel)                 (kCanNumberOfTxStatObjects)
# define CAN_HL_TX_DYN_ROM_STOPINDEX(channel)              (kCanNumberOfTxObjects)
# define CAN_HL_TX_DYN_RAM_STOPINDEX(channel)              (kCanNumberOfTxDynObjects)
/* # define CAN_HL_RX_STOPINDEX(channel)                      (kCanNumberOfRxObjects) */
# define CAN_HL_RX_BASIC_STOPINDEX(channel)                (kCanNumberOfRxBasicCANObjects)
# if defined( C_SEARCH_HASH ) 
#  define CAN_HL_RX_FULL_STOPINDEX(canHwChannel)           (kCanNumberOfRxFullCANObjects)
# else
#  define CAN_HL_RX_FULL_STOPINDEX(canHwChannel)           (kCanNumberOfRxBasicCANObjects+kCanNumberOfRxFullCANObjects)
# endif
# define CAN_HL_INIT_OBJ_STOPINDEX(channel)                (kCanNumberOfInitObjects)
# define CAN_HL_LOG_HW_TX_STOPINDEX(canHwChannel)          (kCanNumberOfUsedTxCANObjects)
# define CAN_HL_HW_TX_STOPINDEX(canHwChannel)              (kCanHwTxStartIndex     +kCanNumberOfUsedTxCANObjects)
# define CAN_HL_HW_RX_FULL_STOPINDEX(canHwChannel)         (kCanHwRxFullStartIndex +kCanNumberOfRxFullCANObjects)
# define CAN_HL_HW_RX_BASIC_STOPINDEX(canHwChannel)        (kCanHwRxBasicStartIndex+kCanNumberOfUsedRxBasicCANObjects)
# define CAN_HL_HW_UNUSED_STOPINDEX(canHwChannel)          (kCanHwUnusedStartIndex +kCanNumberOfUnusedObjects)

#else
#  define canHwChannel                                     channel   /*brackets are not allowed here due to compiler error with Renesas HEW compiler for SH2*/
#  define CAN_HL_HW_CHANNEL_STARTINDEX(channel)            (channel)
#  define CAN_HL_HW_CHANNEL_STOPINDEX(channel)             (channel)

#  define CAN_HL_HW_MSG_TRANSMIT_INDEX(canHwChannel)       (CanHwMsgTransmitIndex[(canHwChannel)])
#  define CAN_HL_HW_TX_NORMAL_INDEX(canHwChannel)          (CanHwTxNormalIndex[(canHwChannel)])
/* Offset which has to be added to change the hardware Tx handle into a logical handle, which is unique over all channels */
/*        Tx-Hardware-Handle - CAN_HL_HW_TX_STARTINDEX(canHwChannel) + CAN_HL_LOG_HW_TX_STARTINDEX(canHwChannel) */
#  define CAN_HL_TX_OFFSET_HW_TO_LOG(canHwChannel)         (CanTxOffsetHwToLog[(canHwChannel)])

# define CAN_HL_TX_STARTINDEX(channel)                     (CanTxStartIndex[(channel)])
# define CAN_HL_TX_STAT_STARTINDEX(channel)                (CanTxStartIndex[(channel)])
# define CAN_HL_TX_DYN_ROM_STARTINDEX(channel)             (CanTxDynRomStartIndex[(channel)])
# define CAN_HL_TX_DYN_RAM_STARTINDEX(channel)             (CanTxDynRamStartIndex[(channel)])
/* # define CAN_HL_RX_STARTINDEX(channel)                     (CanRxStartIndex[(channel)]) */
/* index to access the ID tables - Basic index only for linear search */
# define CAN_HL_RX_BASIC_STARTINDEX(channel)               (CanRxBasicStartIndex[(channel)])      
# define CAN_HL_RX_FULL_STARTINDEX(canHwChannel)           (CanRxFullStartIndex[(canHwChannel)])
# define CAN_HL_INIT_OBJ_STARTINDEX(channel)               (CanInitObjectStartIndex[(channel)])      
# define CAN_HL_LOG_HW_TX_STARTINDEX(canHwChannel)         (CanLogHwTxStartIndex[(canHwChannel)])
#  define CAN_HL_HW_TX_STARTINDEX(canHwChannel)             (CanHwTxStartIndex[(canHwChannel)])   
#  define CAN_HL_HW_RX_FULL_STARTINDEX(canHwChannel)        (CanHwRxFullStartIndex[(canHwChannel)])
#  define CAN_HL_HW_RX_BASIC_STARTINDEX(canHwChannel)       (CanHwRxBasicStartIndex[(canHwChannel)]) 
#  define CAN_HL_HW_UNUSED_STARTINDEX(canHwChannel)         (CanHwUnusedStartIndex[(canHwChannel)])
                                                           
# define CAN_HL_TX_STOPINDEX(channel)                      (CanTxStartIndex[(channel) + 1]) 
# define CAN_HL_TX_STAT_STOPINDEX(channel)                 (CanTxDynRomStartIndex[(channel)])
# define CAN_HL_TX_DYN_ROM_STOPINDEX(channel)              (CanTxStartIndex[(channel) + 1]) 
# define CAN_HL_TX_DYN_RAM_STOPINDEX(channel)              (CanTxDynRamStartIndex[(channel) + 1])
/* # define CAN_HL_RX_STOPINDEX(channel)                      (CanRxStartIndex[(channel) + 1]) */
/* index to access the ID tables - Basic index only for linear search */
# define CAN_HL_RX_BASIC_STOPINDEX(channel)                (CanRxFullStartIndex[CAN_HL_HW_CHANNEL_STARTINDEX(channel)])
# define CAN_HL_INIT_OBJ_STOPINDEX(channel)                (CanInitObjectStartIndex[(channel) + 1])
# define CAN_HL_LOG_HW_TX_STOPINDEX(canHwChannel)          (CanLogHwTxStartIndex[(canHwChannel) +1])      
#  define CAN_HL_HW_TX_STOPINDEX(canHwChannel)              (CanHwTxStopIndex[(canHwChannel)])
#  define CAN_HL_HW_RX_FULL_STOPINDEX(canHwChannel)         (CanHwRxFullStopIndex[(canHwChannel)])
#  define CAN_HL_HW_RX_BASIC_STOPINDEX(canHwChannel)         (CanHwRxBasicStopIndex[(canHwChannel)])
#  define CAN_HL_HW_UNUSED_STOPINDEX(canHwChannel)         (CanHwUnusedStopIndex[(canHwChannel)])

#endif


#if defined( C_SINGLE_RECEIVE_CHANNEL )

# define CANRANGE0ACCMASK(i)                               C_RANGE0_ACC_MASK
# define CANRANGE0ACCCODE(i)                               C_RANGE0_ACC_CODE
# define CANRANGE1ACCMASK(i)                               C_RANGE1_ACC_MASK
# define CANRANGE1ACCCODE(i)                               C_RANGE1_ACC_CODE
# define CANRANGE2ACCMASK(i)                               C_RANGE2_ACC_MASK
# define CANRANGE2ACCCODE(i)                               C_RANGE2_ACC_CODE
# define CANRANGE3ACCMASK(i)                               C_RANGE3_ACC_MASK
# define CANRANGE3ACCCODE(i)                               C_RANGE3_ACC_CODE

# define APPL_CAN_MSG_RECEIVED( i )                        (APPL_CAN_MSGRECEIVED( i ))

# define APPLCANRANGE0PRECOPY( i )                         (ApplCanRange0Precopy( i ))   
# define APPLCANRANGE1PRECOPY( i )                         (ApplCanRange1Precopy( i ))   
# define APPLCANRANGE2PRECOPY( i )                         (ApplCanRange2Precopy( i ))   
# define APPLCANRANGE3PRECOPY( i )                         (ApplCanRange3Precopy( i ))   

# define APPL_CAN_BUSOFF( i )                              (ApplCanBusOff())
# define APPL_CAN_WAKEUP( i )                              (ApplCanWakeUp())

# if defined( C_ENABLE_CAN_CANCEL_NOTIFICATION )
#  define APPLCANCANCELNOTIFICATION( i, j )                (APPL_CAN_CANCELNOTIFICATION( j ))
# else
#  define APPLCANCANCELNOTIFICATION( i, j )
# endif
# if defined( C_ENABLE_CAN_MSG_TRANSMIT_CANCEL_NOTIFICATION )
#  define APPLCANMSGCANCELNOTIFICATION( i )                (APPL_CAN_MSGCANCELNOTIFICATION())
# else
#  define APPLCANMSGCANCELNOTIFICATION( i )
# endif

# define CAN_RX_INDEX_TBL(channel,id)                      (CanRxIndexTbl[id])

#else

# define CANRANGE0ACCMASK(i)                               (CanChannelObject[i].RangeMask[0])
# define CANRANGE0ACCCODE(i)                               (CanChannelObject[i].RangeCode[0])
# define CANRANGE1ACCMASK(i)                               (CanChannelObject[i].RangeMask[1])
# define CANRANGE1ACCCODE(i)                               (CanChannelObject[i].RangeCode[1])
# define CANRANGE2ACCMASK(i)                               (CanChannelObject[i].RangeMask[2])
# define CANRANGE2ACCCODE(i)                               (CanChannelObject[i].RangeCode[2])
# define CANRANGE3ACCMASK(i)                               (CanChannelObject[i].RangeMask[3])
# define CANRANGE3ACCCODE(i)                               (CanChannelObject[i].RangeCode[3])

/* generated id type of the range */
# define CANRANGE0IDTYPE(i)                                (CanChannelObject[i].RangeIdType[0])
# define CANRANGE1IDTYPE(i)                                (CanChannelObject[i].RangeIdType[1])
# define CANRANGE2IDTYPE(i)                                (CanChannelObject[i].RangeIdType[2])
# define CANRANGE3IDTYPE(i)                                (CanChannelObject[i].RangeIdType[3])

# define APPL_CAN_MSG_RECEIVED( i )                        (CanChannelObject[(i)->Channel].ApplCanMsgReceivedFct(i))

# define APPLCANRANGE0PRECOPY( i )                         (CanChannelObject[(i)->Channel].ApplCanRangeFct[0](i))
# define APPLCANRANGE1PRECOPY( i )                         (CanChannelObject[(i)->Channel].ApplCanRangeFct[1](i))   
# define APPLCANRANGE2PRECOPY( i )                         (CanChannelObject[(i)->Channel].ApplCanRangeFct[2](i))   
# define APPLCANRANGE3PRECOPY( i )                         (CanChannelObject[(i)->Channel].ApplCanRangeFct[3](i))   

# define APPL_CAN_BUSOFF( i )                              (CanChannelObject[i].ApplCanBusOffFct(i))
# define APPL_CAN_WAKEUP( i )                              (CanChannelObject[i].ApplCanWakeUpFct(i))

# if defined( C_ENABLE_CAN_CANCEL_NOTIFICATION )
#  define APPLCANCANCELNOTIFICATION( i, j )                (CanChannelObject[i].ApplCanCancelNotificationFct( j ))
# else
#  define APPLCANCANCELNOTIFICATION( i, j )
# endif

# if defined( C_ENABLE_CAN_MSG_TRANSMIT_CANCEL_NOTIFICATION )
#  define APPLCANMSGCANCELNOTIFICATION( i )                (CanChannelObject[i].ApplCanMsgTransmitCancelNotifyFct( i ))
# else
#  define APPLCANMSGCANCELNOTIFICATION( i )
# endif

# define CAN_RX_INDEX_TBL(channel,id)                      (CanRxIndexTbl[channel][id])

#endif


#if defined ( C_ENABLE_CAN_CAN_INTERRUPT_CONTROL )
# define CAN_CAN_INTERRUPT_DISABLE(i)                        (CanCanInterruptDisable(i))
# define CAN_CAN_INTERRUPT_RESTORE(i)                        (CanCanInterruptRestore(i))
#else
# define CAN_CAN_INTERRUPT_DISABLE(i)
# define CAN_CAN_INTERRUPT_RESTORE(i)
#endif


#if defined( C_ENABLE_INDIVIDUAL_POLLING )
/* define first index to array CanHwObjIndivPolling[][] */
#  define CAN_HWOBJINDIVPOLLING_INDEX0                     (canHwChannel)
#endif


/* mask for range enable status */
#define kCanRange0                                         ((vuint16)1)
#define kCanRange1                                         ((vuint16)2)
#define kCanRange2                                         ((vuint16)4)
#define kCanRange3                                         ((vuint16)8)


/* Assertions ----------------------------------------------------------------*/
/*lint -function(exit,ApplCanFatalError)*/

/*lint -emacro( (506), assertUser) */
#if defined( C_ENABLE_USER_CHECK )
# if defined( C_SINGLE_RECEIVE_CHANNEL )
#  define assertUser(p,c,e)                                if (!(p))   {ApplCanFatalError(e);}                    /* PRQA S 3412 */
# else
#  define assertUser(p,c,e)                                if (!(p))   {ApplCanFatalError((c),(e));}              /* PRQA S 3412 */
# endif
#else
# define assertUser(p,c,e)
#endif

/*lint -emacro( (506), assertGen) */
#if defined( C_ENABLE_GEN_CHECK )
# if defined( C_SINGLE_RECEIVE_CHANNEL )
#  define assertGen(p,c,e)                                 if (!(p))   {ApplCanFatalError(e);}                    /* PRQA S 3412 */
# else
#  define assertGen(p,c,e)                                 if (!(p))   {ApplCanFatalError((c),(e));}              /* PRQA S 3412 */
# endif
#else
# define assertGen(p,c,e)
#endif

/*lint -emacro( (506), assertHardware) */
#if defined( C_ENABLE_HARDWARE_CHECK )
# if defined( C_SINGLE_RECEIVE_CHANNEL )
#  define assertHardware(p,c,e)                            if (!(p))   {ApplCanFatalError(e);}                    /* PRQA S 3412 */
# else
#  define assertHardware(p,c,e)                            if (!(p))   {ApplCanFatalError((c),(e));}              /* PRQA S 3412 */
# endif
#else
# define assertHardware(p,c,e)
#endif

/*lint -emacro( (506), assertInternal) */
#if defined( C_ENABLE_INTERNAL_CHECK )
# if defined( C_SINGLE_RECEIVE_CHANNEL )
#  define assertInternal(p,c,e)                            if (!(p))   {ApplCanFatalError(e);}                   /* PRQA S 3412 */
# else
#  define assertInternal(p,c,e)                            if (!(p))   {ApplCanFatalError((c),(e));}             /* PRQA S 3412 */
# endif
#else
# define assertInternal(p,c,e)
#endif

#if defined( C_ENABLE_TRANSMIT_QUEUE )

#if defined( C_CPUTYPE_16BIT )
# define kCanTxQueueShift     4
#endif


/* mask used to get the flag index from the handle */
# define kCanTxQueueMask      (((vuint8)1 << kCanTxQueueShift) - (vuint8)1)


#if defined( C_SINGLE_RECEIVE_CHANNEL )
# define CAN_HL_TXQUEUE_PADBITS(channel)             ((CanTransmitHandle)0)
# define CAN_HL_TXQUEUE_STARTINDEX(channel)          ((CanSignedTxHandle)0)
# define CAN_HL_TXQUEUE_STOPINDEX(channel)           ((CanSignedTxHandle)kCanTxQueueSize)
#else
# define CAN_HL_TXQUEUE_PADBITS(channel)             ((CanTransmitHandle)CanTxQueuePadBits[(channel)])
# define CAN_HL_TXQUEUE_STARTINDEX(channel)          (CanTxQueueStartIndex[(channel)])
# define CAN_HL_TXQUEUE_STOPINDEX(channel)           (CanTxQueueStartIndex[(channel) + (CanChannelHandle)1])
#endif


#endif
#if defined(C_ENABLE_HW_LOOP_TIMER)
#  define APPLCANTIMERSTART(a)                                 ApplCanTimerStart(CAN_CHANNEL_CANPARA_FIRST a)
#  define APPLCANTIMERLOOP(a)                                  ApplCanTimerLoop(CAN_CHANNEL_CANPARA_FIRST a)
#  define APPLCANTIMEREND(a)                                   ApplCanTimerEnd(CAN_CHANNEL_CANPARA_FIRST a)
#else
# define APPLCANTIMERSTART(a)
# define APPLCANTIMERLOOP(a)                                   ((vuint8)kCanOk)
# define APPLCANTIMEREND(a)
#endif

#if defined(C_COMP_COSMIC_MCS12X_MSCAN12)  
/* mask to check for valid CPU Tx interrupts */
# define kCanPendingTxObjMask                                  ((vuint8)0x06) /* we use Tx mailbox 2 and 1 for transmission */
#endif


#if defined (C_SINGLE_RECEIVE_CHANNEL)
#  define CAN_CNTRL_BASIS_ADR(channel)                         ((tCanCntrlRegBlock MEMORY_CAN *)(V_REG_BLOCK_ADR + kCanBasisAdr))
#else
#  define CAN_CNTRL_BASIS_ADR(channel)                         ((tCanCntrlRegBlock MEMORY_CAN *)(V_REG_BLOCK_ADR + CanBasisAdr[channel]))
#endif

#define CAN_RX_MAILBOX_BASIS_ADR(channel)                      ((tMsgObject MEMORY_CAN *)&(CAN_CNTRL_BASIS_ADR(channel) -> CanRxBuf))
#define CAN_TX_MAILBOX_BASIS_ADR(channel)                      ((tMsgObject MEMORY_CAN *)&(CAN_CNTRL_BASIS_ADR(channel) -> CanTxBuf))

#define CTL0                                                   (CAN_CNTRL_BASIS_ADR(channel) -> CanCTL0)
#define CTL1                                                   (CAN_CNTRL_BASIS_ADR(channel) -> CanCTL1)
#define CBTR0                                                  (CAN_CNTRL_BASIS_ADR(channel) -> CanCBTR0)
#define CBTR1                                                  (CAN_CNTRL_BASIS_ADR(channel) -> CanCBTR1)
#define CRFLG                                                  (CAN_CNTRL_BASIS_ADR(channel) -> CanCRFLG)
#define CRIER                                                  (CAN_CNTRL_BASIS_ADR(channel) -> CanCRIER)
#define CANTFLG                                                (CAN_CNTRL_BASIS_ADR(channel) -> CanCTFLG)
#define CTIER                                                  (CAN_CNTRL_BASIS_ADR(channel) -> CanCTIER)
#define CTARQ                                                  (CAN_CNTRL_BASIS_ADR(channel) -> CanCTARQ)
#define CTAAK                                                  (CAN_CNTRL_BASIS_ADR(channel) -> CanCTAAK)
#define CTBSEL                                                 (CAN_CNTRL_BASIS_ADR(channel) -> CanCTBSEL)
#define CIDAC                                                  (CAN_CNTRL_BASIS_ADR(channel) -> CanCIDAC)
#define CMISC                                                  (CAN_CNTRL_BASIS_ADR(channel) -> CanCMISC)
#define CRXERR                                                 (CAN_CNTRL_BASIS_ADR(channel) -> CanCRXERR)
#define CTXERR                                                 (CAN_CNTRL_BASIS_ADR(channel) -> CanCTXERR)
#define CIDAR0                                                 (CAN_CNTRL_BASIS_ADR(channel) -> CanCIDAR0)
#define CIDAR1                                                 (CAN_CNTRL_BASIS_ADR(channel) -> CanCIDAR1)
#define CIDAR2                                                 (CAN_CNTRL_BASIS_ADR(channel) -> CanCIDAR2)
#define CIDAR3                                                 (CAN_CNTRL_BASIS_ADR(channel) -> CanCIDAR3)
#define CIDMR0                                                 (CAN_CNTRL_BASIS_ADR(channel) -> CanCIDMR0)
#define CIDMR1                                                 (CAN_CNTRL_BASIS_ADR(channel) -> CanCIDMR1)
#define CIDMR2                                                 (CAN_CNTRL_BASIS_ADR(channel) -> CanCIDMR2)
#define CIDMR3                                                 (CAN_CNTRL_BASIS_ADR(channel) -> CanCIDMR3)
#define CIDAR4                                                 (CAN_CNTRL_BASIS_ADR(channel) -> CanCIDAR4)
#define CIDAR5                                                 (CAN_CNTRL_BASIS_ADR(channel) -> CanCIDAR5)
#define CIDAR6                                                 (CAN_CNTRL_BASIS_ADR(channel) -> CanCIDAR6)
#define CIDAR7                                                 (CAN_CNTRL_BASIS_ADR(channel) -> CanCIDAR7)
#define CIDMR4                                                 (CAN_CNTRL_BASIS_ADR(channel) -> CanCIDMR4)
#define CIDMR5                                                 (CAN_CNTRL_BASIS_ADR(channel) -> CanCIDMR5)
#define CIDMR6                                                 (CAN_CNTRL_BASIS_ADR(channel) -> CanCIDMR6)
#define CIDMR7                                                 (CAN_CNTRL_BASIS_ADR(channel) -> CanCIDMR7)

# if defined(C_SINGLE_RECEIVE_CHANNEL)
#  define CanLL_CanInterruptDisable(canHwChannel, localInterruptOldFlagPtr)    CanCanInterruptDisableInternal((localInterruptOldFlagPtr))
# else
#  define CanLL_CanInterruptDisable(canHwChannel, localInterruptOldFlagPtr)    CanCanInterruptDisableInternal((canHwChannel), (localInterruptOldFlagPtr))
# endif

# if defined(C_SINGLE_RECEIVE_CHANNEL)
#  define CanLL_CanInterruptRestore(canHwChannel, localInterruptOldFlag)       CanCanInterruptRestoreInternal((localInterruptOldFlag))
# else
# define CanLL_CanInterruptRestore(canHwChannel, localInterruptOldFlag)        CanCanInterruptRestoreInternal((canHwChannel), (localInterruptOldFlag))
# endif

#if defined(C_COMP_COSMIC_MCS12X_MSCAN12)  
#define CanLL_TxIsHWObjFree(canHwChannel, txObjHandle)                         (((CANTFLG & CanMailboxSelect[txObjHandle]) != 0) ? kCanTrue : kCanFalse)
#endif


#if defined( C_ENABLE_SLEEP_WAKEUP )
# define CanLL_HwIsSleep(CAN_HW_CHANNEL_CANPARA_ONLY)                          (((CTL1 & SLPAK) == SLPAK) ? kCanTrue : kCanFalse)
#else
# define CanLL_HwIsSleep(CAN_HW_CHANNEL_CANPARA_ONLY)                          (kCanFalse)
#endif

#define CanLL_HwIsStop(CAN_HW_CHANNEL_CANPARA_ONLY)                            (((CTL1 & INITAK) == INITAK) ? kCanTrue : kCanFalse)

#define CanLL_HwIsBusOff(CAN_HW_CHANNEL_CANPARA_ONLY)                          (((CRFLG & BOFFIF) == BOFFIF) ?  kCanTrue : kCanFalse)

#if defined( C_ENABLE_EXTENDED_STATUS )
# define CanLL_HwIsPassive(CAN_HW_CHANNEL_CANPARA_ONLY)                        ((((CRFLG & (TSTAT1 | TSTAT0)) == TSTAT1) || ((CRFLG & (RSTAT1 | RSTAT0)) == RSTAT1)) ? kCanTrue : kCanFalse)

# define CanLL_HwIsWarning(CAN_HW_CHANNEL_CANPARA_ONLY)                        ((((CRFLG & (TSTAT1 | TSTAT0)) == TSTAT0) || ((CRFLG & (RSTAT1 | RSTAT0)) == RSTAT0)) ? kCanTrue : kCanFalse)
#endif /* C_ENABLE_EXTENDED_STATUS */

/*
HT: necessary for ASR, ApplCanTimerStart and END are empty macros.
*/
#if defined(C_MULTIPLE_RECEIVE_CHANNEL)
# define CanLL_ApplCanTimerStart(loop)                                         ApplCanTimerStart(channel, loop)
# define CanLL_ApplCanTimerEnd(loop)                                           ApplCanTimerEnd(channel, loop)
# define CanLL_ApplCanTimerLoop(loop)                                          ApplCanTimerLoop(channel, loop)
#else
# define CanLL_ApplCanTimerStart(loop)                                         ApplCanTimerStart(loop)
# define CanLL_ApplCanTimerEnd(loop)                                           ApplCanTimerEnd(loop)
# define CanLL_ApplCanTimerLoop(loop)                                          ApplCanTimerLoop(loop)
#endif

/***************************************************************************/
/* Defines / data types / structs / unions                                 */
/***************************************************************************/

#if defined( C_ENABLE_TRANSMIT_QUEUE )
#endif
/* Define CAN Chip hardware; segment must be located in locator file    */
/* register layout of the can chip                                      */
/* Structure describing CAN receive buffer. */

/* The description of the hardware registers is located in the header because
   the XGate part needs also these typedefs
*/
#if defined(C_COMP_COSMIC_MCS12X_MSCAN12)  
# if defined(C_ENABLE_XGATE_USED)
typedef struct
{
  volatile vuint16* pXGif;
  vuint16           nTxMask;
  vuint16           nRxMask;
  vuint16           nErrorMask;
  vuint16           nWakeUpMask;
}
tCanIrqChannelInfo;
# endif
#endif

/****************************************************************************/
/* Constants                                                                */
/****************************************************************************/

#if defined( C_ENABLE_TRANSMIT_QUEUE )
/* ROM CATEGORY 1 START*/
/* lookup table for setting the flags in the queue */
V_MEMROM0 static V_MEMROM1 tCanQueueElementType V_MEMROM2 CanShiftLookUp[1 << kCanTxQueueShift] = 
{
#if defined( C_CPUTYPE_16BIT ) 
  (tCanQueueElementType)0x00000001, (tCanQueueElementType)0x00000002, (tCanQueueElementType)0x00000004, (tCanQueueElementType)0x00000008, 
  (tCanQueueElementType)0x00000010, (tCanQueueElementType)0x00000020, (tCanQueueElementType)0x00000040, (tCanQueueElementType)0x00000080
#endif

#if defined( C_CPUTYPE_16BIT )
 ,(tCanQueueElementType)0x00000100, (tCanQueueElementType)0x00000200, (tCanQueueElementType)0x00000400, (tCanQueueElementType)0x00000800, 
  (tCanQueueElementType)0x00001000, (tCanQueueElementType)0x00002000, (tCanQueueElementType)0x00004000, (tCanQueueElementType)0x00008000
#endif

};

/* returns the highest pending flag from the lower nibble */
V_MEMROM0 static V_MEMROM1 vsint8 V_MEMROM2 CanGetHighestFlagFromNibble[16] =        /* PRQA S 3218 */ /* Misra rule 8.7: only accessed in one function. - depends on configuration */
{    
  (vsint8)-1,                /* (vsint8)0xFF - changed due to misra ; cast due to R32C */
  0x00,
  0x01, 0x01,
  0x02, 0x02, 0x02, 0x02,
  0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03
};
/* ROM CATEGORY 1 END*/
#endif
/* Global constants with CAN driver main and subversion */
/* ROM CATEGORY 4 START*/
V_MEMROM0 V_MEMROM1 vuint8 V_MEMROM2 kCanMainVersion   = (vuint8)(( DRVCAN_MCS12XMSCANHLL_VERSION ) >> 8);  /*lint !e572 !e778*/
V_MEMROM0 V_MEMROM1 vuint8 V_MEMROM2 kCanSubVersion    = (vuint8)( DRVCAN_MCS12XMSCANHLL_VERSION & 0x00FF );
V_MEMROM0 V_MEMROM1 vuint8 V_MEMROM2 kCanBugFixVersion = (vuint8)( DRVCAN_MCS12XMSCANHLL_RELEASE_VERSION );
/* ROM CATEGORY 4 END*/

#if defined(C_COMP_COSMIC_MCS12X_MSCAN12)  
static V_MEMROM0 V_MEMROM1 vuint8 V_MEMROM2 CanMailboxSelect[3] = 
{
  (vuint8)0x01, (vuint8)0x02, (vuint8)0x04
};

static V_MEMROM0 V_MEMROM1 vuint8 V_MEMROM2 CanFirstMailbox[8] =
{
  (vuint8)0xFF, (vuint8)0x00, (vuint8)0x01, (vuint8)0x00,
  (vuint8)0x02, (vuint8)0x00, (vuint8)0x01, (vuint8)0x00
};
#endif

#if defined(C_COMP_COSMIC_MCS12X_MSCAN12)  
# if defined(C_ENABLE_XGATE_USED)
/* index = (MSCAN HW Offset >> 6) & 0x7 */
V_MEMROM0 V_MEMROM1 tCanIrqChannelInfo V_MEMROM2 CanXGateIrqChannelInfo[5] =
{
  {(volatile vuint16*)0x38C, 0x0100, 0x0200, 0x0400, 0x0800 }, /* CAN 0 */
  {(volatile vuint16*)0x38C, 0x0010, 0x0020, 0x0040, 0x0080 }, /* CAN 1 */
  {(volatile vuint16*)0x38C, 0x0001, 0x0002, 0x0004, 0x0008 }, /* CAN 2 */
  {(volatile vuint16*)0x38E, 0x1000, 0x2000, 0x4000, 0x8000 }, /* CAN 3 */
  {(volatile vuint16*)0x38E, 0x0100, 0x0200, 0x0400, 0x0800 }  /* CAN 4 */
};

V_MEMROM0 V_MEMROM1 vuint8 V_MEMROM2 CanXGateChannelToInfo[8] = 
{
  (vuint8)0x03, /* Baseaddress: 0x200 -> Irq Channel 0x4d */
  (vuint8)0xFF,
  (vuint8)0x04, /* Baseaddress: 0x280 -> Irq Channel 0x49 */
  (vuint8)0xFF,
  (vuint8)0xFF,
  (vuint8)0x00, /* Baseaddress: 0x140 -> Irq Channel 0x59 */
  (vuint8)0x01, /* Baseaddress: 0x180 -> Irq Channel 0x55 */
  (vuint8)0x02, /* Baseaddress: 0x1C0 -> Irq Channel 0x51 */
};
# endif
#endif

# if defined(C_COMP_COSMIC_MCS12X_MSCAN12)  
#  if defined(C_ENABLE_CAN_RAM_CHECK)
#   if defined(C_ENABLE_EXTENDED_ID)    
V_MEMROM0 V_MEMROM1 static vuint32 V_MEMROM2 CanMemCheckId[3]  = {0x55555555,0xAAAAAAAA,0x00000000};
#   else
V_MEMROM0 V_MEMROM1 static vuint16 V_MEMROM2 CanMemCheckId[3]  = {0x5555,0xAAAA,0x0000};
#   endif
V_MEMROM0 V_MEMROM1 static vuint8 V_MEMROM2 CanMemCheckData[3] = {0x55,0xAA,0x00};
V_MEMROM0 V_MEMROM1 static vuint8 V_MEMROM2 CanMemCheckDlc[3]  = {0x08,0x06,0x01};
V_MEMROM0 V_MEMROM1 static vuint8 V_MEMROM2 CanMemCheckPrio[3] = {0x01,0x02,0x04};
#  endif
# endif

/***************************************************************************/
/* CAN-Hardware Data Definitions                                            */
/***************************************************************************/


/***************************************************************************/
/* external declarations                                                    */
/***************************************************************************/

#if !defined( CANDRV_SET_CODE_TEST_POINT )
# define CANDRV_SET_CODE_TEST_POINT(x)
#else
extern vuint8 tscCTKTestPointState[CTK_MAX_TEST_POINT];       /* PRQA S 3447 */
#endif

/***************************************************************************/
/* global data definitions                                                 */
/***************************************************************************/

/* RAM CATEGORY 1 START*/
volatile CanReceiveHandle canRxHandle[kCanNumberOfChannels];        /* PRQA S 1514 */
/* RAM CATEGORY 1 END*/

/* RAM CATEGORY 3 START*/
#if defined( C_ENABLE_CONFIRMATION_FCT ) && \
    defined( C_ENABLE_DYN_TX_OBJECTS )   && \
    defined( C_ENABLE_TRANSMIT_QUEUE )
CanTransmitHandle          confirmHandle[kCanNumberOfChannels];
#endif
/* RAM CATEGORY 3 END*/

/* RAM CATEGORY 1 START*/
#if defined( C_ENABLE_CONFIRMATION_FLAG )
/* Msg(4:0750) A union type has been used. MISRA Rules 110 - no change */
V_MEMRAM0 volatile V_MEMRAM1_NEAR union CanConfirmationBits V_MEMRAM2_NEAR CanConfirmationFlags;       /* PRQA S 0759 */
#endif

#if defined( C_ENABLE_INDICATION_FLAG )
/* Msg(4:0750) A union type has been used. MISRA Rules 110 - no change */
V_MEMRAM0 volatile V_MEMRAM1_NEAR union CanIndicationBits   V_MEMRAM2_NEAR CanIndicationFlags;         /* PRQA S 0759 */
#endif
/* RAM CATEGORY 1 END*/

/* RAM CATEGORY 1 START*/
#if defined( C_ENABLE_VARIABLE_RX_DATALEN )
/* ##RI1.4 - 3.31: Dynamic Receive DLC */
volatile vuint8 canVariableRxDataLen[kCanNumberOfRxObjects];
#endif
/* RAM CATEGORY 1 END*/

/* RAM CATEGORY 1 START*/
CanChipDataPtr canRDSRxPtr[kCanNumberOfChannels];                  /* PRQA S 1514 */
/* RAM CATEGORY 1 END*/

/* RAM CATEGORY 1 START*/
CanChipDataPtr canRDSTxPtr[kCanNumberOfUsedTxCANObjects];          /* PRQA S 1514 */
/* RAM CATEGORY 1 END*/

/***************************************************************************/
/* local data definitions                                                  */
/***************************************************************************/

/* support for CAN driver features : */
/* RAM CATEGORY 1 START*/
static volatile CanTransmitHandle canHandleCurTxObj[kCanNumberOfUsedTxCANObjects];
/* RAM CATEGORY 1 END*/

/* RAM CATEGORY 2 START*/
#if defined( C_ENABLE_ECU_SWITCH_PASS )
static vuint8 canPassive[kCanNumberOfChannels];
#endif
/* RAM CATEGORY 2 END*/

/* RAM CATEGORY 2 START*/
#if defined( C_ENABLE_CAN_RAM_CHECK )
static vuint8 canComStatus[kCanNumberOfChannels]; /* stores the decision of the App after the last CAN RAM check */
#endif

#if defined( C_ENABLE_DYN_TX_OBJECTS )
static volatile vuint8 canTxDynObjReservedFlag[kCanNumberOfTxDynObjects];

# if defined( C_ENABLE_DYN_TX_ID )
static tCanTxId0 canDynTxId0[kCanNumberOfTxDynObjects];
#  if (kCanNumberOfUsedCanTxIdTables > 1)
static tCanTxId1 canDynTxId1[kCanNumberOfTxDynObjects];
#  endif
#  if (kCanNumberOfUsedCanTxIdTables > 2)
static tCanTxId2 canDynTxId2[kCanNumberOfTxDynObjects];
#  endif
#  if (kCanNumberOfUsedCanTxIdTables > 3)
static tCanTxId3 canDynTxId3[kCanNumberOfTxDynObjects];
#  endif
#  if (kCanNumberOfUsedCanTxIdTables > 4)
static tCanTxId4 canDynTxId4[kCanNumberOfTxDynObjects];
#  endif
#  if defined( C_ENABLE_MIXED_ID )
#   if defined( C_HL_ENABLE_IDTYPE_IN_ID )
#   else
static tCanIdType                 canDynTxIdType[kCanNumberOfTxDynObjects];
#   endif
#  endif
# endif

# if defined( C_ENABLE_DYN_TX_DLC )
static vuint8                   canDynTxDLC[kCanNumberOfTxDynObjects];
# endif
# if defined( C_ENABLE_DYN_TX_DATAPTR )
static vuint8*                  canDynTxDataPtr[kCanNumberOfTxDynObjects];
# endif
# if defined( C_ENABLE_CONFIRMATION_FCT )
# endif 
#endif /* C_ENABLED_DYN_TX_OBJECTS */


#if defined( C_ENABLE_TX_MASK_EXT_ID )
static tCanTxId0 canTxMask0[kCanNumberOfChannels];
# if (kCanNumberOfUsedCanTxIdTables > 1)
static tCanTxId1 canTxMask1[kCanNumberOfChannels];
# endif
# if (kCanNumberOfUsedCanTxIdTables > 2)
static tCanTxId2 canTxMask2[kCanNumberOfChannels];
# endif
# if (kCanNumberOfUsedCanTxIdTables > 3)
static tCanTxId3 canTxMask3[kCanNumberOfChannels];
# endif
# if (kCanNumberOfUsedCanTxIdTables > 4)
static tCanTxId4 canTxMask4[kCanNumberOfChannels];
# endif
#endif

#if defined( C_ENABLE_VARIABLE_DLC )
static vuint8 canTxDLC_RAM[kCanNumberOfTxObjects];
#endif

#if defined( C_HL_ENABLE_COPROCESSOR_SUPPORT )
#else
static volatile vuint8 canStatus[kCanNumberOfChannels];

# if defined( C_ENABLE_PART_OFFLINE )
static vuint8 canTxPartOffline[kCanNumberOfChannels];
# endif
#endif
/* RAM CATEGORY 2 END*/

/* RAM CATEGORY 1 START*/
static vsintx          canCanInterruptCounter[kCanNumberOfChannels];
#if defined( C_HL_ENABLE_CAN_IRQ_DISABLE )
static tCanLLCanIntOld canCanInterruptOldStatus[kCanNumberOfHwChannels];
#endif
/* RAM CATEGORY 1 END*/

#if defined( C_HL_ENABLE_LAST_INIT_OBJ )
/* RAM CATEGORY 4 START*/
static CanInitHandle lastInitObject[kCanNumberOfChannels];
/* RAM CATEGORY 4 END*/
#endif
#if defined( C_ENABLE_TX_POLLING )          || \
    defined( C_ENABLE_RX_BASICCAN_POLLING ) || \
    defined( C_ENABLE_ERROR_POLLING )       || \
    defined( C_ENABLE_WAKEUP_POLLING )      
/* RAM CATEGORY 2 START*/
static vuint8 canPollingTaskActive[kCanNumberOfChannels];
/* RAM CATEGORY 2 END*/
#endif

/* RAM CATEGORY 1 START*/
#if defined( C_ENABLE_RX_BASICCAN_OBJECTS )
static tCanRxInfoStruct        canRxInfoStruct[kCanNumberOfChannels];
#endif
#if defined( C_ENABLE_CAN_TX_CONF_FCT )
/* ##RI-1.10 Common Callbackfunction in TxInterrupt */
static tCanTxConfInfoStruct    txInfoStructConf[kCanNumberOfChannels];
#endif

#if defined( C_ENABLE_COND_RECEIVE_FCT )
static volatile vuint8 canMsgCondRecState[kCanNumberOfChannels];
#endif

#if defined( C_ENABLE_RX_QUEUE )
# if defined( C_ENABLE_RX_QUEUE_EXTERNAL )
static tCanRxQueue* canRxQueuePtr;     /* pointer to the rx queue */
#  define canRxQueue                                       (*canRxQueuePtr)
# else
static tCanRxQueue canRxQueue;         /* the rx queue (buffer and queue variables) */
# endif
#endif
/* RAM CATEGORY 1 END*/

#if defined( C_ENABLE_TRANSMIT_QUEUE )
/* RAM CATEGORY 1 START*/
static volatile tCanQueueElementType canTxQueueFlags[kCanTxQueueSize];
/* RAM CATEGORY 1 END*/
#endif
#if defined( C_ENABLE_CAN_RAM_CHECK )
/* ROM CATEGORY 4 START*/
V_MEMROM0 static V_MEMROM1 vuint8  V_MEMROM2 CanMemCheckValues8bit[3] = 
{
  0xAA, 0x55, 0x00
};
V_MEMROM0 static V_MEMROM1 vuint16 V_MEMROM2 CanMemCheckValues16bit[3] = 
{
  0xAAAA, 0x5555, 0x0000
};
V_MEMROM0 static V_MEMROM1 vuint32 V_MEMROM2 CanMemCheckValues32bit[3] = 
{
  0xAAAAAAAAUL, 0x55555555UL, 0x00000000UL
};
/* ROM CATEGORY 4 END*/
#endif

# if defined(C_HL_ENABLE_COPROCESSOR_SUPPORT)
#  if defined( C_ENABLE_PART_OFFLINE )
static vuint8 canTxPartOffline[kCanNumberOfChannels];
#  endif
static volatile vuint8 canStatus[kCanNumberOfChannels];
# endif
/* CanTransmit needs to know if the Tx Irq is disabled */
static vuintx canCanIrqDisabled[kCanNumberOfChannels];
# if defined(C_ENABLE_TX_POLLING)
static volatile vuint8 canllTxStatusFlag[kCanNumberOfChannels];
# endif


#if defined(C_ENABLE_XGATE_USED)
/* counter for the global XGate interrupt enable/ restore */
static vuintx canCanXGateIrqtCount;
#endif


/***************************************************************************/
/*  local prototypes                                                       */
/***************************************************************************/
#if defined( C_ENABLE_RX_BASICCAN_OBJECTS )
# if defined( C_ENABLE_RX_QUEUE )
/* CODE CATEGORY 1 START*/
static vuint8 CanHL_ReceivedRxHandleQueue(CAN_CHANNEL_CANTYPE_ONLY);
/* CODE CATEGORY 1 END*/
# endif
/* CODE CATEGORY 1 START*/
# if defined( C_ENABLE_RX_QUEUE )
static vuint8 CanHL_ReceivedRxHandle( CAN_CHANNEL_CANTYPE_FIRST tCanRxInfoStruct *pCanRxInfoStruct );
# else
static vuint8 CanHL_ReceivedRxHandle( CAN_CHANNEL_CANTYPE_ONLY );
# endif
/* CODE CATEGORY 1 END*/
# if defined( C_ENABLE_INDICATION_FLAG ) || \
     defined( C_ENABLE_INDICATION_FCT )
/* CODE CATEGORY 1 START*/
static void CanHL_IndRxHandle( CanReceiveHandle rxHandle );
/* CODE CATEGORY 1 END*/
# endif
#endif
#if defined( C_ENABLE_RX_BASICCAN_OBJECTS )
/* CODE CATEGORY 1 START*/
static void CanBasicCanMsgReceived(CAN_HW_CHANNEL_CANTYPE_FIRST CanObjectHandle rxObjHandle);  
/* CODE CATEGORY 1 END*/
#endif

/* CODE CATEGORY 1 START*/
static void CanHL_TxConfirmation(CAN_CHANNEL_CANTYPE_FIRST CanObjectHandle txObjHandle);
/* CODE CATEGORY 1 END*/
# if defined( C_ENABLE_CAN_TRANSMIT )
/* CODE CATEGORY 1 START*/
static vuint8 CanCopyDataAndStartTransmission(CAN_CHANNEL_CANTYPE_FIRST CanObjectHandle txObjHandle, CanTransmitHandle txHandle)  C_API_3;   /*lint !e14 !e31*/
/* CODE CATEGORY 1 END*/
# endif   /* C_ENABLE_CAN_TRANSMIT */

#if defined( C_ENABLE_TRANSMIT_QUEUE )
/* CODE CATEGORY 4 START*/
static void CanDelQueuedObj( CAN_CHANNEL_CANTYPE_ONLY );
/* CODE CATEGORY 4 END*/
# if defined( C_ENABLE_TX_POLLING ) 
/* CODE CATEGORY 2 START*/
static void CanHl_RestartTxQueue( CAN_CHANNEL_CANTYPE_ONLY );
/* CODE CATEGORY 2 END*/
# endif
#endif 

/* CODE CATEGORY 2 START*/
static void CanHL_ErrorHandling( CAN_HW_CHANNEL_CANTYPE_ONLY );
/* CODE CATEGORY 2 END*/

#if defined( C_ENABLE_VARIABLE_RX_DATALEN )
/* CODE CATEGORY 1 START*/
static void CanSetVariableRxDatalen ( CanReceiveHandle rxHandle, vuint8 dataLen );
/* CODE CATEGORY 1 END*/
#endif


#if defined( C_ENABLE_SLEEP_WAKEUP )
/* CODE CATEGORY 4 START*/
static void CanLL_WakeUpHandling(CAN_CHANNEL_CANTYPE_ONLY);
/* CODE CATEGORY 4 END*/
#endif

static void CanCanInterruptDisableInternal(CAN_CHANNEL_CANTYPE_FIRST tCanLLCanIntOld* pOldFlagPtr);
static void CanCanInterruptRestoreInternal(CAN_CHANNEL_CANTYPE_FIRST tCanLLCanIntOld nOldFlag);

#if defined( C_ENABLE_CAN_RAM_CHECK )
/* CODE CATEGORY 4 START*/
static vuint8 CanLL_IsMailboxCorrupt(CAN_HW_CHANNEL_CANTYPE_FIRST CanObjectHandle hwObjHandle);
/* CODE CATEGORY 4 END*/
#endif

/***************************************************************************/
/*  Error Check                                                            */
/***************************************************************************/

/***************** error check for Organi process **************************/







/***************** error check for not supported feature  **************************/




#if defined( C_ENABLE_COMMON_CAN )
# error "Common CAN is not supported with this CAN driver implementation"
#endif

#if defined( C_ENABLE_MULTI_ECU_CONFIG )
# error "Multiple Configuration is not supported with this CAN driver implementation"
#endif


#if (VSTDLIB__COREHLL_VERSION  <  0x0213 )
# error "Incompatible version of VStdLib. Core Version 2.13.00 or higher is necessary."
#endif


#if defined( C_SEARCH_HASH )
# if !defined( kHashSearchListCountEx )
#  error "kHashSearchListCountEx not defined"
# endif
# if !defined( kHashSearchMaxStepsEx )
#  error "kHashSearchMaxStepsEx not defined"
# endif
# if !defined( kHashSearchListCount )
#  error "kHashSearchListCount not defined"
# endif
# if !defined( kHashSearchMaxSteps )
#  error "kHashSearchMaxSteps not defined"
# endif
# if ( (kHashSearchMaxStepsEx < 1) ||(kHashSearchMaxStepsEx > 32768) )
#  error "kHashSearchMaxStepsEx has ilegal value"
# endif
# if ( kHashSearchListCountEx > 32768 )
#  error "Hash table for extended ID is too large"
# endif
# if ( (kHashSearchMaxSteps < 1) ||(kHashSearchMaxSteps > 32768) )
#  error "kHashSearchMaxStepsEx has ilegal value"
# endif
# if ( kHashSearchListCount > 32768 )
#  error "Hash table for standard ID is too large"
# endif
# if !defined( C_ENABLE_EXTENDED_ID) && (kHashSearchListCountEx > 0)
#  error "kHashSearchListCountEx has to be 0 in this configuration"
# endif
# if defined( C_ENABLE_EXTENDED_ID) && !defined( C_ENABLE_MIXED_ID) &&(kHashSearchListCount > 0)
#  error "kHashSearchListCount has to be 0 in this configuration"
# endif
#endif





#if defined( C_ENABLE_RX_QUEUE )
# if !defined( C_HL_ENABLE_RX_INFO_STRUCT_PTR )
#  error "RX-Queue requires C_HL_ENABLE_RX_INFO_STRUCT_PTR"
# endif
#endif




#if defined ( CAN_POLLING_IRQ_DISABLE ) || defined ( CAN_POLLING_IRQ_RESTORE )
# error "Feature has changed - use C_DISABLE_CAN_CAN_INTERRUPT_CONTROL to remove the CAN interrupt disabling"
#endif



/***************************************************************************/
/*  Functions                                                              */
/***************************************************************************/


/****************************************************************************
| NAME:             CanCanInterruptDisableInternal
| CALLED BY:        CanCanInterruptDisable
| PRECONDITIONS:    none
|
| INPUT PARAMETERS: nChannel: CAN channel to be disabled
|                   pOldFlagPtr: pointer to variable which stores the current state
|
| RETURN VALUES:    -
|
| DESCRIPTION:      disables CAN IRQs
****************************************************************************/
static void CanCanInterruptDisableInternal(CAN_CHANNEL_CANTYPE_FIRST tCanLLCanIntOld* pOldFlagPtr)
{
  CanDeclareGlobalInterruptOldStatus
  CanNestedGlobalInterruptDisable();
  CanBeginCriticalXGateSection();
  pOldFlagPtr -> oldCanCRIER = CRIER;
  pOldFlagPtr -> oldCanCTIER = CTIER;
  CRIER = kCanCRIERInterruptsDisabledVal;
  CTIER = (vuint8)0;
  CanDisableXGateInterrupts();
  canCanIrqDisabled[channel] = kCanTrue;
  CanEndCriticalXGateSection();
  CanNestedGlobalInterruptRestore();
}

/****************************************************************************
| NAME:             CanCanInterruptRestoreInternal
| CALLED BY:        CanCanInterruptRestore
| PRECONDITIONS:    none
|
| INPUT PARAMETERS: channel: CAN channel to be restored
|                   nOldFlag: the old IRQ register value
|
| RETURN VALUES:    -
|
| DESCRIPTION:      retores the previous CAN IRQ state
****************************************************************************/
static void CanCanInterruptRestoreInternal(CAN_CHANNEL_CANTYPE_FIRST tCanLLCanIntOld nOldFlag)
{
  CanDeclareGlobalInterruptOldStatus
  CanNestedGlobalInterruptDisable();
  CanBeginCriticalXGateSection();
  CRIER = nOldFlag.oldCanCRIER;
  CTIER = nOldFlag.oldCanCTIER;
  CanRestoreXGateInterrupts();
  canCanIrqDisabled[channel] = kCanFalse;
  CanEndCriticalXGateSection();
  CanNestedGlobalInterruptRestore();
}

# if defined( C_ENABLE_MEMCOPY_SUPPORT )
/****************************************************************************
| NAME:             CanCopyFromCan
| CALLED BY:        Application
| PRECONDITIONS:    none
|
| INPUT PARAMETERS: void *             dst        | pointer to destionation buffer
|                   CanChipDataPtr     src        | pointer to CAN buffer
|                   vuint8             len        | number of bytes to copy
|
| RETURN VALUES:    -
|
| DESCRIPTION:      copy data from CAN receive buffer to RAM.
|
| ATTENTION:        
****************************************************************************/
/* Msg(4:3673) The object addressed by the pointer "txMsgStruct" is not modified in this function.
   The use of "const" should be considered to indicate that it never changes. MISRA Rule 81 - no change */
/* CODE CATEGORY 1 START*/
#  if defined (V_ENABLE_USED_GLOBAL_VAR)
void CanCopyFromCan(V_MEMRAM1_FAR void V_MEMRAM2_FAR V_MEMRAM3_FAR *dst, CanChipDataPtr src, vuint8 len)     /* PRQA S 3673 */      /* suppress message about const pointer */
#  else
void CanCopyFromCan(void *dst, CanChipDataPtr src, vuint8 len)     /* PRQA S 3673 */      /* suppress message about const pointer */
#  endif
{
#  if defined(C_COMP_COSMIC_MCS12X_MSCAN12)  
#   if defined (V_ENABLE_USED_GLOBAL_VAR)
  VStdMemCpyRamToFarRam(dst, src, len);
#   else
  VStdMemCpyRamToRam(dst, src, len);
#   endif
#  endif
}
/* CODE CATEGORY 1 END*/

/****************************************************************************
| NAME:             CanCopyToCan
| CALLED BY:        Application
| PRECONDITIONS:    none
|
| INPUT PARAMETERS: void *             src        | pointer to source buffer
|                   CanChipDataPtr     dst        | pointer to CAN buffer
|                   vuint8             len        | number of bytes to copy
|
| RETURN VALUES:    -
|
| DESCRIPTION:      copy data from CAN receive buffer to RAM.
|
| ATTENTION:        
****************************************************************************/
/* Msg(4:3673) The object addressed by the pointer "txMsgStruct" is not modified in this function.
   The use of "const" should be considered to indicate that it never changes. MISRA Rule 81 - no change */
/* CODE CATEGORY 1 START*/
#  if defined (V_ENABLE_USED_GLOBAL_VAR)
void CanCopyToCan(CanChipDataPtr dst, V_MEMRAM1_FAR void V_MEMRAM2_FAR V_MEMRAM3_FAR *src, vuint8 len)       /* PRQA S 3673 */     /* suppress message about const pointer */
#  else
void CanCopyToCan(CanChipDataPtr dst, void *src, vuint8 len)       /* PRQA S 3673 */     /* suppress message about const pointer */
#  endif
{
#  if defined(C_COMP_COSMIC_MCS12X_MSCAN12)  
#   if defined (V_ENABLE_USED_GLOBAL_VAR)
  VStdMemCpyFarRamToRam(dst, src, len);
#   else
  VStdMemCpyRamToRam(dst, src, len);
#   endif
#  endif
}
/* CODE CATEGORY 1 END*/
# endif

# if defined ( C_ENABLE_DRIVER_STATUS )
/************************************************************************
| NAME:             CanGetDriverStatus
| CALLED BY:        Application
| PRECONDITIONS:    none  
| INPUT PARAMETER:  CanChannelHandle   channel    | 
| RETURN VALUE:     kCanDriverBusoff:     CAN Driver is in state Busoff, 
|                                         Init mode is not reached jet.
|                   kCanDriverBusoffInit: init mode is reached, CanResetBusoffEnd
|                                         can be processed without delay on entry. 
|                   kCanDriverNormal:     init mode is already left. CanResetBusoffEnd
|                                         can be processed without delay on entry 
|                                         (if not already performed). 
|
| DESCRIPTION:      The API function provide the current state of the 
|                   bus off recovery. This allows to call CanResetBusOffEnd
|                   more early to be able to receive messages as soon 
|                   as possible after Busoff.
*************************************************************************/
/* CODE CATEGORY 4 START*/
vuint8 CanGetDriverStatus(CAN_CHANNEL_CANTYPE_ONLY)
{
  vuint8 retVal;


  /* check whether still in bus-off recovery */
  if((CRFLG & BOFFIF) == BOFFIF)
  {
    /* the channel is bus-off, look for the status changed flag */
    if((CRFLG & CSCIF) == CSCIF)
    {
      /* check status flag was not yet cleared (ApplCanBusOff() was not yet called) */
      retVal = kCanDriverBusoff;
    }
    else
    {
      /* CanInit was called(), wait until the recovery is done, CanResetBusOffEnd has no effect on this platform at the macro is empty */
      retVal = kCanDriverBusoffInit;
    }
  }
  else
  {
    /* the channel was not bus-off, or the recovery was completed */
    retVal = kCanDriverNormal;
  }

  return retVal;
}
/* CODE CATEGORY 4 END*/
# endif /* C_ENABLE_DRIVER_STATUS         */

#if defined( C_ENABLE_SLEEP_WAKEUP )
/****************************************************************************
| NAME:             CanLL_WakeUpHandling
| CALLED BY:        CanWakeUpTask() and WakeUp-ISR
| PRECONDITIONS:    none
|
| INPUT PARAMETERS: CanChannelHandle  channel        | current CAN channel
|                   and parameters which are defined in CanLL_WakeUpTaskLocalParameter
|
| RETURN VALUES:    -
|
| DESCRIPTION:      perform wakeup handling.
|
| ATTENTION:        CanLL_WakeUpHandling has to contain:
|                     ApplCanPreWakeUp(CAN_CHANNEL_CANPARA_ONLY);
|                     CanWakeUp(CAN_CHANNEL_CANPARA_ONLY);        - if hardware does not wake up automatically
|                     APPL_CAN_WAKEUP( channel );
|                   In case of CommonCAN, it might be necessary to call CanWakeUp even if the hardware
|                   wakes up automatically to make sure all associated HW channels are awaken.
****************************************************************************/
/* CODE CATEGORY 4 START*/
static void CanLL_WakeUpHandling(CAN_CHANNEL_CANTYPE_ONLY)
{
# if defined( C_ENABLE_APPLCANPREWAKEUP_FCT )
  ApplCanPreWakeUp(CAN_CHANNEL_CANPARA_ONLY);
# endif
  (void)CanWakeUp(CAN_CHANNEL_CANPARA_ONLY);   /* if hardware does not wake up automatically */
  CRFLG = WUPIF; /* acknowledge wakeup flag */
  APPL_CAN_WAKEUP( channel );
}
/* CODE CATEGORY 4 END*/
#endif

# if defined( C_ENABLE_CAN_RAM_CHECK )
/****************************************************************************
| NAME:             CanLL_IsMailboxCorrupt
| CALLED BY:        CanCheckMemory()
| PRECONDITIONS:    none
|
| INPUT PARAMETERS: CanChannelHandle  channel        | current CAN channel
|                   CanObjectHandle   hwObjHandle    | Handle to hardware object
|
| RETURN VALUES:    kCanTrue:  Mailbox is corrupt
|                   kCanFalse: Mailbox is not corrupt  
|
| DESCRIPTION:      check the current mailbox at index hwObjHandle
****************************************************************************/
/* CODE CATEGORY 4 START*/
static vuint8 CanLL_IsMailboxCorrupt(CAN_HW_CHANNEL_CANTYPE_FIRST CanObjectHandle hwObjHandle)
{
  vuint8 i;
#  if defined(C_COMP_COSMIC_MCS12X_MSCAN12)  
  vuint8 j;
#  endif
  vuint8 returnVal;

  returnVal = kCanFalse;

  if(hwObjHandle != 0)
  {
    CTBSEL = (vuint8)hwObjHandle;

	for(i = 0; i < 3; i++)
    {
#  if defined(C_ENABLE_EXTENDED_ID)
      CAN_TX_MAILBOX_BASIS_ADR(channel) -> Id         = CanMemCheckId[i];
#  else
      CAN_TX_MAILBOX_BASIS_ADR(channel) -> Id         = CanMemCheckId[i];
      CAN_TX_MAILBOX_BASIS_ADR(channel) -> IdEx       = CanMemCheckId[i];
#  endif

#  if defined(C_COMP_COSMIC_MCS12X_MSCAN12)   
      for(j = 0; j < 8; j++)
      {
        CAN_TX_MAILBOX_BASIS_ADR(channel)->DataFld[j] = CanMemCheckData[i];
      }
#  endif

      CAN_TX_MAILBOX_BASIS_ADR(channel) -> Dlc        = CanMemCheckDlc[i];
      CAN_TX_MAILBOX_BASIS_ADR(channel) -> Prio       = CanMemCheckPrio[i];

#  if defined(C_ENABLE_EXTENDED_ID)
      if(CAN_TX_MAILBOX_BASIS_ADR(channel)  -> Id    != CanMemCheckId[i])
#  else
      if((CAN_TX_MAILBOX_BASIS_ADR(channel) -> Id    != CanMemCheckId[i]) ||
         (CAN_TX_MAILBOX_BASIS_ADR(channel) -> IdEx  != CanMemCheckId[i]))
#  endif
      {
        returnVal = kCanTrue;
      }

#  if defined(C_COMP_COSMIC_MCS12X_MSCAN12)  
      for(j = 0; j < 8; j++)
      {
        if(CAN_TX_MAILBOX_BASIS_ADR(channel) -> DataFld[j] != CanMemCheckData[i])
        {
          returnVal = kCanTrue;
        }
      }
#  endif

      if(CAN_TX_MAILBOX_BASIS_ADR(channel) -> Dlc          != CanMemCheckDlc[i])
      {
        returnVal = kCanTrue;
      }
      if(CAN_TX_MAILBOX_BASIS_ADR(channel) -> Prio         != CanMemCheckPrio[i])
      {
        returnVal = kCanTrue;
      }
    }
  }

  return returnVal;
}
/* CODE CATEGORY 4 END*/
# endif

#if ((defined( C_ENABLE_RX_BASICCAN_OBJECTS ) && !defined( C_ENABLE_RX_BASICCAN_POLLING )) || \
     !defined( C_ENABLE_TX_POLLING )        || \
      defined( C_ENABLE_INDIVIDUAL_POLLING ) || \
      !defined( C_ENABLE_ERROR_POLLING )     ||\
      !defined( C_ENABLE_WAKEUP_POLLING ))        /* ISR necessary; no pure polling configuration*/
/****************************************************************************
| NAME:             Interrupt Functions
| CALLED BY:
| PRECONDITIONS:
| INPUT PARAMETERS: canHwChannel
| RETURN VALUES:    none
| DESCRIPTION:      Interrupt service functions according to the CAN controller
|                   interrupt stucture
|                   - check for the interrupt reason ( interrupt source )
|                   - work appropriate interrupt:
|                     + status/error interrupt (busoff, wakeup, error warning)
|                     + basic can receive
|                     + full can receive
|                     + can transmit
|
|                   If an Rx-Interrupt occurs while the CAN controller is in Sleep mode, 
|                   a wakeup has to be generated. 
|
|                   If an Tx-Interrupt occurs while the CAN controller is in Sleep mode, 
|                   an assertion has to be called and the interrupt has to be ignored.
|
|                   The name of BrsTimeStrt...() and BrsTimeStop...() can be addapted to 
|                   realy used name of the interrupt functions.
|
****************************************************************************/

# if defined(C_COMP_COSMIC_MCS12X_MSCAN12)  
#  if defined(C_ENABLE_ISR_SPLIT)

#   if !defined(C_ENABLE_RX_BASICCAN_POLLING)
/****************************************************************************
| NAME:             CanRxIrqHandler
| CALLED BY:        Can Rx interrupt 
| PRECONDITIONS:    Rx interrupt enabled
| INPUT PARAMETERS: CAN channel on which the interrupt was fired
| RETURN VALUES:    none
| DESCRIPTION:      code doubled to indexed IRQ handling
****************************************************************************/
/* CODE CATEGORY 1 START*/
C_API_1 void C_API_2 CanRxIrqHandler(CAN_HW_CHANNEL_CANTYPE_ONLY)
{
  CanBasicCanMsgReceived(CAN_CHANNEL_CANPARA_FIRST 0);
}
/* CODE CATEGORY 1 END*/
#   endif

#   if !defined(C_ENABLE_TX_POLLING)
/****************************************************************************
| NAME:             CanTxIrqHandler
| CALLED BY:        Can Tx interrupt 
| PRECONDITIONS:    Tx interrupt enabled
| INPUT PARAMETERS: CAN channel on which the interrupt was fired
| RETURN VALUES:    none
| DESCRIPTION:      code doubled to indexed IRQ handling
****************************************************************************/
/* CODE CATEGORY 1 START*/
C_API_1 void C_API_2 CanTxIrqHandler(CAN_HW_CHANNEL_CANTYPE_ONLY)
{
  CanHL_TxConfirmation(CAN_CHANNEL_CANPARA_FIRST CanFirstMailbox[CTIER & CANTFLG]);
}
/* CODE CATEGORY 1 END*/
#   endif

#   if !defined(C_ENABLE_ERROR_POLLING)
/****************************************************************************
| NAME:             CanErrorIrqHandler
| CALLED BY:        Can Error interrupt 
| PRECONDITIONS:    Error interrupt enabled
| INPUT PARAMETERS: CAN channel on which the interrupt was fired
| RETURN VALUES:    none
| DESCRIPTION:      code doubled to indexed IRQ handling
****************************************************************************/
/* CODE CATEGORY 1 START*/
C_API_1 void C_API_2 CanErrorIrqHandler(CAN_HW_CHANNEL_CANTYPE_ONLY)
{
  CanHL_ErrorHandling(CAN_CHANNEL_CANPARA_ONLY);
}
/* CODE CATEGORY 1 END*/
#   endif

#   if defined(C_ENABLE_SLEEP_WAKEUP)
#    if !defined(C_ENABLE_WAKEUP_POLLING)
/****************************************************************************
| NAME:             CanWakeUpIrqHandler
| CALLED BY:        Can Wakeup interrupt 
| PRECONDITIONS:    Wakeup interrupt enabled
| INPUT PARAMETERS: CAN channel on which the interrupt was fired
| RETURN VALUES:    none
| DESCRIPTION:      code doubled to indexed IRQ handling
****************************************************************************/
/* CODE CATEGORY 1 START*/
C_API_1 void C_API_2 CanWakeUpIrqHandler(CAN_HW_CHANNEL_CANTYPE_ONLY)
{
  CanLL_WakeUpHandling(CAN_CHANNEL_CANPARA_ONLY);
}
/* CODE CATEGORY 1 END*/
#    endif
#   endif

#  endif /* C_COMP_COSMIC_MCS12X_MSCAN12... */
# endif /* C_ENABLE_ISR_SPLIT */
#endif /* C_ENABLE_RX_BASICCAN_OBJECTS... */
#if ((defined( C_ENABLE_RX_BASICCAN_OBJECTS ) && !defined( C_ENABLE_RX_BASICCAN_POLLING )) || \
     !defined( C_ENABLE_TX_POLLING )        || \
      defined( C_ENABLE_INDIVIDUAL_POLLING ) || \
      !defined( C_ENABLE_ERROR_POLLING )     ||\
      !defined( C_ENABLE_WAKEUP_POLLING ))        /* ISR necessary; no pure polling configuration*/
/****************************************************************************
| NAME:             CanIsr
| CALLED BY:        HLL,Asr: Interrupt
| PRECONDITIONS:
| INPUT PARAMETERS: none
| RETURN VALUES:    none
| DESCRIPTION:      Interrupt service functions according to the CAN controller
|                   interrupt stucture
|
|  Attention:  Name has to be repleaced with the name of the ISR. 
|              Naming conventions: with Name = "","Rx","Tx","RxTx","Wakeup","Status" 
|  The name of the ISR will always have a channel index at the end. Even in single channel
|  systems.
****************************************************************************/

# if defined(C_COMP_COSMIC_MCS12X_MSCAN12)  


#   if defined(C_ENABLE_ISR_SPLIT)
#   else /*C_ENABLE_ISR_SPLIT */


#    if defined(kCanPhysToLogChannelIndex_0)
/****************************************************************************
| NAME:             CanRxInterrupt_0
| CALLED BY:
| PRECONDITIONS:    Rx interrupt has to be enabled
| INPUT PARAMETERS: none
| RETURN VALUES:    none
| DESCRIPTION:      MS-CAN Rx interrupt function
****************************************************************************/

#     if (!defined(C_ENABLE_RX_BASICCAN_POLLING) && defined(C_ENABLE_RX_BASICCAN_OBJECTS)) || \
         (defined(C_ENABLE_RX_BASICCAN_POLLING) && defined(C_ENABLE_INDIVIDUAL_POLLING) && defined(C_ENABLE_RX_BASICCAN_OBJECTS))
#      if defined(C_ENABLE_OSEK_OS) && defined(C_ENABLE_OSEK_OS_INTCAT2)
#       if defined(osdIsrCanRxInterrupt_0Cat)
#        if (osdIsrCanRxInterrupt_0Cat != 2)
#         error "inconstant configuration of Osek-OS interrupt categorie between CANgen and OIL-configurator (CanIsr)"
#        endif
#       endif
ISR(CanRxInterrupt_0)
#      else
#       if defined(C_ENABLE_OSEK_OS) && defined (osdIsrCanRxInterrupt_0Cat)
#        if (osdIsrCanRxInterrupt_0Cat != 1)
#         error "inconstant configuration of Osek-OS interrupt categorie between CANgen and OIL-configurator (CanIsr)"
#        endif
#       endif
CAN_RX_IRQ_FUNC(CanRxInterrupt_0)
#      endif /* C_ENABLE_OSEK_OS */
{
#      if defined(C_SINGLE_RECEIVE_CHANNEL)
  CanBasicCanMsgReceived(0);
#      else
  CanBasicCanMsgReceived((CanChannelHandle)kCanPhysToLogChannelIndex_0, 0);
#      endif
}
#     endif


/****************************************************************************
| NAME:             CanTxInterrupt_0
| CALLED BY:
| PRECONDITIONS:    Tx interrupt has to be enabled
| INPUT PARAMETERS: none
| RETURN VALUES:    none
| DESCRIPTION:      MS-CAN Tx interrupt function
****************************************************************************/

#     if (!defined(C_ENABLE_TX_POLLING)) || (defined(C_ENABLE_TX_POLLING) && defined(C_ENABLE_INDIVIDUAL_POLLING))
#      if defined(C_ENABLE_OSEK_OS) && defined(C_ENABLE_OSEK_OS_INTCAT2)
#       if defined(osdIsrCanTxInterrupt_0Cat)
#        if (osdIsrCanTxInterrupt_0Cat != 2)
#         error "inconstant configuration of Osek-OS interrupt categorie between CANgen and OIL-configurator (CanIsr)"
#        endif
#       endif
ISR(CanTxInterrupt_0)
#      else
#       if defined(C_ENABLE_OSEK_OS) && defined (osdIsrCanTxInterrupt_0Cat)
#        if (osdIsrCanTxInterrupt_0Cat != 1)
#         error "inconstant configuration of Osek-OS interrupt categorie between CANgen and OIL-configurator (CanIsr)"
#        endif
#       endif
CAN_TX_IRQ_FUNC(CanTxInterrupt_0)
#       endif /* C_ENABLE_OSEK_OS */
{
#      if defined(C_SINGLE_RECEIVE_CHANNEL)
  CanHL_TxConfirmation(CanFirstMailbox[CTIER & CANTFLG & kCanPendingTxObjMask]);
#      else
  CanChannelHandle channel;
  channel = (CanChannelHandle)kCanPhysToLogChannelIndex_0;
  CanHL_TxConfirmation(channel, CanFirstMailbox[CTIER & CANTFLG & kCanPendingTxObjMask]);
#      endif
}
#     endif


/****************************************************************************
| NAME:             CanWakeUpInterrupt_0
| CALLED BY:
| PRECONDITIONS:    Wakeup interrupt has to be enabled
| INPUT PARAMETERS: none
| RETURN VALUES:    none
| DESCRIPTION:      MS-CAN wakeup interrupt function
****************************************************************************/

#     if defined(C_ENABLE_SLEEP_WAKEUP)
#      if defined(C_ENABLE_OSEK_OS) && defined(C_ENABLE_OSEK_OS_INTCAT2)
#       if defined(osdIsrCanWakeUpInterrupt_0Cat)
#        if (osdIsrCanWakeUpInterrupt_0Cat != 2)
#         error "inconstant configuration of Osek-OS interrupt categorie between CANgen and OIL-configurator (CanIsr)"
#        endif
#       endif
ISR(CanWakeUpInterrupt_0)
#      else
#       if defined(C_ENABLE_OSEK_OS) && defined (osdIsrCanWakeUpInterrupt_0Cat)
#        if (osdIsrCanWakeUpInterrupt_0Cat != 1)
#         error "inconstant configuration of Osek-OS interrupt categorie between CANgen and OIL-configurator (CanIsr)"
#        endif
#       endif
CAN_WAKEUP_IRQ_FUNC(CanWakeUpInterrupt_0)
#      endif /* C_ENABLE_OSEK_OS */
{
#      if defined(C_SINGLE_RECEIVE_CHANNEL)
  CanLL_WakeUpHandling();
#      else
  CanLL_WakeUpHandling((CanChannelHandle)kCanPhysToLogChannelIndex_0);
#      endif
}
#     endif


/****************************************************************************
| NAME:             CanErrorInterrupt_0
| CALLED BY:
| PRECONDITIONS:    Error interrupt has to be enabled
| INPUT PARAMETERS: none
| RETURN VALUES:    none
| DESCRIPTION:      MS-CAN error interrupt function
****************************************************************************/

#     if !defined(C_ENABLE_ERROR_POLLING)
#      if defined(C_ENABLE_OSEK_OS) && defined(C_ENABLE_OSEK_OS_INTCAT2)
#       if defined(osdIsrCanErrorInterrupt_0Cat)
#        if (osdIsrCanErrorInterrupt_0Cat != 2)
#         error "inconstant configuration of Osek-OS interrupt categorie between CANgen and OIL-configurator (CanIsr)"
#        endif
#       endif
ISR(CanErrorInterrupt_0)
#      else
#       if defined(C_ENABLE_OSEK_OS) && defined (osdIsrCanErrorInterrupt_0Cat)
#        if (osdIsrCanErrorInterrupt_0Cat != 1)
#         error "inconstant configuration of Osek-OS interrupt categorie between CANgen and OIL-configurator (CanIsr)"
#        endif
#       endif
CAN_ERROR_IRQ_FUNC(CanErrorInterrupt_0)
#      endif /* C_ENABLE_OSEK_OS */
{
#      if defined(C_SINGLE_RECEIVE_CHANNEL)
  CanHL_ErrorHandling();
#      else
  CanHL_ErrorHandling((CanChannelHandle)kCanPhysToLogChannelIndex_0);
#      endif
}
#     endif


#    endif /* kCanPhysToLogChannelIndex_0 */


#    if defined(kCanPhysToLogChannelIndex_1)
/****************************************************************************
| NAME:             CanRxInterrupt_1
| CALLED BY:
| PRECONDITIONS:    Rx interrupt has to be enabled
| INPUT PARAMETERS: none
| RETURN VALUES:    none
| DESCRIPTION:      MS-CAN Rx interrupt function
****************************************************************************/

#     if (!defined(C_ENABLE_RX_BASICCAN_POLLING) && defined(C_ENABLE_RX_BASICCAN_OBJECTS)) || \
         (defined(C_ENABLE_RX_BASICCAN_POLLING) && defined(C_ENABLE_INDIVIDUAL_POLLING) && defined(C_ENABLE_RX_BASICCAN_OBJECTS))
#      if defined(C_ENABLE_OSEK_OS) && defined(C_ENABLE_OSEK_OS_INTCAT2)
#       if defined(osdIsrCanRxInterrupt_1Cat)
#        if (osdIsrCanRxInterrupt_1Cat != 2)
#         error "inconstant configuration of Osek-OS interrupt categorie between CANgen and OIL-configurator (CanIsr)"
#        endif
#       endif
ISR(CanRxInterrupt_1)
#      else
#       if defined(C_ENABLE_OSEK_OS) && defined (osdIsrCanRxInterrupt_1Cat)
#        if (osdIsrCanRxInterrupt_1Cat != 1)
#         error "inconstant configuration of Osek-OS interrupt categorie between CANgen and OIL-configurator (CanIsr)"
#        endif
#       endif
CAN_RX_IRQ_FUNC(CanRxInterrupt_1)
#      endif /* C_ENABLE_OSEK_OS */
{
#      if defined(C_SINGLE_RECEIVE_CHANNEL)
  CanBasicCanMsgReceived(0);
#      else
  CanBasicCanMsgReceived((CanChannelHandle)kCanPhysToLogChannelIndex_1, 0);
#      endif
}
#     endif


/****************************************************************************
| NAME:             CanTxInterrupt_1
| CALLED BY:
| PRECONDITIONS:    Tx interrupt has to be enabled
| INPUT PARAMETERS: none
| RETURN VALUES:    none
| DESCRIPTION:      MS-CAN Tx interrupt function
****************************************************************************/

#     if (!defined(C_ENABLE_TX_POLLING)) || (defined(C_ENABLE_TX_POLLING) && defined(C_ENABLE_INDIVIDUAL_POLLING))
#      if defined(C_ENABLE_OSEK_OS) && defined(C_ENABLE_OSEK_OS_INTCAT2)
#       if defined(osdIsrCanTxInterrupt_1Cat)
#        if (osdIsrCanTxInterrupt_1Cat != 2)
#         error "inconstant configuration of Osek-OS interrupt categorie between CANgen and OIL-configurator (CanIsr)"
#        endif
#       endif
ISR(CanTxInterrupt_1)
#      else
#       if defined(C_ENABLE_OSEK_OS) && defined (osdIsrCanTxInterrupt_1Cat)
#        if (osdIsrCanTxInterrupt_1Cat != 1)
#         error "inconstant configuration of Osek-OS interrupt categorie between CANgen and OIL-configurator (CanIsr)"
#        endif
#       endif
CAN_TX_IRQ_FUNC(CanTxInterrupt_1)
#       endif /* C_ENABLE_OSEK_OS */
{
#      if defined(C_SINGLE_RECEIVE_CHANNEL)
  CanHL_TxConfirmation(CanFirstMailbox[CTIER & CANTFLG & kCanPendingTxObjMask]);
#      else
  CanChannelHandle channel;
  channel = (CanChannelHandle)kCanPhysToLogChannelIndex_1;
  CanHL_TxConfirmation(channel, CanFirstMailbox[CTIER & CANTFLG & kCanPendingTxObjMask]);
#      endif
}
#     endif


/****************************************************************************
| NAME:             CanWakeUpInterrupt_1
| CALLED BY:
| PRECONDITIONS:    Wakeup interrupt has to be enabled
| INPUT PARAMETERS: none
| RETURN VALUES:    none
| DESCRIPTION:      MS-CAN wakeup interrupt function
****************************************************************************/

#     if defined(C_ENABLE_SLEEP_WAKEUP)
#      if defined(C_ENABLE_OSEK_OS) && defined(C_ENABLE_OSEK_OS_INTCAT2)
#       if defined(osdIsrCanWakeUpInterrupt_1Cat)
#        if (osdIsrCanWakeUpInterrupt_1Cat != 2)
#         error "inconstant configuration of Osek-OS interrupt categorie between CANgen and OIL-configurator (CanIsr)"
#        endif
#       endif
ISR(CanWakeUpInterrupt_1)
#      else
#       if defined(C_ENABLE_OSEK_OS) && defined (osdIsrCanWakeUpInterrupt_1Cat)
#        if (osdIsrCanWakeUpInterrupt_1Cat != 1)
#         error "inconstant configuration of Osek-OS interrupt categorie between CANgen and OIL-configurator (CanIsr)"
#        endif
#       endif
CAN_WAKEUP_IRQ_FUNC(CanWakeUpInterrupt_1)
#      endif /* C_ENABLE_OSEK_OS */
{
#      if defined(C_SINGLE_RECEIVE_CHANNEL)
  CanLL_WakeUpHandling();
#      else
  CanLL_WakeUpHandling((CanChannelHandle)kCanPhysToLogChannelIndex_1);
#      endif
}
#     endif


/****************************************************************************
| NAME:             CanErrorInterrupt_1
| CALLED BY:
| PRECONDITIONS:    Error interrupt has to be enabled
| INPUT PARAMETERS: none
| RETURN VALUES:    none
| DESCRIPTION:      MS-CAN error interrupt function
****************************************************************************/

#     if !defined(C_ENABLE_ERROR_POLLING)
#      if defined(C_ENABLE_OSEK_OS) && defined(C_ENABLE_OSEK_OS_INTCAT2)
#       if defined(osdIsrCanErrorInterrupt_1Cat)
#        if (osdIsrCanErrorInterrupt_1Cat != 2)
#         error "inconstant configuration of Osek-OS interrupt categorie between CANgen and OIL-configurator (CanIsr)"
#        endif
#       endif
ISR(CanErrorInterrupt_1)
#      else
#       if defined(C_ENABLE_OSEK_OS) && defined (osdIsrCanErrorInterrupt_1Cat)
#        if (osdIsrCanErrorInterrupt_1Cat != 1)
#         error "inconstant configuration of Osek-OS interrupt categorie between CANgen and OIL-configurator (CanIsr)"
#        endif
#       endif
CAN_ERROR_IRQ_FUNC(CanErrorInterrupt_1)
#      endif /* C_ENABLE_OSEK_OS */
{
#      if defined(C_SINGLE_RECEIVE_CHANNEL)
  CanHL_ErrorHandling();
#      else
  CanHL_ErrorHandling((CanChannelHandle)kCanPhysToLogChannelIndex_1);
#      endif
}
#     endif


#    endif /* kCanPhysToLogChannelIndex_1 */


#    if defined(kCanPhysToLogChannelIndex_2)
/****************************************************************************
| NAME:             CanRxInterrupt_2
| CALLED BY:
| PRECONDITIONS:    Rx interrupt has to be enabled
| INPUT PARAMETERS: none
| RETURN VALUES:    none
| DESCRIPTION:      MS-CAN Rx interrupt function
****************************************************************************/

#     if (!defined(C_ENABLE_RX_BASICCAN_POLLING) && defined(C_ENABLE_RX_BASICCAN_OBJECTS)) || \
         (defined(C_ENABLE_RX_BASICCAN_POLLING) && defined(C_ENABLE_INDIVIDUAL_POLLING) && defined(C_ENABLE_RX_BASICCAN_OBJECTS))
#      if defined(C_ENABLE_OSEK_OS) && defined(C_ENABLE_OSEK_OS_INTCAT2)
#       if defined(osdIsrCanRxInterrupt_2Cat)
#        if (osdIsrCanRxInterrupt_2Cat != 2)
#         error "inconstant configuration of Osek-OS interrupt categorie between CANgen and OIL-configurator (CanIsr)"
#        endif
#       endif
ISR(CanRxInterrupt_2)
#      else
#       if defined(C_ENABLE_OSEK_OS) && defined (osdIsrCanRxInterrupt_2Cat)
#        if (osdIsrCanRxInterrupt_2Cat != 1)
#         error "inconstant configuration of Osek-OS interrupt categorie between CANgen and OIL-configurator (CanIsr)"
#        endif
#       endif
CAN_RX_IRQ_FUNC(CanRxInterrupt_2)
#      endif /* C_ENABLE_OSEK_OS */
{
#      if defined(C_SINGLE_RECEIVE_CHANNEL)
  CanBasicCanMsgReceived(0);
#      else
  CanBasicCanMsgReceived((CanChannelHandle)kCanPhysToLogChannelIndex_2, 0);
#      endif
}
#     endif


/****************************************************************************
| NAME:             CanTxInterrupt_2
| CALLED BY:
| PRECONDITIONS:    Tx interrupt has to be enabled
| INPUT PARAMETERS: none
| RETURN VALUES:    none
| DESCRIPTION:      MS-CAN Tx interrupt function
****************************************************************************/

#     if (!defined(C_ENABLE_TX_POLLING)) || (defined(C_ENABLE_TX_POLLING) && defined(C_ENABLE_INDIVIDUAL_POLLING))
#      if defined(C_ENABLE_OSEK_OS) && defined(C_ENABLE_OSEK_OS_INTCAT2)
#       if defined(osdIsrCanTxInterrupt_2Cat)
#        if (osdIsrCanTxInterrupt_2Cat != 2)
#         error "inconstant configuration of Osek-OS interrupt categorie between CANgen and OIL-configurator (CanIsr)"
#        endif
#       endif
ISR(CanTxInterrupt_2)
#      else
#       if defined(C_ENABLE_OSEK_OS) && defined (osdIsrCanTxInterrupt_2Cat)
#        if (osdIsrCanTxInterrupt_2Cat != 1)
#         error "inconstant configuration of Osek-OS interrupt categorie between CANgen and OIL-configurator (CanIsr)"
#        endif
#       endif
CAN_TX_IRQ_FUNC(CanTxInterrupt_2)
#       endif /* C_ENABLE_OSEK_OS */
{
#      if defined(C_SINGLE_RECEIVE_CHANNEL)
  CanHL_TxConfirmation(CanFirstMailbox[CTIER & CANTFLG & kCanPendingTxObjMask]);
#      else
  CanChannelHandle channel;
  channel = (CanChannelHandle)kCanPhysToLogChannelIndex_2;
  CanHL_TxConfirmation(channel, CanFirstMailbox[CTIER & CANTFLG & kCanPendingTxObjMask]);
#      endif
}
#     endif


/****************************************************************************
| NAME:             CanWakeUpInterrupt_2
| CALLED BY:
| PRECONDITIONS:    Wakeup interrupt has to be enabled
| INPUT PARAMETERS: none
| RETURN VALUES:    none
| DESCRIPTION:      MS-CAN wakeup interrupt function
****************************************************************************/

#     if defined(C_ENABLE_SLEEP_WAKEUP)
#      if defined(C_ENABLE_OSEK_OS) && defined(C_ENABLE_OSEK_OS_INTCAT2)
#       if defined(osdIsrCanWakeUpInterrupt_2Cat)
#        if (osdIsrCanWakeUpInterrupt_2Cat != 2)
#         error "inconstant configuration of Osek-OS interrupt categorie between CANgen and OIL-configurator (CanIsr)"
#        endif
#       endif
ISR(CanWakeUpInterrupt_2)
#      else
#       if defined(C_ENABLE_OSEK_OS) && defined (osdIsrCanWakeUpInterrupt_2Cat)
#        if (osdIsrCanWakeUpInterrupt_2Cat != 1)
#         error "inconstant configuration of Osek-OS interrupt categorie between CANgen and OIL-configurator (CanIsr)"
#        endif
#       endif
CAN_WAKEUP_IRQ_FUNC(CanWakeUpInterrupt_2)
#      endif /* C_ENABLE_OSEK_OS */
{
#      if defined(C_SINGLE_RECEIVE_CHANNEL)
  CanLL_WakeUpHandling();
#      else
  CanLL_WakeUpHandling((CanChannelHandle)kCanPhysToLogChannelIndex_2);
#      endif
}
#     endif


/****************************************************************************
| NAME:             CanErrorInterrupt_2
| CALLED BY:
| PRECONDITIONS:    Error interrupt has to be enabled
| INPUT PARAMETERS: none
| RETURN VALUES:    none
| DESCRIPTION:      MS-CAN error interrupt function
****************************************************************************/

#     if !defined(C_ENABLE_ERROR_POLLING)
#      if defined(C_ENABLE_OSEK_OS) && defined(C_ENABLE_OSEK_OS_INTCAT2)
#       if defined(osdIsrCanErrorInterrupt_2Cat)
#        if (osdIsrCanErrorInterrupt_2Cat != 2)
#         error "inconstant configuration of Osek-OS interrupt categorie between CANgen and OIL-configurator (CanIsr)"
#        endif
#       endif
ISR(CanErrorInterrupt_2)
#      else
#       if defined(C_ENABLE_OSEK_OS) && defined (osdIsrCanErrorInterrupt_2Cat)
#        if (osdIsrCanErrorInterrupt_2Cat != 1)
#         error "inconstant configuration of Osek-OS interrupt categorie between CANgen and OIL-configurator (CanIsr)"
#        endif
#       endif
CAN_ERROR_IRQ_FUNC(CanErrorInterrupt_2)
#      endif /* C_ENABLE_OSEK_OS */
{
#      if defined(C_SINGLE_RECEIVE_CHANNEL)
  CanHL_ErrorHandling();
#      else
  CanHL_ErrorHandling((CanChannelHandle)kCanPhysToLogChannelIndex_2);
#      endif
}
#     endif


#    endif /* kCanPhysToLogChannelIndex_2 */


#    if defined(kCanPhysToLogChannelIndex_3)
/****************************************************************************
| NAME:             CanRxInterrupt_3
| CALLED BY:
| PRECONDITIONS:    Rx interrupt has to be enabled
| INPUT PARAMETERS: none
| RETURN VALUES:    none
| DESCRIPTION:      MS-CAN Rx interrupt function
****************************************************************************/

#     if (!defined(C_ENABLE_RX_BASICCAN_POLLING) && defined(C_ENABLE_RX_BASICCAN_OBJECTS)) || \
         (defined(C_ENABLE_RX_BASICCAN_POLLING) && defined(C_ENABLE_INDIVIDUAL_POLLING) && defined(C_ENABLE_RX_BASICCAN_OBJECTS))
#      if defined(C_ENABLE_OSEK_OS) && defined(C_ENABLE_OSEK_OS_INTCAT2)
#       if defined(osdIsrCanRxInterrupt_3Cat)
#        if (osdIsrCanRxInterrupt_3Cat != 2)
#         error "inconstant configuration of Osek-OS interrupt categorie between CANgen and OIL-configurator (CanIsr)"
#        endif
#       endif
ISR(CanRxInterrupt_3)
#      else
#       if defined(C_ENABLE_OSEK_OS) && defined (osdIsrCanRxInterrupt_3Cat)
#        if (osdIsrCanRxInterrupt_3Cat != 1)
#         error "inconstant configuration of Osek-OS interrupt categorie between CANgen and OIL-configurator (CanIsr)"
#        endif
#       endif
CAN_RX_IRQ_FUNC(CanRxInterrupt_3)
#      endif /* C_ENABLE_OSEK_OS */
{
#      if defined(C_SINGLE_RECEIVE_CHANNEL)
  CanBasicCanMsgReceived(0);
#      else
  CanBasicCanMsgReceived((CanChannelHandle)kCanPhysToLogChannelIndex_3, 0);
#      endif
}
#     endif


/****************************************************************************
| NAME:             CanTxInterrupt_3
| CALLED BY:
| PRECONDITIONS:    Tx interrupt has to be enabled
| INPUT PARAMETERS: none
| RETURN VALUES:    none
| DESCRIPTION:      MS-CAN Tx interrupt function
****************************************************************************/

#     if (!defined(C_ENABLE_TX_POLLING)) || (defined(C_ENABLE_TX_POLLING) && defined(C_ENABLE_INDIVIDUAL_POLLING))
#      if defined(C_ENABLE_OSEK_OS) && defined(C_ENABLE_OSEK_OS_INTCAT2)
#       if defined(osdIsrCanTxInterrupt_3Cat)
#        if (osdIsrCanTxInterrupt_3Cat != 2)
#         error "inconstant configuration of Osek-OS interrupt categorie between CANgen and OIL-configurator (CanIsr)"
#        endif
#       endif
ISR(CanTxInterrupt_3)
#      else
#       if defined(C_ENABLE_OSEK_OS) && defined (osdIsrCanTxInterrupt_3Cat)
#        if (osdIsrCanTxInterrupt_3Cat != 1)
#         error "inconstant configuration of Osek-OS interrupt categorie between CANgen and OIL-configurator (CanIsr)"
#        endif
#       endif
CAN_TX_IRQ_FUNC(CanTxInterrupt_3)
#       endif /* C_ENABLE_OSEK_OS */
{
#      if defined(C_SINGLE_RECEIVE_CHANNEL)
  CanHL_TxConfirmation(CanFirstMailbox[CTIER & CANTFLG & kCanPendingTxObjMask]);
#      else
  CanChannelHandle channel;
  channel = (CanChannelHandle)kCanPhysToLogChannelIndex_3;
  CanHL_TxConfirmation(channel, CanFirstMailbox[CTIER & CANTFLG & kCanPendingTxObjMask]);
#      endif
}
#     endif


/****************************************************************************
| NAME:             CanWakeUpInterrupt_3
| CALLED BY:
| PRECONDITIONS:    Wakeup interrupt has to be enabled
| INPUT PARAMETERS: none
| RETURN VALUES:    none
| DESCRIPTION:      MS-CAN wakeup interrupt function
****************************************************************************/

#     if defined(C_ENABLE_SLEEP_WAKEUP)
#      if defined(C_ENABLE_OSEK_OS) && defined(C_ENABLE_OSEK_OS_INTCAT2)
#       if defined(osdIsrCanWakeUpInterrupt_3Cat)
#        if (osdIsrCanWakeUpInterrupt_3Cat != 2)
#         error "inconstant configuration of Osek-OS interrupt categorie between CANgen and OIL-configurator (CanIsr)"
#        endif
#       endif
ISR(CanWakeUpInterrupt_3)
#      else
#       if defined(C_ENABLE_OSEK_OS) && defined (osdIsrCanWakeUpInterrupt_3Cat)
#        if (osdIsrCanWakeUpInterrupt_3Cat != 1)
#         error "inconstant configuration of Osek-OS interrupt categorie between CANgen and OIL-configurator (CanIsr)"
#        endif
#       endif
CAN_WAKEUP_IRQ_FUNC(CanWakeUpInterrupt_3)
#      endif /* C_ENABLE_OSEK_OS */
{
#      if defined(C_SINGLE_RECEIVE_CHANNEL)
  CanLL_WakeUpHandling();
#      else
  CanLL_WakeUpHandling((CanChannelHandle)kCanPhysToLogChannelIndex_3);
#      endif
}
#     endif


/****************************************************************************
| NAME:             CanErrorInterrupt_3
| CALLED BY:
| PRECONDITIONS:    Error interrupt has to be enabled
| INPUT PARAMETERS: none
| RETURN VALUES:    none
| DESCRIPTION:      MS-CAN error interrupt function
****************************************************************************/

#     if !defined(C_ENABLE_ERROR_POLLING)
#      if defined(C_ENABLE_OSEK_OS) && defined(C_ENABLE_OSEK_OS_INTCAT2)
#       if defined(osdIsrCanErrorInterrupt_3Cat)
#        if (osdIsrCanErrorInterrupt_3Cat != 2)
#         error "inconstant configuration of Osek-OS interrupt categorie between CANgen and OIL-configurator (CanIsr)"
#        endif
#       endif
ISR(CanErrorInterrupt_3)
#      else
#       if defined(C_ENABLE_OSEK_OS) && defined (osdIsrCanErrorInterrupt_3Cat)
#        if (osdIsrCanErrorInterrupt_3Cat != 1)
#         error "inconstant configuration of Osek-OS interrupt categorie between CANgen and OIL-configurator (CanIsr)"
#        endif
#       endif
CAN_ERROR_IRQ_FUNC(CanErrorInterrupt_3)
#      endif /* C_ENABLE_OSEK_OS */
{
#      if defined(C_SINGLE_RECEIVE_CHANNEL)
  CanHL_ErrorHandling();
#      else
  CanHL_ErrorHandling((CanChannelHandle)kCanPhysToLogChannelIndex_3);
#      endif
}
#     endif


#    endif /* kCanPhysToLogChannelIndex_3 */


#    if defined(kCanPhysToLogChannelIndex_4)
/****************************************************************************
| NAME:             CanRxInterrupt_4
| CALLED BY:
| PRECONDITIONS:    Rx interrupt has to be enabled
| INPUT PARAMETERS: none
| RETURN VALUES:    none
| DESCRIPTION:      MS-CAN Rx interrupt function
****************************************************************************/

#     if (!defined(C_ENABLE_RX_BASICCAN_POLLING) && defined(C_ENABLE_RX_BASICCAN_OBJECTS)) || \
         (defined(C_ENABLE_RX_BASICCAN_POLLING) && defined(C_ENABLE_INDIVIDUAL_POLLING) && defined(C_ENABLE_RX_BASICCAN_OBJECTS))
#      if defined(C_ENABLE_OSEK_OS) && defined(C_ENABLE_OSEK_OS_INTCAT2)
#       if defined(osdIsrCanRxInterrupt_4Cat)
#        if (osdIsrCanRxInterrupt_4Cat != 2)
#         error "inconstant configuration of Osek-OS interrupt categorie between CANgen and OIL-configurator (CanIsr)"
#        endif
#       endif
ISR(CanRxInterrupt_4)
#      else
#       if defined(C_ENABLE_OSEK_OS) && defined (osdIsrCanRxInterrupt_4Cat)
#        if (osdIsrCanRxInterrupt_4Cat != 1)
#         error "inconstant configuration of Osek-OS interrupt categorie between CANgen and OIL-configurator (CanIsr)"
#        endif
#       endif
CAN_RX_IRQ_FUNC(CanRxInterrupt_4)
#      endif /* C_ENABLE_OSEK_OS */
{
#      if defined(C_SINGLE_RECEIVE_CHANNEL)
  CanBasicCanMsgReceived(0);
#      else
  CanBasicCanMsgReceived((CanChannelHandle)kCanPhysToLogChannelIndex_4, 0);
#      endif
}
#     endif


/****************************************************************************
| NAME:             CanTxInterrupt_4
| CALLED BY:
| PRECONDITIONS:    Tx interrupt has to be enabled
| INPUT PARAMETERS: none
| RETURN VALUES:    none
| DESCRIPTION:      MS-CAN Tx interrupt function
****************************************************************************/

#     if (!defined(C_ENABLE_TX_POLLING)) || (defined(C_ENABLE_TX_POLLING) && defined(C_ENABLE_INDIVIDUAL_POLLING))
#      if defined(C_ENABLE_OSEK_OS) && defined(C_ENABLE_OSEK_OS_INTCAT2)
#       if defined(osdIsrCanTxInterrupt_4Cat)
#        if (osdIsrCanTxInterrupt_4Cat != 2)
#         error "inconstant configuration of Osek-OS interrupt categorie between CANgen and OIL-configurator (CanIsr)"
#        endif
#       endif
ISR(CanTxInterrupt_4)
#      else
#       if defined(C_ENABLE_OSEK_OS) && defined (osdIsrCanTxInterrupt_4Cat)
#        if (osdIsrCanTxInterrupt_4Cat != 1)
#         error "inconstant configuration of Osek-OS interrupt categorie between CANgen and OIL-configurator (CanIsr)"
#        endif
#       endif
CAN_TX_IRQ_FUNC(CanTxInterrupt_4)
#       endif /* C_ENABLE_OSEK_OS */
{
#      if defined(C_SINGLE_RECEIVE_CHANNEL)
  CanHL_TxConfirmation(CanFirstMailbox[CTIER & CANTFLG & kCanPendingTxObjMask]);
#      else
  CanChannelHandle channel;
  channel = (CanChannelHandle)kCanPhysToLogChannelIndex_4;
  CanHL_TxConfirmation(channel, CanFirstMailbox[CTIER & CANTFLG & kCanPendingTxObjMask]);
#      endif
}
#     endif


/****************************************************************************
| NAME:             CanWakeUpInterrupt_4
| CALLED BY:
| PRECONDITIONS:    Wakeup interrupt has to be enabled
| INPUT PARAMETERS: none
| RETURN VALUES:    none
| DESCRIPTION:      MS-CAN wakeup interrupt function
****************************************************************************/

#     if defined(C_ENABLE_SLEEP_WAKEUP)
#      if defined(C_ENABLE_OSEK_OS) && defined(C_ENABLE_OSEK_OS_INTCAT2)
#       if defined(osdIsrCanWakeUpInterrupt_4Cat)
#        if (osdIsrCanWakeUpInterrupt_4Cat != 2)
#         error "inconstant configuration of Osek-OS interrupt categorie between CANgen and OIL-configurator (CanIsr)"
#        endif
#       endif
ISR(CanWakeUpInterrupt_4)
#      else
#       if defined(C_ENABLE_OSEK_OS) && defined (osdIsrCanWakeUpInterrupt_4Cat)
#        if (osdIsrCanWakeUpInterrupt_4Cat != 1)
#         error "inconstant configuration of Osek-OS interrupt categorie between CANgen and OIL-configurator (CanIsr)"
#        endif
#       endif
CAN_WAKEUP_IRQ_FUNC(CanWakeUpInterrupt_4)
#      endif /* C_ENABLE_OSEK_OS */
{
#      if defined(C_SINGLE_RECEIVE_CHANNEL)
  CanLL_WakeUpHandling();
#      else
  CanLL_WakeUpHandling((CanChannelHandle)kCanPhysToLogChannelIndex_4);
#      endif
}
#     endif


/****************************************************************************
| NAME:             CanErrorInterrupt_4
| CALLED BY:
| PRECONDITIONS:    Error interrupt has to be enabled
| INPUT PARAMETERS: none
| RETURN VALUES:    none
| DESCRIPTION:      MS-CAN error interrupt function
****************************************************************************/

#     if !defined(C_ENABLE_ERROR_POLLING)
#      if defined(C_ENABLE_OSEK_OS) && defined(C_ENABLE_OSEK_OS_INTCAT2)
#       if defined(osdIsrCanErrorInterrupt_4Cat)
#        if (osdIsrCanErrorInterrupt_4Cat != 2)
#         error "inconstant configuration of Osek-OS interrupt categorie between CANgen and OIL-configurator (CanIsr)"
#        endif
#       endif
ISR(CanErrorInterrupt_4)
#      else
#       if defined(C_ENABLE_OSEK_OS) && defined (osdIsrCanErrorInterrupt_4Cat)
#        if (osdIsrCanErrorInterrupt_4Cat != 1)
#         error "inconstant configuration of Osek-OS interrupt categorie between CANgen and OIL-configurator (CanIsr)"
#        endif
#       endif
CAN_ERROR_IRQ_FUNC(CanErrorInterrupt_4)
#      endif /* C_ENABLE_OSEK_OS */
{
#      if defined(C_SINGLE_RECEIVE_CHANNEL)
  CanHL_ErrorHandling();
#      else
  CanHL_ErrorHandling((CanChannelHandle)kCanPhysToLogChannelIndex_4);
#      endif
}
#     endif


#    endif /* kCanPhysToLogChannelIndex_4 */


#   endif /* C_ENABLE_ISR_SPLIT */


# endif /* C_COMP_COSMIC_MCS12X_MSCAN12... */



/* CODE CATEGORY 1 END*/
#endif

/****************************************************************************
| NAME:             CanInit
| CALLED BY:        CanInitPowerOn(), Network management
| PRECONDITIONS:    none
| INPUT PARAMETERS: Handle to initstructure
| RETURN VALUES:    none
| DESCRIPTION:      initialization of chip-hardware
|                   initialization of receive and transmit message objects
****************************************************************************/
/* CODE CATEGORY 4 START*/
C_API_1 void C_API_2 CanInit( CAN_CHANNEL_CANTYPE_FIRST CanInitHandle initObject )     /* PRQA S 1505 */
{
#if defined( C_ENABLE_CAN_RAM_CHECK )
  vuint8                 canRamCheckStatus;
#endif
  CanObjectHandle        hwObjHandle;
#if defined( C_ENABLE_CAN_CANCEL_NOTIFICATION ) || \
    defined( C_ENABLE_CAN_MSG_TRANSMIT_CANCEL_NOTIFICATION )
  CanTransmitHandle      txHandle;
#endif
  CanObjectHandle        logTxObjHandle;

  # if defined(C_COMP_COSMIC_MCS12X_MSCAN12)  
  #  if defined(C_SEARCH_XGATE)
  vsintx i;
  #  endif
  # endif
  vuint8 tmpCRIER;

#if defined( C_HL_ENABLE_LAST_INIT_OBJ )  
  lastInitObject[channel] = initObject;
#endif

#if defined( C_MULTIPLE_RECEIVE_CHANNEL )
/* Msg(4:3759) Implicit conversion: int to unsigned short. MISRA Rule 43 - no change in RI 1.4 */
  initObject  += CAN_HL_INIT_OBJ_STARTINDEX(channel);
#endif


#if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser(channel < kCanNumberOfChannels, kCanAllChannels, kErrorChannelHdlTooLarge);     /* PRQA S 3109 */
#endif
  assertUser(initObject < kCanNumberOfInitObjects, channel, kErrorInitObjectHdlTooLarge);    /* PRQA S 3109 */
  assertUser(initObject < CAN_HL_INIT_OBJ_STOPINDEX(channel), channel, kErrorInitObjectHdlTooLarge);     /* PRQA S 3109 */

#if defined( C_ENABLE_CAN_RAM_CHECK )
  canRamCheckStatus = kCanRamCheckOk;
#endif

  {

    /* request init mode */
    CTL0 = INITRQ;
    APPLCANTIMERSTART(kCanLoopInitReq);
    while( ((CTL1 & INITAK) == 0) && APPLCANTIMERLOOP(kCanLoopInitReq) )
    {
      ; /* wait while not in init mode */
    }
    APPLCANTIMEREND(kCanLoopInitReq);
    
    CTL1  = (vuint8)(CanInitObject[initObject].CanInitCTL1 | CANE);
    # if defined(C_ENABLE_ALTERNATE_CLKSRC)
    CTL1  |= (vuint8)CLKSRC;
    # endif
    
    CBTR0 = CanInitObject[initObject].CanInitCBTR0;     /* set baudrate */
    CBTR1 = CanInitObject[initObject].CanInitCBTR1;
    CIDAC = CanInitObject[initObject].CanInitCIDAC;     /* set filter mode */
    
    /* set acceptance filter registers */
    CIDAR0 = CanInitObject[initObject].CanInitCIDAR0;
    CIDAR1 = CanInitObject[initObject].CanInitCIDAR1;
    CIDAR2 = CanInitObject[initObject].CanInitCIDAR2;
    CIDAR3 = CanInitObject[initObject].CanInitCIDAR3;
    CIDMR0 = CanInitObject[initObject].CanInitCIDMR0;
    CIDMR1 = CanInitObject[initObject].CanInitCIDMR1;
    CIDMR2 = CanInitObject[initObject].CanInitCIDMR2;
    CIDMR3 = CanInitObject[initObject].CanInitCIDMR3;
    CIDAR4 = CanInitObject[initObject].CanInitCIDAR4;
    CIDAR5 = CanInitObject[initObject].CanInitCIDAR5;
    CIDAR6 = CanInitObject[initObject].CanInitCIDAR6;
    CIDAR7 = CanInitObject[initObject].CanInitCIDAR7;
    CIDMR4 = CanInitObject[initObject].CanInitCIDMR4;
    CIDMR5 = CanInitObject[initObject].CanInitCIDMR5;
    CIDMR6 = CanInitObject[initObject].CanInitCIDMR6;
    CIDMR7 = CanInitObject[initObject].CanInitCIDMR7;
    
    
    /* initial state of the Tx interrupts after reset */
    if(canCanIrqDisabled[channel] == kCanTrue) /* disable CAN interrupts? */
    {
      canCanInterruptOldStatus[channel].oldCanCTIER = (vuint8)0x00; /* disable all Tx interrupts */
    }
    else
    {
      CanBeginCriticalXGateSection();
      CTIER = (vuint8)0x00; /* disable all Tx interrupts */
      CanEndCriticalXGateSection();
    }
    
    # if defined(C_COMP_COSMIC_MCS12X_MSCAN12)  
    #  if defined(C_ENABLE_XGATE_USED)
    /* initialize the handle */
    canXgRxHandle[channel] = (CanReceiveHandle)kCanBufferFree;
    canRxSyncObj[channel] = 0; /* unlock the XGate Rx path*/
    #   if defined(C_ENABLE_RANGE_0) || defined(C_ENABLE_RANGE_1) || defined(C_ENABLE_RANGE_2) || defined(C_ENABLE_RANGE_3)
    canXgHitRange[channel] = (vuint8)0x00;
    
    #    if defined(C_ENABLE_RANGE_0)
    #     if defined(C_SINGLE_RECEIVE_CHANNEL)
    #      if(C_RANGE0_IDTYPE == kCanIdTypeStd)
    canRangeConfig[channel].nMask[0] = (tCanRxId0)~MK_RX_RANGE_MASK_IDSTD0(CANRANGE0ACCMASK(channel));
    canRangeConfig[channel].nCode[0] = (tCanRxId0) MK_RX_RANGE_CODE_IDSTD0(CANRANGE0ACCCODE(channel));
    #      else
    canRangeConfig[channel].nMask[0] = (tCanRxId0)~MK_RX_RANGE_MASK_IDEXT0(CANRANGE0ACCMASK(channel));
    canRangeConfig[channel].nCode[0] = (tCanRxId0) MK_RX_RANGE_CODE_IDEXT0(CANRANGE0ACCCODE(channel));
    #      endif
    #     else
    canRangeConfig[channel].nMask[0] = (tCanRxId0)~(CANRANGE0ACCMASK(channel).Id0);
    canRangeConfig[channel].nCode[0] = (tCanRxId0) (CANRANGE0ACCCODE(channel).Id0);
    #     endif
    #    else
    canRangeConfig[channel].nMask[0] = (tCanRxId0)0x00000000;
    canRangeConfig[channel].nCode[0] = (tCanRxId0)0xFFFFFFFF;
    #    endif
    
    #    if defined(C_ENABLE_RANGE_1)
    #     if defined(C_SINGLE_RECEIVE_CHANNEL)
    #      if(C_RANGE1_IDTYPE == kCanIdTypeStd)
    canRangeConfig[channel].nMask[1] = (tCanRxId0)~MK_RX_RANGE_MASK_IDSTD0(CANRANGE1ACCMASK(channel));
    canRangeConfig[channel].nCode[1] = (tCanRxId0) MK_RX_RANGE_CODE_IDSTD0(CANRANGE1ACCCODE(channel));
    #      else
    canRangeConfig[channel].nMask[1] = (tCanRxId0)~MK_RX_RANGE_MASK_IDEXT0(CANRANGE1ACCMASK(channel));
    canRangeConfig[channel].nCode[1] = (tCanRxId0) MK_RX_RANGE_CODE_IDEXT0(CANRANGE1ACCCODE(channel));
    #      endif
    #     else
    canRangeConfig[channel].nMask[1] = (tCanRxId0)~(CANRANGE1ACCMASK(channel).Id0);
    canRangeConfig[channel].nCode[1] = (tCanRxId0) (CANRANGE1ACCCODE(channel).Id0);
    #     endif
    #    else
    canRangeConfig[channel].nMask[1] = (tCanRxId0)0x00000000;
    canRangeConfig[channel].nCode[1] = (tCanRxId0)0xFFFFFFFF;
    #    endif
    
    #    if defined(C_ENABLE_RANGE_2)
    #     if defined(C_SINGLE_RECEIVE_CHANNEL)
    #      if(C_RANGE2_IDTYPE == kCanIdTypeStd)
    canRangeConfig[channel].nMask[2] = (tCanRxId0)~MK_RX_RANGE_MASK_IDSTD0(CANRANGE2ACCMASK(channel));
    canRangeConfig[channel].nCode[2] = (tCanRxId0) MK_RX_RANGE_CODE_IDSTD0(CANRANGE2ACCCODE(channel));
    #      else
    canRangeConfig[channel].nMask[2] = (tCanRxId0)~MK_RX_RANGE_MASK_IDEXT0(CANRANGE2ACCMASK(channel));
    canRangeConfig[channel].nCode[2] = (tCanRxId0) MK_RX_RANGE_CODE_IDEXT0(CANRANGE2ACCCODE(channel));
    #      endif
    #     else
    canRangeConfig[channel].nMask[2] = (tCanRxId0)~(CANRANGE2ACCMASK(channel).Id0);
    canRangeConfig[channel].nCode[2] = (tCanRxId0) (CANRANGE2ACCCODE(channel).Id0);
    #     endif
    #    else
    canRangeConfig[channel].nMask[2] = (tCanRxId0)0x00000000;
    canRangeConfig[channel].nCode[2] = (tCanRxId0)0xFFFFFFFF;
    #    endif
    
    #    if defined(C_ENABLE_RANGE_3)
    #     if defined(C_SINGLE_RECEIVE_CHANNEL)
    #      if(C_RANGE3_IDTYPE == kCanIdTypeStd)
    canRangeConfig[channel].nMask[3] = (tCanRxId0)~MK_RX_RANGE_MASK_IDSTD0(CANRANGE3ACCMASK(channel));
    canRangeConfig[channel].nCode[3] = (tCanRxId0) MK_RX_RANGE_CODE_IDSTD0(CANRANGE3ACCCODE(channel));
    #      else
    canRangeConfig[channel].nMask[3] = (tCanRxId0)~MK_RX_RANGE_MASK_IDEXT0(CANRANGE3ACCMASK(channel));
    canRangeConfig[channel].nCode[3] = (tCanRxId0) MK_RX_RANGE_CODE_IDEXT0(CANRANGE3ACCCODE(channel));
    #      endif
    #     else
    canRangeConfig[channel].nMask[3] = (tCanRxId0)~(CANRANGE3ACCMASK(channel).Id0);
    canRangeConfig[channel].nCode[3] = (tCanRxId0) (CANRANGE3ACCCODE(channel).Id0);
    #     endif
    #    else
    canRangeConfig[channel].nMask[3] = (tCanRxId0)0x00000000;
    canRangeConfig[channel].nCode[3] = (tCanRxId0)0xFFFFFFFF;
    #    endif
    
    #    if !defined(C_SINGLE_RECEIVE_CHANNEL)
    canRangeConfig[channel].nActiveMask = CanChannelObject[channel].RangeActiveFlag;
    #    endif
    
    #   endif
    #  endif
    # endif
    
    /* leave init mode before to start the RAM check */
    CTL0 &= (vuint8)~INITRQ;
    CTL0 = (vuint8)(CanInitObject[initObject].CanInitCTL0 & ((vuint8)~INITRQ));
    APPLCANTIMERSTART(kCanLoopExitInit);
    while(((CTL1 & INITAK) == INITAK) && APPLCANTIMERLOOP(kCanLoopExitInit))
    {
      ; /* wait until init mode is left */
    }
    APPLCANTIMEREND(kCanLoopExitInit);
    
    
    /* Init Tx-Objects -------------------------------------------------------- */
    /* init saved Tx handles: */                   
    /* in case of CommonCAN, transmission is always on the frist HW channel of a CommonCAN channel */
    {
#if defined( C_MULTIPLE_RECEIVE_CHANNEL )
      assertGen( (vsintx)CAN_HL_HW_TX_STOPINDEX(canHwChannel) + CAN_HL_TX_OFFSET_HW_TO_LOG(canHwChannel) 
                 == CAN_HL_LOG_HW_TX_STOPINDEX(canHwChannel), channel, kErrorHwToLogTxObjCalculation);          /* PRQA S 3109 */
      assertGen( (vsintx)CAN_HL_HW_TX_STARTINDEX(canHwChannel) + CAN_HL_TX_OFFSET_HW_TO_LOG(canHwChannel) 
                 == CAN_HL_LOG_HW_TX_STARTINDEX(canHwChannel), channel, kErrorHwToLogTxObjCalculation);         /* PRQA S 3109 */
      assertGen( CAN_HL_LOG_HW_TX_STARTINDEX(canHwChannel) <= CAN_HL_LOG_HW_TX_STOPINDEX(canHwChannel), channel, kErrorHwToLogTxObjCalculation);  /* PRQA S 3109 */
#endif
  
      for (hwObjHandle=CAN_HL_HW_TX_STARTINDEX(canHwChannel); hwObjHandle<CAN_HL_HW_TX_STOPINDEX(canHwChannel); hwObjHandle++ )     /*lint !e661*/
      {
        logTxObjHandle = (CanObjectHandle)((vsintx)hwObjHandle + CAN_HL_TX_OFFSET_HW_TO_LOG(canHwChannel));     /*lint !e661*/

#if defined( C_ENABLE_CAN_CANCEL_NOTIFICATION ) || \
    defined( C_ENABLE_CAN_MSG_TRANSMIT_CANCEL_NOTIFICATION )
        if((canStatus[channel] & kCanHwIsInit) == kCanHwIsInit)                    /*lint !e661*/
        {
          /* inform application, if a pending transmission is canceled */
          txHandle = canHandleCurTxObj[logTxObjHandle];

# if defined( C_ENABLE_CAN_CANCEL_NOTIFICATION )
          if( txHandle < kCanNumberOfTxObjects )
          {
            APPLCANCANCELNOTIFICATION(channel, txHandle);
          }
# endif
# if defined( C_ENABLE_CAN_MSG_TRANSMIT_CANCEL_NOTIFICATION )
          if( txHandle == kCanBufferMsgTransmit)
          { 
            APPLCANMSGCANCELNOTIFICATION(channel);
          } 
# endif
        }
#endif

        canHandleCurTxObj[logTxObjHandle] = kCanBufferFree;                   /* MsgObj is free */

#if defined( C_ENABLE_CAN_RAM_CHECK )
        if(kCanTrue == CanLL_IsMailboxCorrupt(CAN_HW_CHANNEL_CANPARA_FIRST hwObjHandle))
        {
# if defined( C_ENABLE_NOTIFY_CORRUPT_MAILBOX )
          ApplCanCorruptMailbox(CAN_CHANNEL_CANPARA_FIRST hwObjHandle);
# endif
          canRamCheckStatus = kCanRamCheckFailed;
        }
#endif

      }

    }

    /* init unused msg objects ------------------------------------------------ */
    for (hwObjHandle=CAN_HL_HW_UNUSED_STARTINDEX(canHwChannel); hwObjHandle<CAN_HL_HW_UNUSED_STOPINDEX(canHwChannel); hwObjHandle++ )  /*lint !e661 !e681*/
    {
    }



#if defined( C_ENABLE_RX_BASICCAN_OBJECTS )
    /* init basic can receive msg object: ------------------------------------- */
    for (hwObjHandle=CAN_HL_HW_RX_BASIC_STARTINDEX(canHwChannel); hwObjHandle<CAN_HL_HW_RX_BASIC_STOPINDEX(canHwChannel); hwObjHandle++ )
    {
#if defined( C_ENABLE_CAN_RAM_CHECK )
      if(kCanTrue == CanLL_IsMailboxCorrupt(CAN_HW_CHANNEL_CANPARA_FIRST hwObjHandle))
      {
# if defined( C_ENABLE_NOTIFY_CORRUPT_MAILBOX )
        ApplCanCorruptMailbox(CAN_CHANNEL_CANPARA_FIRST hwObjHandle);
# endif
        canRamCheckStatus = kCanRamCheckFailed;
      }
#endif

      /* the LL driver has to know which ID tpyes have to be received by which object */
    }
#endif
                     
    # if defined(C_ENABLE_TX_POLLING)
    canllTxStatusFlag[channel] = 0;
    # endif
    
    /* initial state of the Rx, wakeup and error interrupts after reset */
    # if defined( C_ENABLE_RX_BASICCAN_OBJECTS )
    tmpCRIER = (vuint8)0x00; /* disable all possible interrupt sources - then enable the used one */
    
    #  if defined(C_ENABLE_ERROR_POLLING)
    tmpCRIER |= (vuint8)TSTAT0E;           /* in case of polling the status change interrupt is disabled */
    #  else
    tmpCRIER |= (vuint8)(CSCIE | TSTAT0E); /* at least we want bus-off notification */
    #   if defined(C_ENABLE_OVERRUN)
    tmpCRIER |= OVRIE; /* enable overrun interrupt */
    #   endif
    # endif
    
    #  if defined (C_ENABLE_INDIVIDUAL_POLLING) || (!defined (C_ENABLE_INDIVIDUAL_POLLING) && !defined(C_ENABLE_RX_BASICCAN_POLLING))
    #   if defined ( C_ENABLE_INDIVIDUAL_POLLING )
    if (CanHwObjIndivPolling[CAN_HWOBJINDIVPOLLING_INDEX0][CAN_HL_HW_RX_BASIC_STARTINDEX(canHwChannel)] == (vuint8)0x00)
    #   endif
    {
      tmpCRIER |= RXE; /* enable Rx interrupt */
    }
    #  endif
    
    #  if defined(C_ENABLE_SLEEP_WAKEUP)
    #   if defined(C_ENABLE_WAKEUP_POLLING)
    #   else
    tmpCRIER |= WUPIE; /* enable wakeup interrupt */
    #   endif
    #  endif
    
    if(canCanIrqDisabled[channel] == kCanTrue) /* disable CAN interrupts? */
    {
      canCanInterruptOldStatus[channel].oldCanCRIER = tmpCRIER;
    }
    else
    {
      CRIER = tmpCRIER;
    }
    # endif
  } /* end of loop over all hw channels */

#if defined( C_ENABLE_TX_OBSERVE )
  ApplCanInit( CAN_CHANNEL_CANPARA_FIRST CAN_HL_LOG_HW_TX_STARTINDEX(canHwChannel), CAN_HL_LOG_HW_TX_STOPINDEX(canHwChannel) );
#endif
#if defined( C_ENABLE_MSG_TRANSMIT_CONF_FCT )
  APPL_CAN_MSGTRANSMITINIT( CAN_CHANNEL_CANPARA_ONLY );
#endif

#if defined( C_ENABLE_CAN_RAM_CHECK )
  if( canRamCheckStatus == kCanRamCheckFailed)
  {
    /* let the application decide if communication is disabled */
    if (ApplCanMemCheckFailed(CAN_CHANNEL_CANPARA_ONLY) == kCanDisableCommunication)
    {
      canComStatus[channel] = kCanDisableCommunication;
    }  
  }
#endif


} /* END OF CanInit */
/* CODE CATEGORY 4 END*/


/****************************************************************************
| NAME:             CanInitPowerOn
| CALLED BY:        Application
| PRECONDITIONS:    This function must be called by the application before
|                   any other CAN driver function 
|                   Interrupts must be disabled
| INPUT PARAMETERS: none
| RETURN VALUES:    none
| DESCRIPTION:      Initialization of the CAN chip
****************************************************************************/
/* CODE CATEGORY 4 START*/
C_API_1 void C_API_2 CanInitPowerOn( void )
{ 

#if defined( C_ENABLE_VARIABLE_DLC )        || \
      defined( C_ENABLE_DYN_TX_OBJECTS )      || \
      defined( C_ENABLE_INDICATION_FLAG )     || \
      defined( C_ENABLE_CONFIRMATION_FLAG )
  CanTransmitHandle txHandle;
#endif
#if defined( C_ENABLE_VARIABLE_RX_DATALEN )
  CanReceiveHandle rxHandle;
#endif
  CAN_CHANNEL_CANTYPE_LOCAL


  VStdInitPowerOn();

#if defined( C_ENABLE_VARIABLE_DLC )
  for (txHandle = 0; txHandle < kCanNumberOfTxObjects; txHandle++)
  {
    assertGen(XT_TX_DLC(CanGetTxDlc(txHandle))<(vuint8)9, kCanAllChannels, kErrorTxROMDLCTooLarge);      /* PRQA S 3109 */
    canTxDLC_RAM[txHandle] = CanGetTxDlc(txHandle);
  }
#endif

#if defined( C_ENABLE_DYN_TX_OBJECTS )
  /*  Reset dynamic transmission object management -------------------------- */
  for (txHandle = 0; txHandle < kCanNumberOfTxDynObjects; txHandle++)
  {
    /*  Reset management information  */
    canTxDynObjReservedFlag[txHandle] = 0;
  }
#endif /* C_ENABLE_DYN_TX_OBJECTS */

#if defined( C_ENABLE_VARIABLE_RX_DATALEN )
  /*  Initialize the array with received dlc -------------------------- */
  for (rxHandle = 0; rxHandle < kCanNumberOfRxObjects; rxHandle++) {
    canVariableRxDataLen[rxHandle] = CanGetRxDataLen(rxHandle);
  }
#endif

#if defined( C_ENABLE_INDICATION_FLAG )
/* txHandle as loop variable is ok here, because the number of indication bytes is not as high as the number of Rx Messages */
  for (txHandle = 0; txHandle < kCanNumberOfIndBytes; txHandle++) {
    CanIndicationFlags._c[txHandle] = 0;
  }
#endif

#if defined( C_ENABLE_CONFIRMATION_FLAG )
  for (txHandle = 0; txHandle < kCanNumberOfConfBytes; txHandle++) {
    CanConfirmationFlags._c[txHandle] = 0;
  }
#endif

#if defined( C_ENABLE_RX_QUEUE )
# if defined( C_ENABLE_RX_QUEUE_EXTERNAL )
/* in this case, the rx queue pointer no initialized yet. This will be done ofter CanInitPowerOn() via ... by the application */
# else
  CanDeleteRxQueue();
# endif
#endif

  #if defined(C_COMP_COSMIC_MCS12X_MSCAN12)  
  {
    CanChannelHandle ch;
    for(ch = 0; ch < kCanNumberOfChannels; ch++)
    {
      canCanIrqDisabled[ch] = kCanFalse;
  #  if defined(C_ENABLE_RX_QUEUE)
      canRDSRxPtr[ch] = (vuint8*)(canRxInfoStruct[ch].pChipData);
  #  else
  #   if defined(C_SEARCH_XGATE)
      canRDSRxPtr[ch] = (vuint8*)&(canRxTmpBuf[ch].DataFld[0]);
      /* initialize the XGate interrupt counter */
      canCanXGateIrqtCount = 0;
  #   else
      canRDSRxPtr[ch] = (vuint8*)&(CAN_CNTRL_BASIS_ADR(ch) -> CanRxBuf.DataFld[0]);
  #   endif
  #  endif
  # if defined ( C_ENABLE_CAN_TX_CONF_FCT )
      txInfoStructConf[ch].pChipMsgObj = (CanChipDataPtr)(CAN_TX_MAILBOX_BASIS_ADR(ch));
      txInfoStructConf[ch].pChipData   = (CanChipMsgPtr)(CAN_TX_MAILBOX_BASIS_ADR(ch) -> DataFld);
  # endif
    }
  }
  #endif
  
  

#if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  for (channel=0; channel<kCanNumberOfChannels; channel++)
#endif
  {

#if defined( C_ENABLE_CAN_RAM_CHECK )
    canComStatus[channel] = kCanEnableCommunication;
#endif
   
    canStatus[channel]              = kCanStatusInit;


#if defined( C_ENABLE_CAN_TX_CONF_FCT )
    txInfoStructConf[channel].Channel = channel;
#endif
#if defined( C_ENABLE_RX_BASICCAN_OBJECTS )
    canRxInfoStruct[channel].Channel  = channel;
#endif
    canCanInterruptCounter[channel] = 0; 

#if defined( C_ENABLE_TX_POLLING )          || \
      defined( C_ENABLE_RX_BASICCAN_POLLING ) || \
      defined( C_ENABLE_ERROR_POLLING )       || \
      defined( C_ENABLE_WAKEUP_POLLING )      
    canPollingTaskActive[channel] = 0;
#endif

#if defined( C_ENABLE_DYN_TX_OBJECTS )   && \
    defined( C_ENABLE_CONFIRMATION_FCT ) && \
    defined( C_ENABLE_TRANSMIT_QUEUE )
  /*  Reset dynamic transmission object management -------------------------- */
    confirmHandle[channel] = kCanBufferFree;
#endif

#if defined( C_ENABLE_TX_MASK_EXT_ID )
    canTxMask0[channel] = 0;
# if (kCanNumberOfUsedCanTxIdTables > 1)
    canTxMask1[channel] = 0;
# endif
# if (kCanNumberOfUsedCanTxIdTables > 2)
    canTxMask2[channel] = 0;
# endif
# if (kCanNumberOfUsedCanTxIdTables > 3)
    canTxMask3[channel] = 0;
# endif
# if (kCanNumberOfUsedCanTxIdTables > 4)
    canTxMask4[channel] = 0;
# endif
#endif

#if defined( C_ENABLE_ECU_SWITCH_PASS )
    canPassive[channel]             = 0;
#endif

#if defined( C_ENABLE_PART_OFFLINE )
    canTxPartOffline[channel]       = kCanTxPartInit;
#endif
#if defined( C_ENABLE_COND_RECEIVE_FCT )
    canMsgCondRecState[channel]     = kCanTrue;
#endif

    canRxHandle[channel] = kCanRxHandleNotUsed;

    {
#if defined( C_ENABLE_TRANSMIT_QUEUE )
  /* clear all Tx queue flags */
      CanDelQueuedObj( CAN_CHANNEL_CANPARA_ONLY );
#endif

      CanInit( CAN_CHANNEL_CANPARA_FIRST 0 );

      /* canStatus is only set to init and online, if CanInit() is called for this channel. */
      canStatus[channel]              |= (kCanHwIsInit | kCanTxOn);
    }

  }

} /* END OF CanInitPowerOn */
/* CODE CATEGORY 4 END*/

#if defined( C_ENABLE_TRANSMIT_QUEUE )
/************************************************************************
* NAME:               CanDelQueuedObj
* CALLED BY:          
* PRECONDITIONS:      
* PARAMETER:          notify: if set to 1 for every deleted obj the appl is notified
* RETURN VALUE:       -
* DESCRIPTION:        Resets the bits with are set to 0 in mask
*                     Clearing the Transmit-queue
*************************************************************************/
/* CODE CATEGORY 4 START*/
static void CanDelQueuedObj( CAN_CHANNEL_CANTYPE_ONLY )
{ 
  CanSignedTxHandle     queueElementIdx;
  #if defined( C_ENABLE_CAN_CANCEL_NOTIFICATION )
  CanSignedTxHandle     elementBitIdx;
  CanTransmitHandle     txHandle;
  tCanQueueElementType  elem;
  #endif

# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertInternal(channel < kCanNumberOfChannels, kCanAllChannels, kErrorInternalChannelHdlTooLarge);    /* PRQA S 3109 */
# endif

  #  if defined( C_ENABLE_CAN_CANCEL_NOTIFICATION )
  if((canStatus[channel] & kCanHwIsInit) == kCanHwIsInit)
  {
    CAN_CAN_INTERRUPT_DISABLE(CAN_CHANNEL_CANPARA_ONLY);        /* avoid interruption by CanHL_TxConfirmation */
    for(queueElementIdx = CAN_HL_TXQUEUE_STARTINDEX(channel); queueElementIdx < CAN_HL_TXQUEUE_STOPINDEX(channel); queueElementIdx++)
    { 
      elem = canTxQueueFlags[queueElementIdx];
      if(elem != (tCanQueueElementType)0) /* is there any flag set in the queue element? */
      {
        /* iterate through all flags and notify application for every scheduled transmission */
        for(elementBitIdx = ((CanSignedTxHandle)1 << kCanTxQueueShift) - (CanSignedTxHandle)1; elementBitIdx >= (CanSignedTxHandle)0; elementBitIdx--)
        { 
          if( ( elem & CanShiftLookUp[elementBitIdx] ) != (tCanQueueElementType)0)
          { 
            txHandle = (((CanTransmitHandle)queueElementIdx << kCanTxQueueShift) + (CanTransmitHandle)elementBitIdx) - CAN_HL_TXQUEUE_PADBITS(channel);
            APPLCANCANCELNOTIFICATION(channel, txHandle);
          } 
        } 
        canTxQueueFlags[queueElementIdx] = (tCanQueueElementType)0;
      }
    } 
    CAN_CAN_INTERRUPT_RESTORE(CAN_CHANNEL_CANPARA_ONLY);
  } 
  else
  #  endif
  {
    for(queueElementIdx = CAN_HL_TXQUEUE_STARTINDEX(channel); queueElementIdx < CAN_HL_TXQUEUE_STOPINDEX(channel); queueElementIdx++)
    { 
      canTxQueueFlags[queueElementIdx] = (tCanQueueElementType)0;
    } 
  }

# if defined( C_HL_ENABLE_COPROCESSOR_SUPPORT )
  #if defined(C_COMP_COSMIC_MCS12X_MSCAN12)
  # if defined(VGEN_GENY)
  #  if defined(C_ENABLE_XGATE_USED)
  canXgTxQueueCnt[channel] = 0;
  for (i = CAN_HL_TX_STARTINDEX(channel); i < CAN_HL_TX_STOPINDEX(channel); i++)
  {
    canXgTxQueueFlags[i] = 0;
  }
  #  endif
  # endif
  #endif
# endif
}
/* CODE CATEGORY 4 END*/
#endif

#if defined( C_ENABLE_CAN_TRANSMIT )
# if defined( C_ENABLE_CAN_CANCEL_TRANSMIT )
/* CODE CATEGORY 3 START*/
/****************************************************************************
| NAME:             CanCancelTransmit
| CALLED BY:        Application
| PRECONDITIONS:    none
| INPUT PARAMETERS: Tx-Msg-Handle
| RETURN VALUES:    none
| DESCRIPTION:      delete on Msg-Object
****************************************************************************/
C_API_1 void C_API_2 CanCancelTransmit( CanTransmitHandle txHandle )
{
  CanDeclareGlobalInterruptOldStatus
  CAN_CHANNEL_CANTYPE_LOCAL 
  CanObjectHandle        logTxObjHandle;
  /* ##RI1.4 - 1.6: CanCancelTransmit and CanCancelMsgTransmit */
#  if defined( C_ENABLE_CANCEL_IN_HW )
  CanObjectHandle        txObjHandle;
#  endif

# if defined( C_ENABLE_TRANSMIT_QUEUE )
  CanSignedTxHandle queueElementIdx; /* index for accessing the tx queue */
  CanSignedTxHandle elementBitIdx;  /* bit index within the tx queue element */
  CanTransmitHandle queueBitPos;  /* physical bitposition of the handle */
# endif

  if (txHandle < kCanNumberOfTxObjects)         /* legal txHandle ? */
  {
# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
    channel = CanGetChannelOfTxObj(txHandle);
# endif

# if defined( C_ENABLE_MULTI_ECU_PHYS )
    assertUser(((CanTxIdentityAssignment[txHandle] & V_ACTIVE_IDENTITY_MSK) != (tVIdentityMsk)0 ), channel , kErrorDisabledTxMessage);    /* PRQA S 3109 */
# endif

    CanNestedGlobalInterruptDisable();
# if defined( C_ENABLE_TRANSMIT_QUEUE )
    #if defined( C_ENABLE_INTERNAL_CHECK ) &&\
        defined( C_MULTIPLE_RECEIVE_CHANNEL )
    if (sizeof(queueBitPos) == 1)
    {
      assertInternal( (((vuint16)kCanNumberOfTxObjects + (vuint16)CanTxQueuePadBits[kCanNumberOfChannels-1]) <= 256u), kCanAllChannels, kErrorTxQueueTooManyHandle) /* PRQA S 3109 */ /*lint !e572 !e506*/
    }
    else
    {
      assertInternal( (((vuint32)kCanNumberOfTxObjects + (vuint32)CanTxQueuePadBits[kCanNumberOfChannels-1]) <= 65536u), kCanAllChannels, kErrorTxQueueTooManyHandle) /* PRQA S 3109 */ /*lint !e572 !e506*/
    }
    #endif
    queueBitPos  = txHandle + CAN_HL_TXQUEUE_PADBITS(channel);
    queueElementIdx = (CanSignedTxHandle)(queueBitPos >> kCanTxQueueShift); /* get the queue element where to set the flag */
    elementBitIdx  = (CanSignedTxHandle)(queueBitPos & kCanTxQueueMask);   /* get the flag index wihtin the queue element */
    if( (canTxQueueFlags[queueElementIdx] & CanShiftLookUp[elementBitIdx]) != (tCanQueueElementType)0 )
    {
      canTxQueueFlags[queueElementIdx] &= ~CanShiftLookUp[elementBitIdx]; /* clear flag from the queue */
      APPLCANCANCELNOTIFICATION(channel, txHandle);
    }
# endif

    logTxObjHandle = (CanObjectHandle)((vsintx)CAN_HL_HW_TX_NORMAL_INDEX(canHwChannel) + CAN_HL_TX_OFFSET_HW_TO_LOG(canHwChannel));
    if (canHandleCurTxObj[logTxObjHandle] == txHandle)
    {
      canHandleCurTxObj[logTxObjHandle] = kCanBufferCancel;

      /* ##RI1.4 - 1.6: CanCancelTransmit and CanCancelMsgTransmit */
# if defined( C_ENABLE_CANCEL_IN_HW )
      txObjHandle = CAN_HL_HW_TX_NORMAL_INDEX(canHwChannel);
      #if defined(C_COMP_COSMIC_MCS12X_MSCAN12)  
      CTARQ |= CanMailboxSelect[txObjHandle]; /* request cancellation of the Tx message */
      #endif
      
      /* on successful cancellation the msCAN behaves as if a successful Tx occured */
# endif /* C_ENABLE_CANCEL_IN_HW */
      APPLCANCANCELNOTIFICATION(channel, txHandle);
    }

    CanNestedGlobalInterruptRestore();
  } /* if (txHandle < kCanNumberOfTxObjects) */
}
/* CODE CATEGORY 3 END*/
# endif /* defined( C_ENABLE_CAN_CANCEL_TRANSMIT ) */

#endif /* if defined( C_ENABLE_CAN_TRANSMIT ) */


#if defined( C_ENABLE_MSG_TRANSMIT_CONF_FCT )
/****************************************************************************
| NAME:             CanCancelMsgTransmit
| CALLED BY:        Application
| PRECONDITIONS:    none
| INPUT PARAMETERS: none or channel
| RETURN VALUES:    none
| DESCRIPTION:      delete on Msg-Object
****************************************************************************/
/* CODE CATEGORY 3 START*/
C_API_1 void C_API_2 CanCancelMsgTransmit( CAN_CHANNEL_CANTYPE_ONLY )
{
  CanDeclareGlobalInterruptOldStatus
  CanObjectHandle  logTxObjHandle;
# if defined( C_ENABLE_CANCEL_IN_HW )
  CanObjectHandle  txObjHandle;    /* ##RI1.4 - 1.6: CanCancelTransmit and CanCancelMsgTransmit */
# endif

# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser(channel < kCanNumberOfChannels, kCanAllChannels, kErrorChannelHdlTooLarge);    /* PRQA S 3109 */
# endif


  logTxObjHandle = (CanObjectHandle)((vsintx)CAN_HL_HW_MSG_TRANSMIT_INDEX(canHwChannel) + CAN_HL_TX_OFFSET_HW_TO_LOG(canHwChannel));

  CanNestedGlobalInterruptDisable();
  if (canHandleCurTxObj[logTxObjHandle] == kCanBufferMsgTransmit)
  {
    canHandleCurTxObj[logTxObjHandle] = kCanBufferCancel;

    /* ##RI1.4 - 1.6: CanCancelTransmit and CanCancelMsgTransmit */
# if defined( C_ENABLE_CANCEL_IN_HW )
    txObjHandle = CAN_HL_HW_MSG_TRANSMIT_INDEX(canHwChannel);
    #if defined(C_COMP_COSMIC_MCS12X_MSCAN12)  
    CTARQ |= CanMailboxSelect[txObjHandle]; /* request cancellation of the Tx message */
    #endif
    
    /* on successful cancellation the msCAN behaves as if a successful Tx occured */
# endif
    APPLCANMSGCANCELNOTIFICATION(channel);
  }
  CanNestedGlobalInterruptRestore();
}
/* CODE CATEGORY 3 END*/
#endif


#if defined( C_ENABLE_CAN_TRANSMIT )
# if defined( C_ENABLE_VARIABLE_DLC )
/* CODE CATEGORY 2 START*/
/****************************************************************************
| NAME:             CanTransmitVarDLC
| CALLED BY:        Netmanagement, application
| PRECONDITIONS:    Can driver must be initialized
| INPUT PARAMETERS: Handle to Tx message, DLC of Tx message
| RETURN VALUES:    kCanTxFailed: transmit failed
|                   kCanTxOk    : transmit was succesful
| DESCRIPTION:      If the CAN driver is not ready for send, the application
|                   decide, whether the transmit request is repeated or not.
****************************************************************************/
C_API_1 vuint8 C_API_2 CanTransmitVarDLC(CanTransmitHandle txHandle, vuint8 dlc)
{
  assertUser(dlc<(vuint8)9, kCanAllChannels, kErrorTxDlcTooLarge);         /* PRQA S 3109 */
  assertUser(txHandle<kCanNumberOfTxObjects, kCanAllChannels , kErrorTxHdlTooLarge);     /* PRQA S 3109 */

  canTxDLC_RAM[ txHandle ] = (canTxDLC_RAM[ txHandle ] & CanLL_DlcMask) | MK_TX_DLC(dlc);

  return CanTransmit( txHandle );
} /* END OF CanTransmitVarDLC */
/* CODE CATEGORY 2 END*/
# endif   /* C_ENABLE_VARIABLE_DLC */

/****************************************************************************
| NAME:             CanTransmit
| CALLED BY:        application
| PRECONDITIONS:    Can driver must be initialized
| INPUT PARAMETERS: Handle of the transmit object to be send
| RETURN VALUES:    kCanTxFailed: transmit failed
|                   kCanTxOk    : transmit was succesful
| DESCRIPTION:      If the CAN driver is not ready for send, the application
|                   decide, whether the transmit request is repeated or not.
****************************************************************************/
/* CODE CATEGORY 2 START*/

C_API_1 vuint8 C_API_2 CanTransmit(CanTransmitHandle txHandle)  C_API_3   /*lint !e14 !e31*/
{
  CanDeclareGlobalInterruptOldStatus

# if !defined( C_ENABLE_TX_POLLING )          ||\
     !defined( C_ENABLE_TRANSMIT_QUEUE )      ||\
     defined( C_ENABLE_INDIVIDUAL_POLLING )
  CanObjectHandle      txObjHandle;
  CanObjectHandle      logTxObjHandle;
  vuint8             rc;
# endif   /* ! C_ENABLE_TX_POLLING  || ! C_ENABLE_TRANSMIT_QUEUE || C_ENABLE_TX_FULLCAN_OBJECTS || C_ENABLE_INDIVIDUAL_POLLING */
  CAN_CHANNEL_CANTYPE_LOCAL

# if defined( C_ENABLE_TRANSMIT_QUEUE )
  CanSignedTxHandle queueElementIdx; /* index for accessing the tx queue */
  CanSignedTxHandle elementBitIdx;  /* bit index within the tx queue element */
  CanTransmitHandle queueBitPos;  /* physical bitposition of the handle */
# endif
  #if defined(C_COMP_COSMIC_MCS12X_MSCAN12)  
  # if defined(C_ENABLE_ECU_SWITCH_PASS)
  #  if !defined(C_ENABLE_TX_POLLING) ||       \
        !defined( C_ENABLE_TRANSMIT_QUEUE )
  #  else
    CanObjectHandle      txObjHandle;
  #  endif   /* ! C_ENABLE_TX_POLLING  || ! C_ENABLE_TRANSMIT_QUEUE */
  #  if defined ( C_MULTIPLE_RECEIVE_CHANNEL)
    /* unfortunately needed because HL part initializes the channel too late */
    channel = CanGetChannelOfTxObj(txHandle);          /*lint !e661*/
  #  endif
  if(canPassive[channel] != 0)
  {
    /* initialize the tx object handle with 1 */
    txObjHandle = 1;
  }
  # endif
  #endif



  assertUser(txHandle<kCanNumberOfTxObjects, kCanAllChannels , kErrorTxHdlTooLarge);      /* PRQA S 3109 */

# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  channel = CanGetChannelOfTxObj(txHandle);          /*lint !e661*/
# endif

# if defined( C_ENABLE_MULTI_ECU_PHYS )
  assertUser(((CanTxIdentityAssignment[txHandle] & V_ACTIVE_IDENTITY_MSK) != (tVIdentityMsk)0 ), channel , kErrorDisabledTxMessage);    /* PRQA S 3109 */
# endif

  /* test offline ---------------------------------------------------------- */
  if ( (canStatus[channel] & kCanTxOn) != kCanTxOn )           /* transmit path switched off */
  {
    return kCanTxFailed;
  }

# if defined( C_ENABLE_PART_OFFLINE )
  if ( (canTxPartOffline[channel] & CanTxSendMask[txHandle]) != (vuint8)0)   /*lint !e661*/ /* CAN off ? */
  {
    return (kCanTxPartOffline);
  }
# endif

# if defined( C_ENABLE_CAN_RAM_CHECK )
  if(canComStatus[channel] == kCanDisableCommunication)
  {
    return(kCanCommunicationDisabled);
  }
# endif

# if defined( C_ENABLE_SLEEP_WAKEUP )
  assertUser(!CanLL_HwIsSleep(CAN_HW_CHANNEL_CANPARA_ONLY), channel, kErrorCanSleep);    /* PRQA S 3109 */
# endif
  assertUser(!CanLL_HwIsStop(CAN_HW_CHANNEL_CANPARA_ONLY), channel, kErrorCanStop);      /* PRQA S 3109 */

  /* passive mode ---------------------------------------------------------- */
# if defined( C_ENABLE_ECU_SWITCH_PASS )
  if ( canPassive[channel] != (vuint8)0)                             /*  set passive ? */
  {
#  if defined( C_ENABLE_CAN_TX_CONF_FCT ) || \
      defined( C_ENABLE_CONFIRMATION_FCT )
    CAN_CAN_INTERRUPT_DISABLE(CAN_CHANNEL_CANPARA_ONLY);      /* avoid CAN Rx interruption */
#  endif

#  if defined( C_ENABLE_CAN_TX_CONF_FCT )
/* ##RI-1.10 Common Callbackfunction in TxInterrupt */
    txInfoStructConf[channel].Handle  = txHandle;
    /* in case of passive mode we do not need to modifiy the mailbox */
    APPL_CAN_TX_CONFIRMATION(&txInfoStructConf[channel]);
#  endif

#  if defined( C_ENABLE_CONFIRMATION_FLAG )       /* set transmit ready flag  */
#   if C_SECURITY_LEVEL > 20
    CanNestedGlobalInterruptDisable();
#   endif
    CanConfirmationFlags._c[CanGetConfirmationOffset(txHandle)] |= CanGetConfirmationMask(txHandle);      /*lint !e661*/
#   if C_SECURITY_LEVEL > 20
    CanNestedGlobalInterruptRestore();
#   endif
#  endif

#  if defined( C_ENABLE_CONFIRMATION_FCT )
    {
#   if defined( C_HL_ENABLE_DUMMY_FCT_CALL )
#   else
      if ( CanGetApplConfirmationPtr(txHandle) != V_NULL )
#   endif
      {
         (CanGetApplConfirmationPtr(txHandle))(txHandle);   /* call completion routine  */
      }
    }
#  endif /* C_ENABLE_CONFIRMATION_FCT */

#  if defined( C_ENABLE_CAN_TX_CONF_FCT ) || \
      defined( C_ENABLE_CONFIRMATION_FCT )
    CAN_CAN_INTERRUPT_RESTORE(CAN_CHANNEL_CANPARA_ONLY);
#  endif

    return kCanTxOk;
  }
# endif


   /* can transmit enabled ================================================== */

   /* ----------------------------------------------------------------------- */
   /* ---  transmit queue with one objects ---------------------------------- */
   /* ---  transmit using fullcan objects ----------------------------------- */
   /* ----------------------------------------------------------------------- */

#  if !defined( C_ENABLE_TX_POLLING )          ||\
      !defined( C_ENABLE_TRANSMIT_QUEUE )      ||\
      defined( C_ENABLE_INDIVIDUAL_POLLING )

  txObjHandle = CAN_HL_HW_TX_NORMAL_INDEX(canHwChannel);                                          /* msg in object 0 */

  logTxObjHandle = (CanObjectHandle)((vsintx)txObjHandle + CAN_HL_TX_OFFSET_HW_TO_LOG(canHwChannel));
#  endif   /* ! C_ENABLE_TX_POLLING  || ! C_ENABLE_TRANSMIT_QUEUE || C_ENABLE_TX_FULLCAN_OBJECTS || C_ENABLE_INDIVIDUAL_POLLING */

  CanNestedGlobalInterruptDisable();
 
  /* test offline after interrupt disable ---------------------------------- */
  if ( (canStatus[channel] & kCanTxOn) != kCanTxOn )                /* transmit path switched off */
  {
    CanNestedGlobalInterruptRestore();
    return kCanTxFailed;
  }

# if defined( C_ENABLE_TRANSMIT_QUEUE )
#  if !defined( C_ENABLE_TX_POLLING )         ||\
      defined( C_ENABLE_INDIVIDUAL_POLLING )
  if (

#   if defined( C_ENABLE_TX_POLLING )
#    if defined( C_ENABLE_INDIVIDUAL_POLLING )
       ( ( CanHwObjIndivPolling[CAN_HWOBJINDIVPOLLING_INDEX0][txObjHandle] != (vuint8)0 )       /* Object is used in polling mode! */
                         || (canHandleCurTxObj[logTxObjHandle] != kCanBufferFree) )    /* MsgObj used?  */
#    else
        /* write always to queue; transmission is started out of TxTask */
#    endif
#   else
       ( canHandleCurTxObj[logTxObjHandle] != kCanBufferFree)    /* MsgObj used?  */
#   endif
     )
#  endif   /*  ( C_ENABLE_TX_FULLCAN_OBJECTS )  || !( C_ENABLE_TX_POLLING ) || ( C_ENABLE_INDIVIDUAL_POLLING ) */

    {
      /* tx object 0 used -> set msg in queue: -----------------------------*/
      queueBitPos  = txHandle + CAN_HL_TXQUEUE_PADBITS(channel);
      queueElementIdx = (CanSignedTxHandle)(queueBitPos >> kCanTxQueueShift); /* get the queue element where to set the flag */
      elementBitIdx  = (CanSignedTxHandle)(queueBitPos & kCanTxQueueMask);   /* get the flag index wihtin the queue element */
      canTxQueueFlags[queueElementIdx] |= CanShiftLookUp[elementBitIdx];
      CanNestedGlobalInterruptRestore();
      return kCanTxOk;                          
  }
# endif  /* C_ENABLE_TRANSMIT_QUEUE */

# if !defined( C_ENABLE_TX_POLLING )          ||\
     !defined( C_ENABLE_TRANSMIT_QUEUE )      ||\
     defined( C_ENABLE_INDIVIDUAL_POLLING )

#  if defined( C_ENABLE_TRANSMIT_QUEUE )    && \
      ( !defined( C_ENABLE_TX_POLLING )         ||\
        defined( C_ENABLE_INDIVIDUAL_POLLING )  )
  else
#  endif
  {
  /* check for transmit message object free ---------------------------------*/
    if (( canHandleCurTxObj[logTxObjHandle] != kCanBufferFree)    /* MsgObj used?  */
       || ( !CanLL_TxIsHWObjFree( canHwChannel, txObjHandle ) )

      /* hareware-txObject is not free --------------------------------------*/
       )  /* end of if question */

    {  /* object used */
      /* tx object n used, quit with error */
      CanNestedGlobalInterruptRestore();
      return kCanTxFailed;
    }
  }

  /* Obj, pMsgObject points to is free, transmit msg object: ----------------*/
  canHandleCurTxObj[logTxObjHandle] = txHandle;/* Save hdl of msgObj to be transmitted*/
  CanNestedGlobalInterruptRestore();

  rc = CanCopyDataAndStartTransmission( CAN_CHANNEL_CANPARA_FIRST txObjHandle, txHandle);

#  if defined( C_ENABLE_CAN_CANCEL_NOTIFICATION )
  if ( rc == kCanTxNotify)
  {
    rc = kCanTxFailed;      /* ignore notification if calls of CanCopy.. is performed within CanTransmit */
  }
#  endif


  return(rc);

# else   /* ! C_ENABLE_TX_POLLING  || ! C_ENABLE_TRANSMIT_QUEUE || C_ENABLE_TX_FULLCAN_OBJECTS || C_ENABLE_INDIVIDUAL_POLLING */
# endif   /* ! C_ENABLE_TX_POLLING  || ! C_ENABLE_TRANSMIT_QUEUE || C_ENABLE_TX_FULLCAN_OBJECTS || C_ENABLE_INDIVIDUAL_POLLING */

} /* END OF CanTransmit */
/* CODE CATEGORY 2 END*/


/****************************************************************************
| NAME:             CanCopyDataAndStartTransmission
| CALLED BY:        CanTransmit, CanHl_RestartTxQueue and CanHL_TxConfirmation
| PRECONDITIONS:    - Can driver must be initialized
|                   - canTxCurHandle[logTxObjHandle] must be set
|                   - the hardwareObject (txObjHandle) must be free
| INPUT PARAMETERS: txHandle: Handle of the transmit object to be send
|                   txObjHandle:  Nr of the HardwareObjects to use
| RETURN VALUES:    kCanTxFailed: transmit failed
|                   kCanTxOk    : transmit was succesful
| DESCRIPTION:      If the CAN driver is not ready for send, the application
|                   decide, whether the transmit request is repeated or not.
****************************************************************************/
/* PRQA S 2001 ++ */    /* suppress misra message about usage of goto */
/* CODE CATEGORY 1 START*/
static vuint8 CanCopyDataAndStartTransmission( CAN_CHANNEL_CANTYPE_FIRST CanObjectHandle txObjHandle, CanTransmitHandle txHandle) C_API_3   /*lint !e14 !e31*/
{
   CanDeclareGlobalInterruptOldStatus
   vuint8             rc;
   CanObjectHandle      logTxObjHandle;
#  if defined( C_ENABLE_COPY_TX_DATA )  
   TxDataPtr   CanMemCopySrcPtr;
#  endif
# if defined( C_ENABLE_DYN_TX_OBJECTS )
   CanTransmitHandle    dynTxObj;
# endif /* C_ENABLE_DYN_TX_OBJECTS */
# if defined( C_ENABLE_PRETRANSMIT_FCT )
   CanTxInfoStruct      txStruct;
# endif

   #if defined(C_ENABLE_COPY_TX_DATA)
   # if defined(C_COMP_COSMIC_MCS12X_MSCAN12)  
   vsintx i;
   vuint8 *pDestData;
   #  if defined (V_ENABLE_USED_GLOBAL_VAR)
   V_MEMRAM1_FAR vuint8 V_MEMRAM2_FAR V_MEMRAM3_FAR *pSrcData;
   #  else
   vuint8 *pSrcData;
   #  endif
   # endif
   #endif

# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
   assertInternal(channel < kCanNumberOfChannels, kCanAllChannels, kErrorInternalChannelHdlTooLarge);      /* PRQA S 3109 */
# endif
   assertInternal(txHandle < kCanNumberOfTxObjects, kCanAllChannels , kErrorInternalTxHdlTooLarge);        /* PRQA S 3109 */

# if defined( C_ENABLE_DYN_TX_OBJECTS )
   if (txHandle >= CAN_HL_TX_DYN_ROM_STARTINDEX(channel))
   {
     dynTxObj = (txHandle - CAN_HL_TX_DYN_ROM_STARTINDEX(channel)) + CAN_HL_TX_DYN_RAM_STARTINDEX(channel);    /* PRQA S 3382,0291 */
   }
   else
   {
     dynTxObj = (CanTransmitHandle)0;
   }
# endif /* C_ENABLE_DYN_TX_OBJECTS */

   assertInternal(txObjHandle < CAN_HL_HW_TX_STOPINDEX(canHwChannel), channel, kErrorTxObjHandleWrong);      /* PRQA S 3109 */
   logTxObjHandle = (CanObjectHandle)((vsintx)txObjHandle + CAN_HL_TX_OFFSET_HW_TO_LOG(canHwChannel));

   assertHardware( CanLL_TxIsHWObjFree( canHwChannel, txObjHandle ), channel, kErrorTxBufferBusy);          /* PRQA S 3109 */

   CanBeginCriticalXGateSection();
   #if defined(C_COMP_COSMIC_MCS12X_MSCAN12)  
   CTBSEL = CanMailboxSelect[txObjHandle]; /* select the appropriate Tx buffer */
   #endif
   
   CanEndCriticalXGateSection();

   /* set id and dlc  -------------------------------------------------------- */
   {
# if defined( C_ENABLE_DYN_TX_DLC ) || \
      defined( C_ENABLE_DYN_TX_ID )
     if (txHandle >= CAN_HL_TX_DYN_ROM_STARTINDEX(channel))
     {           /* set dynamic part of dynamic objects ----------------------*/
#  if defined( C_ENABLE_DYN_TX_ID )
        #if defined(C_COMP_COSMIC_MCS12X_MSCAN12)  
        CAN_TX_MAILBOX_BASIS_ADR(channel) -> Id   = canDynTxId0[dynTxObj];
        CAN_TX_MAILBOX_BASIS_ADR(channel) -> Prio = TX1; /* Prio Std Tx > Prio LowLevel Tx */
        #endif
        
        
#  endif
 
#  if defined( C_ENABLE_DYN_TX_DLC )
        #if defined(C_COMP_COSMIC_MCS12X_MSCAN12)  
        CAN_TX_MAILBOX_BASIS_ADR(channel) -> Dlc = (canDynTxDLC[dynTxObj]);
        #endif
        
#  endif
     }
     else
     {          /* set part of static objects assocciated the dynamic --------*/
#  if defined( C_ENABLE_DYN_TX_ID )
#   if defined( C_ENABLE_TX_MASK_EXT_ID )
#    if defined( C_ENABLE_MIXED_ID )
        if (CanGetTxIdType(txHandle)==kCanIdTypeStd)
        {
          #if defined(C_COMP_COSMIC_MCS12X_MSCAN12)  
          CAN_TX_MAILBOX_BASIS_ADR(channel) -> Id   = CanGetTxId0(txHandle);
          CAN_TX_MAILBOX_BASIS_ADR(channel) -> Prio = TX1; /* Prio Std Tx > Prio LowLevel Tx */
          #endif
          
          
        }
        else
#    endif
        {
          /* mask extened ID */
          #if defined(C_COMP_COSMIC_MCS12X_MSCAN12)  
          CAN_TX_MAILBOX_BASIS_ADR(channel) -> Id   = (CanGetTxId0(txHandle)|canTxMask0[channel]);
          CAN_TX_MAILBOX_BASIS_ADR(channel) -> Prio = TX1; /* Prio Std Tx > Prio LowLevel Tx */
          #endif
          
          
        }
#   else
        #if defined(C_COMP_COSMIC_MCS12X_MSCAN12)  
        CAN_TX_MAILBOX_BASIS_ADR(channel) -> Id   = CanGetTxId0(txHandle);
        CAN_TX_MAILBOX_BASIS_ADR(channel) -> Prio = TX1; /* Prio Std Tx > Prio LowLevel Tx */
        #endif
        
        
#   endif
#  endif

#  if defined( C_ENABLE_DYN_TX_DLC )
#   if defined( C_ENABLE_VARIABLE_DLC )
        /* init DLC, RAM */
        #if defined(C_COMP_COSMIC_MCS12X_MSCAN12)  
        CAN_TX_MAILBOX_BASIS_ADR(channel) -> Dlc = (canTxDLC_RAM[txHandle]);
        #endif
        
#   else
        /* init DLC, ROM */
        #if defined(C_COMP_COSMIC_MCS12X_MSCAN12)  
        CAN_TX_MAILBOX_BASIS_ADR(channel) -> Dlc = CanGetTxDlc(txHandle);
        #endif
        
#   endif
#  endif
     }
# endif
     /* set static part commen for static and dynamic objects ----------------*/
# if defined( C_ENABLE_DYN_TX_ID )
# else
#  if defined( C_ENABLE_TX_MASK_EXT_ID )
#   if defined( C_ENABLE_MIXED_ID )
     if (CanGetTxIdType(txHandle)==kCanIdTypeStd)
     {
       #if defined(C_COMP_COSMIC_MCS12X_MSCAN12)  
       CAN_TX_MAILBOX_BASIS_ADR(channel) -> Id   = CanGetTxId0(txHandle);
       CAN_TX_MAILBOX_BASIS_ADR(channel) -> Prio = TX1; /* Prio Std Tx > Prio LowLevel Tx */
       #endif
       
       
     }
     else
#   endif
     {
       /* mask extened ID */
       #if defined(C_COMP_COSMIC_MCS12X_MSCAN12)  
       CAN_TX_MAILBOX_BASIS_ADR(channel) -> Id   = (CanGetTxId0(txHandle)|canTxMask0[channel]);
       CAN_TX_MAILBOX_BASIS_ADR(channel) -> Prio = TX1; /* Prio Std Tx > Prio LowLevel Tx */
       #endif
       
       
     }
#  else
     #if defined(C_COMP_COSMIC_MCS12X_MSCAN12)  
     CAN_TX_MAILBOX_BASIS_ADR(channel) -> Id   = CanGetTxId0(txHandle);
     CAN_TX_MAILBOX_BASIS_ADR(channel) -> Prio = TX1; /* Prio Std Tx > Prio LowLevel Tx */
     #endif
     
     
#  endif
# endif
# if defined( C_ENABLE_DYN_TX_DLC )
# else
#  if defined( C_ENABLE_VARIABLE_DLC )
     /* init DLC, RAM */
     #if defined(C_COMP_COSMIC_MCS12X_MSCAN12)  
     CAN_TX_MAILBOX_BASIS_ADR(channel) -> Dlc = (canTxDLC_RAM[txHandle]);
     #endif
     
#  else
     /* init DLC, ROM */
     #if defined(C_COMP_COSMIC_MCS12X_MSCAN12)  
     CAN_TX_MAILBOX_BASIS_ADR(channel) -> Dlc = CanGetTxDlc(txHandle);
     #endif
     
#  endif
# endif

# if defined( C_ENABLE_MIXED_ID )
#  if defined( C_HL_ENABLE_IDTYPE_IN_ID )
#  else
#   if defined( C_ENABLE_DYN_TX_DLC ) || \
       defined( C_ENABLE_DYN_TX_ID )
     if (txHandle >=  CAN_HL_TX_DYN_ROM_STARTINDEX(channel))
     {                      /* set dynamic part of dynamic objects */
#    if defined( C_ENABLE_DYN_TX_ID )
#    else
#    endif
     }
#   else
#   endif
#  endif
# endif

   }


 /* call pretransmit function ----------------------------------------------- */
# if defined( C_ENABLE_PRETRANSMIT_FCT )

   /* pointer needed for other modules */
   CanBeginCriticalXGateSection();
   #if defined(C_COMP_COSMIC_MCS12X_MSCAN12)  
   CTBSEL                      = CanMailboxSelect[txObjHandle]; /* select the appropriate Tx buffer */
   (txStruct.pChipData)                = (CanChipDataPtr)&(CAN_TX_MAILBOX_BASIS_ADR(channel) -> DataFld[0]);
   canRDSTxPtr[logTxObjHandle] = (txStruct.pChipData);
   #endif
   
   
   CanEndCriticalXGateSection();
   txStruct.Handle      = txHandle;

   {  
#  if defined( C_HL_ENABLE_DUMMY_FCT_CALL )
#  else
    /* Is there a PreTransmit function ? ------------------------------------- */ 
    if ( CanGetApplPreTransmitPtr(txHandle) != V_NULL )    /* if PreTransmit exists */ 
#  endif
    { 
      if ( (CanGetApplPreTransmitPtr(txHandle)) (txStruct) == kCanNoCopyData)  
      { 
      
        
        /* Do not copy the data - already done by the PreTransmit-function */  
        /* --- start transmission --- */  
        goto startTransmission; 
      } 
    } 
   }  
# endif /* C_ENABLE_PRETRANSMIT_FCT */

 /* copy data --------------------------------------------------------------- */
# if defined( C_ENABLE_COPY_TX_DATA )  
#  if defined( C_ENABLE_DYN_TX_DATAPTR )  
   if (txHandle >=  CAN_HL_TX_DYN_ROM_STARTINDEX(channel))  
   {  
      CanMemCopySrcPtr = canDynTxDataPtr[dynTxObj];  
   }  
   else  
#  endif  
   {  
     CanMemCopySrcPtr = CanGetTxDataPtr(txHandle);  
   }  
 /* copy via index in MsgObj data field, copy always 8 bytes -----------*/  
   if ( CanMemCopySrcPtr != V_NULL )   /* copy if buffer exists */
   {
#  if C_SECURITY_LEVEL > 10
     CanNestedGlobalInterruptDisable();  
#  endif

     #if defined(C_COMP_COSMIC_MCS12X_MSCAN12)  
     pDestData = (vuint8*)&(CAN_TX_MAILBOX_BASIS_ADR(channel) -> DataFld[0]);
     # if defined (V_ENABLE_USED_GLOBAL_VAR)
     pSrcData  = (V_MEMRAM1_FAR vuint8 V_MEMRAM2_FAR V_MEMRAM3_FAR *)CanMemCopySrcPtr;
     # else
     pSrcData  = (vuint8 *)CanMemCopySrcPtr;
     # endif
     for(i = 7; i >= 0; i--)
     {
       pDestData[i] = pSrcData[i];
     }
     #endif
     

#  if C_SECURITY_LEVEL > 10
     CanNestedGlobalInterruptRestore();  
#  endif
   }
# endif /* ( C_ENABLE_COPY_TX_DATA ) */  

   CANDRV_SET_CODE_TEST_POINT(0x10A);

# if defined( C_ENABLE_PRETRANSMIT_FCT )
/* Msg(4:2015) This label is not a case or default label for a switch statement. MISRA Rule 55 */
startTransmission:
# endif

   /* test offline and handle and start transmission ------------------------ */
   CanNestedGlobalInterruptDisable();  
   /* If CanTransmit was interrupted by a re-initialization or CanOffline */  
   /* no transmitrequest of this action should be started      */  
   if ((canHandleCurTxObj[logTxObjHandle] == txHandle) && ((canStatus[channel] & kCanTxOn) == kCanTxOn))
   {  
     
     
     #if defined(C_ENABLE_TX_POLLING)
     # if defined(C_COMP_COSMIC_MCS12X_MSCAN12)  
     canllTxStatusFlag[channel] |= TX1;
     # endif
     
     #endif
     
     #if defined(C_COMP_COSMIC_MCS12X_MSCAN12)  
     CanBeginCriticalXGateSection();
     CANTFLG = CanMailboxSelect[txObjHandle]; /* transmit message    */
     CTBSEL = TX2; /* ESCAN00022867, the assignment is relevant if normal transmission interrupts low-level transmission */
     # if defined (C_ENABLE_INDIVIDUAL_POLLING) || (!defined (C_ENABLE_INDIVIDUAL_POLLING) && !defined(C_ENABLE_TX_POLLING))
     #  if defined ( C_ENABLE_INDIVIDUAL_POLLING )
     if (CanHwObjIndivPolling[CAN_HWOBJINDIVPOLLING_INDEX0][txObjHandle] == (vuint8)0x00)
     #  endif
     {
       /* in case of disabled Tx Irq the request needs to be stored in the old status variable */
       if(canCanIrqDisabled[channel] == kCanTrue)
       {
         canCanInterruptOldStatus[channel].oldCanCTIER |= CanMailboxSelect[txObjHandle]; /* enable Tx interrupt */
       }
       else
       {
         CTIER |= CanMailboxSelect[txObjHandle]; /* enable Tx interrupt */
       }
     }
     # endif
     CanEndCriticalXGateSection();
     #endif
     

# if defined( C_ENABLE_TX_OBSERVE )
     ApplCanTxObjStart( CAN_CHANNEL_CANPARA_FIRST logTxObjHandle );
# endif
     rc = kCanTxOk;                                    
   }  
   else  
   {  
# if defined( C_ENABLE_CAN_CANCEL_NOTIFICATION )
     if (canHandleCurTxObj[logTxObjHandle] == txHandle)
     {
       /* only CanOffline was called on higher level */
       rc = kCanTxNotify;
     }
     else
# endif
     {
       rc = kCanTxFailed;   
     }
     assertInternal((canHandleCurTxObj[logTxObjHandle] == txHandle) || (canHandleCurTxObj[logTxObjHandle] == kCanBufferFree),
                                                                                       channel, kErrorTxHandleWrong);  /* PRQA S 3109 */
     canHandleCurTxObj[logTxObjHandle] = kCanBufferFree;  /* release TxHandle (CanOffline) */
   }  

   CanNestedGlobalInterruptRestore();

   
   return (rc);   

} /* END OF CanCopyDataAndStartTransmission */
/* CODE CATEGORY 1 END*/
/* PRQA S 2001 ++ */    /* suppress misra message about usage of goto */
#endif /* if defined( C_ENABLE_CAN_TRANSMIT ) */


#if defined( C_ENABLE_TX_POLLING ) || \
    defined( C_ENABLE_RX_BASICCAN_POLLING ) || \
    defined( C_ENABLE_ERROR_POLLING ) || \
    defined( C_ENABLE_WAKEUP_POLLING ) 
/****************************************************************************
| NAME:             CanTask
| CALLED BY:        application
| PRECONDITIONS:
| INPUT PARAMETERS: none 
| RETURN VALUES:    none
| DESCRIPTION:      - cyclic Task, 
|                   - polling error bus off
|                   - polling Tx objects
|                   - polling Rx objects
****************************************************************************/
/* CODE CATEGORY 2 START*/
C_API_1 void C_API_2 CanTask(void)
{
  CAN_CHANNEL_CANTYPE_LOCAL

# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  for (channel = 0; channel < kCanNumberOfChannels; channel++)
# endif
  {
    {
# if defined( C_ENABLE_ERROR_POLLING )
      CanErrorTask(CAN_CHANNEL_CANPARA_ONLY);
# endif

# if defined( C_ENABLE_SLEEP_WAKEUP )
#  if defined( C_ENABLE_WAKEUP_POLLING )
      CanWakeUpTask(CAN_CHANNEL_CANPARA_ONLY);
#  endif
# endif

# if defined( C_ENABLE_TX_POLLING ) 
      CanTxTask(CAN_CHANNEL_CANPARA_ONLY);
# endif


# if defined( C_ENABLE_RX_BASICCAN_OBJECTS ) && \
    defined( C_ENABLE_RX_BASICCAN_POLLING )
      CanRxBasicCANTask(CAN_CHANNEL_CANPARA_ONLY);
# endif
    }
  }
}
/* CODE CATEGORY 2 END*/
#endif

#if defined( C_ENABLE_ERROR_POLLING )
/****************************************************************************
| NAME:             CanErrorTask
| CALLED BY:        application
| PRECONDITIONS:
| INPUT PARAMETERS: none 
| RETURN VALUES:    none
| DESCRIPTION:      - cyclic Task, 
|                   - polling error status
****************************************************************************/
/* CODE CATEGORY 2 START*/
C_API_1 void C_API_2 CanErrorTask( CAN_CHANNEL_CANTYPE_ONLY )
{

# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser(channel < kCanNumberOfChannels, kCanAllChannels, kErrorChannelHdlTooLarge);    /* PRQA S 3109 */
# endif
  assertUser((canPollingTaskActive[channel] == (vuint8)0), channel, kErrorPollingTaskRecursion);     /* PRQA S 3109 */

  if (canPollingTaskActive[channel] == (vuint8)0)  /* avoid reentrance */
  {
    canPollingTaskActive[channel] = 1;

    {
# if defined( C_ENABLE_SLEEP_WAKEUP )
      if ( !CanLL_HwIsSleep(CAN_HW_CHANNEL_CANPARA_ONLY))
# endif
      {
        CAN_CAN_INTERRUPT_DISABLE(CAN_CHANNEL_CANPARA_ONLY);
        CanHL_ErrorHandling(CAN_HW_CHANNEL_CANPARA_ONLY);
        CAN_CAN_INTERRUPT_RESTORE(CAN_CHANNEL_CANPARA_ONLY);
      }
    }

    canPollingTaskActive[channel] = 0;
  }
}
/* CODE CATEGORY 2 END*/
#endif

#if defined( C_ENABLE_SLEEP_WAKEUP )
# if defined( C_ENABLE_WAKEUP_POLLING )
/****************************************************************************
| NAME:             CanWakeUpTask
| CALLED BY:        application
| PRECONDITIONS:
| INPUT PARAMETERS: none 
| RETURN VALUES:    none
| DESCRIPTION:      - cyclic Task, 
|                   - polling CAN wakeup event
****************************************************************************/
/* CODE CATEGORY 2 START*/
C_API_1 void C_API_2 CanWakeUpTask(CAN_CHANNEL_CANTYPE_ONLY)
{
  CanDeclareGlobalInterruptOldStatus

#  if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser(channel < kCanNumberOfChannels, kCanAllChannels, kErrorChannelHdlTooLarge);     /* PRQA S 3109 */
#  endif
  assertUser((canPollingTaskActive[channel] == (vuint8)0), channel, kErrorPollingTaskRecursion);    /* PRQA S 3109 */

  if (canPollingTaskActive[channel] == (vuint8)0)  /* avoid reentrance */
  {
    canPollingTaskActive[channel] = 1;

    if((CRFLG & WUPIF) == WUPIF) /* check if a wakeup occured */
    {
      CANDRV_SET_CODE_TEST_POINT(0x111);
      CanNestedGlobalInterruptDisable();          /* ESCAN00021223 */
      CanLL_WakeUpHandling(CAN_CHANNEL_CANPARA_ONLY);
      CanNestedGlobalInterruptRestore();          /* ESCAN00021223 */
    }
    canPollingTaskActive[channel] = 0;
  }
}
/* CODE CATEGORY 2 END*/
# endif /* C_ENABLE_WAKEUP_POLLING */
#endif /* C_ENABLE_SLEEP_WAKEUP */

#if defined( C_ENABLE_TX_POLLING ) 
/****************************************************************************
| NAME:             CanTxTask
| CALLED BY:        application
| PRECONDITIONS:
| INPUT PARAMETERS: none 
| RETURN VALUES:    none
| DESCRIPTION:      - cyclic Task, 
|                   - polling Tx objects
****************************************************************************/
/* CODE CATEGORY 2 START*/
C_API_1 void C_API_2 CanTxTask( CAN_CHANNEL_CANTYPE_ONLY )
{
  CanObjectHandle      txObjHandle;

  vuint8 tmpCondition;
  CanDeclareGlobalInterruptOldStatus


# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser(channel < kCanNumberOfChannels, kCanAllChannels, kErrorChannelHdlTooLarge);    /* PRQA S 3109 */
# endif
  assertUser((canPollingTaskActive[channel] == (vuint8)0), channel, kErrorPollingTaskRecursion);    /* PRQA S 3109 */

  if (canPollingTaskActive[channel] == (vuint8)0)  /* avoid reentrance */
  {
    canPollingTaskActive[channel] = 1;


# if defined( C_ENABLE_SLEEP_WAKEUP )
    if ( !CanLL_HwIsSleep(CAN_HW_CHANNEL_CANPARA_ONLY))
# endif
    {
      /*--  polling Tx objects ----------------------------------------*/

# if defined( C_ENABLE_TX_POLLING )
      /* check for global confirmation Pending and may be reset global pending confirmation */
      {
        for ( txObjHandle = CAN_HL_HW_TX_STARTINDEX(canHwChannel); txObjHandle < CAN_HL_HW_TX_STOPINDEX(canHwChannel) ; txObjHandle++ )
        {
#  if defined( C_ENABLE_INDIVIDUAL_POLLING )
          if ( CanHwObjIndivPolling[CAN_HWOBJINDIVPOLLING_INDEX0][txObjHandle] != (vuint8)0 )
#  endif
          {
            /* check for dedicated confirmation pending */
            # if defined(C_COMP_COSMIC_MCS12X_MSCAN12)  
            CanNestedGlobalInterruptDisable();
            if(((CANTFLG & CanMailboxSelect[txObjHandle]) != 0) && ((canllTxStatusFlag[channel] & CanMailboxSelect[txObjHandle]) != 0))
            {
              tmpCondition = 1;
            }
            else
            {
              tmpCondition = 0;
            }
            CanNestedGlobalInterruptRestore();
            if(tmpCondition != 0)
            # endif
            {
              CANDRV_SET_CODE_TEST_POINT(0x110);
              CAN_CAN_INTERRUPT_DISABLE(CAN_CHANNEL_CANPARA_ONLY);
              /* do tx confirmation */
              CanHL_TxConfirmation(CAN_HW_CHANNEL_CANPARA_FIRST txObjHandle );
              CAN_CAN_INTERRUPT_RESTORE(CAN_CHANNEL_CANPARA_ONLY);
            }
          } /* if individual polling & object has to be polled */
        }
      }
# endif /*( C_ENABLE_TX_POLLING ) */


# if defined( C_ENABLE_TRANSMIT_QUEUE )
      CanHl_RestartTxQueue( CAN_CHANNEL_CANPARA_ONLY );
# endif /*  C_ENABLE_TRANSMIT_QUEUE */

    } /* if ( CanLL_HwIsSleep... ) */

    canPollingTaskActive[channel] = 0;
  }


} /* END OF CanTxTask */
/* CODE CATEGORY 2 END*/
#endif /* C_ENABLE_TX_POLLING */

#if defined( C_ENABLE_TRANSMIT_QUEUE )
# if defined( C_ENABLE_TX_POLLING ) 
/****************************************************************************
| NAME:             CanHl_RestartTxQueue
| CALLED BY:        CanTxTask, via CanLL (TxMsgDestroyed)
| PRECONDITIONS:
| INPUT PARAMETERS: none 
| RETURN VALUES:    none
| DESCRIPTION:      start transmission if queue entry exists and HW is free
****************************************************************************/
/* CODE CATEGORY 2 START*/
static void CanHl_RestartTxQueue( CAN_CHANNEL_CANTYPE_ONLY )
{
  CanTransmitHandle    txHandle;
#  if  defined( C_ENABLE_CAN_CANCEL_NOTIFICATION )
  vuint8             rc;
#  endif
  CanDeclareGlobalInterruptOldStatus

  CanSignedTxHandle         queueElementIdx;    /* use as signed due to loop */
  CanSignedTxHandle         elementBitIdx;
  tCanQueueElementType             elem;

#  if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertInternal(channel < kCanNumberOfChannels, kCanAllChannels, kErrorInternalChannelHdlTooLarge);      /* PRQA S 3109 */
#  endif


  if (canHandleCurTxObj[(vsintx)CAN_HL_HW_TX_NORMAL_INDEX(canHwChannel) + CAN_HL_TX_OFFSET_HW_TO_LOG(canHwChannel)] == kCanBufferFree)
  {
    for(queueElementIdx = CAN_HL_TXQUEUE_STOPINDEX(channel) - (CanSignedTxHandle)1; 
                             queueElementIdx >= CAN_HL_TXQUEUE_STARTINDEX(channel); queueElementIdx--)
    {
      elem = canTxQueueFlags[queueElementIdx];
      if(elem != (tCanQueueElementType)0) /* is there any flag set in the queue element? */
      {

        /* Transmit Queued Objects */
        /* start the bitsearch */
        {
        #if defined( C_CPUTYPE_16BIT )
          if((elem & (tCanQueueElementType)0x0000FF00) != (tCanQueueElementType)0)
          {
            if((elem & (tCanQueueElementType)0x0000F000) != (tCanQueueElementType)0)
            {
              elementBitIdx = (CanSignedTxHandle)CanGetHighestFlagFromNibble[elem >> 12] + 12;
            }
            else /*0x00000F00*/
            {
              elementBitIdx = (CanSignedTxHandle)CanGetHighestFlagFromNibble[elem >> 8] + 8;
            }
          }
          else
        #endif
          {
            if((elem & (tCanQueueElementType)0x000000F0) != (tCanQueueElementType)0)
            {
              elementBitIdx = (CanSignedTxHandle)CanGetHighestFlagFromNibble[elem >> 4] + 4;
            }
            else /* 0x0000000F*/
            {
              elementBitIdx = (CanSignedTxHandle)CanGetHighestFlagFromNibble[elem];
            }
          }
        }
        txHandle = (((CanTransmitHandle)queueElementIdx << kCanTxQueueShift) + (CanTransmitHandle)elementBitIdx) - CAN_HL_TXQUEUE_PADBITS(channel);
        {

            CanNestedGlobalInterruptDisable();
            if (canHandleCurTxObj[(vsintx)CAN_HL_HW_TX_NORMAL_INDEX(canHwChannel) + CAN_HL_TX_OFFSET_HW_TO_LOG(canHwChannel)] == kCanBufferFree)
            {
              if ( (canTxQueueFlags[queueElementIdx] & CanShiftLookUp[elementBitIdx]) != (tCanQueueElementType)0 )
              {
                canTxQueueFlags[queueElementIdx] &= ~CanShiftLookUp[elementBitIdx]; /* clear flag from the queue */
                /* Save hdl of msgObj to be transmitted*/
                canHandleCurTxObj[(vsintx)CAN_HL_HW_TX_NORMAL_INDEX(canHwChannel) + CAN_HL_TX_OFFSET_HW_TO_LOG(canHwChannel)] = txHandle;
                CanNestedGlobalInterruptRestore();  
#  if defined( C_ENABLE_CAN_CANCEL_NOTIFICATION )
                rc = CanCopyDataAndStartTransmission(CAN_CHANNEL_CANPARA_FIRST CAN_HL_HW_TX_NORMAL_INDEX(canHwChannel), txHandle);
                if ( rc == kCanTxNotify)
                {
                  APPLCANCANCELNOTIFICATION(channel, txHandle);
                }
#  else
                (void)CanCopyDataAndStartTransmission(CAN_CHANNEL_CANPARA_FIRST CAN_HL_HW_TX_NORMAL_INDEX(canHwChannel), txHandle);
#  endif
                return;
              }

            }
            CanNestedGlobalInterruptRestore();
            return;
        }
      }
    }
  }
} /* End of CanHl_RestartTxQueue */
/* CODE CATEGORY 2 END*/
# endif
#endif /*  C_ENABLE_TRANSMIT_QUEUE */


#if defined( C_ENABLE_RX_BASICCAN_POLLING ) && \
    defined( C_ENABLE_RX_BASICCAN_OBJECTS )
/****************************************************************************
| NAME:             CanRxBasicCANTask
| CALLED BY:        application
| PRECONDITIONS:
| INPUT PARAMETERS: none 
| RETURN VALUES:    none
| DESCRIPTION:      - cyclic Task, 
|                   - polling Rx BasicCAN objects
****************************************************************************/
/* CODE CATEGORY 2 START*/
C_API_1 void C_API_2 CanRxBasicCANTask(CAN_CHANNEL_CANTYPE_ONLY)
{

  CanObjectHandle     rxObjHandle;          /* keyword register removed 2005-06-29 Ht */


# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser(channel < kCanNumberOfChannels, kCanAllChannels, kErrorChannelHdlTooLarge);    /* PRQA S 3109 */
# endif
  assertUser((canPollingTaskActive[channel] == (vuint8)0), channel, kErrorPollingTaskRecursion);   /* PRQA S 3109 */

  if (canPollingTaskActive[channel] == (vuint8)0)  /* avoid reentrance */
  {
    canPollingTaskActive[channel] = 1;


# if defined( C_ENABLE_SLEEP_WAKEUP )
    if ( !CanLL_HwIsSleep(CAN_HW_CHANNEL_CANPARA_ONLY))
# endif
    {

      {
        for (rxObjHandle=CAN_HL_HW_RX_BASIC_STARTINDEX(canHwChannel); rxObjHandle<CAN_HL_HW_RX_BASIC_STOPINDEX(canHwChannel); rxObjHandle++ )
        {
# if defined( C_ENABLE_INDIVIDUAL_POLLING )
          if ( CanHwObjIndivPolling[CAN_HWOBJINDIVPOLLING_INDEX0][rxObjHandle] != (vuint8)0 )
# endif
          {
            /* check for dedicated indication pending */
            if(RXF == (CRFLG & RXF)) /* something received? */
            {
              CANDRV_SET_CODE_TEST_POINT(0x108);

              CAN_CAN_INTERRUPT_DISABLE(CAN_CHANNEL_CANPARA_ONLY);
              CanBasicCanMsgReceived( CAN_HW_CHANNEL_CANPARA_FIRST rxObjHandle);
              CAN_CAN_INTERRUPT_RESTORE(CAN_CHANNEL_CANPARA_ONLY);
            }
          } /* if individual polling & object has to be polled */
        }
      }
    } /* if ( CanLL_HwIsSleep... )  */

    canPollingTaskActive[channel] = 0;
  }

} /* END OF CanRxTask */
/* CODE CATEGORY 2 END*/
#endif /* C_ENABLE_RX_BASICCAN_POLLING && C_ENABLE_RX_BASICCAN_OBJECTS */

/****************************************************************************
| NAME:             CanHL_ErrorHandling
| CALLED BY:        CanISR(), CanErrorTask()
| PRECONDITIONS:
| INPUT PARAMETERS: none 
| RETURN VALUES:    none
| DESCRIPTION:      - error interrupt (busoff, error warning,...)
****************************************************************************/
/* CODE CATEGORY 2 START*/
static void CanHL_ErrorHandling( CAN_HW_CHANNEL_CANTYPE_ONLY )
{

#if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertInternal(canHwChannel < kCanNumberOfHwChannels, kCanAllChannels, kErrorInternalChannelHdlTooLarge);      /* PRQA S 3109 */
#endif




  /* check for status register (bus error)--*/
  if( ((CRFLG & BOFFIF) == BOFFIF) && ((CRFLG & CSCIF) == CSCIF) )
  {
    /*==BUS OFF ERROR=========================*/
    APPL_CAN_BUSOFF( CAN_CHANNEL_CANPARA_ONLY );            /* call application specific function */
  }

#if defined( C_HL_ENABLE_OVERRUN_IN_STATUS )
  /* check for status register (overrun occured)--*/
# if defined( C_ENABLE_OVERRUN )
  if(OVRIF == (CRFLG & OVRIF))
  {
    ApplCanOverrun( CAN_CHANNEL_CANPARA_ONLY );
  }
# endif
#endif
  CRFLG = (OVRIF | CSCIF); /* clear overrun flag and status changed */

} /* END OF CanHL_ErrorHandling */
/* CODE CATEGORY 2 END*/


#if defined( C_ENABLE_INDIVIDUAL_POLLING )
# if defined( C_ENABLE_TX_POLLING )
/****************************************************************************
| NAME:             CanTxObjTask()
| CALLED BY:        
| PRECONDITIONS:    
| INPUT PARAMETERS: CAN_HW_CHANNEL_CANTYPE_FIRST 
|                   CanObjectHandle txObjHandle
| RETURN VALUES:    none
| DESCRIPTION:      Polling special Object
****************************************************************************/
/* CODE CATEGORY 2 START*/
C_API_1 void C_API_2 CanTxObjTask(CAN_HW_CHANNEL_CANTYPE_FIRST CanObjectHandle txObjHandle)      /* PRQA S 1330 */
{

  vuint8 tmpCondition;
  CanDeclareGlobalInterruptOldStatus


#  if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser(canHwChannel < kCanNumberOfHwChannels, kCanAllChannels, kErrorChannelHdlTooLarge);  /* PRQA S 3109 */
#  endif
  assertUser(txObjHandle < CAN_HL_HW_TX_STOPINDEX(canHwChannel), channel, kErrorHwHdlTooLarge);     /* PRQA S 3109 */
  assertUser((CanSignedTxHandle)txObjHandle >= (CanSignedTxHandle)CAN_HL_HW_TX_STARTINDEX(canHwChannel), channel, kErrorHwHdlTooSmall);   /* PRQA S 3109 */ /*lint !e568 */
  assertUser(CanHwObjIndivPolling[CAN_HWOBJINDIVPOLLING_INDEX0][txObjHandle] != (vuint8)0, channel, kErrorHwObjNotInPolling);    /* PRQA S 3109 */
  assertUser((canPollingTaskActive[channel] == (vuint8)0), channel, kErrorPollingTaskRecursion);     /* PRQA S 3109 */

  if (canPollingTaskActive[channel] == (vuint8)0)  /* avoid reentrance */
  {
    canPollingTaskActive[channel] = 1;

#  if defined( C_ENABLE_SLEEP_WAKEUP )
    if ( !CanLL_HwIsSleep(CAN_HW_CHANNEL_CANPARA_ONLY))
#  endif
    {

      /* check for dedicated confirmation pending */
      # if defined(C_COMP_COSMIC_MCS12X_MSCAN12)  
      CanNestedGlobalInterruptDisable();
      if(((CANTFLG & CanMailboxSelect[txObjHandle]) != 0) && ((canllTxStatusFlag[channel] & CanMailboxSelect[txObjHandle]) != 0))
      {
        tmpCondition = 1;
      }
      else
      {
        tmpCondition = 0;
      }
      CanNestedGlobalInterruptRestore();
      if(tmpCondition != 0)
      # endif
      {
        CAN_CAN_INTERRUPT_DISABLE(CAN_CHANNEL_CANPARA_ONLY);
        /* do tx confirmation */
        CanHL_TxConfirmation(CAN_HW_CHANNEL_CANPARA_FIRST txObjHandle );
        CAN_CAN_INTERRUPT_RESTORE(CAN_CHANNEL_CANPARA_ONLY);
      }

#  if defined( C_ENABLE_TRANSMIT_QUEUE )
      if ( txObjHandle == CAN_HL_HW_TX_NORMAL_INDEX(channel) )
      {
        CanHl_RestartTxQueue( CAN_CHANNEL_CANPARA_ONLY );
      }
#  endif /*  C_ENABLE_TRANSMIT_QUEUE */
    }

    canPollingTaskActive[channel] = 0;
  }
} /*CanTxObjTask*/
/* CODE CATEGORY 2 END*/
# endif    /* defined( C_ENABLE_INDIVIDUAL_POLLING ) && defined( C_ENABLE_TX_POLLING ) */


# if defined( C_ENABLE_RX_BASICCAN_POLLING ) && \
    defined( C_ENABLE_RX_BASICCAN_OBJECTS )
/****************************************************************************
| NAME:             CanRxBasicCANObjTask()
| CALLED BY:        
| PRECONDITIONS:    
| INPUT PARAMETERS: CAN_HW_CHANNEL_CANTYPE_FIRST 
|                   CanObjectHandle rxObjHandle
| RETURN VALUES:    none
| DESCRIPTION:      Polling special Object
****************************************************************************/
/* CODE CATEGORY 2 START*/
C_API_1 void C_API_2 CanRxBasicCANObjTask(CAN_HW_CHANNEL_CANTYPE_FIRST CanObjectHandle rxObjHandle)        /* PRQA S 1330 */
{
#  if kCanNumberOfHwObjPerBasicCan > 1
  CanObjectHandle i;
#  endif



#  if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser(canHwChannel < kCanNumberOfHwChannels, kCanAllChannels, kErrorChannelHdlTooLarge);   /* PRQA S 3109 */
#  endif
  assertUser(rxObjHandle < CAN_HL_HW_RX_BASIC_STOPINDEX(canHwChannel), channel, kErrorHwHdlTooLarge);  /* PRQA S 3109 */
  assertUser((CanSignedRxHandle)rxObjHandle >= (CanSignedRxHandle)CAN_HL_HW_RX_BASIC_STARTINDEX(canHwChannel), channel, kErrorHwHdlTooSmall); /* PRQA S 3109 */ /*lint !e568 */
  assertUser(CanHwObjIndivPolling[CAN_HWOBJINDIVPOLLING_INDEX0][rxObjHandle] != (vuint8)0, channel, kErrorHwObjNotInPolling);   /* PRQA S 3109 */
  assertUser((canPollingTaskActive[channel] == (vuint8)0), channel, kErrorPollingTaskRecursion);  /* PRQA S 3109 */

  if (canPollingTaskActive[channel] == (vuint8)0)  /* avoid reentrance */
  {
    canPollingTaskActive[channel] = 1;

#  if defined( C_ENABLE_SLEEP_WAKEUP )
    if ( !CanLL_HwIsSleep(CAN_HW_CHANNEL_CANPARA_ONLY) )
#  endif
    {

#  if kCanNumberOfHwObjPerBasicCan > 1
      /* loop over all HW objects assigned to one BasicCAN  */
      for (i = 0; i < kCanNumberOfHwObjPerBasicCan; i++)
      {
#  endif
        /* check for dedicated indication pending */
        if(RXF == (CRFLG & RXF)) /* something received? */
        {
          CANDRV_SET_CODE_TEST_POINT(0x109);
          CAN_CAN_INTERRUPT_DISABLE(CAN_CHANNEL_CANPARA_ONLY);
          CanBasicCanMsgReceived( CAN_HW_CHANNEL_CANPARA_FIRST rxObjHandle);
          CAN_CAN_INTERRUPT_RESTORE(CAN_CHANNEL_CANPARA_ONLY);
        }
#  if kCanNumberOfHwObjPerBasicCan > 1
        rxObjHandle++;
      }
#  endif
    }
    canPollingTaskActive[channel] = 0;
  }
} /*CanRxBasicCANObjTask*/
/* CODE CATEGORY 2 END*/
# endif
#endif /*C_ENABLE_INDIVIDUAL_POLLING*/

#if defined( C_ENABLE_RX_BASICCAN_OBJECTS )
/****************************************************************************
| NAME:             CanBasicCanMsgReceived
| CALLED BY:        CanISR()
| PRECONDITIONS:
| INPUT PARAMETERS: internal can chip number
| RETURN VALUES:    none
| DESCRIPTION:      - basic can receive
****************************************************************************/
/* PRQA S 2001 ++ */    /* suppress misra message about usage of goto */
/* CODE CATEGORY 1 START*/
static void CanBasicCanMsgReceived( CAN_HW_CHANNEL_CANTYPE_FIRST CanObjectHandle rxObjHandle)
{
# if defined( C_HL_ENABLE_RX_INFO_STRUCT_PTR )
  tCanRxInfoStruct    *pCanRxInfoStruct;
# endif  
  
# if ( !defined( C_SEARCH_HASH ) )  ||\
     defined( C_HL_ENABLE_HW_RANGES_FILTER ) || \
     defined( C_ENABLE_RANGE_0 ) || \
     defined( C_ENABLE_RANGE_1 ) || \
     defined( C_ENABLE_RANGE_2 ) || \
     defined( C_ENABLE_RANGE_3 )
  tCanRxId0 idRaw0;
#  if (kCanNumberOfUsedCanRxIdTables > 1)
  tCanRxId1 idRaw1;
#  endif
#  if (kCanNumberOfUsedCanRxIdTables > 2)
  tCanRxId2 idRaw2;
#  endif
#  if (kCanNumberOfUsedCanRxIdTables > 3)
  tCanRxId3 idRaw3;
#  endif
#  if (kCanNumberOfUsedCanRxIdTables > 4)
  tCanRxId4 idRaw4;
#  endif
# endif

# if defined( C_SEARCH_HASH )
#  if (kCanNumberOfRxBasicCANObjects > 0)
#   if (kHashSearchListCountEx > 0)
  vuint32          idExt;
#    if (kHashSearchListCountEx > 1)
  vuint32          winternExt;        /* prehashvalue         */
#    endif
#   endif
#   if (kHashSearchListCount > 0)
  vuint16          idStd;
#    if (kHashSearchListCount > 1)
  vuint16          winternStd;        /* prehashvalue         */
#    endif
#   endif
#   if (((kHashSearchListCountEx > 1) && (kHashSearchMaxStepsEx > 1)) ||\
        ((kHashSearchListCount > 1)   && (kHashSearchMaxSteps > 1))) 
  vuint16          i_increment;    /* delta for next step  */
  vsint16          count;
#   endif
#  endif  /* kCanNumberOfRxBasicCANObjects > 0 */
# endif

  #if defined(C_COMP_COSMIC_MCS12X_MSCAN12)  
  # if defined(C_ENABLE_XGATE_USED)
  #  if defined(C_HL_ENABLE_HW_RANGES_FILTER)
  #   if defined(C_ENABLE_RANGE_0) || defined(C_ENABLE_RANGE_1) || \
         defined(C_ENABLE_RANGE_2) || defined(C_ENABLE_RANGE_3)
  vuint8 tmpCanXgHitRange;
  #   endif
  #  endif
  vuint16 index; /* used to acknowledge pending XGate interrupt */
  vuint16 nInfoIndex;
  # endif
  #endif
  
  #if defined(C_COMP_COSMIC_MCS12X_MSCAN12)  
  vuint8* pId; /* points to the current ID in hardware (temp buffer for XGate) for remote frame check */
  #endif

# if defined( C_ENABLE_GENERIC_PRECOPY ) || \
    defined( C_ENABLE_PRECOPY_FCT )     || \
    defined( C_ENABLE_COPY_RX_DATA )    || \
    defined( C_ENABLE_INDICATION_FLAG ) || \
    defined( C_ENABLE_INDICATION_FCT )  || \
    defined( C_ENABLE_DLC_CHECK )       || \
    defined( C_ENABLE_NOT_MATCHED_FCT )
#  if (kCanNumberOfRxBasicCANObjects > 0)

  CanReceiveHandle         rxHandle;

  rxHandle = kCanRxHandleNotUsed;

#  endif /* kCanNumberOfRxBasicCANObjects > 0 */
# endif

  CANDRV_SET_CODE_TEST_POINT(0x100);

# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertInternal(canHwChannel < kCanNumberOfHwChannels, kCanAllChannels, kErrorInternalChannelHdlTooLarge);  /* PRQA S 3109 */
# endif

  #if defined(C_COMP_COSMIC_MCS12X_MSCAN12)  
  # if defined(C_SEARCH_XGATE)
  pId = (vuint8*)(&(canRxTmpBuf[channel]));
  # else
  pId = (vuint8*)CAN_RX_MAILBOX_BASIS_ADR(channel);
  # endif
  #endif
  
  
  #if defined(C_ENABLE_EXTENDED_ID)
  /* extended ID? */
  if((pId[1] & (vuint8)0x08) == (vuint8)0x08)
  {
    /* remote frame in extended format? */
    if((pId[3] & (vuint8)0x01) == (vuint8)0x01)
    {
      goto finishBasicCan;
    }
  }
  # if defined(C_ENABLE_MIXED_ID)
  else
  {
    /* remote frame in standard format? */
    if((pId[1] & (vuint8)0x10) == (vuint8)0x10)
    {
      goto finishBasicCan;
    }
  }
  # endif
  #else
  /* standard ID? */
  if((pId[1] & (vuint8)0x10) == (vuint8)0x10)
  {
    /* remote frame in standard format? */
    goto finishBasicCan;
  }
  #endif

# if defined( C_HL_ENABLE_RX_INFO_STRUCT_PTR )
  pCanRxInfoStruct =  &canRxInfoStruct[channel];
  #if defined(C_COMP_COSMIC_MCS12X_MSCAN12)  
  # if defined(C_SEARCH_XGATE)
  (pCanRxInfoStruct->pChipMsgObj) = (CanChipMsgPtr)&(canRxTmpBuf[channel]);
  # else
  (pCanRxInfoStruct->pChipMsgObj) = (CanChipMsgPtr)CAN_RX_MAILBOX_BASIS_ADR(channel);
  # endif
  #endif
  
  
  #if defined(C_COMP_COSMIC_MCS12X_MSCAN12)  
  # if defined(C_SEARCH_XGATE)
  (pCanRxInfoStruct->pChipData) = (CanChipDataPtr)&(canRxTmpBuf[channel].DataFld[0]);
  # else
  (pCanRxInfoStruct->pChipData) = (CanChipDataPtr)&(CAN_RX_MAILBOX_BASIS_ADR(channel) -> DataFld[0]);
  # endif
  #endif
  
  
  canRDSRxPtr[channel] = pCanRxInfoStruct->pChipData;
# else
  #if defined(C_COMP_COSMIC_MCS12X_MSCAN12)  
  # if defined(C_SEARCH_XGATE)
  (canRxInfoStruct[channel].pChipMsgObj) = (CanChipMsgPtr)&(canRxTmpBuf[channel]);
  # else
  (canRxInfoStruct[channel].pChipMsgObj) = (CanChipMsgPtr)CAN_RX_MAILBOX_BASIS_ADR(channel);
  # endif
  #endif
  
  
  #if defined(C_COMP_COSMIC_MCS12X_MSCAN12)  
  # if defined(C_SEARCH_XGATE)
  (canRxInfoStruct[channel].pChipData) = (CanChipDataPtr)&(canRxTmpBuf[channel].DataFld[0]);
  # else
  (canRxInfoStruct[channel].pChipData) = (CanChipDataPtr)&(CAN_RX_MAILBOX_BASIS_ADR(channel) -> DataFld[0]);
  # endif
  #endif
  
  
  canRDSRxPtr[channel] = canRxInfoStruct[channel].pChipData;
# endif
  CAN_HL_P_RX_INFO_STRUCT_HANDLE(channel)      = kCanRxHandleNotUsed;  

# if defined( C_ENABLE_CAN_RAM_CHECK )
  if(canComStatus[channel] == kCanDisableCommunication)
  {
    goto finishBasicCan; /* ignore reception */
  }
# endif

# if defined( C_HL_ENABLE_OVERRUN_IN_STATUS )
# else
#  if defined( C_ENABLE_OVERRUN )
  #  error "not used by this driver"
    ApplCanOverrun( CAN_CHANNEL_CANPARA_ONLY );
  }
#  endif
# endif

  /**************************** reject remote frames  ****************************************/


  /**************************** reject messages with unallowed ID type ****************************************/
# if defined( C_HL_ENABLE_REJECT_UNWANTED_IDTYPE )
#  if defined( C_ENABLE_EXTENDED_ID )
#   if defined( C_ENABLE_MIXED_ID )
#   else
  if (CanRxActualIdType( CAN_HL_P_RX_INFO_STRUCT(channel) ) != kCanIdTypeExt)
  {
    goto finishBasicCan;
  }
#   endif
#  else
  if (CanRxActualIdType( CAN_HL_P_RX_INFO_STRUCT(channel) ) != kCanIdTypeStd)
  {
    goto finishBasicCan;
  }
#  endif
# endif /* C_HL_ENABLE_REJECT_UNWANTED_IDTYPE */


# if defined( C_ENABLE_COND_RECEIVE_FCT )
  /**************************** conditional message receive function  ****************************************/
  if(canMsgCondRecState[channel]==kCanTrue)
  {
    ApplCanMsgCondReceived( CAN_HL_P_RX_INFO_STRUCT(channel) );
  }
# endif

# if defined( C_ENABLE_RECEIVE_FCT )
  /**************************** call ApplCanMsgReceived() ****************************************************/
  if (APPL_CAN_MSG_RECEIVED( CAN_HL_P_RX_INFO_STRUCT(channel) ) == kCanNoCopyData)
  {
    goto finishBasicCan;
  }
# endif
  

# if ( !defined( C_SEARCH_HASH ) ) || \
     defined( C_HL_ENABLE_HW_RANGES_FILTER ) || \
     defined( C_ENABLE_RANGE_0 ) || \
     defined( C_ENABLE_RANGE_1 ) || \
     defined( C_ENABLE_RANGE_2 ) || \
     defined( C_ENABLE_RANGE_3 )
  /**************************** calculate idRaw for filtering ************************************************/
#  if defined( C_ENABLE_EXTENDED_ID )
#   if defined( C_ENABLE_MIXED_ID )
  if (CanRxActualIdType(CAN_HL_P_RX_INFO_STRUCT(channel)) == kCanIdTypeExt)
#   endif
  {
#   if defined( C_ENABLE_RX_MASK_EXT_ID )
    idRaw0 = CanRxActualIdRaw0( CAN_HL_P_RX_INFO_STRUCT(channel) ) & MK_EXTID0(C_MASK_EXT_ID);
#    if (kCanNumberOfUsedCanRxIdTables > 1)
    idRaw1 = CanRxActualIdRaw1( CAN_HL_P_RX_INFO_STRUCT(channel) ) & MK_EXTID1(C_MASK_EXT_ID);
#    endif
#    if (kCanNumberOfUsedCanRxIdTables > 2)
    idRaw2 = CanRxActualIdRaw2( CAN_HL_P_RX_INFO_STRUCT(channel) ) & MK_EXTID2(C_MASK_EXT_ID);
#    endif
#    if (kCanNumberOfUsedCanRxIdTables > 3)
    idRaw3 = CanRxActualIdRaw3( CAN_HL_P_RX_INFO_STRUCT(channel) ) & MK_EXTID3(C_MASK_EXT_ID);
#    endif
#    if (kCanNumberOfUsedCanRxIdTables > 4)
    idRaw4 = CanRxActualIdRaw4( CAN_HL_P_RX_INFO_STRUCT(channel) ) & MK_EXTID4(C_MASK_EXT_ID);
#    endif
#   else
    idRaw0 = CanRxActualIdRaw0( CAN_HL_P_RX_INFO_STRUCT(channel) ) & MK_EXTID0(0x1FFFFFFF);
#    if (kCanNumberOfUsedCanRxIdTables > 1)
    idRaw1 = CanRxActualIdRaw1( CAN_HL_P_RX_INFO_STRUCT(channel) ) & MK_EXTID1(0x1FFFFFFF);
#    endif
#    if (kCanNumberOfUsedCanRxIdTables > 2)
    idRaw2 = CanRxActualIdRaw2( CAN_HL_P_RX_INFO_STRUCT(channel) ) & MK_EXTID2(0x1FFFFFFF);
#    endif
#    if (kCanNumberOfUsedCanRxIdTables > 3)
    idRaw3 = CanRxActualIdRaw3( CAN_HL_P_RX_INFO_STRUCT(channel) ) & MK_EXTID3(0x1FFFFFFF);
#    endif
#    if (kCanNumberOfUsedCanRxIdTables > 4)
    idRaw4 = CanRxActualIdRaw4( CAN_HL_P_RX_INFO_STRUCT(channel) ) & MK_EXTID4(0x1FFFFFFF);
#    endif
#   endif /*  C_ENABLE_RX_MASK_EXT_ID */
  }
#   if defined( C_ENABLE_MIXED_ID )
  else
  {
    idRaw0 = CanRxActualIdRaw0( CAN_HL_P_RX_INFO_STRUCT(channel) ) & MK_STDID0(0x7FF);
#    if (kCanNumberOfUsedCanRxIdTables > 1)
    idRaw1 = CanRxActualIdRaw1( CAN_HL_P_RX_INFO_STRUCT(channel) ) & MK_STDID1(0x7FF);
#    endif
#    if (kCanNumberOfUsedCanRxIdTables > 2)
    idRaw2 = CanRxActualIdRaw2( CAN_HL_P_RX_INFO_STRUCT(channel) ) & MK_STDID2(0x7FF);
#    endif
#    if (kCanNumberOfUsedCanRxIdTables > 3)
    idRaw3 = CanRxActualIdRaw3( CAN_HL_P_RX_INFO_STRUCT(channel) ) & MK_STDID3(0x7FF);
#    endif
#    if (kCanNumberOfUsedCanRxIdTables > 4)
    idRaw4 = CanRxActualIdRaw4( CAN_HL_P_RX_INFO_STRUCT(channel) ) & MK_STDID4(0x7FF);
#    endif
  }
#   endif
#  else /* C_ENABLE_EXTENDED_ID */
  {
    idRaw0 = CanRxActualIdRaw0( CAN_HL_P_RX_INFO_STRUCT(channel) ) & MK_STDID0(0x7FF);
#   if (kCanNumberOfUsedCanRxIdTables > 1)
    idRaw1 = CanRxActualIdRaw1( CAN_HL_P_RX_INFO_STRUCT(channel) ) & MK_STDID1(0x7FF);
#   endif
#   if (kCanNumberOfUsedCanRxIdTables > 2)
    idRaw2 = CanRxActualIdRaw2( CAN_HL_P_RX_INFO_STRUCT(channel) ) & MK_STDID2(0x7FF);
#   endif
#   if (kCanNumberOfUsedCanRxIdTables > 3)
    idRaw3 = CanRxActualIdRaw3( CAN_HL_P_RX_INFO_STRUCT(channel) ) & MK_STDID3(0x7FF);
#   endif
#   if (kCanNumberOfUsedCanRxIdTables > 4)
    idRaw4 = CanRxActualIdRaw4( CAN_HL_P_RX_INFO_STRUCT(channel) ) & MK_STDID4(0x7FF);
#   endif
  }
#  endif /* C_ENABLE_EXTENDED_ID */
# endif /* ( !defined( C_SEARCH_HASH ) && ...  defined( C_ENABLE_RANGE_3 )*/

  /******************* Range filtering ********************************************************************/
# if defined( C_HL_ENABLE_HW_RANGES_FILTER )

  #  if defined(C_ENABLE_RANGE_0) || defined(C_ENABLE_RANGE_1) || \
        defined(C_ENABLE_RANGE_2) || defined(C_ENABLE_RANGE_3)
    tmpCanXgHitRange = canXgHitRange[channel];
  
  #   if defined( C_ENABLE_RANGE_0 )
    if((tmpCanXgHitRange & 0x01) != 0)
    {
  #    if defined ( C_MULTIPLE_RECEIVE_CHANNEL )
      if ( (CanChannelObject[channel].RangeActiveFlag & kCanRange0) != (vuint16)0 )
      {
  #    else    /* C_SINGLE_RECEIVE_CHANNEL) */
      {
  #    endif
  
  #    if defined(C_ENABLE_RX_QUEUE_RANGE)
        if (CanRxQueueRange0[channel] == kCanTrue)
        {
          pCanRxInfoStruct->Handle      = kCanRxHandleRange0;
          (void)CanHL_ReceivedRxHandleQueue( CAN_CHANNEL_CANPARA_ONLY );
          goto finishBasicCan;
        }
      else
  #    endif  /* C_ENABLE_RX_QUEUE */
        {
          if (APPLCANRANGE0PRECOPY( CAN_HL_P_RX_INFO_STRUCT(channel) ) == kCanNoCopyData)
          {
            goto finishBasicCan;
          }
        }
      }
    }
  #   endif  /* C_ENABLE_RANGE_0 */
  
  #   if defined( C_ENABLE_RANGE_1 )
    if((tmpCanXgHitRange & 0x02) != 0)
    {
  #    if defined ( C_MULTIPLE_RECEIVE_CHANNEL )
      if ( (CanChannelObject[channel].RangeActiveFlag & kCanRange1) != (vuint16)0 )
      {
  #    else    /* C_SINGLE_RECEIVE_CHANNEL) */
      {
  #    endif
  
  #    if defined(C_ENABLE_RX_QUEUE_RANGE)
        if (CanRxQueueRange1[channel] == kCanTrue)
        {
          pCanRxInfoStruct->Handle      = kCanRxHandleRange1;
          (void)CanHL_ReceivedRxHandleQueue( CAN_CHANNEL_CANPARA_ONLY );
          goto finishBasicCan;
        }
      else
  #    endif  /* C_ENABLE_RX_QUEUE */
        {
          if (APPLCANRANGE1PRECOPY( CAN_HL_P_RX_INFO_STRUCT(channel) ) == kCanNoCopyData)
          {
            goto finishBasicCan;
          }
        }
      }
    }
  #   endif  /* C_ENABLE_RANGE_1 */
  
  #   if defined( C_ENABLE_RANGE_2 )
    if((tmpCanXgHitRange & 0x04) != 0)
    {
  #    if defined ( C_MULTIPLE_RECEIVE_CHANNEL )
      if ( (CanChannelObject[channel].RangeActiveFlag & kCanRange2) != (vuint16)0 )
      {
  #    else    /* C_SINGLE_RECEIVE_CHANNEL) */
      {
  #    endif
  
  #    if defined(C_ENABLE_RX_QUEUE_RANGE)
        if (CanRxQueueRange2[channel] == kCanTrue)
        {
          pCanRxInfoStruct->Handle      = kCanRxHandleRange2;
          (void)CanHL_ReceivedRxHandleQueue( CAN_CHANNEL_CANPARA_ONLY );
          goto finishBasicCan;
        }
      else
  #    endif  /* C_ENABLE_RX_QUEUE */
        {
          if (APPLCANRANGE2PRECOPY( CAN_HL_P_RX_INFO_STRUCT(channel) ) == kCanNoCopyData)
          {
            goto finishBasicCan;
          }
        }
      }
    }
  #   endif  /* C_ENABLE_RANGE_2 */
  
  #   if defined( C_ENABLE_RANGE_3 )
    if((tmpCanXgHitRange & 0x08) != 0)
    {
  #    if defined ( C_MULTIPLE_RECEIVE_CHANNEL )
      if ( (CanChannelObject[channel].RangeActiveFlag & kCanRange3) != (vuint16)0 )
      {
  #    else    /* C_SINGLE_RECEIVE_CHANNEL) */
      {
  #    endif
  
  #    if defined(C_ENABLE_RX_QUEUE_RANGE)
        if (CanRxQueueRange3[channel] == kCanTrue)
        {
          pCanRxInfoStruct->Handle      = kCanRxHandleRange3;
          (void)CanHL_ReceivedRxHandleQueue( CAN_CHANNEL_CANPARA_ONLY );
          goto finishBasicCan;
        }
      else
  #    endif  /* C_ENABLE_RX_QUEUE */
        {
          if (APPLCANRANGE3PRECOPY( CAN_HL_P_RX_INFO_STRUCT(channel) ) == kCanNoCopyData)
          {
            goto finishBasicCan;
          }
        }
      }
    }
  #   endif  /* C_ENABLE_RANGE_3 */
  
  #  endif

# else

  {
#  if defined( C_ENABLE_RANGE_0 )
#   if defined( C_MULTIPLE_RECEIVE_CHANNEL )
    if ( (CanChannelObject[channel].RangeActiveFlag & kCanRange0) != (vuint16)0 )
    {
#    if defined( C_ENABLE_MIXED_ID ) && !defined( C_HL_ENABLE_IDTYPE_IN_ID )
      if (CanRxActualIdType( CAN_HL_P_RX_INFO_STRUCT(channel) ) == CANRANGE0IDTYPE(channel))
#    endif
      {
        if ( C_RANGE_MATCH( CAN_RX_IDRAW_PARA, CANRANGE0ACCMASK(channel), CANRANGE0ACCCODE(channel)) )
#   else    /* C_SINGLE_RECEIVE_CHANNEL) */
    {
#    if (C_RANGE0_IDTYPE == kCanIdTypeStd )
#     if defined( C_ENABLE_MIXED_ID ) && !defined( C_HL_ENABLE_IDTYPE_IN_ID )
      if (CanRxActualIdType( CAN_HL_P_RX_INFO_STRUCT(channel) ) == kCanIdTypeStd)
#     endif
      {
        if ( C_RANGE_MATCH_STD( CAN_RX_IDRAW_PARA, CANRANGE0ACCMASK(channel), CANRANGE0ACCCODE(channel)) )
#    else  /* C_RANGE_0_IDTYPE == kCanIdTypeExt */
#     if defined( C_ENABLE_MIXED_ID ) && !defined( C_HL_ENABLE_IDTYPE_IN_ID )
      if (CanRxActualIdType( CAN_HL_P_RX_INFO_STRUCT(channel) ) == kCanIdTypeExt)
#     endif
      {
        if ( C_RANGE_MATCH_EXT( CAN_RX_IDRAW_PARA, CANRANGE0ACCMASK(channel), CANRANGE0ACCCODE(channel)) )
#    endif
#   endif
        {
#   if defined( C_ENABLE_RX_QUEUE_RANGE )
          if (CanRxQueueRange0[channel] == kCanTrue)
          {
            pCanRxInfoStruct->Handle      = kCanRxHandleRange0;
            (void)CanHL_ReceivedRxHandleQueue( CAN_CHANNEL_CANPARA_ONLY );
            goto finishBasicCan;
          }
          else
#   endif  /* C_ENABLE_RX_QUEUE */
          {
            if (APPLCANRANGE0PRECOPY( CAN_HL_P_RX_INFO_STRUCT(channel) ) == kCanNoCopyData)
            {
              goto finishBasicCan;
            }
          }
        }
      }
    }
#  endif  /* C_ENABLE_RANGE_0 */

#  if defined( C_ENABLE_RANGE_1 )
#   if defined( C_MULTIPLE_RECEIVE_CHANNEL )
    if ( (CanChannelObject[channel].RangeActiveFlag & kCanRange1) != (vuint16)0 )
    {
#    if defined( C_ENABLE_MIXED_ID ) && !defined( C_HL_ENABLE_IDTYPE_IN_ID )
      if (CanRxActualIdType( CAN_HL_P_RX_INFO_STRUCT(channel) ) == CANRANGE1IDTYPE(channel))
#    endif
      {
        if ( C_RANGE_MATCH( CAN_RX_IDRAW_PARA, CANRANGE1ACCMASK(channel), CANRANGE1ACCCODE(channel)) )
#   else    /* C_SINGLE_RECEIVE_CHANNEL) */
    {
#    if (C_RANGE1_IDTYPE == kCanIdTypeStd )
#     if defined( C_ENABLE_MIXED_ID ) && !defined( C_HL_ENABLE_IDTYPE_IN_ID )
      if (CanRxActualIdType( CAN_HL_P_RX_INFO_STRUCT(channel) ) == kCanIdTypeStd)
#     endif
      {
        if ( C_RANGE_MATCH_STD( CAN_RX_IDRAW_PARA, CANRANGE1ACCMASK(channel), CANRANGE1ACCCODE(channel)) )
#    else  /* C_RANGE_1_IDTYPE == kCanIdTypeExt */
#     if defined( C_ENABLE_MIXED_ID ) && !defined( C_HL_ENABLE_IDTYPE_IN_ID )
      if (CanRxActualIdType( CAN_HL_P_RX_INFO_STRUCT(channel) ) == kCanIdTypeExt)
#     endif
      {
        if ( C_RANGE_MATCH_EXT( CAN_RX_IDRAW_PARA, CANRANGE1ACCMASK(channel), CANRANGE1ACCCODE(channel)) )
#    endif
#   endif
        {
#   if defined( C_ENABLE_RX_QUEUE_RANGE )
          if (CanRxQueueRange1[channel] == kCanTrue)
          {
            pCanRxInfoStruct->Handle      = kCanRxHandleRange1;
            (void)CanHL_ReceivedRxHandleQueue( CAN_CHANNEL_CANPARA_ONLY );
            goto finishBasicCan;
          }
          else
#   endif  /* C_ENABLE_RX_QUEUE */
          {
            if (APPLCANRANGE1PRECOPY( CAN_HL_P_RX_INFO_STRUCT(channel) ) == kCanNoCopyData)
            {
              goto finishBasicCan;
            }
          }
        }
      }
    }
#  endif  /* C_ENABLE_RANGE_1 */

#  if defined( C_ENABLE_RANGE_2 )
#   if defined( C_MULTIPLE_RECEIVE_CHANNEL )
    if ( (CanChannelObject[channel].RangeActiveFlag & kCanRange2) != (vuint16)0 )
    {
#    if defined( C_ENABLE_MIXED_ID ) && !defined( C_HL_ENABLE_IDTYPE_IN_ID )
      if (CanRxActualIdType( CAN_HL_P_RX_INFO_STRUCT(channel) ) == CANRANGE2IDTYPE(channel))
#    endif
      {
        if ( C_RANGE_MATCH( CAN_RX_IDRAW_PARA, CANRANGE2ACCMASK(channel), CANRANGE2ACCCODE(channel)) )
#   else    /* C_SINGLE_RECEIVE_CHANNEL) */
    {
#    if (C_RANGE2_IDTYPE == kCanIdTypeStd )
#     if defined( C_ENABLE_MIXED_ID ) && !defined( C_HL_ENABLE_IDTYPE_IN_ID )
      if (CanRxActualIdType( CAN_HL_P_RX_INFO_STRUCT(channel) ) == kCanIdTypeStd)
#     endif
      {
        if ( C_RANGE_MATCH_STD( CAN_RX_IDRAW_PARA, CANRANGE2ACCMASK(channel), CANRANGE2ACCCODE(channel)) )
#    else  /* C_RANGE_2_IDTYPE == kCanIdTypeExt */
#     if defined( C_ENABLE_MIXED_ID ) && !defined( C_HL_ENABLE_IDTYPE_IN_ID )
      if (CanRxActualIdType( CAN_HL_P_RX_INFO_STRUCT(channel) ) == kCanIdTypeExt)
#     endif
      {
        if ( C_RANGE_MATCH_EXT( CAN_RX_IDRAW_PARA, CANRANGE2ACCMASK(channel), CANRANGE2ACCCODE(channel)) )
#    endif
#   endif
        {
#   if defined( C_ENABLE_RX_QUEUE_RANGE )
          if (CanRxQueueRange2[channel] == kCanTrue)
          {
            pCanRxInfoStruct->Handle      = kCanRxHandleRange2;
            (void)CanHL_ReceivedRxHandleQueue( CAN_CHANNEL_CANPARA_ONLY );
            goto finishBasicCan;
          }
          else
#   endif  /* C_ENABLE_RX_QUEUE */
          {
            if (APPLCANRANGE2PRECOPY( CAN_HL_P_RX_INFO_STRUCT(channel) ) == kCanNoCopyData)
            {
              goto finishBasicCan;
            }
          }
        }
      }
    }
#  endif  /* C_ENABLE_RANGE_2 */

#  if defined( C_ENABLE_RANGE_3 )
#   if defined( C_MULTIPLE_RECEIVE_CHANNEL )
    if ( (CanChannelObject[channel].RangeActiveFlag & kCanRange3) != (vuint16)0 )
    {
#    if defined( C_ENABLE_MIXED_ID ) && !defined( C_HL_ENABLE_IDTYPE_IN_ID )
      if (CanRxActualIdType( CAN_HL_P_RX_INFO_STRUCT(channel) ) == CANRANGE3IDTYPE(channel))
#    endif
      {
        if ( C_RANGE_MATCH( CAN_RX_IDRAW_PARA, CANRANGE3ACCMASK(channel), CANRANGE3ACCCODE(channel)) )
#   else    /* C_SINGLE_RECEIVE_CHANNEL) */
    {
#    if (C_RANGE3_IDTYPE == kCanIdTypeStd )
#     if defined( C_ENABLE_MIXED_ID ) && !defined( C_HL_ENABLE_IDTYPE_IN_ID )
      if (CanRxActualIdType( CAN_HL_P_RX_INFO_STRUCT(channel) ) == kCanIdTypeStd)
#     endif
      {
        if ( C_RANGE_MATCH_STD( CAN_RX_IDRAW_PARA, CANRANGE3ACCMASK(channel), CANRANGE3ACCCODE(channel)) )
#    else  /* C_RANGE_3_IDTYPE == kCanIdTypeExt */
#     if defined( C_ENABLE_MIXED_ID ) && !defined( C_HL_ENABLE_IDTYPE_IN_ID )
      if (CanRxActualIdType( CAN_HL_P_RX_INFO_STRUCT(channel) ) == kCanIdTypeExt)
#     endif
      {
        if ( C_RANGE_MATCH_EXT( CAN_RX_IDRAW_PARA, CANRANGE3ACCMASK(channel), CANRANGE3ACCCODE(channel)) )
#    endif
#   endif
        {
#   if defined( C_ENABLE_RX_QUEUE_RANGE )
          if (CanRxQueueRange3[channel] == kCanTrue)
          {
            pCanRxInfoStruct->Handle      = kCanRxHandleRange3;
            (void)CanHL_ReceivedRxHandleQueue( CAN_CHANNEL_CANPARA_ONLY );
            goto finishBasicCan;
          }
          else
#   endif  /* C_ENABLE_RX_QUEUE */
          {
            if (APPLCANRANGE3PRECOPY( CAN_HL_P_RX_INFO_STRUCT(channel) ) == kCanNoCopyData)
            {
              goto finishBasicCan;
            }
          }
        }
      }
    }
#  endif  /* C_ENABLE_RANGE_3 */

  }

# endif /* defined( C_HL_ENABLE_HW_RANGES_FILTER ) */

# if defined( C_ENABLE_GENERIC_PRECOPY ) || \
    defined( C_ENABLE_PRECOPY_FCT )     || \
    defined( C_ENABLE_COPY_RX_DATA )    || \
    defined( C_ENABLE_INDICATION_FLAG ) || \
    defined( C_ENABLE_INDICATION_FCT )  || \
    defined( C_ENABLE_DLC_CHECK )       || \
    defined( C_ENABLE_NOT_MATCHED_FCT )
# if (kCanNumberOfRxBasicCANObjects > 0)

   /* search the received id in ROM table: */

  #if defined(C_COMP_COSMIC_MCS12X_MSCAN12)  
  # if defined(C_SEARCH_XGATE)
  rxHandle = canXgRxHandle[channel]; /* get the handle which was found by the XGate */
  # endif
  #endif

#  if defined( C_SEARCH_LINEAR )
  /* ************* Linear search ******************************************** */
  for (rxHandle = CAN_HL_RX_BASIC_STARTINDEX(channel); rxHandle < CAN_HL_RX_BASIC_STOPINDEX(channel) ;rxHandle++)
  {
    if( idRaw0 == CanGetRxId0(rxHandle) )
    {
#   if (kCanNumberOfUsedCanRxIdTables > 1)
      if( idRaw1 == CanGetRxId1(rxHandle) )
#   endif
      {
#   if (kCanNumberOfUsedCanRxIdTables > 2)
        if( idRaw2 == CanGetRxId2(rxHandle) )
#   endif
        {
#   if (kCanNumberOfUsedCanRxIdTables > 3)
          if( idRaw3 == CanGetRxId3(rxHandle) )
#   endif
          {
#   if (kCanNumberOfUsedCanRxIdTables > 4)
            if( idRaw4 == CanGetRxId4(rxHandle) )
#   endif
            {
#   if defined( C_ENABLE_MIXED_ID ) 
#    if defined( C_HL_ENABLE_IDTYPE_IN_ID )
#    else
              /* verify ID type, if not already done with the ID raw */
              if (CanRxActualIdType( CAN_HL_P_RX_INFO_STRUCT(channel) ) == CanGetRxIdType(rxHandle))
#    endif
#   endif
              {
                break;    /*exit loop with index rxHandle */
              }
            }
          }
        }
      }
    }
  }
#  endif

#  if defined( C_SEARCH_HASH )
  /* ************* Hash search ********************************************* */
    
#   if defined( C_ENABLE_EXTENDED_ID ) 
  /* one or more Extended ID listed */
#    if defined( C_ENABLE_MIXED_ID )
  if((CanRxActualIdType( CAN_HL_P_RX_INFO_STRUCT(channel) )) == kCanIdTypeExt)
#    endif
#    if (kHashSearchListCountEx > 0)
  {
  /* calculate the logical ID */
#     if defined( C_ENABLE_RX_MASK_EXT_ID )
    idExt          = (CanRxActualId( CAN_HL_P_RX_INFO_STRUCT(channel) ) &  C_MASK_EXT_ID ) | \
                                                                               ((vuint32)channel << 29);                
#     else
    idExt          = CanRxActualId( CAN_HL_P_RX_INFO_STRUCT(channel) )| ((vuint32)channel << 29);
#     endif

#     if (kHashSearchListCountEx == 1)
    rxHandle       = (CanReceiveHandle)0;
#     else
    winternExt     = idExt + kHashSearchRandomNumberEx;                    
    rxHandle       = (CanReceiveHandle)(winternExt % kHashSearchListCountEx);
#     endif /* (kHashSearchListCountEx == 1) */

#     if ((kHashSearchListCountEx == 1) || (kHashSearchMaxStepsEx == 1))
    if (idExt != CanRxHashIdEx[rxHandle])   
    { 
#      if defined( C_ENABLE_NOT_MATCHED_FCT )
      ApplCanMsgNotMatched( CAN_HL_P_RX_INFO_STRUCT(channel) );
#      endif
      goto finishBasicCan;
    }
#     else /* (kHashSearchListCountEx == 1) || (kHashSearchMaxStepsEx == 1) */

    i_increment = (vuint16)((winternExt % (kHashSearchListCountEx - 1)) + (vuint8)1);          /* ST10-CCAN Keil compiler complains without cast */
    count       = (vsint16)kHashSearchMaxStepsEx - 1;               
  
    while(idExt != CanRxHashIdEx[rxHandle])   
    {
      if(count == (vsint16)0)  
      {
#      if defined( C_ENABLE_NOT_MATCHED_FCT )
        ApplCanMsgNotMatched( CAN_HL_P_RX_INFO_STRUCT(channel) );
#      endif
        goto finishBasicCan;
      }
      count--;
      rxHandle += i_increment;
      if( rxHandle >= (vuint16)kHashSearchListCountEx ) 
      {
        rxHandle -= kHashSearchListCountEx;
      }
    }
#     endif  /* (kHashSearchListCountEx == 1) || (kHashSearchMaxStepsEx == 1) */
    rxHandle = CanRxMsgIndirection[rxHandle+kHashSearchListCount];
  }
#    else /* (kHashSearchListCountEx > 0) */
  {
#     if defined( C_ENABLE_NOT_MATCHED_FCT )
    ApplCanMsgNotMatched( CAN_HL_P_RX_INFO_STRUCT(channel) );
#     endif
    goto finishBasicCan;
  }
#    endif /* (kHashSearchListCountEx > 0) */

#    if defined( C_ENABLE_MIXED_ID )
  else   /* if((CanRxActualIdType( CAN_HL_P_RX_INFO_STRUCT(channel) )) == kCanIdTypeStd)  */
#    endif
#   endif /* If defined( C_ENABLE_EXTENDED_ID ) */

#   if defined( C_ENABLE_MIXED_ID ) || !defined( C_ENABLE_EXTENDED_ID ) 
#    if (kHashSearchListCount > 0)
  {
    idStd          = ((vuint16)CanRxActualId( CAN_HL_P_RX_INFO_STRUCT(channel) ) | ((vuint16)channel << 11));    /* calculate the logical ID */

#     if (kHashSearchListCount == 1)
    rxHandle       = (CanReceiveHandle)0;
#     else
    winternStd     = idStd + kHashSearchRandomNumber;
    rxHandle       = (CanReceiveHandle)(winternStd % kHashSearchListCount);
#     endif /* (kHashSearchListCount == 1) */

#     if ((kHashSearchListCount == 1)  || (kHashSearchMaxSteps == 1))
    if (idStd != CanRxHashId[rxHandle])
    {
#      if defined( C_ENABLE_NOT_MATCHED_FCT )
      ApplCanMsgNotMatched( CAN_HL_P_RX_INFO_STRUCT(channel) );
#      endif
      goto finishBasicCan;
    }
#     else /* ((kHashSearchListCount == 1)  || (kHashSearchMaxSteps == 1)) */

    i_increment = (vuint16)((winternStd % (kHashSearchListCount - 1)) + (vuint8)1);
    count       = (vsint16)kHashSearchMaxSteps-1;

    /* type of CanRxHashId table depends on the used type of Id */
    while ( idStd != CanRxHashId[rxHandle])
    {
      if (count == (vsint16)0)
      {
#     if defined( C_ENABLE_NOT_MATCHED_FCT )
        ApplCanMsgNotMatched( CAN_HL_P_RX_INFO_STRUCT(channel) );
#     endif
        goto finishBasicCan;
      }
      count--; 
      rxHandle += i_increment;
      if ( rxHandle >= kHashSearchListCount )
      {
        rxHandle -= kHashSearchListCount;
      }
    }
#     endif /* ((kHashSearchListCount == 1)  || (kHashSearchMaxSteps == 1)) */
    rxHandle = CanRxMsgIndirection[rxHandle];
  }
#    else /* (kHashSearchListCount > 0) */
  {
#     if defined( C_ENABLE_NOT_MATCHED_FCT )
    ApplCanMsgNotMatched( CAN_HL_P_RX_INFO_STRUCT(channel) );
#     endif
    goto finishBasicCan;
  }
#    endif /* (kHashSearchListCount > 0) */
#   endif /* defined( C_ENABLE_MIXED_ID ) || !defined( C_ENABLE_EXTENDED_ID ) */ 
#  endif /* defined( C_SEARCH_HASH ) */



  /**************************** handle filtered message ****************************************************/
#  if defined( C_SEARCH_HASH )
  assertInternal((rxHandle < kCanNumberOfRxObjects), kCanAllChannels , kErrorRxHandleWrong);  /* PRQA S 3109 */ /* legal rxHandle ? */
#  else
  if ( rxHandle < CAN_HL_RX_BASIC_STOPINDEX(channel)) 
#  endif
  {
    /* ID found in table */
#  if defined( C_SEARCH_HASH ) 
#  else
#   if defined( C_ENABLE_RX_MSG_INDIRECTION )
    rxHandle = CanRxMsgIndirection[rxHandle];       /* indirection for special sort-algoritms */
#   endif
#  endif

    CAN_HL_P_RX_INFO_STRUCT_HANDLE(channel) = rxHandle;  

#  if defined( C_ENABLE_RX_QUEUE )
    if (CanHL_ReceivedRxHandleQueue( CAN_CHANNEL_CANPARA_ONLY ) == kCanHlContinueRx)
#  else
    if (CanHL_ReceivedRxHandle( CAN_CHANNEL_CANPARA_ONLY ) == kCanHlContinueRx)
#  endif
    { 
#  if defined( C_ENABLE_INDICATION_FLAG ) || \
          defined( C_ENABLE_INDICATION_FCT )
      #if defined(C_COMP_COSMIC_MCS12X_MSCAN12)  
      # if defined(C_ENABLE_XGATE_USED)
      #   if defined(C_SINGLE_RECEIVE_CHANNEL)
      index = (kCanBasisAdr >> 6) & 0x07;
      #   else
      index = (CanBasisAdr[channel] >> 6) & 0x07;
      #   endif
      nInfoIndex = CanXGateChannelToInfo[index];
      *CanXGateIrqChannelInfo[nInfoIndex].pXGif = CanXGateIrqChannelInfo[nInfoIndex].nRxMask;
      CanBeginCriticalXGateSection();
      canRxSyncObj[channel] = 0; 
      CanEndCriticalXGateSection();
      # else
      CRFLG = RXF; /* clear receive pending flag */
      # endif
      #endif
      

      CanHL_IndRxHandle( rxHandle );
      

      return;
#  endif
    }
  }
#  if defined( C_ENABLE_NOT_MATCHED_FCT )
#   if defined( C_SEARCH_HASH )
#   else
  else
  {
    ApplCanMsgNotMatched( CAN_HL_P_RX_INFO_STRUCT(channel) );
  }
#   endif
#  endif
# else  /* kCanNumberOfRxBasicCANObjects > 0 */
#  if defined( C_ENABLE_NOT_MATCHED_FCT )
  ApplCanMsgNotMatched( CAN_HL_P_RX_INFO_STRUCT(channel) );
#  endif
# endif /* kCanNumberOfRxBasicCANObjects > 0 */

# endif

  goto finishBasicCan;     /* to avoid compiler warning */

/* Msg(4:2015) This label is not a case or default label for a switch statement. MISRA Rule 55 */
finishBasicCan:

  /* make receive buffer free*/
  #if defined(C_COMP_COSMIC_MCS12X_MSCAN12)  
  # if defined(C_ENABLE_XGATE_USED)
  #   if defined(C_SINGLE_RECEIVE_CHANNEL)
  index = (kCanBasisAdr >> 6) & 0x07;
  #   else
  index = (CanBasisAdr[channel] >> 6) & 0x07;
  #   endif
  nInfoIndex = CanXGateChannelToInfo[index];
  *CanXGateIrqChannelInfo[nInfoIndex].pXGif = CanXGateIrqChannelInfo[nInfoIndex].nRxMask;
  CanBeginCriticalXGateSection();
  canRxSyncObj[channel] = 0; 
  CanEndCriticalXGateSection();
  # else
  CRFLG = RXF; /* clear receive pending flag */
  # endif
  #endif
  


  return;    /* to avoid compiler warnings about label without code */

} /* end of BasicCan */
/* CODE CATEGORY 1 END*/
/* PRQA S 2001 -- */    /* suppress misra message about multiple return and usage of goto */
#endif


#if defined( C_ENABLE_RX_BASICCAN_OBJECTS )
# if ( kCanNumberOfRxObjects > 0 )
/****************************************************************************
| NAME:             CanHL_ReceivedRxHandle
| CALLED BY:        CanBasicCanMsgReceived, CanFullCanMsgReceived
| PRECONDITIONS:
| INPUT PARAMETERS: Handle of received Message to access generated data
| RETURN VALUES:    none
| DESCRIPTION:      DLC-check, Precopy and copy of Data for received message
****************************************************************************/
/* CODE CATEGORY 1 START*/
#  if defined( C_ENABLE_RX_QUEUE )
static vuint8 CanHL_ReceivedRxHandle( CAN_CHANNEL_CANTYPE_FIRST tCanRxInfoStruct *pCanRxInfoStruct )
{
#  else
static vuint8 CanHL_ReceivedRxHandle( CAN_CHANNEL_CANTYPE_ONLY )
{
#  endif
#  if !defined( C_ENABLE_RX_QUEUE ) &&\
    defined( C_HL_ENABLE_RX_INFO_STRUCT_PTR )
  tCanRxInfoStruct    *pCanRxInfoStruct;
#  endif

#  if defined( C_ENABLE_COPY_RX_DATA )
#   if C_SECURITY_LEVEL > 20
  CanDeclareGlobalInterruptOldStatus
#   endif
#  endif

# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertInternal(channel < kCanNumberOfChannels, kCanAllChannels, kErrorInternalChannelHdlTooLarge);  /* PRQA S 3109 */
# endif

#  if !defined( C_ENABLE_RX_QUEUE ) &&\
    defined( C_HL_ENABLE_RX_INFO_STRUCT_PTR )
  pCanRxInfoStruct =  &canRxInfoStruct[channel];
#  endif

#  if defined( C_ENABLE_MULTI_ECU_PHYS )
  if ( (CanRxIdentityAssignment[CAN_HL_P_RX_INFO_STRUCT_HANDLE(channel)] & V_ACTIVE_IDENTITY_MSK) == (tVIdentityMsk)0 )
  {
    /* message is not a receive message in the active indentity */
    CANDRV_SET_CODE_TEST_POINT(0x10B);
    return  kCanHlFinishRx;
  }
#  endif


#  if defined( C_ENABLE_DLC_CHECK )  
#   if defined( C_ENABLE_DLC_CHECK_MIN_DATALEN )
  if ( (CanGetRxMinDataLen(CAN_HL_P_RX_INFO_STRUCT_HANDLE(channel))) > CanRxActualDLC( CAN_HL_P_RX_INFO_STRUCT(channel) ) )
#   else
  if ( (CanGetRxDataLen(CAN_HL_P_RX_INFO_STRUCT_HANDLE(channel))) > CanRxActualDLC( CAN_HL_P_RX_INFO_STRUCT(channel) ) )
#   endif
  {
    /* ##RI1.4 - 2.7: Callbackfunction-DLC-Check */
#   if defined( C_ENABLE_DLC_FAILED_FCT )
    ApplCanMsgDlcFailed( CAN_HL_P_RX_INFO_STRUCT(channel) ); 
#   endif  /*C_ENABLE_DLC_FAILED_FCT */
    return  kCanHlFinishRx;
  }
#  endif

#  if defined( C_ENABLE_VARIABLE_RX_DATALEN )
  CanSetVariableRxDatalen (CAN_HL_P_RX_INFO_STRUCT_HANDLE(channel), CanRxActualDLC( CAN_HL_P_RX_INFO_STRUCT(channel)));
#  endif

#  if defined( C_ENABLE_GENERIC_PRECOPY )
  if ( APPL_CAN_GENERIC_PRECOPY( CAN_HL_P_RX_INFO_STRUCT(channel) ) != kCanCopyData)
  {
    return kCanHlFinishRx;  
  }
#  endif

#  if defined( C_ENABLE_PRECOPY_FCT )
#   if defined( C_HL_ENABLE_DUMMY_FCT_CALL )
#   else
  if ( CanGetApplPrecopyPtr(CAN_HL_P_RX_INFO_STRUCT_HANDLE(channel)) != V_NULL )    /*precopy routine */
#   endif
  {
    /* canRxHandle in indexed drivers only for consistancy check in higher layer modules */
    canRxHandle[channel] = CAN_HL_P_RX_INFO_STRUCT_HANDLE(channel);
    
    if ( CanGetApplPrecopyPtr(CAN_HL_P_RX_INFO_STRUCT_HANDLE(channel))( CAN_HL_P_RX_INFO_STRUCT(channel) )==kCanNoCopyData )
    {  /* precopy routine returns kCanNoCopyData:   */
      return  kCanHlFinishRx;
    }                      /* do not copy data check next irpt */
  }
#  endif

#  if defined( C_ENABLE_COPY_RX_DATA )
  /* no precopy or precopy returns kCanCopyData : copy data -- */
  /* copy via index -------------------------------------------*/
  if ( CanGetRxDataPtr(CAN_HL_P_RX_INFO_STRUCT_HANDLE(channel)) != V_NULL )      /* copy if buffer exists */
  {
    /* copy data ---------------------------------------------*/
#   if C_SECURITY_LEVEL > 20
    CanNestedGlobalInterruptDisable();
#   endif
    CANDRV_SET_CODE_TEST_POINT(0x107);
#   if defined( C_ENABLE_COPY_RX_DATA_WITH_DLC )
    if ( CanRxActualDLC( CAN_HL_P_RX_INFO_STRUCT(channel)) < CanGetRxDataLen(CAN_HL_P_RX_INFO_STRUCT_HANDLE(channel)) )
    {
#    if defined( C_HL_ENABLE_RX_INFO_STRUCT_PTR )    
      #if defined(C_COMP_COSMIC_MCS12X_MSCAN12)  
      # if defined(V_ENABLE_USED_GLOBAL_VAR)
      VStdMemCpyRamToFarRam(CanGetRxDataPtr(CAN_HL_P_RX_INFO_STRUCT_HANDLE(channel)), pCanRxInfoStruct->pChipData, CanRxActualDLC(CAN_HL_P_RX_INFO_STRUCT(channel)));
      # else
      VStdMemCpyRamToRam(CanGetRxDataPtr(CAN_HL_P_RX_INFO_STRUCT_HANDLE(channel)), pCanRxInfoStruct->pChipData, CanRxActualDLC(CAN_HL_P_RX_INFO_STRUCT(channel)));
      # endif
      #endif
#    else
      #if defined(C_COMP_COSMIC_MCS12X_MSCAN12)  
      # if defined(V_ENABLE_USED_GLOBAL_VAR)
      VStdMemCpyRamToFarRam(CanGetRxDataPtr(CAN_HL_P_RX_INFO_STRUCT_HANDLE(channel)), canRxInfoStruct[channel].pChipData, CanRxActualDLC(CAN_HL_P_RX_INFO_STRUCT(channel)));
      # else
      VStdMemCpyRamToRam(CanGetRxDataPtr(CAN_HL_P_RX_INFO_STRUCT_HANDLE(channel)), canRxInfoStruct[channel].pChipData, CanRxActualDLC(CAN_HL_P_RX_INFO_STRUCT(channel)));
      # endif
      #endif
#    endif 
    }
    else
#   endif   /* C_ENABLE_COPY_RX_DATA_WITH_DLC */
    {
#   if defined( C_HL_ENABLE_RX_INFO_STRUCT_PTR )    
      #if defined(C_COMP_COSMIC_MCS12X_MSCAN12)  
      # if defined(V_ENABLE_USED_GLOBAL_VAR)
      VStdMemCpyRamToFarRam(CanGetRxDataPtr(CAN_HL_P_RX_INFO_STRUCT_HANDLE(channel)), pCanRxInfoStruct->pChipData, CanGetRxDataLen(CAN_HL_P_RX_INFO_STRUCT_HANDLE(channel)));
      # else
      VStdMemCpyRamToRam(CanGetRxDataPtr(CAN_HL_P_RX_INFO_STRUCT_HANDLE(channel)), pCanRxInfoStruct->pChipData, CanGetRxDataLen(CAN_HL_P_RX_INFO_STRUCT_HANDLE(channel)));
      # endif
      #endif
#   else
      #if defined(C_COMP_COSMIC_MCS12X_MSCAN12)  
      # if defined(V_ENABLE_USED_GLOBAL_VAR)
      VStdMemCpyRamToFarRam(CanGetRxDataPtr(CAN_HL_P_RX_INFO_STRUCT_HANDLE(channel)), canRxInfoStruct[channel].pChipData, CanGetRxDataLen(CAN_HL_P_RX_INFO_STRUCT_HANDLE(channel)));
      # else
      VStdMemCpyRamToRam(CanGetRxDataPtr(CAN_HL_P_RX_INFO_STRUCT_HANDLE(channel)), canRxInfoStruct[channel].pChipData, CanGetRxDataLen(CAN_HL_P_RX_INFO_STRUCT_HANDLE(channel)));
      # endif
      #endif
#   endif 
    }
#   if C_SECURITY_LEVEL > 20
    CanNestedGlobalInterruptRestore();
#   endif
  }
#  endif /* ( C_ENABLE_COPY_RX_DATA ) */

  CANDRV_SET_CODE_TEST_POINT(0x105);
  return kCanHlContinueRx;
} /* end of CanReceivceRxHandle() */
/* CODE CATEGORY 1 END*/

#  if defined( C_ENABLE_INDICATION_FLAG ) || \
     defined( C_ENABLE_INDICATION_FCT )
/****************************************************************************
| NAME:             CanHL_IndRxHandle
| CALLED BY:        CanBasicCanMsgReceived, CanFullCanMsgReceived
| PRECONDITIONS:
| INPUT PARAMETERS: Handle of received Message to access generated data
| RETURN VALUES:    none
| DESCRIPTION:      DLC-check, Precopy and copy of Data for received message
****************************************************************************/
/* CODE CATEGORY 1 START*/
static void CanHL_IndRxHandle( CanReceiveHandle rxHandle )
{
#   if defined( C_ENABLE_INDICATION_FLAG )
#    if C_SECURITY_LEVEL > 20
  CanDeclareGlobalInterruptOldStatus
#    endif
#   endif

#   if defined( C_ENABLE_INDICATION_FLAG )
#    if C_SECURITY_LEVEL > 20
  CanNestedGlobalInterruptDisable();
#    endif
  CanIndicationFlags._c[CanGetIndicationOffset(rxHandle)] |= CanGetIndicationMask(rxHandle);
#    if C_SECURITY_LEVEL > 20
  CanNestedGlobalInterruptRestore();
#    endif
#   endif

#   if defined( C_ENABLE_INDICATION_FCT )
#    if defined( C_HL_ENABLE_DUMMY_FCT_CALL )
#    else
  if ( CanGetApplIndicationPtr(rxHandle) != V_NULL )
#    endif
  {
    CanGetApplIndicationPtr(rxHandle)(rxHandle);  /* call IndicationRoutine */
  }
#   endif
}
/* CODE CATEGORY 1 END*/
#  endif /* C_ENABLE_INDICATION_FLAG || C_ENABLE_INDICATION_FCT  */
# endif /* ( kCanNumberOfRxObjects > 0 ) */
#endif


/****************************************************************************
| NAME:             CanHL_TxConfirmation
| CALLED BY:        CanISR()
| PRECONDITIONS:
| INPUT PARAMETERS: - internal can chip number
|                   - interrupt ID
| RETURN VALUES:    none
| DESCRIPTION:      - full can transmit
****************************************************************************/
/* PRQA S 2001 ++ */    /* suppress misra message about usage of goto */
/* CODE CATEGORY 1 START*/
static void CanHL_TxConfirmation( CAN_HW_CHANNEL_CANTYPE_FIRST CanObjectHandle txObjHandle)
{
  CanObjectHandle       logTxObjHandle;
  CanTransmitHandle     txHandle;

 
#if defined( C_ENABLE_TRANSMIT_QUEUE )
  CanSignedTxHandle         queueElementIdx;    /* use as signed due to loop */
  CanSignedTxHandle         elementBitIdx;
  tCanQueueElementType             elem;
  CanDeclareGlobalInterruptOldStatus
#else
# if defined( C_ENABLE_CONFIRMATION_FLAG )
#  if C_SECURITY_LEVEL > 20
  CanDeclareGlobalInterruptOldStatus
#  endif
# endif
#endif


#if defined( C_ENABLE_CAN_CANCEL_NOTIFICATION ) && \
    defined( C_ENABLE_TRANSMIT_QUEUE )
  vuint8 rc;
#endif



# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertInternal(canHwChannel < kCanNumberOfHwChannels, kCanAllChannels, kErrorInternalChannelHdlTooLarge);  /* PRQA S 3109 */
# endif



  logTxObjHandle = (CanObjectHandle)((vsintx)txObjHandle + CAN_HL_TX_OFFSET_HW_TO_LOG(canHwChannel));

  #if !defined(C_ENABLE_TX_POLLING)
  if((vuint8)0xFF == txObjHandle)
  {
    goto finishCanHL_TxConfirmation;
  }
  #endif
  
  #if !defined(C_ENABLE_TX_POLLING) || (defined(C_ENABLE_TX_POLLING) && defined(C_ENABLE_INDIVIDUAL_POLLING))
  # if defined(C_COMP_COSMIC_MCS12X_MSCAN12)  
  {
    CTIER &= (vuint8)~CanMailboxSelect[txObjHandle]; /* deactivate Tx interrupt, might redundant in case of individual polling but is faster without a check */
  }
  
  
  # endif
  
  
  #else
  # if defined(C_COMP_COSMIC_MCS12X_MSCAN12)  
  canllTxStatusFlag[channel] &= (vuint8)~CanMailboxSelect[txObjHandle];
  # endif
  #endif
  
  # if defined(C_COMP_COSMIC_MCS12X_MSCAN12)  
  CTBSEL = CanMailboxSelect[txObjHandle]; /* ESCAN00023651, make the mailbox visible that transmitted */
  # endif

  txHandle = canHandleCurTxObj[logTxObjHandle];           /* get saved handle */

  /* check associated transmit handle */
  if (txHandle == kCanBufferFree)
  {
    assertInternal (0, channel, kErrorTxHandleWrong);          /* PRQA S 3109 */ /*lint !e506 !e774*/
    goto finishCanHL_TxConfirmation;
  }  

#if defined( C_ENABLE_TX_OBSERVE ) || \
    defined( C_ENABLE_CAN_TX_CONF_FCT )
# if defined( C_ENABLE_CANCEL_IN_HW )
  # if defined(C_COMP_COSMIC_MCS12X_MSCAN12)  
  /* only if there is not HW cancellation acknowledgement */
  if((CTAAK & CanMailboxSelect[txObjHandle]) == 0)
  # endif
  
# endif
  {
# if defined( C_ENABLE_TX_OBSERVE )
    {
      ApplCanTxObjConfirmed( CAN_CHANNEL_CANPARA_FIRST logTxObjHandle );
    }
# endif

# if defined( C_ENABLE_CAN_TX_CONF_FCT )
/* ##RI-1.10 Common Callbackfunction in TxInterrupt */
    txInfoStructConf[channel].Handle  = txHandle;
    /* in case of passive mode we do not need to modifiy the mailbox */
    APPL_CAN_TX_CONFIRMATION(&txInfoStructConf[channel]);
# endif
  }
#endif /* defined( C_ENABLE_TX_OBSERVE ) || defined( C_ENABLE_CAN_TX_CONF_FCT ) */

#if defined( C_ENABLE_TRANSMIT_QUEUE )
# if defined( C_ENABLE_MSG_TRANSMIT )
  if (txObjHandle != CAN_HL_HW_TX_NORMAL_INDEX(canHwChannel))
  {
    canHandleCurTxObj[logTxObjHandle] = kCanBufferFree;                 /* free msg object of FullCAN or LowLevel Tx obj. */
  }
# endif
#else
  canHandleCurTxObj[logTxObjHandle] = kCanBufferFree;                   /* free msg object if queue is not used */
#endif

  if (txHandle != kCanBufferCancel)
  {
#if defined( C_ENABLE_MSG_TRANSMIT )
    if (txObjHandle == CAN_HL_HW_MSG_TRANSMIT_INDEX(canHwChannel))
    {
# if defined( C_ENABLE_MSG_TRANSMIT_CONF_FCT )
      APPL_CAN_MSGTRANSMITCONF( CAN_CHANNEL_CANPARA_ONLY );
# endif

      goto finishCanHL_TxConfirmation;
    }
#endif

#if defined( C_ENABLE_MULTI_ECU_PHYS )
    assertInternal(((CanTxIdentityAssignment[txHandle] & V_ACTIVE_IDENTITY_MSK) != (tVIdentityMsk)0 ), 
                                                                channel , kErrorInternalDisabledTxMessage);  /* PRQA S 3109 */
#endif

#if defined( C_ENABLE_CONFIRMATION_FLAG )       /* set transmit ready flag  */
# if C_SECURITY_LEVEL > 20
    CanNestedGlobalInterruptDisable();
# endif
    CanConfirmationFlags._c[CanGetConfirmationOffset(txHandle)] |= CanGetConfirmationMask(txHandle);
# if C_SECURITY_LEVEL > 20
    CanNestedGlobalInterruptRestore();
# endif
#endif

#if defined( C_ENABLE_CONFIRMATION_FCT )
    {
# if defined( C_HL_ENABLE_DUMMY_FCT_CALL )
# else
      if ( CanGetApplConfirmationPtr(txHandle) != V_NULL )
# endif
      {
        (CanGetApplConfirmationPtr(txHandle))(txHandle);   /* call completion routine  */
      }
    }
#endif /* C_ENABLE_CONFIRMATION_FCT */

  } /* end if kCanBufferCancel */
  
#if defined( C_ENABLE_TRANSMIT_QUEUE )
# if defined( C_ENABLE_MSG_TRANSMIT )
  if (txObjHandle == CAN_HL_HW_TX_NORMAL_INDEX(canHwChannel)) 
# endif
  {
    CanNestedGlobalInterruptDisable();                /* ESCAN00008914 */

    for(queueElementIdx = CAN_HL_TXQUEUE_STOPINDEX(channel) - (CanSignedTxHandle)1; 
                             queueElementIdx >= CAN_HL_TXQUEUE_STARTINDEX(channel); queueElementIdx--)
    {
      elem = canTxQueueFlags[queueElementIdx];
      if(elem != (tCanQueueElementType)0) /* is there any flag set in the queue element? */
      {

        CanNestedGlobalInterruptRestore();

        /* start the bitsearch */
        {
        #if defined( C_CPUTYPE_16BIT )
          if((elem & (tCanQueueElementType)0x0000FF00) != (tCanQueueElementType)0)
          {
            if((elem & (tCanQueueElementType)0x0000F000) != (tCanQueueElementType)0)
            {
              elementBitIdx = (CanSignedTxHandle)CanGetHighestFlagFromNibble[elem >> 12] + 12;
            }
            else /*0x00000F00*/
            {
              elementBitIdx = (CanSignedTxHandle)CanGetHighestFlagFromNibble[elem >> 8] + 8;
            }
          }
          else
        #endif
          {
            if((elem & (tCanQueueElementType)0x000000F0) != (tCanQueueElementType)0)
            {
              elementBitIdx = (CanSignedTxHandle)CanGetHighestFlagFromNibble[elem >> 4] + 4;
            }
            else /* 0x0000000F*/
            {
              elementBitIdx = (CanSignedTxHandle)CanGetHighestFlagFromNibble[elem];
            }
          }
        }
        txHandle = (((CanTransmitHandle)queueElementIdx << kCanTxQueueShift) + (CanTransmitHandle)elementBitIdx) - CAN_HL_TXQUEUE_PADBITS(channel);
        {

            /* compute the logical message handle */
            CanNestedGlobalInterruptDisable();
 
            if ( (canTxQueueFlags[queueElementIdx] & CanShiftLookUp[elementBitIdx]) != (tCanQueueElementType)0 )
            {
              canTxQueueFlags[queueElementIdx] &= ~CanShiftLookUp[elementBitIdx]; /* clear flag from the queue */

              CanNestedGlobalInterruptRestore();  
              canHandleCurTxObj[logTxObjHandle] = txHandle;/* Save hdl of msgObj to be transmitted*/
# if defined( C_ENABLE_CAN_CANCEL_NOTIFICATION )
              rc = CanCopyDataAndStartTransmission( CAN_CHANNEL_CANPARA_FIRST txObjHandle,txHandle); 
              if ( rc == kCanTxNotify)
              {
                APPLCANCANCELNOTIFICATION(channel, txHandle);
              }
# else       /* C_ENABLE_CAN_CANCEL_NOTIFICATION */
              (void)CanCopyDataAndStartTransmission( CAN_CHANNEL_CANPARA_FIRST txObjHandle,txHandle); 
# endif /* C_ENABLE_CAN_CANCEL_NOTIFICATION */

              goto finishCanHL_TxConfirmation;
            }
            /* meanwhile, the queue is empty. E.g. due to CanOffline on higher level */
            canHandleCurTxObj[logTxObjHandle] = kCanBufferFree;  /* free msg object if queue is empty */
            CanNestedGlobalInterruptRestore();  
            goto finishCanHL_TxConfirmation;
            
        }
        /* no entry found in Queue */
# if defined( CANHL_TX_QUEUE_BIT )
# else
        CanNestedGlobalInterruptDisable();                /*lint !e527 ESCAN00008914 */
                                                 /* unreachable in case of Bit-Queue */
# endif                                                 
      }
    }

    canHandleCurTxObj[logTxObjHandle] = kCanBufferFree;  /* free msg object if queue is empty */
    CanNestedGlobalInterruptRestore();                 /* ESCAN00008914 */
  }
#endif
  /* check for next msg object in queue and transmit it */

/* Msg(4:2015) This label is not a case or default label for a switch statement. MISRA Rule 55 */
finishCanHL_TxConfirmation:

  # if defined(C_COMP_COSMIC_MCS12X_MSCAN12)  
  CTBSEL = (vuint8)((vuint8)~CTBSEL & kCanPendingTxObjMask); /* ESCAN00023651, make visible the mailbox that was probably changed (if it was not changed, this is not a risk) */
  # endif

  return;

} /* END OF CanTxInterrupt */
/* CODE CATEGORY 1 END*/
/* PRQA S 2001 -- */    /* suppress misra message about usage of goto */


#if defined( C_ENABLE_ECU_SWITCH_PASS )
/************************************************************************
* NAME:               CanSetActive
* CALLED BY:          application
* PRECONDITIONS:      none
* PARAMETER:          none or channel
* RETURN VALUE:       none
* DESCRIPTION:        Set the CAN driver into active mode
*************************************************************************/
/* CODE CATEGORY 4 START*/
C_API_1 void C_API_2 CanSetActive( CAN_CHANNEL_CANTYPE_ONLY )
{
#if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser(channel < kCanNumberOfChannels, kCanAllChannels, kErrorChannelHdlTooLarge);     /* PRQA S 3109 */
#endif

  canPassive[channel] = 0;
} /* END OF CanSetActive */
/* CODE CATEGORY 4 END*/

/************************************************************************
* NAME:               CanSetPassive
* CALLED BY:          application
* PRECONDITIONS:      none
* PARAMETER:          none or channel
* RETURN VALUE:       none
* DESCRIPTION:        Set the can driver into passive mode
*************************************************************************/
/* CODE CATEGORY 4 START*/
C_API_1 void C_API_2 CanSetPassive( CAN_CHANNEL_CANTYPE_ONLY )
{
#if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser(channel < kCanNumberOfChannels, kCanAllChannels, kErrorChannelHdlTooLarge);      /* PRQA S 3109 */
#endif

  canPassive[channel] = 1;
 
# if defined( C_ENABLE_TRANSMIT_QUEUE )
  /* clear all Tx queue flags: */
  CanDelQueuedObj( CAN_CHANNEL_CANPARA_ONLY ); 
# endif
 

} /* END OF CanSetPassive */
/* CODE CATEGORY 4 END*/
#endif /* IF defined( C_ENABLE_ECU_SWITCH_PASS ) */


#if defined( C_ENABLE_OFFLINE )
/************************************************************************
* NAME:               CanOnline( CanChannelHandle channel )
* CALLED BY:          netmanagement
* PRECONDITIONS:      none
* PARAMETER:          none or channel
* RETURN VALUE:       none
* DESCRIPTION:        Switch on transmit path
*************************************************************************/
/* CODE CATEGORY 4 START*/
C_API_1 void C_API_2 CanOnline(CAN_CHANNEL_CANTYPE_ONLY)
{
  CanDeclareGlobalInterruptOldStatus

#if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser(channel < kCanNumberOfChannels, kCanAllChannels, kErrorChannelHdlTooLarge);    /* PRQA S 3109 */
#endif

  CanNestedGlobalInterruptDisable();

  canStatus[channel] |= kCanTxOn;

# if defined( C_ENABLE_ONLINE_OFFLINE_CALLBACK_FCT )
  APPL_CAN_ONLINE(CAN_CHANNEL_CANPARA_ONLY);
# endif
  CanNestedGlobalInterruptRestore();

}
/* CODE CATEGORY 4 END*/


/************************************************************************
* NAME:               CanOffline( CanChannelHandle channel )
* CALLED BY:          netmanagement
* PRECONDITIONS:      none
* PARAMETER:          none or channel
* RETURN VALUE:       none
* DESCRIPTION:        Switch off transmit path
*************************************************************************/
/* CODE CATEGORY 4 START*/
C_API_1 void C_API_2 CanOffline(CAN_CHANNEL_CANTYPE_ONLY)
{
  CanDeclareGlobalInterruptOldStatus

#if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser(channel < kCanNumberOfChannels, kCanAllChannels, kErrorChannelHdlTooLarge);     /* PRQA S 3109 */
#endif

  CanNestedGlobalInterruptDisable();

  canStatus[channel] &= kCanTxNotOn;

# if defined( C_ENABLE_ONLINE_OFFLINE_CALLBACK_FCT )
  APPL_CAN_OFFLINE(CAN_CHANNEL_CANPARA_ONLY);
# endif
  CanNestedGlobalInterruptRestore();

# if defined( C_ENABLE_TRANSMIT_QUEUE )
  CanDelQueuedObj( CAN_CHANNEL_CANPARA_ONLY );
# endif

}
/* CODE CATEGORY 4 END*/
#endif /* if defined( C_ENABLE_OFFLINE ) */


#if defined( C_ENABLE_PART_OFFLINE )
/************************************************************************
* NAME:               CanSetPartOffline
* CALLED BY:          application
* PRECONDITIONS:      none
* PARAMETER:          (channel), sendGroup
* RETURN VALUE:       none
* DESCRIPTION:        Switch partial off transmit path
*************************************************************************/
/* CODE CATEGORY 4 START*/
C_API_1 void C_API_2 CanSetPartOffline(CAN_CHANNEL_CANTYPE_FIRST vuint8 sendGroup)
{
  CanDeclareGlobalInterruptOldStatus

#if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser(channel < kCanNumberOfChannels, kCanAllChannels, kErrorChannelHdlTooLarge);    /* PRQA S 3109 */
#endif

  CanNestedGlobalInterruptDisable();
  canTxPartOffline[channel] |= sendGroup;   /* set offlinestate and Save for use of CanOnline/CanOffline */
  CanNestedGlobalInterruptRestore();
}
/* CODE CATEGORY 4 END*/


/************************************************************************
* NAME:               CanSetPartOnline
* CALLED BY:          application
* PRECONDITIONS:      none
* PARAMETER:          (channel), invSendGroup
* RETURN VALUE:       none
* DESCRIPTION:        Switch partial on transmit path
*************************************************************************/
/* CODE CATEGORY 4 START*/
C_API_1 void C_API_2 CanSetPartOnline(CAN_CHANNEL_CANTYPE_FIRST vuint8 invSendGroup)
{
  CanDeclareGlobalInterruptOldStatus

#if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser(channel < kCanNumberOfChannels, kCanAllChannels, kErrorChannelHdlTooLarge);    /* PRQA S 3109 */
#endif

  CanNestedGlobalInterruptDisable();
  canTxPartOffline[channel] &= invSendGroup;
  CanNestedGlobalInterruptRestore();
}
/* CODE CATEGORY 4 END*/


/************************************************************************
* NAME:               CanGetPartMode
* CALLED BY:          application
* PRECONDITIONS:      none
* PARAMETER:          none or channel
* RETURN VALUE:       canTxPartOffline
* DESCRIPTION:        return status of partoffline-Mode
*************************************************************************/
/* CODE CATEGORY 4 START*/
C_API_1 vuint8 C_API_2 CanGetPartMode(CAN_CHANNEL_CANTYPE_ONLY)
{
#if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser(channel < kCanNumberOfChannels, kCanAllChannels, kErrorChannelHdlTooLarge);   /* PRQA S 3109 */
#endif

  return canTxPartOffline[channel];
}
/* CODE CATEGORY 4 END*/
#endif


/****************************************************************************
| NAME:             CanGetStatus
| CALLED BY:
| PRECONDITIONS:
| INPUT PARAMETERS: none or channel
| RETURN VALUES:    Bit coded status (more than one status can be set):
|                   kCanTxOn
|                   kCanHwIsStop        
|                   kCanHwIsInit        
|                   kCanHwIsInconsistent
|                   kCanHwIsWarning     
|                   kCanHwIsPassive     
|                   kCanHwIsBusOff      
|                   kCanHwIsSleep       
| DESCRIPTION:      returns the status of the transmit path and the CAN hardware.
|                   Only one of the statusbits Sleep,Busoff,Passiv,Warning is set.
|                   Sleep has the highest priority, error warning the lowerst. 
****************************************************************************/
/* CODE CATEGORY 3 START*/
C_API_1 vuint8 C_API_2 CanGetStatus( CAN_CHANNEL_CANTYPE_ONLY )
{
#if defined( C_ENABLE_EXTENDED_STATUS )
#endif


#if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser(channel < kCanNumberOfChannels, kCanAllChannels, kErrorChannelHdlTooLarge);    /* PRQA S 3109 */
#endif

#if defined( C_ENABLE_EXTENDED_STATUS )

# if defined( C_ENABLE_SLEEP_WAKEUP )
  /***************************** verify Sleep mode *************************************/
  if ( CanLL_HwIsSleep(CAN_HW_CHANNEL_CANPARA_ONLY)   )  { return ( VUINT8_CAST ( canStatus[channel] | kCanHwIsSleep ) ); }

# endif

  /***************************** verify Stop mode *************************************/
  if ( CanLL_HwIsStop(CAN_HW_CHANNEL_CANPARA_ONLY)    )  { return ( VUINT8_CAST ( canStatus[channel] | kCanHwIsStop ) ); }

  /***************************** verify Busoff *************************************/
  if ( CanLL_HwIsBusOff(CAN_HW_CHANNEL_CANPARA_ONLY)  )  { return ( VUINT8_CAST ( canStatus[channel] | kCanHwIsBusOff ) ); }

  /***************************** verify Error Passiv *******************************/
  {
    if ( CanLL_HwIsPassive(CAN_HW_CHANNEL_CANPARA_ONLY) )  { return ( VUINT8_CAST ( canStatus[channel] | kCanHwIsPassive ) ); }
  }

  /***************************** verify Error Warning ******************************/
  {
    if ( CanLL_HwIsWarning(CAN_HW_CHANNEL_CANPARA_ONLY) )  { return ( VUINT8_CAST ( canStatus[channel] | kCanHwIsWarning ) ); }
  }
#endif
  return ( VUINT8_CAST (canStatus[channel] & kCanTxOn) );

} /* END OF CanGetStatus */
/* CODE CATEGORY 3 END*/


/****************************************************************************
| NAME:             CanSleep
| CALLED BY:
| PRECONDITIONS:
| INPUT PARAMETERS: none or channel
| RETURN VALUES:    kCanOk, if CanSleep was successfull
|                   kCanFailed, if function failed
|                   kCanNotSupported, if this function is not supported
| DESCRIPTION:      disable CAN 
****************************************************************************/
/* CODE CATEGORY 4 START*/
C_API_1 vuint8 C_API_2 CanSleep(CAN_CHANNEL_CANTYPE_ONLY)
{
#if defined( C_ENABLE_SLEEP_WAKEUP )
  vuint8         canReturnCode;
#endif

#if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser(channel < kCanNumberOfChannels, kCanAllChannels, kErrorChannelHdlTooLarge);   /* PRQA S 3109 */
#endif
  assertUser((canCanInterruptCounter[channel] == (vsintx)0), channel, kErrorDisabledCanInt);    /* PRQA S 3109 */

#if defined( C_ENABLE_COND_RECEIVE_FCT )
  /* this has to be done, even if SLEEP_WAKEUP is not enabled */
  canMsgCondRecState[channel] = kCanTrue;
#endif

#if defined( C_ENABLE_SLEEP_WAKEUP )

  assertUser((canStatus[channel] & kCanTxOn) != kCanTxOn, channel, kErrorCanOnline);   /* PRQA S 3109 */

  {
    assertUser(!CanLL_HwIsStop(CAN_HW_CHANNEL_CANPARA_ONLY), channel, kErrorCanStop);     /* PRQA S 3109 */
    {
    
      CTL0 |= SLPRQ;    /* set sleep request bit */
      APPLCANTIMERSTART(kCanLoopCanSleep);
      while(((CTL0 & SLPRQ) == SLPRQ) && ((CTL1 & SLPAK) != SLPAK) && APPLCANTIMERLOOP(kCanLoopCanSleep))
      {
        ; /* wait until CAN controller sets the sleep acknowledge flag */
      } 
      APPLCANTIMEREND(kCanLoopCanSleep);
    
      if(SLPAK == (CTL1 & SLPAK)) /* check if sleep request was successful */
      {
        canReturnCode = kCanOk;
      }
      else
      {
        canReturnCode = kCanFailed;
      }
    }
  }
  return canReturnCode;
#else
# if defined( C_MULTIPLE_RECEIVE_CHANNEL ) && \
     defined( V_ENABLE_USE_DUMMY_STATEMENT )
  channel = channel;
# endif
  return kCanNotSupported;
#endif
} /* END OF CanSleep */
/* CODE CATEGORY 4 END*/

/****************************************************************************
| NAME:             CanWakeUp
| CALLED BY:
| PRECONDITIONS:
| INPUT PARAMETERS: none or channel
| RETURN VALUES:    kCanOk, if CanWakeUp was successfull
|                   kCanFailed, if function failed
|                   kCanNotSupported, if this function is not supported
| DESCRIPTION:      enable CAN 
****************************************************************************/
/* CODE CATEGORY 4 START*/
C_API_1 vuint8 C_API_2 CanWakeUp( CAN_CHANNEL_CANTYPE_ONLY )
{
#if defined( C_ENABLE_SLEEP_WAKEUP )
  vuint8         canReturnCode;


#if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser(channel < kCanNumberOfChannels, kCanAllChannels, kErrorChannelHdlTooLarge);     /* PRQA S 3109 */
#endif
  assertUser((canCanInterruptCounter[channel] == (vsintx)0), channel, kErrorDisabledCanInt);    /* PRQA S 3109 */

  {
    {
      CTL0 &= (vuint8)~SLPRQ; /* clear sleep request */
    
      APPLCANTIMERSTART(kCanLoopCanWakeup);
      while(((CTL1 & SLPAK) == SLPAK) && APPLCANTIMERLOOP(kCanLoopCanWakeup))
      { 
        ; /* wait until CAN controller is awake */
      } 
      APPLCANTIMEREND(kCanLoopCanWakeup);
    
      if(SLPAK == (CTL1 & SLPAK))
      {
        canReturnCode = kCanFailed; /* CAN controller still sleeping */
      }
      else
      {
        canReturnCode = kCanOk;
      }
    }
  }
  return canReturnCode;
#else
# if defined( C_MULTIPLE_RECEIVE_CHANNEL ) && \
     defined( V_ENABLE_USE_DUMMY_STATEMENT )
  channel = channel;
# endif
  return kCanNotSupported;
#endif /* C_ENABLE_SLEEP_WAKEUP */
} /* END OF CanWakeUp */
/* CODE CATEGORY 4 END*/


#if defined( C_ENABLE_STOP )
/****************************************************************************
| NAME:             CanStop
| CALLED BY:
| PRECONDITIONS:
| INPUT PARAMETERS: none
| RETURN VALUES:    kCanOk, if success
|                   kCanFailed, if function failed
|                   kCanNotSupported, if this function is not supported
| DESCRIPTION:      stop CAN-controller
****************************************************************************/
/* CODE CATEGORY 4 START*/
C_API_1 vuint8 C_API_2 CanStop( CAN_CHANNEL_CANTYPE_ONLY )
{
  vuint8         canReturnCode;

# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser(channel < kCanNumberOfChannels, kCanAllChannels, kErrorChannelHdlTooLarge);   /* PRQA S 3109 */
# endif
  assertUser((canStatus[channel] & kCanTxOn) != kCanTxOn, channel, kErrorCanOnline);   /* PRQA S 3109 */

  {
# if defined( C_ENABLE_SLEEP_WAKEUP )
    assertUser(!CanLL_HwIsSleep(CAN_HW_CHANNEL_CANPARA_ONLY), channel, kErrorCanSleep);   /* PRQA S 3109 */
# endif
    {
      /* request init mode */
      CTL0 = INITRQ;
      APPLCANTIMERSTART(kCanLoopInitReq);
      while( ((CTL1 & INITAK) != INITAK) && APPLCANTIMERLOOP(kCanLoopInitReq) )
      { 
        ; /* wait while not in init mode */
      } 
      APPLCANTIMEREND(kCanLoopInitReq);
    
    
      if(INITAK == (CTL1 & INITAK))
      {
        canReturnCode = kCanOk;
        canStatus[channel] |= kCanHwIsStop;
      }
      else
      {
        canReturnCode = kCanFailed;
      }
    }
  }
  return canReturnCode;
}
/* CODE CATEGORY 4 END*/

/****************************************************************************
| NAME:             CanStart
| CALLED BY:
| PRECONDITIONS:
| INPUT PARAMETERS: none
| RETURN VALUES:    kCanOk, if success
|                   kCanFailed, if function failed
|                   kCanNotSupported, if this function is not supported
| DESCRIPTION:      restart CAN-controller
****************************************************************************/
/* CODE CATEGORY 4 START*/
C_API_1 vuint8 C_API_2 CanStart( CAN_CHANNEL_CANTYPE_ONLY )
{
  vuint8         canReturnCode;

# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser(channel < kCanNumberOfChannels, kCanAllChannels, kErrorChannelHdlTooLarge);   /* PRQA S 3109 */
# endif

  {
    CanInit(CAN_CHANNEL_CANPARA_FIRST lastInitObject[channel]);
    canStatus[channel] &= (vuint8)~kCanHwIsStop;
    canReturnCode = kCanOk;
  }
  return canReturnCode;
}
/* CODE CATEGORY 4 END*/
#endif /* if defined( C_ENABLE_STOP ) */

#if defined (C_ENABLE_CAN_CAN_INTERRUPT_CONTROL)
/****************************************************************************
| NAME:             CanCanInterruptDisable
| CALLED BY:
| PRECONDITIONS:
| INPUT PARAMETERS: none
| RETURN VALUES:    none
| DESCRIPTION:      disables CAN interrupts and stores old interrupt status
****************************************************************************/
/* CODE CATEGORY 1 START*/
C_API_1 void C_API_2 CanCanInterruptDisable( CAN_CHANNEL_CANTYPE_ONLY )
{
# if defined (C_ENABLE_OSEK_CAN_INTCTRL)

  {
    OsCanCanInterruptDisable(CAN_HW_CHANNEL_CANPARA_ONLY);
  }
# else  /* defined (C_ENABLE_OSEK_CAN_INTCTRL) */

  CanDeclareGlobalInterruptOldStatus
#  if defined( C_HL_ENABLE_CAN_IRQ_DISABLE )
#  endif

  /* local variable must reside on stack or registerbank, switched */
  /* in interrupt level                                            */
  /* disable global interrupt                                      */
#  if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser(channel < kCanNumberOfChannels, kCanAllChannels, kErrorChannelHdlTooLarge);    /* PRQA S 3109 */
#  endif
  assertUser(canCanInterruptCounter[channel]<(vsint8)0x7f, kCanAllChannels, kErrorIntDisableTooOften);    /* PRQA S 3109 */

  CanNestedGlobalInterruptDisable();
  if (canCanInterruptCounter[channel] == (vsintx)0)  /* if 0 then save old interrupt status */
  {
#  if defined( C_HL_ENABLE_CAN_IRQ_DISABLE )
    {
#   if defined( C_ENABLE_SLEEP_WAKEUP )
      assertUser(!CanLL_HwIsSleep(CAN_HW_CHANNEL_CANPARA_ONLY), channel, kErrorCanSleep);     /* PRQA S 3109 */
#   endif

      CanLL_CanInterruptDisable(canHwChannel, &canCanInterruptOldStatus[canHwChannel]);
    }
#  endif
#  if defined( C_ENABLE_INTCTRL_ADD_CAN_FCT )
    ApplCanAddCanInterruptDisable(channel);
#  endif
  }
  canCanInterruptCounter[channel]++;               /* common for all platforms */

  CanNestedGlobalInterruptRestore();
# endif  /* C_ENABLE_OSEK_CAN_INTCTRL */
} /* END OF CanCanInterruptDisable */
/* CODE CATEGORY 1 END*/

/****************************************************************************
| NAME:             CanCanInterruptRestore
| CALLED BY:
| PRECONDITIONS:
| INPUT PARAMETERS: none
| RETURN VALUES:    none
| DESCRIPTION:      restores the old interrupt status of the CAN interrupt if 
|                   canCanInterruptCounter[channel] is zero
****************************************************************************/
/* CODE CATEGORY 1 START*/
C_API_1 void C_API_2 CanCanInterruptRestore( CAN_CHANNEL_CANTYPE_ONLY )
{
# if defined (C_ENABLE_OSEK_CAN_INTCTRL)

  {
    OsCanCanInterruptRestore(CAN_HW_CHANNEL_CANPARA_ONLY);
  }
# else  /* defined (C_ENABLE_OSEK_CAN_INTCTRL) */

  CanDeclareGlobalInterruptOldStatus
#  if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser(channel < kCanNumberOfChannels, kCanAllChannels, kErrorChannelHdlTooLarge);     /* PRQA S 3109 */
#  endif
  assertUser(canCanInterruptCounter[channel]>(vsintx)0, kCanAllChannels, kErrorIntRestoreTooOften);   /* PRQA S 3109 */

  CanNestedGlobalInterruptDisable();
  /* restore CAN interrupt */
  canCanInterruptCounter[channel]--;

  if (canCanInterruptCounter[channel] == (vsintx)0)         /* restore interrupt if canCanInterruptCounter=0 */
  {
#  if defined( C_HL_ENABLE_CAN_IRQ_DISABLE )    
    {
#   if defined( C_ENABLE_SLEEP_WAKEUP )
      assertUser(!CanLL_HwIsSleep(CAN_HW_CHANNEL_CANPARA_ONLY), channel, kErrorCanSleep);   /* PRQA S 3109 */
#   endif

      CanLL_CanInterruptRestore(canHwChannel, canCanInterruptOldStatus[canHwChannel]);
    }
#  endif

#  if defined( C_ENABLE_INTCTRL_ADD_CAN_FCT )
    ApplCanAddCanInterruptRestore(channel);
#  endif
  }
  CanNestedGlobalInterruptRestore();
# endif  /* defined (C_ENABLE_OSEK_CAN_INTCTRL) */
} /* END OF CanCanInterruptRestore */
/* CODE CATEGORY 1 END*/
#endif /* defined (C_ENABLE_CAN_CAN_INTERRUPT_CONTROL) */

#if defined( C_ENABLE_MSG_TRANSMIT )
/************************************************************************
* NAME:               CanMsgTransmit
* CALLED BY:          CanReceivedFunction
* PRECONDITIONS:      Called in Receive Interrupt
* PARAMETER:          Pointer to message buffer data block; This can either be
*                     a RAM structure data block or the receive buffer in the
*                     CAN chip
* RETURN VALUE:       The return value says that a transmit request was successful
*                     or not
* DESCRIPTION:        Transmit functions for gateway issues (with dynamic
*                     messages). If the transmit buffer is not free, the message
*                     is inserted in the FIFO ring buffer.
*************************************************************************/
/* Msg(4:3673) The object addressed by the pointer "txMsgStruct" is not modified in this function.
   The use of "const" should be considered to indicate that it never changes. MISRA Rule 81 - no change */
/* CODE CATEGORY 2 START*/
# if defined ( V_ENABLE_USED_GLOBAL_VAR )
/* txMsgStruct is located in far memory */
C_API_1 vuint8 C_API_2 CanMsgTransmit( CAN_CHANNEL_CANTYPE_FIRST V_MEMRAM1_FAR tCanMsgTransmitStruct V_MEMRAM2_FAR V_MEMRAM3_FAR *txMsgStruct )       /* PRQA S 3673 */      /* suppress message about const pointer */
{
# else
C_API_1 vuint8 C_API_2 CanMsgTransmit( CAN_CHANNEL_CANTYPE_FIRST tCanMsgTransmitStruct *txMsgStruct )       /* PRQA S 3673 */      /* suppress message about const pointer */
{
# endif
  CanDeclareGlobalInterruptOldStatus
  vuint8                 rc;
  CanObjectHandle          txObjHandle;
  CanObjectHandle          logTxObjHandle;

  #if defined(C_ENABLE_COPY_TX_DATA)
  # if defined(C_COMP_COSMIC_MCS12X_MSCAN12)  
  vsintx i;
  vuint8 *pDestData;
  #  if defined (V_ENABLE_USED_GLOBAL_VAR)
  V_MEMRAM1_FAR vuint8 V_MEMRAM2_FAR V_MEMRAM3_FAR *pSrcData;
  #  else
  vuint8 *pSrcData;
  #  endif
  # endif
  #endif

# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser(channel < kCanNumberOfChannels, kCanAllChannels, kErrorChannelHdlTooLarge);     /* PRQA S 3109 */
# endif


  CanNestedGlobalInterruptDisable();

  /* --- test on CAN transmit switch --- */
  if ( (canStatus[channel] & kCanTxOn) != kCanTxOn )                /* transmit path switched off */
  {
    CanNestedGlobalInterruptRestore();
    return kCanTxFailed;
  }

# if defined( C_ENABLE_CAN_RAM_CHECK )
  if(canComStatus[channel] == kCanDisableCommunication)
  {
    CanNestedGlobalInterruptRestore();
    return(kCanCommunicationDisabled);
  }
# endif

# if defined( C_ENABLE_SLEEP_WAKEUP )
  assertUser(!CanLL_HwIsSleep(CAN_HW_CHANNEL_CANPARA_ONLY), channel, kErrorCanSleep);    /* PRQA S 3109 */
# endif
  assertUser(!CanLL_HwIsStop(CAN_HW_CHANNEL_CANPARA_ONLY), channel, kErrorCanStop);      /* PRQA S 3109 */
  
  /* --- check on passive state --- */
# if defined( C_ENABLE_ECU_SWITCH_PASS )
  if ( canPassive[channel] != (vuint8)0)                             /*  set passive ? */
  {
    CanNestedGlobalInterruptRestore();
#  if defined( C_ENABLE_MSG_TRANSMIT_CONF_FCT )
    APPL_CAN_MSGTRANSMITCONF( CAN_CHANNEL_CANPARA_ONLY );
#  endif
    return (kCanTxOk);
  }
# endif /* C_ENABLE_ECU_SWITCH_PASS */

  /* calculate index for canhandleCurTxObj (logical object handle) */
  logTxObjHandle = (CanObjectHandle)((vsintx)CAN_HL_HW_MSG_TRANSMIT_INDEX(canHwChannel) + CAN_HL_TX_OFFSET_HW_TO_LOG(canHwChannel));

  /* check for transmit message object free ---------------------------------*/
  /* MsgObj used?  */
  if (( canHandleCurTxObj[logTxObjHandle] != kCanBufferFree ))    
  {
    CanNestedGlobalInterruptRestore();
    return kCanTxFailed;
  }

  /* Obj, pMsgObject points to is free, transmit msg object: ----------------*/
  /* Save hdl of msgObj to be transmitted*/
  canHandleCurTxObj[logTxObjHandle] = kCanBufferMsgTransmit;
  CanNestedGlobalInterruptRestore();



  txObjHandle = CAN_HL_HW_MSG_TRANSMIT_INDEX(canHwChannel);
  assertHardware( CanLL_TxIsHWObjFree(canHwChannel, txObjHandle ), channel, kErrorTxBufferBusy);

  CanBeginCriticalXGateSection();
  #if defined(C_COMP_COSMIC_MCS12X_MSCAN12)  
  CTBSEL = CanMailboxSelect[txObjHandle]; /* select the appropriate Tx buffer */
  #endif
  
  CanEndCriticalXGateSection();

  CanNestedGlobalInterruptDisable();
  /* Copy all data into transmit object */


  /* If CanTransmit was interrupted by a re-initialization or CanOffline */  
  /* no transmitrequest of this action should be started      */  
  if ((canHandleCurTxObj[logTxObjHandle] == kCanBufferMsgTransmit) && 
                                   ( (canStatus[channel] & kCanTxOn) == kCanTxOn ) )
  {  
     # if defined(C_COMP_COSMIC_MCS12X_MSCAN12)  
     #  if defined(V_ENABLE_USED_GLOBAL_VAR)
     VStdMemCpyFarRamToRam(CAN_TX_MAILBOX_BASIS_ADR(channel), txMsgStruct, 13);
     #  else
     VStdMemCpyRamToRam(CAN_TX_MAILBOX_BASIS_ADR(channel), txMsgStruct, 13);
     #  endif
     CAN_TX_MAILBOX_BASIS_ADR(channel) -> Prio = TX2; /* Prio Std Tx > Prio LowLevel Tx */
     # endif
     

     #if defined(C_ENABLE_TX_POLLING)
     canllTxStatusFlag[channel] |= TX2;
     #endif
     CanBeginCriticalXGateSection();
     CANTFLG  = CanMailboxSelect[txObjHandle]; /* transmit message */
     CTBSEL = TX1; /* ESCAN00022867, the assignment is relevant if low-level transmission interrupts normal transmission */
     #if defined (C_ENABLE_INDIVIDUAL_POLLING) || (!defined (C_ENABLE_INDIVIDUAL_POLLING) && !defined(C_ENABLE_TX_POLLING))
     # if defined ( C_ENABLE_INDIVIDUAL_POLLING )
     if (CanHwObjIndivPolling[CAN_HWOBJINDIVPOLLING_INDEX0][txObjHandle] == (vuint8)0x00)
     # endif
     /* in case of disabled Tx Irq the request needs to be stored in the old status variable */
     {
       if(canCanIrqDisabled[channel] == kCanTrue)
       {
         canCanInterruptOldStatus[channel].oldCanCTIER |= CanMailboxSelect[txObjHandle]; /* enable Tx interrupt */
       }
       else
       {
         CTIER |= CanMailboxSelect[txObjHandle]; /* enable Tx interrupt */
       }
     }
     #endif
     CanEndCriticalXGateSection();

# if defined( C_ENABLE_TX_OBSERVE )
     ApplCanTxObjStart( CAN_CHANNEL_CANPARA_FIRST logTxObjHandle );
# endif
   
     rc = kCanTxOk;                                    
  }  
  else  
  {  
    /* release TxHandle (CanOffline) */
    canHandleCurTxObj[logTxObjHandle] = kCanBufferFree;  
    rc = kCanTxFailed;   
  }  

  CanNestedGlobalInterruptRestore();


  return rc;
} /*end of CanMsgTransmit() */
/* CODE CATEGORY 2 END*/
#endif


#if defined( C_ENABLE_DYN_TX_OBJECTS )
/************************************************************************
* NAME:           CanGetDynTxObj
* PARAMETER:      txHandle - Handle of the dynamic object to reserve
* RETURN VALUE:   kCanNoTxDynObjAvailable (0xFF) - 
*                   object not available
*                 0..F0 - 
*                   Handle to dynamic transmission object
* DESCRIPTION:    Function reserves and return a handle to a dynamic 
*                   transmission object
*
*                 To use dynamic transmission, an application must get
*                 a dynamic object from CAN-driver. 
*                 Before transmission, application must set all attributes 
*                 (id, dlc, data, confirmation function/flag, pretransmission
*                 etc. - as configurated). 
*                 Application can use a dynamic object for one or many
*                 transmissions (as it likes) - but finally, it must
*                 release the dynamic object by calling CanReleaseDynTxObj.
*************************************************************************/
/* CODE CATEGORY 3 START*/
C_API_1 CanTransmitHandle C_API_2 CanGetDynTxObj(CanTransmitHandle txHandle )
{
  CanTransmitHandle nTxDynObj;
  CanDeclareGlobalInterruptOldStatus
  CAN_CHANNEL_CANTYPE_LOCAL
  
  assertUser((txHandle < kCanNumberOfTxObjects), kCanAllChannels, kErrorTxHdlTooLarge);    /* PRQA S 3109 */

# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  channel = CanGetChannelOfTxObj(txHandle);
# endif

  assertUser((txHandle <  CAN_HL_TX_DYN_ROM_STOPINDEX(channel)), channel, kErrorAccessedInvalidDynObj);  /* PRQA S 3109 */
  assertUser((txHandle >= CAN_HL_TX_DYN_ROM_STARTINDEX(channel)), channel, kErrorAccessedStatObjAsDyn);  /* PRQA S 3109 */ /*lint !e568 */

  nTxDynObj = (txHandle - CAN_HL_TX_DYN_ROM_STARTINDEX(channel)) + CAN_HL_TX_DYN_RAM_STARTINDEX(channel); /* PRQA S 3382,0291 */

  CanNestedGlobalInterruptDisable();
  if ( canTxDynObjReservedFlag[nTxDynObj] != (vuint8)0)
  {
    CanNestedGlobalInterruptRestore();
    return kCanNoTxDynObjAvailable;
  }
  /*  Mark dynamic object as used  */
  canTxDynObjReservedFlag[nTxDynObj] = 1;

# if defined( C_ENABLE_CONFIRMATION_FLAG )
  CanConfirmationFlags._c[CanGetConfirmationOffset(txHandle)] &= 
                            (vuint8)(~CanGetConfirmationMask(txHandle));
# endif
  CanNestedGlobalInterruptRestore();

  /* Initialize dynamic object */
# if defined( C_ENABLE_DYN_TX_DATAPTR )  
  canDynTxDataPtr[nTxDynObj] = V_NULL;  
# endif  
  

  return (txHandle);
}
/* CODE CATEGORY 3 END*/

/************************************************************************
* NAME:           CanReleaseDynTxObj
* PARAMETER:      hTxObj -
*                   Handle of dynamic transmission object
* RETURN VALUE:   --
* DESCRIPTION:    Function releases dynamic transmission object
*                   which was reserved before (calling CanGetDynTxObj)
*                 
*                 After a transmission of one or more messages is finished,
*                 application must free the reserved resource, formally the
*                 dynamic transmission object calling this function.
*
*                 As the number of dynamic transmission object is limited,
*                 application should not keep unused dynamic transmission
*                 objects for a longer time.
*************************************************************************/
/* CODE CATEGORY 3 START*/
C_API_1 vuint8 C_API_2 CanReleaseDynTxObj(CanTransmitHandle txHandle)
{
  CanTransmitHandle dynTxObj;
  CAN_CHANNEL_CANTYPE_LOCAL
# if defined( C_ENABLE_TRANSMIT_QUEUE )
  CanSignedTxHandle queueElementIdx; /* index for accessing the tx queue */
  CanSignedTxHandle elementBitIdx;  /* bit index within the tx queue element */
  CanTransmitHandle queueBitPos;  /* physical bitposition of the handle */
# endif
  
  assertUser((txHandle < kCanNumberOfTxObjects), kCanAllChannels, kErrorTxHdlTooLarge);    /* PRQA S 3109 */

# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  channel = CanGetChannelOfTxObj(txHandle);
# endif

  assertUser((txHandle <  CAN_HL_TX_DYN_ROM_STOPINDEX(channel)), channel, kErrorAccessedInvalidDynObj);  /* PRQA S 3109 */
  assertUser((txHandle >= CAN_HL_TX_DYN_ROM_STARTINDEX(channel)), channel, kErrorAccessedStatObjAsDyn);  /* PRQA S 3109 */ /*lint !e568 */

  dynTxObj = (txHandle - CAN_HL_TX_DYN_ROM_STARTINDEX(channel)) + CAN_HL_TX_DYN_RAM_STARTINDEX(channel);  /* PRQA S 3382,0291 */

  assertInternal((canTxDynObjReservedFlag[dynTxObj] != (vuint8)0), channel, kErrorReleasedUnusedDynObj);  /* PRQA S 3109 */

# if defined( C_ENABLE_TRANSMIT_QUEUE )
  #if defined( C_ENABLE_INTERNAL_CHECK ) &&\
      defined( C_MULTIPLE_RECEIVE_CHANNEL )
  if (sizeof(queueBitPos) == 1)
  {
    assertInternal( (((vuint16)kCanNumberOfTxObjects + (vuint16)CanTxQueuePadBits[kCanNumberOfChannels-1]) <= 256u), kCanAllChannels, kErrorTxQueueTooManyHandle) /* PRQA S 3109 */ /*lint !e572 !e506*/
  }
  else
  {
    assertInternal( (((vuint32)kCanNumberOfTxObjects + (vuint32)CanTxQueuePadBits[kCanNumberOfChannels-1]) <= 65536u), kCanAllChannels, kErrorTxQueueTooManyHandle) /* PRQA S 3109 */ /*lint !e572 !e506*/
  }
  #endif
  queueBitPos  = txHandle + CAN_HL_TXQUEUE_PADBITS(channel);
  queueElementIdx = (CanSignedTxHandle)(queueBitPos >> kCanTxQueueShift); /* get the queue element where to set the flag */
  elementBitIdx  = (CanSignedTxHandle)(queueBitPos & kCanTxQueueMask);   /* get the flag index wihtin the queue element */
  if( (canTxQueueFlags[queueElementIdx] & CanShiftLookUp[elementBitIdx]) != (tCanQueueElementType)0 )
  {
  }
  else
# endif
  {
    if (
# if defined( C_ENABLE_CONFIRMATION_FCT ) && \
    defined( C_ENABLE_TRANSMIT_QUEUE )
         (confirmHandle[channel] == txHandle) ||       /* confirmation active ? */
# endif
         (canHandleCurTxObj[(vsintx)CAN_HL_HW_TX_NORMAL_INDEX(canHwChannel) + CAN_HL_TX_OFFSET_HW_TO_LOG(canHwChannel)] != txHandle) )
    {
      /*  Mark dynamic object as not used  */
      canTxDynObjReservedFlag[dynTxObj] = 0;
      return(kCanDynReleased);
    }
  }
  return(kCanDynNotReleased);
}
/* CODE CATEGORY 3 END*/
#endif /* C_ENABLE_DYN_TX_OBJECTS */


#if defined( C_ENABLE_DYN_TX_ID ) 
# if !defined( C_ENABLE_EXTENDED_ID ) ||\
     defined( C_ENABLE_MIXED_ID )
/************************************************************************
* NAME:           CanDynTxObjSetId
* PARAMETER:      hTxObj -
*                   Handle of dynamic transmission object
*                 id -
*                   Id (standard-format) to register with dynamic object
* RETURN VALUE:   --
* DESCRIPTION:    Function registers submitted id (standard format)
*                 with dynamic object referenced by handle.
*************************************************************************/
/* CODE CATEGORY 2 START*/
C_API_1 void C_API_2 CanDynTxObjSetId(CanTransmitHandle txHandle, vuint16 id)
{
  CanTransmitHandle dynTxObj;

  CAN_CHANNEL_CANTYPE_LOCAL
  
  assertUser((txHandle < kCanNumberOfTxObjects), kCanAllChannels, kErrorTxHdlTooLarge);  /* PRQA S 3109 */

#  if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  channel = CanGetChannelOfTxObj(txHandle);
#  endif

  assertUser((txHandle <  CAN_HL_TX_DYN_ROM_STOPINDEX(channel)), channel, kErrorAccessedInvalidDynObj);  /* PRQA S 3109 */
  assertUser((txHandle >= CAN_HL_TX_DYN_ROM_STARTINDEX(channel)), channel, kErrorAccessedStatObjAsDyn);  /* PRQA S 3109 */ /*lint !e568 */
  assertUser(id <= (vuint16)0x7FF, channel, kErrorWrongId);                                              /* PRQA S 3109 */

  dynTxObj = (txHandle - CAN_HL_TX_DYN_ROM_STARTINDEX(channel)) + CAN_HL_TX_DYN_RAM_STARTINDEX(channel); /* PRQA S 3382,0291 */

#  if defined( C_ENABLE_MIXED_ID )
#   if defined( C_HL_ENABLE_IDTYPE_IN_ID )
#   else
  canDynTxIdType[dynTxObj]  = kCanIdTypeStd; 
#   endif
#  endif

  canDynTxId0[dynTxObj] = MK_STDID0(id);
#  if (kCanNumberOfUsedCanTxIdTables > 1)
  canDynTxId1[dynTxObj] = MK_STDID1(id);
#  endif
#  if (kCanNumberOfUsedCanTxIdTables > 2)
  canDynTxId2[dynTxObj] = MK_STDID2(id);
#  endif
#  if (kCanNumberOfUsedCanTxIdTables > 3)
  canDynTxId3[dynTxObj] = MK_STDID3(id);
#  endif
#  if (kCanNumberOfUsedCanTxIdTables > 4)
  canDynTxId4[dynTxObj] = MK_STDID4(id);
#  endif
}
/* CODE CATEGORY 2 END*/
# endif /* !defined( C_ENABLE_EXTENDED_ID ) || defined( C_ENABLE_MIXED_ID ) */
#endif /* C_ENABLE_DYN_TX_ID */

#if defined( C_ENABLE_DYN_TX_ID ) && \
    defined( C_ENABLE_EXTENDED_ID )
/************************************************************************
* NAME:           CanDynTxObjSetExtId
* PARAMETER:      hTxObj -  Handle of dynamic transmission object
*                 idExtHi - Id low word (extended-format) to register with
*                                                         dynamic object
*                 idExtLo - Id high word (extended-format) 
* RETURN VALUE:   --
* DESCRIPTION:    Function registers submitted id (standard format)
*                 with dynamic object referenced by handle.
*************************************************************************/
/* CODE CATEGORY 2 START*/
C_API_1 void C_API_2 CanDynTxObjSetExtId(CanTransmitHandle txHandle, vuint16 idExtHi, vuint16 idExtLo)
{
  CanTransmitHandle dynTxObj;
  CAN_CHANNEL_CANTYPE_LOCAL
  
  assertUser((txHandle < kCanNumberOfTxObjects), kCanAllChannels, kErrorTxHdlTooLarge);   /* PRQA S 3109 */

# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  channel = CanGetChannelOfTxObj(txHandle);
# endif

  assertUser((txHandle <  CAN_HL_TX_DYN_ROM_STOPINDEX(channel)), channel, kErrorAccessedInvalidDynObj);  /* PRQA S 3109 */
  assertUser((txHandle >= CAN_HL_TX_DYN_ROM_STARTINDEX(channel)), channel, kErrorAccessedStatObjAsDyn);  /* PRQA S 3109 */ /*lint !e568 */
  assertUser(idExtHi <= (vuint16)0x1FFF, channel, kErrorWrongId);                                        /* PRQA S 3109 */

  dynTxObj = (txHandle - CAN_HL_TX_DYN_ROM_STARTINDEX(channel)) + CAN_HL_TX_DYN_RAM_STARTINDEX(channel); /* PRQA S 3382,0291 */

# if defined( C_ENABLE_MIXED_ID )
#  if defined( C_HL_ENABLE_IDTYPE_IN_ID )
#  else
  canDynTxIdType[dynTxObj] = kCanIdTypeExt; 
#  endif
# endif

  canDynTxId0[dynTxObj]      = MK_EXTID0( ((vuint32)idExtHi<<16) | (vuint32)idExtLo );
# if (kCanNumberOfUsedCanTxIdTables > 1)
  canDynTxId1[dynTxObj]      = MK_EXTID1( ((vuint32)idExtHi<<16) | (vuint32)idExtLo );
# endif
# if (kCanNumberOfUsedCanTxIdTables > 2)
  canDynTxId2[dynTxObj]      = MK_EXTID2( ((vuint32)idExtHi<<16) | (vuint32)idExtLo );
# endif
# if (kCanNumberOfUsedCanTxIdTables > 3)
  canDynTxId3[dynTxObj]      = MK_EXTID3( ((vuint32)idExtHi<<16) | (vuint32)idExtLo );
# endif
# if (kCanNumberOfUsedCanTxIdTables > 4)
  canDynTxId4[dynTxObj]      = MK_EXTID4( ((vuint32)idExtHi<<16) | (vuint32)idExtLo );
# endif
}
/* CODE CATEGORY 2 END*/
#endif


#if defined( C_ENABLE_DYN_TX_DLC )
/************************************************************************
* NAME:           CanDynTxObjSetDlc
* PARAMETER:      hTxObj -
*                   Handle of dynamic transmission object
*                 dlc -
*                   data length code to register with dynamic object
* RETURN VALUE:   --
* DESCRIPTION:    Function registers data length code with 
*                 dynamic object referenced by submitted handle.
*************************************************************************/
/* CODE CATEGORY 2 START*/
C_API_1 void C_API_2 CanDynTxObjSetDlc(CanTransmitHandle txHandle, vuint8 dlc)
{
  CanTransmitHandle dynTxObj;
  CAN_CHANNEL_CANTYPE_LOCAL
  
  assertUser((txHandle < kCanNumberOfTxObjects), kCanAllChannels, kErrorTxHdlTooLarge);     /* PRQA S 3109 */

# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  channel = CanGetChannelOfTxObj(txHandle);
# endif
  
  assertUser((txHandle <  CAN_HL_TX_DYN_ROM_STOPINDEX(channel)), channel, kErrorAccessedInvalidDynObj);  /* PRQA S 3109 */
  assertUser((txHandle >= CAN_HL_TX_DYN_ROM_STARTINDEX(channel)), channel, kErrorAccessedStatObjAsDyn);  /* PRQA S 3109 */ /*lint !e568 */
  assertUser(dlc<(vuint8)9, channel, kErrorTxDlcTooLarge);                                               /* PRQA S 3109 */
  
  dynTxObj = (txHandle - CAN_HL_TX_DYN_ROM_STARTINDEX(channel)) + CAN_HL_TX_DYN_RAM_STARTINDEX(channel); /* PRQA S 3382,0291 */
  
# if defined( C_ENABLE_EXTENDED_ID )
  canDynTxDLC[dynTxObj] = MK_TX_DLC_EXT(dlc);
# else
  canDynTxDLC[dynTxObj] = MK_TX_DLC(dlc);
# endif
}
/* CODE CATEGORY 2 END*/
#endif /* C_ENABLE_DYN_TX_DLC */


#if defined( C_ENABLE_DYN_TX_DATAPTR )
/************************************************************************
* NAME:           CanDynTxObjSetData
* PARAMETER:      hTxObj -
*                   Handle of dynamic transmission object
*                 pData -
*                   data reference to be stored in data buffer of dynamic object
* RETURN VALUE:   --
* DESCRIPTION:    Functions stores reference to data registered with
*                 dynamic object.
*                 
*                 The number of byte copied is (always) 8. The number of 
*                 relevant (and consequently evaluated) byte is to be 
*                 taken from function CanDynObjGetDLC.
*************************************************************************/
/* CODE CATEGORY 2 START*/
C_API_1 void C_API_2 CanDynTxObjSetDataPtr(CanTransmitHandle txHandle, void* pData)
{
  CanTransmitHandle dynTxObj;
  CAN_CHANNEL_CANTYPE_LOCAL
  
  assertUser((txHandle < kCanNumberOfTxObjects), kCanAllChannels, kErrorTxHdlTooLarge);    /* PRQA S 3109 */

# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  channel = CanGetChannelOfTxObj(txHandle);
# endif

  assertUser((txHandle <  CAN_HL_TX_DYN_ROM_STOPINDEX(channel)), channel, kErrorAccessedInvalidDynObj);  /* PRQA S 3109 */
  assertUser((txHandle >= CAN_HL_TX_DYN_ROM_STARTINDEX(channel)), channel, kErrorAccessedStatObjAsDyn);  /* PRQA S 3109 */ /*lint !e568 */

  dynTxObj = (txHandle - CAN_HL_TX_DYN_ROM_STARTINDEX(channel)) + CAN_HL_TX_DYN_RAM_STARTINDEX(channel);  /* PRQA S 3382,0291 */

  canDynTxDataPtr[dynTxObj] = pData;
}
/* CODE CATEGORY 2 END*/
#endif /* C_ENABLE_DYN_TX_DATAPTR */




#if defined( C_ENABLE_TX_MASK_EXT_ID )
/************************************************************************
* NAME:               CanSetTxIdExtHi
* CALLED BY:          
* PRECONDITIONS:      CanInitPower should already been called.
* PARAMETER:          new source address for the 29-bit CAN-ID
* RETURN VALUE:       -
* DESCRIPTION:        Sets the source address in the lower 8 bit of the
*                     29-bit CAN identifier.
*************************************************************************/
/* CODE CATEGORY 4 START*/
C_API_1 void C_API_2 CanSetTxIdExtHi( CAN_CHANNEL_CANTYPE_FIRST  vuint8 mask )
{  
# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser(channel < kCanNumberOfChannels, kCanAllChannels, kErrorChannelHdlTooLarge);     /* PRQA S 3109 */
# endif
  assertUser(mask <= (vuint8)0x1F, channel, kErrorWrongMask);                                /* PRQA S 3109 */

  canTxMask0[channel] = (canTxMask0[channel] & MK_EXTID0(0x00FFFFFFUL)) | MK_EXTID0((vuint32)mask<<24);
# if (kCanNumberOfUsedCanTxIdTables > 1)
  canTxMask1[channel] = (canTxMask1[channel] & MK_EXTID1(0x00FFFFFFUL)) | MK_EXTID1((vuint32)mask<<24);
# endif
# if (kCanNumberOfUsedCanTxIdTables > 2)
  canTxMask2[channel] = (canTxMask2[channel] & MK_EXTID2(0x00FFFFFFUL)) | MK_EXTID2((vuint32)mask<<24);
# endif
# if (kCanNumberOfUsedCanTxIdTables > 3)
  canTxMask3[channel] = (canTxMask3[channel] & MK_EXTID3(0x00FFFFFFUL)) | MK_EXTID3((vuint32)mask<<24);
# endif
# if (kCanNumberOfUsedCanTxIdTables > 4)
  canTxMask4[channel] = (canTxMask4[channel] & MK_EXTID4(0x00FFFFFFUL)) | MK_EXTID4((vuint32)mask<<24);
# endif
}
/* CODE CATEGORY 4 END*/

/************************************************************************
* NAME:               CanSetTxIdExtMidHi
* CALLED BY:          
* PRECONDITIONS:      CanInitPower should already been called.
* PARAMETER:          new source address for the 29-bit CAN-ID
* RETURN VALUE:       -
* DESCRIPTION:        Sets the source address in the lower 8 bit of the
*                     29-bit CAN identifier.
*************************************************************************/
/* CODE CATEGORY 4 START*/
C_API_1 void C_API_2 CanSetTxIdExtMidHi( CAN_CHANNEL_CANTYPE_FIRST  vuint8 mask )
{
# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser(channel < kCanNumberOfChannels, kCanAllChannels, kErrorChannelHdlTooLarge);     /* PRQA S 3109 */
# endif

  canTxMask0[channel] = (canTxMask0[channel] & MK_EXTID0(0xFF00FFFFUL)) | MK_EXTID0((vuint32)mask<<16);   /*lint !e572*/
# if (kCanNumberOfUsedCanTxIdTables > 1)
  canTxMask1[channel] = (canTxMask1[channel] & MK_EXTID1(0xFF00FFFFUL)) | MK_EXTID1((vuint32)mask<<16);   /*lint !e572*/
# endif
# if (kCanNumberOfUsedCanTxIdTables > 2)
  canTxMask2[channel] = (canTxMask2[channel] & MK_EXTID2(0xFF00FFFFUL)) | MK_EXTID2((vuint32)mask<<16);   /*lint !e572*/
# endif
# if (kCanNumberOfUsedCanTxIdTables > 3)
  canTxMask3[channel] = (canTxMask3[channel] & MK_EXTID3(0xFF00FFFFUL)) | MK_EXTID3((vuint32)mask<<16);   /*lint !e572*/
# endif
# if (kCanNumberOfUsedCanTxIdTables > 4)
  canTxMask4[channel] = (canTxMask4[channel] & MK_EXTID4(0xFF00FFFFUL)) | MK_EXTID4((vuint32)mask<<16);   /*lint !e572*/
# endif
}
/* CODE CATEGORY 4 END*/

/************************************************************************
* NAME:               CanSetTxIdExtMidLo
* CALLED BY:          
* PRECONDITIONS:      CanInitPower should already been called.
* PARAMETER:          new source address for the 29-bit CAN-ID
* RETURN VALUE:       -
* DESCRIPTION:        Sets the source address in the lower 8 bit of the
*                     29-bit CAN identifier.
*************************************************************************/
/* CODE CATEGORY 4 START*/
C_API_1 void C_API_2 CanSetTxIdExtMidLo( CAN_CHANNEL_CANTYPE_FIRST  vuint8 mask )
{
# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser(channel < kCanNumberOfChannels, kCanAllChannels, kErrorChannelHdlTooLarge);    /* PRQA S 3109 */
# endif

  canTxMask0[channel] = (canTxMask0[channel] & MK_EXTID0(0xFFFF00FFUL)) | MK_EXTID0((vuint32)mask<<8);    /*lint !e572*/
# if (kCanNumberOfUsedCanTxIdTables > 1)
  canTxMask1[channel] = (canTxMask1[channel] & MK_EXTID1(0xFFFF00FFUL)) | MK_EXTID1((vuint32)mask<<8);    /*lint !e572*/
# endif
# if (kCanNumberOfUsedCanTxIdTables > 2)
  canTxMask2[channel] = (canTxMask2[channel] & MK_EXTID2(0xFFFF00FFUL)) | MK_EXTID2((vuint32)mask<<8);    /*lint !e572*/
# endif
# if (kCanNumberOfUsedCanTxIdTables > 3)
  canTxMask3[channel] = (canTxMask3[channel] & MK_EXTID3(0xFFFF00FFUL)) | MK_EXTID3((vuint32)mask<<8);    /*lint !e572*/
# endif
# if (kCanNumberOfUsedCanTxIdTables > 4)
  canTxMask4[channel] = (canTxMask4[channel] & MK_EXTID4(0xFFFF00FFUL)) | MK_EXTID4((vuint32)mask<<8);    /*lint !e572*/
# endif
}
/* CODE CATEGORY 4 END*/

/************************************************************************
* NAME:               CanSetTxIdExtLo
* CALLED BY:          
* PRECONDITIONS:      CanInitPower should already been called.
* PARAMETER:          new source address for the 29-bit CAN-ID
* RETURN VALUE:       -
* DESCRIPTION:        Sets the source address in the lower 8 bit of the
*                     29-bit CAN identifier.
*************************************************************************/
/* CODE CATEGORY 4 START*/
C_API_1 void C_API_2 CanSetTxIdExtLo( CAN_CHANNEL_CANTYPE_FIRST  vuint8 mask )
{
# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser(channel < kCanNumberOfChannels, kCanAllChannels, kErrorChannelHdlTooLarge);    /* PRQA S 3109 */
# endif

  canTxMask0[channel] = (canTxMask0[channel] & MK_EXTID0(0xFFFFFF00UL)) | MK_EXTID0((vuint32)mask);     /*lint !e572*/
# if (kCanNumberOfUsedCanTxIdTables > 1)
  canTxMask1[channel] = (canTxMask1[channel] & MK_EXTID1(0xFFFFFF00UL)) | MK_EXTID1((vuint32)mask);     /*lint !e572*/
# endif
# if (kCanNumberOfUsedCanTxIdTables > 2)
  canTxMask2[channel] = (canTxMask2[channel] & MK_EXTID2(0xFFFFFF00UL)) | MK_EXTID2((vuint32)mask);     /*lint !e572*/
# endif
# if (kCanNumberOfUsedCanTxIdTables > 3)
  canTxMask3[channel] = (canTxMask3[channel] & MK_EXTID3(0xFFFFFF00UL)) | MK_EXTID3((vuint32)mask);     /*lint !e572*/
# endif
# if (kCanNumberOfUsedCanTxIdTables > 4)
  canTxMask4[channel] = (canTxMask4[channel] & MK_EXTID4(0xFFFFFF00UL)) | MK_EXTID4((vuint32)mask);    /*lint !e572*/
# endif
}
/* CODE CATEGORY 4 END*/
#endif

#if defined( C_ENABLE_TX_OBSERVE )
/************************************************************************
* NAME:               CanTxGetActHandle
* CALLED BY:          
* PRECONDITIONS:      
* PARAMETER:          logical hardware object handle
* RETURN VALUE:       handle of the message in the assigned mailbox
* DESCRIPTION:        get transmit handle of the message, which is currently
*                     in the mailbox txHwObject.
*************************************************************************/
/* CODE CATEGORY 3 START*/
C_API_1 CanTransmitHandle C_API_2 CanTxGetActHandle( CanObjectHandle logicalTxHdl )
{
  assertUser(logicalTxHdl < kCanNumberOfUsedTxCANObjects, kCanAllChannels, kErrorTxHwHdlTooLarge);     /* PRQA S 3109 */

  return (canHandleCurTxObj[logicalTxHdl]);       /*lint !e661*/
}
/* CODE CATEGORY 3 END*/
#endif

#if defined( C_ENABLE_VARIABLE_RX_DATALEN )
/************************************************************************
* NAME:               CanSetVariableRxDatalen
* CALLED BY:          
* PRECONDITIONS:      
* PARAMETER:          rxHandle: Handle of receive Message for which the datalen has
*                               to be changed
*                     dataLen:  new number of bytes, which have to be copied to the 
*                               message buffer.
* RETURN VALUE:       -
* DESCRIPTION:        change the dataLen of a receive message to copy a 
*                     smaller number of bytes than defined in the database.
*                     the dataLen can only be decreased. If the parameter
*                     dataLen is bigger than the statically defined value
*                     the statically defined value will be set.
*************************************************************************/
/* CODE CATEGORY 1 START*/
static void CanSetVariableRxDatalen (CanReceiveHandle rxHandle, vuint8 dataLen)
{
  assertInternal(rxHandle < kCanNumberOfRxObjects, kCanAllChannels , kErrorRxHandleWrong);  /* PRQA S 3109 */ /* legal rxHandle ? */
  /* assertion for dataLen not necessary due to runtime check */

  if (dataLen < CanGetRxDataLen(rxHandle))
  {
    canVariableRxDataLen[rxHandle]=dataLen;
  }
  else
  {
    canVariableRxDataLen[rxHandle] = CanGetRxDataLen(rxHandle);
  }
}
/* CODE CATEGORY 1 END*/
#endif

#if defined( C_ENABLE_COND_RECEIVE_FCT )
/************************************************************************
* NAME:               CanSetMsgReceivedCondition
* CALLED BY:          Application
* PRECONDITIONS:      
* PARAMETER:          -.
* RETURN VALUE:       -
* DESCRIPTION:        The service function CanSetMsgReceivedCondition()
*                     enables the calling of ApplCanMsgCondReceived()
*************************************************************************/
/* CODE CATEGORY 3 START*/
C_API_1 void C_API_2 CanSetMsgReceivedCondition( CAN_CHANNEL_CANTYPE_ONLY )
{
# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser((channel < kCanNumberOfChannels), kCanAllChannels, kErrorChannelHdlTooLarge);     /* PRQA S 3109 */
# endif

  canMsgCondRecState[channel] = kCanTrue;
}
/* CODE CATEGORY 3 END*/

/************************************************************************
* NAME:               CanResetMsgReceivedCondition
* CALLED BY:          Application
* PRECONDITIONS:      
* PARAMETER:          -
* RETURN VALUE:       -
* DESCRIPTION:        The service function CanResetMsgReceivedCondition()
*                     disables the calling of ApplCanMsgCondReceived()
*************************************************************************/
/* CODE CATEGORY 3 START*/
C_API_1 void C_API_2 CanResetMsgReceivedCondition( CAN_CHANNEL_CANTYPE_ONLY )
{
# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser((channel < kCanNumberOfChannels), kCanAllChannels, kErrorChannelHdlTooLarge);    /* PRQA S 3109 */
# endif

  canMsgCondRecState[channel] = kCanFalse;
}
/* CODE CATEGORY 3 END*/

/************************************************************************
* NAME:               CanGetMsgReceivedCondition
* CALLED BY:          Application
* PRECONDITIONS:      
* PARAMETER:          -
* RETURN VALUE:       status of Conditional receive function:
*                     kCanTrue : Condition is set -> ApplCanMsgCondReceived 
*                                will be called
*                     kCanFalse: Condition is not set -> ApplCanMsgCondReceived
*                                will not be called
* DESCRIPTION:        The service function CanGetMsgReceivedCondition() 
*                     returns the status of the condition for calling
*                     ApplCanMsgCondReceived()
*************************************************************************/
/* CODE CATEGORY 3 START*/
C_API_1 vuint8 C_API_2 CanGetMsgReceivedCondition( CAN_CHANNEL_CANTYPE_ONLY )
{
# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertUser((channel < kCanNumberOfChannels), kCanAllChannels, kErrorChannelHdlTooLarge);   /* PRQA S 3109 */
# endif

  return( canMsgCondRecState[channel] );
}
/* CODE CATEGORY 3 END*/
#endif


#if defined( C_MULTIPLE_RECEIVE_CHANNEL )
/************************************************************************
* NAME:           ApplCanChannelDummy
* PARAMETER:      channel
*                 current receive channel
* RETURN VALUE:   ---
* DESCRIPTION:    dummy-function for unused Callback-functions
*************************************************************************/
/* CODE CATEGORY 3 START*/
C_API_1 void C_API_2 ApplCanChannelDummy( CanChannelHandle channel )
{
# if defined( V_ENABLE_USE_DUMMY_STATEMENT )
  channel = channel;     /* to avoid lint warnings */
# endif
}
/* CODE CATEGORY 3 END*/
#endif   /* C_MULTIPLE_RECEIVE_CHANNEL */


#if defined( C_MULTIPLE_RECEIVE_CHANNEL ) || \
    defined( C_HL_ENABLE_DUMMY_FCT_CALL )
/************************************************************************
* NAME:           ApplCanRxStructPtrDummy
* PARAMETER:      rxStruct
*                 pointer of CanRxInfoStruct
* RETURN VALUE:   kCanCopyData 
* DESCRIPTION:    dummy-function for unused Callback-functions
*************************************************************************/
/* CODE CATEGORY 1 START*/
C_API_1 vuint8 C_API_2 ApplCanRxStructPtrDummy( CanRxInfoStructPtr rxStruct )
{
# if defined( V_ENABLE_USE_DUMMY_STATEMENT )
  rxStruct = rxStruct;     /* to avoid lint warnings */
# endif
  return kCanCopyData;
}
/* CODE CATEGORY 1 END*/

/************************************************************************
* NAME:           ApplCanTxHandleDummy
* PARAMETER:      txHandle
*                 transmit handle
* RETURN VALUE:   ---
* DESCRIPTION:    dummy-function for unused Callback-functions
*************************************************************************/
/* CODE CATEGORY 1 START*/
C_API_1 void C_API_2 ApplCanTxHandleDummy( CanTransmitHandle txHandle )
{
# if defined( V_ENABLE_USE_DUMMY_STATEMENT )
  txHandle = txHandle;     /* to avoid lint warnings */
# endif
}
/* CODE CATEGORY 1 END*/
#endif   /* C_MULTIPLE_RECEIVE_CHANNEL || C_HL_ENABLE_DUMMY_FCT_CALL */

#if defined( C_HL_ENABLE_DUMMY_FCT_CALL )
/************************************************************************
* NAME:           ApplCanTxStructDummy
* PARAMETER:      txStruct
*                 pointer of CanTxInfoStruct
* RETURN VALUE:   kCanCopyData
* DESCRIPTION:    dummy-function for unused Callback-functions
*************************************************************************/
/* CODE CATEGORY 1 START*/
C_API_1 vuint8 C_API_2 ApplCanTxStructDummy( CanTxInfoStruct txStruct )
{
# if defined( V_ENABLE_USE_DUMMY_STATEMENT )
  txStruct = txStruct;     /* to avoid lint warnings */
# endif
  return kCanCopyData;
}
/* CODE CATEGORY 1 END*/

/************************************************************************
* NAME:           ApplCanRxHandleDummy
* PARAMETER:      rxHandle
*                 receive handle
* RETURN VALUE:   ---
* DESCRIPTION:    dummy-function for unused Callback-functions
*************************************************************************/
/* CODE CATEGORY 1 START*/
C_API_1 void C_API_2 ApplCanRxHandleDummy( CanReceiveHandle rxHandle )
{
# if defined( V_ENABLE_USE_DUMMY_STATEMENT )
  rxHandle = rxHandle;     /* to avoid lint warnings */
# endif  
}
/* CODE CATEGORY 1 END*/
#endif /* C_HL_ENABLE_DUMMY_FCT_CALL */

#if defined( C_ENABLE_RX_QUEUE )
/************************************************************************
* NAME:               CanHL_ReceivedRxHandleQueue
* CALLED BY:          CanBasicCanMsgReceived, CanFullCanMsgReceived
* Preconditions:      none
* PARAMETER:          none
* RETURN VALUE:       none
* DESCRIPTION:        Writes receive data into queue or starts further
*                     processing for this message
*************************************************************************/
/* CODE CATEGORY 1 START*/
static vuint8 CanHL_ReceivedRxHandleQueue(CAN_CHANNEL_CANTYPE_ONLY)
{
  CanDeclareGlobalInterruptOldStatus
  tCanRxInfoStruct    *pCanRxInfoStruct;


# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
  assertInternal(channel < kCanNumberOfChannels, kCanAllChannels, kErrorInternalChannelHdlTooLarge);  /* PRQA S 3109 */
# endif

  /* Rx Queue is supported with C_HL_ENABLE_RX_INFO_STRUCT_PTR only! */
  pCanRxInfoStruct =  &canRxInfoStruct[channel];

  /* if C_ENABLE_APPLCANPRERXQUEUE is not set, a macro ApplCanPreRxQueue has to be provided by the tool */
  /* in case of ranges, ApplCanPreRxQueue has to return kCanCopyData! */
# if defined( C_ENABLE_APPLCANPRERXQUEUE )
  if(ApplCanPreRxQueue(CAN_HL_P_RX_INFO_STRUCT(channel)) == kCanCopyData)
# endif
  {
    /* Disable the interrupts because nested interrupts can take place -
      CAN interrupts of all channels have to be disabled here*/
    CanNestedGlobalInterruptDisable();
    if(canRxQueue.canRxQueueCount < kCanRxQueueSize)   /* Queue full ? */
    {
      if (canRxQueue.canRxQueueWriteIndex == (kCanRxQueueSize - 1) )
      {
        canRxQueue.canRxQueueWriteIndex = 0;
      }
      else
      {
        canRxQueue.canRxQueueWriteIndex++;
      }
      canRxQueue.canRxQueueBuf[canRxQueue.canRxQueueWriteIndex].Channel = channel;
      canRxQueue.canRxQueueBuf[canRxQueue.canRxQueueWriteIndex].Handle  = pCanRxInfoStruct->Handle;

      #if defined(C_COMP_COSMIC_MCS12X_MSCAN12)  
      # if defined(C_SEARCH_XGATE)
      VStdMemCpyRamToRam(canRxQueue.canRxQueueBuf[canRxQueue.canRxQueueWriteIndex].CanChipMsgObj, &(canRxTmpBuf[channel]), sizeof(tCanMsgTransmitStruct));
      # else
      VStdMemCpyRamToRam(canRxQueue.canRxQueueBuf[canRxQueue.canRxQueueWriteIndex].CanChipMsgObj, CAN_RX_MAILBOX_BASIS_ADR(channel), sizeof(tCanMsgTransmitStruct));
      # endif
      #endif
      
      

      canRxQueue.canRxQueueCount++;
    }
# if defined( C_ENABLE_RXQUEUE_OVERRUN_NOTIFY )
    else
    {
      ApplCanRxQueueOverrun();
    }
# endif
    CanNestedGlobalInterruptRestore();
  } 
# if defined( C_ENABLE_APPLCANPRERXQUEUE )
  else
  {
    /* Call the application call-back functions and set flags */
#  if defined( C_ENABLE_RX_QUEUE_RANGE )
    if (pCanRxInfoStruct->Handle < kCanNumberOfRxObjects )
#  endif
    { 
      return CanHL_ReceivedRxHandle(CAN_CHANNEL_CANPARA_FIRST pCanRxInfoStruct);
    }
  }
# endif
  return kCanHlFinishRx;
}
/* CODE CATEGORY 1 END*/

/************************************************************************
* NAME:               CanHandleRxMsg
* CALLED BY:          Application
* Preconditions:      none
* PARAMETER:          none
* RETURN VALUE:       none
* DESCRIPTION:        Calls PreCopy and/or Indication, if existent and
*                     set the indication flag
*************************************************************************/
/* CODE CATEGORY 2 START*/
C_API_1 void C_API_2 CanHandleRxMsg(void)
{
  CanDeclareGlobalInterruptOldStatus
  CAN_CHANNEL_CANTYPE_LOCAL
  tCanRxInfoStruct        localCanRxInfoStruct;

  while ( canRxQueue.canRxQueueCount != (vuintx)0 )
  {

# if defined( C_MULTIPLE_RECEIVE_CHANNEL )
    channel = canRxQueue.canRxQueueBuf[canRxQueue.canRxQueueReadIndex].Channel;
# endif


    CAN_CAN_INTERRUPT_DISABLE( CAN_CHANNEL_CANPARA_ONLY );

    /* Call the application call-back functions and set flags */
    localCanRxInfoStruct.Handle      = canRxQueue.canRxQueueBuf[canRxQueue.canRxQueueReadIndex].Handle;
    localCanRxInfoStruct.pChipData   = (CanChipDataPtr)&(canRxQueue.canRxQueueBuf[canRxQueue.canRxQueueReadIndex].CanChipMsgObj.DataFld[0]);
    canRDSRxPtr[channel] = localCanRxInfoStruct.pChipData;
    localCanRxInfoStruct.pChipMsgObj = (CanChipMsgPtr) &(canRxQueue.canRxQueueBuf[canRxQueue.canRxQueueReadIndex].CanChipMsgObj);
    localCanRxInfoStruct.Channel     = channel;

# if defined( C_ENABLE_RX_QUEUE_RANGE )
    switch ( localCanRxInfoStruct.Handle)
    {
#  if defined( C_ENABLE_RANGE_0 )
      case kCanRxHandleRange0: (void)APPLCANRANGE0PRECOPY( &localCanRxInfoStruct ); break;
#  endif
#  if defined( C_ENABLE_RANGE_1 )
      case kCanRxHandleRange1: (void)APPLCANRANGE1PRECOPY( &localCanRxInfoStruct ); break;
#  endif
#  if defined( C_ENABLE_RANGE_2 )
      case kCanRxHandleRange2: (void)APPLCANRANGE2PRECOPY( &localCanRxInfoStruct ); break;
#  endif
#  if defined( C_ENABLE_RANGE_3 )
      case kCanRxHandleRange3: (void)APPLCANRANGE3PRECOPY( &localCanRxInfoStruct ); break;
#  endif
      default:
#  if defined( C_ENABLE_INDICATION_FLAG ) || \
      defined( C_ENABLE_INDICATION_FCT )
             if( CanHL_ReceivedRxHandle( CAN_CHANNEL_CANPARA_FIRST &localCanRxInfoStruct ) == kCanHlContinueRx )
             {
               CanHL_IndRxHandle(localCanRxInfoStruct.Handle);
             }
#  else
             (void) CanHL_ReceivedRxHandle( CAN_CHANNEL_CANPARA_FIRST &localCanRxInfoStruct );
#  endif
             break;
   }
# else
#  if defined( C_ENABLE_INDICATION_FLAG ) || \
      defined( C_ENABLE_INDICATION_FCT )
    if( CanHL_ReceivedRxHandle( CAN_CHANNEL_CANPARA_FIRST &localCanRxInfoStruct ) == kCanHlContinueRx )
    {
      CanHL_IndRxHandle(localCanRxInfoStruct.Handle);
    }
#  else
    (void) CanHL_ReceivedRxHandle( CAN_CHANNEL_CANPARA_FIRST &localCanRxInfoStruct );
#  endif
# endif
    
    CAN_CAN_INTERRUPT_RESTORE( CAN_CHANNEL_CANPARA_ONLY );

    CanNestedGlobalInterruptDisable();
    if (canRxQueue.canRxQueueReadIndex == (kCanRxQueueSize - 1) )
    {
      canRxQueue.canRxQueueReadIndex = 0;
    }
    else
    {
      canRxQueue.canRxQueueReadIndex++;
    }
    canRxQueue.canRxQueueCount--;
    CanNestedGlobalInterruptRestore();
  }
  return;
} /* end of CanHandleRxMsg() */
/* CODE CATEGORY 2 END*/

/************************************************************************
* NAME:               CanDeleteRxQueue
* CALLED BY:          Application, CAN driver
* Preconditions:      none
* PARAMETER:          none
* RETURN VALUE:       none
* DESCRIPTION:        delete receive queue
*************************************************************************/
/* CODE CATEGORY 4 START*/
C_API_1 void C_API_2 CanDeleteRxQueue(void)
{
  CanDeclareGlobalInterruptOldStatus
  
  CanNestedGlobalInterruptDisable();
  canRxQueue.canRxQueueWriteIndex  = (vuintx)0xFFFFFFFF;
  canRxQueue.canRxQueueReadIndex   = 0;
  canRxQueue.canRxQueueCount       = 0;  
  CanNestedGlobalInterruptRestore();
} /* end of CanDeleteRxQueue() */
/* CODE CATEGORY 4 END*/

# if defined( C_ENABLE_RX_QUEUE_EXTERNAL )
/************************************************************************
* NAME:               CanSetRxQueuePtr
* CALLED BY:          Application
* Preconditions:      none
* PARAMETER:          pQueue: pointer to the external queue which is used by the driver
* RETURN VALUE:       none
* DESCRIPTION:        If the driver is configured to use a rx queue it is
*                     possible to use an application defined queue.
*                     In case of a QNX system the queue is located in the
*                     system page
*************************************************************************/
/* CODE CATEGORY 4 START*/
C_API_1 void C_API_2 CanSetRxQueuePtr(tCanRxQueue* pQueue)
{
  canRxQueuePtr = pQueue;
}
/* CODE CATEGORY 4 END*/
# endif  /* C_ENABLE_EXTERNAL_RX_QUEUE */
#endif /* C_ENABLE_RX_QUEUE */




/* End of channel */
/* STOPSINGLE_OF_MULTIPLE */
/* Kernbauer Version: 1.16 Konfiguration: can_driver Erzeugungsgangnummer: 1 */

/* Kernbauer Version: 1.16 Konfiguration: can_driver Erzeugungsgangnummer: 1 */

/* Kernbauer Version: 1.16 Konfiguration: can_driver Erzeugungsgangnummer: 1 */

/* Kernbauer Version: 1.16 Konfiguration: can_driver Erzeugungsgangnummer: 1 */

/************   Organi, Version 3.9.0 Vector-Informatik GmbH  ************/
/************   Organi, Version 3.9.0 Vector-Informatik GmbH  ************/
