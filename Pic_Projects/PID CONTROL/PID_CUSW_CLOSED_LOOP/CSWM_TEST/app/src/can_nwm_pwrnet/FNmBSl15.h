/*  *****   STARTSINGLE_OF_MULTIPLE    *****  */


/******************************************************************************
| Project Name: Nm_ClassB_Fiat
|
|  Description: Implementation of Fiat ClassB Network Management
|
|------------------------------------------------------------------------------
|               C O P Y R I G H T
|------------------------------------------------------------------------------
| Copyright (c) 2000-2010 by Vector Informatik GmbH.      All rights reserved.
|
|       This software is copyright protected and 
|       proprietary to Vector Informatik GmbH.
|       Vector Informatik GmbH grants to you only
|       those rights as set out in the license conditions.
|       All other rights remain with Vector Informatik GmbH.
|
|       Diese Software ist urheberrechtlich geschuetzt. 
|       Vector Informatik GmbH raeumt Ihnen an dieser Software nur 
|       die in den Lizenzbedingungen ausdruecklich genannten Rechte ein.
|       Alle anderen Rechte verbleiben bei Vector Informatik GmbH.
|
|------------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|------------------------------------------------------------------------------
| Initials     Name                      Company
| --------     ---------------------     --------------------------------------
| Bs           Thomas Balster            Vector Informatik GmbH
| dH           Gunnar de Haan            Vector Informatik GmbH
| Et           Thomas Ebert              Vector Informatik GmbH
| Th           Jochen Theuer             Vector Informatik GmbH
| Krt          Kerstin Thim              Vector Informatik GmbH
| visoh        Oliver Hornung            Vector Informatik GmbH
|------------------------------------------------------------------------------
|               R E V I S I O N   H I S T O R Y
|------------------------------------------------------------------------------
| Date        Ver        Author   Description
| ---------   -----      ------   ----------------------------------------------------
| 09-May-00    1.00      dH       Creation
| 26-Jun-00    1.01      dH       added extern declaration for function NmCanError
|                                 removed extern declaration for function NmPrecopy
| 23-Oct-00    1.02      dH       added current failstate
| 30-Nov-00    1.03a     dH       added prototype for ApplNmBusOff() callback
| 14-SEP-2001  1.04      Th       adaption on VECTOR standard
| 20-NOV-2001  1.05      Th       little adaptions against compiler warnings
| 11-DEC-2001  1.06      Th       ESCAN00001902 FIAT NM15 can now be configured via GenTool
| 15-JAN-2002  1.07      Et,Th    ESCAN00002058 merge network management +15 and +30
| 24-JAN-2002  1.08      Bs       ESCAN00002153 extern declaration of stored version numbers added 
| 25-FEB-2002  1.08.01   Th       ESCAN00002246 added support for single receive buffer
|                                 ESCAN00002373 added support for indexed CAN driver
| 07-MAR-2002  1.09      Et       no changes
| 11-MAR-2002  1.10      Et       no changes
| 17-APR-2002  1.11      Th       merging several versions
| 14-JUN-2002  1.12      Bs       no changes
| 07-MAY-2003  1.13      Th       ESCAN00005392 adaption on new bus off behaviour
| 23-JUN-2003  1.14      Th       no changes, see C-file
| 2003-09-01   1.15      Et       no changes, see C-file
| 2003-09-10   1.16      Et       ESCAN00006599 replace NM_CHANNEL_CAN... by NM_CHANNEL_NM...
| 2003-09-17   1.17.00   Bs       ESCAN00006589 master algorithm added
| 2003-10-09   1.18.00   Bs       minor changes
| 2003-10-20   1.19.00   Bs       ESCAN00006835 little changes to support indexed CANdriver systems
| 2003-12-01   1.20.00   Bs       ESCAN00007100 delete assertion about NmConfirmation in generated table
|                                 ESCAN00007101 encapsulation of assertion error defines has to be corrected
| 2003-12-10   1.21.00   Bs       ESCAN00007142 after reception of SLEEP command 
|                                  the slave has to stop Tx handling at once
| 2004-03-29   1.22.00   Bs       ESCAN00007035 use range precopy function for NM message reception
|                                  delete unnecessary assertion defines
|                                 ESCAN00008002 support ECU addresses higher than 31 / 
|                                  delete unnecessary assertion defines and re-number the remaining defines
|                                 ESCAN00008043 change macro access to serve MISRA rules
|                                 ESCAN00008056 nmSendMess must be visible externally (MISRA message)
|                                 ESCAN00008162 add callback function for transistion E4
| 2004-04-21   1.23.00   Bs       ESCAN00008240 changes just in source file
|                                 ESCAN00008243 use compatibility defines just for master
|                                 ESCAN00008244 change precopy function for slaves from range to message precopy
|                                 ESCAN00008312 changes just in source file
| 2006-04-04   1.24.00   Bs       file header updated
|                                 assertion error codes explained
|                                 ESCAN00008563 unnecessary 'brace' deleted
|                                 ESCAN00007099 Organi comment added to put declaration of version number storage 
|                                  into a not code doubled section
|                                 ESCAN00010895 Naming Conventions: change version defines to serve Vector naming rules
|                                 ESCAN00014719 rework of organi switches _COMMENT
|                                 ESCAN00015971 Master: delete function 'NmSetMasterActiveLoad'
|                                 ESCAN00015997 Slave15: Add callback functions 'ApplNmCanNormal' and 'ApplNmCanSleep'
| 2006-07-06   1.25.00   Bs       ESCAN00016717 changes just in source file
|                                 ESCAN00016889 changes just in source file
|                                 ESCAN00016890 changes just in source file
| 2006-08-25   1.25.01   Krt      ESCAN00017438 changes just in source file
| 2008-01-07   1.26.00   visoh    ESCAN00018751 no changes
|                                 ESCAN00022957 BusOff handling in NetworkSilent for Slave 30
|                                 ESCAN00023955 no changes
| 2008-09-16   1.27.00   visoh    ESCAN00015996 Changed memory qualifier
|                                 ESCAN00026385 no changes
|                                 ESCAN00026888 no changes
|                                 ESCAN00030061 Adapted Fiat NM for GENy support
| 2008-12-08   1.28.00   visoh    ESCAN00031881 Added Support for "Physical Multiple ECU"
| 2009-01-30   1.29.00   visoh    ESCAN00032692 no changes
|                                 ESCAN00032717 Improved MISRA compliance
|                                 ESCAN00032775 no changes
|                                 ESCAN00032820 no changes
| 2010-01-26   1.30.00   visoh    ESCAN00040188 MISRA improvements
|                                 ESCAN00040387 No changes in this file
| 2010-06-21   1.30.01   visoh    ESCAN00043044 No changes in this file
|*****************************************************************************/

#ifndef  NM_FIAT_H
#define  NM_FIAT_H


/*****************************************************************************/
/* Include files                                                             */
/*****************************************************************************/
#include "v_def.h"                       /* Include common definition header */
#include "nm_cfg.h"                       /* Include NM configuration header */

/*****************************************************************************/
/* compatibility defines                                                     */
/*****************************************************************************/



/***************************************************************************/
/* Defines                                                                 */
/***************************************************************************/
/* ##V_CFG_MANAGEMENT ##CQProject : Nm_ClassB_Fiat CQComponent : Implementation */
#define NM_CLASSB_FIAT_VERSION         0x0130
#define NM_CLASSB_FIAT_RELEASE_VERSION   0x01

/* for compatibility to older versions */
#define NM_FIAT_VERSION         NM_CLASSB_FIAT_VERSION
#define NM_FIAT_BUGFIX_VERSION  NM_CLASSB_FIAT_RELEASE_VERSION

/* values of signal "active loads" */
#define kNmNoActiveLoads       0
#define kNmActiveLoads         1

/* values of signal "DLL status" */
#define kNmErrorActive         0 /* DLL = ACTIVE */
#define kNmErrorPassive        1 /* DLL = WARNING/PASSIVE */
#define kNmBusOff              2 /* DLL = BUSOFF */

/* values of signal "generic fail status" or "current fail status" */
#define kNmNoFunctionFail      0
#define kNmFunctionFail        1

/* values of signal "end of line status" */
#define kNmNoEolProgAtFIAT     0
#define kNmEolProgAtFIAT       1


/* values of NM node state*/
#define kNmPowerOff            0xFF
#define kNmNetOff              1
#define kNmNetOn               2



/***************************************************************************/
/* Supporting Indexed Systems                                              */
/***************************************************************************/
/* NM Types */
#if defined( NM_ENABLE_INDEXED_NM )
# error: "NmFiatB configuration issue: Indexed NM is not supported!"
#else
# if ( kNmNumberOfChannels > 1 )
#  error: "NmFiatB configuration issue: More than one NM channel non-indexed is not supported!"
# endif
#endif

/* CAN Types */
#if defined ( C_MULTIPLE_RECEIVE_CHANNEL )
# define NM_CHANNEL_CANTYPE_ONLY     CanChannelHandle channel
# define NM_CHANNEL_CANTYPE_FIRST    CanChannelHandle channel,
# define NM_CHANNEL_CANPARA_ONLY     channel
# define NM_CHANNEL_CANPARA_FIRST    channel,
#else
/* PRQA S 3460 1 */ /* 3460 Macro defines a type specifier keyword for code optimization reasons. */
# define NM_CHANNEL_CANTYPE_ONLY     void
# define NM_CHANNEL_CANTYPE_FIRST
# define NM_CHANNEL_CANPARA_ONLY
# define NM_CHANNEL_CANPARA_FIRST
#endif

#if defined ( C_MULTIPLE_RECEIVE_CHANNEL )
# if defined( NM_ENABLE_INDEXED_NM )
# else
#  if defined ( NM_CAN_CHANNEL )
#   define NM_NMTOCAN_CHANNEL_IND  NM_CAN_CHANNEL
#  else
#   define NM_NMTOCAN_CHANNEL_IND  0
#  endif
# endif
#else
# define NM_NMTOCAN_CHANNEL_IND
#endif



#if defined( NM_ENABLE_SOFTWARE_CHECK )
/***************************************************************************/
/* error codes for ApplNmFatalError                                        */
/***************************************************************************/
# define kNmErrorExternalStateReq         0x01  /* illegal external state request in precopy function */
# define kNmErrorInternalStateReq         0x02  /* illegal internal state request in 'NmSetNewMode' function */
# if defined( NM_TYPE_FIAT_15_SLAVE )
# else
#  define kNmErrorIllReqSwitchDef         0x03  /* not supported state in state machine (variable 'nmNodeState') */
#  define kNmErrorIllTimSwitchDef         0x04  /* not supported state in state machine (variable 'nmNodeState') */
# endif
#  define kNmErrorSetCurrFailState        0x05  /* illegal failure request in 'NmSetGenericFailState' function */
# define kNmErrorIllCanChnlHdl            0x06  /* illegal parameter in 'NmCanError' function */
# define kNmErrorIllTxConfHdl             0x07  /* illegal parameter in 'NmConfirmation' function */
# define kNmErrorSetEolState              0x08  /* illegal EOL state request in 'NmSetEolState' function */
# define kNmErrorSetGenFailState          0x09  /* illegal failure request in 'NmSetGenericFailState' function */
#endif /* #if defined( NM_ENABLE_SOFTWARE_CHECK ) */


/***************************************************************************/
/* Data types                                                              */
/***************************************************************************/


/****************************************************************************/
/* Constants                                                                */
/****************************************************************************/
/* global constants for main-, sub- and bugfix version */
V_MEMROM0 extern V_MEMROM1 vuint8 V_MEMROM2 kNmMainVersion;
V_MEMROM0 extern V_MEMROM1 vuint8 V_MEMROM2 kNmSubVersion;
V_MEMROM0 extern V_MEMROM1 vuint8 V_MEMROM2 kNmReleaseVersion;


/*****************************************************************************/
/* global data definitions                                                   */
/*****************************************************************************/
/* data buffer for NM message of slave ECU */
#  if defined ( VGEN_GENY )
/* declaration provided in drv_par.h: V_MEMRAM0 extern V_MEMRAM1_NEAR vuint8 V_MEMRAM2_NEAR nmSendMess[2]; */
#  else
extern vuint8 nmSendMess[2];
#  endif


/***************************************************************************/
/* function prototypes                                                     */
/***************************************************************************/
extern void NmInitPowerOn( void );
extern void NmInit( void );
extern void NmTask( void );


#if defined( NM_TYPE_FIAT_15_SLAVE ) 
/*
 * The prototypes of the service functions for the CAN driver are defined
 * by the CAN driver.
 * - NmCanError()
 * - NmConfirmation()
 * - NmPrecopy()
 */
#endif

extern void NmSetNewMode( vuint8 state );
extern void NmSetEolState( vuint8 state );
extern void NmSetGenericFailState( vuint8 state );
#if defined( NM_TYPE_FIAT_15_SLAVE ) 
extern void NmSetCurrentFailState ( vuint8 state );
#endif
extern vuint8 NmGetDllState( void );
extern vuint8 NmGetNetState( void );


# if defined( NM_ENABLE_BUSOFF_FCT )
extern void ApplNmBusOff(void);
# endif
# if defined( NM_ENABLE_BUSOFFEND_FCT )
extern void ApplNmBusOffEnd(void);
# endif

#if defined( NM_ENABLE_SOFTWARE_CHECK )
extern void ApplNmFatalError( vuint8 error );
#endif

extern void ApplNmCanNormal( void );
extern void ApplNmCanSleep( void );



#endif /* NM_FIAT_H */

/*  *****   STOPSINGLE_OF_MULTIPLE    *****  */
/************   Organi, Version 3.9.0 Vector-Informatik GmbH  ************/
