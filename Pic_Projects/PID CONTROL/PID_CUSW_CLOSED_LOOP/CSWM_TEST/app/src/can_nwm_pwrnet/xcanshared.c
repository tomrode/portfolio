/**************************************************************************************************
| Project Name: C A N - D R I V E R
|    File Name: XCANSHARED.C
|
|  Description: Implementation of the CAN driver - file for shared RAM area
|               Target systems: Freescale MCS12X
|               Compiler:       Cosmic
|                               Metrowerks
|--------------------------------------------------------------------------------------------------
|               C O P Y R I G H T
|--------------------------------------------------------------------------------------------------
|   Copyright (c) by Vector Informatik GmbH.     All rights reserved.
|
|   This software is copyright protected and proprietary to 
|   Vector Informatik GmbH. Vector Informatik GmbH grants to you 
|   only those rights as set out in the license conditions. All 
|   other rights remain with Vector Informatik GmbH.
|--------------------------------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|--------------------------------------------------------------------------------------------------
| Initials     Name                      Company
| --------     ---------------------     ----------------------------------------------------------
| Ml           Patrick Markl             Vector Informatik GmbH 
| Ou           Mihai Olariu              Vector Informatik GmbH
|--------------------------------------------------------------------------------------------------
|               R E V I S I O N   H I S T O R Y
|--------------------------------------------------------------------------------------------------
| Date       Version   Author  Description
| ---------  --------  ------  --------------------------------------------------------------------
| 2004-07-20 00.01.00    Ml     Creation
| 2004-10-14 01.00.00    Ml     Added support for Metrowerks
| 2004-11-21 01.01.00    Ml     ESCAN00010293: Store value of CIDAC
| 2005-02-06 02.00.00    Ml     Added transmission path
| 2005-08-26 02.01.00    Msr/Ml ESCAN00013352: Added synchronization
| 2005-12-20 02.01.00    Ml     ESCAN00014745: Added encapsulation for GENy
|                        Ml     ESCAN00014792: no changes
| 2006-04-10 02.02.00    Ml     ESCAN00015415: no changes
|                        Ml     ESCAN00016034: no changes
| 2006-06-06 02.03.00    Ml     ESCAN00016549: no changes
|                        Ml     ESCAN00016704: no changes
| 2007-01-21 02.04.00    Ml     ESCAN00018781: Fixed XGate search if just one receive message exists
|                        Ou     ESCAN00019728: Update for the multi ECU configuration support in XGate
| 2007-03-09 02.05.00    Ml     ESCAN00019893: Removed parts for transmission
| 2007-04-09 02.06.00    Ou     ESCAN00020104: Upgrade to RI 1.5
| 2007-09-04 02.07.00    Ou     ESCAN00021695: Messages received in range(s) are not processed
| 2008-08-20 02.08.00    Ou     ESCAN00027373: ID search is not further performed if at least one range was hit
| 2008-12-16 02.09.00    Ou     ESCAN00032051: Remove the inverted comma in the XGate code
|*************************************************************************************************/
#include "can_inc.h"
#include "xcan_def.h"

#if defined(C_ENABLE_XGATE_USED)

# if defined(C_ENABLE_RANGE_0) || defined(C_ENABLE_RANGE_1) || defined(C_ENABLE_RANGE_2) || defined(C_ENABLE_RANGE_3)
/* Rangeconfiguration */
V_MEMRAM0 V_MEMRAM1 tCanXGateChannelRangeCfg V_MEMRAM2 canRangeConfig[kCanNumberOfChannels];
/* shared variable to exchange the hit range */
V_MEMRAM0 V_MEMRAM1 vuint8 V_MEMRAM2 canXgHitRange[kCanNumberOfChannels];
# endif

/* shared variable to exchange the Rx message handle */
V_MEMRAM0 V_MEMRAM1 CanReceiveHandle V_MEMRAM2 canXgRxHandle[kCanNumberOfChannels];

/* temporary buffer for the currently received message */
V_MEMRAM0 V_MEMRAM1 tMsgObject V_MEMRAM2 canRxTmpBuf[kCanNumberOfChannels];

/* flag to synchronize the XGate Rx Irq and the CPU12 Rx interrupt */
V_MEMRAM0 V_MEMRAM1 volatile vuintx V_MEMRAM2 canRxSyncObj[kCanNumberOfChannels];



#endif
/************   Organi, Version 3.9.0 Vector-Informatik GmbH  ************/
/************   Organi, Version 3.9.0 Vector-Informatik GmbH  ************/
