/******   STARTSINGLE_OF_MULTIPLE    ******/

/*****************************************************************************
| Project Name: C A N - D R I V E R
|    File Name: can_isr.c
|
|  Description: Implemenation of the CAN driver - This file contains the
|               entry points of the CAN interrupt. This functions in this
|               file have to be located in the near page
|               
|
|-----------------------------------------------------------------------------
|               C O P Y R I G H T
|-----------------------------------------------------------------------------
| Copyright (c) 1996-2007 by Vector Informatik GmbH.       All rights reserved.
|
|   Alle Rechte an der Software verbleiben bei der Vector Informatik GmbH.
|   Vector Informatik GmbH raeumt dem Lizenznehmer das unwiderrufliche, 
|   geographisch und zeitlich nicht beschraenkte, jedoch nicht ausschlie�liche 
|   Nutzungsrecht innerhalb des Lizenznehmers ein. Die Weitergabe des 
|   Nutzungsrechts durch den Lizenznehmer ist auf dessen Zulieferer
|   beschraenkt. Die Zulieferer sind zu verpflichten, die Software nur im 
|   Rahmen von Projekten f�r den Lizenznehmer zu verwenden; weitere Rechte der 
|   Zulieferer sind auszuschlie�en.
|
|-----------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-----------------------------------------------------------------------------
| Initials     Name                      Company
| --------     ---------------------     -------------------------------------
| Ml           Patrick Markl             Vector Informatik GmbH 
| Ces          Senol Cendere             Vector Informatik GmbH 
|-----------------------------------------------------------------------------
|               R E V I S I O N   H I S T O R Y
|-----------------------------------------------------------------------------
| Date       Version   Author  Description
| ---------  --------  ------  -----------------------------------------------
| 2004-07-05 00.01.00    Ml     Creation 
| 2004-11-12 01.00.00    Ml     Codedoubling of functions added
| 2006-01-04 01.01.00    Ml     ESCAN00014793: Added pragmas for Metrowerks
| 2006-05-03 01.02.00    Ml     ESCAN00016237: Changed names of ISRs
| 2007-01-11 01.03.00    Ces    ESCAN00018770: Support indexed API for single channel configurations
|                        Ces    ESCAN00018928: Support Multi ECU configuration
| 2007-02-25 01.04.00    Ml     ESCAN00019569: Changed macros of ISRs in order to work with Autosar OS
| 2007-03-15 01.05.00    Ml     ESCAN00014095: logical and physical channel indices can be different
| 2007-04-04 01.06.00    Ces    ESCAN00020177: Adaptions for Autosar
| 2007-04-09 01.07.00    Ou     ESCAN00020104: Upgrade to RI 1.5
| 2007-09-04 01.08.00    Ou     Avoid compile error if a configuration without any Rx message is defined
| 2008-01-09 01.09.00    Ou     ESCAN00022514: Interrupts don't work with OSEK OS
|                        Ou     ESCAN00023567: ISRs should not be mapped in the default section for non-banked routines
|***************************************************************************/

#include "can_inc.h"               /* dependend configuration of the driver.*/

#if defined(C_ENABLE_ISR_SPLIT)

# if !defined(C_ENABLE_RX_BASICCAN_POLLING)
void CanRxIrqHandler(CAN_CHANNEL_CANTYPE_ONLY);
# endif
# if !defined(C_ENABLE_TX_POLLING)
void CanTxIrqHandler(CAN_CHANNEL_CANTYPE_ONLY);
# endif
# if !defined(C_ENABLE_WAKEUP_POLLING)
#  if defined(C_ENABLE_SLEEP_WAKEUP)
void CanWakeUpIrqHandler(CAN_CHANNEL_CANTYPE_ONLY);
#  endif
# endif
# if !defined(C_ENABLE_ERROR_POLLING)
void CanErrorIrqHandler(CAN_CHANNEL_CANTYPE_ONLY);
# endif

# if defined(CAN_TX_IRQ_FUNC_DECL)
# else
#  if (defined(C_ENABLE_TX_POLLING)) && (!defined(C_ENABLE_INDIVIDUAL_POLLING))
#    define CAN_TX_IRQ_FUNC_DECL(Func)
#  else
#   if defined(C_ENABLE_OSEK_OS) && defined(C_ENABLE_OSEK_OS_INTCAT2)
#    define CAN_TX_IRQ_FUNC(Func)
#    define CAN_TX_IRQ_FUNC_DECL(Func)
#   else
#    define CAN_TX_IRQ_FUNC(Func)          CAN_IRQ_FUNC_QUAL1 void CAN_IRQ_FUNC_QUAL2 Func(void)
#    define CAN_TX_IRQ_FUNC_DECL(Func)     CAN_TX_IRQ_FUNC(Func);
#   endif
#  endif
# endif

# if defined(CAN_RX_IRQ_FUNC_DECL)
# else
#  if (defined(C_ENABLE_RX_BASICCAN_POLLING)) && (!defined(C_ENABLE_INDIVIDUAL_POLLING))
#    define CAN_RX_IRQ_FUNC_DECL(Func)
#  else
#   if defined(C_ENABLE_OSEK_OS) && defined(C_ENABLE_OSEK_OS_INTCAT2)
#    define CAN_RX_IRQ_FUNC(Func)
#    define CAN_RX_IRQ_FUNC_DECL(Func)
#   else
#    define CAN_RX_IRQ_FUNC(Func)          CAN_IRQ_FUNC_QUAL1 void CAN_IRQ_FUNC_QUAL2 Func(void)
#    define CAN_RX_IRQ_FUNC_DECL(Func)     CAN_RX_IRQ_FUNC(Func);
#   endif
#  endif
# endif

# if defined(CAN_ERROR_IRQ_FUNC_DECL)
# else
#  if defined(C_ENABLE_ERROR_POLLING)
#    define CAN_ERROR_IRQ_FUNC_DECL(Func)
#  else
#   if defined(C_ENABLE_OSEK_OS) && defined(C_ENABLE_OSEK_OS_INTCAT2)
#    define CAN_ERROR_IRQ_FUNC(Func)
#    define CAN_ERROR_IRQ_FUNC_DECL(Func)
#   else
#    define CAN_ERROR_IRQ_FUNC(Func)       CAN_IRQ_FUNC_QUAL1 void CAN_IRQ_FUNC_QUAL2 Func(void)
#    define CAN_ERROR_IRQ_FUNC_DECL(Func)  CAN_ERROR_IRQ_FUNC(Func);
#   endif
#  endif
# endif

# if defined(CAN_WAKEUP_IRQ_FUNC_DECL)
# else
#  if defined(C_ENABLE_WAKEUP_POLLING)
#   define CAN_WAKEUP_IRQ_FUNC_DECL(Func)
#   error "Wakeup Polling Is Not Allowed!"
#  else
#   if defined(C_ENABLE_OSEK_OS) && defined(C_ENABLE_OSEK_OS_INTCAT2)
#    define CAN_WAKEUP_IRQ_FUNC(Func)
#    define CAN_WAKEUP_IRQ_FUNC_DECL(Func)
#   else
#    define CAN_WAKEUP_IRQ_FUNC(Func)      CAN_IRQ_FUNC_QUAL1 void CAN_IRQ_FUNC_QUAL2 Func(void)
#    define CAN_WAKEUP_IRQ_FUNC_DECL(Func) CAN_WAKEUP_IRQ_FUNC(Func);
#   endif
#  endif
# endif

/****************************************************************************
| NAME:             CanIsr
| CALLED BY:
| PRECONDITIONS:
| INPUT PARAMETERS: none
| RETURN VALUES:    none
| DESCRIPTION:      Interrupt service functions according to the CAN controller
|                   interrupt stucture
|                   - check for the interrupt reason ( interrupt source )
|                   - work appropriate interrupt:
|                     + status/error interrupt (busoff, wakeup, error warning)
|                     + basic can receive
|                     + full can receive
|                     + can transmit
|
|                   If an Rx-Interrupt occurs while the CAN controller is in Sleep mode, 
|                   a wakeup has to be generated. 
|
|                   If an Tx-Interrupt occurs while the CAN controller is in Sleep mode, 
|                   an assertion has to be called and the interrupt has to be ignored.
|
|                   The name of BrsTimeStrt...() and BrsTimeStop...() can be addapted to 
|                   realy used name of the interrupt functions.
|
|  ##Ht: description of actions for each kind of interrupt is missing
****************************************************************************/
# if ((defined( C_ENABLE_RX_BASICCAN_OBJECTS ) && !defined( C_ENABLE_RX_BASICCAN_POLLING )) || \
      !defined( C_ENABLE_TX_POLLING )        || \
       defined( C_ENABLE_INDIVIDUAL_POLLING ) || \
       !defined( C_ENABLE_ERROR_POLLING )     ||\
       !defined( C_ENABLE_WAKEUP_POLLING ))        /* ISR necessary; no pure polling configuration*/

#   if defined(kCanPhysToLogChannelIndex_0)


#    if (!defined(C_ENABLE_RX_BASICCAN_POLLING) && defined(C_ENABLE_RX_BASICCAN_OBJECTS)) || (defined(C_ENABLE_RX_BASICCAN_POLLING) && defined(C_ENABLE_INDIVIDUAL_POLLING) && defined(C_ENABLE_RX_BASICCAN_OBJECTS))
#     if defined(C_ENABLE_OSEK_OS) && defined(C_ENABLE_OSEK_OS_INTCAT2)
ISR(CanRxInterrupt_0)
#     else
CAN_RX_IRQ_FUNC(CanRxInterrupt_0)
#     endif
{
#     if defined(C_SINGLE_RECEIVE_CHANNEL)
  CanRxIrqHandler();
#     else
  CanRxIrqHandler((CanChannelHandle)kCanPhysToLogChannelIndex_0);
#     endif
}
#    endif



#    if (!defined(C_ENABLE_TX_POLLING)) || (defined(C_ENABLE_TX_POLLING) && defined(C_ENABLE_INDIVIDUAL_POLLING))
#     if defined(C_ENABLE_OSEK_OS) && defined(C_ENABLE_OSEK_OS_INTCAT2)
ISR(CanTxInterrupt_0)
#     else
CAN_TX_IRQ_FUNC(CanTxInterrupt_0)
#     endif
{
#     if defined(C_SINGLE_RECEIVE_CHANNEL)
  CanTxIrqHandler();
#     else
  CanChannelHandle channel;
  channel = (CanChannelHandle)kCanPhysToLogChannelIndex_0;
  CanTxIrqHandler(channel);
#     endif
}
#    endif



#    if defined(C_ENABLE_SLEEP_WAKEUP)
#     if defined(C_ENABLE_OSEK_OS) && defined(C_ENABLE_OSEK_OS_INTCAT2)
ISR(CanWakeUpInterrupt_0)
#     else
CAN_WAKEUP_IRQ_FUNC(CanWakeUpInterrupt_0)
#     endif
{
#     if defined(C_SINGLE_RECEIVE_CHANNEL)
  CanWakeUpIrqHandler();
#     else
  CanWakeUpIrqHandler((CanChannelHandle)kCanPhysToLogChannelIndex_0);
#     endif
}
#    endif



#    if !defined(C_ENABLE_ERROR_POLLING)
#     if defined(C_ENABLE_OSEK_OS) && defined(C_ENABLE_OSEK_OS_INTCAT2)
ISR(CanErrorInterrupt_0)
#     else
CAN_ERROR_IRQ_FUNC(CanErrorInterrupt_0)
#     endif
{
#     if defined(C_SINGLE_RECEIVE_CHANNEL)
  CanErrorIrqHandler();
#     else
  CanErrorIrqHandler((CanChannelHandle)kCanPhysToLogChannelIndex_0);
#     endif
}
#    endif


#   endif

#   if defined(kCanPhysToLogChannelIndex_1)


#    if (!defined(C_ENABLE_RX_BASICCAN_POLLING) && defined(C_ENABLE_RX_BASICCAN_OBJECTS)) || (defined(C_ENABLE_RX_BASICCAN_POLLING) && defined(C_ENABLE_INDIVIDUAL_POLLING) && defined(C_ENABLE_RX_BASICCAN_OBJECTS))
#     if defined(C_ENABLE_OSEK_OS) && defined(C_ENABLE_OSEK_OS_INTCAT2)
ISR(CanRxInterrupt_1)
#     else
CAN_RX_IRQ_FUNC(CanRxInterrupt_1)
#     endif
{
#     if defined(C_SINGLE_RECEIVE_CHANNEL)
  CanRxIrqHandler();
#     else
  CanRxIrqHandler((CanChannelHandle)kCanPhysToLogChannelIndex_1);
#     endif
}
#    endif



#    if (!defined(C_ENABLE_TX_POLLING)) || (defined(C_ENABLE_TX_POLLING) && defined(C_ENABLE_INDIVIDUAL_POLLING))
#     if defined(C_ENABLE_OSEK_OS) && defined(C_ENABLE_OSEK_OS_INTCAT2)
ISR(CanTxInterrupt_1)
#     else
CAN_TX_IRQ_FUNC(CanTxInterrupt_1)
#     endif
{
#     if defined(C_SINGLE_RECEIVE_CHANNEL)
  CanTxIrqHandler();
#     else
  CanChannelHandle channel;
  channel = (CanChannelHandle)kCanPhysToLogChannelIndex_1;
  CanTxIrqHandler(channel);
#     endif
}
#    endif



#    if defined(C_ENABLE_SLEEP_WAKEUP)
#     if defined(C_ENABLE_OSEK_OS) && defined(C_ENABLE_OSEK_OS_INTCAT2)
ISR(CanWakeUpInterrupt_1)
#     else
CAN_WAKEUP_IRQ_FUNC(CanWakeUpInterrupt_1)
#     endif
{
#     if defined(C_SINGLE_RECEIVE_CHANNEL)
  CanWakeUpIrqHandler();
#     else
  CanWakeUpIrqHandler((CanChannelHandle)kCanPhysToLogChannelIndex_1);
#     endif
}
#    endif



#    if !defined(C_ENABLE_ERROR_POLLING)
#     if defined(C_ENABLE_OSEK_OS) && defined(C_ENABLE_OSEK_OS_INTCAT2)
ISR(CanErrorInterrupt_1)
#     else
CAN_ERROR_IRQ_FUNC(CanErrorInterrupt_1)
#     endif
{
#     if defined(C_SINGLE_RECEIVE_CHANNEL)
  CanErrorIrqHandler();
#     else
  CanErrorIrqHandler((CanChannelHandle)kCanPhysToLogChannelIndex_1);
#     endif
}
#    endif


#   endif

#   if defined(kCanPhysToLogChannelIndex_2)


#    if (!defined(C_ENABLE_RX_BASICCAN_POLLING) && defined(C_ENABLE_RX_BASICCAN_OBJECTS)) || (defined(C_ENABLE_RX_BASICCAN_POLLING) && defined(C_ENABLE_INDIVIDUAL_POLLING) && defined(C_ENABLE_RX_BASICCAN_OBJECTS))
#     if defined(C_ENABLE_OSEK_OS) && defined(C_ENABLE_OSEK_OS_INTCAT2)
ISR(CanRxInterrupt_2)
#     else
CAN_RX_IRQ_FUNC(CanRxInterrupt_2)
#     endif
{
#     if defined(C_SINGLE_RECEIVE_CHANNEL)
  CanRxIrqHandler();
#     else
  CanRxIrqHandler((CanChannelHandle)kCanPhysToLogChannelIndex_2);
#     endif
}
#    endif



#    if (!defined(C_ENABLE_TX_POLLING)) || (defined(C_ENABLE_TX_POLLING) && defined(C_ENABLE_INDIVIDUAL_POLLING))
#     if defined(C_ENABLE_OSEK_OS) && defined(C_ENABLE_OSEK_OS_INTCAT2)
ISR(CanTxInterrupt_2)
#     else
CAN_TX_IRQ_FUNC(CanTxInterrupt_2)
#     endif
{
#     if defined(C_SINGLE_RECEIVE_CHANNEL)
  CanTxIrqHandler();
#     else
  CanChannelHandle channel;
  channel = (CanChannelHandle)kCanPhysToLogChannelIndex_2;
  CanTxIrqHandler(channel);
#     endif
}
#    endif



#    if defined(C_ENABLE_SLEEP_WAKEUP)
#     if defined(C_ENABLE_OSEK_OS) && defined(C_ENABLE_OSEK_OS_INTCAT2)
ISR(CanWakeUpInterrupt_2)
#     else
CAN_WAKEUP_IRQ_FUNC(CanWakeUpInterrupt_2)
#     endif
{
#     if defined(C_SINGLE_RECEIVE_CHANNEL)
  CanWakeUpIrqHandler();
#     else
  CanWakeUpIrqHandler((CanChannelHandle)kCanPhysToLogChannelIndex_2);
#     endif
}
#    endif



#    if !defined(C_ENABLE_ERROR_POLLING)
#     if defined(C_ENABLE_OSEK_OS) && defined(C_ENABLE_OSEK_OS_INTCAT2)
ISR(CanErrorInterrupt_2)
#     else
CAN_ERROR_IRQ_FUNC(CanErrorInterrupt_2)
#     endif
{
#     if defined(C_SINGLE_RECEIVE_CHANNEL)
  CanErrorIrqHandler();
#     else
  CanErrorIrqHandler((CanChannelHandle)kCanPhysToLogChannelIndex_2);
#     endif
}
#    endif


#   endif

#   if defined(kCanPhysToLogChannelIndex_3)


#    if (!defined(C_ENABLE_RX_BASICCAN_POLLING) && defined(C_ENABLE_RX_BASICCAN_OBJECTS)) || (defined(C_ENABLE_RX_BASICCAN_POLLING) && defined(C_ENABLE_INDIVIDUAL_POLLING) && defined(C_ENABLE_RX_BASICCAN_OBJECTS))
#     if defined(C_ENABLE_OSEK_OS) && defined(C_ENABLE_OSEK_OS_INTCAT2)
ISR(CanRxInterrupt_3)
#     else
CAN_RX_IRQ_FUNC(CanRxInterrupt_3)
#     endif
{
#     if defined(C_SINGLE_RECEIVE_CHANNEL)
  CanRxIrqHandler();
#     else
  CanRxIrqHandler((CanChannelHandle)kCanPhysToLogChannelIndex_3);
#     endif
}
#    endif



#    if (!defined(C_ENABLE_TX_POLLING)) || (defined(C_ENABLE_TX_POLLING) && defined(C_ENABLE_INDIVIDUAL_POLLING))
#     if defined(C_ENABLE_OSEK_OS) && defined(C_ENABLE_OSEK_OS_INTCAT2)
ISR(CanTxInterrupt_3)
#     else
CAN_TX_IRQ_FUNC(CanTxInterrupt_3)
#     endif
{
#     if defined(C_SINGLE_RECEIVE_CHANNEL)
  CanTxIrqHandler();
#     else
  CanChannelHandle channel;
  channel = (CanChannelHandle)kCanPhysToLogChannelIndex_3;
  CanTxIrqHandler(channel);
#     endif
}
#    endif



#    if defined(C_ENABLE_SLEEP_WAKEUP)
#     if defined(C_ENABLE_OSEK_OS) && defined(C_ENABLE_OSEK_OS_INTCAT2)
ISR(CanWakeUpInterrupt_3)
#     else
CAN_WAKEUP_IRQ_FUNC(CanWakeUpInterrupt_3)
#     endif
{
#     if defined(C_SINGLE_RECEIVE_CHANNEL)
  CanWakeUpIrqHandler();
#     else
  CanWakeUpIrqHandler((CanChannelHandle)kCanPhysToLogChannelIndex_3);
#     endif
}
#    endif



#    if !defined(C_ENABLE_ERROR_POLLING)
#     if defined(C_ENABLE_OSEK_OS) && defined(C_ENABLE_OSEK_OS_INTCAT2)
ISR(CanErrorInterrupt_3)
#     else
CAN_ERROR_IRQ_FUNC(CanErrorInterrupt_3)
#     endif
{
#     if defined(C_SINGLE_RECEIVE_CHANNEL)
  CanErrorIrqHandler();
#     else
  CanErrorIrqHandler((CanChannelHandle)kCanPhysToLogChannelIndex_3);
#     endif
}
#    endif


#   endif

#   if defined(kCanPhysToLogChannelIndex_4)


#    if (!defined(C_ENABLE_RX_BASICCAN_POLLING) && defined(C_ENABLE_RX_BASICCAN_OBJECTS)) || (defined(C_ENABLE_RX_BASICCAN_POLLING) && defined(C_ENABLE_INDIVIDUAL_POLLING) && defined(C_ENABLE_RX_BASICCAN_OBJECTS))
#     if defined(C_ENABLE_OSEK_OS) && defined(C_ENABLE_OSEK_OS_INTCAT2)
ISR(CanRxInterrupt_4)
#     else
CAN_RX_IRQ_FUNC(CanRxInterrupt_4)
#     endif
{
#     if defined(C_SINGLE_RECEIVE_CHANNEL)
  CanRxIrqHandler();
#     else
  CanRxIrqHandler((CanChannelHandle)kCanPhysToLogChannelIndex_4);
#     endif
}
#    endif



#    if (!defined(C_ENABLE_TX_POLLING)) || (defined(C_ENABLE_TX_POLLING) && defined(C_ENABLE_INDIVIDUAL_POLLING))
#     if defined(C_ENABLE_OSEK_OS) && defined(C_ENABLE_OSEK_OS_INTCAT2)
ISR(CanTxInterrupt_4)
#     else
CAN_TX_IRQ_FUNC(CanTxInterrupt_4)
#     endif
{
#     if defined(C_SINGLE_RECEIVE_CHANNEL)
  CanTxIrqHandler();
#     else
  CanChannelHandle channel;
  channel = (CanChannelHandle)kCanPhysToLogChannelIndex_4;
  CanTxIrqHandler(channel);
#     endif
}
#    endif



#    if defined(C_ENABLE_SLEEP_WAKEUP)
#     if defined(C_ENABLE_OSEK_OS) && defined(C_ENABLE_OSEK_OS_INTCAT2)
ISR(CanWakeUpInterrupt_4)
#     else
CAN_WAKEUP_IRQ_FUNC(CanWakeUpInterrupt_4)
#     endif
{
#     if defined(C_SINGLE_RECEIVE_CHANNEL)
  CanWakeUpIrqHandler();
#     else
  CanWakeUpIrqHandler((CanChannelHandle)kCanPhysToLogChannelIndex_4);
#     endif
}
#    endif



#    if !defined(C_ENABLE_ERROR_POLLING)
#     if defined(C_ENABLE_OSEK_OS) && defined(C_ENABLE_OSEK_OS_INTCAT2)
ISR(CanErrorInterrupt_4)
#     else
CAN_ERROR_IRQ_FUNC(CanErrorInterrupt_4)
#     endif
{
#     if defined(C_SINGLE_RECEIVE_CHANNEL)
  CanErrorIrqHandler();
#     else
  CanErrorIrqHandler((CanChannelHandle)kCanPhysToLogChannelIndex_4);
#     endif
}
#    endif


#   endif


# endif

#endif
/******   STOPSINGLE_OF_MULTIPLE    ******/
/************   Organi, Version 3.9.0 Vector-Informatik GmbH  ************/
/************   Organi, Version 3.9.0 Vector-Informatik GmbH  ************/
