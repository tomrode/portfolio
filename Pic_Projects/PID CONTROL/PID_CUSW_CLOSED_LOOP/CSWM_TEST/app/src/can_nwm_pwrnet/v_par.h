/* -----------------------------------------------------------------------------
  Filename:    v_par.h
  Description: Toolversion: 03.01.09.01.01.01.51.01.00.00
               
               Serial Number: CBD1010151
               Customer Info: Continental_Temic
                              Package: Chrysler Fiat_Sgl_SLP3_ClassB_Clmp15 
                              Micro: Freescale MC9S12XS128 
                              Compiler: Cosmic 4.7.8
               
               
               Generator Fwk   : GENy 
               Generator Module: GenTool_GenyVcfgNameDecorator
               
               Configuration   : H:\SCM_DCX_CSWM_MY_12\Can_Nwm_PwrNet\cmd\CUSW_SLP3_1138DBC_DDT60_009_101211.gny
               
               ECU: 
                       TargetSystem: Hw_Mcs12xCpu
                       Compiler:     Cosmic
                       Derivates:    MCS12X
               
               Channel "Channel0":
                       Databasefile: H:\SCM_DCX_CSWM_MY_12\Documents\chrysler-Data_dict\PPF_E2B_1138_BCAN.dbc
                       Bussystem:    CAN
                       Manufacturer: Fiat
                       Node:         CSWM

  Generated by , 2011-12-01  08:37:03
 ----------------------------------------------------------------------------- */
/* -----------------------------------------------------------------------------
  C O P Y R I G H T
 -------------------------------------------------------------------------------
  Copyright (c) 2001-2010 by Vector Informatik GmbH. All rights reserved.
 
  This software is copyright protected and proprietary to Vector Informatik 
  GmbH.
  
  Vector Informatik GmbH grants to you only those rights as set out in the 
  license conditions.
  
  All other rights remain with Vector Informatik GmbH.
 -------------------------------------------------------------------------------
 ----------------------------------------------------------------------------- */

#if !defined(__V_PAR_H__)
#define __V_PAR_H__

/* -----------------------------------------------------------------------------
    &&&~ BaseEnv_PHF_Includes
 ----------------------------------------------------------------------------- */

#include "v_cfg.h"
#include "v_def.h"


/* -----------------------------------------------------------------------------
    &&&~ GENy Version Information
 ----------------------------------------------------------------------------- */

#define VGEN_DELIVERY_VERSION_BYTE_0         0x03
#define VGEN_DELIVERY_VERSION_BYTE_1         0x01
#define VGEN_DELIVERY_VERSION_BYTE_2         0x09
#define VGEN_DELIVERY_VERSION_BYTE_3         0x01
#define VGEN_DELIVERY_VERSION_BYTE_4         0x01
#define VGEN_DELIVERY_VERSION_BYTE_5         0x01
#define VGEN_DELIVERY_VERSION_BYTE_6         0x51
#define VGEN_DELIVERY_VERSION_BYTE_7         0x01
#define VGEN_DELIVERY_VERSION_BYTE_8         0x00
#define VGEN_DELIVERY_VERSION_BYTE_9         0x00
#define kGENyVersionNumberOfBytes            10
/* ROM CATEGORY 4 START */
V_MEMROM0 extern  V_MEMROM1 vuint8 V_MEMROM2 kGENyVersion[kGENyVersionNumberOfBytes];
/* ROM CATEGORY 4 END */



typedef struct tDBCVersionTag
{
  vuint8 kYear;
  vuint8 kMonth;
  vuint8 kWeek;
  vuint8 kDay;
  vuint32 kNumber;
} tDBCVersion;
V_MEMROM0 extern  V_MEMROM1 tDBCVersion V_MEMROM2 kDBCVersion[1];

/* begin Fileversion check */
#ifndef SKIP_MAGIC_NUMBER
#ifdef MAGIC_NUMBER
  #if MAGIC_NUMBER != 82318335
      #error "The magic number of the generated file <H:\SCM_DCX_CSWM_MY_12\Documents\GENy Generated\1138_DDT_009\v_par.h> is different. Please check time and date of generated files!"
  #endif
#else
  #define MAGIC_NUMBER 82318335
#endif  /* MAGIC_NUMBER */
#endif  /* SKIP_MAGIC_NUMBER */

/* end Fileversion check */

#endif /* __V_PAR_H__ */
