/* Kernbauer Version: 1.16 Konfiguration: can_driver Erzeugungsgangnummer: 1 */


/**********************************************************************************************************************
| Project Name: C A N - D R I V E R
|    File Name: XCAN_DRV.C
|
|  Description: Implementation of the CAN driver
|               Target systems: Freescale MCS12X - XGate part
|               Compiler:       Cosmic
|                               Metrowerks
|----------------------------------------------------------------------------------------------------------------------
|               C O P Y R I G H T
|----------------------------------------------------------------------------------------------------------------------
|   Copyright (c) by Vector Informatik GmbH.     All rights reserved.
|
|   This software is copyright protected and proprietary to 
|   Vector Informatik GmbH. Vector Informatik GmbH grants to you 
|   only those rights as set out in the license conditions. All 
|   other rights remain with Vector Informatik GmbH.
|----------------------------------------------------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|----------------------------------------------------------------------------------------------------------------------
| Initials     Name                      Company
| --------     ---------------------     ------------------------------------------------------------------------------
| Ml           Patrick Markl             Vector Informatik GmbH 
| Msr          Martin Schlodder          Vector Informatik GmbH 
| Ou           Mihai Olariu              Vector Informatik GmbH
|----------------------------------------------------------------------------------------------------------------------
|               R E V I S I O N   H I S T O R Y
|----------------------------------------------------------------------------------------------------------------------
| Date       Version   Author  Description
| ---------  --------  ------  ----------------------------------------------------------------------------------------
| 2004-07-05 00.01.00    Ml     Creation 
| 2004-10-14 01.00.00    Ml     Added support for Metrowerks
| 2004-11-21 01.01.00    Ml     ESCAN00010293: Store value of CIDAC
| 2005-10-09 01.02.00    Ml     ESCAN00013822: rx handle is stored channel specific
|                        Ml     ESCAN00013823: Range is called correctly
| 2005-10-31 02.00.00    Ml     Added transmission path
|                        Msr/Ml ESCAN00013352: Added synchronization
|                        Ml     Split into high and low level driver
| 2005-12-20 02.01.00    Ml     ESCAN00014745: Added encapsulation for GENy
|                        Ml     ESCAN00014792: XGate search works now also with mixed IDs
| 2006-04-10 02.02.00    Ml     ESCAN00015415: no changes
|                        Ml     ESCAN00016034: Encapsulated C_MASK_EXT_ID
| 2006-06-06 02.03.00    Ml     ESCAN00016549: Moved semaphore handling to low level part of driver
|                        Ml     ESCAN00016704: Semaphores are handled using intrinsic functions
| 2007-01-21 02.04.00    Ml     ESCAN00018781: Fixed XGate search if just one receive message exists
|                        Ou     ESCAN00019728: Update for the multi ECU configuration support in XGate
| 2007-03-09 02.05.00    Ml     ESCAN00019893: Removed parts for transmission
| 2007-04-09 02.06.00    Ou     ESCAN00020104: Upgrade to RI 1.5
|                        Ou     ESCAN00020163: Apply the erratum workaround regarding "SSEM instruction"
| 2007-09-04 02.07.00    Ou     ESCAN00019366: Receive messages are ignored by the CAN driver
|                        Ou     ESCAN00021695: Messages received in range(s) are not processed
| 2008-08-20 02.08.00    Ou     ESCAN00027373: ID search is not further performed if at least one range was hit
| 2008-12-16 02.09.00    Ou     ESCAN00032051: Remove the inverted comma in the XGate code
|*********************************************************************************************************************/
#include "can_inc.h"

#if defined(C_ENABLE_XGATE_USED)
# if defined(C_COMP_COSMIC_MCS12X_MSCAN12)
#  include <processor.h>  /* used for semaphore handling */
# endif


/*********************************************************************************************************************/
/* Version check                                                                                                     */
/*********************************************************************************************************************/
# if(DRVCAN_MCS12XXGATEEXT_VERSION != 0x0209)
#  error "Source and Header file are inconsistent!"
# endif
# if(DRVCAN_MCS12XXGATEEXT_RELEASE_VERSION != 0x00)
#  error "Source and Header file are inconsistent!"
# endif

/*********************************************************************************************************************/
/* Defines                                                                                                           */
/*********************************************************************************************************************/
# define XGCRFLG            (((tCanCntrlRegBlock MEMORY_CAN*)pmsCAN) -> CanCRFLG)
# define XGCAN_RX_BUF_PTR   ((tMsgObject MEMORY_CAN*)&(((tCanCntrlRegBlock MEMORY_CAN*)(pmsCAN)) -> CanRxBuf))
# define XGCAN_RX_ID        (((tMsgObject MEMORY_CAN*)&(((tCanCntrlRegBlock MEMORY_CAN*)(pmsCAN)) -> CanRxBuf)) -> Id)
# define XGCAN_RX_ID_TYPE   (XGCAN_RX_ID & kCanIdTypeExt)

# define XG_RXF            ((vuint8)0x01)    /* Receive buffer full */


#  define subContext context

# if defined(C_COMP_COSMIC_MCS12X_MSCAN12)
#  define CAN_ADDRESS_PTR         context.pCanAddress
#  define RX_SEARCH_START_INDEX   subContext.nRxSearchStartIndex
#  define RX_SEARCH_STOP_INDEX    subContext.nRxSearchStopIndex
#  define LOG_CHANNEL             subContext.nLogChannel
# endif


# if defined(C_ENABLE_RANGE_0) || defined(C_ENABLE_RANGE_1) || \
     defined(C_ENABLE_RANGE_2) || defined(C_ENABLE_RANGE_3)
#  define C_RANGE_ACTIVE
# endif


#if defined(C_COMP_COSMIC_MCS12X_MSCAN12)
# define kCanXGateCanSemaphore 0x00
#endif

/*********************************************************************************************************************/
/* Internal types                                                                                                    */
/*********************************************************************************************************************/


/*********************************************************************************************************************/
/* Internal LookUp tables                                                                                            */
/*********************************************************************************************************************/


/*********************************************************************************************************************/
/* Internal data                                                                                                     */
/*********************************************************************************************************************/
V_MEMRAM0 V_MEMRAM1 tCanXGateRxInfoStruct V_MEMRAM2 canXgRxInfoStruct;

/*********************************************************************************************************************/
/* Functions                                                                                                         */
/*********************************************************************************************************************/

/**********************************************************************************************************************
Name          : CanXGateRxInterrupt
Preconditions : Receive interrrupt was fired
Parameters    : context: pointer to an context for the ISR
Returnvalue   : -
Description   : CAN Rx ISR, if the Rx interrupt is catched by the XGate
**********************************************************************************************************************/
C_XG_IRQ_1 void C_XG_IRQ_2 CanXGateRxInterrupt(C_XG_IRQ_ARG_TYPE context) C_XG_IRQ_3
{
  tCanCntrlRegBlock* pmsCAN;
  CanTransmitHandle nTmpRxHandle;
  tCanRxId0 id;                   /* temporary storage of the received identifier */
#if(kCanNumberOfRxObjects > 1)  
  vsint16   nRight, nLeft, nMid;  /* used for binary search */
#endif
  vuint8  bFireCpuIrq;            /* decides if the IRQ is dispatched to the CPU */
# if defined(C_RANGE_ACTIVE) 
  vuintx i;
# endif
  vuint8 tmpCanXgHitRange = (vuint8)0x00;


  pmsCAN       = (tCanCntrlRegBlock*)CAN_ADDRESS_PTR;
  bFireCpuIrq  = kCanFalse;
  nTmpRxHandle = (CanReceiveHandle)0xFFFF; /* reset current receive handle */
  id           = XGCAN_RX_ID;              /* read received CAN identifier */

# if defined(C_ENABLE_EXTENDED_ID)
#  if defined(C_ENABLE_MIXED_ID)
  if(XGCAN_RX_ID_TYPE == kCanIdTypeStd)
  {
    id &= (vuint32)0xFFF80000;
  }
  else
#  endif
  {
#  if defined(C_ENABLE_RX_MASK_EXT_ID)
    id &= MK_EXTID0(C_MASK_EXT_ID);
#  endif
  }
# endif

/**********************************************************************************************************************/
/* Range handling                                                                                                     */
/**********************************************************************************************************************/
# if defined(C_RANGE_ACTIVE)
  for(i = 0; i < 4; i++)
  {
#  if !defined(C_SINGLE_RECEIVE_CHANNEL)
    if(canRangeConfig[LOG_CHANNEL].nActiveMask & (vuint8)(1 << i))
#  endif
    {
      if((tCanRxId0)(id & canRangeConfig[LOG_CHANNEL].nMask[i]) == canRangeConfig[LOG_CHANNEL].nCode[i])
      {
        tmpCanXgHitRange |= (vuint8)(1 << i);
      }
    }
  }
  /* check if there was a range hit */
  if(tmpCanXgHitRange != (vuint8)0x00)
  {
    /* range are always handled by the CPU12! */
    bFireCpuIrq = kCanTrue;
  }
# endif /* C_ENABLE_RANGE_[0..3] */

/*********************************************************************************************************************/
/* Software filtering                                                                                                */
/*********************************************************************************************************************/
#if(kCanNumberOfRxObjects == 1)
  if(id == CanXGateRxId0[RX_SEARCH_START_INDEX])
  {
    nTmpRxHandle = RX_SEARCH_START_INDEX;
  }
#else
  /* Binary search for the received message handle */
  nLeft  = RX_SEARCH_START_INDEX;
  nRight = RX_SEARCH_STOP_INDEX - 1;

  while(nLeft < nRight)
  {
    nMid = (nLeft + nRight) >> 1;

    if(id == CanXGateRxId0[nMid])
    {
      nTmpRxHandle = nMid;
      break; /* handle found -> exit loop */
    }

    if(id < CanXGateRxId0[nMid])
    {
      nLeft = nMid;
    }
    else /* id > canRxId0Ram[nMid] ? */
    {
      nRight = nMid;
    }

    if((nRight - nLeft) == 1)
    {
      if(id == CanXGateRxId0[nLeft])
      {
        nTmpRxHandle = nLeft;
        break;
      }
      else if(id == CanXGateRxId0[nRight])
      { 
        nTmpRxHandle = nRight;
        break;
      }
      else
      {
        break;
      }
    }
  }
#endif

/**********************************************************************************************************************/
/* Call Precopy function in XGate                                                                                     */
/**********************************************************************************************************************/
# if defined(VGEN_GENY) && defined(C_ENABLE_XGATE_PRECOPY_FCT)
  if((V_NULL != CanXGateApplPrecopyFctPtr[nTmpRxHandle]) && (nTmpRxHandle != (vuint16)0xFFFF))
  {
    canXgRxInfoStruct.nChannel    = LOG_CHANNEL;
    canXgRxInfoStruct.pChipMsgObj = (CanChipMsgPtr)XGCAN_RX_BUF_PTR;
    canXgRxInfoStruct.nHandle     = nTmpRxHandle;

    if((kCanCopyData == CanXGateApplPrecopyFctPtr[nTmpRxHandle](&canXgRxInfoStruct)) && (kCanFalse == bFireCpuIrq))
    {
      bFireCpuIrq = kCanTrue;
    }
  }
# else
  if(nTmpRxHandle != (vuint16)0xFFFF)
  {
    bFireCpuIrq = kCanTrue;
  }
# endif

/**********************************************************************************************************************/
/* Rx Interrupt Epilogue                                                                                              */
/**********************************************************************************************************************/
#if defined(C_ENABLE_NOT_MATCHED_FCT) || defined (C_ENABLE_RECEIVE_FCT)
  bFireCpuIrq = kCanTrue;
# endif
  if(kCanFalse == bFireCpuIrq)
  {
    /* The message is not dispatched to CPU - just acknowledge the pending IRQ */
    XGCRFLG = XG_RXF;
  }
  else
  {
    while((vuintx)0x0001 == canRxSyncObj[LOG_CHANNEL])
    { 
      ; /* wait until CPU12 Rx ISR allows for further processing */
    } 
    
    #if defined(C_COMP_COSMIC_MCS12X_MSCAN12)
    /* So far there is no risk as only the XGate lock this semaphore but the application could try to lock another semaphore, then a workaround is needed: 
       According to errata sheet: execute two consecutive "SSEM" instructions to set a semaphore. Ignore result of the first "SSEM" instruction. 
       Carry-Flag will indicate correctly whether XGATE has allocated the semaphore after the second "SSEM" instruction is executed. */
    do
    {
      _ssem(kCanXGateCanSemaphore);
    }
    while(!_ssem(kCanXGateCanSemaphore));
    #endif
    
    canRxSyncObj[LOG_CHANNEL] = (vuintx)0x0001;                       /* CPU12 has to acknowledge this event */
    /* The CPU12 has to do some postprocessing - copy now all the local data to global memory */
# if defined(C_RANGE_ACTIVE)
    canXgHitRange[LOG_CHANNEL] = tmpCanXgHitRange;
# endif
    canXgRxHandle[LOG_CHANNEL] = nTmpRxHandle;
    canRxTmpBuf[LOG_CHANNEL]  = *(XGCAN_RX_BUF_PTR); /* copy rx data to temp buffer */
    XGCRFLG  = XG_RXF;                               /* acknowledge the receive irq */
    #if defined(C_COMP_COSMIC_MCS12X_MSCAN12)
    _csem(kCanXGateCanSemaphore);
    #endif

    #if defined(C_COMP_COSMIC_MCS12X_MSCAN12)
    # asm
    sif; XGate part finished -> posthandling of the IRQ is done in the CPU12
    # endasm
    #endif
  }
}


#endif


/************   Organi, Version 3.9.0 Vector-Informatik GmbH  ************/
/************   Organi, Version 3.9.0 Vector-Informatik GmbH  ************/
