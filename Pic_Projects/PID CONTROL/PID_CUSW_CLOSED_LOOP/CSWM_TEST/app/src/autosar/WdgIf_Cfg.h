/* Created at 15:25 on 1-4-2009 */
/* Version of HALCoGen used : 2.9.3 */
/* Code generated for       : MC9S12XS128 */
//****************************************************************************/
/* Copyright(c) - This Program/Software is the exclusive property of Infosys.
The software may not be reproduced or given to third parties without prior
consent of Infosys.

     Filename                      : WdgIf_Cfg.h
     Version                       : 1.0.1
     Microcontroller Platform      : MC9S12XS128
     Compiler                      : Codewarrior HCS12X V5.7.0

     Description                   : Header file for WDG Configuration

     Requirement Specification     : AUTOSAR_SWS_WatchdogInterface.pdf 2.0

     Module Design                 : AUTOTMAL_DDD_WDGIF_SPAL2.0.doc version 1.00

     Platform Dependant[yes/no]    : no

     To be changed by user[yes/no] : no
*/
/*****************************************************************************/

/* Revision History

Class of Change
-------------------------------------------------------------------------------
N - New Module.
F - Functional development.
E - Error Correction.
O - Optimization without functional change.
-------------------------------------------------------------------------------
 00      03-Feb-2006     Arindam Gupta, Infosys
-------------------------------------------------------------------------------
Class: N
Cause: New Module
Detail:This is header file for the configuration of Watchdog Interface.
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
 01      28-Mar-2006     Arindam Gupta, Infosys
-------------------------------------------------------------------------------
Class: N
Cause: Changes done for SPAL 2.0
Detail:The changes are done as per AUTOSAR_SWS_WatchdogInterface.pdf 2.0
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
 02      09-Aug-2006     Arindam Gupta, Infosys
-------------------------------------------------------------------------------
Class: O
Cause: Symbolic names of drivers defined
Detail:Symbolic Names of driver defined with the Device Index values  
-------------------------------------------------------------------------------
******************************************************************************/


/* to avoid Multiple inclusion */
#ifndef _WDGIF_CFG_H
#define _WDGIF_CFG_H

/*...................................Macros..................................*/

/*...................Configuration Description File..........................*/

/* Define if development error has to be reported */
#define WDGIF_DEV_ERROR_DETECT 										          OFF

/* Symbolic Names of the Watchdog Drivers added */
#define WDG_INTERNAL_INDEX                                  0 
#define WDGIF_NUMBER_OF_DEVICES                             1


#endif /* end _WDGIF_CFG_H */
/*.............................END OF WdgIf_Cfg.h.............................*/

