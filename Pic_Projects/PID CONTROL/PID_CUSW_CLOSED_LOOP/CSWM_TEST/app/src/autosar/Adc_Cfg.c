/* Created at 15:24 on 1-4-2009 */
/* Version of HALCoGen used : 2.9.3 */
/* Code generated for       : MC9S12XS128 */
/*************************************************************************/
/* Copyright(c) - This Program/Software is the exclusive property of Infosys.
The software may not be reproduced or given to third parties without prior
consent of Infosys.

     Filename                      : Adc_Cfg.c
     Version                       : 3.0.1
     Microcontroller Platform      : MC9S12XS128
     Compiler                      : Codewarrior HCS12X V5.0.28

     Description                   : This file contains the initialisation
                                     values of configuration parameters.

     Requirement Specification     : AUTOSAR_SWS_ADC_Driver.pdf version 1.0.16

     Module Design                 : AUTOSAR_DDD_ADC_SPAL2.0.doc version 2.11

     Platform Dependant[yes/no]    : yes

     To be changed by user[yes/no] : no
*/

/*****************************************************************************/

/* Revision History

Class of Change
------------------------------------------------------------------------------
N - New Module.
F - Functional development.
E - Error Correction.
O - Optimization without functional change.
------------------------------------------------------------------------------
 00     04.01.2006      Kapil Kumar, Infosys
-----------------------------------------------------------------------------
Class:  N
Cause:  New Module
Detail: This file contains the configured values of configuration structure..
-----------------------------------------------------------------------------
-------------------------------------------------------------------------------
 01     21-Aug-2006     Karthik, Infosys
-------------------------------------------------------------------------------
Class:  E
Cause:  Lint Analysis 
Detail: 
-------------------------------------------------------------------------------
 02     8-Dec-2006      Kasinath Hegde, Infosys
-------------------------------------------------------------------------------
Class:  F
Cause:  Software Changes for S12XEP100 
Detail: Configuration parameters modified for S12XEP100
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 03     23-Mar-2007     Rajesh, Infosys
-----------------------------------------------------------------------------
Class:  E
Cause:  Bug Fixes - 530, 565, 676, 552, 682, 683, 686
Detail: Size of the ADC Gated Buffer is determined as (Gated Samples * No. of
channels in that group), to hold the conversion values of all samples.
-----------------------------------------------------------------------------

******************************************************************************/

/* Implements this Interface */
#include "Adc.h"
/* Module Interface header */
#include "AdcIf_Cbk.h"

const Adc_GroupConfigType Adc_GroupData[ADC_CONFIGURED_GROUPS] = 
{
  /*Group 0 */
  {
		ADC_HW_UNIT_0,
		ADC_TRIGGER_SOURCE_0, 
		ADC_CONV_MODE_0, 
		{
			ADC_GROUP_LENGTH_0, 
			ADC_STARTING_CHANNEL_0, 
			ADC_GROUPRESOLUTION_0, 
			ADC_SAMPLE_TIME_0,
			ADC_WRAP_AROUND_0 ,
			ADC_DISCHARGE_BEF_SAMPLE_0 
		},
  },
	
  /*Group 1 */
  {
		ADC_HW_UNIT_1,
		ADC_TRIGGER_SOURCE_1, 
		ADC_CONV_MODE_1, 
		{
			ADC_GROUP_LENGTH_1, 
			ADC_STARTING_CHANNEL_1, 
			ADC_GROUPRESOLUTION_1, 
			ADC_SAMPLE_TIME_1,
			ADC_WRAP_AROUND_1 ,
			ADC_DISCHARGE_BEF_SAMPLE_1 
		},
  },
	
  /*Group 2 */
  {
		ADC_HW_UNIT_2,
		ADC_TRIGGER_SOURCE_2, 
		ADC_CONV_MODE_2, 
		{
			ADC_GROUP_LENGTH_2, 
			ADC_STARTING_CHANNEL_2, 
			ADC_GROUPRESOLUTION_2, 
			ADC_SAMPLE_TIME_2,
			ADC_WRAP_AROUND_2 ,
			ADC_DISCHARGE_BEF_SAMPLE_2 
		},
  },
	
  /*Group 3 */
  {
		ADC_HW_UNIT_3,
		ADC_TRIGGER_SOURCE_3, 
		ADC_CONV_MODE_3, 
		{
			ADC_GROUP_LENGTH_3, 
			ADC_STARTING_CHANNEL_3, 
			ADC_GROUPRESOLUTION_3, 
			ADC_SAMPLE_TIME_3,
			ADC_WRAP_AROUND_3 ,
			ADC_DISCHARGE_BEF_SAMPLE_3 
		},
  },
	
  /*Group 4 */
  {
		ADC_HW_UNIT_4,
		ADC_TRIGGER_SOURCE_4, 
		ADC_CONV_MODE_4, 
		{
			ADC_GROUP_LENGTH_4, 
			ADC_STARTING_CHANNEL_4, 
			ADC_GROUPRESOLUTION_4, 
			ADC_SAMPLE_TIME_4,
			ADC_WRAP_AROUND_4 ,
			ADC_DISCHARGE_BEF_SAMPLE_4 
		},
  },
	
  /*Group 5 */
  {
		ADC_HW_UNIT_5,
		ADC_TRIGGER_SOURCE_5, 
		ADC_CONV_MODE_5, 
		{
			ADC_GROUP_LENGTH_5, 
			ADC_STARTING_CHANNEL_5, 
			ADC_GROUPRESOLUTION_5, 
			ADC_SAMPLE_TIME_5,
			ADC_WRAP_AROUND_5 ,
			ADC_DISCHARGE_BEF_SAMPLE_5 
		},
  },
	
  /*Group 6 */
  {
		ADC_HW_UNIT_6,
		ADC_TRIGGER_SOURCE_6, 
		ADC_CONV_MODE_6, 
		{
			ADC_GROUP_LENGTH_6, 
			ADC_STARTING_CHANNEL_6, 
			ADC_GROUPRESOLUTION_6, 
			ADC_SAMPLE_TIME_6,
			ADC_WRAP_AROUND_6 ,
			ADC_DISCHARGE_BEF_SAMPLE_6 
		},
  },
	
  /*Group 7 */
  {
		ADC_HW_UNIT_7,
		ADC_TRIGGER_SOURCE_7, 
		ADC_CONV_MODE_7, 
		{
			ADC_GROUP_LENGTH_7, 
			ADC_STARTING_CHANNEL_7, 
			ADC_GROUPRESOLUTION_7, 
			ADC_SAMPLE_TIME_7,
			ADC_WRAP_AROUND_7 ,
			ADC_DISCHARGE_BEF_SAMPLE_7 
		},
  }
	
  
};
const Adc_ConfigType Adc_Config[ADC_MULTIPLE_CONFIGURATIONS] =
{
    /* 0- ADC settings */   
    {
        {
            ADC0_PRESCALE_0
        },
        {
            ADC0_FREEZEMODE_0
        },
        {
            ADC0_STOPCONV_MODE_0
        },
        &Adc_GroupData[0]
    }
    

};

  
