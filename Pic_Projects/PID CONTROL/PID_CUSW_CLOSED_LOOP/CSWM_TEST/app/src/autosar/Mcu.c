/* Created at 15:25 on 1-4-2009 */
/* Version of HALCoGen used : 2.9.3 */
/* Code generated for       : MC9S12XS128 */

/*****************************************************************************/
/* Copyright(c) - This Program/Software is the exclusive property of Infosys.
The software may not be reproduced or given to third parties without prior
consent of Infosys.

   Filename                        : Mcu.c
   Version                         : 4.3.9
   Microcontroller Platform        : MC9S12XS128
   Compiler                        : Codewarrior HCS12X V5.7.0

   Description                     :This file contains function definitions
                                    of all functions of MCU Driver Module.
                                    It includes:
                                    1)  Mcu_Init
                                    2)  Mcu_InitRamSection
                                    3)  Mcu_InitClock
                                    4)  Mcu_DistributePllClock
                                    5)  Mcu_GetPllStatus
                                    6)  Mcu_PerformReset
                                    7)  Mcu_SetMode
                                    8)  Mcu_GetResetReason
                                    9)  Mcu_GetVersionInfo
                                    10) Mcu_TimerInit

   Requirement Specification       : AUTOSAR_SWS_McuDriver.doc ver 1.2.0

   Module Design                   : AUTOSAR_DDD_MCU_SPAL2.0.doc ver 2.10a

   Platform Dependant[yes/no]      : yes

   To be changed by user[yes/no]   : yes
*/

/*****************************************************************************/

/* Revision History

Class of Change
-------------------------------------------------------------------------------
N - New Module.
F - Functional development.
E - Error Correction.
O - Optimization without functional change.
-------------------------------------------------------------------------------
 00     02-Feb-2006     Kapil Kumar, Infosys
-------------------------------------------------------------------------------
Class: N
Cause: New Module
Detail:This file implements the MCU driver.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 01     30-Mar-2006     Kapil Kumar, Infosys
-------------------------------------------------------------------------------
Class: F
Cause: Changes for SPAL2.0
Detail:Changes done as per AUTOSAR_SWS_McuDriver.pdf ver 1.2.0
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 02     09-Jun-2006     Harini KE, Infosys
-------------------------------------------------------------------------------
Class: E
Cause: Std_types.h changed,Issues from customer, Anomalies reported from RTRT
Detail:Code modified for the following reasons
       1.Std_VersionInfoType structure is modified in Std_Types.h 
       2.points reported and accepted in Problem report list 
       3.points reported and accepted in RTRT anomaly report list
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 03     10-Jul-2006     Harini KE, Infosys
-------------------------------------------------------------------------------
Class: F
Cause: Macro conversion, and functional change for some APIs, Banked memory 
       model support,Removing _H from the version info of header file
Detail:Code modified for the following reasons-
       1.Implementation of macros for functions Mcu_DistributePllclock,
         Mcu_GetPllStatus, Mcu_PerformReset and Mcu_GetResetReason
       2.Mcu_PerformReset now does an illegal Address Reset 
       3.Mcu_InitRamSection now supports both Banked and Large memory model
       4.Removed _H from the version info of header file
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 04     11-Aug-2006     Harini KE, Infosys
-------------------------------------------------------------------------------
Class: E
Cause: COP Reset not working, Full stop mode not functional 
Detail:Code modified for the following reasons
       1. A call to _Startup is made in Mcu_COPWatchdog_Isr
       2. Mcu_ClkMonitor_Isr and Mcu_COPWatchdog_Isr are updated as reset vectors
       3. Dem call added in Mcu_Init
       4. Register setting corrected in Mcu_Init
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
 05     11-Dec-2006     Anwar Husen, Infosys
-------------------------------------------------------------------------------
Class: N
Cause: New Module
Detail:This file is updated for the derivative MC9S12XEP100 for the MCU driver.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 06     11-Jan-2007     Anwar Husen, Infosys
-------------------------------------------------------------------------------
Class: F
Cause: FM0,FM1 bit setting
Detail:For each Clock setting there will be a separate Frequency modulation ratio
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 07     30-Jan-2007     Anwar Husen, Infosys
-------------------------------------------------------------------------------
Class: F
Cause: Change request #609 in PRL
Detail:Pre-processor checks are added to implement the Change request #609.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 08     23-Mar-2007     Anwar Husen, Infosys
-------------------------------------------------------------------------------
Class: O
Cause: Common implementation across derivatives
Detail:If DET is not selected, then Mcu_DistributePllClock  API/APIMACRO will 
       select the PLL clock as system clock.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 09     26-Mar-2007     Rose Paul , Infosys
-------------------------------------------------------------------------------
Class : E
Cause : DISABLE was defined as 0, but it is also used by the preprocessor
        (e.g. #pragma MESSAGE DISABLE ...).The preprocessor directive
        does not work after the inclusion of MCAL_types.h.
        PRL 689- Lint warning 502
Detail: Macros in Mcal_Types prefixed by "MCAL_".
        Suffixed hardcoded values,in expressions with unary operator,with 'u'
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 10     26-Mar-2007     Rose Paul , Infosys
-------------------------------------------------------------------------------
Class : E
Cause : DISABLE was defined as 0, but it is also used by the preprocessor
        (e.g. #pragma MESSAGE DISABLE ...).The preprocessor directive
        does not work after the inclusion of MCAL_types.h.
        PRL 689- Lint warning 502
Detail: Macros in Mcal_Types prefixed by "MCAL_".
        Suffixed hardcoded values,in expressions with unary operator,with 'u'
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 11     29-Mar-2007     Khanindra , Infosys
-------------------------------------------------------------------------------
Class : F
Cause : Implementation of Freescale security recommendation
Detail: Two Isrs Mcu_SystemFailure_Isr and Mcu_SystemFailureAnalyse_Isr are 
        added. Mcu_Init,Mcu_InitClock and Mcu_SetMode APIs are updated.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 12     02-Apr-2007     Khanindra , Infosys
-------------------------------------------------------------------------------
Class : O
Cause : After Incorporating Code review comments
Detail: Mcu_Init,Mcu_InitClock,Mcu_SystemFailure_Isr and 
        Mcu_SystemFailureAnalyse_Isr are updated accordingly.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 13     16-May-2007     Khanindra , Infosys
-------------------------------------------------------------------------------
Class : F
Cause : Updated Mcu_InitClock for Freescale security recommendation
Detail: Updated Mcu_InitClock for Freescale security recommendation
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 14     24-Aug-2007     Khanindra , Infosys
-------------------------------------------------------------------------------
Class : F
Cause : In accordance with the issue #746
Detail: Mcu_Init() and Mcu_InitClock() is updated accordingly
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 15     17-Oct-2007     Prashant Kulkarni , Infosys
-------------------------------------------------------------------------------
Class : F
Cause : To support XS128 microcontroller
Detail: Code modifications done so as to support XS128 microcontroller. TIM 
        and ECT are used synonomously
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 16     27-Nov-2007     Khanindra , Infosys
-------------------------------------------------------------------------------
Class : E
Cause : In accordance with the issue #842
Detail: Mcu_SystemFailure_Isr() and Mcu_SystemFailureAnalyse_Isr() are updated 
        accordingly.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 17     03-Jan-2008     Khanindra , Infosys
-------------------------------------------------------------------------------
Class : E
Cause : In accordance with the issues #818,#888
Detail: #818: Mcu_TimerInit is updated for the register TSCR2.
        #888: RTI is disabled in Mcu_SetMode if RTI is Disabled in HALCoGen 
              under low power mode.              
        Also Global variable Mcu_PowerOnWakeUp is declared to handle Power On
        Wake Up reason. APIs Mcu_Init, Mcu_SetMode and ISR Mcu_RTI_Interrupt_Isr
        are updated accordingly.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 18     01-Apr-2008     Khanindra , Infosys
-------------------------------------------------------------------------------
Class : E
Cause : In accordance with the issues #954 and #978
Detail: #954: Mcu_Init is updated. PowerON Reset check is done first. In 
              PowerON Reset, NO_INIT variables are initialized.
        #978: Precompiler check for MCU_SW_PATCH_VERSION is removed.
-------------------------------------------------------------------------------

******************************************************************************/

/*...................................File Includes...........................*/

/* Module header */
#include "Mcu.h"


/*...................................Macros..................................*/

#define MCU_SW_MAJOR_VERSION_C                     4
#define MCU_SW_MINOR_VERSION_C                     3
#define MCU_SW_PATCH_VERSION_C                     9
#define MCU_AR_MAJOR_VERSION_C                     1
#define MCU_AR_MINOR_VERSION_C                     2
#define MCU_AR_PATCH_VERSION_C                     0

#define    ECT_TSCR1           TSCR1
#define    ECT_TSCR2           TSCR2
#define    ECT_PTPSR           PTPSR
#define    ECT_TSCR2_TCRE      TSCR2_TCRE
#define    ECT_TSCR2_PR_MASK   TSCR2_PR_MASK
/*...............................version Check...............................*/
/* Do version checking before compilation */
#if (MCU_SW_MAJOR_VERSION==MCU_SW_MAJOR_VERSION_C)
#if (MCU_SW_MINOR_VERSION==MCU_SW_MINOR_VERSION_C)
#if (MCU_AR_MAJOR_VERSION==MCU_AR_MAJOR_VERSION_C)
#if (MCU_AR_MINOR_VERSION==MCU_AR_MINOR_VERSION_C)
#if (MCU_AR_PATCH_VERSION==MCU_AR_PATCH_VERSION_C)


/*.................................Local Macro definition ...................*/


/* ..........................Global variables................................*/

/* These global variables have external linkages */
/* This variable to store reset reason.Reset reason will be cleared in 
 Mcu_Init and this variable will hold the reason */
volatile Mcu_ResetType Mcu_Reset_Reason = MCU_POWER_ON_RESET;
#pragma DATA_SEG MCU_DATA_RAM
volatile uint8 Mcu_SWResetFlag=FALSE;
#pragma DATA_SEG DEFAULT


/* These global variables have internal linkages */


/* Define a local static variable to access UserConfig elements in Mcu
   functions */
/* Flags for SW reset,COP reset and Clock failure 
   reside in NO_INIT segment */
#pragma DATA_SEG MCU_DATA_RAM 
static volatile uint8 Mcu_COPResetFlag=FALSE;
static volatile uint8 Mcu_ClockMonitorResetFlag=FALSE;

#pragma DATA_SEG DEFAULT
static const Mcu_ConfigType *Mcu_UserConfig_Ptr;

/* Static variable to store reset reason.Reset reason will be cleared in 
   Mcu_Init and this variable will hold the reason */


/* store clock settings, for use in PLL ISR */
static volatile uint8 Mcu_ClkSettings;
/* Mcu Power On Wake Up Flag */
static volatile uint8 Mcu_PowerOnWakeUp = FALSE;

/* Set Oscillator Fault to disbale CAN communication */
extern @far void IntFlt_F_Set_Osci_Err(void);

/* Startup function declaration */
void _Startup(void);

/*.........................END Local static variable declaration ............*/


/*..........................API Definitions..................................*/

/******************************************************************************
* Function Name              : Mcu_Init
*
* Arguments                  : Mcu_ConfigType*
*                              The parameter is a pointer 
*                              to the configuration that shall be initialized.
*                              It contains user defined configuration for MCU 
*                              clock, MCU modes and RAM Sections.   
*
* Description                : This function Initializes global vriables
*                              and static variables.
*
* Return Type                : void
*
* Requirement numbers        : MCU026
*****************************************************************************/


void Mcu_Init(const Mcu_ConfigType *ConfigPtr )
{


    /*Copy configptr to a local static variable: to be used by other APIs 
    in MCU*/  
    Mcu_UserConfig_Ptr = ConfigPtr;

    /* Store the reset reason in a variable and clear reset flags */
    /*Check if PowerON  Reset occured */
    if(TRUE== CRGFLG_PORF)
    {
        Mcu_Reset_Reason = MCU_POWER_ON_RESET;
        CRGFLG = CRGFLG_PORF_MASK;
        Mcu_PowerOnWakeUp = TRUE;
        /* Initialize the flags for SW reset,COP reset and Clock failure */
        Mcu_ClockMonitorResetFlag = FALSE;
        Mcu_COPResetFlag          = FALSE;
        Mcu_SWResetFlag           = FALSE;
        
    }
    else
    {
        /* Check if Clock monitor reset occured */
        if(TRUE == Mcu_ClockMonitorResetFlag)
        {    
            Mcu_Reset_Reason = MCU_CLOCK_MONITOR_RESET;
            Mcu_ClockMonitorResetFlag = FALSE;
        }
	     /* Check if COP reset occured */
	     if(TRUE == Mcu_COPResetFlag)
	     {
            Mcu_Reset_Reason = MCU_WATCHDOG_RESET;
            Mcu_COPResetFlag = FALSE;
        }
        /*Check if Illegal Address Reset occured */
        if((TRUE == Mcu_SWResetFlag) && (TRUE == CRGFLG_ILAF))
        {
            Mcu_Reset_Reason = MCU_SW_RESET;
            Mcu_SWResetFlag = FALSE;
            CRGFLG_ILAF = TRUE;
        }
        else if(TRUE == CRGFLG_ILAF)
        {
            Mcu_Reset_Reason = MCU_ILLEGAL_ADDRESS_RESET;
            CRGFLG_ILAF = TRUE;
        } 
        
        /*Check if Low Voltage Reset occured */
        if(TRUE== CRGFLG_LVRF)
        {
            Mcu_Reset_Reason =MCU_LOW_VOLTAGE_RESET;
            CRGFLG = CRGFLG_LVRF_MASK;
        }
    }
    

}
/******************************************************************************
* Function Name              : Mcu_InitRamSection
*
* Arguments                  : Mcu_RamSectionType(uint8)
*                              The parameter is the RAM section no. that is to
*                              be initialised
*
*
* Description                : This function Initializes the particular
                               RAM section as given in configuration structure.
*
* Return Type                : Std_ReturnType
*
* Requirement numbers        : MCU011
*****************************************************************************/


Std_ReturnType Mcu_InitRamSection(const Mcu_RamSectionType RamSection )
{

    uint8 *__far ramptr;
    uint32 index;


    /* initialize index and RamPtr to point to RAM sector base address */
    index = 0;
    ramptr = (uint8 *__far)(Mcu_UserConfig_Ptr->MCU_RAM_SECTOR_SETTING
                                           [RamSection].Mcu_RamSect_BaseAddr);


    /*Set RAM to Preset value */
    while(index < (Mcu_UserConfig_Ptr->MCU_RAM_SECTOR_SETTING
                                       [RamSection].Mcu_RamSect_Size))
    {
        *(ramptr++) = Mcu_UserConfig_Ptr->MCU_RAM_SECTOR_SETTING
                                          [RamSection].Mcu_RamSect_Preset;
        index++;

    }

    return E_OK;

}
/******************************************************************************
* Function Name              : Mcu_InitClock
*
* Arguments                  : ClockSetting(uint8)
*                              The parameter is the Clock settings no. in
                               configuration structurethat is to be initialised.
*
*
* Description                : This function Initializes the clock with
                               particular settings as given in configuration
                               structure.
*
* Return Type                : Std_ReturnType
*
* Requirement numbers        : MCU009
*****************************************************************************/


Std_ReturnType Mcu_InitClock(const Mcu_ClockType ClockSetting )
{

    uint16 mcu_count = 0xFFFF; 
    /* Check for Self Clock Mode */
    do
    {
        mcu_count--;
        if (0 == mcu_count)
        {
            /* Perform SW-Reset */
            Mcu_PerformReset();
        }
    }
    while (0 != (CRGFLG & 0x01));

    /* store clock setting for use in SCM ISR */
    Mcu_ClkSettings =ClockSetting;


    /* If timer is configured */
    Mcu_TimerInit(ClockSetting);

    /* Clear  CLKSEL_PLLSEL bit to deselect the PLL*/ 
    CLKSEL_PLLSEL = FALSE;

    /*IPLL Frequency Modulation Enable Bit — FM1 and FM0 enable additional 
    frequency modulation on the VCOCLK.*/
    PLLCTL_FM = Mcu_UserConfig_Ptr->MCU_CLOCK_SETTING[ClockSetting].Mcu_PLLCTLFM_Value;

    /* Disable the interrupt */
    CRGINT_LOCKIE = FALSE;

    /* ON the PLL by setting PLLON */
    PLLCTL_PLLON =TRUE;

    /*Initialise CRG Synthesizer Register as per the configuration  */
    SYNR =Mcu_UserConfig_Ptr->MCU_CLOCK_SETTING[ClockSetting].Mcu_SYNR_RegValue;

    /*Initialise CRG Reference Divider Register as per the configuration  */
    REFDV=Mcu_UserConfig_Ptr->MCU_CLOCK_SETTING
    [ClockSetting].Mcu_REFDV_RegValue;

    /*Initialise CRG Reference Divider Register as per the configuration  */
    POSTDIV=Mcu_UserConfig_Ptr->MCU_CLOCK_SETTING
    [ClockSetting].Mcu_POSTDIV_RegValue;

    /*Enable/Disable Interrupt for SCM/RTI/LOCK */
    CRGINT =  Mcu_UserConfig_Ptr->MCU_CRGINT.Byte;
    return E_OK;

}
/******************************************************************************
* Function Name              : Mcu_SetMode
*
* Arguments                  : Mcu_ModeType : mode no. configured in config
                               structure(0..2)
*
* Description                : This function sets the Low power modes for
                               RTI/COP and PLL and then put the system in
                               the configured low power mode
*
* Return Type                : void
*
* Requirement numbers        : MCU001
*****************************************************************************/


void Mcu_SetMode(const Mcu_ModeType McuMode)
{





    switch(Mcu_UserConfig_Ptr->MCU_MODE_SETTING[McuMode].Mcu_PowerMode)
    {

        case  MCU_POWER_MODE_WAIT:

                   /*Set Low power mode in  CLKSEL register ( wait mode for
                   PLL/RTI ->these could be enabled/disabled during wait
                   mode,based on the configuration) */
                    CLKSEL_PLLWAI =  ~(Mcu_UserConfig_Ptr->MCU_MODE_SETTING
                                     [McuMode].Mcu_PLL_Enable_WaitMode);

                    CLKSEL_RTIWAI =  ~(Mcu_UserConfig_Ptr->MCU_MODE_SETTING
                                     [McuMode].Mcu_RTI_Enable_WaitMode);
    

                    if(MCAL_DISABLE == Mcu_UserConfig_Ptr->MCU_MODE_SETTING
                            [McuMode].Mcu_PLL_Enable_WaitMode)
                    {
                        CRGINT_LOCKIE = FALSE;  
                    }
            
            /* Disable the RTI Interrupt if RTI is Disabled through 
            HALCoGen */
            if(MCAL_DISABLE == Mcu_UserConfig_Ptr->MCU_MODE_SETTING
                                     [McuMode].Mcu_RTI_Enable_WaitMode)
            {
                CRGINT_RTIE = FALSE;  
            }            


                    StartWaitMode ;

        break;


        case  MCU_POWER_MODE_PSEUDO_STOP:

                    /*Set mode in PLLCTL register (Stop mode for RTI and COP ->
                    these could be enabled/disabled during Pseudo stop  mode,
                    based on the configuration) */
                    PLLCTL_PCE =  Mcu_UserConfig_Ptr->MCU_MODE_SETTING
                                  [McuMode].Mcu_COP_Enable_PseudoStopMode;

                    PLLCTL_PRE =  Mcu_UserConfig_Ptr->MCU_MODE_SETTING
                                  [McuMode].Mcu_RTI_Enable_PseudoStopMode;

                    CLKSEL_PSTP = TRUE;       /* Oscillator keeps running */

                    CRGINT_LOCKIE = FALSE;

                    StartStopMode ;

        break;


        case  MCU_POWER_MODE_STOP:

                    CLKSEL_PSTP = FALSE;       /* Oscillator disabled */

                    CRGINT_LOCKIE = FALSE;

                    StartStopMode;

        break;


        default :
        break;

    }


        /* Provide Power ON wake up reason to the ECU state manager */
        if(TRUE == Mcu_PowerOnWakeUp)
        {
            ECUM_WKSOURCE_POWER = 1;
            EcuM_SetWakeUpEvent(WakeupSource);
        }



}
/*****************************************************************************
* Function Name              : Mcu_GetVersionInfo
*
* Arguments                  : Std_VersionInfoType *versioninfo 
*
* Description                : This API stores the version information of the 
                               MCU module in the variable pointed by the pointer
                               passed to the function. The version information 
                               include
                               1.Module ID
                               2.Version ID
                               3.Vendor specific version numbers.
*
* Return Type                : void
*
* Requirement numbers        : MCU103
******************************************************************************/


void Mcu_GetVersionInfo(Std_VersionInfoType *versioninfo)
{

    if(NULL_PTR != versioninfo)
	{
		/* Copy MODULE ID */
    	versioninfo->moduleID=MCU_MODULE_ID;
    	/* Copy VENDOR ID */
    	versioninfo->vendorID=MCU_VENDOR_ID;
    	/* Cope SW vendor version info. */
    	versioninfo->sw_major_version=MCU_SW_MAJOR_VERSION_C;
    	versioninfo->sw_minor_version=MCU_SW_MINOR_VERSION_C;
    	versioninfo->sw_patch_version=MCU_SW_PATCH_VERSION_C;
	}


}
/******************************************************************************
* Function Name              : Mcu_TimerInit
*
* Arguments                  : Mcu_ClockType
*                             The parameter is the Clock settings no. in 
                              configuration structurethat is to be initialised.
*
* Description                : This function Initializes the timer registers.
*
* Return Type                : void
*
* Requirement numbers        : MCU009
*****************************************************************************/


void Mcu_TimerInit(const Mcu_ClockType ClockSetting) 
{



/* ..................... ECT/ Port T Hw Settings .................... */
/* 3rd Bit is PRNT = Legacy/Precision Timer bit */
if(ECT_LEGACY_TIMER ==
  (Mcu_UserConfig_Ptr->Mcu_ExtSetting[ClockSetting].Mcu_EctHwSettings.all
  & (MCAL_BIT3_MASK)))
{
        ECT_TSCR2 &= ~ECT_TSCR2_PR_MASK;
/* Set prescaler for legacy timer */
ECT_TSCR2|=(Mcu_UserConfig_Ptr->Mcu_ExtSetting[ClockSetting].Mcu_Prescaler);
}
else
{
/* Set prescaler for precision timer */
ECT_PTPSR=Mcu_UserConfig_Ptr->Mcu_ExtSetting[ClockSetting].Mcu_Prescaler;
}
/************************ TSCR1 SETTINGS **********************************
ICU060 Set to Normal Mode
[1] Set the ECT Peripheral in Normal mode
[2] Allows the timer flag clearing to function normally
[3] Set Timer type- Legacy/Precision
[4] Set Wait bit according to User Configuration
[5] Set Freeze bit according to User Configuration
 **************************** PORT P **************************************/
ECT_TSCR1|=Mcu_UserConfig_Ptr->Mcu_ExtSetting[ClockSetting].Mcu_EctHwSettings.
     all;
/*In TSCR2- Reset TCRE bit */
ECT_TSCR2_TCRE = FALSE;


}
#pragma CODE_SEG __NEAR_SEG NON_BANKED

/******************************************************************************
* Function Name              : Mcu_SelfClockMode_Isr
*
* Arguments                  : void
*
* Description                : This ISR is invoked when the system
                               enters self clock mode.Error is reported to DEM.
*
* Return Type                : void
*
* Requirement numbers        : MCU054
*******************************************************************************/

void Mcu_SelfClockMode_Isr(void)
{

    /*Clear SCMIF bit */
    CRGFLG = CRGFLG_SCMIF_MASK;
    
    /* ADDED THIS FUNCTION CALL TO DISABLE CAN COMMUNICATION AND TO SET AN INTERNAL FAULT */
    IntFlt_F_Set_Osci_Err();
}

/******************************************************************************
* Function Name              : Mcu_PllLockInterrupt_Isr
*
* Arguments                  : void
*
* Description                : System will generate PLL Lock Interrupt when the
                               LOCK condition of PLL has changed, either from 
                               a locked state to unlocked state or vice-versa.

* Return Type                : void
*
* Requirement numbers        : 
*******************************************************************************/

void Mcu_PllLockInterrupt_Isr (void)     
{
    if (!CRGFLG_LOCK) /* if PLL not locked */
    {
        (void)Mcu_InitClock(Mcu_ClkSettings); /* start PLL again */
    } 

    /*make PLL clock as system clock if lock has occured */
    else
    {
        Mcu_DistributePllClock();
    }

    /* clear LOCKIF flag */
    CRGFLG = CRGFLG_LOCKIF_MASK; 
}

/******************************************************************************
* Function Name              : Mcu_COPWatchdog
*
* Arguments                  : void
*
* Description                : System will generate COP reset vector when there
                               is a COP reset.
*
* Return Type                : void
*
* Requirement numbers        : 
*******************************************************************************/

void Mcu_COPWatchdog (void)     
{
   
    Mcu_COPResetFlag = TRUE; 
    _Startup();
    
}

/******************************************************************************
* Function Name              : Mcu_ClkMonitor
*
* Arguments                  : void
*
* Description                : System will generate Clockmontior vector when there
                               is a clock failure.
*
* Return Type                : void
*
* Requirement numbers        : 
*******************************************************************************/

void Mcu_ClkMonitor (void)     
{
   
    Mcu_ClockMonitorResetFlag = TRUE; 
    _Startup();
    
}


#pragma CODE_SEG DEFAULT


/******************************************************************************/

#else
   #error " Mismatch in AR Patch Version of Mcu.c & Mcu.h "
#endif

#else
   #error " Mismatch in AR Minor Version of Mcu.c & Mcu.h "
#endif

#else
   #error " Mismatch in AR Major Version of Mcu.c & Mcu.h "
#endif

#else
   #error " Mismatch in SW Minor Version of Mcu.c & Mcu.h "
#endif

#else
   #error " Mismatch in SW Major Version of Mcu.c & Mcu.h "
#endif

/* End of version check */

/*...............................END OF Mcu.C................................*/


