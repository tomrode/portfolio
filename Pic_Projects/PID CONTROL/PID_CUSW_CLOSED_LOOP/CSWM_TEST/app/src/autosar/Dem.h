/* Created at 15:25 on 1-4-2009 */
/* Version of HALCoGen used : 2.9.3 */
/* Code generated for       : MC9S12XS128 */

/*****************************************************************************/
/* Copyright(c) - This Program/Software is the exclusive property of Infosys.
The software may not be reproduced or given to third parties without prior
consent of Infosys.

     Filename                      : Dem.h
     Version                       : 1.0.0
     Microcontroller Platform      : MC9S12XS128
     Compiler                      : Codewarrior HCS12X V5.7.0

     Description                   : This file contains common types, macros
                                     and function declarations of all functions
                                     of the DEM Module

     Requirement Specification     : RS_LLD_CONFIG_AUTOSAR.pdf

     Platform Dependant[yes/no]    : no

     To be changed by user[yes/no] : no
*/
/*****************************************************************************/

/* To avoid double inclusion */
#ifndef     _DEM_H
#define     _DEM_H

/*...................................File Includes...........................*/

#include "Std_Types.h"
#include "Dem_Cfg.h"

/*...................................Macros..................................*/

#define DEM_MAJOR_VERSION (1)
#define DEM_MINOR_VERSION (0)
#define DEM_PATCH_VERSION (0)

/* Moduel ID of DEM */
#define DEM_MODULE_ID     (54)

/* configuration of DET support >0 is enabled */
#define DEM_DEV_ERROR_DETECT            (1)

/*.........................Type Definitions..................................*/

typedef uint16 Dem_EventIdType;

typedef enum
{
    DEM_EVENT_STATUS_PASSED,
    DEM_EVENT_STATUS_FAILED
} Dem_EventStatusType;

/*.........................Global Function Prototypes........................*/

/* The Below API is accessed from the modules MCU,FLASH,RAMTST,WDG and CAN */
void Dem_SetEventStatus (Dem_EventIdType EventId,
                         Dem_EventStatusType EventStatus);
#endif /* end _DEM_H */

/*...............................END OF DEM.H................................*/

