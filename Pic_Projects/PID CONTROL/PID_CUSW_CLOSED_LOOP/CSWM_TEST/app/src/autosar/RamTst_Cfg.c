/* Created at 15:24 on 1-4-2009 */
/* Version of HALCoGen used : 2.9.3 */
/* Code generated for       : MC9S12XS128 */
/*****************************************************************************/
/* Copyright(c) - This Program/Software is the exclusive property of Infosys. 
The software may not be reproduced or given to third parties without prior 
consent of Infosys.

     Filename                      : RamTst_Cfg.c
     Version                       : 1.0.1
     Microcontroller Platform      : MC9S12XS128
     Compiler                      :  Compiler for Freescale HC12 V5.0.30

     Description                   : Configuration  file for RAM Test
                                     
     Requirement Specification     : AUTOSAR_SWS_RAMTest.doc 0.12
                
     Module Design                 : AUTOTMAL_DDD_RAMTest_SPAL2.0.doc 
                                     version 2.00
     
     Platform Dependant[yes/no]    : yes
     
     To be changed by user[yes/no] : no
*/
/*****************************************************************************/

/* Revision History

Class of Change
-------------------------------------------------------------------------------
N - New Module.
F - Functional development.
E - Error Correction.
O - Optimization without functional change.
-------------------------------------------------------------------------------
 00      09-May-2006     Khanindra Jyoti Deka, Infosys
-------------------------------------------------------------------------------
Class: N
Cause: New Module
Detail:This is Configuration  file for RAM Test.
-------------------------------------------------------------------------------
 01      17-Oct-2007     Prashant Kulkarni, Infosys
-------------------------------------------------------------------------------
Class: E
Cause: Bug fixing
Detail:Fixed issue 830. RAMTST_TEST_COMPLETED_NOTIFICATION and
		RAMTST_TEST_ERROR_NOTIFICATION misspelled as RAMTST_TEST_COMPLETED and
		RAMTST_TEST_ERROR_OCCURED
-------------------------------------------------------------------------------

******************************************************************************/

/*...................................File Includes...........................*/

/* Module header */
#include "RamTst.h"

/* Block Params for Checkerboard test */
const RamTst_BlockParamsType RamTst_CheckerboardBlockParams\
                             [RAMTST_CHECKERBOARD_NUMBER_OF_BLOCKS]=
{
    /* Block Number 1 */
    {  
    RAM_TEST_CB_BLCK1,
    RAMTST_CHECKERBOARD_START_ADDRESS_1,
    RAMTST_CHECKERBOARD_END_ADDRESS_1
    },
};

/* Config Structure for RAM Test */
const RamTst_ConfigParamsType RamTst_Config = 
{  
    RAMTST_NUMBER_OF_ALGORITHMS,
    RAMTST_DEFAULT_TEST_ALGORITHM,
    RAMTST_TEST_COMPLETED_NOTIFICATION,
    RAMTST_TEST_ERROR_NOTIFICATION, 
  
    /* CHECKERBOARD Algorithm */
    RAMTST_CHECKERBOARD_ALGORITHM_ID,
    RAMTST_CHECKERBOARD_NUMBER_OF_TESTED_CELLS,
    RAMTST_CHECKERBOARD_NUMBER_OF_BLOCKS, 
    RamTst_CheckerboardBlockParams,
  
};
   
/*...........................End of RamTst_Cfg.c............................*/
