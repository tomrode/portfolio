/* Created at 15:25 on 1-4-2009 */
/* Version of HALCoGen used : 2.9.3 */
/* Code generated for       : MC9S12XS128 */

/*****************************************************************************/
/* Copyright(c) - This Program/Software is the exclusive property of Infosys. 
The software may not be reproduced or given to third parties without prior 
consent of Infosys.

     Filename                      : Gpt.c
     Version                       : 3.3.8
     Microcontroller Platform      : MC9S12XS128
     Compiler                      : Codewarrior HCS12X V4.5

     Description                   : This file contains all function 
                                     definitions of all functions of GPT 
                                     Driver module. 
                                     It includes:
                                     1) Gpt_Init
                                     2) Gpt_DeInit
                                     3) Gpt_StartTimer
                                     4) Gpt_StartTimerAbsolute
                                     4) Gpt_StopTimer
                                     5) Gpt_GetTimeElapsed
                                     6) Gpt_GetTimeRemaining
                                     7) Gpt_EnableNotification
                                     8) Gpt_DisableNotification
                                     9) Gpt_SetMode
                                     10)Gpt_DisableWakeup
                                     11)Gpt_EnableWakeup
                                     
     Requirement Specification     : WP4.2.2.1.12-SWS-GPT-Driver.doc 1.1.6
                
     Module Design                 : AUTOTMAL_DDD_GPT_SPAL2.0.doc version 2.00c
     
     Platform Dependant[yes/no]    : yes
     
     To be changed by user[yes/no] : yes
*/
/*****************************************************************************/
   
/* Revision History       

Class of Change
-------------------------------------------------------------------------------
N - New Module.
F - Functional development.
E - Error Correction.
O - Optimization without functional change.
-------------------------------------------------------------------------------
 00    02-Feb-2006     Sowmya, Infosys
-------------------------------------------------------------------------------
Class: N
Cause: New Module
Detail:This file implements the GPT driver.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 01    22-Mar-2006       Sowmya, Infosys
-------------------------------------------------------------------------------
Class: F
Cause: Functional development
Detail:Upgrade to suit to 1.1.6 GPT specification.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 02    08-Jun-2006       Sowmya, Infosys
-------------------------------------------------------------------------------
Class: F
Cause: Functional development
Detail:- Code modified for points reported and accepted in RTRT anomaly report 
         list
       - Added requirement no.170 in Gpt_GetTimeRemaining.
       - Updated Gpt_GetVersionInfo
       - Std_VersionInfoType structure is modified in Std_Types.h 
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 03    26-Jun-2006     Sowmya, Infosys
-------------------------------------------------------------------------------
Class: O
Cause: Implementation of macros for functions Gpt_EnableNotification and 
       Gpt_DisableNotification
Detail:Added Function Macro's for Gpt_EnableNotification and 
       Gpt_DisableNotification
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 04    04-Aug-2006     Rose Paul ,  Infosys
-------------------------------------------------------------------------------
Class: E
Cause: Bug Fixes from Problem Report - 337,338,346
Detail:Fixes:
       1) GptDeInit flag removed (Redundant)
       2) Shifted macros from Gpt.h to Gpt.c to avoid integration compile error
       3) Removed 'OR' from Clearing of Interrupt flags
       4) GetVersionInfo need not check for InitComplete
       5) Bit manipulation macros shifted to Std_Types.h
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 05    11-Aug-2006     Sowmya ,  Infosys
-------------------------------------------------------------------------------
Class: E
Cause: Fixes from RTRT anomaly report.
Detail:Code modified for points reported and accepted in Re-Run of RTRT anomaly 
       report list.
       1) Added requirement no.221 in Gpt_GetVersionInfo.
       2) Explicit check is done in Gpt_DeInit.
       3) Bracket added in Gpt_GetTimeElapsed.
       4) GPT_REPORT_WAKEUP_SOURCE check removed from Gpt_SetMode (Redundant)
       5) Requirement numbers are updated in the comments for all the API's    
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 06     28-Sep-2006     Rose Paul ,  Infosys
-------------------------------------------------------------------------------
Class: E
Cause: Bug Fixes from Problem Report - 466
Detail:Fixes:
       1) Gpt_GetTimeElapsed:TC<TCNT (overflow)condition stt,
          calculation is changed,overflow checking condition changed from
          TC>TCNT to TC>=TCNT and delay >(TC-TCNT) to delay >= (TC-TCNT)
       2) Gpt_GetTimeRemaining: TC<TCNT (overfolow)condition stt,
          calculation is changed,overfolw checking condition changed from
          TC>TCNT to TC>=TCNT and delay >(TC-TCNT) to delay >= (TC-TCNT)
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 07     17-Oct-2006     Rose Paul ,  Infosys
-------------------------------------------------------------------------------
Class: E
Cause: Correction of issue No: 528 in PRL -
Detail:Fixes:
       1) Interrupt flag cleared in StartTimer and StopTimer
       2) Error condition to return error value in Elapsed and Remaining
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 08      15-Nov-2006     Ajaykumar ,  Infosys
-------------------------------------------------------------------------------
Class: E
Cause: Bug Fixes from Problem Report - 466
Detail:Fixes:
       1) Gpt_InitCompleteFlag initialised to FALSE globally
       2) Gpt_InitCompleteFlag changed from bit flag to uint8 variable
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 09    08-Dec-2006     Ramprasad P B, Infosys
-------------------------------------------------------------------------------
Class: N
Cause: New Module
Detail:This file implements the GPT driver for S12XEP100.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 10    12-Jan-2007     Ramprasad P B, Infosys
-------------------------------------------------------------------------------
Class: F
Cause: Functional development
Detail:1)All the hardware resources that were initialized in Gpt_Init are 
          deinitialized to their power on reset values.
       2) Legacy timer always being selected in Gpt_Init is solved
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 11     23-Jan-2007     Ramprasad P B, Infosys
-------------------------------------------------------------------------------
Class: F
Cause: Issue 592
Detail:PITMTLD0 & PITMTLD1 values are modified in Gpt_Init & Gpt_DeInit based 
       on the preprocessor switch value.  
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 12     28-Mar-2007     Rajesh , Infosys
-------------------------------------------------------------------------------
Class:  E
Cause:  Internally Found Issues and PRL's 528,570,571 fixed
Detail: Gpt_WakeupCapable is set correctly and MCCTL is cleared appropiately and
        Interrupt Flags are cleared at appropiate places and s/w variables are 
        initialised in Init.MCCNT Loaded with the Delay value in Continuous 
        conversion.   
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 13     28-Mar-2007     Rajesh , Infosys
-------------------------------------------------------------------------------
Class:  F
Cause:  CR#339
Detail: A new API Gpt_StartTimerAbsolute is added.
-------------------------------------------------------------------------------
 14    14-May-2007     Ramprasad P B , Infosys
-------------------------------------------------------------------------------
Class:  O
Cause:  Returned result of MCAL_GET_BIT() function is compared with MCAL_CLEAR
        instead of 0. 
Detail: The above changes are being done in Gpt_GetTimeElapsed,
        Gpt_GetTimeRemaining & in the Interrupt Handling functions.  
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 15    24-Jul-2007      Khanindra, Infosys
-------------------------------------------------------------------------------
Class: F
Cause: Updated for the derivatives MC9S12XEG384 and MC9S12XEG256 
Detail:Updated for the derivatives MC9S12XEG384 and MC9S12XEG256. Preprocessor
       switches are used to differentiate between XEP and XEG.
       Gpt_ModulusDwnCount16_Isr is updated. APIs are updated for PIT channels.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 16    29-Aug-2007      Khanindra, Infosys
-------------------------------------------------------------------------------
Class: O
Cause: PRL Issue #801 is fixed
Detail:Changes done in the function headers to fix the issue #801
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 17    01-Oct-2007      Khanindra, Infosys
-------------------------------------------------------------------------------
Class: F
Cause: Updated for the derivatives MC9S12XS128
Detail:Updated for the derivatives MC9S12XS128. Preprocessor switch 
       HAL_MC9S12XS128 is used to differentiate from other derivatives.As 
       Modulus Down Counter is not available in MC9S12XS128,so Modulus Down 
       Counter related codes are removed using Preprocessor switch.TIM is used 
       in XS128. ECT and TIM are used synonymously.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 18    28-Nov-2007      Khanindra, Infosys
-------------------------------------------------------------------------------
Class: O
Cause: Changes done for the issue 856
Detail:APIs Gpt_DeInit,Gpt_GetTimeElapsed,Gpt_GetTimeRemaining,Gpt_StartTimer,
       Gpt_StopTimer,Gpt_EnableNotification,Gpt_DisableNotification,
       Gpt_SetMode,Gpt_DisableWakeup,Gpt_EnableWakeup are updated for 
       reordering the DET error check GPT_E_UNINIT in sync with the other 
       driver modules.
-------------------------------------------------------------------------------
 19    24-Dec-2007      Khanindra, Infosys
-------------------------------------------------------------------------------
Class: O
Cause: Changes done for the issue #873,#863
Detail:#873: API Gpt_DeInit is updated for the variable Gpt_WakeupCapable for 
             better time and memory efficency.
       #863: APIs Gpt_StartTimer and Gpt_StartTimerAbsolute are updated for the 
             passing parameter 'value'. In the API Gpt_StartTimer, DET check 
             for the parameter 'value' is updated.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 20    31-Mar-2008      Khanindra, Infosys
-------------------------------------------------------------------------------
Class: E
Cause: Changes done for the issue #975 and #978
Detail:#975: APIs Gpt_StartTimer,Gpt_StartTimerAbsolute and ISR 
             Gpt_ModulusDwnCount16_Isr are updated as per the issue #975
       #978: Pre compiler check for SW Patch Version is removed.
-------------------------------------------------------------------------------

******************************************************************************/

/*...................................File Includes...........................*/

/* Module header */
#include "Gpt.h"

/* OS header file */
#include "os.h"

/*...................................Macros..................................*/
/* Declaring the required macros to perform the version check */
/* SW ->Software vendor version info */
#define GPT_SW_MAJOR_VERSION_C                  (3)
#define GPT_SW_MINOR_VERSION_C                  (3)
#define GPT_SW_PATCH_VERSION_C                  (8)
/* AR ->AUTOSAR version info */
#define GPT_AR_MAJOR_VERSION_C                  (1)
#define GPT_AR_MINOR_VERSION_C                  (1)
#define GPT_AR_PATCH_VERSION_C                  (6)

/* Version checking before compilation */
#if(GPT_SW_MAJOR_VERSION==GPT_SW_MAJOR_VERSION_C) /* Vendor Check  */
#if(GPT_SW_MINOR_VERSION==GPT_SW_MINOR_VERSION_C)

#if(GPT_AR_MAJOR_VERSION==GPT_AR_MAJOR_VERSION_C) /* Autosar Check */
#if(GPT_AR_MINOR_VERSION==GPT_AR_MINOR_VERSION_C)
#if(GPT_AR_PATCH_VERSION==GPT_AR_PATCH_VERSION_C)

/* Timer Channel mode */
#define GPT_MODE_ONESHOT                        ((uint8)0)

/* Maximum Timer value */
#define GPT_MAXCOUNT_VALUE                      (0xFFFF)

/* Error value - 0xFFFFFFFF */
#define GPT_REMAINING_ERROR_VALUE               ((uint32)4294967295)

/* Error value - 0 */
#define GPT_ELAPSED_ERROR_VALUE                 ((uint32)0)
/*Offsets the ECT/TIM Channel Count*/
#define PIT_CHANNEL_OFFSET                      ((uint8)8) 

/*Index of the ECT/TIM End Channel(Static Value)*/
#define ECT_END_CHANNEL_INDEX                   ((uint8)7)

/*Index of the PIT End Channel(Static Value)*/
#define PIT_END_CHANNEL_INDEX                   ((uint8)11) 

/* Macros for Timer channels */
#define ECT_CHANNEL_2                            ((uint8)2)

/* Definition of ECT/TIM channels */
#define ECT_TC0          TC0
#define ECT_TC1          TC1
#define ECT_TC2          TC2
#define ECT_TC3          TC3
#define ECT_TC4          TC4
#define ECT_TC5          TC5
#define ECT_TC6          TC6
#define ECT_TC7          TC7
#define ECT_TIE          TIE
#define ECT_TFLG1        TFLG1
#define ECT_TIOS         TIOS
#define ECT_OC7D         OC7D
#define ECT_OC7M         OC7M
#define ECT_TCTL1        TCTL1
#define ECT_TCTL2        TCTL2
#define ECT_TCNT         TCNT

/* ..........................Global variables............................... */

/* These global variables have external linkages */

/* These global variables have internal linkages */
static  const Gpt_ConfigType *Gpt_UserConfig_Ptr;


/****************************************************************************** 
   The following  array will be of size equal to maximum configured channels.
   Tool will replace the mapindex with the channels configured. 
******************************************************************************/
static uint8 Gpt_Mapindex[GPT_MAX_CONFIG_CHANNELS]=
                              {
                                  ECT_CHANNEL_2
                              };

/* Declare variables to store ECT/TIM timeout delay */
uint16 ECT_Delay0;
uint16 ECT_Delay1;
uint16 ECT_Delay2;
uint16 ECT_Delay3;
uint16 ECT_Delay4;
uint16 ECT_Delay5;
uint16 ECT_Delay6;
uint16 ECT_Delay7;
/* Declare variables to store ECT/TIM counter value */
uint16 ECT_Count0;
uint16 ECT_Count1;
uint16 ECT_Count2;
uint16 ECT_Count3;
uint16 ECT_Count4;
uint16 ECT_Count5;
uint16 ECT_Count6;
uint16 ECT_Count7;

/* To store address of timeout delay for each timer channel */
static uint16* const Gpt_TimeoutDelay[GPT_MAX_CHANNELS] = 
{
    &(ECT_Delay0),&(ECT_Delay1),&(ECT_Delay2),&(ECT_Delay3),
    &(ECT_Delay4),&(ECT_Delay5),&(ECT_Delay6),&(ECT_Delay7),
    (uint16*)&(PITLD0),(uint16*)&(PITLD1),(uint16*)&(PITLD2),
    (uint16*)&(PITLD3)
};

/* To store address of counter register for each timer channel */
static uint16* const Gpt_CounterValue[GPT_MAX_CHANNELS] =
{
    (uint16*)&(ECT_Count0),(uint16*)&(ECT_Count1),(uint16*)&(ECT_Count2),
    (uint16*)&(ECT_Count3),(uint16*)&(ECT_Count4),(uint16*)&(ECT_Count5),
    (uint16*)&(ECT_Count6),(uint16*)&(ECT_Count7),(uint16*)&(PITCNT0),
    (uint16*)&(PITCNT1),(uint16*)&(PITCNT2),(uint16*)&(PITCNT3)
};

/*******************For ECT***************************************************/                                            

/* To store address of Output compare regsiter for ECT timer channels */
static uint16* const Gpt_AddrOutputCompare[GPT_MAX_ECT_CHANNELS] = 
{
    (uint16*)&(ECT_TC0),(uint16*)&(ECT_TC1),(uint16*)&(ECT_TC2),
    (uint16*)&(ECT_TC3),(uint16*)&(ECT_TC4),(uint16*)&(ECT_TC5),
    (uint16*)&(ECT_TC6),(uint16*)&(ECT_TC7)
};

/********************* For GPT timer channels*********************************/


/**************************************************************************
   Buffer to store notification enable/disable for configured channels 
   1 byte for ECT/TIM channels
   1 byte for PIT  channels
**************************************************************************/
static uint8 Gpt_Enable_Notification[GPT_NOOFHARDWARETYPES];

/******************************************************************************
   Buffer to store timer on/off information for configured channels 
   1 byte for ECT /TIMchannels
   1 byte for PIT  channels
******************************************************************************/   
static uint8 Gpt_TimerOnFlag[GPT_NOOFHARDWARETYPES];

/******************************************************************************
   Buffer to store timeout information for configured channels 
   1 byte for ECT/TIM channels
   1 byte for PIT  channels
******************************************************************************/   
static uint8 Gpt_Timeoutcount[GPT_NOOFHARDWARETYPES];

/*..........................Local functions..................................*/

/*..........................Local functions definitions......................*/

/*..........................API Definitions..................................*/

/*****************************************************************************
* Function Name              : Gpt_GetVersionInfo
*
* Arguments                  : Std_VersionInfoType *versioninfo 
*
* Description                : This API stores the version information of the 
*                              GPT module in the variable pointed by the 
*                              pointer passed to the function. The version 
*                              information includes
*                              1.Module ID
*                              2.Version ID
*                              3.Vendor specific version numbers.
*
* Return Type                : void
*
* Requirement numbers        : GPT181,GPT221
******************************************************************************/

void Gpt_GetVersionInfo(Std_VersionInfoType *versioninfo)
{
    

    if(NULL_PTR != versioninfo)
    {
        /* Copy MODULE ID */
        versioninfo->moduleID=GPT_MODULE_ID;

        /* Copy VENDOR ID */
        versioninfo->vendorID=GPT_VENDOR_ID;

        /* Copy SW vendor version info. */

        versioninfo->sw_major_version=GPT_SW_MAJOR_VERSION_C;

        versioninfo->sw_minor_version=GPT_SW_MINOR_VERSION_C;

        versioninfo->sw_patch_version=GPT_SW_PATCH_VERSION_C;

    }

}
/******************************************************************************
* Function Name             : Gpt_Init
*
* Arguments                 : Gpt_ConfigType*
*                             The parameter is a pointer 
*                             to the configuration that shall be initialized.
*                             Different sets of static configuration 
*                             that shall be initialized. 
*
* Description               : This function Initializes global vriables
*                             and static variables.
*
* Return Type               : void
*
* Requirement numbers       : GPT006,GPT107,GPT068,GPT205,GPT219
******************************************************************************/ 

void Gpt_Init(const Gpt_ConfigType *ConfigPtr)
{
    uint8 index=0; 
    

    
    DisableAllInterrupts();
    
    /* GPT107: Disable notifications */
    Gpt_Enable_Notification[0]=  MCAL_DISABLE;
    Gpt_Enable_Notification[1] = MCAL_DISABLE;
    
    /*Initialise all S/w Variables and Buffers used*/
    
    
    /*S/w Buffers used are initialised*/        
    for(index=0;index<GPT_NOOFHARDWARETYPES;index++)
     {
       Gpt_TimerOnFlag[index] =MCAL_DISABLE;      
       Gpt_Timeoutcount[index]=MCAL_DISABLE;      
     }    
      

    /****************** Disable Interrupts for configured Channels ***********/      
    
    /************************************************************************** 
    * Disable Interrupt and flag for configured ECT/TIM timer channels.
    **************************************************************************/
    ECT_TIE &= (~(ConfigPtr->Gpt_ECTHardware.Gpt_ChannelConfigured));
    
    /* Clear main timer interrupt flag for all the configured channels
       by writing 1 to it */
    ECT_TFLG1 = (ConfigPtr->Gpt_ECTHardware.Gpt_ChannelConfigured);       


    /****************** Initialise ECT/TIM Hardware ******************************/

    /* Enable Output Compare for given channel */
    ECT_TIOS = (ECT_TIOS |  (ConfigPtr->Gpt_ECTHardware.Gpt_ChannelConfigured));
    
    /**************************************************************************
    * Added functionality *
    * "Output compare mask action for ECT/TIM timer channels".
    * When the output compare 7 mask register(OC7Mx) is enabled for a given channel
    * and Output compare 7 data register(OC7Dx) is loaded with (high/low) value for
    * the same channel , this data from OC7Dx will be transfered to the timer 
    * port on a successful channel 7 output compare.
    **************************************************************************/
    /* Setting the Output Compare 7 data register */
    ECT_OC7D = (ECT_OC7D | (ConfigPtr->Gpt_ECTHardware.Gpt_DataValue));
    
    /* Setting the Output Compare 7 mask register */
    ECT_OC7M = (ECT_OC7M | (ConfigPtr->Gpt_ECTHardware.Gpt_DataTransfer));
    
    /**************************************************************************
    * Added functionality *
    * " Output action on timer port"
    * Output action on timer port can be enabled for ECT/TIM timer channels, user 
    * can configure the output action for each configured timer channel by 
    * setting :- OMx(Output Mode) and OLx(Output level) on timer control 
    * register(TCTL1/TCTL2).
    * The output actions will be :-
    * OMx               OLx              Action
      -------------------------------------------------------------------------
      0                  0               Timer disconnected from pin
      0                  1               Toggle output line
      1                  0               Clear output line
      1                  1               Set output line
    **************************************************************************/  
    
    ECT_TCTL1&= GPT_ACTIVATION_DEINIT_T47_MASK;
    ECT_TCTL2&= GPT_ACTIVATION_DEINIT_T03_MASK;
    
    /* For timer channel 4 to channel 7 */
    ECT_TCTL1 = (ECT_TCTL1 | (ConfigPtr->Gpt_ECTHardware.Gpt_OutputAction1));
    
    /* For timer channel 0 to channel 3 */
    ECT_TCTL2 = (ECT_TCTL2 | (ConfigPtr->Gpt_ECTHardware.Gpt_OutputAction2));
    
    /****************** End of ECT Hardware Initialization *******************/
   
    /* Copy the address to a file specific pointer */
    Gpt_UserConfig_Ptr = ConfigPtr;
        
    EnableAllInterrupts();
    

}
/******************************************************************************
* Function Name             : Gpt_GetTimeElapsed
*
* Arguments                 : ECT-Can take values{GPT_CHANNEL_0 ->GPT_CHANNEL_7}
*                             PIT-Can take values
*                             {GPT_CHANNEL_8 ->GPT_CHANNEL_11}
*
* Description               : This service will calculate the time elapsed for 
*                             a given channel.Calling this function
*                               
* Return Type               : value
*
* Requirement numbers       : GPT010,GPT097,GPT100,GPT049
                              
******************************************************************************/

Gpt_ValueType Gpt_GetTimeElapsed (Gpt_ChannelType channel) 
{
    /* Get the Actual Channel number */ 
    uint8 index_ch = Gpt_Mapindex[channel];
    
    /* Declare a local variable to store the time elapsed value */
    Gpt_ValueType TimeElapsed = GPT_ELAPSED_ERROR_VALUE;
    


    /* Is the channel mode one shot */
    if((GPT_MODE_ONESHOT == Gpt_UserConfig_Ptr->Gpt_Channel[channel].\
        Gpt_ModeDefault) && (MCAL_CLEAR!=MCAL_GET_BIT(Gpt_Timeoutcount,index_ch)))
    {
          /**********************************************************************
          * GPT100: When calling this function on a channel configured for 
          * one shot mode ,after the timeout period  has already expired,
          * the timeout period value(passed as parameter for Gpt_StartTimer) 
          * is returned.
          **********************************************************************/
          TimeElapsed = (uint32)(*Gpt_TimeoutDelay[index_ch]);
    }
    else  /* Calculate Time Elapsed */
    {       
        /* Check for ECT/TIM hardware */
        if(index_ch <= ECT_END_CHANNEL_INDEX) 
        {
            /* Declare a local variable to store the current count value for ECT */    
            uint16 Current_count;            

            /* Store the current count value */
            Current_count = ECT_TCNT;
            
            /******************************************************************
            * Overflow condition has been changed. 
            ******************************************************************/
            /* Check if TCx >= TCNT */
            if((*Gpt_AddrOutputCompare[index_ch]) >= Current_count) 
            {
                /* Is TimeoutDelay >= TCx - TCNT */
                if ((*Gpt_TimeoutDelay[index_ch]) >=
                    ((*Gpt_AddrOutputCompare[index_ch]) - 
                       Current_count)) 
                {   
                    /******************************************************
                    * Counter Value = (TCx - TCNT) 
                    ******************************************************/
                    (*Gpt_CounterValue[index_ch]) =
                        ((*Gpt_AddrOutputCompare[index_ch]) -
                           Current_count);
                } 
                else /* Error condition */
                {
                    /******************************************************
                    * This condition will never occur !!!
                    ******************************************************/
                    TimeElapsed=GPT_ELAPSED_ERROR_VALUE;
                    return (TimeElapsed);
                }
            }
            else   /* If TCx < TCNT */
            {
                /**********************************************************
                * Counter Value = (TCNT - TCx)
                **********************************************************/
                (*Gpt_CounterValue[index_ch]) = (GPT_MAXCOUNT_VALUE-
                  Current_count + (*Gpt_AddrOutputCompare[index_ch]) + 1);
                                                
            }
        } /* End of ECT/TIM hardware */
        /**********************************************************************
        * GPT010: Time Elapsed in one shot mode - Initial timer
        * value (Timeout) minus the current count register value. 
        * TimeElapsed = TimeoutDelay - Counter Value.
        **********************************************************************/ 
        TimeElapsed = ((uint32)((*Gpt_TimeoutDelay[index_ch]) -
                                (*Gpt_CounterValue[index_ch])));
    }
    return (TimeElapsed);

}
/******************************************************************************
* Function Name             : Gpt_GetTimeRemaining
*
* Arguments                 : ECT-Can take values{GPT_CHANNEL_0 ->GPT_CHANNEL_7}
*                             PIT-Can take values
*                             {GPT_CHANNEL_8 ->GPT_CHANNEL_11}
*
* Description               : This service will calculate the time remaining 
*                             for a given channel.Calling this function
*                               
* Return Type               : value
*
* Requirement numbers       : GPT010,GPT097,GPT100,GPT049
                              
******************************************************************************/             

Gpt_ValueType Gpt_GetTimeRemaining (Gpt_ChannelType channel) 
{
    /* Get the Actual Channel number */
    uint8 index_ch = Gpt_Mapindex[channel];
        
    /* Declare a local variable to store the time elapsed value */
    Gpt_ValueType TimeRemaining = 0;
    

    /* GPT 170: Calling the function prior to starting the timer 
    channel, biggest available value should be returned */
    if(MCAL_CLEAR == MCAL_GET_BIT(Gpt_TimerOnFlag,index_ch)&&
      (MCAL_CLEAR == MCAL_GET_BIT(Gpt_Timeoutcount,index_ch)))
    {
        /* Biggest available value depending upon the size of Gpt_ValueType */           
        TimeRemaining = GPT_REMAINING_ERROR_VALUE;
        return TimeRemaining ;
    }

    /* Is the channel mode oneshot ? */
    if((GPT_MODE_ONESHOT == Gpt_UserConfig_Ptr->Gpt_Channel[channel].\
        Gpt_ModeDefault) && (MCAL_CLEAR!=MCAL_GET_BIT(Gpt_Timeoutcount,index_ch)))
    {
        /**********************************************************************
        * GPT102: When calling this function on a channel configured for one 
        * shot mode ,after the timeout period  has already expired,value "0" 
        * is returned.
        **********************************************************************/
        TimeRemaining = (uint32) 0;
    }
    /* In one shot or continuous */
    else
    {
        /* Check the hardware for ECT/TIM channel */
        if (index_ch <= ECT_END_CHANNEL_INDEX) 
        {
            /* Declare a local variable to store the current count for ECT */
            uint16 Current_count;           

            /* Store the current count value */
            Current_count = ECT_TCNT;
            
            /******************************************************************
            * Overflow condition has been changed. 
            ******************************************************************/
            /* Check if TCx >= TCNT */
            if((*Gpt_AddrOutputCompare[index_ch]) >= Current_count) 
            {
                /* Is TimeoutDelay >= TCx - TCNT */
                if ((*Gpt_TimeoutDelay[index_ch]) >=
                    ((*Gpt_AddrOutputCompare[index_ch]) - Current_count)) 
                {   
                    /**********************************************************
                    * Counter Value = (TCx - TCNT) 
                    **********************************************************/
                    (*Gpt_CounterValue[index_ch]) = 
                       ((*Gpt_AddrOutputCompare[index_ch]) - Current_count);
                } 
                else /* Error condition */
                {
                    /******************************************************
                    * This condition will never occur !!!
                    ******************************************************/
                    TimeRemaining=GPT_REMAINING_ERROR_VALUE;
                    return (TimeRemaining);
                }
            }
            else   /* If TCx < TCNT */
            {
                /**************************************************************
                * Counter Value = (TCNT - TCx)
                **************************************************************/
                (*Gpt_CounterValue[index_ch]) = (GPT_MAXCOUNT_VALUE-
                  Current_count + (*Gpt_AddrOutputCompare[index_ch]) + 1);
            }/* End of overflow check for ECT */
            
        } /* End of ECT hardware */
        
        /* GPT083:Time Remaining  = Current count register value */
        TimeRemaining = (uint32)(*Gpt_CounterValue[index_ch]);
    }
    return (TimeRemaining);

}
/******************************************************************************
* Function Name             : Gpt_StartTimer
*
* Arguments                 : ECT-Can take values{GPT_CHANNEL_0 ->GPT_CHANNEL_7}
*                             PIT-Can take values
*                             {GPT_CHANNEL_8 ->GPT_CHANNEL_11}
*                             timeout value {0 -> 65535}
*                                
* Description               : This service will start the selected timer channel 
*                             with a defined timeout period.Starting a channel 
*                             does not automatically enable notifications.
*                               
* Return Type               : void                                                                     
*
* Requirement numbers       : GPT212,GPT060,GPT218,GPT012,GPT224,GPT084,GPT085
                              
******************************************************************************/

void Gpt_StartTimer (Gpt_ChannelType channel,Gpt_ValueType value)
{   
    /* Local Loop variables */
    uint8 index_ch = Gpt_Mapindex[channel]; 
    

         
    DisableAllInterrupts();
    
    /* Set the Timer on flag */
    MCAL_SET_BIT(Gpt_TimerOnFlag,index_ch);
        
    /* Store the timeout delay */
    if((ECT_END_CHANNEL_INDEX < index_ch)&&(index_ch <= PIT_END_CHANNEL_INDEX))
    {
        --value;
    }
    (*Gpt_TimeoutDelay[index_ch])  = (uint16)value;
    
    /* Check for ECT hardware */     
    if(index_ch <= ECT_END_CHANNEL_INDEX) 
    {      
        /**********************************************************************
        * Removed switch case for clearing OMx and OLx bit in TCTL1/TCTL2
        **********************************************************************/
        /* Load the Output compare register */
        *Gpt_AddrOutputCompare[index_ch] = (uint16)(ECT_TCNT + value);

        /* Clear the interrupt flag */
        ECT_TFLG1 = (1u << index_ch);

        /* Enable Interrupt */
        ECT_TIE = ( ECT_TIE | (1u << index_ch));
                                  
    }/* End of ECT hardware */
  
    
    EnableAllInterrupts();

}
/******************************************************************************
* Function Name             : Gpt_StartTimerAbsolute
*
* Arguments                 : ECT-Can take values{GPT_CHANNEL_0 ->GPT_CHANNEL_7}
*                             PIT-Can take values
*                             {GPT_CHANNEL_8 ->GPT_CHANNEL_11}
*
* Description               : This service will start the selected timer 
*                             channel with an absolute timeout period.
*                             Starting a channel does not automatically
*                             enable notifications.Absolute Delay is loaded
*                             into TC.Only Oneshot Conversion is possible
*                             and GetTimeElapsed and GetTimeRemaining API's
*                             cannot be called
*
* Return Type               : void
*
* Requirement numbers       : NOT AUTOSAR STANDARD

******************************************************************************/

void Gpt_StartTimerAbsolute (Gpt_ChannelType channel,Gpt_ValueType value)
{
    /* Local Loop variables */
    uint8 index_ch = Gpt_Mapindex[channel];    


    DisableAllInterrupts();
    
    /* Set the Timer on flag */
    MCAL_SET_BIT(Gpt_TimerOnFlag,index_ch);

    /* Store the Absolute delay */
    if((ECT_END_CHANNEL_INDEX < index_ch)&&(index_ch <= PIT_END_CHANNEL_INDEX))
    {
        --value;
    }
    (*Gpt_TimeoutDelay[index_ch])  = (uint16)value;

    /* Check for ECT/TIM hardware */
    if(index_ch <= ECT_END_CHANNEL_INDEX)
    {
        
        /* Load the Output compare register */
        *Gpt_AddrOutputCompare[index_ch] = (*Gpt_TimeoutDelay[index_ch]);

		    /* Clear the interrupt flag */
        ECT_TFLG1 = (1u << index_ch);

        /* Enable Interrupt */
        ECT_TIE = ( ECT_TIE | (1u << index_ch));

    }/* End of ECT hardware */
    
    EnableAllInterrupts();

}
/******************************************************************************
* Function Name             : Gpt_StopTimer
*
* Arguments                 : ECT-Can take values{GPT_CHANNEL_0 ->GPT_CHANNEL_7}
*                             PIT-Can take values
*                             {GPT_CHANNEL_8 ->GPT_CHANNEL_11}
*
* Description               : This service will stop the selected timer channel 
*                             Stopping a channel automatically disables
*                             notifications.
*                               
* Return Type               : void                                                                     
*
* Requirement numbers       : GPT213,GPT099,GPT225,GPT103
                              
******************************************************************************/

void Gpt_StopTimer(Gpt_ChannelType channel) 
{
    /* Local Loop variables */
    uint8 index_ch = Gpt_Mapindex[channel];
         


    DisableAllInterrupts();
    
    /*****************************For ECT/TIM Hardware****************************/   
    if(index_ch <= ECT_END_CHANNEL_INDEX) 
    {
        /* Stop the given channel - Disable the interrupt for given channel */
        ECT_TIE = ECT_TIE & (~(1u << (index_ch)));
        
        /* Clear the interrupt flag */
        ECT_TFLG1 = (1u << index_ch);

    }/* End of ECT/TIM hardware */
   
    /* Clear the timeout count for given channel */
    MCAL_CLEAR_BIT(Gpt_Timeoutcount,index_ch);
    
    /* Clear the timer on flag for given channel */
    MCAL_CLEAR_BIT(Gpt_TimerOnFlag,index_ch);
    
    EnableAllInterrupts();

}
/******************************************************************************
* Function Name             : Gpt_EnableNotification
*
* Arguments                 : ECT-Can take values{GPT_CHANNEL_0 ->GPT_CHANNEL_7}
*                             PIT-Can take values
*                             {GPT_CHANNEL_8 ->GPT_CHANNEL_11}
*
* Description               : This service will enable the GPT timeout 
*                             notification.                            
*                               
* Return Type               : void
*
* Requirement numbers       : GPT014,GPT091,GPT226,GPT214
                              
******************************************************************************/

void Gpt_EnableNotification(Gpt_ChannelType channel) 
{
    /* Local Loop variables */
    uint8 index_ch = Gpt_Mapindex[channel];


    
    /* GPT014:Enable Notification for the given GPT timer channel*/
    MCAL_SET_BIT(Gpt_Enable_Notification,index_ch);

}
/******************************************************************************
* Function Name             : Gpt_DisableNotification
*
* Arguments                 : ECT-Can take values{GPT_CHANNEL_0 ->GPT_CHANNEL_7}
*                             PIT-Can take values
*                             {GPT_CHANNEL_8 ->GPT_CHANNEL_11}
*
* Description               : This service will disable the GPT timeout 
*                             notification.                            
*                               
* Return Type               : void
*
* Requirement numbers       : GPT015,GPT092,GPT227,GPT217
                              
******************************************************************************/

void Gpt_DisableNotification(Gpt_ChannelType channel) 
{
    /* Local Loop variables */
    uint8 index_ch = Gpt_Mapindex[channel];



    /* GPT015:Disable Notification for the given GPT timer channel */        
    MCAL_CLEAR_BIT(Gpt_Enable_Notification,index_ch);

}
/*........................... Interrupt Routines ............................*/
 
#pragma CODE_SEG __NEAR_SEG NON_BANKED 
   
/******************************************************************************
* Function Name             : Gpt_ECTTimeoutHandler_2_Isr
*
* Arguments                 : None
*
* Description               : ISR for activation on "ECT Channel 2":
*                             1.Clears the timeout flag
*                             2.If channel mode is continuous, timer is 
*                               restarted.
*                             3.If channel mode is oneshot,timer is stopped.
*                             3.Performs function according to mode set
*                             4.Reports Wakeup if the channel is configured as 
*                             wakeup capable.
*                             5.Calls calback function if notification enabled.
*                            
* Return Type               : void
*
* Requirement numbers       : GPT232,GPT233,GPT086,GPT235
                              
******************************************************************************/
void Gpt_ECTTimeoutHandler_2_Isr(void) 
{                
    
    /* Clear the Main Timer interrupt flag register */
    ECT_TFLG1 = (MCAL_BIT2_MASK);
    
    /* Is the channel mode one shot */
    if (GPT_MODE_ONESHOT == 
        Gpt_UserConfig_Ptr->Gpt_Channel[GPT_CHANNEL_2].Gpt_ModeDefault)
    {
        /* Disable Interrupt to stop the timer in one shot mode */
        ECT_TIE &= (~(MCAL_BIT2_MASK));

        /* Clear the timer on flag */
        MCAL_CLEAR_BIT(Gpt_TimerOnFlag,ECT_CHANNEL_2);

        /* Set the timeout count */
        MCAL_SET_BIT(Gpt_Timeoutcount,ECT_CHANNEL_2);
    }
    else  /* If the channel mode is continuous */
    {
        /* Restart the ECT/TIM timer channel for continuous mode */
        *Gpt_AddrOutputCompare[ECT_CHANNEL_2] = 
            ((*Gpt_AddrOutputCompare[ECT_CHANNEL_2]) + 
             (*Gpt_TimeoutDelay[ECT_CHANNEL_2]));        
    }
    
    {
        /* Check the callback function for NULL pointer and 
           notification enabled flag */
        if ((NULL_PTR!= Gpt_UserConfig_Ptr->Gpt_Channel[GPT_CHANNEL_2].\
             Gpt_Notification) &&
            (MCAL_CLEAR!=MCAL_GET_BIT(Gpt_Enable_Notification,ECT_CHANNEL_2)))
        {
            /* GPT232:Call the callback function */
            (Gpt_UserConfig_Ptr-> Gpt_Channel[GPT_CHANNEL_2].\
             Gpt_Notification)();
        }
    }
    
}
       
#pragma CODE_SEG DEFAULT
 
#else
   #error " Mismatch in AR Patch Version of Gpt.c & Gpt.h "
#endif

#else
   #error " Mismatch in AR Minor Version of Gpt.c & Gpt.h " 
#endif

#else
   #error " Mismatch in AR Major Version of Gpt.c & Gpt.h " 
#endif

#else
   #error " Mismatch in SW Minor Version of Gpt.c & Gpt.h " 
#endif

#else
   #error " Mismatch in SW Major Version of Gpt.c & Gpt.h " 
#endif

/* End of version check */
/*...............................END OF Gpt.c................................*/


