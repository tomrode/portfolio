/* Created at 15:24 on 1-4-2009 */
/* Version of HALCoGen used : 2.9.3 */
/* Code generated for       : MC9S12XS128 */
/*****************************************************************************/
/* Copyright(c) - This Program/Software is the exclusive property of Infosys.
   The software may not be reproduced or given to third parties without prior
   consent of Infosys.

   Filename                     : Dio_cfg.h
   Version                      : 3.0.1
   Microcontroller Platform     : MC9S12XS128
   Compiler                     : Codewarrior HCS12X V5.0.28
   
   Description                  : This file contains the configuration 
                                  parameters used for DIO Driver Module.
                                  Symbolic names are defined for Port,channels
                                  and groups used in DIO module.  
 
   Requirement Specification    : AUTOSAR_SWS_DIODriver.pdf version 2.00
                
   Module Design                : AUTOTMAL_DDD_DIO_SPAL2.0.doc version 1.0.0

   Platform Dependant[yes/no]   : yes
     
   To be changed by user[yes/no]: no
*/

/*****************************************************************************/
/* Revision History

Class of Change
-------------------------------------------------------------------------------
N - New Module.
F - Functional development.
E - Error Correction.
O - Optimization without functional change.
-------------------------------------------------------------------------------
00  02-Feb-2006  Kiran G S, Infosys
-------------------------------------------------------------------------------
Class : N
Cause : New Module
Detail: This file implements the function definitions of DIO driver.
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
01  19-Mar-2006   kiran G S,  Infosys
-------------------------------------------------------------------------------
Class : F
Cause : Functional development
Detail: Upgrade to suit to 2.0.0 DIO specification.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
02  28-Jun-2006   kiran G S,  Infosys
-------------------------------------------------------------------------------
Class : O
Cause : Macro definition were added for the functions Dio_ReadChannel
        Dio_WriteChannel,Dio_ReadPort,Dio_WritePort,Dio_ReadChannelGroup and
        Dio_WriteChannelGroup.
                                                  .
Detail: Preprocessor switches for enabling disabling macros are added.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 04        01-Feb-2007     Kasinath, Infosys
-------------------------------------------------------------------------------
Class : F
Cause : A2D0 & A2D1 Channel Changes
Detail: Port A2D0 modified to handle 16 channels and port A2D1 8 channels.
-------------------------------------------------------------------------------
 ------------------------------------------------------------------------------- 
  05        05-Oct-2007     Ajaykumar, Infosys 
 ------------------------------------------------------------------------------- 
 Class : F 
 Cause : MC9S12XS128 controller support 
 Detail: MC9S12XS128 related pre-processor switches added. 
 ------------------------------------------------------------------------------- 
*/
/*****************************************************************************/

/* To avoid Multiple inclusion */  
#ifndef _DIO_CFG_H
#define _DIO_CFG_H

/*.............................MACRO DEFINITIONS.............................*/

/* Preprocessor switch for enabling the development error detection */
#define DIO_DEV_ERROR_DETECT               OFF      


/* It depends depends on the package configuration
   which user selects from Port driver.(112, 80 or 64) User can select 
   number of Channels from the PORT driver module.*/

#define  DIO_MAXPORTS                   9
#define  DIO_MAXCHANNELS                59

/* It will define the Maximum Number of groups used in DIO module*/
#define  DIO_MAXGROUPS                  2

/* Symbolic names depends on the package configuration which user selects from
   Port driver.(112,80 or 64) and also number of pins PORT driver assigned for
   DIO */
#define LED_U12S_EN         DIO_CHANNEL_A_0
#define RL_LED_SW_DETECT         DIO_CHANNEL_A_1
#define RR_LED_SW_DETECT         DIO_CHANNEL_A_2
#define CAN_STB         DIO_CHANNEL_A_3
#define U12T_EN         DIO_CHANNEL_A_4
#define REL_A_MON         DIO_CHANNEL_A_5
#define IGN_WHEEL_MON         DIO_CHANNEL_A_6
#define REL_B_MON         DIO_CHANNEL_A_7

#define MUX_A0         DIO_CHANNEL_B_0
#define MUX_A1         DIO_CHANNEL_B_1
#define MUX_A2         DIO_CHANNEL_B_2
#define SYMBOLIC_PORT_B_PIN_3_LEVEL         DIO_CHANNEL_B_3
#define SYMBOLIC_PORT_B_PIN_4_LEVEL         DIO_CHANNEL_B_4
#define SYMBOLIC_PORT_B_PIN_5_LEVEL         DIO_CHANNEL_B_5
#define SYMBOLIC_PORT_B_PIN_6_LEVEL         DIO_CHANNEL_B_6
#define SYMBOLIC_PORT_B_PIN_7_LEVEL         DIO_CHANNEL_B_7



#define SYMBOLIC_PORT_E_PIN_0_LEVEL         DIO_CHANNEL_E_0
#define SYMBOLIC_PORT_E_PIN_1_LEVEL         DIO_CHANNEL_E_1
#define IGN_REAR_MON         DIO_CHANNEL_E_2
#define REL_STW_EN         DIO_CHANNEL_E_3
#define VAR_BIT2         DIO_CHANNEL_E_4
#define VAR_BIT1         DIO_CHANNEL_E_5
#define VAR_BIT0         DIO_CHANNEL_E_6
#define SYMBOLIC_PORT_E_PIN_7_LEVEL         DIO_CHANNEL_E_7


#define SYMBOLIC_PORT_J_PIN_6_LEVEL         DIO_CHANNEL_J_6
#define SYMBOLIC_PORT_J_PIN_7_LEVEL         DIO_CHANNEL_J_7


#define CAN_RX         DIO_CHANNEL_M_0
#define CAN_TX         DIO_CHANNEL_M_1
#define MISO_LEVEL         DIO_CHANNEL_M_2
#define SSO_LEVEL         DIO_CHANNEL_M_3
#define TSLICE_PIN         DIO_CHANNEL_M_4
#define SCLK_LEVEL         DIO_CHANNEL_M_5

#define LED1_RR_EN         DIO_CHANNEL_P_0
#define PWM_VS_FR_LEVEL         DIO_CHANNEL_P_1
#define PWM_VS_FL_LEVEL         DIO_CHANNEL_P_2
#define PWM_BASE_REL_LEVEL         DIO_CHANNEL_P_3
#define LED2_RL_EN         DIO_CHANNEL_P_4
#define LED1_RL_EN         DIO_CHANNEL_P_5
#define LED2_RR_EN         DIO_CHANNEL_P_7

#define REL_B_EN         DIO_CHANNEL_S_0
#define REL_A_EN         DIO_CHANNEL_S_1
#define VENT_EN         DIO_CHANNEL_S_2
#define _LEVEL         DIO_CHANNEL_S_3

#define SYMBOLIC_PORT_T_PIN_0_LEVEL         DIO_CHANNEL_T_0
#define SYMBOLIC_PORT_T_PIN_1_LEVEL         DIO_CHANNEL_T_1
#define SYMBOLIC_PORT_T_PIN_2_LEVEL         DIO_CHANNEL_T_2
#define PWM_HS_FL_LEVEL         DIO_CHANNEL_T_3
#define PWM_HS_FR_LEVEL         DIO_CHANNEL_T_4
#define PWM_HS_RL_LEVEL         DIO_CHANNEL_T_5
#define PWM_HS_RR_LEVEL         DIO_CHANNEL_T_6
#define PWM_STW_LEVEL         DIO_CHANNEL_T_7

#define VAD_U12T_LEVEL         DIO_CHANNEL_AD0L_0
#define VAD_ISENSE_HS_RL_LEVEL         DIO_CHANNEL_AD0L_1
#define VAD_ISENSE_HS_RR_LEVEL         DIO_CHANNEL_AD0L_2
#define VAD_ISENSE_HS_FL_LEVEL         DIO_CHANNEL_AD0L_3
#define VAD_ISENSE_HS_FR_LEVEL         DIO_CHANNEL_AD0L_4
#define VAD_MUX2_OUT_LEVEL         DIO_CHANNEL_AD0L_5
#define VAD_MUX1_OUT_LEVEL         DIO_CHANNEL_AD0L_6
#define VAD_UINT_LEVEL         DIO_CHANNEL_AD0L_7

#define SYMBOLIC_PORT_A					DIO_PORT_A
#define SYMBOLIC_PORT_B					DIO_PORT_B
#define SYMBOLIC_PORT_E					DIO_PORT_E
#define SYMBOLIC_PORT_J					DIO_PORT_J
#define SYMBOLIC_PORT_M					DIO_PORT_M
#define SYMBOLIC_PORT_P					DIO_PORT_P
#define SYMBOLIC_PORT_S					DIO_PORT_S
#define SYMBOLIC_PORT_T					DIO_PORT_T
#define SYMBOLIC_PORT_AD0L					DIO_PORT_AD0L

/* Symbolic name for the group depends on the Number of Groups 
   configured by user */
#define  MuxSelGroup					(&DioConfigData[0])
#define  VarReadGrp					(&DioConfigData[1])


#endif  /* end of #ifndef _DIO_CFG_H */ 

/*.............................END OF Dio_Cfg.h..............................*/

