/* Created at 15:25 on 1-4-2009 */
/* Version of HALCoGen used : 2.9.3 */
/* Code generated for       : MC9S12XS128 */
/*****************************************************************************/
/* Copyright(c) - This Program/Software is the exclusive property of Infosys. 
The software may not be reproduced or given to third parties without prior 
consent of Infosys.

     Filename                      : Wdg_Cfg.c
     Version                       : 1.0.2
     Microcontroller Platform      : MC9S12XS128
     Compiler                      : Codewarrior HCS12X V5.7.0

     Description                   : This file contains all function 
                                     definitions of all functions of 
                                     Watchdog Driver module.
                                     
     Requirement Specification     : AUTOSAR_SWS_WatchdogDriver.pdf 2.0
                
     Module Design                 : AUTOTMAL_DDD_WDG_SPAL2.0.doc version1.00
     
     Platform Dependant[yes/no]    : yes
     
     To be changed by user[yes/no] : no
*/
/*****************************************************************************/
   
/* Revision History

Class of Change
-------------------------------------------------------------------------------
N - New Module.
F - Functional development.
E - Error Correction.
O - Optimization without functional change.
-------------------------------------------------------------------------------
 00      20-Jan-2006     Arindam Gupta, Infosys
-------------------------------------------------------------------------------
Class: N
Cause: New Module
Detail:This file implements the Watchdog driver.
------------------------------------------------------------------------------

-------------------------------------------------------------------------------
 01      03-Apr-2006     Arindam Gupta, Infosys
-------------------------------------------------------------------------------
Class:  F
Cause:  Changes done for SPAL2.0
Detail: Changes done as per SPAL2.0.
-------------------------------------------------------------------------------
 02      29-Nov-2007     Isha Gupta, Infosys
-------------------------------------------------------------------------------
Class:  O
Cause:  Updated for PRL #853
Detail: Updated for PRL #853
-------------------------------------------------------------------------------
******************************************************************************/

#include "Wdg.h"





const Wdg_ConfigType  Wdg_Config[WDG_MAX_NUMBER_OF_CONFIG] =
                                  {
                                      WDG_DEFAULT_MODE
                                  };


