/* Created at 15:24 on 1-4-2009 */
/* Version of HALCoGen used : 2.9.3 */
/* Code generated for       : MC9S12XS128 */
/*****************************************************************************/
/* Copyright(c) - This Program/Software is the exclusive property of Infosys. 
The software may not be reproduced or given to third parties without prior 
consent of Infosys.

     Filename                      : RamTstIf_Cbk.c
     Version                       : 1.0.0
     Microcontroller Platform      : MC9S12XS128
     Compiler                      : Compiler for Freescale HC12 V5.0.30

     Description                   : This file contains the callback function 
                                     definitions.
                                     
     Requirement Specification     : AUTOSAR_SWS_RAMTest.doc 0.12
                
     Module Design                 : AUTOTMAL_DDD_RAMTest_SPAL2.0.doc 
                                     version 2.00
     
     Platform Dependant[yes/no]    : no
     
     To be changed by user[yes/no] : yes
*/
/*****************************************************************************/
   
/* Revision History

Class of Change
-------------------------------------------------------------------------------
N - New Module.
F - Functional development.
E - Error Correction.
O - Optimization without functional change.
-------------------------------------------------------------------------------
 00      24-Jul-2006     Khanindra Jyoti Deka, Infosys
-------------------------------------------------------------------------------
Class: N
Cause: New Module
Detail:This file contains the callback function definitions.
-------------------------------------------------------------------------------

******************************************************************************/

/* Callback header file */
#include "RamTstIf_Cbk.h"

/*------------- End of File --------------------------------------------------*/

