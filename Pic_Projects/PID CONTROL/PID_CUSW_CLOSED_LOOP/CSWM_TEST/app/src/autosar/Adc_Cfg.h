/* Created at 15:24 on 1-4-2009 */
/* Version of HALCoGen used : 2.9.3 */
/* Code generated for       : MC9S12XS128 */
/*************************************************************************/
/* Copyright(c) - This Program/Software is the exclusive property of Infosys. 
The software may not be reproduced or given to third parties without prior 
consent of Infosys.

     Filename                      : Adc_Cfg.h
     Version                       : 3.0.2
     Microcontroller Platform      : MC9S12XS128
     Compiler                      : Codewarrior HCS12X V5.0.28

     Description                   : This file contains pre-compile/static
                                     configuration  parameters for the 
                                     ADC driver.

     Requirement Specification     : AUTOSAR_SWS_ADC_Driver.pdf version 1.0.16

     Module Design                 : AUTOSAR_DDD_ADC_SPAL2.0.doc version 2.11

     Platform Dependant[yes/no]    : yes

     To be changed by user[yes/no] : no
*/

/*****************************************************************************/

/* Revision History

Class of Change
------------------------------------------------------------------------------
N - New Module.
F - Functional development.
E - Error Correction.
O - Optimization without functional change.
------------------------------------------------------------------------------
 00     04-Jan-2006     Kapil Kumar, Infosys
------------------------------------------------------------------------------
Class:  N
Cause:  New Module
Detail: This file contains the static parameters for ADC driver.
-------------------------------------------------------------------------------
 01     21-Aug-2006     Karthik, Infosys
-------------------------------------------------------------------------------
Class:  E
Cause:  Lint Analysis 
Detail: 
-------------------------------------------------------------------------------
02      8-Dec-2006      Kasinath Hegde, Infosys
-------------------------------------------------------------------------------
Class:  F
Cause:  Software Changes for S12XEP100 
Detail: Additional Configuration parameters added for S12XEP100 
-------------------------------------------------------------------------------
------------------------------------------------------------------------------
03      23-Jan-2007     Kasinath Hegde  Infosys
------------------------------------------------------------------------------
Class:  F
Cause:  ADC HW Unit Identifier change
Detail: Names of ADC HW Units 0 & 1 (ATD0 & ATD1) changed to ADC_HW_0 and
        ADC_HW_1
-----------------------------------------------------------------------------
------------------------------------------------------------------------------
04      23-Mar-2007     Rajesh,  Infosys
------------------------------------------------------------------------------
Class:  E
Cause:  Bug Fixes - 530, 565, 676, 552, 682, 683, 686
Detail: ADC_ATD0 & ADC_ATD1 added to selectively use only those HW units that
are configured.
-----------------------------------------------------------------------------

******************************************************************************/


#ifndef _ADC_CFG_H
#define _ADC_CFG_H


/******************************************************************************
**                      Global Symbols                                       **
******************************************************************************/

/*.............................MACRO DEFINITIONS..............................*/

/*...................Configuration Description File...........................*/

/* Preprocessor switch for enabling the development error detection */
#define ADC_DEV_ERROR_DETECT            OFF
#define ADC_GRP_NOTIF_CAPABILITY        OFF
#define ADC_HW_TRIGGER_CAPABILITY       OFF

#define ADC_ONDEMAND_CONFIGURATION      OFF
#define ADC_GATED_CONFIGURATION         OFF

/*No. of configured groups */
#define ADC_CONFIGURED_GROUPS           8 
#define ADC_GATED_CONFIGURED_GROUPS     0

/* No of multiple configurations*/
#define ADC_MULTIPLE_CONFIGURATIONS    1

#define Group0			0
#define Group1			1
#define Group2			2
#define Group3			3
#define Group4			4
#define Group5			5
#define Group6			6
#define Group7			7

#define ADC_CONFIG_0 0

#define VAD_U12T 0
#define VAD_ISENSE_HS_RL 1
#define VAD_ISENSE_HS_RR 2
#define VAD_ISENSE_HS_FL 3
#define VAD_ISENSE_HS_FR 4
#define VAD_MUX2_OUT 5
#define VAD_MUX1_OUT 6
#define VAD_UINT 7
												                                                                                                                                                                                     
/************data ***********/

/* configuration -0 */
/*Prescale for ADC0*/
#define  ADC0_PRESCALE_0     0

/*Freeze Mode for ADC0*/
#define ADC0_FREEZEMODE_0                   FINISH_CURRENT

/*ADC Stopmode Conversion for ADC0 */
#define ADC0_STOPCONV_MODE_0               STOPCONV_OFF





/* define Group configuration for 8 groups :*/

/*Group configuration set 0 */
#define ADC_HW_UNIT_0                      ADC_HW_0
#define ADC_TRIGGER_SOURCE_0               ADC_TRIGG_SRC_SW_API
#define ADC_CONV_MODE_0                    ADC_CONV_MODE_ONESHOT
#define ADC_GROUP_LENGTH_0                 1
#define ADC_STARTING_CHANNEL_0             0
#define ADC_GROUPRESOLUTION_0              RESOLUTION_10_BIT
#define ADC_SAMPLE_TIME_0                  CONV_4
#define ADC_WRAP_AROUND_0                  15
#define ADC_DISCHARGE_BEF_SAMPLE_0               ON

/*Group configuration set 1 */
#define ADC_HW_UNIT_1                      ADC_HW_0
#define ADC_TRIGGER_SOURCE_1               ADC_TRIGG_SRC_SW_API
#define ADC_CONV_MODE_1                    ADC_CONV_MODE_ONESHOT
#define ADC_GROUP_LENGTH_1                 1
#define ADC_STARTING_CHANNEL_1             1
#define ADC_GROUPRESOLUTION_1              RESOLUTION_10_BIT
#define ADC_SAMPLE_TIME_1                  CONV_4
#define ADC_WRAP_AROUND_1                  15
#define ADC_DISCHARGE_BEF_SAMPLE_1               ON

/*Group configuration set 2 */
#define ADC_HW_UNIT_2                      ADC_HW_0
#define ADC_TRIGGER_SOURCE_2               ADC_TRIGG_SRC_SW_API
#define ADC_CONV_MODE_2                    ADC_CONV_MODE_ONESHOT
#define ADC_GROUP_LENGTH_2                 1
#define ADC_STARTING_CHANNEL_2             2
#define ADC_GROUPRESOLUTION_2              RESOLUTION_10_BIT
#define ADC_SAMPLE_TIME_2                  CONV_4
#define ADC_WRAP_AROUND_2                  15
#define ADC_DISCHARGE_BEF_SAMPLE_2               ON

/*Group configuration set 3 */
#define ADC_HW_UNIT_3                      ADC_HW_0
#define ADC_TRIGGER_SOURCE_3               ADC_TRIGG_SRC_SW_API
#define ADC_CONV_MODE_3                    ADC_CONV_MODE_ONESHOT
#define ADC_GROUP_LENGTH_3                 1
#define ADC_STARTING_CHANNEL_3             3
#define ADC_GROUPRESOLUTION_3              RESOLUTION_10_BIT
#define ADC_SAMPLE_TIME_3                  CONV_4
#define ADC_WRAP_AROUND_3                  15
#define ADC_DISCHARGE_BEF_SAMPLE_3               ON

/*Group configuration set 4 */
#define ADC_HW_UNIT_4                      ADC_HW_0
#define ADC_TRIGGER_SOURCE_4               ADC_TRIGG_SRC_SW_API
#define ADC_CONV_MODE_4                    ADC_CONV_MODE_ONESHOT
#define ADC_GROUP_LENGTH_4                 1
#define ADC_STARTING_CHANNEL_4             4
#define ADC_GROUPRESOLUTION_4              RESOLUTION_10_BIT
#define ADC_SAMPLE_TIME_4                  CONV_4
#define ADC_WRAP_AROUND_4                  15
#define ADC_DISCHARGE_BEF_SAMPLE_4               ON

/*Group configuration set 5 */
#define ADC_HW_UNIT_5                      ADC_HW_0
#define ADC_TRIGGER_SOURCE_5               ADC_TRIGG_SRC_SW_API
#define ADC_CONV_MODE_5                    ADC_CONV_MODE_ONESHOT
#define ADC_GROUP_LENGTH_5                 1
#define ADC_STARTING_CHANNEL_5             5
#define ADC_GROUPRESOLUTION_5              RESOLUTION_10_BIT
#define ADC_SAMPLE_TIME_5                  CONV_4
#define ADC_WRAP_AROUND_5                  15
#define ADC_DISCHARGE_BEF_SAMPLE_5               ON

/*Group configuration set 6 */
#define ADC_HW_UNIT_6                      ADC_HW_0
#define ADC_TRIGGER_SOURCE_6               ADC_TRIGG_SRC_SW_API
#define ADC_CONV_MODE_6                    ADC_CONV_MODE_ONESHOT
#define ADC_GROUP_LENGTH_6                 1
#define ADC_STARTING_CHANNEL_6             6
#define ADC_GROUPRESOLUTION_6              RESOLUTION_10_BIT
#define ADC_SAMPLE_TIME_6                  CONV_4
#define ADC_WRAP_AROUND_6                  15
#define ADC_DISCHARGE_BEF_SAMPLE_6               ON

/*Group configuration set 7 */
#define ADC_HW_UNIT_7                      ADC_HW_0
#define ADC_TRIGGER_SOURCE_7               ADC_TRIGG_SRC_SW_API
#define ADC_CONV_MODE_7                    ADC_CONV_MODE_ONESHOT
#define ADC_GROUP_LENGTH_7                 1
#define ADC_STARTING_CHANNEL_7             7
#define ADC_GROUPRESOLUTION_7              RESOLUTION_10_BIT
#define ADC_SAMPLE_TIME_7                  CONV_4
#define ADC_WRAP_AROUND_7                  15
#define ADC_DISCHARGE_BEF_SAMPLE_7               ON






#endif  /* end of #ifndef _ADC_CFG_H */ 

/*.............................END OF ADC_CFG.H..................................*/
