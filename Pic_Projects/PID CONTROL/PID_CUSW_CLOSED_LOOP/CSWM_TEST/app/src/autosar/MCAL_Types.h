/* Created at 15:25 on 1-4-2009 */
/* Version of HALCoGen used : 2.9.3 */
/* Code generated for       : MC9S12XS128 */

/*****************************************************************************/
/* Copyright(c) - This Program/Software is the exclusive property of Infosys.
The software may not be reproduced or given to third parties without prior
consent of Infosys.

     Filename                      : MCAL_Types.h
     Version                       : 1.0.1
     Microcontroller Platform      : MC9S12XS128
     Compiler                      : Codewarrior HCS12X V5.7.0

     Description                   : Includes the controller header
                                     (for register definitions) and common type
                                     definitions and macros.
                                     (Not AUTOSAR standard)

     Requirement Specification     : N.A.

     Platform Dependant[yes/no]    : yes

     To be changed by user[yes/no] : no
*/
/*****************************************************************************/
/* Revision History

Class of Change
-------------------------------------------------------------------------------
N - New Module.
F - Functional development.
E - Error Correction.
O - Optimization without functional change.
-------------------------------------------------------------------------------
 00      26-Feb-2007   Rose Paul , Infosys
-------------------------------------------------------------------------------
Class : E
Cause : DISABLE was defined as 0, but it is also used by the preprocessor
        (e.g. #pragma MESSAGE DISABLE ...).The preprocessor directive
        does not work after the inclusion of MCAL_types.h.
        PRL 689- Lint warning 502
Detail: Macros in Mcal_Types prefixed by "MCAL_".
        Suffixed hardcoded values,in expressions with unary operator,with 'u'
-------------------------------------------------------------------------------
******************************************************************************/

/* To avoid double inclusion */
#ifndef _MCAL_TYPES_H
#define _MCAL_TYPES_H


/*...................................File Includes...........................*/

#include "mc9s12xs128.h"

/* Infosys defines */

#define MCAL_ENABLE    1
#define MCAL_DISABLE   0
#define MCAL_CLEAR     0x00


typedef union
{
    uint8     all;
    struct
    {
        uint8 b0:1;
        uint8 b1:1;
        uint8 b2:1;
        uint8 b3:1;
        uint8 b4:1;
        uint8 b5:1;
        uint8 b6:1;
        uint8 b7:1;
    } bit;
    struct
    {
        uint8 p0:2;
        uint8 p1:2;
        uint8 p2:2;
        uint8 p3:2;
    } pair;
   struct
   {
        uint8 n0:4;
        uint8 n1:4;
   } nibble;
} Mcal_FlagsType;

#define  MCAL_BIT0_MASK             (uint8)1
#define  MCAL_BIT1_MASK             (uint8)2
#define  MCAL_BIT2_MASK             (uint8)4
#define  MCAL_BIT3_MASK             (uint8)8
#define  MCAL_BIT4_MASK             (uint8)16
#define  MCAL_BIT5_MASK             (uint8)32
#define  MCAL_BIT6_MASK             (uint8)64
#define  MCAL_BIT7_MASK             (uint8)128

#define MCAL_SETBITS(val,mask)      (val |= mask)

#define MCAL_CLRBITS(val,mask)      (val &= (~(mask)))

#define MCAL_GET_LOWER_BYTE(val)    ((val & 0x00ff))

#define MCAL_GET_UPPER_BYTE(val)    ((val & 0xff00) >> 8u)

#define MCAL_CLEAR_LOWER_BYTE(val)  ((val=val & 0xff00))

#define MCAL_CLEAR_UPPER_BYTE(val)  ((val=val & 0x00ff)) 

/*Clear Macros for flags*/
#define MCAL_CLEAR_BIT(arr, index)  (arr[index/8u] &= ~(uint8)(1u<<(index%8u)))

/*Get Macros for flags*/
#define MCAL_GET_BIT(arr, index)    (arr[index/8u] & (1u<<(index%8u)))

/*Set Macros for flags*/
#define MCAL_SET_BIT(arr, index)    (arr[index/8u] |= (1u<<(index%8u)))


/* End Infosys defines */

#endif
/*..........................END OF MCAL_Types.h..............................*/
