/* Created at 15:25 on 1-4-2009 */
/* Version of HALCoGen used : 2.9.3 */
/* Code generated for       : MC9S12XS128 */
/*****************************************************************************/
/* Copyright(c) - This Program/Software is the exclusive property of Infosys. 
The software may not be reproduced or given to third parties without prior 
consent of Infosys.

   Filename                        : Pwm_Cfg.c
   Version                         : 2.2.2
   Microcontroller Platform        : MC9S12XS128
   Compiler                        : Codewarrior HCS12X V5.7.0

   Description                     : This file contains the initialisation of 
                                     PWM configuration parameters.
   Requirement Specification       : AUTOSAR_SWS_PwmDriver.doc version 1.0.7
   
   Module Design                   : AUTOTMAL_DDD_PWM_SPAL2.0.doc version 0.1

   Platform Dependant[yes/no]      : yes
   
   To be changed by user[yes/no]   : no
*/

/*****************************************************************************/
   
/* Revision History

Class of Change
-------------------------------------------------------------------------------
N - New Module.
F - Functional development.
E - Error Correction.
O - Optimization without functional change.
-------------------------------------------------------------------------------
 00     02-Jan-2006     Muthuvelavan, Infosys
-------------------------------------------------------------------------------
Class: N
Cause: New Module
Detail:This file contains the initialisation of configuration parameters.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 01     02-Mar-2006     Muthuvelavan, Infosys
-------------------------------------------------------------------------------
Class: F
Cause: Functional development
Detail:Update for SPAL2.0 specification.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 02     24-Feb-2007     Khanindra , Infosys
-------------------------------------------------------------------------------
Class: N
Cause: 16 bit PWM implementation
Detail: Implemented 16 bit PWM and incorporated the review comments for S12XEP100 
        (Version 1.2.0)   
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 03     14-May-2007     Khanindra , Infosys
-------------------------------------------------------------------------------
Class: O
Cause: Vetsion is updated in sync with Pwm_Cfg.h
Detail: Vetsion is updated in sync with Pwm_Cfg.h 
******************************************************************************/

/* Module Header file */                
#include "Pwm.h"

/* Initialisation of variable of type Pwm_ConfigType */ 
/* Initialisation of Config */
const Pwm_ConfigType Pwm_Config[ PWM_MAX_CONFIG ]=
{/*Configuration Start*/
    { /* Config Start */
        /* PWM HW parameters */
        {   
            PWM_MODE_PWM_0,
            PWM_CHANNEL_ALIGNED_PWM_0,
            PWM_POLARITY_PWM_0,
            PWM_IDLE_STATE_PWM_0,
            PWM_CHANNEL_CLASS_PWM_0,
            PWM_PRESCALE_B_A_0,
            PWM_CLOCK_SELECT_0,
            /* Channel Config. */
            { 
                /* Channel 1 */
                {
                    PWM_PERIOD_DEFAULT_PWM1_0,
                    PWM_DUTYCYCLE_DEFAULT_PWM1_0
                },
                /* Channel 2 */
                {
                    PWM_PERIOD_DEFAULT_PWM2_0,
                    PWM_DUTYCYCLE_DEFAULT_PWM2_0
                },
                /* Channel 3 */
                {
                    PWM_PERIOD_DEFAULT_PWM3_0,
                    PWM_DUTYCYCLE_DEFAULT_PWM3_0
                }
            }/* End of PWM channel config.*/
        },/* End of PWM HW config.*/
        /* ECT HW parameters */
        {                            
            PWM_POLARITY_1_ECT_0,
            PWM_POLARITY_2_ECT_0,
            PWM_IDLE_STATE_1_ECT_0,
            PWM_IDLE_STATE_2_ECT_0,
            PWM_CHANNEL_CLASS_ECT_0, 
            {
                    PWM_NOTIFICATION_CBK_ECT3_0,
                    PWM_NOTIFICATION_CBK_ECT4_0,
                    PWM_NOTIFICATION_CBK_ECT5_0,
                    PWM_NOTIFICATION_CBK_ECT6_0,
                    PWM_NOTIFICATION_CBK_ECT7_0
            },
            /* Channel Config. */
            { 
                /* Channel 3 */
                {
                    PWM_PERIOD_DEFAULT_ECT3_0,
                    PWM_DUTYCYCLE_DEFAULT_ECT3_0
                },
                /* Channel 4 */
                {
                    PWM_PERIOD_DEFAULT_ECT4_0,
                    PWM_DUTYCYCLE_DEFAULT_ECT4_0
                },
                /* Channel 5 */
                {
                    PWM_PERIOD_DEFAULT_ECT5_0,
                    PWM_DUTYCYCLE_DEFAULT_ECT5_0
                },
                /* Channel 6 */
                {
                    PWM_PERIOD_DEFAULT_ECT6_0,
                    PWM_DUTYCYCLE_DEFAULT_ECT6_0
                },
                /* Channel 7 */
                {
                    PWM_PERIOD_DEFAULT_ECT7_0,
                    PWM_DUTYCYCLE_DEFAULT_ECT7_0
                }
            }/* End of ECT channel config. */
        }/* End of ECT HW config. */
    }/*Config End*/
};/*Configuration End*/

/*--------------------- End of Pwm_Cfg.c File -----------------------------------------*/
