/* Created at 15:25 on 1-4-2009 */
/* Version of HALCoGen used : 2.9.3 */
/* Code generated for       : MC9S12XS128 */
/*****************************************************************************/
/* Copyright(c) - This Program/Software is the exclusive property of Infosys.
The software may not be reproduced or given to third parties without prior
consent of Infosys.

Filename                        : os.h
Version                         : 1.0.0
Microcontroller Platform        : MC9S12XS128
Compiler                        : Codewarrior HCS12X V5.7.0

Description                     : This file contains Enable-Disable Interrupts Macros

Requirement Specification       : N.A.

 Module Design                  : N.A.

 Platform Dependant[yes/no]     : yes

To be changed by user[yes/no]   : no
*/
/*****************************************************************************/
#ifndef __OSEK__OS_H
#define __OSEK__OS_H

#define EnableAllInterrupts()   {_asm ("CLI");}		/* MODIFIED */
#define DisableAllInterrupts()  {_asm ("SEI");}		/* MODIFIED */

#endif
/*...............................END OF os.h................................*/
