/* Created at 15:24 on 1-4-2009 */
/* Version of HALCoGen used : 2.9.3 */
/* Code generated for       : MC9S12XS128 */
/*****************************************************************************/
/* Copyright(c) - This Program/Software is the exclusive property of Infosys. 
The software may not be reproduced or given to third parties without prior 
consent of Infosys.

   Filename                        : PwmIf_Cbk.c
   Version                         : 2.1.1
   Microcontroller Platform        : MC9S12XS128
   Compiler                        : Codewarrior HCS12X V5.7.0

   Description                     : This file contains the callback function 
                                     definitions.
   Requirement Specification       : AUTOSAR_SWS_PwmDriver.doc version 1.0.7
                
   Module Design                   : AUTOTMAL_DDD_PWM_SPAL2.0.doc version 1.0

   Platform Dependant[yes/no]      : yes   
     
   To be changed by user[yes/no]   : yes  
*/

/*****************************************************************************/
   
/* Revision History

Class of Change
-------------------------------------------------------------------------------
N - New Module.
F - Functional development.
E - Error Correction.
O - Optimization without functional change.
-------------------------------------------------------------------------------
 00     10-Jan-06     Muthuvelavan, Infosys
-------------------------------------------------------------------------------
Class: N
Cause: New Module
Detail:This file contains the callback function definitions.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 01     17-Apr-06     Muthuvelavan, Infosys
-------------------------------------------------------------------------------
Class: F
Cause: Specification update.
Detail:update for SPAL2.0
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 00     24-Feb-2007     Khanindra , Infosys
-------------------------------------------------------------------------------
Class: N
Cause: New Module
Detail:This file contains the PWM callback function definitions.
       MC9S12XEP100 PWM code(Version 1.2.0) is used as baseligned code.
-------------------------------------------------------------------------------
******************************************************************************/

#include "PwmIf_Cbk.h"
#include "TYPEDEF.h"
#include "Pwm_Stagger.h"

void Pwm_Notification_HFL(void)
{
    /* User can add his code here */
    StaggerF_Enable_Next_Ch(FL_TIM_CH);  
}

void Pwm_Notification_HFR(void)
{
    /* User can add his code here */     
    StaggerF_Enable_Next_Ch(FR_TIM_CH);  
}

void Pwm_Notification_HRL(void)
{
    /* User can add his code here */     
    StaggerF_Enable_Next_Ch(RL_TIM_CH);  
}

void Pwm_Notification_HRR(void)
{
    /* User can add his code here */     
    StaggerF_Enable_Next_Ch(RR_TIM_CH);  
}

/*------------- End of File --------------------------------------------------*/
