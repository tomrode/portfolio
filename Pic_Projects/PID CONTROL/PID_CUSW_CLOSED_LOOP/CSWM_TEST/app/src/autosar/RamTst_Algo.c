/* Created at 15:24 on 1-4-2009 */
/* Version of HALCoGen used : 2.9.3 */
/* Code generated for       : MC9S12XS128 */
/*****************************************************************************/
/* Copyright(c) - This Program/Software is the exclusive property of Infosys. 
The software may not be reproduced or given to third parties without prior 
consent of Infosys.

     Filename                      : RamTst_Algo.c
     Version                       : 1.0.4
     Microcontroller Platform      : MC9S12XS128
     Compiler                      : Compiler for Freescale HC12 V5.0.30

     Description                   : This file contains all functions of 
                                     algorithms of RAM Test module 
                                     
     Requirement Specification     : AUTOSAR_SWS_RAMTest.doc 0.12
                
     Module Design                 : AUTOTMAL_DDD_RAMTest_SPAL2.0.doc version 
                                     version 2.00
     
     Platform Dependant[yes/no]    : yes
     
     To be changed by user[yes/no] : no
*/
/*****************************************************************************/
   
/* Revision History

Class of Change
-------------------------------------------------------------------------------
N - New Module.
F - Functional development.
E - Error Correction.
O - Optimization without functional change.
-------------------------------------------------------------------------------
 00      09-May-2006     Khanindra Jyoti Deka, Infosys
-------------------------------------------------------------------------------
Class: N
Cause: New Module
Detail:This file implements the RAM Test algorithms.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
01      26-Mar-2007   Rose Paul , Infosys
-------------------------------------------------------------------------------
Class : E
Cause : DISABLE was defined as 0, but it is also used by the preprocessor
        (e.g. #pragma MESSAGE DISABLE ...).The preprocessor directive
        does not work after the inclusion of MCAL_types.h.
Detail: Macros in Mcal_Types prefixed by "MCAL_".
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
02      25-Jul-2007   Khanindra Jyoti Deka , Infosys
-------------------------------------------------------------------------------
Class : O
Cause : To support banked memory module as well.
Detail: __far pointer added at all necessary fields.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
03      27-Nov-2007   Khanindra Jyoti Deka , Infosys
-------------------------------------------------------------------------------
Class : E
Cause : Changes done for the issue 858
Detail: Functions MarchTest,WalkPathTest,GalpatTest,TransparentGalpatTest,
        ModifiedHammingCodeTest,CRC_Generation are updated for the RAM end 
        Address.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
04      20-Mar-2008   Khanindra Jyoti Deka, Infosys
-------------------------------------------------------------------------------
Class: F
Cause: Redirection of local variables to DATASAVE_RAM is removed.
Detail:Redirection of local variables to DATASAVE_RAM is removed from all 
       algorithms 
-------------------------------------------------------------------------------

******************************************************************************/

/*...................................File Includes...........................*/

/* Standard Types file */
#include "Std_Types.h"
/* Infosys defines header file */
#include "MCAL_Types.h"

/* RAM Test Configuration header file */
#include "RamTst_Cfg.h" 

/* Module header file */
#include "RamTst_Algo.h"

/* OS header file */
#include "os.h"

/*...................................Macros..................................*/
#define NOT_REPEAT  1
#define REPEAT      2
#define COMPLETE    3
#define FIRST_RUN   1
#define SECOND_RUN  2
#define NEW_BYTE    0
#define END_BIT     8
#define POLY16      0x1021      /* The used polynimial is 0x11021 */
#define RAM_END_ADDRESS (0xFFFFF)

/* ..........................Global variables................................*/

#pragma section [safe_ram]
//#pragma DATA_SEG DATASAVE_RAM
/* Starting RAM location for Datasave */
volatile uint8                 RamTst_DataSave1;

/* Ending RAM location for Datasave */
volatile uint8                 RamTst_DataSave2;

/* These global variables have external linkages */
/* Block Starting Address For The Algorithm */
RamTst_BlockAddressType        RamTst_BlockStartAddressForAlgo;

/* Block Ending Address For The Algorithm */
RamTst_BlockAddressType        RamTst_BlockEndAddressForAlgo;

/* Block ID For The Algorithm */
RamTst_BlockIDType             RamTst_BlockIDForAlgo;

/* RAM Test Results */
RamTst_TestResultType          *RamTst_Result;

uint8 *__far RamTst_CRC_Addr=FALSE;

//#pragma DATA_SEG DEFAULT

/* These global variables have internal linkages */

/*..........................Local Function Definitions ......................*/

/******************************************************************************
* Function Name             : CheckerboardTest
*
* Arguments                 : None 
*
* Description               : This function implements the Checkerboard Test 
*                             Algorithm.A checker-board type pattern of 0s and 
*                             1s are written into the cells of a bitoriented 
*                             memory.The cells are then inspected in pairs to 
*                             ensure that the contents are same and correct. 
*                             The address of the first cell of such a pair is 
*                             variable and the address ofthe second cell of the
*                             pair is formed by inverting bitwise the first 
*                             address. In the first run,the address range of 
*                             the memory is run towards higher addresses from 
*                             the variable address, and in a second run towards
*                             lower addresses. Both runs are then repeated with
*                             an inverted preassignment.A failure message is 
*                             produced if any difference occurs.
*
*                               
* Return Type               : None
*
* Requirement numbers       : RamTst050
******************************************************************************/
void CheckerboardTest( void ) 
{
    /* Local variable declarations and initializtion*/
    uint8 Variable_1;
    uint8 Variable_2;
    uint8 Repeat;
    uint8 Run;
    RamTst_BlockAddressType RamTst_Address;
    RamTst_BlockAddressType StartAddress;
    uint8 *__far Address1=0;
    uint8 *__far Address2=0;

    /* Initialization of the local variables */
    Variable_1=0x55;
    Variable_2=0xAA;
    Repeat = NOT_REPEAT;
    Run = FIRST_RUN;

    /* Take the start address for the test */
    StartAddress = RamTst_PresentAddress;  
    /* Check whether the cycle is complete */
    while(COMPLETE != Repeat) 
    {
  
        /* Take the RAM Test present Address */
        Address1 = (uint8 *__far)RamTst_PresentAddress;
    
        /* Take another cell whose address is bitwise inverse of the 
        first addres 
        Address1 */
        RamTst_Address = RamTst_PresentAddress;
        RamTst_Address = (~RamTst_Address)& 0x000FFFFF;
        Address2 = ((uint8 *__far)RamTst_Address);
    
        /* Is Address2 in range */
        if((Address2 >= (uint8 *__far)RAMTST_BLOCK_START_ADDRESS) && (Address2 <=\
        (uint8 *__far)RAMTST_BLOCK_END_ADDRESS)) 
        {
            /* Disable Interrupts */
            DisableAllInterrupts();
      
            /* Save contents of Address1 in RamTst_DataSave1 */
            RamTst_DataSave1 = (uint8)*Address1;       
            /* Save contents of Address2 in RamTst_DataSave2 */
            RamTst_DataSave2 = (uint8)*Address2;        
      
            /* Write Variable_1 to the RAM location of Address1 */
            *Address1 = Variable_1;
      
            /* Write Variable_2 to the RAM location of Address2 */
            *Address2 = Variable_2;
      
            /* Check if Variable_2 is not equal to contents of Address2 */
            if(Variable_2 != *Address2) 
            {
                /* Enable Interrupts */
                EnableAllInterrupts();
        
                /* Update RAM Test result */
                RamTst_Result[PRESENT_BLOCK_ID] = RAMTST_RESULT_NOT_OK;
        
                /* Re-store RamTst_DataSave2 to Address2 */
                *Address2 = RamTst_DataSave2;
                /* End */
                return;
            }
            /* Re-store RamTst_DataSave2 to Address2 */
            *Address2 = RamTst_DataSave2;
        }/* End of Is Address2 in range */
        else        /* If Address2 is not in range */
        {
            /* Disable Interrupts */
            DisableAllInterrupts();
      
            /* Save contents of Address1 in RamTst_DataSave1 */
            RamTst_DataSave1 = (uint8)*Address1;       
      
            /* Write Variable_1 to the RAM location of Address1 */
            *Address1 = Variable_1;
        } /* End of If Address2 is not in range */
    
        /* Check if Variable_1 is not equal to contents of Address1 */
        if(Variable_1 != *Address1) 
        {
            /* Enable Interrupts */
            EnableAllInterrupts();
        
            /* Update RAM Test result */
            RamTst_Result[PRESENT_BLOCK_ID] = RAMTST_RESULT_NOT_OK;
      
            /* Re-store RamTst_DataSave2 to Address2 */
            *Address1 = RamTst_DataSave1;
            /* End */
            return;
        }
        /* Re-store RamTst_DataSave1 to Address1 */
        *Address1 = RamTst_DataSave1;
    
        /* Enable Interrupts */
        EnableAllInterrupts();
    
        /* Check Run */    
        if(Run == FIRST_RUN) 
        {
            /* Increment the Present Address */
            ++RamTst_PresentAddress;
            /* Increment the RamTst_TestedCellCounter */
            ++RamTst_TestedCellCounter;
      
            /* Check if the Address1 reaches the End block address or the test 
            complets the number of cells */
            if (!((RamTst_PresentAddress <=RAMTST_BLOCK_END_ADDRESS) &&\
            (RamTst_TestedCellCounter <= RamTst_PresentNumberOfTestedCell-1))) 
            {
                /* Change Run Status */
                Run = SECOND_RUN;
        
                /* Clear RamTst_TestedCellCounter */
                RamTst_TestedCellCounter = NO_CONTENT;
        
                /* Put the present address in a local variable for future use*/
                --RamTst_PresentAddress;
            }
        } /* End of if(Run == FIRST_RUN) */
    
        else  /* If Run == SECOND_RUN */
        {
            /* Decreament the Address1 */
            --RamTst_PresentAddress;
      
            /* Check whether Address1 has reached the starting address */
            if(RamTst_PresentAddress < StartAddress) 
            {
                /* Repeat the test for inverted pre-assignment */
                if(Repeat == NOT_REPEAT) 
                {
                    /* Increment the Present Address */
                    ++RamTst_PresentAddress;
                    /* Write 0xAA to Variable_1 */
                    Variable_1=0xAA;
                    /* Write 0x55 to Variable_2 */
                    Variable_2=0x55;
                    /* Update Repeat to REPEAT */
                    Repeat = REPEAT;
                    /* Update Run to FIRST_RUN */
                    Run = FIRST_RUN;
                } 
                else if (Repeat == REPEAT)
                {
                    /* Update Repeat to COMPLETE */
                    Repeat = COMPLETE;
                }
            }
        } /* End of If Run == SECOND_RUN */
    
    } /* End of while(Repeat != COMPLETE) */
    RamTst_PresentAddress= (StartAddress+ RamTst_PresentNumberOfTestedCell-1);
    return;
}

/*...........................End of RamTst_Algo.c............................*/
#pragma section []
