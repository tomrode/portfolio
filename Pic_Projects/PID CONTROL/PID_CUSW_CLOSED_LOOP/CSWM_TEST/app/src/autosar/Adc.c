/* Created at 15:24 on 1-4-2009 */
/* Version of HALCoGen used : 2.9.3 */
/* Code generated for       : MC9S12XS128 */

/******************************************************************************/
/* Copyright(c) - This Program/Software is the exclusive property of Infosys.
The software may not be reproduced or given to third parties without prior
consent of Infosys.

     Filename                      : Adc.c
     Version                       : 5.0.6
     Microcontroller Platform      : MC9S12XS128
     Compiler                      : Codewarrior HCS12X V5.0.28

     Description                   : This file contains macros,type definitions,
                                     declaration of global variables,
                                     declaration of static global variables,
                                     delaration of local functions, and function
                                     definitions of Adc Driver Module.

     Requirement Specification     : AUTOSAR_SWS_ADC_Driver.pdf version 1.0.16

     Module Design                 : AUTOSAR_DDD_ADC_SPAL2.0.doc version 2.11

     Platform Dependant[yes/no]    : yes

     To be changed by user[yes/no] : yes
*/
/******************************************************************************/
/*
 Revision History

Class of Change
-------------------------------------------------------------------------------
N - New Module.
F - Functional development.
E - Error Correction.
O - Optimization without functional change.
-------------------------------------------------------------------------------
 00      06-Jan-2006     Kapil Kumar  Infosys
-------------------------------------------------------------------------------
Class: N
Cause: New Module
Detail:This file implements the ADC driver.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 01     29-Mar-2006      Kapil Kumar, Infosys
-------------------------------------------------------------------------------
Class: F
Cause: Changes for SPAL2.0
Detail:Changes done as per AUTOSAR_SWS_AdcDriver.pdf ver 1.0.16
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 02     14-Jun-2006      Karthik, Infosys
-------------------------------------------------------------------------------
Class: E,O
Cause: Changes done for Anomalies reported in RTRT
Detail: The changes are done as per RTRT anomaly list
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 03     10-July-2006     Karthik, Infosys
-------------------------------------------------------------------------------
Class:  E,O
Cause:  File version information macro renaming & Issue reported from Customer
        testing(284,266,200,245,246,256,292)
Detail: ADC_SW_MAJOR_VERSION_H and related macros is renamed
        as ADC_SW_MAJOR_VERSION etc.
        Changes are done to APIs Adc_StartGroupConversion & Adc_GatedSetUp.
        Issue List comments have all the details of the changes done.
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
 04     04-Aug-2006      Karthik, Infosys
-------------------------------------------------------------------------------
Class:  E
Cause:  Issue fixes 245,348,349,350,357
Detail: Power down options in wait mode is configured correctly.
ADC107 added to Adc_GatedSetup API.
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
 05     04-Aug-2006      Karthik, Infosys
-------------------------------------------------------------------------------
Class:  E
Cause:  RTRT anamoly fix
Detail:
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
 06     21-Aug-2006      Karthik, Infosys
-------------------------------------------------------------------------------
Class:  E
Cause:  Lint Analysis
Detail:
-------------------------------------------------------------------------------
 07     08-Dec-2006      Kasinath Hegde  Infosys
-------------------------------------------------------------------------------
Class: F
Cause: Changes for S12XEP100
Detail:This file implements the ADC driver changes required for S12XEP100
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 08     22-Jan-2007      Kasinath Hegde,  Infosys
-------------------------------------------------------------------------------
Class:  F
Cause:  ADC HW Unit 0 to handle additional 8 Channels.
Detail: S12XEP100-ADC has 2 ADC HW Units 16 channels each. Code modified to
            handle additional 8 channels in ADC HW Unit 0.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 09     23-Mar-2007      Rajesh,  Infosys
-------------------------------------------------------------------------------
Class:  E
Cause: Bug Fixes - 530, 565, 676, 552, 682, 683, 686
Detail: Only configured HW units are initialized in Adc_Init.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 10     03-May-2007      Rajesh,  Infosys
-------------------------------------------------------------------------------
Class:  E
Cause:  Adc_GetGatedLastPointer returns next address to the last converted value
Detail: UserGatedBuffer-1 is returned.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 11     18-Oct-2007      Ajaykumar,  Infosys
-------------------------------------------------------------------------------
Class:  E
Cause:  Macro ATD0CTL2_AWAI_MASK not decalred in mc9s12xs128.h
Detail: ATD0CTL2_AWAI_MASK changed with macro ATD0CTL2_ICLKSTP.
        Preprocessor switch added for S12X128
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 12     31-Dec-2007      Ajaykumar,  Infosys
-------------------------------------------------------------------------------
Class:  E, O
Cause:  PRL issues #897,#885 and #877
Detail: 1. API's Adc_StartGroupConversion,Adc0_ConversionCompleteIsr,
           Adc1_ConversionCompleteIsr, Adc_SingleValueReadChannel and 
           Init_Variables updated for PRL #885. 
           Converted value is stored in Adc_Converted_Value array in Isr's
           and can be read by using function Adc_SingleValueReadChannel.
        2. Adc_StartOnDemandConversion API updated for PRL #897. Driver now
           prevents Ondemand conversion from suspending a streaming conversion.
        3. Preprocessor switches added for code optimization for PRL#877. 
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 13    31-Mar-2008       Ajaykumar, Infosys
-------------------------------------------------------------------------------
Class: E
Cause: Changes done for the issue #978
Detail:#978: Pre compiler check for SW Patch Version is removed.
-------------------------------------------------------------------------------

******************************************************************************/

/*...................................File Includes...........................*/


/* DEM header file */
/* Module header */
#include "Adc.h"

/*...................................Macros..................................*/       
#define ADC_SW_MAJOR_VERSION_C                     5
#define ADC_SW_MINOR_VERSION_C                     0
#define ADC_SW_PATCH_VERSION_C                     6
#define ADC_AR_MAJOR_VERSION_C                     1
#define ADC_AR_MINOR_VERSION_C                     0
#define ADC_AR_PATCH_VERSION_C                     16

/*...............................version Check...............................*/
#if (ADC_SW_MAJOR_VERSION==ADC_SW_MAJOR_VERSION_C)
#if (ADC_SW_MINOR_VERSION==ADC_SW_MINOR_VERSION_C)
#if (ADC_AR_MAJOR_VERSION==ADC_AR_MAJOR_VERSION_C)
#if (ADC_AR_MINOR_VERSION==ADC_AR_MINOR_VERSION_C)
#if (ADC_AR_PATCH_VERSION==ADC_AR_PATCH_VERSION_C)

/*.............................Local Macros..................................*/
/* Define Identifiers for ADC HW Units */
#define ADC_0          ((uint8)0U)


/*Define Power ON reset values for ADC_0*/
#define ADC_0_CTL0_DEFAULT_VALUE                 ((uint8)0x07U)
#define ADC_0_CTL1_DEFAULT_VALUE                 ((uint8)0x27U)
#define ADC_0_CTL2_DEFAULT_VALUE                 ((uint8)0x00U)
#define ADC_0_CTL3_DEFAULT_VALUE                 ((uint8)0x20U)
#define ADC_0_CTL4_DEFAULT_VALUE                 ((uint8)0x05U)
#define ADC_0_CTL5_DEFAULT_VALUE                 ((uint8)0x00U)

/*define no. of channels in ADC_0 */
#define ADC_CHANNELS_IN_ADC_0                ((uint8)8U)

/*define Mask for channel sequence length */
#define ADC_ATDCTL3_SC_MASK   ((uint8)0x78U)

/*default value for result buffer */
#define ADC_BUFFER_EMPTY    (0xFFFFU)

/* Default Illegal Group */
#define ADC_INVALID_GROUP ((uint8)0xFFU)

/*The size of each Status Flag.*/
#define ADC_STAT_FLAG_SIZE          8

#define ADC_0_BEGIN_CHANNEL_INDEX   0

#define ADC_1_BEGIN_CHANNEL_INDEX   16

/*Macro to set a particular register bit to the user configered value */
#define  ADC_Set_RegisterBit(register_value,config_value,mask) \
         (config_value>=1?(register_value|mask): \
         (register_value&(~mask)))

/*Macro to set multiple bits of a register to the user configered value */
/*corresponding bits have to be cleared before using this macro*/
#define  ADC_Set_RegisterMultipleBits(register_value,config_value,mask) \
         ((register_value | (mask*config_value)))


/*........................End  of Macros ........................*/

/*........................Start of enums ........................*/

typedef enum {
                ADC_0_CHANNEL_RESULT_REGISTER_L = 0,
                ADC_NO_OF_CONVCOMPLETE_REG
             };
/*........................End of enums ........................*/

/* ..........................Global variables................................*/

/* These global variables have external linkages */

/* define a local static variable to access Adc_UserConfig elements in Adc
   functions */
 static const Adc_ConfigType* Adc_UserConfig;

  /*variable to store previously started group no. */
 static uint8 Prev_Group;

 /*Variable array for storing started group conversions */
 static Adc_ConversionStatusType Adc_Conversion_StartFlag[ADC_CONFIGURED_GROUPS];

 /*Starting Channel for ADC_0 & ADC_1*/
 static uint8 Adc_Starting_Channel[NO_OF_HW_UNITS];

 /*Conversion sequence length for ADC_0 & ADC_1*/
 static uint8 Adc_Seq_Length[NO_OF_HW_UNITS];

/*Variable to store converted channelno.[0]->ch0-7,[1]->ch8-15,[2]->ch16-23*/
static uint8 Adc_Converted_Channels[ADC_NO_OF_CONVCOMPLETE_REG];


/*Variable array to hold started group no.*/
static uint8 Adc_StartedGroup[NO_OF_HW_UNITS];

static Adc_ValueType Adc_Converted_Value[ADC_MAX_NO_OF_CHANNELS];

/*.........................END Local static variable declaration .............*/


/*..........................Local functions Declarations..................................*/


/* initialise static/global variables */
void Init_Variables(void);
/* initialise ADC_0 ADC hardware */
void Init_HW0(void);


/*..........................End Local functions Declarations..................................*/


/*..........................Local functions Definitions..................................*/

/*****************************************************************************
* Function Name              : Init_Variables
*
* Arguments                 : None

* Description                : This function Initializes the static
*                              variables to be used between different APIs
*                              of ADC driver.
* Return Type                : void
*
******************************************************************************/

void Init_Variables(void)
{
    uint8 Temp;

    for (Temp=0;Temp<NO_OF_HW_UNITS;Temp++)
    {
  		/*starting channel for ADC_0 & ADC_1 HW */
  		Adc_Starting_Channel[Temp]= FALSE;
      	/*sequence length for ADC_0 & ADC_1 HW */
  		Adc_Seq_Length[Temp]= FALSE;

  	  /*Previous Group initialised to 0(to avoid taking garbage Values)*/
  		Prev_Group=FALSE;



    }

    for (Temp=0;Temp<ADC_CONFIGURED_GROUPS;Temp++)
    {
        /*clear conversion start flags for all groups */
        Adc_Conversion_StartFlag[Temp] = ADC_CONV_NOT_STARTED;
    }

    /*clear conversion complete channel flags */
    for (Temp=0;Temp<ADC_NO_OF_CONVCOMPLETE_REG;Temp++)
    {
        Adc_Converted_Channels[Temp]  = FALSE;
    }


    for(Temp=0;Temp<ADC_MAX_NO_OF_CHANNELS;Temp++)
    {
        Adc_Converted_Value[Temp]= OUT_OF_RANGE;
    }

}


/*****************************************************************************
* Function Name              : Init_HW0
*
* Arguments                 : const Adc_ConfigType *ConfigPtr
*                              Pointer to the runtime configuration structure.
*
*                              The parameter is a pointer to the
*                              controller specific configuration
*                              that shall be initialized.Different sets of
*                              static configuration
*                              that shall be initialized.
*
* Description                : This function Initializes the
*                              controller specific hardware settings
*                              for ADC HW Unit 0 (ADC_0) controller as
*                              specified by the register set pointer.
*                              Other registers are set to default values
*
* Return Type                : void
******************************************************************************/


void Init_HW0(void)
{

    /*Initialise control register 0for wrap around channel(No wrap around)*/
    ATD0CTL0 = ADC_0_CTL0_DEFAULT_VALUE;

    /*Initialise control register 1 */
    ATD0CTL1 = ADC_0_CTL1_DEFAULT_VALUE;


    /*Initialize control register 3 for non FIFO , Freeze mode and default
    seq. length*/
    ATD0CTL3 = ADC_0_CTL3_DEFAULT_VALUE |(uint8)Adc_UserConfig->
                                                   Adc_Freeze_Mode[ADC_0];

    /*Initialise Prescale value for control register 4 */
    ATD0CTL4 = (uint8)Adc_UserConfig->ADC_PRESCALE[ADC_0];

    /*Initialise control register 2 for ADC Conversion in MCU STOP Mode,
        disable HW trigger and enable interrupt */
    ATD0CTL2 = (uint8) (ADC_0_CTL2_DEFAULT_VALUE | ADC_Set_RegisterBit(\
         ATD0CTL2, Adc_UserConfig->Adc_StopConv[ADC_0],ATD0CTL2_ICLKSTP) \
         | ATD0CTL2_ASCIE_MASK);


}




/*..........................End Local functions Definitions..................................*/


/*..........................API Definitions..................................*/

/*****************************************************************************
* Function Name              : Adc_Init
*
* Arguments                 : const Adc_ConfigType *ConfigPtr
*                              Pointer to the runtime configuration structure.
*
*                              The parameter is a pointer to the
*                              controller specific configuration
*                              that shall be initialized.Different sets of
*                              static configuration
*                              that shall be initialized.
*
* Description                : This function Initializes the
*                              controller specific hardware settings
*                              for the ADC controller
*                              specified by the register set pointer.
*
* Return Type                : void
*
* Requirement numbers        : ADC054 ADC056 ADC150 ADC151 ADC147 ADC077 ADC132
******************************************************************************/

void Adc_Init(const Adc_ConfigType *ConfigPtr)
{
        

    /* detect development errors */

    /* initialise Static and Global variables , clear notification flags */
    Init_Variables();

    /* copy contents of ConfigPtr structure to a Global variable */
    Adc_UserConfig = ConfigPtr;

    /*<ADC054> <ADC056> <ADC077> initialise  ADC_0 HW  as per user config
    and Private data.*/
    Init_HW0();




}
/*****************************************************************************
* Function Name              : Adc_StartGroupConversion
*
* Arguments                 : Adc_GroupType group
*
* Description                : This function starts the conversion for
*                              SW API triggered ADC in one shot or
*                              continuous conversion mode
*
* Return Type                : void
*
* Requirement numbers        : ADC061 ADC060 ADC125 ADC086 ADC133 ADC107 ADC154
******************************************************************************/

void Adc_StartGroupConversion(Adc_GroupType group)
{

    /*variable to collate control word of ADC register */
    uint8 ControlByte;
    uint8 conversion_mode=0;
    uint8 index;
    uint8 channel;


    /* report development errors */


    {



                 /*Start Conversion for ADC HW Unit 0 (ADC_0) */

            /***************************************************************
            Set control words to CTL0..4,based on the configuration  settings
            *****************************************************************/
                         /*set wrap around in CTL0*/

            ATD0CTL0 =(uint8) Adc_UserConfig->Adc_GrpCfg[group].
                            ADC_GROUP_DEFINITION.Adc_Wrap_around;

            /*Set conversion sequence length */
            Adc_Seq_Length[ADC_0] = Adc_UserConfig->Adc_GrpCfg[group] \
                                              .ADC_GROUP_DEFINITION.Adc_TotalCh;
            ControlByte = ATD0CTL3;

            /*Reset sequence length bits*/
            ControlByte = (uint8)(ControlByte & (~ADC_ATDCTL3_SC_MASK));

            ControlByte =(uint8)(ControlByte |ATD0CTL3_DJM_MASK \
                           |ADC_Set_RegisterMultipleBits \
                     (ControlByte,Adc_Seq_Length[ADC_0],ATD0CTL3_S1C_MASK));

            ATD0CTL3 =  ControlByte;

            /*set channel resolution, sample time*/
           /************* SET CHANNEL RESOLUTION ********************/
            ControlByte = ATD0CTL1;

            /*CHANNEL RESOLUTION*/
            ControlByte = ControlByte & (~ATD0CTL1_SRES_MASK);

            ControlByte =(uint8)(ControlByte |ADC_Set_RegisterMultipleBits(\
                            ControlByte, Adc_UserConfig->Adc_GrpCfg[group].\
                            ADC_GROUP_DEFINITION.ADC_CHANNEL_RESOLUTION,\
                            ATD0CTL1_SRES0_MASK));

           /************* DIS_SMP********************/
            ControlByte |=ADC_Set_RegisterBit(ControlByte,
                            Adc_UserConfig->Adc_GrpCfg[group].ADC_GROUP_DEFINITION.\
                            Adc_Discharge,ATD0CTL1_SMP_DIS_MASK);
            ATD0CTL1 = ControlByte;
           /************* SET SAMPLE TIME********************/
            ControlByte = ATD0CTL4;
                        ControlByte = ControlByte &(~ATD0CTL4_SMP_MASK);

            ControlByte = ControlByte |ADC_Set_RegisterMultipleBits( \
                           ControlByte,Adc_UserConfig->Adc_GrpCfg[group]. \
                           ADC_GROUP_DEFINITION.ADC_CHANNEL_CONV_TIME, \
                           ATD0CTL4_SMP0_MASK);

            ATD0CTL4 = ControlByte;

            /*Set starting channel and conversion mode */
            Adc_Starting_Channel[ADC_0] =(Adc_UserConfig->Adc_GrpCfg[group]
                                       .ADC_GROUP_DEFINITION.Adc_Start_Channel);


            /* provide group no. for notification call back function,
              if notification is enabled */


	       /*store group no. for  start of conversion */
		   Adc_StartedGroup[ADC_0] = group;
		   /*clear results channels flags*/
		   Adc_Converted_Channels[ADC_0_CHANNEL_RESULT_REGISTER_L] =MCAL_CLEAR;
           Adc_Conversion_StartFlag[group] = ADC_CONV_STARTED;
           /*Set control word for CTL register5 */
		    ControlByte = MCAL_CLEAR;
			if((Adc_UserConfig->Adc_GrpCfg[group].ADC_GROUP_CONV_MODE == ADC_CONV_MODE_ON_DEMAND) ||
									   (Adc_UserConfig->Adc_GrpCfg[group].ADC_GROUP_CONV_MODE == ADC_CONV_MODE_ONESHOT))

			{
			  conversion_mode = 0;
			}
			else
			{
			   conversion_mode = 1;
			}
			   ControlByte =ControlByte |ADC_Set_RegisterBit(ControlByte,\
						  conversion_mode, ATD0CTL5_SCAN_MASK)\
						  | ATD0CTL5_MULT_MASK | Adc_Starting_Channel[ADC_0];

        channel = Adc_UserConfig->Adc_GrpCfg[group].ADC_GROUP_DEFINITION.Adc_Start_Channel; 

        /* Set the Converted value for channels for which conversion to be started
           as  OUT_OF_RANGE, indicating conversion in progress */
        for(index=0;
            index<Adc_UserConfig->Adc_GrpCfg[group].ADC_GROUP_DEFINITION.Adc_TotalCh;
            index++)
        {
    
            Adc_Converted_Value[channel]=OUT_OF_RANGE;
        
            channel = ((channel < Adc_UserConfig->Adc_GrpCfg[group].ADC_GROUP_DEFINITION.Adc_Wrap_around))?++channel:ADC_0_BEGIN_CHANNEL_INDEX;
    
        }

        	/*write to ctl register 5 to start conversion */

            if(Adc_Seq_Length[ADC_0]> 1)
            {
                ATD0CTL5 = ControlByte;
            }
            else
            {
                ATD0CTL5 = ControlByte & (~ATD0CTL5_MULT_MASK);
            }

        }

    /* store previous group no. to check for busy state in the next conversion*/
    Prev_Group = group;


}
/*****************************************************************************
* Function Name              : Adc_SingleValueReadChannel
*
* Arguments                  : Adc_ChannelType channel
*
* Description                :  This function reads the converted ADC value
*                               of the specified channel.
*
* Return Type                : Adc_ValueType
*
* Requirement numbers        : ADC075 ADC113 ADC122 ADC076 ADC152

Note:                        : ADC106 is implemented as the part of HW init.

******************************************************************************/

Adc_ValueType Adc_SingleValueReadChannel(Adc_ChannelType channel)
{
    

    /* <ADC075> <ADC113> Get the result register value */
    /*<ADC244>return OUT_OF_RANGE,if read happens before end of conversion*/

    return(Adc_Converted_Value[channel]);


}
/*****************************************************************************
* Function Name              : Adc_GetGroupStatus
*
* Arguments                  : Adc_GroupType group
*                              The parameter is the group no. for the result.

* Description                : This Service reads Adc group status.
* Return Type                : Adc_StatusType:
                               ADC_IDLE ,ADC_BUSY_NO_VALUE,ADC_BUSY,
                               ADC_COMPLETED
*
* Requirement numbers        : ADC220 ADC221 ADC222 ADC223 ADC224 ADC225
******************************************************************************/


Adc_StatusType Adc_GetGroupStatus (Adc_GroupType group)
{
    Adc_StatusType AdcStatus;

    /*Store default Adc status */
    AdcStatus = ADC_IDLE;


     /* report development errors */

    /*ADC221: : This service shall return ADC_IDLE if the method is called
     before any conversion of the specified group is started.*/
    if( ADC_CONV_NOT_STARTED == Adc_Conversion_StartFlag[group])
    {
        AdcStatus = ADC_IDLE;
    }
    else if(ADC_CONV_FINISHED ==Adc_Conversion_StartFlag[group])
    {
        /*ADC224: :return ADC_COMPLETED if the conversion of the specified group
        is finished*/
        AdcStatus = ADC_COMPLETED;
    }

    /*ADC222:  This service shall return ADC_BUSY_NO_VALUE if the method
     is called while the first conversion of the specified group (after
     Adc_Init method has been called) is on going.*/

			  /* Check for conversion complete channels for ADC HW Unit 0 (ATD0) */
			  else if (Adc_UserConfig->Adc_GrpCfg[group].\
			   ADC_GROUP_DEFINITION.Adc_Start_Channel < ADC_CHANNELS_IN_ADC_0)
			  {

					 /*For ATD0, whatever may the total channels be ATD0STAT2L is set
					  1st followed by ATD0STAT2H.Checking the 1st flag is enough*/
					 if(MCAL_CLEAR==\
					  Adc_Converted_Channels[ADC_0_CHANNEL_RESULT_REGISTER_L])
				     {
					     AdcStatus = ADC_BUSY_NO_VALUE;
				     }
                  /*ADC223:return ADC_BUSY while the specified group conversion
                  is on going (except the first conversion).*/
    			     else if(ADC_CONV_STARTED == Adc_Conversion_StartFlag[group])
				     {
					     AdcStatus = ADC_BUSY;
				     }
				    else{}

			 }
			  return AdcStatus;


}
/*****************************************************************************
* Function Name              : Adc_GetVersionInfo
*
* Arguments                  : Std_VersionInfoType *versioninfo
*
* Description                : This API stores the version information of the
                               ADC module in the variable pointed by the pointer
                               passed to the function. The version information
                               include
                               1.Module ID
                               2.Version ID
                               3.Vendor specific version numbers.
*
* Return Type                : void
*
* Requirement numbers        : ADC236
******************************************************************************/


void Adc_GetVersionInfo(Std_VersionInfoType *versioninfo)
{


     if(NULL_PTR != versioninfo)
	 {
	        /* Copy MODULE ID */
	        versioninfo->moduleID=ADC_MODULE_ID;
	        /* Copy VENDOR ID */
	        versioninfo->vendorID=ADC_VENDOR_ID;
	        /* Copy SW vendor version info. */
	        versioninfo->sw_major_version=ADC_SW_MAJOR_VERSION_C;
	        versioninfo->sw_minor_version=ADC_SW_MINOR_VERSION_C;
	        versioninfo->sw_patch_version=ADC_SW_PATCH_VERSION_C;
 	}

}
/*..........................ISR Definitions..................................*/

#pragma CODE_SEG __NEAR_SEG NON_BANKED

//HALCOGEN-START
/*****************************************************************************
* Function Name              : Adc0_ConversionCompleteIsr
*
* Arguments                  : None
*
* Description                : This ISR passes the control to the function
*                              pointer as given in config structure
*
* Return Type                : void
*
******************************************************************************/

void Adc0_ConversionCompleteIsr(void)
{

    uint8 index;
    uint8 channel;

    /* Declaration of pointer to the Result  register set of ADC HW Unit 0*/
   	/*store converted channels for checks in other APIs */
    Adc_Converted_Channels[ADC_0_CHANNEL_RESULT_REGISTER_L] = ATD0STAT2L;

    /*clear conversion complete flag */
    ATD0STAT0_SCF  =TRUE;

    /* For one shot and  Continuous  Conversion*/
    if( (ADC_CONV_MODE_ONESHOT==Adc_UserConfig->Adc_GrpCfg[Adc_StartedGroup[ADC_0]].ADC_GROUP_CONV_MODE) || 
        (ADC_CONV_MODE_CONTINUOUS==Adc_UserConfig->Adc_GrpCfg[Adc_StartedGroup[ADC_0]].ADC_GROUP_CONV_MODE))
    {
        channel = Adc_UserConfig->Adc_GrpCfg[Adc_StartedGroup[ADC_0]].ADC_GROUP_DEFINITION.Adc_Start_Channel;
        
        /* Update the converted value for corresponding channel in 
           Adc_Converted_Value */ 
        for(index=0;
            index<Adc_UserConfig->Adc_GrpCfg[Adc_StartedGroup[ADC_0]].ADC_GROUP_DEFINITION.Adc_TotalCh;
            index++)
        {
            Adc_Converted_Value[channel]=(uint16)(*(&ATD0DR0+index));
            channel = ((channel < Adc_UserConfig->Adc_GrpCfg[Adc_StartedGroup[ADC_0]].ADC_GROUP_DEFINITION.Adc_Wrap_around))?++channel:ADC_0_BEGIN_CHANNEL_INDEX;
        }
    }

      /*Continuous Conversion*/
      /*Continuous Group Notification in each call of the Interrupt
      See Sec6.6 Sequential Diagram*/



    /*Oneshot Conversion*/
  	if(ADC_CONV_MODE_ONESHOT==Adc_UserConfig->Adc_GrpCfg\
  		[Adc_StartedGroup[ADC_0]].\
  		ADC_GROUP_CONV_MODE)
  	{
  		if(ADC_CONV_STARTED ==\
  		   Adc_Conversion_StartFlag[Adc_StartedGroup[ADC_0]])
  		{
  			Adc_Conversion_StartFlag[Adc_StartedGroup[ADC_0]] =\
  				ADC_CONV_FINISHED;

  			/*send notification if gated conversion is not available*/
  		}

  	}


        /*Gated Conversion*/


      /* Ondemand conversion*/

}//end of ADC0 ISR
//HALCOGEN-END

/****************************************************/
//HALCOGEN-END

#pragma CODE_SEG DEFAULT



#else
   #error " Mismatch in AR Patch Version of Adc.c & Adc.h "
#endif

#else
   #error " Mismatch in AR Minor Version of Adc.c & Adc.h "
#endif

#else
   #error " Mismatch in AR Major Version of Adc.c & Adc.h "
#endif

#else
   #error " Mismatch in SW Minor Version of Adc.c & Adc.h "
#endif

#else
   #error " Mismatch in SW Major Version of Adc.c & Adc.h "
#endif
/* End of version check */

/*...............................END OF Adc.C................................*/


