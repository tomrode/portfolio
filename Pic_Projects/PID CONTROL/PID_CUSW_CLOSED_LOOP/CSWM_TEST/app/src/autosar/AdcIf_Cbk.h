/* Created at 15:24 on 1-4-2009 */
/* Version of HALCoGen used : 2.9.3 */
/* Code generated for       : MC9S12XS128 */
/*****************************************************************************/
/* Copyright(c) - This Program/Software is the exclusive property of Infosys. 
The software may not be reproduced or given to third parties without prior 
consent of Infosys.

   Filename                        : AdcIf_Cbk.h
   Version                         : 2.0.0
   Microcontroller Platform        : MC9S12XS128
   Compiler                        : Codewarrior HCS12X V5.0.28

   Description                     : Header file for adc_Cbk.c.
                                     
   Requirement Specification       : AUTOSAR_SWS_ADC_Driver.pdf version 1.0.16

   Module Design                   :  AUTOSAR_DDD_ADC_SPAL2.0.doc version 2.11

   Platform Dependant[yes/no]      : no
     
   To be changed by user[yes/no]   : no
*/

/*****************************************************************************/
   
/*...................................File Includes...........................*/


#ifndef _ADCIF_CBK_H
#define _ADCIF_CBK_H

/*...............................Function Prototypes.........................*/
 


#endif /* #ifndef _Adc_CBK_H */

/*...........................End of adc_Cbk.h..............................*/
