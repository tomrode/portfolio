/* Created at 15:25 on 1-4-2009 */
/* Version of HALCoGen used : 2.9.3 */
/* Code generated for       : MC9S12XS128 */
/******************************************************************************/
/* Copyright(c) - This Program/Software is the exclusive property of Infosys. 
The software may not be reproduced or given to third parties without prior 
consent of Infosys.

     Filename                      : GptIf_Cbk.c
     Version                       : 3.2.0
     Microcontroller Platform      : MC9S12XS128
     Compiler                      : Codewarrior HCS12X V4.5

     Description                   : This file contains all function 
                                     definitions of all functions of GPT 
                                     Driver module. 
                                     
     Requirement Specification     : AUTOSAR_SWS_GPTDriver.doc 1.1.6
                
     Module Design                 : AUTOTMAL_DDD_GPT.doc version 2.00c
     
     Platform Dependant[yes/no]    : yes
     
     To be changed by user[yes/no] : yes
*/
/*****************************************************************************/
   
/* Revision History

Class of Change
-------------------------------------------------------------------------------
N - New Module.
F - Functional development.
E - Error Correction.
O - Optimization without functional change.
-------------------------------------------------------------------------------
 00    02-Feb-2006     Sowmya, Infosys
-------------------------------------------------------------------------------
Class: N
Cause: New Module
Detail:This file contains the callback function definitions.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 01    22-Mar-2006     Sowmya, Infosys
-------------------------------------------------------------------------------
Class: F
Cause: Functional development
Detail:Upgrade to suit to 1.1.6 GPT specification.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 02    08-Dec-2006      Ramprasad P B, Infosys
-------------------------------------------------------------------------------
Class: F
Cause: New Module
Detail:This file contains the callback function definitions for S12XEP100.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 03    24-Jul-2007      Khanindra, Infosys
-------------------------------------------------------------------------------
Class: F
Cause: Updated for the derivatives MC9S12XEG384 and MC9S12XEG256 
Detail:Updated for the derivatives MC9S12XEG384 and MC9S12XEG256 
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 04    01-Oct-2007      Khanindra, Infosys
-------------------------------------------------------------------------------
Class: F
Cause: Updated for the derivatives MC9S12XS128
Detail:Updated for the derivatives MC9S12XS128. Preprocessor switch 
       HAL_MC9S12XS128 is used to differentiate from other derivatives.As 
       Modulus Down Counter is not available in MC9S12XS128,so Modulus Down 
       Counter related codes are removed using Preprocessor switch.
-------------------------------------------------------------------------------
******************************************************************************/

/*...................................File Includes...........................*/
#include "GptIf_Cbk.h"

/* External Functions */
extern @near void mainF_TimeSliceGen(void);  

void Gpt_TmSlice_Isr(void)
{
	mainF_TimeSliceGen();	// Call the application time slice generator  
}


/*.........................End of GptIf_Cbk.c..................................*/
