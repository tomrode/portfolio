/* Created at 15:24 on 1-4-2009 */
/* Version of HALCoGen used : 2.9.3 */
/* Code generated for       : MC9S12XS128 */
/*****************************************************************************/
/* Copyright(c) - This Program/Software is the exclusive property of Infosys. 
The software may not be reproduced or given to third parties without prior 
consent of Infosys.

     Filename                      : RamTst.h
     Version                       : 1.0.3
     Microcontroller Platform      : MC9S12XS128
     Compiler                      : Compiler for Freescale HC12 V5.0.30
     
     Description                   : Header file for RAM Test.
                                     
     Requirement Specification     : AUTOSAR_SWS_RAMTest.doc 0.12
     
     Module Design                 : AUTOTMAL_DDD_RAMTest_SPAL2.0.doc 
                                     version 2.00
     
     Platform Dependant[yes/no]    : yes
     
     To be changed by user[yes/no] : no
*/
/*****************************************************************************/
   
/* Revision History

Class of Change
-------------------------------------------------------------------------------
N - New Module.
F - Functional development.
E - Error Correction.
O - Optimization without functional change.
-------------------------------------------------------------------------------
 00      09-May-2006     Khanindra Jyoti Deka, Infosys
-------------------------------------------------------------------------------
Class: N
Cause: New Module
Detail:This is header file for the RAM Test.
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
 01        24-Aug-2006     Khanindra Jyoti Deka, Infosys
-------------------------------------------------------------------------------
Class: O
Cause: Change has done for RTRT anomaly report
Detail:DataSave_Result is initialized by NO_CONTENTF
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
 02        27-Feb-2008     Khanindra Jyoti Deka, Infosys
-------------------------------------------------------------------------------
Class: O
Cause: Version Updated
Detail:File version is updated in sync with RamTst.c
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
 03        03-Apr-2008     Khanindra Jyoti Deka, Infosys
-------------------------------------------------------------------------------
Class: O
Cause: Version Updated
Detail:File version is updated in sync with RamTst.c
-------------------------------------------------------------------------------

******************************************************************************/

/* to avoid Multiple inclusion */
#ifndef _RAMTST_H
#define _RAMTST_H


/*...................................File Includes...........................*/

/* Stdtypes header file */
#include "Std_Types.h"
#include "RamTst_Types.h"

/* RAM Test Configuration header file */
#include "RamTst_Cfg.h" 

/* Callback header file */
#include "RamTstIf_Cbk.h"

/*...................................Macros..................................*/
/******************************************************************************
* Declaring the required macros to perform the version check                 
* Please Note: These macros should be typecasted with (uint8) as per SRS      
* General.This has not been done as per specification as uint8 is a typedef  
* (runtime) and these macros are used at preprocessing time.                
******************************************************************************/

/* SW ->Software vendor version info */
#define RAMTST_SW_MAJOR_VERSION                     (1)
#define RAMTST_SW_MINOR_VERSION                     (0)
#define RAMTST_SW_PATCH_VERSION                     (3)
/* AR ->AUTOSAR version info */
#define RAMTST_AR_MAJOR_VERSION                     (0)
#define RAMTST_AR_MINOR_VERSION                     (12)
#define RAMTST_AR_PATCH_VERSION                     (0)

/* Module ID */     
#define RAMTST_MODULE_ID                            ((uint8)93)

/* Vendor ID */
#define RAMTST_VENDOR_ID                            ((uint16)40)

/* Declaring the service ID macros of the RAM Test service APIs */
#define RAMTST_INIT_ID                              ((uint8)0x00)
#define RAMTST_MAIN_FUNCTION_ID                     ((uint8)0x01)
#define RAMTST_STOP_ID                              ((uint8)0x02)
#define RAMTST_CONTINUE_ID                          ((uint8)0x03)
#define RAMTST_GET_EXECUTION_STATUS_ID              ((uint8)0x04)
#define RAMTST_GET_TEST_RESULT_ID                   ((uint8)0x05)
#define RAMTST_GET_TEST_RESULT_PER_BLOCK_ID         ((uint8)0x06)
#define RAMTST_GET_TEST_ALGORITHM_ID                ((uint8)0x07)
#define RAMTST_CHANGE_NUMBER_OF_TESTED_CELLS_ID     ((uint8)0x08)
#define RAMTST_GET_NUMBER_OF_TESTED_CELLS_ID        ((uint8)0x09)
#define RAMTST_GET_VERSION_INFO_ID                  ((uint8)0x0A)
#define RAMTST_CHANGE_TEST_ALGORITHM_ID             ((uint8)0x0B)

/* Declaring the error ID macros of the RAM Test error messages */
#define RAMTST_E_STATUS_FAILURE                     ((uint8)0x01)
#define RAMTST_E_OUT_OF_RANGE                       ((uint8)0x02)
#define RAMTST_E_UNINIT                             ((uint8)0x03)

/* Maximum number of tested cells */
#define RAMTST_MAX_NUMBER_OF_TESTED_CELLS           ((uint16)0x1FFF)
#define PUT_NEW_BLOCK                               ((uint8)0)

/* RAM Test end blok ID */
#define RAMTST_END_BLOCK_ID                  RAMTST_TEST_TOTAL_NUMBER_OF_BLOCKS
/* RAM Test end Algorithm ID */
#define RAMTST_END_ALGORITHM_ID                RamTst_Config.RamTst_AlgParams\
                                               [RAMTST_NUMBER_OF_ALGORITHMS-1]\
                                               .RamTst_AlgorithmID
/* RAM Test present number of tested cells */
#define RAMTST_PRESENT_NUMBER_OF_TESTED_CELLS  RamTst_Config.RamTst_AlgParams\
               [(RamTst_DataType)RamTst_PresentAlgorithm-(RamTst_DataType)1].\
               RamTst_NumberOfTestedCells
/* RAM Test default algorithm */
#define RAMTST_PRESENT_DEFAULT_TEST_ALGORITHM  RamTst_PresentAlgorithm 
/* RAM Test present block starting address */
#define PRESENT_BLOCK_STARTING_ADDRESS         (RamTst_Config.RamTst_AlgParams\
                                    [RAMTST_PRESENT_DEFAULT_TEST_ALGORITHM-1].\
                                    RamTst_BlockParams+RamTst_PresentBlock)->\
                                    RamTst_StartAddress
/* End address of the present block.*/
#define END_ADDRESS_OF_THE_PRESENT_BLOCK       RAMTST_BLOCK_END_ADDRESS
/* Number of block for present algorithm */
#define NUMBER_OF_BLOCKS_FOR_PRESENT_ALGO      RamTst_Config.RamTst_AlgParams\
                                               [RamTst_PresentAlgorithm-1].\
                                               RamTst_NumberOfBlocks

/* RamTst014: The execution status is changed to RAMTST_EXECUTION_STOPPED*/
#define RamTst_Stop() (RamTst_ExecutionStatus = RAMTST_EXECUTION_STOPPED)

/* RamTst018: The execution status is changed to RAMTST_EXECUTION_CONTINUE */
#define RamTst_Continue() (RamTst_ExecutionStatus = RAMTST_EXECUTION_CONTINUE)

/* RamTst019: Return the current RAM Test execution status */
#define RamTst_GetExecutionStatus() (RamTst_ExecutionStatus)

/* RamTst039: Return the current RAM Test result for the specified block */
#define RamTst_GetTestResultPerBlock(RamTst_Data)\
    (RamTst_TestResult[RamTst_Data-1])

/* RamTst085: Re-initialize all RAM Test relevant global variables */
/* RamTst083: Change the used TestAlgorithm */
#define RamTst_ChangeTestAlgorithm(RamTst_Data) ((RamTst_NewAlgorithm =\
    RamTst_Data),(RamTst_PresentAlgorithm = RAMTST_ALGORITHM_UNDEFINED),\
    RamTst_PresentAddress = (RamTst_BlockAddressType)1,\
    RamTst_PresentBlock = PUT_NEW_BLOCK,\
    RamTst_NumberOfNewTestedCells = RamTst_Config.RamTst_AlgParams\
  [(RamTst_DataType)RamTst_NewAlgorithm-(RamTst_DataType)1].\
  RamTst_NumberOfTestedCells)

/* RamTst021: Return the current RAM Test algorithm ID */
#define RamTst_GetTestAlgorithm() (RamTst_PresentAlgorithm)

/* RamTst036: Change the current number of tested cells */
#define RamTst_ChangeNumberOfTestedCells(RamTst_Data) \
    (RamTst_NumberOfNewTestedCells = RamTst_Data)

/* RamTst034: Return the current number of tested cells per cycle */
#define RamTst_GetNumberOfTestedCells() (RamTst_NumberOfNewTestedCells)

/*.........................Type Definitions..................................*/
/* This a function pointer array to point to all the algorithms */
typedef void (*RamTst_AlgorithmFunctionPtrType)(void);

/* Data types of the tested RAM cells */
typedef uint16 RamTst_DataType;

/* RAM Test Execution Status Type */
typedef enum
{
    /* The RAM Test is not initialized or not usable.*/
    /* This shall be the default value after reset. */
    /* This status shall have the value 0.*/
    RAMTST_EXECUTION_UNINIT, 

    /* The RAM Test is initialized and ready to be started.*/    
    RAMTST_EXECUTION_INIT,

    /* The RAM Test is currently running.*/  
    RAMTST_EXECUTION_RUNNING,

    /* The RAM Test is stopped.*/        
    RAMTST_EXECUTION_STOPPED,

    /* The RAM Test is ready for continue.*/     
    RAMTST_EXECUTION_CONTINUE, 

    /* State used for white box testing.*/   
    RAMTST_EXECUTION_STATE_UNDEFINED
    
} RamTst_ExecutionStatusType;

/* RAM Test Algorithm Type */
typedef enum
{
    /* Undefined algorithm (uninitialized value) */
    /* This shall be the default value after reset*/ 
    /* It shall have the value 0.*/
    RAMTST_ALGORITHM_UNDEFINED,

    /* Checkerboard test algorithm */
    RAMTST_CHECKERBOARD_TEST

} RamTst_AlgorithmType;

/* Data type of number of blocks*/
typedef uint16 RamTst_NumberOfBlocksType;

/* Data type of number of configured container for algorithms */
typedef uint16 RamTst_NumberOfAlgorithmsType;  

/* RAM Test Notification callback type */
typedef void(*RamTst_NotificationClbkType)(void);   

/* Configuration type for block */
typedef struct
{
    /* Id of the RAM block */
    RamTst_BlockIDType RamTst_BlockID;

    /* Start Address of the RAM block */
    RamTst_BlockAddressType RamTst_StartAddress;

    /* End Address of the RAM block */
    RamTst_BlockAddressType RamTst_EndAddress;
}RamTst_BlockParamsType;            

/* Configuration type for algorithm */         
typedef struct
{
    /* Algorithm ID */
    RamTst_AlgorithmType RamTst_AlgorithmID;

    /* Initial value for a RAM variable which could be changed by the function 
    "RamTst_ChangeNumberOfTestedCells" */
    RamTst_NumberOfTestedCellsType RamTst_NumberOfTestedCells;

    /* Number of RAM blocks */
    RamTst_NumberOfBlocksType RamTst_NumberOfBlocks;

    /* Container for block parameters.*/
    RamTst_BlockParamsType *RamTst_BlockParams;

}RamTst_AlgParamsType;          


/* Configuration type for RAM Test module */            
typedef struct
{
    /*  Number of configured algorithms */
    RamTst_NumberOfAlgorithmsType RamTst_NumberOfAlgorithms;

    /* This is the default algorithm after the "RamTst_Init(..)" function.
    This is the initial value for a RAM variable which could be changed by the 
    function "RamTst_ChangeTestAlgorithm" */
    RamTst_AlgorithmType RamTst_DefaultTestAlgorithm;

    /* This function will be called after finishing the ram test without an 
    error. */
    RamTst_NotificationClbkType RamTst_CompleteNotification_Ptr;

    /* This function will be called if an error occurs during the RAM test. */
    RamTst_NotificationClbkType RamTst_ErrorNotification_Ptr;

    /* This is the parameter container for an algorithm. */
    RamTst_AlgParamsType RamTst_AlgParams[RAMTST_NUMBER_OF_ALGORITHMS];  

}RamTst_ConfigParamsType;   /* Data types of the tested RAM cells */

/*.........................Global Variable declarations......................*/
/* RAM Test config structure */
extern const RamTst_ConfigParamsType  RamTst_Config;

#pragma section [safe_ram]
//#pragma DATA_SEG DATASAVE_RAM 
extern RamTst_ExecutionStatusType     RamTst_ExecutionStatus;
extern RamTst_DataType                RamTst_Data;
extern RamTst_TestResultType          RamTst_TestResult\
                                      [RAMTST_TEST_TOTAL_NUMBER_OF_BLOCKS];
extern RamTst_BlockIDType             RamTst_BlockIDForAlgo;
extern RamTst_AlgorithmType           RamTst_NewAlgorithm;
extern RamTst_AlgorithmType           RamTst_PresentAlgorithm;
extern RamTst_NumberOfTestedCellsType RamTst_NumberOfNewTestedCells;
extern RamTst_BlockIDType             RamTst_PresentBlock;
//#pragma DATA_SEG DEFAULT 
/*.........................Global Function Prototypes........................*/
/* Exported functions prototypes */
/* ------------------------------*/
extern void RamTst_Init ( void );

extern RamTst_TestResultType RamTst_GetTestResult ( void );

extern void RamTst_GetVersionInfo ( Std_VersionInfoType *versioninfo );
extern void RamTst_MainFunction ( void );
#pragma section []

#endif  /*_RAMTST_H*/

/*.............................END OF RamTst.h...............................*/

