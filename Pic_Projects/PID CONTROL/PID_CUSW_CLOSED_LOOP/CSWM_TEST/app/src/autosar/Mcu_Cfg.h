/* Created at 15:25 on 1-4-2009 */
/* Version of HALCoGen used : 2.9.3 */
/* Code generated for       : MC9S12XS128 */
/*****************************************************************************/
/* Copyright(c) - This Program/Software is the exclusive property of Infosys.
The software may not be reproduced or given to third parties without prior
consent of Infosys.

Filename                        : Mcu_Cfg.h
Version                         : 3.2.4
Microcontroller Platform        : MC9S12XS128
Compiler                        : Codewarrior HCS12X V5.7.0

Description                     : This file contains the static configuration
                                  for MCU driver module.

Requirement Specification       : AUTOSAR_SWS_McuDriver.pdf version 1.2.0

Module Design                   : AUTOSAR_DDD_MCU_SPAL2.0.doc version 2.10a

Platform Dependant[yes/no]      : yes 

To be changed by user[yes/no]   : no 

*/

/*****************************************************************************/

/* Revision History

Class of Change
-------------------------------------------------------------------------------
N - New Module.
F - Functional development.
E - Error Correction.
O - Optimization without functional change.
-------------------------------------------------------------------------------
 00     02-Feb-2006     Kapil Kumar, Infosys
-------------------------------------------------------------------------------
Class: N
Cause: New Module
Detail:This file implements the MCU driver static configuration.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 01     30-Mar-2006     Kapil Kumar, Infosys
-------------------------------------------------------------------------------
Class: F
Cause: Changes for SPAL2.0
Detail:Changes done as per AUTOSAR_SWS_McuDriver.pdf ver 1.2.0
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 02     24-Jul-2006     Harini KE, Infosys
-------------------------------------------------------------------------------
Class: F
Cause: Changes for SPAL2.0
Detail:Changes done for including Macro enabling of certain APIs and Enabling 
of RTI interrupt based on the user selection.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 03     11-Dec-2006     Anwar Husen, Infosys
-------------------------------------------------------------------------------
Class: N
Cause: New Module
Detail:This file is updated for MC9S12XEP100 derivative for MCU driver.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 04     11-Jan-2007     Anwar Husen, Infosys
-------------------------------------------------------------------------------
Class: F
Cause: FM0,FM1 bit setting
Detail:For each Clock setting there will be a separate Frequency modulation ratio
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 05     30-Jan-2007     Anwar Husen, Infosys
-------------------------------------------------------------------------------
Class: F
Cause: Change request #609 in PRL
Detail:Pre-processor checks are added to implement the Change request #609.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 06     26-Mar-2007   Rose Paul , Infosys
-------------------------------------------------------------------------------
Class : E
Cause : DISABLE was defined as 0, but it is also used by the preprocessor
        (e.g. #pragma MESSAGE DISABLE ...).The preprocessor directive
        does not work after the inclusion of MCAL_types.h.
Detail: Macros in Mcal_Types prefixed by "MCAL_".
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 07     29-Mar-2007   Khanindra , Infosys
-------------------------------------------------------------------------------
Class : F
Cause : For Implementation of Freescale security recommendation
Detail: For Implementation of Freescale security recommendation
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 08     24-Aug-2007   Khanindra , Infosys
-------------------------------------------------------------------------------
Class : F
Cause : In accordance with the issue #746
Detail: Macro MCU_SYSTEM_FAILURE_ANALYSIS_MCU_SCR is added
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 09     18-Oct-2007   Prashant Kulkarni , Infosys
-------------------------------------------------------------------------------
Class : F
Cause : To support XS128 microcontroller
Detail: preprocessor added for MCU_ECT_DELAY_COUNTER as it is not applicable
			for XS128
-------------------------------------------------------------------------------
******************************************************************************/


#ifndef _MCU_CFG_H
#define _MCU_CFG_H

/*****************************************************************************
**                      Global Symbols                                      **
*****************************************************************************/



#define HAL_MC9S12XEP100				OFF
#define HAL_MC9S12XEG384				OFF
#define HAL_MC9S12XEG256				OFF
#define HAL_MC9S12XS128					ON
/* Define if development error has to be reported */
#define MCU_DEV_ERROR_DETECT      OFF

/*Define if DEM error has to be reported */
#define MCU_DEM_ERROR_DETECT      OFF

/* Define if Wake-up reason notification has to be done */
#define MCU_REPORT_WAKE_UP_REASON      ON

#define MCU_TIMER_INIT                 ON

#define MCU_RAMSECTION_INIT            ON

#define MCU_SETMODE                    ON


/* Wakup Event Reason */
#define MCU_WKSOURCE_POWER            0
#define MCU_WKSOURCE_TIMER_EXPIRED    1
#define MCU_WKSOURCE_RESET            0
#define MCU_WKSOURCE_INTERNAL_RESET   0
#define MCU_WKSOURCE_INTERNAL_WDG     0
#define MCU_WKSOURCE_EXTERNAL_WDG     0
#define MCU_WKSOURCE_ICU              0
#define MCU_WKSOURCE_GPT              0
#define MCU_WKSOURCE_CANIF            0
#define MCU_WKSOURCE_UNUSED           0

/*...................Configuration Description ..........................*/
#define MCU_CONFIGURED_RAM_SECTORS        1
#define MCU_CONFIGURED_MCU_MODES          1
#define MCU_CONFIGURED_CLOCK_SETTINGS     1

#define CLK_SETTING_0		0


/* Ram sector configuration for 1 RAM sectors ..........................*/

    /* MCU_RAM_SECTOR_SETTING 0 */
    #define MCU_RAM_BASE_ADDRESS_0      0xFE000
    #define MCU_RAM_SIZE_0              0x2000
    #define MCU_RAM_PRESET_0            0x0



/* MCU mode configuration for 1 Power modes ..........................*/

    /* MCU power mode 0 settings */
    #define MCU_POWER_MODE_0            MCU_POWER_MODE_WAIT 
    #define MCU_RTI_WAITMODE_0          MCAL_DISABLE 
    #define MCU_PLL_WAITMODE_0          MCAL_DISABLE 
    #define MCU_COP_PSEUDOSTOPMODE_0    MCAL_DISABLE 
    #define MCU_RTI_PSEUDOSTOPMODE_0    MCAL_DISABLE 



/*Clock configuration for 1 settings */

    /*MCU_CLOCK_SETTING_VALUE 0 */
    #define MCU_SYNR_VALUE_0            0x1
    #define MCU_REFDIV_VALUE_0          0x80
    #define MCU_POSTDIV_VALUE_0          0x0



    /* Reset type configuration */
    #define MCU_RESET_TYPE_0                MCU_WATCHDOG_RESET

    /*CRGINT register value */
    #define MCU_CRG_INT                 0x12
    /*PLL frequency modulation bits value*/
    #define MCU_PLLCTL_FM_0 0x00

    /* ECT HW setting(s) */

    #define MCU_ECT_HW_SETTINGS_0       0x80
    #define MCU_ECT_PRESCALER_0         0x5
/*............................System Failure Analysis........................*/

/* Defines for ISRs and Failure Analysis */
#define MCU_SYSTEM_FAILURE                     OFF
#define MCU_SYSTEM_FAILURE_ANALYSIS_MCU_SCR   OFF
#define MCU_SYSFAILURE_NOTIF                    OFF


#endif  /* end of _MCU_CFG_H */ 

/*.............................END OF Mcu_Cfg.H...............................*/