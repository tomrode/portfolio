/* Created at 15:25 on 1-4-2009 */
/* Version of HALCoGen used : 2.9.3 */
/* Code generated for       : MC9S12XS128 */
/******************************************************************************/
/* Copyright(c) - This Program/Software is the exclusive property of Infosys. 
   The software may not be reproduced or given to third parties without prior 
   consent of Infosys.

   Filename                      : Port.h
   Version                       : 2.0.8
   Microcontroller Platform      : MC9S12XS128
   Compiler                      : Codewarrior HCS12X V5.0.28

   Description                   : Header file for PORT

   Requirement Specification     : AUTOSAR_SWS_Port_Driver_working.doc ver1.1.3

   Module Design                 : AUTOTMAL_DDD_PORT_SPAL2.0.doc version 1.0

   Platform Dependant[yes/no]    : yes   

   To be changed by user[yes/no] : no  
*/
/******************************************************************************/
/* Revision History

Class of Change
-------------------------------------------------------------------------------
N - New Module.
F - Functional development.
E - Error Correction.
O - Optimization without functional change.
-------------------------------------------------------------------------------
 00        04-Jan-2006     Muthuvelavan Natarajan, Infosys
-------------------------------------------------------------------------------
Class : N
Cause : New Module
Detail: This is header file for the PORT driver.
-------------------------------------------------------------------------------
 01        03-Mar-2006     Muthuvelavan Natarajan, Infosys
-------------------------------------------------------------------------------
Class : F
Cause : Functional development
Detail: Update for SPAL2.0.
-------------------------------------------------------------------------------
 02        09-Jun-2006     Vijayakumar K, Infosys
-------------------------------------------------------------------------------
Class : O
Cause : Support multiple configuration structures
Detail: Symbolic pin name & pin functionality are moved out of config structure
-------------------------------------------------------------------------------
 03        27-Jun-2006     Vijayakumar K, Infosys
-------------------------------------------------------------------------------
Class : O
Cause : Macros/Inline for speed optimization
Detail: Port_SetPinDirection is configurable as Macro
-------------------------------------------------------------------------------
 04        09-Jan-2007     Kasinath, Infosys
-------------------------------------------------------------------------------
Class : F
Cause : Version updated in line with .c file updation
Detail: Version updated in line with .c file updation
-------------------------------------------------------------------------------
 05        31-Jan-2007     Kasinath, Infosys
-------------------------------------------------------------------------------
Class : F
Cause : A2D0 & A2D1 Channel Changes
Detail: Port A2D0 modified to handle 16 channels and port A2D1 8 channels.
-------------------------------------------------------------------------------
 06        26-Mar-2007     Rose Paul , Infosys
-------------------------------------------------------------------------------
Class : E
Cause : PRL 689- Lint warning 502
Detail: Suffixed hardcoded values,in expressions with unary operator,with 'u'
-------------------------------------------------------------------------------
 07        11-Apr-2007     Kasinath, Infosys
-------------------------------------------------------------------------------
Class : E
Cause : Fix for issue 716
Detail:  Pins not used in lower pin packages also initialized.
-------------------------------------------------------------------------------
 08        26-Jun-2007     Sandeep, Infosys
-------------------------------------------------------------------------------
Class : E
Cause : Fix for issue 759, Lint error.
Detail: TRUE replaced by PORT_PIN_OUT in Port_SetPinDirection Macro.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 09        25-Sep-2007     Khanindra, Infosys
-------------------------------------------------------------------------------
Class : E
Cause : Fix for issue 816
Detail: Global variable Port_PTT_Level is externed.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 10        01-Oct-2007     Ajaykumar, Infosys
-------------------------------------------------------------------------------
Class : F
Cause : MC9S12XS128 controller support
Detail: MC9S12XS128 related pre-processor switches added.
-------------------------------------------------------------------------------
 11        16-Nov-2007     Ajaykumar, Infosys
-------------------------------------------------------------------------------
Class : E
Cause : Bugfix for #833
Detail: Preprocessor switches added for Pin packages.
-------------------------------------------------------------------------------
 12        18-Jan-2008     Ajaykumar, Infosys
-------------------------------------------------------------------------------
Class : E
Cause : Source file updated
Detail: Updated in sync with Port.c
------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 13        31-Mar-2008     Ajaykumar,  Infosys
-------------------------------------------------------------------------------
Class:  E
Cause:  PRL issue 978
Detail: Updated in sync with Port.c
-------------------------------------------------------------------------------
******************************************************************************/

/* to avoid Multiple inclusion */
#ifndef _PORT_H
#define _PORT_H

/*...................................File Includes............................*/

/* Standard types header file */
#include "Std_Types.h"
/* Infosys defines header file */
#include "MCAL_Types.h"
/* CFG header file */
#include "Port_Cfg.h"
/* OS header file */
#include "os.h"

/*...................................Macros...................................*/

/* Declaring the required macros to perform the version check */

#define PORT_SW_MAJOR_VERSION         (2)
#define PORT_SW_MINOR_VERSION         (0)
#define PORT_SW_PATCH_VERSION         (8)
#define PORT_AR_MAJOR_VERSION         (1)
#define PORT_AR_MINOR_VERSION         (1)
#define PORT_AR_PATCH_VERSION         (3)


/* Module ID */

#define PORT_MODULE_ID                ((uint8)124)

/* Vendor ID */
/* Infosys' Vendor ID */
#define PORT_VENDOR_ID                ((uint16)40)

/* Service IDs */  

#define PORT_INIT_ID                  ((uint8)0)
#define PORT_SETPINDIRECTION_ID       ((uint8)1)
#define PORT_REFRESHPORTDIRECTION_ID  ((uint8)2)
#define PORT_GETVERSIONINFO_ID        ((uint8)3)

/* Error IDs */

#define PORT_E_PARAM_PIN              ((uint8)10)
#define PORT_E_DIRECTION_UNCHANGEABLE ((uint8)11)
#define PORT_E_PARAM_CONFIG           ((uint8)12)

/* Port Pin IDs */
enum {
    PORT_A_PIN_0 = 0,
    PORT_A_PIN_1,
    PORT_A_PIN_2,
    PORT_A_PIN_3,
    PORT_A_PIN_4,
    PORT_A_PIN_5,
    PORT_A_PIN_6,
    PORT_A_PIN_7,
    PORT_B_PIN_0,
    PORT_B_PIN_1,
    PORT_B_PIN_2,
    PORT_B_PIN_3,
    PORT_B_PIN_4,
    PORT_B_PIN_5,
    PORT_B_PIN_6,
    PORT_B_PIN_7,
    PORT_E_PIN_0,
    PORT_E_PIN_1,
    PORT_E_PIN_2,
    PORT_E_PIN_3,
    PORT_E_PIN_4,
    PORT_E_PIN_5,
    PORT_E_PIN_6,
    PORT_E_PIN_7,
    PORT_T_PIN_0,
    PORT_T_PIN_1,
    PORT_T_PIN_2,
    PORT_T_PIN_3,
    PORT_T_PIN_4,
    PORT_T_PIN_5,
    PORT_T_PIN_6,
    PORT_T_PIN_7,
    PORT_S_PIN_0,
    PORT_S_PIN_1,
    PORT_S_PIN_2,
    PORT_S_PIN_3,
    PORT_S_PIN_4,
    PORT_S_PIN_5,
    PORT_S_PIN_6,
    PORT_S_PIN_7,
    PORT_M_PIN_0,
    PORT_M_PIN_1,
    PORT_M_PIN_2,
    PORT_M_PIN_3,
    PORT_M_PIN_4,
    PORT_M_PIN_5,
    PORT_M_PIN_6,
    PORT_M_PIN_7,
    PORT_P_PIN_0,
    PORT_P_PIN_1,
    PORT_P_PIN_2,
    PORT_P_PIN_3,
    PORT_P_PIN_4,
    PORT_P_PIN_5,
    PORT_P_PIN_6,
    PORT_P_PIN_7,
    PORT_J_PIN_0,
    PORT_J_PIN_1,
    PORT_J_PIN_2,
    PORT_J_PIN_3,
    PORT_J_PIN_4,
    PORT_J_PIN_5,
    PORT_J_PIN_6,
    PORT_J_PIN_7,
    PORT_A2D0L_PIN_0,
    PORT_A2D0L_PIN_1,
    PORT_A2D0L_PIN_2,
    PORT_A2D0L_PIN_3,
    PORT_A2D0L_PIN_4,
    PORT_A2D0L_PIN_5,
    PORT_A2D0L_PIN_6,
    PORT_A2D0L_PIN_7,
    PORT_MAX_PINS
};

/* To define PORT index */
enum {
    PORT_A = 0,
    PORT_B,
    PORT_E,
    PORT_T,
    PORT_S,
    PORT_M,
    PORT_P,
    PORT_J,
    PORT_A2D0L,
    PORT_MAX_INDEX
};

/*.........................Type Definitions...................................*/

/* Symbolic Pin Name */
typedef uint8 Port_PinType;

/* This is used to control the availability of the free-running clocks and the 
   free-running clock divider on ECLK, ECLKX2 pins  */
typedef uint8 Port_EClkType;

/* This supports the re-routing of the CAN0, CAN4, SPI0, SPI1, and SPI2 pins to 
   alternative ports. This allows a software re-configuration of the pinouts of 
   the different package options with respect to above peripherals. */
typedef uint8 Port_ModRType;

typedef enum{
    PORT_PIN_IN, /* Direction = INPUT */
    PORT_PIN_OUT /* Direction = OUTPUT */
}Port_PinDirectionType;

/* Possible modes in each Port pin */
typedef enum{
    UNUSED, /* Pin not used */
    NOT_AVAILABLE, /* Pin not available */
    DIO, /* Pin used as DIO */
    SPI, /* Pin used as normal SPI I/O */
    SPI_CS,/* Pin reserved to be used as CS for SPI */  
    ICU,/* Pin reserved for ICU driver */
    PWM,/* Pin reserved for PWM driver */
    GPT,/* Pin reserved for GPT driver */
    ADC,/* Pin reserved for ADC driver */
    CAN,/* Pin reserved for CAN driver */
    KWP,KWH,KWJ,/* Pin used for KWP,KWH,KWJ */
    CAN_0,CAN_1,CAN_2,CAN_3,CAN_4,
    PWM_TIM,PWM_ECT,PWM_16bit
}Port_ModeType;

/* Type definition for Symbolic Pin Name */
typedef struct{                            
    Port_PinType    Pin0;    
    Port_PinType    Pin1;    
    Port_PinType    Pin2;    
    Port_PinType    Pin3;    
    Port_PinType    Pin4;    
    Port_PinType    Pin5;    
    Port_PinType    Pin6;    
    Port_PinType    Pin7;    
}Port_NameType;

/* Type definition for Pin Functionality */
typedef struct{
    Port_ModeType   Pin0;
    Port_ModeType   Pin1;
    Port_ModeType   Pin2;
    Port_ModeType   Pin3;
    Port_ModeType   Pin4;
    Port_ModeType   Pin5;
    Port_ModeType   Pin6;
    Port_ModeType   Pin7;
}Port_FuncType;

/* Type definition for Pin Level (Data) */
/* This holds the value driven out to the pin if it is used as a GPIO */
typedef union{
    uint8    PortLevel;
    struct{
        uint8    Pin0_Level    :1;    
        uint8    Pin1_Level    :1;    
        uint8    Pin2_Level    :1;    
        uint8    Pin3_Level    :1;    
        uint8    Pin4_Level    :1;    
        uint8    Pin5_Level    :1;    
        uint8    Pin6_Level    :1;    
        uint8    Pin7_Level    :1;    
    }Pins;
}Port_LevelType;

/* Type definition for Pin Direction (Data) */
/* This defines whether the pin is used as an input or an output.
   If a peripheral module controls the pin the contents of this is ignored */
typedef union{   
    uint8    PortDirection;
    struct{    
        uint8    Pin0_Direction    :1;    
        uint8    Pin1_Direction    :1;    
        uint8    Pin2_Direction    :1;    
        uint8    Pin3_Direction    :1;    
        uint8    Pin4_Direction    :1;    
        uint8    Pin5_Direction    :1;    
        uint8    Pin6_Direction    :1;    
        uint8    Pin7_Direction    :1;    
    }Pins;
}Port_DirectionType;

/* Type definition for Pin Drive Strength */
/* If the pin is used as an output this allows the configuration of the 
   drive strength.*/
typedef union{   
    uint8    PortDrive;
    struct{            
        uint8    Pin0_Drive    :1;    
        uint8    Pin1_Drive    :1;    
        uint8    Pin2_Drive    :1;    
        uint8    Pin3_Drive    :1;    
        uint8    Pin4_Drive    :1;    
        uint8    Pin5_Drive    :1;    
        uint8    Pin6_Drive    :1;    
        uint8    Pin7_Drive    :1;    
    }Pins;  
}Port_DriveType;

/* Type definition for Pull Enable */
/* This is used to turn on a pull-up or pull-down device. 
   It becomes active only if the pin is used as input or as wired-OR output. */
typedef union{   
    uint8    PortPullEnable;
    struct{                          
        uint8    Pin0_PullEnable    :1;    
        uint8    Pin1_PullEnable    :1;    
        uint8    Pin2_PullEnable    :1;    
        uint8    Pin3_PullEnable    :1;    
        uint8    Pin4_PullEnable    :1;    
        uint8    Pin5_PullEnable    :1;    
        uint8    Pin6_PullEnable    :1;    
        uint8    Pin7_PullEnable    :1;    
    }Pins;    
}Port_PullEnableType;

/* Type definition for Polarity Selection */
/* This selects either a pull-up or pull-down device if enabled. 
   It becomes active only if the pin is used as an input. 
   A pull-up device can be activated if the pin is used as a wired-OR output. 
   If the pin is used as an interrupt input this is used to select the active 
   interrupt edge. */
typedef union{   
    uint8    PortPolarity;
    struct{                           
        uint8    Pin0_Polarity    :1; 
        uint8    Pin1_Polarity    :1; 
        uint8    Pin2_Polarity    :1; 
        uint8    Pin3_Polarity    :1; 
        uint8    Pin4_Polarity    :1; 
        uint8    Pin5_Polarity    :1; 
        uint8    Pin6_Polarity    :1; 
        uint8    Pin7_Polarity    :1; 
    }Pins;           
}Port_PolarityType;

/* Type definition for Wired OR Configuration */
/* If the pin is used as output this is used to turn off the active high drive. 
   This allows wired-OR type connections of outputs. */
typedef union{   
    uint8    PortWiredOr;
    struct{                         
        uint8    Pin0_WiredOr    :1;    
        uint8    Pin1_WiredOr    :1;    
        uint8    Pin2_WiredOr    :1;    
        uint8    Pin3_WiredOr    :1;    
        uint8    Pin4_WiredOr    :1;    
        uint8    Pin5_WiredOr    :1;    
        uint8    Pin6_WiredOr    :1;    
        uint8    Pin7_WiredOr    :1;    
    }Pins;   
}Port_WiredOrType;

/* Type definition for Digital Input Control */
/* This is used to control the digital input buffer from the analog input
   pin (ANx) to PTADx data register. */
typedef union{   
    uint8    PortDIE;
    struct{                         
        uint8    Pin0_DIE    :1;
        uint8    Pin1_DIE    :1;
        uint8    Pin2_DIE    :1;
        uint8    Pin3_DIE    :1;
        uint8    Pin4_DIE    :1;
        uint8    Pin5_DIE    :1;
        uint8    Pin6_DIE    :1;
        uint8    Pin7_DIE    :1;
    }Pins;
}Port_DIEType;

/* Type definition for dynamic direction changeability */
typedef union{   
    uint8    PortDirConfig;
    struct{          
        uint8    Pin0_DirConfig    :1;    
        uint8    Pin1_DirConfig    :1;    
        uint8    Pin2_DirConfig    :1;    
        uint8    Pin3_DirConfig    :1;    
        uint8    Pin4_DirConfig    :1;    
        uint8    Pin5_DirConfig    :1;    
        uint8    Pin6_DirConfig    :1;    
        uint8    Pin7_DirConfig    :1;    
    }Pins;   
}Port_DirConfigType;


/*
The configurable features for each port are:

    Symbolic pin name 
    Functionality (Mode)
    Dynamic direction changeabilty
    LEVEL - Data
    DD    - Data Direction
    RD    - Reduced Drive
    PE    - Pull Device Enable
    PS    - Polarity Select
    W-OR  - Wired-OR Mode
    IE    - Interrupt Enable
    DIE   - Digital Input Enable

The configuration availability for each port are:

    Port   LEVEL  DD   RD   PE   PS  W-OR  DIE
    A        Y    Y    y    y    -    -    -
    B        Y    Y    y    y    -    -    -
    E        Y    Y    y    y    -    -    -
    K        Y    Y    y    y    -    -    -
    T        Y    Y    Y    Y    Y    -    -
    S        Y    Y    Y    Y    Y    Y    -
    M        Y    Y    Y    Y    Y    Y    -
    P        Y    Y    Y    Y    Y    -    -
    H        Y    Y    Y    Y    Y    -    -
    J        Y    Y    Y    Y    Y    -    -
    AD0L     Y    Y    Y    Y    -    -    Y
    AD0H     Y    Y    Y    Y    -    -    Y
Following features are available for all ports:
    Symbolic pin name 
    Functionality (Mode)
    Dynamic direction changeabilty
*/

/* Configuration structure for ports A, B,E and K */
typedef struct{                            
    Port_LevelType      Level;           /* Data Register */
}Port_ABCDEKRegType;

/* Configuration structure for port T */
typedef struct{ 
    Port_LevelType      Level;           /* Data Register */
    Port_DriveType      Drive;           /* Reduced Drive Register */
    Port_PullEnableType PullEnable;      /* Pull Device Enable Register */
    Port_PolarityType   Polarity;        /* Polarity Select Register */
}Port_TType;

/* Configuration structure for ports S and M */
typedef struct{                            
    Port_LevelType      Level;           /* Data Register */
    Port_DriveType      Drive;           /* Reduced Drive Register */
    Port_PullEnableType PullEnable;      /* Pull Device Enable Register */
    Port_PolarityType   Polarity;        /* Polarity Select Register */
    Port_WiredOrType    WiredOr;         /* Wired-OR Mode Register */
}Port_SMType;

/* Configuration structure for ports P, H and J */
typedef struct{                            
    Port_LevelType      Level;           /* Data Register */
    Port_DriveType      Drive;           /* Reduced Drive Register */
    Port_PullEnableType PullEnable;      /* Pull Device Enable Register */
    Port_PolarityType   Polarity;        /* Polarity Select Register */
}Port_PHJType;

/* Configuration structure for ports AD0L, AD0H  */
typedef struct{                            
    Port_LevelType      Level;           /* Data Register */
    Port_DriveType      Drive;           /* Reduced Drive Register */
    Port_PullEnableType PullEnable;      /* Pull Up Enable Register */
    Port_DIEType        DIE;             /* Digital Input Enable Register */
}Port_A2DType;

/* Structure for only Direction and DirChangeable */
typedef struct{
    Port_DirectionType  Direction;       /* Data Direction Register */
    Port_DirConfigType  DirChangeable;   /* Direction Changeable */
}Port_DirAndDirChangeType;

/* Miscellaneous configuration structure */
typedef struct{                            
    Port_PullEnableType PullEnable;  /* Pull-up Up Control Register (PUCR) */
    Port_DriveType      Drive;       /* Reduced Drive Register (RDRIV) */
    Port_EClkType       ECLK;        /* ECLK Control Register (ECLKCTL) */
    Port_ModRType       MODR;        /* Module Routing Register (MODRR) */
    uint8               PTT_RR;      /* Port T routing Register (PTTRR) */
}Port_ControlType;

/* Consolidated Configuration structure */
typedef struct{                           
    Port_ABCDEKRegType  Port_A;      /* Configuration structure for Port A */
    Port_ABCDEKRegType  Port_B;      /* Configuration structure for Port B */
    Port_ABCDEKRegType  Port_E;      /* Configuration structure for Port E */
    Port_TType          Port_T;      /* Configuration structure for Port T */
    Port_SMType         Port_S;      /* Configuration structure for Port S */
    Port_SMType         Port_M;      /* Configuration structure for Port M */
    Port_PHJType        Port_P;      /* Configuration structure for Port P */
    Port_PHJType        Port_J;      /* Configuration structure for Port J */
    Port_A2DType        Port_A2D0L;   /* Configuration structure for Port AD0L */
    /* Direction and DirChangeable for all the ports */
    Port_DirAndDirChangeType Port_AToA2D[PORT_MAX_INDEX];    
    Port_ControlType    Port_Control;/* Miscellaneous configuration structure */
}Port_ConfigType;

/*.........................Global Variable declarations......................*/
extern const Port_ConfigType Port_Config[PORT_MAX_NUMBER_OF_CONFIG];

/* Save the configured PTT level */
extern uint8 Port_PTT_Level;

/*.........................Global Function Prototypes........................*/

extern void Port_Init(const Port_ConfigType* ConfigPtr);
extern void Port_SetPinDirection(Port_PinType Pin,
                                 Port_PinDirectionType Direction);
extern void Port_RefreshPortDirection(void);
extern void Port_GetVersionInfo(Std_VersionInfoType *versioninfo);


#endif /* End of #ifndef _PORT_H */

/*...............................END OF Port.h................................*/