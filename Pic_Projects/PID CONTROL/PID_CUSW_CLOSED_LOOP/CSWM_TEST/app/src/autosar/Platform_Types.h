/* Created at 15:25 on 1-4-2009 */
/* Version of HALCoGen used : 2.9.3 */
/* Code generated for       : MC9S12XS128 */
/*******************************************************************************
**                                                                            **
**  SRC-MODULE: Platform_Types.h                                              **
**                                                                            **
**  Copyright (C) none (open standard)                                        **
**                                                                            **
**  TARGET    : ALL                                                           **
**                                                                            **
**  COMPILER  : Code Warrior/Cosmic                                           **
**                                                                            **
**  PROJECT   : AUTOSAR Demonstrator R1, BMW Standard Core 6                  **
**                                                                            **
**  AUTHOR    : Robert Feist                                                  **
**                                                                            **
**  PURPOSE   : Provision of platform and compiler dependend types            **
**                                                                            **
**  REMARKS   : Version control and support provided by BMW Group             **
**              Implemented SWS: SWS Platform Types V1.0.0 (final)            **
**                                                                            **
**  PLATFORM DEPENDANT [yes/no]: yes                                          **
**                                                                            **
**  TO BE CHANGED BY USER [yes/no]: no                                        **
**                                                                            **
*******************************************************************************/

/*******************************************************************************
**                      Author Identity                                       **
********************************************************************************
**
** Initials     Name                       Company
** --------     -------------------------  ------------------------------------
** fer          Robert Feist               BMW Group
** cma          Christoph Mueller-Albrecht BMW Group
**
*******************************************************************************/

/*******************************************************************************
**                      Revision Control History                              **
*******************************************************************************/

/*
 * V5.0.1:  01.07.2005, fer  : All type constructs using 'short int' or 'long int'
                               replaced by sole use of 'short' and 'long'.
                               uint8_least is mapped to 'unsigned char' due to
                               better results.
                               uint16_least is mapped to 'unsigned short' due to
                               better results.
 * V5.0.0:  03.05.2005, fer  : Typedefinition for 'BitType' removed
 * V4.0.0:  22.04.2005, fer  : Type name 'bool' replaced by name 'boolean'
 * V3.0.0:  15.04.2005, fer  : Bit_Type changed to BitType
                               Defines TRUE and FALSE moved from Std_Types.h in
                               here due to possible conflicts with compilers already
                               providing these as in-built symbols. In this case,
                               the definitions will be excluded.
 * V2.1.0:  16.03.2005, fer  : Type 'bool' added
 * V2.0.0:  10.03.2005, fer  : block comments added (**), released
 * V1.0.0:  09.03.2005, fer  : initial version
*/


#ifndef PLATFORM_TYPES_H
#define PLATFORM_TYPES_H

/*******************************************************************************
**                      Include Section                                       **
*******************************************************************************/

/*******************************************************************************
**                      Global Symbols                                        **
*******************************************************************************/
/*
  File version information
*/
#define PLATFORM_MAJOR_VERSION  5
#define PLATFORM_MINOR_VERSION  0
#define PLATFORM_PATCH_VERSION  1

/*
  CPU register type width
*/
#define CPU_TYPE_8        8
#define CPU_TYPE_16       16
#define CPU_TYPE_32       32

/*
  Bit order definition
*/
#define MSB_FIRST         0                 /* Big endian bit ordering        */
#define LSB_FIRST         1                 /* Little endian bit ordering     */

/*
  Byte order definition
*/
#define HIGH_BYTE_FIRST   0                 /* Big endian byte ordering       */
#define LOW_BYTE_FIRST    1                 /* Little endian byte ordering    */

/*
  Word order definition
*/
#define HIGH_WORD_FIRST   0                 /* Big endian word ordering       */
#define LOW_WORD_FIRST    1                 /* Little endian word ordering    */


/*
  Platform type and endianess definitions for MC9S12XDP512
*/
#define CPU_TYPE            CPU_TYPE_16
#define CPU_BIT_ORDER       LSB_FIRST
#define CPU_BYTE_ORDER      HIGH_BYTE_FIRST

/*******************************************************************************
**                      Global Data Types                                     **
*******************************************************************************/

/*
  AUTOSAR integer data types
*/
typedef signed char         sint8;          /*        -128 .. +127            */
typedef unsigned char       uint8;          /*           0 .. 255             */
typedef signed int          sint16;         /*      -32768 .. +32767          */
typedef unsigned int        uint16;         /*           0 .. 65535           */
typedef signed long         sint32;         /* -2147483648 .. +2147483647     */
typedef unsigned long       uint32;         /*           0 .. 4294967295      */
typedef float               float32;
typedef double              float64;

typedef unsigned char       uint8_least;    /* At least 8 bit                 */
typedef unsigned short      uint16_least;   /* At least 16 bit                */
typedef unsigned long       uint32_least;   /* At least 32 bit                */
typedef signed char         sint8_least;    /* At least 7 bit + 1 bit sign    */
typedef signed short        sint16_least;   /* At least 15 bit + 1 bit sign   */
typedef signed long         sint32_least;   /* At least 31 bit + 1 bit sign   */

typedef unsigned char       boolean;        /* for use with TRUE/FALSE        */

#ifndef TRUE                                             /* conditional check */
 #define TRUE      ((boolean) 1)
#endif

#ifndef FALSE                                            /* conditional check */
 #define FALSE     ((boolean) 0)
#endif

/*******************************************************************************
**                      Global Data                                           **
*******************************************************************************/


/*******************************************************************************
**                      Global Function Prototypes                            **
*******************************************************************************/


#endif /* PLATFORM_TYPES_H */
