/* Created at 15:24 on 1-4-2009 */
/* Version of HALCoGen used : 2.9.3 */
/* Code generated for       : MC9S12XS128 */
/*****************************************************************************/
/* Copyright(c) - This Program/Software is the exclusive property of Infosys. 
The software may not be reproduced or given to third parties without prior 
consent of Infosys.

     Filename                      : RamTst_Types.h
     Version                       : 1.0.0
     Microcontroller Platform      : MC9S12XS128
     Compiler                      : Codewarrior HCS12X V5.0.28

     Description                   : Header file for RAM Test common types.
                                     This file contains all the common user
                                     defined data types for the files RamTst.h 
                                     and RamTst_Algo.h
                                     
     Requirement Specification     : AUTOSAR_SWS_RAMTest.doc 0.12
                
     Module Design                 : AUTOTMAL_DDD_RAMTest_SPAL2.0.doc 
                                     version 2.00
     
     Platform Dependant[yes/no]    : yes
     
     To be changed by user[yes/no] : no
*/
/*****************************************************************************/
/* Revision History

Class of Change
-------------------------------------------------------------------------------
N - New Module.
F - Functional development.
E - Error Correction.
O - Optimization without functional change.
-------------------------------------------------------------------------------
 00      09-May-2006     Khanindra Jyoti Deka, Infosys
-------------------------------------------------------------------------------
Class: N
Cause: New Module
Detail:This file contains all the common user defined data types for the files 
       RamTst.h and RamTst_Algo.h
-------------------------------------------------------------------------------

******************************************************************************/

/* to avoid Multiple inclusion */
#ifndef _RAMTST_TYPES_H
#define _RAMTST_TYPES_H

/*...................................Macros..................................*/

#define NO_CONTENT                         (0)
#define NO_OPERATION                       (0)

#define PRESENT_BLOCK_ID             RamTst_BlockIDForAlgo
#define RAMTST_BLOCK_START_ADDRESS   RamTst_BlockStartAddressForAlgo
#define RAMTST_BLOCK_END_ADDRESS     RamTst_BlockEndAddressForAlgo

/*.........................Type Definitions..................................*/

/* RAM Test Result Type */
typedef enum
{
    /* The RAM Test is not executed. */
    /* This shall be the default value after reset. */
    /* This status shall have the value 0. */
    RAMTST_RESULT_NOT_TESTED=0,

    /* The RAM Test has been tested with OK result */
    RAMTST_RESULT_OK,

    /* The RAM Test has been tested with NOT-OK result. */
    RAMTST_RESULT_NOT_OK,

    /* This should never happen. Just for redundancy. */
    RAMTST_RESULT_UNDEFINED

} RamTst_TestResultType;

/* Data type of number of tested RAM cells */
typedef uint16 RamTst_NumberOfTestedCellsType; 

/* Data type of block address pointer */
typedef uint32 RamTst_BlockAddressType;  

/* Data type of number of RAM blocks  */
typedef uint16 RamTst_BlockIDType;  

#endif  /*_RAMTST_TYPE_H*/

/*...........................END OF RamTst_Types.h...........................*/

