/* Created at 15:24 on 1-4-2009 */
/* Version of HALCoGen used : 2.9.3 */
/* Code generated for       : MC9S12XS128 */
/*****************************************************************************/
/* Copyright(c) - This Program/Software is the exclusive property of Infosys. 
The software may not be reproduced or given to third parties without prior 
consent of Infosys.

     Filename                      : RamTst_cfg.h
     Version                       : 1.0.1
     Microcontroller Platform      : MC9S12XS128
     Compiler                      : Compiler for Freescale HC12 V5.0.30

     Description                   : Header file for RAM Test.
                                     
     Requirement Specification     : AUTOSAR_SWS_RAMTest.doc 0.12
                
     Module Design                 : AUTOTMAL_DDD_RAMTest_SPAL2.0.doc  
                                     version 2.00
     
     Platform Dependant[yes/no]    : yes
     
     To be changed by user[yes/no] : no
*/
/*****************************************************************************/
   
/* Revision History

Class of Change
-------------------------------------------------------------------------------
N - New Module.
F - Functional development.
E - Error Correction.
O - Optimization without functional change.
-------------------------------------------------------------------------------
 00      09-May-2006     Khanindra Jyoti Deka, Infosys
-------------------------------------------------------------------------------
Class: N
Cause: New Module
Detail:This is header file for the RAM Test configuration parameters.
-------------------------------------------------------------------------------
 01      17-Oct-2007     Prashant Kulkarni, Infosys
-------------------------------------------------------------------------------
Class: E
Cause: Bug fixing
Detail:Fixed issue 830. RAMTST_TEST_COMPLETED_NOTIFICATION and
		RAMTST_TEST_ERROR_NOTIFICATION misspelled as RAMTST_TEST_COMPLETED and
		RAMTST_TEST_ERROR_OCCURED
-------------------------------------------------------------------------------

******************************************************************************/

/* to avoid Multiple inclusion */
#ifndef _RAMTST_CFG_H
#define _RAMTST_CFG_H

/*...................................File Includes...........................*/

/* Stdtypes header file */
#include "Std_Types.h"

/*...................................Macros..................................*/
/* RAM Test Module Configuration */

/* Pre-processor switch to enable development error detection & reporting. */
#define RAMTST_DEV_ERROR_DETECT                       OFF 
/* Pre-processor switch to enable production error detection & reporting. */
#define RAMTST_PRO_ERROR_DETECT                       OFF 

/* Configuration structure */

/*  Number of configured algorithms */
#define RAMTST_NUMBER_OF_ALGORITHMS                   (1)
/* Default algorithm ID*/
#define RAMTST_DEFAULT_TEST_ALGORITHM                 ((RamTst_AlgorithmType)1)
/* RAM Test complete notification */
#define RAMTST_TEST_COMPLETED_NOTIFICATION                 NULL_PTR
/* RAM Test error notification */
#define RAMTST_TEST_ERROR_NOTIFICATION          NULL_PTR

/* CHECKERBOARD Algorithm */
#define RAMTST_CHECKERBOARD_ALGORITHM_ID              ((RamTst_AlgorithmType)1)
#define RAMTST_CHECKERBOARD_NUMBER_OF_TESTED_CELLS    (16)
#define RAMTST_CHECKERBOARD_NUMBER_OF_BLOCKS          (1)

/* Block Number 1*/
#define RAM_TEST_CB_BLCK1                                       (1)
#define RAMTST_CHECKERBOARD_START_ADDRESS_1           (0x2000)    //(0x000FE100) MODIFIED
#define RAMTST_CHECKERBOARD_END_ADDRESS_1             (0x3DDD)    //(0x000FE200) MODIFIED


/* Total number of blocks */
#define RAMTST_TEST_TOTAL_NUMBER_OF_BLOCKS            ((RamTst_BlockIDType)1)

#endif
/*............................END OF RamTst_cfg.h............................*/

