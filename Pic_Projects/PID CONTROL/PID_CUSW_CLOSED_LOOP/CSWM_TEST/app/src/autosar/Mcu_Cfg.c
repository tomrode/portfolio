/* Created at 15:25 on 1-4-2009 */
/* Version of HALCoGen used : 2.9.3 */
/* Code generated for       : MC9S12XS128 */
/*****************************************************************************/
/* Copyright(c) - This Program/Software is the exclusive property of Infosys.
The software may not be reproduced or given to third parties without prior
consent of Infosys.

Filename                        : Mcu_Cfg.c
Version                         : 3.2.4
Microcontroller Platform        : MC9S12XS128
Compiler                        : Codewarrior HCS12X V5.7.0

Description                     : This file contains the initialisation of 
                                  configuration parameters.
Requirement Specification       : AUTOSAR_SWS_McuDriver.pdf version 1.2.0
   
Module Design                   : AUTOSAR_DDD_MCU_SPAL2.0.doc version 2.10a
*/

/*****************************************************************************/

/* Revision History

Class of Change
-------------------------------------------------------------------------------
N - New Module.
F - Functional development.
E - Error Correction.
O - Optimization without functional change.
-------------------------------------------------------------------------------
 00     02-Feb-06     Kapil Kumar, Infosys
-------------------------------------------------------------------------------
Class: N
Cause: New Module
Detail:This file implements the MCU driver configuration.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 01     30-Mar-2006     Kapil Kumar, Infosys
-------------------------------------------------------------------------------
Class: F
Cause: Changes for SPAL2.0
Detail:Changes done as per AUTOSAR_SWS_McuDriver.pdf ver 1.2.0
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 02     24-Jul-2006     Harini KE, Infosys
-------------------------------------------------------------------------------
Class: F
Cause: Changes for SPAL2.0
Detail:Changes done for including Macro enabling of certain APIs and Enabling 
of RTI interrupt based on the user selection.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
03     11-Dec-2006     Anwar Husen, Infosys
-------------------------------------------------------------------------------
Class: N
Cause: New Module
Detail:This file is updated for MC9S12XEP100 derivative for the MCU driver 
configuration.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
04     11-Jan-2007        Anwar Husen, Infosys
-------------------------------------------------------------------------------
Class: F
Cause: FM0,FM1 bit setting
Detail:For each Clock setting there will be a separate Frequency modulation ratio
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
05     30-Jan-2007        Anwar Husen, Infosys
-------------------------------------------------------------------------------
Class: F
Cause: Change request #609 in PRL
Detail:Pre-processor checks are added to implement the Change request #609.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------/
 08     18-Oct-2007   Prashant Kulkarni , Infosys/
-------------------------------------------------------------------------------/
Class : F/
Cause : To support XS128 microcontroller/
Detail: preprocessor added for MCU_ECT_DELAY_COUNTER as it is not applicable/
			for XS128/
-------------------------------------------------------------------------------/
*******************************************************************************/


#include "Mcu.h"

const Mcu_ConfigType   Mcu_Config =
{
    MCU_CONFIGURED_RAM_SECTORS,
    MCU_CONFIGURED_MCU_MODES,
    MCU_CONFIGURED_CLOCK_SETTINGS,


{
     {

    /* Ram sector configuration for 1 RAM section(s) */
        /* MCU_RAM_SECTOR_SETTING 0 */
        MCU_RAM_BASE_ADDRESS_0,
        MCU_RAM_SIZE_0,
        MCU_RAM_PRESET_0
     }
},


{
     {

    /* MCU mode configuration for 1 mode(s)*/ 
        /* MCU_POWER_MODE_SETTING 0 */
        MCU_POWER_MODE_0,
        MCU_RTI_WAITMODE_0,
        MCU_PLL_WAITMODE_0,
        MCU_COP_PSEUDOSTOPMODE_0,
        MCU_RTI_PSEUDOSTOPMODE_0
     }
},


{
     {

    /* Clock configuration for 1 setting(s) */
        /* MCU_CLOCK_SETTING_VALUE 0 */
        MCU_SYNR_VALUE_0,
        MCU_REFDIV_VALUE_0,
        MCU_POSTDIV_VALUE_0,
        MCU_PLLCTL_FM_0
     }
},
    /* Reset type configuration */
    MCU_RESET_TYPE_0,

    MCU_CRG_INT,



{
     {

    /* ECT HW setting(s) */
        MCU_ECT_HW_SETTINGS_0,
        MCU_ECT_PRESCALER_0
     }
},
        MCU_WKSOURCE_POWER,
        MCU_WKSOURCE_RESET,
        MCU_WKSOURCE_INTERNAL_RESET,
        MCU_WKSOURCE_INTERNAL_WDG,
        MCU_WKSOURCE_EXTERNAL_WDG,
        MCU_WKSOURCE_TIMER_EXPIRED,
        MCU_WKSOURCE_ICU,              
        MCU_WKSOURCE_GPT,              
        MCU_WKSOURCE_CANIF,          
        MCU_WKSOURCE_UNUSED

};