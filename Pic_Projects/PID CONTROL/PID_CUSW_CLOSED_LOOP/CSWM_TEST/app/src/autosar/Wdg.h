/* Created at 15:25 on 1-4-2009 */
/* Version of HALCoGen used : 2.9.3 */
/* Code generated for       : MC9S12XS128 */
//****************************************************************************/
/* Copyright(c) - This Program/Software is the exclusive property of Infosys.
The software may not be reproduced or given to third parties without prior
consent of Infosys.

     Filename                      : Wdg.h
     Version                       : 1.1.10
     Microcontroller Platform      : MC9S12XS128
     Compiler                      : Codewarrior HCS12X V5.7.0

     Description                   : Header file for WDG

     Requirement Specification     : AUTOSAR_SWS_Watchdog_Driver.pdf 2.0

     Module Design                 : AUTOTMAL_DDD_WDG_SPAL2.0.doc version 1.00

     Platform Dependant[yes/no]    : yes

     To be changed by user[yes/no] : no
*/
/*****************************************************************************/

/* Revision History

Class of Change
-------------------------------------------------------------------------------
N - New Module.
F - Functional development.
E - Error Correction.
O - Optimization without functional change.
-------------------------------------------------------------------------------
 00      04-Feb-2006     Arindam Gupta, Infosys
-------------------------------------------------------------------------------
Class: N
Cause: New Module
Detail:This is header file for Watchdog Driver.
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
 01      27-Mar-2006     Arindam Gupta, Infosys
-------------------------------------------------------------------------------
Class: N
Cause: Changes done for SPAL2.0
Detail:The changes are done as per AUTOSAR_SWS_WatchdogDriver.pdf 2.0.
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
 02      03-Jul-2006     Arindam Gupta, Infosys
-------------------------------------------------------------------------------
Class: O
Cause: Added macro check for Trigger function.
Detail:The changes are done as per new requirement from Temic.
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
 03      17-Jul-2006     Arindam Gupta, Infosys
-------------------------------------------------------------------------------
Class: O
Cause: RTRT anomaly report for Beta2
Detail:Updated to suit the anomaly report
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
 04     15-Nov-2006      Ajaykumar, Infosys
-------------------------------------------------------------------------------
Class: E
Cause: Correction of issue No: 527 in PRL
Detail:Wdg_State initialised to WDG_UNINIT globally in Wdg.c
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
 05      01-Dec-2006     Arindam Gupta, Infosys
-------------------------------------------------------------------------------
Class: O
Cause: Changes done as per the changes in Wdg.c file
Detail:Changes done as per the changes in Wdg.c file
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
 06     29-Mar-2007   Khanindra , Infosys
-------------------------------------------------------------------------------
Class : F
Cause : For Implementation of Freescale security recommendation
Detail: For Implementation of Freescale security recommendation
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
 07     22-Aug-2007      Isha Gupta, Infosys
-------------------------------------------------------------------------------
Class : F
Cause : Updated in sync with Wdg.c
Detail: Updated in sync with Wdg.c
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
 08     29-Nov-2007      Isha Gupta, Infosys
-------------------------------------------------------------------------------
Class : O
Cause : Updated in sync with Wdg.c
Detail: Updated in sync with Wdg.c
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
 09     31-Mar-2008      Isha Gupta, Infosys
-------------------------------------------------------------------------------
Class : E
Cause : Updated for PRL issue #973
Detail: Values for error codes reported by WDG API services have been corrected
-------------------------------------------------------------------------------

******************************************************************************/
/* to avoid Multiple inclusion */
#ifndef _WDG_H
#define _WDG_H

/*...................................File Includes...........................*/

/* Watchdog Interface Types header file */
#include "WdgIf_Types.h"

/* Watchdog Driver configuration file */
#include "Wdg_Cfg.h"



/* This header file includes all the standard data types
   and common return types
*/
#include "Std_Types.h"

/* Infosys defines header file */
#include "MCAL_Types.h"


/*...................................Macros..................................*/

/* Declaring the required macros to perform the version check */
#define WDG_SW_MAJOR_VERSION                               1
#define WDG_SW_MINOR_VERSION                               1
#define WDG_SW_PATCH_VERSION                               10

#define WDG_AR_MAJOR_VERSION                               2
#define WDG_AR_MINOR_VERSION                               0
#define WDG_AR_PATCH_VERSION                               0

/* Declaring the Vendor ID */
#define WDG_VENDOR_ID                	              (uint16)40


/* Declaring the Vendor ID */
#define WDG_MODULE_ID																(uint8)102

/* Declaring the service ID macros of the Watchdog Driver service APIs */

/* Declaring the service ID for Wdg_Init function */
#define WDG_INIT_ID                                 (uint8)0

/* Declaring the service ID for Wdg_Setmode function */
#define WDG_SETMODE_ID                              (uint8)1

/* Declaring the service ID for Wdg_Init function */
#define WDG_TRIGGER_ID                              (uint8)2

/* Assigning a value for Normal Trigger mode */
#define WDG_NORMAL_TRIGGER                          (uint8)0

/* Assigning a value for Window Trigger mode */
#define WDG_WINDOW_TRIGGER                          (uint8)1

/* Declaring the initial and final activation codes to restart the
   Watchdog timer */
#define WDG_INITIAL_ACTIVATION_CODE                  0x55
#define WDG_FINAL_ACTIVATION_CODE                    0xAA

/* Following development errors are generated by Watchdog Driver API services,
if invoked with incorrect input parameters */
#define WDG_E_DRIVER_STATE                          ((uint8)0x10)
#define WDG_E_PARAM_MODE                            ((uint8)0x11)
#define WDG_E_PARAM_CONFIG                          ((uint8)0x12)


/*.........................Type Definitions..................................*/

typedef enum
{
     WDG_OFF_MODE,
     WDG_SLOW_MODE,
     WDG_FAST_MODE
}Wdg_ModeType;

typedef enum
{
     /* Watchdog is not initialised or unusable */
     WDG_UNINIT,
     /* Watchdog is idle and not being switched modes */
     WDG_IDLE,
     /* Watchdog is currently being switched */
     WDG_BUSY
}Wdg_Driver_State;

typedef struct
{
    Wdg_ModeType Wdg_Default_Mode;

}Wdg_ConfigType;

extern const Wdg_ConfigType  Wdg_Config[WDG_MAX_NUMBER_OF_CONFIG];

/*.........................Global Function Prototypes........................*/

/* This function initialises all the initialisation variables of the module */
extern void Wdg_Init(const Wdg_ConfigType *ConfigPtr);

/* This function sets the mode of the driver requested by the upper layer */
extern Std_ReturnType Wdg_SetMode(WdgIf_ModeType Mode);

/* Macro definition of the function */
#define Wdg_Trigger() ARMCOP=WDG_INITIAL_ACTIVATION_CODE; \
                      ARMCOP=WDG_FINAL_ACTIVATION_CODE
/* This function triggers the hardware */
extern void Wdg_GetVersionInfo(Std_VersionInfoType *versionInfo);



#endif /* end _WDG_H */


/*...............................END OF Wdg.h................................*/
