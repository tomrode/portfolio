/*****************************************************************************/
/* Copyright(c) - This Program/Software is the exclusive property of Infosys.
The software may not be reproduced or given to third parties without prior
consent of Infosys.

     Filename                      : ComM.h
     Version                       : 1.0.0
     Microcontroller Platform      : MC9S12XS128
     Compiler                      : Codewarrior HCS12X V5.7.0

     Description                   : This file contains ComM definitions
                                     required for compilation and testing
                                     of the Can Interface module.

     Requirement Specification     : N.A.

     Platform Dependant[yes/no]    : no

     To be changed by user[yes/no] : no
*/
/*****************************************************************************/

/* To avoid double inclusion */
#ifndef _COMM_H
#define _COMM_H
typedef unsigned char ComM_ChannelHandleType;
#endif /* end _COMM_H */
/*...............................END OF ComM.h................................*/
