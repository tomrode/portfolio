/* Created at 15:25 on 1-4-2009 */
/* Version of HALCoGen used : 2.9.3 */
/* Code generated for       : MC9S12XS128 */
/******************************************************************************/
/* Copyright(c) - This Program/Software is the exclusive property of Infosys. 
The software may not be reproduced or given to third parties without prior 
consent of Infosys.

     Filename                      : GptIf_Cbk.h
     Version                       : 3.2.0
     Microcontroller Platform      : MC9S12XS128
     Compiler                      : Codewarrior HCS12X V4.5

     Description                   : Header file for GptIf_Cbk.c

     Requirement Specification     : WP4.2.2.1.12-SWS-GPT-Driver.doc 1.1.6

     Module Design                 : AUTOTMAL_DDD_GPT_SPAL2.0.doc version 2.00c

     Platform Dependant[yes/no]    : yes   

     To be changed by user[yes/no] : no       
*/
/*****************************************************************************/

/* Revision History

Class of Change
-------------------------------------------------------------------------------
N - New Module.
F - Functional development.
E - Error Correction.
O - Optimization without functional change.
-------------------------------------------------------------------------------
 00    02-Feb-2006     Sowmya, Infosys
-------------------------------------------------------------------------------
Class: N
Cause: New Module
Detail:Header file for GptIf_Cbk.c
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 01    22-Mar-2006     Sowmya, Infosys
-------------------------------------------------------------------------------
Class: F
Cause: Functional development
Detail:Upgrade to suit to 1.1.6 GPT specification.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 02    08-Dec-2006     Ramprasad P B, Infosys
-------------------------------------------------------------------------------
Class: N
Cause: New Module
Detail:Header file for GptIf_Cbk.c for S12XEP100.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 03    24-Jul-2007      Khanindra, Infosys
-------------------------------------------------------------------------------
Class: F
Cause: Updated for the derivatives MC9S12XEG384 and MC9S12XEG256 
Detail:Updated for the derivatives MC9S12XEG384 and MC9S12XEG256
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 04    01-Oct-2007      Khanindra, Infosys
-------------------------------------------------------------------------------
Class: F
Cause: Updated for the derivatives MC9S12XS128
Detail:Updated for the derivatives MC9S12XS128. Preprocessor switch 
       HAL_MC9S12XS128 is used to differentiate from other derivatives.As 
       Modulus Down Counter is not available in MC9S12XS128,so Modulus Down 
       Counter related codes are removed using Preprocessor switch.
-------------------------------------------------------------------------------
******************************************************************************/

/* to avoid Multiple inclusion */
#ifndef _GPTIF_CBK_H
#define _GPTIF_CBK_H

/**********************Callback functions*************************************/

void Gpt_TmSlice_Isr(void);


#endif   

/*...............................END OF GptIf_Cbk.h................................*/