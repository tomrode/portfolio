/* Created at 15:24 on 1-4-2009 */
/* Version of HALCoGen used : 2.9.3 */
/* Code generated for       : MC9S12XS128 */
/*****************************************************************************/
/* Copyright(c) - This Program/Software is the exclusive property of Infosys.
The software may not be reproduced or given to third parties without prior
consent of Infosys.

    Filename                      : Adc_Cbk.c
    Version                       : 2.0.0
    Microcontroller Platform      : MC9S12XS128
    Compiler                      : Codewarrior HCS12X V5.0.28

    Description                   : This file contains definitions for all 
                                    callbacks/notifications.

    Requirement Specification     : AUTOSAR_SWS_ADC_Driver.pdf version 1.0.16

    Module Design                 : AUTOSAR_DDD_ADC_SPAL2.0.doc version 2.11

    Platform Dependant[yes/no]    : no
     
    To be changed by user[yes/no] : yes

*/

/*****************************************************************************/
   
/*...................................File Includes...........................*/

#include "AdcIf_Cbk.h"


/*..............................Function Definitions.........................*/



