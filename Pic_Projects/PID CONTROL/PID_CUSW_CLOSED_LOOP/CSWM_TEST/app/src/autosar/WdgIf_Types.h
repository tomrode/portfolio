/* Created at 15:25 on 1-4-2009 */
/* Version of HALCoGen used : 2.9.3 */
/* Code generated for       : MC9S12XS128 */
//****************************************************************************/
/* Copyright(c) - This Program/Software is the exclusive property of Infosys.
The software may not be reproduced or given to third parties without prior
consent of Infosys.

     Filename                      : WdgIf_Types.h
     Version                       : 1.0.1
     Microcontroller Platform      : MC9S12XS128
     Compiler                      : Codewarrior HCS12X V5.7.0

     Description                   : Header file for WDGIF Types

     Requirement Specification     : AUTOSAR_SWS_Watchdog_Interface.pdf 2.0

     Module Design                 : AUTOTMAL_DDD_WDGIF_SPAL2.0.doc version 1.00

     Platform Dependant[yes/no]    : no

     To be changed by user[yes/no] : no
*/
/*****************************************************************************/

/* Revision History

Class of Change
-------------------------------------------------------------------------------
N - New Module.
F - Functional development.
E - Error Correction.
O - Optimization without functional change.
-------------------------------------------------------------------------------
 00      02-Feb-2006     Arindam Gupta, Infosys
-------------------------------------------------------------------------------
Class: N
Cause: New Module
Detail:This is header file depicting types of Watchdog Interface.
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
 01      27-Mar-2006     Arindam Gupta, Infosys
-------------------------------------------------------------------------------
Class: N
Cause:  Changes done for SPAL 2.0
Detail: The changes are done as per AUTOSAR_SWS_WatchdogInterface.pdf 2.0
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 02      28-Feb-2007       Ramprasad P B, Infosys
-------------------------------------------------------------------------------
Class: E
Cause:  Bug fixing
Detail: Enum WdgIf_StatusType member WDIF_IDLE corrected as WDGIF_IDLE
-------------------------------------------------------------------------------

******************************************************************************/

/* to avoid Multiple inclusion */
#ifndef _WDGIF_TYPES_H
#define _WDGIF_TYPES_H

/*...................................File Includes...........................*/

/* Standard Types header file */
#include "Std_Types.h"

/* Watchdog Driver header file */
#include "WdgIf_Cfg.h"


/*.........................Type Definitions..................................*/

/* These are the different state transitons the Watchdog driver might undergo*/
typedef enum
{
     /* Watchdog is not initialised or unusable */
     WDGIF_UNINIT,
     /* Watchdog is idle and not being switched modes */
     WDGIF_IDLE,
     /* Watchdog is currently being switched */
     WDGIF_BUSY
}WdgIf_StatusType;

/* These are the different modes the Watchdog driver might operate in */
typedef enum
{
     /* Watchdog is disabled */
     WDGIF_OFF_MODE,
     /* Watchdog is setup for a long timeout period */
     WDGIF_SLOW_MODE,
     /* Watchdog is setup for a short timeout period */
     WDGIF_FAST_MODE
}WdgIf_ModeType;


#endif /* end _WDGIF_TYPES_H */


/*...............................End of WdgIf_Types.h.........................*/






