/* Created at 15:25 on 1-4-2009 */
/* Version of HALCoGen used : 2.9.3 */
/* Code generated for       : MC9S12XS128 */
/******************************************************************************/
/* Copyright(c) - This Program/Software is the exclusive property of Infosys. 
   The software may not be reproduced or given to third parties without prior 
   consent of Infosys.
                     
   Filename                      : Port_Cfg.h
   Version                       : 2.0.3
   Microcontroller Platform      : MC9S12XS128
   Compiler                      : Codewarrior HCS12X V5.0.28

   Description                   : Configuration Header file for PORT Driver

   Requirement Specification     : AUTOSAR_SWS_Port_Driver_working.doc ver1.1.3
                
   Module Design                 : AUTOTMAL_DDD_PORT_SPAL2.0.doc version 1.0

   Platform Dependant[yes/no]    : yes   
     
   To be changed by user[yes/no] : no  
*/
/******************************************************************************/
/* Revision History

Class of Change
-------------------------------------------------------------------------------
N - New Module.
F - Functional development.
E - Error Correction.
O - Optimization without functional change.
-------------------------------------------------------------------------------
 00        03-Feb-2006     Vijayakumar K, Infosys
-------------------------------------------------------------------------------
Class : N
Cause : New Module
Detail: This is the configuration header file for the PORT driver.
-------------------------------------------------------------------------------
 01        03-Mar-2006     Muthuvelavan Natarajan, Infosys
-------------------------------------------------------------------------------
Class : F
Cause : Functional development
Detail: Update for SPAL2.0.
-------------------------------------------------------------------------------
 02        09-Jun-2006     Vijayakumar K, Infosys
-------------------------------------------------------------------------------
Class : F
Cause : Support multiple configuration structures & IRQCR configuration
Detail: Symbolic pin name & pin functionality are moved out of config structure
-------------------------------------------------------------------------------
 03        27-Jun-2006     Vijayakumar K, Infosys
-------------------------------------------------------------------------------
Class : O
Cause : Macros/Inline for speed optimization
Detail: Port_SetPinDirection is configurable as Macro
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 04        31-Jan-2007     Kasinath, Infosys
-------------------------------------------------------------------------------
Class : F
Cause : A2D0 & A2D1 Channel Changes
Detail: Port A2D0 modified to handle 16 channels and port A2D1 8 channels.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 05       11-Apr-2007      Kasinath, Infosys
-------------------------------------------------------------------------------
Class : E
Cause : Fix for issue 716
Detail: Pins not used in lower pin packages also initialized.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 06        05-Oct-2007     Ajaykumar, Infosys
-------------------------------------------------------------------------------
Class : F
Cause : MC9S12XS128 controller support
Detail: MC9S12XS128 related pre-processor switches added.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
07         16-Nov-2007     Ajaykumar, Infosys
-------------------------------------------------------------------------------
Class : E
Cause : Bugfix for #833
Detail: Preprocessor switches added for Pin packages.
-------------------------------------------------------------------------------
*/
/******************************************************************************/

/* to avoid Multiple inclusion */
#ifndef _PORT_CFG_H
#define _PORT_CFG_H

/*...................................Macros..................................*/

/*...................Configuration Description File...........................*/

/* Preprocessor switch for enabling the development error detection */
#define PORT_DEV_ERROR_DETECT                (OFF)

#define PORT_MAX_NUMBER_OF_CONFIG            (1)

#define PORT_config_0		0

/* IRQC - Interrupt Control Register Configuration 
   Bit 7: IRQE : IRQ Select Edge Sensitive Only 
          0-IRQ configured for low level recognition.
          1 IRQ configured to respond only to falling edges.
          
   Bit 6: IRQEN : External IRQ Enable
          0 External IRQ pin is disconnected from interrupt logic.
          1 External IRQ pin is connected to interrupt logic. */

#define IRQC                            ((uint8)0x0)

/* Symbolic Pin Names from the Configuration tool */

#define  LED_U12S_EN_DIR		(PORT_A_PIN_0)
#define  RL_LED_SW_DETECT_DIR		(PORT_A_PIN_1)
#define  RR_LED_SW_DETECT_DIR		(PORT_A_PIN_2)
#define  CAN_STB_DIR		(PORT_A_PIN_3)
#define  U12T_EN_DIR		(PORT_A_PIN_4)
#define  REL_A_MON_DIR		(PORT_A_PIN_5)
#define  IGN_WHEEL_MON_DIR		(PORT_A_PIN_6)
#define  REL_B_MON_DIR		(PORT_A_PIN_7)


#define  MUX_A0_DIR		(PORT_B_PIN_0)
#define  MUX_A1_DIR		(PORT_B_PIN_1)
#define  MUX_A2_DIR		(PORT_B_PIN_2)
#define  SYMBOLIC_PORT_B_PIN_3_DIR		(PORT_B_PIN_3)
#define  SYMBOLIC_PORT_B_PIN_4_DIR		(PORT_B_PIN_4)
#define  SYMBOLIC_PORT_B_PIN_5_DIR		(PORT_B_PIN_5)
#define  SYMBOLIC_PORT_B_PIN_6_DIR		(PORT_B_PIN_6)
#define  SYMBOLIC_PORT_B_PIN_7_DIR		(PORT_B_PIN_7)


#define  SYMBOLIC_PORT_E_PIN_0_DIR		(PORT_E_PIN_0)
#define  SYMBOLIC_PORT_E_PIN_1_DIR		(PORT_E_PIN_1)
#define  IGN_REAR_MON_DIR		(PORT_E_PIN_2)
#define  REL_STW_EN_DIR		(PORT_E_PIN_3)
#define  VAR_BIT2_DIR		(PORT_E_PIN_4)
#define  VAR_BIT1_DIR		(PORT_E_PIN_5)
#define  VAR_BIT0_DIR		(PORT_E_PIN_6)
#define  SYMBOLIC_PORT_E_PIN_7_DIR		(PORT_E_PIN_7)


#define  SYMBOLIC_PORT_J_PIN_6_DIR		(PORT_J_PIN_6)
#define  SYMBOLIC_PORT_J_PIN_7_DIR		(PORT_J_PIN_7)


#define  CAN_RX_DIR		(PORT_M_PIN_0)
#define  CAN_TX_DIR		(PORT_M_PIN_1)
#define  MISO_DIR		(PORT_M_PIN_2)
#define  SSO_DIR		(PORT_M_PIN_3)
#define  TSLICE_PIN_DIR		(PORT_M_PIN_4)
#define  SCLK_DIR		(PORT_M_PIN_5)


#define  LED1_RR_EN_DIR		(PORT_P_PIN_0)
#define  PWM_VS_FR_DIR		(PORT_P_PIN_1)
#define  PWM_VS_FL_DIR		(PORT_P_PIN_2)
#define  PWM_BASE_REL_DIR		(PORT_P_PIN_3)
#define  LED2_RL_EN_DIR		(PORT_P_PIN_4)
#define  LED1_RL_EN_DIR		(PORT_P_PIN_5)
#define  LED2_RR_EN_DIR		(PORT_P_PIN_7)


#define  REL_B_EN_DIR		(PORT_S_PIN_0)
#define  REL_A_EN_DIR		(PORT_S_PIN_1)
#define  VENT_EN_DIR		(PORT_S_PIN_2)
#define  _DIR		(PORT_S_PIN_3)


#define  SYMBOLIC_PORT_T_PIN_0_DIR		(PORT_T_PIN_0)
#define  SYMBOLIC_PORT_T_PIN_1_DIR		(PORT_T_PIN_1)
#define  SYMBOLIC_PORT_T_PIN_2_DIR		(PORT_T_PIN_2)
#define  PWM_HS_FL_DIR		(PORT_T_PIN_3)
#define  PWM_HS_FR_DIR		(PORT_T_PIN_4)
#define  PWM_HS_RL_DIR		(PORT_T_PIN_5)
#define  PWM_HS_RR_DIR		(PORT_T_PIN_6)
#define  PWM_STW_DIR		(PORT_T_PIN_7)


#define  VAD_U12T_DIR		(PORT_A2D0L_PIN_0)
#define  VAD_ISENSE_HS_RL_DIR		(PORT_A2D0L_PIN_1)
#define  VAD_ISENSE_HS_RR_DIR		(PORT_A2D0L_PIN_2)
#define  VAD_ISENSE_HS_FL_DIR		(PORT_A2D0L_PIN_3)
#define  VAD_ISENSE_HS_FR_DIR		(PORT_A2D0L_PIN_4)
#define  VAD_MUX2_OUT_DIR		(PORT_A2D0L_PIN_5)
#define  VAD_MUX1_OUT_DIR		(PORT_A2D0L_PIN_6)
#define  VAD_UINT_DIR		(PORT_A2D0L_PIN_7)



/* Port Mode */

#define PORT_MODE_PIN_PA0                    (DIO)
#define PORT_MODE_PIN_PA1                    (DIO)
#define PORT_MODE_PIN_PA2                    (DIO)
#define PORT_MODE_PIN_PA3                    (DIO)
#define PORT_MODE_PIN_PA4                    (DIO)
#define PORT_MODE_PIN_PA5                    (DIO)
#define PORT_MODE_PIN_PA6                    (DIO)
#define PORT_MODE_PIN_PA7                    (DIO)
                                      
#define PORT_MODE_PIN_PB0                    (DIO)
#define PORT_MODE_PIN_PB1                    (DIO)
#define PORT_MODE_PIN_PB2                    (DIO)
#define PORT_MODE_PIN_PB3                    (UNUSED)
#define PORT_MODE_PIN_PB4                    (UNUSED)
#define PORT_MODE_PIN_PB5                    (UNUSED)
#define PORT_MODE_PIN_PB6                    (UNUSED)
#define PORT_MODE_PIN_PB7                    (UNUSED)
                                      
#define PORT_MODE_PIN_PC0                    (NOT_AVAILABLE)
#define PORT_MODE_PIN_PC1                    (NOT_AVAILABLE)
#define PORT_MODE_PIN_PC2                    (NOT_AVAILABLE)
#define PORT_MODE_PIN_PC3                    (NOT_AVAILABLE)
#define PORT_MODE_PIN_PC4                    (NOT_AVAILABLE)
#define PORT_MODE_PIN_PC5                    (NOT_AVAILABLE)
#define PORT_MODE_PIN_PC6                    (NOT_AVAILABLE)
#define PORT_MODE_PIN_PC7                    (NOT_AVAILABLE)
                                      
#define PORT_MODE_PIN_PD0                    (NOT_AVAILABLE)
#define PORT_MODE_PIN_PD1                    (NOT_AVAILABLE)
#define PORT_MODE_PIN_PD2                    (NOT_AVAILABLE)
#define PORT_MODE_PIN_PD3                    (NOT_AVAILABLE)
#define PORT_MODE_PIN_PD4                    (NOT_AVAILABLE)
#define PORT_MODE_PIN_PD5                    (NOT_AVAILABLE)
#define PORT_MODE_PIN_PD6                    (NOT_AVAILABLE)
#define PORT_MODE_PIN_PD7                    (NOT_AVAILABLE)
                                      
#define PORT_MODE_PIN_PE0                    (UNUSED)
#define PORT_MODE_PIN_PE1                    (UNUSED)
#define PORT_MODE_PIN_PE2                    (DIO)
#define PORT_MODE_PIN_PE3                    (DIO)
#define PORT_MODE_PIN_PE4                    (DIO)
#define PORT_MODE_PIN_PE5                    (DIO)
#define PORT_MODE_PIN_PE6                    (DIO)
#define PORT_MODE_PIN_PE7                    (UNUSED)
                                      
#define PORT_MODE_PIN_PK0                    (NOT_AVAILABLE)
#define PORT_MODE_PIN_PK1                    (NOT_AVAILABLE)
#define PORT_MODE_PIN_PK2                    (NOT_AVAILABLE)
#define PORT_MODE_PIN_PK3                    (NOT_AVAILABLE)
#define PORT_MODE_PIN_PK4                    (NOT_AVAILABLE)
#define PORT_MODE_PIN_PK5                    (NOT_AVAILABLE)
#define PORT_MODE_PIN_PK6                    (NOT_AVAILABLE)
#define PORT_MODE_PIN_PK7                    (NOT_AVAILABLE)
                                      
#define PORT_MODE_PIN_PT0                    (UNUSED)
#define PORT_MODE_PIN_PT1                    (UNUSED)
#define PORT_MODE_PIN_PT2                    (UNUSED)
#define PORT_MODE_PIN_PT3                    (PWM_TIM)
#define PORT_MODE_PIN_PT4                    (PWM_TIM)
#define PORT_MODE_PIN_PT5                    (PWM_TIM)
#define PORT_MODE_PIN_PT6                    (PWM_TIM)
#define PORT_MODE_PIN_PT7                    (PWM_TIM)
                                      
#define PORT_MODE_PIN_PS0                    (DIO)
#define PORT_MODE_PIN_PS1                    (DIO)
#define PORT_MODE_PIN_PS2                    (DIO)
#define PORT_MODE_PIN_PS3                    (UNUSED)
#define PORT_MODE_PIN_PS4                    (NOT_AVAILABLE)
#define PORT_MODE_PIN_PS5                    (NOT_AVAILABLE)
#define PORT_MODE_PIN_PS6                    (NOT_AVAILABLE)
#define PORT_MODE_PIN_PS7                    (NOT_AVAILABLE)
                                      
#define PORT_MODE_PIN_PM0                    (DIO)
#define PORT_MODE_PIN_PM1                    (DIO)
#define PORT_MODE_PIN_PM2                    (UNUSED)
#define PORT_MODE_PIN_PM3                    (UNUSED)
#define PORT_MODE_PIN_PM4                    (DIO)
#define PORT_MODE_PIN_PM5                    (UNUSED)
#define PORT_MODE_PIN_PM6                    (NOT_AVAILABLE)
#define PORT_MODE_PIN_PM7                    (NOT_AVAILABLE)
                                      
#define PORT_MODE_PIN_PP0                    (DIO)
#define PORT_MODE_PIN_PP1                    (PWM)
#define PORT_MODE_PIN_PP2                    (PWM)
#define PORT_MODE_PIN_PP3                    (PWM)
#define PORT_MODE_PIN_PP4                    (DIO)
#define PORT_MODE_PIN_PP5                    (DIO)
#define PORT_MODE_PIN_PP6                    (NOT_AVAILABLE)
#define PORT_MODE_PIN_PP7                    (DIO)
                                      
#define PORT_MODE_PIN_PH0                    (NOT_AVAILABLE)
#define PORT_MODE_PIN_PH1                    (NOT_AVAILABLE)
#define PORT_MODE_PIN_PH2                    (NOT_AVAILABLE)
#define PORT_MODE_PIN_PH3                    (NOT_AVAILABLE)
#define PORT_MODE_PIN_PH4                    (NOT_AVAILABLE)
#define PORT_MODE_PIN_PH5                    (NOT_AVAILABLE)
#define PORT_MODE_PIN_PH6                    (NOT_AVAILABLE)
#define PORT_MODE_PIN_PH7                    (NOT_AVAILABLE)
                                      
#define PORT_MODE_PIN_PJ0                    (NOT_AVAILABLE)
#define PORT_MODE_PIN_PJ1                    (NOT_AVAILABLE)
#define PORT_MODE_PIN_PJ2                    (NOT_AVAILABLE)
#define PORT_MODE_PIN_PJ3                    (NOT_AVAILABLE)
#define PORT_MODE_PIN_PJ4                    (NOT_AVAILABLE)
#define PORT_MODE_PIN_PJ5                    (NOT_AVAILABLE)
#define PORT_MODE_PIN_PJ6                    (UNUSED)
#define PORT_MODE_PIN_PJ7                    (UNUSED)
                                      
#define PORT_MODE_PIN_PAD00                  (ADC)
#define PORT_MODE_PIN_PAD01                  (ADC)
#define PORT_MODE_PIN_PAD02                  (ADC)
#define PORT_MODE_PIN_PAD03                  (ADC)
#define PORT_MODE_PIN_PAD04                  (ADC)
#define PORT_MODE_PIN_PAD05                  (ADC)
#define PORT_MODE_PIN_PAD06                  (ADC)
#define PORT_MODE_PIN_PAD07                  (ADC)
                                      
#define PORT_MODE_PIN_PAD08                  (NOT_AVAILABLE)
#define PORT_MODE_PIN_PAD09                  (NOT_AVAILABLE)
#define PORT_MODE_PIN_PAD10                  (NOT_AVAILABLE)
#define PORT_MODE_PIN_PAD11                  (NOT_AVAILABLE)
#define PORT_MODE_PIN_PAD12                  (NOT_AVAILABLE)
#define PORT_MODE_PIN_PAD13                  (NOT_AVAILABLE)
#define PORT_MODE_PIN_PAD14                  (NOT_AVAILABLE)
#define PORT_MODE_PIN_PAD15                  (NOT_AVAILABLE)
                                      
#define PORT_MODE_PIN_PAD16                  (NOT_AVAILABLE)
#define PORT_MODE_PIN_PAD17                  (NOT_AVAILABLE)
#define PORT_MODE_PIN_PAD18                  (NOT_AVAILABLE)
#define PORT_MODE_PIN_PAD19                  (NOT_AVAILABLE)
#define PORT_MODE_PIN_PAD20                  (NOT_AVAILABLE)
#define PORT_MODE_PIN_PAD21                  (NOT_AVAILABLE)
#define PORT_MODE_PIN_PAD22                  (NOT_AVAILABLE)
#define PORT_MODE_PIN_PAD23                  (NOT_AVAILABLE)


/* Configuration Values Set 1*/

#define PORT_LEVEL_VALUE_PORT_A_1              ((uint8)0x6)
#define PORT_DIRECTION_PORT_A_1                ((uint8)0x19)
#define PORT_DIRECTION_CHANGEABLE_PORT_A_1     ((uint8)0x0)

#define PORT_LEVEL_VALUE_PORT_B_1              ((uint8)0x0)
#define PORT_DIRECTION_PORT_B_1                ((uint8)0x7)
#define PORT_DIRECTION_CHANGEABLE_PORT_B_1     ((uint8)0x0)

#define PORT_LEVEL_VALUE_PORT_E_1              ((uint8)0x8)
#define PORT_DIRECTION_PORT_E_1                ((uint8)0x8)
#define PORT_DIRECTION_CHANGEABLE_PORT_E_1     ((uint8)0x0)

#define PORT_LEVEL_VALUE_PORT_T_1              ((uint8)0x0)
#define PORT_DIRECTION_PORT_T_1                ((uint8)0xF8)
#define PORT_REDUCED_DRIVE_PORT_T_1            ((uint8)0x0)
#define PORT_PULL_ENABLE_PORT_T_1              ((uint8)0x0)
#define PORT_POLARITY_SELECT_PORT_T_1          ((uint8)0x0)
#define PORT_DIRECTION_CHANGEABLE_PORT_T_1     ((uint8)0x0)

#define PORT_LEVEL_VALUE_PORT_S_1              ((uint8)0x3)
#define PORT_DIRECTION_PORT_S_1                ((uint8)0xF7)
#define PORT_REDUCED_DRIVE_PORT_S_1            ((uint8)0x0)
#define PORT_PULL_ENABLE_PORT_S_1              ((uint8)0xFF)
#define PORT_POLARITY_SELECT_PORT_S_1          ((uint8)0x0)
#define PORT_WIRED_OR_PORT_S_1                 ((uint8)0x0)
#define PORT_DIRECTION_CHANGEABLE_PORT_S_1     ((uint8)0x0)

#define PORT_LEVEL_VALUE_PORT_M_1              ((uint8)0x0)
#define PORT_DIRECTION_PORT_M_1                ((uint8)0xD2)
#define PORT_REDUCED_DRIVE_PORT_M_1            ((uint8)0x0)
#define PORT_PULL_ENABLE_PORT_M_1             ((uint8)0x0)
#define PORT_POLARITY_SELECT_PORT_M_1          ((uint8)0x0)
#define PORT_WIRED_OR_PORT_M_1                 ((uint8)0x0)
#define PORT_DIRECTION_CHANGEABLE_PORT_M_1     ((uint8)0x0)

#define PORT_LEVEL_VALUE_PORT_P_1              ((uint8)0x0)
#define PORT_DIRECTION_PORT_P_1                ((uint8)0xFF)
#define PORT_REDUCED_DRIVE_PORT_P_1            ((uint8)0x0)
#define PORT_PULL_ENABLE_PORT_P_1              ((uint8)0x0)
#define PORT_POLARITY_SELECT_PORT_P_1          ((uint8)0x0)
#define PORT_DIRECTION_CHANGEABLE_PORT_P_1     ((uint8)0x0)

#define PORT_LEVEL_VALUE_PORT_J_1              ((uint8)0x0)
#define PORT_DIRECTION_PORT_J_1                ((uint8)0x3F)
#define PORT_REDUCED_DRIVE_PORT_J_1            ((uint8)0x0)
#define PORT_PULL_ENABLE_PORT_J_1              ((uint8)0xF7)

#define PORT_POLARITY_SELECT_PORT_J_1          ((uint8)0x0)
#define PORT_DIRECTION_CHANGEABLE_PORT_J_1     ((uint8)0x0)

#define PORT_LEVEL_VALUE_PORT_A2D0L_1           ((uint8)0x0)
#define PORT_DIRECTION_PORT_A2D0L_1             ((uint8)0x0)
#define PORT_REDUCED_DRIVE_PORT_A2D0L_1         ((uint8)0x0)
#define PORT_PULL_ENABLE_PORT_A2D0L_1           ((uint8)0x0)
#define PORT_DIGITAL_INPUT_ENABLE_PORT_A2D0L_1  ((uint8)0xFF)
#define PORT_DIRECTION_CHANGEABLE_PORT_A2D0L_1  ((uint8)0x0)

                                      
#define PORT_PUCR_1                            ((uint8)0x80)
#define PORT_RDRIV_1                           ((uint8)0x0)
#define PORT_ECLKCTL_1                         ((uint8)0xC1)
#define PORT_MODRR_1                           ((uint8)0x0)
#define PORT_PTTRR_1                           ((uint8)0x0)


#endif  /* end of #ifndef _PORT_CFG_H */ 
/*.............................END OF Port_Cfg.h..............................*/