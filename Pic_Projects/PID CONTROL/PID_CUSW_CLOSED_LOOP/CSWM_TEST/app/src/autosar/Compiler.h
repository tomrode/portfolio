/* Created at 15:25 on 1-4-2009 */
/* Version of HALCoGen used : 2.9.3 */
/* Code generated for       : MC9S12XS128 */
/*******************************************************************************
**                                                                            **
**  SRC-MODULE: Compiler.h                                                    **
**                                                                            **
**  Copyright (C) none (open standard)                                        **
**                                                                            **
**  TARGET    : ALL                                                           **
**                                                                            **
**  COMPILER  : Code Warrior                                                  **
**                                                                            **
**  PROJECT   : SC6                                                           **
**                                                                            **
**  AUTHOR    : Robert Feist                                                  **
**                                                                            **
**  PURPOSE   : Provider of compiler specific (non-ANSI) keywords, types      **
**              intrinsic's and so on. All mappings of keywords which are     **
**              not standardized and/or compiler specific shall be placed     **
**              and organized in this compiler specific header. The file is   **
**              needed to decouple the code of all standard core components   **
**              from compiler topics and thus enhance portability.            **
**                                                                            **
**  REMARKS   : Version control and support provided by BMW Group             **
**                                                                            **
**              Requirement fulfilment:                                       **
**                  [BSW00361] Compiler specific language extension header    **
**              Requirement dependency:                                       **
**                  [BSW00306] Avoid compiler and platform specific keywords  **
**                  [BSW00348] Standard type header                           **
**                                                                            **
**  PLATFORM DEPENDANT [yes/no]: yes                                          **
**                                                                            **
**  TO BE CHANGED BY USER [yes/no]: no                                        **
**                                                                            **
*******************************************************************************/

/*******************************************************************************
**                      Author Identity                                       **
********************************************************************************
**
** Initials     Name                       Company
** --------     -------------------------  ------------------------------------
** fer          Robert Feist               BMW Group
** cma          Christoph Mueller-Albrecht BMW Group
**
*******************************************************************************/

/*******************************************************************************
**                      Revision Control History                              **
*******************************************************************************/

/*
 * V2.0.0:  10.03.2005, fer  : defines ROM_MEM, _STATIC_ and NULL_PTR added
 *                             block comments added (**), released
*/

#ifndef COMPILER_H
#define COMPILER_H

/*******************************************************************************
**                      Include Section                                       **
*******************************************************************************/

#include "Compiler_Cfg.h"

/*******************************************************************************
**                      Global Symbols                                        **
*******************************************************************************/
/*
  File version information
*/
#define COMPILER_MAJOR_VERSION  2
#define COMPILER_MINOR_VERSION  0
#define COMPILER_PATCH_VERSION  0

/*******************************************************************************
**                      Global Data Types                                     **
*******************************************************************************/
#define ROM_MEM   const
#define _STATIC_  static

#ifndef NULL_PTR
 #define NULL_PTR  ((void *)0)
#endif

/*******************************************************************************
**                      Global Data                                           **
*******************************************************************************/


/*******************************************************************************
**                      Global Function Prototypes                            **
*******************************************************************************/


#endif /* COMPILER_H */
