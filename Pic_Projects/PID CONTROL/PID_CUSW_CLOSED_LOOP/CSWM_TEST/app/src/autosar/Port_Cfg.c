/* Created at 15:25 on 1-4-2009 */
/* Version of HALCoGen used : 2.9.3 */
/* Code generated for       : MC9S12XS128 */
/******************************************************************************/
/* Copyright(c) - This Program/Software is the exclusive property of Infosys. 
   The software may not be reproduced or given to third parties without prior 
   consent of Infosys.
                     
   Filename                      : Port_Cfg.c
   Version                       : 2.0.3
   Microcontroller Platform      : MC9S12XS128
   Compiler                      : Codewarrior HCS12X V5.0.28

   Description                   : This file contains the initialisation of 
                                   configuration parameters.
                                   

   Requirement Specification     : AUTOSAR_SWS_Port_Driver_working.doc ver1.1.3
                
   Module Design                 : AUTOTMAL_DDD_PORT_SPAL2.0.doc version 1.0

   Platform Dependant[yes/no]    : yes   
     
   To be changed by user[yes/no] : no  
*/
/******************************************************************************/
/* Revision History

Class of Change
-------------------------------------------------------------------------------
N - New Module.
F - Functional development.
E - Error Correction.
O - Optimization without functional change.
-------------------------------------------------------------------------------
 00        03-Feb-2006     Vijayakumar K, Infosys
-------------------------------------------------------------------------------
Class : N
Cause : New Module
Detail: This file implements configuration for the PORT driver.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 01        03-Mar-2006     Muthuvelavan Natarajan, Infosys
-------------------------------------------------------------------------------
Class : F
Cause : Functional development
Detail: Update for SPAL2.0.
-------------------------------------------------------------------------------
 02        09-Jun-2006     Vijayakumar K, Infosys
-------------------------------------------------------------------------------
Class : O
Cause : Support multiple configuration structures
Detail: Symbolic pin name & pin functionality are moved out of config structure
-------------------------------------------------------------------------------
 03        27-Jun-2006     Vijayakumar K, Infosys
-------------------------------------------------------------------------------
Class : O
Cause : Macros/Inline for speed optimization
Detail: Port_SetPinDirection is configurable as Macro
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 04        31-Jan-2007     Kasinath, Infosys
-------------------------------------------------------------------------------
Class : F
Cause : A2D0 & A2D1 Channel Changes
Detail: Port A2D0 modified to handle 16 channels and port A2D1 8 channels.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 05        11-Apr-2007     Kasinath, Infosys
-------------------------------------------------------------------------------
Class : E
Cause : Fix for issue 716
Detail: Pins not used in lower pin packages also initialized.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 06        05-Oct-2007     Ajaykumar, Infosys
-------------------------------------------------------------------------------
Class : F
Cause : MC9S12XS128 controller support
Detail: MC9S12XS128 related pre-processor switches added.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 07        16-Nov-2007     Ajaykumar, Infosys
-------------------------------------------------------------------------------
Class : E
Cause : Bugfix for #833
Detail: Preprocessor switches added for Pin packages.
-------------------------------------------------------------------------------
*/
/******************************************************************************/


/* Module Header file */
#include "Port.h"

/* Initialisation of Configuration structure for PORT */ 

/* Initialisation of Config */
const Port_ConfigType Port_Config[PORT_MAX_NUMBER_OF_CONFIG] = 

{
    {
        /* Configuration structure for Port A */
        { 
            PORT_LEVEL_VALUE_PORT_A_1
        },
        
        /* Configuration structure for Port B */
        { 
            PORT_LEVEL_VALUE_PORT_B_1
        },
        /* Configuration structure for Port E */
        { 
            PORT_LEVEL_VALUE_PORT_E_1
        },
        /* Configuration structure for Port T */
        { 
            PORT_LEVEL_VALUE_PORT_T_1,
            PORT_REDUCED_DRIVE_PORT_T_1,
            PORT_PULL_ENABLE_PORT_T_1,
            PORT_POLARITY_SELECT_PORT_T_1
        },
        
        /* Configuration structure for Port S */
        { 
            PORT_LEVEL_VALUE_PORT_S_1,
            PORT_REDUCED_DRIVE_PORT_S_1,
            PORT_PULL_ENABLE_PORT_S_1,
            PORT_POLARITY_SELECT_PORT_S_1,
            PORT_WIRED_OR_PORT_S_1
        },
        
        /* Configuration structure for Port M */
        { 
            PORT_LEVEL_VALUE_PORT_M_1,
            PORT_REDUCED_DRIVE_PORT_M_1,
            PORT_PULL_ENABLE_PORT_M_1,
            PORT_POLARITY_SELECT_PORT_M_1,
            PORT_WIRED_OR_PORT_M_1
        },
        
        /* Configuration structure for Port P */
        { 
            PORT_LEVEL_VALUE_PORT_P_1,
            PORT_REDUCED_DRIVE_PORT_P_1,
            PORT_PULL_ENABLE_PORT_P_1,
            PORT_POLARITY_SELECT_PORT_P_1
        },
        /* Configuration structure for Port J */
        { 
            PORT_LEVEL_VALUE_PORT_J_1,
            PORT_REDUCED_DRIVE_PORT_J_1,
            PORT_PULL_ENABLE_PORT_J_1,
            PORT_POLARITY_SELECT_PORT_J_1
        },
        
        /* Configuration structure for Port AD0L */
        { 
            PORT_LEVEL_VALUE_PORT_A2D0L_1,
            PORT_REDUCED_DRIVE_PORT_A2D0L_1,
            PORT_PULL_ENABLE_PORT_A2D0L_1,
            PORT_DIGITAL_INPUT_ENABLE_PORT_A2D0L_1
        },
        /* Direction and DirChangeable for all the ports */
        {
            /* PORT_A */
            {
                PORT_DIRECTION_PORT_A_1,
                PORT_DIRECTION_CHANGEABLE_PORT_A_1
            },
            {
                PORT_DIRECTION_PORT_B_1,
                PORT_DIRECTION_CHANGEABLE_PORT_B_1
            },
            {
                PORT_DIRECTION_PORT_E_1,
                PORT_DIRECTION_CHANGEABLE_PORT_E_1
            },
            {
                PORT_DIRECTION_PORT_T_1,
                PORT_DIRECTION_CHANGEABLE_PORT_T_1
            },
            {
                PORT_DIRECTION_PORT_S_1,
                PORT_DIRECTION_CHANGEABLE_PORT_S_1
            },
            {
                PORT_DIRECTION_PORT_M_1,
                PORT_DIRECTION_CHANGEABLE_PORT_M_1
            },
            {
                PORT_DIRECTION_PORT_P_1,
                PORT_DIRECTION_CHANGEABLE_PORT_P_1
            },
            {
                PORT_DIRECTION_PORT_J_1,
                PORT_DIRECTION_CHANGEABLE_PORT_J_1
            },
            {
                PORT_DIRECTION_PORT_A2D0L_1,
                PORT_DIRECTION_CHANGEABLE_PORT_A2D0L_1
            },
        },
        /* Miscellaneous configuration structure */
        {
            PORT_PUCR_1,
            PORT_RDRIV_1,
            PORT_ECLKCTL_1,
            PORT_MODRR_1,
            PORT_PTTRR_1
        }
    }
};

/*.............................END OF Port_Cfg.c..............................*/
