/* Created at 15:24 on 1-4-2009 */
/* Version of HALCoGen used : 2.9.3 */
/* Code generated for       : MC9S12XS128 */
/*************************************************************************/
/* Copyright(c) - This Program/Software is the exclusive property of Infosys.
The software may not be reproduced or given to third parties without prior
consent of Infosys.

     Filename                      : Adc.h
     Version                       : 5.0.6
     Microcontroller Platform      : MC9S12XS128
     Compiler                      : Codewarrior HCS12X V5.0.28

     Description                   : This file contains all function
                                    declarations of all functions of ADC Driver
                                     Module

     Requirement Specification     : AUTOSAR_SWS_ADC_Driver.pdf version 1.0.16

     Module Design                 : AUTOSAR_DDD_ADC_SPAL2.0.doc version 2.11

     Platform Dependant[yes/no]    : yes

     To be changed by user[yes/no] : no
*/

/*****************************************************************************/

/* Revision History

Class of Change
------------------------------------------------------------------------------
N - New Module.
F - Functional development.
E - Error Correction.
O - Optimization without functional change.
------------------------------------------------------------------------------
 00     06-Jan-2006     Kapil Kumar, Infosys
------------------------------------------------------------------------------
Class: N
Cause: New Module
Detail:This file contains the published interfaces of ADC driver.
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
 01     30-Mar-2006     Kapil Kumar, Infosys
-------------------------------------------------------------------------------
Class: F
Cause: Changes for SPAL2.0
Detail:Changes done as per AUTOSAR_SWS_AdcDriver.pdf ver 1.0.16
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 02     14-Jun-2006     Karthik, Infosys
-------------------------------------------------------------------------------
Class:  E,O
Cause:  Changes done for Anomalies reported in RTRT
Detail: The changes are done as per RTRT anomaly list
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 03     5-July-2006     Karthik, Infosys
-------------------------------------------------------------------------------
Class:  E,O
Cause:  File version information macro renaming
Detail: ADC_SW_MAJOR_VERSION_H and related macros is renamed
        as ADC_SW_MAJOR_VERSION etc.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 04     04-Aug-2006     Karthik, Infosys
-------------------------------------------------------------------------------
Class:  E
Cause:  Version number change
Detail: Version changed to match with the version of Adc.c
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
 05     11-Aug-2006     Karthik, Infosys
-------------------------------------------------------------------------------
Class:  E
Cause:  Version number change
Detail: Version changed to match with the version of Adc.c
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 06     21-Aug-2006     Karthik, Infosys
-------------------------------------------------------------------------------
Class:  E
Cause:  Lint Analysis 
Detail: 
-------------------------------------------------------------------------------
 07     08-Dec-2006     Kasinath Hegde  , Infosys
-------------------------------------------------------------------------------
Class:  F
Cause:  Software changes required for S12XEP100 
Detail: Types added for S12XEP100
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 08     23-Jan-2007     Kasinath Hegde, Infosys
-------------------------------------------------------------------------------
Class:  F
Cause:  ADC HW Unit Identifier change
Detail: Names of ADC HW Units 0 & 1 (ATD0 & ATD1) changed to ADC_HW_0 and
        ADC_HW_1
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 09     23-Mar-2007     Rajesh, Infosys
-------------------------------------------------------------------------------
Class: E
Cause: Bug Fixes - 530, 565, 676, 552, 682, 683, 686
Detail: Only configured HW units are initialized in Adc_Init.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 10     03-May-2007     Rajesh, Infosys
-------------------------------------------------------------------------------
Class: E
Cause: 
Detail:Updated in Sync with Adc.c
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 11     18-Oct-2007     Ajaykumar,  Infosys
-------------------------------------------------------------------------------
Class:  E
Cause:  Source file updated
Detail: Version number changed
-------------------------------------------------------------------------------
 12     31-Dec-2007     Ajaykumar,  Infosys
-------------------------------------------------------------------------------
Class:  O
Cause:  PRL issue #877
Detail: Preprocessor switches added for code optimization for PRL#877.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 13     31-Mar-2008     Ajaykumar,  Infosys
-------------------------------------------------------------------------------
Class:  E
Cause:  PRL issue 978
Detail: Updated in sync with Adc.c
-------------------------------------------------------------------------------

******************************************************************************/

#ifndef _ADC_H
#define _ADC_H


/*****************************************************************************
**                      Include Section                                      *
*****************************************************************************/

#include "Std_Types.h"
/* Infosys defines header file */
#include "MCAL_Types.h"

/************local defines */

/* Pre-compile/static configuration parameters for ADC*/
#include "Adc_Cfg.h"


/*****************************************************************************
**                      Global Symbols                                      **
*****************************************************************************/

/*
  Published paramters
*/

/* File version information */
#define ADC_SW_MAJOR_VERSION                         5
#define ADC_SW_MINOR_VERSION                         0
#define ADC_SW_PATCH_VERSION                         6 
#define ADC_AR_MAJOR_VERSION                         1
#define ADC_AR_MINOR_VERSION                         0
#define ADC_AR_PATCH_VERSION                         16

/* ADC module id 123 -> 0x7B */ 

#define ADC_MODULE_ID                          ((uint8)0x7B)
#define ADC_VENDOR_ID                          ((uint16)40)


/* Declaring the service ID macros of the MCU Driver service APIs */
#define ADC_INIT_ID                     ((uint8)0x00U)
#define ADC_DEINIT_ID                   ((uint8)0x01U)
#define ADC_STARTGROUPCONVERSION_ID     ((uint8)0x02U)
#define ADC_STOPGROUPCONVERSION_ID      ((uint8)0x03U)
#define ADC_SINGLEVALUEREADCHANNEL_ID   ((uint8)0x04U)
#define ADC_ENABLEHARDWARETRIGGER_ID    ((uint8)0x05U)
#define ADC_DISABLEHARDWARETRIGGER_ID   ((uint8)0x06U)
#define ADC_ENABLEGROUPNOTIFICATION_ID  ((uint8)0x07U)
#define ADC_DISABLEGROUPNOTIFICATION_ID ((uint8)0x08U)
#define ADC_GATEDSETUP_ID               ((uint8)0x09U)
#define ADC_STARTONDEMANDCONVERSION_ID  ((uint8)0x0AU)
#define ADC_ONDEMANDREADCHANNEL_ID      ((uint8)0x0BU)
#define ADC_STARTGATEDCONVERSION_ID     ((uint8)0x0CU)
#define ADC_GETGATEDLASTPOINTER_ID      ((uint8)0x0DU)
#define ADC_GETGROUPSTATUS_ID           ((uint8)0x0EU)
#define ADC_GETVERSIONINFO_ID           ((uint8)0x0FU)

/*Published Parameters*/
/* 10 bits resolution*/
/* #define ADC_MAX_CHANNEL_RESOLUTION ((uint8)10) */
/* ADC_CHANNEL_VALUESIGNED - Unsigned */
/*#define ADC_CHANNEL_VALUESIGNED    ((uint8)0) */
/* ADC OnDemand result */
/* #define ADC_CHANNEL_RESULT         ((uint8)0) */
#define ADC_STOP_IMMEDIATE            1U
#define ADC_STOP_AFTER_CURRENT        0U
#define ADC_TYPE_STOP_CONV            ADC_STOP_IMMEDIATE
/*#define ADC_TYPE_SCAN_RESTART      ((uint8)0) */
/* HW Priority not supported */
/*#define ADC_HW_PRIORITY            ((uint8)0)*/
/*
  Development error values
*/
/* service is calld without Adc Driver intialization */
#define ADC_E_UNINIT                   ((uint8)0x0AU)

/* Service is called while conversion is on going */
#define ADC_E_BUSY                     ((uint8)0x0BU)

/* Service is called with invalid parameter channel  */
#define ADC_E_PARAM_CHANNEL            ((uint8)0x14U)

/* Service is called with invalid parameter group  */
#define ADC_E_PARAM_GROUP              ((uint8)0x15U)

/* service is called for a group with wrong conversion mode */
#define ADC_E_WRONG_CONV_MODE          ((uint8)0x16U)

/* service is called for a group with wrong trigger source */
#define ADC_E_WRONG_TRIGG_SRC          ((uint8)0x17U)


/* Service is called when global notification capability is disabled or
   Service is called for a group where notification function is not
   configured */
#define ADC_E_NOTIF_CAPABILITY         ((uint8)0x18U)

/* Service is called when global hardware trigger capability is disabled */
#define ADC_E_HW_TRIGGER_CAPABILITY    ((uint8)0x19U)

/* Service is called with a NULL configuration parameter */
#define ADC_E_PARAM_CONFIG             ((uint8)0x1EU)

/*Service is called when the trigger source is busy*/
#define ADC_E_TRIGG_SRC_BUSY           ((uint8)0x29U)

/*Service is called when gated group is not initialised */
#define ADC_E_GATED_UNINIT             ((uint8)0x0CU)

/****************Used Macros in the code ************************/
/* Value of NO_OF_HW_UNITS based on number of HW units in
   pin package selected ( value 1 or 2 only)*/
#define NO_OF_HW_UNITS          ((uint8)1U)

#define OUT_OF_RANGE            (0xFFFFU)

/* Value of ADC_MAX_NO_OF_CHANNELS based on number of channels in
   pin package selected ( value one of 8/16/24/32)*/
#define ADC_MAX_NO_OF_CHANNELS  ((uint8)8U)


/******************End Used Macros in the code *****************/

/****************************************************************************
**                      Global Data Types                                  **
*****************************************************************************/

/* Numeric ID of an ADC channel */
typedef uint8  Adc_ChannelType;

/* Numeric ID for configured ADC channel groups */
typedef uint8  Adc_GroupType;

/* Return value for ADC conversion result */
typedef uint16 Adc_ValueType;

/*Result buffer type for gated groups*/
typedef uint16 Adc_PointerBufferType;

/*Pointer type to the result buffer for gated conversion*/
typedef uint16* Adc_Pointer;

/*Type for Discharge before sampling */
 typedef void (*Adc_DischargeType) (void);    

/*****************************************************************************
**                      runtime configurable Parameters                      **
******************************************************************************/


/* prescale factor */
typedef uint8 Adc_PrescaleType;     /* default =5. else 0-31*/

/*Adc group trigger sources */
typedef enum {
               ADC_TRIGG_SRC_SW_API=0,
               ADC_TRIGG_SRC_HW_EXT
             }Adc_Group_Trgsrc_OneshotType;

/* ADC conversion modes */
typedef enum {
               ADC_CONV_MODE_ONESHOT=0,
               ADC_CONV_MODE_CONTINUOUS,
               ADC_CONV_MODE_ON_DEMAND,
               ADC_CONV_MODE_GATED
             }Adc_GroupConvModeType;


/*ADC length of 2nd phase of sample time */
typedef enum{
                CONV_4 =0,
                CONV_6,
                CONV_8,
                CONV_10,
                CONV_12,
                CONV_16,
                CONV_20,
                CONV_24 
            }Adc_ConvTimeType;

/*ADC channel resolution */
typedef enum{
               RESOLUTION_8_BIT =0,
               RESOLUTION_10_BIT,
               RESOLUTION_12_BIT
            }ResolutionType;


typedef enum {
                ADC_IDLE =0,
                ADC_BUSY_NO_VALUE,
                ADC_BUSY,
                ADC_COMPLETED
             }Adc_StatusType;


/***************Private HW configuration ***************/

/* conversion type */
typedef enum {
                CONTINUE_CONV=0,
                RESERVED,
                FINISH_CURRENT,
                FREEZE
             }Adc_Freeze_ModeType;

typedef enum {
                STOPCONV_OFF=0,
                STOPCONV_ON
             }Adc_StopConvType;

typedef enum{
               ADC_HW_0 =0,
               ADC_HW_1
            }Adc_HW_UnitType;

typedef enum{
               ADC_CONV_NOT_STARTED=0,
               ADC_CONV_STARTED,
               ADC_CONV_FINISHED
            }Adc_ConversionStatusType;

/*This enum defines how the samples shall be acquired while the gate is open*/
typedef enum{
               ADC_GATED_GROUP_ACQ_HW =0,
               ADC_GATED_GROUP_ACQ_CONT
            }Adc_GatedGroupAcqTriggSrc;


/* Group definition structure*/

  typedef struct
  {
      uint8            Adc_TotalCh ;/*Number of channels in group */
      uint8            Adc_Start_Channel;/* Internal channel number */

      /*Multi-channel Sample mode */
      ResolutionType   ADC_CHANNEL_RESOLUTION; /* Resolution of the channel */
      Adc_ConvTimeType ADC_CHANNEL_CONV_TIME; /* conversion/sample time for
                                                   each channel */
      uint8            Adc_Wrap_around;    /*wrap around channel no.*/
      uint8             Adc_Discharge;    /*discharge before sample bit */
}Adc_GroupDefType;

/* Group configuration structure*/
typedef struct
{
    /*Adc HW unit */
    Adc_HW_UnitType  Adc_HW_Unit;

    /* Group trigger source */
    Adc_Group_Trgsrc_OneshotType  ADC_GROUP_TRIGG_SRC_ONESHOT;

    /* Group conversion mode */
    Adc_GroupConvModeType        ADC_GROUP_CONV_MODE;

    /* reference to channels in the group*/
    Adc_GroupDefType             ADC_GROUP_DEFINITION;



}Adc_GroupConfigType;


/* Type definition for ADC driver configuration */
typedef struct
{

    Adc_PrescaleType            ADC_PRESCALE[NO_OF_HW_UNITS];

    /*Adc Freeze Mode*/
    Adc_Freeze_ModeType         Adc_Freeze_Mode[NO_OF_HW_UNITS];
   /* Adc_PowerDownType           Adc_PowerDownMode[NO_OF_HW_UNITS];*/
     Adc_StopConvType         Adc_StopConv[NO_OF_HW_UNITS];

    /* Group configuration array */
    const Adc_GroupConfigType         *Adc_GrpCfg;

}Adc_ConfigType;




/*This container contains the streaming configuration (parameters) of the
 group if ADC_GROUP_CONV_MODE has been set as ADC_CONV_MODE_GATED.*/

typedef struct
{
    Adc_GroupType               ADC_GROUP;

    /*<ADC198> pointer to result buffer */
    Adc_PointerBufferType*      ADC_GATED_BUFFER_POINTER;

    /* <ADC199> No. of ADC values to be acquired */
    uint32                      ADC_GATED_NUM_SAMPLES;

    Adc_GatedGroupAcqTriggSrc   ADC_GATED_GROUP_ACQ_TRIGG_SRC;


}Adc_ConfigGatedType;

/* Global Variables*/

/*declare config structures for normal and gated configurations*/
extern const Adc_ConfigType Adc_Config[ADC_MULTIPLE_CONFIGURATIONS];

/*****************************************************************************
**                      Global Functions                                    **
*****************************************************************************/

extern void Adc_Init(const Adc_ConfigType* ConfigPtr);
extern void Adc_StartGroupConversion(Adc_GroupType group);
extern Adc_ValueType Adc_SingleValueReadChannel(Adc_ChannelType channel);
extern Adc_StatusType Adc_GetGroupStatus (Adc_GroupType group);
extern void Adc_GetVersionInfo (Std_VersionInfoType *versioninfo);


#pragma CODE_SEG __NEAR_SEG NON_BANKED

/*............ISR prototypes.................................................*/

/* ADC notification ISR Declaration */
extern void Adc0_ConversionCompleteIsr(void);

#pragma CODE_SEG DEFAULT

#endif /* ADC_H */
