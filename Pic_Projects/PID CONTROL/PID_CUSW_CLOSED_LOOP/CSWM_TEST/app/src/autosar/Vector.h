/* Created at 15:25 on 1-4-2009 */
/* Version of HALCoGen used : 2.9.3 */
/* Code generated for       : MC9S12XS128 */
/*****************************************************************************/
/* Copyright(c) - This Program/Software is the exclusive property of Infosys. 
The software may not be reproduced or given to third parties without prior 
consent of Infosys.

   Filename                        : Vector.h
   Version                         : 1.1.0
   Microcontroller Platform        : MC9S12XS128
   Compiler                        : Codewarrior HCS12X V5.7.0.

   Description                     : Header file for Vector.c
                                       
   Requirement Specification       : N.A.
                
   Module Design                   : N.A.

   Platform Dependant[yes/no]      : yes
     
   To be changed by user[yes/no]   : no
*/

/*****************************************************************************/
   
/* Revision History

Class of Change
-------------------------------------------------------------------------------
N - New Module.
F - Functional development.
E - Error Correction.
O - Optimization without functional change.
-------------------------------------------------------------------------------
00     20-Nov-06     Anwar Husen G, Infosys
-------------------------------------------------------------------------------
Class: N
Cause: New Module
Detail:This file contains interrupt vector table.
-------------------------------------------------------------------------------
******************************************************************************/

#ifndef _VECTOR_H
#define _VECTOR_H

/*  include module header files */
#include "Adc_Irq.h"
#include "Mcu_Irq.h"
#include "Gpt_Irq.h"
#include "Pwm_Irq.h"

#pragma CODE_SEG __NEAR_SEG NON_BANKED

/* dummy isr */
void interrupt near My_Dummy_Isr(void);

#pragma CODE_SEG DEFAULT

#endif /* #ifndef VECTOR_H */
