/* Created at 15:24 on 1-4-2009 */
/* Version of HALCoGen used : 2.9.3 */
/* Code generated for       : MC9S12XS128 */
/*****************************************************************************/
/* Copyright(c) - This Program/Software is the exclusive property of Infosys. 
The software may not be reproduced or given to third parties without prior 
consent of Infosys.

     Filename                      : RamTst_Algo.h
     Version                       : 1.0.0
     Microcontroller Platform      : MC9S12XS128
     Compiler                      :  Compiler for Freescale HC12 V5.0.30

     Description                   : Header file for RAM Test.
                                     
     Requirement Specification     : AUTOSAR_SWS_RAMTest.doc 0.12
                
     Module Design                 : AUTOTMAL_DDD_RAMTest_SPAL2.0.doc 
                                     version 2.00
     
     Platform Dependant[yes/no]    : yes
     
     To be changed by user[yes/no] : no
*/
/*****************************************************************************/ 

/* Revision History

Class of Change
-------------------------------------------------------------------------------
N - New Module.
F - Functional development.
E - Error Correction.
O - Optimization without functional change.
-------------------------------------------------------------------------------
 00      09-May-2006     Khanindra Jyoti Deka, Infosys
-------------------------------------------------------------------------------
Class: N
Cause: New Module
Detail:This is header file for the RAM Test Algorithms.
-------------------------------------------------------------------------------

******************************************************************************/

/* to avoid Multiple inclusion */
#ifndef _RAMTST_ALGO_H
#define _RAMTST_ALGO_H

/*...................................File Includes...........................*/

/* Stdtypes header file */
#include "RamTst_Types.h"

/*.........................Type Definitions..................................*/
/* To define Nibble Type */
typedef union 
{
    uint8 Nibble :4;
   
    struct 
    {
        uint8 b0:1;
        uint8 b1:1;
        uint8 b2:1;
        uint8 b3:1;
    } bit;
} NibbleType;

/*.........................Global Variable declarations......................*/
#pragma section [safe_ram]
//#pragma DATA_SEG DATASAVE_RAM
/* Common variables of RamTst.c and RamTst_Algo.c files */
extern RamTst_BlockAddressType        RamTst_BlockStartAddressForAlgo;
extern RamTst_BlockAddressType        RamTst_BlockEndAddressForAlgo;
extern RamTst_BlockIDType             RamTst_BlockIDForAlgo;
extern RamTst_NumberOfTestedCellsType RamTst_PresentNumberOfTestedCell;
extern RamTst_BlockAddressType        RamTst_PresentAddress;
extern RamTst_NumberOfTestedCellsType RamTst_TestedCellCounter;
extern RamTst_TestResultType          *RamTst_Result;
extern volatile uint8                 RamTst_DataSave1;
extern volatile uint8                 RamTst_DataSave2;
//#pragma DATA_SEG DEFAULT
 
/*.........................Global Function Prototypes........................*/
/* Exported functions prototypes */
/* ------------------------------*/
extern  void    CheckerboardTest ( void );
#pragma section [] 
#endif  /*_RAMTST_ALGO_H*/

/*............................END OF RamTst_Algo.h...........................*/

