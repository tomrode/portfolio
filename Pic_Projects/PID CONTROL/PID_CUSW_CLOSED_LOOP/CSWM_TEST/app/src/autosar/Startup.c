/* Created at 15:25 on 1-4-2009 */
/* Version of HALCoGen used : 2.9.3 */
/* Code generated for       : MC9S12XS128 */
/*****************************************************************************/
/* Copyright(c) - This Program/Software is the exclusive property of Infosys. 
The software may not be reproduced or given to third parties without prior 
consent of Infosys.

   Filename                        : Startup.c
   Version                         : 1.0.3
   Microcontroller Platform        : MC9S12XS128
   Compiler                        : Compiler for Freescale HC12 V5.0.30

   Description                     : This file contains code for Flash/Eeprom
                                     and Wdg for startup
                                      
   Requirement Specification       : NA
                
   Module Design                   : NA

   Platform Dependant[yes/no]      : yes
     
   To be changed by user[yes/no]   : no
*/

/*****************************************************************************/

/* Revision History

Class of Change
-------------------------------------------------------------------------------
N - New Module.
F - Functional development.
E - Error Correction.
O - Optimization without functional change.
-------------------------------------------------------------------------------
 00     09-Apr-2007     Khanindra, Infosys
-------------------------------------------------------------------------------
Class: N
Cause: New Module
Detail:This file gives the code segment for WDG in Startup.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 01     16-May-2007     Khanindra, Infosys
-------------------------------------------------------------------------------
Class: O
Cause: Removed COPCTL related code.
Detail:Removed COPCTL related code.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 02     21-Aug-2007     Arindam Gupta, Infosys
-------------------------------------------------------------------------------
Class: O
Cause: Change in clock calculation for FLash/EEPROM
Detail:Code added to assign FCLK Register
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 03     29-Nov-2007     Isha Gupta, Infosys
-------------------------------------------------------------------------------
Class: O
Cause: Related to PRL #853.
Detail:The code to trigger watchdog will be removed, if the watchdog default
       mode is WDG_OFF_MODE.
-------------------------------------------------------------------------------
*****************************************************************************/

/*****************************************************************************/
/* 
 *  Start Watchdog Code
 */
/*****************************************************************************/
#define _CLKSEL_ADR  (0x39)
#define _ARMCOP_ADR  (0x3F)
#define _COPCTL_ADR_WDG  (0x3C)

#define _COP_WAITMODE        1

/* Update Watchdog waitmode */
((*(volatile unsigned char*)_CLKSEL_ADR)= 
                          ((*(volatile unsigned char*)_CLKSEL_ADR)&(0xFE))|
                          (((volatile unsigned char)(_COP_WAITMODE))&(0x01)));

/* Check for windows mode */
if(0x80 != ((*(volatile unsigned char*)_COPCTL_ADR_WDG)&0x80))
{
    /* Fill the activation code to restart the timer in appropriate order */
    ((*(volatile unsigned char*)_ARMCOP_ADR)=0x55);
    ((*(volatile unsigned char*)_ARMCOP_ADR)=0xAA);   
}

#undef _DO_ENABLE_COP_

/*****************************************************************************/
/* 
 *  End Watchdog Code
 */
/*****************************************************************************/
/*****************************************************************************/
/*
*  Start EEPROM/FLASH Code
*/
/*****************************************************************************/

#define _FCLKSEL_ADR     (0x100)
#define _FCLK_DIVIDER    (0x07)

/* Assign Clock register */
if(0x80 != ((*(volatile unsigned char*)_FCLKSEL_ADR) & 0x80))
{
    (*(volatile unsigned char*)_FCLKSEL_ADR) = _FCLK_DIVIDER;
}

/*****************************************************************************/
/*
 *  End EEPROM/FLASH Code
 */
/*****************************************************************************/

/********************************End of Startup.c*****************************/
