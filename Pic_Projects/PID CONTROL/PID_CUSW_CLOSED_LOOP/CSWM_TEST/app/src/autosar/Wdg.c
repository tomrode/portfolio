/* Created at 15:25 on 1-4-2009 */
/* Version of HALCoGen used : 2.9.3 */
/* Code generated for       : MC9S12XS128 */

/*****************************************************************************/
/* Copyright(c) - This Program/Software is the exclusive property of Infosys.
The software may not be reproduced or given to third parties without prior
consent of Infosys.

     Filename                      : Wdg.c
     Version                       : 1.1.10
     Microcontroller Platform      : MC9S12XS128
     Compiler                      : Codewarrior HCS12X V5.7.0

     Description                   : This file contains all function
                                     definitions of all functions of
                                     Watchdog Driver module.

     Requirement Specification     : AUTOSAR_SWS_WatchdogDriver.pdf 2.0

     Module Design                 : AUTOTMAL_DDD_WDG_SPAL2.0.doc version 1.00

     Platform Dependant[yes/no]    : yes

     To be changed by user[yes/no] : yes
*/
/*****************************************************************************/

/* Revision History

Class of Change
-------------------------------------------------------------------------------
N - New Module.
F - Functional development.
E - Error Correction.
O - Optimization without functional change.
-------------------------------------------------------------------------------
 00      02-Feb-2006     Arindam Gupta, Infosys
-------------------------------------------------------------------------------
Class: N
Cause: New Module
Detail:This file implements the Watchdog driver.
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
 01      28-Mar-2006     Arindam Gupta, Infosys
-------------------------------------------------------------------------------
Class: N
Cause: Added VersionInfo Function
Detail:The changes are done as per AUTOSAR_SWS_WatchdogDriver.pdf 2.0.
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
 02      04-Apr-2006     Arindam Gupta, Infosys
-------------------------------------------------------------------------------
Class: O
Cause: Optimised SetMode & Init functions.
Detail:The changes are done as per AUTOSAR_SWS_WatchdogDriver.pdf 2.0.
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
 03      03-Jul-2006     Arindam Gupta, Infosys
-------------------------------------------------------------------------------
Class: O
Cause: Added macro check for Trigger function.
Detail:The changes are done as per new requirement from Temic.
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
 04      17-Jun-2006     Arindam Gupta, Infosys
-------------------------------------------------------------------------------
Class: O
Cause: RTRT anomaly report for Beta2
Detail:Updated to suit the anomaly report
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
 05      15-Nov-2006     Ajaykumar, Infosys
-------------------------------------------------------------------------------
Class: E
Cause: Correction of issue No: 527 in PRL
Detail:Wdg_State initialised to WDG_UNINIT globally
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
 06      01-Dec-2006     Arindam Gupta, Infosys
-------------------------------------------------------------------------------
Class: E
Cause: Correction of issue No: 527,576 in PRL
Detail:Branch statements included in OFF section and Switch mode check done
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
 07      08-Jan-2007     Khanindra Jyoti Deka, Infosys
-------------------------------------------------------------------------------
Class: E
Cause: Correction of issue No: 588 in PRL
Detail:The API Wdg_SetMode() is updated for DEM.
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
08     29-Mar-2007   Khanindra , Infosys
-------------------------------------------------------------------------------
Class : F
Cause : For Implementation of Freescale security recommendation
Detail: For Implementation of Freescale security recommendation
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
09     02-Apr-2007   Khanindra , Infosys
-------------------------------------------------------------------------------
Class : O
Cause : After Incorporating Code review comments
Detail: Wdg_Init is updated according to Code review comments.
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
10     16-May-2007   Khanindra , Infosys
-------------------------------------------------------------------------------
Class : F
Cause : Updated for FOPT register
Detail: Updated for FOPT register
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
 11     22-Aug-2007      Isha Gupta, Infosys
-------------------------------------------------------------------------------
Class : F
Cause : Correction of issue #784 in PRL
Detail: The API Wdg_SetMode is updated for req. #WDG008.
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
 12     29-Nov-2007      Isha Gupta, Infosys
-------------------------------------------------------------------------------
Class: O
Cause: Updated Wdg_Init Api for PRL #853.
Detail:Preprocessor check for adding/removing call to Wdg_Trigger Api.
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
 13     31-Mar-2008      Isha Gupta, Infosys
-------------------------------------------------------------------------------
Class : E
Cause : Updated for PRL issue #978
Detail: Compilation check for SW patch version removed.
-------------------------------------------------------------------------------

******************************************************************************/

/*...........................File Includes....................................*/


/* This includes the header file of Watchdog driver */
#include "Wdg.h"

/*...................................Macros..................................*/

/* Assigning the version numbers for the source files */
#define WDG_SW_MAJOR_VERSION_C                        1
#define WDG_SW_MINOR_VERSION_C                        1
#define WDG_SW_PATCH_VERSION_C                        10

#define WDG_AR_MAJOR_VERSION_C                        2
#define WDG_AR_MINOR_VERSION_C                        0
#define WDG_AR_PATCH_VERSION_C                        0

/* The Location for FOPT value */
#define WDG_FOPT_ADR    0xFF0E
/*...............................version Check................................*/

/* Do version checking before compilation */
#if(WDG_SW_MAJOR_VERSION==WDG_SW_MAJOR_VERSION_C)
#if(WDG_SW_MINOR_VERSION==WDG_SW_MINOR_VERSION_C)
#if(WDG_AR_MAJOR_VERSION==WDG_AR_MAJOR_VERSION_C)
#if(WDG_AR_MINOR_VERSION==WDG_AR_MINOR_VERSION_C)
#if(WDG_AR_PATCH_VERSION==WDG_AR_PATCH_VERSION_C)

/* ..........................Global variables................................*/

/* This variable indicates the present state of Watchdog driver */
/* Update the flash location to update in COPCTL */
//const uint16 Wdg_FOPT_Value @WDG_FOPT_ADR = WDG_FOPT_CONFIG_VALUE;	/* MODIFIED */




/*..........................API Definitions..................................*/

/******************************************************************************
* Function Name              : Wdg_Init
*
* Arguments                  : Wdg_ConfigType* ConfigPtr
*
* Description			           : This function initializes global vriables
*                              static variables used in Watchdog Driver module
*                              and Watchdog Driver module hardware.
*
* Return Type                : void
*
* Requirements               : WDG001,WDG028,WDG009,WDG003,WDG025,WDG019
******************************************************************************/



void Wdg_Init(const Wdg_ConfigType *ConfigPtr)
{

    /* WDG025: Check if Driver Disabling is allowed by User */
    if(WDG_OFF_MODE==ConfigPtr->Wdg_Default_Mode)
    {
        return;
    }
    /* Trigger the Watchdog module */ 
    Wdg_Trigger(); 

}
/******************************************************************************
* Function Name              : Wdg_SetMode
*
* Arguments                  : WdgIf_ModeType Mode
*                              The parameter indicates the mode to which the
*                              driver has to be shifted to.
*
* Description			           : This function sets the mode requested by the
*                              Interface.
*
* Return Type                : Std_ReturnType
*                              This returns E_OK or E_NOT_OK depending on the
*                              success of mode change.
*
* Requirements               : WDG004,WDG051,WDG032,WDG016,WDG008,WDG017,WDG018
*                              WDG026.
*
*******************************************************************************/


Std_ReturnType Wdg_SetMode(WdgIf_ModeType Mode)
{

    /* Check if Development Error Detect is enabled or not */


    /* Check if passed mode if OFF mode or not */
    if(WDGIF_OFF_MODE==Mode)
    {
         /* Check  if Disabling of driver is allowed or not */
         /* Check if DEM detect macro is enabled */
         /* Return E_NOT_OK */
         return(E_NOT_OK);
    }

    /* Check if the mode being passed is Fast mode */
    else if(WDGIF_FAST_MODE==Mode)
    {
         /* Writing timeout period chosen by user, in register */
         COPCTL=WDG_SETTINGS_FAST;
         /* Check if DEM detect macro is enabled */
    }
    /* Check if the mode being passed is Slow mode */
    else if(WDGIF_SLOW_MODE==Mode)
    {
         /* Writing timeout period chosen by user, in register */
         COPCTL=WDG_SETTINGS_SLOW;
    /* Check if DEM detect macro is enabled */
    }

    /* Requirement# WDG032 */
    return(E_OK);

}
/******************************************************************************
* Function Name              : Wdg_GetVersionInfo
*
* Arguements                 : Std_VersionInfoType *versionInfo
*                              The pointer stores the version info of the module
*
* Description			           : This function returns the version info of the
*                              module.
*
* Return Type                : void
*
* Requirements               : WDG067,WDG068
*
*******************************************************************************/

void Wdg_GetVersionInfo(Std_VersionInfoType *versionInfo)
{

     
     if(NULL_PTR != versionInfo)
     {
        /* Returns Module Id */
        /* Requirement# WDG067 */
        versionInfo -> moduleID = WDG_MODULE_ID;
        /* Returns Vendor Id */
        /* Requirement# WDG067 */
        versionInfo -> vendorID = WDG_VENDOR_ID;
        /* Returns Major Version of the module */
        /* Requirement# WDG068 */
        versionInfo -> sw_major_version = WDG_SW_MAJOR_VERSION_C;
        /* Returns Minor Version of the module */
        /* Requirement# WDG068 */
        versionInfo -> sw_minor_version = WDG_SW_MINOR_VERSION_C;
        /* Returns Patch version of the module */
        /* Requirement# WDG068 */
        versionInfo -> sw_patch_version = WDG_SW_PATCH_VERSION_C;
     }

}


#else
   #error " Mismatch in AR Patch Version of Wdg.c & Wdg.h "
#endif

#else
   #error " Mismatch in AR Minor Version of Wdg.c & Wdg.h "
#endif

#else
   #error " Mismatch in AR Major Version of Wdg.c & Wdg.h "
#endif

#else
   #error " Mismatch in SW Minor Version of Wdg.c & Wdg.h "
#endif

#else
   #error " Mismatch in SW Major Version of Wdg.c & Wdg.h "
#endif

/* End of version check */
/*...............................END OF Wdg.C................................*/


