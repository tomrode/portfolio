/* Created at 15:25 on 1-4-2009 */
/* Version of HALCoGen used : 2.9.3 */
/* Code generated for       : MC9S12XS128 */
//****************************************************************************/
/* Copyright(c) - This Program/Software is the exclusive property of Infosys.
The software may not be reproduced or given to third parties without prior
consent of Infosys.

     Filename                      : WdgIf.h
     Version                       : 1.0.5
     Microcontroller Platform      : MC9S12XS128
     Compiler                      : Codewarrior HCS12X V5.7.0

     Description                   : Header file for WDG

     Requirement Specification     : AUTOSAR_SWS_WatchdogInterface.pdf 2.0

     Module Design                 : AUTOTMAL_DDD_WDGIF_SPAL2.0.doc version 1.00

     Platform Dependant[yes/no]    : no

     To be changed by user[yes/no] : no

*****************************************************************************/

/* Revision History

Class of Change
-------------------------------------------------------------------------------
N - New Module.
F - Functional development.
E - Error Correction.
O - Optimization without functional change.
-------------------------------------------------------------------------------
 00      03-Feb-2006     Arindam Gupta, Infosys
-------------------------------------------------------------------------------
Class: N
Cause: New Module
Detail:This is header file for Watchdog Interface.
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
 01      27-Mar-2006     Arindam Gupta, Infosys
-------------------------------------------------------------------------------
Class: N
Cause: Changes done for SPAL2.0
Detail:The changes are done as per AUTOSAR_SWS_WatchdogInterface.pdf 2.0
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
 02      04-Apr-2006     Arindam Gupta, Infosys
-------------------------------------------------------------------------------
Class: O
Cause: Added inline functional macros to SetMode & Trigger functions
Detail:The changes are done as per AUTOSAR_SWS_WatchdogInterface.pdf 2.0
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
 03      02-Jun-2006     Arindam Gupta, Infosys
-------------------------------------------------------------------------------
Class: O
Cause: Changed to suit RTRT anomaly report
Detail:The naming of WdgIf_SetModeFct_Ptr,WdgIf_TriggerFct_Ptr changed as per 
       naming convention document.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 04      03-Jul-2006     Arindam Gupta, Infosys
-------------------------------------------------------------------------------
Class: O
Cause: Added macro check for Trigger function pointer array.
Detail:The changes are done as per new requirement from Temic on WDG which 
       effects the Interface part.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 05      17-Jul-2006     Arindam Gupta, Infosys
-------------------------------------------------------------------------------
Class: O
Cause: RTRT anomaly report for Beta2
Detail:Updated to suit the anomaly report
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 06      09-Aug-2006     Arindam Gupta, Infosys
-------------------------------------------------------------------------------
Class: O
Cause: Direct mapping of Wdg Trigger
Detail:WdgIf_Trigger macro is directly mapped to Wdg Trigger
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 07      31-Mar-2008     Ajaykumar,  Infosys
-------------------------------------------------------------------------------
Class:  E
Cause:  PRL issue 978
Detail: Updated in sync with WdgIf.c
-------------------------------------------------------------------------------
******************************************************************************/
/* to avoid Multiple inclusion */
#ifndef _WDGIF_H
#define _WDGIF_H

/*...................................File Includes...........................*/

/* Standard Types header file */
#include "Std_Types.h"
#include "WdgIf_Types.h"
/* Watchdog Driver header file */
#include "Wdg.h"

/*...................................Macros..................................*/

/* Declaring the required macros to perform the version check */
#define WDGIF_SW_MAJOR_VERSION                        1
#define WDGIF_SW_MINOR_VERSION                        0
#define WDGIF_SW_PATCH_VERSION                        5

#define WDGIF_AR_MAJOR_VERSION                        2
#define WDGIF_AR_MINOR_VERSION                        0
#define WDGIF_AR_PATCH_VERSION                        0


/* Declaring the Vendor ID */
#define WDGIF_VENDOR_ID                	          (uint16)40


/* Declaring the Vendor ID */
#define WDGIF_MODULE_ID                            (uint8)43

/* Following development errors are generated by Watchdog Interface API
 services if invoked with incorrect input parameters
*/
#define WDGIF_E_PARAM_DEVICE		                            0x10

/*...................................Typedefs................................*/



/*...........Functional APIs of Interface being mapped to Driver .............*/

/* Declaring the arrays pointing to driver functions */
/* Function to Set the Mode by calling driver function */

#define WdgIf_SetMode(DeviceIndex,WdgMode) Wdg_SetMode(WdgMode)
/* Function to Trigger the timer by calling driver function */
#define WdgIf_Trigger(DeviceIndex) Wdg_Trigger()


#endif /* end _WDGIF_H */


/*...............................END OF WdgIf.h...............................*/






