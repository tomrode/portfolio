/* Created at 15:25 on 1-4-2009 */
/* Version of HALCoGen used : 2.9.3 */
/* Code generated for       : MC9S12XS128 */

/*****************************************************************************/
/* Copyright(c) - This Program/Software is the exclusive property of Infosys.
The software may not be reproduced or given to third parties without prior
consent of Infosys.

    Filename                      : EcuM.h
    Version                       : 1.0.0
    Microcontroller Platform      : MC9S12XS128
    Compiler                      : Codewarrior HCS12X V5.7.0

    Description                   : This file contains EcuM declarations and
                                    defines required for compilation and
                                    testing of the Can Interface,MCU modules.

    Requirement Specification     : AUTOSAR_SWS_EcuStateManager.doc Ver 1.2.2

    Module Design                 : N.A.

    Platform Dependant[yes/no]    : no

    To be changed by user[yes/no] : no
*/
/*****************************************************************************/

/* Revision History

Class of Change
-------------------------------------------------------------------------------
N - New Module.
F - Functional development.
E - Error Correction.
O - Optimization without functional change.
-------------------------------------------------------------------------------
 00    25-Jul-2006     Rose Paul V., Infosys
-------------------------------------------------------------------------------
Class: N
Cause: New Module
Detail:This file is implemented as a dummy file for modules CAN Interface & MCU
-------------------------------------------------------------------------------

******************************************************************************/

/* To avoid double inclusion */
#ifndef _ECUM_H
#define _ECUM_H

/*...................................File Includes...........................*/

#include "Std_Types.h"

typedef struct
{
    uint16 Power:1;
    uint16 Reset:1;
    uint16 InternalReset:1;
    uint16 InternalWdg:1;
    uint16 ExternalWdg:1;
    uint16 TimerExpired:1;
    uint16 Icu:1;
    uint16 Gpt:1;
    uint16 CanIf:1;
    uint16 Unused:7;
    /* To extend */
}EcuM_WakeupSourceType;

/*.........................Global Variable declarations......................*/

extern EcuM_WakeupSourceType WakeupSource;

#define ECUM_WKSOURCE_POWER            WakeupSource.Power
#define ECUM_WKSOURCE_RESET            WakeupSource.Reset
#define ECUM_WKSOURCE_TIMER_EXPIRED    WakeupSource.TimerExpired
#define ECUM_WKSOURCE_INTERNAL_RESET   WakeupSource.InternalReset
#define ECUM_WKSOURCE_INTERNAL_WDG     WakeupSource.InternalWdg
#define ECUM_WKSOURCE_EXTERNAL_WDG     WakeupSource.ExternalWdg
#define ECUM_WKSOURCE_ICU              WakeupSource.Icu
#define ECUM_WKSOURCE_GPT              WakeupSource.Gpt
#define ECUM_WKSOURCE_CANIF            WakeupSource.CanIf

/*.........................Global Function Prototypes........................*/

extern void EcuM_SetWakeUpEvent(EcuM_WakeupSourceType events);

#endif /* end _ECUM_H */

/*...............................END OF ECUM.H...............................*/
