/* Created at 15:25 on 1-4-2009 */
/* Version of HALCoGen used : 2.9.3 */
/* Code generated for       : MC9S12XS128 */
/*****************************************************************************/
/*Filename                        : Mcu.h
  Version                         : 4.3.9
  Microcontroller Platform        : MC9S12XS128
  Compiler                        : Codewarrior HCS12X V5.7.0
  Description                     : Header file for Mcu.c 
  Requirement Specification       : AUTOSAR_SWS_McuDriver.doc version 1.2.0
  
  Module Design                   : AUTOSAR_DDD_MCU_SPAL2.0.doc ver 2.10a
  
  Platform Dependant[yes/no]      : yes
  
  To be changed by user[yes/no]   : no
 */
/*****************************************************************************/

/* Revision History

Class of Change
-------------------------------------------------------------------------------
N - New Module.
F - Functional development.
E - Error Correction.
O - Optimization without functional change.
-------------------------------------------------------------------------------
 00     02-Feb-2006     Kapil Kumar, Infosys
-------------------------------------------------------------------------------
Class: N
Cause: New Module
Detail:This file contains the interface APIs of the MCU driver.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 01     30-Mar-2006     Kapil Kumar, Infosys
-------------------------------------------------------------------------------
Class: F
Cause: Changes for SPAL2.0
Detail:Changes done as per AUTOSAR_SWS_McuDriver.pdf ver 1.2.0
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 02     09-Jun-2006     Harini KE, Infosys
-------------------------------------------------------------------------------
Class: E
Cause: Std_types.h changed,Issues from customer, Anomalies reported from RTRT
Detail:Code modified for the following reasons
       1.Std_VersionInfoType structure is modified in Std_Types.h 
       2.points reported and accepted in Problem report list 
       3.points reported and accepted in RTRT anomaly report list
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 03     10-Jul-2006     Harini KE, Infosys
-------------------------------------------------------------------------------
Class: F
Cause: Macro conversion, and functional change for some APIs, Banked memory 
       model support,Removing _H from the version info of header file
Detail:Code modified for the following reasons-
       1.Implementation of macros for functions Mcu_DistributePllclock,
         Mcu_GetPllStatus, Mcu_PerformReset and Mcu_GetResetReason
       2.Mcu_PerformReset now does an illegal Address Reset 
       3.Mcu_InitRamSection now supports both Banked and Large memory model
       4.Removed _H from the version info of header file
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 04     11-Aug-2006     Harini KE, Infosys
-------------------------------------------------------------------------------
Class: F
Cause: COP Reset not working, Full stop mode not functional 
Detail:Code modified for the following reasons
       1. A call to _Startup is made in Mcu_COPWatchdog_Isr
       2. Mcu_ClkMonitor_Isr and Mcu_COPWatchdog_Isr are updated as reset vectors
       3. Dem call added in Mcu_Init
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 05      15-Nov-2006     Ajaykumar, Infosys
-------------------------------------------------------------------------------
Class: E
Cause: Correction of issue No: 529 in PRL
Detail:Mcu_InitCompleteFlag initialised to FALSE globally in Mcu.c
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
 06     11-Dec-2006     Anwar Husen, Infosys
-------------------------------------------------------------------------------
Class: N
Cause: New Module
Detail:This file contains the interface APIs of the MCU driver.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 07     11-Jan-2007       Anwar Husen, Infosys
-------------------------------------------------------------------------------
Class: F
Cause: FM0,FM1 bit setting
Detail:For each Clock setting there will be a separate Frequency modulation ratio.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 08     30-Jan-2007       Anwar Husen, Infosys
-------------------------------------------------------------------------------
Class: F
Cause: Change Request #609 in PRL
Detail:Pre-processor checks are added to implement the Change request #609.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 09     23-Mar-2007     Anwar Husen, Infosys
-------------------------------------------------------------------------------
Class: O
Cause: Common implementation across derivatives
Detail:If DET is not selected, then Mcu_DistributePllClock  API/APIMACRO will 
       select the PLL clock as system clock.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 10     26-Mar-2007     Rose Paul , Infosys
-------------------------------------------------------------------------------
Class : E
Cause : DISABLE was defined as 0, but it is also used by the preprocessor
        (e.g. #pragma MESSAGE DISABLE ...).The preprocessor directive
        does not work after the inclusion of MCAL_types.h.
        PRL 689- Lint warning 502
Detail: Macros in Mcal_Types prefixed by "MCAL_".
        Suffixed hardcoded values,in expressions with unary operator,with 'u'
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 11     29-Mar-2007     Khanindra , Infosys
-------------------------------------------------------------------------------
Class : F
Cause : Implementation of Freescale security recommendation
Detail: Mcu_ModeCfgType is modified for COPWAI
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 12     02-Apr-2007     Khanindra , Infosys
-------------------------------------------------------------------------------
Class : O
Cause : After Incorporating Code review comments
Detail: After Incorporating Code review comments
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 13     17-Oct-2007     Prashant Kulkarni , Infosys
-------------------------------------------------------------------------------
Class : F
Cause : To support XS128 microcontroller
Detail: Code modifications done so as to support XS128 microcontroller. TIM 
          and ECT are used synonomously
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 14     03-Jan-2008     Khanindra , Infosys
-------------------------------------------------------------------------------
Class : O
Cause : File version has updated.
Detail: File version is updated in sync with Mcu.c file.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 15     01-Apr-2008     Khanindra , Infosys
-------------------------------------------------------------------------------
Class : O
Cause : File version updat.
Detail: File version updated in sync with Mcu.c
-------------------------------------------------------------------------------

******************************************************************************/

#ifndef _MCU_H
#define _MCU_H

/*...................................File Includes...........................*/
/* Stdtypes header file */
#include "Std_Types.h"
/* Infosys defines header file */
#include "MCAL_Types.h"
/* CFG header file */
#include "Mcu_Cfg.h"

#include "EcuM.h"

/*...................................Macros..................................*/

/* Declaring the required macros to perform the version check */
#define MCU_SW_MAJOR_VERSION        4
#define MCU_SW_MINOR_VERSION        3
#define MCU_SW_PATCH_VERSION        9
#define MCU_AR_MAJOR_VERSION        1
#define MCU_AR_MINOR_VERSION        2
#define MCU_AR_PATCH_VERSION        0

/* Vendor ID  */
#define MCU_VENDOR_ID               ((uint16)40)

/*Module ID*/
#define MCU_MODULE_ID               ((uint8)101)

/* Service IDs */
/* Declaring the service ID macros of the MCU Driver service APIs */
#define MCU_INIT_ID                 ((uint8)0x00)
#define MCU_INITRAMSECTION_ID       ((uint8)0x01)
#define MCU_INITCLOCK_ID            ((uint8)0x02)
#define MCU_DISTRIBUTEPLLCLOCK_ID   ((uint8)0x03)
#define MCU_GET_PLLSTATUS_ID        ((uint8)0x04)
#define MCU_GETRESETREASON_ID       ((uint8)0x05)
#define MCU_GETRESETRAWVALUE_ID     ((uint8)0x06)
#define MCU_PERFORMRESET_ID         ((uint8)0x07)
#define MCU_SETMODE_ID              ((uint8)0x08)
#define MCU_GETVERSIONINFO_ID       ((uint8)0x09)
#define MCU_TIMER_INIT_ID           ((uint8)0x0A)

/* DET errors definition */
#define MCU_E_PARAM_CONFIG          ((uint8)0x10)
#define MCU_E_PARAM_CLOCK           ((uint8)0x11)
#define MCU_E_PARAM_MODE            ((uint8)0x12)
#define MCU_E_PARAM_RAMSECTION      ((uint8)0x13)
#define MCU_E_PLL_NOT_LOCKED        ((uint8)0x14)
#define MCU_E_UNINIT                ((uint8)0x15)


/* Function macros */ /* MODIFIED */
#define StartWaitMode  { _asm ("WAI"); }
#define StartStopMode  { _asm ("ANDCC #127"); _asm ("STOP");} /*Clear S bit and 
                                                 execute STOP instruction.*/

/* For software reset purpose */
#define MCU_ILLEGAL_RAM_ADDRESS  0x3FFF
#define MCU_RAM_VALUE            0xAA  

#define MCU_NO_CONTENT           (0)
/******************************************************************************
 *  Name                 : Mcu_GetResetRawValue
 *  Description          : Get Raw RESET type routine
 *  Parameters           : void
 *  Return               : Mcu_RawResetType
 *****************************************************************************/
/* Because the hardware does not have a reset status register
 it must return 0x00 */
#define Mcu_GetResetRawValue() ((Mcu_RawResetType)MCU_RAW_RESET_UNKNOWN)

 /*Api Definitions */
#define Mcu_DistributePllClock()  (CLKSEL_PLLSEL = TRUE)  


#define  Mcu_GetPllStatus()       ((TRUE==CRGFLG_LOCK)?(MCU_PLL_LOCKED):(MCU_PLL_UNLOCKED))

#define Mcu_GetResetReason()	(Mcu_Reset_Reason)
/*define legacy timer for ECT module*/
#define ECT_LEGACY_TIMER     0

/*.........................Type Definitions..................................*/

/* Identification (ID) for a clock setting configured in the configuration 
   structure 
*/
typedef uint8 Mcu_ClockType;

/* Type of RAW reset */
typedef uint8 Mcu_RawResetType;

/* Identification (ID) for a RAM section configured in the configuration 
   structure */
typedef uint8 Mcu_RamSectionType;

/* Identification (ID) for a Mcu mode configured in the configuration
   structure 
 */
typedef uint8 Mcu_ModeType;

/*Type for Lock/unLock state of PLL */
typedef enum 
{
    MCU_PLL_UNLOCKED=0,
    MCU_PLL_LOCKED
}Mcu_PllStatusType;

/* Type of reset */
typedef enum
{
MCU_POWER_ON_RESET=0,/* Power On Reset */
    MCU_WATCHDOG_RESET,        /* Internal Watchdog Timer Reset */
    MCU_SW_RESET,              /* Software Reset */
    MCU_LOW_VOLTAGE_RESET,     /* Low Voltage Reset*/
    MCU_ILLEGAL_ADDRESS_RESET, /* Illegal Address Reset */
    MCU_CLOCK_MONITOR_RESET,   /* Clock Fail Reset */
    MCU_EXTERNAL_RESET         /* External Reset */
}Mcu_ResetType;

/* MCU Low Power mode type */
typedef enum
{
    MCU_POWER_MODE_WAIT = 0,      /* Wait mode */
    MCU_POWER_MODE_PSEUDO_STOP,   /* Pseudo-stop mode */
    MCU_POWER_MODE_STOP           /* FullStop mode */
}Mcu_PowerModeType;

/* Type of reset reason */
typedef enum
{
    MCU_RAW_RESET_UNKNOWN = (uint8)0x00
}Mcu_RawResetReasonType;

/* Clock failure notification type*/

typedef enum
{
    ENABLE_NOTIFICATION=0,
    DISABLE_NOTIFICATION
}Mcu_ClockFailureNotificationType;

/* Definition for RAM section           
     - RAM section base address           
     - RAM section size                   
     - Data pre-setting to be initialized */
typedef struct{
    uint32  Mcu_RamSect_BaseAddr;
    uint32  Mcu_RamSect_Size;
    uint8   Mcu_RamSect_Preset;
}Mcu_RamSectionCfgType;

/* Definition for MCU mode. Contains: 
     - Change of MCU power mode         
     - MCU specific mode properties     */
typedef struct
{
    Mcu_PowerModeType Mcu_PowerMode;
    uint8             Mcu_RTI_Enable_WaitMode;
    uint8             Mcu_PLL_Enable_WaitMode;
    uint8             Mcu_COP_Enable_PseudoStopMode;
    uint8             Mcu_RTI_Enable_PseudoStopMode;
}Mcu_ModeCfgType;

/* Definition for Clock setting. Contains:    
     - MCU special clock distribution settings   
     - PLL settings /start lock options   */
typedef struct
{
    /* Value to be stored in SYNR register if PLL is active */
    uint8   Mcu_SYNR_RegValue;

    /* Value to be stored in REFDV register if PLL is active */
    uint8   Mcu_REFDV_RegValue;
   /* Value to be stored in POSTDV register if PLL is active */
   uint8   Mcu_POSTDIV_RegValue;
   /* PLLCTL_FM bits values */
   uint8   Mcu_PLLCTLFM_Value; 
}Mcu_ClockCfgType;


typedef union
{
  byte Byte;
  struct {
    byte             :1;
    byte SCMIE       :1;        /* Self-clock mode Interrupt Enable */
    byte             :1;
    byte             :1;
    byte LOCKIE      :1;        /* Lock Interrupt Enable */
    byte             :1;
    byte             :1;            
    byte RTIE        :1;        /* Real Time Interrupt Enable */
  } Bits;
}Mcu_CrgIntType ;


typedef union
{
  byte Byte;
  struct {
    byte RTR0    :1; /* Real Time Interrupt Modulus Counter Select Bit 0 */
    byte RTR1    :1; /* Real Time Interrupt Modulus Counter Select Bit 1 */
    byte RTR2    :1; /* Real Time Interrupt Modulus Counter Select Bit 2 */
    byte RTR3    :1; /* Real Time Interrupt Modulus Counter Select Bit 3 */
    byte RTR4    :1; /* Real Time Interrupt Prescale Rate Select Bit 4 */
    byte RTR5    :1; /* Real Time Interrupt Prescale Rate Select Bit 5 */
    byte RTR6    :1; /* Real Time Interrupt Prescale Rate Select Bit 6 */
    byte RTDEC   :1; /* Decimal or Binary Divider Select Bit */
  } Bits;
  struct {
    byte grpRTR  :7;
    byte         :1;
  } MergedBits;
} Mcu_RtiCtlType;


 /* ..................... ECT/ Port T Hw Settings ....................... */    
typedef struct
{
    Mcal_FlagsType Mcu_EctHwSettings;

    uint8 Mcu_Prescaler;                        /* Configurable prescale     */

}Mcu_ExtSettingType;

/* Structure to hold the MCU driver configuration set                   
   Contains the initialization data for the MCU Driver:                 
      - MCU dependent properties                                        
      - Reset Configuration                                             
      - Definition of MCU modes                                         
      - Definition of Clock settings                                    
      - Definition of RAM sections                                      
      - Raw reset reason                                                
      - MCU specific properties [clock safety features - clock monitor] 
      - Wake-up reasons                                                 */
typedef struct
{
    /* Number of RAM  sectors */
    Mcu_RamSectionType        MCU_NUMBER_OF_RAM_SECTORS;  

    /* Number of MCU modes */
    Mcu_ModeType              MCU_NUMBER_OF_MCU_MODES;                  

    /* Number of clock settings */
    Mcu_ClockType             MCU_NUMBER_OF_CLOCK_SETTINGS;            

    /* RAM areas configuration array */                                 
    Mcu_RamSectionCfgType     MCU_RAM_SECTOR_SETTING[MCU_CONFIGURED_RAM_SECTORS];  

    /* Mcu Modes configuration */
    Mcu_ModeCfgType           MCU_MODE_SETTING[MCU_CONFIGURED_MCU_MODES];  

    Mcu_ClockCfgType          MCU_CLOCK_SETTING[MCU_CONFIGURED_CLOCK_SETTINGS];     

    /* MCU specific reset configuration*/
    Mcu_ResetType			 MCU_RESET_SETTING;	           

    /*Enables/Disable Lock/RTI interrupt.*/
    Mcu_CrgIntType      MCU_CRGINT ;

    /* ECT/ Port T Hw Settings .*/
    Mcu_ExtSettingType Mcu_ExtSetting[MCU_CONFIGURED_CLOCK_SETTINGS];

    /*Wakeup reasons ( not implemented in 1st release)*/
    EcuM_WakeupSourceType         MCU_WAKEUP_REASON;

}Mcu_ConfigType;

/*.........................Global Variable declarations......................*/
extern volatile Mcu_ResetType Mcu_Reset_Reason;

/*Software reset flag for PerformReset macro function*/
#pragma DATA_SEG MCU_DATA_RAM
extern volatile uint8 Mcu_SWResetFlag;
#pragma DATA_SEG DEFAULT

/*.........................Global Function Prototypes........................*/
/* MCU timer initialization routine (called also by Mcu_InitClock)*/
extern void Mcu_TimerInit(const Mcu_ClockType ClockSetting);
/* MCU driver initialization routine */
extern void Mcu_Init(const Mcu_ConfigType *ConfigPtr );

/* RAM section initialization routine */
extern Std_ReturnType Mcu_InitRamSection(const Mcu_RamSectionType RamSection );

/* MCU Clock initialization routine */
extern Std_ReturnType Mcu_InitClock(const Mcu_ClockType ClockSetting );

#define Mcu_PerformReset()  Mcu_SWResetFlag = TRUE;   \
                            *((uint8 *__far)MCU_ILLEGAL_RAM_ADDRESS) = MCU_RAM_VALUE       

/* MCU Mode set request routine */
extern void Mcu_SetMode(const Mcu_ModeType McuMode);

/*returns version info of MCU driver */
extern void Mcu_GetVersionInfo (Std_VersionInfoType *versioninfo);

#pragma CODE_SEG __NEAR_SEG NON_BANKED

/*............ISR prototypes.................................................*/

 /* MCU notification ISR Declaration */

extern void Mcu_SelfClockMode_Isr(void);
extern void Mcu_PllLockInterrupt_Isr(void);
extern void Mcu_COPWatchdog(void);
extern void Mcu_ClkMonitor(void); 

#pragma CODE_SEG DEFAULT

extern const Mcu_ConfigType   Mcu_Config;

#endif /* End of _MCU_H*/

/*...............................END OF Mcu.H................................*/