/* Created at 15:25 on 1-4-2009 */
/* Version of HALCoGen used : 2.9.3 */
/* Code generated for       : MC9S12XS128 */
/*****************************************************************************/
/* Copyright(c) - This Program/Software is the exclusive property of Infosys.  
The software may not be reproduced or given to third parties without prior  
consent of Infosys. 
						
   Filename                        : Vector.c
   Version                         : 1.1.0
   Microcontroller Platform        : MC9S12XS128
   Compiler                        : Codewarrior HCS12X V5.7.0

   Description                     : This file contains interrupt vector table.
                                       
   Requirement Specification       : N.A.
                
   Module Design                   : N.A.

   Platform Dependant[yes/no]      : yes
     
   To be changed by user[yes/no]   : no
*/

/*****************************************************************************/
   
/* Revision History

Class of Change
-------------------------------------------------------------------------------
N - New Module.
F - Functional development.
E - Error Correction.
O - Optimization without functional change.
-------------------------------------------------------------------------------
00     20-Nov-06     Anwar Husen G, Infosys
-------------------------------------------------------------------------------
Class: N
Cause: New Module
Detail:This file contains interrupt vector table.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 01     29-Mar-2007   Khanindra , Infosys
-------------------------------------------------------------------------------
Class : F
Cause : For Implementation of Freescale security recommendation
Detail: For Implementation of Freescale security recommendation
-------------------------------------------------------------------------------
******************************************************************************/

#include "Vector.h"


/* Vector base address */
#define VECTOR_START      0xFF10

#pragma CODE_SEG __NEAR_SEG NON_BANKED

/* Interrupt vector table */     
void (* near const vectors[])(void) @ VECTOR_START ={ 
    My_Dummy_Isr,           /* 0xFF10 */
    My_Dummy_Isr,           /* 0xFF12 */
    My_Dummy_Isr,           /* 0xFF14 */
    My_Dummy_Isr,           /* 0xFF16 */
    My_Dummy_Isr,           /* 0xFF18 */
    My_Dummy_Isr,           /* 0xFF1A */
    My_Dummy_Isr,           /* 0xFF1C */
    My_Dummy_Isr,           /* 0xFF1E */
    My_Dummy_Isr,           /* 0xFF20 */
    My_Dummy_Isr,           /* 0xFF22 */
    My_Dummy_Isr,           /* 0xFF24 */
    My_Dummy_Isr,           /* 0xFF26 */
    My_Dummy_Isr,           /* 0xFF28 */
    My_Dummy_Isr,           /* 0xFF2A */
    My_Dummy_Isr,           /* 0xFF2C */
    My_Dummy_Isr,           /* 0xFF2E */
    My_Dummy_Isr,           /* 0xFF30 */
    My_Dummy_Isr,           /* 0xFF32 */
    My_Dummy_Isr,           /* 0xFF34 */
    My_Dummy_Isr,           /* 0xFF36 */
    My_Dummy_Isr,           /* 0xFF38 */
    My_Dummy_Isr,           /* 0xFF3A */
    My_Dummy_Isr,           /* 0xFF3C */
    My_Dummy_Isr,           /* 0xFF3E */
    My_Dummy_Isr,           /* 0xFF40 */
    My_Dummy_Isr,           /* 0xFF42 */
    My_Dummy_Isr,           /* 0xFF44 */
    My_Dummy_Isr,           /* 0xFF46 */
    My_Dummy_Isr,           /* 0xFF48 */
    My_Dummy_Isr,           /* 0xFF4A */
    My_Dummy_Isr,           /* 0xFF4C */
    My_Dummy_Isr,           /* 0xFF4E */
    My_Dummy_Isr,           /* 0xFF50 */
    My_Dummy_Isr,           /* 0xFF52 */
    My_Dummy_Isr,           /* 0xFF54 */
    My_Dummy_Isr,           /* 0xFF56 */
    My_Dummy_Isr,           /* 0xFF58 */
    My_Dummy_Isr,           /* 0xFF5A */
    My_Dummy_Isr,           /* 0xFF5E */
    My_Dummy_Isr,           /* 0xFF5E */
    My_Dummy_Isr,           /* 0xFF60 */
    My_Dummy_Isr,           /* 0xFF62 */
    My_Dummy_Isr,           /* 0xFF64 */
    My_Dummy_Isr,           /* 0xFF66 */
    My_Dummy_Isr,           /* 0xFF68 */
    My_Dummy_Isr,           /* 0xFF6A */
    My_Dummy_Isr,           /* 0xFF6C */
    My_Dummy_Isr,           /* 0xFF6E */
    My_Dummy_Isr,           /* 0xFF70 */
    My_Dummy_Isr,           /* 0xFF72 */
    My_Dummy_Isr,           /* 0xFF74 */
    My_Dummy_Isr,           /* 0xFF76 */
    My_Dummy_Isr,           /* 0xFF78 */
    My_Dummy_Isr,           /* 0xFF7A */
    My_Dummy_Isr,           /* 0xFF7C */
    My_Dummy_Isr,           /* 0xFF7E */
    My_Dummy_Isr,           /* 0xFF80 */
    My_Dummy_Isr,           /* 0xFF82 */
    My_Dummy_Isr,           /* 0xFF84 */
    My_Dummy_Isr,           /* 0xFF86 */
    My_Dummy_Isr,           /* 0xFF88 */
    My_Dummy_Isr,           /* 0xFF8A */
    My_Dummy_Isr,           /* 0xFF8C */
    My_Dummy_Isr,           /* 0xFF8E */
    My_Dummy_Isr,           /* 0xFF90 */
    My_Dummy_Isr,           /* 0xFF92 */
    My_Dummy_Isr,           /* 0xFF94 */
    My_Dummy_Isr,           /* 0xFF96 */
    My_Dummy_Isr,           /* 0xFF98 */
    My_Dummy_Isr,           /* 0xFF9A */
    My_Dummy_Isr,           /* 0xFF9C */
    My_Dummy_Isr,           /* 0xFF9E */
    My_Dummy_Isr,           /* 0xFFA0 */
    My_Dummy_Isr,           /* 0xFFA2 */
    My_Dummy_Isr,           /* 0xFFA4 */
    My_Dummy_Isr,           /* 0xFFA6 */
    My_Dummy_Isr,           /* 0xFFA8 */
    My_Dummy_Isr,           /* 0xFFAA */
    My_Dummy_Isr,           /* 0xFFAC */
    My_Dummy_Isr,           /* 0xFFAE */
    My_Dummy_Isr,           /* 0xFFB0 */
    My_Dummy_Isr,           /* 0xFFB2 */
    My_Dummy_Isr,           /* 0xFFB4 */
    My_Dummy_Isr,           /* 0xFFB6 */
    My_Dummy_Isr,           /* 0xFFB8 */
    My_Dummy_Isr,           /* 0xFFBA */
    My_Dummy_Isr,           /* 0xFFBC */
    My_Dummy_Isr,           /* 0xFFBE */
    My_Dummy_Isr,           /* 0xFFC0 */
    My_Dummy_Isr,           /* 0xFFC2 */
    CrgScm_Isr,             /* 0xFFC4 */
    CrgPllLck_Isr,          /* 0xFFC6 */
    My_Dummy_Isr,           /* 0xFFC8 */
    My_Dummy_Isr,           /* 0xFFCA */
    My_Dummy_Isr,           /* 0xFFCC */
    My_Dummy_Isr,           /* 0xFFCE */
    My_Dummy_Isr,           /* 0xFFD0 */
    Atd_0_Isr,              /* 0xFFD2 */
    My_Dummy_Isr,           /* 0xFFD4 */
    My_Dummy_Isr,           /* 0xFFD6 */
    My_Dummy_Isr,           /* 0xFFD8 */
    My_Dummy_Isr,           /* 0xFFDA */
    My_Dummy_Isr,           /* 0xFFDC */
    My_Dummy_Isr,           /* 0xFFDE */
    Ect_7_Isr,              /* 0xFFE0 */  
    Ect_6_Isr,              /* 0xFFE2 */  
    Ect_5_Isr,              /* 0xFFE4 */  
    Ect_4_Isr,              /* 0xFFE6 */  
    Ect_3_Isr,              /* 0xFFE8 */  
    Ect_2_Isr,              /* 0xFFEA */  
    My_Dummy_Isr,           /* 0xFFEC */
    My_Dummy_Isr,           /* 0xFFEE */
    My_Dummy_Isr,           /* 0xFFF0 */
    My_Dummy_Isr,           /* 0xFFF2 */
    My_Dummy_Isr,           /* 0xFFF4 */
    My_Dummy_Isr,           /* 0xFFF6 */
    My_Dummy_Isr,           /* 0xFFF8 */
    //Cop_Reset_Isr,          /* 0xFFFA */
    //ClkMon_Reset_Isr,       /* 0xFFFC */
    //_Startup                /* 0xFFFE */
};

/* Dummy isr definition */
interrupt void near My_Dummy_Isr(void)
{
    /* Do Nothing */
}

#pragma CODE_SEG DEFAULT

/*...............................End Of Vector.c.............................*/
