/* Created at 15:25 on 1-4-2009 */
/* Version of HALCoGen used : 2.9.3 */
/* Code generated for       : MC9S12XS128 */

/*****************************************************************************/
/*   Copyright (C) none (open standard)

     Filename                      : Std_Types.h
     Version                       : 1.0.0
     Microcontroller Platform      : MC9S12XS128
     Compiler                      : Codewarrior HCS12X V5.7.0

     Description                   : This header contains Standard Type
                                     Definitions and Defines

     Requirement Specification     : RS_LLD_CONFIG_AUTOSAR.pdf

     Platform Dependant[yes/no]    : no

     To be changed by user[yes/no] : no
*/
/*****************************************************************************/

#ifndef _STD_TYPES_H
#define _STD_TYPES_H

#include "compiler.h"
#include "Platform_Types.h"

#define E_OK          0
#define E_NOT_OK      1
#define ON            1
#define OFF           0
#define STD_HIGH      1
#define STD_LOW       0
#define STD_ACTIVE    1
#define STD_IDLE      0

typedef uint8 Std_ReturnType;

typedef struct
{
        uint16 vendorID;
        uint8  moduleID;
        uint8 sw_major_version;
        uint8 sw_minor_version;
        uint8 sw_patch_version;
} Std_VersionInfoType;

#endif


