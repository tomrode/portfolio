/* Created at 15:24 on 1-4-2009 */
/* Version of HALCoGen used : 2.9.3 */
/* Code generated for       : MC9S12XS128 */

/*****************************************************************************/
/* Copyright(c) - This Program/Software is the exclusive property of Infosys. 
The software may not be reproduced or given to third parties without prior 
consent of Infosys.

     Filename                      : RamTst.c
     Version                       : 1.0.3
     Microcontroller Platform      : MC9S12XS128
     Compiler                      : Compiler for Freescale HC12 V5.0.30

     Description                   : This file contains all function 
                                     definitions of all functions of 
                                     RAM Test module 
                                     
     Requirement Specification     : AUTOSAR_SWS_RAMTest.doc 0.12
     
     Module Design                 : AUTOTMAL_DDD_RAMTest_SPAL2.0.doc 
                                     version 2.00
     
     Platform Dependant[yes/no]    : yes
     
     To be changed by user[yes/no] : yes
*/
/*****************************************************************************/

/* Revision History

Class of Change
-------------------------------------------------------------------------------
N - New Module.
F - Functional development.
E - Error Correction.
O - Optimization without functional change.
-------------------------------------------------------------------------------
 00      09-May-2006     Khanindra Jyoti Deka, Infosys
-------------------------------------------------------------------------------
Class: N
Cause: New Module
Detail:This file implements the RAM Test
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 01      24-Aug-2006     Khanindra Jyoti Deka, Infosys
-------------------------------------------------------------------------------
Class: O
Cause: Change has done for RTRT anomaly report
Detail:DataSave_Result is initialized by NO_CONTENTF
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 02      27-Feb-2008     Khanindra Jyoti Deka, Infosys
-------------------------------------------------------------------------------
Class: F
Cause: Code modified to check Number Of Tested Cells.
Detail:API RamTst_MainFunction is updated to check Number Of Tested Cells in 
       end cycle for the Transparent Galpat Test. 
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 03      03-Apr-2008     Khanindra Jyoti Deka, Infosys
-------------------------------------------------------------------------------
Class: F
Cause: RamTst_MainFunction is updated to test Stack locations.Issue 957 and 978
Detail:#957: API RamTst_MainFunction is updated to test Stack locations. 
             Redirection of local variables to DATASAVE_RAM is removed.
       #978: Pre compilation check for RAMTST_SW_PATCH_VERSION is removed. 
       #1006: API RamTst_MainFunction is updated for Failure handle. 
-------------------------------------------------------------------------------

******************************************************************************/

/*...................................File Includes...........................*/
/* Module header */

#include "RamTst.h"

/* RAM Test algorithm header */
#include "RamTst_Algo.h"
 
/*...................................Macros..................................*/
/* SW ->Software vendor version info */
#define RAMTST_SW_MAJOR_VERSION_C        (1)
#define RAMTST_SW_MINOR_VERSION_C        (0)
#define RAMTST_SW_PATCH_VERSION_C        (3)
/* AR ->AUTOSAR version info */
#define RAMTST_AR_MAJOR_VERSION_C        (0)
#define RAMTST_AR_MINOR_VERSION_C        (12)
#define RAMTST_AR_PATCH_VERSION_C        (0)

#define DATA_SAVE_LOCATION_OK            ((uint8)1)
#define DATA_SAVE_LOCATION_NOT_OK        ((uint8)2)
#define PUT_NEW_ADDRESS                  ((uint8)1)

#define RAM_SP_SIZE                      ((uint16)40)
//#define RAM_SP_ADDRESS                   ((uint16)((uint16)&RamTst_Result\
//                                           + RAM_SP_SIZE))
#define RAM_SP_ADDRESS                  ((uint16)0x3dde)	// MODIFIED - This works! This address has to match the safe_ram location in linker file

/*...............................Version Check...............................*/

/* Do version checking before compilation */
#if (RAMTST_SW_MAJOR_VERSION == RAMTST_SW_MAJOR_VERSION_C) 
#if (RAMTST_SW_MINOR_VERSION == RAMTST_SW_MINOR_VERSION_C) 
#if (RAMTST_AR_MAJOR_VERSION == RAMTST_AR_MAJOR_VERSION_C) 
#if (RAMTST_AR_MINOR_VERSION == RAMTST_AR_MINOR_VERSION_C) 
#if (RAMTST_AR_PATCH_VERSION == RAMTST_AR_PATCH_VERSION_C)

/* ..........................Global variables................................*/
/* These global variables have external linkages */
#pragma section [safe_ram]
//#pragma DATA_SEG DATASAVE_RAM 

/* RAM Test data */
RamTst_DataType                RamTst_Data;

/* RAM Test execution status */
RamTst_ExecutionStatusType     RamTst_ExecutionStatus = \
                                      RAMTST_EXECUTION_UNINIT;
/* RAM Test Present Algorithm for the test */
RamTst_AlgorithmType           RamTst_PresentAlgorithm = \
                                      RAMTST_ALGORITHM_UNDEFINED;

/* RAM Test Present Block to be tested */
//static RamTst_BlockIDType             RamTst_PresentBlock;
RamTst_BlockIDType             RamTst_PresentBlock;

/* RAM Test Present tested cell address */
RamTst_BlockAddressType               RamTst_PresentAddress;

/* RAM Test Present tested cell Counter */
RamTst_NumberOfTestedCellsType        RamTst_TestedCellCounter;

/* RAM Test Result blockwise */
RamTst_TestResultType          RamTst_TestResult\
                                      [RAMTST_TEST_TOTAL_NUMBER_OF_BLOCKS];
                               
/* RAM Test Present Number Of Tested Cells */                               
RamTst_NumberOfTestedCellsType        RamTst_PresentNumberOfTestedCell;

/* RAM Test Total Number Of Tested Cells */
RamTst_NumberOfTestedCellsType RamTst_NumberOfNewTestedCells;

/* RAM Test new algorithm */
//static RamTst_AlgorithmType           RamTst_NewAlgorithm;
RamTst_AlgorithmType           RamTst_NewAlgorithm;

/* These global variables have internal linkages */
/* Function pointer to call different algorithms */
static RamTst_AlgorithmFunctionPtrType RamTst_AlgorithmFunctionPtr\
                     [RAMTST_NUMBER_OF_ALGORITHMS] =
                     {NULL_PTR};

/* Store SP address during Stack test */
uint16  RamTst_SP_1;
uint16  RamTst_SP_2;

//#pragma DATA_SEG DEFAULT



/*..........................Local functions Declarations..................................*/

uint8 DataSaveLocationCheck(void);


/*..........................End Local functions Declarations..................................*/


/*..........................Local functions Definitions..................................*/

/******************************************************************************
* Function Name             : DataSaveLocationCheck
*
* Arguments                 : None 
*
* Description               : This function is to check the RAM location which 
*                             is selected by user to save the data.
*                               
* Return Type               : uint8
*
* Requirement numbers       : RamTst007
******************************************************************************/
uint8 DataSaveLocationCheck(void)
{
    /* Local variable declarations and initializtion*/
    uint8 Data_0x55;
    uint8 Data_0xAA;
    uint8 DataSave_Result;
    
    /* Initialization of local variables */
    Data_0x55 = (uint8)0x55;
    Data_0xAA = (uint8)0xAA;
    DataSave_Result = NO_CONTENT;
    
    /* Write 0x55 in the RAM location */
    RamTst_DataSave1 =  Data_0x55;
    
    /* Is the RAM location OK? */
    if(0x55 == RamTst_DataSave1)
    {
        /* Write 0xAA in the RAM location */
        RamTst_DataSave1 =  Data_0xAA;
      
        /* Is the RAM location OK? */
        if(0xAA == RamTst_DataSave1) 
        {
            /* Write 0x55 in the RAM location */
            RamTst_DataSave2 =  Data_0x55;
        
            /* Is the RAM location OK? */
            if(0x55 == RamTst_DataSave2) 
            {
                /* Write 0xAA in the RAM location */
                RamTst_DataSave2 =  Data_0xAA;
          
                /* Is the RAM location OK? */
                if(0xAA == RamTst_DataSave2) 
                {
                    DataSave_Result =DATA_SAVE_LOCATION_OK; 
                }
          
                /* For corrupt RAM location */ 
                else
                {
                    DataSave_Result =DATA_SAVE_LOCATION_NOT_OK;
                }
            }
        
            /* For corrupt RAM location */ 
            else
            {
                DataSave_Result =DATA_SAVE_LOCATION_NOT_OK;
            }
        }
      
        /* For corrupt RAM location */ 
        else
        {
            DataSave_Result =DATA_SAVE_LOCATION_NOT_OK;
        }
    }
    
    /* For corrupt RAM location */ 
    else
    {
        DataSave_Result =DATA_SAVE_LOCATION_NOT_OK;
    }
    
    /* Return the Datasave test result */
    return DataSave_Result;
} 



/*..........................End Local functions Definitions..................................*/


/*..........................API Definitions..................................*/

/******************************************************************************
* Function Name             : RamTst_Init
*
* Arguments                 : None 
*
* Description               : This API initializes all RAM Test global 
*                             variables.This routine changes the execution 
*                             status to RAMTST_EXECUTION_INIT.
*                               
* Return Type               : None
*
* Requirement numbers       : RamTst007                              
******************************************************************************/

void RamTst_Init(void) 

{
    /* Local variable declarations and initializtion*/
    RamTst_BlockIDType Counter_BlockID = NO_CONTENT;

         /* RamTst007: Initialize all RAM Test relevant global variables */
    RamTst_PresentAlgorithm =  RAMTST_DEFAULT_TEST_ALGORITHM;
    RamTst_PresentBlock = NO_CONTENT;
    RamTst_PresentAddress = PUT_NEW_ADDRESS;
    RamTst_TestedCellCounter = NO_CONTENT;   
    RamTst_BlockIDForAlgo = NO_CONTENT;
    RamTst_BlockStartAddressForAlgo = 0;
    RamTst_BlockEndAddressForAlgo = 0;
    RamTst_Result = NULL_PTR;
    RamTst_PresentNumberOfTestedCell = 0;
   
    RamTst_NumberOfNewTestedCells = RAMTST_PRESENT_NUMBER_OF_TESTED_CELLS;
    RamTst_NewAlgorithm = RAMTST_PRESENT_DEFAULT_TEST_ALGORITHM;
   
    /* Initialize the RAM Test result of every block */
    for(Counter_BlockID = 0;Counter_BlockID<RAMTST_TEST_TOTAL_NUMBER_OF_BLOCKS;\
        ++Counter_BlockID) 
    {
        RamTst_TestResult[Counter_BlockID]= RAMTST_RESULT_NOT_TESTED;
    }
   
    /* RamTst_AlgorithmFunctionPtr initialization */
    RamTst_AlgorithmFunctionPtr[(RamTst_DataType)RAMTST_CHECKERBOARD_TEST-\
        (RamTst_DataType)1] = CheckerboardTest;





   
    /* RAM Test status updation */
    RamTst_ExecutionStatus = RAMTST_EXECUTION_INIT;
    
    return;

}
/******************************************************************************
* Function Name             : RamTst_GetTestResult
*
* Arguments                 : None 
*
* Description               : The API returns the current RAM Test result.
*                               
* Return Type               : RamTst_TestResultType
*
* Requirement numbers       : RamTst089,RamTst068,RamTst024 
******************************************************************************/

RamTst_TestResultType RamTst_GetTestResult ( void )

{
   
    /* RamTst024: Return the current RAM Test result */
    return RamTst_TestResult[PRESENT_BLOCK_ID];

}
/******************************************************************************
* Function Name             : RamTst_GetVersionInfo
*
* Arguments                 : Std_VersionInfoType *versioninfo 
*
* Description               : This API stores the version information of the 
*                             RAM Test module in the variable pointed by the 
*                             pointer passed to the function. The version 
*                             information includes
*                             1.Module ID
*                             2.Version ID
*                             3.Vendor specific version numbers.
*                               
* Return Type               : None
*
* Requirement numbers       : RamTst089,RamTst068,RamTst079,RamTst078    
******************************************************************************/

void RamTst_GetVersionInfo ( Std_VersionInfoType *versioninfo )

{
    
    if(NULL_PTR != versioninfo )
    {
        /* This service returns the version information of this module. 
                   The version information includes:
                    -   Module Id
                    -   Vendor Id
                    -   Vendor specific version numbers.
        */
        versioninfo->moduleID         = RAMTST_MODULE_ID;
        versioninfo->vendorID         = RAMTST_VENDOR_ID;
        versioninfo->sw_major_version = RAMTST_SW_MAJOR_VERSION_C;
        versioninfo->sw_minor_version = RAMTST_SW_MINOR_VERSION_C;
        versioninfo->sw_patch_version = RAMTST_SW_PATCH_VERSION_C;
    }

}
/******************************************************************************
* Function Name             : RamTst_MainFunction
*
* Arguments                 : None 
*
* Description               : This API is for executing the RAM Test. This API 
*                             tests the defined RAM areas, starting with the 
*                             first RAM area in the RamTst_ConfigParams. When 
*                             the RAM area is completely tested, the test shall 
*                             restart from the beginning at the next call of 
*                             this service.With one call the service shall test 
*                             a defined number of RAM cells.
*                             
* Return Type               : None
*
* Requirement numbers       : RamTst089,RamTst068,RamTst060,RamTst008,RamTst009
*                             RamTst010,RamTst011,RamTst047,RamTst059
******************************************************************************/

void RamTst_MainFunction ( void )

{

    
    /* RamTst060: Is RAM Test Destructive? */
    /* Check if the RAM area is OK or NOT */
    if(DATA_SAVE_LOCATION_NOT_OK == DataSaveLocationCheck())
    {
        /* Report DEM Error */
        if( NULL_PTR != RamTst_Config.RamTst_ErrorNotification_Ptr)
        {
            /* RamTst_Error();*/
            RamTst_Config.RamTst_ErrorNotification_Ptr();
        }
        return;
    }
   
    /* Is RAM Execution status RAMTST_EXECUTION_STOPPED */
    if(RAMTST_EXECUTION_STOPPED == RamTst_ExecutionStatus) 
    {
        if( NULL_PTR != RamTst_Config.RamTst_ErrorNotification_Ptr)
	       {
            /* RamTst_Error();*/
            RamTst_Config.RamTst_ErrorNotification_Ptr();
	       }
        return;
    }
    
    /* RamTst009: Check Execution status and update that */
    ((RamTst_ExecutionStatus == RAMTST_EXECUTION_INIT)||
    (RamTst_ExecutionStatus == RAMTST_EXECUTION_CONTINUE))?
    (RamTst_ExecutionStatus = RAMTST_EXECUTION_RUNNING):\
    (RamTst_ExecutionStatusType)NO_OPERATION;
    
    /* RamTst059: Each test contains a defined number of RAM cells */
    RamTst_PresentNumberOfTestedCell = RamTst_NumberOfNewTestedCells; 
    
    /* Take the default test algorithm */
    (RAMTST_ALGORITHM_UNDEFINED == RamTst_PresentAlgorithm)?
    (RamTst_PresentAlgorithm = RamTst_NewAlgorithm):\
    (RamTst_AlgorithmType)NO_OPERATION;

    /* Variable definition for RamTst_Algo.c */
    RamTst_BlockIDForAlgo           = 
        ((RamTst_Config.RamTst_AlgParams[RamTst_PresentAlgorithm-1].\
        RamTst_BlockParams+(RamTst_PresentBlock))->RamTst_BlockID - \
            (RamTst_DataType)1);
    /* If the same block is tested more than once then initialise the block */
    if((RamTst_PresentAddress == (RamTst_BlockAddressType)PUT_NEW_ADDRESS)\
      &&(RAMTST_RESULT_NOT_TESTED != RamTst_TestResult[PRESENT_BLOCK_ID]))
    {
        RamTst_TestResult[PRESENT_BLOCK_ID] = RAMTST_RESULT_NOT_TESTED;
    }
    
    /* Take the starting address for a new block */
    (RamTst_PresentAddress == (RamTst_BlockAddressType)PUT_NEW_ADDRESS)\
        ?(RamTst_PresentAddress = PRESENT_BLOCK_STARTING_ADDRESS)\
        :(RamTst_BlockAddressType)NO_OPERATION;
        
    /* Variable definition for RamTst_Algo.c */    
    RamTst_BlockStartAddressForAlgo = 
        (RamTst_Config.RamTst_AlgParams[RamTst_PresentAlgorithm-1].\
        RamTst_BlockParams+(RamTst_PresentBlock))->RamTst_StartAddress;
    /* Variable definition for RamTst_Algo.c */    
    RamTst_BlockEndAddressForAlgo   = 
        (RamTst_Config.RamTst_AlgParams[RamTst_PresentAlgorithm-1].\
        RamTst_BlockParams+(RamTst_PresentBlock))->RamTst_EndAddress;
    /* Variable definition for RamTst_Algo.c */    
    RamTst_Result = RamTst_TestResult;
    
    /* Shift the SP to point to a DATASAVE_RAM location */
    RamTst_SP_1 = RAM_SP_ADDRESS;
    //__asm STS RamTst_SP_2 /* Save the present SP in a DATSAVE_RAM variable */
    //__asm LDS RamTst_SP_1 /* Load the SP with an address located in the DATASAVE_RAM*/
    {_asm ("STS _RamTst_SP_2");} /* Save the present SP in a DATSAVE_RAM variable */ /* MODIFIED */
    {_asm ("LDS _RamTst_SP_1");} /* Load the SP with an address located in the DATASAVE_RAM*/ /* MODIFIED */
    
    
    /* RamTst008: Execute the RAM Test */
    (*RamTst_AlgorithmFunctionPtr[(RamTst_DataType)RamTst_PresentAlgorithm-\
        (RamTst_DataType)1])();
    
    /* Reload the stack pointer with the original saved address */
    //__asm LDS RamTst_SP_2 
    {_asm ("LDS _RamTst_SP_2");} /* MODIFIED */
    
    /* RamTst011: Is RAM Test result OK? */
    if(RAMTST_RESULT_NOT_OK == RamTst_TestResult[PRESENT_BLOCK_ID]) 
    {
        if( NULL_PTR != RamTst_Config.RamTst_ErrorNotification_Ptr)
	       {
            /* RamTst_Error();*/
            RamTst_Config.RamTst_ErrorNotification_Ptr();
	       }
        /* Update the end address of the block */
        RamTst_PresentAddress = END_ADDRESS_OF_THE_PRESENT_BLOCK;
    }
    
    /* Is RamTst_PresentAddress less than END_ADDRESS_OF_THE_PRESENT_BLOCK */
    if(RamTst_PresentAddress >= END_ADDRESS_OF_THE_PRESENT_BLOCK)
    {    
        /* RamTst047: Value updation for the next call */
        if(RamTst_PresentBlock == NUMBER_OF_BLOCKS_FOR_PRESENT_ALGO -\
          (RamTst_BlockIDType)1)
        {
            /* RamTst010: Update RAM Test result */
            if(RAMTST_RESULT_NOT_TESTED == RamTst_TestResult[PRESENT_BLOCK_ID])
            {
                RamTst_TestResult[PRESENT_BLOCK_ID] = RAMTST_RESULT_OK;
            }
            /* Initialize for new Block */
            RamTst_PresentBlock = PUT_NEW_BLOCK;
            /* Initialize for new Address */
            RamTst_PresentAddress = PUT_NEW_ADDRESS;
            /* Initialize for new algorithm */
            RamTst_PresentAlgorithm = RamTst_NewAlgorithm;
        } 
        else
        {
            /* RamTst010: Update RAM Test result */
            if(RAMTST_RESULT_NOT_TESTED == RamTst_TestResult[PRESENT_BLOCK_ID])
            {
                RamTst_TestResult[PRESENT_BLOCK_ID] = RAMTST_RESULT_OK;
            }
            /* Increament block ID */
            ++RamTst_PresentBlock;
            /* Initialize for new Address */
            RamTst_PresentAddress = PUT_NEW_ADDRESS;
        }
        if( NULL_PTR != RamTst_Config.RamTst_CompleteNotification_Ptr)
	       {
            /*RamTst_TestCompleted ()*/
            RamTst_Config.RamTst_CompleteNotification_Ptr();
	       }
    }
    else
    {
        /* Increment the RamTst_PresentAddress for next cycle */
        ++RamTst_PresentAddress;
    }
    return;

}

#else
   #error " Mismatch in AR Patch Version of RamTst.c & RamTst.h "
#endif

#else
   #error " Mismatch in AR Minor Version of RamTst.c & RamTst.h " 
#endif

#else
   #error " Mismatch in AR Major Version of RamTst.c & RamTst.h " 
#endif

#else
   #error " Mismatch in SW Minor Version of RamTst.c & RamTst.h " 
#endif

#else
   #error " Mismatch in SW Major Version of RamTst.c & RamTst.h " 
#endif

/* End of version check */

/*.............................End of RamTst.c...............................*/
#pragma section []

