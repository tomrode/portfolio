/* Created at 15:25 on 1-4-2009 */
/* Version of HALCoGen used : 2.9.3 */
/* Code generated for       : MC9S12XS128 */

/******************************************************************************/
/* Copyright(c) - This Program/Software is the exclusive property of Infosys.
   The software may not be reproduced or given to third parties without prior
   consent of Infosys.

   Filename                      : Port.c
   Version                       : 2.0.8
   Microcontroller Platform      : MC9S12XS128
   Compiler                      : Codewarrior HCS12X V5.0.28

   Description                   : This file contains function definitions
                                   of all functions of PORT Driver Module.
                                   It includes:

                                   1) Port_Init
                                   2) Port_SetPinDirection
                                   3) Port_RefreshPortDirection
                                   4) Port_GetVersionInfo

   Requirement Specification     : AUTOSAR_SWS_Port_Driver_working.doc ver1.1.3

   Module Design                 : AUTOTMAL_DDD_PORT_SPAL2.0.doc version 1.0

   Platform Dependant[yes/no]    : yes

   To be changed by user[yes/no] : yes
*/
/******************************************************************************/
/* Revision History

Class of Change
-------------------------------------------------------------------------------
N - New Module.
F - Functional development.
E - Error Correction.
O - Optimization without functional change.
-------------------------------------------------------------------------------
 00        02-Feb-2006     Vijayakumar K, Infosys
-------------------------------------------------------------------------------
Class : N
Cause : New Module
Detail: This file implements the PORT driver.
-------------------------------------------------------------------------------
 01        03-Mar-2006     Muthuvelavan Natarajan, Infosys
-------------------------------------------------------------------------------
Class : F
Cause : Functional development
Detail: Update for SPAL2.0.
-------------------------------------------------------------------------------
 02        09-Jun-2006     Vijayakumar K, Infosys
-------------------------------------------------------------------------------
Class : E
Cause : Change of Std_VersionInfoType
        Error Correction & IRQCR initialization
Detail: Port_SetPinDirection modified to exit only after enabling interrupts.
        The duration for which the interrupts are disabled is reduced.
        Logic added to initialize IRQCR in Port_Init
        Port_GetVersionInfo modified
        Init completion check done in Development mode in functions
        Port_SetPinDirection & Port_RefreshPortDirection
-------------------------------------------------------------------------------
 03        27-Jun-2006     Vijayakumar K, Infosys
-------------------------------------------------------------------------------
Class : O
Cause : Macros/Inline for speed optimization
Detail: Port_SetPinDirection is configurable as Macro
-------------------------------------------------------------------------------
 04        09-Jan-2007     Kasinath, Infosys
-------------------------------------------------------------------------------
Class : F
Cause : Register name changes for S12XEP100
Detail: Registers ATD1DIENH instead of ATD1DIEN0,ATD1DIENL instead of ATD1DIEN1
        in DP512
-------------------------------------------------------------------------------
 05        31-Jan-2007     Kasinath, Infosys
-------------------------------------------------------------------------------
Class : F
Cause : A2D0 & A2D1 Channel Changes
Detail: Port A2D0 modified to handle 16 channels and port A2D1 8 channels.
-------------------------------------------------------------------------------
 06        26-Mar-2007     Rose Paul , Infosys
-------------------------------------------------------------------------------
Class : E
Cause : PRL 689- Lint warning 502
Detail: Suffixed hardcoded values,in expressions with unary operator,with 'u'
-------------------------------------------------------------------------------
 07        11-Apr-2007     Kasinath, Infosys
-------------------------------------------------------------------------------
Class : E
Cause :  Fix for issue 716
Detail: Pins not used in lower pin packages also initialized.
-------------------------------------------------------------------------------
 08        26-Jun-2007     Sandeep, Infosys
-------------------------------------------------------------------------------
Class : E
Cause : Fix for issue 759, Lint error.
Detail: TRUE replaced by PORT_PIN_OUT in Port_SetPinDirection Api.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 09        25-Sep-2007     Khanindra, Infosys
-------------------------------------------------------------------------------
Class : E
Cause : Fix for issue 816 (PWM issue)
Detail: Global variable Port_PTT_Level is included and updated in Port_Init().
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 10        01-Oct-2007     Ajaykumar, Infosys
-------------------------------------------------------------------------------
Class : F
Cause : MC9S12XS128 controller support
Detail: MC9S12XS128 related pre-processor switches added.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 11        16-Nov-2007     Ajaykumar, Infosys
-------------------------------------------------------------------------------
Class : E
Cause : Bugfix for #833
Detail: Preprocessor switches added for Pin packages.
-------------------------------------------------------------------------------
 12        18-Jan-2008     Ajaykumar, Infosys
-------------------------------------------------------------------------------
Class : E
Cause : Bugfix for #895,#899
Detail: Non-Bonded pins are initialized to OUTPUT.
        PORT087: API Port_SetPinDirection updated to add a Det check for 
        Invalid port pin.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 13        31-Mar-2008     Ajaykumar, Infosys
-------------------------------------------------------------------------------
Class: E
Cause: Changes done for the issue #978
Detail:#978: Pre compiler check for SW Patch Version is removed.
-------------------------------------------------------------------------------

*/
/******************************************************************************/

/*...................................File Includes............................*/

/* Module header */
#include "Port.h"


/*...................................Macros...................................*/

#define PORT_SW_MAJOR_VERSION_C     (2)
#define PORT_SW_MINOR_VERSION_C     (0)
#define PORT_SW_PATCH_VERSION_C     (8)
#define PORT_AR_MAJOR_VERSION_C     (1)
#define PORT_AR_MINOR_VERSION_C     (1)
#define PORT_AR_PATCH_VERSION_C     (3)

/* For local use */
#define PORT_REG_SIZE               ((uint8)8)

/* For local copy of the elements of configuration structure */

/* For micro-controller registers */

/* Set particular bit(Bit) of a particular register(Reg) to given value(Val) */
#define PORT_SET_REG_BIT(Reg,Bit,Value) \
        ((1 == Value) ? (Reg | (1u << Bit)) : (Reg & (~(1u << Bit))))

#define PORT_DIR_OUTPUT   0xFF

/*...............................Version Check................................*/

/* PORT 070: Do version checking before compilation */

#if (PORT_SW_MAJOR_VERSION == PORT_SW_MAJOR_VERSION_C) 
#if (PORT_SW_MINOR_VERSION == PORT_SW_MINOR_VERSION_C) 
#if (PORT_AR_MAJOR_VERSION == PORT_AR_MAJOR_VERSION_C) 
#if (PORT_AR_MINOR_VERSION == PORT_AR_MINOR_VERSION_C) 
#if (PORT_AR_PATCH_VERSION == PORT_AR_PATCH_VERSION_C) 

/* ..........................Global variables.................................*/

/* Save the configured PTT level */
uint8 Port_PTT_Level;

/* For local copy of the configuration structure */

/*........................... Pointer declarations...........................*/
/* To store DDR registers addresses */
static uint8* const Port_DDR_Reg[]=
{  
    (uint8* const)(&(DDRA)),
    (uint8* const)(&(DDRB)),
    (uint8* const)(&(DDRE)),
    (uint8* const)(&(DDRT)),
    (uint8* const)(&(DDRS)),
    (uint8* const)(&(DDRM)),
    (uint8* const)(&(DDRP)),
    (uint8* const)(&(DDRJ)),
    (uint8* const)(&(DDR1AD0))
}; 

/* To copy ConfigPtr for local use */
static const Port_ConfigType* Port_CfgPtr;


/*..........................API Definitions..................................*/

/*******************************************************************************
* Function Name       : Port_Init
*
* Arguments           : const Port_ConfigType *ConfigPtr -
*                           Pointer to configuration set
*                                            
* Description         : This function is used to initialize ALL ports and port 
*                       pins with the configuration set pointed to by ConfigPtr.
*
*                       PORT078: This function should be called first in order 
*                       to initialize the port for use. 
*                       If not called first then no operation can occur on the 
*                       MCU ports and port pins.
*
*                       PORT071: This function shall also be called after a 
*                       reset, in order to reconfigure the ports and port pins 
*                       of the MCU.
*                        
*                       PORT003: This function may also be used to initialize 
*                       the driver software and reinitialize the ports and port 
*                       pins to another configured state depending on the 
*                       configuration set passed to this service.
*
*                       PORT083: Other driver modules may be dependent on the 
*                       PORT driver depending on the available functionality of 
*                       individual port pins on a MCU.
*
*                       PORT004: The configuration of different functionality 
*                       is allowed for each port and port pin on priority basis
*
*                       PORT082: There is no configuration for level inversion
*
* Return Type         : void
*
* Requirement numbers : PORT083, PORT084, PORT001, PORT002, PORT003, PORT055,
*                       PORT004, PORT079, PORT081, PORT082, PORT041, PORT078,
*                       PORT042, PORT043, PORT071
*******************************************************************************/


void Port_Init (const Port_ConfigType *ConfigPtr)
{

    /* PORT041: Initialize all port and port pins as per ConfigPtr */

    /* PORT001: This module shall initialize the whole port structure of 
       the micro-controller */
     
    /* PORT79 & 81: The configurable features for each port are:

           Symbolic pin name 
           Functionality (Mode)
           Dynamic direction changeability
           LEVEL - Data
           DD    - Data Direction
           RD    - Reduced Drive
           PE    - Pull Device Enable
           PS    - Polarity Select
           W-OR  - Wired-OR Mode
           DIE   - Digital Input Enable */
           
    /* PORT043 & PORT55: Set the direction only after setting the level to 
       avoid glitches and spikes on the affected port pins */           
   
    /* Port A Configuration */
    PORTA    = ConfigPtr->Port_A.Level.PortLevel;
    DDRA     = ConfigPtr->Port_AToA2D[PORT_A].Direction.PortDirection;
    
    /* Port B Configuration */
    PORTB    = ConfigPtr->Port_B.Level.PortLevel;
    DDRB     = ConfigPtr->Port_AToA2D[PORT_B].Direction.PortDirection;
    
    
    /* Port E Configuration */
    PORTE    = ConfigPtr->Port_E.Level.PortLevel;
    DDRE     = ConfigPtr->Port_AToA2D[PORT_E].Direction.PortDirection;
    
    
    /*Non-Bonded pins must be initialized to OUTPUT LOW*/
    /* Port K Configuration */
    DDRK     = PORT_DIR_OUTPUT;
  
    /* Port T Configuration */
    PTT      = ConfigPtr->Port_T.Level.PortLevel;
    DDRT     = ConfigPtr->Port_AToA2D[PORT_T].Direction.PortDirection;
    RDRT     = ConfigPtr->Port_T.Drive.PortDrive;
    PERT     = ConfigPtr->Port_T.PullEnable.PortPullEnable;
    PPST     = ConfigPtr->Port_T.Polarity.PortPolarity;
    Port_PTT_Level =  ConfigPtr->Port_T.Level.PortLevel;
    
    /* Port S Configuration */
    PTS      = ConfigPtr->Port_S.Level.PortLevel;
    DDRS     = ConfigPtr->Port_AToA2D[PORT_S].Direction.PortDirection;
    RDRS     = ConfigPtr->Port_S.Drive.PortDrive;
    PERS     = ConfigPtr->Port_S.PullEnable.PortPullEnable;
    PPSS     = ConfigPtr->Port_S.Polarity.PortPolarity;
    WOMS     = ConfigPtr->Port_S.WiredOr.PortWiredOr;
    
    /* Port M Configuration */
    PTM      = ConfigPtr->Port_M.Level.PortLevel;
    DDRM     = ConfigPtr->Port_AToA2D[PORT_M].Direction.PortDirection;
    RDRM     = ConfigPtr->Port_M.Drive.PortDrive;
    PERM     = ConfigPtr->Port_M.PullEnable.PortPullEnable;    
    PPSM     = ConfigPtr->Port_M.Polarity.PortPolarity;    
    WOMM     = ConfigPtr->Port_M.WiredOr.PortWiredOr;
    
    /* Port P Configuration */
    PTP      = ConfigPtr->Port_P.Level.PortLevel;
    DDRP     = ConfigPtr->Port_AToA2D[PORT_P].Direction.PortDirection;
    RDRP     = ConfigPtr->Port_P.Drive.PortDrive;    
    PERP     = ConfigPtr->Port_P.PullEnable.PortPullEnable;    
    PPSP     = ConfigPtr->Port_P.Polarity.PortPolarity;
    
    
    /*Non-Bonded pins must be initialized to OUTPUT LOW*/
    /* Port H Configuration */
    DDRH     = PORT_DIR_OUTPUT;
  
    /* Port J Configuration */
    PTJ      = ConfigPtr->Port_J.Level.PortLevel;
    DDRJ     = ConfigPtr->Port_AToA2D[PORT_J].Direction.PortDirection;
    RDRJ     = ConfigPtr->Port_J.Drive.PortDrive;    
    PERJ     = ConfigPtr->Port_J.PullEnable.PortPullEnable;    
    PPSJ     = ConfigPtr->Port_J.Polarity.PortPolarity;
    
    /* Port AD0L Configuration */
    PT1AD0    = ConfigPtr->Port_A2D0L.Level.PortLevel;
    DDR1AD0   = ConfigPtr->Port_AToA2D[PORT_A2D0L].Direction.PortDirection;
    RDR1AD0   = ConfigPtr->Port_A2D0L.Drive.PortDrive;
    ATD0DIENL  = ConfigPtr->Port_A2D0L.DIE.PortDIE;
    PER1AD0   = ConfigPtr->Port_A2D0L.PullEnable.PortPullEnable;
    
    
    /*Non-Bonded pins must be initialized to OUTPUT LOW*/
    /* Port AD0H Configuration */
    DDR0AD0   = PORT_DIR_OUTPUT;
  
    
  
    /* Miscellaneous configuration */
    
    PUCR      = ConfigPtr->Port_Control.PullEnable.PortPullEnable;
    RDRIV     = ConfigPtr->Port_Control.Drive.PortDrive;
    ECLKCTL   = ConfigPtr->Port_Control.ECLK;
    MODRR     = ConfigPtr->Port_Control.MODR;
     PTTRR    = ConfigPtr->Port_Control.PTT_RR;
    IRQCR     = IRQC;

    /* Copy ConfigPtr for local use */
    Port_CfgPtr = (Port_ConfigType *)ConfigPtr;
    

}
/*******************************************************************************
* Function Name       : Port_SetPinDirection
*
* Arguments           : Pin Port - Pin ID number
*                       Direction Port - Pin direction
*                                            
*        
* Description         : This API sets the directions of port pins. 
*                       It should be called only during run time. 
*                       It will only allow changing the direction of those 
*                       port pins configured as changeable.
*                       It will not alter the output level on a port pin
*                       pointed to by ConfigPtr.
*                       Re-entrancy is supported in this release with OS 
*                       functions DisableAllInterrupts()&EnableAllInterrupts()
*
* Return Type         : void
*
* Requirement numbers : PORT065, PORT075, PORT051, PORT031, PORT077, PORT087,
*                       PORT063, PORT059, PORT087, PORT054, PORT086. PORT026
*******************************************************************************/


void Port_SetPinDirection (Port_PinType Pin, Port_PinDirectionType Direction)
{
    uint8 BitIndex;    /* Identifies a particulr bit in a register */
    uint8 RegIndex;    /* Identifies a particular register / port */
    uint8 Dir_Change;
    

    /* PORT059: Allow to change the direction of only those port
       pins configured as changeable.*/
    /* Change pin direction as specified */
    /* Locate the register (each register is 8 bit wide) */ 
    RegIndex = Pin>>3;
    
    /* Locate the bit (each register is 8 bit wide) */
    BitIndex = (uint8)(Pin % PORT_REG_SIZE);
    
    /* Copy the corresponding pin's Dir_Changeable */
    Dir_Change=Port_CfgPtr->Port_AToA2D[RegIndex].DirChangeable.PortDirConfig;
    
    /* Check if pin direction can be changed dynamically */
    if(PORT_PIN_OUT==((Dir_Change>>BitIndex)&1))
    {
        /* Disable all interrupts */
        DisableAllInterrupts();
        /*Set the particular bit in the particular register to the given value*/
        (*(Port_DDR_Reg[RegIndex])) = (uint8) 
            PORT_SET_REG_BIT((*(Port_DDR_Reg[RegIndex])),BitIndex,Direction);        
        /* Enable all interrupts */
        EnableAllInterrupts();
    }

}
/*******************************************************************************
* Function Name       : Port_RefreshPortDirection
*
* Arguments           : None
*        
* Description         : This API will refresh the direction of all 
*                       configured ports to the configured direction.
*                       It will exclude those port pins from refreshing 
*                       that are configured as 'pin direction changeable 
*                       during runtime'.
*
* Return Type         : void
*
* Requirement numbers : PORT066, PORT060, PORT061
*******************************************************************************/


void Port_RefreshPortDirection(void)
{
    /* For identifying each Port */
    uint8 PortIndex;
    uint8 Port_Dir_Change[PORT_MAX_INDEX];
    uint8 Port_Dir[PORT_MAX_INDEX];

    /* Refresh all the port pin direction that are not dynamically changeable */
    for (PortIndex =0; PortIndex<PORT_MAX_INDEX ;PortIndex++)
    {
        Port_Dir_Change[PortIndex]=
                Port_CfgPtr->Port_AToA2D[PortIndex].DirChangeable.PortDirConfig;
        Port_Dir[PortIndex]=
                Port_CfgPtr->Port_AToA2D[PortIndex].Direction.PortDirection;
        /* PORT066: Check if pin direction cannot be changed dynamically and
           Check if the port was already initialized */
        /* Find out the direction of the pin from configuration data and
               refresh the direction of the port pins appropriately */
        /* Refresh the port pin direction as Input/Output */
        (*(Port_DDR_Reg[PortIndex])) = 
            ((*(Port_DDR_Reg[PortIndex]))&Port_Dir_Change[PortIndex])|
            (Port_Dir[PortIndex]&(~Port_Dir_Change[PortIndex]));
    }

}
/*****************************************************************************
* Function Name              : Port_GetVersionInfo
*
* Arguments                  : Std_VersionInfoType *versioninfo 
*
* Description                : This API stores the version information of the 
*                              PORT module in the variable pointed by the 
*                              pointer passed to the function. The version 
*                              information include
*                              1.Module ID
*                              2.Version ID
*                              3.Vendor specific version numbers.
*
* Return Type                : void
*
* Requirement numbers        : PORT102
******************************************************************************/

void Port_GetVersionInfo(Std_VersionInfoType *versioninfo)
{

    if(NULL_PTR != versioninfo)
    {
        /* Copy MODULE ID */
        versioninfo->moduleID=PORT_MODULE_ID;
        /* Copy VENDOR ID */
        versioninfo->vendorID=PORT_VENDOR_ID;
        /* Copy SW vendor version info. */
        versioninfo->sw_major_version=PORT_SW_MAJOR_VERSION_C;
        versioninfo->sw_minor_version=PORT_SW_MINOR_VERSION_C;
        versioninfo->sw_patch_version=PORT_SW_PATCH_VERSION_C;
    }

}

#else
   #error " Mismatch in AR Patch Version of Port.c & Port.h "
#endif

#else
   #error " Mismatch in AR Minor Version of Port.c & Port.h " 
#endif

#else
   #error " Mismatch in AR Major Version of Port.c & Port.h " 
#endif

#else
   #error " Mismatch in SW Minor Version of Port.c & Port.h " 
#endif

#else
   #error " Mismatch in SW Major Version of Port.c & Port.h " 
#endif
/* End of version check */

/*...............................END OF Port.c................................*/

