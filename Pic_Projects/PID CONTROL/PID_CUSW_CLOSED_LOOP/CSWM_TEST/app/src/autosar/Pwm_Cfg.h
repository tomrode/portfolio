/* Created at 15:25 on 1-4-2009 */
/* Version of HALCoGen used : 2.9.3 */
/* Code generated for       : MC9S12XS128 */
/*****************************************************************************/
/* Copyright(c) - This Program/Software is the exclusive property of Infosys. 
The software may not be reproduced or given to third parties without prior 
consent of Infosys.

   Filename                        : Pwm_Cfg.h
   Version                         : 2.2.2
   Microcontroller Platform        : MC9S12XS128
   Compiler                        : Codewarrior HCS12X V5.7.0

   Description                     : Configuration Header file for Pwm Driver   
   Requirement Specification       : AUTOSAR_SWS_PwmDriver.doc version 1.0.7
                
   Module Design                   : AUTOTMAL_DDD_PWM_SPAL2.0.doc version 1.0

   Platform Dependant[yes/no]      : yes   
     
   To be changed by user[yes/no]   : no  
*/

/*****************************************************************************/
   
/* Revision History

Class of Change
-------------------------------------------------------------------------------
N - New Module.
F - Functional development.
E - Error Correction.
O - Optimization without functional change.
-------------------------------------------------------------------------------
 00     02-Jan-2006     Muthuvelavan, Infosys
-------------------------------------------------------------------------------
Class: N
Cause: New Module
Detail:This file contains definition of Configuration parameters
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 01     02-Mar-2006     Muthuvelavan, Infosys
-------------------------------------------------------------------------------
Class: F
Cause: Functional development
Detail:Update for SPAL2.0 specification.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 02     24-Feb-2007     Khanindra , Infosys
-------------------------------------------------------------------------------
Class: N
Cause: 16 bit PWM implementation
Detail: Implemented 16 bit PWM and incorporated the review comments for S12Q 
        (Version 1.2.0)   
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 03     14-May-2007     Khanindra , Infosys
-------------------------------------------------------------------------------
Class: O
Cause: Optimization for 8 bit and 16 bit PWM HW channels.
Detail: Macro PWM_PWMHW_CHANNELS_8_BIT and PWM_PWMHW_CHANNELS_16_BIT are added 
        for optimization.
-------------------------------------------------------------------------------
******************************************************************************/
/* To avoid Multiple inclusion */
#ifndef _PWM_CFG_H
#define _PWM_CFG_H

/*...................................Macros..................................*/


/*...................Configuration Description File...........................*/

/* Preprocessor switch for enabling/disabling the development error detection */
#define PWM_DEV_ERROR_DETECT            OFF

/* Preprocessor switch to indicate that the notifications are supported */
#define PWM_NOTIFICATION_SUPPORTED      ON

#define PWM_MAX_CONFIG                    ((uint8)1)

/* Runtime configurable parameters */

#define PWM_PWMHW_CONFIGURED_CHANNELS                  (3)
#define PWM_ECTHW_CONFIGURED_CHANNELS                  (5)

/* PWM Configured/Used channels - PWM HW */
#define PWM_CHANNEL_USED_PWM            ((uint8)0xE)
/* PWM Configured/Used channels - ECT HW */
#define PWM_CHANNEL_USED_ECT            ((uint8)0xF8)
#define PWM_PWMHW_CHANNELS_8_BIT     ON
/*****************************************************************************/
/*
CONFIGURATION 1
*/
/*****************************************************************************/


#define PWM_config_0        ((uint8)0)

/* PWM Low Power Mode selection- PWM HW */
#define PWM_MODE_PWM_0                    ((uint8)0x0)

/* PWM Channel Alignment - PWM HW */
#define PWM_CHANNEL_ALIGNED_PWM_0         ((uint8)0x0)

/* PWM Polarity - PWM HW */
#define PWM_POLARITY_PWM_0               ((uint8)0xE)

/* PWM Idle State - PWM HW */
#define PWM_IDLE_STATE_PWM_0             ((uint8)0x0)

/* PWM Channel Class - PWM HW */
#define PWM_CHANNEL_CLASS_PWM_0           ((uint8)0xE)

/* Pwm prescale clock select */
/* For PWM hardware */
#define PWM_PRESCALE_B_A_0                ((uint8)0x22) 
#define PWM_CLOCK_SELECT_0                ((uint8)0x0)

/* Channel symbolic names */
#define PWM_VS_FR                 PWM_CHANNEL_1
#define PWM_VS_FL                 PWM_CHANNEL_2
#define PWM_BASE_REL                 PWM_CHANNEL_3
#define PWM_HS_FL                PWM_CHANNEL_11
#define PWM_HS_FR                PWM_CHANNEL_12
#define PWM_HS_RL                PWM_CHANNEL_13
#define PWM_HS_RR                PWM_CHANNEL_14
#define PWM_STW                PWM_CHANNEL_15

#define PWM_PERIOD_DEFAULT_PWM1_0         ((uint16)160)
#define PWM_PERIOD_DEFAULT_PWM2_0         ((uint16)160)
#define PWM_PERIOD_DEFAULT_PWM3_0         ((uint16)160)

/* Duty cycle scaling: 
   0x0000-> 0%
   0x4000-> 50%
   0x8000-> 100% */
#define PWM_DUTYCYCLE_DEFAULT_PWM1_0      ((uint16)0x0)
#define PWM_DUTYCYCLE_DEFAULT_PWM2_0      ((uint16)0x0)
#define PWM_DUTYCYCLE_DEFAULT_PWM3_0      ((uint16)0x0)
/* PWM Polarity - ECT HW */
#define PWM_POLARITY_1_ECT_0                ((uint8)0xFF)
#define PWM_POLARITY_2_ECT_0                ((uint8)0xC0)

/* PWM Idle State - ECT HW */
#define PWM_IDLE_STATE_1_ECT_0              ((uint8)0xAA)
#define PWM_IDLE_STATE_2_ECT_0              ((uint8)0x80)

/* PWM Channel Class - ECT HW */
#define PWM_CHANNEL_CLASS_ECT_0           ((uint8)0xF8)

#define PWM_PERIOD_DEFAULT_ECT3_0         ((uint16)25000)
#define PWM_PERIOD_DEFAULT_ECT4_0         ((uint16)25000)
#define PWM_PERIOD_DEFAULT_ECT5_0         ((uint16)25000)
#define PWM_PERIOD_DEFAULT_ECT6_0         ((uint16)25000)
#define PWM_PERIOD_DEFAULT_ECT7_0         ((uint16)25000)

#define PWM_DUTYCYCLE_DEFAULT_ECT3_0      ((uint16)0x0)
#define PWM_DUTYCYCLE_DEFAULT_ECT4_0      ((uint16)0x0)
#define PWM_DUTYCYCLE_DEFAULT_ECT5_0      ((uint16)0x0)
#define PWM_DUTYCYCLE_DEFAULT_ECT6_0      ((uint16)0x0)
#define PWM_DUTYCYCLE_DEFAULT_ECT7_0      ((uint16)0x0)

// Defining Notification callback pointers 
#define PWM_NOTIFICATION_CBK_ECT3_0       Pwm_Notification_HFL
#define PWM_NOTIFICATION_CBK_ECT4_0       Pwm_Notification_HFR
#define PWM_NOTIFICATION_CBK_ECT5_0       Pwm_Notification_HRL
#define PWM_NOTIFICATION_CBK_ECT6_0       Pwm_Notification_HRR
#define PWM_NOTIFICATION_CBK_ECT7_0      NULL_PTR

#endif  /* end of #ifndef _PWM_CFG_H */
 
/*.............................END OF Pwm_Cfg.h..............................*/
