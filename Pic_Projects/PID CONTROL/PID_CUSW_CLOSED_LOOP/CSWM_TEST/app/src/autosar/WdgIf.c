/* Created at 15:25 on 1-4-2009 */
/* Version of HALCoGen used : 2.9.3 */
/* Code generated for       : MC9S12XS128 */

/*****************************************************************************/
/* Copyright(c) - This Program/Software is the exclusive property of Infosys.
The software may not be reproduced or given to third parties without prior
consent of Infosys.

     Filename                      : WdgIf.c
     Version                       : 1.0.5
     Microcontroller Platform      : MC9S12XS128
     Compiler                      : Codewarrior HCS12X V5.7.0

     Description                   : This file contains all function
                                     definitions of all functions of
                                     Watchdog Interface module.

     Requirement Specification     : AUTOSAR_SWS_WatchdogInterface.pdf 2.0

     Module Design                 : AUTOTMAL_DDD_WDGIF_SPAL2.0.doc version 1.00

     Platform Dependant[yes/no]    : no

     To be changed by user[yes/no] : yes
*/
/*****************************************************************************/

/* Revision History

Class of Change
-------------------------------------------------------------------------------
N - New Module.
F - Functional development.
E - Error Correction.
O - Optimization without functional change.
-------------------------------------------------------------------------------
 00      03-Feb-2006     Arindam Gupta, Infosys
-------------------------------------------------------------------------------
Class: N
Cause: New Module
Detail:This file implements the Watchdog Interface.
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
 01      27-Mar-2006     Arindam Gupta, Infosys
-------------------------------------------------------------------------------
Class: F
Cause: Updates for SPAL2.0
Detail:The changes are done as per AUTOSAR_SWS_WatchdogInterface.pdf 2.0.
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
 02      04-Apr-2006     Arindam Gupta, Infosys
-------------------------------------------------------------------------------
Class: O
Cause: Deleted SetMode & Trigger functions and changed to inline macros
Detail:The changes are done as per AUTOSAR_SWS_WatchdogInterface.pdf 2.0.
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
 03      02-Jun-2006     Arindam Gupta, Infosys
-------------------------------------------------------------------------------
Class: O
Cause: Changed the SW Patch version
Detail:Changed the SW Patch version to tally with SW Patch version of WdgIf.h.
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
 04      17-Jul-2006     Arindam Gupta, Infosys
-------------------------------------------------------------------------------
Class: O
Cause: RTRT anomaly report for Beta2
Detail:Updated to suit the anomaly report
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
 05      09-Aug-2006     Arindam Gupta, Infosys
-------------------------------------------------------------------------------
Class: O
Cause: External Driver Support
Detail:Function pointers to be added as per configured drivers
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
 06      31-Mar-2008     Ajaykumar, Infosys
-------------------------------------------------------------------------------
Class: E
Cause: Changes done for the issue #978
Detail:#978: Pre compiler check for SW Patch Version is removed.
-------------------------------------------------------------------------------

******************************************************************************/

/*...........................File Includes....................................*/


/* This includes the header file of Watchdog driver */
#include "WdgIf.h"

/*...................................Macros..................................*/

/* Assigning the version numbers for the source files */
#define WDGIF_SW_MAJOR_VERSION_C                        1
#define WDGIF_SW_MINOR_VERSION_C                        0
#define WDGIF_SW_PATCH_VERSION_C                        5

#define WDGIF_AR_MAJOR_VERSION_C                        2
#define WDGIF_AR_MINOR_VERSION_C                        0
#define WDGIF_AR_PATCH_VERSION_C                        0

/*...............................version Check................................*/

/* Do version checking before compilation */
#if(WDGIF_SW_MAJOR_VERSION==WDGIF_SW_MAJOR_VERSION_C)
#if(WDGIF_SW_MINOR_VERSION==WDGIF_SW_MINOR_VERSION_C)
#if(WDGIF_AR_MAJOR_VERSION==WDGIF_AR_MAJOR_VERSION_C)
#if(WDGIF_AR_MINOR_VERSION==WDGIF_AR_MINOR_VERSION_C)
#if(WDGIF_AR_PATCH_VERSION==WDGIF_AR_PATCH_VERSION_C)



    




#else
   #error " Mismatch in AR Patch Version of WdgIf.c & WdgIf.h "
#endif

#else
   #error " Mismatch in AR Minor Version of WdgIf.c & WdgIf.h "
#endif

#else
   #error " Mismatch in AR Major Version of WdgIf.c & WdgIf.h "
#endif

#else
   #error " Mismatch in SW Minor Version of WdgIf.c & WdgIf.h "
#endif

#else
   #error " Mismatch in SW Major Version of WdgIf.c & WdgIf.h "
#endif

/*...............................End of WdgIf.C...............................*/



