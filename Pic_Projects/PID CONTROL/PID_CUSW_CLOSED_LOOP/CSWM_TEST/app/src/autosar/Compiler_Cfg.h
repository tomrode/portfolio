/* Created at 15:25 on 1-4-2009 */
/* Version of HALCoGen used : 2.9.3 */
/* Code generated for       : MC9S12XS128 */
/*******************************************************************************
**                                                                            **
**  SRC-MODULE: Compiler_Cfg.h                                                **
**                                                                            **
**  Copyright (C) none (open standard)                                        **
**                                                                            **
**  TARGET    : S12X                                                          **
**                                                                            **
**  COMPILER  : CodeWarrior 5.7.0                                             **
**                                                                            **
**  PROJECT   : AUTOSAR Demonstrator R2, BMW Standard Core 6                  **
**                                                                            **
**  AUTHOR    : Christoph Mueller-Albrecht                                    **
**                                                                            **
**  PURPOSE   : Configuration file for memory and pointer classes             **
**              Each module can define those classes as macros and pass       **
**              to the compiler abstraction macros defined in Compiler.h      **
**                                                                            **
**  REMARKS   : Version control and support provided by BMW Group             **
**              AUTOSAR SWS Compiler Abstraction version: 0.9.6               **
**                                                                            **
**  PLATFORM DEPENDENT [yes/no]: yes                                          **
**                                                                            **
**  TO BE CHANGED BY USER [yes/no]: no                                        **
**                                                                            **
*******************************************************************************/

/*******************************************************************************
**                      Author Identity                                       **
********************************************************************************
**
** Initials     Name                       Company
** --------     -------------------------  ------------------------------------
** cma          Christoph Mueller-Albrecht BMW Group
**
*******************************************************************************/

/*******************************************************************************
**                      Revision Control History                              **
*******************************************************************************/

/*
 * V2.1.0:  27.03.2006, cma  : Initial revision based on MICRON input
 */

#ifndef COMPILER_CFG_H 
#define COMPILER_CFG_H 

/*******************************************************************************
**                      Include Section                                       **
*******************************************************************************/

#if(defined _SPI_H)
#include <String.h>
#endif

/*******************************************************************************
**                      Configuration data                                    **
*******************************************************************************/


#endif /* COMPILER_CFG_H */
