/* Created at 15:25 on 1-4-2009 */
/* Version of HALCoGen used : 2.9.3 */
/* Code generated for       : MC9S12XS128 */


/*****************************************************************************/
/* Copyright(c) - This Program/Software is the exclusive property of Infosys.
The software may not be reproduced or given to third parties without prior
consent of Infosys.

     Filename                      : Dem.c
     Version                       : 1.0.0
     Microcontroller Platform      : MC9S12XS128
     Compiler                      : Codewarrior HCS12X V5.7.0

     Description                   : This file contains function definitions
                                     of all functions of the DEM Module

     Requirement Specification     : RS_LLD_CONFIG_AUTOSAR.pdf

     Platform Dependant[yes/no]    : no

     To be changed by user[yes/no] : no
*/
/*****************************************************************************/

/*Dummy Dem.c file */

#include "Dem.h"

/**************************************************************************/

/* The Below API is accessed from the modules MCU,FLASH,RAMTST,WDG and CAN */
void Dem_SetEventStatus(Dem_EventIdType EventId,
                            Dem_EventStatusType EventStatus)

{

}

/*...............................END OF DEM.C................................*/
