/* Created at 15:25 on 1-4-2009 */
/* Version of HALCoGen used : 2.9.3 */
/* Code generated for       : MC9S12XS128 */
/*****************************************************************************/
/* Copyright(c) - This Program/Software is the exclusive property of Infosys. 
The software may not be reproduced or given to third parties without prior 
consent of Infosys.

     Filename                      : Gpt_Cfg.h
     Version                       : 3.2.1
     Microcontroller Platform      : MC9S12XS128
     Compiler                      : Codewarrior HCS12X V4.5

     Description                   : This file contains the configuration 
                                     structures which will be used for GPT
                                     Driver Module configuration settings 
                                     and static configuration parameters.    

     Requirement Specification     : WP4.2.2.1.12-SWS-GPT-Driver.doc 1.1.6

     Module Design                 : AUTOTMAL_DDD_GPT_SPAL2.0.doc version 2.00c

     Platform Dependant[yes/no]    : yes

     To be changed by user[yes/no] : no
*/
/*****************************************************************************/

/* Revision History

Class of Change
-------------------------------------------------------------------------------
N - New Module.
F - Functional development.
E - Error Correction.
O - Optimization without functional change.
-------------------------------------------------------------------------------
 00    02-Feb-2006     Sowmya, Infosys
-------------------------------------------------------------------------------
Class: N
Cause: New Module
Detail:Header file for Gpt_Cfg.c
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 01    22-Mar-2006     Sowmya, Infosys
-------------------------------------------------------------------------------
Class: F
Cause: Upgrade to suit to 1.1.6 GPT specification
Detail:Configuration file
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 02    08-Dec-2006          Ramprasad P B, Infosys
-------------------------------------------------------------------------------
Class: N
Cause: New Module
Detail:Header file for Gpt_Cfg.c for S12XEP100
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 03    23-Jan-2007     Ramprasad P B, Infosys
-------------------------------------------------------------------------------
Class: F
Cause: Functional Development 
Detail:Macros GPT_PIT_PRESCALAR0_CONFIG & GPT_PIT_PRESCALAR0_CONFIG
       added to set Microtimer0(PITMTLD0) and  Microtimer1(PITMTLD1) based 
       on the Macro values.
--------------------------------------------------------------------------
-------------------------------------------------------------------------------
 04    24-Jul-2007      Khanindra, Infosys
-------------------------------------------------------------------------------
Class: F
Cause: Updated for the derivatives MC9S12XEG384 and MC9S12XEG256
Detail:Updated for the derivatives MC9S12XEG384 and MC9S12XEG256
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 04    01-Oct-2007      Khanindra, Infosys
-------------------------------------------------------------------------------
Class: F
Cause: Updated for the derivatives MC9S12XS128
Detail:Updated for the derivatives MC9S12XS128. Preprocessor switch 
       HAL_MC9S12XS128 is used to differentiate from other derivatives.As 
       Modulus Down Counter is not available in MC9S12XS128,so Modulus Down 
       Counter related codes are removed using Preprocessor switch.
-------------------------------------------------------------------------------

******************************************************************************/

/* to avoid Multiple inclusion */
#ifndef _GPT_CFG_H
#define _GPT_CFG_H

/*...................................Macros..................................*/

#define HAL_MC9S12XEP100     OFF
#define HAL_MC9S12XEG384     OFF
#define HAL_MC9S12XEG256     OFF
#define HAL_MC9S12XS128    ON
/*...................Configuration Description File...........................*/

/* Preprocessor switch for enabling the development error detection */
#define GPT_DEV_ERROR_DETECT               OFF

/* Preprocessor switch for enabling the wakeup source reporting */
#define GPT_REPORT_WAKEUP_SOURCE           OFF

/* Preprocessor switch to indicate that the Gpt_GetVersionInfo is supported */
#define GPT_VERSION_INFO_API               ON


#define GPT_MAX_NUMBER_OF_CONFIG           1 

#define GPT_config_0			0

#define GPT_ENABLENOTIFICATION_MACRO       OFF
#define GPT_DISABLENOTIFICATION_MACRO      OFF
/* Runtime configurable parameters */
/***************************ECT**********************************************/

/* ECT channel configured */
#define GPT_ECTHW_CONFIG_CHANNELS_0          ((uint8)0x4)

/* Data on Timer port */
#define GPT_DATA_ON_TIMER_PORT_0             ((uint8)0x0)     

/* Data value on timer port */
#define GPT_DATA_VALUE_0                     ((uint8)0x0)        

/* Output action for channel 4 to channel 7 */
#define GPT_ACTION_CH4_CH7_0                 (0x0) /* ch 4 to 7 */

/* Output action for channel 0 to channel 3 */   
#define GPT_ACTION_CH0_CH3_0                 (0x0) /* ch 0 to 3 */ 

/******************************************************************************
* Symbolic Name -Generated by tool when each channel is selected/configured 
* If the User selects/configures one Channel - mapped to corr enumerator  
* This is a mandatory field that the user has to enter if selected for the
* configuration. The symbolic name should reflect in Config as well! 
******************************************************************************/
/* ECT channels */
#define TimeSlice_CH2                 GPT_CHANNEL_2
/* PIT channels */
/* Modulus Down Counter */


/******************************************************************************
* Timer channel mode - 0 = oneshot
*                      1 = continuous
******************************************************************************/
#define GPT_MODEDEFAULT_ECT2_0               1


/* Wake Up capable - For each channel */
#define GPT_WAKEUP_ECT2_0                    FALSE  /* ECT 2 */


/* Callback Notification function */
#define GPT_NOTIFICATION_CBK_ECT2_0          Gpt_TmSlice_Isr  /* ECT 2 */

/* Callout of IOHW abstraction function */
#define GPT_CALLOUT_ECT2_0                   NULL_PTR     /* ECT 2 */


/******************************************************************************
*Mask is used to make sure that we clear the needed bits and then program them
*with a value(for eg. 01/10 etc.)
*Necessity to clear is explained through this example:
*Case1:TCTL1=11[previous value] is ORED with 01[New value] still we get 11
*which is wrong.Hence these masks have 0 written for the channel configured in
*this module.
*For eg If only channels 2,3,4,6 are configured then :
*GPT_ACTIVATION_DEINIT_T03_MASK=0x0f
*GPT_ACTIVATION_DEINIT_T47_MASK=0xcc
******************************************************************************/
#define GPT_ACTIVATION_DEINIT_T03_MASK  0xCF
#define GPT_ACTIVATION_DEINIT_T47_MASK  0xFF



#endif  /* end of _GPT_CFG_H */ 

/*.............................END OF Gpt_Cfg.h...............................*/