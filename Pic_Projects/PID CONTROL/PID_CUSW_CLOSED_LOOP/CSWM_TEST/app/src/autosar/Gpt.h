/* Created at 15:25 on 1-4-2009 */
/* Version of HALCoGen used : 2.9.3 */
/* Code generated for       : MC9S12XS128 */
/****************************************************************************/
/* Copyright(c) - This Program/Software is the exclusive property of Infosys. 
The software may not be reproduced or given to third parties without prior 
consent of Infosys.

     Filename                      : Gpt.h
     Version                       : 3.3.8
     Microcontroller Platform      : MC9S12XS128
     Compiler                      : Codewarrior HCS12X V4.5

     Description                   : This file contains data types defined in
                                     Gpt Driver module and declaration of 
                                     functions defined in Gpt Driver module
                                     which are used by other modules.  

     Requirement Specification     : WP4.2.2.1.12-SWS-GPT-Driver.doc 1.1.6

     Module Design                 : AUTOTMAL_DDD_GPT_SPAL2.0.doc version 2.00c

     Platform Dependant[yes/no]    : yes   

     To be changed by user[yes/no] : no       
*/
/*****************************************************************************/

/* Revision History


Class of Change
-------------------------------------------------------------------------------
N - New Module.
F - Functional development.
E - Error Correction.
O - Optimization without functional change.
-------------------------------------------------------------------------------
 00    02-Feb-2006     Sowmya, Infosys
-------------------------------------------------------------------------------
Class: N
Cause: New Module
Detail:This is header file for GPT Driver.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 01    22-Mar-2006     Sowmya, Infosys
-------------------------------------------------------------------------------
Class: F
Cause: Functional development
Detail:Upgrade to suit to 1.1.6 GPT specification.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 02    26-Jun-2006     Sowmya, Infosys
-------------------------------------------------------------------------------
Class: O
Cause: Implementation of macros for functions Gpt_EnableNotification and
       Gpt_DisableNotification
Detail: Added Function Macro's for Gpt_EnableNotification and
       Gpt_DisableNotification
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 03    26-Jun-2006     Sowmya, Infosys
-------------------------------------------------------------------------------
Class: O
Cause: Implementation of macros for functions Gpt_EnableNotification and
       Gpt_DisableNotification
Detail: Added Function Macro's for Gpt_EnableNotification and
       Gpt_DisableNotification
-------------------------------------------------------------------------------
 04    04-Aug-2006     Rose Paul ,  Infosys
-------------------------------------------------------------------------------
Class: E
Cause: Bug Fixes from Problem Report - 337,338,346
Detail:Fixes:
       1) GptDeInit flag removed (Redundant)
       2) Shifted macros from Gpt.h to Gpt.c to avoid integration compile error
       3) Removed 'OR' from Clearing of Interrupt flags
       4) GetVersionInfo need not check for InitComplete
       5) Bit manipulation macros shifted to Std_Types.h
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 05    11-Aug-2006     Sowmya ,  Infosys
-------------------------------------------------------------------------------
Class: E
Cause: Fixes from RTRT anomaly report.
Detail:Code modified for points reported and accepted in Re-Run of RTRT anomaly 
       report list.
       1) Added requirement no.221 in Gpt_GetVersionInfo.
       2) Explicit check is done in Gpt_DeInit.
       3) Bracket added in Gpt_GetTimeElapsed.
       4) GPT_REPORT_WAKEUP_SOURCE check removed from Gpt_SetMode (Redundant)
       4) Requirement numbers are updated in the comments for all the API's              
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 06      15-Nov-2006     Ajaykumar, Infosys
-------------------------------------------------------------------------------
Class: E
Cause: Correction of issue No: 528 in PRL
Detail:Gpt_InitCompleteFlag initialised to FALSE globally in Gpt.c
	   Gpt_InitCompleteFlag changed from bit flag to uint8 variable in Gpt.c
-------------------------------------------------------------------------------

 07    08-Dec-2006     Ramprasad P B, Infosys
-------------------------------------------------------------------------------
Class: N
Cause: New Module
Detail:This is header file for GPT Driver for S12XEP100.
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
08    12-Jan-2007     Ramprasad P B, Infosys
-------------------------------------------------------------------------------
Class: F
Cause: Functional development
Detail:Updated for version change.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
09    23-Jan-2007     Ramprasad P B, Infosys
-------------------------------------------------------------------------------
Class: F
Cause: Functional development
Detail:Updated for version change.
----------------------------------------------------------------
-------------------------------------------------------------------------------
 10     28-Mar-2007     Rajesh , Infosys
-------------------------------------------------------------------------------
Class:  E
Cause: 
Detail: Changed in Sync with Gpt.c
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
 11     28-Mar-2007     Rajesh , Infosys
-------------------------------------------------------------------------------
Class:  F
Cause:  CR#339
Detail: A new API Gpt_StartTimerAbsolute is added.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 12     14-May-2007     Ramprasad P B , Infosys
-------------------------------------------------------------------------------
Class:  F
Cause:  
Detail: Changed in Sync with Gpt.c
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 13    24-Jul-2007      Khanindra, Infosys
-------------------------------------------------------------------------------
Class: F
Cause: Updated for the derivatives MC9S12XEG384 and MC9S12XEG256 
Detail:Updated for the derivatives MC9S12XEG384 and MC9S12XEG256.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 14    01-Oct-2007      Khanindra, Infosys
-------------------------------------------------------------------------------
Class: F
Cause: Updated for the derivatives MC9S12XS128
Detail:Updated for the derivatives MC9S12XS128. Preprocessor switch 
       HAL_MC9S12XS128 is used to differentiate from other derivatives.As 
       Modulus Down Counter is not available in MC9S12XS128,so Modulus Down 
       Counter related codes are removed using Preprocessor switch.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 15    28-Nov-2007      Khanindra, Infosys
-------------------------------------------------------------------------------
Class: O
Cause: Version updated in sync with the Gpt.c
Detail:Version updated in sync with the Gpt.c
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 16    24-Dec-2007      Khanindra, Infosys
-------------------------------------------------------------------------------
Class: O
Cause: Changes done for the issue #873,#863
Detail:#873: API Gpt_DeInit is updated for the variable Gpt_WakeupCapable for 
             better time and memory efficency.
       #863: APIs Gpt_StartTimer and Gpt_StartTimerAbsolute are updated for the 
             passing parameter 'value'. In the API Gpt_StartTimer, DET check 
             for the parameter 'value' is updated.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 17    31-Mar-2008      Khanindra, Infosys
-------------------------------------------------------------------------------
Class: O
Cause: Version updated in sync with the Gpt.c
Detail:Version updated in sync with the Gpt.c
-------------------------------------------------------------------------------

******************************************************************************/

/* to avoid Multiple inclusion */
#ifndef _GPT_H
#define _GPT_H

/*...................................File Includes...........................*/

/* Stdtypes header file */
#include "Std_Types.h"

/* Infosys defines header file */
#include "MCAL_Types.h"
/* CFG header file */
#include "Gpt_Cfg.h"

/* Callback header file*/ 
#include "GptIf_Cbk.h"

/*...................................Macros..................................*/
/******************************************************************************
* Declaring the required macros to perform the version check
* Please Note: These macros should be typecasted with (uint8) as per SRS
* General.This has not been done as per specification as uint8 is a typedef
* (runtime) and these macros are used at preprocessing time.
******************************************************************************/

/* SW ->Software vendor version info */
#define GPT_SW_MAJOR_VERSION                   (3)
#define GPT_SW_MINOR_VERSION                   (3)
#define GPT_SW_PATCH_VERSION                   (8)
/* AR ->AUTOSAR version info */
#define GPT_AR_MAJOR_VERSION                   (1)
#define GPT_AR_MINOR_VERSION                   (1)
#define GPT_AR_PATCH_VERSION                   (6)

/* Module ID */     
#define GPT_MODULE_ID                            ((uint8)100)

/* Vendor ID */
#define GPT_VENDOR_ID                            ((uint16)40)

/* Declaring the service ID macros of the GPT Driver service APIs */
#define GPT_GETVERSIONINFO_ID                    (uint8)0x00
#define GPT_INIT_ID                              (uint8)0x01
#define GPT_DEINIT_ID                            (uint8)0x02
#define GPT_GETTIMEELAPSED_ID                    (uint8)0x03
#define GPT_GETTIMEREMAINING_ID                  (uint8)0x04
#define GPT_STARTTIMER_ID                        (uint8)0x05
#define GPT_STOPTIMER_ID                         (uint8)0x06
#define GPT_ENABLENOTIFICATION_ID                (uint8)0x07
#define GPT_DISABLENOTIFICATION_ID               (uint8)0x08
#define GPT_SETMODE_ID                           (uint8)0x09
#define GPT_DISABLEWAKEUP_ID                     (uint8)0x0A
#define GPT_ENABLEWAKEUP_ID                      (uint8)0x0B


/* Declaring the error ID macros of the GPT Driver error messages */
#define GPT_E_UNINIT                             (uint8)0x0A
#define GPT_E_BUSY                               (uint8)0x0B
#define GPT_E_NOT_STARTED                        (uint8)0x0C
#define GPT_E_PARAM_CHANNEL                      (uint8)0x14
#define GPT_E_PARAM_VALUE                        (uint8)0x15
#define GPT_E_PARAM_CONFIG                       (uint8)0x1E
#define GPT_E_PARAM_MODE                         (uint8)0x1F

/* Maximum value for timeout period */
#define GPT_MAX_TIMEOUT_VALUE                    ((uint32)65536)

/* Maximum channels on PIT hardware */
#define GPT_MAX_PIT_CHANNELS                     ((uint8)4)

/* Maximum channels on ECT/TIM hardware */
#define GPT_MAX_ECT_CHANNELS                     ((uint8)8)

/* Maximum available channels */
#define GPT_MAX_CHANNELS                         ((uint8)GPT_MAX_PIT_CHANNELS+\
                                                  GPT_MAX_ECT_CHANNELS )                                                 

/*Gives the count of the number of Timer Hardwares in this derivative*/
#define GPT_NOOFHARDWARETYPES                       ((uint8)2) 

/*.........................Type Definitions..................................*/
/* Numeric ID for GPT channel */
typedef enum
{
    GPT_CHANNEL_2,
    GPT_MAX_CONFIG_CHANNELS
}Gpt_ChannelType;

/* Timer value for GPT timer */
typedef uint32 Gpt_ValueType;

/* Gpt module specific prescaler factor per channel */
typedef uint8  Gpt_PrescaleType;

/* GPT Notification PointerType */
typedef void (*Gpt_NotificationType_Ptr)(void);

/* Gpt Mode Type */ 
typedef enum 
{
  GPT_MODE_NORMAL = 0, /* All notifications are available as configured 
                          and selected by Gpt_DisableNotification() and
                          Gpt_EnableNotification() */

  GPT_MODE_SLEEP       /* Only those notifications are available which 
                          are configured as wakeup capable. */

}Gpt_ModeType;

/* Type Definition for GPT timer channels */
typedef struct 
{
    /* Channel mode used by the GPT channel */
    boolean                       Gpt_ModeDefault;

    /* Wakeup capability of CPU for a channel when timeout period expires */
    boolean                       Gpt_Wakeupcapable;

    /* Pointers to Interface layer call back functions */
    Gpt_NotificationType_Ptr      Gpt_Notification;

}Gpt_ChannelInfoType ;

/* Configuration type for ECT/TIM hardware */
typedef struct
{
     /* ECT/TIM Channel name */
     uint8                         Gpt_ChannelConfigured;

     /* Output action on output compare */
     uint8                         Gpt_OutputAction1;

     /* Output action on output compare */
     uint8                         Gpt_OutputAction2;

     /* Data transfer on timer port on successful channel 7 compare */
     uint8                         Gpt_DataTransfer;

     /* Data value(High/Low) for timer channel */
     uint8                         Gpt_DataValue;

}Gpt_ECTConfigType;

/* Configuration type for GPT module */
typedef struct
{
    /* ECT/TIM hardware */
    Gpt_ECTConfigType           Gpt_ECTHardware;

    /* Channel information */
    Gpt_ChannelInfoType         Gpt_Channel[GPT_MAX_CONFIG_CHANNELS];

}Gpt_ConfigType;


/*.........................Global Variable declarations......................*/

/* Declaration of ConfigPtr  */
extern const Gpt_ConfigType Gpt_Config[GPT_MAX_NUMBER_OF_CONFIG];

/*.........................Global Function Prototypes........................*/

/* Declaration of functions which are used by other modules */
extern void  Gpt_GetVersionInfo(Std_VersionInfoType *versioninfo);
extern void  Gpt_Init(const Gpt_ConfigType *configPtr);
extern Gpt_ValueType Gpt_GetTimeElapsed(Gpt_ChannelType channel);
extern Gpt_ValueType Gpt_GetTimeRemaining(Gpt_ChannelType channel);
extern void  Gpt_StartTimer(Gpt_ChannelType channel,Gpt_ValueType value);
extern void Gpt_StartTimerAbsolute(Gpt_ChannelType channel,Gpt_ValueType value);
extern void  Gpt_StopTimer(Gpt_ChannelType channel);
/* Enable Notification function */
extern void  Gpt_EnableNotification(Gpt_ChannelType channel);
/* Disable Notification function */
extern void  Gpt_DisableNotification(Gpt_ChannelType channel); 

#pragma CODE_SEG __NEAR_SEG NON_BANKED

/*............ISR prototypes.................................................*/

/* For PIT hardware */
/* For ECT/TIM hardware */
extern void  Gpt_ECTTimeoutHandler_2_Isr(void);
/* For 16 bit modulus down counter */

#pragma CODE_SEG DEFAULT

#endif /* End of _Gpt_H */

/*...............................END OF Gpt.h................................*/