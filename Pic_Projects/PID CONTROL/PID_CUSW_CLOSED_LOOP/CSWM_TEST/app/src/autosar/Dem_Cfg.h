/* Created at 15:25 on 1-4-2009 */
/* Version of HALCoGen used : 2.9.3 */
/* Code generated for       : MC9S12XS128 */

/*****************************************************************************/
/* Copyright(c) - This Program/Software is the exclusive property of Infosys.
The software may not be reproduced or given to third parties without prior
consent of Infosys.

     Filename                      : Dem_Cfg.h
     Version                       : 1.0.0
     Microcontroller Platform      : MC9S12XS128
     Compiler                      : Codewarrior HCS12X V5.7.0

     Description                   : This file contains static configuration
                                     settings/parameters which will be used for
                                     DEM.

     Requirement Specification     : RS_LLD_CONFIG_AUTOSAR.pdf

     Platform Dependant[yes/no]    : yes

     To be changed by user[yes/no] : no
*/
/*****************************************************************************/

/* To avoid double inclusion */

#ifndef _DEM_CFG_H
#define _DEM_CFG_H

/*...................................File Includes...........................*/

/*...................................Macros..................................*/

#define DEM_EVENT_ID_DUMMY (0)

#define MCU_E_CLOCK_FAILURE      DEM_EVENT_ID_DUMMY /* MCU Clock failed      */
#define WDG_E_MODE_SWITCH_FAILED DEM_EVENT_ID_DUMMY /* WDG Mode switch failed*/
#define WDG_E_DISABLE_REJECTED   DEM_EVENT_ID_DUMMY /* WDG disabled rejected */
#define CAN_E_TIMEOUT            DEM_EVENT_ID_DUMMY /* CAN timeout error     */
#define FLS_E_ERASE_FAILED       DEM_EVENT_ID_DUMMY /* Flash erase failed    */
#define FLS_E_WRITE_FAILED       DEM_EVENT_ID_DUMMY /* Flash write failed    */
#define FLS_E_COMPARE_FAILED     DEM_EVENT_ID_DUMMY /* Flash compare failed  */
#define RAMTST_E_RAM_FAILURE     DEM_EVENT_ID_DUMMY /* RAM Test failed       */

#endif /* End of _DEM_CFG_H */

/*.............................END OF Dem_Cfg.h..............................*/

