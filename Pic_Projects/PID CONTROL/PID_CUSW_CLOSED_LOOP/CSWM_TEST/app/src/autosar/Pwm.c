/* Created at 15:24 on 1-4-2009 */
/* Version of HALCoGen used : 2.9.3 */
/* Code generated for       : MC9S12XS128 */

/*****************************************************************************/
/* Copyright(c) - This Program/Software is the exclusive property of Infosys. 
The software may not be reproduced or given to third parties without prior 
consent of Infosys.

   Filename                      : Pwm.c
   Version                       : 3.4.5
   Microcontroller Platform      : MC9S12XS128
   Compiler                      : Codewarrior HCS12X V5.7.0

   Description                   : This file contains the definitions of all
                                   the functions of PWM Driver Module.
                                   It includes:
                                   1)  Pwm_Init
                                   2)  Pwm_DeInit
                                   3)  Pwm_SetDutyCycle
                                   4)  Pwm_SetPeriod
                                   5)  Pwm_SetOutputToIdle
                                   6)  Pwm_GetOutputState
                                   7)  Pwm_DisableNotification
                                   8)  Pwm_EnableNotification
                                   9)  Pwm_GetVersionInfo 
                                     
   Requirement Specification     : AUTOSAR_SWS_PwmDriver.doc version 1.0.16
                
   Module Design                 : AUTOTMAL_DDD_PWM_SPAL2.0.doc version 1.0
     
   Platform Dependant[yes/no]    : yes
     
   To be changed by user[yes/no] : yes
*/
/*****************************************************************************/
   
/* Revision History

Class of Change
-------------------------------------------------------------------------------
N - New Module.
F - Functional development.
E - Error Correction.
O - Optimization without functional change.
-------------------------------------------------------------------------------
 00     02-Jan-2006     Muthuvelavan, Infosys
-------------------------------------------------------------------------------
Class: N
Cause: New Module
Detail:This file implements the PWM driver.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 01     02-Mar-2006     Muthuvelavan, Infosys
-------------------------------------------------------------------------------
Class: F
Cause: Functional development
Detail:Update for SPAL2.0 specification.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
02  08-Jun-2006   kiran G S,  Infosys
-------------------------------------------------------------------------------
Class:E
Cause:Std_VersionInfoType structure is modified in Std_Types.h 
      Code modified for points reported and accepted in Problem report list 
      Code modified for points reported and accepted in RTRT anomaly reportlist

Detail:updated functions:
       Pwm_SetPeriodAndDuty to check for Center and left allignment.
       All Interrupt routines to check for 100% and 0% dutycycles.
       Pwm_SetOutputtoIdle and  Pwm_SetPeriodAndDuty,where init complete check
       is done before all other checks.
       Pwm_GetOutputState function to include all possible combinations.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
03  05-Sep-2006   Ajaykumar,  Infosys
-------------------------------------------------------------------------------
Class:E
Cause:For Error correction in API Pwm_GetOutputState 
Detail: Update PWM_Getoutputstate
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
04  27-Dec-2006   Kasinath,  Infosys
-------------------------------------------------------------------------------
Class:F
Cause:Changes for S12XEP100 
Detail:Update the Timer Control Register names with changes as per XEP100 library file
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 05     24-Feb-2007     Khanindra , Infosys
-------------------------------------------------------------------------------
Class: N
Cause: 16 bit PWM implementation
Detail: Implemented 16 bit PWM and incorporated the review comments for S12Q 
        (Version 1.2.0)   
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 06     26-Mar-2007     Rose Paul , Infosys
-------------------------------------------------------------------------------
Class : E
Cause : DISABLE was defined as 0, but it is also used by the preprocessor
        (e.g. #pragma MESSAGE DISABLE ...).The preprocessor directive
        does not work after the inclusion of MCAL_types.h.
        PRL 689- Lint warning 502
Detail: Macros in Mcal_Types prefixed by "MCAL_".
        Suffixed hardcoded values,in expressions with unary operator,with 'u'
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 07     13-Apr-2007     Khanindra , Infosys
-------------------------------------------------------------------------------
Class: F
Cause: CFORC implemented in Pwm_SetOutputToIdle and 0xFF removed from ISRs
Detail: CFORC implemented in Pwm_SetOutputToIdle and 0xFF removed from ISRs
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 08     30-Apr-2007     Khanindra , Infosys
-------------------------------------------------------------------------------
Class: O
Cause: Optimization done in ISRs and for 8 bit and 16 bit PWM HW channels.
Detail:ISRs are updated accordingly.Pwm_Init,Pwm_SetDutyCycle,
       Pwm_SetPeriodAndDuty,Pwm_SetOutputToIdle,Pwm_GetOutputState APIs are 
       updated accordingly.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 09     01-Jun-2007     Khanindra , Infosys
-------------------------------------------------------------------------------
Class: E
Cause: Pwm_SetOutputToIdle is updated for the requirement PWM021
Detail:Pwm_SetOutputToIdle is updated for the requirement PWM021
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 10     30-Aug-2007     Khanindra , Infosys
-------------------------------------------------------------------------------
Class: E
Cause: Incorporated the changes for the issue #733
Detail:Pwm_SetOutputToIdle,Pwm_SetDutyCycle and Pwm_SetPeriodAndDuty are 
       updated accordingly
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 11     25-Sep-2007     Khanindra , Infosys
-------------------------------------------------------------------------------
Class: E
Cause: Incorporated the changes for the issue #816
Detail:Pwm_Init and Pwm_DeInit are updated accordingly
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 12    01-Oct-2007      Khanindra, Infosys
-------------------------------------------------------------------------------
Class: F
Cause: Updated for the derivatives MC9S12XS128
Detail:Updated for the derivatives MC9S12XS128. Preprocessor switch 
       HAL_MC9S12XS128 is used to differentiate from other derivatives.TIM is 
       used in XS128. ECT and TIM are used synonymously.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 13     02-Apr-2008     Khanindra, Infosys
-------------------------------------------------------------------------------
Class: O
Cause: Incorporated the changes for the issue #982 and #978
Detail:#982: APIs Pwm_GetOutputState, Pwm_DisableNotification and 
             Pwm_EnableNotification are updated for reordering the DET error 
             check PWM_E_UNINIT in sync with the other driver modules.
       #978: Pre compiler check for SW Patch Version is removed.
       #988: API Pwm_SetPeriodAndDuty updated for critical sections. 
-------------------------------------------------------------------------------

******************************************************************************/


/*...................................File Includes...........................*/

/* Module header */
#include "Pwm.h"  


#include "os.h"

/* PORT header file */
#include "Port.h"

/*...................................Macros..................................*/       
/* Declaring the required macros to perform the version check */
/* SW ->Software vendor version info */
#define PWM_SW_MAJOR_VERSION_C                  (3)
#define PWM_SW_MINOR_VERSION_C                  (4)
#define PWM_SW_PATCH_VERSION_C                  (5)
/* AR ->AUTOSAR version info */
#define PWM_AR_MAJOR_VERSION_C                  (1)
#define PWM_AR_MINOR_VERSION_C                  (0)
#define PWM_AR_PATCH_VERSION_C                  (7)

/* Duty cycle related macros */
#define PWM_HUNDRED_PERCENT                     ((uint16)0x8000)
/* Register init value macros */
#define PWM_CENTRE_ALLIGNED                     ((uint8)0x01)

/* ECT/TIM channel numbers */
#define PWM_ECT_CH_3  (PWM_CHANNEL_11-PWM_PWMHW_CONFIGURED_CHANNELS)
#define PWM_ECT_CH_4  (PWM_CHANNEL_12-PWM_PWMHW_CONFIGURED_CHANNELS)
#define PWM_ECT_CH_5  (PWM_CHANNEL_13-PWM_PWMHW_CONFIGURED_CHANNELS)
#define PWM_ECT_CH_6  (PWM_CHANNEL_14-PWM_PWMHW_CONFIGURED_CHANNELS)
#define PWM_ECT_CH_7  (PWM_CHANNEL_15-PWM_PWMHW_CONFIGURED_CHANNELS)


/* Definition of Timer channels */
#define ECT_TC0          TC0
#define ECT_TC1          TC1
#define ECT_TC2          TC2
#define ECT_TC3          TC3
#define ECT_TC4          TC4
#define ECT_TC5          TC5
#define ECT_TC6          TC6
#define ECT_TC7          TC7
#define ECT_TIE          TIE
#define ECT_TFLG1        TFLG1
#define ECT_TIOS     	   TIOS
#define ECT_TCTL1        TCTL1
#define ECT_TCTL2        TCTL2
#define ECT_TCNT         TCNT
#define ECT_CFORC        CFORC 
#define ECT_TIE_C0I      TIE_C0I
#define ECT_TIE_C1I      TIE_C1I
#define ECT_TIE_C2I      TIE_C2I
#define ECT_TIE_C3I      TIE_C3I
#define ECT_TIE_C4I      TIE_C4I
#define ECT_TIE_C5I      TIE_C5I
#define ECT_TIE_C6I      TIE_C6I
#define ECT_TIE_C7I      TIE_C7I
#define ECT_TCTL2_OL0    TCTL2_OL0
#define ECT_TCTL2_OL1    TCTL2_OL1
#define ECT_TCTL2_OL2    TCTL2_OL2
#define ECT_TCTL2_OL3    TCTL2_OL3
#define ECT_TCTL1_OL4    TCTL1_OL4
#define ECT_TCTL1_OL5    TCTL1_OL5
#define ECT_TCTL1_OL6    TCTL1_OL6
#define ECT_TCTL1_OL7    TCTL1_OL7
/*...............................version Check...............................*/
/* Do version checking before compilation */
#if (PWM_SW_MAJOR_VERSION==PWM_SW_MAJOR_VERSION_C) 
#if (PWM_SW_MINOR_VERSION==PWM_SW_MINOR_VERSION_C) 
#if (PWM_AR_MAJOR_VERSION==PWM_AR_MAJOR_VERSION_C) 
#if (PWM_AR_MINOR_VERSION==PWM_AR_MINOR_VERSION_C) 
#if (PWM_AR_PATCH_VERSION==PWM_AR_PATCH_VERSION_C)

/* ..........................Global variables................................*/

/* These global variables have external linkages */

/* These global variables have internal linkages */
/***************************************************************************** 
   The following map index array will be of size equal to maximum configured
   channels in a particular HW (in this case 8) which will index logical
   channel no. to actual channel no.
   Ex: Channel_Used = 0b01010101, then
   Pwm_MapPwmIndex[0]=0           
   Pwm_MapPwmIndex[1]=2           
   Pwm_MapPwmIndex[2]=4           
   Pwm_MapPwmIndex[3]=6 
   
   Note: The same explanation holds good for ECT/TIM HW as well.
******************************************************************************/
/* 8-bit channels -> 0x00,0x01,0x02,0x03 */
/* 16-bit channels -> 0x10,0x12,0x14,0x16*/
static uint8 const Pwm_MapPwmIndex[PWM_PWMHW_CONFIGURED_CHANNELS]=
                                                            {
                                                                0x01,0x02,0x03
                                                            };
static uint8 const Pwm_MapEctIndex[PWM_ECTHW_CONFIGURED_CHANNELS]=
                                                            {
                                                                3,4,5,6,7
                                                            };

/* To store Notification enable/disable info. for ECT/TIM channels(configured) */
static uint8 Pwm_Ena_Notif_Ect[PWM_ECTHW_CONFIGURED_CHANNELS];

/* ECT/TIM variables */
/* To store ECT/TIM Channel period */
static uint16 Pwm_Ect_Period[PWM_ECTHW_CONFIGURED_CHANNELS];
/* To store ECT/TIM Channel ON time */
static uint16 Pwm_Ect_On_Time[PWM_ECTHW_CONFIGURED_CHANNELS];
/* To store ECT/TIM Channel OFF time */
static uint16 Pwm_Ect_Off_Time[PWM_ECTHW_CONFIGURED_CHANNELS];

/* Each bit represent one channel in ECT*/
/* 1-> ON time is currently loaded in TCx, 0-> OFF time*/
static Mcal_FlagsType Pwm_On_Off_Current_Stat={MCAL_CLEAR};

/*........................... Pointer declarations...........................*/
/* To store base address of Period registers (PWM HW) */
static uint8* Pwm_Base_Period_Reg = (uint8*)(&(PWMPER0));
/* To store base address of Duty registers (PWM HW) */
static uint8* Pwm_Base_Duty_Reg = (uint8*)(&(PWMDTY0));
/* To store base address of counter registers (PWM HW) */
static uint8* Pwm_Base_Cntr_Reg = (uint8*)(&(PWMCNT0));

/* To store the TC (Output compare) registers base address (ECT HW) */
static uint16* const Pwm_BaseAddr_TC = (uint16* const)(&(ECT_TC0));
/* Copy of ConfigPtr passed to Pwm_Init function */
static const Pwm_ConfigType* Pwm_CfgPtr;
/* IDLE state for ECT channels */
static const uint8 Pwm_ECT_IDLE[PWM_MAX_CONFIG]=
{
     0x0
};
/* Pwm configuration number */
static uint8 Pwm_Confg_Count;
volatile Mcal_FlagsType Pwm_Calculated_POLARITY_1 ={MCAL_CLEAR};
volatile Mcal_FlagsType Pwm_Calculated_POLARITY_2 ={MCAL_CLEAR};


/*..........................API Definitions..................................*/

/******************************************************************************
* Function Name              : Pwm_Init
*
* Arguments                  : Pwm_ConfigType*
*                              The parameter is a pointer 
*                              to the configuration that shall be initialized.
*                              
* Description                : This function Initializes PWM registers
*                              and static variables.
*
* Return Type                : void
*
* Requirement numbers        : PWM046, PWM051, PWM007, PWM062, PWM009, PWM052
******************************************************************************/


void Pwm_Init(const Pwm_ConfigType *ConfigPtr)
{
    uint8 index;
    uint32 DutyCycle;
    uint8 actual_channel;


    /* Save the ConfigPtr, for later use */
    Pwm_CfgPtr = (Pwm_ConfigType *)ConfigPtr;
 
    /******************* Configure PWM hardware channels *********************/
    /* Init concat channels used and low power modes */
    PWMCTL |= ConfigPtr->PWM_PWMHW.POWER_MODE;
    
    /* Prescale value for ClockA&B */
    PWMPRCLK_PCKA = (ConfigPtr->PWM_PWMHW.PRESCALE_B_A & 0x07);
    PWMPRCLK_PCKB = (ConfigPtr->PWM_PWMHW.PRESCALE_B_A & 0x70)>>4;
    /* Channel clock select: */ 
    PWMCLK |= ConfigPtr->PWM_PWMHW.CLOCK_SELECT;

    /* Channel alignment */ 
    /* each bit, 1->centre aligned, 0->left aligned*/
    PWMCAE |= ConfigPtr->PWM_PWMHW.CHANNEL_ALIGNED;

    /*Initialise polarity register so that by dafault PWM starts  with
    active state*/ /* configure only used channels */
    PWMPOL |= ConfigPtr->PWM_PWMHW.POLARITY;

    for(index=0;index<PWM_PWMHW_CONFIGURED_CHANNELS;index++)
    {
        /* Take the actual channel. For 8 bit channel the value will be 0x00,
        0x01,0x02,0x03 so on. 
        For 16 bit channels the value will be 0x10,0x12,0x14,0x16 */
        actual_channel=Pwm_MapPwmIndex[index];

            /*Initialise pwm period for all channels*/
            (*(Pwm_Base_Period_Reg+actual_channel))=(uint8)
                         ConfigPtr->PWM_PWMHW.CHANNEL[index].PERIOD_DEFAULT;
            /*Initialise pwm duty cycle for all channels*/
            if(ConfigPtr->PWM_PWMHW.CHANNEL[index].DUTYCYCLE_DEFAULT <
                                                         PWM_HUNDRED_PERCENT)
            {
                 DutyCycle =ConfigPtr->PWM_PWMHW.CHANNEL[index].\
                            DUTYCYCLE_DEFAULT;
                 DutyCycle *=(uint32)(*(Pwm_Base_Period_Reg+actual_channel));
                 (*(Pwm_Base_Duty_Reg+actual_channel))= (uint8)
                            ((DutyCycle<<1)>>16);
            }
            else
            {
                (*(Pwm_Base_Duty_Reg+actual_channel))=
                                (*(Pwm_Base_Period_Reg+actual_channel));
            }
    }/* End for(index=0;index<PWM_PWMHW_CONFIGURED_CHANNELS;index++)*/
    /* Channel used -i.e.enabled channels*/
    PWME |= PWM_CHANNEL_USED_PWM;
    
    /******************* Configure ECT hardware channels *********************/
    DisableAllInterrupts();
    /* Take the configuration number */
    Pwm_Confg_Count =  ConfigPtr - Pwm_Config;
    /* Update the port Reg PTT with the IDLE value */
    PTT  = ((~PWM_CHANNEL_USED_ECT)&Port_PTT_Level) | Pwm_ECT_IDLE[Pwm_Confg_Count]; 
    for(index=0;index<PWM_ECTHW_CONFIGURED_CHANNELS;index++) 
    {
        Pwm_Ect_Period[index]=
                            ConfigPtr->PWM_ECTHW.CHANNEL[index].PERIOD_DEFAULT;
        /* Calculate ON time */
        if(ConfigPtr->PWM_ECTHW.CHANNEL[index].DUTYCYCLE_DEFAULT < 
                                                     PWM_HUNDRED_PERCENT)
        {
            DutyCycle =ConfigPtr->PWM_ECTHW.CHANNEL[index].DUTYCYCLE_DEFAULT;
            DutyCycle *=(uint32)Pwm_Ect_Period[index];
            Pwm_Ect_On_Time[index]=(uint16)((DutyCycle<<1)>>16);
        }
        else
        {
            Pwm_Ect_On_Time[index]=Pwm_Ect_Period[index];
        }
        /* Calculate OFF time */
        Pwm_Ect_Off_Time[index]=Pwm_Ect_Period[index]-Pwm_Ect_On_Time[index];
        /* Initialize TCn register */
        (*(Pwm_BaseAddr_TC+index))=(ECT_TCNT+(uint16)Pwm_Ect_On_Time[index]);
        /* Disable all notifications */
        Pwm_Ena_Notif_Ect[index]= MCAL_CLEAR;
    }/* End of for(index=0;index<PWM_ECTHW_CONFIGURED_CHANNELS;index++)*/
    
    /* Save polarity for ISRs */
    Pwm_Calculated_POLARITY_2.all = Pwm_CfgPtr->PWM_ECTHW.POLARITY_2;
    Pwm_Calculated_POLARITY_1.all = Pwm_CfgPtr->PWM_ECTHW.POLARITY_1;
    
    /* Initialize ECT_TCTL1 and ECT_TCTL2 for polarity */

    /* only init used channels */
    ECT_TCTL2 |= ConfigPtr->PWM_ECTHW.POLARITY_2;
    ECT_TCTL1 |= ConfigPtr->PWM_ECTHW.POLARITY_1;

    /* Initialize TIOS register */
    ECT_TIOS |= PWM_CHANNEL_USED_ECT;
    /* Initialize TIE register */
    ECT_TIE |= PWM_CHANNEL_USED_ECT;
    
    EnableAllInterrupts();

}
/**************************************************************************
* Function Name              : Pwm_SetDutyCycle
*
* Arguments                  : ChannelNumber & DutyCycle    
*                              valid ChannelNumber -> 0 to 15
*                              valid DutyCycle -> 0x0000 to 0x8000
*                              anything above 0x8000 will be considered
*                              as 0x8000.           
* Description                : This service will set the duty cycle of the 
                               PWM channel. 
*
* Return Type                : void
*
* Requirement numbers        : PWM047, PWM051, PWM044, PWM013, PWM014, PWM015,
                               PWM016, PWM059
******************************************************************************/


void Pwm_SetDutyCycle(Pwm_ChannelType ChannelNumber,uint16 DutyCycle)
{
    uint8 actual_channel;
    uint8 ect_logical=0;
    volatile uint8 Check_Var=0;

    if(ChannelNumber>=PWM_PWMHW_CONFIGURED_CHANNELS)
    {
        ect_logical=ChannelNumber-PWM_PWMHW_CONFIGURED_CHANNELS;
        actual_channel= Pwm_MapEctIndex[ect_logical];
    }
    else
    {
        actual_channel=Pwm_MapPwmIndex[ChannelNumber];
    }

    if(ChannelNumber>=PWM_PWMHW_CONFIGURED_CHANNELS)
    {
        /* DutyCycle > 0x8000 is considered as 100% */
        if(DutyCycle > 0x8000)
        {
            DutyCycle = 0x8000;
        }
        
        /* If duty is not 0% or 100% then enable the respective interrupt */
        if(((DutyCycle > 0)&&(DutyCycle < 0x8000))||((DutyCycle == 0)&&\
        (0 != Pwm_Ect_On_Time[ect_logical])) 
        ||((DutyCycle == 0x8000)&&(0 != Pwm_Ect_Off_Time[ect_logical]))) 
        {
            Check_Var = ON;
        } 
        
        /*
        In order to achieve the desired polarity in the next period
        it is required to update the OL and OM bit for 0% and 100% duty in 
        this API itself for respective channels.
        By this we ensure that the signal is as expected from the next
        cycle itself 
        */
        if(DutyCycle == 0)
        {
            if(actual_channel<4)
            {
                ECT_TCTL2 = (ECT_TCTL2 & ~(1u<<(actual_channel*2))|
                (2u<<(actual_channel*2)))|(~(Pwm_CfgPtr->PWM_ECTHW.POLARITY_2)&
                (1u<<(actual_channel*2))|(2u<<(actual_channel*2)));
            }
            else
            {
                ECT_TCTL1 = (ECT_TCTL1 & ~(1u<<((actual_channel-4)*2))|
                (2u<<((actual_channel-4)*2)))|
                (~(Pwm_CfgPtr->PWM_ECTHW.POLARITY_1)&
                (1u<<((actual_channel-4)*2))|(2u<<((actual_channel-4)*2)));
            } 
        }

        else if((DutyCycle == 0x8000)||((DutyCycle != 0)&&(0 == Pwm_Ect_On_Time[ect_logical])))
        {
            if(actual_channel<4)
            {
                ECT_TCTL2 = (ECT_TCTL2 & ~(1u<<(actual_channel*2))|
                (2u<<(actual_channel*2)))|((Pwm_CfgPtr->PWM_ECTHW.POLARITY_2)&
                (1u<<(actual_channel*2))|(2u<<(actual_channel*2)));
            }
            else
            {
                ECT_TCTL1 = (ECT_TCTL1 & ~(1u<<((actual_channel-4)*2))|
                (2u<<((actual_channel-4)*2)))|
                ((Pwm_CfgPtr->PWM_ECTHW.POLARITY_1)&
                (1u<<((actual_channel-4)*2))|(2u<<((actual_channel-4)*2)));
            } 
        }
        
        DisableAllInterrupts();       
        
        /* Update the TCx reg with TCNT and Period value*/
        if(((DutyCycle != 0)&&(0 == Pwm_Ect_On_Time[ect_logical])) 
        ||((DutyCycle != 0x8000)&&(0 == Pwm_Ect_Off_Time[ect_logical])))
        {
            (*(Pwm_BaseAddr_TC+actual_channel)) = 
                                      ECT_TCNT + Pwm_Ect_Period[ect_logical];
        }
               
        /* The following method gives faster and compacter 
        code as S12 has no barrel shifter*/
        Pwm_Ect_On_Time[ect_logical]=(uint16)
            ((((uint32)DutyCycle*(uint32)Pwm_Ect_Period[ect_logical])<<1)>>16);
        Pwm_Ect_Off_Time[ect_logical]=(uint16)
                    (Pwm_Ect_Period[ect_logical]-Pwm_Ect_On_Time[ect_logical]);
        
        /* Treatment for
        0% => switch to idle with next compare, then deactivate IR
        100% => switch to active with next compare, then deactivate IR
        */
        if(ON == Check_Var ) 
        {
            /* Enable interrupts*/
            ECT_TIE |= ((uint8)(1<<actual_channel));
            Check_Var = OFF;
        }
        EnableAllInterrupts();
        
    }
    else 
    {
        /* Write PWM registers to update dutycycle */
            if(DutyCycle < PWM_HUNDRED_PERCENT)/* If DutyCycle < 100% */
            {
               (*(Pwm_Base_Duty_Reg+actual_channel))=(uint8)
               ((((uint32)DutyCycle*(uint32)(*(Pwm_Base_Period_Reg+\
               actual_channel)))<<1)>>16);
            }
            else  /* If DutyCycle >= 100% */
            {
                (*(Pwm_Base_Duty_Reg+actual_channel))=
                                    (*(Pwm_Base_Period_Reg+actual_channel));
            }
    }/* End of  if(ChannelNumber>=PWM_PWMHW_CONFIGURED_CHANNELS) */

}
/*****************************************************************************
* Function Name              : Pwm_SetOutputToIdle
*
* Arguments                  : ChannelNumber
*                              valid ChannelNumber -> 0 to 15
* Description                : This service will set immediately the PWM output 
                               state to the idle state.  
*
* Return Type                : void
*
* Requirement numbers        : PWM047, PWM051, PWM044, PWM045, PWM021, PWM048
******************************************************************************/


void Pwm_SetOutputToIdle(Pwm_ChannelType ChannelNumber) 
{       
    uint8 actual_channel;
    uint8 ect_logical=0;


    if(ChannelNumber>=PWM_PWMHW_CONFIGURED_CHANNELS)
    {
        ect_logical=ChannelNumber-PWM_PWMHW_CONFIGURED_CHANNELS;
        actual_channel=
        Pwm_MapEctIndex[ChannelNumber-PWM_PWMHW_CONFIGURED_CHANNELS];
    }
    else
    {
        actual_channel=Pwm_MapPwmIndex[ChannelNumber];
    }

    /* is channel declared as variable period type? */
    if((ChannelNumber>=PWM_PWMHW_CONFIGURED_CHANNELS)&&
       (PWM_VARIABLE_PERIOD== 
              ((Pwm_CfgPtr->PWM_ECTHW.CHANNEL_CLASS>>actual_channel)&1)))
    {
        if(actual_channel<4)
        {
            ECT_TCTL2 = (ECT_TCTL2 & ~(1u<<(actual_channel*2u))|
            (2u<<(actual_channel*2u)))|((Pwm_CfgPtr->PWM_ECTHW.IDLE_STATE_2)&
            (1u<<(actual_channel*2u))|(2u<<(actual_channel*2u)));
        }
        else
        {
            ECT_TCTL1 = (ECT_TCTL1 & ~(1u<<((actual_channel-4)*2u))|
            (2u<<((actual_channel-4)*2u)))|((Pwm_CfgPtr->PWM_ECTHW.IDLE_STATE_1)&
            (1u<<((actual_channel-4)*2u))|(2u<<((actual_channel-4)*2u)));
        }

        DisableAllInterrupts();
        Pwm_Ect_On_Time[ect_logical]=0;
        Pwm_Ect_Off_Time[ect_logical]=Pwm_Ect_Period[ect_logical];
        /* To get immediate response */
        ECT_CFORC |=((uint8)(1u<<actual_channel));
        /*Disable PWM (ECT HW) interrupts */
        ECT_TIE &= (~(uint8)(1u<<actual_channel));
        /* Disable PWM (ECT HW) Channel */
        ECT_TIOS &= (~(uint8)(1u<<actual_channel));
        EnableAllInterrupts();
    }
    else
    {
        if(PWM_VARIABLE_PERIOD==
                  ((Pwm_CfgPtr->PWM_PWMHW.CHANNEL_CLASS>>actual_channel)&1))
        {
            if(TRUE==((Pwm_CfgPtr->PWM_PWMHW.IDLE_STATE>>actual_channel)&1))
            {
                /* Set the output levels of the channel to Idle state */
                *(Pwm_Base_Period_Reg+actual_channel)=1;
                *(Pwm_Base_Duty_Reg+actual_channel)=
                            ((PWMPOL>>actual_channel)&1);
            }
            else
            {
                /* Set the output levels of the channel to Idle state */
                *(Pwm_Base_Period_Reg+actual_channel)=1;
                *(Pwm_Base_Duty_Reg+actual_channel)=
                                            (((PWMPOL>>actual_channel)&1)?0:1);
            }
            /* PWM021: Update PWMCNTx register to get immediate effect */
            *((Pwm_Base_Cntr_Reg+actual_channel)) = MCAL_CLEAR;
        }
    }

}


/*****************************************************************************
* Function Name              : Pwm_GetOutputState
*
* Arguments                  : ChannelNumber
*                              valid ChannelNumber -> 0 to 15
* Description                : This service will read the PWM output signal 
                               level and return the same. 
*                              
* Return Type                : Pwm_OutputStateType
*
* Requirement numbers        : PWM047, PWM051, PWM044, PWM022
******************************************************************************/


Pwm_OutputStateType Pwm_GetOutputState(Pwm_ChannelType ChannelNumber) 
{                
    uint8 output_state=PWM_LOW;
    uint8 actual_channel;


    if(ChannelNumber>=PWM_PWMHW_CONFIGURED_CHANNELS)
    {
        actual_channel=
        Pwm_MapEctIndex[ChannelNumber-PWM_PWMHW_CONFIGURED_CHANNELS];
    }
    else
    {
        actual_channel=Pwm_MapPwmIndex[ChannelNumber];
    }

    if(ChannelNumber>=PWM_PWMHW_CONFIGURED_CHANNELS)
    {
        /* Read ECT registers */   
        output_state=((PTIT>>(actual_channel))&1);
    }
    else
    {
            /* Read PWM registers to get output level of the particular 
            channel */
            /* is output level active? */
            if((*(Pwm_Base_Cntr_Reg+actual_channel))<
               (*(Pwm_Base_Duty_Reg+actual_channel))&&
                 (TRUE==((Pwm_CfgPtr->PWM_PWMHW.POLARITY>>actual_channel)&1)))
            {
                output_state = PWM_HIGH;
            }
            else if((*(Pwm_Base_Cntr_Reg+actual_channel))>=
               (*(Pwm_Base_Duty_Reg+actual_channel))&&
                 (FALSE==((Pwm_CfgPtr->PWM_PWMHW.POLARITY>>actual_channel)&1)))
            {
                output_state = PWM_HIGH;
            }        
    } /* End of if(ChannelNumber>=PWM_PWMHW_CONFIGURED_CHANNELS) */

    return (output_state);

}
/**************************************************************************
* Function Name              : Pwm_DisableNotification
*
* Arguments                  : ChannelNumber
*                              valid ChannelNumber -> 0 to 15
* Description                : This service will disable the PWM signal edge 
                               notification. 
*
* Return Type                : void
*
* Requirement numbers        : PWM047, PWM051, PWM044, PWM023
******************************************************************************/


void Pwm_DisableNotification(Pwm_ChannelType ChannelNumber) 
{                

    Pwm_Ena_Notif_Ect[ChannelNumber-PWM_PWMHW_CONFIGURED_CHANNELS]= MCAL_CLEAR;

}
/***************************************************************************
* Function Name              : Pwm_EnableNotification
*
* Arguments                  : ChannelNumber & Notification
*                              valid ChannelNumber -> 0 to 15
*                              valid Notification -> 1,2&3
* Description                : This service will enable the PWM signal edge 
                               notification according to Notification 
                               parameter. 
*
* Return Type                : void
*
* Requirement numbers        : PWM047, PWM051, PWM044, PWM024
******************************************************************************/


void Pwm_EnableNotification(Pwm_ChannelType ChannelNumber,
                               Pwm_EdgeNotificationType Notification)
{

    Pwm_Ena_Notif_Ect[ChannelNumber-PWM_PWMHW_CONFIGURED_CHANNELS]=
                                                           (uint8)Notification;

}
/*****************************************************************************
* Function Name              : Pwm_GetVersionInfo
*
* Arguments                  : Std_VersionInfoType *versioninfo 
*
* Description                : This API stores the version information of the 
                               PWM module in the variable pointed by the pointer
                               passed to the function. The version information 
                               include
                               1.Module ID
                               2.Version ID
                               3.Vendor specific version numbers.
*
* Return Type                : void
*
* Requirement numbers        : PWM068
******************************************************************************/


void Pwm_GetVersionInfo(Std_VersionInfoType *versioninfo)
{

    if (NULL_PTR != versioninfo)
    {
        /* Copy MODULE ID */
        versioninfo->moduleID=PWM_MODULE_ID;
        /* Copy VENDOR ID */
        versioninfo->vendorID=PWM_VENDOR_ID;
        /* Cope SW vendor version info. */
        versioninfo->sw_major_version=PWM_SW_MAJOR_VERSION_C;
        versioninfo->sw_minor_version=PWM_SW_MINOR_VERSION_C;
        versioninfo->sw_patch_version=PWM_SW_PATCH_VERSION_C;
}

}
                                                
#pragma CODE_SEG __NEAR_SEG NON_BANKED
/*..........................ISR Definitions..................................*/
/******************************************************************************
* Function Name          : Pwm_TIM_Channel_3_Isr
*
* Arguments              : void
*
* Description            : ISR for PWM output on TIM Channel 3:
*                          1) Saves TIE reg. and disables all TIM interrupts
*                          2) Clears interrupt flag
*                          3) Loads ON/OFF values onto TCn register.
*                          4) Re-enables interrupts
*
* Return Type            : void
*
* Requirement numbers    : N.A.
*
******************************************************************************/
void  Pwm_TIM_Channel_3_Isr(void)
{
    /* Clear channel flag in ECT_TFLG1 reg */
    ECT_TFLG1 =MCAL_BIT3_MASK;
 
    /* Load ECT_TCTL2,TCn registers */
    if(0==Pwm_Ect_Off_Time[PWM_ECT_CH_3]) /* 100% duty cycle */
    {
        /* update status variable */
        Pwm_On_Off_Current_Stat.all |= 0x08;
        /* Disable interrupt */
        ECT_TIE_C3I = 0;
    }
    else if(0==Pwm_Ect_On_Time[PWM_ECT_CH_3])/* 0% duty cycle */
    {
        /* update status variable */
        Pwm_On_Off_Current_Stat.all = (Pwm_On_Off_Current_Stat.all & 0xF7);
        /* Disable interrupt */
        ECT_TIE_C3I = 0;
    }
    else
    {
        /* Load TCn register */
        if(Pwm_Calculated_POLARITY_2.bit.b6==ECT_TCTL2_OL3)
        {
            /* Load ON time */
            ECT_TC3 +=(uint16)Pwm_Ect_On_Time[PWM_ECT_CH_3];
            /* update status variable */
            Pwm_On_Off_Current_Stat.all |= 0x08;
            ECT_TCTL2_OL3= ~Pwm_Calculated_POLARITY_2.bit.b6;
        }
        else
        {
            /* Load OFF time */
            ECT_TC3 +=(uint16)Pwm_Ect_Off_Time[PWM_ECT_CH_3];
            /* update status variable */
            Pwm_On_Off_Current_Stat.all = (Pwm_On_Off_Current_Stat.all & 0xF7);
            ECT_TCTL2_OL3= Pwm_Calculated_POLARITY_2.bit.b6;
        }
    }/* if(0==Pwm_Ect_Off_Time[PWM_ECT_CH_3]) */

    /* Check if callback pointer is not a NULL_PTR */
    if(NULL_PTR != Pwm_CfgPtr->PWM_ECTHW.NOTIFICATION[PWM_ECT_CH_3])
    {
        if((uint8)PWM_BOTH_EDGES == Pwm_Ena_Notif_Ect[PWM_ECT_CH_3])
        {
            /* Call Notification function */
            (Pwm_CfgPtr->PWM_ECTHW.NOTIFICATION[PWM_ECT_CH_3])();
        }
        else if((uint8)PWM_RISING_EDGE==Pwm_Ena_Notif_Ect[PWM_ECT_CH_3])
        {
            /* is Default Polarity high &TCn delay loaded for ON time? */
            if((TRUE==Pwm_On_Off_Current_Stat.bit.b3)&&
               (TRUE==Pwm_Calculated_POLARITY_2.bit.b6))
            {
                /* Call Notification function */
                (Pwm_CfgPtr->PWM_ECTHW.NOTIFICATION[PWM_ECT_CH_3])();
            }
        }
        else if((uint8)PWM_FALLING_EDGE==Pwm_Ena_Notif_Ect[PWM_ECT_CH_3])
        {
            /* is Default Polarity high &TCn delay loaded for OFF time? */
            if((FALSE==Pwm_On_Off_Current_Stat.bit.b3)&&
               (TRUE==Pwm_Calculated_POLARITY_2.bit.b6))
            {
                /* Call Notification function */
                (Pwm_CfgPtr->PWM_ECTHW.NOTIFICATION[PWM_ECT_CH_3])();
            }
        }/* End if(PWM_BOTH_EDGES == Pwm_Ena_Notif_Ect[PWM_ECT_CH_3]) */
    }/*Endif(NULL_PTR!=Pwm_CfgPtr->PWM_ECTHW.NOTIFICATION[PWM_ECT_CH_3]) */
}
/******************************************************************************
* Function Name          : Pwm_TIM_Channel_4_Isr
*
* Arguments              : void
*
* Description            : ISR for PWM output on TIM Channel 4:
*                          1) Saves TIE reg. and disables all TIM interrupts
*                          2) Clears interrupt flag
*                          3) Loads ON/OFF values onto TCn register.
*                          4) Re-enables interrupts
*
* Return Type            : void
*
* Requirement numbers    : N.A.
*
******************************************************************************/
void  Pwm_TIM_Channel_4_Isr(void)
{
    /* Clear channel flag in ECT_TFLG1 reg */
    ECT_TFLG1 =MCAL_BIT4_MASK;
    /* Load ECT_TCTL1,TCn registers */
    if(0==Pwm_Ect_Off_Time[PWM_ECT_CH_4]) /* 100% duty cycle */
    {
        /* update status variable */
        Pwm_On_Off_Current_Stat.all |= 0x10;
        /* Disable interrupt */
        ECT_TIE_C4I = 0;
    }
    else if(0==Pwm_Ect_On_Time[PWM_ECT_CH_4])/* 0% duty cycle */
    {
        /* update status variable */
        Pwm_On_Off_Current_Stat.all = (Pwm_On_Off_Current_Stat.all & 0xEF);
        /* Disable interrupt */
        ECT_TIE_C4I = 0;
    }
    else
    {
        /* Load TCn register */
        if(Pwm_Calculated_POLARITY_1.bit.b0==ECT_TCTL1_OL4)
        {
            /* Load ON time */
            ECT_TC4 +=(uint16)Pwm_Ect_On_Time[PWM_ECT_CH_4];
            /* update status variable */
            Pwm_On_Off_Current_Stat.all |= 0x10;
            ECT_TCTL1_OL4= ~Pwm_Calculated_POLARITY_1.bit.b0;
        }
        else
        {
            /* Load OFF time */
            ECT_TC4 +=(uint16)Pwm_Ect_Off_Time[PWM_ECT_CH_4];
            /* update status variable */
            Pwm_On_Off_Current_Stat.all = (Pwm_On_Off_Current_Stat.all & 0xEF);
            ECT_TCTL1_OL4= Pwm_Calculated_POLARITY_1.bit.b0;
        }
    }/* if(0==Pwm_Ect_Off_Time[PWM_ECT_CH_4]) */

    /* Check if callback pointer is not a NULL_PTR */
    if(NULL_PTR != Pwm_CfgPtr->PWM_ECTHW.NOTIFICATION[PWM_ECT_CH_4])
    {
        if((uint8)PWM_BOTH_EDGES == Pwm_Ena_Notif_Ect[PWM_ECT_CH_4])
        {
            /* Call Notification function */
            (Pwm_CfgPtr->PWM_ECTHW.NOTIFICATION[PWM_ECT_CH_4])();
        }
        else if((uint8)PWM_RISING_EDGE==Pwm_Ena_Notif_Ect[PWM_ECT_CH_4])
        {
            /* is Default Polarity high &TCn delay loaded for ON time? */
            if((TRUE==Pwm_On_Off_Current_Stat.bit.b4)&&
               (TRUE==Pwm_Calculated_POLARITY_1.bit.b0))
            {
                /* Call Notification function */
                (Pwm_CfgPtr->PWM_ECTHW.NOTIFICATION[PWM_ECT_CH_4])();
            }
        }
        else if((uint8)PWM_FALLING_EDGE==Pwm_Ena_Notif_Ect[PWM_ECT_CH_4])
        {
            /* is Default Polarity high &TCn delay loaded for OFF time? */
            if((FALSE==Pwm_On_Off_Current_Stat.bit.b4)&&
               (TRUE==Pwm_Calculated_POLARITY_1.bit.b0))
            {
                /* Call Notification function */
                (Pwm_CfgPtr->PWM_ECTHW.NOTIFICATION[PWM_ECT_CH_4])();
            }
        }/* End if(PWM_BOTH_EDGES == Pwm_Ena_Notif_Ect[PWM_ECT_CH_4]) */
    }/*Endif(NULL_PTR!=Pwm_CfgPtr->PWM_ECTHW.NOTIFICATION[PWM_ECT_CH_4]) */
}
/******************************************************************************
* Function Name          : Pwm_TIM_Channel_5_Isr
*
* Arguments              : void
*
* Description            : ISR for PWM output on TIM Channel 5:
*                          1) Saves TIE reg. and disables all TIM interrupts
*                          2) Clears interrupt flag
*                          3) Loads ON/OFF values onto TCn register.
*                          4) Re-enables interrupts
*
* Return Type            : void
*
* Requirement numbers    : N.A.
*
******************************************************************************/
void  Pwm_TIM_Channel_5_Isr(void)
{
    /* Clear channel flag in ECT_TFLG1 reg */
    ECT_TFLG1 =MCAL_BIT5_MASK;
    /* Load ECT_TCTL1,TCn registers */
    if(0==Pwm_Ect_Off_Time[PWM_ECT_CH_5]) /* 100% duty cycle */
    {
        /* update status variable */
        Pwm_On_Off_Current_Stat.all |= 0x20;
        /* Disable interrupt */
        ECT_TIE_C5I = 0;
    }
    else if(0==Pwm_Ect_On_Time[PWM_ECT_CH_5])/* 0% duty cycle */
    {
        /* update status variable */
        Pwm_On_Off_Current_Stat.all = (Pwm_On_Off_Current_Stat.all & 0xDF);
        /* Disable interrupt */
        ECT_TIE_C5I = 0;
    }
    else
    {
        /* Load TCn register */
        if(Pwm_Calculated_POLARITY_1.bit.b2==ECT_TCTL1_OL5)
        {
            /* Load ON time */
            ECT_TC5 +=(uint16)Pwm_Ect_On_Time[PWM_ECT_CH_5];
            /* update status variable */
            Pwm_On_Off_Current_Stat.all |= 0x20;
            ECT_TCTL1_OL5= ~Pwm_Calculated_POLARITY_1.bit.b2;
        }
        else
        {
            /* Load OFF time */
            ECT_TC5 +=(uint16)Pwm_Ect_Off_Time[PWM_ECT_CH_5];
            /* update status variable */
            Pwm_On_Off_Current_Stat.all = (Pwm_On_Off_Current_Stat.all & 0xDF);
            ECT_TCTL1_OL5= Pwm_Calculated_POLARITY_1.bit.b2;
        }
    }/* if(0==Pwm_Ect_Off_Time[PWM_ECT_CH_5]) */

    /* Check if callback pointer is not a NULL_PTR */
    if(NULL_PTR != Pwm_CfgPtr->PWM_ECTHW.NOTIFICATION[PWM_ECT_CH_5])
    {
        if((uint8)PWM_BOTH_EDGES == Pwm_Ena_Notif_Ect[PWM_ECT_CH_5])
        {
            /* Call Notification function */
            (Pwm_CfgPtr->PWM_ECTHW.NOTIFICATION[PWM_ECT_CH_5])();
        }
        else if((uint8)PWM_RISING_EDGE==Pwm_Ena_Notif_Ect[PWM_ECT_CH_5])
        {
            /* is Default Polarity high &TCn delay loaded for ON time? */
            if((TRUE==Pwm_On_Off_Current_Stat.bit.b5)&&
               (TRUE==Pwm_Calculated_POLARITY_1.bit.b2))
            {
                /* Call Notification function */
                (Pwm_CfgPtr->PWM_ECTHW.NOTIFICATION[PWM_ECT_CH_5])();
            }
        }
        else if((uint8)PWM_FALLING_EDGE==Pwm_Ena_Notif_Ect[PWM_ECT_CH_5])
        {
            /* is Default Polarity high &TCn delay loaded for OFF time? */
            if((FALSE==Pwm_On_Off_Current_Stat.bit.b5)&&
               (TRUE==Pwm_Calculated_POLARITY_1.bit.b2))
            {
                /* Call Notification function */
                (Pwm_CfgPtr->PWM_ECTHW.NOTIFICATION[PWM_ECT_CH_5])();
            }
        }/* End if(PWM_BOTH_EDGES == Pwm_Ena_Notif_Ect[PWM_ECT_CH_5]) */
    }/*Endif(NULL_PTR!=Pwm_CfgPtr->PWM_ECTHW.NOTIFICATION[PWM_ECT_CH_5]) */
}
/******************************************************************************
* Function Name          : Pwm_TIM_Channel_6_Isr
*
* Arguments              : void
*
* Description            : ISR for PWM output on TIM Channel 6:
*                          1) Saves TIE reg. and disables all TIM interrupts
*                          2) Clears interrupt flag
*                          3) Loads ON/OFF values onto TCn register.
*                          4) Re-enables interrupts
*
* Return Type            : void
*
* Requirement numbers    : N.A.
*
******************************************************************************/
void  Pwm_TIM_Channel_6_Isr(void)
{
    /* Clear channel flag in ECT_TFLG1 reg */
    ECT_TFLG1 =MCAL_BIT6_MASK;
    /* Load ECT_TCTL2,TCn registers */
    if(0==Pwm_Ect_Off_Time[PWM_ECT_CH_6]) /* 100% duty cycle */
    {
        /* update status variable */
        Pwm_On_Off_Current_Stat.all |= 0x40;
        /* Disable interrupt */
        ECT_TIE_C6I = 0;
    }
    else if(0==Pwm_Ect_On_Time[PWM_ECT_CH_6])/* 0% duty cycle */
    {
        /* update status variable */
        Pwm_On_Off_Current_Stat.all = (Pwm_On_Off_Current_Stat.all & 0xBF);
        /* Disable interrupt */
        ECT_TIE_C6I = 0;
    }
    else
    {
        /* Load TCn register */
        if(Pwm_Calculated_POLARITY_1.bit.b4==ECT_TCTL1_OL6)
        {
            /* Load ON time */
            ECT_TC6 +=(uint16)Pwm_Ect_On_Time[PWM_ECT_CH_6];
            /* update status variable */
            Pwm_On_Off_Current_Stat.all |= 0x40;
            ECT_TCTL1_OL6= ~Pwm_Calculated_POLARITY_1.bit.b4;
        }
        else
        {
            /* Load OFF time */
            ECT_TC6 +=(uint16)Pwm_Ect_Off_Time[PWM_ECT_CH_6];
            /* update status variable */
            Pwm_On_Off_Current_Stat.all = (Pwm_On_Off_Current_Stat.all & 0xBF);
            ECT_TCTL1_OL6= Pwm_Calculated_POLARITY_1.bit.b4;
        }
    }/* if(0==Pwm_Ect_Off_Time[PWM_ECT_CH_6]) */

    /* Check if callback pointer is not a NULL_PTR */
    if(NULL_PTR != Pwm_CfgPtr->PWM_ECTHW.NOTIFICATION[PWM_ECT_CH_6])
    {
        if((uint8)PWM_BOTH_EDGES == Pwm_Ena_Notif_Ect[PWM_ECT_CH_6])
        {
            /* Call Notification function */
            (Pwm_CfgPtr->PWM_ECTHW.NOTIFICATION[PWM_ECT_CH_6])();
        }
        else if((uint8)PWM_RISING_EDGE==Pwm_Ena_Notif_Ect[PWM_ECT_CH_6])
        {
            /* is Default Polarity high &TCn delay loaded for ON time? */
            if((TRUE==Pwm_On_Off_Current_Stat.bit.b6)&&
               (TRUE==Pwm_Calculated_POLARITY_1.bit.b4))
            {
                /* Call Notification function */
                (Pwm_CfgPtr->PWM_ECTHW.NOTIFICATION[PWM_ECT_CH_6])();
            }
        }
        else if((uint8)PWM_FALLING_EDGE==Pwm_Ena_Notif_Ect[PWM_ECT_CH_6])
        {
            /* is Default Polarity high &TCn delay loaded for OFF time? */
            if((FALSE==Pwm_On_Off_Current_Stat.bit.b6)&&
               (TRUE==Pwm_Calculated_POLARITY_1.bit.b4))
            {
                /* Call Notification function */
                (Pwm_CfgPtr->PWM_ECTHW.NOTIFICATION[PWM_ECT_CH_6])();
            }
        }/* End if(PWM_BOTH_EDGES == Pwm_Ena_Notif_Ect[PWM_ECT_CH_6]) */
    }/*Endif(NULL_PTR!=Pwm_CfgPtr->PWM_ECTHW.NOTIFICATION[PWM_ECT_CH_6]) */
}
/******************************************************************************
* Function Name          : Pwm_TIM_Channel_7_Isr
*
* Arguments              : void
*
* Description            : ISR for PWM output on TIM Channel 7:
*                          1) Saves TIE reg. and disables all TIM interrupts
*                          2) Clears interrupt flag
*                          3) Loads ON/OFF values onto TCn register.
*                          4) Re-enables interrupts
*
* Return Type            : void
*
* Requirement numbers    : N.A.
*
******************************************************************************/
void  Pwm_TIM_Channel_7_Isr(void)
{
    /* Clear channel flag in ECT_TFLG1 reg */
    ECT_TFLG1 =MCAL_BIT7_MASK;
    /* Load ECT_TCTL1,TCn registers */
    if(0==Pwm_Ect_Off_Time[PWM_ECT_CH_7]) /* 100% duty cycle */
    {
        /* update status variable */
        Pwm_On_Off_Current_Stat.all |= 0x80;
        /* Disable interrupt */
        ECT_TIE_C7I = 0;
    }
    else if(0==Pwm_Ect_On_Time[PWM_ECT_CH_7])/* 0% duty cycle */
    {
        /* update status variable */
        Pwm_On_Off_Current_Stat.all = (Pwm_On_Off_Current_Stat.all & 0x7F);
        /* Disable interrupt */
        ECT_TIE_C7I = 0;
    }
    else
    {
        /* Load TCn register */
        if(Pwm_Calculated_POLARITY_1.bit.b6==ECT_TCTL1_OL7)
        {
            /* Load ON time */
            ECT_TC7 +=(uint16)Pwm_Ect_On_Time[PWM_ECT_CH_7];
            /* update status variable */
            Pwm_On_Off_Current_Stat.all |= 0x80;
            ECT_TCTL1_OL7= ~Pwm_Calculated_POLARITY_1.bit.b6;
        }
        else
        {
            /* Load OFF time */
            ECT_TC7 +=(uint16)Pwm_Ect_Off_Time[PWM_ECT_CH_7];
            /* update status variable */
            Pwm_On_Off_Current_Stat.all = (Pwm_On_Off_Current_Stat.all & 0x7F);
            ECT_TCTL1_OL7= Pwm_Calculated_POLARITY_1.bit.b6;
        }
    }/* if(0==Pwm_Ect_Off_Time[PWM_ECT_CH_7]) */

        /* Check if callback pointer is not a NULL_PTR */
        if(NULL_PTR != Pwm_CfgPtr->PWM_ECTHW.NOTIFICATION[PWM_ECT_CH_7])
    {
        if((uint8)PWM_BOTH_EDGES == Pwm_Ena_Notif_Ect[PWM_ECT_CH_7])
        {
            /* Call Notification function */
            (Pwm_CfgPtr->PWM_ECTHW.NOTIFICATION[PWM_ECT_CH_7])();
        }
        else if((uint8)PWM_RISING_EDGE==Pwm_Ena_Notif_Ect[PWM_ECT_CH_7])
        {
            /* is Default Polarity high &TCn delay loaded for ON time? */
            if((TRUE==Pwm_On_Off_Current_Stat.bit.b7)&&
               (TRUE==Pwm_Calculated_POLARITY_1.bit.b6))
            {
                /* Call Notification function */
                (Pwm_CfgPtr->PWM_ECTHW.NOTIFICATION[PWM_ECT_CH_7])();
            }
        }
        else if((uint8)PWM_FALLING_EDGE==Pwm_Ena_Notif_Ect[PWM_ECT_CH_7])
        {
            /* is Default Polarity high &TCn delay loaded for OFF time? */
            if((FALSE==Pwm_On_Off_Current_Stat.bit.b7)&&
               (TRUE==Pwm_Calculated_POLARITY_1.bit.b6))
            {
                /* Call Notification function */
                (Pwm_CfgPtr->PWM_ECTHW.NOTIFICATION[PWM_ECT_CH_7])();
            }
        }/* End if(PWM_BOTH_EDGES == Pwm_Ena_Notif_Ect[PWM_ECT_CH_7]) */
    }/*End if(NULL_PTR!=Pwm_CfgPtr->PWM_ECTHW.NOTIFICATION[PWM_ECT_CH_7])*/
}
#pragma CODE_SEG DEFAULT


#else
   #error " Mismatch in AR Patch Version of Pwm.c & Pwm.h "
#endif

#else
   #error " Mismatch in AR Minor Version of Pwm.c & Pwm.h " 
#endif

#else
   #error " Mismatch in AR Major Version of Pwm.c & Pwm.h " 
#endif

#else
   #error " Mismatch in SW Minor Version of Pwm.c & Pwm.h " 
#endif

#else
   #error " Mismatch in SW Major Version of Pwm.c & Pwm.h " 
#endif

/* End of version check */
   
/*...............................END OF Pwm.c................................*/


