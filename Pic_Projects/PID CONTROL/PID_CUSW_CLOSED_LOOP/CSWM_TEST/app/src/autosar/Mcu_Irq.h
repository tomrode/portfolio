/* Created at 15:25 on 1-4-2009 */
/* Version of HALCoGen used : 2.9.3 */
/* Code generated for       : MC9S12XS128 */
/*************************************************************************/
/* Copyright(c) - This Program/Software is the exclusive property of Infosys.
The software may not be reproduced or given to third parties without prior
consent of Infosys.

Filename                      : Mcu_Irq.h
Version                       : 2.0.0
Microcontroller Platform      : MC9S12XS128
Compiler                      : Codewarrior HCS12X V5.7.0

Description                   : This file contains the declaration of ISRs.

 Requirement Specification     : AUTOSAR_SWS_MCUDriver.pdf version 1.2.0

 Module Design                 : AUTOSAR_DDD_MCU_SPAL2.0.doc version 2.10a
 Platform Dependant[yes/no]    : yes

To be changed by user[yes/no] : no
*/

/******************************************************************************/

/* Revision History

Class of Change
-------------------------------------------------------------------------------
N - New Module.
F - Functional development.
E - Error Correction.
O - Optimization without functional change.
-------------------------------------------------------------------------------
 00      14-Jul-2006  Harini, Infosys
-------------------------------------------------------------------------------
Class: N
Cause: New Module
Detail:This file contains the declaration of ISRs.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 01     11-Aug-2006     Harini, Infosys
-------------------------------------------------------------------------------
Class: E
Cause: New Isrs added
Detail:Clock Monitor and COP are treated as Interrupt 
       for NON OSEK project.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 02     11-Aug-2006     Ajaykumar, Infosys
-------------------------------------------------------------------------------
Class: E
Cause: Clock Monitor and COP as reset vectors
Detail:Clock Monitor and COP are deleted from this file, they are
       now treated as reset vectors
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 03      11-Dec-2006       Anwar Husen,      Infosys
-------------------------------------------------------------------------------
Class: N
Cause: New Module
Detail:This file contains the declaration of ISRs.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 04     26-Mar-2007    Rose Paul , Infosys
-------------------------------------------------------------------------------
Class: E
Cause: DISABLE was defined as 0, but it is also used by the preprocessor
            (e.g. #pragma MESSAGE DISABLE ...).The preprocessor directive
            does not work after the inclusion of MCAL_types.h.
Detail:Macros in Mcal_Types prefixed by "MCAL_".
------------------------------------------------------------------------------- 
-------------------------------------------------------------------------------
 05     29-Mar-2007   Khanindra , Infosys
-------------------------------------------------------------------------------
Class : F
Cause : For Implementation of Freescale security recommendation
Detail: For Implementation of Freescale security recommendation
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
06     02-Apr-2007   Khanindra , Infosys
-------------------------------------------------------------------------------
Class : O
Cause : After Incorporating Code review comments
Detail: After Incorporating Code review comments
-------------------------------------------------------------------------------

********************************************************************************/

#ifndef _MCU_IRQ_H
#define _MCU_IRQ_H

#include "Mcu.h"

#pragma CODE_SEG __NEAR_SEG NON_BANKED

/*............ISR prototypes.................................................*/

interrupt  void near CrgScm_Isr(void);
interrupt  void near CrgPllLck_Isr(void);


#pragma CODE_SEG DEFAULT

#endif

/*............................End of Mcu_Irq.h...............................*/
