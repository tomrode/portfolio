/* Created at 15:24 on 1-4-2009 */
/* Version of HALCoGen used : 2.9.3 */
/* Code generated for       : MC9S12XS128 */
/*****************************************************************************/
/* Copyright(c) - This Program/Software is the exclusive property of Infosys.
   The software may not be reproduced or given to third parties without prior
   consent of Infosys.

   Filename                     : Dio_Cfg.c
   Version                      : 2.1.2
   Microcontroller Platform     : MC9S12XS128
   Compiler                     : Codewarrior HCS12X V5.7.0
   
   Description                  : This file contains the initialization 
                                  for the group structure used in DIO module.
                                  It also  initializes the variables with the 
                                  symbolic names for the configured ports and
                                  channels. 
 
   Requirement Specification    : AUTOSAR_SWS_DIODriver.pdf version 2.0.0
                
   Module Design                : AUTOTMAL_DDD_DIO_SPAL2.0.doc version 1.0.0

   Platform Dependant[yes/no]   : yes
     
   To be changed by user[yes/no]: no
*/
/*****************************************************************************/
/* Revision History

Class of Change
-------------------------------------------------------------------------------
N - New Module.
F - Functional development.
E - Error Correction.
O - Optimization without functional change.
-------------------------------------------------------------------------------
00  02-Feb-2006  Kiran G S, Infosys
-------------------------------------------------------------------------------
Class : N
Cause : New Module
Detail: This file implements the function definitions of DIO driver.
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
01  19-Mar-2006   kiran G S,  Infosys
-------------------------------------------------------------------------------
Class: F
Cause: Functional development
Detail:Upgrade to suit to 2.0.0 DIO specification.
-------------------------------------------------------------------------------
 ------------------------------------------------------------------------------- 
 02  05-Oct-2007   Ajaykumar,  Infosys 
 ------------------------------------------------------------------------------- 
 Class: F 
 Cause : MC9S12XS128 controller support 
 Detail: MC9S12XS128 related pre-processor switches added. 
 ------------------------------------------------------------------------------- 
*/
/*****************************************************************************/


/*...................................File Includes...........................*/

/* Implements this Interface */                
#include "Dio.h"
                                                       

/* Initialisation of variable of type Dio_ChannelGroupType */ 
/* Initialisation of DioConfigData */
const Dio_ChannelGroupType DioConfigData[DIO_MAXGROUPS] = 
                                {                           
                                    { 
                                        SYMBOLIC_PORT_B,
                                        
                                        0,
                                        
                                        7
                                    },
                                                  
                                    { 
                                        SYMBOLIC_PORT_E,
                                        
                                        4,
                                        
                                        112
                                    }
                                                  
                                };
                          

/*--------------------- End of Dio_Cfg.c-------------------------------------*/
