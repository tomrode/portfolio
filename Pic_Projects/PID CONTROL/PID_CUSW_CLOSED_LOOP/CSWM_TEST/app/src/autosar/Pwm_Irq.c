/* Created at 15:25 on 1-4-2009 */
/* Version of HALCoGen used : 2.9.3 */
/* Code generated for       : MC9S12XS128 */
/*****************************************************************************/
/* Copyright(c) - This Program/Software is the exclusive property of Infosys.
The software may not be reproduced or given to third parties without prior
consent of Infosys.

   Filename                        : Pwm_Irq.c
   Version                         : 1.0.3

   Microcontroller Platform        : MC9S12XS128
   Compiler                        : Codewarrior HCS12X V5.7.0

   Description                     : This file contains ISRs for PWM module

   Requirement Specification       : AUTOSAR_SWS_PwmDriver.pdf version 1.0.7

   Module Design                   : AUTOTMAL_DDD_PWM_SPAL2.0.doc version 1.0

   Platform Dependant[yes/no]      : yes

   To be changed by user[yes/no]   : no
*/
/*****************************************************************************/

/* Revision History

Class of Change
-------------------------------------------------------------------------------
N - New Module.
F - Functional development.
E - Error Correction.
O - Optimization without functional change.
-------------------------------------------------------------------------------
 00      14-Jul-2006    Harini, Infosys
-------------------------------------------------------------------------------
Class: N
Cause: New Module
Detail:This file contains ISRs for PWM module
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 01     24-Feb-2007     Khanindra , Infosys
-------------------------------------------------------------------------------
Class: N
Cause: New Module
Detail:This file contains ISRs for PWM module
       MC9S12XEP100 PWM code(Version 1.2.0) is used as baseligned code.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 02     26-Mar-2007     Rose Paul , Infosys
-------------------------------------------------------------------------------
Class: E
Cause: DISABLE was defined as 0, but it is also used by the preprocessor
            (e.g. #pragma MESSAGE DISABLE ...).The preprocessor directive
            does not work after the inclusion of MCAL_types.h.
       Lint warning : ISR prototype
Detail:Macros in Mcal_Types prefixed by "MCAL_".
------------------------------------------------------------------------------- 
******************************************************************************/

/*...................................File Includes...........................*/

#include "Pwm_Irq.h"

#pragma CODE_SEG __NEAR_SEG NON_BANKED

interrupt void near Ect_3_Isr(void)
{
    Pwm_TIM_Channel_3_Isr();
}
interrupt void near Ect_4_Isr(void)
{
    Pwm_TIM_Channel_4_Isr();
}
interrupt void near Ect_5_Isr(void)
{
    Pwm_TIM_Channel_5_Isr();
}
interrupt void near  Ect_6_Isr(void)
{
    Pwm_TIM_Channel_6_Isr();
}
interrupt void near  Ect_7_Isr(void)
{
    Pwm_TIM_Channel_7_Isr();
}

#pragma CODE_SEG DEFAULT

/*.........................End of Pwm_Irq.c..................................*/
