/* Created at 15:24 on 1-4-2009 */
/* Version of HALCoGen used : 2.9.3 */
/* Code generated for       : MC9S12XS128 */
/*****************************************************************************/
/* Copyright(c) - This Program/Software is the exclusive property of Infosys. 
The software may not be reproduced or given to third parties without prior 
consent of Infosys.

   Filename                        : PwmIf_Cbk.h
   Version                         : 2.1.1
   Microcontroller Platform        : MC9S12XS128
   Compiler                        : Codewarrior HCS12X V5.7.0

   Description                     : Header file for PwmIf_Cbk.c
   Requirement Specification       : AUTOSAR_SWS_PwmDriver.doc version 1.0.7
                
   Module Design                   : AUTOTMAL_DDD_PWM_SPAL2.0.doc version 1.0

   Platform Dependant[yes/no]      : yes   
     
   To be changed by user[yes/no]   : no  
*/

/*****************************************************************************/
   
/* Revision History

Class of Change
-------------------------------------------------------------------------------
N - New Module.
F - Functional development.
E - Error Correction.
O - Optimization without functional change.
-------------------------------------------------------------------------------
 00     10.January.05     Muthuvelavan, Infosys
-------------------------------------------------------------------------------
Class: N
Cause: New Module
Detail:Header file for PwmIf_Cbk.c
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 01     17-Apr-06     Muthuvelavan, Infosys
-------------------------------------------------------------------------------
Class: F
Cause: Specification update.
Detail:update for SPAL2.0
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 02     24-Feb-2007     Khanindra , Infosys
-------------------------------------------------------------------------------
Class: N
Cause: New Module
Detail:Header file for PwmIf_Cbk.c
       MC9S12Q96 PWM code(Version 1.2.0) is used as baseligned code.
-------------------------------------------------------------------------------
******************************************************************************/

#ifndef _PWMIF_CBK_H
#define _PWMIF_CBK_H
 
void Pwm_Notification_HFL(void);
void Pwm_Notification_HFR(void);
void Pwm_Notification_HRL(void);
void Pwm_Notification_HRR(void);

#endif   
/*------------- End of File --------------------------------------------------*/
