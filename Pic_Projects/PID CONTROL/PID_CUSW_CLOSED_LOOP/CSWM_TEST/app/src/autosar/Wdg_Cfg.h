/* Created at 15:25 on 1-4-2009 */
/* Version of HALCoGen used : 2.9.3 */
/* Code generated for       : MC9S12XS128 */
//****************************************************************************/
/* Copyright(c) - This Program/Software is the exclusive property of Infosys.
The software may not be reproduced or given to third parties without prior
consent of Infosys.

     Filename                      : Wdg_Cfg.h
     Version                       : 1.0.2
     Microcontroller Platform      :MC9S12XS128
     Compiler                      : Codewarrior HCS12X V5.7.0

     Description                   : Header file for WDG

     Requirement Specification     : AUTOSAR_SWS_Watchdog_Driver.pdf 2.0

     Module Design                 : AUTOTMAL_DDD_WDG_SPAL2.0.doc version 1.00

     Platform Dependant[yes/no]    : yes

     To be changed by user[yes/no] : no
*/
/*****************************************************************************/

/* Revision History

Class of Change
-------------------------------------------------------------------------------
N - New Module.
F - Functional development.
E - Error Correction.
O - Optimization without functional change.
-------------------------------------------------------------------------------
 00      02-Feb-2006     Arindam Gupta, Infosys
-------------------------------------------------------------------------------
Class: N
Cause: New Module
Detail:This is configuration header file for Watchdog Driver.
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
 01      27-Mar-2006     Arindam Gupta, Infosys
-------------------------------------------------------------------------------
Class: N
Cause: Changes done for SPAL 2.0
Detail:The changes are done as per AUTOSAR_SWS_WatchdogDriver.pdf 2.0.
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
 02      03-Jul-2006     Arindam Gupta, Infosys
-------------------------------------------------------------------------------
Class: O
Cause: Added macro check for Trigger function.
Detail:The changes are done as per new requirement from Temic.
-------------------------------------------------------------------------------
 03      16-May-2007     Khanindra, Infosys
-------------------------------------------------------------------------------
Class: F
Cause: WDG_FOPT_CONFIG_VALUE macro included.
Detail:WDG_FOPT_CONFIG_VALUE macro included.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 04      29-Nov-2007     Isha Gupta, Infosys
-------------------------------------------------------------------------------
Class: O
Cause: WDG_DEFAULT_MODE macro included.
Detail:WDG_DEFAULT_MODE macro included for PRL #853.
-------------------------------------------------------------------------------


******************************************************************************/
/* to avoid Multiple inclusion */
#ifndef _WDG_CFG_H
#define _WDG_CFG_H

/*...................................Macros..................................*/


/*...................Configuration Description File..........................*/

/* Define if development error has to be reported */
#define WDG_DEV_ERROR_DETECT 			OFF

/* Define if production error has to be reported */
#define WDG_DEM_ERROR_DETECT 			OFF


#define WDG_DISABLE_ALLOWED                               OFF

/* Maximum number of configuration structures */
#define WDG_MAX_NUMBER_OF_CONFIG    1

/* Configuration set names */
#define WDG_config0 0


#define WDG_TRIGGER_MACRO                               OFF
#define WDG_FOPT_CONFIG_VALUE                             0xF9FE

#define WDG_SETTINGS_SLOW                                 0x06

#define WDG_SETTINGS_FAST                                 0x02

#define WDG_SETTINGS_OFF                                  0x00


#define WDG_DEFAULT_MODE                                  WDG_SLOW_MODE

#endif/* end _WDG_CFG_H */

/*.............................END OF Wdg_Cfg.h..............................*/

