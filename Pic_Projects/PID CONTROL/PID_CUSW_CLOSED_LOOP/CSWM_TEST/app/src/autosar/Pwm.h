/* Created at 15:24 on 1-4-2009 */
/* Version of HALCoGen used : 2.9.3 */
/* Code generated for       : MC9S12XS128 */
/*****************************************************************************/
/* Copyright(c) - This Program/Software is the exclusive property of Infosys. 
The software may not be reproduced or given to third parties without prior 
consent of Infosys.

   Filename                        : Pwm.h
   Version                         : 3.4.5
   Microcontroller Platform        : MC9S12XS128
   Compiler                        : Codewarrior HCS12X V5.7.0

   Description                     : Header file for Pwm module driver
   Requirement Specification       : AUTOSAR_SWS_PwmDriver.doc version 1.0.7
                
   Module Design                   : AUTOTMAL_DDD_PWM_SPAL2.0.doc version 1.0

   Platform Dependant[yes/no]      : yes   
     
   To be changed by user[yes/no]   : no  
*/

/*****************************************************************************/ 
  
/* Revision History

Class of Change
-------------------------------------------------------------------------------
N - New Module.
F - Functional development.
E - Error Correction.
O - Optimization without functional change.
-------------------------------------------------------------------------------
 00     02-Jan-2006     Muthuvelavan, Infosys
-------------------------------------------------------------------------------
Class: N
Cause: New Module
Detail:Header file for the PWM module driver.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 01     02-Mar-2006     Muthuvelavan, Infosys
-------------------------------------------------------------------------------
Class: F
Cause: Functional development
Detail:Update for SPAL2.0 specification.
-------------------------------------------------------------------------------
Class: F
Cause: Functional development
Detail:Update for SPAL2.0 specification.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
02  08-Jun-2006   kiran G S,  Infosys
-------------------------------------------------------------------------------
Class:E
Cause:Std_VersionInfoType structure is modified in Std_Types.h 
Code modified for points reported and accepted in Problem report list 
Code modified for points reported and accepted in RTRT anomaly reportlist

Detail:updated functions:
Pwm_SetPeriodAndDuty to check for Center and left allignment.
All Interrupt routines to check for 100% and 0% dutycycles.
Pwm_SetOutputtoIdle and  Pwm_SetPeriodAndDuty,where init complete check
is done before all other checks.
Pwm_GetOutputState function to include all possible combinations.
-------------------------------------------------------------------------------
---------------------------------------------------------------------------------
03  05-Sep-2006   Ajaykumar,  Infosys
-------------------------------------------------------------------------------
Class:E
Cause:Source file updated 

Detail:Source file updated 
-------------------------------------------------------------------------------
---------------------------------------------------------------------------------
04  05-Jan-2007   Kasinath,  Infosys
-------------------------------------------------------------------------------
Class:F
Cause:To be in line with .c file,version no.updated  

Detail:To be in line with .c file,version no.updated  
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 05     24-Feb-2007     Khanindra , Infosys
-------------------------------------------------------------------------------
Class: N
Cause: 16 bit PWM implementation
Detail: Implemented 16 bit PWM and incorporated the review comments for S12Q 
        (Version 1.2.0)   
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
06     26-Mar-2007     Rose Paul , Infosys
-------------------------------------------------------------------------------
Class : E
Cause : Updated .c for PRLs
Detail: Incremented version number
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 07     13-Apr-2007     Khanindra , Infosys
-------------------------------------------------------------------------------
Class: F
Cause: In sync with Pwm.c the verson has updated
Detail: NA
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 08     25-Sep-2007     Khanindra , Infosys
-------------------------------------------------------------------------------
Class: F
Cause: In sync with Pwm.c the verson has updated
Detail: NA
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 09     01-Oct-2007     Khanindra , Infosys
-------------------------------------------------------------------------------
Class: F
Cause: In sync with Pwm.c the verson has updated
Detail: NA
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 10     02-Apr-2008     Khanindra, Infosys
-------------------------------------------------------------------------------
Class: O
Cause: Version updated in sync with the Pwm.c
Detail:Version updated in sync with the Pwm.c
-------------------------------------------------------------------------------

******************************************************************************/
/* To avoid Multiple inclusion */
#ifndef _PWM_H
#define _PWM_H

/*...................................File Includes...........................*/
/* Standard types header file */
/* This header file includes all the standard data types, platform dependent
   header file and common return types */
#include "Std_Types.h"
/* Infosys defines header file */
#include "MCAL_Types.h"
/* Pwm Driver configuration file */
#include "Pwm_Cfg.h"
/* Pwm callback interface file */
#include "PwmIf_Cbk.h"

/*...................................Macros..................................*/
/* Declaring the required macros to perform the version check */
/* SW ->Software vendor version info */
#define PWM_SW_MAJOR_VERSION                  (3)
#define PWM_SW_MINOR_VERSION                  (4)
#define PWM_SW_PATCH_VERSION                  (5)
/* AR ->AUTOSAR version info */
#define PWM_AR_MAJOR_VERSION                  (1)
#define PWM_AR_MINOR_VERSION                  (0)
#define PWM_AR_PATCH_VERSION                  (7)

/* Module ID */
#define PWM_MODULE_ID                           ((uint8)121)

/* Vendor ID */
/* Infosys' Vendor ID  */
#define PWM_VENDOR_ID                           ((uint16)40)

/* Service IDs */
/* Declaring the service ID macros of the PWM Driver service APIs */
#define PWM_INIT_ID                             ((uint8)0)
#define PWM_DEINIT_ID                           ((uint8)1)
#define PWM_SETDUTYCYCLE_ID                     ((uint8)2)
#define PWM_SETPERIODANDDUTY_ID                 ((uint8)3)
#define PWM_SETOUTPUTTOIDLE_ID                  ((uint8)4)
#define PWM_GETOUTPUTSTATE_ID                   ((uint8)5)
#define PWM_DISABLENOTIFICATION_ID              ((uint8)6)
#define PWM_ENABLENOTIFICATION_ID               ((uint8)7)
#define PWM_GETVERSIONINFO_ID                   ((uint8)8)

/* Error IDs */
#define PWM_E_PARAM_CONFIG                      ((uint8)0x10)
#define PWM_E_UNINIT                            ((uint8)0x11)
#define PWM_E_PARAM_CHANNEL                     ((uint8)0x12)
#define PWM_E_PERIOD_UNCHANGEABLE               ((uint8)0x13)

/* channel definitions */
enum
{
    /* for PWM hardware */
    PWM_CHANNEL_1,
    PWM_CHANNEL_2,
    PWM_CHANNEL_3,
    /* for ECT hardware */
    PWM_CHANNEL_11,
    PWM_CHANNEL_12,
    PWM_CHANNEL_13,
    PWM_CHANNEL_14,
    PWM_CHANNEL_15,
 PWM_MAX_CONFIGURED_CHANNELS 
};


/* To define Variable/Fixed period classes */
enum{
    PWM_VARIABLE_PERIOD=0,
    PWM_FIXED_PERIOD
};

/* To define LOW/HIGH PWM outputs */
enum{
    PWM_LOW=0,
    PWM_HIGH
};

/*.........................Type Definitions..................................*/
/* Low Power Modes selection 
   bit 0,1 : unused
   bit2    : 1->Freeze mode enabled 0-> Freeze mode disabled
   bit3    : 1->Wait mode enabled 0-> Wait mode disabled 
   bit 4to7: unused */
typedef uint8 Pwm_ModeType;
/* Channel Type - each PWM channel has 1 bit:
   1b  - channel used 
   0b  - channel not used */
typedef uint8 Pwm_ChannelType;
/* Alignment Type - each PWM channel has 1 bit:
   1b  - Center Aligned output 
   0b  - Left Aligned output */
typedef uint8 Pwm_AlignmentType;
/* Polarity - each PWM channel has 1 bit:
   1b  - each PWM o/p starts with HIGH state
   0b  - each PWM o/p starts with LOW state */
typedef uint8 Pwm_PolarityType;
/* Idle state - each PWM channel has 1 bit:
   1b  - PWM_HIGH 
   0b  - PWM_LOW */
typedef uint8 Pwm_OutputStateType;
/* Channel Class - each PWM channel has 1 bit:
   1b  - Fixed Period type 
   0b  - Variable Period type */
typedef uint8 Pwm_Channel_ClassType;
/* To define data type for Channel Period */
typedef uint16 Pwm_PeriodType;

/* To define data type for Channel Duty Cycle
   0x0000-> 0%, 0x4000-> 50%, 0x8000-> 100% */
typedef uint16  Pwm_DutyCycleType;

/* Edge notification types */
typedef enum{
    PWM_RISING_EDGE=1,
    PWM_FALLING_EDGE,
    PWM_BOTH_EDGES
}Pwm_EdgeNotificationType;

/* Notification callback ptr */
typedef void (*Pwm_CallbackPtrType) (void);

/* type definition for channel configuration */
typedef struct
{
    /* Period scaling: unit: ns 
       Period= Pwm_Period_Default*resolution */
    Pwm_PeriodType          PERIOD_DEFAULT;
    /* Duty cycle scaling: 
       0x0000-> 0%
       0x4000-> 50%
       0x8000-> 100% */
    Pwm_DutyCycleType       DUTYCYCLE_DEFAULT;
    /* Pointers to Interface layer call back functions */
}Pwm_ChannelConfigType;
/* Configuration type of PWM Hardware */
typedef struct
{
    /* Low Power Mode selection */
    Pwm_ModeType            POWER_MODE;
    /* Alignment Type - each PWM channel has 1 bit:
    1b  - Center Aligned output
    0b  - Left Aligned output */
    Pwm_AlignmentType       CHANNEL_ALIGNED;
    /* Polarity - each PWM channel has 1 bit:
    1b  - each PWM o/p starts with HIGH state
    0b  - each PWM o/p starts with LOW state */
    Pwm_PolarityType        POLARITY;
    /* Idle state - each PWM channel has 1 bit:
    1b  - PWM_HIGH
    0b  - PWM_LOW */
    Pwm_OutputStateType     IDLE_STATE;
    /* Channel Class - each PWM channel has 1 bit:
    1b  - Fixed Period type
    0b  - Variable Period type */
    Pwm_Channel_ClassType   CHANNEL_CLASS;
    /* Prescale - Bits0,1,2 are used to store prescaleA info.
    - Bits4,5,6 are used to store prescaleB info.
    - Bits 3&7 are unused */
    uint8                   PRESCALE_B_A;
    /* clock select, 1 bit for each channel
     Bit 0->0:Clock A  1:Clock_SA , Bit 1->0:Clock A  1:Clock SA
     Bit 2->0:Clock B  1:Clock SB , Bit 3->0:Clock B  1:Clock SB
     Bit 4->0:Clock A  1:Clock SA , Bit 5->0:Clock A  1:Clock SA
     Bit 6->0:Clock B  1:Clock SB , Bit 7->0:Clock B  1:Clock SB */
    uint8                   CLOCK_SELECT;
    Pwm_ChannelConfigType   CHANNEL[PWM_PWMHW_CONFIGURED_CHANNELS];
}Pwm_PWMHW_ConfigType;

/* Configuration type of ECT Hardware */
typedef struct
{
    /* Polarity - each PWM channel has 1 bit:
    1b  - each PWM o/p starts with HIGH state
    0b  - each PWM o/p starts with LOW state */
    /* For TCTL1 */
    Pwm_PolarityType        POLARITY_1;
    /* For TCTL2 */
    Pwm_PolarityType        POLARITY_2;

    /* Idle state - each PWM channel has 1 bit:
    1b  - PWM_HIGH 
    0b  - PWM_LOW */
    /* For TCTL1 */
    Pwm_OutputStateType     IDLE_STATE_1;
    /* For TCTL2 */
    Pwm_OutputStateType     IDLE_STATE_2;

    /* Channel Class - each PWM channel has 1 bit:
    1b  - Fixed Period type 
    0b  - Variable Period type */
    Pwm_Channel_ClassType   CHANNEL_CLASS;

    /* Pointers to Interface layer call back functions */
    Pwm_CallbackPtrType     NOTIFICATION[PWM_ECTHW_CONFIGURED_CHANNELS];

    Pwm_ChannelConfigType   CHANNEL[PWM_ECTHW_CONFIGURED_CHANNELS];
}Pwm_ECTHW_ConfigType;

/* Configuration type of PWM Driver */
typedef struct {
    Pwm_PWMHW_ConfigType    PWM_PWMHW;
    Pwm_ECTHW_ConfigType    PWM_ECTHW;  
}Pwm_ConfigType;

/*.........................Global Variable declarations......................*/
extern  const Pwm_ConfigType Pwm_Config[PWM_MAX_CONFIG];

/*.........................Global Function Prototypes........................*/
/* Declaration of functions which are used by other modules */
extern  void    Pwm_Init(const Pwm_ConfigType *ConfigPtr);
/* MODIFIED function declaration to far */
extern @far void Pwm_SetDutyCycle(Pwm_ChannelType ChannelNumber,
                                 uint16 DutyCycle);
extern @far void Pwm_SetOutputToIdle(Pwm_ChannelType ChannelNumber);
extern @far Pwm_OutputStateType Pwm_GetOutputState(Pwm_ChannelType ChannelNumber);
extern @far void Pwm_DisableNotification(Pwm_ChannelType ChannelNumber);
extern @far void Pwm_EnableNotification(Pwm_ChannelType ChannelNumber,
                                       Pwm_EdgeNotificationType Notification);
extern  void    Pwm_GetVersionInfo(Std_VersionInfoType *versioninfo);

#pragma CODE_SEG __NEAR_SEG NON_BANKED
/*............ISR prototypes.................................................*/
extern void Pwm_TIM_Channel_3_Isr(void);
extern void Pwm_TIM_Channel_4_Isr(void);
extern void Pwm_TIM_Channel_5_Isr(void);
extern void Pwm_TIM_Channel_6_Isr(void);
extern void Pwm_TIM_Channel_7_Isr(void);
#pragma CODE_SEG DEFAULT

#endif /* End of #ifndef _PWM_H */

/*...............................END OF Pwm.h................................*/