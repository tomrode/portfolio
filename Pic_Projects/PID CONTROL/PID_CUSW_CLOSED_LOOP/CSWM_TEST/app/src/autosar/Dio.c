/* Created at 15:24 on 1-4-2009 */
/* Version of HALCoGen used : 2.9.3 */
/* Code generated for       : MC9S12XS128 */

/*****************************************************************************/
/* Copyright(c) - This Program/Software is the exclusive property of Infosys.
   The software may not be reproduced or given to third parties without prior
   consent of Infosys.

   Filename                     : Dio.c
   Version                      : 3.0.3
   Microcontroller Platform     : MC9S12XS128
   Compiler                     : Codewarrior HCS12X V5.0.28

   Description                  : This file contains all the function 
                                  definiton for DIO module.
                                  
                                  Dio_ReadChannel
                                  Dio_WriteChannel
                                  Dio_ReadPort
                                  Dio_WritePort
                                  Dio_ReadChannelGroup
                                  Dio_WriteChannelGroup
                                  Dio_GetVersionInfo

   Requirement Specification    : AUTOSAR_SWS_DIODriver.doc version 2.0.0

   Module Design                : AUTOTMAL_DDD_DIO_SPAL2.0.doc version 1.0.0

   Platform Dependant[yes/no]   : yes

   To be changed by user[yes/no]: yes
*/
/*****************************************************************************/
/* Revision History

Class of Change
-------------------------------------------------------------------------------
N - New Module.
F - Functional development.
E - Error Correction.
O - Optimization without functional change.
-------------------------------------------------------------------------------
00  02-Feb-2006  Kiran G S, Infosys
-------------------------------------------------------------------------------
Class : N
Cause : New Module
Detail: This file implements the function definitions of DIO driver.
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
01  19-Mar-2006   kiran G S,  Infosys
-------------------------------------------------------------------------------
Class : F
Cause : Functional development
Detail: Upgrade to suit to 2.0.0 DIO specification.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
02  08-Jun-2006   kiran G S,  Infosys
-------------------------------------------------------------------------------
Class : E
Cause : Std_VersionInfoType structure is modified in Std_Types.h 
        Code modified for points reported and accepted in Problem report list 
        Code modified for points reported and accepted in RTRT anomaly reportlist

Detail: updated Dio_WriteChannelGroup to check overflow and Dio_WriteChannel 
        function to take only valid level.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
03  28-Jun-2006   kiran G S,  Infosys
-------------------------------------------------------------------------------
Class : O
Cause : Macro definition were added for the functions Dio_ReadChannel
        Dio_WriteChannel,Dio_ReadPort,Dio_WritePort,Dio_ReadChannelGroup and
        Dio_WriteChannelGroup.

Detail: All the functions except Dio_GetVersionInfo are updated with Macro
        expansions.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
04  07-Sep-2006   Ajaykumar,  Infosys
-------------------------------------------------------------------------------
Class:E
Cause:Reentrancy related error correction.

Detail:Disable/Enable interrupts macros removed in Dio_ReadChannel,Dio_ReadPort,
       Dio_WritePort and Dio_ReadChannelGroup.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
05  01-Feb-2007   Kasinath,  Infosys
-------------------------------------------------------------------------------
Class: F
Cause: A2D0 & A2D1 Channel Changes.
Detail:Port A2D0 modified to handle 16 channels and port A2D1 8 channels.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
06   22-Mar-2007   Sandeep,  Infosys
-------------------------------------------------------------------------------
Class : E
Cause : Temic comments for not using const for variables 
Detail: Dio_PortWrite_Ptr and Dio_PortRead_Ptr made const
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
07   03-Oct-2007   Ajaykumar,  Infosys 
-------------------------------------------------------------------------------
Class : F 
Cause : MC9S12XS128 controller support 
Detail: MC9S12XS128 related pre-processor switches added. 
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
08   31-Mar-2008   Ajaykumar, Infosys
-------------------------------------------------------------------------------
Class : E
Cause : Changes done for the issue #978
Detail: #978: Pre compiler check for SW Patch Version is removed.
-------------------------------------------------------------------------------

*/
/*****************************************************************************/

/*...................................File Includes...........................*/


/* Implementing this Interface */
#include "Dio.h"

/*...................................Macros..................................*/

/* Defining the  Macros to perform the version check */
#define DIO_SW_MAJOR_VERSION_C    3
#define DIO_SW_MINOR_VERSION_C    0
#define DIO_SW_PATCH_VERSION_C    3

#define DIO_AR_MAJOR_VERSION_C    2
#define DIO_AR_MINOR_VERSION_C    0
#define DIO_AR_PATCH_VERSION_C    1

/* Defining Macros which will be used in the program */
#define DIO_ZERO                     0
#define DIO_ONE                      1

/*...............................Version Check...............................*/

/* Doing version checking before compilation */
#if(DIO_SW_MAJOR_VERSION==DIO_SW_MAJOR_VERSION_C)
#if(DIO_SW_MINOR_VERSION==DIO_SW_MINOR_VERSION_C)
#if(DIO_AR_MAJOR_VERSION==DIO_AR_MAJOR_VERSION_C)
#if(DIO_AR_MINOR_VERSION==DIO_AR_MINOR_VERSION_C)
#if(DIO_AR_PATCH_VERSION==DIO_AR_PATCH_VERSION_C) 

/* ..........................Global variables............................... */

/* These global variables have internal linkages */

uint8 Dio_Dummy; 
/* Declaring port data registers which will be used for writing  */
uint8* const Dio_PortWrite_Ptr[DIO_PORTS] =
                                            {    
  
                                                  &(PORTA),&(PORTB), 
                                                  &(Dio_Dummy), 
                                                  &(Dio_Dummy), 
                                                  &(PORTE),&(PTH), 
                                                  &(PTJ),&(PORTK),&(PTM), 
                                                  &(PTP),&(PTS),&(PTT), 
                                                  &(PT1AD0),&(PT0AD0), 
                                                  &(Dio_Dummy)
                                            };

/* Declaring port Input and data registers which will be used for Reading  */                                                
uint8* const Dio_PortRead_Ptr[DIO_PORTS] = 
                                            {
  
                                                  &(PORTA),&(PORTB), 
                                                  &(Dio_Dummy), 
                                                  &(Dio_Dummy), 
                                                  &(PORTE),&(PTIH), 
                                                  &(PTIJ),&(PORTK),&(PTIM), 
                                                  &(PTIP),&(PTIS),&(PTIT), 
                                                  &(PT1AD0),&(PT0AD0), 
                                                  &(Dio_Dummy)
                                            };



/*..........................API Definitions..................................*/

/*****************************************************************************
* Function Name              : Dio_GetVersionInfo
*
* Arguments                  : Std_VersionInfoType *versioninfo 
*
* Description                : This API stores the version information of the 
*                              DIO module in the variable pointed by the pointer
*                              passed to the function. The version information 
*                              include
*                               1.Module ID
*                               2.Version ID
*                               3.Vendor specific Information.
*
* Return Type                : void
*
* Requirement numbers        : DIO125
******************************************************************************/

void Dio_GetVersionInfo(Std_VersionInfoType *versioninfo)
{

    if(NULL_PTR != versioninfo)
     {
    /* To store the version information of DIO module */
    versioninfo->moduleID       = DIO_MODULE_ID;
    versioninfo->vendorID       = DIO_VENDOR_ID;
    versioninfo->sw_major_version= DIO_SW_MAJOR_VERSION_C;
    versioninfo->sw_minor_version= DIO_SW_MINOR_VERSION_C;
    versioninfo->sw_patch_version= DIO_SW_PATCH_VERSION_C;
     }

}

#else
   #error " Mismatch in AR Patch Version of Dio.c & Dio.h "
#endif

#else
   #error " Mismatch in AR Minor Version of Dio.c & Dio.h " 
#endif

#else
   #error " Mismatch in AR Major Version of Dio.c & Dio.h " 
#endif

#else
   #error " Mismatch in SW Minor Version of Dio.c & Dio.h " 
#endif

#else
   #error " Mismatch in SW Major Version of Dio.c & Dio.h " 
#endif
/* End of version check */
/*...............................END OF Dio.c................................*/


