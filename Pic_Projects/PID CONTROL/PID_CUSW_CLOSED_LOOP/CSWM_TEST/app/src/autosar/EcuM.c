/* Created at 15:25 on 1-4-2009 */
/* Version of HALCoGen used : 2.9.3 */
/* Code generated for       : MC9S12XS128 */

/*****************************************************************************/
/* Copyright(c) - This Program/Software is the exclusive property of Infosys.
The software may not be reproduced or given to third parties without prior
consent of Infosys.

     Filename                      : EcuM.c
     Version                       : 1.0.0
     Microcontroller Platform      : MC9S12XS128
     Compiler                      : Codewarrior HCS12X V5.7.0

     Description                   : This file contains EcuM definitions
                                     required for compilation and testing
                                     of the Can Interface,MCU modules.

     Requirement Specification     : N.A.

     Platform Dependant[yes/no]    : no

     To be changed by user[yes/no] : no
*/
/*****************************************************************************/

/*Dummy EcuM.c*/
#include "EcuM.h"

/*.........................Global Variable declarations......................*/

EcuM_WakeupSourceType WakeupSource;

void EcuM_SetWakeUpEvent(EcuM_WakeupSourceType events)
{

}
/*...............................END OF EcuM.c................................*/

