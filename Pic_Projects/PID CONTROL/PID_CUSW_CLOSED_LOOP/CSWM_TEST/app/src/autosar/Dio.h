/* Created at 15:24 on 1-4-2009 */
/* Version of HALCoGen used : 2.9.3 */
/* Code generated for       : MC9S12XS128 */
/*****************************************************************************/
/* Copyright(c) - This Program/Software is the exclusive property of Infosys.
   The software may not be reproduced or given to third parties without prior
   consent of Infosys.

   Filename                     : Dio.h
   Version                      : 3.0.3
   Microcontroller Platform     : MC9S12XS128
   Compiler                     : Codewarrior HCS12X V5.0.28
   
   Description                  : This file contains data types and declaration
                                  of functions defined in DIO Driver module
                                  which are used by the other modules.
   
   Requirement Specification    : AUTOSAR_SWS_DIODriver.pdf version 2.0.0
   
   Module Design                : AUTOTMAL_DDD_DIO_SPAL2.0.doc version 1.0.0

   Platform Dependant[yes/no]   : yes   
     
   To be changed by user[yes/no]: no  
*/
/*****************************************************************************/
/* Revision History

Class of Change
-------------------------------------------------------------------------------
N - New Module.
F - Functional development.
E - Error Correction.
O - Optimization without functional change.
-------------------------------------------------------------------------------
00  02-Feb-2006  Kiran G S, Infosys
-------------------------------------------------------------------------------
Class : N
Cause : New Module
Detail: This file implements the function definitions of DIO driver.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
01  19-Mar-2006   kiran G S,  Infosys
-------------------------------------------------------------------------------
Class : F
Cause : Functional development
Detail: Upgrade to suit to 2.0.0 DIO specification.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
02  08-Jun-2006   kiran G S,  Infosys
-------------------------------------------------------------------------------
Class : E
Cause : Std_VersionInfoType structure is modified in Std_Types.h 
        Code modified for points reported and accepted in Problem report list 
        Code modified for points reported and accepted in RTRT anomaly report

Detail: updated Dio_WriteChannelGroup to check overflow and Dio_WriteChannel 
        function to take only valid level.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
03  28-Jun-2006   kiran G S,  Infosys
-------------------------------------------------------------------------------
Class : O
Cause : Macro definition were added for the functions Dio_ReadChannel
        Dio_WriteChannel,Dio_ReadPort,Dio_WritePort,Dio_ReadChannelGroup and
        Dio_WriteChannelGroup.
Detail: All the functions except Dio_GetVersionInfo were updated with Macro
        expansions.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
04  07-Sep-2006   Ajaykumar,  Infosys
-------------------------------------------------------------------------------
Class:E
Cause:Reentrancy related error correction.
Detail:Disable/Enable interrupts macros removed in Dio_WritePort.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
05  01-Feb-2007   Kasinath,  Infosys
-------------------------------------------------------------------------------
Class: F
Cause: A2D0 & A2D1 Channel Changes
Detail:Port A2D0 modified to handle 16 channels and port A2D1 8 channels.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
06  22-Mar-2007  Sandeep, Infosys
-------------------------------------------------------------------------------
Class : E
Cause : Temic comments for not using const for variables 
Detail: Dio_PortWrite_Ptr and Dio_PortRead_Ptr made const
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
07   03-Oct-2007   Ajaykumar,  Infosys 
-------------------------------------------------------------------------------
Class : F 
Cause : Changes for MC9S12XS128 controller  
Detail: Port, Pin package information is changed 
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
08   31-Mar-2008   Ajaykumar,  Infosys
-------------------------------------------------------------------------------
Class:  E
Cause:  PRL issue 978
Detail: Updated in sync with Dio.c
-------------------------------------------------------------------------------

******************************************************************************/
/* To avoid Multiple inclusion */  
#ifndef _DIO_H
#define _DIO_H

/*...................................File Includes...........................*/
/* platform dependent files are included in this header */
#include "Std_Types.h"

/* Infosys defines header file */
#include "MCAL_Types.h"

/* Dio Driver configuration file */
#include "Dio_Cfg.h"

/* OS header file */
#include "os.h"

/*...................................Macros..................................*/
/* Defining the  Macros to perform the version check */
#define DIO_SW_MAJOR_VERSION                     3
#define DIO_SW_MINOR_VERSION                     0
#define DIO_SW_PATCH_VERSION                     3

#define DIO_AR_MAJOR_VERSION                     2
#define DIO_AR_MINOR_VERSION                     0
#define DIO_AR_PATCH_VERSION                     1

/* Defining the module id for DIO driver module */
#define DIO_MODULE_ID                       ((uint8)120)

/* Defining the Vendor ID */
#define DIO_VENDOR_ID                       ((uint16)40)

/* Defining the service ID macros of the DIO Driver service APIs */
#define DIO_READCHANNEL_ID                  ((uint8)0)
#define DIO_WRITECHANNEL_ID                 ((uint8)1)
#define DIO_READPORT_ID                     ((uint8)2)
#define DIO_WRITEPORT_ID                    ((uint8)3)
#define DIO_READCHANNELGROUP_ID             ((uint8)4)
#define DIO_WRITECHANNELGROUP_ID            ((uint8)5)
#define DIO_GETVERSIONINFO_ID               ((uint8)0x12)

/* Error IDs */
#define DIO_E_PARAM_INVALID_CHANNEL_ID     ((uint8)10)
#define DIO_E_PARAM_INVALID_PORT_ID        ((uint8)20)
#define DIO_E_PARAM_INVALID_GROUP_ID       ((uint8)31) 

/* Defining Channels */
#define DIO_CHANNEL_A_0     ((uint8)0)
#define DIO_CHANNEL_A_1     ((uint8)1)
#define DIO_CHANNEL_A_2     ((uint8)2)
#define DIO_CHANNEL_A_3     ((uint8)3)
#define DIO_CHANNEL_A_4     ((uint8)4)
#define DIO_CHANNEL_A_5     ((uint8)5)
#define DIO_CHANNEL_A_6     ((uint8)6)
#define DIO_CHANNEL_A_7     ((uint8)7)

#define DIO_CHANNEL_B_0     ((uint8)8)
#define DIO_CHANNEL_B_1     ((uint8)9)
#define DIO_CHANNEL_B_2     ((uint8)10)
#define DIO_CHANNEL_B_3     ((uint8)11)
#define DIO_CHANNEL_B_4     ((uint8)12)
#define DIO_CHANNEL_B_5     ((uint8)13)
#define DIO_CHANNEL_B_6     ((uint8)14)
#define DIO_CHANNEL_B_7     ((uint8)15)



#define DIO_CHANNEL_E_0     ((uint8)32)
#define DIO_CHANNEL_E_1     ((uint8)33)
#define DIO_CHANNEL_E_2     ((uint8)34)
#define DIO_CHANNEL_E_3     ((uint8)35)
#define DIO_CHANNEL_E_4     ((uint8)36)
#define DIO_CHANNEL_E_5     ((uint8)37)
#define DIO_CHANNEL_E_6     ((uint8)38)
#define DIO_CHANNEL_E_7     ((uint8)39)


#define DIO_CHANNEL_J_6     ((uint8)54)
#define DIO_CHANNEL_J_7     ((uint8)55)


#define DIO_CHANNEL_M_0     ((uint8)64)
#define DIO_CHANNEL_M_1     ((uint8)65)
#define DIO_CHANNEL_M_2     ((uint8)66)
#define DIO_CHANNEL_M_3     ((uint8)67)
#define DIO_CHANNEL_M_4     ((uint8)68)
#define DIO_CHANNEL_M_5     ((uint8)69)

#define DIO_CHANNEL_P_0     ((uint8)72)
#define DIO_CHANNEL_P_1     ((uint8)73)
#define DIO_CHANNEL_P_2     ((uint8)74)
#define DIO_CHANNEL_P_3     ((uint8)75)
#define DIO_CHANNEL_P_4     ((uint8)76)
#define DIO_CHANNEL_P_5     ((uint8)77)
#define DIO_CHANNEL_P_7     ((uint8)79)

#define DIO_CHANNEL_S_0     ((uint8)80)
#define DIO_CHANNEL_S_1     ((uint8)81)
#define DIO_CHANNEL_S_2     ((uint8)82)
#define DIO_CHANNEL_S_3     ((uint8)83)

#define DIO_CHANNEL_T_0     ((uint8)88)
#define DIO_CHANNEL_T_1     ((uint8)89)
#define DIO_CHANNEL_T_2     ((uint8)90)
#define DIO_CHANNEL_T_3     ((uint8)91)
#define DIO_CHANNEL_T_4     ((uint8)92)
#define DIO_CHANNEL_T_5     ((uint8)93)
#define DIO_CHANNEL_T_6     ((uint8)94)
#define DIO_CHANNEL_T_7     ((uint8)95)

#define DIO_CHANNEL_AD0L_0     ((uint8)96)
#define DIO_CHANNEL_AD0L_1     ((uint8)97)
#define DIO_CHANNEL_AD0L_2     ((uint8)98)
#define DIO_CHANNEL_AD0L_3     ((uint8)99)
#define DIO_CHANNEL_AD0L_4     ((uint8)100)
#define DIO_CHANNEL_AD0L_5     ((uint8)101)
#define DIO_CHANNEL_AD0L_6     ((uint8)102)
#define DIO_CHANNEL_AD0L_7     ((uint8)103)

/* defining Ports */
#define DIO_PORT_A					(uint8)0
#define DIO_PORT_B					(uint8)1
#define DIO_PORT_E					(uint8)4
#define DIO_PORT_J					(uint8)6
#define DIO_PORT_M					(uint8)8
#define DIO_PORT_P					(uint8)9
#define DIO_PORT_S					(uint8)10
#define DIO_PORT_T					(uint8)11
#define DIO_PORT_AD0L					(uint8)12
 
/*.........................Type Definitions..................................*/

/* Declaring Variables in case Macro is enabled */

#define DIO_PORTWIDTH                8
#define DIO_PORTS                   15

extern  uint8  Dio_Dummy;
/* Declaring port data registers which will be used for writing  */
extern  uint8* const Dio_PortWrite_Ptr[DIO_PORTS];

/* Declaring port Input and data registers which will be used for Reading  */
extern  uint8* const Dio_PortRead_Ptr[DIO_PORTS];

typedef uint8  Dio_ChannelType;

typedef uint8  Dio_PortType;

typedef uint8  Dio_PortLevelType;

typedef uint8 Dio_LevelType;


/* defining structure  used in grouping of channels */
typedef struct
            {
                /* Port on which Channel group is defined*/
                Dio_PortType Dio_PortGroup;
                
                /* Position of the Channel group onthe Port,
                   Counted from the LSB*/  
                uint8 Dio_Offset;

                /*Mask Which defines the positions of the channel group */  
                uint8 Dio_Mask;

            }Dio_ChannelGroupType;   

/*.........................Global Variable declarations......................*/

/* Variable to hold only the configured channels */
extern const Dio_ChannelType Dio_ChannelName[DIO_MAXCHANNELS];
/* Variable to hold only the configured ports */
extern  const Dio_PortType  Dio_PortName[DIO_MAXPORTS];
/* Variable to hold only the configured groups */
extern  const Dio_ChannelGroupType DioConfigData[DIO_MAXGROUPS];

/*.........................Global Function Prototypes........................*/

/* Declaration of functions/Macros which are used by DIO driver module */

/* Read channel */
#define Dio_ReadChannel(ChannelId) (((*Dio_PortRead_Ptr[(uint8)(ChannelId/DIO_PORTWIDTH)]\
                                   >> ((uint8)(ChannelId % DIO_PORTWIDTH))) & 0x01 ))

/* Write channel */
#define Dio_WriteChannel(ChannelId, Level)   DisableAllInterrupts(); \
			((Level == STD_HIGH) ?  \
			(*Dio_PortWrite_Ptr[((uint8)(ChannelId / DIO_PORTWIDTH))]= \
            ((*Dio_PortWrite_Ptr[((uint8)(ChannelId / DIO_PORTWIDTH))])| \
            (Level<<((uint8)(ChannelId % DIO_PORTWIDTH))))) : \
            (*Dio_PortWrite_Ptr[((uint8)(ChannelId / DIO_PORTWIDTH))]= \
            ((*Dio_PortWrite_Ptr[(uint8)(ChannelId / DIO_PORTWIDTH)])) & \
            (~(uint8)(STD_HIGH << ((uint8)(ChannelId % DIO_PORTWIDTH))))));\
            EnableAllInterrupts()

/* Read port */
#define Dio_ReadPort(PortId)    (*Dio_PortRead_Ptr[PortId])

/* Write port */
#define Dio_WritePort(PortId,Level) (*Dio_PortWrite_Ptr[PortId] = Level)

/* Read channel group */
#define Dio_ReadChannelGroup(ChannelGroupIdPtr)    (((*Dio_PortRead_Ptr[(ChannelGroupIdPtr)->Dio_PortGroup])\
                                 & ((ChannelGroupIdPtr)->Dio_Mask))  \
                                 >> (ChannelGroupIdPtr)->Dio_Offset)

/* Write channel group */
#define Dio_WriteChannelGroup(ChannelGroupIdPtr,Level)  DisableAllInterrupts(); \
					  *Dio_PortWrite_Ptr[(ChannelGroupIdPtr)->Dio_PortGroup]= \
                      (~((ChannelGroupIdPtr)->Dio_Mask)& \
                      (*Dio_PortWrite_Ptr[(ChannelGroupIdPtr)->Dio_PortGroup])) \
                      |((Level << (ChannelGroupIdPtr)->Dio_Offset)&((ChannelGroupIdPtr)->Dio_Mask)); \
                      EnableAllInterrupts()

/* Get Version info */
extern void  Dio_GetVersionInfo(Std_VersionInfoType* versioninfo);


#endif /*  end _DIO_H */


/*...............................End OF Dio.h................................*/

