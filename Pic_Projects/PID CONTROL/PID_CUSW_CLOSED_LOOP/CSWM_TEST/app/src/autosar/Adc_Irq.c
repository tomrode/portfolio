/* Created at 15:24 on 1-4-2009 */
/* Version of HALCoGen used : 2.9.3 */
/* Code generated for       : MC9S12XS128 */
/*****************************************************************************/
/* Copyright(c) - This Program/Software is the exclusive property of Infosys.
The software may not be reproduced or given to third parties without prior
consent of Infosys.

   Filename                        : Adc_Irq.c
   Version                         : 2.0.0
   Microcontroller Platform        : MC9S12XS128
   Compiler                        : Codewarrior HCS12X V5.0.28

   Description                     : This file contains interrupt service routine for
                                     ADC module

   Requirement Specification       : AUTOSAR_SWS_ADC_Driver.pdf 1.0.16

   Module Design                   : AUTOSAR_DDD_ADC_SPAL2.0.doc version 2.11

   Platform Dependant[yes/no]      : yes

   To be changed by user[yes/no]   : no
*/

/*****************************************************************************/

/* Revision History

Class of Change
-------------------------------------------------------------------------------
N - New Module.
F - Functional development.
E - Error Correction.
O - Optimization without functional change.
-------------------------------------------------------------------------------
00     14-Jul-2006     Karthik, Infosys
-------------------------------------------------------------------------------
Class: N
Cause: New Module
Detail:This file contains ADC ISRs
-------------------------------------------------------------------------------
  
01     21-Aug-2006     Karthik, Infosys
-------------------------------------------------------------------------------
Class:  E
Cause:  Lint Analysis 
Detail: 
-------------------------------------------------------------------------------
	
02     23-Mar-2007     Rajesh, Infosys
-------------------------------------------------------------------------------
Class: E
Cause: Bug Fixes - 530, 565, 676, 552, 682, 683, 686
Detail: Only configured HW units are initialized in Adc_Init.
-------------------------------------------------------------------------------
	  
******************************************************************************/

/*...................................File Includes...........................*/

#include "Adc_Irq.h"

#pragma CODE_SEG __NEAR_SEG NON_BANKED

/*************************** ADC0 ISR ***************************************/

	void near interrupt Atd_0_Isr(void)
{
	Adc0_ConversionCompleteIsr();
}
/*-------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------*/

#pragma CODE_SEG DEFAULT


/*............End of Adc_Irq.c...............................................*/



