/* Created at 15:25 on 1-4-2009 */
/* Version of HALCoGen used : 2.9.3 */
/* Code generated for       : MC9S12XS128 */
/*****************************************************************************/
/* Copyright(c) - This Program/Software is the exclusive property of Infosys.
The software may not be reproduced or given to third parties without prior
consent of Infosys.

     Filename                      : Gpt_Irq.c
     Version                       : 3.2.0
     Microcontroller Platform      : MC9S12XS128
     Compiler                      : Codewarrior HCS12X V4.5

     Description                   : This file contains service routines for
                                     GPT module

     Requirement Specification     : WP4.2.2.1.12-SWS-GPT-Driver.doc 1.1.6

     Module Design                 : AUTOTMAL_DDD_GPT_SPAL2.0.doc version 2.00c

     Platform Dependant[yes/no]    : yes

     To be changed by user[yes/no] : no
*/
/*****************************************************************************/

/* Revision History

Class of Change
-------------------------------------------------------------------------------
N - New Module.
F - Functional development.
E - Error Correction.
O - Optimization without functional change.
-------------------------------------------------------------------------------
 00    14-Jul-2006     Sowmya, Infosys
-------------------------------------------------------------------------------
Class: N
Cause: New Module
Detail:This file contains interrupt service routines for GPT module
-------------------------------------------------------------------------------
 01    22-Mar-2006     Sowmya, Infosys
-------------------------------------------------------------------------------
Class: F
Cause: Functional development
Detail:Upgrade to suit to 1.1.6 GPT specification.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 02    08-Dec-2006      Ramprasad P B , Infosys
-------------------------------------------------------------------------------
Class: N
Cause: New Module
Detail:This file contains interrupt service routine for GPT module S12XEP100
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
 03    24-Jul-2007      Khanindra, Infosys
-------------------------------------------------------------------------------
Class: F
Cause: Updated for the derivatives MC9S12XEG384 and MC9S12XEG256 
Detail:Updated for the derivatives MC9S12XEG384 and MC9S12XEG256 
-------------------------------------------------------------------------------

******************************************************************************/

/*...................................File Includes...........................*/

/* Module header */

#include "Gpt_Irq.h"

#pragma CODE_SEG __NEAR_SEG NON_BANKED

/*************************** GPT ECT 2 ISR ***************************************/

	interrupt  void near Ect_2_Isr(void)
{
	Gpt_ECTTimeoutHandler_2_Isr();
}
/*-------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------*/

#pragma CODE_SEG DEFAULT


/*............End of Gpt_Irq.c...............................................*/



