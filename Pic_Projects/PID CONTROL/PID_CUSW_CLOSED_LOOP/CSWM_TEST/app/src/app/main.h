#ifndef MAIN_H_
#define MAIN_H_

/********************************************************************************/
/*                        A U T H O R   I D E N T I T Y							*/
/********************************************************************************/
/* Initials      Name                   Company									*/
/* --------      --------------------   --------------------------------------- */ 
/* HK            Harsha Yekollu         Continental Automotive Systems          */
/* CK            Can Kulduk             Continental Automotive Systems			*/
/********************************************************************************/


/********************************************************************************/
/*                   CHANGE HISTORY for MAIN  		                            */
/********************************************************************************/
/* 09.01.2006 HY -  Header file created                                			*/
/* 12.20.2007 CK -  Copied from CSWM MY09 project                   		    */
/* 10.27.2008 CK -  Added IC_A1_AMB_TEMP_AVE_SNA #define 						*/
/*               -  IGN_SNA = 7 instead of 6.									*/
/* 10.31.2008 CK -  Updated MAIN_TMSLICE_PERIOD for TIM CH2 instead of PIT.     */
/* 12.16.2008 CK -  Changed mainCSWM_stat delay to 800ms (due to ENG_RPM cyclic)*/
/* 01.08.2009 HY -  main_variant_select_c variable type changed to global       */
/* 03.10.2009 CK -  Added a #define for 'LD' 'DX' and 'DZ' vehicle lines.       */
/*                  Updated 'DA' vehicle line #define value.                    */
/* 04.14.2009 CK -  Added JC vehicle line #define. Deleted PWR_NET #ifdef.  	*/
/* 04.16.2009 CK -  Deleted un-used #define(s): ENG_RPM_LIMIT, VOLTAGE_LOW_LIMIT*/
/*                  VOLTAGE_HI_LIMIT, AMB_TEMP_FOR_HEATING, AMB_TEMP_FOR_VENTING*/
/* 04.16.2009 HY -  CSWM Bussed messages Variant masks for PN added             */
/* 04.28.2009 HY -  mainF_OFF_NonSupported_Outputs_ServiceParts () added        */
/* 10.04.2009 CC - FTR00139665 - New vehicle Line RT-RM                         */
/* 08.10.2010 HY - MKS 52170:Vehicle line selection differentiated between TIPM and PN */
/* 09.21.2010 HY - autostart_check_complete_b introduced under main_flag2_c     */
/* 09.28.2010 HY - #define PWR_NET removed from the code. As we have only one   */
/*                 kind of implementation there is no TIPM going forward        */
/********************************************************************************
 *                              CUSW UPDATES
 * 05.18.2011 HY - AMB TEMP AVERAGE SNA value changed according to FIAT 
 * 				   IC_A1_AMB_TEMP_AVE_SNA modified as EXTERNAL_TEMP_SNA
 * 
***************************** Constants/Defines *********************************/


#undef EXTERN 
#ifdef MAIN_C 
#define EXTERN 
#else
#define EXTERN extern
#endif

/***************************** Constants/Defines *********************************/
/* Main Defines */
#define TRUE	1
#define FALSE	0

/* for PIT: Not used anymore */
//#define MAIN_TMSLICE_PERIOD 250	// for Period for timer that generates our time slice (1.25ms)

/* For TIM CH2 */
#define MAIN_TMSLICE_PERIOD 625	// Period for timer that generates our time slice (1.25ms)

///* MKS 52170: Vehicle line values differentiated depending on the Architecture */
/* Power Net Vehicles */
//#define VEH_LX  	0x02
//#define VEH_LD		0x29     // Added on 3.10.2009 by CK. Please verify this value with latest CCM.
//#define VEH_JC  	0x03     // Added on 04.14.2009 by CK. (value from CCMv915)

#define VEH_PF      0x50       //According to PROXI.
#define VEH_MM      51       //Dummy
#define VEH_MA      52       //Dummy
#define VEH_KL      53       //Dummy
#define VEH_UF      54       //Dummy
#define VEH_UT      55       //Dummy
#define VEH_UA      56       //Dummy
#define VEH_PK      57       //Dummy
#define VEH_TR      58       //Dummy



#define VEH_BUS_CCN_FRONT_REAR_K 		0x01
#define VEH_BUS_HVAC_FRONT_ONLY_K 		0x02
#define VEH_SWITCH_REAR_ONLY_K 			0x03
#define VEH_BUS_HVAC_FRONT_RAER_RT_K 	0x04
#define VEH_BUS_DM_REAR_K               0x05   //Vehicle Line RT RM


/* Model Year Defines */
#define MY_09       0x09
#define MY_11       0x0B

/*****************CSWM Enable Definitions *****************/
//#define ENG_RPM_LIMIT			100		// Commented by CK 04.16.2009. Using EEPROM parameter instead

//#define VOLTAGE_LOW_LIMIT       100   // Commented by CK 04.16.2009. Not Used
//#define VOLTAGE_HI_LIMIT		160		// Commented by CK 04.16.2009. Not Used

/* Timing */
#define MAIN_LED_DSPLY_TM        2        // Defgault: 2 second short-term LED Display time
#define MAIN_FLT_MTR_TM          2        // Default: 2 second fault mature time

#define MAIN_CSWM_STAT_DELAY     20       // 20 loops in 40ms gives 800ms delay

#define MAIN_REMST_CONFIG_DELAY  24       // 24 loops in 20ms timeslice = 480ms delay

#define MAIN_CFG_RemSt_CSWM      0x28     // Remote Start Driver Comfort System (Remote Start Feature CSWM)

#define MAIN_REMST_DELAY         200      // 200 loops in 20ms timeslice = 4000ms (4 sec) delay (8-bit only)

#define MAIN_2SEC_DELAY          200      // 200 loops in 10ms gives 2000ms (2 second) delay

/* PWM calculation (for Autosar PWM driver) */
#define MAIN_ZERO_PWM            0        // for minimum PWM (0%   duty cycle = 0x0000)
#define MAIN_MAX_PWM             32768    // for maximum PWM (100% duty cycle = 0x8000)
#define MAIN_N100                100      // Constant used in PWM calculation
#define MAIN_N1                  1        // Constant used in PWM calculation
#define MAIN_N68                 68       // Constant used in PWM calculation
#define MAIN_N327                327      // Constant used in PWM calculation

// Commented by CK 04.16.2009. Using EEPROM parameters for heating and venting remote start automatic activation instead
//#define AMB_TEMP_FOR_HEATING    4440	/* Changed limit from 10c to 4.4      */ // 10C: Remote Start Heating automatic activation  
//#define AMB_TEMP_FOR_VENTING    6670	/* Changed limit from 30c to 26.7c    */ // 30C: Remote Start Venting automatic activation

#define EXTERNAL_TEMP_SNA         0	// External Temperature Average No Valid data available

/*************************************************************************************/
/*                                Variant Defines                                    */
/*************************************************************************************/
/* 1. HW ECU Configuration                                                           */
/* These values are determined from the ECU/HW                                       */
/* Variants V1 to V6 SW functionality will be determined from HW ECU Configuration   */
/* If HW config shows ECU is with V7,then SW functionality will be determined        */
/* from CAN Bus message.                                                             */
/* V1 t0 V7 Part Number information will be determined from HW PIns                  */
/* 2. SW ECU Configuration                                                           */
/* These values are for SW functionality                                             */
/* Variant V1 to V6 will be mapped from HW ECU configuration                         */
/* If HW is with V7, SW functionality will be determined from CAN Bus messages       */
/* 3. SW Masks:                                                                      */
/* These defines will determine the SW functionality based on SW ECU configuration   */
/* values.                                                                           */
/* 4. Variant V7, Service parts configuration (Only for PN)                          */
/* These masks are used only incase HW configuration shows the ECU is with V7        */
/* At services, only V7 will be replaced for all other variants                      */
/* In such cases, SW  functionality will be determined from Bus message              */
/*************************************************************************************/

/******************         1. HW ECU Configuration                   *****************/
/******************         To display Part Number Information        *****************/
/******************         V1 to V6 SW ECU configuration mapped      *****************/
/* CSWM MY 11 Variants Configuration
 * Variant   PE6(Pin 25) PE5(Pin 26) PE4(Pin 27)  Decimal    HFS   HRS   HSW   VFS
 *   V1          0           0           0           0        X      
 *   V2 		 1           0           0			 4        X     X
 *   V3          0           1           0			 2        X           X     X       
 *   V4          1           1           0			 6        X           X 
 * 	 V5          0           0           1  		 1        X                 X  
 *   V6          1           0           1		`	 5        X     X     X   
 *   V7          0           1           1			 3        X     X     X     X   
 *   V8          1           1           1			 7
 *  where, 
 *  HFS = Heated Front Seats
 *  HRS = Heated Rear Seats
 *  HSW = Heated Steering Wheel
 *  VFS = Vented Front Seats
 *                                  */
/*defines from the hardware pin out readings */
#define CSWM_V1		0x00  // HFS
#define CSWM_V2 	0x04  // HFS + HRS
#define CSWM_V3		0x02  // HFS + HSW + VFS
#define CSWM_V4		0x06  // HFS + HSW
#define CSWM_V5		0x01  // HFS + VFS
#define CSWM_V6		0x05  // HFS + HRS + HSW
#define CSWM_V7		0x03  // HFS + HRS + HSW + VFS	 	
#define CSWM_V8		0x07  // HFS + HRS + VFS MKS 68478

/******************         2. SW ECU Configuration                   *****************/
/******************         CSWM_config_c variable hold values        *****************/
#define CSWM_2HS_VARIANT_1_K            0x03
#define CSWM_4HS_VARIANT_2_K            0x0F
#define CSWM_2HS_2VS_HSW_VARIANT_3_K    0x73
#define CSWM_2HS_HSW_VARIANT_4_K        0x43
#define CSWM_2HS_2VS_VARIANT_5_K        0x33
#define CSWM_4HS_HSW_VARIANT_6_K        0x4F
#define CSWM_4HS_2VS_HSW_VARIANT_7_K    0x7F
#define CSWM_4HS_2VS_VARIANT_8_K        0x3F                      //MKS  68478 
#define CSWM_NO_OUTPUT_VARIANT_K        0x00  


/************                     3. SW MASKS                           **************/
/************  SW Functionality to be determined from CSWM_config_c     **************/

/* OLD CSWM MY 09 Variants MASK Configuration       */
/* Bit : 7 6 5 4 3 2 1 0                  */
/*         | | | | | | |---------> F L HS */
/*         | | | | | |-----------> F R HS */
/*         | | | | |-------------> R L HS */
/*         | | | |---------------> R R HS */
/*         | | |-----------------> F L VS */
/*         | |-------------------> F R VS */
/*         |---------------------> HSW    */
#define CSWM_4HS_VARIANT_MASK_K       0x0F
#define CSWM_2VS_MASK_K               0x30
#define CSWM_HSW_MASK_K               0x40

/****************** 4. Service parts Configuration for V7 (Only for PN)*****************
 * ECU CFG 5 ->EC_Memory(Byte 7 and 8 of ECU CFG5) Signal acceptance LOW BYTE for CSWM (BYTE 8), HIGH BYTE for MSM (Byte 7)
 * BIT 0   -> Rear Heated Seats (R)
 * BIT 1   -> Front Vented Seats (V)
 * BIT 2   -> Heated Steering Wheel (W)
 * BIT 3,4 -> Seat Type (ST)
 * BIT 5   -> Wheel Type (WT)
 * 
 * Combination is by default added with Front Heated Seats.
 * For Example If BIT 0 is set means it will be with FRONT + REAR Heated seats configuration.
/******************         If HW PIN shows V7                        *****************/

#define V2_RHS_BUS_MASK            0x01   //Mask to determine V2 Rear Heated seats present or not.

#define V3_HSW_FVS_BUS_MASK        0x06   //Mask to determine V3 HSW and Front Vents present or not.
#define V4_HSW_BUS_MASK            0x04   //Mask to determine V4 Steering Wheel Present or not.
#define V5_FVS_BUS_MASK            0x02   //Mask to determine V2 Front Vents Present or not.  MKS 68500
#define V6_HSW_RHS_BUS_MASK        0x05   //Mask to determine V6 HSW and Rear Heats present or not.
#define V7_FVS_RHS_HSW_BUS_MASK    0x07   //Mask to determine V7 Front vents, Rear Heats and HSW present or not.
#define V8_FVS_RHS_BUS_MASK        0x03   //Mask to determine V8 Front vents and Rear Heats  present or not.
#define LEATHER_CLOTH_SEAT_MASK    0x18   //Mask to determine Module equipped with Leather seats or cloth seats //MKS 57313 //MKS 68476 base leather/ perf leather / cloth
#define LEATHER_WOOD_STW_MASK      0x20   //Mask to determine Module equipped with Leather seats or wood STW //MKS 57313
#define ECUCFG5_MASK               0x3F   //Mask for low byte of Ecucfg5 message //MKS 57313

#define PROXI_NOT_PRGMED		   0x00
#define PROXI_HW_MISMATCH          0x01
#define PROXI_PRGMED_OK			   0x02	

/********************************************************************************/


/***************************** Enumarations *************************************/
/* Main CSWM Status */
typedef enum{
    UNDEFINED=0,            // undefined state
    GOOD=1,                 // outputs may turn on, continious LED display may be active
    BAD=2,                  // outputs may not turn on, continious LED display may be active
    UGLY=3                  // outputs may not turn on, 2 second LED display may be active
}main_CSWM_Stat_e;

/* Ignition Status */
typedef enum{
    IGN_LOCK=0,             // lock
    IGN_OFF=1,              // off
    IGN_ACC=2,              // accesory
    IGN_OFF_ACC=3,          // off accesory
    IGN_RUN=4,              // run: outputs may turn on, continious LED display may be active
    IGN_START=5,            // start: outputs may not turn on, continious LED display may be active
    IGN_SNA=7               // signal not available 
}main_Ign_Stat_e;

/* Load Shed Status for D and W series */
typedef enum{
    LDSHD_ZERO=0,           // load shed 0: outputs may turn on, continious LED display may be active
    LDSHD_ONE=1,            // Do not act on Load-Shed 1
    LDSHD_TWO=2,            // load shed 2: outputs may not turn on, 60 second mature time for low voltage fault    
    LDSHD_THREE=3,          // Do not act on Load-Shed 3    
    LDSHD_FOUR=4            // load shed 4: outputs may not turn on, 2 second LED display may be active    
}main_LoadShed_Stat_e;
	
/* Multiplexer and AD channels */
typedef enum{
    CH0=0,                  // Channel 0         
    CH1=1,                  // Channel 1 
    CH2=2,                  // Channel 2 
    CH3=3,                  // Channel 3
    CH4=4,                  // Channel 4
    CH5=5,                  // Channel 5 
    CH6=6,                  // Channel 6 
    CH7=7,                  // Channel 7         
    CH_LMT=8                // Number of Channels   
}main_ch_e;

/* Multiplexer selection */
typedef enum{
    MUX1=0,                // Multiplexer 1 
    MUX2=1,                // Multiplexer 2
    MUX_LMT=2              // Number of multiplexers
}main_mux_e;

/* Main Functinality States */
typedef enum{
    NORMAL=0,             // Normal Functinality   
    REM_STRT_HEATING=1,   // Remote start with automatic heating activation (Amb. temp < 4.4C)
    REM_STRT_VENTING=2,   // Remote start with automatic venting activation (Amb. temp > 26.7C)
    REM_STRT_USER_SWTCH=3, // Remote start with user switch activation (4.4C <= Amb. temp <= 26.7C) 
    AUTO_STRT_HEATING=4,   // AUTO start with automatic heating activation (Amb. temp < 4.4C)
    AUTO_STRT_VENTING=5,   // AUTO start with automatic venting activation (Amb. temp > 26.7C)
    AUTO_STRT_USER_SWTCH=6 //AUTO start with user switch activation (4.4C <= Amb. temp <= 26.7C)   
}main_state_e;

/* Driver Side Selection */
typedef enum{
    LHD=1,                // Left Hand Drive
    RHD=2,                // Right Hand Drive
    LHD_RHD_SNA=3         // Not programmed
}main_Drive_Side_e;

/* Remote Start: Configurable feature status request */
typedef enum{             // Remote Start Driver Comfort System Feature Status
    CFG_DEFAULT=0,        // CSWM Feature is at its default state (Enabled)
    CFG_DISABLE=1,        // CSWM Feature is disabled
    CFG_ENABLE=2,         // CSWM Feature is enabled
    CFG_SPEC=3            // CSWM Feature is enabled in Specific setting (Not Used)
}main_RemSt_CFG_Stat;

/* Mcu test type */
typedef enum{
    MAIN_RAM_TEST=0,       // Ram test 
    MAIN_ROM_TEST=1        // Rom test
}main_mcu_test_type_e;
/********************************************************************************/


/******************************STRUCTURES****************************************/
/* Main application parameters  - will be stored in EEPROM */
typedef struct{
    u_8Bit Flt_Mtr_Tm;            // fault mature time (default: 2 seconds)
    u_8Bit LED_Dsply_Tm;          // LED display time (default: 2 seconds)
}main_appl_params;

EXTERN main_appl_params main_prms;

/* AtoD conversion parameters */
typedef struct{
    u_8Bit slctd_muxCH_c;         // selected mux channel
    u_8Bit slctd_adCH_c;          // selected AD channel
    u_8Bit slctd_mux_c;           // selected multiplexer
}main_ADC_prms;
/********************************************************************************/


/*********************************GLOBAL VARIABLES*******************************/
EXTERN u_8Bit main_CSWM_Status_c; 	   //Main CSWM Status (GOOD/LIMP IN/FAILURE states with respect to IGN,LOADSHED,VOLTAGE and ENGRPM)
EXTERN u_8Bit main_CSWM_Func_State_c;  //CSWM Functionality state (Normal or Remote Start)
EXTERN u_8Bit main_ECU_reset_rq_c; 	   //Diag service ECU Reset $11 $01
EXTERN u_8Bit main_FrontSeat_Status_c; //Front seat req status either CCN or HVAC
EXTERN u_8Bit main_RearSeat_Status_c;  //Rear seat req status either CCN or Internal switch
EXTERN u_8Bit main_state_c;            //Main functionality state (Normal or remote start functinalities)
EXTERN u_8Bit main_SwReq_Front_c;      //Switch request for Front Seats
EXTERN u_8Bit main_SwReq_Rear_c;       //Switch request for Front Seats
EXTERN u_8Bit main_RemSt_delay_c;      //4 second Remote Start delay (due to LHD_RHD cyclic 2000)
EXTERN u_8Bit main_SwReq_stW_c;		  // Vehicle line WK and WD is special for Steering Wheel
EXTERN u_8Bit hardware_proxi_mismatch_c;	  // Hardware Proxi Mismatch


EXTERN union char_bit main_flag_c;                   // main flags 
#define nm_offset_2p5ms_b          main_flag_c.b.b0  // 2.5 ms offset for Tp tasks
#define U12S_enable_b              main_flag_c.b.b1  // flag used to switch U12S on/off
#define RAM_is_checked_b           main_flag_c.b.b2  // RAM is checked (once every POR)
#define ROM_is_checked_b           main_flag_c.b.b3  // ROM is checked (once every POR)
#define Allow_CSWM_status_Update_b main_flag_c.b.b4  // 800ms delay after every power on reset.
#define Allow_stWheel_mngmnt_b     main_flag_c.b.b5  // 2 second delay for steering wheel
#define RemSt_activated_b          main_flag_c.b.b6  // Activates CSWM remote start once for each request

EXTERN union char_bit main_flag2_c;                    // main flags 
#define Rear_U12S_enable_b          main_flag2_c.b.b0  // flag used to switch REAR U12S on/off
#define main_U12S_keep_ON_b         main_flag2_c.b.b1  // flag used to keep U12S on during stWheel STG detection in Relay module
#define main_kept_U12S_ON_b			main_flag2_c.b.b2  // Flag that lets Relay module to know that we are ready for STG tests on High side driver
#define autostart_check_complete_b  main_flag2_c.b.b3  //Autostart check complete flag.
/* main_RAM_test_rslt: */
/* 0: Not tested       */
/* 1: Result OK        */
/* 2: Result not OK    */
/* 3: Result undefined */
EXTERN u_8Bit main_RAM_test_rslt;  		// RAM test result
EXTERN u_8Bit main_EngRpm_lowLmt_c;    // Engine Rpm low limit
EXTERN u_8Bit main_ecu_cfg_for_v7_c;    //ECU configuration for HW variant V7 determined from EC_Memory Bussed message
EXTERN u_8Bit main_ecu_seat_cfg_c;      //ECU Seat configuration

EXTERN u_8Bit main_variant_select_c;         //Variable holds the varaint PIN reading from Hardware.
/********************************************************************************/


/**********************************Function Prototypes***************************/
/* Global Function Prototypes */
u_16Bit @near mainF_Calc_PWM(u_8Bit Duty_cycle_c);
/* For Internal Faults and Rom test modules */
void @near mainF_Finish_Mcu_Test(main_mcu_test_type_e test_type); 
/* For Diagnostic ONLY */
void @near mainF_CSWM_Status(void);
/*For service parts and PN */
void @near mainF_OFF_NonSupported_Outputs_ServiceParts(void);
/********************************************************************************/


#endif /*MAIN_H_*/
