#ifndef AD_FILTER_H
#define AD_FILTER_H

/********************************************************************************/
/*                        A U T H O R   I D E N T I T Y							*/
/********************************************************************************/
/* Initials      Name                   Company									*/
/* --------      --------------------   --------------------------------------- */ 
/* CK            Can Kulduk             Continental Automotive Systems			*/
/* HK            Harsha Yekollu         Continental Automotive Systems          */
/* FU            Fransisco Uribe        Continental Automotive Systems          */
/********************************************************************************/


/*********************************************************************************/
/*                         CHANGE HISTORY for AD_FLITER                          */
/*********************************************************************************/
/* 01.17.2007 CK - header file created 											 */
/* 03.07.2008 HY - copied from CSWM  MY09                                        */
/* 04.08.2009 CK - New define is added for heater and STW Isense calculations.   */
/*                 New variables are added for heater and STW Isense calculation.*/
/*                 Two un-used defines (old Isense calculations) are commented.  */
/*********************************************************************************/


/********************************** @Include(s) **********************************/
#include "main.h"
/*********************************************************************************/


/******************************* @Constants/Defines *******************************/
#define AD_N13                          13      // Constant used in the normalization of Isense

//#define AD_N14                          14      // Heater Isense calculation constant	// NOT USED 04.08.2009

#define AD_N100                         100     // Another constant used in the normalization of Isense
#define AD_N49							49      // for Heater and STW New Isense calculations

#define AD_N42                          42      // Constant used in the normalization of Vent Vsense

#define AD_N3                           3       // Vent Isense calculation constant2

#define AD_N10                          10      // Vent Isense calculation constant3

#define AD_N8                           8       // Vent Vsense calculation constant

#define AD_N4                           4       // Heater Vsense calculation
#define AD_N2                           2       // Constant used in stW Isense calculation
#define AD_N1                           1       // Constant used in the calculation of Heater Vsense
//#define AD_N48                          48      // Constant used in the calculation of StW Isense	// NOT USED 04.08.2009

/* Heated seat partial open impedence detection */
#define AD_N61                          61      // Vsense multiplier  
#define AD_N5                           5       // Isense multiplier 
#define AD_MAX_Z_RSLT                   255     // Invalid impedence calculation result
#define AD_INVL_ISENSE                  0       // Invalid impedence calculation result
/*********************************************************************************/


/********************************** @Variables ***********************************/
extern u_8Bit	AD_Iref_seatH_c;		// current reference for Heated seats
extern u_16Bit  AD_Vref_seatH_w;		// voltage reference for Heated seats

extern u_8Bit	AD_Iref_STW_c;			// current reference for steering wheel
extern u_16Bit	AD_Vref_STW_w;          // voltage reference for steering wheel
/*********************************************************************************/


/******************************** @Data Structures *******************************/
/* Filter structure */
typedef struct{
    u_16Bit unflt_rslt_w;                          // unfiltered result
    u_16Bit avg_w;                                 // average
    u_16Bit flt_rslt_w;                            // filtered result  
}AD_Filter_s;

extern AD_Filter_s AD_mux_Filter[MUX_LMT][CH_LMT];      // one for each mux. Each mux has 8 channels (2x8).
extern AD_Filter_s AD_Filter_set[CH_LMT];               // one for each AD channel
/*********************************************************************************/


/****************************** @Function Prototypes *****************************/
/* For General Application Software */
@near void ADF_Init(void);
@near void ADF_Filter(u_8Bit AD_Filter_ch);
@near void ADF_Mux_Filter(u_8Bit AD_Filter_slctd_mux, u_8Bit AD_Filter_mux_ch);
@near void ADF_Str_UnFlt_Mux_Rslt(u_8Bit AD_Filter_feed, u_8Bit AD_Filter_mux_feed);
@near void ADF_Str_UnFlt_Rslt(u_8Bit AD_Filter_control);
@near void ADF_AtoD_Cnvrsn(u_8Bit AD_Filter_supply);
/* For Heated Seats and Steering Wheel */
@near u_8Bit ADF_Nrmlz_Vsense(u_16Bit AD_Vsense_value);
@near u_8Bit ADF_Nrmlz_Isense(u_16Bit AD_Isense_value);
@near u_8Bit ADF_Nrmlz_StW_Isense(u_16Bit AD_Stw_Isense_value);
@near u_8Bit ADF_Hs_PrtlOpen_Z_Cnvrsn(u_16Bit Vsense_AD, u_16Bit Isense_AD);
/* For Vents */
@near u_8Bit ADF_Nrmlz_Vs_Vsense(u_16Bit AD_vs_Vsense_value);
@near u_8Bit ADF_Nrmlz_Vs_Isense(u_16Bit AD_vs_Isense_value);
/*********************************************************************************/


#endif

