/********************************************************************************/
/*                        A U T H O R   I D E N T I T Y							*/
/********************************************************************************/
/* Initials      Name                   Company									*/
/* --------      --------------------   --------------------------------------- */ 
/* HK            Harsha Yekollu         Continental Automotive Systems          */
/* CC            Christian Cortes       Continental Automotive Systems          */
/********************************************************************************/


/********************************************************************************/
/*                   CHANGE HISTORY for MAIN MODULE                             */
/********************************************************************************/
/* 08.15.2006 CK    -  Module created                                			*/
/* 12.20.2007 CK	-  Module copied from CSWM MY09 project                     */
/* 12.20.2007 CK	-  Time Slice function activated           					*/
/* 12-20-2007 CK	-  MY10 Code added                         					*/
/* 01.25.2008 HY	-  Added the CAN NWM functions in time slice      			*/
/* 04.10.2008 HY	-  Added Heat, CanIO cyclic Tasks                 		    */
/* 04.11.2008 HY	-  Added Temperatue module, cyclic Tasks          			*/
/* 04.14.2008 HY	-  Added Relay Module Init and Timeslice tasks     			*/
/* 04.21.2008 HY	-  Added Venting Module time slice functionalities 			*/
/* 06.24.2008 FU	-  nwmngF_State_Transition added to 40ms	   	   			*/
/* 					   nwmngF_Init added to main								*/
/* 					   FMemLibF_Init added to main								*/
/* 					   FMemLibF_CheckDtcConditionTimer added to 320ms			*/
/* 					   FMemLibF_CyclicDtcTask added to 320ms	    			*/
/*					   stWF_Flt_Mntr()enabled in 20ms		  		    		*/
/*					   rlF_RelayDTC added in 80ms                  				*/
/*					   hsF_HeatDTC	added in 80ms								*/
/*				 	   stWF_DTC_Manager added in 80ms		   					*/
/*				  	   BattVF_DTC_Manager added in 160 ms						*/
/*				 	   MAIN_TMSLICE_PERIOD added								*/
/* 08.15.2008 CK   -   Added new EEPROM Driver includes.                	    */ 
/*				   -   Initilization Function (EE_PowerupInit).	   				*/
/*				   -   Added call to EE_Manager() in 5ms timslice  				*/
/* 10.27.2008 CK   -   Modified mainF_RemSt_Manager function in order to disable*/
/*                     Remote start automatic activation functinality if SNA    */
/* 					   is received in AMB_TEMP_AVG signal in IC_A1 message  	*/
/* 10.28.2008 CK   -   Commented out CclInitPowerOn in mainLF_Init function.    */
/*  				   CclInitPowerOn is being called in nwmngF_Init.			*/
/* 10.29.2008 CK   -   Propulsion system active check is added to               */
/*					   mainF_CSWM_Status function								*/
/* 11.12.2008 YH   -   HVAC LOC added to the mainF_CSWM_Status function         */
/* 11.24.2008 CK   -   Updates regarding to Load Shed Strategy for PowerNet.    */
/* 					   mainLF_Int and mainF_CSWM_Status functions modified.     */
/*                     New variable main_ldShd_mnt_stat_c created.              */
/*                      														*/
/* 12.16.2008 CK   -   Change the wait time after POR before we start updating  */
/*                     main_CSWM_status. Engine RPM signal is cyclicly (500ms)  */
/*                     received. We wait 810ms before updating main_CSWM_status.*/
/* 01.23.2009 CK   -   PC-Lint related updates.									*/
/* 02.13.2009 CK   -   Updated mainF_CSWM_Status function.                      */
/*                     Added a check of diag_rc_all_op_lock_b for engine rpm.   */
/*                     This will allow to run the DCX Routine Control CSWM      */
/*                     ouptut tests, if engine RPM is zero.                     */
/* 03.24.2009 CK   -   Modified 10ms timeslice. Took stWF_Manager outside of the*/
/*                     main wheel delay if statement.                           */
/*                     Modified 20ms timeslice. Deleted the if statement around */
/*                     stWF_Flt_Monitor function.                               */
/*                     These modifications are done to allow steering wheel     */
/*                     management (accepting switch requests etc.) right after  */
/*                     a POR. We used to wait 2 seconds for this. Now we are    */
/*                     waiting 2 seconds to read stW_Temp and stW_TempSenor_Flt */
/*                     signals from the CAN-I bus, but we receive the switch    */
/*                     requests turn on the output right after a POR.           */
/*                     MKS ID: 27476                                            */
/* 03.31.2009 CK   -   Enabled Ram test. Calling RamTst_Init() in Autosar       */
/*                     Initilizations. Finalized mainLF_App_RamTst() function.  */
/*                     This function is called in 10ms timeslice.               */
/* 04.08.2009 CK   -   Added a call to ADF_Init function in main.               */
/* 04.16.2009 HY   -   mainLF_Variant_Select () function modifed to support     */
/*                     service parts logic for Power Net series                 */
/*                 -   mainF_CSWM_Status () updated to have final_CBC_LOC_b and */
/*                     final_TGW_LOC_b.                                         */
/* 04.28.2009 HY   -   mainF_OFF_NonSupported_Outputs_ServiceParts () added     */
/* 11.25.2009 CC   -   mainF_RemSt_Manager FTR00139669- Autostart feature for   */
/*                     PowerNet Series											*/
/* 06.18.2010 HY   -   MKS 52743: mainF_RemSt_Manager updated to fix the Issue  */
/*                     Now Once Remote start is over for Power NEt, we enable the*/
/*                     Rear seats switch requests.                               */
/* 09.21.2010 HY   -   autostart_check_complete_b logic added for Power net section*/
/*                     under mainF_RemSt_Manager                                 */
/* 09.28.2010 HY   -   Autostart logic, ECU Configuration logic commented        */
/*                     #define PWR_NET commented                                 */
/********************************************************************************/
/***********************************************************************************************************
 * 05.13.2011 	Harsha   -  	Updated for CUSW: all the changes did for MY10 ported to the module.
 * 06-15-2011	Harsha	 75322	Cleanup the code. Delete the unused code for this program.
 * 								mainF_CSWM_Status updated by taking our loadshed monitor.
 * 08-10-2011   Harsha   81838  mainLF_Variant_Select updated based on the received PROXI.
 * 08-16-2011   Harsha   81849  FMemLibF_NMFailSts function called in 320ms TS.
 * 09-26-2011	Harsha	        PROXI and Real HW mismatch Logic updated in mainLF_Variant_Select function.
************************************************************************************************************/

#define MAIN_C


/********************************** @Include(s) **********************************/
#include "TYPEDEF.h"
#include "main.h"
#include "il_inc.h"
#include "v_inc.h"
//#include "ccl_inc.h"
//#include "Dpm.h"
#include "Canio.h"
#include "desc.h"
//#include "appdesc.h"
#include "DiagMain.h"
#include "DiagWrite.h"
#include "DiagIOCtrl.h"
#include "DiagRtnCtrl.h"
#include "heating.h"
#include "relay.h"
#include "FaultMemoryLib.h"
#include "BattV.h"
#include "StWheel_Heating.h"
#include "Venting.h"
#include "AD_Filter.h"
#include "ROM_Test.h"
#include "Temperature.h"
//#include "PRODUCT.h"
#include "Internal_Faults.h"
#include "Network_Management.h"
#include "mw_eem.h"
/* Autosar Includes */
#include "os.h"
#include "Adc.h"
#include "Dio.h"
#include "Gpt.h"
#include "Mcu.h"
#include "Pwm.h"
#include "Port.h"
#include "RamTst.h"
#ifdef WDG_EN
	#include "Wdg.h"
#endif
/********************************************************************************/


/*********************************** external ***********************************/
// extern @far void DPM_API_CALL_TYPE DpmExternalRequest(t_DpmUserNumber UserNumber);
// extern @far void CCL_API_CALL_TYPE Ccl_10_0msTaskCont (void);
// extern @far void CCL_API_CALL_TYPE Ccl_20_0msTaskCont(void); 
// extern @far void CCL_API_CALL_TYPE Ccl_10_2msTaskCont(void);
// extern @far void CCL_API_CALL_TYPE CclInitPowerOn(CCL_ECUS_NODESTYPE_ONLY);

//extern @far void EE_PowerupInit(void);
//extern @far void EE_Manager(void);

/*********************************************************************************/


/*************************** Local Functions Prototypes **************************/
void near mainLF_Init(void);
//void near mainF_CSWM_Status(void);
void near mainF_RemSt_Manager(void);
void near mainLF_Variant_Select(void); 				 //Selects the variant.
//extern void diagF_DCX_OutputTest(void);
/*********************************************************************************/


/*********************************** @ variables *********************************/
volatile union char_bit main_timeSlice_request_c;	 // Determines which time slice to serve
volatile union char_bit main_timeSlice_counter_c;	 // Counter used in time slice generation
Std_ReturnType main_InitClock_stat;					 // Clock Initilization status

//union char_bit main_flag_c;                          // main flags 
//#define nm_offset_2p5ms_b          main_flag_c.b.b0  // 2.5 ms offset for Tp tasks
//#define U12S_enable_b              main_flag_c.b.b1  // flag used to switch U12S on/off
//#define RAM_is_checked_b           main_flag_c.b.b2  // RAM is checked (once every POR)
//#define ROM_is_checked_b           main_flag_c.b.b3  // ROM is checked (once every POR)
//#define Allow_CSWM_status_Update_b main_flag_c.b.b4  // 800ms delay after every power on reset.
//#define Allow_stWheel_mngmnt_b     main_flag_c.b.b5  // 2 second delay for steering wheel
//#define RemSt_activated_b          main_flag_c.b.b6  // Activates CSWM remote start once for each request
//#define main_failure_b             main_flag_c.b.b7  //Main System failure occured for Heaters. Set the flag 

//union char_bit main_flag2_c;                         // main flags 
//#define Rear_U12S_enable_b        main_flag2_c.b.b0  // flag used to switch REAR U12S on/off

u_8Bit main_SystemV_stat_c;           				 // System voltage status
u_8Bit main_BatteryV_stat_c;          				 // Battery voltage status
u_8Bit main_ldShd_mnt_stat_c;   					 // Main load Shed monitor status variable.
u_8Bit main_PCB_temp_stat_c;          				 // PCB temperature status

u_8Bit main_CSWM_stat_delay_c;        // 800ms delay after powerOnReset to update main_CSWM_status 
u_8Bit main_wheel_delay_c;            // 2 second delay to start wheel management and fault detection

//u_8Bit main_RemSt_SWconfig_c;         // Enable (0x55) / Disable (0xAA) remote start software

u_8Bit main_AmbTemp_RemStart_Heat_c; // Holds eeprom value
u_8Bit main_AmbTemp_RemStart_Vent_c; // Holds eeprom value
/*********************************************************************************/
	
	
/******************************* @Constants/Defines ******************************/	
#define ENABLE_REMOTE_CONTROL_SW         0x55 // Enable Remote control software
//#define DISABLE_REMOTE_CONTROL_SW        0xAA // Disable Remote control software // NOT USED
/*********************************************************************************/
	
	
/************************************* Macros ************************************/
#define LoadStack_Pointer() {_asm("xref __stack"); _asm("lds #__stack");}
#define Modified_StartUp() {_asm("xref __Startup"); _asm("jsr __Startup");}
/*********************************************************************************/


/*********************************************************************************/
/* SA Text                                                                       */
/* -------                                                                       */
/* This function is called after a power on or a COP reset and re-initializes 	 */
/* all the variables that are used in the main module. 							 */
/*                                                                               */
/* SD Text                                                                       */
/* -------                                                                       */
/* Initialization of the variables that are used in the main module is performed */
/* here.                                               							 */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* not called                                                                    */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* no calls                                                                      */
/*																				 */
/*********************************************************************************/
void near mainLF_Init(void)
{
	main_timeSlice_counter_c.cb = 0;		// Time slice counter is cleared
	main_timeSlice_request_c.cb = 0;		// Time slice requests are cleared
	main_InitClock_stat = 0;				// Init clock state cleared	
	
	main_prms.Flt_Mtr_Tm   = MAIN_LED_DSPLY_TM;  // Default: 2 second short-term LED Display time
	main_prms.LED_Dsply_Tm = MAIN_FLT_MTR_TM;    // Default: 2 second fault mature time

	main_CSWM_Status_c = GOOD;              // every power on reset start with CSWM status as GOOD
	main_state_c = NORMAL;                  // CSWM default state: Normal functinality

	/* Make 100% sure following variables init to zero */
	main_CSWM_stat_delay_c = 0;             // 800ms delay after powerOnReset to update main_CSWM_status 
	main_wheel_delay_c = 0;                 // 2 second delay to start wheel management and fault detection
//	main_flag_c.cb =0;                      // Make sure all of these bits are cleared
	main_ECU_reset_rq_c = 0;                // Clear Diagnostic Service $11 $01 ECU Reset request
	main_RemSt_delay_c = 0;                 // 4 second Remote start delay (LHD_RHD cyclic 2000)
	main_U12S_keep_ON_b = 0;                // clear flag to keep U12S on
	main_kept_U12S_ON_b = 0;                // clear kept on flag

	mainLF_Variant_Select(); //Select the variant

	EE_BlockRead(EE_ENG_RPM_LIMIT,  &main_EngRpm_lowLmt_c);	// Store the Engine RPM low limit (disables CSWM ouputs)
	EE_BlockRead(EE_REMOTE_CTRL_HEAT_AMBTEMP, (u_8Bit*)&main_AmbTemp_RemStart_Heat_c);
	EE_BlockRead(EE_REMOTE_CTRL_VENT_AMBTEMP, (u_8Bit*)&main_AmbTemp_RemStart_Vent_c);

	main_SystemV_stat_c = V_STAT_GOOD;      // every power on reset start with System voltage status as GOOD
	main_BatteryV_stat_c = V_STAT_GOOD;     // every power on reset start with Battery voltage status as GOOD
	main_ldShd_mnt_stat_c = LDSHD_MNT_GOOD; // Load shed monitor status is good
	main_PCB_temp_stat_c = PCB_STAT_GOOD;   // every power on reset start with PCB temperature status as GOOD

	Pwm_SetDutyCycle(PWM_HS_FL, 0x0000); 
	Pwm_SetDutyCycle(PWM_HS_FR, 0x0000); 
	Pwm_SetDutyCycle(PWM_HS_RL, 0x0000); 
	Pwm_SetDutyCycle(PWM_HS_RR, 0x0000); 
	Pwm_SetDutyCycle(PWM_STW, 0x0000);
	Pwm_SetDutyCycle(PWM_VS_FL, 0x0000);
	Pwm_SetDutyCycle(PWM_VS_FR, 0x0000);
}


/********************************************************************************/
/*                        Variant Selection Functions                           */
/********************************************************************************/
void near mainLF_Variant_Select(void)
{
	/* This function gets called at the main Initialization time.                          */
	/* Through hardware we select the varaint.                                             */
	/* Variant   PE6(Pin 25) PE5(Pin 26) PE4(Pin 27)  Decimal   HFS   HRS   HSW   VFS      */
	/* V1          0           0           0           0        X                          */
	/* V2          1           0           0           4        X     X                    */
	/* V3          0           1           0           2        X           X     X        */
	/* V4          1           1           0           6        X           X              */
	/* V5          0           0           1           1        X                 X        */
	/* V6          1           0           1        `  5        X     X     X              */
	/* V7          0           1           1           3        X     X     X     X        */
	/* V8          1           1           1           7        X     X           X        */
	/* where,                                                                              */
	/* HFS = Heated Front Seats                                                            */
	/* HRS = Heated Rear Seats                                                             */
	/* HSW = Heated Steering Wheel                                                         */
	/* VFS = Vented Front Seats                                                            */
	/*                                                                                     */
	/* Defines from the hardware PIN readings                                              */
	/* #define CSWM_V1        0x00  $/$/ HFS                                               */
	/* #define CSWM_V2        0x04  $/$/ HFS + HRS                                         */
	/* #define CSWM_V3        0x02  $/$/ HFS + HSW + VFS                                   */
	/* #define CSWM_V4        0x06  $/$/ HFS + HSW                                         */
	/* #define CSWM_V5        0x01  $/$/ HFS + VFS                                         */
	/* #define CSWM_V6        0x05  $/$/ HFS + HRS + HSW                                   */
	/* #define CSWM_V7        0x03  $/$/ HFS + HRS + HSW + VFS                             */
	/* #define CSWM_V8        0x07  $/$/ HFS + HRS + VFS                                   */
	/*                                                                                     */
	/* Defines used for SW functionality and for Diagnstic service(Read configuration).    */
	/* #define CSWM_2HS_VARIANT_1_K            0x03                                        */
	/* #define CSWM_4HS_VARIANT_2_K            0x0F                                        */
	/* #define CSWM_2HS_2VS_HSW_VARIANT_3_K    0x73                                        */
	/* #define CSWM_2HS_HSW_VARIANT_4_K        0x43                                        */
	/* #define CSWM_2HS_2VS_VARIANT_5_K        0x33                                        */
	/* #define CSWM_4HS_HSW_VARIANT_6_K        0x4F                                        */
	/* #define CSWM_4HS_2VS_HSW_VARIANT_7_K    0x7F                                        */
	/* #define CSWM_4HS_2VS_VARIANT_8_K        0x3F                                        */

	/* At the time of Initialization, Logic gets the HW Variant Information from the module */
	/* Also checks the PROXI configuration from EEPROM. If nothing written to the EEPROM, Module */
	/* updates the default V4 information to the EEPROM.If written reads the info from EEPROM.	 */
	/* From the EEPROM, Logic checks for the outputs selected by either PROXI written/default    */
	/* If HW and PROXI EEP is not matching, HW Information over rides the PROXI.				 */

	unsigned char temp_hw_cfg_c;
	//Read the value from the PINs.
	main_variant_select_c = Dio_ReadChannelGroup(VarReadGrp);
	
	/* Also we receive the variant Information from the PROXI also */
	/* If PROXI and real HW are mis matching, Real HW over rides the PROXI Information */
	
	/* Check for V4. MY12 For MY12, we are using only the Variant 4 */
	if(main_variant_select_c == CSWM_V4)
	{
		temp_hw_cfg_c = CSWM_2HS_HSW_VARIANT_4_K;
	}
	else
	{
		/* If not V4, we still make the variant as V4 */
		temp_hw_cfg_c = CSWM_2HS_HSW_VARIANT_4_K;
	}

///* HW Support is for Variant 1? */
//	if (main_variant_select_c == CSWM_V1) 
//	{
//		/* Assign the SW functionality variable. */
//		CSWM_config_c = CSWM_2HS_VARIANT_1_K;
//		
//		/* HW Support is for Variant 2? */
//	} else if (main_variant_select_c == CSWM_V2){
//		/* Assign the SW functionality variable. */
//		CSWM_config_c = CSWM_4HS_VARIANT_2_K;
//		/* HW Support is for Variant 3? */	
//	} else if (main_variant_select_c == CSWM_V3) {
//		/* Assign the SW functionality variable. */
//		CSWM_config_c = CSWM_2HS_2VS_HSW_VARIANT_3_K;
//		/* HW Support is for Variant 4? */	
//	} else if (main_variant_select_c == CSWM_V4) {
//		/* Assign the SW functionality variable. */
//		CSWM_config_c = CSWM_2HS_HSW_VARIANT_4_K;
//		/* HW Support is for Variant 5? */		
//	} else if (main_variant_select_c == CSWM_V5) {
//		/* Assign the SW functionality variable. */
//		CSWM_config_c = CSWM_2HS_2VS_VARIANT_5_K;
//		/* HW Support is for Variant 6? */
//	} else if (main_variant_select_c == CSWM_V6) {
//		/* Assign the SW functionality variable. */
//		CSWM_config_c = CSWM_4HS_HSW_VARIANT_6_K;
//		/* HW Support is for Variant 7? */
//	} else if (main_variant_select_c == CSWM_V7) {

		/* Read PROXI Configuration from EEPROM */
		EE_BlockRead(EE_PROXI_CFG, (UINT8*)&ru_eep_PROXI_Cfg);
		hardware_proxi_mismatch_c = FALSE;

	/* PROXI Configuration is not updated. First Load with the default Values */
	if(ru_eep_PROXI_Cfg.byte[0] == 0xFF)
	{
		//memset((u_8Bit *)&ru_eep_PROXI_Cfg, PROXI_ID_VEH_SIZE + PROXI_CRC_SIZE - 1, PROXI_CFG_CODE_DEFAULT);
		ru_eep_PROXI_Cfg.byte[0] = PROXI_CFG_CODE_DEFAULT;
		ru_eep_PROXI_Cfg.byte[1] = PROXI_CFG_CODE_DEFAULT;
		ru_eep_PROXI_Cfg.byte[2] = PROXI_CFG_CODE_DEFAULT;
		ru_eep_PROXI_Cfg.byte[3] = PROXI_CFG_CODE_DEFAULT;
		ru_eep_PROXI_Cfg.byte[4] = PROXI_CFG_CODE_DEFAULT;
		ru_eep_PROXI_Cfg.byte[5] = PROXI_CFG_CODE_DEFAULT_LAST_BYTE;
			
		ru_eep_PROXI_Cfg.s.Driver_Side = PROXI_DS_DEFAULT;
		ru_eep_PROXI_Cfg.s.Remote_Start = PROXI_RS_DEFAULT;
		ru_eep_PROXI_Cfg.s.Vehicle_Line_Configuration = PROXI_VEH_LINE_DEFAULT;
		ru_eep_PROXI_Cfg.s.Country_Code = PROXI_COUNTRY_CODE_US;
		ru_eep_PROXI_Cfg.s.Model_Year = PROXI_MY_DEFAULT;
		ru_eep_PROXI_Cfg.s.Heated_Seats_Variant = PROXI_HEATSEAT_DEFAULT;
		ru_eep_PROXI_Cfg.s.Seat_Material = PROXI_SEAT_TYPE_CLOTH;
		ru_eep_PROXI_Cfg.s.Wheel_Material = PROXI_WHEEL_TYPE_WOOD;
		ru_eep_PROXI_Cfg.s.Steering_Wheel = PROXI_WHEEL_PRESENT;
			
		EE_BlockWrite(EE_PROXI_CFG, (UINT8*)&ru_eep_PROXI_Cfg);
			
	}

	/* Check PROXI configuration for Heated Seats */
	if(ru_eep_PROXI_Cfg.s.Heated_Seats_Variant == PROXI_HEATSEAT_FRONT)
	{
		/* Load the PROXI configuration for Front Heated Seats */
		CSWM_config_c = CSWM_2HS_VARIANT_1_K;
	}
	else
	{
		/* Load the PROXI configuration for Front and Rear Heated Seats */
		CSWM_config_c = CSWM_4HS_VARIANT_2_K;
	}
	
	/* Check PROXI configuration for Heated Steering Wheel */
	if(ru_eep_PROXI_Cfg.s.Steering_Wheel == PROXI_WHEEL_PRESENT)
	{
		/* Update the already loaded PROXI with the Wheel configuration */
		CSWM_config_c |= CSWM_HSW_MASK_K;
	}

	//HW PROXI MISMATCH INFORMATION CHECKED for F10B SERVICE
	if( (ru_eep_PROXI_Cfg.byte[0] == PROXI_CFG_CODE_DEFAULT) &&
		(ru_eep_PROXI_Cfg.byte[1] == PROXI_CFG_CODE_DEFAULT) &&
		(ru_eep_PROXI_Cfg.byte[2] == PROXI_CFG_CODE_DEFAULT))
	{
		hardware_proxi_mismatch_c = PROXI_NOT_PRGMED;
	}
	else
	{
		/* Now test the HW Configuration vs the Learned PROXI configuration */
		if (CSWM_config_c == temp_hw_cfg_c ) 
		{
			//Variant 4 No mismatch	
			hardware_proxi_mismatch_c = PROXI_PRGMED_OK;

		}
		else
		{		
			//Mis match between the PROXI and HW. Use the HW variant Information to turn ON the outputs */
			CSWM_config_c = temp_hw_cfg_c;
			hardware_proxi_mismatch_c = PROXI_HW_MISMATCH;
		}
		
	}

} //End of Variant selection function.


/*********************************************************************************/
/* SA Text                                                                       */
/* -------                                                                       */
/* Interrupt driven time slice scheduler generated using the general purpose     */
/* timer channel 7. The interrupt generation rate is 1.25ms. 					 */
/*                                                                               */
/* SD Text                                                                       */
/* -------                                                                       */
/* This function controls which time slice should be executed next.              */
/* It increments an 8 bit variable, main_timeSlice_counter_c, starting from 0.   */
/* The least significant bit that is set represents the time slice will be       */
/* activated next.    															 */
/* For example:                                                                  */
/* 0 0 0 0 0 0 0 1 => 2.5 ms time slice                                          */
/* 0 0 0 0 0 0 1 0 => 5   ms time slice                                          */
/* 0 0 0 0 0 0 1 1 => 2.5 ms time slice                                          */
/* 0 0 0 0 0 1 0 0 => 10  ms time slice                                          */
/* ...                                                                           */
/* 0 0 0 0 1 0 0 0 => 20  ms time slice                                          */
/* ...                                                                           */
/* 0 0 0 1 0 0 0 0 => 40  ms time slice                                          */
/* ...                                                                           */
/* 0 0 1 0 0 0 0 0 => 80  ms time slice                                          */
/* ...                                                                           */
/* 0 1 0 0 0 0 0 0 => 160 ms time slice                                          */
/* ...                                                                           */
/* 1 0 0 0 0 0 0 0 => 320 ms time slice                                          */
/*                                                                               */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* not called                                                                    */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* mainF_Report_TmSlice_Overrun()                                                */
/*																				 */
/*********************************************************************************/
void near mainF_TimeSliceGen()
{
	main_timeSlice_counter_c.cb++;	// increment the time slice counter
	
	// Time Slice Generation
	if(main_timeSlice_counter_c.b.b0 == TRUE)
	{
		main_timeSlice_request_c.b.b0 = TRUE;	// Set 2.5ms request
		
	}else if(main_timeSlice_counter_c.b.b1 == TRUE){
		
		main_timeSlice_request_c.b.b1 = TRUE;	// Set 5ms request	
		
	}else if(main_timeSlice_counter_c.b.b2 == TRUE){
		
		main_timeSlice_request_c.b.b2 = TRUE;	// Set 10ms request
		
	}else if(main_timeSlice_counter_c.b.b3 == TRUE){
		
		main_timeSlice_request_c.b.b3 = TRUE;	// Set 20ms request
		
	}else if(main_timeSlice_counter_c.b.b4 == TRUE){
		
		main_timeSlice_request_c.b.b4 = TRUE;	// Set 40ms request
		
	}else if(main_timeSlice_counter_c.b.b5 == TRUE){
		
		main_timeSlice_request_c.b.b5 = TRUE;	// Set 80ms request
		
	}else if(main_timeSlice_counter_c.b.b6 == TRUE){
		
		main_timeSlice_request_c.b.b6 = TRUE;	// Set 160ms request
		
	}else if(main_timeSlice_counter_c.b.b7 == TRUE){
		
		main_timeSlice_request_c.b.b7 = TRUE;	// Set 320ms request		
	}
}

#ifdef RAM_TEST
/****************************************************************************************/
/* Function Name: 	void near mainLF_App_RamTst(void)						            */
/* Type: 			Local Function														*/
/* Returns: 		None																*/
/* Parameters: 		None																*/
/* Description:     This function checks if RAM test is still running.					*/
/*					If it is, RAM check for next 'n' number of bytes is performed.      */
/*					When RAM test completes, it checks for RAM error.					*/
/*					If necassary, it sets or clears the internal fault.					*/
/*																						*/
/* Calls to: 		RamTst_MainFunction()												*/
/*					RamTst_GetTestResult()												*/
/*					RamTst_Stop()														*/
/*					IntFlt_F_Chck_Ram_Err()												*/
/*					IntFlt_F_Clr_Ram_Err()   											*/
/*																						*/
/* Calls from:      main() - 2.5ms timeslice											*/
/* 																						*/
/****************************************************************************************/
void near mainLF_App_RamTst(void)
{
	/* Check if RAM test is still running */
	if(main_RAM_test_rslt == RAMTST_RESULT_NOT_TESTED){
		/* Perform the RAM test for next 'n' number of bytes. 'n' is set in Halcogen tool */
		/* 'n' is defined as RAMTST_CHECKERBOARD_NUMBER_OF_TESTED_CELLS in RamTst_Cfg.h */
		RamTst_MainFunction();
		/* Keep checking if we are done with the RAM test */
		main_RAM_test_rslt = RamTst_GetTestResult();
	}else{
		/* Check if we have not performed the test result check */
		if(RAM_is_checked_b == FALSE){
			/* Stop the RAM test*/
			RamTst_Stop();
			/* Set an internal fault if it is necassary */
			IntFlt_F_Chck_Ram_Err();
		}else{
			/* RAM test result is checked. Clear the internal fault if it is set. */
			IntFlt_F_Clr_Ram_Err();
		}
	}

//	/* Perform the RAM test for next 'n' number of bytes. 'n' is set in Halcogen tool */
//	RamTst_MainFunction();
//	/* Keep checking if we are done with the RAM test */
//	main_RAM_test_rslt = RamTst_GetTestResult();
//	/* Check if we have not performed the test result check */
//	if(RAM_is_checked_b == FALSE){
//		/* Stop the RAM test*/
//		RamTst_Stop();
//		/* Set an internal fault if it is necassary */
//		IntFlt_F_Chck_Ram_Err();
//	}else{
//		/* RAM test result is checked. Clear the internal fault if it is set. */
//		IntFlt_F_Clr_Ram_Err();
//	}
}
#endif	

/****************************************************************************************/
/* SA Text                                                                              */
/* -------                                                                              */
/* Execution of the application code start with this function when the microcontroller  */
/* is supplied with power.																*/
/*                                                                                      */
/* SD Text                                                                              */
/* -------                                                                              */
/* Main function is responsible of the following tasks:                                 */
/* 1. Initilization of all autosar and application modules whenever there is any kind   */
/* of reset.                															*/
/* 2. Scheduling of cyclic functions with a interrupt driven operating system concept.  */
/*                                                                                      */
/*                                                                                      */
/*                                                                                      */
/* Calls from                                                                           */
/* --------------                                                                       */
/* not called                                                                           */
/*                                                                                      */
/* Calls to                                                                             */
/* --------                                                                             */
/*																						*/
/****************************************************************************************/
void near main(void)
{
	/* Start of Program */
	DisableAllInterrupts();	// Disable all interrupts for initilizations	
	//RPAGE = 0xFD; 		// Default RPAGE reg. value: ensures a RAM space between 0x1000 to 0x3FFF
	//PPAGE = 0xFE; 		// Default PPAGE reg. value: ensures a Flash space between 0x4000 to 0xFFFF
	//EPAGE = 0xFE; 		// Default EPAGE reg. value: ensures a Data Flash space between 0x0800 to 0x0FFF	
	LoadStack_Pointer();	// load stack pointer
	Modified_StartUp();	// Jump to modified start-up sub-routine
#ifdef BOOTLOADER	
	IVBR = 0x40;
#endif	
	
	/* Autosar Initilizations */
	Mcu_Init(&Mcu_Config);	
	main_InitClock_stat = Mcu_InitClock(CLK_SETTING_0);
	Port_Init(&Port_Config[PORT_config_0]);
#ifdef WDG_EN
    Wdg_Init(&Wdg_Config[WDG_config0]);
#endif    
    RamTst_Init();	  
	Gpt_Init(&Gpt_Config[GPT_config_0]);	
	DisableAllInterrupts();		// Gpt_Init Enables all interrupts	
	Gpt_StartTimer(TimeSlice_CH2,(Gpt_ValueType)MAIN_TMSLICE_PERIOD);	
	DisableAllInterrupts();		// Gpt_StartTimer Enables all interrupts	
	Gpt_EnableNotification(TimeSlice_CH2);	
	Pwm_Init(&Pwm_Config[PWM_config_0]);	
	DisableAllInterrupts();		// Pwm_Init Enables all interrupts
	Adc_Init(&Adc_Config[ADC_CONFIG_0]);	
	
	DisableAllInterrupts();
//    nwmngF_Init();
//    EE_PowerupInit(); 	
//	mainLF_Init();		// main Init
//	canF_Init();	
//    diagF_Init();
//    EE_LoadDefaults ();  

	nwmngF_Init();
	EE_PowerupInit(); 	
	diagF_Init();
	EE_LoadDefaults();  
	
	mainLF_Init();		// main Init
	canF_Init();
	
	ADF_Init();         // ADC module init
    IntFlt_F_Init();    // internal faults init
    ROMTstF_Init();		// rom test int
	BattVF_Init();	    // Voltage monitor init	
	rlF_InitRelay();	// relay init
	hsF_Init ();		// heaters init
	stWF_Init();		// steering wheel init
	vsF_Init();			// vents init
	FMemLibF_Init();	// DTC Fault Memory Library Init
	TempF_Init ();		// Temperature module init

	/*Watchdog Fast mode (watchdog timeout period is = 8 ms). */
#ifdef WDG_EN
    Wdg_Trigger();
    Wdg_SetMode(WDG_FAST_MODE);
#endif	
    
	EnableAllInterrupts();	// Initilizations are completed. Enabling all interrupts		
  nmCan_Start_IL();     // Start IL Rx and Tx
	
	while(TRUE)
	{
		if(main_timeSlice_request_c.b.b0 == TRUE)
		{
			/* 2.5ms Tasks */		
			//Dio_WriteChannel(TSLICE_PIN, STD_HIGH);	// Used for Time Slice Measurements			
#ifdef WDG_EN
                      
			/* Check if we have a Diagnostic request for ECU reset */
			if (main_ECU_reset_rq_c == 0) {			
				/* Trigger Watchdog and reset its timeout period */
				Wdg_Trigger();				
			}
			else {				
				/* Do not trigger watchdog. We will get a COP reset. */				
			}
#endif			
                      
			/* Perform U12S AtoD conversion */
			//DDRE  |= 0x80;                               /* Data direction 7 I hope this becomes an output*/
                        //PORTE = PORTE^0x80; 
                        TempF_U12S_ADC();
			
			/* Heated seat AtoD conversions */
			hsF_AD_Readings();
			
			/* Check if we are supporting steering wheel */
			if ( (CSWM_config_c & CSWM_HSW_MASK_K) == CSWM_HSW_MASK_K)
			{	
				/* StWheel AtoD conversions */
				stWF_AD_Rdng();
			}
			else
			{				
				/* This variant does not carry StWheel */
			}
			
			/* Check if we are supporting vents */
			if ( (CSWM_config_c & CSWM_2VS_MASK_K) == CSWM_2VS_MASK_K)
			{				
				/* Vent AD AtoD conversions */
				vsF_ADC_Monitor();
			}
			else
			{				
				/* This variant does not carry Vents */
			}
			
			//Dio_WriteChannel(TSLICE_PIN, STD_LOW);	// Used for Time Slice Measurements
			/* Once we are done, clear the request bit for 2.5ms time slice */
			main_timeSlice_request_c.b.b0 = FALSE;
                       
			
		}else if(main_timeSlice_request_c.b.b1 == TRUE){
			/* 5ms Tasks */
                        //DDRE  |= 0x80;                               /* Data direction 7 I hope this becomes an output*/
                        //PORTE |= 0x80;                               /* set the Port pin PE7 high I hope pin 24 on micro*/ 
			/* If self clock mode is detected try to set the oscillator internal fault */
			IntFlt_F_Chck_Osci_Err();
			
			/* Local Battery Voltage AD Reading (worst case 210us) */
			/* CAUTION: Has to be called before mainF_CSWM_Status. */
			BattVF_AD_Rdg();
			
//			/* Go to the Rear LED U12S switching only incase Variant supports 4H functionality */
//			/* Harsha commneted. Enaling the U12S PIN logic done at heat manager in Heating.c */
//			/* And if switch requests are from Hardware */
//			if (((CSWM_config_c & CSWM_4HS_VARIANT_MASK_K) == CSWM_4HS_VARIANT_MASK_K) && (main_SwReq_Rear_c == VEH_SWITCH_REAR_ONLY_K))
//			{
//				/* Switch power to Rear LED U12S circuit on/off */
//				Rear_U12S_enable_b = ~Rear_U12S_enable_b;
//
//				/* @Rear LED U12S Control */
//				if (Rear_U12S_enable_b == TRUE) {
//					/* Enable Rear U12S for switch readings */
//					Dio_WriteChannel(LED_U12S_EN, STD_HIGH);
//				}
//			}
			
			hsF_HeatRequestToFet();

			if ((CSWM_config_c & CSWM_HSW_MASK_K) == CSWM_HSW_MASK_K)
			{
				/* StWheel Output control */
				stWF_Output_Control();
			}
			else
			{
				/* This variant does not carry StWheel */
			}

			/* Check if we are supporting vents */
			if ((CSWM_config_c & CSWM_2VS_MASK_K) == CSWM_2VS_MASK_K)
			{
				/* Vent Output Control */
				vsF_Output_Cntrl();
			}
			else
			{	
				/* This variant does not carry Vents */
			}
			
			/* Flash Driver Manager */
			EE_Manager();
			
			/* Once we are done, clear the request bit for 5ms time slice */
			main_timeSlice_request_c.b.b1 = FALSE;
                        //PORTE &= 0x7F;                                 /* shut off port for task time measurement*/ 
			
		}else if(main_timeSlice_request_c.b.b2 == TRUE){
			/* 10ms Tasks */
			//Dio_WriteChannel(TSLICE_PIN, STD_HIGH);	// Used for Time Slice Measurements
	    	        //DDRE  |= 0x80;                               /* Data direction 7 I hope this becomes an output*/
                        //PORTE |= 0x80;                               /* set the Port pin PE7 high I hope pin 24 on micro*/ 
                        /* Cyclic Check for PLL lock */
                         IntFlt_F_Chck_PLL();
                         nmCan_Cyclic_Tasks();
			/* 810ms delay after every power on reset. Then, start updating the main_CSWM_status (BCM_A7, FCM_A1, FCM_A2 messages cyclic update every 100ms) We also wait for our own PCB temperature reading */
			if (Allow_CSWM_status_Update_b == TRUE) {
				/* Start monitoring the CSWM status */
				mainF_CSWM_Status();
			}
			else {
				/* Keep waiting */
			}
			//Ccl_10_0msTaskCont(); //Harsha Added	
			//Ccl_10_2msTaskCont(); //Harsha Added			
			
			rlF_RelayManager ();
			
			hsF_HeatManager ();

			/* Check if we are supporing steering wheel */
			if ( (CSWM_config_c & CSWM_HSW_MASK_K) == CSWM_HSW_MASK_K)
			{
				/* 2 second delay to recevie HSW_TEMP and HSW_TempSensFlt */
				if (main_wheel_delay_c < MAIN_2SEC_DELAY)
				{
					/* Keep waiting 2 seconds */
					main_wheel_delay_c++;
				}
				else
				{
					/* We should receive the wheel temperature feedbacks. Allow management. */
					/* Modified on 03.24.2009 by CK. This bit is now used to allow to read */
					/* steering wheel temperature and steering wheel sensor fault status in */
					/* StWheel_Heating module. See stWF_Rdg_Temperature function. */
					/* MKS ID: 27476 */
					Allow_stWheel_mngmnt_b = TRUE;
				}
				/* Modified on 03.24.2009 by CK to allow management right after a POR. */
				/* MKS ID: 27476 */
				/* StWheel Manager */
				stWF_Manager();
			}
			else
			{
				/* This variant does not carry StWheel */
			}
			
			/* Check if we are supporting vents */
			if ((CSWM_config_c & CSWM_2VS_MASK_K) == CSWM_2VS_MASK_K)
			{
				/* Vs Manager */
				vsF_Manager();
			}
			else
			{
				/* This variant does not carry Vents */
			}
			
			diagF_CyclicWriteTask();
			
			/*  Evaluate remote start signal*/
			canF_CyclicRemoteStart();
			
			/* Cyclic Call for all Indication Functions */
			canF_CyclicManIndFunc();
			
			/* RAM test. Should finish execution in approximately 6 seconds after POR. */
			/* During first 6 seconds, it takes about 1ms to execute this timeslice */
			/* Then, execution time of this timeslice goes down to about 530ms */
#ifdef RAM_TEST			
			mainLF_App_RamTst();
#endif			
			//Dio_WriteChannel(TSLICE_PIN, STD_LOW);	// Used for Time Slice Measurements						
			/* Once we are done, clear the request bit for 10ms time slice */
			main_timeSlice_request_c.b.b2 = FALSE;
                        //PORTE &= 0x7F; 
			
		}else if(main_timeSlice_request_c.b.b3 == TRUE){
			/* 20ms Tasks */
			//Ccl_20_0msTaskCont(); //Harsha Added
			
			/* Check for 4 second Remote start delay (LHD_RHD cyclic 2000) */
			if (main_RemSt_delay_c < MAIN_REMST_DELAY)
			{
				/* Increment remote start counter */
				main_RemSt_delay_c++;
			}
			else
			{
				/* Monitor for Remote Start activation */
				mainF_RemSt_Manager();
			}

			/* Check if we are supporing steering wheel */
			if ( (CSWM_config_c & CSWM_HSW_MASK_K) == CSWM_HSW_MASK_K)
			{
//				/* ORIGINAL */			
//				/* 2 second delay for wheel management (BCM_A8 cyclic 1000ms) (SCCM_A1 100ms) (StW_ActnRq 500ms) is completed */
//				if (Allow_stWheel_mngmnt_b == TRUE)
//				{
//					/* monitor for stWheel fault */
//					stWF_Flt_Mntr();
//				}
//				else
//				{
//					/* Wait until we start the stWheel management before allowing fault detection */
//				}				
				/* Modified on 03.24.2009 by CK. Allow fault monitoring right after a POR. */
				/* MKS ID: 27476 */
				/* monitor for stWheel fault */
				stWF_Flt_Mntr();				
			}
			else
			{
				/* This variant does not carry StWheel */
			}
			
			/* Check if ROM test execution is finished */
			if((ROMTst_status != ROMTST_EXECUTION_FINISHED)){
				/* ROM Test (Completes in about ??? seconds) */
				ROMTstF_Calc_ChckSum();
			}else{
				
				/* Check for ROM checksum error once every POR */
				if(ROM_is_checked_b == FALSE) {
					/* ROM test execution is finished. Check for ROM checksum error */
					ROMTstF_Chck_Err();
				}else{
					/* ROM checksum is checked. Clear the ROM error if necassary. */
					ROMTstF_Clr_Rom_Err();
					
				} // Check for ROM checksum error once every POR
			}	// Check if ROM test execution is finished
			
			/* DCX Routine Control function to run output tests */
			diagF_DCX_OutputTest();		
						
			/* Once we are done, clear the request bit for 20ms time slice */
			main_timeSlice_request_c.b.b3 = FALSE;
                       
			
		}else if(main_timeSlice_request_c.b.b4 == TRUE){
			/* 40ms Tasks */			
			/* Transit the ecu state from Local to External or viceversa */
			nwmngF_State_Transition();			
			
			/* 800ms delay after every power on reset. Then, start updating the Voltage status (FCM_A1 and FCM_A2 messages cyclic update every 100ms) */
			if (main_CSWM_stat_delay_c < MAIN_CSWM_STAT_DELAY)
			{
				/* Keep waiting until 800ms delay expires */
				main_CSWM_stat_delay_c++;
			}
			else
			{
				/* Start Voltage Status Monitor */
				BattVF_Manager();

				/* We got the system voltage reading allow main_CSWM_status update */
				Allow_CSWM_status_Update_b = TRUE;
			}

			/* Updates the Original VIN,Current VIN and Vehicle Info */
			canF_CyclicTask();
			
			/* Check for PCB sensor occurance updates - Performs update only if it is necassary (only once per ignition cycle) */
			//TempF_Chck_PCB_Sensor_Updt();  // NOT USED FOR MY11

			/* Check for PCB Sensor Error Updates */
			//TempF_Upt_PCB_Sensor_Err();	// NOT USED FOR MY11
			
			/* Sends the RoE Message when ever necessary */
//			FMemLibF_SendROE_Light();
			
			//IF Module configures with 4HS and Vehicle equipped with Hardware switches for rear seats 
			//Do the fault detection for rear seat switch stuck detection
			if (((CSWM_config_c & CSWM_4HS_VARIANT_MASK_K) == CSWM_4HS_VARIANT_MASK_K) && (main_SwReq_Rear_c == VEH_SWITCH_REAR_ONLY_K))
			{
				hsF_RearSwitchStuckDTC();
			}
			
			/* Turn OFF not supported outputs for service parts */
			mainF_OFF_NonSupported_Outputs_ServiceParts();
			
			/* Check if we are supporing steering wheel */
			if ( (CSWM_config_c & CSWM_HSW_MASK_K) == CSWM_HSW_MASK_K)
			{
				/* MKS ID: 66832 */
				/* monitor for Isense current limit set and timers. */
				stWF_CurrentLimit_Timer();				
			}
			else
			{
				/* This variant does not carry StWheel */
			}
			
			/* Once we are done, clear the request bit for 40ms time slice */
			main_timeSlice_request_c.b.b4 = FALSE;
			
		}else if(main_timeSlice_request_c.b.b5 == TRUE){
			/* 80ms Tasks */			
//			/* Switch power to thermistor circuit on/off */
//			U12S_enable_b = ~U12S_enable_b;
//
//			/* @U12S Control (to avoid over heating seat thermistor circuit) */
//			if (U12S_enable_b == TRUE)
//			{
//				/* Enable U12S for thermistor readings */
//				Dio_WriteChannel(U12T_EN, STD_HIGH);
//			}		
			
			if(relay_C_keep_U12S_ON_b == TRUE){
				/* Enable U12S for thermistor readings */
				Dio_WriteChannel(U12T_EN, STD_HIGH);
				/* A flag will be set in Temperature module to let Relay know that U12S has been kept on for at least for 40ms */
				main_U12S_keep_ON_b = TRUE;				
			}else{
				/* Clear the flag to disable keeping U12S On for Temperature module */
				main_U12S_keep_ON_b = FALSE;
				
				/* Switch power to thermistor circuit on/off */
				U12S_enable_b = ~U12S_enable_b;
				/* @U12S Control (to avoid over heating seat thermistor circuit) */
				if (U12S_enable_b == TRUE)
				{
					/* Enable U12S for thermistor readings */
					Dio_WriteChannel(U12T_EN, STD_HIGH);
				}				
			}
			
			rlF_RelayDTC();

			hsF_HeatDTC();
			
			IntFlt_F_DTC_Monitor();

			/* Check if we are supporting vents */
			if ( (CSWM_config_c & CSWM_2VS_MASK_K) == CSWM_2VS_MASK_K)
			{
				/* Vents Fault Detection */
				vsF_Fault_Mntr();

				/* Vent related DTC manager */
				vsF_DTC_Manager();
			}
			else
			{
				/* This variant does not carry Vents */
			}

			/* Check if we are supporting steering wheel */
			if ( (CSWM_config_c & CSWM_HSW_MASK_K) == CSWM_HSW_MASK_K)
			{
				/* StWheel DTC manager */
				stWF_DTC_Manager();
			}
			else
			{
				/* This variant does not carry StWheel */
			}
			
			/* Once we are done, clear the request bit for 80ms time slice */
			main_timeSlice_request_c.b.b5 = FALSE;
			
		}else if(main_timeSlice_request_c.b.b6 == TRUE){
			/* 160ms Tasks */			
			/* Enable power supply for thermistor circuit */
			U12S_enable_b = TRUE;
			
			/* Thermistor temperature readings for each heated seat (execution tm: 341us) */
			TempF_Monitor_ADC();

			/* Call this function after getting the heated seat NTC reading. */
			/* Heated seat NTC Fault Monitor                                 */
			TempF_HS_NTC_Flt_Monitor();

			/* PCB Temperature AD reading */
			TempF_PCB_ADC();

			/* Call this function after getting the PCB AD reading. */
			/* PCB NTC Fault Monitor                                */
			TempF_PCB_NTC_Flt_Monitor();

			/* System and Battery voltage DTCs */
			BattVF_DTC_Manager();

			/* Heated seat NTC DTCs */
			hsF_NTCDTC();
			
			/* Once we are done, clear the request bit for 160ms time slice */
			main_timeSlice_request_c.b.b6 = FALSE;
			
		}else if(main_timeSlice_request_c.b.b7 == TRUE){
			/* 320ms Tasks */	
			//Dio_WriteChannel(TSLICE_PIN, STD_HIGH);	// Used for Time Slice Measurements
			/* Monitor time and stepdown logic */
			hsF_MonitorHeatTimes();
			
			/* Check if we are supporing steering wheel */
			if ((CSWM_config_c & CSWM_HSW_MASK_K) == CSWM_HSW_MASK_K)
			{
				/* stWheel Timer */
				stWF_Timer();
			}
			else
			{
				/* This variant does not carry StWheel */
			}

			FMemLibF_CheckDtcConditionTimer();
			
			FMemLibF_NMFailSts();
						
			/* Cyclic DTC Task */
			FMemLibF_CyclicDtcTask();
			
			/* Once we are done, clear the request bit for 320ms time slice */
			main_timeSlice_request_c.b.b7 = FALSE;
			//Dio_WriteChannel(TSLICE_PIN, STD_LOW);	// Used for Time Slice Measurements
		}	// end of else if
	}	// end of while(TRUE)
}	// end of main(void)


/****************************************************************************************/
/* This function gets called every 10ms.                                                */
/* It determines the CSWM output status by examining:                                   */
/* 1. Loss of communication with CBC and/or TGW nodes (PN)                              */
/* 2. PCB over temperature                                                              */
/* 3. Highest priority internal faults (which disable CAN communication - PLL,          */
/* oscillator, ROM error etc)                           								*/
/* 4. Engine RPM                                                                        */
/* 5. Ignition status                                                                   */
/* 6. Load shed status                                                                  */
/* 7. System and battery voltage status                                                 */
/* This function sets the main_CSWM_status_c varible to three different states.         */
/*																						*/
/* 1. main_CSWM_status_c = GOOD                                                         */
/* The user is allowed to turn on an output. There will be continous LED display if an  */
/* output is turned on.                              									*/
/*																						*/
/* 2. main_CSWM_status_c = BAD                                                          */
/* The user is not allowed to turn on an output. However, there will be continous LED   */
/* display if the user tries to turn on an output.    									*/
/*																						*/
/* 3. main_CSWM_status_c = UGLY                                                         */
/* The user is not allowed to turn on an output. There will be a short term 2 second LED*/
/* display if the user tries to turn on an output. 										*/
/*																						*/
/****************************************************************************************/
void near mainF_CSWM_Status(void)
{
	/* Get the most recent system and battery voltage status and PCB temperature status */
	main_SystemV_stat_c = BattVF_Get_Status(SYSTEM_V);

	main_BatteryV_stat_c = BattVF_Get_Status(BATTERY_V);
	
	main_ldShd_mnt_stat_c = BattVF_Get_LdShd_Mnt_Stat();

	main_PCB_temp_stat_c = TempF_Get_PCB_NTC_Stat();

	/* *** ATTENTION: Not enabled all conditions. At beginning ***                                                             */
	/* Check for final FCM or CCN faults, PCB over temperature, and high priority internal faults (set in this ignition cycle) */

	if ( (final_CBC_LOC_b == FALSE) && 
		(final_TGW_LOC_b == FALSE) &&
		(main_PCB_temp_stat_c != PCB_STAT_OVERTEMP) && 
		(int_flt_byte0.cb == NO_INT_FLT) &&
		(canio_canbus_off_b == FALSE))
	{
		/* 1. Check Engine RPM With new conversion If it is < 416 we will not activate outputs.*/
		/* Either we need to go by 384 or 416 */
		if ((canio_RX_EngineSpeed_c <  main_EngRpm_lowLmt_c ) && 
		    (diag_io_active_b == FALSE) && (diag_rc_all_op_lock_b == FALSE) )
		{
			/* Set CSWM status to UGLY */
			main_CSWM_Status_c = UGLY;
		}
		else
		{
			/* 2. Check Ignition Status */
			if (canio_RX_IGN_STATUS_c != IGN_RUN)
			{
				/* Is it IGN_START? */
				if (canio_RX_IGN_STATUS_c == IGN_START)
				{
					/* Check all other conditions before setting CSWM status to BAD (load shed, matured voltage faults) */
					if ( (main_ldShd_mnt_stat_c != LDSHD_MNT_UGLY) &&
						(main_SystemV_stat_c != V_STAT_UGLY) &&
						(main_BatteryV_stat_c != V_STAT_UGLY) )
					{
						/* Set CSWM status to BAD */
						main_CSWM_Status_c = BAD;
					}
					else
					{
						/* Set CSWM status to UGLY */
						main_CSWM_Status_c = UGLY;
					}
				}
				else
				{
					/* Set CSWM status to UGLY */
					main_CSWM_Status_c = UGLY;
				}
			}
			else
			{
//				/* 3. Check Load Shed */
//				if (main_ldShd_mnt_stat_c != LDSHD_MNT_GOOD)
//				{
//					/* Is load shedmonitor status bad? */
//					if (main_ldShd_mnt_stat_c == LDSHD_MNT_BAD)
//					{
//						/* Check for matured voltage faults before setting CSWM status to BAD */
//						if ((main_SystemV_stat_c != V_STAT_UGLY) &&
//							(main_BatteryV_stat_c != V_STAT_UGLY))
//						{
//							/* Set CSWM status to BAD */
//							main_CSWM_Status_c = BAD;
//						}
//						else
//						{
//							/* Set CSWM status to UGLY */
//							main_CSWM_Status_c = UGLY;
//						}
//					}
//					else
//					{
//						if (main_ldShd_mnt_stat_c == LDSHD_MNT_UGLY)
//						{
//							/* Set CSWM status to UGLY */
//							main_CSWM_Status_c = UGLY;
//						}
//						else
//						{
//							/* Not applicable. */
//						}
//					}
//				}
//				else
//				{
					/* 4. Check System Voltage */
					if (main_SystemV_stat_c != V_STAT_GOOD)
					{
						/* Check matured voltage faults before setting CSWM status to BAD */
						if ((main_SystemV_stat_c != V_STAT_UGLY) &&
							(main_BatteryV_stat_c != V_STAT_UGLY))
						{
							/* Set CSWM status to BAD */
							main_CSWM_Status_c = BAD;
						}
						else
						{
							/* Set CSWM status to UGLY */
							main_CSWM_Status_c = UGLY;
						}
					}
					else
					{
						/* 5. Check Battery Voltage */
						if (main_BatteryV_stat_c != V_STAT_GOOD)
						{
							/* Check form preliminary battery voltage fault */
							if (main_BatteryV_stat_c == V_STAT_BAD)
							{
								/* Set CSWM status to BAD */
								main_CSWM_Status_c = BAD;
							}
							else
							{
								/* Set CSWM status to UGLY */
								main_CSWM_Status_c = UGLY;
							}
						}
						else
						{
							/* Set CSWM status to GOOD */
							main_CSWM_Status_c = GOOD;
						}
					} //End for Section 5
				}//End for section 4
//			} //End for Section 3
		} //End for Section 2
	} //End of Section 1
	else
	{
		/* Set CSWM status to UGLY */
		main_CSWM_Status_c = UGLY;
	}

} //Function End


/****************************************************************************************/
/*									mainF_RemSt_Manager 							    */
/****************************************************************************************/
/* This function update the CSWM main state. There is a 4 second Remote start delay     */
/* (LHD_RHD cyclic 2000). 																*/
/* There are four possible states for CSWM.                                             */
/*                                                                                      */
/* 1. Normal Functinality                                                               */
/* 2. Remote start with automatic heating activation (Amb. temp < 5C)                   */
/* 3. Remote start with automatic venting activation (Amb. temp > 26C)                  */
/* 4. Remote start with user switch activation (5C <= Amb. temp <= 26C)                 */
/*																						*/
/* Note: If AMB_TEMP = SNA = Remote start with user switch activation                   */
/* Please see main_state_e enumaration for more details.                                */
/*																						*/
/****************************************************************************************/
void near mainF_RemSt_Manager(void)
{
	/* Local variable(s) */
	static u_8Bit main_RemSt_configDelay_c = 0;            // Desicion delay for Remote Start Manager
	
	/* Check if Remote control software is enabled */
	if (/*main_RemSt_SWconfig_c == ENABLE_REMOTE_CONTROL_SW*/1)
	{
		/* Check If Remote Start event is activated */
		if ( (canio_RX_IgnRun_RemSt_c == TRUE) && (RemSt_activated_b == FALSE) )
		{
			/* Check if 480ms delay has expired (24*20ms) */
			if (main_RemSt_configDelay_c < MAIN_REMST_CONFIG_DELAY)
			{
				/* Increment the delay count */
				main_RemSt_configDelay_c++;
			}
			else
			{
				/* Modifid 10.27.2008 */
				/* Check if the RemSt_configStat for CSWM is On (TRUE) */
				/* Also check if ambient temperature average signal is not equal to SNA */
				if ( (canio_RemSt_configStat_c == TRUE) && (canio_RX_ExternalTemperature_c != EXTERNAL_TEMP_SNA) )
				{
					/* Check if External temperature is in heating range 40F*/
					if (canio_RX_ExternalTemperature_c <=  main_AmbTemp_RemStart_Heat_c)
					{
						/* State 1: Remote start with automatic heating activation */
						main_state_c = REM_STRT_HEATING;
					}
					else
					{
						/* Check if External temperature is in venting range 26.7C*/
						if (canio_RX_ExternalTemperature_c >=  main_AmbTemp_RemStart_Vent_c)
						{
							/* State 2: Remote start with automatic venting activation */
							main_state_c = REM_STRT_VENTING;
						}
						else
						{
							/* State 3: Remote start with user switch activation */
							main_state_c = REM_STRT_USER_SWTCH;
						}
					}
				}
				else {
					/* Set main state to REM_STRT_USER_SWTCH when remote start event is activated and CSWM RemSt config is off (false) */
					/* Or Ambient Temp Average is SNA. */
					main_state_c = REM_STRT_USER_SWTCH;
				}
				/* Remote start event is activated. Lock this function until RemSt timeouts. */
				RemSt_activated_b = TRUE;
			}
		}
		else
		{
			/* Check if Remote start event is finished */
			if (canio_RX_IgnRun_RemSt_c == FALSE)
			{
/* FTR00139669 -- Autostart feature for PowerNet Series.*/

				if ((canio_RemSt_configStat_c == TRUE) && (RemSt_activated_b == FALSE) )
				{
						/* Check if ambient temperature is in heating range */
						/* Changed limit from 10c to 4.4      */
						if (canio_RX_ExternalTemperature_c <=  main_AmbTemp_RemStart_Heat_c )
						{
							/* State 1: Auto start with automatic heating activation */
							main_state_c = AUTO_STRT_HEATING;
						}
						else
						{
							/* Check if ambient temperature is in venting range */
							/* Changed limit from 30c to 26.7c    */
							if (canio_RX_ExternalTemperature_c >=  main_AmbTemp_RemStart_Vent_c)
							{
								/* State 2: Auto start with automatic venting activation */
								main_state_c = AUTO_STRT_VENTING;
							}
							else
							{
								/* State 3: Auto start with user switch activation */
								main_state_c = AUTO_STRT_USER_SWTCH;
							}
						}
					/* Remote start event is activated. Lock this function until RemSt timeouts. */
					RemSt_activated_b = TRUE;
					

			     }
				else
				{
					/* MKS 58316: */
					/* If CSWM configuration is with out the steering wheel then set the autostart check complete flag*/
					/* This flag is set at the end of Steering Wheel function If Wheel is equipped */
					if((CSWM_config_c == CSWM_2HS_VARIANT_1_K) || (CSWM_config_c == CSWM_4HS_VARIANT_2_K) || (CSWM_config_c == CSWM_2HS_2VS_VARIANT_5_K) || (CSWM_config_c == CSWM_4HS_2VS_VARIANT_8_K) )
					{
						autostart_check_complete_b = TRUE;
					}

					/* MKS 52743: After remote start, Rear seats to be activated */
					/* For Power Net, After remote start is over and IGN is in RUN, we are not enabling the rear seats */
					/* We are always in main_state other than NORMAL */
					
					/* Harsha added an extra check: 09/21/2010 */
					/* change the main state to NORMAL once we complete the autostart check */
					if (/*canio_RemSt_configStat_c == FALSE*/autostart_check_complete_b == TRUE)
					{
						/* Return to normal functinality */
						main_state_c = NORMAL;
				
//						/* Inactivate the RemSt */
//						RemSt_activated_b = FALSE;
				
						/* Clear remote start delay count */
						main_RemSt_configDelay_c = 0;
					 }
					 else
					 {
									/* No: Keep the RemSt active */
					 }
				}
			}
			else
			{
				/* No: Keep the RemSt active */
			}
		}
	}
	else
	{
		/* Remote start software is disabled. Always stay in normal mode. */
	}
}


/****************************************************************************************/
/*									mainF_Finish_Mcu_Test 							    */
/****************************************************************************************/
/* Input parameter(s):                                                                  */
/* test_type: Mcu test type (Ram test or Rom test)                                      */
/*                                                                                      */
/* This function updates the checked flag of the specificied Mcu test.                  */
/* After setting the checked flag, the Mcu test code will not be executed for the rest  */
/* of the ignition cycle. 																*/
/*                                                                                      */
/* Calls from                                                                           */
/* --------------                                                                       */
/* IntFlt_F_Chck_Ram_Err :                                                              */
/*    mainF_Finish_Mcu_Test(main_mcu_test_type_e test_type)                             */
/* ROMTstF_Chck_Err :                                                                   */
/*    mainF_Finish_Mcu_Test(main_mcu_test_type_e test_type)                             */
/*                                                                                      */
/*																						*/
/****************************************************************************************/
void near mainF_Finish_Mcu_Test(main_mcu_test_type_e test_type)
{
	/* Check if test type is RAM test */
	if (test_type == MAIN_RAM_TEST) {
		/* RAM is checked */
		RAM_is_checked_b = TRUE;
	}
	else {
		/* Check if test type is ROM test */
		if (test_type == MAIN_ROM_TEST) {
			/* ROM is checked */
			ROM_is_checked_b = TRUE;
		}
		else {
			/* Not applicable */
		}
	}
}


/****************************************************************************************/
/*									mainF_Calc_PWM      							    */
/****************************************************************************************/
/* Input parameter: Duty_cycle_c, desired pwm duty cycle (an 8 bit value 0 to 100).     */
/*                                                                                      */
/* This function calculates the corresponding 16-bit value for the Autosar pwm driver,  */
/* 0 to 0x8000 (AUTOSAR_MAX_PWM), in order to place output at desired pwm duty cycle.   */
/*                                                                                      */
/* Actual Formula:                                                                      */
/* Autosar Pwm = (AUTOSAR_MAX_PWM / 100) * Duty_cycle_c                                 */
/*                                                                                      */
/*                                                                                      */
/* Calls from                                                                           */
/* --------------                                                                       */
/* stWF_Updt_Pwm :                                                                      */
/*    u_16Bit mainF_Calc_PWM(u_8Bit main_duty_cycle_c)                                  */
/* vsF_Nrmlz_PWM :                                                                      */
/*    u_16Bit mainF_Calc_PWM(u_8Bit main_duty_cycle_c)                                  */
/*                                                                                      */
/*																						*/
/****************************************************************************************/
u_16Bit near mainF_Calc_PWM(u_8Bit Duty_cycle_c)
{
	/* Local variable(s) */
	u_16Bit main_pwm_w;          // stores the calculated pwm value for Autosar PWM function
	u_8Bit main_duty_cycle_c;    // desired duty cycle

	/* Store the duty cycle */
	main_duty_cycle_c = Duty_cycle_c;

	/* Check for valid duty cycle ( 0 < duty cycle <= 100 */
	if ( (main_duty_cycle_c != MAIN_ZERO_PWM) && (main_duty_cycle_c <= MAIN_N100) ) {
		
		/* Check the duty cycle */
		if (main_duty_cycle_c != MAIN_N100) {
			
			/* OLD PWM = ROUNDUP ( (327*Duty Cycle)+((Duty Cycle*68)/100) ) */
			/* NEW FOR CSWM MY11: PWM = ( (327*Duty Cycle)+((Duty Cycle*68)/100)) */
			//main_pwm_w = ( ( (MAIN_N327*main_duty_cycle_c) + ((main_duty_cycle_c*MAIN_N68)/MAIN_N100) ) + MAIN_N1);
			main_pwm_w = ( (MAIN_N327*main_duty_cycle_c) + ((main_duty_cycle_c*MAIN_N68)/MAIN_N100) );
		}
		else {
			/* Maximum PWM */
			main_pwm_w = MAIN_MAX_PWM;
		}
	}
	else {
		/* Set pwm to zero */
		main_pwm_w = MAIN_ZERO_PWM;
	}
	return(main_pwm_w);
}
/****************************************************************************************/
/*					mainF_OFF_NonSupported_Outputs_ServiceParts      					*/
/****************************************************************************************/
/* This function works for Power Net series vehicle lines for service                   */
/* part variants. that is V7                                                            */
/* Will get executed only incase the latest learned ECU configuration is different from */
/* the stored/known configuration and reduced from Max variant to lower                 */
/* In such cases, actual logic never executes as the configuration gets reduced         */
/* For safety purposes, here we turn OFF the outputs for non supported functionality    */
/* for the current IGN cycle                                                            */
/*																						*/
/****************************************************************************************/
void near mainF_OFF_NonSupported_Outputs_ServiceParts(void)
{
	
	if(ecucfg_update_b == TRUE)
	{
		/* Turn off Rear outputs and clear their variables If Rear Seats are not supported */
		if((CSWM_config_c & CSWM_4HS_VARIANT_MASK_K) != CSWM_4HS_VARIANT_MASK_K)
		{
			hsF_RearSeat_TurnOFF();
		}
		/* Turn off vent outputs and clear their variables If vents are not supported*/
		if ((CSWM_config_c & CSWM_2VS_MASK_K) != CSWM_2VS_MASK_K)
		{
			/* Vents */
			vsF_Clear( (u_8Bit) VS_FL);
			vs_Vsense_c[VS_FL] = 0;           // Clear Vent voltage reading
			vs_Isense_c[VS_FL] = 0;           // Clear Vent current reading
			vsF_LED_Display ( (u_8Bit) VS_FL);
			
			vsF_Clear( (u_8Bit) VS_FR);
			vs_Vsense_c[VS_FR] = 0;           // Clear Vent voltage reading
			vs_Isense_c[VS_FR] = 0;           // Clear Vent current reading
			
			vsF_LED_Display ( (u_8Bit) VS_FR);
			
			vsF_Output_Cntrl();
		}
		/* Turn off Wheel output and clear their variables If HSW is not supported*/
		if ((CSWM_config_c & CSWM_HSW_MASK_K) != CSWM_HSW_MASK_K)
		{
			/* Steering Wheel */
			stWF_Clear();
			
			stW_Vsense_HS_c = 0;         // clear steering wheel voltage reading high side
			stW_Vsense_LS_c = 0;         // Clear steering wheel voltage reading low side
			stW_Isense_c = 0;            // Clear steering wheel current reading low side
			
			stWF_LED_Display();
			
			stWF_Output_Control();
		}
		
		ecucfg_update_b = FALSE;
	}
	
}

