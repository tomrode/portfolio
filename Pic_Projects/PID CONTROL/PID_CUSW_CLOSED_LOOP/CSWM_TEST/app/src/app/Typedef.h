#ifndef TYPEDEF_H_
#define TYPEDEF_H_

struct char_wert
{
	unsigned char highbyte;
	unsigned char lowbyte;
};
      
union word_char
{
	unsigned short w;         
	struct char_wert cw;      
};

struct long_wert
{
	unsigned char hhbyte;
	unsigned char hbyte;
	unsigned char lbyte;
	unsigned char llbyte;
};
      
union long_char
{
	unsigned long l;         
	struct long_wert lw;      
};

struct bits
{
	unsigned char  b0:1;
	unsigned char  b1:1;
	unsigned char  b2:1;
	unsigned char  b3:1;
	unsigned char  b4:1;
	unsigned char  b5:1;
	unsigned char  b6:1;
	unsigned char  b7:1;
};
      
struct bits16
{
	unsigned char  b0:1;         
	unsigned char  b1:1;
	unsigned char  b2:1;
	unsigned char  b3:1;
	unsigned char  b4:1;
	unsigned char  b5:1;
	unsigned char  b6:1;
	unsigned char  b7:1;

	unsigned char  b10:1;          
	unsigned char  b11:1;
	unsigned char  b12:1;
	unsigned char  b13:1;
	unsigned char  b14:1;
	unsigned char  b15:1;
	unsigned char  b16:1;
	unsigned char  b17:1;
};
      
union char_bit
{
	unsigned char cb;
	struct   bits  b;
};
      
union word_bit
{
	unsigned char cb[2];         
	unsigned short cw;      
	struct bits16  b;
};

struct bit2             
{
	struct bits16 w20;    
};

union char_bit2
{
	unsigned char c2 [2]; 
	struct bit2 w2;       
};

struct bit4
{
	struct bits16 w40;
	struct bits16 w41;
};

union char_bit4
{
	unsigned char c4 [4];
	struct bit4 w4;
};

struct bit6
{
	struct bits16 w60;
	struct bits16 w61;
	struct bits16 w62;
};

union char_bit6
{
	unsigned char c6 [6];
	struct bit6 w6;
};

struct bit8
{
	struct bits16 w80;
	struct bits16 w81;
	struct bits16 w82;
	struct bits16 w83;
};

union char_bit8
{
	unsigned char c8 [8];
	struct bit8 w8;
};

union bit_byte_l
{
	unsigned long         all;
	union char_bit        byte[4];
};

typedef unsigned int u_temp_l;
typedef int temp_l;

typedef unsigned long u_long_l;
typedef long long_l;

typedef unsigned long int u_32Bit;
typedef signed long int s_32Bit;
typedef unsigned short int u_16Bit;
typedef signed short int s_16Bit;
typedef unsigned char u_8Bit;
typedef signed char s_8Bit; 

#endif /*TYPEDEF_H_*/
