/********************************************************************************/
/*                        A U T H O R   I D E N T I T Y							*/
/********************************************************************************/
/* Initials      Name                   Company									*/
/* --------      --------------------   --------------------------------------- */ 
/* CK            Can Kulduk             Continental Automotive Systems			*/
/********************************************************************************/


/*********************************************************************************/
/*                   CHANGE HISTORY for ROM TEST   MODULE                        */
/*********************************************************************************/
/* 12.21.2006 CK - Module re-created						     			 	 */
/* 10.09.2008 CK - Module copied from CSWM MY09 project.                		 */
/* 10.09.2008 CK - Started adapting code for CSWM MY11                  		 */
/* 01.23.2009 CK - PC-Lint related updates.										 */
/*********************************************************************************/


/********************************** @Include(s) **********************************/
#include "typedef.h"
#include "main.h"
#include "ROM_Test.h"
#include "Mcu.h"
#include "Dio.h"
#include "Internal_Faults.h"
#include "mw_eem.h"
/*********************************************************************************/


/********************************* @Variables ************************************/
u_16Bit   ROMTst_loop_cntr;      // Loop counter 
u_8Bit    ROMTst_page;           // ROM test entry page (3A, 3B, etc.)
u_8Bit   *ROMTst_startAddrs_p;   // Pointer to the ROM test entry start address
u_8Bit   *ROMTst_endAddrs_p;     // Pointer to the ROM test entry end address
u_8Bit    ROMTst_status;         // ROM test status (running / finished etc).
u_8Bit    ROMTst_result;         // ROM test result (OK / NOT_OK etc).
//u_8Bit    ROMTst_checksum;       // 8-bit ROM test (checksum) value
u_16Bit    ROMTst_checksum;      // 16-bit ROM test (checksum) value 

union char_bit ROMTst_Flag1;
#define ppage_updated_b    ROMTst_Flag1.b.b0     // Indicates that PPAGE is updated
/*********************************************************************************/

	
/****************************** @Data Structures *********************************/
/* ROM Test Entry */
typedef struct 
{
	u_16Bit flag;                   // Entry flag
	u_8Bit  page;                   // Page Number 
	u_8Bit *startAddress;           // Start adress of the entry
	u_8Bit *endAddress;             // End address of the entry
}ROMTst_Entry_s;
/* ROM Test Result */
typedef struct 
{
	u_8Bit endFlagByte;             // End Flag 
	//u_8Bit checkSum;                // Checksum (8-bit)
	u_16Bit checkSum;                // Checksum (16-bit)
}ROMTst_Rslt_s;
/*********************************************************************************/


/*********************************** @Pointers ***********************************/
ROMTst_Entry_s  *ROMTst_Entry_p; // Pointer to the CheckSum Entry structure
ROMTst_Rslt_s   *ROMTst_Rslt_p;  // Pointer to the CheckSum Result structure
/*********************************************************************************/


/********************************* external data *********************************/
extern int /*_checksum(void);*/ _checksum16(void);         // Cosmic compiler 8-bit checksum function
extern const u_8Bit _ROMTstStart;  // start address for ROM test (defined in cswmMY10_link.cmd file) // Original
//extern const ROMTst_Entry_s *_ROMTstStart;  // start address for ROM test (defined in cswmMY10_link.cmd file) // Updated
/*********************************************************************************/


/****************************************************************************************/
/* Function Name: 	@far void ROMTstF_Init(void)										*/
/* Type: 			Global Function														*/
/* Returns: 		None																*/
/* Parameters: 		None																*/
/* Description:     This function initializes the ROM Test module.						*/
/*																						*/
/* Calls to: 																			*/
/* Calls from:      main (initiliazation path)											*/
/* 																						*/
/****************************************************************************************/
void ROMTstF_Init(void)
{
	ROMTst_status   = ROMTST_EXECUTION_INIT;            // ROM Test is initialized and ready to be started
	ROMTst_result   = ROMTST_RESULT_NOT_TESTED;         // The ROM Test is not executed
	/* Point the ROM test structure to the start address of the ROM test (defined in the linker file) */
	ROMTst_Entry_p = (ROMTst_Entry_s *) &_ROMTstStart; // Original
	//ROMTst_Entry_p = &_ROMTstStart;		// Updated version
	
	ROMTst_page         = ROMTst_Entry_p->page;         // locate the page
	ROMTst_startAddrs_p = ROMTst_Entry_p->startAddress; // locate the start address
	ROMTst_endAddrs_p   = ROMTst_Entry_p->endAddress;   // locate the end address  
}


/****************************************************************************************/
/* Function Name: 	@far void ROMTstF_Calc_ChckSum(void)								*/
/* Type: 			Global Function														*/
/* Returns: 		None																*/
/* Parameters: 		None																*/
/* Description:     Performs 16-bit checksum calculation for all the selected segments.	*/
/*                  Uses the same algorithm as Cosmic:									*/
/*                  "Starting with zero, the CRC byte is first rotated one bit left,    */ 
/*                  then xor'ed with the code byte. The CRC values stored in the        */
/*                  checksum descriptor are the one's complement value of he expected   */
/*                  CRC".																*/
/*																						*/
/* Calls to: 																			*/
/* Calls from:      main ()																*/
/* 																						*/
/****************************************************************************************/
void ROMTstF_Calc_ChckSum(void)
{	
	/* Check the ROM Test execution status */
	if ( (ROMTst_status == ROMTST_EXECUTION_INIT) || (ROMTst_status == ROMTST_EXECUTION_RUNNING) ) {
		/* Clear the loop counter */
		ROMTst_loop_cntr = 0;
		
		/* @Check the current entry page */
		if ( (ROMTst_page != ROMTST_COMMON_PAGE1) && 
			 (ROMTst_page != ROMTST_COMMON_PAGE2) &&
			 (!ppage_updated_b) ) {		
			/* Update the PPAGE Register with current page info */
			_PPAGE.Byte = ROMTst_page;
			/* Set the ppage updated flag (for current segment) */
			ppage_updated_b = TRUE;
		}else {	
			/* No change is necassary */
		}	// Check the current entry page
		
		/* @ROM test algorithm */
		/* Check the number of iterations and check if we are at the end of ROM test */
		while ( (ROMTst_loop_cntr < MAX_LOOP_CNT) && (ROMTst_startAddrs_p != ROMTst_endAddrs_p) ) {	
			/* Checksum algorithm - 8bit. Calculates the checksum correctly. */
			/*_asm (" sei");                    // disable interrupts
			_asm (" ldaa _ROMTst_checksum");  // load accumulator A with ROMTst_checksum
			_asm (" adda #$80");              // bitwise OR ROMTst_checksum with 0x80 (set MSB) 
			_asm (" rola");                   // shift all bits of accumulator A one place to the left
			_asm (" staa _ROMTst_checksum");   // store content of accumulator A in ROMTst_checksum			
			_asm (" cli");                    // enable interrupts
			ROMTst_checksum ^= (*ROMTst_startAddrs_p);    // XOR the checksum with the code byte */	
			/* End of Checksum algorithm - 8bit */
			
			/* Checksum algorithm - 16bit. */			
			_asm (" sei");					// disable interrupts
			_asm (" ldd _ROMTst_checksum"); // load accumulator D with ROMTst_checksum
			_asm (" addd #$8000");			// bitwise OR ROMTst_checksum with 0x8000 (set MSB)
			_asm (" rolb");					// Rotate left accumulator B
			_asm (" rola");					// Rotate left accumulator A
			_asm (" std _ROMTst_checksum"); // store content of accumulator D in ROMTst_checksum
			_asm (" cli");
			ROMTst_checksum ^= (*ROMTst_startAddrs_p);    // XOR the checksum with the code byte
			/* End of Checksum algorithm - 16bit */
			
			/* Move to the next start address */
			ROMTst_startAddrs_p++;
			ROMTst_loop_cntr++;    //increment the loop counter
		}
		
		/* @Check if we are at the end of ROM test for current segment */
		if (ROMTst_startAddrs_p == ROMTst_endAddrs_p) {
			/* Advance to the next ROM test entry */
			ROMTst_Entry_p++;
			/* Clear the address updated flag for the next entry */
			ppage_updated_b = FALSE;
			
			/* Check the flag byte (Are we done with the entire ROM test?) */
			if (ROMTst_Entry_p->flag == ROMTST_CONTINUE_FLAG) {
				/* Get the next segment data */
				/* Store the page of the next entry */
				ROMTst_page = ROMTst_Entry_p->page;
				/* Store the start address of the next entry */
				ROMTst_startAddrs_p = ROMTst_Entry_p->startAddress;
				/* Store the end address of the next entry */
				ROMTst_endAddrs_p = ROMTst_Entry_p->endAddress;
				/* ROM Test execution is running */
				ROMTst_status = ROMTST_EXECUTION_RUNNING;
			}else {
				/* End of ROMTST.                             */
				/* Take the one's compliment of the checksum. */
				ROMTst_checksum = ~ROMTst_checksum;
				/* Point the ROM test result to our current ROM entry */
				ROMTst_Rslt_p = (ROMTst_Rslt_s *) ROMTst_Entry_p;
				/* ROM Test execution finished */
				ROMTst_status = ROMTST_EXECUTION_FINISHED;

				/* Compare the checksum's */
				if (ROMTst_Rslt_p->checkSum == ROMTst_checksum) {
					/* ROM test result is O.K. */
					ROMTst_result = ROMTST_RESULT_OK;
				}else {
					/* ROM test result is NOT_OK */
					ROMTst_result = ROMTST_RESULT_NOT_OK;
				}	// Compare the checksum's
			}	// Check the flag byte 
		}else {
			/* Current Segment (Entry) CheckSum is not completed */
			
		}	// Check if we are at the end of ROM test for current segment
	}else {
		/* Check ROM test status */
		if (ROMTst_status != ROMTST_EXECUTION_FINISHED) {
			/* ROM test result is undefined */
			ROMTst_result = ROMTST_RESULT_UNDEFINED;
		}else {
			/* ROM test is finished */
			
		}	// Check ROM test status
	}	// Check the ROM Test execution status
}


/****************************************************************************************/
/* Function Name: 	@far void ROMTstF_Compare_ChckSum(void)								*/
/* Type: 			Local Function														*/
/* Returns: 		None																*/
/* Parameters: 		None																*/
/* Description:     This functions places a call to _checksum() to let the linker know	*/
/*                  that it needs to calculate the checksum.							*/
/*																						*/
/* Calls to: 		_checksum() 														*/
/* Calls from:      																	*/
/* 																						*/
/****************************************************************************************/
void ROMTstF_Compare_ChckSum(void)
{
	/* Local variable(s) */
	int ROMTest_checksum_return;        // for checksum function
	/* We must include this function call _checksum16 for the linker, so it does calculate the checksum */
	ROMTest_checksum_return = /*_checksum();*/ _checksum16();
}


/****************************************************************************************/
/* Function Name: 	@far void ROMTstF_Clr_Rom_Err(void)									*/
/* Type: 			Global Function														*/
/* Returns: 		None																*/
/* Parameters: 		None																*/
/* Description:     This functions is used to clear the ROM error that sets an internal	*/
/*                  fault.																*/
/*																						*/
/* Calls to: 		EE_BlockRead(EEBlockLocation elocation, u_8Bit *pbydest)			*/
/* IntFlt_F_Update(u_8Bit Byte_number, u_8Bit Bit_mask, Int_Flt_Op_Type_e intFlt_Op)	*/
/* Calls from:      main()																*/
/* 																						*/
/****************************************************************************************/
void ROMTstF_Clr_Rom_Err(void)
{
	/* Read the ROM error byte */
	EE_BlockRead(EE_INTERNAL_FLT_BYTE0, (u_8Bit*)&intFlt_bytes[ZERO]);
	
	/* Check if we have to clear the ROM checksum error */
	if ( ( (intFlt_bytes[ZERO] & ROM_CHKSUM_ERR_MASK) == ROM_CHKSUM_ERR_MASK) &&
		 (ROMTst_result == ROMTST_RESULT_OK) ) {
		/* Update known faults */
		IntFlt_F_Update(ZERO, CLR_ROM_CHCKSUM_ERR, INT_FLT_CLR);
		/* Clear internal fault flag */
		Rom_chksum_err_b = FALSE;
	}else {
		/* No update is necassary. */

	}	// Check if we have to clear the ROM checksum error
}


/****************************************************************************************/
/* Function Name: 	@far void ROMTstF_Chck_Err(void)									*/
/* Type: 			Global Function														*/
/* Returns: 		None																*/
/* Parameters: 		None																*/
/* Description:     This function checks the Rom test result after execution of the     */
/*                  test is completed. If Rom test result is failed, it updates the 	*/
/*                  internal faults	and disables the CAN communication for the rest of  */
/*                  the ignition cycle.													*/
/* Calls to: 		Dio_WriteChannel( ChannelId, Level)									*/
/* IntFlt_F_Update(u_8Bit Byte_number,u_8Bit Bit_mask,Int_Flt_Op_Type_e intFlt_Op)		*/
/*                  mainF_Finish_Mcu_Test(main_mcu_test_type_e test_type)				*/
/*                  IntFlt_F_ClearOutputs()												*/
/* Calls from:      main()																*/
/* 																						*/
/****************************************************************************************/
void ROMTstF_Chck_Err(void)
{
	/* Local variable(s) */
	static u_8Bit Rom_CAN_disable_cnt = 0;        // CAN communication disable counter

	/* Check if high priority fault detection is enabled */
	if (intFlt_config_c == ENABLE_HIGH_PRIORITY_INTFLT_DET) {

		/* Check if ROM test is o.k. */
		if (ROMTst_result == ROMTST_RESULT_OK) {
			/* Clear CAN disable  counter */
			Rom_CAN_disable_cnt = 0;
			/* Clear internal fault flag */
			Rom_chksum_err_b = FALSE;
			/* Finish ROM test */
			mainF_Finish_Mcu_Test(MAIN_ROM_TEST);
		}else {

			/* Check fault flag to decide if we need to clear all outputs */
			if (Rom_chksum_err_b == TRUE) {
				/* Outputs should be already cleared */
			}else {
				/* Turn off outputs and LED display */
				IntFlt_F_ClearOutputs();
				/* Set internal fault flag */
				Rom_chksum_err_b = TRUE;
			}	// Check fault flag to decide if we need to clear all outputs

			/* Read the ROM error byte */ 
			EE_BlockRead(EE_INTERNAL_FLT_BYTE0, (u_8Bit*)&intFlt_bytes[ZERO]);
			
			/* Check if we already updated the internal fault */
			if ( (intFlt_bytes[ZERO]& ROM_CHKSUM_ERR_MASK) == ROM_CHKSUM_ERR_MASK) {
				
				/* Check if CAN communication disable delay has been expired */
				/* *** Delay to wait output LEDs turn off ***                */
				if (Rom_CAN_disable_cnt < INTFLT_CAN_DISABLE_TM) {
					/* Increment the delay counter */
					Rom_CAN_disable_cnt++;
				}else {
					/* Disable CAN communication (by pulling standby pin high) */
					Dio_WriteChannel(CAN_STB, STD_HIGH);
					/* Finish ROM test */
					mainF_Finish_Mcu_Test(MAIN_ROM_TEST);
				}	// Check if CAN communication disable delay has been expired
			}else {
				/* Update known faults */
				IntFlt_F_Update(ZERO, ROM_CHKSUM_ERR_MASK, INT_FLT_SET);
			}	// Check if we already updated the internal fault 
			
		}	// Check if ROM test is o.k.
	}else {
		/* High Priority internal fault (fault that disable CAN communication) detection is disabled */
		mainF_Finish_Mcu_Test(MAIN_ROM_TEST);
	}	// Check if high priority fault detection is enabled
}

