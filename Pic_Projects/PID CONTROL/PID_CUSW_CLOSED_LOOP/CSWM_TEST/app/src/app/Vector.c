/* CAN_NWM includes */
//#include "can_inc.h"
#include "il_inc.h"
#include "v_inc.h"
/* Autosar Includes */
#include "Adc_Irq.h"
#include "Mcu_Irq.h"
#include "Gpt_Irq.h"
#include "Pwm_Irq.h"

/* External Data */
extern near void main(void);
extern interrupt near void IsrFlashCmdCompleteInterrupt(void);

/*extern interrupt near void CanTxInterrupt(void); 
extern interrupt near void CanRxInterrupt(void); 
extern interrupt near void CanWakeUpInterrupt(void); 
extern interrupt near void CanErrorInterrupt(void); */

/* Function Prototypes */
interrupt near void dummy_isr(void); 

#ifdef BOOTLOADER

	/* Redefine a section for the vector table to simplify the link process */
	//#pragma section const {vector_boot}
	#pragma section const {vector_boot} 

	/* Vector Table Structure */
	typedef struct
	{
		//unsigned char jmp_opcode;
		void (* const _vectab)();
	} VectorTable_s;

	/* Vector Table Defines */
	//#define jump_opcode                 0x06,        /* CPU12 Opcode for the jump instruction */
    #define jump_opcode                             /* No jump is needed without Boothloader */
	#define VECTOR_TABLE_SIZE            120         /* Size of the vector table */
	
#else
	
	/* Redefine a section for the vector table to simplify the link process */
	#pragma section const {vector}  
	/* Vector Table Structure */
	typedef struct
	{
		void (* const _vectab)();
	} VectorTable_s;
	/* Vector Table Defines */
	#define jump_opcode                             /* No jump is needed without Boothloader */
	#define VECTOR_TABLE_SIZE            120        /* Size of the vector table */
	
#endif
	
/* Vector Table */ 
const VectorTable_s VectorTable[VECTOR_TABLE_SIZE] = 
{	
		jump_opcode dummy_isr,           /* 0xFF10 Spurious Interrupt */
		jump_opcode dummy_isr,           /* 0xFF12 System Call Interrut (SYS) */
		jump_opcode dummy_isr,           /* 0xFF14 Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFF16 Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFF18 Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFF1A Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFF1C Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFF1E Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFF20 Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFF22 Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFF24 Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFF26 Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFF28 Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFF2A Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFF2C Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFF2E Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFF30 Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFF32 Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFF34 Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFF36 Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFF38 Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFF3A Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFF3C Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFF3E ATD0 Compare Interrupt */
		jump_opcode dummy_isr,           /* 0xFF40 Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFF42 Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFF44 Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFF46 Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFF48 Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFF4A Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFF4C Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFF4E Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFF50 Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFF52 Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFF54 Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFF56 Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFF58 Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFF5A Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFF5E Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFF5E Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFF60 Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFF62 Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFF64 Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFF66 Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFF68 Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFF6A Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFF6C Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFF6E Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFF70 Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFF72 Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFF74 Periodic interrupt timer channel 3 */
		jump_opcode dummy_isr,           /* 0xFF76 Periodic interrupt timer channel 2 */
		jump_opcode dummy_isr,           /* 0xFF78 Periodic interrupt timer channel 1 */
		jump_opcode dummy_isr,           /* 0xFF7A Periodic interrupt timer channel 0 */
		jump_opcode dummy_isr,           /* 0xFF7C Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFF7E Autonomous periodical interrupt (API) */
		jump_opcode dummy_isr,           /* 0xFF80 Low-voltage interrupt */
		jump_opcode dummy_isr,           /* 0xFF82 Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFF84 Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFF86 Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFF88 Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFF8A Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFF8C PWM emergency shutdown */
		jump_opcode dummy_isr,           /* 0xFF8E Port P */
		jump_opcode dummy_isr,           /* 0xFF90 Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFF92 Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFF94 Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFF96 Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFF98 Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFF9A Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFF9C Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFF9E Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFFA0 Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFFA2 Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFFA4 Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFFA6 Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFFA8 Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFFAA Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFFAC Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFFAE Reserved: ? */
		jump_opcode CanTxInterrupt_0,      /* 0xFFB0 CAN0 Transmit */
		jump_opcode CanRxInterrupt_0,      /* 0xFFB2 CAN0 Receive */
		jump_opcode CanErrorInterrupt_0,   /* 0xFFB4 CAN0 errors */
		jump_opcode CanWakeUpInterrupt_0,  /* 0xFFB6 CAN0 wake-up */
		jump_opcode IsrFlashCmdCompleteInterrupt,           /* 0xFFB8 FLASH */
		jump_opcode dummy_isr,           /* 0xFFBA Flash Fault Detect */
		jump_opcode dummy_isr,           /* 0xFFBC Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFFBE Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFFC0 Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFFC2 Reserved: ? */
		jump_opcode CrgScm_Isr,          /* 0xFFC4 CRG self clock mode */
		jump_opcode CrgPllLck_Isr,       /* 0xFFC6 CRG PLL lock */
		jump_opcode dummy_isr,           /* 0xFFC8 Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFFCA Reserved: ? */
		jump_opcode dummy_isr,           /* 0xFFCC Port H */
		jump_opcode dummy_isr,           /* 0xFFCE Port J */
		jump_opcode dummy_isr,           /* 0xFFD0 Reserved: for what ? */
		jump_opcode Atd_0_Isr,           /* 0xFFD2 ATD0: ATD Conversion complete */
		jump_opcode dummy_isr,           /* 0xFFD4 SCI1 */
		jump_opcode dummy_isr,           /* 0xFFD6 SCI0 */
		jump_opcode dummy_isr,           /* 0xFFD8 SPI0 */
		jump_opcode dummy_isr,           /* 0xFFDA TIM Pulse accumulator input edge */
		jump_opcode dummy_isr,           /* 0xFFDC TIM Pulse accumulator A overflow */
		jump_opcode dummy_isr,           /* 0xFFDE TIM timer overflow */
		jump_opcode Ect_7_Isr,           /* 0xFFE0 TIM CH7: PWM_STW */  
		jump_opcode Ect_6_Isr,           /* 0xFFE2 TIM CH6: PWM_HS_RR */  
		jump_opcode Ect_5_Isr,           /* 0xFFE4 TIM CH5: PWM_HS_RL */  
		jump_opcode Ect_4_Isr,           /* 0xFFE6 TIM CH4: PWM_HS_FR*/  
		jump_opcode Ect_3_Isr,           /* 0xFFE8 TIM CH3: PWM_HS_FL */  
		jump_opcode Ect_2_Isr,           /* 0xFFEA TIM CH2: Time Slice Generator */  
		jump_opcode dummy_isr,           /* 0xFFEC TIM CH1: */
		jump_opcode dummy_isr,           /* 0xFFEE TIM CH0: */
		jump_opcode dummy_isr,           /* 0xFFF0 Real Time Interrupt */
		jump_opcode dummy_isr,           /* 0xFFF2 IRQ not */
		jump_opcode dummy_isr,           /* 0xFFF4 XIRQ not */
		jump_opcode dummy_isr,           /* 0xFFF6 SWI */
		jump_opcode dummy_isr,           /* 0xFFF8 Unimplemented */
		jump_opcode main,		 		 /* 0xFFFA Cop_Reset_Isr, COP failure reset*/
		jump_opcode dummy_isr,			 /* 0xFFFC ClkMon_Reset_Isr, */
		jump_opcode main,		 		 /* 0xFFFE Reset _Startup, */ 	
};

#pragma section const {}

#pragma section const {text}

/* Dummy Isr */
interrupt near void dummy_isr(void)
{
	_asm("nop"); 
}





