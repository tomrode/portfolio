#ifndef ROM_TEST_H
#define ROM_TEST_H

/********************************************************************************/
/*                        A U T H O R   I D E N T I T Y							*/
/********************************************************************************/
/* Initials      Name                   Company									*/
/* --------      --------------------   --------------------------------------- */ 
/* CK            Can Kulduk             Continental Automotive Systems			*/
/* HY            Harsha Yekollu         Continental Automotive SYstems          */
/********************************************************************************/


/*********************************************************************************/
/*                   CHANGE HISTORY for ROM TEST MODULE                          */
/*********************************************************************************/
/* 12.21.2006 CK 		- Module created						     			 */
/* 10.09.2008 CK		- Module copied from CSWM MY09 project.                  */
/* 10.09.2008 CK        - Started adapting code for CSWM MY11                    */
/* 01.08.2009 HY        - RomTst_Result and RomTst_checksum modified to external */ 
/*********************************************************************************/


/********************************** @Include(s) **********************************/
/* N
 * O
 * N
 * E */
/*********************************************************************************/


/******************************* @Enumaration(s) ********************************/
/* ROM Test result type */
typedef enum
{
    ROMTST_RESULT_NOT_TESTED=0,    // The ROM Test is not executed (default value after reset)
    ROMTST_RESULT_OK=1,            // The ROM Test has been tested with OK result
    ROMTST_RESULT_NOT_OK=2,        // The ROM Test has been tested with NOT-OK result
    ROMTST_RESULT_UNDEFINED        // This should never happen
}RomTst_Rslt; 
/* ROM Test execution status */
typedef enum
{
    ROMTST_EXECUTION_UNINIT = 0,     // ROM Test is not initialized or not usable (default value after a POR)
    ROMTST_EXECUTION_INIT = 1,       // ROM Test is initialized and ready to be started  
    ROMTST_EXECUTION_RUNNING = 2,    // ROM Test is currently running
    ROMTST_EXECUTION_FINISHED = 3,   // ROM Test is finished   
    ROMTST_EXECUTION_UNDEFINED = 4   // ROM test status is undefined
}RomTst_ExecutionStat;
/**********************************************************************************/


/******************************* @Constants/Defines *******************************/
/* Maximum number of bytes that is checked at each call to calculate checksum function */
#define MAX_LOOP_CNT            300		// Modified for CSWM MY11
/* ROM Test flags */ // MODIFIED for CSWM_MY11
#define ROMTST_CONTINUE_FLAG    /*0x8101*/ 0x8401 //for 16-bit
#define ROMTST_END_FLAG         0
/* Page */	// MODIFIED FOR CSWM_MY11
#define ROMTST_COMMON_PAGE1     0xFD
#define ROMTST_COMMON_PAGE2     0xFF
/**********************************************************************************/


/******************************** @Global variables *******************************/
extern u_8Bit    ROMTst_status;      // ROM test status (running / finished etc).
extern u_16Bit   ROMTst_checksum;      // 16-bit ROM test (checksum) value 
extern u_8Bit    ROMTst_result;         // ROM test result (OK / NOT_OK etc).
/**********************************************************************************/


/****************************** @Function Prototypes ******************************/
void ROMTstF_Init(void);
void ROMTstF_Calc_ChckSum(void);
void ROMTstF_Compare_ChckSum(void);
void ROMTstF_Chck_Err(void);
void ROMTstF_Clr_Rom_Err(void);
/**********************************************************************************/


#endif