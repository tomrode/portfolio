/********************************************************************************/
/*                        A U T H O R   I D E N T I T Y							*/
/********************************************************************************/
/* Initials      Name                   Company									*/
/* --------      --------------------   --------------------------------------- */ 
/* CK            Can Kulduk             Continental Automotive Systems			*/
/* HK            Harsha Yekollu         Continental Automotive Systems          */
/* FU            Fransisco Uribe        Continental Automotive Systems          */
/********************************************************************************/


/*********************************************************************************/
/*                   CHANGE HISTORY for AD_FLITER MODULE                         */
/*********************************************************************************/
/* 01.17.2007 CK - Module created                                           	 */
/* 03.07.2008 HY - copied from CSWM  MY09                                        */
/* 04.03.2008 HY - Updated new Micro MUX/AD selection                            */
/* 06.26.2008 FU - ADF_Nrmlz_StW_Isense updated						 		 	 */
/* 04.07.2009 CK - Updates regarding to seat heaters and STW current calculations*/
/*               - Added ADF_Int() function. Updated ADF_Nrmlz_Isense and        */
/*                 ADF_Nrmlz_StW_Isense functions. 								 */
/*                 MKS Change Request ID: 28078									 */
/*********************************************************************************/


/*********************************************************************************/
/* Module Description															 */
/* This module performs the AtoD conversion for all AD channels.                 */
/* Filtered and unfiltered results of conversions are stored in this module.     */
/* It also performs the AD to 100mV and/or 100mA conversions for output (heaters,*/
/* vents, and steering wheel) feedbacks. 										 */
/*********************************************************************************/


/********************************** @Include(s) **********************************/
#include "TYPEDEF.h"            
#include "AD_Filter.h"         
#include "Mcu.h"
#include "Adc.h"
#include "mw_eem.h"
/*********************************************************************************/


/*********************************** @ variables *********************************/
/* Filter flags */
union char_bit AD_Filter_flg[CH_LMT];                // filter flags
#define Loaded_b        b0                           // filter is loaded with the first AD reading
#define Vld_b           b1                           // filter is valid

/* Mux Filter flags */
union char_bit AD_Filter_mux_flg[MUX_LMT][CH_LMT];   // mux filter flags
#define mux_Loaded_b   b0                            // mux filter loaded
#define mux_Vld_b      b1                            // mux filter valid

/* Filter sets */
AD_Filter_s AD_mux_Filter[MUX_LMT][CH_LMT];          // one for each mux. Each mux has 8 channels (2x8).
AD_Filter_s AD_Filter_set[CH_LMT];                   // one for each AD channel

/* AD Conversion Status */
u_8Bit AD_Filter_cnvrsn_stat_c[CH_LMT];              // AD conversion complete/in-complete

/* To store heated seat Iref and Vref values that are read from emulated EEPROM */
u_8Bit	AD_Iref_seatH_c;		// current reference for Heated seats
u_16Bit AD_Vref_seatH_w;		// voltage reference for Heated seats

/* To store steering wheel Iref and Vref values that are read from emulated EEPROM */
u_8Bit	AD_Iref_STW_c;			// current reference for steering wheel
u_16Bit	AD_Vref_STW_w;          // voltage reference for steering wheel
/*********************************************************************************/


/*********************************************************************************/
/* This function is called after each POR in main function (application          */
/* initializations).											 				 */
/*                                                                               */
/* This function reads and stores the following data from emulated EEPROM.       */
/* Iref_seatH: default 2A. Used in heaters current calculation.					 */
/* Vref_seatH: default 712mV. Used in heaters current calculation.  			 */
/* Iref_STW: default 8A. Used in steering wheel heater current calculation.		 */
/* Vref_STW: default 825mV. Used in steering wheel heater current calculation.   */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* main :                                                               		 */
/*    ADF_Init()                                                                 */
/*                                                                               */
/* Calls to                                                                      */
/* --------------                                                                */
/* EE_BlockRead(EEBlockLocation elocation, u_8Bit *pbydest)                      */
/* 																				 */
/*********************************************************************************/
void near ADF_Init(void)
{	
//	/* Copy heated seat Iref and Vref values from emulated EEPROM */
//	EE_BlockRead(EE_IREF_SEAT_HEAT, (u_8Bit*)&AD_Iref_seatH_c);	// default: 2A
//	EE_BlockRead(EE_VREF_SEAT_HEAT, (u_8Bit*)&AD_Vref_seatH_w);	// default:712mV
//	
//	/* Copy steering wheel Iref and Vref values from emulated EEPROM */
//	EE_BlockRead(EE_IREF_STW, (u_8Bit*)&AD_Iref_STW_c);	// default: 8A
//	EE_BlockRead(EE_VREF_STW, (u_8Bit*)&AD_Vref_STW_w);	// default: 825mV
	
	/* The above 4 reads commented because, there is no need for them to keep in EEP */
	/* They are just constants. Initial development was to add this for EOL */
	/*But EOL is not using these parameters */
	/* Just commenting out. If EOL decides to use these, we need to re enable all the code */
	AD_Iref_seatH_c = 2;
	AD_Vref_seatH_w = 712;
	AD_Iref_STW_c = 8;
	AD_Vref_STW_w = 825;
}


/*********************************************************************************/
/* This function does the software filtering of the AD channels which are        */
/* directly connected to S12X micro.											 */
/*                                                                               */
/* PAD07: VAD_UINT (IGN_FRONT or Battery Voltage)                                */
/* PAD04: VAD_ISENSE_HEATER_FR                                                   */
/* PAD03: VAD_ISENSE_HEATER_FL                                                   */
/* PAD02: VAD_ISENSE_HEATER_RR                                                   */
/* PAD01: VAD_ISENSE_HEATER_RL                                                   */
/* PAD00: VAD_U12T  (Heated seat thermistor circuitry supply power)              */
/*                                                                               */
/* SD Text                                                                       */
/* -------                                                                       */
/* 1st time:                                                                     */
/* -  Store unfiltered result as the filtered result                             */
/* -- Load filter average with first unfiltered reading * 8                      */
/*                                                                               */
/* After 1st reading:                                                            */
/* -   Subtract the last filtered value from average                             */
/* --  Add the new unfiltered value to average                                   */
/* --- Divide filter average by 8 and store it as the filtered result            */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* BattVF_AD_Rdg :                                                               */
/*    ADF_Filter(u_8Bit AD_Filter_ch)                                            */
/* hsLF_Isense_AD_Conversion :                                                   */
/*    ADF_Filter(u_8Bit AD_Filter_ch)                                            */
/* TempF_Monitor_ADC :                                                           */
/*    ADF_Filter(u_8Bit AD_Filter_ch)                                            */
/* 																				 */
/*********************************************************************************/
void near ADF_Filter(u_8Bit AD_Filter_ch)
{
	/* Local variable(s) */
	u_8Bit AD_ch;

	/* Store AD channel */
	AD_ch = AD_Filter_ch;

	/* Check if we have the first reading yet */
	if (AD_Filter_flg[AD_ch].b.Loaded_b) {
		/* Substract the last filtered value from average (last 7 values) */
		AD_Filter_set[AD_ch].avg_w -= AD_Filter_set[AD_ch].flt_rslt_w;
		
		/* Add the new value to our average (last eight values) */
		AD_Filter_set[AD_ch].avg_w +=AD_Filter_set[AD_ch].unflt_rslt_w;
		
		/* Filtered result = average / 8 */
		AD_Filter_set[AD_ch].flt_rslt_w = (AD_Filter_set[AD_ch].avg_w>>3);

		/* Current filter is valid */
		AD_Filter_flg[AD_ch].b.Vld_b = TRUE;
	}
	else {
		/* Filter average is loaded with first reading * 8 */
		AD_Filter_set[AD_ch].avg_w = (AD_Filter_set[AD_ch].unflt_rslt_w<<3);

		/* Also store the unfiltered result into filtered result */
		AD_Filter_set[AD_ch].flt_rslt_w = AD_Filter_set[AD_ch].unflt_rslt_w;

		/* First time - Set the filter loaded flag */
		AD_Filter_flg[AD_ch].b.Loaded_b = TRUE;
	}
}

/*********************************************************************************/
/* This function performs the software filtering for AD channels (PAD06  PAD05)  */
/* which are connected to the multiplexers. 									 */
/*                                                                               */
/* MUX1 (connected to PAD06):        MUX2 (connected to PAD05):                  */
/* Ch 0: RIGHT_FRONT_THERM           Ch 0: RIGHT_REAR_THERM                      */
/* Ch 1: LEFT_FRONT_THERM            Ch 1: LEFT_REAR_THERM                       */
/* Ch 2: VAD_HEATER_FR               Ch 2: VAD_HEATER_RR                         */
/* Ch 3: VAD_HEATER_FL               Ch 3: VAD_HEATER_RL                         */
/* Ch 4: VAD_ISENSE_WHEEL            Ch 4: VAD_VENT_L                            */
/* Ch 5: LOW_SIDE_VSENSE             Ch 5: VAD_VENT_R                            */
/* Ch 6: VAD_VSENSE_WHEEL_HIGH       Ch 6: VAD_ISENSE_VL                         */
/* Ch 7: PCB_Temperature             Ch 7: VAD_ISENSE_VR                         */
/*                                                                               */
/* Filtering algorithm works similar to ADF_Filter function.                     */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* hsLF_RearSeat_Battery_AD_Readings :                                           */
/*    ADF_Mux_Filter(u_8Bit AD_Filter_slctd_mux,u_8Bit AD_Filter_mux_ch)         */
/* hsLF_Vsense_AD_Conversion :                                                   */
/*    ADF_Mux_Filter(u_8Bit AD_Filter_slctd_mux,u_8Bit AD_Filter_mux_ch)         */
/* stWF_AD_Cnvrn :                                                               */
/*    ADF_Mux_Filter(u_8Bit AD_Filter_slctd_mux,u_8Bit AD_Filter_mux_ch)         */
/* TempF_Monitor_ADC :                                                           */
/*    ADF_Mux_Filter(u_8Bit AD_Filter_slctd_mux,u_8Bit AD_Filter_mux_ch)         */
/* TempF_PCB_ADC :                                                               */
/*    ADF_Mux_Filter(u_8Bit AD_Filter_slctd_mux,u_8Bit AD_Filter_mux_ch)         */
/* vsF_AD_Cnvrsn :                                                               */
/*    ADF_Mux_Filter(u_8Bit AD_Filter_slctd_mux,u_8Bit AD_Filter_mux_ch)         */
/* 																				 */
/*********************************************************************************/
void near ADF_Mux_Filter(u_8Bit AD_Filter_slctd_mux, u_8Bit AD_Filter_mux_ch)
{
	/* Local variable(s) */
	u_8Bit AD_mux;
	u_8Bit AD_mux_ch;

	/* Store Mux information */
	AD_mux = AD_Filter_slctd_mux;
	AD_mux_ch = AD_Filter_mux_ch;

	/* Check if we have the first reading yet */
	if (AD_Filter_mux_flg[AD_mux][AD_mux_ch].b.mux_Loaded_b) {
		/* Substract the most current filtered value from average (last 7 values) */
		AD_mux_Filter[AD_mux][AD_mux_ch].avg_w -=AD_mux_Filter[AD_mux][AD_mux_ch].flt_rslt_w;

		/* Add the new value to our average (last eight values) */
		AD_mux_Filter[AD_mux][AD_mux_ch].avg_w += AD_mux_Filter[AD_mux][AD_mux_ch].unflt_rslt_w;

		/* Filtered result = average / 8 */
		AD_mux_Filter[AD_mux][AD_mux_ch].flt_rslt_w = (AD_mux_Filter[AD_mux][AD_mux_ch].avg_w>>3);

		/* Current filter is valid */
		AD_Filter_mux_flg[AD_mux][AD_mux_ch].b.mux_Vld_b = TRUE;

	}
	else {
		/* Filter average is loaded with first reading * 8 */
		AD_mux_Filter[AD_mux][AD_mux_ch].avg_w = (AD_mux_Filter[AD_mux][AD_mux_ch].unflt_rslt_w<<3);

		/* Also store the unfiltered result into filtered result */
		AD_mux_Filter[AD_mux][AD_mux_ch].flt_rslt_w = AD_mux_Filter[AD_mux][AD_mux_ch].unflt_rslt_w;

		/* First time - Set the filter loaded flag */
		AD_Filter_mux_flg[AD_mux][AD_mux_ch].b.mux_Loaded_b = TRUE;

	}
}

/*********************************************************************************/
/* Input parameters:                                                             */
/* AD_Filter_feed     = AD channel number (00 or 05)                             */
/* AD_Filter_mux_feed = Mux channel (0 to 7)                                     */
/*                                                                               */
/* Stores the unfiltered AD readings for a chosen muxed AD channel into          */
/* Ad_mux_Filter, two-dimentional array. 										 */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* hsLF_RearSeat_Battery_AD_Readings :                                           */
/*    ADF_Str_UnFlt_Mux_Rslt(u_8Bit AD_Filter_feed,u_8Bit AD_Filter_mux_feed)    */
/* hsLF_Vsense_AD_Conversion :                                                   */
/*    ADF_Str_UnFlt_Mux_Rslt(u_8Bit AD_Filter_feed,u_8Bit AD_Filter_mux_feed)    */
/* stWF_AD_Cnvrn :                                                               */
/*    ADF_Str_UnFlt_Mux_Rslt(u_8Bit AD_Filter_feed,u_8Bit AD_Filter_mux_feed)    */
/* TempF_PCB_ADC :                                                               */
/*    ADF_Str_UnFlt_Mux_Rslt(u_8Bit AD_Filter_feed,u_8Bit AD_Filter_mux_feed)    */
/* TempF_Single_Thrmstr_ADC :                                                    */
/*    ADF_Str_UnFlt_Mux_Rslt(u_8Bit AD_Filter_feed,u_8Bit AD_Filter_mux_feed)    */
/* vsF_AD_Cnvrsn :                                                               */
/*    ADF_Str_UnFlt_Mux_Rslt(u_8Bit AD_Filter_feed,u_8Bit AD_Filter_mux_feed)    */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* Adc_SingleValueReadChannel(Adc_ChannelType channel)                           */
/*																				 */
/*********************************************************************************/
void near ADF_Str_UnFlt_Mux_Rslt(u_8Bit AD_Filter_feed, u_8Bit AD_Filter_mux_feed)
{
	/* Local variable(s) */
	u_8Bit AD_feed;
	u_8Bit AD_mux_feed;

	/* Store AD and MUX feed selections */
	AD_feed = AD_Filter_feed;
	AD_mux_feed = AD_Filter_mux_feed;

	switch (AD_feed) {
		case CH5:
		{
			/* Store unfiltered mux2 result */
			AD_mux_Filter[MUX2][AD_mux_feed].unflt_rslt_w = Adc_SingleValueReadChannel(VAD_MUX2_OUT);
			break;
		}
		case CH6:
		{
			/* Store unfiltered mux1 result */
			AD_mux_Filter[MUX1][AD_mux_feed].unflt_rslt_w = Adc_SingleValueReadChannel(VAD_MUX1_OUT);
			break;
		}
		default :
		{
			/* This is not an muxed AD channel */
		}
	}
}

/*********************************************************************************/
/* Input parameters:                                                             */
/* Ad_Filter_control: Selected AD conversion channel                             */
/*                                                                               */
/* Stores the unfiltered AtoD conversion results for AD channels that are not    */
/* connected to the muxes. 														 */
/*                                                                               */
/* AD Channel summary:                                                           */
/*                                                                               */
/* CH0: U12S                                                                     */
/* CH1: Isense Heater RL                                                         */
/* CH2: Isense Heater RR                                                         */
/* CH3: Isense Heater FL                                                         */
/* CH4: Isense Heater FR                                                         */
/* CH5: Mux 2 Output                                                             */
/* CH6: Mux 1 Output                                                             */
/* CH7: Uint                                                                     */
/* Calls from                                                                    */
/* --------------                                                                */
/* BattVF_AD_Rdg :                                                               */
/*    ADF_Str_UnFlt_Rslt(u_8Bit AD_Filter_control)                               */
/* hsLF_Isense_AD_Conversion :                                                   */
/*    ADF_Str_UnFlt_Rslt(u_8Bit AD_Filter_control)                               */
/* TempF_Monitor_ADC :                                                           */
/*    ADF_Str_UnFlt_Rslt(u_8Bit AD_Filter_control)                               */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* Adc_SingleValueReadChannel(Adc_ChannelType channel)                           */
/* 																				 */
/*********************************************************************************/
void near ADF_Str_UnFlt_Rslt(u_8Bit AD_Filter_control)
{
	/* Local variable(s) */
	u_8Bit AD_control;

	/* Store AD filter control (channel) */
	AD_control = AD_Filter_control;

	switch (AD_control) {

	    case CH0:
	    {
		    /* Store the unfiltered result for U12S */
		    AD_Filter_set[AD_control].unflt_rslt_w = Adc_SingleValueReadChannel(VAD_U12T);
		    break;
	    }
		case CH1:
		{
			/* Store the unfiltered result for Hs rear left Isense */
			AD_Filter_set[AD_control].unflt_rslt_w = Adc_SingleValueReadChannel(VAD_ISENSE_HS_RL);
			break;
		}
		case CH2:
		{
			/* Store the unfiltered result for Hs rear right Isense */
			AD_Filter_set[AD_control].unflt_rslt_w = Adc_SingleValueReadChannel(VAD_ISENSE_HS_RR);
			break;
		}
		case CH3:
		{
			/* Store the unfiltered result for Hs front left Isense */
			AD_Filter_set[AD_control].unflt_rslt_w = Adc_SingleValueReadChannel(VAD_ISENSE_HS_FL);

			break;

		}
		case CH4:
		{
			/* Store the unfiltered result for Hs front right Isense */
			AD_Filter_set[AD_control].unflt_rslt_w = Adc_SingleValueReadChannel(VAD_ISENSE_HS_FR);
			break;
		}
		case CH7:
		{
			/* Store the unfiltered result for Uint (system battery voltage) */
			AD_Filter_set[AD_control].unflt_rslt_w = Adc_SingleValueReadChannel(VAD_UINT);
			break;
		}
		default :
		{
			/* This is not a regular AD channel */
		}
	}
}

/*********************************************************************************/
/* Input parameters:                                                             */
/* AD_Filter_supply: Selected AD channel AtoD conversion is going to be performed*/
/* on.                                                                           */
/*                                                                               */
/* This function starts the AtoD conversion on the selected AD channel.          */
/* It waits until the conversion is completed. Typically, Ad_Str_UnFlt_Mux_Rslt  */
/* or Ad_Str_unFlt_Rslt function is called after the execution of this function  */
/* is finished. 																 */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* BattVF_AD_Rdg :                                                               */
/*    ADF_AtoD_Cnvrsn(u_8Bit AD_Filter_supply)                                   */
/* hsLF_Isense_AD_Conversion :                                                   */
/*    ADF_AtoD_Cnvrsn(u_8Bit AD_Filter_supply)                                   */
/* hsLF_RearSeat_Battery_AD_Readings :                                           */
/*    ADF_AtoD_Cnvrsn(u_8Bit AD_Filter_supply)                                   */
/* hsLF_Vsense_AD_Conversion :                                                   */
/*    ADF_AtoD_Cnvrsn(u_8Bit AD_Filter_supply)                                   */
/* stWF_AD_Cnvrn :                                                               */
/*    ADF_AtoD_Cnvrsn(u_8Bit AD_Filter_supply)                                   */
/* TempF_Monitor_ADC :                                                           */
/*    ADF_AtoD_Cnvrsn(u_8Bit AD_Filter_supply)                                   */
/* TempF_PCB_ADC :                                                               */
/*    ADF_AtoD_Cnvrsn(u_8Bit AD_Filter_supply)                                   */
/* TempF_Single_Thrmstr_ADC :                                                    */
/*    ADF_AtoD_Cnvrsn(u_8Bit AD_Filter_supply)                                   */
/* vsF_AD_Cnvrsn :                                                               */
/*    ADF_AtoD_Cnvrsn(u_8Bit AD_Filter_supply)                                   */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* Adc_StartGroupConversion(Adc_GroupType group)                                 */
/* Adc_GetGroupStatus(Adc_GroupType group)                                       */
/*																				 */
/*********************************************************************************/
void near ADF_AtoD_Cnvrsn(u_8Bit AD_Filter_supply)
{
	/* Local variable(s) */
	u_8Bit AD_supply;

	/* Store AD filter suply */
	AD_supply = AD_Filter_supply;

	/* Start AD conversion on selected AD channel */
	Adc_StartGroupConversion(AD_supply);

	/* Wait until conversion is complete */
	/* Instead of while we may want to use an interuto*/
	while (AD_Filter_cnvrsn_stat_c[AD_supply] != ADC_COMPLETED) {
		/* Check if the conversion is complete */
		AD_Filter_cnvrsn_stat_c[AD_supply] = Adc_GetGroupStatus(AD_supply);
	}
	/* Clear the conversion status */
	AD_Filter_cnvrsn_stat_c[AD_supply] = ADC_IDLE;
}

/*********************************************************************************/
/* Input parameter:                                   							 */
/* AD_Vsense_value: 10 bit Vsense AD value            							 */
/*                                                    							 */
/* Return parameter:                                  							 */
/* AD_nrmlz_Vsense: 8 bit Vsense value in 100mV units 							 */
/*                                                   							 */
/* Hardware conversion formula:                       							 */
/* Vpin[V] = (AD*3.35*5)/1023                         							 */
/*                                                    							 */
/* Software estimation:                               							 */
/* Vsense[100mV] = ( ( (AD*16)+(Ad*0.4) ) / 100)      							 */
/*                                                    							 */
/* Calls from                                         							 */
/* --------------                                     							 */
/* hsLF_Vsense_AD_Conversion :                        							 */
/*    u_8Bit ADF_Nrmlz_Vsense(u_16Bit AD_Vsense_value) 							 */
/* stWF_Vsense_AD_Rdg :                               							 */
/*    u_8Bit ADF_Nrmlz_Vsense(u_16Bit AD_Vsense_value)							 */
/*                                                    							 */
/* 																				 */
/*********************************************************************************/
u_8Bit near ADF_Nrmlz_Vsense(u_16Bit AD_Vsense_value)
{
	u_8Bit  AD_nrmlzd_Vsense;                  // local variable for converted Vsense (in 100mV)
	u_16Bit AD_Vsense = AD_Vsense_value;       // filtered Vsense AD value

	/* @approximation (Heater Vsense AD to 100mV conversion): */
	/* Heater Vsense in 100mV = ( ( (ADvalue*16)+(ADvalue*0.4) )/100 ) */
	AD_nrmlzd_Vsense = (u_8Bit) ( ( (AD_Vsense<<4) + ((AD_Vsense*AD_N4)/AD_N10) ) / AD_N100);

	/* Return Heater Vsense in 100mV */
	return(AD_nrmlzd_Vsense);
}

/*********************************************************************************/
/* Input parameter:                                    							 */
/* AD_Isense_value: 10 bit Isense AD value            							 */
/*                                                    							 */
/* Return parameter:                                  							 */
/* AD_nrmlz_Isense: 8 bit Isense value in 100mA units 							 */
/*                                                    							 */
/* Hardware conversion formula:                       							 */
/* Ipin[A] = (AD*2760*5)/(1000*1023) - OLD                  					 */
/* Ipin[A] = (AD*((AD_Iref_seatH_c*1000)/AD_Vref_seatH_w)*5)/1023 - NEW 		 */
/*                                                    							 */
/* Software estimation:                               							 */
/* Isense[100mA] = ( (AD*14) / 100) - OLD                   					 */
/* Isense[100mA] = ((AD*interimResult)/interimResult2) - NEW     				 */
/* where interimRresult: ROUNDUP ((Iref_seatH*49)/10)                            */
/*       interimResult2: ROUNDUP (Vref_seatH/10)                                 */
/*                                                    							 */
/* Calls from                                         							 */
/* --------------                                     							 */
/* hsLF_Isense_AD_Readings :                          							 */
/*    u_8Bit ADF_Nrmlz_Isense(u_16Bit AD_Isense_value)							 */
/*																				 */
/*********************************************************************************/
u_8Bit near ADF_Nrmlz_Isense(u_16Bit AD_Isense_value)
{
	u_16Bit AD_Isense = AD_Isense_value;         // Filtered Isense AD value
	u_8Bit  AD_nrmlzd_Isense = 0;                // local variable for converted Isense (in 100mA)
	u_8Bit AD_nrmlzd_Isense_interim = 0;		 // interimRresult: ROUNDUP ((Iref_seatH*49)/10)
	u_8Bit AD_nrmlzd_Isense_interim2 = 0;		 // interimResult2: ROUNDUP (Vref_seatH/10)

	/* @approximation (Isense AD to Isense 100mA conversion): */
	/* Isense in 100mA = ( (AD*14)/100) - OLD */
	//AD_nrmlzd_Isense = (u_8Bit) ( (AD_Isense*AD_N14) / AD_N100);	// OLD
	
	/* Isense[100mA] = ((AD*interimResult)/interimResult2) - NEW */
	
	/* MODIFIED on 04.07.2009 by CK */
	/* Check against Vref minimum */
	if(AD_Vref_seatH_w >= AD_N10){
		AD_nrmlzd_Isense_interim = (u_8Bit) (((AD_Iref_seatH_c*AD_N49)/AD_N10)+AD_N1);
		AD_nrmlzd_Isense_interim2 = (u_8Bit) ((AD_Vref_seatH_w/AD_N10)+AD_N1);
		AD_nrmlzd_Isense = (u_8Bit) ((AD_Isense*AD_nrmlzd_Isense_interim)/AD_nrmlzd_Isense_interim2);
	}else{
		AD_nrmlzd_Isense = 0;
	}	
	
	/* Return the Isense in 100mA */
	return(AD_nrmlzd_Isense);
}

/*********************************************************************************/
/* Input parameter:                                              				 */
/* AD_vs_Vsense_value: 10-bit AD value that needs to be converted				 */
/*                                                               				 */
/* Return paramater:                                             				 */
/* AD_nrmlzd_vs_Vsense: 8-bit Vsense value in 100mV units        				 */
/*                                                               				 */
/* Hardware formula:                                              				 */
/* Vpin[V] = (AD*3.4*5) / 1023                                   				 */
/*                                                               				 */
/* Software estimation:                                          				 */
/* Vpin[100mV] = ( ( (AD*42)+(AD/2) ) / 256)                     				 */
/*                                                               				 */
/* Calls from                                                    				 */
/* --------------                                                				 */
/* vsF_Vsense_AD_Rdg :                                           				 */
/*    u_8Bit ADF_Nrmlz_Vs_Vsense(u_16Bit AD_vs_Vsense_value)     				 */
/*																				 */
/*********************************************************************************/
u_8Bit near ADF_Nrmlz_Vs_Vsense(u_16Bit AD_vs_Vsense_value)
{
	u_8Bit  AD_nrmlzd_vs_Vsense;                        // local variable for converted Vsense (in 100mV)
	u_16Bit AD_vs_Vsense = (AD_vs_Vsense_value+AD_N3);  // Filtered Vsense AD value

	/* @approximation (Vsense AD to Vsense 100mV conversion): */
	/* Result in 100mV = ( ( (ADvalue*42)+(Advalue/2) )/256 ) */
	AD_nrmlzd_vs_Vsense = (u_8Bit) ( ( (AD_vs_Vsense*AD_N42)+(AD_vs_Vsense>>AD_N1) ) >> AD_N8);

	/* Return Vsense in 100mV */
	return(AD_nrmlzd_vs_Vsense);
}

/*********************************************************************************/
/* Input parameter:                                              				 */
/* AD_vs_Isense_value: 10-bit AD value that needs to be converted				 */
/*                                                               				 */
/* Return paramater:                                             				 */
/* AD_nrmlzd_vs_Isense: 8-bit Isense value in 100mA units        				 */
/*                                                               				 */
/* Hardware formula:                                             				 */
/* Ipin[A] = (AD*2.61*5)/1023                                    				 */
/*                                                               				 */
/* Software estimation:                                          				 */
/* Ipin[100mA] = ( ( (AD*13) - (AD/4) ) / 100)                   				 */
/*                                                               				 */
/* Calls from                                                    				 */
/* --------------                                                				 */
/* vsF_Isense_AD_Rdg :                                           				 */
/*    u_8Bit ADF_Nrmlz_Vs_Isense(u_16Bit AD_vs_Isense_value)     				 */
/*																				 */
/*********************************************************************************/
u_8Bit near ADF_Nrmlz_Vs_Isense(u_16Bit AD_vs_Isense_value)
{
	/* Local variable(s) */
	u_16Bit AD_vs_Isense = (AD_vs_Isense_value+AD_N3); // Isense AD value
	u_8Bit AD_vs_nrmlzd_Isense = 0;                    // local variable for normalized Isense (in 100mA)

	/* @approximation (Isense AD to Isense 100mA conversion): */
	/* Isense in 100mA = ( ( (AD*13) - (AD/4) ) / 100) */
	AD_vs_nrmlzd_Isense = (u_8Bit) ( ( (AD_vs_Isense*AD_N13)-(AD_vs_Isense>>AD_N2) ) / AD_N100 );

	/* Return the Vent Isense in 100mA */
	return(AD_vs_nrmlzd_Isense);
}

/*********************************************************************************/
/* Input parameter:                                               				 */
/* AD_Stw_Isense_value: 10-bit AD value that needs to be converted				 */
/*                                                                				 */
/* Return paramater:                                              				 */
/* AD_Stw_nrmlzd_Isense: 8-bit Isense value in 100mA units        				 */
/*                                                                				 */
/* Hardware conversion formula:									  				 */
/* Ipin[A] = (AD*9700*5)/(1000*1023) -OLD  					  				     */
/* Ipin[A] = (AD*((AD_Iref_STW_c*1000)/AD_Vref_STW_w)*5)/1023 - NEW		  		 */
/*                                                                				 */
/* Software estimation:                                           				 */
/* Ipin[100mA] = ( (AD*48) / 100) - OLD                                			 */
/* Ipin[100A] = ((AD*interimResult)/interimResult2) - NEW		  	 			 */
/* where interimResult: ROUNDDOWN (Iref_STW*49)/10                               */
/*       interimResult2: ROUNDDOWN (Vref_STW/10)                                 */
/*                                                                				 */
/* Calls from                                                     				 */
/* --------------                                                 				 */
/* stWF_Isense_AD_Rdg :                                          		         */
/*    u_8Bit ADF_Nrmlz_StW_Isense(u_16Bit AD_Stw_Isense_value)   				 */
/*																				 */
/*********************************************************************************/
u_8Bit near ADF_Nrmlz_StW_Isense(u_16Bit AD_Stw_Isense_value)
{
	/* Local variable(s) */
	u_16Bit AD_Stw_Isense = AD_Stw_Isense_value;  // Filtered Isense AD value
	u_8Bit  AD_Stw_nrmlzd_Isense = 0;             // local variable for converted Isense (in 100mA)
	u_8Bit AD_Stw_nrmlzd_Isense_interim = 0;	  // interimResult: ROUNDDOWN (Iref_STW*49)/10
	u_8Bit AD_Stw_nrmlzd_Isense_interim2 = 0;	  // interimResult2: ROUNDDOWN (Vref_STW/10)
	

	/* @approximation (StW Isense AD to 100mA conversion): */
	/* Result in 100mA = ( (AD*48) / 100) - OLD */
	//AD_Stw_nrmlzd_Isense = (u_8Bit) ( (AD_Stw_Isense*AD_N48) / AD_N100); // OLD
	
	/* Ipin[100A] = ((AD*interimResult)/interimResult2) - NEW */
	
	/* MODIFIED on 04.07.2009 by CK. New Formula. */
	/* Check against minimum valid Vref */
	if(AD_Vref_STW_w >= AD_N10){
		AD_Stw_nrmlzd_Isense_interim = (u_8Bit) ((AD_Iref_STW_c*AD_N49)/AD_N10);
		AD_Stw_nrmlzd_Isense_interim2 = (u_8Bit) (AD_Vref_STW_w/AD_N10);
		AD_Stw_nrmlzd_Isense = (u_8Bit) ((AD_Stw_Isense*AD_Stw_nrmlzd_Isense_interim)/AD_Stw_nrmlzd_Isense_interim2);
	}else{
		AD_Stw_nrmlzd_Isense = 0;
	}	

	/* Return the Isense in 100mA */
	return(AD_Stw_nrmlzd_Isense);
}

/*********************************************************************************/
/* Input:                                                                        */
/* Vsense_AD: 10-bit filtered Vsense AD result for the selected heater           */
/* Isense_AD: 10-bit filtered Isense AD result for the selected heater           */
/*                                                                               */
/* This function takes the 10-bit Vsense and Isense values for selected heater   */
/* and calculates the corresponding impedence equivalent which is used in the    */
/* partial open fault detection for the heated seat outpus. 					 */
/*                                                                               */
/* The formula is:                                                               */
/*                                                                               */
/* Z = ( (Vsense*61) / (Isense*5) )  => (Interim result)                         */
/*                                                                               */
/* Note: If Isense value is zero or impedence interim result is greater than 255 */
/* (maximum 8-bit result) the return value is set to 255. Otherwise, the         */
/* impedence result is set equal to interim result.                				 */
/*																				 */
/*********************************************************************************/
u_8Bit near ADF_Hs_PrtlOpen_Z_Cnvrsn(u_16Bit Vsense_AD, u_16Bit Isense_AD)
{
	/* Local variable(s) */
	u_16Bit AD_hs_Vsense_w = Vsense_AD;                 // stores 10-bit Heated seat Vsense reading
	u_16Bit AD_hs_Isense_w = Isense_AD;                 // stores 10-bit Heated seat Isense reading
	u_16Bit AD_hs_interim_rslt_w = 0;                   // 16-bit Interim Impedence result
	u_8Bit  AD_hs_Z_rslt_c = 0;                         // 8-bit Heated seat impedence result

	/* Check for valid Isense before performing impedence calculation (to unsure the Isense is > 0) */
	if (AD_hs_Isense_w > AD_INVL_ISENSE) {
		/* Perform impedence calculation      */
		/* Z = ( (Vsense * 61) / (Isense*5) ) */
		AD_hs_interim_rslt_w = ( (AD_hs_Vsense_w*AD_N61) / (AD_hs_Isense_w*AD_N5) );
		
		/* Check if interim result is 8-bit (<= 255) */
		if (AD_hs_interim_rslt_w <= AD_MAX_Z_RSLT) {
			/* Set interim result as the final impedence result */
			AD_hs_Z_rslt_c = (u_8Bit) AD_hs_interim_rslt_w; 
		}
		else {
			/* Set final result to maximum impedence limit (255) */
			AD_hs_Z_rslt_c = (u_8Bit) AD_MAX_Z_RSLT;
		}
	}
	else {
		/* Isense is zero. Set Impedence to its max limit (255) */
		AD_hs_Z_rslt_c = (u_8Bit) AD_MAX_Z_RSLT;
	}
	/* Return the 8-bit heated seat impedence result */
	return(AD_hs_Z_rslt_c);
}

