#ifndef DIAGSEC_H_
#define DIAGSEC_H_

/*****************************************************************************************************
*  (c) Copyright 2011 Continental Automotive Systems, all rights reserved
*
*  All material contained herein is CONTINENTAL CONFIDENTIAL and PROPRIETARY.
*  Any reproduction of this material without written consent from 
*  Continental AG is strictly forbidden.                                  
*
*  Filename:     diagSec.h
*
*  Created_by:   Harsha Yekollu
*
*  Date_created: 01/19/2011
*
*  Description:  Main Header file for UDS Diagnostics - Security Access Functionality. 
*
*  Revision_history:
*
*  MKS ID       Author         Date      Description of Change(s)
*  --------  -------------  --------  --------------------------------------
*  xxxxxxx   H. Yekollu     01/19/11  Initial creation for FIAT CUSW MY12 Program
*  xxxxxxx   H. Yekollu     01/19/11  
*  75322	 H. Yekollu	    10/06/11   Cleanup the code.
* 
* 
* 
******************************************************************************************************/
#undef EXTERN 

#ifdef DIAGSEC_C
#define EXTERN 
#else
#define EXTERN extern
#endif

/*****************************************************************************************************
*     								LOCAL #DEFINES
******************************************************************************************************/
/* Chrysler Security Algorithm: Mode 5 Constants for FIAT */
#define SEC_MOD1_CONST1    0x6C8FE84F 	/* PLEASE DO NOT MODIFY THIS CONSTANT VALUE WIHTOUT CHRYSLER APPROVAL */
#define SEC_MOD1_CONST2    0x9C30D3BE   /* PLEASE DO NOT MODIFY THIS CONSTANT VALUE WIHTOUT CHRYSLER APPROVAL */

//#define FAILED_ACCESS_ATTEMPT_SET            1 
//#define DELAY_TIMER_1SEC                     100  //called in 10ms time slice.
#define DELAY_TIMER_10SEC                    1000 //called in 10ms time slice

/* Define for SECURITY ACCESS */
#define ECU_POWERUP_STATE                    0
#define ECU_LOCKED_SEC_DELAY_TIMER_INACTIVE  1
#define ECU_LOCKED_SEC_DELAY_TIMER_ACTIVE    2
#define ECU_LOCKED_SEED_GENERATED            3
//#define ECU_LOCKED_KEY_GENERATION_TIMEOUT    4
#define ECU_UNLOCKED                         5 
/*****************************************************************************************************
*     								LOCAL VARIABLES
******************************************************************************************************/
/* Bit and Byte variables */
EXTERN union char_bit diag_flags_c;
#define sec_acc_unlock_b        diag_flags_c.b.b0
#define diag_writedata_b        diag_flags_c.b.b1
//#define gen_key_timeout_b       diag_flags_c.b.b2
#define delay_timer_active_b    diag_flags_c.b.b3
#define seed_generated_b        diag_flags_c.b.b4
//#define soft_reset_b            diag_flags_c.b.b5
//#define gen_key_timer_b         diag_flags_c.b.b6

/* Holds the actual Security access state */
EXTERN unsigned char diag_sec_access1_state_c;  //Holds Security Level 1 status
EXTERN unsigned char diag_sec_access5_state_c;  //Holds Security Level 5 status

static union long_char diag_seed_l,diag_seed_generated_l;
static union long_char tool_key_l, hsm_key_l;

//static unsigned char diag_1sec_timer_c;
static int diag_10sec_timer_w;

/* Chrysler Security Algorithm: Mode 5 variables for CUSW */	
static unsigned long secMod1_var1_l;
static unsigned long secMod1_var2_l;	

/* Holds the False Access Attempt flag information */
/* 0-Security access succesfully made              */
/* 1-Security access failed                        */
static unsigned char diag_sec_FAA_c;
/*****************************************************************************************************
*     								FUNCTIONS
******************************************************************************************************/
extern @far void diagSecF_Main_Init (void);

extern @far void diagSecF_DefSession_Init (void);

extern @far void diagSecF_ExtSession_Init (void);

extern @far void diagSecF_CyclicTask (void);



#endif /*DIAGSEC_H_*/
