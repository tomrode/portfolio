#define DIAGRTNCTRL_C

/*****************************************************************************************************
*  (c) Copyright 2011 Continental Automotive Systems, all rights reserved
*
*  All material contained herein is CONTINENTAL CONFIDENTIAL and PROPRIETARY.
*  Any reproduction of this material without written consent from 
*  Continental AG is strictly forbidden.                                  
*
*  Filename:     DiagRTNCTRL.c
*
*  Created_by:   Harsha Yekollu
*
*  Date_created: 01/21/2011
*
*  Description:  Main file for UDS Diagnostics -- Routine Control LID Services. 
*
*  Revision_history:
*
*  MKS ID       Author         Date      Description of Change(s)
*  --------  -------------  --------  --------------------------------------
*  xxxxxxx   H. Yekollu     01/21/11  Initial creation for FIAT CUSW MY12 Program
*  74548     H. Yekollu     06/07/11  Update new DDT60-00-005 services.
*  75322	 H. Yekollu	    10/06/11   Cleanup the code.
* 
* 
* 
******************************************************************************************************/
/*****************************************************************************************************
* 									DIAGIOCTRL
* This module handles the UDS Routine Control Services - Service 0x31.
* 
*****************************************************************************************************/

/*****************************************************************************************************
*     								Include Files
******************************************************************************************************/
/* CAN_NWM Related Include files */
#include "il_inc.h"
#include "v_inc.h"
#include "desc.h"
#include "appdesc.h"

#include "TYPEDEF.h"
#include "main.h"
#include "DiagMain.h"
#include "DiagSec.h"
#include "DiagRead.h"
#include "DiagWrite.h"
#include "DiagIOCtrl.h"
#include "DiagRtnCtrl.h"
#include "FaultMemoryLib.h"
#include "FaultMemory.h"
#include "canio.h"

#include "heating.h"
#include "Venting.h"
#include "StWheel_Heating.h"
#include "Temperature.h"
#include "Internal_Faults.h"
#include "AD_Filter.h"
#include "BattV.h"
#include "mw_eem.h"
#include "mc9s12xs128.h"
#include "fbl_def.h"

/* Autosar include Files */
#include "Mcu.h"
#include "Port.h"
#include "Dio.h"
#include "Gpt.h"
#include "Pwm.h"
#include "Adc.h"
#include "Wdg.h"

/*****************************************************************************************************
*     								LOCAL ENUMS
******************************************************************************************************/

/*****************************************************************************************************
*     								LOCAL #DEFINES
******************************************************************************************************/
/* Defines for Routine control SID */
//#define START_ROUTINE               0x01
//#define STOP_ROUTINE                0x02
//#define REQUEST_ROUTINE_RESULTS     0x03
//
//#define ERASE_MIRROR_MEMORY_DTCS    0xFF02
//#define CHECK_PGM_PRE_CONDITIONS    0xFF03
//#define FAIL_SAFE_CONDITIONS        0xFF05
//
//#define SET_FAULT_STATUS            0x0201
//#define REQUEST_ECHO_ROUTINE        0x0202 
//#define CHECK_OUTPUTS				0x0203  // DCX Routine Control service ID for testing the outputs

//#define CONTROL_NETWORKING_MON_MODE 0x0215

//#define D_80_KILOMETER_K   5

/* DCX Routine Control (to check outputs) defines */
#define DCX_ROUTINE_HS_2SEC		100		// 100*20ms) First, 2 seconds of the test (Heaters_steering wheel)
#define DCX_ROUTINE_HS_TIME		250		// (250*20ms) First, 5 seconds of the test (Heaters_steering wheel)
#define DCX_ROUTINE_VS_2SEC     350		// (350*20ms) The end of first 2 seconds of Vent test (7 seconds)
#define DCX_ROUTINE_TOTAL_TIME	950		// (850*20ms) The rest of the test, 12 more seconds (totaling 19 seconds)

/* Routine Control Check Programming Preconditions - List of Programming Conditions */
/* Service: $31 $01 $FF $03 */
//#define FRST_RES_BYTE		0x03		// First response of for Routine Control Check Programming Preconditions

/* Note: The rest of the programming conditions are not checked */ 
//#define CSWM_OUTPUT_ACTV		0x55	// At least one of the CSWM outputs is active
//#define CSWM_OUTPUT_NOT_ACTV 	0xAA 	// No CSWM output is active


/*****************************************************************************************************
*     								LOCAL VARIABLES
******************************************************************************************************/
/* DCX Routine Control time conters */
u_16Bit diag_DCX_Test_cntr_w;		// Counter to monitor running test time
static enum DIAG_RESPONSE Response;
static u_8Bit DataLength/*,temp_failure_cnt_c,overtemp_stat,temp_CswmOutput_stat_c*/;
//static u_16Bit SubFunctionRequest,TempOdo,RoutineIdentifier;
//static u_temp_l DataLen;
/*****************************************************************************************************
*     								LOCAL FUNCTION CALL
******************************************************************************************************/
void diagRtnCtrlLF_Clr_All_IO_Control (void);
extern @far void diagF_DiagResponse(enum DIAG_RESPONSE Response, u_temp_l Length, DescMsgContext* pMsgContext);
/*********************************************************************************/
/*                     diagF_DCX_OutputTest                  	 				 */
/*********************************************************************************/
/* Function Name: @far void diagF_DCX_OutputTest(void)			      	 		 */
/* Type: Gloabal Function													     */
/* Returns: None (It updates the value of global variable diag_op_rc_status_c).  */
/* Parameters: None																 */
/* Description: This function runs the DCX Output Test. 						 */
/*              1st: Seat heaters and steering wheel are turned on.			 	 */
/*              They stay on for 5 seconds to insure to detect output faults.    */
/*              2nd: Vents are turned on (if the vehicle supports vents).     	 */
/*              Vents stay on for 14 seconds to insure to detect output faults.  */
/*              (if there are no matured vent faults already).                   */
/*              Finally, diag_op_rc_status_c variable is updated. 				 */
/* 																				 */
/*              Initial value of diag_op_rc_status_c: TEST_NOT_STARTED           */
/*              While the test is running: diag_op_rc_status_c = TEST_RUNNING.   */
/*              If there are no output faults at the end of the test then, 		 */
/* 				diag_op_rc_status_c = TEST_PASSED.								 */
/*              Otherwise (at least one output fault is present).                */
/*              diag_op_rc_status_c = TEST_FAILED_DTC_PRESENT 					 */
/*              If the routine is stopped by the diagnostic tool then,           */
/*				diag_op_rc_status_c = TEST_STOPPED.								 */
/*              If any of the pre conditions fails during the test, then         */
/* 				diag_op_rc_status_c = TEST_PRE_CONDITIONS_FAILURE				 */
/*                                                                               */
/* 				For more information please see the Diagnose header file,        */
/*              enum DIAG_DCX_ROUTINE_CONTROL_STATUS.							 */
/*																			     */
/* Calls to: 																	 */
/*    vsF_Get_Fault_Status(vs_choice) 											 */
/* Calls from:																	 */	
/*    main() 20ms timeslice												         */
/*																				 */ 
/*********************************************************************************/
void diagF_DCX_OutputTest(void)
{
	/* Local variable(s) */
	u_8Bit DCX_test_clear_flag = 0;
	u_8Bit temp_Vs_FL_fault_Stat = VS_NO_FLT_MASK;
	u_8Bit temp_Vs_FR_fault_Stat = VS_NO_FLT_MASK;
	u_8Bit temp_RL_seat_NTC_Stat = NTC_STAT_GOOD;
	u_8Bit temp_RR_seat_NTC_Stat = NTC_STAT_GOOD;
	
	/* Check if the Routine Control Output test is started */	
	if(diag_rc_all_op_lock_b == TRUE){
		
		/* While the test is running update Test pre condition status */
		diagLF_DCX_PreConditions_Chck();
		
		/* Switch statement to control DCX Routine control process */
		switch(diag_op_rc_status_c)
		{
			/* TEST_RUNNING */
			case TEST_RUNNING:
				/* Check if we are in first two seconds of the test */
				if(diag_DCX_Test_cntr_w < DCX_ROUTINE_HS_2SEC){
					/* Set the DCX routine control heaters lock bit */
					diag_rc_all_heaters_lock_b = TRUE;
					/* Increment the test counter to monitor test time */
					diag_DCX_Test_cntr_w++;
				}else{
					/* Continiously check if all the pre conditions are still met during the rest of the test */
					if(diag_op_rc_preCondition_stat_c == PRE_CONDITIONS_OK){
						/* 1st portion of the Test: Heaters+Steering Wheel (5 seconds) */
						if(diag_DCX_Test_cntr_w < DCX_ROUTINE_HS_TIME){
							/* Set the DCX routine control heaters lock bit */
							diag_rc_all_heaters_lock_b = TRUE;
							/* Increment the test counter to monitor test time */
							diag_DCX_Test_cntr_w++;
						}else{
							/* 2nd portion of the Test: Vents (Performed if we are supporting vents) */
							if( (diag_DCX_Test_cntr_w < DCX_ROUTINE_TOTAL_TIME) &&
								((CSWM_config_c & CSWM_2VS_MASK_K) == CSWM_2VS_MASK_K) ){
								/* Clear the DCX routine control heaters lock bit */
								diag_rc_all_heaters_lock_b = FALSE;
								/* Set the DCX routine control vents lock bit */
								diag_rc_all_vents_lock_b = TRUE;
								/* Check if we are performing first 2 seconds of vent test */
								if(diag_DCX_Test_cntr_w < DCX_ROUTINE_VS_2SEC){
									/* Ensures at least 2 second LED display on vents when a matured fault */
									/* already present before test start. */
									diag_DCX_Test_cntr_w++;
								}else{
									/* Get vent fault status. If we are not supporting vents this function will return VS_NO_FLT_MASK */
									temp_Vs_FL_fault_Stat = vsF_Get_Fault_Status(VS_FL);
									temp_Vs_FR_fault_Stat = vsF_Get_Fault_Status(VS_FR);
									
									/* Check for matured vent faults */
									if( (temp_Vs_FL_fault_Stat <= VS_PRL_FLT_MAX) && (temp_Vs_FR_fault_Stat <= VS_PRL_FLT_MAX) ){
										/* No matured faults. Continue the test */
										diag_DCX_Test_cntr_w++;
									}else{
										/* Already present matured vent fault. Finish the test by setting test counter to limit */
										diag_DCX_Test_cntr_w = DCX_ROUTINE_TOTAL_TIME;
									}								
								}
							}else{
								/* Test is finished. Clear test running bit before processing the result */
								diag_op_rc_status_c &= DIAG_CLR_TEST_RUNNING_MASK;
								
								/* Get vent fault status. If we are not supporting vents this function will return VS_NO_FLT_MASK */
								temp_Vs_FL_fault_Stat = vsF_Get_Fault_Status(VS_FL);
								temp_Vs_FR_fault_Stat = vsF_Get_Fault_Status(VS_FR);
								
								/* Check if we are supporting rear seats */
								if ((CSWM_config_c & CSWM_4HS_VARIANT_MASK_K) == CSWM_4HS_VARIANT_MASK_K){
									/* Rear seats are supported. Copy NTC status to temp variable */
									temp_RL_seat_NTC_Stat = temperature_HS_NTC_status[REAR_LEFT];
									temp_RR_seat_NTC_Stat = temperature_HS_NTC_status[REAR_RIGHT];
								}else{
									/* Rear seats are not supported. NTC faults will not be set. */
									/* Set temp variable status to good */
									temp_RL_seat_NTC_Stat = NTC_STAT_GOOD;
									temp_RR_seat_NTC_Stat = NTC_STAT_GOOD;
								}
								
								/* End of Test: Get the results by checking fault status of the outputs */
								if( ( (hs_status_flags_c[FRONT_LEFT].cb & DIAG_HS_MATURED_FLT_MASK) == 0x00) &&
									( (hs_status_flags_c[FRONT_RIGHT].cb & DIAG_HS_MATURED_FLT_MASK) == 0x00) &&
									( (hs_status_flags_c[REAR_LEFT].cb & DIAG_HS_MATURED_FLT_MASK) == 0x00) &&
									( (hs_status_flags_c[REAR_RIGHT].cb & DIAG_HS_MATURED_FLT_MASK) == 0x00) &&
									(stW_Flt_status_w <= STW_PRL_FLT_MAX) &&
									(temp_Vs_FL_fault_Stat <= VS_PRL_FLT_MAX) &&
									(temp_Vs_FR_fault_Stat <= VS_PRL_FLT_MAX) &&
									(temperature_HS_NTC_status[FRONT_LEFT] == NTC_STAT_GOOD) &&
									(temperature_HS_NTC_status[FRONT_RIGHT] == NTC_STAT_GOOD) && 
									(temp_RL_seat_NTC_Stat == NTC_STAT_GOOD) && 
									(temp_RR_seat_NTC_Stat == NTC_STAT_GOOD) ){
									/* There is no matured output faults present. Test is passed. */
									diag_op_rc_status_c = TEST_PASSED;
							
								}else{
									/* There should be at least one output fault. */
									diag_op_rc_status_c = TEST_FAILED_DTC_PRESENT;							
								}
								/* Test is finished. Set the flag to clear DCX Routine Control variables */
								DCX_test_clear_flag = TRUE;
							}
						}						
					}else{
						/* Test pre conditions failed while running the test */
						diag_op_rc_status_c = TEST_PRE_CONDITIONS_FAILURE;
						/* Set the flag to clear DCX Routine Control variables */
						DCX_test_clear_flag = TRUE;
					}
				}
				break;

			/* TEST_STOPPED */	
			case TEST_STOPPED:
				/* Set the flag to clear DCX Routine Control variables */
				DCX_test_clear_flag = TRUE;
				break;
			/* DEFAULT */	
			default:
				/* Set the flag to clear DCX Routine Control variables */
				DCX_test_clear_flag = TRUE;
				/* No need to modify diag_op_rc_status_c */
		}	// end of switch statement		
	}else{
		/* Test is not started. No need to update test status. */
		
		/* We do not check test pre conditions if the test is not activated. */
		/* Report back the latest known preConditions status while the test was running */
	}
	
	/* Perform Chrysler Routine Control Output Test Pre-conditions check */
		
	/* Check if we need to clear DCX Routine Control variables */
	if(DCX_test_clear_flag == TRUE){
		/* Clear the counter that is used to monitor the running test time */
		diag_DCX_Test_cntr_w = 0;		
		/* Clear DCX Routine Control flags */
		diag_rc_lock_flags_c.cb = 0x00;
	}else{
		/* Test is running. */		
	}
}

/*********************************************************************************/
/*                     diagRtnCtrlLF_Clr_All_IO_Control                  	 	 */
/*********************************************************************************/
/* Function Name: @far void diagLF_Clr_All_IO_Control(void)		      	 		 */
/* Type: Local Function													     */
/* Returns: None (It updates the value of global variable diag_op_rc_status_c).  */
/* Parameters: None																 */
/* Description: This function clears all Diagnostic IO Cnotrol requests.         */
/*																			     */
/* Calls to: 																	 */
/*    None 											 							 */
/* Calls from:																	 */	
/*    ApplDescProcessRoutineControl 									         */
/*																				 */ 
/*********************************************************************************/
void diagRtnCtrlLF_Clr_All_IO_Control(void)
{
	/* Local variables*/
	u_8Bit cntr1;	// for loop counter
	
	/* Clear Diagnostic IO control active */
	diag_io_active_b = FALSE;
	
	/* Clear Diagnostic IO Control vs and hs all lock bits */
	diag_io_lock2_flags_c.cb = 0;

	/* Heated Seats. Clear the IO Lock Bits and IO Control values to Zero */
	for (cntr1=0; cntr1<4; cntr1++)
	{
		diag_io_hs_lock_flags_c[cntr1].cb = 0;
		diag_io_hs_rq_c[cntr1] = 0;
		diag_io_hs_state_c[cntr1] = 0;
	}
	
	/* Vent Seats. Clear the IO Lock Bits and IO Control values to Zero */
	for (cntr1=0; cntr1<2; cntr1++)
	{
		diag_io_vs_lock_flags_c[cntr1].cb = 0;
		diag_io_vs_rq_c[cntr1] = 0;
		diag_io_vs_state_c[cntr1] = 0;
	}
	
	/* Steering wheel IO Lock Bits and IO Control values to Zero */
	diag_io_st_whl_rq_lock_b = FALSE;
	diag_io_st_whl_state_lock_b = FALSE;
	diag_io_st_whl_rq_c = 0; 
	diag_io_st_whl_state_c = 0;
}
/*********************************************************************************/
/*                     diagLF_DCX_PreConditions_Chck                  	 		 */
/*********************************************************************************/
/* Function Name: @far void diagLF_DCX_PreConditions_Chck(void)			         */
/* Type: Local Function													         */
/* Returns: None (It updates the value of diag_op_rc_preCondition_stat_c).       */
/* Parameters: None																 */
/* Description: This function check the pre conditions to run DCX Output Test.	 */
/*              Init. value of diag_op_rc_preCondition_stat_c: PRE_CONDITIONS_OK */
/*				Note: More then one of the pre-conditions may not be present once*/
/*              the test starts. Appropriate bits of the                         */
/*              diag_op_rc_preCondition_stat_c will be set if this is the case.  */
/* 																				 */
/* Calls to: 																	 */
/*    BattVF_Get_Status(voltage_choice) 										 */
/*    BattVF_Get_LdShd_Mnt_Stat()												 */
/* 	  TempF_Get_PCB_NTC_Stat()													 */
/* Calls from:																	 */	
/*    diagF_DCX_OutputTest												         */
/*    ApplDescProcessRoutineControl												 */
/*																				 */ 
/*********************************************************************************/
void diagLF_DCX_PreConditions_Chck(void)
{
	/* Local variables */
	//u_8Bit temp_SystemV_Stat_c, temp_BatteryV_Stat_c, temp_ldShd_Stat_c, temp_PCB_tempStat_c;
	u_8Bit temp_ldShd_Stat_c, temp_PCB_tempStat_c;
	
	/* Read System voltage, local battery voltage, load shed, and PCB temperature status */
	//temp_BatteryV_Stat_c = BattVF_Get_Status(BATTERY_V);
	//temp_SystemV_Stat_c = BattVF_Get_Status(SYSTEM_V);
	temp_ldShd_Stat_c = BattVF_Get_LdShd_Mnt_Stat();
	temp_PCB_tempStat_c = TempF_Get_PCB_NTC_Stat();
	
	/* 1st: Check ignition status (bit 0) */
	if(canio_RX_IGN_STATUS_c != IGN_RUN){
		/* Ignition is out of RUN position */
		/* Lint Warning 652: DIAG_DCX_PRE_CONDITIONS_STATUS::IGN_NOT_RUN declared previously */
		//diag_op_rc_preCondition_stat_c |= IGN_NOT_RUN;
		/* Modified on 04.24.09 by CK */
		diag_op_rc_preCondition_stat_c |= IGN_OTHER_THEN_RUN;
	}else{
		/* Clear ignition out of run position bit(0) */
		diag_op_rc_preCondition_stat_c &= DIAG_CLR_IGN_RUN;
	}
	
	/* 2nd: Check CAN bus off condition (bit 1) */
	if(canio_canbus_off_b == TRUE){
		/* CAN communication fault */
		diag_op_rc_preCondition_stat_c |= CAN_COM_FLT;
	}else{
		/* Clear CAN communication fault bit(1) */
		diag_op_rc_preCondition_stat_c &= DIAG_CLR_CAN_COM;
	}
	
	/* 3rd: Check Internal Faults (bit 2) */
	if( (intFlt_bytes[ZERO] != NO_INT_FLT) || (intFlt_bytes[ONE] != NO_INT_FLT) || (intFlt_bytes[TWO] != NO_INT_FLT) ){
		/* Internal faults is set */
		diag_op_rc_preCondition_stat_c |= INT_FLT;
	}else{
		/* Clear Highest Priorty internal fault bit(2) */
		diag_op_rc_preCondition_stat_c &= DIAG_CLR_INT_FLT;
	}
	
	/* 4th: Check System or Local Battery voltage range (bit 3) */	
	/* Commented by CK on 04.29.2009. Check current voltage instead of voltage status */
	/* Voltage Status is set to bad or ugly when load shed is active to update main_CSWM_Status_c */
//	if( (temp_BatteryV_Stat_c != V_STAT_GOOD) || (temp_SystemV_Stat_c != V_STAT_GOOD) ){
//		/* System or Local battery voltage is out of operation range */
//		diag_op_rc_preCondition_stat_c |= VOLTAGE_RANGE; 
//	}else{
//		/* Clear System or Local battery voltage out of range bit(3) */
//		diag_op_rc_preCondition_stat_c &= DIAG_CLR_VOLTAGE_RANGE;
//	}
	
	/* 4th: Check System or Local Battery voltage range (bit 3) */	
	if( (BattV_set[SYSTEM_V].crt_vltg_c > BattV_cals.Thrshld_a[SET_OV_LMT]) ||
		(BattV_set[SYSTEM_V].crt_vltg_c< BattV_cals.Thrshld_a[SET_UV_LMT]) ||
		(BattV_set[BATTERY_V].crt_vltg_c > BattV_cals.Thrshld_a[SET_OV_LMT]) || 
		(BattV_set[BATTERY_V].crt_vltg_c < BattV_cals.Thrshld_a[SET_UV_LMT]) ){
		/* System or Local battery voltage is out of operation range */
		diag_op_rc_preCondition_stat_c |= VOLTAGE_RANGE;
	}else{
		/* Clear System or Local battery voltage out of range bit(3) */
		diag_op_rc_preCondition_stat_c &= DIAG_CLR_VOLTAGE_RANGE;
	}
	
	/* 5th: Check Load Shed status (bit 4) */
	if(temp_ldShd_Stat_c != LDSHD_MNT_GOOD){
		/* Load shed criteria is present */
		diag_op_rc_preCondition_stat_c |= LOAD_SHD_ACTV;
	}else{
		/* Clear Load shed active bit(4) */
		diag_op_rc_preCondition_stat_c &= DIAG_CLR_LOAD_SHED_ACT;
	}
	
	/* 6th: Check PCB over temperature condition (bit 5) */
	if(temp_PCB_tempStat_c == PCB_STAT_OVERTEMP){
		/* PCB over temperature condition is present */
		diag_op_rc_preCondition_stat_c |= PCB_OVERTEMP_ACTV;
	}else{
		/* Clear PCB over temperature condition bit(5) */
		diag_op_rc_preCondition_stat_c &= DIAG_CLR_PCB_OVER_TEMP;
	}
	
	/* 7th: Check loss of communication with a major Network node (FCM, CCN, HVAC, CBC, or TGW) (bit 6) */
	if( (final_FCM_LOC_b == TRUE) || (final_CCN_LOC_b == TRUE) || (final_HVAC_LOC_b == TRUE) || (final_CBC_LOC_b == TRUE) || (final_TGW_LOC_b == TRUE) ){		
		/* Loss of communication with a major network node is present */
		diag_op_rc_preCondition_stat_c |= LOC_FLT;
	}else{
		/* Clear LOC with major network node bit(6) */
		diag_op_rc_preCondition_stat_c &= DIAG_CLR_LOC;
	}
	
	/* 8th: Check if Temperature is above maximum threshold for heaters and/or steering wheel (bit7) */
	if ((CSWM_config_c & CSWM_4HS_VARIANT_MASK_K) == CSWM_4HS_VARIANT_MASK_K){
		/* We are supporing rear seats. Check all NTCs */
		/* Check if any of the seats temperature > 70 C (and no NTC SHRT_TO_GND fault) */
		/* stW_temperature_c will be updated from CAN message only if we are supporting steering wheel */
		if( ( (NTC_AD_Readings_c[FRONT_LEFT] < hs_cal_prms.hs_prms[hs_vehicle_line_c].NTC_CutOFF_c[4]) && (NTC_AD_Readings_c[FRONT_LEFT] >= temperature_s.HS_NTC_Lw_Thrshld) ) ||
			( (NTC_AD_Readings_c[FRONT_RIGHT] < hs_cal_prms.hs_prms[hs_vehicle_line_c].NTC_CutOFF_c[4]) && (NTC_AD_Readings_c[FRONT_RIGHT] >= temperature_s.HS_NTC_Lw_Thrshld) ) ||
			( (NTC_AD_Readings_c[REAR_LEFT] < hs_cal_prms.hs_prms[hs_vehicle_line_c].NTC_CutOFF_c[4]) && (NTC_AD_Readings_c[REAR_LEFT] >= temperature_s.HS_NTC_Lw_Thrshld) ) ||
			( (NTC_AD_Readings_c[REAR_RIGHT] < hs_cal_prms.hs_prms[hs_vehicle_line_c].NTC_CutOFF_c[4]) && (NTC_AD_Readings_c[REAR_RIGHT] >= temperature_s.HS_NTC_Lw_Thrshld) ) ||
			(stW_temperature_c > stW_prms.cals[0].MaxTemp_Threshld) ){
			/* Set the 7th bit */
			diag_op_rc_preCondition_stat_c |= TEMP_ABOVE_MAX_THRSHLD;			
		}else{
			/* Clear Temperature is above maximum threshold for heaters and/or steering wheel bit(7) */
			diag_op_rc_preCondition_stat_c &= DIAG_CLR_TEMP_ABOVE_THRSHLD;
		}		
	}else{
		/* Not suppoting rear seats. Only chek front NTC's */
		/* Check if any of the front seats temperature > 70 C (and no NTC SHRT_TO_GND fault) */
		/* stW_temperature_c will be updated from CAN message only if we are supporting steering wheel */
		if( ( (NTC_AD_Readings_c[FRONT_LEFT] < hs_cal_prms.hs_prms[hs_vehicle_line_c].NTC_CutOFF_c[4]) && (NTC_AD_Readings_c[FRONT_LEFT] >= temperature_s.HS_NTC_Lw_Thrshld) ) ||
			( (NTC_AD_Readings_c[FRONT_RIGHT] < hs_cal_prms.hs_prms[hs_vehicle_line_c].NTC_CutOFF_c[4]) && (NTC_AD_Readings_c[FRONT_RIGHT] >= temperature_s.HS_NTC_Lw_Thrshld) ) ||
			(stW_temperature_c > stW_prms.cals[0].MaxTemp_Threshld) ){
			/* Set the 7th bit */
			diag_op_rc_preCondition_stat_c |= TEMP_ABOVE_MAX_THRSHLD;
		}else{
			/* Clear Temperature is above maximum threshold for heaters and/or steering wheel bit(7) */
			diag_op_rc_preCondition_stat_c &= DIAG_CLR_TEMP_ABOVE_THRSHLD;
		}
	}
}
/*  ********************************************************************************
 * Function name:ApplDescStart_31010201_Set_Fault_Status (Service request header:$31 $1 $2 $1 )
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescStart_31010201_Set_Fault_Status(DescMsgContext* pMsgContext)
{
   DtcStructType    TempDtcCode;
   TempDtcCode.all = 0;

	// Get the DTC code from the request buffer
	TempDtcCode.byte[1] = pMsgContext->reqData[0]; //Harsha..Re arranged
	TempDtcCode.byte[2] = pMsgContext->reqData[1];
	TempDtcCode.byte[3] = pMsgContext->reqData[2];

	if (pMsgContext->reqDataLen == 4)
	{
		switch (TempDtcCode.all) 
        {
			  case DTC_HS_FL_SHORT_TO_GND_K:
			  {
			     if (pMsgContext->reqData[3] == 0x01)
			     {
				      hs_status_flags_c[ FRONT_LEFT ].b.hs_final_short_gnd_b = TRUE;
			     }
			     else
			     {
			        if (pMsgContext->reqData[3] == 0)
			        {
				         hs_status_flags_c[ FRONT_LEFT ].b.hs_final_short_gnd_b = FALSE;
			        }
			     }
			     break;
			  }
			  
			  case DTC_HS_FL_SHORT_TO_BAT_K:
			  {
			     /* Failed? */
			     if (pMsgContext->reqData[3] == 0x01)
			     {
				      hs_status_flags_c[ FRONT_LEFT ].b.hs_final_short_bat_b = TRUE;
			     }
			     else 
                 {
			    	 /* Passed? */
			    	 if (pMsgContext->reqData[3] == 0x00)
			        {
				         hs_status_flags_c[ FRONT_LEFT ].b.hs_final_short_bat_b = FALSE;
			        }
			        else
			        {
				         //
			        }
			     }
			     break;
			  }
			  
			  case DTC_HS_FL_OPEN_FULL_K:
			  {
			     /* Failed? */
			     if (pMsgContext->reqData[3] == 0x01)
			     {
				      hs_status_flags_c[ FRONT_LEFT ].b.hs_final_open_b = TRUE;
			     }
			     else 
                 {
			         /* Passed? */
			         if (pMsgContext->reqData[3] == 0x00)
			         {
					      hs_status_flags_c[ FRONT_LEFT ].b.hs_final_open_b = FALSE;
					 }
			        else
			        {
				         //
			        }
			     }
			     break;
			  }
			  
			  case DTC_HS_FL_OPEN_PARTIAL_K:
			  {
			     /* Failed? */
			     if (pMsgContext->reqData[3] == 0x01)
			     {
				      hs_status_flags_c[ FRONT_LEFT ].b.hs_final_partial_open_b = TRUE;
			     }
			     else
			     {
				      /* Passed? */
				      if (pMsgContext->reqData[3] == 0x00)
				      {
				         hs_status_flags_c[ FRONT_LEFT ].b.hs_final_partial_open_b = FALSE;
				      }
				      else
				      {
					       //
				      }
			     }
			     break;
			  }
			  
			  case DTC_HS_FR_SHORT_TO_BAT_K:
			  {
			     /* Failed? */
			     if (pMsgContext->reqData[3] == 0x01)
			     {
				      hs_status_flags_c[ FRONT_RIGHT ].b.hs_final_short_bat_b = TRUE;
			     }
			     else
			     {
			        /* Passed? */
			        if (pMsgContext->reqData[3] == 0x00)
			        {
				         hs_status_flags_c[ FRONT_RIGHT ].b.hs_final_short_bat_b = FALSE;
			        }
			        else
			        {
				         //
			        }
			     }
			     break;
			  }
			  case DTC_HS_FR_OPEN_FULL_K:
			  {
				      /* Failed? */
				      if (pMsgContext->reqData[3] == 0x01) 
				      {
					       hs_status_flags_c[ FRONT_RIGHT ].b.hs_final_open_b = TRUE;
				      }
				      else 
				      {
					        /* Passed? */
					        if (pMsgContext->reqData[3] == 0x00) 
					        {
						         hs_status_flags_c[ FRONT_RIGHT ].b.hs_final_open_b = FALSE;
					        }
					        else 
					        {
						         //
					        }
				       }
				       break;
			  }
			  case DTC_HS_FR_OPEN_PARTIAL_K:
			  {
				   /* Failed? */
				   if (pMsgContext->reqData[3] == 0x01)
				   {
					    hs_status_flags_c[ FRONT_RIGHT ].b.hs_final_partial_open_b = TRUE;
				   }
				   else 
				   {
					      /* Passed? */
					      if (pMsgContext->reqData[3] == 0x00)
					      {
						       hs_status_flags_c[ FRONT_RIGHT ].b.hs_final_partial_open_b = FALSE;
					      }
					      else
					      {
						       //
				 	      }
				   }
				   break;
			  }
			  
			  case DTC_HS_RL_SHORT_TO_GND_K:
			  {
				   if (pMsgContext->reqData[3] == 0x01)
				   {
					    hs_status_flags_c[ REAR_LEFT ].b.hs_final_short_gnd_b = TRUE;
				   }
				   else
				   {
					      if (pMsgContext->reqData[3] == 0)
					      {
						       hs_status_flags_c[ REAR_LEFT ].b.hs_final_short_gnd_b = FALSE;
					      }
				   }
				   break;
			  }
			  case DTC_HS_RL_SHORT_TO_BAT_K:
			  {
				   /* Failed? */
				   if (pMsgContext->reqData[3] == 0x01)
				   {
					    hs_status_flags_c[ REAR_LEFT ].b.hs_final_short_bat_b = TRUE;
				   }
				   else
				   {
					      /* Passed? */
					      if (pMsgContext->reqData[3] == 0x00)
					      {
						       hs_status_flags_c[ REAR_LEFT ].b.hs_final_short_bat_b = FALSE;
					      }
					      else
					      {
						       //
					      }
				   }
				   break;
			  }
			  case DTC_HS_RL_OPEN_FULL_K:
			  {
				   /* Failed? */
				   if (pMsgContext->reqData[3] == 0x01)
				   {
					    hs_status_flags_c[ REAR_LEFT ].b.hs_final_open_b = TRUE;
				   }
				   else
				   {
					      /* Passed? */
					      if (pMsgContext->reqData[3] == 0x00)
					      {
						       hs_status_flags_c[ REAR_LEFT ].b.hs_final_open_b = FALSE;
					      }
					      else
					      {
						       //
					      }
				   }
				   break;
			  }
			  case DTC_HS_RL_OPEN_PARTIAL_K:
			  {
				     /* Failed? */
				     if (pMsgContext->reqData[3] == 0x01)
				     {
					      hs_status_flags_c[ REAR_LEFT ].b.hs_final_partial_open_b = TRUE;
				     }
				     else
				     {
					        /* Passed? */
					        if (pMsgContext->reqData[3] == 0x00)
					        {
						         hs_status_flags_c[ REAR_LEFT ].b.hs_final_partial_open_b = FALSE;
					        }
					        else
					        {
						         //
					        }
				     }
				     break;
			  }
			  case DTC_HS_RR_SHORT_TO_GND_K:
			  {
				   if (pMsgContext->reqData[3] == 0x01)
				   {
					    hs_status_flags_c[ REAR_RIGHT ].b.hs_final_short_gnd_b = TRUE;
				   }
				   else
				   {
					      if (pMsgContext->reqData[3] == 0)
					      {
						       hs_status_flags_c[ REAR_RIGHT ].b.hs_final_short_gnd_b = FALSE;
					      }
				   }
				   break;
			  }
			  case DTC_HS_RR_SHORT_TO_BAT_K:
			  {
				   /* Failed? */
				   if (pMsgContext->reqData[3] == 0x01)
				   {
					    hs_status_flags_c[ REAR_RIGHT ].b.hs_final_short_bat_b = TRUE;
				   }
				   else
				   {
					      /* Passed? */
					      if (pMsgContext->reqData[3] == 0x00)
					      {
						       hs_status_flags_c[ REAR_RIGHT ].b.hs_final_short_bat_b = FALSE;
					      }
					      else
					      {
						       //
					      }
				   }
				   break;
			  }
			  case DTC_HS_RR_OPEN_FULL_K:
			  {
				   /* Failed? */
				   if (pMsgContext->reqData[3] == 0x01)
				   {
					    hs_status_flags_c[ REAR_RIGHT ].b.hs_final_open_b = TRUE;
				   }
				   else
				   {
					      /* Passed? */
					      if (pMsgContext->reqData[3] == 0x00)
					      {
						       hs_status_flags_c[ REAR_RIGHT ].b.hs_final_open_b = FALSE;
					      }
					      else
					      {
						       //
					      }
				   }
				   break;
			  }
			  case DTC_HS_RR_OPEN_PARTIAL_K:
			  {
				   /* Failed? */
				   if (pMsgContext->reqData[3] == 0x01)
				   {
					    hs_status_flags_c[ REAR_RIGHT ].b.hs_final_partial_open_b = TRUE;
				   }
				   else
				   {
					      /* Passed? */
					      if (pMsgContext->reqData[3] == 0x00)
					      {
						       hs_status_flags_c[ REAR_RIGHT ].b.hs_final_partial_open_b = FALSE;
					      }
					      else
					      {
						       //
					      }
				   }
				   break;
			  }
			  case DTC_VS_FL_SHORT_TO_GND_K:
			  {
				   /* Set DTC */
				   if (pMsgContext->reqData[3] == 0x01)
				   {
					    //Enable Fault
					    vsF_Diag_DTC_Control(VS_SET_DTC, VS_MTRD_GND_MASK, VS_FL);
				   }
				   else
				   {
					    /* Clear DTCs */
					    if (pMsgContext->reqData[3] == 0)
					    {
						     //disable Fault
						     vsF_Diag_DTC_Control(VS_CLR_DTC, VS_MTRD_GND_MASK, VS_FL);
					    }
				   }
				   break;
			  }
			  case DTC_VS_FL_SHORT_TO_BAT_K:
			  {
				   /* Set DTC */
				   if (pMsgContext->reqData[3] == 0x01)
				   {
					    //Enable Fault
					    vsF_Diag_DTC_Control(VS_SET_DTC, VS_MTRD_BATT_MASK, VS_FL);
				   }
				   else
				   {
					    /* Clear DTCs */
					    if (pMsgContext->reqData[3] == 0x00)
					    {
						     //disable Fault
						     vsF_Diag_DTC_Control(VS_CLR_DTC, VS_MTRD_BATT_MASK, VS_FL);
					    }
					    else
					    {
						     //
					    }
				   }
				   break;
			  }
			  case DTC_VS_FL_OPEN_K:
			  {
				   /* Set DTC */
				   if (pMsgContext->reqData[3] == 0x01)
				   {
					    //Enable Fault
					    vsF_Diag_DTC_Control(VS_SET_DTC, VS_MTRD_OPEN_MASK, VS_FL);
				   }
				   else
				   {
					    /* Clear DTCs */
					    if (pMsgContext->reqData[3] == 0x00)
					    {
						     //disable Fault
						     vsF_Diag_DTC_Control(VS_CLR_DTC, VS_MTRD_OPEN_MASK, VS_FL);
					    }
					    else
					    {
						     //
					    }
				   }
				   break;
			  }
			  case DTC_VS_FR_SHORT_TO_GND_K:
			  {
				   /* Set DTC */
				   if (pMsgContext->reqData[3] == 0x01)
				   {
					    //Enable Fault
					    vsF_Diag_DTC_Control(VS_SET_DTC, VS_MTRD_GND_MASK, VS_FR);
				   }
				   else
				   {
					      /* Clear DTCs */
					      if (pMsgContext->reqData[3] == 0)
					      {
						       //disable Fault
						       vsF_Diag_DTC_Control(VS_CLR_DTC, VS_MTRD_GND_MASK, VS_FR);
					      }
				   }
				   break;
			  }
			  case DTC_VS_FR_SHORT_TO_BAT_K:
			  {
				   /* Set DTC */
				   if (pMsgContext->reqData[3] == 0x01)
				   {
					    //Enable Fault
					    vsF_Diag_DTC_Control(VS_SET_DTC, VS_MTRD_BATT_MASK, VS_FR);
				   }
				   else
				   {
					    /* Clear DTCs */
					    if (pMsgContext->reqData[3] == 0x00)
					    {
						     //disable Fault
						     vsF_Diag_DTC_Control(VS_CLR_DTC, VS_MTRD_BATT_MASK, VS_FR);
					    }
					    else
					    {
						     //
					    }
				   }
				   break;
			  }
			  case DTC_VS_FR_OPEN_K:
			  {
				   /* Set DTC */
				   if (pMsgContext->reqData[3] == 0x01)
				   {
					    //Enable Fault
					    vsF_Diag_DTC_Control(VS_SET_DTC, VS_MTRD_OPEN_MASK, VS_FR);
				   }
				   else
				   {
					    /* Clear DTCs */
					    if (pMsgContext->reqData[3] == 0x00)
					    {
						     //disable Fault
						     vsF_Diag_DTC_Control(VS_CLR_DTC, VS_MTRD_OPEN_MASK, VS_FR);
					    }
					    else
					    {
						     //
					    }
				   }
				   break;
			  }
			  case DTC_HSW_OPEN_FULL_K:
			  {
				   /* Set DTC */
				   if (pMsgContext->reqData[3] == 0x01)
				   {
					    //Enable Fault
					    stWF_Diag_DTC_Control(STW_SET_DTC, STW_MTRD_FULL_OPEN_MASK);
				   }
				   else
				   {
					    /* Clear DTCs */
					    if (pMsgContext->reqData[3] == 0x00)
					    {
						     //disable Fault
						     stWF_Diag_DTC_Control(STW_CLR_DTC, STW_MTRD_FULL_OPEN_MASK);
					    }
					    else
					    {
						     //
					    }
				   }
				   break;
			  }
//						case DTC_HSW_OPEN_PARTIAL_K:
//						{
//							/* Set DTC */
//							if (pMsgContext->reqData[6] == 0x01)
//							{
//								//Enable Fault
////								stWF_Diag_DTC_Control(STW_SET_DTC, STW_MTRD_PARTIAL_OPEN_MASK);
//							}
//							else
//							{
//								/* Clear DTCs */
//								if (pMsgContext->reqData[6] == 0x00)
//								{
//									//disable Fault
////									stWF_Diag_DTC_Control(STW_CLR_DTC, STW_MTRD_PARTIAL_OPEN_MASK);
//								}
//								else
//								{
//									//
//								}
//							}
//							break;
//						}
			  case DTC_HSW_SHORT_TO_GND_K:
			  {
				   /* Set DTC */
				   if (pMsgContext->reqData[3] == 0x01)
				   {
					    //Enable Fault
					    stWF_Diag_DTC_Control(STW_SET_DTC, STW_MTRD_GND_MASK);
				   }
				   else
			  	 {
					    /* Clear DTCs */
					    if (pMsgContext->reqData[3] == 0x00)
					    {
						     //disable Fault
						     stWF_Diag_DTC_Control(STW_CLR_DTC, STW_MTRD_GND_MASK);
					    }
					    else
					    {
						     //
					    }
				   }
				  break;
			  }
			  case DTC_HSW_SHORT_TO_BAT_K:
			  {
				   /* Set DTC */
				   if (pMsgContext->reqData[3] == 0x01)
				   {
					    //Enable Fault
					    stWF_Diag_DTC_Control(STW_SET_DTC, STW_MTRD_BATT_MASK);
				   }
				   else
				   {
					      /* Clear DTCs */
					      if (pMsgContext->reqData[3] == 0x00)
					      {
						       //disable Fault
						       stWF_Diag_DTC_Control(STW_CLR_DTC, STW_MTRD_BATT_MASK);
					      }
					      else
					      {
						       //
					      }
				   }
				   break;
			  }


			  default :
			  {
				 Response = SUBFUNCTION_NOT_SUPPORTED;
			  }
	}
		}
		else
	   {		
			Response = LENGTH_INVALID_FORMAT;
	   }
				/* 02.17.2009 CK */
				/* Clear data Lenght: Response for this service does not have any additional information */
				DataLength = 0;

				if(pMsgContext->reqData[3]	!= 0x00 && pMsgContext->reqData[3] != 0x01)
 				{
   						Response = SUBFUNCTION_NOT_SUPPORTED;
 				}
 				if( Response != SUBFUNCTION_NOT_SUPPORTED && Response != LENGTH_INVALID_FORMAT )
   			{
						pMsgContext->reqDataLen=3;				
				    Response = RESPONSE_OK;
				}				
        diagF_DiagResponse(Response, (pMsgContext->reqDataLen)+DataLength, pMsgContext);
}

/*  ********************************************************************************
 * Function name:ApplDescStart_31010202_Request_Echo_Routine (Service request header:$31 $1 $2 $2 )
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescStart_31010202_Request_Echo_Routine(DescMsgContext* pMsgContext)
{
        /* 02.17.2009 CK */
				/* Clear data Lenght: Response for this service does not have any additional information */
				//DataLength = 0;
				
				/* Modified on 04.17.2009 by CK. Response lenght is suppose to be 6 bytes. */
				/* Echo the last byte received in the request message (data byte) to make response lenght 6 bytes. */
				pMsgContext ->reqData[4] = pMsgContext ->reqData[3];
				DataLength = 1;
				
				Response = RESPONSE_OK;
				diagF_DiagResponse(Response, DataLength, pMsgContext);
}

/*  ********************************************************************************
 * Function ApplDescStart_31030203_CSWM_Ouput_Test (Service request header:$31 $1 $2 $3 )
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescStart_31010203_CSWM_Ouput_Test(DescMsgContext* pMsgContext)
{
      /* Clear all Diagnostic IO Control IO Lock Bits and IO Control values to Zero */
			diagRtnCtrlLF_Clr_All_IO_Control();				
			/* Set the lock all bit to disable all switch inputs during DCX routine control */
			diag_rc_all_op_lock_b = TRUE;
			/* Update cswm main status in "real-time" */
			mainF_CSWM_Status();	
			/* Check if steering wheel is supported */
			if((CSWM_config_c & CSWM_HSW_MASK_K) == CSWM_HSW_MASK_K)
      {
					/* Set a flag that clears the stW ontime with each request */
					stW_clr_ontime_b = TRUE;
			}
      else
      {
					/* Steering wheel is not supported */
			}
    	
    	/* Fill first byte of response with status info */
			pMsgContext->resData[0] = diag_op_rc_status_c;	
			/* For every start, stop or request result command read preconditions status */
			diagLF_DCX_PreConditions_Chck();
			/* Fill the second byte of the response. Preconditions status */
			pMsgContext->resData[1] = diag_op_rc_preCondition_stat_c; 
		  DataLength = 2;
		  /* START routine control */
    	diag_op_rc_status_c = TEST_RUNNING;	 			// Set DCX routine control status to TEST_RUNNING
    	Response = RESPONSE_OK;    						    // Set data response type to RESPONSE_OK
    	diagF_DiagResponse(Response, (pMsgContext->reqDataLen)+DataLength, pMsgContext);
}

/*  ********************************************************************************
 * Function ApplDescStop_31030203_CSWM_Ouput_Test (Service request header:$31 $2 $2 $3 )
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescStop_31010203_CSWM_Ouput_Test(DescMsgContext* pMsgContext)
{
        /* STOP Control Routine */
	      diag_op_rc_status_c = TEST_STOPPED;				// Set DCX routine control status to TEST_STOPPED	
	      /* Fill first byte of response with status info */
			  pMsgContext->resData[0] = diag_op_rc_status_c;	
				/* For every start, stop or request result command read preconditions status */
				diagLF_DCX_PreConditions_Chck();
				/* Fill the second byte of the response. Preconditions status */
				pMsgContext->resData[1] = diag_op_rc_preCondition_stat_c; 
		    DataLength = 2;									// Set Data Lenght to 2	    	
        Response = RESPONSE_OK;		    				// Set data response type to RESPONSE_OK
        diagF_DiagResponse(Response, (pMsgContext->reqDataLen)+DataLength, pMsgContext);
}

/*  ********************************************************************************
 * Function ApplDescReqResults_31030203_CSWM_Ouput_Test (Service request header:$31 $3 $2 $3 )
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescReqResults_31010203_CSWM_Ouput_Test(DescMsgContext* pMsgContext)
{
      /* Note: We will update the first byte of the response message with status info before we send it out */
			/* Fill first byte of response with status info */
			  pMsgContext->resData[0] = diag_op_rc_status_c;	
				/* For every start, stop or request result command read preconditions status */
				diagLF_DCX_PreConditions_Chck();
				/* Fill the second byte of the response. Preconditions status */
				pMsgContext->resData[1] = diag_op_rc_preCondition_stat_c; 
		    DataLength = 2;									// Set Data Lenght to 2	    	
        Response = RESPONSE_OK;		    				// Set data response type to RESPONSE_OK
        diagF_DiagResponse(Response, (pMsgContext->reqDataLen)+DataLength, pMsgContext);
}

/*  ********************************************************************************
 * Function name:ApplDescStartFlashErase (Service request header:$31 $1 $FF $0 )
 * Description:WARNING !!!! Start & Stop address size have to be resized for each project
 * Returns:  nothing
 * Parameter(s):
 *   - pMsgContext->reqData:
 *       - Points to the first service request data byte.
 *       - Access type: read
 *   - pMsgContext->resData:
 *       - Points to the first writeable byte for the service response data.
 *       - Access type: read/write
 *   - pMsgContext->reqDataLen:
 *       - Contains the count of the service request data bytes (Sid is excluded).
 *       - Access type: read
 *   - pMsgContext->resDataLen:
 *       - Must be initialized with the count of the service response data bytes (Sid is excluded).
 *       - Access type: read/write
 *   - pMsgContext->msgAddInfo.reqType:
 *       - Shows the addressing type of the request (kDescPhysReq or kDescFuncReq).
 *       - Access type: read
 *   - pMsgContext->msgAddInfo.resOnReq:
 *       - Indicates if there will be response. Allowed only to write only 0 !!!
 *       - Access type: read/write
 *   - pMsgContext->msgAddInfo.suppPosRes:
 *       - UDS only!If set no positive response will be sent on this request.
 *       - Access type: read
 * Particularitie(s) and limitation(s):
 *   - The function "DescProcessingDone" must be called from now on (within this
 * main-handler or later).
 *   - The function "DescSetNegResponse" can be called within this main-handler or later
 * but before calling "DescProcessingDone".
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescStartFlashErase(DescMsgContext* pMsgContext)
{
//  /* <<TBD>> Remove this comment once you have completely implemented this function!!! */
//  /* Dummy example how to access the request data. */
//  /* Assumming the FIRST DATA byte contains important data which has to be less than a constant value. */
//  if(pMsgContext->reqData[0] < 0xFF)
//  {
//    /* Received data is in range process further. */
//    /* Dummy example of how to write the response data. */
//    pMsgContext->resData[0] = 0xFF;
//    /* Always set the correct length of the response data. */
//    pMsgContext->resDataLen = 1;
//  }
//  else
//  {
    /* Request contains invalid data - send negative response! */
    DescSetNegResponse(kDescNrcRequestOutOfRange);
//  }
  /* User service processing finished. */
  DescProcessingDone();
}


/*  ********************************************************************************
 * Function name:ApplDescStartcheckProgrammingDependencies_FlashChecksum (Service request header:$31 $1 $FF $1 )
 * Description:WARNING !!!! Start & Stop address size have to be resized for each project
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescStartcheckProgrammingDependencies_FlashChecksum(DescMsgContext* pMsgContext)
{
  /* <<TBD>> Remove this comment once you have completely implemented this function!!! */
  /* Dummy example how to access the request data. */
  /* Assumming the FIRST DATA byte contains important data which has to be less than a constant value. */
//  if(pMsgContext->reqData[0] < 0xFF)
//  {
//    /* Received data is in range process further. */
//    /* Dummy example of how to write the response data. */
//    pMsgContext->resData[0] = 0xFF;
//    /* Always set the correct length of the response data. */
//    pMsgContext->resDataLen = 1;
//  }
//  else
//  {
    /* Request contains invalid data - send negative response! */
    DescSetNegResponse(kDescNrcRequestOutOfRange);
//  }
  /* User service processing finished. */
  DescProcessingDone();
}



/*  ********************************************************************************
 * Function name:ApplDescStopFlashErase (Service request header:$31 $2 $FF $0 )
 * Description:WARNING !!!! Start & Stop address size have to be resized for each project
 * Returns:  nothing
 * Parameter(s):
 *   - pMsgContext->reqData:
 *       - Points to the first service request data byte.
 *       - Access type: read
 *   - pMsgContext->resData:
 *       - Points to the first writeable byte for the service response data.
 *       - Access type: read/write
 *   - pMsgContext->reqDataLen:
 *       - Contains the count of the service request data bytes (Sid is excluded).
 *       - Access type: read
 *   - pMsgContext->resDataLen:
 *       - Must be initialized with the count of the service response data bytes (Sid is excluded).
 *       - Access type: read/write
 *   - pMsgContext->msgAddInfo.reqType:
 *       - Shows the addressing type of the request (kDescPhysReq or kDescFuncReq).
 *       - Access type: read
 *   - pMsgContext->msgAddInfo.resOnReq:
 *       - Indicates if there will be response. Allowed only to write only 0 !!!
 *       - Access type: read/write
 *   - pMsgContext->msgAddInfo.suppPosRes:
 *       - UDS only!If set no positive response will be sent on this request.
 *       - Access type: read
 * Particularitie(s) and limitation(s):
 *   - The function "DescProcessingDone" must be called from now on (within this
 * main-handler or later).
 *   - The function "DescSetNegResponse" can be called within this main-handler or later
 * but before calling "DescProcessingDone".
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescStopFlashErase(DescMsgContext* pMsgContext)
{
  /* <<TBD>> Remove this comment once you have completely implemented this function!!! */
  /* Dummy example how to access the request data. */
  /* Assumming the FIRST DATA byte contains important data which has to be less than a constant value. */
//  if(pMsgContext->reqData[0] < 0xFF)
//  {
//    /* Received data is in range process further. */
//    /* Dummy example of how to write the response data. */
//    pMsgContext->resData[0] = 0xFF;
//    /* Always set the correct length of the response data. */
//    pMsgContext->resDataLen = 1;
//  }
//  else
//  {
    /* Request contains invalid data - send negative response! */
    DescSetNegResponse(kDescNrcRequestOutOfRange);
//  }
  /* User service processing finished. */
  DescProcessingDone();
}


/*  ********************************************************************************
 * Function name:ApplDescStopcheckProgrammingDependencies_FlashChecksum (Service request header:$31 $2 $FF $1 )
 * Description:WARNING !!!! Start & Stop address size have to be resized for each project
 * Returns:  nothing
 * Parameter(s):
 *   - pMsgContext->reqData:
 *       - Points to the first service request data byte.
 *       - Access type: read
 *   - pMsgContext->resData:
 *       - Points to the first writeable byte for the service response data.
 *       - Access type: read/write
 *   - pMsgContext->reqDataLen:
 *       - Contains the count of the service request data bytes (Sid is excluded).
 *       - Access type: read
 *   - pMsgContext->resDataLen:
 *       - Must be initialized with the count of the service response data bytes (Sid is excluded).
 *       - Access type: read/write
 *   - pMsgContext->msgAddInfo.reqType:
 *       - Shows the addressing type of the request (kDescPhysReq or kDescFuncReq).
 *       - Access type: read
 *   - pMsgContext->msgAddInfo.resOnReq:
 *       - Indicates if there will be response. Allowed only to write only 0 !!!
 *       - Access type: read/write
 *   - pMsgContext->msgAddInfo.suppPosRes:
 *       - UDS only!If set no positive response will be sent on this request.
 *       - Access type: read
 * Particularitie(s) and limitation(s):
 *   - The function "DescProcessingDone" must be called from now on (within this
 * main-handler or later).
 *   - The function "DescSetNegResponse" can be called within this main-handler or later
 * but before calling "DescProcessingDone".
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescStopcheckProgrammingDependencies_FlashChecksum(DescMsgContext* pMsgContext)
{
  /* <<TBD>> Remove this comment once you have completely implemented this function!!! */
  /* Dummy example how to access the request data. */
  /* Assumming the FIRST DATA byte contains important data which has to be less than a constant value. */
  if(pMsgContext->reqData[0] < 0xFF)
  {
    /* Received data is in range process further. */
    /* Dummy example of how to write the response data. */
    pMsgContext->resData[0] = 0xFF;
    /* Always set the correct length of the response data. */
    pMsgContext->resDataLen = 1;
  }
  else
  {
    /* Request contains invalid data - send negative response! */
    DescSetNegResponse(kDescNrcRequestOutOfRange);
  }
  /* User service processing finished. */
  DescProcessingDone();
}


/*  ********************************************************************************
 * Function name:ApplDescReqResultsFlashErase (Service request header:$31 $3 $FF $0 )
 * Description:WARNING !!!! Start & Stop address size have to be resized for each project
 * Returns:  nothing
 * Parameter(s):
 *   - pMsgContext->reqData:
 *       - Points to the first service request data byte.
 *       - Access type: read
 *   - pMsgContext->resData:
 *       - Points to the first writeable byte for the service response data.
 *       - Access type: read/write
 *   - pMsgContext->reqDataLen:
 *       - Contains the count of the service request data bytes (Sid is excluded).
 *       - Access type: read
 *   - pMsgContext->resDataLen:
 *       - Must be initialized with the count of the service response data bytes (Sid is excluded).
 *       - Access type: read/write
 *   - pMsgContext->msgAddInfo.reqType:
 *       - Shows the addressing type of the request (kDescPhysReq or kDescFuncReq).
 *       - Access type: read
 *   - pMsgContext->msgAddInfo.resOnReq:
 *       - Indicates if there will be response. Allowed only to write only 0 !!!
 *       - Access type: read/write
 *   - pMsgContext->msgAddInfo.suppPosRes:
 *       - UDS only!If set no positive response will be sent on this request.
 *       - Access type: read
 * Particularitie(s) and limitation(s):
 *   - The function "DescProcessingDone" must be called from now on (within this
 * main-handler or later).
 *   - The function "DescSetNegResponse" can be called within this main-handler or later
 * but before calling "DescProcessingDone".
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescReqResultsFlashErase(DescMsgContext* pMsgContext)
{
  /* <<TBD>> Remove this comment once you have completely implemented this function!!! */
  /* Contains no request data */
  /* Dummy example of how to write the response data. */
  pMsgContext->resData[0] = 0xFF;
  /* Always set the correct length of the response data. */
  pMsgContext->resDataLen = 1;
  /* User service processing finished. */
  DescProcessingDone();
}

/*  ********************************************************************************
 * Function name:ApplDescReqResultscheckProgrammingDependencies_FlashChecksum (Service request header:$31 $3 $FF $1 )
 * Description:WARNING !!!! Start & Stop address size have to be resized for each project
 * Returns:  nothing
 * Parameter(s):
 *   - pMsgContext->reqData:
 *       - Points to the first service request data byte.
 *       - Access type: read
 *   - pMsgContext->resData:
 *       - Points to the first writeable byte for the service response data.
 *       - Access type: read/write
 *   - pMsgContext->reqDataLen:
 *       - Contains the count of the service request data bytes (Sid is excluded).
 *       - Access type: read
 *   - pMsgContext->resDataLen:
 *       - Must be initialized with the count of the service response data bytes (Sid is excluded).
 *       - Access type: read/write
 *   - pMsgContext->msgAddInfo.reqType:
 *       - Shows the addressing type of the request (kDescPhysReq or kDescFuncReq).
 *       - Access type: read
 *   - pMsgContext->msgAddInfo.resOnReq:
 *       - Indicates if there will be response. Allowed only to write only 0 !!!
 *       - Access type: read/write
 *   - pMsgContext->msgAddInfo.suppPosRes:
 *       - UDS only!If set no positive response will be sent on this request.
 *       - Access type: read
 * Particularitie(s) and limitation(s):
 *   - The function "DescProcessingDone" must be called from now on (within this
 * main-handler or later).
 *   - The function "DescSetNegResponse" can be called within this main-handler or later
 * but before calling "DescProcessingDone".
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescReqResultscheckProgrammingDependencies_FlashChecksum(DescMsgContext* pMsgContext)
{
  /* <<TBD>> Remove this comment once you have completely implemented this function!!! */
  /* Contains no request data */
  /* Dummy example of how to write the response data. */
  pMsgContext->resData[0] = 0xFF;
  /* Always set the correct length of the response data. */
  pMsgContext->resDataLen = 1;
  /* User service processing finished. */
  DescProcessingDone();
}
