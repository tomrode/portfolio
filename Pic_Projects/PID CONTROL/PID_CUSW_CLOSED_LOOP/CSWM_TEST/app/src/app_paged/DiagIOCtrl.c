#define DIAGIOCTRL_C

/*****************************************************************************************************
*  (c) Copyright 2011 Continental Automotive Systems, all rights reserved
*
*  All material contained herein is CONTINENTAL CONFIDENTIAL and PROPRIETARY.
*  Any reproduction of this material without written consent from 
*  Continental AG is strictly forbidden.                                  
*
*  Filename:     DiagIOCTRL.c
*
*  Created_by:   Harsha Yekollu
*
*  Date_created: 01/21/2011
*
*  Description:  Main file for UDS Diagnostics -- Write Data By LID Services. 
*
*  Revision_history:
*
*  MKS ID       Author         Date      Description of Change(s)
*  --------  -------------  --------  --------------------------------------
*  xxxxxxx   H. Yekollu     01/21/11  Initial creation for FIAT CUSW MY12 Program
*  xxxxxxx   H. Yekollu     06/02/11  Complies with 60-00-004.Xls DDT
*  74548     H. Yekollu     06/07/11  Update new DDT60-00-005 services.
* 									  - 2F 5013, 5014, 5015 and 5016 services added for EOL.	
*  75322	 H. Yekollu	    10/06/11   Cleanup the code.
* 
******************************************************************************************************/
/*****************************************************************************************************
* 									DIAGIOCTRL
* This module handles the UDS Write Data By LID - Service 0x2F.
* 
*****************************************************************************************************/

/*****************************************************************************************************
*     								Include Files
******************************************************************************************************/
/* CAN_NWM Related Include files */
#include "il_inc.h"
#include "v_inc.h"
#include "desc.h"
#include "appdesc.h"

#include "TYPEDEF.h"
#include "main.h"

#include "DiagMain.h"
#include "DiagSec.h"
#include "DiagRead.h"
#include "DiagWrite.h"
#include "DiagIOCtrl.h"
#include "canio.h"

#include "heating.h"
#include "Venting.h"
#include "StWheel_Heating.h"
#include "Temperature.h"
#include "Internal_Faults.h"
#include "AD_Filter.h"
#include "BattV.h"
#include "mw_eem.h"
#include "mc9s12xs128.h"
#include "fbl_def.h"

/* Autosar include Files */
#include "Mcu.h"
#include "Port.h"
#include "Dio.h"
#include "Gpt.h"
#include "Pwm.h"
#include "Adc.h"
#include "Wdg.h"

/*****************************************************************************************************
*     								LOCAL ENUMS
******************************************************************************************************/

/*****************************************************************************************************
*     								LOCAL #DEFINES
******************************************************************************************************/


/* Defines for IO Controlling Services EOL Purpose */
//#define D_EOL_SPECIAL_IO_START_K                  0xD100
//#define D_EOL_IO_RELAY1_K                         0xD101
//#define D_EOL_IO_RELAY2_K                         0xD102
//#define D_EOL_IO_RELAY3_LO_K                      0xD104
//
//#define D_TEST_DISABLE_NTC                        0xD10B //Not used.
//
///* Additional Diagnostic Service IDs */
//#define D_IO_CLR_INT_FLTS                         0xD10D    /* Clears all Internal Faults */
//
//#define D_IO_TEMIC_ENDTEST_K                      0xE000
//#define D_IO_UNKNOWN_PID_K                        0xFFFF

/*****************************************************************************************************
*     								LOCAL VARIABLES
******************************************************************************************************/
static enum DIAG_RESPONSE Response;
//static u_temp_l Length;
static u_temp_l DataLength;
/*****************************************************************************************************
*     								LOCAL STRUCTURES
******************************************************************************************************/
/* Structures */

/*****************************************************************************************************
*     								LOCAL FUCNTIONS
******************************************************************************************************/
extern @far void diagF_DiagResponse(enum DIAG_RESPONSE Response, u_temp_l Length, DescMsgContext* pMsgContext);
/*  ********************************************************************************
 * Function name:ApplDescControl_COM_SER_66322 (Service request header:$2F $50 $1 $3 )
 * $2F $5001 $03 - Short Term Adjustment - ACTUATE ALL HEATERS
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescControl_2F500103_Actuate_all_heaters(DescMsgContext* pMsgContext)
{
	
	/* Initialize the DataLength. If to be adjusted logic adjusts */
	DataLength = 1;
	
	/* This check is added on 02.12.2009 Is Chrysler Routine Control to test CSWM outputs active? */
	if(diag_rc_all_op_lock_b == FALSE)
    {
 	    /* If the Data is not valid, Discard the data and send Request out of range response */
		/* Valid data is from 0 to 4. 0- OFF, 1-LL1,2-HL1,3-HL2,4-HL3                        */
		if (pMsgContext->reqData[0] <= 4)
		{
			/* Set the Io lock for all heated seats. */
			diag_io_all_hs_lock_b = 1; // TRUE;
							
			/* If this flag set, mainF_CSWM_Status() function will never look for ENG_RPM condition as a valid enable condition for outputs */
			diag_io_active_b = 1; // TRUE;

			/* Get the latest value from diagnsotic request */
			diag_io_hs_rq_c[FRONT_LEFT] = pMsgContext->reqData[0];
			diag_io_hs_rq_c[FRONT_RIGHT] = pMsgContext->reqData[0];
							
			/* If variant is 4H then only do process the Rear Seat Request, else do not */
			if ((CSWM_config_c & CSWM_4HS_VARIANT_MASK_K) == CSWM_4HS_VARIANT_MASK_K)
			{
				diag_io_hs_rq_c[REAR_LEFT] = pMsgContext->reqData[0];
				diag_io_hs_rq_c[REAR_RIGHT] = pMsgContext->reqData[0];
			}
				/* Clear all vent request */
				diag_io_all_vs_lock_b = 0;
				diag_io_vs_lock_flags_c[LEFT_FRONT].b.switch_rq_lock_b  = 0;
	 			diag_io_vs_lock_flags_c[RIGHT_FRONT].b.switch_rq_lock_b = 0;
				//DataLength = 1;
				Response = RESPONSE_OK;
		}
		else
		{
			//DataLength = 1;
			Response = REQUEST_OUT_OF_RANGE;
		}
     }
     else
     {
		/* Do not allow any Diagnostic IO Control during Chrysler Routine Control to test CSWM outputs */
		//DataLength = 1;						// Set data lenght to 1
		Response = CONDITIONS_NOT_CORRECT;	// Response with conditions not correct
	 }	
					/* Check if diagnostic IO control is active */
	 if(diag_io_active_b == TRUE)
     {
		 /* For any diagnostic IO request */
		 /* Call the mainF_CSWM_Status Immediately! In order to update status in "real-time" */
		 mainF_CSWM_Status();
	 }
     else
     {
		 /* Do nothing */
	 }
	diagF_DiagResponse(Response, DataLength/*+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext);   
}
/*  ********************************************************************************
 * Function name:ApplDescControl_COM_SER_66324 (Service request header:$2F $50 $2 $3 )
 * $2F $5002 $03 - Short Term Adjustment - ACTUATE ALL VENTS
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescControl_2F500203_Actuate_all_Vents(DescMsgContext* pMsgContext)
{
	
	/* Initialize the DataLength. If to be adjusted logic adjusts */
	DataLength = 1;

    /* This check is added on 02.12.2009 Is Chrysler Routine Control to test CSWM outputs active? */
	if(diag_rc_all_op_lock_b == FALSE)
    {
		/* If Variant is equipped with 2VS, then proceed to do the IO Controlling, Else reject */
		if ((CSWM_config_c & CSWM_2VS_MASK_K) == CSWM_2VS_MASK_K)
		{						
			/* Check for valid request */
			if (pMsgContext->reqData[0] <= VL_HI)
			{
					/* Set the Io lock for all vent seats output request. */
					diag_io_all_vs_lock_b = 1; // TRUE;
								
					/* Set Diag Active flag.                                                                                      */
					/* mainF_CSWM_Status() function will never look for ENG_RPM condition as a valid enable condition for outputs */
					diag_io_active_b = 1; // TRUE;

					/* Get the latest value from diagnsotic request */
					diag_io_vs_rq_c[LEFT_FRONT] = pMsgContext->reqData[0];
					diag_io_vs_rq_c[RIGHT_FRONT] = pMsgContext->reqData[0];
					/* Set a flag that clears the vent ontime with each diag IO request */
					vs_Flag2_c[VS_FL].b.diag_clr_ontime_b = TRUE;
					vs_Flag2_c[VS_FR].b.diag_clr_ontime_b = TRUE;
					/* Clear all heat requests - while actuating all vents we will not turn off all heat requests */
					diag_io_all_hs_lock_b = 0;
					diag_io_hs_lock_flags_c[FRONT_LEFT].b.switch_rq_lock_b = 0;
					diag_io_hs_lock_flags_c[FRONT_RIGHT].b.switch_rq_lock_b = 0;
					diag_io_hs_lock_flags_c[REAR_LEFT].b.switch_rq_lock_b = 0;
					diag_io_hs_lock_flags_c[REAR_RIGHT].b.switch_rq_lock_b = 0;

					//DataLength = 1;
					Response = RESPONSE_OK;								
			}
			else
			{
					//DataLength = 1;
					/* Request is out of range */
					Response = REQUEST_OUT_OF_RANGE;
			}							
		}
		else 
        {
			//DataLength = 1;
			Response = SUBFUNCTION_NOT_SUPPORTED;
		}
	}
    else
    {
		/* Do not allow any Diagnostic IO Control during Chrysler Routine Control to test CSWM outputs */
		//DataLength = 1;						// Set data lenght to 1
		Response = CONDITIONS_NOT_CORRECT;	// Response with conditions not correct
	}
					 /* Check if diagnostic IO control is active */
    if(diag_io_active_b == TRUE)
    {
		/* For any diagnostic IO request */
		/* Call the mainF_CSWM_Status Immediately! In order to update status in "real-time" */
		mainF_CSWM_Status();
	}
    else
    {
		      /* Do nothing */
	}
  diagF_DiagResponse(Response, DataLength/*+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext); 
}
/*  ********************************************************************************
 * Function name:ApplDescControl_COM_SER_66327 (Service request header:$2F $50 $3 $3 )
 * $2F $5003 $03 - Short Term Adjustment - FRONT LEFT HEAT SEAT REQUEST - INPUT
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescControl_2F500303_F_L_Heat_Seat_Request_BI(DescMsgContext* pMsgContext)
{
	/* Initialize the DataLength. If to be adjusted logic adjusts */
	DataLength = 1;

    /* This check is added on 02.12.2009 Is Chrysler Routine Control to test CSWM outputs active?  */
	if(diag_rc_all_op_lock_b == FALSE)
    {		
		/* Check if Led Status lock is inactive and Valid data present */
		if ((!diag_io_hs_lock_flags_c[FRONT_LEFT].b.led_status_lock_b) && (!diag_io_all_hs_lock_b) && (pMsgContext->reqData[0] <= 4))
		{
			/* Set the flag. This flag takes control over normal execution of Front Left seat requests */
			diag_io_hs_lock_flags_c[FRONT_LEFT].b.switch_rq_lock_b = 1; //TRUE;

			/* Set Diag Active flag.                                                                                      */
			/* mainF_CSWM_Status() function will never look for ENG_RPM condition as a valid enable condition for outputs */
			diag_io_active_b = 1; // TRUE;

			/* Clear the front left vent IO request */
			diag_io_vs_lock_flags_c[LEFT_FRONT].b.switch_rq_lock_b = 0; // FALSE;
			/* Get the Switch request from the test tool. This value might be HL3, HL2, HL1, LL2 or LL1 */
			diag_io_hs_rq_c[FRONT_LEFT] = pMsgContext->reqData[0];
			DataLength = 0;
			Response = RESPONSE_OK;
		}
		else
		{
			//DataLength = 1;
			if (pMsgContext->reqData[0] > 5)
			{
				/* Values are not in range: Send Request out of range response */
				Response = REQUEST_OUT_OF_RANGE;
			}
			else
			{
				/* LED status lock is active : Send conditions not correct reponse */
				Response = CONDITIONS_NOT_CORRECT;
			}
		}						
	}
    else
    {
		/* Do not allow any Diagnostic IO Control during Chrysler Routine Control to test CSWM outputs */
		//DataLength = 1;						// Set data lenght to 1
		Response = CONDITIONS_NOT_CORRECT;	// Response with conditions not correct
	}
    /* Check if diagnostic IO control is active */
	if(diag_io_active_b == TRUE)
    {
		/* For any diagnostic IO request */
		/* Call the mainF_CSWM_Status Immediately! In order to update status in "real-time" */
		mainF_CSWM_Status();
	}
    else
    {
		/* Do nothing */
	}
     diagF_DiagResponse(Response, DataLength/*+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext);	
}
/*  ********************************************************************************
 * Function name:ApplDescControl_COM_SER_66329 (Service request header:$2F $50 $4 $3 )
 * $2F $5004 $03 - Short Term Adjustment - FRONT RIGHT HEAT SEAT REQUEST - INPUT
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescControl_2F500403_F_R_Heat_Seat_Request_BI(DescMsgContext* pMsgContext)
{
	/* Initialize the DataLength. If to be adjusted logic adjusts */
	DataLength = 1;

    /* This check is added on 02.12.2009 Is Chrysler Routine Control to test CSWM outputs active? */
	if(diag_rc_all_op_lock_b == FALSE)
    {
		/* Check if Led Status lock is inactive and Valid data present */
		if ((!diag_io_hs_lock_flags_c[FRONT_RIGHT].b.led_status_lock_b) && (!diag_io_all_hs_lock_b) && (pMsgContext->reqData[0] <= 4))
		{
			/* Set the flag. This flag takes control over normal execution of Front Right seat requests */
			diag_io_hs_lock_flags_c[FRONT_RIGHT].b.switch_rq_lock_b = 1; //TRUE;
							
			/* Set Diag Active flag.                                                                                      */
			/* mainF_CSWM_Status() function will never look for ENG_RPM condition as a valid enable condition for outputs */
			diag_io_active_b = 1; // TRUE;

			/* Clear the front right vent IO request */
			diag_io_vs_lock_flags_c[RIGHT_FRONT].b.switch_rq_lock_b = 0; // FALSE;	
			/* Get the Switch request from the test tool. This value might be HL3, HL2, HL1, LL2 or LL1 */
			diag_io_hs_rq_c[FRONT_RIGHT] = pMsgContext->reqData[0];
			DataLength = 0;
			Response = RESPONSE_OK;
		}
		else
		{
			//DataLength = 1;
			if (pMsgContext->reqData[0] > 5)
			{
				/* Values are not in range: Send Request out of range response */
				Response = REQUEST_OUT_OF_RANGE;
			}
			else
			{
				/* LED status lock is active : Send conditions not correct reponse */
				Response = CONDITIONS_NOT_CORRECT;
			}
		}														
	}
    else
    {
		/* Do not allow any Diagnostic IO Control during Chrysler Routine Control to test CSWM outputs */
		//DataLength = 1;						// Set data lenght to 1
		Response = CONDITIONS_NOT_CORRECT;	// Response with conditions not correct
	}
	/* Check if diagnostic IO control is active */
	if(diag_io_active_b == TRUE)
    {
		/* For any diagnostic IO request */
		/* Call the mainF_CSWM_Status Immediately! In order to update status in "real-time" */
		mainF_CSWM_Status();
	}
    else
    {
		/* Do nothing */
	}
    diagF_DiagResponse(Response, DataLength/*+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext);
}
/*  ********************************************************************************
 * Function name:ApplDescControl_COM_SER_66331 (Service request header:$2F $50 $5 $3 )
 * $2F $5005 $03 - Short Term Adjustment - REAR LEFT HEAT SEAT REQUEST - INPUT
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescControl_2F500503_R_L_Heat_Seat_Request_BI(DescMsgContext* pMsgContext)
{
	/* Initialize the DataLength. If to be adjusted logic adjusts */
	DataLength = 1;

    /* This check is added on 02.12.2009 Is Chrysler Routine Control to test CSWM outputs active? */
	if(diag_rc_all_op_lock_b == FALSE)
    {
		/* If the variant is 4H then do the IO Controlling */
		/* Else display Sub-function not supported         */
		if ((CSWM_config_c & CSWM_4HS_VARIANT_MASK_K) == CSWM_4HS_VARIANT_MASK_K)
		{
			/* Check if Led Status lock is inactive */
			if ((!diag_io_hs_lock_flags_c[REAR_LEFT].b.led_status_lock_b) && (!diag_io_all_hs_lock_b) && (pMsgContext->reqData[0] <= 4))
			{
				/* Set the flag. This flag takes control over normal execution of Rear Left seat requests */
				diag_io_hs_lock_flags_c[REAR_LEFT].b.switch_rq_lock_b = 1; // TRUE;
						
				/* Set Diag Active flag.                                                                                      */
				/* mainF_CSWM_Status() function will never look for ENG_RPM condition as a valid enable condition for outputs */
				diag_io_active_b = 1; // TRUE;

				/* Get the Switch request from the test tool. This value might be HL3, HL2, HL1, LL2 or LL1 */
				diag_io_hs_rq_c[REAR_LEFT] = pMsgContext->reqData[0];
				DataLength = 0;
				Response = RESPONSE_OK;
			}
			else
			{
				//DataLength = 1;
				if (pMsgContext->reqData[0] > 5)
				{
					/* Values are not in range: Send Request out of range response */
					Response = REQUEST_OUT_OF_RANGE;
				}
				else
				{
					/* LED status lock is active : Send conditions not correct reponse */
					Response = CONDITIONS_NOT_CORRECT;
				}						
			}									
		}
		else 
		{
			//DataLength = 1;
			Response = SUBFUNCTION_NOT_SUPPORTED;
		}		
	}
    else
    {
		/* Do not allow any Diagnostic IO Control during Chrysler Routine Control to test CSWM outputs */
		//DataLength = 1;						// Set data lenght to 1
		Response = CONDITIONS_NOT_CORRECT;	// Response with conditions not correct
	}
	/* Check if diagnostic IO control is active */
	if(diag_io_active_b == TRUE)
    {
		/* For any diagnostic IO request */
		/* Call the mainF_CSWM_Status Immediately! In order to update status in "real-time" */
		mainF_CSWM_Status();
	}
    else
    {
		/* Do nothing */
	}
    diagF_DiagResponse(Response, DataLength/*+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext);
}
/*  ********************************************************************************
 * Function name:ApplDescControl_COM_SER_66333 (Service request header:$2F $50 $6 $3 )
 * $2F $5006 $03 - Short Term Adjustment - REAR RIGHT SEAT REQUEST - INPUT
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescControl_2F500603_R_R_Heat_Seat_Request_BI(DescMsgContext* pMsgContext)
{
	/* Initialize the DataLength. If to be adjusted logic adjusts */
	DataLength = 1;

    /* This check is added on 02.12.2009 Is Chrysler Routine Control to test CSWM outputs active? */
	if(diag_rc_all_op_lock_b == FALSE)
    {	
		/* If the variant is 4H then do the IO Controlling */
		/* Else display Sub-function not supported         */
		if ((CSWM_config_c & CSWM_4HS_VARIANT_MASK_K) == CSWM_4HS_VARIANT_MASK_K)
		{
		   /* Check if Led Status lock is inactive */
			if ((!diag_io_hs_lock_flags_c[REAR_RIGHT].b.led_status_lock_b) && (!diag_io_all_hs_lock_b) && (pMsgContext->reqData[0] <= 4))
			{
				/* Set the flag. This flag takes control over normal execution of Rear Right seat requests */
				diag_io_hs_lock_flags_c[REAR_RIGHT].b.switch_rq_lock_b = 1; // TRUE;
						
				/* Set Diag Active flag.                                                                                      */
				/* mainF_CSWM_Status() function will never look for ENG_RPM condition as a valid enable condition for outputs */
				diag_io_active_b = 1; // TRUE;

				/* Get the Switch request from the test tool. This value might be HL3, HL2, HL1, LL2 or LL1 */
				diag_io_hs_rq_c[REAR_RIGHT] = pMsgContext->reqData[0];
				DataLength = 0;
				Response = RESPONSE_OK;
			}
			else
			{
				//DataLength = 1;
				if (pMsgContext->reqData[0] > 5)
				{
					/* Values are not in range: Send Request out of range response */
					Response = REQUEST_OUT_OF_RANGE;
				}
				else
				{
					/* LED status lock is active : Send conditions not correct reponse */
					Response = CONDITIONS_NOT_CORRECT;
				}								
			 }
		}
		else 
        {
			//DataLength = 1;
			Response = SUBFUNCTION_NOT_SUPPORTED;
		}
	}
    else
    {
		/* Do not allow any Diagnostic IO Control during Chrysler Routine Control to test CSWM outputs */
		//DataLength = 1;						// Set data lenght to 1
		Response = CONDITIONS_NOT_CORRECT;	// Response with conditions not correct
	}
	/* Check if diagnostic IO control is active */
	if(diag_io_active_b == TRUE)
    {
		/* For any diagnostic IO request */
		/* Call the mainF_CSWM_Status Immediately! In order to update status in "real-time" */
		mainF_CSWM_Status();
	}
    else
    {
		/* Do nothing */
	}
    diagF_DiagResponse(Response, DataLength/*+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext);
}
/*  ********************************************************************************
 * Function name:ApplDescControl_COM_SER_66335 (Service request header:$2F $50 $7 $3 )
 * $2F $5007 $03 - Short Term Adjustment - FRONT LEFT VENT SEAT REQUEST - INPUT
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescControl_2F500703_F_L_Vent_Seat_Request_BI(DescMsgContext* pMsgContext)
{
	/* Initialize the DataLength. If to be adjusted logic adjusts */
	DataLength = 1;

     /* This check is added on 02.12.2009 Is Chrysler Routine Control to test CSWM outputs active? */
	if(diag_rc_all_op_lock_b == FALSE)
    {
		/* If Variant is equipped with 2VS, then proceed to do the IO Controlling, Else reject */
		if ((CSWM_config_c & CSWM_2VS_MASK_K) == CSWM_2VS_MASK_K)
		{
			/* Check if Led Status lock is inactive */
			if ( (diag_io_vs_lock_flags_c[LEFT_FRONT].b.led_status_lock_b == FALSE) && 
				 (diag_io_all_vs_lock_b == FALSE) && 
				 (pMsgContext->reqData[0] <= VL_HI) )
			{
				/* Set the flag. This flag takes control over normal execution of Front Left vent requests */
				diag_io_vs_lock_flags_c[LEFT_FRONT].b.switch_rq_lock_b = 1; // TRUE;
								
				/* Set Diag Active flag.                                                                                      */
				/* mainF_CSWM_Status() function will never look for ENG_RPM condition as a valid enable condition for outputs */
				diag_io_active_b = 1; // TRUE;

				/* Set a flag that clears the vent ontime with each diag IO request */
				vs_Flag2_c[VS_FL].b.diag_clr_ontime_b = TRUE;
				/* Clear the front left heat IO request */
				diag_io_hs_lock_flags_c[FRONT_LEFT].b.switch_rq_lock_b = 0; // FALSE;
				/* Get the Switch request from the test tool. This value might be HL3, HL2, HL1, LL2 or LL1 */
				diag_io_vs_rq_c[LEFT_FRONT] = pMsgContext->reqData[0];
				DataLength = 0;
				Response = RESPONSE_OK;
			}
			else
			{
				//DataLength = 1;
				/* Check LED lock */
				if ( (diag_io_vs_lock_flags_c[LEFT_FRONT].b.led_status_lock_b == TRUE) || (diag_io_all_vs_lock_b == TRUE) )
				{
					/* LED status lock is active: Send conditions not correct reponse */
					Response = CONDITIONS_NOT_CORRECT;
				}
				else
				{
					/* Request is out of range */
					Response = REQUEST_OUT_OF_RANGE;
				}
			}						
		}
		else 
		{
			//DataLength = 1;
			Response = SUBFUNCTION_NOT_SUPPORTED;
		}
    }
    else
    {
				/* Do not allow any Diagnostic IO Control during Chrysler Routine Control to test CSWM outputs */
				//DataLength = 1;						// Set data lenght to 1
				Response = CONDITIONS_NOT_CORRECT;	// Response with conditions not correct
	}
     /* Check if diagnostic IO control is active */
	 if(diag_io_active_b == TRUE)
     {
		/* For any diagnostic IO request */
		/* Call the mainF_CSWM_Status Immediately! In order to update status in "real-time" */
		mainF_CSWM_Status();
	 }
     else
     {
		/* Do nothing */
	 }
      diagF_DiagResponse(Response, DataLength/*+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext);	
}
/*  ********************************************************************************
 * Function name:ApplDescControl_COM_SER_66337 (Service request header:$2F $50 $8 $3 )
 * $2F $5008 $03 - Short Term Adjustment - FRONT RIGHT VENT SEAT REQUEST - INPUT
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescControl_2F500803_F_R_Vent_Seat_Request_BI(DescMsgContext* pMsgContext)
{

	/* Initialize the DataLength. If to be adjusted logic adjusts */
	DataLength = 1;

    /* This check is added on 02.12.2009 Is Chrysler Routine Control to test CSWM outputs active? */
	if(diag_rc_all_op_lock_b == FALSE)
    {
		/* If Variant is equipped with 2VS, then proceed to do the IO Controlling, Else reject */
		if ((CSWM_config_c & CSWM_2VS_MASK_K) == CSWM_2VS_MASK_K)
		{				
            /* Check if Led Status lock is inactive */
			if ( (diag_io_vs_lock_flags_c[RIGHT_FRONT].b.led_status_lock_b == FALSE) && 
				 (diag_io_all_vs_lock_b == FALSE) && 
				 (pMsgContext->reqData[0] <= VL_HI) )
			{
				/* Set the flag. This flag takes control over normal execution of Front Right vent requests */
				diag_io_vs_lock_flags_c[RIGHT_FRONT].b.switch_rq_lock_b = 1; // TRUE;
								
				/* Set Diag Active flag.                                                                                      */
				/* mainF_CSWM_Status() function will never look for ENG_RPM condition as a valid enable condition for outputs */
				diag_io_active_b = 1; // TRUE;							

				/* Set a flag that clears the FR vent ontime with each diag IO request */
				vs_Flag2_c[VS_FR].b.diag_clr_ontime_b = TRUE;
				/* Clear the front right heat IO request */
				diag_io_hs_lock_flags_c[FRONT_RIGHT].b.switch_rq_lock_b = 0; // FALSE;
				/* Get the Switch request from the test tool. This value might be HL3, HL2, HL1, LL2 or LL1 */
				diag_io_vs_rq_c[RIGHT_FRONT] = pMsgContext->reqData[0];
				DataLength = 0;
				Response = RESPONSE_OK;
			}
			else 
            {
				//DataLength = 1;
				/* Check LED lock */
				if ( (diag_io_vs_lock_flags_c[RIGHT_FRONT].b.led_status_lock_b == TRUE) || (diag_io_all_vs_lock_b == TRUE) ) 
                {
					/* LED status lock is active: Send conditions not correct reponse */
					Response = CONDITIONS_NOT_CORRECT;
				}
				else 
                {
					/* Request is out of range */
					Response = REQUEST_OUT_OF_RANGE;
				}
			}		
		}
		else 
        {
			//DataLength = 1;
			Response = SUBFUNCTION_NOT_SUPPORTED;
		}
	}
    else
    {
		/* Do not allow any Diagnostic IO Control during Chrysler Routine Control to test CSWM outputs */
		//DataLength = 1;						// Set data lenght to 1
		Response = CONDITIONS_NOT_CORRECT;	// Response with conditions not correct
	}
     /* Check if diagnostic IO control is active */
	if(diag_io_active_b == TRUE)
    {
		/* For any diagnostic IO request */
		/* Call the mainF_CSWM_Status Immediately! In order to update status in "real-time" */
		mainF_CSWM_Status();
	}
    else
    {
		/* Do nothing */
	}
    diagF_DiagResponse(Response, DataLength/*+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext);		
}
/*  ********************************************************************************
 * Function name:ApplDescControl_COM_SER_66339 (Service request header:$2F $50 $9 $3 )
 * $2F $5009 $03 - Short Term Adjustment - HEATED STEERING WHEEL REQUEST - INPUT
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescControl_2F500903_Heated_Steering_Wheel_Request_BI(DescMsgContext* pMsgContext)
{
	/* Initialize the DataLength. If to be adjusted logic adjusts */
	DataLength = 1;

    /* This check is added on 02.12.2009 Is Chrysler Routine Control to test CSWM outputs active? */
	if(diag_rc_all_op_lock_b == FALSE)
    {			
		if ((CSWM_config_c & CSWM_HSW_MASK_K) == CSWM_HSW_MASK_K)
		{
			/* Check if Led Status lock is inactive */
			if ( (!diag_io_st_whl_state_lock_b) && (pMsgContext->reqData[0] <= 4) )
			{
				/* Set the flag. This flag takes control over normal execution of steering wheel requests */
				diag_io_st_whl_rq_lock_b = 1; // TRUE;				
				/* Set Diag Active flag.                                                                                      */
				/* mainF_CSWM_Status() function will never look for ENG_RPM condition as a valid enable condition for outputs */
				diag_io_active_b = 1; // TRUE;

				/* Set a flag that clears the stW ontime with each diag IO request */
				stW_clr_ontime_b = TRUE;
				/* Get the Switch request from the test tool. This value might be ON or OFF */
				diag_io_st_whl_rq_c = pMsgContext->reqData[0];
				DataLength = 0;
				Response = RESPONSE_OK;
			}
			else
			{
				//DataLength = 1;
				if (pMsgContext->reqData[0] <= 4)
				{
					/* Switch lock is active: Send conditions not correct reponse */
					Response = CONDITIONS_NOT_CORRECT;
				}
				else
				{
					/* Request is out of range */
					Response = REQUEST_OUT_OF_RANGE;
				}
			}														
		}
		else 
        {
			//DataLength = 1;
			Response = SUBFUNCTION_NOT_SUPPORTED;
		}			
	}
    else
    {
		/* Do not allow any Diagnostic IO Control during Chrysler Routine Control to test CSWM outputs */
		//DataLength = 1;						// Set data lenght to 1
		Response = CONDITIONS_NOT_CORRECT;	// Response with conditions not correct
	}
	/* Check if diagnostic IO control is active */
	if(diag_io_active_b == TRUE)
    {
		/* For any diagnostic IO request */
		/* Call the mainF_CSWM_Status Immediately! In order to update status in "real-time" */
		mainF_CSWM_Status();
	}
    else
    {
		/* Do nothing */
	}
    diagF_DiagResponse(Response, DataLength/*+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext);
}
/*  ********************************************************************************
 * Function name:ApplDescControl_COM_SER_66341 (Service request header:$2F $50 $A $3 )
 * $2F $500A $03 - Short Term Adjustment - FRONT LEFT HEAT SEAT STATUS - OUTPUT
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescControl_2F500A03_F_L_Heat_Seat_Status_BO(DescMsgContext* pMsgContext)
{
	/* Initialize the DataLength. If to be adjusted logic adjusts */
	DataLength = 1;

    /* This check is added on 02.12.2009 Is Chrysler Routine Control to test CSWM outputs active? */
	if(diag_rc_all_op_lock_b == FALSE)
    {
        /* Check if Switch lock is inactive */
		if ((!diag_io_hs_lock_flags_c[FRONT_LEFT].b.switch_rq_lock_b) && (!diag_io_all_hs_lock_b) && ((pMsgContext->reqData[0] <= 1)||(pMsgContext->reqData[0] == 3)))
		{
			/* Set the flag. This flag takes control over normal execution of Front Left seat LED Status */
			diag_io_hs_lock_flags_c[FRONT_LEFT].b.led_status_lock_b = 1; // TRUE;
			/* Clear the front left vent LED request */
			diag_io_vs_lock_flags_c[LEFT_FRONT].b.led_status_lock_b = 0; // FALSE;
			/* Get the Switch request from the test tool. This value might be HIGH,LOW or OFF */
			diag_io_hs_state_c[FRONT_LEFT] = pMsgContext->reqData[0];
			DataLength = 0;
			Response = RESPONSE_OK;
		}
		else
		{
			//DataLength = 1;
			/* Check to see the requested value is in range or not */
			if ((pMsgContext->reqData[0] > 3) || (pMsgContext->reqData[0] == 2) )
			{
				/* Values are not in range: Send Request out of range response */
				Response = REQUEST_OUT_OF_RANGE;
			}
			else
			{
				/* Switch lock is active: Send conditions not correct reponse */
				Response = CONDITIONS_NOT_CORRECT;
			}
		}
	}
    else
    {
		/* Do not allow any Diagnostic IO Control during Chrysler Routine Control to test CSWM outputs */
		//DataLength = 1;						// Set data lenght to 1
		Response = CONDITIONS_NOT_CORRECT;	// Response with conditions not correct
	}

	diagF_DiagResponse(Response, DataLength/*+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext);
}
/*  ********************************************************************************
 * Function name:ApplDescControl_COM_SER_66343 (Service request header:$2F $50 $B $3 )
 * $2F $500B $03 - Short Term Adjustment - FRONT  RIGHT HEAT SEAT STATUS - OUTPUT
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescControl_2F500B03_F_R_Heat_Seat_Status_BO(DescMsgContext* pMsgContext)
{
	/* Initialize the DataLength. If to be adjusted logic adjusts */
	DataLength = 1;

    /* This check is added on 02.12.2009 Is Chrysler Routine Control to test CSWM outputs active? */
	if(diag_rc_all_op_lock_b == FALSE)
    {
        if ((!diag_io_hs_lock_flags_c[FRONT_RIGHT].b.switch_rq_lock_b) && (!diag_io_all_hs_lock_b)&& ((pMsgContext->reqData[0] <= 1)||(pMsgContext->reqData[0] == 3)))
		{
			/* Set the flag. This flag takes control over normal execution of Front right seat LED Status */
			diag_io_hs_lock_flags_c[FRONT_RIGHT].b.led_status_lock_b = 1; // TRUE;
			/* Clear the front right vent LED request */
			diag_io_vs_lock_flags_c[RIGHT_FRONT].b.led_status_lock_b = 0; // FALSE;
			/* Get the Switch request from the test tool. This value might be HIGH,LOW or OFF */
			diag_io_hs_state_c[FRONT_RIGHT] = pMsgContext->reqData[0];

			DataLength = 0;
			Response = RESPONSE_OK;
		}
		else
		{
			//DataLength = 1;
			if ((pMsgContext->reqData[0] > 3) || (pMsgContext->reqData[0] == 2) )
			{
				/* Values are not in range: Send Request out of range response */
				Response = REQUEST_OUT_OF_RANGE;
			}
			else
			{
				/* Switch lock is active: Send conditions not correct reponse */
				Response = CONDITIONS_NOT_CORRECT;
			}
		}							
	}
    else
    {
		/* Do not allow any Diagnostic IO Control during Chrysler Routine Control to test CSWM outputs */
		//DataLength = 1;						// Set data lenght to 1
		Response = CONDITIONS_NOT_CORRECT;	// Response with conditions not correct
	}
     /* Check if diagnostic IO control is active */
	diagF_DiagResponse(Response, DataLength/*+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext);	
}
/*  ********************************************************************************
 * Function name:ApplDescControl_COM_SER_66345 (Service request header:$2F $50 $C $3 )
 * $2F $500C $03 - Short Term Adjustment - REAR LEFT HEAT SEAT STATUS - OUTPUT
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescControl_2F500C03_R_L_Heat_Seat_Status_BO(DescMsgContext* pMsgContext)
{
	/* Initialize the DataLength. If to be adjusted logic adjusts */
	DataLength = 1;

    /* This check is added on 02.12.2009 Is Chrysler Routine Control to test CSWM outputs active? */
	if(diag_rc_all_op_lock_b == FALSE)
    {
		/* If the variant is 4H then do the IO Controlling */
		/* Else display Sub-function not supported         */
		if ((CSWM_config_c & CSWM_4HS_VARIANT_MASK_K) == CSWM_4HS_VARIANT_MASK_K)
		{
			/* Check if Switch lock is inactive */
			if ((!diag_io_hs_lock_flags_c[REAR_LEFT].b.switch_rq_lock_b) && (!diag_io_all_hs_lock_b) && ((pMsgContext->reqData[0] <= 1)||(pMsgContext->reqData[0] == 3)))
			{
				/* Set the flag. This flag takes control over normal execution of Rear Left seat LED Status */
				diag_io_hs_lock_flags_c[REAR_LEFT].b.led_status_lock_b = 1; // TRUE;
				/* Get the Switch request from the test tool. This value might be HIGH,LOW or OFF */
				diag_io_hs_state_c[REAR_LEFT] = pMsgContext->reqData[0];
				DataLength = 0;
				Response = RESPONSE_OK;
			}
			else
			{
				//DataLength = 1;
				if ((pMsgContext->reqData[0] > 3) || (pMsgContext->reqData[0] == 2) )
				{
					/* Values are not in range: Send Request out of range response */
					Response = REQUEST_OUT_OF_RANGE;
				}
				else
				{
					/* Switch lock is active: Send conditions not correct reponse */
					Response = CONDITIONS_NOT_CORRECT;
				}
			}
		}
		else 
        {
			//DataLength = 1;
			Response = SUBFUNCTION_NOT_SUPPORTED;
		}				
	}
    else
    {
		/* Do not allow any Diagnostic IO Control during Chrysler Routine Control to test CSWM outputs */
		DataLength = 1;						// Set data lenght to 1
		Response = CONDITIONS_NOT_CORRECT;	// Response with conditions not correct
	}

	diagF_DiagResponse(Response, DataLength/*+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext);	
}
/*  ********************************************************************************
 * Function name:ApplDescControl_COM_SER_66348 (Service request header:$2F $50 $D $3 )
 * $2F $500D $03 - Short Term Adjustment - REAR RIGHT HEAT SEAT STATUS - OUTPUT
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescControl_2F500D03_R_R_Heat_Seat_Status_BO(DescMsgContext* pMsgContext)
{
	/* Initialize the DataLength. If to be adjusted logic adjusts */
	DataLength = 1;

    /* This check is added on 02.12.2009 Is Chrysler Routine Control to test CSWM outputs active? */
	if(diag_rc_all_op_lock_b == FALSE)
    {	
		/* If the variant is 4H then do the IO Controlling */
		/* Else display Sub-function not supported         */
		if ((CSWM_config_c & CSWM_4HS_VARIANT_MASK_K) == CSWM_4HS_VARIANT_MASK_K)
		{			
			/* Check if Switch lock is inactive and data is in valid range */
			if ((!diag_io_hs_lock_flags_c[REAR_RIGHT].b.switch_rq_lock_b) && (!diag_io_all_hs_lock_b) && ((pMsgContext->reqData[0] <= 1)||(pMsgContext->reqData[0] == 3)))
			{
				/* Set the flag. This flag takes control over normal execution of Rear Right seat LED Status */
				diag_io_hs_lock_flags_c[REAR_RIGHT].b.led_status_lock_b = 1; // TRUE;
				/* Get the Switch request from the test tool. This value might be HIGH,LOW or OFF */
				diag_io_hs_state_c[REAR_RIGHT] = pMsgContext->reqData[0];
				DataLength = 0;
				Response = RESPONSE_OK;
			}
			else
			{
				//DataLength = 1;
				if ((pMsgContext->reqData[0] > 3) || (pMsgContext->reqData[0] == 2) )
				{
					/* Values are not in range: Send Request out of range response */
					Response = REQUEST_OUT_OF_RANGE;
				}
				else
				{
					/* Switch lock is active: Send conditions not correct reponse */
					Response = CONDITIONS_NOT_CORRECT;
				}
			}
		}
		else 
        {
			//DataLength = 1;
			Response = SUBFUNCTION_NOT_SUPPORTED;
		}				
	}
    else
    {
		/* Do not allow any Diagnostic IO Control during Chrysler Routine Control to test CSWM outputs */
		//DataLength = 1;						// Set data lenght to 1
		Response = CONDITIONS_NOT_CORRECT;	// Response with conditions not correct
	}

	diagF_DiagResponse(Response, DataLength/*+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext);	
}
/*  ********************************************************************************
 * Function name:ApplDescControl_COM_SER_66352 (Service request header:$2F $50 $E $3 )
 * $2F $500E $03 - Short Term Adjustment - FRONT LEFT VENT SEAT STATUS - OUTPUT
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescControl_2F500E03_F_L_Vent_Seat_Status_BO(DescMsgContext* pMsgContext)
{
	/* Initialize the DataLength. If to be adjusted logic adjusts */
	DataLength = 1;

    /* This check is added on 02.12.2009 Is Chrysler Routine Control to test CSWM outputs active? */
	if(diag_rc_all_op_lock_b == FALSE)
    {				
		/* If Variant is equipped with 2VS, then proceed to do the IO Controlling, Else reject */
		if ((CSWM_config_c & CSWM_2VS_MASK_K) == CSWM_2VS_MASK_K)
		{
	        /* Check if Switch lock is inactive */
			if ( (diag_io_vs_lock_flags_c[LEFT_FRONT].b.switch_rq_lock_b == FALSE) && (diag_io_all_vs_lock_b == FALSE) && ( (pMsgContext->reqData[0] == VL0) || (pMsgContext->reqData[0] == VL_LOW) || (pMsgContext->reqData[0] == VL_HI) ) )
			{
				/* Set the flag. This flag takes control over normal execution of Front Left vent LED Status */
				diag_io_vs_lock_flags_c[LEFT_FRONT].b.led_status_lock_b = 1; // TRUE;
				/* Clear the front left heat LED request */
				diag_io_hs_lock_flags_c[FRONT_LEFT].b.led_status_lock_b = 0; // FALSE;
				/* Get the Switch request from the test tool. This value might be HIGH,LOW or OFF */
				diag_io_vs_state_c[LEFT_FRONT] = pMsgContext->reqData[0];
				DataLength = 0;
				Response = RESPONSE_OK;
			}
			else
			{
				//DataLength = 1;
				/* Check switch lock */
				if ( (diag_io_vs_lock_flags_c[LEFT_FRONT].b.switch_rq_lock_b == TRUE) || (diag_io_all_vs_lock_b == TRUE) )
				{
					/* Switch lock is active: Send conditions not correct reponse */
					Response = CONDITIONS_NOT_CORRECT;
				}
				else
				{
					/* Request is out of range */
					Response = REQUEST_OUT_OF_RANGE;
				}
			}				
		}
		else 
        {
			//DataLength = 1;
			Response = SUBFUNCTION_NOT_SUPPORTED;
		}				
	}
    else
    {
		/* Do not allow any Diagnostic IO Control during Chrysler Routine Control to test CSWM outputs */
		//DataLength = 1;						// Set data lenght to 1
		Response = CONDITIONS_NOT_CORRECT;	// Response with conditions not correct
	}

	diagF_DiagResponse(Response, DataLength/*+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext);	
}
/*  ********************************************************************************
 * Function name:ApplDescControl_COM_SER_66354 (Service request header:$2F $50 $F $3 )
 * $2F $500F $03 - Short Term Adjustment - FRONT RIGHT VENT SEAT STATUS - OUTPUT
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescControl_2F500F03_F_R_Vent_Seat_Status_BO(DescMsgContext* pMsgContext)
{
	/* Initialize the DataLength. If to be adjusted logic adjusts */
	DataLength = 1;

    /* This check is added on 02.12.2009 Is Chrysler Routine Control to test CSWM outputs active? */
	if(diag_rc_all_op_lock_b == FALSE)
    {				
		/* If Variant is equipped with 2VS, then proceed to do the IO Controlling, Else reject */
		if ((CSWM_config_c & CSWM_2VS_MASK_K) == CSWM_2VS_MASK_K) 
        {
            /* Check if Switch lock is inactive */
			if ( (diag_io_vs_lock_flags_c[RIGHT_FRONT].b.switch_rq_lock_b == FALSE) && (diag_io_all_vs_lock_b == FALSE) && ( (pMsgContext->reqData[0] == VL0) || (pMsgContext->reqData[0] == VL_LOW) || (pMsgContext->reqData[0] ==VL_HI) ) )
			{
				/* Set the flag. This flag takes control over normal execution of Front right vent LED Status */
				diag_io_vs_lock_flags_c[RIGHT_FRONT].b.led_status_lock_b = 1; // TRUE;
				/* Clear the front right heat LED request */
				diag_io_hs_lock_flags_c[FRONT_RIGHT].b.led_status_lock_b = 0; // FALSE;
				/* Get the LED request from the test tool. This value might be HIGH,LOW or OFF */
				diag_io_vs_state_c[RIGHT_FRONT] = pMsgContext->reqData[0];
				DataLength = 0;
				Response = RESPONSE_OK;
			}
			else
			{
				//DataLength = 1;
				/* Check Switch Lock */
				if ( (diag_io_vs_lock_flags_c[RIGHT_FRONT].b.switch_rq_lock_b == TRUE) || (diag_io_all_vs_lock_b == TRUE) )
				{
					/* Switch lock is active: Send conditions not correct reponse */
					Response = CONDITIONS_NOT_CORRECT;
				}
				else
				{
					/* Request is out of range */
					Response = REQUEST_OUT_OF_RANGE;
				}
			}
		}
		else 
        {
			//DataLength = 1;
			Response = SUBFUNCTION_NOT_SUPPORTED;
		}				
	}
    else
    {
		/* Do not allow any Diagnostic IO Control during Chrysler Routine Control to test CSWM outputs */
		//DataLength = 1;						// Set data lenght to 1
		Response = CONDITIONS_NOT_CORRECT;	// Response with conditions not correct
	}

	diagF_DiagResponse(Response, DataLength/*+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext);	
}
/*  ********************************************************************************
 * Function name:ApplDescControl_COM_SER_66356 (Service request header:$2F $50 $10 $3 )
 * $2F $5010 $03 - Short Term Adjustment - HEATED STEERING WHEEL STATUS - OUTPUT
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescControl_2F501003_Heated_Steering_Wheel_Status_BO(DescMsgContext* pMsgContext)
{
	/* Initialize the DataLength. If to be adjusted logic adjusts */
	DataLength = 1;

    /* This check is added on 02.12.2009 Is Chrysler Routine Control to test CSWM outputs active? */
	if(diag_rc_all_op_lock_b == FALSE)
    {			
		if ((CSWM_config_c & CSWM_HSW_MASK_K) == CSWM_HSW_MASK_K)
		{
			/* Check if switch lock is inactive */
			if ( (!diag_io_st_whl_rq_lock_b) && (pMsgContext->reqData[0] <= 4) )
			{
				/* Set the flag. This flag takes control over normal execution of Steering Wheel LED Status */
				diag_io_st_whl_state_lock_b = 1; // TRUE;
				/* Get the Switch request from the test tool. This value might be ON or OFF */
				diag_io_st_whl_state_c = pMsgContext->reqData[0];
				DataLength = 0;
				Response = RESPONSE_OK;
			}
			else
			{
				//DataLength = 1;
				if (pMsgContext->reqData[0] <= 3)
				{
					/* Switch lock is active: Send conditions not correct reponse */
					Response = CONDITIONS_NOT_CORRECT;
				}
				else
				{
					/* Request is out of range */
					Response = REQUEST_OUT_OF_RANGE;
				}
			}
		}
		else 
        {
			//DataLength = 1;
			Response = SUBFUNCTION_NOT_SUPPORTED;
		}				
	}
    else
    {
		/* Do not allow any Diagnostic IO Control during Chrysler Routine Control to test CSWM outputs */
		//DataLength = 1;						// Set data lenght to 1
		Response = CONDITIONS_NOT_CORRECT;	// Response with conditions not correct
	}
	
	diagF_DiagResponse(Response, DataLength/*+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext);	
}
/*  ********************************************************************************
 * Function name:ApplDescControl_COM_SER_66358 (Service request header:$2F $50 $11 $3 )
 * $2F $5011 $03 - Short Term Adjustment - LOAD SHEDDING STATUS - INPUT
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescControl_2F501103_Load_shedding_status_BI(DescMsgContext* pMsgContext)
{
	if (pMsgContext->reqData[0] <= 4)
	{
		/* Set the flag. This flag takes control over normal execution of Load Shed Status */
		diag_io_load_shed_lock_b = 1; // TRUE;
		/* Get the Switch request from the test tool. This value might be ON or OFF */
		diag_io_load_shed_c = pMsgContext->reqData[0];
		/* Store the Diagnostic Load Shed request in canio load shed */
		canio_LOAD_SHED_c = diag_io_load_shed_c;
		DataLength = 0;
		Response = RESPONSE_OK;
	}
	else
	{
		DataLength = 1;
		/* Request is out of range */
		Response = REQUEST_OUT_OF_RANGE;
	}

	diagF_DiagResponse(Response, DataLength/*+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext);	
}
/*  ********************************************************************************
 * Function name:ApplDescControl_COM_SER_66362 (Service request header:$2F $50 $12 $3 )
 * $2F $5012 $03 - Short Term Adjustment - IGNITION STATUS - INPUT
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescControl_2F501203_Ignition_Status(DescMsgContext* pMsgContext)
{
    if (pMsgContext->reqData[0] <= 6)
	{
		/* Set the flag. This flag takes control over normal execution of IGN Status */
		diag_io_ign_lock_b = 1; // TRUE;
		/* Get the Switch request from the test tool. This value might be ON or OFF */
		diag_io_ign_state_c = pMsgContext->reqData[0];
		/* Supports Ignition Status by IO Control */
		canio_RX_IGN_STATUS_c = diag_io_ign_state_c;
		DataLength = 0;
		Response = RESPONSE_OK;
	}
	else
	{
		DataLength = 1;
		/* Request is out of range */
		Response = REQUEST_OUT_OF_RANGE;
	}
	/* Check if diagnostic IO control is active */
	if(diag_io_active_b == TRUE)
    {
		/* For any diagnostic IO request */
		/* Call the mainF_CSWM_Status Immediately! In order to update status in "real-time" */
		mainF_CSWM_Status();
	}
    else
    {
		/* Do nothing */
	}
    diagF_DiagResponse(Response, DataLength/*+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext);	
}

/*  ********************************************************************************
 * Function name:ApplDescControl_2F501303_Flash_Program_Data_1 (Service request header:$2F $50 $13 $3 )
 * 2F 5013 03 01(Turn ON Special EOL Tests)
 * With out this service, No EOL services will get functioned
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescControl_2F501303_Flash_Program_Data_1(DescMsgContext* pMsgContext)
{
	if (pMsgContext->reqData[0] == 1)
	{
		/* Enable the Special EOL IO Commands */
		diag_eol_io_lock_b = TRUE;
	}
	else
	{
		/* Disable the EOL Special Commands */
		diag_eol_io_lock_b = FALSE;
	}
	//pMsgContext->resData[3] = pMsgContext->reqData[3];
	DataLength = 0;
	Response = RESPONSE_OK;
	
    diagF_DiagResponse(Response, DataLength/*+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext);	
	
}

/*  ********************************************************************************
 * Function name:ApplDescControl_2F501403_Flash_Program_Data_2 (Service request header:$2F $50 $14 $3 )
 * 2F 5014 03 01(Turn ON  Control Relay A with out turning ON the FRONT SEATS.
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescControl_2F501403_Flash_Program_Data_2(DescMsgContext* pMsgContext)
{
	if (diag_eol_io_lock_b)
	{
		if (pMsgContext->reqData[0] == 1)
		{
			/* Set No Fault detection bit for Relay A */
			diag_eol_no_fault_detection_b = TRUE;

			/* There is a request for 100% PWM. Turn ON Base Relay with 100% PWM */
			Pwm_SetDutyCycle(PWM_BASE_REL, 0x8000);

			/* Turn ON the Port A */
			Dio_WriteChannel(REL_A_EN, STD_LOW);

		}
		else
		{
			/* Clear No Fault detection bit */
			diag_eol_no_fault_detection_b = FALSE;

			/* Turn OFF the Relay A port */
			Dio_WriteChannel(REL_A_EN, STD_HIGH);

			/* Turn OFF Base Relay  */
			Pwm_SetDutyCycle(PWM_BASE_REL, 0x0000);

		}
		DataLength = 0;
		Response = RESPONSE_OK;
	}
	else
	{
		DataLength = 1;
		Response = CONDITIONS_NOT_CORRECT;
	}
	
    diagF_DiagResponse(Response, DataLength/*+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext);	
	
}

/*  ********************************************************************************
 * Function name:ApplDescControl_2F501503_Flash_Program_Data_3 (Service request header:$2F $50 $15 $3 )
 * 2F 5015 03 01(Turn ON  Control Relay B with out turning ON the REAR SEATS.
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescControl_2F501503_Flash_Program_Data_3(DescMsgContext* pMsgContext)
{
	if (diag_eol_io_lock_b) {
		if (pMsgContext->reqData[0] == 1)
		{
			/* Set No Fault detection bit for Relay A */
			diag_eol_no_fault_detection_b = TRUE;

			/* There is a request for 100% PWM. Turn ON Base Relay with 100% PWM */
			Pwm_SetDutyCycle(PWM_BASE_REL, 0x8000);

			/* Turn ON the Port B */
			Dio_WriteChannel(REL_B_EN, STD_LOW);

		}
		else
		{
			/* Clear No Fault detection bit */
			diag_eol_no_fault_detection_b = FALSE;

			/* Turn OFF the Relay B port */
			Dio_WriteChannel(REL_B_EN, STD_HIGH);

			/* Turn OFF Base Relay  */
			Pwm_SetDutyCycle(PWM_BASE_REL, 0x0000);

		}
		DataLength = 0;
		Response = RESPONSE_OK;
	}
	else
	{
		DataLength = 1;
		Response = CONDITIONS_NOT_CORRECT;
	}
    diagF_DiagResponse(Response, DataLength/*+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext);	
	
}

/*  ********************************************************************************
 * Function name:ApplDescControl_2F501603_Flash_Program_Data_4 (Service request header:$2F $50 $16 $3 )
 * 2F 5016 03 01(Turn ON  Control Relay C LOW SIDE with out turning ON the WHEEL.
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescControl_2F501603_Flash_Program_Data_4(DescMsgContext* pMsgContext)
{
	if (diag_eol_io_lock_b)
	{
		if (pMsgContext->reqData[0] == 1)
		{
			/* Set No Fault detection bit for Relay A */
			diag_eol_no_fault_detection_b = TRUE;

			/* There is a request for 100% PWM. Turn ON Base Relay with 100% PWM */
			Pwm_SetDutyCycle(PWM_BASE_REL, 0x8000);

			/* Turn ON the Relay C Steering Wheel */
			Dio_WriteChannel(REL_STW_EN, STD_LOW);
		}
		else
		{
			/* Clear No Fault detection bit */
			diag_eol_no_fault_detection_b = FALSE;

			/* Turn OFF the Relay C port */
			Dio_WriteChannel(REL_STW_EN, STD_HIGH);

			/* Turn OFF Base Relay  */
			Pwm_SetDutyCycle(PWM_BASE_REL, 0x0000);
		}
		DataLength = 0;
		Response = RESPONSE_OK;
	}
	else
	{
		DataLength = 1;
		Response = CONDITIONS_NOT_CORRECT;
	}
    diagF_DiagResponse(Response, DataLength/*+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext);	
	
}

/*  ********************************************************************************
 * Function name:ApplDescReturnControl_2F500103_Actuate_all_heaters (Service request header:$2F $50 $1 $0 )
 * $2F $5001 $00 - RETURN CONTROL TO ECU - ACTUATE ALL HEATERS
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescReturnControl_2F500103_Actuate_all_heaters(DescMsgContext* pMsgContext)
{
  	/* clear the Io lock for all heat seats. Returns control to normal exection */
	diag_io_all_hs_lock_b = 0; //FALSE;
	/* If this flag clears, mainF_CSWM_Status() function will look for ENG_RPM condition as a valid enable condition for outputs */
	diag_io_active_b = 0; //FALSE;
	/* Clear the Diag IO variable as we are returning control to ECU */
	diag_io_hs_rq_c[FRONT_LEFT] = 0;
	diag_io_hs_rq_c[FRONT_RIGHT] = 0;
	diag_io_hs_rq_c[REAR_LEFT] = 0;
	diag_io_hs_rq_c[REAR_RIGHT] = 0;
	//DataLength = 0;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, 0/*DataLength+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext); 
}
/*  ********************************************************************************
 * Function name:ApplDescReturnControl_2F500203_Actuate_all_Vents (Service request header:$2F $50 $2 $0 )
 * $2F $5002 $00 - RETURN CONTROL TO ECU - ACTUATE ALL VENTS
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescReturnControl_2F500203_Actuate_all_Vents(DescMsgContext* pMsgContext)
{
	/* clear the Io lock for all vent seats. Returns control to normal exection */
	diag_io_all_vs_lock_b = 0; // FALSE;
	diag_io_vs_rq_c[LEFT_FRONT] = 0;	// added by can
	diag_io_vs_rq_c[RIGHT_FRONT] = 0;	// added by can				
	/* If this flag clears, mainF_CSWM_Status() function will look for ENG_RPM condition as a valid enable condition for outputs */
	diag_io_active_b = 0; // FALSE;
	//DataLength = 1;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, 0/*DataLength+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext); 				
}
/*  ********************************************************************************
 * Function name:ApplDescReturnControl_2F500303_F_L_Heat_Seat_Request_BI (Service request header:$2F $50 $3 $0 )
 * $2F $5003 $00 - RETURN CONTROL TO ECU - FRONT LEFT HEAT SEAT REQUEST
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescReturnControl_2F500303_F_L_Heat_Seat_Request_BI(DescMsgContext* pMsgContext)
{
	/* Clear the lock flag. by clearing this flag returns control to normal execution of Front Left seat requests */
	diag_io_hs_lock_flags_c[FRONT_LEFT].b.switch_rq_lock_b = 0; // FALSE;					
	/* If this flag clears, mainF_CSWM_Status() function will look for ENG_RPM condition as a valid enable condition for outputs */
	diag_io_active_b = 0; // FALSE;
	/* Clear the Diag IO variable as we are returning control to ECU */
	diag_io_hs_rq_c[FRONT_LEFT] = 0;
	//DataLength = 0;
	Response = RESPONSE_OK;
    diagF_DiagResponse(Response, 0/*DataLength*/, pMsgContext); 					
}
/*  ********************************************************************************
 * Function name:ApplDescReturnControl_2F500403_F_R_Heat_Seat_Request_BI(Service request header:$2F $50 $4 $0 )
 * $2F $5004 $00 - RETURN CONTROL TO ECU - FRONT RIGHT HEAT SEAT REQUEST
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescReturnControl_2F500403_F_R_Heat_Seat_Request_BI(DescMsgContext* pMsgContext)
{				
	/* Clear the lock flag. by clearing this flag returns control to normal execution of Front Right seat requests */
	diag_io_hs_lock_flags_c[FRONT_RIGHT].b.switch_rq_lock_b = 0; // FALSE;
	/* If this flag clears, mainF_CSWM_Status() function will look for ENG_RPM condition as a valid enable condition for outputs */
	diag_io_active_b = 0; // FALSE;
	/* Clear the Diag IO variable as we are returning control to ECU */
	diag_io_hs_rq_c[FRONT_RIGHT] = 0;
	//DataLength = 0;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, 0/*DataLength+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext); 					
}
/*  ********************************************************************************
 * Function name:ApplDescReturnControl_2F500503_R_L_Heat_Seat_Request_BI (Service request header:$2F $50 $5 $0 )
 * $2F $5005 $00 - RETURN CONTROL TO ECU - REAR LEFT HEAT SEAT REQUEST
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescReturnControl_2F500503_R_L_Heat_Seat_Request_BI(DescMsgContext* pMsgContext)
{			
	/* Clear the lock flag. by clearing this flag returns control to normal execution of Rear Left seat requests */
	diag_io_hs_lock_flags_c[REAR_LEFT].b.switch_rq_lock_b = 0; // FALSE;
	/* If this flag clears, mainF_CSWM_Status() function will look for ENG_RPM condition as a valid enable condition for outputs */
	diag_io_active_b = 0; // FALSE;
	/* Clear the Diag IO variable as we are returning control to ECU */
	diag_io_hs_rq_c[REAR_LEFT] = 0;
	//DataLength = 0;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, 0/*DataLength+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext); 			
}
/*  ********************************************************************************
 * Function name:ApplDescReturnControl_2F500603_R_R_Heat_Seat_Request_BI (Service request header:$2F $50 $6 $0 )
 * $2F $5006 $00 - RETURN CONTROL TO ECU - REAR RIGHT HEAT SEAT REQUEST
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescReturnControl_2F500603_R_R_Heat_Seat_Request_BI(DescMsgContext* pMsgContext)
{					
	/* Clear the lock flag. by clearing this flag returns control to normal execution of Rear Right seat requests */
	diag_io_hs_lock_flags_c[REAR_RIGHT].b.switch_rq_lock_b = 0; // FALSE;
	/* If this flag clears, mainF_CSWM_Status() function will look for ENG_RPM condition as a valid enable condition for outputs */
	diag_io_active_b = 0; // FALSE;
	/* Clear the Diag IO variable as we are returning control to ECU */
	diag_io_hs_rq_c[REAR_RIGHT] = 0;
	//DataLength = 0;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, 0/*DataLength+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext); 					
}
/*  ********************************************************************************
 * Function name:ApplDescReturnControl_2F500703_F_L_Vent_Seat_Request_BI (Service request header:$2F $50 $7 $0 )
 * $2F $5007 $00 - RETURN CONTROL TO ECU - FRONT LEFT VENT SEAT REQUEST
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescReturnControl_2F500703_F_L_Vent_Seat_Request_BI(DescMsgContext* pMsgContext)
{
	/* Clear the lock flag. by clearing this flag returns control to normal execution of Front Left Vent seat requests */
	diag_io_vs_lock_flags_c[LEFT_FRONT].b.switch_rq_lock_b = 0; // FALSE;
	diag_io_vs_rq_c[LEFT_FRONT] = 0;	//added by can 11.05.2008
	/* If this flag clears, mainF_CSWM_Status() function will look for ENG_RPM condition as a valid enable condition for outputs */
	diag_io_active_b = 0; // FALSE;
	//DataLength = 0;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, 0/*DataLength+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext); 			
}
/*  ********************************************************************************
 * Function name:ApplDescReturnControl_2F500803_F_R_Vent_Seat_Request_BI (Service request header:$2F $50 $8 $0 )
 * $2F $5008 $00 - RETURN CONTROL TO ECU - FRONT RIGHT VENT SEAT REQUEST
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescReturnControl_2F500803_F_R_Vent_Seat_Request_BI(DescMsgContext* pMsgContext)
{
    /* Clear the lock flag. by clearing this flag returns control to normal execution of Front right Vent seat requests */
	diag_io_vs_lock_flags_c[RIGHT_FRONT].b.switch_rq_lock_b = 0; // FALSE;
	diag_io_vs_rq_c[RIGHT_FRONT] = 0;	//added by can 11.05.2008
	/* If this flag clears, mainF_CSWM_Status() function will look for ENG_RPM condition as a valid enable condition for outputs */
	diag_io_active_b = 0; // FALSE;
	//DataLength = 0;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, 0/*DataLength+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext); 					
}
/*  ********************************************************************************
 * Function name:ApplDescReturnControl_2F500903_Heated_Steering_Wheel_Request_BI (Service request header:$2F $50 $9 $0 )
 * $2F $5009 $00 - RETURN CONTROL TO ECU - HEATED STEERING WHEEL REQUEST
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescReturnControl_2F500903_Heated_Steering_Wheel_Request_BI(DescMsgContext* pMsgContext)
{
	/* Clear the lock flag. by clearing this flag returns control to normal execution for steering wheel requests */
	diag_io_st_whl_rq_lock_b = 0; // FALSE;
	diag_io_st_whl_rq_c = 0; // added by Can 11.06.2008
	/* If this flag clears, mainF_CSWM_Status() function will look for ENG_RPM condition as a valid enable condition for outputs */
	diag_io_active_b = 0; // FALSE;
	//DataLength = 0;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, 0/*DataLength+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext); 					
}
/*  ********************************************************************************
 * Function name:ApplDescReturnControl_2F500A03_F_L_Heat_Seat_Status_BO (Service request header:$2F $50 $A $0 )
 * $2F $500A $00 - RETURN CONTROL TO ECU - FRONT LEFT HEAT SEAT STATUS - OUTPUT
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescReturnControl_2F500A03_F_L_Heat_Seat_Status_BO(DescMsgContext* pMsgContext)
{
	/* Clear the lock flag. by clearing this flag returns control to normal execution of Front Left seat LED status */
	diag_io_hs_lock_flags_c[FRONT_LEFT].b.led_status_lock_b = 0; // FALSE;			
	//DataLength = 0;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, /*DataLength+SIZE_OF_BLOCKNUMBER_K*/0, pMsgContext); 		          
}
/*  ********************************************************************************
 * Function name:ApplDescReturnControl_2F500B03_F_R_Heat_Seat_Status_BO (Service request header:$2F $50 $B $0 )
 * $2F $500B $00 - RETURN CONTROL TO ECU - FRONT RIGHT HEAT SEAT STATUS - OUTPUT
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescReturnControl_2F500B03_F_R_Heat_Seat_Status_BO(DescMsgContext* pMsgContext)
{
	/* Clear the lock flag. by clearing this flag returns control to normal execution of Front Right seat LED status */
	diag_io_hs_lock_flags_c[FRONT_RIGHT].b.led_status_lock_b = 0; // FALSE;
	//DataLength = 0;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, 0/*DataLength+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext); 
}
/*  ********************************************************************************
 * Function name:ApplDescReturnControl_2F500C03_R_L_Heat_Seat_Status_BO (Service request header:$2F $50 $C $0 )
 * $2F $500C $00 - RETURN CONTROL TO ECU - REAR LEFT HEAT SEAT STATUS - OUTPUT
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescReturnControl_2F500C03_R_L_Heat_Seat_Status_BO(DescMsgContext* pMsgContext)
{
	/* Clear the lock flag. by clearing this flag returns control to normal execution of Rear Left seat LED status */
	diag_io_hs_lock_flags_c[REAR_LEFT].b.led_status_lock_b = 0; // FALSE;
	//DataLength = 0;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, 0/*DataLength+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext); 
}
/*  ********************************************************************************
 * Function name:ApplDescReturnControl_2F500D03_R_R_Heat_Seat_Status_BO (Service request header:$2F $50 $D $0 )
 * $2F $500D $00 - RETURN CONTROL TO ECU - REAR RIGHT HEAT SEAT STATUS - OUTPUT
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescReturnControl_2F500D03_R_R_Heat_Seat_Status_BO(DescMsgContext* pMsgContext)
{
	/* Clear the lock flag. by clearing this flag returns control to normal execution of Rear Right seat LED status */
	diag_io_hs_lock_flags_c[REAR_RIGHT].b.led_status_lock_b = 0; // FALSE;
	//DataLength = 0;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, 0/*DataLength+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext); 
}
/*  ********************************************************************************
 * Function name:ApplDescReturnControl_2F500E03_F_L_Vent_Seat_Status_BO (Service request header:$2F $50 $E $0 )
 * $2F $500E $00 - RETURN CONTROL TO ECU - FRONT LEFT VENT SEAT STATUS - OUTPUT
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescReturnControl_2F500E03_F_L_Vent_Seat_Status_BO(DescMsgContext* pMsgContext)
{
	/* Clear the lock flag. by clearing this flag returns control to normal execution of Front Left Vent seat LED status */
	diag_io_vs_lock_flags_c[LEFT_FRONT].b.led_status_lock_b = 0; // FALSE;
	diag_io_vs_state_c[LEFT_FRONT] = 0;	/// added by Can 11.06.2008
	//DataLength = 0;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, 0/*DataLength+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext); 
}					
/*  ********************************************************************************
 * Function name:ApplDescReturnControl_2F500F03_F_R_Vent_Seat_Status_BO (Service request header:$2F $50 $F $0 )
 * $2F $500F $00 - RETURN CONTROL TO ECU - FRONT RIGHT VENT SEAT STATUS - OUTPUT
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescReturnControl_2F500F03_F_R_Vent_Seat_Status_BO(DescMsgContext* pMsgContext)
{
	/* Clear the lock flag. by clearing this flag returns control to normal execution of Front Right Vent seat LED status */
	diag_io_vs_lock_flags_c[RIGHT_FRONT].b.led_status_lock_b = 0; // FALSE;
	diag_io_vs_state_c[RIGHT_FRONT] = 0; // added by Can 11.06.2008
	//DataLength = 0;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, 0/*DataLength+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext); 
}
/*  ********************************************************************************
 * Function name:ApplDescReturnControl_2F501003_Heated_Steering_Wheel_Status_BO (Service request header:$2F $50 $10 $0 )
 * $2F $5010 $00 - RETURN CONTROL TO ECU - HEATED STEERING WHEEL STATUS - OUTPUT
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescReturnControl_2F501003_Heated_Steering_Wheel_Status_BO(DescMsgContext* pMsgContext)
{
	/* Clear the lock flag. by clearing this flag returns control to normal execution of Steering Wheel LED status */
	diag_io_st_whl_state_lock_b = 0; // FALSE;
	diag_io_st_whl_state_c = 0; //added by Can 11.06.2008
	//DataLength = 0;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, 0/*DataLength+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext); 
}
/*  ********************************************************************************
 * Function name:ApplDescReturnControl_2F501103_Load_shedding_status_BI (Service request header:$2F $50 $11 $0 )
 * $2F $5011 $00 - RETURN CONTROL TO ECU - LOAD SHEDDING STATUS 
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescReturnControl_2F501103_Load_shedding_status_BI(DescMsgContext* pMsgContext)
{
	/* Clear the lock flag. by clearing this flag returns control to normal execution of Load Shed status */
	diag_io_load_shed_lock_b = 0; // FALSE:
	//DataLength = 0;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, 0/*DataLength+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext); 
}
/*  ********************************************************************************
 * Function name:ApplDescReturnControl_2F501203_Ignition_Status (Service request header:$2F $50 $12 $0 )
 * $2F $5012 $00 - RETURN CONTROL TO ECU - IGNITION STATUS 
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescReturnControl_2F501203_Ignition_Status(DescMsgContext* pMsgContext)
{				
	/* Clear the lock flag. by clearing this flag returns control to normal execution of IGN status */
	diag_io_ign_lock_b = 0; // FALSE;
	/* If this flag clears, mainF_CSWM_Status() function will look for ENG_RPM condition as a valid enable condition for outputs */
	diag_io_active_b = 0; //FALSE;
	//DataLength = 0;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, 0/*DataLength+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext); 
}

/*  ********************************************************************************
 * Function name:ApplDescReturnControl_2F501303_Flash_Program_Data_1 (Service request header:$2F $50 $13 $0 )
 * $2F $5013 $00 - RETURN CONTROL TO ECU - FLASH PROGRAM DATA 1 (Turn OFF Special EOL Tests)
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescReturnControl_2F501303_Flash_Program_Data_1(DescMsgContext* pMsgContext)
{				
	/* Disable the EOL Special Commands */
	diag_eol_io_lock_b = FALSE;
	//DataLength = 0;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, 0/*DataLength+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext); 
}

/*  ********************************************************************************
 * Function name:ApplDescReturnControl_2F501403_Flash_Program_Data_2 (Service request header:$2F $50 $14 $0 )
 * $2F $5014 $00 - RETURN CONTROL TO ECU - FLASH PROGRAM DATA 2  (Turn OFF Control Relay A)
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescReturnControl_2F501403_Flash_Program_Data_2(DescMsgContext* pMsgContext)
{
	/* Clear No Fault detection bit */
	diag_eol_no_fault_detection_b = FALSE;

	/* Turn OFF the Relay A port */
	Dio_WriteChannel(REL_A_EN, STD_HIGH);

	/* Turn OFF Base Relay  */
	Pwm_SetDutyCycle(PWM_BASE_REL, 0x0000);

	//DataLength = 0;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, 0/*DataLength+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext); 
}

/*  ********************************************************************************
 * Function name:ApplDescReturnControl_2F501503_Flash_Program_Data_3 (Service request header:$2F $50 $15 $0 )
 * $2F $5015 $00 - RETURN CONTROL TO ECU - FLASH PROGRAM DATA 3 (Turn OFF Control Relay B)
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescReturnControl_2F501503_Flash_Program_Data_3(DescMsgContext* pMsgContext)
{				
	/* Clear No Fault detection bit */
	diag_eol_no_fault_detection_b = FALSE;

	/* Turn OFF the Relay B port */
	Dio_WriteChannel(REL_B_EN, STD_HIGH);

	/* Turn OFF Base Relay  */
	Pwm_SetDutyCycle(PWM_BASE_REL, 0x0000);

	//DataLength = 0;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, 0/*DataLength+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext); 
}

/*  ********************************************************************************
 * Function name:ApplDescReturnControl_2F501603_Flash_Program_Data_4 (Service request header:$2F $50 $16 $0 )
 * $2F $5016 $00 - RETURN CONTROL TO ECU - FLASH PROGRAM DATA 4 (Turn OFF Control Relay C LOW SIDE)
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescReturnControl_2F501603_Flash_Program_Data_4(DescMsgContext* pMsgContext)
{				
	/* Clear No Fault detection bit */
	diag_eol_no_fault_detection_b = FALSE;

	/* Turn OFF the Relay C port */
	Dio_WriteChannel(REL_STW_EN, STD_HIGH);

	/* Turn OFF Base Relay  */
	Pwm_SetDutyCycle(PWM_BASE_REL, 0x0000);
	//DataLength = 0;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, 0/*DataLength+SIZE_OF_BLOCKNUMBER_K*/, pMsgContext); 
}



