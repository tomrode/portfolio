#ifndef STWHEEL_HEATING_H
#define STWHEEL_HEATING_H

/********************************************************************************/
/*                        A U T H O R   I D E N T I T Y							*/
/********************************************************************************/
/* Initials      Name                   Company									*/
/* --------      --------------------   --------------------------------------- */ 
/* CK            Can Kulduk             Continental Automotive Systems			*/
/* HK            Harsha Yekollu         Continental Automotive Systems          */
/* CC            Christian Cortes       Continental Automotive Systems          */
/********************************************************************************/


/*********************************************************************************/
/*                               CHANGE HISTORY                                  */
/*********************************************************************************/
/* 10.23.2006 CK - created heater file   				     			 		 */
/* 03.24.2008 HY - copied file from CSWM MY09 project.                  	     */
/* 09.30.2008 CK - Enumarations updates for CSWM MY11                     	  	 */
/* 10.21.2008 CK - EFUSE defines are taken out. Not used for MY11		 		 */ 
/* 01.22.2009 CK - PC-Lint related updates.                                      */
/* 03.24.2009 CK - Commented unused define STW_STATUS_DELAY.                     */
/*                 Added a new define STW_TEMP_22C which is used in stWF_Init    */
/* 09.18.2009 CC - Added New Constant STW_REARSEATB_ISENSE_THRHLD FTR00139667    */
/* 03.07.2011 HY - MKS 66719: stW_Level_e enum and stW_cal_params structure updated */
/*                 to adjust the new Heat level.									*/
/* 03.08.2011 HY - MKS 66724: stW_cfg_c new variable added to have HSW Configuration */
/* 03.09.2011 HY - MKS 66832: STW_ISENSE_8A_LIMIT threshold added (8Amps)            */
/*********************************************************************************/


/******************************* @Enumaration(s) ********************************/
/* Steering Wheel Levels (used to update stwheel level) */
typedef enum{
    WL0=0,                                    // Wheel level 0
    WL1=1,                                    // Wheel level 1
    WL2=2,                                    // Wheel level 2
    WL3=3,                                    // Wheel level 3
    WL4=4,									  // Wheel level 4	New level
    STW_LV_LMT=5
}stW_Level_e;

/* Isense Type */
typedef enum{
    OFFSET_ISENSE=0,    // Isense reading when Steering wheel is turned off
    ON_ISENSE=1         // Isense reading when Steering wheel is turned on
}stW_Isense_type_e;

/* Wheel side selection (used for vsense reading) */
typedef enum{
    WHEEL_HIGH_SIDE=0,                        // Wheel high side 
    WHEEL_LOW_SIDE=1,                         // Wheel low side     
    WHEEL_SIDE_LMT=2                          // wheel side limit
}stW_side_e;

/* A3 StW Fault Type */
typedef enum{
    STW_FULL_OPEN=0,                          // Full open fault
    STW_PARTIAL_OPEN=1,                       // Partial open fault      
    STW_SHRT_TO_BATT=2,                       // Short to Battery fault
    STW_SHRT_TO_GND=3,                        // Short to Ground fault
    STW_IGN_WHEEL=4,                          // Ignition Wheel fault
    STW_TEMP_SENSOR_FLT=5                     // Temperature Sensor faults (01, 10, or 11)
}stW_Flt_Type_e;    

/* Diagnostic DTC Control */
typedef enum{
    STW_CLR_DTC=0,                            // Clear DTC by diagnostic command
    STW_SET_DTC=1                             // Set DTC by diagnostic command
}stW_DTC_Control_e;

/* StW Switch Status */
typedef enum{
    STW_NOT_PSD=0,                            // stWheel switch is not pressed
    STW_PSD=1,                                // stWheel switch is pressed
    STW_SNA=3					              // stWheel switch request signal is not pressed
}stW_Swtch_Stat_e;

/* StW Fault Detection */
typedef enum{
	STW_PARTIAL_OPEN_ISENSE=0,               // Minimum Isense threshold for partial open detection
    STW_FULL_OPEN_ISENSE=1,                  // Isense threshold for full open detection
    STW_OVR_CURRENT_ISENSE=2,                // Maximum Isense threshold for over current detection
    STW_SHRT_TO_GND_VSENSE=3,                // Vsense short to ground threshold
    STW_SHRT_TO_BATT_VSENSE=4,               // VSense short to battery threshold
    STW_SNSR_FLT_RANGE_ONTIME=5,             //Output ontime before detecting HSW Sensor Flt below range ambient < -10 C (in minutes) 
    FLT_DTC_LMT=6                            // number of fault detection elements   
}stW_Flt_Dtc_e;

/* StW Temperature Sensor Fault Status */
typedef enum{
    STW_TEMPERATURE_SENSOR_OK=0,              // Sensor O.K. Normal operation mode
    STW_TEMPERATURE_SENSOR_SHRT=1,            // Sensor Short circuit fault
    STW_TEMPERATURE_SENSOR_OPEN=2,            // Sensor Open circuit fault
    STW_TEMPERATURE_SENSOR_SNA=3              // Sensor signal not available fault
}stW_tmprtr_sensor_flt_status_e;

typedef enum{
	STW_LEATHER=0,
	STW_WOOD=1
}stW_Material_e;

typedef enum{
	STW_VEH_PF = 0,
	STW_VEH_MM = 2,
	STW_VEH_KL = 4,
	STW_VEH_UF = 6,
	STW_VEH_UK = 8
}stW_Veh_Offset_e;
/**********************************************************************************/


/******************************* @Constants/Defines *******************************/
/* TimeOut calculations: 60 sec / 320ms = 187.5 cyles in 320 ms time slice for 1 minute */
#define    STW_TM_SLICE_MLTPLR                187

/* 2 second delay after power on reset to read BCM_A8 (200 * 10ms) */
//#define    STW_STATUS_DELAY                   200		/* 03.24.2009 NOT USED. Commented by CK */

/* 1 sec = 50 counts in 20 ms time slice (stWF_Flt_Mntr) */
#define    STW_MTR_FLT_TM_MDLTN               50

/* 1 sec = 100 counts in 10ms time slice (stWF_Manager) */
#define    STW_LED_DSPLY_TM_MDLTN            100

/* For Open and Partial open detection */
#define    STW_ISENSE_DELAY                   15  // Isense reading analysis delay (15*20ms = 300ms)

/*For Open and Short to Battery detection */
#define    STW_OPEN_or_STB_DELAY			  7   // Open or Short to Battery Detection Delay after we stop PWMing High Side Driver  

#define    STW_PRL_FLT_MAX                    63  // stWheel preliminary fault status maximum

#define    STW_TEMP_SNA						0xFF  // Steering Wheel Temperature SNA

/* Steering wheel temperature @ 22.5 C. Used in initiliazation. */
#define    STW_TEMP_22C						125	  // Steering wheel temperature 22.5 C

/* StW Fault Masks */
#define STW_NO_FLT_MASK                   0       // No stWheel fault is present       
#define STW_PRLM_FULL_OPEN_MASK           1       // Preliminary full open fault
#define STW_PRLM_PARTIAL_OPEN_MASK        2       // Preliminary partial open fault 
#define STW_PRLM_GND_MASK                 4       // Preliminary short to ground fault
#define STW_PRLM_BATT_MASK                8       // Preliminary short to battery fault
#define STW_PRLM_IGN_WHEEL_MASK          16       // Preliminary ignition wheel fault (No power to wheel)
#define STW_PRELIM_TEMP_SENSOR_FLT       32       // Preliminary temperature sensor fault
#define STW_MTRD_FULL_OPEN_MASK          64       // Matured full open fault 
#define STW_MTRD_PARTIAL_OPEN_MASK      128       // Matured partial open fault  
#define STW_MTRD_GND_MASK               256       // Matured short to ground fault 
#define STW_MTRD_BATT_MASK              512       // Matured short to batery fault
#define STW_MTRD_IGN_WHEEL_MASK        1024       // Matured ignition wheel fault (No power to wheel)
#define STW_MTRD_TEMP_SENS_FLT_MASK    2048       // Matured temperature sensor fault

/* StW Clear Preliminary fault masks */
#define STW_CLR_PRLM_FULL_OPEN        65534       // Clear preliminary full open fault  
#define STW_CLR_PRLM_PARTIAL_OPEN     65533       // Clear preliminary partial open fault 
#define STW_CLR_PRLM_GND              65531       // Clear preliminary short to ground fault
#define STW_CLR_PRLM_BATT             65527       // Clear preliminary short to battery fault
#define STW_CLR_PRLM_IGN_WHEEL        65519       // Clear preliminary ignition wheel fault
#define STW_CLR_PRLM_TEMP_SENSOR      65503       // Clear preliminary temperature sensor fault
#define STW_REARSEATB_ISENSE_THRHLD     140       // combined Wheel and Rear Seats current Threshold 14Amps
#define STW_ISENSE_CNTR			       1500         // Counter to reach 1 min (In 40ms TS) //MKS 66832

/* sTWheel Ignition Status (*** IMPORTANT: Inverse logic is used for this signal ***) */
//#define STW_IGN_STAT_OFF                  1       // StWheel Ignition Status is Off
//#define STW_IGN_STAT_ON                   0       // StWheel Ignition Status is ON

/* sTWheel Ignition Status (Regular logic is used for this signal ***) */ 

#define STW_IGN_STAT_OFF                  0       // StWheel Ignition Status is Off
#define STW_IGN_STAT_ON                   1       // StWheel Ignition Status is ON

/* StWheel EFUSE state */
//#define STW_EFUSE_OFF                     0       // stWheel EFUSE is off
//#define STW_EFUSE_ON                      1       // stWheel EFUSE is on

#define STW_DATA_LENGHT                   3       // stW Write Data by LID maximum lenght

#define STW_SNSR_FLT_AMBIENT_LMT       90       // Stw temp sensor fault below range ambient limt +5C
/**********************************************************************************/


/******************************** @Global variables *******************************/
extern union char_bit stW_Flag1_c;
#define stW_state_b              stW_Flag1_c.b.b0    // StW state: On or Off
#define stW_not_vld_swtch_rq_b   stW_Flag1_c.b.b1    // Not valid switch request 
#define stW_sw_rq_b              stW_Flag1_c.b.b2    // software request that turns on/off the stW output
#define stW_swtch_rq_prps_b      stW_Flag1_c.b.b3    // switch request purpose (to turn on or off the output)
#define stW_diag_cntrl_act_b     stW_Flag1_c.b.b4    // diagnostics IO control active
#define stW_LED_off_b            stW_Flag1_c.b.b5    // turns off the LED as soon as stWheel fault is matured
#define stW_clr_ontime_b         stW_Flag1_c.b.b6    // reset the ontime to zero with new diagnostic request
#define stW_EFUSE_reset_b        stW_Flag1_c.b.b7    // bit used to reset the EFUSE (not used anymore)

/* Fault Detection parameters */
extern u_8Bit stW_Vsense_HS_c;              // Steering wheel voltage reading high side
extern u_8Bit stW_Vsense_LS_c;              // Steering wheel voltage reading low side
extern u_8Bit stW_Isense_c;                 // Steering wheel current reading low side
extern u_16Bit stW_Isense_offset_w;         // Steering wheel Isense offset
extern u_8Bit  stW_RearHeat_Isense_c;       // Steering Wheel Isense plus Rear Heaters Isense

extern u_16Bit stW_Flt_status_w;            // StW Fault Status

extern u_8Bit stW_temperature_c;            // Most recently read wheel temperature

extern u_8Bit stW_cfg_c;                    //Holds Heated Steering Wheel Configuration.
extern u_8Bit stW_veh_offset_c;				//Holds the vehicle line offset to get the right values from eep array.

extern u_8Bit stW_flt_calibs_c[FLT_DTC_LMT]; //Thresholds for detecting the output faults for wheel. Moved out from stW_cal_params
/**********************************************************************************/


/******************************** @Global structures ******************************/

/* Only read access please for the elements of this structure. */
/* Calibration parameters. (19 bytes) */
struct stW_cal{
   u_8Bit TimeOut_c[5];          // Timeout for the stWheel heater (5 bytes)
   u_8Bit Pwm_a[5];              // Pwm duty cycles (5 bytes)
//   u_8Bit Flt_Dtc[5];            // Fault Detection parameters (5 bytes)
   u_8Bit WL1Temp_Threshld;      // Maximum Temperature thresholds for WL1 (1 byte)
   u_8Bit WL2Temp_Threshld;      // Maximum Temperature thresholds for WL2 (1 byte)
   u_8Bit WL3Temp_Threshld;      // Maximum Temperature thresholds for WL3 (1 byte)
   u_8Bit WL4Temp_Threshld;      // Maximum Temperature thresholds for WL4 (1 byte)  
   u_8Bit MaxTemp_Threshld;      // Maximum Temperature thresholds for stWheel (1 byte)
   u_8Bit WL1TempChck_Time;      // Additional timers to automatically update wheel level (1 bytes) 
   u_8Bit WL2TempChck_Time;      // Additional timers to automatically update wheel level (1 bytes) 
   u_8Bit WL3TempChck_Time;      // Additional timers to automatically update wheel level (1 bytes) 
   u_8Bit WL4TempChck_Time;      // Additional timers to automatically update wheel level (1 bytes)
//   u_8Bit SnsrFlt_BelowRng_Ontime_lmt; // Output ontime before detecting HSW Sensor Flt below range ambient < -10 C (in minutes) 
};

struct stW_cal_params{
	struct stW_cal cals[10];   //5 vehicle lines each with two sets.
};

/* StWheel calibration parameters structure declaration */
extern struct stW_cal_params stW_prms;
/**********************************************************************************/


/****************************** @Function Prototypes ******************************/
/* For Main */
@far void stWF_Manager(void);
@far void stWF_Output_Control(void);
@far void stWF_Init(void);
@far void stWF_Timer(void);
@far void stWF_Flt_Mntr(void);
@far void stWF_AD_Rdng(void);
@far void stWF_DTC_Manager(void);

/* For Internal Faults */
@far void stWF_Clear(void);
@far void stWF_LED_Display(void);

/* For Diagnose */
@far u_8Bit stWF_Chck_Diag_Prms(u_8Bit stw_LID_serv, u_8Bit *stw_data_p);
@far u_8Bit stWF_Write_Diag_Data(u_8Bit stW_LID);
@far u_8Bit stWF_Get_Output_Stat(void);
@far void stWF_Diag_DTC_Control(stW_DTC_Control_e stW_DTC_cntrl, u_16Bit stW_DTC);
/* For Battery */
@far u_8Bit stWF_Actvtn_Stat(void);

/* called in 40ms Time slice to look for any current limits happen and set the corresponding flags */
/* to decide on logic. Controls 8A and 12A limits logic */
@far void stWF_CurrentLimit_Timer (void);
/**********************************************************************************/


#endif

