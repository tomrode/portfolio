/********************************************************************************/
/*               A U T H O R   I D E N T I T Y									*/
/********************************************************************************/
/* Initials      Name                   Company									*/
/* --------      --------------------   --------------------------------------- */ 
/* HY            Harsha Yekollu         Continental Automotive Systems          */
/* CK            Can Kulduk             Continental Automotive Systems			*/
/********************************************************************************/


/*********************************************************************************/
/*                   CHANGE HISTORY for RELAY MODULE                             */
/*********************************************************************************/
/* 03.24.2008/Harsha - Module created                                            */
/* 04.14.2008/Harsha - Enabled the Front Heated Seats Relay Logic                */
/* 04.14.2008/Harsha - No Power to Rear Seats detection modified for MY11        */
/* 09.05.2008 CK     - New Steering Wheel Fault detection (MY11) implementation  */
/* 10.20.2008 CK     - Enabled setting Relay Internal Faults for CSWM MY11		 */
/* 10.22.2008 CK     - Updates to Relay A and Relay B fault detection routines   */
/*                     in order to detect FET fault on Front and Rear outputs    */
/* 10.27.2008 CK     - Updated rlLF_RelayCPortOFF_FETOFF_Phase1_Detection        */
/* 					   to insure to turn off U12S when there is no stW request   */
/* 10.31.2008 HY     - rlLF_Control_BaseRelay modified.                          */
/*                     Set duty cycle function gets called only if the base pwm  */
/*                     values are different from current to previous             */
/* 01.08.2009 CK     - First took out unused defines.                            */
/*                   - Modifications with respect to MKS ID: 22112:              */
/*                     1. Added a counter variable to wait 80ms before turning   */
/*                     off the low side and base relay for steering wheel after  */
/*                     high side is turned off. 								 */
/*                     2. Added a variable to store the stw low side off time    */
/*                     limit which is read from D-Flash in rlF_InitRelay.        */
/*                     3. Modified rlLF_BaseRelay_For_Steering_OFF function.     */
/*                     4. Also added clearing off new counter varibale to        */
/*                     rlF_RelayManager and rlF_RelayDiagnostics functions.      */
/* 01.13.2009 CK     - 1. Modified rlLF_RelayCPortON_FETOFF_Phase2_Detection.    */
/*                     Check relay_C_internal_Fault_b instead of                 */
/*                     relay_C_stuck_LOW_b to avoid setting a Short to Ground    */
/*                     fault after only one Vsense reading below threshold.      */
/*                     2. Added clearing of relay_C_2000ms_fault_counter_c to    */
/*                     rlLF_RelayCPortON_FETOFF_Phase2_Detection and             */
/*                     rlLF_Control_RelayC_Port functions to insure 2 seconds    */
/*                     fault mature time for short to ground (relay C stuck low).*/
/*                     														     */
/* 01.23.2009 CK     - PC-Lint updates. No complaints from PC-Lint after fixes.  */
/* 04.06.2009 CK     - MKS Change Request I.D. 27978 Relay Internal fault(s) and */
/*                     No Power to Rear Seats DTCs are being stored in the same  */
/*                     ignition cycle if main_CSWM_Status goes to UGLY.          */
/*                     Updated rlF_RelayManager function (main_CSWM_Status = UGLY*/
/*                     path. We are not clearing bits that are related to Relay  */
/*                     internal faults and No Power to Rear seats fault.         */
/* 04.16.2010 HY     - MKS50111 . Moved 6 Individual Relay Threshol EEP Blocks   */
/*                     to Single Block. Updates in RlF_Init function             */ 
/*********************************************************************************/

#define RELAY_C

/********************************** @Include(s) **********************************/
/*Application Inculde files */
#include "canio.h"
#include "TYPEDEF.h"
#include "main.h"
#include "heating.h"
#include "relay.h"
#include "Venting.h"
//#include "FaultMemory.h"
//#include "FaultMemoryLib.h"
#include "BattV.h"
#include "StWheel_Heating.h"
#include "Internal_Faults.h"
#include "DiagMain.h"
#include "DiagIOCtrl.h"
#include "mw_eem.h"
/* Standard include files */
#include <stdio.h>
#include <string.h>
/* Autosar include Files */
#include "Mcu.h"
#include "Port.h"
#include "Dio.h"
#include "Gpt.h"
#include "Pwm.h"
#include "Adc.h"
/*********************************************************************************/


/* external data */
/* forward declarations                                                                         */
/* extern u_8Bit BattVF_Get_Crt_Uint(void);                                                     */
/* extern void IntFlt_F_Update(u_8Bit Byte_number,u_8Bit Bit_mask,Int_Flt_Op_Type_e intFlt_Op); */
/* extern void rlF_RelayDiagnostics(void);                                                      */
/* extern void rlLF_BaseRelay_For_FrontSeats_OFF(void);                                         */
/* extern void rlLF_BaseRelay_For_RearSeats_OFF(void);                                          */
/* extern void rlLF_BaseRelay_For_Steering_OFF(void);                                           */
/* extern void rlLF_BaseRelay_ON_With100PWM_For_RelayA(void);                                   */
/* extern void rlLF_BaseRelay_ON_With100PWM_For_RelayB(void);                                   */
/* extern void rlLF_BaseRelay_ON_With100PWM_For_RelayC(void);                                   */
/* extern void rlLF_Control_BaseRelay(void);                                                    */
/* extern void rlLF_Control_RelayA_Port(void);                                                  */
/* extern void rlLF_Control_RelayB_Port(void);                                                  */
/* extern void rlLF_Control_RelayC_Port(void);                                                  */
/* extern void rlLF_MatureTime_For_RelayA_InternalFault(void);                                  */
/* extern void rlLF_MatureTime_For_RelayB_InternalFault(void);                                  */
/* extern void rlLF_MatureTime_For_RelayC_InternalFault(void);                                  */
/* extern void rlLF_NoPower_To_RearSeats_Detection(void);                                       */
/* extern void rlLF_RelayAPortOFF_FETOFF_Phase1_Detection(void);                                */
/* extern void rlLF_RelayAPortON_FETOFF_Phase2_Detection(void);                                 */
/* extern void rlLF_RelayBPortOFF_FETOFF_Phase1_Detection(void);                                */
/* extern void rlLF_RelayBPortON_FETOFF_Phase2_Detection(void);                                 */
/* extern void rlLF_RelayCPortOFF_FETOFF_Phase1_Detection(void);                                */
/* extern void rlLF_RelayCPortON_FETOFF_Phase2_Detection(void);                                 */
/* extern void rlLF_Set_Relay_Err(void);                                                        */
/* extern void rlLF_TurnOFF_BaseRelay(void);                                                    */


/******************************* @Constants/Defines *******************************/
#define    RELAY_MON_LOW                      0     // Relay Monitor feedback LOW
#define    RELAY_MON_HIGH                     1     // Relay Monitor feedback HIGH

/* Normalized (with respect to Uint) Fault Detection parameters */
//#define		HS_V_SHRT_TO_GND                10 //15    // Vsense for shrt to ground 
//#define		HS_V_SHRT_TO_BATT               50    // Vsense for shrt to battery

//#define     STW_V_SHRT_TO_GND				15    // Steering Wheel Vsense limit for short to ground
//#define     STW_V_SHRT_TO_BATT			50 //60    // Steering Wheel Vsense limit for short to battery
//#define     STW_V_STUCK_OPEN              25	  // Steering Wheel Stuck Open fault threshold

#define    HSW_PHASE1_TEST_VOLT_OFF		1	// Test Voltage OFF, High Side and Low Side OFF
#define    HSW_PHASE1_TEST_VOLT_ON		2	// Test Voltage ON, High Side and Low Side OFF

//#define    RELAY_8V_CALIBRATION             80    //Cut off value for Relay

//#define    RELAY_2SEC_TIME                  200   //200 * 10ms

//#define    RELAY_PHASE1_DELAY_TIME            4//10  //10 * 10 = 100ms
//#define    RELAY_PHASE2_DELAY_TIME            4//10  //10 * 10 = 100ms

//#define    RELAY_100PERCENT_PWM             0x8000

/* IMPORTANT: At least 80ms delay is needed before starting STB detection after U12S is turned off */
/* 01.08.2008 This constnat is moved to emulated EEPROM */
//#define     STW_STB_DELAY		8           // 80ms Delay (after U12S is turned off) needed to start the STB detection on StWheel LowSide
/**********************************************************************************/


/*********************************** @Structures **********************************/
//typedef struct{
//    u_8Bit PWM_a[VS_LV_LMT];          // Pwm duty cycles (3 bytes)
//    u_8Bit Flt_Dtc[FLT_DTC_PRM_LMT];  // Fault detection thresholds (4 bytes)
//    u_16Bit start_up_tm;              // Motor start up time (2 byte)
//    u_8Bit vs_2sec_mature_time;       // Regular Fault detection time (except open. Removed #define added in eep)
//    u_8Bit vs_10sec_mature_time;      //For vents Open (Tracker Id#43)
//}Vs_cal_s;
//extern Vs_cal_s Vs_cal_prms;

//typedef struct{
//   u_8Bit TimeOut_c[4];          // Timeout for the stWheel heater (4 bytes)
//   u_8Bit Pwm_a[4];              // Pwm duty cycles (4 bytes)
//   u_8Bit Flt_Dtc[5];            // Fault Detection parameters (5 bytes)
//   u_8Bit WL2Temp_Threshld;      // Maximum Temperature thresholds for WL2 (1 byte)
//   u_8Bit WL3Temp_Threshld;      // Maximum Temperature thresholds for WL3 (1 byte)  
//   u_8Bit MaxTemp_Threshld;      // Maximum Temperature thresholds for stWheel (1 byte) 
//   u_8Bit WL2TempChck_Time;      // Additional timers to automatically update wheel level (1 bytes) 
//   u_8Bit WL3TempChck_Time;      // Additional timers to automatically update wheel level (1 bytes)
//   u_8Bit SnsrFlt_BelowRng_Ontime_lmt; // Output ontime before detecting HSW Sensor Flt below range ambient < -10 C (in minutes) 
//}stW_cal_params;
//extern stW_cal_params stW_prms;
/**********************************************************************************/


/*********************************************************************************/
/*                           Byte variables                                      */
/*********************************************************************************/
unsigned char relay_1sec_100pwm_counter_c;     //1sec Wait time to stay in 100% PWM

unsigned char relay_A_2000ms_fault_counter_c; //2Sec Mature time for Relay A fault Front Seats
unsigned char relay_B_2000ms_fault_counter_c; //2 Sec Mature time for Relay B fault Rear Seats.
unsigned char relay_C_2000ms_fault_counter_c; //2 Sec Mature time for Relay C fault Steering Wheel.
//unsigned char rear_seats_no_power_fault_counter_c; //2 Sec Mature time for Rear Seats incase No Power detected.

//unsigned char relay_UBATT_status_c;     //Local Battery Voltage status NOT USED
unsigned char relay_A_status_c;         //Relay A Monitor Port Status (FL&FR Heater and Vent)
unsigned char relay_B_status_c;         //Relay B Monitor Port Status (RL&RR Heater)
unsigned char relay_B_power_status_c;   //Relay B Power Status        (RL&RR Heater Power))
unsigned char relay_C_Test_Voltage_Pin_status_c; //Relay C Short to Ground test pin status

unsigned char relay_UBATT_current_value_c; //Holds the current Battery voltage value

unsigned char relay_base_pwm_value_c; //Holds the current cycle base relay PWM value
unsigned char relay_base_prev_pwm_value_c; //Holds the Previous cycle base relay PWM value
unsigned int relay_autosar_pwm_w; //holds the autosar PWM value

unsigned char relay_delay_time_phase1_c; //Delay time to do fault detection at Phase 1 during the start up
unsigned char relay_delay_time_phase2_c; //Delay time to do fault detection at Phase 1 during the start up

unsigned char relay_delay_time_for_steering_c; // Delay time ocunter for switching between HSW Phase 1 Short to GND and Short to BAT fault detection

unsigned char relay_fault_detection_type_c; //Fault detection type for HSW

unsigned char relay_A_stuck_HI_delay_c;  //Wait time to detect Realy A Struck HI
unsigned char relay_B_stuck_HI_delay_c;  //Wait time to detect Realy B Struck HI
unsigned char rear_seat_no_power_delay_c; //No Power to rear seat detection delay counter

//unsigned char relay_index_c;
//unsigned char relay_UBatt_cutoff_value_c; //Holds value from eeprom
//unsigned char relay_phase1_delay_c;//Holds value from eeprom
//unsigned char relay_phase2_delay_c;//Holds value from eeprom
//unsigned char relay_stw_stb_delay_c; //Holds value from eeprom
//unsigned char relay_stw_stk_open_c; //Holds value from eeprom

/* IMPORTANT: In order to avoid setting Steering wheel short to battery fault a delay time is being
 * introduced on 09/15/2008. After we turn off the U12S (which is being done in the background to avoid
 * self heating effect on heated seat NTC readings: 40ms ON and 120ms OFF) 80ms delay is necassary to 
 * reach an average U12S reading which is close to the actual value. With an efficient amount of delay
 * we shall be able to avoid setting short to battery fault when there is none.
 */
u_8Bit relay_C_STB_delay_c;		

/* Delay counter used to turn off the steering wheel low side and base relay */
/* after high side is turned off. */
u_8Bit relay_stw_LO_Side_Off_cntr_c;

/* Stores the stw low side off time limit read from D-Flash */
//u_8Bit relay_stw_LO_Side_Off_time_lmt_c;
/*********************************************************************************/


/*********************************************************************************/
/*                             LOCAL FUNCTION PROTOTYPES                         */
/*********************************************************************************/
/* Local Functions used in RelayManager (Total 11)*/

/* 1. This function checks for all the Front side switch requests */
/* If all the Front Seats are Not active, this function turns OFF the Base Relay Flag */
void rlLF_BaseRelay_For_FrontSeats_OFF(void);
/* 2. This function checks for all the Rear seat switch requests */
/* If all the Rear Seats are Not active, this function turns OFF the Base Relay Flag */
void rlLF_BaseRelay_For_RearSeats_OFF(void);
/* 3. This function checks for Steering Wheel switch requests */
/* If HSW is Not active, this function turns OFF the Base Relay Flag */
void rlLF_BaseRelay_For_Steering_OFF(void);

/* 4. This function checks for all the Front side switch requests */
/* this function turns ON the Base Relay Flag, when it sees the first Front side switch req */
void rlLF_BaseRelay_ON_With100PWM_For_RelayA(void);
/* 5. This function checks for all the Rear side switch requests */
/* this function turns ON the Base Relay Flag, when it sees the first Rear side switch req */
void rlLF_BaseRelay_ON_With100PWM_For_RelayB(void);
/* 6. This function checks for Steering Wheel switch requests */
/* this function turns ON the Base Relay Flag, when it sees the first time request */
void rlLF_BaseRelay_ON_With100PWM_For_RelayC(void);

/* 7. This function controls the Base Relay PWM */
void rlLF_Control_BaseRelay(void);

/* 8. This function turns ON/OFF the Front side connected Relay A */
void rlLF_Control_RelayA_Port(void);
/* 9. This function turns ON/OFF the Rear side connected Relay B */
void rlLF_Control_RelayB_Port(void);
/* 10. This function turns ON/OFF the HSW connected Relay C */
void rlLF_Control_RelayC_Port(void);

/* 11. This function turns OFF the Base Relay PWM */
void rlLF_TurnOFF_BaseRelay(void);

/* Local Functions used in RelayDiagnostics (Total 10) */
/* 1. This function diagnoses the No power to Rear Seats condition */
void rlLF_NoPower_To_RearSeats_Detection(void);

/* 2. Phase 1 detection for Front Seat connected Relay A */
/* PORT OFF and FET OFF condition*/
void rlLF_RelayAPortOFF_FETOFF_Phase1_Detection(void);
/* 3. Phase 1 detection for Rear Seat connected Relay B */
/* PORT OFF and FET OFF condition*/
void rlLF_RelayBPortOFF_FETOFF_Phase1_Detection(void);
/* 4. Phase 1 detection for HSW connected Relay C */
/* PORT OFF and FET OFF condition*/
void rlLF_RelayCPortOFF_FETOFF_Phase1_Detection(void);

/* 5. Phase 1 detection for Front Seat connected Relay A */
/* PORT ON and FET OFF condition */
void rlLF_RelayAPortON_FETOFF_Phase2_Detection(void);
/* 6. Phase 1 detection for Rear Seat connected Relay B */
/* PORT ON and FET OFF condition */
void rlLF_RelayBPortON_FETOFF_Phase2_Detection(void);
/* 7. Phase 1 detection for HSW connected Relay C */
/* PORT ON and FET OFF condition */
void rlLF_RelayCPortON_FETOFF_Phase2_Detection(void);

/* 8. Phase 1/2 Fault mature time for Front Seats Relay A */
void rlLF_MatureTime_For_RelayA_InternalFault(void);
/* 9. Phase 1/2 Fault mature time for Rear Seats Relay B */
/* Also mature time for No Power to Rear Seats condition */
void rlLF_MatureTime_For_RelayB_InternalFault(void);
/* 10. Phase 1/2 Fault mature time for Rear Seats Relay C */
void rlLF_MatureTime_For_RelayC_InternalFault(void);

void rlLF_Set_Relay_Err(unsigned char relay_bit_mask, Int_Flt_Op_Type_e Operation);
/*********************************************************************************/


/*********************************************************************************/
/*                            INITIALIZATION FUNCTION                            */
/*********************************************************************************/
/* This function gets called only once at the module start up.						*/
/* (Either if apply the power to Module or When modules wakesup or after any Reset).*/
/* Initialize the variables for Relay Module.                                       */
@far void rlF_InitRelay(void)
{
	relay_UBATT_current_value_c = 120;       //Initially taking the value as 12v
	
	EE_BlockRead(EE_AUTOSAR_100P_PWM, (u_8Bit*)&relay_autosar_pwm_w);

	/******************************START: MKS 50111**************************************************/	
//	EE_BlockRead(EE_RELAY_UBAT_CUTOFF_LIMIT, &relay_UBatt_cutoff_value_c);
//	EE_BlockRead(EE_RELAY_PHASE1_DETECT_DELAY, &relay_phase1_delay_c);
//	EE_BlockRead(EE_RELAY_PHASE2_DETECT_DELAY, &relay_phase2_delay_c);
//	EE_BlockRead(EE_RELAY_STW_STB_DELAY, &relay_stw_stb_delay_c);
//	EE_BlockRead(EE_RELAY_STW_STUCK_OPEN, &relay_stw_stk_open_c);
//	EE_BlockRead(EE_RELAY_STW_LO_SIDE_OFF_DELAY, &relay_stw_LO_Side_Off_time_lmt_c);
	EE_BlockRead(EE_RELAY_THRESHOLDS, (u_8Bit*)&relay_calibs_c);

//	relay_UBatt_cutoff_value_c = relay_calibs_c[RL_UBAT_CUTOFF];
//	relay_phase1_delay_c = relay_calibs_c[RL_PHASE1_DELAY];
//	relay_phase2_delay_c = relay_calibs_c[RL_PHASE2_DELAY];
//	relay_stw_stb_delay_c = relay_calibs_c[RL_HSW_STB_DELAY];
//	relay_stw_stk_open_c = relay_calibs_c[RL_HSW_STUCK_OPEN];
//	relay_stw_LO_Side_Off_time_lmt_c = relay_calibs_c[RL_HSW_LOSIDE_OFF_DELAY];
	/******************************END: MKS 50111**************************************************/
	
	
	/* Initially making the Autosar PWM to 100%. */
//	relay_autosar_pwm_w = /*mirror_eeprom.autosar_100percent_pwm;*/ 0x8000;

	/* Initially set the fault type to Short to BAT for HSW Phase 1 diagnsotics */
	relay_fault_detection_type_c = HSW_PHASE1_TEST_VOLT_OFF;
}

/*********************************************************************************/
/*                            rlF_RelayManager                                   */
/*********************************************************************************/
/* This functions gets executed every 10ms.                                      */
/* Main functionality is to look for the conditions to turn ON/OFF the relay.    */
/* Total of 11 functions gets called inside the Relay Manager function           */
/* Will go inside the logic if Main CSWM Status GOOD or BAD                 */
/* Good is Every internal condition is OK                                   */
/* BAD is one of the internal conditions are detected not yet matured       */
/* UGLY is one of the internal conditions are detected and fault is matured */
@far void rlF_RelayManager(void)
{
	if ( main_CSWM_Status_c != UGLY )
	{
		/* Base Relay OFF for Front Seats/Relay A */
		rlLF_BaseRelay_For_FrontSeats_OFF();

		/* Base Relay OFF for Rear Seats/Relay B */
		rlLF_BaseRelay_For_RearSeats_OFF();

		/* Base Relay OFF for Steering/Relay C */
		rlLF_BaseRelay_For_Steering_OFF();

		/* Base Relay Flag ON for Front Seats/Relay A with 100% PWM */
		rlLF_BaseRelay_ON_With100PWM_For_RelayA();

		/* Base Relay Flag ON for Rear Seats/Relay B with 100% PWM */
		rlLF_BaseRelay_ON_With100PWM_For_RelayB();

		/* Base Relay Flag ON for Steering Wheel/Relay C with 100% PWM */
		rlLF_BaseRelay_ON_With100PWM_For_RelayC();

		/* Control Base Relay with 100% PWM/Variable PWM */
		rlLF_Control_BaseRelay();

		/* Control Relay A Port ON/OFF */
		rlLF_Control_RelayA_Port();

		/* Control Relay B Port ON/OFF */
		rlLF_Control_RelayB_Port();

		/* Control Relay C Port ON/OFF */
		rlLF_Control_RelayC_Port();

		/* Turn OFF PWM Base Relay */
		rlLF_TurnOFF_BaseRelay();

		/* Do the Relay DIagnsotics */
		rlF_RelayDiagnostics();
		
	}
	else
	{
		/* Turn OFF Base Relay, Ports and clear all variables supporting Relay functionality */
//		relay_index_c = RELAY_A;

		/* Turn OFF the Relay A port */
		Dio_WriteChannel(REL_A_EN, STD_HIGH);
		relay_A_status_c = RELAY_MON_LOW;

		/* Turn OFF the Relay B port */
		Dio_WriteChannel(REL_B_EN, STD_HIGH);
		relay_B_status_c = RELAY_MON_LOW;

		/* Turn OFF the Relay C port */
		Dio_WriteChannel(REL_STW_EN, STD_HIGH);

		/* Send 0% PWM/Turn OFF the Base Relay */
		Pwm_SetDutyCycle(PWM_BASE_REL, 0x0000);
		
		/*************************************/
		/*** MKS Change Request I.D. 27978 ***/
		/*** Start of code Modification    ***/
		/*************************************/		
		/* Clear all relay_status_c bits */
		relay_status_c.cb = 0;
		
		if( (canio_RX_IGN_STATUS_c != IGN_RUN) && (canio_RX_IGN_STATUS_c != IGN_START) ){
			/* Clear all status Bits */			
			relay_status2_c.cb = 0;
			relay_fault_status_c.cb = 0;
			relay_fault_status2_c.cb = 0;

			/* Clear all control bits */
			relay_control_c.cb = 0;
			relay_control2_c.cb = 0;			
		}else{
			/* relay_status2_c: We can clear bit 0,1,2,3,4 right now */
			base_relay_active_for_steering_wheel_b = FALSE;	// bit0
			relay_C_enabled_b = FALSE;						// bit1
			relay_C_Ok_b = FALSE;							// bit2
			relay_C_phase1_ok_b = FALSE;					// bit3
			relay_C_phase2_ok_b = FALSE;					// bit4
			/* DO NOT CLEAR bit 5,6,7 right now */
			
			/* relay_fault_status_c: We can not clear any of these bits right now */
			
			/* relay_fault_status2_c: We can clear bit 1,3,4,5,6,7 right now */
			relay_C_stuck_HI_b = FALSE;				// bit1 Not Used
			relay_C_short_at_FET_b = FALSE;			// bit3 Not Used
			Base_relay_always_100P_PWM_b = FALSE;	// bit4
			Relay_A_Test_b = FALSE;					// bit5 Not Used
			Relay_B_Test_b = FALSE;					// bit6 Not Used
			Relay_C_Test_b = FALSE;					// bit7 Not Used
			/* DO NOT CLEAR bit 0 and 2 right now */
			
			/* relay_control_c: We can clear bit 0,1,2,3 right now */
			relay_A_phase1_ok_b = FALSE; 	// bit0
			relay_B_phase1_ok_b = FALSE;	// bit1
			relay_A_phase2_ok_b = FALSE;	// bit2
			relay_B_phase2_ok_b = FALSE;	// bit3
			/* DO NOT CLEAR bit 4,5,6,7 right now */
			
			/* relay_control2_c: We can clear bit 2,3,4 right now */
			relay_B_no_power_b = FALSE; 		// bit2 Not Used
			relay_C_no_power_b = FALSE; 		// bit3 Not Used
			relay_C_keep_U12S_ON_b = FALSE;		// bit4
			/* DO NOT CLEAR bit 0 and 1 right now (bits 5,6,7 are not defined) */
		}
		/*************************************/
		/*** MKS Change Request I.D. 27978 ***/
		/***    End of code Modification   ***/
		/*************************************/


		/* Initially set the fault type to Short to BAT for HSW Phase 1 diagnsotics */
		relay_fault_detection_type_c = HSW_PHASE1_TEST_VOLT_OFF;

		/* Clear the 1sec wait time counter to wait at 100% PWM for base relay */
		relay_1sec_100pwm_counter_c = 0;

		/* Clear the 2sec mature fault counter for Relay A and Relay B internal faults */
		relay_A_2000ms_fault_counter_c = 0;
		relay_B_2000ms_fault_counter_c = 0;
		relay_C_2000ms_fault_counter_c = 0;
//		rear_seats_no_power_fault_counter_c = 0;

		/* Clear the delay counter after changing the IGN status other than RUN/START. So for next ignition cycle again we wait for 100ms before going to relay diagnostics */
		relay_delay_time_phase1_c = 0;
		relay_delay_time_phase2_c = 0;
		rear_seat_no_power_delay_c = 0;

		/* Clear the delay counter for before detecting the Struck HI relay */
		relay_A_stuck_HI_delay_c = 0;
		relay_B_stuck_HI_delay_c = 0;

		relay_base_pwm_value_c = 0;
		relay_base_prev_pwm_value_c = 0;
//		relay_autosar_pwm_w = /*mirror_eeprom.autosar_100percent_pwm;*/ 0x8000;
		EE_BlockRead(EE_AUTOSAR_100P_PWM, (u_8Bit*)&relay_autosar_pwm_w);
		
		/* Clear Relay C STB delay counter */
		relay_C_STB_delay_c = 0;
		relay_delay_time_for_steering_c = 0;
		
		/* Make sure stw LO side off delay counter is cleared */
		relay_stw_LO_Side_Off_cntr_c = 0;
	}

}

/*********************************************************************************/
/*                        rlLF_BaseRelay_For_FrontSeats_OFF                      */
/*********************************************************************************/
/* Base Relay OFF for Front Seats/Relay A                                        */
/* Relay A is linked to Front Heaters and Front Vents.                           */
/* Base Relay OFF means clearing the "Base Relay Active Flag" for Front Seats.   */
/* Actual Turn OFF will be done at rlLF_TurnOFF_BaseRelay.                       */
/*                                                                               */
/* Check for All the Front Seat Requests.                                        */
/* If either of them is ON, Keep waiting for OFF request, Else                   */
/* If all the Front Seat Requests are OFF, check for Base Relay condition.       */
/* If Base Relay is ON, then turn OFF the Base Relay active Flag for Front Seats */
/* If Base Relay is OFF, do nothing.                                             */
/* Calls from                                                                    */
/* --------------                                                                */
/* rlF_RelayManager :                                                            */
/*    rlLF_BaseRelay_For_FrontSeats_OFF()                                        */
void rlLF_BaseRelay_For_FrontSeats_OFF (void)
{
	
	/* If all the Front Seats are Not active                                         */
	if ((hs_rem_seat_req_c[FRONT_LEFT] == SET_OFF_REQUEST)  &&
	    (hs_rem_seat_req_c[FRONT_RIGHT] == SET_OFF_REQUEST) &&
	    (vs_Flag1_c[VS_FL].b.sw_rq_b == FALSE)              &&
	    (vs_Flag1_c[VS_FR].b.sw_rq_b == FALSE) ) {

		/* If base Relay is turned ON, clearing the bit */
		if (base_relay_active_for_front_seats_b) {
			/* clear the bit */
			base_relay_active_for_front_seats_b = FALSE;

		}
		else {
			/* Front Seats are OFF. Do nothing. */

		}

	}
	else {
		/* Either all or one of the Front Seats are ON              */
		/* Keep waiting to see all the front seat outputs are OFF   */

	}

}

/*********************************************************************************/
/*                            rlLF_BaseRelay_For_RearSeats_OFF                   */
/*********************************************************************************/
/* Base Relay OFF for Rear Seats/Relay B                                        */
/* Relay B is linked to Rear Heaters.                                           */
/* Base Relay OFF means clearing the "Base Relay Active Flag" for Rear Seats.   */
/* Actual Turn OFF will be done at rlLF_TurnOFF_BaseRelay.                      */
/*                                                                              */
/* Check for All the Rear Seat Requests.                                        */
/* If either of them is ON, Keep waiting for OFF request, Else                  */
/* If all the Rear Seat Requests are OFF, check for Base Relay condition.       */
/* If Base Relay is ON, then turn OFF the Base Relay active Flag for Rear Seats */
/* If Base Relay is OFF, do nothing.                                            */
/*                                                                              */
/* Calls from                                                                   */
/* --------------                                                               */
/* rlF_RelayManager :                                                           */
/*    rlLF_BaseRelay_For_RearSeats_OFF()                                        */
/*                                                                              */
void rlLF_BaseRelay_For_RearSeats_OFF (void)
{
	
	/* If any of the Rear Seats are Not active */
	if ((hs_rem_seat_req_c[REAR_LEFT] == SET_OFF_REQUEST) &&
	    (hs_rem_seat_req_c[REAR_RIGHT] == SET_OFF_REQUEST) ) {
		
		/* If Base Relay is turned ON, clearing the base relay active for rear seats bit. */
		if (base_relay_active_for_rear_seats_b) {

			base_relay_active_for_rear_seats_b = FALSE;

		}
		else {
			/* We already turned off the port. Do nothing. */

		}

	}
	else {
		/* Either all or one of the Rear Seats are ON              */
		/* Keep waiting to see all the front seat outputs are OFF   */

	}

}

/*********************************************************************************/
/*                            rlLF_BaseRelay_For_Steering_OFF                    */
/*********************************************************************************/
/* Base Relay OFF for Steering/Relay C                                              */
/* Relay C is linked to Heated Steering Wheel.                                      */
/* Base Relay OFF means clearing the "Base Relay Active Flag" for Steering Wheel.   */
/* Actual Turn OFF will be done at rlLF_TurnOFF_BaseRelay                           */
/*                                                                                  */
/* Check for the Heated Steering Wheel Request.                                     */
/* If it is ON, Keep waiting for OFF request, Else                                  */
/* check for Base Relay condition.                                                  */
/* If Base Relay is ON, then turn OFF the Base Relay active Flag for Steering Wheel */
/* If Base Relay is OFF, do nothing.                                                */
/*                                                                                  */
/* Calls from                                                                       */
/* --------------                                                                   */
/* rlF_RelayManager :                                                               */
/*    rlLF_BaseRelay_For_Steering_OFF()                                             */
void rlLF_BaseRelay_For_Steering_OFF (void)
{
	
	/* If Steeting Wheel is Not active and There is no Remote Start event */
	if  (stW_sw_rq_b == FALSE)
	{
		/* If Base Relay is turned ON, clear the base relay active for HSW bit. */
		if (base_relay_active_for_steering_wheel_b)
		{
			/* Check if delay time is expired after high side is turned off */
			if(relay_stw_LO_Side_Off_cntr_c >= /*relay_stw_LO_Side_Off_time_lmt_c*/relay_calibs_c[RL_HSW_LOSIDE_OFF_DELAY]){
				/* Clear the flag in order to turn off the low side and base relay */
				base_relay_active_for_steering_wheel_b = FALSE;	
			}else{
				relay_stw_LO_Side_Off_cntr_c++;	//increment low side off delay counter
			}			
		}
		else
		{
			/* we already turned off the port. Make sure relay_stw_LO_Side_Off_cntr_c is cleared  */
			relay_stw_LO_Side_Off_cntr_c = 0;
		}
	}
	else
	{
		/* Steering Wheel Heating is ON, Keep waiting to see its OFF */
		/* Make sure stw LO side off delay counter is cleared */
		relay_stw_LO_Side_Off_cntr_c = 0;
	}
}

/*********************************************************************************/
/*                      rlLF_BaseRelay_ON_With100PWM_For_RelayA                  */
/*********************************************************************************/
/* Base Relay ON for Front Seats/Relay A with 100% PWM                           */
/* Base Relay ON means "Setting the Base Relay Active Flag" for Front Seats      */
/* and setting the 100% PWM flag.                                                */
/* Actual PWM logic will be done at rlLF_Control_BaseRelay                       */
/* Turn ON the Base Relay Active for Front Seats flag along with 100% PWM Flag only when */
/* 1. Base Relay is OFF for Front Seats                                          */
/* 2. There are NO internal Faults for Relay A                                   */
/* 3. There are NO Short to BATTERY conditions for Front Seats                   */
/* Else, Do nothing                                                              */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* rlF_RelayManager :                                                            */
/*    rlLF_BaseRelay_ON_With100PWM_For_RelayA()                                  */
void rlLF_BaseRelay_ON_With100PWM_For_RelayA(void)
{
	
	/* Turn On the base relay with 100% incase 										 */
	/* of very first switch request for the front seats								 */
	/* and no fault for Relay A and No short to BAT at Front heaters/vents           */
	if ((base_relay_active_for_front_seats_b == FALSE) &&
	    (relay_A_internal_Fault_b == FALSE) &&
	    (hs_status_flags_c[ FRONT_LEFT ].b.hs_final_short_bat_b == FALSE) &&
	    (hs_status_flags_c[ FRONT_RIGHT ].b.hs_final_short_bat_b == FALSE)&&
	    (vs_shrtTo_batt_mntr_c!= VS_SHRT_TO_BATT)) {

		/* Any switch request present either for Vents or Heats? */
		/* or In Remote Start either  Automatic or switch press */
		if ((hs_rem_seat_req_c[FRONT_LEFT] != SET_OFF_REQUEST)  ||
		    (hs_rem_seat_req_c[FRONT_RIGHT] != SET_OFF_REQUEST) ||
		    (vs_Flag1_c[VS_FL].b.sw_rq_b == TRUE)               ||
		    (vs_Flag1_c[VS_FR].b.sw_rq_b == TRUE)) {

			/* Enable base relay start flag for Fronts as we received */
			/* the first switch request to Front seats */
			base_relay_active_for_front_seats_b = TRUE;

			/* Enable 100% PWM flag */
			base_relay_100_pwm_b = TRUE;

			/* Clear the 1sec 100% PWM Time counter. As we need to start from beginning */
			relay_1sec_100pwm_counter_c = 0;

		}
		else {
			/* Keep waiting for the first switch request */

		}

	}
	else {
		/* Either Base Relay for Front seats active or There is an internal fault */
		/* or there is a short to BAT at one of the front outputs. */ 
		/* Do nothing even if we get a new switch request */

	}

}

/*********************************************************************************/
/*                      rlLF_BaseRelay_ON_With100PWM_For_RelayB                  */
/*********************************************************************************/
/* Base Relay ON for Rear Seats/Relay A with 100% PWM                            */
/* Base Relay ON means "Setting the Base Relay Active Flag" for Rear Seats       */
/* and setting the 100% PWM flag.                                                */
/* Actual PWM logic will be done at rlLF_Control_BaseRelay                       */
/* Turn ON the Base Relay Active for Rear Seats flag along with 100% PWM Flag only when */
/* 1. Base Relay is OFF for Front Seats                                          */
/* 2. There are NO internal Faults for Relay A                                   */
/* 3. There are NO Short to BATTERY conditions for Front Seats                   */
/* 4. There is Power to Rear Seats                                               */
/* Else, Do nothing                                                              */
/* Calls from                                                                    */
/* --------------                                                                */
/* rlF_RelayManager :                                                            */
/*    rlLF_BaseRelay_ON_With100PWM_For_RelayB()                                  */
void rlLF_BaseRelay_ON_With100PWM_For_RelayB(void)
{
	
	/* Turn On the base relay with 100* incase of very first switch request for      */
	/* the Rear seats and no fault for Relay B                                       */
	if ((base_relay_active_for_rear_seats_b == FALSE) &&
	    (relay_B_internal_Fault_b == FALSE) &&
	    (hs_status_flags_c[ REAR_LEFT ].b.hs_final_short_bat_b == FALSE) &&
	    (hs_status_flags_c[ REAR_RIGHT ].b.hs_final_short_bat_b == FALSE) &&
	   (Rear_Seats_no_power_final_b == FALSE)) {
		
		/* Any switch request present for rear seats? */
		if ((hs_rem_seat_req_c[REAR_LEFT] != SET_OFF_REQUEST) ||
		    (hs_rem_seat_req_c[REAR_RIGHT] != SET_OFF_REQUEST)) {

			/* Enable base relay start flag for Rears as we received the */
			/* first switch request to Rear seats */
			base_relay_active_for_rear_seats_b = TRUE;

			/* Enable 100% PWM Flag */
			base_relay_100_pwm_b = TRUE;

			/* Clear the 1sec 100% PWM Time counter. As we need to start from beginning */
			relay_1sec_100pwm_counter_c = 0;


		}
		else {
			/* Kee waiting for the switch request. */

		}

	}
	else {
		/* Either Base Relay for Rear seats active or There is an internal fault or */
		/* there is a short to bat at one of Rear seat outputs. */
		/* Do nothing even if we get a new switch request */

	}

}

/*********************************************************************************/
/*                      rlLF_BaseRelay_ON_With100PWM_For_RelayC                  */
/*********************************************************************************/
/* Base Relay Flag ON for Steering Wheel/Relay C with 100% PWM                                          */
/* Base Relay ON means "Setting the Base Relay Active Flag" for Steering ans setting the 100% PWM flag. */
/* Actual PWM logic will be done at rlLF_Control_BaseRelay                                              */
/*                                                                                                      */
/* Turn ON the Base Relay Active for Steering flag along with 100% PWM Flag only when                   */
/* 1. Base Relay is OFF for Steering Wheel                                                              */
/* 2. There are NO internal Faults for Relay C                                                          */
/* 3. There are NO Short to BATTERY conditions for Steering Wheel                                       */
/* Else, Do nothing                                                                                     */
/*                                                                                                      */
/* Calls from                                                                                           */
/* --------------                                                                                       */
/* rlF_RelayManager :                                                                                   */
/*    rlLF_BaseRelay_ON_With100PWM_For_RelayC()                                                         */
void rlLF_BaseRelay_ON_With100PWM_For_RelayC(void)
{
	
	/* Turn On the base relay with 100% incase of very first switch request for the Wheel */
	/* and no fault for Relay C */
	if ((base_relay_active_for_steering_wheel_b == FALSE) &&
	    (relay_C_internal_Fault_b == FALSE)&&
	    (stW_Flt_status_w <= STW_PRL_FLT_MAX))
	{
		/* Any switch request present for Steering Wheel? and Phase 1 detection is OK? */
		/* In Remote start active and Phase 1 ok                                       */
		if ((stW_sw_rq_b == TRUE) && (relay_C_phase1_ok_b == TRUE))
		{
			/* Enable base relay start flag for HSW as we received the first request to HSW*/
			base_relay_active_for_steering_wheel_b = TRUE;

			/* Enable 100% PWM Flag */
			base_relay_100_pwm_b = TRUE;

			/* Clear the 1sec 100% PWM Time counter. As we need to start from beginning */
			relay_1sec_100pwm_counter_c = 0;
		}
		else
		{
			/* Keep waiting for the switch request. */
		}
	}
	else
	{
		/* Either Base Relay for Steering Wheel active or */
		/* There is an internal fault or there is a short to bat at LOW Side */ 
		/* Do nothing even if we get a new switch request */
	}
}

/*********************************************************************************/
/*                           rlLF_Control_BaseRelay                              */
/*********************************************************************************/
/* Control Base Relay with 100% PWM/Variable PWM                                                                               */
/*                                                                                                                             */
/* We come here, If any of Outputs got requested to turn ON.                                                                   */
/* Base Relay with 100% PWM (For 1sec):                                                                                        */
/* Logic will turn ON when ever there is a new switch request corresponds to each Relay (Only the first time)                  */
/* Ex: If Front Seats are OFF and got new switch request for any of Front Seats, Base Relay will be turned ON to 100% for 1sec */
/* If we receive another switch request for the Front Seat outputs, It will not change to 100% as it is already served         */
/* At this moment If we receive switch request for Rear Seat/Steering Wheel (First Time), Base Relay PWM changes to 100%       */
/* Base Relay with Variable PWM (After 1sec)                                                                                   */
/* Equation is PWM = Cut off value/Vbat                                                                                        */
/*             PWM = 8v/current voltage                                                                                        */
/*                                                                                                                             */
/* Calls from                                                                                                                  */
/* --------------                                                                                                              */
/* rlF_RelayManager :                                                                                                          */
/*    rlLF_Control_BaseRelay()                                                                                                 */
void rlLF_Control_BaseRelay(void)
{
	
	/* Check to see is there any request bits set for Base relay to be ON */
	if ((base_relay_active_for_front_seats_b) ||
	    (base_relay_active_for_rear_seats_b) ||
	    (base_relay_active_for_steering_wheel_b))
	{
		/* If set.Check to see is there any request for 100% PWM */
		if (base_relay_100_pwm_b)
		{
			
			
			/* Autosar PWM should be 100% when base relay flag set. */
			//	relay_autosar_pwm_w = /*mirror_eeprom.autosar_100percent_pwm;*/ 0x8000;
			EE_BlockRead(EE_AUTOSAR_100P_PWM, (u_8Bit*)&relay_autosar_pwm_w);

			/* There is a request for 100% PWM. Turn ON Base Relay with 100% PWM */
			Pwm_SetDutyCycle(PWM_BASE_REL, relay_autosar_pwm_w);

			/* Wait 1sec time before going for variable PWM. */
			if (relay_1sec_100pwm_counter_c > 100)
			{
				/* One second passed. Clear the 100% PWM flag.. For EMC Tests commented. Needs to re activate. */
				/* For time being commented. After full development to be taken out */
				base_relay_100_pwm_b = FALSE;
				
				/* Clear the 1sec 100% PWM counter to process the next request */
				relay_1sec_100pwm_counter_c = 0;
			}
			else
			{
				/* Increment the 1sec counter */
				relay_1sec_100pwm_counter_c++;
			}
		}
		else
		{
			/*Variable PWM Logic */
			/* If there is no special case to stay Base relay with 100% PWM, do regular logic, else be out from this loop */
			if (!Base_relay_always_100P_PWM_b)
			{
				/* 1 sec timer expires. Now we need to send the variable PWM.  */
				/* Get the current Battery voltage reading into local variable */

				relay_UBATT_current_value_c = BattVF_Get_Crt_Uint();

				/* Check to see if the current battery voltage value is greater than the cut off value */

				if ( relay_UBATT_current_value_c >  /*relay_UBatt_cutoff_value_c*/relay_calibs_c[RL_UBAT_CUTOFF] )
				{
					/* Get the new PWM value based on the given equation */
					/* PWM = Vrelay/Vbat                                 */
					//relay_base_pwm_value_c = ( relay_UBatt_cutoff_value_c * 100) / relay_UBATT_current_value_c; // ORIGINAL
					// Updated by CK to avoid PC-Lint loss of precision message 01.23.2009
					relay_base_pwm_value_c = (unsigned char) ( ((unsigned int)(/*relay_UBatt_cutoff_value_c*/relay_calibs_c[RL_UBAT_CUTOFF]*100)) / relay_UBATT_current_value_c );

					/* Call setDutyCycle only incase the PWM changes */
					if (relay_base_pwm_value_c != relay_base_prev_pwm_value_c)
					{
						/* get the actual PWm value. */
						relay_autosar_pwm_w = mainF_Calc_PWM(relay_base_pwm_value_c);

						/* Send the Actual PWM value to do variable Pwming */
						Pwm_SetDutyCycle(PWM_BASE_REL, relay_autosar_pwm_w);
						
						/*Store the latest calculated base PWM value to the prev */
						/*So next time slice, this prev values will be checked with latest */
						relay_base_prev_pwm_value_c = relay_base_pwm_value_c;
						
					}
					
				}
				else
				{
					EE_BlockRead(EE_AUTOSAR_100P_PWM, (u_8Bit*)&relay_autosar_pwm_w);
					/* Send 100%PWM                                                                                     */
					/* There is Potential problem with UBATT, went down lesser than 8v Turn ON Base Relay with 100% PWM */
					Pwm_SetDutyCycle(PWM_BASE_REL, /*mirror_eeprom.autosar_100percent_pwm*/relay_autosar_pwm_w );
				}
			}
			else
			{
				//Stay at 100% PWM.
			}
		}
		/* Set the PWM active flag */
		relay_pwm_is_active_b = TRUE;
	}
	else
	{
		/* There is no request to turn on the base Relay. Keep waiting to get the Base relay tutn ON bit. */
	}
}

/*********************************************************************************/
/*                           rlLF_Control_RelayA_Port                            */
/*********************************************************************************/
/* If Base Relay is active for Front Seats, and Phase 1 detection is Ok then the logic tries to turn ON The Relay A Port. */
/* If Base Relay is not active and Relay A Port is ON, the logic turns OFF the Relay A Port.                              */
/*                                                                                                                        */
/* Calls from                                                                                                             */
/* --------------                                                                                                         */
/* rlF_RelayManager :                                                                                                     */
/*    rlLF_Control_RelayA_Port()                                                                                          */
/*                                                                                                                        */
void rlLF_Control_RelayA_Port(void)
{
	
	/* Check to see Base Relay request for Front seats present */
	if (base_relay_active_for_front_seats_b) {

		/* Is Phase 1 Diagnostics OK for Port A? */
		if (relay_A_phase1_ok_b) {

			/* TURN ON PORT A LOGIC.                                          */
			/* Check to see if Port A is already turned ON. If ON do nothing. */
			/* If OFF now the time to turn ON the Port A */
			if (relay_A_enabled_b) {
				/* Already turned ON the Port A. Do nothing */

			}
			else {
				/* Port A is not turned ON. Now this is the time to Turn ON the Port A */
				Dio_WriteChannel(REL_A_EN, STD_LOW);

				/* Set the Relay A enabled bit to HIGH */
				relay_A_enabled_b = TRUE;

				/* Wait 40ms before going for Relay C Phase 2 fault detection */
				relay_delay_time_phase2_c = 0;

			}

		}
		else {
			/* Phase 1 Diagnostics turned to be faulty. Do not turn ON the Port A. */

		}

	}
	else {
		/* Either Base Relay request just turned OFF or never set the flag.                 */
		/* Case 1:Just turned OFF.                                                          */
		/* Check to see, is Relay A port turned ON. If Port A turned ON, turn OFF the port. */
		if (relay_A_enabled_b) {
			/* Turn OFF the Relay A port */
			Dio_WriteChannel(REL_A_EN, STD_HIGH);

			/* Turn OFF the Relay A port Bit */
			relay_A_enabled_b = FALSE;

			/* Turn OFF the Relay A Ok bit. It again learns when there is a new switch request present */
			relay_A_Ok_b = FALSE;

			/* Turn OFF the Phase1  Ok bit set. Again it learns */
			relay_A_phase1_ok_b = FALSE;

			/* Turn OFF the Phase2  Ok bit set. Again it learns */
			relay_A_phase2_ok_b = FALSE;

			/* Clear the wait period time, SO that again it starts counting and then start doing the fault detection. */
			relay_delay_time_phase1_c = 0;
			relay_delay_time_phase2_c = 0;

		}
		else {
			/* Case 2: Never set the Flag or turned OFF the Port A. Do nothing. */

		}

	}

}

/*********************************************************************************/
/*                           rlLF_Control_RelayB_Port                            */
/*********************************************************************************/
/* If Base Relay is active for Rear Seats, and Phase 1 detection is Ok then the logic tries to turn ON The Relay B Port. */
/* If Base Relay is not active and Relay B Port is ON, the logic turns OFF the Relay B Port                              */
/*                                                                                                                       */
/* Calls from                                                                                                            */
/* --------------                                                                                                        */
/* rlF_RelayManager :                                                                                                    */
/*    rlLF_Control_RelayB_Port()                                                                                         */
void rlLF_Control_RelayB_Port(void)
{
	/* Check to see Base Relay request for Rear seats present */
	if (base_relay_active_for_rear_seats_b) {
		/* Is Phase 1 Diagnostics OK for Port B? */
		if (relay_B_phase1_ok_b) {
			/* TURN ON PORT B LOGIC.                                                                                    */
			/* Check to see if Port B is already turned ON. If ON do nothing. If OFF now the time to turn ON the Port B */
			if (relay_B_enabled_b) {
				/* Already turned ON the Port B. Do nothing */

			}
			else {
				/* Port B is not turned ON.Now this is the time to Turn ON the Port B */
				Dio_WriteChannel(REL_B_EN, STD_LOW);

				/* Set the Relay B enabled bit to HIGH */
				relay_B_enabled_b = TRUE;

				/* Wait 40ms before going for Relay C Phase 2 fault detection */
				relay_delay_time_phase2_c = 0;

			}

		}
		else {
			/* Phase 1 Diagnostics turned to be faulty. Do not turn ON the Port B. */

		}

	}
	else {
		/* Either Base Relay request just turned OFF or never set the flag.                 */
		/* Case 1:Just turned OFF.                                                          */
		/* Check to see, is Relay B port turned ON. If Port B turned ON, turn OFF the port. */

		if (relay_B_enabled_b) {
			/* Turn OFF the Relay B port */
			Dio_WriteChannel(REL_B_EN, STD_HIGH);

			/* Turn OFF the Relay B port Bit */
			relay_B_enabled_b = FALSE;

			/* Turn OFF the Relay B Ok bit. It again learns when there is a new switch request present */
			relay_B_Ok_b = FALSE;

			/* Turn OFF the Phase1  Ok bit set. Again it learns */
			relay_B_phase1_ok_b = FALSE;

			/* Turn OFF the Phase2  Ok bit set. Again it learns */
			relay_B_phase2_ok_b = FALSE;

			/* Clear the wait period time, SO that again it starts counting and then start doing the fault detection. */
			relay_delay_time_phase1_c = 0;
			relay_delay_time_phase2_c = 0;

		}
		else {
			/* Case 2: Never set the Flag or turned OFF the Port B. Do nothing. */

		}

	}

}

/*********************************************************************************/
/*                           rlLF_Control_RelayC_Port                            */
/*********************************************************************************/
/* If Base Relay is active for Steering Wheel, and Phase 1 detection is Ok then the logic tries to turn ON The Relay C Port. */
/* If Base Relay is not active and Relay C Port is ON, the logic turns OFF the Relay C Port                                  */
/*                                                                                                                           */
/* Calls from                                                                                                                */
/* --------------                                                                                                            */
/* rlF_RelayManager :                                                                                                        */
/*    rlLF_Control_RelayC_Port()                                                                                             */
void rlLF_Control_RelayC_Port(void)
{
	/* Check to see Base Relay request for Steering Wheel present */
	if (base_relay_active_for_steering_wheel_b)
	{
		/* Check to see, did we pass the Phase 1 diagnsotics for Steering Wheel */
		if (relay_C_phase1_ok_b)
		{
			/* TURN ON PORT C LOGIC.                                                                                    */
			/* Check to see if Port C is already turned ON. If ON do nothing. If OFF now the time to turn ON the Port C */
			if (relay_C_enabled_b)
			{
				/* Already turned ON the Port C. Do nothing */
			}
			else
			{
				/* Port C is not turned ON. Now this is the time to Turn ON the Port C (Inverse Logic) */
				Dio_WriteChannel(REL_STW_EN, STD_LOW);

				/* Set the Relay C enabled bit to HIGH */
				relay_C_enabled_b = TRUE;

				/* Set Fault detection type to Short to BAT */
				relay_fault_detection_type_c = HSW_PHASE1_TEST_VOLT_OFF;

				/* Wait 40ms before going for Relay C Phase 2 fault detection */
				relay_delay_time_phase2_c = 0;
			}
		}
		else
		{
			/* Phase 1 Diagnostics turned to be faulty. Do not turn ON the Port C. */
		}
	}
	else
	{
		/* Either Base Relay request just turned OFF or never set the flag.                 */
		/* Case 1:Just turned OFF.                                                          */
		/* Check to see, is Relay C port turned ON. If Port C turned ON, turn OFF the port. */
		if (relay_C_enabled_b)
		{
			/* Turn OFF the Relay C port */
			Dio_WriteChannel(REL_STW_EN, STD_HIGH);

			/* Turn OFF the Relay C port Bit */
			relay_C_enabled_b = FALSE;

			/* Turn OFF the Relay C Ok bit. It again learns when there is a new switch request present */
			relay_C_Ok_b = FALSE;

			/* Turn OFF the Phase1  Ok bit set. Again it learns */
			relay_C_phase1_ok_b = FALSE;

			/* Turn OFF the Phase2  Ok bit set. Again it learns */
			relay_C_phase2_ok_b = FALSE;

			/* Clear the wait period time, SO that again it starts counting and then start doing the fault detection. */
			relay_delay_time_phase1_c = 0;
			relay_delay_time_phase2_c = 0;
			
			/* Added on 01.03.2009 Clear fault mature counter. We turned off the low side */
			relay_C_2000ms_fault_counter_c = 0;
		}
		else
		{
			/* Case 2: Never set the Flag or turned OFF the Port C. Do nothing. */
		}
	}
}

/*********************************************************************************/
/*                           rlLF_TurnOFF_BaseRelay                              */
/*********************************************************************************/
/* Logic Turns OFF the base Relay only when If all the Base Relay turn ON requests are set to FALSE. */
/*                                                                                                   */
/* Calls from                                                                                        */
/* --------------                                                                                    */
/* rlF_RelayManager :                                                                                */
/*    rlLF_TurnOFF_BaseRelay()                                                                       */
void rlLF_TurnOFF_BaseRelay(void)
{
	/* Check to see, if Front, Rear and Steering Wheel base relay requests are not set */
	if (!base_relay_active_for_front_seats_b &&
	    !base_relay_active_for_rear_seats_b &&
	    !base_relay_active_for_steering_wheel_b)
	{
		/* Check to see the Relay is PWming */
		if (relay_pwm_is_active_b)
		{
			/* Autosar PWM should be 0% . */
			relay_autosar_pwm_w = 0x0000;

			/* Send 0% PWM . This is the time to Turn OFF the Base Relay */
			Pwm_SetDutyCycle(PWM_BASE_REL, relay_autosar_pwm_w);

			/* clear PWM active flag. This is the time to turn OFF the Base Relay */
			relay_pwm_is_active_b = 0;

			/* As we are turning OFF the base relay here, clear the Phase 1 and phase 2 fault detection timers here. SO it waits atleast 40ms to again start detecting the faults. */
			/* Clear the 1sec wait time counter to wait at 100% PWM for base relay */
			relay_1sec_100pwm_counter_c = 0;

			/* Clear the 2sec mature fault counter for Relay A and Relay B internal faults */
			relay_A_2000ms_fault_counter_c = 0;
			relay_B_2000ms_fault_counter_c = 0;
			relay_C_2000ms_fault_counter_c = 0;
//			rear_seats_no_power_fault_counter_c = 0;

			/* Clear the delay counter after changing the IGN status other than RUN/START. So for next ignition cycle again we wait for 100ms before going to relay diagnostics */
			relay_delay_time_phase1_c = 0;
			relay_delay_time_phase2_c = 0;
			rear_seat_no_power_delay_c = 0;

			/* Clear the delay counter for before detecting the Struck HI relay */
			relay_A_stuck_HI_delay_c = 0;
			relay_B_stuck_HI_delay_c = 0;

			relay_base_pwm_value_c = 0;
			relay_base_prev_pwm_value_c = 0;
		}
		else
		{
			/* Already turned OFF the Base Relay. Do nothing */
		}
	}
	else
	{
		/* Either one of the request is ON, do not turn OFF the base Relay */
	}
}

/*********************************************************************************/
/*                           rlF_RelayDiagnostics                                */
/*********************************************************************************/
/* If the safety relay is having internal faults, Relay module will not allow the heated seats or vents to turn ON.                                                                                                                      */
/* Similarly If there is no power to Rear Seats, Rear seats will not get turned ON                                                                                                                                                       */
/* If we detect STG/STB for Steering Wheel, WHeel will not get turned ON.                                                                                                                                                                */
/*                                                                                                                                                                                                                                       */
/* The faults we detect here are:                                                                                                                                                                                                        */
/* A. Front Seats                                                                                                                                                                                                                        */
/* A1. Relay A Struck HI...Relay A OFF and FET OFF state                                                                                                                                                                                 */
/* A2. Relay A Struck Lo...Relay A ON and FET OFF state                                                                                                                                                                                  */
/* B: Rear Seats                                                                                                                                                                                                                         */
/* B1: No Power to Rear Seats                                                                                                                                                                                                            */
/* B2: Relay B Struck HI...Relay B OFF and FET OFF state                                                                                                                                                                                 */
/* B3: Relay B Struck Lo...Relay B ON and FET OFF state                                                                                                                                                                                  */
/* C: Steering Wheel HI side                                                                                                                                                                                                             */
/* C1: Short to GND for Steering Wheel HI side... Relay C OFF and FET OFF state                                                                                                                                                          */
/* C2: Short to BAT for Steering WHeel HI side... Relay C OFF and FET OFF state                                                                                                                                                          */
/* C3: Relay C Struck LO...Relay C ON and FET OFF state                                                                                                                                                                                  */
/*                                                                                                                                                                                                                                       */
/* If the module receives all the initial conditions and all the initial conditions are OK(IGN status is in RUN/START and Battery voltage is in Operational range and No internal faults), Relay module starts the diagnostics.          */
/*                                                                                                                                                                                                                                       */
/* After POR, If initial conditions are OK, logic waits 40ms before going for fault detection, to make sure we are getting normailized feedback.                                                                                         */
/*                                                                                                                                                                                                                                       */
/* At any point of time logic detects No Power to Rear seats, as long as it meets all the initial conditions are OK.                                                                                                                     */
/*                                                                                                                                                                                                                                       */
/* In two Phases we test the Relay functionality.                                                                                                                                                                                        */
/*                                                                                                                                                                                                                                       */
/* Phase 1(Before Switch Request):If every thing is GOOD, Relay functionality then goes to the next level.That is wait for switch request, turn ON the Base Relay and turn ON the respective REL A or REL B based on the switch request. */
/* Else, If we detect faults here module will not turn ON the heats or vetns for the current IGN cycle.                                                                                                                                  */
/* Here basically we detect STRUCK HI faults for Relay A/B and STG/STB for Steering Wheel.                                                                                                                                               */
/*                                                                                                                                                                                                                                       */
/* Phase 2(After Switch request):When REL A or REL B turned ON, again module performs diagnostics.                                                                                                                                       */
/* IF module fails here module will not turn ON the heats or vetns for the current IGN cycle.                                                                                                                                            */
/* Else if module passes the diagnsotics, goes to the next level. That is Relay module passes information to the heat/vent module to turn on the heat or vent.                                                                           */
/* Here we detect STRUCK LO faults for Relay A/B/C.                                                                                                                                                                                      */
/*                                                                                                                                                                                                                                       */
/* STRUCK HI Detection (RELAY is OFF and FET is OFF)                                                                                                                                                                                     */
/* If we observe RELAY MONITOR Status as HIGH and all connected outputs Vsense to the particular Relay are Mimimal                                                                                                                       */
/*                                                                                                                                                                                                                                       */
/* STRUCK LO Detection (Relay is ON and FET is OFF)                                                                                                                                                                                      */
/* If we observe RELAY MONITOR status as LOW and any connected outputs Vsense to the particular Relay are minimal                                                                                                                        */
/*                                                                                                                                                                                                                                       */
/* SHORT TO BAT Detection for Steering Wheel (Relay is OFF and FET OFF)                                                                                                                                                                  */
/* When nothing is operating and STG detection Pin is disabled, logic should observe Zero voltage feeback for Vsense of Steering wheel HI side.                                                                                          */
/* If Logic gets Vsense value > 5v, blows SHB for Heated steering Wheel.                                                                                                                                                                 */
/* If values are in Range, Logic enables the STG Pin detection and goes to STG fault detection after 40ms.                                                                                                                               */
/*                                                                                                                                                                                                                                       */
/* SHORT TO GND Detection for Steering Wheel (Relay is OFF and FET OFF)                                                                                                                                                                  */
/* In previous step enabled the STG PIN, by enabling the STG Pin, we should get minimum of 7v.                                                                                                                                           */
/* IF we observe less than 7v, Logic blows Short to GND for Steering WHeel                                                                                                                                                               */
/* If value is in Range,, logic disables the STG PIN, enables the STB detection. Logic waits 40ms and then proceed to detect the STB.                                                                                                    */
/*                                                                                                                                                                                                                                       */
/* The above two processes will be contineously monitored untill it gets switch request for Heated Steering Wheel.                                                                                                                       */
/*                                                                                                                                                                                                                                       */
/* Calls from                                                                                                                                                                                                                            */
/* --------------                                                                                                                                                                                                                        */
/* rlF_RelayManager :                                                                                                                                                                                                                    */
/*    rlF_RelayDiagnostics()                                                                                                                                                                                                             */
/*                                                                                                                                                                                                                                       */
/* Calls to                                                                                                                                                                                                                              */
/* --------                                                                                                                                                                                                                              */
/* rlLF_NoPower_To_RearSeats_Detection()                                                                                                                                                                                                 */
/* rlLF_RelayAPortOFF_FETOFF_Phase1_Detection()                                                                                                                                                                                          */
/* rlLF_RelayAPortON_FETOFF_Phase2_Detection()                                                                                                                                                                                           */
/* rlLF_RelayBPortOFF_FETOFF_Phase1_Detection()                                                                                                                                                                                          */
/* rlLF_RelayBPortON_FETOFF_Phase2_Detection()                                                                                                                                                                                           */
/* rlLF_RelayCPortON_FETOFF_Phase2_Detection()                                                                                                                                                                                           */
/* rlLF_RelayCPortOFF_FETOFF_Phase1_Detection()                                                                                                                                                                                          */
/* rlLF_MatureTime_For_RelayC_InternalFault()                                                                                                                                                                                            */
/* rlLF_MatureTime_For_RelayB_InternalFault()                                                                                                                                                                                            */
/* rlLF_MatureTime_For_RelayA_InternalFault()                                                                                                                                                                                            */
@far void rlF_RelayDiagnostics(void)
{
	/* Store Relay A status - this will be used in fault detection */
	relay_A_status_c = (unsigned char) Dio_ReadChannel(REL_A_MON);

	/* Store Relay B status - this will be used in fault detection */
	relay_B_status_c = (unsigned char) Dio_ReadChannel(REL_B_MON);
	
	/* Get the Test Voltage status */
	relay_C_Test_Voltage_Pin_status_c = (unsigned char) Dio_ReadChannel(U12T_EN);
	
	/* If main CSWM status is GOOD and Locally measured voltage is in operational range */
	if ( main_CSWM_Status_c == GOOD )
	{
		/* Do the Fault Detection only when there are no EOL tests */
		if (!diag_eol_no_fault_detection_b)
		{
			/* B1: No Power to Rear Seats Preliminary Fault Detection */
			rlLF_NoPower_To_RearSeats_Detection();

			/* Phase 1 detection.                                           */
			/* Wait for 40ms before going to the fault detection at Phase 1 */
			if (relay_delay_time_phase1_c >=  /*relay_phase1_delay_c*/relay_calibs_c[RL_PHASE1_DELAY])
			{
				/* A1: Relay A OFF and FET OFF fault detection */
				rlLF_RelayAPortOFF_FETOFF_Phase1_Detection();

				/* B2: Relay B OFF and FET OFF fault detection */
				rlLF_RelayBPortOFF_FETOFF_Phase1_Detection();

				/* C1, C2: Relay C OFF and FET OFF fault detection */
				rlLF_RelayCPortOFF_FETOFF_Phase1_Detection();
			}
			else
			{
				/* Keep incrementing as not reached 40ms */
				relay_delay_time_phase1_c++;
			}
			
			/* Phase 2 Detection                                             */
			/* Wait for 40ms before detecting the fault detection at Phase 2 */
			if (relay_delay_time_phase2_c >=  /*relay_phase2_delay_c*/relay_calibs_c[RL_PHASE2_DELAY])
			{
				/* A2: Relay A turned ON and FET is OFF fault detection */
				rlLF_RelayAPortON_FETOFF_Phase2_Detection();

				/* B3: Relay B turned ON and FET is OFF fault detection */
				rlLF_RelayBPortON_FETOFF_Phase2_Detection();

				/* C3: Relay C turned ON and FET is OFF fault detection */
				rlLF_RelayCPortON_FETOFF_Phase2_Detection();
			}
			else
			{
				/* Keep incrementing as not reached 40ms */
				relay_delay_time_phase2_c++;
			}

			/* 2Sec Mature Time for Relay A Internal Fault */
			rlLF_MatureTime_For_RelayA_InternalFault();

			/* 2Sec Mature Time for Relay B Internal Fault and No Power to Rear Seats */
			rlLF_MatureTime_For_RelayB_InternalFault();

			/* 2Sec Mature Time for Relay C Internal Fault */
			rlLF_MatureTime_For_RelayC_InternalFault();
		}
		else
		{
			/* Special EOL tests are running. Do not go for Fault detection */
		}
	}
	else
	{
		/* Clear the delay counter after changing the Main CSWM status other than GOOD. So for next Good cycle again we wait for 40ms before going to relay diagnostics */
		relay_delay_time_phase1_c = 0;
		relay_delay_time_phase2_c = 0;
		rear_seat_no_power_delay_c = 0;

		/* Clear the delay counter for before detecting the Struck HI relay */
		relay_A_stuck_HI_delay_c = 0;
		relay_B_stuck_HI_delay_c = 0;

		/* Clear the 2sec mature fault counter for Relay A, Relay B and Relay C internal faults */
		relay_A_2000ms_fault_counter_c = 0;
		relay_B_2000ms_fault_counter_c = 0;
		relay_C_2000ms_fault_counter_c = 0;

		Rear_Seats_no_power_prelim_b = FALSE;
		/* Clear this bit to keep U12S on during STG tests. */
		relay_C_keep_U12S_ON_b = FALSE;
		/* Clear Relay C STB delay counter */
		relay_C_STB_delay_c = 0;		
		/* Initially set the fault type to Short to BAT for HSW Phase 1 diagnsotics */
		relay_fault_detection_type_c = HSW_PHASE1_TEST_VOLT_OFF;
		relay_delay_time_for_steering_c = 0;
		relay_stw_LO_Side_Off_cntr_c = 0;
	}
}

/*********************************************************************************/
/*                     rlLF_NoPower_To_RearSeats_Detection                       */
/*********************************************************************************/
/* This logic works all the time for CSWM with Populated version of Vented Seats.                                                    */
/* If there is no fault set, this logic tries to detect No power to the Rear Seats.                                                  */
/*                                                                                                                                   */
/* After Powerup the module, logic waits 40ms to settle and then contineously monitors the stauts for IGN REAR.                      */
/* If the Status is LOW, Logic sets the Preliminary fault.                                                                           */
/*                                                                                                                                   */
/* rlLF_MatureTime_For_RelayB_InternalFault() will set the final fault for No power to rear seats and hsF_HeatDTC will blow the DTC. */
/*                                                                                                                                   */
/* Calls from                                                                                                                        */
/* --------------                                                                                                                    */
/* rlF_RelayDiagnostics :                                                                                                            */
/*    rlLF_NoPower_To_RearSeats_Detection()                                                                                          */
void rlLF_NoPower_To_RearSeats_Detection(void)
{
	/*Do Fault detection only incase HW equipped with Rear Seats */
	if ((CSWM_config_c & CSWM_4HS_VARIANT_MASK_K) == CSWM_4HS_VARIANT_MASK_K)
	{
		/* Check to see did we blow already no power to Rear Seat DTC. If yes, no need to do fault detection for this IGN cycle, Else do the fault detection always */
		if (!Rear_Seats_no_power_final_b)
		{
			/* Wait for 40ms after POR, before detecting the No Power to Rear seats */
			if (rear_seat_no_power_delay_c >= /*relay_phase1_delay_c*/relay_calibs_c[RL_PHASE1_DELAY])
			{
				/* Read the Status */
				relay_B_power_status_c = (unsigned char) Dio_ReadChannel(IGN_REAR_MON);

				/* If status is HIGH, we are good, else set the preliminary fault. */
				if (relay_B_power_status_c == RELAY_MON_HIGH)
				{
					/* Power is detected. Clear the preliminary fault */
					Rear_Seats_no_power_prelim_b = FALSE;
				}
				else
				{
					/* Power is not detected. set the preliminary fault and proceed to 2sec monitor time */
					Rear_Seats_no_power_prelim_b = TRUE;
				}
			}
			else
			{
				/* Increment the counter to reach 40ms. */
				rear_seat_no_power_delay_c++;
			}
		}
		/*Already No power to Rear seats fault logged. Do nothing */
	}
	/* HW is not equipped with Rear Seats. Do nothing */
} //End of Function

/*********************************************************************************/
/*                     rlLF_RelayAPortOFF_FETOFF_Phase1_Detection                 */
/*********************************************************************************/
/* Phase 1 detection for Relay A: (Nothing Turned ON wrt to Relay A)                                                                   */
/* Relay A OFF and FET OFF fault detection:                                                                                            */
/*                                                                                                                                     */
/* The Logic will not execute                                                                                                          */
/* First 40ms time after POR and Port Turns OFF from ON State. (40ms wait time to settle AD Readings)                                  */
/* If there is already fault logged for current Ignition cycle. (Internal Fault set for Relay A or Short to BAT for Heaters and Vents) */
/* If already Phase 1 detection is over and Relay A is ON.                                                                             */
/*                                                                                                                                     */
/* GOOD CONDITION:                                                                                                                     */
/* As nothing turned ON, Logic should observe                                                                                          */
/* Relay A Status as LOW and                                                                                                           */
/* Vsense Readings as Zero.                                                                                                            */
/* Sets Phase1 OK bit set. waits for switch request to go for Phase 2 detection.                                                       */
/*                                                                                                                                     */
/* BAD CONDITION to BLOW STB for Particular Output:                                                                                    */
/* Relay A Status as HIGH and One of Vsense Readings are HIGH (>5v)                                                                    */
/* This condition treated as STB condition.                                                                                            */
/* Phase 1 is not OK. Logic will never goes to Phase 2 detection.                                                                      */
/* STB fault set happens at hsF_HeatDiangostics.                                                                                       */
/*                                                                                                                                     */
/* BAD CONDITION to BLOW INTERNAL FAULT:                                                                                               */
/* Relay A Status as HIGH and Vsense Readings are Zero                                                                                 */
/* This is Sturck HI condition.                                                                                                        */
/* Phase 1 is not OK. Logic will never goes to Phase 2 detection.                                                                      */
/* Wait time Logic to set Internal Fault happens at rlLF_MatureTime_For_RelayA_InternalFault()                                         */
/*                                                                                                                                     */
/* Calls from                                                                                                                          */
/* --------------                                                                                                                      */
/* rlF_RelayDiagnostics :                                                                                                              */
/*    rlLF_RelayAPortOFF_FETOFF_Phase1_Detection()                                                                                     */
/*                                                                                                                                     */
void rlLF_RelayAPortOFF_FETOFF_Phase1_Detection(void)
{
	/* Check to see if any internal fault set for Relay A or Short to BAT.        */
	/* If not set do the fault detection. 										  */
	/* If set for the present ignition cycle stop doing the fault detection       */
	if ((relay_A_internal_Fault_b == FALSE) &&
	    (hs_status_flags_c[ FRONT_LEFT ].b.hs_final_short_bat_b == FALSE) &&
	    (hs_status_flags_c[ FRONT_RIGHT ].b.hs_final_short_bat_b == FALSE)&&
	    (vs_shrtTo_batt_mntr_c != VS_SHRT_TO_BATT) ) {
		
		/* If Relay A port Not turned ON then start doing */
		if (!relay_A_enabled_b ) {
			
			/* Relay A Monitor is LOW && Vsense is LOW for both Front Heaters. GOOD Condition */
			if ( (relay_A_status_c == RELAY_MON_LOW) &&
			   ((hs_Vsense_c[FRONT_LEFT] < /*hs_STG_Vsense_cutoff_c*/hs_flt_dtc_c[HS_STG_VSENSE])&&
			   (hs_Vsense_c[FRONT_RIGHT] < /*hs_STG_Vsense_cutoff_c*/hs_flt_dtc_c[HS_STG_VSENSE])) &&
			   ((vs_Vsense_c[VS_FL] < Vs_flt_cal_prms.Flt_Dtc[V_SHRT_TO_GND])&&
			   (vs_Vsense_c[VS_FR] < Vs_flt_cal_prms.Flt_Dtc[V_SHRT_TO_GND]))) {
				
				/* Good Condition. GO to the next step     */
				/* Back ground check is ok for Front Relay */
				relay_A_phase1_ok_b = TRUE;

				/* Clear the Struck Relay HIGH Flag for Relay A */
				relay_A_stuck_HI_b = FALSE;

				/* Clear individual short to bat flag */
				relay_A_short_at_FET_b = FALSE;

				/* Clear the fault mature time counter for Relay A */
				relay_A_2000ms_fault_counter_c = 0;

				relay_A_stuck_HI_delay_c = 0;

			}
			else {
				/* Again Wait 40ms before deciding wether it is true Struck Relay or Short to Bat condition */
				if (relay_A_stuck_HI_delay_c >= /*relay_phase1_delay_c*/relay_calibs_c[RL_PHASE1_DELAY] ) {

					/* Relay Monitor is HI && Vsense is LOW */
					/* Struck Relay A HIGH..BAD Condition   */
					if ((relay_A_status_c == RELAY_MON_HIGH) && 
					   ((hs_Vsense_c[FRONT_LEFT] < /*hs_STG_Vsense_cutoff_c*/hs_flt_dtc_c[HS_STG_VSENSE])&&
					    (hs_Vsense_c[FRONT_RIGHT] < /*hs_STG_Vsense_cutoff_c*/hs_flt_dtc_c[HS_STG_VSENSE]))&&
					   ((vs_Vsense_c[VS_FL] < Vs_flt_cal_prms.Flt_Dtc[V_SHRT_TO_GND])&&
					   (vs_Vsense_c[VS_FR] < Vs_flt_cal_prms.Flt_Dtc[V_SHRT_TO_GND]))) {
						
						/* Do not proceed for further fault detection. If this falg is false, we wait for 2sec and set internal fault for Relay A */
						relay_A_phase1_ok_b = FALSE;

						/* This flag will prevent turning ON the Front heater PWMs/Vent PWMs as we have internal fault */
						relay_A_Ok_b = FALSE;

						/* Set the Struck Relay HIGH Flag for Relay A */
						relay_A_stuck_HI_b = TRUE;

						/* Clear individual short to bat flag */
						relay_A_short_at_FET_b = FALSE;

					}
					else {
						/* The other scenario might be Relay Monitor HIGH and Vsense is HIGH. This case will be a Short to BAT at Heater level. We detect this at heater diagnostics */
						relay_A_short_at_FET_b = TRUE;

						/* Clear the Struck Relay HIGH Flag for Relay A */
						relay_A_stuck_HI_b = FALSE;

					}

				}
				else {
					/* Keep incrementing */
					relay_A_stuck_HI_delay_c++;

				} //End of Else path for Incrementing relay_A_stuck_HI_delay_c

			}

		}
		else {
			/* Relay A is enabled. Do the fault detection at second place */

		}

	}
	else {
		/* Relay A internal fault set. or one of the front outputs Short to BAT conditions detected. Do nothing for this current Ignition cycle. */

	}

}

/*********************************************************************************/
/*                     rlLF_RelayBPortOFF_FETOFF_Phase1_Detection                */
/*********************************************************************************/
/* Phase 1 detection for Relay B: (Nothing Turned ON wrt to Relay B)                                                                                   */
/* Relay B OFF and FET OFF fault detection:                                                                                                            */
/*                                                                                                                                                     */
/*                                                                                                                                                     */
/* The Logic will not execute                                                                                                                          */
/* If the CSWM variant is not supporting Rear Seats.                                                                                                   */
/* First 40ms time after POR and Port Turns OFF from ON State. (40ms wait time to settle AD Readings)                                                  */
/* If there is already fault logged for current Ignition cycle. (Internal Fault set for Relay B or Short to BAT for Heaters or No Power to Rear Seats) */
/* If already Phase 1 detection is over and Relay B is ON.                                                                                             */
/*                                                                                                                                                     */
/* GOOD CONDITION:                                                                                                                                     */
/* As nothing turned ON, Logic should observe                                                                                                          */
/* Relay B Status as LOW and                                                                                                                           */
/* Vsense Readings as Zero.                                                                                                                            */
/* Sets Phase1 OK bit set. waits for switch request to go for Phase 2 detection.                                                                       */
/*                                                                                                                                                     */
/* BAD CONDITION to BLOW STB for Particular Output:                                                                                                    */
/* Relay B Status as HIGH and One of Vsense Readings are HIGH (>5v)                                                                                    */
/* This condition treated as STB condition.                                                                                                            */
/* Phase 1 is not OK. Logic will never goes to Phase 2 detection.                                                                                      */
/* STB fault set happens at hsF_HeatDiangostics.                                                                                                       */
/*                                                                                                                                                     */
/* BAD CONDITION to BLOW INTERNAL FAULT:                                                                                                               */
/* Relay B Status as HIGH and Vsense Readings are Zero                                                                                                 */
/* This is Sturck HI condition.                                                                                                                        */
/* Phase 1 is not OK. Logic will never goes to Phase 2 detection.                                                                                      */
/* Wait time Logic to set Internal Fault happens at rlLF_MatureTime_For_RelayB_InternalFault()                                                         */
/*                                                                                                                                                     */
/* Calls from                                                                                                                                          */
/* --------------                                                                                                                                      */
/* rlF_RelayDiagnostics :                                                                                                                              */
/*    rlLF_RelayBPortOFF_FETOFF_Phase1_Detection()                                                                                                     */
void rlLF_RelayBPortOFF_FETOFF_Phase1_Detection(void)
{
	/* Check to see whether we are supporting the 4 heat seat configuration */
	if ((CSWM_config_c & CSWM_4HS_VARIANT_MASK_K) == CSWM_4HS_VARIANT_MASK_K)
	{
		/* If we detect there is no power to the rear sears, we stop doing the fault detection for internal faults. */
		if ((!Rear_Seats_no_power_prelim_b) && (!Rear_Seats_no_power_final_b))
		{
			/* Check to see if any internal fault set for Relay A. If not set do the fault detection. If set for the present ignition cycle stop doing the fault detection */
			if ((relay_B_internal_Fault_b == FALSE)&&
			    (hs_status_flags_c[ REAR_LEFT ].b.hs_final_short_bat_b == FALSE) &&
			    (hs_status_flags_c[ REAR_RIGHT ].b.hs_final_short_bat_b == FALSE))
			{
				/* Relay B port not turned ON and No Production tests for RelayB */
				if (!relay_B_enabled_b)
				{
					/* Relay B Monitor is LOW && Vsense is LOW. GOOD Condition */
					if ((relay_B_status_c == RELAY_MON_LOW) && 
					   ((hs_Vsense_c[REAR_LEFT]<  /*hs_STG_Vsense_cutoff_c*/hs_flt_dtc_c[HS_STG_VSENSE]) && 
							   (hs_Vsense_c[REAR_RIGHT]<  /*hs_STG_Vsense_cutoff_c*/hs_flt_dtc_c[HS_STG_VSENSE])))
					{
						/* Good Condition. GO to the next step    */
						/* Back ground check is ok for Rear Relay */
						relay_B_phase1_ok_b = 1;

						/* Set the Struck Relay HIGH Flag for Relay B */
						relay_B_stuck_HI_b = FALSE;

						/* Clear individual short to bat flag */
						relay_B_short_at_FET_b = FALSE;

						/* Clear the fault mature time counter for Relay B */
						relay_B_2000ms_fault_counter_c = 0;

						relay_B_stuck_HI_delay_c = 0;
					}
					else
					{
						/* Again Wait 40ms before deciding wether it is true Struck Relay or Short to Bat condition */
						if (relay_B_stuck_HI_delay_c >= /*relay_phase1_delay_c*/relay_calibs_c[RL_PHASE1_DELAY] )
						{
							/* Relay Monitor is HI && Vsense is LOW */
							/* Struck Relay B HIGH..BAD Condition   */
							if ((relay_B_status_c == RELAY_MON_HIGH) && 
							   ((hs_Vsense_c[REAR_LEFT] <  /*hs_STG_Vsense_cutoff_c*/hs_flt_dtc_c[HS_STG_VSENSE])&&
							   (hs_Vsense_c[REAR_RIGHT] <  /*hs_STG_Vsense_cutoff_c*/hs_flt_dtc_c[HS_STG_VSENSE])))
							{
								/* Do not proceed for further fault detection. If this falg is false, we wait for 2sec and set internal fault for Relay B */
								relay_B_phase1_ok_b = FALSE;

								/* This flag will prevent turning ON the Front heater PWMs as we have internal fault */
								relay_B_Ok_b = FALSE;

								/* Set the Struck Relay HIGH Flag for Relay B */
								relay_B_stuck_HI_b = TRUE;

								/* Clear individual short to bat flag */
								relay_B_short_at_FET_b = FALSE;
							}
							else
							{
								/* The other scenario might be Relay Monitor HIGH and Vsense is HIGH. This case will be a Short to BAT at Heater level. We detect this at heater diagnostics */
								relay_B_short_at_FET_b = TRUE;

								/* Clear the Struck Relay HIGH Flag for Relay A */
								relay_B_stuck_HI_b = FALSE;
							}
						}
						else
						{
							relay_B_stuck_HI_delay_c++;
						}
					}
				}
				else
				{
					/* Relay B is enabled. Do the fault detection at second place */
				}
			}//No fault check loop
			else
			{
				/* Relay B internal fault set. Do nothing for this current Ignition cycle. */
			}
		}// No power detection loop
	} //Variant check loop
	else
	{
		//We are not supporting Rear seats. Do nothing
	}
}//End of Function

/*********************************************************************************/
/*                     rlLF_RelayCPortOFF_FETOFF_Phase1_Detection                */
/*********************************************************************************/
/* Phase 1 detection for Relay C: (Nothing Turned ON wrt to Relay B)                                                                                      */
/* Relay C OFF and FET OFF fault detection:                                                                                                               */
/*                                                                                                                                                        */
/*                                                                                                                                                        */
/* The Logic will not execute                                                                                                                             */
/* If the CSWM variant is not supporting Steering Wheel Functionality.                                                                                    */
/* First 40ms time after POR and Port Turns OFF from ON State. (40ms wait time to settle AD Readings)                                                     */
/* If there is already fault logged for current Ignition cycle. (Internal Fault set for Relay C or Short to BAT for Wheel or Short to GND for Wheel)      */
/* If already Phase 1 detection is over and Relay B is ON.                                                                                                */
/*                                                                                                                                                        */
/* Unlike Relay A or Relay B, the Phase 1 fault detection is different here for Relay C.                                                                  */
/* Relay A and B, we detect the Struck HI Relay condition at Phase 1.(Internal Fault)                                                                     */
/* Relay C, we detect STB and STG.(Not Internal Fault)                                                                                                    */
/*                                                                                                                                                        */
/* By default if every thing is OFF wrt to Relay C/Wheel, STB will be detected contineously.                                                              */
/* If there is true STB, the logic will never go to detect the STG.                                                                                       */
/* If there is no STB and If logic sees  a switch request for Steering Wheel, it enables the STG detection PIn waits 40ms before detecting the STG fault. */
/* If there is true STG, logic will never goes to the next phase.                                                                                         */
/*                                                                                                                                                        */
/* Either STG or STB set, the final fault will be set at stWF_Flt_Mntr().                                                                                 */
/* If there are no STG and STB then Logic sets Phase 1 Ok.                                                                                                */
/* Next step to go for Phase 2 detection.                                                                                                                 */
/*                                                                                                                                                        */
/* Calls from                                                                                                                                             */
/* --------------                                                                                                                                         */
/* rlF_RelayDiagnostics :                                                                                                                                 */
/*    rlLF_RelayCPortOFF_FETOFF_Phase1_Detection()                                                                                                        */
void rlLF_RelayCPortOFF_FETOFF_Phase1_Detection(void)
{
	/* Check if steering wheel is supported */
	if ((CSWM_config_c & CSWM_HSW_MASK_K) == CSWM_HSW_MASK_K)
	{
		
		/********************************************/
		/* Relay C OFF and FET OFF fault detection	*/
		/********************************************/
		
		/* Perform fault detection if no internal fault set */
		if ((relay_C_internal_Fault_b == FALSE) &&
		    (stW_Flt_status_w != STW_MTRD_BATT_MASK) &&
		    (stW_Flt_status_w != STW_MTRD_GND_MASK) )
		{
			/* Is Relay C turned ON? */
			if (!relay_C_enabled_b)
			{
				/* are we checking for STB fault? */
				if (relay_fault_detection_type_c == HSW_PHASE1_TEST_VOLT_OFF)
				{
					/* Have the delay counter reached the 40 ms? */
					if (relay_delay_time_for_steering_c >=  /*relay_phase1_delay_c*/relay_calibs_c[RL_PHASE1_DELAY])
					{
						/* Get the Test Voltage status */
						relay_C_Test_Voltage_Pin_status_c = (unsigned char) Dio_ReadChannel(U12T_EN);
						
						/* Is test voltage (U12S) off? */
						if(relay_C_Test_Voltage_Pin_status_c == STD_LOW){
							
							/* Check if we have waited sufficient amount of time (80ms) */
							/* (after U12 is turned off) before starting STB detection*/
							if(relay_C_STB_delay_c >= /*relay_stw_stb_delay_c*/relay_calibs_c[RL_HSW_STB_DELAY]){
								/* Check for STB on HS and LS */
								if( (stW_Vsense_HS_c > stW_flt_calibs_c[STW_SHRT_TO_BATT_VSENSE]) || 
									(stW_Vsense_LS_c > stW_flt_calibs_c[STW_SHRT_TO_BATT_VSENSE]) ){
									/* We have a STB */
									relay_C_phase1_Test_Voltage_OFF_fault_b = TRUE;								
								}else{
									/* Wait for activation requests for Heated Steering Wheel */
									if ((stW_sw_rq_b == TRUE) && (stW_state_b == FALSE))
									{
										/* Reset the delay counter */
										relay_delay_time_for_steering_c = 0;
										/* Set Test Voltage ON (for STG fault check) */
										relay_fault_detection_type_c = HSW_PHASE1_TEST_VOLT_ON;
										/* Set this bit to keep U12S on during STG tests */
										relay_C_keep_U12S_ON_b = TRUE;
									}
									/* No STB */
									relay_C_phase1_Test_Voltage_OFF_fault_b = FALSE;
								}								
							}else{
								/* Increment STB delay counter while we are getting the U12S average */
								relay_C_STB_delay_c++;
							}			
						}else{
							/* We have to catch U12S low in order to finish STB detection */
							/* U12S is ON. Clear STB delay counter here! */
							relay_C_STB_delay_c = 0;
							/* Modified 10.27.2008 */
							/* If there is no stW request then do not keep on the U12S */
							if( (stW_sw_rq_b == FALSE) && (relay_C_keep_U12S_ON_b == TRUE) ){
								relay_C_keep_U12S_ON_b = FALSE;
							}
						}
					}
					else
					{
						/* Increment the delay counter */
						relay_delay_time_for_steering_c++;
					}
				}
				else
				{
					/* Clear STB delay counter */
					relay_C_STB_delay_c = 0;
					/* Have the delay counter reached the 40 ms? */
					if (relay_delay_time_for_steering_c >= /*relay_phase1_delay_c*/relay_calibs_c[RL_PHASE1_DELAY])
					{
						/* Get the Test Voltage status */
						//relay_C_Test_Voltage_Pin_status_c = (unsigned char) Dio_ReadChannel(U12T_EN);
						
						/* Is test voltage (U12S) kept on? */
						if(main_kept_U12S_ON_b == TRUE){
							/* Check STG on Low side driver */
							if(stW_Vsense_LS_c < stW_flt_calibs_c[STW_SHRT_TO_GND_VSENSE]){
								/* We have STG on Low Side */
								relay_C_phase1_Test_Voltage_ON_fault_b = TRUE;	
							}else{
								/* No STG on Low side */
								relay_C_phase1_Test_Voltage_ON_fault_b = FALSE;
							}	
							/* Did we pass STB and STG fault tests? */
							if ( (!relay_C_phase1_Test_Voltage_OFF_fault_b) && 
								    ((!relay_C_phase1_Test_Voltage_ON_fault_b) &&
								    (relay_fault_detection_type_c == HSW_PHASE1_TEST_VOLT_ON)) )
							{
								/* Enable Relay C */
								relay_C_phase1_ok_b = TRUE;
							}else{
								/* Disable Relay C */
								relay_C_phase1_ok_b = FALSE;
							}
							relay_delay_time_for_steering_c = 0;
						}else{
							/* We have to catch U12S High in order to finish STG detection */							
						}
					}
					else
					{
						/* Increment the delay counter */
						relay_delay_time_for_steering_c++;
					}
				}
			}
			else
			{
				/* Relay C is enabled					*/
				/* Do the fault detection at Phase 2	*/
				/* Clear STB delay counter */
				relay_C_STB_delay_c = 0;
				/* Modified 10.27.2008 */
				/* If there is no stW request then do not keep on the U12S */
				if( (stW_sw_rq_b == FALSE) && (relay_C_keep_U12S_ON_b == TRUE) ) {
					relay_C_keep_U12S_ON_b = FALSE;
				}
			}
		}
		else
		{
			/* Relay C internal fault set					*/
			/* Do nothing for this current Ignition cycle	*/
			/* Clear this bit to keep U12S on during STG tests. No StWheel Request */
			if(relay_C_keep_U12S_ON_b == TRUE){
				relay_C_keep_U12S_ON_b = FALSE;
			}		
			/* Clear STB delay counter */
			relay_C_STB_delay_c = 0;
		}
	}
	else
	{
		/* Steering Wheel not supported */
		if(relay_C_keep_U12S_ON_b == TRUE){
			relay_C_keep_U12S_ON_b = FALSE;
		}
		/* Clear STB delay counter */
		relay_C_STB_delay_c = 0;
	}
}

/*********************************************************************************/
/*                     rlLF_RelayAPortON_FETOFF_Phase2_Detection                 */
/*********************************************************************************/
/* Phase 2 detection for Relay A: (Relay A Turned ON and FET OFF)                                                                      */
/*                                                                                                                                     */
/* The Logic will not execute                                                                                                          */
/* First 40ms time after POR and Port Turns OFF from ON State. (40ms wait time to settle AD Readings)                                  */
/* If there is already fault logged for current Ignition cycle. (Internal Fault set for Relay A or Short to BAT for Heaters and Vents) */
/* If Phase 1 Ok is not set                                                                                                            */
/* If already Phase 2 detection is over and FET is ON.                                                                                 */
/*                                                                                                                                     */
/* GOOD CONDITION:                                                                                                                     */
/* As Relay A turned ON, Logic should observe                                                                                          */
/* Relay A Status as HIGH and                                                                                                          */
/* Vsense Readings as Zero.                                                                                                            */
/* Sets Phase2 OK bit and Relay A OK bit set.                                                                                          */
/* With Phase 2 Ok, we will be not going for Intenral Fault Mature function for Relay A.                                               */
/* WIth Relay A Ok, Heat/Vent logic will turn ON the output.                                                                           */
/*                                                                                                                                     */
/* BAD CONDITION to BLOW INTERNAL FAULT:                                                                                               */
/* Relay A Status as LOW and Vsense Readings are Zero                                                                                  */
/* This is Sturck LOW condition.                                                                                                       */
/* Phase 2 is not OK. Relay A OK Bit will not set.                                                                                     */
/* with Relay A Ok bit not set, heat/Vent logic will not turn ON the output.                                                           */
/*                                                                                                                                     */
/* Wait time Logic to set Internal Fault happens at rlLF_MatureTime_For_RelayA_InternalFault()                                         */
/*                                                                                                                                     */
/* Calls from                                                                                                                          */
/* --------------                                                                                                                      */
/* rlF_RelayDiagnostics :                                                                                                              */
/*    rlLF_RelayAPortON_FETOFF_Phase2_Detection()                                                                                      */
void rlLF_RelayAPortON_FETOFF_Phase2_Detection(void)
{
	/* Check to see if any internal fault set for Relay A. If not set do the fault detection. If set for the present ignition cycle stop doing the fault detection */
	if ((relay_A_internal_Fault_b == FALSE) &&
	    (hs_status_flags_c[ FRONT_LEFT ].b.hs_final_short_bat_b == FALSE) &&
	    (hs_status_flags_c[ FRONT_RIGHT ].b.hs_final_short_bat_b == FALSE)&&
	    (vs_shrtTo_batt_mntr_c != VS_SHRT_TO_BATT)) {
		
		/* Relay A Turned ON and not went through the Relay fault detection phase 2 and no faulty bahvior at phase 1. then go inside the logic and do the fault detection */
		if (relay_A_enabled_b && !relay_A_phase2_ok_b && relay_A_phase1_ok_b && !relay_A_short_at_FET_b) {
			
			/* Relay A Monitor is LOW && Vsense is LOW. BAD Condition */
			/* Struck Relay LOW condition                             */
			if ((relay_A_status_c == RELAY_MON_LOW) && 
			   ((hs_Vsense_c[FRONT_LEFT] <  /*hs_STG_Vsense_cutoff_c*/hs_flt_dtc_c[HS_STG_VSENSE])||
			   (hs_Vsense_c[FRONT_RIGHT] <  /*hs_STG_Vsense_cutoff_c*/hs_flt_dtc_c[HS_STG_VSENSE])||
			   (vs_Vsense_c[VS_FL] <  Vs_flt_cal_prms.Flt_Dtc[V_SHRT_TO_GND])|| 
			   (vs_Vsense_c[VS_FR] <  Vs_flt_cal_prms.Flt_Dtc[V_SHRT_TO_GND]))) {
				
				/* Set the Struck Relay LOW Flag */
				relay_A_stuck_LOW_b = TRUE;

				/* there is a problem. */
				relay_A_phase2_ok_b = FALSE;

				/* This flag will prevent turning ON the Front heater PWMs/Vent PWMs as we have internal fault */
				relay_A_Ok_b = FALSE;
			}
			else {
				/* Relay Monitor is HI */
				if (relay_A_status_c == RELAY_MON_HIGH) {

					/* If Vsense is LOW..GOOD condition. Now The threshold comparision changed. Instead of <1v now we are checking <5v */
					if ((hs_Vsense_c[FRONT_LEFT] <  /*hs_STB_Vsense_cutoff_c*/hs_flt_dtc_c[HS_STB_VSENSE]) &&
						(hs_Vsense_c[FRONT_RIGHT] <  /*hs_STB_Vsense_cutoff_c*/hs_flt_dtc_c[HS_STB_VSENSE]) &&
						(vs_Vsense_c[VS_FL] <  Vs_flt_cal_prms.Flt_Dtc[V_SHRT_TO_BATT]) && 
					    (vs_Vsense_c[VS_FR] <  Vs_flt_cal_prms.Flt_Dtc[V_SHRT_TO_BATT]))
					{
						
						/* Good Condition. This completes fault detection. */
						relay_A_phase2_ok_b = TRUE;

						/* Clear the Struck Relay LOW Flag */
						relay_A_stuck_LOW_b = FALSE;

						/* Indication to heaters that they can turn ON the heaters */
						relay_A_Ok_b = TRUE;

						/* Clear the fault mature time counter for Relay A */
						relay_A_2000ms_fault_counter_c = 0;
					}
					else {						
						/* 10.22.2008 New Implementation */
						/* Check for Front FET fault */
						if ( (hs_Vsense_c[FRONT_LEFT] >= /*hs_STB_Vsense_cutoff_c*/hs_flt_dtc_c[HS_STB_VSENSE]) ||
							 (hs_Vsense_c[FRONT_RIGHT] >= /*hs_STB_Vsense_cutoff_c*/hs_flt_dtc_c[HS_STB_VSENSE]) ||
							 (vs_Vsense_c[VS_FL] >= Vs_flt_cal_prms.Flt_Dtc[V_SHRT_TO_BATT]) || 
							 (vs_Vsense_c[VS_FR] >= Vs_flt_cal_prms.Flt_Dtc[V_SHRT_TO_BATT]) ){
							/* There is a problem. */
							relay_A_phase2_ok_b = FALSE;

							/* This flag will prevent turning ON the Front heater PWMs/Vent PWMs as we have internal fault */
							relay_A_Ok_b = FALSE;

							/* Front FET Internal fault. */
							relay_F_FET_fault_b = TRUE;
							
						}else{
							/* No Front FET fault is present */
						}
						/* Original */
//						/* If FL Vsense is HIGH. FL FET internal fault */
//						if (hs_Vsense_c[FRONT_LEFT] >= /*mirror_eeprom.hs_short_to_bat*/HS_V_SHRT_TO_BATT) {
//							
//							/* There is a problem. */
//							relay_A_phase2_ok_b = FALSE;
//
//							/* This flag will prevent turning ON the Front heater PWMs/Vent PWMs as we have internal fault */
//							relay_A_Ok_b = FALSE;
//
//							/* Front Left FET Internal fault. */
//							relay_FL_FET_fault_b = TRUE;
//
//						}
//						else {
//							/* if FR Vsense is HIGH. It is FR FET internla fualt */
//							if (hs_Vsense_c[FRONT_RIGHT] >= /* mirror_eeprom.hs_short_to_bat*/ HS_V_SHRT_TO_BATT) {
//								
//								/* There is a problem. */
//								relay_A_phase2_ok_b = FALSE;
//
//								/* This flag will prevent turning ON the Front heater PWMs/Vent PWMs as we have internal fault */
//								relay_A_Ok_b = FALSE;
//
//								/* Front Right FET Internal fault. */
//								relay_FR_FET_fault_b = TRUE;
//
//							}
//							else {
//								/* If Vents FL Vsense is HIGH. FL FET internal fault */
//								if (vs_Vsense_c[VS_FL] >= /* mirror_eeprom.vs_short_to_bat*/HS_V_SHRT_TO_BATT) {
//									
//									/* There is a problem. */
//									relay_A_phase2_ok_b = FALSE;
//
//									/* This flag will prevent turning ON the Front heater PWMs/Vent PWMs as we have internal fault */
//									relay_A_Ok_b = FALSE;
//
//									/* Front Left FET Internal fault. */
//									relay_FL_FET_fault_b = TRUE;
//
//								}
//								else {
//									/* if FR Vsense is HIGH. It is FR FET internla fualt */
//									if (vs_Vsense_c[VS_FR] >= /* mirror_eeprom.vs_short_to_bat*/ HS_V_SHRT_TO_BATT) {
//										
//										/* There is a problem. */
//										relay_A_phase2_ok_b = FALSE;
//
//										/* This flag will prevent turning ON the Front heater PWMs/Vent PWMs as we have internal fault */
//										relay_A_Ok_b = FALSE;
//
//										/* Front Right FET Internal fault. */
//										relay_FR_FET_fault_b = TRUE;
//
//									}
//									else {
//										/* IMPOSSIBLE SITUATION. LOGIC SHOULD NEVER COME HERE */
//
//									} //End of Imposible situation else
//
//								} //End of Vents FR Vsense HIGH else path
//
//							} //End of Vents FL Vsense HIGH else path
//
//						} //End of Heats FR Vsense HIGH else path

					} //End of Heats FL Vsense HIGH else path

				}
				else {
					/* IMPOSSIBLE SITUATION. LOGIC SHOULD NEVER COME HERE */

				} //End of Impossible situation

			} //End of Thrid If Else Loop

		} //End of Second If Loop
		else {
			
			/* Either Relay A port not turned ON or problem detected at Phase 1 or finished phase 2 detetion */

		} //End of Second If Else loop 

	} //End of Main If loop entry
	else {
		/* Do not proceed to do the fault detection. As already Relay fault present or one of the front heated seats are shorted to Bat */

	} //End og Main If Else Loop

} //End of function

/*********************************************************************************/
/*                     rlLF_RelayBPortON_FETOFF_Phase2_Detection                 */
/*********************************************************************************/
/* Phase 2 detection for Relay B: (Relay B Turned ON and FET OFF)                                                                                      */
/*                                                                                                                                                     */
/* The Logic will not execute                                                                                                                          */
/* If the CSWM variant is not supporting Rear Seats.                                                                                                   */
/* First 40ms time after POR and Port Turns OFF from ON State. (40ms wait time to settle AD Readings)                                                  */
/* If there is already fault logged for current Ignition cycle. (Internal Fault set for Relay B or Short to BAT for Heaters or No Power to Rear Seats) */
/* If Phase 1 Ok is not set                                                                                                                            */
/* If already Phase 2 detection is over and FET is ON.                                                                                                 */
/*                                                                                                                                                     */
/* GOOD CONDITION:                                                                                                                                     */
/* As Relay B turned ON, Logic should observe                                                                                                          */
/* Relay B Status as HIGH and                                                                                                                          */
/* Vsense Readings as Zero.                                                                                                                            */
/* Sets Phase2 OK bit and Relay B OK bit set.                                                                                                          */
/* With Phase 2 Ok, we will be not going for Intenral Fault Mature function for Relay A.                                                               */
/* WIth Relay B Ok, Heat/Vent logic will turn ON the output.                                                                                           */
/*                                                                                                                                                     */
/* BAD CONDITION to BLOW INTERNAL FAULT:                                                                                                               */
/* Relay B Status as LOW and Vsense Readings are Zero                                                                                                  */
/* This is Sturck LOW condition.                                                                                                                       */
/* Phase 2 is not OK. Relay B OK Bit will not set.                                                                                                     */
/* with Relay B Ok bit not set, heat logic will not turn ON the output.                                                                                */
/*                                                                                                                                                     */
/* Wait time Logic to set Internal Fault happens at rlLF_MatureTime_For_RelayB_InternalFault()                                                         */
/*                                                                                                                                                     */
/* Calls from                                                                                                                                          */
/* --------------                                                                                                                                      */
/* rlF_RelayDiagnostics :                                                                                                                              */
/*    rlLF_RelayBPortON_FETOFF_Phase2_Detection()                                                                                                      */
void rlLF_RelayBPortON_FETOFF_Phase2_Detection(void)
{
	/* Check to see wether we are supporting the 4s heat. If yes go inside */
	if ((CSWM_config_c & CSWM_4HS_VARIANT_MASK_K) == CSWM_4HS_VARIANT_MASK_K) {
		
		/* If we detect there is no power to the rear sears, we stop doing the fault detection for internal faults. */
		if ((!Rear_Seats_no_power_prelim_b) && (!Rear_Seats_no_power_final_b))  {

			if ((relay_B_internal_Fault_b == FALSE)&&
			    (hs_status_flags_c[ REAR_LEFT ].b.hs_final_short_bat_b == FALSE) &&
			    (hs_status_flags_c[ REAR_RIGHT ].b.hs_final_short_bat_b == FALSE)) {
				
				/* Relay B Turned ON and not went through the Relay fault detection phase 2 and no faulty bahvior at phase 1. then go inside the logic and do the fault detection */
				if (relay_B_enabled_b && !relay_B_phase2_ok_b && relay_B_phase1_ok_b && !relay_B_short_at_FET_b) {
					
					/* Relay B Monitor is LOW && Vsense is LOW. BAD Condition */
					/* Struck Relay LOW condition                             */
					if ((relay_B_status_c == RELAY_MON_LOW) && 
					   ((hs_Vsense_c[REAR_LEFT] <  /*hs_STG_Vsense_cutoff_c*/hs_flt_dtc_c[HS_STG_VSENSE])|| 
					   (hs_Vsense_c[REAR_RIGHT] <  /*hs_STG_Vsense_cutoff_c*/hs_flt_dtc_c[HS_STG_VSENSE]))) {
						
						/* Set the Struck Relay LOW Flag */
						relay_B_stuck_LOW_b = TRUE;

						/* We can not really differentiate either relay stuck low or no power to Relay B */
						/* Set No power to Relay B bit.                                                   */
						
						/* there is a problem. */
						relay_B_phase2_ok_b = FALSE;

						/* This flag will prevent turning ON the Front heater PWMs/Vent PWMs as we have internal fault */
						relay_B_Ok_b = FALSE;

					}
					else {
						/* Relay Monitor is HI */
						if (relay_B_status_c == RELAY_MON_HIGH) {

							/* If Vsense is LOW..GOOD conditionNow The threshold comparision changed. Instead of <1v now we are checking <5v */
							if ((hs_Vsense_c[REAR_LEFT] <  /*hs_STB_Vsense_cutoff_c*/hs_flt_dtc_c[HS_STB_VSENSE])&& 
							   (hs_Vsense_c[REAR_RIGHT] <  /*hs_STB_Vsense_cutoff_c*/hs_flt_dtc_c[HS_STB_VSENSE])) {
								
								/* Good Condition. This completes fault detection. */
								relay_B_phase2_ok_b = TRUE;
								
								/* Clear the Struck Relay LOW Flag */
								relay_B_stuck_LOW_b = FALSE;
								
								/* Indication to heaters that they can turn ON the heaters */
								relay_B_Ok_b = TRUE;
								
								/* Clear the fault mature time counter for Relay B */
								relay_B_2000ms_fault_counter_c = 0;

							}
							else {
								/* 10.22.2008 New Implementation */
								/* Check for Rear FET fault */
								if ( (hs_Vsense_c[REAR_LEFT] >= /*hs_STB_Vsense_cutoff_c*/hs_flt_dtc_c[HS_STB_VSENSE]) ||
									 (hs_Vsense_c[REAR_RIGHT] >= /*hs_STB_Vsense_cutoff_c*/hs_flt_dtc_c[HS_STB_VSENSE]) ){
									
									/* There is a problem. */
									relay_B_phase2_ok_b = 0;
									
									/* This flag will prevent turning ON the Rear heater PWMs as we have internal fault */
									relay_B_Ok_b = FALSE;
									
									/* Rear FET Internal fault. */
									relay_R_FET_fault_b = TRUE;
									
								}else{
									/* No Rear FET fault is present */
								}
								
//								/* Original */
//								/* If RL Vsense is HIGH. FL FET internal fault */
//								if (hs_Vsense_c[REAR_LEFT] >= /* mirror_eeprom.hs_short_to_bat*/ HS_V_SHRT_TO_BATT) {
//									
//									/* There is a problem. */
//									relay_B_phase2_ok_b = 0;
//									
//									/* This flag will prevent turning ON the Front heater PWMs/Vent PWMs as we have internal fault */
//									relay_B_Ok_b = FALSE;
//									
//									/* Front Left FET Internal fault. */
//									relay_RL_FET_fault_b = TRUE;
//
//								}
//								else {
//									/* if RR Vsense is HIGH. It is FR FET internla fualt */
//									if (hs_Vsense_c[REAR_RIGHT] >= /* mirror_eeprom.hs_short_to_bat*/ HS_V_SHRT_TO_BATT) {
//										
//										/* There is a problem. */
//										relay_B_phase2_ok_b = 0;
//										
//										/* This flag will prevent turning ON the Front heater PWMs/Vent PWMs as we have internal fault */
//										relay_B_Ok_b = FALSE;
//										
//										/* Front Right FET Internal fault. */
//										relay_RR_FET_fault_b = TRUE;
//
//									}
//									else {
//										
//										/* IMPOSSIBLE SITUATION. LOGIC SHOULD NEVER COME HERE */
//
//									}
//
//								} //end of RL Heat Vsense HIGH Path 

							} //end of RR Heat Vsense HIGH Path

						}
						else {
							
							/* IMPOSSIBLE SITUATION. LOGIC SHOULD NEVER COME HERE */

						}

					} // end of Relay B status Else path

				}
				else {
					
					/* Either Relay B port not turned ON or problem detected at Phase 1 or finished phase 2 detetion */

				}

			}
			else {

				//Do not proceed to do the fault detection.  As already Relay fault present or one of the front heated seats are shorted to Bat

			}

		} //No power check condition end

	}
	else {

		//We are not supporting the  4HS. Do nothing for rear seats.

	}

} //End of function

/*********************************************************************************/
/*                     rlLF_RelayCPortON_FETOFF_Phase2_Detection                 */
/*********************************************************************************/
/* Phase 2 detection for Relay C: (Relay C Turned ON and FET OFF)                                                                           */
/*                                                                                                                                          */
/* The Logic will not execute                                                                                                               */
/* If the CSWM variant is not supporting Steering Wheel.                                                                                    */
/* First 40ms time after POR and Port Turns OFF from ON State. (40ms wait time to settle AD Readings)                                       */
/* If there is already fault logged for current Ignition cycle. (Internal Fault set for Relay C or Short to BAT for Wheel or STG for Wheel) */
/* If Phase 1 Ok is not set                                                                                                                 */
/*                                                                                                                                          */
/*                                                                                                                                          */
/* GOOD CONDITION:                                                                                                                          */
/* As Relay C turned ON, Logic should observe                                                                                               */
/* Sets Phase2 OK bit and Relay C OK bit set.                                                                                               */
/* With Phase 2 Ok, we will be not going for Intenral Fault Mature function for Relay C.                                                    */
/* WIth Relay C Ok, Wheel logic will turn ON the output.                                                                                    */
/*                                                                                                                                          */
/* BAD CONDITION to BLOW INTERNAL FAULT:                                                                                                    */
/* This is Stuck Open condition.                                                                                                            */
/* Phase 2 is not OK. Relay C OK Bit will not set.                                                                                          */
/* with Relay C Ok bit not set, stw logic will not turn ON the output.                                                                     */
/*                                                                                                                                          */
/* Wait time Logic to set Internal Fault happens at rlLF_MatureTime_For_RelayC_InternalFault()                                              */
/*                                                                                                                                          */
/* Calls from                                                                                                                               */
/* --------------                                                                                                                           */
/* rlF_RelayDiagnostics :                                                                                                                   */
/*    rlLF_RelayCPortON_FETOFF_Phase2_Detection()                                                                                           */
void rlLF_RelayCPortON_FETOFF_Phase2_Detection(void)
{
	/* Check if steering wheel is supported */
	if ((CSWM_config_c & CSWM_HSW_MASK_K) == CSWM_HSW_MASK_K)
	{
		/********************************************/
		/* Relay C ON and FET OFF fault detection	*/
		/********************************************/
		
		/* Perform fault detection if no internal fault or STB or STG is set */
		if ((relay_C_internal_Fault_b == FALSE)&&
		    (stW_Flt_status_w != STW_MTRD_BATT_MASK) &&
		    (stW_Flt_status_w != STW_MTRD_GND_MASK) )
		{
			/* Relay C is enabled					*/
			/* Relay C Phase 1 Passed				*/
			/* Relay C Phase 2 is not compeleted    */
			/* Relay C Stuck Open not set           */
			
			/* Modified on 01.13.2009: Check relay_C_internal_Fault_b instead of relay_C_stuck_LOW_b */
			/* to avoid setting a short to ground fault after only one Vsense reading that is below threshold */
			if ( (relay_C_enabled_b == TRUE) && (relay_C_phase1_ok_b == TRUE) && 
				 (relay_C_phase2_ok_b == FALSE) && (relay_C_internal_Fault_b == FALSE) )
			{
				/* Get the Test Voltage status */
				//relay_C_Test_Voltage_Pin_status_c = (unsigned char) Dio_ReadChannel(U12T_EN);
				
				/* Last thing we need to check is STG on High Side: Is test voltage On? */
				if(main_kept_U12S_ON_b == TRUE){
					/* Check STG on Low Side after the Relay C is enabled! (Vsense_LS should go to zero) */
					if(stW_Vsense_LS_c > /*relay_stw_stk_open_c*/relay_calibs_c[RL_HSW_STUCK_OPEN]){
						/* LS Relay Stuck OFF Fault*/
						relay_C_stuck_LOW_b = TRUE;
						/* Relay C Phase 2 Failed */
						relay_C_phase2_ok_b = FALSE;
						/* Disable PWM for High side */
						relay_C_Ok_b = FALSE;						
					}else{
						/* No LS Relay Stuck OFF Fault */
						relay_C_stuck_LOW_b = FALSE;
						/* Relay C Phase 2 Passed */
						relay_C_phase2_ok_b = TRUE;
						/* Enable PWM for High side */
						relay_C_Ok_b = TRUE;								
						/* Added on 01.03.2009: Clear relay_C_2000ms_fault_counter_c */
						/* Relay C is o.k. Clear 2 sec fault mature counter */
						relay_C_2000ms_fault_counter_c = 0;
					}
					/* We are done with Relay C tests! Turn off the relay request to keep U12S on. */
					relay_C_keep_U12S_ON_b = FALSE;
				}else{
					/* We need test voltage On in order to test Stuck Low on Low Side and finish the tests */
				}
			}
			else
			{
				/* Relay C is Disabled or	*/
				/* Relay C Phase 1 and/or 2 Failed	*/				
			}
		}
		else
		{
			/* Relay C internal fault set					*/
			/* Do nothing for this current Ignition cycle	*/
			/* Clear this bit to keep U12S on during STG tests. No StWheel Request */
			if(relay_C_keep_U12S_ON_b == TRUE){
				relay_C_keep_U12S_ON_b = FALSE;
			}				
		}
	}
	else
	{
		/* Steering Wheel not supported */
		/* Clear this bit to keep U12S on during STG tests. No StWheel Request */
		if(relay_C_keep_U12S_ON_b == TRUE){
			relay_C_keep_U12S_ON_b = FALSE;
		}	
	}
}

/*********************************************************************************/
/*                     rlLF_MatureTime_For_RelayA_InternalFault                  */
/*********************************************************************************/
/* Mature Internal Fault for Relay A:                                                                                                       */
/*                                                                                                                                          */
/* The Logic will not execute                                                                                                               */
/* First 40ms time after POR and Port Turns OFF from ON State. (40ms wait time to settle AD Readings)                                       */
/* If there is already fault logged for current Ignition cycle. (Internal Fault set for Relay A or Short to BAT for Heaters and Vents)      */
/*                                                                                                                                          */
/* 2sec Wait time:                                                                                                                          */
/* If Logic detects Either RELAY STRUCK HI(Phase 1 Ok Not set) or RELAY STRUCK LOW (Phase 2 Ok Not set), it starts incrementing 2sec timer. */
/* Once it reaches 2sec timer it sets the Internal Fault bit for Relay A                                                                    */
/*                                                                                                                                          */
/* Set DTC will be taken care by rlF_RelayDTC                                                                                               */
/*                                                                                                                                          */
/* Calls from                                                                                                                               */
/* --------------                                                                                                                           */
/* rlF_RelayDiagnostics :                                                                                                                   */
/*    rlLF_MatureTime_For_RelayA_InternalFault()                                                                                            */
/*                                                                                                                                          */
void rlLF_MatureTime_For_RelayA_InternalFault(void)
{
	/* Check to see there are no Short to BAT conditions exist prior to setting the internal fault bit. */
	if ((relay_A_short_at_FET_b == FALSE) &&
	    (hs_status_flags_c[ FRONT_LEFT ].b.hs_final_short_bat_b == FALSE) &&
	    (hs_status_flags_c[ FRONT_RIGHT ].b.hs_final_short_bat_b == FALSE)&&
	    (vs_shrtTo_batt_mntr_c != VS_SHRT_TO_BATT) &&
	    (!relay_A_internal_Fault_b)) {
		
		/* Check to see If Relay A is turned ON and Faulty scenario occured at Phase 2 level or Fault occured at Phase 1. */
		if ((relay_A_enabled_b && !relay_A_phase2_ok_b) ||  !relay_A_phase1_ok_b) {
			
			/* Wait for 2 seconds to mature the Front Relay internal fault. */
			if (relay_A_2000ms_fault_counter_c >= /*mature_2sec_time_c*/hs_flt_dtc_c[HS_2SEC_MATURE_TIME]) {
				
				/* 2 sec completed. Set the Internal fault for Relay 1. */
				relay_A_internal_Fault_b = TRUE;
				
				/* Clear the 2sec time counter for Relay A */
				relay_A_2000ms_fault_counter_c = 0;
				
				//Check for F L swich requests present 
				//If yes, Wait time LEd counter variable be set to 2sec
				//With this the next cycle, heaters will turn OFF the outputs 
				if(hs_rem_seat_req_c[FRONT_LEFT] != SET_OFF_REQUEST) 
				{
					/* Make sure that, the wait time for 2sec LED counter */
					/* matures, so immediately we can turn OFF the Outputs  */
					hs_monitor_2sec_led_counter_c[ FRONT_LEFT ] = /*mature_2sec_time_c*/hs_flt_dtc_c[HS_2SEC_MATURE_TIME];
				}
				
				if(hs_rem_seat_req_c[FRONT_RIGHT] != SET_OFF_REQUEST) 
				{
					/* Make sure that, the wait time for 2sec LED counter */
					/* matures, so immediately we can turn OFF the Outputs  */
					hs_monitor_2sec_led_counter_c[ FRONT_RIGHT ] = /*mature_2sec_time_c*/hs_flt_dtc_c[HS_2SEC_MATURE_TIME];
				}

			}
			else {
				/* 2sec are not completed. Keep Incrementing the timer. */
				relay_A_2000ms_fault_counter_c++;

			}

		}
		else {
			/* No fault conditions for Relay A. Do nothing. */

		}

	}
	else {
		/* Short to BAt conditions detected.Clear the 2sec timer to mature the Internla fault */
		relay_A_2000ms_fault_counter_c = 0;

	}

} //End of Function

/*********************************************************************************/
/*                     rlLF_MatureTime_For_RelayB_InternalFault                  */
/*********************************************************************************/
/* Mature Internal Fault for Relay B and Mature No Power to Rear Seats Fault:                                                                          */
/*                                                                                                                                                     */
/* The Logic will not execute                                                                                                                          */
/* If the CSWM variant is not supporting Rear Seats.                                                                                                   */
/* If there is already fault logged for current Ignition cycle. (Internal Fault set for Relay B or Short to BAT for Heaters or No Power to Rear Seats) */
/*                                                                                                                                                     */
/* 2sec Wait time for Setting Internal Fault:                                                                                                          */
/* If Logic detects Either RELAY STRUCK HI(Phase 1 Ok Not set) or RELAY STRUCK LOW (Phase 2 Ok Not set), it starts incrementing 2sec timer.            */
/* Once it reaches 2sec timer it sets the Internal Fault bit for Relay B.                                                                              */
/* Set DTC will be taken care by rlF_RelayDTC()                                                                                                        */
/*                                                                                                                                                     */
/* 2sec Wait time for Setting No Power to Rear Seats Fault:                                                                                            */
/* If Logic detects Preliminary fault for No Power to Rear Seats, it starts incrementing 2sec timer.                                                   */
/* Once it reaches 2sec timer it sets the Final Fault for No Power to Rear Seats.                                                                      */
/* Blowing DTC will be taken care by hsF_HeatDTC()                                                                                                     */
/*                                                                                                                                                     */
/*                                                                                                                                                     */
/* Calls from                                                                                                                                          */
/* --------------                                                                                                                                      */
/* rlF_RelayDiagnostics :                                                                                                                              */
/*    rlLF_MatureTime_For_RelayB_InternalFault()                                                                                                       */
void rlLF_MatureTime_For_RelayB_InternalFault(void)
{
	/* Check to see wether we are supporting the 4s heat. If yes go inside */
	if ((CSWM_config_c & CSWM_4HS_VARIANT_MASK_K) == CSWM_4HS_VARIANT_MASK_K) {
		
		/* Check to see there are no Short to BAT conditions exist prior to setting the internal fault bit. */
		if ((relay_B_short_at_FET_b == FALSE) &&
		    (hs_status_flags_c[ REAR_LEFT ].b.hs_final_short_bat_b == FALSE) &&
		    (hs_status_flags_c[ REAR_RIGHT ].b.hs_final_short_bat_b == FALSE)&&
		    (!relay_B_internal_Fault_b) &&
		    (!Rear_Seats_no_power_final_b)) {
			
			/* Check to see do we have Power to Relay B. If yes mature the internal fault, else mature no power to Relay B */
			if ((!Rear_Seats_no_power_prelim_b) && (!Rear_Seats_no_power_final_b)) {
				
				/* Check to see If Relay B is turned ON and Faulty scenario occured at Phase 2 level or Fault occured at Phase 1. */
				if ((relay_B_enabled_b && !relay_B_phase2_ok_b) ||  !relay_B_phase1_ok_b) {
					
					/* Wait for 2sec to mature the Internal fault for Rear Relay Internal fault */
					if (relay_B_2000ms_fault_counter_c >= /*mature_2sec_time_c*/hs_flt_dtc_c[HS_2SEC_MATURE_TIME]) {
						
						/* 2sec matured. Ser the internal fault for Rear Relay */
						relay_B_internal_Fault_b = TRUE;
						
						/* Claer the 2sec timer for Relay B */
						relay_B_2000ms_fault_counter_c = 0;
						
						//Check for R L swich requests present 
						//If yes, Wait time LEd counter variable be set to 2sec
						//With this the next cycle, heaters will turn OFF the outputs 
						if(hs_rem_seat_req_c[REAR_LEFT] != SET_OFF_REQUEST) 
						{
							/* Make sure that, the wait time for 2sec LED counter */
							/* matures, so immediately we set the fault           */
							hs_monitor_2sec_led_counter_c[ REAR_LEFT ] = /*mature_2sec_time_c*/hs_flt_dtc_c[HS_2SEC_MATURE_TIME];
						}
						
						//Check for R R swich requests present 
						//If yes, Wait time LEd counter variable be set to 2sec
						//With this the next cycle, heaters will turn OFF the outputs 
						if(hs_rem_seat_req_c[REAR_RIGHT] != SET_OFF_REQUEST) 
						{
							/* Make sure that, the wait time for 2sec LED counter */
							/* matures, so immediately we set the fault           */
							hs_monitor_2sec_led_counter_c[ REAR_RIGHT ] = /*mature_2sec_time_c*/hs_flt_dtc_c[HS_2SEC_MATURE_TIME];
						}

					}
					else {
						/* 2sec are not completed. Keep Incrementing the timer. */
						relay_B_2000ms_fault_counter_c++;

					}

				}
				else {
					/* No fault conditions for Relay B. Do nothing. */

				}

			}
			else {
				/* Wait for 2sec to mature the Internal fault for Rear Relay Internal fault */
				if (relay_B_2000ms_fault_counter_c >= /*mature_2sec_time_c*/hs_flt_dtc_c[HS_2SEC_MATURE_TIME] ) {
					
					/* 2sec matured. Set the no power to Rear seats */
					Rear_Seats_no_power_final_b = TRUE;
					
					/* Claer the 2sec timer for Relay B */
					relay_B_2000ms_fault_counter_c = 0;

				}
				else {
					/* 2sec are not completed. Keep Incrementing the timer. */
					relay_B_2000ms_fault_counter_c++;

				}

			}

		}
		else {
			/* Short to BAt conditions detected.Clear the 2sec timer to mature the Internla fault */
			relay_B_2000ms_fault_counter_c = 0;

		}

	}
	else {
		//We are not supporting the  4HS. Do nothing for rear seats.

	}

} //End of Function

/*********************************************************************************/
/*                     rlLF_MatureTime_For_RelayC_InternalFault                  */
/*********************************************************************************/
/* Mature Internal Fault for Relay C:                                                                                                              */
/*                                                                                                                                                 */
/* The Logic will not execute                                                                                                                      */
/* If the CSWM variant is not supporting Heated Steering Wheel.                                                                                    */
/* If there is already fault logged for current Ignition cycle. (Internal Fault set for Relay C or Short to BAT for Wheel or Short to GND for set) */
/*                                                                                                                                                 */
/* 2sec Wait time for Setting Internal Fault:                                                                                                      */
/* If Logic detects RELAY STRUCK LOW (Phase 2 Ok Not set), it starts incrementing 2sec timer.                                                      */
/* Once it reaches 2sec timer it sets the Internal Fault bit for Relay C.                                                                          */
/* Set DTC will be taken care by rlF_RelayDTC()                                                                                                    */
/*                                                                                                                                                 */
/* Calls from                                                                                                                                      */
/* --------------                                                                                                                                  */
/* rlF_RelayDiagnostics :                                                                                                                          */
/*    rlLF_MatureTime_For_RelayC_InternalFault()                                                                                                   */
void rlLF_MatureTime_For_RelayC_InternalFault(void)
{
	/* Check to see there are no Short to BAT conditions exist prior to setting the internal fault bit. */
	if ((relay_C_internal_Fault_b == FALSE)&&
	    (stW_Flt_status_w != STW_MTRD_BATT_MASK) &&
	    (stW_Flt_status_w != STW_MTRD_GND_MASK))
	{
		/* Check to see If Relay C is turned ON and Faulty scenario occured at Phase 2 level or Fault occured at Phase 1. */
		if ( (relay_C_enabled_b == TRUE) && 
		     (relay_C_phase2_ok_b == FALSE) && 
		     (relay_C_stuck_LOW_b == TRUE) )
		{
			/* Wait for 2sec to mature the Internal fault for Rear Relay Internal fault */
			if (relay_C_2000ms_fault_counter_c >=  /*mature_2sec_time_c*/hs_flt_dtc_c[HS_2SEC_MATURE_TIME])
			{
				/* 2sec matured. Set the internal fault for Rear Relay */
				relay_C_internal_Fault_b = TRUE;
				
				/* Claer the 2sec timer for Relay B */
				relay_C_2000ms_fault_counter_c = 0;
			}
			else
			{
				/* 2sec are not completed. Keep Incrementing the timer. */
				relay_C_2000ms_fault_counter_c++;
			}
		}
		else
		{
			/* No fault conditions for Relay C. Do nothing. */
		}
	}
	else
	{
		/* Short to BAt conditions detected.Clear the 2sec timer to mature the Internla fault */
		relay_C_2000ms_fault_counter_c = 0;
	}
} //End of function

/*********************************************************************************/
/*                             rlF_RelayDTC                                      */
/*********************************************************************************/
/* Input to this function are from rlF_Relay diagnsotics ().                                                                                          */
/* If this function sees any of the internal fault bits set, this function calles for rlLF_Set_Relay_Err () to set the internal fault bits in EEPROM. */
/* Else calls rlLF_Set_Relay_Err () to clear the internal fault bits set for Relay.                                                                   */
/*                                                                                                                                                    */
/* Calls from                                                                                                                                         */
/* --------------                                                                                                                                     */
/* main :                                                                                                                                             */
/*    rlF_RelayDTC()                                                                                                                                  */
/*                                                                                                                                                    */
/* Calls to                                                                                                                                           */
/* --------                                                                                                                                           */
/* rlLF_Set_Relay_Err()                                                                                                                               */
@far void rlF_RelayDTC(void)
{
	/* Module blows the same DTC incase of Relay A fault occured or Relay B fault occured */
	/* If Relay A is faulty, store the DTC with active status                             */
	
	/* 10.22.2008 Updated */
	/* Check if Relay A internal fault mature time has expired */
	if(relay_A_internal_Fault_b == TRUE){
		
		/* Check Relay A faults */
		if(relay_A_stuck_HI_b == TRUE){
			/* Set Internal fault RELAY_A_STUCK_HI */
			rlLF_Set_Relay_Err(RELAY_A_STUCK_HI_MASK, INT_FLT_SET);				
		}else if(relay_A_stuck_LOW_b == TRUE ){
			/* Set Internal fault RELAY_A_STUCK_LO */
			rlLF_Set_Relay_Err(RELAY_A_STUCK_LO_MASK, INT_FLT_SET);				
		}else if(relay_F_FET_fault_b == TRUE){
			/* Set Internal fault F_HS_DRIVER_FLT */
			rlLF_Set_Relay_Err(F_HS_DRIVER_FLT_MASK, INT_FLT_SET);
		}
	}
	else
	{
		RelayA_Stuck_HI_b = FALSE;
		rlLF_Set_Relay_Err(CLR_RELAYA_STUCK_HI, INT_FLT_CLR);

		RelayA_Stuck_LO_b = FALSE;
		rlLF_Set_Relay_Err(CLR_RELAYA_STUCK_LO, INT_FLT_CLR);
		
		relay_F_FET_fault_b = FALSE; 		
		rlLF_Set_Relay_Err(CLR_F_DRIVER_FAULT, INT_FLT_CLR);
		
	}
	
	/* Check to see, we are supporting the 4 Heat seat */
	if ((CSWM_config_c & CSWM_4HS_VARIANT_MASK_K) == CSWM_4HS_VARIANT_MASK_K)
	{
		/* If Relay B is faulty, store the DTC with active status */
		if(relay_B_internal_Fault_b == TRUE){
			
			/* Check Relay B faults */
			if(relay_B_stuck_HI_b == TRUE){
				/* Set Internal fault RELAY_B_STUCK_HI */
				rlLF_Set_Relay_Err(RELAY_B_STUCK_HI_MASK, INT_FLT_SET);				
			}else if(relay_B_stuck_LOW_b == TRUE ){
				/* Set Internal fault RELAY_B_STUCK_LO */
				rlLF_Set_Relay_Err(RELAY_B_STUCK_LO_MASK, INT_FLT_SET);				
			}else if(relay_R_FET_fault_b == TRUE){
				/* Set Internal fault R_HS_DRIVER_FLT */
				rlLF_Set_Relay_Err(R_HS_DRIVER_FLT_MASK, INT_FLT_SET);
			}
		}
		else
		{
			RelayB_Stuck_HI_b = FALSE;
			rlLF_Set_Relay_Err(CLR_RELAYB_STUCK_HI, INT_FLT_CLR);

			RelayB_Stuck_LO_b = FALSE;
			rlLF_Set_Relay_Err(CLR_RELAYB_STUCK_LO, INT_FLT_CLR);
			
			relay_R_FET_fault_b = FALSE;
			rlLF_Set_Relay_Err(CLR_R_DRIVER_FAULT, INT_FLT_CLR);
		}
	}
	else
	{
		//We are not supporting the 4 seat Variant. Do nothing
	}
	
	/* IMPORTANT NOTE: MY11 Steering wheel stuck open will be set as Short to Ground fault */
	/* No stuck open internal fault is set */
	/* Check to see, we are supporting the Steering Wheel */
//	if ((CSWM_config_c & CSWM_HSW_MASK_K) == CSWM_HSW_MASK_K)
//	{
//		/* If Relay C is faulty, store the DTC with active status */
//		if (relay_C_internal_Fault_b)
//		{
//			if (relay_C_stuck_LOW_b)
//			{
//				rlLF_Set_Relay_Err(RELAY_C_STUCK_LO_MASK, INT_FLT_SET);
//			}
//		}
//		else
//		{
//			//RelayC_Stuck_HI_b = FALSE;
//			RelayC_Stuck_LO_b = FALSE;
//
//			rlLF_Set_Relay_Err(CLR_RELAYC_STUCK_LO, INT_FLT_CLR);
//		}
//	}
//	else
//	{
//		//We are not supporting the Steering Wheel. Do nothing
//	}
} //End of function

/*********************************************************************************/
/*                             rlLF_Set_Relay_Err                                */
/*********************************************************************************/
/* This function gets called when ever the Relay Logic observes the Faulty scenario with the Relay ports.   */
/* After disabling the functionality, this function sets the internal fault flags and writes to the EEPROM. */
/* The same way, when fault goes away, this functions gets called and updates the EEPROM.                   */
/*                                                                                                          */
/* Calls from                                                                                               */
/* --------------                                                                                           */
/* rlF_RelayDTC :                                                                                           */
/*    rlLF_Set_Relay_Err()                                                                                  */
/*                                                                                                          */
/* Calls to                                                                                                 */
/* --------                                                                                                 */
/* IntFlt_F_Update(u_8Bit Byte_number,u_8Bit Bit_mask,Int_Flt_Op_Type_e intFlt_Op)                          */
void rlLF_Set_Relay_Err(unsigned char relay_bit_mask, Int_Flt_Op_Type_e Operation)
{
	u_8Bit relay_bit = relay_bit_mask;        // bit mask (to set or clear the fault)
	u_8Bit relay_set_mask = 0;                // set bit mask
	
	/* Read the relay internal fault byte */
	EE_BlockRead(EE_INTERNAL_FLT_BYTE1, (u_8Bit*)&intFlt_bytes[ONE]);
	
	/* Check operation type (set or clear internal fault) */
	if (Operation == INT_FLT_SET) {
		
		/* Check if we already updated the internal fault */
		if ( (intFlt_bytes[ONE] & relay_bit_mask) == relay_bit_mask) {			
			/* Already updated internal fault */
		}
		else {
			/* Update known faults */
			IntFlt_F_Update(ONE, relay_bit, INT_FLT_SET);
			
			/* Set the internal fault bit */
			int_flt_byte1.cb |= relay_bit;
		}
	}	// Operation: Set Internal Fault If statement end
	else {
		/* Store the set mask */
		relay_set_mask = (u_8Bit)(~relay_bit);
		
		/* Check if we have to clear the internal fault */
		if ( (intFlt_bytes[ONE] & relay_set_mask) == relay_set_mask) {
			
			/* Update known faults */
			IntFlt_F_Update(ONE, relay_bit, INT_FLT_CLR);
			
			/* Clear the internal fault bit */
			int_flt_byte1.cb &= relay_bit;
		}
		else {
			/* Internal fault bit is not set */
		}
	}	// Operation: Clear Internal fault. Else path end
} //End of Function

/*********************************************************************************/
/*                         rlF_Production_Support NOT USED                       */
/*********************************************************************************/
//@far void rlF_Production_Support(unsigned char relay_c, unsigned char action_c)
//{
//	
//	/* This function gets called when ever production commands gets executed for the Relay Module                                      */
//	/* This function individually turns ON the BAse Relay and other relay ports for testing purpose with out giving the switch request */
//
//}
