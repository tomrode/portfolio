#define DIAGREAD_C

/*****************************************************************************************************
*  (c) Copyright 2011 Continental Automotive Systems, all rights reserved
*
*  All material contained herein is CONTINENTAL CONFIDENTIAL and PROPRIETARY.
*  Any reproduction of this material without written consent from 
*  Continental AG is strictly forbidden.                                  
*
*  Filename:     DiagRead.c
*
*  Created_by:   Harsha Yekollu
*
*  Date_created: 01/19/2011
*
*  Description:  Main file for UDS Diagnostics -- Read Data By LID Services. 
*
*  Revision_history:
*
*  MKS ID       Author         Date      Description of Change(s)
*  --------  -------------  --------  --------------------------------------
*  xxxxxxx   H. Yekollu     01/19/11  Initial creation for FIAT CUSW MY12 Program
*  74548     H. Yekollu     06/07/11  Update new DDT60-00-005 services.
*  81840	 H. Yekollu		08/05/11  Issue fixed for 223001, as it always reported response pending.	
*  82322     H. Yekollu		09/18/11  Updates to new DDT 60-00-007 services	
*  75322	 H. Yekollu	    10/06/11  Cleanup the code.
*  86223	 H. Yekollu		10/12/11  DDT 60-00-009 Updates. 
* 
******************************************************************************************************/
/*****************************************************************************************************
* 									DIAGREAD
* This module handles the UDS Read Data By LID - Service 0x22.
* 
*****************************************************************************************************/

/*****************************************************************************************************
*     								Include Files
******************************************************************************************************/
/* CAN_NWM Related Include files */
#include "il_inc.h"
#include "v_inc.h"
#include "desc.h"
#include "appdesc.h"

#include "TYPEDEF.h"
#include "main.h"
#include "DiagMain.h"
#include "DiagIOCtrl.h"
#include "DiagSec.h"
#include "DiagRead.h"
#include "DiagWrite.h"
#include "canio.h"
#include "FaultMemoryLib.h"
#include "heating.h"
#include "Venting.h"
#include "StWheel_Heating.h"
#include "Temperature.h"
#include "Internal_Faults.h"
#include "AD_Filter.h"
#include "BattV.h"
#include "Rom_Test.h"
#include "mw_eem.h"
#include "mc9s12xs128.h"
#include "fbl_def.h"



/* Autosar include Files */
#include "Mcu.h"
//#include "Port.h"
#include "Dio.h"
//#include "Gpt.h"
//#include "Pwm.h"
//#include "Adc.h"
//#include "Wdg.h"

/*****************************************************************************************************
*     								LOCAL ENUMS
******************************************************************************************************/
/*****************************************************************************************************
*     								LOCAL #DEFINES
******************************************************************************************************/
///* Low and HIGH bytes of RPM */
//#define LOWRPM(x)  (x & 0x00FF)            
//#define HIGHRPM(x) ((x >> 8) & 0x00FF)
///* LOW, MEDIUM  and HIGH bytes of ODOMETER */
//#define LOWODO(x)  (x & 0x000000FF)            
//#define MEDIUMODO(x) ((x >> 8) & 0x000000FF)
//#define HIGHODO(x) ((x >> 16) & 0x000000FF)

/* Macros */
#define LOWBYTE(x)  (x & 0x00FF)            
#define HIGHBYTE(x) ((x >> 8) & 0x00FF)     

/*****************************************************************************************************
*     								LOCAL VARIABLES
******************************************************************************************************/
/* New Diagnostic Service $22 02 7E: Read Conditions to activate CSWM Outputs variables */
u_8Bit diag_srvc_22027E_byte1;	// First byte of the check conditions to activate CSWM Outputs
u_8Bit diag_srvc_22027E_byte2;	// Second byte of the check conditions to activate CSWM Outputs
u_8Bit diag_srvc_22F10B_byte0;	//zero byte 
u_8Bit diag_srvc_22F10B_byte1; // First byte of the check conditions to ECU configuration
u_8Bit diag_srvc_22F10B_byte2; // Second byte of the check conditions to ECU configuration
u_8Bit diag_ldShd_Stat_c, diag_PCB_tempStat_c;
//static u_temp_l DataLen;    //Not used PC-Lint Warning 528
static u_temp_l DataLength;
static unsigned char temp_c;
static enum DIAG_RESPONSE Response;
/*****************************************************************************************************
*     								LOCAL FUNCTION CALLS
******************************************************************************************************/
extern @far void diagF_DiagResponse(enum DIAG_RESPONSE Response, u_temp_l Length, DescMsgContext* pMsgContext);
/*  ********************************************************************************
 * Function ApplDescRead_220101_Bussed_Outputs_Status (Service request header:$22 $1 $1 )
 * $22 $0101 - Bussed Output Service:
 * 
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescRead_220101_Bussed_Outputs_Status(DescMsgContext* pMsgContext)
{
      
    /* DataByte  Bit    Information                                     */
	/*  0        0,1,2  000: Left Front Heating status: Not Activated   */
	/*                  001: Left Front Heating status: Low Level 1     */
	/*                  010: Left Front Heating status: Low Level 2     */
	/*                  011: Left Front Heating status: High Level 1    */
	/*                  100: Left Front Heating status: High Level 2    */
	/*                  101: Left Front Heating status: High Level 3    */
	/*  0        3,4,5  000: Right Front Heating status: Not Activated  */
	/*                  001: Right Front Heating status: Low Level 1    */
	/*                  010: Right Front Heating status: Low Level 2    */
	/*                  011: Right Front Heating status: High Level 1   */
	/*                  100: Right Front Heating status: High Level 2   */
	/*                  101: Right Front Heating status: High Level 3   */
	/*  0        6,7    Reserved                                        */
	/*                                                                  */
	/* DataByte  Bit    Information$*$/                                 */
	/*  1        0,1,2  000: Left Rear Heating status: Not Activated    */
	/*                  001: Left Rear Heating status: Low Level 1      */
	/*                  010: Left Rear Heating status: Low Level 2      */
	/*                  011: Left Rear Heating status: High Level 1     */
	/*                  100: Left Rear Heating status: High Level 2     */
	/*                  101: Left Rear Heating status: High Level 3     */
	/*  1        3,4,5  000: Right Rear Heating status: Not Activated   */
	/*                  001: Right Rear Heating status: Low Level 1     */
	/*                  010: Right Rear Heating status: Low Level 2     */
	/*                  011: Right Rear Heating status: High Level 1    */
	/*                  100: Right Rear Heating status: High Level 2    */
	/*                  101: Right Rear Heating status: High Level 3    */
	/*  1        6,7    Reserved                                        */
	/*                                                                  */
	/* DataByte  Bit    Information                                     */
	/*  2        0,1    00: Left Front Vent status: Not Activated       */
	/*                  01: Left Front Vent status: Level 1             */
	/*                  10: Left Front Vent status: Level 2             */
	/*                  11: Left Front Vent status: Level 3             */
	/*  2        2,3    00: Right Front Vent status: Not Activated      */
	/*                  01: Right Front Vent status: Level 1            */
	/*                  10: Right Front Vent status: Level 2            */
	/*                  11: Right Front Vent status: Level 3            */
	/*  2        4-7    Reserved                                        */
	/*                                                                  */
	/* DataByte  Bit    Information                                     */
	/*  3        0,1    HSW Status                                      */
	/*  3        2-7    Reserved                                        */

	/* Initialization */
	temp_c = 0;
	/* Read the Front LeftHeating status from CAN and Fill Byte 1 (Bit 0 1 and 2)*/
	temp_c = hs_output_status_c [FRONT_LEFT];
	pMsgContext->resData[0] = temp_c;
			
	/* Read the Front Right Heating status from CAN and Fill Byte 1 (Bit 3,4 and 5)*/
	temp_c = hs_output_status_c [FRONT_RIGHT];
	temp_c <<= 3;
			
	pMsgContext->resData[0] |= temp_c;

	/* If we are supporting 4 Heat Seats, then go inside and fill the exact status */
	if ((CSWM_config_c & CSWM_4HS_VARIANT_MASK_K) == CSWM_4HS_VARIANT_MASK_K)
	{
		temp_c = 0;
				
//		/* Read the Rear Left Heating status from CAN and Fill Byte 1 (Bit 0,1 and 2)*/
		temp_c = hs_output_status_c [REAR_LEFT];
		pMsgContext->resData[1] = temp_c;
		
		/* Read the Rear Right Heating status from CAN and Fill Byte 1 (Bit 3,4 and 5)*/
		temp_c = hs_output_status_c [REAR_RIGHT];
		temp_c <<= 3;
		pMsgContext->resData[1] |= temp_c;

	}
	else
	{
		pMsgContext->resData[1] = 0x00;
	}
	/* If we are supporting 2 Vents, then go inside and fill the exact status */
	if ((CSWM_config_c & CSWM_2VS_MASK_K) == CSWM_2VS_MASK_K)
	{
		temp_c = 0;
				
		temp_c = vsF_Get_Output_Stat(VS_FL);
		pMsgContext->resData[2] = temp_c;
		
		temp_c = vsF_Get_Output_Stat(VS_FR);
		temp_c <<= 2; 
		pMsgContext->resData[2] |= temp_c;
	}
	else
	{
		pMsgContext->resData[2] = 0x00;
	}
	/* If we are supporting HSW then go inside and fill the exact status */
	if ((CSWM_config_c & CSWM_HSW_MASK_K) == CSWM_HSW_MASK_K)
	{
		pMsgContext->resData[3] = stWF_Get_Output_Stat();
	}
	else
	{
		pMsgContext->resData[3] = 0x00;
	}
			
	/* Remote start enabled status */
	temp_c = 0;
	temp_c = canio_RemSt_configStat_c;
	temp_c <<= 1;
	pMsgContext->resData[4] = temp_c;
						
	DataLength = 5;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, DataLength, pMsgContext);
}

/*  ********************************************************************************
 * Function ApplDescRead_220102_Bussed_Input (Service request header:$22 $1 $2 ) 
 * $22 $0102 - Bussed Input Service:
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescRead_220102_Bussed_Input(DescMsgContext* pMsgContext)
{
    unsigned char temp_Proxi_c[5];  
	/* Buseed Input Service. Most of the Signals received from CAN Bus will be captured here. 	*/
	/* FIAT SIGANLS: Starts with resData[0]														*/
	
	/* BYTE 0:			Engine Speed															*/	
	
	/* BYTE 1 BIT 0:	Engine Speed Valid Data													*/
	/* BYTE 1 BIT 1,2:	Engine Status															*/
	/* BYTE 1 BIT 3,4,5:Ignition Status															*/
	/* BYTE 1 BIT 6:	Ign Run Remote Start Active												*/
	/* BYTE 1 BIT 7:	Last Known Remote Start Configuration Status							*/
	
	/* BYTE 2 BIT 0,1:	Right Front Heat Request Status											*/
	/* BYTE 2 BIT 2,3:	Left Front Heat Request Status											*/
	/* BYTE 2 BIT 4,5:	Right Front Vent Request Status											*/
	/* BYTE 2 BIT 6,7:	Left Front Vent Request Status											*/

	/* BYTE 3 BIT 0,1:	Steering Wheel Request Status											*/
	/* BYTE 3 BIT 2,3:	Driver side Information													*/
	/* BYTE 3 BIT 4,5:	Steering Wheel Temp Sns Fault											*/
	/* BYTE 3 BIT 6,7:	Reserved											                	*/
	
	/* BYTE 4:			Steering Wheel Temperature												*/
	/* BYTE 5:			System Voltage															*/
	/* BYTE 6:			Measured Battery Voltage												*/
	/* Byte 7:			External Temperature													*/
	
	//EE_BlockRead(EE_PROXI_CFG, (u_8Bit*)&temp_Proxi_c[0]);

	/* BYTE 0 */
	pMsgContext->resData[0] = canio_RX_EngineSpeed_c;

	/* BYTE 1: BIT 0: BIT 1,2: BIT 3,4,5: BIT 6: BIT 7: */
	temp_c = 0; //Init the value.
	pMsgContext->resData[1] = temp_c; //Initialization
	temp_c = IlGetRxEngineSpeedValidData();
	pMsgContext->resData[1] = temp_c; //Engine Speed Valid Data Info stored.
	
	temp_c = IlGetRxEngineSts();
	temp_c <<=1;
	pMsgContext->resData[1] |= temp_c; //Engine Status stored
	
	temp_c = canio_RX_IGN_STATUS_c;
	temp_c <<= 3;
	pMsgContext->resData[1] |= temp_c; //Ignition Status stored
	
	temp_c = canio_RX_IgnRun_RemSt_c;
	temp_c <<= 6;
	pMsgContext->resData[1] |= temp_c; //Ignition Run Remote Start Active stored
	
	temp_c = canio_RemSt_configStat_c;
	temp_c<<=7;
	pMsgContext->resData[1] |= temp_c; //Last Known Remote start status stored

	/* BYTE 2: BIT 0,1: BIT 2,3: BIT 4,5: BIT 6,7: */
	temp_c = 0; //Init the value.
	pMsgContext->resData[2] = temp_c;  //Initialization
	temp_c = canio_RX_FR_HS_RQ_TGW_c;
	pMsgContext->resData[2] = temp_c;  //Front Right Heating Switch Request Stored
	temp_c = canio_RX_FL_HS_RQ_TGW_c;
	temp_c <<= 2;
	pMsgContext->resData[2] |= temp_c; //Front Left Heating Switch Request Stored
	/* Check Front Vents are supported */
	/* If supported fill Byte 2, Bit 4,5 with Front Right Vent and Bit 6,7 with Front Left vent */
	if( ((CSWM_config_c & CSWM_2VS_MASK_K) == CSWM_2VS_MASK_K) )
	{
		temp_c = canio_RF_VS_RQ_c;
		temp_c <<= 4;
		pMsgContext->resData[2] |= temp_c; //Front Right Vent Switch Request Stored.
		temp_c = canio_LF_VS_RQ_c;
		temp_c <<= 6;
		pMsgContext->resData[2] |= temp_c; //Front Left Vent Switch Request Stored.
	}
	
	/* BYTE 3: BIT 0,1: BIT 2,3: BIT 4,5: BIT 6,7: */
	temp_c = 0; //Init the value.
	pMsgContext->resData[3] = temp_c;  //Initialization
	/* If HSW is supported, fill Byte 3, bit 0,1 with Request status */
	if ((CSWM_config_c & CSWM_HSW_MASK_K) == CSWM_HSW_MASK_K)
	{
		/* Read the HSW Request Information from CAN BUS and fill Bit 0,1 */
		temp_c = canio_RX_HSW_RQ_TGW_c;
		pMsgContext->resData[3] = temp_c;
	}
	/* Driver side status will be filled in Bit 2 and 3      */
	if(canio_LHD_RHD_c)
	{
	temp_c = 0x02;     //Table conversions  2114
	}
	else
	{
	temp_c = 0x01;     //Table conversions  2114
	}
	temp_c <<= 2;
	pMsgContext->resData[3] |= temp_c;
	/* If HSW is supported, fill bit 4,5 with HSW Temp sens Flt */
	if ((CSWM_config_c & CSWM_HSW_MASK_K) == CSWM_HSW_MASK_K)
	{
		/* Read the HSW Request Information from CAN BUS and fill Bit 4,5 */
		temp_c = canio_RX_StW_TempSens_FltSts_c;
		temp_c<<=4;
		pMsgContext->resData[3] |= temp_c;
	}
	
	/* BYTE 4: HSW Temperature   */                          
	/* Initialization */
	pMsgContext->resData[4] = 0;
	/* If HSW is supported, fill Byte 4, bit 0,1 with Request status */
	if ((CSWM_config_c & CSWM_HSW_MASK_K) == CSWM_HSW_MASK_K)
	{
		/* Store the Heated Steering Wheel temperature from Bus */
		pMsgContext->resData[4] = canio_RX_StW_TempSts_c;
	}
			
	/* BYTE 5: Read the Battery Voltage from CAN Bus and fill */
	pMsgContext->resData[5] = canio_RX_BatteryVoltageLevel_c;
	
	/*BYTE 6 - Measured Battery voltage information */
	pMsgContext->resData[6] = BattVF_Get_Crt_Uint();
	
	/* BYTE 7 - EXternal Temperature */
	pMsgContext->resData[7] = canio_RX_ExternalTemperature_c;
	/* To be filled once receive the specific changes approved */
	DataLength = 8;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, DataLength, pMsgContext);
}

/*  ********************************************************************************
 * Function ApplDescRead_220103_Rear_Seat_Switch_Input_Status (Service request header:$22 $1 $3 )
 * $22 $0103 - Rear Seat Switch Input Status:
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescRead_220103_Rear_Seat_Switch_Input_Status(DescMsgContext* pMsgContext)
{
    //Grab the values incase Rear heated seats present and from HW Switch inputs
	if( ((CSWM_config_c & CSWM_4HS_VARIANT_MASK_K) == CSWM_4HS_VARIANT_MASK_K) && (main_SwReq_Rear_c == VEH_SWITCH_REAR_ONLY_K) )
	{
		/*Init the two variables */
		temp_c = 0;
		pMsgContext->resData[0] = temp_c;
				
		//First two bits are R L Switch status(Pressed 00/Not pressed 01)
		temp_c = hs_rear_swth_stat_c[RL_SWITCH];
		//temp_c = (unsigned char) Dio_ReadChannel(RL_LED_SW_DETECT);
		pMsgContext->resData[0] = temp_c;
				
		temp_c = 0;
		//3,4 bits are R R Switch status(Pressed 00/Not pressed 01)
		temp_c = hs_rear_swth_stat_c[RR_SWITCH];
		//temp_c = (unsigned char) Dio_ReadChannel(RR_LED_SW_DETECT);
		temp_c<<=2;
		pMsgContext->resData[0] |= temp_c;
				
		temp_c = 0;
		//Bit 5. Contains status of Rear Left switch pressed for this IGN cycle
		temp_c = hs_rear_swpsd_stat_c[RL_SWITCH];
		temp_c<<=4;
		pMsgContext->resData[0] |= temp_c;

		temp_c = 0;
		//Bit 6. Contains status of Rear Left switch pressed for this IGN cycle
		temp_c = hs_rear_swpsd_stat_c[RR_SWITCH];
		temp_c<<=5;
		pMsgContext->resData[0] |= temp_c;

		DataLength = 1;
		Response = RESPONSE_OK;
				
	}
	else
	{
       DataLength = 1;
	   Response = SUBFUNCTION_NOT_SUPPORTED;
    }
    diagF_DiagResponse(Response, DataLength, pMsgContext);
}

/*  ********************************************************************************
 * Function ApplDescRead_220104_Conditions_to_activate_CSWM_outputs (Service request header:$22 $1 $4 )
 * $22 $0104 - Conditions to Activate CSWM Outputs
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescRead_220104_Conditions_to_activate_CSWM_outputs(DescMsgContext* pMsgContext)
{
    diag_srvc_22027E_byte1 = 0;
	diag_srvc_22027E_byte2 = 0;
	/* Read System voltage, local battery voltage, load shed, and PCB temperature status */
			
	/* Commented by CK on 04.29.2009. Looking at current voltage instead of voltage status */
	//diag_SystemV_Stat_c = BattVF_Get_Status(SYSTEM_V);
	//diag_BatteryV_Stat_c = BattVF_Get_Status(BATTERY_V);
	diag_ldShd_Stat_c = BattVF_Get_LdShd_Mnt_Stat();
	diag_PCB_tempStat_c = TempF_Get_PCB_NTC_Stat();
			
	/******************/
	/* Byte 1: Checks */
	/******************/
			
	/* Bit 1: Ignition status is run*/
	if(canio_RX_IGN_STATUS_c == IGN_RUN){
		diag_srvc_22027E_byte1 |= IGN_STAT_IS_RUN;
	}else{
		diag_srvc_22027E_byte1 &= (~IGN_STAT_IS_RUN);
	}
			
	/* Bit 2: Front left heated seat NTC <= 70C */
	if(NTC_AD_Readings_c[FRONT_LEFT] >= hs_cal_prms.hs_prms[hs_vehicle_line_c].NTC_CutOFF_c[4]){
		diag_srvc_22027E_byte1 |= FL_HS_NTC_LESS_70C;						
	}else{
		diag_srvc_22027E_byte1 &= (~FL_HS_NTC_LESS_70C);
	}
			
	/* Bit 3: Front right heated seat NTC <= 70C */
	if(NTC_AD_Readings_c[FRONT_RIGHT] >= hs_cal_prms.hs_prms[hs_vehicle_line_c].NTC_CutOFF_c[4]){
		diag_srvc_22027E_byte1 |= FR_HS_NTC_LESS_70C;						
	}else{
		diag_srvc_22027E_byte1 &= (~FR_HS_NTC_LESS_70C);
	}
			
	/* Bit 4: Rear left heated seat NTC <= 70C */
	if(NTC_AD_Readings_c[REAR_LEFT] >= hs_cal_prms.hs_prms[hs_vehicle_line_c].NTC_CutOFF_c[4]){
		diag_srvc_22027E_byte1 |= RL_HS_NTC_LESS_70C;						
	}else{
		diag_srvc_22027E_byte1 &= (~RL_HS_NTC_LESS_70C);
	}
			
	/* Bit 5: Rear right heated seat NTC <= 70C */
	if(NTC_AD_Readings_c[REAR_RIGHT] >= hs_cal_prms.hs_prms[hs_vehicle_line_c].NTC_CutOFF_c[4]){
		diag_srvc_22027E_byte1 |= RR_HS_NTC_LESS_70C;						
	}else{
		diag_srvc_22027E_byte1 &= (~RR_HS_NTC_LESS_70C);
	}
			
	/* Bit 6: Heated steering wheel temperature <= 50C */
	if(stW_temperature_c <= stW_prms.cals[0].MaxTemp_Threshld){
		diag_srvc_22027E_byte1 |= HSW_TEMP_LESS_50C;
	}else{
		diag_srvc_22027E_byte1 &= (~HSW_TEMP_LESS_50C);
	}
			
	/* Bit 7: Engine RPM in range or Propulsion System active */
	if( (canio_RX_EngineSpeed_c > main_EngRpm_lowLmt_c) ){
		diag_srvc_22027E_byte1 |= ENG_RPM_IN_RANGE;
	}else{
		diag_srvc_22027E_byte1 &= (~ENG_RPM_IN_RANGE);
	}
			
	/* Bit 8: System and Local Battery Voltage are in operation range 10V to 16V */
	if( (BattV_set[SYSTEM_V].crt_vltg_c <= BattV_cals.Thrshld_a[SET_OV_LMT]) &&
		(BattV_set[SYSTEM_V].crt_vltg_c >= BattV_cals.Thrshld_a[SET_UV_LMT]) &&
		(BattV_set[BATTERY_V].crt_vltg_c <= BattV_cals.Thrshld_a[SET_OV_LMT]) && 
		(BattV_set[BATTERY_V].crt_vltg_c >= BattV_cals.Thrshld_a[SET_UV_LMT]) ){
		diag_srvc_22027E_byte1 |= VOLTAGE_IN_RANGE;
	}else{
		diag_srvc_22027E_byte1 &= (~VOLTAGE_IN_RANGE);
	}
			
	/******************/
	/* Byte 2: Checks */
	/******************/
			
	/* Bit 1: PCB over temperature condition is present */
	if(diag_PCB_tempStat_c == PCB_STAT_OVERTEMP){
		diag_srvc_22027E_byte2 |= PCB_OVER_TEMP_PRESENT;
	}else{
		diag_srvc_22027E_byte2 &= (~PCB_OVER_TEMP_PRESENT);
	}
			
	/* Bit 2: Load shed is active */
	if(diag_ldShd_Stat_c != LDSHD_MNT_GOOD){
		diag_srvc_22027E_byte2 |= LOAD_SHED_ACTIVE;
	}else{
		diag_srvc_22027E_byte2 &= (~LOAD_SHED_ACTIVE);
	}
			
	/* Bit 3: Can communication or LOC with major Network node is present */
	if( (canio_canbus_off_b == TRUE) || (final_FCM_LOC_b == TRUE) || (final_CCN_LOC_b == TRUE) || (final_HVAC_LOC_b == TRUE) || (final_CBC_LOC_b == TRUE) || (final_TGW_LOC_b == TRUE) ){
		diag_srvc_22027E_byte2 |= CAN_COMM_OR_LOC_FLT;
	}else{
		diag_srvc_22027E_byte2 &= (~CAN_COMM_OR_LOC_FLT);
	}
			
	/* Bit 4: Internal fault is present */
	if( (intFlt_bytes[ZERO] != NO_INT_FLT) || (intFlt_bytes[ONE] != NO_INT_FLT) || (intFlt_bytes[TWO] != NO_INT_FLT) ){
		diag_srvc_22027E_byte2 |= INTERNAL_FLT_PRESENT;
	}else{
		diag_srvc_22027E_byte2 &= (~INTERNAL_FLT_PRESENT);
	}					
			
	pMsgContext->resData[0] = diag_srvc_22027E_byte1;
	pMsgContext->resData[1] = diag_srvc_22027E_byte2;					
	DataLength = 2;
	Response = RESPONSE_OK;
    diagF_DiagResponse(Response, DataLength, pMsgContext);
}

/*  ********************************************************************************
 * Function ApplDescRead_220105_Heated_Seat_NTC_Feedback_Sensor_Status (Service request header:$22 $1 $5 )
 * $22 $0105 - Sensor Voltage and Temperature to NTC Feedback
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescRead_220105_Heated_Seat_NTC_Feedback_Sensor_Status(DescMsgContext* pMsgContext)
{
      
    /* Read the Present NTC Readings read from Ad channels */
	pMsgContext->resData[0] = NTC_AD_Readings_c[FRONT_LEFT]; // F L Seat NTC Reading
	pMsgContext->resData[1] = NTC_AD_Readings_c[FRONT_RIGHT]; // F R Seat NTC Reading
	pMsgContext->resData[2] = NTC_AD_Readings_c[REAR_LEFT];  // R L Seat NTC Reading
	pMsgContext->resData[3] = NTC_AD_Readings_c[REAR_RIGHT];  // R R Seat NTC Reading

	/* 10.28.2008  Modified */
	pMsgContext->resData[4] = TempF_NtcAdStd_to_CanTemp(NTC_AD_Readings_c[FRONT_LEFT]); 
	pMsgContext->resData[5] = TempF_NtcAdStd_to_CanTemp(NTC_AD_Readings_c[FRONT_RIGHT]);
	pMsgContext->resData[6] = TempF_NtcAdStd_to_CanTemp(NTC_AD_Readings_c[REAR_LEFT]);  
	pMsgContext->resData[7] = TempF_NtcAdStd_to_CanTemp(NTC_AD_Readings_c[REAR_RIGHT]);

	DataLength = 8;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, DataLength, pMsgContext);
}

/*  ********************************************************************************
 * Function ApplDescRead_220106_HeatVentWheel_Current_Sense (Service request header:$22 $1 $6 )
 * $22 $0106 - Seat Current Sense
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescRead_220106_HeatVentWheel_Current_Sense(DescMsgContext* pMsgContext)
{
  	  
    pMsgContext->resData[0] = hs_Isense_c[FRONT_LEFT]; //Front Left Heater Current Sense Reading 
	pMsgContext->resData[1] = hs_Isense_c[FRONT_RIGHT]; //Front Right Heater Current Sense Reading

	if ((CSWM_config_c & CSWM_4HS_VARIANT_MASK_K) == CSWM_4HS_VARIANT_MASK_K)
	{
		pMsgContext->resData[2] = hs_Isense_c[REAR_LEFT]; //Rear Left Heater Current Sense Reading
		pMsgContext->resData[3] = hs_Isense_c[REAR_RIGHT]; //Rear Right Heater Current Sense Reading
	}
	else
	{
		pMsgContext->resData[2] = 0x00;
		pMsgContext->resData[3] = 0x00;
	}
	if ((CSWM_config_c & CSWM_HSW_MASK_K) == CSWM_HSW_MASK_K)
	{
		pMsgContext->resData[4] = stW_Isense_c; //Steering Wheel Current Sense Reading
	}
	else
	{
		pMsgContext->resData[4] = 0x00;
	}
	if((CSWM_config_c & CSWM_2VS_MASK_K) == CSWM_2VS_MASK_K)
	{
	    pMsgContext->resData[5] = vs_Isense_c[VS_FL]; // VL Isense
	    pMsgContext->resData[6] = vs_Isense_c[VS_FR]; // VR Isense
	}
	else
	{
		pMsgContext->resData[5] = 0x00;
		pMsgContext->resData[6] = 0x00;
	}
			
	DataLength = 7;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, DataLength, pMsgContext);
}

/*  ********************************************************************************
 * Function ApplDescRead_220107_Vented_Seats_Voltage_Sense (Service request header:$22 $1 $7 )
 * $22 $0107 - Vent Seat Voltage Sense: 
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescRead_220107_Vented_Seats_Voltage_Sense(DescMsgContext* pMsgContext)
{
	if((CSWM_config_c & CSWM_2VS_MASK_K) == CSWM_2VS_MASK_K)
	{
		/* Vent Vsense AD readings (displayed in hex) */
		pMsgContext->resData[0] = vs_Vsense_c[VS_FL]; // VL Vsense 
		pMsgContext->resData[1] = vs_Vsense_c[VS_FR]; // VR Vsense
	}
	else
	{
		pMsgContext->resData[0] = 0x00;
		pMsgContext->resData[1] = 0x00;
		
	}
	DataLength = 2;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, DataLength, pMsgContext);
}

/*  ********************************************************************************
 * Function ApplDescReadECU_time_stamps_RAM (Service request header:$22 $10 $8 )
 * $22 $1008 - Read ECU Time Stamps
 * 
 * UPDATE DID'S $1008, $1009, $2008 and $2009	(e-mail dated 03/24/2011)
 * Every 60sec (1 min):	When Accumulation timer range of 0 	  - 1800 Sec 	 (0 - 30min)
 * Every 300sec(5 min):	When Accumulation timer range of 1801 - 3600 sec     (31 - 60 min)
 * Every 600sec(10 min):	When Accumulation timer range of 3601 - Max sec      (61 - max min)
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescReadECU_time_stamps_RAM(DescMsgContext* pMsgContext)
{

	pMsgContext->resData[0] = fmem_lifetime_l.lw.hhbyte;
	pMsgContext->resData[1] = fmem_lifetime_l.lw.hbyte;
	pMsgContext->resData[2] = fmem_lifetime_l.lw.lbyte;
	pMsgContext->resData[3] = fmem_lifetime_l.lw.llbyte;
	
	DataLength = 4;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, DataLength, pMsgContext);
}

/*  ********************************************************************************
 * Function ApplDescReadECU_time_stamps_from_KeyOn_RAM (Service request header:$22 $10 $9 )
 * Description:See  to TFO 09009. 
 * $22 $1009 - Read ECU Timestamps from KEY ON
 * 
 * UPDATE DID'S $1008, $1009, $2008 and $2009	(e-mail dated 03/24/2011)
 * Every 60sec (1 min):	When Accumulation timer range of 0 	  - 1800 Sec 	 (0 - 30min)
 * Every 300sec(5 min):	When Accumulation timer range of 1801 - 3600 sec     (31 - 60 min)
 * Every 600sec(10 min):	When Accumulation timer range of 3601 - Max sec      (61 - max min)
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescReadECU_time_stamps_from_KeyOn_RAM(DescMsgContext* pMsgContext)
{
		
	//EE_BlockRead(EE_ECU_TIME_KEYON, (u_8Bit*)&pMsgContext->resData[0]);
	pMsgContext->resData[0] = HIGHBYTE(fmem_keyon_time_w);
	pMsgContext->resData[1] = LOWBYTE(fmem_keyon_time_w);

	DataLength = 2;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, DataLength, pMsgContext);
}

/*  ********************************************************************************
 * Function ApplDescRead_22102A_Check_EOL_configuration_data (Service request header:$22 $10 $2A )
 * Description: not available 
 * Returns:  nothing
 * Particularitie(s) and limitation(s):
 *   - The function "DescProcessingDone" must be called from now on (within this
 * main-handler or later).
 *   - The function "DescSetNegResponse" can be called within this main-handler or later
 * but before calling "DescProcessingDone".
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescRead_22102A_Check_EOL_configuration_data(DescMsgContext* pMsgContext)
{
	/*Read all 9 bytes */
	EE_BlockRead(EE_PROXI_FAILURE, (u_8Bit*)&pMsgContext->resData[0]);
	DataLength = 9;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, DataLength, pMsgContext);
	
}

/*  ********************************************************************************
 * Function ApplDescReadOdometer (Service request header:$22 $20 $1 )
 * Description:See  to TFO 09009.
 * $22 $2001 - Read OdoMeter
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescReadOdometer(DescMsgContext* pMsgContext)
{
	/* Byte 0-2:  	Odometer value				(3 Byte)						*/
	//Commented the stored ODO. Instead pointing to the latest. EEPROM will be updated every Power ON.
	//EE_BlockRead(EE_ODOMETER, (u_8Bit*)&pMsgContext->resData[0]);
	
	pMsgContext->resData[0] = canio_RX_TotalOdo_c[0];
	pMsgContext->resData[1] = canio_RX_TotalOdo_c[1];
	pMsgContext->resData[2] = canio_RX_TotalOdo_c[2];

	DataLength = 3;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, DataLength, pMsgContext);
}

/*  ********************************************************************************
 * Function ApplDescReadOdometer_content_to_the_last_F_ROM_updating (Service request header:$22 $20 $2 )
 * Description:See TFO 7284/01 - TFO 09009.
 * $22 $2002 - Read Odometer content to the last F-ROM Updating
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescReadOdometer_content_to_the_last_F_ROM_updating(DescMsgContext* pMsgContext)
{
      /* ODO Meter content to the last F-Rom updating*/
	EE_BlockRead(EE_ODOMETER_LAST_PGM, (u_8Bit*)&pMsgContext->resData[0]);
	
	DataLength = 3;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, DataLength, pMsgContext);
}

/*  ********************************************************************************
 * Function ApplDescReadNumber_of_FlashRom_re_writings_F_ROM (Service request header:$22 $20 $3 )
 * Description:See TFO 7284/01- TFO 09009  for details
 * $22 $2003 - Read Number Of FlashROM Re Writings
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescReadNumber_of_FlashRom_re_writings_F_ROM(DescMsgContext* pMsgContext)
{
	/* Byte 0:  	No of Re programs				(1 Byte)						*/
	//pMsgContext->resData[2] = Flash Rom Writings; 
	EE_BlockRead(EE_FBL_PROG_ATTEMPTS, (u_8Bit*)&pMsgContext->resData[0]);

	DataLength = 1;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, DataLength, pMsgContext);
}

/*  ********************************************************************************
 * Function ApplDescReadECU_time_stamps_EEPROM (Service request header:$22 $20 $8 )
 * Description:This RDI Must be not change after riprogramming, see  to TFO 09009 for further
 * details.
 * 
 * $22 $2008 - Read ECU Timestamps from EEPROM.
 * 
 * UPDATE DID'S $1008, $1009, $2008 and $2009	(e-mail dated 03/24/2011)
 * Every 60sec (1 min):	When Accumulation timer range of 0 	  - 1800 Sec 	 (0 - 30min)
 * Every 300sec(5 min):	When Accumulation timer range of 1801 - 3600 sec     (31 - 60 min)
 * Every 600sec(10 min):	When Accumulation timer range of 3601 - Max sec      (61 - max min)
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescReadECU_time_stamps_EEPROM(DescMsgContext* pMsgContext)
{
	/* Byte 0-3:  	ECU Life Cycle				(4 Byte)						*/
			
	EE_BlockRead(EE_ECU_TIMESTAMPS, (u_8Bit*)&pMsgContext->resData[0]); //Read the stored value of life time from EEPROM
	
	DataLength = 4;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, DataLength, pMsgContext);
}

/*  ********************************************************************************
 * Function ApplDescReadECU_time_stamps_from_KeyOn_EEPROM (Service request header:$22 $20 $9 )
 * Description:This RDI Must be not change after riprogramming,  see  to TFO 09009 for further
 * details.
 * 
 * $22 $2009 - Read ECU Timestamps from KEY ON
 * 
 * UPDATE DID'S $1008, $1009, $2008 and $2009	(e-mail dated 03/24/2011)
 * Every 60sec (1 min):	When Accumulation timer range of 0 	  - 1800 Sec 	 (0 - 30min)
 * Every 300sec(5 min):	When Accumulation timer range of 1801 - 3600 sec     (31 - 60 min)
 * Every 600sec(10 min):	When Accumulation timer range of 3601 - Max sec      (61 - max min)
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescReadECU_time_stamps_from_KeyOn_EEPROM(DescMsgContext* pMsgContext)
{
	/* Byte 0-1:  	ECU time in this IGN cycle		(2 Byte)						*/
	EE_BlockRead(EE_ECU_TIME_KEYON, (u_8Bit*)&pMsgContext->resData[0]);
	
	DataLength = 2;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, DataLength, pMsgContext);
}

/*  ********************************************************************************
 * Function ApplDescReadKeyOn_counter_EEPROM (Service request header:$22 $20 $A )
 * Description:This RDI Must be not change after riprogramming,  see  to TFO 09009 for further
 * details.
 * 
 * $22 $200A - Read KeyONCounter from EEPROM
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescReadKeyOn_counter_EEPROM(DescMsgContext* pMsgContext)
{
		
	//pMsgContext->resData[2] = Key ON Counter;
	EE_BlockRead(EE_ECU_KEYON_COUNTER, (u_8Bit*)&pMsgContext->resData[0]);
	
	DataLength = 2;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, DataLength, pMsgContext);
}

/*  ********************************************************************************
 * Function ApplDescReadECU_Time_first_DTC_detection_EEPROM (Service request header:$22 $20 $B )
 * Description:See  to TFO 09009.
 * 
 * $22 $200B - Read ECU Timestamps on First DTC detection (EEPROM)
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescReadECU_Time_first_DTC_detection_EEPROM(DescMsgContext* pMsgContext)
{

	//Read the stored value of life time from EEPROM when the first DTC Detected.
	EE_BlockRead(EE_ECU_TIME_FIRSTDTC, (u_8Bit*)&pMsgContext->resData[0]); 

	DataLength = 4;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, DataLength, pMsgContext);
}

/*  ********************************************************************************
 * Function ApplDescReadECU_Time_Stamps_from_KeyOn_first_DTC_detection_EEPROM (Service request header:$22 $20 $C )
 * Description:See  to TFO 09009.
 * 
 * $22 $200C - Read ECU Timestamps from keyON on first DTC detection (EEPROM)
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescReadECU_Time_Stamps_from_KeyOn_first_DTC_detection_EEPROM(DescMsgContext* pMsgContext)
{
	//Read the stored value of KeyON time from EEPROM when the first DTC Detected.
	EE_BlockRead(EE_ECU_TIME_KEYON_FIRSTDTC, (u_8Bit*)&pMsgContext->resData[0]); 

	DataLength = 2;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, DataLength, pMsgContext);
}

/*  ********************************************************************************
 * Function ApplDescReadKeyOn_Counter_Status_EEPROM (Service request header:$22 $20 $F )
 * Description:This RDI Must be not change after riprogramming, see  to TFO 09009 for further
 * details.
 * 
 * $22 $200F - Keyon Counter Status.??????????
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescReadKeyOn_Counter_Status_EEPROM(DescMsgContext* pMsgContext)
{
	
	//pMsgContext->resData[2] = Key ON Counter status; TO BE UPDATED

	//Read the stored value of KeyON counter from EEPROM 
	//EE_BlockRead(EE_ECU_KEYON_COUNTER, (u_8Bit*)&pMsgContext->resData[0]); 
	DataLength = 1;
	Response = RESPONSE_OK; 
	diagF_DiagResponse(Response, DataLength, pMsgContext);
}

/*  ********************************************************************************
 * Function ApplDescReadProgrammingStatus_EEPROM_FLASH (Service request header:$22 $20 $10 )
 * Description:This parameter must report the status of LAST programming performed by external
 * tool.
 * 
 * $22 $2010 - Read Programming Status (EEPROM/FLASH)
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescReadProgrammingStatus_EEPROM_FLASH(DescMsgContext* pMsgContext)
{

	//pMsgContext->resData[2] = Programming status; 
	EE_BlockRead(EE_FBL_PROG_STATUS, (u_8Bit*)&pMsgContext->resData[0]); 

	DataLength = 4;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, DataLength, pMsgContext);
}

/*  ********************************************************************************
 * Function ApplDescRead_2E2023_System_Configuration_PROXI (Service request header:$22 $20 $23 )
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescRead_2E2023_System_Configuration_PROXI(DescMsgContext* pMsgContext)
{
  
	DataLength = 255;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, DataLength, pMsgContext);

}

/*  ********************************************************************************
 * Function ApplDescRead_222024_Received_PROXI_Information (Service request header:$22 $20 $24 )
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescRead_222024_Received_PROXI_Information(DescMsgContext* pMsgContext)
{

  		/* Proxi ID */

	temp_c = 0;

	temp_c = ru_eep_PROXI_Cfg.s.PROXI_IDVeh.s.Digit11;
    temp_c <<= 4;

  	pMsgContext->resData[0] =  temp_c;	

	pMsgContext->resData[0] |= ru_eep_PROXI_Cfg.s.PROXI_IDVeh.s.Digit10;

	temp_c = 0;

	temp_c = ru_eep_PROXI_Cfg.s.PROXI_IDVeh.s.Digit9;
    temp_c <<= 4;

  	pMsgContext->resData[1] =  temp_c;	

	pMsgContext->resData[1] |= ru_eep_PROXI_Cfg.s.PROXI_IDVeh.s.Digit8;


	temp_c = 0;

	temp_c = ru_eep_PROXI_Cfg.s.PROXI_IDVeh.s.Digit7;
    temp_c <<= 4;

  	pMsgContext->resData[2] =  temp_c;	

	pMsgContext->resData[2] |= ru_eep_PROXI_Cfg.s.PROXI_IDVeh.s.Digit6;

  		/* Proxi crc */

	temp_c = 0;

	temp_c = ru_eep_PROXI_Cfg.s.PROXI_CRC.s.Digit5;
    temp_c <<= 4;

  	pMsgContext->resData[3] =  temp_c;	

	pMsgContext->resData[3] |= ru_eep_PROXI_Cfg.s.PROXI_CRC.s.Digit4;


	temp_c = 0;

	temp_c = ru_eep_PROXI_Cfg.s.PROXI_CRC.s.Digit3;
    temp_c <<= 4;

  	pMsgContext->resData[4] =  temp_c;	

	pMsgContext->resData[4] |= ru_eep_PROXI_Cfg.s.PROXI_CRC.s.Digit2;


	temp_c = 0;

	temp_c = ru_eep_PROXI_Cfg.s.PROXI_CRC.s.Digit1;
    temp_c <<= 4;

  	pMsgContext->resData[5] =  temp_c;	

	pMsgContext->resData[5] |= NOT_USED_VAL;


  		/* Proxi vehicleline configuration */

  	pMsgContext->resData[6] = ru_eep_PROXI_Cfg.s.Vehicle_Line_Configuration;

  		/* Proxi Country Code */

  	pMsgContext->resData[7] = ru_eep_PROXI_Cfg.s.Country_Code;


	/* BYTE 9: BIT 0,1: BIT 2: BIT 3: BIT 4,5: BIT 6,7: */
	temp_c = 0; //Init the value.
	pMsgContext->resData[8] = temp_c; //Initialization

	/* Proxi Heated seat variant */
	temp_c = ru_eep_PROXI_Cfg.s.Heated_Seats_Variant;
	pMsgContext->resData[8] = temp_c;
/* Proxi Steering wheel present Bit 2 */
	temp_c = ru_eep_PROXI_Cfg.s.Steering_Wheel;
	temp_c <<=2;
	pMsgContext->resData[8] |= temp_c;

	/* Proxi Vented seats present Bit 3 */
	temp_c = NOT_USED_VAL;
	temp_c <<=3;
	pMsgContext->resData[8] |= temp_c;

	/* Seat Material Bit 4 */
	temp_c = ru_eep_PROXI_Cfg.s.Seat_Material;
	temp_c <<=4;
	pMsgContext->resData[8] |= temp_c;

	/* Seat Material Bit 6 */
	temp_c = ru_eep_PROXI_Cfg.s.Wheel_Material;
	temp_c <<=6;
	pMsgContext->resData[8] |= temp_c;

	/* BYTE 10: BIT 0: BIT 1: BIT 2-7 */
	temp_c = 0; //Init the value.
	pMsgContext->resData[9] = temp_c; //Initialization

	/* Proxi Driver side  */
	temp_c = ru_eep_PROXI_Cfg.s.Driver_Side;
	pMsgContext->resData[9] = temp_c;

	/* Remote start Bit 1 */
	temp_c = ru_eep_PROXI_Cfg.s.Remote_Start;
	temp_c <<=1;
	pMsgContext->resData[9] |= temp_c;

	pMsgContext->resData[10] = ru_eep_PROXI_Cfg.s.Model_Year;

  
	DataLength = 11;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, DataLength, pMsgContext);

}

/*  ********************************************************************************
 * Function ApplDescRead_222800_Heated_Seat_PWM_Calibration_Parameters_Front_Seats (Service request header:$22 $28 $0 )
 * 
 * $22 $2800 - Read Heated Seat Calibration Parameters PWM (EEPROM)
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescRead_222800_Heated_Seat_PWM_Calibration_Parameters_Front_Seats(DescMsgContext* pMsgContext)
{
	
	unsigned char hs_vehicle_line_offset_c= 0;
	
	/* Byte 0: LL1 PWM */
	/* Byte 1: HL1 PWM */
	/* Byte 2: HL2 PWM */
	/* Byte 3: HL3 PWM */
    EE_BlockRead(EE_HEAT_CAL_DATA_PF, (u_8Bit*)&hs_cal_prms);
    
	pMsgContext->resData[0] = hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].Pwm_c[0];
	pMsgContext->resData[1] = hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].Pwm_c[1];
	pMsgContext->resData[2] = hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].Pwm_c[2];
	pMsgContext->resData[3] = hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].Pwm_c[3];
			
	DataLength = 4;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, DataLength, pMsgContext);
}

/*  ********************************************************************************
 * Function name:ApplDescRead_222801_Vented_Seat_PWM_Calibration_Parameters (Service request header:$22 $28 $1 )
 * 
 * $22 $2801 - Read Vented Seat Calibration Parameters PWM (EEPROM)
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescRead_222801_Vented_Seat_PWM_Calibration_Parameters(DescMsgContext* pMsgContext)
{
		
	if ( (CSWM_config_c & CSWM_2VS_MASK_K) == CSWM_2VS_MASK_K)
	{
		/* Byte 0: Vent Level 1 PWM */
		/* Byte 1: Vent Level 3 PWM */
		pMsgContext->resData[0] = Vs_cal_prms.cals[0].PWM_a[VL_LOW];
		pMsgContext->resData[1] = Vs_cal_prms.cals[0].PWM_a[VL_HI - 1];
		DataLength = 2;
		Response = RESPONSE_OK;
	}
	else
	{
		DataLength = 1;
		Response = SUBFUNCTION_NOT_SUPPORTED;
	}
	diagF_DiagResponse(Response, DataLength, pMsgContext);
}

/*  ********************************************************************************
 * Function name:ApplDescRead_222802_Heated_Steering_Wheel_PWM_Calibration_Parameters (Service request header:$22 $28 $2 )
 * 
 * $22 $2802 - Read Heated steering Wheel Calibration Parameters PWM (EEPROM)
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescRead_222802_Heated_Steering_Wheel_PWM_Calibration_Parameters(DescMsgContext* pMsgContext)
{

	
	if ( (CSWM_config_c & CSWM_HSW_MASK_K) == CSWM_HSW_MASK_K)
	{
		/* Byte 0,1,2 and 3: WL1,WL2,WL3 and WL4 PWM's */
		pMsgContext->resData[0] = stW_prms.cals[stW_cfg_c+stW_veh_offset_c].Pwm_a[WL1];
		pMsgContext->resData[1] = stW_prms.cals[stW_cfg_c+stW_veh_offset_c].Pwm_a[WL2];
		pMsgContext->resData[2] = stW_prms.cals[stW_cfg_c+stW_veh_offset_c].Pwm_a[WL3];
		pMsgContext->resData[3] = stW_prms.cals[stW_cfg_c+stW_veh_offset_c].Pwm_a[WL4];
		DataLength = 4;
		Response = RESPONSE_OK;
	}
	else
	{
		DataLength = 1;
		Response = SUBFUNCTION_NOT_SUPPORTED;
	}
	diagF_DiagResponse(Response, DataLength, pMsgContext);
}

/*  ********************************************************************************
 * Function name:ApplDescRead_222803_Heated_Seat_Max_Timeout_Parameters_Front_Seats (Service request header:$22 $28 $3 )
 * Description: not available 
 * 
 * $22 $2803 - Read Heated Seat Maximum Timeout Parameters (EEPROM)
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescRead_222803_Heated_Seat_Max_Timeout_Parameters_Front_Seats(DescMsgContext* pMsgContext)
{
	unsigned char hs_vehicle_line_offset_c= 0;
		
	/* Byte 0: LL1 PWM */
	/* Byte 1: HL1 PWM */
	/* Byte 2: HL2 PWM */
	/* Byte 3: HL3 PWM */
	pMsgContext->resData[0] = hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].Max_TimeOut_c[0];
	pMsgContext->resData[1] = hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].Max_TimeOut_c[1];
	pMsgContext->resData[2] = hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].Max_TimeOut_c[2];
	pMsgContext->resData[3] = hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].Max_TimeOut_c[3];
			
	DataLength = 4;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, DataLength, pMsgContext);
}

/*  ********************************************************************************
 * Function name:ApplDescRead_222804_Heated_Steering_Wheel_Max_Timeout_Parameters (Service request header:$22 $28 $4 )
 * 
 * $22 $2804 - Read Heated steering wheel MAX timeout parameters (EEPROM)
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescRead_222804_Heated_Steering_Wheel_Max_Timeout_Parameters(DescMsgContext* pMsgContext)
{
		
	if ( (CSWM_config_c & CSWM_HSW_MASK_K) == CSWM_HSW_MASK_K)
	{
		/* Byte 0,1,2 and 3: WL1,WL2,WL3 and WL4 Max Timeout's */
		pMsgContext->resData[0] = stW_prms.cals[stW_cfg_c+stW_veh_offset_c].TimeOut_c[WL1];
		pMsgContext->resData[1] = stW_prms.cals[stW_cfg_c+stW_veh_offset_c].TimeOut_c[WL2];
		pMsgContext->resData[2] = stW_prms.cals[stW_cfg_c+stW_veh_offset_c].TimeOut_c[WL3];
		pMsgContext->resData[3] = stW_prms.cals[stW_cfg_c+stW_veh_offset_c].TimeOut_c[WL4];
		DataLength = 4;
		Response = RESPONSE_OK;
	}
	else
	{
		DataLength = 1;
		Response = SUBFUNCTION_NOT_SUPPORTED;
	}
	diagF_DiagResponse(Response, DataLength, pMsgContext);
}

/*  ********************************************************************************
 * Function name:ApplDescRead_222805_Voltage_Level_ON (Service request header:$22 $28 $5 )
 * 
 * $22 $2805 - Read Voltage Level ON (EEPROM)
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescRead_222805_Voltage_Level_ON(DescMsgContext* pMsgContext)
{
		
	pMsgContext->resData[0] = BattV_cals.Thrshld_a[2];
			
	DataLength = 1;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, DataLength, pMsgContext);
}

/*  ********************************************************************************
 * Function name:ApplDescRead_222806_Voltage_Level_OFF (Service request header:$22 $28 $6 )
 * Description: not available 
 * 
 * $22 $2806 - Read Voltage Level OFF (EEPROM)
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescRead_222806_Voltage_Level_OFF(DescMsgContext* pMsgContext)
{
		
	pMsgContext->resData[0] = BattV_cals.Thrshld_a[3];

	DataLength = 1;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, DataLength, pMsgContext);
}

/*  ********************************************************************************
 * Function ApplDescRead_222807_PCB_Over_Temperature_Condition_Status (Service request header:$22 $28 $7 )
 * 
 * $22 $2807 - Read PCB Over Temperature condition status (EEPROM)
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescRead_222807_PCB_Over_Temperature_Condition_Status(DescMsgContext* pMsgContext)
{
  		
	EE_BlockRead(EE_OVER_TEMP_FAILSAFE, &pMsgContext->resData[0]);
//	EE_BlockRead(EE_PCB_OTEMP_ODOSTAMP_LATEST, &pMsgContext->resData[2]);
//	EE_BlockRead(EE_PCB_OTEMP_COUNTER, &pMsgContext->resData[4]);
//	EE_BlockRead(EE_PCB_OVERTEMP_STAT, &pMsgContext->resData[5]);

	DataLength = 6;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, DataLength, pMsgContext);
}

/*  ********************************************************************************
 * Function name:ApplDescRead_222808_Heated_Seat_NTC_Cutoff_Parameters_Front_Seats (Service request header:$22 $28 $8 )
 * Description: not available 
 * 
 * $22 $2808 - Read Heated Seat NTC Cutoff parameters
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescRead_222808_Heated_Seat_NTC_Cutoff_Parameters_Front_Seats(DescMsgContext* pMsgContext)
{
	unsigned char hs_vehicle_line_offset_c= 0;
	/* Heaters NTC cut off Readings from EEPROM */
		
	/* Read the NTC Reading cut off values from EEPROM Data */
	/* 0,49,55,55,70c                                       */
	pMsgContext->resData[0] = TempF_NtcAdStd_to_CanTemp(hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].NTC_CutOFF_c[0]); //PWM 1 No cut off
	pMsgContext->resData[1] = TempF_NtcAdStd_to_CanTemp(hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].NTC_CutOFF_c[1]); //PWM 1 No cut off
	pMsgContext->resData[2] = TempF_NtcAdStd_to_CanTemp(hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].NTC_CutOFF_c[2]); //PWM 1 No cut off
	pMsgContext->resData[3] = TempF_NtcAdStd_to_CanTemp(hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].NTC_CutOFF_c[3]); //PWM 1 No cut off
	pMsgContext->resData[4] = TempF_NtcAdStd_to_CanTemp(hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].NTC_CutOFF_c[4]); //PWM 1 No cut off
			
	DataLength = 5;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, DataLength, pMsgContext);
}

/*  ********************************************************************************
 * Function name:ApplDescRead_222809_Steering_Wheel_Temperature_Cut_off_Parameters (Service request header:$22 $28 $9 )
 * 
 * $22 $2809 - Read Heated steering wheel Temperature cut off parameters
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescRead_222809_Steering_Wheel_Temperature_Cut_off_Parameters(DescMsgContext* pMsgContext)
{
		
	if ( (CSWM_config_c & CSWM_HSW_MASK_K) == CSWM_HSW_MASK_K)
	{
		/* Byte 0,1,2,3 abd 4: WL1,WL2,WL3,WL4 and Max cut off temp */
		pMsgContext->resData[0] = stW_prms.cals[stW_cfg_c+stW_veh_offset_c].WL1Temp_Threshld;; 
		pMsgContext->resData[1] = stW_prms.cals[stW_cfg_c+stW_veh_offset_c].WL2Temp_Threshld;
		pMsgContext->resData[2] = stW_prms.cals[stW_cfg_c+stW_veh_offset_c].WL3Temp_Threshld;
		pMsgContext->resData[3] = stW_prms.cals[stW_cfg_c+stW_veh_offset_c].WL4Temp_Threshld;
		pMsgContext->resData[4] = stW_prms.cals[stW_cfg_c+stW_veh_offset_c].MaxTemp_Threshld;
		DataLength = 5;
		Response = RESPONSE_OK;
	}
	else
	{
		DataLength = 1;
		Response = SUBFUNCTION_NOT_SUPPORTED;
	}
	diagF_DiagResponse(Response, DataLength, pMsgContext);
}

/*  ********************************************************************************
 * Function name:ApplDescRead_22280A_HeatVent_Auto_Activation_Temperature_Calibration_Paramet (Service request header:$22 $28 $A )
 * 
 * $22 $280A - Read Heat/Vent Auto activation temperature calibration parameters
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescRead_22280A_HeatVent_Auto_Activation_Temperature_Calibration_Paramet(DescMsgContext* pMsgContext)
{
		
	/* Byte 0 and 1: Heat Amb temp limit */
	/* Byte 2 and 3: Vent Amb temp limit */
	pMsgContext->resData[0] = 0x00;
	EE_BlockRead(EE_REMOTE_CTRL_HEAT_AMBTEMP, &pMsgContext->resData[1]);
	pMsgContext->resData[2] = 0x00;
	EE_BlockRead(EE_REMOTE_CTRL_VENT_AMBTEMP, &pMsgContext->resData[3]);
	DataLength = 4;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, DataLength, pMsgContext);
}

/*  ********************************************************************************
 * Function name:ApplDescRead_22280C_Heated_Steering_Wheel_Min_Timeout_Parameters (Service request header:$22 $28 $C )
 * 
 * $22 $280C - Read Heated steering Wheel Parameters MIN timeouts
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescRead_22280C_Heated_Steering_Wheel_Min_Timeout_Parameters(DescMsgContext* pMsgContext)
{
		
	pMsgContext->resData[0] = stW_prms.cals[stW_cfg_c+stW_veh_offset_c].WL1TempChck_Time;; 
	pMsgContext->resData[1] = stW_prms.cals[stW_cfg_c+stW_veh_offset_c].WL2TempChck_Time;
	pMsgContext->resData[2] = stW_prms.cals[stW_cfg_c+stW_veh_offset_c].WL3TempChck_Time;
	pMsgContext->resData[3] = stW_prms.cals[stW_cfg_c+stW_veh_offset_c].WL4TempChck_Time;
	DataLength = 4;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, DataLength, pMsgContext);
}

/*  ********************************************************************************
 * Function ApplDescRead_22280D_Load_Shed_Fault_Status (Service request header:$22 $28 $D )
 * 
 * $22 $280D - Read Loadshed Fault Status..NOT REQUIRED
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescRead_22280D_Load_Shed_Fault_Status(DescMsgContext* pMsgContext)
{
      /* Load Shed Fault Status MKS 47356*/
		
			
	/* Read Latest Load Shed signal from Bus Byte 1*/
	/* Byte 2 and 3: Last Known Load shed 2 apeearance ODO stamp */
	/* Byte 4 and 5: Last Known Load shed 4 apeearance ODO Stamp */
	pMsgContext->resData[0] = canio_LOAD_SHED_c; // Load shed status from Bus
//	EE_BlockRead(EE_LOADSHED2_ODOSTAMP, &pMsgContext->resData[1]);
//	EE_BlockRead(EE_LOADSHED4_ODOSTAMP, &pMsgContext->resData[3]);
	DataLength = 5;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, DataLength, pMsgContext);
}

/*  ********************************************************************************
 * Function name:ApplDescRead_22280E_Heated_Seat_PWM_Calibration_Parameters_Rear_Seats (Service request header:$22 $28 $E )
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescRead_22280E_Heated_Seat_PWM_Calibration_Parameters_Rear_Seats(DescMsgContext* pMsgContext)
{
	unsigned char hs_vehicle_line_offset_c= 1;
	
    //Grab the values incase Rear heated seats present and from HW Switch inputs
	if( ((CSWM_config_c & CSWM_4HS_VARIANT_MASK_K) == CSWM_4HS_VARIANT_MASK_K) && (main_SwReq_Rear_c == VEH_SWITCH_REAR_ONLY_K) )
	{
		/* Byte 0: LL1 PWM */
		/* Byte 1: HL1 PWM */
		/* Byte 2: HL2 PWM */
		/* Byte 3: HL3 PWM */
    	EE_BlockRead(EE_HEAT_CAL_DATA_PF, (u_8Bit*)&hs_cal_prms);
    
		pMsgContext->resData[0] = hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].Pwm_c[0];
		pMsgContext->resData[1] = hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].Pwm_c[1];
		pMsgContext->resData[2] = hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].Pwm_c[2];
		pMsgContext->resData[3] = hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].Pwm_c[3];
			
		DataLength = 4;
		Response = RESPONSE_OK;

	}
	else
	{
       	DataLength = 1;
	   	Response = SUBFUNCTION_NOT_SUPPORTED;
    }


	diagF_DiagResponse(Response, DataLength, pMsgContext);
}


/*  ********************************************************************************
 * Function name:ApplDescRead_22280F_Heated_Seat_Max_Timeout_Parameters_Rear_Seats (Service request header:$22 $28 $F )
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescRead_22280F_Heated_Seat_Max_Timeout_Parameters_Rear_Seats(DescMsgContext* pMsgContext)
{
	unsigned char hs_vehicle_line_offset_c= 1;
		
    //Grab the values incase Rear heated seats present and from HW Switch inputs
	if( ((CSWM_config_c & CSWM_4HS_VARIANT_MASK_K) == CSWM_4HS_VARIANT_MASK_K) && (main_SwReq_Rear_c == VEH_SWITCH_REAR_ONLY_K) )
	{
		/* Byte 0: LL1 PWM */
		/* Byte 1: HL1 PWM */
		/* Byte 2: HL2 PWM */
		/* Byte 3: HL3 PWM */
		pMsgContext->resData[0] = hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].Max_TimeOut_c[0];
		pMsgContext->resData[1] = hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].Max_TimeOut_c[1];
		pMsgContext->resData[2] = hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].Max_TimeOut_c[2];
		pMsgContext->resData[3] = hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].Max_TimeOut_c[3];
			
		DataLength = 4;
		Response = RESPONSE_OK;

	}
	else
	{
       	DataLength = 1;
	   	Response = SUBFUNCTION_NOT_SUPPORTED;
    }

	diagF_DiagResponse(Response, DataLength, pMsgContext);
}


/*  ********************************************************************************
 * Function name:ApplDescRead_222810_Heated_Seat_NTC_Cutoff_Parameters_Rear_Seats (Service request header:$22 $28 $10 )
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescRead_222810_Heated_Seat_NTC_Cutoff_Parameters_Rear_Seats(DescMsgContext* pMsgContext)
{
	unsigned char hs_vehicle_line_offset_c= 1;
	/* Heaters NTC cut off Readings from EEPROM */
		
    //Grab the values incase Rear heated seats present and from HW Switch inputs
	if( ((CSWM_config_c & CSWM_4HS_VARIANT_MASK_K) == CSWM_4HS_VARIANT_MASK_K) && (main_SwReq_Rear_c == VEH_SWITCH_REAR_ONLY_K) )
	{
		/* Read the NTC Reading cut off values from EEPROM Data */
		/* 0,49,55,55,70c                                       */
		pMsgContext->resData[0] = TempF_NtcAdStd_to_CanTemp(hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].NTC_CutOFF_c[0]); //PWM 1 No cut off
		pMsgContext->resData[1] = TempF_NtcAdStd_to_CanTemp(hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].NTC_CutOFF_c[1]); //PWM 1 No cut off
		pMsgContext->resData[2] = TempF_NtcAdStd_to_CanTemp(hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].NTC_CutOFF_c[2]); //PWM 1 No cut off
		pMsgContext->resData[3] = TempF_NtcAdStd_to_CanTemp(hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].NTC_CutOFF_c[3]); //PWM 1 No cut off
		pMsgContext->resData[4] = TempF_NtcAdStd_to_CanTemp(hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].NTC_CutOFF_c[4]); //PWM 1 No cut off
			
		DataLength = 5;
		Response = RESPONSE_OK;
		DataLength = 4;
		Response = RESPONSE_OK;

	}
	else
	{
       	DataLength = 1;
	   	Response = SUBFUNCTION_NOT_SUPPORTED;
    }

	diagF_DiagResponse(Response, DataLength, pMsgContext);
}


/*  ********************************************************************************
 * Function name:ApplDescRead_223000_Program_ECU_Data_at_Flash_Station_1 (Service request header:$22 $30 $0 )
 * 22 3000 READ TEMIC BOM NUMBER -- 15 Byte length -- EOL SERVICES
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescRead_223000_Program_ECU_Data_at_Flash_Station_1(DescMsgContext* pMsgContext)
{
	if(diag_eol_io_lock_b)
	{
		EE_BlockRead(EE_BOARD_TEMIC_BOM_NUMBER, &pMsgContext->resData[0]);
		DataLength = 15;
		Response = RESPONSE_OK;
	}
	else
	{
		DataLength = 1;
		Response = CONDITIONS_NOT_CORRECT;
	}
	
	diagF_DiagResponse(Response, DataLength, pMsgContext);
	
}

/*  ********************************************************************************
 * Function name:ApplDescRead_223001_Program_ECU_Data_at_Flash_Station_2 (Service request header:$22 $30 $1 )
 * 22 3001 READ CONTROL BYTE FINAL - 1 Byte Length -- EOL SERVICES
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescRead_223001_Program_ECU_Data_at_Flash_Station_2(DescMsgContext* pMsgContext)
{
	if(diag_eol_io_lock_b)
	{
		EE_BlockRead(EE_CTRL_BYTE_FINAL, &pMsgContext->resData[0]);
		DataLength = 1;
		Response = RESPONSE_OK;
	}
	else
	{
		DataLength = 1;
		Response = CONDITIONS_NOT_CORRECT;
	}
	
	diagF_DiagResponse(Response, DataLength, pMsgContext);
}


/*  ********************************************************************************
 * Function name:ApplDescReadReadWrite (Service request header:$22 $30 $2 )
 * 22 3002 READ FLASHSTATION DATA (Total 50 Bytes) -- EOL SERVICE READ DATA
 * Internal EOL Test Data..Right now with 35 bytes of data
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescReadReadWrite(DescMsgContext* pMsgContext)
{
	if(diag_eol_io_lock_b)
	{

		unsigned char *pChar;
		u_16Bit  diag_vs_StrtUpTm_w = 0;
		/* Read the Rom Test checksum and Results */
		pMsgContext->resData[0] = HIGHBYTE(ROMTst_checksum); //High byte of Rom CHecksum
		pMsgContext->resData[1] = LOWBYTE(ROMTst_checksum);  //Low byte of Rom CHecksum
		pMsgContext->resData[2] = ROMTst_result;  	         // Rom Test Result:0-> Not tested; 1-> Result OK; 2-> Not OK; 3-> Undefined
	
		pMsgContext->resData[3] = main_variant_select_c; // Variant selection from Hardware pins
	
		pMsgContext->resData[4] = (unsigned char) Dio_ReadChannel(REL_A_MON); //Relay A  Status
		pMsgContext->resData[5] = (unsigned char) Dio_ReadChannel(REL_B_MON); //Relay B  Status
	 	pMsgContext->resData[6] = stW_Vsense_LS_c; //Vsense at LOW Side
	 	pMsgContext->resData[7] = (unsigned char) Dio_ReadChannel(REL_STW_EN); //Relay C ENable PIN)
	
	 	pMsgContext->resData[8] = temperature_NTC_supplyV; //Read U12S PIN
	 	pMsgContext->resData[9] = BattVF_Get_Crt_Uint(); //Read UINIT
		pMsgContext->resData[10] = (u_8Bit) (temperature_PCB_ADC>>8);    // Read PCB Temp High Byte
		pMsgContext->resData[11] = (u_8Bit) (temperature_PCB_ADC);       // Read PCB Temp Low Byte 
	
		pMsgContext->resData[12] = hs_Vsense_c[FRONT_LEFT]; // F L Seat Vsense AD Reading
		pMsgContext->resData[13] = hs_Vsense_c[FRONT_RIGHT]; // F R Seat Vsense AD Reading
		pMsgContext->resData[14] = hs_Vsense_c[REAR_LEFT];  // R L Seat Vsense AD Reading
		pMsgContext->resData[15] = hs_Vsense_c[REAR_RIGHT];  // R R Seat Vsense AD Reading
	
		pMsgContext->resData[16] = stW_Vsense_HS_c;         //Steering Wheel High Side Vsense
		pMsgContext->resData[17] = stW_Isense_c;			//Steering Wheel High Side Isense
	
		/* Vent Isense AD readings (displayed in hex) */
		pMsgContext->resData[18] = vs_Vsense_c[VS_FL]; // VL Vsense 
		pMsgContext->resData[19] = vs_Vsense_c[VS_FR]; // VR Vsense
		pMsgContext->resData[20] = vs_Isense_c[VS_FL]; // VL Isense
		pMsgContext->resData[21] = vs_Isense_c[VS_FR]; // VR Isense
	
		/* Read the Read Rampup value from EEPROM (2 bytes) */
		diag_vs_StrtUpTm_w = Vs_flt_cal_prms.start_up_tm;	
		pMsgContext->resData[22] = (u_8Bit) HIGHBYTE(diag_vs_StrtUpTm_w); /* Store High byte */
		pMsgContext->resData[23] = (u_8Bit) LOWBYTE(diag_vs_StrtUpTm_w); /* Store Low Byte */
	
		diag_vs_StrtUpTm_w = vsF_Modify_StartUp_Tm(); //Turn OFF Vent RampUp
		pMsgContext->resData[24] = (u_8Bit) HIGHBYTE(diag_vs_StrtUpTm_w); //Store High byte
		pMsgContext->resData[25] = (u_8Bit) LOWBYTE(diag_vs_StrtUpTm_w);  //Store LOW Byte
	
		pMsgContext->resData[26] = (unsigned char) Dio_ReadChannel(IGN_REAR_MON);
		pMsgContext->resData[27] = (u_8Bit) Dio_ReadChannel(IGN_WHEEL_MON);
		
		pMsgContext->resData[28] = (u_8Bit) Dio_ReadChannel(RL_LED_SW_DETECT);
		pMsgContext->resData[29] = (u_8Bit) Dio_ReadChannel(RR_LED_SW_DETECT);
	
		pChar = (unsigned char *)((unsigned int *)(0x004000));
		//pChar = (unsigned char *)(*(unsigned int *)(&pMsgContext->reqData[3]));
		memcpy( &pMsgContext->resData[30],pChar,5); //To store, from , total length
		//pMsgContext->resData[30] = *pChar;
		
		DataLength = 50;
		Response = RESPONSE_OK;
	}
	else
	{
		DataLength = 1;
		Response = CONDITIONS_NOT_CORRECT;
	}
	
	diagF_DiagResponse(Response, DataLength, pMsgContext);
}


/*  ********************************************************************************
 * Function ApplDescRead_22F100_Active_Diagnostic_Information (Service request header:$22 $F1 $0 )
 * 
 * $22 $F100 - Read Active Diagnsotic Information
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescRead_22F100_Active_Diagnostic_Information(DescMsgContext* pMsgContext)
{
  
    /* Byte 0: Active Diagnostic status of ECU                                 */
    /*         Bit 0   - ECU Software Mode (0 = Application, 1 = Boot)         */
	/*         Bit 1   - Gateway(0 = ECU is not a Gateway, 1 = ECU is Gateway) */
	/*         Bit 2-7 - Reserved (Set to Zero)                                */
	/*                                                                         */
	/* Byte 1: Active Diagnostic Variant 60                                    */
	/* Byte 2: Active Diagnostic Version 00                                    */
	/* Byte 3: Active Diagnostic Session                                       */
	/* Byte 4: Reserved                                                        */

	pMsgContext->resData[0] = 0x00; //Active Diagnostic Status of ECU 
	pMsgContext->resData[1] = 0x60; //Diagnostic variant
	pMsgContext->resData[2] = 0x00; //Diagnostic version
	pMsgContext->resData[3] = DescGetSessionIdOfSessionState(DescGetStateSession());
	pMsgContext->resData[4] = 0x00; //Reserved byte

	DataLength = 5;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, DataLength, pMsgContext);
}


/*  ********************************************************************************
 * Function ApplDescRead_22F10B_ECU_Configuration (Service request header:$22 $F1 $B )
 * 
 * $22 $F10B - Read ECU Configuration
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescRead_22F10B_ECU_Configuration(DescMsgContext* pMsgContext)
{
  
    /* Byte 0: ECU Configuration:                                           */
	/*         Bit 0: Front Left Heater  (1 = Supported, 0 = Not Supported) */
	/*         Bit 1: Front Right Heater (1 = Supported, 0 = Not Supported) */
	/*         Bit 2: Rear Left Heater   (1 = Supported, 0 = Not Supported) */
	/*         Bit 3: Rear Right Heater  (1 = Supported, 0 = Not Supported) */
	/*         Bit 4: Front Left Vent    (1 = Supported, 0 = Not Supported) */
	/*         Bit 5: Front Right Vent   (1 = Supported, 0 = Not Supported) */
	/*         Bit 6: Steering Wheel     (1 = Supported, 0 = Not Supported) */
	/*         Bit 7: Reserved           (Set to Zero)                      */
		
    /* Byte 1: Proxi seat type and wheel type: 								*/
	/*         Bit 0: Proxi Seat Type - Cloth (1 = True, 0 = False)         */
	/*         Bit 1: Proxi Seat Type - Perforeated Leather (1 = True, 0 = False)         */
	/*         Bit 2: Proxi Seat Type - Base Leather (1 = True, 0 = False)  */
	/*         Bit 3: Proxi Wheel Type - Wood/Leather (1 = True, 0 = False) */
	/*         Bit 4: Proxi Wheel Type - Leather Only (1 = True, 0 = False) */
	/*         Bit 5: Reserved           (Set to Zero)                      */
    diag_srvc_22F10B_byte0 = 0;
    diag_srvc_22F10B_byte1 = 0;
    diag_srvc_22F10B_byte2 = 0;
		

	    
    /* Byte 1, Bit 1: FRONT HEATER PRESENT*/
	if (CSWM_config_c & CSWM_2HS_VARIANT_1_K) {
		 diag_srvc_22F10B_byte0 |= FRONT_HEAT_PRESENT;
	}else{
		 diag_srvc_22F10B_byte0	&= (~FRONT_HEAT_PRESENT);
	}

    /* Byte 1, Bit 2: FRONT AND REAR HEATERS PRESENT*/
	if (CSWM_config_c & REAR_HEATER_MASK_K) {
		 diag_srvc_22F10B_byte0 |= FRONT_REAR_HEAT_PRESENT;
	}else{
		 diag_srvc_22F10B_byte0	&= (~FRONT_REAR_HEAT_PRESENT);
	}

    /* Byte 1, Bit 3: VENTS PRESENT*/
	if (CSWM_config_c & CSWM_2VS_MASK_K) {
		 diag_srvc_22F10B_byte0 |= VENT_PRESENT;
	}else{
		 diag_srvc_22F10B_byte0	&= (~VENT_PRESENT);
	}

    /* Byte 1, Bit 4: STW PRESENT*/
	if (CSWM_config_c & CSWM_HSW_MASK_K) {
		 diag_srvc_22F10B_byte0 |= STW_PRESENT;
	}else{
		 diag_srvc_22F10B_byte0	&= (~STW_PRESENT);
	}
    /*Variant Info*/
	pMsgContext->resData[0] = diag_srvc_22F10B_byte0; 


    /* Byte 2, Bit 1: Seat Material = Cloth*/
	if (ru_eep_PROXI_Cfg.s.Seat_Material == PROXI_SEAT_TYPE_CLOTH) {
		 diag_srvc_22F10B_byte1 |= SEAT_TYPE_CLOTH;
	}else{
		 diag_srvc_22F10B_byte1	&= (~SEAT_TYPE_CLOTH);
	}


    /* Bit 3: Seat Material = perf leather*/
	if (ru_eep_PROXI_Cfg.s.Seat_Material == PROXI_SEAT_TYPE_PERF) {
		 diag_srvc_22F10B_byte1 |= SEAT_TYPE_PERF_LEATHER;
	}else{
		 diag_srvc_22F10B_byte1	&= (~SEAT_TYPE_PERF_LEATHER);
	}

    /* Bit 4: Seat Material = leather*/
	if (ru_eep_PROXI_Cfg.s.Seat_Material == PROXI_SEAT_TYPE_LEATHER) {
		 diag_srvc_22F10B_byte1 |= SEAT_TYPE_LEATHER;
	}else{
		 diag_srvc_22F10B_byte1	&= (~SEAT_TYPE_LEATHER);
	}

    /* Bit 4: Wheel Material = Wood*/
	if (ru_eep_PROXI_Cfg.s.Wheel_Material == PROXI_WHEEL_TYPE_WOOD) {
		 diag_srvc_22F10B_byte1 |= WHEEL_TYPE_WOOD;
	}else{
		 diag_srvc_22F10B_byte1	&= (~WHEEL_TYPE_WOOD);
	}

    /* Bit 5: Wheel Material = Leather*/
	if (ru_eep_PROXI_Cfg.s.Wheel_Material == PROXI_WHEEL_TYPE_LEATHER) {
		 diag_srvc_22F10B_byte1 |= WHEEL_TYPE_LEATHER;
	}else{
		 diag_srvc_22F10B_byte1	&= (~WHEEL_TYPE_LEATHER);
	}

     /* Byte 3, Bit 1: Heated Seats Present*/
	if ( (ru_eep_PROXI_Cfg.s.Heated_Seats_Variant == PROXI_HEATSEAT_FRONT)||  (ru_eep_PROXI_Cfg.s.Heated_Seats_Variant == PROXI_HEATSEAT_FRONT_REAR) ) {
		 diag_srvc_22F10B_byte2 |= HEATSEAT_FRONT_P;
	}else{
		 diag_srvc_22F10B_byte2	&= (~HEATSEAT_FRONT_P);
	}

     /* Bit 2: Heated Seats Present*/
	if (ru_eep_PROXI_Cfg.s.Heated_Seats_Variant == PROXI_HEATSEAT_FRONT_REAR) {
		 diag_srvc_22F10B_byte2 |= HEATSEAT_FRONT_REAR_P;
	}else{
		 diag_srvc_22F10B_byte2	&= (~HEATSEAT_FRONT_REAR_P);
	}


     /* Bit 3: Heated Seats Present  Variant 4 2FH + STW*/
		 diag_srvc_22F10B_byte2	&= (~VENTSEAT_FRONT_P);

     /* Bit 4: Heated Seats Present*/
	if (ru_eep_PROXI_Cfg.s.Steering_Wheel == PROXI_WHEEL_PRESENT) {
		 diag_srvc_22F10B_byte2 |= STEERING_WHEEL_P;
	}else{
		 diag_srvc_22F10B_byte2	&= (~STEERING_WHEEL_P);
	}

	pMsgContext->resData[1] = diag_srvc_22F10B_byte1;                                    
	pMsgContext->resData[2] = diag_srvc_22F10B_byte2;
	pMsgContext->resData[3] = hardware_proxi_mismatch_c;	 	// Hardware / Proxi Mismatch



	DataLength = 4;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, DataLength, pMsgContext);}

/*  ********************************************************************************
 * Function ApplDescRead_22F112_Hardware_Part_Number (Service request header:$22 $F1 $12 )
 * 
 * $22 $F112 - Read Hardware Part Number
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescRead_22F112_Hardware_Part_Number(DescMsgContext* pMsgContext)
{
    /* 10 Byte ASCII */
	//EE_BlockRead(EE_FBL_FIAT_ECU_HW_NO, &pMsgContext->resData[0]);
	EE_BlockRead(EE_FBL_CHR_ECU_PNO, &pMsgContext->resData[0]); //ECU PNO, SW PNO and HW PNO all are same.
	

	DataLength = 10;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, DataLength, pMsgContext);
}


/*  ********************************************************************************
 * Function ApplDescRead_22F122_Software_Part_Number (Service request header:$22 $F1 $22 )
 * 
 * $22 $F122 - Read Software Part Number
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescRead_22F122_Software_Part_Number(DescMsgContext* pMsgContext)
{
  
    /* 10 Byte ASCII */
    EE_BlockRead(EE_FBL_CHR_APPL_SW_PNO, &pMsgContext->resData[0]);
			
	DataLength = 10;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, DataLength, pMsgContext);
}

/*  ********************************************************************************
 * Function ApplDescRead_22F132_ECU_Part_Number (Service request header:$22 $F1 $32 )
 * 
 * $22 $F132 - Read ECU Part Number
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescRead_22F132_ECU_Part_Number(DescMsgContext* pMsgContext)
{
  
    /* 10 Byte ASCII */
	EE_BlockRead(EE_FBL_CHR_ECU_PNO, &pMsgContext->resData[0]);
			
	DataLength = 10;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, DataLength, pMsgContext);
}

/*  ********************************************************************************
 * Function ApplDescReadBootSoftwareIdentification (Service request header:$22 $F1 $80 )
 * 
 * $22 $F180 - Read Boot Software Identification
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescReadBootSoftwareIdentification(DescMsgContext* pMsgContext)
{
  
    /* Byte 0:		Number Of Modules	(1 Byte)			*/
	/* Byte 1:  	Module ID			(1 Byte)			*/
	/* Byte 2-11:	Version Module ID	(10 Bytes)			*/
	/* Byte 12-13:	Supplier ID			(2 Bytes)			*/
			
	pMsgContext->resData[0] = 0x01; //Number of Modules
	EE_BlockRead(EE_FBL_BOOT_SW_IDENT, &pMsgContext->resData[1]); //BOOT SW IDENTIFICATION.

	DataLength = 14;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, DataLength, pMsgContext);
}

/*  ********************************************************************************
 * Function ApplDescReadApplicationSoftwareIdentification (Service request header:$22 $F1 $81 )
 * 
 * $22 $F181 - Read Application Software Identification
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescReadApplicationSoftwareIdentification(DescMsgContext* pMsgContext)
{
  		
    /* Byte 0:		Number Of Modules	(1 Byte)			*/
	/* Byte 1:  	Module ID			(1 Byte)			*/
	/* Byte 2-11:	Version Module ID	(10 Bytes)			*/
	/* Byte 12-13:	Supplier ID			(2 Bytes)			*/
			
	pMsgContext->resData[0] = 0x01; //Number of Modules
	EE_BlockRead(EE_FBL_APPL_SW_IDENT, &pMsgContext->resData[1]); //APPL SW IDENTIFICATION.

	DataLength = 14;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, DataLength, pMsgContext);
}

/*  ********************************************************************************
 * Function ApplDescReadApplicationDataIdentification (Service request header:$22 $F1 $82 )
 * 
 * $22 $F182 - Read Application Data Identification.
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescReadApplicationDataIdentification(DescMsgContext* pMsgContext)
{
  			
    /* Byte 0:		Number Of Modules	(1 Byte)			*/
	/* Byte 1:  	Module ID			(1 Byte)			*/
	/* Byte 2-11:	Version Module ID	(10 Bytes)			*/
	/* Byte 12-13:	Supplier ID			(2 Bytes)			*/
			
	pMsgContext->resData[0] = 0x01; //Number of Modules
	EE_BlockRead(EE_FBL_APPL_DATA_IDENT, &pMsgContext->resData[1]); //APPL SW IDENTIFICATION.

	DataLength = 14;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, DataLength, pMsgContext);
}

/*  ********************************************************************************
 * Function ApplDescReadBootSoftwareFingerprint (Service request header:$22 $F1 $83 )
 * 
 * $22 $F183 - Read Boot Software Fingerprint
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescReadBootSoftwareFingerprint(DescMsgContext* pMsgContext)
{
      
	/* Byte 0:  	Module ID			(1 Byte)			*/
	/* Byte 1-9:	Tester Code			(9 Bytes)			*/
	/* Byte 10:		Programing Date Year(1 Byte)			*/
	/* Byte 11:		Programing Date Month(1 Byte)			*/
	/* Byte 12:		Programing Date Date(1 Byte)			*/
			
	//EE_BlockRead(EE_FBL_BOOT_SW_FPRINT, &pMsgContext->resData[0]); 
	pMsgContext->resData[0] = 0x01; //Module ID
	pMsgContext->resData[1] = 0x20; 
	pMsgContext->resData[2] = 0x20; 
	pMsgContext->resData[3] = 0x20; 
	pMsgContext->resData[4] = 0x20; 
	pMsgContext->resData[5] = 0x20; 
	pMsgContext->resData[6] = 0x20; 
	pMsgContext->resData[7] = 0x20; 
	pMsgContext->resData[8] = 0x20; 
	pMsgContext->resData[9] = 0x20; 
	pMsgContext->resData[10] = 0x11; //Boot Program year
	pMsgContext->resData[11] = 0x08; //Boot Program Month 
	pMsgContext->resData[12] = 0x08; //Boot Program Date
	
	DataLength = 13;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, DataLength, pMsgContext);
}

/*  ********************************************************************************
 * Function ApplDescReadApplicationSoftwareFingerprint (Service request header:$22 $F1 $84 )
 * 
 * $22 $F184 - Read Application Software Fingerprint
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescReadApplicationSoftwareFingerprint(DescMsgContext* pMsgContext)
{
      
    /* Byte 0:  	Module ID			(1 Byte)			*/
	/* Byte 1-9:	Tester Code			(9 Bytes)			*/
	/* Byte 10:		Programing Date Year(1 Byte)			*/
	/* Byte 11:		Programing Date Month(1 Byte)			*/
	/* Byte 12:		Programing Date Date(1 Byte)			*/
			
	EE_BlockRead(EE_FBL_APPL_SW_FPRINT, &pMsgContext->resData[0]); //Tester Code + Pgm date
	DataLength = 13;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, DataLength, pMsgContext);
}

/*  ********************************************************************************
 * Function ApplDescReadApplicationDataFingerprint (Service request header:$22 $F1 $85 )
 * 
 * $22 $F185 - Read Application Data Fingerprint
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescReadApplicationDataFingerprint(DescMsgContext* pMsgContext)
{
  		
    /* Byte 0:  	Module ID			(1 Byte)			*/
	/* Byte 1-9:	Tester Code			(9 Bytes)			*/
	/* Byte 10:		Programing Date Year(1 Byte)			*/
	/* Byte 11:		Programing Date Month(1 Byte)			*/
	/* Byte 12:		Programing Date Date(1 Byte)			*/
			
	EE_BlockRead(EE_FBL_APPL_DATA_FPRINT, &pMsgContext->resData[0]); //Tester Code + Pgm date
	DataLength = 13;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, DataLength, pMsgContext);
}

/*  ********************************************************************************
 * Function ApplDescReadActiveDiagnosticSessionDataIdentifier (Service request header:$22 $F1 $86 )
 * 
 * $22 $F186 - Read Active Diagnostic Session Data Identifier
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescReadActiveDiagnosticSessionDataIdentifier(DescMsgContext* pMsgContext)
{
 			
    /* Byte 0:  	Active Diag Session		(1 Byte) */
			
	pMsgContext->resData[0] = DescGetSessionIdOfSessionState(DescGetStateSession());;
	DataLength = 1;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, DataLength, pMsgContext);
}

/*  ********************************************************************************
 * Function ApplDescReadVehicleManufacturerSparePartNumber (Service request header:$22 $F1 $87 )
 * 
 * $22 $F187 - Read Vehicle Manufacturer Spare Part Number
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescReadVehicleManufacturerSparePartNumber(DescMsgContext* pMsgContext)
{
      
    /* Byte 0-10:		Vehicle Manufacturer Spare P/N (11 Bytes) */
			
	EE_BlockRead(EE_FBL_SPARE_PNO, &pMsgContext->resData[0]); //spare part number
	DataLength = 11;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, DataLength, pMsgContext);
}

/*  ********************************************************************************
 * Function ApplDescReadEcuSerialNumber (Service request header:$22 $F1 $8C )
 * 
 * $22 $F18C - Read ECU serial Number
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescReadEcuSerialNumber(DescMsgContext* pMsgContext)
{
			
    /* Byte 0-13:		(14 Bytes) */
			
	EE_BlockRead(EE_SERIAL_NUMBER, &pMsgContext->resData[0]);

	DataLength = 14;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, DataLength, pMsgContext);
}

/*  ********************************************************************************
 * Function ApplDescReadVIN (Service request header:$22 $F1 $90 )

 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescReadVIN(DescMsgContext* pMsgContext)
{
      /* $22 $F1 $90 */
      /* VIN Number  */
  		
      /* Byte 0-16: (17 Bytes) */
			
	EE_BlockRead(EE_VIN_ORIGINAL, &pMsgContext->resData[0]);
			
	DataLength = 17;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, DataLength, pMsgContext);
}

/*  ********************************************************************************
 * Function ApplDescReadSystemSupplierEcuHardwareNumber (Service request header:$22 $F1 $92 )
 
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescReadSystemSupplierEcuHardwareNumber(DescMsgContext* pMsgContext)
{
      /* $22 $F1 $92 */
      /* System Supplier ECU Hardware Number */
      
  		/* Byte 0-10:		System Supplier HW Number (11 Bytes)			*/
			
	/* Hardware Part Number (11 Byte ASCII) */
	EE_BlockRead(EE_FBL_FIAT_ECU_HW_NO, &pMsgContext->resData[0]);

	DataLength = 11;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, DataLength, pMsgContext);
}

/*  ********************************************************************************
 * Function ApplDescReadSystemSupplierEcuHardwareVersionNumber (Service request header:$22 $F1 $93 )

 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescReadSystemSupplierEcuHardwareVersionNumber(DescMsgContext* pMsgContext)
{
      /* $22 $F1 $93 */
      /* System Supplier ECU Hardware Version Number */
  		
      /* Byte 0:  	HW Version Number			(1 Byte)				*/
			
	EE_BlockRead(EE_FBL_FIAT_ECU_HW_VERSION, &pMsgContext->resData[0]);
	DataLength = 1;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, DataLength, pMsgContext);
}

/*  ********************************************************************************
 * Function ApplDescReadSystemSupplierEcuSoftwareNumber (Service request header:$22 $F1 $94 )

 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescReadSystemSupplierEcuSoftwareNumber(DescMsgContext* pMsgContext)
{
      /* $22 $F1 $94 */
      /* System Supplier ECU Software Number */
			
    /* Byte 0-10:		System Supplier SW Number (11 Bytes)			*/
	/* Hardware Part Number (11 Byte ASCII) */
	EE_BlockRead(EE_FBL_FIAT_ECU_SW_NO, &pMsgContext->resData[0]);

	DataLength = 11;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, DataLength, pMsgContext);
}


/*  ********************************************************************************
 * Function ApplDescReadSystemSupplierEcuSoftwareVersionNumber (Service request header:$22 $F1 $95 )
 
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescReadSystemSupplierEcuSoftwareVersionNumber(DescMsgContext* pMsgContext)
{
      /* $22 $F1 $95 */
      /* System Supplier ECU Software Version Number */
			
//	pMsgContext->resData[0] = 11;
//	pMsgContext->resData[1] = 22;
	EE_BlockRead(EE_FBL_FIAT_ECU_SW_VERSION, &pMsgContext->resData[0]);
	
	DataLength = 2;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, DataLength, pMsgContext);
}


/*  ********************************************************************************
 * Function ApplDescReadExhaustRegulationOrTypeApprovalNumber (Service request header:$22 $F1 $96 )

 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescReadExhaustRegulationOrTypeApprovalNumber(DescMsgContext* pMsgContext)
{
      /* $22 $F1 $96 */
      /* Exhaust Regulation or Type Approval Number */
      
    /* Byte 0-5:  	Exhaust Approval Number			(6 Byte)			*/
	pMsgContext->resData[0] = 0x20;
	pMsgContext->resData[1] = 0x20;
	pMsgContext->resData[2] = 0x20;
	pMsgContext->resData[3] = 0x20;
	pMsgContext->resData[4] = 0x20;
	pMsgContext->resData[5] = 0x20;

//	EE_BlockRead(EE_FBL_APPROVAL_NO, &pMsgContext->resData[0]);
	DataLength = 6;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, DataLength, pMsgContext);
}


/*  ********************************************************************************
 * Function ApplDescReadIdentificationCode (Service request header:$22 $F1 $A0 )
 
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescReadIdentificationCode(DescMsgContext* pMsgContext)
{
      /* $22 $F1 $A0 */
      /* Identification Code */
      
	/* Byte 0-10:		Vehicle Manufacturer Spare P/N (11 Bytes)		*/
	/* Byte 11-27:		VIN Number 					(17 Bytes)			*/
	/* Byte 28-38:		System Supplier HW Number 	(11 Bytes)			*/
	/* Byte 39:  	    HW Version Number			(1 Byte)			*/
	/* Byte 40-50:		System Supplier SW Number 	(11 Bytes)			*/
	/* Byte 51-52:  	SW Version Number			(2 Byte)			*/
	/* Byte 53-58:  	Exhaust Approval Number		(6 Byte)			*/
	/* Byte 59-63:  	ISO Code					(5 Byte)			*/
			
	EE_BlockRead(EE_FBL_SPARE_PNO, &pMsgContext->resData[0]); //spare part number
	EE_BlockRead(EE_VIN_ORIGINAL, &pMsgContext->resData[11]); //VIN Number
	EE_BlockRead(EE_FBL_FIAT_ECU_HW_NO, &pMsgContext->resData[28]); //HW Number
	EE_BlockRead(EE_FBL_FIAT_ECU_HW_VERSION, &pMsgContext->resData[39]); //HW Version
	EE_BlockRead(EE_FBL_FIAT_ECU_SW_NO, &pMsgContext->resData[40]); //SW PArt Number
	EE_BlockRead(EE_FBL_FIAT_ECU_SW_VERSION, &pMsgContext->resData[51]); //SW Version
	EE_BlockRead(EE_FBL_APPROVAL_NO, &pMsgContext->resData[53]); //Approval Number
	pMsgContext->resData[59] = 0x20; //ISO Code Not supported by Program
	pMsgContext->resData[60] = 0x20; //ISO Code Not supported by Program
	pMsgContext->resData[61] = 0x20; //ISO Code Not supported by Program
	pMsgContext->resData[62] = 0x20; //ISO Code Not supported by Program
	pMsgContext->resData[63] = 0x20; //ISO Code Not supported by Program

	DataLength = 64;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, DataLength, pMsgContext);			
}

/*  ********************************************************************************
 * Function ApplDescReadSincomAndFactory (Service request header:$22 $F1 $A4 )
 * 
 * According to SD12018 Rev_c, this service to be supported wit bytes set to $20 (BLANK ASCII)
 
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescReadSincomAndFactory(DescMsgContext* pMsgContext)
{
    /* Byte 0-2:  Sincom				(3 Byte)						*/
	/* Byte 3-4:	Factory				(2 Byte)						*/
	/* Byte 5:		Series				(1 Byte)						*/
			
	pMsgContext->resData[0] = 0x20;
	pMsgContext->resData[1] = 0x20;
	pMsgContext->resData[2] = 0x20;
	pMsgContext->resData[3] = 0x20;
	pMsgContext->resData[4] = 0x20;
	pMsgContext->resData[5] = 0x20;
	
	DataLength = 6;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, DataLength, pMsgContext);	
}


/*  ********************************************************************************
 * Function ApplDescReadISOCode (Service request header:$22 $F1 $A5 )
 * 
 * According to SD12018 Rev_c, this service to be supported wit bytes set to $20 (BLANK ASCII)
 
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescReadISOCode(DescMsgContext* pMsgContext)
{
      /* $22 $F1 $A5 */
      /* ISO Code */
      
	pMsgContext->resData[0] = 0x20;
	pMsgContext->resData[1] = 0x20;
	pMsgContext->resData[2] = 0x20;
	pMsgContext->resData[3] = 0x20;
	pMsgContext->resData[4] = 0x20;
	
	DataLength = 5;
	Response = RESPONSE_OK;
	diagF_DiagResponse(Response, DataLength, pMsgContext);	
}

#if defined (DESC_ENABLE_MULTI_CFG_SUPPORT)
/* ********************************************************************************
 * Function name:ApplDescIsDataIdSupported
 * Description: Additionaly reject a supported PID (multi ECU configuration)
 * Returns:  kDescTrue - if still supported, kDescFalse - if not supported
 * Parameter(s):The PID number
 * Particularitie(s) and limitation(s):
 *   - The function "DescProcessingDone" may not be called.
 *   - The function "DescSetNegResponse" may not be called.
 ******************************************************************************** */
DescBool ApplDescIsDataIdSupported(vuint16 pid)
{
  return kDescTrue;
}
#endif



