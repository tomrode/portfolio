#ifndef BATTV_H
#define BATTV_H

/********************************************************************************/
/*                        A U T H O R   I D E N T I T Y							*/
/********************************************************************************/
/* Initials      Name                   Company									*/
/* --------      --------------------   --------------------------------------- */ 
/* CK            Can Kulduk             Continental Automotive Systems			*/
/* HY            Harsha Yekollu         Continental Automotive Systems          */
/********************************************************************************/


/********************************************************************************/
/*                   CHANGE HISTORY for BATTV MODULE                            */
/********************************************************************************/
/* mm/dd/yyyy	User - Change comments											*/
/* 02.12.2007   CK   - Heater file created                                      */
/* 03-03-2008	HY	 - Copied from CSWM MY09 project     						*/
/* 11.24.2008   CK   - Load shed monitor status enum is added.                  */
/* 11.24.2008   CK   - Added BattV_Get_LdShd_Mnt_Stat function prototype        */
/* 12.08.2008   CK   - Updated erratic voltage mature time define to 3 seconds  */
/* 12.08.2008   CK   - Added a new define to clear the erratic voltage bit when */
/*                     the difference between system and battery voltage bit    */
/*                     drops below set threshold (default: 2.5V) minus 0.5V.    */
/* 01.23.2009   CK   - PC-Lint related updates.                                 */
/********************************************************************************/


/******************************* @Enumaration(s) ********************************/
/* Voltage Types */
typedef enum{
    SYSTEM_V=0,             // System (CAN) Voltage
    BATTERY_V=1,            // Battery (Local) Voltage 
    TYPE_LMT=2              // Voltage type array limit
}BattV_type_e;

/* Voltage Thresholds */
typedef enum{
    SET_OV_LMT=0,           // Set over voltage threshold
    CLR_OV_LMT=1,           // Clear over voltage threshold   
    CLR_UV_LMT=2,           // Clear under voltage threshold
    SET_UV_LMT=3,           // Set under voltage threshold
    INTERNAL_FLT_LMT=4,     // Internal fault limit 
    THRSHLD_LMT=5           // Voltage threshold array limit
}BattV_Thrshlds_e;

/* Voltage Timings */
typedef enum{
    SET_UV_DLY_TM=0,        // Delay time before setting preliminary under voltage fault
    FLT_MTR_TM=1,           // Voltage fault mature time
    FLT_DE_MTR_TM=2,        // Voltage fault de-mature time
    TM_LMT=3                // Voltage timings array limit
}BattV_Timing_e;

/* Voltage Status */
typedef enum{
    V_STAT_UNDEFINED=0,     // Voltage status undefined
    V_STAT_GOOD=1,          // Voltage status good
    V_STAT_BAD=2,           // Voltage status bad
    V_STAT_UGLY=3           // Voltage status ugly
}BattV_Stat_e;

/* Fault types */
typedef enum{
    UNDER_VOLTAGE_FAULT=0,  // Fault type under voltage       
    OVER_VOLTAGE_FAULT=1    // Fault type over voltage 
}BattV_Flt_Type_e;

/* Local Battery Voltage Drop compensation */
typedef enum{
	NO_OUTPUT_ON=0,				    // No Outputs are ON (no compensation 0.0V)
	ONE_OUTPUT_ON=1,                // Only one heater or one vent output is ON (0.4 V compensation)
	MULTIPLE_OUTPUTS_ON=2,			// Two, three or four outputs are ON (heater/vent) (0.7 V compensation)
	ONLY_STW_OUTPUT_ON=3,		    // Only Steering Wheel Output is ON (0.5 V compensation)
	STW_PLUS_TWO_OUTPUTS_ON=4,		// Steering wheel + 1/2 heater/vent outputs are ON (1.1 V compensation)
	STW_PLUS_FOUR_OUTPUTS_ON=5,		// Steering wheel + 3/4 heater/vent outputs are ON (1.3 V compensation)
	BATT_COMPENSATION_LMT=6			// Array limit
}BattV_Compensation_e;

/* Added on 11.24.2008 CK. Load Shed Monitor Status. Used in mainF_CSWM_Status function. */
typedef enum{
	LDSHD_MNT_GOOD=0,			// Outputs may turn ON along with their indicator LEDs
	LDSHD_MNT_BAD=1,			// Outputs may not turn ON, LED display for 60 seconds before maturing low voltage fault
	LDSHD_MNT_UGLY=2			// Outputs may not turn ON, 2 second LED display
}BattV_Ldshd_mnt_stat_e;
/********************************************************************************/


/******************************* @Constants/Defines *****************************/
/* Voltage Fault Status masks */
#define V_FLT_UNDEFINED       0         // fault undefined
#define PRL_OV_FLT            1         // least significant (0 bit) of voltage status
#define PRL_UV_FLT            2         // 1st bit of the voltage status
#define MTR_OV_FLT            4         // 2nd bit of the voltage status
#define MTR_UV_FLT            8         // 3rd bit of the voltage status
#define V_NO_FLT              16        // 4th bit of the voltage status

/* Timing */
#define BATTV_MDLTN            25        // 1 sec=25 counts in 40ms timeslice (battery time modulation)
#define BATTV_ERRATIC_V_TM     75        // 3 sec=75 counts in 40ms timeslice UPDATED on 12.08.2008
#define DELAY_2_SEC_CALL40MILI 50        //  Delay count  Change Request ID 32286  (50*40ms = 2 seconds)


/* New Define to clear Erratic Voltage Bit */
#define BATTV_HALF_A_VOLT      5        // Minimum Voltage recovery needed before we clear the erratic voltage bit

/* Write Data by LID */
#define BATTV_DATA_LENGHT      1        // Write Data by LID maximum lenght

/* Uint AD to Uint 100mV conversion */
#define BATTV_N42             42        // Constant used in Uint AD to Uint 100mV conversion
#define BATTV_N8               8        // Another constant used in AD to Uint 100mV conversion
#define BATTV_N7               7		// Replace constant 8 with 7 for CSWM MY 10
#define BATTV_N4               4		// 4 counts * 40 ms = 160ms wait time to update fault status when compensation changes

#define BATTV_COMPENSATION_SIZE 6		// Battery voltage compensation data size

/* Active Number of outputs */
#define BATTV_NO_OUTPUT_ACTIVE	0		// No output is active
#define BATTV_1_OUTPUT_ACTIVE	1		// One output is active
#define BATTV_2_OUTPUTS_ACTIVE  2		// Two ouptuts are active
#define BATTV_3_OUTPUTS_ACTIVE  3		// Three outputs are active
#define BATTV_4_OUTPUTS_ACTIVE  4		// Four outputs are active
/********************************************************************************/


/*************************** @Global Structures *********************************/

/* Only read access please for the elements of these structure. */
/* Voltage calibration data (8 bytes) */
typedef struct{
    u_8Bit Thrshld_a[THRSHLD_LMT];      // Voltage thresholds (5 bytes)
    u_8Bit Timing_a[TM_LMT];            // Voltage timings (3 bytes)
}BattV_s;

/* voltage calibration parameters */
extern BattV_s BattV_cals;

/* Battery data structure */
typedef struct{
    u_8Bit crt_vltg_c;                  // stores most recent voltage reading
    u_8Bit flt_stat_c;                  // stores the current voltage fault status
    u_8Bit prvs_flt_stat_c;             // stores the previous fault status
    u_8Bit status_c;                    // stores the voltage status (good, bad, ugly)
    u_16Bit set_UV_cnt_w;               // count for setting UV fault delay
    u_16Bit flt_mtr_cnt_w;              // count for fault mature time
    u_16Bit flt_de_mtr_cnt_w;           // count for fault de-mature time
}BattV_prms;

/* Battery parameter sets */
extern BattV_prms BattV_set[TYPE_LMT];         // Parameter sets (one for system voltage, one for battery voltage)
/********************************************************************************/


/****************************** @Function Prototypes ****************************/
/* For Main */
@far void BattVF_Init(void);
@far void BattVF_Manager(void);
@far void BattVF_AD_Rdg(void);
@far u_8Bit BattVF_Get_Status(BattV_type_e Voltage_type);
@far void BattVF_DTC_Manager(void);
@far u_8Bit BattVF_Get_LdShd_Mnt_Stat(void);		// To be used in mainF_CSWM_Status function

/* For Diagnose */
@far u_8Bit BattVF_Chck_Diag_Prms(u_8Bit BattV_LID_ser, u_8Bit *BattV_data_pc);
@far u_8Bit BattVF_Write_Diag_Data(u_8Bit BattV_service);

/* For Relay and Diagnose */
@far u_8Bit BattVF_Get_Crt_Uint(void);

/* For Rear Seat and NTC Supply voltage conversions to 100mV */
@far u_8Bit BattVF_AD_to_100mV_Cnvrsn(u_16Bit BattV_AD_value);
/********************************************************************************/


#endif