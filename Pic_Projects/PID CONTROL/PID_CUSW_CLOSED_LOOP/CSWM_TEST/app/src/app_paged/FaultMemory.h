#ifndef FAULTMEMORY_H
#define FAULTMEMORY_H

/********************************************************************************/
/*                   CHANGE HISTORY for FAULTMEMORY MODULE                      */
/********************************************************************************/
/* mm/dd/yyyy	User		- Change comments									*/
/* 02-07-2008	Harsha		- File Added										*/
/* 06-16-2008	Francisco	- 	DTC_NO_BAT_FOR_REAR_SEATS_K code changed   		*/
/*								DTC_NO_BAT_FOR_STEERING_WHEEL_K code changed	*/
/*								DTC_CAN_INTERIOR_BUSOFF_K added					*/
/* 02-18-2009   CK          - Deleted not supported STW partial open DTC and    */
/* 							  adjusted the DTC indexes accordingly              */
/* 04-15-2009   Harsha      - New LOC DTCs for PN (CBC,TGW) added               */
/*                          - Also Stuck Switch DTCs moved up before LOC        */ 
/********************************************************************************/
/************************************* CUSW UPDATES ******************************************************/
/* 10.29.2010   Harsha - DTC Status FMEM_DTC_AVAILABILITY_MASK_K updated to 0x0F						 */
/* 06.05.2011   Harsha - FMEM_DTC_STORED_NOT_ACTIVE_MASK_K mask updated to 0x08                          */
/************************************* CUSW UPDATES ******************************************************/

/*   NAME:              FAULTMEMORY.H											*/
/* 																				*/

#undef EXTERN 
#ifdef FAULTMEMORY_C 
#define EXTERN 
#else
#define EXTERN extern
#endif

/* # defines */
#define D_POSITIV_RESPONSE_REQUIRED_K           0x00
#define D_NO_POSITIV_RESPONSE_REQUIRED_K        0x80
#define D_SUBFUNCTION_MASK_K                    0x7F 

#define D_EMISSION_RELATED_SYSTEMS_K            0x000000
#define D_ALL_POWERTRAIN_DTC_K                  0x000001
#define D_ALL_CHASSIS_DTC_K                     0x400000
#define D_ALL_BODY_DTC_K                        0x800000
#define D_ALL_NET_DTC_K                         0xC00000
#define D_ALL_DTC_K                             0xFFFFFF

/* DTC Format Identifiers */
#define D_ISO15031_6DTC_FORMAT_K                0x00
#define D_ISO14229_1DTC_FORMAT_K                0x01
#define D_SAEJ1939_73DTC_FORMAT_K               0x02

#define FMEM_DTC_AVAILABILITY_MASK_K                 0x0F //(Bit3, Bit2, Bit1 and Bit 0)
#define FMEM_DTC_STORED_NOT_ACTIVE_MASK_K			 0x08 //(Bit 3)

/* Defines to the DTC table */
#define D_ERROR_K                               0x00
#define D_EVENT_K                               0x01
#define D_WARNING_INDIKATOR_K                   0x01
#define D_NO_WARNING_INDIKATOR_K                0x00

#define D_0_SECONDS_K                           0
#define D_2_SECONDS_K                           2000
#define D_4_SECONDS_K                           4000
#define D_5_SECONDS_K                           5000 
#define D_60_SECONDS_K                          60000

#define D_ZEITSCHEIBE_K                         128

#define D_PRIO_1_K                              0x01
#define D_PRIO_2_K                              0x02
#define D_PRIO_3_K                              0x03

#define D_DTC_ON_K                              0x01
#define D_DTC_OFF_K                             0x02

//EXTERN const unsigned char SIZEOF_ALL_SUPPORTED_DTC; //PC-Lint Warning 652


/*  New Error Codes */
/* DTC Definition List                                                                          */
/* Power Train Systems      Chassis Systems    Body Systems       Network Communication Systems */
/* Bin   Decode  Hex        Bin   Decode  Hex  Bin   Decode  Hex  Bin   Decode  Hex             */
/* 0000 -- P0 -- 0          0100 -- C0 -- 4    1000 -- B0 -- 8    1100 -- U0 -- C               */
/* 0001 -- P1 -- 1          0101 -- C1 -- 5    1001 -- B1 -- 9    1101 -- U1 -- D               */
/* 0010 -- P2 -- 2          0110 -- C2 -- 6    1010 -- B2 -- A    1110 -- U2 -- E               */
/* 0011 -- P3 -- 3          0111 -- C3 -- 7    1011 -- B3 -- B    1111 -- U3 -- E               */

//Index 	Dtc Description used             	UDS DTC     DTC Format  DTC Descprption
/* 0x00 1 */ #define DTC_HS_FL_SHORT_TO_GND_K  /*B1E9911*/   0x9E9911     //FL Heat Seat Short to gnd/Over load 
/* 0x01 2 */ #define DTC_HS_FL_SHORT_TO_BAT_K  /*B1E9912*/   0x9E9912     //FL Heat Seat Short to Battery  
/* 0x02 3 */ #define DTC_HS_FL_OPEN_FULL_K     /*B1E9913*/   0x9E9913     //FL Heat Seat Open circuit
/* 0x03 4 */ #define DTC_HS_FL_OPEN_PARTIAL_K  /*B1E991E*/   0x9E991E     //FL Heat Seat partial Open circuit

/* 0x04 5 */ #define DTC_HS_FR_SHORT_TO_GND_K  /*B1E9A11*/   0x9E9A11     //FR Heat Seat Short to gnd/Over load
/* 0x05 6 */ #define DTC_HS_FR_SHORT_TO_BAT_K  /*B1E9A12*/   0x9E9A12     //FR Heat Seat Short to Battery  
/* 0x06 7 */ #define DTC_HS_FR_OPEN_FULL_K     /*B1E9A13*/   0x9E9A13     //FR Heat Seat Open circuit
/* 0x07 8 */ #define DTC_HS_FR_OPEN_PARTIAL_K  /*B1E9A1E*/   0x9E9A1E     //FR Heat Seat Partial Open circuit

/* 0x08 9 */ #define DTC_HS_RL_SHORT_TO_GND_K  /*B1E9B11*/   0x9E9B11     //RL Heat Seat Short to gnd/Over load  
/* 0x09 10*/ #define DTC_HS_RL_SHORT_TO_BAT_K  /*B1E9B12*/   0x9E9B12     //RL Heat Seat Short to Battery
/* 0x0A 11*/ #define DTC_HS_RL_OPEN_FULL_K     /*B1E9B13*/   0x9E9B13     //RL Heat Seat Open circuit
/* 0x0B 12*/ #define DTC_HS_RL_OPEN_PARTIAL_K  /*B1E9B1E*/   0x9E9B1E     //RL Heat Seat Partial Open circuit

/* 0x0C 13*/#define DTC_HS_RR_SHORT_TO_GND_K  /*B1E9C11*/  0x9E9C11     //RR Heat Seat Short to gnd/Over load
/* 0x0D 14*/#define DTC_HS_RR_SHORT_TO_BAT_K  /*B1E9C12*/  0x9E9C12     //RR Heat Seat Short to Battery  
/* 0x0E 15*/#define DTC_HS_RR_OPEN_FULL_K     /*B1E9C13*/  0x9E9C13     //RR Heat Seat Open circuit
/* 0x0F 16*/#define DTC_HS_RR_OPEN_PARTIAL_K  /*B1E9C1E*/  0x9E9C1E     //RR Heat Seat Partial Open circuit

/* 0x10 17*/#define DTC_VS_FL_SHORT_TO_GND_K  /*B1E9D11*/  0x9E9D11     //FL Vent Seat Short to gnd/Over load  
/* 0x11 18*/#define DTC_VS_FL_SHORT_TO_BAT_K  /*B1E9D12*/  0x9E9D12     //FL Vent Seat Short to Battery
/* 0x12 19*/#define DTC_VS_FL_OPEN_K          /*B1E9D13*/  0x9E9D13     //FL Vent Seat Open circuit

/* 0x13 20*/#define DTC_VS_FR_SHORT_TO_GND_K  /*B1E9E11*/  0x9E9E11     //FR Vent Seat Short to gnd/Over load
/* 0x14 21*/#define DTC_VS_FR_SHORT_TO_BAT_K  /*B1E9E12*/  0x9E9E12     //FR Vent Seat Short to Battery  
/* 0x15 22*/#define DTC_VS_FR_OPEN_K          /*B1E9E13*/  0x9E9E13     //FR Vent Seat Open circuit

/* 0x16 23*/#define DTC_HSW_SHORT_TO_GND_K    /*B10C411*/  0x90C411     //Steering Wheel Short to gnd/Over load  
/* 0x17 24*/#define DTC_HSW_SHORT_TO_BAT_K    /*B10C412*/  0x90C412     //Steering Wheel Short to Battery
/* 0x18 25*/#define DTC_HSW_OPEN_FULL_K       /*B10C413*/  0x90C413     //Steering Wheel Open circuit

/* 0x19 26*/#define DTC_FL_HEAT_NTC_HIGH_K    /*B1EB11B*/  0x9EB11B     //FL Heater NTC Failure Sensor High
/* 0x1A 27*/#define DTC_FR_HEAT_NTC_HIGH_K    /*B1EB21B*/  0x9EB21B     //FR Heater NTC Failure Sensor High
/* 0x1B 28*/#define DTC_RL_HEAT_NTC_HIGH_K    /*B1EB31B*/  0x9EB31B     //RL Heater NTC Failure Sensor High
/* 0x1C 29*/#define DTC_RR_HEAT_NTC_HIGH_K    /*B1EB41B*/  0x9EB41B     //RR Heater NTC Failure Sensor High

/* 0x1D 30*/#define DTC_FL_HEAT_NTC_LOW_K     /*B1EB11A*/  0x9EB11A     //FL Heater NTC Failure Sensor Low
/* 0x1E 31*/#define DTC_FR_HEAT_NTC_LOW_K     /*B1EB21A*/  0x9EB21A     //FR Heater NTC Failure Sensor Low
/* 0x1F 32*/#define DTC_RL_HEAT_NTC_LOW_K     /*B1EB31A*/  0x9EB31A     //RL Heater NTC Failure Sensor Low
/* 0x20 33*/#define DTC_RR_HEAT_NTC_LOW_K     /*B1EB41A*/  0x9EB41A     //RR Heater NTC Failure Sensor Low

/* 0x21 34*/#define DTC_HARDWARE_ERROR_K      /*B221A00*/  0xA21A00     //Internal Fault 

/* 0x22 35*/#define DTC_BATTERY_VOLTAGE_MIN_ERROR_K  /*B210C18*/ 0xA10C18     //Battery voltage Low
/* 0x23 36*/#define DTC_BATTERY_VOLTAGE_MAX_ERROR_K  /*B210C17*/ 0xA10C17     //Battery voltage High
/* 0x24 37*/#define DTC_SYSTEM_VOLTAGE_MIN_ERROR_K   /*B21DD84*/ 0xA1DD84     //System voltage Low
/* 0x25 38*/#define DTC_SYSTEM_VOLTAGE_MAX_ERROR_K   /*B21DD85*/ 0xA1DD85     //System voltage HIGH

/* 0x26 39*/#define DTC_NO_BAT_FOR_REAR_SEATS_K      /*B11DC13*/ 0x91DC13     //No power to Rear Seats (Tracker#46) 
/* 0x27 40*/#define DTC_NO_BAT_FOR_STEERING_WHEEL_K  /*B11C113*/ 0x91C113     //No power to Steering Wheel (Tracker#46)

/* CAUTION: Do not move the Index 0x28 and 0x29 for Rear seat stuck DTCs */
/* If you wish to move,Heating.c module, hsF_HeatManager function also to be adjusted with right index */
/* As logic looking for Index status. If status is set, heat logic will not allow the switch request */
/* for rear seats if vehicle line is with L or W series */ 
/* 0x28 41*/#define DTC_HS_RL_SWITCH_STUCK_PSD_K	/*B11482A*/  0x91482A    //Rear Left Switch Stuck Pressed state 
/* 0x29 42*/#define DTC_HS_RR_SWITCH_STUCK_PSD_K	/*B114D2A*/  0x914D2A    //Rear Right Switch Stuck Pressed state 

/* 0x2A 43*/#define DTC_HSW_TEMP_SENS_FLT_SNA_K     /*U144600*/  0xD44600     // HSW Temp signal SNA Fault
/* 0x2B 44*/#define DTC_CAN_INTERIOR_BUSOFF_K       /*U001100*/  0xC02000     //Bus OFF Performance (New DTC for V1)

/* 0x2C 45*/#define DTC_NWM_LOC_WITH_CBC_K          /*U014000*/  0xC14000     //LOC with CBC Module
/* 0x2D 46*/#define DTC_NWM_LOC_WITH_TGW_K          /*U014700*/  0xC14700     //LOC with TGW Module


/* Function prototypes */

void FMemApplInit(void);

//void diagF_DiagResponse(enum DIAG_RESPONSE Response, u_temp_l Length, DescMsgContext* pMsgContext);
#endif
