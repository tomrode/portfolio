#ifndef DIAGREAD_H_
#define DIAGREAD_H_

/*****************************************************************************************************
*  (c) Copyright 2011 Continental Automotive Systems, all rights reserved
*
*  All material contained herein is CONTINENTAL CONFIDENTIAL and PROPRIETARY.
*  Any reproduction of this material without written consent from 
*  Continental AG is strictly forbidden.                                  
*
*  Filename:     DiagRead.h
*
*  Created_by:   Harsha Yekollu
*
*  Date_created: 01/19/2011
*
*  Description:  Main Header file for UDS Diagnostics .. Read Data By LID Services 
*
*  Revision_history:
*
*  MKS ID       Author         Date      Description of Change(s)
*  --------  -------------  --------  --------------------------------------
*  xxxxxxx   H. Yekollu     01/19/11  Initial creation for FIAT CUSW MY12 Program
*  xxxxxxx   H. Yekollu     01/19/11  
*
* 
* 
* 
******************************************************************************************************/
#undef EXTERN 

#ifdef DIAGREAD_C
#define EXTERN 
#else
#define EXTERN extern
#endif

/*****************************************************************************************************
*     								LOCAL #DEFINES
******************************************************************************************************/
/* New Diagnostic Service $22 02 7E: Read Conditions to activate CSWM Outputs defines */
/* Byte 1: */
#define IGN_STAT_IS_RUN		0x01	// First bit: Ignition status is run
#define FL_HS_NTC_LESS_70C  0x02	// Second bit: Front left heated seat NTC <= 70C
#define FR_HS_NTC_LESS_70C  0x04    // Third bit: Front right heated seat NTC <= 70C
#define RL_HS_NTC_LESS_70C  0x08    // Fourth bit: Rear left heated seat NTC <= 70C
#define RR_HS_NTC_LESS_70C  0x10    // Fifth bit: Rear right heated seat NTC <= 70C
#define HSW_TEMP_LESS_50C   0x20    // Sixth bit: Heated steering wheel temperature <= 50C
#define ENG_RPM_IN_RANGE    0x40    // Seventh bit: Engine RPM in range or Propulsion System active
#define VOLTAGE_IN_RANGE    0x80    // Eight bit: System and Local Battery Voltage are in operation range 10V to 16V
/* Byte 2: */
#define PCB_OVER_TEMP_PRESENT	0x01	// First bit: PCB over temperature condition is present
#define LOAD_SHED_ACTIVE        0x02    // Second bit: Load shed is active 
#define CAN_COMM_OR_LOC_FLT     0x04	// Third bit: Can communication or LOC with major Network node is present
#define INTERNAL_FLT_PRESENT    0x08    // Fourth bit: Internal fault is present

/* Diagnostic Service $22 F1 0B: Read ECU configuration */
/* Byte 0: */
#define FRONT_HEAT_PRESENT       0x01     // Front heaters present
#define FRONT_REAR_HEAT_PRESENT  0x02     // Front and rear heaters present
#define VENT_PRESENT             0x04     // Vent present
#define STW_PRESENT              0x08     // STW present
#define REAR_HEATER_MASK_K		 0x0C	 // rear heaters mask

/* Byte 1: */
#define SEAT_TYPE_CLOTH		0x01	// First bit: Seat Type cloth
#define SEAT_TYPE_PERF_LEATHER  0x02	// Second bit: Seat Type perf Leather
#define SEAT_TYPE_LEATHER  0x04    // Third bit: Seat type leather
#define WHEEL_TYPE_WOOD  0x08    // Fourth bit: Wheel Type Wood
#define WHEEL_TYPE_LEATHER  0x10    // Fifth bit: Wheel Type Leather
/* Byte 2: */
#define HEATSEAT_FRONT_P	   0x01	// First bit:  Heated seat front present
#define HEATSEAT_FRONT_REAR_P  0x02	// Second bit: Heated seat front and rear present
#define VENTSEAT_FRONT_P       0x04	// Second bit: Front Vented present
#define STEERING_WHEEL_P       0x08	// Second bit: Steering Wheel present



/*****************************************************************************************************
*     								LOCAL VARIABLES
******************************************************************************************************/
/*****************************************************************************************************
*     								FUNCTIONS
******************************************************************************************************/
//void diagReadF_EcuDev_Data_22H(enum DIAG_RESPONSE* Response, u_temp_l* DataLen, DescMsgContext* pMsgContext);

//void diagReadF_RAM_Data_22H(enum DIAG_RESPONSE* Response, u_temp_l* DataLen, DescMsgContext* pMsgContext);

//void diagReadF_EngParamRAM_Data_22H(enum DIAG_RESPONSE* Response, u_temp_l* DataLen, DescMsgContext* pMsgContext);

//void diagReadF_EngParamEEP_Data_22H(enum DIAG_RESPONSE* Response, u_temp_l* DataLen, DescMsgContext* pMsgContext);

//void diagReadF_Calib_Data_22H(enum DIAG_RESPONSE* Response, u_temp_l* DataLen, DescMsgContext* pMsgContext);

//void diagReadF_EOL_Data_22H(enum DIAG_RESPONSE* Response, u_temp_l* DataLen, DescMsgContext* pMsgContext);

#endif /*DIAGREAD_H_*/
