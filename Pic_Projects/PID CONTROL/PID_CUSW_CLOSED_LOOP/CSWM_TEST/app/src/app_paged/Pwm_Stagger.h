#ifndef PWM_STAGGER_H
#define PWM_STAGGER_H

/********************************************************************************/
/*                        A U T H O R   I D E N T I T Y							*/
/********************************************************************************/
/* Initials      Name                   Company									*/
/* --------      --------------------   --------------------------------------- */ 
/* CK            Can Kulduk             Continental Automotive Systems			*/
/********************************************************************************/


/********************************************************************************/
/*                   CHANGE HISTORY for BATTV MODULE                            */
/********************************************************************************/
/* mm/dd/yyyy	User - Change comments											*/
/* 04.11.2007   CK   - Heater file created                                      */
/* 09.22.2008	CK	 - Copied from CSWM MY09 project     						*/
/* 09.22.2008   CK   - Updated Stagger_Channel_Type enum and TC_REG_OFFSET      */
/*                     define for CSWM MY11 (XS128 micro).						*/
/********************************************************************************/


/******************************* @Enumaration(s) ********************************/
/* Ecu Time Type */
typedef enum{
    OFF_TIME=0,
    ON_TIME=1
}Stagger_EctTime_Type;

/* Channels (Corresponds to Heated Seat output TIM channels) */
/* Updated for MY11 */
typedef enum{
	FL_TIM_CH=3,
    FR_TIM_CH=4,
    RL_TIM_CH=5,
    RR_TIM_CH=6    
}Stagger_Channel_Type;

/* TCx Operation: This tells us whether to add or subtract the time difference */
typedef enum{
    SUBTRACT=0,
    ADD=1
}Stagger_Operation_Type;
/********************************************************************************/


/******************************* @Constants/Defines *****************************/
#define HS_SIZE              4       // maximum number of Heated seat channels available
#define INVALID_CH_INDEX     4       // Invalid channel index
/* Updated for MY11: First 2 Tim channels are not used. So TC reg offset should be 2 for MY11. */
#define TC_REG_OFFSET        2       // Offset for TC Register of current and next channel 

#define MAX_COUNT           0xFFFF   // Maximum count for each TCx register

#define CH_PERIOD           25000    // Hs channel period (in timer clock counts)
#define CH_HALF_PERIOD      12500    // half period  
#define N250                250      // (Period/100)used to calculate the time difference

#define N100                100      // Number 100 used to check for 100% duty cycle
/********************************************************************************/

/****************************** @Function Prototypes ******************************/
@far void StaggerF_Manager(Stagger_Channel_Type Channel_ID, u_16Bit this_duty_cycle);
@far void StaggerF_Enable_Next_Ch(Stagger_Channel_Type Channel_ID);
/********************************************************************************/


#endif

