#ifndef TEMPERATURE_H
#define TEMPERATURE_H

/********************************************************************************/
/*                        A U T H O R   I D E N T I T Y							*/
/********************************************************************************/
/* Initials      Name                   Company									*/
/* --------      --------------------   --------------------------------------- */ 
/* CK            Can Kulduk             Continental Automotive Systems			*/
/* HY            Harsha Yekollu         Continental Automotive Systems          */
/* FU            Fransisco Uribe        Continental Automotive Systems          */
/********************************************************************************/


/********************************************************************************/
/*                   CHANGE HISTORY for TEMPERATURE MODULE                      */
/********************************************************************************/
/* mm/dd/yyyy User - Change comments											*/
/* 01.22.2007   CK - Header file created										*/
/* 03.07.2008   HY - Copied from CSWM MY09 project                              */
/* 06-17-2008	FU - OVER_TEMP_SET added										*/
/* 					 OVER_TEMP_CLEAR added										*/
/* 					 OVER_TEMP_COUNTER_LMT added								*/
/* 09.05.2008   CK - Added function prototype TempF_U12S_ADC 					*/
/* 10.22.2008   CK - Function prototypes commented for PCB STB and PCB STG fault*/
/* 10.28.2008   CK - Added function prototypes for canTemp conversions       	*/
/* 11.13.2008   CK - Changed the wait threshold to 4 minutes before we set the  */
/*				     PCB over themperature condition. Affective S0B (084600) SW.*/
/* 04.02.2009   CK - Moved declaration of temperature_cal_s into header file in */
/*                   order to support $22 02 AF (Read PCB Temperature thresholds*/
/*                   diagnostic service).                            			*/
/********************************************************************************/


/******************************* @Enumaration(s) ********************************/
/* NTC Status */
typedef enum{
    NTC_STAT_UNDEFINED=0,    // NTC fault status undefined (initial value)
    NTC_SHRT_TO_GND=1,       // NTC fault - short to ground 
    NTC_OPEN_CKT=2,          // NTC faullt - open circuit or short to battery
    NTC_STAT_GOOD=3          // NTC status good
}temperature_NTC_Stat_e;

/* PCB NTC Status */
typedef enum{
    PCB_NTC_STAT_UNDEFINED=0,    // PCB NTC Status undefined (initial value)
    PCB_SHRT_TO_BATT=1,          // PCB NTC Short to battery fault (NOT USED FOR MY11)
    PCB_SHRT_TO_GND=2,           // PCB NTC Short to Ground fault (NOT USED FOR MY11)
    PCB_STAT_OVERTEMP=3,         // PCB NTC Over Temperature: Set when temperature >= 100C
    PCB_STAT_GOOD=4              // PCB NTC status good: Set when temperature < 90C
}temperature_PCB_Stat_e;
/********************************************************************************/


/****************************** @Constants/Defines ******************************/
#define TEMPERATURE_HS_LMT             4   // Total Number of Heated Seats

#define TEMPERATURE_FLT_MTR_TM        12   // NTC faults mature time (160ms*(12+1)=2.08 seconds)

#define IGN_CYCLE_LMT                 10   // 10th consecutive ignition cycle

#define TEMPERATURE_NTC_LMT           255  // Maximum 8-bit NTC limit

#define TEMPERATURE_COLD_AMBIENT_LMT  60 // Ambient temperature of -10 C

#define TEMPERATURE_HS_1MIN           187  // 1 minute in 320ms timeslice

/* OLD THRESHOLD TO WAIT (10 MIN.) BEFORE WE SET THE PCB OVER TEMPERATURE CONDITION */
//#define TEMPERATURE_PCB_OT_TM_LMT     3750 // 10 minute PCB OverTemperature limit to set DTC (160ms tm slice)

/* NEW THRESHOLD TO WAIT (4 MIN.) BEFORE WE SET THE PCB OVER TEMPERATURE CONDITION */
#define TEMPERATURE_PCB_OT_TM_LMT     1500 // 4 minute PCB OverTemperature limit to set DTC (160ms tm slice)

/* PCB Over temp status	*/
#define OVER_TEMP_SET                   1
#define OVER_TEMP_CLEAR                 0
#define OVER_TEMP_COUNTER_LMT         254
extern u_8Bit temperature_NTC_supplyV;     // stored most recent NTC supply voltage in 100mV units
/********************************************************************************/


/******************************* @Global variables ******************************/
extern u_8Bit temperature_HS_NTC_status[TEMPERATURE_HS_LMT];  // stores heated seat NTC fault status

/* For EOL Tests. Access only. Please do not modify these variables */
extern u_16Bit temperature_PCB_ADC;        // stores PCB temperature reading
extern u_8Bit temperature_NTC_supplyV;     // stored most recent NTC supply voltage in 100mV units
/********************************************************************************/

/******************************** @Data Structures ******************************/
/* Only read access please for the elements of this structure. */
/* Temperature Calibration struct: - 8 Bytes */
typedef struct{
    u_8Bit HS_NTC_Lw_Thrshld;        // Low threshold used to set a NTC fault short to ground
    u_8Bit HS_NTC_Hgh_Thrshld;       // High threshold used to set a NTC fault open circuit
    u_8Bit NTC_Supply_V_Low_Thrshld; // NTC Supply Voltage Low Threshold
//    u_8Bit PCB_sensor_err_occurance; // PCB sensor error occurance for previous ignition cycle (true/false)
//    u_8Bit PCB_sensor_occurance_cnt; // PCB sensor error consecutive occurance count
    u_8Bit NTCOpen_OutputOntime_lmt; // Output ontime before detecting NTC open at ambient < -10 C (in minutes)
//    u_16Bit PCB_NTC_shrtToBatt;      // No PCB NTC or Short to Battery Threshold
//    u_16Bit PCB_NTC_shrtToGnd;       // No PCB Pullup or Short to Ground
    u_16Bit PCB_Set_OverTemp;        // Threshold used to set an PCB over temperature fault
    u_16Bit PCB_Clear_OverTemp;      // Threshold used to clear an PCB over temperature fault
}temperature_cal_s;

/* Decleration of temperature module calibration parameters */
extern temperature_cal_s temperature_s;

struct temp_failsafe_data {
	u_16Bit PCB_OverTemp_OdoStamp_First_w;      //Holds ODO reading when first time OT happen
	u_16Bit PCB_OverTemp_OdoStamp_Latest_w;     //Holds ODO reading Latest Over temp happen
	u_8Bit  PCB_OverTemp_Counter_c;             //Holds No of times PCB Overtemp occured
	u_8Bit  PCB_OverTemp_Status_c;              //Holds PCB Over Temp Status 
};

/* NTC Failsafe data to notify for External Diagnostic command */
extern struct temp_failsafe_data  Temp_FailSafe_prms;

/********************************************************************************/

/**************************** @Function Prototypes ******************************/
/* For Main */
@far void TempF_Init(void);
@far void TempF_PCB_ADC(void);
@far void TempF_Monitor_ADC(void);
@far void TempF_HS_NTC_Flt_Monitor(void);
@far void TempF_PCB_NTC_Flt_Monitor(void);
@far u_8Bit TempF_Get_PCB_NTC_Stat(void);
//@far void TempF_Chck_PCB_Sensor_Updt(void);	// NOT USED
//@far void TempF_Upt_PCB_Sensor_Err(void);		// NOT USED
@far void TempF_U12S_ADC(void);
/* For Heating */
@far u_8Bit TempF_Get_NTC_Rdg(u_8Bit temperature_hs_position);
/* For Diagnostics */
@far u_8Bit TempF_NtcAdStd_to_CanTemp(u_8Bit NtcAd_c);
@far u_8Bit TempF_CanTemp_to_NtcAdStd(u_8Bit CanTemp_c);
/********************************************************************************/


#endif

