/********************************************************************************/
/*                   CHANGE HISTORY for CANIO MODULE                            */
/********************************************************************************/
/* mm/dd/yyyy User		-	Change comments										*/
/* 02-07-2008 Harsha	-	New Module created									*/
/*							MY09 Code added										*/
/* 06-16-2008Francisco	- 	DTC_CAN_INTERIOR_BUSOFF_K added						*/
/* 06-23-2008 Francisco	-	ApplDescProcessReadDtcInformation enabled			*/
/*							ApplDescProcessClearDiagnosticInformation enabled	*/
/*							ApplDescProcessControlDTCSetting enabled			*/
/* 11-11-2008 Harsha    -   The following DTCs are added                        */
/*							DTC_NWM_LOC_WITH_HVAC_K								*/
/*							DTC_HS_RL_SWITCH_STUCK_PSD_K						*/
/*							DTC_HS_RR_SWITCH_STUCK_PSD_K						*/
/* 02-18-2009 CK        -   Not supported STW Partial Open DTC is commented.    */
/* 04-15-2009 Harsha    -   New DTCs for PN added and adjusted the stuck swith  */
/*                          DTCs position                                       */
/* 09-28-2010 Harsha    -   PWR_NET commented. code to have pwr net logic       */
/********************************************************************************/
/*   NAME:              FAULTMEMORY.C                                   		*/
/* 																				*/
/* This module handles Only the DTC related services.                                                                       */
/* UDS services sub-devided into two files.                                                                                 */
/* ALl the DTC related services handles in FaultMemory.C and all other remaining tasks will be handled in (DIAGNOSE.C)      */
/* When ever the external test tool requests any DTC information from the module, respective callback function gets called. */
/* The callback function handling will be done here and eventually goes to the FaultMemoryLib.c and returns information.    */
/* All the CDD (earlier DDT) implementation for DTC services done here.                                                     */

#define FAULTMEMORY_C

/*  Includes*/

/* CAN_NWM Related Include files */
/* CAN_NWM Related Include files */
#include "il_inc.h"
#include "v_inc.h"
#include "desc.h"
#include "appdesc.h"

#include "TYPEDEF.h"
#include "DiagMain.h"
#include "FaultMemory.h"
#include "FaultMemoryLib.h"
#include "main.h"
#include "mw_eem.h"


/* external data */
/* forward declarations                                                                                    */
/* extern void diagF_DiagResponse(void);                                                                   */
/* extern void FlashF_ReadEEPROMMirror(unsigned short address,unsigned char count,unsigned char * buffer); */
/* extern void FMemLibF_ClearDiagnosticInformation(void);                                                  */
/* extern void FMemLibF_ReadDiagnosticInformation(void);                                                   */

/* Lint Info 734: diagF_DiagResponse Loss of precision (arg. no. 2) (16 bits to 8 bits) Changed by CK 04.24.09 */
//extern void diagF_DiagResponse(enum DIAG_RESPONSE Response, unsigned char Length, DescMsgContext* pMsgContext);

/* defines */

//#define fmemLF_MemSet           memset  //PC-Lint update: Not referenced Info 750
//#define fmemLF_MemCpy           memcpy
//#define fmemLF_MemCmp           memcmp
//#define fmemLF_MemMove          memmove   

/* DTC ROM Table description */
const struct DTC_ROM_STR fmem_AllRegisteredDtc_t[] =
{
 {DTC_HS_FL_SHORT_TO_GND_K,           D_0_SECONDS_K,  D_PRIO_3_K,  D_ERROR_K,  D_NO_WARNING_INDIKATOR_K },
 { DTC_HS_FL_SHORT_TO_BAT_K,          D_0_SECONDS_K,  D_PRIO_3_K,  D_ERROR_K,  D_NO_WARNING_INDIKATOR_K },
 { DTC_HS_FL_OPEN_FULL_K,             D_0_SECONDS_K,  D_PRIO_3_K,  D_ERROR_K,  D_NO_WARNING_INDIKATOR_K },
 { DTC_HS_FL_OPEN_PARTIAL_K,          D_0_SECONDS_K,  D_PRIO_3_K,  D_ERROR_K,  D_NO_WARNING_INDIKATOR_K },
 { DTC_HS_FR_SHORT_TO_GND_K,          D_0_SECONDS_K,  D_PRIO_3_K,  D_ERROR_K,  D_NO_WARNING_INDIKATOR_K },
 { DTC_HS_FR_SHORT_TO_BAT_K,          D_0_SECONDS_K,  D_PRIO_3_K,  D_ERROR_K,  D_NO_WARNING_INDIKATOR_K },
 { DTC_HS_FR_OPEN_FULL_K,             D_0_SECONDS_K,  D_PRIO_3_K,  D_ERROR_K,  D_NO_WARNING_INDIKATOR_K },
 { DTC_HS_FR_OPEN_PARTIAL_K,          D_0_SECONDS_K,  D_PRIO_3_K,  D_ERROR_K,  D_NO_WARNING_INDIKATOR_K },
 { DTC_HS_RL_SHORT_TO_GND_K,          D_0_SECONDS_K,  D_PRIO_3_K,  D_ERROR_K,  D_NO_WARNING_INDIKATOR_K },
 { DTC_HS_RL_SHORT_TO_BAT_K,          D_0_SECONDS_K,  D_PRIO_3_K,  D_ERROR_K,  D_NO_WARNING_INDIKATOR_K },
 { DTC_HS_RL_OPEN_FULL_K,             D_0_SECONDS_K,  D_PRIO_3_K,  D_ERROR_K,  D_NO_WARNING_INDIKATOR_K },
 { DTC_HS_RL_OPEN_PARTIAL_K,          D_0_SECONDS_K,  D_PRIO_3_K,  D_ERROR_K,  D_NO_WARNING_INDIKATOR_K },
 { DTC_HS_RR_SHORT_TO_GND_K,          D_0_SECONDS_K,  D_PRIO_3_K,  D_ERROR_K,  D_NO_WARNING_INDIKATOR_K },
 { DTC_HS_RR_SHORT_TO_BAT_K,          D_0_SECONDS_K,  D_PRIO_3_K,  D_ERROR_K,  D_NO_WARNING_INDIKATOR_K },
 { DTC_HS_RR_OPEN_FULL_K,             D_0_SECONDS_K,  D_PRIO_3_K,  D_ERROR_K,  D_NO_WARNING_INDIKATOR_K },
 { DTC_HS_RR_OPEN_PARTIAL_K,          D_0_SECONDS_K,  D_PRIO_3_K,  D_ERROR_K,  D_NO_WARNING_INDIKATOR_K },
 { DTC_VS_FL_SHORT_TO_GND_K,          D_0_SECONDS_K,  D_PRIO_3_K,  D_ERROR_K,  D_NO_WARNING_INDIKATOR_K },
 { DTC_VS_FL_SHORT_TO_BAT_K,          D_0_SECONDS_K,  D_PRIO_3_K,  D_ERROR_K,  D_NO_WARNING_INDIKATOR_K },
 { DTC_VS_FL_OPEN_K,                  D_0_SECONDS_K,  D_PRIO_3_K,  D_ERROR_K,  D_NO_WARNING_INDIKATOR_K },
 { DTC_VS_FR_SHORT_TO_GND_K,          D_0_SECONDS_K,  D_PRIO_3_K,  D_ERROR_K,  D_NO_WARNING_INDIKATOR_K },
 { DTC_VS_FR_SHORT_TO_BAT_K,          D_0_SECONDS_K,  D_PRIO_3_K,  D_ERROR_K,  D_NO_WARNING_INDIKATOR_K },
 { DTC_VS_FR_OPEN_K,                  D_0_SECONDS_K,  D_PRIO_3_K,  D_ERROR_K,  D_NO_WARNING_INDIKATOR_K },
 { DTC_HSW_SHORT_TO_GND_K,            D_0_SECONDS_K,  D_PRIO_3_K,  D_ERROR_K,  D_NO_WARNING_INDIKATOR_K },
 { DTC_HSW_SHORT_TO_BAT_K,            D_0_SECONDS_K,  D_PRIO_3_K,  D_ERROR_K,  D_NO_WARNING_INDIKATOR_K },
 { DTC_HSW_OPEN_FULL_K,               D_0_SECONDS_K,  D_PRIO_3_K,  D_ERROR_K,  D_NO_WARNING_INDIKATOR_K },
 { DTC_FL_HEAT_NTC_HIGH_K,            D_0_SECONDS_K,  D_PRIO_3_K,  D_ERROR_K,  D_NO_WARNING_INDIKATOR_K },
 { DTC_FR_HEAT_NTC_HIGH_K,            D_0_SECONDS_K,  D_PRIO_3_K,  D_ERROR_K,  D_NO_WARNING_INDIKATOR_K },
 { DTC_RL_HEAT_NTC_HIGH_K,            D_0_SECONDS_K,  D_PRIO_3_K,  D_ERROR_K,  D_NO_WARNING_INDIKATOR_K },
 { DTC_RR_HEAT_NTC_HIGH_K,            D_0_SECONDS_K,  D_PRIO_3_K,  D_ERROR_K,  D_NO_WARNING_INDIKATOR_K },
 { DTC_FL_HEAT_NTC_LOW_K,             D_0_SECONDS_K,  D_PRIO_3_K,  D_ERROR_K,  D_NO_WARNING_INDIKATOR_K },
 { DTC_FR_HEAT_NTC_LOW_K,             D_0_SECONDS_K,  D_PRIO_3_K,  D_ERROR_K,  D_NO_WARNING_INDIKATOR_K },
 { DTC_RL_HEAT_NTC_LOW_K,             D_0_SECONDS_K,  D_PRIO_3_K,  D_ERROR_K,  D_NO_WARNING_INDIKATOR_K },
 { DTC_RR_HEAT_NTC_LOW_K,             D_0_SECONDS_K,  D_PRIO_3_K,  D_ERROR_K,  D_NO_WARNING_INDIKATOR_K },
 { DTC_HARDWARE_ERROR_K,              D_0_SECONDS_K,  D_PRIO_3_K,  D_ERROR_K,  D_NO_WARNING_INDIKATOR_K },
 { DTC_BATTERY_VOLTAGE_MIN_ERROR_K,   D_0_SECONDS_K,  D_PRIO_3_K,  D_EVENT_K,  D_NO_WARNING_INDIKATOR_K },
 { DTC_BATTERY_VOLTAGE_MAX_ERROR_K,   D_0_SECONDS_K,  D_PRIO_3_K,  D_EVENT_K,  D_NO_WARNING_INDIKATOR_K },
 { DTC_SYSTEM_VOLTAGE_MIN_ERROR_K,    D_0_SECONDS_K,  D_PRIO_3_K,  D_EVENT_K,  D_NO_WARNING_INDIKATOR_K },
 { DTC_SYSTEM_VOLTAGE_MAX_ERROR_K,    D_0_SECONDS_K,  D_PRIO_3_K,  D_EVENT_K,  D_NO_WARNING_INDIKATOR_K },
 { DTC_NO_BAT_FOR_REAR_SEATS_K,       D_0_SECONDS_K,  D_PRIO_3_K,  D_ERROR_K,  D_NO_WARNING_INDIKATOR_K },
 { DTC_NO_BAT_FOR_STEERING_WHEEL_K,   D_0_SECONDS_K,  D_PRIO_3_K,  D_ERROR_K,  D_NO_WARNING_INDIKATOR_K },
 { DTC_HS_RL_SWITCH_STUCK_PSD_K,      D_0_SECONDS_K,  D_PRIO_3_K,  D_ERROR_K,  D_NO_WARNING_INDIKATOR_K },
 { DTC_HS_RR_SWITCH_STUCK_PSD_K,      D_0_SECONDS_K,  D_PRIO_3_K,  D_ERROR_K,  D_NO_WARNING_INDIKATOR_K },
 { DTC_HSW_TEMP_SENS_FLT_SNA_K,       D_0_SECONDS_K,  D_PRIO_3_K,  D_ERROR_K,  D_NO_WARNING_INDIKATOR_K },
 { DTC_CAN_INTERIOR_BUSOFF_K,         D_0_SECONDS_K,  D_PRIO_3_K,  D_ERROR_K,  D_NO_WARNING_INDIKATOR_K },
 { DTC_NWM_LOC_WITH_CBC_K,            D_0_SECONDS_K,  D_PRIO_3_K,  D_ERROR_K,  D_NO_WARNING_INDIKATOR_K },
 { DTC_NWM_LOC_WITH_TGW_K,            D_0_SECONDS_K,  D_PRIO_3_K,  D_ERROR_K,  D_NO_WARNING_INDIKATOR_K }, 							
};

struct DTC_RAM_STR d_AllDtc_a[sizeof(fmem_AllRegisteredDtc_t)/sizeof(struct DTC_ROM_STR)];
static enum DIAG_RESPONSE Response;
static u_temp_l DataLen = 0;
static u_temp_l SubFunctionRequest;

/*Prototypes*/
extern @far void diagF_DiagResponse(enum DIAG_RESPONSE Response, u_temp_l Length, DescMsgContext* pMsgContext);
extern void FMemLibF_ClearDiagnosticInformation(enum DIAG_RESPONSE* Response, DescMsgContext* pMsgContext);
extern void FMemLibF_ReadDiagnosticInformation(enum DIAG_RESPONSE* Response, u_temp_l* DataLen, DescMsgContext* pMsgContext, unsigned char Subservice);

/* FMemApplInit		********************************************************************************************/
/* After every Reset, At the initialization copies all the DTC information from EEPROM Stacks to Local stacks. */
/* 88 bytes of DTC status                                                                                      */
/* 56 bytes of Chronostack Information                                                                         */
/* 32 bytes of Historical Stack Information and                                                                */
/* 3 bytes of Historical Interogation record information will be copied from Shadow to Local RAM.              */
/*********************************************************************************************/
void FMemApplInit(void)
{
	/* Copy all the DTC Status info from Shadow to Local */
//	FlashF_ReadEEPROMMirror(FlashM_MakeAddress(mirror_Dtc_Status), 88, (unsigned char*)&d_AllDtc_a[0]/*.DtcState*/);
	/* Copy all the chrono stack information into local */
//	FlashF_ReadEEPROMMirror(FlashM_MakeAddress(mirror_Chrono_Stack), 56, (unsigned char*)&d_DtcMemory.elem[0] );
	/* Copy all the Historical stack information into local */
//	FlashF_ReadEEPROMMirror(FlashM_MakeAddress(mirror_Historical_Stack), 32, (unsigned char*)&d_DtcHistoricalMemory.elem[0] );
	/* Copy the Interogation Record information into Local */
//	FlashF_ReadEEPROMMirror(FlashM_MakeAddress(mirror_Int_Record_Counter), 3, &d_HistoricalInterrogationRecord.counter );
	
	EE_BlockRead(EE_CHRONO_STACK, (u_8Bit*)&d_DtcMemory);
	EE_BlockRead(EE_DTC_STATUS, (u_8Bit*)&d_AllDtc_a);
	EE_BlockRead(EE_HISTORICAL_STACK, (u_8Bit*)&d_DtcHistoricalMemory);
	
}
/*  ********************************************************************************
 * Function name:ApplDescSUBF_02FaultMem (Service request header:$19 $2 )
  ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescSUBF_02FaultMem(DescMsgContext* pMsgContext)
{
  /* Supported by FaultMemoryLib */
	/* Call the fault memory handler */
	Subservice = 0x02;
  FMemLibF_ReadDiagnosticInformation(&Response, &DataLen, pMsgContext, Subservice);
	diagF_DiagResponse(Response, DataLen, pMsgContext);
}
/*  ********************************************************************************
 * Function name:ApplDescSUBF_04FaultMem (Service request header:$19 $4 )
  ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescSUBF_04FaultMem(DescMsgContext* pMsgContext)
{
  /* Supported by FaultMemoryLib */
	/* Call the fault memory handler */
	Subservice = 0x04;
	FMemLibF_ReadDiagnosticInformation(&Response, &DataLen, pMsgContext, Subservice);
	diagF_DiagResponse(Response, DataLen, pMsgContext);
}

/*  ********************************************************************************
 * Function name:ApplDescSUBF_06FaultMem (Service request header:$19 $6 )
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescSUBF_06FaultMem(DescMsgContext* pMsgContext)
{
  /* Supported by FaultMemoryLib */
	/* Call the fault memory handler */
  Subservice = 0x06;
	FMemLibF_ReadDiagnosticInformation(&Response, &DataLen, pMsgContext, Subservice);
	diagF_DiagResponse(Response, DataLen, pMsgContext);
}

/*  ********************************************************************************
 * Function name:ApplDescSUBF_07FaultMem (Service request header:$19 $7 )
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescSUBF_07FaultMem(DescMsgContext* pMsgContext)
{
  /* Supported by FaultMemoryLib */
	/* Call the fault memory handler */
  Subservice = 0x07;
	FMemLibF_ReadDiagnosticInformation(&Response, &DataLen, pMsgContext, Subservice);
	diagF_DiagResponse(Response, DataLen, pMsgContext);
}

/*  ********************************************************************************
 * Function name:ApplDescSUBF_08FaultMem (Service request header:$19 $8 )
 * Description:
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescSUBF_08FaultMem(DescMsgContext* pMsgContext)
{
  /* Supported by FaultMemoryLib */
	/* Call the fault memory handler */
	Subservice = 0x08;
	FMemLibF_ReadDiagnosticInformation(&Response, &DataLen, pMsgContext, Subservice);
	diagF_DiagResponse(Response, DataLen, pMsgContext);
}

/*  ********************************************************************************
 * Function name:ApplDescSUBF_09FaultMem (Service request header:$19 $9 )
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescSUBF_09FaultMem(DescMsgContext* pMsgContext)
{
  /* Supported by FaultMemoryLib */
	/* Call the fault memory handler */
	Subservice = 0x09;
	FMemLibF_ReadDiagnosticInformation(&Response, &DataLen, pMsgContext, Subservice);
	diagF_DiagResponse(Response, DataLen, pMsgContext);
}


/*  ********************************************************************************
 * Function name:ApplDescSUBF_0CFaultMem (Service request header:$19 $C )
********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescSUBF_0CFaultMem(DescMsgContext* pMsgContext)
{
  /* Supported by FaultMemoryLib */
	/* Call the fault memory handler */
	Subservice = 0x0C;
	FMemLibF_ReadDiagnosticInformation(&Response, &DataLen, pMsgContext, Subservice);
	diagF_DiagResponse(Response, DataLen, pMsgContext);
}

/*  ********************************************************************************
 * Function name:ApplDescSUBF_0EFaultMem (Service request header:$19 $E )
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescSUBF_0EFaultMem(DescMsgContext* pMsgContext)
{
  /* Supported by FaultMemoryLib */
	/* Call the fault memory handler */
  Subservice = 0x0E;
	FMemLibF_ReadDiagnosticInformation(&Response, &DataLen, pMsgContext, Subservice);
	diagF_DiagResponse(Response, DataLen, pMsgContext);
}

/*  ********************************************************************************
 * Function name:ApplDescClearFaultMem (Service request header:$14 )
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescClearFaultMem(DescMsgContext* pMsgContext)
{
  /* Calling Fault Memory Library */
  FMemLibF_ClearDiagnosticInformation(&Response, pMsgContext);
  diagF_DiagResponse(Response, 0, pMsgContext);
}
/*  ********************************************************************************
 * Function name:ApplDescCtrlDtcSettingOn (Service request header:$85 $1 )
  ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescCtrlDtcSettingOn(DescMsgContext* pMsgContext)
{
  //u_temp_l SubFunctionRequest;

	/* store the request data into temp variable */
	//SubFunctionRequest = pMsgContext->reqData[0];

	/* Is the requested datalength is correct? */
	//if (pMsgContext->reqDataLen == 1)
	//{	
			  /* Enable the DTCs*/
				d_DtcSettingModeEnabled_t = TRUE; 
				    
        //if (SubFunctionRequest & D_NO_POSITIV_RESPONSE_REQUIRED_K)
			  //{ 
            /* No Response required */
        //} 
        //else
        //{  
				    /* Prepare the response */
				    Response = RESPONSE_OK;
			  //}
	//}
  //else
	//{
		    //Response = LENGTH_INVALID_FORMAT;
	//}
	 diagF_DiagResponse(Response, pMsgContext->reqDataLen, pMsgContext);
}


/*  ********************************************************************************
 * Function name:ApplDescCtrlDtcSettingOff (Service request header:$85 $2 )
  ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescCtrlDtcSettingOff(DescMsgContext* pMsgContext)
{
//  	//u_temp_l SubFunctionRequest;
// 
// 	/* store the request data into temp variable */
// 	SubFunctionRequest = pMsgContext->reqData[0];
// 
// 	/* Is the requested datalength is correct? */
// 	if (pMsgContext->reqDataLen == 1)
// 	{	
// 			  /* Disable the DTCs*/
 				d_DtcSettingModeEnabled_t = FALSE; 
// 				    
//         if (SubFunctionRequest & D_NO_POSITIV_RESPONSE_REQUIRED_K)
// 			  { 
//             /* No Response required */
//         } 
//         else
//         {  
// 				    /* Prepare the response */
 				    Response = RESPONSE_OK;
// 			  }
// 	}
//   else
// 	{
// 		    Response = LENGTH_INVALID_FORMAT;
// 	}
	 diagF_DiagResponse(Response, pMsgContext->reqDataLen, pMsgContext);
}
