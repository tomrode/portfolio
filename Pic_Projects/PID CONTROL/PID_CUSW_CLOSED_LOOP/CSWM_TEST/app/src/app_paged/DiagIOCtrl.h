#ifndef DIAGIOCTRL_H_
#define DIAGIOCTRL_H_

/*****************************************************************************************************
*  (c) Copyright 2011 Continental Automotive Systems, all rights reserved
*
*  All material contained herein is CONTINENTAL CONFIDENTIAL and PROPRIETARY.
*  Any reproduction of this material without written consent from 
*  Continental AG is strictly forbidden.                                  
*
*  Filename:     DiagIOCtrl.h
*
*  Created_by:   Harsha Yekollu
*
*  Date_created: 01/21/2011
*
*  Description:  Main Header file for UDS Diagnostics .. IO Control Services 
*
*  Revision_history:
*
*  MKS ID       Author         Date      Description of Change(s)
*  --------  -------------  --------  --------------------------------------
*  xxxxxxx   H. Yekollu     01/21/11  Initial creation for FIAT CUSW MY12 Program
*  xxxxxxx   H. Yekollu     01/21/11  
*
* 
* 
* 
******************************************************************************************************/
#undef EXTERN 

#ifdef DIAGIOCTRL_C
#define EXTERN 
#else
#define EXTERN extern
#endif

/*****************************************************************************************************
*     								VARIABLES
******************************************************************************************************/
/* IO controlling variables */
/* IO Controlling parameters */
/* The below variables holds the values from Diag buffer when tester requests IO controling */
EXTERN unsigned char diag_io_load_shed_c;    //Holds the Load shed control value
EXTERN unsigned char diag_io_ign_state_c;    //Holds the Ignition control value
EXTERN unsigned char diag_io_vs_all_out_c; //Holds the vent seat output request control value for 2 seats
EXTERN unsigned char diag_io_st_whl_rq_c;    //Holds the steering wheel output request control value
EXTERN unsigned char diag_io_st_whl_state_c; //Holds the steering wheel LED control value

//EXTERN unsigned char diag_op_rc_time_c; //Holds the time duration to run the output tests through routine control
EXTERN unsigned char diag_op_rc_status_c; //Status of the output tests through routine control
EXTERN unsigned char diag_op_rc_preCondition_stat_c;	// Chrysler ROutine Control Pre-condition status

EXTERN unsigned char diag_io_hs_rq_c [4];//Holds the Heated seat individual output request control value
#define diag_io_hs_fl_rq_c   diag_io_hs_rq_c [0]
#define diag_io_hs_fr_rq_c   diag_io_hs_rq_c [1]
#define diag_io_hs_rl_rq_c   diag_io_hs_rq_c [2]
#define diag_io_hs_rr_rq_c   diag_io_hs_rq_c [3]

EXTERN unsigned char diag_io_hs_state_c [4]; //Holds the Heated seat individual LED control value
#define diag_io_hs_fl_state_c   diag_io_hs_state_c [0]
#define diag_io_hs_fr_state_c   diag_io_hs_state_c [1]
#define diag_io_hs_rl_state_c   diag_io_hs_state_c [2]
#define diag_io_hs_rr_state_c   diag_io_hs_state_c [3]

EXTERN unsigned char diag_io_vs_rq_c [2];//Holds the Vented seat individual output request control value
#define diag_io_vs_fl_rq_c   diag_io_vs_rq_c [0]
#define diag_io_vs_fr_rq_c   diag_io_vs_rq_c [1]

EXTERN unsigned char diag_io_vs_state_c [2]; //Holds the Vented seat individual LED control value
#define diag_io_vs_fl_state_c   diag_io_vs_state_c [0]
#define diag_io_vs_fr_state_c   diag_io_vs_state_c [1]

/* Io Control Locking Bits. If the following bits are set, the respective functionality is under IO */
EXTERN union char_bit  diag_io_lock_flags_c;
#define diag_io_load_shed_lock_b		diag_io_lock_flags_c.b.b0
#define diag_io_ign_lock_b				diag_io_lock_flags_c.b.b2
#define diag_io_st_whl_rq_lock_b		diag_io_lock_flags_c.b.b7	/* stWheel request lock */

EXTERN union char_bit  diag_io_lock2_flags_c;
#define diag_io_all_vs_lock_b			diag_io_lock2_flags_c.b.b0	/* lock for all vents */
#define diag_io_all_hs_lock_b          	diag_io_lock2_flags_c.b.b4
 
EXTERN union char_bit  diag_io_lock3_flags_c;
#define diag_eol_io_lock_b             	diag_io_lock3_flags_c.b.b0  /* EOL Tests Lock Flag	*/
#define diag_eol_no_fault_detection_b  	diag_io_lock3_flags_c.b.b1  /* EOL Tests running, No Fault detection */
#define diag_io_st_whl_state_lock_b    	diag_io_lock3_flags_c.b.b2	/* stWheel led status lock */
#define diag_io_active_b                diag_io_lock3_flags_c.b.b3 /* diag IO active for Heat/vent/HSW */
#define diag_failsafe_cond_b           	diag_io_lock3_flags_c.b.b4 	/*If set, do not detect LOC */
#define diag_load_def_eemap_b 	        diag_io_lock3_flags_c.b.b5  /*Is set, load default eeprom map */
#define diag_load_bl_eep_backup_b       diag_io_lock3_flags_c.b.b6  /*Is set, Bootloader backup will be written */

EXTERN union char_bit   diag_io_hs_lock_flags_c[ 4 ];
#define switch_rq_lock_b              b0   /* individual lock flags for heated seats*/
#define led_status_lock_b             b1   /* Final fault flag for short to gnd Detection*/

EXTERN union char_bit   diag_io_vs_lock_flags_c[ 2 ];
#define switch_rq_lock_b              b0   /* individual lock flags for vented seats*/
#define led_status_lock_b             b1   /* led status lock for vented seats */

/*****************************************************************************************************
*     								FUNCTIONS
******************************************************************************************************/
//void diagIOCtrlF_DataID_2FH(enum DIAG_RESPONSE* Response, u_temp_l* DataLen, DescMsgContext* pMsgContext);

//void diagF_DiagResponse(enum DIAG_RESPONSE Response, u_temp_l Length, DescMsgContext* pMsgContext);
#endif /*DIAGIOCTRL_H_*/
