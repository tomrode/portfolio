



/********************************************************************************/
/*                        A U T H O R   I D E N T I T Y                                                 */
/********************************************************************************/
/* Initials      Name                   Company                                                                 */
/* --------      --------------------   --------------------------------------- */ 
/* CK            Can Kulduk             Continental Automotive Systems          */
/* HY            Harsha Yekollu         Continental Automotive Systems          */
/* FU            Fransisco Uribe                Continental Automotive Systems  */
/* CC            Christian Cortes       Continental Automotive Systems          */
/********************************************************************************/


/********************************************************************************/
/*                         CHANGE HISTORY for BATTERY MODULE                    */
/********************************************************************************/
/* 02.12.2007 CK - Module created                                                       */
/* 03.03.2008 HY - Module copied from CSWM MY09 project                         */
/* 09.06.2008 FU - SET_UV_DLY_TM set to 3                                                               */
/* 13.06.2008 FU - BattVF_Mntr_Stat -> OV delay                                                 */
/* 08.25.2008 HY - Added EE_BlockRead/Write calls                               */
/* 09.08.2008 HY - For EE_BlockWrites copying data into local structure         */
/*                 Function BattVF_Copy_Diag_Prms is not being used any more    */   
/* 10.23.2008 CK - Internal fault setting is enabled (BattV_Chck_ErraticV_Rdg)  */
/* 10.23.2008 CK - Modified BattVF_Write_Diag_Data function                     */
/* 10.30.2008 CK - Battery compensation code to avoid setting low battery       */
/*                 voltage fault prematurely is added (BattVLF_CalcCompensation */
/*                 function). BattVF_Mntr_Stat function is modified.            */
/* 10.31.2008 CK - Modified BattVF_AD_to_100mV_Cnvrsn function                  */
/* 11.11.2008 CK - Updated BattVF_Chck_Diag_Prms and BattVF_Write_Diag_Data     */
/*                                 for battery compensation diagnostic writes.                                  */
/* 11.24.2008 CK - Modified BattVF_Manager and BattVF_LdShed_Monitor functions  */
/*                 in order to implement the load shed strategy for Powernet.   */
/* 12.08.2008 CK - Updated BattV_Chck_ErraticV_Rdg function. We set the erratic */
/*                 voltage bit when the system and battery voltage difference is*/
/*                 greater then or equal to default threshold limit (2.5V) for  */
/*                 at least 3 seconds (UPDATE: instead of 2 seconds).           */
/*                 UPDATE: We clear the erratic voltage bit when the difference */
/*                 between system and battery voltage drops below threshold     */
/*                 limit minus 0.5 V (default: 2.5 - 0.5 = 2.0 V).              */
/* 01.22.2009 CK - PC-Lint fixes. No complaints from PC-Lint after fixes.       */
/* 03.20.2009 CK - Customer does not want to have a low system voltage DTC set  */
/*                                 when a load shed 3 and higher or battery reached critical    */
/*                             state signal active is received from CANI-bus for PowerNet   */
/*                                 architecture. Updated BattVF_DTC_Manager accordingly to      */
/*                 meet this new reuirement. MKS ID: 27158                                      */
/* 04.14.2009 CK - Updated BattVF_LdShed_Monitor function to check Batt_ST_Crit */
/*                 (Battery reached critical state) signal for PowerNet         */
/*                                 architecture.                                                                                                */
/* 08.19.2009 CC - Modified BattVF_LdShed_Monitor(void) System level low voltage */ 
/*                 DTC will be blown when we see Load shed 4 signal for more than*/
/*                 2sec. MKS Change Request ID 32286                             */
/* 02.17.2010 HY - Modiifed BattVF_DTC_Manager to not blow System under voltage  */
/*                 when observe Load Shed 2/4                                    */
/*               - Modified BattVF_LdShed_Monitor to have ODO Stamp when         */
/*                 Load Shed 2/4 observed.                                       */
/*               - Both above entires tied to MKS ID 47356                       */
/* 09-28-2010 HY - #Ifdef PWR_NET commented. and code contains Power net logic   */ 
/***********************************************************************************************
 * 06-15-2011	Harsha	 75322	Cleanup the code. Delete the unused code for this program.
 *								Deleted function is: BattVF_LdShed_Monitor 
 * 08-08-2011   Harsha   81842	BattVF_AD_to_100mv_cnvrsn reverted back to old formula.
 * 
 * 
 * 
 * 
 * 
 * 
 ***********************************************************************************************/


/********************************************************************************/
/* Software Design Text                                                         */
/* According to Functional Specification, the CSWM shall operate over the       */
/* voltage range 10.0V - 16.0V.                                                         */
/* This module monitors the system and local battery voltage readings, load shed*/
/* status and matures/de-matures appropriate voltage faults.                                    */
/********************************************************************************/


/********************************** @Include(s) **********************************/
#include "TYPEDEF.h"
#include "BattV.h"   
#include "main.h"
#include "canio.h"
#include "AD_Filter.h"
#include "DiagWrite.h"
#include "FaultMemory.h"
#include "FaultMemoryLib.h"
#include "mw_eem.h"
#include "Internal_faults.h"
#include "Venting.h"
#include "Heating.h"
#include "StWheel_Heating.h"
/********************************************************************************/


/********************************* external data *********************************/
/* N                                                                             
 * O
 * N
 * E */
/*********************************************************************************/


/*********************************** @ variables *********************************/
u_16Bit BattV_set_UV_tm;              // stores the set UV fault delay time for chosen timeslice
u_16Bit BattV_flt_mtr_tm;             // stores fault mature time for chosen timeslice
u_16Bit BattV_de_mtr_tm;              // stores fault de-mature time for chosen timeslice

u_16Bit BattV_ADC_Uint;               // local battery voltage (AtoD converted)

u_16Bit BattV_ldShed_flt_cnt_w;       // Load shed fault mature count

u_16Bit BattV_ldShed4_2secDelay_cnt_w;       // Load shed_4 2 seconds Delay count  Change Request ID 32286

u_16Bit BattV_Erratic_V_cnt_w;        // Erratic voltage reading mature time

u_8Bit  BattV_slct;                   // Selects the Battery voltage type for DTC management

//u_8Bit  BattV_diag_prog_data_c[BATTV_DATA_LENGHT];    //Write Data by LID data array NOT USED ANYMORE

union char_bit BattV_Flags1_c;        // Battery voltage flags
#define load_shed_fault_active_b      BattV_Flags1_c.b.b0  // Load shed flt. status (sets UV fault)
//#define loadshed2_odostamp_read_b     BattV_Flags1_c.b.b1  // Load shed 2 Odo Stamp store status
//#define loadshed4_odostamp_read_b     BattV_Flags1_c.b.b2  // Load shed 4 Odo Stamp store status

u_8Bit  BattV_compensation_c;             // variable that stores the present battery compensation needed 
u_8Bit  BattV_prev_compnsation_c;         // stores the previous battery compensation needed 
u_8Bit  BattV_nmbr_of_actv_outputs;   // Used in calculating battery compensation
u_8Bit  BattV_compensation_a[BATTV_COMPENSATION_SIZE];  // stores D-Flash defaults for battery compensation data
u_8Bit  BattV_ldShd_Mnt_stat_c;           // Load shed monitor status. To be used in mainF_CSWM_Status function
/*********************************************************************************/


/******************************** @Data Structures ********************************/
/* Declaration of voltage calibration parameters */
BattV_s BattV_cals;

/* Battery parameter sets */
BattV_prms BattV_set[TYPE_LMT];         // Parameter sets (one for system voltage, one for battery voltage)
/*********************************************************************************/


/*********************************************************************************/
/* BattVF_Init                                                                                                                                   */
/* Initiliazes the battery module variables after each power on reset.           */
/* Note: System and Battery voltage status are initiliazed as V_STAT_GOOD.       */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* main :                                                                        */
/*    BattVF_Init()                                                              */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* EE_BlockRead(EEBlockLocation elocation, u_8Bit *pbydest)                                  */
/*                                                                                                                                                               */
/*********************************************************************************/
@far void BattVF_Init(void)
{
	

	/* Copy all Battery Calibration data into local structure */
	EE_BlockRead(EE_BATTERY_CALIB_DATA, (u_8Bit*)&BattV_cals.Thrshld_a[SET_OV_LMT]);
	
	/* Copy battery compensation data into local array */
	EE_BlockRead(EE_BATT_COMPENSATION_DATA, (u_8Bit*)&BattV_compensation_a[NO_OUTPUT_ON]);
	
	/* Init Voltage Status as good */
	BattV_set[SYSTEM_V].status_c    = V_STAT_GOOD;
	BattV_set[BATTERY_V].status_c   = V_STAT_GOOD;
	
	/* No current faults */
	BattV_set[SYSTEM_V].flt_stat_c  = V_NO_FLT;
	BattV_set[BATTERY_V].flt_stat_c = V_NO_FLT;
	
	/* No previous faults */
	BattV_set[SYSTEM_V].prvs_flt_stat_c  = V_NO_FLT;
	BattV_set[BATTERY_V].prvs_flt_stat_c = V_NO_FLT;
	
	/* load Shed monitor status */
	BattV_ldShd_Mnt_stat_c = LDSHD_MNT_GOOD;
	
	/* Local Voltage timing variables */
	BattV_set_UV_tm = BattV_cals.Timing_a[SET_UV_DLY_TM] * BATTV_MDLTN; // Set UV delay time (3*25)
	BattV_flt_mtr_tm = BattV_cals.Timing_a[FLT_MTR_TM] * BATTV_MDLTN;   // Fault mature time (60*25)
	BattV_de_mtr_tm = BattV_cals.Timing_a[FLT_DE_MTR_TM] * BATTV_MDLTN; // Fault de-mature time (15*25)
	
	BattV_slct = BATTERY_V;        // Select voltage type for DTC management
}


/*********************************************************************************/
/* BattVF_Clr_Flt_Times                                                                                                                  */
/* Clears fault mature/de-mature times. It also clears the set under voltage time*/
/* (3 second delay is used before turning off the outputs in case of under       */
/* voltage dips).                                                                                                                                */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* BattVF_Mntr_Stat :                                                            */
/*    BattVF_Clr_Flt_Times(unsigned char BattV_type)                             */
/* BattVF_Updt_Flt_Stat :                                                        */
/*    BattVF_Clr_Flt_Times(unsigned char BattV_type)                             */
/*                                                                                                                                                               */
/*********************************************************************************/
@far void BattVF_Clr_Flt_Times(BattV_type_e Voltage_type)
{
	/* Clear fault counts */
	BattV_set[Voltage_type].set_UV_cnt_w     = 0;
	BattV_set[Voltage_type].flt_mtr_cnt_w    = 0;
	BattV_set[Voltage_type].flt_de_mtr_cnt_w = 0;
}


/*********************************************************************************/
/* BattVF_Clear                                                                                                                                  */
/* Clears all battery variables if ignition status is other then run or start.   */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* BattVF_Manager :                                                              */
/*    BattVF_Clear()                                                             */
/*                                                                                                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/*    BattVF_Clr_Flt_Times (BattV_type_e Voltage_type)                           */
/*                                                                                                                                                               */
/*********************************************************************************/
@far void BattVF_Clear(void)
{

	/* Init Voltage Status as good */
	BattV_set[SYSTEM_V].status_c    = V_STAT_GOOD;
	BattV_set[BATTERY_V].status_c   = V_STAT_GOOD;
	
	/* No current faults */
	BattV_set[SYSTEM_V].flt_stat_c  = V_NO_FLT;
	BattV_set[BATTERY_V].flt_stat_c = V_NO_FLT;
	
	/* No previous faults */
	BattV_set[SYSTEM_V].prvs_flt_stat_c  = V_NO_FLT;
	BattV_set[BATTERY_V].prvs_flt_stat_c = V_NO_FLT;
	
	/* load Shed monitor status */
	BattV_ldShd_Mnt_stat_c = LDSHD_MNT_GOOD;
	
	BattV_ldShed_flt_cnt_w = 0;       // Clear Load shed fault mature count
	BattV_ldShed4_2secDelay_cnt_w = 0; // Clear load Shed_4 2 sec delay count
	
	BattV_Erratic_V_cnt_w = 0;        // Clear Erratic voltage reading mature time
	
	/* Clear system voltage fault times */
	BattVF_Clr_Flt_Times(SYSTEM_V);
	
	/* Clear battery voltage fault times */
	BattVF_Clr_Flt_Times(BATTERY_V);
}


/*********************************************************************************/
/* BattVF_AD_Rdg                                                                                                                                 */
/* Perform the AD conversion to read local battery voltage (Uint on AD channel7).*/
/* It stores the filtered battery volateg AD result in BattV_ADC_Uint variable.  */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* main :                                                                        */
/*    BattVF_AD_Rdg()                                                            */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* ADF_AtoD_Cnvrsn(u_8Bit AD_Filter_supply)                                      */
/* ADF_Str_UnFlt_Rslt(u_8Bit AD_Filter_control)                                  */
/* ADF_Filter(u_8Bit AD_Filter_ch)                                               */
/*                                                                                                                                                               */
/*********************************************************************************/
@far void BattVF_AD_Rdg(void)
{
	/* Analog to digital conversion for AD channel 7 (Unint) */
	ADF_AtoD_Cnvrsn(CH7);
	
	/* Store the unfiltered result for AD channel */
	ADF_Str_UnFlt_Rslt(CH7);
	
	/* Filter the read AD channel */
	ADF_Filter(CH7);
	
	/* Store filtered result */
	BattV_ADC_Uint = AD_Filter_set[CH7].flt_rslt_w;
}


/*********************************************************************************/
/* BattVF_AD_to_100mV_Cnvrsn                                                                                                     */
/* Input parameter:                                                              */
/* BattV_AD_value: 10-bit AD value that needs to be converted to 100mV units     */
/*                                                                               */
/* Return parameter:                                                             */
/* BattV_100mV_c: 8-bit Battery voltage value in 100mV units                     */
/*                                                                               */
/* Hardware formula:                                                             */
/* Vuint[V] = ( (AD*3.4*5)/1023 ) + 0.7                                          */
/*                                                                               */
/* Software formula:                                                             */
/* Vuint[100mV] = ( ( ( (AD*42)+(AD/2) ) /256) + 8)                              */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* BattVF_Str_Crt_Vltg_Rdg :                                                     */
/*    u_8Bit BattVF_AD_to_100mV_Cnvrsn(u_16Bit BattV_AD_value)                   */
/* TempF_Chck_NTC_SupplyV :                                                      */
/*    u_8Bit BattVF_AD_to_100mV_Cnvrsn(u_16Bit BattV_AD_value)                   */
/*                                                                                                                                                               */
/*********************************************************************************/
@far u_8Bit BattVF_AD_to_100mV_Cnvrsn(u_16Bit BattV_AD_value)
{
	u_16Bit BattV_Uint_AD;      // local varible that stores the Uint AD value
	u_8Bit  BattV_100mV_c;      // 100mV equivalent of the AD reading
	u_16Bit temp_c;

	/* Store the AD value */
	BattV_Uint_AD = BattV_AD_value;

	//MY09 and MY10 CSWM COde */
	/* @Conversion from Uint AD to Uint 100mV */
//	/* OLD: Voltage in 100mV: ( ( ( (Uint AD*42) + (Uint AD/2) ) / 256) + 8) */
	BattV_100mV_c = (u_8Bit) ( ( ( (BattV_Uint_AD*BATTV_N42)+(BattV_Uint_AD>>1) ) >> 8) + /*BATTV_N7*/ BATTV_N8);

	
//	temp_c = (u_8Bit) ( ( ( (BattV_Uint_AD*BATTV_N42)+(BattV_Uint_AD>>1) ) >> 8) + /*BATTV_N7*/ BATTV_N8);
//	/* New FIAT Conversion: 0.16v = 1 */
//	/* BattV_100mV_c = ( (BattV_100mV_c /16) * 10)*/
//	BattV_100mV_c = (u_8Bit)( (temp_c * 10) / 16);
	
	return(BattV_100mV_c);
}


/*********************************************************************************/
/* BattVF_Updt_Flt_Stat                                                                                                                  */
/* Input parameters:                                                             */
/* Voltage_Type: System or battery voltage                                       */
/* Volt_Fault_Type: Over or under voltage fault                                  */
/*                                                                               */
/* Updates the voltage fault status for the selected type.                       */
/* It set a preliminary voltage fault until mature time is reached (default 60   */
/* seconds mature time). Then, it matures the appropriate voltage fault.                 */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* BattVF_Mntr_Stat :                                                            */
/*        BattVF_Updt_Flt_Stat(Voltage_type, Volt_Fault_type)                                    */
/* BattVF_Rcv_OV :                                                               */
/*    BattVF_Updt_Flt_Stat(Voltage_type, Volt_Fault_type)                        */
/* BattVF_Rcv_UV :                                                               */
/*    BattVF_Updt_Flt_Stat(Voltage_type, Volt_Fault_type)                        */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* BattVF_Clr_Flt_Times(unsigned char BattV_type)                                */
/*                                                                                                                                                               */
/*********************************************************************************/
@far void BattVF_Updt_Flt_Stat(BattV_type_e Voltage_type, BattV_Flt_Type_e Volt_Fault_type)
{
	/* Store the previous voltage status */
	BattV_set[Voltage_type].prvs_flt_stat_c = BattV_set[Voltage_type].flt_stat_c;
	
	/* @Update Fault Status */
	/* Check if voltage stayed bad for the fault mature time */
	if ( (BattV_set[Voltage_type].flt_mtr_cnt_w < (BattV_flt_mtr_tm-1)) && (BattV_set[Voltage_type].status_c != V_STAT_UGLY) ) {
		

		/* Preliminary Voltage Faults */
		/* Keep counting until the voltage fault is matured */
		BattV_set[Voltage_type].flt_mtr_cnt_w++;
		
		/* Clear fault de-mature time */
		BattV_set[Voltage_type].flt_de_mtr_cnt_w = 0;
		
		/* Check the fault type */
		if (Volt_Fault_type == OVER_VOLTAGE_FAULT) {
			/* Set preliminary OV fault */
			BattV_set[Voltage_type].flt_stat_c = PRL_OV_FLT;
		}
		else {
			/* Set preliminary UV fault */
			BattV_set[Voltage_type].flt_stat_c = PRL_UV_FLT;
		}
	}
	else {
		/* Matured Voltage Faults */
		/* Check the fault type */
		if (Volt_Fault_type == OVER_VOLTAGE_FAULT) {
			/* Set matured OV fault */
			BattV_set[Voltage_type].flt_stat_c = MTR_OV_FLT;
		}
		else {
			/* Set matured UV fault */
			BattV_set[Voltage_type].flt_stat_c = MTR_UV_FLT;
		}
	}
	

	/* @Clear Fault Count if applicable */
	if (BattV_set[Voltage_type].prvs_flt_stat_c != BattV_set[Voltage_type].flt_stat_c) {
		/* Clear fault counts */
		BattVF_Clr_Flt_Times(Voltage_type);
	}
}


/*********************************************************************************/
/* BattVF_Rcv_UV                                                                                                                                 */
/* Input parameter:                                                              */
/* Voltage type: System or battery voltage                                       */
/*                                                                               */
/* Checks the recovery from an under voltage condition.                          */
/* If voltage reading stays above the clear under voltage threshold (default     */
/* 10.5V) for the de-mature time (default 15 seconds), then voltage fault is     */
/* de-matured succesfully.                                                                                                               */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* BattVF_Mntr_Stat :                                                            */
/*    BattVF_Rcv_UV(BattV_type_e Voltage_type)                                   */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* BattVF_Updt_Flt_Stat(Voltage_type, Volt_Fault_type)                           */
/*                                                                                                                                                           */
/*********************************************************************************/
@far void BattVF_Rcv_UV(BattV_type_e Voltage_type)
{
	/* Check the recovery condition to clear the UV fault */
	if (BattV_set[Voltage_type].crt_vltg_c > BattV_cals.Thrshld_a[CLR_UV_LMT]) {

		/* Check if voltage stayed good for the de-mature time */
		if (BattV_set[Voltage_type].flt_de_mtr_cnt_w < (BattV_de_mtr_tm-1)) {
			/* Keep the same voltage status. Count the de-mature time */
			BattV_set[Voltage_type].flt_de_mtr_cnt_w++;
			/* Clear fault mature time */
			BattV_set[Voltage_type].flt_mtr_cnt_w = 0;
		}
		else {
			/* Update voltage status: No faults */
			BattV_set[Voltage_type].flt_stat_c = V_NO_FLT;
		}
	}
	else {
		/* Recovery is not possible. Keep updating the fault status in order to mature the fault. */
		BattVF_Updt_Flt_Stat(Voltage_type, UNDER_VOLTAGE_FAULT); 
		/* Clear fault demature time */
		BattV_set[Voltage_type].flt_de_mtr_cnt_w = 0;
	}
}

/*********************************************************************************/
/* BattVF_Rcv_OV                                                                                                                                 */
/* Input parameter:                                                              */
/* Voltage type: System or battery voltage                                       */
/*                                                                               */
/* Checks the recovery from an over voltage condition.                           */
/* If voltage reading stays below the clear over voltage threshold (default      */
/* 15.5V) for the de-mature time (default 15 seconds), then voltage fault is     */
/* de-matured succesfully.                                                                                                               */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* BattVF_Mntr_Stat :                                                            */
/*    BattVF_Rcv_OV(BattV_type_e Voltage_type)                                   */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* BattVF_Updt_Flt_Stat(Voltage_type, Volt_Fault_type)                           */
/*                                                                                                                                                           */
/*********************************************************************************/
@far void BattVF_Rcv_OV(BattV_type_e Voltage_type)
{
	/* Check the recovery condition to clear the OV fault */
	if (BattV_set[Voltage_type].crt_vltg_c < BattV_cals.Thrshld_a[CLR_OV_LMT]) {
		/* Check if voltage stayed good for the de-mature time */
		if (BattV_set[Voltage_type].flt_de_mtr_cnt_w < (BattV_de_mtr_tm-1)) {
			/* Keep the same voltage status. Count the de-mature time */
			BattV_set[Voltage_type].flt_de_mtr_cnt_w++;
			
			/* Clear fault mature time */
			BattV_set[Voltage_type].flt_mtr_cnt_w =0;
		}
		else {
			/* Update voltage status: No faults */
			BattV_set[Voltage_type].flt_stat_c = V_NO_FLT;
		}
	}
	else {
		/* Recovery is not possible. Keep updating the fault status in order to mature the fault. */
		BattVF_Updt_Flt_Stat(Voltage_type, OVER_VOLTAGE_FAULT); 
		
		/* Clear fault demature time */
		BattV_set[Voltage_type].flt_de_mtr_cnt_w = 0;
	}
}

/*********************************************************************************/
/*                        BattVLF_CalcCompensation                               */
/*********************************************************************************/
/* Function Name: @far void BattVF_CalcCompensation(void)                                                */
/* Type: Local Function                                                                                                          */
/* Returns: u_8Bit (battery voltage compensation needed)                                                 */
/* Parameters: None                                                                                                                              */
/* Description: This function calculates the battery compensation needed to set  */
/*              low battery voltage fault (not used for system voltage) when 'n' */
/*              number of CSWM outputs are active at high level.                 */
/*                                                                                                                                                               */
/*              There is certain amount of voltage drop CSWM senses when outputs */
/*              are active. We decided to test this strategy to avoid setting a  */
/*              low battery voltage DTC pre-maturely. This should also help us   */
/*              with a theoretical scenario where CSWM toogles its outputs       */
/*                              on and off because while the outputs are on we are below low     */
/*              voltage limit and when we turn off the outputs during 1 minute   */
/*                              low battery voltage mature time our voltage jumps back up the    */
/*              operational range.                                                                                               */
/*                                                                                                                                                           */
/* Calls to: None                                                                                                                                */
/* Calls from:                                                                                                                                   */     
/*    BattVF_Mntr_Stat()                                                                                                                 */
/*                                                                                                                                                               */ 
/*********************************************************************************/
@far void BattVLF_CalcCompensation(void)
{
	/* local variables */
	u_8Bit nmbr_of_actv_vs = 0;
	u_8Bit nmbr_of_actv_hs = 0;     
	u_8Bit stW_active = 0;
	
	/* Reset total number of active outputs to zero */
	BattV_nmbr_of_actv_outputs = 0;
	
	/* Get the number of heater outputs that are turned on at highest level (HL3) */
	/* Exception with vents. At VS_LOW there is still considerable amount of voltage drop. */
	/* Get the heated steering wheel status (TRUE if we are at WL3, Wl2, or WL1) */
	nmbr_of_actv_vs = vsF_Get_Actv_outputNumber();  // get number of active vents at VS_LOW or VS_HI
	nmbr_of_actv_hs = hsF_GetActv_OutputNumber();   // get number of active heats at HL3
	stW_active = stWF_Actvtn_Stat();                                // Check if steering wheel is turned ON (at any level WL1, WL2, or WL3)
	
	/* total number of vents and heats active at highest level */
	BattV_nmbr_of_actv_outputs = (nmbr_of_actv_vs + nmbr_of_actv_hs);
	
	/* Check if steering wheel is active */
	if (stW_active == TRUE) {
		/* Steering wheel is active */
		/* Check the number of active heat/vent outputs and update compensation */
		switch (BattV_nmbr_of_actv_outputs) {
			

			case BATTV_NO_OUTPUT_ACTIVE:
			     /* Only Steering Wheel is On */
			{
				BattV_compensation_c = BattV_compensation_a[ONLY_STW_OUTPUT_ON];
				break;
			}
			

			case BATTV_1_OUTPUT_ACTIVE:
			case BATTV_2_OUTPUTS_ACTIVE: 
			     /* Steering wheel + one or two heater/vent outputs are on */
			{
				BattV_compensation_c = BattV_compensation_a[STW_PLUS_TWO_OUTPUTS_ON];
				break;
			}
			

			case BATTV_3_OUTPUTS_ACTIVE:
			case BATTV_4_OUTPUTS_ACTIVE: 
			     /* Steering wheel + three or four heater/vent outputs are on */
			{
				BattV_compensation_c = BattV_compensation_a[STW_PLUS_FOUR_OUTPUTS_ON];
				break;
			}
			
			default :
			{
				BattV_compensation_c = BattV_compensation_a[ONLY_STW_OUTPUT_ON];
			}
		}
	}
	else {
		/* Steering wheel is not active */
		/* Check the number of active heat/vent outputs and update compensation */
		switch (BattV_nmbr_of_actv_outputs) {
			

			case BATTV_NO_OUTPUT_ACTIVE:
			     /* No Output is turned ON */
			{
				BattV_compensation_c = BattV_compensation_a[NO_OUTPUT_ON];
				break;
			}
			

			case BATTV_1_OUTPUT_ACTIVE: 
			     /* Only one heater or one vent output is ON */
			{
				BattV_compensation_c = BattV_compensation_a[ONE_OUTPUT_ON];
				break;
			}
			

			case BATTV_2_OUTPUTS_ACTIVE: 
			case BATTV_3_OUTPUTS_ACTIVE:
			case BATTV_4_OUTPUTS_ACTIVE:
			     /* Two, three or four outputs are ON (heater and/or vent)  */
			{
				BattV_compensation_c = BattV_compensation_a[MULTIPLE_OUTPUTS_ON];
				break;
			}
			

			default :
			{
				BattV_compensation_c = BattV_compensation_a[NO_OUTPUT_ON];
			}
		}
	}
	

	/* Check if present compensation value is different then previous one */
	if (BattV_compensation_c != BattV_prev_compnsation_c) {
		

		/* Compensation has changed. Meaning, number of outputs that are turned on is changed. */
		/* We need to wait for Battery voltage reading (AD conversion) to catch up in order to not to set a */
		/* preliminary fault which will disable the outputs for 15 seconds! Otherwise preliminary fault will */
		/* be set immedietly because we have waited 3 seconds (set_UV_cnt_w) to get here already. */ 
		
		/* So, we can modify the set_UV_cnt_w value here to give us 160ms for AD reading to catch up to actual */
		/* battery voltage reading */
		if (BattV_set[BATTERY_V].set_UV_cnt_w == BattV_set_UV_tm) {
			/* 4*40ms = 160ms wait time to update fault status when the compensation is changed. */
			BattV_set[BATTERY_V].set_UV_cnt_w = (BattV_set[BATTERY_V].set_UV_cnt_w - BATTV_N4);
		}
		else {
			/* We should never come here. */
		}
	}
	else {
		/* No change occured in the compensation value. No action needed. */
	}
	

	/* Update previous compensation */
	BattV_prev_compnsation_c = BattV_compensation_c;
}
// end of BattVLF_CalcCompensation 

/*********************************************************************************/
/* BattVF_Mntr_Stat                                                                                                                      */
/* Input parameter:                                                              */
/* Voltage_type: System or battery voltage selection                             */
/*                                                                               */
/* This function monitors the system and battery voltage readings.               */
/* If any of the voltage types get out of the operation range (default range:    */
/* 10.0V to 16.0V), then it starts updating the voltage fault status for the     */
/* selected type.                                                                                                                                */
/*                                                                               */
/* This function also supports the recovery from an under or over voltage faults */
/* within the same ignition cycle.                                               */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* BattVF_Manager :                                                              */
/*    BattVF_Mntr_Stat(BattV_type_e Voltage_type)                                */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* BattVF_Updt_Flt_Stat(Voltage_type, Volt_Fault_type)                           */
/* BattVF_Rcv_OV(BattV_type_e Voltage_type)                                      */
/* BattVF_Rcv_UV(BattV_type_e Voltage_type)                                      */
/* BattVF_Clr_Flt_Times(unsigned char BattV_type)                                */
/*                                                                                                                                                               */
/*********************************************************************************/
@far void BattVF_Mntr_Stat(BattV_type_e Voltage_type)
{
	/* Check if voltage reading is not in the operation range */
	if ( (BattV_set[Voltage_type].crt_vltg_c > BattV_cals.Thrshld_a[SET_OV_LMT]) || (BattV_set[Voltage_type].crt_vltg_c < BattV_cals.Thrshld_a[SET_UV_LMT]) ) {
		
		if ( (BattV_set[Voltage_type].set_UV_cnt_w < BattV_set_UV_tm) && (BattV_set[Voltage_type].status_c == V_STAT_GOOD) ) {
			/* Wait set UV fault delay time */
			BattV_set[Voltage_type].set_UV_cnt_w++;
		}
		else {
			/* Check for OV fault */
			if (BattV_set[Voltage_type].crt_vltg_c > BattV_cals.Thrshld_a[SET_OV_LMT]) {
				/* Over Voltage Fault */
				BattVF_Updt_Flt_Stat(Voltage_type, OVER_VOLTAGE_FAULT);
			}
			else {
				/* Modified 10.30.2008 */
				if (Voltage_type == SYSTEM_V) {
					/* Under Voltage Fault */
					BattVF_Updt_Flt_Stat(Voltage_type, UNDER_VOLTAGE_FAULT);
				}
				else {
					/* Calculate battery compensation needed */
					BattVLF_CalcCompensation();                                             
					
					/* Check if we need to mature Under Voltage Fault */
					/* Also check to UV delay time */
					if ( (BattV_set[Voltage_type].crt_vltg_c < (BattV_cals.Thrshld_a[SET_UV_LMT] - BattV_compensation_c) ) && 
					   ( (BattV_set[Voltage_type].set_UV_cnt_w == BattV_set_UV_tm) || (BattV_set[Voltage_type].status_c != V_STAT_GOOD) ) ) {
						/* Under Voltage Fault */
						BattVF_Updt_Flt_Stat(Voltage_type, UNDER_VOLTAGE_FAULT);
					}
					else {
						/* Do not set the battery under voltage fault */
						/* Compensation for voltage drop when the outputs are on */
					}
				}
			}
		}
	}
	else {
		switch (BattV_set[Voltage_type].flt_stat_c) {
			
			case PRL_OV_FLT:
			case MTR_OV_FLT:
			{
				/* Check for recovery from OV faults */
				BattVF_Rcv_OV(Voltage_type);
				break;
			}
			
			case PRL_UV_FLT:
			case MTR_UV_FLT:
			{
				/* Check for recovery from UV faults */
				BattVF_Rcv_UV(Voltage_type);
				break;
			}
			
			default :
			{
				/* No fault is present */
				BattV_set[Voltage_type].flt_stat_c = V_NO_FLT;
				
				/* Clear fault counts */
				BattVF_Clr_Flt_Times(Voltage_type);
				
				/* Clear previous fault status */
				BattV_set[Voltage_type].prvs_flt_stat_c = V_NO_FLT;
				
				/* Added on 10.30.2008: Clear battery compensation variables */
				if (Voltage_type == BATTERY_V) {
					BattV_compensation_c = 0;
					BattV_prev_compnsation_c = 0;
					BattV_nmbr_of_actv_outputs = 0;         // Added on 12.08.2008
				}
				else {
					/* Nothing to do for system voltage */
				}
			}
		}
	}
}


/*********************************************************************************/
/* BattVF_Get_Status                                                                                                                     */
/* Input parameter:                                                              */
/* Voltage_type: System or battery voltage selection                             */
/* Note: Load shed fault status is also monitored.                               */
/*                                                                               */
/* This function updates the selected voltage status.                            */
/* There are four available states for voltage status:                           */
/* 1. V_STAT_UNDEFINED: Dummy value for undefined status                         */
/* 2. V_STAT_GOOD: No preliminary nor matured voltage faults (Voltage has been   */
/* in operational range for at least 15 seconds)                                                                 */
/* 3. V_STAT_BAD: Preliminary voltage fault is present                           */
/* 4. V_STAT_UGLY: Matured voltage fault is present                              */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* mainF_CSWM_Status :                                                           */
/*    u_8Bit BattVF_Get_Status(BattV_type_e Voltage_type)                        */
/* TempF_PCB_NTC_Flt_Monitor :                                                   */
/*    u_8Bit BattVF_Get_Status(BattV_type_e Voltage_type)                        */
/*                                                                                                                                                               */
/*********************************************************************************/
@far u_8Bit BattVF_Get_Status(BattV_type_e Voltage_type)
{
	/* @Update Voltage Status */
	switch (BattV_set[Voltage_type].flt_stat_c) {
		
		case V_NO_FLT:
		{
			BattV_set[Voltage_type].status_c = V_STAT_GOOD;
			break;
		}
		
		case PRL_OV_FLT:
		case PRL_UV_FLT:
		{
			BattV_set[Voltage_type].status_c = V_STAT_BAD;
			BattV_set[Voltage_type].set_UV_cnt_w = 0;      // prelim. fault is set - clear delay time to set UV
			break;
		}
		

		case MTR_OV_FLT:
		case MTR_UV_FLT:
		{
			BattV_set[Voltage_type].status_c = V_STAT_UGLY;
			BattV_set[Voltage_type].set_UV_cnt_w = 0;       // clear set UV delay time
			BattV_set[Voltage_type].flt_mtr_cnt_w = 0;      // clear fault mature time
			break;
		}
		
		default :
		{
			BattV_set[Voltage_type].status_c = V_STAT_UNDEFINED;
			BattVF_Clr_Flt_Times(Voltage_type);
		}
	}
	
	/* Check load shed status */
	if (Voltage_type == SYSTEM_V) {
		/* Check load shed fault flag */
		if (load_shed_fault_active_b == TRUE) {
			/* System voltage status is ugly */
			BattV_set[SYSTEM_V].status_c = V_STAT_UGLY;
		}
		else {
			/* Update with fault status */
		}
	}
	else {
		/* No update needed for local battery voltage */
	}
	return(BattV_set[Voltage_type].status_c);
}


/*********************************************************************************/
/* BattVF_Str_Crt_Vltg_Rdg                                                                                                               */
/* This function stores the most recent battery voltage reading.                 */
/* It also checks for a FCM node timeout. If FCM is present on the bus, it stores*/
/* the system voltage reading.                                                                                                   */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* BattVF_Manager :                                                              */
/*    BattVF_Str_Crt_Vltg_Rdg()                                                  */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* u_8Bit BattVF_AD_to_100mV_Cnvrsn(u_16Bit BattV_AD_value)                      */
/*                                                                               */
/*********************************************************************************/
@far void BattVF_Str_Crt_Vltg_Rdg(void)
{
	
	/* Store the most recent local battery voltage reading in 100mV units */
	BattV_set[BATTERY_V].crt_vltg_c = BattVF_AD_to_100mV_Cnvrsn(BattV_ADC_Uint);
	
	/* Check FCM Timeout */
	//if (Timeout_BATT_VOLT) {
		/* System voltage timeout - do not store the system voltage reading */
  //	}
  //	else {
		/* No Timeout - store the most recent system voltage reading */
		BattV_set[SYSTEM_V].crt_vltg_c = canio_RX_BatteryVoltageLevel_c;
  //	}
}


/*********************************************************************************/
/* BattV_Chck_ErraticV_Rdg                                                                                                               */
/* This function sets an internal bit, which does not set an internal fault, if  */
/* the difference between system and battery voltage reading is greater then or  */
/* equal to threshold limit (default 2.5V) for at least three seconds.           */
/*                                                                               */
/* Erratic voltage bit is cleared when the difference between system and battery */
/* voltage is less then threshold limit - 0.5 V (Default: 2.5 - 0.5 = 2.0 V).    */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* BattVF_Manager :                                                              */
/*    BattV_Chck_ErraticV_Rdg()                                                  */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* EE_BlockRead(EEBlockLocation elocation, u_8Bit *pbydest)                      */
/* IntFlt_F_Update(u_8Bit Byte,u_8Bit Bit_mask,Int_Flt_Op_Type_e intFlt_Op)      */
/*********************************************************************************/
@far void BattV_Chck_ErraticV_Rdg(void)
{
	/* Local variable(s) */
	u_8Bit BattV_difference_c;    //stores the voltage difference between SYSTEM and BATTERY voltage
	
	/* Read the erratic voltage byte */
	EE_BlockRead(EE_INTERNAL_FLT_BYTE3, (u_8Bit*)&intFlt_bytes[THREE]);
	
	/* Find the voltage difference between SYSTEM and BATTERY voltage readings */
	if (BattV_set[SYSTEM_V].crt_vltg_c >= BattV_set[BATTERY_V].crt_vltg_c) {
		/* Difference = System - Battery */
		BattV_difference_c = BattV_set[SYSTEM_V].crt_vltg_c - BattV_set[BATTERY_V].crt_vltg_c;
	}
	else {
		/* Difference = Battery - System */
		BattV_difference_c = BattV_set[BATTERY_V].crt_vltg_c - BattV_set[SYSTEM_V].crt_vltg_c;
	}
	

	/* Check voltage difference between system and local battery voltage */
	if (BattV_difference_c < BattV_cals.Thrshld_a[INTERNAL_FLT_LMT]) {
		/*  Clear erratic voltage reading fault count to insure 3 second before we set the erratic voltage bit */
		BattV_Erratic_V_cnt_w = 0;
		
		/* Voltage difference between system and battery voltage readings is negligable. */
		/* Check if we have to clear the erratic voltage reading occurance */
		if ( (intFlt_bytes[THREE] & ERRATIC_LOCAL_VOLTAGE_MASK) == ERRATIC_LOCAL_VOLTAGE_MASK) {
			/* Check for valid erratic voltage threshold. It has to be greater then or equal to 0.5V. */
			if (BattV_cals.Thrshld_a[INTERNAL_FLT_LMT] >= BATTV_HALF_A_VOLT) {
				/* Check if the difference between system and battery voltage is decreased by at least 0.6V */
				if (BattV_difference_c < (BattV_cals.Thrshld_a[INTERNAL_FLT_LMT] - BATTV_HALF_A_VOLT) ) {
					/* Clear Erratic Voltage reading occurance */
					IntFlt_F_Update(THREE, CLR_ERRATIC_VOLTAGE_RDG, INT_FLT_CLR);
					
					/* Clear internal fault flag */
					Erratic_Local_Voltage_b = FALSE;
				}
				else {
					/* Do not Clear the Erratic voltage bit yet. */
					/*The difference between system and battery voltage has NOT been decreased by at least 0.6V */
				}
			}
			else {
				/* Threshold is not valid. Conditions are not met to clear the erratic voltage bit */
			}
		}
		else {
			/* Occurance is not set */
		}
	}
	else {
		/* UPDATED on 12.08.2008: Check 3 second mature time */
		if ( (BattV_Erratic_V_cnt_w < BATTV_ERRATIC_V_TM) && (Erratic_Local_Voltage_b == FALSE) ) {
			/* 3 second monitor. Increment fault mature count */
			BattV_Erratic_V_cnt_w++;
		}
		else {
			/* Check if we already updated this occurance */
			if ( (intFlt_bytes[THREE] & ERRATIC_LOCAL_VOLTAGE_MASK) == ERRATIC_LOCAL_VOLTAGE_MASK) {
				/* Already updated the occurance */
			}
			else {
				/* Update known faults */
				IntFlt_F_Update(THREE, ERRATIC_LOCAL_VOLTAGE_MASK, INT_FLT_SET);
				
				/* Clear erratic voltage reading fault count */
				BattV_Erratic_V_cnt_w = 0;
				
				/* Set internal fault flag */
				Erratic_Local_Voltage_b = TRUE;
			}
		}
	}
}

/*********************************************************************************/
/* BattVF_LdShed_Monitor                                                                                                                 */
/* Monitoring of the load shed status is accomplished in this function.          */
/* If load shed 2 status is receved for fault mature time (default 60 seconds),  */
/* ODOSTAMP will be stored in EEPROM. MKS ID 47356                               */
/* If load shed 4 status is received, ODOSTAMP will be stored in EEPROM after 2sec.  */
/* Under voltage fault is stored if load shed 0 status is received (assuming     */
/* system and battery voltage readings are in operational range).                */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* BattVF_Manager :                                                              */
/*    BattVF_LdShed_Monitor()                                                    */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* no calls                                                                      */
/*                                                                                                                                                               */
/*********************************************************************************/
//@far void BattVF_LdShed_Monitor(void)
//{
//	
//	/* Clear load shed fault mature count. This counter is not used for PowerNet */
//	BattV_ldShed_flt_cnt_w = 0;
//		
//	/* Added check for Battery Reached Critical State signal on 04.14.2009 (CK) */
//	/* Check for Load Shed Level 3, Load Shed Level 7 and Battery Reached Critical State signals */
//	if ( (canio_PN14_LS_Lvl3_c == TRUE) || (canio_PN14_LS_Lvl7_c == TRUE) || (canio_Batt_ST_Crit_c == TRUE) ) {
//		/* Set load shed fault active */
//		load_shed_fault_active_b = TRUE;
//			
//		/* Update load shed monitor status to UGLY */
//		BattV_ldShd_Mnt_stat_c = LDSHD_MNT_UGLY;
//	}
//	else {
//		/* No load shed fault for level other then 3 or 7 */
//		load_shed_fault_active_b = FALSE;               
//			
//		/* No load shed fault. Update load shed monitor status to GOOD */
//		BattV_ldShd_Mnt_stat_c = LDSHD_MNT_GOOD;
//	}
//	
//}


/*********************************************************************************/
/* BattVF_Manager                                                                                                                                */
/* This cyclic function uses the load shed status, system and battery voltage    */
/* readings in order to monitor the voltage status.                                                      */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* main :                                                                        */
/*    BattVF_Manager()                                                           */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* BattVF_Str_Crt_Vltg_Rdg()                                                     */
/* BattVF_Mntr_Stat(BattV_type_e Voltage_type)                                   */
/* BattV_Chck_ErraticV_Rdg()                                                     */
/* BattVF_LdShed_Monitor()                                                       */
/* BattVF_Clear()                                                                */
/*                                                                                                                                                               */
/*********************************************************************************/
@far void BattVF_Manager(void)
{
	/* Check ignition status */
	if ( (canio_RX_IGN_STATUS_c == IGN_RUN) || (canio_RX_IGN_STATUS_c == IGN_START) ) {
		/* Store the most recent battery and system (if applicable) voltage readings */
		BattVF_Str_Crt_Vltg_Rdg();
		
		/* Monitor local voltage reading (system and battery voltage reading difference */
		/* is greater then default: set the internal fault bit which does not set a DTC) */
		BattV_Chck_ErraticV_Rdg();
			
		/* 11.24.2208 CK MODIFIED FUNCTION: Monitor Load shed fault */
		//BattVF_LdShed_Monitor(); //Not required
			
		/* Monitor System voltage */
		BattVF_Mntr_Stat(SYSTEM_V);
			
		/* Monitor Battery voltage */
		BattVF_Mntr_Stat(BATTERY_V);
	}
	else {
		/* Clear Battery variables */
		BattVF_Clear();
	}

}


/*********************************************************************************/
/* BattVF_Get_Crt_Uint                                                                                                                   */
/* This function converts local battery voltage AD value to 100mV units.                 */
/*                                                                               */
/* Return parameter:                                                             */
/* BattV_crt_Uint_c: Most recent local battery voltage reading in 100mV units.   */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* rlF_RelayManager :                                                            */
/*    u_8Bit BattVF_Get_Crt_Uint()                                               */
/*                                                                                                                                                               */
/*********************************************************************************/
@far u_8Bit BattVF_Get_Crt_Uint(void)
{
	u_8Bit BattV_crt_Uint_c;        // local variable that stores the current Uint in 100mV uints
	
	/* Store the most recent local battery voltage reading in 100mV units */
	BattV_crt_Uint_c = BattVF_AD_to_100mV_Cnvrsn(BattV_ADC_Uint);
	
	return(BattV_crt_Uint_c);
}


/*********************************************************************************/
/* BattVF_DTC_Manager                                                                                                                    */
/* This function sets the DTC for system and battey voltage.                     */
/* There are two DTCs for each type of voltage rading. Over voltage DTC and Under*/
/* voltage DTC.                                                                                                                                  */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* main :                                                                        */
/*    BattVF_DTC_Manager()                                                       */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* FMemLibF_ReportDtc(DtcType DtcCode,enum ERROR_STATES State)                   */
/*                                                                                                                                                           */
/*********************************************************************************/
@far void BattVF_DTC_Manager(void)
{
	/* Select voltage type for DTC management */

	if (BattV_slct == SYSTEM_V) {
		
		/* Update DTC status for local battery voltage */
		BattV_slct = BATTERY_V;
		/* Under Voltage (Local Battery) */
		/* Check for a matured under voltage fault */
		if (BattV_set[BATTERY_V].flt_stat_c == MTR_UV_FLT) {
			/* Under Voltage DTC is active for Battery Voltage */
			FMemLibF_ReportDtc(DTC_BATTERY_VOLTAGE_MIN_ERROR_K, ERROR_ON_K);
		}
		else {
			/* Under Voltage DTC is not active for Battery Voltage */
			FMemLibF_ReportDtc(DTC_BATTERY_VOLTAGE_MIN_ERROR_K, ERROR_OFF_K);
		}
	}
	else {
		/* Update DTC status for System voltage */
		BattV_slct = SYSTEM_V;  
		/***************************************/
		/**** MKS Change Request ID: 27158 *****/
		/********* Beginning of Change *********/
		/* PowerNet architecture */
		/* Under Voltage (System) */
		/* Check for a matured under voltage fault */
		if ( (BattV_set[SYSTEM_V].flt_stat_c == MTR_UV_FLT) ) {
			/* Under Voltage DTC is active for System Voltage */
			FMemLibF_ReportDtc(DTC_SYSTEM_VOLTAGE_MIN_ERROR_K, ERROR_ON_K);
		}
		else {
			/* Under Voltage DTC is not active for System Voltage */
			FMemLibF_ReportDtc(DTC_SYSTEM_VOLTAGE_MIN_ERROR_K, ERROR_OFF_K);
		}

		/***************************************/
		/**** MKS Change Request ID: 27158 *****/
		/************ End of Change ************/
	}
	

	/* Over Voltage */
	/* Check for a matured over voltage fault */
	if (BattV_set[BattV_slct].flt_stat_c == MTR_OV_FLT) {
		
		/* Check voltage type */
		if (BattV_slct == SYSTEM_V) {

			/* Over Voltage DTC is active for System Voltage */
			FMemLibF_ReportDtc(DTC_SYSTEM_VOLTAGE_MAX_ERROR_K, ERROR_ON_K);
		}
		else {
			/* Over Voltage DTC is active for Battery Voltage */
			FMemLibF_ReportDtc(DTC_BATTERY_VOLTAGE_MAX_ERROR_K, ERROR_ON_K);
		}
	}
	else {
		/* Check voltage type */
		if (BattV_slct == SYSTEM_V) {
			/* Over Voltage DTC is not active for System Voltage */
			FMemLibF_ReportDtc(DTC_SYSTEM_VOLTAGE_MAX_ERROR_K, ERROR_OFF_K);
		}
		else {
			/* Over Voltage DTC is not active for Battery Voltage */
			FMemLibF_ReportDtc(DTC_BATTERY_VOLTAGE_MAX_ERROR_K, ERROR_OFF_K);
		}
	}
}


/*********************************************************************************/
/* BattVF_Copy_Diag_Prms (NOT USED)                                                                                      */
/* Input parameter:                                                              */
/* BattV_data: Pointer to the start of the voltage data that needs to be checked.*/
/*                                                                               */
/* This function copies the new voltage calibration dater into buffer array      */
/* before writing it to non-volatile memory.                                                                     */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* BattVF_Chck_Diag_Prms :                                                       */
/*    BattVF_Copy_Diag_Prms(u_8Bit * BattV_data)                                 */
/*                                                                                                                                                               */
/*********************************************************************************/
//@far void BattVF_Copy_Diag_Prms(u_8Bit * BattV_data)
//{
//
//      u_8Bit j;                // for loop variable
//      u_8Bit * BattV_data_p;   // pointer to battery data that need to be copied
//
//      /* Store the pointer for battery data */
//      BattV_data_p = BattV_data;
//
//      for (j=0; j<BATTV_DATA_LENGHT; j++ ){
//              BattV_diag_prog_data_c[j] = *(BattV_data_p + j);
//      }
//}

/*********************************************************************************/
/* BattVF_Chck_Diag_Prms                                                                                                                 */
/* Input parameters:                                                             */
/* BattV_LID_ser: LID service ID                                                 */
/* BattV_data_pc: Pointer to the start of the voltage data that needs to be      */
/* checked.                                                                              */
/*                                                                               */
/* Return parameter:                                                             */
/* BattV_chck_rslt_c: 1 if check is valid. 0 Otherwise.                          */
/*                                                                               */
/* This function check whether the new voltage calibration data is acceptable or */
/* not. If data is valid, the function return 1, else it returns 0.                      */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* ApplDescProcessWriteDataByIdentifier :                                        */
/*    u_8Bit BattVF_Chck_Diag_Prms(u_8Bit BattV_LID_ser,u_8Bit * BattV_data_pc)  */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* BattVF_Copy_Diag_Prms(u_8Bit * BattV_data)                                    */
/*                                                                                                                                                               */
/*********************************************************************************/
@far u_8Bit BattVF_Chck_Diag_Prms(u_8Bit BattV_LID_ser, u_8Bit *BattV_data_pc)
{
	/* Local variable(s) */
	u_8Bit BattV_LID_service;            // Battery LID service
	u_8Bit *BattV_data_p_c;              // pointer to battery data that needs to be checked
	u_8Bit BattV_chck_rslt_c = FALSE;    // Check result
	
	/* Store LID service and pointer to battery data */
	BattV_LID_service = BattV_LID_ser;
	BattV_data_p_c = BattV_data_pc;

	switch (BattV_LID_service) {
		
		case VOLTAGE_ON:
		{
			/* Voltage ON Default    Allowed Range (100mV) */
			/*         59           85 <= voltage <= 125  */
			if ( (*BattV_data_p_c < 85) || (*BattV_data_p_c > 125) || (*BattV_data_p_c <= BattV_cals.Thrshld_a[SET_UV_LMT]) ) {
				/* Diagnostic data is not in allowed range.               */
				/* Do not give access for write diagnostic data function. */
				BattV_chck_rslt_c = FALSE;
			}
			else {
				/* Copy the data in local array. */
				//              BattVF_Copy_Diag_Prms(BattV_data_p_c); //Harsha commented and added below statement.
				BattV_cals.Thrshld_a[CLR_UV_LMT] = *BattV_data_p_c; //Clear UV condition for new values.                                
				
				/* Data is in correct range.              */
				/* Give access to Write Diagnostics data. */
				BattV_chck_rslt_c = TRUE;
			}
			break;
		}
		
		case VOLTAGE_OFF:
		{
			/* Voltage ON Default    Allowed Range (100mV) */
			/*         56           80 <= voltage <= 120  */
			if ( (*BattV_data_p_c < 80) || (*BattV_data_p_c > 120) || (*BattV_data_p_c >= BattV_cals.Thrshld_a[CLR_UV_LMT]) ) {
				/* Diagnostic data is not in allowed range.               */
				/* Do not give access for write diagnostic data function. */
				BattV_chck_rslt_c = FALSE;
			}
			else {
				/* Copy the data in local array. */
				//      BattVF_Copy_Diag_Prms(BattV_data_p_c); //Harsha commented.
				BattV_cals.Thrshld_a[SET_UV_LMT] = *BattV_data_p_c; //Set UV condition for new values.
				
				/* Data is in correct range.              */
				/* Give access to Write Diagnostics data. */
				BattV_chck_rslt_c = TRUE;
			}
			break;
		}
		
		case BATTERY_COMPENSATION:
		{
			
			/*              Battery Compensation Type                                                  Default(100mV)               Allowed Range (100mV)     */
			/* No Outputs are ON (no compensation)                                            0                                 0 <= voltage <= 30        */
			/* Only one heater or vent output is ON                                                   4                                     0 <= voltage <= 30                */
			/* Two, three or four outputs are ON (heater and/or vent)                 7                                     0 <= voltage <= 30            */
			/* Only steering wheel ON                                         5                                 0 <= voltage <= 30            */
			/* Steering wheel + 1/2 heater/vent outputs are ON                                11                            0 <= voltage <= 30                */
			/* Steering wheel + 3/4 heater/vent outputs are ON                                13                            0 <= voltage <= 30                */
			if ( ( *BattV_data_p_c > 30 ) ||
			   ( *(BattV_data_p_c + 1) > 30) ||
			   ( *(BattV_data_p_c + 2) > 30) ||   
			   ( *(BattV_data_p_c + 3) > 30) ||
			   ( *(BattV_data_p_c + 4) > 30) ||
			   ( *(BattV_data_p_c + 5) > 30) ) {
				
				/* Diagnostic data is not in allowed range.               */
				/* Do not give access for write diagnostic data function. */
				BattV_chck_rslt_c = FALSE;
			}
			else {
				/* Copy the data in local array. */
				BattV_compensation_a[0] = *BattV_data_p_c;              // No Outputs are ON (no compensation)
				BattV_compensation_a[1] = *(BattV_data_p_c+1);  // Only one heater or vent output is ON
				BattV_compensation_a[2] = *(BattV_data_p_c+2);  // Two, three or four outputs are ON (heater and/or vent)
				BattV_compensation_a[3] = *(BattV_data_p_c+3);  // Only steering wheel ON
				BattV_compensation_a[4] = *(BattV_data_p_c+4);  // Steering wheel + 1/2 heater/vent outputs are ON 
				BattV_compensation_a[5] = *(BattV_data_p_c+5);  // Steering wheel + 3/4 heater/vent outputs are ON
				
				/* Data is in correct range.              */
				/* Give access to Write Diagnostics data. */
				BattV_chck_rslt_c = TRUE;
			}
			break;
		}
		
		default :
		{
			BattV_chck_rslt_c = FALSE;
		}
	}
	return(BattV_chck_rslt_c);
}


/*********************************************************************************/
/* BattVF_Write_Diag_Data                                                                                                                */
/* Input Parameter:                                                              */
/* BattV_service: LID service for the write operation                            */
/*                                                                               */
/* Return parameter: 1 if the write is complete. 0 otherwise.                    */
/*                                                                               */
/* This function performs write operation of the new voltage calibration data.   */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* diagLF_WriteData :                                                            */
/*    u_8Bit BattVF_Write_Diag_Data(u_8Bit BattV_service)                        */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* EE_BlockWrite(EEBlockLocation elocation, u_8Bit *pbysrc)                                      */
/*                                                                                                                                                               */
/*********************************************************************************/
@far u_8Bit BattVF_Write_Diag_Data(u_8Bit BattV_service)
{
	/* Local variable(s) */
	u_8Bit BattV_LID;                    // LID service
	u_8Bit BattV_write_rslt_c = FALSE;   // Write result
	
	/* Store battery LID service */
	BattV_LID = BattV_service;
	
	/* New implementation */
	/* Check for valid LID service */
	if ( (BattV_LID == VOLTAGE_ON) || (BattV_LID == VOLTAGE_OFF) || (BattV_LID == BATTERY_COMPENSATION) ) {
		/* Check battery compensation LID */
		if (BattV_LID == BATTERY_COMPENSATION) {
			/* Update the entire battery compensation parameters */
			EE_BlockWrite(EE_BATT_COMPENSATION_DATA, (u_8Bit*)&BattV_compensation_a[NO_OUTPUT_ON]);
		}
		else {
			/* Update the entire battery calibration parameters */
			EE_BlockWrite(EE_BATTERY_CALIB_DATA, (u_8Bit*)&BattV_cals);
		}
		/* Let Diagnostic module know that we have placed a call to EE_BlockWrite */
		BattV_write_rslt_c = TRUE;
	}
	else {
		/* Not a valid LID. Let the Diagnostic module know. */
		/* Nothing should be written to emulated EEPROM */
		BattV_write_rslt_c = FALSE;
	}
	return(BattV_write_rslt_c);
}


/*********************************************************************************/
/* BattV_Get_LdShd_Mnt_Stat                                                                                                      */
/* This function returns the most recent load shed monitor status.                               */
/*                                                                               */
/* Return parameter:                                                             */
/* BattV_ldShd_Mnt_stat_c: Most recent load shed monitor status.                                 */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* mainF_CSWM_Status :                                                           */
/*    u_8Bit BattV_Get_LdShd_Mnt_Stat()                                          */
/*                                                                                                                                                               */
/*********************************************************************************/
@far u_8Bit BattVF_Get_LdShd_Mnt_Stat(void)
{
	/* Return the most recent load shed monitor status */
	return(BattV_ldShd_Mnt_stat_c);
}
/* Ino.413C0 */

