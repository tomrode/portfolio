#ifndef VENTING_H
#define VENTING_H

/********************************************************************************/
/*                        A U T H O R   I D E N T I T Y							*/
/********************************************************************************/
/* Initials      Name                   Company									*/
/* --------      --------------------   --------------------------------------- */ 
/* CK            Can Kulduk             Continental Automotive Systems			*/
/* HK            Harsha Yekollu         Continental Automotive Systems          */
/* FU            Fransisco Uribe        Continental Automotive Systems          */
/********************************************************************************/


/********************************************************************************/
/*                   CHANGE HISTORY for VENTING MODULE                          */
/********************************************************************************/
/* mm/dd/yyyy	User - Change comments											*/
/* 10/26/2006   CK   - Heater file created                             			*/ 
/* 06/20/2008	FU	 - Vs_HVAC_Swtch_Stat_e added								*/
/* 09/25/2008   CK   - Updated Vs_Swtch_Stat_e.                        			*/         
/* 10.30.2008   CK   - Added Vs active Number defines							*/
/* 01.23.2009   CK   - PC-Lint related updates.                                 */
/* 02.09.2009   CK   - Added vsF_Get_Fault_Status function prototype			*/
/********************************************************************************/


/********************************* @Include(s) **********************************/
#include "TYPEDEF.h"
/********************************************************************************/


/******************************* @Enumaration(s) ********************************/
/* Vs Levels */
typedef enum{
    VL0=0,                            // Vs level 0 (vent is turned off)
    VL_LOW=1,                         // Vs low level
    VL_HI=3                           // Vs high level  
}Vs_Level_e;

/* Vs Location */
typedef enum{
    VS_FL=0,                          // Front left vs          
    VS_FR=1,                          // Front right vs
    VS_LMT=2                          // Number of Vent Limit
}Vs_Location_e;
/* Vent Isense Type */
typedef enum{
    VS_OFFSET_ISENSE=0,               // Isense reading when vent is turned off
    VS_ON_ISENSE=1                    // Isense reading when vent is turned on
}Vs_Isense_type_e;

/* Fault Types */
typedef enum{
    VS_OPEN=0,                        // Open circuit 
    VS_SHRT_TO_GND=1,                 // Short to ground
    VS_SHRT_TO_BATT=2                 // Short to battery
}Vs_Flt_Type_e;

/* Diagnostic DTC Control */
typedef enum{
    VS_CLR_DTC=0,                     // Clear DTC by diagnostic command
    VS_SET_DTC=1                      // Set DTC by diagnostic command
}Vs_DTC_Control_e;

/* Vs Switch Status */
typedef enum{
    VS_NOT_PSD=0,                     // Vs switch is not pressed
    VS_OFF=1,                         // Vented seat off. (Signal that turns off the vents)
    VS_LOW=2,						  // Vented seat low (NOT USED)
    VS_HI=3,						  // Vented seat high (NOT USED)	
    VS_PSD=4,                         // Vs switch is pressed
    VS_SNA=7						  // Vented seat signal not available	
}Vs_Swtch_Stat_e;

/* Vs HVAC Switch Status */
typedef enum{
	VS_HVAC_NOT_PSD=0,					// Vs switch is not pressed
    VS_HVAC_PSD=1,						// Vs switch is pressed
    VS_HVAC_SNA=3						// Vs switch signal is not available
}Vs_HVAC_Swtch_Stat_e;

/* Vs fault detection parameters */
typedef enum{
    V_OPEN=0,                         // Open Vsense Threshold
    V_SHRT_TO_GND=1,                  // Short to Ground Vsense Threshold
    V_SHRT_TO_BATT=2,                 // Short to Battery Vsense Threshold
    V_EN_OPEN_DEC=3,                  // Vent output voltage threshold to enable open detection
    FLT_DTC_PRM_LMT=4                 // Fault Detection parameter limit
}Vs_flt_dtc_e;
/********************************************************************************/


/******************************* @Constants/Defines *****************************/
#define VS_LV_LMT                  3  // Vent level limit (used for array indexing)

#define VS_PRL_FLT_MAX             7  // vs preliminary fault status maximum

/* A3 Vent Set Fault Masks */
#define VS_NO_FLT_MASK             0  // No fault is present (all bits cleared)
#define VS_PRLM_OPEN_MASK          1  // Preliminary open fault (bit 0) 
#define VS_PRLM_GND_MASK           2  // Preliminary short to ground fault  (bit 1)
#define VS_PRLM_BATT_MASK          4  // Preliminary short to battery fault (bit 2) 
#define VS_MTRD_OPEN_MASK          8  // Matured open fault (bit 3)
#define VS_MTRD_GND_MASK          16  // Matured short to ground fault  (bit 4)
#define VS_MTRD_BATT_MASK         32  // Matured short to battery fault (bit 5)

#define VS_10SEC_OPEN_MASK        0x55 //Special case, only DTC will be blown. Functionality remains same  

/* A3 Vent Clear Preliminary fault masks */
#define VS_CLR_PRLM_OPEN          254 // Clear Preliminary open fault
#define VS_CLR_PRLM_GND           253 // Clear Preliminary open fault
#define VS_CLR_PRLM_BATT          251 // Clear Preliminary open fault

#define VS_DATA_LENGHT      2         // Write Data by LID maximum lenght

/* 1 second = 100 cycles in 10 ms (vsF_Manager) */
#define VS_TM_MDLTN        100        // Time modulation (used in converting seconds into time slice cycles)

#define VS_FLT_MTR_TM       25        // Fault mature time (25 * 80ms = 2 seconds)

/* Vent motor start up modification for EOL tests */
#define VS_FAILED_RSLT      1         // Failed result for EOL motor start up modification

/* For Short to Battery detection */
#define VS_VSENSE_DLY       2         // (2*80ms=160ms delay to start short to Battery detection)

/* Vs active number defines */
#define VS_ZERO_ACTV		0		  // Zero vent is active
#define VS_ONE_ACTV 		1		  // One vent is active
#define VS_TWO_ACTV			2		  // Two vents are active
/********************************************************************************/


/******************************** @Global variables *****************************/
extern union char_bit vs_Flag1_c[VS_LMT];
#define state_b              b0     // Vs state: On/Off
#define sw_rq_b              b1     // software request that turns on/off the vs
#define LED_off_b            b2     // turns off the LED display as soon as a vs fault matures
#define start_up_cmplt_b     b3     // Motor start up complete
#define RemSt_auto_b         b4     // Remote start with automatic vent activation indicator

extern union char_bit vs_Flag2_c[VS_LMT];
#define swtch_rq_prps_b      b0      // switch request purpose (to turn on or off the vs)
#define invld_swtch_rq_b     b1      // invalid switch request
#define diag_cntrl_actv_b    b2      // diagnostic control is active
#define diag_clr_ontime_b    b3      // resets the ontime to zero with each new diagnostic request

/* Fault Detection parameters */
extern u_8Bit vs_Vsense_c[VS_LMT];   // Vent voltage reading
extern u_8Bit vs_Isense_c[VS_LMT];   // Vent current reading

extern u_8Bit vs_shrtTo_batt_mntr_c; // monitors short to battery fault on either (or both) vent
/********************************************************************************/


/******************************** @Global structures ****************************/

/* Only read access please for the elements of this structure. */
/* Calibration parameters (11 bytes) */
struct Vs_cal{
    u_8Bit PWM_a[VS_LV_LMT];          // Pwm duty cycles (3 bytes)
};

struct Vs_cal_s{
	struct Vs_cal cals[6];          //6 Vehicle lines.
};

/* Vs Calibration parameters */
extern struct Vs_cal_s Vs_cal_prms;

typedef struct {
    u_8Bit Flt_Dtc[FLT_DTC_PRM_LMT];  // Fault detection thresholds (4 bytes)
    u_16Bit start_up_tm;              // Motor start up time (2 byte)
    u_8Bit vs_2sec_mature_time;       // Regular Fault detection time (except open. Removed #define added in eep)
    u_8Bit vs_10sec_mature_time;      //For vents Open (Tracker Id#43)
}Vs_flt_cal_s;

extern Vs_flt_cal_s Vs_flt_cal_prms;
/********************************************************************************/


/****************************** @Function Prototypes ****************************/
/* For Main */
@far void vsF_Init(void);
@far void vsF_ADC_Monitor(void);
@far void vsF_Output_Cntrl(void);
@far void vsF_Manager(void);
@far void vsF_Fault_Mntr(void);
@far void vsF_DTC_Manager(void);
/* For Internal Faults */
@far void vsF_Clear(u_8Bit vs_position);
@far void vsF_LED_Display(u_8Bit vs_mark);
/* For Diagnose */
@far u_8Bit vsF_Chck_Diag_Prms(u_8Bit vs_LID_ser, u_8Bit *vs_data_p);
@far u_8Bit vsF_Write_Diag_Data(u_8Bit vs_LID);
@far u_8Bit vsF_Get_Output_Stat(u_8Bit vs_site);
@far void vsF_Diag_DTC_Control(Vs_DTC_Control_e Vs_DTC_cntrl, u_8Bit vs_DTC, Vs_Location_e Vent_Location);
@far u_16Bit vsF_Modify_StartUp_Tm(void);
@far u_8Bit vsF_Get_Fault_Status(u_8Bit vs_choice);
/* For Battery */
@far u_8Bit vsF_Get_Actv_outputNumber(void);
/********************************************************************************/


#endif

