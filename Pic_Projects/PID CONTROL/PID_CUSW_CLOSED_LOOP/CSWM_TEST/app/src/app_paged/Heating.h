
#ifndef HEATING_H
#define HEATING_H

/**********************************************************************************************/
/*                        CHANGE HISTORY for HEATING MODULE                                   */
/**********************************************************************************************/
/* mm/dd/yyyy   User    -  Change comments                                                    */
/* 03.06.2008/Harsha    -  New file created                                                   */
/* 06.18.2008 Francisco -  hs_HL3_set_b added to hs_status2_flags_c                           */
/* 01.30.2009 Harsha    -  HL3_executed_b added to hs_status2_flags_c                         */
/* 04.14.2009 Harsha    -  An indication to Rear seat switch pressed added                    */
/*                         This var will set when ever there is a switch press                */
/*                         and status does not change until the IGN stat change               */
/*                         hs_rear_swpsd_stat_c[] is the var name                             */
/* 04.16.2009 Harsha    -  HL3_executed_b commented due to spec change                        */
/*                         Additional logic is removed                                        */
/* 08.21.2009 Christian -  Change Request ID 32456 add two variables			              */
/*                         hs_PartialOL_Imp_Rear_cutoff_c and hs_PartialOL_Imp_Front_cutoff_c */
/* 04.17.2010 Harsha    -  MKS 50039 New structure hs_calib_data and new enum added           */
/*                         for Heat Calib Parameters                                          */
/*                         MKS 50111 New variables added for EEP Enhancement support          */
/*                         MKS 50168 hs_ntc_failsafe_data structure created                   */
/* 06.11.2010 Harsha    -  MKS 52375 Rear Seat stuck switch variable names changed from specific */
/*                         to general. HS_15SEC_MATURE_TIME to HS_REAR_STUCK_MATURE_TIME      */
/*                         and HS_2SEC_DEMATURE_TIME to HS_REAR_STUCK_DEMATURE_TIME             */ 
/*                      -  MKS 52372 - Unique partial OPEN detection thresholds introduced    */
/*                         across vehicle lines. Commented the previous two variables         */
/*                         hs_calib_data structure updated to have two unique thresholds for  */
/*                         each vehicle line.                                                 */
/* 10.19.2010 Harsha    -  enum for vehicle lines differentiated between TIPM and PN          */
/*                         struct hs_calib_s size reduced to 6 from 8                         */
/**********************************************************************************************/
/*   NAME:              HEATING.H                                                       */
/*                                                                                                                                                              */

/* EXTERN-Definition*/

#ifdef HEATING_C  

#undef EXTERN 
#define EXTERN 
#else
#undef EXTERN 
#define EXTERN extern
#endif

/* Defines */

#define HEAT_LEVEL_OFF  7

/* CAN constants for internal HSM state machine */
#define SET_OFF_REQUEST         0  //OFF Command
#define SET_LO_REQUEST          1
#define SET_HI_REQUEST          2
#define SET_PID_REQUEST         3
//#define SET_HIGH_100_REQUEST               100

#define HS_ACTIVE             0x55
#define HS_NOT_ACTIVE         0xAA

/* This should go to Main.H..Internal Fault description and #defines */
#define MAIN_RELAY_A_INTERNAL_FAULT        0x0001
#define MAIN_RELAY_B_INTERNAL_FAULT        0x0002
#define MAIN_INTERNAL2_FAULT               0x0004
#define MAIN_INTERNAL3_FAULT               0x0008
#define MAIN_INTERNAL4_FAULT               0x0010
#define MAIN_INTERNAL5_FAULT               0x0020
#define MAIN_INTERNAL6_FAULT               0x0040
#define MAIN_INTERNAL7_FAULT               0x0080

#define MAIN_INTERNAL8_FAULT               0x0100
#define MAIN_INTERNAL9_FAULT               0x0200
#define MAIN_INTERNAL10_FAULT              0x0400
#define MAIN_INTERNAL11_FAULT              0x0800
#define MAIN_INTERNAL12_FAULT              0x1000
#define MAIN_INTERNAL13_FAULT              0x2000
#define MAIN_INTERNAL14_FAULT              0x4000
#define MAIN_INTERNAL15_FAULT              0x8000

EXTERN unsigned short main_internal_fault_w;

/* enums for: LED state Display */
enum {
CAN_LED_OFF
,CAN_LED_LOW
,CAN_LED_MED
,CAN_LED_HI
};

/* Enums for Diag IO control request values */
enum {
HS_DIAG_IO_RQ_OFF
,HS_DIAG_IO_RQ_LL1
/*,HS_DIAG_IO_RQ_LL2*/ //Not used
,HS_DIAG_IO_RQ_HL1
,HS_DIAG_IO_RQ_HL2
,HS_DIAG_IO_RQ_HL3
};

/* Enums for Diag IO Seat Stauts values for HSM_A1 message */
enum {
HS_DIAG_IO_STATUS_OFF
,HS_DIAG_IO_STATUS_LOW
,HS_DIAG_IO_STATUS_MED
,HS_DIAG_IO_STATUS_HI
};

/* Enums for UDS Status Values */
enum {
HS_READ_STATUS_OFF
,HS_READ_STATUS_LL1
,HS_READ_STATUS_LL2
,HS_READ_STATUS_HL1
,HS_READ_STATUS_HL2
,HS_READ_STATUS_HL3
};

/* Total Vehicle lines supported for the program */
enum {
	HS_VEH_PF = 0
	,HS_VEH_MM_MA = 1
	,HS_VEH_KL = 2
	,HS_VEH_UF_UA = 3
	,HS_VEH_UT = 4
	,HS_VEH_PK = 5
	,HS_VEH_TR = 6
	,TOTAL_VEH = 7
};

/* MKS 61288: New enum added */
enum {
	HS_VEH_CLOTH  = 0
	,HS_VEH_BASE_LEATHER  =  2
	,HS_VEH_PERF_LEATHER  = 4
};

/* Heated Seat Fault Detection calibration parameters */
enum{
	HS_FULL_OPEN_ISENSE=0,               // Isense threshold for full open detection (0.5A)
    HS_OVERLOAD_ISENSE=1,                // Maximum Isense threshold for over current detection (10A)
    HS_STB_VSENSE=2,                     // VSense short to battery threshold (5v)
    HS_STG_VSENSE=3,                     // VSense short to Ground threshold  (1v) 
    HS_2SEC_MATURE_TIME=4,               // 2sec Maturity time for blowing DTCs (10ms*200times = 2sec)
    HS_REAR_STUCK_MATURE_TIME=5,         // 60 Sec Mature time for Rear seat Stuck switch detection
    HS_REAR_STUCK_DEMATURE_TIME=6,        // 2 Sec Demature time for Rear Seat stuch swith 
    FLT_LIMIT=7
};

enum
   {
    PID_OVERRIDE_HEAT_DIAGNOSTICS_ON=0,
    PID_OVERRIDE_HEAT_DIAGNOSTICS_OFF=1
   };

/* Bit variables */
EXTERN union char_bit   hs_control_flags_c[ ALL_HEAT_SEATS ];
#define hs_start_timer_b              b0   /* Flag for the heating timers  to start,  to clear, to pause*/
#define hs_can_msg_flow_ctrl_b        b1   /* Flag for CAN message. Makes sure that only once logic goes
inside for each request */
#define hs_io_rq_control_b            b2   /* Flag to denote that, IO controlling is ON for seat request*/
#define hs_io_stat_control_b          b3   /* Flag to denote that, IO controlling is ON for seat status*/
#define ntc_greater_70c_b             b4   /* Flag to denote that, NTC Readings are > 70c */
#define remote_flow_b                 b5   /* Falg to denote that, Remote Start started */
#define remote_user_b                 b6   /* Flag for CAN message in Remote start. Only once process req */
#define ntc_failure_first_time_b      b7   /* When first time NTC >70 occurs, thsi flag will be set, cleared
when fault goes away. */

EXTERN union char_bit   hs_status_flags_c[ ALL_HEAT_SEATS ];
#define hs_prelim_short_gnd_b         b0   /* Preliminary fault flag for short to gnd Detection*/
#define hs_final_short_gnd_b          b1   /* Final fault flag for short to gnd Detection*/
#define hs_prelim_open_b              b2   /* Preliminary fault flag for open Detection*/
#define hs_final_open_b               b3   /* Final fault flag for open Detection*/
#define hs_prelim_short_bat_b         b4   /* Preliminary fault flag for short to gnd Detection*/
#define hs_final_short_bat_b          b5   /* Final fault flag for short to gnd Detection*/
#define hs_prelim_partial_open_b      b6   /* Flag to denote that there is Preliminary partial open*/
#define hs_final_partial_open_b       b7   /* Flag to denote that there is matured partial open*/

EXTERN union char_bit   hs_status2_flags_c[ ALL_HEAT_SEATS ];
#define hs_HL3_set_b         b0   /* If set, means we are from HL3, else no*/
#define hs_HL2_set_b         b1   /* If set, means we are from HL2, else no*/
#define hs_HL1_set_b         b2   /* If set, means we are from HL1, else no*/
//#define hs_LL2_set_b         b3   /* If set, means we are from LL2, else no*/
//#define HL3_executed_b       b3   /* If this flag is set means in present IGN cycle, HL3 100% PWM ran atleast once */
#define hs_LL1_set_b         b4   /* If set, means we are from LL1, else no*/
#define hs_heat_level_off_b  b5   /* If set, means no heat level active, else either of heat levels are act*/
#define hs_heater_state_b    b6   /* Holds the actual state of each Heater. 1 ON, 0 OFF */
#define hs_output_fault_set_b b7  /*If any of heater faults set, this flag will be set to TRUE, else FALSE */

EXTERN union char_bit   hs_status3_flags_c[ ALL_HEAT_SEATS ];
#define final_STB_or_OPEN_b  b0   /* If set, means unclear wether it is STB or OPEN*/
#define hs_rem_temp_gret70_b b1   /* While in Remote control Thermistor temp crossed 50c */
#define hs_setoff_rq_b       b2   /* Holds OFF request state. 1 --OFF Req, 0 -- Other than OFF Request */
//#define hs_optests_started   b3   /* Set to 1, once the output tests starts */
#define first_time_ugly_b    b4   /* When Outputs are ON, first time heaters observes Main CSWM status Ugly this flag will be set */
#define self_tests_active_b  b5   /* This flag will set when there is an Output control self test for heaters set */
#define tests_started_b      b6   /* This flag will be set when there Self tests started and allows only once to start*/


EXTERN union char_bit hs_rear_seat_flags_c[ALL_REAR_SWITCH];
//EXTERN union char_bit hs_rear_seat_flags_c[ALL_HEAT_SEATS];
#define stksw_psd            b0   /* If set, dtc will be stored with Active status */

/* Byte and Array variables */

/* Variables used to store the vehicle line information. */
EXTERN unsigned char hs_vehicle_line_c;
EXTERN unsigned char hs_seat_config_c;  //MKS 61288: Gives the seat configuration from EEP (Cloth/Base Leather/Perf Leather)

EXTERN unsigned char hs_rear_swth_stat_c[ALL_REAR_SWITCH]; //Status of Switch position
EXTERN unsigned char hs_rear_swpsd_stat_c[ALL_REAR_SWITCH]; /* If set, means atleast once rear switch was pressed in current IGN cycle.*/

/* Currently allowed heat PWM for (HIGH3/HIGH2/HIGH1/LOW2/LOW1 ... 100%/95%/25%/50%/7%) */
EXTERN unsigned char hs_allowed_heat_pwm_c [ALL_HEAT_SEATS];
#define hs_allowed_heat_pwm_fl_c  hs_allowed_heat_pwm_c[ FRONT_LEFT ]
#define hs_allowed_heat_pwm_fr_c  hs_allowed_heat_pwm_c[ FRONT_RIGHT ]
#define hs_allowed_heat_pwm_rl_c  hs_allowed_heat_pwm_c[ REAR_LEFT ]
#define hs_allowed_heat_pwm_rr_c  hs_allowed_heat_pwm_c[ REAR_RIGHT ]

/* Heated seat status..Which we send while requesting the HSM status using KWp $$21 $$05 */
EXTERN unsigned char hs_output_status_c [ALL_HEAT_SEATS];
#define hs_output_status_fl_c   hs_output_status_c[ FRONT_LEFT ]
#define hs_output_status_fr_c   hs_output_status_c[ FRONT_RIGHT ]
#define hs_output_status_rl_c   hs_output_status_c[ REAR_LEFT ]
#define hs_output_status_rr_c   hs_output_status_c[ REAR_RIGHT ]

/* Stores the latest seat Switch Request to Heat seats output FETS.                                    */
/* Incase module returns from Load shed or short or open faults to re-activate the present seat output */
EXTERN unsigned char hs_rem_seat_req_c[ ALL_HEAT_SEATS ];
#define hs_rem_seat_req_fl_c      hs_rem_seat_req_c[ FRONT_LEFT ]
#define hs_rem_seat_req_fr_c      hs_rem_seat_req_c[ FRONT_RIGHT ]
#define hs_rem_seat_req_rl_c      hs_rem_seat_req_c[ REAR_LEFT ]
#define hs_rem_seat_req_rr_c      hs_rem_seat_req_c[ REAR_RIGHT ]

/* Time counter varaible for monitoring 2sec LED display incase of faults matured and new switch requested */
EXTERN unsigned char hs_monitor_2sec_led_counter_c[ ALL_HEAT_SEATS ];
#define hs_monitor_2sec_led_counter_fl_c      hs_monitor_2sec_led_counter_c[ FRONT_LEFT ]
#define hs_monitor_2sec_led_counter_fr_c      hs_monitor_2sec_led_counter_c[ FRONT_RIGHT ]
#define hs_monitor_2sec_led_counter_rl_c      hs_monitor_2sec_led_counter_c[ REAR_LEFT ]
#define hs_monitor_2sec_led_counter_rr_c      hs_monitor_2sec_led_counter_c[ REAR_RIGHT ]

/* Displays the actual PWM state. PWM ON or PWM OFF */
EXTERN unsigned char hs_pwm_state_c[ALL_HEAT_SEATS];

EXTERN unsigned char hs_Vsense_c[ALL_HEAT_SEATS];     // Heat voltage readings   
EXTERN unsigned char hs_Isense_c[ALL_HEAT_SEATS];     // Heat current readings
EXTERN unsigned char hs_Zsense_c[ALL_HEAT_SEATS];     // Heat Impedance readings 

EXTERN unsigned char CSWM_config_c;   //Module configuration

/* Holds the actual NTC Feedback values */
EXTERN unsigned char NTC_AD_Readings_c[ALL_HEAT_SEATS];

/* Timeout counter for the Heated Seats */
EXTERN unsigned short hs_time_counter_w[ ALL_HEAT_SEATS ];
//#define hs_time_counter_fl_w      hs_time_counter_w[ FRONT_LEFT ]
//#define hs_time_counter_fr_w      hs_time_counter_w[ FRONT_RIGHT ]
//#define hs_time_counter_rl_w      hs_time_counter_w[ REAR_LEFT ]
//#define hs_time_counter_rr_w      hs_time_counter_w[ REAR_RIGHT ]

EXTERN unsigned int hs_autosar_pwm_w[ALL_HEAT_SEATS];

/* Variable for holding the Heat parameters from EEPROM */
//EXTERN unsigned char hs_heat_pwm_values_c[4]; 
//EXTERN unsigned char hs_heat_timeout_values_c[4]; //Actual timeout values
//EXTERN unsigned char hs_NTC_check_timeout_values_c[4]; //Wait period to check the NTC Readings
//EXTERN unsigned char hs_NTC_feedback_values_c[5]; //NTC Feedback values from EEPROM
//EXTERN unsigned char hs_OL_Isense_cutoff_c; //Open Load cut off value
//EXTERN unsigned char hs_PartialOL_Imp_Front_cutoff_c; //Partial Open load cut off value FrontSeats CR32456
//EXTERN unsigned char hs_PartialOL_Imp_Rear_cutoff_c; //Partial Open load cut off value FrontSeats CR32456
//EXTERN unsigned char hs_overload_Isense_cutoff_c; //Over load I sense detection cut off value
//EXTERN unsigned char hs_STB_Vsense_cutoff_c;  //Stort to Bat detection cut off value
//EXTERN unsigned char hs_STG_Vsense_cutoff_c; //Short to GND detection cut off value.
EXTERN unsigned char hs_remote_start_cfg_c; //Remote start config insertion point.
//EXTERN unsigned char mature_2sec_time_c;  //2sec time for fault mature
//EXTERN unsigned char hs_rear_stksw_mat_time_c;//15sec mature time incase stuck switch detected,
//EXTERN unsigned char hs_rear_stksw_demat_time_c;//2sec demature time stuck switch 

EXTERN unsigned char hs_rear_U12S_stat_c; //status of U12S PIN for rear seats

/* New addition: EEP Enhancement MKS 50111 */
EXTERN unsigned char hs_flt_dtc_c[FLT_LIMIT]; //Contain the Fault thresholds, mature, demature times for heater/RelayA/B faults

/******************************** @Global structures ******************************/
/* Calibration parameters. (17 bytes) MKS 50039 */
struct hs_calib_data {
	
   unsigned char Pwm_c[4];              // Pwm duty cycles (4 bytes)
//   unsigned char Min_TimeOut_c[4];      // Min Timeouts heater (4 bytes)
   unsigned char Max_TimeOut_c[4];      // Max Timeouts heater (4 bytes)
   unsigned char NTC_CutOFF_c[5];       // NTC Cut OFF Temp values for heaters (5 bytes)
   unsigned char partial_open_c;     //MKS 52372: Front Heater and Rear Heater Partial OPEN detection thresholds
 
};

struct hs_calib_s{
	struct hs_calib_data   hs_prms[6];  
};

/* StWheel calibration parameters structure declaration */
EXTERN struct hs_calib_s hs_cal_prms;


struct hs_ntc_failsafe_data {
	unsigned short First_odostamp_w;
	unsigned short recent_odostamp_w;
	unsigned char failure_counter_c;
};

/* NTC Failsafe data to notify for External Diagnostic command */
EXTERN struct hs_ntc_failsafe_data  hs_NTC_FailSafe_prms;

/**********************************************************************************/

/* Function Prototypes */
@far void hsF_Init(void);

@far void hsF_HeatManager(void);

void hsF_GetHeatParameters(void);

@far void hsF_MonitorHeatTimes(void);

/* Interactive function with Vents. Vent will look into this function before turning on the Vents. If heated seats are not active vent will turn ON immediatly, else if heats are ON, first they will turn OFF the Heats and turn vent ON */
@far unsigned char hsF_FrontHeatSeatStatus(unsigned char hs_nr);

//@far unsigned char hsF_VentOnHeatOff(unsigned char hs_nr); //Not used

/* This function sets the right PWM and right request to timer channel */
@far void hsF_HeatRequestToFet(void);

/* Actual PWM Control of the FET */
void hsLF_ControlFET(unsigned char hs_nr, unsigned char duty_cycle);

/* If there are any final Flags set for the heaters, this routine will set the DTCs in active state, else if final flags were cleared once setting, changes the status of DTC from active to stored. */
@far void hsF_HeatDTC(void);

/* If there are any final Flags set for the NTCs, this routine will set the DTCs in active state, else if final flags were cleared once setting, changes the status of DTC from active to stored. */
@far void hsF_NTCDTC(void);

/* Rear Seat Switch stuck Pressed DTC detection and reporting DTC to stack */ 
@far void hsF_RearSwitchStuckDTC (void);

/* Reads Vsense and Isense feedback values */
@far void hsF_AD_Readings(void);

/* This function will get called when ever there is a write request for Heater calibration parameters. */
/* And This function verifies the data received from external tool is in range or not.                 */
/* If data is in range returns OK else not OK                                                          */
@far unsigned char hsF_CheckDiagnoseParameter( unsigned char service, unsigned char *data_p_c );

/* This function actually performs writes to Shadow EEPROM, if we get write requests from External Tool. */
@far unsigned char hsF_WriteData_to_ShadowEEPROM(unsigned char service);

/* This function updates the right values(PWM, timeout, CAN status etc) and handles this information to other functions */
@far void hsLF_UpdateHeatParameters(unsigned char hs_nr, unsigned char value_c);

/* Initializes all the variables to default values */
void hsLF_ClearHeatSequence(void);

/* This function returns the number of active heat outputs. */
@far unsigned char hsF_GetActv_OutputNumber(void);

/* This function will get executed for PN V7 service parts*/ 
/* only incase if the ECU config is mis matching with stoted config and Rear seats are not supported */
/* With the latest config */
@far void hsF_RearSeat_TurnOFF(void);

#endif


