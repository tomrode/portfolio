#ifndef DIAGWRITE_H_
#define DIAGWRITE_H_

/*****************************************************************************************************
*  (c) Copyright 2011 Continental Automotive Systems, all rights reserved
*
*  All material contained herein is CONTINENTAL CONFIDENTIAL and PROPRIETARY.
*  Any reproduction of this material without written consent from 
*  Continental AG is strictly forbidden.                                  
*
*  Filename:     DiagWrite.h
*
*  Created_by:   Harsha Yekollu
*
*  Date_created: 01/21/2011
*
*  Description:  Main Header file for UDS Diagnostics .. Write Data By LID Services 
*
*  Revision_history:
*
*  MKS ID       Author         Date      Description of Change(s)
*  --------  -------------  --------  --------------------------------------
*  xxxxxxx   H. Yekollu     01/21/11  Initial creation for FIAT CUSW MY12 Program
*	81838	 H. Yekollu		08/01/11  PROXI Implementation	
* 
* 
* 
******************************************************************************************************/
#undef EXTERN 

#ifdef DIAGWRITE_C
#define EXTERN 
#else
#define EXTERN extern
#endif

/*****************************************************************************************************
*     								LOCAL ENUMS
******************************************************************************************************/
/* enums for: No of Seats to provide Heat */
EXTERN enum DIAG_WRITE_SEATS{
HS_PWM
,HS_TIMEOUT
,VS_PWM
,HSW_PWM
,HSW_TIMEOUT
,VOLTAGE_ON
,VOLTAGE_OFF
,BATTERY_COMPENSATION
,CSWM_VARIANT
,HS_NTC_CUT_OFF                    //Limits to 0,49,55,55,70c values
,NTC_FAULT_READINGS                //Limits to Short to GND and Open faults
,HSW_TEMP_CUT_OFF
,HS_MIN_TIMEOUT
,HSW_MIN_TIMEOUT
,REMOTE_AMB_TEMP_LIMIT
,HS_IREF_VREF
,STW_IREF_VREF
,ECU_WRITE_MEMORY
,HS_PWM_REAR
,HS_TIMEOUT_REAR
,HS_NTC_CUTOFF_REAR
};

typedef enum
{
	E_PROXI_LRN_START,
	E_PROXI_CFG_UPDATE,
	E_REJ_TABLE_UPDATE,
	E_PROXI_POS_RSP,
	E_PROXI_NEG_RSP
} E_PROXI_LRN_STATE;

EXTERN E_PROXI_LRN_STATE re_PROXI_state;
/*****************************************************************************************************
*     								PROXI CONFIGURATION
******************************************************************************************************/
#define PROXI_DATA_SIZE			11u
#define PROXI_CRC_SIZE			3u
#define PROXI_ID_VEH_SIZE		3u
#define CFG_CODE_LEN			11u
#define PROXI_REJ_FEEDBACK_TBL_SIZE	9u
#define START_CRC_CALC_INDEX	25u

#define PROXI_DIGIT_11_BYTE		0u
#define PROXI_DIGIT_10_BYTE		1u
#define PROXI_DIGIT_9_BYTE		2u
#define PROXI_DIGIT_8_BYTE		3u
#define PROXI_DIGIT_7_BYTE		4u
#define PROXI_DIGIT_6_BYTE		5u
#define PROXI_DIGIT_5_BYTE		6u
#define PROXI_DIGIT_4_BYTE		7u
#define PROXI_DIGIT_3_BYTE		8u
#define PROXI_DIGIT_2_BYTE		9u
#define PROXI_DIGIT_1_BYTE		10u
#define PROXI_DS_BYTE			57u
#define PROXI_HEATSEAT_BYTE		61u
#define PROXI_VL_BYTE			104u
#define PROXI_COUNTRY_BYTE		106u
#define PROXI_RS_BYTE			107u
#define PROXI_SEAT_TYPE_BYTE	123u
#define PROXI_WHEEL_BYTE		123u
#define PROXI_WHEEL_TYPE_BYTE	123u
#define PROXI_MY_BYTE			124u

#define PROXI_CFG_CODE_MASK		0xFF
#define PROXI_DIGIT_MASK		0x0F
#define PROXI_DS_MASK			0x08
#define PROXI_HEATSEAT_MASK		0x03
#define PROXI_SEAT_TYPE_MASK	0x0C
#define PROXI_WHEEL_MASK		0x10
#define PROXI_WHEEL_TYPE_MASK	0x60
#define PROXI_RS_MASK			0x01
#define PROXI_COUNTRY_MASK		0xFF
#define PROXI_VL_MASK			0xFF
#define PROXI_MY_MASK			0xFF

#define PROXI_DIGIT_OFFSET		0u
#define PROXI_HEATSEAT_OFFSET	0u
#define PROXI_SEAT_TYPE_OFFSET	2u
#define PROXI_WHEEL_OFFSET		4u
#define PROXI_WHEEL_TYPE_OFFSET	5u
#define PROXI_DS_OFFSET			3u
#define PROXI_RS_OFFSET			0u
#define PROXI_COUNTRY_OFFSET	0u
#define PROXI_VL_OFFSET			0u
#define PROXI_MY_OFFSET			0u

#define LOW_CFG_CODE_LIMIT		(0x30u)
#define UP_CFG_CODE_LIMIT		(0x39u)

#define INVALID_DATA			0x00

#define PROXI_MAX_NUM_ERRORS	3u

#define PROXI_FILE_LEN			255u
#define CRC_INDEX_PROXI_FILE	6u
#define CRC_NUM_OF_DIGITS		5u

#define SHIFT_LEFT_BYTE			8u
#define NOT_USED_VAL 			0x00

#define REJECTION_FEEDBACK_DEFAULT 	0x00
#define GET_FIRST_NRC()				(ras_Rejection_Feedback_Tbl[0].Error_Type)

#define PROXI_DS_LHD						(0x00)
#define PROXI_DS_RHD						(0x01u)
#define PROXI_ABSENT						(0x00u)
#define PROXI_PRESENT						(0x01u)
#define PROXI_HEATSEAT_ABSENT				(0x00)
#define PROXI_HEATSEAT_FRONT				(0x01u)
#define PROXI_HEATSEAT_FRONT_REAR			(0x02u)
#define PROXI_SEAT_TYPE_INVALID				(0x00)
#define PROXI_SEAT_TYPE_CLOTH				(0x01u)
#define PROXI_SEAT_TYPE_LEATHER				(0x02u)
#define PROXI_SEAT_TYPE_PERF				(0x03u)
#define PROXI_WHEEL_ABSENT					(0x00)
#define PROXI_WHEEL_PRESENT					(0x01u)
#define PROXI_WHEEL_TYPE_INVALID			(0x00)
#define PROXI_WHEEL_TYPE_WOOD				(0x01u)
#define PROXI_WHEEL_TYPE_LEATHER			(0x02u)
#define PROXI_VEH_LINE_PF					(0x50u)

#define PROXI_CFG_CODE_DEFAULT				(0xAA)
#define PROXI_CFG_CODE_DEFAULT_LAST_BYTE	(0xA0)
#define PROXI_DS_DEFAULT					(PROXI_DS_LHD)
#define PROXI_VEH_LINE_DEFAULT				(PROXI_VEH_LINE_PF)
#define PROXI_COUNTRY_CODE_DEFAULT			(0x00)
#define PROXI_RS_DEFAULT					(PROXI_PRESENT)
#define PROXI_HEATSEAT_DEFAULT				(PROXI_HEATSEAT_FRONT)
#define PROXI_SEAT_TYPE_DEFAULT				(PROXI_SEAT_TYPE_CLOTH)
#define PROXI_WHEEL_DEFAULT					(PROXI_WHEEL_PRESENT)
#define PROXI_WHEEL_TYPE_DEFAULT			(PROXI_WHEEL_TYPE_WOOD)
#define PROXI_MY_DEFAULT					(0x0B)

#define PROXI_COUNTRY_CODE_US				0xCD       // 205

//#define GET_PROXI_LEARNED()					(rub_PROXI_Learned)

/*****************************************************************************************************
*     								LOCAL Structures
******************************************************************************************************/

/* Rejection Feedback Table */
typedef struct
{
	u_8Bit Error_Type;
	u_8Bit Error_BytePos;
	u_8Bit Error_Mask;
}S_REJECTION_FEEDBACK_REC;

EXTERN S_REJECTION_FEEDBACK_REC ras_Rejection_Feedback_Tbl[PROXI_MAX_NUM_ERRORS];
EXTERN S_REJECTION_FEEDBACK_REC def_Feedback_Tbl[PROXI_MAX_NUM_ERRORS];

/* PROXI Configuration Code type */
typedef struct
{
	u_8Bit Digit10 : 4;
	u_8Bit Digit11 : 4;
	u_8Bit Digit8 : 4;
	u_8Bit Digit9 : 4;
	u_8Bit Digit6 : 4;
	u_8Bit Digit7 : 4;
}S_PROXI_ID_VEH;

typedef struct
{
	u_8Bit Digit4 : 4;
	u_8Bit Digit5 : 4;
	u_8Bit Digit2 : 4;
	u_8Bit Digit3 : 4;
	u_8Bit unused0 : 4;
	u_8Bit Digit1 : 4;
}S_PROXI_CRC;

typedef union
{
	u_8Bit byte[PROXI_ID_VEH_SIZE];
	S_PROXI_ID_VEH s;
}U_PROXI_ID_VEH;

typedef union
{
	u_8Bit byte[PROXI_CRC_SIZE];
	S_PROXI_CRC s;
}U_PROXI_CRC;

typedef struct
{
	U_PROXI_ID_VEH PROXI_IDVeh;
	U_PROXI_CRC PROXI_CRC;

	u_8Bit Vehicle_Line_Configuration;
	u_8Bit Country_Code;
	u_8Bit Model_Year;

	u_8Bit Driver_Side : 1;
	u_8Bit Heated_Seats_Variant : 2;
	u_8Bit Remote_Start : 1;
	u_8Bit Seat_Material : 2;
	u_8Bit Wheel_Material : 2;

	u_8Bit Steering_Wheel : 1;
	u_8Bit unused0 : 7;
}S_PROXI_DATA;

typedef union
{
	u_8Bit byte[PROXI_DATA_SIZE];
    S_PROXI_DATA s;
}U_PROXI_DATA;

EXTERN U_PROXI_DATA ru_eep_PROXI_Cfg, ru_def_eep_map;

/*****************************************************************************************************
*     								VARIABLES
******************************************************************************************************/
/* Holds the write data contents: received from the external test tool */
EXTERN u_8Bit diag_temp_buffer_c[ 20 ];

EXTERN u_8Bit PROXI_Learned_c;
/*****************************************************************************************************
*     								FUNCTIONS
******************************************************************************************************/
//void diagWriteF_EcuDev_Data_2EH(enum DIAG_RESPONSE* Response, u_temp_l* DataLen, DescMsgContext* pMsgContext);

//void diagWriteF_Calib_Data_2EH(enum DIAG_RESPONSE* Response, u_temp_l* DataLen, DescMsgContext* pMsgContext);

//void diagWriteF_EOL_Data_2EH(enum DIAG_RESPONSE* Response, u_temp_l* DataLen, DescMsgContext* pMsgContext);

void diagWriteF_CyclicTask (void);

void diagLF_WriteData(unsigned char service);

#endif /*DIAGWRITE_H_*/
