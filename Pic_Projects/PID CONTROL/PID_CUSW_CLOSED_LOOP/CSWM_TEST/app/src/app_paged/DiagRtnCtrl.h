#ifndef DIAGRTNCTRL_H_
#define DIAGRTNCTRL_H_

/*****************************************************************************************************
*  (c) Copyright 2011 Continental Automotive Systems, all rights reserved
*
*  All material contained herein is CONTINENTAL CONFIDENTIAL and PROPRIETARY.
*  Any reproduction of this material without written consent from 
*  Continental AG is strictly forbidden.                                  
*
*  Filename:     DiagRtnCtrl.h
*
*  Created_by:   Harsha Yekollu
*
*  Date_created: 01/21/2011
*
*  Description:  Main Header file for UDS Diagnostics .. Routine Control Services 
*
*  Revision_history:
*
*  MKS ID       Author         Date      Description of Change(s)
*  --------  -------------  --------  --------------------------------------
*  xxxxxxx   H. Yekollu     01/21/11  Initial creation for FIAT CUSW MY12 Program
*  xxxxxxx   H. Yekollu     01/21/11  
*
* 
* 
* 
******************************************************************************************************/
#undef EXTERN 

#ifdef DIAGRTNCTRL_C
#define EXTERN 
#else
#define EXTERN extern
#endif

/*****************************************************************************************************
*     								ENUMS
******************************************************************************************************/
EXTERN enum DIAG_DCX_ROUTINE_CONTROL_STATUS{
TEST_NOT_STARTED = 0,				// Test is not started (initial value)
TEST_PASSED = 1,					// Test completed. It passed with no failures (bit 0)
TEST_RUNNING = 2,					// Test is in run process (bit 1)
TEST_STOPPED = 4,					// Test stopped by diagnotic tool before it is completed (bit 2)
TEST_PRE_CONDITIONS_FAILURE = 8,	// Test stopped. While running the test at least one of the pre-conditons failed (outputs turned off) (bit 3)
TEST_FAILED_DTC_PRESENT = 16        // Test completed and there is at least one output DTC is present
//HS_FAILURE = 16,					// Test completed. Heated seat matured fault is present (bit 4)
//STW_FAILURE = 32,					// Test completed. Steering Wheel matured fault is present (bit 5)
//VS_FAILURE = 64					// Test completed. Vent matured fault is present (bit 6)
};

/* New CSWM Output Test Pre-conditions added by CK on 04.17.2009 */
EXTERN enum DIAG_DCX_PRE_CONDITIONS_STATUS{
PRE_CONDITIONS_OK = 0,			// Preconditions are O.K. (initial value)
IGN_OTHER_THEN_RUN = 1,			// Ignition is out of RUN position (bit 0)
CAN_COM_FLT	= 2,				// CAN communication fault (CAN bus off) (bit 1)
INT_FLT = 4,					// Internal fault is present (bit 2)
VOLTAGE_RANGE = 8,				// System or Local Battery voltage is out of operation range (bit 3)
LOAD_SHD_ACTV = 16,				// Load shed active (bit 4)
PCB_OVERTEMP_ACTV = 32,			// PCB over temperature condition is present (bit 5)
LOC_FLT = 64,					// Loss of communication (LOC) with a  major network node (FCM, CCN, or HVAC) (bit 6)
TEMP_ABOVE_MAX_THRSHLD = 128	// Temperature is above maximum threshold for heaters and/or steering wheel (bit 7)
};

/*****************************************************************************************************
*     								#DEFINES
******************************************************************************************************/
///* These masks are used to clear DCX Routine Control Pre-condition failures */	// OLD commented on 04.17.2009 by CK
//#define DIAG_CLR_IGN_RUN			254
//#define DIAG_CLR_CAN_COM            253  
//#define DIAG_CLR_INT_FLT            251
//#define DIAG_CLR_LOCAL_VOLT_RANGE   247
//#define DIAG_CLR_SYSTEM_VOLT_RANGE  239
//#define DIAG_CLR_LOAD_SHED_ACT      223
//#define DIAG_CLR_PCB_OVER_TEMP      191
//#define DIAG_CLR_LOC                127

/* Updated Clear #defines for CSWM Output Test added by CK on 04.17.2009 */
#define DIAG_CLR_IGN_RUN			254
#define DIAG_CLR_CAN_COM            253 
#define DIAG_CLR_INT_FLT            251
#define DIAG_CLR_VOLTAGE_RANGE		247
#define DIAG_CLR_LOAD_SHED_ACT		239
#define DIAG_CLR_PCB_OVER_TEMP		223
#define DIAG_CLR_LOC				191
#define DIAG_CLR_TEMP_ABOVE_THRSHLD	127

/* These masks used to clear output failures */
//#define DIAG_CLR_HS_FAILURE_MASK  	239		// Clear Bit 4 (HS_FAILURE)   1110 1111 = 239
//#define DIAG_CLR_STW_FAILURE_MASK	223 	// Clear Bit 5 (STW_FAILURE)  1101 1111 = 223
//#define DIAG_CLR_VS_FAILURE_MASK	191     // Clear Bit 6 (VS FAILURE)   1011 1111 = 191
#define DIAG_CLR_TEST_RUNNING_MASK  253		// Clear Bit 1 (TEST_RUNNING) 1111 1101 = 253

/* This mask is used to check heater matured fault */
#define DIAG_HS_MATURED_FLT_MASK	0xAA	// To check if any of the matured fault bits is set 1010 1010


/*****************************************************************************************************
*     								FUNCTIONS
******************************************************************************************************/
//void diagRtnCtrlF_DataID_31H(enum DIAG_RESPONSE* Response, u_temp_l* DataLen, DescMsgContext* pMsgContext);
extern @far void diagF_DCX_OutputTest(void);
extern void diagLF_DCX_PreConditions_Chck(void);
//void diagF_DiagResponse(enum DIAG_RESPONSE Response, u_temp_l Length, DescMsgContext* pMsgContext);

#endif /*DIAGRTNCTRL_H_*/
