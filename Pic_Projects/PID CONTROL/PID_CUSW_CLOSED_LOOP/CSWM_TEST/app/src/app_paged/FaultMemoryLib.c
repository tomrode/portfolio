#define FAULTMEMORYLIB_C

/********************************************************************************/
/*                   CHANGE HISTORY for FaultMemoryLib MODULE                   */
/********************************************************************************/
/* mm/dd/yyyy User		- Change comments										*/
/* 06-17-2008 Francisco	- SIZEOF_ALL_SUPPORTED_DTC changed to 45				*/
/* 							 	DTC_PROCESS_EACH_TIME changed to 9				*/
/* 							 	DTC_PROCESS_TIMES added							*/
/* 							 	FMemLibF_UpdatestoEEPROM updated				*/
/* 06-23-2008 Francisco	- FMemLibF_ReadDiagnosticInformation enabled			*/
/* 11-11-2008 Harsha    - SIZEOF_ALL_SUPPORTED_DTC changed to 48 from 45        */
/* 12-02-2008 Harsha    - FMemLibF_SendROE_Light updated for PowerNet           */
/* 02-18-2009 CK        - Modified number of supported DTCs define              */
/* 						  (SIZEOF_ALL_SUPPORTED_DTC). New value is 47.          */
/* 04-15-2009 Harsha    - SIZEOF_ALL_SUPPORTED_DTC modified for PN to 46        */
/* 04-23-2009 Harsha    - FMemLibF_UpdatestoEEPROM() logic changes to Switch case*/
/*                      - operation_cycle_counter logic updated in CyclicTask() */
/*                        and FMemLibLF_IncDtcOperationCyclCounter              */
/* 04-28-2009  CK       - Modified FMemLibF_CyclicDtcTask function to fix the   */
/*                        issue with ROe message. Index is out of boundry for   */
/*                        d_AllDtc_a[] when no_of_dtc_c variable becomes 48     */
/*                        (DTC_PROCESS_TIMES* DTC_PROCESS_EACH_TIME). 			*/
/*                        no_of_dtc_c variable is adjusted by two for PWR_NET   */
/*                        46 DTcs and by one for TIPM 47 DTCs. Update this code */
/*                        if number of supported DTCs changes.                  */
/* 06-19-2009 Harsha    - Modifed FMemLibF_CheckDtcSupportOverDtc function to   */
/*                        have a return value when there is no DTC found        */
/* 09-28-2010 Harsha    -   PWR_NET commented. code to have pwr net logic       */
/********************************************************************************/
/*************************************** CUSW UPDATES ************************************************
 * 10.29.2010 Harsha    - FMemLibF_ReadDiagnosticInformation re consturcted with FIAT service definitions
 * 05.01.2011 Harsha    - Engineering Parameters updated to the SW such as
 * 						  ECU Timestamps, Timestamps from KEY ON, KEYON Counter etc.
 * 06.05.2011 Harsha    - MKS 74409: FMemLibF_ECUTimeStamps added. Taken out the logic from and added 
 *                        cyclic task added in new function.  												   	
 * 06.05.2011 Harsha    - MKS 74407: Snapshot and Environmental Data logic updated in cyclic DTC Task  
 *                        Self Healing Counter (Event counter changed to 40 from 100 for CUSW)         
 *                        FMemLibLF_DecCounter function added for this purpose                         
 *                        FMemLibLF_IncDtcOperationCyclCounter changed as FMemLibLF_DecDtcOperationCyclCounter 
 * 06.05.2011 Harsha    - MKS 74411: FMemLibF_ReadDiagnosticInformation updated with logic 
 *             
 * 08.12.2011 Harsha    - MKS 81847: Read DTC's not reporting the complete set.
 * 						  1902 - FMemLibLF_GetNumberOfRegisteredDtc updated to report total set of 10.
 * 						  1906 - FMemLibF_ReadDiagnosticInformation updated for right byte order.	
 * 						  190C - FMemLibF_ReadDiagnosticInformation updated with correct search info
 * 						  190E - FMemLibF_ReadDiagnosticInformation updated with correct search info
 * 08.16.2011 Harsha    - MKS 81849: GenericFailSts and CurrentFailSts flags updated for NWM.
 * 						  FMemLibF_NMFailSts new function created to handle this.
 * 						  Will be called in 320ms time slice. constantly looks for DTC Status.
 * 						  And updates the variables which will be used in NWM module.	
 * 11-29-2011 Christian  - MKS 91804 Update "Report DTC Snapshot Record by DTC 	
 *                       Number" ($19 $04) diagnostic command modification FMemLibF_ReadDiagnosticInformation 
 * 11-23-2011 Christian - MKS 91913 Clear "DTC Snapshot Record 2" when "Clear Diagnostic Information" command 	
 *                       is requested 
 * 02-16-2012 Christian - MKS 98347 Reset during self healing on Historical Stack
/*************************************** CUSW UPDATES **************************************************/
/*   NAME:              FAULTMEMORYLIB.C	                                    */
/* 																				*/

/* CAN_NWM Related Include files */
//#include "v_inc.h"
//#include "ccl_inc.h"
//#include "ccl_par.h"		// Not used Lint Info 766: Commented by CK 04.24.2009
//#include "ccl_cfg.h"		// repeated include Lint warning 537: Commented by CK 04.24.09
//#include "desc.h"			// repeated include Lint warning 537: Commented by CK 04.24.09
//#include "appdesc.h"		// repeated include Lint warning 537: Commented by CK 04.24.09
//#include "sip_vers.h" 	// Not used Lint Info 766: Commented by CK 04.24.2009
//#include "hw_config.h"	// Not used Lint Info 766: Commented by CK 04.24.2009

/* NWM Inculde files */
//#include "can_inc.h"		// repeated include Lint warning 537: Commented by CK 04.24.09
//#include "dbk_par.h"		// repeated include Lint warning 537: Commented by CK 04.24.09



#include "TYPEDEF.h"
#include "DiagMain.h"
#include "FaultMemoryLib.h"
#include "FaultMemory.h"
#include "canio.h"
#include "Network_Management.h"
#include "main.h"
#include "il_inc.h"
#include "v_inc.h"
#include "desc.h"
//#include "heating.h"		// Not used Lint Info 766: Commented by CK 04.24.2009

//#include "Flash_EEPROM.h"

#include "mw_eem.h"

//#include <string.h>	// repeated include Lint warning 537: Commented by CK 04.24.09

/* external data */
/* forward declarations                                                                                    */
/* extern void Flash_WriteEEPROMMirror(unsigned short address,unsigned char count,unsigned char * buffer); */
/* extern void FlashF_UpdateEEPROM_Immediate(void);                                                        */
/* extern void FMemLibF_CheckDtcSupportOverDtc(void);                                                      */
/* extern void FMemLibF_ClearDtcMemory(void);                                                              */
/* extern void FMemLibF_GetIgnition(void);                                                                 */
/* extern void FMemLibF_GetOdometer(void);                                                                 */
/* extern void FMemLibF_IsOdometerValid(void);                                                             */
/* extern void FMemLibLF_GetByteOfDtc(void);                                                               */
/* extern void FMemLibLF_GetNumberOfRegisteredDtc(void);                                                   */
/* extern void FmemLibLF_IncCounter(void);                                                                 */
/* extern void FMemLibLF_IncDtcOperationCyclCounter(void);                                                 */
/* extern void FMemLibLF_SearchDtcIndexInDtcMemory(void);                                                  */
/* extern void FMemLibLF_TransferFaultToHistoricalStack(void);                                             */
/* extern void FMemLibLF_UpdateHistoricalInterrogationRecord(void);                                        */

/* enums */
enum OPERATION_CYCLE
{
 OPERATION_CYLE_OFF,
 OPERATION_CYLE_ON
};

/* Defines */
#ifndef NULL

#define NULL                                    (void *)0

#endif

#define D_NOT_FOUNDED_K                         0x00
#define D_FOUNDED_K                             0x10

#define D_SELF_HEALING_CRITERION_K              40 //CUSW EVENT Counter

#define D_5_SEC_K                               16//5sec timer (320ms * 16 = 5120ms)

#define LOWBYTE(x)  (x & 0x00FF)            
#define HIGHBYTE(x) ((x >> 8) & 0x00FF)

#define fmemLF_MemSet           memset
#define fmemLF_MemCpy           memcpy
//#define fmemLF_MemCmp           memcmp  //Not used
#define fmemLF_MemMove          memmove   

#define SIZEOF_ALL_SUPPORTED_DTC              46  //(Total DTCs supported for CUSW)	

#define DTC_PROCESS_TIMES                  	   4  // (Total DTCs processed in 4 times)
#define DTC_PROCESS_EACH_TIME                 12  // 9 (Every time 12 DTCs processed)

/* Function prototypes */
/* Returns the DTC code 3 byte information byte by byte */
unsigned char FMemLibLF_GetByteOfDtc(unsigned char DtcIndex, unsigned char Byte);

/* Returns the no of DTcs present in either of the stacks by the searchmask input */
unsigned char FMemLibLF_GetNumberOfMaskedDtc(unsigned char DtcSearchMask, enum FAULTMEMORY_TYP FaultMemoryTyp);

/* Returns the No of DTCs actually registered in stacks */
u_temp_l FMemLibLF_GetNumberOfRegisteredDtc(enum FAULTMEMORY_TYP FaultMemoryTyp);
//unsigned char FMemLibLF_GetNumberOfRegisteredDtc(enum FAULTMEMORY_TYP FaultMemoryTyp);

/* Updates the Historical Interrogation record */
void FMemLibLF_UpdateHistoricalInterrogationRecord(void);

/* Transfers the chrono stack errors to Historical stack */
void FMemLibLF_TransferFaultToHistoricalStack(temp_l StartIndex, temp_l Number);
//void FMemLibLF_TransferFaultToHistoricalStack(unsigned char StartIndex, unsigned char Number);

/* Returns the Index from stack if DTC found */
temp_l FMemLibLF_SearchDtcIndexInDtcMemory(unsigned char DtcIndex, enum FAULTMEMORY_TYP FaultMemoryTyp);
//static unsigned char FMemLibLF_SearchDtcIndexInDtcMemory(unsigned char DtcIndex, enum FAULTMEMORY_TYP FaultMemoryTyp);

/* Returns the Index from ROM list if DTC found */
//static temp_l FMemLibLF_SearchDtcIndexInRom(unsigned long DtcCode); //Not used
//static unsigned char FMemLibLF_SearchDtcIndexInRom(unsigned long DtcCode);

static temp_l FMemLibLF_SearchDtcIndexWithLowerPriority(u_temp_l Priority);

//Clears single dtc
void FMemLibLF_ClearSingleDtc(unsigned char DtcIndex);

void FMemLibLF_DecDtcOperationCyclCounter(void);

//u_temp_l FmemLibLF_IncCounter(u_temp_l Counter, u_temp_l MaxValue); //Not used

void FMemLibF_ClearDiagnosticInformation(enum DIAG_RESPONSE* Response, DescMsgContext* pMsgContext);
void FMemLibF_ReadDiagnosticInformation(enum DIAG_RESPONSE* Response, u_temp_l* DataLen, DescMsgContext* pMsgContext, unsigned char Subservice);

//u_temp_l FMemLibLF_IncCounter(u_temp_l Counter, u_temp_l MaxValue);
unsigned char FMemLibLF_IncCounter(u_temp_l Counter, u_temp_l MaxValue);

/* TO dectement the Event counter from 40 to ZERO. whenActive fault goes to Stored */
unsigned char FMemLibLF_DecCounter(u_temp_l Counter, u_temp_l MaxValue);

u_8Bit    ecu_1min_counter_c, ecu_keyon_counter_c; //Counter Increments in 320ms for life time and key on
u_8Bit    ecu_life_counter_c; //For every 5min this counter clears and update set to EEPROM
union long_char fmem_lifetime_l;  //ECU Life time in 1 min increments           
u_16Bit   fmem_keyon_time_w; //ECU time from KEY ON in 15sec Increments
u_16Bit   fmem_keyon_counter_w;  //Number of IGN cycles for the ECU Life time
u_8Bit    fmem_nm_status_c; // Switch case to process the NM Fail Status Info regarding DTCs

u_8Bit fmem_genericFailSts;
u_8Bit fmem_currentFailSts;

unsigned char d_HistInterrogRecordEnable_t;

unsigned char cyclic_dtc_count_c;
unsigned char dtc_loop_count_c;
unsigned char no_of_dtc_c;
unsigned char fault_update_case_c; //Switch case to process DTC updates to EEPROM.

/* FMemLibF_Init	*/
@far void FMemLibF_Init(void)
{

	fault_update_case_c = 0;
	fmem_nm_status_c = 0;
	/* By default enable the Report DTCs and disable the updates to Historical Int record. */
	d_DtcSettingModeEnabled_t = TRUE;
	d_HistInterrogRecordEnable_t = FALSE;
	d_OperationCycle_c = OPERATION_CYLE_ON;
	/* Set the Ignition start 5sec timer.                                                                            */
	/* If IGN goes to Start and comes back to RUN, we start 5sec timer. TIll 5sec, logic should not update the DTCs. */
	/* Within 5 seconds after a starting phase no DTC action will be permitted                                       */
	d_IgnitionStart5secTimer_c = D_5_SEC_K;

	ecu_1min_counter_c = CLEAR_COUNTER; //Clear the one min increment counter for updating the  ECU Life time variable in RAM
	ecu_keyon_counter_c = CLEAR_COUNTER; //Clear the 15sec increment counter for updating the ECU KEY ON time variable in RAM
	ecu_life_counter_c = CLEAR_COUNTER; //Clear the 5 min counter for updating ECU Life time and KEY ON in EEPROM
	
	fmem_keyon_time_w = CLEAR_COUNTER; // Start fresh timer to keep the value of KEY ON TImer for each IGN cycle
	EE_BlockRead(EE_ECU_TIMESTAMPS, (u_8Bit*)&fmem_lifetime_l); //Read the stored value of life time from EEPROM
	
	EE_BlockRead(EE_ECU_KEYON_COUNTER, (u_8Bit*)&fmem_keyon_counter_w); //Read the stored value of IGN Cycles of ECU Life time
	
	/* Copy the EEPROM DTC information to Local */
	FMemApplInit();
	
}

/* FMemLibF_CyclicDtcTask	*/
/* Description of Cyclic Task, Read before analyzing the function */
/*                                                                                                                                                                                                                */
/* Heart of Library.                                                                                                                                                                                              */
/*                                                                                                                                                                                                                */
/* This function processes the error states announced by the application and acts in the error memory.                                                                                                            */
/*                                                                                                                                                                                                                */
/* A. Report or process any DTCs, only incase DTC setting Mode is enabled.(By default make sure this Flag is set to TRUE)                                                                                         */
/*                                                                                                                                                                                                                */
/* B: With In 5seconds after starting phase of module (Generally after POR), do not process any DTCs.                                                                                                             */
/*                                                                                                                                                                                                                */
/* C: As we have total of 46 DTCs, and by processing all the 48 DTCs (this function needs to finish with in 1.25ms) we are exceeding the timeslice loop.                                                          */
/* Because of this we modified the code to not exceed 1.25ms at any point of time..                                                                                                                               */
/* Modification: Instead of dealing with 48 DTCs contineously in same loop, now the loop is devided into 4 times.                                                                                                 */
/* Means every time we come inside this function we monitor for 12 DTCS. By doing this we are safe with Timeslice.                                                                                                */
/*                                                                                                                                                                                                                */
/* D: Process every time 12 DTCs. Will serve each and every DTC from the list, even it does not set.                                                                                                              */
/*                                                                                                                                                                                                                */
/* E1: Check the DTC reported state from the Application. Each and every DTC should be with either ERROR_OFF or ERROR_ON. Initial state with ERROR_NO_CHECK.                                                      */
/* E1A: (ERROR_OFF): Set the DTC Status bit "TestNotCompletedSinceLastClear" to FALSE.                                                                                                                            */
/* Means we completed the test and found no Issues.                                                                                                                                                               */
/* E1B: (ERROR_ON): When particular DTC is ON, then it start counting the Mature time.                                                                                                                            */
/* For CSWM this logic is not necessary because we are maturing the fault in Application itself. Because of this with out changing the code, we made the Mature time as Zero for all the DTCs in DTC table.       */
/* E1C: (ERROR_NO_CHECK): Nothing to Do.                                                                                                                                                                          */
/*                                                                                                                                                                                                                */
/* E2: Check for Mature time completed or not incase E1B is set.                                                                                                                                                  */
/* If completes then we are ready to transfer DTC to Memory, else we wait untill Mature time completes.                                                                                                           */
/* For CSWM If we are here means we are always Ready to Transfer information to EEPROM.                                                                                                                           */
/*                                                                                                                                                                                                                */
/* E3: Check to see the If the processed DTC is already in DTC Chrono Stack or not?                                                                                                                               */
/* D_FOUND_K: Available in ChronoStack                                                                                                                                                                            */
/* D_NOT_FOUND_K: Not Available in ChronoStack                                                                                                                                                                    */
/*                                                                                                                                                                                                                */
/* E4: Before updating any check to see how many DTCs are present in Chrono Stack.                                                                                                                                */
/*                                                                                                                                                                                                                */
/* From E1(Application) and E3(EEPROM) we have the following combinations.                                                                                                                                        */
/*                                                                                                                                                                                                                */
/* D_NOT_FOUND_K with ERROR_OFF_K                                                                                                                                                                                 */
/* D_NOT_FOUND_K with ERROR_ON_K                                                                                                                                                                                  */
/* D_FOUND_K with ERROR_OFF_K                                                                                                                                                                                     */
/* D_FOUND_K with ERROR_ON_K                                                                                                                                                                                      */
/*                                                                                                                                                                                                                */
/* F1: (D_NOT_FOUND_K with ERROR_OFF_K)                                                                                                                                                                           */
/* Processed DTC is not available in EEPROM and Its application state is no faulty.                                                                                                                               */
/* If the Processed DTC falls in this category, then set the DTC status bit (TestFailed to FALSE). Out from Loop go to step "E", Process the next available DTC.                                                  */
/*                                                                                                                                                                                                                */
/* F2: (D_NOT_FOUNDED_K with ERROR_ON_K)                                                                                                                                                                          */
/* Means Processed DTC is not found in EEPROM and Its application state is Faulty.                                                                                                                                */
/* If the processed DTC falls inthis category, then we had problem in Application and we matured the DTC. Now we need to store this DTC information in EEPROM.(New error, is not located yet in the error memory) */
/*                                                                                                                                                                                                                */
/* F2A: First update the DTC status byte information Locally.                                                                                                                                                     */
/* TestFailed = TRUE (Informs the status byte that, this particular DTC test Failed)                                                                                                                              */
/* TestFailedSinceLastClear = TRUE (Indicates Test is failed since Last clear)                                                                                                                                    */
/* TestNotCompletedSinceLastClear = FALSE (Indicates Test completed)                                                                                                                                              */
/* WarningIndicatorRequested = FALSE (We are not supporting the Warning Lamp Indicator)                                                                                                                           */
/*                                                                                                                                                                                                                */
/* F2B: From Step "E4", we receive the information from Chrono Stack that how many faults are present.                                                                                                            */
/* Max chrono Stack size for CSWM is 8.                                                                                                                                                                           */
/* If the number is Less than 8 then Move the stack down one shift                                                                                                                                                */
/* Else If module supports DTC Priority, check for the less priority numbers in stack, then if available delete the lowest priority DTC and move down one shift the stack.                                        */
/* Else discard the new DTC as Chrono Stack is FULL with DTCs.                                                                                                                                                    */
/*                                                                                                                                                                                                                */
/* F2C: ADD THE RECORD TO CHRONO STACK (First by updating the local Stack and then by updating the EEPROM)                                                                                                        */
/* Update DTC Status: Confirmed = TRUE;                                                                                                                                                                           */
/* Update DTC Index                                                                                                                                                                                               */
/* Update frequency_counter = 1                                                                                                                                                                                   */
/* Update operation_cycle_counter = 40                                                                                                                                                                            */
/* Update ECU Lifetime and ECU Lifetime from KeyON, Key On counter to the record.                                                                                                                                 */
/* Also update If the very first time DTC is matured for this ECU the Lifetime, Lifetime from KeyON at seperate NVM areas                                                                                         */
/* Also update the First confirmed DTC and recent confirmed DTCs respectively in this section at seperate NVM areas                             																  */
/* Update the Stack in EEPROM.                                                                                                                                                                                    */
/* Out from Loop go to step "E", Process the next available DTC.                                                                                                                                                  */
/*                                                                                                                                                                                                                */
/* F3: (D_FOUNDED_K with ERROR_OFF_K)                                                                                                                                                                             */
/* Means Processed DTC is found in EEPROM and its present application status is no faulty.                                                                                                                        */
/* Check is the first time we are comming after the fault is de-matured.                                                                                                                                          */
/* If YES, then Update the DTC status byte "TestFailed = FALSE"                                                                                                                                                   */
/* Update the Stack both Locally and in EEPROM.                                                                                                                                                                   */
/* If NO, means we are already de-matured the fault and now check for Self Healing.                                                                                                                               */
/* If The Opearation cycle Reaches 40, delete the fault from Stack and move the stack down one line.                                                                                                             */
/*                                                                                                                                                                                                                */
/* If any Priority 1 DTCs available, for supported Modules , logic will not self heal those DTCs.                                                                                                                 */
/*                                                                                                                                                                                                                */
/* F4: (D_FOUNDED_K with ERROR_ON_K)                                                                                                                                                                              */
/* Means Processed DTC is in EEPROM and Its Present status is Faulty.                                                                                                                                             */
/* By 2 cases we can come here.                                                                                                                                                                                   */
/* Case 1: DTC status is matured and (In this case Logic will do nothing)                                                                                                                                         */
/* Case 2: DTC status is de-matured again DTC becomes active.                                                                                                                                                     */
/* In this case as status changed from stored to active, update the following                                                                                                                                     */
/*                                                                                                                                                                                                                */
/* Update TestFailed = TRUE                                                                                                                                                                                       */
/* Update operation_cycle_counter = 0                                                                                                                                                                             */
/* Update frequency_counter++; Latches at 255                                                                                                                                                                     */
/* Update Snapshot Data 2nd record here																																											  */
/* Update the Stack in EEPROM.                                                                                                                                                                                    */
/* Out from Loop go to step "E", Process the next available DTC.                                                                                                                                                  */
/*                                                                                                                                                                                                                */
/*                                                                                                                                                                                                                */
/* Modifications to this Function by Harsha 																													   */
/* 1. Step D got introduced.                                                                                                                                       */
/*                                                                                                                                                                 */
/* D: As we have total of 47 DTCs, and by processing all the 47 DTCs (this function needs to finish with in 1.25ms) we are exceeding the timeslice loop.           */
/* Because of this we modified the code to not exceed 1.25ms at any point of time..                                                                                */
/* Modification: Instead of dealing with 47 DTCs contineously in same loop, now the loop is devided into 4 times.                                                  */
/* Means every time we come inside this function we monitor for 12 DTCS. By doing this we are safe with Timeslice.                                                 */
/*                                                                                                                                                                 */
/* E: Process every time 12 DTCs. Will serve each and every DTC from the list, even it does not set. (Earlier with actual Library file we were processing 44 DTCs) */
/*                                                                                                                                                                 */
/* 2. Step F4: Got modified                                                                                                                                        */
/* Modification:                                                                                                                                                   */
/* Most Recent Odometer value will get updated only when DTC status changes from Stored to active                                                                  */
/* Earler the logic was updating the most recent odo meter always.                                                                                                 */
/*                                                                                                                                                                 */
/* 3. FlashF_WriteEEPOM() added where ever necessary to store the information to EEPROM.                                                                           */
/*                                                                                                                                                                 */
@far void FMemLibF_CyclicDtcTask(void)
{
    unsigned char temp_recent_conf_dtc, DTCMemIndex = 0; //Because of Warning 676, this variable introduced.
	/* temp. Variable */
	u_temp_l   Index, SearchResult, TransferDtcToFaultMemory, NumberOfRegisteredDtc, dtc_in_chrono = 0, dtc_in_hist = 0 ;
	temp_l     DtcMemoryIndex, DtcHistIndex;
	union long_char  temp_lifetime_l, temp_lifetime_firstdtc;  //ECU Life time in 1 min increments           

	struct DTC_MEMORY_ELEMENT    TempDtcMemoryElement;
	unsigned short            temp_time_ecu_keyon;
	unsigned char temp_first_confirmeddtc;
	
	/* Cyclic DTC Task */
	/* A. Is DTC settings Enables? */
	if (d_DtcSettingModeEnabled_t == TRUE)
	{
		/* B. within 5 seconds after a starting phase no DTC permit */
		if (d_IgnitionStart5secTimer_c != 0)
		{
			/* Do nothing. For the first 5sec */
		}
		else
		{
			/* C.Process DTC Loop Logic */
			/* Split the Total no of DTCs processing to 4 times. Each time we will be processing 12 DTCs. */
			/* WHen logic calls for first time Loop count is set with Zero                                */
			if (dtc_loop_count_c < DTC_PROCESS_TIMES)
			{
				/* Start up of Index every time */
				cyclic_dtc_count_c = (unsigned char)(dtc_loop_count_c * DTC_PROCESS_EACH_TIME);
				/* No of DTCs Count. */
				no_of_dtc_c = cyclic_dtc_count_c + DTC_PROCESS_EACH_TIME;
				/* Prepare the loop for next itteration */
				dtc_loop_count_c++;
				
				/* Start of change: Added on 04.28.09 by CK: */				
				/* Check if dtc_loop_count_c is equal to DTC_PROCESS_TIMES (4) */
				if(dtc_loop_count_c == DTC_PROCESS_TIMES){
					/* no_of_dtc_c variable becomes 48 (4*12) when dtc_loop_count_c is equal to DTC_PROCESS_TIMES (4) */
					/* In the following for loop 'Index' variable is incremented two extra times for PWR_NET */
					/* 46 DTC are supported instead of 48 for PWR_NET architecture */
					/* This results in clearing RoE_config_status_c variable. Index is out of boundry for d_AllDtc_a[]. */
					/* Thus, Roe message is not send when a fault is matured(active) or stored */
					/* Decrement the no_of_dtc_c variable by two to match to 46 DTCs. */
					no_of_dtc_c -= 2;
					/* End of Change. */
				}
			}
			else
			{
				dtc_loop_count_c = 0;
				cyclic_dtc_count_c = (unsigned char)(dtc_loop_count_c * DTC_PROCESS_EACH_TIME);
				no_of_dtc_c = cyclic_dtc_count_c + DTC_PROCESS_EACH_TIME;
				dtc_loop_count_c++;
			}
			/* D. Process every time 12 DTCs */
			for (Index = cyclic_dtc_count_c; Index < no_of_dtc_c; Index++)
			{
				/* E1: Check the DTC reported state from the Application.          */
				/* Each and every DTC should be with either ERROR_OFF or ERROR_ON. */
				/* Initial state with ERROR_NO_CHECK.                              */
				switch (d_AllDtc_a[Index].reported_state) {
					/* E1A: Case ERROR_OFF_K */
					case ERROR_OFF_K:
					{
						/* Clear the time. This is not neccessary for CSWM. Because this DTC_ON_TIME is like Mature time for DTC. This will be taken care at individual DTC level. */
						d_AllDtc_a[Index].dtc_on_time = 0;
						/* Error path set the status bit to FALSE. Means we completed the test and found no Issues */
						d_AllDtc_a[Index].DtcState.state.TestFailedThisMonitoringCycle = FALSE;
						d_AllDtc_a[Index].DtcState.state.Pending = FALSE;
						
						break;
					}
					/* E1B: Case ERROR_ON_K */
					case ERROR_ON_K:
					{
						//Time, which is present the error, Mature time for error
						d_AllDtc_a[Index].dtc_on_time = FMemLibLF_IncCounter(d_AllDtc_a[Index].dtc_on_time, fmem_AllRegisteredDtc_t[Index].dtc_error_time);
						break;
					}
					/* E1C: Case ERROR_NO_CHECK_K */
					case ERROR_NO_CHECK_K:
					{
						//Nothing to do
						break;
					}
				}
				/* E2: Is mature time happened for the DTC to set? */
				if (d_AllDtc_a[Index].dtc_on_time >=  fmem_AllRegisteredDtc_t[Index].dtc_error_time)
				{
					TransferDtcToFaultMemory = TRUE;
				}
				else
				{
					TransferDtcToFaultMemory = FALSE;
				}
				/* E3: Is this error already present in Chrono? */
				if ((DtcMemoryIndex = FMemLibLF_SearchDtcIndexInDtcMemory((unsigned char)Index, FAULT_MEMORY)) >= 0)
				{
					SearchResult = D_FOUNDED_K;
					DTCMemIndex = (unsigned char)DtcMemoryIndex; //Because of Warning 676, this variable introduced.
				}
				else
				{
					SearchResult = D_NOT_FOUNDED_K;
				}
				/* E4: Determine the no of errors in Chrono stack */
				NumberOfRegisteredDtc = FMemLibLF_GetNumberOfRegisteredDtc(FAULT_MEMORY);
				switch (SearchResult | d_AllDtc_a[Index].reported_state) {
					/* F1: Case D_NOT_FOUNDED_K | ERROR_OFF_K */
					case D_NOT_FOUNDED_K | ERROR_OFF_K:
					{
						//in the error memory of errors not standing yet is inactive
						d_AllDtc_a[Index].DtcState.state.TestFailed = FALSE;
						break;
					}
					/* F2: Case D_NOT_FOUNDED_K | ERROR_ON_K */
					case D_NOT_FOUNDED_K | ERROR_ON_K:
					{
						//new error, is not located yet in the error memory
						if (TransferDtcToFaultMemory == TRUE)
						{
							/* F2A: Update the status byte information */
							// Update the status
							d_AllDtc_a[Index].DtcState.state.TestFailed = TRUE;
							d_AllDtc_a[Index].DtcState.state.TestFailedThisMonitoringCycle = TRUE;
							d_AllDtc_a[Index].DtcState.state.Pending = TRUE;

							if (fmem_AllRegisteredDtc_t[Index].dtc_warning_indicator == TRUE) {
								d_AllDtc_a[Index].DtcState.state.WarningIndicatorRequested = TRUE;
							}
							else
							{
								d_AllDtc_a[Index].DtcState.state.WarningIndicatorRequested = FALSE;
							}
							
							/* F2B: Check for the position in Chrono and If available update the local stack by moving down one shift. Else Discard the fault */
							/* Check the no of DTCs present in DTC memory */
							if (NumberOfRegisteredDtc < SIZEOF_DTC_MEMORY)
							{
#ifndef gesamter_Fehlerspeicher
								//all registered errors around one downward shift
								fmemLF_MemMove((unsigned char*)&d_DtcMemory.elem[1], (unsigned char*)&d_DtcMemory.elem[0], (sizeof(struct DTC_MEMORY_ELEMENT)*NumberOfRegisteredDtc));
#else
								//entire error memory around one downward shift
								fmemLF_MemMove((unsigned char*)&d_DtcMemory.elem[1], (unsigned char*)&d_DtcMemory.elem[0], (sizeof(struct DTC_MEMORY_ELEMENT)*(SIZEOF_DTC_MEMORY-1)));
#endif
							}
							else
							{
								/* If Number is greater than size of chrono, get the lowest priority DTC from Chrono */
								if ((DtcMemoryIndex = FMemLibLF_SearchDtcIndexWithLowerPriority(fmem_AllRegisteredDtc_t[Index].dtc_priority)) >= 0)
								{
									FMemLibLF_ClearSingleDtc(d_DtcMemory.elem[DtcMemoryIndex].dtc_RomIndex);

									fmemLF_MemMove((unsigned char*)&d_DtcMemory.elem[1], (unsigned char*)&d_DtcMemory.elem[0], (sizeof(struct DTC_MEMORY_ELEMENT)* (u_temp_l)DtcMemoryIndex) );
								}
								else
								{
									break;
								}
							}
							
							/* F2C: Add the record to EEPROM Chrono stack */
							// Errors to first position of the error memory register
							d_DtcMemory.elem[0].dtc_RomIndex = (unsigned char)Index;
							d_AllDtc_a[Index].DtcState.state.Confirmed = TRUE;
							d_DtcMemory.elem[0].frequency_counter = 1;
							d_DtcMemory.elem[0].operation_cycle_counter = D_SELF_HEALING_CRITERION_K;
							//Update the chrono stack with ECU Life time and ECU Key on time values
							d_DtcMemory.elem[0].ecu_lifetime = fmem_lifetime_l;
							d_DtcMemory.elem[0].ecu_time_keyon = fmem_keyon_time_w;
							d_DtcMemory.elem[0].key_on_counter = fmem_keyon_counter_w; 
							
							/* Write to SHadow RAM */
							update_chrono_stack_b = TRUE;   
							/* Write to SHadow RAM */
							update_dtc_status_info_b = TRUE;
							
							//Read the First time DTC Life time value from EEPROM
							EE_BlockRead(EE_ECU_TIME_FIRSTDTC, (u_8Bit*)&temp_lifetime_firstdtc);
							EE_BlockRead(EE_ECU_TIME_KEYON_FIRSTDTC, (u_8Bit*)&temp_time_ecu_keyon);
							
							//If the area is all FF's and then a DTC is matured means First come based fill the area
							//This will be used in $190C service. 
							EE_BlockRead(EE_FIRST_CONF_DTC, &temp_first_confirmeddtc);
							if(temp_first_confirmeddtc == 0xFF)
							{
								temp_first_confirmeddtc = d_DtcMemory.elem[0].dtc_RomIndex;
								EE_BlockWrite(EE_FIRST_CONF_DTC, &temp_first_confirmeddtc);
							}
							
							//We are here means a new DTC is got matured. store the new DTC in EEP.
							//This will be used in $190E service.
							EE_BlockWrite(EE_RECENT_CONF_DTC, (u_8Bit*)&d_DtcMemory.elem[0].dtc_RomIndex);
							
							/* If the value from EEPROM is not updated means, nothing has been stored */
							/* Update the ECU TIME STAMPS when the first time DTC occurs */
							/* Also update the ECU Time stamps since key ON when the first time DTC occurs */
							if (temp_lifetime_firstdtc.l == 0x00)
							{
								EE_BlockWrite(EE_ECU_TIME_FIRSTDTC, (u_8Bit*)&fmem_lifetime_l);
								EE_BlockWrite(EE_ECU_TIME_KEYON_FIRSTDTC, (u_8Bit*)&fmem_keyon_time_w);
							}
							else
							{
								//First time DTC variables updated in EEPROM. Nothing todo
							}
							
							//We are here means There is an active DTC set this flag
							//Used in NWM_CSWM
							//nwm_current_fail_state_b = TRUE;
							
						}
						else
						{
							break;
						}
						break;
					}
					/* F3: Case D_FOUNDED_K | ERROR_OFF_K */
					case D_FOUNDED_K | ERROR_OFF_K:
					{
						/* If we are comming first time after the fault is de-matured? */
						if (d_AllDtc_a[Index].DtcState.state.TestFailed == TRUE)
						{
							/* Status Update */
							d_AllDtc_a[Index].DtcState.state.TestFailed = FALSE;
							d_AllDtc_a[Index].DtcState.state.TestFailedThisMonitoringCycle = FALSE;
							d_AllDtc_a[Index].DtcState.state.Pending = FALSE;
							
							if(d_OperationCycle_c == OPERATION_CYLE_ON)
							{
								// DTC got dematured in first 10sec of POR.
								// FMemLibLF_DecDtcOperationCyclCounter will Decrement the Operation cycle counter.
								//d_DtcMemory.elem[DTCMemIndex].operation_cycle_counter = 0;
								//CUSW Update. Keep the Operation cycle counter at 40.
								d_DtcMemory.elem[DTCMemIndex].operation_cycle_counter = D_SELF_HEALING_CRITERION_K;
							}
							else
							{
								// DTC got de matured after 10sec of POR. Decrement the Operation cycle counter to 39.
								//Because FMemLibLF_DecDtcOperationCyclCounter function will not get called for this IGN cycle.
								//The function will get called only once after completing 10sec and then locked for the entire IGN cycle.
								//d_DtcMemory.elem[DTCMemIndex].operation_cycle_counter = 1;
								d_DtcMemory.elem[DTCMemIndex].operation_cycle_counter = 39; //CUSW update
							}

							/* Write to SHadow RAM */
							update_chrono_stack_b = TRUE;      
							/* Write to SHadow RAM */
							update_dtc_status_info_b = TRUE;
							
						}
						else
						{
							/* Check to see the particular proceesed DTC is not with Prority 1 labled. Then check for Operation cycles. Once the operation cycle reaches 100, logic remvoes from the stack and shifts down one. */
							if (fmem_AllRegisteredDtc_t[Index].dtc_priority != D_PRIO_1_K)
							{
								if (d_DtcMemory.elem[DTCMemIndex].operation_cycle_counter == 0)
								{
									//It reached the self healing/Event counter to 0 from 40. Move information to Historical Stack    
									//FMemLibLF_TransferFaultToHistoricalStack(DtcMemoryIndex, 1);
									// Clear the information in chrono stack
									FMemLibLF_ClearSingleDtc((unsigned char)Index);
									fmemLF_MemMove((unsigned char*)&d_DtcMemory.elem[DTCMemIndex], (unsigned char*)&d_DtcMemory.elem[DTCMemIndex+1], (sizeof(struct DTC_MEMORY_ELEMENT)*((NumberOfRegisteredDtc-1) - (u_temp_l)DtcMemoryIndex)));

									fmemLF_MemSet((unsigned char*)&d_DtcMemory.elem[NumberOfRegisteredDtc-1], 0xFF, sizeof(struct DTC_MEMORY_ELEMENT));
									/* MKS 91913 */									
									/* Clear the information in Historical stack snapshot 2	  */
									dtc_in_hist = FMemLibLF_GetNumberOfRegisteredDtc(HISTORICAL_MEMORY);
						
									/* MKS 98347 Reset during self healing on Historical Stack*/	
									DtcHistIndex = FMemLibLF_SearchDtcIndexInDtcMemory((unsigned char)Index, HISTORICAL_MEMORY);
									
									if((DtcHistIndex >= 0) && (dtc_in_hist > 0)) /* Clear only if the searched DTC is present in historical stack */
									{
										/* Clearing the searched DTC in historical stack */ 
										fmemLF_MemMove((unsigned char*)&d_DtcHistoricalMemory.elem[DtcHistIndex], (unsigned char*)&d_DtcHistoricalMemory.elem[DtcHistIndex+1], (sizeof(struct DTC_HISTORICAL_MEMORY_ELEMENT)*((dtc_in_hist-1) - (u_temp_l)DtcHistIndex)));
										fmemLF_MemSet((unsigned char*)&d_DtcHistoricalMemory.elem[dtc_in_hist-1], 0xFF, sizeof(struct DTC_HISTORICAL_MEMORY_ELEMENT));
									}									
									else
									{
										//No need to move the historical stack
									}
									
                                    //After self Healing the DTC, now check any DTCs present in Chrono Stack.
									// Get the No of DTCs Registered in Chrono
									dtc_in_chrono = FMemLibLF_GetNumberOfRegisteredDtc(FAULT_MEMORY);
									
									/* If there are no DTC's in Chrono Stack means, clear the information */
									/* Else, do not modify */
									if (0 == dtc_in_chrono)
									{
										//CUSW Updates
										temp_lifetime_firstdtc.l = 0x00;
										temp_time_ecu_keyon = 0x00;
										temp_recent_conf_dtc = 0xFF;
										EE_BlockWrite(EE_ECU_TIME_FIRSTDTC, (u_8Bit*)&temp_lifetime_firstdtc);
										EE_BlockWrite(EE_ECU_TIME_KEYON_FIRSTDTC, (u_8Bit*)&temp_time_ecu_keyon);
										EE_BlockWrite(EE_RECENT_CONF_DTC, &temp_recent_conf_dtc);
										
									}
									
									/* Write to SHadow RAM */
									update_chrono_stack_b = TRUE;     
									/* MKS 91913 */	
									/* Write to SHadow RAM */
									update_historical_stack_b = TRUE;
									/* Write to SHadow RAM */
									update_dtc_status_info_b = TRUE;
							
								}
							}
						}
						//We are here means There is an stored DTC set this flag
						//Used in NWM_CSWM
						//nwm_generic_fail_state_b = TRUE;
						break;
					}
					/* F4: Case D_FOUNDED_K | ERROR_ON_K */
					case D_FOUNDED_K | ERROR_ON_K:
					{
						if (TransferDtcToFaultMemory == TRUE)
						{
							/* Status change? */
							if (d_AllDtc_a[Index].DtcState.state.TestFailed == FALSE)
							{
								d_AllDtc_a[Index].DtcState.state.TestFailed = TRUE;
								d_AllDtc_a[Index].DtcState.state.TestFailedThisMonitoringCycle = TRUE;
								d_AllDtc_a[Index].DtcState.state.Pending = TRUE;
								
								FMemLibLF_TransferFaultToHistoricalStack(DtcMemoryIndex, 1);
								
								//We are here means a new DTC is got matured. store the new DTC in EEP.
								//This will be used as Most recent confirmed DTC.
								EE_BlockWrite(EE_RECENT_CONF_DTC, (u_8Bit*)&d_DtcMemory.elem[DTCMemIndex].dtc_RomIndex);

								d_DtcMemory.elem[DTCMemIndex].operation_cycle_counter = D_SELF_HEALING_CRITERION_K;
								
								d_DtcMemory.elem[DTCMemIndex].frequency_counter = FMemLibLF_IncCounter((u_temp_l)(d_DtcMemory.elem[DTCMemIndex].frequency_counter), 0xFF);
								
								fmemLF_MemCpy((unsigned char*)&TempDtcMemoryElement, (unsigned char*)&d_DtcMemory.elem[DTCMemIndex], sizeof(struct DTC_MEMORY_ELEMENT));
								
								fmemLF_MemMove((unsigned char*)&d_DtcMemory.elem[1], (unsigned char*)&d_DtcMemory.elem[0], (sizeof(struct DTC_MEMORY_ELEMENT)* DTCMemIndex));
								
								fmemLF_MemCpy((unsigned char*)&d_DtcMemory, (unsigned char*)&TempDtcMemoryElement, sizeof(struct DTC_MEMORY_ELEMENT));
								/* Write to SHadow RAM */
								update_chrono_stack_b = TRUE;      
								/* Write to SHadow RAM */
								update_dtc_status_info_b = TRUE;
							
							}
						}
						break;
					}
					default :
					{
					}
				}
			}
		}
		
	}
	/* Call EEPROM update for any updates to DTC stack */
	FMemLibF_UpdatestoEEPROM(); 
	/* Call for ECU Time stamps Update */
	FMemLibF_ECUTimeStamps();
}

void FMemLibF_ClearDiagnosticInformation(enum DIAG_RESPONSE* Response, DescMsgContext* pMsgContext)
{
	u_temp_l NumberOfRegisteredDtc;
	temp_l Index, DtcMemoryIndex;
	
	DtcStructType    TempDtcCode;
	TempDtcCode.all = 0;

	// Get the DTC code from the request buffer
	TempDtcCode.byte[1] = pMsgContext->reqData[0]; //Harsha..Re arranged
	TempDtcCode.byte[2] = pMsgContext->reqData[1];
	TempDtcCode.byte[3] = pMsgContext->reqData[2];

	//Get the No of DTCs Registered in Chrono
	NumberOfRegisteredDtc = FMemLibLF_GetNumberOfRegisteredDtc(FAULT_MEMORY);

	if (pMsgContext->reqDataLen == 3)
	{
		switch (TempDtcCode.all) {
			/* case D_EMISSION_RELATED_SYSTEMS_K, D_ALL_POWERTRAIN_DTC_K, D_ALL_CHASSIS_DTC_K */
			case D_EMISSION_RELATED_SYSTEMS_K:
			case D_ALL_POWERTRAIN_DTC_K:
			case D_ALL_CHASSIS_DTC_K:
			{
				/* Prepare the response */
				*Response = REQUEST_OUT_OF_RANGE;
				break;
			}
			/* case D_ALL_BODY_DTC_K, D_ALL_NET_DTC_K, D_ALL_DTC_K */
			case D_ALL_BODY_DTC_K:
			case D_ALL_NET_DTC_K:
			case D_ALL_DTC_K:
			{
				//FMemLibLF_TransferFaultToHistoricalStack(0, (temp_l)NumberOfRegisteredDtc);
				/* Clear the chrono stack */
				FMemLibF_ClearDtcMemory(FAULT_MEMORY);      //Clear snapshot record 1 stack
				/* MKS 91913 */	
				FMemLibF_ClearDtcMemory(HISTORICAL_MEMORY); //Clear snapshot record 2 stack
				*Response = RESPONSE_OK;
				break;
			}
			default :
			{
				/* Is this error present in list? */
				if ((Index = FMemLibF_CheckDtcSupportOverDtc(TempDtcCode.all)) >= 0) {
					/* Is this error present in chrono memory */
					if ((DtcMemoryIndex = FMemLibLF_SearchDtcIndexInDtcMemory((unsigned char)Index, FAULT_MEMORY)) >= 0)
					{
						// Transfer the chrono error to historical memory
						//FMemLibLF_TransferFaultToHistoricalStack(DtcMemoryIndex, 1);
						// Clear the error
						FMemLibLF_ClearSingleDtc((unsigned char)Index);
						// alle unter diesem Fehler eingetragenen Fehler um eins nach oben shiften
						fmemLF_MemMove((unsigned char*)&d_DtcMemory.elem[DtcMemoryIndex], (unsigned char*)&d_DtcMemory.elem[DtcMemoryIndex+1], (sizeof(struct DTC_MEMORY_ELEMENT)*((NumberOfRegisteredDtc-1) - (u_temp_l)DtcMemoryIndex)));
						// l�sche das letzte Element im Fehlerspeicher
						fmemLF_MemSet((unsigned char*)&d_DtcMemory.elem[NumberOfRegisteredDtc-1], 0xFF, sizeof(struct DTC_MEMORY_ELEMENT));
				
					}
					else
					{
						// clear signle error
						FMemLibLF_ClearSingleDtc((unsigned char)Index);
					}
				}
				else
				{
					*Response = REQUEST_OUT_OF_RANGE;
				}
			}
		}
	}
	else
	{
		*Response = LENGTH_INVALID_FORMAT;
	}
}

void FMemLibF_ClearDtcMemory(enum FAULTMEMORY_TYP FaultMemoryTyp)
{
	u_temp_l Index;
	unsigned char temp_recent_conf_dtc;
	unsigned short temp_time_ecu_keyon;
	union long_char  temp_lifetime_firstdtc;  //ECU Life time in 1 min increments           

	switch (FaultMemoryTyp) {
		case FAULT_MEMORY:
		{
			/* go through all errors */
			for ( Index = 0; Index < SIZEOF_ALL_SUPPORTED_DTC; Index++)
			{
				FMemLibLF_ClearSingleDtc((unsigned char)Index);
			}
			/* Write to SHadow RAM */
			/* Set the Update Flag. So In next available Time slice we write contents to EEPROM. */
			update_dtc_status_info_b = TRUE;

#ifdef OHNE_MEMSET
			/* Go through error memory */
			for (Index = 0; Index < sizeof(struct DTC_MEMORY); Index++)
			{
				((unsigned char*)&d_DtcMemory)[Index] = 0xFF;
			}
#else
			fmemLF_MemSet((unsigned char*)&d_DtcMemory, 0xFF, sizeof(struct DTC_MEMORY));
			
//	/************************************* CUSW UPDATES *********************************************/
			temp_lifetime_firstdtc.l = 0x00;
			temp_time_ecu_keyon = 0x00;
			temp_recent_conf_dtc = 0xFF;
			EE_BlockWrite(EE_ECU_TIME_FIRSTDTC, (u_8Bit*)&temp_lifetime_firstdtc);
			EE_BlockWrite(EE_ECU_TIME_KEYON_FIRSTDTC, (u_8Bit*)&temp_time_ecu_keyon);
			EE_BlockWrite(EE_RECENT_CONF_DTC, &temp_recent_conf_dtc);
			EE_BlockWrite(EE_FIRST_CONF_DTC, &temp_recent_conf_dtc);
//	/************************************* CUSW UPDATES *********************************************/
			
#endif
			/* Write to SHadow RAM */
			/* Set the update flag. So in Next available timeslice we write contents to EEPROM. */
			update_chrono_stack_b = TRUE;   
			break;
		}
		case HISTORICAL_MEMORY:
		{
#ifdef OHNE_MEMSET
			for (Index = 0; Index < sizeof(struct DTC_HISTORICAL_MEMORY); Index++)
			{
				((unsigned char*)&d_DtcHistoricalMemory)[Index] = 0xFF;
			}
#else

			fmemLF_MemSet((unsigned char*)&d_DtcHistoricalMemory, 0xFF, sizeof(struct DTC_HISTORICAL_MEMORY));
#endif
			/* Write to SHadow RAM */
			/* Set the Update flag. So in Next available timeslice we write contents to EEPROM. */
			update_historical_stack_b = TRUE;    
			break;
		}
	}
}

/*********************************** FMemLibF_ReadDiagnosticInformation	***********************************/
/*                        SUPPORTED SERVICES for READ DIAGNOSTIC INFORMATION                              */       
/**********************************************************************************************************/
/*                                                                                                        */
/* 1: 19 02 (ReportDtcByStatusMask):                                                                      */
/* This Parameter specifies that the ECU Shall Transmit to the tester a list of DTCs and corresponding    */
/* Statuses matching a tester defined status MASK                                                         */
/*                                                                                                        */
/* 2: 19 04 (ReportDTCSnapshotRecordByDTCNumber):														  */
/* This Parameter specifies that the ECU shall Transmit to the tester the DTC Snapshot records associated */
/* with a tester defined DTC Number and DTCSnapshot record number (0xFF - For all Records)                */
/*                                                                                                        */
/* 3: 19 06 (ReportDTCExtendedDataRecordByDTCNumber):													  */
/* This parameter specifies that the ECU shall transmit to the tester the DTC Extended Data records       */
/* associated with a tester defined DTC Number and DTC Extended Data Record Number (0xFF - ForAll Records)*/
/*                                                                                                        */
/* 4: 19 07 (ReportNumberOfDTCBySeverityMaskRecord)														  */
/* This parameter specifies that the server shall transmit to the client the number of DTCs matching a    */
/* client defined severity mask Record																	  */
/* NOTE: If the ECU does not support the severity mechanism the response must contains the number of      */
/* faults stored.																		    			  */
/*                                                                                                        */
/* 5: 19 08 (ReportDTCBySeverityMaskRecord)																  */
/* This parameter specifies that the server shall transmit to the client the number of DTCs and correspond*/
/* Statuses matching a client defined severity mask record                                                */
/* NOTE: If the ECU does not support the severity mechanism the response must contains the faults stored  */
/*                                                                                                        */
/* 6: 19 09 (ReportSeverityInformationOfDTC)															  */
/* This parameter specifies that the server shall transmit to the client the severity information of a    */
/* specifc DTC specified in the client request message                                                    */
/* NOTE: If the ECU does not support the severity mechanism the response must contains the single         */
/* supported class.																						  */
/*                                                                                                        */
/* 7: 19 0C (ReportFirstConfirmedDTC)																	  */
/* This parameter specifies that the server shall transmit to the client the first confirmed DTC to be    */
/* detected by the server since the last clear of diagnostic information.								  */
/* NOTE:The information reported via this sub function parameter shall be independant of the aging process*/
/* Ex: If a DTC ages such that its status is allowed to be reset, the first confirmed DTC record shall    */
/* to be preserved by the server, regardless of any other DTCs that become confirmed afterwords           */
/*                                                                                                        */
/* 8: 19 0E (ReportMostConfirmedDTC)																	  */
/* This parameter specifies that the server shall transmit to the client the most recent confirmed DTC    */
/* to be detected by the server since the last clear of diagnostic information.							  */
/**********************************************************************************************************/
/**********************************************************************************************************/

void FMemLibF_ReadDiagnosticInformation(enum DIAG_RESPONSE* Response, u_temp_l* DataLen, DescMsgContext* pMsgContext,  unsigned char Subservice)
{
	/* temp. Variable */
	temp_l  Index = 0, NumberOfRegisteredDtc, NumberOfMaskedDtc = 0, DtcMemoryIndex, DtcSearchMask;
	unsigned char temp_snapshot_nr;
	unsigned char temp_offset_nr;
	unsigned char temp_snapshot1_flag;
	unsigned char temp_snapshot2_flag;

	switch (Subservice) {
	
		case 0x02: // ReportDtcByStatusMask
		{
			// Mask search
			DtcSearchMask = pMsgContext->reqData[0];
			DtcSearchMask &= FMEM_DTC_AVAILABILITY_MASK_K;

			// Get the No of DTCs Registered in Chrono
			NumberOfRegisteredDtc = (temp_l)FMemLibLF_GetNumberOfRegisteredDtc(FAULT_MEMORY);

			/* If the received content is larger than max bufferlenght of 200 bytes? */
			if (((FMemLibLF_GetNumberOfMaskedDtc((unsigned char)DtcSearchMask, FAULT_MEMORY)*4)+2) < kDescPrimBufferLen)
			{
				//Fill the first byte .. DTC Status Availablity Mask
				pMsgContext->resData[0] = FMEM_DTC_AVAILABILITY_MASK_K;
				
				if (DtcSearchMask)
				{
					/* Go through all the registered errors */
					for (DtcMemoryIndex = 0; DtcMemoryIndex < NumberOfRegisteredDtc; DtcMemoryIndex++)
					{
						if (d_AllDtc_a[d_DtcMemory.elem[DtcMemoryIndex].dtc_RomIndex].DtcState.all & DtcSearchMask)
						{
							pMsgContext->resData[(NumberOfMaskedDtc*4)+1] = FMemLibLF_GetByteOfDtc(d_DtcMemory.elem[DtcMemoryIndex].dtc_RomIndex, 1);
							pMsgContext->resData[(NumberOfMaskedDtc*4)+2] = FMemLibLF_GetByteOfDtc(d_DtcMemory.elem[DtcMemoryIndex].dtc_RomIndex, 2);
							pMsgContext->resData[(NumberOfMaskedDtc*4)+3] = FMemLibLF_GetByteOfDtc(d_DtcMemory.elem[DtcMemoryIndex].dtc_RomIndex, 3);

							// Get the Error Status
							pMsgContext->resData[(NumberOfMaskedDtc*4)+4] = d_AllDtc_a[d_DtcMemory.elem[DtcMemoryIndex].dtc_RomIndex].DtcState.all;

							NumberOfMaskedDtc++;
						}
					}
					//Total Response length
					*DataLen = ((u_temp_l)NumberOfMaskedDtc*4)+1;
				}
				else
				{
					*DataLen = 1;
				}

				/* Set the response flag */
				*Response = RESPONSE_OK;
			}
			else
			{
				*Response = REQUEST_OUT_OF_RANGE;
			}
			
			break;
		}
		
		case 0x04: // ReportDTCSnapshotRecordByDTCNumber
		{
			DtcStructType    TempDtcCode;
			TempDtcCode.all = 0;
			temp_offset_nr = 0;
			temp_snapshot1_flag= 0;
			temp_snapshot2_flag= 0;



			// Get the DTC Code from the request buffer..Harsha Re aranged the buffer
			TempDtcCode.byte[1] = pMsgContext->reqData[0];
			TempDtcCode.byte[2] = pMsgContext->reqData[1];
			TempDtcCode.byte[3] = pMsgContext->reqData[2];

			temp_snapshot_nr = pMsgContext->reqData[3];
			
			/* Is the requested error exists in list?..Harsha added && condition */
			if ((Index = FMemLibF_CheckDtcSupportOverDtc(TempDtcCode.all)) >= 0 &&
			   (Index = FMemLibF_CheckDtcSupportOverDtc(TempDtcCode.all)) < SIZEOF_ALL_SUPPORTED_DTC) {
				/* Fill the DTC code in response buffer */
				pMsgContext->resData[0] = pMsgContext->reqData[0];
				pMsgContext->resData[1] = pMsgContext->reqData[1];
				pMsgContext->resData[2] = pMsgContext->reqData[2];
				pMsgContext->resData[3] = d_AllDtc_a[Index].DtcState.all;


				/* MKS 91804 Update "Report DTC Snapshot Record by DTC  numbers" */
				/* Fill the DTC status */
				//pMsgContext->resData[3] = d_AllDtc_a[Index].DtcState.all;
				/* Is error present in Chrono */
				if ( ((DtcMemoryIndex = FMemLibLF_SearchDtcIndexInDtcMemory((unsigned char)Index, FAULT_MEMORY)) >= 0) &&
				     ((temp_snapshot_nr == 0x00) || (temp_snapshot_nr == 0xFF))) {
					//Transfer the error to Historical memory
					//FMemLibLF_TransferFaultToHistoricalStack(DtcMemoryIndex, 1);

					//Arange the Environmental data here
					pMsgContext->resData[4] = 0x00; //DTC Snapshot Data Record Number First Record
					pMsgContext->resData[5] = 4; //Number of Record Identifiers in the snapshot.

					/* Get the data ECU Life time , ECU KEY ON time, Key on counter and DTC failure type */
					pMsgContext->resData[6] = 0x10;
					pMsgContext->resData[7] = 0x08;
					pMsgContext->resData[8] = d_DtcMemory.elem[DtcMemoryIndex].ecu_lifetime.lw.hhbyte;
					pMsgContext->resData[9] = d_DtcMemory.elem[DtcMemoryIndex].ecu_lifetime.lw.hbyte;
					pMsgContext->resData[10] = d_DtcMemory.elem[DtcMemoryIndex].ecu_lifetime.lw.lbyte;
					pMsgContext->resData[11] = d_DtcMemory.elem[DtcMemoryIndex].ecu_lifetime.lw.llbyte;

					pMsgContext->resData[12] = 0x10;
					pMsgContext->resData[13] = 0x09;
					pMsgContext->resData[14] = HIGHBYTE(d_DtcMemory.elem[DtcMemoryIndex].ecu_time_keyon);
					pMsgContext->resData[15] = LOWBYTE(d_DtcMemory.elem[DtcMemoryIndex].ecu_time_keyon);

					pMsgContext->resData[16] = 0x20;
					pMsgContext->resData[17] = 0x0A;
					pMsgContext->resData[18] = HIGHBYTE(d_DtcMemory.elem[DtcMemoryIndex].key_on_counter);
					pMsgContext->resData[19] = LOWBYTE(d_DtcMemory.elem[DtcMemoryIndex].key_on_counter);

					pMsgContext->resData[20] = 0x60;
					pMsgContext->resData[21] = 0x82;
					pMsgContext->resData[22] = pMsgContext->resData[2];
					/* First 19th bytes are fill snapshot 1 */
					temp_offset_nr = 19;
					/* Notified snapshot  1 was filled */
					temp_snapshot1_flag = TRUE;
					
					*DataLen = 23;
				}
				else
				{
					*DataLen = 4;
					 // Do nothing
				}
				/* MKS 91804 Update "Report DTC Snapshot Record by DTC  numbers" */
               if ( ((DtcMemoryIndex = FMemLibLF_SearchDtcIndexInDtcMemory((unsigned char)Index, HISTORICAL_MEMORY)) >= 0) &&
					     ((temp_snapshot_nr == 0x01) || (temp_snapshot_nr == 0xFF)))
				{
					//Arange the Environmental data here
					pMsgContext->resData[4+temp_offset_nr] = 0x01; //DTC Snapshot Data Record Number First Record
					pMsgContext->resData[5+temp_offset_nr] = 4; //Number of Records reported
					
					/* Get the data ECU Life time , ECU KEY ON time, Key on counter and DTC failure type */
					pMsgContext->resData[6+temp_offset_nr] = 0x10;
					pMsgContext->resData[7+temp_offset_nr] = 0x08;
					pMsgContext->resData[8+temp_offset_nr] = d_DtcHistoricalMemory.elem[DtcMemoryIndex].ecu_lifetime.lw.hhbyte;
					pMsgContext->resData[9+temp_offset_nr] = d_DtcHistoricalMemory.elem[DtcMemoryIndex].ecu_lifetime.lw.hbyte;
					pMsgContext->resData[10+temp_offset_nr] = d_DtcHistoricalMemory.elem[DtcMemoryIndex].ecu_lifetime.lw.lbyte;
					pMsgContext->resData[11+temp_offset_nr] = d_DtcHistoricalMemory.elem[DtcMemoryIndex].ecu_lifetime.lw.llbyte;

					pMsgContext->resData[12+temp_offset_nr] = 0x10;
					pMsgContext->resData[13+temp_offset_nr] = 0x09;
					pMsgContext->resData[14+temp_offset_nr] = HIGHBYTE(d_DtcHistoricalMemory.elem[DtcMemoryIndex].ecu_time_keyon);
					pMsgContext->resData[15+temp_offset_nr] = LOWBYTE(d_DtcHistoricalMemory.elem[DtcMemoryIndex].ecu_time_keyon);

					pMsgContext->resData[16+temp_offset_nr] = 0x20;
					pMsgContext->resData[17+temp_offset_nr] = 0x0A;
					pMsgContext->resData[18+temp_offset_nr] = HIGHBYTE(d_DtcHistoricalMemory.elem[DtcMemoryIndex].key_on_counter);
					pMsgContext->resData[19+temp_offset_nr] = LOWBYTE(d_DtcHistoricalMemory.elem[DtcMemoryIndex].key_on_counter);

					pMsgContext->resData[20+temp_offset_nr] = 0x60;
					pMsgContext->resData[21+temp_offset_nr] = 0x82;
					pMsgContext->resData[22+temp_offset_nr] = pMsgContext->resData[2];
					/* Notified snapshot  2 was filled */
					temp_snapshot2_flag = TRUE;

									
					if (temp_snapshot_nr == 0xFF)
					{
						/*Means both snapshots*/
						*DataLen = 42;
					}
					else
					{
						/* Just Snapshot 2*/
						*DataLen = 23;
					}
				}
				else
				{
					if(temp_snapshot1_flag == TRUE)
					{
						*DataLen = 23;
					}
					else
					{
						*DataLen = 4;
						
					}
				}

//				/* Neither Snapshot 1 nor Snapshot 2 */
//				if (! (temp_snapshot1_flag) && 	!(temp_snapshot2_flag))
//				{
//						*DataLen = 4;
//				}
//				else
//				{
//					 	// Do nothing
//				}
				*Response = RESPONSE_OK;
			}
			else {
				*Response = REQUEST_OUT_OF_RANGE;
			}
			break;
		}
		
		case 0x06: // ReportDtcExtendedDataRecordByDtcNumber
		{
			DtcStructType    TempDtcCode;
			TempDtcCode.all = 0;

			// Get the DTC Code from the request buffer..Harsha Re aranged the buffer
			TempDtcCode.byte[1] = pMsgContext->reqData[0];
			TempDtcCode.byte[2] = pMsgContext->reqData[1];
			TempDtcCode.byte[3] = pMsgContext->reqData[2];

			/* Is the requested error exists in list?..Harsha added && condition */
			if ((Index = FMemLibF_CheckDtcSupportOverDtc(TempDtcCode.all)) >= 0 &&
			   (Index = FMemLibF_CheckDtcSupportOverDtc(TempDtcCode.all)) < SIZEOF_ALL_SUPPORTED_DTC) {
				/* Fill the DTC code in response buffer */
				pMsgContext->resData[0] = pMsgContext->reqData[0];
				pMsgContext->resData[1] = pMsgContext->reqData[1];
				pMsgContext->resData[2] = pMsgContext->reqData[2];

				/* Fill the DTC status */
				pMsgContext->resData[3] = d_AllDtc_a[Index].DtcState.all;
				/* Is error present in Chrono */
				if ((DtcMemoryIndex = FMemLibLF_SearchDtcIndexInDtcMemory((unsigned char)Index, FAULT_MEMORY)) >= 0) {

					//Arange the Environmental data here
					pMsgContext->resData[4] = 1; //DTC Extended Data Record Number
					
					/* Umgebungsdaten */
//					pMsgContext->resData[5] = fmem_AllRegisteredDtc_t[Index].dtc_event; //occurance flag 
//					pMsgContext->resData[6] = d_DtcMemory.elem[DtcMemoryIndex].frequency_counter;
					pMsgContext->resData[5] = 0x60;
					pMsgContext->resData[6] = 0x80;
					pMsgContext->resData[7] = d_DtcMemory.elem[DtcMemoryIndex].operation_cycle_counter;
					
					pMsgContext->resData[8] = 0x60;
					pMsgContext->resData[9] = 0x81;
					pMsgContext->resData[10] = 0x00; //Warning Lamp is OFF
					
					*DataLen = 11;

				}
				else {
					*DataLen = 4;

				}
				*Response = RESPONSE_OK;
			}
			else {
				*Response = REQUEST_OUT_OF_RANGE;
			}
			//FMemLibLF_UpdateHistoricalInterrogationRecord();
			break;
		}
		
		case 0x07: // ReportNumberOfDtcBySeverityMaskRecord
		{
			//Just retreiving the number of DTCs in Stack, as we do not support Severity Mask 
			//Mask Search
			DtcSearchMask = pMsgContext->reqData[0];
			DtcSearchMask &= FMEM_DTC_AVAILABILITY_MASK_K;

			//Fill the first byte .. DTC Status Availablity Mask
			pMsgContext->resData[0] = FMEM_DTC_AVAILABILITY_MASK_K;

			//DTC Format Identifier
			pMsgContext->resData[1] = D_ISO15031_6DTC_FORMAT_K;//D_ISO14229_1DTC_FORMAT_K;

			/* Number of errors in 2 byte (high byte always 0, there of no more than 256 errors to be */
			pMsgContext->resData[2] = 0x00;
			pMsgContext->resData[3] = FMemLibLF_GetNumberOfMaskedDtc((unsigned char)DtcSearchMask, FAULT_MEMORY);

			/* Data length */
			*DataLen = 4;

			/* Set information for Request to send */
			*Response = RESPONSE_OK;

			//Update the Historical stack
			//FMemLibLF_UpdateHistoricalInterrogationRecord();

			break;
		}
		
		case 0x08: // ReportDtcBySeverityMaskRecord 
		{
			// Mask search pMsgContext->reqData[0] is the first byte received for severity mask.
			//We do not have specific requirements for severity, just filling the bytes information with
			//service 02 and filling 08 service with dummy bytes to satisfy the service.
			DtcSearchMask = pMsgContext->reqData[1];
			DtcSearchMask &= FMEM_DTC_AVAILABILITY_MASK_K;

			// Get the No of DTCs Registered in Chrono
			NumberOfRegisteredDtc = (temp_l)FMemLibLF_GetNumberOfRegisteredDtc(FAULT_MEMORY);

			/* If the received content is larger than max bufferlenght of 200 bytes? */
			if (((FMemLibLF_GetNumberOfMaskedDtc((unsigned char)DtcSearchMask, FAULT_MEMORY)*4)+2) < kDescPrimBufferLen)
			{
				//Fill the first byte .. DTC Status Availablity Mask
				pMsgContext->resData[0] = FMEM_DTC_AVAILABILITY_MASK_K;
				
				if (DtcSearchMask)
				{
					/* Go through all the registered errors */
					for (DtcMemoryIndex = 0; DtcMemoryIndex < NumberOfRegisteredDtc; DtcMemoryIndex++)
					{
						if (d_AllDtc_a[d_DtcMemory.elem[DtcMemoryIndex].dtc_RomIndex].DtcState.all & DtcSearchMask)
						{
							pMsgContext->resData[(NumberOfMaskedDtc*6)+1] = 0; //No severity defined for this program
							pMsgContext->resData[(NumberOfMaskedDtc*6)+2] = 0; //No functionality defined for this program
							pMsgContext->resData[(NumberOfMaskedDtc*6)+3] = FMemLibLF_GetByteOfDtc(d_DtcMemory.elem[DtcMemoryIndex].dtc_RomIndex, 1);
							pMsgContext->resData[(NumberOfMaskedDtc*6)+4] = FMemLibLF_GetByteOfDtc(d_DtcMemory.elem[DtcMemoryIndex].dtc_RomIndex, 2);
							pMsgContext->resData[(NumberOfMaskedDtc*6)+5] = FMemLibLF_GetByteOfDtc(d_DtcMemory.elem[DtcMemoryIndex].dtc_RomIndex, 3);

							// Get the Error Status
							pMsgContext->resData[(NumberOfMaskedDtc*6)+6] = d_AllDtc_a[d_DtcMemory.elem[DtcMemoryIndex].dtc_RomIndex].DtcState.all;

							NumberOfMaskedDtc++;
						}
					}
					//Total Response length
					*DataLen = ((u_temp_l)NumberOfMaskedDtc*6)+1;
				}
				else
				{
					*DataLen = 1;
				}

				/* Set the response flag */
				*Response = RESPONSE_OK;
			}
			else
			{
				*Response = REQUEST_OUT_OF_RANGE;
			}
			
			break;
		}
		
		case 0x09: // ReportSeverityInformationOfDTC 
		{
			DtcStructType    TempDtcCode;
			TempDtcCode.all = 0;

			// Get the DTC Code from the request buffer..Harsha Re aranged the buffer
			TempDtcCode.byte[1] = pMsgContext->reqData[0];
			TempDtcCode.byte[2] = pMsgContext->reqData[1];
			TempDtcCode.byte[3] = pMsgContext->reqData[2];
			
			pMsgContext->resData[0] = FMEM_DTC_AVAILABILITY_MASK_K;
			

			/* Is the requested error exists in list?..Harsha added && condition */
			if ((Index = FMemLibF_CheckDtcSupportOverDtc(TempDtcCode.all)) >= 0 &&
			   (Index = FMemLibF_CheckDtcSupportOverDtc(TempDtcCode.all)) < SIZEOF_ALL_SUPPORTED_DTC) {

				pMsgContext->resData[1] = 0; //Severity not defined for this program 
			
				pMsgContext->resData[2] = 0; /* DTC Functional Unit Not defined for this program */
				/* Fill the DTC code in response buffer */
				pMsgContext->resData[3] = TempDtcCode.byte[1];
				pMsgContext->resData[4] = TempDtcCode.byte[2];
				pMsgContext->resData[5] = TempDtcCode.byte[3];
				
				/* Fill the DTC status */
				pMsgContext->resData[6] = d_AllDtc_a[Index].DtcState.all;
				
				*Response = RESPONSE_OK;
				*DataLen = 7;
			}
			else
			{
				*Response = REQUEST_OUT_OF_RANGE;
			}
			break;
		}
		
		/* Case 0x0C Report First Confirmed DTC */
		case 0x0C: // Report First Confirmed DTC 
		{
			unsigned char temp_dtc_index = 0;
			EE_BlockRead(EE_FIRST_CONF_DTC, &temp_dtc_index);
			
			if(temp_dtc_index >= 0 && temp_dtc_index <= SIZEOF_ALL_SUPPORTED_DTC)
			{
				//Fill the first byte .. DTC Status Availablity Mask
				pMsgContext->resData[0] = FMEM_DTC_AVAILABILITY_MASK_K;

				pMsgContext->resData[1] = FMemLibLF_GetByteOfDtc(temp_dtc_index, 1);
				pMsgContext->resData[2] = FMemLibLF_GetByteOfDtc(temp_dtc_index, 2);
				pMsgContext->resData[3] = FMemLibLF_GetByteOfDtc(temp_dtc_index, 3);
				/* Fill the DTC status */
				pMsgContext->resData[4] = d_AllDtc_a[temp_dtc_index].DtcState.all;
				
				*DataLen = 5;
				*Response = RESPONSE_OK;
				
			}
			else
			{
				*Response = RESPONSE_OK;
			}
			break;
			
//			if ( (DtcMemoryIndex = FMemLibLF_SearchDtcIndexInDtcMemory(temp_dtc_index, FAULT_MEMORY)) >= 0)
//			{
//				/* Fill the DTC status */
//				pMsgContext->resData[0] = d_AllDtc_a[temp_dtc_index].DtcState.all;
//
//				pMsgContext->resData[1] = FMemLibLF_GetByteOfDtc(temp_dtc_index, 1);
//				pMsgContext->resData[2] = FMemLibLF_GetByteOfDtc(temp_dtc_index, 2);
//				pMsgContext->resData[3] = FMemLibLF_GetByteOfDtc(temp_dtc_index, 3);
//				
//				*DataLen = 4;
//				*Response = RESPONSE_OK;
//			}
//			else
//			{
//				*Response = REQUEST_OUT_OF_RANGE;
//			}
//			
//			
//			break;
		}
		
		/* Case 0x0E Report Most recent Confirmed DTC */
		case 0x0E: // Report Most recent Confirmed DTC 
		{
			unsigned char temp_dtc_index = 0;
			EE_BlockRead(EE_RECENT_CONF_DTC, &temp_dtc_index);
			
			if(temp_dtc_index >= 0 && temp_dtc_index <= SIZEOF_ALL_SUPPORTED_DTC)
			{
				//Fill the first byte .. DTC Status Availablity Mask
				pMsgContext->resData[0] = FMEM_DTC_AVAILABILITY_MASK_K;

				pMsgContext->resData[1] = FMemLibLF_GetByteOfDtc(temp_dtc_index, 1);
				pMsgContext->resData[2] = FMemLibLF_GetByteOfDtc(temp_dtc_index, 2);
				pMsgContext->resData[3] = FMemLibLF_GetByteOfDtc(temp_dtc_index, 3);
				/* Fill the DTC status */
				pMsgContext->resData[4] = d_AllDtc_a[temp_dtc_index].DtcState.all;
				
				*DataLen = 5;
				*Response = RESPONSE_OK;
				
			}
			else
			{
				*Response = RESPONSE_OK;
			}
			break;
			
//			if ( (DtcMemoryIndex = FMemLibLF_SearchDtcIndexInDtcMemory(temp_dtc_index, FAULT_MEMORY)) >= 0)
//			{
//				
//				/* Fill the DTC status */
//				pMsgContext->resData[0] = d_AllDtc_a[temp_dtc_index].DtcState.all;
//
//				pMsgContext->resData[1] = FMemLibLF_GetByteOfDtc(temp_dtc_index, 1);
//				pMsgContext->resData[2] = FMemLibLF_GetByteOfDtc(temp_dtc_index, 2);
//				pMsgContext->resData[3] = FMemLibLF_GetByteOfDtc(temp_dtc_index, 3);
//				
//				*DataLen = 4;
//				*Response = RESPONSE_OK;
//			}
//			else
//			{
//				*Response = REQUEST_OUT_OF_RANGE;
//			}
//			
//			break;
		}
		
		default :
		{
			//Set information for Request to send
			*Response = SUBFUNCTION_NOT_SUPPORTED;
		}
	}
}

void FMemLibF_ReportDtc(unsigned long DtcCode, enum ERROR_STATES State)
{
	//Application module announces error status ex:fault matures, de-matures etc..
	//unsigned long DtcCode: DTCcode
	//enum ERROR_STATES State: Status of particular DTC code matured/de-matured

	/* temp. Variable */
	//temp_l Index, ReportEnable = 0;
	//unsigned char Index, ReportEnable = 0;
	temp_l Index;
	unsigned char ReportEnable = 0;

	// Enable criteria to update the ERROR_STATES
//	ReportEnable = 1;

//	if (ReportEnable)
//	{
		if ((Index = FMemLibF_CheckDtcSupportOverDtc(DtcCode)) >= 0)
		{
			d_AllDtc_a[Index].reported_state = State;
		}
//	}
}

/* FMemLibF_CheckDtcConditionTimer	*/
/* 									*/
/* This function makes sure that CSWM is Ok with the section 9.3.3.4 in DC-10746 (Rev C).                                                                                                                                                                                                                   */
/*                                                                                                                                                                                                                                                                  */
/* The Ignition Cycle Counter is defined as a counter for the occurrence of a transition from ignition "off" to ignition "run" precluding all transitions through ignition "start/crank" mode including a de-bounce time ending in ignition "run". Following are the steps which compose an ignition cycle: */
/* 1.    Ignition off                                                                                                                                                                                                                                                                 */
/* 2.    Ignition run                                                                                                                                                                                                                                                                 */
/* 3.    10 second de-bounce after ignition run transition                                                                                                                                                                                                                                                  */
/* Ignition cycle counters are used to:                                                                                                                                                                                                                                                                 */
/* -    Indicate the number of consecutive ignition cycles recorded since a DTC first became "stored - not active" and is no longer an "active" DTC.                                                                                                                                                        */
/* -    Self-heal a specific fault record (see section 9.4.1.2C.2 for more details).                                                                                                                                                                                                                        */
/* DPRS 9.3-31*    Each ECU shall support a unique ignition cycle counter for each fault record.                                                                                                                                                                                                            */
/* DPRS 9.3-32*    Each ECU shall use a ten (10) second timer used to increment each ignition cycle counter.                                                                                                                                                                                                */
/* DPRS 9.3-44    Each ECU shall start the ten (10) second timer as soon as the "Ignition off" to "ignition run" transition has occurred.                                                                                                                                                                   */
/* NOTE -     This implies that the timer is not interrupted while the ignition state equals "start/crank".                                                                                                                                                                                                 */
/* DPRS 9.3-33*    Each ECU shall not increment the ignition cycle counter more than once per ignition cycle.                                                                                                                                                                                               */
/* DPRS 9.3-34*    The ignition cycle counter value shall initially be set to zero (0) at the first instance of an "active" DTC.                                                                                                                                                                            */
/* DPRS 9.3-35*    The ignition cycle counter value shall be reset to zero (0) if the DTC status transitions back from "stored - not active" to "active".                                                                                                                                                   */
/* DPRS 9.3-36*    The ignition cycle counter value shall be set to one (1) when the DTC first transitions to a "stored - not active" state.                                                                                                                                                                */
/* DPRS 9.3-37*    The ignition cycle counter value shall be incremented by one (1) at the end of each consecutive ignition cycle (if the 10 second timer has been met before) that the DTC is "stored - not active".                                                                                       */
/* DPRS 9.3-38*    If the ignition cycle counter value reaches its maximum value (255) and the DTC still remains "stored - not active", the value shall remain latched at 255.                                                                                                                              */
/* NOTE -     The ignition cycle counter may only exceed 100 when the DTC has a priority of 1, which means that the DTC cannot be self-healed, otherwise the DTC will be self-healed at 100 ignition cycles (refer to section for 9.4.1.2C.2 further details).                                              */
/* DPRS 9.3-39*    The ignition cycle counter value shall be retained between power cycles.                                                                                                                                                                                                                 */
/* 								                                                                                              */
/* 								                                                                                              */
/* This function works as follows.                                                                                            */
/* Called in every 320 ms time slice.                                                                                         */
/* When program starts first time, It starts 10sec de-bounce timer after IGN RUN Transition.                                  */
/* After timer reaches 10sec, calls the IncOperationCyceCounter function and enables the Historical Interrogation record bit. */
/*                                                                                                                            */
/* This function was modified from actual Library function.                                                                   */
/* Modified Section:                                                                                                          */
/* Initial state changed from OPERATION_CYLE_OFF to OPERATION_CYLE_ON.                                                        */
/* By doing this we make sure that every time module runs we wait 10sec before updating the Opeartion cycle counter.          */
/* With actual logic, we never execute this in actual vehicle line. CSWM is out of Bus if IGN is other than RUN.              */
/* 								                                                                                              */
@far void FMemLibF_CheckDtcConditionTimer(void)
{
	//static enum OPERATION_CYCLE d_OperationCycle_t = OPERATION_CYLE_OFF; //Harsha Commented. In actual vehicle, this logic never works.
	//static enum OPERATION_CYCLE d_OperationCycle_t = OPERATION_CYLE_ON; //Moved to Init function
	static unsigned char d_OperationCycle10secTimer_c = 0;
	unsigned char IgnitionState;

#define D_10_SEC_K        31   //1000 if calls in 10ms, 31 if calls in 320ms time slice

	IgnitionState = FMemLibF_GetIgnition();

	switch (d_OperationCycle_c) {
		case OPERATION_CYLE_OFF:
		{
			/* Initially when program starts clear the 10sec timer */
			// Timer reset
			d_OperationCycle10secTimer_c = 0;

			if( (IgnitionState != IGN_RUN) && (IgnitionState != IGN_START) )
			{
				// Update to Historical Interogation record not set
				d_HistInterrogRecordEnable_t = FALSE; 
				// set the state ON
				d_OperationCycle_c = OPERATION_CYLE_ON;
			}
			break;
		}
		case OPERATION_CYLE_ON:
		{
			if (IgnitionState == IGN_RUN)
			{
				// Start Incrementing the 10sec timer
				d_OperationCycle10secTimer_c = FMemLibLF_IncCounter(d_OperationCycle10secTimer_c, D_10_SEC_K);
			}
			else {
				// Timer reset
				d_OperationCycle10secTimer_c = 0;
			}
			/* Is 10sec time achieved? */
			if (d_OperationCycle10secTimer_c == D_10_SEC_K)
			{
				
				fmem_keyon_counter_w++; //Update the value of IGN Cycles of ECU Life time
				EE_BlockWrite(EE_ECU_KEYON_COUNTER, (u_8Bit*)&fmem_keyon_counter_w); //Write the latest value of IGN Cycles of ECU Life time
				
				EE_BlockWrite(EE_ODOMETER, (UINT8*)&canio_RX_TotalOdo_c[0]); //$2001 service
				
				/* Time to decrement the Operational cycle counter, If DTC dematures */
				FMemLibLF_DecDtcOperationCyclCounter();
				// Update to Historical Interogation record to set
				d_HistInterrogRecordEnable_t = TRUE;
				// Statewechsel
				d_OperationCycle_c = OPERATION_CYLE_OFF;
			}
			break;
		}
		default :
		{
			// default
			d_OperationCycle_c = OPERATION_CYLE_OFF;
		}
	}
	if (IgnitionState == IGN_START)
	{
		d_IgnitionStart5secTimer_c = D_5_SEC_K;
	}
	else
	{
		if (d_IgnitionStart5secTimer_c > 0)
		{
			d_IgnitionStart5secTimer_c--;
		}
	}
	
}

temp_l FMemLibF_CheckDtcSupportOverDtc(DtcType DtcCode)
//s_8Bit FMemLibF_CheckDtcSupportOverDtc(DtcType DtcCode)
{
	//u_temp_l Index;
	temp_l Index;

	for (Index = 0; Index < SIZEOF_ALL_SUPPORTED_DTC; Index++)
	{
		if (fmem_AllRegisteredDtc_t[Index].dtc_code.all == DtcCode)
		{
			/* Commented the checking.. We are supporting all the DTCs. No need to check with this function,.Harsha */
//			if (/*FMemLibLF_CheckDtcSupportOverIndex(Index)*/1)
//			{
				return Index;
//			}
//			else
//			{
//				return -1;
//			}
		}
	}
	//Could not find any information. Send back nothing found.
	return -1;

}

u_temp_l FMemLibF_GetOdometer(void)
{
	
	return canio_RX_TravelDistance_w;
}

unsigned char FMemLibF_GetIgnition(void)
{
	
	return canio_RX_IGN_STATUS_c;
}

u_temp_l FMemLibF_IsOdometerValid(unsigned short Odometer)
{
	if (Odometer != CANBUS_KILOMETER_INVALID_K)
	{
		
		return 1;
	}
	else
	{
		
		return 0;
	}
}

void FMemLibLF_ClearSingleDtc(unsigned char DtcIndex)
{
	d_AllDtc_a[DtcIndex].dtc_on_time = 0x00;
	d_AllDtc_a[DtcIndex].DtcState.all = 0x00;
	d_AllDtc_a[DtcIndex].reported_state = ERROR_NO_CHECK_K;
//	d_AllDtc_a[DtcIndex].DtcState.state.TestNotCompletedSinceLastClear = TRUE; //CUSW.. Not Needed
}

/* FMemLibLF_GetByteOfDtc	*/
/* The byte of the error code supplies at the place DtcIndex in the Rom array	*/
/* unsigned char DtcIndex: Position of the error in the Rom array				*/
/* unsigned char byte: 3 bytes of error code									*/ 
unsigned char FMemLibLF_GetByteOfDtc(unsigned char DtcIndex, unsigned char Byte)
{
	
	return fmem_AllRegisteredDtc_t[DtcIndex].dtc_code.byte[Byte];
}

/* FMemLibLF_GetNumberOfMaskedDtc	*/
/* Returns the number of errors present in either Chrono or Historical stacks based on the search mask. */
unsigned char FMemLibLF_GetNumberOfMaskedDtc(unsigned char DtcSearchMask, enum FAULTMEMORY_TYP FaultMemoryTyp)
{
	#define EMPTY_SECTION  0xFF
	
  unsigned char Index, NumberOfRegisteredDtc, NumberOfMaskedDtc = 0;

	switch (FaultMemoryTyp) {
		case FAULT_MEMORY:
		{
			for (Index = 0; Index < SIZEOF_ALL_SUPPORTED_DTC; Index++)
			{
				if (d_AllDtc_a[Index].DtcState.all & DtcSearchMask)
				{
					NumberOfMaskedDtc++;
				}
			}
			break;
		}
		case HISTORICAL_MEMORY:
		{
			NumberOfRegisteredDtc = (unsigned char)FMemLibLF_GetNumberOfRegisteredDtc(HISTORICAL_MEMORY);
			for (Index = 0; Index < NumberOfRegisteredDtc; Index++)
			{
//				if (d_DtcHistoricalMemory.elem[Index].DtcState.all & DtcSearchMask)
				if(d_DtcHistoricalMemory.elem[Index].dtc_RomIndex !=  EMPTY_SECTION)
        {
					NumberOfMaskedDtc++;
				}
			}
			break;
		}
	}
	
	return NumberOfMaskedDtc;
}

/* FMemLibLF_GetNumberOfRegisteredDtc	*/
/* Returns no of errors in memory based on the Fault memory type input	*/
u_temp_l FMemLibLF_GetNumberOfRegisteredDtc(enum FAULTMEMORY_TYP FaultMemoryTyp)
{
	u_temp_l Number = 0;

	switch (FaultMemoryTyp) {
		case FAULT_MEMORY:
		{
			while ((d_DtcMemory.elem[Number].dtc_RomIndex != 0xFF) && (Number < (SIZEOF_DTC_MEMORY) ) )
			{
				Number++;
			}
			break;
		}
		case HISTORICAL_MEMORY:
		{
			while ((d_DtcHistoricalMemory.elem[Number].dtc_RomIndex != 0xFF) && (Number < (SIZEOF_DTC_HISTORICAL_MEMORY)))
			{
				Number++;
			}
			break;
		}
	}
	
	return Number;
}

/* FMemLibLF_DecDtcOperationCyclCounter	*/
/* This function gets called from "FMemLibF_CheckDtcConditionTimer".                                                                                                                                                  */
/* FMemLibF_CheckDtcConditionTimer waits 10sec after noticing the IGN_RUN state for the first time and then calls this function.                                                                                      */
/*                                                                                                                                                                                                                    */
/* This function actually checks are there any DTCs present in Stack.                                                                                                                                                 */
/* If present, it updates the IGN Cycle counter only for the present DTCs with status stored.                                                                                                                         */
/*                                                                                                                                                                                                                    */
/* And updates this information in Shadaow RAM.                                                                                                                                                                       */
/*                                                                                                                                                                                                                    */
/* DPRS 9.3-37*    The ignition cycle counter value shall be incremented by one (1) at the end of each consecutive ignition cycle (if the 10 second timer has been met before) that the DTC is "stored - not active". */
/*                                                                                                                                                                                                                    */
void FMemLibLF_DecDtcOperationCyclCounter(void)
{
	/* temp. Variable */
	unsigned char Index, NumberOfRegisteredDtc;

	NumberOfRegisteredDtc = (unsigned char)FMemLibLF_GetNumberOfRegisteredDtc(FAULT_MEMORY);

	for (Index = 0; Index < NumberOfRegisteredDtc; Index++)
	{
		
		//Changed logic: Now it checks the actual status from RAM and see if the status gets changed to stored.
		//If stored then Decrements the operation cycle counter from 40 to 39,38...0.
		if ( (d_AllDtc_a[d_DtcMemory.elem[Index].dtc_RomIndex].DtcState.all & FMEM_DTC_AVAILABILITY_MASK_K ) == FMEM_DTC_STORED_NOT_ACTIVE_MASK_K )
		{
			d_DtcMemory.elem[Index].operation_cycle_counter = FMemLibLF_DecCounter(d_DtcMemory.elem[Index].operation_cycle_counter, D_SELF_HEALING_CRITERION_K);
			/* Write to Shadow RAM */
			update_chrono_stack_b = TRUE;   
		}

	}
}

/* FMemLibLF_SearchDtcIndexInDtcMemory	*/
/* This function returns the index from Stacks, if the searched DTC is present. Based on the request call, it checks either Chronostack or historical stack */
/* IF not found it returns -1.                                                                                                                              */
/* Searches whether DTC present or not in both the fault memories based on the index					                                                    */
/* Returns index (Index from the stack)if found ,else -1.												                                                    */
temp_l FMemLibLF_SearchDtcIndexInDtcMemory(unsigned char DtcIndex, enum FAULTMEMORY_TYP FaultMemoryTyp)
{
    temp_l Indexvalue;
	u_temp_l Index;

	switch (FaultMemoryTyp) {
		case FAULT_MEMORY:
		{
			for (Index = 0; Index < SIZEOF_DTC_MEMORY; Index++)
			{
				if (d_DtcMemory.elem[Index].dtc_RomIndex == DtcIndex)
				{
					//dtc index stands in the error memory in the place
					return Index;
					//Indexvalue = /*(temp_l)*/Index;
				}
				else
				{
					//DTC not yet found
					if (Index == (SIZEOF_DTC_MEMORY-1))
					{
						return -1;
						//Indexvalue = -1;
					}
					else
					{
						//DTC not yet found
					}
				}
			}
			break;
		}
		case HISTORICAL_MEMORY:
		{
			for (Index = 0; Index < SIZEOF_DTC_HISTORICAL_MEMORY; Index++)
			{
				if (d_DtcHistoricalMemory.elem[Index].dtc_RomIndex == DtcIndex)
				{
					//dtc index stands in the shade error memory in the place
					return Index;
					//Indexvalue = (temp_l)Index;
				}
				else
				{
					//DTC not yet found
					if (Index == (SIZEOF_DTC_MEMORY-1))
					{
						return -1;
						//Indexvalue = -1;
					}
					else
					{
						//DTC not yet found
					}
				}
			}
			break;
		}
	}
	return (Indexvalue);
}

static temp_l FMemLibLF_SearchDtcIndexWithLowerPriority(u_temp_l Priority)
{
	/* temp. Variable */
	temp_l Index, IndexMerker = 0, LowestPriority = 0;

	for (Index = 0; Index < SIZEOF_DTC_MEMORY; Index++) {
		if (fmem_AllRegisteredDtc_t[d_DtcMemory.elem[Index].dtc_RomIndex].dtc_priority >= LowestPriority)
		{
			/* Harsha added If environment */
			if (d_AllDtc_a[d_DtcMemory.elem[Index].dtc_RomIndex].DtcState.state.TestFailed == FALSE)
			{
				/* Get the lowest priority from stack */
				LowestPriority = fmem_AllRegisteredDtc_t[d_DtcMemory.elem[Index].dtc_RomIndex].dtc_priority;
				IndexMerker = Index;
			}
		}
	}
	if (LowestPriority >= (temp_l)Priority)
	{

		return IndexMerker;
	}
	else
	{
		/* No dtc found */
		return -1;
	}
}

/* FMemLibLF_TransferFaultToHistoricalStack. USED FOR SECOND SNAPSHOT RECORD.*/
/* MKS 74407 */
/* This Function gets called when ever there is a DTC Present aas First snapshot record and again matures */
/* Stores the latest ECU Life time, ECU time form Key ON, Key ON counters */
/* Start index: Position, starting from which index to transfer					                                                                                                                                          */
/* Number: No of errors to transfer												                                                                                                                                          */
void FMemLibLF_TransferFaultToHistoricalStack(temp_l StartIndex, temp_l Number)
{
#ifdef JEDEN_FEHLER_UEBERNEHMEN
	u_temp_l Index;

	for (Index = 0; Index < number; Index++)
	{
		/* complete shaded memory around one downward shift */
		fmemLF_MemMove((unsigned char*)&d_DtcHistoricalMemory.elem[1], (unsigned char*)&d_DtcHistoricalMemory.elem[0], (sizeof(struct DTC_HISTORICAL_MEMORY_ELEMENT)*(SIZEOF_DTC_HISTORICAL_MEMORY-1)));
	}
	for (Index = startindex; Index < startindex+number; Index++)
	{
		//Transfer the error to shaded memory
		d_DtcHistoricalMemory.elem[Index-startindex].original_odometer = d_DtcMemory.elem[Index].original_odometer;
		d_DtcHistoricalMemory.elem[Index-startindex].DtcState.all = d_AllDtc_a[d_DtcMemory.elem[Index].dtc_RomIndex].DtcState.all;
		d_DtcHistoricalMemory.elem[Index-startindex].dtc_RomIndex = d_DtcMemory.elem[Index].dtc_RomIndex;
	}
#else
	temp_l Index, DtcHistoricalMemoryIndex;
	//unsigned char Index;
	struct DTC_HISTORICAL_MEMORY_ELEMENT    TempDtcHistMemoryElement;

	for (Index = StartIndex+Number-1; Index >= StartIndex; Index--)
	{
		/* is error already in the shaded memory? */
		if ((DtcHistoricalMemoryIndex = FMemLibLF_SearchDtcIndexInDtcMemory(d_DtcMemory.elem[Index].dtc_RomIndex, HISTORICAL_MEMORY)) >= 0)
		{
			/* As we noticed that the Index is already present, do nothing for the matched index. */
			/* For Chrysler: Here the code should be Nothing */
			/* For FIAT: Every time the DTC matures (after 2nd snapshot), here is the place to update the info */
			d_DtcHistoricalMemory.elem[DtcHistoricalMemoryIndex].ecu_lifetime = fmem_lifetime_l;
			d_DtcHistoricalMemory.elem[DtcHistoricalMemoryIndex].ecu_time_keyon = fmem_keyon_time_w;
			d_DtcHistoricalMemory.elem[DtcHistoricalMemoryIndex].key_on_counter = fmem_keyon_counter_w;
			
			fmemLF_MemCpy((unsigned char*)&TempDtcHistMemoryElement, (unsigned char*)&d_DtcHistoricalMemory.elem[DtcHistoricalMemoryIndex], sizeof(struct DTC_HISTORICAL_MEMORY_ELEMENT));
			
			fmemLF_MemMove((unsigned char*)&d_DtcHistoricalMemory.elem[1], (unsigned char*)&d_DtcHistoricalMemory.elem[0], (sizeof(struct DTC_HISTORICAL_MEMORY_ELEMENT)* DtcHistoricalMemoryIndex));
			
			fmemLF_MemCpy((unsigned char*)&d_DtcHistoricalMemory, (unsigned char*)&TempDtcHistMemoryElement, sizeof(struct DTC_HISTORICAL_MEMORY_ELEMENT));
			
			/* Write to Shadow RAM.. Only when there is a real case for updating the Historical Stack. */
			update_historical_stack_b = TRUE;
		}
		else
		{
			//2nd snapshot data update.
			//complete shaded memory around one downward shift
			fmemLF_MemMove((unsigned char*)&d_DtcHistoricalMemory.elem[1], (unsigned char*)&d_DtcHistoricalMemory.elem[0], (sizeof(struct DTC_HISTORICAL_MEMORY_ELEMENT)*(SIZEOF_DTC_HISTORICAL_MEMORY-1)));

			d_DtcHistoricalMemory.elem[0].ecu_lifetime = fmem_lifetime_l;
			d_DtcHistoricalMemory.elem[0].ecu_time_keyon = fmem_keyon_time_w;
			d_DtcHistoricalMemory.elem[0].key_on_counter = fmem_keyon_counter_w;
            d_DtcHistoricalMemory.elem[0].dtc_RomIndex = d_DtcMemory.elem[Index].dtc_RomIndex;

			/* Write to Shadow RAM.. Only when there is a real case for updating the Historical Stack. */
			update_historical_stack_b = TRUE;
			
		}
	}
#endif
}


//u_temp_l FMemLibLF_IncCounter(u_temp_l Counter, u_temp_l MaxValue)
unsigned char FMemLibLF_IncCounter(u_temp_l Counter, u_temp_l MaxValue)
{
	if (Counter < MaxValue)
	{
		Counter++;
		
	}
	
	return (unsigned char)Counter;
}

/* This function will get updated when ever there is a change to DTC status                        */
/* Logic will check for the flags which are set at appropriate times to process the EEPROM updates */

void FMemLibF_UpdatestoEEPROM(void)
{
	switch (fault_update_case_c)
	{
		case 0:
		{	
			/* Time to update the DTC Status Info? */
			if (update_dtc_status_info_b)
			{
				EE_BlockWrite(EE_DTC_STATUS, (u_8Bit*)&d_AllDtc_a);
				/* Updates to DTC Status done for Shadow */
				update_dtc_status_info_b = FALSE;
			}
			//Updated the DTC status, next time update Chronostack
			fault_update_case_c++;
			break;
		}
		case 1:
		{	
			/* Time to Update the chronostack? */
			if (update_chrono_stack_b)
			{
				EE_BlockWrite(EE_CHRONO_STACK, (u_8Bit*)&d_DtcMemory);
				/* Chrono Stack Updated in Shadow. */
				update_chrono_stack_b = FALSE;
			}
			//Updated the Chronostack, next time update Historical stack
			fault_update_case_c++;
			break;
		}
		case 2:
		{	
			/* Time to Update the Historical Stack? */
			if (update_historical_stack_b)
			{
				EE_BlockWrite(EE_HISTORICAL_STACK, (u_8Bit*)&d_DtcHistoricalMemory);
				/* Historical Stack Updated in Shadow. */
				update_historical_stack_b = FALSE;
			}
			//Updated the Historical stack, next time update Historical Interrogation record
			fault_update_case_c = 0;
			break;
		}
		
		default:
		{
			//Completed all the tasks. Go for another loop from beginning.
			fault_update_case_c = 0;
		}
		
	}
}

/**************************************************************************************************
 * Function Name:		FMemLibF_ECUTimeStamps
 * 
 * Called In:			FMemLibF_CyclicDtcTask
 * 
 * Calls To:			EE_BlockRead
 * 						EE_BlockWrite
 * 
 * Input:				ECU Lifetime from the EEPROM
 * 
 * Output:				ECU Lifetime to RAM and EEPROM
 * 						ECU time from Key ON to RAM and EEPROM.
 * 						 
 * Description:			This function updates for the the Engineering data (ECU Life time and ECU KEY ON)
 * 						RAM UPDATE: 
 * 						Every 1min, ECU TIME STAMPS will get incremented in RAM (Life time of ECU)
 * 						Every 15sec, ECU TIME from KEY ON will get incremented in RAM (Time for that IGN)
 * 						EEPROM UPDATE: (e-mail dated 03/24/2011)
 * 						Every 60sec (1 min):When Accumulation timer range of 0- 1800 Sec(0 - 30min)
 * 						Every 300sec(5 min):When Accumulation timer range of 1801 - 3600 sec (31 - 60 min)
 * 						Every 600sec(10 min):When Accumulation timer range of 3601 - Max sec (61 - max min)
 ***************************************************************************************************/
@far void FMemLibF_ECUTimeStamps(void)
{
	union long_char  temp_lifetime_l;  //ECU Life time in 1 min increments           
	EE_BlockRead(EE_ECU_TIMESTAMPS, (u_8Bit*)&temp_lifetime_l); //Read the stored value of life time from EEPROM

	/* Check for 15 sec timer */
	/* If reaches 15 sec, increment the ECU key on counter, and clear the 15 sec counter */
	/* Else increment the 15 seccounter */
	if (ecu_keyon_counter_c >= FIFTEEN_SEC_TIMER)
	{
		fmem_keyon_time_w ++; //Increment the 15sec KEY ON counter. Variable used to store info into EEPROM
		ecu_keyon_counter_c = CLEAR_COUNTER;
	}
	else
	{
		ecu_keyon_counter_c++; //Not reached 15sec. Increment.
	}
	
	/* Check for one minute timer */
	/* If reaches 1 min, increment the ECU Life time, 5 min counter and clear the 1 min counter */
	/* Else increment the 1 min counter */
	if(ecu_1min_counter_c >= ONE_MINUTE_TIMER)
	{
		
		fmem_lifetime_l.l++; //Increment the ECU Life time counter. Variable used to store info into EEPROM
		ecu_1min_counter_c = CLEAR_COUNTER; //Clear 1min counter, so next iteration it starts fresh
		//ecu_life_counter_c ++; //Increment 5 min counter
		
		if(temp_lifetime_l.l < 30)
		{
			EE_BlockWrite(EE_ECU_TIMESTAMPS, (u_8Bit*)&fmem_lifetime_l);
			EE_BlockWrite(EE_ECU_TIME_KEYON, (u_8Bit*)&fmem_keyon_time_w);
		}
		else 
		{
			ecu_life_counter_c ++; //Increment 5 min counter
		}
		
		if((temp_lifetime_l.l >= 30 && temp_lifetime_l.l < 60))
		{
			if(ecu_life_counter_c == FIVE_MINUTE_COUNTER)
			{
				EE_BlockWrite(EE_ECU_TIMESTAMPS, (u_8Bit*)&fmem_lifetime_l);
				EE_BlockWrite(EE_ECU_TIME_KEYON, (u_8Bit*)&fmem_keyon_time_w);
				ecu_life_counter_c = CLEAR_COUNTER;
			}
		}
		else
		{
			if(ecu_life_counter_c == TEN_MINUTE_COUNTER)
			{
				EE_BlockWrite(EE_ECU_TIMESTAMPS, (u_8Bit*)&fmem_lifetime_l);
				EE_BlockWrite(EE_ECU_TIME_KEYON, (u_8Bit*)&fmem_keyon_time_w);
				ecu_life_counter_c = CLEAR_COUNTER;
			}
		}
		
	}
	else
	{
		ecu_1min_counter_c++; //Not reached 1min, increment.
	}
	
}

/* This function works for Event Counter. Self Healing */
/* When the first time DTC state comes to stored from Active, the initial value set to 40 */
/* For subsequent IGN cycles If fault not set again, this value gets decremented by 1 */
/* When Event counter reaches 0, the DTC in Chrono stack gets deleted */
unsigned char FMemLibLF_DecCounter(u_temp_l Counter, u_temp_l MaxValue)
{
	if ((Counter == MaxValue) || (Counter != 0x0))
	{
		Counter--;
		
	}
	
	return (unsigned char)Counter;
}

/**************************************************************************************************
 * Function Name:		FMemLibF_NMFailSts
 * 
 * Called In:			Time Slice 320ms
 * 
 * Calls To:			None
 * 						
 * Input:				None
 * 
 * Output:				CurrentFailSts and GenericFailSts Information will be filled.
 * 						 
 * Description:			This function updates the NM Related data. GenericFailSts and CurrentFailSts
 * 						GENERIC FAIL STATUS: 
 * 						Set 1: As long as there is an error present with status either ACTIVE/STORED
 * 						Set 0: When there is no error Present 
 * 						CURRENT FAIL STATUS:
 * 						Set 1: As long as there is an ACTIVE ERROR present in stack						
 * 						Set 0: Other cases.						
 * 						
 ***************************************************************************************************/
@far void FMemLibF_NMFailSts(void)
{
	unsigned char i = 0;
	/* Initialize the values */
	
	switch (fmem_nm_status_c)
	{
	    /* GENERIC FAIL STATUS */
		case 0:
		{	
			fmem_genericFailSts = 0;
			
			/* Look for complete DTC Status flags for the supported DTC's*/
			for (i = 0; i < SIZEOF_ALL_SUPPORTED_DTC; i++)
			{
				/* Check for the confirmed flag. If set means Generic Failure case is TRUE */
				if (d_AllDtc_a[i].DtcState.state.Confirmed == TRUE)
				{
					/* Will be used in NWM Module to send the status on Bus */
					fmem_genericFailSts = TRUE;
					break;
				}
			}
			
			//Updated the Generic Fail Status, next time update Current Fail Status.
			fmem_nm_status_c++;
			break;
		}
		
		/* CURRENT FAIL STATUS */
		case 1:
		{	
			fmem_currentFailSts = 0;
			
			for (i = 0; i < SIZEOF_ALL_SUPPORTED_DTC; i++)
			{
				if (d_AllDtc_a[i].DtcState.state.TestFailed == TRUE)
				{
					/* Will be used in NWM Module to send the status on Bus */
					fmem_currentFailSts = TRUE;
					break;
				}
			}
			
			//Updated the Current Fail Status, next time update Generic Fail Status
			fmem_nm_status_c = 0;
			break;
		}
		
		default:
		{
			fmem_genericFailSts = 0;
			fmem_currentFailSts = 0;
			//Completed all the tasks. Go for another loop from beginning.
			fmem_nm_status_c = 0;
		}
		
	}
	
}

	
