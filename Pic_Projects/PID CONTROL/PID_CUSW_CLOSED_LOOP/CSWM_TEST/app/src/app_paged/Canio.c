/********************************************************************************************/
/*                   CHANGE HISTORY for CANIO MODULE                                        */
/********************************************************************************************/
/* mm/dd/yyyy User		-   Change comments										            */
/* 02-05-2008/Harsha	-   New Module created									            */
/* 02-15-2008/Harsha	-   MY09 Code added										            */
/* 02-11-2008/Harsha	-   HVAC signals Activated                                          */
/* 03-03-2008/Harsha	-   Vehicle Line Functionality activated                            */
/* 06-13-2008 Francisco	- 	LOC_ENABLE_5_SEC defined    						            */
/* 							canLF_LOCTimeout_Condition new logic				            */
/* 06-16-2008 Francisco	- 	canF_CCN_LOC_DTC updated   							            */
/*							canF_FCM_LOC_DTC updated							            */
/*							Diagnose.h included									            */
/* 							canLF_WriteOriginalVIN updated						            */
/*							canLF_LOCTimeout_Condition updated					            */
/*                          canio_Debounce_Time_c initialized in canF_Init					*/
/*                          canio_Debounce_Time_c reset in canLF_Veh_Line_Check_From_Bus	*/
/* 08-04-2008/Harsha    -   CSWM_PrplsnSysAtv_IndFunc added                                 */
/*                          CSWM_PrplsnSysAtv_TimeoutFunc Dummy function added              */
/*                          VMM 820 Files Generated                                         */
/* 09-08-2008/Harsha    -   At the initialization, switch requests defaulted to WK series   */
/* 10-10-2008/Harsha    -   LOC DTCs does not set. MKS Entry 15306                          */
/*                          FMemLibF_ReportDtc enabled in canF_CCN_LOC_DTC and              */
/*                                                        canF_FCM_LOC_DTC					*/
/* 11-11-2008/Harsha    -   canF_HVAC_LOC_DTC function added for "W" series vehicle lines   */
/* 11-18-2008/Harsha    -   PowerNet Ind functions enabled by switch cases                  */
/* 03-10-2009 CK        -   Included VEH_LD in vehicle line check in canF_Init and          */
/*                          canLF_VehicleInfo_Update functions. 							*/
/* 03-20-2009 CK        -   Added VEH_DX and VEH_DZ in vehicle line check in canF_Init and  */
/* 							canLF_VehicleInfo_Update functions. 							*/
/* 							Removed VEH_WH, VEH_WC, and VEH_RT since these vehicle lines    */
/* 							are not carring CSWM any more according to latest spec.         */
/* 04.14.2009 CK        -   New CCM version 915 updates.                                    */
/*                          Updated CSWM_PN14_LHC_Stat_IndFunc to get Batt_ST_Crit signal.  */
/*                      -   Added two indication functions CSWM_EC_ECUCfg5_Stat_IndFunc and */
/*                          CSWM_EC_Memory_IndFunc related to CSWM variant selection logic. */ 
/* 04.14.2009 CK        -   Added new PowerNet vehicle line 'JC' checks to canF_Init and    */
/*                          canLF_VehicleInfo_Update functions.                             */
/* 04.16.2009 Harsha    -   canLF_VehicleInfo_Update function updated to have Service parts */
/*                          Information for PN.                                             */
/*                      -   canF_CBC_LOC_DTC and canF_TGW_LOC_DTC functions added           */
/* 04.24.2009 CK        -   PC-Lint updates.												*/
/* 09.18.2009 Christian -   canF_CyclicTask(void).	MkS 35404  VIN Information			    */
/* 09.18.2009 Christian -  canLF_VehicleInfo_Update, canF_Init added CSWM_LR_HS_RQ_2_IndFunc  */
/*						   CSWM_RR_HS_RQ_2_IndFunc   FTR00139665 - New vehicle Line RT- RM	*/
/* 11.25.2009 Christian	-  canF_TGW_LOC_DTC FTR00139668 - Now If module receives            */ 
/*                         TGW_DISP_STAT = SNA blow TGW LOC									*/
/* 01.15.2010 Christian	-  canF_Init        FTR00141796 - MKS 42926 - after disabling       */ 
/*                         the Remote start feature RemSt_DCS flag is OFF in that IGN cycle.*/
/*                          Later when cycles the Ignition, RemSt_DCs comes up with default */
/*							value as ON.													*/
/* 03-31-2010 Harsha    -  MKS 49533 Issue fixed at canF_init function                      */
/* 04-14-2010 Harsha    -  MKS 50039 hs_Vehicle_Line variable assigned based on EEP vehicle */
/*                         Updated function is canF_Init                                    */
/* 05-28-2010 Harsha    -  MKS 51862: Default vehicle line information changed from WK to DS*/
/*                         Functions updated are: canF_Init and canLF_VehicleInfo_Update    */ 
/* 06-18-2010 Harsha    -  MKS 52741: Default vehicle line switch requests for Power Net    */
/*                         Now takes the L series information.                              */
/*                         Case 5 of canLF_VehicleInfo_Update got updated to reflect this   */ 
/* 08-10-2010 Harsha    -  MKS 55170: supported vehicle lines differentiated between        */
/*                         TIPM and PN architectures.                                       */
/*                         canF_init and canLF_VehicleInfo_Update() functions modifed       */
/* 10-06-2010 Harsha    -  MKS 57313: canF_init updated to read seat Configuration          */
/*                         decide Power Net configuration parameters based on Vehicle line  */
/*                         and Seat configuration.                                          */
/*                         canLF_VehicleInfo_Update function case 4 updated to have the     */
/*                         seat configuration stored in eeprom                              */
/* 01-10-2011 Christian -  MKS 61288: Because of new calibrations introduced to the program */
/*                         canio.c file also needs to be updated to the EEPROM with right   */
/*                         vehicle lines and seat configurations also.                      */
/*                         canF_Init and canLF_VehicleInfo_Update functions updated to have */
/*                         the new Implementation.											*/
/*04-01-2011 Christian  - MKS 68504 Powernet: Adjust loop timing for CFG_RQ BAF message.    */ 
/*                        New function was added canF_CyclicRemoteStart()                   */
/*04-01-2011 Christian  - MKS 68500 Add HSW to ECU Config 5. canLF_VehicleInfo_Update()     */   
/*04.04.2011 Christian  - MKS 68478 Add New Variant -Front and Rear Heated Seats + Front Vented Seats */     
/********************************************************************************************/

/********************************************************************************************
 *								CUSW UPDATES
 * ******************************************************************************************												
 * 09-28-2010 Harsha    -  #define PWR_NET removed.                                         
 *                         Main goal to have PWR_NET logic with ECUconfiguration            
 * 02-10-2011 C.Cortes  -  Added the CUSW FIAT signals access.				
 * 06-07-2011 Harsha        Implemented the VIN Logic.				
 * 06-14-2011 Harsha  75156 Implemented the timeout logic for the LOC DTC's.
 * 							Added the Indication flags to the receiving signals. If not received the
 * 							Ind flag, automatically timers will get incremented.
 * 							Functions updated are canF_TGW_LOC_DTC and canF_CBC_LOC_DTC
 * 06-15-2011 Harsha  75322	Cleanup the code. Delete the unused code for this program.
 * 08-08-2011 Harsha  81842	Battery voltage conversion made according to the new CAN Message to have
 * 							corresponding 100mv value. 
 * 							CSWM_BatVolt_ManIndFunc updated to relfact this.
 * 08-09-2011 Harsha  81838 PROXI Implementation logic. Updated functions are canF_Init. 
 * 							Newly added function is CSWM_CFG_DATA_RESP_ManIndFunc
 * 							Deleted function is canLF_VehicleInfo_Update,
 * 							canLF_Veh_Line_Check_From_Bus and canLF_ECU_Cfg5_Check_From_Bus	
 * 09-26-2011 Harsha  84977 New .dbc file updated with 1138.
 * 
 * 
 * 
 * 
 ********************************************************************************************/



/* This module basically looking for the neccessary messages received from CAN Bus.                                                                                                                                              */
/* Indication functions will gets called when ever the particular message got transmitted on Bus by other modules, so by getting the callback function, CSWM module knows the latest change to the message and stores the value. */
/* Timeout functions are callback functions gets called when ever the message timeout happens.                                                                                                                                   */
/* If any of the timeout callback functions called menas we have LOC fault.                                                                                                                                                      */
/*                                                                                                                                                                                                                               */
/* Note about Indication flags:                                                                                                                                                                                                  */
/* Indication flags are set by the DBKOM automatically when we receive the particular message on bus.                                                                                                                            */
/* If application uses the Indication flags, it is application responsible to clear the Indication flag after they served.                                                                                                       */
/* Note about Timeout flags:                                                                                                                                                                                                     */
/* Timeout flags are set by the DBKOM automatically when the particular message on bus was not received by specific time.                                                                                                        */
/* Timeout flags get cleared by DBKOM automatically when the message receives again.                                                                                                                                             */
/*                                                                  */

#define CANIO_C

/* includes */

/* NWM Inculde files */

#include "il_inc.h"
#include "v_inc.h"
//#include "can_par.h"

/*Application Inculde files */
#include "canio.h"
#include "TYPEDEF.h"
#include "main.h"
#include "heating.h"
#include "DiagIOCtrl.h"
#include "DiagWrite.h"
#include "FaultMemory.h"
#include "FaultMemoryLib.h"
#include "Heating.h"
#include "StWheel_Heating.h"
#include "mw_eem.h"

/* Defines */
#define VIN_MAX_COUNT_K       10
#define NET_CONFIG_PRGM       0x01
#define VEH_LINE_DEBOUNCE_TIME  50  /* 2 seconds @40ms timeslice rate */
#define BASE_DELAY_2_SEC        50  /* 2 seconds @40ms timeslice rate */
#define SNA_TGW_DISP_STAT		0x0F /* SNA FTR00139668  --   TGW LOC detection change-Now If module receives TGW_DISP_STAT = SNA blow TGW LOC */
#define LOC_ENABLE_WAIT_TIME_K 125  /*Wait time for LOC 5sec */

#define	CBC_TIMEOUT_TIME_K	25  //CBC Signal Timeout time 40ms * 25 times = 1000ms
#define TGW_TIMEOUT_TIME_K	250 //CBC Signal Timeout time 40ms * 250 times = 10000ms

/* Macros */
#define LOWMEMORY(x)  (x & 0x00FF)            
#define HIGHMEMORY(x) ((x >> 8) & 0x00FF)

/* variables */
unsigned char Canio_VehIdNumberCounter_c;

static unsigned char update_veh_state_c;		// update_veh_delay_c variable is Not accessed.

unsigned char temp_odo_c; 

unsigned char canio_LOC_Timeout_Count_c;
unsigned char canio_Debounce_Time_c;
unsigned char canio_current_veh_line_c;

unsigned char aux_c;

unsigned char canio_current_EcuCfg5_stat_c; //Present value will be hold after initalization.
unsigned char EcuCfg5_counter_c; //Counts upto 50 (2sec time), If value ECUCfg5 value fluctuates

unsigned char canio_CBCTimeOut_Additional_WaitTime_c; //PN CBC additional wait time after timeout signal for CBC
unsigned char canio_TGWTimeOut_Additional_WaitTime_c; //PN TGW additional wait time after timeout signal for CBC
	
u_8Bit canio_RemSt_stat_updated;  // Indicates that remote start status is updated.
/* Global Structure */
//struct Proxi_cfg Proxi_data_s;

/* Function prototypes */

/* Writes the original VIN to Mirror */
void canLF_WriteOriginalVIN(void);
/* Writes Vehicle Information to Mirror */
//void canLF_VehicleInfo_Update(void);
/* Enable conditions for the LOC */
void canLF_LOCTimeout_Condition(void);
/* This function checks vehicle line value from bus atleast for 2 sec */
/* to see the value gets from Bus is consistant                       */
//unsigned char canLF_Veh_Line_Check_From_Bus (void);

/* This function checks ECU Cfg5 status value from bus atleast for 2 sec */
/* to see the value gets from Bus is consistant                          */
/* Applicable only for Power Net                                         */
//unsigned char canLF_ECU_Cfg5_Check_From_Bus (void);





/**************************************************************************************************
 * Function Name:		canF_Init
 * Called In:			Initialization Phase
 * Calls To:			EE_BlockRead
 * 						
 * Input:				None. Every time Module starts this function will get executed.
 * 						
 * Output:				Seat Config, ECU Config, Wheel Config, Vehicle Info will get updated
 * 						based on the EEPROM.
 * 
 * Description:			Called at the beginning of the module startup.
 * 						Looks for EEPROM Information to operate the Module with stored info.
 ***************************************************************************************************/
@far void canF_Init(void)
{
	unsigned char temp_Vehicle_line_c;
	
	EE_BlockRead(EE_REMOTE_CTRL_STAT, &canio_RemSt_stored_stat);

//	/* Read PROXI Configuration from EEPROM */
//	EE_BlockRead(EE_PROXI_CFG, (UINT8*)&ru_eep_PROXI_Cfg);
//	
//	/* PROXI Configuration is not updated. First Load with the default Values */
//	if(ru_eep_PROXI_Cfg.byte[0] == 0xFF)
//	{
//		//memset((u_8Bit *)&ru_eep_PROXI_Cfg, PROXI_ID_VEH_SIZE + PROXI_CRC_SIZE - 1, PROXI_CFG_CODE_DEFAULT);
//		ru_eep_PROXI_Cfg.byte[0] = PROXI_CFG_CODE_DEFAULT;
//		ru_eep_PROXI_Cfg.byte[1] = PROXI_CFG_CODE_DEFAULT;
//		ru_eep_PROXI_Cfg.byte[2] = PROXI_CFG_CODE_DEFAULT;
//		ru_eep_PROXI_Cfg.byte[3] = PROXI_CFG_CODE_DEFAULT;
//		ru_eep_PROXI_Cfg.byte[4] = PROXI_CFG_CODE_DEFAULT;
//		ru_eep_PROXI_Cfg.byte[5] = PROXI_CFG_CODE_DEFAULT_LAST_BYTE;
//		
//		ru_eep_PROXI_Cfg.s.Driver_Side = PROXI_DS_DEFAULT;
//		ru_eep_PROXI_Cfg.s.Remote_Start = PROXI_RS_DEFAULT;
//		ru_eep_PROXI_Cfg.s.Vehicle_Line_Configuration = PROXI_VEH_LINE_DEFAULT;
//		ru_eep_PROXI_Cfg.s.Country_Code = PROXI_COUNTRY_CODE_US;
//		ru_eep_PROXI_Cfg.s.Model_Year = PROXI_MY_DEFAULT;
//		ru_eep_PROXI_Cfg.s.Heated_Seats_Variant = PROXI_HEATSEAT_DEFAULT;
//		ru_eep_PROXI_Cfg.s.Seat_Material = PROXI_SEAT_TYPE_CLOTH;
//		ru_eep_PROXI_Cfg.s.Wheel_Material = PROXI_WHEEL_TYPE_WOOD;
//		ru_eep_PROXI_Cfg.s.Steering_Wheel = PROXI_WHEEL_PRESENT;
//		
//		EE_BlockWrite(EE_PROXI_CFG, (UINT8*)&ru_eep_PROXI_Cfg);
//		
//	}
	
	canio_status_flags_c.cb = 0;
	canio_RX_TravelDistance_w = CANBUS_KILOMETER_INVALID_K; //Initialize with the invalid default odo
	update_veh_state_c = 0;
	
	canio_Debounce_Time_c = 0;
	/* Get the values from the PROXI Config either default or stored one's */
	canio_LHD_RHD_c = ru_eep_PROXI_Cfg.s.Driver_Side; //LHD;

    /* Default . */
	main_SwReq_stW_c=0;
	
	/* Check Remote Start stored status */
	if (canio_RemSt_stored_stat == REMST_ENABLED)
	{
		/* Set Remote Start config status to ON */
		canio_RemSt_configStat_c = TRUE;
		
		/* MKS 42926  after disabling the Remote start feature RemSt_DCS flag is OFF in that IGN cycle. */
		/* Later when cycles the Ignition, RemSt_DCs comes up with default value as ON.	                */
		/* Set our remote start signal */
		IlPutTxRemSt_DCSSts(TRUE);
		    
	}
	else 
	{
		/* Set Remote Start config status to OFF. No automatic activation. */
		canio_RemSt_configStat_c = FALSE;

		/* MKS 42926  after disabling the Remote start feature RemSt_DCS flag is OFF in that IGN cycle. */
		/* Later when cycles the Ignition, RemSt_DCs comes up with default value as ON.	                */
		/* Clear our remote Start signal */
		IlPutTxRemSt_DCSSts(FALSE);
	}
	

    /* Vehicle lines with Front switch requests from CAN bus HVAC */
	main_SwReq_Front_c = VEH_BUS_HVAC_FRONT_ONLY_K;
	/*vehicle lines with Rear seats with switch connector */
	main_SwReq_Rear_c = VEH_SWITCH_REAR_ONLY_K;
	
}


/**************************************************************************************************
 * Function Name:		canF_CyclicTask
 * Called In:			Main Timeslice (40ms)
 * Calls To:			canLF_VehicleInfo_Update
 * 						canLF_LOCTimeout_Condition
 * 						canLF_WriteOriginalVIN
 * 						
 * Input:				As long IGN in RUN, System UP, this function gets called
 * 						
 * Output:				canLF_VehicleInfo_Update: Vehicle Information received from PROXI will get updated.
 * 						canLF_LOCTimeout_Condition: LOC DTC Initial conditions Check
 * 						canLF_WriteOriginalVIN: Update VIN Information to EEP, If necessary
 * 
 * Description:			Called in every 40ms timeslice.
 * 						Will update the information as received.
 ***************************************************************************************************/
@far void canF_CyclicTask(void)
{
	unsigned char temp_vin_counter_c,temp_vin_ctr_limit_c;
		
	/* Vehicle line Update */
	//canLF_VehicleInfo_Update();
	/* Initial conditions to be checked for LOC */
	canLF_LOCTimeout_Condition();
	/* Write Original VIN */
	canLF_WriteOriginalVIN();
	
}

/**************************************************************************************************
 * Function Name:		canF_CyclicRemoteStart
 * Called In:			Main Timeslice (10ms)
 * Calls To:			EE_BlockRead
 * 						EE_BlockWrite
 * 
 * Input:				EEPROM stored value to see Previous Remote Start activity enabled/disabled
 * 						Latest value received from CFG_RQ (CSWM_CFG_RQ_ManIndFunc)
 * 						
 * Output:				canio_RemSt_configStat_c = TRUE/FALSE based on latest Info. This will be used
 * 						in main Remote Start Functionality.
 * 						If TRUE: Remote start enabled.
 * 						If FALSE: Remote Start disabled.
 * 
 * 						Update EEP If Status changes.
 * 						Update CSWM_A1 message If status changes.
 * 
 * Description:			This function first looks for Remote Start status from EEP and the value.
 * 						Received from latest. If the stored Info and the latest received Info are
 * 						different, then the latest will be updated to EEP and CSWM_A1 message. 
 ***************************************************************************************************/
@far void canF_CyclicRemoteStart(void)
{
	/* Copy the structure to Local */
   //	tRemSt_DCS LocalRemSt_DCS;
	unsigned char temp_remote_stat_c;
		
	EE_BlockRead(EE_REMOTE_CTRL_STAT,&temp_remote_stat_c);
	/* Remote Start Event Status bit information from CSWM */
	if (canio_RX_Rem_CFG_FEATURE_c == MAIN_CFG_RemSt_CSWM) {
		
		/* Get the RemSt_DCS Container */
	   //	LocalRemSt_DCS.c = dbkGetTxRemSt_DCS();

		/* Requested CFG is default or Enabled */
		if ( (canio_RX_Rem_CFG_STAT_RQ_c == CFG_DEFAULT) || (canio_RX_Rem_CFG_STAT_RQ_c == CFG_ENABLE) ) {
			/* Update the stored Remote start status variable */
			canio_RemSt_stored_stat = REMST_ENABLED;

			/* Set our remote start signal */
		    IlPutTxRemSt_DCSSts(TRUE);

			//SetBitTxRemSt_DCS(LocalRemSt_DCS);
			/* Set the Remote start config to ON */
			canio_RemSt_configStat_c = TRUE;

			/* Check if we need to update the stored Remote start status */
			if (temp_remote_stat_c == REMST_ENABLED) {
				/* No update is necassary */
			}
			else {
				/* Modified version: CK 04.24.2009 */
				/* Write New Remote Start status. */
				EE_BlockWrite(EE_REMOTE_CTRL_STAT,&canio_RemSt_stored_stat);
				canio_RemSt_stat_updated = TRUE;
			}
		}
		else {
			/* Requested CFG is disabled */
			if (canio_RX_Rem_CFG_STAT_RQ_c == CFG_DISABLE) {
				/* Update the stored Remote start status variable */
				canio_RemSt_stored_stat = REMST_DISABLED;

				/* Clear our remote tart signal */
				IlPutTxRemSt_DCSSts(FALSE);

				//ClearBitTxRemSt_DCS(LocalRemSt_DCS);
				/* Set the Remote Start config to OFF */
				canio_RemSt_configStat_c = FALSE;
				/* Check if we need to update the stored Remote start status */
				if (temp_remote_stat_c == REMST_DISABLED) {
					/* No update is necassary */
				}
				else {
					/* Modified version: CK 04.24.2009 */
					/* Write New Remote Start status. */
					EE_BlockWrite(EE_REMOTE_CTRL_STAT,&canio_RemSt_stored_stat);
					canio_RemSt_stat_updated = TRUE;
					
				}
			}
			else {
				/* Its an undefined signal. Do not act upon it. */
			}
		}
		/* Send the new state on Bus */
//		dbkPutTxRemSt_DCS(LocalRemSt_DCS);
	}
	else {
		/* This is not the Remote Start Driver Comfort System. Do nothing. */
	}

}

/**************************************************************************************************
 * Function Name:		canF_CyclicManIndFunc
 * Called In:			Main Timeslice (10ms)
 * Calls To:			CSWM_VehStart_PKT_ManIndFunc
 * 						CSWM_EngRPM_ManIndFunc
 * 						CSWM_FL_HS_RQ_TGW_ManIndFunc
 * 						CSWM_FR_HS_RQ_TGW_ManIndFunc
 * 						CSWM_HSW_RQ_TGW_ManIndFunc
 * 						CSWM_ExtTemp_ManIndFunc
 * 						CSWM_BatVolt_ManIndFunc
 * 						CSWM_ODO_ManIndFunc
 * 						CSWM_CFG_RQ_ManIndFunc
 * 						CSWM_StW_Actn_Rq_ManIndFunc
 * 						CSWM_VIN_ManIndFunc
 * 						CSWM_EngineSts_ManIndFunc
 * 						CSWM_EngineSpeedValidData_ManIndFunc
 * 
 * Input:				As long IGN in RUN, System UP, this function gets called
 * 						
 * Output:				All the Received messages for CUSW performance will be stored in.
 * 
 * Description:			Stores the values received from CAN Bus. 
 ***************************************************************************************************/
@far void canF_CyclicManIndFunc(void)
{
	/* Call the below functions to get the values from CAN Bus */
	/* As FIAT architecture is not based on Interrupts, We need to manually call these functions */
	/* To receive the signal values */
	CSWM_VehStart_PKT_ManIndFunc(); //IGN Status and RemoteStartActive Info
	CSWM_EngRPM_ManIndFunc();		//Engine RPM
	CSWM_FL_HS_RQ_TGW_ManIndFunc();	//F L Seat Heater Request
	CSWM_FR_HS_RQ_TGW_ManIndFunc();	//F L Seat Heater Request
	CSWM_HSW_RQ_TGW_ManIndFunc();	//Steering Wheel Request
	CSWM_ExtTemp_ManIndFunc();		//External TEMP (AMB TEMP) Info
	CSWM_BatVolt_ManIndFunc();		//System voltage Info
	CSWM_ODO_ManIndFunc();			//Odometer Info
	CSWM_CFG_RQ_ManIndFunc();		//Remote Start Info
	CSWM_StW_Actn_Rq_ManIndFunc();	//StWheel Temp and SnsFlt Ingo
	CSWM_VIN_ManIndFunc ();			//VIN	
	CSWM_EngineSts_ManIndFunc();	//Engine Status
	CSWM_EngineSpeedValidData_ManIndFunc();//Engine Speed Valid
	CSWM_CFG_DATA_RESP_ManIndFunc ();
}


/**************************************************************************************************
 * Function Name:		canF_TGW_LOC_DTC
 * Called In:			canLF_LOCTimeout_Condition
 * Calls To:			FMemLibF_ReportDtc
 * 
 * Input:				Looks for Initial Conditions like, no can Bus OFF.
 * 						No Received Signal from CBC_I4 for IGN Status
 * 						
 * Output:				If conditions met, sets "final_CBC_LOC_b" 
 * 						Calls  FMemLibF_ReportDtc function to Mature the CBC LOC DTC.
 * 						If conditions not met, clears "final_CBC_LOC_b"
 * 						Calls  FMemLibF_ReportDtc function to De-Mature the CBC LOC DTC.
 * 
 * Description:			Looks for the above conditions to met. 
 * 						If the conditions are met, waits 5sec
 * 						and sets the final_CBC_LOC_b flag and stores the Active DTC info to EEP.
 * 						If the conditions are not met, 
 * 						clears the final_CBC_LOC_b flag and stores the DTC info to EEP.	
 ***************************************************************************************************/
void canF_CBC_LOC_DTC(void)
{

	/* Do the LOC when only failsafe conditions disabled and CAN Bus OFF Error was not present. */
	if ((diag_failsafe_cond_b == FALSE) &&
		(canio_canbus_off_b == FALSE))
	{
		/* Check to see, Is there timeout for the Ign Signal */
		/* If Yes, means the singal not received for > 1sec. Set the flag for LOC */
		/* Else Just increment the timer. IF signal receives, counter will get cleared */
		if(canio_IgnRun_RemSt_Timeout_c > CBC_TIMEOUT_TIME_K)
		{
			final_CBC_LOC_b = TRUE;
		}
		else
		{
			canio_IgnRun_RemSt_Timeout_c++;
			final_CBC_LOC_b = FALSE;
		}

		if ( final_CBC_LOC_b )
		{
			//Wait additional 5sec, If it is true Bus Off, we should not blow LOC */
			if (canio_CBCTimeOut_Additional_WaitTime_c >= LOC_ENABLE_WAIT_TIME_K)
			{
				FMemLibF_ReportDtc(DTC_NWM_LOC_WITH_CBC_K, ERROR_ON_K);
			}
			else
			{
				canio_CBCTimeOut_Additional_WaitTime_c++;
			}
		}
		else
		{
			/* No timeout. */
			FMemLibF_ReportDtc(DTC_NWM_LOC_WITH_CBC_K, ERROR_OFF_K);
			canio_CBCTimeOut_Additional_WaitTime_c = 0;
		}
	}
	else
	{
		//Do not detect LOC for CBC.
	}
}


/**************************************************************************************************
 * Function Name:		canF_TGW_LOC_DTC
 * Called In:			canLF_LOCTimeout_Condition
 * Calls To:			FMemLibF_ReportDtc
 * 
 * Input:				Looks for Initial Conditions like, no can Bus OFF.
 * 						No Received Signal from TGW_A1 for Front Left Heater Switch
 * 						
 * Output:				If conditions met, sets "final_TGW_LOC_b" 
 * 						Calls  FMemLibF_ReportDtc function to Mature the TGW LOC DTC.
 * 						If conditions not met, clears "final_TGW_LOC_b"
 * 						Calls  FMemLibF_ReportDtc function to De-Mature the TGW LOC DTC.
 * 
 * Description:			Looks for the above conditions to met. 
 * 						If the conditions are met, waits 5sec
 * 						and sets the final_TGW_LOC_b flag and stores the Active DTC info to EEP.
 * 						If the conditions are not met, 
 * 						clears the final_TGW_LOC_b flag and stores the DTC info to EEP.	
 ***************************************************************************************************/
/* SDO@canF_TGW_LOC_DTC@ */
/* If LOC enable conditions are met, then if we see timeout for any of the following signals, we blow LOC for TGW module. */
void canF_TGW_LOC_DTC(void)
{
	/* This function will get executed only incase of L/J vehicle lines                          */

	/* Do the LOC when only failsafe conditions disabled and CAN Bus OFF Error was not present. */
	if ((diag_failsafe_cond_b == FALSE) &&
		(canio_canbus_off_b == FALSE))
	{
		/* Check to see, Is there timeout for the Ign Signal */
		/* If Yes, means the singal not received for > 10sec. Set the flag for LOC */
		/* Else Just increment the timer. IF signal receives, counter will get cleared */
		if(canio_FL_HS_RQ_TGW_Timeout_c > TGW_TIMEOUT_TIME_K)
		{
			final_TGW_LOC_b = TRUE;
		}
		else
		{
			canio_FL_HS_RQ_TGW_Timeout_c++;
			final_TGW_LOC_b = FALSE;
		}

		if (final_TGW_LOC_b)
		{
			//Wait additional 5sec, If it is true Bus Off, we should not blow LOC */
			if (canio_TGWTimeOut_Additional_WaitTime_c >= LOC_ENABLE_WAIT_TIME_K)
			{
				FMemLibF_ReportDtc(DTC_NWM_LOC_WITH_TGW_K, ERROR_ON_K);
			}
			else
			{
				canio_TGWTimeOut_Additional_WaitTime_c++;
			}
		}
		else
		{
			/* No timeout. */
			FMemLibF_ReportDtc(DTC_NWM_LOC_WITH_TGW_K, ERROR_OFF_K);
			canio_TGWTimeOut_Additional_WaitTime_c = 0;
		}
	}
	else
	{
		//Do not detect LOC for TGW.
	}
}


/* SDO@canLF_VehicleInfo_Update@ */
/* SD Text                                                                                    */
/* -------                                                                                    */
/* This function gets executed to update the latest vehicle line information into the EEPROM. */
/* Based on the Vehicle line learned from the CAN BUS it stores information into the Mirror.  */
/*                                                                                            */
/* Also updates Body style, Model Year, Country code and ECU CFG for PN.                      */
/*                                                                                            */
/*For model year 2011, vehicle lines will be added, 										  */	
/*some of which require modification of the sources of the activation signals for the 		  */
/*front and rear seat heaters.																  */
/**********************************************************************************************/
/* Section 8.11.5: Source of Rear Seat Heat Activation Signal                                 */
/*Distinction between bussed rear seat heat activation and direct connection of switches      */ 
/*according to the vehicle line																  */	
/*Bussed	Hard Wired Input																  */
/*DS		WK																				  */	
/*D2		WD																				  */
/*DA		LX																				  */
/*DD		LD																				  */
/*DJ		JC																				  */			
/*DP																						  */
/*DX																						  */	
/*DZ 																						  */
/**********************************************************************************************/
/* ECU Configuration for Service parts:                                                       */
/* (Spec change-e-mail 02/20/09 from Brian)													  */
/* The information contained in EC_Memory is valid if EC_ECUCgf5_Stat = PROGRAMMED            */
/* All other states the MSM or CSWM shall use the last known valid configuration              */
/* If there isn't a last known configuration each module shall default to min feature content */
/**********************************************************************************************/
/* Calls from                                                                                 */
/* --------------                                                                             */
/* canF_CyclicTask :                                                                          */
/*    canLF_VehicleInfo_Update()                                                              */
/*                                                                                            */
/* Calls to                                                                                   */
/* --------                                                                                   */
/* canLF_Veh_Line_Check_From_Bus()                                                            */
/* 		                                                                                      */
//void canLF_VehicleInfo_Update(void)
//{
//	/* This function gets executed to update the latest vehicle line information into the EEPROM. */
//	/* Based on the Vehicle line learned from the CAN BUS it stores information into the Mirror.  */
//	unsigned char temp_Vehicle_line_c, store_veh_line_c;
//	unsigned char temp_body_style_c;
//	unsigned char temp_model_year_c;
//	unsigned char temp_country_code_c;
//	unsigned char temp_ecu_config_c, temp_ecucfg5_stat_c,temp_ecumemory_c,temp_seatcfg_c,temp_stwcfg_c;
//	
//	//EE_BlockRead(EE_PROXI_CFG, (u_8Bit*)&Proxi_data_s);
//	
//	/* First time after POR ? */
//	if (!update_veh_info_b)
//	{
//
//		/* Modified on 04.24.09 by CK: Body style signal not available = 0x0F */
//		if (/* canio_BODY_STYLE_c != 0x0F*/1)	
//		{
//			switch (update_veh_state_c) {
//				/* Vehicle line update */
//    			case 0:
//				{
//					/* Get the vehicle line value stored in EEPROM */
////					EE_BlockRead(EE_VEH_LINE, &temp_Vehicle_line_c);
//
//					/* After initial 2sec wait period , here wait atleast 2sec before getting the actual vehicle line from Bus.	*/
//					/* If 2 seconds pass and the value is consistant, we go inside the loop.	*/
//					if (/* canLF_Veh_Line_Check_From_Bus() */1)
//					{
//							/* Is the vehicle line valid? */
//							if (/* (canio_current_veh_line_c == VEH_PF) ||
//								(canio_current_veh_line_c == VEH_MM) ||
//								(canio_current_veh_line_c == VEH_MA) ||
//								(canio_current_veh_line_c == VEH_KL) ||
//								(canio_current_veh_line_c == VEH_UF) ||
//								(canio_current_veh_line_c == VEH_UT) ||
//								(canio_current_veh_line_c == VEH_UA) ||
//								(canio_current_veh_line_c == VEH_PK) ||
//								(canio_current_veh_line_c == VEH_TR) */0 
//								)							
//						{
//							/* Is the vehicle information in EEPROM is not written or the information in EEPROM to the actual value is different */
//							/* the new value we write into EEPROM                                                                                */
//							if (/*Proxi_data_s.vehicle_line_c == 0xFF || Proxi_data_s.vehicle_line_c != canio_current_veh_line_c*/0)
//							{
//								//Proxi_data_s.vehicle_line_c = canio_current_veh_line_c;
//								//EE_BlockWrite(EE_PROXI_CFG, (u_8Bit*)&Proxi_data_s);
//								update_veh_state_c++;
//								/* Value changed needs to update the EEPROM */
//								update_over_veh_info_b = 0;
//
//								/* Vehicle lines with Front switch requests from CAN bus HVAC */
//								main_SwReq_Front_c = VEH_BUS_HVAC_FRONT_ONLY_K;
//								/*vehicle lines with Rear seats with switch connector */
//								main_SwReq_Rear_c = VEH_SWITCH_REAR_ONLY_K;
//								/* Steering Wheel switch requests are from HVAC, , avoid default WK vehicle line  */
//								main_SwReq_stW_c = 0;
//							}
//							else
//							{
//								/* Information is there in EEPROM. Just update the switch case */
//								update_veh_state_c++;
//								/* no need to update the EEPROM */
//								update_over_veh_info_b = 1;
//								/* We are here means the updates are finished for vehicle line for sure */
//							}
//						}
//						else
//						{
//							/* The value received from Bus is not valid vehicle line, now to store the value in EEPROM, check to see what value was present in EEPROM. */
//							/* If the default value is present do nothing.                                                                                     */
//							/* Else store the default value                                                                                                    */
//							if (/*Proxi_data_s.vehicle_line_c == VEH_PF*/1)
//							{
//								/* Information is there in EEPROM. Just update the switch case */
//								update_veh_state_c++;
//								/* no need to update the EEPROM */
//								update_over_veh_info_b = 1;
//							}
//							else
//							{
//								/* We are here means, The value received from Bus is Invalid value, and the value from EEPROM is not the actual/default vehicle line. Store the default vehicle line to EEPROM */
//								//Proxi_data_s.vehicle_line_c = VEH_PF;
//								/* Lint Info 774: if always evaluates to true: Commented by CK 04.24.09 */
//								/* Modified version: CK 04.24.2009 */
//								//EE_BlockWrite(EE_PROXI_CFG, (u_8Bit*)&Proxi_data_s);
//								update_veh_state_c++;
//								/* Value changed needs to update the EEPROM */
//								update_over_veh_info_b = 0;
//							
//								/* Vehicle lines with Front switch requests from CAN bus HVAC */
//								main_SwReq_Front_c = VEH_BUS_HVAC_FRONT_ONLY_K;
//								/*vehicle lines with Rear seats with switch connector */
//								main_SwReq_Rear_c = VEH_SWITCH_REAR_ONLY_K;
//							}
//						}
//						
//				}
//				else
//				{
//				}
//				break;
//			}
//				
////			/* Body Style Update */
////			case 1:
////			{
//////				EE_BlockRead(EE_BODY_STYLE, &temp_body_style_c);
////					
////				/* Is Body style information in EEPROM is not written or the information in EEPROM to the actual value is different */
////				/* the new value we write into EEPROM                                                                               */
////				if (  Proxi_data_s.vehicle_line_c == 0xFF || temp_body_style_c != canio_BODY_STYLE_c )
////				{
////					EE_BlockWrite(EE_BODY_STYLE, &canio_BODY_STYLE_c);
////					update_veh_state_c++;
////					/* Value changed needs to update the EEPROM */
////					update_over_veh_info_b = 0;
////				}
////				else
////				{
////					update_veh_state_c++;
////					/* no need to update the EEPROM */
////					update_over_veh_info_b = 1;
////				}
////				break;
////			}
//				
//			/* Model Year Update */
//			case 1:
//			{
////				EE_BlockRead(EE_VEH_MODEL_YEAR, &temp_model_year_c);
//					
//				if ( /*Proxi_data_s.model_year_c == 0xFF || Proxi_data_s.model_year_c != canio_MODEL_YEAR_c */0)
//				{
//					//Proxi_data_s.model_year_c = canio_MODEL_YEAR_c;
//					//EE_BlockWrite(EE_PROXI_CFG, (u_8Bit*)&Proxi_data_s);
//					update_veh_state_c++;
//					/* Value changed needs to update the EEPROM */
//					update_over_veh_info_b = 0;					
//				}
//				else
//				{
//					update_veh_state_c++;
//					/* no need to update the EEPROM */
//					update_over_veh_info_b = 1;
//				}
//				break;
//			}
//
////			/* Country Code Update */
////			case 3:
////			{
////				EE_BlockRead(EE_CTRY_CODE, &temp_country_code_c);
////				
////				if ( /* temp_country_code_c == 0xFF || temp_country_code_c != canio_COUNTRY_c */0 )
////				{
////					EE_BlockWrite(EE_CTRY_CODE, &canio_COUNTRY_c);
////					update_veh_state_c++;
////					update_over_veh_info_b = 0;
////				}
////				else
////				{
////					update_veh_state_c++;
////					update_over_veh_info_b = 1;
////				}
////				break;
////			}
//			/* ECU Configuration for Service parts */
//			case 2:
//			{
//					
///**********************************************************************************************/
///* ECU Configuration for Service parts:                                                       */
///* (Spec change-e-mail 02/20/09 from Brian)													  */
///* The information contained in EC_Memory is valid if EC_ECUCgf5_Stat = PROGRAMMED            */
///* All other states the MSM or CSWM shall use the last known valid configuration              */
///* If there isn't a last known configuration each module shall default to min feature content */
//
///* First logic checks is HW equipped with Variant V7									      */
///* If Variant is other than V7, logic just jumps to the next case to finish the routine       */
///* If variant is V7, logic waits atleast 2sec to determine whether the received value is stable*/
///* If the value is not stable again itjumps to the next case to finsih the routine            */
///* In such cases, logic works with either known/default ECU config                            */
//					
///**********************************************************************************************/
////					if (/*main_variant_select_c == CSWM_V7*/0)
////					{
////						if ( /*canLF_ECU_Cfg5_Check_From_Bus()*/0)
////						{
////							/* Read the latest known ECU configuration from EEPROM */
////							//EE_BlockRead(EE_PROXI_CFG, (u_8Bit*)&Proxi_data_s);
////
////							/* Check to see, ECU Cfg 5 message is with programmed state */
////							/* If in Programmed state, enter inside */
////							/* Else, just update the case, before leaving this function, logic updates */
////							/* either valid information or default information for variants */
////								if (/*canio_EC_ECUCfg5_Stat_c == NET_CONFIG_PRGM*/1)
////								{
////									if((Proxi_data_s.ecu_cfg_c == 0xFF) || 
////									   (Proxi_data_s.ecu_cfg_c != temp_ecumemory_c) ||
////									   (Proxi_data_s.seat_cfg_c != temp_seatcfg_c) ||
////									   (Proxi_data_s.wheel_cfg_c != temp_stwcfg_c) )
////									{
////										if((Proxi_data_s.ecu_cfg_c == 0xFF) || 
////										   (Proxi_data_s.ecu_cfg_c != temp_ecumemory_c))
////										{
////											/* Update the Main ECU config to EEPROM */		
////											Proxi_data_s.ecu_cfg_c = temp_ecumemory_c;
////											EE_BlockWrite(EE_PROXI_CFG, (u_8Bit*)&Proxi_data_s);
////										}
////										if(Proxi_data_s.seat_cfg_c != temp_seatcfg_c)
////										{
////										/* Update the Main ECU config to EEPROM */		
////										Proxi_data_s.seat_cfg_c = temp_seatcfg_c;
////										EE_BlockWrite(EE_PROXI_CFG, (u_8Bit*)&Proxi_data_s);
////										}
////										if(Proxi_data_s.wheel_cfg_c != temp_stwcfg_c)
////										{
////										/* Update the Main ECU config to EEPROM */		
////										Proxi_data_s.wheel_cfg_c = temp_stwcfg_c;
////										EE_BlockWrite(EE_PROXI_CFG, (u_8Bit*)&Proxi_data_s);
////										}
////											
////										update_veh_state_c++;
////										/* update the EEPROM */
////										update_over_veh_info_b = 0;
////									}
////									else
////									{
////										/* Nothing changed from previous cycle */
////										/* Just update the case */
////										update_veh_state_c++;
////										/* no need to update the EEPROM */
////										update_over_veh_info_b = 1;
////									}
////								}
////								else
////								{
////									/* ECU Config stat 5 status is not in programmed state */
////									/* Just update the case */
////									update_veh_state_c++;
////									/* no need to update the EEPROM */
////									update_over_veh_info_b = 1;
////								}
////						}
////						else
////						{
////							/* Cfg5 status is not stable for atleast 2sec */
////							/* Jump to next case */
////							if(/*EcuCfg5_counter_c >= 50*/1)
////							{
////								//ECU Cfg5 Status message is fluctuating.
////								//We can not determine any value for this IGN cycle. Go for default.
////								/* Just update the case */
////								update_veh_state_c++;
////								/* no need to update the EEPROM */
////								update_over_veh_info_b = 1;
////							}
////						}
////								
////					}
////					else
////					{
////						//Variant from HW is other than V7. Do nothing
////						/* Just update the case */
////						update_veh_state_c++;
////						/* no need to update the EEPROM */
////						update_over_veh_info_b = 1;
////						
////					}
//					//Variant from HW is other than V7. Do nothing
//					/* Just update the case */
//					update_veh_state_c++;
//					/* no need to update the EEPROM */
//					update_over_veh_info_b = 1;
//	
//				break;	
//			}
//				
//				
//			/* Finalyzing the updates */
//			case 3:
//			{
//					update_veh_state_c = 0; //Clear the increment state
//					update_veh_info_b = 1;  //Set the flag, so logic will not enter second time
//					/* Are the updates to Vehicle Info are True? */
//					if (!update_over_veh_info_b) {
//						timeto_update_vehinfo_b = 1; //Set the flag to denote that there is an update to vehicle info
//					}
//
//
//					/* MKS 52741: Enabled the below code Harsha..06/18/2010 */
//					/* When vehicle line not found for PN, switch request to follow the L series */
//					/* Vehicle lines with Front switch requests from CAN bus HVAC */
//					main_SwReq_Front_c = VEH_BUS_HVAC_FRONT_ONLY_K;
//					/*vehicle lines with Rear seats with switch connector */
//					main_SwReq_Rear_c = VEH_SWITCH_REAR_ONLY_K;
//
////					if (main_variant_select_c == CSWM_V7)
////					{
////						//Finalize the updates for ECU configuration PN
////						if(main_ecu_cfg_for_v7_c == V2_RHS_BUS_MASK)
////						{
////							/* Assign the SW functionality variable. */
////							CSWM_config_c = CSWM_4HS_VARIANT_2_K;
////						} else if (main_ecu_cfg_for_v7_c == V3_HSW_FVS_BUS_MASK){
////							/* Assign the SW functionality variable. */
////							CSWM_config_c = CSWM_2HS_2VS_HSW_VARIANT_3_K;
////						}else if (main_ecu_cfg_for_v7_c == V4_HSW_BUS_MASK){
////							/* Assign the SW functionality variable. */
////							CSWM_config_c = CSWM_2HS_HSW_VARIANT_4_K;
////						} else if (main_ecu_cfg_for_v7_c == V6_HSW_RHS_BUS_MASK){
////							/* Assign the SW functionality variable. */
////							CSWM_config_c = CSWM_4HS_HSW_VARIANT_6_K;
////						} else if (main_ecu_cfg_for_v7_c == V7_FVS_RHS_HSW_BUS_MASK){
////							/* Assign the SW functionality variable. */
////							CSWM_config_c = CSWM_4HS_2VS_HSW_VARIANT_7_K;
////						} else {
////							/* Default Assign the SW functionality variable. */
////							CSWM_config_c = CSWM_2HS_VARIANT_1_K;
////						}
////					}
////					else
////					{
////						//Variant from HW is other than V7. Do nothing
////					}
//					/* Completed Case 5: It is time to check if we need to turn */
//					/* off any un-supported outputs. mainF_OFF_NonSupported_Outputs_ServiceParts function in 40ms time slice */
//					ecucfg_update_b = TRUE;
//
//					break;
//				}
//				default:
//				{
//					/* */
//				}
//			}
//		}
//		else
//		{
//			/* Body style: SNA */
//			//update_veh_delay_c++;		// Not Used
//		}
//	}
//}

/**************************************************************************************************
 * Function Name:		canLF_WriteOriginalVIN
 * Called In:			canF_CyclicTask
 * 
 * Input:				Current ODO (From EEPROM)
 * 						and flag to check for update VIN (From VIN Message: CSWM_VIN_ManIndFunc)
 * 
 * Output:				If conditions met, VIN will be written to EEPROM.
 * 
 * Description:			This function looks for the current ODO reading from EEPROM and the VIN update
 * 						flag from VIN Message. If the flag is set and ODO is <= 5km, VIN will get	
 * 						updated into EEPROM.
 ***************************************************************************************************/
void canLF_WriteOriginalVIN(void)
{
	unsigned char temp_ODO_c[3];
	EE_BlockRead(EE_ODOMETER, (UInt8*)&temp_ODO_c[0]); 

	/* We are here means we are still with in Odo range to Update the Original VIN.  */
	/* Check to see logic passed 10 succesive cycles received the same VIN from Bus. */
	/* Time to update Original VIN? */
	if (update_original_vin_b)
	{
		if ((temp_ODO_c[0] == 0xFF) || (temp_ODO_c[0] == 0x00) && 
			(temp_ODO_c[1] == 0xFF) || (temp_ODO_c[1] == 0x00) &&
			(temp_ODO_c[2] == 0xFF) || (temp_ODO_c[2] <= 0x05))
		{
			/* VIN Original and Present VIN Info from Bus are different. */
			/* Update the Current Information to VIN Original            */
			/* Modified version: CK 04.24.2009 */
			/* Lint Warning 545: Suspicious use of & */
			EE_BlockWrite(EE_VIN_ORIGINAL, (u_8Bit*)&canio_CurrentVIN[0]);
			diag_load_bl_eep_backup_b = 1; //Its time to take backup for Boot data
			
			update_original_vin_b = 0;
		}
		else
		{
			/* No need to update the VIN Original */
		}
	}
	else
	{
		/* Original VIN from Shadow is  equal to the value from Bus	*/
	}
	
}


/**************************************************************************************************
 * Function Name:		canLF_LOCTimeout_Condition
 * Called In:			canF_CyclicTask
 * 
 * Input:				DTC Setting Enabled (Will take care by DTC cyclic logic)
 * 						Ignition status (Current IGN or Last Known IGN Status in RUN Only)
 * 						System Voltage (Current or Last known in Operational Range)
 * 						No Bus OFF flag.
 * 
 * Output:				If conditions met, Calls 
 * 						canF_CBC_LOC_DTC and 
 * 						canF_TGW_LOC_DTC functions.
 * 
 * Description:			Looks for the above conditions to met. If the conditions are met, waits 5sec
 * 						and calls the LOC DTC check functions. Else clears the 5sec timer and stays.	
 ***************************************************************************************************/
void canLF_LOCTimeout_Condition(void)
{

	unsigned char temp_result_c = 0;
	/* Condition 2 */
	if (canio_RX_IGN_STATUS_c == IGN_RUN)
	{
		/* IGN Status is RUN. Enable criteria for IGN Status is fullfilled. */
	}
	else
	{
		/* IGN STatus is other than RUN. Enable condition is failed */
		temp_result_c = 1;
	}
	
	/* Condition 3 */
	if ((canio_RX_BatteryVoltageLevel_c >= 90) && (canio_RX_BatteryVoltageLevel_c <= 160))
	{
		/* System voltage is in Range. Enable condition is OK */
	}
	else
	{
		/* System voltage is not in Range. Enable condition is failed */
		temp_result_c = 1;
	}
	
//	/* Condition 4 */
//	if (canio_RX_TravelDistance_w >= 5)
//	{
//		/* Odo meter is greater than 80km */
//		/* Each bit is 16km               */
//		/* Enable condition ok            */
//	}
//	else
//	{
//		/* Enable condition failed. */
//		temp_result_c = 1;
//	}
	
//	/* Condition 5 */
//	/* Check Is Network Configuration status is Programmed or not */
//	if (canio_NET_CFG_STAT_c == NET_CONFIG_PRGM)
//	{
//		/* Status is programmed. Enable criteria for NET Config status fullfilled. */
//	}
//	else
//	{
//		/* Status is Not Programmed. Enable condition Failed. */
//		temp_result_c = 1;
//	}
	
	/* Condition 6 */
	if (canio_canbus_off_b == FALSE)
	{
		/* No CAN Bus faults. */
	}
	else
	{
		/* CAN Bus fault present. Enable conditions failed */
		temp_result_c = 1;
	}

	/* Condition 7 */
	/* Either one or more enable conditions are failed. Do not increment the 5sec counter. */
	if (temp_result_c)
	{
		canio_LOC_Timeout_Count_c = 0;
		
		canio_CBCTimeOut_Additional_WaitTime_c = 0;
		canio_TGWTimeOut_Additional_WaitTime_c = 0;
		
	}
	else
	{
		/* Enable conditions met. Check to see wait time reached 5sec before blowing the DTC. If not Increment the counter */
		if (canio_LOC_Timeout_Count_c < LOC_ENABLE_WAIT_TIME_K)
		{
			canio_LOC_Timeout_Count_c++;
		}
	}
	/* Check to see counter reached 5sec timer.           */
	/* If not reached wait                                */
	if (canio_LOC_Timeout_Count_c >= LOC_ENABLE_WAIT_TIME_K)
	{
	    canF_CBC_LOC_DTC ();
	    canF_TGW_LOC_DTC ();
	}
	

}

/* SDO@canLF_Veh_Line_Check_From_Bus@ */
/*                                                                                                                                              */
/* When module gets power, this function does not allow "canLF_VehicleInfo_Update" to go through for 2sec.										*/
/* In these 2sec this function checks if vehicle line value received from bus is the same as the one stored recently to the local parameter.	*/
/* If both are same increments the timer. Else store the latest vehicle line into the local parameter and start 2sec timer from beginning. 		*/
/*                                                                                                                                              */
/* Until 2sec passed this function returns failed.                                                                                             */
/*                                                                                                                                              */
/* Calls from                                                                                                                                   */
/* --------------                                                                                                                               */
/* canLF_VehicleInfo_Update :                                                                                                                   */
/*    canLF_Veh_Line_Check_From_Bus()                                                                                                           */
/*                                                                                                                                              */
//unsigned char canLF_Veh_Line_Check_From_Bus (void)
//{
//	/* Check to see 2sec timer passed. If we are in 2second period go inside the logic. and return conditions are failed. */
//	if (canio_Debounce_Time_c < VEH_LINE_DEBOUNCE_TIME)
//	{
//		/* Check to see the value which we received from bus is same as the one which we stored recently to the local parameter. If both are same increment the timer. Else store the latest vehicle line into the local parameter and start 2sec timer from beginning. */
//		if (canio_current_veh_line_c != canio_VEH_LINE_c)
//		{
//			/* Store the latest value into local variable. And start the debounce time */
//			canio_current_veh_line_c = canio_VEH_LINE_c;
//			canio_Debounce_Time_c = 0;
//		}
//		else
//		{
//			/* The value received and the stored value are same. Increment the 2sec counter. */
//			canio_Debounce_Time_c++;
//		}
//		/* 2sec time did not pass, just return */
//		return 0;
//	}
//	else {
//		/* Reset counter */
//		canio_Debounce_Time_c = 0;
//		
//		/* Passed the 2sec timer, means we have consistant vehicle line value from Bus. */
//		return 1;
//	}
//}

/* SDO@canLF_ECU_Cfg5_Check_From_Bus@ */
/*                                                                                                                                              */
/* When module gets power, this function does not allow "canLF_VehicleInfo_Update (ECU Cfg Case 4)" to go through for 2sec.					    */
/* In these 2sec this function checks if ECU Cfg value received from bus is the same as the one stored recently to the local parameter.	        */
/* If both are same increments the timer. Else store the latest Cfg into the local parameter and start 2sec timer from beginning. 		        */
/* Until 2sec passed this function returns failed.                                                                                              */
/* Also If the value is fluctuating for more than 50 times/2sec time returns Failed                                                             */
/* In such cases Logic jumps to next case to lock the ECU config as not determined for this IGN cycle and runs with default/known valid config  */
/*                                                                                                                                              */
/* Calls from                                                                                                                                   */
/* --------------                                                                                                                               */
/* canLF_VehicleInfo_Update :                                                                                                                   */
/*    canLF_Veh_Line_Check_From_Bus()                                                                                                           */
/*                                                                                                                                              */
//unsigned char canLF_ECU_Cfg5_Check_From_Bus (void)
//{
//	/* Check to see 2sec timer passed. If we are in 2second period go inside the logic. and return conditions are failed. */
//	if (canio_Debounce_Time_c < BASE_DELAY_2_SEC)
//	{
//		/* Check to see the value which we received from bus is same as the one which we stored recently to the local parameter. If both are same increment the timer. Else store the latest vehicle line into the local parameter and start 2sec timer from beginning. */
//		if (canio_current_EcuCfg5_stat_c != canio_EC_ECUCfg5_Stat_c)
//		{
//			/* Store the latest value into local variable. And start the debounce time */
//			canio_current_EcuCfg5_stat_c = canio_EC_ECUCfg5_Stat_c;
//			canio_Debounce_Time_c = 0;
//			//Seems value is fluctuating. Just add the counter.
//			//If the value is still fluctuating, after reaching count 50, logic automatically jumps to the next level */
//			EcuCfg5_counter_c++;
//		}
//		else
//		{
//			/* The value received and the stored value are same. Increment the 2sec counter. */
//			canio_Debounce_Time_c++;
//		}
//		/* 2sec time did not pass, just return */
//		return 0;
//	}
//	else {
//		/* Reset counter */
//		canio_Debounce_Time_c = 0;
//		
//		/* Passed the 2sec timer, means we have consistant vehicle line value from Bus. */
//		return 1;
//	}
//}


/**************************************************************************************************
 * Function Name:		CSWM_VehStart_PKT_ManIndFunc
 * Called In:			canF_CyclicManIndFunc (10ms Time Slice)
 * 
 * Input:				Indication Flag and Info for IGN Status and RemSt Active Data.
 * 
 * Output:				IGN Status and RemStActv will be stored in global variable.
 * 						Used for main_CSWM_Status and functionality Logic.
 * 
 * Description:			Looks for Indication Flags. If sets, will receive Information from CAN Bus.
 * 						And stores the Information in Variable, 
 * 						If IGN Status is other than RUN, outputs will not trigger.
 * 						If RemStActv is not set, Remote Start Functionality will not be active.
 ***************************************************************************************************/
void  CSWM_VehStart_PKT_ManIndFunc(void)
{

	unsigned char local_flag_CmdIgnSts_c,local_flag_RemStActvSts_c;
	
	local_flag_CmdIgnSts_c = IlGetClrCmdIgnStsIndication();
	local_flag_RemStActvSts_c = IlGetClrRemStActvStsIndication();
	
	if(local_flag_CmdIgnSts_c)
	{
		if (diag_io_ign_lock_b == FALSE)
		{
			canio_RX_IGN_STATUS_c = IlGetRxCmdIgnSts();			
		}
		//local_flag_CmdIgnSts_c = 0;
	}
	else
	{
		/* Signal Not received or Timeout occured */
	}
	
	if(local_flag_RemStActvSts_c)
	{
		/* Get the Remote Start activation Bit */
		canio_RX_IgnRun_RemSt_c = IlGetRxRemStActvSts();
		/* When ever receives the message info from CAN, clear the counter */
		canio_IgnRun_RemSt_Timeout_c = 0;
		//local_flag_RemStActvSts_c = 0;
	}
	else
	{
		
	}
}

/**************************************************************************************************
 * Function Name:		CSWM_EngRPM_ManIndFunc
 * Called In:			canF_CyclicManIndFunc (10ms Time Slice)
 * 
 * Input:				Indication Flag and Info for Engine RPM Data.
 * 
 * Output:				Engine RPM will be stored in global variable.
 * 						Used for main_CSWM_Status and functionality Logic.
 * 
 * Description:			Looks for Indication Flags. If sets, will receive Information from CAN Bus.
 * 						And stores the Information in Variable, 
 * 						If not in range outputs will not trigger.
 ***************************************************************************************************/
void  CSWM_EngRPM_ManIndFunc(void)
{
	unsigned char local_flag_EngineSpeed_c;
	
	local_flag_EngineSpeed_c = IlGetClrEngineSpeedIndication();
	
	if(local_flag_EngineSpeed_c)
	{
		/* Get the ENG RPM value into Local variable */
		canio_RX_EngineSpeed_c = IlGetRxEngineSpeed();
	}
	else
	{
		/*Signal Not Received */
	}
}

/**************************************************************************************************
 * Function Name:		CSWM_FL_HS_RQ_TGW_ManIndFunc
 * Called In:			canF_CyclicManIndFunc (10ms Time Slice)
 * 
 * Input:				Indication Flag and Info for FL Heater Request Information from user.
 * 
 * Output:				Wheel Request will be stored in global variable.
 * 						Used for Seat Heater Logic.
 * 
 * Description:			Looks for Indication Flags. If sets, will receive Information from CAN Bus.
 * 						And stores the Information in Variable.
 * 						If not received "canio_FL_HS_RQ_TGW_Timeout_c" will be incremented to blow LOC for TGW
 * 						Based on Switch Request, FL Heater O/P change the PWM's.
 ***************************************************************************************************/
void  CSWM_FL_HS_RQ_TGW_ManIndFunc(void)
{
	unsigned char local_FL_HS_RQ_flag_c;
	
	local_FL_HS_RQ_flag_c = IlGetClrFL_HS_RQ_TGWStsIndication();
	
	if(local_FL_HS_RQ_flag_c)
	{
		/* Get the Front Left Heat Switch request from Bus  */
		canio_RX_FL_HS_RQ_TGW_c = IlGetRxFL_HS_RQ_TGWSts(); 
		/* When ever receives the message info from CAN clear the signal */
		canio_FL_HS_RQ_TGW_Timeout_c = 0;
	}
	else
	{
		/*Signal Not received */
	}
} 

/**************************************************************************************************
 * Function Name:		CSWM_FR_HS_RQ_TGW_ManIndFunc
 * Called In:			canF_CyclicManIndFunc (10ms Time Slice)
 * 
 * Input:				Indication Flag and Info for FR Heater Request Information from user.
 * 
 * Output:				Wheel Request will be stored in global variable.
 * 						Used for Seat Heater Logic.
 * 
 * Description:			Looks for Indication Flags. If sets, will receive Information from CAN Bus.
 * 						And stores the Information in Variable, 
 * 						Based on Switch Request, FR Heater O/P change the PWM's.
 ***************************************************************************************************/
void  CSWM_FR_HS_RQ_TGW_ManIndFunc(void)
{
	unsigned char local_FR_HS_RQ_flag_c;
	
	local_FR_HS_RQ_flag_c = IlGetClrFR_HS_RQ_TGWStsIndication();
	
	if(local_FR_HS_RQ_flag_c)
	{
		/* Get the Front Left Heat Switch request from Bus  */
		canio_RX_FR_HS_RQ_TGW_c = IlGetRxFR_HS_RQ_TGWSts();
	}
	else
	{
		/*Signal Not Received */
	}
} 

/**************************************************************************************************
 * Function Name:		CSWM_HSW_RQ_TGW_ManIndFunc
 * Called In:			canF_CyclicManIndFunc (10ms Time Slice)
 * 
 * Input:				Indication Flag and Info for Steering Wheel Request Information from user.
 * 
 * Output:				Wheel Request will be stored in global variable.
 * 						Used for Wheel Logic.
 * 
 * Description:			Looks for Indication Flags. If sets, will receive Information from CAN Bus.
 * 						And stores the Information in Variable, 
 * 						Based on Switch Request, steering Wheel O/P change the PWM's.
 ***************************************************************************************************/
void  CSWM_HSW_RQ_TGW_ManIndFunc(void)
{ 
	unsigned char local_HSW_RQ_flag_c;
	
	local_HSW_RQ_flag_c = IlGetClrHSW_RQ_TGWStsIndication();
	
	if(local_HSW_RQ_flag_c)
	{
		/* Now pass the information to the variable which used in Steering file */
		canio_RX_HSW_RQ_TGW_c = IlGetRxHSW_RQ_TGWSts();
	}
	else
	{
		/*Signal Not Received */
	
	}


}

/**************************************************************************************************
 * Function Name:		CSWM_ExtTemp_ManIndFunc
 * Called In:			canF_CyclicManIndFunc (10ms Time Slice)
 * 
 * Input:				Indication Flag and Info for External Temp voltage Data.
 * 
 * Output:				Battery voltage will be stored in global variable.
 * 						Used for Remote Start Logic.
 * 
 * Description:			Looks for Indication Flags. If sets, will receive Information from CAN Bus.
 * 						And stores the Information in Variable, 
 * 						Based on Ext Temp signal, Remote Start behavior gets changed.
 ***************************************************************************************************/
void  CSWM_ExtTemp_ManIndFunc(void)
{
	unsigned char local_ExtTemp_flag_c;
	
	local_ExtTemp_flag_c = IlGetClrExternalTemperatureIndication();
	
	if(local_ExtTemp_flag_c)
	{
		/* Copy the value into temporary variable */
		canio_RX_ExternalTemperature_c = IlGetRxExternalTemperature();
	}
	else
	{
		/*Signal Not Received */
	}
}

/**************************************************************************************************
 * Function Name:		CSWM_BatVolt_ManIndFunc
 * Called In:			canF_CyclicManIndFunc (10ms Time Slice)
 * 
 * Input:				Indication Flag and Info for System battery voltage Data.
 * 
 * Output:				Battery voltage will be stored in global variable.
 * 						Used for main_CSWM_Status and functionality Logic.
 * 
 * Description:			Looks for Indication Flags. If sets, will receive Information from CAN Bus.
 * 						And stores the Information in Variable, 
 * 						If not in range outputs will not trigger.
 ***************************************************************************************************/
void   CSWM_BatVolt_ManIndFunc(void)
{
	unsigned char local_BatVolt_flag_c;
	
	local_BatVolt_flag_c = IlGetClrBatteryVoltageLevelIndication();

	if(local_BatVolt_flag_c)
	{
		
		canio_RX_BatteryVoltageLevel_c = IlGetRxBatteryVoltageLevel();
		/* MKS 81842: Converting back to 100mv */
		canio_RX_BatteryVoltageLevel_c = (u_8Bit)( (canio_RX_BatteryVoltageLevel_c * 16) / 10);
	}
	else
	{
		/*Signal Not Received */
	}
}


/**************************************************************************************************
 * Function Name:		CSWM_ODO_ManIndFunc
 * Called In:			canF_CyclicManIndFunc (10ms Time Slice)
 * 
 * Input:				Indication Flag and Info for TotalOdo Data.
 * 
 * Output:				Odometer will be stored in global variable.
 * 						Used for Remote Start Logic.
 * 
 * Description:			Looks for Indication Flags. If sets, will receive Information from CAN Bus.
 * 						And stores the Information in Variable, will be used for Diagnostic purposes.
 ***************************************************************************************************/
void   CSWM_ODO_ManIndFunc(void)
{
	union long_char  temp_TotalOdo_l;              
	unsigned char local_TotalOdo_flag_c;
	
	local_TotalOdo_flag_c = IlGetClrTotalOdometerIndication();
	
	if(local_TotalOdo_flag_c)
	{
		/*Get the Total ODO Meter value to the Local Working variable */
		/*CDA Conversion Issue. They are looking for 0.1 factor. So multiplying the received value with 10 */
		temp_TotalOdo_l.l = ( (unsigned long) IlGetRxTotalOdometer() * 10);
		
		canio_RX_TotalOdo_c[0] = temp_TotalOdo_l.lw.hbyte;
		canio_RX_TotalOdo_c[1] = temp_TotalOdo_l.lw.lbyte;
		canio_RX_TotalOdo_c[2] = temp_TotalOdo_l.lw.llbyte;
	}
	else
	{
		/*Signal Not Received */
	}
		
}

/**************************************************************************************************
 * Function Name:		CSWM_CFG_RQ_ManIndFunc
 * Called In:			canF_CyclicManIndFunc (10ms Time Slice)
 * 
 * Input:				Indication Flag and Info for CFG_RQ Data.
 * 
 * Output:				CFG_Feature and CFG_STatus Data will be stored in global variable.
 * 						Used for Remote Start Logic.
 * 						CFG_Feature -> Defines the Remote Start value
 * 						CFG_Stat -> Defines whether it is enabled or disabled.	
 * 
 * Description:			Looks for Indication Flags. If sets, will receive Information from CAN Bus.
 * 						And stores the Information in Variable, will be used for Remote Start Logic.
 ***************************************************************************************************/
void   CSWM_CFG_RQ_ManIndFunc(void)
{
	unsigned char local_CFG_RQ_flag_c;
	
	local_CFG_RQ_flag_c = IlGetClrCFG_FeatureCntrlIndication();
	local_CFG_RQ_flag_c |= IlGetClrCFG_STAT_RQCntrlIndication();
	
	if(local_CFG_RQ_flag_c)
	{
		/* Get the Remote start CFG Feature from Bus when ever it changes */
		canio_RX_Rem_CFG_FEATURE_c = IlGetRxCFG_FeatureCntrl();
		/* Get the Remote start CFG RQ from Bus when ever it changes */
		canio_RX_Rem_CFG_STAT_RQ_c = IlGetRxCFG_STAT_RQCntrl();
	}
	else
	{
		/*Signal Not Received */
	}
}


/**************************************************************************************************
 * Function Name:		CSWM_StW_Actn_Rq_ManIndFunc
 * Called In:			canF_CyclicManIndFunc (10ms Time Slice)
 * 
 * Input:				Indication Flag and Info for StW_Actn_Rq Data.
 * 
 * Output:				Steering Wheel Temp and Sens Fault Data will be stored in global variable.
 * 						Used in StWheel_Heating module.
 * 
 * Description:			Looks for Indication Flags. If sets, will receive Information from CAN Bus.
 * 						And stores the Information in Variable, will be used in other modules.
 * 	
 ***************************************************************************************************/
void   CSWM_StW_Actn_Rq_ManIndFunc(void)
{

	unsigned char local_StW_Actn_Rq_flag_c;
	
	local_StW_Actn_Rq_flag_c = IlGetClrStW_TempSens_FltStsIndication();
	local_StW_Actn_Rq_flag_c |= IlGetClrStW_TempStsIndication();
	
	if(local_StW_Actn_Rq_flag_c)
	{
	/* Get the Steering Wheel Temp */
	canio_RX_StW_TempSts_c = IlGetRxStW_TempSts();
	/* Get the Steering Wheel Sensor Fault */
	canio_RX_StW_TempSens_FltSts_c = IlGetRxStW_TempSens_FltSts();
	}
	else
	{
		/*Signal Not Received */
	}
}

/**************************************************************************************************
 * Function Name:		CSWM_EngineSts_ManIndFunc
 * Called In:			canF_CyclicManIndFunc (10ms Time Slice)
 * 
 * Input:				Indication Flag for EngineStatus Data.
 * 
 * Output:				EngineStatus Data will be stored in global variable.
 * 						USAGE: To be Defined.
 * 
 * Description:			Looks for Indication Flags. If sets, will receive Information from CAN Bus.
 * 						And stores the Information in Variable, will be used in other modules.
 * 	
 ***************************************************************************************************/
void   CSWM_EngineSts_ManIndFunc(void)
{
	unsigned char local_EngineSts_flag_c;
	
	local_EngineSts_flag_c = IlGetClrEngineStsIndication();
	
	if(local_EngineSts_flag_c)
	{
		/* Get THIS SIGNAL TO LOCAL */
		canio_RX_EngineSts_c = IlGetRxEngineSts();
	}
	else
	{
		/*Signal Not Received */
	}
}


/**************************************************************************************************
 * Function Name:		CSWM_EngineSpeedValidData_ManIndFunc
 * Called In:			canF_CyclicManIndFunc (10ms Time Slice)
 * 
 * Input:				Indication Flags for EngineSpeed Valid Data.
 * 
 * Output:				EngineSpeedValid Data will be stored in global variable.
 * 						USAGE: To be Defined.
 * 
 * Description:			Looks for Indication Flags. If sets, will receive Information from CAN Bus.
 * 						And stores the Information in Variable, will be used in other modules.
 * 	
 ***************************************************************************************************/
void   CSWM_EngineSpeedValidData_ManIndFunc(void)
{
	unsigned char local_EngSpeed_flag_c;

	local_EngSpeed_flag_c = IlGetClrEngineSpeedValidDataIndication();
	
	if(local_EngSpeed_flag_c)
	{
	/* Get THIS SIGNAL TO LOCAL */
	canio_RX_EngineSpeedValidData_c = IlGetRxEngineSpeedValidData();
	}
	else
	{
		/*Signal Not Received */
	}
}

/**************************************************************************************************
 * Function Name:		CSWM_CFG_DATA_RESP_ManIndFunc
 * Called In:			canF_CyclicManIndFunc (10ms Time Slice)
 * 
 * Input:				Indication Flags for EngineSpeed Valid Data.
 * 
 * Output:				EngineSpeedValid Data will be stored in global variable.
 * 						USAGE: To be Defined.
 * 
 * Description:			Looks for Indication Flags. If sets, will receive Information from CAN Bus.
 * 						And stores the Information in Variable, will be used in other modules.
 * 	
 ***************************************************************************************************/
void   CSWM_CFG_DATA_RESP_ManIndFunc(void)
{
	unsigned char local_Config_c;

	//local_EngSpeed_flag_c = IlGetClrEngineSpeedValidDataIndication();
	local_Config_c = IlGetClrDigit_01Indication();
	
	if(local_Config_c)
	{
	/* Get THIS SIGNAL TO LOCAL */
	//canio_RX_EngineSpeedValidData_c = IlGetRxEngineSpeedValidData();
		IlPutTxDigit_01(ru_eep_PROXI_Cfg.s.PROXI_CRC.s.Digit1);
		IlPutTxDigit_02(ru_eep_PROXI_Cfg.s.PROXI_CRC.s.Digit2);
		IlPutTxDigit_03(ru_eep_PROXI_Cfg.s.PROXI_CRC.s.Digit3);
		IlPutTxDigit_04(ru_eep_PROXI_Cfg.s.PROXI_CRC.s.Digit4);
		IlPutTxDigit_05(ru_eep_PROXI_Cfg.s.PROXI_CRC.s.Digit5);
		IlPutTxDigit_06(ru_eep_PROXI_Cfg.s.PROXI_IDVeh.s.Digit6);
		IlPutTxDigit_07(ru_eep_PROXI_Cfg.s.PROXI_IDVeh.s.Digit7);
		IlPutTxDigit_08(ru_eep_PROXI_Cfg.s.PROXI_IDVeh.s.Digit8);
		IlPutTxDigit_09(ru_eep_PROXI_Cfg.s.PROXI_IDVeh.s.Digit9);
		IlPutTxDigit_10(ru_eep_PROXI_Cfg.s.PROXI_IDVeh.s.Digit10);
		IlPutTxDigit_11(ru_eep_PROXI_Cfg.s.PROXI_IDVeh.s.Digit11);
		
		CanTransmit(CanTxCFG_DATA_CODE_RSP_CSWM);
	}
	else
	{
		/*Signal Not Received */
	}
}

/**************************************************************************************************
 * Function Name:		CSWM_VIN_ManIndFunc
 * Called In:			canF_CyclicManIndFunc (10ms Time Slice)
 * 
 * Input:				Indication Flags for VIN_MSG and VIN_DATA.
 * 						Stored VIN Information from EEPROM.
 * 
 * Output:				If conditions met, sets "update_original_vin_b", used in canLF_WriteOriginalVIN
 * 
 * Description:			Looks for Indication Flags. If sets, will receive Information from CAN Bus.
 * 						Then Checks for Identical Information for 10 times.	
 * 						After 10 times period, reads already stored value in EEPROM.
 * 						If the Info received from Bus and EEP values are different, sets 
 * 						"update_original_vin_b" flag. Else None.
 ***************************************************************************************************/
void  CSWM_VIN_ManIndFunc (void)
{

	unsigned char local_buffer[7],local_VIN_DATA[7],local_VIN_MSG_flag_c;
	unsigned char canio_Rx_VIN_MSG_c;
	
	local_VIN_MSG_flag_c = IlGetClrVIN_MSGIndication();
	
	if(local_VIN_MSG_flag_c)
	{
		canio_Rx_VIN_MSG_c = IlGetRxVIN_MSG();
	
		IlGetRxVIN_DATA(local_VIN_DATA);
		/* Re arrange the Bytes. For some reason only for this VIN retreiving data in reverse */
		local_buffer[0] = local_VIN_DATA[6];
		local_buffer[1] = local_VIN_DATA[5];
		local_buffer[2] = local_VIN_DATA[4];
		local_buffer[3] = local_VIN_DATA[3];
		local_buffer[4] = local_VIN_DATA[2];
		local_buffer[5] = local_VIN_DATA[1];
		local_buffer[6] = local_VIN_DATA[0];

	}
	else
	{
		/*Signal Not Received */
	}

	
	if(canio_Rx_VIN_MSG_c == 0x00)
	{
		
		if (!memcmp((canio_CurrentVIN), (&local_buffer[0]),7) )
		{
			//Both are equal do nothing
		}
		else
		{
			/* Values are different. Copy the first 7 byte information into current VIN */
			memcpy(canio_CurrentVIN, (&local_buffer[0]), 7);
			/* Got the new information. clear the counter to Zero */
			Canio_VehIdNumberCounter_c = 0;
		}
		
	}
	else if(canio_Rx_VIN_MSG_c == 0x01)
	{
		
		if (!memcmp((canio_CurrentVIN+7), (&local_buffer[0]),7) )
		{
			//Both are equal do nothing
		}
		else
		{
			/* Values are different. Copy the next 7 byte information into current VIN */
			memcpy((canio_CurrentVIN+7), (&local_buffer[0]), 7);
			/* Got the new information. clear the counter to Zero */
			Canio_VehIdNumberCounter_c = 0;
		}


	}
	else if(canio_Rx_VIN_MSG_c == 0x02)
	{
		
		if (!memcmp((canio_CurrentVIN+14), (&local_buffer[0]), 3))
		{
			/* We need to make sure atleast for 10cycles the VIN number received from bus is identical */
			Canio_VehIdNumberCounter_c++;
			if (Canio_VehIdNumberCounter_c >= VIN_MAX_COUNT_K)
			{
				//Clear the Counter
				Canio_VehIdNumberCounter_c = 0;
				/* Time to update the Original vin */
				/* Got the same VIN for 10 successive times.                                               */
				EE_BlockRead(EE_VIN_ORIGINAL, (u_8Bit*)&canio_Vehicle_ID_Number[0]);
				/* If the Current VIN from Shadow is not equal to the value from Bus, we update the Current VIN information to Shadow */
				if (memcmp((canio_Vehicle_ID_Number), canio_CurrentVIN, 17))
				{
					/* Time to update the original VIN*/
					update_original_vin_b = 1;
				}
			}
		}
		else
		{
			/* Values are different. Copy the next 7 byte information into current VIN */
			memcpy((canio_CurrentVIN+14), (&local_buffer[0]), 3);
			/* Got the new information. clear the counter to Zero */
			Canio_VehIdNumberCounter_c = 0;
		}

	}
	else
	{
		
	}
	
//	unsigned char* local_buffer;
	
//	switch ( canio_Rx_VIN_MSG_c ) {
//
//	/* First 7 byte VIN comparision and storing */
//	case 0x00:
//	{
//		IlGetRxVIN_DATA(local_buffer);
//		/* compare the first 7 bytes of latest VIN received from bus to the current VIN value in RAM. If both values are equal do nothing. Else copy the latest value into the currentVIN variable the first 7 byte info */
//		if (/*!memcmp((canio_CurrentVIN), (local_buffer),7)*/1 )
//		{
//			//Both are equal do nothing
//		}
//		else
//		{
//			/* Values are different. Copy the first 7 byte information into current VIN */
//			memcpy(canio_CurrentVIN, (local_buffer), 7);
//			/* Got the new information. clear the counter to Zero */
//			Canio_VehIdNumberCounter_c = 0;
//		}
//		break;
//	}

//	/* Second 7 byte VIN comparision and storing */
//	case 0x01:
//	{
//		IlGetRxVIN_DATA(local_buffer);
//		/* compare the next 7 bytes of latest VIN received from bus to the current VIN value in RAM. If both values are equal do nothing. Else copy the latest value into the currentVIN variable the valus 8 to 14 byte info */
//		if (!memcmp((canio_CurrentVIN+7), (local_buffer),7) )
//		{
//			//Both are equal do nothing
//		}
//		else
//		{
//			/* Values are different. Copy the next 7 byte information into current VIN */
//			memcpy((canio_CurrentVIN+7), (local_buffer), 7);
//			/* Got the new information. clear the counter to Zero */
//			Canio_VehIdNumberCounter_c = 0;
//		}
//		break;
//	}
//
//	/* Remaining 3 byte VIN comparision and storing */
//	case 0x02:
//	{
//		IlGetRxVIN_DATA(local_buffer);
//		/* compare the remaining 3 bytes of latest VIN received from bus to the current VIN value in RAM. If both values are equal do nothing. Else copy the latest value into the currentVIN variable the valus 15 to 17 byte info */
//		if (!memcmp((canio_CurrentVIN+14), (local_buffer), 3))
//		{
//			/* We need to make sure atleast for 10cycles the VIN number received from bus is identical */
//			Canio_VehIdNumberCounter_c++;
//			if (Canio_VehIdNumberCounter_c >= VIN_MAX_COUNT_K)
//			{
//				//Clear the Counter
//				Canio_VehIdNumberCounter_c = 0;
//				/* Time to update the Original vin */
//				/* Got the same VIN for 10 successive times.                                               */
//				/* From now on we can update Original vin, If not updated with in 40 miles of Odo reading. */
//				update_original_vin_b = 1;
//				/* Lint Warning 545: Suspicious use of & */
//				//EE_BlockRead(EE_VIN_CURRENT, (u_8Bit*)&canio_Vehicle_ID_Number);
//				EE_BlockRead(EE_VIN_ORIGINAL, (u_8Bit*)&canio_Vehicle_ID_Number[0]);
////					FlashF_ReadEEPROMMirror(FlashM_MakeAddress(mirror_VIN_Current), 17, canio_Vehicle_ID_Number );
//				/* If the Current VIN from Shadow is not equal to the value from Bus, we update the Current VIN information to Shadow */
//				if (memcmp((canio_Vehicle_ID_Number), canio_CurrentVIN, 17))
//				{
//					/* Time to update the Current vin */
//					update_current_vin_b = 1;
//				}
//			}
//		}
//		else
//		{
//			/* Values are different. Copy the next 7 byte information into current VIN */
//			memcpy((canio_CurrentVIN+14), (local_buffer), 3);
//			/* Got the new information. clear the counter to Zero */
//			Canio_VehIdNumberCounter_c = 0;
//		}
//		break;
//	}
//	default:
//	{
//		/* Nothing to do for default case */
//	}
//  }
}
