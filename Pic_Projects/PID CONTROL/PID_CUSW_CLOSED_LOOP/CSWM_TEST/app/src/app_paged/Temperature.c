/********************************************************************************/
/*                        A U T H O R   I D E N T I T Y							*/
/********************************************************************************/
/* Initials      Name                   Company									*/
/* --------      --------------------   --------------------------------------- */ 
/* CK            Can Kulduk             Continental Automotive Systems			*/
/* HY            Harsha Yekollu         Continental Automotive Systems          */
/* FU            Fransisco Uribe        Continental Automotive Systems          */
/********************************************************************************/


/********************************************************************************/
/*                   CHANGE HISTORY for TEMPERATURE MODULE                      */
/********************************************************************************/
/* 01.22.2007 CK - Module created                                               */
/* 03.07.2008 HY - Module copied from CSWM MY09 project                       	*/
/* 04-10-2008 HY - MY09 Code added                                          	*/
/* 04.11.2008 HY - Updated new Micro MUX/AD selection                       	*/
/* 08.25.2008 HY - Added EE_BlockRead call at Init to read calibs from eep  	*/
/* 08.25.2008 HY - Added BlockRead/Write calls to set/clear PCB Overtemp fns 	*/  
/* 09.05.2008 CK - Added function TempF_U12S_ADC                                */
/* 10.22.2008 CK - Enabled NTC Supply Voltage Internal Fault (Occurance)        */
/*               - Commented PCB STB and PCB STG fault codes (not used)         */
/*               - Modified TempF_PCB_NTC_Flt_Monitor function                  */
/* 10.28.2008 CK - Added Bernhard's NtcAd to CanTemp conversion code         	*/
/* 12.03.2008 CK - Updated comments regarding to new PCB Over Temperature       */
/*                 Thresholds.                                                  */
/* 01.22.2009 CK - PC-Lint fixes. No complaints from PC-Lint after fixes.       */
/* 04.02.2009 CK - Moved declaration of temperature_cal_s into header file in   */
/*                 order to support $22 02 AF (Read PCB Temperature thresholds  */
/*                 diagnostic service).                            			    */
/********************************************************************************/


/*********************************************************************************/
/* Software Design Text                                                          */
/* -------                                                                       */
/* This module is used to monitor the heated seat NTC status. It also checks the */
/* PCB temperature in order to detect sensor fault or extreme temperature        */
/* conditions. 																	 */
/*********************************************************************************/


/********************************** @Include(s) **********************************/
#include "TYPEDEF.h"
#include "Temperature.h"
#include "canio.h"            
#include "AD_Filter.h"          
#include "Mcu.h"
#include "Port.h"
#include "Dio.h"
#include "Adc.h"
#include "BattV.h"
#include "Heating.h"
#include "Internal_Faults.h"
#include "FaultMemoryLib.h"
#include "mw_eem.h"
/*********************************************************************************/


/********************************* external data *********************************/
/* N
 * O
 * N
 * E */
/*********************************************************************************/


/*********************************** @ variables *********************************/
/* AtoD converted temperature readings */
u_16Bit temperature_PCB_ADC;                             // stores PCB temperature reading
u_8Bit  temperature_hs_thrmstr_ADC[TEMPERATURE_HS_LMT];  // stores heated seat thermistor readings

u_8Bit  temperature_HS_NTC_status[TEMPERATURE_HS_LMT];   // stores NTC fault status
u_8Bit  temperature_PCB_NTC_status_c;                    // stores on board PCB NTC status

u_8Bit  temperature_NTC_SupplyV_flt_mtr_cnt;             // Count to mature NTC Supply voltage count
u_8Bit  temperature_NTC_supplyV;            // stored most recent NTC supply voltage in 100mV units

u_16Bit temperature_HS_ontimeLmt_w;         // HS ontime limit before detecting NTC open at ambient < -10 C  

//u_8Bit  temperature_PCBsensor_data[2];      // PCB sensor error cleared data (contains 0,0 after POR) NOT USED

u_16Bit temperature_PCB_OT_cnt;             // PCB over temperature time counter (10 min to set DTC)
//u_16Bit PCB_OverTemp_OdoStamp_First_w;      //Holds ODO reading when first time OT happen
//u_16Bit PCB_OverTemp_OdoStamp_Latest_w;     //Holds ODO reading Latest Over temp happen
//u_8Bit  PCB_OverTemp_Counter_c;             //Holds No of times PCB Overtemp occured
//u_8Bit  PCB_OverTemp_Status_c;              //Holds PCB Over Temp Status 

/* Flags - NOT USED*/
//union char_bit temperature_flag1_c;
//#define over_temperature_flt_set_b    temperature_flag1_c.b.b0 // indicates over temperature fault is set
//#define PCB_sensor_error_updated_b    temperature_flag1_c.b.b1 // allows only one update per ignition cycle
//#define PCB_sensor_upt_cmpleted_b     temperature_flag1_c.b.b2 // PCB sensor update is completed
/*********************************************************************************/


/*********************************** @ Constants *********************************/
/* canTemp conversion defines */
#define T_REFPOINTS 	26				// Number of Temperature reference points
#define T_LIMIT  		250				// Returned result if ntcAD is 85 C or greater
#define T_N10			10				// Constant used in canTemp conversion
#define T_N2			2				// Constant used in canTemp conversion	
#define T_N1            1				// Constant used in canTemp conversion
#define T_N5			5               // Constant used in canTemp conversion
/*********************************************************************************/


/*********************************************************************************/
/* CAN temp convertion is designed by Bernhard Humpert							 */
/* Constant Array (Vector) for Temperature Lookup in Flash-Memory				 */
/*   ntc_read[0]																 */
/*   ntc_read[1]																 */
/*      ...																		 */
/*   ntc_read[i]																 */
/*      ...     																 */
/*   ntc_read[T_REFPOINTS-1]													 */	
/* Where the index is the CAN temperature / 10									 */
/* Index range 0 to 25 for CAN temperature 0 to 250								 */
/* CAN temperature = (deg C + 40) * 2											 */
/* Equivalent to temperature range -40 deg C to +85 deg C						 */
/*																				 */
/*  Standardized CSWM AD value for CSWM MY09 and CSWM MY11						 */	
/*  !!! only valid for the following NTC:        !!!                             */
/*  !!! B57861-S 103-A 18    Manufacturer: EPCOS !!!                             */
/*  Standardized CSWM AD value => CAN temp {8bits}, temp.<deg C>, [row index]    */
/*********************************************************************************/
const u_8Bit NTC_READ[T_REFPOINTS] = {
 	 /* ntcAd      canTemp  RealTemp   Index */
	 /* -----      -------  --------   ----- */
		229,	/*   0, 	-40.0, 		[00] */
		228,    /*  10, 	-35.0, 		[01] */
		227,    /*  20, 	-30.0, 		[02] */
		225,    /*  30, 	-25.0, 		[03] */
		223,    /*  40, 	-20.0, 		[04] */
		220,    /*  50, 	-15.0, 		[05] */
		218,    /*  60, 	-10.0, 		[06] */
		214,    /*  70, 	 -5.0, 		[07] */
		210,    /*  80,   	  0.0, 		[08] */
		204,    /*  90,   	  5.0, 		[09] */
		198,    /* 100,  	 10.0, 		[10] */
		191,    /* 110,  	 15.0, 		[11] */
		182,    /* 120,  	 20.0, 		[12] */
		173,    /* 130,  	 25.0, 		[13] */
		164,    /* 140,  	 30.0, 		[14] */
		153,    /* 150,  	 35.0, 		[15] */
		142,    /* 160,  	 40.0, 		[16] */
		131,    /* 170,  	 45.0, 		[17] */
		120,    /* 180,  	 50.0, 		[18] */
		109,    /* 190,  	 55.0, 		[19] */
		99,     /* 200,  	 60.0, 		[20] */
		89,     /* 210,  	 65.0, 		[21] */
		79,     /* 220,   	 70.0, 		[22] */
		71,     /* 230,  	 75.0, 		[23] */
		63,     /* 240,  	 80.0, 		[24] */
		56      /* 250,   	 85.0, 		[25] */
	/* out of range,   					[26] */
};
/*********************************************************************************/


/******************************** @Data Structures *******************************/
/* Declare of Temperature Calibration parameters */
temperature_cal_s temperature_s;

/* Temperature AD parameters */
main_ADC_prms temperature_AD_prms;

/* Over Temperature Failsafe Data stored in EEP */
struct temp_failsafe_data  Temp_FailSafe_prms;
/*********************************************************************************/


/*********************************************************************************/
/*                                TempF_Init                                     */
/*********************************************************************************/
/* Initiliazes temperature calibration parameters, heated seat NTC and PCB       */
/* status after each power on reset. 											 */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* main :                                                                        */
/*    TempF_Init()                                                               */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* EE_BlockRead(EEBlockLocation elocation, u_8Bit *pbydest)                	     */
/*                                                                               */
/*********************************************************************************/
@far void TempF_Init(void)
{
	/* Copy all Temperature Calibration data into local structure */
	EE_BlockRead(EE_TEMPERATURE_CALIB_DATA, (u_8Bit*)&temperature_s);
	
	/* Init PCB NTC status as good after each power on reset */
	temperature_PCB_NTC_status_c = PCB_STAT_GOOD;
	
	/* Init NTC status as good after each power on reset */
	temperature_HS_NTC_status[0] = NTC_STAT_GOOD;
	temperature_HS_NTC_status[1] = NTC_STAT_GOOD;
	temperature_HS_NTC_status[2] = NTC_STAT_GOOD;
	temperature_HS_NTC_status[3] = NTC_STAT_GOOD;
	/* Calculate the cycles in 320 timeslice for NTC open detection (ambient <-10 C) */
	temperature_HS_ontimeLmt_w = ( (temperature_s.NTCOpen_OutputOntime_lmt * TEMPERATURE_HS_1MIN) + (temperature_s.NTCOpen_OutputOntime_lmt>>1) );
}

/*********************************************************************************/
/*                                TempF_ADC_SetUp                                */
/*********************************************************************************/
/* Input:                                                                        */
/* temperature_thrmstr_sel_c: Heated seat NTC that we like to select             */
/*                                                                               */
/* This function updates the temperature AD parameters. This structure contains  */
/* the following data: 															 */
/*                                                                               */
/* typedef struct{                                                               */
/*     u_8Bit slctd_muxCH_c;         // selected mux channel                     */
/*     u_8Bit slctd_adCH_c;          // selected AD channel                      */
/*     u_8Bit slctd_mux_c;           // selected multiplexer                     */
/* }main_ADC_prms;                   // Analog to digital conversion structure   */
/*                                                                               */
/*                 Mux    MuxCH    AdCH                                          */
/* FL Seat NTC:     1       1        6                                           */
/* FR Seat NTC:     1       0        6                                           */
/* RL Seat NTC:     2       1        5                                           */
/* RR Seat NTC:     2       0        5                                           */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* TempF_Single_Thrmstr_ADC :                                                    */
/*    TempF_ADC_SetUp(u_8Bit temperature_thrmstr_sel_c)                          */
/*                                                                               */
/*********************************************************************************/
@far void TempF_ADC_SetUp(u_8Bit temperature_thrmstr_sel_c)
{
	/* Local variable(s) */
	u_8Bit temperature_hs_sel;    // selected heated seat
	
	/* Store the selected heated seat */
	temperature_hs_sel = temperature_thrmstr_sel_c;

	switch (temperature_hs_sel) {
		/* @Front Left Seat Thermistor */
		case FRONT_LEFT:
		{
			temperature_AD_prms.slctd_adCH_c  = CH6;     // selected AD channel is Channel 6
			temperature_AD_prms.slctd_mux_c   = MUX1;    // selected mux for front left seat thermistor is MUX1
			temperature_AD_prms.slctd_muxCH_c = CH1;     // selected mux channel is Channel 1
			break;
		}
		
		/* @Front Right Seat Thermistor */
		case FRONT_RIGHT:
		{
			temperature_AD_prms.slctd_adCH_c  = CH6;     // selected AD channel is Channel 6
			temperature_AD_prms.slctd_mux_c   = MUX1;    // selected mux for front right seat thermistor is MUX1
			temperature_AD_prms.slctd_muxCH_c = CH0;     // selected mux channel is Channel 0
			break;
		}
		
		/* @Rear Left Seat Thermistor */
		case REAR_LEFT:
		{
			temperature_AD_prms.slctd_adCH_c  = CH5;     // selected AD channel is Channel 5
			temperature_AD_prms.slctd_mux_c   = MUX2;    // selected mux for rear left seat thermistor is MUX2
			temperature_AD_prms.slctd_muxCH_c = CH1;     // selected mux channel is Channel 1

			break;
		}
		
		/* @Rear Right Seat Thermistor */
		case REAR_RIGHT:
		{
			temperature_AD_prms.slctd_adCH_c  = CH5;     // selected AD channel is Channel 5
			temperature_AD_prms.slctd_mux_c   = MUX2;    // selected mux for rear right seat thermistor is MUX2
			temperature_AD_prms.slctd_muxCH_c = CH0;     // selected mux channel is Channel 0
			break;
		}

		default :
		{
			/* Invalid seat request */
		}
	}
}

/*********************************************************************************/
/*                                TempF_PCB_ADC                                  */
/*********************************************************************************/
/* This function perform the AtoD conversion for PCB temperature reading.        */
/* Mux: 1                                                                        */
/* Mux channel: 7                                                                */
/* AD Channel: 6                                                                 */
/*                                                                               */
/* After conversion, it filteres the result and stores the filtered result in    */
/* temperature_PCB_ADC variable. 												 */
/* AD_mux_Filter structure which contains the following information.             */
/*                                                                               */
/* typedef struct{                                                               */
/*     u_16Bit unflt_rslt_w;    $/$/ unfiltered result                           */
/*     u_16Bit avg_w;           $/$/ average                                     */
/*     u_16Bit flt_rslt_w;      $/$/ filtered result                             */
/* }AD_Filter_s;                $/$/ Filter structure                            */
/*                                                                               */
/* AD_Filter_s AD_mux_Filter[MUX_LMT][CH_LMT];  // one for each mux. Each mux    */
/* has 8 channels (2x8).       													 */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* main (160ms timeslice)                                                 		 */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* ADF_AtoD_Cnvrsn(u_8Bit AD_Filter_supply)                                      */
/* Dio_WriteChannelGroup( ChannelGroupIdPtr, Level)                              */
/* ADF_Str_UnFlt_Mux_Rslt(u_8Bit AD_Filter_feed,u_8Bit AD_Filter_mux_feed)       */
/* ADF_Mux_Filter(u_8Bit AD_Filter_slctd_mux,u_8Bit AD_Filter_mux_ch)            */
/*                                                                               */
/*********************************************************************************/
@far void TempF_PCB_ADC(void)
{
	/* Select mux channel 7 */
	Dio_WriteChannelGroup(MuxSelGroup, CH7);
	
	/* AtoD conversion routine for muxed feedbacks (AD channel:6 Mux channel: 7) */
	ADF_AtoD_Cnvrsn(CH6);
	
	/* Conversion is completed. Store the unfiltered mux result */
	ADF_Str_UnFlt_Mux_Rslt(CH6, CH7);
	
	/* Filter the muxed AD reading for - mux 1 channel 7 */
	ADF_Mux_Filter(MUX1, CH7);
	
	/* Store filtered PCB Temperature reading */
	temperature_PCB_ADC = AD_mux_Filter[MUX1][CH7].flt_rslt_w;
}

/*********************************************************************************/
/*                                TempF_Single_Thrmstr_ADC                       */
/*********************************************************************************/
/* Input:                                                                        */
/* temperature_sel_c: Selected heated seat NTC                                   */
/*                                                                               */
/* This function performs the AtoD conversion for the selected NTC.              */
/* After conversion, result is filtered and stored in AD_mux_Filter structure.   */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* TempF_Monitor_ADC :                                                           */
/*    TempF_Single_Thrmstr_ADC(u_8Bit temperature_thrmstr_sel_c)                 */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* ADF_AtoD_Cnvrsn(u_8Bit AD_Filter_supply)                                      */
/* ADF_Str_UnFlt_Mux_Rslt(u_8Bit AD_Filter_feed,u_8Bit AD_Filter_mux_feed)       */
/* Dio_WriteChannelGroup( ChannelGroupIdPtr, Level)                              */
/* TempF_ADC_SetUp(u_8Bit temperature_thrmstr_sel_c)                             */
/*                                                                               */
/*********************************************************************************/
@far void TempF_Single_Thrmstr_ADC(u_8Bit temperature_sel_c)
{
	/* Local variable(s) */
	u_8Bit temperature_pick;    // Selected Thermistor
	
	/* Store the thermistor pick */
	temperature_pick = temperature_sel_c;
	
	/* Setup for AD conversion (select AD channel, mux channel etc.) */
	TempF_ADC_SetUp(temperature_pick);
	
	/* Choice mux channel */
	Dio_WriteChannelGroup(MuxSelGroup, temperature_AD_prms.slctd_muxCH_c);
	
	/* AtoD conversion for muxed Thermistor feedback */
	ADF_AtoD_Cnvrsn(temperature_AD_prms.slctd_adCH_c);
	
	/* Conversion is completed. Store the unfiltered mux result */
	ADF_Str_UnFlt_Mux_Rslt(temperature_AD_prms.slctd_adCH_c, temperature_AD_prms.slctd_muxCH_c);
}

/*********************************************************************************/
/*                            TempF_Stndrdz_Trmstr_Rdg                           */
/*********************************************************************************/
/* Input:                                                                        */
/* temperature_mux: Selected mux                                                 */
/* temperature_mux_ch: Selected mux channel                                      */
/*                                                                               */
/* This function normalizes the NTC reading so the voltage fluctuation           */
/* (temperature_U12s_local_w) does not effect the NTC reading results.           */
/*                                                                               */
/* It takes the 10-bit unfiltered AD reading (temperature_AD_ulf_w) for the      */
/* selected NTC and performs the following:                                      */
/*                                                                               */
/* Normalized NTC reading = 													 */
/* ( (temperature_AD_ulf_w*64) / (temperature_U12s_local_w/4) )                  */
/*                                                                               */
/* Note: minimum allowed temperature_U12s_local_w: 259 LSB ~ 5 V                 */
/*                                                                               */
/* ********************************                                              */
/* Theoritical Worst Case Analysis:                                              */
/*                                                                               */
/* temperature_AD_ulf_w * 64: makes NTC AD result 16 bit (max: 1023*64 = 65472)  */
/* temperature_U12s_local_w/4: makes U12S voltage  8-bit (min allwd: 259/4 = 64) */
/*                                                                               */
/* Normalized NTC reading = 1023 (theoritical worst case value 10-bit): For this */
/* case NTC reading will be set to 255 (Maximum limit for 8-bit filtered result) */
/* ********************************                                              */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* TempF_Monitor_ADC :                                                           */
/* TempF_Stndrdz_Trmstr_Rdg(u_8Bit slctd_mux,u_8Bit slctd_mux_ch)                */
/*                                                                               */
/*********************************************************************************/
@far void TempF_Stndrdz_Trmstr_Rdg(u_8Bit temperature_mux, u_8Bit temperature_mux_ch)
{
	/* Local variable(s) */
	u_8Bit temperature_slctd_mux;             // selected mux
	u_8Bit temperature_slctd_mux_ch;          // selected mux channel
	u_16Bit temperature_stndrdz_thrmstr_w;    // local variable for standardized thermistor reading
	u_16Bit temperature_AD_ulf_w;             // local variable for unfiltered AD rdg
	u_16Bit temperature_U12s_local_w;         // local variable for unfiltered U12S
	
	/* Store selected mux and mux channel */
	temperature_slctd_mux = temperature_mux;
	temperature_slctd_mux_ch = temperature_mux_ch;
	temperature_AD_ulf_w = AD_mux_Filter[temperature_slctd_mux][temperature_slctd_mux_ch].unflt_rslt_w;
	temperature_U12s_local_w = AD_Filter_set[CH0].unflt_rslt_w;
	
	/* standart thermistor = (Thermistor Rdg*64) / (U12S/4) */
	temperature_stndrdz_thrmstr_w = ( (temperature_AD_ulf_w<<6) / (temperature_U12s_local_w>>2) );
	
	/* Check result against our maximum threshold */
	if (temperature_stndrdz_thrmstr_w <= TEMPERATURE_NTC_LMT) {
		/* Store back the standardized thermistor reading back into unfiltered value */
		AD_mux_Filter[temperature_slctd_mux][temperature_slctd_mux_ch].unflt_rslt_w = temperature_stndrdz_thrmstr_w;
	}
	else {
		/* Set the NTC reading to our maximum limit (255) */
		AD_mux_Filter[temperature_slctd_mux][temperature_slctd_mux_ch].unflt_rslt_w = TEMPERATURE_NTC_LMT;
	}
}

/*********************************************************************************/
/*                            TempF_Chck_NTC_SupplyV                             */
/*********************************************************************************/
/* This function checks valid NTC supply voltage (U12S) in order to determine    */
/* reliable NTC readings for the heated seats. 									 */
/*                                                                               */
/* If U12S reading is <= 5.0V (50 100mV) it waits 2 seconds before maturing the  */
/* internal fault (byte 2, bit 2).               								 */
/*                                                                               */
/* If U12S reading is > 5.0V it checks if the internal fault bit is set.         */
/* If set: it clears this bit (Internal fault goes to stored if no other bits    */
/* are set - occurance)                            								 */
/* Else: NTC supply voltage is valid (normal operation). Clears the occurance if */
/* necassary.                                   								 */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* TempF_Monitor_ADC :                                                           */
/*    TempF_Chck_NTC_SupplyV()                                                   */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* IntFlt_F_Update(u_8Bit Byte,u_8Bit Bit_mask,Int_Flt_Op_Type_e intFlt_Op)      */
/* u_8Bit BattVF_AD_to_100mV_Cnvrsn(u_16Bit BattV_AD_value)                      */
/* EE_BlockRead((EEBlockLocation elocation, u_8Bit *pbydest) 					 */ 
/*                                                                               */
/*********************************************************************************/
@far void TempF_Chck_NTC_SupplyV(void)
{
	/* Convert NTC supply voltage reading to 100mV units */
	temperature_NTC_supplyV = BattVF_AD_to_100mV_Cnvrsn(AD_Filter_set[CH0].flt_rslt_w);
	
	/* Read the latest status of the internal fault byte and store it */
	EE_BlockRead(EE_INTERNAL_FLT_BYTE2, (u_8Bit*)&intFlt_bytes[TWO]);
	
	/* Check NTC Supply Voltage (Is it good ?) */
	if (temperature_NTC_supplyV > temperature_s.NTC_Supply_V_Low_Thrshld) {
		
		/* Check if we have to clear the NTC supply voltage internal occurance bit */
		if ( (intFlt_bytes[TWO] & NTC_SUPPLY_LOW_MASK) == NTC_SUPPLY_LOW_MASK ) {
			
			/* Clear NTC supply fault */
			IntFlt_F_Update(TWO, CLR_NTC_SUPPLY_LOW, INT_FLT_CLR);
			
			/* Clear internal fault flag */
			NTC_Supply_Voltage_Flt_b = FALSE;
			
			/* NTC supply voltage fault mature count is cleared */
			temperature_NTC_SupplyV_flt_mtr_cnt = 0;
		}
		else {
			/* NTC supply voltage fault is not set (normal operation) */
		}

	}
	else {
		/* No. Check fault mature time (2 seconds) */
		if ( (temperature_NTC_SupplyV_flt_mtr_cnt <= TEMPERATURE_FLT_MTR_TM) && (NTC_Supply_Voltage_Flt_b == FALSE) ) {
			
			/* Increment the fault mature time */
			temperature_NTC_SupplyV_flt_mtr_cnt++;
		}
		else {
			/* Check if we already updated this occurance */
			if ( (intFlt_bytes[TWO] & NTC_SUPPLY_LOW_MASK) == NTC_SUPPLY_LOW_MASK) {
				/* Already updated the occurance */
			}
			else {
				/* Update known faults */
				IntFlt_F_Update(TWO, NTC_SUPPLY_LOW_MASK, INT_FLT_SET);
				
				/* Set internal fault flag */
				NTC_Supply_Voltage_Flt_b = TRUE;
				
				/* Clear fault mature count */
				temperature_NTC_SupplyV_flt_mtr_cnt = 0;
			}
		}
	}
}

/****************************************************************************************/
/* Function Name: @far void TempF_U12S_ADC(void)									    */
/* Type: Global Function																*/
/* Returns: None																		*/
/* Parameters: None																		*/
/* Description: This function is called in 2.5ms time slice								*/
/* to cyclically read and convert the U12S analog reading to digital value.				*/
/* Calls to: None																		*/
/* Calls from: main() - 2.5ms timeslice													*/
/*																						*/
/****************************************************************************************/
@far void TempF_U12S_ADC(void)
{
	/* @AtoD conversion for U12S (stored unfiltered U12S value) */
	ADF_AtoD_Cnvrsn(CH0);
	
	/* Store the unfiltered result for AD channel */
	ADF_Str_UnFlt_Rslt(CH0);
	
	/* Filter the U12S (NTC supply voltage) AD channel for check */
	ADF_Filter(CH0);	
}

/*********************************************************************************/
/*                            TempF_Monitor_ADC                                  */
/*********************************************************************************/
/* Cyclic function which is responsible of NTC readings for all heated seats.    */
/*                                                                               */
/* 1st: This function performs the AtoD conversion for U12S (NTC supply voltage) */
/* 2nd: It checks if NTC supply voltage is valid (U12S > 5.0V)                   */
/*                                                                               */
/* If U12S reading is <= 5.0V this means there is a problem with the NTC supply  */
/* voltage. In this case, we do not update the hs NTC readings and the hs NTC    */
/* status. The internal fault (occurance) is set. 								 */
/*                                                                               */
/* If U12S is valid, we perform the NTC reading for each heated seat. Each       */
/* reading is normalized to 8-bit (Voltage fluctuations are compensated). The    */
/* results are filtered and stored.                    							 */
/*                                                                               */
/* 3rd: It also disables the U12S to avoid (or minimize) the self heating affect.*/
/*                                                                               */
/* Note: U12S stays on for 40ms and it stays off for 120ms.                      */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* main :                                                                        */
/*    TempF_Monitor_ADC()                                                        */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* ADF_AtoD_Cnvrsn(u_8Bit AD_Filter_supply)                                      */
/* ADF_Str_UnFlt_Rslt(u_8Bit AD_Filter_control)                                  */
/* ADF_Mux_Filter(u_8Bit AD_Filter_slctd_mux,u_8Bit AD_Filter_mux_ch)            */
/* ADF_Filter(u_8Bit AD_Filter_ch)                                               */
/* TempF_Single_Thrmstr_ADC(u_8Bit temperature_thrmstr_sel_c)                    */
/* TempF_Chck_NTC_SupplyV()                                                      */
/* TempF_Stndrdz_Trmstr_Rdg(u_8Bit slctd_mux,u_8Bit slctd_mux_ch)                */
/* Dio_WriteChannel( ChannelId, Level)                                           */
/* 																				 */
/*********************************************************************************/
@far void TempF_Monitor_ADC(void)
{
	/* Local variable for loop iteration (4 thermistor readings) */
	u_8Bit i;    
	
	/* U12S AtoD convertion is performed in 2.5ms time slice for CSWM MY11 */
	/* Because U12S is also used in Steering Wheel fault detection.        */
	/* @AtoD conversion for U12S (stored unfiltered U12S value) */
	//ADF_AtoD_Cnvrsn(CH0);
	
	/* Store the unfiltered result for AD channel */
	//ADF_Str_UnFlt_Rslt(CH0);
	
	/* Filter the U12S (NTC supply voltage) AD channel for check */
	//ADF_Filter(CH0);
	
	/* Check NTC supply voltage */
	TempF_Chck_NTC_SupplyV();
	
	/* @Thermistor reading for each heated seat */
	/* Check if NTC supply voltage is o.k. */
	if ( ((intFlt_bytes[TWO] & NTC_SUPPLY_LOW_MASK) != NTC_SUPPLY_LOW_MASK) && (temperature_NTC_supplyV > temperature_s.NTC_Supply_V_Low_Thrshld) ) {

		for (i=0; i<TEMPERATURE_HS_LMT; i++) {
			
			/* Read the selected thermistor value */
			TempF_Single_Thrmstr_ADC(i);
			
			/* Standardize (Normalize) Thermistor reading against U12S and save it as unfiltered value */
			TempF_Stndrdz_Trmstr_Rdg(temperature_AD_prms.slctd_mux_c, temperature_AD_prms.slctd_muxCH_c);
			
			/* Filter the normalized thermistor reading */
			ADF_Mux_Filter(temperature_AD_prms.slctd_mux_c, temperature_AD_prms.slctd_muxCH_c);
			
			/* Store the filtered thermistor reading */
			temperature_hs_thrmstr_ADC[i] = (u_8Bit) AD_mux_Filter[temperature_AD_prms.slctd_mux_c][temperature_AD_prms.slctd_muxCH_c].flt_rslt_w;
		}
	}
	else {
		/* There is a problem with the NTC supply voltage. Do not update the hs NTC readings and status */
	}
	
	/* Check if we have to keep U12S on for Relay C (StWheel STG tests on Low Side Driver) */
	if(main_U12S_keep_ON_b == TRUE){		
		/* Let Relay Module know that we are ready for STG tests on Low side driver */
		main_kept_U12S_ON_b = TRUE;
	}else{
		/* Normal functinality. @Disable U12S to avoid self heating */
		Dio_WriteChannel(U12T_EN, STD_LOW);
		/* U12S is off */
		main_kept_U12S_ON_b = FALSE;
	}	
}

/*********************************************************************************/
/*                            TempF_HS_NTC_Flt_Monitor                           */
/*********************************************************************************/
/* This function monitors each of the heated seat NTC status. It is called       */
/* cyclically (after receiving the first NTC reading). 							 */
/*                                                                               */
/* It updates the each NTC status if and only if:                                */
/* 1. Ignition status is RUN                                                     */
/* 2. NTC supply voltage is valid (> 5.0V)                                       */
/*                                                                               */
/* Possible NTC status states are:                                               */
/* typedef enum{                                                                 */
/*     NTC_STAT_UNDEFINED=0,    // NTC fault status undefined (initial value)    */
/*     NTC_SHRT_TO_GND=1,       // NTC fault - short to ground                   */
/*     NTC_OPEN_CKT=2,          // NTC faullt - open circuit or short to battery */
/*     NTC_STAT_GOOD=3          // NTC status good                               */
/* }temperature_NTC_Stat_e;                                                      */
/*                                                                               */
/* There are two possible DTCs for each heated seat NTC (total 8 DTCs for four   */
/* heated seats).                               								 */
/* 1. NTC HIGH DTC (NTC_OPEN_CKT or short to battery)                            */
/* 2. NTC LOW DTC  (NTC_SHRT_TO_GND)                                             */
/*                                                                               */
/* NTC status is set to NTC_SHRT_TO_GND if NTC AD reading is < 10.               */
/* NTC status is set to NTC_OPEN_CKT if there is a short to battery (NTC AD      */
/* reading is set to 255).                            							 */
/*                                                                               */
/* NTC status is also set to NTC_OPEN_CKT:                                       */
/* NTC reading > NTC high threshold (226) and                                    */
/*                                                                               */
/* if: Ambient temperature is < -10C and heated seat has been turned on for at   */
/* least 2 minutes.                                								 */
/* else: (ambient temp. >= 10C) set NTC_OPEN_CKT right away.                     */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* main :                                                                        */
/*    TempF_HS_NTC_Flt_Monitor()                                                 */
/*                                                                               */
/*********************************************************************************/
@far void TempF_HS_NTC_Flt_Monitor(void)
{
	/* *** ATTENTION: Call this function after getting the heated seat NTC reading *** */
	u_8Bit j;    // for loop variable
	

	/* Check hs NTC Supply voltage status, ignition status, and CCN_RT timeout */
	if ( ((intFlt_bytes[TWO] & NTC_SUPPLY_LOW_MASK) != NTC_SUPPLY_LOW_MASK) &&
			(temperature_NTC_supplyV > temperature_s.NTC_Supply_V_Low_Thrshld) &&
			(canio_RX_IGN_STATUS_c == IGN_RUN) )
	{
		/* Monitor heated seat NTC faults */
		for (j=0; j<TEMPERATURE_HS_LMT; j++) {
			/* Check for short to ground fault (AD < 10) */
			if (temperature_hs_thrmstr_ADC[j] < temperature_s.HS_NTC_Lw_Thrshld)
			{
				/* Set NTC Short to ground fault */
				temperature_HS_NTC_status[j] = NTC_SHRT_TO_GND;
			}
			else {
				/* Check for short to battery fault in order to set NTC high DTC */
				if (temperature_hs_thrmstr_ADC[j] == TEMPERATURE_NTC_LMT)
				{
					/* Set NTC High fault (short to battery) */
					temperature_HS_NTC_status[j] = NTC_OPEN_CKT;
				}
				else
				{
					/* Check for open circuit fault (AD > 226) */
					if (temperature_hs_thrmstr_ADC[j] > temperature_s.HS_NTC_Hgh_Thrshld)
					{
						/* Check if ambient tempearture is less than -10 C (3000) */
						if (canio_RX_ExternalTemperature_c < TEMPERATURE_COLD_AMBIENT_LMT)
						{
							/* Check if the seat heater has been turned on for sufficient amount of time */
							if (hs_time_counter_w[j] > temperature_HS_ontimeLmt_w)
							{
								/* Set NTC High fault (Open circuit) */
								temperature_HS_NTC_status[j] = NTC_OPEN_CKT;
							}
							else
							{
								/* Wait Hs ontime delay to determine if NTC is working or not */
							}
						}
						else
						{
							/* Ambient temp. at least -10C. Set NTC High fault (Open circuit) */
							temperature_HS_NTC_status[j] = NTC_OPEN_CKT;
						}
					}
					else
					{
						/* NTC status is good */
						temperature_HS_NTC_status[j] = NTC_STAT_GOOD;
					}
				}
			}
		}
	}
	else
	{
		/* NTC supply voltage is not reliable or ignition status is other then run. Do not perform NTC fault detection. */
	}
	
}

/*********************************************************************************/
/*                            TempF_Get_NTC_Rdg                                  */
/*********************************************************************************/
/* Input:                                                                    	 */
/* temperature_hs_position: Selected heated seat NTC.                        	 */
/*                                                                           	 */
/* This function is called from heating module.                              	 */
/* This function returns the latest NTC reading of the selected heated seat. 	 */
/*                                                                           	 */
/* Calls from                                                                	 */
/* --------------                                                            	 */
/* hsF_HeatManager :                                                         	 */
/*    u_8Bit TempF_Get_NTC_Rdg(u_8Bit temperature_hs_position)               	 */
/*                                                                            	 */
/*********************************************************************************/
@far u_8Bit TempF_Get_NTC_Rdg(u_8Bit temperature_hs_position)
{
	/* Local variable(s) */
	u_8Bit temperature_hs_location;    //heated seat location
	
	/* Store heated seat location */
	temperature_hs_location = temperature_hs_position;
	
	/* Returns the temperature of the selected seat */
	return (temperature_hs_thrmstr_ADC[temperature_hs_location]);
}

/*********************************************************************************/
/*                            TempF_Set_PCB_OverTemp                             */
/*********************************************************************************/
/* This function is used to set the PCB over temperature internal bit            */
/* (Internal fault is not set) when PCB temperature exceeds (>=) 105C on the     */
/* measurement point. 															 */
/*                                                                               */
/* Note: This bit will be cleared when/if PCB temperature goes below 90C (after  */
/* exceeding 105).                                                               */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* TempF_PCB_NTC_Flt_Monitor :                                                   */
/*    TempF_Set_PCB_OverTemp()                                                   */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* EE_BlockRead(EEBlockLocation elocation, u_8Bit *pbydest)     				 */
/* FMemLibF_GetOdometer()                                                        */
/* EE_BlockWrite(EEBlockLocation elocation, u_8Bit *pbysrc)                      */
/* 																				 */
/*********************************************************************************/
@far void TempF_Set_PCB_OverTemp(void)
{
	/* Tracker Id $$47 */
	/* Local variables */ 
	u_16Bit TempOdo;	// each count is 16km. So, TempOdo = 5 = 5*16= 80km

//	EE_BlockRead(EE_PCB_OTEMP_ODOSTAMP_FIRST,(u_8Bit*)&PCB_OverTemp_OdoStamp_First_w);
//	EE_BlockRead(EE_PCB_OTEMP_ODOSTAMP_LATEST,(u_8Bit*)&PCB_OverTemp_OdoStamp_Latest_w);
//	EE_BlockRead(EE_PCB_OTEMP_COUNTER,(u_8Bit*)&PCB_OverTemp_Counter_c);
//	EE_BlockRead(EE_PCB_OVERTEMP_STAT,(u_8Bit*)&PCB_OverTemp_Status_c);
	
	EE_BlockRead(EE_OVER_TEMP_FAILSAFE,(u_8Bit*)&Temp_FailSafe_prms);
	
	/* Get the odo meter information */
	TempOdo = FMemLibF_GetOdometer();

	/* Check to see is the first time PCB Over Temp condition occured                                              */
	/* If yes, update the First time odo stamp along with latest odo stamp and increment the pcb over temp counter */
	/* Else only update the Latest odo stamp along with counter and status                                         */
	if (Temp_FailSafe_prms.PCB_OverTemp_OdoStamp_First_w == 0xFFFF) {
		
		/* First time we are seeing the PCB Over temperature condition. */
		/* Store the Odo stamp to mirror eeprom                         */
		Temp_FailSafe_prms.PCB_OverTemp_OdoStamp_First_w = TempOdo;
//		EE_BlockWrite(EE_PCB_OTEMP_ODOSTAMP_FIRST,(u_8Bit*)&PCB_OverTemp_OdoStamp_First_w);
	}
	else {
		
		/* First time odo got already updated. No need to change the first time odo stamp. */
		/* Update other variables for history */
	}
	
	/* Detected Over temperature condition.         */
	/* Store the Latest Odo stamp to mirror eeprom. */
	Temp_FailSafe_prms.PCB_OverTemp_OdoStamp_Latest_w = TempOdo;
//	EE_BlockWrite(EE_PCB_OTEMP_ODOSTAMP_LATEST,(u_8Bit*)&PCB_OverTemp_OdoStamp_Latest_w);

	/* Check to see over temperature counter reached max iterations 254. */
	/* If not Increment the counter. Else latch the value at 254.        */
	if (Temp_FailSafe_prms.PCB_OverTemp_Counter_c < OVER_TEMP_COUNTER_LMT) {
		
		/* Increment the PCB Over temp counter */
		Temp_FailSafe_prms.PCB_OverTemp_Counter_c++;
//		EE_BlockWrite(EE_PCB_OTEMP_COUNTER,(u_8Bit*)&PCB_OverTemp_Counter_c);
	}
	else {		
		/* Reached 254 times. do not increment the counter */
	}
	
	/* Set the PCB Over temp status to set */
	Temp_FailSafe_prms.PCB_OverTemp_Status_c = OVER_TEMP_SET;
//	EE_BlockWrite(EE_PCB_OVERTEMP_STAT,(u_8Bit*)&PCB_OverTemp_Status_c);
	EE_BlockWrite(EE_OVER_TEMP_FAILSAFE,(u_8Bit*)&Temp_FailSafe_prms);
	
}

/*********************************************************************************/
/*                            TempF_Clr_PCB_OverTemp                             */
/*********************************************************************************/
/* This function clears internal fault bit (which does not sets the internal     */
/* fault) when PCB temperature goes below 90C (after being above 105C). 		 */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* TempF_PCB_NTC_Flt_Monitor :                                                   */
/*    TempF_Clr_PCB_OverTemp()                                                   */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* EE_BlockRead(EEBlockLocation elocation, u_8Bit *pbydest)     				 */
/* EE_BlockWrite(EEBlockLocation elocation, u_8Bit *pbysrc)                      */
/*																			     */
/*********************************************************************************/
@far void TempF_Clr_PCB_OverTemp(void)
{
	/* Read PCB over temperature  status */
	EE_BlockRead(EE_OVER_TEMP_FAILSAFE,(u_8Bit*)&Temp_FailSafe_prms);

	/* Check to see if the over temp status is set. */                                                                                                         
	if (Temp_FailSafe_prms.PCB_OverTemp_Status_c == OVER_TEMP_SET) {
		
		/* Clear PCB Over temperature status */
		Temp_FailSafe_prms.PCB_OverTemp_Status_c = OVER_TEMP_CLEAR;
		/* Update emulated EEPROM because we are lower than PCB Over temp limit */
		EE_BlockWrite(EE_OVER_TEMP_FAILSAFE,(u_8Bit*)&Temp_FailSafe_prms);		
	}
	else {		
		/* Over temperature status is clear. Do nothing. */
	}
}

/*********************************************************************************/
/*                            TempF_Clr_PCB_Sensor_Flt (NOT USED)                */
/*********************************************************************************/
/* When PCB temperature status is good, this function is called to check whether */
/* we have to clear the PCB sensor fault. 										 */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* TempF_Chck_PCB_Sensor_Err :                                                   */
/*    TempF_Clr_PCB_Sensor_Flt()                                                 */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* IntFlt_F_Update(u_8Bit Byte,u_8Bit Bit_mask,Int_Flt_Op_Type_e intFlt_Op)      */
/* 																				 */
/*********************************************************************************/
//@far void TempF_Clr_PCB_Sensor_Flt(void)
//{
//	/* Check if we already clear the PCB Sensor HI fault */
//	if ( /*(mirror_eeprom_cals.internal_flt_bytes[TWO] & PCB_SENSOR_HI_MASK) == PCB_SENSOR_HI_MASK*/1) {
//		/* Update known faults */
//		IntFlt_F_Update(TWO, CLR_PCB_SENSOR_HI, INT_FLT_CLR);
//		
//		/* Clear internal fault flag */
//		PCB_Sensor_HI_b = FALSE;
//	}
//	else {
//		/* PCB Sensor High bit is not set. */
//	}
//	
//	/* Check if we already clear the PCB Sensor LO fault */
//	if ( /*(mirror_eeprom_cals.internal_flt_bytes[TWO] & PCB_SENSOR_LO_MASK) == PCB_SENSOR_LO_MASK*/1) {
//		/* Update known faults */
//		IntFlt_F_Update(TWO, CLR_PCB_SENSOR_LO, INT_FLT_CLR);
//		
//		/* Clear internal fault flag */
//		PCB_Sensor_LO_b = FALSE;
//	}
//	else {
//		/* PCB Sensor Low bit is not set. */
//	}
//}

/*********************************************************************************/
/*                            TempF_Chck_PCB_Sensor_Updt (NOT USED)              */
/*********************************************************************************/
/* This function is used to capture the PCB sensor error condition for 10        */
/* consecutive ignition cycles.                 								 */
/*                                                                               */
/* It updates the following only once per ignition cycle:                        */
/* PCB_sensor_err_occurance which indicates a consecutive ignition cycle PCB     */
/* error event.                              									 */
/* PCB_sensor_occurance_cnt which is used to count the PCB error until we reach  */
/* the ignition cycle limit (default 10). 										 */
/* Once we reach the ignition cycle limit, a PCB sensor error internal fault will*/
/* be matured.                          										 */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* main :                                                                        */
/*    TempF_Chck_PCB_Sensor_Updt()                                               */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* Flash_WriteEEPROMMirror(unsigned short address,unsigned char count,           */
/* unsigned char * buffer)                          							 */
/* 																				 */
/*********************************************************************************/
//@far void TempF_Chck_PCB_Sensor_Updt(void)
//{
//	/* Local variables */
//	u_8Bit wrt_rslt = 0;        // write result
//	
//	/* Check if update is completed (and necassary) - Perform this only once per ignition cycle */
//	if ( (PCB_sensor_error_updated_b == TRUE) && (PCB_sensor_upt_cmpleted_b == FALSE) ) {
//		/* PCB sensor error detected. Update Shadow RAM with occurance data. */
//		//wrt_rslt = FlashF_WriteEEPROMMirror(FlashM_MakeAddress(mirror_PCB_sensor_error), 2, (u_8Bit*)&temperature_s.PCB_sensor_err_occurance);
//		
//		/* Is Shadow RAM updated? */
//		if (/*wrt_rslt == SHADOW_WRITE_COMPLETE*/1) {
//			/* PCB Sensor error information update is completed for this ignition cycle. Lock this update for the rest of the ignition cycle. */
//			PCB_sensor_upt_cmpleted_b = TRUE;
//		}
//		else {
//			/* Try again in next execution cycle. */
//		}
//
//	}
//	else {
//		/* Update for this ignition cycle is either completed or not necassary */
//	}
//}

/*********************************************************************************/
/*                            TempF_Upt_PCB_Sensor_Err (NOT USED)                */
/*********************************************************************************/
/* This function is called cyclically to check if PCB sensor internal fault has  */
/* to be matured or not. 														 */
/*                                                                               */
/* The PCB sensor error internal fault is matured if:                            */
/* PCB sensor error is detected for 10 (default value) consecutive ignition      */
/* cycles.                   													 */
/* It is dematured if PCB temperature reading goes back to "good" range.         */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* main :                                                                        */
/*    TempF_Upt_PCB_Sensor_Err()                                                 */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* IntFlt_F_Update(u_8Bit Byte,u_8Bit Bit_mask,Int_Flt_Op_Type_e intFlt_Op)      */
/* 																				 */
/*********************************************************************************/
//@far void TempF_Upt_PCB_Sensor_Err(void)
//{
//	/* Check PCB sensor fault status and occurance count */
//	if ( ((temperature_PCB_NTC_status_c == PCB_SHRT_TO_BATT) || (temperature_PCB_NTC_status_c == PCB_SHRT_TO_GND)) && (temperature_s.PCB_sensor_occurance_cnt == IGN_CYCLE_LMT) ) {
//		/* Is Update necassary? */
//		if (/* ((mirror_eeprom_cals.internal_flt_bytes[TWO] & PCB_SENSOR_HI_MASK) != PCB_SENSOR_HI_MASK) && ((mirror_eeprom_cals.internal_flt_bytes[TWO] & PCB_SENSOR_LO_MASK) != PCB_SENSOR_LO_MASK) */1) {
//			/* Record PCB sensor fault accordingly */
//			if (temperature_PCB_NTC_status_c == PCB_SHRT_TO_BATT) {
//				/* Update sensor high fault */
//				IntFlt_F_Update(TWO, PCB_SENSOR_HI_MASK, INT_FLT_SET);
//				
//				/* Set internal fault flag */
//				PCB_Sensor_HI_b = TRUE;
//			}
//			else {
//				/* Update sensor low fault */
//				IntFlt_F_Update(TWO, PCB_SENSOR_LO_MASK, INT_FLT_SET);
//				
//				/* Set internal fault flag */
//				PCB_Sensor_LO_b = TRUE;
//			}
//
//		}
//		else {
//			/* Internal fault is already updated */
//		}
//
//	}
//	else {
//		/* Conditions are not meet in order to update the PCB sensor fault */
//	}
//}

/*********************************************************************************/
/*                            TempF_Clr_PCB_Err_Cnt (NOT USED)                   */
/*********************************************************************************/
/* This function is called only if PCB sensor occurance has been detected at     */
/* least once in previous ignition cycles.                                       */
/* It is called when PCB sensor status is good, so its purpose is to clear the   */
/* PCB sensor ocuurance data (consecutive occurance flag and occurance count). 	 */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* TempF_Chck_PCB_Sensor_Err :                                                   */
/*    TempF_Clr_PCB_Err_Cnt()                                                    */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* Flash_WriteEEPROMMirror(unsigned short address,unsigned char count,           */
/* unsigned char * buffer)                                                       */
/* 																				 */
/*********************************************************************************/
//@far void TempF_Clr_PCB_Err_Cnt(void)
//{
//	/* Local variables */
//	u_8Bit sensor_data_wrt_rslt = 0;          // clear sensor data write result
//	
//	/* Update Shadow RAM with cleared PCB sensor error data */
//	//sensor_data_wrt_rslt = FlashF_WriteEEPROMMirror(FlashM_MakeAddress(mirror_PCB_sensor_error), 2, (u_8Bit*)&temperature_PCBsensor_data[0]);
//	
//	/* Is Shadow RAM update complete? */
//	if (/*sensor_data_wrt_rslt == SHADOW_WRITE_COMPLETE*/1) {
//		/* Clear PCB sensor error data */
//		temperature_s.PCB_sensor_err_occurance = 0;
//		temperature_s.PCB_sensor_occurance_cnt = 0;
//	}
//	else {
//		/* Shadow update is not succesfull. Try again in next execution cycle. */
//	}
//}

/*********************************************************************************/
/*                            TempF_Chck_PCB_Sensor_Err (NOT USED)               */
/*********************************************************************************/
/* This function checks the PCB temperature sensor faults.                       */
/*                                                                               */
/* If PCB NTC status is set to PCB_SHRT_TO_BATT or PCB_SHRT_TO_GND for 10        */
/* consecutive ignition cycles, the internal fault (occurance) is set.           */
/* int_flt_byte2.b.b0    // PCB Sensor reading too high (Short to battery/open)  */
/* int_flt_byte2.b.b1    // PCB Sensor reading too low  (Short to ground)        */
/*                                                                               */
/* If PCB NTC status goes back to PCB_STAT_GOOD at any time, the internal fault  */
/* is cleared (Sensor fault occurance count that count 10 consecutive ignition   */
/*cycles is also cleared). 														 */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* TempF_PCB_NTC_Flt_Monitor :                                                   */
/*    TempF_Chck_PCB_Sensor_Err()                                                */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* TempF_Clr_PCB_Sensor_Flt()                                                    */
/* TempF_Clr_PCB_Err_Cnt()                                                       */
/* 																				 */
/*********************************************************************************/
//@far void TempF_Chck_PCB_Sensor_Err(void)
//{
//	/* Check PCB sensor fault status */
//	if ( ((temperature_PCB_NTC_status_c == PCB_SHRT_TO_BATT) || (temperature_PCB_NTC_status_c == PCB_SHRT_TO_GND)) && (PCB_sensor_error_updated_b == FALSE) ) {
//		/* Record PCB sensor error occurance */
//		temperature_s.PCB_sensor_err_occurance = TRUE;
//		
//		/* Check if we reached error occurance count limit */
//		if (temperature_s.PCB_sensor_occurance_cnt < IGN_CYCLE_LMT) {
//			/* No. Increment the occurance count */
//			temperature_s.PCB_sensor_occurance_cnt++;
//		}
//		else {
//			/* Sensor occurance count has reached the limit. We will set the PCB Sensor error internal fault */
//		}
//		
//		/* PCB sensor error info (occurance and count) for this ignition cycle has been updated */
//		PCB_sensor_error_updated_b = TRUE;
//	}
//	else {
//		/* Check PCB sensor status */
//		if (temperature_PCB_NTC_status_c == PCB_STAT_GOOD) {
//			/* Check if PCB Sensor internal faults needs to be cleared */
//			TempF_Clr_PCB_Sensor_Flt();
//			
//			/* Check if we have to clear the occurance data */
//			if ( /*(mirror_eeprom_cals.Temperature_data_s.PCB_sensor_err_occurance == 0) && (mirror_eeprom_cals.Temperature_data_s.PCB_sensor_occurance_cnt == 0)*/1 ) {
//				/* Occurance and its count is cleared */
//			}
//			else {
//				/* Clear occurance data */
//				TempF_Clr_PCB_Err_Cnt();
//			}
//		}
//		else {
//			/* Do not modify Internal PCB Sensor faults (this is Over temperature or undefined case) */
//		}
//	}
//}

/*********************************************************************************/
/*                            TempF_PCB_NTC_Flt_Monitor                          */
/*********************************************************************************/
/* This function monitors the PCB NTC status.                                    */
/*                                                                               */
/* Here are the available states for the PCB NTC status:                         */
/* typedef enum{                                                                 */
/*     PCB_NTC_STAT_UNDEFINED=0,    // PCB NTC Status undefined (initial value)  */
/*     PCB_SHRT_TO_BATT=1,          // PCB NTC Short to battery fault            */
/*     PCB_SHRT_TO_GND=2,           // PCB NTC Short to Ground fault             */
/*     PCB_STAT_OVERTEMP=3, // PCB NTC Over Temperature: temperature >= 105C     */
/*     PCB_STAT_GOOD=4      // PCB NTC status good: temperature <= 90C  		 */
/* }temperature_PCB_Stat_e;                                                      */
/*                                                                               */
/* temperature_PCB_NTC_status_c is set to PCB_STAT_OVERTEMP if PCB temperature   */
/* is >= 105C. After this event, temperature_PCB_NTC_status_c is not updated to  */
/* PCB_STAT_GOOD until PCB temperature goes below 90C. 							 */
/*                                                                               */
/* The PCB sensor fault is also managed in this function by placing a call to    */
/* TempF_Chck_PCB_Sensor_Err function.                                           */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* main :                                                                        */
/*    TempF_PCB_NTC_Flt_Monitor()                                                */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* u_8Bit BattVF_Get_Status(BattV_type_e Voltage_type)                           */
/* EE_BlockReadEEBlockLocation elocation, u_8Bit *pbydest)                       */
/* TempF_Clr_PCB_OverTemp()                                                      */
/* TempF_Set_PCB_OverTemp()                                                      */
/* 																				 */
/*********************************************************************************/
@far void TempF_PCB_NTC_Flt_Monitor(void)
{
	u_8Bit temperature_BatteryV_stat_c;    // local variable to store the local battery voltage status
	
	/* Store the local battery voltage status */
	temperature_BatteryV_stat_c = BattVF_Get_Status(BATTERY_V);
	
	/* Modified Implementation */
	/* Check if local battery voltage is in operation range */
	if(temperature_BatteryV_stat_c == V_STAT_GOOD){

		/* Tracker Id #47 */
		/* NEW THRESHOLDS starting with 08.46.00 (S0B) Release */
		/* Read over temperature status: Should be set if PCB temp >= (100 C) for 4 minutes */
//		EE_BlockRead(EE_PCB_OVERTEMP_STAT,(u_8Bit*)&PCB_OverTemp_Status_c);
		EE_BlockRead(EE_OVER_TEMP_FAILSAFE,(u_8Bit*)&Temp_FailSafe_prms);
		
		/* Is PCB in over temperature range? */
		if (temperature_PCB_ADC <= temperature_s.PCB_Set_OverTemp) {
			
			/* Check to see over temp status is already set. If already set do not process */
			if (Temp_FailSafe_prms.PCB_OverTemp_Status_c == OVER_TEMP_CLEAR) {
				
				/* Over temperature status is not set yet */
				/* Check if 4 minutes has elapsed before setting the over temperature status */
				if (temperature_PCB_OT_cnt <TEMPERATURE_PCB_OT_TM_LMT) {					
					/* Increment PCB over temperature counter */
					temperature_PCB_OT_cnt++;
				}
				else {					
					/* Set PCB NTC over temperature fault */
					temperature_PCB_NTC_status_c = PCB_STAT_OVERTEMP;
					/* Over temperature fault is set */
					/* Record this PCB over temperature occurance in EEPROM */
					TempF_Set_PCB_OverTemp();
				}
			}
			else {
				/* PCB is at over temperature range */
				temperature_PCB_NTC_status_c = PCB_STAT_OVERTEMP;
			}
		}
		else {			
			/* Clear PCB over temperature counter:  PCB Temp < 100 C */
			temperature_PCB_OT_cnt = 0;
			
			/* Check if over temperature fault was set previously */
			if (Temp_FailSafe_prms.PCB_OverTemp_Status_c == OVER_TEMP_SET) {				
				/* Check if temperature is greater then or equal 90 C (AD < 213) and less then 100 C */
				if (temperature_PCB_ADC < temperature_s.PCB_Clear_OverTemp) {
					
					/* Over temperature fault was set.   */
					/* Now, 90 <= temp < 100.            */
					/* Do not modify the PCB NTC status. */

					/* Still keep over temperature fault */
					temperature_PCB_NTC_status_c = PCB_STAT_OVERTEMP;
				}
				else {					
					/* Clear PCB Over Temperature occurance (Temp is < 90 C) */
					TempF_Clr_PCB_OverTemp();
					/* PCB NTC is good */
					temperature_PCB_NTC_status_c = PCB_STAT_GOOD;
				}
			}
			else {				
				/* Check if we have to clear PCB Over Temperature occurance */
				TempF_Clr_PCB_OverTemp();
				/* PCB temperature is less then 100 C */
				/* PCB NTC is good                    */
				temperature_PCB_NTC_status_c = PCB_STAT_GOOD;
			}
		}		
	}else{
		/* Local battery voltage is not in operation range. */
		/* Clear the PCB over temperature counter. */
		temperature_PCB_OT_cnt = 0;		
	}
	/* End of Modified Implementation */	
}

/*********************************************************************************/
/*                            TempF_Get_PCB_NTC_Stat                             */
/*********************************************************************************/
/* This function returns the most recent PCB NTC status, which is used in        */
/* deciding main CSWM status.                                                    */
/*                                                                               */
/* If temperature_PCB_NTC_status_c is set to PCB_STAT_OVERTEMP, the main CSWM    */
/* status is updated to UGLY. So, no outputs are allowed to turn on, only 2      */
/* second LED display is present with switch request(s). 						 */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* ApplDescCheckSessionTransition :                                              */
/*    u_8Bit TempF_Get_PCB_NTC_Stat()                                            */
/* mainF_CSWM_Status :                                                           */
/*    u_8Bit TempF_Get_PCB_NTC_Stat()                                            */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* no calls                                                                      */
/*																				 */
/*********************************************************************************/
@far u_8Bit TempF_Get_PCB_NTC_Stat(void)
{
	return(temperature_PCB_NTC_status_c);
}

/*********************************************************************************/
/* Designed by: Bernhard Humpert 												 */
/* Implemented by: Can Kulduk													 */
/* Calculate CAN temperature equivalent for NTC reading.						 */
/* NTC reading is the supply voltage compensated value.							 */
/* Linear interpolation:														 */
/* t = y[i] + ( (x[i]-x) * (y[i+1]-y[i]) / (x[i]-x[i+1]) )						 */
/* where:																		 */
/*   x      = NtcAd_c:        current NTC reading								 */
/*   x[i]   = ntc_read[i]:    next higher NTC reading							 */
/*   x[i+1] = ntc_read[i+1]:  next lower NTC reading							 */
/*   y[i]   = can_temp(i):    CAN temperature equivalent for x[i] 				 */
/*   y[i+1] = can_temp(i+1):  CAN temperature equivalent for x[i+1]				 */
/*                            (in (deg C + 40) * 2 )							 */
/*********************************************************************************/
@far u_8Bit TempF_NtcAdStd_to_CanTemp(u_8Bit NtcAd_c)
{
	/* Local variables */
	u_8Bit k = 0;		   				// stores the found index of the ntcAd reading in our lookup table 
	u_16Bit interim_canTemp = 0;		// intermidiate value of the calculated CAN temperature
	u_8Bit canTemp = 0;					// final result (8-bit value) CAN temp equivalent of ntcAd reading
	u_8Bit NtcAd_rdg_c = 0;             // Current NtcAd value
	
	NtcAd_rdg_c = NtcAd_c;				// store the current standarized NTC reading
	
	/* Check if ntcAD reading is greater then CAN Temp high range (+85 C) */
	if(NtcAd_rdg_c < NTC_READ[T_REFPOINTS-1]){	
		
		/* current standarized NTC reading is out of CAN temperature range (Temp > +85 C) */
		canTemp = T_LIMIT;	// return the canTemp as it is 85 C
		
	}else{
		
		/* Find a reference index for the current NTC reading */
		while ((NTC_READ[k] > NtcAd_c) && (k < T_REFPOINTS-2))
		{
			/* Not found. Increment the index */
			k++;
		}
		
		/* Convert standarized NtcAd reading to canTemp value if found index is not zero */
		if(k != FALSE){
			
			k = (k-T_N1);											// adjust the index
			interim_canTemp = (NTC_READ[k]-NtcAd_c);				// find the difference between table entry and current ntcAd
			interim_canTemp = (interim_canTemp*T_N10);			    // 16-bit unsigned int for divison
			canTemp = (NTC_READ[k] - NTC_READ[k+T_N1]);				// find the difference between next table entry and current ntcAd
			interim_canTemp = (interim_canTemp + (canTemp/T_N2) );	// add denominator/2 for rounding
			interim_canTemp = (interim_canTemp/canTemp);			// div: 16-bit/8-bit
			canTemp = (u_8Bit) (k*T_N10);							// next lower CAN temp = index*10 (Maximum 250)
			canTemp = (u_8Bit) (canTemp+interim_canTemp);			// final canTemp result
			
		}else{
			/* k=0: Meaning temperature is at least -40 C. Return 0 for canTemp */
			/* canTemp is initiliazed with zero. So, no update is necassary */
		}		
	}
	return(canTemp);
}


/*********************************************************************************/
/* Designed by: Bernhard Humpert 												 */
/* Implemented by: Can Kulduk													 */
/* Calculate equivalent NTC reading from CAN temp.						 		 */
/* NTC reading is the supply voltage compensated value.							 */
/* Linear interpolation:														 */
/* t = y[i+1] + ( (x[i+1]-x) * (y[i]-y[i+1]) / (x[i]-x[i+1]) )				     */
/* t = y[i+1] + ( ((i+1)*10-x) * (y[i]-y[i+1]) / 10 )							 */
/* where:																		 */
/* x      = CanTemp_c:      current CAN temperature								 */
/* x[i]   = i*10:           next lower CAN temperature							 */
/* x[i+1] = (i+1)*10:       next higher CAN temperature							 */	
/* y[i]   = ntc_read[i]:    NTC reading equivalent for x[i] 					 */
/* y[i+1] = ntc_read[i+1]:  NTC reading equivalent for x[i+1]					 */	
/*                            (in (deg C + 40) * 2 )							 */
/*********************************************************************************/
@far u_8Bit TempF_CanTemp_to_NtcAdStd(u_8Bit CanTemp_c)
{
	/* Local variables */
	u_8Bit j = 0;		   				// stores the found index of the canTemp reading in our lookup table 
	u_16Bit interim_ntcAd = 0;			// intermidiate value of the calculated ntcAD temperature
	u_8Bit ntcAd = 0;					// final result (8-bit value) ntcAd reading equivalent of CAN temp
	u_8Bit canTemp_rdg_c = 0;           // Current canTemp value
	
	canTemp_rdg_c = CanTemp_c;			// store the current canTemp
	
	j = (u_8Bit) (canTemp_rdg_c/T_N10);	// Find a reference index of the current canTemp
	
	/* Check the index of the canTemp */
	if(j <= T_REFPOINTS-2){
		
		ntcAd = (u_8Bit) ( (j+T_N1)*T_N10 );     					// next higher CAN temp Reference point (Maximum 250)
		interim_ntcAd = (NTC_READ[j]-NTC_READ[j+T_N1]);				// find the difference between the two table lookup values
		interim_ntcAd = (interim_ntcAd*(ntcAd - canTemp_rdg_c));    // 16-bit result
		interim_ntcAd = ((interim_ntcAd+T_N5)/T_N10);         		// for rounding	and making an 8-bit result                                  
		ntcAd = (u_8Bit) ((NTC_READ[j+1] + interim_ntcAd)) ;		// final ntcAd result
	       
	}else{
		ntcAd = NTC_READ[T_REFPOINTS-1];
	}
	return(ntcAd);	
}

