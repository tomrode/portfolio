/********************************************************************************/
/*                        A U T H O R   I D E N T I T Y							*/
/********************************************************************************/
/* Initials      Name                   Company									*/
/* --------      --------------------   --------------------------------------- */ 
/* CK            Can Kulduk             Continental Automotive Systems			*/
/* HK            Harsha Yekollu         Continental Automotive Systems          */
/* FU            Fransisco Uribe        Continental Automotive Systems          */
/* CC            Christian Cortes       Continental Automotive Systems          */
/********************************************************************************/


/********************************************************************************/
/*                   CHANGE HISTORY for STEERING WHEEL MODULE                   */
/********************************************************************************/
/* 09.01.2006 CK - Module created			 	         						*/
/* 03.24.2008 HY - Module copied to mY11                                   		*/
/* 04.15.2008 HY - AD channels updated                                     		*/
/* 04.15.2008 HY - Threshold values hardcoded.                             		*/
/* 06.23.2008 FU - stWF_Chck_EFUSE_Rst removed			            			*/
/* 08.25.2008 HY - Added EE_BlockRead,EE_BlockWrite calls                       */
/* 09.08.2008 HY - For EE_BlockWrites copying data into local structure    		*/
/*                 Function stWF_Copy_Diag_Prms is not being used any more 		*/     
/* 09.12.2008 CK - Update fault detection for Change Request ID 14592      		*/
/* 09.30.2008 CK - Update read steering wheel temperature routine for      		*/
/*                 Change Request ID 15284                                 		*/
/* 10.21.2008 CK - Update Status function is modified in order to insure   		*/
/*                 steering wheel output turn off when temperature signal  		*/
/*                 SNA (0xFF) is received.                                 		*/
/* 10.21.2008 CK - Commented all reset EFUSE and stWF_Copy_Diag_Prms code	    */
/* 10.21.2008 CK - Updated stWF_Output_Control and stWF_Clear functions to      */
/*                 insure that we call Pwm_SetDutyCycle function only when we   */
/*                 change the PWM. 												*/
/* 10.29.2008 CK - Propulsion system active code is added to stWF_swtch_Mngmnt 	*/
/* 10.30.2008 CK - Added stWF_Actvtn_Stat function which returns the output     */
/*                 state. This is used in battery voltage compensation 			*/
/*                 calculation.													*/
/* 10.31.2008 CK - Update sWF_LED_Display function. Dbk fucntions are called    */
/* 				   only when there is a change in LED status.					*/
/* 11.06.2008 CK - Updated the allowed steering wheel NTC thresholds for writing*/
/*                 in stWF_Chck_Diag_Prms										*/
/* 11.26.2008 CK - PowerNet implementation to reduce load to WL1 automatically  */
/*                 when load shed 2 condition is present.  						*/
/*                 Created stWLF_Chck_LdShd_Lvl_Reduction function. 			*/
/*                 Updated stWF_Manager function.								*/
/* 12.03.2008 CK - Updated stWF_Actvtn_Stat function. Local battery voltage drop*/
/*                 compensation is performed when the output is turned on at    */
/*                 WL3, WL2, or WL1.			                                */
/* 12.17.2008 CK - Added a call to stWF_Updt_PWM function in stWF_Manager       */
/*                 in order to recover from UGLY to GOOD status change.         */
/*                 This is necassary for occurance recovery such as system and  */
/*                 or local battery matured faults. Switch request comes at the */
/*                 and of the de-mature time, while we are doing only LED       */
/*                 display.                                                     */
/* 01.07.2009 CK - Modified stWF_Output_Control function for MKS entry 22112.   */
/* 01.09.2009 CK - Deleted diag_eol_io_lock_b check in stWF_Output_Control      */
/*                 and stWF_Flt_Mntr functions. High side can be turned on and  */
/*                 fault detection is performed during eol.						*/
/* 01.22.2009 CK - PC-Lint Fixes. No complaints from PC-Lint after fixes.       */
/* 02.12.2009 CK - Chrysler Routine Control Service implemetation to run output */
/*                 test. Updated stWF_Manager, stWF_Diag_IO_Cntrl and           */
/*                 stWF_Swtch_Mngmnt, and stWF_Fnsh_LED_Dslpy functions.		*/
/* 02.18.2009 CK - Open load and short to battery fault detection update.       */
/*                 Set the stW_Open_or_STB_b flag after two second in           */
/*                 stWF_Updt_Flt_Stat function in order to insure two second    */
/*                 fault mature time.											*/
/* 02.18.2009 CK - Commented not supported STW Partial Open DTC code in         */
/* 				   stWF_DTC_Manager function.									*/
/* 03.24.2009 CK - Modified stWF_Init and stWF_Rdg_Temperature functions to     */
/*                 allow steering wheel management to start right after a POR.  */
/*                 No more two second delay before we start accepting the switch*/
/*                 request. MKS ID: 27476                                       */
/* 04.08.2009 CK - Updated stWF_Chck_Diag_Prms and stWF_Write_Diag_Data         */
/*                 functions to support new EOL write command to update         */
/*                 STW_Iref and STW_Vref parameters in D-Flash.         		*/
/*                 MKS Change Request ID: 28078                                 */
/* 04.09.2009 CK - Simplified function stWF_Isense_AD_Rdg to speed up Isense    */
/*                 reading. Off Isense is not used in CSWM MY11                 */
/* 04.14.2009 CK - Updated stWLF_Chck_Lsd2_Lvl_Reduction function to check for  */
/*                 Battery Reached Critical State signal before reducing to WL1.*/
/* 04.14.2009 CK - Modified stWLF_Get_Swtch_Rq function for MKS ID: 28084.      */
/*                 HSW switch request will come from CCN node for W-series      */
/*                 vehicles as well (just like D-series vehicles). L-series     */
/*                 (PowerNet) switch request stays with TGW node.               */
/* 05.12.2009 HY - MKS 28818: stWLF_Chck_mainState updated to stay outputs ON in case  */
/*                 Remote start active flag goes OFF. commented stWF_Clear      */
/* 09.04.2009 CC - Modified stWLF_Get_Swtch_Rq -   Uncomment MKS ID: 28084.     */
/* 09.18.2009 CC -   hsF_HeatManager(void).	MkS 35407 ENG RPM IGN is in RUN     */
/*                     and ENG RPM is not running customer requested to allow   */
/*                     switch requests to display 2sec LED 	     				*/
/* 09.18.2009 CC -    stWF_Manager() and stWF_Chck_Auto_Lvl_Updt() FTR00139667  */
/*                    At any point of time, If module sees >14A total current   */
/*                    draw when   Rear seats and HSW outputs are ON, HSW output */
/*                      would be reduced to the next available low level.		*/
/* 10.04.2009 CC - stWF_Rdg_Temperature()   FTR00139665 - New vehicle Line RT-RM */
/*                 Steering Wheel Temperature  temp sna signal to be followed   */
/*                 with BCM_A8 signal											*/
/* 11.25.2009 CC - stWLF_RemSt_Chck(),stWLF_Chck_mainState and stWF_Clear () was*/
/*                 modified FTR00139669 - Autostart feature for PowerNet Series.*/
/* 02-17-2010 HY - Modiifed stWLF_Chck_mainState() to fulfil new requirement    */
/*		           MKS 47354: When IGN status remains in RUN and Ign Run Remote */
/*                 Start turns Inactive, HSW will start back from 100% PWM      */
/* 02-18-2010 HY - DECISION CHANGE: Now goes to 100% from 50% while in Remote   */
/*                 Above logic commented and enabled 100% PWM in Remote start   */ 
/* 09-21-2010 HY - autostart_check_complete_b introduced to have final check at */
/*                 main Remote start manager function to go main state from     */
/*                 Auto start to Normal for Power Net                           */
/********************************************************************************
 * 06-15-2011	Harsha	  75322	 Cleanup the code. Delete the unused code for this program.
 * 								 Modified functions are stWF_Rdg_Temperature,stWLF_Get_Swtch_Rq.
 * 								 Deleted functions are stWLF_Chck_Lsd2_Lvl_Reduction
 * 08-09-2011	Harsha	  81838	 PROXI Implementation. Checking data from PROXI eeprom CFG updated
 * 								 in stwF_Init function.	
 * 								 Also as Steering Wheel is supported for more vehicle lines,
 * 								 accessing the EEP Parameters logic updated like Heating.c.
 * 11-30-2011   Harsha    91783  HSW_TEMP_CUT_OFF limits got adjusted to 60C, as the parameters increased.
 * 12-08-2011   Christian 92650  MKS 92650 Heated Steering Wheel Issue If Level is 4 was added to last OR
 * 
 * 
 * 
 * 
 * 
 ********************************************************************************/


/*********************************************************************************/
/* Software Design Text                                                          */
/* -------                                                                       */
/* According to the functional specification, CSWM will switch on the power for  */
/* the heated steering wheel only if the engine is running A thermistor sensor 	 */ 
/* feedback, from the steering wheel heating element via the CAN bus to the 	 */
/* CSWM will regulate the heater output to prevent the wheel temperature to rise */
/* above the maximum threshold temperature set point. A calibration lookup table */
/* of 2 high settings and 1 low setting is to be used. Each press of the switch  */
/* will change the Wheel heat setting from OFF to ON, and ON to OFF, or else the */
/* automatic timer will turn off the wheel heat cycle in 70 minutes. 	         */
/*********************************************************************************/


/********************************** @Include(s) **********************************/
#include "il_inc.h"
#include "v_inc.h"
#include "TYPEDEF.h"            
#include "StWheel_Heating.h"     
#include "main.h"
#include "canio.h"
#include "DiagMain.h"
#include "DiagIOCtrl.h"
#include "DiagWrite.h"
#include "AD_Filter.h"
#include "Mcu.h"
#include "Dio.h"
#include "Port.h"
#include "Pwm.h"
#include "FaultMemory.h"
#include "FaultMemoryLib.h"
#include "Relay.h"
//#include "Temperature.h"
#include "mw_eem.h"
/* At any point of time, If module sees >14A total current draw when Rear seats and HSW outputs are ON, HSW output would be reduced to the next available low level. */
#include "heating.h"

/*********************************************************************************/


/********************************* external data *********************************/
/* N
 * O
 * N
 * E */
/*********************************************************************************/


/*********************************** @ variables *********************************/
u_8Bit stW_level_c;                  // Wheel level (WL0, WL1, etc)
u_8Bit stW_status_c;                 // Steering Wheel Status (GOOD, BAD, UGLY)
u_8Bit stW_lst_sw_rq_c;              // Last switch request
u_8Bit stW_LED_stat_c;               // LED Status (On /Off)
u_8Bit stW_prev_LED_stat_c;			 // Previous LED status
u_16Bit stW_Flt_status_w;            // StW Fault Status
u_16Bit stW_onTime_w;                // Amount of time that stW has been On
u_16Bit stW_TimeOut_Cycles_w;        // Timeout cycles
u_16Bit stW_Flt_Mtr_cnt;             // fault mature count
u_16Bit stW_LED_Dsply_Tm_cnt;        // LED display time
u_16Bit stW_pwm_w;                   // PWM of the wheel
u_16Bit stW_prev_pwm;				 // previous PWM value

u_8Bit stW_temperature_c;            // Most recently read wheel temperature
u_8Bit stW_tmprtr_sensor_flt_c;      // StWheel Temperature sensor fault
u_8Bit stW_pwm_state_c;              // Steering wheel pwm output state
u_8Bit stW_cfg_c;                    //Holds steering Wheel Configuration.
u_8Bit stW_veh_offset_c;			 //Holds the vehicle line offset to get the right values from eep array.
u_8Bit stW_flt_calibs_c[FLT_DTC_LMT];
//u_8Bit stW_EFUSE_state;              // Steering wheel EFUSE state	//NOT USED

/* Fault Detection parameters */
u_8Bit stW_Vsense_HS_c;              // Steering wheel voltage reading high side
u_8Bit stW_Vsense_LS_c;              // Steering wheel voltage reading low side
u_8Bit stW_Isense_c;                 // Steering wheel current reading low side
u_16Bit stW_Isense_offset_w;         // Steering wheel Isense offset
u_8Bit stW_Ign_stat;                 // Steering wheel ignition (battery power) status
u_8Bit stW_RearHeat_Isense_c;		 // Steering Wheel Isense plus Rear Heaters Isense FTR00139667

/* 09/19/2007: */
/* StWheel Isense is read when we observe a high pwm signal on the low side output (PT2 pwm channel).                                    */
/* At low pwm levels (10% pwm), we may only get 2 Isense readings every 50ms (20Hz frequency).                                           */
/* So, we will wait 300ms before we start Open and Partial Open fault detection after stWheel low side is turned on (from off position). */
u_8Bit stW_Isense_dly_cnt_c;         // Isense analysis delay counter (waits 320ms for open detection)

/* 09/11/2008: */
/* Steering Wheel Open and Short to battery faults show similar results. */
/* HS_Vsense is close to Steering Wheel Ignition voltage and Isense is close to zero.  */
/* After detecting one of these faults, steerig wheel output will be turned off after approximately 2 seconds. */
/* We will wait 140ms before deciding which fault to set, as we need to see if HS_Vsense will come down after */
/* We stop PWMing the High side driver. After 140ms HS_Vsense reading seems to go down to 0 when there is no short to battery. */
u_8Bit stW_chck_Open_or_STB_dly_c; 

/* Converted (seconds to time slice count) timing variables */
u_16Bit stW_Flt_Mtr_Tm;              // fault mature time with respect to time slice
u_16Bit stW_LED_Dsply_Tm;            // short term LED display with respect to time slice

/* StWHeel bits/flags */
union char_bit stW_Flag1_c;
union char_bit stW_Flag2_c;
#define stW_RemSt_auto_b     stW_Flag2_c.b.b0  // Remote start automatic stWheel activation indicator
#define stW_Open_or_STB_b    stW_Flag2_c.b.b1  // Used to determine whether we have an open or STB on High Side Driver
#define stW_Isense_limit_b       stW_Flag2_c.b.b2  // Used to determine whether 8A limit is set or not.
//#define stW_12A_limit_b      stW_Flag2_c.b.b3  // Used to determine whether 12A limit is set or not.

u_8Bit stW_lst_main_state_c;        // Previous main state (to turn off the stWheel when RemSt timeout)
//u_8Bit stW_diag_prog_data_c[STW_DATA_LENGHT];    //Write Data by LID data array (not used)
u_16Bit stW_WL1TempChck_lmt_w;        // WL1 temp. check limit
u_16Bit stW_WL2TempChck_lmt_w;        // WL2 temp. check limit
u_16Bit stW_WL3TempChck_lmt_w;        // WL3 temp. check limit
u_16Bit stW_WL4TempChck_lmt_w;        // WL4 temp. check limit .. MKS 66719 Harsha added
/* (Tracker Id #38) */
u_16Bit stW_ontimeLmt_w;   // HSW ontime limit before detecting Sensor fault under detectable range at ambient < -10 C

u_16Bit stW_Isense_timer_w;                    //Timer for 8A Current limit.
//u_16Bit stW_12A_timer_w;                   //Timer for 12A Current limit.
/**********************************************************************************/


/******************************** @Data Structures ********************************/
/* StWheel calibration parameters structure declaration */
struct stW_cal_params stW_prms;
/* StWheel AD conversion parameter */
main_ADC_prms stW_AD_prms;
/* Local StW Status structure */
//tHSW_Stat Local_HSW_Stat;
/**********************************************************************************/


/*********************************************************************************/
/*                                stWF_Clear                                     */
/*********************************************************************************/
/* This function makes sure that low-side of the steering wheel element is  	 */
/* turned off. It also clears the wheel variables. 								 */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* IntFlt_F_ClearOutputs :                                                       */
/*    stWF_Clear()                                                               */
/* stWF_Fnsh_LED_Dslpy :                                                         */
/*    stWF_Clear()                                                               */
/* stWF_LED_Display :                                                            */
/*    stWF_Clear()                                                               */
/* stWF_Rtrn_Cntrl_toECU :                                                       */
/*    stWF_Clear()                                                               */
/* stWF_Timer :                                                                  */
/*    stWF_Clear()                                                               */
/* stWF_Upt_Lvl :                                                                */
/*    stWF_Clear()                                                               */
/* stWLF_Chck_mainState :                                                        */
/*    stWF_Clear()                                                               */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* Pwm_SetDutyCycle(Pwm_ChannelType ChannelNumber,uint16 DutyCycle)              */
/*																				 */
/*********************************************************************************/
@far void stWF_Clear(void)
{
	/* Check for ignition status */
	if ( (canio_RX_IGN_STATUS_c != IGN_RUN) && (canio_RX_IGN_STATUS_c != IGN_START) )
	{
		stW_lst_sw_rq_c        = 0;            // clear the last switch request
		stW_Flt_Mtr_cnt        = 0;            // clear fault mature time
		stW_Isense_dly_cnt_c   = 0;            // clear Isense analysis delay
		stW_TimeOut_Cycles_w   = 0;            // clear timeout cycles

		stW_Flt_status_w    = STW_NO_FLT_MASK; //no stw faults

		stW_status_c           = GOOD;         //Stw status is good

		stW_diag_cntrl_act_b   = FALSE;        // diagnostics control is inactive
		stW_not_vld_swtch_rq_b = FALSE;        // clear the invalid switch request
		//stW_EFUSE_reset_b      = FALSE;        // clear the EFUSE reset bit (NOT USED)
		stW_RemSt_auto_b       = FALSE;        // Clear remote start automatic activation bit		

		stW_Open_or_STB_b      = FALSE;        // Clear the bit that is used to determine Open or STB fault on HS driver

		diag_io_st_whl_rq_c    = 0;            // diagnostic stW request is off
		diag_io_st_whl_state_c = 0;            // diagnostic stW LED state is off

		stW_Vsense_HS_c        = 0;            // clear steering wheel voltage reading high side
		stW_Vsense_LS_c        = 0;            // Clear steering wheel voltage reading low side
		stW_Isense_c           = 0;            // Clear steering wheel current reading low side

		stW_Ign_stat           = 0;            // Clear steering wheel ignition (battery power) status

		stW_lst_main_state_c = NORMAL;         // Last main state is defaulted to normal
		
		stW_chck_Open_or_STB_dly_c = 0;		   // Clear Open or STB detection delay counter 
	}
	else
	{
		/* Ignition run or start. Do not clear these variables */
	}
	/* variables that are cleared without having ignition other than run/start */
	stW_level_c            = 0;        // current wheel level is WL0 
	stW_onTime_w           = 0;        // clear Amount of time that stW has been On
	stW_pwm_w              = 0;        // PWM is 0
	stW_LED_stat_c         = 0;        // LED status is off
	stW_pwm_state_c        = 0;        // pwm state is PWM_LOW

	/* 11.05.2008 Clear the state bit ONLY in the stWF_Output_Cntrl function */
	/* This is done to make sure we turn off the steering wheel. */
	//stW_state_b            = FALSE;    // stW is off
	stW_sw_rq_b            = FALSE;    // software request off
	stW_LED_off_b          = FALSE;    // clear LED off flag
	stW_swtch_rq_prps_b    = FALSE;    // switch request purpose is to turn off
	//stW_RemSt_auto_b       = FALSE;        // Clear remote start automatic activation bit FTR00139669
	stW_clr_ontime_b       = FALSE;    // reset ontime to zero flag

	stW_Isense_limit_b         = FALSE;        // Clear the 8A limit set flag.
//	stW_12A_limit_b        = FALSE;        // Clear the 12A limit set flag.

	stW_LED_Dsply_Tm_cnt   = 0;        // clear LED display time
	
	stW_Isense_timer_w         = 0;        //8A current limit timer.
//	stW_12A_timer_c        = 0;        //12A current limit timer.
}

/*********************************************************************************/
/*                                stWF_Init                                      */
/*********************************************************************************/
/* This function is called after a power on reset and re-initializes all the     */
/* steering wheel variables with their default values. 							 */
/* Calls from                                                                    */
/* --------------                                                                */
/* main :                                                                        */
/*    stWF_Init()                                                                */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* EE_BlockRead(EEBlockLocation elocation, u_8Bit *pbydest)                      */
/* 																				 */
/*********************************************************************************/
@far void stWF_Init(void)
{
	/* Read PROXI Configuration from EEPROM */
	EE_BlockRead(EE_PROXI_CFG, (UINT8*)&ru_eep_PROXI_Cfg);
	/* Get the All wheel parameters from EEP to Shadow RAM */
	EE_BlockRead(EE_WHEEL_CAL_DATA, (u_8Bit*)&stW_prms); 

	if(ru_eep_PROXI_Cfg.s.Vehicle_Line_Configuration == PROXI_VEH_LINE_PF)
	{
		stW_veh_offset_c = STW_VEH_PF;
	}
	else 
	{
		/* As of now we have only one vehicle line that is PF */
		stW_veh_offset_c = STW_VEH_PF;
	}

	if (ru_eep_PROXI_Cfg.s.Wheel_Material == PROXI_WHEEL_TYPE_LEATHER) 
	{
		/* Get the configuration value here for leather*/
		stW_cfg_c = STW_LEATHER; 
	}
	else
	{
		/* Get WOOD Configuration here. */
		stW_cfg_c = STW_WOOD; 
	}

	
//	/* Calibration data - EEPROM */
//	/* Copy all Steering Wheel Calibration data into local structure */
//	if (ru_eep_PROXI_Cfg.s.Wheel_Material == PROXI_WHEEL_TYPE_WOOD) 
//	{
//		/* Get the All Leather wheel parameters from EEP to Shadow RAM */
//		EE_BlockRead(EE_WHEEL_CAL_DATA, (u_8Bit*)&stW_prms); 
//	}
//	else
//	{
//		/* Module is with Wood. Get the wood parameters from EEP */
//		EE_BlockRead(EE_WHEEL_CAL_DATA, (u_8Bit*)&stW_prms);   
//	}

	EE_BlockRead(EE_WHEEL_FAULT_THRESHOLDS, (u_8Bit*)&stW_flt_calibs_c);   
	
	/* No faults */
	stW_Flt_status_w = STW_NO_FLT_MASK;    //no stw faults
	
	/* Status Good */
	stW_status_c = GOOD;                   //Stw status is good
	
	/* Timing */
	stW_Flt_Mtr_Tm = (MAIN_FLT_MTR_TM * STW_MTR_FLT_TM_MDLTN);       // Fault mature time (2*50=100) in 20ms tm
	stW_LED_Dsply_Tm = (MAIN_LED_DSPLY_TM * STW_LED_DSPLY_TM_MDLTN); // LED Display time (2*100=200) in 10ms tm

//	/* Initiliaze temperature check limits for WL1 .. Harsha Added */
//	stW_WL1TempChck_lmt_w = ((stW_prms.WL1TempChck_Time * STW_TM_SLICE_MLTPLR) + (stW_prms.WL1TempChck_Time>>1));
//
//	/* Initiliaze temperature check limits for WL2 */
//	stW_WL2TempChck_lmt_w = ((stW_prms.WL2TempChck_Time * STW_TM_SLICE_MLTPLR) + (stW_prms.WL2TempChck_Time>>1));
//	
//	/* Initiliaze temperature check limits for WL3 */
//	stW_WL3TempChck_lmt_w = ((stW_prms.WL3TempChck_Time * STW_TM_SLICE_MLTPLR) + (stW_prms.WL3TempChck_Time>>1));
//	
//	/* Initiliaze temperature check limits for WL4 .. harsha added */
//	stW_WL4TempChck_lmt_w = ((stW_prms.WL4TempChck_Time * STW_TM_SLICE_MLTPLR) + (stW_prms.WL4TempChck_Time>>1));

	/* Initiliaze temperature check limits for WL1 .. Harsha Added */
	stW_WL1TempChck_lmt_w = ((stW_prms.cals[stW_veh_offset_c+stW_cfg_c].WL1TempChck_Time * STW_TM_SLICE_MLTPLR) + (stW_prms.cals[stW_veh_offset_c+stW_cfg_c].WL1TempChck_Time>>1));

	/* Initiliaze temperature check limits for WL2 */
	stW_WL2TempChck_lmt_w = ((stW_prms.cals[stW_veh_offset_c+stW_cfg_c].WL2TempChck_Time * STW_TM_SLICE_MLTPLR) + (stW_prms.cals[stW_veh_offset_c+stW_cfg_c].WL2TempChck_Time>>1));
	
	/* Initiliaze temperature check limits for WL3 */
	stW_WL3TempChck_lmt_w = ((stW_prms.cals[stW_veh_offset_c+stW_cfg_c].WL3TempChck_Time * STW_TM_SLICE_MLTPLR) + (stW_prms.cals[stW_veh_offset_c+stW_cfg_c].WL3TempChck_Time>>1));
	
	/* Initiliaze temperature check limits for WL4 .. harsha added */
	stW_WL4TempChck_lmt_w = ((stW_prms.cals[stW_veh_offset_c+stW_cfg_c].WL4TempChck_Time * STW_TM_SLICE_MLTPLR) + (stW_prms.cals[stW_veh_offset_c+stW_cfg_c].WL4TempChck_Time>>1));

	/* Change Request (Tracker Id #38)                                                                             */
	/* Calculate the cycles in 320 timeslice for HSW Sensor fault below detectable range detection (ambient <+5 C) */
//	stW_ontimeLmt_w = ( (stW_prms.SnsrFlt_BelowRng_Ontime_lmt * STW_TM_SLICE_MLTPLR) + (stW_prms.SnsrFlt_BelowRng_Ontime_lmt>>1) );
	stW_ontimeLmt_w = ( (stW_flt_calibs_c[STW_SNSR_FLT_RANGE_ONTIME] * STW_TM_SLICE_MLTPLR) + (stW_flt_calibs_c[STW_SNSR_FLT_RANGE_ONTIME]>>1) );
	
	/* Initiliaze last main state as normal */
	stW_lst_main_state_c = NORMAL;


	/* Initiliaze steering wheel temperature and sensor fault status */
	/* To allow functinality right after a POR */
	/* MKS ID: 27476 */
	stW_temperature_c = STW_TEMP_22C;	// steering wheel temperature is 22.5 C	
	stW_tmprtr_sensor_flt_c = (u_8Bit) STW_TEMPERATURE_SENSOR_OK; // steering wheel sensor fault status is o.k.
}

/*********************************************************************************/
/*                                stWF_AD_Cnvrn                                  */
/*********************************************************************************/
/* After selecting the AD channel and mux channel this function is called to     */
/* perform the AtoD conversion and filter the result for the selected steering   */
/* wheel feedback. 																 */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* stWF_Isense_AD_Rdg :                                                          */
/*    stWF_AD_Cnvrn()                                                            */
/* stWF_Vsense_AD_Rdg :                                                          */
/*    stWF_AD_Cnvrn()                                                            */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* ADF_AtoD_Cnvrsn(u_8Bit AD_Filter_supply)                                      */
/* ADF_Str_UnFlt_Mux_Rslt(u_8Bit AD_Filter_feed,u_8Bit AD_Filter_mux_feed)       */
/* ADF_Mux_Filter(u_8Bit AD_Filter_slctd_mux,u_8Bit AD_Filter_mux_ch)            */
/* 																				 */
/*********************************************************************************/
@far void stWF_AD_Cnvrn(void)
{
	/* Analog to digital conversion routine for muxed output */
	ADF_AtoD_Cnvrsn(stW_AD_prms.slctd_adCH_c);
	
	/* Conversion is completed. Store the unfiltered mux result */
	ADF_Str_UnFlt_Mux_Rslt(stW_AD_prms.slctd_adCH_c, stW_AD_prms.slctd_muxCH_c);
	
	/* Filter the muxed AD reading */
	ADF_Mux_Filter(stW_AD_prms.slctd_mux_c, stW_AD_prms.slctd_muxCH_c);
}

/*********************************************************************************/
/*                                stWF_Vsense_AD_Rdg                             */
/*********************************************************************************/
/* This function reads Vsense feedback for high-side and low-side of the         */
/* steering wheel by: 															 */
/* 1. Selecting the mux channel                                                  */
/* 2. Performing AtoD conversion                                                 */
/* 3. Converting AD reading to 100mV units                                       */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* stWF_AD_Rdng :                                                                */
/*    stWF_Vsense_AD_Rdg()                                                       */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* u_8Bit ADF_Nrmlz_Vsense(u_16Bit AD_Vsense_value)                              */
/* stWF_AD_Cnvrn()                                                               */
/* Dio_WriteChannelGroup( ChannelGroupIdPtr, Level)                              */
/*																				 */
/*********************************************************************************/
@far void stWF_Vsense_AD_Rdg(void)
{
	u_8Bit i;    // loop variable
	for (i=0; i<WHEEL_SIDE_LMT; i++)
	{
		/* Check side (high or low) */
		if (i == WHEEL_HIGH_SIDE)
		{
			/* Select mux channel 6 for stWheel Vsense (High side) reading (MUX1) */
			stW_AD_prms.slctd_muxCH_c = CH6;
		}
		else
		{
			/* Select mux channel 5 for stWheel Vsense (Low side) reading (MUX1) */
			stW_AD_prms.slctd_muxCH_c = CH5;
		}
		
		/* Choice the mux1 input */
		Dio_WriteChannelGroup(MuxSelGroup, stW_AD_prms.slctd_muxCH_c);
		
		/* Perfrom Vsense AD conversion and filtering */
		stWF_AD_Cnvrn();
		
		/* Check side (high or low) */
		if (i == WHEEL_HIGH_SIDE)
		{
			/* Store the filtered Vsense reading */
			stW_Vsense_HS_c = ADF_Nrmlz_Vsense(AD_mux_Filter[stW_AD_prms.slctd_mux_c][stW_AD_prms.slctd_muxCH_c].flt_rslt_w);
		}
		else
		{
			/* Store the filtered Vsense reading */
			stW_Vsense_LS_c = ADF_Nrmlz_Vsense(AD_mux_Filter[stW_AD_prms.slctd_mux_c][stW_AD_prms.slctd_muxCH_c].flt_rslt_w);
		}
	}
}

/*********************************************************************************/
/*                                stWF_Isense_AD_Rdg                             */
/*********************************************************************************/
/* Input parameter:                                                              */
/* Isense_Rdg_Type: Used to store the Isense reading as ON_ISENSE. 				 */
/* 																				 */
/* This function reads Isense feedback for low-side of the steering wheel by:    */
/* 1. Selecting the Wheel Isense mux channel                                     */
/* 2. Performing AtoD conversion        										 */
/* 3. Converting AD reading to 100mA units                                       */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* stWF_AD_Rdng :                                                                */
/*    stWF_Isense_AD_Rdg(stW_Isense_type_e Isense_Rdg_Type)                      */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* u_8Bit ADF_Nrmlz_StW_Isense(u_16Bit AD_Stw_Isense_value)                      */
/* stWF_AD_Cnvrn()                                                               */
/* Dio_WriteChannelGroup( ChannelGroupIdPtr, Level)                              */
/* 																				 */
/*********************************************************************************/
@far void stWF_Isense_AD_Rdg(stW_Isense_type_e Isense_Rdg_Type)
{
	/* Local variable(s) */ 				// Commented on 04.09.2009 by CK Not using local variables
	//u_16Bit stW_interim_Isense_w = 0;     // Variable that stores intermediate Isense reading
	
	/* Select mux channel 4 for stWheel Isense reading */
	stW_AD_prms.slctd_muxCH_c = CH4;
	
	/* Choice the mux1 input */
	Dio_WriteChannelGroup(MuxSelGroup, stW_AD_prms.slctd_muxCH_c);
		
	/* Perfrom Vsense AD conversion and filtering */
	stWF_AD_Cnvrn();
	
	/* Added on 04.09.2009 by CK. Simplified to speed up Isense reading. OFF Isense is not used in CSWM MY11 */
	/* Check Isense reading type */
	if(Isense_Rdg_Type == ON_ISENSE){
		/* Convert the 10-bit AD result to 8-bit Isense in 100mA units */
		stW_Isense_c = ADF_Nrmlz_StW_Isense(AD_mux_Filter[stW_AD_prms.slctd_mux_c][stW_AD_prms.slctd_muxCH_c].flt_rslt_w);
	}else{
		stW_Isense_c = 0;
	}	
	
	/* ORIGINAL */
//	/* Check Isense reading type */
//	if (Isense_Rdg_Type == ON_ISENSE) {
//		
//		/* Store the filtered Isense reading (in 10-bit AD unit) */
//		stW_interim_Isense_w = AD_mux_Filter[stW_AD_prms.slctd_mux_c][stW_AD_prms.slctd_muxCH_c].flt_rslt_w;
//		
//		/* Check if interim Isense is greater than Isense offset */
//		if (stW_interim_Isense_w > stW_Isense_offset_w) {
//			/* Update the Intermediate Isense result by subtracting the offset from it */
//			stW_interim_Isense_w -= stW_Isense_offset_w;
//			
//			/* Convert the 10-bit AD result to 8-bit Isense in 100mA units */
//			stW_Isense_c = ADF_Nrmlz_StW_Isense(stW_interim_Isense_w);
//		}
//		else {
//			/* Set Isense to zero */
//			stW_Isense_c = 0;
//		}
//	}
//	else
//	{
//		/* Store the Isense offset (in 10-bit AD unit) */
//		stW_Isense_offset_w = AD_mux_Filter[stW_AD_prms.slctd_mux_c][stW_AD_prms.slctd_muxCH_c].flt_rslt_w; 
//	}
}

/*********************************************************************************/
/*                                stWF_AD_Rdng                                   */
/*********************************************************************************/
/* This function manages all the steering wheel AD conversions.                  */
/* When low-side is disabled, it performs Isense offset and Vsense readings.     */
/*                                                                               */
/* If the low-side is enabled, it checks the pwm signal state (either signal is  */
/* high or low). If the pwm signal state is high, it performs the Isense 		 */
/* (on Isense) and Vsense readings. Otherwise it skips the AtoD conversion.      */
/*                                                                               */
/* Note: PWM period of wheel is 50ms (20 Hz frequency). Minimum pwm duty cycle   */
/* for wheel is 10%. So, minimum pwm signal on time is 5ms. This function is     */
/* called cyclically every 2.5ms. In the worst case, it ensures two on readings  */
/* per pwm period. 															     */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* main :                                                                        */
/*    stWF_AD_Rdng()                                                             */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* stWF_Vsense_AD_Rdg()                                                          */
/* stWF_Isense_AD_Rdg(stW_Isense_type_e Isense_Rdg_Type)                         */
/* Pwm_OutputStateType Pwm_GetOutputState(Pwm_ChannelType ChannelNumber)         */
/*																				 */
/*********************************************************************************/
@far void stWF_AD_Rdng(void)
{
	/* @Setup for AD conversion */
	/* Select the stWheel AD convertion parameters */
	stW_AD_prms.slctd_mux_c = MUX1;        // select the mux1 for stWheel feedbacks (Isense and Vsense)
	stW_AD_prms.slctd_adCH_c = CH6;        // select AD channel 6 in order to read the mux1 output
	
	/* Check Pwm Output State (update Isense only when Pwm State is High) */
	stW_pwm_state_c = (u_8Bit) Pwm_GetOutputState(PWM_STW);
	
	/* @AD Conversion */
	/* Check if stWheel is turned On */
	if (stW_state_b == TRUE)
	{
		/* Check pwm state */
		if (stW_pwm_state_c == PWM_HIGH)
		{
			/* Perform Vsense ADC */
			stWF_Vsense_AD_Rdg();
			
			/* Perform Isense ADC */
			stWF_Isense_AD_Rdg(ON_ISENSE);
		}
		else
		{
			/* Skip ADC. Pwm state is low */
		}
	}
	else
	{
		/* Perform Vsense ADC */
		stWF_Vsense_AD_Rdg();
		
		/* Compute Isense Offset */
		/* Francisco Uribe 6/30/2008 Offset compensation disabled for BTS6143D */
		/* stWF_Isense_AD_Rdg(OFFSET_ISENSE); */
		stW_Isense_offset_w = 0; /* Offset compensation disabled for BTS6143D */
		
		/* Clear stWheel On Isense */
		stW_Isense_c = 0;
	}
}

/*********************************************************************************/
/*                                stWF_Cal_TmOut                                 */
/*********************************************************************************/
/* This function calculates the cycles needed in 320ms timeslice 				 */
/* (this is where StWF_Timer is called) for present steering wheel level (WL0,   */
/* WL1, WL2, WL3 or WL4). 															 */
/*                                                                               */
/* Note: 60 seconds / 320ms = 187.5. So,                                         */
/* Timeout Cycles = wheel level timeout[in minutes] * 187.5                      */
/*                                                                               */
/* If timeout minutes is an even number, conversion is exact. If it is odd,      */
/* error is insignificant (160ms).                                               */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* stWF_Timer :                                                                  */
/*    stWF_Cal_TmOut()                                                           */
/*                                                                               */
/*********************************************************************************/
@far void stWF_Cal_TmOut(void)
{
	/* Check wheel level as a safety */
	if ( (stW_level_c > WL0) && (stW_level_c <= WL4) ) {
		/* Calculate how many cycles we need in 320ms timeslice. */
		stW_TimeOut_Cycles_w = ( (stW_prms.cals[stW_veh_offset_c+stW_cfg_c].TimeOut_c[stW_level_c] * STW_TM_SLICE_MLTPLR) + (stW_prms.cals[stW_veh_offset_c+stW_cfg_c].TimeOut_c[stW_level_c]>>1) );
	}
	else {
		/* Timer is inactive */
		stW_TimeOut_Cycles_w = 0;
	}
}

/*********************************************************************************/
/*                                stWF_Rdg_Temperature                           */
/*********************************************************************************/
/* This function performs two readings via the CAN bus:                          */
/* 1. Steering Wheel Temperature: stored in stW_temperature_c                    */
/* 2. Steering Wheel Temperature Sensor Fault Status: stored in 				 */
/* stW_tmprtr_sensor_flt_c 														 */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* stWF_Updt_Status :                                                            */
/*    stWF_Rdg_Temperature()                                                     */
/*                                                                               */
/*********************************************************************************/
@far void stWF_Rdg_Temperature(void)
{
	/* Check for a 2 second delay after a POR before starting to read */
	/* steering wheel temperature and sensor fault status */
	/* Check if Allow_stWheel_mngmnt_b is set to TRUE. */
	/* MKS ID: 27476 */
	if(Allow_stWheel_mngmnt_b == TRUE){
		
		/* Read Wheel Temperature and Sensor Fault Status */
		stW_temperature_c = canio_RX_StW_TempSts_c;		
		stW_tmprtr_sensor_flt_c = canio_RX_StW_TempSens_FltSts_c;

	}else{
		/* stW_Temp and stW_TempSensor_Flt signals reception details. */
		/* stW_Actn_Rq: Cyclic: 500ms, Launch type: Cyclic+Change, Vehicle: L-series */
		
		/* We multiply the Cyclic time by two to unsure valid signal reception */
		/* before we start looking into signals (stW_Temp and stW_TempSensor_Flt) on the CAN-I bus. */
		
		/* Two second delay has not passed yet. Keep using initiliazation values. */
	}
}

/*********************************************************************************/
/*                                stWF_Upt_Lvl                                   */
/*********************************************************************************/
/* This function updates the steering wheel level when: 						 */
/* 1. New valid switch request is present               						 */
/* 2. Steering wheel level timeout occurs               						 */
/*                                                      						 */
/* Under normal mode/Remote/Autostart level changes are:                 		 */
/* WL0 to WL4                                           						 */
/* WL4 to WL3 (With timeout only)												 */											
/* WL3 to WL2 (with timeout only)                       						 */
/* WL2 to WL1 (with timeout only)                       						 */
/* WL1 to WL0 (with timeout only)                       						 */
/*                                                      						 */
/*                                                      						 */
/* Calls from                                           						 */
/* --------------                                       						 */
/* stWF_Swtch_Cntrl :                                   						 */
/*    stWF_Upt_Lvl()                                    						 */
/* stWF_Timer :                                         						 */
/*    stWF_Upt_Lvl()                                    						 */
/*                                                      						 */
/* Calls to                                             						 */
/* --------                                             						 */
/* stWF_Clear()                                         						 */
/*																				 */
/*********************************************************************************/
@far void stWF_Upt_Lvl(void)
{
	/* MKS 47354 : HSW should go 100% while in Remote start */
	/* Step down sequence is same for all cases now with this change */
	/* Harsha Commented the else path and taken out the conditions .. 02/18/10 */
	
//	/* Are we in normal operation mode? (No Remote Start) Or Auto Start FTR00139669 */
//	if (/*(main_state_c == NORMAL) || (main_state_c == AUTO_STRT_VENTING) || (main_state_c == AUTO_STRT_USER_SWTCH) || (main_state_c == AUTO_STRT_HEATING)*/1) {
		/* Check stwheel level */
		switch (stW_level_c) {

		case WL0:
		{
			/* Transfer from WL0 to WL4 */
			stW_level_c = WL4;
			break;
		}

		case WL2:
		{
			/* Transfer from WL2 to WL1 */
			stW_level_c = WL1;
			break;
		}

		case WL3:
		{
			/* Transfer from WL3 to WL2 */
			stW_level_c = WL2;
			break;
		}
		
		case WL4:
		{
			/* Transfer from WL4 to WL3 */
			stW_level_c = WL3;
			break;
		}

		default :
		{
			/* Turn off or keep stwheel off */
			stWF_Clear();
		}
		}
//	}
//	else { 
//			/* Remote Start mode */
//			if (stW_level_c == WL0) {
//				/* Transfer from WL0 to WL2 */
//				stW_level_c = WL2;
//			}
//			else {
//				if (stW_level_c == WL2) {
//					/* Transfer from WL2 to WL1 (only with timeout) */
//					stW_level_c = WL1;
//				}
//				else {
//					/* Turn off or keep stwheel off */
//					stWF_Clear();
//				}
//			}
//		}
}


/*********************************************************************************/
/*                                stWF_Updt_Pwm                                  */
/*********************************************************************************/
/* Present level duty cycle is passed as a parameter to the 				     */
/* mainF_Calc_Pwm function. 													 */
/* Result is the pwm value for the present steering wheel level.                 */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* stWF_Chck_Auto_Lvl_Updt :                                                     */
/*    stWF_Updt_Pwm()                                                            */
/* stWF_Chck_Swtch_Prps :                                                        */
/*    stWF_Updt_Pwm()                                                            */
/* stWF_Timer :                                                                  */
/*    stWF_Updt_Pwm()                                                            */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* u_16Bit mainF_Calc_PWM(u_8Bit main_duty_cycle_c)                              */
/*																				 */
/*********************************************************************************/
@far void stWF_Updt_Pwm(void)
{
	/* Check the wheel level */
	if ( (stW_level_c > WL0) && (stW_level_c <= WL4) ) {
		/* Calculate the pwm for wheel level */
		stW_pwm_w = mainF_Calc_PWM(stW_prms.cals[stW_veh_offset_c+stW_cfg_c].Pwm_a[stW_level_c]);
	}
	else {
		/* Keep Pwm at zero */
		stW_pwm_w = 0;
	}
}

/*********************************************************************************/
/*                           stWF_Diag_IO_Updt_Lvl                               */
/*********************************************************************************/
/* This function selects the next steering wheel level WL0, WL1, WL2, WL3 or WL4 */
/* using the diagnostic request data. 											 */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* stWF_Diag_IO_Cntrl :                                                          */
/*    stWF_Diag_IO_Updt_Lvl()                                                    */
/*                                                                               */
/*********************************************************************************/
@far void stWF_Diag_IO_Updt_Lvl(void)
{
	/* Check for valid wheel request */
	if ( (diag_io_st_whl_rq_c > WL0) && (diag_io_st_whl_rq_c <= WL4) ) {
		/* Update wheel level with diagnostic data */
		stW_level_c = diag_io_st_whl_rq_c;
	}
	else {
		/* Keep wheel level at WL0 */
		stW_level_c = WL0;
	}
}

/*********************************************************************************/
/*                        stWF_Diag_Timeout_Updt_Lvl                             */
/*********************************************************************************/
/* This function is used to step down to next stwheel level when a timeout 		 */
/* occurs during diagnostic mode. 												 */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* stWF_Timer :                                                                  */
/*    stWF_Diag_Timeout_Updt_Lvl()                                               */
/*                                                                               */
/*********************************************************************************/
@far void stWF_Diag_Timeout_Updt_Lvl(void)
{
	/* Check for valid wheel request */
	if ( (diag_io_st_whl_rq_c > WL0) && (diag_io_st_whl_rq_c <= WL4) ) {
		/* Step down a level */
		diag_io_st_whl_rq_c -= 1;
	}
	else {
		/* Keep wheel level at zero */
		diag_io_st_whl_rq_c = WL0;
	}
}

/*********************************************************************************/
/*                        stWLF_Chck_Diag_TurnOff                                */
/*********************************************************************************/
/* This function checks whether stWheel diagnostic request has to be turned off  */
/* or not. 																		 */
/* It is called from StWF_Timer function when we get a timeout.                  */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* stWF_Timer :                                                                  */
/*    stWLF_Chck_Diag_TurnOff()                                                  */
/*                                                                               */
/*********************************************************************************/
@far void stWLF_Chck_Diag_TurnOff(void)
{
	/* Check for a timeout during Wheel level 1 (in diagnostic mode) */
	if ( (diag_io_st_whl_rq_lock_b == TRUE) && (stW_level_c == WL1) ) {
		/* Clear Diagnostic request */
		diag_io_st_whl_rq_c = 0;
	}
	else {
		/* Do not clear the diagnostic request */
	}
}

/*********************************************************************************/
/*                              stWF_Timer                                       */
/*********************************************************************************/
/* This function starts a counter (stW_onTime_w) as soon as the steering wheel   */
/* heater is turned on.     													 */
/*                                                                               */
/* It monitors the on time to determine if a time out limit is reached or not.   */
/*                                                                               */
/* stWheelF_Timer                                                                */
/* +----------------++---------------+---------+---------------+---------+-------+ */
/* |                ||       1       |    2    |       3       |    4    |   5   | */
/* +================++===============+=========+===============+=========+=======+ */
/* |stW_onTime_w    ||       <       |    <    |       <       |    <    |  >=   | */
/* |                ||    TimeOut    | TimeOut |    TimeOut    | TimeOut |TimeOut| */
/* +----------------++---------------+---------+---------------+---------+-------+ */
/* |stW_state_b     ||   WHEEL_ON    |WHEEL_OFF|   WHEEL_OFF   |WHEEL_OFF|   -   | */
/* +----------------++---------------+---------+---------------+---------+-------+ */
/* |stW_Flt_status_w||STW_NO_FLT_MASK|    -    |      !=       |    -    |   -   | */
/* |                ||               |         |STW_NO_FLT_MASK|         |       | */
/* +----------------++---------------+---------+---------------+---------+-------+ */
/* |stW_status_c    ||       -       |   !=    |      !=       |  UGLY   |   -   | */
/* |                ||               |  UGLY   |     UGLY      |         |       | */
/* +================++===============+=========+===============+=========+=======+ */
/* |stW_onTime_w    ||               |         |               |         |       | */
/* |=               ||       X       |         |               |         |       | */
/* |stW_onTime_w+1  ||               |         |               |         |       | */
/* +----------------++---------------+---------+---------------+---------+-------+ */
/* |stW_onTime_w    ||               |         |               |         |       | */
/* |=               ||               |    X    |       X       |         |       | */
/* |stW_onTime_w    ||               |         |               |         |       | */
/* +----------------++---------------+---------+---------------+---------+-------+ */
/* |stW_onTime_w    ||               |         |               |         |       | */
/* |=               ||               |         |               |    X    |   X   | */
/* |0               ||               |         |               |         |       | */
/* +----------------++---------------+---------+---------------+---------+-------+ */
/*                                                                               */
/* SD Text                                                                       */
/* -------                                                                       */
/* Supports stepping down a stwheel level automatically with time outs.          */
/* Whenever timeout occurs, it updates the stwheel level and selects the 		 */
/* appropriate pwm for new level. 												 */
/*                                                                               */
/* Default timeouts for stwheel levels are:                                      */
/* WL1: 30 minutes (10% PWM)                                                     */
/* WL2: 30 minutes (35% PWM)                                                     */
/* WL3: 15 minutes (65% PWM)                                                     */
/* WL4: 15 minutes (100% PWM)													 */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* main :                                                                        */
/*    stWF_Timer()                                                               */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* stWF_Cal_TmOut()                                                              */
/* stWF_Updt_Pwm()                                                               */
/* stWF_Diag_Timeout_Updt_Lvl()                                                  */
/* stWF_Clear()                                                                  */
/* stWF_Upt_Lvl()                                                                */
/* stWLF_Chck_Diag_TurnOff()                                                     */
/*																				 */
/*********************************************************************************/
@far void stWF_Timer(void)
{
	/* Calculate the timeout for the wheel level */
	stWF_Cal_TmOut();
	
	/* Check for a timeout */
	if (stW_onTime_w < stW_TimeOut_Cycles_w) {
		/* Check if wheel heating is on */
		if ( (stW_state_b == TRUE) && (stW_Flt_status_w == STW_NO_FLT_MASK) ) {
			/* Increment the wheel onTime */
			stW_onTime_w++;
		}
		else {
			/* Check for matured faults */
			if (stW_status_c != UGLY) {
				/* Suspend the timer */
			}
			else {
				/* Reset the timer */
				stW_onTime_w = 0;
			}
		}
	}
	else {
		/* Reset the timer */
		stW_onTime_w = 0;
		
		/* Check if wheel level is WL2 or WL3 */
		if ( (stW_level_c > WL1) && (stW_level_c <= WL4) ) {
			/* Check diagnostic control */
			if (stW_diag_cntrl_act_b == FALSE) {
				/* Update stWheel level */
				stWF_Upt_Lvl();
			}
			else {
				/* Update diagnostic level due to timeout */
				stWF_Diag_Timeout_Updt_Lvl();
			}
			/* Update pwm */
			stWF_Updt_Pwm();
		}
		else
		{
			/* Check Diagnostic TurnOff */
			stWLF_Chck_Diag_TurnOff();
			
			/* Clear stWheel variables */
			stWF_Clear();
		}
	}
}

/*********************************************************************************/
/*                        stWF_Updt_Status                                       */
/*********************************************************************************/
/* Inforamtion used to determine stwheel status are:                             */
/* 1. StWheel fault status                                                       */
/* 2. Relay C internal fault status                                              */
/* 3. StWheel temperature                                                        */
/*																				 */
/* If above information is in operation range, main CSWM status is taken as the  */
/* stwheel status.                                                               */
/* Otherwise, stwheel status is set to "UGLY". Low-side can not be turned on.    */
/* Only short-term LED display.                                                  */
/*                                                                               */
/* This function also diactivates the LED display (by setting stW_LED_off_b) as  */
/* soon as stwheel status is set to "UGLY" for the first time (ex: setting a     */
/* stwheel short to battery fault). 											 */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* stWF_Manager :                                                                */
/*    stWF_Updt_Status()                                                         */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* stWF_Rdg_Temperature()                                                        */
/*																				 */
/*********************************************************************************/
@far void stWF_Updt_Status(void)
{
	/* Local variable(s) */
	u_8Bit stW_prv_stat_c;        //stores the previous stWheel status
	
	/* Store the previous steering wheel status */
	stW_prv_stat_c = stW_status_c;
	
	/* Read Wheel Temperature, ambient temperature and Sensor Fault Status */
	stWF_Rdg_Temperature();
	
	/* Update stWheel status */
	/* Check wheel temp, relay C status, and stwheel fault status */
	if ( (stW_Flt_status_w <= STW_PRL_FLT_MAX) && (relay_C_internal_Fault_b == FALSE) && 
		 (stW_temperature_c <= stW_prms.cals[stW_veh_offset_c+stW_cfg_c].MaxTemp_Threshld) && (stW_temperature_c != STW_TEMP_SNA) ) {
		/* stWheel status is equal to main CSWM status */
		stW_status_c = main_CSWM_Status_c;
	}
	else {
		/* We have a matured stW fault or temperature (ambient and/or output) is too high. */
		stW_status_c = UGLY;
	}
	
	/* Check if we have to turn off the LED display */
	if ( (stW_prv_stat_c != UGLY) && (stW_status_c == UGLY) ) {
		/* Set the bit that turns off the LED display - we come here only once when a fault is matured */
		stW_LED_off_b = TRUE;
	}
}

/*********************************************************************************/
/*                        stWLF_Get_Swtch_Rq                                     */
/*********************************************************************************/
/* Function Name: @far u_8Bit stWLF_Get_Swtch_Rq(void)						     */
/* Type: Local Function															 */
/* Returns: u_8Bit (current switch request)                                      */
/* Parameters: None																 */
/* Description: This function returns the steering wheel switch requests for     */
/* all vehicle lines.															 */
/*																			     */
/* Calls to: None																 */
/* Calls from:																	 */	
/*    stWF_Swtch_Mngmnt()														 */
/*    stWF_Swtch_Cntrl()														 */
/*																				 */ 
/*********************************************************************************/
@far u_8Bit stWLF_Get_Swtch_Rq(void)
{

	/* Modified on 04.14.2009: MKS Change Request ID: 28084 */	
	/* Decleration and initiliazation of local variables */
	u_8Bit swtch_rq = STW_SNA;
	
	swtch_rq = canio_RX_HSW_RQ_TGW_c;	


//	/* Note: First time powered on modules will look for switch request from HVAC for first 2 seconds. */
//	/* Check if CCN is the node who is sending the stW request. D and W series vehicles. */
//	if( (main_SwReq_Front_c == VEH_BUS_CCN_FRONT_REAR_K) || main_SwReq_stW_c == VEH_BUS_CCN_FRONT_REAR_K )
//	{
//	// store the current switch request from the CCN for D and W vehicle lines
//		swtch_rq = canio_HSW_RQ_c;		
//	}else{
//		// store the current switch request from the HVAC for L vehicle lines and RM/RT series 
//		swtch_rq = canio_RX_HSW_RQ_TGW_c;	
//	}
	
	return(swtch_rq);
	
	/* End of modifications: MKS Change Request ID: 28084 */
}

/*********************************************************************************/
/*                        stWF_Swtch_Mngmnt                                      */
/*********************************************************************************/
/* This function determines the switch (or diagnostic) purpose of incoming       */
/* signal.                                                                       */
/* After receiving a valid switch (or diagnostic) request, it sets the stWheel   */
/* switch request purpose (stW_swtch_rq_prps_b) as WHEEL_ON or WHEEL_OFF.        */
/*                                                                               */
/* Note: Switch purpose is an important information. All the automatic 			 */
/* recoveries (load shed 2 to 0, preliminary faults, etc) is done using this     */
/* information (see stWF_Manager). 												 */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* stWF_Manager :                                                                */
/*    stWF_Swtch_Mngmnt()                                                        */
/*																				 */
/*********************************************************************************/
@far void stWF_Swtch_Mngmnt(void)
{
	u_8Bit stW_rq = 0;           // current stw request
	
	/* Check if Diagnostic IO Control is inactive                                                                     */
	if ( (diag_io_st_whl_rq_lock_b == FALSE) && (diag_io_st_whl_state_lock_b == FALSE) && (diag_rc_all_op_lock_b == FALSE) ) {
		
		/* Read the current switch request */
		stW_rq = stWLF_Get_Swtch_Rq();
		
		/* @Switch Management */
		/* Check HSW_RQ signal */
		if ( (stW_rq == STW_PSD) || (stW_rq == STW_NOT_PSD) ) {
			/* Check for a valid switch request */
			if ( (stW_rq == STW_PSD) && (stW_lst_sw_rq_c == STW_NOT_PSD) ) {
				/* Determine whether switch purpose is to turn on */
				if (stW_swtch_rq_prps_b == FALSE) {
					/* StW is turned on */
					stW_swtch_rq_prps_b = TRUE;
					
					/* New switch request. Reset the timer. */
					stW_onTime_w = 0;
					stW_LED_Dsply_Tm_cnt = 0;
				}
				else {
					/* StW is turned off */
					stW_swtch_rq_prps_b = FALSE;
				}
			}
			else {
				/* Not a valid switch request. Keep the switch purpose unchanged. */
			}
		}
		else {
			/* Check for STW_SNA signal */
			if(stW_rq == STW_SNA){
				/* StW is turned off */
				stW_swtch_rq_prps_b = FALSE;
				
			}
			else{
				/* Not a valid signal. Do not act on it. */
			}
		}
		
		/* Store the last switch request */
		stW_lst_sw_rq_c = stW_rq;
	}
	else {
		/* @Diagnostic Management */
		
		/* Check if DCX Routine Control Output Test flag is active */
		if(diag_rc_all_op_lock_b == TRUE){			
			/* Is the test running for heaters? */
			if(diag_rc_all_heaters_lock_b == TRUE){
				/* StW is turned on */
				stW_swtch_rq_prps_b = TRUE;					
			}else{
				/* StW is turned off */
				stW_swtch_rq_prps_b = FALSE;
			}		
		}else{
			/* Check if diagnostic LED lock is inactive */
			if (diag_io_st_whl_state_lock_b == FALSE) {
				/* Check for valid Diagnostic IO request */
				if ( (diag_io_st_whl_rq_c > WL0) && (diag_io_st_whl_rq_c <= WL4) ) {
					/* StW is turned on */
					stW_swtch_rq_prps_b = TRUE;
				}
				else {
					/* StW is turned off */
					stW_swtch_rq_prps_b = FALSE;
				}
			}
			else {
				/* Only LED Display */
			}			
		}
	}
}

/*********************************************************************************/
/*                        stWF_Fnsh_LED_Dslpy                                    */
/*********************************************************************************/
/* Called at the end of a short-term (default 2 seconds) LED display routine in  */
/* order to clear the appropriate bit flags and/or variables. 					 */	
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* stWF_LED_Display :                                                            */
/*    stWF_Fnsh_LED_Dslpy()                                                      */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* stWF_Clear()                                                                  */
/* 																				 */
/*********************************************************************************/
@far void stWF_Fnsh_LED_Dslpy(void)
{
	/* Clear stW */
	stWF_Clear();
	
	/* Check if Diagnostic IO Control lock is active */
	if (diag_io_st_whl_rq_lock_b == TRUE) {
		/* Clear Diagnostic request flag in order to support short term LED display */
		diag_io_st_whl_rq_c = 0;
	}
	
	/* Check if Diagnostic LED lock is active */
	if (diag_io_st_whl_state_lock_b == TRUE) {
		/* Clear Diagnostic LED flag in order to support short term LED display */
		diag_io_st_whl_state_c = 0;
	}
}

/*********************************************************************************/
/*                        stWF_LED_Display                                       */
/*********************************************************************************/
/* This function is responsible to send a message on the CAN bus in order to     */
/* turn on/off the steering wheel heater light emitting diode (LED). 			 */
/*                                                                               */
/*                                                                               */
/* stWheelF_Update_LED_Display                                                   */
/* +----------------------------++---------+--------+-----+---------+---------+-----+ */
/* |                            ||    1    |   2    |  3  |    4    |    5    |  6  | */
/* +============================++=========+========+=====+=========+=========+=====+ */
/* |                            ||   RUN   |  RUN   | RUN |   RUN   |   RUN   |     | */
/* |canio_RX_IGN_STATUS_c          ||   ||    |   ||   | ||  |   ||    |   ||    |other| */
/* |                            ||  START  | START  |START|  START  |  START  |     | */
/* +----------------------------++---------+--------+-----+---------+---------+-----+ */
/* |stW_LED_off_b               ||  TRUE   | FALSE  |FALSE|  FALSE  |  FALSE  |  -  | */
/* +----------------------------++---------+--------+-----+---------+---------+-----+ */
/* |stW_swtch_rq_prps_b         ||WHEEL_OFF|WHEEL_ON|  -  |WHEEL_OFF|WHEEL_OFF|  -  | */
/* +----------------------------++---------+--------+-----+---------+---------+-----+ */
/* |stW_LED_support_b           ||    -    |   -    |TRUE |  FALSE  |  FALSE  |     | */
/* |                            ||         |        |     |         |         |     | */
/* +----------------------------++---------+--------+-----+---------+---------+-----+ */
/* |diag_io_stw_whl_state_lock_b||    -    |   -    |  -  |  TRUE   |  FALSE  |     | */
/* |                            ||         |        |     |         |         |     | */
/* +----------------------------++---------+--------+-----+---------+---------+-----+ */
/* |stW_status_c                ||    -    |   !=   |UGLY |    -    |    -    |     | */
/* |                            ||         |  UGLY  |     |         |         |     | */
/* +============================++=========+========+=====+=========+=========+=====+ */
/* |HSW_LED_Status              ||         |        |     |         |         |     | */
/* |=                           ||         |   X    |     |    X    |         |     | */
/* |Continuous                  ||         |        |     |         |         |     | */
/* +----------------------------++---------+--------+-----+---------+---------+-----+ */
/* |HSW_LED_Status              ||         |        |     |         |         |     | */
/* |=                           ||         |        |     |         |         |     | */
/* |2                           ||         |        |  X  |         |         |     | */
/* |Sec                         ||         |        |     |         |         |     | */
/* |Display                     ||         |        |     |         |         |     | */
/* +----------------------------++---------+--------+-----+---------+---------+-----+ */
/* |HSW_LED_Status              ||         |        |     |         |         |     | */
/* |=                           ||         |        |     |         |         |     | */
/* |No                          ||    X    |        |     |         |    X    |  X  | */
/* |LED                         ||         |        |     |         |         |     | */
/* |display                     ||         |        |     |         |         |     | */
/* +----------------------------++---------+--------+-----+---------+---------+-----+ */
/*                                                                               */
/*                                                                               */
/* SD Text                                                                       */
/* -------                                                                       */
/* Three main parts of this function are:                                        */
/*                                                                               */
/* 1. Checking if a fault is just matured. If yes, it finishes the LED display   */
/* right away for stwheel output.                                  				 */
/* 2. Arbitrating LED display logic. Deciding on whether continious, short-term, */
/* or no LED display is appropriate.                             				 */
/* 3. Perfoming the LED display by manipulating and sending the appropriate      */
/* stwheel LED signal on the CAN bus.                                 			 */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* IntFlt_F_ClearOutputs :                                                       */
/*    stWF_LED_Display()                                                         */
/* stWF_Manager :                                                                */
/*    stWF_LED_Display()                                                         */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* stWF_Clear()                                                                  */
/* stWF_Fnsh_LED_Dslpy()                                                         */
/* dbkGetTxHSW_Stat( variable, value)                                            */
/* IlPutTxHSW_StatSts(c)( value)                                                      */
/* SetValueTxHSW_Stat( variable, value)                                          */
/*																				 */
/*********************************************************************************/
@far void stWF_LED_Display(void)
{
	/* @Check if a fault is just matured */
	if (stW_LED_off_b == TRUE) {
		
		/* Set counter to finish LED Display */
		stW_LED_Dsply_Tm_cnt = stW_LED_Dsply_Tm;
		
		/* Clear LED off flag */
		stW_LED_off_b = FALSE;
	}
	else {
		/* No need to turn on the LED due to matured fault */
	}
	
	/* @Arbitrate LED display */
	/* Check ignition status */
	if ( (canio_RX_IGN_STATUS_c == IGN_RUN) || (canio_RX_IGN_STATUS_c == IGN_START) ) {
		
		/* Check switch purpose */
		if (stW_swtch_rq_prps_b == TRUE) {
			
			/* Check if there is no matured faults */
			if (stW_status_c != UGLY) {
				/* CONTINIOUS LED DISPLAY */
				stW_LED_stat_c = TRUE;
			}
			else {
				/* Short-term LED DISPLAY */
				if (stW_LED_Dsply_Tm_cnt < stW_LED_Dsply_Tm) {
					/* Count LED Display time */
					stW_LED_Dsply_Tm_cnt++;
					
					/* Set LED Stat ON */
					stW_LED_stat_c = TRUE;
				}
				else {					
					/* Is Chrysler Routine Control (Output Test) running? */
					if(diag_rc_all_op_lock_b == FALSE){
						/* Regular Logic */
						/* Finish short term LED Display */
						stWF_Fnsh_LED_Dslpy();						
					}else{
						/* Special case when we have Chrysler Routine Control Output test */
						/* We matured a DTC during the test. Set LED Stat OFF */
						stW_LED_stat_c = FALSE;
					}
				}
			}
		}
		else {
			/* Check if diagnostic LED lock is active */
			if (diag_io_st_whl_state_lock_b == TRUE) {
				/* Continuous display for LED I/O Control (ignore fault status) */
				
				/* New: Added on 11.06.2008. Since we are doing LED display when */
				/* LED status changes, We need to update the stW_LED_stat_c variable here */
				/* Check LED state */
				if ( (diag_io_st_whl_state_c > WL0) && (diag_io_st_whl_state_c <= WL4) ) {
					/* Set LED Stat ON */
					stW_LED_stat_c = TRUE;
				}
				else {
					/* Set LED Stat OFF. Diagnostic Request is OFF for WL0 */
					stW_LED_stat_c = FALSE;
				}
			}
			else {
				/* Clear stw. */
				stWF_Clear();
			}
		}
	}
	else {
		/* Clear stw. */
		stWF_Clear();
	}
	
	/* @Perform LED Display */
	
	/* Update the LED status only incase the previously stored status is different */
	/* than the present for steering wheel */
	if(stW_prev_LED_stat_c != stW_LED_stat_c){
		
		/* Get the StWheel Container */
	   //	Local_HSW_Stat.c = dbkGetTxHSW_Stat();
		
		/* Set the LED status */
	   //	SetValueTxHSW_Stat(Local_HSW_Stat, stW_LED_stat_c);

		/* Trasmit the LED status to the Bus */
		IlPutTxHSW_StatSts(stW_LED_stat_c);
		
		/* Update the previous LED status */
		stW_prev_LED_stat_c = stW_LED_stat_c;
		
	}else{
		/* No change in the LED status for steering wheel */
	}
}

/*********************************************************************************/
/*                        stWF_Output_Control                                    */
/*********************************************************************************/
/* This function is responsible to turn on/off the high-side steering wheel      */
/* driver.                                                           			 */
/*                                                                               */
/* stWheelF_PortHandler                                                          */
/* +------------++--------+---------+-----+                                      */
/* |            ||   1    |    2    |  3  |                                      */
/* +============++========+=========+=====+                                      */
/* |stW_sw_rq_c ||WHEEL_ON|WHEEL_OFF|  -  |                                      */
/* +------------++--------+---------+-----+                                      */
/* |relay_C_Ok_b||  TRUE  |    -    |FALSE|                                      */
/* +============++========+=========+=====+                                      */
/* |stW_state_b ||        |         |     |                                      */
/* |=           ||   X    |         |     |                                      */
/* |WHEEL_ON    ||        |         |     |                                      */
/* +------------++--------+---------+-----+                                      */
/* |stW_state_b ||        |         |     |                                      */
/* |=           ||        |    X    |  X  |                                      */
/* |WHEEL_OFF   ||        |         |     |                                      */
/* +------------++--------+---------+-----+                                      */
/*                                                                               */
/*                                                                               */
/* SD Text                                                                       */
/* -------                                                                       */
/* Turn on the high-side stwheel output by pwm'ing it if relay C is OK and there */
/* is a softare request (stW_sw_rq_b is set, see stWF_Manager). 				 */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* main :                                                                        */
/*    stWF_Output_Control()                                                      */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* Pwm_SetDutyCycle(Pwm_ChannelType ChannelNumber,uint16 DutyCycle)              */
/*																				 */
/*********************************************************************************/
@far void stWF_Output_Control(void)
{	
	/* Check if stWheel sw request is present */
	if ( (stW_sw_rq_b == TRUE) && (relay_C_Ok_b == TRUE) )
	{
		/* Pwm the STWheel output (High side) */			
		/* Call the function only if current pwm is different then previous pwm */
		if(stW_prev_pwm != stW_pwm_w){
			Pwm_SetDutyCycle(PWM_STW, stW_pwm_w);
		}else{
			/* No change in pwm. No need to call Pwm_SetDutyCycle */
		}
		/* Update the previous pwm variable in order to detect a pwm change */
		stW_prev_pwm = stW_pwm_w;	// do not use stW_prev_pwm when calling Pwm_SetDutyCycle
		
		/* Set StW Port On */
		stW_state_b = TRUE;
		
		/* StWheel high side driver is turned on. Clear Open or Short to Battery fault detection 100ms delay counter */
		stW_chck_Open_or_STB_dly_c = 0;
	}
	else
	{							
		/* Check if PWM is active */
		if( (stW_state_b == TRUE) || (stW_prev_pwm != 0) ){
			/* Turn off Pwm for STWheel output (low side) */
			Pwm_SetDutyCycle(PWM_STW, 0);	
			
			/*************************************************/
			/*** Begin code modification for MKS ID: 22112 ***/
			/*************************************************/
			DisableAllInterrupts();
			//TCTL1_OM7 = TRUE;   	// Set OM7 (in order to clear output) 
			//TCTL1_OL7 = FALSE;  	// Clear OL7
			CFORC_FOC7 = TRUE;		// Force output compare action for channel 7 while turning off (for immediate affect) 
			//TIE_C7I = FALSE; 		// disable channel 7 interrupt
			EnableAllInterrupts();
			/*************************************************/
			/*** End code modification for MKS ID: 22112   ***/
			/*************************************************/			
			
		}else{
			/* PWM is not active. No need to call Pwm_SetDutyCycle */
		}		
		
		/*************************************************/
		/*** Begin code modification for MKS ID: 22112 ***/
		/*************************************************/
		
		/* Fail-safe code attempt: 01.07.2009 CK */			
		/* Check PORT T Input Register (PTIT) bit 7. PTIT7 is used for steering wheel. */
		/* According to MC9S12XS datasheet: "A read always returns the buffered input */
		/* state of the associated pin. It can be used to detect overload or short circuit */
		/* conditions on output pins". */
		if(PTIT_PTIT7 == TRUE){
			/* Try turning off the steering wheel output */
			Pwm_SetDutyCycle(PWM_STW, 0);
		}else{
			/* Steering wheel output is not being PWMed. No action. */
		}		
		/*************************************************/
		/*** End code modification for MKS ID: 22112   ***/
		/*************************************************/
		
		/* Set previous pwm to zero */
		stW_prev_pwm = 0;
			
		/* StWheel low side is turned off. Clear Isense analysis delay counter */
		stW_Isense_dly_cnt_c = 0;
		
		/* Clear StW Port */
		stW_state_b = FALSE;
	}
}

/*********************************************************************************/
/*                        stWF_Chck_Swtch_Prps                                   */
/*********************************************************************************/
/* Turns on/off the software request (stW_sw_rq_b) that controls the stwheel     */
/* low-side driver. 															 */
/* It checks stwheel switch purpose and status.                                  */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* stWF_Diag_IO_Cntrl :                                                          */
/*    stWF_Chck_Swtch_Prps()                                                     */
/* stWF_Swtch_Cntrl :                                                            */
/*    stWF_Chck_Swtch_Prps()                                                     */
/* stWLF_RemSt_Chck :                                                            */
/*    stWF_Chck_Swtch_Prps()                                                     */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* stWF_Updt_Pwm()                                                               */
/* 																				 */
/*********************************************************************************/
@far void stWF_Chck_Swtch_Prps(void)
{
	/* Check for switch purpose */
	if (stW_swtch_rq_prps_b == TRUE)
	{
		/* Decide to serve or reject the switch request */
		if (stW_status_c == GOOD)
		{
			/* Everything is O.K. Update pwm */
			stWF_Updt_Pwm();
			
			/* Turn on the StW software request */
			stW_sw_rq_b = TRUE;
		}
		else
		{
			/* Check if stW_status is BAD */
			if (stW_status_c == BAD)
			{
				/* Still Update pwm, but output will stay off */
				stWF_Updt_Pwm();
			}
			else
			{
				/* Keep Pwm at zero */
				stW_pwm_w = 0;
			}
			/* Turn off the StW software request - Keep stW off */
			stW_sw_rq_b = FALSE;
		}
	}
	else
	{
		/* Turn off the StW software request */
		stW_sw_rq_b = FALSE;
	}
}

/*********************************************************************************/
/*                        stWF_Rtrn_Cntrl_toECU                                  */
/*********************************************************************************/
/* It turns off the stwheel low-side output (and clears all related variables)   */
/* after diagnostic session is completed. 										 */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* stWF_Manager :                                                                */
/*    stWF_Rtrn_Cntrl_toECU()                                                    */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* stWF_Clear()                                                                  */
/*																				 */
/*********************************************************************************/
@far void stWF_Rtrn_Cntrl_toECU(void)
{
	stW_diag_cntrl_act_b   = FALSE;    // diagnostics is inactive

	stW_Flt_Mtr_cnt        = 0;        // clear fault mature count
	
	/* Turn off StWheel and clear stWheel parameters */
	stWF_Clear();                      // clear stW variables
}

/*********************************************************************************/
/*                        stWF_Swtch_Cntrl                                       */
/*********************************************************************************/
/* Processes the switch request by:                      						 */
/*                                                       						 */
/* 1. Deciding if this is a valid switch request         						 */
/* 2. Updating stwheel level and checking switch purpose 						 */
/* 3. Setting the appropriate pwm for next level         						 */
/*                                                       						 */
/* Calls from                                            						 */
/* --------------                                        						 */
/* stWF_Manager :                                        						 */
/*    stWF_Swtch_Cntrl()                                 						 */
/*                                                       						 */
/* Calls to                                              						 */
/* --------                                              						 */
/* stWF_Chck_Swtch_Prps()                                						 */
/* stWF_Upt_Lvl()                                        						 */
/*																				 */
/*********************************************************************************/
@far void stWF_Swtch_Cntrl(void)
{
	u_8Bit stW_crt_rq = 0;           // current stw request
	
	/* Read the Switch Request */
	stW_crt_rq = stWLF_Get_Swtch_Rq();
	
	/* Check for valid switch request */
	if ( (stW_crt_rq == STW_PSD) && (stW_not_vld_swtch_rq_b == FALSE) ) {
		/* Disable switch */
		stW_not_vld_swtch_rq_b = TRUE;
		
		/* Update wheel level */
		stWF_Upt_Lvl();
		
		/* Check switch purpose */
		stWF_Chck_Swtch_Prps();
	}
	else {
		/* Disable switch until it is released */
		if (stW_crt_rq == STW_NOT_PSD) {
			/* Enable switch */
			stW_not_vld_swtch_rq_b = FALSE;
		}
		else {
			/* Disable switch */
			stW_not_vld_swtch_rq_b = TRUE;
		}
	}
}

/*********************************************************************************/
/*                        stWF_Diag_IO_Cntrl                                     */
/*********************************************************************************/
/* Activates the diagnostic IO control software.                                 */
/* It updates the stwheel level and checks switch purpose before modifying the   */
/* pwm. 																		 */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* stWF_Manager :                                                                */
/*    stWF_Diag_IO_Cntrl()                                                       */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* stWF_Diag_IO_Updt_Lvl()                                                       */
/* stWF_Chck_Swtch_Prps()                                                        */
/*																				 */
/*********************************************************************************/
@far void stWF_Diag_IO_Cntrl(void)
{
	/* Set diagnostic IO control active */
	stW_diag_cntrl_act_b = TRUE;
	
	/* Check if this is a new Diagnostic IO request */
	if (stW_clr_ontime_b == TRUE)
	{
		/* Reset the timer. */
		stW_onTime_w = 0;
		stW_LED_Dsply_Tm_cnt = 0;
		
		/* Clear ontime flag */
		stW_clr_ontime_b = FALSE;
	}
	else
	{
		/* Existing request */		
	}
	
	/* Check if DCX Routine Control Output Test flag is set */
	if(diag_rc_all_op_lock_b == TRUE){
		
		/* Check if we are running the test for heaters */
		if(diag_rc_all_heaters_lock_b == TRUE){
			
			/* Set steering wheel level to WL4 for DCX Routine Control Test */
			stW_level_c = WL4;
			
			/* Check Diagnostic IO request purpose */
			stWF_Chck_Swtch_Prps();
			
		}else{
			/* Heaters test is completed. Keep of the Steering Wheel for the remaining of the test */
			stWF_Clear();
		}
		
	}else{
		/* Check if Diagnostics LED control is inactive */
		if (diag_io_st_whl_state_lock_b == FALSE)
		{
			/* Update wheel level with diagnostic data */
			stWF_Diag_IO_Updt_Lvl();
			
			/* Check Diagnostic IO request purpose */
			stWF_Chck_Swtch_Prps();
		}
		else
		{
			/* Keep the StW software request off */
			stW_sw_rq_b = FALSE;
			
			/* Keep StW switch purpose off */
			stW_swtch_rq_prps_b = FALSE;
		}		
	}
}

/*********************************************************************************/
/*                        stWF_Chck_EFUSE_Rst (NOT USED)                         */
/*********************************************************************************/
/* EFUSE is disconnected automatically by the hardware if short to battery fault */
/* is present on the low-side driver.                    						 */
/*                                                                               */
/* This function can only be activated by a diagnostic command.                  */
/*                                                                               */
/* In order to reset the EFUSE perform the following:                            */
/* 1. Send $$2F $$D1 $$0C $$03 $$01 (diagnostic command to reset the EFUSE)      */
/* 2. Perform an ignition cycle (off-run).                                       */
/*                                                                               */
/* This function resets the EFUSE by:                                            */
/* 1. Setting Port Direction of Pin PP4 as an Output.                            */
/* 2. Setting PP4 pin High and waiting 10ms.                                     */
/* 3. Setting Port Direction of Pin PP4 as an Input (after 2nd step).            */
/* Note: Ignition cycle has to be performed to be able to activate the stwheel   */
/* low-side driver again (storing short to battery fault). 						 */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* stWF_Manager :                                                                */
/*    stWF_Chck_EFUSE_Rst()                                                      */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* Port_SetPinDirection(Port_PinType Pin,Port_PinDirectionType Direction)        */
/* uint8 Dio_ReadChannel( ChannelId)                                             */
/* Dio_WriteChannel( ChannelId, Level)                                           */
/*																				 */
/*********************************************************************************/
//@far void stWF_Chck_EFUSE_Rst(void)
//{
//	/* Check EFUSE reset */
//	if (stW_EFUSE_reset_b == TRUE)
//	{
//		/* Set Port Direction as Input */
////		Port_SetPinDirection (PORT_P_EFUSE_WHEEL_IO_DIR, PORT_PIN_IN);
//		
//		/* Clear EFUSE reset flag */
//		stW_EFUSE_reset_b = FALSE;
//		
//		/* Clear reset wheel EFUSE IO request */
//		diag_rst_stwheel_EFUSE_b = FALSE;
//	}
//	else
//	{
//		/* Read wheel EFUSE state */
////		stW_EFUSE_state = (u_8Bit) Dio_ReadChannel(PORT_P_EFUSE_WHEEL_IO);
//		
//		/* Check EFUSE state */
////		if (stW_EFUSE_state == STW_EFUSE_OFF)
////		{
//			/* Check if low side is turned off */
//			if (stW_state_b == FALSE)
//			{
//				/* Set Port Direction as Output */
////				Port_SetPinDirection (PORT_P_EFUSE_WHEEL_IO_DIR, PORT_PIN_OUT);
//				
//				/* Set output pin high */
////				Dio_WriteChannel(PORT_P_EFUSE_WHEEL_IO, STD_HIGH);
//				
//				/* Set EFUSE reset flag */
//				stW_EFUSE_reset_b = TRUE;
//			}
//			else
//			{
//				/* Wait until low side turns off */
//			}
////		}
////		else
////		{
//			/* EFUSE is ON */
////		}
//	}
//}

/*********************************************************************************/
/*                        stWLF_RemSt_Chck                                       */
/*********************************************************************************/
//* This function checks if automatic activation is required for steering wheel  */
/* output in remote start event.                              					 */
/*                                                                               */
/* If it is required, it updated the steering wheel parameters                   */
/* (level, switch purpose etc.) in order to perform the automatic activation. 	 */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* stWF_Manager :                                                                */
/*    stWLF_RemSt_Chck()                                                         */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* stWF_Chck_Swtch_Prps()                                                        */
/*																				 */
/*********************************************************************************/
@far void stWLF_RemSt_Chck(void)
{
	/* Check if remote start automatic stWheel activation is needed */
	if (main_state_c == REM_STRT_HEATING) {
		/* Did automatic activation take place yet? */
		if (stW_RemSt_auto_b == TRUE) {
			/* Automatic activation already started */
		}
		else {
			/* Update stWheel purpose */
			stW_swtch_rq_prps_b = TRUE;
			
			
			/* MKS 47354: Insertion point is WL4(100%) */
			/* Remote start insertion point: WL4 */
			stW_level_c = WL4;
			
			/* Check switch purpose */
			stWF_Chck_Swtch_Prps();
			
			/* Set the automatic activation bit */
			stW_RemSt_auto_b = TRUE;
		}
	}
	else {
		/* Do not perform automatic activation. */
	}

	/* FTR00139669 -- Autostart feature for PowerNet Series. The insertion point for Auto Start is WL3*/
	/* Check if remote start automatic stWheel activation is needed */
	if (main_state_c == AUTO_STRT_HEATING) {
		/* Did automatic activation take place yet? */
		if (stW_RemSt_auto_b == TRUE) {
			/* Automatic activation already started */
		}
		else {
			/* Update stWheel purpose */
			stW_swtch_rq_prps_b = TRUE;
			
			/* Steering Wheel insertion point */
			stW_level_c = WL4;
			
			/* Check switch purpose */
			stWF_Chck_Swtch_Prps();
			
			/* Set the automatic activation bit */
			stW_RemSt_auto_b = TRUE;
			
		}
	}
	else {
		/* Do not perform automatic activation. */
	}
	autostart_check_complete_b = TRUE; //Make sure Auto start complete flag check in main remote start check.

}

/*********************************************************************************/
/*                        stWLF_Chck_mainState                                   */
/*********************************************************************************/
/* This function is used to detect the remote start activation and deactivation  */
/* events.                														 */
/* Remote start deactivation (timeout): It turns off the steering wheel output   */
/* (calls the stWF_Clear). 														 */
/*                                                                               */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* stWF_Manager :                                                                */
/*    stWLF_Chck_mainState()                                                     */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* stWF_Clear()                                                                  */
/*																				 */
/*********************************************************************************/
@far void stWLF_Chck_mainState(void)
{
	 //u_8Bit stW_status = 0;           // current stw Status
	
	/* Check Remote Start Timeout */
	/* Check for Remote start timeout (before updating last main state) */
	if ( (stW_lst_main_state_c != NORMAL) && (main_state_c == NORMAL) ) {
		/* Clear remote start automatic activation bit (for both sides) */
		stW_RemSt_auto_b = FALSE;   // Clear remote start automatic activation bit

      		
		/* Make sure stWheel output is turned off at the end of remote start event */
		
		/* Harsha Commented for New requirement 05.12.2009 */
		/* MKS 28818: When remote start completes, new requirement says turned Outputs */
		/* Shall stay ON */
		//stWF_Clear();
		
		/* Harsha added new requirement .. 02/17/2010 .. For WK S2B Release */
		/* MKS 47354: When IGN status remains in RUN and Ign Run Remote Start turns Inactive */
		/* HSW will start back from 100% PWM */
		/* Harsha commented the below logic .. 02/18/2010 . Not needed to goback to 100%*/
		/* From start up, we go to 100%. New change */
//		stW_status = stWF_Actvtn_Stat(); //Check HSW is ON/OFF
//		/* If Already started, again restart with 100% PWM, else donothing to the previous state */
//		if (stW_status)
//		{
//			/* Reset the wheel ontime */
//			stW_onTime_w = 0;
//            /* Set Wheel PWM to 100% */
//			stW_level_c = WL3;
//		}

	}
	else {
		/* No remote start timeout */
	}
	
	/* Check Remote Start Activation */
	/* Check for Remote start timeout (before updating last main state) */
	if ( (main_state_c != NORMAL) && (stW_lst_main_state_c == NORMAL) ) {
		/* Is main state REM_STRT_HEATING? */
	   /* FTR00139669 -- Autostart feature for PowerNet Series.*/
		if ((main_state_c == REM_STRT_HEATING) || (main_state_c == REM_STRT_USER_SWTCH) ) {
			/* Reset the wheel ontime */
			stW_onTime_w = 0;
			
			/* MKS 47354: Insertion point is WL4(100%) */
			/* Remote start insertion point: WL4 */
			stW_level_c = WL4;
		}
//	/* FTR00139669 -- Autostart feature for PowerNet Series. The insertion point for Auto Start is WL3*/
		else {
				if ((main_state_c == AUTO_STRT_HEATING) || (main_state_c == AUTO_STRT_USER_SWTCH)){

					/* Reset the wheel ontime */
					stW_onTime_w = 0;
					/* Remote start insertion point: WL3 */
					stW_level_c = WL4;

				}
				else{
					/* Turn off the stWheel output */
					stW_level_c = WL0;
				}
		}

	}
	else {
		/* No remote start activation */
	}
	
	/* Update last main state */
	stW_lst_main_state_c = main_state_c;
}

/*********************************************************************************/
/*                        stWF_Chck_Auto_Lvl_Updt                                */
/*********************************************************************************/
/* Ouput ontime is checked in order to automatically update the wheel level when output exceeds the maximum  */
/* Temperature for the present level. 				                                                         */
/* With AL SW, the Temp Check Minimum times are given below.                                                 */
/* For WL4 Check Temerature time is set to 0 min.															 */													
/* For WL3 check temperature time is set to 0 minute.                            							 */
/* For WL2 check temperature time is set for 0 minutes.                          							 */
/* For WL1 check temperature time is set for 0 minutes.                          							 */
/* These timer values will be calibratable.                                  							     */
/*                                                                               							 */
/* Also From WL4 logic will stepdown to WL3 to WL2, in the following conditions.							 */
/* If the combined Rear Heated seats and Wheel Isense draws more than 14Amps.								 */
/* If the Wheel Isense draws more than 8Amps current.														 */
/*                                                                               							 */
/* Maximum wheel temperature is set to 45 C.                                     							 */
/* Maximum temperature for WL4 is set to 36 C.                                   							 */
/* Maximum temperature for WL3 is set to 39 C.                                   							 */
/* Maximum temperature for WL2 is set to 43 C.                                   						     */
/*                                                                               							 */
/* Calls from                                                                    */
/* --------------                                                                */
/* stWF_Manager :                                                                */
/*    stWF_Chck_Auto_Lvl_Updt()                                                  */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* stWF_Updt_Pwm()                                                               */
/*																				 */
/*********************************************************************************/
@far void stWF_Chck_Auto_Lvl_Updt(void)
{
		/* Check for automatic level update in WL4 */
	    /* OR Check for automatic level updates when Isense > 14 Amps FTR00139667 */
		/* OR If Level is 4 and sees Steering Wheel Isense > 8 Amps for more than 1 min*/
		/* MKS 92650 Heated Steering Wheel Issue If Level is 4 was added to last OR  */
		/* previous code just stW_Isense_limit_b == TRUE in last OR                 */
		if ( ((stW_level_c == WL4) && (stW_onTime_w >= stW_WL4TempChck_lmt_w) && (stW_temperature_c >= stW_prms.cals[stW_veh_offset_c+stW_cfg_c].WL4Temp_Threshld)) || 
			 ((stW_level_c == WL4) && (stW_RearHeat_Isense_c >= STW_REARSEATB_ISENSE_THRHLD ))  ||
			 ((stW_level_c == WL4) && (stW_Isense_limit_b == TRUE)) ) {
			/* Reset the wheel ontime */
			stW_onTime_w = 0;
			
			/* New wheel level is WL3 */
			stW_level_c = WL3;
			
			/* Update pwm for WL3 */
			stWF_Updt_Pwm();
		}
		else {
			
			//Clear the 8A Limit flag, as either we came out from WL4 or WL4 is not executed.
			//8A limit check is only applicable to WL4.
			stW_Isense_limit_b = FALSE;
			
			/* Check for automatic level update in WL3 */      
			if ( (stW_level_c == WL3) && (stW_onTime_w >= stW_WL3TempChck_lmt_w) && (stW_temperature_c >= stW_prms.cals[stW_veh_offset_c+stW_cfg_c].WL3Temp_Threshld) ) {
				/* Reset the wheel ontime */
				stW_onTime_w = 0;
				
				/* New wheel level is WL2 */
				stW_level_c = WL2;
				
				/* Update pwm for WL2 */
				stWF_Updt_Pwm();
			}
			else {

		
				/* Check for automatic level update in WL2 */      
				if ( (stW_level_c == WL2) && (stW_onTime_w >= stW_WL2TempChck_lmt_w) && (stW_temperature_c >= stW_prms.cals[stW_veh_offset_c+stW_cfg_c].WL2Temp_Threshld) ) {
					/* Reset the wheel ontime */
					stW_onTime_w = 0;
					
					/* New wheel level is WL1 */
					stW_level_c = WL1;
					
					/* Update pwm for WL1 */
					stWF_Updt_Pwm();
				}
				else
				{
	//				/* Check for automatic level update in WL1 .. Harsha added*/      
	//				if ( (stW_level_c == WL1) && (stW_onTime_w >= stW_WL1TempChck_lmt_w) && (stW_temperature_c >= stW_prms.WL1Temp_Threshld) ) {
	//					/* Reset the wheel ontime */
	//					stW_onTime_w = 0;
	//					
	//					/* New wheel level is WL0 */
	//					stW_level_c = WL0;
	//					
	//					/* Update pwm for WL0 */
	//					stWF_Updt_Pwm();
	//				}
	//				else
	//				{
	//				/* No automatic level update necassary due to exceeding temperature limit in WL3 or WL2 or WL1 */
	//				}
				}

			}

		}
		
}


/*********************************************************************************/
/*                        stWLF_Chck_Lsd2_Lvl_Reduction                       	 */
/*********************************************************************************/
/* Function Name: @far void stWLF_Chck_Lsd2_Lvl_Reduction(void) 				 */
/* Type: Local Function													         */
/* Returns: None                     											 */
/* Parameters: None 															 */
/* Description: This function implements the new PowerNet requirement.           */
/*              If CSWM is already �on� and receives the load-shed level 2       */
/*              signal from CAN I bus then, The heated steering wheel, if not    */
/*              already, will reduce the duty cycle to WL1.           	 		 */
/*																			     */
/* Calls to: None																 */
/* Calls from:																	 */	
/*    stWF_Manager()															 */
/*																				 */  
/*********************************************************************************/
//@far void stWLF_Chck_Lsd2_Lvl_Reduction(void)
//{
//	/* Updated on 04.14.2009 by CK to check Battery Reached Critical State signal before reducing level. */
//	/* Check if Load Shed 2 signal is present (without load shed 3, Load Shed 7 and Battery Reached Critical State) */
//	if( (canio_PN14_LS_Lvl2_c == TRUE) && (canio_PN14_LS_Lvl3_c == FALSE) &&
//		(canio_PN14_LS_Lvl7_c == FALSE) && (canio_Batt_ST_Crit_c == FALSE) && (stW_status_c == GOOD) )
//	{
//		/* Check present steering wheel level to determine if load reduction is necassary */
//		if( (stW_level_c == WL4) || (stW_level_c == WL3) || (stW_level_c == WL2))			
//		{
//			/* Reset the steering wheel ontime due to level update */
//			stW_onTime_w = 0;
//			/* Reduce steering wheel level to WL1 */
//			stW_level_c = WL1;
//			/* Update the steering wheel Pwm duty cycle with respect to WL1 */
//			stWF_Updt_Pwm();
//
//		}else{
//			/* Conditions are not met to reduce level. No action is needed. */
//		}	// Check steering wheel level else path end
//	}else{
//		/* Load Reduction Load Shed 2 condition is not present by it self or vs stW_status_c is not good */
//	}	// ==> (Output is not turned on if stW_status_c is not good). No action is needed.
//}

/*********************************************************************************/
/*                               stWF_Manager                                    */
/*********************************************************************************/
/* This function receives the user requests to turn on/off the steering wheel    */
/* heater.                                           							 */
/*                                                                               */
/* It determines whether to serve the turn on request by examining the cswm      */
/* output status, and steering wheel fault conditions. 							 */
/*                                                                               */
/* SD Text                                                                       */
/* -------                                                                       */
/* This function manages the stwheel output control by:                          */
/*                                                                               */
/* 1. Checking and updating stwheel status                                       */
/* 2. Receiving the switch or diagnostic requests                                */
/* 3. Arbitrating switch or diagnostic logic                                     */
/* 4. Controlling stwheel low-side driver                                        */
/* 5. Performing LED display                                                     */
/*                                                                               */
/* It also checks and serves EFUSE reset requests.                               */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* main :                                                                        */
/*    stWF_Manager()                                                             */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* stWF_Updt_Status()                                                            */
/* stWF_Swtch_Mngmnt()                                                           */
/* stWF_LED_Display()                                                            */
/* stWF_Rtrn_Cntrl_toECU()                                                       */
/* stWF_Swtch_Cntrl()                                                            */
/* stWF_Diag_IO_Cntrl()                                                          */
/* stWF_Chck_EFUSE_Rst()                                                         */
/* stWLF_RemSt_Chck()                                                            */
/* stWLF_Chck_mainState()                                                        */
/* stWF_Chck_Auto_Lvl_Updt()                                                     */
/*																				 */
/*********************************************************************************/
@far void stWF_Manager(void)
{
	/* @StW Manager General Tasks */
	/* Check main state (to detect Remote Start activation or diactivation) */
	stWLF_Chck_mainState();
	
	/* Update stWheel status */
	stWF_Updt_Status();
	
	/* Switch management */
	stWF_Swtch_Mngmnt();
	
	/* @Arbitrate Control */
	/* Check if Diagnostic IO Control is inactive */
	if ( (diag_io_st_whl_rq_lock_b == FALSE) && (diag_io_st_whl_state_lock_b == FALSE) && (diag_rc_all_op_lock_b == FALSE) )
	{
		/* Check if we need to return control to ECU */
		if (stW_diag_cntrl_act_b == FALSE)
		{	
			/* Switch Control */
			stWF_Swtch_Cntrl();
			
			/* Check for Remote Start automatic activation */
			stWLF_RemSt_Chck();
			
			//stWLF_Chck_Lsd2_Lvl_Reduction(); //Not used for this program
		}
		else
		{
			/* Return control to ECU */
			stWF_Rtrn_Cntrl_toECU();
		}
	}
	else
	{
		/* Diagnostic IO Control */
		stWF_Diag_IO_Cntrl();
	}
	
	/* Check for automatic level updates in WL3 and WL2 to avoid over heating */
	stWF_Chck_Auto_Lvl_Updt();
	
	/* @Output management (if stW status is not good, keep the wheel off) */
	if (stW_status_c != GOOD)
	{
		/* Turn off the StW software request */
		stW_sw_rq_b = FALSE;
	}
	else
	{
		/* Check last switch purpose */
		if (stW_swtch_rq_prps_b == TRUE)
		{
			/* Turn on the StW software request */
			stW_sw_rq_b = TRUE;
			
			/* Added on 12.17.2008: Update the PWM */
			/* This is necassary for recovering from UGLY to GOOD status change. */
			/* Example: System and/or Local Battery voltage occurance going from UGLY to GOOD */
			stWF_Updt_Pwm();
		}
		else
		{
			/* Turn off the StW software request */
			stW_sw_rq_b = FALSE;
		}
	}
	
	/* @LED management */
	stWF_LED_Display();

/* At any point of time, If module sees >14A total current draw when Rear seats and HSW outputs are ON, HSW output would be reduced to the next available low level. FTR00139667 */
	stW_RearHeat_Isense_c = stW_Isense_c + hs_Isense_c[REAR_LEFT]+hs_Isense_c[REAR_RIGHT];
}

/*********************************************************************************/
/*                               stWF_Updt_Flt_Stat                              */
/*********************************************************************************/
/* Input parameter:                                                              */
/* Wheel_Fault: Type of the stwheel fault present                                */
/*                                                                               */
/* This function checks the fault mature time.                                   */
/* If it is not expired, it sets the preliminary fault for the selected fault    */
/* type.                                										 */
/* If or when mature time is expired, it matures the appropriate stwheel fault   */
/* (and clears the preliminary fault). 											 */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* stWF_Flt_Mntr :                                                               */
/*    stWF_Updt_Flt_Stat(unsigned char stW_flt_type)                             */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* no calls                                                                      */
/*																				 */
/*********************************************************************************/
@far void stWF_Updt_Flt_Stat(stW_Flt_Type_e Wheel_Fault)
{
	/* Check the fault mature time and fault status */
	if (stW_Flt_Mtr_cnt < (stW_Flt_Mtr_Tm-1))
	{
		/* Count fault mature time */
		stW_Flt_Mtr_cnt++;
		
		/* @Preliminary Faults */
		switch (Wheel_Fault) {

			case STW_FULL_OPEN:
			{
				/* Update stW fault status */
				stW_Flt_status_w |= STW_PRLM_FULL_OPEN_MASK;
				break;
			}

			case STW_PARTIAL_OPEN:
			{
				/* Update stW fault status */
				stW_Flt_status_w |= STW_PRLM_PARTIAL_OPEN_MASK;
				break;
			}

			case STW_SHRT_TO_GND:
			{
				/* Update stW fault status */
				stW_Flt_status_w |= STW_PRLM_GND_MASK;
				break;
			}

			case STW_SHRT_TO_BATT:
			{
				/* Update stW fault status */
				stW_Flt_status_w |= STW_PRLM_BATT_MASK;
				break;
			}

			case STW_IGN_WHEEL:
			{
				/* Update stW fault status */
				stW_Flt_status_w |= STW_PRLM_IGN_WHEEL_MASK;
				break;
			}

			case STW_TEMP_SENSOR_FLT:
			{
				/* Update stW fault status */
				stW_Flt_status_w |= STW_PRELIM_TEMP_SENSOR_FLT;
				break;
			}

			default :
			{
				/* Fault type is not correct. Clear the fault status */
				stW_Flt_status_w = STW_NO_FLT_MASK;
			}
		}
	}
	else
	{
		/* @Matured faults */
		switch (Wheel_Fault) {

			case STW_FULL_OPEN:
			{
				/* Use this bit to determine if we have an Open or STB on High Side Driver */
				/* After the output turns off. */
				stW_Open_or_STB_b = TRUE;
				/* Mature stW fault */
				stW_Flt_status_w = STW_MTRD_FULL_OPEN_MASK;	
				break;
			}

			case STW_PARTIAL_OPEN:
			{
				/* Mature stW fault */
				stW_Flt_status_w = STW_MTRD_PARTIAL_OPEN_MASK;
				break;
			}

			case STW_SHRT_TO_GND:
			{
				/* Mature stW fault */
				stW_Flt_status_w = STW_MTRD_GND_MASK;
				break;
			}

			case STW_SHRT_TO_BATT:
			{
				/* Mature stW fault */
				stW_Flt_status_w = STW_MTRD_BATT_MASK;
				break;
			}

			case STW_IGN_WHEEL:
			{
				/* Mature stW fault */
				stW_Flt_status_w = STW_MTRD_IGN_WHEEL_MASK;
				break;
			}

			case STW_TEMP_SENSOR_FLT:
			{
				/* Mature stW fault */
				stW_Flt_status_w = STW_MTRD_TEMP_SENS_FLT_MASK;
				break;
			}

			default :
			{
				/* Fault type is not correct. Clear the fault status */
				stW_Flt_status_w = STW_NO_FLT_MASK;
			}
		}
		/* Clear fault mature count */
		stW_Flt_Mtr_cnt = 0;
	}
}

/*********************************************************************************/
/*                               stWF_Flt_Mntr                                   */
/*********************************************************************************/
/* This function examines the current and voltage feedback values of the         */
/* steering wheel heater in order to determine any fault conditions.        	 */
/*                                                                               */
/* There are six faults that can set a steering wheel output DTC. These are      */
/* (by their checking priority):                                         		 */
/* (1) No power to steering wheel                                                */
/* (2) Steering wheel temperature sensor fault status                            */
/* (3) Short to Battery                                                          */
/* (4) Short to Ground                                                           */
/* (5) Full Open circuit                                                         */
/* (6) Partial Open circuit                                                      */
/*                                                                               */
/* No fault detection if steering wheel heater is not turned on.                 */
/* 2 second time window before maturing a fault.                                 */
/*                                                                               */
/*                                                                               */
/* stWheelF_FaultDetection                                                       */
/* +--------------------------++------------+-----------+-----------+-----------+-------------+------------+ */
/* |                          ||     1      |     2     |     3     |     4     |      5      |     6      | */
/* +==========================++============+===========+===========+===========+=============+============+ */
/* |stW_Ign_stat              ||IGN_STAT_OFF|IGN_STAT_ON|IGN_STAT_ON|IGN_STAT_ON| IGN_STAT_ON |IGN_STAT_ON | */
/* +--------------------------++------------+-----------+-----------+-----------+-------------+------------+ */
/* |stW_tmprtr_sensor_flt_c   ||     -      |    !=     |    ==     |    ==     |     ==      |     ==     | */
/* |                          ||            | SENSOR_OK | SENSOR_OK | SENSOR_OK |  SENSOR_OK  | SENSOR_OK  | */
/* +--------------------------++------------+-----------+-----------+-----------+-------------+------------+ */
/* |stW_EFUSE_state           ||     -      |     -     |    OFF    |    ON     |     ON      |     ON     | */
/* |                          ||            |           |     *     |           |             |            | */
/* +--------------------------++------------+-----------+-----------+-----------+-------------+------------+ */
/* |relay_C_phase1_Test_Voltage_OFF_fault_b||     -      |     -     |   TRUE    |   FALSE   |    FALSE    |   FALSE    | */
/* |                          ||            |           |     *     |           |             |            | */
/* +--------------------------++------------+-----------+-----------+-----------+-------------+------------+ */
/* |main_CSWM_status_c        ||     -      |     -     |     -     |   GOOD    |    GOOD     |    GOOD    | */
/* +--------------------------++------------+-----------+-----------+-----------+-------------+------------+ */
/* |relay_C_internal_Fault_b  ||     -      |     -     |     -     |   FALSE   |    FALSE    |   FALSE    | */
/* +--------------------------++------------+-----------+-----------+-----------+-------------+------------+ */
/* |stW_state_b               ||     -      |     -     |     -     |    OFF    |     ON      |     ON     | */
/* +--------------------------++------------+-----------+-----------+-----------+-------------+------------+ */
/* |relay_C_phase1_Test_Voltage_ON_fault_b||     -      |     -     |     -     |   TRUE    |    FALSE    |   FALSE    | */
/* +--------------------------++------------+-----------+-----------+-----------+-------------+------------+ */
/* |                          ||            |           |           |           |      <      | FULL_OPEN  | */
/* |                          ||            |           |           |           |FULL_OPEN_LMT|     <=     | */
/* |stW_Isense_c              ||     -      |     -     |     -     |     -     |             |   ISENSE   | */
/* |                          ||            |           |           |           |             |     <      | */
/* |                          ||            |           |           |           |             |     =      | */
/* |                          ||            |           |           |           |             |PARTIAL_OPEN| */
/* +==========================++============+===========+===========+===========+=============+============+ */
/* |stW_Flt_status_w          ||            |           |           |           |             |            | */
/* |=                         ||     X      |           |           |           |             |            | */
/* |STW_IGN_WHEEL_FLT         ||            |           |           |           |             |            | */
/* +--------------------------++------------+-----------+-----------+-----------+-------------+------------+ */
/* |stW_Flt_status_w          ||            |           |           |           |             |            | */
/* |=                         ||            |     X     |           |           |             |            | */
/* |STW_TEMP_SENSOR_FLT       ||            |           |           |           |             |            | */
/* +--------------------------++------------+-----------+-----------+-----------+-------------+------------+ */
/* |stW_Flt_status_w          ||            |           |           |           |             |            | */
/* |=                         ||            |           |     X     |           |             |            | */
/* |SHRT_TO_BATTERY           ||            |           |           |           |             |            | */
/* +--------------------------++------------+-----------+-----------+-----------+-------------+------------+ */
/* |stW_Flt_status_w          ||            |           |           |           |             |            | */
/* |=                         ||            |           |           |     X     |             |            | */
/* |SHRT_TO_GND               ||            |           |           |           |             |            | */
/* +--------------------------++------------+-----------+-----------+-----------+-------------+------------+ */
/* |stW_Flt_status_w          ||            |           |           |           |             |            | */
/* |=                         ||            |           |           |           |      X      |            | */
/* |FULL_OPEN                 ||            |           |           |           |             |            | */
/* +--------------------------++------------+-----------+-----------+-----------+-------------+------------+ */
/* |stW_Flt_status_w          ||            |           |           |           |             |            | */
/* |=                         ||            |           |           |           |             |     X      | */
/* |PARTIAL_OPEN              ||            |           |           |           |             |            | */
/* +--------------------------++------------+-----------+-----------+-----------+-------------+------------+ */
/*                                                                               */
/*                                                                               */
/* SD Text                                                                       */
/* -------                                                                       */
/* This function manages the detect a steering wheel output fault.               */
/* Only one of the stwheel output faults shall be matured at any given time.     */
/* Therefore, the fault detection is prioritized.                       		 */
/*                                                                               */
/* In order to detect, stwheel short to ground, full open or partial open faults:*/
/* 1. main_CSWM_status_c is good                                                 */
/* 2. No Relay C internal faults is set                                          */
/*                                                                               */
/* All other stwheel faults (no power, temperature sensor fault, and short to    */
/* battery) can be detected without having above conditions satisfied. 			 */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* main :                                                                        */
/*    stWF_Flt_Mntr()                                                            */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* stWF_Updt_Flt_Stat(unsigned char stW_flt_type)                                */
/* uint8 Dio_ReadChannel( ChannelId)                                             */
/*																				 */
/*********************************************************************************/
@far void stWF_Flt_Mntr(void)
{
	/* Local variable(s) */
	u_16Bit stW_prv_flt_typ = stW_Flt_status_w;     // stores the previous fault type
	
	/* Store stWheel ignition status */
	stW_Ign_stat = (u_8Bit) Dio_ReadChannel(IGN_WHEEL_MON);
	
	/* @Wheel Fault Detection */

	/* Check Steering Wheel Power (Wheel Ignition status) */
	if ( (stW_Ign_stat == STW_IGN_STAT_ON) &&
		 (stW_Flt_status_w != STW_MTRD_IGN_WHEEL_MASK) )
	{
		/* No preliminary ignition wheel fault */
		stW_Flt_status_w &= STW_CLR_PRLM_IGN_WHEEL;
		
		/* Check Wheel temperature sensor fault */
		if ((stW_tmprtr_sensor_flt_c == STW_TEMPERATURE_SENSOR_OK) &&
			(stW_Flt_status_w != STW_MTRD_TEMP_SENS_FLT_MASK))
		{
			/* No preliminary temperature sensor fault */
			stW_Flt_status_w &= STW_CLR_PRLM_TEMP_SENSOR;
			
			if ( (stW_Flt_status_w != STW_MTRD_BATT_MASK) &&
				 (relay_C_phase1_Test_Voltage_OFF_fault_b == FALSE) )
			{
				/* No preliminary short to battery fault */
				stW_Flt_status_w &= STW_CLR_PRLM_BATT;
				
				/* Check if stWheel fault detection is feasible */
				if ( (main_CSWM_Status_c == GOOD) &&
					 (relay_C_internal_Fault_b == FALSE) )
				{
					/* Check if high side is enabled */
					if (stW_state_b == TRUE)
					{				
						/* Check for Isense analysis delay (320ms) */
						if (stW_Isense_dly_cnt_c < STW_ISENSE_DELAY)
						{
							/* Increment Isense delay counter */
							stW_Isense_dly_cnt_c++;
						}
						else
						{							
							/* Check for Short to Ground*/
							if ((stW_Vsense_HS_c < stW_flt_calibs_c[STW_SHRT_TO_GND_VSENSE]) ||
								(stW_Isense_c > stW_flt_calibs_c[STW_OVR_CURRENT_ISENSE]) )
							{
								/* This is a STG. Clear The flag that is used to determine Open or STB */
								stW_Open_or_STB_b = FALSE;
								/* Set Short to Ground */
								stWF_Updt_Flt_Stat(STW_SHRT_TO_GND);
							}
							else
							{
								/* No preliminary short to ground fault */
								stW_Flt_status_w &= STW_CLR_PRLM_GND;
								
								/* Check Isense for Open circuit */
								if (stW_Isense_c < stW_flt_calibs_c[STW_FULL_OPEN_ISENSE])
								{
									/* Open circuit fault */
									stWF_Updt_Flt_Stat(STW_FULL_OPEN);
								}
								else
								{											
									/* No stWheel fault */
									stW_Flt_status_w = STW_NO_FLT_MASK;
									/* Clear The flag that is used to determine Open or STB */
									stW_Open_or_STB_b = FALSE;										
								}
							}
						}
					}
					else
					{						
						/* Check for short to ground fault */
						if ( (relay_C_phase1_Test_Voltage_ON_fault_b == TRUE) && (stW_Flt_status_w != STW_MTRD_GND_MASK) )
						{
							/* Short to Ground */
							stWF_Updt_Flt_Stat(STW_SHRT_TO_GND);
						}
						else
						{
							/* No preliminary short to ground fault */
							stW_Flt_status_w &= STW_CLR_PRLM_GND;
							
							/* 140ms Delay after we stop PWMing the high side driver */
							if(stW_chck_Open_or_STB_dly_c >= STW_OPEN_or_STB_DELAY){
								
								/* NEW: Check if we detected a STB while the High Side Driver was turned on */
								if( (stW_Open_or_STB_b == TRUE) && (stW_Vsense_HS_c > stW_flt_calibs_c[STW_SHRT_TO_BATT_VSENSE]) ){
									/* Set the STB fault immediately (we already waited 2 seconds to mature the fault */
									stW_Flt_Mtr_cnt = stW_Flt_Mtr_Tm;
									/* Set STB */
									stWF_Updt_Flt_Stat(STW_SHRT_TO_BATT);
									/* Clear The flag that is used to determine Open or STB */
									stW_Open_or_STB_b = FALSE;
									
								}else{
									/* Check if we need to set the open fault */
									if(stW_Open_or_STB_b == TRUE){													
										/* Set the STB fault immediately (we already waited 2 seconds to mature the fault */
										stW_Flt_Mtr_cnt = stW_Flt_Mtr_Tm;
										/* Set Open circuit fault */
										stWF_Updt_Flt_Stat(STW_FULL_OPEN);
										/* Clear the bit in order to set the Open Fault on High Side Driver */
										stW_Open_or_STB_b = FALSE;	
									}else{
										/* No preliminary full open */
										stW_Flt_status_w &= STW_CLR_PRLM_FULL_OPEN;
										/* No preliminary partial open */
										stW_Flt_status_w &= STW_CLR_PRLM_PARTIAL_OPEN;
										/* Check for Regular STB */
										if(stW_Vsense_HS_c > stW_flt_calibs_c[STW_SHRT_TO_BATT_VSENSE]){
											/* Set STB */
											stWF_Updt_Flt_Stat(STW_SHRT_TO_BATT);
										}else{
											/* No STB present */
										}
									}
								}
								
							}else{
								/* Wait 120ms after stopping PWMing the high side driver */
								stW_chck_Open_or_STB_dly_c++;
							}								
						}
					}
				}else{
					/* If we have relay stuck low or FET is always off Set STG */
					if( (relay_C_internal_Fault_b == TRUE) && (stW_Flt_status_w != STW_MTRD_GND_MASK) ){
						/* Set STG Fault */
						stWF_Updt_Flt_Stat(STW_SHRT_TO_GND);					
					}else{
						/* No preliminary short to ground fault */
						stW_Flt_status_w &= STW_CLR_PRLM_GND;
						/* Conditions are not met to mature a stwheel fault. Clear fault count */
						stW_Flt_Mtr_cnt = 0;
					}
				}					
			}
			else
			{
				/* Check for short to battery fault */
				if(stW_Flt_status_w != STW_MTRD_BATT_MASK){
					/* Set STB */
					stWF_Updt_Flt_Stat(STW_SHRT_TO_BATT);						
				}else{
					/* Already matured STB fault */
				}
			}
		}
		else
		{
			/* Check if we have matured the Temperature Sensor fault */
			if (stW_Flt_status_w != STW_MTRD_TEMP_SENS_FLT_MASK)
			{
				/* Check if this vehicle belongs to D/R-series vehicle lines */
				if((main_SwReq_Front_c == VEH_BUS_CCN_FRONT_REAR_K) || (main_SwReq_Rear_c == VEH_BUS_DM_REAR_K)){
					/* Check If Temp sensor flt received as below detectable range */
					if ((stW_tmprtr_sensor_flt_c == STW_TEMPERATURE_SENSOR_SHRT))
					{
						/* Extra care before we set the fault. Check Ambient Temperature and stW ontime */
						/* Check if ambient tempearture is less than +5 C (4500) */
						if (canio_RX_ExternalTemperature_c < STW_SNSR_FLT_AMBIENT_LMT)
						{
							/* Check if the seat heater has been turned on for sufficient amount of time */
							if (stW_onTime_w > stW_ontimeLmt_w)
							{
								/* Set Steering Wheel Temperature Sensor Fault as we passed sufficient amount of time with Ambient temp is less than -10c */
								stWF_Updt_Flt_Stat(STW_TEMP_SENSOR_FLT);
							}
							else
							{
								/* Wait ontime delay to determine if NTC is working or not */
							}
						}
						else
						{
							/* Set Steering Wheel Temperature Sensor Fault */
							/* Ambient temp is at least +5c                */
							stWF_Updt_Flt_Stat(STW_TEMP_SENSOR_FLT);
						}
					}
					else
					{
						/* If not then it is either signal Received as SNA or Above detectable range. Straight away update the status. */
						/* Set Steering Wheel Temperature Sensor Fault                                                                 */
						stWF_Updt_Flt_Stat(STW_TEMP_SENSOR_FLT);
					}	
					
				}else{
					/* For W and L series vehicles STW_TEMP_SENSOR_FLT is matured in two seconds. */
					stWF_Updt_Flt_Stat(STW_TEMP_SENSOR_FLT);						
				}
			}
			else
			{
				/* Already matured Temperature Sensor Fault */
			}
		}
	}
	else
	{
		/* Check if we matured Ignition wheel fault */
		if (stW_Flt_status_w != STW_MTRD_IGN_WHEEL_MASK)
		{
			/* Set Steering Wheel igniton fault */
			stWF_Updt_Flt_Stat(STW_IGN_WHEEL);
		}
		else
		{
			/* Already matured Ignition Wheel fault */
		}
	}

	/* Clear fault mature count if applicable: Fault status changes */
	if ( (stW_prv_flt_typ != stW_Flt_status_w) ||
		 (stW_Flt_status_w == STW_NO_FLT_MASK) )
	{
		stW_Flt_Mtr_cnt = 0;
	}
	/* Check if we need to clear the flag used in determining open or STB */
	/* Preliminary Full Open Flag is not set and */
	/* Matured Open Flag is not set and */
	/* stW_Open_or_STB_b flag is set to TRUE */
	if( ( (stW_Flt_status_w & STW_PRLM_FULL_OPEN_MASK) != STW_PRLM_FULL_OPEN_MASK) && 
		( (stW_Flt_status_w & STW_MTRD_FULL_OPEN_MASK) != STW_MTRD_FULL_OPEN_MASK) &&
		(stW_Open_or_STB_b == TRUE) ){
		/* Clear The flag that is used to determine Open or STB */
		stW_Open_or_STB_b = FALSE;
	}
}

/*********************************************************************************/
/*                               stWF_DTC_Manager                                */
/*********************************************************************************/
/* Controls the stwheel faults DTC status.                                       */
/* If there is a stwheel matured fault, it sets the appropriate DTC by calling   */
/* the FMemLibF_ReportDtc function. 											 */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* main :                                                                        */
/*    stWF_DTC_Manager()                                                         */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* FMemLibF_ReportDtc(DtcType DtcCode,enum ERROR_STATES State)                   */
/*																				 */
/*********************************************************************************/
@far void stWF_DTC_Manager(void)
{
	/* Full Open Circuit */
	if ( (stW_Flt_status_w & STW_MTRD_FULL_OPEN_MASK) == STW_MTRD_FULL_OPEN_MASK)
	{
		/* Open Circuit DTC is active */
		if(stW_Open_or_STB_b == FALSE){
			FMemLibF_ReportDtc(DTC_HSW_OPEN_FULL_K, ERROR_ON_K);
		}		
	}
	else
	{
		/* Open Circuit DTC is not active */
		FMemLibF_ReportDtc(DTC_HSW_OPEN_FULL_K, ERROR_OFF_K);
	}
	
	/* 02.18.2009: Steering Wheel Partial Open DTC is not supported for CSWM MY11 */
//	/* Partial Open Circuit */
//	if ( (stW_Flt_status_w & STW_MTRD_PARTIAL_OPEN_MASK) == STW_MTRD_PARTIAL_OPEN_MASK)
//	{
//		/* Open Circuit DTC is active */
//		FMemLibF_ReportDtc(DTC_HSW_OPEN_PARTIAL_K, ERROR_ON_K);
//	}
//	else
//	{
//		/* Open Circuit DTC is not active */
//		FMemLibF_ReportDtc(DTC_HSW_OPEN_PARTIAL_K, ERROR_OFF_K);
//	}
	
	/* Short to Ground */
	if ( (stW_Flt_status_w & STW_MTRD_GND_MASK) == STW_MTRD_GND_MASK)
	{
		/* Short to Ground DTC is active */
		FMemLibF_ReportDtc(DTC_HSW_SHORT_TO_GND_K, ERROR_ON_K);
	}
	else
	{
		/* Short to Ground DTC is not active */
		FMemLibF_ReportDtc(DTC_HSW_SHORT_TO_GND_K, ERROR_OFF_K);
	}
	
	/* Short to Battery */
	if ( (stW_Flt_status_w & STW_MTRD_BATT_MASK) == STW_MTRD_BATT_MASK)
	{
		/* Short to Battery DTC is active */
		FMemLibF_ReportDtc(DTC_HSW_SHORT_TO_BAT_K, ERROR_ON_K);
	}
	else
	{
		/* Short to Battery DTC is not active */
		FMemLibF_ReportDtc(DTC_HSW_SHORT_TO_BAT_K, ERROR_OFF_K);
	}
	
	/* Ignition Wheel Fault (No pwer to steering wheel) */
	if ( (stW_Flt_status_w & STW_MTRD_IGN_WHEEL_MASK) == STW_MTRD_IGN_WHEEL_MASK)
	{
		/* Short to Battery DTC is active */
		FMemLibF_ReportDtc(DTC_NO_BAT_FOR_STEERING_WHEEL_K, ERROR_ON_K);
	}
	else
	{
		/* Short to Battery DTC is not active */
		FMemLibF_ReportDtc(DTC_NO_BAT_FOR_STEERING_WHEEL_K, ERROR_OFF_K);
	}

	/* Temperature Sensor Fault */
	if ( (stW_Flt_status_w & STW_MTRD_TEMP_SENS_FLT_MASK) == STW_MTRD_TEMP_SENS_FLT_MASK)
	{
		/* Wheel temperature sensor fault SNA active */
		FMemLibF_ReportDtc(DTC_HSW_TEMP_SENS_FLT_SNA_K, ERROR_ON_K);
	}
	else
	{
		/* Wheel temperature sensor fault SNA is not active */
		FMemLibF_ReportDtc(DTC_HSW_TEMP_SENS_FLT_SNA_K, ERROR_OFF_K);
	}
}

/*********************************************************************************/
/*                               stWF_Copy_Diag_Prms (NOT USED)                  */
/*********************************************************************************/
/* Input paramter:                                                               */
/* u_8Bit* stW_data: Pointer to the new calibration data.                        */
/*                                                                               */
/* This function copies the new stwheel calibration data into a buffer array if  */
/* the new data is in allowed range. 											 */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* stWF_Chck_Diag_Prms :                                                         */
/*    stWF_Copy_Diag_Prms(unsigned char * stW_data)                              */
/*                                                                               */
/*********************************************************************************/
//@far void stWF_Copy_Diag_Prms(u_8Bit *stW_data)
//{
//	u_8Bit a;                     // for loop variable
//	u_8Bit *stW_cpy_data_p;       // pointer to data that needs to be copied
//
//	stW_cpy_data_p = stW_data;    // point to copy data
//
//	for (a=0; a<STW_DATA_LENGHT; a++) {
//
//		stW_diag_prog_data_c[a] = *(stW_cpy_data_p + a);
//	}
//
//}

/*********************************************************************************/
/*                               stWF_Chck_Diag_Prms                             */
/*********************************************************************************/
/* Input paramter:                                                               */
/* stW_LED_serv: Diagnostic service ID that selects particular calibration data  */
/* (stwheel pwm's or timeouts).             									 */
/* * stW_data_p: pointer to the new calibration data                             */
/*                                                                               */
/* Return parameter:                                                             */
/* stW_chck_rslt: 1 if new data is in allowed range. 0 otherwise.                */
/*                                                                               */
/* This function performs a check on the new stwheel calibration data to find out*/
/* whether it is in allowed range or not. 										 */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* ApplDescProcessWriteDataByIdentifier :                                        */
/*    stWF_Chck_Diag_Prms(unsigned char stw_LID_serv,unsigned char * stw_data_p) */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* 												                                 */
/*********************************************************************************/
@far u_8Bit stWF_Chck_Diag_Prms(u_8Bit stw_LID_serv, u_8Bit *stw_data_p)
{
	/* Local variable(s) */
	u_8Bit stW_LID_service;           // LID service
	u_8Bit *stW_data_p_c;             // pointer to start address of data needs to be checked
	u_8Bit stW_chck_rslt = FALSE;     // check result

	stW_LID_service = stw_LID_serv;   // store the LID service 
	stW_data_p_c = stw_data_p;        // point to the data that needs to be checked

	switch (stW_LID_service) {

	case HSW_PWM:
	{
		/* Wheel Level    Default PWM      Allowed Range (duty cycle) */
		/*    WL1            10                10 to 100              */
		/*    WL2            35                10 to 100              */
		/*    WL3            65                10 to 100              */
		/*    WL4           100                10 to 100              */
		if ( ( *stW_data_p_c < 10 || *stW_data_p_c > 100 ) ||
		   ( ( *(stW_data_p_c + 1) < 10 || *(stW_data_p_c + 1) > 100) ) ||
		   ( ( *(stW_data_p_c + 2) < 10 || *(stW_data_p_c + 2) > 100) ) ||
		   ( ( *(stW_data_p_c + 3) < 10 || *(stW_data_p_c + 3) > 100) ) ) {
			/* Diagnostic data is not in allowed range.               */
			/* Do not give access for write diagnostic data function. */
			stW_chck_rslt = FALSE;
		}
		else {
			/* Copy the data in local array. */
//				stWF_Copy_Diag_Prms(stW_data_p_c); //Harsha commented. 09.08.08
			
			stW_prms.cals[stW_veh_offset_c+stW_cfg_c].Pwm_a[WL1] = *stW_data_p_c;
			stW_prms.cals[stW_veh_offset_c+stW_cfg_c].Pwm_a[WL2] = *(stW_data_p_c + 1);
			stW_prms.cals[stW_veh_offset_c+stW_cfg_c].Pwm_a[WL3] = *(stW_data_p_c + 2);
			stW_prms.cals[stW_veh_offset_c+stW_cfg_c].Pwm_a[WL4] = *(stW_data_p_c + 3);
			
			/* Data is in correct range.              */
			/* Give access to Write Diagnostics data. */
			stW_chck_rslt = TRUE;
		}
		break;
	}

	case HSW_TIMEOUT:
	{
		/* Wheel Level    Default Timeout      Allowed Range (minutes) */
		/*    WL1            15                    1 to 60             */
		/*    WL2            15                    1 to 60             */
		/*    WL3            30                    1 to 60             */
		/*    WL4            30                    1 to 60             */
		if ( ( *stW_data_p_c < 1 || *stW_data_p_c > 60 ) ||
		   ( ( *(stW_data_p_c + 1) < 1 || *(stW_data_p_c + 1) > 60) ) ||
		   ( ( *(stW_data_p_c + 2) < 1 || *(stW_data_p_c + 2) > 60) ) ||
		   ( ( *(stW_data_p_c + 3) < 1 || *(stW_data_p_c + 3) > 60) )) {
			/* Diagnostic data is not in allowed range.               */
			/* Do not give access for write diagnostic data function. */
			stW_chck_rslt = FALSE;
		}
		else {
			/* Copy the data in local array. */
//				stWF_Copy_Diag_Prms(stW_data_p_c); //Harsha commented. 09.08.08
			
			stW_prms.cals[stW_veh_offset_c+stW_cfg_c].TimeOut_c[WL1] = *stW_data_p_c;
			stW_prms.cals[stW_veh_offset_c+stW_cfg_c].TimeOut_c[WL2] = *(stW_data_p_c + 1);
			stW_prms.cals[stW_veh_offset_c+stW_cfg_c].TimeOut_c[WL3] = *(stW_data_p_c + 2);
			stW_prms.cals[stW_veh_offset_c+stW_cfg_c].TimeOut_c[WL4] = *(stW_data_p_c + 3);
			
			/* Data is in correct range.              */
			/* Give access to Write Diagnostics data. */
			stW_chck_rslt = TRUE;
		}
		break;
	}
	case HSW_TEMP_CUT_OFF:
	{
		/* Wheel Level    Default temp      Allowed Range (Normalized) */
		/*    WL1            N/A                   Not written         */
		/*    WL2            186 (53 C)            1 to 200 (60 C)     */
		/*    WL3            180 (50 C)            1 to 200 (60 C)     */
		/*    WL4            174 (47 C)            1 to 200 (60 C)     */
		/*    Max            188 (54 C)            1 to 200 (60 C)     */
		if ( ( *stW_data_p_c < 1 || *stW_data_p_c > 200 ) ||
		   ( ( *(stW_data_p_c + 1) < 1 || *(stW_data_p_c + 1) > 200) ) ||
		   ( ( *(stW_data_p_c + 2) < 1 || *(stW_data_p_c + 2) > 200) ) ||
		   ( ( *(stW_data_p_c + 3) < 1 || *(stW_data_p_c + 3) > 200) ) ||
		   ( ( *(stW_data_p_c + 4) < 1 || *(stW_data_p_c + 4) > 200) ) ) {
			/* Diagnostic data is not in allowed range.               */
			/* Do not give access for write diagnostic data function. */
			stW_chck_rslt = FALSE;
		}
		else {
			/* Copy the data in local array. */
//				stWF_Copy_Diag_Prms(stW_data_p_c); //Harsha commented. 09.08.08

			stW_prms.cals[stW_veh_offset_c+stW_cfg_c].WL1Temp_Threshld = *stW_data_p_c ; //Added in case If decides to have new value
			stW_prms.cals[stW_veh_offset_c+stW_cfg_c].WL2Temp_Threshld = *(stW_data_p_c + 1);
			stW_prms.cals[stW_veh_offset_c+stW_cfg_c].WL3Temp_Threshld = *(stW_data_p_c + 2);
			stW_prms.cals[stW_veh_offset_c+stW_cfg_c].WL4Temp_Threshld = *(stW_data_p_c + 3);
			stW_prms.cals[stW_veh_offset_c+stW_cfg_c].MaxTemp_Threshld = *(stW_data_p_c + 4);
			
			/* Data is in correct range.              */
			/* Give access to Write Diagnostics data. */
			stW_chck_rslt = TRUE;
		}
		break;
	}
		
	case HSW_MIN_TIMEOUT:
	{
		/* Wheel Level    Default Timeout      Allowed Range (minutes) */
		/*    WL1            0                    0 to 60             */
		/*    WL2            0                    0 to 60             */
		/*    WL3            0                    0 to 60             */
		/*    WL4            0                    0 to 60             */
		if ( ( *stW_data_p_c < 0 || *stW_data_p_c > 60 ) ||
		   ( ( *(stW_data_p_c + 1) < 0 || *(stW_data_p_c + 1) > 60) ) ||
		   ( ( *(stW_data_p_c + 2) < 0 || *(stW_data_p_c + 2) > 60) ) ||
		   ( ( *(stW_data_p_c + 3) < 0 || *(stW_data_p_c + 3) > 60) ) ) {
			/* Diagnostic data is not in allowed range.               */
			/* Do not give access for write diagnostic data function. */
			stW_chck_rslt = FALSE;
		}
		else {
			/* Copy the data in local array. */
//				stWF_Copy_Diag_Prms(stW_data_p_c); //Harsha commented. 09.08.08
			stW_prms.cals[stW_veh_offset_c+stW_cfg_c].WL1TempChck_Time = *(stW_data_p_c);
			stW_prms.cals[stW_veh_offset_c+stW_cfg_c].WL2TempChck_Time = *(stW_data_p_c + 1);
			stW_prms.cals[stW_veh_offset_c+stW_cfg_c].WL3TempChck_Time = *(stW_data_p_c + 2);
			stW_prms.cals[stW_veh_offset_c+stW_cfg_c].WL4TempChck_Time = *(stW_data_p_c + 3);
			
			/* Data is in correct range.              */
			/* Give access to Write Diagnostics data. */
			stW_chck_rslt = TRUE;
		}
		break;
	}		
	
//	case HSW_CFG:
//	{
//		if(( *stW_data_p_c == 0x00 || *stW_data_p_c == 0x01 ))
//		{
//			//If Value is either 0x00 or 0x01 Its correct value. Store the right value into EEP.
//			
//			stW_cfg_c = *(stW_data_p_c);
//			stW_chck_rslt = TRUE;
//		}
//		else
//		{
//			stW_chck_rslt = FALSE;
//		}
//		
//		break;
//	}
		
		case STW_IREF_VREF:
		{
			/* Iref Default(A)   Allowed Range (A) */
			/*      8                7 to 12       */	//Decimal
			/*     0x08             0x07 to 0x0C   */	//Hex
			/* Vref Default(mV)  Allowed Range (mV)*/
			/*     825              700 to 1300    */	//Decimal
			/*    0x339            0x2BC to 0x514  */	//Hex
			if( ( *stW_data_p_c < 0x07 || *stW_data_p_c > 0x0C ) ||						// Iref Check
				( *(stW_data_p_c+1) < 0x02 ) || 										// Vref Low range Check1 < 0x2xx
				( ( *(stW_data_p_c + 1) == 0x02 && *(stW_data_p_c + 2) < 0xBC) ) ||     // Vref Low range Check2 < 0x2BC
				( *(stW_data_p_c+1) > 0x05 ) ||											// Vref high range Check1 > 0x5xx
				( ( *(stW_data_p_c + 1) == 0x05 && *(stW_data_p_c + 2) > 0x14) ) ){     // Vref high range Check2 > 0x514
				/* Diagnostic data is not in allowed range.               */
				/* Do not give access for write diagnostic data function. */
				stW_chck_rslt = FALSE;				
			}else{
				
				/* Update date with valid Iref and Vref values */
				AD_Iref_STW_c = *(stW_data_p_c);
				AD_Vref_STW_w = *(stW_data_p_c+1);
				AD_Vref_STW_w = (AD_Vref_STW_w<<8);		// shift information to high byte
				AD_Vref_STW_w |= *(stW_data_p_c+2);		// or with low byte
				
				/* Data is in correct range.              */
				/* Give access to Write Diagnostics data. */
				stW_chck_rslt = TRUE;				
			}			
			break;
		}

		default :
		{
			stW_chck_rslt = FALSE;

		}
	}

	return(stW_chck_rslt);
}

/*********************************************************************************/
/*                               stWF_Write_Diag_Data                            */
/*********************************************************************************/
/* Input parameter:                                                              */
/* stW_LID: Diagnostic service ID number that selects certain calibration data   */
/* (wheel pwms or wheel timeouts).                                               */
/*                                                                               */
/* Once it is determined that new calibration data is in allowed range, this     */
/* function is called to update the non-volatile memory with new calibration     */
/* data. 																		 */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* diagLF_WriteData :                                                            */
/*    stWF_Write_Diag_Data(unsigned char stW_LID)                                */
/*                                                                               */
/*********************************************************************************/
@far u_8Bit stWF_Write_Diag_Data(u_8Bit stW_LID)
{
	/* Local variable(s) */
	u_8Bit stW_LID_s = stW_LID;    // store the LID request
	u_8Bit stW_write_rslt = FALSE; // Write result (Success or Fail)
	
	/* Check for valid LID service */
	/* Check for valid LID service */
	if ( (stW_LID_s == HSW_PWM) || 
		(stW_LID_s == HSW_TIMEOUT) || 
		(stW_LID_s == HSW_TEMP_CUT_OFF) || 
		(stW_LID_s == HSW_MIN_TIMEOUT) ){

		/* Copy all Steering Wheel Calibration data into local structure */
		if (/*Proxi_data_s.wheel_cfg_c == 0x01*/1) 
		{
			/* store the latest All Leather wheel parameters from Shadow to EEP */
			EE_BlockWrite(EE_WHEEL_CAL_DATA, (u_8Bit*)&stW_prms); 
		}
		else
		{
			/* Module is with Wood. Store the latest wood parameters to EEP */
			EE_BlockWrite(EE_WHEEL_CAL_DATA, (u_8Bit*)&stW_prms);   
		}
		/* Let Diagnostic module know that we have placed a call to EE_BlockWrite */
		stW_write_rslt = TRUE;
	}else{
		/* Check for last valid LID */
		if(stW_LID_s == STW_IREF_VREF){
			/* Update the stw Iref and Vref parameters . Not used*/
//			EE_BlockWrite(EE_IREF_STW, (u_8Bit*)&AD_Iref_STW_c);	// Write Iref_STW parameter
//			EE_BlockWrite(EE_VREF_STW, (u_8Bit*)&AD_Vref_STW_w);    // Write Vref_STW parameter
			/* Let Diagnostic module know that we have placed a call to EE_BlockWrite */
			stW_write_rslt = TRUE;			
		}else{
			/* Not a valid LID. Let the Diagnostic module know. Nothing should be written to emulated EEPROM */
			stW_write_rslt = FALSE;			
		}
	}
	return(stW_write_rslt);
}

/*********************************************************************************/
/*                               stWF_Get_Output_Stat                            */
/*********************************************************************************/
/* This function returns the present stwheel level WL0, WL1, WL2, or WL3. 		 */
/*                                                                        		 */
/* Calls from                                                             		 */
/* --------------                                                         		 */
/* ApplDescProcessReadDataByIdentifier :                                  		 */
/*    u_8Bit stWF_Get_Output_Stat()                                       		 */
/*                                                                        		 */
/*********************************************************************************/
@far u_8Bit stWF_Get_Output_Stat(void)
{
	return(stW_level_c);
}

/*********************************************************************************/
/*                               stWF_Diag_DTC_Control                           */
/*********************************************************************************/
/* Input parameters:                                                             */
/* stW_DTC_cntrl: Control input to set or clear a chosen DTC by a diagnostic 	 */
/* command.          															 */
/* stW_DTC: Selected stwheel fault which will be activated.                      */
/*                                                                               */
/* This function is used to mature or de-mature the selected stwheel fault in    */
/* diagnostic mode. 															 */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* ApplDescProcessRoutineControl :                                               */
/* stWF_Diag_DTC_Control(stW_DTC_Control_e stW_DTC_cntrl,stW_Flt_Type_e stW_DTC) */
/*																				 */
/*********************************************************************************/
@far void stWF_Diag_DTC_Control(stW_DTC_Control_e stW_DTC_cntrl, u_16Bit stW_DTC)
{
	/* Local variable(s) */
	u_16Bit stW_chosen_DTC = stW_DTC;
	
	/* Check if we want to set a DTC */
	if (stW_DTC_cntrl == STW_SET_DTC) {
		/* Update fault status */
		stW_Flt_status_w = stW_chosen_DTC;
	}
	else {
		/* Clear all faults */
		stW_Flt_status_w = STW_NO_FLT_MASK;
		
		/* Clear fault mature count */
		stW_Flt_Mtr_cnt = 0;
	}
}

/*********************************************************************************/
/*                        stWF_Actvtn_Stat                                   	 */
/*********************************************************************************/
/* Function Name: @far u_8Bit stWF_Actvtn_Stat(void)						     */
/* Type: Global Function													     */
/* Returns: u_8Bit (steering wheel output activation status)                     */
/* Parameters: None																 */
/* Description: This function returns the steering wheel activation status.      */
/* 				TRUE = ACTIVE (Steering Wheel has to be at WL3, Wl2, or WL1)     */
/*              To be considered for battery compensation.                       */
/* 				FALSE = NOT ACTIVE												 */
/*																			     */
/* Calls to: None																 */
/* Calls from:																	 */	
/*    BattVF_Compensation_Calc()												 */
/*																				 */ 
/*********************************************************************************/
@far u_8Bit stWF_Actvtn_Stat(void)
{
	u_8Bit stW_actvtn_stat_c = FALSE;	// local variable to store steering wheel activation status
	
	/* check stW output state and level */
	if( (stW_state_b == TRUE) && (stW_level_c != WL0) ){
		stW_actvtn_stat_c = TRUE;	// output is active (turned ON)
	}else{
		stW_actvtn_stat_c = FALSE;	// output is not active (turned OFF)
	}
	return(stW_actvtn_stat_c);	
}

/*****************************************************************************************/
/*                        stWF_CurrentLimit_Timer                                   	 */
/*****************************************************************************************/
/* Function Name: @far stWF_CurrentLimit_Timer(void)						             */
/* Returns: None											                             */
/* Description: 																		 */
/* For 8AMPS Current Limit:	(8A for W series, Rest is 10A)								 */
/* If Wheel is in WL4 and observes current draw of more than 8A/10A, the counter starts  */
/* incrementing for 1min (in 40ms time slice this function gets called.                  */
/* Once counter reaches the 1500 counts(1min), it sets the stW_Isense_limit_b flag to TRUE.*/
/*****************************************************************************************/
@far void stWF_CurrentLimit_Timer(void)
{
	u_8Bit stW_Isense_limit_c = 80; 

    //For powernet current limit test is at 8A  MKS 68476
	//stW_Isense_limit_c = 80; //8A Limit

	/* 8A Current Limit Logic */
	/* If Heat level @ WL4 and Isense draw more than 8A/10A for 1min, set the Isense limit flag */
	if( (stW_level_c == WL4) && (stW_Isense_c >= stW_Isense_limit_c ) )
	{
		/* Check Flag is set, If not increment the timer to reach 1min */
		if ((stW_Isense_limit_b == FALSE) && (stW_Isense_timer_w < STW_ISENSE_CNTR))
		{
			stW_Isense_timer_w++;
		}
		else
		{
			/* Set the 8A/10A limit Flag. This will be used in stWF_Chck_Auto_Lvl_Updt to stepdown to WL3 */
			stW_Isense_limit_b = TRUE;
		}
	}
	else
	{
		/* Either not at WL4 or Isense draws less than 8A */
		stW_Isense_timer_w = 0;
	}

}




