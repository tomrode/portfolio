/********************************************************************************/
/*                   CHANGE HISTORY for CANIO MODULE                            */
/********************************************************************************/
/* mm/dd/yyyy	User		- 	Change comments									*/
/* 06-17-2008	Francisco	- 	nwmngF_Get_NmStatus_ValidateBusOFF added		*/
/* 11-19-2008   CK          -   Added PowerNet callback functions.              */
/*                              NmSendBroadcastUserData, OnmExtInit             */
/*                              OnmExtConfirmation, OnmExtPrecopy               */
/*                              NmGetRingResource, OnmExtTask                   */
/* 02.05.2009   CK              Commented not used nwmngF_Chck_Micro_Rst func.  */
/* 03.25.2009   CK          -   Deleted not used variable(s) and define(s).     */
/* 04.16.2009   CK          -   Commented PowerNet API functions with Harsha.   */
/* 								Instead, we are linking onmxdc.o 				*/
/********************************************************************************/
/*************************************** CUSW UPDATES ************************************************
 * 08.16.2011 Harsha    - MKS 81849: GenericFailSts and CurrentFailSts flags updated for NWM.
 * 						  nwmngLF_NWM_CSWM_MsgUpdate updated with the GenericFailSts and CurrentFailSts
 * 09-26-2011 Harsha    - MKS 84970: nwmngF_Get_NmStatus_ValidateBusOFF updated to have setting the 
 * 						  CAN Bus OFF DTC.	
/*************************************** CUSW UPDATES **************************************************/

/* 																				*/
/*   NAME:              Network_Management.c                                	*/
/* 																				*/
/* Main functionality of this module is to look for Bus sleep/Wakeup conditions (For SLP9 it is External/Local/Sleep) and set the transceiver according to the conditions.	*/
/* And also looks for input from CANIO module incase for LOC fault set.                                                                                                   	*/
/*                                                                                                                                                                  		*/


/* includes */
#include "il_inc.h"
#include "v_inc.h"
#include "desc.h"
//#include "dpm.h"				// Lin Warning 537: heater file not used. Commented by CK 04.24.09
#include "TYPEDEF.h"
#include "os.h"             // DisableAllInterrupts()
#include "main.h"
#include "Network_Management.h"
#include "canio.h"
#include "Heating.h"
#include "Venting.h"
#include "StWheel_Heating.h"
#include "FaultMemory.h"
#include "FaultMemoryLib.h"
#include "FNmBSl15.h"
#include "mw_eem.h"
#include "Mcu.h"
#include "Dio.h"

/* Module Change History */

/* Variables */
static unsigned char   nwm_BusOFF_counter_c;

/* Defines */
#define C_BUS_OFF_MAX	1//7 // (1 Failure + 5 Phase 'A' retries + 1 Phase 'B' Retry)

/* Updates the NWM_CSWM message */
void nwmngLF_NWM_CSWM_MsgUpdate(void);

/* SDM@nwmngF_Init@@ 	*/
/*						*/
void nwmngF_Init(void)
{
 // nwm_current_fail_state_b = 0;
 // nwm_generic_fail_state_b = 0;
  
	/* Init CAN Driver */
  CanInitPowerOn();
  /* Init Network Management - FNMS15 */
  NmInitPowerOn();
  /*Setting ECU Number for TP Channel*/
  ComSetCurrentECU(currentECU);
  /* Init Transport Layer*/
  TpInitPowerOn();
  /* Init Interaction Layer */
  IlInitPowerOn();
  /* Network Management is turned ON */
  NmSetNewMode(kNmNetOn);
}

void nmCan_Cyclic_Tasks(void)
{
  /* Cyclic Desc tasks */
  DescTask();
  /* Cyclic Transport Layer tasks */
  TpTask();
  /* Cyclic RX and TX update */
  IlRxTask();
  IlTxTask();
  /* Cyclic Network Management - FNMS15 tasks */
  NmTask();
}
void nmCan_Start_IL(void)
{
  /* Start RX and TX*/
  IlRxStart();
  IlTxStart();
}
/****************************************************************************************/
/* Function Name: 	@far void nwmngF_Chck_Micro_Rst(void) - NOT USED             		*/
/* Type: 			Global Function														*/
/* Returns: 		None																*/
/* Parameters: 		None																*/
/* Description:     This function generates a COP reset, if CAN bus off conditions      */
/*                  is present continiously for 10 seconds.								*/
/*																						*/
/* Calls to: 		None																*/
/* Calls from:      NOT CALLED			 												*/
/* 																						*/
/****************************************************************************************/
//void nwmngF_Chck_Micro_Rst(void)
//{
//	/* Check if Can Bus is Off */
//	if(canio_canbus_off_b == TRUE){
//		/* Check for 10 second delay before generating a COP reset */
//		if(nm_cabbusOff_microRst_cnt < CANBUS_OFF_MICRO_RST_TM){
//			/* increment canBusOff 10 second counter */
//			nm_cabbusOff_microRst_cnt++;			
//		}else{
//			/* if this byte is set to TRUE, we will generate a COP reset */
//			main_ECU_reset_rq_c = TRUE;
//		}		
//	}else{
//		/* Check if we need to clear the 10 second counter */
//		if(nm_cabbusOff_microRst_cnt != 0){
//			/* clear the canBusOff 10 second counter */
//			nm_cabbusOff_microRst_cnt = 0;
//		}else{
//			/* No need to clear the canBusOff 10 second counter */
//		}
//	}
//}

/* SDM@nwmngF_State_Transition@@ 	*/
/*									*/
void nwmngF_State_Transition(void)
{
//	if ((hs_rem_seat_req_c[FRONT_LEFT] != SET_OFF_REQUEST) ||
//	    (hs_rem_seat_req_c[FRONT_RIGHT] != SET_OFF_REQUEST) ||
//	    (hs_rem_seat_req_c[REAR_LEFT] != SET_OFF_REQUEST) ||
//	    (hs_rem_seat_req_c[REAR_RIGHT] != SET_OFF_REQUEST)||
//	    (vs_Flag2_c[VS_FL].b.swtch_rq_prps_b != FALSE) ||
//	    (vs_Flag2_c[VS_FR].b.swtch_rq_prps_b != FALSE) || 
//	    (stW_swtch_rq_prps_b != FALSE) )
//	{
//		
//		//DpmExternalRequest(DPM_CSWM);
//	}
//	else
//	{
//		//DpmLocalRequest(DPM_CSWM);
//	}
	nwmngF_Get_NmStatus_ValidateBusOFF ();
	
	nwmngLF_NWM_CSWM_MsgUpdate ();
}


/*******************************************************************************************************/
/* Function Name:	nwmngLF_NWM_CSWM_MsgUpdate
 * called in:		
 * Description:		NM Message to get updated with valid information related to the ECU.
 * 					The following parameters to be updated.
 * 					typedef struct _c_NWM_CSWM_RDS_msgTypeTag
					{
  						vbittype Zero_byte : 8;
  						vbittype SystemStatus : 2;
  						vbittype ActiveLoadSlave : 1;
  						vbittype EOL : 1;
  						vbittype GenericFailSts : 1;
  						vbittype CurrentFailSts : 1;
  						vbittype D_ES : 2;
					} _c_NWM_CSWM_RDS_msgType;
					
 *	Zero Byte:	Fixed filed with always equal to 0, to guarantee that a NM message alwaya generate wakeup
 *	SC:				0 = Wakeup Request .... SYSTEM STATUS
 * 					1 = Not used
 * 					2 = SA (System stay Active response)
 * 					3 = Not used
 * 	AL:				0 = No ACTIVE LOADs in Slave. Slave node does not need to communicate with others to perform
 * 				    	its functions.
 * 					1 = Active loads in Slave. The salve node appl needs to communicate with other nodes to
 * 				    	perform its functions.
 * 	D_ES			0 = The CAN controller is in Error Active State ..... DLL ERROR STATUS
 * 					1 = The CAN Controller is in Error Passive/Warning state
 * 					2 = The CAN controller is in Bus OFF State
 * 					3 = Not used.
 * 	CurrentFailSts:	0 = No DTC is present with "DTC Status = Present"
 * 					1 = Atleast one DTC with "DTC Status = Present" is set in System Memory
 * 	GenericFailSts:	0 = No DTC is set
 * 					1 = Atleast one DTC is set in the system memory
 * 	EOL:			0 = No EOL Programming at FIAT
 * 					1 = EOL Programming at FIAT
/*******************************************************************************************************/

void nwmngLF_NWM_CSWM_MsgUpdate (void)
{
	unsigned char Nm_status = 0x00; //By default status is false. Update only in functions If the status changes.
	
	//Nm_status = fmem_currentFailSts;
	NmSetCurrentFailState (fmem_currentFailSts);
	//Nm_status = fmem_genericFailSts;
	NmSetGenericFailState (fmem_genericFailSts);
	Nm_status = 0x01; //EOL is to be present for CUSW.
	NmSetEolState (Nm_status);
	
}

/* SDM@nwmngF_Get_NmStatus_ValidateBusOFF@@ 								 */
/*																			 */
/* Get the NM Status and look for Bus OFF DTC Condition gets cleared if set. */
/* Enter inside the logic only If Bus OFF Performance was TRUE.              */
/* Clear the Bus OFF Performance DTC, If Network status is active.           */
/*																			 */
void nwmngF_Get_NmStatus_ValidateBusOFF(void)
{
	
    unsigned char nwm_bus_off_start_event;
    unsigned char nwm_bus_off_end_event;

    // Copy Overrun and bus off flags to local
    // variables since they are set on interrupts
    DisableAllInterrupts();

    nwm_bus_off_start_event = can_bus_off_start_b;
    nwm_bus_off_end_event = can_bus_off_end_b;
    
    can_bus_off_start_b = FALSE;
    can_bus_off_end_b = FALSE;
    
    //Enable the Interrupts
    EnableAllInterrupts();
   
    //Handle Bus OFF Event start
    if(nwm_bus_off_start_event == TRUE)
    {
    	if(nwm_BusOFF_counter_c < C_BUS_OFF_MAX)
    	{
    		nwm_BusOFF_counter_c++;
    	}
    }
    
    // Handle Bus Off End Event
    if (nwm_bus_off_end_event == TRUE)
    {
        // reinitialize transceiver!
		/* Enable CAN communication (by pulling standby pin low) */
		Dio_WriteChannel(CAN_STB, STD_LOW);
		canio_canbus_off_b = FALSE;
    }
    
    if(nwm_BusOFF_counter_c >= C_BUS_OFF_MAX)
    {
    	FMemLibF_ReportDtc(DTC_CAN_INTERIOR_BUSOFF_K, ERROR_ON_K);
    	canio_canbus_off_b = TRUE;
    }
    
    if(canio_canbus_off_b == FALSE)
    {
    	FMemLibF_ReportDtc(DTC_CAN_INTERIOR_BUSOFF_K, ERROR_OFF_K);
    }
    
    
}

void ApplNmCanNormal (void)
{
	/* Communication is normal */
	canio_canbus_off_b = FALSE;
    nwm_BusOFF_counter_c = 0;
}
void ApplNmCanSleep (void)
{

}
void ApplNmBusOff (void)
{
    IlTxWait();
    IlRxWait();
    can_bus_off_start_b = TRUE;
    
    //FMemLibF_ReportDtc(DTC_CAN_INTERIOR_BUSOFF_K, ERROR_ON_K);//Testing
}
void ApplNmBusOffEnd (void)
{
    IlTxRelease();
    IlRxRelease();
    can_bus_off_end_b = TRUE;
    nwm_BusOFF_counter_c = 0;
    
    //FMemLibF_ReportDtc(DTC_CAN_INTERIOR_BUSOFF_K, ERROR_OFF_K); //Testing
}
// void CanWakeUpInterrupt (void)
// {
// 
// }
void ApplCanWakeUp(void)
{
    /* Callback function at the transition from sleep to sleep indication recommended */
}


