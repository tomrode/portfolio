#ifndef CANIO_H
#define CANIO_H

/********************************************************************************/
/*                   CHANGE HISTORY for CANIO MODULE                            */
/********************************************************************************/
/* mm/dd/yyyy	User		- 	Change comments									*/
/* 02-05-2008	Harsha		- 	First created for VMM742						*/
/* 06-16-2008	Francisco	- 	canio_canbus_off_b added						*/
/* 08-04-2008   Harsha      -   canio_SyncPrplsnSysAtv_c added                  */
/* 04-14-2009   CK          -   New variables added for CCM915.                 */
/*                              canio_Batt_ST_Crit_c, canio_EC_ECUCfg5_Stat_c,  */
/*                              and canio_EC_Memory_w                           */
/* 04-15-2009   Harsha      -   canF_CBC_LOC_DTC and canF_TGW_LOC_DTC functions */
/*                              added.                                          */
/*                          -   New variables canio_NetCfg_CBC_IN_c and         */
/*                              canio_NetCfg_TGW_c added for LOC detection to PN*/
/* 04-28-2009   Harsha      -   ecucfg_update_b flag added for PN               */
/* 25-11-2009   Christian   -  added canio_TGW_Disp_Stat_c FTR00139668          */
/* 04-28-2010   Harsha      -  Moved REMST_ENABLED/DISABLED and                 */
/*                             canio_RemSt_stored_stat from canio.c to canio.h  */ 
/* 09-28-2010   Harsha      -  #ifdef PWR_NET commented. ECU configuration      */
/*                             commneted                                        */ 
/********************************************************************************
 * 06-15-2011	Harsha	 75322	Cleanup the code. Delete the unused code for this program.
 * 
 * 
 * 
 * 
 * 
 * 
**********************************************************************************/
/*   NAME:              CANIO.H                                   				*/
/* 																				*/

#undef EXTERN 
#ifdef CANIO_C 
#define EXTERN 
#else
#define EXTERN extern
#endif

/* defines */

#define CANBUS_KILOMETER_INVALID_K       0xffff

#define REMST_ENABLED                    0x55 // Remote Start is enable
#define REMST_DISABLED                   0xAA // Remote Start is disabled

/* Enumerations */

EXTERN enum TOTAL_HEAT_SEATS{
FRONT_LEFT  //The same used for Partial OPEN DTC Fronts
,FRONT_RIGHT
,REAR_LEFT
,REAR_RIGHT
,ALL_HEAT_SEATS
};

EXTERN enum HEAT_REAR_SWITCHES{
RL_SWITCH
,RR_SWITCH     //The same used for Partial OPEN DTC Rears
,ALL_REAR_SWITCH
};

EXTERN enum TOTAL_VENT_SEATS{
LEFT_FRONT
,RIGHT_FRONT
,ALL_VENT_SEATS
};

/* enums for the Heat timers..either start, pause or clear */

EXTERN enum TIMER_STATES {
TIMER_START
,TIMER_PAUSE
,TIMER_CLEAR
};

EXTERN enum HS_SWITCH_REQ{
 HS_NOT_PSD   = 0    //0
,HS_PSD       = 1      //4
,HS_SNA       = 3
,HS_OFF       = 7  // Not used
};

/* Bit variables */

EXTERN union char_bit canio_status_flags_c;
#define update_original_vin_b   canio_status_flags_c.b.b0 //Flag for updates to original vin when it is ready
#define update_current_vin_b    canio_status_flags_c.b.b1 //Flag for updates to current VIN when it is ready
#define update_veh_info_b       canio_status_flags_c.b.b2 //Flag for updates to vehicle info when it is ready
#define update_vin_odo_count_b  canio_status_flags_c.b.b3 //Flag denotes time to update Vin odo counter
#define timeto_update_vehinfo_b canio_status_flags_c.b.b4 //Flag denotes an update to veh info to eep
#define update_over_veh_info_b  canio_status_flags_c.b.b5 //Update already happened
#define ecucfg_update_b         canio_status_flags_c.b.b6 //Service parts update(1 = complete,0 - not completed)

EXTERN union char_bit  canio_status2_flags_c;
#define canio_canbus_off_b		canio_status2_flags_c.b.b0 //Final CAN Bus Off flag
#define final_CCN_LOC_b         canio_status2_flags_c.b.b1 //Final CCN LOC flag to blow DTC and disable functionality
#define final_FCM_LOC_b         canio_status2_flags_c.b.b2 //Final FCM LOC flag to blow DTC and disable functionality
#define final_HVAC_LOC_b        canio_status2_flags_c.b.b3 //Final HVAC LOC flag to blow DTC and disable functionality
#define final_CBC_LOC_b         canio_status2_flags_c.b.b4 //Final CBC LOC flag to blow DTC and disable functionality
#define final_TGW_LOC_b         canio_status2_flags_c.b.b5 //Final TGW LOC flag to blow DTC and disable functionality
#define can_bus_off_start_b     canio_status2_flags_c.b.b6 //Flag denotes Bus OFF event started.Called in ApplNmBusOff callback function.
#define can_bus_off_end_b       canio_status2_flags_c.b.b7 //Flag denotes Bus OFF event end.Called in ApplNmBusOffEnd callback function.

EXTERN unsigned char canio_RemSt_stored_stat;   // Stored Remote Start status. Read from non-volatile memory. 


/* Values from CAN BUS received by CSWM */

/* Travel Distance value from CAN Bus */
EXTERN unsigned short canio_RX_TravelDistance_w; //Reads the Travel Distance value
/*Total ODO Value from CAN Bus */
EXTERN unsigned char canio_RX_TotalOdo_c[3]; //Total ODO

/* Battery Voltage from CAN Bus */
EXTERN unsigned char canio_RX_BatteryVoltageLevel_c;
/* Switch Requests from CAN Bus */
/* HEated seat Switch Request from CAN */
//EXTERN unsigned char canio_heated_seat_request_c [ALL_HEAT_SEATS];
//#define canio_L_F_HS_RQ_c  canio_heated_seat_request_c[ FRONT_LEFT ]
//#define canio_R_F_HS_RQ_c  canio_heated_seat_request_c[ FRONT_RIGHT ]
//#define canio_L_R_HS_RQ_c  canio_heated_seat_request_c[ REAR_LEFT ]
//#define canio_R_R_HS_RQ_c  canio_heated_seat_request_c[ REAR_RIGHT ]
/* HEated seat Switch Request from CAN bus NEW signal from HVAC*/
EXTERN unsigned char canio_hvac_heated_seat_request_c [ALL_HEAT_SEATS];
#define canio_RX_FL_HS_RQ_TGW_c  canio_hvac_heated_seat_request_c[ FRONT_LEFT ] //L and w series
#define canio_RX_FR_HS_RQ_TGW_c  canio_hvac_heated_seat_request_c[ FRONT_RIGHT ]	//L and w series

//#define canio_LR_HS_RQ_2_c  canio_hvac_heated_seat_request_c[ REAR_LEFT ] //RM and RT vehicle line
//#define canio_RR_HS_RQ_2_c  canio_hvac_heated_seat_request_c[ REAR_RIGHT ] //RM and RT vehicle line

/* Vent seat switch request from CAN bus from CCN*/
EXTERN unsigned char canio_vent_seat_request_c [ALL_VENT_SEATS];
#define canio_L_F_VS_RQ_c  canio_vent_seat_request_c [LEFT_FRONT]
#define canio_R_F_VS_RQ_c  canio_vent_seat_request_c [RIGHT_FRONT]
/* Vent seat switch request from CAN bus NEW from HVAC*/
EXTERN unsigned char canio_hvac_vent_seat_request_c [ALL_VENT_SEATS];
#define canio_LF_VS_RQ_c  canio_hvac_vent_seat_request_c [LEFT_FRONT]
#define canio_RF_VS_RQ_c  canio_hvac_vent_seat_request_c [RIGHT_FRONT]

/* Heated Steering Wheel request from CAN */
//EXTERN unsigned char canio_HSW_RQ_c; //From CCN
EXTERN unsigned char canio_RX_HSW_RQ_TGW_c; //New signal from TGW 
/* Steering Wheel Signals */
/* Heated Steering Wheel temperature and temperature sensor fault */
//EXTERN unsigned char canio_HSW_TEMPERATURE_c;		//From BCM_A8
//EXTERN unsigned char canio_HSW_TEMPSENSOR_FAULT_c;  //From BCM_A8
EXTERN unsigned char canio_RX_StW_TempSts_c; //From SCCM_A1
EXTERN unsigned char canio_RX_StW_TempSens_FltSts_c; //From SCCM_A1
EXTERN unsigned char canio_RX_EngineSts_c;
EXTERN unsigned char canio_RX_EngineSpeedValidData_c;

/* Enable values for Outputs to turn ON like IGN status, LOAD_SHED status, ENG_RPM status etc. */
/* Ignition status from CAN Bus */
EXTERN unsigned char canio_RX_IGN_STATUS_c;
/* Load Shed status from CAN Bus */
EXTERN unsigned char canio_LOAD_SHED_c;

/*Load shed status from CAN Bus for Powernet */
//EXTERN unsigned char canio_PN14_LS_Actv_c;
//EXTERN unsigned char canio_PN14_LS_Lvl1_c;
//EXTERN unsigned char canio_PN14_LS_Lvl2_c;
//EXTERN unsigned char canio_PN14_LS_Lvl3_c;
//EXTERN unsigned char canio_PN14_LS_Lvl7_c;
//EXTERN unsigned char canio_Batt_ST_Crit_c;

/*TGW status  from CAN Bus for Powernet FTR00139668  --   TGW LOC detection change-Now If module receives TGW_DISP_STAT = SNA blow TGW LOC */
//EXTERN unsigned char canio_TGW_Disp_Stat_c;

/* Engine RPM value from CAN Bus */
EXTERN unsigned char canio_RX_EngineSpeed_c;
/* Vehicle Information like MY, Veh Line, Body style etc.. */
/* Vehicle Information from CAN Bus */
EXTERN unsigned char canio_VEH_LINE_c;
//EXTERN unsigned char canio_vehicle_c; //This value is used for other modules, derived from eep.
EXTERN unsigned char canio_MODEL_YEAR_c;
EXTERN unsigned char canio_BODY_STYLE_c;
EXTERN unsigned char canio_COUNTRY_c;
EXTERN unsigned char canio_LHD_RHD_c;

/* Net config status from CAN Bus */
//EXTERN unsigned char canio_NET_CFG_STAT_c;
//EXTERN unsigned char canio_CGW_Stat_c; //For TIPM
//EXTERN unsigned char canio_CCN_Stat_c; //For TIPM
//EXTERN unsigned char canio_NetCfg_CBC_IN_c; //For PowerNet
//EXTERN unsigned char canio_NetCfg_TGW_c; //For PowerNet.

/* VIN from CAN Bus */
EXTERN unsigned char canio_Vehicle_ID_Number [17];
EXTERN unsigned char canio_CurrentVIN[17];

/* IOD Fuse out, AMP Temp from CAN Bus */
//EXTERN unsigned char canio_IOD_FUSE_OUT_c;
EXTERN unsigned char canio_RX_ExternalTemperature_c;

/* VIN Information */
/* Remote Start Event Signals */
EXTERN unsigned char canio_RX_Rem_CFG_FEATURE_c;
EXTERN unsigned char canio_RX_Rem_CFG_STAT_RQ_c;
EXTERN unsigned char canio_RX_IgnRun_RemSt_c;
EXTERN unsigned char canio_RemSt_configStat_c;

/* Raw values received from CAN Bus */
//EXTERN unsigned char canio_raw_ign1_c;
//EXTERN unsigned char canio_ign_start_type_c;

/* System Propolsion Active info for Hybrid Vehicles.          */
/* This information together with ENG_RPM to be added in logic */
//EXTERN unsigned char canio_SyncPrplsnSysAtv_c;

/* For PowerNet architecture CSWM variant selection logic */

/* ECU configuration message status 5 */
/* This information will be used in CSWM variant selection. */
/* ECUCfg5_stat should be PROGRAMMED (0x01) for variant logic. */
//EXTERN unsigned char canio_EC_ECUCfg5_Stat_c;

///* Memory ECU Configuration (16-bit value) */
//EXTERN unsigned short canio_EC_Memory_w;

/* TIMEOUT SIGNALS */
EXTERN unsigned char canio_FL_HS_RQ_TGW_Timeout_c; //Timeout counter to set DTC for LOC TGW (>10sec)
EXTERN unsigned char canio_IgnRun_RemSt_Timeout_c; //Timeout counter to set DTC for LOC CBC (>1sec)

/*******************************  GLOBAL Strcuture *********************************/
//struct Proxi_cfg{
//	  unsigned char model_year_c;            //MY Information
//	  unsigned char vehicle_line_c;          //Vehicle line information (CT/DS)
//	  unsigned char ecu_cfg_c;               //Known ECU configuration  
//	  unsigned char seat_cfg_c;              //Known SEAT configuration (Cloth/Base L/Perf L)
//	  unsigned char wheel_cfg_c;             //Known Wheel Type (Leather/Wood)
//};
//
//EXTERN struct Proxi_cfg Proxi_data_s;

/* Function prototypes */
@far void canF_Init(void);
/* Writes Vehicle line Information to Mirror */
@far void canF_CyclicTask(void);
/* Evaluate remote start signal */
@far void canF_CyclicRemoteStart(void);

/* Call all manual Indication Function every 10 msec */
@far void canF_CyclicManIndFunc(void);

/* CBC LOC Detection Function */
void canF_CBC_LOC_DTC(void);
/* TGW LOC Detection Function */
void canF_TGW_LOC_DTC(void);

void  CSWM_VehStart_PKT_ManIndFunc(void);

void CSWM_EngRPM_ManIndFunc(void);

void  CSWM_FL_HS_RQ_TGW_ManIndFunc(void);

void  CSWM_FR_HS_RQ_TGW_ManIndFunc(void);

void  CSWM_HSW_RQ_TGW_ManIndFunc(void);

void  CSWM_ExtTemp_ManIndFunc(void);

void  CSWM_BatVolt_ManIndFunc(void);

void  CSWM_ODO_ManIndFunc(void);

void  CSWM_CFG_RQ_ManIndFunc(void);

void  CSWM_StW_Actn_Rq_ManIndFunc(void);

void  CSWM_VIN_ManIndFunc (void);

void  CSWM_EngineSts_ManIndFunc (void);

void  CSWM_EngineSpeedValidData_ManIndFunc (void);

void  CSWM_CFG_DATA_RESP_ManIndFunc (void);



void /*DBK_API_CALLBACK_TYPE*/ CSWM_VehCfg1_Pkt_IndFunc(void);

void /*DBK_API_CALLBACK_TYPE*/ CSWM_NET_CFG_DATA_INT_IndFunc(void);

void /*DBK_API_CALLBACK_TYPE*/ CSWM_PN14_LHC_Stat_IndFunc(void);

void /*DBK_API_CALLBACK_TYPE*/ CSWM_EC_ECUCfg5_Stat_IndFunc(void);

void /*DBK_API_CALLBACK_TYPE*/ CSWM_EC_Memory_IndFunc(void);

void /*DBK_API_CALLBACK_TYPE*/ CSWM_TGW_DISP_STAT_IndFunc(void);


#endif