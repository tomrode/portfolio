#ifndef NETWORK_MANAGEMENT_H
#define NETWORK_MANAGEMENT_H

/********************************************************************************/
/*                   CHANGE HISTORY for NETWORK_MANAGEMENT MODULE              	*/
/********************************************************************************/
/* mm/dd/yyyy	User		- Change comments									*/
/* 02-07-2008	Harsha		- File Added										*/
/* 06-17-2008	Francisco	- 	undef EXTERN added						   		*/
/*								DTC_NO_BAT_FOR_STEERING_WHEEL_K code changed	*/
/*								nwmngF_Get_NmStatus_ValidateBusOFF added		*/
/********************************************************************************/
/*   NAME:              NETWORK_MANAGEMENT.H                       	            */
/* 																				*/

#undef EXTERN 
#ifdef NETWORK_MANAGEMENT_C
#define EXTERN 
#else
#define EXTERN extern
#endif

//EXTERN union char_bit  nwm_status_flags_c;
//#define nwm_current_fail_state_b    nwm_status_flags_c.b.b0
//#define nwm_generic_fail_state_b    nwm_status_flags_c.b.b1

/* Function Prototypes */
/* Init function for the DPM,CCL modules. Initializes the Tranceiver settings internally. */
extern @far void nwmngF_Init(void);
extern @far void nmCan_Cyclic_Tasks(void);
extern @far void nmCan_Start_IL(void);
/* If Can bus is off, it generates a COP reset after 10 second delay (40ms Timeslice) */
/* THIS FUNCTION SHOULD NOT BE USED */
//EXTERN @far void nwmngF_Chck_Micro_Rst(void);

/* Gives the state transition from External to Local */
extern @far void nwmngF_State_Transition(void);

/* Check for Bus OFF DTC Condition */
extern @far void nwmngF_Get_NmStatus_ValidateBusOFF(void);

extern void ApplNmCanNormal( void );

extern void ApplNmCanSleep( void );

extern void ApplNmBusOff(void);

extern void ApplNmBusOffEnd(void);

#endif
