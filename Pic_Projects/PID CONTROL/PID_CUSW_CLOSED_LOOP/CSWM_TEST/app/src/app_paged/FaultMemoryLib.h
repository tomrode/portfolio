#ifndef FAULTMEMORYLIB_H
#define FAULTMEMORYLIB_H

/********************************************************************************/
/*                   CHANGE HISTORY for FAULTMEMORYLIB MODULE                   */
/********************************************************************************/
/* mm/dd/yyyy	User		- 	Change comments									*/
/********************************************************************************/
/************************************* CUSW UPDATES ******************************************************/
/* 10.29.2010 Harsha - No OF ERROR MEMORY records are 10. older chrono stack is 8                        */
/*                     Updated SIZEOF_DTC_MEMORY, DTC_MEMORY_ELEMENT and DTC_HISTORICAL_MEMORY_ELEMENT   */
/* 06-05-2011 Harsha - MKS 74407:With update CHRONO contains 110 bytes, Historical contains 90 bytes     */
/*                     Chrono contains 1st snapshot and Envi data, Historical contains 2nd snap shot data*/
/*                   - Status Byte updated to fill with FIAT Standards.                                  */
/* 08-16-2011 Harsha - FMemLibF_NMFailSts added.
/************************************* CUSW UPDATES ******************************************************/

/*   NAME:              FAULTMEMORYLIB.H                               			*/
/* 																				*/

#undef EXTERN 
#ifdef FAULTMEMORYLIB_C 
#define EXTERN 
#else
#define EXTERN extern
#endif

enum FAULTMEMORY_TYP
{
 FAULT_MEMORY,
 HISTORICAL_MEMORY
};

enum ERROR_STATES
{
 ERROR_OFF_K      = 0,
 ERROR_ON_K       = 1,
 ERROR_NO_CHECK_K = 2
};

/* UDS DTC Status information Supported Bits Bit 0, 3, 4, 5 (for CHRYSLER)                              */
/* UDS DTC Status information Supported Bits Bit 0, 1, 2, 3 (for FIAT)  				    			*/
/* 															*/
/* Bit 0 Information   Test Failed    Mandatory (Chrysler/FIAT)  										*/
/* This bit shall indicate the result of the most recently performed test. 								*/
/* A logical '1' shall indicate that the last test failed meaning that the failure is completly matured	*/
/* Reset to '0' if the result of the most recently performed test returns a "pass" result meaning that 	*/ 
/* all de-mature criteria have been fulfilled 'or' after a call has been made to Clear Diagnostic 		*/
/* Information.																							*/
/* 1 : most recent return from DTC test indicated a failed result. 										*/
/* 0 : most recent return from DTC test indicated no failure detected. 									*/
/* 																										*/
/* 																										*/
/* Bit 1 Information   Test Failed This Operation Cycle    Optional/Conditional(Chrysler) Mandatory(FIAT) 		*/ 
/* This bit shall indicate either that a diagnostic test has reported a Test Failed result at any time during	*/
/*   the current operation cycle 																				*/
/* Reset to 0 when a new monitoring cycle is initiated or after a call to Clear Diagnostic Information 			*/
/* Once this bit is set to 1, it shall remain a 1 until a new monitoring cycle is started. 						*/
/* Test Failed bit = 1 was reported at least once during the current monitoring cycle. 							*/
/* Test Failed bit = 0 has not been reported during the current monitoring cycle or after a call was made 		*/
/* to clear Diagnostic information during the current operation cycle 											*/
/* 																												*/
/* 																												*/
/* Bit 2 Information   Pending DTC    Optional(Chrysler) FIAT (Mandatory) 										*/
/* This bit shall indicate whether or not a diagnostic test has reported a Test Failed result at any time 		*/
/*   during the current or last completed operation cycle. The status shall only be updated if the test runs  	*/
/*   and completes 																								*/
/* The criteria to set the "Pending DTC" bit and the "Test failed This Operation cycle" are the same. The 		*/
/*   difference is that the Test failed this operation cycle is cleared at the end of current operation cycle 	*/
/*   but the Pending DTC bit is not cleared until an operation cycle completed where the test has passed 		*/
/*   atleast once and never failed 																				*/
/* 1 : This bit shall be set to 1 and latched if a malfunction is detected during the current monitoring 		*/ 
/*       cycle 																									*/
/* 0 : This bit shall be set to 0 after completing a monitoring cycle during which the test completed 			*/
/*       and a malfunction was not detected or upon a call to the Clear Diagnostic Information service. 		*/
/* 																												*/
/* 																												*/
/* Bit 3 Information   Confirmed DTC    Mandatory (Chrysler/FIAT) 												*/
/* This bit shall indicate whether a malfunction was detected enough times to warrant that the DTC is stored 	*/
/*   in long-term memory ( depending on the DTC confirmation criteria). This information can be used by the 	*/
/*   external test tool to request additional diagnostic information such as extended data records 				*/
/* A confirmed DTC does not always indicate that the malfunction is necessarily present at the time of 			*/
/*   request.(Pending DTC or Test Failed This Monitoring Cycle can be used to determine if a malfunction is 	*/
/*   present at the time of request 																			*/
/* 1 : DTC confirmed at least once since the last call to Clear Diagnostic Information and de-mature criteria 	*/
/*       have not yet been satisfied. 																			*/
/* 0 : DTC has never been confirmed since the last call to Clear Diagnostic Information or de-mature criteria 	*/
/*       have been satisfied for the DTC or after a call to Clear Diagnsotic Information 						*/
/* 																												*/
/* 																												*/
/* Bit 4 Information   Test Not Completed Since Last Clear    Mandatory (Chrysler) Not used (FIAT) 				*/
/* This bit shall indicate whether a DTC test has ever run and completed since the last time a call was			*/
/* made to clear Diagnostic Information 																		*/
/* One (1) shall indicate that the DTC test has not run to completion. 											*/
/* If the test runs and passes or fails (Test Failed This Monitoring Cycle = 1) 								*/
/* then the bit shall be set to a Zero (0) 																		*/
/* Reset to One (1) after a call to Clear Diagnostic Information. 												*/
/* 1 : DTC test has not run to completion since the last time diagnostic information was cleared 				*/
/* 0 : DTC test has returned either a passed or failed test result at least one time since the last time 		*/
/* diagnostic information was cleared 																			*/ 
/* 																												*/
/* 																												*/
/* Bit 5 Information   Test Failed Since Last Clear    Optional (Chrysler) Not used (FIAT)						*/
/* This bit shall indicate whether a DTC test has ever returned a Test Failed = 1 result since the last time	*/
/* a call was made to clear diagnostic information (latched Test Failed This Monitoring Cycle =1) 				*/
/* Zero (0) shall indicate that the test has not run or that the DTC test ran and passed (but never failed)		*/
/* If the test runs and fails then the bit shall remain latched at a 1. 										*/
/* Reset to Zero ( 0 ) after a call to Clear Diagnostic Information. In contradiction to the Confirmed DTC		*/
/* this bit is not reset by aging criteria or whe nit was over written due to an overflow of fault memory 		*/
/* 1 : DTC test returned a Test Failed This Monitoring Cycle = 1 result at least once since the last time		*/
/*     diagnostic information was cleared. 																		*/
/* 0 : DTC test has not indicated a Test Failed This Monitoring Cycle = 1 result since the last time			*/
/*     diagnostic information was cleared. 																		*/
/* 																												*/
/* 																												*/
/* Bit 6 Information   Test Not Completed This Operation Cycle    Optional (Chrysler/FIAT)						*/
/* This bit shall indicate whether a DTC test has ever run and completed during the current operation cycle		*/
/* 1 : DTC test has not run to completion this monitoring cycle (or since the last time diagnostic 				*/
/*     information was cleared this monitoring cycle) 															*/
/* 0 : DTC test has returned either a passed or Test Failed This Monitoring Cycle = 1 result during the			*/
/*     current monitoring cycle or a call was made to Clear Diagnostic Information. 							*/ 
/* 																												*/
/* 																												*/
/* Bit 7 Information   Warning Indicator Requested    Optional (Chrysler/FIAT)									*/
/* This bit shall report the status of any warning indicators associated with a particular DTC 					*/
/* Warning outputs may consist of indicator lamp(s), displayed text information, etc. 							*/
/* If no warning indicators exist for a given system or particular DTC, this status shall default to '0' 		*/
/* 1 : Warning indicator requested to be ON. 																	*/
/* 0 : Warning indicator requested to be OFF. 																	*/
/* If the warning indicator is on for a given DTC, then Confirmed DTC shall be also be set to '1' 				*/
/* 																												*/
struct STATE            // DTC Status
 {
  unsigned char         TestFailed:                             1;      // 
  unsigned char         TestFailedThisMonitoringCycle:          1;      // 
  unsigned char         Pending:                                1;      // 
  unsigned char         Confirmed:                              1;      // 
  unsigned char         TestNotCompletedSinceLastClear:         1;      // 
  unsigned char         TestFailedSinceLastClear:               1;      // 
  unsigned char         TestNotCompletedThisMonitoringCycle:    1;      // 
  unsigned char         WarningIndicatorRequested:              1;      // 
 };

/*****************************	SNAPSHOT RECORD 1 and ENVIRONMENTAL DATA *****************************/
/************************************** TOTAL 10 RECORDS *********************************************/ 
/* Chrono Stack (DTC Memory) Details */
#define SIZEOF_DTC_MEMORY                      10      //Size of a Chrono stack Memory

// Each Record now will have 15 bytes. 15 * 10 = 150 Bytes.
struct DTC_MEMORY_ELEMENT
{
// unsigned short         original_odometer;    // Odometer reading with entry of the error in error memory
// unsigned short         most_recent_odometer; // current odometer reading of the error in the error memory
 unsigned char          frequency_counter;        // No of Times DTC Occured.
 unsigned char          operation_cycle_counter;  // Event counter. Once DTC stored, no of IGN cycles passed.
 unsigned char          dtc_RomIndex;             // DTCcodeindex im ROM
 union long_char        ecu_lifetime;             // ECU Time stamps (Life time in 1min increments)
 unsigned short         ecu_time_keyon;           //ECU Time since KEY ON in this IGN cycle (in 15sec increments)
 unsigned short         key_on_counter;           // No of IGN Cycles
};

//DTC_Memory contains max of 10 DTC information
struct DTC_MEMORY
{
  struct DTC_MEMORY_ELEMENT elem[SIZEOF_DTC_MEMORY];     
};

EXTERN struct DTC_MEMORY d_DtcMemory;   // Chrono stack memory

/*****************************	SNAPSHOT RECORD 2 *****************************/
/************************************** TOTAL 10 RECORDS *********************************************/ 
/* Historical Memory Details */
#define SIZEOF_DTC_HISTORICAL_MEMORY          SIZEOF_DTC_MEMORY  //Size of Historical Memory 

//Second SnapShot Record
//The information in the second snapshot record must be stored and/or updated each time there is a 
//transition from 0 to 1 of the test failed bit having confirmed DTC(Error already stored) at value 1

// Each Record consists of 9 Bytes 9 * 10 = 90 Bytes.
struct DTC_HISTORICAL_MEMORY_ELEMENT
{
// u_temp_l               ecu_lifetime;        // ECU Time stamps (Life time in 1min increments)
 union long_char        ecu_lifetime;             // ECU Time stamps (Life time in 1min increments)
 unsigned short         ecu_time_keyon;      //ECU Time since KEY ON in this IGN cycle (in 15sec increments)
 unsigned short         key_on_counter;      // No of IGN Cycles
 unsigned char          dtc_RomIndex;        // DTC code index im ROM
};

// EEPROM Historical Table ..Historical memory max of 10 DTCs in Historical stack
struct DTC_HISTORICAL_MEMORY
{
  struct DTC_HISTORICAL_MEMORY_ELEMENT elem[SIZEOF_DTC_HISTORICAL_MEMORY];      // 
};

EXTERN struct DTC_HISTORICAL_MEMORY d_DtcHistoricalMemory;  // Historical stack memory

/************************************ DTC CODE *******************************************/
/* Structure for Error code table (DTC_ROM_STR) */
union tByte_l
{
  unsigned long         all;
  unsigned char         byte[4];
};

typedef unsigned long DtcType;

typedef union
{
 DtcType                all;
 unsigned char          byte[sizeof(DtcType)]; 
}DtcStructType;

/***********************************	ALL DTC's LIST	************************************/
//Structure for error code table
struct DTC_ROM_STR
{
  //union tByte_l         dtc_code;
  DtcStructType         dtc_code;
  unsigned char         dtc_error_time          :6;
  unsigned char         dtc_priority            :2;
  unsigned char         dtc_event               :1;
  unsigned char         dtc_warning_indicator   :1;
};

extern const struct DTC_ROM_STR fmem_AllRegisteredDtc_t[];

/**********************************	DTC STATUS ****************************************/
/**************************************************************************************/
/* Structure for diagnostic accesses to RAM (DTC_RAM_STR and d_AllDtc_a[]) */
//Structure for diagnostic accesses to RAM
struct DTC_RAM_STR
{
 unsigned char          dtc_on_time             :6;
 enum ERROR_STATES      reported_state          :2;
 union
  {
   unsigned char        all;                             
   struct STATE         state;                           
  } DtcState; // DTC Status
};
extern struct DTC_RAM_STR d_AllDtc_a[];


EXTERN unsigned char d_IgnitionStart5secTimer_c;
EXTERN unsigned char d_DtcSettingModeEnabled_t;
EXTERN unsigned char d_OperationCycle_c; //Added new variable.
EXTERN unsigned char Subservice;

EXTERN union long_char fmem_lifetime_l;  //ECU Life time in 1 min increments           
EXTERN u_16Bit   fmem_keyon_time_w; //ECU time from KEY ON in 15sec Increments

EXTERN u_8Bit fmem_genericFailSts;
EXTERN u_8Bit fmem_currentFailSts;

#define FIVE_MINUTE_COUNTER       5   //5 min counter when it updates for every 1min
#define TEN_MINUTE_COUNTER		 10   //10 min counter when it updates for every 10 min.

#define FIFTEEN_SEC_TIMER        47   //15sec timer when it runs in 320ms
#define ONE_MINUTE_TIMER        187   //1 min timer when it runs in 320ms

#define CLEAR_COUNTER             0     // Clear the 15sec amd 1min counter 

/* Function Prototypes */
@far void FMemLibF_Init(void);
@far void FMemLibF_CyclicDtcTask(void);
@far void FMemLibF_UpdatestoEEPROM(void);
@far void FMemLibF_ECUTimeStamps(void);
@far void FMemLibF_NMFailSts(void);

void FMemLibF_ReportDtc(DtcType DtcCode, enum ERROR_STATES State);
@far void FMemLibF_CheckDtcConditionTimer(void);
void FMemLibF_ClearDtcMemory(enum FAULTMEMORY_TYP FaultMemoryTyp);

u_temp_l FMemLibF_GetOdometer(void);
u_temp_l FMemLibF_IsOdometerValid(unsigned short Odometer);
unsigned char FMemLibF_GetIgnition(void);
temp_l FMemLibF_CheckDtcSupportOverDtc(DtcType DtcCode);

#endif
