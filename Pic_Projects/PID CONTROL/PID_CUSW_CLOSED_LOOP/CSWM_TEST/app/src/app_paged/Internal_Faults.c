/********************************************************************************/
/*                        A U T H O R   I D E N T I T Y							*/
/********************************************************************************/
/* Initials      Name                   Company									*/
/* --------      --------------------   --------------------------------------- */ 
/* CK            Can Kulduk             Continental Automotive Systems			*/
/* HY            Harsha Yekollu         Continental Automotive Systems          */
/********************************************************************************/


/*********************************************************************************/
/*                                CHANGE HISTORY                                 */
/*********************************************************************************/
/* 05.09.2007 CK - module created						     					 */
/* 03.24.2008 HY - Module copied from CSWM MY09 project.             		     */
/* 09.29.2008 CK - Started adapting code for CSWM MY11                 			 */
/* 01.22.2009 CK - PC-Lint fixes. No complaints from PC-Lint after fixes. 		 */
/* 04.28.2009 CK - Updated IntFlt_F_ClearOutputs function. Place a call to clear */
/*                 xx_Output_Cntrl function to turn off the pwm                  */
/*********************************************************************************/


/*********************************************************************************/
/* Software Design Text                                                          */
/* -------                                                                       */
/* This module monitors the internal fault storage and updates.                  */
/* It also checks and/or sets micro-controller related faults, such as PLL		 */ 
/* unlocked, oscillator (self-clock mode), and RAM cell failures. 				 */
/*********************************************************************************/


/********************************** @Include(s) **********************************/
#include "il_inc.h"
#include "v_inc.h"
#include "TYPEDEF.h"
#include "mw_eem.h"
#include "Internal_Faults.h"
#include "main.h"
#include "FaultMemory.h"
#include "FaultMemoryLib.h"
#include "Mcu.h"
#include "Dio.h"
#include "RamTst.h"
#include "Venting.h"
#include "StWheel_Heating.h"
#include "canio.h"
#include "Heating.h"
/*********************************************************************************/


/********************************* external data *********************************/
/* N
 * O
 * N
 * E */
/*********************************************************************************/


/*********************************** @ variables *********************************/
volatile u_8Bit intFlt_self_clck_mode;   // Indicates that micro-controller has been in self clock mode

Mcu_PllStatusType PLL_status;            // PLL status
u_8Bit intFlt_PLL_unlock_cnt_c;          // Counter to keep track PLL unlock time
u_8Bit intFlt_PLL_err_tm_lmt_c;          // PLL unlock time limit - used to set the internal fault DTC

/* Internal Fault Bytes (for this Ignition cycle) */
union char_bit int_flt_byte0;            // Internal fault byte 0
union char_bit int_flt_byte1;            // Internal fault byte 1
union char_bit int_flt_byte2;            // Internal fault byte 2
union char_bit int_flt_byte3;            // Internal fault byte 3

u_8Bit intFlt_bytes[INT_FLT_BYTES];

/* Stores the Enable(0x55) / Disable(0xAA) high priority fault (disables CAN comm) detection option */
u_8Bit intFlt_config_c;                  // Internal fault Byte 0 disable/enable option
/**********************************************************************************/


/****************************************************************************************/
/*									IntFlt_F_Init 										*/
/****************************************************************************************/
/* Initiliazes internal fault variables after a power on reset.                         */
/*                                                                                      */
/* Calls from                                                                           */
/* --------------                                                                       */
/* main :                                                                               */
/*    IntFlt_F_Init()                                                                   */
/*                                                                                      */
/* Calls to                                                                             */
/* --------                                                                             */
/* EE_BlockRead(EEBlockLocation elocation, u_8Bit *pbydest)    						    */
/****************************************************************************************/
@far void IntFlt_F_Init(void)
{
	/* Make sure all internal fault bytes are cleared */
	int_flt_byte0.cb = 0;    
	int_flt_byte1.cb = 0;
	int_flt_byte2.cb = 0;
	int_flt_byte3.cb = 0;
	
	intFlt_bytes[ZERO] = 0;
	intFlt_bytes[ONE] = 0;
	intFlt_bytes[TWO] = 0;
	intFlt_bytes[THREE] = 0;
	
	/* Initilize the PLL and clock mode variables */
	intFlt_self_clck_mode = INTFLT_NORMAL_CLK_MODE;
	PLL_status = MCU_PLL_LOCKED;
	
	/* PLL unlock couter is reset to zero */
	intFlt_PLL_unlock_cnt_c = 0;
		
	/* Read high priority internal fault detection enable/disable configuration */
	EE_BlockRead(EE_INTERNAL_FLT_CFG, (u_8Bit*)&intFlt_config_c);

	/* Store the PLL unlock time limit. */
	/* If PLL is unlocked more than this time, set the PLL unlocked internal fault */
	EE_BlockRead(EE_PLL_FAULT_TIME_LMT, (u_8Bit*)&intFlt_PLL_err_tm_lmt_c);
}

/****************************************************************************************/
/* Function Name: 	@far void IntFlt_F_Read_Fault_Bytes(void)							*/
/* Type: 			Local Function														*/
/* Returns: 		None																*/
/* Parameters: 		None																*/
/* Description:     This function reads the internal fault bytes.						*/
/*																						*/
/* Calls to: 		EE_BlockRead(EEBlockLocation elocation, u_8Bit *pbydest)			*/
/* Calls from:      IntFlt_F_Chck_Unused_Bits()											*/
/* 																						*/
/****************************************************************************************/
@far void IntFlt_F_Read_Fault_Bytes(void)
{
	/* Read the latest status of the internal fault bytes and store it */
	EE_BlockRead(EE_INTERNAL_FLT_BYTE0, (u_8Bit*)&intFlt_bytes[ZERO]);
	EE_BlockRead(EE_INTERNAL_FLT_BYTE1, (u_8Bit*)&intFlt_bytes[ONE]);
	EE_BlockRead(EE_INTERNAL_FLT_BYTE2, (u_8Bit*)&intFlt_bytes[TWO]);
	EE_BlockRead(EE_INTERNAL_FLT_BYTE3, (u_8Bit*)&intFlt_bytes[THREE]);	
}

/****************************************************************************************/
/*								IntFlt_F_Chck_Unused_Bits								*/
/****************************************************************************************/
/* This function is called cyclically (every 80ms).                                     */
/* This function checks if any of the unused bits of the internal faults is set due to  */
/* an invalid (or unknown) write(s).    											    */
/* It cleares the unused fault bits (if any of them is set) in order to avoid setting   */
/* an internal fault when there is none. 												*/
/*                                                                                      */
/* Calls from                                                                           */
/* --------------                                                                       */
/* IntFlt_F_DTC_Monitor :                                                               */
/*    IntFlt_F_Chck_Unused_Bits()                                                       */
/*                                                                                      */
/* Calls to                                                                             */
/* --------                                                                             */
/* IntFlt_F_Update(u_8Bit Byte_number,u_8Bit Bit_mask,Int_Flt_Op_Type_e intFlt_Op)      */
/****************************************************************************************/
@far void IntFlt_F_Chck_Unused_Bits(void)
{
	/* Read the latest status of the internal fault bytes */
	IntFlt_F_Read_Fault_Bytes();	
	
	/* Check Byte 0 unused bits */
	if ( (intFlt_bytes[ZERO] | BYTE0_USED_BITS) == BYTE0_USED_BITS) {
		/* No Unused bits are set in Byte 0 */
	}
	else {
		/* Update known faults */
		IntFlt_F_Update(ZERO, BYTE0_USED_BITS, INT_FLT_CLR);
	}

	/* Check Byte 1 unused bits */
	if ( (intFlt_bytes[ONE] | BYTE1_USED_BITS) == BYTE1_USED_BITS) {
		/* No Unused bits are set in Byte 1 */
	}
	else {
		/* Update known faults */
		IntFlt_F_Update(ONE, BYTE1_USED_BITS, INT_FLT_CLR);
	}
	
	/* Check Byte 2 unused bits */
	if ( (intFlt_bytes[TWO] | BYTE2_USED_BITS) == BYTE2_USED_BITS) {
		/* No Unused bits are set in Byte 2 */
	}
	else {
		/* Update known faults */
		IntFlt_F_Update(TWO, BYTE2_USED_BITS, INT_FLT_CLR);
	}
	
	/* Check Byte 3 unused bits */
	if ( (intFlt_bytes[THREE] | BYTE3_USED_BITS) == BYTE3_USED_BITS) {
		/* No Unused bits are set in Byte 3 */
	}
	else {
		/* Update known faults */
		IntFlt_F_Update(THREE, BYTE3_USED_BITS, INT_FLT_CLR);
	}
}

/****************************************************************************************/
/*								IntFlt_F_Chck_CAN_Comm  								*/
/****************************************************************************************/
/* This function is called cyclically (every 80ms).                                     */
/* If we recover from a fault that disables CAN communication, this function re-enables */
/* the CAN communication by pulling CAN standby pin low.								*/
/*                                                                                      */
/* Calls from                                                                           */
/* --------------                                                                       */
/* IntFlt_F_DTC_Monitor :                                                               */
/*    IntFlt_F_Chck_CAN_Comm()                                                          */
/*                                                                                      */
/* Calls to                                                                             */
/* --------                                                                             */
/* Dio_WriteChannel( ChannelId, Level)                                                  */
/****************************************************************************************/
@far void IntFlt_F_Chck_CAN_Comm(void)
{
	/* Local variables */
	u_8Bit CAN_Stdy_pin_stat = 0;
	
	/* Store the CAN standby pin status            */
	/* 1. STD_HIGH = CAN communication is disabled */
	/* 2. STD_LOW = CAN communication is enabled   */
	CAN_Stdy_pin_stat = (u_8Bit) Dio_ReadChannel(CAN_STB);
	
	/* Read the internal fault byte that is capable of disabling the CAN communication  ??? */
	// Not needed already read in Monitor function
	//EE_BlockRead(EE_INTERNAL_FLT_BYTE0, (u_8Bit*)&intFlt_bytes[ZERO]);	
	
	/* Check if we have to enable CAN communication */
	if ( (CAN_Stdy_pin_stat == STD_HIGH) && (intFlt_bytes[ZERO] == NO_INT_FLT) && (intFlt_self_clck_mode == INTFLT_NORMAL_CLK_MODE) ) {

		/* Enable CAN communication (by pulling standby pin low) */
		Dio_WriteChannel(CAN_STB, STD_LOW);
	}
	else {
		/* Do not modify CAN standby pin status */
	}
}
/****************************************************************************************/
/*								IntFlt_F_DTC_Monitor    								*/
/****************************************************************************************/
/* This function monitors the internal fault bytes (total of 4 bytes) and if any of the */
/* bits is set in first three bytes, then it matures the internal fault DTC. 			*/
/*                                                                                      */
/* Calls from                                                                           */
/* --------------                                                                       */
/* main :                                                                               */
/*    IntFlt_F_DTC_Monitor()                                                            */
/*                                                                                      */
/* Calls to                                                                             */
/* --------                                                                             */
/* FMemLibF_ReportDtc(DtcType DtcCode,enum ERROR_STATES State)                          */
/* IntFlt_F_Chck_CAN_Comm()                                                             */
/* IntFlt_F_Chck_Unused_Bits()                                                          */
/****************************************************************************************/
@far void IntFlt_F_DTC_Monitor(void)
{
	/* Local variable(s) */
	u_8Bit intFlt_active = FALSE;    // internal fault active indicator
	
	/* Check unused bits in internal faults */
	IntFlt_F_Chck_Unused_Bits();
	
	/* Check for active Internal Fault(s) */
	/* Check for any known internal faults from previous or current ignition cycles */
	if ( (intFlt_bytes[ZERO] == NO_INT_FLT) && (intFlt_bytes[ONE] == NO_INT_FLT) && (intFlt_bytes[TWO] == NO_INT_FLT) ) {
		/* No internal bits are set */
		intFlt_active = FALSE;
	}
	else {
		/* Set internal fault active */
		intFlt_active = TRUE;
	}
	
	/* DTC Management */
	/* Check for active internal fault */
	if (intFlt_active == TRUE) {
		/* Internal Fault DTC is active */
		FMemLibF_ReportDtc(DTC_HARDWARE_ERROR_K, ERROR_ON_K);
	}
	else {
		/* No intrenal error DTC */
		FMemLibF_ReportDtc(DTC_HARDWARE_ERROR_K, ERROR_OFF_K);
	}
	
	/* Check CAN communication (use this function to enable CAN communication if necasary) */
	IntFlt_F_Chck_CAN_Comm();
}

/****************************************************************************************/
/* Function Name: 	@far EEBlockLocation IntFltLF_Get_Location(u_8Bit sel_byte)    		*/
/* Type: 			Local Function														*/
/* Returns: 		EEBlockLocation	(EEBlock location of the internal fault byte        */
/* Parameters: 		u_8Bit sel_byte (selected internal fault byte)  					*/
/* Description:     This function gets the location of the internal fault byte.			*/
/*																						*/
/* Calls to: 		None																*/
/* Calls from:       																	*/
/* IntFlt_F_Update(u_8Bit Byte_number, u_8Bit Bit_mask,Int_Flt_Op_Type_e intFlt_Op)		*/
/* 																						*/
/****************************************************************************************/
@far EEBlockLocation IntFltLF_Get_Location(u_8Bit sel_byte)
{
	/* local variable(s) */
	u_8Bit fault_byte = sel_byte;
	EEBlockLocation my_location = (EEBlockLocation)0;
	
	/* Find the EEBlock location of the selected internal fault */
	switch(fault_byte)
	{
		case ZERO:
			my_location = EE_INTERNAL_FLT_BYTE0;
			break;
		case ONE:
			my_location = EE_INTERNAL_FLT_BYTE1;
			break;
		case TWO:
			my_location = EE_INTERNAL_FLT_BYTE2;
			break;
		case THREE:
			my_location = EE_INTERNAL_FLT_BYTE3;
			break;
		default: 
			break;
	}	
	/* Return the location of the selected internal fault in EEBlock */
	/* 0 (zero) is an invalid location */
	return(my_location);
}

/****************************************************************************************/
/*								IntFlt_F_Update        					    			*/
/****************************************************************************************/
/* This function is responsible to update the Shadow RAM with the new internal fault    */
/* information. It places a call to FlashF_UpdateEEPROM_Immediate function after        */
/* writing the new fault in order to reduce the amount of time it takes to update       */
/* non-volatile memeory.                                                                */
/* *** ATTENTION ***                                                                    */
/* We have to make sure that Shadow RAM is updated and its write is succesfull before   */
/* we can call FlashF_UpdateEEPROM_Immediate.                                           */
/* Calls from                                                                           */
/* --------------                                                                       */
/* BattV_Chck_ErraticV_Rdg :                                                            */
/*    IntFlt_F_Update(u_8Bit Byte_number,u_8Bit Bit_mask,Int_Flt_Op_Type_e intFlt_Op)   */
/* IntFlt_F_Chck_Osci_Err :                                                             */
/*    IntFlt_F_Update(u_8Bit Byte_number,u_8Bit Bit_mask,Int_Flt_Op_Type_e intFlt_Op)   */
/* IntFlt_F_Chck_PLL :                                                                  */
/*    IntFlt_F_Update(u_8Bit Byte_number,u_8Bit Bit_mask,Int_Flt_Op_Type_e intFlt_Op)   */
/* IntFlt_F_Chck_Ram_Err :                                                              */
/*    IntFlt_F_Update(u_8Bit Byte_number,u_8Bit Bit_mask,Int_Flt_Op_Type_e intFlt_Op)   */
/* IntFlt_F_Chck_Unused_Bits :                                                          */
/*    IntFlt_F_Update(u_8Bit Byte_number,u_8Bit Bit_mask,Int_Flt_Op_Type_e intFlt_Op)   */
/* IntFlt_F_Clr_Osci_Err :                                                              */
/*    IntFlt_F_Update(u_8Bit Byte_number,u_8Bit Bit_mask,Int_Flt_Op_Type_e intFlt_Op)   */
/* IntFlt_F_Clr_PLL_Err :                                                               */
/*    IntFlt_F_Update(u_8Bit Byte_number,u_8Bit Bit_mask,Int_Flt_Op_Type_e intFlt_Op)   */
/* IntFlt_F_Clr_Ram_Err :                                                               */
/*    IntFlt_F_Update(u_8Bit Byte_number,u_8Bit Bit_mask,Int_Flt_Op_Type_e intFlt_Op)   */
/* rlLF_Set_Relay_Err :                                                                 */
/*    IntFlt_F_Update(u_8Bit Byte_number,u_8Bit Bit_mask,Int_Flt_Op_Type_e intFlt_Op)   */
/* ROMTstF_Chck_Err :                                                                   */
/*    IntFlt_F_Update(u_8Bit Byte_number,u_8Bit Bit_mask,Int_Flt_Op_Type_e intFlt_Op)   */
/* TempF_Chck_NTC_SupplyV :                                                             */
/*    IntFlt_F_Update(u_8Bit Byte_number,u_8Bit Bit_mask,Int_Flt_Op_Type_e intFlt_Op)   */
/* TempF_Clr_PCB_OverTemp :                                                             */
/*    IntFlt_F_Update(u_8Bit Byte_number,u_8Bit Bit_mask,Int_Flt_Op_Type_e intFlt_Op)   */
/* TempF_Clr_PCB_Sensor_Flt :                                                           */
/*    IntFlt_F_Update(u_8Bit Byte_number,u_8Bit Bit_mask,Int_Flt_Op_Type_e intFlt_Op)   */
/* TempF_Set_PCB_OverTemp :                                                             */
/*    IntFlt_F_Update(u_8Bit Byte_number,u_8Bit Bit_mask,Int_Flt_Op_Type_e intFlt_Op)   */
/* TempF_Upt_PCB_Sensor_Err :                                                           */
/*    IntFlt_F_Update(u_8Bit Byte_number,u_8Bit Bit_mask,Int_Flt_Op_Type_e intFlt_Op)   */
/*                                                                                      */
/* Calls to                                                                             */
/* --------                                                                             */
/*                                                       								*/
/*                                                                            			*/
/****************************************************************************************/
@far void IntFlt_F_Update(u_8Bit Byte_number, u_8Bit Bit_mask, Int_Flt_Op_Type_e intFlt_Op)
{	
	/* Local variable(s) */
	u_8Bit intFlt_byte = Byte_number;    // Byte number
	u_8Bit intFlt_mask = Bit_mask;       // Bit mask
	u_8Bit intFlt_temp = 0;              // stores the selected internal byte
	EEBlockLocation flt_location = (EEBlockLocation) 0;	 // Internal Fault location in EE Block
	
	/* Check for valid byte number */
	if (intFlt_byte <= THREE) {
		
		/* Read the latest status of the internal fault bytes */ // Do we need this?
		IntFlt_F_Read_Fault_Bytes();
		
		/* Store the selected known internal fault byte into a temporary variable for manipulation */
		/* Do this, so if write is not succesfull we can re-try again.                             */
		/* We update the known internal fault byte once the write is succesfull!                   */
		intFlt_temp = intFlt_bytes[intFlt_byte];
		
		/* Check operation type */
		if (intFlt_Op == INT_FLT_SET) {
			/* Set the selected bit */
			intFlt_temp |= intFlt_mask;
		}
		else {
			/* Clear the selected bit */
			intFlt_temp &= intFlt_mask;
		}
		
		/* Find the internal fault location in EE Block */
		flt_location = IntFltLF_Get_Location(intFlt_byte);
		
		/* Check if fault location is valid */
		if(flt_location != FALSE){
			/* Write the updated internal fault byte */		
			EE_BlockWrite(flt_location, &intFlt_temp);
		}else{
			/* Invalid location. Do we want to do anything here? */
		}
	}
	else {
		/* Not a valid byte. We will not set an internal fault. */
	}
}
/****************************************************************************************/
/*								IntFlt_F_ClearOutputs  					    			*/
/****************************************************************************************/
/* This function turns off all outputs and clears the LED displays before disabling the */
/* CAN communication for high priority internal faults (oscillator, pll error etc).     */
/*                                                                                      */
/* Calls from                                                                           */
/* --------------                                                                       */
/* IntFlt_F_Chck_Osci_Err :                                                             */
/*    IntFlt_F_ClearOutputs()                                                           */
/* IntFlt_F_Chck_PLL :                                                                  */
/*    IntFlt_F_ClearOutputs()                                                           */
/* IntFlt_F_Chck_Ram_Err :                                                              */
/*    IntFlt_F_ClearOutputs()                                                           */
/* ROMTstF_Chck_Err :                                                                   */
/*    IntFlt_F_ClearOutputs()                                                           */
/*                                                                                      */
/* Calls to                                                                             */
/* --------                                                                             */
/* vsF_Clear(u_8Bit vs_pstn)                                                            */
/* vsF_LED_Display(u_8Bit vs_pstn)                                                      */
/* hsLF_ClearHeatSequence()                                                             */
/* stWF_LED_Display()                                                                   */
/* stWF_Clear()                                                                         */
/****************************************************************************************/
@far void IntFlt_F_ClearOutputs(void)
{
	/* Turn off all outputs and clear their variables */
	/* Check if we are supporting vents */
	if ((CSWM_config_c & CSWM_2VS_MASK_K) == CSWM_2VS_MASK_K)
	{
		/* Vents */
		vsF_Clear( (u_8Bit) VS_FL);
		vsF_LED_Display ( (u_8Bit) VS_FL);
		vsF_Clear( (u_8Bit) VS_FR);
		vsF_LED_Display ( (u_8Bit) VS_FR);
		vsF_Output_Cntrl();
	}
	else
	{
		/* Vents are not in the configuration */
	}
	
	/* Check if we are supporting stwheel */
	if ((CSWM_config_c & CSWM_HSW_MASK_K) == CSWM_HSW_MASK_K)
	{
		/* Steering Wheel */
		stWF_Clear();
		stWF_LED_Display();
		stWF_Output_Control();
	}
	else
	{
		/* StWheel is not in the configuration */
	}
	
	/* Heaters */
	hsLF_ClearHeatSequence();
}
/****************************************************************************************/
/*								IntFlt_F_Clr_PLL_Err  					    			*/
/****************************************************************************************/
/* This function is called when PLL status is locked (polling every 10ms).              */
/* It checks whether we need to clear the PLL error or not.                             */
/* With this function we can recovery from a PLL error within the same ignition cycle   */ 
/* (internal fault will be stored and the user can re-active the outputs). 				*/
/*                                                                                      */
/* Calls from                                                                           */
/* --------------                                                                       */
/* IntFlt_F_Chck_PLL :                                                                  */
/*    IntFlt_F_Clr_PLL_Err()                                                            */
/*                                                                                      */
/* Calls to                                                                             */
/* --------                                                                             */
/* EE_BlockRead(EEBlockLocation elocation, u_8Bit *pbydest)								*/
/* IntFlt_F_Update(u_8Bit Byte_number,u_8Bit Bit_mask,Int_Flt_Op_Type_e intFlt_Op)      */
/****************************************************************************************/
@far void IntFlt_F_Clr_PLL_Err(void)
{
	/* Read the PLL error byte */ // Do we need this?
	EE_BlockRead(EE_INTERNAL_FLT_BYTE0, (u_8Bit*)&intFlt_bytes[ZERO]);
	
	/* Check if we have to clear the PLL error */
	if ( (intFlt_bytes[ZERO] & PLL_ERR_MASK) == PLL_ERR_MASK) {
		/* Update known faults */
		IntFlt_F_Update(ZERO, CLR_PLL_ERR, INT_FLT_CLR);
	}
	else {
		/* PLL fault is not set. */
	}
	/* Clear internal Fault Flag */
	PLL_Flt_b = FALSE;
}
/****************************************************************************************/
/*								IntFlt_F_Chck_PLL  					    				*/
/****************************************************************************************/
/* This function checks the status of the Phase Lock Loop (PLL) cyclically.             */
/* If PLL is locked CSWM works under normal operation conditions.                       */
/* If PLL is unlocked, this function matures an internal fault, updates non-volatile    */
/* memory, and disables CAN communication and all outputs. 								*/
/*                                                                                      */
/* Calls from                                                                           */
/* --------------                                                                       */
/* main :                                                                               */
/*    IntFlt_F_Chck_PLL()                                                               */
/*                                                                                      */
/* Calls to                                                                             */
/* --------                                                                             */
/* IntFlt_F_Update(u_8Bit Byte_number,u_8Bit Bit_mask,Int_Flt_Op_Type_e intFlt_Op)      */
/* Mcu_PllStatusType Mcu_GetPllStatus()                                                 */
/* Dio_WriteChannel( ChannelId, Level)                                                  */
/* IntFlt_F_ClearOutputs()                                                              */
/* IntFlt_F_Clr_PLL_Err()                                                               */
/****************************************************************************************/
@far void IntFlt_F_Chck_PLL(void)
{	
	/* Local variable(s) */
	static u_8Bit PLL_CAN_disable_cnt = 0;        // CAN communication disable counter
	
	/* Check if high priority fault detection is enabled */
	if (intFlt_config_c == ENABLE_HIGH_PRIORITY_INTFLT_DET) {
		
		/* Check PLL status */
		PLL_status = Mcu_GetPllStatus();
		
		/* Is PLL locked? */
		if (PLL_status == MCU_PLL_LOCKED) {
			/* PLL is locked. Clear PLL unlock count */
			intFlt_PLL_unlock_cnt_c = 0;
			
			/* Clear CAN disable counter */
			PLL_CAN_disable_cnt = 0;
			
			/* Check if we have to Clear the PLL error */
			IntFlt_F_Clr_PLL_Err();
		}
		else {
			/* Check if PLL is unlocked for more than PLL unlock time limit */
			if ( (intFlt_PLL_unlock_cnt_c < intFlt_PLL_err_tm_lmt_c) && (PLL_Flt_b == FALSE) ) {
				
				/* Count PLL unlock time */
				intFlt_PLL_unlock_cnt_c++;
			}
			else {
				/* Check fault flag to decide if we need to clear all outputs */
				if (PLL_Flt_b == TRUE) {
					/* Outputs should be already cleared */
				}
				else {
					/* Clear all outputs */
					IntFlt_F_ClearOutputs();
					
					/* Set internal fault flag */
					PLL_Flt_b = TRUE;
				}				
				/* Read the PLL error byte */ // Do we need this?
				EE_BlockRead(EE_INTERNAL_FLT_BYTE0, (u_8Bit*)&intFlt_bytes[ZERO]);
				
				/* Check if we already updated the internal fault */
				if ( (intFlt_bytes[ZERO] & PLL_ERR_MASK) == PLL_ERR_MASK) {
					/* Check if CAN communication disable delay has been expired */
					/* *** Delay to wait output LEDs turn off ***                */
					if (PLL_CAN_disable_cnt < INTFLT_CAN_DISABLE_TM) {
						/* Increment the delay counter */
						PLL_CAN_disable_cnt++;
					}
					else {
						/* Disable CAN communication (by pulling standby pin high) */
						Dio_WriteChannel(CAN_STB, STD_HIGH);
					}
				}
				else {
					/* Update known faults */
					IntFlt_F_Update(ZERO, PLL_ERR_MASK, INT_FLT_SET);
				}
			}
		}
	}
	else {
		/* High Priority internal fault (fault that disable CAN communication) detection is disabled */
	}
}
/****************************************************************************************/
/*								IntFlt_F_Set_Osci_Err  					    			*/
/****************************************************************************************/
/* Whenever oscillator is not functining properly, micro-controller is suppose to enter */ 
/* into a self-clock mode.                                                    			*/
/* This function is called from an interrupt service routine, Mcu_SelfClockMode_Isr.	*/
/* It tries to set an internal fault, disable CAN communication and all outputs. 		*/
/* It checks whether we are entering or exiting the self clock mode.                    */
/* Important note: It may take significantly longer time to update internal faults in   */
/* this mode. 				                                                            */
/*                                                                                      */
/* Calls from                                                                           */
/* --------------                                                                       */
/* Mcu_SelfClockMode_Isr :                                                              */
/*    IntFlt_F_Set_Osci_Err()                                                           */
/****************************************************************************************/
@far void IntFlt_F_Set_Osci_Err(void)
{	
	/* IMPORTANT NOTE: The CRGV4 generates a self-clock mode interrupt when the SCM  */
	/* condition of the system has changed, either entered or exited self-clock mode.*/
	/* (Initial mode is: Normal) */
	/* ATTENTION: This function is called from an Interrupt Service Routine (Mcu_SelfClockMode_Isr)*/

	/* Check if we are entering or exiting the self clock mode */
	if (intFlt_self_clck_mode == INTFLT_NORMAL_CLK_MODE) {
		/* Entering: Set the self clock mode indicator */
		intFlt_self_clck_mode = INTFLT_SELF_CLK_MODE;
	}
	else {
		/* Exciting: Set the normal clock mode indicator */
		intFlt_self_clck_mode = INTFLT_NORMAL_CLK_MODE;
	}

}
/****************************************************************************************/
/*								IntFlt_F_Clr_Osci_Err  					    			*/
/****************************************************************************************/
/* This function checks the clock mode (self clcok mode or normal clock mode).          */
/* If we are in normal clock mode, it checks whether we need to clear the oscillator    */
/* error or not. 																		*/
/*                                                                                      */
/* Calls from                                                                           */
/* --------------                                                                       */
/* IntFlt_F_Chck_Osci_Err :                                                             */
/*    IntFlt_F_Clr_Osci_Err()                                                           */
/*                                                                                      */
/* Calls to                                                                             */
/* --------                                                                             */
/* IntFlt_F_Update(u_8Bit Byte_number,u_8Bit Bit_mask,Int_Flt_Op_Type_e intFlt_Op)      */
/****************************************************************************************/
@far void IntFlt_F_Clr_Osci_Err(void)
{	
	/* Check if we are in normal clock mode */
	if (intFlt_self_clck_mode == INTFLT_NORMAL_CLK_MODE) {
		
		/* Read the Oscillator error byte */ // Do we need this?
		EE_BlockRead(EE_INTERNAL_FLT_BYTE0, (u_8Bit*)&intFlt_bytes[ZERO]);

		/* Check if we have to clear the Oscillator error */
		if ( (intFlt_bytes[ZERO] & CRYSTAL_OSCILLATOR_MASK) == CRYSTAL_OSCILLATOR_MASK) {
			/* Update known faults */
			IntFlt_F_Update(ZERO, CLR_OSC_ERR, INT_FLT_CLR);
		}
		else {
			/* Oscillator error is not set. Normal Operation */
		}
		/* Clear internal fault flag */
		Crystal_Osci_Flt_b = FALSE;
	}
	else {
		/* Invalid case. */
	}
}
/****************************************************************************************/
/*								IntFlt_F_Chck_Osci_Err  					    		*/
/****************************************************************************************/
/* This function is called cyclically (every 5ms).                                 		*/
/*                                                                                 		*/
/* If we are in self clock mode:                                                   		*/
/* 1. Disables CAN communication                                                   		*/
/* 2. Turn off all outputs                                                         		*/
/* 3. Matures an internal fault (oscillator error)                                 		*/
/*                                                                                 		*/
/* If we are in normal clock mode:                                                 		*/
/* 1. Makes sure that oscillator error is not set.                                 		*/
/*                                                                                 		*/
/* Calls from                                                                      		*/
/* --------------                                                                  		*/
/* main :                                                                          		*/
/*    IntFlt_F_Chck_Osci_Err()                                                     		*/
/*                                                                                 		*/
/* Calls to                                                                        		*/
/* --------                                                                        		*/
/* IntFlt_F_ClearOutputs()                                                         		*/
/* Dio_WriteChannel( ChannelId, Level)                                             		*/
/* IntFlt_F_Update(u_8Bit Byte_number,u_8Bit Bit_mask,Int_Flt_Op_Type_e intFlt_Op) 		*/
/* IntFlt_F_Clr_Osci_Err()                                                         		*/
/****************************************************************************************/
@far void IntFlt_F_Chck_Osci_Err(void)
{	
	/* Check if high priority fault detection is enabled */
	if (intFlt_config_c == ENABLE_HIGH_PRIORITY_INTFLT_DET) {
		/* Are we in self clock mode? */
		if (intFlt_self_clck_mode == INTFLT_SELF_CLK_MODE) {
			/* Disable CAN communication (by pulling standby pin high) */
			Dio_WriteChannel(CAN_STB, STD_HIGH);
			
			/* Check fault flag to decide if we need to clear all outputs */
			if (Crystal_Osci_Flt_b == TRUE) {
				/* Outputs should be already cleared */
			}
			else {
				/* Clear all outputs */
				IntFlt_F_ClearOutputs();

				/* Set internal fault flag */
				Crystal_Osci_Flt_b = TRUE;
			}
			/* Read the Oscillator error byte */ // Do we need this?
			EE_BlockRead(EE_INTERNAL_FLT_BYTE0, (u_8Bit*)&intFlt_bytes[ZERO]);
			
			/* Check if we already updated the oscillator fault */
			if ( (intFlt_bytes[ZERO] & CRYSTAL_OSCILLATOR_MASK) == CRYSTAL_OSCILLATOR_MASK) {
				/* Already updated internal fault */
			}
			else {
				/* Update known faults */
				IntFlt_F_Update(ZERO, CRYSTAL_OSCILLATOR_MASK, INT_FLT_SET);
			}
		}
		else {
			/* Check if we have to recover from Oscillator error */
			IntFlt_F_Clr_Osci_Err();
		}
	}
	else {
		/* High Priority internal fault (fault that disable CAN communication) detection is disabled */
	}
}
/****************************************************************************************/
/*								IntFlt_F_Clr_Ram_Err  					    	    	*/
/****************************************************************************************/
/* This function makes sure that RAM error is not set when RAM test result is o.k. 		*/
/*                                                                                 		*/
/* Calls from                                                                      		*/
/* --------------                                                                  		*/
/* main :                                                                          		*/
/*    IntFlt_F_Clr_Ram_Err()                                                       		*/
/*                                                                                 		*/
/* Calls to                                                                       		*/
/* --------                                                                        		*/
/* IntFlt_F_Update(u_8Bit Byte_number,u_8Bit Bit_mask,Int_Flt_Op_Type_e intFlt_Op)		*/
/****************************************************************************************/
@far void IntFlt_F_Clr_Ram_Err(void)
{
	/* Read the Ram error byte */ // Do we need this?
	EE_BlockRead(EE_INTERNAL_FLT_BYTE0, (u_8Bit*)&intFlt_bytes[ZERO]);
	
	/* Check if we have to clear the RAM error */
	if ( ( (intFlt_bytes[ZERO] & RAM_ERR_MASK) == RAM_ERR_MASK) && (main_RAM_test_rslt == RAMTST_RESULT_OK) ){
		/* Update known faults */
		IntFlt_F_Update(ZERO, CLR_RAM_ERR, INT_FLT_CLR);
		
		/* Ram Test is o.k. */
		Ram_err_b = FALSE;
	}
	else {
		/* No update is necassary. */
	}
}
/****************************************************************************************/
/*								IntFlt_F_Chck_Ram_Err  					    	    	*/
/****************************************************************************************/
/* This function is called when RAM test finishes its check.                            */
/* If there are no damaged RAM cells (RAM test result is o.k.), then no internal RAM    */
/* fault is set.                         												*/
/* If the RAM test result is not o.k., this function sets the internal fault, disables  */
/* CAN communication, and all outputs. 													*/
/*                                                                                      */
/* Calls from                                                                           */
/* --------------                                                                       */
/* main :                                                                               */
/*    IntFlt_F_Chck_Ram_Err()                                                           */
/*                                                                                      */
/* Calls to                                                                             */
/* --------                                                                             */
/* IntFlt_F_Update(u_8Bit Byte_number,u_8Bit Bit_mask,Int_Flt_Op_Type_e intFlt_Op)      */
/* Dio_WriteChannel( ChannelId, Level)                                                  */
/* mainF_Finish_Mcu_Test(main_mcu_test_type_e test_type)                                */
/* IntFlt_F_ClearOutputs()                                                              */
/****************************************************************************************/
@far void IntFlt_F_Chck_Ram_Err(void)
{	
	/* Local variable(s) */
	static u_8Bit Ram_CAN_disable_cnt = 0;        // CAN communication disable counter
	
	/* Check if high priority fault detection is enabled */
	if (intFlt_config_c == ENABLE_HIGH_PRIORITY_INTFLT_DET) {
		
		/* Check RAM test result */
		if (main_RAM_test_rslt == RAMTST_RESULT_OK) {
			/* Clear CAN disable counter */
			Ram_CAN_disable_cnt = 0;			
			/* Ram Test is o.k. */
			Ram_err_b = FALSE;			
			/* RAM is checked */
			mainF_Finish_Mcu_Test(MAIN_RAM_TEST);
		}
		else {
			/* Check fault flag to decide if we need to clear all outputs */
			if (Ram_err_b == TRUE) {
				/* Outputs should be already cleared */
			}
			else {
				/* Clear outputs (Turn off outputs and LED display) */
				IntFlt_F_ClearOutputs();				
				/* Set internal fault flag */
				Ram_err_b = TRUE;
			}
			/* Read the Ram error byte */ // Do we need this?
			EE_BlockRead(EE_INTERNAL_FLT_BYTE0, (u_8Bit*)&intFlt_bytes[ZERO]);
			
			/* Check if we already updated the Ram error */
			if ( (intFlt_bytes[ZERO] & RAM_ERR_MASK) == RAM_ERR_MASK) {
				/* Check if CAN communication disable delay has been expired */
				/* *** Delay to wait output LEDs turn off ***                */
				if (Ram_CAN_disable_cnt < INTFLT_CAN_DISABLE_TM) {
					/* Increment the delay counter */
					Ram_CAN_disable_cnt++;
				}
				else {
					/* Disable CAN communication (by pulling standby pin high) */
					Dio_WriteChannel(CAN_STB, STD_HIGH);					
					/* RAM is checked */
					mainF_Finish_Mcu_Test(MAIN_RAM_TEST);
				}
			}
			else {
				/* Update known faults */
				IntFlt_F_Update(ZERO, RAM_ERR_MASK, INT_FLT_SET);
			}
		}
	}
	else {
		/* High Priority internal fault (fault that disable CAN communication) detection is disabled */
		mainF_Finish_Mcu_Test(MAIN_RAM_TEST);
	}
}

