#ifndef RELAY_H
#define RELAY_H

/********************************************************************************/
/*               A U T H O R   I D E N T I T Y									*/
/********************************************************************************/
/* Initials      Name                   Company									*/
/* --------      --------------------   --------------------------------------- */ 
/* HY            Harsha Yekollu         Continental Automotive Systems          */
/* CK            Can Kulduk             Continental Automotive Systems			*/
/********************************************************************************/

/*********************************************************************************/
/*                   CHANGE HISTORY for RELAY MODULE                             */
/*********************************************************************************/
/* 03.04.2008/Harsha - New file added                                            */
/* 09.05.2008 CK     - Added a new define for steering wheel fault detection     */
/* 10.22.2008 CK     - Modified relay_fault_status_c for new FET fault routines  */
/* 01.23.2009 CK     - PC-Lint related updates.                                  */
/* 04.16.2010 HY     - MKS 50111                                                 */
/*                     Moved 6 Individual Relay Thresholds to into 1 Block       */
/*                     New enum created to support the above                     */ 
/*********************************************************************************/

/* EXTERN-Definition*/
#undef EXTERN 

#ifdef RELAY_C   

#define EXTERN 

#else

#define EXTERN extern

#endif

/************************** @Global variables and Defines*************************/
EXTERN union char_bit relay_status_c;
#define base_relay_active_for_front_seats_b       relay_status_c.b.b0 
/*1 = request to turn ON Base Relay for Front Seats */
/*0 = request to turn OFF Base Relay from Front Seats */ 
#define base_relay_active_for_rear_seats_b        relay_status_c.b.b1 
/*1 = request to turn ON Base Relay for Rear Seats */
/*0 = request to turn OFF Base Relay from Rear Seats */
#define base_relay_100_pwm_b                      relay_status_c.b.b2 
/*1 = Base Relay with 100% PWM*/
/*0 = Base Relay either with variable PWM or not turned ON */
#define relay_A_enabled_b                         relay_status_c.b.b3 
/*1 = Relay A Enabled */
/*0 = Relay A disabled */
#define relay_B_enabled_b                         relay_status_c.b.b4 
/*1 = Relay B Enabled */
/*0 = Relay B disabled */
#define relay_pwm_is_active_b                     relay_status_c.b.b5 
/*1 = Base Relay with PWM is active*/
/*0 = Base Relay is not active */
#define relay_A_Ok_b                              relay_status_c.b.b6 
/*1 = Relay A is OK. */
/*0 = Relay A is Not OK */ 
#define relay_B_Ok_b                              relay_status_c.b.b7 
/*1 = Relay B is OK. */
/*0 = Relay B is Not OK */

EXTERN union char_bit relay_status2_c;
#define base_relay_active_for_steering_wheel_b    relay_status2_c.b.b0 
/*1 = request to turn ON Base Relay for Steering Wheel */
/*0 = request to turn OFF Base Relay from Steering Wheel */ 
#define relay_C_enabled_b                         relay_status2_c.b.b1 
/*1 = Relay C Enabled */
/*0 = Relay C disabled */
#define relay_C_Ok_b                              relay_status2_c.b.b2 
/*1 = Relay C is OK. */
/*0 = Relay C is Not OK */ 
#define relay_C_phase1_ok_b                       relay_status2_c.b.b3
/* 1 = Relay C Passed First Phase fault diagnsotics */
/* 0 = Relay C failed first phase fault diagnsotics */
#define relay_C_phase2_ok_b                       relay_status2_c.b.b4
/* 1 = Relay C Passed Second Phase fault diagnsotics */
/* 0 = Relay C failed Second phase fault diagnsotics */
#define relay_C_phase1_Test_Voltage_ON_fault_b             relay_status2_c.b.b5
/* 1 = Relay C failed Test Voltage fault diagnsotics */
/* 0 = Relay C passed Test Voltage fault diagnsotics */
#define relay_C_phase1_Test_Voltage_OFF_fault_b             relay_status2_c.b.b6
/* 1 = Relay C failed Short to BAT fault diagnsotics */
/* 0 = Relay C passed Short to BAT fault diagnsotics */
#define relay_C_low_side_fault_b                  relay_status2_c.b.b7
/* 1 = Low Side fault set                            */
/* 0 = Low Side fault Clear                          */

EXTERN union char_bit relay_fault_status2_c;
#define relay_C_internal_Fault_b                  relay_fault_status2_c.b.b0
/* 1 = Relay C is Bad and there is Internal Fault */
/* 0 = Relay C is OK and no Internal fault        */
#define relay_C_stuck_HI_b                       relay_fault_status2_c.b.b1		// Not Used 04.06.2009
/* 1: When Struck Relay HIGH happens for A */
/* 0: Normal */
#define relay_C_stuck_LOW_b                      relay_fault_status2_c.b.b2
/* 1: When Struck Relay LOW happens for A */
/* 0: Normal */
#define relay_C_short_at_FET_b                    relay_fault_status2_c.b.b3	// Not Used 04.06.2009
/* 1 = Steering Wheel Short to Bat condition detected early at Relay module */
/* 0 = Normal */
#define Base_relay_always_100P_PWM_b              relay_fault_status2_c.b.b4
/* 1 = Base Relay will be with 100% Always untill heat is OFF */
/* 0 = Base relay will be with 00% only for 1sec and then PWMs normally */
#define Relay_A_Test_b                            relay_fault_status2_c.b.b5	// Not Used 04.06.2009
/* 1 = Realy A Test under progress                             */
/* 0 = Realy A Test done                                       */
#define Relay_B_Test_b                            relay_fault_status2_c.b.b6	// Not Used 04.06.2009
/* 1 = Realy B Test under progress                             */
/* 0 = Realy B Test done                                       */
#define Relay_C_Test_b                            relay_fault_status2_c.b.b7
/* 1 = Realy C Test under progress                             */
/* 0 = Realy C Test done                                       */

EXTERN union char_bit relay_fault_status_c;
#define relay_A_stuck_HI_b                       relay_fault_status_c.b.b0
/* 1: When Struck Relay HIGH happens for A */
/* 0: Normal */
#define relay_A_stuck_LOW_b                      relay_fault_status_c.b.b1
/* 1: When Struck Relay LOW happens for A */
/* 0: Normal */
#define relay_B_stuck_HI_b                       relay_fault_status_c.b.b2
/* 1: When Struck Relay HIGH happens for A */
/* 0: Normal */
#define relay_B_stuck_LOW_b                      relay_fault_status_c.b.b3
/* 1: When Struck Relay LOW happens for A */
/* 0: Normal */
#define relay_F_FET_fault_b						 relay_fault_status_c.b.b4
/* 1: there is an internal FET problem on Front FET */
/* 0: Normal */
#define relay_R_FET_fault_b						 relay_fault_status_c.b.b5
/* 1: there is an internal FET problem on Rear FET */
/* 0: Normal */

/* Original */
//#define relay_FL_FET_fault_b                      relay_fault_status_c.b.b4
///* 1: there is an internal FET problem for FL seat */
///* 0: Normal */
//#define relay_FR_FET_fault_b                      relay_fault_status_c.b.b5
///* 1: there is an internal FET problem for FR seat */
///* 0: Normal */
//#define relay_RL_FET_fault_b                      relay_fault_status_c.b.b6
///* 1: there is an internal FET problem for RL seat */
///* 0: Normal */
//#define relay_RR_FET_fault_b                      relay_fault_status_c.b.b7
///* 1: there is an internal FET problem for RR seat */
///* 0: Normal */

EXTERN union char_bit relay_control_c;
#define relay_A_phase1_ok_b                          relay_control_c.b.b0
/* 1 = Relay A Passed First Phase fault diagnsotics */
/* 0 = Relay A failed first phase fault diagnsotics */
#define relay_B_phase1_ok_b                          relay_control_c.b.b1
/* 1 = Relay B Passed First Phase fault diagnsotics */
/* 0 = Relay B failed first phase fault diagnsotics */
#define relay_A_phase2_ok_b                          relay_control_c.b.b2
/* 1 = Relay A Passed Second Phase fault diagnsotics */
/* 0 = Relay A failed Second phase fault diagnsotics */
#define relay_B_phase2_ok_b                          relay_control_c.b.b3
/* 1 = Relay B Passed First Phase fault diagnsotics */
/* 0 = Relay B failed first phase fault diagnsotics */
#define relay_A_internal_Fault_b                  relay_control_c.b.b4
/* 1 = Relay A is Bad and there is Internal Fault */
/* 0 = Relay A is OK and no Internal fault        */ 
#define relay_B_internal_Fault_b                  relay_control_c.b.b5
/* 1 = Relay B is Bad and there is Internal Fault */
/* 0 = Relay B is OK and no Internal fault        */
#define relay_A_short_at_FET_b                    relay_control_c.b.b6
/* 1 = FL or FR individual Short to Bat condition detected early at Relay module */
/* 0 = Normal */
#define relay_B_short_at_FET_b                    relay_control_c.b.b7
/* 1 = RL or RR individual Short to Bat condition detected early at Relay module */
/* 0 = Normal */ 

EXTERN union char_bit relay_control2_c;
#define Rear_Seats_no_power_prelim_b              relay_control2_c.b.b0
#define Rear_Seats_no_power_final_b               relay_control2_c.b.b1
/* 1 = Relay B with No Power             */
/* 0 = Relay B with Power                */
#define relay_B_no_power_b                        relay_control2_c.b.b2		// Not Used 04.02.2009
/* 1 = Relay C with No Power             */
/* 0 = Relay C with Power                */
#define relay_C_no_power_b                        relay_control2_c.b.b3		// Not Used 04.02.2009
#define relay_C_keep_U12S_ON_b					  relay_control2_c.b.b4	

EXTERN unsigned char relay_calibs_c [6]; //MKS 50111 to support one eep block
/*********************************************************************************/


/******************************* @Enumaration(s) ********************************/
///* Enums for Relay Index */
//enum {
// RELAY_A
//,RELAY_B
//,RELAY_C_HI
//,CONTROL_BASE_RELAY
///*,RELAY_FAULTS*/
//,BASE_RELAY_OFF
//};

/* Enums for Relay Calib parameters MKS 50111 */
enum {
	RL_UBAT_CUTOFF
	,RL_PHASE1_DELAY
	,RL_PHASE2_DELAY
	,RL_HSW_STB_DELAY
	,RL_HSW_STUCK_OPEN
	,RL_HSW_LOSIDE_OFF_DELAY
};
/*********************************************************************************/

/****************************** @Function Prototypes *******************************/
@far void rlF_InitRelay(void);
/* Basic Routine for Relay module to turn ON/OFF the relays. */
@far void rlF_RelayManager(void);
/* This routine will be responsible for detecting any faulty scenario with the relay. */
@far void rlF_RelayDiagnostics(void);
/* If there are any final Flags set, this routine will set the DTCs in active state, else if final flags were cleared once setting, changes the status of DTC from active to stored. */
@far void rlF_RelayDTC(void);
/* This function gets called when ever production commands gets executed for the Relay Module                                      */
/* This function individually turns ON the BAse Relay and other relay ports for testing purpose with out giving the switch request */
//@far void rlF_Production_Support(unsigned char relay_c, unsigned char action_c); NOT USED
/*********************************************************************************/


#endif