#define HEATING_C

/****************************************************************************************/
/*                   CHANGE HISTORY for HEATING MODULE                           		*/
/****************************************************************************************/
/* 03.24.2008 Harsha 	- 	Module created                                            	*/
/* 04.11.2008 Harsha 	- 	hsF_Manager is splitted into the following functions      	*/
/*                     		hsLF_Catch_Signals                                        	*/
/*                     		hsLF_Catch_Rear_Switch_Requests                           	*/
/*                     		hsLF_Process_DiagIO_Outputs                               	*/
/*                     		hsLF_Process_DiagIO_LEDStatus                             	*/
/*                     		hsLF_Process_Swtich_Requests_Bus                          	*/
/*                     		hsLF_Process_Swtich_Requests_ECU                          	*/
/* 04.11.2008 Harsha 	-	Modified hsLF_UpdateParameters to adjust Rear switch logic	*/
/* 04.11.2008 Harsha 	-	Added new function hsLF_SendRearSeat_HWStatus             	*/
/* 04.14.2008 Harsha 	-	Tested CCN Switch Logic										*/
/* 04.15.2008 Harsha 	-	Tested HVAC Switch Logic                            		*/
/* 04.15.2008 Harsha 	-	Tested Rear Switch Logic. HW needs to be changed   			*/
/*                   	-	Switch detections lined to U12S.                    		*/
/* 06.18.2008 Francisco	-	HEAT_LEVEL_ changed											*/
/*							HS_RMT_START_												*/
/* 06.19.2008 Francisco	-	hsLF_Vsense_AD_Readings modified							*/
/* 06.20.2008 Francisco	-	VEH_SWITCH_REAR_ONLY_K condition added in hsLF_Catch_Signals*/
/* 08.25.2008 Harsha    -   Added EE_BlockRead call to read default paramters @init     */
/*                      -   Added EE_BlockWrite call to write PWM/Timeout               */ 
/*                      -   Added EE_DirectWrite call to general writes(Not working)    */
/* 10.10.2008 Harsha    -   SNA Signal Reaction added to the Heater switch requests     */
/*							Modified "hsLF_Process_Switch_Requests_Bus" to react on SNA */
/*                          More info look into MKS Entry 15560 						*/
/* 10.28.2008 Harsha    -   Modified "hsF_HeatRequestToFet" to allow setDuty cycle function */
/*                          to execute only incase PWM is active                        */
/*                      -   Modifed "hsF_CheckDiagnoseParameter" and                    */
/*                          "hsF_WriteData_to_ShadowEEPROM" to serve new diag writes    */
/*                      -   Added new function "hsLF_ConvertDegToAD" for diag writes    */
/*                      -   Added new function "hsF_RearSwitchStuckDTC" to detect stuck dtc */
/* 11.17.2008 Harsha    -   Added PowerNet signal access to the following funcitons      */
/*							hsLF_SendHeatedSeatCanStatus and hsLF_GetHEatedSeatCanStatus */
/* 12.02.2008 Harsha    -   Added new Loadshed strategy for PowerNet series				 */
/*                          hsLF_Process_Switch_Requests_Bus							 */
/*							hsLF_RemoteStart_Control									 */
/*							hsLF_2secLEDDisplay	and										 */
/*							hsLF_2SecMonitorTime_FaultDetection functions modified       */
/* 12.08.2008 CK        -   Updated hsF_GetActv_OutputNumber function. Battery voltage   */
/*                          compensation is performed only when an output is turned on   */
/*                          at 100% PWM (or highest level HL3).                          */
/* 01.21.2009 Harsha    -   hsF_HeatManager updated for Rear Seat Switch request conditions */
/*                          Before accepting switch requests it looks for eeprom stored data */
/*                          If Fault is active in EEPROM, logic does not allow further    */
/*                          switch requests.                                              */
/*                          New defines added REAR_LEFT_STUCK_INDEX, REAR_RIGHT_STUCK_INDEX */
/*                          added to achieve the above information                        */
/*                      -   hsLF_ControlFET updated                                       */
/*                          Autosar PWM gets updated according to the duty cycle          */
/*                          with the modification, stagger manager will get updated with  */
/*                          correct information                                           */
/*                      -   hsF_RearSwitchStuckDTC function modified                      */
/* 01-30-2009 Harsha    -   MKS 23752: Updated hsLF_UpdateHeatParameters () function      */
/*                          Second time Switch request for 100%, will check NTC feedbacks */
/*                          from the moment heaters turns ON                              */
/* 02-16-2009 Harsha    -   hsLF_Process_DiagIO_Outputs function name changed to          */
/*                          hsLF_Process_DiagIO_RC_Outputs as the same function looks for */
/*                          Heaters IO Controlling and Self tests routine control         */ 
/* 02-18-2009 CK        -   New values of rear seat switch stuck DTC index defines.       */
/*                          Index is updated due to the deletion of not supported STW     */
/*                          partial open.												  */
/* 04.09.2009 CK        -   MKS change Request ID: 28078. New Isense caulculation for     */
/*                          heated seat and STW. New EOL read/write service to update     */
/*                          Iref_seatH and Vref_seatH parameters which are stored in      */
/*                          emulated EEPROM (after EE_INTERROG_RECORD).Updated 			  */
/*                          hsF_CheckDiagnoseParameter and hsF_WriteData_to_ShadowEEPROM  */
/*                          functions with HS_IREF_VREF case.                             */
/* 04.14.2009 Harsha    -   hsLF_Catch_Rear_Switch_Requests function updated to have      */
/*                          rear seat switch pressed state                                */
/* 04.16.2009 Harsha    -   hsLF_UpdateHeatParameters function updated for Heat Level 3   */
/*                          MKS 23752 modifications deleted because of spec change        */
/*                          NTC >55c check will be done from beginning of heat cycle now  */
/*                      -   hsLF_2SecLEDDisplay () function modified to adjust MKS 27984  */
/*                          Issue when any relay faults present, main cswm status is bad  */
/*                          Module behaving the main cswm status bad condition not the    */
/*                          fault behavior                                                */
/*                      -   REAR_LEFT_STUCK_INDEX and REAR_RIGHT_STUCK_INDEX values changes */
/*                          because of DTC placement in DTC module                        */
/*                      -   hsLF_2SecLEDDisplay () function modified to adjust MKS 22332  */
/*                          Issue is Heated Seats are reducing load level to LL1 due to   */
/*                          Load Shed 2 during Diagnostics IO Control.                    */
/* 04.28.2009 Harsha    -   hsF_RearSeat_TurnOFF() function added for service parts       */
/* 05.12.2009 Harsha    -   MKS 28818 update in hsF_HeatManager function.                 */
/*                          commented calling of hsLF_UpdateHeatParameters with OFF, in   */
/*                          function hsF_HeatManager to allow the outputs to stay ON      */
/*                          ever returning from Remote start                              */
/* 06.19.2009 Harsha    -   hsLF_Process_DiagIO_RC_Outputs function updated to have       */
/*                          Diag IO control for HL3 to get selected any time in same IGN  */
/*                          cycle. Earlier It was allowed to get selected only once per   */
/*                          IGN cycle.                                                    */
/*                      -   hsLF_Catch_Signals function updated. While returning control  */
/*                          from IO controlling, If rear seats are equipped and Hardwired */
/*                          seat request variable is intialized with default values       */
/* 08-21-2009 Christian -   hsLF_HeatDiagnostics(void) Add different thresholds to be     */ 
/*							different with Front and Rear seats Change Request ID 32456   */
/* 09.18.2009 Christian    -   hsF_HeatManager(void).	MkS 35407 ENG RPM IGN is in RUN   */
/*                               and ENG RPM is not running customer requested to allow   */
/*                               switch requests to display 2sec LED 					  */
/* 10.28.2009 Christian -  hsLF_Catch_Signals()	FTR00139665 - New vehicle Line RT- RM     */
/*                       when the new vehicle lines are on the bus, the switch requests   */ 
/*                       for rear heaters come from  new signals						  */										  
/* 11.25.2009 Christian - hsLF_Enable_RearHeaters() was added and hsLF_RemoteStart Control*/
/*                       was modified FTR00139669 - Autostart feature for PowerNet Series.*/
/* 11.30.2009 Christian - commented hsLF_Enable_RearHeaters () in hsLF_RemoteStart Control */
/*                        after discussing with Harsha, as it is repeated code, instead added */
/* 						  hsLF_Process_Switch_Requests_ECU () at same location             */                       
/* 01.20.2010 Christian	- hsF_HeatManager ()  - MKS 44686  - When Remote start is          */ 
/*                         activated, in the first 4.5sec, Rear Heated seats are operated. */
/*                          According to the spec, while Remst is active, Rear seats to be */
/*							disabled												       */
/* 02-17-2010 Harsha    - Modiifed hsF_HeatManager() to fulfil new requirement             */
/*		                  MKS 47354: When IGN status remains in RUN and Ign Run Remote     */
/*                        Start turns Inactive, Heat seat will start back from 100% PWM    */
/* 02-18-2010 Harsha    - Decision changed by CHRYSLER. No need for the above logic        */                         
/*                        Commented the above logic                                        */
/* 04-16-2010 Harsha    - MKS 50039:                                                       */
/*                        hsF_Init modified to get the current vehicle line calibs         */
/*                        hsF_CheckDiagnoseParameter updated                               */
/*                        hsLF_ConvertDegToAD updated with new logic                       */
/*                      - MKS 50111:                                                       */
/*                        hsF_init updated with EE_BlockRead to read flt calibs            */
/*                      - MKS 50168:                                                       */
/*                        hsF_HeatManager updated to get NTC Failsafe values for diag read */
/* 04-28-2010 Harsha    - MKS 50738, New loop counter introduced                           */
/*                        Wait times of Partial OPEN increased from 2sec to 2min           */
/*                        Instead change the 2sec variable in EEPROM, another loop counter */
/*                        hs_monitor_loop_counter_c introduced to reach the increased time */
/*                        Modifed function is hsLF_2SecMonitorTime_FaultDetection          */
/* 05-28-2010 Harsha    - MKS 51868 - hsLF_Catch_Signals updated to not have faulta at EOL */
/*                        for front Seats                                                  */
/* 06-11-2010 Harsha    - MKS 52375 - hsF_Init function got updated to have modifed constants */
/*                        for Rear Seat Stuck Switch times                                  */
/*                      - MKS 52379 - hsLF_HeatDiagnostics updated.                         */
/*                        When Partial OPEN detection kicks, IF logic is in HL3/HL2 move    */
/*                        Heat Level to HL1                                                 */
/*                        Updated hsF_MonitorHeatTimes by taking out the Partial OPEN prel  */
/*                        fault check before entering stepdown sequence                     */
/* 06-18-2010 Harsha    - MKS 52700: Issue fixed. When Rear stuck present and gives POR     */
/*                        outputs getting activated. (Happening for Power Net Only)         */
/*                        Modified hsLF_RemoteStart_Control function. For powernet          */
/*                        First time it goes to check for Autostart and inthat logic added  */
/*                        DTC Checking logic. Now we will not turn ON the outputs when      */
/*                        stuck DTC is present.                                             */
/* 07-21-2010 Harsha    - MKS 54427: NTC Cut OFF parameters write service updated           */
/*                        hsLF_ConvertDegToAD function expecting degC, and then in function */
/*                        call converting degC to degF. But the CDA tool is already sending */
/*                        converted degF value as input. Taken out the extra conversion in  */
/*                        hsLF_ConvertDegToAD function call                                 */
/* 07-28-2010 Harsha    - MKS 54495: Rear Seat LED status check enabled for EOL services    */
/*                        This change is required because of the default vehicle line change*/
/*                        from WK to DS for TIPM architecture                               */
/*                        SW already have special EOL start flag. Used this flag to enable  */
/*                        LED status for the rear seats                                     */
/*                        Functions updated are hsF_HeatManager and hsLF_UpdateHeatParameters*/
/* 08-02-2010 Harsha    - MKS 54709: When Self tests or Diag IO is active, Partial OPEN DTC  */
/*                        mature time changed to 2sec from 2min. For regular switch requests */
/*                        mature time stays at 2min.                                         */
/*                        Updated function hsLF_2SecMonitorTime_FaultDetection               */
/* 09-29-2010 Harsha    - PWR_NET #ifdef removed. Logic to have Power Net information        */
/******************************************************************************************/
/******************************************************************************************
 * 06-15-2011	Harsha	 75322	Cleanup the code. Delete the unused code for this program.
 * 								hsLF_Process_Switch_Requests_Bus updated with removal of Loadshed signals.
 * 								hsLF_Catch_Signals updated.
 * 
 * 08-09-2011	Harsha	 81838	PROXI Implementation. Checking data from PROXI eeprom CFG updated
 * 								in hsF_Init function.	
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 ******************************************************************************************/


/* SD Text                                                                  */
/* -------                                                                  */
/* All the Heating functionality for the 4 seats will be done here.         */
/* Switch request acceptance                                                */
/* Asking for Relay based on the switch request                             */
/* Turn ON/OFF heats for the respective sides with correct PWM and timeouts */
/* Do the diagnostics for each side                                         */
/* Mature the faults in case of short or open                               */
/* 2sec LED display fault behavior                                          */
/* Check for any calibration data change for heat seats, write to EEPROM    */

/* includes */
/* NWM Inculde files */
#include "il_inc.h"
#include "v_inc.h"
/*Application Inculde files */
#include "TYPEDEF.h"
#include "DiagMain.h"
#include "DiagIOCtrl.h"
#include "DiagWrite.h"
#include "canio.h"
//#include "Eval_Board_RefDef.h"
//#include "Flash_EEPROM.h"
#include "main.h"
#include "heating.h"
#include "relay.h"
#include "FaultMemory.h"
#include "FaultMemoryLib.h"
//#include "BattV.h"
#include "Temperature.h"
#include "AD_Filter.h"
#include "Venting.h"
#include "Pwm_Stagger.h" 
//#include "StWheel_Heating.h"
#include "Internal_Faults.h"
#include "mw_eem.h"

/* Standard include files */
//#include <stdio.h>
//#include <string.h>
/* Autosar include Files */
#include "Mcu.h"
#include "Port.h"
#include "Dio.h"
#include "Gpt.h"
#include "Pwm.h"
#include "Adc.h"

/* external data */

/* forward declarations                                                                                                  */
/* extern void ADF_AtoD_Cnvrsn(u_8Bit AD_Filter_supply);                                                                 */
/* extern void ADF_Filter(u_8Bit AD_Filter_ch);                                                                          */
/* extern void ADF_Mux_Filter(u_8Bit AD_Filter_slctd_mux,u_8Bit AD_Filter_mux_ch);                                       */
/* extern u_8Bit ADF_Nrmlz_Isense(u_16Bit AD_Isense_value);                                                              */
/* extern u_8Bit ADF_Nrmlz_Vsense(u_16Bit AD_Vsense_value);                                                              */
/* extern void ADF_Str_UnFlt_Mux_Rslt(u_8Bit AD_Filter_feed,u_8Bit AD_Filter_mux_feed);                                  */
/* extern void ADF_Str_UnFlt_Rslt(u_8Bit AD_Filter_control);                                                             */
/* extern void dbkGetTxL_F_HS(void);                                                                                     */
/* extern void dbkGetTxR_F_HS(void);                                                                                     */
/* extern void dbkGetTxRR_SEAT(void);                                                                                    */
/* extern void dbkPutTxL_F_HS( variable);                                                                                */
/* extern void dbkPutTxR_F_HS(void);                                                                                     */
/* extern void dbkPutTxRR_SEAT(void);                                                                                    */
/* extern void FMemLibF_ReportDtc(DtcType DtcCode,enum ERROR_STATES State);                                              */
/* extern void hsLF_2SecLEDDisplay(void);                                                                                */
/* extern void hsLF_2SecMonitorTime_FaultDetection(void);                                                                */
/* extern void hsLF_ClearHeatSequence(void);                                                                             */
/* extern void hsLF_ControlFET(unsigned char hs_nr,unsigned char duty_cycle);                                            */
/* extern void hsLF_GetHeatedSeatCanStatus(void);                                                                        */
/* extern void hsLF_HeatDiagnostics(void);                                                                               */
/* extern void hsLF_RemoteStart_Control(void);                                                                           */
/* extern void hsLF_SendHeatedSeatCanStatus(Heat_Seat_Status HeatSeatStatus,unsigned char hs_nr,unsigned char action_c); */
/* extern void hsLF_Timer(hs_start_timer_b hsStartTimerB,unsigned char hs_nr,unsigned char action_c);                    */
/* extern void hsLF_UpdateHeatParameters(unsigned char hs_nr,unsigned char value_c);                                     */
/* extern void StaggerF_Manager(Stagger_Channel_Type Channel_ID,u_16Bit this_duty_cycle);                                */
/* extern u_8Bit TempF_Get_NTC_Rdg(u_8Bit temperature_hs_position);                                                      */

/* Defines */

/* Based on the defines, heating module points to the actual PWM and Timeout values (hs_heat_pwm_values_c and hs_heat_timeout_values_c */
#define HEAT_LEVEL_HL3  3
#define HEAT_LEVEL_HL2  2
#define HEAT_LEVEL_HL1  1
#define HEAT_LEVEL_LL1  0

#define VEH_LINE_REAROFFSET   1 //Rear Seat Calibs OFFset from the main Table.
#define VEH_LINE_FRONTOFFSET  0 //Front Seat Calibs OFFset from the main Table.

/* Seat Mask */
#define    HS_FL_SEAT_MASK           0x01
#define    HS_FR_SEAT_MASK           0x02
#define    HS_RL_SEAT_MASK           0x04
#define    HS_RR_SEAT_MASK           0x08
#define    HS_ALL_SEAT_MASK          0x0F

/* Remote start Insertion Point */
//#define HS_RMT_START_HL3             0x11
//#define HS_RMT_START_HL2             0x22
//#define HS_RMT_START_HL1             0x33
//#define HS_RMT_START_LL1             0x44

//#define LOAD_DEFAULTS                0x55  //Not used any where..PC Lint Info 752..Harsha 11/04/2009

/* Rear Seat Stuck Switch detection defines */
#define STK_PRESS_K                0x00
//#define STK_RELEASE_K              0x01    //Not used any where..PC Lint Info 752..Harsha 11/04/2009

/* Rear Seat Stuck Switch DTC Status EEPROM Location Index */
/* Note: These Indexes to be matched with FaultMemoryLib.h */
/* 02.18.2009 New values for rear seat switch stuck DTC defines */
//#define REAR_LEFT_STUCK_INDEX      0x2D 	//0x2E	02.18.2009 New Index after deleting STW partial open DTC
//#define REAR_RIGHT_STUCK_INDEX     0x2E		//0x2F  02.18.2009 New Index after deleting STW partial open DTC
#define REAR_LEFT_STUCK_INDEX      0x28 	//0x2D	04.15.2009 New Index after moving up
#define REAR_RIGHT_STUCK_INDEX     0x29		//0x2E  05.15.2009 New Index after moving up

/* Definitions for PID controller */
#define PID_NTC_MAX        (255)
#define PID_FRAC_BITS      (16)
#define PID_SCALE_FACTOR   (1 << PID_FRAC_BITS)  /* 1.000 in U16.16 format */
#define PID_MAX            (100*PID_SCALE_FACTOR)
#define PID_MIN            (0)
#define PID_KP             (40*PID_SCALE_FACTOR)
#define PID_KI             (5.00*PID_SCALE_FACTOR)
#define PID_KD             (0.10*PID_SCALE_FACTOR)
#define PID_DT             (1)                   /* Called every 10 ms */
#define PID_HEAT_HL3       (124)                 /* #define PID_HEAT_HL3 (131 or 0x83) NO I NEED (124 0x7C) about 45 degrees C or 113 degrees F*/ 
#define PID_HEAT_HL2       (113)                 /* about 40 degrees C or 104 degrees F*/
#define PID_HEAT_HL1       (153)                 /* about 35 degrees C or 95 degrees  F*/
#define PID_HEAT_LL1       (91)                  /* #define PID_HEAT_LL3 (164 or 0xA4) NO I NEED (91 0x5B)  about 30 degrees C or 86 degrees  F*/       
#define PID_WAIT_TIME      (1024)                /* Increments of 0.01 seconds */

/* Byte and Array variables */
/* PID controller setpoints */
u_8Bit hs_pid_setpoint[ALL_HEAT_SEATS];

//unsigned char hs_rl_led1_stat_c; //Hardware R L LED1 status value  //Not used any where..PC Lint Info 752..Harsha 11/04/2009
//unsigned char hs_rl_led2_stat_c; //Hardware R L LED2 status value  //Not used any where..PC Lint Info 752..Harsha 11/04/2009  
//unsigned char hs_rr_led1_stat_c; //Hardware R L LED1 status value  //Not used any where..PC Lint Info 752..Harsha 11/04/2009 
//unsigned char hs_rr_led2_stat_c; //Hardware R L LED2 status value  //Not used any where..PC Lint Info 752..Harsha 11/04/2009
unsigned char hs_rear_seat_led_status_c [4];

/* Switch Request to Heat seats */
unsigned char hs_switch_request_c [ALL_HEAT_SEATS];
//#define hs_switch_request_fl_c  hs_switch_request_c[ FRONT_LEFT ]  //Not used any where..PC Lint Info 752..Harsha 11/04/2009
//#define hs_switch_request_fr_c  hs_switch_request_c[ FRONT_RIGHT ] //Not used any where..PC Lint Info 752..Harsha 11/04/2009
//#define hs_switch_request_rl_c  hs_switch_request_c[ REAR_LEFT ]   //Not used any where..PC Lint Info 752..Harsha 11/04/2009
//#define hs_switch_request_rr_c  hs_switch_request_c[ REAR_RIGHT ]  //Not used any where..PC Lint Info 752..Harsha 11/04/2009

/* Stores the present seat request From IO Controlling through KWP Diagnostics */
unsigned char hs_IO_seat_req_c[ ALL_HEAT_SEATS ];
//#define hs_IO_seat_req_fl_c      hs_IO_seat_req_c[ FRONT_LEFT ]   //Not used any where..PC Lint Info 752..Harsha 11/04/2009
//#define hs_IO_seat_req_fr_c      hs_IO_seat_req_c[ FRONT_RIGHT ]  //Not used any where..PC Lint Info 752..Harsha 11/04/2009
//#define hs_IO_seat_req_rl_c      hs_IO_seat_req_c[ REAR_LEFT ]    //Not used any where..PC Lint Info 752..Harsha 11/04/2009
//#define hs_IO_seat_req_rr_c      hs_IO_seat_req_c[ REAR_RIGHT ]   //Not used any where..PC Lint Info 752..Harsha 11/04/2009

/* Stores the seat status From IO Controlling through KWP Diagnostics */
unsigned char hs_IO_seat_stat_c[ ALL_HEAT_SEATS ];
//#define hs_IO_seat_stat_fl_c      hs_IO_seat_stat_c[ FRONT_LEFT ]   //Not used any where..PC Lint Info 752..Harsha 11/04/2009
//#define hs_IO_seat_stat_fr_c      hs_IO_seat_stat_c[ FRONT_RIGHT ]  //Not used any where..PC Lint Info 752..Harsha 11/04/2009
//#define hs_IO_seat_stat_rl_c      hs_IO_seat_stat_c[ REAR_LEFT ]    //Not used any where..PC Lint Info 752..Harsha 11/04/2009
//#define hs_IO_seat_stat_rr_c      hs_IO_seat_stat_c[ REAR_RIGHT ]   //Not used any where..PC Lint Info 752..Harsha 11/04/2009

/* Switch Request to Heat seats output FETs    $/$/heiz_fr_switch_request_to_fet_c */
unsigned char hs_switch_request_to_fet_c [ALL_HEAT_SEATS];
//#define hs_switch_request_to_fet_fl_c  hs_switch_request_to_fet_c[ FRONT_LEFT ]  //Not used any where..PC Lint Info 752..Harsha 11/04/2009
//#define hs_switch_request_to_fet_fr_c  hs_switch_request_to_fet_c[ FRONT_RIGHT ] //Not used any where..PC Lint Info 752..Harsha 11/04/2009
//#define hs_switch_request_to_fet_rl_c  hs_switch_request_to_fet_c[ REAR_LEFT ]   //Not used any where..PC Lint Info 752..Harsha 11/04/2009
//#define hs_switch_request_to_fet_rr_c  hs_switch_request_to_fet_c[ REAR_RIGHT ]  //Not used any where..PC Lint Info 752..Harsha 11/04/2009

/* This variable holds the latest executed pwm VALUE. Compare this variable with the hs_allowed_heat_pwm and decide whether there is a need to change the PWM if both values are different. */
unsigned char hs_temp_heat_pwm_c[ALL_HEAT_SEATS];
//#define hs_temp_heat_pwm_fl_c     hs_temp_heat_pwm_c[FRONT_LEFT]   //Not used any where..PC Lint Info 752..Harsha 11/04/2009
//#define hs_temp_heat_pwm_fr_c     hs_temp_heat_pwm_c[FRONT_RIGHT]  //Not used any where..PC Lint Info 752..Harsha 11/04/2009
//#define hs_temp_heat_pwm_rl_c     hs_temp_heat_pwm_c[REAR_LEFT]    //Not used any where..PC Lint Info 752..Harsha 11/04/2009
//#define hs_temp_heat_pwm_rr_c     hs_temp_heat_pwm_c[REAR_RIGHT]   //Not used any where..PC Lint Info 752..Harsha 11/04/2009

/*  Currently allowed heat time for (HIGH2/HIGH1/LOW2/LOW1 ... 10/20/15/15 in min) */
unsigned char hs_allowed_heat_time_c [ALL_HEAT_SEATS];
//#define hs_allowed_heat_time_fl_c  hs_allowed_heat_time_c[ FRONT_LEFT ]  //Not used any where..PC Lint Info 752..Harsha 11/04/2009
//#define hs_allowed_heat_time_fr_c  hs_allowed_heat_time_c[ FRONT_RIGHT ] //Not used any where..PC Lint Info 752..Harsha 11/04/2009
//#define hs_allowed_heat_time_rl_c  hs_allowed_heat_time_c[ REAR_LEFT ]   //Not used any where..PC Lint Info 752..Harsha 11/04/2009
//#define hs_allowed_heat_time_rr_c  hs_allowed_heat_time_c[ REAR_RIGHT ]  //Not used any where..PC Lint Info 752..Harsha 11/04/2009

/*  Currently allowed wait time for NTC Check for (HIGH2/HIGH1/LOW2/LOW1 ... 1/2/5/0 in min) */
unsigned char hs_wait_time_check_NTC_c [ALL_HEAT_SEATS];

/* NTC feedback thresholds */
unsigned char hs_allowed_NTC_c [ALL_HEAT_SEATS];

/* Stores the present seat request Heated Seats */
unsigned char hs_temp_seat_req_c[ ALL_HEAT_SEATS ];
#define hs_temp_seat_req_fl_c      hs_temp_seat_req_c[ FRONT_LEFT ]
#define hs_temp_seat_req_fr_c      hs_temp_seat_req_c[ FRONT_RIGHT ]
#define hs_temp_seat_req_rl_c      hs_temp_seat_req_c[ REAR_LEFT ]
#define hs_temp_seat_req_rr_c      hs_temp_seat_req_c[ REAR_RIGHT ]

/* Stores the present seat status */
unsigned char hs_temp_seat_stat_c[ ALL_HEAT_SEATS ];
//#define hs_temp_seat_stat_fl_c      hs_temp_seat_stat_c[ FRONT_LEFT ]
//#define hs_temp_seat_stat_fr_c      hs_temp_seat_stat_c[ FRONT_RIGHT ]
//#define hs_temp_seat_stat_rl_c      hs_temp_seat_stat_c[ REAR_LEFT ]
//#define hs_temp_seat_stat_rr_c      hs_temp_seat_stat_c[ REAR_RIGHT ]

/* Time counter varaible for monitoring 2sec to mature or demature incase of primary faults set */
unsigned char hs_monitor_prelim_fault_counter_c[ ALL_HEAT_SEATS ];
//#define hs_monitor_prelim_fault_counter_fl_c      hs_monitor_prelim_fault_counter_c[ FRONT_LEFT ]  //Not used any where..PC Lint Info 752..Harsha 11/04/2009
//#define hs_monitor_prelim_fault_counter_fr_c      hs_monitor_prelim_fault_counter_c[ FRONT_RIGHT ] //Not used any where..PC Lint Info 752..Harsha 11/04/2009
//#define hs_monitor_prelim_fault_counter_rl_c      hs_monitor_prelim_fault_counter_c[ REAR_LEFT ]   //Not used any where..PC Lint Info 752..Harsha 11/04/2009
//#define hs_monitor_prelim_fault_counter_rr_c      hs_monitor_prelim_fault_counter_c[ REAR_RIGHT ]  //Not used any where..PC Lint Info 752..Harsha 11/04/2009

/* This value holds the latest autosar PWM value for the heaters. */
/* 100% is 0x8000                                                 */
//unsigned int hs_autosar_pwm_w[ALL_HEAT_SEATS];

//Rear Seat input switch status
//unsigned char hs_rear_swth_stat_c[ALL_REAR_SWITCH]; //Status of Switch position. Moved to heating.h
unsigned short hs_rear_mature_time_w;    //EEPROM value * 40ms mature wait time before setting fault
unsigned char hs_rear_demature_time_c;  //EEPROM value * 40ms demature wait time before setting fault
unsigned short hs_rear_mature_counter_w[ALL_REAR_SWITCH];  //On time counter for mature time
unsigned char hs_rear_demature_counter_c[ALL_REAR_SWITCH]; //On time counter for de mature
unsigned char hs_bounce_counter_c[ALL_REAR_SWITCH]; //Bounce counter when switch pressed


/* Holds the Write data from UDS Diagnostics request */
unsigned char hs_diag_prog_data_c[ 5 ];

/* This variable determines which heat side we are looking for DTCs */
unsigned char hs_seat_fault_counter_c;
unsigned char hs_ntc_fault_counter_c;

/****************************** Start MKS 50738 & MKS 58709 ************************************/
/* MKS 50738: New loop counter introduced to increase wait time from 2sec to 2min */
/* MKS 58709: Loop counter changed from hs_monitor_loop_counter_c to hs_monitor_loop_counter_w  */
/* This counter variable used to increase the wait time for Partial OPEN faults */

/* MKS 50738: Partial OPEN 2min(120 sec) = 60 times (2sec one loop) */
//unsigned char hs_monitor_loop_counter_c[ALL_HEAT_SEATS];

/* MKS 58709: Partial OPEN 15 min(900 sec) = 450 times (2sec one loop) */
unsigned short hs_monitor_loop_counter_w[ALL_HEAT_SEATS]; //from AL
/****************************** End MKS 50738 & MKS 58709 ************************************/
//unsigned short counter_w;  //Not used any where..PC Lint Info 752..Harsha 11/04/2009
//unsigned char hs_PWM_off_counter_c; //Not used any where..PC Lint Info 752..Harsha 11/04/2009

/* Heated Seats AD conversion parameter */
main_ADC_prms hs_AD_parameters;


union char_bit hs_temp_set_c; 
#define hs_ntc_updated_b       hs_temp_set_c.b.b0
unsigned char hs_vehicle_line_offset_c;

/* Function prototypes */
/* Functional Fault detection for the outputs */
void hsLF_HeatDiagnostics(void);

/* Incase fault happens, fault behaviour */
void hsLF_2SecLEDDisplay(void);

/* Send the Heated seat status on Bus */
void hsLF_SendHeatedSeatCanStatus(unsigned char hs_nr,unsigned char action_c);

/* Get the heated seat status from Bus */
void hsLF_GetHeatedSeatCanStatus(unsigned char hs_nr);

/* Function starts/stops the timeout counter for the Heated seat PWMs */
void hsLF_Timer(unsigned char hs_nr,unsigned char action_c);

/* This function will be called after executing the diagnostics for Heaters */
void hsLF_2SecMonitorTime_FaultDetection(unsigned char hs_nr);

/* These functions will do the Isense, Vsense, Impedance AD Readings respectively */
void hsLF_Isense_AD_Readings(unsigned char seat);
void hsLF_Vsense_AD_Readings(unsigned char seat);
void hsLF_Zsense_AD_Readings(unsigned char seat);

/* These functions will do the conversion for Isense and Vsense readings respectively */
void hsLF_Isense_AD_Conversion(void);
void hsLF_Vsense_AD_Conversion(unsigned char heater);

/* This function retrieves the actual PWM state of each heater. 1 for PWMing 0 for OFF */
void hsLF_GetPWM_State(unsigned char heater);

/* Stores the valid Diagnostic data into a buffer, from which the writes are performed */
void hsLF_CopyDiagnoseParameter( unsigned char * );
void hsLF_ConvertDegToAD(unsigned char serv, unsigned char *);

/* Sub Functions called from hsF_HeatManager */
/* Remote Start Control for Heated Seats */
void hsLF_RemoteStart_Control (void);
/* Enable rear heaters for Automatic Start PowerNet Architecture */
//void hsLF_Enable_RearHeaters(void); //commented.
/* ALways looks for signals from Bus or from HW if Rear seats signals to be looked */
void hsLF_Catch_Signals(unsigned char hs_nr);
/* Always looks for HW signal requests for Rear seats */
void hsLF_Catch_Rear_Switch_Requests(unsigned char hs_nr);
/* Process any of Diag IO control or Routine control for Seat Outputs */
void hsLF_Process_DiagIO_RC_Outputs(unsigned char hs_nr);
/* Process any of Diag IO control for Seat LEDs */
void hsLF_Process_DiagIO_LEDStatus (unsigned char hs_nr);
/* Process Switch requests from the Bus either CCN or HVAC */
void hsLF_Process_Switch_Requests_Bus (unsigned char hs_nr);
/* Process Switch requests from the Own HArdware for rear seats */
void hsLF_Process_Switch_Requests_ECU (unsigned char hs_nr);
/* Change the LED display for Rear Seats if equipped with HW */
void hsLF_SendRearSeat_HWStatus(unsigned char hs_nr,unsigned char action_c);
/*Special Routine control tests for Heater outputs */
//void hsLF_OutputTests (unsigned char hs_nr); //Not used any where..PC Lint Info 752..Harsha 11/04/2009

/* PID controller */
void pid_cal(void);

/* Temperature setpoint controller */
void pid_heat_setting(void);

/****************************************************************************************/
/*										hsF_Init 										*/
/****************************************************************************************/
/* Initialization sequence for the Heating Module.                                       */
/* The important calibration parameters will be stored into Local variables from EEPROM. */
/* Also make sure all other local variables are set to Pre defined values.               */
/*                                                                                       */
/* Calls from                                                                            */
/* --------------                                                                        */
/* main :                                                                                */
/*    hsF_Init()                                                                         */
/*                                                                                       */
/* Calls to                                                                              */
/* --------                                                                              */
/* hsLF_ClearHeatSequence()                                                              */
@far void hsF_Init(void)
{

	/* Get all the parameters from EEP to Shadow RAM */
	EE_BlockRead(EE_HEAT_CAL_DATA_PF, (u_8Bit*)&hs_cal_prms); //MKS 61288

	/* Calib Parameters to be get in for default PF as of MY12 Program */
	/* This logic needs to be upgraded once we get more vehicle lines */
	if(ru_eep_PROXI_Cfg.s.Vehicle_Line_Configuration == PROXI_VEH_LINE_PF)
	{
		hs_vehicle_line_c = HS_VEH_PF;
	}
	else 
	{
		/* As of now we have only one vehicle line that is PF */
		hs_vehicle_line_c = HS_VEH_PF;
	}

	/* Get the Seat Material Type */
	if(ru_eep_PROXI_Cfg.s.Seat_Material == PROXI_SEAT_TYPE_CLOTH)
	{
		/* Calib Parameters to be get in for Cloth */
	    hs_seat_config_c = HS_VEH_CLOTH;
	}
	else if(ru_eep_PROXI_Cfg.s.Seat_Material == PROXI_SEAT_TYPE_LEATHER)
	{
		/* Calib Parameters to be get in for Cloth */
	    hs_seat_config_c = HS_VEH_BASE_LEATHER;
	}
	else
	{
		/* Calib Parameters to be get in for Cloth */
	    hs_seat_config_c = HS_VEH_PERF_LEATHER;
	}

	EE_BlockRead(EE_HEAT_FAULT_THRESHOLDS, (u_8Bit*)&hs_flt_dtc_c); //MKS 50111

	/* MKS 52375: Constant Names changed from specific to general */
	hs_rear_mature_time_w = /*hs_rear_stksw_mat_time_c*/ hs_flt_dtc_c[HS_REAR_STUCK_MATURE_TIME] * 25; //Called in 40ms time slice to detect stuck switch
	hs_rear_demature_time_c = /*hs_rear_stksw_demat_time_c*/ (unsigned char)(hs_flt_dtc_c[HS_REAR_STUCK_DEMATURE_TIME]* 25); //Called in 40ms time slice to demature stuck switch

	/* This function call will store all the default values to the RAM variables */
	hsLF_ClearHeatSequence();
}

/****************************************************************************************/
/*									hsF_HeatManager 									*/
/****************************************************************************************/
/* Logic handler for the Heat seats.                                                                                                                                                                             */
/*                                                                                                                                                                                                               */
/* If the condition of ignition=RUN/START met, this function determines how to interpret CAN switch input for the vehicle lines DS.                                                                              */
/* Same function works for                                                                                                                                                                                       */
/*                                                                                                                                                                                                               */
/* Normal switching cases from CAN Bus                                                                                                                                                                           */
/* Diagnostic IO controlling using UDS.                                                                                                                                                                          */
/* Remote Start Functionality (Branching done for hsLF_RemoteStart_Control).                                                                                                                                     */
/*                                                                                                                                                                                                               */
/* Getting Switch Request							*/
/* MY10 supports different vehicle lines            */
/* Veh lines DS, D2, DA, DD, DJ, DP looks for CCN signals (Front and Rear) */
/* Veh line RT looks for HVAC signals (Front and Rear)                     */
/* Veh lines WK, WH, WD, WC and LX looks for HVAC signals (Front seat)     */
/* and for Rear seats looks from Own HArdware switch press                 */
/*                                                                                                                                                                                                               */
/* IO control will have higher precedance than the Normal Switching. While doing IO, normal switch is disabled.                                                                                                  */
/* If vehicle is equipped with Remote Start, Heaters will turn ON automatically If AMB TEMp <= 4.4c.                                                                                                               */
/*                                                                                                                                                                                                               */
/* Pre-conditions to start Heating:                                                                                                                                                                              */
/* The CSWM shall turn on the heated seats only with engine running.                                                                                                                                             */
/* Switching Logic:                                                                                                                                                                                              */
/* Seat heating has 2 selectable heat settings of HI and LO.                                                                                                                                                     */
/* A calibration lookup table of 2 high settings(HL2, HL1) and 2 low settings (LL2, LL1) is to be used.                                                                                                          */
/*                                                                                                                                                                                                               */
/* Always the Heat starts with 100% PWM (2 LEDs Illuminated) with first switch request.                                                                                                                          */
/*                                                                                                                                                                                                               */
/* HL3: 15min with  100% PWM  0min wait time to check NTC feedbacks                                                                                                                                              */
/* HL2: 15min with   50% PWM  0min wait time to check NTC feedbacks                                                                                                                                              */
/* HL1: 30min with   25% PWM  0min wait time to check NTC feedbacks                                                                                                                                              */
/* LL1: 45min with   10% PWM  0min wait time to check NTC feedbacks Runs for 45min                                                                                                                               */
/*                                                                                                                                                                                                               */
/* Though we have 3 high settings and 1 low settings, at any point time the HSM_A1 will be displaying the status as HS_HI (if we are in any of 3 high settings) and HS_LOW (If we are in low settings).          */
/*                                                                                                                                                                                                               */
/* Heat Seat Output signal (HSM_A1/CSWM_A1 in message Matrix) will send the status of CSWM.                                                                                                                      */
/* For Heated seats we will be displaying either HS_HI/HS_LOW/HS_OFF.                                                                                                                                            */
/*                                                                                                                                                                                                               */
/* Each press of the switch will change the seat heat setting from                                                                                                                                               */
/* OFF to HI                                                                                                                                                                                                     */
/* HI to LO and                                                                                                                                                                                                  */
/* LO to OFF, or else the automatic timer will turn off the heat cycle in roughly 60 minutes.                                                                                                                    */
/*                                                                                                                                                                                                               */
/* Thermistor Reading and control logic:                                                                                                                                                                         */
/* We will be reading the NTC feedback always with every 40ms, but the decision will be made on these feedbacks at the following steps:                                                                          */
/*                                                                                                                                                                                                               */
/* 1. At any point of time while Heaters are operating and if logic observes the thermistor readings are > Cut OFF Temp, Logic turns OFF heaters.                                                                */
/*    Then onwards, If user gives switch request logic displays LEDs for 2SEC time.                                                                                                                              */
/*                                                                                                                                                                                                               */
/* 2. When switch pressed first time: (OFF to HI)                                                                                                                                                                */
/*    Directly goes to HL3 with 100% PWM                                                                                                                                                                         */
/*                                                                                                                                                                                                               */
/* 3  When switch pressed Second time: (HI to LO)                                                                                                                                                                */
/*    Directly goes to LL1.                                                                                                                                                                                      */
/*                                                                                                                                                                                                               */
/* 4  When Switch pressed Third Time: (LOW to OFF)                                                                                                                                                               */
/*    Turns OFF the Heaters                                                                                                                                                                                      */
/*                                                                                                                                                                                                               */
/* Diagnostic Feedback:                                                                                                                                                                                          */
/* Once any seat is turned ON, logic will be looking for the output feedback. Based on the feedback range module will allow heat to continue, or turn OFF if any fault occurs that is either Short or Open.      */
/* The function to look feedback is hsF_HeatDiagnostics()                                                                                                                                                        */
/*                                                                                                                                                                                                               */
/* Heat Fault behavior:                                                                                                                                                                                          */
/* In case any of the faults occuurs, module have defined fault behavior. that is turn OFF the heats. and again user issues switch request we display LED for 2sec                                               */
/* The function to display the fault behavior is hsF_2SecLEDDisplay().                                                                                                                                           */
/*                                                                                                                                                                                                               */
/* STEP 0: Enable the U12S LED Pin for Rear seats, if vehicle lines are WK/LX                                                                                                                                    */   
/* STEP 1: Look for IGN Status. IF RUN/START enter into the Logic. Else clear all the variables and turn OFF outputs (Step 9)                                                                                    */
/* STEP 2: Look for NTC Status. Enter inside logic If Status is other than UNDEFINED. Else Wait here                                                                                                             */
/* STEP 3: Look for any IO controlling happening for Outputs. If no IO look for signals                                                                                                                          */
/* STEP 4: Look for any IO controlling happening for LED status. If not go for next step.                                                                                                                        */
/* STEP 5: Look for Main status. If status is Normal, execute the normal execution, else execute Remote control                                                                                                  */
/* STEP 5A: Look for Switch request source and process accordingly.                                                                                                                                              */
/* STEP 6: Look for NTC value greater than Cut OFF Temp condition.                                                                                                                                               */
/* STEP 7: Heat diagnsotics Logic.                                                                                                                                              								 */
/* STEP 8: 2Sec LED Display Logic.                                                                                                                                              							     */
/* STEP 9: Clear Heat sequence Logic                                                                                                                                                                             */            
/* Calls from                                                                                                                                                                                                    */
/* --------------                                                                                                                                                                                                */
/* main :                                                                                                                                                                                                        */
/*    hsF_HeatManager()                                                                                                                                                                                          */
/*                                                                                                                                                                                                               */
/* Calls to                                                                                                                                                                                                      */
/* --------                                                                                                                                                                                                      */
/* hsLF_2SecLEDDisplay()                                                                                                                                                                                         */
/* u_8Bit TempF_Get_NTC_Rdg(u_8Bit temperature_hs_position)                                                                                                                                                      */
/* hsLF_ClearHeatSequence()                                                                                                                                                                                      */
/* hsLF_HeatDiagnostics()                                                                                                                                                                                        */
/* hsLF_Timer(unsigned char hs_nr,unsigned char action_c)                                                                                                                                                        */
/* hsLF_GetHeatedSeatCanStatus()                                                                                                                                                                                 */
/* hsLF_2SecMonitorTime_FaultDetection()                                                                                                                                                                         */
/* hsLF_UpdateHeatParameters()                                                                                                                                                                                   */
/* hsLF_RemoteStart_Control()                                                                                                                                                                                    */
/* hsLF_Catch_Signals(unsigned char hs_nr)                                                                                                                                                                       */
/* hsLF_Catch_Rear_Switch_Requests(unsigned char hs_nr)                                                                                                                                                          */
/* hsLF_Process_DiagIO_RC_Outputs(unsigned char hs_nr)                                                                                                                                                              */
/* hsLF_Process_DiagIO_LEDStatus (unsigned char hs_nr)                                                                                                                                                           */
/* hsLF_Process_Switch_Requests_Bus (unsigned char hs_nr)                                                                                                                                                        */
/* hsLF_Process_Switch_Requests_ECU (unsigned char hs_nr)                                                                                                                                                        */
/* hsLF_SendRearSeat_HWStatus(unsigned char hs_nr,unsigned char action_c)                                                                                                                                        */
@far void hsF_HeatManager(void)
{
	unsigned char hs_nr, hs_LED_U12S_stat_c;
	u_16Bit TempOdo;	// each count is 16km. So, TempOdo = 5 = 5*16= 80km

	
	/* Read the LED_U12S_EN signal */
	hs_LED_U12S_stat_c = Dio_ReadChannel(LED_U12S_EN);

    /**************************************** STEP 0 ***********************************/
	/*                                 MKS 21792									   */ 
	/**************************************** STEP 0 ***********************************/
	/* 12/15/2008: We moved the enabling of the LED switch circuitry from inside the ignition run/start */
	/* condition to outide of it. Without enabling the LED circuitry, the RL and RR switch inputs are LOW */
	/* which leads having them seen as pressed. We do this to avoid setting the stuck switch DTCs. */
	
	/* Enable the Rear LED U12S switching only incase Variant supports 4H functionality */
	/* And if switch requests are from Hardware (And if it is not already enabled) */
	/* MKS 54495: Added EOL Start flag also to enable the  Rear switch readings */
	if (((CSWM_config_c & CSWM_4HS_VARIANT_MASK_K) == CSWM_4HS_VARIANT_MASK_K) && 
		((main_SwReq_Rear_c == VEH_SWITCH_REAR_ONLY_K) || (diag_eol_io_lock_b == TRUE)) && 
		(hs_LED_U12S_stat_c == STD_LOW) )
	{
			/* ENABLE Rear U12S for switch readings */
			Dio_WriteChannel(LED_U12S_EN, STD_HIGH);
	}

	/* Go to the Heat switch logic, if the IGN is in RUN or START. One more IF condition to be added below this to check if NTC status is other than NOT DEFINED. This logic to be added right after for loop. */
    /**************************************** STEP 1 ***********************************/
	/**************************************** STEP 1 ***********************************/
	if ( (canio_RX_IGN_STATUS_c == IGN_RUN) || (canio_RX_IGN_STATUS_c == IGN_START) )
	{
		for (hs_nr = FRONT_LEFT; hs_nr < ALL_HEAT_SEATS; hs_nr++)
		{
			/* Get the  NTC Readings into Local variable.. For testing purpose..Needs to be deleted */
			NTC_AD_Readings_c[hs_nr] = TempF_Get_NTC_Rdg(hs_nr);

		    /**************************************** STEP 2 ***********************************/
			/**************************************** STEP 2 ***********************************/
			/* Is NTC Status is with values? */
			if (temperature_HS_NTC_status[hs_nr] != NTC_STAT_UNDEFINED)
			{
			    /**************************************** STEP 3 ***********************************/
				/* Process IO Controlling for individual Seat Requests or All seat request at one time or If no IO controlling accept the switch request logic */
			    /**************************************** STEP 3 ***********************************/
				/* Are we processing the switch request through normal case or through IO Controlling? */
				/* IF through the Normal Bus request, get the value from Bus.                          */
				/* If it is through IO Controlling or Routine control self tests						*/
				if ((!diag_io_hs_lock_flags_c[hs_nr].b.switch_rq_lock_b) && (!diag_io_all_hs_lock_b) && (diag_rc_all_op_lock_b == FALSE) )
				{
						/* Either IO control Just ended or No IO control, catch the signals from Bus */
						hsLF_Catch_Signals (hs_nr);
				}
				else
				{
						/* IO Controlling is ON/Special tests(Self tests for heaters) ON, Process the Diag IO request/Special Routine control request */
						hsLF_Process_DiagIO_RC_Outputs (hs_nr);
				}
				
			    /**************************************** STEP 4 ***********************************/
				/* IO controlling for individual Seat LED status */
			    /**************************************** STEP 4 ***********************************/
				hsLF_Process_DiagIO_LEDStatus (hs_nr);

				/**************************************** STEP 5 ***********************************/
				/* Process Normal Switch Sequence/Remote start */
			    /**************************************** STEP 5 ***********************************/
				/* Main State is Normal.Do the normal switch request */
				if (main_state_c == NORMAL)
				{
					/* Just returned from Remote Start? Turn OFF either F L or F R depending on the Driver state. */
					if (hs_control_flags_c[ hs_nr ].b.remote_user_b)
					{
						/* Turn OFF the outputs, as we are just returning from Remote start.. */
						/* Harsha Commented for New requirement 05.12.2009 */
						/* MKS 28818: When remote start completes, new requirement says turned Outputs */
						/* Shall stay ON */
						//hsLF_UpdateHeatParameters(hs_nr, HEAT_LEVEL_OFF);
						
						/* Harsha added new requirement .. 02/17/2010 .. For WK S2B Release */
						/* MKS 47354: When IGN status remains in RUN and Ign Run Remote Start turns Inactive */
						/* Heated Seat will start back from 100% PWM */
						
						/* Harsha commented. Chrysler roll back their decision ..02/18/2010 */
//						if (hs_rem_seat_req_c[hs_nr] != SET_OFF_REQUEST)
//						{
//							hsLF_UpdateHeatParameters(hs_nr, HEAT_LEVEL_HL3);
//						}
						

						/* Clear the flag., Next time this logic will not execute */
						hs_control_flags_c[ hs_nr ].b.remote_user_b = FALSE;
					}
					else
					{
						/* Either state is Normal or completed Remote start. */
					}

				    /**************************************** STEP 5A ***********************************/
				    /**************************************** STEP 5A ***********************************/
					/* Processing for rear seats and switch requests from ECU? */
					/* If YES, look for Hardware switch request */
					/* If NO, look for switch requests from BUS */
					// MKS 44686 When Remote start is activated, in the first 4.5sec, Rear Heated seats are operated. According to the spec, while Remst is active, Rear seats to be disabled
					// to fix it && (canio_RX_IgnRun_RemSt_c == FALSE) was added
					if (((hs_nr == REAR_LEFT)||(hs_nr == REAR_RIGHT)) && (main_SwReq_Rear_c == VEH_SWITCH_REAR_ONLY_K) && (canio_RX_IgnRun_RemSt_c == FALSE))
					{
						
						//MKS 21852
						//Accept hardwired switch inputs only if there are no stuck switch faults
						//Logic looks for actual eeprom information to see the Rear switch stuck DTC status */
						//If the DTCs are adjusted in FaultMemoryLib.h, the index needs to be adjusted here for Rear stuck DTCs */
						if(hs_nr == REAR_LEFT && 
						   hs_rear_seat_flags_c[ RL_SWITCH ].b.stksw_psd == FALSE &&
						   d_AllDtc_a[REAR_LEFT_STUCK_INDEX].DtcState.state.TestFailed == FALSE)
						{
							/*Switch requests from BUS. */
							hsLF_Process_Switch_Requests_ECU (REAR_LEFT);
						}
						//Accept hardwired switch inputs only if there are no stuck switch faults
						if(hs_nr == REAR_RIGHT && 
						   hs_rear_seat_flags_c[ RR_SWITCH ].b.stksw_psd == FALSE &&
						   d_AllDtc_a[REAR_RIGHT_STUCK_INDEX].DtcState.state.TestFailed == FALSE)
						{
							/*Switch requests from BUS. */
							hsLF_Process_Switch_Requests_ECU (REAR_RIGHT);
						}

					}
					else
					{

					// MKS 44686 When Remote start is activated, in the first 4.5sec, Rear Heated seats are operated. According to the spec, while Remst is active, Rear seats to be disabled
					// to fix it the IF statement was added

					  	if(((hs_nr==  FRONT_LEFT) || (hs_nr==FRONT_RIGHT)) ||  (((hs_nr == REAR_LEFT)||(hs_nr == REAR_RIGHT))&& (canio_RX_IgnRun_RemSt_c == FALSE)))
					  	{
							/*Switch requests from BUS. */
							hsLF_Process_Switch_Requests_Bus (hs_nr);
						}
						else
						{ 	/* Do nothing */
						}
						
					}
				}
				else
				{
					/* State is in Remote Start. DO the Remote start functionality */
					hsLF_RemoteStart_Control();
				}
				
			    /**************************************** STEP 6 ***********************************/
				/* New addition: MKS 61288: New calibration set */
				/* To get the actual Parameters based on Fronts or Rears */
				if ((hs_nr == REAR_LEFT)||(hs_nr == REAR_RIGHT))
				{
					hs_vehicle_line_offset_c= VEH_LINE_REAROFFSET;	
				}
				else
				{
					hs_vehicle_line_offset_c= VEH_LINE_FRONTOFFSET;
				}
			
			/* NTC > 70c Information/NTC Supply voltage Fault/ NTC Faults. */
				if (((NTC_AD_Readings_c[hs_nr] != 0) && (NTC_AD_Readings_c[hs_nr] < /*hs_NTC_feedback_values_c[4]*/hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].NTC_CutOFF_c[4])) ||
			    /*(NTC_Supply_Voltage_Flt_b) ||*/
			    (temperature_HS_NTC_status[hs_nr]!= NTC_STAT_GOOD)) {
				
				/* Get the odo meter information */
				TempOdo = FMemLibF_GetOdometer();
				
				/* New addition to the SW 08.21.02                                                                                                                                                                                                                                                                 */
				/* If we see the Temperature NTC condition, to turn OFF the o/ps and LEDs immediatly for Heaters, When ever the fault matures, we make sure the monitor counter reaches the 2sec. With this we will be not waiting extra additional 2sec wait time to turn OFF the LED for the first time the fault occurs. If fault goes away we clear the flag to make sure, again if fault set in the same IGN cycle we will follow the same process. */
				if (hs_control_flags_c[ hs_nr ].b.ntc_failure_first_time_b == FALSE) {
					
					/* We are at Special case. NTC >70c Pass this information to  2sec LED display logic */
					hs_control_flags_c[ hs_nr ].b.ntc_greater_70c_b = TRUE;

					/***************** START       MKS 50168   *****************************/
					/* New diagnostic service introduced to have > NTC Failsafe happens */
					/*Below logic is added by Harsha to make sure the diagnostic read 22 028B satisfies */
					if((NTC_AD_Readings_c[hs_nr] != 0) && (NTC_AD_Readings_c[hs_nr] < hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].NTC_CutOFF_c[4]))
					{
						/* Read the previously stored information from EEPROM */
						EE_BlockRead(EE_HEAT_NTC_FAILSAFE, (u_8Bit*)&hs_NTC_FailSafe_prms);

						/* Is data logged first time for this IGN cycle to EEP? */
						if(hs_ntc_updated_b == FALSE)
						{
							//Check to see Is this the first time we are writing into EEPROM					
							if(hs_NTC_FailSafe_prms.First_odostamp_w == 0xFFFF)
							{
								/* Write the first time happened ODO Stamp to eep*/
								hs_NTC_FailSafe_prms.First_odostamp_w = TempOdo;
							}
					
							/*Write the recent odo stamp when failure happened */
							hs_NTC_FailSafe_prms.recent_odostamp_w = TempOdo;
					
							/* Increment the No of times fault occured and Latch @254 */
							if(hs_NTC_FailSafe_prms.failure_counter_c != 254)
							{
								hs_NTC_FailSafe_prms.failure_counter_c++;
							}
					
							/* Update the contents to EEPROM */
							EE_BlockWrite(EE_HEAT_NTC_FAILSAFE, (u_8Bit*)&hs_NTC_FailSafe_prms);
					
							/* Set the flag, so we will not add duplicate information again for this IGN cycle */
							hs_ntc_updated_b = TRUE;
						}
					}
					/***************** END:       MKS 50168   *****************************/
					
					/*Check to see any switch requests are present */
					/* If present turn OFF the LEDs immediatly with out waiting for extra 2sec */
					/* To do this, assign the 2sec wait time to the variable */
					if(hs_rem_seat_req_c[hs_nr] != SET_OFF_REQUEST)
					{
					/* One time mature the counter for 2sec. */
					hs_monitor_2sec_led_counter_c[hs_nr] = /*mature_2sec_time_c*/hs_flt_dtc_c[HS_2SEC_MATURE_TIME];
					}

					/* Make sure we will be not comming into this loop again and again if fault still present */
					hs_control_flags_c[ hs_nr ].b.ntc_failure_first_time_b = TRUE;
				}
				else
				{
					//DO nothing
				}

			}
			else 
			{
				/* Clear the >70c variable Main state. */
				hs_control_flags_c[ hs_nr ].b.ntc_greater_70c_b = FALSE;

				/* Clear the flag to make sure process works if the fault happens the next time in same IGN cycle. */
				hs_control_flags_c[ hs_nr ].b.ntc_failure_first_time_b = FALSE;
				/* clear the flag, In same IGN cycle, If again happens we can add the Information to EEP */
				hs_ntc_updated_b = FALSE;

			 }

		} //End of NTC Status check
		else {
			//Wait untill we get the values from AD.

		}


	  } //End of Processing FOR Loop
		
	    /**************************************** STEP 7 ***********************************/
	    /**************************************** STEP 7 ***********************************/
		/* Look for Feedback values */
		/*hsLF_HeatDiagnostics();*/

	    /**************************************** STEP 8 ***********************************/
	    /**************************************** STEP 8 ***********************************/
		hsLF_2SecLEDDisplay();
	} //End of IGN Check condition
	else
	{
		/* STEP 9 */
		/* IGN Status is other than RUN/START. Turn OFF all outputs */
		hsLF_ClearHeatSequence();
	}

    pid_heat_setting();

} //End Of Heat Manager

/****************************************************************************************/
/*							          hsLF_OutputTests      							*/
/****************************************************************************************/
//void hsLF_OutputTests(unsigned char hs_nr)
//{
//
//
//	if ( (diag_rc_all_heaters_lock_b || diag_rc_all_op_lock_b) && 
//		(hs_status3_flags_c[ hs_nr ].b.hs_optests_started == FALSE)	)
//	{
//		/* Do not allow into regular switch logic */
//		hs_control_flags_c[ hs_nr ].b.hs_can_msg_flow_ctrl_b = TRUE;
//	
//		/* Update the Heat parameter values */
//		hsLF_UpdateHeatParameters(hs_nr, HEAT_LEVEL_HL3);
//		
//		diag_op_rc_status_c == TEST_IN_PROGRESS_HEATERS;
//		
//		hs_status3_flags_c[ hs_nr ].b.hs_optests_started = TRUE;
//		
//		counter_w = 0;
//	}
//	else
//	{
//		if (counter_w > 1000)
//		{
//		/* Allow into regular switch logic */
////		hs_control_flags_c[ hs_nr ].b.hs_can_msg_flow_ctrl_b = FALSE;
//		
//		/* 3sec time over, Update the Heat parameter values to OFF*/
//		hsLF_UpdateHeatParameters(hs_nr, HEAT_LEVEL_OFF);
//			if(diag_op_rc_status_c != TEST_ABORTED_HEATERS_FAULT)
//			{
//				diag_op_rc_status_c = TEST_COMPLETED_NO_HEATSEAT_ERRORS;
//				
//				diag_rc_all_heaters_lock_b = FALSE;
//				diag_rc_all_op_lock_b = FALSE;
////				hs_status3_flags_c[ hs_nr ].b.hs_optests_started = FALSE;
//			}
//			
//		}
//		else
//		{
//			counter_w++;
//		}
//		
//	}
//
//	
//}

/****************************************************************************************/
/*							hsLF_Process_DiagIO_RC_Outputs      						*/
/****************************************************************************************/
/* Diag IO controlling for Heater output logic                                           */
/* Step 1: If any of Io control lock bit or Self test lock bit set means,                */
/*         Logic enters inside STEP 1A.                                                  */
/*         Else goes to Step 2                                                           */
/* Step 1A:Get the request for the first time and do not allow the regular logic to      */
/*         turn ON or OFF the heaters                                                    */
/*         Process the diagnostic IO request and update the heat parameters accordingly  */
/* Step 1B:If internal IO Controlling flag is set means (First time after completing the */
/*         IO control), clear the internal IO flag, turn OFF the outputs and             */
/*         enable the regular logic                                                      */ 
void hsLF_Process_DiagIO_RC_Outputs(unsigned char hs_seats)
{
	//STEP 1
	if ((diag_io_hs_lock_flags_c[hs_seats].b.switch_rq_lock_b == TRUE) || (diag_io_all_hs_lock_b == TRUE) || (diag_rc_all_heaters_lock_b == TRUE))
	{
	    //STEP 1A:
		/* IO Controling Logic for Individual heat request or Actuate All Seats Logic  or */
		/* All seats Self tests automatic request */
		
		/* Check to see is request from IO Controlling or Routine control ? */
		if(diag_rc_all_heaters_lock_b == FALSE)
		{ 
			/* Request is through IO control */
			/* Set the flag to say that we are in IO control */
			hs_control_flags_c [hs_seats].b.hs_io_rq_control_b = TRUE;
			
			/* Take the value from IO Request */
			hs_IO_seat_req_c[ hs_seats ] = diag_io_hs_rq_c [hs_seats];

			/* Allow only once to process the IO untill we get new IO request	*/
			diag_io_hs_rq_c[hs_seats] = 7;

		}
		else
		{
			/* Request is through self test routine control */
			/* Set the flag to say that we are in Self test control */
			hs_status3_flags_c [hs_seats].b.self_tests_active_b = TRUE;
			
		}
		
		/* Do not allow into regular switch logic */
		hs_control_flags_c[ hs_seats ].b.hs_can_msg_flow_ctrl_b = TRUE;

		/* Is IO requested for HL3.. or Self tests requested? */
		if ((hs_IO_seat_req_c[ hs_seats ] == HS_DIAG_IO_RQ_HL3) || (diag_rc_all_heaters_lock_b == TRUE) )
		{
			/* Check to see, self tests already started? */
			/* If started, do not react for self tests, else react.*/
			/* or If it is diag IO control enter inside */
			if ( ((hs_status3_flags_c [hs_seats].b.tests_started_b == FALSE) && (diag_rc_all_heaters_lock_b == TRUE)) ||
				(hs_IO_seat_req_c[ hs_seats ] == HS_DIAG_IO_RQ_HL3) )
			{
				/* Front Seats are default ones */
				if(hs_seats == FRONT_LEFT || hs_seats == FRONT_RIGHT)
				{
					/* Update the Heat parameter values */
					hsLF_UpdateHeatParameters(hs_seats, HEAT_LEVEL_HL3);
					
					hs_status3_flags_c [hs_seats].b.tests_started_b = TRUE;
				}
				else
				{
					/* Enable the rear seat logic only in case Rear seats are equipped */
					if((CSWM_config_c & CSWM_4HS_VARIANT_MASK_K) == CSWM_4HS_VARIANT_MASK_K)
					{
						/* Update the Heat parameter values */
						hsLF_UpdateHeatParameters(hs_seats, HEAT_LEVEL_HL3);
						
						hs_status3_flags_c [hs_seats].b.tests_started_b = TRUE;
					}
				}
			
			}
		}
		else
		{
			/* Is IO requested for HL2 */
			if (hs_IO_seat_req_c[ hs_seats ] == HS_DIAG_IO_RQ_HL2)
			{
				/* Update the Heat parameter values */
				hsLF_UpdateHeatParameters(hs_seats, HEAT_LEVEL_HL2);
			}
			else
			{
				/* Is IO requested for HL1 */
				if (hs_IO_seat_req_c[ hs_seats ] == HS_DIAG_IO_RQ_HL1)
				{
					/* Update the Heat parameter values */
					hsLF_UpdateHeatParameters(hs_seats, HEAT_LEVEL_HL1);
				}
				else
				{
					/* Is IO requested for LL1 */
					if (hs_IO_seat_req_c[ hs_seats ] == HS_DIAG_IO_RQ_LL1)
					{
						/* Update the Heat parameter values */
						hsLF_UpdateHeatParameters(hs_seats, HEAT_LEVEL_LL1);
					}
					else
					{
						/* Is IO requested OFF */
						if (hs_IO_seat_req_c[ hs_seats ] == HS_DIAG_IO_RQ_OFF)
						{
							/* Update the Heat parameter values */
							hsLF_UpdateHeatParameters(hs_seats, HEAT_LEVEL_OFF);
							/*Clear the test started flag.*/
							hs_status3_flags_c [hs_seats].b.tests_started_b = FALSE;
						}
						else
						{
						//Do nothing
						}
					}
				}
			}
		}
	
	}
	else
	{
		//STEP 1B
		/*If first time logic comes here */
		if( (hs_control_flags_c [hs_seats].b.hs_io_rq_control_b == TRUE) || (hs_status3_flags_c [hs_seats].b.self_tests_active_b == TRUE) )
		{
			/* Though the diag control is completed, still the module is in UGLY State */
			/*When first time module returns control to ECU, clear all the parameters */
			/* Just comming out of IO Control. Make sure switch OFF all the outputs. */
			/*  Allows to enter once inside the switch logic.                        */
			hs_control_flags_c[ hs_seats ].b.hs_can_msg_flow_ctrl_b = FALSE;

			/* Update the Heat parameter values */
			hsLF_UpdateHeatParameters(hs_seats, HEAT_LEVEL_OFF);

			if(hs_control_flags_c [hs_seats].b.hs_io_rq_control_b == TRUE)
			{
				/* Clear the IO control flag. So we make sure that this logic works for only once. */
				hs_control_flags_c [hs_seats].b.hs_io_rq_control_b = FALSE;
				
				/* Clear the stored data from the uds diag test tool */
				hs_IO_seat_req_c[ hs_seats ] = 0;
				
			}
			else
			{
				/* Clear the Routine control active flag. So we make sure that this logic works for only once. */
				hs_status3_flags_c [hs_seats].b.self_tests_active_b = FALSE;
				
			}
			
			/* Clear the tests started flag. */
			hs_status3_flags_c [hs_seats].b.tests_started_b = FALSE;

		}
	
	}
} //End of Diag IO processing for Outputs

/****************************************************************************************/
/*							hsLF_Process_DiagIO_LEDStatus      							*/
/****************************************************************************************/
/* Diag IO controlling for Heater LED Status                                            */
/* Step 1: Look for any Diag IO lock set for Heater LED Status                          */
/*         If LED status LOCK is set Go to Step 3                                       */
/*         If LED status LOCK is not set got to Step 2                                  */ 
/* Step 2: Look for Internal flag is set for IO controlling                             */
/*         If cleared, do nothing                                                       */
/*         If not cleared go to step 2A                                                 */
/* Step 2A:Internal Flag was not cleared means we are first time comming out of         */
/*         LED IO control.                                                              */
/*         If it is for DS vehicle line, clear the LED status with respect to HSM_A1    */
/*         If it is for WK/LX vehicle lines, clear LED status with HSM_A1 as well as    */
/*         Hardwired switch status                                                      */
/*         Allow the regular logic to enable the switch requests                        */
/*         Clear the internal flag                                                      */
/* Step 3: Set the Internal flag to say that we are in IO control                       */
/*         Get the requested LED status to check from the IO control                    */
/*         Send the HSM_A1 status with the information received from IO control         */
/*         If vehicle line is WK/LX, and if the request is for rear seats               */
/*         send the information to Hard wired inputs also                               */
/*         Disable the regular heat switch logic                                        */
void hsLF_Process_DiagIO_LEDStatus (unsigned char hs_seat)
{
	//STEP 1
	/* Are we looking for the Heat seat status through IO controlling? */
	if (!diag_io_hs_lock_flags_c[hs_seat].b.led_status_lock_b)
	{
		//STEP 2
		/* Is logic just comming out of IO control? */
		if (!hs_control_flags_c [hs_seat].b.hs_io_stat_control_b)
		{
			/* Do nothing . We are not in IO control for LED status. */

		}
		else
		{
			//STEP 2A
			/* We are just out of IO for LED status. Clear OFF all the status */
			
			/*Check if the seats are Rears and processed to be with HW */
			if (((hs_seat == REAR_LEFT)||(hs_seat == REAR_RIGHT)) && (main_SwReq_Rear_c == VEH_SWITCH_REAR_ONLY_K))
			{
				/* Turn OFF all LED status to OFF for own HW */
				//hsLF_SendRearSeat_HWStatus(hs_seat, CAN_LED_OFF);
				/* Set the CAN output to Zero */
				hsLF_SendHeatedSeatCanStatus(hs_seat, CAN_LED_OFF);
				
			}
			else
			{
				/* Set the CAN output to Zero */
				hsLF_SendHeatedSeatCanStatus(hs_seat, CAN_LED_OFF);
			}
			
			/* Clear the IO control flag. So we make sure that this logic works for only once. */
			hs_control_flags_c [hs_seat].b.hs_io_stat_control_b = FALSE;

			/* Just comming out of IO Control. Make sure switch OFF all the outputs. */
			/*  Allows to enter once inside the switch logic.                        */
			hs_control_flags_c[ hs_seat ].b.hs_can_msg_flow_ctrl_b = FALSE;
		}
	}
	else
	{   
		//STEP 3
		/* IO Controlling Logic for FL Seat Status */
		/* Set the flag to say that we are in IO control for LED Status */
		hs_control_flags_c [hs_seat].b.hs_io_stat_control_b = TRUE;

		/* Get the value from the IO Controlling.         */
		/* 04 == HIGH; 02 == MEDIUM; 01 == LOW; 00 == OFF */
		hs_IO_seat_stat_c [hs_seat] = diag_io_hs_state_c [hs_seat];

		if (hs_IO_seat_stat_c [hs_seat] == HS_DIAG_IO_STATUS_HI)
		{
			/*Check if the seats are Rears and processed to be with HW */
			if (((hs_seat == REAR_LEFT)||(hs_seat == REAR_RIGHT)) && (main_SwReq_Rear_c == VEH_SWITCH_REAR_ONLY_K))
			{
				/* Turn HI LED status for own HW */
				//hsLF_SendRearSeat_HWStatus(hs_seat, CAN_LED_HI);
				/* Here we are setting the Heated seat status on Bus to HIGH */
				hsLF_SendHeatedSeatCanStatus(hs_seat, CAN_LED_HI);
				
			}
			else
			{
				/* Here we are setting the Heated seat status on Bus to HIGH */
				hsLF_SendHeatedSeatCanStatus(hs_seat, CAN_LED_HI);

			}
		}
		else
		{
			if (hs_IO_seat_stat_c [hs_seat] == HS_DIAG_IO_STATUS_LOW)
			{
				/*Check if the seats are Rears and processed to be with HW */
				if (((hs_seat == REAR_LEFT)||(hs_seat == REAR_RIGHT)) && (main_SwReq_Rear_c == VEH_SWITCH_REAR_ONLY_K))
				{
					/* Turn LOW LED status for own HW */
					//hsLF_SendRearSeat_HWStatus(hs_seat, CAN_LED_LOW);
					/* Here we are setting the Heated seat status on Bus to LOW */
					hsLF_SendHeatedSeatCanStatus(hs_seat, CAN_LED_LOW);
				}
				else
				{
					/* Here we are setting the Heated seat status on Bus to LOW */
					hsLF_SendHeatedSeatCanStatus(hs_seat, CAN_LED_LOW);
				}
			}
			else
			{
				/*Check if the seats are Rears and processed to be with HW */
				if (((hs_seat == REAR_LEFT)||(hs_seat == REAR_RIGHT)) && (main_SwReq_Rear_c == VEH_SWITCH_REAR_ONLY_K))
				{
					/* Turn OFF LED status to OFF for own HW */
					//hsLF_SendRearSeat_HWStatus(hs_seat, CAN_LED_OFF);
					/* Set the CAN Bus output to Zero */
					hsLF_SendHeatedSeatCanStatus(hs_seat, CAN_LED_OFF);
					
				}
				else
				{
					/* Set the CAN output to Zero */
					hsLF_SendHeatedSeatCanStatus(hs_seat, CAN_LED_OFF);
				}
			}
		}
		
		/* Do not allow into regular switch logic */
		hs_control_flags_c[ hs_seat ].b.hs_can_msg_flow_ctrl_b = TRUE;

	}

}

/****************************************************************************************/
/*								hsLF_Catch_Signals       								*/
/****************************************************************************************/
/* This function gets called from hsF_HeatManager                                       */
/* Catches the Switch requests from Bus                                                 */
/* Calls hsLF_Catch_Rear_Switch_Request incase switch requests are not from BUS         */
/* If Logic comes just out of IO Controlling for outputs, turns OFF the outputs         */
/* Each seat will look for two different kinds of signals                               */
/* Front Seats:																	        */
/*               All vehicle lines looks for TGW_A1 signal message from TGW             */
/* Rear Seats:																	        */
/*              All vehicle lines looks for direct switch requests from switches        */

void hsLF_Catch_Signals(unsigned char hs_side)
{
	
	/* Is logic comming just out of IO Control or through Normal Switch request. */
	if ((hs_control_flags_c [hs_side].b.hs_io_rq_control_b == FALSE) && (hs_status3_flags_c [hs_side].b.self_tests_active_b == FALSE))
	{
		switch (hs_side) {

			case FRONT_LEFT:
			{
				/* Check any diag IO happening for Vents */
				if (!diag_io_vs_lock_flags_c[hs_side].b.switch_rq_lock_b)
				{
					
					/* Not from IO Control. Get the value from TGW Node. */
					hs_temp_seat_req_fl_c = canio_RX_FL_HS_RQ_TGW_c;

//					/* Is Switch request to be from CCN */          
//					if (main_SwReq_Front_c == VEH_BUS_CCN_FRONT_REAR_K) {
//						/* Not from IO Control. Get the value from CCN Node. */
//						hs_temp_seat_req_fl_c = canio_L_F_HS_RQ_c;
//
//					}
//					else
//					{
//						/*Switch request is not From CCN. Check SwitcH request from HVAC */
//						if ((main_SwReq_Front_c == VEH_BUS_HVAC_FRONT_ONLY_K) ||(main_SwReq_Front_c == VEH_BUS_HVAC_FRONT_RAER_RT_K )) {
//							
//							/* Not from IO Control. Get the value from HVAC Node. */
//							hs_temp_seat_req_fl_c = canio_RX_FL_HS_RQ_TGW_c;
//
//						}
//						else
//						{
//							/* Logic should never come here */
//						}
//
//					}

				} //Diag IO check for Vents
				else
				{
					/* Diag IO for Vents ON. Do nothing for F R Seat */
				}
				break;

			} //End of Case FRONT_LEFT

			case FRONT_RIGHT:
			{
				
				/* Check any diag IO happening for Vents */
				if (!diag_io_vs_lock_flags_c[hs_side].b.switch_rq_lock_b)
				{
					
					/* Not from IO Control. Get the value from CCN Node. */
					hs_temp_seat_req_fr_c = canio_RX_FR_HS_RQ_TGW_c;

//					/* Is Switch request to be from CCN */          
//					if (main_SwReq_Front_c == VEH_BUS_CCN_FRONT_REAR_K)
//					{
//						/* Not from IO Control. Get the value from CCN Node. */
//						hs_temp_seat_req_fr_c = canio_R_F_HS_RQ_c;
//					}
//					else
//					{
//						/*Switch request is not From CCN. Check Switch request from HVAC */
//						if ((main_SwReq_Front_c == VEH_BUS_HVAC_FRONT_ONLY_K) ||(main_SwReq_Front_c == VEH_BUS_HVAC_FRONT_RAER_RT_K ))
//						{
//							/* Not from IO Control. Get the value from CCN Node. */
//							hs_temp_seat_req_fr_c = canio_RX_FR_HS_RQ_TGW_c;
//						}
//						else
//						{
//							/* Logic should never come here */
//						}
//					}
				} //Diag IO Check for Vents
				else
				{
					/* Diag IO for Vents ON. Do nothing for F R Seat */
				}
				break;
			} //End of Case FRONT_RIGHT

			case REAR_LEFT:
			{
				/* Process only if Rear Heaters Supported */
				if ((CSWM_config_c & CSWM_4HS_VARIANT_MASK_K) == CSWM_4HS_VARIANT_MASK_K)
				{
					/* Then the signal to be from Hardware. */
					hsLF_Catch_Rear_Switch_Requests(hs_side);
					
//					/* Is Switch request to be from CCN */          
//					if (main_SwReq_Rear_c == VEH_BUS_CCN_FRONT_REAR_K)
//					{
//						/* Not from IO Control. Get the value from CCN Bus. */
//						hs_temp_seat_req_rl_c = canio_L_R_HS_RQ_c;
//					}
//					else
//					{
//						if (main_SwReq_Rear_c == VEH_BUS_DM_REAR_K)
//						{
//								/* Not from IO Control. Get the value from DM RL Bus FTR00139665. */
//								hs_temp_seat_req_rl_c = canio_LR_HS_RQ_2_c;
//
//						}
//						else
//						{
//							/*Switch request is not From CCN. */
//							/* Then the signal to be from Hardware. */
//							hsLF_Catch_Rear_Switch_Requests(hs_side);
//						}
//							
//					}
				}

				break;
			}

			case REAR_RIGHT:
			{
				/* Process only if Rear Heaters Supported */
				if ((CSWM_config_c & CSWM_4HS_VARIANT_MASK_K) == CSWM_4HS_VARIANT_MASK_K)
				{
					
					/* Get the requests from Hardware Input. */
					hsLF_Catch_Rear_Switch_Requests(hs_side);

//					/* Is Switch request to be from CCN */          
//					if (main_SwReq_Rear_c == VEH_BUS_CCN_FRONT_REAR_K)
//					{
//						/* Not from IO Control. Get the value from CCN Bus. */
//						hs_temp_seat_req_rr_c = canio_R_R_HS_RQ_c;
//					}
//					else
//					{
//						if (main_SwReq_Rear_c == VEH_BUS_DM_REAR_K)
//						{
//								/* Not from IO Control. Get the value from DM RR Bus. FTR00139665 */
//								hs_temp_seat_req_rr_c = canio_RR_HS_RQ_2_c;
//
//						}
//						else
//						{
//							/*Switch request is not From CCN. */
//							/* Then the signal to be from Hardware. */
//							hsLF_Catch_Rear_Switch_Requests(hs_side);
//						}						
//					}
				}
				break;
			}
			
			default:
			{
				
			}
		}
	}
	else
	{
		/* Just comming out of IO Control. Make sure switch OFF all the outputs. */
		/*  Allows to enter once inside the switch logic.                        */
		hs_control_flags_c[ hs_side ].b.hs_can_msg_flow_ctrl_b = FALSE;

		/* Update the Heat parameter values */
		hsLF_UpdateHeatParameters(hs_side, HEAT_LEVEL_OFF);

//		/* Clear the IO control flag. So we make sure that this logic works for only once. */
//		hs_control_flags_c [hs_side].b.hs_io_rq_control_b = FALSE;
//
//		/* Clear the stored data from the uds diag test tool */
//		hs_IO_seat_req_c[ hs_side ] = 0;
		
		if(hs_control_flags_c [hs_side].b.hs_io_rq_control_b == TRUE)
		{
			/* Clear the IO control flag. So we make sure that this logic works for only once. */
			hs_control_flags_c [hs_side].b.hs_io_rq_control_b = FALSE;
			
			/* Clear the stored data from the uds diag test tool */
			hs_IO_seat_req_c[ hs_side ] = 0;
		}
		else
		{
			/* Clear the Routine control active flag. So we make sure that this logic works for only once. */
			hs_status3_flags_c [hs_side].b.self_tests_active_b = FALSE;
		}
		
		/* Clear the tests started flag. */
		hs_status3_flags_c [hs_side].b.tests_started_b = FALSE;

		/* Also make sure the Rear seats switch rq is in non pressed state, as variable Zero is like Pressed */
		/* Inverse logic: 0 means Pressed, 1 means Not pressed */
		if ((CSWM_config_c & CSWM_4HS_VARIANT_MASK_K) == CSWM_4HS_VARIANT_MASK_K)
		{
			/* Is Switch request to be from CCN */
			/* MKS 51868: (EOL Tests FRONT heated seats) failing for V2 and V6 variants for PN */
			/* The logic to be executed for REAR_LEFT and REAR_RIGHT not for all seats*/
			if (((hs_side == REAR_LEFT)||(hs_side == REAR_RIGHT))&& (main_SwReq_Rear_c == VEH_SWITCH_REAR_ONLY_K))
			{
				/* Rear switch If equipped for W/L series Initialization to not pressed state */
				hs_temp_seat_req_c[hs_side] = 0x01;
			}
		}


	}

} //End of Catch Signals function

/****************************************************************************************/
/*							hsLF_Catch_Rear_Switch_Requests								*/
/****************************************************************************************/
/* This function gets called from hsLF_Catch_Signals                                    */
/* Catches the Switch requests from HW                                                  */
/* This function looks for switch signals for rear seats for the vehicle lines W and L  */
/* As the switch requests are from direct switches logic implemented with debounce      */
/* Every 10ms time this function gets called                                            */
/* Logic waits for 5 consecutive LOW signals in duration of 50ms to determine the       */
/* Correct switch request                                                               */
void hsLF_Catch_Rear_Switch_Requests(unsigned char hs_rear)
{
	//Read the Rear U12S Pin status
	hs_rear_U12S_stat_c = (unsigned char) Dio_ReadChannel(LED_U12S_EN);

	switch (hs_rear) {

		case REAR_LEFT:
		{
			if (hs_rear_U12S_stat_c)
			{
				/* Get the Switch detection status for R L */ 
				//hs_temp_seat_req_rl_c = (unsigned char) Dio_ReadChannel(RL_LED_SW_DETECT);
				hs_rear_swth_stat_c[RL_SWITCH] = (unsigned char) Dio_ReadChannel(RL_LED_SW_DETECT);
				if(hs_rear_swth_stat_c[RL_SWITCH] == STD_LOW)
				{
					if(hs_bounce_counter_c[RL_SWITCH] >= 5)
					{
						hs_temp_seat_req_rl_c = STD_LOW;
						/*Informs to 22027D UDS service that, atleast once R L switch was pressed in this IGN cycle */
						hs_rear_swpsd_stat_c[RL_SWITCH] = 1; //newly added.
					}
					else
					{
						hs_bounce_counter_c[RL_SWITCH]++;
					}
					
				}
				else 
				{
					if(hs_bounce_counter_c[RL_SWITCH] > 0)
					{
						hs_bounce_counter_c[RL_SWITCH]--;
					}
					else
					{
						hs_temp_seat_req_rl_c = STD_HIGH;
					}
						
	
				}
		
			}
			break;
		}

		case REAR_RIGHT:
		{
			if (hs_rear_U12S_stat_c)
			{
				/* Get the Switch detection status for R L */ 
				//hs_temp_seat_req_rl_c = (unsigned char) Dio_ReadChannel(RL_LED_SW_DETECT);
				hs_rear_swth_stat_c[RR_SWITCH] = (unsigned char) Dio_ReadChannel(RR_LED_SW_DETECT);
				if(hs_rear_swth_stat_c[RR_SWITCH] == STD_LOW)
				{
					if(hs_bounce_counter_c[RR_SWITCH] >= 5)
					{
						hs_temp_seat_req_rr_c = STD_LOW;
						/*Informs to 22027D UDS service that, atleast once R R switch was pressed in this IGN cycle */
						hs_rear_swpsd_stat_c[RR_SWITCH] = 1;
					}
					else
					{
						hs_bounce_counter_c[RR_SWITCH]++;
					}
					
				}
				else 
				{
					if(hs_bounce_counter_c[RR_SWITCH] > 0)
					{
						hs_bounce_counter_c[RR_SWITCH]--;
					}
					else
					{
						hs_temp_seat_req_rr_c = STD_HIGH;
					}
						
	
				}
			}
			break;
		}
		
		default:
		{
			
		}
	}
}

/****************************************************************************************/
/*							hsLF_Process_Switch_Requests_Bus							*/
/****************************************************************************************/
/* This function gets executed every 10ms time slice.                                    */
/* Once logic receives the switch requests from Bus this function processes              */
/*  the requests accordingly                                                             */
/* For Regular series (D and W): HS_PSD is 4											 */
/* For Powernet series(L): HS_PSD is 1                                                   */
/* Step 1: Logic processes the switch requests if they are valid and                     */
/*         current switch request to be different from the previous request              */
/*         If the conditions are met, logic enters Step 2 to process the switch requests */
/*         If not logic waits at Step 4                                                  */
/* Step 2: As soon enters inside, sets the control bit, and stores the switch reuest     */
/*         value into the local variable(Like previous state of switch request).         */
/*         Step 4 clears the control bit to enter again step 2, only if the current      */
/*         switch request state is different than the previously stored value            */
/*         Logic looks for latest heated seat status on Bus                              */
/*         Depending on the heated seat status, the new switch request will update the   */
/*         next level of heated parameters explained in Step 3                           */
/* Step 3: Received current switch request 		current Seat status		 Next state      */
/*         OFF or SNA: 							xxx						 OFF             */
/*         HS_PSD:								OFF	                     HI              */
/*         HS_PSD                               HI                       LO              */
/*         HS_PSD                               LO                       OFF             */
/* Step 4: clears the control bit to enter again step 2, only if the current             */
/*         switch request state is different than the previously stored value and        */
/*         control bit was set                                                           */
/*                                                                                       */
/* Special case with PowerNet:                                                           */
/* If the switch request is pressed and current seat status is OFF, logic suppose to set */
/* HI PWM. But limitation is                                                             */                                                                              
/* If there are no output faults and no Loadshed 3/7 present and If Load shed 2 present  */
/* Heat level goes to LO level                                                           */  

void hsLF_Process_Switch_Requests_Bus (unsigned char hs_pos)
{
	//STEP 1
	/*SNA reaction Referrence MKS entry 15560 */
	/* Are the switch requests are valid and entering first time for the same request? */
	if ((hs_control_flags_c[ hs_pos ].b.hs_can_msg_flow_ctrl_b == FALSE) &&
	    ((hs_temp_seat_req_c [hs_pos] == HS_PSD) ||
	    (hs_temp_seat_req_c [hs_pos] == HS_OFF) ||
	    (hs_temp_seat_req_c [hs_pos] == HS_SNA)))
	{
		//STEP 2
		/* Switch requests are valid and processing the switch request */
		hs_control_flags_c[ hs_pos ].b.hs_can_msg_flow_ctrl_b = TRUE;

		/* Store the latest switch request to a variable */
		hs_switch_request_c [hs_pos] = hs_temp_seat_req_c [hs_pos];

		/* Get the latest Heated seat status from CAN Bus */
		hsLF_GetHeatedSeatCanStatus(hs_pos);

		/* New switch request processed. First clear the Timer */
		hsLF_Timer(hs_pos, TIMER_CLEAR);

		//STEP 3:
		/* Process the new switch request. Here at this moment we either will be having HS_OFF or HS_PSD conditions */
		switch (hs_switch_request_c [hs_pos]) {
			
			/* case HS_OFF */
			/* If the Switch Request is HS_OFF, no matter what the actual status is, we turn OFF the FET outputs and Turn OFF the LED status */
			case HS_OFF:
			case HS_SNA:
			{
				/* Update the Heat parameter values */
				hsLF_UpdateHeatParameters(hs_pos, HEAT_LEVEL_OFF);

				break;
			}
			
			/* case HS_PSD */
			case HS_PSD:
			{
				/* If the current state is OFF and if we get new switch request */
				if (hs_temp_seat_stat_c [hs_pos] == CAN_LED_OFF)
				{
//					/* Go to LOW LEVEL of heat if logic observes only Load shed 2 and No heater fault */
//					if( (hs_status2_flags_c[ hs_pos ].b.hs_output_fault_set_b == FALSE)&&
//					    (canio_PN14_LS_Lvl2_c == TRUE)&&
//					    (canio_PN14_LS_Lvl3_c == FALSE) && 
//					    (canio_PN14_LS_Lvl7_c == FALSE) )
//					{
//						/* Update the Heat parameter values to LOW LEVEL as load shed is 2 for LX*/
//						hsLF_UpdateHeatParameters(hs_pos, HEAT_LEVEL_LL1);
//					}
					/* Update the Heat parameter values */
					hsLF_UpdateHeatParameters(hs_pos, HEAT_LEVEL_HL3);
				}
				else
				{
					/* If the current state is HI and if we get new switch request */
					if (hs_temp_seat_stat_c [hs_pos] == CAN_LED_HI)
					{
						/* Update the Heat parameter values */
						hsLF_UpdateHeatParameters(hs_pos, HEAT_LEVEL_LL1);
					}
					else
					{
						/* The only remaining state is LOW. If we are In LOW State and receives a new switch request, we should be OFF. */
						hsLF_UpdateHeatParameters(hs_pos, HEAT_LEVEL_OFF);
					}
				}
				break;
			}
			
			default:
			{
				
			}
		}
		
		/* Store the current switch request to the variable. So in case if the faults were gone and we were in Monitoring time, we can re-activate our outputs. */
		hs_rem_seat_req_c[hs_pos] = hs_switch_request_to_fet_c[hs_pos];
	}
	else
	{
		//STEP 4
		/* IS the switch request changed from the last time? */
		if (hs_control_flags_c[ hs_pos ].b.hs_can_msg_flow_ctrl_b &&
		   hs_switch_request_c [hs_pos] != hs_temp_seat_req_c [hs_pos])
		{
			/* If so allow the handler to perform the new switch logic. */
			hs_control_flags_c[ hs_pos ].b.hs_can_msg_flow_ctrl_b = FALSE;
		}
	}
} //End of Switch request processing from BUS

/****************************************************************************************/
/*							hsLF_Process_Switch_Requests_ECU							*/
/****************************************************************************************/
/* This function gets executed every 10ms time slice.                                    */
/* Once logic receives the switch requests from direct switch this function processes    */
/*  the requests accordingly (Only for Rear seats)                                       */
/* For Regular series (W): STD_LOW is 0											         */
/* For Powernet series(L): STD_LOW is 0                                                  */
/* Step 1: Logic processes the switch requests if they are valid and                     */
/*         current switch request to be different from the previous request              */
/*         If the conditions are met, logic enters Step 2 to process the switch requests */
/*         If not logic waits at Step 4                                                  */
/* Step 2: As soon enters inside, sets the control bit, and stores the switch reuest     */
/*         value into the local variable(Like previous state of switch request).         */
/*         Step 4 clears the control bit to enter again step 2, only if the current      */
/*         switch request state is different than the previously stored value            */
/*         Logic looks for latest heated seat status on Bus                              */
/*         Depending on the heated seat status, the new switch request will update the   */
/*         next level of heated parameters explained in Step 3                           */
/* Step 3: Received current switch request 		current Seat status		 Next state      */
/*         STD_LOW:								OFF	                     HI              */
/*         STD_LOW                              HI                       LO              */
/*         STD_LOW                              LO                       OFF             */
/* Step 4: clears the control bit to enter again step 2, only if the current             */
/*         switch request state is different than the previously stored value and        */
/*         control bit was set                                                           */
/*                                                                                       */
/* Special case with PowerNet:                                                           */
/* If the switch request is pressed and current seat status is OFF, logic suppose to set */
/* HI PWM. But limitation is                                                             */                                                                              
/* If there are no output faults and no Loadshed 3/7 present and If Load shed 2 present  */
/* Heat level goes to LO level                                                           */  
void hsLF_Process_Switch_Requests_ECU (unsigned char hs_rears)
{
	//STEP 1
	/* Are the switch requests valid and entering first time for the same request? */
	if ((hs_control_flags_c[ hs_rears ].b.hs_can_msg_flow_ctrl_b == FALSE) &&
	    (hs_temp_seat_req_c [hs_rears] == STD_LOW) )
	{
		//STEP 2
		/* Switch requests are valid and processing the switch request */
		hs_control_flags_c[ hs_rears ].b.hs_can_msg_flow_ctrl_b = TRUE;

		/* Store the latest switch request to a variable */
		hs_switch_request_c [hs_rears] = hs_temp_seat_req_c [hs_rears];

		/* New switch request processed. First clear the Timer */
		hsLF_Timer(hs_rears, TIMER_CLEAR);

		//STEP 3
		/* Process the new switch request. Here at this moment we either will be having HS_OFF or HS_PSD conditions */
		if (hs_rear_seat_led_status_c[hs_rears] == CAN_LED_OFF)
		{
//			/* Go to LOW LEVEL of heat if logic observes only Load shed 2 and No heater fault */
//					if((hs_status2_flags_c[ hs_rears ].b.hs_output_fault_set_b == FALSE)&&
//						(canio_PN14_LS_Lvl2_c == TRUE)&&
//						(canio_PN14_LS_Lvl3_c == FALSE) && 
//						(canio_PN14_LS_Lvl7_c == FALSE))
//					{
//						/* Update the Heat parameter values to LOW LEVEL as load shed is 2 for LX*/
//						hsLF_UpdateHeatParameters(hs_rears, HEAT_LEVEL_LL1);
//					}
					/* Update the Heat parameter values */
					hsLF_UpdateHeatParameters(hs_rears, HEAT_LEVEL_HL3);
		}
		else
		{
			if (hs_rear_seat_led_status_c[hs_rears] == CAN_LED_HI)
			{
				/* Update the Heat parameter values */
				hsLF_UpdateHeatParameters(hs_rears, HEAT_LEVEL_LL1);
			}
			else
			{
				/* The only remaining state is LOW. If we are In LOW State and receives a new switch request, we should be OFF. */
				hsLF_UpdateHeatParameters(hs_rears, HEAT_LEVEL_OFF);
			}
		}
		/* Store the current switch request to the variable. So in case if the faults were gone and we were in Monitoring time, we can re-activate our outputs. */
		hs_rem_seat_req_c[hs_rears] = hs_switch_request_to_fet_c[hs_rears];
	}
	else
	{
		//STEP 4
		/* IS the switch request changed from the last time? */
		if (hs_control_flags_c[ hs_rears ].b.hs_can_msg_flow_ctrl_b &&
		   hs_switch_request_c [hs_rears] != hs_temp_seat_req_c [hs_rears])
		{
			/* If so allow the handler to perform the new switch logic. */
			hs_control_flags_c[ hs_rears ].b.hs_can_msg_flow_ctrl_b = FALSE;
		}
	}
} //End of Switch request processing from ECU

/****************************************************************************************/
/*								hsLF_RemoteStart_Control								*/
/****************************************************************************************/
/* Remote Start Functionality for Heaters.                                                                                                                      */
/*                                                                                                                                                              */
/* Inputs:                                                                                                                                                      */
/* Driver Side                                                                                                                                                  */
/* Ambient Temperature                                                                                                                                          */
/* User switch Request.                                                                                                                                         */
/* Remote Start Configuration Information from EEPROM.                                                                                                          */
/*                                                                                                                                                              */
/* If Remote Start active bit is set                                                                                                                            */
/*                                                                                                                                                              */
/* 1. Depending on the Driver side signal, either Front Left or Front Side Heater output will get activated during the Remote start state.                      */
/* 2. If Ambient Temperature is <10c, heaters will get activated automatically with LOW LEVEL PWM.(Either LL2 or LL1)                                           */
/* 3. If Ambient Temperature is > 30c Vents will be activated automatically.                                                                                    */
/* 4. If the Ambient Temp is >10c and <30c, state is Remote start active but User switch required to either turn ON Heaters/Vents.                              */
/* 5. Depending on the Remote Start configuration from EEPROM, Logic turns ON heaters with all the possible Heat Levels.                                        */
/*    Value 0x11 -> HL3 (Default configured. Tracker Id#44)                                                                                                     */
/*          0x22 -> HL2                                                                                                                                         */
/*          0x33 -> HL1                                                                                                                                         */
/*          0x44 -> LL1                                                                                                                                         */
/* Logic is sub devided into 4 categories.                                                                                                                      */
/*                                                                                                                                                              */
/*                                                                                                                                                              */
/* State 1: Finding the LHD or RHD                                                                                                                              */
/* State 2: Remote Start Heating Automatic                                                                                                                      */
/* State 3: Remote Start Venting Automatic                                                                                                                      */
/* State 4: Remote Start User switch Request                                                                                                                    */
/*                                                                                                                                                              */
/* State 1 and State 2:                                                                                                                                         */
/* Once Logic start automatically either State 1 or State 2, the next state is either Timeout (15min Timeout for Remote Start) or State 3.                      */
/*                                                                                                                                                              */
/* State 3:                                                                                                                                                     */
/* If Logic receives user switch request depending on the Remote start configuration from EEPROM and Heater previous status                                     */
/* Logic either turns ON/OFF the heaters.                                                                                                                       */
/*                                                                                                                                                              */
/* Changes to the function:(Tracker Id#44)                                                                                                                      */
/* Enabled the other side of heat functionality for the remote start functionality, but no automatic start,user interaction needed.                             */
/* Change reuqest says " During Remote start functionality allow full functionality of the passenger heated/vented seat through the heated/vent switch request" */
/*                                                                                                                                                              */
/* Calls from                                                                                                                                                   */
/* --------------                                                                                                                                               */
/* hsF_HeatManager :                                                                                                                                            */
/*    hsLF_RemoteStart_Control()                                                                                                                                */
void hsLF_RemoteStart_Control(void)
{
	unsigned char hs_auto_rem_c = 0; //pc-lint warning 644 eliminated.
	unsigned char hs_switch_rem_c = 0;
	unsigned char hs_seat;

    /**************************************** STEP 1 ***********************************/
    /**************************************** STEP 1 ***********************************/
	/* Check for Driver side. If it is default or Left hand Drive, make heated seat Automatic in Remote start for Front Left, else it is Front Right */
	/* If Front Left is automatic, then Front Right would be enabled for switch request in Remote Start.                                             */
	/* If Front Right is automatic, then Front Left would be enabled for switch request in Remote start                                              */

	if ((canio_LHD_RHD_c == LHD) ||(canio_LHD_RHD_c == 0x03))
	{
		/* Automatic functionality is for F L */
		hs_auto_rem_c = FRONT_LEFT;

		/* Tracker Id#44                                                          */
		/* other seat to be enabled for Switch request. In this case Front Right. */
		hs_switch_rem_c = FRONT_RIGHT;
	}
	else
	{
		if (canio_LHD_RHD_c == RHD)
		{
			/* Automatic functionality is for F R */
			hs_auto_rem_c = FRONT_RIGHT;

			/* Tracker Id#44                                                         */
			/* other seat to be enabled for Switch request. In this case Front Left. */
			hs_switch_rem_c = FRONT_LEFT;
		}
		else
		{
			//IMPOSSIBLE SITUATION. We should never come here.
		}
	} //End of Finding LHD or RHD
	
    /**************************************** STEP 2 ***********************************/
    /**************************************** STEP 2 ***********************************/
	/* Remote Start Automatic for Heaters */
	/* We are in Remote Control Phase.                                           */
	/* Check to see is this REMOTE START AUTOMATIC or REMOTE START USER REQUEST. */
	/* FTR00139669 -- Autostart feature for PowerNet Series.*/
	if ((main_state_c == REM_STRT_HEATING) || (main_state_c == AUTO_STRT_HEATING) )
	{
		/* Depending on the Driver Position either we start Front Left or Front Right           */
		/* Once we start by automatic the next state is either user siwtch request or time out. */
		if (hs_control_flags_c[ hs_auto_rem_c ].b.remote_user_b == FALSE)
		{
			/* New switch request processed. First clear the Timer */
			hsLF_Timer(hs_auto_rem_c, TIMER_CLEAR);

//			/* Configured to start with HL3? */
//			if ( hs_remote_start_cfg_c == HS_RMT_START_HL3)
//			{

//				/* Go to LOW LEVEL of heat if logic observes only Load shed 2 and No heater fault */
//				if((hs_status2_flags_c[ hs_auto_rem_c ].b.hs_output_fault_set_b == FALSE)&&
//					    (canio_PN14_LS_Lvl2_c == TRUE)&&
//					    (canio_PN14_LS_Lvl3_c == FALSE) && 
//					    (canio_PN14_LS_Lvl7_c == FALSE))
//				{
//					/* Update the Heat parameter values to LOW LEVEL as load shed is 2 for LX*/
//					hsLF_UpdateHeatParameters(hs_auto_rem_c, HEAT_LEVEL_LL1);
//				}
				/* Update the Heat parameter values */
				hsLF_UpdateHeatParameters(hs_auto_rem_c, HEAT_LEVEL_HL3);
//			}
			
			/* Go to the next state, i.e user switch request for Remote Start Automatic Heater side */
			hs_control_flags_c[ hs_auto_rem_c ].b.remote_user_b = TRUE;
		}
		else
		{
			//We are in next state. Do nothing
		}
		
		/* Tracker Id#44                                                                                                                                                                      */
		/* Look for other heat side which is not with Auto remote start. After Auto start happens for Heater, the next state for the other heated seat is the Switch Request control by user. */
		if (hs_control_flags_c[ hs_switch_rem_c ].b.remote_user_b == FALSE)
		{
			/* Go to the next state, i.e user switch request for Remote start other heater side */
			hs_control_flags_c[ hs_switch_rem_c ].b.remote_user_b = TRUE;
		}
	}
	else
	{
		/* Either we already started with Remote start Automatic or we are in state of Remote Start user */
		/* Go to the next level                                                                          */
	}
    /**************************************** STEP 3 ***********************************/
    /**************************************** STEP 3 ***********************************/
	/* Remote Start Automatic for Vents */
	/* We are in Remote Start Automatic for Venting.                                                                                          */
	/* based on the Driver State, either Vents tun ON the F L or F R with HIGH Level 100% PWM.                                                */
	/* Even when we are in Remote start venting, if user gives switch request for Heating , we should accept and should go to the HIGH Level. */
	/* Check to see is this Remote Start Venting                                                                                              */
	/* FTR00139669 -- Autostart feature for PowerNet Series.*/
	if ((main_state_c == REM_STRT_VENTING)  || (main_state_c == AUTO_STRT_VENTING))
	{
		/* As automatic start happend for vents, now enable the user control for the seat. */
		if (hs_control_flags_c[ hs_auto_rem_c ].b.remote_user_b == FALSE)
		{
			hs_control_flags_c[ hs_auto_rem_c ].b.remote_user_b = TRUE;
		}
		else
		{
			//We are already in Remote user state. Do nothing
		}
		
		/* Tracker Id#44                                                                                                                                                                     */
		/* Look for other heat side which is not with Auto remote start. After Auto start happens for vents, the next state for the other heated seat is the Switch Request control by user. */
		if (hs_control_flags_c[ hs_switch_rem_c ].b.remote_user_b == FALSE)
		{
			/* Go to the next state, i.e user switch request for Remote start other heater side */
			hs_control_flags_c[ hs_switch_rem_c ].b.remote_user_b = TRUE;
		}
	}
	else
	{
		/* Either we already started with Remote start Automatic or we are in state of Remote Start user */
		/* Go to the next level                                                                          */
	}
    /**************************************** STEP 4 ***********************************/
    /**************************************** STEP 4 ***********************************/
	/* Remote Start User switch/User Switch after Remote Heater Automatic/User switch after Remote Vent Automatic */
	/* Check to see are we at Remote start user switch press or        */
	/* After Remote start automatic.                                   */
	/* In both cases we are ready to accept switch requests from user. */
	/* FTR00139669 -- Autostart feature for PowerNet Series.*/
	if ( (main_state_c == REM_STRT_USER_SWTCH)||
		 (main_state_c == AUTO_STRT_USER_SWTCH) || 
	     (hs_control_flags_c[ hs_auto_rem_c ].b.remote_user_b == TRUE) ||
	     (hs_control_flags_c[ hs_switch_rem_c ].b.remote_user_b == TRUE) )
	{

		/* Under Remote Start, we only enable Front Left and Front Right Heaters. */
		for (hs_seat = FRONT_LEFT; hs_seat <=  FRONT_RIGHT; hs_seat++)
		{
			hsLF_Process_Switch_Requests_Bus(hs_seat);

		} //End of For LOOP
		/* FTR00139669 -- Autostart feature for PowerNet Series. For autostart rear heaters are enable*/

		if ( (main_state_c == AUTO_STRT_USER_SWTCH) || (main_state_c == AUTO_STRT_VENTING) || (main_state_c == AUTO_STRT_HEATING) )
		{
			/* MKS 52700: When Rear seat stuck switch present and we are in Auto feature */
			/* Next POR logic enabling the Outputs,according to spec logic should not enable the */
			/* Rear seats which has active DTC for Stuck switch */
			/* For power Net, Logic will be here always for switch requests to PN */
			/* Commented the code and added the following code to satisfy the spec */
//			/* Under Remote Start, we only enable Front Left and Front Right Heaters. */
//		 	for (hs_nr = REAR_LEFT; hs_nr <=  REAR_RIGHT; hs_nr++)
//			{
//				hsLF_Process_Switch_Requests_ECU(hs_nr);
//			}
			
			/* The following code added by Harsha on 06/18/2010 */
			//MKS 21852
			//Accept hardwired switch inputs only if there are no stuck switch faults
			//Logic looks for actual eeprom information to see the Rear switch stuck DTC status */
			//If the DTCs are adjusted in FaultMemoryLib.h, the index needs to be adjusted here for Rear stuck DTCs */
			if(hs_seat == REAR_LEFT && 
			   hs_rear_seat_flags_c[ RL_SWITCH ].b.stksw_psd == FALSE &&
			   d_AllDtc_a[REAR_LEFT_STUCK_INDEX].DtcState.state.TestFailed == FALSE)
			{
				/*Switch requests from BUS. */
				hsLF_Process_Switch_Requests_ECU (REAR_LEFT);
			}
			//Accept hardwired switch inputs only if there are no stuck switch faults
			if(hs_seat == REAR_RIGHT && 
			   hs_rear_seat_flags_c[ RR_SWITCH ].b.stksw_psd == FALSE &&
			   d_AllDtc_a[REAR_RIGHT_STUCK_INDEX].DtcState.state.TestFailed == FALSE)
			{
				/*Switch requests from BUS. */
				hsLF_Process_Switch_Requests_ECU (REAR_RIGHT);
			}


		}
		else
		{
	    	//Do nothing
		}
	} //End of Remote start User Switch if condition

} //End of Remote Start Control function

/****************************************************************************************/
/*								hsF_MonitorHeatTimes    								*/
/****************************************************************************************/
/* This function will be called cyclically to check any timeouts happened for the particular heat PWM level, as well suspend the time counters in case of any preliminary fault condition arises. */
/* And resume the time count once module comes out from Preliminary fault.                                                                                                                        */
/*                                                                                                                                                                                                */
/* CSWM has 2 timeouts in particular.                                                                                                                                                             */
/* 1. Actual timeout for the particular Level                                                                                                                                                     */
/* 2. Timeout for checking the NTC feedbacks and taking decision to move to next available level.                                                                                                 */
/* HL3: 10min with  100% PWM  2min wait time to check NTC feedbacks                                                                                                                               */
/* HL2: 20min with   50% PWM  2min wait time to check NTC feedbacks                                                                                                                               */
/* HL1: 15min with   25% PWM 12min wait time to check NTC feedbacks                                                                                                                               */
/* LL1: 45min with   10% PWM  Runs for 45min                                                                                                                                                      */
/*                                                                                                                                                                                                */
/* Stepdown with Actual Timeout:                                                                                                                                                                  */
/* 1. HL3 -> HL2 (After Completing 10 min time)                                                                                                                                                   */
/* 2. HL2 -> HL1 (After completing 20 min time)                                                                                                                                                   */
/* 3. HL1 -> LL1 (After completing 15 min time)                                                                                                                                                   */
/* 4. LL1 -> OFF (After completing 15 min time)                                                                                                                                                   */
/*                                                                                                                                                                                                */
/* Stepdown with NTC Feedback Timeout: (Tracker Id#61)                                                                                                                                            */
/* 1. HL3 -> HL2 (After Completing 2 min time and NTC feedback is >55C)                                                                                                                           */
/* 2. HL2 -> HL1 (After completing 2 min time and NTC feedback is >55C)                                                                                                                           */
/* 3. HL1 -> LL1 (After completing 12 min time and NTC feedback is >49C)                                                                                                                          */
/* 4. LL1 -> OFF (After completing 15 min time)                                                                                                                                                   */
/*                                                                                                                                                                                                */
/* OFF Heaters Logic:                                                                                                                                                                             */
/* 1. HL3 -> OFF (If NTC > 70c, Immediate)                                                                                                                                                        */
/* 2. HL2 -> OFF (If NTC > 70c, Immediate)                                                                                                                                                        */
/* 3. HL1 -> OFF (If NTC > 70c, Immediate)                                                                                                                                                        */
/* 4. LL1 -> OFF (If NTC > 70c, Immediate)                                                                                                                                                        */
/*                                                                                                                                                                                                */
/* Calls from                                                                                                                                                                                     */
/* --------------                                                                                                                                                                                 */
/* main :                                                                                                                                                                                         */
/*    hsF_MonitorHeatTimes()                                                                                                                                                                      */
/*                                                                                                                                                                                                */
/* Calls to                                                                                                                                                                                       */
/* --------                                                                                                                                                                                       */
/* hsLF_Timer(unsigned char hs_nr,unsigned char action_c)                                                                                                                                         */
/* hsLF_UpdateHeatParameters()                                                                                                                                                                    */
@far void hsF_MonitorHeatTimes(void)
{
	
	unsigned char hs_seat,hs_const_c;
	
	for (hs_seat = FRONT_LEFT; hs_seat < ALL_HEAT_SEATS; hs_seat++)
	{
		/*MKS 52379: Preliminary Partial OPEN flag condition taken out from the IF condition */
		/*Customer requested to have stepdown sequence running, once Partial OPEN detection starts */
		/* Allow logic inside if there are no preliminary faults and Main CSWM status is good to start counting the timeout */
		if ((main_CSWM_Status_c == GOOD) &&
		    (!hs_status_flags_c[ hs_seat ].b.hs_prelim_short_gnd_b) &&
		    (!hs_status_flags_c[ hs_seat ].b.hs_prelim_short_bat_b) &&
		    (!hs_status_flags_c[ hs_seat ].b.hs_prelim_open_b)    /*  &&
		    (!hs_status_flags_c[ hs_seat ].b.hs_prelim_partial_open_b)*/)
		{
			/* If timer started for any of the seats, logic go inside */
			if (hs_control_flags_c[ hs_seat ].b.hs_start_timer_b)
			{
				
				/* NTC Feedback check logic after wait time */
				/* Do the NTC Check only for HL3/HL2/HL1 */
//				if (hs_status2_flags_c[hs_seat].b.hs_LL1_set_b == FALSE)
//				{
					/* Check to see, is the wait time over and crossed the NTC threshold for the particular level */
					/* Changed for Heated Seat Minimum Parameters */
					if ( (hs_time_counter_w[ hs_seat ] >= (hs_wait_time_check_NTC_c[ hs_seat ] * 187)) && 
					     (NTC_AD_Readings_c[hs_seat] <= hs_allowed_NTC_c[hs_seat]) )
					{
						
						/* Wait time is over and Feedback readings are not in aloowed range. */
						/* Increment the timer to complete the level.                        */
						hs_time_counter_w[ hs_seat ] = (hs_allowed_heat_time_c[ hs_seat ] * 187);
					}
					else
					{
						/* Do Nothing */
					}
//				}
//				else
//				{
//					/* For Low Level there is no NTC threshold defined at this moment. So it needs to go full time out. */
//				}
				
				/* Heater Timeout Logic */
				/* Is timeout occured for the first */
				if (hs_time_counter_w[ hs_seat ] >= (hs_allowed_heat_time_c[ hs_seat ] * /*3 *60*/187))
				{
					/* Is timeout happend for the HL3? */
					if (hs_status2_flags_c[hs_seat].b.hs_HL3_set_b)
					{
						/* Clear the flag as we completed the HL3 */
						hs_status2_flags_c[hs_seat].b.hs_HL3_set_b = FALSE;
						
						/* Case 2:                                   */
						/* Update the Heat parameter values with HL2 */
						hsLF_UpdateHeatParameters(hs_seat, HEAT_LEVEL_HL2);
					}
					else
					{
						/* Is time out happened for HL2? This is a special case. No temp feedback. Directly go to HL1 */
						if (hs_status2_flags_c[hs_seat].b.hs_HL2_set_b)
						{
							/* Clear the flag as we completed the HL2 */
							hs_status2_flags_c[hs_seat].b.hs_HL2_set_b = FALSE;
							
							/* Update the Heat parameter values with HL1*/
							hsLF_UpdateHeatParameters(hs_seat, HEAT_LEVEL_HL1);
						}
						else
						{
							/* Is timeout occured for HL1? */
							if (hs_status2_flags_c[hs_seat].b.hs_HL1_set_b)
							{
								/* Clear the flag as we completed the HL1 */
								hs_status2_flags_c[hs_seat].b.hs_HL1_set_b = FALSE;
								
								/* Update the Heat parameter values with LL1 */
								hsLF_UpdateHeatParameters(hs_seat, HEAT_LEVEL_LL1);
							}
							else
							{
								/* Is timeout occured for LL1 */
								if (hs_status2_flags_c[hs_seat].b.hs_LL1_set_b)
								{
									/* Clear the flag as we completed the LL1 */
									hs_status2_flags_c[hs_seat].b.hs_LL1_set_b = FALSE;
									
									/* Update the Heat parameter values with OFF*/
									hsLF_UpdateHeatParameters(hs_seat, HEAT_LEVEL_OFF);
								}
								else
								{
									/* We passed all the scenarios.Do nothing */
								}
							}
						}
					}

					/* Store the current switch request to the variable. So in case if the faults were gone and we were in Monitoring time, we can re-activate our outputs. */
					hs_rem_seat_req_c[hs_seat] = hs_switch_request_to_fet_c[ hs_seat ];
				}
				else
				{
					/* Increment the timer */
					hs_time_counter_w[ hs_seat ]++;
				}
			}
			else
			{
				/* Heaters were not started. Do nothing */
			}
		}
		else
		{
			//Do not enter inside the logic
		}
	}
}

/****************************************************************************************/
/*								hsLF_HeatDiagnostics    								*/
/****************************************************************************************/
/* This function will do the diagnsotics for all the four seats.                                                                    */
/*                                                                                                                                  */
/* Step 1: Initial condition to check the diagnostics is that Main_CSWM_Status should be in GOOD state.                             */
/*                                                                                                                                  */
/* Once we see the state GOOD, we proceed further, else logic will not proceed to do the fault detection.                           */
/*                                                                                                                                  */
/* Step 2: If there is any switch request, we do Open, Partial Open, Short to GND/Over load fault detection.                        */
/* If we already matured any of OPEN/PARTIAL OPEN/SHORT TO GND faults, we do not do fault detection for the current Ignition cycle. */
/* Step 2A: Again we check here are there any internal faults set, while doing the Fault detection when switch was pressed.         */
/* With this check we can eliminate the multiple faults for the case of internal faults.                                            */
/* Step 2B: Check If Vsense is less than the threshold or check Isense is greater than the threshold.                               */
/* If any of the condition is true, we set the Preliminary SHORT TO GND Fault for seats.                                            */
/* Step 2C: If Isense is less than the Full open threshold, we set Preliminary OPEN CIRCUIT fault for Heaters.                      */
/* Step 2D: If Isense is between Full open and Partial Open threshold, we set Preliminary PARTIAL OPEN CIRCUIT fault for Heaters.   */
/* Step 2E: If all the readings are OK, we clear all the preliminary faults.                                                        */
/*                                                                                                                                  */
/* Step 3: If there is no switch request, we do the Short to BAT detection.                                                         */
/* Step 3A: Front Seats Short to Bat detection.                                                                                     */
/* Do the fault detection only incase If we did not blow any Short to BAT faults for Front Heatersand Relay A is good.              */
/* Step 3B: Check the Vsense value is greater than the Threshold                                                                    */
/* If yes we set Preliminary SHORT TO GND fault for Front Heaters.                                                                  */
/* If all readings are in range, we clear the Preliminary Fault flag.                                                               */
/*                                                                                                                                  */
/* Step 3C: Rear Seats Short to Bat detection.                                                                                      */
/* Do the fault detection only incase If we did not blow any Short to BAT faults for Rear Seats and Relay B is good.                */
/* Step 3D: Check the Vsense value is greater than the Threshold                                                                    */
/* If yes we set Preliminary SHORT TO GND fault for Rear Heaters.                                                                   */
/* If all readings are in range, we clear the Preliminary Fault flag.                                                               */
/*                                                                                                                                  */
/* Logic proceeds further to "hsLF_2SecMonitorTime_FaultDetection"                                                                  */
/* This function allows to wait 2sec before maturing the final faults.                                                              */
/*                                                                                                                                  */
/* Calls from                                                                                                                       */
/* --------------                                                                                                                   */
/* hsF_HeatManager :                                                                                                                */
/*    hsLF_HeatDiagnostics()                                                                                                        */
/*                                                                                                                                  */
/* Calls to                                                                                                                         */
/* --------                                                                                                                         */
/* hsLF_2SecLEDDisplay()                                                                                                            */
void hsLF_HeatDiagnostics(void)
{
	/* This function will be monitoring for Heated seat output diagnsotic values Vdiag and Isense in case heaters are ON */
	unsigned char hs_seat;
	
	/* Step 1: If the Main Status is good and and if there are no mature faults for the heater outputs, we do the fault detection */
	if (main_CSWM_Status_c == GOOD)
	{
		for (hs_seat = FRONT_LEFT; hs_seat < ALL_HEAT_SEATS; hs_seat++)
		{
			//Clear the Ugly status Bit
			hs_status3_flags_c[ hs_seat ].b.first_time_ugly_b = FALSE;
			//hs_PWM_off_counter_c = 0;
//			if (/*main_CSWM_status_c == GOOD*/1)
//			{
				/* Step 2: If any of the Heated seats are active and if we do not have any matured faults , we do fault detection for Open, partial open, Short to GND and Over current, else we do fault detection for Short to BAT */
				if ( (hs_status2_flags_c[ hs_seat ].b.hs_heater_state_b)  && 
				     (!hs_status_flags_c[ hs_seat ].b.hs_final_short_gnd_b) &&
				     (!hs_status_flags_c[ hs_seat ].b.hs_final_open_b)      &&
				     (!hs_status_flags_c[ hs_seat ].b.hs_final_partial_open_b) )
				{
					/* Step 2A: Do diagnostics only if there are no internal faluts present.                                          */
					/* This helps not blowing other faults in case we are good at beginning and while running we get Internal fautls. */
					if ((((hs_seat == FRONT_LEFT) || (hs_seat == FRONT_RIGHT)) && (!relay_A_internal_Fault_b)) ||
					   (((hs_seat == REAR_LEFT) || (hs_seat == REAR_RIGHT)) && (!relay_B_internal_Fault_b && !Rear_Seats_no_power_prelim_b)))
					{
						/* Check if Vsense is O.K and Isense is OK. */
						if ((hs_Vsense_c[hs_seat] >= hs_flt_dtc_c[HS_STG_VSENSE]) &&
						    (hs_Isense_c[hs_seat] < hs_flt_dtc_c[HS_OVERLOAD_ISENSE]))
						{
							/* Step 2C: Check Isense for detecting Prelim Open Load */
							if (hs_Isense_c[hs_seat] < hs_flt_dtc_c[HS_FULL_OPEN_ISENSE])
							{
								/* Open Load */
								/* Set the Preliminary fault for the Open Load condition */
								hs_status_flags_c[ hs_seat ].b.hs_prelim_open_b = TRUE;
								
								/* Clear the Preliminary fault for the Partial Open Load condition */
								hs_status_flags_c[ hs_seat ].b.hs_prelim_partial_open_b = FALSE;
								
								/* Clear the Preliminary fault for the Short to GND condition */
								hs_status_flags_c[ hs_seat ].b.hs_prelim_short_gnd_b = FALSE;
							}
							else
							{

								/* Partial Open different from Front to Rear Seats Change Request ID 32456*/
								/* First Check Front Seats*/
							   if ((hs_seat == FRONT_LEFT) || (hs_seat == FRONT_RIGHT))
							   	{
									/* Step 2D: Check ISense for detecting Prelim Partial Open Load Front Seats CR 32456*/
									if (hs_Zsense_c[hs_seat] >= 70)
									{
										/* Partial Open Load */
										/* Set the Preliminary fault for the Partial Open Load condition */
										hs_status_flags_c[ hs_seat ].b.hs_prelim_partial_open_b = TRUE;
									
										/* Clear the Preliminary fault for the Open Load condition */
										hs_status_flags_c[ hs_seat ].b.hs_prelim_open_b = FALSE;
									
										/* Clear the Preliminary fault for the Short to GND condition */
										hs_status_flags_c[ hs_seat ].b.hs_prelim_short_gnd_b = FALSE;
										
										/* MKS 52379: When Partial OPEN detection starts, If module at HL3/HL2 */
										/* step down to HL1 */
										if( (hs_status2_flags_c[hs_seat].b.hs_HL3_set_b) ||
											(hs_status2_flags_c[hs_seat].b.hs_HL2_set_b)	)
										{
											hsLF_UpdateHeatParameters(hs_seat, HEAT_LEVEL_HL1);
										}
										
									}
									else
									{
										/*  Step 2E: No Faults */
										/* No fault for heats */
										hs_status_flags_c[ hs_seat ].b.hs_prelim_short_gnd_b = FALSE;
										hs_status_flags_c[ hs_seat ].b.hs_prelim_open_b = FALSE;
										hs_status_flags_c[ hs_seat ].b.hs_prelim_partial_open_b = FALSE;
										hs_status_flags_c[ hs_seat ].b.hs_prelim_short_bat_b = FALSE;
									}

							   }
							   else
								{
									/* Else  Rear Seats  Different threshold*/
									if (hs_Zsense_c[hs_seat] >= 80)
									{
										/* Partial Open Load */
										/* Set the Preliminary fault for the Partial Open Load condition */
										hs_status_flags_c[ hs_seat ].b.hs_prelim_partial_open_b = TRUE;
									
										/* Clear the Preliminary fault for the Open Load condition */
										hs_status_flags_c[ hs_seat ].b.hs_prelim_open_b = FALSE;
									
										/* Clear the Preliminary fault for the Short to GND condition */
										hs_status_flags_c[ hs_seat ].b.hs_prelim_short_gnd_b = FALSE;
										
										/* MKS 52379: When Partial OPEN detection starts, If module at HL3/HL2 */
										/* step down to HL1 */
										if( (hs_status2_flags_c[hs_seat].b.hs_HL3_set_b) ||
											(hs_status2_flags_c[hs_seat].b.hs_HL2_set_b)	)
										{
											hsLF_UpdateHeatParameters(hs_seat, HEAT_LEVEL_HL1);
										}

									}
									else
									{
										/*  Step 2E: No Faults */
										/* No fault for heats */
										hs_status_flags_c[ hs_seat ].b.hs_prelim_short_gnd_b = FALSE;
										hs_status_flags_c[ hs_seat ].b.hs_prelim_open_b = FALSE;
										hs_status_flags_c[ hs_seat ].b.hs_prelim_partial_open_b = FALSE;
										hs_status_flags_c[ hs_seat ].b.hs_prelim_short_bat_b = FALSE;
									}

								}


							}

						}
						else
						{
							/* Step 2B: Check Isense for detecting the preliminary overload */
							/* /Short to GND                                                */
							/* Set the Preliminary fault for the Short to GND condition */
							hs_status_flags_c[ hs_seat ].b.hs_prelim_short_gnd_b = TRUE;
							
							/* Clear the Preliminary fault for the Open Load condition */
							hs_status_flags_c[ hs_seat ].b.hs_prelim_open_b = FALSE;
						}
					}
					else
					{
						/* Clear immediatly the fault monitor counter incase if we see the internal faults. */
						/* Clear the monitor time counter                                                   */
						hs_monitor_prelim_fault_counter_c[hs_seat] = 0;
						
						/* No fault for heats */
						hs_status_flags_c[ hs_seat ].b.hs_prelim_short_gnd_b = FALSE;
						hs_status_flags_c[ hs_seat ].b.hs_prelim_open_b = FALSE;
						hs_status_flags_c[ hs_seat ].b.hs_prelim_partial_open_b = FALSE;
						hs_status_flags_c[ hs_seat ].b.hs_prelim_short_bat_b = FALSE;
					}
				}
				else
				{
					/* Step 3: Short to BAT Preliminary detection */
					/* Step 3A: Check to see the checking is for F L or F R */
					if ((hs_seat == FRONT_LEFT) || (hs_seat == FRONT_RIGHT))
					{
						/* Step 3A: Enter inside the fault detection logic only if there is no short to bat detected and Relay A is good. */
						if ((!hs_status_flags_c[ hs_seat ].b.hs_final_short_bat_b) && (!relay_A_internal_Fault_b))
						{
							/* Step 3B: Check the voltage feedback.*/
							/*Also check for any FET fault flag is set */
							/* If voltage is HIGH and no FET Faults, set the preliminary STB flag */
							if ((hs_Vsense_c[hs_seat] > /*hs_STB_Vsense_cutoff_c*/hs_flt_dtc_c[HS_STB_VSENSE]) && (!relay_F_FET_fault_b))
							{
								/* Set the Preliminary fault for the Short to BAT condition */
								hs_status_flags_c[ hs_seat ].b.hs_prelim_short_bat_b = TRUE;
							}
							else
							{
								/* Clear the Prelim fault */
								hs_status_flags_c[ hs_seat ].b.hs_prelim_short_bat_b = FALSE;
							}
						}
						else
						{
							/* Either Short to BAT for particular heater                                        */
							/* detected or Relay A fault happened. Do not do short to BAT for paricular heater. */
						}
					}
					else
					{
						/* Step 3C: Now check for remaining heaters R L and R R                                                  */
						/* Enter inside the fault detection logic only if there is no short to bat detected and Relay A is good. */
						if ((!hs_status_flags_c[ hs_seat ].b.hs_final_short_bat_b) && (!relay_B_internal_Fault_b))
						{
							/* Step 3D: Check the voltage feedback */
							if ( (hs_Vsense_c[hs_seat] > /*hs_STB_Vsense_cutoff_c*/hs_flt_dtc_c[HS_STB_VSENSE]) && (!relay_R_FET_fault_b))
							{
								/* Set the Preliminary fault for the Short to BAT condition */
								hs_status_flags_c[ hs_seat ].b.hs_prelim_short_bat_b = TRUE;
							}
							else
							{
								/* Clear the Prelim fault */
								hs_status_flags_c[ hs_seat ].b.hs_prelim_short_bat_b = FALSE;
							}
						}
						else
						{
							/* Either Short to BAT for particular heater detected                      */
							/* or Relay A fault happened. Do not do short to BAT for paricular heater. */
						}
					}
				}
				/* Monitor the fault detection further */
				//hsLF_2SecMonitorTime_FaultDetection(hs_nr, fault_condition_c);
				hsLF_2SecMonitorTime_FaultDetection(hs_seat);
//			}
//			else
//			{
//			}
		}
	}
	else
	{

		/* Main CSWM Status is Not GOOD. First step clear the Preliminary Fault counter to not blow any output faults */
		/* If Main CSWM Status is UGLY, for safety purposes turn OFF the Outputs directly */
		/* Also If any of the outputs are ON and first time logic observes UGLY state, as Logic already */
		/* displaying LEDs, no need to wait for extra 2sec to display LED */
		/*For those conditions, we need to turn OFF LED immedialty */
    	for (hs_seat = FRONT_LEFT; hs_seat < ALL_HEAT_SEATS; hs_seat++)
		{
    		/* Clear the monitor time counter */
    		hs_monitor_prelim_fault_counter_c[hs_seat] = 0;
    		
    		if (main_CSWM_Status_c == UGLY) 
    		{
    			
				if(hs_status3_flags_c[ hs_seat ].b.first_time_ugly_b == FALSE /*||
				  (hs_status3_flags_c[ hs_seat ].b.first_time_ugly_b == TRUE && hs_PWM_off_counter_c < 10)*/ )
				{
    			/* Turn OFF the outputs for safty */
    			/* hsLF_ControlFET(hs_seat, SET_OFF_REQUEST); */
                hs_pid_setpoint[hs_seat] = SET_OFF_REQUEST;
    			/*Increment the counter to allow the SET OFF Request to process for safety */
    			//hs_PWM_off_counter_c++;
				}
    			
				/*LED Status: If any switch request is present means we are already displaying the LED */
				/* In that case, no need to wait for 2sec when we first observe the UGLY condition */
				if((hs_rem_seat_req_c[hs_seat] != SET_OFF_REQUEST) &&
					(hs_status3_flags_c[ hs_seat ].b.first_time_ugly_b == FALSE) )
				{
					hs_monitor_2sec_led_counter_c[hs_seat] = /*mature_2sec_time_c*/hs_flt_dtc_c[HS_2SEC_MATURE_TIME];
				}
				hs_status3_flags_c[ hs_seat ].b.first_time_ugly_b = TRUE;
   			
    		}
    		else
    		{
				hs_status3_flags_c[ hs_seat ].b.first_time_ugly_b = FALSE;
			//	hs_PWM_off_counter_c = 0;
    		}
   		
		}
		
	}
}

/****************************************************************************************/
/*								hsLF_2SecLEDDisplay      								*/
/****************************************************************************************/
/* This function monitors all the faulty scenarios, according to adjusts the LED display.                                                                     */
/* If the conditions are good (no fault), or recovers from preliminary fault state, this function just updates the remembered state to the actual state.      */
/* If Loadshed 4 Set/UV Final Fault set/OV Final Fault Set/any of Heaters Final Fault set,  First we stop outputs and after 2sec monitor time LED will be OFF */
/* If Loadshed 2 set/IGN in Start Phase/UV or OV Preliminary faults set, Logic stops the output PWM and waits in this state.                                  */
/* If Relay A/B internal faults set/ No power to Rear seats set If we receive switch request First we stop the output PWMs and after 2sec LED will be OFF     */
/* If NTC readings > 70c and receives the new switch request just display LED for 2sec.                                                                       */
/*                                                                                                                                                            */
/* Calls from                                                                                                                                                 */
/* --------------                                                                                                                                             */
/* hsF_HeatManager :                                                                                                                                          */
/*    hsLF_2SecLEDDisplay()                                                                                                                                   */
/* hsLF_HeatDiagnostics :                                                                                                                                     */
/*    hsLF_2SecLEDDisplay()                                                                                                                                   */
/*                                                                                                                                                            */
/* Calls to                                                                                                                                                   */
/* --------                                                                                                                                                   */
/* hsLF_SendHeatedSeatCanStatus(unsigned char hs_nr,unsigned char action_c)                                                                                   */
void hsLF_2SecLEDDisplay(void)
{

	unsigned char hs_seat;

	for (hs_seat = FRONT_LEFT; hs_seat < ALL_HEAT_SEATS; hs_seat++) {

		/* If Load shed 4 set/UV or OV final fault set                  */
		/* If any of the final FET faults Open, STG or Partial Open set */
		/* If any of the NTC Fault set                                  */
		/* If Rear seat Stuck switch fault present(MKS 21852)           */  
		if ((main_CSWM_Status_c == UGLY) ||
		   (hs_status_flags_c[ hs_seat ].b.hs_final_open_b) ||
		   (hs_status_flags_c[ hs_seat ].b.hs_final_partial_open_b) ||
		   (hs_status_flags_c[ hs_seat ].b.hs_final_short_gnd_b) ||
		   (hs_control_flags_c[ hs_seat ].b.ntc_greater_70c_b) ||
		   (hs_seat == REAR_LEFT && hs_rear_seat_flags_c[ RL_SWITCH ].b.stksw_psd) ||
		   (hs_seat == REAR_RIGHT && hs_rear_seat_flags_c[ RR_SWITCH ].b.stksw_psd)   )
		{
			
			if (hs_rem_seat_req_c[hs_seat] != SET_OFF_REQUEST)
			{
				if (hs_monitor_2sec_led_counter_c[hs_seat] >= /*mature_2sec_time_c*/hs_flt_dtc_c[HS_2SEC_MATURE_TIME])
				{
					/* We have reached the 2sec time, turn OFF completely */
					hsLF_UpdateHeatParameters(hs_seat, HEAT_LEVEL_OFF);
					
					/* Set the remember seat req status to OFF */
					hs_rem_seat_req_c[hs_seat] = hs_switch_request_to_fet_c[hs_seat];
					
					/* Clear the 2sec increment timer */
					hs_monitor_2sec_led_counter_c[hs_seat] = 0;
				}
				else
				{
					/* Not reached 2 sec time. Increment the timer. */
					hs_monitor_2sec_led_counter_c[hs_seat]++;

					hs_switch_request_to_fet_c[hs_seat] = SET_OFF_REQUEST;
				}
			}
			else
			{
				//Do Nothing
			}
		}
		else
		{
			
//			/* Go to LOW LEVEL of heat if logic observes only Load shed 2 and No heater fault and no IO controlling is present*/
//			/* Check whether any switch request already present and Heat seat status is other than LOW*/
//			if ( (hs_status2_flags_c[ hs_seat ].b.hs_output_fault_set_b == FALSE)&&
//				 /*(canio_PN14_LS_Lvl2_c == TRUE)&&
//				 (canio_PN14_LS_Lvl3_c == FALSE) && 
//				 (canio_PN14_LS_Lvl7_c == FALSE) &&*/
//				 (!diag_io_hs_lock_flags_c[hs_seat].b.switch_rq_lock_b) &&  //MKS update: 22332
//				 (!diag_io_all_hs_lock_b) && //MKS update: 22332
//				 (diag_rc_all_op_lock_b == FALSE) )
//			{
//				/* Check to see any switch request happend? */
//				if (hs_rem_seat_req_c[hs_seat] != SET_OFF_REQUEST)
//				{
//					/* Get the latest Heated seat status from CAN Bus */
//					hsLF_GetHeatedSeatCanStatus(hs_seat);
//
//					/* Go to LOW Level only incase Heat seat status is other than LOW */
//					if(hs_temp_seat_stat_c [hs_seat] != CAN_LED_LOW)
//					{
//						/* Update the Heat parameter values with LL1 */
//						hsLF_UpdateHeatParameters(hs_seat, HEAT_LEVEL_LL1);
//					}
//				}
//			}
//			else
//			{
//				//DO nothing
//			}
			


				/* If we see Relay A Internal Fault/Any of Front Seat Short to BAT */
				if ((hs_seat == FRONT_LEFT) ||(hs_seat == FRONT_RIGHT))
				{
					if (relay_A_internal_Fault_b || (hs_status_flags_c[ FRONT_LEFT ].b.hs_final_short_bat_b) ||
					   (hs_status_flags_c[ FRONT_RIGHT ].b.hs_final_short_bat_b) ||(vs_shrtTo_batt_mntr_c == VS_SHRT_TO_BATT))
					{

						if (hs_rem_seat_req_c[hs_seat] != SET_OFF_REQUEST)
						{
							if (hs_monitor_2sec_led_counter_c[hs_seat] >= /*mature_2sec_time_c*/hs_flt_dtc_c[HS_2SEC_MATURE_TIME])
							{
								/* We have reached the 2sec time, turn OFF completely */
								hsLF_UpdateHeatParameters(hs_seat, HEAT_LEVEL_OFF);
								
								/* Set the remember seat req status to OFF */
								hs_rem_seat_req_c[hs_seat] = hs_switch_request_to_fet_c[hs_seat];
								
								/* Clear the 2sec increment timer */
								hs_monitor_2sec_led_counter_c[hs_seat] = 0;
								
							}
							else
							{
								/* Not reached 2 sec time. Increment the timer. */
								hs_monitor_2sec_led_counter_c[hs_seat]++;

								hs_switch_request_to_fet_c[hs_seat] = SET_OFF_REQUEST;
							}
						}
						else
						{
							//Do Nothing
						}
					}
					else
					{
						//MKS 27984 update
						/* UV/OV preliminary fault set or IGN in Start or Enters the Load shed 2 */
						if (main_CSWM_Status_c == BAD)
						{
							/* Turn OFF the Heater outputs as condition is BAD */
							hs_switch_request_to_fet_c[hs_seat] = SET_OFF_REQUEST;
							
						}
						else
						{
							/* Else either we are good for ever or recovered from BAD or UGLY conditions */
							hs_switch_request_to_fet_c[ hs_seat ] = hs_rem_seat_req_c[hs_seat];
							
						}
//						/* Else either we are good for ever or recovered from BAD or UGLY conditions */
//						hs_switch_request_to_fet_c[ hs_seat ] = hs_rem_seat_req_c[hs_seat];
					}
				}
				else
				{
					/* If we see Relay B Internal Fault/Rear Left or Right Seat Short to Bat fault or If we see no power supply for Rear Seats */
					if (relay_B_internal_Fault_b || (hs_status_flags_c[ REAR_LEFT ].b.hs_final_short_bat_b) ||
					   (hs_status_flags_c[ REAR_RIGHT ].b.hs_final_short_bat_b) || Rear_Seats_no_power_final_b)
					{
						if (hs_rem_seat_req_c[hs_seat] != SET_OFF_REQUEST)
						{
							if (hs_monitor_2sec_led_counter_c[hs_seat] >= /*mature_2sec_time_c*/hs_flt_dtc_c[HS_2SEC_MATURE_TIME])
							{
								/* We have reached the 2sec time, turn OFF completely */
								hsLF_UpdateHeatParameters(hs_seat, HEAT_LEVEL_OFF);
								
								/* Set the remember seat req status to OFF */
								hs_rem_seat_req_c[hs_seat] = hs_switch_request_to_fet_c[hs_seat];
								
								/* Clear the 2sec increment timer */
								hs_monitor_2sec_led_counter_c[hs_seat] = 0;
								
							}
							else
							{
								/* Not reached 2 sec time. Increment the timer. */
								hs_monitor_2sec_led_counter_c[hs_seat]++;

								hs_switch_request_to_fet_c[hs_seat] = SET_OFF_REQUEST;
							}
						}
						else
						{
							//Do Nothing
						}
					}
					else
					{
						//MKS 27984 update
						/* UV/OV preliminary fault set or IGN in Start or Enters the Load shed 2 */
						if (main_CSWM_Status_c == BAD)
						{
							/* Turn OFF the Heater outputs as condition is BAD */
							hs_switch_request_to_fet_c[hs_seat] = SET_OFF_REQUEST;
							
						}
						else
						{
							/* Else either we are good for ever or recovered from BAD or UGLY conditions */
							hs_switch_request_to_fet_c[ hs_seat ] = hs_rem_seat_req_c[hs_seat];
							
						}
//						/* Else either we are good for ever or recovered from BAD or UGLY conditions */
//						hs_switch_request_to_fet_c[ hs_seat ] = hs_rem_seat_req_c[hs_seat];
					}
				}
//			}
		}
	}
}

/****************************************************************************************/
/*								hsLF_ClearHeatSequence      							*/
/****************************************************************************************/
/* This function gets executed in two conditions.                                                                     */
/* First time at the time of initialization and                                                                       */
/* Second condition is when the IGN status is in OFF.                                                                 */
/*                                                                                                                    */
/* This function sets the local variables to default values.                                                          */
/* It make sure that all the time counters are cleared and Switch requests are OFf and the heated seat status to OFF. */
/*                                                                                                                    */
/* Calls from                                                                                                         */
/* --------------                                                                                                     */
/* hsF_HeatManager :                                                                                                  */
/*    hsLF_ClearHeatSequence()                                                                                        */
/* hsF_Init :                                                                                                         */
/*    hsLF_ClearHeatSequence()                                                                                        */
/* IntFlt_F_ClearOutputs :                                                                                            */
/*    hsLF_ClearHeatSequence()                                                                                        */
/*                                                                                                                    */
/* NAME:               void hsLF_ClearHeatSequence( void )                                 */
/* This function is called from following functions in the following instances:            */
/* hsF_Init                            upon power-on.                                      */
/* hsF_HeatManager                     If the Ignition cycle is not in RUN/START position. */
void hsLF_ClearHeatSequence(void)
{
	unsigned char hs_seat;

	for (hs_seat = FRONT_LEFT; hs_seat < ALL_HEAT_SEATS; hs_seat++ )
	{
		/* hsLF_ControlFET(hs_seat, SET_OFF_REQUEST); */ //This function makes sure that, all the PWMs are at 0%
        hs_pid_setpoint[hs_seat] = SET_OFF_REQUEST;

		hs_control_flags_c[ hs_seat ].cb = 0; //Holds the Heat start timer bit, Pre Heat Bit and Message flow ctrl bit
		hs_status_flags_c[ hs_seat ].cb = 0;  // Holds the status bits
		hs_status2_flags_c[ hs_seat ].cb = 0;  // Holds the status bits
		hs_status3_flags_c[ hs_seat ].cb = 0;  // Holds the status bits
		hs_switch_request_to_fet_c [hs_seat] = 0; //Holds the actual request to FET,Initially SET_OFF_REQUEST
		hs_allowed_heat_pwm_c [hs_seat] = 0; //Holds the current PWM for the current level
		hs_allowed_heat_time_c [hs_seat] = 0; //Holds the current Timeout value for the PWM.
		hs_wait_time_check_NTC_c[hs_seat] = 0; //Holds the wait time period to check the NTC feedback for current level
		hs_allowed_NTC_c[hs_seat] = 0; //Holds the NTC Thresholds for current Level
	   //	hs_temp_seat_req_c[hs_seat] = 1;  //Holds the momentary switch req status
		hs_temp_seat_stat_c [hs_seat] = 0; //Holds the current seat stat
		hs_rem_seat_req_c [hs_seat] = 0;   // Holds the current seat request state and can be used when faults were removed
		hs_output_status_c [hs_seat] = 0; // Output Heat seat status display using KWP
		hs_time_counter_w[ hs_seat ] = 0x00000; //Clear the timeout counter
		hs_status2_flags_c[ hs_seat ].b.hs_heat_level_off_b = TRUE; //Holds the heater status.
		hs_rear_seat_led_status_c [hs_seat] = CAN_LED_OFF; //Rear Seat LED status set to Zero
//		hs_rear_swpsd_stat_c[hs_seat] = 0; //Rear seat switch press status shall be cleared.

	
		hs_Vsense_c [hs_seat] = 0;
		hs_Isense_c [hs_seat] = 0;
		hs_Zsense_c [hs_seat] = 0;
	}

//	hs_vehicle_line_last_c = 0;
//	hs_vehicle_line_c = 0;
	hs_temp_set_c.cb = 0; //WHen IGN is Not in Run/Start clear the NTC Fault data flag.

	hsLF_Timer(ALL_HEAT_SEATS, TIMER_CLEAR);  //Clear the Time
	hsLF_SendHeatedSeatCanStatus(ALL_HEAT_SEATS, CAN_LED_OFF); //Initially set the CAN_LED_OFF status on Bus
	
    //The below 4 lines added for Tracker ID #15298
	/* Turn OFF the R L LED1 */
	Dio_WriteChannel(LED1_RL_EN, STD_LOW);
	/* Turn OFF the R L LED2 */
	Dio_WriteChannel(LED2_RL_EN, STD_LOW);
	
	/* Turn OFF the R R LED1 */
	Dio_WriteChannel(LED1_RR_EN, STD_LOW);
	/* Turn OFF the R R LED2 */
	Dio_WriteChannel(LED2_RR_EN, STD_LOW);

	/* Inactivate the RemSt */
	/* FTR00139669 -- Autostart feature for PowerNet Series.*/
	RemSt_activated_b = FALSE;
	
	hs_rear_swpsd_stat_c [RL_SWITCH] = 0; //Rear seat switch press status shall be cleared.
	hs_rear_swpsd_stat_c [RR_SWITCH] = 0; //Rear seat switch press status shall be cleared.
		

    /* For front seats switch request values 1 = PRESS 0 = NOT PRESS  */
    /* For rear seat we are getting inverse logic, and switch request values 0 = PRESS 1 = NOT PRESS */
    hs_temp_seat_req_c[FRONT_LEFT]=0;
	hs_temp_seat_req_c[FRONT_RIGHT]=0;
	hs_temp_seat_req_c[REAR_LEFT]=1;
	hs_temp_seat_req_c[REAR_RIGHT]=1;

}

/****************************************************************************************/
/*								hsLF_SendHeatedSeatCanStatus      						*/
/****************************************************************************************/
/* This function gets called when ever the Heates seat status gets changed.                                    */
/* Main functionality is to update the new heat status and send it on to the Bus.                              */
/* Values are HS_HI or HS_LO or HS_OFF.                                                                        */
/*                                                                                                             */
/* Added PowerNet signal access																				   */
/* Calls from                                                                                                  */
/* --------------                                                                                              */
/* hsLF_2SecLEDDisplay :                                                                                       */
/*    hsLF_SendHeatedSeatCanStatus(unsigned char hs_nr,unsigned char action_c)                                 */
/* hsLF_UpdateHeatParameters :                                                                                 */
/*    hsLF_SendHeatedSeatCanStatus(Heat_Seat_Status HeatSeatStatus,unsigned char hs_nr,unsigned char action_c) */
/*                                                                                                             */
/* Calls to                                                                                                    */
/* --------                                                                                                    */
/* dbkPutTxL_F_HS( variable)                                                                                   */
/* dbkGetTxL_F_HS()                                                                                            */
/* dbkPutTxR_F_HS()                                                                                            */
/* dbkGetTxR_F_HS()                                                                                            */
/* dbkPutTxRR_SEAT()                                                                                           */
/* dbkGetTxRR_SEAT()                                                                                           */
/* INPUT PARAMETER: HS_FL, HS_FR, HS_RL, HS_RR or HS_ALL_SEATS */
/*                        and                                  */
/*                  CAN_LED_HI, CAN_LED_LOW, CAN_LED_OFF       */
/* Description: This function will send the heat seat current  */
/*              status to the outside world. That is to CAN BUS*/

void hsLF_SendHeatedSeatCanStatus(unsigned char hs_side,unsigned char action_c)
{
	
	unsigned char hs_seat_mask_c;

	switch (hs_side) {
		/* Front Left Heat Mask */
		case FRONT_LEFT:
		{
			/* Make the heated Seat Mask to Front Left */
			hs_seat_mask_c = HS_FL_SEAT_MASK;
			break;
		}
									    
		/* Front Right Heat Mask */
		case FRONT_RIGHT:
		{
			hs_seat_mask_c = HS_FR_SEAT_MASK;
			break;
		}
		
		/* Rear Left Heat Mask */
		case REAR_LEFT:
		{
			hs_seat_mask_c = HS_RL_SEAT_MASK;
			break;
		}
		
		/* Rear Right Heat Mask */
		case REAR_RIGHT:
		{
			hs_seat_mask_c = HS_RR_SEAT_MASK;
			break;
		}
		
		/* All seat Mask */
		case ALL_HEAT_SEATS:
		{
			hs_seat_mask_c = HS_ALL_SEAT_MASK;
			break;
		}
		
		default:
		{
			
		}
	}
	
	/* Front Left Seat Status Handling */
	/* For Left Front Heated seat */
	if (HS_FL_SEAT_MASK & hs_seat_mask_c)
	{
		/* Send the new state on Bus */
		IlPutTxFL_HS_STATSts(action_c);
	}
	
	/* Front Right Seat Status Handling */
	/* For Right Front Heated seat */
	if (HS_FR_SEAT_MASK & hs_seat_mask_c)
	{
		/* Send the new state on Bus */
		IlPutTxFR_HS_STATSts(action_c);
	}
	
}

/****************************************************************************************/
/*								hsLF_SendRearSeat_HWStatus      						*/
/*																						*/
/* Calls from:																			*/
/* -----------																			*/
/* hsLF_Process_DiagIO_LEDStatus														*/
/* hsLF_UpdateHeatParameters															*/
/*																						*/
/* Calls to:																			*/
/* -----------																			*/
/* Dio_WriteChannel																		*/
/*																						*/
/* Description:																			*/
/* ------------																			*/
/* INPUT PARAMETER:  HS_RL, HS_RR                              							*/
/*                        and                                  							*/
/*                   CAN_LED_HI, CAN_LED_LOW, CAN_LED_OFF       						*/
/* Description: This function will send the heat seat current  							*/
/*              status to the ECU. That is to own HArdware     							*/

/****************************************************************************************/
void hsLF_SendRearSeat_HWStatus(unsigned char hs_rear,unsigned char action_c)
{

	switch (hs_rear) {

		case REAR_LEFT:
		{

			if (action_c == CAN_LED_HI) {

				/* Turn ON the R L LED1 ON */
				Dio_WriteChannel(LED1_RL_EN, STD_HIGH);
				/* Turn ON the R L LED2 ON */
				Dio_WriteChannel(LED2_RL_EN, STD_HIGH);

				hs_rear_seat_led_status_c [hs_rear] = CAN_LED_HI;
				
			} else if (action_c == CAN_LED_LOW){

					/* Turn ON the R L LED1 */
					Dio_WriteChannel(LED1_RL_EN, STD_HIGH);
					/* Turn OFF the R L LED2 */
					Dio_WriteChannel(LED2_RL_EN, STD_LOW);

					hs_rear_seat_led_status_c [hs_rear] = CAN_LED_LOW;

				} else {

					/* Turn OFF the R L LED1 */
					Dio_WriteChannel(LED1_RL_EN, STD_LOW);
					/* Turn OFF the R L LED2 */
					Dio_WriteChannel(LED2_RL_EN, STD_LOW);

					hs_rear_seat_led_status_c [hs_rear] = CAN_LED_OFF;
				}
			break;

		}

		case REAR_RIGHT:
		{
			if (action_c == CAN_LED_HI) {

				/* Turn ON the R R LED1 ON */
				Dio_WriteChannel(LED1_RR_EN, STD_HIGH);
				/* Turn ON the R R LED2 ON */
				Dio_WriteChannel(LED2_RR_EN, STD_HIGH);

				hs_rear_seat_led_status_c [hs_rear] = CAN_LED_HI;

			} else if (action_c == CAN_LED_LOW){

					/* Turn ON the R R LED1 */
					Dio_WriteChannel(LED1_RR_EN, STD_HIGH);
					/* Turn OFF the R R LED2 */
					Dio_WriteChannel(LED2_RR_EN, STD_LOW);

					hs_rear_seat_led_status_c [hs_rear] = CAN_LED_LOW;

			} else {

					/* Turn OFF the R R LED1 */
					Dio_WriteChannel(LED1_RR_EN, STD_LOW);
					/* Turn OFF the R R LED2 */
					Dio_WriteChannel(LED2_RR_EN, STD_LOW);

					hs_rear_seat_led_status_c [hs_rear] = CAN_LED_OFF;
			}
			break;
		}
		
		default:
		{
			
		}
	}

} //End of hsLF_SendRearSeat_HWStatus

/****************************************************************************************/
/*								hsLF_GetHeatedSeatCanStatus      						*/
/****************************************************************************************/
/* This function gets executed when ever a new switch request present for the Heated seats. */
/* Retrieves the Heates seat status at the time switch request present.                     */
/* The values are HS_HI or HS_LO or HS_OFF.                                                 */
/* Added Powernet signal access															    */
/*                                                                                          */
/* Calls from                                                                               */
/* --------------                                                                           */
/* hsF_HeatManager :                                                                        */
/*    hsLF_GetHeatedSeatCanStatus()                                                         */
/*                                                                                          */
/* Calls to                                                                                 */
/* --------                                                                                 */
/* dbkGetTxL_F_HS()                                                                         */
/* dbkGetTxR_F_HS()                                                                         */
/* dbkGetTxRR_SEAT()                                                                        */

void hsLF_GetHeatedSeatCanStatus(unsigned char hs_pstn)
{

	/* INPUT PARAMETER: HS_FL, HS_FR, HS_RL, HS_RR or HS_ALL_SEATS */
	/*                        and                                  */
	/*                  CAN_LED_HI, CAN_LED_LOW, CAN_LED_OFF       */
	/* Description: This function will Get the latest heat seat    */
	/*              status from outside world. That is from CAN BUS*/

	
		
	switch (hs_pstn) {
		/* Front Left Heat status */
		case FRONT_LEFT:
		{
			/* Get the complete Left front container information into local container */
			//LocalGet_FL_HVS_STAT.c = dbkGetTxFL_HVS_STAT();
			
			/* Extract the Front left Heated seat status from container to local variable */
			//hs_temp_seat_stat_fl_c = GetValueTxFL_HS_STAT( LocalGet_FL_HVS_STAT);
			IlEnterCriticalFL_HS_STATSts();
			hs_temp_seat_stat_c[FRONT_LEFT] = CSWM_A1.CSWM_A1.FL_HS_STATSts;
			IlLeaveCriticalFL_HS_STATSts();
			
			//hs_temp_seat_stat_c[FRONT_LEFT] = IlPrivateGetTxFL_HS_STATSts();
			
			break;
		}
		
		/* Front Right Heat Status */
		case FRONT_RIGHT:
		{
			/* Get the complete Left front container information into local container */
			//LocalGet_FR_HVS_STAT.c = dbkGetTxFR_HVS_STAT();
			
			/* Extract the Front left Heated seat status from container to local variable */
			//hs_temp_seat_stat_fr_c = GetValueTxFR_HS_STAT( LocalGet_FR_HVS_STAT);
			IlEnterCriticalFR_HS_STATSts();
			hs_temp_seat_stat_c[FRONT_RIGHT] = CSWM_A1.CSWM_A1.FR_HS_STATSts;
			IlLeaveCriticalFR_HS_STATSts();
			
			//hs_temp_seat_stat_c[FRONT_RIGHT] = IlPrivateGetTxFR_HS_STATSts();
			break;
			
		}
		
		/* Rear Left Heat Status */
//		case REAR_LEFT:
//		{
			/* Get the Rear Left Seat LED status */
			//hs_temp_seat_stat_rl_c = hs_rear_seat_led_status_c[REAR_LEFT];
//			hs_temp_seat_stat_c[REAR_LEFT] = hs_rear_seat_led_status_c[REAR_LEFT];
//			break;
//		}
		
		/* Rear Right Heat Status */
//		case REAR_RIGHT:
//		{
			/* Get the Rear Right Seat LED status */
			//hs_temp_seat_stat_rr_c = hs_rear_seat_led_status_c[REAR_RIGHT];
//			hs_temp_seat_stat_c[REAR_RIGHT] = hs_rear_seat_led_status_c[REAR_RIGHT];
//			break;
//		}
		
		default:
		{
			
		}
		
	}
		
//#endif		
		

}

/****************************************************************************************/
/*								       hsLF_Timer      						            */
/****************************************************************************************/
/* This function will get executed when ever there is a new switch request present/timeout happens for current PWM level. */
/* Basically this function clears the time counter for PWM heater timeouts.                                               */
/*                                                                                                                        */
/*                                                                                                                        */
/* Calls from                                                                                                             */
/* --------------                                                                                                         */
/* hsF_HeatManager :                                                                                                      */
/*    hsLF_Timer(unsigned char hs_nr,unsigned char action_c)                                                              */
/* hsF_MonitorHeatTimes :                                                                                                 */
/*    hsLF_Timer(unsigned char hs_nr,unsigned char action_c)                                                              */
/*                                                                                                                        */
void hsLF_Timer(unsigned char hs_pos,unsigned char action)
{

	/* INPUT PARAMETER: HS_FL, HS_FR, HS_RL, HS_RR or HS_ALL_SEATS */
	/*                        and                                  */
	/*                  TIMER_START, TIMER_PAUSE, TIMER_CLEAR      */
	/* Description: This function will let your timer to start     */
	/*              counting time, puase at any point of time and  */
	/*              clear the timer                                */

	/* Either timer clear or timer start, we clear the timer counter, so it starts with Zero in case of start or stay at 0 in case of clear */
//	hs_time_counter_w[hs_pos] = 0x0000; //PC-Lint errot 415

	if (action == TIMER_START) {
		hs_control_flags_c[ hs_pos ].b.hs_start_timer_b = TRUE;
		hs_time_counter_w[hs_pos] = 0x0000;
	}
	else {

		if (hs_pos == ALL_HEAT_SEATS) {

			for ( hs_pos = FRONT_LEFT; hs_pos < ALL_HEAT_SEATS; hs_pos++ ) {

				hs_control_flags_c[ hs_pos ].b.hs_start_timer_b = FALSE;
				hs_time_counter_w[ hs_pos ] = 0x0000;
			}
		}
		else {
			hs_control_flags_c[ hs_pos ].b.hs_start_timer_b = FALSE;
			hs_time_counter_w[hs_pos] = 0x0000;
		}
	}
}

/****************************************************************************************/
/*								   hsLF_UpdateHeatParameters   						    */
/****************************************************************************************/
/* This function gets executed when ever there is a Heat level change either by switch request or by stepdown logic.                 */
/*                                                                                                                                   */
/* Based on the inputs, this function basically updates the PWM and timeout values for the new switch request or for timedout logic. */
/* Based on these values module actually PWM.                                                                                        */
/* This function also calls the hsLF_SendHeatedSeatCanStatus() to update the current seat status.                                    */
/* Calls hsLF_Timer() to clear/start the timer.                                                                                      */
/*                                                                                                                                   */
/* Calls from                                                                                                                        */
/* --------------                                                                                                                    */
/* hsF_HeatManager :                                                                                                                 */
/*    hsLF_UpdateHeatParameters()                                                                                                    */
/* hsF_MonitorHeatTimes :                                                                                                            */
/*    hsLF_UpdateHeatParameters()                                                                                                    */
/* vsF_Rqst_Drg_Hs_Cycl :                                                                                                            */
/*    hsLF_UpdateHeatParameters(unsigned char hs_nr,unsigned char value_c)                                                           */
/* hsLF_Process_DiagIO_RC_Outputs ()																									 */
/*                                                                                                                                   */
/* Calls to                                                                                                                          */
/* --------                                                                                                                          */
/* hsLF_SendHeatedSeatCanStatus(Heat_Seat_Status HeatSeatStatus,unsigned char hs_nr,unsigned char action_c)                          */
@far void hsLF_UpdateHeatParameters(unsigned char hs_seatpos, unsigned char value_c)
{
	/* First Clear all the flags, then based on the Heat level set the particular flag */
	/* Flag denotes to other functions that HL3 not running */
	hs_status2_flags_c[hs_seatpos].b.hs_HL3_set_b = FALSE;

	/* Flag denotes to other functions that HL2 not running */
	hs_status2_flags_c[hs_seatpos].b.hs_HL2_set_b = FALSE;

	/* Flag denotes to other functions that HL1 not running */
	hs_status2_flags_c[hs_seatpos].b.hs_HL1_set_b = FALSE;

	/* Flag denotes to other functions that LL1 not running */
	hs_status2_flags_c[hs_seatpos].b.hs_LL1_set_b = FALSE;

	if (value_c == HEAT_LEVEL_OFF)
	{
		/* Clear the PWM and Timer values */
		hs_allowed_heat_pwm_c[ hs_seatpos ] = 0; 
		hs_allowed_heat_time_c[ hs_seatpos ] = 0;
		hs_wait_time_check_NTC_c[hs_seatpos] = 0;
		hs_allowed_NTC_c[hs_seatpos] = 0;

		/* Clear the PWM value */
        /* hsLF_ControlFET(hs_seatpos, SET_OFF_REQUEST); */
        hs_pid_setpoint[hs_seatpos] = SET_OFF_REQUEST;
        
		/* Set the switch request FET to OFF. Here we set the PWM to Zero when it processed. */
		hs_switch_request_to_fet_c[hs_seatpos] = SET_OFF_REQUEST;

		/* MKS 54495: Added EOL Start flag to get the LED Status */
		if (((hs_seatpos == REAR_LEFT)||(hs_seatpos == REAR_RIGHT)) && 
			((main_SwReq_Rear_c == VEH_SWITCH_REAR_ONLY_K) || (diag_eol_io_lock_b == TRUE)) )
		{
			/* Set the Own Hardware LED Status to Zero */
			hsLF_SendRearSeat_HWStatus(hs_seatpos, CAN_LED_OFF);
			/* Set the CAN output to Zero */
			hsLF_SendHeatedSeatCanStatus(hs_seatpos, CAN_LED_OFF);
			
		}
		else
		{
			/* Set the CAN output to Zero */
			hsLF_SendHeatedSeatCanStatus(hs_seatpos, CAN_LED_OFF);
		}
		
		/* Display status for $$21 $$05.. */
		hs_output_status_c [hs_seatpos] = HS_READ_STATUS_OFF;

		/* Heaters are OFF, Clear the timers */
		hsLF_Timer(hs_seatpos, TIMER_CLEAR);

		/* Flag denotes to other functions that Heat level is off */
		hs_status2_flags_c[hs_seatpos].b.hs_heat_level_off_b = TRUE;

		/* Flag denotes to other functions that Heater state is off */
		hs_status2_flags_c[hs_seatpos].b.hs_heater_state_b = FALSE;

		/* As we are turning OFF the particular heater, clear the counter too. */
		hs_monitor_prelim_fault_counter_c[hs_seatpos] = 0;

		/* As we are turning OFF the particular heater, clear the Final 2sec LED counter too. */
		hs_monitor_2sec_led_counter_c[hs_seatpos] = 0;

		/* As we are turning OFF particular heater, clear the Preliminary fault bits also here. */
		/* Clear the Preliminary Fault flag for Short to Bat                                    */
		hs_status_flags_c[ hs_seatpos ].b.hs_prelim_short_bat_b = FALSE;

		/* For safe locking clear all other preliminary flags */
		hs_status_flags_c[ hs_seatpos ].b.hs_prelim_short_gnd_b = FALSE;

		hs_status_flags_c[ hs_seatpos ].b.hs_prelim_open_b = FALSE;

		hs_status_flags_c[ hs_seatpos ].b.hs_prelim_partial_open_b = FALSE;
		
//		hs_Vsense_c [hs_seatpos] = 0;
//		hs_Isense_c [hs_seatpos] = 0;
//		hs_Zsense_c [hs_seatpos] = 0;
	}
	else {
		/* New addition: MKS 61288: New calibration set */
		/* To get the actual Parameters based on Fronts or Rears */
		if ((hs_seatpos == REAR_LEFT)||(hs_seatpos == REAR_RIGHT))
		{
				hs_vehicle_line_offset_c= VEH_LINE_REAROFFSET;	
		}
		else
		{
				hs_vehicle_line_offset_c= VEH_LINE_FRONTOFFSET;   
		}
		
		/* Get the PWM and Timer values in to Local variable */
		hs_allowed_heat_pwm_c[ hs_seatpos ] = /*hs_heat_pwm_values_c[value_c]*/hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].Pwm_c[value_c];
		 
		hs_allowed_heat_time_c[ hs_seatpos ] = /*hs_heat_timeout_values_c[value_c]*/hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].Max_TimeOut_c[value_c];
		
		//hs_wait_time_check_NTC_c[hs_seatpos] = /*hs_NTC_check_timeout_values_c[value_c]*/hs_cal_prms.hs_prms[hs_vehicle_line_c].Min_TimeOut_c[value_c];
		
		hs_allowed_NTC_c[hs_seatpos] = /*hs_NTC_feedback_values_c[value_c]*/hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].NTC_CutOFF_c[value_c];

		if (value_c == HEAT_LEVEL_LL1)
		{
			/* MKS 54495: Added EOL Start flag to get the LED Status */
			if (((hs_seatpos == REAR_LEFT)||(hs_seatpos == REAR_RIGHT)) && 
				((main_SwReq_Rear_c == VEH_SWITCH_REAR_ONLY_K) || (diag_eol_io_lock_b == TRUE)) )
			{
				/* Set the own HW status to LOW */
				hsLF_SendRearSeat_HWStatus(hs_seatpos, CAN_LED_LOW);
				/* Here we are setting the Heated seat status on Bus to LOW */
				hsLF_SendHeatedSeatCanStatus(hs_seatpos, CAN_LED_LOW);

			}
			else
			{
				/* Here we are setting the Heated seat status on Bus to LOW */
				hsLF_SendHeatedSeatCanStatus(hs_seatpos, CAN_LED_LOW);
			}
			/* Set the switch request FET to the Heat LO level. */
			hs_switch_request_to_fet_c[hs_seatpos] = SET_LO_REQUEST;
		}
		else
		{
			/* MKS 54495: Added EOL Start flag to get the LED Status */
			if (((hs_seatpos == REAR_LEFT)||(hs_seatpos == REAR_RIGHT)) && 
				((main_SwReq_Rear_c == VEH_SWITCH_REAR_ONLY_K) || (diag_eol_io_lock_b == TRUE)) )
			{
				/* Set the own HW status to HIGH */
				hsLF_SendRearSeat_HWStatus(hs_seatpos, CAN_LED_HI);
				/* Here we are setting the Heated seat status on Bus to HIGH */
				hsLF_SendHeatedSeatCanStatus(hs_seatpos, CAN_LED_HI);

			}
			else
			{
				/* Here we are setting the Heated seat status on Bus to HIGH */
				hsLF_SendHeatedSeatCanStatus(hs_seatpos, CAN_LED_HI);
			}
			
			/* Set the switch request FET to the Heat HI level. */
			hs_switch_request_to_fet_c[hs_seatpos] = SET_HI_REQUEST;

		}
		
		/* New switch request processed. Start the Timer for FL */
		hsLF_Timer(hs_seatpos, TIMER_START);

		/* Flag denotes to other functions that Heat level is running */
		hs_status2_flags_c[hs_seatpos].b.hs_heat_level_off_b = FALSE;

		switch (value_c) {

			case HEAT_LEVEL_HL3:
			{
				/* Flag denotes to other functions that HL3 is running */
				hs_status2_flags_c[hs_seatpos].b.hs_HL3_set_b = TRUE;

				/* Display status for $$21 $$05.. */
				hs_output_status_c [hs_seatpos] = HS_READ_STATUS_HL3; 

				break;
			}

			case HEAT_LEVEL_HL2:
			{
				/* Flag denotes to other functions that HL3 is running */
				hs_status2_flags_c[hs_seatpos].b.hs_HL2_set_b = TRUE;
				
				/* Display status for $$21 $$05.. */
				hs_output_status_c [hs_seatpos] = HS_READ_STATUS_HL2; 

				break;
			}

			case HEAT_LEVEL_HL1:
			{
				/* Flag denotes to other functions that HL1 is running */
				hs_status2_flags_c[hs_seatpos].b.hs_HL1_set_b = TRUE;

				/* Display status for $$21 $$05.. */
				hs_output_status_c [hs_seatpos] = HS_READ_STATUS_HL1; 

				break;
			}
			
			/* case HEAT_LEVEL_LL1 */
			case HEAT_LEVEL_LL1:
			{
				/* Flag denotes to other functions that LL1 is running */
				hs_status2_flags_c[hs_seatpos].b.hs_LL1_set_b = TRUE;

				/* Display status for $$21 $$05.. */
				hs_output_status_c [hs_seatpos] = HS_READ_STATUS_LL1; 

				break;
			}
			
			default:
			{
				
			}
		}
	}
	/* Store the current switch request to the variable. So in case if the faults were gone and we were in Monitoring time, we can re-activate our outputs. */
	hs_rem_seat_req_c[hs_seatpos] = hs_switch_request_to_fet_c[hs_seatpos];
} //End of Update Parameters function

/****************************************************************************************/
/*								   hsF_HeatRequestToFet   						        */
/****************************************************************************************/
/* This function gets called from 5ms timeslice.                                        */
/* The logic works only, when ever there is a change to adjust the PWM for the heaters. */
/* Or for some reason, If Output PWM is HIGH even after SET OFF request, logic enters   */
/*                                                                                      */
/* Calls from                                                                           */
/* --------------                                                                       */
/* main :                                                                               */
/*    hsF_HeatRequestToFet()                                                            */
/*                                                                                      */
/* Calls to                                                                             */
/* --------                                                                             */
/* hsLF_ControlFET(unsigned char hs_nr,unsigned char duty_cycle)                        */
@far void hsF_HeatRequestToFet(void)
{

	unsigned char hs_position;

	for (hs_position = FRONT_LEFT; hs_position < ALL_HEAT_SEATS; hs_position++ ) {

		/* Front Seats */
		/* If the Heat Request is for either Front LEft or Front Right, check to see that Relay is OK or not. IF Relay is OK, then Go for PWMing. Else do not PWM the heat. */
		if ((hs_position == FRONT_LEFT || hs_position == FRONT_RIGHT) && relay_A_Ok_b)
		{
			switch (hs_switch_request_to_fet_c[hs_position]) {

				case /*SET_HL3_REQUEST:
				     SET_HL2_REQUEST:
				     SET_HL1_REQUEST:
				     SET_LL2_REQUEST:
				     SET_LL1_REQUEST:*/
				     SET_HI_REQUEST:
				case SET_LO_REQUEST:
				{
					/*Either switch req is HI/LOW, set off is False */
					hs_status3_flags_c[hs_position].b.hs_setoff_rq_b = FALSE;
					
					/* Set the PWM value */
					/* hsLF_ControlFET(hs_position, hs_allowed_heat_pwm_c[ hs_position ]); */
					break;
				}

				case SET_OFF_REQUEST:
				{
					/* If PWM is HIGH or Set off request is not served for first time after turning off heaters, enter inside */
					/* Turn OFF the PWM */
					if((hs_pwm_state_c[hs_position] == PWM_HIGH) || (hs_status3_flags_c[hs_position].b.hs_setoff_rq_b == FALSE))
					{
						/* hsLF_ControlFET(hs_position, SET_OFF_REQUEST); */
                        hs_pid_setpoint[hs_position] = SET_OFF_REQUEST;
						
						/*Served the first Set off request after getting the OFF request */
						hs_status3_flags_c[hs_position].b.hs_setoff_rq_b = TRUE; 
					}
					break;
				}
				
				default:
				{
					/* hsLF_ControlFET(hs_position, SET_OFF_REQUEST);*/
					hs_pid_setpoint[hs_position] = SET_OFF_REQUEST;
				}
			}
		}
		else
		{
		}
		/* Rear Seats */
		/* Go to the Rear Seat PWM processing only incase Variant supports 4H functionality */
		if (((CSWM_config_c & CSWM_4HS_VARIANT_MASK_K) == CSWM_4HS_VARIANT_MASK_K))
		{
			/* If the Heat Request is for either Rear Left or Rear Right, check to see that Relay is OK or not. IF Relay is OK, then Go for PWMing. Else do not PWM the heat. */
			if ((hs_position == REAR_LEFT || hs_position == REAR_RIGHT) && relay_B_Ok_b)
			{
				switch (hs_switch_request_to_fet_c[hs_position]) {

					case /*SET_HL3_REQUEST:
					     SET_HL2_REQUEST:
					     SET_HL1_REQUEST:
					     SET_LL2_REQUEST:
					     SET_LL1_REQUEST:*/
					     SET_HI_REQUEST:
					case SET_LO_REQUEST:
					{
						/*Either switch req is HI/LOW, set off is False */
						hs_status3_flags_c[hs_position].b.hs_setoff_rq_b = FALSE;
						/* Set the PWM value */
						/*hsLF_ControlFET(hs_position, hs_allowed_heat_pwm_c[ hs_position ]); */
						break;
					}

					case SET_OFF_REQUEST:
					{
						/* If PWM is HIGH or Set off request is not served for first time after turning off heaters, enter inside */
						/* Turn OFF the PWM */
						if((hs_pwm_state_c[hs_position] == PWM_HIGH) || (hs_status3_flags_c[hs_position].b.hs_setoff_rq_b == FALSE) )
						{
							/*hsLF_ControlFET(hs_position, SET_OFF_REQUEST);*/
							hs_pid_setpoint[hs_position] = SET_OFF_REQUEST;

							/*Served the first Set off request after getting the OFF request */
							hs_status3_flags_c[hs_position].b.hs_setoff_rq_b = TRUE;
						}
						break;
					}
					
					default:
					{
						/*hsLF_ControlFET(hs_position, SET_OFF_REQUEST);*/
						hs_pid_setpoint[hs_position] = SET_OFF_REQUEST;
					}
				}
			}
			else
			{
				//Do nothing
			}
		}
		else
		{
			//Do nothing
		}
	}
}

/****************************************************************************************/
/*									hsF_HeatDTC    								        */
/****************************************************************************************/
/* Calls from                                                  */
/* --------------                                              */
/* main :                                                      */
/*    hsF_HeatDTC()                                            */
/*                                                             */
/* Calls to                                                    */
/* --------                                                    */
/* FMemLibF_ReportDtc(DtcType DtcCode,enum ERROR_STATES State) */
@far void hsF_HeatDTC(void)
{
	switch (hs_seat_fault_counter_c) {
		/* Case FL side */
		case 0:
		{
			/* FL Seat Short to GND, Short to BAT, OpenLoad and Partial Open Load DTC Register */
			/* If the LF Seat Heater Control Cicuit is Short to BAT. */
			if ( hs_status_flags_c[ FRONT_LEFT ].b.hs_final_short_bat_b ) {

				/* Short to BAT Present */
				/* Store the Active DTC for FL */
				FMemLibF_ReportDtc(DTC_HS_FL_SHORT_TO_BAT_K, ERROR_ON_K);

				/* Change the status of other DTCs for FL in case DTC is present stored */
				FMemLibF_ReportDtc(DTC_HS_FL_SHORT_TO_GND_K, ERROR_OFF_K);
				FMemLibF_ReportDtc(DTC_HS_FL_OPEN_FULL_K, ERROR_OFF_K);
				FMemLibF_ReportDtc(DTC_HS_FL_OPEN_PARTIAL_K, ERROR_OFF_K);
			}
			else {
				/* If the LF Seat Heater Control Circuit is Short to GND */
				if (hs_status_flags_c[ FRONT_LEFT ].b.hs_final_short_gnd_b) {
					/* Short to GND Present */
					
					/* Store the Active DTC for FL */
					FMemLibF_ReportDtc(DTC_HS_FL_SHORT_TO_GND_K, ERROR_ON_K);
					
					/* Change the status of other DTCs for FL in case stored */
					FMemLibF_ReportDtc(DTC_HS_FL_OPEN_FULL_K, ERROR_OFF_K);
					FMemLibF_ReportDtc(DTC_HS_FL_OPEN_PARTIAL_K, ERROR_OFF_K);
				}
				else {
					/* If the LF Seat Heater Control Cicuit is Full Open Load */
					if ( hs_status_flags_c[ FRONT_LEFT ].b.hs_final_open_b ) {
						/* If Full Open Present */
						
						/* Store the Active DTC for FL */
						FMemLibF_ReportDtc(DTC_HS_FL_OPEN_FULL_K, ERROR_ON_K);
						
						/* Change the status of other DTCs for FL in case stored */
						FMemLibF_ReportDtc(DTC_HS_FL_SHORT_TO_GND_K, ERROR_OFF_K);
						//FMemLibF_ReportDtc(DTC_HS_FL_SHORT_TO_BAT_K, ERROR_OFF_K);
						FMemLibF_ReportDtc(DTC_HS_FL_OPEN_PARTIAL_K, ERROR_OFF_K);
					}
					else {
						/* If the LF Seat Heater Control Circuit is with Partial Open */
						if (hs_status_flags_c[ FRONT_LEFT ].b.hs_final_partial_open_b) {
							/* If Partial Open Present */
							
							/* Store the Active DTC for FL */
							FMemLibF_ReportDtc(DTC_HS_FL_OPEN_PARTIAL_K, ERROR_ON_K);
							
							/* Change the status of other DTCs for FL in case stored */
							FMemLibF_ReportDtc(DTC_HS_FL_OPEN_FULL_K, ERROR_OFF_K);
							FMemLibF_ReportDtc(DTC_HS_FL_SHORT_TO_GND_K, ERROR_OFF_K);
						}
						else {
							/* When NO Fault Present */
							/* Change the status of other DTCs for FL in case DTC is present stored */
							FMemLibF_ReportDtc(DTC_HS_FL_SHORT_TO_BAT_K, ERROR_OFF_K);
							FMemLibF_ReportDtc(DTC_HS_FL_SHORT_TO_GND_K, ERROR_OFF_K);
							FMemLibF_ReportDtc(DTC_HS_FL_OPEN_FULL_K, ERROR_OFF_K);
							FMemLibF_ReportDtc(DTC_HS_FL_OPEN_PARTIAL_K, ERROR_OFF_K);
						}
					}
				}
			}
			hs_seat_fault_counter_c = 1;
			break;
		}

		/* Case FR Side */
		case 1:
		{
			/* FR Seat Short to GND, Short to BAT, OpenLoad and Partial Open Load DTC Register */
			/* If the LF Seat Heater Control Cicuit is Short to BAT */
			if ( hs_status_flags_c[ FRONT_RIGHT ].b.hs_final_short_bat_b ) {
				/* Short to BAT Present */
				
				/* Store the Active DTC for FL */
				FMemLibF_ReportDtc(DTC_HS_FR_SHORT_TO_BAT_K, ERROR_ON_K);
				
				/* Change the status of other DTCs for FL in case DTC is present stored */
				FMemLibF_ReportDtc(DTC_HS_FR_SHORT_TO_GND_K, ERROR_OFF_K);
				FMemLibF_ReportDtc(DTC_HS_FR_OPEN_FULL_K, ERROR_OFF_K);
				FMemLibF_ReportDtc(DTC_HS_FR_OPEN_PARTIAL_K, ERROR_OFF_K);
			}
			else {
				/* If the LF Seat Heater Control Circuit is Short to GND */
				if (hs_status_flags_c[ FRONT_RIGHT ].b.hs_final_short_gnd_b) {
					/* Short to GND Present */
					
					/* Store the Active DTC for FL */
					FMemLibF_ReportDtc(DTC_HS_FR_SHORT_TO_GND_K, ERROR_ON_K);
					
					/* Change the status of other DTCs for FL in case stored */
					FMemLibF_ReportDtc(DTC_HS_FR_OPEN_FULL_K, ERROR_OFF_K);
					FMemLibF_ReportDtc(DTC_HS_FR_OPEN_PARTIAL_K, ERROR_OFF_K);
				}
				else {
					/* If the LF Seat Heater Control Cicuit is Full Open Load */
					if ( hs_status_flags_c[ FRONT_RIGHT ].b.hs_final_open_b ) {
						
						/* If Full Open Present */
						/* Store the Active DTC for FL */
						FMemLibF_ReportDtc(DTC_HS_FR_OPEN_FULL_K, ERROR_ON_K);
						
						/* Change the status of other DTCs for FL in case stored */
						FMemLibF_ReportDtc(DTC_HS_FR_SHORT_TO_GND_K, ERROR_OFF_K);
						FMemLibF_ReportDtc(DTC_HS_FR_OPEN_PARTIAL_K, ERROR_OFF_K);
					}
					else {
						/* If the LF Seat Heater Control Circuit is with Partial Open */
						if (hs_status_flags_c[ FRONT_RIGHT ].b.hs_final_partial_open_b) {
							/* If Partial Open Present */
							/* Store the Active DTC for FL */
							FMemLibF_ReportDtc(DTC_HS_FR_OPEN_PARTIAL_K, ERROR_ON_K);
							
							/* Change the status of other DTCs for FL in case stored */
							FMemLibF_ReportDtc(DTC_HS_FR_OPEN_FULL_K, ERROR_OFF_K);
							FMemLibF_ReportDtc(DTC_HS_FR_SHORT_TO_GND_K, ERROR_OFF_K);
						}
						else {
							/* When NO Fault Present */
							/* Change the status of other DTCs for FL in case DTC is present stored */
							FMemLibF_ReportDtc(DTC_HS_FR_SHORT_TO_BAT_K, ERROR_OFF_K);
							FMemLibF_ReportDtc(DTC_HS_FR_SHORT_TO_GND_K, ERROR_OFF_K);
							FMemLibF_ReportDtc(DTC_HS_FR_OPEN_FULL_K, ERROR_OFF_K);
							FMemLibF_ReportDtc(DTC_HS_FR_OPEN_PARTIAL_K, ERROR_OFF_K);
						}
					}
				}
			}

			hs_seat_fault_counter_c = 2;
			break;
		}
		
		/* Case RL side */
		case 2:
		{
			if ((CSWM_config_c & CSWM_4HS_VARIANT_MASK_K) == CSWM_4HS_VARIANT_MASK_K)
			{
				/* RL Seat Short to GND, Short to BAT, OpenLoad and Partial Open Load DTC Register */
				/* If the LF Seat Heater Control Cicuit is Short to BAT */
				if ( hs_status_flags_c[ REAR_LEFT ].b.hs_final_short_bat_b )
				{
					/* Short to BAT Present */
					/* Store the Active DTC for RL */
					FMemLibF_ReportDtc(DTC_HS_RL_SHORT_TO_BAT_K, ERROR_ON_K);
					
					/* Change the status of other DTCs for FL in case DTC is present stored */
					FMemLibF_ReportDtc(DTC_HS_RL_SHORT_TO_GND_K, ERROR_OFF_K);
					FMemLibF_ReportDtc(DTC_HS_RL_OPEN_FULL_K, ERROR_OFF_K);
					FMemLibF_ReportDtc(DTC_HS_RL_OPEN_PARTIAL_K, ERROR_OFF_K);
				}
				else
				{
					/* If the LF Seat Heater Control Circuit is Short to GND */
					if (hs_status_flags_c[ REAR_LEFT ].b.hs_final_short_gnd_b)
					{
						/* Short to GND Present */
						/* Store the Active DTC for RL */
						FMemLibF_ReportDtc(DTC_HS_RL_SHORT_TO_GND_K, ERROR_ON_K);
						
						/* Change the status of other DTCs for FL in case stored */
						FMemLibF_ReportDtc(DTC_HS_RL_OPEN_FULL_K, ERROR_OFF_K);
						FMemLibF_ReportDtc(DTC_HS_RL_OPEN_PARTIAL_K, ERROR_OFF_K);
					}
					else
					{
						/* If the LF Seat Heater Control Cicuit is Full Open Load */
						if ( hs_status_flags_c[ REAR_LEFT ].b.hs_final_open_b )
						{
							/* If Full Open Present */
							/* Store the Active DTC for RL */
							FMemLibF_ReportDtc(DTC_HS_RL_OPEN_FULL_K, ERROR_ON_K);
							
							/* Change the status of other DTCs for FL in case stored */
							FMemLibF_ReportDtc(DTC_HS_RL_SHORT_TO_GND_K, ERROR_OFF_K);
							FMemLibF_ReportDtc(DTC_HS_RL_OPEN_PARTIAL_K, ERROR_OFF_K);
						}
						else
						{
							/* If the LF Seat Heater Control Circuit is with Partial Open */
							if (hs_status_flags_c[ REAR_LEFT ].b.hs_final_partial_open_b)
							{
								/* If Partial Open Present */
								/* Store the Active DTC for RL */
								FMemLibF_ReportDtc(DTC_HS_RL_OPEN_PARTIAL_K, ERROR_ON_K);
								
								/* Change the status of other DTCs for FL in case stored */
								FMemLibF_ReportDtc(DTC_HS_RL_OPEN_FULL_K, ERROR_OFF_K);
								FMemLibF_ReportDtc(DTC_HS_RL_SHORT_TO_GND_K, ERROR_OFF_K);
							}
							else
							{
								/* When NO Fault Present */
								/* Change the status of other DTCs for FL in case DTC is present stored */
								FMemLibF_ReportDtc(DTC_HS_RL_SHORT_TO_BAT_K, ERROR_OFF_K);
								FMemLibF_ReportDtc(DTC_HS_RL_SHORT_TO_GND_K, ERROR_OFF_K);
								FMemLibF_ReportDtc(DTC_HS_RL_OPEN_FULL_K, ERROR_OFF_K);
								FMemLibF_ReportDtc(DTC_HS_RL_OPEN_PARTIAL_K, ERROR_OFF_K);
							}
						}
					}
				}
				hs_seat_fault_counter_c = 3;
			}
			else
			{
				hs_seat_fault_counter_c = 0;
			}

			break;
		}
		
		/* Case RR Side */
		case 3:
		{

			if ((CSWM_config_c & CSWM_4HS_VARIANT_MASK_K) == CSWM_4HS_VARIANT_MASK_K)
			{
				/* RR Seat Short to GND, Short to BAT, OpenLoad and Partial Open Load DTC Register */
				/* If the LF Seat Heater Control Cicuit is Short to BAT */
				if ( hs_status_flags_c[ REAR_RIGHT ].b.hs_final_short_bat_b )
				{
					/* Short to BAT Present */
					/* Store the Active DTC for FL */
					FMemLibF_ReportDtc(DTC_HS_RR_SHORT_TO_BAT_K, ERROR_ON_K);

					/* Change the status of other DTCs for FL in case DTC is present stored */
					FMemLibF_ReportDtc(DTC_HS_RR_SHORT_TO_GND_K, ERROR_OFF_K);
					FMemLibF_ReportDtc(DTC_HS_RR_OPEN_FULL_K, ERROR_OFF_K);
					FMemLibF_ReportDtc(DTC_HS_RR_OPEN_PARTIAL_K, ERROR_OFF_K);
				}
				else
				{
					/* If the LF Seat Heater Control Circuit is Short to GND */
					if (hs_status_flags_c[ REAR_RIGHT ].b.hs_final_short_gnd_b)
					{
						/* Short to GND Present */
						/* Store the Active DTC for FL */
						FMemLibF_ReportDtc(DTC_HS_RR_SHORT_TO_GND_K, ERROR_ON_K);

						/* Change the status of other DTCs for FL in case stored */

						//FMemLibF_ReportDtc(DTC_HS_RR_SHORT_TO_BAT_K, ERROR_OFF_K);
						FMemLibF_ReportDtc(DTC_HS_RR_OPEN_FULL_K, ERROR_OFF_K);
						FMemLibF_ReportDtc(DTC_HS_RR_OPEN_PARTIAL_K, ERROR_OFF_K);
					}
					else
					{
						/* If the LF Seat Heater Control Cicuit is Full Open Load */
						if ( hs_status_flags_c[ REAR_RIGHT ].b.hs_final_open_b )
						{
							/* If Full Open Present */
							/* Store the Active DTC for FL */
							FMemLibF_ReportDtc(DTC_HS_RR_OPEN_FULL_K, ERROR_ON_K);

							/* Change the status of other DTCs for FL in case stored */
							FMemLibF_ReportDtc(DTC_HS_RR_SHORT_TO_GND_K, ERROR_OFF_K);
							//FMemLibF_ReportDtc(DTC_HS_RR_SHORT_TO_BAT_K, ERROR_OFF_K);
							FMemLibF_ReportDtc(DTC_HS_RR_OPEN_PARTIAL_K, ERROR_OFF_K);
						}
						else
						{
							/* If the LF Seat Heater Control Circuit is with Partial Open */
							if (hs_status_flags_c[ REAR_RIGHT ].b.hs_final_partial_open_b)
							{
								/* If Partial Open Present */
								/* Store the Active DTC for FL */
								FMemLibF_ReportDtc(DTC_HS_RR_OPEN_PARTIAL_K, ERROR_ON_K);

								/* Change the status of other DTCs for FL in case stored */

								//FMemLibF_ReportDtc(DTC_HS_RR_SHORT_TO_BAT_K, ERROR_OFF_K);
								FMemLibF_ReportDtc(DTC_HS_RR_OPEN_FULL_K, ERROR_OFF_K);
								FMemLibF_ReportDtc(DTC_HS_RR_SHORT_TO_GND_K, ERROR_OFF_K);
							}
							else
							{
								/* When NO Fault Present */
								/* Change the status of other DTCs for FL in case DTC is present stored */
								FMemLibF_ReportDtc(DTC_HS_RR_SHORT_TO_BAT_K, ERROR_OFF_K);
								FMemLibF_ReportDtc(DTC_HS_RR_SHORT_TO_GND_K, ERROR_OFF_K);
								FMemLibF_ReportDtc(DTC_HS_RR_OPEN_FULL_K, ERROR_OFF_K);
								FMemLibF_ReportDtc(DTC_HS_RR_OPEN_PARTIAL_K, ERROR_OFF_K);
							}
						}
					}
				}
				hs_seat_fault_counter_c = 4;
			}
			else
			{
				hs_seat_fault_counter_c = 0;
			}

			break;
		}

		/* Case No Rear Battery */
		case 4:
		{
			if ((CSWM_config_c & CSWM_4HS_VARIANT_MASK_K) == CSWM_4HS_VARIANT_MASK_K)
			{
				/* No Rear Seat Battery */
				if (Rear_Seats_no_power_final_b)
				{
					/* Store the Active DTC for RL */
					FMemLibF_ReportDtc(DTC_NO_BAT_FOR_REAR_SEATS_K, ERROR_ON_K);
				}
				else
				{
					/* Store the Active DTC for RL */
					FMemLibF_ReportDtc(DTC_NO_BAT_FOR_REAR_SEATS_K, ERROR_OFF_K);
				}
			}
			hs_seat_fault_counter_c = 0;
			break;
		}
		
		default:
		{
			hs_seat_fault_counter_c = 0;
		}
	}
}

/* SDO@hsF_NTCDTC@ */
/*                                                             */
/* History                                                     */
/* --------                                                    */
/* created by YekolluH 01/29/2007 10:40:05                     */
/* modified by YekolluH 11/05/2007 08:33:28                    */
/*                                                             */
/* Labels                                                      */
/* ------                                                      */
/* Status : in process                                         */
/*                                                             */
/* SA Text                                                     */
/* -------                                                     */
/* no text                                                     */
/*                                                             */
/* SD Text                                                     */
/* -------                                                     */
/* no text                                                     */
/*                                                             */
/* Calls from                                                  */
/* --------------                                              */
/* main :                                                      */
/*    hsF_NTCDTC()                                             */
/*                                                             */
/* Calls to                                                    */
/* --------                                                    */
/* FMemLibF_ReportDtc(DtcType DtcCode,enum ERROR_STATES State) */
/* void hsF_NTCDTC(void) */
@far void hsF_NTCDTC(void)
{
	/* If there is no NTC Power supply fault, then only blow the individual NTC faults if detected. */
	if (!NTC_Supply_Voltage_Flt_b)
	{
		switch (hs_ntc_fault_counter_c) {

			/* case FL NTC DTC */
			case 0:
			{
				/* FL NTC DTC Register */
				if (temperature_HS_NTC_status[FRONT_LEFT] == /*NTC_MTRD_OPEN_MASK*/ NTC_OPEN_CKT)
				{
					/* Store the Active DTC for FL NTC */
					FMemLibF_ReportDtc(DTC_FL_HEAT_NTC_HIGH_K, ERROR_ON_K);

					/* Change the status of other DTCs for FL in case DTC is present stored */
					FMemLibF_ReportDtc(DTC_FL_HEAT_NTC_LOW_K, ERROR_OFF_K);
				}
				else
				{
					if (temperature_HS_NTC_status[FRONT_LEFT] == /*NTC_MTRD_GND_MASK*/ NTC_SHRT_TO_GND)
					{
						/* Store the Active DTC for FL NTC */
						FMemLibF_ReportDtc(DTC_FL_HEAT_NTC_LOW_K, ERROR_ON_K);

						/* Change the status of other DTCs for FL in case DTC is present stored */
						FMemLibF_ReportDtc(DTC_FL_HEAT_NTC_HIGH_K, ERROR_OFF_K);
					}
					else
					{
						/* Change the status of other DTCs for FL in case DTC is present stored */
						FMemLibF_ReportDtc(DTC_FL_HEAT_NTC_LOW_K, ERROR_OFF_K);

						/* Change the status of other DTCs for FL in case DTC is present stored */
						FMemLibF_ReportDtc(DTC_FL_HEAT_NTC_HIGH_K, ERROR_OFF_K);
					}
				}
				hs_ntc_fault_counter_c = 1;

				break;
			}

			/* Case FR NTC DTC */
			case 1:
			{
				/* FR NTC DTC Register */
				if (temperature_HS_NTC_status[FRONT_RIGHT] == /*NTC_MTRD_OPEN_MASK*/ NTC_OPEN_CKT)
				{
					/* Store the Active DTC for FL NTC */
					FMemLibF_ReportDtc(DTC_FR_HEAT_NTC_HIGH_K, ERROR_ON_K);
					
					/* Change the status of other DTCs for FL in case DTC is present stored */
					FMemLibF_ReportDtc(DTC_FR_HEAT_NTC_LOW_K, ERROR_OFF_K);
				}
				else
				{
					if (temperature_HS_NTC_status[FRONT_RIGHT] == /*NTC_MTRD_GND_MASK*/NTC_SHRT_TO_GND)
					{
						/* Store the Active DTC for FL NTC */
						FMemLibF_ReportDtc(DTC_FR_HEAT_NTC_LOW_K, ERROR_ON_K);

						/* Change the status of other DTCs for FL in case DTC is present stored */
						FMemLibF_ReportDtc(DTC_FR_HEAT_NTC_HIGH_K, ERROR_OFF_K);
					}
					else
					{
						/* Change the status of other DTCs for FL in case DTC is present stored */
						FMemLibF_ReportDtc(DTC_FR_HEAT_NTC_LOW_K, ERROR_OFF_K);

						/* Change the status of other DTCs for FL in case DTC is present stored */
						FMemLibF_ReportDtc(DTC_FR_HEAT_NTC_HIGH_K, ERROR_OFF_K);
					}
				}
				hs_ntc_fault_counter_c = 2;

				break;
			}

			/* Case RL NTC DTC */
			case 2:
			{
				if ((CSWM_config_c & CSWM_4HS_VARIANT_MASK_K) == CSWM_4HS_VARIANT_MASK_K)
				{
					/* RL NTC DTC Register */
					if (temperature_HS_NTC_status[REAR_LEFT] == /*NTC_MTRD_OPEN_MASK*/ NTC_OPEN_CKT)
					{
						/* Store the Active DTC for FL NTC */
						FMemLibF_ReportDtc(DTC_RL_HEAT_NTC_HIGH_K, ERROR_ON_K);
						
						/* Change the status of other DTCs for FL in case DTC is present stored */
						FMemLibF_ReportDtc(DTC_RL_HEAT_NTC_LOW_K, ERROR_OFF_K);
					}
					else
					{
						if (temperature_HS_NTC_status[REAR_LEFT] == /*NTC_MTRD_GND_MASK*/ NTC_SHRT_TO_GND)
						{
							/* Store the Active DTC for FL NTC */
							FMemLibF_ReportDtc(DTC_RL_HEAT_NTC_LOW_K, ERROR_ON_K);

							/* Change the status of other DTCs for FL in case DTC is present stored */
							FMemLibF_ReportDtc(DTC_RL_HEAT_NTC_HIGH_K, ERROR_OFF_K);
						}
						else
						{
							/* Change the status of other DTCs for FL in case DTC is present stored */
							FMemLibF_ReportDtc(DTC_RL_HEAT_NTC_LOW_K, ERROR_OFF_K);
							
							/* Change the status of other DTCs for FL in case DTC is present stored */
							FMemLibF_ReportDtc(DTC_RL_HEAT_NTC_HIGH_K, ERROR_OFF_K);
						}
					}
					hs_ntc_fault_counter_c = 3;
				}
				else
				{
					hs_ntc_fault_counter_c = 0;
				}

				break;
			}
			
			/* Case RR NTC DTC */
			case 3:
			{
				if ((CSWM_config_c & CSWM_4HS_VARIANT_MASK_K) == CSWM_4HS_VARIANT_MASK_K)
				{
					/* RR NTC DTC Register */
					if (temperature_HS_NTC_status[REAR_RIGHT] == /*NTC_MTRD_OPEN_MASK*/ NTC_OPEN_CKT)
					{
						/* Store the Active DTC for FL NTC */
						FMemLibF_ReportDtc(DTC_RR_HEAT_NTC_HIGH_K, ERROR_ON_K);

						/* Change the status of other DTCs for FL in case DTC is present stored */
						FMemLibF_ReportDtc(DTC_RR_HEAT_NTC_LOW_K, ERROR_OFF_K);
					}
					else
					{
						if (temperature_HS_NTC_status[REAR_RIGHT] == /*NTC_MTRD_GND_MASK*/ NTC_SHRT_TO_GND)
						{
							/* Store the Active DTC for FL NTC */
							FMemLibF_ReportDtc(DTC_RR_HEAT_NTC_LOW_K, ERROR_ON_K);

							/* Change the status of other DTCs for FL in case DTC is present stored */
							FMemLibF_ReportDtc(DTC_RR_HEAT_NTC_HIGH_K, ERROR_OFF_K);
						}
						else
						{
							/* Change the status of other DTCs for FL in case DTC is present stored */
							FMemLibF_ReportDtc(DTC_RR_HEAT_NTC_LOW_K, ERROR_OFF_K);

							/* Change the status of other DTCs for FL in case DTC is present stored */
							FMemLibF_ReportDtc(DTC_RR_HEAT_NTC_HIGH_K, ERROR_OFF_K);
						}
					}
				}
				hs_ntc_fault_counter_c = 0;

				break;
			}
			
			default:
			{
				hs_ntc_fault_counter_c = 0;
			}
		}
	}
}


/*****************************************************************************/
/*                                                                           */
/* Calls from                                                                */
/* --------------                                                            */
/* Main :                                                                    */
/*                                                                           */
/* Calls to                                                                  */
/* --------                                                                  */
/* hsLF_UpdateHeatParameters                                                 */
/* FMemLibF_ReportDtc														 */
/*****************************************************************************/
@far void hsF_RearSwitchStuckDTC (void)
{
	/* This function gets called every 40ms to see the switch stuck happend for rear seats        		   */
	/* Step 1 : Start the function Fault detection only if IGN is in RUN/Start position                    */ 
	/* Step 2 : Read the status of Rear Switch Inputs                                              		   */
	/* Step 3 : If any of Rear seat switch input is LOW (Pressed), start the fault detection.              */
	/*          Step 3A deals with Mature criteria, Step 3B deals after mature criteria met                */
	/*          If any Rear seat switch input is HIGH (Not pressed) looks for demature criteria (Step 4)   */
	/* Step 3A: Mature counter reaches 15sec, depending on seat side, sets the stuck switch pressed flag   */
	/*          and turn OFF the Rear Seat Output                                                          */
	/*          If the flag is set means for rest of IGN cycle, we will not detect the fault for same seat */
	/*          If counter not matured, logic increments the mature time counter till it reaches 15sec.    */
	/* Step 3B: Once the mature criteria met, logic blows the DTC into eeprom                              */
	/* Step 4 : De Mature criteria:                                                                        */
	/*          If demature time reached 2sec time, depending on seat side logic changes DTC status(STORED)*/ 
	/*          Else Logic waits for 2sec                                                                  */
	
	//STEP 1
	if((canio_RX_IGN_STATUS_c == IGN_RUN) || (canio_RX_IGN_STATUS_c == IGN_START))
	{
		unsigned char hs_rearseat;
		//STEP 2
		//Get the status of Rear Seat Switch Inputs
		hs_rear_swth_stat_c[RL_SWITCH] = (unsigned char) Dio_ReadChannel(RL_LED_SW_DETECT);
		hs_rear_swth_stat_c[RR_SWITCH] = (unsigned char) Dio_ReadChannel(RR_LED_SW_DETECT);
		
		for (hs_rearseat = RL_SWITCH; hs_rearseat < ALL_REAR_SWITCH; hs_rearseat++) {
			
			//STEP 3
			//Check to see if status is in PRESSED condition
			if(hs_rear_swth_stat_c[hs_rearseat] == STK_PRESS_K)
			{
				//STEP 3A
				hs_rear_demature_counter_c[hs_rearseat] = 0;
				//STEP 3A: Check to see is the fault already matured in the same IGN cycle
				//If Yes goto STEP 3B, do not do the fault detection.
				if (hs_rear_seat_flags_c[ hs_rearseat ].b.stksw_psd == FALSE)
				{
					//STEP 3A
					hs_rear_demature_counter_c[hs_rearseat] = 0;
					
					//Check the wait time, it not matured increment the counter, else Report the DTC
					if(hs_rear_mature_counter_w[hs_rearseat] >= hs_rear_mature_time_w)
					{
						if(hs_rearseat == RL_SWITCH)
						{
							
							/* We have reached the 2sec time, turn OFF completely */
							hsLF_UpdateHeatParameters(REAR_LEFT, HEAT_LEVEL_OFF);
							
							/* Set the remember seat req status to OFF */
							hs_rem_seat_req_c[REAR_LEFT] = hs_switch_request_to_fet_c[REAR_LEFT];
							
						}
						else
						{
							/* We have reached the 2sec time, turn OFF completely */
							hsLF_UpdateHeatParameters(REAR_RIGHT, HEAT_LEVEL_OFF);
							/* Set the remember seat req status to OFF */
							hs_rem_seat_req_c[REAR_RIGHT] = hs_switch_request_to_fet_c[REAR_RIGHT];

						}
						//Set the flag
						hs_rear_seat_flags_c[ hs_rearseat ].b.stksw_psd = TRUE;
						
					}
					else
					{
						hs_rear_mature_counter_w[hs_rearseat]++;
					}
		
				} 
				else 
				{    
					    //STEP 3B
					    /*Switch is in pressed state and passed 15sec mature time, now enter the DTC to stack */
						if (hs_rearseat == RL_SWITCH)
						{
							/* Store the Active DTC for Rear Left Stuck Switch */
							FMemLibF_ReportDtc(DTC_HS_RL_SWITCH_STUCK_PSD_K, ERROR_ON_K);
						}
						else
						{
							/* Store the Active DTC for Rear Left Stuck Switch */
							FMemLibF_ReportDtc(DTC_HS_RR_SWITCH_STUCK_PSD_K, ERROR_ON_K);
						}
					
				}
			
					
			} // When Switch in PRESSED State Handle Logic/DTC Mature Logic end
			else
			{
				//Step 4 DE MATURE LOGIC
				hs_rear_mature_counter_w[hs_rearseat] = 0;
				
				if(hs_rear_demature_counter_c[hs_rearseat] >= hs_rear_demature_time_c)
				{
					hs_rear_seat_flags_c[ hs_rearseat ].b.stksw_psd = FALSE;
	//     			hs_rear_demature_counter_c[hs_rear] = 0;
					
					/*De mature criteria met. Change the status of DTC from Active to stored */
					if(hs_rearseat == RL_SWITCH)
					{
							/* Clear the Active DTC for Rear Left Stuck Switch */
							FMemLibF_ReportDtc(DTC_HS_RL_SWITCH_STUCK_PSD_K, ERROR_OFF_K);
					}
					else
					{
						/* Clear the Active DTC for Rear Right Stuck Switch */
						FMemLibF_ReportDtc(DTC_HS_RR_SWITCH_STUCK_PSD_K, ERROR_OFF_K);
					}

				}
				else
				{
					/* Increment the de mature counter to reach 2sec */
					hs_rear_demature_counter_c[hs_rearseat]++;
				}
				
			} //When Swich is not pressed handling/DTC de mature logic end
			
		 	
		} //For Loop End
		
		
	}  //End of Ignition status check
	else
	{
		/* 01.21.09: Do not clear the variables with respect to stuck switch.*/
		/* Wait for IGN Status to get changed. */
       
	}

} //Function call end


/* SDO@hsLF_ControlFET@ */
/*                                                                           */
/* History                                                                   */
/* --------                                                                  */
/* created by YekolluH 01/29/2007 13:16:24                                   */
/* modified by YekolluH 10/31/2007 08:25:14                                  */
/*                                                                           */
/* Labels                                                                    */
/* ------                                                                    */
/* Status : in process                                                       */
/*                                                                           */
/* SA Text                                                                   */
/* -------                                                                   */
/* no text                                                                   */
/*                                                                           */
/* SD Text                                                                   */
/* -------                                                                   */
/* no text                                                                   */
/*                                                                           */
/* Calls from                                                                */
/* --------------                                                            */
/* hsF_HeatRequestToFet :                                                    */
/*    hsLF_ControlFET(unsigned char hs_nr,unsigned char duty_cycle)          */
/*                                                                           */
/* Calls to                                                                  */
/* --------                                                                  */
/* StaggerF_Manager(Stagger_Channel_Type Channel_ID,u_16Bit this_duty_cycle) */
/* void hsLF_ControlFET(unsigned char hs_nr,unsigned char duty_cycle) */
/* Parameters Passed are: Seat Position and duty cycle                       */
/* For the particular seat, logic changes the PWM state only incase          */
/* Previously stored duty cycle is different or                              */
/* WHen Set OFF request present (With this always we make sure that O/Ps are */
/* OFF                                                                       */
void hsLF_ControlFET(unsigned char seatpos, unsigned char duty_cycle)
{
	
	/* Check for new request or off request */
	if ((hs_temp_heat_pwm_c[seatpos] != duty_cycle) || (duty_cycle == SET_OFF_REQUEST) ) {

		/* Store duty cycle */
		hs_temp_heat_pwm_c[seatpos] = duty_cycle;
		
		/* Check if this is an off request */
		if (duty_cycle != SET_OFF_REQUEST) {
			
			/* Heater output state is On */
			hs_status2_flags_c[seatpos].b.hs_heater_state_b = TRUE;
		}
		else {
			/* Heater output state is off */
			hs_status2_flags_c[seatpos].b.hs_heater_state_b = FALSE;
		}

		/* MKS: 22312 */
		/* Get the latest Autosar PWM value */
		hs_autosar_pwm_w[seatpos] = mainF_Calc_PWM(duty_cycle);

		/* Stagger Hs outputs */
		switch (seatpos) {
			
			/* FRONT_LEFT */
			case FRONT_LEFT:
			{
				//Pwm_SetDutyCycle(PWM_HS_FL, hs_autosar_pwm_w[FRONT_LEFT]); 
				StaggerF_Manager(FL_TIM_CH, hs_autosar_pwm_w[FRONT_LEFT]);				
				break;
			}
			
			/* FRONT_RIGHT */
			case FRONT_RIGHT:
			{
				//Pwm_SetDutyCycle(PWM_HS_FR, hs_autosar_pwm_w[FRONT_RIGHT]); 
				StaggerF_Manager(FR_TIM_CH, hs_autosar_pwm_w[FRONT_RIGHT]);
				break;
			}
			
			/* REAR_LEFT */
			case REAR_LEFT:
			{
				
				//Pwm_SetDutyCycle(PWM_HS_RL, hs_autosar_pwm_w[REAR_LEFT]); 
				StaggerF_Manager(RL_TIM_CH, hs_autosar_pwm_w[REAR_LEFT]);
				break;
			}

			/* REAR_RIGHT */
			case REAR_RIGHT:
			{
				//Pwm_SetDutyCycle(PWM_HS_RR, hs_autosar_pwm_w[REAR_RIGHT]); 
				StaggerF_Manager(RR_TIM_CH, hs_autosar_pwm_w[REAR_RIGHT]);
				break;
			}
			
			default:
			{
				
			}
		}
	}
	else
	{
		//PWM duty cycle was not changed. Stay at present heat level
	}
}

/* SDO@hsLF_2SecMonitorTime_FaultDetection@ */
/*                                                                                                                                                                             */
/* History                                                                                                                                                                     */
/* --------                                                                                                                                                                    */
/* created by YekolluH 01/29/2007 13:19:30                                                                                                                                     */
/* modified by YekolluH 09/20/2007 15:11:05                                                                                                                                    */
/*                                                                                                                                                                             */
/* Labels                                                                                                                                                                      */
/* ------                                                                                                                                                                      */
/* Status : in process                                                                                                                                                         */
/*                                                                                                                                                                             */
/* SA Text                                                                                                                                                                     */
/* -------                                                                                                                                                                     */
/* no text                                                                                                                                                                     */
/*                                                                                                                                                                             */
/* SD Text                                                                                                                                                                     */
/* -------                                                                                                                                                                     */
/* This function gets called from hsF_HeatManager every 10ms timeslice.                                                                                                        */
/*                                                                                                                                                                             */
/* Purpose of this function to wait 2sec time to mature the fault.                                                                                                             */
/*                                                                                                                                                                             */
/* If any of the Primary faults detected for the heater outputs, this function will increment the 2sec timer.                                                                  */
/* Once Logic crosses 2sec, sets the Final fault for the particular output.                                                                                                    */
/*                                                                                                                                                                             */
/* Small deviation in Setting Final flag for TRUE OPEN:                                                                                                                        */
/* To distinguish the faulty type, when ever the module detects open, after two second mature time outputs will be turned OFF but will be not be setting the Final Open flag.  */
/* Logic waits another 2seconds to check wether it is a STG fault or Open fault. If Module detects STG Logic sets the Final flag for STB else it sets the final flag for OPEN. */
/*                                                                                                                                                                             */
/*                                                                                                                                                                             */
/* Calls from                                                                                                                                                                  */
/* --------------                                                                                                                                                              */
/* hsF_HeatManager :                                                                                                                                                           */
/*    hsLF_2SecMonitorTime_FaultDetection()                                                                                                                                    */
/*                                                                                                                                                                             */
/* Calls to                                                                                                                                                                    */
/* --------                                                                                                                                                                    */
/* no calls                                                                                                                                                                    */
/* void hsLF_2SecMonitorTime_FaultDetection(void) */
//void hsLF_2SecMonitorTime_FaultDetection(unsigned char hs_nr, unsigned char fault)
void hsLF_2SecMonitorTime_FaultDetection(unsigned char hs_seatpos)
{
	
#define HS_PART_OPEN_LOOP_K        449   //15 min wait time for Partial OPEN maturity
	
	/* Check to see if any of the Preliminary flags set. If not for safe clear all the preliminary flags and  clear the monitor fault counter. */
	if (hs_status_flags_c[ hs_seatpos ].b.hs_prelim_short_bat_b ||
	    hs_status_flags_c[ hs_seatpos ].b.hs_prelim_short_gnd_b ||
	    hs_status_flags_c[ hs_seatpos ].b.hs_prelim_open_b      ||
	    hs_status_flags_c[ hs_seatpos ].b.hs_prelim_partial_open_b ||
	    hs_status3_flags_c[ hs_seatpos ].b.final_STB_or_OPEN_b) {

		/* Wait untill the 2second timer counts to greater than 2sec */
		if (hs_monitor_prelim_fault_counter_c[hs_seatpos] >= /*mature_2sec_time_c*/hs_flt_dtc_c[HS_2SEC_MATURE_TIME]) {

			/* Enter inside if Preliminary flag is for Short to Bat */
			if (hs_status_flags_c[ hs_seatpos ].b.hs_prelim_short_bat_b) {

				/* Set the Final Fault flag */
				hs_status_flags_c[ hs_seatpos ].b.hs_final_short_bat_b = TRUE;

				/* Clear the Preliminary Fault flag for Short to Bat */
				hs_status_flags_c[ hs_seatpos ].b.hs_prelim_short_bat_b = FALSE;

				/* For safe locking clear all other preliminary flags */
				hs_status_flags_c[ hs_seatpos ].b.hs_prelim_short_gnd_b = FALSE;
				hs_status_flags_c[ hs_seatpos ].b.hs_prelim_open_b = FALSE;
				hs_status_flags_c[ hs_seatpos ].b.hs_prelim_partial_open_b = FALSE;

				/* Waited 2 more seconds. Confirmed that it is Short to BAT. Blow the STB instead Open */
				hs_status3_flags_c[ hs_seatpos ].b.final_STB_or_OPEN_b = FALSE;
			}
			else {

				/* Enter inside if Preliminary flag is for Short to gnd */
				if (hs_status_flags_c[ hs_seatpos ].b.hs_prelim_short_gnd_b) {
					/* Set the Final Fault flag */
					hs_status_flags_c[ hs_seatpos ].b.hs_final_short_gnd_b = TRUE;

					/* Clear the Preliminary Fault flag for Short to GND */
					hs_status_flags_c[ hs_seatpos ].b.hs_prelim_short_gnd_b = FALSE;

					/* For safe locking, clear all other preliminary flags */
					hs_status_flags_c[ hs_seatpos ].b.hs_prelim_short_bat_b = FALSE;
					hs_status_flags_c[ hs_seatpos ].b.hs_prelim_open_b = FALSE;
					hs_status_flags_c[ hs_seatpos ].b.hs_prelim_partial_open_b = FALSE;
				}
				else {

					/* Enter inside if Preliminary flag is for Open */
					if (hs_status_flags_c[ hs_seatpos ].b.hs_prelim_open_b) {
						/* Set the Final Fault flag */
						//hs_status_flags_c[ hs_seatpos ].b.hs_final_open_b = TRUE;

						/* We are uncertain that is this a ture OPEN or not. Wait 2 more seconds to see. */
						hs_status3_flags_c[ hs_seatpos ].b.final_STB_or_OPEN_b = TRUE;

						/* Clear the Preliminary Fault flag for Partial Open */
						hs_status_flags_c[ hs_seatpos ].b.hs_prelim_open_b = FALSE;

						/* For safe locking, clear all other preliminary flags */
						hs_status_flags_c[ hs_seatpos ].b.hs_prelim_short_bat_b = FALSE;
						hs_status_flags_c[ hs_seatpos ].b.hs_prelim_short_gnd_b = FALSE;
						hs_status_flags_c[ hs_seatpos ].b.hs_prelim_partial_open_b = FALSE;
						
						
					}
					else {

						if (hs_status3_flags_c[ hs_seatpos ].b.final_STB_or_OPEN_b) {
							/* Waited 2 more seconds. COnfirmed that it is True OPEN. Blow the Open DTC */
							hs_status_flags_c[ hs_seatpos ].b.hs_final_open_b = TRUE;

							/* Waited 2 more seconds. Confirmed that there is no Short to BAT. Blow Open Fault and clear the flag. */
							hs_status3_flags_c[ hs_seatpos ].b.final_STB_or_OPEN_b = FALSE;
						}
						else {
							
							
							/* MKS 54709: New condition added to the SW */
							/* When the self tests are activated and observes the Partial OPEN detection */
							/* we wait only 2sec to mature the fault. For regular logic we wait 2min */
							if ( (diag_io_hs_lock_flags_c[hs_seatpos].b.switch_rq_lock_b == TRUE) || 
								 (diag_io_all_hs_lock_b == TRUE) || 
								 (diag_rc_all_heaters_lock_b == TRUE) )
							{
								/*When we are here means we already completed 2sec/ 1 loop */
								hs_monitor_loop_counter_w [hs_seatpos] = HS_PART_OPEN_LOOP_K;
							}
							
							/* MKS 50738: New If condition added for Partial OPEN detection */
							/* Wait time to blow Partail OPEN is increased from 2sec to 2min(120sec) */
							
							/* Is timer reached 2min? Then set the Final Flag , else increment the loop counter till it reaches 60 */
							/*When we are here means we already completed 2sec/ 1 loop */
							if(hs_monitor_loop_counter_w [hs_seatpos] >= HS_PART_OPEN_LOOP_K)
							{
								/* Else it is Partial Open.                  */
								/* Set the Final Fault flag for Partial Open */
								hs_status_flags_c[ hs_seatpos ].b.hs_final_partial_open_b = TRUE;

								/* Clear the Preliminary Fault flag */
								hs_status_flags_c[ hs_seatpos ].b.hs_prelim_partial_open_b = FALSE;

								/* For safe locking, clear all other preliminary flags */
								hs_status_flags_c[ hs_seatpos ].b.hs_prelim_short_bat_b = FALSE;
								hs_status_flags_c[ hs_seatpos ].b.hs_prelim_short_gnd_b = FALSE;
								hs_status_flags_c[ hs_seatpos ].b.hs_prelim_open_b = FALSE;
								
							}
							else
							{
								/* Increment the Loop counter to reach 2min */
								hs_monitor_loop_counter_w [hs_seatpos]++;
								
								/* Clear the monitor time counter, so It goes again and again till it reach 2min */
								hs_monitor_prelim_fault_counter_c[hs_seatpos] = 0;

							}
							
						}
					}
				}
			}

			/* Moved the below 3 statements inside IF statement */
//			/* We have reached the 2sec time, turn OFF completely */
//			hsLF_UpdateHeatParameters(hs_seatpos, HEAT_LEVEL_OFF);
//
//			/* Set the remember seat req status to OFF */
//			hs_rem_seat_req_c[hs_seatpos] = hs_switch_request_to_fet_c[hs_seatpos];
//	
//			/* Clear the monitor time counter */
//			hs_monitor_prelim_fault_counter_c[hs_seatpos] = 0;
			
			/* If any of the faults set for the heater, a flag will be set for other functions*/
			/* to know, there is one fault for heaters . Added for LX 08.51.00*/
			if(hs_status_flags_c[ hs_seatpos ].b.hs_final_short_bat_b ||
			   hs_status_flags_c[ hs_seatpos ].b.hs_final_short_gnd_b ||
			   hs_status_flags_c[ hs_seatpos ].b.hs_final_open_b      ||
			   hs_status_flags_c[ hs_seatpos ].b.hs_final_partial_open_b ||
			   hs_status3_flags_c[ hs_seatpos ].b.final_STB_or_OPEN_b )
			{
				hs_status2_flags_c[ hs_seatpos ].b.hs_output_fault_set_b = TRUE;

				/* We have reached the wait time, turn OFF completely */
				hsLF_UpdateHeatParameters(hs_seatpos, HEAT_LEVEL_OFF);

				/* Set the remember seat req status to OFF */
				hs_rem_seat_req_c[hs_seatpos] = hs_switch_request_to_fet_c[hs_seatpos];

				/* Clear the monitor time counter */
				hs_monitor_prelim_fault_counter_c[hs_seatpos] = 0;
				hs_monitor_loop_counter_w [hs_seatpos] = 0;
			}
		}
		else {
			/* Not reached 2 sec time. Increment the monitor counter */
			hs_monitor_prelim_fault_counter_c[hs_seatpos]++;
		}
	}
	else {
		/* Clear the monitor time counter */
		hs_monitor_prelim_fault_counter_c[hs_seatpos] = 0;
		hs_monitor_loop_counter_w[hs_seatpos] = 0;
		/* No fault for heats */
		hs_status_flags_c[ hs_seatpos ].b.hs_prelim_short_gnd_b = FALSE;
		hs_status_flags_c[ hs_seatpos ].b.hs_prelim_open_b = FALSE;
		hs_status_flags_c[ hs_seatpos ].b.hs_prelim_partial_open_b = FALSE;
		hs_status_flags_c[ hs_seatpos ].b.hs_prelim_short_bat_b = FALSE;

		/* 29.6.2007 8:24 by kuldukc */
		/* hs_monitor_prelim_fault_counter_c[hs_seatpos] = FALSE; */
	}

/* Moved inside the logic */	
//	/* If any of the faults set for the heater, a flag will be set for other functions*/
//	/* to know, there is one fault for heaters . Added for LX 08.51.00*/
//	if(hs_status_flags_c[ hs_seatpos ].b.hs_final_short_bat_b ||
//	   hs_status_flags_c[ hs_seatpos ].b.hs_final_short_gnd_b ||
//	   hs_status_flags_c[ hs_seatpos ].b.hs_final_open_b      ||
//	   hs_status_flags_c[ hs_seatpos ].b.hs_final_partial_open_b /*||
//	   hs_status3_flags_c[ hs_seatpos ].b.final_STB_or_OPEN_b */)
//	{
//		hs_status2_flags_c[ hs_seatpos ].b.hs_output_fault_set_b = TRUE;
//
//	}
}

/* SDO@hsF_FrontHeatSeatStatus@ */
/*                                                        */
/* History                                                */
/* --------                                               */
/* created by YekolluH 01/29/2007 13:23:24                */
/* modified by YekolluH 02/19/2007 11:29:03               */
/*                                                        */
/* Labels                                                 */
/* ------                                                 */
/* Status : in process                                    */
/*                                                        */
/* SA Text                                                */
/* -------                                                */
/* no text                                                */
/*                                                        */
/* SD Text                                                */
/* -------                                                */
/* no text                                                */
/*                                                        */
/* Calls from                                             */
/* --------------                                         */
/* vsF_Clear :                                            */
/*    u_8Bit hsF_FrontHeatSeatStatus(unsigned char hs_nr) */
/* vsF_Manager :                                          */
/*    u_8Bit hsF_FrontHeatSeatStatus(unsigned char hs_nr) */
/*                                                        */
/* Calls to                                               */
/* --------                                               */
/* no calls                                               */
/* u_8Bit hsF_FrontHeatSeatStatus(unsigned char hs_nr) */
@far unsigned char hsF_FrontHeatSeatStatus(unsigned char hs_position)
{
	/* This function will give the latest status of the heated seats */

	unsigned char hs_pos;
	unsigned char hs_status;

	hs_pos = hs_position;

	switch (hs_pos) {

		case FRONT_LEFT:
		{
			if (hs_rem_seat_req_c[FRONT_LEFT] != SET_OFF_REQUEST) {

				hs_status = HS_ACTIVE;
			}
			else {
				hs_status = HS_NOT_ACTIVE;
			}
			break;
		}

		case FRONT_RIGHT:
		{
			if (hs_rem_seat_req_c[FRONT_RIGHT] != SET_OFF_REQUEST) {

				hs_status = HS_ACTIVE;
			}
			else {
				hs_status = HS_NOT_ACTIVE;
			}
			break;
		}

		default :
		{
			hs_status = HS_NOT_ACTIVE;
		}
	}

	return (hs_status);
}

/* SDO@hsF_VentOnHeatOff@ */
/*                                          */
/* History                                  */
/* --------                                 */
/* created by YekolluH 01/29/2007 13:25:11  */
/* modified by YekolluH 01/29/2007 13:25:59 */
/*                                          */
/* Labels                                   */
/* ------                                   */
/* Status : in process                      */
/*                                          */
/* SA Text                                  */
/* -------                                  */
/* no text                                  */
/*                                          */
/* SD Text                                  */
/* -------                                  */
/* no text                                  */
/*                                          */
/* Calls from                               */
/* --------------                           */
/* not called                               */
/*                                          */
/* Calls to                                 */
/* --------                                 */
/* no calls                                 */
/* void hsF_VentOnHeatOff(void) */
//@far unsigned char hsF_VentOnHeatOff(unsigned char hs_nr)
//{
//	/* This function will set the heaters to OFF, incase Vents are requested. */
//	switch (hs_nr) {
//
//		case FRONT_LEFT:
//		{
//			hs_rem_seat_req_c[FRONT_LEFT] = SET_OFF_REQUEST;
//			break;
//		}
//
//		case FRONT_RIGHT:
//		{
//			hs_rem_seat_req_c[FRONT_RIGHT] = SET_OFF_REQUEST;
//			break;
//		}
//	}
//}

/* SDO@hsF_AD_Readings@ */
/*                                          */
/* History                                  */
/* --------                                 */
/* created by YekolluH 01/30/2007 08:35:34  */
/* modified by YekolluH 08/30/2007 16:17:50 */
/*                                          */
/* Labels                                   */
/* ------                                   */
/* Status : in process                      */
/*                                          */
/* SA Text                                  */
/* -------                                  */
/* no text                                  */
/*                                          */
/* SD Text                                  */
/* -------                                  */
/* no text                                  */
/*                                          */
/* Calls from                               */
/* --------------                           */
/* no text                                  */
/*                                          */
/* Calls to                                 */
/* --------                                 */
/* no text                                  */
/* void hsF_AD_Readings(void) */
@far void hsF_AD_Readings(void)
{
	unsigned char hs_pos;

	for (hs_pos = FRONT_LEFT; hs_pos < ALL_HEAT_SEATS; hs_pos++) {
		
		/* Get the PWM state */
		hsLF_GetPWM_State(hs_pos);
		
		/* AD Readings for function call for Heater Vsense and Isense */
		/* Check if the HS is turned on by the SW (Status of switch request after turned on/off PWM) */
		if (hs_status2_flags_c[hs_pos].b.hs_heater_state_b ) {
			
			/* Check if PWM level is HIGH (Actual status of heater PWM) */
			if (hs_pwm_state_c[hs_pos] == PWM_HIGH) {
				
				/* Vsense and Isense */
				
				/* AD conversion for Vsense */
				hsLF_Vsense_AD_Readings(hs_pos);
				
				/* AD conversion for Isense */
				hsLF_Isense_AD_Readings(hs_pos);
				
				/* AD Conversion for Impedance */
				hsLF_Zsense_AD_Readings(hs_pos);
			}
			else {
				/* PWM is LOW Do not pursue AD conversion */
			}

		}
		else {
			/* Heater is OFF is off: AD conversion for Vsense */
			hsLF_Vsense_AD_Readings(hs_pos);
			
			/* Make sure Isense rdg is cleared */
			hs_Isense_c[hs_pos] = 0;
			hs_Zsense_c[hs_pos] = 0;
		}
	}
}

/* SDO@hsLF_Isense_AD_Readings@ */
/*                                                  */
/* History                                          */
/* --------                                         */
/* created by YekolluH 01/30/2007 09:10:25          */
/* modified by YekolluH 08/30/2007 16:17:50         */
/*                                                  */
/* Labels                                           */
/* ------                                           */
/* Status : in process                              */
/*                                                  */
/* SA Text                                          */
/* -------                                          */
/* no text                                          */
/*                                                  */
/* SD Text                                          */
/* -------                                          */
/* no text                                          */
/*                                                  */
/* Calls from                                       */
/* --------------                                   */
/* hsF_AD_Readings                                  */
/*                                                  */
/* Calls to                                         */
/* --------                                         */
/* u_8Bit ADF_Nrmlz_Isense(u_16Bit AD_Isense_value) */
/* void hsLF_Isense_AD_Readings(void) */
void hsLF_Isense_AD_Readings(unsigned char seat)
{
	/* SetUp for AD reading */
	switch (seat) {
		/* case FRONT_LEFT */
		case FRONT_LEFT:
		{
			hs_AD_parameters.slctd_adCH_c = CH3;        // set selected AD channel to CH3 - AN03/PAD03
			break;
		}
		
		/* Case FRONT_RIGHT */
		case FRONT_RIGHT:
		{
			hs_AD_parameters.slctd_adCH_c = CH4;        // set selected AD channel to CH4 - AN04/PAD04
			break;
		}
		
		/* Case REAR_LEFT */
		case REAR_LEFT:
		{
			hs_AD_parameters.slctd_adCH_c = CH1;        // set selected AD channel to CH1 - AN01/PAD01
			break;
		}
		
		/* Case REAR_RIGHT */
		case REAR_RIGHT:
		{
			hs_AD_parameters.slctd_adCH_c = CH2;        // set selected AD channel to CH2 - AN02/PAD02
			break;
		}
		
		default:
		{
			
		}
	}
	
	/* Isense AD conversion */
	hsLF_Isense_AD_Conversion();
	
	/* Store the filtered Isense */
	hs_Isense_c[seat] = (unsigned int) ADF_Nrmlz_Isense(AD_Filter_set[hs_AD_parameters.slctd_adCH_c].flt_rslt_w);
	
}

/* SDO@hsLF_Vsense_AD_Readings@ */
/*                                          */
/* History                                  */
/* --------                                 */
/* created by YekolluH 01/30/2007 09:10:25  */
/* modified by YekolluH 08/30/2007 16:17:50 */
/*                                          */
/* Labels                                   */
/* ------                                   */
/* Status : in process                      */
/*                                          */
/* SA Text                                  */
/* -------                                  */
/* no text                                  */
/*                                          */
/* SD Text                                  */
/* -------                                  */
/* no text                                  */
/*                                          */
/* Calls from                               */
/* --------------                           */
/* hsF_AD_Readings                          */
/*                                          */
/* Calls to                                 */
/* --------                                 */
/* no calls                                 */
/* void hsLF_Vsense_AD_Readings(void) */
void hsLF_Vsense_AD_Readings(unsigned char seatpos)
{
	
	/* SetUp for AD reading */
	switch (seatpos) {
		/* case FRONT_LEFT */
		case FRONT_LEFT:
		{
			hs_AD_parameters.slctd_adCH_c = CH6;        // set selected AD channel to Channel 6 
			hs_AD_parameters.slctd_mux_c  = MUX1;       // set selected mux to MUX1
			hs_AD_parameters.slctd_muxCH_c = CH3;       // Set selected MUX Channel to CH3
			break;
		}
		
		/* Case FRONT_RIGHT */
		case FRONT_RIGHT:
		{
			hs_AD_parameters.slctd_adCH_c = CH6;        // set selected AD channel to Channel 6 
			hs_AD_parameters.slctd_mux_c  = MUX1;       // set selected mux to MUX1
			hs_AD_parameters.slctd_muxCH_c = CH2;       // Set selected MUX Channel to CH2
			break;
		}
		
		/* Case REAR_LEFT */
		case REAR_LEFT:
		{
			hs_AD_parameters.slctd_adCH_c = CH5;        // set selected AD channel to Channel 5 
			hs_AD_parameters.slctd_mux_c  = MUX2;       // set selected mux to MUX2
			hs_AD_parameters.slctd_muxCH_c = CH3;       // Set selected MUX Channel to CH3
			break;
		}
		
		/* Case REAR_RIGHT */
		case REAR_RIGHT:
		{
			hs_AD_parameters.slctd_adCH_c = CH5;        // set selected AD channel to Channel 5 
			hs_AD_parameters.slctd_mux_c  = MUX2;       // set selected mux to MUX2
			hs_AD_parameters.slctd_muxCH_c = CH2;       // Set selected MUX Channel to CH2
			break;
		}
		
		default:
		{
			
		}
	}

	Dio_WriteChannelGroup(MuxSelGroup, hs_AD_parameters.slctd_muxCH_c);
	
	/* Vsense AD conversion */
	hsLF_Vsense_AD_Conversion(seatpos);
}

/* SDO@hsLF_Zsense_AD_Readings@ */
/*                                          */
/* History                                  */
/* --------                                 */
/* created by YekolluH 08/24/2007 10:50:29  */
/* modified by YekolluH 08/30/2007 16:17:50 */
/*                                          */
/* Labels                                   */
/* ------                                   */
/* Status : in process                      */
/*                                          */
/* SA Text                                  */
/* -------                                  */
/* no text                                  */
/*                                          */
/* SD Text                                  */
/* -------                                  */
/* no text                                  */
/*                                          */
/* Calls from                               */
/* --------------                           */
/* hsF_AD_Readings                          */
/*                                          */
/* Calls to                                 */
/* --------                                 */
/* no calls                                 */
/* void hsLF_Zsense_AD_Readings(void) */
void hsLF_Zsense_AD_Readings(unsigned char seat)
{

	//Get the Impedance values to detect Heaters Partial Open.
	unsigned char hs_mux = hs_AD_parameters.slctd_mux_c;
	unsigned char hs_mux_ch = hs_AD_parameters.slctd_muxCH_c;
	unsigned char hs_ad_ch = hs_AD_parameters.slctd_adCH_c;

	unsigned short hs_fltrd_Vsense_w = AD_mux_Filter[hs_mux][hs_mux_ch].flt_rslt_w;
	unsigned short hs_fltrd_Isense_w = AD_Filter_set[hs_ad_ch].flt_rslt_w;

	hs_Zsense_c[seat] = ADF_Hs_PrtlOpen_Z_Cnvrsn(hs_fltrd_Vsense_w, hs_fltrd_Isense_w);
}

/* SDO@hsLF_Vsense_AD_Conversion@ */
/*                                                                         */
/* History                                                                 */
/* --------                                                                */
/* created by YekolluH 01/30/2007 09:59:28                                 */
/* modified by YekolluH 08/30/2007 16:17:50                                */
/*                                                                         */
/* Labels                                                                  */
/* ------                                                                  */
/* Status : in process                                                     */
/*                                                                         */
/* SA Text                                                                 */
/* -------                                                                 */
/* no text                                                                 */
/*                                                                         */
/* SD Text                                                                 */
/* -------                                                                 */
/* no text                                                                 */
/*                                                                         */
/* Calls from                                                              */
/* --------------                                                          */
/* hsLF_Vsense_AD_Readings                                                 */
/*                                                                         */
/* Calls to                                                                */
/* --------                                                                */
/* u_8Bit ADF_Nrmlz_Vsense(u_16Bit AD_Vsense_value)                        */
/* ADF_AtoD_Cnvrsn(u_8Bit AD_Filter_supply)                                */
/* ADF_Str_UnFlt_Mux_Rslt(u_8Bit AD_Filter_feed,u_8Bit AD_Filter_mux_feed) */
/* ADF_Mux_Filter(u_8Bit AD_Filter_slctd_mux,u_8Bit AD_Filter_mux_ch)      */
/* void hsLF_Vsense_AD_Conversion(void) */
void hsLF_Vsense_AD_Conversion(unsigned char heater)
{
	/* AD conversion for muxed feedback */
	ADF_AtoD_Cnvrsn(hs_AD_parameters.slctd_adCH_c);
	
	/* Call the function to get the pwm state again */
	hsLF_GetPWM_State(heater);
	
	/* Again check to see if the HS is turned on by the SW, If not means there is no swithc request and Get the Vsense calculations */
	/* If seat request is presentm If PWM state is HIGH for that heater, get the Vsense readings.                                   */

	if (hs_status2_flags_c[heater].b.hs_heater_state_b ) {

		if (hs_pwm_state_c[heater] == PWM_HIGH) {
			
			/* Get unfiltered results */
			ADF_Str_UnFlt_Mux_Rslt(hs_AD_parameters.slctd_adCH_c, hs_AD_parameters.slctd_muxCH_c);
			
			/* Filter the result */
			ADF_Mux_Filter(hs_AD_parameters.slctd_mux_c, hs_AD_parameters.slctd_muxCH_c);
			
			/* Store the filtered Vsense */
			hs_Vsense_c[heater] = ADF_Nrmlz_Vsense(AD_mux_Filter[hs_AD_parameters.slctd_mux_c][hs_AD_parameters.slctd_muxCH_c].flt_rslt_w);
		}
		else {
			//If PWM state is not high do not do the calculations
		}
	}
	else {
		/* Get unfiltered results */
		ADF_Str_UnFlt_Mux_Rslt(hs_AD_parameters.slctd_adCH_c, hs_AD_parameters.slctd_muxCH_c);
		
		/* Filter the result */
		ADF_Mux_Filter(hs_AD_parameters.slctd_mux_c, hs_AD_parameters.slctd_muxCH_c);
		
		/* Store the filtered Vsense */
		hs_Vsense_c[heater] = ADF_Nrmlz_Vsense(AD_mux_Filter[hs_AD_parameters.slctd_mux_c][hs_AD_parameters.slctd_muxCH_c].flt_rslt_w);
	}
}

/* SDO@hsLF_Isense_AD_Conversion@ */
/*                                              */
/* History                                      */
/* --------                                     */
/* created by YekolluH 01/30/2007 10:00:16      */
/* modified by YekolluH 08/30/2007 16:17:50     */
/*                                              */
/* Labels                                       */
/* ------                                       */
/* Status : in process                          */
/*                                              */
/* SA Text                                      */
/* -------                                      */
/* no text                                      */
/*                                              */
/* SD Text                                      */
/* -------                                      */
/* no text                                      */
/*                                              */
/* Calls from                                   */
/* --------------                               */
/* hsLF_Isense_AD_Readings                      */
/*                                              */
/* Calls to                                     */
/* --------                                     */
/* ADF_AtoD_Cnvrsn(u_8Bit AD_Filter_supply)     */
/* ADF_Str_UnFlt_Rslt(u_8Bit AD_Filter_control) */
/* ADF_Filter(u_8Bit AD_Filter_ch)              */
/* void hsLF_Isense_AD_Conversion(void) */
void hsLF_Isense_AD_Conversion(void)
{
	ADF_AtoD_Cnvrsn(hs_AD_parameters.slctd_adCH_c);

	ADF_Str_UnFlt_Rslt(hs_AD_parameters.slctd_adCH_c);

	ADF_Filter(hs_AD_parameters.slctd_adCH_c);
}

/* SDO@hsLF_GetPWM_State@ */
/*                                          */
/* History                                  */
/* --------                                 */
/* created by YekolluH 02/13/2007 09:32:48  */
/* modified by YekolluH 02/19/2007 11:31:42 */
/*                                          */
/* Labels                                   */
/* ------                                   */
/* Status : in process                      */
/*                                          */
/* SA Text                                  */
/* -------                                  */
/* no text                                  */
/*                                          */
/* SD Text                                  */
/* -------                                  */
/* no text                                  */
/*                                          */
/* Calls from                               */
/* --------------                           */
/* hsF_AD_Readings                          */
/* hsLF_Vsense_AD_Conversion                */
/*                                          */
/* Calls to                                 */
/* --------                                 */
/* no calls                                 */
/* void hsLF_GetPWM_State(void) */
void hsLF_GetPWM_State(unsigned char heater)
{
	//This function retrieves the actual PWM state of each heater when requested.
	
	/* Get the PWM State */
	switch (heater) {

		case FRONT_LEFT:
		{
			hs_pwm_state_c[FRONT_LEFT] = (unsigned char) Pwm_GetOutputState(PWM_HS_FL);
			break;
		}

		case FRONT_RIGHT:
		{
			hs_pwm_state_c[FRONT_RIGHT] = (unsigned char) Pwm_GetOutputState(PWM_HS_FR);
			break;
		}

		case REAR_LEFT:
		{
			hs_pwm_state_c[REAR_LEFT] = (unsigned char) Pwm_GetOutputState(PWM_HS_RL);
			break;
		}

		case REAR_RIGHT:
		{
			hs_pwm_state_c[REAR_RIGHT] = (unsigned char) Pwm_GetOutputState(PWM_HS_RR);
			break;
		}
		default:
		{
			
		}
	}
}

/* SDO@hsF_CheckDiagnoseParameter@ */
/*                                          */
/* History                                  */
/* --------                                 */
/* created by YekolluH 02/28/2007 13:42:18  */
/* modified by YekolluH 11/12/2007 08:43:11 */
/*                                          */
/* Labels                                   */
/* ------                                   */
/* Status : in process                      */
/*                                          */
/* SA Text                                  */
/* -------                                  */
/* no text                                  */
/*                                          */
/* SD Text                                  */
/* -------                                  */
/* no text                                  */
/*                                          */
/* Calls from                               */
/* --------------                           */
/* ApplDescProcessWriteDataByIdentifier :   */
/*    hsF_CheckDiagnoseParameter()          */
/*                                          */
/* Calls to                                 */
/* --------                                 */
/* hsLF_CopyDiagnoseParameter               */
/* hsLF_ConvertDegToAD                      */
/********************************************/
/* void hsF_CheckDiagnoseParameter(void) */
/* This function will get called when ever there is a write request for Heater calibration parameters. */
/* And This function verifies the data received from external tool is in range or not.                 */
/* If data is in range returns OK and calls copy function else not OK								   */                                                          
@far unsigned char hsF_CheckDiagnoseParameter( unsigned char service, unsigned char *data_p_c )
{

	unsigned char temp_return_c,i;

	switch (service) {
		
	/* HS_PWM */
	case HS_PWM:
	case HS_PWM_REAR:
	{
		
		/* Allowed Range for changes to PWM are between 7% to 100% PWM.   */
		/* Data_p_c------> 7 to 100 (default is 10%PWM...........LL1 PWM) */
		/* Data_p_c+1 ---> 7 to 100 (default is 25%PWM...........HL1 PWM) */
		/* Data_p_c+2 ---> 7 to 100 (default is 50%PWM...........HL2 PWM) */
		/* Data_p_c+3 ---> 7 to 100 (default is100%PWM...........HL3 PWM) */

		if ((*data_p_c < 7  || *data_p_c > 100 ) ||
		    (*(data_p_c + 1) < 7  || *(data_p_c + 1) > 100) ||
		    (*(data_p_c + 2) < 7  || *(data_p_c + 2) > 100) ||
		    (*(data_p_c + 3) < 7  || *(data_p_c + 3) > 100) ) {
			
			/* Either One or more of the data received for heat level PWM is not in range. Do not allow the writes. Send Data Not OK */
			temp_return_c = 0;
		}
		else {
			/* Copy the data into buffer.and execute write */
			//hsLF_CopyDiagnoseParameter( data_p_c );

			/* New service REAR Seats added as we have new set of parameters for Rears now */
			/* New addition: MKS 61288: New calibration set */
			/* To update the actual Parameters based on Fronts or Rears */
			if (service == HS_PWM_REAR)
			{
				hs_vehicle_line_offset_c= VEH_LINE_REAROFFSET;	
			}
			else
			{
				hs_vehicle_line_offset_c= VEH_LINE_FRONTOFFSET;

			}
			/* Update the local Buffer at right areas with the new parameters */
			hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].Pwm_c[0] = *data_p_c;
			hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].Pwm_c[1] = *(data_p_c + 1);
			hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].Pwm_c[2] = *(data_p_c + 2);
			hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].Pwm_c[3] = *(data_p_c + 3);

			/* Data is in Range. Send Data OK. */
			temp_return_c = 1;
		}

		break;
	}
		
	/* HS_TIMEOUT */
	case HS_TIMEOUT:
	case HS_TIMEOUT_REAR:
	{
		/* Allowed range for timeouts from 1 to 60min.                     */
		/* Data_p_c------> 1 to 60 (default is 45min..........LL1 Timeout) */
		/* Data_p_c+1 ---> 1 to 60 (default is 30min..........HL1 Timeout) */
		/* Data_p_c+2 ---> 1 to 60 (default is 20min..........HL2 Timeout) */
		/* Data_p_c+3 ---> 1 to 60 (default is 10min..........HL3 Timeout) */

		if ((*data_p_c < 1  || *data_p_c > 60 ) ||
		    (*(data_p_c + 1) < 1  || *(data_p_c + 1) > 60) ||
		    (*(data_p_c + 2) < 1  || *(data_p_c + 2) > 60) ||
		    (*(data_p_c + 3) < 1  || *(data_p_c + 3) > 60) ) {

			/* Either One or more of the data received for heat level Timeouts are not in range. Do not allow the writes. Send Data Not OK */
			temp_return_c = 0;
		}
		else {
			/* Copy the data into buffer.and execute write */
			//hsLF_CopyDiagnoseParameter( data_p_c );

			/* New service REAR Seats added as we have new set of parameters for Rears now */
			/* New addition: MKS 61288: New calibration set */
			/* To update the actual Parameters based on Fronts or Rears */
			if (service == HS_TIMEOUT_REAR)
			{
				hs_vehicle_line_offset_c = VEH_LINE_REAROFFSET; 

			}
			else
			{
				hs_vehicle_line_offset_c = VEH_LINE_FRONTOFFSET;

			}
			
			/* Update the local Buffer at right areas with the new parameters */
			hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].Max_TimeOut_c[0] = *data_p_c;
			hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].Max_TimeOut_c[1] = *(data_p_c + 1);
			hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].Max_TimeOut_c[2] = *(data_p_c + 2);
			hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].Max_TimeOut_c[3] = *(data_p_c + 3);

			/* Data is in Range. Send Data OK. */
			temp_return_c = 1;
		}

		break;
	}

		/* ECU_WRITE_MEMORY $/$/Generic Write (Write Memory By address) */
		case ECU_WRITE_MEMORY:
		{
//			/* COnditions to be determined... Harsha 09/30/2008*/
//			if (/*(*data_p_c  == 0x00 ) && (*(data_p_c + 3) <= 0x0A)*/1) {
				/* Data is in Range.Allow Writes */
				hsLF_CopyDiagnoseParameter( data_p_c );

				/* Allow writes */
				temp_return_c = 1; 
//			}
//			else {
//				/* Data Not Ready..DO not give access to Write */
//				temp_return_c = 0;
//			}

			break;
		}

		/* CSWM_VARIANT  Not required. We are getting values from Hardware PINs*/
		case CSWM_VARIANT:
		{	
			/* Data_p_c------>                              */
			/* #define CSWM_2HS_VARIANT_1_K            0x03 */
			/* #define CSWM_4HS_VARIANT_2_K            0x0F */
			/* #define CSWM_2HS_2VS_HSW_VARIANT_3_K    0x73 */
			/* #define CSWM_2HS_HSW_VARIANT_4_K        0x43 */
			/* #define CSWM_2HS_2VS_VARIANT_5_K        0x33 */
			/* #define CSWM_4HS_HSW_VARIANT_6_K        0x4F */
			/* #define CSWM_4HS_2VS_HSW_VARIANT_7_K    0x7F */

			if ((*data_p_c == 0x03) || (*data_p_c == 0x0F) || (*data_p_c == 0x73) || (*data_p_c == 0x43) ||
			    (*data_p_c == 0x33) || (*data_p_c == 0x4F) || (*data_p_c == 0x7F) )
			{
				/* Copy the data into buffer.and execute write */
//				hsLF_CopyDiagnoseParameter( data_p_c );
			
				/* Data is in Range. Send Data OK. */
				temp_return_c = 1;
			}
			else {		
				/* Either One or more of the data received for heat level Timeouts are not in range. Do not allow the writes. Send Data Not OK */
				temp_return_c = 0;
			}

			break;
		}

		/* NTC_CUT_OFF_READINGS */
		case HS_NTC_CUT_OFF:
		case HS_NTC_CUTOFF_REAR:
		{
			/* Data_p_c------> 20 to 235(default is 139..........41c) */
			/* Data_p_c+1 ---> 20 to 235(default is 139..........49c) */
			/* Data_p_c+2 ---> 20 to 235(default is 102..........55c) */
			/* Data_p_c+3 ---> 20 to 235(default is 102..........55c) */
			/* Data_p_c+4 ---> 20 to 235(default is  79 .........70c) */

			if ((*data_p_c > 235  || *data_p_c < 20 ) ||
			    (*(data_p_c + 1) > 235  || *(data_p_c + 1) < 20) ||
			    (*(data_p_c + 2) > 235  || *(data_p_c + 2) < 20) ||
			    (*(data_p_c + 3) > 235  || *(data_p_c + 3) < 20) ||
			    (*(data_p_c + 4) > 235  || *(data_p_c + 4) < 20) ) {

				/* Either One or more of the data received for heat level PWM is not in range. Do not allow the writes. Send Data Not OK */
				temp_return_c = 0;
			}
			else {				
 
				/* Copy the data into buffer.and execute write */
				hsLF_ConvertDegToAD( service, data_p_c );



				/* Data is in Range. Send Data OK. */
				temp_return_c = 1;
			}

			break;
		}
		case HS_MIN_TIMEOUT:
		{
			/* Allowed range for timeouts from 1 to 60min.                     */
			/* Data_p_c------> 0 to 45 (default is  0min..........LL1 Timeout) */
			/* Data_p_c+1 ---> 0 to 30 (default is  0min..........HL1 Timeout) */
			/* Data_p_c+2 ---> 0 to 20 (default is  0min .........HL2 Timeout) */
			/* Data_p_c+3 ---> 0 to 10 (default is  0min..........HL3 Timeout) */

			if ((*data_p_c > 45 ) ||
			    (*(data_p_c + 1) > 30) ||
			    (*(data_p_c + 2) > 20) ||
			    (*(data_p_c + 3) > 10) ) {

				/* Either One or more of the data received for heat level Timeouts are not in range. Do not allow the writes. Send Data Not OK */
				temp_return_c = 0;
			}
			else {
				/* Copy the data into buffer.and execute write */
				//hsLF_CopyDiagnoseParameter( data_p_c );
				
				/* MKS 50039 */
//				hs_cal_prms.hs_prms[hs_vehicle_line_c].Min_TimeOut_c[0] = *data_p_c;
//				hs_cal_prms.hs_prms[hs_vehicle_line_c].Min_TimeOut_c[1] = *(data_p_c + 1); 
//				hs_cal_prms.hs_prms[hs_vehicle_line_c].Min_TimeOut_c[2] = *(data_p_c + 2);
//				hs_cal_prms.hs_prms[hs_vehicle_line_c].Min_TimeOut_c[3] = *(data_p_c + 3);
				
				/* Data is in Range. Send Data OK. */
				temp_return_c = 1;
			}

			break;
		}
		case REMOTE_AMB_TEMP_LIMIT:
		{
//			/* Allowed range for timeouts from 1 to 60min.                     */
//			/* Data_p_c------> 1 to 60 (default is 15min..........LL1 Timeout) */
//			/* Data_p_c+1 ---> 1 to 60 (default is 15min..........LL2 Timeout) */
//			/* Data_p_c+2 ---> 1 to 60 (default is 20min..........HL1 Timeout) */
//			/* Data_p_c+3 ---> 1 to 60 (default is 10min..........HL2 Timeout) */
//
//			if ((*data_p_c < 1  || *data_p_c > 60 ) ||
//			    (*(data_p_c + 1) < 1  || *(data_p_c + 1) > 60) ||
//			    (*(data_p_c + 2) < 1  || *(data_p_c + 2) > 60) ||
//			    (*(data_p_c + 3) < 1  || *(data_p_c + 3) > 60) ) {
//
//				/* Either One or more of the data received for heat level Timeouts are not in range. Do not allow the writes. Send Data Not OK */
//				temp_return_c = 0;
//			}
//			else {
//				/* Copy the data into buffer.and execute write */
//				hsLF_CopyDiagnoseParameter( data_p_c );
//				
//				/* Data is in Range. Send Data OK. */
//				temp_return_c = 1;
//			}
				/* Copy the data into buffer.and execute write */
				hsLF_CopyDiagnoseParameter( data_p_c );
				
				/* Data is in Range. Send Data OK. */
				temp_return_c = 1;

			break;
		}
		/* New case HS_IREF_VREF: EOL write to update Iref abd Vref values of Heated Seats */
		/* Added by CK ok 04.09.2008. */
		case HS_IREF_VREF:
		{
			/* Iref Default(A)   Allowed Range (A) */
			/*      2                1 to 4        */	// Decimal
			/*     0x02             0x01 to 0x04   */	// Hex
			/* Vref Default(mV)  Allowed Range (mV)*/
			/*     712              300 to 1423    */	// Decimal
			/*    0x2C8            0x12C to 0x58F  */	// Hex
			if( (*data_p_c < 0x01 || *data_p_c > 0x04) ||					// Iref Check
				(*(data_p_c+1) < 0x1 ) ||									// Vref Low range Check1 < 0x1xx
				( (*(data_p_c+1) == 0x01 && *(data_p_c+2) < 0x2C) ) ||		// Vref Low range Check2 < 0x12C
				( *(data_p_c+1) > 0x05 ) ||									// Vref high range Check1 > 0x5xx
				( (*(data_p_c+1) == 0x05 && *(data_p_c+2) > 0x8F) ) ){		// Vref high range Check2 > 0x58F
				
				/* Either One or more of the data received for heated seat Iref and Vref are not in range. Do not allow the writes. Send Data Not OK */
				temp_return_c = 0;				
			}else{
				
				/* Update data with valid Iref and Vref values */
				AD_Iref_seatH_c = *(data_p_c);
				AD_Vref_seatH_w = *(data_p_c+1);
				AD_Vref_seatH_w = (AD_Vref_seatH_w<<8);		// shift data_p_c+1 information to high byte
				AD_Vref_seatH_w |= *(data_p_c+2);			// or with low byte (data_p_c+2) 
				
				/* Data is in Range. Send Data OK. */
				temp_return_c = 1;				
			}			
			break;
		}
		
		/* default */
		default :
		{
			temp_return_c = 0;

			break;
		}
	}

	return( temp_return_c );
}

/* SDO@hsLF_CopyDiagnoseParameter@ */
/*                                          */
/* History                                  */
/* --------                                 */
/* created by YekolluH 02/28/2007 14:46:25  */
/* modified by YekolluH 03/01/2007 08:16:25 */
/*                                          */
/* Labels                                   */
/* ------                                   */
/* Status : in process                      */
/*                                          */
/* SA Text                                  */
/* -------                                  */
/* no text                                  */
/*                                          */
/* SD Text                                  */
/* -------                                  */
/* no text                                  */
/*                                          */
/* Calls from                               */
/* --------------                           */
/*hsF_CheckDiagnoseParameter                */
/*                                          */
/* Calls to                                 */
/* --------                                 */
/* no calls                                 */
/* void hsLF_CopyDiagnoseParameter(void) */
void hsLF_CopyDiagnoseParameter( unsigned char *data_p_c )
{

	unsigned char i;

	for ( i = 0; i < 5; i++ ) {
		hs_diag_prog_data_c[ i ] = *(data_p_c + i);
	}
}

/********************************************************************/
/* Calls from:														*/
/* -------------													*/
/* hsF_CheckDiagnoseParameter										*/
/*																	*/
/* Calls to															*/
/* -------------													*/
/* TempF_CanTemp_to_NtcAdStd										*/
/*																	*/
/* Description:														*/
/* -------------													*/		
/* Parameter passed to this function is in Deg C value              */
/* This function converts Deg C to Can Temperature and passed to the*/
/* TempF_CanTemp_to_NtcAdStd function                               */
/* CAN temperature = (deg C + 40) * 2								*/
/********************************************************************/
void hsLF_ConvertDegToAD(unsigned char serv, unsigned char *data_p_c)
{
	unsigned char i;
	
	/* New addition: MKS 61288: New calibration set */
	/* To Update the actual Parameters based on Fronts or Rears */
	if (serv == HS_NTC_CUTOFF_REAR)
	{
		hs_vehicle_line_offset_c= VEH_LINE_REAROFFSET;	
	}
	else
	{
		hs_vehicle_line_offset_c= VEH_LINE_FRONTOFFSET;
	}


	for ( i = 0; i < 5; i++ ) {

 		//hs_diag_prog_data_c[ i ] = TempF_CanTemp_to_NtcAdStd( (*(data_p_c + i)+40) * 2);

		/* MKS 54427 Harsha 07/21/2010: commented the below line and added the next line */
		/* The TempF_CanTemp_to_NtcAdStd we have is converting degF to AD readings */
		/* CDA Tool is already converting the degC to degF, so we are passing the degF input from CDA straight away */
		/* No need for us to do again the conversion degC to degF */
//		hs_cal_prms.hs_prms[hs_vehicle_line_c].NTC_CutOFF_c[i] = TempF_CanTemp_to_NtcAdStd( (*(data_p_c + i)+40) * 2); //MKS 50039
		hs_cal_prms.hs_prms[hs_seat_config_c+hs_vehicle_line_offset_c].NTC_CutOFF_c[i] = TempF_CanTemp_to_NtcAdStd( *(data_p_c + i) ); //MKS 50039 & MKS 54427 
	}
	
}


/* SDO@hsF_WriteData_to_ShadowEEPROM@ */
/*                                          */
/* History                                  */
/* --------                                 */
/* created by YekolluH 02/28/2007 15:09:37  */
/* modified by YekolluH 11/12/2007 08:43:11 */
/*                                          */
/* Labels                                   */
/* ------                                   */
/* Status : in process                      */
/*                                          */
/* SA Text                                  */
/* -------                                  */
/* no text                                  */
/*                                          */
/* SD Text                                  */
/* -------                                  */
/* no text                                  */
/*                                          */
/* Calls from                               */
/* --------------                           */
/* diagLF_WriteData :                       */
/*    hsF_WriteData_to_ShadowEEPROM()       */
/*                                          */
/* Calls to                                 */
/* --------                                 */
/* EE_BlockWrite()                          */
/* EE_DirectWrite()                         */

@far unsigned char hsF_WriteData_to_ShadowEEPROM(unsigned char service)
{
	//PC-Lint warning 527. To delete these warnings added the below and eliminated return statements.
	u_8Bit hs_result_c = FALSE;   // write result (success or fail)

	switch (service) {

		/* HS_PWM */
//	    case HS_PWM_REAR:
	    case HS_PWM:
		{
			
		//	EE_BlockWrite(EE_HEAT_PWM, &hs_diag_prog_data_c[0]);
			EE_BlockWrite(EE_HEAT_CAL_DATA_PF, (u_8Bit*)&hs_cal_prms);
			hs_result_c = TRUE;
			break;
		}

		/* HS_TIMEOUT */
//		case HS_TIMEOUT_REAR:
		case HS_TIMEOUT:
		{
			
		//	EE_BlockWrite(EE_HEAT_TIMEOUT, &hs_diag_prog_data_c[0]);
			EE_BlockWrite(EE_HEAT_CAL_DATA_PF, (u_8Bit*)&hs_cal_prms);
			hs_result_c = TRUE;
			break;
		}

		/* ECU_WRITE_MEMORY */
		case ECU_WRITE_MEMORY:
		{
			
			unsigned long dest;
			dest = (*(unsigned long*)(&hs_diag_prog_data_c[ 0 ]));
			if (EE_DirectWrite(dest,&hs_diag_prog_data_c[ 5 ],hs_diag_prog_data_c[ 4 ]) == 0) {

			//dest = dest + 1024; //(Where the backup address is)
			//EE_DirectWrite(dest,&hs_diag_prog_data_c[ 5 ],hs_diag_prog_data_c[ 4 ]);
			hs_result_c = TRUE;
			}
			else {

			hs_result_c = TRUE;
			}
			
			
			break;
		}
	
//      FOR MY11 Programs, This service is not necessary. 		
//		/* CSWM_VARIANT .. Temic Purpose Only .. Not accesible for others */
//		case CSWM_VARIANT:
//		{
//			if (/*FlashF_WriteEEPROMMirror(FlashM_MakeAddress(CSWM_variant),1,&hs_diag_prog_data_c[0]) == SHADOW_WRITE_COMPLETE*/1) {
//
//				return (1);
//			}
//			else {
//
//				return (0);
//			}
//			break;
//		}
		
		/* NTC_CUT_OFF_READINGS  */
//		case HS_NTC_CUT_OFF_REAR:
		case HS_NTC_CUT_OFF:
		{
		//	EE_BlockWrite(EE_HEAT_NTC_CUTOFF_VALUES, &hs_diag_prog_data_c[0]);
			EE_BlockWrite(EE_HEAT_CAL_DATA_PF, (u_8Bit*)&hs_cal_prms);
			hs_result_c = TRUE;
			break;
		}
		/* Minimum timeouts for Heaters  */
		case HS_MIN_TIMEOUT:
		{
		//	EE_BlockWrite(EE_HEAT_NTC_TIMEOUT, &hs_diag_prog_data_c[0]);
		//	EE_BlockWrite(EE_HEAT_CAL_DATA_PF, (u_8Bit*)&hs_cal_prms);
			hs_result_c = TRUE;
			break;
		}
		/* Remote start ambient temp limits  */
		case REMOTE_AMB_TEMP_LIMIT:
		{
			EE_BlockWrite(EE_REMOTE_CTRL_HEAT_AMBTEMP, &hs_diag_prog_data_c[1]);
			EE_BlockWrite(EE_REMOTE_CTRL_VENT_AMBTEMP, &hs_diag_prog_data_c[3]);
			
			hs_result_c = TRUE;
			break;
		}
		/* New case HS_IREF_VREF: EOL write to update Iref abd Vref values of Heated Seats */
		/* Added by CK ok 04.09.2008. Not used.*/
		case HS_IREF_VREF:
		{
//			/* Update the seated heat Iref and Vref parameters */
//			EE_BlockWrite(EE_IREF_SEAT_HEAT, (u_8Bit*)&AD_Iref_seatH_c);	// Write Iref_seatH parameter
//			EE_BlockWrite(EE_VREF_SEAT_HEAT, (u_8Bit*)&AD_Vref_seatH_w);	// Write Vref_seatH parameter
					
			hs_result_c = TRUE;		
			break;
		}		
		/* default */
		default :
		{
			
			hs_result_c = FALSE;
		}
	}
	return (hs_result_c);
}

/*********************************************************************************/
/*                        hsF_GetActv_OutputNumber                         	     */
/*********************************************************************************/
/* Function Name: @far unsigned char hsF_GetActv_OutputNumber(void)	 		 	 */
/* Type: Global Function													     */
/* Returns: unsigned char (number of active heat outputs)             			 */
/* Parameters: None																 */
/* Description: This function returns the number of active heat outputs. 		 */
/*              Heat output has to be at highest level in order to be considered */
/*              as active for battery compensation.                              */ 
/* Calls to: None																 */
/* Calls from:																	 */	
/*    BattVF_Compensation_Calc()												 */
/*																				 */  
/*********************************************************************************/
@far unsigned char hsF_GetActv_OutputNumber(void)
{
	unsigned char hs_actv_number_c = 0;	// Number of active heats
	unsigned char cnt_c = 0;			// loop counter
	
	for(cnt_c=0; cnt_c<ALL_HEAT_SEATS; cnt_c++)
	{
		/* Check each heat output state and level */
		if( (hs_status2_flags_c[cnt_c].b.hs_heater_state_b == TRUE) && (hs_status2_flags_c[cnt_c].b.hs_HL3_set_b == TRUE) )
		{
			/* This heated seat is active. Increment the heated seat active counter */
			hs_actv_number_c++;
		}else{
			/* This heated seat is not active. Do not increment the heated seat active counter */			
		}
	}
	return(hs_actv_number_c);
}

/****************************************************************************************/
/*                        hsF_RearSeat_TurnOFF                         	                */
/****************************************************************************************/
/* Function Name: @far void hsF_RearSeat_TurnOFF(void)	 		 	                    */
/* Type: Global Function													            */
/* This function works for Power Net series vehicle lines for service                   */
/* part variants. that is V7                                                            */
/* Will get executed only incase the latest learned ECU configuration is different from */
/* the stored/known configuration and reduced from Max variant to lower                 */
/* In such cases, actual logic never executes as the configuration gets reduced         */
/* For safety purposes, here we turn OFF the outputs for non supported functionality    */
/* for the current IGN cycle                                                            */
/*																				        */
/* Check to see, Rear Left and/or Rear Right requests are present                       */
/* If any of them are present, turn OFF the Rear outputs as the variant is not supported */
/****************************************************************************************/
@far void hsF_RearSeat_TurnOFF(void)
{
		if(hs_rem_seat_req_c[REAR_LEFT] != SET_OFF_REQUEST)
		{
			/* Turn OFF the non supported heater functionality */
			hsLF_UpdateHeatParameters(REAR_LEFT, HEAT_LEVEL_OFF);
			/* Set the remember seat req status to OFF */
			//hs_rem_seat_req_c[REAR_LEFT] = hs_switch_request_to_fet_c[REAR_LEFT];
			/* Clear the 2sec increment timer */
			//hs_monitor_2sec_led_counter_c[REAR_LEFT] = 0;
		}
		if(hs_rem_seat_req_c[REAR_RIGHT] != SET_OFF_REQUEST)
		{
			/* Turn OFF the non supported heater functionality */
			hsLF_UpdateHeatParameters(REAR_RIGHT, HEAT_LEVEL_OFF);
			/* Set the remember seat req status to OFF */
			//hs_rem_seat_req_c[REAR_RIGHT] = hs_switch_request_to_fet_c[REAR_RIGHT];
			/* Clear the 2sec increment timer */
			//hs_monitor_2sec_led_counter_c[REAR_RIGHT] = 0;
		}
			/* DISABLE Rear U12S for switch readings */
			Dio_WriteChannel(LED_U12S_EN, STD_LOW);
}

/*****************************************************************************/
/* pid_cal():                                                                */
/*                                                                           */
/* Description:  This function implements a PID controller with overflow     */
/*               and underflow protection.  NOTE:  All seats positions are   */
/*               updated.                                                    */
/*                                                                           */
/* Inputs:       None                                                        */
/*                                                                           */
/* Outputs:      None (NOTE: PWM outputs are updated)                        */
/*                                                                           */
/* CALLS FROM:   pid_heat_setting()                                          */
/*                                                                           */
/* CALLS TO:     hsLF_ControlFET()                                           */
/*****************************************************************************/

void pid_cal(void)
{
  /* local variables */
  u_8Bit actual;
  u_8Bit hs_position;
  s_16Bit error;
  s_32Bit output;
  s_32Bit p_term;
  s_32Bit i_term_scalar;
  s_32Bit d_term;

  static s_16Bit pre_error[ALL_HEAT_SEATS];
  static s_32Bit i_term[ALL_HEAT_SEATS];

  for (hs_position = 0; hs_position < ALL_HEAT_SEATS; hs_position++)
  {
    /* Is the setpoint non-zero?  If so... */
    if (hs_pid_setpoint[hs_position] != 0)
    {
      /* Calculate the actual reading. */
      actual = (u_8Bit)(PID_NTC_MAX) - NTC_AD_Readings_c[hs_position];
      
      /* Calculate the error. */
      error = (s_16Bit)(hs_pid_setpoint[hs_position] - actual);

      /* Calculate the proportional term. */
      p_term = (s_32Bit)(PID_KP)*(s_32Bit)(error);

      /* Calculate the integral term and saturate. */
      i_term_scalar = i_term[hs_position];
   
      i_term_scalar = i_term_scalar + (s_32Bit)(PID_KI*PID_DT)*(s_32Bit)(error);

      if (i_term_scalar > PID_MAX)
      {
        i_term_scalar = PID_MAX;
      }
      else if (i_term_scalar < PID_MIN)
      {
        i_term_scalar = PID_MIN;
      }

      i_term[hs_position] = i_term_scalar;

      /* Calculate the derivative term. */
      d_term = (s_32Bit)(PID_KD/PID_DT)*(s_32Bit)(error - pre_error[hs_position]);

      /* Calculate the output and saturate. */ 
      output = p_term + i_term[hs_position] + d_term;
  
      if (output > PID_MAX)
      {
        output = PID_MAX;
      }
      else if (output < PID_MIN)
      {
        output = PID_MIN;
      }

      /* Update the error history. */
      pre_error[hs_position] = error;
    }
    else
    {
      /* Reset the integrator and error history when 0% PWM is requested. */
      i_term[hs_position] = 0;
      pre_error[hs_position] = 0;
      output = 0;
    }

    /* Update the selected PWM channel */
    hsLF_ControlFET(hs_position, ((u_8Bit)(output >> PID_FRAC_BITS)));
  }
}

/*****************************************************************************/
/* pid_heat_setting():                                                       */
/*                                                                           */
/* Description:  This function controls the individual heat levels and each  */
/*               seat. It still uses the current time strategy.              */
/*                                                                           */
/* Inputs:       None                                                        */
/*                                                                           */
/* Outputs:      None                                                        */
/*                                                                           */
/* CALLS FROM:   hsf_HeatManager()                                           */
/*                                                                           */
/* CALLS TO:     pid_cal()                                                   */       
/*                                                                           */
/* NOTES:        EXTERN enum TOTAL_HEAT_SEATS (from canio.h)                 */
/*                                                                           */
/*****************************************************************************/

void pid_heat_setting(void)
{ 
  /* Local variables*/
  u_8Bit hs_position;
  static u_16Bit wait_time = 0;
    
  /* Has the wait time completed?  If so... */
  if (wait_time >= PID_WAIT_TIME)
  {
      hs_position = FRONT_RIGHT;
//    for(hs_position = 0; hs_position < ALL_HEAT_SEATS; hs_position++)
//    {
      if (hs_status2_flags_c[hs_position].b.hs_HL3_set_b == TRUE)              /* In HL3 now */
      {
        hs_pid_setpoint[hs_position] = PID_HEAT_HL3;
      }
      else if(hs_status2_flags_c[hs_position].b.hs_HL2_set_b == TRUE)          /* In HL2 now */
      {
        hs_pid_setpoint[hs_position] = PID_HEAT_HL2;
      }
      else if (hs_status2_flags_c[hs_position].b.hs_HL1_set_b == TRUE)         /* In HL1 now */
      {
        hs_pid_setpoint[hs_position] = PID_HEAT_HL1;
      }
      else if (hs_status2_flags_c[hs_position].b.hs_LL1_set_b == TRUE)         /* In LL1 now */
      {
        hs_pid_setpoint[hs_position] = PID_HEAT_LL1;
      } 
      else
      {
        hs_pid_setpoint[hs_position] = SET_OFF_REQUEST;
      }
//    }

    pid_cal();
  }
  else
  {
    /* Increment the wait timer */
    wait_time++;
  } 
}  
