/********************************************************************************/
/*                        A U T H O R   I D E N T I T Y							*/
/********************************************************************************/
/* Initials      Name                   Company									*/
/* --------      --------------------   --------------------------------------- */ 
/* CK            Can Kulduk             Continental Automotive Systems			*/
/********************************************************************************/


/********************************************************************************/
/*                   CHANGE HISTORY for BATTV MODULE                            */
/********************************************************************************/
/* mm/dd/yyyy	User - Change comments											*/
/* 04.11.2007   CK   - Heater file created                                      */
/* 09.22.2008	CK	 - Copied from CSWM MY09 project     						*/
/* 01.23.2009   CK   - No issues regaards to PC-Lint.                           */
/********************************************************************************/


/********************************************************************************/                                                                                                                                         
/* SD Text                                                                      */
/* -------                                                                      */
/* This module staggers four channel (heated seat outputs) signals.   			*/
/* the available Timer channels), one for each heated seat output. 				*/
/* Idea is to activate the next channel on the falling edge of the present      */
/* channel in the queue.                                              			*/
/* Important note: 100% Pwm'ed channels are not added to the stagger queue.     */
/* Advantages:                                                                  */
/* Minimizes the changes in the current consumption                             */
/* When all channels are Pwm'ed at 25%, current consumption (theoretically      */
/* speaking) stays constant.                                           			*/
/* Dynamic staggering (Stagger order is not fixed).                             */
/********************************************************************************/


/********************************** @Include(s) **********************************/
#include "TYPEDEF.h"
#include "Main.h"
#include "Mcu.h"
#include "Pwm.h"
#include "Pwm_Stagger.h"
#include "canio.h"
#include "Heating.h"
/********************************************************************************/


/********************************* external data *********************************/
/* N
 * O
 * N
 * E */
/*********************************************************************************/


/******************************** @Data Structures ********************************/
typedef struct{
    u_8Bit  channel;                    // stores the timer channel
    u_16Bit pwm;                        // stores the pwm duty cycles
}stagger_data;

stagger_data stagger_s[HS_SIZE];        // Stagger structure (one for each heated seat output)
/*********************************************************************************/


/*********************************** @ variables *********************************/
union char_bit stagger_flags1[HS_SIZE];
#define ch_occupied     b0              // current channel index is occupied
#define ch_activated    b1              // current channel is activated  

static u_8Bit waited = 0;  // Waits for one falling edge on current channel (before activating next channel)
/*********************************************************************************/


/*********************************************************************************/
/* SD Text                                                                       */
/* -------                                                                       */
/* Input parameter:                                                              */
/* Channel_ID: Timer channel identification number                               */
/*                                                                               */
/* Return parameter: 1 if channel already exists in the stagger queue.           */
/* 0 otherwise. 																 */
/*                                                                               */
/* This function checks whether channel already exists in the stagger queue or   */
/* not. 																		 */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* StaggerF_Delete_Ch :                                                          */
/*    u_8Bit StaggerF_Chck_Existing_Ch(Stagger_Channel_Type Channel_ID)          */
/* StaggerF_Manager :                                                            */
/*    u_8Bit StaggerF_Chck_Existing_Ch(Stagger_Channel_Type Channel_ID)          */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* no calls                                                                      */
/* 																				 */
/*********************************************************************************/
@far u_8Bit StaggerF_Chck_Existing_Ch(Stagger_Channel_Type Channel_ID)
{
	/* Local variables */
	u_8Bit chck_result = FALSE;        // existing channel check result
	u_8Bit i;                          // for loop iteration

	for (i=0; i<HS_SIZE; i++) {
		
		/* Check for existing channel */
		if (stagger_s[i].channel == Channel_ID) {
			
			/* Existing channel is found */
			chck_result = TRUE;			
			
			/* Set i to limit to exit the for loop */
			i = HS_SIZE;
		}
		else {			
			/* No. Go to next iteration */
		}
	}
	return(chck_result);
}

/*********************************************************************************/
/* SD Text                                                                       */
/* -------                                                                       */
/* Input parameter:                                                              */
/* Channel_ID: ID number of the channel that needs to be find.                   */
/*                                                                               */
/* Return parameter:                                                             */
/* location: Index of the selected channel in the stagger queue.                 */
/*                                                                               */
/* This function searches for the selected channel in the stagger queue and      */
/* returns the index of this channel. 											 */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* StaggerF_Enable_Next_Ch :                                                     */
/*    u_8Bit StaggerF_Find_Index(Stagger_Channel_Type Channel_ID)                */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* no calls                                                                      */
/* 																				 */
/*********************************************************************************/
@far u_8Bit StaggerF_Find_Index(Stagger_Channel_Type Channel_ID)
{
	u_8Bit location = INVALID_CH_INDEX;   // found index
	u_8Bit p;                             // for loop iteration

	for (p=0; p<HS_SIZE; p++) {
		
		/* Search for selected channel index */
		if (stagger_s[p].channel == Channel_ID) {
			
			/* Store found index */
			location = p;
			
			/* Channel found. Exit the for loop */
			p = HS_SIZE;
		}
		else {			
			/* No this is not the selected channel. Go to next iteration */
		}
	}
	return(location);
}

/*********************************************************************************/
/* SD Text                                                                       */
/* -------                                                                       */
/* This function turn off all the heated seat outputs by setting their pwm duty  */
/* cycle to 0%.   																 */
/* Condition: Heated seat channel exists in the stagger queue (it is not Pwm'ed  */
/* by 100%).      																 */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* StaggerF_Delete_Ch :                                                          */
/*    StaggerF_Turn_Off_All_Ch()                                                 */
/* StaggerF_Manager :                                                            */
/*    StaggerF_Turn_Off_All_Ch()                                                 */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* Pwm_DisableNotification(Pwm_ChannelType ChannelNumber)                        */
/* Pwm_SetDutyCycle(Pwm_ChannelType ChannelNumber,uint16 DutyCycle)              */
/* Pwm_EnableNotification(Pwm_ChannelType ChannelNumber,Pwm_EdgeNotificationType */
/* Notification) 																 */
/* 																				 */
/*********************************************************************************/
@far void StaggerF_Turn_Off_All_Ch(void)
{
	/* Check for 100% duty cycle */
	if (hs_allowed_heat_pwm_c[FRONT_LEFT] != N100) {
		
		/* Turn off FL */
		Pwm_DisableNotification(PWM_HS_FL);
		//Pwm_SetDutyCycle(PWM_HS_FL, 0); /* Changed 6 JUNE 2012*/
	}
	else {		
		/* Keep FL at 100% */
	}
	
	/* Check for 100% duty cycle */
	if (hs_allowed_heat_pwm_c[FRONT_RIGHT] != N100) {
		
		/* Turn off FR */
		Pwm_DisableNotification(PWM_HS_FR);
		//Pwm_SetDutyCycle(PWM_HS_FR, 0); /* Changed 6 JUNE 2012*/
	}
	else {		
		/* Keep FR at 100% */
	}
	
	/* Check for 100% duty cycle */
	if (hs_allowed_heat_pwm_c[REAR_LEFT] != N100) {
		
		/* Turn off RL */
		Pwm_DisableNotification(PWM_HS_RL);
		//Pwm_SetDutyCycle(PWM_HS_RL, 0); /* Changed 6 JUNE 2012*/ 
	} 
	else {		
		/* Keep RL at 100% */
	}
	
	/* Check for 100% duty cycle */
	if (hs_allowed_heat_pwm_c[REAR_RIGHT] != N100) {
		
		/* Turn off RR */
		Pwm_DisableNotification(PWM_HS_RR);
		//Pwm_SetDutyCycle(PWM_HS_RR, 0); /* Changed 6 JUNE 2012*/
	}
	else {		
		/* Keep RR at 100% */
	}
}

/*********************************************************************************/
/* SD Text                                                                       */
/* -------                                                                       */
/* Input parameters:                                                             */
/* Channel_ID: Timer channel that is being added to the stagger queue.           */
/* duty_cycle: Duty cycle of the selected timer channel (selected heated seat    */
/* output).                                                                  	 */
/*                                                                               */
/* This function adds selected channel information (Channel ID and duty cycle) to*/
/* the stagger queue structure in the first available position found. 			 */
/*                                                                               */
/* It also collapes the queue structure, so it can start building up with the    */
/* added channel information.                                                 	 */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* StaggerF_Manager :                                                            */
/*    StaggerF_Add_Channel(Stagger_Channel_Type Channel_ID,u_16Bit duty_cycle)   */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* no calls                                                                      */
/* 																				 */
/*********************************************************************************/
@far void StaggerF_Add_Channel(Stagger_Channel_Type Channel_ID, u_16Bit duty_cycle)
{
	u_8Bit b;                        // for loop iteration
	u_16Bit  pwm_cycle = duty_cycle; // new channel pwm duty cycle
	u_8Bit added = FALSE;            // channel is succesfully added indicator

	for (b=0; b<HS_SIZE; b++) {
		
		/* Check if current position is available */
		if ( (stagger_flags1[b].b.ch_occupied == FALSE) && (added == FALSE) ) {
			
			/* Enter channel data */
			stagger_s[b].channel = Channel_ID;
			stagger_s[b].pwm = pwm_cycle;
			
			/* Update channel flags */
			stagger_flags1[b].b.ch_occupied = TRUE;
			stagger_flags1[b].b.ch_activated = FALSE;
			
			/* Channel is added */
			added = TRUE;
		}
		else {			
			/* Clear active flag for occupied channel */
			stagger_flags1[b].b.ch_activated = FALSE;
		}
	}
}

/*********************************************************************************/
/* SD Text                                                                       */
/* -------                                                                       */
/* Input parameters:                                                             */
/* Channel_ID: Timer channel ID.                                                 */
/* new_pwm: New duty cycle of the selected channel.                              */
/*                                                                               */
/* This function updates the existing channel duty cycle in the stagger queue.   */
/* It also collapes the stagger queue structure, so it can start building up with*/
/* the new duty cycle information. 												 */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* StaggerF_Manager :                                                            */
/*    StaggerF_Update_Ch(Stagger_Channel_Type Channel_ID,u_16Bit new_pwm)        */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* no calls                                                                      */
/* 																				 */
/*********************************************************************************/
@far void StaggerF_Update_Ch(Stagger_Channel_Type Channel_ID, u_16Bit new_pwm)
{
	u_16Bit  updated_pwm = new_pwm;  // new pwm duty cycle
	u_8Bit updated = FALSE;          // channel is updated succesfully
	u_8Bit c;                        // for loop iteration

	for (c=0; c<HS_SIZE; c++) {
		
		/* Is this the correct channel index? */
		if ( (stagger_s[c].channel == Channel_ID) && (updated == FALSE) ) {
			
			/* Update pwm */
			stagger_s[c].pwm = updated_pwm;
			
			/* Update channel flags */
			stagger_flags1[c].b.ch_occupied = TRUE;
			stagger_flags1[c].b.ch_activated = FALSE;
			
			/* Channel is updated succesfully */
			updated = TRUE;
		}
		else {			
			/* Clear channel active flag */
			stagger_flags1[c].b.ch_activated = FALSE;
		}
	}
}

/*********************************************************************************/
/* SD Text                                                                       */
/* -------                                                                       */
/* Input parameter:                                                              */
/* Channel_ID: ID of the channel that needs to be deleted.                       */
/*                                                                               */
/* This function performs the following:                                         */
/* 1. Turn off all channels.                                                     */
/* 2. Deletes the selected channel from the stagger queue.                       */
/* 3. Shifts all channels (after the delete index) one place up.                 */
/* 4. Activates/Re-activates the first channel in the queue.                     */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* StaggerF_Manager :                                                            */
/*    StaggerF_Delete_Ch(Stagger_Channel_Type Channel_ID)                        */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* StaggerF_Turn_Off_All_Ch()                                                    */
/* u_8Bit StaggerF_Chck_Existing_Ch(Stagger_Channel_Type Channel_ID)             */
/* Pwm_EnableNotification(Pwm_ChannelType ChannelNumber,Pwm_EdgeNotificationType */
/* Notification) 																 */	
/* Pwm_SetDutyCycle(Pwm_ChannelType ChannelNumber,uint16 DutyCycle)              */
/* 																				 */
/*********************************************************************************/
@far void StaggerF_Delete_Ch(Stagger_Channel_Type Channel_ID)
{
	u_8Bit existing_channel = 0;         // existing channel result (true or false)
	u_8Bit k;                            // loop iteration
	
	/* Check if this is an existing channel */
	existing_channel = StaggerF_Chck_Existing_Ch(Channel_ID);
	
	/* Check if there are any channels active */
	if (existing_channel == TRUE) {
		
		/* Turn off all channels */
		StaggerF_Turn_Off_All_Ch();
		
		/* Shift up channel data (after delete channel) */
		for (k=0; k<(HS_SIZE-1); k++) {
			
			/* Find the index of the delete channel */
			if (stagger_s[k].channel == Channel_ID) {

				for (; k<(HS_SIZE-1); k++) {
					
					/* Copy channel data from next to current channel */
					stagger_s[k].channel = stagger_s[k+1].channel;
					stagger_s[k].pwm = stagger_s[k+1].pwm;
					
					/* Update channel flags */
					stagger_flags1[k].b.ch_occupied = stagger_flags1[k+1].b.ch_occupied;
					stagger_flags1[k].b.ch_activated = FALSE;
				}
			}
			else {				
				/* Clear channel active flag */
				stagger_flags1[k].b.ch_activated = FALSE;
			}
		}		
		/* Delete last channel entry */		
		/* Clear the data in last entry */
		stagger_s[HS_SIZE-1].channel = 0;
		stagger_s[HS_SIZE-1].pwm = 0;
		
		/* Clear last channel flags */
		stagger_flags1[HS_SIZE-1].b.ch_occupied = FALSE;
		stagger_flags1[HS_SIZE-1].b.ch_activated = FALSE;
		
		/* Check if there is at least one occupied channel */
		if (stagger_flags1[0].b.ch_occupied == TRUE) {
			
			/* Activate first channel */
			stagger_flags1[0].b.ch_activated = TRUE;
			Pwm_EnableNotification(stagger_s[0].channel, PWM_FALLING_EDGE);
			Pwm_SetDutyCycle(stagger_s[0].channel, stagger_s[0].pwm);
		}
		else {			
			/* There is no occupied channels */
		}
	}
	else {		
		/* Keep the channel off */
		Pwm_SetDutyCycle(Channel_ID, 0);
	}
}

/*********************************************************************************/
/* SD Text                                                                       */
/* -------                                                                       */
/* Input parameters:                                                             */
/* pwm_channel: Selected timer channel ID                                        */
/* ectTime_Type: Return value type (on or off time).                             */
/*                                                                               */
/* Return parameter:                                                             */
/* Time_w: Either On-time or Off-time of the selected channel.                   */
/*                                                                               */
/* This function calculates either the on or off time of the selected channel    */
/* using the pwm period (50ms) and present duty cycle information. 				 */
/*                                                                               */
/* This data is used to calculate the next channel's activation time (falling    */
/* edge of the present channel).                                   				 */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* StaggerF_Enable_Next_Ch :                                                     */
/*    u_16Bit StaggerF_Get_EctTime(u_8Bit pwm_channel,Stagger_EctTime_Type       */
/* ectTime_type)                                                      			 */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* no calls                                                                      */
/* 																				 */
/*********************************************************************************/
@far u_16Bit StaggerF_Get_EctTime(u_8Bit pwm_channel, Stagger_EctTime_Type ectTime_type)
{
	u_8Bit selected_channel = pwm_channel;  // selected channel number (heated seat output)
	u_16Bit Time_w = 0;                     // return value of the function (On or Off time)

	switch (selected_channel) {
	
		case FL_TIM_CH:
		{
			if (ectTime_type == ON_TIME) {				
				/* On Time = (250 * Duty Cycle) */
				Time_w = (N250 * hs_allowed_heat_pwm_c[FRONT_LEFT]);
			}
			else {				
				/* off Time: (Period - On time) */
				Time_w = (CH_PERIOD - (N250 * hs_allowed_heat_pwm_c[FRONT_LEFT]));
			}
			break;
		}

		case FR_TIM_CH:
		{
			if (ectTime_type == ON_TIME) {				
				/* On Time = (250 * Duty Cycle) */
				Time_w = (N250 * hs_allowed_heat_pwm_c[FRONT_RIGHT]);
			}
			else {
				/* off Time: (Period - On time) */
				Time_w = (CH_PERIOD - (N250 * hs_allowed_heat_pwm_c[FRONT_RIGHT]));
			}
			break;
		}

		case RL_TIM_CH:
		{
			if (ectTime_type == ON_TIME) {				
				/* On Time = (250 * Duty Cycle) */
				Time_w = (N250 * hs_allowed_heat_pwm_c[REAR_LEFT]);
			}
			else {				
				/* off Time: (Period - On time) */
				Time_w = (CH_PERIOD - (N250 * hs_allowed_heat_pwm_c[REAR_LEFT]));
			}
			break;
		}

		case RR_TIM_CH:
		{
			if (ectTime_type == ON_TIME) {				
				/* On Time = (250 * Duty Cycle) */
				Time_w = (N250 * hs_allowed_heat_pwm_c[REAR_RIGHT]);
			}
			else {				
				/* off Time: (Period - On time) */
				Time_w = (CH_PERIOD - (N250 * hs_allowed_heat_pwm_c[REAR_RIGHT]));
			}
			break;
		}
		
		default :
		{			
			/* We shall never come here */
		}
	}

	return(Time_w);
}

/*********************************************************************************/
/* SD Text                                                                       */
/* -------                                                                       */
/* Input parameters:                                                             */
/* present_index: channel index in the stagger queue structure                   */
/* time_difference: time offset calculated in the calling function               */
/* TCx_Operation: Timer channel operation type (add or subtract the time 	     */
/* difference).                                                                  */
/*                                                                               */
/* This function performs the actual staggering action by modifying the next (in */
/* the queue) channels TC registers count value.                                 */
/* The modification is performed by either adding or subtracting the time        */
/* difference to present channel TC register count. The result is stored as the  */
/* next TC register count value. 												 */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* StaggerF_Enable_Next_Ch :                                                     */
/*    StaggerF_Adjust_Tcx(u_8Bit present_index,u_16Bit time_difference,          */
/* Stagger_Operation_Type TCx_Operation)                                         */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* no calls                                                                      */
/* 																				 */
/*********************************************************************************/
@far void StaggerF_Adjust_Tcx(u_8Bit present_index, u_16Bit time_difference, Stagger_Operation_Type TCx_Operation)
{
	u_8Bit this_index = present_index;     // channel index in the stagger data structure array
	u_16Bit tm_offset = time_difference;     // time offset calculated in calling function 
	volatile u_16Bit *TC_This_Ch_p = (u_16Bit*)(&(TC2)); // Current TC Register Pointer
	volatile u_16Bit *TC_Next_Ch_p = (u_16Bit*)(&(TC2)); // Next TC Register Pointer
	
	/* Check valid index */
	if (this_index < (HS_SIZE-1)) {
		
		/* Locate current and next TC Registers */		
		TC_This_Ch_p += ((u_8Bit)stagger_s[this_index].channel - TC_REG_OFFSET);
		TC_Next_Ch_p += ((u_8Bit)stagger_s[this_index+1].channel - TC_REG_OFFSET);
				
		/* Check operation type */
		if (TCx_Operation == SUBTRACT) {
			
			/* Check offset with respect to current channel TC value */
			if ((*TC_This_Ch_p) >= tm_offset) {				
				/* Update next TC register (subtract) */
				(*TC_Next_Ch_p) = (u_16Bit) (((*TC_This_Ch_p) - tm_offset));
			}
			else {				
				/* Update next TC Register */
				(*TC_Next_Ch_p) = (u_16Bit)(MAX_COUNT - (tm_offset-(*TC_This_Ch_p)));
			}
		}
		else {			
			/* Update next TC Register (Add) */
			(*TC_Next_Ch_p) = (u_16Bit) ((*TC_This_Ch_p) + tm_offset);
		}
	}
	else {		
		/* Invalid index */
	}
}

/*********************************************************************************/
/* SD Text                                                                       */
/* -------                                                                       */
/* Input paramter:                                                               */
/* Channel_ID: Present channel ID number.                                        */
/*                                                                               */
/* This function is called in Pwm callback functions in order to activate the    */
/* next channel in the stagger queue structure. 								 */
/* First, this function checks whether next channel needs activation or not.     */
/* If it does, it calculates the time difference necassary to achieve the        */
/* staggering between present and next channel.     							 */
/* Then, it modifies the TC register counter of the next channel and enables the */
/* falling edge notification.                									 */
/*                                                                               */
/* Four possible cases when calculating time difference (offset):                */
/*                                                                               */
/* Case 1: Both channel pwm is < 50%                                             */
/* offset: Off1 - On2 (Off1: off time of present channel, On2: On time of next   */
/* channel)                                    									 */
/*                                                                               */
/* Case 2: Both channel pwm is >= 50%                                            */
/* offset: On1 - Off2 (Tc operation is ADD for case 2)                           */
/*                                                                               */
/* Case 3: Current channel off time < Next channel On time                       */
/* offset: On2 - Off1 (Tc Operation is ADD for case 3)                           */
/*                                                                               */
/* Case 4: All other possibilities                                               */
/* offset: Off1 - On2                                                            */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* Pwm_Notification_HFL :                                                        */
/*    StaggerF_Enable_Next_Ch(Stagger_Channel_Type Channel_ID)                   */
/* Pwm_Notification_HFR :                                                        */
/*    StaggerF_Enable_Next_Ch(Stagger_Channel_Type Channel_ID)                   */
/* Pwm_Notification_HRL :                                                        */
/*    StaggerF_Enable_Next_Ch(Stagger_Channel_Type Channel_ID)                   */
/* Pwm_Notification_HRR :                                                        */
/*    StaggerF_Enable_Next_Ch(Stagger_Channel_Type Channel_ID)                   */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* StaggerF_Adjust_Tcx(u_8Bit present_index,u_16Bit time_difference,             */
/* Stagger_Operation_Type TCx_Operation)                  						 */
/* u_16Bit StaggerF_Get_EctTime(u_8Bit pwm_channel, ectTime_type)                */
/* Pwm_EnableNotification(Pwm_ChannelType ChannelNumber, Notification)           */
/* Pwm_SetDutyCycle(Pwm_ChannelType ChannelNumber,uint16 DutyCycle)              */
/* u_8Bit StaggerF_Find_Index(Stagger_Channel_Type Channel_ID)                   */
/* Pwm_DisableNotification(Pwm_ChannelType ChannelNumber)                        */
/* 																				 */
/*********************************************************************************/
@far void StaggerF_Enable_Next_Ch(Stagger_Channel_Type Channel_ID)
{	
	/* Local variables */
	u_8Bit found_index = INVALID_CH_INDEX;        // channel index
	u_16Bit present_ch_OffTime  = 0;              // Present channel Ect off Time
	u_16Bit next_ch_OffTime     = 0;              // Next channel Ect off Time
	u_16Bit timeDifference      = 0;              // TCx offset
	Stagger_Operation_Type Operation = SUBTRACT;  // TCx Operation
	
	/* Find the channel index */
	found_index = StaggerF_Find_Index(Channel_ID);
	
	/* Check if we have waited for one falling edge on this channel */
	if (waited == TRUE) {
		
		/* Clear waited flag for next channel */
		waited = FALSE;
		
		/* Is next channel needs to be activated? */
		if ( (stagger_flags1[found_index+1].b.ch_occupied == TRUE) && (stagger_flags1[found_index+1].b.ch_activated == FALSE) && (found_index < (HS_SIZE-1)) ) {
			
			/* Get the present channel Ect Off time */
			present_ch_OffTime = StaggerF_Get_EctTime((u_8Bit)stagger_s[found_index].channel, OFF_TIME);
			
			/* Get next channel Ect On time */
			next_ch_OffTime = StaggerF_Get_EctTime((u_8Bit)stagger_s[found_index+1].channel, OFF_TIME);
			
			/* Check off time relationship. PWM pulse arithmetic */
			if ( (present_ch_OffTime >= CH_HALF_PERIOD) && (next_ch_OffTime >= CH_HALF_PERIOD) ) {
	
				/* Case 1:              */
				/* Both channels <= 50% */
				/* Offset: Off1 - On2   */
				timeDifference = ( present_ch_OffTime - (CH_PERIOD-next_ch_OffTime) );
			}
			else {				
				/* Check PWM pulses of current and next channels */
				if ( (present_ch_OffTime < CH_HALF_PERIOD) && (next_ch_OffTime < CH_HALF_PERIOD) ) {
			
					/* Case 2:             */
					/* Both channels > 50% */
					/* Offset: On1 - Off2  */
					timeDifference = ((CH_PERIOD-present_ch_OffTime)- next_ch_OffTime);
					
					/* Update operation to add */
					Operation = ADD;
				}
				else {
			
					/* Check offtime and ontime relationship */
					if (present_ch_OffTime < (CH_PERIOD-next_ch_OffTime) ) {
						
						/* Case 3:                    */
						/* Current Channel off time < */
						/* Next Channel on time       */
						/* Offset: On2 - Off1         */
						timeDifference = ( (CH_PERIOD-next_ch_OffTime) - present_ch_OffTime );
						
						/* Update operation to add */
						Operation = ADD;
					}
					else {						
						/* Case 4:                 */
						/* all other possibilities */
						/* Offset: Off1 - On2      */
						timeDifference = ( present_ch_OffTime - (CH_PERIOD-next_ch_OffTime) );
					}
				}
			}			
			/* Adjust Next channel TCx register for next output capture event (this yields to the desired staggering!!!) */
			StaggerF_Adjust_Tcx(found_index, timeDifference, Operation);
			
			/* Update the channel active flag */
			stagger_flags1[found_index+1].b.ch_activated = TRUE;
			Pwm_EnableNotification(stagger_s[found_index+1].channel, PWM_FALLING_EDGE);
			
			/* Activate the next channel */
			Pwm_SetDutyCycle(stagger_s[found_index+1].channel, stagger_s[found_index+1].pwm);
		}
		else {			
			/* Can we diactive the channel notification? */
			if (stagger_flags1[found_index].b.ch_activated == TRUE) {
				
				/* Disable notification */
				Pwm_DisableNotification(stagger_s[found_index].channel);
			}
		}
	}
	else {		
		/* Set waited to true */
		waited = TRUE;
	}
}

/*********************************************************************************/
/* SD Text                                                                       */
/* -------                                                                       */
/* Input parameters:                                                             */
/* Channel_ID: Heated seat output selection (timer channel ID)                   */
/* this_duty_cycle: present pwm duty cycle for the selected channel (this        */
/* information is very valuable) 												 */
/*                                                                               */
/* This function manages staggering operations such as add, update, or delete    */
/* channel.                  													 */
/* It is also responsible to activate the first channel in the stagger queue.    */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* hsLF_ControlFET :                                                             */
/*    StaggerF_Manager(Stagger_Channel_Type Channel_ID,u_16Bit this_duty_cycle)  */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* StaggerF_Delete_Ch(Stagger_Channel_Type Channel_ID)                           */
/* StaggerF_Update_Ch(Stagger_Channel_Type Channel_ID,u_16Bit new_pwm)           */
/* StaggerF_Add_Channel(Stagger_Channel_Type Channel_ID,u_16Bit duty_cycle)      */
/* StaggerF_Turn_Off_All_Ch()                                                    */
/* u_8Bit StaggerF_Chck_Existing_Ch(Stagger_Channel_Type Channel_ID)             */
/* Pwm_SetDutyCycle(Pwm_ChannelType ChannelNumber,uint16 DutyCycle)              */
/* 																				 */
/*********************************************************************************/
@far void StaggerF_Manager(Stagger_Channel_Type Channel_ID, u_16Bit this_duty_cycle)
{
	u_16Bit current_pwm = this_duty_cycle;   // current duty cycle
	u_8Bit  existing_ch = FALSE;             // existing channel check result
	
	/* Check pwm duty cycle */
	if ( (current_pwm != 0) && (current_pwm != MAIN_MAX_PWM) ) {
		
		/* Check if this is an existing channel */
		existing_ch = StaggerF_Chck_Existing_Ch(Channel_ID);
		
		/* Turn off all channels */
		StaggerF_Turn_Off_All_Ch();
		
		/* existing channel ? */
		if (existing_ch == FALSE) {
			
			/* Add new channel */
			StaggerF_Add_Channel(Channel_ID, current_pwm);
		}
		else {			
			/* Update existing channel */
			StaggerF_Update_Ch(Channel_ID, current_pwm);
		}
		
		/* Activate first channel */
		stagger_flags1[0].b.ch_activated = TRUE;
		Pwm_EnableNotification(stagger_s[0].channel, PWM_FALLING_EDGE);
		Pwm_SetDutyCycle(stagger_s[0].channel, stagger_s[0].pwm);
	}
	else {
		/* Are we turning off the output? */
		if (current_pwm == 0) {
			
			/* Delete channel */
			StaggerF_Delete_Ch(Channel_ID);
		}
		else {			
			/* 100 % duty cycle */
			Pwm_SetDutyCycle(Channel_ID, current_pwm);
			
			/* Check if this is an existing channel */
			existing_ch = StaggerF_Chck_Existing_Ch(Channel_ID);
			
			/* If no, nothing to do */
			if (existing_ch == FALSE) {				
				/* This channel is turned on at 100% as being activated. No need to delete this channel (It does not exits in the stagger queue!) */
			}
			else {				
				/* This channel existed in the stagger queue. Now It is being activated at 100% pwm. We need to delete this channel from the stagger queue. */
				StaggerF_Delete_Ch(Channel_ID);
			}
		}
	}
}

