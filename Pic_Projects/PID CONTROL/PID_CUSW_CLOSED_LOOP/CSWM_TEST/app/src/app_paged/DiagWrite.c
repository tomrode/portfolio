#define DIAGWRITE_C

/*****************************************************************************************************
*  (c) Copyright 2011 Continental Automotive Systems, all rights reserved
*
*  All material contained herein is CONTINENTAL CONFIDENTIAL and PROPRIETARY.
*  Any reproduction of this material without written consent from 
*  Continental AG is strictly forbidden.                                  
*
*  Filename:     DiagWrite.c
*
*  Created_by:   Harsha Yekollu
*
*  Date_created: 01/21/2011
*
*  Description:  Main file for UDS Diagnostics -- Write Data By LID Services. 
*
*  Revision_history:
*
*  MKS ID       Author         Date      Description of Change(s)
*  --------  -------------  --------  --------------------------------------
*  xxxxxxx   H. Yekollu     01/21/11  Initial creation for FIAT CUSW MY12 Program
*  74548     H. Yekollu     06/07/11  Update new DDT60-00-005 services.
*  81840	 H. Yekollu		08/05/11  - Diag Writes were not working. Issue was not pointing the diag buffer
* 									  properly. This Issue fixed.
* 									  - Write ECU Serial Number location changed, now the information
* 									  can be shared between the BOOT and APPL.
*  81838	 H. Yekollu		08/05/11  PROXI Implementation. Service 2E2023 and supported functions
* 									  added. 		
*  82322     H. Yekollu		09/18/11  Updates to new DDT 60-00-007 services	
*  75322	 H. Yekollu	    10/06/11   Cleanup the code.
*  86223	 H. Yekollu		10/12/11  DDT 60-00-009 Updates. 
*  			 H. Yekollu		10/14/11  didwrite_rfhub_check_crc_byte_by_byte function added in 2E2023 service
* 									  Will report the exact location incase of a CRC Failure in 102A service.	
* 
******************************************************************************************************/
/*****************************************************************************************************
* 									DIAGREAD
* This module handles the UDS Write Data By LID - Service 0x2E.
* 
*****************************************************************************************************/

/*****************************************************************************************************
*     								Include Files
******************************************************************************************************/
/* CAN_NWM Related Include files */
#include "il_inc.h"
#include "v_inc.h"
#include "desc.h"
#include "appdesc.h"

#include "TYPEDEF.h"
#include "main.h"
#include "DiagMain.h"
#include "DiagSec.h"
//#include "DiagRead.h"
#include "DiagWrite.h"
#include "DiagIOCtrl.h"
#include "canio.h"


#include "heating.h"
#include "Venting.h"
#include "StWheel_Heating.h"
//#include "Temperature.h"
#include "Internal_Faults.h"
#include "AD_Filter.h"
#include "BattV.h"
#include "mw_eem.h"
#include "mc9s12xs128.h"
#include "fbl_def.h"

/* Autosar include Files */
#include "Mcu.h"
//#include "Port.h"
//#include "Dio.h"
//#include "Gpt.h"
//#include "Pwm.h"
//#include "Adc.h"
//#include "Wdg.h"

/*****************************************************************************************************
*     								LOCAL ENUMS
******************************************************************************************************/

/*****************************************************************************************************
*     								LOCAL #DEFINES
******************************************************************************************************/
#define WRITE_DATA_FOR_HS_PWM                      0x00
#define WRITE_DATA_FOR_VS_PWM                      0x01
#define WRITE_DATA_FOR_HSW_PWM                     0x02
#define WRITE_DATA_FOR_HS_TIMEOUT                  0x03
#define WRITE_DATA_FOR_HSW_TIMEOUT                 0x04
#define WRITE_DATA_FOR_VOLTAGE_ON                  0x05
#define WRITE_DATA_FOR_VOLTAGE_OFF                 0x06
#define WRITE_HEAT_NTC_CUTOFF                      0x08  
#define WRITE_HSW_TEMP_CUTOFF                      0x09 
#define WRITE_AMB_TEMP_LIMITS_REMOTEST             0x0A 
#define WRITE_HEAT_MIN_TIMEOUT                     0x0B 
#define WRITE_HSW_MIN_TIMEOUT                      0x0C 
#define WRITE_HW_PART_NUMBER                       0x0D 
#define WRITE_HW_VERSION                           0x0E 
#define WRITE_SW_FINGERPRINT                       0x0F
#define WRITE_REMOTE_START_CFG                     0x10 
#define WRITE_ECU_SERIAL_NUMBER                    0x12
#define WRITE_CLR_INTERNAL_FLTS                    0x13
#define WRITE_CTRL_BYTE_FINAL                      0x14 
#define WRITE_TEMIC_BOM_NUMBER                     0x15 
#define WRITE_BATT_COMPENSATION                    0x16 
#define WRITE_EEP_VIRGIN_FLAG                      0x17
#define WRITE_HS_PWM_REAR	                       0x18
#define WRITE_HS_TIMEOUT_REAR                      0x19
#define WRITE_HS_NTC_CUTOFF_REAR                   0x1A

/* Definitions for CRC Byte-by-byte check */
#define CRC_LOW_BYTE_LOC		10u
#define CRC_HIGH_BYTE_LOC		6u
#define CRC_DECIMAL_FACTOR		10u
#define CRC_CHARACTER_OFFSET	0x30u

/* Low and HIGH bytes of RPM */
//#define LOWRPM(x)  (x & 0x00FF)            
//#define HIGHRPM(x) ((x >> 8) & 0x00FF)

/* Macros */
//#define LOWBYTE(x)  (x & 0x00FF)            
//#define HIGHBYTE(x) ((x >> 8) & 0x00FF)     

/*****************************************************************************************************
*     								LOCAL VARIABLES
******************************************************************************************************/
//static u_temp_l DataLen;
extern unsigned char diag_writedata_mode_c;
static enum DIAG_RESPONSE Response;
static u_temp_l DataLength;

E_PROXI_LRN_STATE re_PROXI_state;

u_8Bit rub_Test;
/*****************************************************************************************************
*     								LOCAL FUNCTION CALLS
******************************************************************************************************/
extern @far void diagF_DiagResponse(enum DIAG_RESPONSE Response, u_temp_l Length, DescMsgContext* pMsgContext);
u_16Bit didwrite_rfhub_convert_ascii_string_to_num(DescMsgItem * pub_CRCStr, u_8Bit lub_CRCStrLen);
u_16Bit didwrite_rfhub_reversed_crc16_mmc_upd(u_16Bit luw_crc_old, u_8Bit lub_data);
u_8Bit didwrite_rfhub_ReturnPROXISignal(DescMsgItem * data, u_8Bit lub_BytePos, u_8Bit lub_SignalMask, u_8Bit lub_Offset);
void didwrite_rfhub_SetPROXIError(u_8Bit lub_RecordPos, u_8Bit lub_ErrorBytePos, u_8Bit lub_ErrorMask, DescNegResCode lub_ErrorType);
//u_8Bit didwrite_rfhub_IsPROXIVehLineInCalFile(u_16Bit luw_PROXIVehLine);
u_16Bit multiply_u16_u16_u16 (u_16Bit ruw_op1, u_16Bit ruw_op2);
u_8Bit didwrite_rfhub_check_crc_byte_by_byte(u_16Bit, u_16Bit, u_8Bit);


void diagWriteF_CyclicTask(void)
{
	/* Will be entered inside the logic if and only if any write requests are present through external test tool */
	if (diag_writedata_b)
	{
		diagLF_WriteData(diag_writedata_mode_c/*, pMsgContext*/);
	}
	else
	{
		//There are no writes. Wait for write bit to set
	}
}	

void diagLF_WriteData(unsigned char service/*, DescMsgContext* pMsgContext*/)
{
//	unsigned char Response;
//	unsigned char DataLength;

	switch (service) {
		case WRITE_DATA_FOR_HS_PWM:
		{
			if ( hsF_WriteData_to_ShadowEEPROM( HS_PWM))
			{
				/* Write is complete. */
				diag_writedata_b = 0;
				diag_writedata_mode_c = 0;
			}
			else
			{
				/* Wait..Do nothing */
			}
			break;
		}
		
		case WRITE_DATA_FOR_HS_TIMEOUT:
		{
			if ( hsF_WriteData_to_ShadowEEPROM( HS_TIMEOUT))
			{
				/* Write is complete. */
				diag_writedata_b = 0;
				diag_writedata_mode_c = 0;
			}
			else
			{
				/* Wait..Do nothing */
			}
			break;
		}
		case WRITE_DATA_FOR_VS_PWM:
		{
			if ( vsF_Write_Diag_Data(VS_PWM) == TRUE)
			{
				diag_writedata_b = 0;
				diag_writedata_mode_c = 0;
			}
			else
			{
				/* Wait..Do nothing */
			}
			break;
		}
		case WRITE_DATA_FOR_HSW_PWM:
		{
			if ( stWF_Write_Diag_Data(HSW_PWM) == TRUE)
			{
				/* Write is complete. */
				diag_writedata_b = 0;
				diag_writedata_mode_c = 0;
			}
			else
			{
				/* Wait..Do nothing */
			}
			break;
		}
		case WRITE_DATA_FOR_HSW_TIMEOUT:
		{
			if ( stWF_Write_Diag_Data(HSW_TIMEOUT) == TRUE)
			{
				/* Write is complete. */
				diag_writedata_b = 0;
				diag_writedata_mode_c = 0;
			}
			else
			{
				/* Wait..Do nothing */
			}
			break;
		}
//		case ECU_WRITE_MEMORY:
//		{
//			if ( hsF_WriteData_to_ShadowEEPROM( ECU_WRITE_MEMORY))
//			{
//				/* Write is complete. */
//				diag_writedata_b = 0;
//				diag_writedata_mode_c = 0;
//			}
//			else
//			{
//				/* Wait..Do nothing */
//			}
//			break;
//		}
		case WRITE_DATA_FOR_VOLTAGE_ON:
		{
			if ( BattVF_Write_Diag_Data(VOLTAGE_ON) == TRUE)
			{
				/* Write is complete. */
				diag_writedata_b = 0;
				diag_writedata_mode_c = 0;
			}
			else
			{
				/* Wait..Do nothing */
			}
			break;
		}
		case WRITE_DATA_FOR_VOLTAGE_OFF:
		{
			if ( BattVF_Write_Diag_Data(VOLTAGE_OFF) == TRUE)
			{
				/* Write is complete. */
				diag_writedata_b = 0;
				diag_writedata_mode_c = 0;
			}
			else
			{
				/* Wait..Do nothing */
			}
			break;
		}
		case WRITE_HW_PART_NUMBER:
		{

            //Write the HW part Number to EEPROM 
			//EE_BlockWrite(EE_FBL_APPL_HW_PNO, &diag_temp_buffer_c[ 0 ]);
			diag_load_bl_eep_backup_b = TRUE; //Time to take boot backup

			/* Write is complete. */
			diag_writedata_b = 0;
			diag_writedata_mode_c = 0;
			break;
		}
//		case WRITE_ECU_PART_NUMBER:
//		{
//            //Write the ECU part Number to EEPROM 
//			//EE_BlockWrite(EE_FBL_APPL_ECU_PNO, &diag_temp_buffer_c[ 0 ]);
//			diag_load_bl_eep_backup_b = TRUE; //Time to take boot backup
//
//			/* Write is complete. */
//			diag_writedata_b = 0;
//			diag_writedata_mode_c = 0;
//			break;
//		}
		case WRITE_HW_VERSION:
		{
            //Write the HW Version to EEPROM 
			//EE_BlockWrite(EE_FBL_APPL_HW_VERSION, &diag_temp_buffer_c[ 0 ]);
			diag_load_bl_eep_backup_b = TRUE; //Time to take boot backup

			/* Write is complete. */
			diag_writedata_b = 0;
			diag_writedata_mode_c = 0;
			break;
		}
		/* WRITE_SW_FINGERPRINT - needs to be taken out from CDD */
		case WRITE_SW_FINGERPRINT:
		{
			/* Write is complete. */
			diag_writedata_b = 0;
			diag_writedata_mode_c = 0;
			break;
		}
		
		case WRITE_ECU_SERIAL_NUMBER:
		{
            //Write the HW Version to EEPROM 
			EE_BlockWrite(EE_SERIAL_NUMBER, &diag_temp_buffer_c[ 0 ]);

			/* Write is complete. */
			diag_writedata_b = 0;
			diag_writedata_mode_c = 0;
			diag_load_bl_eep_backup_b = 1; //Its time to take backup for Boot data	

			break;
		}

		/* WRITE_HEAT_NTC_CUTOFF  */
		case WRITE_HEAT_NTC_CUTOFF:
		{
			if ( hsF_WriteData_to_ShadowEEPROM( HS_NTC_CUT_OFF))
			{
				/* Write is complete. */
				diag_writedata_b = 0;
				diag_writedata_mode_c = 0;
			}
			else
			{
				/* Wait..Do nothing */
			}
			break;
		}
		case WRITE_HEAT_MIN_TIMEOUT:
		{
			if ( hsF_WriteData_to_ShadowEEPROM( HS_MIN_TIMEOUT))
			{
				/* Write is complete. */
				diag_writedata_b = 0;
				diag_writedata_mode_c = 0;
			}
			else
			{
				/* Wait..Do nothing */
			}
			break;
		}

		case WRITE_HSW_TEMP_CUTOFF:
		{
			if ( stWF_Write_Diag_Data(HSW_TEMP_CUT_OFF) == TRUE)
			{
				/* Write is complete. */
				diag_writedata_b = 0;
				diag_writedata_mode_c = 0;
			}
			else
			{
				/* Wait..Do nothing */
			}
			break;
		}
		case WRITE_HSW_MIN_TIMEOUT:
		{
			if ( stWF_Write_Diag_Data(HSW_MIN_TIMEOUT) == TRUE)
			{
				/* Write is complete. */
				diag_writedata_b = 0;
				diag_writedata_mode_c = 0;
			}
			else
			{
				/* Wait..Do nothing */
			}
			break;
		}
		/* WRITE_Remote start Ambient temp limits  */
		case WRITE_AMB_TEMP_LIMITS_REMOTEST:
		{
			if ( hsF_WriteData_to_ShadowEEPROM( REMOTE_AMB_TEMP_LIMIT))
			{
				/* Write is complete. */
				diag_writedata_b = 0;
				diag_writedata_mode_c = 0;
			}
			else
			{
				/* Wait..Do nothing */
			}
			break;
		}
		
		/* WRITE_CLR_INTERNAL_FLTS  ..Temic Only...To Clear Internal Faults */
		case WRITE_CLR_INTERNAL_FLTS:
		{
            // New: Clear internal faults
			EE_BlockWrite(EE_INTERNAL_FLT_BYTE0, &intFlt_bytes[ZERO]);
			EE_BlockWrite(EE_INTERNAL_FLT_BYTE1, &intFlt_bytes[ONE]);
			EE_BlockWrite(EE_INTERNAL_FLT_BYTE2, &intFlt_bytes[TWO]);
			EE_BlockWrite(EE_INTERNAL_FLT_BYTE3, &intFlt_bytes[THREE]);
			
			/* Write is complete. */
			diag_writedata_b = 0;
			diag_writedata_mode_c = 0;			
			break;
		}

		case WRITE_CTRL_BYTE_FINAL:
		{
            //Write the ECU part Number to EEPROM 
			EE_BlockWrite(EE_CTRL_BYTE_FINAL, &diag_temp_buffer_c[ 0 ]);
			/* Write is complete. */
			diag_writedata_b = 0;
			diag_writedata_mode_c = 0;
			break;
		}

		case WRITE_TEMIC_BOM_NUMBER:
		{
            //Write the HW Version to EEPROM 
			EE_BlockWrite(EE_BOARD_TEMIC_BOM_NUMBER, &diag_temp_buffer_c[ 0 ]);
			/* Write is complete. */
			diag_writedata_b = 0;
			diag_writedata_mode_c = 0;
			break;
		}
		case WRITE_BATT_COMPENSATION:
		{
			if ( BattVF_Write_Diag_Data(BATTERY_COMPENSATION) == TRUE)
			{
				diag_writedata_b = 0;
				diag_writedata_mode_c = 0;
			}
			else
			{
				/* Wait..Do nothing */
			}
			break;
		}	
//		/* New case WRITE_HS_IREF_VREF: Update heated seat Iref and Vref parameters */
//		case WRITE_HS_IREF_VREF:
//		{
//			/* Check if hsF_WriteData_to_ShadowEEPROM functions returned TRUE */
//			/* Indicating that a call to EE_BlockWrite is placed */	
//			if(hsF_WriteData_to_ShadowEEPROM(HS_IREF_VREF) == TRUE){
//				
//				/* Clear write variables */
//				diag_writedata_b = 0;
//				diag_writedata_mode_c = 0;
//				
//			}else{
//				/* Wait..Do nothing*/
//			}			
//			break;
//		}		
//		/* New case WRITE_STW_IREF_VREF: Update steering wheel Iref and Vref parameters */
//		case WRITE_STW_IREF_VREF:
//		{
//			/* Check if stWF_Write_Diag_Data functions returned TRUE */
//			/* Indicating that a call to EE_BlockWrite is placed */			
//			if( stWF_Write_Diag_Data(STW_IREF_VREF) == TRUE){
//				
//				/* Clear write variables*/
//				diag_writedata_b = 0;
//				diag_writedata_mode_c = 0;
//				
//			}else{
//				/* Wait..Do nothing*/
//			}			
//			break;
//		}
		
		/* MKS 49539 */
		case WRITE_REMOTE_START_CFG:
		{
			
			//tRemSt_DCS LocalRemSt_DCS;

            //Write the Remote start config to EEPROM 
			EE_BlockWrite(EE_REMOTE_CTRL_STAT, &diag_temp_buffer_c[ 0 ]);
			
			if(diag_temp_buffer_c[ 0 ] == 0x01)
			{
				//SetBitTxRemSt_DCS(LocalRemSt_DCS);
				canio_RemSt_configStat_c = TRUE;
				
				canio_RemSt_stored_stat = REMST_ENABLED;
	            //Write the Remote start config to EEPROM 
				EE_BlockWrite(EE_REMOTE_CTRL_STAT, &canio_RemSt_stored_stat);
			}
			else
			{
				//ClearBitTxRemSt_DCS(LocalRemSt_DCS);
				canio_RemSt_configStat_c = FALSE;
				
				canio_RemSt_stored_stat = REMST_DISABLED;
	            //Write the Remote start config to EEPROM 
				EE_BlockWrite(EE_REMOTE_CTRL_STAT, &canio_RemSt_stored_stat);

			}
			/* Send the new state on Bus */
			//dbkPutTxRemSt_DCS(LocalRemSt_DCS);
		

			/* Write is complete. */
			diag_writedata_b = 0;
			diag_writedata_mode_c = 0;
			
			break;
		}

		
		case WRITE_EEP_VIRGIN_FLAG:
		{
			
			UInt16 mark_virginflag_fail_w;
			/* Write a value to VIRGIN Flag with other value than the supposed value */
			mark_virginflag_fail_w = 0xA55A;
			EE_BlockWrite(EE_VRGN_FLAG_START_BLOCK, (UInt8*)&mark_virginflag_fail_w);        
			/* Write is complete. */
			diag_writedata_b = 0;
			diag_writedata_mode_c = 0;
			break;
		}
		
		case WRITE_HS_PWM_REAR:
		{
			if ( hsF_WriteData_to_ShadowEEPROM( HS_PWM_REAR))
			{
				/* Write is complete. */
				diag_writedata_b = 0;
				diag_writedata_mode_c = 0;
			}
			else
			{
				/* Wait..Do nothing */
			}
			break;
		}
		
		case WRITE_HS_TIMEOUT_REAR:
		{
			if ( hsF_WriteData_to_ShadowEEPROM( HS_TIMEOUT_REAR))
			{
				/* Write is complete. */
				diag_writedata_b = 0;
				diag_writedata_mode_c = 0;
			}
			else
			{
				/* Wait..Do nothing */
			}
			break;
		}
		
		case WRITE_HS_NTC_CUTOFF_REAR:
		{
			if ( hsF_WriteData_to_ShadowEEPROM( HS_NTC_CUTOFF_REAR))
			{
				/* Write is complete. */
				diag_writedata_b = 0;
				diag_writedata_mode_c = 0;
			}
			else
			{
				/* Wait..Do nothing */
			}
			break;
		}

		
		default :
		{
			/* Write is complete. */
			diag_writedata_b = 0;
			diag_writedata_mode_c = 0;
		}

	}
}



/*  ********************************************************************************
 * Function name:ApplDescWrite_222800_Heated_Seat_PWM_Calibration_Parameters_Front_Seats (Service request header:$2E $28 $0 )
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescWrite_222800_Heated_Seat_PWM_Calibration_Parameters_Front_Seats(DescMsgContext* pMsgContext)
{
	/* Initialize the DataLength. If to be adjusted logic adjusts */
	DataLength = 1;

	/* Byte 0,1,2 and 3: LL1,HL1,HL2 and HL3 PWM */
	if (diag_sec_access5_state_c == ECU_UNLOCKED)
	{
		if (hsF_CheckDiagnoseParameter( HS_PWM,&pMsgContext->reqData[0] ))
		{
			/* Data is OK Set the flag to write contents into EEPROM */
			diag_writedata_b = 1;							
			DataLength = 0;
			Response = RESPONSE_OK;
			diag_writedata_mode_c = WRITE_DATA_FOR_HS_PWM;
		}
		else
		{
			/* Do not write any thing to EEPROM */
			diag_writedata_b = 0;
			diag_writedata_mode_c = 0;
			//DataLength = 1;
			Response = REQUEST_OUT_OF_RANGE;
		}
	}
	else
	{
		/* Do not write any thing to EEPROM */
		diag_writedata_b = 0;
		diag_writedata_mode_c = 0;
		//DataLength = 1;
		Response = SECURITY_ACCESS_DENIED;
	}
	diagF_DiagResponse(Response, DataLength+SIZE_OF_BLOCKNUMBER_K, pMsgContext);
}

/*  ********************************************************************************
 * Function name:ApplDescWrite_COM_SER_66281 (Service request header:$2E $28 $1 )
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescWrite_222801_Vented_Seat_PWM_Calibration_Parameters(DescMsgContext* pMsgContext)
{
	/* Initialize the DataLength. If to be adjusted logic adjusts */
	DataLength = 1;
     
	/* Byte 0: Vent Level 1 PWM */
	/* Byte 1: Vent Level 2 PWM */
	if ((CSWM_config_c & CSWM_2VS_MASK_K) == CSWM_2VS_MASK_K) {
		if (diag_sec_access5_state_c == ECU_UNLOCKED)
		{
			if ( vsF_Chck_Diag_Prms(VS_PWM, (&pMsgContext->reqData[0])) == TRUE)
			{
				/* Data is OK Set the flag to write contents into EEPROM */
				diag_writedata_b = 1;
				DataLength = 0;
				Response = RESPONSE_OK;
				diag_writedata_mode_c = WRITE_DATA_FOR_VS_PWM;
			}
			else
			{
				/* Do not write any thing to EEPROM */
				diag_writedata_b = 0;
				diag_writedata_mode_c = 0;

				//DataLength = 1;
				Response = REQUEST_OUT_OF_RANGE;
			}
		}
		else
		{
			/* Do not write any thing to EEPROM */
			diag_writedata_b = 0;
			diag_writedata_mode_c = 0;

			//DataLength = 1;
			Response = SECURITY_ACCESS_DENIED;
		}
	}
	else
	{
		/* Do not write any thing to EEPROM */
		diag_writedata_b = 0;
		diag_writedata_mode_c = 0;

		//DataLength = 1;
		Response = SUBFUNCTION_NOT_SUPPORTED;
	}
    diagF_DiagResponse(Response, DataLength+SIZE_OF_BLOCKNUMBER_K, pMsgContext);
}

/*  ********************************************************************************
 * Function name:ApplDescWrite_222802_Heated_Steering_Wheel_PWM_Calibration_Parameters (Service request header:$2E $28 $2 )
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescWrite_222802_Heated_Steering_Wheel_PWM_Calibration_Parameters(DescMsgContext* pMsgContext)
{
	/* Initialize the DataLength. If to be adjusted logic adjusts */
	DataLength = 1;

	/* Byte 0,1,2 and 3: Wheel Level 1,2,3 and 4 PWM */
	if ((CSWM_config_c & CSWM_HSW_MASK_K) == CSWM_HSW_MASK_K)
	{
		if (diag_sec_access5_state_c == ECU_UNLOCKED)
		{
			if (stWF_Chck_Diag_Prms(HSW_PWM, (&pMsgContext->reqData[0])) == TRUE)
			{
				/* Data is OK Set the flag to write contents into EEPROM */
				diag_writedata_b = 1;
				DataLength = 0;
				Response = RESPONSE_OK;
				diag_writedata_mode_c = WRITE_DATA_FOR_HSW_PWM;
			}
			else
			{
				/* Do not write any thing to EEPROM */
				diag_writedata_b = 0;
				diag_writedata_mode_c = 0;

				//DataLength = 1;
				Response = REQUEST_OUT_OF_RANGE;
			}
		}
		else
		{
			/* Do not write any thing to EEPROM */
			diag_writedata_b = 0;
			diag_writedata_mode_c = 0;

			//DataLength = 1;
			Response = SECURITY_ACCESS_DENIED;
		}
	}
	else
	{
		/* Do not write any thing to EEPROM */
		diag_writedata_b = 0;
		diag_writedata_mode_c = 0;

		//DataLength = 1;
		Response = SUBFUNCTION_NOT_SUPPORTED;
	}
	diagF_DiagResponse(Response, DataLength+SIZE_OF_BLOCKNUMBER_K, pMsgContext);
}

/*  ********************************************************************************
 * Function name:ApplDescWrite_222803_Heated_Seat_Max_Timeout_Parameters_Front_Seats (Service request header:$2E $28 $3 )
 
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescWrite_222803_Heated_Seat_Max_Timeout_Parameters_Front_Seats(DescMsgContext* pMsgContext)
{
	/* Initialize the DataLength. If to be adjusted logic adjusts */
	DataLength = 1;

	/* Byte 0,1,2 and 3: LL1,HL1, HL2 and HL3 Timeout */
	if (diag_sec_access5_state_c == ECU_UNLOCKED)
	{
		if ( hsF_CheckDiagnoseParameter( HS_TIMEOUT,&pMsgContext->reqData[0] ))
		{
			/* Data is OK Set the flag to write contents into EEPROM */
			diag_writedata_b = 1;
			DataLength = 0;
			Response = RESPONSE_OK;
			diag_writedata_mode_c = WRITE_DATA_FOR_HS_TIMEOUT;
		}
		else
		{
			/* Do not write any thing to EEPROM */
			diag_writedata_b = 0;
			diag_writedata_mode_c = 0;
			//DataLength = 1;
			Response = REQUEST_OUT_OF_RANGE;
		}
	}
	else
	{
		/* Do not write any thing to EEPROM */
		diag_writedata_b = 0;
		diag_writedata_mode_c = 0;

		//DataLength = 1;
		Response = SECURITY_ACCESS_DENIED;
	}
	diagF_DiagResponse(Response, DataLength+SIZE_OF_BLOCKNUMBER_K, pMsgContext);
}

/*  ********************************************************************************
 * Function name:ApplDescWrite_222804_Heated_Steering_Wheel_Max_Timeout_Parameters (Service request header:$2E $28 $4 )
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescWrite_222804_Heated_Steering_Wheel_Max_Timeout_Parameters(DescMsgContext* pMsgContext)
{
	/* Initialize the DataLength. If to be adjusted logic adjusts */
	DataLength = 1;

	/* Byte 0,1,2 and 3: Wheel Level 1,2,3 and 4 Timeout */
	if ((CSWM_config_c & CSWM_HSW_MASK_K) == CSWM_HSW_MASK_K)
	{
		if (diag_sec_access5_state_c == ECU_UNLOCKED)
		{
			if ( stWF_Chck_Diag_Prms(HSW_TIMEOUT, (&pMsgContext->reqData[0])) == TRUE)
			{
				/* Data is OK Set the flag to write contents into EEPROM */
				diag_writedata_b = 1;
				DataLength = 0;
				Response = RESPONSE_OK;
				diag_writedata_mode_c = WRITE_DATA_FOR_HSW_TIMEOUT;
			}
			else
			{
				/* Do not write any thing to EEPROM */
				diag_writedata_b = 0;
				diag_writedata_mode_c = 0;
				//DataLength = 1;
				Response = REQUEST_OUT_OF_RANGE;
			}
		}
		else
		{
			/* Do not write any thing to EEPROM */
			diag_writedata_b = 0;
			diag_writedata_mode_c = 0;

			//DataLength = 1;
			Response = SECURITY_ACCESS_DENIED;
		}
	}
	else {
		/* Do not write any thing to EEPROM */
		diag_writedata_b = 0;
		diag_writedata_mode_c = 0;

		//DataLength = 1;
		Response = SUBFUNCTION_NOT_SUPPORTED;
	}
	diagF_DiagResponse(Response, DataLength+SIZE_OF_BLOCKNUMBER_K, pMsgContext);
}

/*  ********************************************************************************
 * Function name:ApplDescWrite_222805_Voltage_Level_ON (Service request header:$2E $28 $5 )
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescWrite_222805_Voltage_Level_ON(DescMsgContext* pMsgContext)
{
	/* Initialize the DataLength. If to be adjusted logic adjusts */
	DataLength = 1;

	/* Byte 0: Voltage ON Threshold */
	if (diag_sec_access5_state_c == ECU_UNLOCKED)
	{
		if ( BattVF_Chck_Diag_Prms(VOLTAGE_ON, (&pMsgContext->reqData[0])) == TRUE)
		{
			/* Data is OK Set the flag to write contents into EEPROM */
			diag_writedata_b = 1;
			DataLength = 0;
			Response = RESPONSE_OK;
			diag_writedata_mode_c = WRITE_DATA_FOR_VOLTAGE_ON;
		}
		else
		{
			/* Do not write any thing to EEPROM */
			diag_writedata_b = 0;
			diag_writedata_mode_c = 0;

			//DataLength = 1;
			Response = REQUEST_OUT_OF_RANGE;
		}
	}
	else
	{
		/* Do not write any thing to EEPROM */
		diag_writedata_b = 0;
		diag_writedata_mode_c = 0;

		//DataLength = 1;
		Response = SECURITY_ACCESS_DENIED;
	}
	diagF_DiagResponse(Response, DataLength+SIZE_OF_BLOCKNUMBER_K, pMsgContext);
}

/*  ********************************************************************************
 * Function name:ApplDescWrite_222806_Voltage_Level_OFF (Service request header:$2E $28 $6 )
 
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescWrite_222806_Voltage_Level_OFF(DescMsgContext* pMsgContext)
{
	/* Initialize the DataLength. If to be adjusted logic adjusts */
	DataLength = 1;

	/* Byte 0: Voltage ON Threshold */
	if (diag_sec_access5_state_c == ECU_UNLOCKED)
	{
		if ( BattVF_Chck_Diag_Prms(VOLTAGE_OFF, (&pMsgContext->reqData[0])) == TRUE)
		{
			/* Data is OK Set the flag to write contents into EEPROM */
			diag_writedata_b = 1;
			DataLength = 0;
			Response = RESPONSE_OK;
			diag_writedata_mode_c = WRITE_DATA_FOR_VOLTAGE_OFF;
		}
		else
		{
			/* Do not write any thing to EEPROM */
			diag_writedata_b = 0;
			diag_writedata_mode_c = 0;

			//DataLength = 1;
			Response = REQUEST_OUT_OF_RANGE;
		}
	}
	else
	{
		/* Do not write any thing to EEPROM */
		diag_writedata_b = 0;
		diag_writedata_mode_c = 0;
		//DataLength = 1;
		Response = SECURITY_ACCESS_DENIED;
	}
	diagF_DiagResponse(Response, DataLength+SIZE_OF_BLOCKNUMBER_K, pMsgContext);
}

/*  ********************************************************************************
 * Function name:ApplDescWrite_222808_Heated_Seat_NTC_Cutoff_Parameters_Front_Seats (Service request header:$2E $28 $8 )
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescWrite_222808_Heated_Seat_NTC_Cutoff_Parameters_Front_Seats(DescMsgContext* pMsgContext)
{
	/* Initialize the DataLength. If to be adjusted logic adjusts */
	DataLength = 1;

	if ( diag_sec_access5_state_c == ECU_UNLOCKED)
	{
		if ( hsF_CheckDiagnoseParameter( HS_NTC_CUT_OFF,&pMsgContext->reqData[0] ))
		{
			/* Data is OK Set the flag to write contents into EEPROM */
			diag_writedata_b = 1;
			DataLength = 0;
			Response = RESPONSE_OK;
			diag_writedata_mode_c = WRITE_HEAT_NTC_CUTOFF;
		}
		else
		{
			/* Do not write any thing to EEPROM */
			diag_writedata_b = 0;
			diag_writedata_mode_c = 0;

			//DataLength = 1;
			Response = REQUEST_OUT_OF_RANGE;
		}
	}
	else
	{
		/* Do not write any thing to EEPROM */
		diag_writedata_b = 0;
		diag_writedata_mode_c = 0;

		//DataLength = 1;
		Response = SECURITY_ACCESS_DENIED;
	}
	diagF_DiagResponse(Response, DataLength+SIZE_OF_BLOCKNUMBER_K, pMsgContext);
}

/*  ********************************************************************************
 * Function name:ApplDescWrite_222809_Steering_Wheel_Temperature_Cut_off_Parameters (Service request header:$2E $28 $9 )
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescWrite_222809_Steering_Wheel_Temperature_Cut_off_Parameters (DescMsgContext* pMsgContext)
{
	/* Initialize the DataLength. If to be adjusted logic adjusts */
	DataLength = 1;

      /* WRITE_HSW_TEMP_CUTOFF*/
		
	/* Byte 0: Wheel Level 1 cut off */
	/* Byte 1: Wheel Level 2 cut off */
	/* Byte 2: Wheel Level 3 cut off */
	if ((CSWM_config_c & CSWM_HSW_MASK_K) == CSWM_HSW_MASK_K)
	{
		if (diag_sec_access5_state_c == ECU_UNLOCKED)
		{
			if ( stWF_Chck_Diag_Prms(HSW_TEMP_CUT_OFF, (&pMsgContext->reqData[0])) == TRUE)
			{
				/* Data is OK Set the flag to write contents into EEPROM */
				diag_writedata_b = 1;
				DataLength = 0;
				Response = RESPONSE_OK;
				diag_writedata_mode_c = WRITE_HSW_TEMP_CUTOFF;
			}
			else
			{
				/* Do not write any thing to EEPROM */
				diag_writedata_b = 0;
				diag_writedata_mode_c = 0;
				//DataLength = 1;
				Response = REQUEST_OUT_OF_RANGE;
			}
		}
		else
		{
			/* Do not write any thing to EEPROM */
			diag_writedata_b = 0;
			diag_writedata_mode_c = 0;

			//DataLength = 1;
			Response = SECURITY_ACCESS_DENIED;
		}
	}
	else {
		/* Do not write any thing to EEPROM */
		diag_writedata_b = 0;
		diag_writedata_mode_c = 0;

		//DataLength = 1;
		Response = SUBFUNCTION_NOT_SUPPORTED;
	}
	diagF_DiagResponse(Response, DataLength+SIZE_OF_BLOCKNUMBER_K, pMsgContext);
}

/*  ********************************************************************************
 * Function name:ApplDescWrite_22280A_HeatVent_Auto_Activation_Temperature_Calibration_Paramet (Service request header:$2E $28 $A )
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescWrite_22280A_HeatVent_Auto_Activation_Temperature_Calibration_Paramet(DescMsgContext* pMsgContext)
{
	/* Initialize the DataLength. If to be adjusted logic adjusts */
	DataLength = 1;

    /* AMbient temp limits for remote start */
		
	/* Byte 0: Heat Low  */
	/* Byte 1: Heat high */
	/* Byte 2: Vent Low */
	/* Byte 3: Vent High */

	if ( diag_sec_access5_state_c == ECU_UNLOCKED)
	{
		if ( hsF_CheckDiagnoseParameter( REMOTE_AMB_TEMP_LIMIT,&pMsgContext->reqData[0] ))
		{
			/* Data is OK Set the flag to write contents into EEPROM */
			diag_writedata_b = 1;
			DataLength = 0;
			Response = RESPONSE_OK;
			diag_writedata_mode_c = WRITE_AMB_TEMP_LIMITS_REMOTEST;
		}
		else
		{
			/* Do not write any thing to EEPROM */
			diag_writedata_b = 0;
			diag_writedata_mode_c = 0;

			//DataLength = 1;
			Response = REQUEST_OUT_OF_RANGE;
		}
	}
	else
	{
		/* Do not write any thing to EEPROM */
		diag_writedata_b = 0;
		diag_writedata_mode_c = 0;

		//DataLength = 1;
		Response = SECURITY_ACCESS_DENIED;
	}
	diagF_DiagResponse(Response, DataLength+SIZE_OF_BLOCKNUMBER_K, pMsgContext);
}

/*  ********************************************************************************
 * Function name:ApplDescWrite_22280C_Heated_Steering_Wheel_Min_Timeout_Parameters (Service request header:$2E $28 $C )
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescWrite_22280C_Heated_Steering_Wheel_Min_Timeout_Parameters(DescMsgContext* pMsgContext)
{
	/* Initialize the DataLength. If to be adjusted logic adjusts */
	DataLength = 1;

	/* Byte 0,1,2 and 3: Wheel Level 1,2,3 and 4 Timeout */
	if ((CSWM_config_c & CSWM_HSW_MASK_K) == CSWM_HSW_MASK_K)
	{
		if (diag_sec_access5_state_c == ECU_UNLOCKED)
		{
			if ( stWF_Chck_Diag_Prms(HSW_MIN_TIMEOUT, (&pMsgContext->reqData[0])) == TRUE)
			{
				/* Data is OK Set the flag to write contents into EEPROM */
				diag_writedata_b = 1;
				DataLength = 0;
				Response = RESPONSE_OK;
				diag_writedata_mode_c = WRITE_HSW_MIN_TIMEOUT;
			}
			else
			{
				/* Do not write any thing to EEPROM */
				diag_writedata_b = 0;
				diag_writedata_mode_c = 0;
				//DataLength = 1;
				Response = REQUEST_OUT_OF_RANGE;
			}
		}
		else
		{
			/* Do not write any thing to EEPROM */
			diag_writedata_b = 0;
			diag_writedata_mode_c = 0;

			//DataLength = 1;
			Response = SECURITY_ACCESS_DENIED;
		}
	}
	else {
		/* Do not write any thing to EEPROM */
		diag_writedata_b = 0;
		diag_writedata_mode_c = 0;

		//DataLength = 1;
		Response = SUBFUNCTION_NOT_SUPPORTED;
	}
	diagF_DiagResponse(Response, DataLength+SIZE_OF_BLOCKNUMBER_K, pMsgContext);
}

/*  ********************************************************************************
 * Function name:ApplDescWrite_22280E_Heated_Seat_PWM_Calibration_Parameters_Rear_Seats (Service request header:$2E $28 $E )
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescWrite_22280E_Heated_Seat_PWM_Calibration_Parameters_Rear_Seats(DescMsgContext* pMsgContext)
{
	/* Initialize the DataLength. If to be adjusted logic adjusts */
	DataLength = 1;

	if ((CSWM_config_c & CSWM_4HS_VARIANT_MASK_K) == CSWM_4HS_VARIANT_MASK_K)
	{
		if (diag_sec_access5_state_c == ECU_UNLOCKED)
		{
			if (hsF_CheckDiagnoseParameter( HS_PWM_REAR,&pMsgContext->reqData[0] ))
			{
				/* Data is OK Set the flag to write contents into EEPROM */
				diag_writedata_b = 1;
				DataLength = 0;
				Response = RESPONSE_OK;
				diag_writedata_mode_c = WRITE_HS_PWM_REAR;
			}
			else
			{
				/* Do not write any thing to EEPROM */
				diag_writedata_b = 0;
				diag_writedata_mode_c = 0;

				//DataLength = 1;
				Response = REQUEST_OUT_OF_RANGE;
			}
		}
		else
		{
			/* Do not write any thing to EEPROM */
			diag_writedata_b = 0;
			diag_writedata_mode_c = 0;

			//DataLength = 1;
			Response = SECURITY_ACCESS_DENIED;
		}
	}
	else
	{
		/* Do not write any thing to EEPROM */
		diag_writedata_b = 0;
		diag_writedata_mode_c = 0;

		//DataLength = 1;
		Response = SUBFUNCTION_NOT_SUPPORTED;
	}
	diagF_DiagResponse(Response, DataLength+SIZE_OF_BLOCKNUMBER_K, pMsgContext);
	
}


/*  ********************************************************************************
 * Function name:ApplDescWrite_22280F_Heated_Seat_Max_Timeout_Parameters_Rear_Seats (Service request header:$2E $28 $F )
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescWrite_22280F_Heated_Seat_Max_Timeout_Parameters_Rear_Seats(DescMsgContext* pMsgContext)
{
	/* Initialize the DataLength. If to be adjusted logic adjusts */
	DataLength = 1;

	if ((CSWM_config_c & CSWM_4HS_VARIANT_MASK_K) == CSWM_4HS_VARIANT_MASK_K)
	{
		if (diag_sec_access5_state_c == ECU_UNLOCKED)
		{
			if (hsF_CheckDiagnoseParameter( HS_TIMEOUT_REAR,&pMsgContext->reqData[0] ))
			{
				/* Data is OK Set the flag to write contents into EEPROM */
				diag_writedata_b = 1;
				DataLength = 0;
				Response = RESPONSE_OK;
				diag_writedata_mode_c = WRITE_HS_TIMEOUT_REAR;
			}
			else
			{
				/* Do not write any thing to EEPROM */
				diag_writedata_b = 0;
				diag_writedata_mode_c = 0;

				//DataLength = 1;
				Response = REQUEST_OUT_OF_RANGE;
			}
		}
		else
		{
			/* Do not write any thing to EEPROM */
			diag_writedata_b = 0;
			diag_writedata_mode_c = 0;

			//DataLength = 1;
			Response = SECURITY_ACCESS_DENIED;
		}
	}
	else
	{
		/* Do not write any thing to EEPROM */
		diag_writedata_b = 0;
		diag_writedata_mode_c = 0;

		//DataLength = 1;
		Response = SUBFUNCTION_NOT_SUPPORTED;
	}
	diagF_DiagResponse(Response, DataLength+SIZE_OF_BLOCKNUMBER_K, pMsgContext);
	
}


/*  ********************************************************************************
 * Function name:ApplDescWrite_222810_Heated_Seat_NTC_Cutoff_Parameters_Rear_Seats (Service request header:$2E $28 $10 )
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescWrite_222810_Heated_Seat_NTC_Cutoff_Parameters_Rear_Seats(DescMsgContext* pMsgContext)
{
	/* Initialize the DataLength. If to be adjusted logic adjusts */
	DataLength = 1;

	if ((CSWM_config_c & CSWM_4HS_VARIANT_MASK_K) == CSWM_4HS_VARIANT_MASK_K)
	{
		if (diag_sec_access5_state_c == ECU_UNLOCKED)
		{
			if (hsF_CheckDiagnoseParameter( HS_NTC_CUTOFF_REAR,&pMsgContext->reqData[0] ))
			{
				/* Data is OK Set the flag to write contents into EEPROM */
				diag_writedata_b = 1;
				DataLength = 0;
				Response = RESPONSE_OK;
				diag_writedata_mode_c = WRITE_HS_NTC_CUTOFF_REAR;
			}
			else
			{
				/* Do not write any thing to EEPROM */
				diag_writedata_b = 0;
				diag_writedata_mode_c = 0;

				//DataLength = 1;
				Response = REQUEST_OUT_OF_RANGE;
			}
		}
		else
		{
			/* Do not write any thing to EEPROM */
			diag_writedata_b = 0;
			diag_writedata_mode_c = 0;

			//DataLength = 1;
			Response = SECURITY_ACCESS_DENIED;
		}
	}
	else
	{
		/* Do not write any thing to EEPROM */
		diag_writedata_b = 0;
		diag_writedata_mode_c = 0;

		//DataLength = 1;
		Response = SUBFUNCTION_NOT_SUPPORTED;
	}
	diagF_DiagResponse(Response, DataLength+SIZE_OF_BLOCKNUMBER_K, pMsgContext);

}


/*  ********************************************************************************
 * Function name:ApplDescWrite_223000_Program_ECU_Data_at_Flash_Station_1 (Service request header:$2E $30 $0 )
 * 2E 3000 READ TEMIC BOM NUMBER -- 15 Byte length -- EOL SERVICES
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescWrite_223000_Program_ECU_Data_at_Flash_Station_1(DescMsgContext* pMsgContext)
{
	unsigned char i;
	for ( i = 0; i < 20; i++ )
	{
		/* Get all the, to be modified contents from External tool to Local variable */
		diag_temp_buffer_c[ i ] = pMsgContext->reqData[ i ];
	}

	if(diag_eol_io_lock_b)
	{
		/* Data is OK Set the flag to write contents into EEPROM */
		diag_writedata_b = 1;
		/*Set Writemode for Temic BOM Number. */
		diag_writedata_mode_c = WRITE_TEMIC_BOM_NUMBER;
		DataLength = 0;
		Response = RESPONSE_OK;
	}
	else
	{
		DataLength = 1;
		Response = CONDITIONS_NOT_CORRECT;
	}
	diagF_DiagResponse(Response, DataLength+SIZE_OF_BLOCKNUMBER_K, pMsgContext);

}


/*  ********************************************************************************
 * Function name:ApplDescWrite_223001_Program_ECU_Data_at_Flash_Station_2 (Service request header:$2E $30 $1 )
 * 2E 3001 READ CONTROL BYTE FINAL - 1 Byte Length -- EOL SERVICES
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescWrite_223001_Program_ECU_Data_at_Flash_Station_2(DescMsgContext* pMsgContext)
{
	unsigned char i;
	for ( i = 0; i < 20; i++ )
	{
		/* Get all the, to be modified contents from External tool to Local variable */
		diag_temp_buffer_c[ i ] = pMsgContext->reqData[ i ];
	}

	if(diag_eol_io_lock_b)
	{
		/* Data is OK Set the flag to write contents into EEPROM */
		diag_writedata_b = 1;
		/*Set Writemode for Control Byte Final. */
		diag_writedata_mode_c = WRITE_CTRL_BYTE_FINAL;
		DataLength = 0;
		Response = RESPONSE_OK;
	}
	else
	{
		DataLength = 1;
		Response = CONDITIONS_NOT_CORRECT;
	}
	diagF_DiagResponse(Response, DataLength+SIZE_OF_BLOCKNUMBER_K, pMsgContext);

}

/*  ********************************************************************************
 * Function ApplDescWriteEcuSerialNumber (Service request header:$2E $F1 $8C )
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescWriteEcuSerialNumber(DescMsgContext* pMsgContext)
{
	unsigned char i;
	for ( i = 0; i < 20; i++ )
	{
		/* Get all the, to be modified contents from External tool to Local variable */
		diag_temp_buffer_c[ i ] = pMsgContext->reqData[ i ];
	}

	if(diag_eol_io_lock_b)
	{
		/* Set the Flag to Write contents to EEPROM */
		diag_writedata_b = TRUE;
		/*Set Writemode for ECU Serial Number. */
		diag_writedata_mode_c = WRITE_ECU_SERIAL_NUMBER;
		DataLength = 0;
		Response = RESPONSE_OK;
	}
	else
	{
		DataLength = 1;
		Response = CONDITIONS_NOT_CORRECT;
	}
	diagF_DiagResponse(Response, DataLength+SIZE_OF_BLOCKNUMBER_K, pMsgContext);

}

/*  ********************************************************************************
 * Function ApplDescWriteVIN (Service request header:$2E $F1 $90 )
 * Description:The access to this label for writing MUST be protected by a proper "seed&key"
 * algorithm. The selected algorithm must NOT be the same used for download
 * purpose!
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescWriteVIN(DescMsgContext* pMsgContext)
{
  /* <<TBD>> Remove this comment once you have completely implemented this function!!! */
  /* Dummy example how to access the request data. */
  /* Assumming the FIRST DATA byte contains important data which has to be less than a constant value. */
//  if(pMsgContext->reqData[0] < 0xFF)
//  {
//    /* Received data is in range process further. */
//    /* Contains no response data */
//  }
//  else
//  {
    /* Request contains invalid data - send negative response! */
    DescSetNegResponse(kDescNrcRequestOutOfRange);
//  }
  /* User service processing finished. */
  DescProcessingDone();
}

/**************************************************************
*  Name                 :  DIDWRITE_System_Configuration_PROXI
*  Description          :  PROXI configuration file
*  Parameters           :  [Input, Output, Input / output]
*  Return               :  nothing
*  Critical/explanation :  [yes / No]
**************************************************************/
void DESC_API_CALLBACK_TYPE ApplDescWrite_2E2023_System_Configuration_PROXI(DescMsgContext* pMsgContext)
{
	DescNegResCode  lu_NRC = kDescNrcNone; 
	u_8Bit lub_PROXIautoTrigger;
	u_8Bit lub_errCnt,temp_PROXI_learned;
	u_8Bit lub_index = 0u;
	U_PROXI_DATA lu_PROXI_Data_Temp;
	U_PROXI_DATA lu_PROXI_Data_OrigBackup;
	u_16Bit luw_CalculatedCRC; 
	u_16Bit luw_ReceivedCRC;

	//This needs to be adjusted according to the CSWM
	if(re_PROXI_state != E_PROXI_LRN_START)
	{
//		/* PROXI learning has been started, process timeout */
//		if( ruw_didwrite_pending_timeout != (T_UWORD)DIDWRITE_OFF  )
//		{
//			ruw_didwrite_pending_timeout--;
//			if(ruw_didwrite_pending_timeout == (T_UWORD)DIDWRITE_OFF)
//			{
//				/* Timeout reached  */
//				lu_NRC = kDescNrcGeneralProgrammingFailure;
//				re_PROXI_state = E_PROXI_NEG_RSP;
//			}
//		}
//		else
//		{
//			/* For robustness, if this function is called with
//			 * ruw_didwrite_pending_timeout already expired, send negative
//			 * response and cancel service repeat */
//			/* Timeout reached  */
			lu_NRC = kDescNrcGeneralProgrammingFailure;
			re_PROXI_state = E_PROXI_NEG_RSP;
//		}
	}
	/* If current state is E_PROXI_LRN_START don't need to process timeout, fallthrough state machine execution. */

	/* do...while structure is used to allow inmediate state transitions */
	do
	{
		lub_PROXIautoTrigger = FALSE;

		/* Copy PROXI Info into temporal variable */
		/* Veh_ID */									/* Function needs to be added */
		lu_PROXI_Data_Temp.s.PROXI_IDVeh.s.Digit11 = didwrite_rfhub_ReturnPROXISignal(&pMsgContext->reqData[0], PROXI_DIGIT_11_BYTE, PROXI_DIGIT_MASK, PROXI_DIGIT_OFFSET);
		lu_PROXI_Data_Temp.s.PROXI_IDVeh.s.Digit10 = didwrite_rfhub_ReturnPROXISignal(&pMsgContext->reqData[0], PROXI_DIGIT_10_BYTE, PROXI_DIGIT_MASK, PROXI_DIGIT_OFFSET);
		lu_PROXI_Data_Temp.s.PROXI_IDVeh.s.Digit9 = didwrite_rfhub_ReturnPROXISignal(&pMsgContext->reqData[0], PROXI_DIGIT_9_BYTE, PROXI_DIGIT_MASK, PROXI_DIGIT_OFFSET);
		lu_PROXI_Data_Temp.s.PROXI_IDVeh.s.Digit8 = didwrite_rfhub_ReturnPROXISignal(&pMsgContext->reqData[0], PROXI_DIGIT_8_BYTE, PROXI_DIGIT_MASK, PROXI_DIGIT_OFFSET);
		lu_PROXI_Data_Temp.s.PROXI_IDVeh.s.Digit7 = didwrite_rfhub_ReturnPROXISignal(&pMsgContext->reqData[0], PROXI_DIGIT_7_BYTE, PROXI_DIGIT_MASK, PROXI_DIGIT_OFFSET);
		lu_PROXI_Data_Temp.s.PROXI_IDVeh.s.Digit6 = didwrite_rfhub_ReturnPROXISignal(&pMsgContext->reqData[0], PROXI_DIGIT_6_BYTE, PROXI_DIGIT_MASK, PROXI_DIGIT_OFFSET);

		/* CRC */
		lu_PROXI_Data_Temp.s.PROXI_CRC.s.Digit5 = didwrite_rfhub_ReturnPROXISignal(&pMsgContext->reqData[0], PROXI_DIGIT_5_BYTE, PROXI_DIGIT_MASK, PROXI_DIGIT_OFFSET);
		lu_PROXI_Data_Temp.s.PROXI_CRC.s.Digit4 = didwrite_rfhub_ReturnPROXISignal(&pMsgContext->reqData[0], PROXI_DIGIT_4_BYTE, PROXI_DIGIT_MASK, PROXI_DIGIT_OFFSET);
		lu_PROXI_Data_Temp.s.PROXI_CRC.s.Digit3 = didwrite_rfhub_ReturnPROXISignal(&pMsgContext->reqData[0], PROXI_DIGIT_3_BYTE, PROXI_DIGIT_MASK, PROXI_DIGIT_OFFSET);
		lu_PROXI_Data_Temp.s.PROXI_CRC.s.Digit2 = didwrite_rfhub_ReturnPROXISignal(&pMsgContext->reqData[0], PROXI_DIGIT_2_BYTE, PROXI_DIGIT_MASK, PROXI_DIGIT_OFFSET);
		lu_PROXI_Data_Temp.s.PROXI_CRC.s.Digit1 = didwrite_rfhub_ReturnPROXISignal(&pMsgContext->reqData[0], PROXI_DIGIT_1_BYTE, PROXI_DIGIT_MASK, PROXI_DIGIT_OFFSET);
		lu_PROXI_Data_Temp.s.PROXI_CRC.s.unused0 = NOT_USED_VAL; /* Last byte do not care - always zero */

		/* PROXI Configuration data */
		lu_PROXI_Data_Temp.s.Driver_Side = didwrite_rfhub_ReturnPROXISignal(&pMsgContext->reqData[0], PROXI_DS_BYTE, PROXI_DS_MASK, PROXI_DS_OFFSET);
		lu_PROXI_Data_Temp.s.Heated_Seats_Variant = didwrite_rfhub_ReturnPROXISignal(&pMsgContext->reqData[0], PROXI_HEATSEAT_BYTE, PROXI_HEATSEAT_MASK, PROXI_HEATSEAT_OFFSET);
		lu_PROXI_Data_Temp.s.Remote_Start = didwrite_rfhub_ReturnPROXISignal(&pMsgContext->reqData[0], PROXI_RS_BYTE, PROXI_RS_MASK, PROXI_RS_OFFSET);
		lu_PROXI_Data_Temp.s.Seat_Material = didwrite_rfhub_ReturnPROXISignal(&pMsgContext->reqData[0], PROXI_SEAT_TYPE_BYTE, PROXI_SEAT_TYPE_MASK, PROXI_SEAT_TYPE_OFFSET);
		lu_PROXI_Data_Temp.s.Wheel_Material = didwrite_rfhub_ReturnPROXISignal(&pMsgContext->reqData[0], PROXI_WHEEL_TYPE_BYTE, PROXI_WHEEL_TYPE_MASK, PROXI_WHEEL_TYPE_OFFSET);
		lu_PROXI_Data_Temp.s.Steering_Wheel = didwrite_rfhub_ReturnPROXISignal(&pMsgContext->reqData[0], PROXI_WHEEL_BYTE, PROXI_WHEEL_MASK, PROXI_WHEEL_OFFSET);
		lu_PROXI_Data_Temp.s.Country_Code = didwrite_rfhub_ReturnPROXISignal(&pMsgContext->reqData[0], PROXI_COUNTRY_BYTE, PROXI_COUNTRY_MASK, PROXI_COUNTRY_OFFSET);
		lu_PROXI_Data_Temp.s.Vehicle_Line_Configuration = didwrite_rfhub_ReturnPROXISignal(&pMsgContext->reqData[0], PROXI_VL_BYTE, PROXI_VL_MASK, PROXI_VL_OFFSET);
		lu_PROXI_Data_Temp.s.Model_Year = didwrite_rfhub_ReturnPROXISignal(&pMsgContext->reqData[0], PROXI_MY_BYTE, PROXI_MY_MASK, PROXI_MY_OFFSET);
		lu_PROXI_Data_Temp.s.unused0 = NOT_USED_VAL; /* Unused bits */

		switch( re_PROXI_state )
		{
			case E_PROXI_LRN_START:
				/* Clear error flag */
				lub_errCnt = 0;

				/* Harsha: Update the Rejection feedback table to EEPROM with 0x00. 9 byte value in EEPROM */
				/* Clear Rejection Feedback table */
				//memfill((u_8Bit *)ras_Rejection_Feedback_Tbl, PROXI_REJ_FEEDBACK_TBL_SIZE, REJECTION_FEEDBACK_DEFAULT);
				//(void)eeprom_BlockUpdate(PROXI_REJECTION_FEEDBACK_TBL);
				memset((u_8Bit *)ras_Rejection_Feedback_Tbl, REJECTION_FEEDBACK_DEFAULT, PROXI_REJ_FEEDBACK_TBL_SIZE);
				EE_BlockWrite(EE_PROXI_FAILURE, (UINT8*)&ras_Rejection_Feedback_Tbl[0]);
				
				
				/* Perform Configuration Code Check */
				for( lub_index = 0; lub_index < CFG_CODE_LEN; lub_index++ )
				{
					if( ((pMsgContext->reqData[lub_index]) >= LOW_CFG_CODE_LIMIT ) &&
						((pMsgContext->reqData[lub_index]) <= UP_CFG_CODE_LIMIT) )
					{
						/* no error - do nothing */
					}
					else
					{
						/* ERROR found, if max number of error not reached yet save new error on Rejection Feedback table */
						if( (lub_errCnt < PROXI_MAX_NUM_ERRORS) )
						{
							lu_NRC = kDescNrcInvalidFormat;
							/* This function to be added */
							didwrite_rfhub_SetPROXIError(lub_errCnt, lub_index, PROXI_CFG_CODE_MASK, lu_NRC);
							lub_errCnt++;
						}
					}
				}

				/* Perform Plausibility check */
				/* Heated Seats Variant Information check */
				if(	(lub_errCnt < PROXI_MAX_NUM_ERRORS) &&
					((u_8Bit)lu_PROXI_Data_Temp.s.Heated_Seats_Variant == FALSE ) )
				{
					lu_NRC = kDescNrcInvalidFormat;
					didwrite_rfhub_SetPROXIError(lub_errCnt, PROXI_HEATSEAT_BYTE, PROXI_HEATSEAT_MASK, lu_NRC);
					lub_errCnt++;
				}

				/* Seat Material Check */
				if( (lub_errCnt < PROXI_MAX_NUM_ERRORS) &&
					((u_8Bit)lu_PROXI_Data_Temp.s.Seat_Material == FALSE ) )
				
				{
					lu_NRC = kDescNrcInvalidFormat;
					didwrite_rfhub_SetPROXIError(lub_errCnt, PROXI_SEAT_TYPE_BYTE, PROXI_SEAT_TYPE_MASK, lu_NRC);
					lub_errCnt++;
				}

				/* Steering Wheel Variant Information check */
				if( (lub_errCnt < PROXI_MAX_NUM_ERRORS) &&
					((u_8Bit)lu_PROXI_Data_Temp.s.Steering_Wheel == PROXI_WHEEL_ABSENT ) )
				{
					lu_NRC = kDescNrcInvalidFormat;
					didwrite_rfhub_SetPROXIError(lub_errCnt, PROXI_WHEEL_BYTE, PROXI_WHEEL_MASK, lu_NRC);
					lub_errCnt++;
				}
				
				/* Steering Wheel Material Type check */
				if( (lub_errCnt < PROXI_MAX_NUM_ERRORS) &&
					((u_8Bit)lu_PROXI_Data_Temp.s.Wheel_Material == PROXI_WHEEL_TYPE_INVALID ) )
				{
					lu_NRC = kDescNrcInvalidFormat;
					didwrite_rfhub_SetPROXIError(lub_errCnt, PROXI_WHEEL_TYPE_BYTE, PROXI_WHEEL_TYPE_BYTE, lu_NRC);
					lub_errCnt++;
				}

				/* VEHICLE_LINE_CONFIGURATION check */
				if( (lub_errCnt < PROXI_MAX_NUM_ERRORS) &&
					(lu_PROXI_Data_Temp.s.Vehicle_Line_Configuration == INVALID_DATA ) )
				{
					lu_NRC = kDescNrcInvalidFormat;
					didwrite_rfhub_SetPROXIError(lub_errCnt, PROXI_VL_BYTE, PROXI_VL_MASK, lu_NRC);
					lub_errCnt++;
				}
//				else
//				{
//					/* Received VehLine from PROXI is in Calibration File */
//					if( (lub_errCnt < PROXI_MAX_NUM_ERRORS) && (didwrite_rfhub_IsPROXIVehLineInCalFile(lu_PROXI_Data_Temp.s.Vehicle_Line_Configuration) == FALSE) )
//					{
//						lu_NRC = kDescNrcRequestOutOfRange;
//						didwrite_rfhub_SetPROXIError(lub_errCnt, PROXI_VL_BYTE, PROXI_VL_MASK, lu_NRC);
//						lub_errCnt++;
//					}
//				}

				/* MAX number of error not reached yet */
				if( lub_errCnt < PROXI_MAX_NUM_ERRORS )
				{
					/* Calculate CRC and perform CRC check */
					luw_CalculatedCRC = 0;
					luw_ReceivedCRC = 0;

					for (lub_index = START_CRC_CALC_INDEX; lub_index < PROXI_FILE_LEN ; lub_index++)
					{
						luw_CalculatedCRC = didwrite_rfhub_reversed_crc16_mmc_upd( luw_CalculatedCRC, pMsgContext->reqData[lub_index]);
					}

					luw_ReceivedCRC = didwrite_rfhub_convert_ascii_string_to_num(&pMsgContext->reqData[CRC_INDEX_PROXI_FILE], CRC_NUM_OF_DIGITS);

					
					/* Check CRC byte-by-byte here, writing every mismatch to table */
					/* If lub_errCnt changed after this call, errors were found */
					lub_errCnt = didwrite_rfhub_check_crc_byte_by_byte(luw_CalculatedCRC, luw_ReceivedCRC, lub_errCnt);

					/* No errors were found in the CRC */
					if( lub_errCnt == 0 )
					{
						/* Save a local backup of the previous PROXI configuration in case EEPROM manager rejects the request */
						memcpy((u_8Bit *)lu_PROXI_Data_OrigBackup.byte,(u_8Bit *)ru_eep_PROXI_Cfg.byte, PROXI_DATA_SIZE);
						memcpy((u_8Bit *)ru_eep_PROXI_Cfg.byte,(u_8Bit *)lu_PROXI_Data_Temp.byte, PROXI_DATA_SIZE);

//						/* no error found on the PROXI Check, update EEPROM */
//						if(eeprom_BlockUpdate(PROXI_CFG) != E_SWP_OK)
//						{
//							/* Not possible to update EEPROM block now */
//							/* Return PROXI_Cfg RAM image to the backup saved */
//							memcpy((T_UBYTE *)ru_eep_PROXI_Cfg.byte,(T_UBYTE *)lu_PROXI_Data_OrigBackup.byte, PROXI_DATA_SIZE);
//
//							lu_NRC = kDescNrcGeneralProgrammingFailure;
//							re_PROXI_state = E_PROXI_NEG_RSP;
//							lub_PROXIautoTrigger = TRUE;
//						}
//						else
//						{
							/* No error requesting block update */
							/* DO NOT SEND RESPONSE YET, start monitoring until EEPROM operation is complete*/
							/* launch pending timeout counter  */
							//ruw_didwrite_pending_timeout = DIDWRITE_PROXI_PENDING_TIMEOUT;
							/* Go to next state */
							re_PROXI_state = E_PROXI_CFG_UPDATE;
							//lub_PROXIautoTrigger = FALSE;
							lub_PROXIautoTrigger = TRUE;
//						}
					}
//					else
//					{
//						/* CRC does not match send negative response ORIGINAL*/
//						lu_NRC = kDescNrcRequestOutOfRange;
//						lub_PROXIautoTrigger = TRUE;
//						re_PROXI_state = E_REJ_TABLE_UPDATE;
//						lub_errCnt++;
//						
////						/* After Chrysler Recommendation Report First byte of CRC as failure in 102A*/
////						lu_NRC = kDescNrcRequestOutOfRange;
////						didwrite_rfhub_SetPROXIError(lub_errCnt, PROXI_DIGIT_5_BYTE, PROXI_CFG_CODE_MASK, lu_NRC);
////						re_PROXI_state = E_REJ_TABLE_UPDATE;
////						lub_PROXIautoTrigger = TRUE;
////						lub_errCnt++;
//
//					}
				}
				
				
				
				if(lub_errCnt > 0)
				{
//					/* At least one error found, update Rejection Feedback table in EEPROM */
//					if(eeprom_BlockUpdate(PROXI_REJECTION_FEEDBACK_TBL) != E_SWP_OK)
//					{
//						/* Not possible to update EEPROM block now */
//						lu_NRC = kDescNrcGeneralProgrammingFailure;
//						re_PROXI_state = E_PROXI_NEG_RSP;
//						lub_PROXIautoTrigger = TRUE;
//					}
//					else
//					{
						/* No error requesting block update */
						/*  DO NOT SEND RESPONSE YET, start monitoring until EEPROM operation is complete*/
						/*  launch pending timeout counter  */
						//ruw_didwrite_pending_timeout = DIDWRITE_PROXI_PENDING_TIMEOUT;
						/* Go to next state */
						re_PROXI_state = E_REJ_TABLE_UPDATE;
						//lub_PROXIautoTrigger = FALSE;
						lub_PROXIautoTrigger = TRUE;
//					}
				}

				break;

			case E_PROXI_CFG_UPDATE:
//				/* Check if update operation */
//				if((T_UWORD)EEPBLK_IDLE_OK != EEPROMBlock_Status(PROXI_CFG))
//				{
//					/* Block is not yet updated */
//				}
//				else
//				{
					/* Block updated */
					re_PROXI_state = E_PROXI_POS_RSP;
					lub_PROXIautoTrigger = TRUE;
					//rub_PROXI_Learned = TRUE;
					//(void) eeprom_BlockUpdate(IS_PROXI_LEARNED);
					EE_BlockWrite(EE_PROXI_CFG, (UINT8*)&ru_eep_PROXI_Cfg);
					temp_PROXI_learned = 0xAA;
					EE_BlockWrite(EE_PROXI_LEARNED, &temp_PROXI_learned);
					
					
					/* Rejection Table already cleared at start of learning */
//				}

				break;

			case E_REJ_TABLE_UPDATE:
//				/* Check if update operation */
//				if((T_UWORD)EEPBLK_IDLE_OK != EEPROMBlock_Status(PROXI_REJECTION_FEEDBACK_TBL))
//				{
//					/* Block is not yet updated */
//				}
//				else
//				{
					lu_NRC = GET_FIRST_NRC();
				    EE_BlockWrite(EE_PROXI_FAILURE, (UINT8*)&ras_Rejection_Feedback_Tbl[0]);
					re_PROXI_state = E_PROXI_NEG_RSP;
					lub_PROXIautoTrigger = TRUE;
//				}

				break;

			case E_PROXI_POS_RSP:
				re_PROXI_state = E_PROXI_LRN_START;
				/* Set inital conditions of pending write mechanism */
				//didwrite_pending_init();
				/* No negative response */
				DescSetNegResponse(kDescNrcNone);
				/* User service processing finished. */
				DescProcessingDone();
				
				lub_PROXIautoTrigger = FALSE;

				break;

			case E_PROXI_NEG_RSP:
				/* Must never reach with no NRC set */
				if(lu_NRC == kDescNrcNone)
				{
					lu_NRC = kDescNrcGeneralProgrammingFailure;
				}
				/* Init state for this DID */
				re_PROXI_state = E_PROXI_LRN_START;
				/* Set inital conditions of pending write mechanism */
				//didwrite_pending_init();
				/* Set negative response */
				DescSetNegResponse(lu_NRC);
				/* User service processing finished. */
				DescProcessingDone();
				
				lub_PROXIautoTrigger = FALSE;
				break;

			default:
				/* Must not enter this state */
				re_PROXI_state = E_PROXI_NEG_RSP;
				lub_PROXIautoTrigger = TRUE;

				break;
		}
	}
	while(lub_PROXIautoTrigger);
}


/********************************************************************************
 * NEW RFHM
 *******************************************************************************/
/**************************************************************
*  Name                 :  didwrite_rfhub_convert_ascii_string_to_num
*  Description          :  Calculate CRC
*  Parameters           :  T_UWORD crc_old, T_UBYTE data
*  Return               :  T_UWORD crc
*  Critical/explanation :  [yes / No]
**************************************************************/
u_16Bit didwrite_rfhub_convert_ascii_string_to_num(DescMsgItem * pub_CRCStr, u_8Bit lub_CRCStrLen)
{
	u_8Bit lub_StrIndex;
	u_16Bit luw_Unit;
	u_16Bit luw_CRCReturn;

	luw_CRCReturn = 0;
	luw_Unit = 1;
    for(lub_StrIndex = lub_CRCStrLen; lub_StrIndex; lub_StrIndex--)
    {
    	luw_CRCReturn += multiply_u16_u16_u16((u_16Bit)(pub_CRCStr[lub_StrIndex-1] & 0x0F), luw_Unit);
    	luw_Unit = multiply_u16_u16_u16(luw_Unit, 10);
    }

    return luw_CRCReturn;
}

/**************************************************************
*  Name                 :  didwrite_rfhub_reversed_crc16_mmc_upd
*  Description          :  Calculate CRC
*  Parameters           :  T_UWORD crc_old, T_UBYTE data
*  Return               :  T_UWORD crc
*  Critical/explanation :  [yes / No]
**************************************************************/
u_16Bit didwrite_rfhub_reversed_crc16_mmc_upd(u_16Bit luw_crc_old, u_8Bit lub_data)
{
	u_8Bit lub_cnt_bits, lub_flag_xor;
    for (lub_cnt_bits = 8; lub_cnt_bits; lub_cnt_bits--)
    {
        lub_flag_xor = (luw_crc_old ^ lub_data) & 0x01;
        lub_data >>= 1;
        luw_crc_old >>= 1;
        if (lub_flag_xor) luw_crc_old ^= 0x8408;
    }
    return(luw_crc_old);
}

/**************************************************************
*  Name                 :  didwrite_rfhub_ReturnPROXISignal
*  Description          :  Return a signal from the PROXI file
*  Parameters           :  T_UBYTE lub_BytePos, T_UBYTE lub_SignalMask, T_UBYTE lub_Offset
*  Return               :  Signal value
*  Critical/explanation :  [yes / No]
**************************************************************/
u_8Bit didwrite_rfhub_ReturnPROXISignal(DescMsgItem * data, u_8Bit lub_BytePos, u_8Bit lub_SignalMask, u_8Bit lub_Offset)
{
	return (u_8Bit)((data[lub_BytePos] & lub_SignalMask) >> lub_Offset );
}

/**************************************************************
*  Name                 :  didwrite_rfhub_SetPROXIError
*  Description          :  Updates Rejection Feedback Table ram image later EEPROM block will be updated
*  Parameters           :  T_UBYTE lub_RecordPos, T_UBYTE lub_ErrorBytePos, T_UBYTE lub_ErrorMask, T_UBYTE lub_ErrorType
*  Return               :  None
*  Critical/explanation :  [yes / No]
**************************************************************/
void didwrite_rfhub_SetPROXIError(u_8Bit lub_RecordPos, u_8Bit lub_ErrorBytePos, u_8Bit lub_ErrorMask, DescNegResCode lub_ErrorType)
{
	if( lub_RecordPos < PROXI_MAX_NUM_ERRORS )
	{
		ras_Rejection_Feedback_Tbl[lub_RecordPos].Error_Type = lub_ErrorType;
		ras_Rejection_Feedback_Tbl[lub_RecordPos].Error_BytePos = lub_ErrorBytePos + 1;
		ras_Rejection_Feedback_Tbl[lub_RecordPos].Error_Mask = lub_ErrorMask;
	}
}

/**************************************************************
*  Name                 :  didwrite_rfhub_check_crc_byte_by_byte
*  Description          :  CRC byte-by-byte check
*  Parameters           :  T_UWORD received_CRC, T_UWORD calculated_CRC
*  Return               :  NEW value of error count, if unchanged
*  							no errors occurred in the CRC check
*  Critical/explanation :  [yes / No]
**************************************************************/
u_8Bit didwrite_rfhub_check_crc_byte_by_byte(u_16Bit luw_ReceivedCRC, u_16Bit luw_CalculatedCRC, u_8Bit lub_ErrCnt)
{
	u_8Bit lub_ByteNum;
	u_8Bit lub_ReceivedChar;
	u_8Bit lub_CalculatedChar;
	DescNegResCode lu_NRC = kDescNrcRequestOutOfRange;
	//T_UBYTE lub_Test;


	for(lub_ByteNum = CRC_LOW_BYTE_LOC; lub_ByteNum >= CRC_HIGH_BYTE_LOC; lub_ByteNum--)
	{
		lub_ReceivedChar = luw_ReceivedCRC % CRC_DECIMAL_FACTOR + CRC_CHARACTER_OFFSET;
		lub_CalculatedChar = luw_CalculatedCRC % CRC_DECIMAL_FACTOR + CRC_CHARACTER_OFFSET;
		if(lub_ReceivedChar != lub_CalculatedChar)
		{
			rub_Test = (lub_ReceivedChar ^ lub_CalculatedChar);
			/* record error in byte_num */
			didwrite_rfhub_SetPROXIError(lub_ErrCnt, lub_ByteNum, rub_Test, lu_NRC);
			lub_ErrCnt++;
			if(lub_ErrCnt == PROXI_MAX_NUM_ERRORS)
			{
				return lub_ErrCnt;
			}
		}
		luw_ReceivedCRC /= CRC_DECIMAL_FACTOR;
		luw_CalculatedCRC /= CRC_DECIMAL_FACTOR;
	}

	return lub_ErrCnt;
}

/**************************************************************
 *  Name                 :  multiply_u16_u16_u16
 *  TAG                  :  Covers_SWR_D_CORE_10_002 API HAL
 *  Description          :  multiplies an unsigned 16-bit value
 *                          with an unsigned 16-bit value and 
 *                          returns a 16-bit value as result
 *  Parameters           :  INPUT : T_UWORD, T_UWORD
 *  Return               :  T_UWORD
 *  Critical/explanation :    No
 **************************************************************/
u_16Bit multiply_u16_u16_u16 (u_16Bit ruw_op1, u_16Bit ruw_op2)
{
  return ((u_16Bit)ruw_op1*ruw_op2);
}

