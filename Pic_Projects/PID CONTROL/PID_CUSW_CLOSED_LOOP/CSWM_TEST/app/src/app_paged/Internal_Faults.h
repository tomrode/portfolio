#ifndef INTERNAL_FAULTS_H
#define INTERNAL_FAULTS_H

/********************************************************************************/
/*                        A U T H O R   I D E N T I T Y							*/
/********************************************************************************/
/* Initials      Name                   Company									*/
/* --------      --------------------   --------------------------------------- */ 
/* CK            Can Kulduk             Continental Automotive Systems			*/
/* HY            Harsha Yekollu         Continental Automotive Systems          */
/********************************************************************************/


/*********************************************************************************/
/*                               CHANGE HISTORY                                  */
/*********************************************************************************/
/* 05.09.2007 CK 		-   header file created					     			 */
/* 03.24.2008 HY		-   copied from CSWM MY09 project.                		 */
/* 10.09.2008 CK        -   Started adapting code for CSWM MY11                  */
/* 10.22.2008 CK        -   Updated internal fault defines                       */
/* 03.31.2009 CK        -   RAM test is enabled. Updated related defines.        */
/*********************************************************************************/


/******************************* @Enumaration(s) ********************************/
/* Internal Fault bytes */
typedef enum{
  ZERO = 0,             // Byte 0
  ONE = 1,              // Byte 1
  TWO = 2,              // Byte 2
  THREE = 3,            // Byte 3
  INT_FLT_BYTES = 4     // Number of bytes
}Int_Flt_Bytes_e;

/* Internal fault operations */
typedef enum{
  INT_FLT_SET = 0,      // Operation that sets the selected internal fault bit  
  INT_FLT_CLR = 1       // Operation that clears the selected internal fault bit 
}Int_Flt_Op_Type_e;
/**********************************************************************************/


/******************************** @Global variables *******************************/
extern union char_bit int_flt_byte0;
#define Crystal_Osci_Flt_b       int_flt_byte0.b.b0    // Crystal Oscillator fault
#define PLL_Flt_b                int_flt_byte0.b.b1    // PLL fault
#define Watchdog_Reset_b         int_flt_byte0.b.b2    // Watchdog reset (not used)
#define Rom_chksum_err_b         int_flt_byte0.b.b3    // Rom checksum error
#define Ram_err_b                int_flt_byte0.b.b4    // Ram error
#define EEPROM_err_b             int_flt_byte0.b.b5    // EEPROM error (not used)
#define Micro_clk_err_b          int_flt_byte0.b.b6    // Microcontroller clock error (not used. same as oscillator error)
// Bit 7 is not used

extern union char_bit int_flt_byte1;
#define RelayA_Stuck_HI_b        int_flt_byte1.b.b0    // Relay A Stuck High
#define RelayA_Stuck_LO_b        int_flt_byte1.b.b1    // Relay A Stuck Low
#define RelayB_Stuck_HI_b        int_flt_byte1.b.b2    // Relay B Stuck High
#define RelayB_Stuck_LO_b        int_flt_byte1.b.b3    // Relay B Stuck Low
#define RelayC_Stuck_HI_b        int_flt_byte1.b.b4    // Relay C Stuck High (not used)
#define RelayC_Stuck_LO_b        int_flt_byte1.b.b5    // Relay C Stuck Low (not used)
#define F_HS_Driver_Flt_b        int_flt_byte1.b.b6    // Front heated seat driver fault
#define R_HS_Driver_Flt_b        int_flt_byte1.b.b7    // Rear heated seat driver fault

/* PCB_Over_Temp detection logic changed from S1(SW0803) to S2(0811) .. Tracker Id#47  */
/* PCB_Over_Temp_b flag is not used any more with internal faults                      */
/* No internal Fault incase of PCB over temperature.                                   */
extern union char_bit int_flt_byte2;
#define PCB_Sensor_HI_b          int_flt_byte2.b.b0    // (Not used) PCB Sensor reading too high (Short to battery/open)
#define PCB_Sensor_LO_b          int_flt_byte2.b.b1    // (Not Used) PCB Sensor reading too low  (Short to ground)
#define NTC_Supply_Voltage_Flt_b int_flt_byte2.b.b2    // Low NTC supply voltage (Occurance)
#define PCB_Over_Temp_b          int_flt_byte2.b.b3    // PCB over temperature condition (Not used S2 onwards)
#define Wheel_LS_FET_ON_b        int_flt_byte2.b.b4    // StWheel low side fet always on  (Not used)
#define Wheel_LS_FET_OFF_b       int_flt_byte2.b.b5    // StWheel low side fet always off (Not used)
// Bit 6 is not used
// Bit 7 is not used

/* These bits will not set an internal fault */
extern union char_bit int_flt_byte3;                   // OCCURANCES (NOT FAULTS)
#define Erratic_Local_Voltage_b  int_flt_byte3.b.b0    // System and Battery voltage reading difference > 2.0 V
// Bit 1 is not used
// Bit 2 is not used
// Bit 3 is not used
// Bit 4 is not used
// Bit 5 is not used
// Bit 6 is not used
// Bit 7 is not used

/* Stores the read values from EEPROM block */
extern u_8Bit intFlt_bytes[INT_FLT_BYTES];

/* Stores the Enable(0x55) / Disable(0xAA) high priority fault (disables CAN comm) detection option */
extern u_8Bit intFlt_config_c;
/**********************************************************************************/


/******************************* @Constants/Defines *******************************/
#define NO_INT_FLT                   0     // No internal fault bits are set for this byte
#define INTFLT_ONE_BYTE              1     // Byte Lenght
#define INT_LED_OFF                  0     // LED off
#define INTFLT_CAN_DISABLE_TM        20    // 101 cycles after a fault is detected
#define INTFLT_SELF_CLK_MODE       0xAA    // Indicates self clock mode
#define INTFLT_NORMAL_CLK_MODE     0x00    // Indicates normal clock mode
#define ENABLE_HIGH_PRIORITY_INTFLT_DET  0x55 // Detect Internal faults that disable CAN communication
#define DISABLE_HIGH_PRIORITY_INTFLT_DET 0xAA // Do not detect faults that disable CAN communication

/* Set Internal Fault Defines */
/* Byte 0 */
#define CRYSTAL_OSCILLATOR_MASK      1    // Oscillator not working properly 
#define PLL_ERR_MASK                 2    // Phase lock does not take place
#define WATCHDOG_RESET_MASK          4    // Watchdog resets (not used)
#define ROM_CHKSUM_ERR_MASK          8    // Rom checksum error
#define RAM_ERR_MASK                16    // Ram cell(s) error
#define EEPROM_ERR_MASK             32    // EEPROM error (Not used)
#define MICRO_CLK_ERR_MASK          64    // Microcontroller clock mulfunction (not used)
/* Byte 1 */
#define RELAY_A_STUCK_HI_MASK        1    // Set Relay A Stuck High fault
#define RELAY_A_STUCK_LO_MASK        2    // Set Relay A Stuck Low fault
#define RELAY_B_STUCK_HI_MASK        4    // Set Relay B Stuck High fault
#define RELAY_B_STUCK_LO_MASK        8    // Set Relay B Stuck Low fault
#define RELAY_C_STUCK_HI_MASK       16    // Set Relay C Stuck High fault (not used)
#define RELAY_C_STUCK_LO_MASK       32    // Set Relay C Stuck Low fault (not used)
#define F_HS_DRIVER_FLT_MASK        64    // Set Front Seat Driver fault
#define R_HS_DRIVER_FLT_MASK       128    // Set Rear Sear Driver fault
/* Byte 2 */
#define PCB_SENSOR_HI_MASK           1    // PCB sensor reading high (set after 10 consecutive detection) (not used)
#define PCB_SENSOR_LO_MASK           2    // PCB sensor short to ground (set after 10 consecutive detection) (not used)
#define NTC_SUPPLY_LOW_MASK          4    // NTC supply voltage is low
#define PCB_OVER_TEMP_MASK           8    // PCB over temperature occurance (Not used)
#define WHEEL_LS_ALWAYS_ON_MASK     16    // StWheel low side is always on (not used)
#define WHEEL_LS_ALWAYS_OFF_MASK    32    // StWheel low side is always off (not used)
/* Byte 3 */
#define ERRATIC_LOCAL_VOLTAGE_MASK   1    // Erratic voltage occurance (DOES NOT SET A DTC)

/* Clear Internal Fault Defines */
/* Byte 0 */
#define CLR_OSC_ERR                254    // Clears Oscillator error
#define CLR_PLL_ERR                253    // Clears PLL error (PLL must be locked)
#define CLR_ROM_CHCKSUM_ERR        247    // Clears ROM checksum error
#define CLR_RAM_ERR                239    // Clears RAM error
#define CLR_EEPROM_ERR             223    // Clears EEPROM error         
/* Byte 0: Used Bit Mask 00001011 (b7...b0) (1 is used; 0 is not used) - OLD */
/* Byte 0: Used Bit Mask 00011011 (b7...b0) (1 is used; 0 is not used) - NEW */
#define BYTE0_USED_BITS            27   // original (ram error is used) - MODIFIED on 03.31.2009 by CK
//#define BYTE0_USED_BITS            11   // modified (no ram error)
/* Byte 1 */
#define CLR_RELAYA_STUCK_HI        254    // Clears Relay A Stuck High fault
#define CLR_RELAYA_STUCK_LO        253    // Clears Relay A Stuck Low fault
#define CLR_RELAYB_STUCK_HI        251    // Clears Relay B Stuck High fault
#define CLR_RELAYB_STUCK_LO        247    // Clears Relay B Stuck Low fault 
#define CLR_RELAYC_STUCK_HI        239    // Clears Relay C Stuck High fault (not used)
#define CLR_RELAYC_STUCK_LO        223    // Clears Relay C Stuck Low fault (not used)
#define CLR_F_DRIVER_FAULT		   191    // Clears Front FET (Front Driver) Fault 	
#define CLR_R_DRIVER_FAULT         127    // Clears Rear FET (Rear Driver) Fault		
///* Byte 1: Used Bit Mask 00101111 (b7...b0) (1 is used; 0 is not used) */
/* Byte 1: Used Bit Mask 11001111 (b7...b0) (1 is used; 0 is not used) */
//#define BYTE1_USED_BITS            47 // original
#define BYTE1_USED_BITS            207  // modified
/* Byte 2 */
#define CLR_PCB_SENSOR_HI          254    // Clears PCB Sensor HI fault (not used)
#define CLR_PCB_SENSOR_LO          253    // Clears PCB Sensor LO fault (not used)
#define CLR_NTC_SUPPLY_LOW         251    // Clears NTC supply voltage low occurance
#define CLR_PCB_OVER_TEMP          247    // Clears PCB Over Temperature occurance (Not used S2 onwards)
/* Byte 2: Used Bit Mask 00001111 (b7...b0) (1 is used; 0 is not used) */
/* Byte 2: Used Bit Mask 00000100 (b7...b0) (1 is used; 0 is not used) */
//#define BYTE2_USED_BITS         15 //PCB Over temp int fault removed for S2
//#define BYTE2_USED_BITS         15 //PCB STB and STG faults removed for S0B
#define BYTE2_USED_BITS            4
/* Byte 3 */
#define CLR_ERRATIC_VOLTAGE_RDG    254    // Clears Erratic local voltage reading occurance
/* Byte 3: Not Used Bit Mask 00000001 (b7...b0) (1 is used; 0 is not used) */
#define BYTE3_USED_BITS            1
/**********************************************************************************/


/****************************** @Function Prototypes ******************************/
/* For Main and ROM Test */
@far void IntFlt_F_Init(void);
@far void IntFlt_F_DTC_Monitor(void);
@far void IntFlt_F_Update(u_8Bit Byte_number, u_8Bit Bit_mask, Int_Flt_Op_Type_e intFlt_Op);
@far void IntFlt_F_Chck_PLL(void);
@far void IntFlt_F_Chck_Ram_Err(void);
@far void IntFlt_F_ClearOutputs(void);
@far void IntFlt_F_Clr_Ram_Err(void);
@far void IntFlt_F_Chck_Osci_Err(void);
/* For Mcu */
@far void IntFlt_F_Set_Osci_Err(void);    // Self Clock Mode (Oscillator error)
/**********************************************************************************/


#endif

