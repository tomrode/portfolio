/********************************************************************************/
/*                        A U T H O R   I D E N T I T Y							*/
/********************************************************************************/
/* Initials      Name                   Company									*/
/* --------      --------------------   --------------------------------------- */ 
/* CK            Can Kulduk             Continental Automotive Systems			*/
/* HK            Harsha Yekollu         Continental Automotive Systems          */
/* FU            Fransisco Uribe        Continental Automotive Systems          */
/* CC            Christian Cortes       Continental Automotive Systems          */
/********************************************************************************/


/********************************************************************************/
/*                   CHANGE HISTORY for VENTING MODULE                          */
/********************************************************************************/
/* 09.01.2006 CK  - module created												*/
/* 03.06.2008 HY  -	Module copied to CSWM MY11                                 	*/
/* 04.21.2008 HY  -	AD Channels adjusted accordingly to MY10                  	*/
/* 04.21.2008 HY  -	Thresholds Hardcoded at initialization function          	*/
/*                	After eep development these thresholds to be moved to eep	*/
/* 04.21.2008 HY  -	Added VENT_EN pin enable in output control function     	*/
/* 04.21.2008 HY  -	HVAC switch support Added                               	*/
/* 06.20.2008 FU  -	vsF_swtch_Mngmnt adapted for HVAC switch request			*/
/* 					vsF_swtch_Cntrl adapted for HVAC switch request				*/
/* 08.25.2006 HY  - Added EE_BlockRead/EE_BlockWrite calls                      */
/* 09.08.2008 HY  - For EE_BlockWrites copying data into local structure        */
/*                  Function vsF_Copy_Diag_Prms is not being used any more      */ 
/* 09.24.2008 CK  - Added disable vent circuitry logic (turn off                */ 
/* 					VENT_EN) output pin when all vent outputs are off.			*/
/* 09.29.2008 CK  - Modified vsF_swtch_Mngmnt to turn off vent outputs when     */
/*                  VS_SNA is received for D-series vehicle lines.              */
/* 10.27.2008 CK  - Updated vsF_Output_Cntrl and vsF_Clear functions to         */
/*                  insure that we call Pwm_SetDutyCycle function only when we  */
/*                  change the PWM. 											*/
/* 				  - Taken out flt_spl_open_c from local Vs structure. This      */
/*                  is not being used to set the vent open fault				*/
/* 10.29.2008 CK  - Propulsion system active code is added to vsF_swtch_Mngmnt  */
/* 10.30.2008 CK  - Added vsF_Get_Actv_outputNumber function which returns the  */
/*                  number of active vent outputs. This is used in battery      */
/*                  voltage compensation calculation.							*/
/* 10.31.2008 CK  - Update vsF_LED_Display function. Dbk fucntions are called   */
/* 					only when there is a change in LED status.					*/
/* 11.20.2008 CK  - Updated vsF_LED_Display function for PowerNet (LX-series).  */
/* 11.25.2008 CK  - PowerNet implementation to reduce load to VL1 automatically */
/*                  when load shed 2 condition is present. 						*/
/*                  Created vsLF_Chck_LdShd_Lvl_Reduction function. 		    */
/*                  Updated vsF_Manager function.								*/
/* 12.08.2008 CK  - Updated vsF_Get_Actv_outputNumber function. Battery voltage */
/*                  compensation is performed when a vent output is turned on at*/
/*                  low or high level. There is still considerable amount of    */
/*                  voltage drop at low vent level due to high frequency (25kHz)*/
/*                  of the PWM signal.                                          */
/* 01.23.2009 CK  - PC-Lint updates. No complaints from PC-Lint after fixes.    */
/* 02.10.2009 CK  - Added a simple function vsF_Get_Fault_Status to return      */
/*                  chosen vent fault status for DCX Routine Control output test*/
/* 02.12.2009 CK -  Chrysler Routine Control Service implemetation to run       */
/*                  output test. Updated vsF_Manager, vsF_Diag_IO_Cntrl,        */
/*                  vsF_swtch_Mngmnt, vsF_Diag_IO_Mngmnt and vsF_LED_Display    */
/*                  functions.         											*/
/*																				*/
/* 04.14.2009 CK - Updated vsLF_Chck_LdShd_Lvl_Reduction function to check for  */
/*                 Battery Reached Critical State signal before reducing to VL1.*/
/* 05.12.2009 HY - vsLF_Chck_mainState updated to keep outputs ON incase        */
/*                 outputs ON and Remote St active flag goes OFF                */
/*                 Change is commented the vsF_Clear function                   */
/* 09.18.2009 CC -   hsF_HeatManager(void).	MkS 35407 ENG RPM IGN is in RUN     */
/*                     and ENG RPM is not running customer requested to allow   */
/*                     switch requests to display 2sec LED 	     				*/
/* 11.25.2009 CC - vsF_Clear(), vsLF_Chck_mainState() and vsLF_RemSt_Check was  */
/*                 modified FTR00139669 - Autostart feature for PowerNet Series.*/
/* 09.21.2010 HY - MKS 56616: Once Remote start is over (Output is ON with 100%)*/
/*                 and IGN is in RUN, Output level to be reduced to LOW         */
/*                 vsLF_Chck_mainState updated to reduce the output to LOW PWM  */
/* 09.21.2010 HY - MKS 56616: In Autostart Vented seats start at LOW PWM        */
/*                 vsLF_Chck_mainState and vsLF_RemSt_Check updated to have LOW */
/********************************************************************************
 * 06-15-2011	Harsha	 75322	Cleanup the code. Delete the unused code for this program.
 * 								Deleted functions are vsLF_Chck_LdShd_Lvl_Reduction
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 ********************************************************************************/


/********************************************************************************/
/* MODULE DESCRIPTION                                                           */ 
/* The module is to vent the two seats by providing variable analog voltages    */
/* driving the external 12V. fans in the seats.									*/
/* Seat venting has 2 selectable speeds.										*/	
/* For seat venting, each press of the switch button will cause the seat to     */
/* change venting modes.     													*/
/* There is no timeouts for vent levels. 										*/
/********************************************************************************/


/********************************* @Include(s) **********************************/
#include "il_inc.h"
#include "v_inc.h"
#include "Venting.h" 
#include "main.h"          
#include "canio.h"
#include "DiagMain.h"
#include "DiagIOCtrl.h"
#include "DiagWrite.h"
#include "AD_Filter.h"
#include "Mcu.h"
#include "Dio.h"
#include "Pwm.h"
#include "FaultMemory.h"
#include "FaultMemoryLib.h"
#include "Relay.h"
#include "Heating.h" 
#include "mw_eem.h"
#include "Pwm_Cfg.h"
/********************************************************************************/


/********************************* external data *********************************/
/* N
 * O
 * N
 * E */
/*********************************************************************************/


/*********************************** @ variables *********************************/
u_8Bit vs_shrtTo_batt_mntr_c;     // monitors if there is a short to battery fault on either vent

/* Vs timings */
u_16Bit vs_LED_dsply_tm;          // LED display time for the chosen time slice
u_16Bit vs_flt_mtr_tm;            // LED display time for the chosen time slice

//u_8Bit vs_diag_prog_data_c[VS_DATA_LENGHT];    //Write Data by LID data array NOT USED

/* Vs Flags */
union char_bit vs_Flag1_c[VS_LMT];
union char_bit vs_Flag2_c[VS_LMT];

/* Fault Detection parameters */
u_8Bit  vs_Vsense_c[VS_LMT];          // Vent voltage reading (100mV unit)
u_8Bit  vs_Isense_c[VS_LMT];          // Vent current reading (100mA unit)
u_16Bit vs_Isense_offset_w[VS_LMT];   // Filtered Vsense offset (in AD unit)

u_8Bit  vs_slct;                      // Selects the Vent for DTC management
u_8Bit  vs_slctd_drive_c;             // Stores drive selection (left-hand drive or right-hand drive)
u_8Bit  vs_other_drive_c;             // Remote start other drive seat selection. Added newly for CR44
u_8Bit vs_lst_main_state_c;           // Previous main state (to turn off the vent when RemSt timeout)

/* These variables make sure to process LED status change when ever it is necessary. */
u_8Bit vs_lst_fl_led_stat_c;            // Previous LED state for FL
u_8Bit vs_lst_fr_led_stat_c;            // Previous LED state for FR
/*********************************************************************************/


/******************************** @Data Structures *******************************/
/* Declare Vs Calibration parameters */
struct Vs_cal_s Vs_cal_prms;
Vs_flt_cal_s Vs_flt_cal_prms;

/* Local Vs Structure */
typedef struct{
    u_8Bit crt_lv_c;         // vs current level
    u_8Bit stat_c;           // vs status (good, bad, ugly)          
    u_8Bit flt_stat_c;       // vs fault status (open, short etc.)
    u_8Bit Vsense_dly_cnt;   // Vsense delay counter (waits 160ms before starting shrt_to_batt detection)
    u_16Bit strt_up_cnt;     // vs motor start up counter
    u_16Bit flt_mtr_cnt;     // fault mature time
    u_16Bit LED_dsply_cnt;   // LED display time
    u_16Bit pwm_w;           // Pwm duty cycle of the Vs
    u_8Bit lst_swtch_rq_c;   // last switch request
    u_16Bit prev_pwm_w;		 // Previous pwm value (in order to call Pwm_SetDutyCycle once on Pwm change)
    //u_8Bit flt_spl_open_c;   // Open fault special case (10sec mature time.. Tracker Id#43)
}vs_prm_s;

/* Vs parameter sets */
vs_prm_s vs_prm_set[VS_LMT];

/* Declare Vs AD conversion parameter */
main_ADC_prms vs_AD_prms;

/* Local Vs Status structures (for LED Display) */
//#ifdef PWR_NET
//	tFL_HVS_STAT Local_L_F_VS;
//	tFR_HVS_STAT Local_R_F_VS;
//#endif
	
//#ifdef TIPM	
//	tL_F_HS   Local_L_F_VS;
//	tR_F_HS   Local_R_F_VS;
//#endif
/*********************************************************************************/


/*********************************************************************************/
/*                            vsF_Clear                                          */
/*********************************************************************************/
/* Clears the variables for a particular vent.                                   */
/*                                                                               */
/* This function is called if the ignition status is other then run/start to     */
/* insure that the vent stays off and all variables are cleared. 				 */
/* 									 											 */
/* Input parameter:                                                              */
/* vs_position: Selected vent position                                           */
/*                                                                               */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* IntFlt_F_ClearOutputs :                                                       */
/*    vsF_Clear(u_8Bit vs_pstn)                                                  */
/* vsF_Fnsh_LED_Dsply :                                                          */
/*    vsF_Clear(u_8Bit vs_pstn)                                                  */
/* vsF_LED_Display :                                                             */
/*    vsF_Clear(u_8Bit vs_pstn)                                                  */
/* vsF_Manager :                                                                 */
/*    vsF_Clear(u_8Bit vs_pstn)                                                  */
/* vsF_Rtrn_Cntrl_toECU :                                                        */
/*    vsF_Clear(u_8Bit vs_pstn)                                                  */
/* vsF_Updt_Crrt_Lvl :                                                           */
/*    vsF_Clear(u_8Bit vs_pstn)                                                  */
/* vsLF_Chck_mainState :                                                         */
/*    vsF_Clear(u_8Bit vs_pstn)                                                  */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* u_8Bit hsF_FrontHeatSeatStatus(unsigned char hs_nr)                           */
/* 																				 */
/*********************************************************************************/
@far void vsF_Clear(u_8Bit vs_position)
{
	/* Local variables */
	u_8Bit vs_pstn = vs_position;    // store vent position
	u_8Bit hs_stat = 0;              // stores the heated seat status (HS_ACTIVE or HS_NOT_ACTIVE)

	/* Read heated seat activation status */
	hs_stat = (u_8Bit) hsF_FrontHeatSeatStatus(vs_pstn);
	
	/* Modified 10.27.2008 */
	/* Check which node we are receiving the switch requests from */
	if(main_SwReq_Front_c == VEH_BUS_CCN_FRONT_REAR_K){
		
		/* CCN node: Check if we need to clear invalid switch flag */
		if ( (hs_stat == HS_ACTIVE) && (canio_vent_seat_request_c[vs_pstn] == VS_NOT_PSD) )
		{
			/* Clear invalid vent switch flag */
			vs_Flag2_c[vs_pstn].b.invld_swtch_rq_b = FALSE;
		}		
	}else{
		
		/* HVAC node: Check if we need to clear invalid switch flag */
		if ( (hs_stat == HS_ACTIVE) && (canio_hvac_vent_seat_request_c[vs_pstn] == VS_NOT_PSD) )
		{
			/* Clear invalid vent switch flag */
			vs_Flag2_c[vs_pstn].b.invld_swtch_rq_b = FALSE;
		}
	}
	
	/* Check if vent diagnostic is active */
	if ( (diag_io_vs_lock_flags_c[vs_pstn].b.switch_rq_lock_b == FALSE) && (diag_io_vs_lock_flags_c[vs_pstn].b.led_status_lock_b == FALSE) && (diag_io_all_vs_lock_b == FALSE) )
	{
		
		/* Clear diagnostics active flag */
		vs_Flag2_c[vs_pstn].b.diag_cntrl_actv_b = FALSE; 
	}
	else
	{
		/* Do not clear diagnostics active flag */
	}
	
	/* Variables that are cleared everytime this function is called */
	vs_prm_set[vs_pstn].crt_lv_c            = 0;        // vs current level is 0

	vs_prm_set[vs_pstn].LED_dsply_cnt       = 0;        // LED display time
	vs_prm_set[vs_pstn].pwm_w               = 0;        // 0 PWM
	vs_prm_set[vs_pstn].strt_up_cnt         = 0;        // clear vs start up counter

	/* 11.05.2008 Clear the state bit ONLY in the vsF_Output_Cntrl function */
	/* This is done to make sure we turn off the vent. */
	//vs_Flag1_c[vs_pstn].b.state_b           = FALSE;    // vs is off
	vs_Flag1_c[vs_pstn].b.sw_rq_b           = FALSE;    // vs software request is off
	vs_Flag1_c[vs_pstn].b.LED_off_b         = FALSE;    // clear LED off flag
	vs_Flag1_c[vs_pstn].b.start_up_cmplt_b  = FALSE;    // Start up complete falg is cleared

	vs_Flag2_c[vs_pstn].b.swtch_rq_prps_b   = FALSE;    // switch purpose is to turn off

	vs_Flag2_c[vs_pstn].b.diag_clr_ontime_b = FALSE;    // resets the ontime to zero
	
	/* Re-initilize all variables if ignition is other then run or start */
	if ( (canio_RX_IGN_STATUS_c != IGN_RUN) && (canio_RX_IGN_STATUS_c != IGN_START) )
	{
		vs_prm_set[vs_pstn].lst_swtch_rq_c      = 0;           // clear the last switch request
		vs_prm_set[vs_pstn].flt_mtr_cnt         = 0;           // clear vs fault mature time
		vs_prm_set[vs_pstn].Vsense_dly_cnt      = 0;           // clear vs Vsense delay time
		vs_prm_set[vs_pstn].flt_stat_c          = VS_NO_FLT_MASK; // no vs faults (open, short etc.)
		
		/* Added newly for S2 */
		//vs_prm_set[vs_pstn].flt_spl_open_c      = VS_NO_FLT_MASK; // no vs Open faults

		vs_prm_set[vs_pstn].stat_c              = GOOD;        // vs status is good

		vs_Flag1_c[vs_pstn].b.RemSt_auto_b      = FALSE;       // Clear remote start automatic activation bit
		vs_Flag2_c[vs_pstn].b.diag_cntrl_actv_b = FALSE;       // diagnostic IO control is inactive
		vs_Flag2_c[vs_pstn].b.invld_swtch_rq_b  = FALSE;       // clear invalid switch flag

		diag_io_vs_rq_c[vs_pstn]                = 0;           // clear diagnostic request  
		diag_io_vs_state_c[vs_pstn]             = 0;           // clear diagnostic LED state
		diag_io_vs_all_out_c                    = 0;           // clear diagnostic all vs request

		vs_Vsense_c[vs_pstn]                    = 0;           // Clear Vent voltage reading
		vs_Isense_c[vs_pstn]                    = 0;           // Clear Vent current reading

		vs_shrtTo_batt_mntr_c                   = 0;           // turn off the shrt_to_batt monitor

		vs_slctd_drive_c                        = VS_LMT;      // Select Both vents (No remote start)

		vs_lst_main_state_c                     = NORMAL;      // Last main state is defaulted to normal
		
		/* Disable vent circuitry. */
		Dio_WriteChannel(VENT_EN, STD_LOW);
	}
	else
	{
		/* These variables are cleared when ignition status is other than run/start */
	}
} //End of function

/*********************************************************************************/
/*                            vsF_Init                                           */
/*********************************************************************************/
/* This function is called after a power on reset and re-initializes the vent    */
/* calibration parameters. 														 */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* main :                                                                        */
/*    vsF_Init()                                                                 */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* EE_BlockRead(EEBlockLocation elocation, u_8Bit *pbydest)         			 */
/* 																				 */
/*********************************************************************************/
@far void vsF_Init(void)
{
	/* loop variable */
	u_8Bit a;    // for loop iteration
	
	/* Set Vent Status */
	for (a=0; a <VS_LMT; a++)
	{
		vs_prm_set[a].flt_stat_c = VS_NO_FLT_MASK;     // no vs faults
		vs_prm_set[a].stat_c     = GOOD;               // vs status is good
	}
	
	/* EEPROM */
	/* Copy all Vent Calibration data into local structure */
	/* This logic needs to be upgraded once PROXI setup made */
	EE_BlockRead(EE_VENT_PWM_DATA, (u_8Bit*)&Vs_cal_prms);
	
	
	EE_BlockRead(EE_VENT_FLT_CAL_DATA, (u_8Bit*)&Vs_flt_cal_prms);

	
	/* Vent Timings */
	vs_LED_dsply_tm = (MAIN_LED_DSPLY_TM * VS_TM_MDLTN); // vs shortTerm LED display (2 seconds in 10ms tmslc)
	vs_flt_mtr_tm =   Vs_flt_cal_prms.vs_2sec_mature_time; //vs fault mature time (2 seconds in 80ms tmslc)

	/* Init vs selection for DTC management (front right vent) */
	vs_slct = VS_FR;
	
	/* New: Manage both vents (no remote start) */
	vs_slctd_drive_c = VS_LMT;
	
	/* Initiliaze last main state as normal */
	vs_lst_main_state_c = NORMAL;
}

/*********************************************************************************/
/*                           vsF_AD_Cnvrsn                                       */
/*********************************************************************************/
/* After selecting the AD channel and mux channel this function is called to     */
/* perform the AtoD conversion and filter the result for the selected vent       */
/* feedback (Isense or Vsense). 												 */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* vsF_Isense_AD_Rdg :                                                           */
/*    vsF_AD_Cnvrsn()                                                            */
/* vsF_Vsense_AD_Rdg :                                                           */
/*    vsF_AD_Cnvrsn()                                                            */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* ADF_AtoD_Cnvrsn(u_8Bit AD_Filter_supply)                                      */
/* ADF_Str_UnFlt_Mux_Rslt(u_8Bit AD_Filter_feed,u_8Bit AD_Filter_mux_feed)       */
/* ADF_Mux_Filter(u_8Bit AD_Filter_slctd_mux,u_8Bit AD_Filter_mux_ch)            */
/* 																				 */
/*********************************************************************************/
@far void vsF_AD_Cnvrsn(void)
{
	/* AD conversion for muxed feedback */
	ADF_AtoD_Cnvrsn(vs_AD_prms.slctd_adCH_c);
	
	/* Conversion is completed. Store the unfiltered mux result */
	ADF_Str_UnFlt_Mux_Rslt(vs_AD_prms.slctd_adCH_c, vs_AD_prms.slctd_muxCH_c);
	
	/* Filter the result */
	ADF_Mux_Filter(vs_AD_prms.slctd_mux_c, vs_AD_prms.slctd_muxCH_c);
}

/*********************************************************************************/
/*                          vsF_Vsense_AD_Rdg                                    */
/*********************************************************************************/
/* Input parameter:                                               				 */
/* vs_postn: Selected vent position                               				 */
/*                                                                				 */
/* This function reads Vsense feedback for hof the selected vent: 				 */
/* 1. Selecting the mux channel                                   				 */
/* 2. Performing AtoD conversion                                  				 */
/* 3. Converting AD reading to 100mV units                        				 */
/*                                                                				 */
/* Calls from                                                    				 */
/* --------------                                                 				 */
/* vsF_ADC_Monitor :                                              				 */
/*    vsF_Vsense_AD_Rdg(u_8Bit vs_pstn)                           				 */
/*                                                                				 */
/* Calls to                                                       				 */
/* --------                                                       				 */
/* u_8Bit ADF_Nrmlz_Vs_Vsense(u_16Bit AD_vs_Vsense_value)         				 */
/* Dio_WriteChannelGroup( ChannelGroupIdPtr, Level)              				 */
/* vsF_AD_Cnvrsn()                                                			 	 */
/* 																				 */
/*********************************************************************************/
@far void vsF_Vsense_AD_Rdg(u_8Bit vs_postn)
{
	/* Local variable(s) */
	u_8Bit vs_pos = vs_postn;    // store vent position

	/* Select Mux Channel for Vsense Reading */
	if (vs_pos == VS_FL) {		
		/* FL Vense */
		vs_AD_prms.slctd_muxCH_c = CH4;
	}
	else {
		/* FR Vsense */
		vs_AD_prms.slctd_muxCH_c = CH5;
	}
	
	Dio_WriteChannelGroup(MuxSelGroup, vs_AD_prms.slctd_muxCH_c);
	
	/* Vsense AD conversion */
	vsF_AD_Cnvrsn();
	
	/* Store the filtered Vsense */
	vs_Vsense_c[vs_pos] = ADF_Nrmlz_Vs_Vsense(AD_mux_Filter[vs_AD_prms.slctd_mux_c][vs_AD_prms.slctd_muxCH_c].flt_rslt_w);
}

/*********************************************************************************/
/*                          vsF_Isense_AD_Rdg                                    */
/*********************************************************************************/
/* Input parameter:                                                              */
/* vs_pstion: selected vent position                                             */
/* Vs_Isense_Rdg_type: Stores the Isense reading as ON_ISENSE or OFF_ISENSE.     */
/*                                                                               */
/* This function reads Isense feedback for the selected vent by:                 */
/* 1. Selecting the Vent Isense mux channel                                      */
/* 2. Performing AtoD conversion                                                 */
/* 3. Converting AD reading to 100mA units                                       */
/*                                                                               */
/* Note: When the vent is turned off, there is still significant Isense reading  */
/* due to our Hardware design. This reading is named "vs Isense offset" in the   */
/* software.                                             						 */
/*                                                                               */
/* This function stores the AD reading as the vs Isense offset when the vent is  */
/* turned off. When the vent is activated, final AD reading is calculated by     */
/* subtracting the Isense offset from the present reading. 						 */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* vsF_ADC_Monitor :                                                             */
/*    vsF_Isense_AD_Rdg(u_8Bit vs_pstn,Vs_Isense_type_e Vs_Isense_Rdg_type)      */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* u_8Bit ADF_Nrmlz_Vs_Isense(u_16Bit AD_vs_Isense_value)                        */
/* Dio_WriteChannelGroup( ChannelGroupIdPtr, Level)                              */
/* vsF_AD_Cnvrsn()                                                               */
/* 																			     */
/*********************************************************************************/
@far void vsF_Isense_AD_Rdg(u_8Bit vs_pstion, Vs_Isense_type_e Vs_Isense_Rdg_type)
{
	/* Local variable(s) */
	u_8Bit  vs_ps = vs_pstion;       // store vent position
	u_16Bit vs_interim_Isense_w = 0; // Intermediate result for the On Isense
	
	/* Select Mux Channel for Isense Reading */
	if (vs_ps == VS_FL) {
		/* FL Isense (mux channel 6) */
		vs_AD_prms.slctd_muxCH_c = CH6;
	}
	else {
		/* FR Isense (mux channel 7) */
		vs_AD_prms.slctd_muxCH_c = CH7;
	}
	
	Dio_WriteChannelGroup(MuxSelGroup, vs_AD_prms.slctd_muxCH_c);
	
	/* Perform Isense AD conversion */
	vsF_AD_Cnvrsn();
	
	/* Check Isense read type */
	if (Vs_Isense_Rdg_type == VS_ON_ISENSE) {
		/* Store the intermediate result for the ON Isense (in 10-bit AD unit) */
		vs_interim_Isense_w = AD_mux_Filter[vs_AD_prms.slctd_mux_c][vs_AD_prms.slctd_muxCH_c].flt_rslt_w;
		
		/* Is Intermediate result bigger than Isense offset? */
		if (vs_interim_Isense_w > vs_Isense_offset_w[vs_ps]) {
			
			/* Update the intermediate Isense result by subtracting the offset from it */
			vs_interim_Isense_w -= vs_Isense_offset_w[vs_ps];
			
			/* Convert 10-bit AD result to 8-bit result in 100mA units */
			vs_Isense_c[vs_ps] = ADF_Nrmlz_Vs_Isense(vs_interim_Isense_w);
		}
		else {
			/* Set Isense to zero */
			vs_Isense_c[vs_ps] = 0;
		}
	}
	else {
		/* Store present vent Isense offset (in 10-bit AD unit) */
		vs_Isense_offset_w[vs_ps] = AD_mux_Filter[vs_AD_prms.slctd_mux_c][vs_AD_prms.slctd_muxCH_c].flt_rslt_w;
	}
}

/*********************************************************************************/
/*                          vsF_ADC_Monitor                                      */
/*********************************************************************************/
/* This function manages both vents AD conversions.                      		 */
/*                                                                       		 */
/* It always performs Vsense reading.                                    		 */
/* When a vent is disabled, it performs Isense offset readings.          		 */
/* If the vent is enabled, it performs the Isense (on Isense)readings.   		 */
/*                                                                       		 */
/* Note: This function is called cyclically every 2.5ms.                 		 */
/*                                                                       		 */
/* Calls from                                                            		 */
/* --------------                                                        		 */
/* main :                                                                		 */
/*    vsF_ADC_Monitor()                                                  	     */
/*                                                                       		 */
/* Calls to                                                             		 */
/* --------                                                              		 */
/* vsF_Vsense_AD_Rdg(u_8Bit vs_pstn)                                     		 */
/* vsF_Isense_AD_Rdg(u_8Bit vs_pstn,Vs_Isense_type_e Vs_Isense_Rdg_type) 		 */
/* 																				 */
/*********************************************************************************/
@far void vsF_ADC_Monitor(void)
{
	/* Local Variable(s) */
	u_8Bit dd;                            // vs index
	
	/* SetUp for AD reading */
	vs_AD_prms.slctd_adCH_c = CH5;        // set selected AD channel to CH5
	vs_AD_prms.slctd_mux_c  = MUX2;       // set selected mux to MUX2
	
	/* AD conversions for all vents */
	for (dd=0; dd<VS_LMT; dd++) {
		/* Perform AD conversion for Vsense */
		vsF_Vsense_AD_Rdg(dd);
		
		/* Check vent sw request */
		if ( (vs_Flag1_c[dd].b.sw_rq_b == TRUE) && (relay_A_Ok_b == TRUE) ) {
			
			/* AD conversion for Isense (Vent On) */
			vsF_Isense_AD_Rdg(dd, VS_ON_ISENSE);
		}
		else {
			/* Compute present vent Isense offset (Vent off) */
			vsF_Isense_AD_Rdg(dd, VS_OFFSET_ISENSE);
			
			/* Clear vent on Isense */
			vs_Isense_c[dd] = 0;
		}
	}
}

/*********************************************************************************/
/*                          vsF_Updt_Status                                      */
/*********************************************************************************/
/* VentsF_Status                                                                 */
/* +------------------++----+----+--------+----+-----------+                     */
/* |                  || 1  | 2  |   3    | 4  |     5     |                     */
/* +==================++====+====+========+====+===========+                     */
/* |relay_A_int_flt   ||TRUE| -  |   -    | -  |   FALSE   |                     */
/* +------------------++----+----+--------+----+-----------+                     */
/* |vs_batt_flt       || -  |TRUE|   -    | -  |   FALSE   |                     */
/* +------------------++----+----+--------+----+-----------+                     */
/* |vs_flt_status_c   || -  | -  |MTRD_FLT| -  |NO_MTRD_FLT|                     */
/* +------------------++----+----+--------+----+-----------+                     */
/* |hs_batt_flt       || -  | -  |   -    |TRUE|   FALSE   |                     */
/* +==================++====+====+========+====+===========+                     */
/* |vent_stat_c       ||    |    |        |    |           |                     */
/* |=                 || X  | X  |   X    | X  |           |                     */
/* |UGLY              ||    |    |        |    |           |                     */
/* +------------------++----+----+--------+----+-----------+                     */
/* |vent_stat_c       ||    |    |        |    |           |                     */
/* |=                 ||    |    |        |    |     X     |                     */
/* |main_CSWM_status_c||    |    |        |    |           |                     */
/* +------------------++----+----+--------+----+-----------+                     */
/*                                                                               */
/* SD Text                                                                       */
/* -------                                                                       */
/* Input parameter:                                                              */
/* vs_location: Selected vent location                                           */
/*                                                                               */
/* Information used to determine vent status are:                                */
/* 1. Selected vent fault status                                                 */
/* 2. Relay A internal fault status                                              */
/* 3. Short to battery fault on any of the front heaters and vents               */
/*                                                                               */
/* If above conditions are o.k., main CSWM status is taken as the selected vent  */
/* status.                                                                       */
/* Otherwise, vent status is set to "UGLY". The selected vent can not be turned  */
/* on. Only short-term LED display.                                              */
/*                                                                               */
/* This function also "kills" the LED display (by setting LED_off_b) as soon as  */
/* vent status is set to "UGLY" for the first time (ex: setting a vent short to  */
/* battery fault). 																 */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* vsF_Manager :                                                                 */
/*    vsF_Updt_Status(u_8Bit vs_pstn)                                            */
/*                                                                               */
/*********************************************************************************/
@far void vsF_Updt_Status(u_8Bit vs_location)
{
	/* Local variable(s) */
	u_8Bit vs_p = vs_location;   // store vent position
	u_8Bit vs_prv_stat_c;        // stores the previous vent status
	
	/* Store the previous vent status */
	vs_prv_stat_c = vs_prm_set[vs_p].stat_c;
	
	/* Update vs status                                            */
	/* Check to make sure we do not have a matured vent fault or relay fault */
	if ( (relay_A_internal_Fault_b == FALSE) && (vs_shrtTo_batt_mntr_c != VS_SHRT_TO_BATT) && (vs_prm_set[vs_p].flt_stat_c <= VS_PRL_FLT_MAX) && (hs_status_flags_c[FRONT_LEFT].b.hs_final_short_bat_b == FALSE) && (hs_status_flags_c[FRONT_RIGHT].b.hs_final_short_bat_b == FALSE) ) {
		
		/* vs status is equal to main CSWM status */
		vs_prm_set[vs_p].stat_c = main_CSWM_Status_c;
	}
	else {
		/* We have a matured fault. Set Vs status is UGLY */
		vs_prm_set[vs_p].stat_c = UGLY;
	}
	/* Check if we have to turn off the LED Display */
	if ( (vs_prv_stat_c != UGLY) && (vs_prm_set[vs_p].stat_c == UGLY)) {
		/* Set the bit that turns off the LED display - we come here only once when a fault is matured */
		vs_Flag1_c[vs_p].b.LED_off_b = TRUE;
	}
}

/*********************************************************************************/
/*                          vsF_Nrmlz_PWM                                        */
/*********************************************************************************/
/* Input paramter:                                                               */
/* vs_spot: selected vent position (FL or FR)                                    */
/*                                                                               */
/* Updates the vent pwm parameter for the present vent level.                    */
/* This value is passed as a parameter to the autosar pwm driver function        */
/* Pwm_SetDutyCycle. 															 */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* vsF_Manager :                                                                 */
/*    vsF_Nrmlz_PWM(u_8Bit vs_pstn)                                              */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* u_16Bit mainF_Calc_PWM(u_8Bit main_duty_cycle_c)                              */
/* 																				 */
/*********************************************************************************/
@far void vsF_Nrmlz_PWM(u_8Bit vs_spot)
{
	/* Local variables */
	u_8Bit vs_spt = vs_spot;    // store vent spot
	u_8Bit vs_pwm_lvl_c;        // local variable to store the vent level
	u_8Bit vs_duty_cycle_c = 0; // local variable to store the vent pwm duty cycle
	
	/* Store current vent level */
	vs_pwm_lvl_c = vs_prm_set[vs_spt].crt_lv_c;
	
	/* Check the duty cycle for vent level */
	if (vs_pwm_lvl_c == VL_LOW) {
		/* Low level duty cycle */
		vs_duty_cycle_c = Vs_cal_prms.cals[hs_vehicle_line_c].PWM_a[1];
	}
	else {
		if (vs_pwm_lvl_c == VL_HI) {
			/* High level duty cycle */
			vs_duty_cycle_c = Vs_cal_prms.cals[hs_vehicle_line_c].PWM_a[2];
		}
		else {
			/* Off Duty cycle */
			vs_duty_cycle_c = 0;
		}
	}	
	/* Keep the same PWM */
	vs_prm_set[vs_spt].pwm_w = mainF_Calc_PWM(vs_duty_cycle_c);
}

/*********************************************************************************/
/*                          vsF_Updt_Crrt_Lvl                                    */
/*********************************************************************************/
/* -------                                                                       */
/* After a valid switch/IO control request, this function puts the vent in the   */
/* next vent level. 															 */
/*                                                                               */
/* VentsF_Update_Current_Level                                                   */
/* +---------------------++-----+-----+-----+----+                               */
/* |                     ||  1  |  2  |  3  | 4  |                               */
/* +=====================++=====+=====+=====+====+                               */
/* |vent_GoTo_HighLevel_b||FALSE|FALSE|FALSE|TRUE|                               */
/* +---------------------++-----+-----+-----+----+                               */
/* |vent_current_level_c || VL0 | VL3 | VL1 | -  |                               */
/* +=====================++=====+=====+=====+====+                               */
/* |vent_current_level_c ||     |     |     |    |                               */
/* |=                    ||  X  |     |     | X  |                               */
/* |VL3                  ||     |     |     |    |                               */
/* +---------------------++-----+-----+-----+----+                               */
/* |vent_current_level_c ||     |     |     |    |                               */
/* |=                    ||     |  X  |     |    |                               */
/* |VL1                  ||     |     |     |    |                               */
/* +---------------------++-----+-----+-----+----+                               */
/* |vent_current_level_c ||     |     |     |    |                               */
/* |=                    ||     |     |  X  |    |                               */
/* |VL0                  ||     |     |     |    |                               */
/* +---------------------++-----+-----+-----+----+                               */
/*                                                                               */
/* SD Text                                                                       */
/* -------                                                                       */
/* Performs the vent level update for a switch request (without timeout).        */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* vsF_swtch_Cntrl :                                                             */
/*    vsF_Updt_Crrt_Lvl(u_8Bit vs_pstn)                                          */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* vsF_Clear(u_8Bit vs_pstn)                                                     */
/*																				 */
/*********************************************************************************/
@far void vsF_Updt_Crrt_Lvl(u_8Bit vs_spot_c)
{
	/* Local variable(s) */
	u_8Bit vs_st = vs_spot_c;    // store vent position
	
	/* Change the current Vs level */
	switch (vs_prm_set[vs_st].crt_lv_c) {

		case VL0:
		{
			/* Going from VL0 to VL_HI */
			vs_prm_set[vs_st].crt_lv_c = VL_HI;
			break;
		}

		case VL_HI:
		{
			/* Going from VL_HI to VL_LOW */
			vs_prm_set[vs_st].crt_lv_c = VL_LOW;
			break;
		}
		default :
		{
			/* Clear Vs */
			vsF_Clear(vs_st);
		}
	}
}

/*********************************************************************************/
/*                          vsF_Get_Output_Stat                                  */
/*********************************************************************************/
/* Input parameter:                                                    			 */
/* vs_site: Selected vent position                                     			 */
/*                                                                     			 */
/* This function returns the present vent level VL0, VL1, VL2, or VL3. 			 */
/*                                                                     			 */
/* Calls from                                                          			 */
/* --------------                                                      			 */
/* ApplDescProcessReadDataByIdentifier :                               			 */
/*    u_8Bit vsF_Get_Output_Stat(u_8Bit vs_pstn)                       			 */
/*                                                                     			 */
/* Calls to                                                            			 */
/* --------                                                            			 */
/* no calls                                                            			 */
/* 																				 */
/*********************************************************************************/
@far u_8Bit vsF_Get_Output_Stat(u_8Bit vs_site)
{
	/* Local variable(s) */
	u_8Bit vs_ste = vs_site;        // store vent position
	
	/* Returns current vent level (VL0, VL_LOW or VL_HI) */
	return(vs_prm_set[vs_ste].crt_lv_c);
}

/*********************************************************************************/
/*                          vsF_Motor_StrtUp                                     */
/*********************************************************************************/
/* Input parameter:                                                              */
/* vs_motor_pos: Selected vent position                                          */
/*                                                                               */
/* In order to insure vent motor startup during cold temperatures (and other     */
/* conditions), the selected vent is turned on at 100% for a defined period of   */
/* time (calibration parameter, default 2 seconds) when it is goes from VL0 to   */
/* any  other level. 															 */
/*                                                                               */
/* Once the motor start-up is completed, the vent is placed at the appropriate   */
/* pwm for the present level.                                                    */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* vsF_Output_Cntrl :                                                            */
/*    vsF_Motor_StrtUp(u_8Bit vs_motor_pos)                                      */
/* 																				 */
/*********************************************************************************/
@far void vsF_Motor_StrtUp(u_8Bit vs_motor_pos)
{
	/* Local variable(s) */
	u_8Bit vs_motor_loc = vs_motor_pos;      // motor activation location
	
	/* Check if motor start up is complete */
	if (vs_Flag1_c[vs_motor_loc].b.start_up_cmplt_b == FALSE) {
		
		/* Check start up time expiration */
		if (vs_prm_set[vs_motor_loc].strt_up_cnt < Vs_flt_cal_prms.start_up_tm) {
			
			/* Increment vent motor start up conter */
			vs_prm_set[vs_motor_loc].strt_up_cnt++;
			
			/* Turn on the vent output at 100% */
			vs_prm_set[vs_motor_loc].pwm_w = MAIN_MAX_PWM;
		}
		else {
			/* Motor start up is completed */
			vs_Flag1_c[vs_motor_loc].b.start_up_cmplt_b = TRUE;
		}
	}
	else {
		/* Clear motor start up counter */
		vs_prm_set[vs_motor_loc].strt_up_cnt = 0; 
		
		/* Set Vs State as turned On */
		vs_Flag1_c[vs_motor_loc].b.state_b = TRUE;
	}
}

/*********************************************************************************/
/*                          vsF_Diag_IO_Mngmnt                                   */
/*********************************************************************************/
/* Input parameter:                                                              */
/* vs_site_c: Selected vent (FL or FR)                                           */
/*                                                                               */
/* This function determines the diagnostic purpose of incoming signal.           */
/* After receiving a diagnostic request, it sets the vent switch request purpose */
/* (vs_swtch_rq_prps_b) as VNT_ON or VNT_OFF.                                    */
/*                                                                               */
/* Note: Switch purpose is an important information. All the automatic recoveries*/
/* (load shed 2 to 0, preliminary faults, etc) is done using this information    */
/* (see vsF_Manager). 															 */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* vsF_swtch_Mngmnt :                                                            */
/*    vsF_Diag_IO_Mngmnt(u_8Bit vs_pstn)                                         */
/*                                                                               */
/*********************************************************************************/
@far void vsF_Diag_IO_Mngmnt(u_8Bit vs_site_c)
{
	/* Local variable(s) */
	u_8Bit vs_po = vs_site_c;        // store vent position
	
	/* Check if DCX Routine Control Output Test flag is set and vent test is running */
	if(diag_rc_all_op_lock_b == TRUE){
		/* Check we are running the test for the vents */
		if(diag_rc_all_vents_lock_b == TRUE){			
			/* Vs switch purpose is to turn ON */
			vs_Flag2_c[vs_po].b.swtch_rq_prps_b = TRUE;
			
		}else{
			/* Vs switch purpose is to turn off */
			vs_Flag2_c[vs_po].b.swtch_rq_prps_b = FALSE;
		}		
	}else{
		
		/* Check if diagnostic LED control is inactive */
		if (diag_io_vs_lock_flags_c[vs_po].b.led_status_lock_b == FALSE) {
			
			/* Check diagnostic IO request */
			if ( (diag_io_vs_rq_c[vs_po] == VL_LOW) || (diag_io_vs_rq_c[vs_po] == VL_HI) ) {
				
				/* Vs switch purpose is to turn ON */
				vs_Flag2_c[vs_po].b.swtch_rq_prps_b = TRUE;
			}
			else {
				/* Invalid request. Vs switch purpose is to turn off */
				vs_Flag2_c[vs_po].b.swtch_rq_prps_b = FALSE;
			}
		}
		else {
			/* LED lock is active. */
		}		
	}
}

/*********************************************************************************/
/*                          vsLF_RemSt_Check                                     */
/*********************************************************************************/
/* This function checks whether automatic vent activation is necassary during    */
/* remote start event or not.                     								 */
/*                                                                               */
/* If it is necassary, it updates vent variables in order to perform the         */
/* automatic activation (once per remote start event). 							 */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* vsF_Manager :                                                                 */
/*    vsLF_RemSt_Check(u_8Bit vs_start_side)                                     */
/* 																				 */
/*********************************************************************************/
@far void vsLF_RemSt_Check(u_8Bit vs_start_side)
{
	/* Local variable(s) */
	u_8Bit vs_loc = vs_start_side;        // Vent side
	
	/* Check if remote start automatic vent activation is needed */
	/* FTR00139669 -- Autostart feature for PowerNet Series.*/
	if ((main_state_c == REM_STRT_VENTING) || (main_state_c == AUTO_STRT_VENTING)) {
		
		/* Did automatic activation take place yet? */
		if (vs_Flag1_c[vs_loc].b.RemSt_auto_b == TRUE) {
			/* Automatic activation already started */
		}
		else {
			
			if (main_state_c == REM_STRT_VENTING)
			{
				/* Set present vent level to VL_HI */
				vs_prm_set[vs_loc].crt_lv_c = VL_HI;
			}
			else
			{
				//Do the auto start with LOW level only incase If outputs are not activated.
				//Reduce the level only in cae If outputs are not activated.
				if (vs_Flag1_c[vs_loc].b.sw_rq_b == FALSE)
				{
					/* MKS 56616: Set present vent level to VL_LOW */
					vs_prm_set[vs_loc].crt_lv_c = VL_LOW;
				}
			}
			
			
			/* Vs switch purpose is to turn ON */
			vs_Flag2_c[vs_loc].b.swtch_rq_prps_b = TRUE;
			
			/* Set the automatic activation bit */
			vs_Flag1_c[vs_loc].b.RemSt_auto_b = TRUE;
			
			/* Decide to serve or reject the automatic activation */
			if (vs_prm_set[vs_loc].stat_c == GOOD) {
				/* Turn on the vs software request */
				vs_Flag1_c[vs_loc].b.sw_rq_b = TRUE;
			}
			else {
				/* Turn off the vs software request */
				vs_Flag1_c[vs_loc].b.sw_rq_b = FALSE;
			}
		}
	}
	else {
		/* Do not perform automatic activation. */
	}
}

/*********************************************************************************/
/*                          vsF_swtch_Mngmnt                                     */
/*********************************************************************************/
/* Input parameter:                                                              */
/* vs_point: Selected vent (FL or FR)                                            */
/*                                                                               */
/* This function determines the switch request purpose of incoming signal.       */
/* After receiving the switch request, it sets the vent switch request purpose   */
/* (vs_swtch_rq_prps_b) as VNT_ON or VNT_OFF.                                    */
/*                                                                               */
/* Note: Switch purpose is an important information. All the automatic recoveries*/
/* (load shed 2 to 0, preliminary faults, etc) is done using this information    */
/* (see vsF_Manager). 															 */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* vsF_Manager :                                                                 */
/*    vsF_swtch_Mngmnt(u_8Bit vs_pstn)                                           */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* vsF_Diag_IO_Mngmnt(u_8Bit vs_pstn)                                            */
/* 																				 */
/*********************************************************************************/
@far void vsF_swtch_Mngmnt(u_8Bit vs_point)
{
	/* *** CAUTION: switch management has to be called before updating the current vent level *** */
	u_8Bit vs_l = vs_point;     // store vent location
	u_8Bit vs_rq = 0;           // current vs request
	
	/* Check if heated seat Diagnostic IO control is inactive. Also, check if DCX Routine Control service is inactive. */
	if ( (diag_io_hs_lock_flags_c[vs_l].b.switch_rq_lock_b == FALSE) && (diag_io_hs_lock_flags_c[vs_l].b.led_status_lock_b == FALSE) && (diag_io_all_hs_lock_b == FALSE) && (diag_rc_all_op_lock_b == FALSE) )
	{
		/* Check if diagnostics IO control is inactive */
		if ( (diag_io_vs_lock_flags_c[vs_l].b.switch_rq_lock_b == FALSE) && (diag_io_vs_lock_flags_c[vs_l].b.led_status_lock_b == FALSE) && (diag_io_all_vs_lock_b == FALSE) && 
			 (diag_rc_all_op_lock_b == FALSE)	)
		{			
			/* Get switch request */
			/* Check vs position */
			if (vs_l == VS_FL)
			{
				if (main_SwReq_Front_c == VEH_BUS_CCN_FRONT_REAR_K)
				{
				/* Store the LF vs request from CCN */
					vs_rq = canio_L_F_VS_RQ_c;
				}
				else
				{
					/* Store the LF vs request from HVAC */
					vs_rq = canio_LF_VS_RQ_c;
				}
			}
			else
			{
				if (main_SwReq_Front_c == VEH_BUS_CCN_FRONT_REAR_K)
				{
					/* Store the RF vs request from CCN */
					vs_rq = canio_R_F_VS_RQ_c;
				}
				else
				{
					/* Store the RF vs request from HVAC */
					vs_rq = canio_RF_VS_RQ_c;	
				}
			}
			
			/* **************/
			/* CCN Logic	*/
			/* **************/
			/* Determine Switch purpose */
			/* Check for valid VS request */
			if (main_SwReq_Front_c == VEH_BUS_CCN_FRONT_REAR_K)
			{
				if ( (vs_rq == VS_PSD) || (vs_rq == VS_NOT_PSD) )
				{
					/* Check for a valid switch request */
					if ( (vs_rq == VS_PSD) && (vs_prm_set[vs_l].lst_swtch_rq_c == VS_NOT_PSD) )
					{
						/* Determine whether switch purpose is to turn on */
						if (vs_Flag2_c[vs_l].b.swtch_rq_prps_b == FALSE)
						{
							/* Vs switch purpose is to turn ON */
							vs_Flag2_c[vs_l].b.swtch_rq_prps_b = TRUE;
						}
						else
						{
							/* Check if current Vs level is VL_LOW */
							if (vs_prm_set[vs_l].crt_lv_c != VL_LOW)
							{
								/* Vs switch purpose is to turn ON */
								vs_Flag2_c[vs_l].b.swtch_rq_prps_b = TRUE;
							}
							else
							{
								/* Vs switch purpose is to turn off */
								vs_Flag2_c[vs_l].b.swtch_rq_prps_b = FALSE;
							}
						}
						/* Clear LED display count */
						vs_prm_set[vs_l].LED_dsply_cnt = 0;
					}
					else
					{
						/* Not a valid request. Keep the switch purpose unchanged. */
					}
				}
				else
				{
					/* Check for VS_OFF or VS_SNA signal */
					if ( (vs_rq == VS_OFF) || (vs_rq == VS_SNA) )
					{
						/* Vs switch purpose is to turn off */
						vs_Flag2_c[vs_l].b.swtch_rq_prps_b = FALSE;
					}
					else
					{
						/* Not a valid signal. Do not act on it. */
					}
				}
			}
			/* **************/
			/* CCN Logic End*/
			/* **************/
			
			/* **************/
			/* HVAC Logic 	*/
			/* **************/
			else
			{
				if ( (vs_rq == VS_HVAC_PSD) || (vs_rq == VS_HVAC_NOT_PSD) )
				{
					/* Check for a valid switch request */
					if ( (vs_rq == VS_HVAC_PSD) && (vs_prm_set[vs_l].lst_swtch_rq_c == VS_HVAC_NOT_PSD) )
					{
						/* Determine whether switch purpose is to turn on */
						if (vs_Flag2_c[vs_l].b.swtch_rq_prps_b == FALSE)
						{
							/* Vs switch purpose is to turn ON */
							vs_Flag2_c[vs_l].b.swtch_rq_prps_b = TRUE;
						}
						else
						{
							/* Check if current Vs level is VL_LOW */
							if (vs_prm_set[vs_l].crt_lv_c != VL_LOW)
							{
								/* Vs switch purpose is to turn ON */
								vs_Flag2_c[vs_l].b.swtch_rq_prps_b = TRUE;
							}
							else
							{
								/* Vs switch purpose is to turn off */
								vs_Flag2_c[vs_l].b.swtch_rq_prps_b = FALSE;
							}
						}
						/* Clear LED display count */
						vs_prm_set[vs_l].LED_dsply_cnt = 0;
					}
					else
					{
						/* Not a valid request. Keep the switch purpose unchanged. */
					}
				}
				else
				{
					/* Check for VS_HVAC_SNA signal */
					if (vs_rq == VS_HVAC_SNA)
					{
						/* Vs switch purpose is to turn off */
						vs_Flag2_c[vs_l].b.swtch_rq_prps_b = FALSE;
					}
					else
					{
						/* Not a valid signal. Do not act on it. */
					}
				}
			}
			/* ***************/
			/* HVAC Logic End*/
			/* ***************/
			
			/* Store the last switch request */
			vs_prm_set[vs_l].lst_swtch_rq_c = vs_rq;
		}
		else
		{
			/* Diagnostic IO management */
			vsF_Diag_IO_Mngmnt(vs_l);
		}
	}
	else
	{
		/* Check if DCX Routine Control is active */
		if(diag_rc_all_op_lock_b == TRUE){
			/* Diagnostic IO management */
			vsF_Diag_IO_Mngmnt(vs_l);			
		}else{
			/* DCX Routine Control is not active. */
			/* Diagnostic IO control for heaters is active: Do not accept vent switch requests */
		}
		
	}

} //End Function

/*********************************************************************************/
/*                          vsF_Output_Cntrl                                     */
/*********************************************************************************/
/* This function is responsible to turn on/off the front vent outputs.           */
/*                                                                               */
/* VentsF_PortHandler                                                            */
/* +------------++----+-----+-----+                                              */
/* |            || 1  |  2  |  3  |                                              */
/* +============++====+=====+=====+                                              */
/* |vs_sw_rq_b  ||TRUE|FALSE|  -  |                                              */
/* +------------++----+-----+-----+                                              */
/* |relay_A_Ok_b||TRUE|  -  |FALSE|                                              */
/* +============++====+=====+=====+                                              */
/* |vs_state_b  ||    |     |     |                                              */
/* |=           || X  |     |     |                                              */
/* |TRUE        ||    |     |     |                                              */
/* +------------++----+-----+-----+                                              */
/* |vs_state_b  ||    |     |     |                                              */
/* |=           ||    |  X  |  X  |                                              */
/* |FALSE       ||    |     |     |                                              */
/* +------------++----+-----+-----+                                              */
/*                                                                               */
/* SD Text                                                                       */
/* -------                                                                       */
/* Turn on the selected vent output by pwm'ing it if relay A is OK and there is a*/
/* softare request (vs_sw_rq_b is set. See vsF_Manager). 						 */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* main :                                                                        */
/*    vsF_Output_Cntrl()                                                         */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* vsF_Motor_StrtUp(u_8Bit vs_motor_pos)                                         */
/* Pwm_SetDutyCycle(Pwm_ChannelType ChannelNumber,uint16 DutyCycle)              */
/* 																				 */
/*********************************************************************************/
@far void vsF_Output_Cntrl(void)
{
	/* loop variable */
	u_8Bit c;            // for loop iteration

	for (c=0; c <VS_LMT; c++) {
		/* Check if Vs sw request is present. Also, check if relay is O.K. */
		if ( (vs_Flag1_c[c].b.sw_rq_b == TRUE) && (relay_A_Ok_b == TRUE) ) {
			
			/* Check vent state */
			if (vs_Flag1_c[c].b.state_b == TRUE) {
				/* Vent is already turned on */
			}
			else {
				/* Selected vent activation */
				/* Turn ON the vent enable pin (Vent circuitry is enabled) */
				Dio_WriteChannel(VENT_EN, STD_HIGH);
				
				/* Perform motor start up */
				vsF_Motor_StrtUp(c);
			}
				
			/* PWM Vs */
			if (c == VS_FL) {
				
				/* New condition check 10.27.2008 */
				/* Check if PWM is changed */
				if( (vs_prm_set[c].prev_pwm_w != vs_prm_set[c].pwm_w) ){
					/* Pwm the FL Vs output */
					Pwm_SetDutyCycle(PWM_VS_FL, vs_prm_set[c].pwm_w);					
				}else{
					/* No change in pwm. No need to call Pwm_SetDutyCycle */
				}
			}
			else {
				/* New condition check 10.27.2008 */
				/* Check if PWM is changed */
				if( (vs_prm_set[c].prev_pwm_w != vs_prm_set[c].pwm_w) ){
					/* Pwm the FR Vs output */
					Pwm_SetDutyCycle(PWM_VS_FR, vs_prm_set[c].pwm_w);					
				}else{
					/* No change in pwm. No need to call Pwm_SetDutyCycle */
				}
			}
			/* Update the previous pwm variable in order to detect a pwm change */
			vs_prm_set[c].prev_pwm_w = vs_prm_set[c].pwm_w;
			
			/* Clear Vsense delay counter for short to battery detection */
			vs_prm_set[c].Vsense_dly_cnt = 0;
		}
		else {
			
			/* No PWM for Vs */
			if (c == VS_FL) {	
				/* Check if Left vent PWM is active */
				if( (vs_Flag1_c[c].b.state_b == TRUE) || (vs_prm_set[c].prev_pwm_w != 0) ){
					/* No PWM for FL Vs */
					Pwm_SetDutyCycle(PWM_VS_FL, 0);					
				}else{
					/* PWM is not active. No need to call Pwm_SetDutyCycle */
				}
			}
			else {
				/* Check if Right vent PWM is active */
				if( (vs_Flag1_c[c].b.state_b == TRUE) || (vs_prm_set[c].prev_pwm_w != 0) ){
					/* No PWM for FR Vs */
					Pwm_SetDutyCycle(PWM_VS_FR, 0);
				}else{
					/* PWM is not active. No need to call Pwm_SetDutyCycle */
				}
			}
			
			/* Set previous pwm to zero of the selected vent */
			vs_prm_set[c].prev_pwm_w = 0;
			
			/* Clear vent pwm */
			vs_prm_set[c].pwm_w = 0;
			
			/* Set Vs State as turned Off */
			vs_Flag1_c[c].b.state_b = FALSE;
			
			/* Clear startup variables */
			vs_prm_set[c].strt_up_cnt = 0;
			vs_Flag1_c[c].b.start_up_cmplt_b = FALSE;
		}

	} //End of For loop
	
	/* Check if both vents are disabled */
	if( (vs_Flag1_c[VS_FL].b.sw_rq_b == FALSE) && (vs_Flag1_c[VS_FR].b.sw_rq_b == FALSE) ){
		/* Turn OFF the vent enable pin (Vent circuitry is disabled) */
		Dio_WriteChannel(VENT_EN, STD_LOW);
	}

}
/*********************************************************************************/
/*                          vsF_Fnsh_LED_Dsply                                   */
/*********************************************************************************/
/* Called at the end of a short-term (default 2 seconds) LED display routine in  */
/* order to clear the appropriate bit flags and/or variables. 					 */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* vsF_LED_Display :                                                             */
/*    vsF_Fnsh_LED_Dsply(u_8Bit vs_pstn)                                         */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* vsF_Clear(u_8Bit vs_pstn)                                                     */
/*																				 */
/*********************************************************************************/
@far void vsF_Fnsh_LED_Dsply(u_8Bit vs_point_c)
{
	/* Local variable(s) */
	u_8Bit vs_pnt = vs_point_c;    // store vent position
	
	/* Clear Vs */
	vsF_Clear(vs_pnt);
	
	/* Check if Diagnostic IO Control lock is active */
	if ( (diag_io_vs_lock_flags_c[vs_pnt].b.switch_rq_lock_b == TRUE) || (diag_io_all_vs_lock_b == TRUE) ) {
		/* Clear diagnostic request */
		diag_io_vs_rq_c[vs_pnt] = 0;
	}
	/* Check if diagnostics LED control lock is active */
	if (diag_io_vs_lock_flags_c[vs_pnt].b.led_status_lock_b == TRUE) {
		/* Clear diagnostics LED state */
		diag_io_vs_state_c[vs_pnt] = 0;
	}
}

/*********************************************************************************/
/*                          vsF_LED_Display                                      */
/*********************************************************************************/
/* This function is responsible to send a message on the CAN bus in order to turn*/
/* on/off the front vent light emitting diodes (LEDs). 							 */
/*                                                                               */
/* VentsF_Update_LED_Status                                                      */
/* +----------------------------++-----+------+------+-------+-------+-----+     */
/* |                            ||  1  |  2   |  3   |   4   |   5   |  6  |     */
/* +============================++=====+======+======+=======+=======+=====+     */
/* |                            || RUN | RUN  | RUN  |  RUN  |  RUN  |     |     */
/* |canio_RX_IGN_STATUS_c          || ||  |  ||  |  ||  |  ||   |  ||   |other|     */
/* |                            ||START|START |START | START | START |     |     */
/* +----------------------------++-----+------+------+-------+-------+-----+     */
/* |vs_LED_off_b                ||TRUE |FALSE |FALSE | FALSE | FALSE |  -  |     */
/* +----------------------------++-----+------+------+-------+-------+-----+     */
/* |vs_swtch_rq_prps_b          ||  -  |VNT_ON|VNT_ON|VNT_OFF|VNT_OFF|  -  |     */
/* +----------------------------++-----+------+------+-------+-------+-----+     */
/* |diag_io_vs_led_status_lock_b||  -  |  -   |  -   | TRUE  | FALSE |  -  |     */
/* +----------------------------++-----+------+------+-------+-------+-----+     */
/* |vs_stat_c                   ||  -  |  !=  | UGLY |   -   |   -   |  -  |     */
/* |                            ||     | UGLY |      |       |       |     |     */
/* +============================++=====+======+======+=======+=======+=====+     */
/* |VS_STATUS                   ||     |      |      |       |       |     |     */
/* |=                           ||     |  X   |      |   X   |       |     |     */
/* |CONTINUOUS                  ||     |      |      |       |       |     |     */
/* |DISPLAY                     ||     |      |      |       |       |     |     */
/* +----------------------------++-----+------+------+-------+-------+-----+     */
/* |VS_STATUS                   ||     |      |      |       |       |     |     */
/* |=                           ||     |      |      |       |       |     |     */
/* |2                           ||     |      |  X   |       |       |     |     */
/* |SEC                         ||     |      |      |       |       |     |     */
/* |DISPLAY                     ||     |      |      |       |       |     |     */
/* +----------------------------++-----+------+------+-------+-------+-----+     */
/* |VS_STATUS                   ||     |      |      |       |       |     |     */
/* |=                           ||     |      |      |       |       |     |     */
/* |NO                          ||  X  |      |      |       |   X   |  X  |     */
/* |LED                         ||     |      |      |       |       |     |     */
/* |DISPLAY                     ||     |      |      |       |       |     |     */
/* +----------------------------++-----+------+------+-------+-------+-----+     */
/*                                                                               */
/* SD Text                                                                       */
/* -------                                                                       */
/* Input parameter:                                                              */
/* vs_mark: Selected vent (FL or FR)                                             */
/*                                                                               */
/* Three main parts of this function are:                                        */
/*                                                                               */
/* 1. Checking if a fault is just matured. If yes, it finishes the LED display   */
/* right away for the vent output.                        						 */
/* 2. Arbitrating LED display logic. Deciding on whether continious, short-term, */
/* or no LED display is appropriate.                    						 */
/* 3. Perfoming the LED display by manipulating and sending the appropriate vent */
/* LED signal on the CAN bus.                           						 */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* IntFlt_F_ClearOutputs :                                                       */
/*    vsF_LED_Display(u_8Bit vs_pstn)                                            */
/* vsF_Manager :                                                                 */
/*    vsF_LED_Display(u_8Bit vs_pstn)                                            */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* vsF_Clear(u_8Bit vs_pstn)                                                     */
/* vsF_Fnsh_LED_Dsply(u_8Bit vs_pstn)                                            */
/* dbkGetTxL_F_HS()                                                              */
/* SetValueTxL_F_VS_STAT( variable, value)                                       */
/* dbkPutTxL_F_HS( variable)                                                     */
/*																				 */
/*********************************************************************************/
@far void vsF_LED_Display(u_8Bit vs_mark)
{
	/* Local variable(s) */
	u_8Bit vs_m = vs_mark;    // marks the selected vent
	u_8Bit vs_crt_rq = 0;     // current request status
	
	/* @Check if a fault is just matured */
	if (vs_Flag1_c[vs_m].b.LED_off_b == TRUE) {
		/* Clear LED off flag */
		vs_Flag1_c[vs_m].b.LED_off_b = FALSE;
		
		/* Set counter to finish LED Display */
		vs_prm_set[vs_m].LED_dsply_cnt = vs_LED_dsply_tm;
	}
	
	/* @LED Display Arbitration */
	/* Check ignition status */
	if ( (canio_RX_IGN_STATUS_c == IGN_RUN) || (canio_RX_IGN_STATUS_c == IGN_START) ) {
		
		/* Check switch purpose */
		if (vs_Flag2_c[vs_m].b.swtch_rq_prps_b == TRUE) {
			/* Check if there is a matured fault */
			if (vs_prm_set[vs_m].stat_c != UGLY) {
				/* Continuous LED Display */
			}
			else {
				/* Short-term LED Display */
				if (vs_prm_set[vs_m].LED_dsply_cnt < vs_LED_dsply_tm) {
					/* Count LED Display time */
					vs_prm_set[vs_m].LED_dsply_cnt++;
				}
				else {					
					/* Is Chrysler Routine Control (Output Test) running? */
					if(diag_rc_all_op_lock_b == FALSE){
						/* Regular Logic */
						/* Finish short term LED display */
						vsF_Fnsh_LED_Dsply(vs_m);						
					}else{
						/* Special case when we have Chrysler Routine Control Output test */
					}
				}
			}
		}
		else {
			/* Check if diagnostic LED lock is active */
			if (diag_io_vs_lock_flags_c[vs_m].b.led_status_lock_b == TRUE) {
				/* Continuous display for LED I/O Control (ignore fault status) */
			}
			else {
				/* Clear Vs */
				vsF_Clear(vs_m);
			}
		}
	}
	else {
		/* Clear Vs */
		vsF_Clear(vs_m);
	}
	
	/* Store the last request */
	/* Check if diagnostic LED lock is inactive */
	if (diag_io_vs_lock_flags_c[vs_m].b.led_status_lock_b == FALSE) {
		
		/* Is Chrysler Routine Control (Output Test) running? */
		if(diag_rc_all_op_lock_b == FALSE){
			/* Store the current request from current vs level */
			vs_crt_rq = vs_prm_set[vs_m].crt_lv_c;			
		}else{
			/* Check if we are still performing short term LED Display */
			if(vs_prm_set[vs_m].LED_dsply_cnt < vs_LED_dsply_tm){
				/* Store the current request from current vs level */
				vs_crt_rq = vs_prm_set[vs_m].crt_lv_c;				
			}else{
				/* Special case when we have Chrysler Routine Control Output test */
				/* We matured a DTC during the test. Set LED Stat OFF */
				vs_crt_rq = VL0;
			}
		}
	}
	else {
		/* Store the current request from diagnostics LED state */
		vs_crt_rq = diag_io_vs_state_c[vs_m];
	}
	
	/* @LED Display */
	if (vs_m == VS_FL) {
		
		/* Update the LED status only incase the previously stored status is different */
		/* than the present for FL */
		if(vs_lst_fl_led_stat_c != vs_crt_rq){
			
			/* Transmit the LED status on the bus */
//			IlPutTxFL_HS_STATSts(Local_L_F_VS);		
//#endif
			
			/* Update the previous LED status for FL */
			vs_lst_fl_led_stat_c = vs_crt_rq;			
		}else{
			/* No change in the LED status for FL */
		}
	}
	else {
		
		/* Update the LED status only incase the previously stored status is different */
		/* than the present for FR */
		if(vs_lst_fr_led_stat_c != vs_crt_rq){
			
			/* Transmit the LED status on the bus */
			//IlPutTxFR_HS_STATSts(Local_R_F_VS);
			
			/* Update the previous LED status for FR */
			vs_lst_fr_led_stat_c = vs_crt_rq;			
		}else{
			/* No change in the LED status for FR */
		}
	}
}

/*********************************************************************************/
/*                          vsF_Chck_Swtch_Prps                                  */
/*********************************************************************************/
/* Input parameter:                                                              */
/* vs_mark_c: Selected vent output (FL or FR)                                    */
/*                                                                               */
/* Turns on/off the software request (vs_sw_rq_b) that controls the vent output. */
/* It checks vent switch purpose and status.                                     */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* vsF_Diag_IO_Cntrl :                                                           */
/*    vsF_Chck_Swtch_Prps(u_8Bit vs_pstn)                                        */
/* vsF_swtch_Cntrl :                                                             */
/*    vsF_Chck_Swtch_Prps(u_8Bit vs_pstn)                                        */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* no calls                                                                      */
/* 																				 */
/*********************************************************************************/
@far void vsF_Chck_Swtch_Prps(u_8Bit vs_mark_c)
{
	/* Local variable(s) */
	u_8Bit vs_ms = vs_mark_c;         // marks the selected vent
	
	/* Check for switch purpose */
	if (vs_Flag2_c[vs_ms].b.swtch_rq_prps_b == TRUE) {
		
		/* Decide to serve or reject the switch request */
		if (vs_prm_set[vs_ms].stat_c == GOOD) {
			
			/* Turn on the vs software request */
			vs_Flag1_c[vs_ms].b.sw_rq_b = TRUE;
		}
		else {
			/* Turn off the vs software request */
			vs_Flag1_c[vs_ms].b.sw_rq_b = FALSE;
		}
	}
	else {
		/* Turn off the vs software request */
		vs_Flag1_c[vs_ms].b.sw_rq_b = FALSE;
	}
}

/*********************************************************************************/
/*                          vsF_swtch_Cntrl                                      */
/*********************************************************************************/
/* Input parameter:                                   							 */
/* vs_switch_pst: Selected vent position              							 */
/*                                                    							 */
/* Processes the switch request by:                   							 */
/*                                                    							 */
/* 1. Deciding if this is a valid switch request      							 */
/* 2. Updating vent level and checking switch purpose 							 */
/* 3. Setting the appropriate pwm for next level      							 */
/*                                                    							 */
/* Calls from                                         							 */
/* --------------                                     							 */
/* vsF_Manager :                                      							 */
/*    vsF_swtch_Cntrl(u_8Bit vs_pstn)                 							 */
/*                                                    							 */
/* Calls to                                           							 */
/* --------                                           							 */
/* vsF_Updt_Crrt_Lvl(u_8Bit vs_pstn)                 							 */
/* vsF_Chck_Swtch_Prps(u_8Bit vs_pstn)               							 */
/*																				 */
/*********************************************************************************/
@far void vsF_swtch_Cntrl(u_8Bit vs_switch_pst)
{
	/* Local variable(s) */
	u_8Bit vs_s = vs_switch_pst;        // marks the selected vent

	/* IF the switch request is through CCN */
	if (main_SwReq_Front_c == VEH_BUS_CCN_FRONT_REAR_K)
	{
		/* Check for valid switch request */
		if ( (canio_vent_seat_request_c[vs_s] == VS_PSD) && (vs_Flag2_c[vs_s].b.invld_swtch_rq_b == FALSE) )
		{
			/* Disable switch */
			vs_Flag2_c[vs_s].b.invld_swtch_rq_b = TRUE;

			/* Update current vs level */
			vsF_Updt_Crrt_Lvl(vs_s);

			/* Check switch purpose before turning on the sw request */
			vsF_Chck_Swtch_Prps(vs_s);
		}
		else
		{
			/* Disable switch until it is released */
			if (canio_vent_seat_request_c[vs_s] == VS_NOT_PSD)
			{
				/* Enable switch */
				vs_Flag2_c[vs_s].b.invld_swtch_rq_b = FALSE;
			}
			else
			{
				/* Disable switch */
				vs_Flag2_c[vs_s].b.invld_swtch_rq_b = TRUE;
			}
		}
	}
	else
	{
		/* Check for valid switch request through HVAC */
		if ( (canio_hvac_vent_seat_request_c[vs_s] == VS_HVAC_PSD) && (vs_Flag2_c[vs_s].b.invld_swtch_rq_b == FALSE) )
		{
			/* Disable switch */
			vs_Flag2_c[vs_s].b.invld_swtch_rq_b = TRUE;

			/* Update current vs level */
			vsF_Updt_Crrt_Lvl(vs_s);

			/* Check switch purpose before turning on the sw request */
			vsF_Chck_Swtch_Prps(vs_s);

		}
		else
		{
			/* Disable switch until it is released */
			if (canio_hvac_vent_seat_request_c[vs_s] == VS_NOT_PSD)
			{
				/* Enable switch */
				vs_Flag2_c[vs_s].b.invld_swtch_rq_b = FALSE;
			}
			else
			{
				/* Disable switch */
				vs_Flag2_c[vs_s].b.invld_swtch_rq_b = TRUE;
			}
		}
	}
}

/*********************************************************************************/
/*                          vsF_Rqst_Drg_Hs_Cycl                                 */
/*********************************************************************************/
/* Input parameter:                                                              */
/* vs_lokation: Selected vent (FL or FR)                                         */
/*                                                                               */
/* This function is called when the module receives a switch or diagnostic vent  */
/* request during heat mode (heated seat is activated by a switch request). 	 */
/*                                                                               */
/* It turns off the appropriate heated seat (vent is turn on at VL3 after this   */
/* function).                                                                	 */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* vsF_Manager :                                                                 */
/*    vsF_Rqst_Drg_Hs_Cycl(u_8Bit vs_pstn)                                       */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* hsLF_UpdateHeatParameters(unsigned char hs_nr,unsigned char value_c)          */
/* 																				 */
/*********************************************************************************/
@far void vsF_Rqst_Drg_Hs_Cycl(u_8Bit vs_lokation)
{
	/* Local variable(s) */
	u_8Bit vs_lk = vs_lokation;        // marks the selected vent
	
	/* Turn off the hs */
	hsLF_UpdateHeatParameters(vs_lk, HEAT_LEVEL_OFF);
}

/*********************************************************************************/
/*                          vsF_Rtrn_Cntrl_toECU                                 */
/*********************************************************************************/
/* Input parameter:                                                              */
/* vs_lktn: Selected vent location                                               */
/*                                                                               */
/* It turns off the selected vent output (and clears all related variables) after*/
/* diagnostic session is completed. 											 */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* vsF_Manager :                                                                 */
/*    vsF_Rtrn_Cntrl_toECU(u_8Bit vs_pstn)                                       */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* vsF_Clear(u_8Bit vs_pstn)                                                     */
/*																				 */
/*********************************************************************************/
@far void vsF_Rtrn_Cntrl_toECU(u_8Bit vs_lktn)
{
	/* Local variable(s) */
	u_8Bit vs_lkt = vs_lktn;     // selected vent location

	vs_Flag2_c[vs_lkt].b.diag_cntrl_actv_b = FALSE;      // diagnostics is inactive

	vs_prm_set[vs_lkt].flt_mtr_cnt         = 0;          // clear vs fault mature time
	
	/* Turn off the vent and clear vent parameters */
	vsF_Clear(vs_lkt);
}

/*********************************************************************************/
/*                          vsF_Diag_IO_Cntrl                                    */
/*********************************************************************************/
/* Input parameter:                                                              */
/* vs_diag_lct: Selected vent (FL or FR)                                         */
/*                                                                               */
/* Activates the diagnostic IO control software.                                 */
/* It clears the vent onTime and LED display counters with each valid diagnostic */
/* request.   																	 */
/* If diagnostic request is to display LED only, it makes sure that vent output  */
/* stays off.  																	 */
/* Otherwise, it updates the vent level and checks switch purpose before         */
/* modifying the pwm. 															 */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* vsF_Manager :                                                                 */
/*    vsF_Diag_IO_Cntrl(u_8Bit vs_pstn)                                          */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* vsF_Chck_Swtch_Prps(u_8Bit vs_pstn)                                           */
/* 																				 */
/*********************************************************************************/
@far void vsF_Diag_IO_Cntrl(u_8Bit vs_diag_lct)
{
	/* Local variable(s) */
	u_8Bit vs_k = vs_diag_lct;     // selected vent location
	
	/* Set diagnostic control active */
	vs_Flag2_c[vs_k].b.diag_cntrl_actv_b = TRUE;
	
	/* Check if this is a new Diagnostic IO request */
	if (vs_Flag2_c[vs_k].b.diag_clr_ontime_b == TRUE) {
		/* Reset the LED display timer */
		vs_prm_set[vs_k].LED_dsply_cnt = 0;
		
		/* Clear the ontime flag */
		vs_Flag2_c[vs_k].b.diag_clr_ontime_b = FALSE;
	}
	else {
		/* Existing Diagnostic Request. */
	}
	
	/* Check if DCX Routine Control Output Test flag is set and vent test is running */
	if(diag_rc_all_op_lock_b == TRUE){
		
		/* Are we running the DCX Routine Control for Vents? */
		if(diag_rc_all_vents_lock_b == TRUE){	
			
			/* Set vent level to VL_HI for DCX Routine Control Test */
			vs_prm_set[vs_k].crt_lv_c = VL_HI;
			
			/* Check switch purpose */
			vsF_Chck_Swtch_Prps(vs_k);
			
		}else{			
			/* Make sure vent stays off during heaters test */
			vsF_Clear(vs_k);	
		}				
	}else{
		/* Check if diagnostics LED control is inactive */
		if (diag_io_vs_lock_flags_c[vs_k].b.led_status_lock_b == FALSE) {
			
			/* Check IO request for range as a safety */
			if ( (diag_io_vs_rq_c[vs_k] == VL_LOW) || (diag_io_vs_rq_c[vs_k] == VL_HI) ) {
				/* Update current vs level */
				vs_prm_set[vs_k].crt_lv_c = diag_io_vs_rq_c[vs_k];
			}
			else {
				/* Keep vs at VL0 */
				vs_prm_set[vs_k].crt_lv_c = VL0;
			}

			/* Check switch purpose */
			vsF_Chck_Swtch_Prps(vs_k);
		}
		else {
			/* Keep the vs software request off */
			vs_Flag1_c[vs_k].b.sw_rq_b = FALSE;
			
			/* Keep Vs switch purpose off */
			vs_Flag2_c[vs_k].b.swtch_rq_prps_b = FALSE;
			
			/* Keep vs level at 0 */
			vs_prm_set[vs_k].crt_lv_c = VL0;
		}		
	}
}

/*********************************************************************************/
/*                          vsLF_Chck_mainState                                  */
/*********************************************************************************/
/* This function captures the remote start activation and deactivation events.   */
/*                                                                               */
/* It turns off the vent outputs when remote start timeout (deactivation) occurs.*/
/* For remote start activation, this fucntion manages the driver side selection  */
/* (canio_LHD_RHD_c).                                                            */
/* Only the driver side vent can be activated during remote start event          */
/* (exception: Diagnostic IO control).                                           */
/*                                                                               */
/* Changes to the function:(Tracker Id#44)                                       */
/* Enabled the other side vent also for the remote start functionality, but no   */
/* automatic start,user interaction needed.                                      */
/* Change request:  " During Remote start functionality allow full functionality */
/* of the passenger heated/vented seat through the heated/vent switch request" 	 */
/*                                                                               */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* vsF_Manager :                                                                 */
/*    vsLF_Chck_mainState()                                                      */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* vsF_Clear(u_8Bit vs_pstn)                                                     */
/* 																				 */
/*********************************************************************************/
@far void vsLF_Chck_mainState(void)
{
	/* Check for Remote start timeout (before updating last main state) */
	if ( (vs_lst_main_state_c != NORMAL) && (main_state_c == NORMAL) ) {
		
		/* Clear remote start automatic activation bit (for both sides) */
		vs_Flag1_c[VS_FL].b.RemSt_auto_b = FALSE;
		vs_Flag1_c[VS_FR].b.RemSt_auto_b = FALSE;  
		
		/* Make sure both vents are turned off at the end of remote start event */
		/* Harsha Commented for New requirement 05.12.2009 */
		/* MKS 28818: When remote start completes, new requirement says turned Outputs */
		/* Shall stay ON */
		//vsF_Clear(VS_FL);
		//vsF_Clear(VS_FR);
		
		/* If previous logic is in Remote start then when Remote start is OFF, reduce vent level */
		if((vs_lst_main_state_c == REM_STRT_VENTING) || (vs_lst_main_state_c == REM_STRT_USER_SWTCH))
		{
			/* MKS 56616: When remote start completes, new requirement says turned Outputs */
			/* Shall reduce to LOW Level */
			/* Driver seat vent entry point VL_LOW */
			vs_prm_set[vs_slctd_drive_c].crt_lv_c = VL_LOW;
		}
		
	}
	else {
		/* No remote start timeout */
	}
	
	/* Vent Management (update selected drive configuration) */
	/* Check main state */
	if (main_state_c == NORMAL) {
		/* Manage Both Vents */
		vs_slctd_drive_c = VS_LMT;
	}
	else {
		/* is canio_LHD_RHD_c equal to RHD? */
		if (canio_LHD_RHD_c == RHD) {
			/* Manage Only Right Vent during RemSt event by Automatic */
			vs_slctd_drive_c = VS_FR;
			
			/* The other side of vent should be enabled for Switch request in remote start.                                              */
			/* By setting this flag, system knows that already remote start phase started and waiting for Switch request input from user */
			vs_Flag1_c[VS_FL].b.RemSt_auto_b = TRUE;
			
			/* To allow switch request for other seat, which is not in automatic remote start */
			vs_other_drive_c = VS_FL;
		}
		else {
			/* For all other canio_LHD_RHD_c                   */
			/* manage only left vent during RemSt by automatic */
			vs_slctd_drive_c = VS_FL;
			
			/* The other side of vent should be enabled for Switch request in remote start.                                              */
			/* By setting this flag, system knows that already remote start phase started and waiting for Switch request input from user */
			vs_Flag1_c[VS_FR].b.RemSt_auto_b = TRUE;
			
			/* To allow switch request for other seat, which is not in automatic remote start */
			vs_other_drive_c = VS_FR;
		}

	}
	
	/* Check Remote Start Activation */
	/* Check for Remote start timeout (before updating last main state) */
	if ( (main_state_c != NORMAL) && (vs_lst_main_state_c == NORMAL) ) {
		
		/* Is main state REM_STRT_VENTING? */
       /* FTR00139669 -- Autostart feature for PowerNet Series.*/		
		if ((main_state_c == REM_STRT_VENTING) || (main_state_c == REM_STRT_USER_SWTCH) /*|| (main_state_c == AUTO_STRT_VENTING) || (main_state_c == AUTO_STRT_USER_SWTCH)*/) {
			/* Driver seat vent entry point VL_HI */
			vs_prm_set[vs_slctd_drive_c].crt_lv_c = VL_HI;
		}
	   	else if((main_state_c == AUTO_STRT_VENTING) || (main_state_c == AUTO_STRT_USER_SWTCH)){
	   		
	   		/* Logic is in Auto start. Reduce the startup point to LOW, if only output is activated */
	   		if(vs_Flag1_c[vs_slctd_drive_c].b.sw_rq_b == FALSE)
	   		{
	   			/* Auto start, entry point is VL_LOW */
	   			/* MKS 56616: Start with LOW PWM for Autostart */
	   			vs_prm_set[vs_slctd_drive_c].crt_lv_c = VL_LOW;
	   		}
	   	}
	   	else {
			/* Turn off the driver seat vent */
		   	vs_prm_set[vs_slctd_drive_c].crt_lv_c = VL0;
	   	}
	}
	else {
		/* No remote start activation */
	}
	
	/* Update last main state */
	vs_lst_main_state_c = main_state_c;
}

/*********************************************************************************/
/*                        vsLF_Chck_Lsd2_Lvl_Reduction                         	 */
/*********************************************************************************/
/* Function Name: @far void vsLF_Chck_Lsd2_Lvl_Reduction(u_8Bit vs_toBe_Reduced) */
/* Type: Local Function													         */
/* Returns: None                     											 */
/* Parameters: u_8Bit vs_toBe_Reduced (Vent needs to be check for load reduction)*/
/* Description: This function implements the new PowerNet requirement.           */
/*              If CSWM is already �on� and receives the load-shed level 2       */
/*              signal from CAN I bus then, The heated or vented seats, if not   */
/*              already, will reduce the duty cycle to LL1 or VL1.           	 */
/*																			     */
/* Calls to: None																 */
/* Calls from:																	 */	
/*    vsF_Manager()																 */
/*																				 */  
/*********************************************************************************/
//@far void vsLF_Chck_LdShd_Lvl_Reduction(u_8Bit vs_toBe_Reduced)
//{
//	/* Local variable(s) */
//	u_8Bit rdcLoc = vs_toBe_Reduced;	// the vent location that needs to be checked
//	
//	/* Updated on 04.14.2009 by CK to check Battery Reached Critical State signal before reducing level. */
//	/* Check if Load Shed 2 signal is present (without load shed 3. Load Shed 7, and Battery Reached Critical State) */
//	/* Also checking for vent status to determine if vent is turned ON. Otherwise, we will perform regular LED display. */
//	if( (canio_PN14_LS_Lvl2_c == TRUE) && (canio_PN14_LS_Lvl3_c == FALSE) &&
//		(canio_PN14_LS_Lvl7_c == FALSE) && (canio_Batt_ST_Crit_c == FALSE) && (vs_prm_set[rdcLoc].stat_c == GOOD) )
//	{
//		/* Check present vent level to determine if load reduction is necassary */
//		if(vs_prm_set[rdcLoc].crt_lv_c == VL_HI){			
//			/* Reduce vent level to VL1 */
//			vs_prm_set[rdcLoc].crt_lv_c = VL_LOW;
//		}else{
//			/* Conditions are not met to reduce level. No action is needed. */
//			
//		}	// Check vent level else path end		
//	}else{
//		/* Load Reduction Load Shed 2 condition is not present or vs status is not good */
//		/* ==> (Output is not turned on). No action is needed. */
//		
//	}	// Load shed 2 signal check else path end	
//}

/*********************************************************************************/
/*                          vsF_Manager                                          */
/*********************************************************************************/
/* This function receives the user requests to turn on/off the vent output.      */
/* It determines whether to serve the turn on request by examining cswm status,  */
/* vent fault conditions. 														 */
/*                                                                               */
/* SD Text                                                                       */
/* -------                                                                       */
/* This function manages the vent outputs (FL and FR) control by:                */
/*                                                                               */
/* 1. Checking and updating vent status                                          */
/* 2. Receiving the switch or diagnostic requests                                */
/* 3. Arbitrating switch or diagnostic logic                                     */
/* 4. Controlling vent output (by setting or clearing sw_rq_b bit                */
/* 5. Performing LED display                                                     */
/*                                                                               */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* not called                                                                    */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* vsF_Updt_Status(u_8Bit vs_pstn)                                               */
/* vsF_Nrmlz_PWM(u_8Bit vs_pstn)                                                 */
/* vsF_swtch_Cntrl(u_8Bit vs_pstn)                                               */
/* vsF_Rtrn_Cntrl_toECU(u_8Bit vs_pstn)                                          */
/* vsF_Diag_IO_Cntrl(u_8Bit vs_pstn)                                             */
/* vsF_Rqst_Drg_Hs_Cycl(u_8Bit vs_pstn)                                          */
/* vsF_Clear(u_8Bit vs_pstn)                                                     */
/* vsF_LED_Display(u_8Bit vs_pstn)                                               */
/* vsF_swtch_Mngmnt(u_8Bit vs_pstn)                                              */
/* vsLF_Chck_mainState()                                                         */
/* vsLF_RemSt_Check(u_8Bit vs_start_side)                                        */
/* u_8Bit hsF_FrontHeatSeatStatus(unsigned char hs_nr)                           */
/*																				 */
/*********************************************************************************/
@far void vsF_Manager(void)
{
	/* Local variables */
	u_8Bit e;                 // for loop iteration
	u_8Bit crt_hs_stat;       // current heated seat status (active/in-active)
	
	/* Check main state (to detect Remote Start activation or diactivation) */
	vsLF_Chck_mainState();

	for (e=0; e<VS_LMT; e++) {
		/* Check driver selection (Perform management for driver seat only during Remote Start event) */
		/* Driver selection for Normal Switch request                                                 */
		/* Driver selection for Automatic Remote start and User switch press for Automatic driver and */
		/* Driver selection for other seat when Auto Remote start is selected (Tracker Id#44).        */

		if ( (vs_slctd_drive_c == VS_LMT) || (e == vs_slctd_drive_c) || (e == vs_other_drive_c)) {
			/* @Vs Manager General Tasks */
			/* Get the latest Vs Status */
			vsF_Updt_Status(e);
			
			/* Switch management */
			vsF_swtch_Mngmnt(e);
						
			/* Check heated seat activation */
			crt_hs_stat = (u_8Bit) hsF_FrontHeatSeatStatus(e);
			
			/* Go to the vent level pwm once we are done with motor start up */
			if (vs_Flag1_c[e].b.start_up_cmplt_b == TRUE) {
				/* Normalize the PWM */
				vsF_Nrmlz_PWM(e);
			}
			else {
				/* Do not normalize pwm */
			}
			
			/* @Arbitrate Control */
			/* Check if seat heating is inactive */
			if (crt_hs_stat == HS_NOT_ACTIVE) {
				/* Check if diagnostics IO control is inactive */
				if ( (diag_io_vs_lock_flags_c[e].b.switch_rq_lock_b == FALSE) && (diag_io_vs_lock_flags_c[e].b.led_status_lock_b == FALSE) && (diag_io_all_vs_lock_b == FALSE) && (diag_rc_all_op_lock_b == FALSE) ) {
					/* Check if diagnostics need to return control to ECU */
					if (vs_Flag2_c[e].b.diag_cntrl_actv_b == FALSE) {
						/* Ordinary Switch Control */
						vsF_swtch_Cntrl(e);
					}
					else {
						/* Return control to ECU */
						vsF_Rtrn_Cntrl_toECU(e);
					}
					
					/* Check for Remote Start automatic activation */
					vsLF_RemSt_Check(e);
					
					/* Check for Automatic load reduction to VL1 under Load shed 2 condition */
					//vsLF_Chck_LdShd_Lvl_Reduction(e); //No Loadshed for this CUSW Program.
				}
				else {
					/* Diagnostic IO Control */
					vsF_Diag_IO_Cntrl(e);
				}

			}
			else {
				/* Check for Vent Diagnostic IO request during heat cycle */
				if ( (diag_io_vs_lock_flags_c[e].b.switch_rq_lock_b == TRUE) || (diag_io_all_vs_lock_b == TRUE) || (diag_io_vs_lock_flags_c[e].b.led_status_lock_b == TRUE) || (diag_rc_all_op_lock_b == TRUE) ) {
					
					/* Check if DCX Routine Control Output Test lock is not set */
					if(diag_rc_all_op_lock_b == FALSE){
						
						/* Regular logic. DCX Routine Control Output test lock is not set */
						/* Vs request during heat cycle */
						vsF_Rqst_Drg_Hs_Cycl(e);
						
					}else{
						/* Do not clear Hs cycle during DCX Routine Control Ouptut Test */
						/* Heater outputs will turn off by heater module once heater test is completed */
					}
					
					/* Diagnostic IO Control */
					vsF_Diag_IO_Cntrl(e);
				}
				else {

					if ((main_SwReq_Front_c == VEH_BUS_CCN_FRONT_REAR_K)) {
						
						/* Check for "valid" vs switch request with CCN */
						if ( (canio_vent_seat_request_c[e] == VS_PSD) && (vs_Flag2_c[e].b.invld_swtch_rq_b == FALSE) && (diag_io_hs_lock_flags_c[e].b.switch_rq_lock_b == FALSE) && (diag_io_hs_lock_flags_c[e].b.led_status_lock_b == FALSE) && (diag_io_all_hs_lock_b == FALSE) ) {
							
							/* Vs request during heat cycle */
							vsF_Rqst_Drg_Hs_Cycl(e);

							/* Ordinary Switch Control */
							vsF_swtch_Cntrl(e);
						}
						else{
							/* Keep the vent off */
							vsF_Clear(e);
						}
					}
					else {
						
						/* Check for "valid" vs switch request with HVAC */
						if ( (canio_hvac_vent_seat_request_c[e] == VS_HVAC_PSD) && (vs_Flag2_c[e].b.invld_swtch_rq_b == FALSE) && (diag_io_hs_lock_flags_c[e].b.switch_rq_lock_b == FALSE) && (diag_io_hs_lock_flags_c[e].b.led_status_lock_b == FALSE) && (diag_io_all_hs_lock_b == FALSE) && (diag_rc_all_op_lock_b == FALSE) ) {
							
							/* Vs request during heat cycle */
							vsF_Rqst_Drg_Hs_Cycl(e);

							/* Ordinary Switch Control */
							vsF_swtch_Cntrl(e);
						}
						else {
							/* Keep the vent off */
							vsF_Clear(e);
						}
					}
				}
			}
			
			/* @Output management (if vs status is not good, keep vs off) */
			if (vs_prm_set[e].stat_c != GOOD) {
				/* Turn off the vs software request */
				vs_Flag1_c[e].b.sw_rq_b = FALSE;
			}
			else {
				/* Check last switch request purpose */
				if (vs_Flag2_c[e].b.swtch_rq_prps_b == TRUE) {
					
					/* Turn on the vs software request */
					vs_Flag1_c[e].b.sw_rq_b = TRUE;
				}
				else {
					/* Turn off the vs software request */
					vs_Flag1_c[e].b.sw_rq_b = FALSE;
				}
			}
			
			/* @LED management */
			vsF_LED_Display(e);
		}
		else {
			/* Remote start is active. */
			/* Make sure passenger seat stays off. */
			vsF_Clear(e);
			
			/* Also keep the LED off for the passenger seat vent */
			vsF_LED_Display(e);
		}
	}
}

/*********************************************************************************/
/*                          vsF_Updt_Flt_Stat                                    */
/*********************************************************************************/
/* Input parameters:                                                             */
/* vs_fault: Type of the vent fault present                                      */
/* vs_flt_pstn: Vent position (FL or FR) where the fault is present              */
/*                                                                               */
/* This function checks the vent fault mature time.                              */
/* If it is not expired, it sets the preliminary fault for selected fault type.  */
/* If or when mature time is expired, it matures the appropriate vent fault (and */
/* clears the preliminary fault).                        						 */
/*                                                                               */
/* Change to Open Fault detection:(Tracker Id#43)                                */
/* Customer requested to change the mature time to blow OPEN DTC for vents. Per  */
/* request added 10sec mature time for blowing the fault. 						 */
/* Changes on HOLD (Contineous operation even after blowing the DTC. At this     */
/* moment outputs turn OFF)                                   					 */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* vsF_Fault_Mntr :                                                              */
/*    vsF_Updt_Flt_Stat(u_8Bit vs_pstn,u_8Bit vs_flt_type)                       */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* no calls                                                                      */
/* 																				 */
/*********************************************************************************/
@far void vsF_Updt_Flt_Stat(u_8Bit vs_flt_pstn, Vs_Flt_Type_e vs_fault)
{
	/* Local variable(s) */
	u_8Bit vs_flt_p = vs_flt_pstn;       // stores faultly vent position
	
	/* Check the fault mature time and fault status */
	if (vs_prm_set[vs_flt_p].flt_mtr_cnt < (vs_flt_mtr_tm-1)) {
		/* Count fault mature time */
		vs_prm_set[vs_flt_p].flt_mtr_cnt++;

		/* @Preliminary Faults */
		switch (vs_fault) {

			case VS_OPEN:
			{
				/* Update vs fault status */
				vs_prm_set[vs_flt_p].flt_stat_c |= VS_PRLM_OPEN_MASK;
				break;
			}

			case VS_SHRT_TO_GND:
			{
				/* Update vs fault status */
				vs_prm_set[vs_flt_p].flt_stat_c |= VS_PRLM_GND_MASK;
				break;
			}

			case VS_SHRT_TO_BATT:
			{
				/* Update vs fault status */
				vs_prm_set[vs_flt_p].flt_stat_c |= VS_PRLM_BATT_MASK;
				break;
			}

			default :
			{
				/* Fault parameter is not correct. Clear the fault status */
				vs_prm_set[vs_flt_p].flt_stat_c = VS_NO_FLT_MASK;
			}
		}
	}
	else {
		
		/* @Matured Faults */
		switch (vs_fault) {

			case VS_OPEN:
			{
				/* Tracker Id#43                                                                                     */
				/* Needs to wait extra 8sec to blow the OPEN DTC. As we are here means already completed 2sec time. */
				if (vs_prm_set[vs_flt_p].flt_mtr_cnt >= (Vs_flt_cal_prms.vs_10sec_mature_time)) {
					
					/* Vent Fault matured. Do not turn OFF the outputs(On Hold). */
					/* Customer driven change                                    */
					vs_prm_set[vs_flt_p].flt_stat_c = VS_MTRD_OPEN_MASK;
					
					/* Clear fault mature count */
					vs_prm_set[vs_flt_p].flt_mtr_cnt = 0;
				}
				else {
					/* Count fault mature time */
					vs_prm_set[vs_flt_p].flt_mtr_cnt++;
				}
				break;
			}

			case VS_SHRT_TO_GND:
			{
				/* Mature vs fault: short to ground */
				vs_prm_set[vs_flt_p].flt_stat_c = VS_MTRD_GND_MASK;
				
				/* Clear fault mature count */
				vs_prm_set[vs_flt_p].flt_mtr_cnt = 0;
				break;
			}

			case VS_SHRT_TO_BATT:
			{
				/* Mature vs fault: short to battery */
				vs_prm_set[vs_flt_p].flt_stat_c = VS_MTRD_BATT_MASK;
				
				/* We have detected a short to battery fault. Disable both vents. Support 2 second LED display. */
				vs_shrtTo_batt_mntr_c = VS_SHRT_TO_BATT;
				
				/* Clear fault mature count */
				vs_prm_set[vs_flt_p].flt_mtr_cnt = 0;
				break;
			}

			default :
			{
				/* Fault parameter is not correct. Clear the fault status */
				vs_prm_set[vs_flt_p].flt_stat_c = VS_NO_FLT_MASK;
				
				/* Clear fault mature count */
				vs_prm_set[vs_flt_p].flt_mtr_cnt = 0;
			}

		}		
		/* Clear fault mature count */
		//vs_prm_set[vs_flt_p].flt_mtr_cnt = 0;
	}
}

/*********************************************************************************/
/*                          vsF_Fault_Mntr                                       */
/*********************************************************************************/
/* This function examines the feedback value of the vent (Isense and Vsense) in  */
/* order to determine any fault conditions.                            			 */
/* 2 second time window before maturing a fault.                                 */
/*                                                                               */
/* There are three possible fault scenarios:                                     */
/* (a) Short to Battery                                                          */
/* (b) Short to Ground                                                           */
/* (c) Open circuit                                                              */
/*                                                                               */
/* No fault detection during following conditions:                               */
/* 1. main_CSWM_status_c is other then good. (Load Shedding, preliminary or      */
/* matured system and/or battery voltage faults, LOC with FCM or CCN etc). 		 */
/* 2. relay_A_internal_Fault_b is set.                                           */
/* 3. Vent short to battery fault is already matured                             */
/*                                                                               */
/* VentsF_Fault_Detection                                                        */
/* +------------------------++--------+-------+--------+--------+-------+        */
/* |                        ||   1    |   2   |   3    |   4    |   5   |        */
/* +========================++========+=======+========+========+=======+        */
/* |vs_state_b              || FALSE  | TRUE  |  TRUE  |  TRUE  | FALSE |        */
/* +------------------------++--------+-------+--------+--------+-------+        */
/* |main_CSWM_status_c      ||  GOOD  | GOOD  |  GOOD  |  GOOD  | GOOD  |        */
/* +------------------------++--------+-------+--------+--------+-------+        */
/* |relay_A_internal_Fault_b|| FALSE  | FALSE | FALSE  | FALSE  | FALSE |        */
/* +------------------------++--------+-------+--------+--------+-------+        */
/* |vs_Vsense_c             ||   >    |   <   |   -    |   >=   |   <   |        */
/* |                        ||STB__LMT|STG_LMT|        |STG_LMT |STB_LMT|        */
/* +------------------------++--------+-------+--------+--------+-------+        */
/* |vs_Isense_c             ||   -    |   -   |   <    |   >=   |   -   |        */
/* |                        ||        |       |OPEN_LMT|OPEN_LMT|       |        */
/* +========================++========+=======+========+========+=======+        */
/* |vs_flt_stat             ||        |       |        |        |       |        */
/* |==                      ||   X    |       |        |        |       |        */
/* |SHRT_TO_BATT            ||        |       |        |        |       |        */
/* +------------------------++--------+-------+--------+--------+-------+        */
/* |vs_flt_stat             ||        |       |        |        |       |        */
/* |==                      ||        |   X   |        |        |       |        */
/* |SHRT_TO_GND             ||        |       |        |        |       |        */
/* +------------------------++--------+-------+--------+--------+-------+        */
/* |vs_flt_stat             ||        |       |        |        |       |        */
/* |==                      ||        |       |   X    |        |       |        */
/* |OPEN_CRKT               ||        |       |        |        |       |        */
/* +------------------------++--------+-------+--------+--------+-------+        */
/* |vs_flt_stat             ||        |       |        |        |       |        */
/* |==                      ||        |       |        |   X    |   X   |        */
/* |VS_NO_FLT_MASK          ||        |       |        |        |       |        */
/* +------------------------++--------+-------+--------+--------+-------+        */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* main :                                                                        */
/*    vsF_Fault_Mntr()                                                           */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* vsF_Updt_Flt_Stat(u_8Bit vs_pstn,u_8Bit vs_flt_type)                          */
/* 																				 */
/*********************************************************************************/
@far void vsF_Fault_Mntr(void)
{
	/* Local variables */
	u_8Bit f;                                              // for loop iteration
	u_8Bit vs_prv_flt_typ_c;                              // stores the previous fault type
	for (f=0; f < VS_LMT; f++) {
		
		/* Store the previous fault status */
		vs_prv_flt_typ_c = vs_prm_set[f].flt_stat_c;
		
		/* @Fault Detection Logic */
		/* Check if Vs fault detection is possible or feasible */
		if ( (main_CSWM_Status_c == GOOD) && (relay_A_internal_Fault_b == FALSE) && (vs_prm_set[f].flt_stat_c != VS_MTRD_BATT_MASK) ) {
			
			/* Check if vs is turned on */
			if (vs_Flag1_c[f].b.state_b == TRUE) {
				
				/* Check short to ground fault */
				if (vs_Vsense_c[f] >= Vs_flt_cal_prms.Flt_Dtc[V_SHRT_TO_GND]) {
					/* No preliminary shrt to gnd */
					vs_prm_set[f].flt_stat_c &= VS_CLR_PRLM_GND;
					
					/* Check vent output voltage in order to determine if open fault detection is feasible */
					if (vs_Vsense_c[f] >= Vs_flt_cal_prms.Flt_Dtc[V_EN_OPEN_DEC]) {
						
						/* Check Isense for Open circuit detection */
						if (vs_Isense_c[f] < Vs_flt_cal_prms.Flt_Dtc[V_OPEN]) {
							/* Open circuit fault */
							vsF_Updt_Flt_Stat(f, VS_OPEN);
						}
						else {
							/* No Vs faults */
							vs_prm_set[f].flt_stat_c = VS_NO_FLT_MASK;
						}

					}
					else {
						/* Open fault detection is not feasible. Clear any preliminary open faults. */
						vs_prm_set[f].flt_stat_c &= VS_CLR_PRLM_OPEN;
					}

				}
				else {
					/* Short to GND */
					vsF_Updt_Flt_Stat(f, VS_SHRT_TO_GND);
				}
			}
			else {
				/* Perform fault detection only when there is no output requests (no ramp-up or start-up) */
				if (vs_Flag1_c[f].b.sw_rq_b == FALSE) {
					
					/* Check for Vsense analysis delay (160ms) */
					if (vs_prm_set[f].Vsense_dly_cnt < VS_VSENSE_DLY)  {
						
						/* Increment Vsense delay counter */
						vs_prm_set[f].Vsense_dly_cnt++;
					}
					else {
						/* Check Vsense */
						if (vs_Vsense_c[f] >= Vs_flt_cal_prms.Flt_Dtc[V_SHRT_TO_BATT]) {
							
							/* Short to Battery */
							vsF_Updt_Flt_Stat(f, VS_SHRT_TO_BATT);
						}
						else {
							/* No preliminary battery fault */
							vs_prm_set[f].flt_stat_c &= VS_CLR_PRLM_BATT;
							
							/* Check for matured faults */
							if (vs_prm_set[f].flt_stat_c > VS_PRL_FLT_MAX) {
								
								/* Keep the same fault status */
							}
							else {
								/* No Vs fault */
								vs_prm_set[f].flt_stat_c = VS_NO_FLT_MASK;
							}
						}
					}
				}
				else {
					/* No fault detection during motor start-up */
				}

			} //End of SHRT to BAT detection

		} //End If case for main if Loop
		else {
			/* Clear fault mature count */
			vs_prm_set[f].flt_mtr_cnt = 0;
		}
		
		/* Clear fault mature count if applicable */
		if ( (vs_prv_flt_typ_c != vs_prm_set[f].flt_stat_c) || (vs_prm_set[f].flt_stat_c == VS_NO_FLT_MASK) ) {
			/* Clear fault mature count */
			vs_prm_set[f].flt_mtr_cnt = 0;
		}

	} //End of For Loop

} //End of function

/*********************************************************************************/
/*                          vsF_DTC_Manager                                      */
/*********************************************************************************/
/* Controls the vent faults DTC status.                                          */
/* If there is a vent matured fault, it sets the appropriate DTC by calling the  */
/* FMemLibF_ReportDtc function.                                     			 */
/*                                                                               */
/* FMemLibF_ReportDtc function takes quite a bit to execute (updates all         */
/* supported DTC information).                                             		 */
/* Therefor, we are updating the DTC status for one vent (start with FR) at a    */
/* time (see vs_slct variable) to improve the time slice performance. 			 */
/*                                                                               */
/* 1st time: Updates FR vent DTC status                                          */
/* 2nd time: Updates FL vent DTC status                                          */
/* 3rd time: Updates FR vent DTC status ...and so on.                            */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* main :                                                                        */
/*    vsF_DTC_Manager()                                                          */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* FMemLibF_ReportDtc(DtcType DtcCode,enum ERROR_STATES State)                   */
/* 																				 */
/*********************************************************************************/
@far void vsF_DTC_Manager(void)
{
	/* Select vent for DTC management (This is added to improve time slice performance) */
	if (vs_slct == VS_FL)
	{
		/* Update DTC status for front right vent */
		vs_slct = VS_FR;
	}
	else
	{
		/* Update DTC status for front left vent */
		vs_slct = VS_FL;
	}
	/* Open circuit */
	
	/* Check for matured open circuit fault */
	if ( (vs_prm_set[vs_slct].flt_stat_c & VS_MTRD_OPEN_MASK) == VS_MTRD_OPEN_MASK)
	{
		/* Check vent position */
		if (vs_slct == VS_FL)
		{
			/* Open Circuit DTC is active for front left vent */
			FMemLibF_ReportDtc(DTC_VS_FL_OPEN_K, ERROR_ON_K);
		}
		else
		{
			/* Open Circuit DTC is active for front right vent */
			FMemLibF_ReportDtc(DTC_VS_FR_OPEN_K, ERROR_ON_K);
		}
	}
	else
	{
		/* Check vent position */
		if (vs_slct == VS_FL)
		{
			/* Open Circuit DTC is not active for front left vent */
			FMemLibF_ReportDtc(DTC_VS_FL_OPEN_K, ERROR_OFF_K);
		}
		else
		{
			/* Open Circuit DTC is not active for front right vent */
			FMemLibF_ReportDtc(DTC_VS_FR_OPEN_K, ERROR_OFF_K);
		}
	}
	
	/* Short to Ground */
	/* Check for matured short to ground fault                                                          */
	/* Also, wait 2 seconds before de-maturing short to ground fault while short to battery is maturing */
	if ( (vs_prm_set[vs_slct].flt_stat_c & VS_MTRD_GND_MASK) == VS_MTRD_GND_MASK)
	{
		/* Check vent position */
		if (vs_slct == VS_FL)
		{
			/* Short to Ground DTC is active for front left vent */
			FMemLibF_ReportDtc(DTC_VS_FL_SHORT_TO_GND_K, ERROR_ON_K);
		}
		else
		{
			/* Short to Ground DTC is active for front right vent */
			FMemLibF_ReportDtc(DTC_VS_FR_SHORT_TO_GND_K, ERROR_ON_K);
		}
	}
	else
	{
		/* Check vent position */
		if (vs_slct == VS_FL)
		{
			/* Short to Ground DTC is not active for front left vent */
			FMemLibF_ReportDtc(DTC_VS_FL_SHORT_TO_GND_K, ERROR_OFF_K);
		}
		else
		{
			/* Short to Ground DTC is not active for front right vent */
			FMemLibF_ReportDtc(DTC_VS_FR_SHORT_TO_GND_K, ERROR_OFF_K);
		}
	}
	
	/* Short to Battery */
	/* Check for matured short to battery fault */
	if ( (vs_prm_set[vs_slct].flt_stat_c & VS_MTRD_BATT_MASK) == VS_MTRD_BATT_MASK)
	{
		/* Check vent position */
		if (vs_slct == VS_FL)
		{
			/* Short to Battery DTC is active for front left vent */
			FMemLibF_ReportDtc(DTC_VS_FL_SHORT_TO_BAT_K, ERROR_ON_K);
		}
		else
		{
			/* Short to Battery DTC is active for front right vent */
			FMemLibF_ReportDtc(DTC_VS_FR_SHORT_TO_BAT_K, ERROR_ON_K);
		}
	}
	else
	{
		/* Check vent position */
		if (vs_slct == VS_FL)
		{
			/* Short to Battery DTC is not active for front right vent */
			FMemLibF_ReportDtc(DTC_VS_FL_SHORT_TO_BAT_K, ERROR_OFF_K);
		}
		else
		{
			/* Short to Battery DTC is not active for front right vent */
			FMemLibF_ReportDtc(DTC_VS_FR_SHORT_TO_BAT_K, ERROR_OFF_K);
		}
	}
}

/*********************************************************************************/
/*                          vsF_Copy_Diag_Prms (NOT USED)                        */
/*********************************************************************************/
/* Input paramter:                                                               */
/* u_8Bit* vs_data: Pointer to the new calibration data.                         */
/*                                                                               */
/* This function copies the new vent calibration data into a buffer array if the */
/* new data is in allowed range. 												 */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* vsF_Chck_Diag_Prms :                                                          */
/*    vsF_Copy_Diag_Prms(u_8Bit * vs_data_p_c)                                   */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* no calls                                                                      */
/*********************************************************************************/
//@far void vsF_Copy_Diag_Prms(u_8Bit *vs_data)
//{
//	/* Local variables */
//	u_8Bit g;                //for loop variable
//	u_8Bit *vs_cpy_data_p;   //pointer to data that needs to be copied
//
//	vs_cpy_data_p = vs_data; // point to copy data
//
//	for (g=0; g<VS_DATA_LENGHT; g++)
//	{
//		vs_diag_prog_data_c[g] = *(vs_cpy_data_p + g);
//	}
//}

/*********************************************************************************/
/*                          vsF_Chck_Diag_Prms                                   */
/*********************************************************************************/
/* Input paramter:                                                               */
/* vs_LED_ser: Diagnostic service ID that selects particular calibration data    */
/* (vent pwm's or timeouts).               										 */
/* * vs_data_p: pointer to the new calibration data                              */
/*                                                                               */
/* Return parameter:                                                             */
/* vs_chck_rslt: 1 if new data is in allowed range. 0 otherwise.                 */
/*                                                                               */
/* This function performs a check on the new vent calibration data to find out   */
/* whether it is in allowed range or not. 										 */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* ApplDescProcessWriteDataByIdentifier :                                        */
/*    u_8Bit vsF_Chck_Diag_Prms(u_8Bit vs_LID_service,u_8Bit* vs_data_p_c)       */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* 										                                         */
/*********************************************************************************/
@far u_8Bit vsF_Chck_Diag_Prms(u_8Bit vs_LID_ser, u_8Bit *vs_data_p)
{
	/* Local variable(s) */
	u_8Bit vs_LID_service = vs_LID_ser;       // LID service
	u_8Bit *vs_data_p_c = vs_data_p;          // pointer to start address of data needs to be checked
	u_8Bit vs_chck_rslt = FALSE;              // check result
	
	/* Is this a valid vs LID service? */
	if (vs_LID_service == VS_PWM) {
		/* Vent Level    Default PWM      Allowed Range (duty cycle) */
		/*    VL_LO          80                50 to 100             */
		/*    VL_HI         100                50 to 100             */
		if ( ( *vs_data_p_c < 50 || *vs_data_p_c > 100 ) ||
		   ( ( *(vs_data_p_c + 1) < 50 || *(vs_data_p_c + 1) > 100) ) ) {
			/* Diagnostic data is not in allowed range.               */
			/* Do not give access for write diagnostic data function. */
			vs_chck_rslt = FALSE;
		}
		else {
			/* Copy the data in local array. */
//			vsF_Copy_Diag_Prms(vs_data_p_c); //Harsha commented. 09.08.2008
			
			Vs_cal_prms.cals[hs_vehicle_line_c].PWM_a[VL_LOW] = *vs_data_p_c;
			Vs_cal_prms.cals[hs_vehicle_line_c].PWM_a[VL_HI - 1] = *(vs_data_p_c + 1);			
			
			/* Data is in correct range.              */
			/* Give access to Write Diagnostics data. */
			vs_chck_rslt = TRUE;
		}
	}
	else {
		/* This service is not supported by diagnostics */
		vs_chck_rslt = FALSE;
	}
	return(vs_chck_rslt);
}

/*********************************************************************************/
/*                          vsF_Write_Diag_Data                                  */
/*********************************************************************************/
/* Input parameter:                                                              */
/* vs_LID: Diagnostic service ID number that selects certain calibration data    */
/* (vent pwms or wheel timeouts).                                                */
/*                                                                               */
/* Once it is determined that new calibration data is in allowed range, this     */
/* function is called to update non-volatile memory with new calibration data.   */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* diagLF_WriteData :                                                            */
/*    u_8Bit vsF_Write_Diag_Data(u_8Bit vs_LID_service)                          */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* EE_BlockWrite()                                                               */
/* 																				 */
/*********************************************************************************/
@far u_8Bit vsF_Write_Diag_Data(u_8Bit vs_LID)
{
	/* Local variable(s) */
	u_8Bit vs_LID_s = vs_LID;       // LID service
	u_8Bit vs_write_rslt = FALSE;   // write result (success or fail)
	
	/* Check if this is a valid vs LID service */
	if (vs_LID_s == VS_PWM) {
		/* Update the entire vs calibration parameters */
		EE_BlockWrite(EE_VENT_PWM_DATA, (u_8Bit*)&Vs_cal_prms);
		/* Let Diagnostic module know that we have placed a call to EE_BlockWrite */
		vs_write_rslt = TRUE;
	}
	else {
		/* Not a valid vs LID service. Write is not succesful. */
		vs_write_rslt = FALSE;
	}
	return(vs_write_rslt);
}

/*********************************************************************************/
/*                          vsF_Diag_DTC_Control                                 */
/*********************************************************************************/
/* Input parameters:                                                             */
/* vs_DTC_cntrl: Control input to set or clear a chosen DTC by a diagnostic      */
/* command.                       												 */
/* vs_DTC: Selected vent fault which will be activated.                          */
/* Vent_Location: Selected vent location where the fault will be set.            */
/*                                                                               */
/* This function is used to mature or de-mature the selected vent fault in       */
/* diagnostic mode.                												 */
/*                                                                               */
/* Calls from                                                                    */
/* --------------                                                                */
/* ApplDescProcessRoutineControl :                                               */
/* vsF_Diag_DTC_Control(Vs_Flt_Type_e,Vs_DTC_cntrl, Vent_Location) 				 */
/*                                                                               */
/* Calls to                                                                      */
/* --------                                                                      */
/* no calls                                                                      */
/*																				 */
/*********************************************************************************/
@far void vsF_Diag_DTC_Control(Vs_DTC_Control_e Vs_DTC_cntrl, u_8Bit vs_DTC, Vs_Location_e Vent_Location)
{
	/* Local variable(s) */
	u_8Bit vs_set_DTC = vs_DTC;    // DTC to be set

	if (Vs_DTC_cntrl == VS_SET_DTC) {
		/* Update fault status */
		vs_prm_set[Vent_Location].flt_stat_c = vs_set_DTC;
	}
	else {
		/* Clear all faults */
		vs_prm_set[Vent_Location].flt_stat_c = VS_NO_FLT_MASK;
		
		/* Clear fault mature count */
		vs_prm_set[Vent_Location].flt_mtr_cnt = 0;
	}
}

/*********************************************************************************/
/*                          vsF_Modify_StartUp_Tm                                */
/*********************************************************************************/
/*                                                                               */
/* Function for EOL tests to modify the vent motor start up time to 0.           */
/* The vent will not go to high level for 2 seconds if this function is called.  */
/*                                                                               */
/* Return value: 0 - success                                                     */
/* Return value: 1 - fail                                                        */
/*                                                                               */
/*********************************************************************************/
@far u_16Bit vsF_Modify_StartUp_Tm(void)
{
	/* Local variable(s) */
	u_16Bit vs_result_c;

	if (diag_eol_io_lock_b == TRUE) {
		/* Set motor start up time to zero for EOL tests. */
		Vs_flt_cal_prms.start_up_tm = 0;
		
		/* Update the result */
		vs_result_c = Vs_flt_cal_prms.start_up_tm ;
	}
	else {
		/* Result is not valid */
		vs_result_c = VS_FAILED_RSLT;
	}
	return(vs_result_c);
}

/*********************************************************************************/
/*                        vsF_Get_Actv_outputNumber                         	 */
/*********************************************************************************/
/* Function Name: @far u_8Bit vsF_Get_Actv_outputNumber(void)			 		 */
/* Type: Global Function													     */
/* Returns: u_8Bit (number of active vent outputs)                     			 */
/* Parameters: None																 */
/* Description: This function returns the number of active vent outputs (0,1,2). */
/*              Note: Vent has to be turned on at low or high level to be        */
/*              considered for battery compensation.	 						 */
/*																			     */
/* Calls to: None																 */
/* Calls from:																	 */	
/*    BattVF_Compensation_Calc()												 */
/*																				 */  
/*********************************************************************************/
@far u_8Bit vsF_Get_Actv_outputNumber(void)
{
	u_8Bit vs_actv_number_c = VS_ZERO_ACTV;	// Number of active vents
	u_8Bit lc1 = 0;							// loop counter
	
	for (lc1=0; lc1 <VS_LMT; lc1++)
	{
		/* Check each vent output state and level (compensation is done for low and high level */
		if(  (vs_Flag1_c[lc1].b.state_b == TRUE) && (vs_prm_set[lc1].crt_lv_c != VL0) ){
			/* This vent is active. Increment the active output counter */
			vs_actv_number_c++;			
		}else{
			/* This vent is not active at the highest level. Do not increment the active vent counter */
		}
	}
	return(vs_actv_number_c);
}

/*********************************************************************************/
/*                vsF_Get_Fault_Status(u_8Bit vs_choice)                       	 */
/*********************************************************************************/
/* Function Name: @far u_8Bit vsF_Get_Fault_Status(void)			 		 	 */
/* Type: Global Function													     */
/* Returns: u_8Bit (selected vent fault status)                     			 */
/* Parameters: selected vent									 				 */
/* Description: This function returns the selected vent fault status.			 */
/*																			     */
/* Calls to: None																 */
/* Calls from:																	 */	
/*    diagF_DCX_OutputTest()												 	 */
/*																				 */  
/*********************************************************************************/
@far u_8Bit vsF_Get_Fault_Status(u_8Bit vs_choice)
{
	/* Local variable(s) */
	u_8Bit vs_chosen = vs_choice;
	
	/* Return the chosen vent fault status */
	return(vs_prm_set[vs_choice].flt_stat_c);
}
