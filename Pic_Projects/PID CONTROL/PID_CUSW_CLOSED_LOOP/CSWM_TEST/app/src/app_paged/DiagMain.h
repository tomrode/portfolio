#ifndef DIAGMAIN_H_
#define DIAGMAIN_H_
/*****************************************************************************************************
*  (c) Copyright 2011 Continental Automotive Systems, all rights reserved
*
*  All material contained herein is CONTINENTAL CONFIDENTIAL and PROPRIETARY.
*  Any reproduction of this material without written consent from 
*  Continental AG is strictly forbidden.                                  
*
*  Filename:     diagmain.h
*
*  Created_by:   Harsha Yekollu
*
*  Date_created: 01/19/2011
*
*  Description:  Main Header file for UDS Diagnostics. 
*
*  Revision_history:
*
*  MKS ID       Author         Date      Description of Change(s)
*  --------  -------------  --------  --------------------------------------
*  xxxxxxx   H. Yekollu     01/19/11  Initial creation for FIAT CUSW MY12 Program
*  xxxxxxx   H. Yekollu     01/19/11  
*
* 
* 
* 
******************************************************************************************************/
#undef EXTERN 

#ifdef DIAGMAIN_C
#define EXTERN 
#else
#define EXTERN extern
#endif

/* Enum DIAG_RESPONSE */
enum DIAG_RESPONSE
{
  NO_RESPONSE,
  RESPONSE_OK,
  RESPONSE_PENDING,
  BUSY_REPEAT_REQUEST,
  REQUEST_OUT_OF_RANGE,
  LENGTH_INVALID_FORMAT,
  CONDITIONS_NOT_CORRECT,
  SUBFUNCTION_NOT_SUPPORTED,
//  TEMPERATURE_TOO_HIGH, //Not used Info 788
  SECURITY_ACCESS_DENIED,
  INVALID_KEY,
  EXCEED_NO_OF_ATTEMPTS,
  TIMEDELAY_NOT_EXPIRED,
  REQUEST_SEQUENCE_ERROR
};
/* Global Define */
#define SIZE_OF_BLOCKNUMBER_K       2

EXTERN union char_bit  diag_status_flags_c;
#define diag_session_active_b       diag_status_flags_c.b.b0
//#define diag_disable_ntc_b          diag_status_flags_c.b.b1 //Not used
//#define diag_rst_stwheel_EFUSE_b    diag_status_flags_c.b.b2 
#define update_chrono_stack_b       diag_status_flags_c.b.b3
#define update_historical_stack_b   diag_status_flags_c.b.b4
#define update_hist_int_record_b    diag_status_flags_c.b.b5
#define update_dtc_status_info_b    diag_status_flags_c.b.b6
#define comn_ctrl_disable_b         diag_status_flags_c.b.b7 //This flag sets to 1, when ever comn ctrl disables the Tx msg of CSWM.

EXTERN union char_bit  diag_rc_lock_flags_c;
#define diag_rc_all_op_lock_b			    diag_rc_lock_flags_c.b.b0	/* lock for all output switch requests */
#define diag_rc_all_heaters_lock_b    diag_rc_lock_flags_c.b.b1   /* Lock for all heater outputs including HSW */
#define diag_rc_all_vents_lock_b		  diag_rc_lock_flags_c.b.b2	/* Lock for all vent outputs */

/* Initialization routine for Diagnostic Main module */
extern @far void diagF_Init(void);
/* Calls in Main time slice will be looking for write by Lid tasks */
@far void diagF_CyclicWriteTask(void);
#endif /*DIAGMAIN_H_*/
