#define DIAGSEC_C

/*****************************************************************************************************
*  (c) Copyright 2011 Continental Automotive Systems, all rights reserved
*
*  All material contained herein is CONTINENTAL CONFIDENTIAL and PROPRIETARY.
*  Any reproduction of this material without written consent from 
*  Continental AG is strictly forbidden.                                  
*
*  Filename:     DiagSec.c
*
*  Created_by:   Harsha Yekollu
*
*  Date_created: 01/19/2011
*
*  Description:  Main file for UDS Diagnostics -- Security Access. 
*
*  Revision_history:
*
*  MKS ID       Author         Date      Description of Change(s)
*  --------  -------------  --------  --------------------------------------
*  xxxxxxx   H. Yekollu     01/19/11  Initial creation for FIAT CUSW MY12 Program
*  xxxxxxx   H. Yekollu     01/19/11  
*  75322	 H. Yekollu	    10/06/11   Cleanup the code.
* 
* 
* 
******************************************************************************************************/
/*****************************************************************************************************
* 									DIAGSEC
* This module handles the UDS Security Access - Service 0x27.
* Covers the following.
* 1. Request Seed Implementation
* 2. Process Key Implementation
* 
*****************************************************************************************************/

/*****************************************************************************************************
*     								Include Files
******************************************************************************************************/
/* CAN_NWM Related Include files */
#include "il_inc.h"
#include "v_inc.h"
#include "desc.h"
#include "appdesc.h"

#include "TYPEDEF.h"
#include "main.h"
#include "DiagMain.h"
#include "DiagSec.h"
#include "canio.h"

#include "heating.h"
#include "Venting.h"
#include "StWheel_Heating.h"
#include "Temperature.h"
#include "Internal_Faults.h"
#include "AD_Filter.h"
#include "BattV.h"
#include "mw_eem.h"
#include "mc9s12xs128.h"
#include "fbl_def.h"

/* Autosar include Files */
#include "Mcu.h"
#include "Port.h"
#include "Dio.h"
#include "Gpt.h"
#include "Pwm.h"
#include "Adc.h"
#include "Wdg.h"

/*****************************************************************************************************
*     								LOCAL ENUMS
******************************************************************************************************/
/*****************************************************************************************************
*     								LOCAL #DEFINES
******************************************************************************************************/


/*****************************************************************************************************
*     								LOCAL VARIABLES
******************************************************************************************************/
static u_temp_l DataLen;
static unsigned char Diag_Request_c,temp_c;
static u_temp_l RespLengthDefault;
static u_temp_l DataLength;
	
 static enum DIAG_RESPONSE Response;
/*****************************************************************************************************
*     								LOCAL FUNCTION CALLS
******************************************************************************************************/
/* This function will be caluclating the seed for gaining the security access */
void diagSecLF_Calculate_Seed(void);

/* This function starts counting the 1sec delay time between seed generation and key response. */
//void diagSecLF_Start_1sec_key_gen_Timer(void);

/* This function starts counting the 10sec delay timer incase the FAA is set to TRUE. */
void diagSecLF_Start_10sec_delay_Timer(void);

unsigned long diagSecLF_Key_alg(unsigned long Seed, unsigned long Constant_1, unsigned long Constant_2);
extern @far void diagF_DiagResponse(enum DIAG_RESPONSE Response, u_temp_l Length, DescMsgContext* pMsgContext);
void diagSecF_RequestSeed(DescMsgContext* pMsgContext);
void diagSecF_ProcessKey(DescMsgContext* pMsgContext);
/***************************************************************************************
 *  FUNCTION:       diagSecF_Main_Init                                                 *
 *  FILENAME:       M:\SCM_DCX_CSWM_MY_12\Application_Paged\src\DiagSec.c              *
 *  PARAMETERS:     (none)                                                             *
 *  DESCRIPTION:    Use for initializing Security                                      *
 *  RETURNS:        (none)                                                             *
 ***************************************************************************************/
@far void diagSecF_Main_Init(void)
{
	/* Security Algorithm Initialization DPRS 11-75, DPRS 11-76, DPRS 11-77. */
    //EE_BlockRead(EE_SEC_FAA, &diag_sec_FAA_c);
	diag_sec_FAA_c = 0;
	/* Check to see there are no Failed access attempts.                                                                                                                                        */
	/* If no FAA, set the security state to Delay timer inactive state. In this situation, if external tool requests for Seed, module responds immediatly.                                      */
	/* If FAA set, security state to be in Delay timer active state. In delay timer active mode, if external tool requests with in 10sec of time a seed request module sends negative response. */

//	if (diag_sec_FAA_c == 0)
//	{
		/* DPRS 11-76:                                                                 */
		/* Set the Security access state for Level 1 to Delay timer Inactive condition */
		diag_sec_access1_state_c = ECU_LOCKED_SEC_DELAY_TIMER_INACTIVE;

		/* Set the Security access state for Level 5 to Delay timer Inactive condition */
		diag_sec_access5_state_c = ECU_LOCKED_SEC_DELAY_TIMER_INACTIVE;

		/* clear the 10sec counter. */
		delay_timer_active_b = FALSE;
//	}
//	else
//	{
//		/* DPRS 11-77:                                                               */
//		/* Set the Security access state for Level 1 to Delay Timer active condition */
//		diag_sec_access1_state_c = ECU_LOCKED_SEC_DELAY_TIMER_ACTIVE;
//		/* Set the Security access state for Level 5 to Delay Timer active condition */
//		diag_sec_access5_state_c = ECU_LOCKED_SEC_DELAY_TIMER_ACTIVE;
//		/* start the 10sec counter. */
//		delay_timer_active_b = TRUE;
//	}
		
		
}	

/***************************************************************************************
 *  FUNCTION:       diagSecF_DefSession_Init                                           *
 *  FILENAME:       M:\SCM_DCX_CSWM_MY_12\Application_Paged\src\DiagSec.c              *
 *  PARAMETERS:     (none)                                                             *
 *  DESCRIPTION:    Use for Default Diag session Init                                  *
 *  RETURNS:        (none)                                                             *
 ***************************************************************************************/
@far void diagSecF_DefSession_Init(void)
{
//	/* If the Security access is unlocked and control comes here by S3 timeout or requested to be at session default */
//	if (diag_sec_access1_state_c == ECU_UNLOCKED)
//	{
		/*  security access state should be at Delay timer Inactive condition */
		diag_sec_access1_state_c = ECU_LOCKED_SEC_DELAY_TIMER_INACTIVE;
		/* Set the Security access state for Level 5 to Delay timer Inactive condition */
		diag_sec_access5_state_c = ECU_LOCKED_SEC_DELAY_TIMER_INACTIVE;
//	}
//	else
//	{
//		/* We are in Default session. Check the Failed Access Attempt flag */
//		if (diag_sec_FAA_c == FAILED_ACCESS_ATTEMPT_SET)
//		{
//			/*  security state should be at Delay Timer active condition */
//			/* Start 10sec timer                                         */
//			diag_sec_access1_state_c = ECU_LOCKED_SEC_DELAY_TIMER_ACTIVE;
//			/* Set the Security access state for Level 5 to Delay Timer active condition */
//			diag_sec_access5_state_c = ECU_LOCKED_SEC_DELAY_TIMER_ACTIVE;
//			/* start the 10sec counter. */
//			delay_timer_active_b = TRUE;
//		}
//		else
//		{
//			/*  security access state should be at Delay timer Inactive condition */
//			diag_sec_access1_state_c = ECU_LOCKED_SEC_DELAY_TIMER_INACTIVE;
//			/* Set the Security access state for Level 5 to Delay timer Inactive condition */
//			diag_sec_access5_state_c = ECU_LOCKED_SEC_DELAY_TIMER_INACTIVE;
//		}
//	}
	/* Clear the 1sec and 10sec timer */
//	diag_1sec_timer_c = 0;
	diag_10sec_timer_w = 0x0000; 
}

/***************************************************************************************
 *  FUNCTION:       diagSecF_DefSession_Init                                           *
 *  FILENAME:       M:\SCM_DCX_CSWM_MY_12\Application_Paged\src\DiagSec.c              *
 *  PARAMETERS:     (none)                                                             *
 *  DESCRIPTION:    Use for Extended Diag session Init                                 *
 *  RETURNS:        (none)                                                             *
 ***************************************************************************************/
@far void diagSecF_ExtSession_Init(void)
{
	//Extended Diagnostic session. To be filled the requirements
	//diag_session_active_b = 1;
	/* If the Security access is unlocked and control comes here by S3 timeout or requested to be at session default */
//	if (diag_sec_access5_state_c == ECU_UNLOCKED)
//	{
		/*  security access state should be at Delay timer Inactive condition */
		diag_sec_access1_state_c = ECU_LOCKED_SEC_DELAY_TIMER_INACTIVE;
		/* Set the Security access state for Level 5 to Delay timer Inactive condition */
		diag_sec_access5_state_c = ECU_LOCKED_SEC_DELAY_TIMER_INACTIVE;
//	}
//	else
//	{
//		/* We are in Default session. Check the Failed Access Attempt flag */
//		if (diag_sec_FAA_c == FAILED_ACCESS_ATTEMPT_SET)
//		{
//			/*  security state should be at Delay Timer active condition */
//			/* Start 10sec timer                                         */
//			diag_sec_access1_state_c = ECU_LOCKED_SEC_DELAY_TIMER_ACTIVE;
//			/* Set the Security access state for Level 5 to Delay Timer active condition */
//			diag_sec_access5_state_c = ECU_LOCKED_SEC_DELAY_TIMER_ACTIVE;
//			/* start the 10sec counter. */
//			delay_timer_active_b = TRUE;
//		}
//		else
//		{
//			/*  security access state should be at Delay timer Inactive condition */
//			diag_sec_access1_state_c = ECU_LOCKED_SEC_DELAY_TIMER_INACTIVE;
//			/* Set the Security access state for Level 5 to Delay timer Inactive condition */
//			diag_sec_access5_state_c = ECU_LOCKED_SEC_DELAY_TIMER_INACTIVE;
//		}
//	}
	/* Clear the 1sec and 10sec timer */
//	diag_1sec_timer_c = 0;
	diag_10sec_timer_w = 0x0000; 
}

/***************************************************************************************
 *  FUNCTION:       diagSecF_CyclicTask                                                *
 *  FILENAME:       M:\SCM_DCX_CSWM_MY_12\Application_Paged\src\DiagSec.c              *
 *  PARAMETERS:     (none)                                                             *
 *  DESCRIPTION:    Cyclic Tasks to perform on Security                                *
 *  RETURNS:        (none)                                                             *
 ***************************************************************************************/
@far void diagSecF_CyclicTask(void)
{

	/* Calculate the seed value */
	diagSecLF_Calculate_Seed();
	/* Start one second Key generation timer */
//	diagSecLF_Start_1sec_key_gen_Timer();
	/* 10 sec delay time counter */
	diagSecLF_Start_10sec_delay_Timer();
	
}	

/***************************************************************************************
 *  FUNCTION:       diagSecLF_Calculate_Seed                                           *
 *  FILENAME:       M:\SCM_DCX_CSWM_MY_12\Application_Paged\src\DiagSec.c              *
 *  PARAMETERS:     (none)                                                             *
 *  DESCRIPTION:    Calculate the random seed                                          *
 *  RETURNS:        (none)                                                             *
 ***************************************************************************************/
void diagSecLF_Calculate_Seed(void)
{

	unsigned long seed_x_l;
	unsigned long seed_y_l;

	if (seed_generated_b)
	{
		seed_generated_b = 1;
	}
	else
	{
		/* Generating the seed.                           */
		/* Take the free running timer into local variable */
		seed_x_l = _TCNT.Word;
		seed_y_l = _TCNT.Word;

		diag_seed_l.l = seed_x_l;

		diag_seed_l.l = diag_seed_l.l << 16;
		diag_seed_l.l |= seed_x_l * seed_y_l;
	}
}

/***************************************************************************************
 *  FUNCTION:       diagSecLF_Start_1sec_key_gen_Timer                                 *
 *  FILENAME:       M:\SCM_DCX_CSWM_MY_12\Application_Paged\src\DiagSec.c              *
 *  PARAMETERS:     (none)                                                             *
 *  DESCRIPTION:    This function starts counting the 1sec delay time between seed     *
 *                  seed generation and key response once receives the seed..          *              
 *  RETURNS:        (none)                                                             *
 ***************************************************************************************/
//void diagSecLF_Start_1sec_key_gen_Timer(void)
//{
//	/* Check to see Key timer start flag set*/
//	if (gen_key_timer_b)
//	{
//		/* Check 1sec is crossed? */
//		/* If not increment the timer to reach 1sec */
//		/* If crossed, set the timeout flag */
//		if (diag_1sec_timer_c > DELAY_TIMER_1SEC)
//		{
//			diag_1sec_timer_c = 0;
//			gen_key_timer_b = FALSE; //Counter incrementation flag ends here. 
//			gen_key_timeout_b = TRUE; //1sec timer reached. set the timeout flag.
//		}
//		else
//		{
//			diag_1sec_timer_c++; //1 sec not reahed. Increment the counter.
//			gen_key_timeout_b = FALSE; //1sec timer not reached. No timeout at this time.
//		}
//	}
//	else
//	{
//		//Do nothing.
//	}
//
//}

/***************************************************************************************
 *  FUNCTION:       diagSecLF_Start_10sec_delay_Timer                                  *
 *  FILENAME:       M:\SCM_DCX_CSWM_MY_12\Application_Paged\src\DiagSec.c              *
 *  PARAMETERS:     (none)                                                             *
 *  DESCRIPTION:    This Function will get executed when ever a wrong key was sent out *
 *                  for trying to get the security access.This function waits user for * 
 *                  10sec before requesting another seed.                              *             
 *  RETURNS:        (none)                                                             *
 ***************************************************************************************/

/* diagLF_Start_10sec_delay_Timer	*/
/* This Function will get executed when ever a wrong key was sent out for trying to get the security access. */
/* This function waits user for 10sec before requesting another seed.                                        */
void diagSecLF_Start_10sec_delay_Timer(void)
{
	/* DPRS 11-71:                                                                                                                                                    */
	/* If delay timer is active means, 10sec counter should start running. In between ext tool requests Seed or sends Key, ecu responds with delay timer not expired. */
	if (delay_timer_active_b)
	{
		if (diag_10sec_timer_w > DELAY_TIMER_10SEC)
		{
			diag_10sec_timer_w = 0;
			delay_timer_active_b = FALSE;
			/* DPRS 11-87: */
			diag_sec_access1_state_c = ECU_LOCKED_SEC_DELAY_TIMER_INACTIVE;
			diag_sec_access5_state_c = ECU_LOCKED_SEC_DELAY_TIMER_INACTIVE;
		}
		else
		{
			diag_10sec_timer_w++;
		}
	}
	else {
		//Do nothing
	}
}

void diagSecF_RequestSeed( DescMsgContext* pMsgContext)
{
	/* Check to see the other security level state is not in LOCKED SEED GENERATED state.                   */
	/* If Security Level 1 state is in LOCKED SEED GENERATED state, then discard the new seed request. Else */
	/* start processing the new seed request for level 5..                                                  */
	if (diag_sec_access1_state_c != ECU_LOCKED_SEED_GENERATED)
	{
		    /* Check to see the delay timer is active. Enter inside logic only if Delay timer is inactive. */
		    if (diag_sec_access5_state_c != ECU_LOCKED_SEC_DELAY_TIMER_ACTIVE)
		    {
			         if (diag_sec_access5_state_c == ECU_UNLOCKED)
			         {
				            /* DPRS 11-97:                                                                                                 */
				            /* Seed generated and ECU is unlocked. As we got another request do not generate new seed, send Zeros as seed. */
				            pMsgContext->resData[0] = 0x00;
				            pMsgContext->resData[1] = 0x00;
				            pMsgContext->resData[2] = 0x00;
				            pMsgContext->resData[3] = 0x00;

				            DataLength = 4;
				            Response = RESPONSE_OK;
			         }
			         else
			         {
				            if (diag_sec_access5_state_c == ECU_LOCKED_SEED_GENERATED)
				            {
                					/* DPRS 11-91:                                                                                                                          */
                					/* Already seed generated and waiting for Key. As we got another seed request, send positive response and send the last generated seed. */
                					memcpy((unsigned char*)(pMsgContext->resData), (unsigned char*)&diag_seed_generated_l, 4);
                					DataLength = 4;
                					Response = RESPONSE_OK;
				            }
				            else
				            {
                					/* DPRS 11-81:                                                                             */
                					/* Start counting the 1sec timer. Ext tool must be respond with in 1 sec timer to send Key */
                					/* Also clear the timeout flag as just we requested */
                					//gen_key_timer_b = TRUE;
                					//gen_key_timeout_b = FALSE;
                					memcpy((unsigned char*)(pMsgContext->resData), (unsigned char*)&diag_seed_l, 4);
                					/* DPRS 11-91:                                                                                                                                                                   */
                					diag_seed_generated_l = diag_seed_l;
                					/* DPRS 11-80: */
                					diag_sec_access5_state_c = ECU_LOCKED_SEED_GENERATED;
                					DataLength = 4;
                					Response = RESPONSE_OK;
			              }
			          }
		    }	          
		    else
		    {
		    	/* DPRS 11-85:                                            */
		    	/* Delay Timer is active.Send timer not expired response. */
		    	DataLength = 1;
		    	Response = TIMEDELAY_NOT_EXPIRED;
		    }
	}
	else
	{
		/* DPRS 11-93:                                                                                                                             */
		/* Received the new seed request when the other security level state is in LOCKED SEED GENERATED condition.. discard the new seed request. */
		DataLength = 1;
		Response = REQUEST_SEQUENCE_ERROR;
	}
	//diagF_DiagResponse(Response, DataLength+RespLengthDefault, pMsgContext);

}

void diagSecF_ProcessKey(DescMsgContext* pMsgContext)
{
	/* Check to see the other security level state is not in LOCKED SEED GENERATED state.                   */
	/* If Security Level 1 state is in LOCKED SEED GENERATED state, then discard the new key response. Else */
	/* start processing the new key response for level 5..                                                  */
	if (diag_sec_access1_state_c != ECU_LOCKED_SEED_GENERATED)
	{
		if (diag_sec_access5_state_c == ECU_LOCKED_SEED_GENERATED )
		{
//			if (gen_key_timeout_b == FALSE)
//			{
				/* Responding with Key with in 1sec after generating seed. */
				/* Clear the key timer and timeout flag, and also the timer */
//				gen_key_timer_b = FALSE;
//				gen_key_timeout_b = FALSE;
//				diag_1sec_timer_c = 0;
				
				/* Calculate the Key. If tool sends valid key response, set security access state to ECU_UNLOCKED. */
				/* IF tool sends invalid key send an Invalid key                                                   */
				/* Added the LX series (PWR_NET) Chrysler Security Algorithm key calculation SecMod1 */
			   	/* PowerNet architecture: LX series */
			   	secMod1_var1_l = SEC_MOD1_CONST1;	/* Initiliaze SecMod1 variable1 */
			   	secMod1_var2_l = SEC_MOD1_CONST2;   /* Initiliaze SecMod1 variable2 */
			   		
			   	hsm_key_l.l = diagSecLF_Key_alg(diag_seed_generated_l.l, secMod1_var1_l, secMod1_var2_l);
				
				memcpy((unsigned char *)&tool_key_l, &(pMsgContext->reqData[0]), 4);
				if (hsm_key_l.l == tool_key_l.l)
				{
					/*Nothing to fill as Positive response */
					/* postive response is 67 02 */
					/* positive resp 67 filled by automatically and rest is only one byte */
					DataLength = 0;
					RespLengthDefault = 1;
					
					/* DPRS 11-88: */
					Response = RESPONSE_OK;
					/* Set the security access state to ECU Unlocked */
					diag_sec_access5_state_c = ECU_UNLOCKED;
					/* Clear the FAA */
					diag_sec_FAA_c = 0;
				}
				else
				{
					/* DPRS 11-92:                       */
					/* Not a valid key. Send Invalid key */
					DataLength = 1;
					Response = INVALID_KEY;
					
					diag_sec_FAA_c++;
					
					/* Failure attempts reached 2 times in same IGN cycle? */
					if(diag_sec_FAA_c >= 2)
					{
						/* and set, the security access state to delay timer active */
						diag_sec_access5_state_c = ECU_LOCKED_SEC_DELAY_TIMER_ACTIVE;
						/* Set the FAA to TURE. */
						//diag_sec_FAA_c = FAILED_ACCESS_ATTEMPT_SET;
						/* start the 10sec counter. */
						delay_timer_active_b = TRUE;
					}
					else
					{
						/* MISRA */
					}
				}
//			}
//			else
//			{
//				/* DPRS 11-89:                                           */
//				/* One second Generation key timed out. Send Invalid key */
//				
//				DataLength = 1;
//				Response = INVALID_KEY;
//				/* and set, the security access state to delay timer active */
//				diag_sec_access5_state_c = ECU_LOCKED_SEC_DELAY_TIMER_ACTIVE;
//				/* Set the FAA to TURE. */
//				//diag_sec_FAA_c = FAILED_ACCESS_ATTEMPT_SET;
//				/* start the 10sec counter. */
//				delay_timer_active_b = TRUE;
//			}
			/* Read the Failed Access Attempt of Security Access from EEPROM. */
			/* If the stored FAA is different from the new, store in EEPROM else discard. */
		    //EE_BlockWrite(EE_SEC_FAA, &diag_sec_FAA_c);
		}
		else
		{
			if (diag_sec_access5_state_c == ECU_LOCKED_SEC_DELAY_TIMER_ACTIVE)
			{
				/* DPRS 11-86:                                                                                                   */
				/* Delay Timer is active. Security access state is not in Seed Generated state..Send timer not expired response. */
				DataLength = 1;
				Response = TIMEDELAY_NOT_EXPIRED;
			}
			else
			{
				/* DPRS 11-78:                                                                                                            */
				/* Delay Timer is not-active. Security access state is not in Seed Generated state..Send Request sequence error response. */
				DataLength = 1;
				Response = REQUEST_SEQUENCE_ERROR;
			}
		}
	}
	else
	{
		/* DPRS 11-94:                                                                                                                                        */
		/* Received the new key response when the other security level state is in LOCKED SEED GENERATED condition.. discard the key response for this level. */
		DataLength = 1;
		Response = REQUEST_SEQUENCE_ERROR;
	}
	//diagF_DiagResponse(Response, DataLength+RespLengthDefault, pMsgContext);

}

/******************************************************************************
* Name         :  diagLF_security_alg
* Called by    :  ApplDescProcessSecurityAccess
* Preconditions:  None
* Parameters   :  unsigned long Seed, unsigned long Constant_1, unsigned long Constant_2
* Return code  :  Chrysler Security Algorithm for LX-series (PWR_NET) (for Security Mode 1: For protected writes)
* Description  :  This function is provided by Chrysler (James P. Savich - Core Vehicle Diagnostics - Team Leader)
*                 This function calculates the security key for HSM (for security mode 1: For protected writes)
*                 Added by CK on 12.01.2008
* 
******************************************************************************/
unsigned long diagSecLF_Key_alg(unsigned long Seed, unsigned long Constant_1, unsigned long Constant_2)
{

	unsigned long Key;
		

	//Re-arrange original Seed from 0xGGHHIIJJ to modified Seed 0xHHGGJJII
	Key = ((Seed << 8) & 0xFF000000) + ((Seed >> 8) & 0x00FF0000) +
		  ((Seed << 8) & 0x0000FF00) + ((Seed >> 8) & 0x000000FF);

	//(Shift modified Seed up 11 times) + (Shift modified Seed down 22 times)
	Key = ((Key << 11) + (Key >> 22)) & 0xFFFFFFFF;

	//Constant_2 and with original Seed
	Constant_2 = (Constant_2 & Seed) & 0xFFFFFFFF;

    //Exclusive OR operations
    Key = (Key ^ Constant_1 ^ Constant_2) & 0xFFFFFFFF;
 

	return Key;
}

/*  ********************************************************************************
 * Function name:ApplDescOnTransitionSecurityAccess
 * Description:Notification function for state change of the given state group, defined by
 * CANdelaStudio.
 * Returns:  nothing
 * Parameter(s):
 *   - newState:
 *       - The state which will be set.
 *       - Access type: read
 *   - formerState:
 *       - The current state of this state group.
 *       - Access type: read
 * Particularitie(s) and limitation(s):
 *   - The function "DescProcessingDone" may not be called.
 *   - The function "DescSetNegResponse" may not be called.
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescOnTransitionSecurityAccess(DescStateGroup newState, DescStateGroup formerState)
{
  /* <<TBD>> Remove this comment once you have completely implemented this function!!! */
   /* This is only a notification function. Using the "formerState" and the "newState" 
   * parameter you can distinguish the different transitions for this state group.
   */
  /* Avoids warnings */
  DESC_IGNORE_UNREF_PARAM(newState);
  DESC_IGNORE_UNREF_PARAM(formerState);

}
/*  ********************************************************************************
 * Function name:ApplDescRequestLevel1Seed (Service request header:$27 $1 )
 * Description:

 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescRequestLevel1Seed(DescMsgContext* pMsgContext)
{
  
  /* Default length of 2 bytes */
  RespLengthDefault = SIZE_OF_BLOCKNUMBER_K;
      
	/* Is the requested datalength is correct? */
	if (pMsgContext->reqDataLen == 1)
	{   
      diagSecF_RequestSeed (pMsgContext);
	}
  else
  {
      Response = LENGTH_INVALID_FORMAT; 
       
  }
  diagF_DiagResponse(Response, DataLength+RespLengthDefault, pMsgContext);      		
}
/*  ********************************************************************************
 * Function name:ApplDescSendLevel1Key (Service request header:$27 $2 )
 * Description:SendKey for download procedure

 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescSendLevel1Key(DescMsgContext* pMsgContext)
{

  /* Default length of 2 bytes */
  RespLengthDefault = SIZE_OF_BLOCKNUMBER_K;
      
	/* Is the requested datalength is correct? */
	if (pMsgContext->reqDataLen == 5)
	{   
      diagSecF_ProcessKey (pMsgContext);  
	}
  else
  {
      Response = LENGTH_INVALID_FORMAT; 
      
  }
  diagF_DiagResponse(Response, DataLength+RespLengthDefault, pMsgContext);       	
}

/*  ********************************************************************************
 * Function name:ApplDescRequestVINRequestSeed (Service request header:$27 $5 )
 * Description:RequestSeed for VIN and SinCom factory  procedure

 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescRequestVINRequestSeed(DescMsgContext* pMsgContext)
{
    
  /* Default length of 2 bytes */
  //RespLengthDefault = SIZE_OF_BLOCKNUMBER_K;
      
	/* Is the requested datalength is correct? */
  if(pMsgContext->reqData[0] < 0xFF)
  {
   
      diagSecF_RequestSeed (pMsgContext);
  }
  else
  {
      Response = LENGTH_INVALID_FORMAT;    
  }
  diagF_DiagResponse(Response, DataLength/*+RespLengthDefault*/, pMsgContext);       		
}


/*  ********************************************************************************
 * Function name:ApplDescSendVINSendKey (Service request header:$27 $6 )
 * Description:

 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescSendVINSendKey(DescMsgContext* pMsgContext)
{

  /* Default length of 2 bytes */
  //RespLengthDefault = SIZE_OF_BLOCKNUMBER_K;
      
	/* Is the requested datalength is correct? */
  if(pMsgContext->reqData[0] < 0xFF)
  {
      diagSecF_ProcessKey (pMsgContext);  
  }
  else
  {
      Response = LENGTH_INVALID_FORMAT; 
  }
        diagF_DiagResponse(Response, DataLength/*+RespLengthDefault*/, pMsgContext);       	
}
