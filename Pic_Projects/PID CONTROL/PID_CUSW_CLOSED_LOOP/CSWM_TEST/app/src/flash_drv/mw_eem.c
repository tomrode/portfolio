
/******************************************************************************
*   (c) Copyright Continental AG 2008
*   All Rights Reserved.
*   Continental AG Confidential Proprietary
*   The Copyright symbol does not also indicate public availability
*   or publication.
*
*   Filename:   mw_eem.c 
*
*   Created_by: L. Kaplan
*       
*   Date_created: 03/25/2008
*
*   Description: EEPROM manager
*
*
    *******************************************************************************
*   Revision History:
*
*   Date          CQ#           Author    
*   MM-DD-YY      oem00000000   Initials   Description of change.
*   -----------   -----------   --------   ------------------------------------
*   03-25-08      oem00023671   LK         EEPROM driver ported from TIPM
*   04-22-08      oem00023840   AN         Some fixes are made to make it work
*   06-26-08      oem00024187   LK         EEPROM changes
*   07-21-08      oem00024249   LK         EEPROM backup sections
*   07-23-08      oem00024258   VS         Issues fixing after S0B release testing
*   09-29-08      --            CK         EEPROM default updates regarding to internal faults
*   10-30-08      --            CK         EEPROM changes regarding to battery compensation 
*   11-11-08      --            HY         6 more bytes added to DTC Status 
*   11-13-08      --            CK         Updated PCB NTC Maximum threshold to 100C (173 A/D
*                                          affective with S0B release (084600)
*   12-03-08      --            CK         Updated battery compensation thresholds. 
*                                                                                  Updated Application and Bootloader Software versions.
*   12-05-08      --            HY         Updated default eep map with powernet partnumbers
*   01-08-09      --            CK         Updates regarding to steering wheel low side off delay 
*   01-22-09      --            CK         PC-Lint fixes.
*   02-03-09      --            CK         Update EE_LoadDefaults function. Load defaults for VIN ODO value and Limit
*   02-23-09      --            CK         We read EE_VIN_ODO_COUNTER before we copy its default value. 
*                                          If it is 0xFF we have a brand new module, default can be copied.
*                                                                                  If it is not 0xFF, this means we are re-flashing. We do not copy the default
*                                                                                  to avoid updating VIN original mistakenly.   
*   04-08-09      28078         CK         EEPROM updates regarding to Seat heaters and STW current calculation.
*                                          Updated sEEPConstants structure and EE_LoadDefaults function.
*   04-14-09      --            HY         Added ROE_CFG,ECU_CFG default values for Power Net series.
*                                          Heater NTC feedback timeouts changed from 0, 12, 2, 2 to 0, 18, 2, 0
*   04-28-09      --            HY         Actual timeout for HL1 changed from 15min to 30min.
*   06-15-09      --            HY         Maximum timeout for WL2(Steering wheel PWM level2) increased from 10 min to 20min
*                                          SW Version and EEP version changed to 09.25 
*   08-21-2009   MKS CR32456    CC         Function EE_LoadDefaults(void) To add different thresholds to be different with Front and Rear seats and Defines
*   02-17-2010   MKS 47356      HY         New variables added to have last known Load shed 2/4 odo stamps
*   04-14-2010   MKS 50039      HY         Set of 136 variables (17 each for 8 differnt vehicle lines )added for new feature.
*                                          Previously used 17 parameters (All vehicle lines uses same calibration) commented and made that space as reserved 
*   04-15-2010   MKS 50111      HY         9 Individual Heater threshold Blocks moved to 1 signle block
*                                          6 Individual Relay threshold Blocks moved to 1 single block
*   04-16-2010   MKS 50168      HY         Added NTC Failsafe (5 Byte Info)
*   05-28-2010   MKS 51866      HY         Heated seat Minimum timeouts changed for All Vehicle lines
*                                          Now Min TImeouts are 0min
*                                          Also the NTC cut off temp thresholds changed for all vehicle lines
*                                          W series - 64c(HL3), 66c(HL2), 68c(HL1), 70c(LL1 and Shutoff)
*                                          Others   - 54c(HL3), 56c(HL2), 58c(HL1), 60c(LL1 and Shutoff)
*   06-11-2010   MKS 52375      HY         Rear Seat stuck switch detection wait time increased to 60sec from 15sec.
*                                          {5, 82, 103, 100, 50, 10, 200, 15, 2} to {5, 82, 103, 100, 50, 10, 200, 60, 2}
*   06-11-2010   MKS 52372      HY         Commented the Partial OPEN Front and Rear Zsense General thersholds and added 16
*                                          16 new variables 2 each for every vehicle line. 
*   08-02-2010   MKS 54707      HY         R series NTC Cut OFFs Calibration parameters got changed.
*                                          New values are (62c-HL3, 63c-HL2, 64c-HL1, 65c-LL1 and Shut OFF)
*                                          R series HL1 PWM changed to 15% for HL1.
*                                          Also HL3 and HL2 MAX Timeouts changed for all vehicle lines to 15min and 15min
*                                          from 10min and 20min respectivelly.   
*   08-09-2010   MKS 55012		HY         Heated steering wheel LEvel 3 100% PWM Max timeout increased to 15min from 10min
*   10-05-2010   MKS 57313      HY         HEat calibration parameters reduced to 6 different sets
*                                          Differentiated TIPM and PN sets. PN sets added with Leather and Cloth
*                                          default values updated with EE_SEAT_CFG value. also future reserve data reduced to 12 bytes    
******************************************************************************/
/******************************************************************************
 * 									CUSW Updates
 *  04-13-2011   MKS 69496      HY          Added defaults for the new boot supported EEPROM services.
 *  06-02-2011   MKS 74315		HY			Adjusted EEPROM Table to fit full 3 pages data.  
 *  08-05-2011   MKS 81845		HY			EEP Map adjusted. Now ECU Serial Number and VIN located
 * 											in Boot area, so APPL and BOOT both can share the information.
 * 				 MKS 81838		HY			PROXI Information also added.
 *  08-18-2011   MKS xxxxx		HY			ECU Time First DTC Detection (200B) and	ECU TIme from Key ON First DTC detection
 * 											(200C) default values changed from FF's to 00's. 
 *  09-15-2011   MKS 84972      HY          Steering Wheel over current calibration threshold adjusted to 10A instead of 12A.
 *  10-12-2011   MKS 86221      HY          Calibrations updated for PF Platform.(Seat Heaters updated)
 *  11-29-2011   MKS 91783      CC          Calibrations updated for PF Platform.(Steering Wheel updated)
 *  02-13-2012   MKS 98062      CC          Calibrations updated for PF Platform.(Seat Heater and Steering Wheel updated) sEEPConstants
 * MKS 98062                                               
 *                       New                 Previous      
 * HL2 PWM              40%                   50%		   
 * HL3 NTC Cut-off      49 C                  56 C		   
 * HL2 NTC Cut-off      49.5 C                56.5		   
 * HL1 NTC Cut-off      50 C                  57 C		   
 * LL1 NTC Cut-off      51 C                  58 C		  
 * HL3 Max Time         15 min                20 min	   
 * MKS 98062                                          
 *                       New            Previous      
 * WL4 NTC cutoff         42 C          47 C		  
 * WL3 NTC cutoff         45 C          50 C		  
 * WL2 NTC cutoff         47 C          53 C		  
 * Shutoff  temp          48 C          54 C		  
 * MKS 103524                                          
 *                       New                  
 * WL4 NTC cutoff         37 C          		  
 * WL3 NTC cutoff         39 C          		  
 * WL2 NTC cutoff         44 C          		  
 * Shutoff  temp          46 C
 * WL2 Duty Cycle         45 %          	
 * WL1 Duty Cycle         35 %
 * WL3 Max Time           30 m
 * WL2 Max Time           60 m
 * WL1 Max Time           60 m
******************************************************************************/


/*****************************************************************************************************
*   Include Files
*****************************************************************************************************/
#include "itbm_basapi.h"
#include "cpu.h"
#include "mw_cfgm.h"

#include "mw_tmrm.h"

#include "memfunc.h"
#include "memdef.h"

#include "hwio_eeprom.h"
#include "mw_eem.h"
#include "hwio_eeprom_cfg.h"
#include "mw_diagm_hw.h"
#include "DiagIOCtrl.h"
#include "hwio_eeprom_direct.h"
#include "EepMap.h"

#include "hwio_wd.h"

#include "versions.h"

/*****************************************************************************************************
*   Local Macro Definitions
*****************************************************************************************************/
#define EE_EEPROM_START_PHY_ADDRESS         (0x100000)
#define EE_EEPROM_START_LOG_ADDRESS         (0x0800)
#define EE_EEPROM_END_PHY_ADDRESS           (0x102000)

//#define EE_RAM_SHADOW_START_PHY_ADDRESS     (0x0FE000)
//#define EE_RAM_SHADOW_START_LOG_ADDRESS     (0x1000)
#define EE_RAM_SHADOW_START_PHY_ADDRESS     (((UInt32)(&byShadowRam)) + 0x0FC000UL)
#define EE_RAM_SHADOW_START_LOG_ADDRESS     ((UInt16)(&byShadowRam))

#define EE_IS_VIRGIN_MARKER                 (0x5AA5)

#define EE_IS_SECTOR_DIRTY(ee_location)      ((abyEEDirtyBits[((ee_location) / 8)]            \
                                            & (abyEEMaskTable[((ee_location) % 8)])) != 0)
#define EE_MAKE_SECTOR_DIRTY(ee_location)    (abyEEDirtyBits[((ee_location) / 8)]             \
                                            |= (abyEEMaskTable[((ee_location) % 8)]))
#define EE_MAKE_SECTOR_CLEAN(ee_location)    (abyEEDirtyBits[((ee_location) / 8)]             \
                                            &= (~(abyEEMaskTable[((ee_location) % 8)])))


/* the flags for checksums, stored in two seperate arrays, the macros take the array as an input */
#define EE_CXSUM_FLAG_IS_SET(array, block_ofst)     (((array[((block_ofst)/8)])       \
                                                    & (abyEEMaskTable[((block_ofst)%8)])) != 0)
#define EE_SET_CXSUM_FLAG(array, block_ofst)        ((array[((block_ofst)/8)])        \
                                                    |= (abyEEMaskTable[((block_ofst)%8)]))
#define EE_CLR_CXSUM_FLAG(array, block_ofst)        ((array[((block_ofst)/8)])        \
                                                    &= (~(abyEEMaskTable[((block_ofst)%8)])))
#define EE_LOCATION_SIZE(elocation)         (aEEBlockTable[elocation].wSize)

#define DFLASH_SECTOR_SIZE                  0x100

/* set the max number of block writes to 50,000 with 17 tolerance to match the old code */
#define MAX_NUM_VTA_BLOCK_WRITES            (50000 - 17)
#define SIZE_OF_VTA_BLOCK                   sizeof(VTABlockWriteData_t)

/* address of the checksum is invalid data.  This is written directly since it needs to be the *
 * first and the last piece of data written to EEPROM to maintain integrity */
#define EE_CXSUM_INVALID_FLAG_ADDRESS       0x100C00

/* this is the data written to the checksum invalid flag to designate that the checksum is valid / invalid */
#define EE_CXSUM_IS_VALID_DATA              0x5AA5A55A
#define EE_CXSUM_IS_INVALID_DATA            0xFFFFFFFF

/* this is the data written to the backup initalized flag to mark if the backups have been generated or not */
#define EE_BACKUPS_INITALIZED              0x5AA5A55A

/* time delay before we can write EEPROM after a power-up */
#define EE_HOLDOFF_TIME                     50      /* 50*20ms = 1sec */

#define EE_INVALID_BLOCK_ID                 0xFF

#define EE_NOT_PROGRAMMED_VIN_ODO_COUNTER   0xFF
/*****************************************************************************************************
*   Local Type Definitions
*****************************************************************************************************/
typedef enum {
EE_COPY_PRIMARY2BACKUP,
EE_COPY_BACKUP2PRIMARY
} EESectionCopyType_t;

/*****************************************************************************************************
*   Local Function Declarations
*****************************************************************************************************/

/*****************************************************************************************************
*   Global Variable Definitions
*****************************************************************************************************/
//DeclareResource(ResEEPROMShadow);

/*****************************************************************************************************
*   Static Variable Definitions
*****************************************************************************************************/
static const UInt8 abyEEMaskTable[8] = { 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80 };

static UInt8 abyEEDirtyBits[EE_NUM_BYTES_IN_DIRTY_ARRAY];
static SEEStatusFlags_t sEEStatusFlags;
static UInt8 abyEEWriteBuff[200];
static UInt8 abyEECxsumUpdateReq[EE_NUM_BYTES_IN_CXSUM_FLAGS_ARRAY];
static UInt8 abyEECxsumFailedFlags[EE_NUM_BYTES_IN_CXSUM_FLAGS_ARRAY];

static ITBM_BOOLEAN checksums_valid;
static ITBM_BOOLEAN version_valid;
/* pointer to the current block being used by the jump strategy */
static UInt8 byVTABlockOfst;

//static SEEPelements_t sEEPElements;

static UInt8 byShadowRam[2000];
UInt16  ee_wait_counter_c; //Harsha added

static UInt8 ee_Iref_seatH_c;           // stores Iref_SeatH. Added by CK on 04.08.2009
static UInt16 ee_Vref_seatH_w;          // stores Iref_SeatH. Added by CK on 04.08.2009
static UInt8 ee_Iref_STW_c;                     // stores Iref_STW. Added by CK on 04.08.2009
static UInt16 ee_Vref_STW_w;            // stores Vref_STW. Added by CK on 04.08.2009  

/*****************************************************************************************************
*   Global and Static Function Definitions
*****************************************************************************************************/
static void EE_VTABlockInit(void);
static void EE_SectionCopy(EECxsumSection_t eSection, EESectionCopyType_t eCopyType);
static void EE_PowerFailureRecovery(void);
static void EE_UpdateAllCxsum(void);
static UInt16 EE_BlockCxsum(UInt16 wStartOffset, UInt16 wEndOffset);
static void EE_VerifyAllCxsum(void);
static void EE_WriteFailedFlags(void);
static void EE_ReadFailedFlags(void);
static void EE_RefreshEEToRAM(void);
static void EE_Manager_Static_Init (void);


const SEEPelements_t sEEPConstants =
{

/************************************** START PAGE 1 ***********************************************/ 		
0xFF,                           //Load Application defaults
0x0B2A,                         //Eeprom version information. // Also needs to be updated in versions.h with every release
0x8000,                         //Default 100% PWM for Autosar values 
5, 100, 50, 10, 200, 60, 2,   //Open, OverLoad, STB, STG, 2sec LED, 60sec mature stuck, 2sec demature stuck 
80, 4, 4, 8, 25, 8,    //Relay Ubat 8V, 40ms wait time P1, 40ms wait time P2, 80ms wait DTW, stuck open and 80ms

0,80,100,                                     // Vent PWMs PF (duty cycles) (Level low 9.6v, high 12v)
0,80,100,                                     // Vent PWMs MM (duty cycles) (Level low 9.6v, high 12v)
0,80,100,                                     // Vent PWMs KL (duty cycles) (Level low 9.6v, high 12v)
0,80,100,                                     // Vent PWMs UF (duty cycles) (Level low 9.6v, high 12v)
0,80,100,                                     // Vent PWMs UT (duty cycles) (Level low 9.6v, high 12v)
0,80,100,                                     // Vent PWMs PK (duty cycles) (Level low 9.6v, high 12v)

2,10,50,95,                                   // Vent Faults (100 mV/mA) (Open, Shrt_to_GND, Shrt_to_BATT, Enable Open Detec.)
400 ,                                           // Vent motor start-up time (400 * 5ms = 2 seconds)
25 ,                                            // 2sec mature time to blow vent faults (except OPEN)
125,                                            // 10sec mature time to blow vent OPEN fault  //Tracker Id#43

10,                                             // Heated Seat NTC > 125C Short to Ground Fault
226,                                            // Heated Seat NTC < -30 !!! Open load Fault
50,                                             // 5 V = 50 (100mV) NTC Supply voltage low threshold
4,                                              // On time before detecting NTC open at ambient < -10 C (in minutes)
173 ,                                           // PCB set over temperature - Outputs Off (100C) NEW THRESHOLD
213,                                            // PCB clear over temperature - Outputs can turn On (90C)

{160,                                           // Set over voltage threshold
155,                                            // Clear over voltage threshold
95,                                             // Clear under voltage threshold
90,                                             // Set under voltage threshold
25},                                            // Erratic local voltage reading threshold = 2.5V (system & battery V. diff.). HAS TO BE >= 2.0V
{3,                                             // Set Under Voltage/Over voltage fault delay time (3 seconds) 
60,                                             // Voltage fault mature time (seconds)
15},                                            // Voltage fault de-mature time (seconds)

{0,                                             // No Outputs are ON (no compensation)
4,                                              // Only one heater or vent output is ON (0.4 V compensation)
7,                                              // Two, three or four outputs are ON (heater and/or vent) (0.7 V compensation)
5,                                              // Only steering wheel ON (0.5 V compensation) 
11,                                             // Steering wheel + 1 or 2 heater/vent outputs are ON (1.1 V compensation)
13},                                            // Steering wheel + 3 or 4 heater/vent outputs are ON (1.3 V compensation)                              


/************************************ WHEEL PF LEATHER ***************************************/
0,60,60,30,15, //(ORIGINAL)                               // StWheel Timeouts (max) (Level 0,1,2,3,4)
//0,30,30,20,7,  //(TESTING)                          // StWheel Timeouts (max) (Level 0,1,2,3,4)
0,35,45,65,100,  //(ORIGINAL)                             // StWheel PWMs (duty cycles) (Level 0,1,2,3,4)
//0,17,37,67,97,  //(TESTING)                             // StWheel PWMs (duty cycles) (Level 0,1,2,3,4)
172, 											// Maximum Temperature thresholds for WL1 (46 C = 172 AD CAN message) MKS 103524
168,											// Maximum Temperature thresholds for WL2 (44 C = 168 AD CAN message) MKS 103524
158,                                            // Maximum Temperature thresholds for WL3 (39 C = 158 AD CAN message) MKS 103524
154,                                            // Maximum Temperature thresholds for WL4 (37 C = 154 AD CAN message) MKS 103524
172,                                            // Maximum Temperature thresholds for stWheel (46 C = 172 AD) MKS 103524
0,                                              // Additional timers to automatically update WL1 (2 minute in 320ms tm)
0,                                              // Additional timers to automatically update WL2 (2 minute in 320ms tm)
0,                                              // Additional timers to automatically update WL3 (0 minute in 320ms tm)
0,                                              // Additional timers to automatically update WL4 (2 minute in 320ms tm)
/************************************ WHEEL PF WOOD ***************************************/
0,60,60,30,15, //(ORIGINAL)                               // StWheel Timeouts (max) (Level 0,1,2,3,4)
//0,30,30,20,9,  //(TESTING)                          // StWheel Timeouts (max) (Level 0,1,2,3,4)
0,35,45,65,100,  //(ORIGINAL)                             // StWheel PWMs (duty cycles) (Level 0,1,2,3,4)
//0,19,39,69,99,  //(TESTING)                             // StWheel PWMs (duty cycles) (Level 0,1,2,3,4)
172, 											// Maximum Temperature thresholds for WL1 (46 C = 172 AD CAN message) MKS 103524
168,											// Maximum Temperature thresholds for WL2 (44 C = 168 AD CAN message) MKS 103524
158,                                            // Maximum Temperature thresholds for WL3 (39 C = 158 AD CAN message) MKS 103524
154,                                            // Maximum Temperature thresholds for WL4 (37 C = 154 AD CAN message) MKS 103524
172,                                            // Maximum Temperature thresholds for stWheel (46 C = 172 AD) MKS 103524
0,                                              // Additional timers to automatically update WL1 (2 minute in 320ms tm)
0,                                              // Additional timers to automatically update WL2 (2 minute in 320ms tm)
0,                                              // Additional timers to automatically update WL3 (0 minute in 320ms tm)
0,                                              // Additional timers to automatically update WL4 (2 minute in 320ms tm)
/************************************ WHEEL MM LEATHER ***************************************/
0,30,30,20,15,                                // StWheel Timeouts (max) (Level 0,1,2,3,4)
0,15,35,65,100,                               // StWheel PWMs (duty cycles) (Level 0,1,2,3,4)
168, 											// Maximum Temperature thresholds for WL1 (44 C = 168 AD CAN message)
166,											// Maximum Temperature thresholds for WL2 (43 C = 166 AD CAN message)
158,                                            // Maximum Temperature thresholds for WL3 (39 C = 158 AD CAN message)
152,                                            // Maximum Temperature thresholds for WL4 (36 C = 152 AD CAN message)
168,                                            // Maximum Temperature thresholds for stWheel (44 C = 168 AD)
0,                                              // Additional timers to automatically update WL1 (2 minute in 320ms tm)
0,                                              // Additional timers to automatically update WL2 (2 minute in 320ms tm)
0,                                              // Additional timers to automatically update WL3 (0 minute in 320ms tm)
0,                                              // Additional timers to automatically update WL4 (2 minute in 320ms tm)
/************************************ WHEEL MM WOOD ***************************************/
0,30,30,20,15,                                // StWheel Timeouts (max) (Level 0,1,2,3,4)
0,15,35,65,100,                               // StWheel PWMs (duty cycles) (Level 0,1,2,3,4)
168, 											// Maximum Temperature thresholds for WL1 (44 C = 168 AD CAN message)
166,											// Maximum Temperature thresholds for WL2 (43 C = 166 AD CAN message)
158,                                            // Maximum Temperature thresholds for WL3 (39 C = 158 AD CAN message)
152,                                            // Maximum Temperature thresholds for WL4 (36 C = 152 AD CAN message)
168,                                            // Maximum Temperature thresholds for stWheel (44 C = 168 AD)
0,                                              // Additional timers to automatically update WL1 (2 minute in 320ms tm)
0,                                              // Additional timers to automatically update WL2 (2 minute in 320ms tm)
0,                                              // Additional timers to automatically update WL3 (0 minute in 320ms tm)
0,                                              // Additional timers to automatically update WL4 (2 minute in 320ms tm)
/************************************ WHEEL KL LEATHER ***************************************/
0,30,30,20,15,                                // StWheel Timeouts (max) (Level 0,1,2,3,4)
0,15,35,65,100,                               // StWheel PWMs (duty cycles) (Level 0,1,2,3,4)
168, 											// Maximum Temperature thresholds for WL1 (44 C = 168 AD CAN message)
166,											// Maximum Temperature thresholds for WL2 (43 C = 166 AD CAN message)
158,                                            // Maximum Temperature thresholds for WL3 (39 C = 158 AD CAN message)
152,                                            // Maximum Temperature thresholds for WL4 (36 C = 152 AD CAN message)
168,                                            // Maximum Temperature thresholds for stWheel (44 C = 168 AD)
0,                                              // Additional timers to automatically update WL1 (2 minute in 320ms tm)
0,                                              // Additional timers to automatically update WL2 (2 minute in 320ms tm)
0,                                              // Additional timers to automatically update WL3 (0 minute in 320ms tm)
0,                                              // Additional timers to automatically update WL4 (2 minute in 320ms tm)
/************************************ WHEEL KL WOOD ***************************************/
0,30,30,20,15,                                // StWheel Timeouts (max) (Level 0,1,2,3,4)
0,15,35,65,100,                               // StWheel PWMs (duty cycles) (Level 0,1,2,3,4)
168, 											// Maximum Temperature thresholds for WL1 (44 C = 168 AD CAN message)
166,											// Maximum Temperature thresholds for WL2 (43 C = 166 AD CAN message)
158,                                            // Maximum Temperature thresholds for WL3 (39 C = 158 AD CAN message)
152,                                            // Maximum Temperature thresholds for WL4 (36 C = 152 AD CAN message)
168,                                            // Maximum Temperature thresholds for stWheel (44 C = 168 AD)
0,                                              // Additional timers to automatically update WL1 (2 minute in 320ms tm)
0,                                              // Additional timers to automatically update WL2 (2 minute in 320ms tm)
0,                                              // Additional timers to automatically update WL3 (0 minute in 320ms tm)
0,                                              // Additional timers to automatically update WL4 (2 minute in 320ms tm)
/************************************ WHEEL UF LEATHER ***************************************/
0,30,30,20,15,                                // StWheel Timeouts (max) (Level 0,1,2,3,4)
0,15,35,65,100,                               // StWheel PWMs (duty cycles) (Level 0,1,2,3,4)
168, 											// Maximum Temperature thresholds for WL1 (44 C = 168 AD CAN message)
166,											// Maximum Temperature thresholds for WL2 (43 C = 166 AD CAN message)
158,                                            // Maximum Temperature thresholds for WL3 (39 C = 158 AD CAN message)
152,                                            // Maximum Temperature thresholds for WL4 (36 C = 152 AD CAN message)
168,                                            // Maximum Temperature thresholds for stWheel (44 C = 168 AD)
0,                                              // Additional timers to automatically update WL1 (2 minute in 320ms tm)
0,                                              // Additional timers to automatically update WL2 (2 minute in 320ms tm)
0,                                              // Additional timers to automatically update WL3 (0 minute in 320ms tm)
0,                                              // Additional timers to automatically update WL4 (2 minute in 320ms tm)
/************************************ WHEEL UF WOOD ***************************************/
0,30,30,20,15,                                // StWheel Timeouts (max) (Level 0,1,2,3,4)
0,15,35,65,100,                               // StWheel PWMs (duty cycles) (Level 0,1,2,3,4)
168, 											// Maximum Temperature thresholds for WL1 (44 C = 168 AD CAN message)
166,											// Maximum Temperature thresholds for WL2 (43 C = 166 AD CAN message)
158,                                            // Maximum Temperature thresholds for WL3 (39 C = 158 AD CAN message)
152,                                            // Maximum Temperature thresholds for WL4 (36 C = 152 AD CAN message)
168,                                            // Maximum Temperature thresholds for stWheel (44 C = 168 AD)
0,                                              // Additional timers to automatically update WL1 (2 minute in 320ms tm)
0,                                              // Additional timers to automatically update WL2 (2 minute in 320ms tm)
0,                                              // Additional timers to automatically update WL3 (0 minute in 320ms tm)
0,                                              // Additional timers to automatically update WL4 (2 minute in 320ms tm)
/******************* WHEEL UK LEATHER **********/
0,30,30,20,15,                                // StWheel Timeouts (max) (Level 0,1,2,3,4)
0,15,35,65,100,                               // StWheel PWMs (duty cycles) (Level 0,1,2,3,4)
168, 											// Maximum Temperature thresholds for WL1 (44 C = 168 AD CAN message)
166,											// Maximum Temperature thresholds for WL2 (43 C = 166 AD CAN message)
158,                                            // Maximum Temperature thresholds for WL3 (39 C = 158 AD CAN message)
152,                                            // Maximum Temperature thresholds for WL4 (36 C = 152 AD CAN message)
168,                                            // Maximum Temperature thresholds for stWheel (44 C = 168 AD)
0,                                              // Additional timers to automatically update WL1 (2 minute in 320ms tm)
0,                                              // Additional timers to automatically update WL2 (2 minute in 320ms tm)
0,                                              // Additional timers to automatically update WL3 (0 minute in 320ms tm)
0,                                              // Additional timers to automatically update WL4 (2 minute in 320ms tm)
/******************** WHEEL UK WOOD ************/
0,30,30,20,15,                                // StWheel Timeouts (max) (Level 0,1,2,3,4)
0,15,35,65,100,                               // StWheel PWMs (duty cycles) (Level 0,1,2,3,4)
168, 											// Maximum Temperature thresholds for WL1 (44 C = 168 AD CAN message)
166,											// Maximum Temperature thresholds for WL2 (43 C = 166 AD CAN message)
158,                                            // Maximum Temperature thresholds for WL3 (39 C = 158 AD CAN message)
152,                                            // Maximum Temperature thresholds for WL4 (36 C = 152 AD CAN message)
168,                                            // Maximum Temperature thresholds for stWheel (44 C = 168 AD)
0,                                              // Additional timers to automatically update WL1 (2 minute in 320ms tm)
0,                                              // Additional timers to automatically update WL2 (2 minute in 320ms tm)
0,                                              // Additional timers to automatically update WL3 (0 minute in 320ms tm)
0,                                              // Additional timers to automatically update WL4 (2 minute in 320ms tm)
/************************************** END PAGE 1 ***********************************************/ 		

/************************************** START PAGE 2 ***********************************************/ 		
5,10,100,15,50,4,                               // Faults (100 mV/mA) (Partial Open, Full open, over current, Shrt_to_GND & BATT, Ontime for HSW Sensor Fault)
/*************************     CALIBRATION AD READINGS *********************************************/
/* 49c -> 122, 49.5 -> 121,  */
/* 50c -> 120, 50.5 -> 119, 51 -> 118, 51.5 -> 116, 52 -> 115, 52.5 -> 114, 53 -> 113, 53.5 -> 112 */
/* 54c -> 111, 54.5 -> 110, 55 -> 109, 55.5 -> 108, 56 -> 107, 56.5 -> 106, 57 -> 105, 57.5 -> 104 */
/* 58c -> 103, 58.5 -> 102, 59 -> 101, 59.5 -> 100, 60 ->  99, 60.5 ->  98, 61 ->  97, 61.5 ->  96 */
/* 62c ->  95, 62.5 ->  94, 63 ->  93, 63.5 ->  92, 64 ->  91, 64.5 ->  90, 65 ->  89, 66   ->  87 */
/*************************     CALIBRATION AD READINGS *********************************************/

/*************************       PF FRONT PARAMETERS .. CLOTH  *****************************************/
/************** The below 14 variables are for vehicle line PF SERIES***************************/
/*     NTC Cut OFF's: LL1 -> 51C, HL1 -> 50C, HL2 -> 49.5C, HL3 -> 49C, Shut OFF -> 51C        */
/*     UPDATE HL3 CUTTOFF to 50.5 -> 119 from 122, Just .5 under shutoff 22_JUNE_2012 T.RODE   */ 
/**********************************************************************************************/
15,  25,  40, 100,       /* B1->LL1 PWM,     B2-> HL1 PWM,      B3-> HL2 PWM,       B4-> HL3 PWM MKS 98062*/
45,  20,  20,  15,       /* B5->LL1 MaxTout, B6-> HL1 MaxTout,  B7-> HL2 MaxTout,   B8-> HL3 MaxTout MKS 98062*/
118, 120, 121, 119, 118,   /* B13->LL1 NTC cutOFF,B14 ->HL1 NTC cutOFF,B15 ->HL2 NTC cutOFF,B16 ->HL3 NTC cutOFF, B17 ->NTC ShutOFF MKS 98062*/
70,                      /* B18 -> Front Heaters Partial OPEN, B19 -> Rear Heaters Partial OPEN */
/*************************       PF REAR PARAMETERS .. CLOTH  *****************************************/
/************** The below 14 variables are for vehicle line PF SERIES***************************/
/*     NTC Cut OFF's: LL1 -> 51C, HL1 -> 50C, HL2 -> 49.5C, HL3 -> 49C, Shut OFF -> 51C        */
/*     UPDATE HL3 CUTTOFF to 50.5 -> 119 from 122, Just .5 under shutoff 22_JUNE_2012 T.RODE   */ 
/**********************************************************************************************/
15,  25,  40, 100,       /* B1->LL1 PWM,     B2-> HL1 PWM,      B3-> HL2 PWM,       B4-> HL3 PWM MKS 98062*/
45,  20,  20,  15,       /* B5->LL1 MaxTout, B6-> HL1 MaxTout,  B7-> HL2 MaxTout,   B8-> HL3 MaxTout MKS 98062*/
118, 120, 121, 119, 118,   /* B13->LL1 NTC cutOFF,B14 ->HL1 NTC cutOFF,B15 ->HL2 NTC cutOFF,B16 ->HL3 NTC cutOFF, B17 ->NTC ShutOFF MKS 98062*/
70,                      /* B18 -> Front Heaters Partial OPEN, B19 -> Rear Heaters Partial OPEN */
/*************************       PF FRONT PARAMETERS .. BASE LEATHER  *****************************************/
/************** The below 14 variables are for vehicle line PF SERIES***************************/
/*     NTC Cut OFF's: LL1 -> 51C, HL1 -> 50C, HL2 -> 49.5C, HL3 -> 49C, Shut OFF -> 51C        */
/*     UPDATE HL3 CUTTOFF to 50.5 -> 119 from 122, Just .5 under shutoff 22_JUNE_2012 T.RODE   */ 
/**********************************************************************************************/
15,  25,  40, 100,       /* B1->LL1 PWM,     B2-> HL1 PWM,      B3-> HL2 PWM,       B4-> HL3 PWM MKS 98062*/
45,  20,  20,  15,       /* B5->LL1 MaxTout, B6-> HL1 MaxTout,  B7-> HL2 MaxTout,   B8-> HL3 MaxTout MKS 98062*/
118, 120, 121, 119, 118,   /* B13->LL1 NTC cutOFF,B14 ->HL1 NTC cutOFF,B15 ->HL2 NTC cutOFF,B16 ->HL3 NTC cutOFF, B17 ->NTC ShutOFF MKS 98062*/
70,                      /* B18 -> Front Heaters Partial OPEN, B19 -> Rear Heaters Partial OPEN */
/*************************       PF REAR PARAMETERS .. BASE LEATHER  *****************************************/
/************** The below 19 variables are for vehicle line L SERIES***************************/
/*     NTC Cut OFF's: LL1 -> 51C, HL1 -> 50C, HL2 -> 49.5C, HL3 -> 49C, Shut OFF -> 51C        */
/*     UPDATE HL3 CUTTOFF to 50.5 -> 119 from 122, Just .5 under shutoff 22_JUNE_2012 T.RODE   */ 
/**********************************************************************************************/
15,  25,  40, 100,       /* B1->LL1 PWM,     B2-> HL1 PWM,      B3-> HL2 PWM,       B4-> HL3 PWM MKS 98062*/
45,  20,  20,  15,       /* B5->LL1 MaxTout, B6-> HL1 MaxTout,  B7-> HL2 MaxTout,   B8-> HL3 MaxTout MKS 98062*/
118, 120, 121, 119, 118,   /* B13->LL1 NTC cutOFF,B14 ->HL1 NTC cutOFF,B15 ->HL2 NTC cutOFF,B16 ->HL3 NTC cutOFF, B17 ->NTC ShutOFF MKS 98062*/
70,                      /* B18 -> Front Heaters Partial OPEN, B19 -> Rear Heaters Partial OPEN */
/*************************       PF FRONT PARAMETERS .. PERF LEATHER *****************************************/
/************** The below 14 variables are for vehicle line PF SERIES***************************/
/*     NTC Cut OFF's: LL1 -> 51C, HL1 -> 50C, HL2 -> 49.5C, HL3 -> 49C, Shut OFF -> 51C         */
/*     UPDATE HL3 CUTTOFF to 50.5 -> 119 from 122, Just .5 under shutoff 22_JUNE_2012 T.RODE   */ 
/**********************************************************************************************/
15,  25,  40, 100,       /* B1->LL1 PWM,     B2-> HL1 PWM,      B3-> HL2 PWM,       B4-> HL3 PWM MKS 98062*/
45,  20,  20,  15,       /* B5->LL1 MaxTout, B6-> HL1 MaxTout,  B7-> HL2 MaxTout,   B8-> HL3 MaxTout MKS 98062*/
118, 120, 121, 119, 118,   /* B13->LL1 NTC cutOFF,B14 ->HL1 NTC cutOFF,B15 ->HL2 NTC cutOFF,B16 ->HL3 NTC cutOFF, B17 ->NTC ShutOFF MKS 98062*/
70,                      /* B18 -> Front Heaters Partial OPEN, B19 -> Rear Heaters Partial OPEN */
/*************************       PF REAR PARAMETERS .. PERF LEATHER  *****************************************/
/************** The below 14 variables are for vehicle line PF SERIES***************************/
/*     NTC Cut OFF's: LL1 -> 51C, HL1 -> 50C, HL2 -> 49.5C, HL3 -> 49C, Shut OFF -> 51C          */
/*     UPDATE HL3 CUTTOFF to 50.5 -> 119 from 122, Just .5 under shutoff 22_JUNE_2012 T.RODE   */ 
/**********************************************************************************************/
15,  25,  40, 100,       /* B1->LL1 PWM,     B2-> HL1 PWM,      B3-> HL2 PWM,       B4-> HL3 PWM MKS 98062*/
45,  20,  20,  15,       /* B5->LL1 MaxTout, B6-> HL1 MaxTout,  B7-> HL2 MaxTout,   B8-> HL3 MaxTout MKS 98062*/
118, 120, 121, 119, 118,   /* B13->LL1 NTC cutOFF,B14 ->HL1 NTC cutOFF,B15 ->HL2 NTC cutOFF,B16 ->HL3 NTC cutOFF, B17 ->NTC ShutOFF MKS 98062*/
70,                      /* B18 -> Front Heaters Partial OPEN, B19 -> Rear Heaters Partial OPEN */
/**********************************************************************************************/

50,                             // 50 * 10ms = 500ms delay to set PLL unlock fault DTC (internal fault)
//0x55,                           // Remote Control Software is enabled (Disabled 0xAA)
0x55,                           // Remote Start Status is Enabled (Disable 0xAA)
0x59,                           //Cut off point for heat to automatically start in remote <4.4c     (Remote start Data)  
0x85,                           //Cut off point for vent to automatically start in remote >26.7c    (Remote start Data)
0x0D,                           // Engine RPM low limit that disables CSWM outputs 0x0D = 400RPM
0x00,                           //Security access False access Attempt flag
0xFFFF,                         //PCB Over Temp First time odo stamp
0xFFFF,                         //PCB Over Temp Latest odo stamp
0x00,                           //PCB Over temp counter (No of times)
0x00,                           //PCB Over Temp status (0 -Not set/1 - Set)
0xFFFF,0xFFFF,0x00,             //NTC Failsafe info (First time ODO, Recent ODO, Freq Counter)

0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFFFF, 0xFFFF,   //Chronostack 0
0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFFFF, 0xFFFF,   //Chronostack 1
0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFFFF, 0xFFFF,   //Chronostack 2
0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFFFF, 0xFFFF,   //Chronostack 3
0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFFFF, 0xFFFF,   //Chronostack 4
0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFFFF, 0xFFFF,   //Chronostack 5
0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFFFF, 0xFFFF,   //Chronostack 6
0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFFFF, 0xFFFF,   //Chronostack 7
0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFFFF, 0xFFFF,   //Chronostack 8
0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFFFF, 0xFFFF,   //Chronostack 9

//0xFF,                             //Model Year
//0xFF,                             //Vehicle Line
//0xFF,                             // NEW:  Known ECU configuration
//0x00,							  // NEW: Known Seat Configuration Cloth, leather
//0x01,                             // NEW: Known Wheel Configuration Leather/Wood
//2,                                // NEW: Iref_seatH: default 2A (unit = A)
//712,                              // NEW: Vref_seatH: default 712mV (unit - mV)
//8,                                // NEW: Iref_STW, default: 8A (unit = A)
//825,                              // NEW: Vref_STW, default: 825mV (unit - mV)
0x00,                            // internal fault byte 0
0x00,                            // internal fault byte 1
0x00,                            // internal fault byte 2
0x00,                            // internal fault byte 3
0x55,                            // Enable the detection of Internal faults that disable CAN communication (Disable 0xAA)
//0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF, //ECU Serial No 
0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF, //PROXI Config
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00, //PROXI Failure
0x55,								//PROXI Not Learned
0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,			//Reserve Data segment 2
/************************************** END PAGE 2 ***********************************************/ 		

/************************************** START PAGE 3 ***********************************************/ 		
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,    //DTC Status
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,    //DTC Status
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,    //DTC Status
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,    //DTC Status
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,    //DTC Status
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,    //DTC Status
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,    //DTC Status
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,                        //DTC Status

0xFF,0xFF,0xFF,0xFF,0xFFFF,0xFFFF,0xFF,               //Historical Stack 0
0xFF,0xFF,0xFF,0xFF,0xFFFF,0xFFFF,0xFF,               //Historical Stack 1
0xFF,0xFF,0xFF,0xFF,0xFFFF,0xFFFF,0xFF,               //Historical Stack 2
0xFF,0xFF,0xFF,0xFF,0xFFFF,0xFFFF,0xFF,               //Historical Stack 3
0xFF,0xFF,0xFF,0xFF,0xFFFF,0xFFFF,0xFF,               //Historical Stack 4
0xFF,0xFF,0xFF,0xFF,0xFFFF,0xFFFF,0xFF,               //Historical Stack 5
0xFF,0xFF,0xFF,0xFF,0xFFFF,0xFFFF,0xFF,               //Historical Stack 6
0xFF,0xFF,0xFF,0xFF,0xFFFF,0xFFFF,0xFF,               //Historical Stack 7
0xFF,0xFF,0xFF,0xFF,0xFFFF,0xFFFF,0xFF,               //Historical Stack 8
0xFF,0xFF,0xFF,0xFF,0xFFFF,0xFFFF,0xFF,               //Historical Stack 9

//0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,   //Original
0x00,							//Control Byte Final

0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,  //Temic BOM No
0x00,0x00,0x00,              // Odometer  CUSW
0x00,0x00,0x00,              // Odometer Last PGM  CUSW
0x00,0x00,0x00,0x00,           // ECU Timestamps  CUSW
0x0000,                        // ECU Time KeyON  CUSW
0x0000,                        // ECU KeyON Counter  CUSW
0x00,0x00,0x00,0x00,           // ECU Time First DTC  CUSW
0x0000,                        // ECU Time KeyON First DTC  CUSW
0xFF,                          // First conf DTC
0xFF,                          // Recent Conf DTC
0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,	//Reserve Data segment 3
0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,   //Reserve Data segment 3

/************************************** END PAGE 3 ***********************************************/ 		


/************************************** START BOOT ***********************************************/ 		
//{0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF},           //Boot SW Identification F180.
//{0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF},           //Appl SW Identification F181.
//{0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF},           //Appl Data Identification F182.
//{0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF},                     //Spare Pno F187.
0x0A,0x12,0x00,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,                         //FIAT ECU HW P NO F192.
0x01,																		    //FIAT ECU HW Version F193
0x0B,0x27,0x00,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,                         //FIAT ECU SW P NO F194.
//{0x0B16},																		//FIAT ECU SW Version F195	
//{0xFF,0xFF,0xFF,0xFF,0xFF,0xFF},                     						    //FIAT Approval NO F196.
//{0xFF,0xFF,0xFF,0xFF,0xFF},                     							    //FIAT Address Ident F1A5.
//{0x35,0x36,0x30,0x34,0x36,0x35,0xFF,0xFF,0xFF,0xFF},                          //DCX SW Part Number )
//{0x35,0x36,0x30,0x34,0x36,0x35,0xFF,0xFF,0xFF,0xFF},                          //DCX ECU Part Number
/************************************** END BOOT ***********************************************/ 		
};

/*****************************************************************************************************
*   Function:       EE_DebugSelfTest
*
*   Description: 
*
*   Caveats:
*
*****************************************************************************************************/
#undef EE_DEBUG_SELF_TEST
#ifdef EE_DEBUG_SELF_TEST

const UInt8 abyEEDebugWriteAA[] = { 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA };
const UInt8 abyEEDebugWrite55[] = { 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55 };
UInt8 abyEEDebugRead[8];

void EE_MANAGER_CALL EE_DebugSelfTest(void)
{

	EEBlockLocation eloc;
	unsigned short      woffset;
	
	/* Macro test */
	for (woffset = 0x00; woffset < EE_SHADOW_SIZE; woffset += EE_SECTOR_SIZE) {
		

		/* TODO: work out a priority for looking at which locations to write first. */
		
		EE_MAKE_SECTOR_DIRTY(woffset);

		if (EE_IS_SECTOR_DIRTY(woffset))
		   /* Clean the sector */ {
			
			EE_MAKE_SECTOR_CLEAN(woffset);

		}
		else {
			_asm("nop");
		}
	
	}
	
	/* Shadow write test */

	for (eloc = EE_FIRST_LOCATION; eloc < EE_NUM_BLOCKS; eloc++) {

		EE_BlockWrite(eloc, abyEEDebugWrite55);
		EE_BlockWrite(eloc, abyEEDebugWriteAA);

	}
	
	/* Shadow read test */

	for (eloc = EE_FIRST_LOCATION; eloc < EE_NUM_BLOCKS; eloc++) {
		
		EE_BlockRead(eloc, abyEEDebugRead);

	}

}


#endif



/*****************************************************************************************************
*   Function:    EE_IsChecksumsValid
*
*   Description: This function returns true if EEPROM checksums was valid
*
*   Caveats:
*
*****************************************************************************************************/
ITBM_BOOLEAN EE_MANAGER_CALL EE_IsChecksumsValid(void)
{
	

	return checksums_valid;



}


/*****************************************************************************************************
*   Function:    EE_IsVersionValid
*
*   Description: This function returns true if EEPROM version was valid
*
*   Caveats:
*
*****************************************************************************************************/



ITBM_BOOLEAN EE_MANAGER_CALL EE_IsVersionValid(void)
{
	

	return version_valid;



}


/*****************************************************************************************************
*   Function:    EE_WaitPendingWrites
*
*   Description: Wait until all EEPROM write is done
*
*   Caveats:
*
*****************************************************************************************************/



void MEM_FAR EE_WaitPendingWrites(void)
{
	

	UINT32 i;
	
	ITBM_tmrmSetTimer(TIMER_20MS_EE_WRITE_HOLDOFF, 0);
	
	/* Call EE Manager while it has something to do */

	

	do {
		

		EE_Manager();
		
		//ITBM_wdKick();

	

	} while (EE_IsBusy());
	

	/* !!! This patch shall be removed when issue in this functions will be fixed. Now data is not really stored in EE if EE_Manager is not busy */

	

	for ( i = 0; i < 10000; i++ ) {
		

		EE_Manager();
		//ITBM_wdKick();

	

	}


}


/*****************************************************************************************************
*   Function:    EE_PowerupInit
*
*   Description: This function initializes the EE driver beneath the manager as well as initializes
*                the internal state of the EE manager on powerup.  This function will NOT be preempted
*                by another task.
*
*   Caveats:
*
*****************************************************************************************************/



@far void /*EE_MANAGER_CALL*/ EE_PowerupInit(void)
{
	

	UInt8 byindex,EeBlockLocation,temp_load_data_c; //Harsha added EeBlockLocation,temp_load_data_c
	//UInt16 TempAddr; //Harsha added
	UInt16 wEEStartFlag;
	UInt16 wEEEndFlag;
	UInt32 lLongBlockRead;
	
	/* clear @far variables */
	EE_Manager_Static_Init();
	EE_Driver_Static_Init();
	
	EE_DriverPowerupInit();
	
#ifdef EE_DEBUG_SELF_TEST
	EE_DebugSelfTest();
#endif
	

	//   sEEPConstants.eeprom_version_w = 0x8000; //Harsha added for testing
	
	
	/* initalize the dirty bit array to zero */

	

	for (byindex=0; byindex < EE_NUM_BYTES_IN_DIRTY_ARRAY; byindex++) {
		

		abyEEDirtyBits[byindex] = 0;

	

	}
	

	/* initalize the checksum update array to zero */

	

	for (byindex=0; byindex < EE_NUM_BYTES_IN_CXSUM_FLAGS_ARRAY; byindex++) {
		

		abyEECxsumUpdateReq[byindex] = 0;
		abyEECxsumFailedFlags[byindex] = 0;

	

	}
	

	/* mark that EEPROM has not fully initalized for this POR */
	sEEStatusFlags.eeInitialized = ITBM_FALSE;
	
	/* mark that checksums are valid until we prove otherwise */
	sEEStatusFlags.eeCxsumIsValid = ITBM_TRUE;
	
	/* load used EE data into shadow RAM */
	EE_RefreshEEToRAM();
	
	/* Read the TOP virgin flag out of EEPROM */
	EE_BlockRead(EE_VRGN_FLAG_START_BLOCK, (UInt8*)&lLongBlockRead);
	wEEStartFlag = (UInt16)lLongBlockRead;
	
	/* Read the BOTTOM virgin flag out of EEPROM */
	EE_BlockRead(EE_VRGN_FLAG_END_BLOCK, (UInt8*)&lLongBlockRead);
	wEEEndFlag = (UInt16)lLongBlockRead;
	
	/* If EITHER virgin flag is not initialized */

	

	if (!((wEEStartFlag == EE_IS_VIRGIN_MARKER) && (wEEEndFlag == EE_IS_VIRGIN_MARKER))) {
		

		/* set up the checksums for a virgin module - the block writes will continue the updates */
		EE_UpdateAllCxsum();
		
		/* since the backups will be generated automatically by the virgin block writes - 
		   set this flag so the backup flag will be written */
		sEEStatusFlags.eeBkupInitializeReq = ITBM_TRUE;
		
		/* if manufacturing wrote any data to EEPROM, create the backup */

		

		for (byindex=0; byindex < EE_CXSUM_NUM_SECTIONS; byindex++) {
			

			EE_SectionCopy(byindex, EE_COPY_PRIMARY2BACKUP);

		

		}
		

		/* EE is not initialized -- copy ROM into shadow RAM and queue the EE writes */

		

		for (byindex=0; byindex < EE_NUM_BLOCKS; byindex++) {
			

			if (aEEBlockTable[byindex].eDefaultLoc != EE_NO_DEFAULT) {
				

				/* there is a default value for this block, set up the write to EEPROM */
				EE_BlockWrite(byindex, aEEDefaultsTable[aEEBlockTable[byindex].eDefaultLoc].pRomAddr);

			

			}
		

		}
		

		/* Write the virgin flags to EEPROM */
		lLongBlockRead = EE_IS_VIRGIN_MARKER;
		EE_BlockWrite(EE_VRGN_FLAG_START_BLOCK, (UInt8*)&lLongBlockRead);        
		EE_BlockWrite(EE_VRGN_FLAG_END_BLOCK, (UInt8*)&lLongBlockRead);
		/* write the initalized values to the failed flags block */
		/* the RAM value was cleared in the steps above */
		EE_WriteFailedFlags();

	

	}
	else {
		

		/* read the checksum failure flags from EEPROM - if we get a hard failure then the flag will be stored here */
		EE_ReadFailedFlags();
		
		/* check the "backups initalized" flag - ensures that modules have valid backups generated after initialization */
		sEEStatusFlags.eeBkupInitializeReq = ITBM_FALSE;
		EE_BlockRead(EE_BACKUPS_INITALIZED_BLOCK, (UInt8*)&lLongBlockRead);

		

		if ( lLongBlockRead != EE_BACKUPS_INITALIZED ) {
			

			/* the backups have not been initalized, start the process to make the primaries match the backups */
			sEEStatusFlags.eeBkupInitializeReq = ITBM_TRUE;
			
			/* call the diagnostic routine to copy all primary blocks into backups */
			/* this function also queues up all checksums */
			EE_SID31_FACallback();

		

		}
		else {
			

			/* backups have been generated so we can recover if there was a power failure during write */
			/* module is not virgin so the checksum valid flag SHOULD be marked as valid */
			/* if not we should update all checksums since power was likely removed during a write */
			/* there are currently no "sensitive" blocks so some data will be lost */
			/* Should we set some type of flag to indicate this occurred so there can be some recovery?? */
			/* make sure we set up the paged EEPROM information */
			EPAGE = GetEPage(EE_CXSUM_INVALID_FLAG_ADDRESS);
			lLongBlockRead = *((unsigned long*)(EEPROM_LOGICAL_START + GetEOffset(EE_CXSUM_INVALID_FLAG_ADDRESS)));

			

			if ( lLongBlockRead != EE_CXSUM_IS_VALID_DATA ) {
				

				EE_PowerFailureRecovery();

			

			}
		

		}
	

	}
	

	{
		

		UINT16 version;
		
		version_valid = ITBM_TRUE;
		
		EE_BlockRead(EE_EEPROM_VERSION, (UINT8*)&version);

		

		if (version != CURRENT_EEPROM_VERSION) {
			

			version = CURRENT_EEPROM_VERSION;
			
			version_valid = ITBM_FALSE;
			
			EE_BlockWrite(EE_EEPROM_VERSION, (UINT8*)&version);

		

		}
	

	}
	

	/* start the holdoff timer for writing EEPROM after power-up */
	ITBM_tmrmSetTimer(TIMER_20MS_EE_WRITE_HOLDOFF, EE_HOLDOFF_TIME);

}


/******************************************************************************************/
/*                            Load defaults                                               */
/******************************************************************************************/



@far void /*EE_MANAGER_CALL*/ EE_LoadDefaults(void)
{

	UInt8 byindex,EeBlockLocation,temp_load_data_c,temp_ecu_time[4],temp_reprog_req_c; //Harsha added EeBlockLocation,temp_load_data_c
	UInt8 *TempAddr; //Harsha added
	UInt8 temp_TotalOdo_c[3];       // will have present odo value stored in eep. 
	UInt16 wEEStartFlag,temp_key_on_w;
	UInt16 wEEEndFlag;
	UInt32 lLongBlockRead;
	
	
	/* Get the Load defaults value from EEPROM. */
	EE_BlockRead(EE_LOAD_DEFAULTS, &temp_load_data_c);
	EE_BlockRead(EE_FBL_REPROG_REQ, &temp_reprog_req_c);

	/* If it is other than 0xAA, means there is need for loading the defaults. */
	/* Either First Time or After Reflash */
	if ( (temp_load_data_c != 0xAA) || (temp_reprog_req_c != 0xAA) )  {
		
//		/* Update the EEPROM Version defaults */
		TempAddr = (UINT8*)&sEEPConstants.eeprom_version_w;

		for (EeBlockLocation = EE_EEPROM_VERSION; EeBlockLocation <= EE_RESERVE_DATAS2; EeBlockLocation++) {

			EE_BlockWrite(EeBlockLocation,  TempAddr); //Harsha Added
			TempAddr += (aEEBlockAddrTable[EeBlockLocation].wSize);
		}

		EE_BlockWrite(EE_DTC_STATUS, (UInt8*)&sEEPConstants.Dtcstat[0]);
		
		EE_BlockRead(EE_ECU_TIMESTAMPS, (UInt8*)&temp_ecu_time[0]); 

		if(temp_ecu_time[0] == 0xFF && temp_ecu_time[1] == 0xFF && temp_ecu_time[2] == 0xFF && temp_ecu_time[3] == 0xFF)
		{
			EE_BlockWrite(EE_ECU_TIMESTAMPS, (UInt8*)&sEEPConstants.ecu_timestamps_l[0]);
		}
		else
		{
			//Reflashing. Do not copy
		}

		EE_BlockRead(EE_ECU_TIME_KEYON, (UInt8*)&temp_key_on_w); 
		
		if (temp_key_on_w == 0xFFFF) {
			/* First execution of this function load default for Key On and Key On counter */
			EE_BlockWrite(EE_ECU_TIME_KEYON, (UINT8*)&sEEPConstants.ecu_timekeyon_w);
			EE_BlockWrite(EE_ECU_KEYON_COUNTER, (UINT8*)&sEEPConstants.ecu_keyon_counter_w);
		}
		else {
			/* Re-flashing. Do not copy Vref_STW */
		}
		/* Update the ECU time First DTC Detection and Key on first DTC detection with 0's */
		EE_BlockWrite(EE_ECU_TIME_FIRSTDTC, (UInt8*)&sEEPConstants.ecu_time_firstdtc_l[0]);
		EE_BlockWrite(EE_ECU_TIME_KEYON_FIRSTDTC, (UINT8*)&sEEPConstants.ecu_timekeyon_firstdtc_w);
		
		/* We are here means, first time we are executing or through re program */
		/* Update the Present ODO meter value, to the Odometer last programmed area */
		EE_BlockRead(EE_ODOMETER, (UInt8*)&temp_TotalOdo_c[0]); //$2001 service
		
		if(temp_TotalOdo_c[0] == 0xFF && temp_TotalOdo_c[1] == 0xFF && temp_TotalOdo_c[2] == 0xFF)
		{
			/*First time flashing. Load default values with 00*/
			EE_BlockWrite(EE_ODOMETER, (UINT8*)&sEEPConstants.odometer_c[0]); //$2001 service Initial value
			EE_BlockWrite(EE_ODOMETER_LAST_PGM, (UINT8*)&sEEPConstants.odometer_last_pgm_c[0]); //$2002 service Initial value
		}
		else
		{
			/* Reflash. Load the Latest ODO Value to the last programmed ODO */
			EE_BlockWrite(EE_ODOMETER_LAST_PGM, (UINT8*)&temp_TotalOdo_c[0]); //$2002 service
		}
		
		
		/* Harsha commented this portion as this is going to be into FblIf.c program */
		/* Update BOOT AREA Part 1*/
//		TempAddr = (UINT8*)&sEEPConstants.fiat_ecu_hw_number_c[0];
//
//		for (EeBlockLocation = EE_FBL_FIAT_ECU_HW_NO; EeBlockLocation <= EE_FBL_FIAT_ECU_SW_NO; EeBlockLocation++) {
//
//			EE_BlockWrite(EeBlockLocation,  TempAddr); //Harsha Added
//			TempAddr += (aEEBlockAddrTable[EeBlockLocation].wSize);
//		}
		
		
		/* At the end, as we are updated the defaults, update the LOAD defaults constant with 0xAA.  */
		/* Next Power ON, module looks for the data and will not reload the defaults.                */
		temp_load_data_c = 0xAA;
		EE_BlockWrite(EE_LOAD_DEFAULTS, &temp_load_data_c);
		EE_BlockWrite(EE_FBL_REPROG_REQ, &temp_load_data_c);
		
	   	diag_load_def_eemap_b = 0;
		diag_load_bl_eep_backup_b = 1; //Its time to take backup for Boot data	

	}


}


/*****************************************************************************************************
*   Function:    EE_Manager
*
*   Description: This function is a scheduled call.  It runs the EE manager which manages the EEPROM
*                operations and interfaces with the EE driver.  This call may be preempted by 
*                another task at any time.  Therefore, when accessing the EE shadow, acquire the
*                resource ResEEPROMShadow and release it as soon as possible.
*
*   Caveats:
*
*****************************************************************************************************/



@far void /*EE_MANAGER_CALL*/ EE_Manager(void)
{
	

	UInt32 lCxsumResult;
	
	ITBM_BOOLEAN        bEEWriteHandled;
	UInt8       byindex;
	UInt8       byDirtyBitOffset;
	UInt8       byEEDirtyBitRead;
	UInt32      lLongBlockWrite;
	
	UInt8       EELocation_temp;
	UInt16      ShadowOffst;
	UInt8       byChecksumIndex;
	
	
	/* preset that we handled any writes */
	bEEWriteHandled = ITBM_FALSE;
	
	/* We can't queue another write if the state machine is busy or if the holdoff timer is running*/

	

	if (EE_IsBusy() != ITBM_TRUE /*&& ITBM_tmrmExpired(TIMER_20MS_EE_WRITE_HOLDOFF)*/ ) {
		

		/* Grab the lock on the eeprom shadow */
		//GetResource(ResEEPROMShadow);

		

		if ( sEEStatusFlags.eeWriteChecksumFlag == ITBM_TRUE ) {
			

			/* we need to queue up the "checksum invalid" flag before we write back any checksummed data */
			*((unsigned long *)abyEEWriteBuff) = (UInt32)(EE_CXSUM_IS_INVALID_DATA);
			/* write the checksum is invalid data to the sector */
			
			EE_ForcedWrite((UInt32)EE_CXSUM_INVALID_FLAG_ADDRESS, abyEEWriteBuff, EE_SECTOR_SIZE);
			/* clear the single shot flag that lets us know to write the invalid flag */
			sEEStatusFlags.eeWriteChecksumFlag = ITBM_FALSE;
			/* Mark the EE checksum for the whole module invalid until it is re-checksumed */
			sEEStatusFlags.eeCxsumIsValid = ITBM_FALSE;
			/* at this point the manager is busy and is going to write something 
			this flag will clear once the state machine is idle */
			sEEStatusFlags.eeManagerBusy = ITBM_TRUE;

		

		}
		else {
			

			/* Go through the shadow and queue the first dirty byte we find.  Check to see if the next
			* location is dirty and also fits in the same sector in an attempt to aggregate multiple small
			* writes into one write that takes the same amount of time.
			*/

			

			for (byindex = 0x00; byindex < EE_NUM_BYTES_IN_DIRTY_ARRAY && !bEEWriteHandled; byindex++) {
				

				/* If the sector dirty marker show any sectors need updating */

				

				if (abyEEDirtyBits[byindex] != 0x00) {
					

					byEEDirtyBitRead = abyEEDirtyBits[byindex];
					byDirtyBitOffset = 0;
					
					/* Shift out the sector that needs to be updated */

					

					while (!(byEEDirtyBitRead & 0x01)) {
						

						byDirtyBitOffset++;
						byEEDirtyBitRead >>= 1;

					

					}
					

					/* Calculate the EE block location to be written 
					shift 3 to have multiple gain of 8 (one byte has 8 flag bits)*/
					EELocation_temp = (byindex << 3) + byDirtyBitOffset;
					
					/* Clean the sector */
					EE_MAKE_SECTOR_CLEAN(EELocation_temp);
					
					/* check if EELocation_temp is valid. normally the condition should true */

					

					if ( (EELocation_temp < EE_NUM_BLOCKS)  
					   ||((EELocation_temp >= EE_NUM_PRIMARY_IDS) && ((EELocation_temp - EE_NUM_PRIMARY_IDS) < EE_NUM_BLOCKS))) {
						

						/* ID0-79 are in primary sections */

						

						if (EELocation_temp < EE_NUM_PRIMARY_IDS ) {
							

							/* start address */
							ShadowOffst = aEEBlockTable[EELocation_temp].wRAMOffset;
							lLongBlockWrite = aEEBlockTable[EELocation_temp].wOffset + EE_EEPROM_START_PHY_ADDRESS;

							

							/* ID80-159 are in backup sections */

						

						}
						else {
							

							/* calc. ee block location index */
							EELocation_temp = EELocation_temp - EE_NUM_PRIMARY_IDS;
							
							/* the ee block has backup section */

							

							if (aEEBlockTable[EELocation_temp].wBackupOffset != EE_NO_BKUP_OFST) {
								

								/* start address */
								ShadowOffst = aEEBlockTable[EELocation_temp].wRAMOffset + SHADOW_BACKUP_START_OFFSET;
								lLongBlockWrite = aEEBlockTable[EELocation_temp].wBackupOffset + EE_EEPROM_START_PHY_ADDRESS;

								

								/* no backup */

							

							}
							else {
								

								/* set EELocation_temp to invalid and move on */
								EELocation_temp = EE_INVALID_BLOCK_ID;

							

							}
						

						}
						

						/* valid ID */

						

						if (EELocation_temp != EE_INVALID_BLOCK_ID) {
							

							/* We can assume at this point that the sector is dirty and that at least one
							* byte needs to be written. copy the shadow block to abyEEWriteBuff[]    */

							

							for (byindex = 0; byindex < aEEBlockTable[EELocation_temp].wSize; byindex++) {
								

								abyEEWriteBuff[byindex]  = byShadowRam[ShadowOffst + byindex];

							

							}
							

							/* Set up the low level EE driver to write with sector to EEPROM */
							//if (EELocation_temp != EE_TT_VTA_JUMP_BLOCK)

							

							{
								

								EE_ForcedWrite(lLongBlockWrite, abyEEWriteBuff, aEEBlockTable[EELocation_temp].wSize);

							

							}
							

							/* multiple eeprom location */
							//else
							//{
							//    EE_ForcedWrite((UInt32)(lLongBlockWrite + byVTABlockOfst * DFLASH_SECTOR_SIZE), 
							//        abyEEWriteBuff, aEEBlockTable[EELocation_temp].wSize);
							//}
							
							
							/* at this point the manager is busy and is going to write something */
							sEEStatusFlags.eeManagerBusy = ITBM_TRUE;

						

						}
					

					}
					

					/* set the flag to break the for-loop. */

					

					bEEWriteHandled = ITBM_TRUE;

				

				}
				else {
					

					/* update checksum at the end of primary or backup bank */

					

					if ((byindex == (EE_NUM_BYTES_IN_PRIMARY_DIRTY_ARRAY - 1)) ||
					   (byindex == (EE_NUM_BYTES_IN_DIRTY_ARRAY - 1))) {
						

						if (byindex == (EE_NUM_BYTES_IN_PRIMARY_DIRTY_ARRAY - 1)) {
							

							byChecksumIndex = EE_CXSUM_FIRST_LOCATION;

						

						}
						else {
							

							byChecksumIndex = EE_CXSUM_FIRST_LOCATION + 1;

						

						}
						

						/* scan through the checksum update array to see if any checksums need to be updated */

						

						while (byChecksumIndex < EE_CXSUM_NUM_BLOCKS) {
							

							/* if this section needs to be updated */

							

							if ( EE_CXSUM_FLAG_IS_SET(abyEECxsumUpdateReq, byChecksumIndex) ) {
								

								/* compute the checksum for this section */
								lCxsumResult = (UInt32)((UInt16)(-(EE_BlockCxsum(aEECxsumTable[byChecksumIndex].wStartOffset, aEECxsumTable[byChecksumIndex].wEndOffset))));
								/* write the checksum to EEPROM */
								EE_BlockWrite(aEECxsumTable[byChecksumIndex].eCxsumBlockLoc, (UInt8*)(&lCxsumResult));
								/* clear the flag for this checksum section */
								EE_CLR_CXSUM_FLAG(abyEECxsumUpdateReq, byChecksumIndex);
								/* mark that we did some processing on EEPROM and we are not steady state */
								bEEWriteHandled = ITBM_TRUE;

							

							}
							

							byChecksumIndex+=2;

						

						}
					

					}
				

				}
			

			}
			

			/* we we still haven't queued anything up in EEPROM - we can assume everything is written back and checksums are updated */
			/* at this point the EE state machine is not writting anything back - steady state */

			

			if ( bEEWriteHandled == ITBM_FALSE ) {
				

				/* if the checksums were previously invalid */

				

				if ( sEEStatusFlags.eeCxsumIsValid == ITBM_FALSE ) {
					

					/* set the RAM flags to signify that the checksums are now valid */
					sEEStatusFlags.eeCxsumIsValid = ITBM_TRUE;
					
					/* we need to queue up the "checksum valid" flag to flag that everything is steady state */
					*((unsigned long *)abyEEWriteBuff) = (UInt32)(EE_CXSUM_IS_VALID_DATA);
					/* write the checksum is invalid data to the sector */
					EE_ForcedWrite(EE_CXSUM_INVALID_FLAG_ADDRESS, abyEEWriteBuff, EE_SECTOR_SIZE);

				

				}
				else {
					

					/* if we had been flagged to initialized backups, they are updated now, flag that backups are initialized */

					

					if ( sEEStatusFlags.eeBkupInitializeReq == ITBM_TRUE ) {
						

						lLongBlockWrite = EE_BACKUPS_INITALIZED;
						EE_BlockWrite(EE_BACKUPS_INITALIZED_BLOCK, (UInt8*)&lLongBlockWrite);
						sEEStatusFlags.eeBkupInitializeReq = ITBM_FALSE;

					

					}
					else {
						

						/* now the EE manager is truely idle - we can handle idle tasks */
						/* we also can assume at this point that EE is fully initialized after a POR */

						

						if ( sEEStatusFlags.eeInitialized == ITBM_FALSE ) {
							

							/* mark that EE is initalized */
							sEEStatusFlags.eeInitialized = ITBM_TRUE;
							/* verify checksums at powerup - we can safely do this since we already checked
							EE_IS_BUSY() above */
							EE_VerifyAllCxsum();

						

						}
						

						sEEStatusFlags.eeManagerBusy = ITBM_FALSE;

					

					}
				

				}
			

			}
		

		}
		

		/* We no longer need the lock on the shadow */
		//ReleaseResource(ResEEPROMShadow);

	

	}
	

	/* Regardless of whether a new write was initiated, we need to run the state machine. */

	

	EE_Handler(); 
	
	/* Harsha Added 10-20-2008 */
	/* If EEP driver is not busy and there is a notification to copy backup boot and wait time of 1sec is over */

	

	if (EE_IsBusy() != ITBM_TRUE) {
		

		if (diag_load_bl_eep_backup_b == 1) {
			

			if (ee_wait_counter_c > 250) {
				//The below write command is dummy to initiate the backup process.
				//Actual writes are based on the primary page values 
				//But the initiation is necessary
				WriteFBLDFlashByte((kEepAddressEcuHwVerNumber),  (UINT8*)&sEEPConstants.fiat_ecu_hw_version_c,1);
				diag_load_bl_eep_backup_b = 0;
			}
			else {
				ee_wait_counter_c++;
			}
		

		}
	

	}


}


/*****************************************************************************************************
*   Function:    EE_GetStatusFlags
*
*   Description: This function returns the current status flags used by the EE manager.  Since EE_BUSY()
*                 is not really representative of the overall status of the EE status we need global access
*                 to there flags.
*   Caveats:     
*
*****************************************************************************************************/



SEEStatusFlags_t EE_MANAGER_CALL EE_GetStatusFlags(void)
{
	

	/* return the loacl EEPROM status flags */
	return sEEStatusFlags;



}


/*****************************************************************************************************
*   Function:    EE_ManagerIsBusy
*
*   Description: This function returns the current state of the EE manager.  Even if the EE Driver is
*                idle, the manager may still be processing data.
*   Caveats:     
*
*****************************************************************************************************/



UInt8 EE_MANAGER_CALL EE_ManagerIsBusy(void)
{
	

	/* check both the manager busy flags and the driver busy flags */

	

	if ( sEEStatusFlags.eeManagerBusy == ITBM_TRUE || EE_IsBusy() == ITBM_TRUE ) {
		

		return ITBM_TRUE;

	

	}
	else {
		

		return ITBM_FALSE;

	

	}


}


/*****************************************************************************************************
*   Function:    EE_BlockWrite
*
*   Description: This function is called from application code to queue up a write to EEPROM. 
*                It is passed the enumerated block ID which references a location in the block table
*                and a pointer to working RAM for the data we need to write.
*   Caveats:     
*
*****************************************************************************************************/



@far void /*EE_MANAGER_CALL*/ EE_BlockWrite(EEBlockLocation elocation, UInt8 *pbysrc)
{
	

	UInt16 windex;
	UInt16 wsize;
	UInt16 wDataOffset;
	ITBM_BOOLEAN bUpdateNeeded;
	
	/* grab the block size from the block table */
	wsize = EE_LOCATION_SIZE(elocation);
	
	/* grab the destination address using the offset from the block table */
	wDataOffset = (UInt16)(aEEBlockTable[elocation].wRAMOffset);
	
	/* Grab the lock on the eeprom shadow */
	//GetResource(ResEEPROMShadow);
	
	/* Assume there is no checksum update needed */
	bUpdateNeeded = ITBM_FALSE;

	

	for (windex = 0; windex < wsize; windex++) {
		

		/* If the two bytes are different, flag the sector they're in as dirty and continue */

		

		if ( byShadowRam[wDataOffset + windex] != pbysrc[windex] ) {
			

			/* There may be a checksum update needed */
			bUpdateNeeded = ITBM_TRUE;
			
			/* call the macro to make the sector dirty - mark it for a write */
			EE_MAKE_SECTOR_DIRTY(elocation);
			
			/* Copy the value into the shadow */
			byShadowRam[wDataOffset + windex] = pbysrc[windex];

		

		}
	

	}
	

	/* if this is a backup block */

	

	if ( aEEBlockTable[elocation].wBackupOffset != EE_NO_BKUP_OFST ) {
		

		wDataOffset += SHADOW_BACKUP_START_OFFSET;

		

		for (windex = 0; windex < wsize; windex++) {
			

			/* If the two bytes are different, flag the sector they're in as dirty and continue */

			

			if ( byShadowRam[wDataOffset + windex] != pbysrc[windex] ) {
				

				/* There may be a checksum update needed */
				bUpdateNeeded = ITBM_TRUE;
				
				/* call the macro to make the sector dirty - mark it for a write */
				EE_MAKE_SECTOR_DIRTY(elocation + EE_NUM_PRIMARY_IDS);
				
				/* Copy the value into the shadow */
				byShadowRam[wDataOffset + windex] = pbysrc[windex];

			

			}
		

		}
	

	}
	

	/* We no longer need the lock on the shadow */
	//ReleaseResource(ResEEPROMShadow);

	

	if (bUpdateNeeded == ITBM_TRUE) {
		

		/* If this block has a associated checksum */

		

		if ( aEEBlockTable[elocation].sChecksumLoc.byChecksumBlock != EE_CXSUM_NONE ) {
			

			/* Set the update flag for this block's checksum location */
			EE_SET_CXSUM_FLAG(abyEECxsumUpdateReq, aEEBlockTable[elocation].sChecksumLoc.byChecksumBlock);

			

			if ( aEEBlockTable[elocation].sChecksumLoc.byBkupChecksumBlock != EE_CXSUM_NONE ) {
				

				/* Set the update flag for this block's checksum location */
				EE_SET_CXSUM_FLAG(abyEECxsumUpdateReq, aEEBlockTable[elocation].sChecksumLoc.byBkupChecksumBlock);

			

			}
			

			if ( sEEStatusFlags.eeCxsumIsValid == ITBM_TRUE ) {
				

				/* set flag to write the checksum flag to invalid */
				sEEStatusFlags.eeWriteChecksumFlag = ITBM_TRUE;

			

			}
		

		}
	

	}


}


/*****************************************************************************************************
*   Function:    EE_FarBlockWrite
*
*   Description: This function is called from application code to queue up a write to EEPROM. 
*                It is passed the enumerated block ID which references a location in the block table
*                and a pointer to working RAM for the data we need to write.
*   Caveats:     the function not used
*
*****************************************************************************************************/
#if (0)



void EE_MANAGER_CALL EE_FarBlockWrite(EEBlockLocation elocation, @far UInt8 *pbysrc)
{
	

	UInt16 windex;
	UInt16 wsize;
	UInt16 wDataOffset;
	ITBM_BOOLEAN bUpdateNeeded;
	
	/* grab the block size from the block table */
	wsize = EE_LOCATION_SIZE(elocation);
	
	/* grab the destination address using the offset from the block table */
	wDataOffset = (UInt16)(aEEBlockTable[elocation].wOffset);
	
	/* Grab the lock on the eeprom shadow */
	//GetResource(ResEEPROMShadow);
	
	/* Assume there is no checksum update needed */
	bUpdateNeeded = ITBM_FALSE;

	

	for (windex = 0; windex < wsize; windex++) {
		

		/* If the two bytes are different, flag the sector they're in as dirty and continue */

		

		if ( byShadowRam[wDataOffset + windex] != pbysrc[windex] ) {
			

			/* There may be a checksum update needed */
			bUpdateNeeded = ITBM_TRUE;
			
			/* call the macro to make the sector dirty - mark it for a write */
			EE_MAKE_SECTOR_DIRTY(((UInt16)(wDataOffset+windex) & 0xFFFC));
			
			/* Copy the value into the shadow */
			byShadowRam[wDataOffset + windex] = pbysrc[windex];

		

		}
	

	}
	

	/* if this is a backup block */

	

	if ( aEEBlockTable[elocation].wBackupOffset != EE_NO_BKUP_OFST ) {
		

		wDataOffset = (UInt16)(aEEBlockTable[elocation].wBackupOffset);

		

		for (windex = 0; windex < wsize; windex++) {
			

			/* If the two bytes are different, flag the sector they're in as dirty and continue */

			

			if ( byShadowRam[wDataOffset + windex] != pbysrc[windex] ) {
				

				/* There may be a checksum update needed */
				bUpdateNeeded = ITBM_TRUE;
				
				/* call the macro to make the sector dirty - mark it for a write */
				EE_MAKE_SECTOR_DIRTY(((UInt16)(wDataOffset+windex) & 0xFFFC));
				
				/* Copy the value into the shadow */
				byShadowRam[wDataOffset + windex] = pbysrc[windex];

			

			}
		

		}
	

	}
	

	/* We no longer need the lock on the shadow */
	//ReleaseResource(ResEEPROMShadow);

	

	if (bUpdateNeeded == ITBM_TRUE) {
		

		/* If this block has a associated checksum */

		

		if ( aEEBlockTable[elocation].sChecksumLoc.byChecksumBlock != EE_CXSUM_NONE ) {
			

			/* Set the update flag for this block's checksum location */
			EE_SET_CXSUM_FLAG(abyEECxsumUpdateReq, aEEBlockTable[elocation].sChecksumLoc.byChecksumBlock);

			

			if ( aEEBlockTable[elocation].sChecksumLoc.byBkupChecksumBlock != EE_CXSUM_NONE ) {
				

				/* Set the update flag for this block's checksum location */
				EE_SET_CXSUM_FLAG(abyEECxsumUpdateReq, aEEBlockTable[elocation].sChecksumLoc.byBkupChecksumBlock);

			

			}
			

			if ( sEEStatusFlags.eeCxsumIsValid == ITBM_TRUE ) {
				

				/* set flag to write the checksum flag to invalid */
				sEEStatusFlags.eeWriteChecksumFlag = ITBM_TRUE;

			

			}
		

		}
	

	}


}


#endif



/*****************************************************************************************************
*   Function:    EE_BlockRead
*
*   Description: This function is called from application code and is used to read blocks from shadow
*                RAM into working RAM.  It is passed an enumerated block ID and returns the block data
*                in working RAM.   
*
*   Caveats:     
*
*****************************************************************************************************/
void EE_MANAGER_CALL EE_BlockRead(EEBlockLocation elocation, UInt8 *pbydest)
{
	

	UInt16 windex;
	UInt16 wsize;
	UInt16 wDataOffset;
	
	/* grab the block size from the block table */
	wsize = EE_LOCATION_SIZE(elocation);
	
	/* check to see if we have flagged that we had a checksum failure on this block */
	/* if we had a primary checksum failure, read the data out of the secondary block */
	/* if the secondary block failed, keep reading out of the secondary block */    
	
	/* if this is a block with backup and the block is flagged as having failed - use the backup */

	

	if ( (aEEBlockTable[elocation].wBackupOffset != EE_NO_BKUP_OFST) &&
	   (EE_CXSUM_FLAG_IS_SET(abyEECxsumFailedFlags, aEEBlockTable[elocation].sChecksumLoc.byChecksumBlock)) ) {
		

		/* we had a failure - read out of the secondary block */
		/* grab the source address using the offset from the block table */
		wDataOffset = (UInt16)(aEEBlockTable[elocation].wRAMOffset + SHADOW_BACKUP_START_OFFSET);

	

	}
	else {
		

		/* grab the source address using the offset from the block table */
		wDataOffset = (UInt16)(aEEBlockTable[elocation].wRAMOffset);

	

	}
	

	/* Grab the lock on the eeprom shadow */
	//GetResource(ResEEPROMShadow);
	
	/* Loop through and read shadow RAM into working RAM */

	

	for (windex = 0; windex < wsize; windex++) {
		

		pbydest[windex] = byShadowRam[wDataOffset + windex];

	

	}
	

	/* We no longer need the lock on the shadow */
	//ReleaseResource(ResEEPROMShadow);



}


/*****************************************************************************************************
*   Function:    EE_FarBlockRead
*
*   Description: This function is called from application code and is used to read blocks from shadow
*                RAM into working RAM.  It is passed an enumerated block ID and returns the block data
*                in working RAM.   
*
*   Caveats:     function not used
*
*****************************************************************************************************/
#if (0)



void EE_MANAGER_CALL EE_FarBlockRead(EEBlockLocation elocation, @far UInt8 *pbydest)
{
	

	UInt16 windex;
	UInt16 wsize;
	UInt16 wDataOffset;
	
	/* grab the block size from the block table */
	wsize = EE_LOCATION_SIZE(elocation);
	
	/* check to see if we have flagged that we had a checksum failure on this block */
	/* if we had a primary checksum failure, read the data out of the secondary block */
	/* if the secondary block failed, keep reading out of the secondary block */    
	
	/* if this is a block with backup and the block is flagged as having failed - use the backup */

	

	if ( (aEEBlockTable[elocation].wBackupOffset != EE_NO_BKUP_OFST) &&
	   (EE_CXSUM_FLAG_IS_SET(abyEECxsumFailedFlags, aEEBlockTable[elocation].sChecksumLoc.byChecksumBlock)) ) {
		

		/* we had a failure - read out of the secondary block */
		/* grab the source address using the offset from the block table */
		wDataOffset = (UInt16)(aEEBlockTable[elocation].wBackupOffset);

	

	}
	else {
		

		/* grab the source address using the offset from the block table */
		wDataOffset = (UInt16)(aEEBlockTable[elocation].wOffset);

	

	}
	

	/* Grab the lock on the eeprom shadow */
	//GetResource(ResEEPROMShadow);
	
	/* Loop through and read shadow RAM into working RAM */

	

	for (windex = 0; windex < wsize; windex++) {
		

		pbydest[windex] = byShadowRam[wDataOffset + windex];

	

	}
	

	/* We no longer need the lock on the shadow */
	//ReleaseResource(ResEEPROMShadow);



}


#endif



/*****************************************************************************************************
*   Function:    EE_DirectWrite
*
*   Description: This function is called from the diagnostic handler and is used to write to
*                EEPROM.  It takes a 32-bit physical addess a data buffer pointer and a
*                buffer size.  It returns a 1 if there was an error setting up the write, else 0.
*   Caveats:     
*****************************************************************************************************/
UInt8 EE_MANAGER_CALL EE_DirectWrite(UInt32 eedestaddr, UInt8 *pbysrc, UInt8 bysize)
{
	

	ITBM_BOOLEAN bUpdateNeeded;
	UInt16 wLogicalStartOffst;
	UInt16 wLogicalEndOffst;
	
	UInt8 byBlockIndex;
	UInt8 EEBlockIndex_temp;
	UInt16 byindex;
	UInt16 ShadowBackupOffset_temp;
	UInt16 ShadowAddrOffset_temp;
	UInt16 srcAddrOffst;
	
	/* if the passed or calculated EEPROM destination address is out of range */

	

	if ( (eedestaddr < EE_EEPROM_START_PHY_ADDRESS) || ((eedestaddr+bysize) >= EE_EEPROM_END_PHY_ADDRESS) ) {
		

		/* error: the address is out of range */
		return 1;

	

	}
	

	/* set up the logical address */

	

	wLogicalStartOffst = eedestaddr - EE_EEPROM_START_PHY_ADDRESS;
	wLogicalEndOffst = (eedestaddr+(bysize-1)) - EE_EEPROM_START_PHY_ADDRESS;
	
	/* set to invalid value */
	EEBlockIndex_temp = EE_INVALID_BLOCK_ID;
	
	/* check if wLogicalStartOffst fall within used range */

	

	for (byBlockIndex = 1; ((byBlockIndex < EEADDR_TAB_SIZE) && (EEBlockIndex_temp == EE_INVALID_BLOCK_ID)); byBlockIndex++) {
		

		/* check if wLogicalStartOffst fall within used range */

		

		if ((wLogicalStartOffst >= aEEBlockAddrTable[byBlockIndex].wOffset ) &&
		   (wLogicalStartOffst < (aEEBlockAddrTable[byBlockIndex].wOffset + aEEBlockAddrTable[byBlockIndex].wSize))) {
			

			/* start address is in the primary bank */

			

			if (aEEBlockAddrTable[byBlockIndex].sEEBlockLoc < EE_NUM_PRIMARY_IDS) {
				

				EEBlockIndex_temp = aEEBlockAddrTable[byBlockIndex].sEEBlockLoc;
				ShadowBackupOffset_temp = 0;

				

				/* start address is in the backup bank */

			

			}
			else {
				

				EEBlockIndex_temp = aEEBlockAddrTable[byBlockIndex].sEEBlockLoc - EE_NUM_PRIMARY_IDS;
				ShadowBackupOffset_temp = SHADOW_BACKUP_START_OFFSET;

			

			}
			

			ShadowAddrOffset_temp = wLogicalStartOffst - aEEBlockAddrTable[byBlockIndex].wOffset;
			srcAddrOffst = 0;

			

			/* check if wLogicalStartOffst fall within eeblock gap */

		

		}
		else {
			

			if ((wLogicalStartOffst < aEEBlockAddrTable[byBlockIndex].wOffset) &&
			   (wLogicalStartOffst >= (aEEBlockAddrTable[byBlockIndex-1].wOffset + aEEBlockAddrTable[byBlockIndex - 1].wSize))) {
				

				/* start address is in the primary bank */

				

				if (aEEBlockAddrTable[byBlockIndex].sEEBlockLoc < EE_NUM_PRIMARY_IDS) {
					

					EEBlockIndex_temp = aEEBlockAddrTable[byBlockIndex].sEEBlockLoc;
					ShadowBackupOffset_temp = 0;

					

					/* start address is in the backup bank */

				

				}
				else {
					

					EEBlockIndex_temp = aEEBlockAddrTable[byBlockIndex].sEEBlockLoc - EE_NUM_PRIMARY_IDS;
					ShadowBackupOffset_temp = SHADOW_BACKUP_START_OFFSET;

				

				}
				

				ShadowAddrOffset_temp = 0;
				srcAddrOffst =  aEEBlockAddrTable[byBlockIndex].wOffset - wLogicalStartOffst;

			

			}
		

		}
	

	}
	

	/* found shadow start address */

	

	if ((EEBlockIndex_temp != EE_INVALID_BLOCK_ID) && (srcAddrOffst < bysize)) {
		

		/* Assume there is no checksum update needed */
		bUpdateNeeded = ITBM_FALSE;
		
		/* Grab the lock on the eeprom shadow */
		//GetResource(ResEEPROMShadow);
		
		byindex = srcAddrOffst;

		

		while (byindex < bysize) {
			

			/* If the two bytes are different, flag the sector they're in as dirty and continue */

			

			if ( byShadowRam[aEEBlockTable[EEBlockIndex_temp].wRAMOffset + ShadowAddrOffset_temp + ShadowBackupOffset_temp] != pbysrc[byindex] ) {
				

				/* There may be a checksum update needed */
				bUpdateNeeded = ITBM_TRUE;
				
				/* call the macro to make the sector dirty - mark it for a write */

				

				if (ShadowBackupOffset_temp < SHADOW_BACKUP_START_OFFSET) {
					

					EE_MAKE_SECTOR_DIRTY(EEBlockIndex_temp);

				

				}
				else {
					

					EE_MAKE_SECTOR_DIRTY(EEBlockIndex_temp + EE_NUM_PRIMARY_IDS);

				

				}
				

				/* Copy the value into the shadow */
				byShadowRam[aEEBlockTable[EEBlockIndex_temp].wRAMOffset + ShadowAddrOffset_temp + ShadowBackupOffset_temp] = pbysrc[byindex];

			

			}
			

			ShadowAddrOffset_temp++;
			
			/* the next data block reached */

			

			if (ShadowAddrOffset_temp >= aEEBlockTable[EEBlockIndex_temp].wSize) {
				

				ShadowAddrOffset_temp = 0;
				EEBlockIndex_temp++;
				
				/* eeprom gap */
				byindex += (aEEBlockTable[EEBlockIndex_temp].wOffset -
				            aEEBlockTable[EEBlockIndex_temp - 1].wOffset - aEEBlockTable[EEBlockIndex_temp - 1].wSize);

			

			}
			

			byindex++;

		

		}
		

		/* if shadow RAM is changed, we will change checksum */

		

		if (bUpdateNeeded == ITBM_TRUE) {
			

			/* scan through the checksum update array and see if the write address is in a checksum area */

			

			for (byindex = EE_CXSUM_FIRST_LOCATION; byindex < EE_CXSUM_NUM_BLOCKS; byindex++) {
				

				/* three possible situations: */
				/* 1. the start address is inside the checksum block */
				/* 2. the end address is inside the checksum block */
				/* 3. the checksum block is totally contained between the start and end addresses */

				

				if ( ((wLogicalStartOffst >= aEECxsumTable[byindex].wStartOffset)
				     && (wLogicalStartOffst <= aEECxsumTable[byindex].wEndOffset))
				     || ((wLogicalEndOffst >= aEECxsumTable[byindex].wStartOffset)
				     && (wLogicalEndOffst <= aEECxsumTable[byindex].wEndOffset))
				     || ((wLogicalStartOffst < aEECxsumTable[byindex].wStartOffset)
				     && (wLogicalEndOffst > aEECxsumTable[byindex].wEndOffset)) ) {
					

					/* Set the update flag for this block's checksum location */
					EE_SET_CXSUM_FLAG(abyEECxsumUpdateReq, byindex);

					

					if ( sEEStatusFlags.eeCxsumIsValid == ITBM_TRUE ) {
						

						sEEStatusFlags.eeWriteChecksumFlag = ITBM_TRUE;

					

					}
				

				}
			

			}
		

		}
		

		/* We no longer need the lock on the shadow */
		//ReleaseResource(ResEEPROMShadow);
		
		/* return no error */

		

		return 0;

	

	}
	else {
		

		/* error: the address is out of range */
		return 1;

	

	}


}


/*****************************************************************************************************
*   Function:    EE_SID31_FACallback
*
*   Description: For all blocks that have backups, copies primary block into backup
*   Caveats: 
*
*****************************************************************************************************/



void EE_MANAGER_CALL EE_SID31_FACallback(void)
{
	

	UInt8 byindex;
	
	/* loop through the EE blocks */

	

	for (byindex=0; byindex < EE_CXSUM_NUM_SECTIONS; byindex++) {
		

		EE_SectionCopy(byindex, EE_COPY_PRIMARY2BACKUP);

	

	}
	

	/* loop through and flag all checksums need updating and clear the failed flag */

	

	for (byindex=EE_CXSUM_FIRST_LOCATION; byindex < EE_CXSUM_NUM_BLOCKS; byindex++) {
		

		/* Set the update flag for this block's checksum location */
		EE_SET_CXSUM_FLAG(abyEECxsumUpdateReq, byindex);
		/* clear the failed flag since we are restoring the backup from primary which */
		/* nulls any backup we had stored.  If the primary was bad it will be bad in backup */
		EE_CLR_CXSUM_FLAG(abyEECxsumFailedFlags, byindex);

	

	}
	

	/* write the now cleared failed flag array to EEPROM */
	EE_WriteFailedFlags();



}


/*****************************************************************************************************
*   Function:    EE_ValidateCxsum
*
*   Description: Checksums the checksum block and verifies it is valid.  Returns 0 for valid checksum. 
*
*   Caveats:     
*
*****************************************************************************************************/
/*UInt16 EE_MANAGER_CALL EE_ValidateCxsum(EEECxsumBlock eCxsumBlock)*/



@far u_16Bit /*EE_MANAGER_CALL*/ EE_ValidateCxsum(EEECxsumBlock eCxsumBlock)
{
	

	UInt16 wResult;
	UInt32 lCxsumval;
	
	/* we can only validate if EEPROM is not busy */

	

	if ( EE_IsBusy() != ITBM_TRUE ) {
		

		/* read the stored checksum value from EEPROM */
		/* make sure we set up the paged EEPROM information */
		EPAGE = GetEPage(EE_EEPROM_START_PHY_ADDRESS + aEEBlockTable[aEECxsumTable[eCxsumBlock].eCxsumBlockLoc].wOffset);
		lCxsumval = *((UInt32*)(EEPROM_LOGICAL_START + GetEOffset(EE_EEPROM_START_PHY_ADDRESS + aEEBlockTable[aEECxsumTable[eCxsumBlock].eCxsumBlockLoc].wOffset)));
		/* compute the checksum for this section */
		wResult = (EE_BlockCxsum(aEECxsumTable[eCxsumBlock].wStartOffset, aEECxsumTable[eCxsumBlock].wEndOffset));
		/* add the stored checksum to the computed checksum */
		wResult += (UInt16)(lCxsumval);

	

	}
	else {
		

		/* we don't want to fault if EEPROM is busy, send no fault */
		wResult = 0;

	

	}
	

	return wResult;



}


/*****************************************************************************************************
*   Function:    EE_RefreshEEToRAM
*
*   Description: Takes a passed flag and refreshes blocks of EEPROM to RAM based on that flag 
*
*   Caveats: called upon powerup.  It needs to verify if it will be used upon wakeup.     
*
*****************************************************************************************************/



void EE_RefreshEEToRAM(void)
{
	

	UInt8 byindex;
	UInt16 wBlockSize;
	UInt32 lDestAddr;
	UInt32 lSrcAddr;
	
	/* we can only copy if EEPROM is not busy */

	

	if ( EE_IsBusy() != ITBM_TRUE ) {
		

		/* loop through all the configured blocks of EEPROM */

		

		for (byindex=0; byindex < EE_NUM_BLOCKS; byindex++) {
			

			/* not EE_TT_VTA_JUMP_BLOCK */
			//if(byindex != EE_TT_VTA_JUMP_BLOCK)

			

			{
				

				/* Grab the lock on the eeprom shadow */
				//GetResource(ResEEPROMShadow);
				
				/* grab the size, source and destination address using the offset from the block table */
				wBlockSize = aEEBlockTable[byindex].wSize;
				//               lDestAddr = (((UInt32)aEEBlockTable[byindex].wRAMOffset) + EE_RAM_SHADOW_START_PHY_ADDRESS);
				lDestAddr = (UInt32)(aEEBlockTable[byindex].wRAMOffset + (UInt16)(&byShadowRam)) + 0xFC000L;
				lSrcAddr = (aEEBlockTable[byindex].wOffset + EE_EEPROM_START_PHY_ADDRESS);
				UniversalMemCopy(lDestAddr, lSrcAddr, wBlockSize);
				
				/* if the block has a backup - refresh the backup too */

				

				if ( aEEBlockTable[byindex].wBackupOffset != EE_NO_BKUP_OFST ) {
					

					//                    lDestAddr = (aEEBlockTable[byindex].wRAMOffset + SHADOW_BACKUP_START_OFFSET + EE_RAM_SHADOW_START_PHY_ADDRESS);
					                    lDestAddr = (UInt32)(aEEBlockTable[byindex].wRAMOffset + (UInt16)(&byShadowRam)) + 0xFC000L + SHADOW_BACKUP_START_OFFSET;
					                    lSrcAddr = (aEEBlockTable[byindex].wBackupOffset + EE_EEPROM_START_PHY_ADDRESS);
					                    UniversalMemCopy(lDestAddr, lSrcAddr, wBlockSize);

				

				}
				

				/* We no longer need the lock on the shadow */
				//ReleaseResource(ResEEPROMShadow);

			

			}
		

		}
	

	}


}


/*****************************************************************************************************
*   Function:    EE_SwapPrimaryAndBackup
*
*   Description: Copies primary to backup or backup to primary, depending on the request.
*
*****************************************************************************************************/



void EE_SectionCopy(EECxsumSection_t eSection, EESectionCopyType_t eCopyType)
{
	

	UInt8 windex;
	UInt16 wSrcOffst;
	UInt16 wDestOffst;
	UInt8  EELocation_temp;
	UInt8  EEBlock_StartID;
	UInt8  EEBlock_EndID;
	EEBlockLocation EEID_index;
	
	/* grab ee block start and end IDs */
	EEBlock_StartID = aEECxsumTable[aEECxsumSectionTable[eSection].eCxsumPrimaryBlock].EEBlockStart;
	EEBlock_EndID = aEECxsumTable[aEECxsumSectionTable[eSection].eCxsumPrimaryBlock].eCxsumBlockLoc;
	
	/* loop thru all the block IDs */

	

	for (EEID_index = EEBlock_StartID; EEID_index < EEBlock_EndID; EEID_index++) {
		

		/* copy backup to primary data */

		

		if ( eCopyType == EE_COPY_BACKUP2PRIMARY ) {
			

			/* grab the source address using the offset from the block table */
			wSrcOffst = aEEBlockTable[EEID_index].wRAMOffset + SHADOW_BACKUP_START_OFFSET;
			
			/* grab the destination address using the offset from the passed destination */
			wDestOffst = aEEBlockTable[EEID_index].wRAMOffset;
			
			/* destination ID */
			EELocation_temp = EEID_index;

			

			/* copy primary data to backup data */

		

		}
		else {
			

			/* grab the source address using the offset from the block table */
			wSrcOffst = aEEBlockTable[EEID_index].wRAMOffset;
			
			/* grab the destination address using the offset from the passed destination */
			wDestOffst = aEEBlockTable[EEID_index].wRAMOffset + SHADOW_BACKUP_START_OFFSET;
			
			/* destination ID */
			EELocation_temp = EEID_index + EE_NUM_PRIMARY_IDS ;

		

		}
		

		/* Grab the lock on the eeprom shadow */
		//GetResource(ResEEPROMShadow);
		
		/* for each ee ID, check the elements */

		

		for (windex = 0; windex < aEEBlockTable[EEID_index].wSize; windex++) {
			

			/* If the two bytes are different, flag the sector they're in as dirty and continue */

			

			if ( byShadowRam[wDestOffst + windex] != byShadowRam[wSrcOffst + windex] ) {
				

				/* call the macro to make the sector dirty - mark it for a write */
				EE_MAKE_SECTOR_DIRTY(EELocation_temp);
				
				/* Copy the value into the shadow */
				byShadowRam[wDestOffst + windex] = byShadowRam[wSrcOffst + windex];

			

			}
		

		}
		

		/* We no longer need the lock on the shadow */
		//ReleaseResource(ResEEPROMShadow);

	

	}
	

	/* always update the checksum as even if the data is the same we may still have a mismatch on the checksum */
	/* flag that we are going to write a checksum section */

	

	if ( sEEStatusFlags.eeCxsumIsValid == ITBM_TRUE ) {
		

		sEEStatusFlags.eeWriteChecksumFlag = ITBM_TRUE;

	

	}
	

	if ( eCopyType == EE_COPY_BACKUP2PRIMARY ) {
		

		/* update the checksum for the primary block */
		EE_SET_CXSUM_FLAG(abyEECxsumUpdateReq, aEECxsumSectionTable[eSection].eCxsumPrimaryBlock);

	

	}
	else {
		

		/* update the checksum for the backup block */
		EE_SET_CXSUM_FLAG(abyEECxsumUpdateReq, aEECxsumSectionTable[eSection].eCxsumBackupBlock);

	

	}


}


/*****************************************************************************************************
*   Function:    EE_PowerFailureRecovery
*
*   Description: Use the backup block to try and recover data if we lose power during a write
*                 Since we read the flag from EEPROM that flags if we were not complete writing
*                 before power was lost we know something is bad.
*
*****************************************************************************************************/



void EE_PowerFailureRecovery(void)
{
	

	UInt8 byindex;
	ITBM_BOOLEAN bCanRecover;
	EEECxsumBlock eCxsumPrimaryBlock;
	EEECxsumBlock eCxsumBackupBlock;
	
	/* loop through the EE blocks */

	

	for (byindex=0; byindex < EE_CXSUM_NUM_SECTIONS; byindex++) {
		

		bCanRecover = ITBM_FALSE;
		
		/* pre-read these values since they are used a lot in the following code */
		eCxsumPrimaryBlock = aEECxsumSectionTable[byindex].eCxsumPrimaryBlock;
		eCxsumBackupBlock = aEECxsumSectionTable[byindex].eCxsumBackupBlock;
		/* checksum the primary section */

		

		if ( EE_ValidateCxsum(eCxsumPrimaryBlock) ) {
			

			/* the primary checksum is BAD - see if the backup is GOOD */

			

			if ( !(EE_ValidateCxsum(eCxsumBackupBlock)) ) {
				

				/* the primary is bad but the backup is good.  This means we were part way through writing the primary
				   when power was lost.  We can use the backup since it wasn't touched yet */
				/* only act if the backup block was not flagged as failed */

				

				if ( !(EE_CXSUM_FLAG_IS_SET(abyEECxsumFailedFlags, eCxsumBackupBlock)) ) {
					

					/* flag that we can recover */
					bCanRecover = ITBM_TRUE;
					/* do a section copy to copy the backup into the primary */
					EE_SectionCopy(byindex, EE_COPY_BACKUP2PRIMARY);

				

				}
			

			}
		

		}
		

		if ( bCanRecover == ITBM_FALSE ) {
			

			/* we couldn't recover */
			/* if the primary block isn't failed, copy the primary into the backup so they match */

			

			if ( !(EE_CXSUM_FLAG_IS_SET(abyEECxsumFailedFlags, eCxsumPrimaryBlock)) ) {
				

				/* do a section copy to copy primary into backup */
				EE_SectionCopy(byindex, EE_COPY_PRIMARY2BACKUP);

			

			}
			else {
				

				/* the primary is failed - since we will never read from it again without running a mode $31/$FA which copies primary to backup
				   and resets the failed flags we can't really do anything except update the checksums */

			

			}
		

		}
	

	}
	

	/* since we don't know exactly which checksums need to be fully updated, update them all */
	EE_UpdateAllCxsum();



}


/*****************************************************************************************************
*   Function:    EE_UpdateAllCxsum
*
*   Description: Updates all of the checksum blocks 
*
*   Caveats:     
*
*****************************************************************************************************/



void EE_UpdateAllCxsum(void)
{
	

	UInt8 byindex;
	
	/* flag that we are going to write a checksum section */

	

	if ( sEEStatusFlags.eeCxsumIsValid == ITBM_TRUE ) {
		

		sEEStatusFlags.eeWriteChecksumFlag = ITBM_TRUE;

	

	}
	

	/* scan through the checksum update array and update all */

	

	for (byindex=EE_CXSUM_FIRST_LOCATION; byindex<EE_CXSUM_NUM_BLOCKS; byindex++) {
		

		/* Set the update flag for this block's checksum location */
		EE_SET_CXSUM_FLAG(abyEECxsumUpdateReq, byindex);

	

	}


}


/*****************************************************************************************************
*   Function:    EE_BlockCxsum
*
*   Description: Returns the 16-bit checksum of the requested range 
*
*   Caveats:     
*
*****************************************************************************************************/



UInt16 EE_BlockCxsum(UInt16 wStartOffset, UInt16 wEndOffset)
{
	

	UInt8 *pbysrc;
	UInt16 windex;
	UInt16 wResult;
	
	/* make sure we set up the paged EEPROM information */
	EPAGE = GetEPage(EE_EEPROM_START_PHY_ADDRESS + wStartOffset);
	pbysrc = (unsigned char *)(EEPROM_LOGICAL_START + GetEOffset(EE_EEPROM_START_PHY_ADDRESS + wStartOffset)); 
	
	/* preclear the checksum result */
	wResult = 0;
	
	/* Loop through the addresses in the section */

	

	for (windex=0; windex<=(wEndOffset-wStartOffset); windex++) {
		

		/* add the current byte to the previous bytes */
		wResult += *(pbysrc);
		
		++pbysrc;

	

	}
	

	return wResult;



}


/*****************************************************************************************************
*   Function:    EE_VerifyAllCxsum
*
*   Description: Loops through all checksums and checks to make sure they are valid.  Sets flags if not.
*
*   Caveats:     
*
*****************************************************************************************************/



void EE_VerifyAllCxsum(void)
{
	

	UInt8 byindex;
	ITBM_BOOLEAN temp;
	
	checksums_valid = ITBM_TRUE;

	

	for (byindex=EE_CXSUM_FIRST_LOCATION; byindex < EE_CXSUM_NUM_BLOCKS; byindex++) {
		

		if ( EE_ValidateCxsum(byindex) ) {
			

			/* we have a checksum failure */
			EE_SET_CXSUM_FLAG(abyEECxsumFailedFlags, byindex);
			
			checksums_valid = ITBM_FALSE;
			
			/* queue up a write to maintain the status of this fault in EEPROM - it will never ever clear */
			EE_WriteFailedFlags();

		

		}
	

	}
	

	temp = checksums_valid ? ITBM_FALSE : ITBM_TRUE;
	/* Report DTC */
	
	//ITBM_diagmHW_BitmaskSetORClear( DTC_ECU_FAULT, DTC_BM_ECU_FAULT_EEPROM_CORRUPTED, temp );



}


/*****************************************************************************************************
*   Function:    EE_WriteFailedFlags
*
*   Description: Writes the failed flags array to EEPROM as three copies 
*  
*   Caveats:     
*
*****************************************************************************************************/



void EE_WriteFailedFlags(void)
{
	

	UInt8 aFailedFlagsSectorCopy[EE_SECTOR_SIZE];
	UInt8 byindex;
	
	*(UInt32 *)&aFailedFlagsSectorCopy = 0;

	

	for (byindex=0; byindex < EE_NUM_BYTES_IN_CXSUM_FLAGS_ARRAY; byindex++) {
		

		aFailedFlagsSectorCopy[byindex] = abyEECxsumFailedFlags[byindex];

	

	}
	

	EE_BlockWrite(EE_CXSUM_FAILED_FLAGS_BLOCK,  (UInt8*)&aFailedFlagsSectorCopy);
	EE_BlockWrite(EE_CXSUM_FAILED_FLAGS2_BLOCK, (UInt8*)&aFailedFlagsSectorCopy);
	EE_BlockWrite(EE_CXSUM_FAILED_FLAGS3_BLOCK, (UInt8*)&aFailedFlagsSectorCopy);



}


/*****************************************************************************************************
*   Function:    EE_ReadFailedFlags
*
*   Description: Reads the failed flags array from EEPROM and votes 
*                The good copy is stored in abyEECXsumFailedFlagsBuffer[0]
*   Caveats:     
*
*****************************************************************************************************/



void EE_ReadFailedFlags(void)
{
	

	UInt8 byindex;
	ITBM_BOOLEAN bFailureDetected;
	UInt8 abyEECXsumFailedFlagsBuffer[EE_NUM_FAILED_FLAG_COPIES][EE_SECTOR_SIZE];
	
	/* preset that there was no failure */
	bFailureDetected = ITBM_FALSE;
	
	EE_BlockRead(EE_CXSUM_FAILED_FLAGS_BLOCK,  (UInt8*)&abyEECXsumFailedFlagsBuffer[0]);
	EE_BlockRead(EE_CXSUM_FAILED_FLAGS2_BLOCK, (UInt8*)&abyEECXsumFailedFlagsBuffer[1]);
	EE_BlockRead(EE_CXSUM_FAILED_FLAGS3_BLOCK, (UInt8*)&abyEECXsumFailedFlagsBuffer[2]);

	

	if ( (*(UInt32*)&abyEECXsumFailedFlagsBuffer[0] == *(UInt32*)&abyEECXsumFailedFlagsBuffer[1])
	   && (*(UInt32*)&abyEECXsumFailedFlagsBuffer[1] == *(UInt32*)&abyEECXsumFailedFlagsBuffer[2]) ) {
		

		/* all copies match, go with copy 1 */

	

	}
	else {
		

		if ( *(UInt32*)&abyEECXsumFailedFlagsBuffer[1] == *(UInt32*)&abyEECXsumFailedFlagsBuffer[2] ) {
			

			/* copy 1 is bad, but 2 and 3 are good.  The write to 1 must have failed */
			*(UInt32 *)&abyEECXsumFailedFlagsBuffer[0] = *(UInt32 *)&abyEECXsumFailedFlagsBuffer[1];
			bFailureDetected = ITBM_TRUE;

		

		}
		else {
			

			/* copy 2 or 3 are bad, go with copy 1 */
			bFailureDetected = ITBM_TRUE;
			/* we should never get this case, but if we do, mark all flags as clear since most likely we will not have */
			/* failed blocks in the system - we don't want to mark good blocks as failed by accident */

			

			if ( *(UInt32*)(&abyEECXsumFailedFlagsBuffer[0]) == 0xFFFFFFFF ) {
				

				*(UInt32 *)&abyEECXsumFailedFlagsBuffer[0] = 0;

			

			}
		

		}
	

	}
	

	for (byindex=0; byindex < EE_NUM_BYTES_IN_CXSUM_FLAGS_ARRAY; byindex++) {
		

		abyEECxsumFailedFlags[byindex] = *(UInt8 *)&abyEECXsumFailedFlagsBuffer[0][byindex];

	

	}
	

	if ( bFailureDetected == ITBM_TRUE ) {
		

		/* if we had a failure we need to write the good data back */
		EE_WriteFailedFlags();

	

	}


}


/*****************************************************************************************************
*   Function:    EE_Manager_Static_Init
*
*   Description: This function init all local variable which is located in paged RAM
*
*   Caveats:     
*                
*                
*
*****************************************************************************************************/



void EE_Manager_Static_Init (void)
{
	

	*(@far UInt8 *)&sEEStatusFlags = 0;
	/* abyEEDirtyBits[], abyEECxsumUpdateReq[], abyEECxsumFailedFlags[] are initialized in EE_PowerupInit()
	   abyEEWriteBuff[] is initialized before used
	   byVTABlockOfst is initialized in EE_VTABlockInit()    */



}



