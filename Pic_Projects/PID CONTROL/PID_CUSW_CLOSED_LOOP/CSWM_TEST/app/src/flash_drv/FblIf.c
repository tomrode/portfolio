/*****************************************************************************
| Project Name: CANfbl for Fiat
|    File Name: FblIf.C
|
|  Description: Interface routines for application
|
|-----------------------------------------------------------------------------
|               C O P Y R I G H T
|-----------------------------------------------------------------------------
| Copyright (c) 2007-2011 by Vector Informatik GmbH, all rights reserved
|
| Please note, that this file contains a collection of callback 
| functions to be used with the Flash Bootloader. These functions may 
| influence the behaviour of the bootloader in principle. Therefore, 
| great care must be taken to verify the correctness of the 
| implementation.
|
| The contents of the originally delivered files are only examples resp. 
| implementation proposals. With regard to the fact that these functions 
| are meant for demonstration purposes only, Vector Informatik�s 
| liability shall be expressly excluded in cases of ordinary negligence, 
| to the extent admissible by law or statute.
|
|-----------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-----------------------------------------------------------------------------
| Initials     Name                      Company
| --------     ---------------------     -------------------------------------
| CB           Christian Baeuerle        Vector Informatik GmbH
|
|-----------------------------------------------------------------------------
|               R E V I S I O N   H I S T O R Y
|-----------------------------------------------------------------------------
| Date        Version   Author  Description
| ----------  --------  ------  ----------------------------------------------
| 2007-08-07  01.00.00  CB      First implementation
| 2007-08-22  01.01.00  CB      Support for 07274
| 2007-09-11  01.02.00  CB      - Sample values for identification
|                               - Added boot software identification
| 2007-10-12  01.03.00  CB      Modified Kit 5   
| 2008-02-06  01.04.00  CB      Support for TI/Tms470
| 2008-02-28  01.05.00  CB      Support for Fj16Lx
| 2008-05-28  01.06.00  CB      Support for 78K0
| 2008-09-04  01.07.00  CB      Support for Xc16x
| 2010-01-18  01.08.00  CB      Support for Fj16Fx
| 2010-06-16  01.09.00  CB      Support for MPC
| 2010-10-20  01.10.00  CB      Support for V850
| 2010-12-14  01.11.00  CB      Chrysler addon
| 2011-01-17  01.12.00  CB      Minor changes
| 2011-01-18  01.13.00  CB      Support for Mcs08
|****************************************************************************/

/*****************************************************************************
 * 								CONTI UPDATES
 * 05-19-2011		Harsha		Numbers updated to suit the chrysler format.
 * 06-13-2011       Harsha      F180, F181, F182 Numbers updated to have latest information. KIT K1.
 * 08-03-2011		Harsha		MKS 81827: Release AA with SW 11.34.00 Information added.
 * 11-30-2011       Harsha      Release AC with SW 11.48.00 Information added.
 * 12-08-2011       Harsha      Release AC with SW 11.49.00 Information added.
 * 02-13-2012       Harsha      Release AD with SW 12.07.00 Information added. 
 *                              MKS 98062 Update Parameters and release info for AD 4.0 release
 * 05-03-2012       Christian   Release AE with SW 12.18.00 Information added.
 *                              MKS 103556 Update Parameters and release info for AE 
|****************************************************************************/
/* Includes *******************************************************************/
#include "fbl_def.h"
#include "FblIf.h"

/* --- Version check --- */
#if ( FBLIF_FIAT_VERSION != 0x0113u ) || \
    ( FBLIF_FIAT_RELEASE_VERSION != 0x00u )
# error "Error in FBLIF.C: Source and header file are inconsistent!"
#endif

/* Defines ********************************************************************/

#if  ((!defined(FBL_ENABLE_KIT_K1)) &&\
      (!defined(FBL_ENABLE_KIT_K2)) &&\
      (!defined(FBL_ENABLE_KIT_K3)) &&\
      (!defined(FBL_ENABLE_KIT_K4)) &&\
      (!defined(FBL_ENABLE_KIT_K5)) )      

/* If none of the Kit files has to be created, create kit K1 */      
# define FBL_ENABLE_KIT_K1
#endif

#if defined(FBL_ENABLE_KIT_K1)
/* Kit K1 ********************************************************************/
/* Kit K1 is the software under test                                         */
/* ***************************************************************************/
# pragma section const {APPLSWHDR}
V_MEMROM0 V_MEMROM1 tApplicationSwHeader V_MEMROM2  applSoftwareHeader = 
{
   /* F192 - F195: Versions */   
	"1018       ", //ASCII Representation for the HW Part Number
   //{0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39,0x30,0x31},
   0x01,
   //0x33,
	"121800     ", //ASCII Representation for the SW Part Number MKS 98062 
   //{0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39,0x30,0x31}, //For testing the FIAT
   {0x00, 0x09},
   //{0x5A, 0xA5}, //For Testing the FIAT
#if defined( FBL_ENABLE_CHRYSLER_ADDON )

   /* F122: Software part number */
   { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
   
   /* F132: Ecu part number */
   { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
#endif

   /* F180: bootSoftware identification */
   {0x01, 0x0B, 0x27, 0x00, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x00, 0xC6},
      
   /* F181: applicationSoftware identification */
   /* MKS 98062 */
   {0x01, 0x0C, 0x12, 0x00, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x00, 0xC6},

   /* F182: applicationData identification */
   /* MKS 98062 */
   {0x01, 0x0C, 0x12, 0x00, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x00, 0xC6},
   
   /* F187: SparePartNumber / drawingNumber */
   /* Harsha updated with Blank which is representation of $20 in ASCII */
//   "12345678901",
   "           ",
   
   
   /* F196: exhaustRegulationOrTypeApprovalNumber */
   /* Harsha updated with Blank which is representation of $20 in ASCII */
//   "654321",
   "      ",
   
   /* F1A5: identificationOption (ISO Code) */
   /* Harsha updated: ISO Code is dummy for the CUSW */
   {0x20, 0x20, 0x20, 0x20, 0x20}
   //{0xA3, 0x52, 0xA2, 0x51, 0xA4}

};
# pragma section const {}

# pragma section const {APPLDATAHDR}

V_MEMROM0 V_MEMROM1 tApplicationDataHeader V_MEMROM2  applDataHeader =
{
   /* F192 - F193: Hardware Versions */   
   "HwNumb     ",
   0xA6,
   
   /* F194 - F195: Software Versions */         
   "NewSwNumb  ",
   {0xA6, 0xA7},
#if defined( FBL_ENABLE_CHRYSLER_ADDON )

   /* F122: Software part number */
   { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
   
   /* F132: Ecu part number */
   { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}
#endif
};
# pragma section const {}
#endif   /* FBL_ENABLE_KIT_K1 */


/* Kit K2 ********************************************************************/
/* Kit K2 is K1 with different identification code                           */ 
/* ***************************************************************************/
#if defined(FBL_ENABLE_KIT_K2)

# pragma section const {APPLSWHDR}
V_MEMROM0 V_MEMROM1 tApplicationSwHeader V_MEMROM2  applSoftwareHeader = 
{
   /* F192 - F195: Versions */   
   "HwNumb     ",
   0xA6,
   "12345678901",
   {0x5A, 0xA5},
#if defined( FBL_ENABLE_CHRYSLER_ADDON )

   /* F122: Software part number */
   { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
   
   /* F132: Ecu part number */
   { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
#endif

   /* F180: bootSoftware identification */
   {0x01, 0x00, 0x0E, 0x00, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
   
   /* F181: applicationSoftware identification */
   {0x01, 0x00, 0x10, 0x00, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},

   /* F182: applicationData identification */
   {0x01, 0x00, 0x10, 0x00, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
   
   /* F187: SparePartNumber / drawingNumber */
   "12345678901",
      
   /* F196: exhaustRegulationOrTypeApprovalNumber */
   "123456",
   
   /* F1A5: identificationOption (ISO Code) */
   {0x01, 0x51, 0xA2, 0x52, 0xA2}
};
# pragma section const {}
# pragma section const {APPLDATAHDR}
V_MEMROM0 V_MEMROM1 tApplicationDataHeader V_MEMROM2  applDataHeader =
{
   /* F192 - F193: Hardware Versions */   
   "HwNumb     ",
   0xA6,
   
   /* F194 - F195: Software Versions */         
   "12345678901",
   {0x5A, 0xA5},
#if defined( FBL_ENABLE_CHRYSLER_ADDON )

   /* F122: Software part number */
   { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
   
   /* F132: Ecu part number */
   { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}
#endif
};
# pragma section const {}
#endif   /* FBL_ENABLE_KIT_K2 */

/* Kit K3 ********************************************************************/
/* Kit K3 is not compatible with ECU hardware                                */ 
/* ***************************************************************************/
#if defined(FBL_ENABLE_KIT_K3)

# pragma section const {APPLSWHDR}

V_MEMROM0 V_MEMROM1 tApplicationSwHeader V_MEMROM2  applSoftwareHeader = 
{
   /* F192 - F193: Hardware Versions */   
   "12345678901",
   0x33,
   
   /* F194 - F195: Software Versions */         
   "12345678901",
   {0x5A, 0xA5},
#if defined( FBL_ENABLE_CHRYSLER_ADDON )

   /* F122: Software part number */
   { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
   
   /* F132: Ecu part number */
   { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
#endif

   /* F180: bootSoftware identification */
   {0x01, 0x00, 0x0E, 0x00, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
      
   /* F181: applicationSoftware identification */
   {0x01, 0x00, 0x10, 0x00, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},

   /* F182: applicationData identification */
   {0x01, 0x00, 0x10, 0x00, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},

   /* F187: SparePartNumber / drawingNumber */
   "12345678901",
   
   /* F196: exhaustRegulationOrTypeApprovalNumber */
   "123456",
   
   /* F1A5: identificationOption (ISO Code) */
   {0x01, 0x51, 0xA2, 0x52, 0xA3}
};
# pragma section const {}

# pragma section const {APPLDATAHDR}
V_MEMROM0 V_MEMROM1 tApplicationDataHeader V_MEMROM2  applDataHeader =
{
   /* F192 - F193: Hardware Versions */   
   "12345678901",
   0x33,
   
   /* F194 - F195: Software Versions */         
   "12345678901",
   {0x5A, 0xA5},
#if defined( FBL_ENABLE_CHRYSLER_ADDON )

   /* F122: Software part number */
   { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
   
   /* F132: Ecu part number */
   { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}
#endif
};
# pragma section const {}

#endif /* FBL_ENABLE_KIT_K3 */

/* Kit K4 ********************************************************************/
/* Kit K4 has a different identification code than K1 and a different        */ 
/* Checksum in the .PRM file (Checksum has to be modified in prm file        */ 
/* manually).                                                                */
/* ***************************************************************************/
#if defined( FBL_ENABLE_KIT_K4)

# pragma section const {APPLSWHDR}

V_MEMROM0 V_MEMROM1 tApplicationSwHeader V_MEMROM2  applSoftwareHeader = 
{
   /* F192 - F193: Hardware Versions */   
   "HwNumb     ",
   0xA6,
   
   /* F194 - F195: Software Versions */         
   "12345678901",
   {0x5A, 0xA5},
#if defined( FBL_ENABLE_CHRYSLER_ADDON )

   /* F122: Software part number */
   { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
   
   /* F132: Ecu part number */
   { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
#endif

   /* F180: bootSoftware identification */
   {0x01, 0x00, 0x0E, 0x00, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
   
   /* F181: applicationSoftware identification */
   {0x01, 0x00, 0x10, 0x00, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},

   /* F182: applicationData identification */
   {0x01, 0x00, 0x10, 0x00, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},

   /* F187: SparePartNumber / drawingNumber */
   "10987654321",
   
   /* F196: exhaustRegulationOrTypeApprovalNumber */
   "654321",
   
   /* F1A5: identificationOption (ISO Code) */
   {0xA3, 0x52, 0xA2, 0x51, 0xA4}
};
# pragma section const {}
# pragma section const {APPLDATAHDR}
V_MEMROM0 V_MEMROM1 tApplicationDataHeader V_MEMROM2  applDataHeader =
{
   /* F192 - F193: Hardware Versions */   
   "HwNumb     ",
   0xA6,
   
   /* F194 - F195: Software Versions */         
   "12345678901",
   {0x5A, 0xA5},
#if defined( FBL_ENABLE_CHRYSLER_ADDON )

   /* F122: Software part number */
   { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
   
   /* F132: Ecu part number */
   { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}
#endif
};
# pragma section const {}

#endif   /* FBL_ENABLE_KIT_K4 */

/* Kit K5 ********************************************************************/
/* Kit K5 is compatible with the hardware but software and data are not      */ 
/* compatible               .                                                */ 
/* ***************************************************************************/
#if defined( FBL_ENABLE_KIT_K5 )

# pragma section const {APPLSWHDR}

V_MEMROM0 V_MEMROM1 tApplicationSwHeader V_MEMROM2  applSoftwareHeader = 
{
   /* F192 - F193: Hardware Versions */   
   "HwNumb     ",
   0xA6,
   
   /* F194 - F195: Software Versions */         
   "NewSwNumb  ",
   {0xA6, 0xA7},
#if defined( FBL_ENABLE_CHRYSLER_ADDON )

   /* F122: Software part number */
   { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
   
   /* F132: Ecu part number */
   { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
#endif

   /* F180: bootSoftware identification */
   {0x01, 0x00, 0x0E, 0x00, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
   
   /* F181: applicationSoftware identification */
   {0x01, 0x00, 0x10, 0x00, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},

   /* F182: applicationData identification */
   {0x01, 0x00, 0x10, 0x00, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},

   /* F187: SparePartNumber / drawingNumber */
   "10987654321",
   
   /* F196: exhaustRegulationOrTypeApprovalNumber */
   "654321",
   
   /* F1A5: identificationOption (ISO Code) */
   {0xA3, 0x52, 0xA2, 0x51, 0xA5}
};
# pragma section const {} 
# pragma section const {APPLDATAHDR}
V_MEMROM0 V_MEMROM1 tApplicationDataHeader V_MEMROM2  applDataHeader =
{
   /* F192 - F193: Hardware Versions */   
   "HwNumb     ",
   0xA6,
   
   /* F194 - F195: Software Versions */         
   "10987654321",
   {0x5A, 0xA5},
#if defined( FBL_ENABLE_CHRYSLER_ADDON )

   /* F122: Software part number */
   { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
   
   /* F132: Ecu part number */
   { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}
#endif
};
# pragma section const {} 

#endif   /* FBL_ENABLE_KIT_K5 */






/******************************************************************************
******************************************************************************/
