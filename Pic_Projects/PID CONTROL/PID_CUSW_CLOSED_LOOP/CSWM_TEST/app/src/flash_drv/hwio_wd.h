#ifndef HWIO_WD
#define HWIO_WD
/******************************************************************************
*   (c) Copyright Continental AG 2008
*   All Rights Reserved.
*   Continental AG Confidential Proprietary
*   The Copyright symbol does not also indicate public availability
*   or publication.
*
*   Filename:   hwio_wd.h 
*
*   Created_by: A. Nikitin
*       
*   Date_created: 04/10/2008
*
*   Description:  Definitions for Watchdog timer driver
*
*
*******************************************************************************
*   Revision History:
*
*   Date          CQ#           Author    
*   MM-DD-YY      oem00000000   Initials   Description of change.
*   -----------   -----------   --------   ------------------------------------
*   04-10-08      oem00023760   AN         Initial version
******************************************************************************/

/******************************************************************************
*   Include Files
******************************************************************************/ 
#include    "itbm_basapi.h" 

/******************************************************************************
*   Macro Definitions  
******************************************************************************/

/*****************************************************************************
*  Local Macro Definitions
*****************************************************************************/

/******************************************************************************
*   Type Definitions
******************************************************************************/

/******************************************************************************
*   External Variables
******************************************************************************/

/******************************************************************************
*   Global Function Prototypes  
******************************************************************************/
/* Watchdog timer initalization */
void MEM_FAR ITBM_wdInit(void);

/* Watchdog counter reset */
void MEM_FAR ITBM_wdKick(void);

/* Hardware reset */
void MEM_FAR ITBM_wdForceReset(void);


#endif /* HWIO_WD */
