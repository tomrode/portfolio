#ifndef MW_CFGM_HWI_CFG_H
#define MW_CFGM_HWI_CFG_H
/******************************************************************************
*   (c) Copyright Continental AG 2007-2008
*   All Rights Reserved.
*   Continental AG Confidential Proprietary
*   The Copyright symbol does not also indicate public availability
*   or publication.
*
*   Filename:   mw_cfgm_hwi_cfg.h 
*
*   Created_by: M. Gorshkova
*       
*   Date_created: 10/18/2007
*
*   Description: Header file fro HWI configuration 
*
*
*******************************************************************************
*   Revision History:
*
*   Date          CQ#           Author    
*   MM-DD-YY      oem00000000   Initials   Description of change.
*   -----------   -----------   --------   ------------------------------------
*   10-18-07      oem00023080   MG         Initial version
*   11-07-07      oem00023080   MG         correction after code review
*   11-23-07      oem00023080   MG         Data type for PWM signals corrected
*   11-30-07      oem00023225   MG         parameter names (port, pin) changed
*   12-11-07      oem00023254   AN         Header files usage is optimized
*   02-12-08      oem00023522   DT         Better accuracy of PWM input duty cycle measurement
*   03-04-08      oem00023608   DT         PWM input driver update
*   03-04-08      oem00023607   AN         Hardware brake signal is added
*   04-15-08      oem00023537   DT         HW diagnostic-related updates
*   04-29-08      oem00023876   AN         External variables are removed
*   05-14-08      oem00023917   VS         Update for S0 hardware
*   05-23-08      oem00023971   VS         Driver issue corrected
*   06-16-08      oem00024125   DT         HW diagnostic update
*   06-23-08      oem00023723   DT         Define unused pins as outputs low
******************************************************************************/

/******************************************************************************
*   Include Files
******************************************************************************/ 
#include    "itbm_basapi.h"
#include    "mw_cfgm_defs.h"               /* common definition for signals */

/*****************************************************************************
 *  Local Macro Definitions
 *****************************************************************************/

/******************************************************************************
*   Type Definitions
******************************************************************************/
typedef enum {
    acc_x_pwm = HWI_FIRST_ID,
    acc_y_pwm,
    gain_minus,
    gain_plus,
    man_app_re,
    led_ps,
    hsd_csns,
    man_app,
    ignition,
    vbatt,
    hsd_out_vovt,
    lever_ref,
    hsd_fs,
    manual_in,
    led_fs,
    manual_in_freq,
#if ITBM_PJ6_DIRECTION != ITBM_PJ6_OUTPUT 
    brake_pedal_in, /* HW brake pedal input */
#endif
    relay_st,

    HWI_MAX_INPUTS
} eApp_HWI_ID;

typedef struct {
    /* target to data receive\transmit */
    DEVICE_TYPE    Device;
    /* Port definition  */
    UINT8          param1;
    /* Channel/bit number of port  */
    UINT8          param2;
    /* Debounce strategy (analog/gigital/none)*/
    UINT8          Dbnc;
    /* Input Application Data total size in bytes */
    UINT8          Data_Size;
    /* Initial value of input Application Data */
    AccessRef      pInitValue;
    /* Current value of input Application Data */
    AccessRef      pDATA;
    /* Active input value */
    ITBM_BOOLEAN   Active_Low;
    /* Current error code of input Application Data */
    UINT8          Error_Code;
} ITBM_HWI_TableType;

/******************************************************************************
*   Macro Definitions  
******************************************************************************/
/* Constants and Macros for HW Input Application Data*/
#define ITBM_HWI_ARRAY_ELEMENTS (HWI_MAX_INPUTS - HWI_FIRST_ID)
#define IS_INPUT(j)  ( ( ( (j) >= HWI_FIRST_ID) && ( (j) < HWI_MAX_INPUTS) ) ? ITBM_TRUE : ITBM_FALSE)
/* for accessing data in the mapping tables by the real index */
#define TRUE_HWI_IDX(id)  (id - HWI_FIRST_ID)

/******************************************************************************
*   External Variables
******************************************************************************/
extern ITBM_HWI_TableType ITBM_HWI_Table[ITBM_HWI_ARRAY_ELEMENTS];

/******************************************************************************
*   Global Function Prototypes  
******************************************************************************/

#endif /* MW_CFGM_HWI_CFG_H */
