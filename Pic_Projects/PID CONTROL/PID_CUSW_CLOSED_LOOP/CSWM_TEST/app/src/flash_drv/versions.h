#ifndef ITBM_VERSIONS_H
#define ITBM_VERSIONS_H
/******************************************************************************
*   (c) Copyright Continental AG 2008
*   All Rights Reserved.
*   Continental AG Confidential Proprietary
*   The Copyright symbol does not also indicate public availability
*   or publication.
*
*   Filename:   versions.h 
*
*   Created_by: D. Tatarinov
*       
*   Date_created: 06/27/08
*
*   Description:  Header file services callbacks
*
*
*   Revision history:
*
*   Date          CQ#           Author     
*   MM-DD-YY      oem00000000   Initials   Description of change
*   -----------   -----------   --------   ------------------------------------
*   06-27-08      oem00024189   DT         Initial version
*   07-22-08      oem00024253   VS         Software Part Number is updated
*   02-03-09      --            CK         EEPROM version is updated
*   04-17-09      --            CK         EEPROM version is updated to 0x0911. Deleted all un-used #define(s)
*   06-15-2009    --            YH         EEP version changes to 0x0919 
*   08-24-2009    --            CC         EEP version changes to 0x0925 
*   04-22-2010    --            YH         EEP Version changes to 0x0A11 
*   09-07-2019    --            YH         EEP Version changes to 0x0A28 
******************************************************************************/

/******************************************************************************
*   Macro Definitions  
******************************************************************************/
#define CURRENT_EEPROM_VERSION 	  0x0B22 //1	/* EEPROM version */
//#define CHECKSUM_VALUE			  0x0000

/* $F10A - ECU Origin */
//#define ECU_ORIG_DCA                0x2 /* ECU Origin */

/* $F10D - Diagnostic Specification Information */
//#define DCX_PROTOCOL_VER            0x3 /* DCX Unified Diagnostic Services - Diagnostic Protocol Version DC-10747 */
//#define DCX_DEFINITION_VER          0x3 /* DCX ECU Flash Reprogramming Requirements Definition Version DC-10761 */ 
//#define DCX_REQUIREMENTS_STD        0x4 /* DCX Diagnostic Performance Requirements Standard DC-10746 */

/* $F112 - Chrysler Group Hardware Part Number */
//#define HW_PART_NUMBER              "0000000000" /* NOTE!!!!! TBD */

/* $F122 - Chrysler Group Software Part Number */
//#define SW_PART_NUMBER              "56029431AB"

/* $F132 - ECU Part Number */
//#define ECU_PART_NUMBER             "0000000000" /* NOTE!!!!! TBD */

/* $F150 - Hardware Version Information */
//#define HW_YEAR                     0x08 /* HW - year !!!TBD!!!!*/
//#define HW_WEEK                     0x16 /* HW - week !!!TBD!!!!*/
//#define HW_PATCH_LEVEL              0x03 /* HW - patch level !!!TBD !!!*/

/* $F151 - Software Version Information */
//#define SW_YEAR                     0x08 /* SW - year */
//#define SW_WEEK                     0x1e /* SW - week */
//#define SW_PATCH_LEVEL              0x04 /* SW - patch level */

/* $F154 - Hardware Supplier Identification */
//#define HW_SUPPLIER_ID              0x003c /* Hardware Supplier Identification */

/* $F155 - Software Supplier Identification */
//#define SW_SUPPLIER_ID              0x003c /* Software Supplier Identification */

/* $F158 - Vehicle Information */
//#define MODEL_YEAR                  10 /* Model Year */
//#define VEHICLE_LINE                9  /* Vehicle Line */
//#define BODY_STYLE                  7  /* Body Style - 4-Door Pickup */
//#define COUNTRY_CODE                2  /* Country Code - USA */

/* $F160 - Software Module Information */
//#define NUMBER_OF_CHANNELS                         kCanNumberOfChannels
//#define HIS_SUPPLIER_ID_LAST_CHANNEL               0x1e
//#define SW_INTEGR_PCKG_VER_LAST_CHANNEL_MSB        kSipMainVersion
//#define SW_INTEGR_PCKG_VER_LAST_CHANNEL_LSB        kSipSubVersion
//#define SW_INTEGR_PCKG_PATCH_VER_LAST_CHANNEL      kSipBugFixVersion
//#define COMM_MATRIX_CHANNEL_MSB                    kDBCMainVersion
//#define COMM_MATRIX_CHANNEL_LSB                    kDBCSubVersion
//#define COMM_MATRIX_PATCH_VER_LAST_CHANNEL         kDBCReleaseVersion
//#define PHY_LAYER_DRIVER_VER_LAST_CHANNEL_MSB      kCanMainVersion
//#define PHY_LAYER_DRIVER_VER_LAST_CHANNEL_LSB      kCanSubVersion
//#define PHY_LAYER_DRIVER_BUGFIX_VER_LAST_CHANNEL   kCanBugFixVersion
//#define NM_VER_LAST_CHANNEL_MSB                    0//kNmMainVersion
//#define NM_VER_LAST_CHANNEL_LSB                    0//kNmSubVersion
//#define NM_BUGFIX_VER_LAST_CHANNEL                 0//kNmBugfixVersion
//#define UDS_MODULE_VER_LAST_CHANNEL_MSB            g_descMainVersion
//#define UDS_MODULE_VER_LAST_CHANNEL_LSB            g_descSubVersion
//#define UDS_BUGFIX_VER_LAST_CHANNEL                g_descBugFixVersion
//#define TRANSPORT_LAYER_VER_LAST_CHANNEL_MSB       kTpMainVersion
//#define TRANSPORT_LAYER_VER_LAST_CHANNEL_LSB       kTpSubVersion
//#define TRANSPORT_LAYER_BUGFIX_VER_LAST_CHANNEL    kTpBugFixVersion

/* $F18C - ECU Serial Number */
//#define ECU_SERIAL_NUMBER            "Z01TR" /* NOTE!!!!! TBD */

/******************************************************************************
*   Type Definitions
******************************************************************************/

/******************************************************************************
*   External Variables
******************************************************************************/
 
/******************************************************************************
*   Global Function Prototypes  
******************************************************************************/

#endif /* ITBM_VERSIONS_H */
