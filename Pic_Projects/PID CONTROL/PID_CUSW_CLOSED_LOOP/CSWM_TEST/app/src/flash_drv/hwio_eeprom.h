/******************************************************************************
*   (c) Copyright Continental AG 2008
*   All Rights Reserved.
*   Continental AG Confidential Proprietary
*   The Copyright symbol does not also indicate public availability
*   or publication.
*
*   Filename:   hwio_eeprom.h 
*
*   Created_by: L. Kaplan
*       
*   Date_created: 03/25/2008
*
*   Description: EEPROM driver header
*******************************************************************************

*******************************************************************************
*               A U T H O R   I D E N T I T Y
*******************************************************************************
* Initials      Name                   Company
* --------      --------------------   ---------------------------------------
* HY            Harsha Yekollu         Continental Automotive Systems
* CK            Can Kulduk             Continental Automotive Systems
*
*
*******************************************************************************

*******************************************************************************
*   Revision History:
*
*   Date          CQ#           Author    
*   MM-DD-YY      oem00000000   Initials   Description of change.
*   -----------   -----------   --------   ------------------------------------
*   03-25-08      oem00023671   LK         EEPROM driver ported from TIPM
*   08-13-08      -             CK         Flash Driver is ported from ITBM project.
*   08-14-08      -             CK         Modified function prototype of 
*				           IsrFlashCmdCompleteInterrupt in order 
*                                          to get the file compiling with Cosmic 
*                                          Compiler version CX12.478 in our setup.
*  08-18-2008     -             CK         Modified the value of FLASH_CLOCK_DIV.
*                                          CSWM MY11 uses 8Mhz. cystal oscillator.
*
******************************************************************************/

#ifndef HWIO_EEPROM_H
#define HWIO_EEPROM_H

/*****************************************************************************************************
 *  Global Macro Definitions
 ****************************************************************************************************/
/* Make all calls to EE_Driver functions far */
#define EE_DRIVER_CALL MEM_FAR

/* Size of an EEPROM sector.  This should never change for the S12X 
   The constant does not refer to DFlash sector size  */
#define EE_SECTOR_SIZE                          (4)

/* Flash Clock Divider Register FCLKDIV 
   1MHz is configured for fclk           */
#define FDIVLD             0x80
/* MODIFIED for CSWM MY11: 8 MHz  Crystal Oscillator */
//#define FLASH_CLOCK_DIV       0x08
#define FLASH_CLOCK_DIV       0x07

/* Flash Configuration Resgister FCNFG 
   enable command complete interrupt, disable fault detection */
#define CCIE               0x80
#define IGNSF              0x10
#define FCNFG_INIT         (CCIE + IGNSF)


/* Flash Staus Register FSTAT */
#define CCIF                0x80
#define ACCERR              0x20
#define FPVIOL              0x10



/* Given an address, this macro returns the sector address (last 256-byte boundary) */
#define EE_SectorAddress(addr)                  ( (unsigned int *)(((UInt16)(*(&addr))) & 0xFF00) )

/* Returns true if the address is on a sector boundary (it's 256-byte aligned) */
#define EE_AddressIsSectorAligned(addr)         ( (((UInt16)(*(&addr))) & 0x00FF) == 0 )

/* Given an address, returns an offset 0-3 relative to the EEPROM sector 
 * For example:
 *
 *  EE_AddressSectorOffset(0x800) = 0
 *  EE_AddressSectorOffset(0x801) = 1
 *  EE_AddressSectorOffset(0x802) = 2
 *  EE_AddressSectorOffset(0x803) = 3
 *  EE_AddressSectorOffset(0x804) = 0
 */
#define EE_AddressSectorOffset(addr)            ( ((UInt16)(*(&addr))) & 0x00FF )

/*****************************************************************************************************
 *  Global Variable Definitions
 ****************************************************************************************************/

/*****************************************************************************************************
 *  Static Variable Definitions
 ****************************************************************************************************/

/*****************************************************************************************************
 *  Global and Static Function Definitions
 ****************************************************************************************************/
/* Modified Function Prototypes */
void EE_DRIVER_CALL             EE_Handler       (void);
unsigned char EE_DRIVER_CALL    EE_IsBusy           (void);
void EE_DRIVER_CALL             EE_DriverPowerupInit(void);
unsigned char EE_DRIVER_CALL    EE_ForcedWrite      (unsigned long ee_dest, unsigned char *pbysrc, 
                                                    unsigned char bysize);
extern void  EE_DRIVER_CALL     EE_Burst_Write       (void);
extern void  EE_DRIVER_CALL     EE_Driver_Static_Init (void);

/* MODIFIED this interrupt prototype */
//extern @interrupt @near void IsrFlashCmdCompleteInterrupt(void);
//extern interrupt near void IsrFlashCmdCompleteInterrupt(void);


/*****************************************************************************************************
 *  Local Type Definitions
 ****************************************************************************************************/

/*****************************************************************************************************
 *  Local Function Declarations
 ****************************************************************************************************/

#endif /* HWIO_EEPROM_H */
