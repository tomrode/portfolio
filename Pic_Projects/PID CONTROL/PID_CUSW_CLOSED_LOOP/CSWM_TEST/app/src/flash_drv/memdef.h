/******************************************************************************
*   (c) Copyright Continental AG 2008
*   All Rights Reserved.
*   Continental AG Confidential Proprietary
*   The Copyright symbol does not also indicate public availability
*   or publication.
*
*   Filename:   memdef.h 
*
*   Created_by: L. Kaplan
*       
*   Date_created: 03/25/2008
*
*   Description: S12X memory locations
*
*
    *******************************************************************************
*   Revision History:
*
*   Date          CQ#           Author    
*   MM-DD-YY      oem00000000   Initials   Description of change.
*   -----------   -----------   --------   ------------------------------------
*   03-25-08      oem00023671   LK         EEPROM driver ported from TIPM
******************************************************************************/
#ifndef _MEMDEF_H_
#define _MEMDEF_H_

/*****************************************************************************************************
*   Include Files
*****************************************************************************************************/

/*****************************************************************************************************
*   Macro Definitions
*****************************************************************************************************/
#define RAM_LOGICAL_START       0x1000
#define RAM_LOGICAL_END         0x3FFF
#define EEPROM_LOGICAL_START    0x0800
#define EEPROM_LOGICAL_END      0x0FFF
#define FLASH_LOGICAL_START     0x4000
#define FLASH_LOGICAL_END       0xFFFF
#define LOGICAL_MEMORY_END      0xFFFF

#define RAM_GLOBAL_START            0x0F8000
#define RAM_GLOBAL_END              0x0FFFFF
#define RAM_GLOBAL_PAGED_BASE       0x1000
#define EEPROM_GLOBAL_START         0x13F000
#define EEPROM_GLOBAL_END           0x13FFFF
#define EEPROM_GLOBAL_PAGED_BASE    0x800
#define FLASH_GLOBAL_START          0x780000
#define FLASH_GLOBAL_END            0x7FFFFF
#define FLASH_GLOBAL_PAGED_BASE     0x8000

/*****************************************************************************************************
*   Type Definitions
*****************************************************************************************************/

/*****************************************************************************************************
*   Function Declarations
*****************************************************************************************************/

/*****************************************************************************************************
*   Variable Definitions
*****************************************************************************************************/

/* _MEMTYPE_H_ */
#endif /* _MEMDEF_H_ */
