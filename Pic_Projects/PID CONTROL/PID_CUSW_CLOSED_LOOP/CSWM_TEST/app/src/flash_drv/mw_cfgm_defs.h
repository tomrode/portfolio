#ifndef MW_CFGM_DEFS_H
#define MW_CFGM_DEFS_H
/******************************************************************************
*   (c) Copyright Continental AG 2007-2008
*   All Rights Reserved.
*   Continental AG Confidential Proprietary
*   The Copyright symbol does not also indicate public availability
*   or publication.
*
*   Filename:   mw_cfgm_defs.h 
*
*   Created_by: M. Gorshkova
*       
*   Date_created: 10/23/2007
*
*   Description: common definitions of signals
*
*
*******************************************************************************
*   Revision History:
*
*   Date          CQ#           Author    
*   MM-DD-YY      oem00000000   Initials   Description of change.
*   -----------   -----------   --------   ------------------------------------
*   10-23-07      oem00023080   MG         Initial version
*   11-03-07      oem00023085   DT         definition for DIO
*   11-07-07      oem00023080   MG         correction after code review
*   11-08-07      oem00023006   DT         definition for ADC and PWM added
*   11-13-07      oem00023084   DT         definition for SPI added
*   11-15-07      oem00023001   DT         PWM Driver definitions
*   12-11-07      oem00023254   AN         Header files usage is optimized
*   12-24-07      oem00023236   DT         Changes made as per CODE review for changes related to protoboard
*   12-28-07      oem00023225   MG         The number of ATD devices changed to 1
*   01-08-08      oem00023464   FS         change  ITBM_ADC_NUMBER_OF_ATD to 1
*   01-09-08      oem00023464   VS         change  ITBM_DIO_NUMBER_OF_ATDS to 2
*   01-21-08      oem00023401   DT         PWM driver updates 
*   02-19-08      oem00023521   DT         The changes were made based on g17487_MC33982_devl branch
*   02-21-08      oem00023542   DT         Overflow-related issue resolved
*   02-27-08      oem00023543   AN         Param and DTC ranges are added
*   03-04-08      oem00023608   DT         PWM input driver update
*   05-17-08      oem00023971   VS         Fixed issues during debugging on S0
*   06-18-08      oem00024039   LP         Add defines for occurrence and faults
*   06-24-08      oem00023723   DT         PJ6 dirrection paremeter added 
******************************************************************************/

/*****************************************************************************
 *  Local Macro Definitions
 *****************************************************************************/

/******************************************************************************
*   Type Definitions
******************************************************************************/

/******************************************************************************
*   Macro Definitions  
******************************************************************************/
/* Absolute value macro */
#define ABS(var)  ((var)<0 ? -(var):(var))

/* Debounce criteria for DI debounce strategy  */
#define D_DBNC_NUMBER_OF_SAMPLES         5
/* Debounce criterias for AI debounce strategy  */
#define A_DBNC_DISPERSION         5
#define A_DBNC_NUMBER_OF_SAMPLES  3

/* Debounce criterias for PWM debounce strategy  */
#define P_DBNC_DISPERSION         1000
#define P_DBNC_NUMBER_OF_SAMPLES  3

#define HWI_FIRST_ID         0x0000
#define HWO_FIRST_ID         0x1000
#define LOCAL_AD_FIRST_ID    0x2000
#define PARAM_FIRST_ID       0x3000
#define DTC_FIRST_ID         0x4000
#define PAL_NULL_ID          0xFFFF

typedef enum {
    DIO,
    PWM,
    AMUX,
    ADC,
    SPI,
    DISP,
    PAL_NUMBER_OF_IOD
}IODriverIDType;

/* ADC related defenition */
#define ADC_PRECISION   5   /* ADC precision */

#define ITBM_ADC_NUMBER_OF_ATD 1 /* Number of ATD devices */

#define ATD_Port0           0 /* ATD0 device definition  */
#define ATD_Port1           1 /* ATD1 device definition  */

#define ITBM_ADC_MAX_CHANNEL_NUM 16

#define ATD_Channel0   0 /* 0 channel definition    */
#define ATD_Channel1   1 /* 1 channel definition    */
#define ATD_Channel2   2 /* 2 channel definition    */
#define ATD_Channel3   3 /* 3 channel definition    */
#define ATD_Channel4   4 /* 4 channel definition    */
#define ATD_Channel5   5 /* 5 channel definition    */
#define ATD_Channel6   6 /* 6 channel definition    */
#define ATD_Channel7   7 /* 7 channel definition    */
#define ATD_Channel8   8 /* 8 channel definition    */
#define ATD_Channel9   9 /* 9 channel definition    */
#define ATD_Channel10 10 /* 10 channel definition   */
#define ATD_Channel11 11 /* 11 channel definition   */
#define ATD_Channel12 12 /* 12 channel definition   */
#define ATD_Channel13 13 /* 13 channel definition   */
#define ATD_Channel14 14 /* 14 channel definition   */
#define ATD_Channel15 15 /* 15 channel definition   */


#define A_DBNC 0      /* analog signal should be debounced */
#define D_DBNC 1      /* digital signal should be debounced */
#define P_DBNC 2      /* PWM signal should be debounced */
#define N_DBNC 0xFF   /* no debounce */

#define ATD_Channe0 0
#define ATD_Channe1 1
#define ATD_Channe2 2
#define ATD_Channe3 3
#define ATD_Channe4 4
#define ATD_Channe5 5
#define ATD_Channe6 6
#define ATD_Channe7 7

/* DIO related definition */

#define ITBM_DIO_NUMBER_OF_PORTS   14 /* Number of ports for current platform */

/* Definitions of ports */
#define PortA              0  /* PortA definition   */
#define PortB              1  /* PortB definition   */
#define PortC              2  /* PortC definition   */
#define PortD              3  /* PortD definition   */
#define PortE              4  /* PortE definition   */
#define PortK              5  /* PortK definition   */
#define PortT              6  /* PortT definition   */
#define PortS              7  /* PortS definition   */
#define PortM              8  /* PortM definition   */
#define PortP              9  /* PortP definition   */
#define PortH             10  /* PortH definition   */
#define PortJ             11  /* PortJ definition   */
#define PortAD0           12  /* PortAD0 definition */
#define PortAD1           13  /* PortAD1 definition */

#define DISP_Port0         0  /* Display data */
#define DISP_Port1         1  /* Bar-graph data */
#define DISP_Port2         2  /* Display intensity data */
#define DISP_Port3         3  /* Bar intensity data */
#define DISP_Port4         4  /* Bicolor leds intensity data */


#define ITBM_DIO_NUMBER_OF_ATDS  2 /* Number of ADC group of registers       */
#define ITBM_DIO_START_ATD      12 /* Port ID from which ADC registers begin */

/* Definitions of pins */
#define Pin0               0  /* Pin0 of the port  */ 
#define Pin1               1  /* Pin1 of the port  */ 
#define Pin2               2  /* Pin2 of the port  */ 
#define Pin3               3  /* Pin3 of the port  */ 
#define Pin4               4  /* Pin4 of the port  */ 
#define Pin5               5  /* Pin5 of the port  */ 
#define Pin6               6  /* Pin6 of the port  */ 
#define Pin7               7  /* Pin7 of the port  */ 
#define Pin8               8  /* Pin8 of the port  */ 
#define Pin9               9  /* Pin9 of the port  */ 
#define Pin10             10  /* Pin10 of the port */ 
#define Pin11             11  /* Pin11 of the port */
#define Pin12             12  /* Pin12 of the port */ 
#define Pin13             13  /* Pin13 of the port */ 
#define Pin14             14  /* Pin14 of the port */ 
#define Pin15             15  /* Pin15 of the port */

/* PWM related definition */
#define ITBM_PWM_NUMBER_OF_CHANNELS 8  /* Number of PWM channels       */

#define PWM_Channel0      0 /* PWM channel 0 definition */
#define PWM_Channel1      1 /* PWM channel 1 definition */
#define PWM_Channel2      2 /* PWM channel 2 definition */
#define PWM_Channel3      3 /* PWM channel 3 definition */
#define PWM_Channel4      4 /* PWM channel 4 definition */
#define PWM_Channel5      5 /* PWM channel 5 definition */
#define PWM_Channel6      6 /* PWM channel 6 definition */
#define PWM_Channel7      7 /* PWM channel 7 definition */

/* Set of output frequency, the clock sources are ClockSA and ClockSB */
#define PWM_FRQ_200    0x86  /* Actualy the value is PWM_period => Fout = clock source / (2 * PWM_period) = 200 Hz */
#define PWM_FRQ_250    0x6b  /* Actualy the value is PWM_period => Fout = clock source / (2 * PWM_period) = 250 Hz */
#define PWM_FRQ_1K     0x1A  /* Actualy the value is PWM_period => Fout = clock source / (2 * PWM_period) = 1000 Hz */

#define PWM_POL_P         0    /* positive polarity */
#define PWM_POL_N         0x80 /* negative polarity */

/* These input capture channels are used for pulse width measure */
#define PWM_inChannel7     7
#define PWM_inChannel6     6
#define PWM_inChannel5     5
#define PWM_inChannel4     4
#define PWM_inChannel3     3
#define PWM_inChannel2     2
#define PWM_inChannel1     1
#define PWM_inChannel0     0

/* type of returned PWM attribute */
#define PWM_IN_DUTY        0  /* duty cycle will be returned */
#define PWM_IN_FREQ        1  /* frequency will be returned */

#define PWM_INP_PULSE_LENGTH  1

/* SPI related definition */
#define SPI_Port0         0
#define SPI_Port1         1
#define SPI_Port2         2

#define ITBM_SPI_NUMBER     1 /* Number of SPI devices      */
#define ITBM_SPI_BIT_NUMBER 8 /* Number of bits in SPI data */

#define BRAKE_TYPE        1
#define EB                1
#define EOH               0


/* accelerometer types */
#define ITBM_ACCL_ANALOG    0
#define ITBM_ACCL_DIGITAL   1

/* PJ6 dirrection */
#define ITBM_PJ6_OUTPUT   0
#define ITBM_PJ6_INPUT    1

/*DTC types - fault and occurrence*/
#define DTC_TYPE_OCCURRENCE 1
#define DTC_TYPE_FAULT      0

/* check the accelerometer type defined by the compile-time parameter */
#if (ITBM_ACCL_TYPE != ITBM_ACCL_ANALOG) && (ITBM_ACCL_TYPE != ITBM_ACCL_DIGITAL)
#error Wrong accelerometer type
#endif

/******************************************************************************
*   External Variables
******************************************************************************/

/******************************************************************************
*   Global Function Prototypes  
******************************************************************************/

#endif /* MW_CFGM_DEFS_H */
