/******************************************************************************
*   (c) Copyright Continental AG 2008
*   All Rights Reserved.
*   Continental AG Confidential Proprietary
*   The Copyright symbol does not also indicate public availability
*   or publication.
*
*   Filename:   memefunc.h 
*
*   Created_by: L. Kaplan
*       
*   Date_created: 03/25/2008
*
*   Description: Header for memory functions
*
*
    *******************************************************************************
*   Revision History:
*
*   Date          CQ#           Author    
*   MM-DD-YY      oem00000000   Initials   Description of change.
*   -----------   -----------   --------   ------------------------------------
*   03-25-08      oem00023671   LK         EEPROM driver ported from TIPM
*   07-21-08      oem00024262   AN         Function calls are changed to far
******************************************************************************/
#ifndef _MEMFUNC_H_
#define _MEMFUNC_H_


/*****************************************************************************************************
*   Include Files
*****************************************************************************************************/

/*****************************************************************************************************
*   Macro Definitions
*****************************************************************************************************/
#define MEMFUNC_CALL    @far

#define FIRSTRAMPAGE                        ((UInt8)0xFB)
#define LASTRAMPAGE                         ((UInt8)0xFF)

/* FBL EEPROM Starting address */
#define kEepFblBaseAddressLocal                 ((UInt32)0x13FF00)
/* Size of FBL Information saved in EEPROM */
#define kEepSizeFblInformation              ((UInt8)0x2E)
/* Size of Segment numbers */
#define kEepSizeSegmentNumber               ((UInt8)0x02)                   
/* Size of segment Starting Address */
#define kEepSizeSegmentStartAddr            ((UInt8)0x04)
/* Size of Segment Length */
#define kEepSizeSegmentLength               ((UInt8)0x04)
/* Size of segmentInfo for each segment */
#define kEepSizeSegmentInfo                 (kEepSizeSegmentStartAddr+kEepSizeSegmentLength)
/* Size of application code CRC */
#define kEepSizeCRCLocal                         ((UInt8)0x04)

/* Address offsets for application data */
#define kEepAddressSegmentNumber            (kEepFblBaseAddressLocal+kEepSizeFblInformation)
#define kEepAddressSegmentStartAddr         (kEepAddressSegmentNumber+kEepSizeSegmentNumber)              
#define kEepAddressSegmentLength            (kEepAddressSegmentStartAddr+kEepSizeSegmentStartAddr)              

/* Address of flash memory CRC */
#define kEepAddressCRCLocal                      (kEepAddressSegmentNumber-kEepSizeCRCLocal)

/*****************************************************************************************************
*   Type Definitions
*****************************************************************************************************/

/*****************************************************************************************************
*   Function Prototypes
*****************************************************************************************************/
void MEMFUNC_CALL MemSet( unsigned int destination, unsigned int size, unsigned char value );
unsigned char MEMFUNC_CALL GetGPage( unsigned long GlobalAddress );
unsigned char MEMFUNC_CALL GetPPage( unsigned long GlobalAddress );
unsigned char MEMFUNC_CALL GetRPage( unsigned long GlobalAddress );
unsigned char MEMFUNC_CALL GetEPage( unsigned long GlobalAddress );
unsigned int MEMFUNC_CALL GetGOffset( unsigned long GlobalAddress );
unsigned int MEMFUNC_CALL GetPOffset( unsigned long GlobalAddress );
unsigned int MEMFUNC_CALL GetROffset( unsigned long GlobalAddress );
unsigned int MEMFUNC_CALL GetEOffset( unsigned long GlobalAddress );
void MEMFUNC_CALL UniversalMemCopy(unsigned long dest, unsigned long src, unsigned int size );
void MEMFUNC_CALL LogicalMemCopy( unsigned int dest, unsigned int src, unsigned int size );
void MEMFUNC_CALL Global2LogicalMemCopy( unsigned int destination, unsigned long source, unsigned int size );
void MEMFUNC_CALL Logical2GlobalMemCopy( unsigned long destination, unsigned int source, unsigned int size );
void MEMFUNC_CALL GlobalMemCopy (unsigned long destination, unsigned long source, unsigned int size);
unsigned int MEMFUNC_CALL GlobalRead16( unsigned long longaddr );
unsigned char MEMFUNC_CALL GlobalRead8( unsigned long longaddr );

ITBM_BOOLEAN MEMFUNC_CALL FlashMemCheck(void);
ITBM_BOOLEAN MEMFUNC_CALL RamMemCheck(void);
unsigned long MEMFUNC_CALL MMRTFlashMemCheck(void);

/*****************************************************************************************************
*   Variable Definitions
*****************************************************************************************************/
/* temporary saved data for ram test */
extern volatile UInt16 save_data;


/* _MEMFUNC_H_ */
#endif