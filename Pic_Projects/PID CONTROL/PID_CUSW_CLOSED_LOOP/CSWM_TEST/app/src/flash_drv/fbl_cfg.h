/* -----------------------------------------------------------------------------
  Filename:    fbl_cfg.h
  Description: Toolversion: 02.02.00.01.01.01.53.01.00.00
               
               Serial Number: CBD1010153
               Customer Info: Continental_Temic
                              Chrysler Fiat_07209_UDS_FBL_wFlash 
                              Freescale MCS12X MC9S12XS128
                              Cosmic 4.7.8
               
               
               Generator Fwk   : GENy 
               Generator Module: GenTool_GenyFblCanBase
               
               Configuration   : N:\Share\YekolluH\FIAT CUSW\BootGENy\CUSW_Boot_041111.gny
               
               ECU: 
                       TargetSystem: Hw_Hc12Cpu
                       Compiler:     Cosmic
                       Derivates:    S12XX
               
               Channel "Channel0":
                       Databasefile: H:\SCM_DCX_CSWM_MY_12\Documents\DBC\PPF_E2B_1110_BCAN.dbc
                       Bussystem:    CAN
                       Manufacturer: Fiat
                       Node:         CSWM

  Generated by , 2011-05-16  09:26:07
 ----------------------------------------------------------------------------- */
/* -----------------------------------------------------------------------------
  C O P Y R I G H T
 -------------------------------------------------------------------------------
  Copyright (c) 2001-2010 by Vector Informatik GmbH. All rights reserved.
 
  This software is copyright protected and proprietary to Vector Informatik 
  GmbH.
  
  Vector Informatik GmbH grants to you only those rights as set out in the 
  license conditions.
  
  All other rights remain with Vector Informatik GmbH.
 -------------------------------------------------------------------------------
 ----------------------------------------------------------------------------- */

#if !defined(__FBL_CFG_H__)
#define __FBL_CFG_H__

/* -----------------------------------------------------------------------------
    &&&~ 
 ----------------------------------------------------------------------------- */

#define FBL_ENABLE_SYSTEM_CHECK
#define FBL_DISABLE_FLASHBLOCK_CHECK
#define FBL_ENABLE_DEBUG_STATUS
#define FBL_WATCHDOG_ON
#define FBL_WATCHDOG_TIME                    (10 / FBL_REPEAT_CALL_CYCLE)
#define FBL_HEADER_ADDRESS                   0xC000
#define FBL_ENABLE_APPL_TASK
#define FBL_ENABLE_MULTIPLE_MODULES
#define FBL_DIAG_BUFFER_LENGTH               4095
#define FBL_DIAG_TIME_P2MAX                  (50 / FBL_REPEAT_CALL_CYCLE)
#define FBL_DIAG_TIME_P3MAX                  (5000 / FBL_REPEAT_CALL_CYCLE)
#define FBL_DISABLE_MULTIPLE_MEM_DEVICES
/*  */
/* Hc12 specific ******************************************************************************** */
#define FBL_ENABLE_BANKING
#define FBL_ECU_FLASHSIZE                    128
#define FBL_OSCCLK                           8000UL
#define FBL_BUSCLK                           4000UL
#define FLASH_SIZE                           1105
#define CAN_BTR01                            0x832B
#ifdef VGEN_ENABLE_CAN_DRV
#else
#define C_REG_BLOCK_OFFSET                   0x0140
#define C_REG_BLOCK_ADR                      0x000
#endif

/*  */
#define FBL_TIMER_PRESCALER_VALUE            0x00
#define FBL_TIMER_RELOAD_VALUE               0x0FA0
/*  */
/*  */
#define FBL_REPEAT_CALL_CYCLE                1
#define FBL_WD_TRIGGER_SIZE                  200
/* FBL Fiat specific */
#define FBL_ENABLE_USERSUBFUNCTION
#define FBL_DISABLE_ENCRYPTION_MODE
#define FBL_DISABLE_FBL_START
#define APPL_FBL_RESET()                     ApplFblReset()
#define FBL_DISABLE_MULTIPLE_NODES
#define DIAG_NODE_ADDRESS                    0xC2
#define SWM_DATA_MAX_NOAR                    8
#define FBL_DISABLE_APPL_DATA
#define FBL_DISABLE_SEGMENT_CHECKSUM
#define FBL_ADDRESSLEN                       0x03
#define APPL_SOFTWARE_HEADER                 0x7E0000
#define APPL_DATA_HEADER                     0x7E0000
#define kFblMaxProgAttempts                  254u
#define FBL_ENABLE_CHRYSLER_ADDON
#define SEC_ECU_KEY                          0x74F66A92
#define SEC_ECU_KEY_2                        0x4DE3A3E3


/* -----------------------------------------------------------------------------
    &&&~ 
 ----------------------------------------------------------------------------- */

#define FBL_ENABLE_EXTENDED_ID
#define FBL_ENABLE_SLEEPMODE
#define FBL_ENABLE_SEC_ACCESS_DELAY


/* begin Fileversion check */
#ifndef SKIP_MAGIC_NUMBER
#ifdef MAGIC_NUMBER
  #if MAGIC_NUMBER != 65124078
      #error "The magic number of the generated file <N:\Share\YekolluH\FIAT CUSW\BootGENy\Generated Files\fbl_cfg.h> is different. Please check time and date of generated files!"
  #endif
#else
  #define MAGIC_NUMBER 65124078
#endif  /* MAGIC_NUMBER */
#endif  /* SKIP_MAGIC_NUMBER */

/* end Fileversion check */

#endif /* __FBL_CFG_H__ */
