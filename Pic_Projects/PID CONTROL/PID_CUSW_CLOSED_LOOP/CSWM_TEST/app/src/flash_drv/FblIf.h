/*****************************************************************************
| Project Name: CANfbl for Fiat
|    File Name: FblIf.H
|
|  Description: Interface routines for application for interaction with the FBL
|
|-----------------------------------------------------------------------------
|               C O P Y R I G H T
|-----------------------------------------------------------------------------
| Copyright (c) 2007-2011 by Vector Informatik GmbH, all rights reserved
|
| Please note, that this file contains a collection of callback 
| functions to be used with the Flash Bootloader. These functions may 
| influence the behaviour of the bootloader in principle. Therefore, 
| great care must be taken to verify the correctness of the 
| implementation.
|
| The contents of the originally delivered files are only examples resp. 
| implementation proposals. With regard to the fact that these functions 
| are meant for demonstration purposes only, Vector Informatik�s 
| liability shall be expressly excluded in cases of ordinary negligence, 
| to the extent admissible by law or statute.
|
|-----------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-----------------------------------------------------------------------------
| Initials     Name                      Company
| --------     ---------------------     -------------------------------------
| CB           Christian Baeuerle        Vector Informatik GmbH
|
|-----------------------------------------------------------------------------
|               R E V I S I O N   H I S T O R Y
|-----------------------------------------------------------------------------
| Date        Ver       Author  Description
| ---------   --------  ------  ----------------------------------------------
| 2007-07-18  01.00.00  CB      First implementation
| 2007-08-22  01.01.00  CB      Support for 07274
| 2007-09-11  01.02.00  CB      Minor changes
| 2007-10-12  01.03.00  CB      No changes  
| 2008-02-06  01.04.00  CB      No changes  
| 2008-02-28  01.05.00  CB      No changes  
| 2008-05-28  01.06.00  CB      No changes  
| 2008-09-04  01.07.00  CB      No changes
| 2010-01-18  01.08.00  CB      Support for Fj16Fx
| 2010-06-16  01.09.00  CB      No changes
| 2010-10-20  01.10.00  CB      Support for V850
| 2010-12-14  01.11.00  CB      Chrysler addon
| 2011-01-17  01.12.00  CB      No changes
| 2011-01-18  01.13.00  CB      No changes
|****************************************************************************/
#ifndef __FB_IF_h__
#define __FB_IF_h__

#define FBLIF_FIAT_VERSION           0x0113u
#define FBLIF_FIAT_RELEASE_VERSION   0x00u


/* Includes *******************************************************************/


/* Defines ********************************************************************/

/* Software identification */
#define kFblSizeIdentification            ((vuint8)0x0D) /* 13 bytes */
#define kFblSizeBootSwIdentification      kFblSizeIdentification
#define kFblSizeApplSwIdentification      kFblSizeIdentification
#define kFblSizeApplDataIdentification    kFblSizeIdentification


/* Tester fingerprint size */
#define kFblSizeModuleId                  0x01u
#define kFblSizeTesterSerialNo            0x09u
#define kFblSizeProgrammingDate           0x03u
#define kFblSizeFingerprint               (kFblSizeModuleId          \
                                          +kFblSizeTesterSerialNo    \
                                          +kFblSizeProgrammingDate)
#define kFblSizeBootSwFingerprint         kFblSizeFingerprint
#define kFblSizeApplSwFingerprint         kFblSizeFingerprint
#define kFblSizeApplDataFingerprint       kFblSizeFingerprint

#define kFblSizeSparePartNo               11u
#define kFblSizeApprovalNo                6u

# if defined( FBL_ENABLE_CHRYSLER_ADDON )
#define kFblSizeHardwarePartNo            10u
#define kFblSizeSoftwarePartNo            10u
#define kFblSizeEcuPartNo                 10u
# endif  /* FBL_ENABLE_CHRYSLER_ADDON */


#define kFblSizeEcuHwNo                   11u
#define kFblSizeEcuHwVersionNo            1u
#define kFblSizeEcuSwNo                   11u
#define kFblSizeEcuSwVersionNo            2u

#define kFblSizeIdentOption               5u 

#define kFblOffsetEcuHwNo                 0u
#define kFblOffsetEcuHwVersionNo          (kFblOffsetEcuHwNo+kFblSizeEcuHwNo)
#define kFblOffsetEcuSwNo                 (kFblOffsetEcuHwVersionNo+kFblSizeEcuHwVersionNo)
#define kFblOffsetEcuSwVersionNo          (kFblOffsetEcuSwNo+kFblSizeEcuSwNo)
# if defined( FBL_ENABLE_CHRYSLER_ADDON )
#define kFblOffsetSoftwarePartNo          (kFblOffsetEcuSwVersionNo+kFblSizeEcuSwVersionNo)
#define kFblOffsetEcuPartNo               (kFblOffsetSoftwarePartNo+kFblSizeSoftwarePartNo)
#define kFblOffsetBootSwIdentification    (kFblOffsetEcuPartNo+kFblSizeEcuPartNo)   
# else
#define kFblOffsetBootSwIdentification    (kFblOffsetEcuSwVersionNo+kFblSizeEcuSwVersionNo)
# endif
#define kFblOffsetApplSwIdentification    (kFblOffsetBootSwIdentification+kFblSizeBootSwIdentification)   
#define kFblOffsetApplDataIdentification  (kFblOffsetApplSwIdentification+kFblSizeApplSwIdentification)
#define kFblOffsetSparePartNo             (kFblOffsetApplDataIdentification+kFblSizeApplDataIdentification)
#define kFblOffsetApprovalNo              (kFblOffsetSparePartNo+kFblSizeSparePartNo)                   
#define kFblOffsetIdentOption             (kFblOffsetApprovalNo+kFblSizeApprovalNo)

                              
/* Typedefs *******************************************************************/

/* applicationSoftware header structure */
typedef struct tagApplicationSwHeader
{
   /* F192 - F195: Hardware- and software Versions */   
   vuint8   ecuHardwareNumber[kFblSizeEcuHwNo];
   vuint8   ecuHardwareVersionNumber;
   vuint8   ecuSoftwareNumber[kFblSizeEcuSwNo];
   vuint8   ecuSoftwareVersionNumber[kFblSizeEcuSwVersionNo];
#if defined( FBL_ENABLE_CHRYSLER_ADDON )

   /* F122: Software part number */
   vuint8   softwarePartNumber[kFblSizeSoftwarePartNo];
   
   /* F132: Ecu part number */
   vuint8   ecuPartNumber[kFblSizeEcuPartNo];
#endif

   /* F180: bootSoftware identification */
   vuint8   bootSoftwareIdentification[kFblSizeIdentification];

   /* F181: applicationSoftware identification */
   vuint8   applSoftwareIdentification[kFblSizeIdentification];

   /* F182: applicationData identification */
   vuint8   applDataIdentification[kFblSizeIdentification];

   /* F187: SparePartNumber / drawingNumber */
   vuint8   sparePartNumber[kFblSizeSparePartNo];


   /* F196: exhaustRegulationOrTypeApprovalNumber */
   vuint8   approvalNumber[kFblSizeApprovalNo];

   /* F1A5: identificationOption (ISO Code) */
   vuint8   identOption[kFblSizeIdentOption];
}tApplicationSwHeader;

/* applicationData header structure */
typedef struct tagApplicationDataHeader
{
   /* F192 - F195: Hardware- and software Versions */
   vuint8   ecuHardwareNumber[kFblSizeEcuHwNo];
   vuint8   ecuHardwareVersionNumber;
   vuint8   ecuSoftwareNumber[kFblSizeEcuSwNo];
   vuint8   ecuSoftwareVersionNumber[kFblSizeEcuSwVersionNo];
#if defined( FBL_ENABLE_CHRYSLER_ADDON )

   /* F122: Software part number */
   vuint8   softwarePartNumber[kFblSizeSoftwarePartNo];

   /* F132: Ecu part number */
   vuint8   ecuPartNumber[kFblSizeEcuPartNo];
#endif
}tApplicationDataHeader;

/******************************************************************************
******************************************************************************/
#endif 
