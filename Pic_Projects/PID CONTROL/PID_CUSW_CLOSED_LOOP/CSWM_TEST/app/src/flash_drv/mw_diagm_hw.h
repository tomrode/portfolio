#ifndef MW_DIAGM_HW_H
#define MW_DIAGM_HW_H
/******************************************************************************
*   (c) Copyright Continental AG 2008
*   All Rights Reserved.
*   Continental AG Confidential Proprietary
*   The Copyright symbol does not also indicate public availability
*   or publication.
*
*   Filename:   mw_diagm_hw.h 
*
*   Created_by: D. Tatarinov
*       
*   Date_created: 03/12/08
*
*   Description:  HW Diagnostic
*
*
*   Revision history:
*
*   Date          CQ#           Author     
*   MM-DD-YY      oem00000000   Initials   Description of change
*   -----------   -----------   --------   ------------------------------------
*   03-12-08      oem00023537   DT         Initial version
*   04-28-08      oem00023876   DT         DTC bitmasks-related functions added
******************************************************************************/
#include "itbm_basapi.h"
#include "mw_cfgm.h"
#include "mw_diagm_hw_isr.h"

/******************************************************************************
*   Macro Definitions  
******************************************************************************/

/******************************************************************************
*   Type Definitions
******************************************************************************/

/******************************************************************************
*   External Variables
******************************************************************************/
 
/******************************************************************************
*   Global Function Prototypes  
******************************************************************************/
Status_Type MEM_FAR ITBM_diagmHW_InitMode( void );
Status_Type MEM_FAR ITBM_diagmHW_RunMode( void );
Status_Type MEM_FAR ITBM_diagmHW_GetParameters( AppDataIDType, eITBM_Param_ID*, eITBM_Param_ID* );
//Status_Type MEM_FAR ITBM_diagmHW_BitmaskSetORClear( AppDataIDType, UINT16, ITBM_BOOLEAN );


#endif /* MW_DIAGM_HW_H */
