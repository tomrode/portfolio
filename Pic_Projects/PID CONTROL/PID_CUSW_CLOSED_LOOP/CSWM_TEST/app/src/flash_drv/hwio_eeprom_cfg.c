
/******************************************************************************
*   (c) Copyright Continental AG 2008
*   All Rights Reserved.
*   Continental AG Confidential Proprietary
*   The Copyright symbol does not also indicate public availability
*   or publication.
*
*   Filename:   hwio_eeprom_cfg.c 
*
*   Created_by: L. Kaplan
*       
*   Date_created: 03/25/2008
*
*   Description: EEPROM calibration tables, should be located on the same page as the EEPROM manager.
*
*
    *******************************************************************************
*   Revision History:
*
*   Date          CQ#           Author    
*   MM-DD-YY      oem00000000   Initials   Description of change.
*   -----------   -----------   --------   ------------------------------------
*   03-25-08      oem00023671   LK         EEPROM driver ported from TIPM
*   04-28-08      oem00023876   LK         DTC bitmasks storing to EEPROM added
*   05-23-08      oem00023971   LK         Short to batt address changed
*   06-05-08      oem00023678   AN         Flag of intended reset is added
*   06-07-08      oem00023975   DT         Flag of illegal opcode added
*   06-16-08      oem00024125   DT         Relay status flag added
*   06-26-08      oem00024187   LK         EEPROM changes
*   06-30-08      oem00024189   DT         $22 diag service-related updates
*   07-08-08      oem00024224   AN         ETC services 0xD60x are added
*   07-21-08      oem00024249   LK         EEPROM backup sections
*   09-04-08                    Harsha     Added EEPROM MAP for CSWM MY11
*   09-29-08      --            CK         EEPROM changes regarding to internal faults
*   10.30.08      --            CK         EEPROM changes regarding to battery compensation
*   11-03-08                    Harsha     EEPROM changes regarding STW Relay parameters
*   01-08-09      --            CK         EEPROM changes regarding to steering wheel low side off delay
*   04-08-09      28078         CK         EEPROM updates regarding to Seat heaters and STW current calculation
*   04-14-09      --            HY         EEPROM Updates, EE_GAIN removed and added EE_ROE_CFG,EE_ECU_CFG
*   08-21-2009   MKS CR32456    CC         To add different thresholds to be different with Front and Rear seats
*   02-17-2010   MKS 47356      HY         Loadshed 2/4 Last Known Odo stamps variables added.
*                                          EE_LOADSHED2_ODOSTAMP and EE_LOADSHED4_ODOSTAMP
*   04-14-2010   MKS 50039      HY         Set of 136 variables (17 each for 8 differnt vehicle lines )added for new feature.
*                                          Previously used 17 parameters (All vehicle lines uses same calibration) commented and made that space as reserved
*   04-15-2010   MKS 50111      HY         Enhancements to Heat Fault detection calibration values
*                                          Moved all 9 Individual Heater Threshold EEP Blocks to 1.
*                                          Moved all 6 Individual Relay Threshols EEP blocks to single one.
*   06-11-2010   MKS 52372      HY         EE_HEAT_FAULT_THRESHOLDS_SIZE changed from 9 parameters to 7 parameters
*                                          EE_RESERVE_DATA_SIZE increased from 11 to 13 bytes
*   10-05-2010   MKS 57313      HY         EE_HEAT_CALIB_DATA_SIZE reduced to 114 from 152
*                                          This is because TIPM vs PN sets differentiated.
*                                          EE_SEAT_CFG new variable added for Leather vs cloth info to be stored in EEPROM
******************************************************************************/
/******************************************************************************
 * 							CUSW UPDATES
 *  04-13-2011   MKS 69496      HY         Update the new Boot Block EEP structures
 *  06-02-2011   MKS 74315		HY			Adjusted EEPROM Table to fit full 3 pages data.
 *  08-04-3011   MKS 81838		
 * 				 MKS 81845  	HY			EEPROM MAP to be adjusted for AB Release becasue of PROXI and Bootloader support
******************************************************************************/

/*****************************************************************************************************
*   Include Files
*****************************************************************************************************/
#include "itbm_basapi.h"
#include "hwio_eeprom.h"
#include "mw_eem.h"
#include "hwio_eeprom_cfg.h"
#include "mw_diagm_dtc.h"
#include "mw_cfgm_dtc_cfg.h"
#include "fbl_def.h"
#include "EepMap.h"
#include "venting.h"
/*****************************************************************************************************
*   Local Macro Definitions
*****************************************************************************************************/
#define EE_VRGN_FLAG_START                      0x0000

#define EE_DATA_START                           0x0100
#define EE_DATA_BKUP_START                      0x0500

#define EE_EEPROM_VERSION_START                 (EE_LOAD_DEFAULTS_START+EE_LOAD_DEFAULTS_SIZE)
#define EE_AUTOSAR_100P_PWM_START               (EE_EEPROM_VERSION_START+EE_EEPROM_VERSION_SIZE)

#define EE_FBL_TOTAL_START                      (kEepFblBaseAddress - 0x100000)
#define EE_FBL_PROGREQ_FLAGS_START              (kEepFblBaseAddress - 0x100000)
//#define EE_FBL_APPL_VALID_FLAG_START            (EE_FBL_PROGREQ_FLAGS_START+EE_FBL_PROGREQ_FLAGS_SIZE)
//#define EE_FBL_PROG_ATTEMPTS_START              (EE_FBL_APPL_VALID_FLAG_START+EE_FBL_APPL_VALID_FLAG_SIZE) 
//#define EE_FBL_PROG_STATUS_START                (EE_FBL_PROG_ATTEMPTS_START+EE_FBL_PROG_ATTEMPTS_SIZE)
//#define EE_FBL_BOOT_SW_IDENT_START				(EE_FBL_PROG_STATUS_START+EE_FBL_PROG_STATUS_SIZE)
//#define EE_FBL_APPL_SW_IDENT_START				(EE_FBL_BOOT_SW_IDENT_START+EE_FBL_BOOT_SW_IDENT_SIZE)
//#define EE_FBL_APPL_DATA_IDENT_START			(EE_FBL_APPL_SW_IDENT_START+EE_FBL_APPL_SW_IDENT_SIZE)
//#define EE_FBL_BOOT_SW_FPRINT_START				(EE_FBL_APPL_DATA_IDENT_START+EE_FBL_APPL_DATA_IDENT_SIZE)
//#define EE_FBL_APPL_SW_FPRINT_START				(EE_FBL_BOOT_SW_FPRINT_START+EE_FBL_BOOT_SW_FPRINT_SIZE)
//#define EE_FBL_APPL_DATA_FPRINT_START			(EE_FBL_APPL_SW_FPRINT_START+EE_FBL_APPL_SW_FPRINT_SIZE)
//#define EE_FBL_SPARE_PNO_START					(EE_FBL_APPL_DATA_FPRINT_START+EE_FBL_APPL_DATA_FPRINT_SIZE)
//#define EE_FBL_FIAT_ECU_HW_NO_START				(EE_FBL_SPARE_PNO_START+EE_FBL_SPARE_PNO_SIZE)
//#define EE_FBL_FIAT_ECU_HW_VERSION_START        (EE_FBL_FIAT_ECU_HW_NO_START+EE_FBL_FIAT_ECU_HW_NO_SIZE)
//#define EE_FBL_FIAT_ECU_SW_NO_START             (EE_FBL_FIAT_ECU_HW_VERSION_START+EE_FBL_FIAT_ECU_HW_VERSION_SIZE)
//#define EE_FBL_FIAT_ECU_SW_VERSION_START        (EE_FBL_FIAT_ECU_SW_NO_START+EE_FBL_FIAT_ECU_SW_NO_SIZE)
//#define EE_FBL_APPROVAL_NO_START				(EE_FBL_FIAT_ECU_SW_VERSION_START+EE_FBL_FIAT_ECU_SW_VERSION_SIZE)				
//#define EE_FBL_ADDRESS_IDENT_START				(EE_FBL_APPROVAL_NO_START+EE_FBL_APPROVAL_NO_SIZE)
//#define EE_FBL_CHR_APPL_SW_PNO_START			(EE_FBL_ADDRESS_IDENT_START+EE_FBL_ADDRESS_IDENT_SIZE)
//#define EE_FBL_CHR_DATA_SW_PNO_START			(EE_FBL_CHR_APPL_SW_PNO_START+EE_FBL_CHR_APPL_SW_PNO_SIZE)
//#define EE_FBL_CHR_ECU_PNO_START				(EE_FBL_CHR_DATA_SW_PNO_START+EE_FBL_CHR_DATA_SW_PNO_SIZE)

#define EE_CXSUM_DATA_OFST 0x0E00
#define EE_CXSUM_DATA_BKUP_OFST 0x0F00

/* move these even high address? */
#define EE_CXSUM_FAILED_FLAGS_START             0x1000
#define EE_CXSUM_FAILED_FLAGS2_START            0x1100
#define EE_CXSUM_FAILED_FLAGS3_START            0x1200
#define EE_BACKUPS_INITALIZED_START             0x1300

/* special flag to hold the checksum is invalid flag - is written directly */
/* this is just a placeholder in this file to make sure it isn't used accidentally */
//#define EE_CXSUM_INVALID_FLAG                   0x0EF8

#define EE_VRGN_FLAG_END                        0x1400

/* The addresses 0xF00-0xFFB are reserved the the Bootloader                 */
/* The addresses 0xFFC-0xFFF are reserved for EEPROM protection by the micro */
/* DO NOT place any data at this address range!!                             */

/* the following is for the ECU Fingerprint data read/written in the bootloader EEPROM */
/* this data can be found in the EepMap.h file in /FBL/EepDrv */
/* did not use the data directly from the file to ensure no code sharing with the bootloader */

/* If any bytes added before EE_IREF_SEAT_HEAT_SIZE, reduce the no of bytes in EE_RESERVE_DATA_SIZE */
/*****************************************************************************/
//#define EE_ONE_BYTE_SIZE						(sizeof(UINT8))
//#define EE_TWO_BYTE_SIZE						(sizeof(UINT16)
//#define EE_THREE_BYTE_SIZE                    3
//#define EE_FOUR_BYTE_SIZE                     (sizeof(UINT32))

#define EE_LOAD_DEFAULTS_SIZE                   (sizeof(UINT8))
#define EE_EEPROM_VERSION_SIZE                  (sizeof(UINT16))
#define EE_AUTOSAR_100P_PWM_SIZE                (sizeof(UINT16))
#define EE_HEAT_FAULT_THRESHOLDS_SIZE           7  
#define EE_RELAY_THRESHOLDS_SIZE                6
#define EE_VENT_PWM_DATA_SIZE                   (sizeof(struct vs_info_data))//18(sizeof(struct vsdata_s)) /*3*/
#define EE_VENT_FLT_CAL_DATA_SIZE               (sizeof(struct vsflt_s))  /*8*/
#define EE_TEMPERATURE_CALIB_DATA_SIZE          (sizeof(struct Temperature_data_s)) /*8*/ 
#define EE_BATTERY_CALIB_DATA_SIZE              (sizeof(struct Battery_data_s)) /*8*/
#define EE_BATT_COMPENSATION_SIZE               6
#define EE_WHEEL_CALIB_DATA_SIZE                (sizeof(struct stW_data_info))//190(sizeof(struct stW_data_s))  /*19*/

#define EE_WHEEL_FAULT_THRESHOLDS_SIZE          6
#define EE_HEAT_CAL_DATA_PF_SIZE                84  // 6 sets of 14 bytes
#define EE_PLL_FAULT_TIME_LMT_SIZE              (sizeof(UINT8))
#define EE_REMOTE_CTRL_STAT_SIZE                (sizeof(UINT8))
#define EE_REMOTE_CTRL_HEAT_AMBTEMP_SIZE        (sizeof(UINT8))
#define EE_REMOTE_CTRL_VENT_AMBTEMP_SIZE        (sizeof(UINT8))
#define EE_ENG_RPM_LIMIT_SIZE                   (sizeof(UINT8))
#define EE_SEC_FAA_SIZE                         (sizeof(UINT8))
#define EE_OVER_TEMP_FAILSAFE_SIZE              6
#define EE_HEAT_NTC_FAILSAFE_SIZE               5//(sizeof(struct hs_ntc_failsafe_s)) 
#define EE_CHRONO_STACK_SIZE                    110 
#define EE_INTERNAL_FLT_SIZE                    (sizeof(UINT8))
#define EE_INTERNAL_FLT_CFG_SIZE                (sizeof(UINT8))
//#define EE_SERIAL_NUMBER_SIZE                   15
#define EE_PROXI_CFG_SIZE                       11
#define EE_PROXI_FAILURE_SIZE					9
#define EE_PROXI_LEARNED_SIZE					(sizeof(UINT8))
#define EE_RESERVE_DATAS2_SIZE					13

#define EE_DTC_STATUS_SIZE                      92/*(sizeof(struct DTC_RAM_STR)*ITBM_DTC_ARRAY_ELEMENTS)*/
#define EE_HISTORICAL_STACK_SIZE                90
//#define EE_VIN_ORIGINAL_SIZE                    17
#define EE_CTRL_BYTE_FINAL_SIZE                 (sizeof(UINT8))
#define EE_BOARD_TEMIC_BOM_NUMBER_SIZE          15
#define EE_ODOMETER_SIZE						 3
#define EE_ODOMETER_LAST_PGM_SIZE				 3
#define	EE_ECU_TIMESTAMPS_SIZE					 (sizeof(UINT32))
#define	EE_ECU_TIME_KEYON_SIZE					 (sizeof(UINT16))
#define	EE_ECU_KEYON_COUNTER_SIZE				 (sizeof(UINT16))
#define	EE_ECU_TIME_FIRSTDTC_SIZE				 (sizeof(UINT32))
#define	EE_ECU_TIME_KEYON_FIRSTDTC_SIZE			 (sizeof(UINT16))
#define EE_DTC_SIZE                              1  //Just the Index
#define EE_RESERVE_DATAS3_SIZE					 36

#define EE_FBL_TOTAL_SIZE						196
#define EE_FBL_PROGREQ_FLAGS_SIZE				(kEepSizeReprogFlag)
#define EE_FBL_APPL_VALID_FLAG_SIZE				(kEepSizeValidityFlags)
#define EE_FBL_PROG_ATTEMPTS_SIZE				(kEepSizeProgAttempts)
#define EE_FBL_PROG_STATUS_SIZE					(kEepSizeProgState)
#define EE_FBL_REPROG_REQ_SIZE					(kEepSizeReprogReq)
#define EE_FBL_BOOT_SW_IDENT_SIZE				(kEepSizeIdentification)
#define EE_FBL_APPL_SW_IDENT_SIZE				(kEepSizeIdentification)
#define EE_FBL_APPL_DATA_IDENT_SIZE				(kEepSizeIdentification)
#define EE_FBL_BOOT_SW_FPRINT_SIZE				(kEepSizeFingerprint)
#define EE_FBL_APPL_SW_FPRINT_SIZE				(kEepSizeFingerprint)
#define EE_FBL_APPL_DATA_FPRINT_SIZE			(kEepSizeFingerprint)
#define EE_FBL_SPARE_PNO_SIZE					(kEepSizeSparePartNo)
#define EE_FBL_FIAT_ECU_HW_NO_SIZE				(kEepSizeEcuHwNo)
#define EE_FBL_FIAT_ECU_HW_VERSION_SIZE			(kEepSizeEcuHwVersionNo)
#define EE_FBL_FIAT_ECU_SW_NO_SIZE				(kEepSizeEcuSwNo)
#define EE_FBL_FIAT_ECU_SW_VERSION_SIZE			(kEepSizeEcuSwVersionNo)
#define EE_FBL_APPROVAL_NO_SIZE					(kEepSizeApprovalNo)
#define EE_FBL_ADDRESS_IDENT_SIZE				(kEepSizeIdentOption)
#define EE_FBL_CHR_APPL_SW_PNO_SIZE				(kEepSizeSoftwarePartNo)
#define EE_FBL_CHR_DATA_SW_PNO_SIZE				(kEepSizeSoftwarePartNo)
#define EE_FBL_CHR_ECU_PNO_SIZE					(kEepSizeEcuPartNo)
#define EE_SERIAL_NUMBER_SIZE					(KEepSizeEcuSerialNo)
#define EE_VIN_ORIGINAL_SIZE                    (KEepSizeVIN)


#define EE_CFG_CXSUM_SIZE                       (EE_SECTOR_SIZE)

#define EE_CXSUM_FAILED_FLAGS_SIZE              (EE_SECTOR_SIZE) 

/*****************************************************************************/
/* ORDER MATTERS - MUST MATCH aEEBlockTable[] BELOW! */
#define EE_LOAD_DEFAULTS_OFST                  (EE_DATA_START)
#define EE_EEPROM_VERSION_OFST                 (EE_LOAD_DEFAULTS_SIZE+EE_LOAD_DEFAULTS_OFST)
#define EE_AUTOSAR_100P_PWM_OFST               (EE_EEPROM_VERSION_SIZE+EE_EEPROM_VERSION_OFST)
#define EE_HEAT_FAULT_THRESHOLDS_OFST          (EE_AUTOSAR_100P_PWM_SIZE+EE_AUTOSAR_100P_PWM_OFST)
#define EE_RELAY_THRESHOLDS_OFST               (EE_HEAT_FAULT_THRESHOLDS_SIZE+EE_HEAT_FAULT_THRESHOLDS_OFST)
#define EE_VENT_PWM_DATA_OFST                  (EE_RELAY_THRESHOLDS_SIZE+EE_RELAY_THRESHOLDS_OFST)
#define EE_VENT_FLT_CAL_DATA_OFST              (EE_VENT_PWM_DATA_SIZE+EE_VENT_PWM_DATA_OFST)
#define EE_TEMPERATURE_CALIB_DATA_OFST         (EE_VENT_FLT_CAL_DATA_SIZE+EE_VENT_FLT_CAL_DATA_OFST)
#define EE_BATTERY_CALIB_DATA_OFST             (EE_TEMPERATURE_CALIB_DATA_SIZE+EE_TEMPERATURE_CALIB_DATA_OFST)
#define EE_BATT_COMPENSATION_OFST              (EE_BATTERY_CALIB_DATA_SIZE+EE_BATTERY_CALIB_DATA_OFST)  
#define EE_WHEEL_CAL_DATA_OFST                 (EE_BATT_COMPENSATION_SIZE+EE_BATT_COMPENSATION_OFST)

#define EE_WHEEL_FAULT_THRESHOLDS_OFST         (EE_WHEEL_CALIB_DATA_SIZE+EE_WHEEL_CAL_DATA_OFST)  
#define EE_HEAT_CAL_DATA_PF_OFST               (EE_WHEEL_FAULT_THRESHOLDS_SIZE+EE_WHEEL_FAULT_THRESHOLDS_OFST)  
#define EE_PLL_FAULT_TIME_LMT_OFST             (EE_HEAT_CAL_DATA_PF_SIZE+EE_HEAT_CAL_DATA_PF_OFST)
#define EE_REMOTE_CTRL_STAT_OFST               (EE_PLL_FAULT_TIME_LMT_SIZE+EE_PLL_FAULT_TIME_LMT_OFST)
#define EE_REMOTE_CTRL_HEAT_AMBTEMP_OFST       (EE_REMOTE_CTRL_STAT_SIZE+EE_REMOTE_CTRL_STAT_OFST)
#define EE_REMOTE_CTRL_VENT_AMBTEMP_OFST       (EE_REMOTE_CTRL_HEAT_AMBTEMP_SIZE+EE_REMOTE_CTRL_HEAT_AMBTEMP_OFST)
#define EE_ENG_RPM_LIMIT_OFST                  (EE_REMOTE_CTRL_VENT_AMBTEMP_SIZE+EE_REMOTE_CTRL_VENT_AMBTEMP_OFST)
#define EE_SEC_FAA_OFST                        (EE_ENG_RPM_LIMIT_SIZE+EE_ENG_RPM_LIMIT_OFST)
#define EE_OVER_TEMP_FAILSAFE_OFST             (EE_SEC_FAA_SIZE+EE_SEC_FAA_OFST) 
#define EE_HEAT_NTC_FAILSAFE_OFST              (EE_OVER_TEMP_FAILSAFE_SIZE+EE_OVER_TEMP_FAILSAFE_OFST)
#define EE_CHRONO_STACK_OFST                   (EE_HEAT_NTC_FAILSAFE_SIZE+EE_HEAT_NTC_FAILSAFE_OFST)
#define EE_INTERNAL_FLT_BYTE0_OFST             (EE_CHRONO_STACK_SIZE + EE_CHRONO_STACK_OFST)
#define EE_INTERNAL_FLT_BYTE1_OFST             (EE_INTERNAL_FLT_SIZE+EE_INTERNAL_FLT_BYTE0_OFST)
#define EE_INTERNAL_FLT_BYTE2_OFST             (EE_INTERNAL_FLT_SIZE+EE_INTERNAL_FLT_BYTE1_OFST)
#define EE_INTERNAL_FLT_BYTE3_OFST             (EE_INTERNAL_FLT_SIZE+EE_INTERNAL_FLT_BYTE2_OFST)
#define EE_INTERNAL_FLT_CFG_OFST               (EE_INTERNAL_FLT_SIZE+EE_INTERNAL_FLT_BYTE3_OFST)
//#define EE_SERIAL_NUMBER_OFST                  (EE_INTERNAL_FLT_CFG_SIZE + EE_INTERNAL_FLT_CFG_OFST)
#define EE_PROXI_CFG_OFST                      (EE_INTERNAL_FLT_CFG_SIZE + EE_INTERNAL_FLT_CFG_OFST)
#define EE_PROXI_FAILURE_OFST                  (EE_PROXI_CFG_SIZE + EE_PROXI_CFG_OFST)
#define EE_PROXI_LEARNED_OFST                  (EE_PROXI_FAILURE_SIZE + EE_PROXI_FAILURE_OFST)
#define EE_RESERVE_DATAS2_OFST                 (EE_PROXI_LEARNED_SIZE + EE_PROXI_LEARNED_OFST)

#define EE_DTC_STATUS_OFST                     (EE_RESERVE_DATAS2_SIZE + EE_RESERVE_DATAS2_OFST)
#define EE_HISTORICAL_STACK_OFST               (EE_DTC_STATUS_SIZE + EE_DTC_STATUS_OFST)
//#define EE_VIN_ORIGINAL_OFST                   (EE_HISTORICAL_STACK_SIZE + EE_HISTORICAL_STACK_OFST)
#define EE_CTRL_BYTE_FINAL_OFST                (EE_HISTORICAL_STACK_SIZE + EE_HISTORICAL_STACK_OFST)
#define EE_BOARD_TEMIC_BOM_NUMBER_OFST         (EE_CTRL_BYTE_FINAL_SIZE + EE_CTRL_BYTE_FINAL_OFST)
#define	EE_ODOMETER_OFST					   (EE_BOARD_TEMIC_BOM_NUMBER_SIZE+EE_BOARD_TEMIC_BOM_NUMBER_OFST)
#define	EE_ODOMETER_LAST_PGM_OFST			   (EE_ODOMETER_SIZE+EE_ODOMETER_OFST)
#define	EE_ECU_TIMESTAMPS_OFST				   (EE_ODOMETER_LAST_PGM_SIZE+EE_ODOMETER_LAST_PGM_OFST)
#define	EE_ECU_TIME_KEYON_OFST				   (EE_ECU_TIMESTAMPS_SIZE+EE_ECU_TIMESTAMPS_OFST)
#define	EE_ECU_KEYON_COUNTER_OFST			   (EE_ECU_TIME_KEYON_SIZE+EE_ECU_TIME_KEYON_OFST)
#define	EE_ECU_TIME_FIRSTDTC_OFST			   (EE_ECU_KEYON_COUNTER_SIZE+EE_ECU_KEYON_COUNTER_OFST)
#define	EE_ECU_TIME_KEYON_FIRSTDTC_OFST		   (EE_ECU_TIME_FIRSTDTC_SIZE+EE_ECU_TIME_FIRSTDTC_OFST)
#define	EE_FIRST_CONF_DTC_OFST		           (EE_ECU_TIME_KEYON_FIRSTDTC_SIZE+EE_ECU_TIME_KEYON_FIRSTDTC_OFST)
#define	EE_RECENT_CONF_DTC_OFST		           (EE_DTC_SIZE+EE_FIRST_CONF_DTC_OFST)
#define	EE_RESERVE_DATAS3_OFST		           (EE_DTC_SIZE+EE_RECENT_CONF_DTC_OFST)

#define EE_DATA_END                            ((EE_RESERVE_DATAS3_OFST + EE_RESERVE_DATAS3_SIZE)-1)

#define EE_LOAD_DEFAULTS_BKUP_OFST                  (EE_DATA_BKUP_START)
#define EE_EEPROM_VERSION_BKUP_OFST                 (EE_LOAD_DEFAULTS_SIZE+EE_LOAD_DEFAULTS_BKUP_OFST)
#define EE_AUTOSAR_100P_PWM_BKUP_OFST               (EE_EEPROM_VERSION_SIZE+EE_EEPROM_VERSION_BKUP_OFST)
#define EE_HEAT_FAULT_THRESHOLDS_BKUP_OFST          (EE_AUTOSAR_100P_PWM_SIZE+EE_AUTOSAR_100P_PWM_BKUP_OFST)
#define EE_RELAY_THRESHOLDS_BKUP_OFST               (EE_HEAT_FAULT_THRESHOLDS_SIZE+EE_HEAT_FAULT_THRESHOLDS_BKUP_OFST)
#define EE_VENT_PWM_DATA_BKUP_OFST                  (EE_RELAY_THRESHOLDS_SIZE+EE_RELAY_THRESHOLDS_BKUP_OFST)
#define EE_VENT_FLT_CAL_DATA_BKUP_OFST              (EE_VENT_PWM_DATA_SIZE+EE_VENT_PWM_DATA_BKUP_OFST)
#define EE_TEMPERATURE_CALIB_DATA_BKUP_OFST         (EE_VENT_FLT_CAL_DATA_SIZE+EE_VENT_FLT_CAL_DATA_BKUP_OFST)
#define EE_BATTERY_CALIB_DATA_BKUP_OFST             (EE_TEMPERATURE_CALIB_DATA_SIZE+EE_TEMPERATURE_CALIB_DATA_BKUP_OFST)
#define EE_BATT_COMPENSATION_BKUP_OFST              (EE_BATTERY_CALIB_DATA_SIZE+EE_BATTERY_CALIB_DATA_BKUP_OFST)
#define EE_WHEEL_CAL_DATA_BKUP_OFST                 (EE_BATT_COMPENSATION_SIZE+EE_BATT_COMPENSATION_BKUP_OFST)

#define EE_WHEEL_FAULT_THRESHOLDS_BKUP_OFST		    (EE_WHEEL_CALIB_DATA_SIZE+EE_WHEEL_CAL_DATA_BKUP_OFST)				
#define EE_HEAT_CAL_DATA_PF_BKUP_OFST				(EE_WHEEL_FAULT_THRESHOLDS_SIZE+EE_WHEEL_FAULT_THRESHOLDS_BKUP_OFST)				
#define EE_PLL_FAULT_TIME_LMT_BKUP_OFST             (EE_HEAT_CAL_DATA_PF_SIZE+EE_HEAT_CAL_DATA_PF_BKUP_OFST)
#define EE_REMOTE_CTRL_STAT_BKUP_OFST               (EE_PLL_FAULT_TIME_LMT_SIZE+EE_PLL_FAULT_TIME_LMT_BKUP_OFST)
#define EE_REMOTE_CTRL_HEAT_AMBTEMP_BKUP_OFST       (EE_REMOTE_CTRL_STAT_SIZE+EE_REMOTE_CTRL_STAT_BKUP_OFST)
#define EE_REMOTE_CTRL_VENT_AMBTEMP_BKUP_OFST       (EE_REMOTE_CTRL_HEAT_AMBTEMP_SIZE+EE_REMOTE_CTRL_HEAT_AMBTEMP_BKUP_OFST)
#define EE_ENG_RPM_LIMIT_BKUP_OFST                  (EE_REMOTE_CTRL_VENT_AMBTEMP_SIZE+EE_REMOTE_CTRL_VENT_AMBTEMP_BKUP_OFST)
#define EE_SEC_FAA_BKUP_OFST                        (EE_ENG_RPM_LIMIT_SIZE+EE_ENG_RPM_LIMIT_BKUP_OFST)
#define EE_OVER_TEMP_FAILSAFE_BKUP_OFST             (EE_SEC_FAA_SIZE+EE_SEC_FAA_BKUP_OFST)
#define EE_HEAT_NTC_FAILSAFE_BKUP_OFST              (EE_OVER_TEMP_FAILSAFE_SIZE+EE_OVER_TEMP_FAILSAFE_BKUP_OFST)
#define EE_CHRONO_STACK_BKUP_OFST                   (EE_HEAT_NTC_FAILSAFE_SIZE+EE_HEAT_NTC_FAILSAFE_BKUP_OFST)
#define EE_INTERNAL_FLT_BYTE0_BKUP_OFST             (EE_CHRONO_STACK_SIZE+EE_CHRONO_STACK_BKUP_OFST)
#define EE_INTERNAL_FLT_BYTE1_BKUP_OFST             (EE_INTERNAL_FLT_SIZE+EE_INTERNAL_FLT_BYTE0_BKUP_OFST)
#define EE_INTERNAL_FLT_BYTE2_BKUP_OFST             (EE_INTERNAL_FLT_SIZE+EE_INTERNAL_FLT_BYTE1_BKUP_OFST)
#define EE_INTERNAL_FLT_BYTE3_BKUP_OFST             (EE_INTERNAL_FLT_SIZE+EE_INTERNAL_FLT_BYTE2_BKUP_OFST)
#define EE_INTERNAL_FLT_CFG_BKUP_OFST               (EE_INTERNAL_FLT_SIZE+EE_INTERNAL_FLT_BYTE3_BKUP_OFST)
//#define EE_SERIAL_NUMBER_BKUP_OFST                  (EE_INTERNAL_FLT_CFG_SIZE + EE_INTERNAL_FLT_CFG_BKUP_OFST)
#define EE_PROXI_CFG_BKUP_OFST                      (EE_INTERNAL_FLT_CFG_SIZE + EE_INTERNAL_FLT_CFG_BKUP_OFST)
#define EE_PROXI_FAILURE_BKUP_OFST                  (EE_PROXI_CFG_SIZE + EE_PROXI_CFG_BKUP_OFST)
#define EE_PROXI_LEARNED_BKUP_OFST                  (EE_PROXI_FAILURE_SIZE + EE_PROXI_FAILURE_BKUP_OFST)
#define EE_RESERVE_DATAS2_BKUP_OFST                 (EE_PROXI_LEARNED_SIZE + EE_PROXI_LEARNED_BKUP_OFST)


#define EE_DTC_STATUS_BKUP_OFST                     (EE_RESERVE_DATAS2_SIZE + EE_RESERVE_DATAS2_BKUP_OFST)
#define EE_HISTORICAL_STACK_BKUP_OFST               (EE_DTC_STATUS_SIZE + EE_DTC_STATUS_BKUP_OFST)
//#define EE_VIN_ORIGINAL_BKUP_OFST                   (EE_HISTORICAL_STACK_SIZE + EE_HISTORICAL_STACK_BKUP_OFST)
#define EE_CTRL_BYTE_FINAL_BKUP_OFST                (EE_HISTORICAL_STACK_SIZE + EE_HISTORICAL_STACK_BKUP_OFST)
#define EE_BOARD_TEMIC_BOM_NUMBER_BKUP_OFST         (EE_CTRL_BYTE_FINAL_SIZE + EE_CTRL_BYTE_FINAL_BKUP_OFST)
#define EE_ODOMETER_BKUP_OFST                       (EE_BOARD_TEMIC_BOM_NUMBER_SIZE+EE_BOARD_TEMIC_BOM_NUMBER_BKUP_OFST)
#define EE_ODOMETER_LAST_PGM_BKUP_OFST              (EE_ODOMETER_SIZE+EE_ODOMETER_BKUP_OFST)
#define EE_ECU_TIMESTAMPS_BKUP_OFST                 (EE_ODOMETER_LAST_PGM_SIZE+EE_ODOMETER_LAST_PGM_BKUP_OFST)
#define EE_ECU_TIME_KEYON_BKUP_OFST                 (EE_ECU_TIMESTAMPS_SIZE+EE_ECU_TIMESTAMPS_BKUP_OFST)
#define EE_ECU_KEYON_COUNTER_BKUP_OFST              (EE_ECU_TIME_KEYON_SIZE+EE_ECU_TIME_KEYON_BKUP_OFST)
#define EE_ECU_TIME_FIRSTDTC_BKUP_OFST              (EE_ECU_KEYON_COUNTER_SIZE+EE_ECU_KEYON_COUNTER_BKUP_OFST)
#define EE_ECU_TIME_KEYON_FIRSTDTC_BKUP_OFST        (EE_ECU_TIME_FIRSTDTC_SIZE+EE_ECU_TIME_FIRSTDTC_BKUP_OFST)
#define	EE_FIRST_CONF_DTC_BKUP_OFST		            (EE_ECU_TIME_KEYON_FIRSTDTC_SIZE+EE_ECU_TIME_KEYON_FIRSTDTC_BKUP_OFST)
#define	EE_RECENT_CONF_DTC_BKUP_OFST		        (EE_DTC_SIZE+EE_FIRST_CONF_DTC_BKUP_OFST)
#define EE_RESERVE_DATAS3_BKUP_OFST                 (EE_DTC_SIZE+EE_RECENT_CONF_DTC_BKUP_OFST)

#define EE_DATA_BKUP_END                            ((EE_RESERVE_DATAS3_BKUP_OFST + EE_RESERVE_DATAS3_SIZE)-1)

#define EE_FBL_TOTAL_OFST						(kEepFblBaseAddress - 0x100000)
#define EE_FBL_PROGREQ_FLAGS_OFST				(kEepFblBaseAddress - 0x100000)
#define EE_FBL_APPL_VALID_FLAG_OFST				(EE_FBL_PROGREQ_FLAGS_SIZE+EE_FBL_PROGREQ_FLAGS_OFST)
#define EE_FBL_PROG_ATTEMPTS_OFST				(EE_FBL_APPL_VALID_FLAG_SIZE+EE_FBL_APPL_VALID_FLAG_OFST)
#define EE_FBL_PROG_STATUS_OFST					(EE_FBL_PROG_ATTEMPTS_SIZE+EE_FBL_PROG_ATTEMPTS_OFST)
#define EE_FBL_REPROG_REQ_OFST				    (EE_FBL_PROG_STATUS_SIZE+EE_FBL_PROG_STATUS_OFST)
#define EE_FBL_BOOT_SW_IDENT_OFST				(EE_FBL_REPROG_REQ_SIZE+EE_FBL_REPROG_REQ_OFST)
#define EE_FBL_APPL_SW_IDENT_OFST				(EE_FBL_BOOT_SW_IDENT_SIZE+EE_FBL_BOOT_SW_IDENT_OFST)
#define EE_FBL_APPL_DATA_IDENT_OFST				(EE_FBL_APPL_SW_IDENT_SIZE+EE_FBL_APPL_SW_IDENT_OFST)
#define EE_FBL_BOOT_SW_FPRINT_OFST				(EE_FBL_APPL_DATA_IDENT_SIZE+EE_FBL_APPL_DATA_IDENT_OFST)
#define EE_FBL_APPL_SW_FPRINT_OFST				(EE_FBL_BOOT_SW_FPRINT_SIZE+EE_FBL_BOOT_SW_FPRINT_OFST)
#define EE_FBL_APPL_DATA_FPRINT_OFST			(EE_FBL_APPL_SW_FPRINT_SIZE+EE_FBL_APPL_SW_FPRINT_OFST)
#define EE_FBL_SPARE_PNO_OFST					(EE_FBL_APPL_DATA_FPRINT_SIZE+EE_FBL_APPL_DATA_FPRINT_OFST)
#define EE_FBL_FIAT_ECU_HW_NO_OFST				(EE_FBL_SPARE_PNO_SIZE+EE_FBL_SPARE_PNO_OFST)
#define EE_FBL_FIAT_ECU_HW_VERSION_OFST			(EE_FBL_FIAT_ECU_HW_NO_SIZE+EE_FBL_FIAT_ECU_HW_NO_OFST)
#define EE_FBL_FIAT_ECU_SW_NO_OFST				(EE_FBL_FIAT_ECU_HW_VERSION_SIZE+EE_FBL_FIAT_ECU_HW_VERSION_OFST)
#define EE_FBL_FIAT_ECU_SW_VERSION_OFST			(EE_FBL_FIAT_ECU_SW_NO_SIZE+EE_FBL_FIAT_ECU_SW_NO_OFST)
#define EE_FBL_APPROVAL_NO_OFST					(EE_FBL_FIAT_ECU_SW_VERSION_SIZE+EE_FBL_FIAT_ECU_SW_VERSION_OFST)
#define EE_FBL_ADDRESS_IDENT_OFST				(EE_FBL_APPROVAL_NO_SIZE+EE_FBL_APPROVAL_NO_OFST)
#define EE_FBL_CHR_APPL_SW_PNO_OFST				(EE_FBL_ADDRESS_IDENT_SIZE+EE_FBL_ADDRESS_IDENT_OFST)
#define EE_FBL_CHR_DATA_SW_PNO_OFST				(EE_FBL_CHR_APPL_SW_PNO_SIZE+EE_FBL_CHR_APPL_SW_PNO_OFST)
#define EE_FBL_CHR_ECU_PNO_OFST					(EE_FBL_CHR_DATA_SW_PNO_SIZE+EE_FBL_CHR_DATA_SW_PNO_OFST)
#define EE_SERIAL_NUMBER_OFST                   (EE_FBL_CHR_ECU_PNO_SIZE + EE_FBL_CHR_ECU_PNO_OFST)
#define EE_VIN_ORIGINAL_OFST                    (EE_SERIAL_NUMBER_SIZE + EE_SERIAL_NUMBER_OFST)


#define EE_FBL_TOTAL_BKUP_OFST                  (kEepFblBackupBaseAddress - 0x100000)

#define SHADOW_DATA_START                       (EE_SECTOR_SIZE)

#define SHADOW_LOAD_DEFAULTS_OFST               (SHADOW_DATA_START)
#define SHADOW_EEPROM_VERSION_OFST              (EE_LOAD_DEFAULTS_SIZE + SHADOW_LOAD_DEFAULTS_OFST)
#define SHADOW_AUTOSAR_100P_PWM_OFST               (EE_EEPROM_VERSION_SIZE + SHADOW_EEPROM_VERSION_OFST)
#define SHADOW_HEAT_FAULT_THRESHOLDS_OFST          (EE_AUTOSAR_100P_PWM_SIZE+SHADOW_AUTOSAR_100P_PWM_OFST)
#define SHADOW_RELAY_THRESHOLDS_OFST               (EE_HEAT_FAULT_THRESHOLDS_SIZE+SHADOW_HEAT_FAULT_THRESHOLDS_OFST)
#define SHADOW_VENT_PWM_DATA_OFST                  (EE_RELAY_THRESHOLDS_SIZE+SHADOW_RELAY_THRESHOLDS_OFST)
#define SHADOW_VENT_FLT_CAL_DATA_OFST              (EE_VENT_PWM_DATA_SIZE+SHADOW_VENT_PWM_DATA_OFST)
#define SHADOW_TEMPERATURE_CALIB_DATA_OFST         (EE_VENT_FLT_CAL_DATA_SIZE+SHADOW_VENT_FLT_CAL_DATA_OFST)
#define SHADOW_BATTERY_CALIB_DATA_OFST             (EE_TEMPERATURE_CALIB_DATA_SIZE+SHADOW_TEMPERATURE_CALIB_DATA_OFST)
#define SHADOW_BATT_COMPENSATION_OFST              (EE_BATTERY_CALIB_DATA_SIZE+SHADOW_BATTERY_CALIB_DATA_OFST)
#define SHADOW_WHEEL_CAL_DATA_OFST                 (EE_BATT_COMPENSATION_SIZE+SHADOW_BATT_COMPENSATION_OFST)

#define SHADOW_WHEEL_FAULT_THRESHOLDS_OFST	       (EE_WHEEL_CALIB_DATA_SIZE+SHADOW_WHEEL_CAL_DATA_OFST)					
#define SHADOW_HEAT_CAL_DATA_PF_OFST			   (EE_WHEEL_FAULT_THRESHOLDS_SIZE+SHADOW_WHEEL_FAULT_THRESHOLDS_OFST)					
#define SHADOW_PLL_FAULT_TIME_LMT_OFST             (EE_HEAT_CAL_DATA_PF_SIZE + SHADOW_HEAT_CAL_DATA_PF_OFST)
#define SHADOW_REMOTE_CTRL_STAT_OFST               (EE_PLL_FAULT_TIME_LMT_SIZE+SHADOW_PLL_FAULT_TIME_LMT_OFST)
#define SHADOW_REMOTE_CTRL_HEAT_AMBTEMP_OFST       (EE_REMOTE_CTRL_STAT_SIZE+SHADOW_REMOTE_CTRL_STAT_OFST)
#define SHADOW_REMOTE_CTRL_VENT_AMBTEMP_OFST       (EE_REMOTE_CTRL_HEAT_AMBTEMP_SIZE+SHADOW_REMOTE_CTRL_HEAT_AMBTEMP_OFST)
#define SHADOW_ENG_RPM_LIMIT_OFST                  (EE_REMOTE_CTRL_VENT_AMBTEMP_SIZE+SHADOW_REMOTE_CTRL_VENT_AMBTEMP_OFST)
#define SHADOW_SEC_FAA_OFST                        (EE_ENG_RPM_LIMIT_SIZE+SHADOW_ENG_RPM_LIMIT_OFST)
#define SHADOW_OVER_TEMP_FAILSAFE_OFST             (EE_SEC_FAA_SIZE+SHADOW_SEC_FAA_OFST)
#define SHADOW_HEAT_NTC_FAILSAFE_OFST              (EE_OVER_TEMP_FAILSAFE_SIZE+SHADOW_OVER_TEMP_FAILSAFE_OFST)
#define SHADOW_CHRONO_STACK_OFST                   (EE_HEAT_NTC_FAILSAFE_SIZE+SHADOW_HEAT_NTC_FAILSAFE_OFST)
#define SHADOW_INTERNAL_FLT_BYTE0_OFST             (EE_CHRONO_STACK_SIZE+SHADOW_CHRONO_STACK_OFST)
#define SHADOW_INTERNAL_FLT_BYTE1_OFST             (EE_INTERNAL_FLT_SIZE+SHADOW_INTERNAL_FLT_BYTE0_OFST)
#define SHADOW_INTERNAL_FLT_BYTE2_OFST             (EE_INTERNAL_FLT_SIZE+SHADOW_INTERNAL_FLT_BYTE1_OFST)
#define SHADOW_INTERNAL_FLT_BYTE3_OFST             (EE_INTERNAL_FLT_SIZE+SHADOW_INTERNAL_FLT_BYTE2_OFST)
#define SHADOW_INTERNAL_FLT_CFG_OFST               (EE_INTERNAL_FLT_SIZE+SHADOW_INTERNAL_FLT_BYTE3_OFST)
//#define SHADOW_SERIAL_NUMBER_OFST                  (EE_INTERNAL_FLT_CFG_SIZE + SHADOW_INTERNAL_FLT_CFG_OFST)
#define SHADOW_PROXI_CFG_OFST                      (EE_INTERNAL_FLT_CFG_SIZE + SHADOW_INTERNAL_FLT_CFG_OFST)
#define SHADOW_PROXI_FAILURE_OFST                  (EE_PROXI_CFG_SIZE + SHADOW_PROXI_CFG_OFST)
#define SHADOW_PROXI_LEARNED_OFST                  (EE_PROXI_FAILURE_SIZE + SHADOW_PROXI_FAILURE_OFST)
#define SHADOW_RESERVE_DATAS2_OFST                 (EE_PROXI_LEARNED_SIZE + SHADOW_PROXI_LEARNED_OFST)



#define SHADOW_DTC_STATUS_OFST                     (EE_RESERVE_DATAS2_SIZE + SHADOW_RESERVE_DATAS2_OFST)
#define SHADOW_HISTORICAL_STACK_OFST               (EE_DTC_STATUS_SIZE + SHADOW_DTC_STATUS_OFST)
//#define SHADOW_VIN_ORIGINAL_OFST                   (EE_HISTORICAL_STACK_SIZE + SHADOW_HISTORICAL_STACK_OFST)
#define SHADOW_CTRL_BYTE_FINAL_OFST                (EE_HISTORICAL_STACK_SIZE + SHADOW_HISTORICAL_STACK_OFST)
#define SHADOW_BOARD_TEMIC_BOM_NUMBER_OFST         (EE_CTRL_BYTE_FINAL_SIZE + SHADOW_CTRL_BYTE_FINAL_OFST)
#define SHADOW_ODOMETER_OFST                       (EE_BOARD_TEMIC_BOM_NUMBER_SIZE+SHADOW_BOARD_TEMIC_BOM_NUMBER_OFST)
#define SHADOW_ODOMETER_LAST_PGM_OFST              (EE_ODOMETER_SIZE + SHADOW_ODOMETER_OFST)
#define SHADOW_ECU_TIMESTAMPS_OFST                 (EE_ODOMETER_LAST_PGM_SIZE + SHADOW_ODOMETER_LAST_PGM_OFST)
#define SHADOW_ECU_TIME_KEYON_OFST                 (EE_ECU_TIMESTAMPS_SIZE + SHADOW_ECU_TIMESTAMPS_OFST)
#define SHADOW_ECU_KEYON_COUNTER_OFST              (EE_ECU_TIME_KEYON_SIZE + SHADOW_ECU_TIME_KEYON_OFST)
#define SHADOW_ECU_TIME_FIRSTDTC_OFST              (EE_ECU_KEYON_COUNTER_SIZE + SHADOW_ECU_KEYON_COUNTER_OFST)
#define SHADOW_ECU_TIME_KEYON_FIRSTDTC_OFST        (EE_ECU_TIME_FIRSTDTC_SIZE + SHADOW_ECU_TIME_FIRSTDTC_OFST)
#define SHADOW_FIRST_CONF_DTC_OFST				   (EE_ECU_TIME_KEYON_FIRSTDTC_SIZE + SHADOW_ECU_TIME_KEYON_FIRSTDTC_OFST)
#define SHADOW_RECENT_CONF_DTC_OFST				   (EE_DTC_SIZE + SHADOW_FIRST_CONF_DTC_OFST)
#define SHADOW_RESERVE_DATAS3_OFST                 (EE_DTC_SIZE + SHADOW_RECENT_CONF_DTC_OFST)

#define SHADOW_CXSUM_DATA_OFST                     (EE_RESERVE_DATAS3_SIZE + SHADOW_RESERVE_DATAS3_OFST)
#define SHADOW_CXSUM_DATA_BKUP_OFST                (EE_CFG_CXSUM_SIZE + SHADOW_CXSUM_DATA_OFST)
#define SHADOW_DATA_END                            ((EE_CFG_CXSUM_SIZE + SHADOW_CXSUM_DATA_BKUP_OFST)-1)

#define SHADOW_FBL_TOTAL_START                  (EE_CFG_CXSUM_SIZE + SHADOW_CXSUM_DATA_BKUP_OFST)
#define SHADOW_FBL_TOTAL_OFST                   (SHADOW_FBL_PROGREQ_FLAGS_START)
#define SHADOW_FBL_PROGREQ_FLAGS_START          (EE_CFG_CXSUM_SIZE + SHADOW_CXSUM_DATA_BKUP_OFST)
#define SHADOW_FBL_PROGREQ_FLAGS_OFST           (SHADOW_FBL_PROGREQ_FLAGS_START)
#define SHADOW_FBL_APPL_VALID_FLAG_START        (EE_FBL_PROGREQ_FLAGS_SIZE + SHADOW_FBL_PROGREQ_FLAGS_OFST)    
#define SHADOW_FBL_APPL_VALID_FLAG_OFST         (SHADOW_FBL_APPL_VALID_FLAG_START)
#define SHADOW_FBL_PROG_ATTEMPTS_START          (EE_FBL_APPL_VALID_FLAG_SIZE + SHADOW_FBL_APPL_VALID_FLAG_OFST)
#define SHADOW_FBL_PROG_ATTEMPTS_OFST           (SHADOW_FBL_PROG_ATTEMPTS_START)
#define SHADOW_FBL_PROG_STATUS_START            (EE_FBL_PROG_ATTEMPTS_SIZE + SHADOW_FBL_PROG_ATTEMPTS_OFST)
#define SHADOW_FBL_PROG_STATUS_OFST             (SHADOW_FBL_PROG_STATUS_START)
#define SHADOW_FBL_REPROG_REQ_START          	(EE_FBL_PROG_STATUS_SIZE+SHADOW_FBL_PROG_STATUS_OFST)
#define SHADOW_FBL_REPROG_REQ_OFST           	(SHADOW_FBL_REPROG_REQ_START)
#define SHADOW_FBL_BOOT_SW_IDENT_START          (EE_FBL_REPROG_REQ_SIZE+SHADOW_FBL_REPROG_REQ_OFST)
#define SHADOW_FBL_BOOT_SW_IDENT_OFST           (SHADOW_FBL_BOOT_SW_IDENT_START)
#define SHADOW_FBL_APPL_SW_IDENT_START          (EE_FBL_BOOT_SW_IDENT_SIZE+SHADOW_FBL_BOOT_SW_IDENT_OFST)
#define SHADOW_FBL_APPL_SW_IDENT_OFST           (SHADOW_FBL_APPL_SW_IDENT_START)
#define SHADOW_FBL_APPL_DATA_IDENT_START        (EE_FBL_APPL_SW_IDENT_SIZE+SHADOW_FBL_APPL_SW_IDENT_OFST)
#define SHADOW_FBL_APPL_DATA_IDENT_OFST         (SHADOW_FBL_APPL_DATA_IDENT_START)
#define SHADOW_FBL_BOOT_SW_FPRINT_START         (EE_FBL_APPL_DATA_IDENT_SIZE+SHADOW_FBL_APPL_DATA_IDENT_OFST)
#define SHADOW_FBL_BOOT_SW_FPRINT_OFST          (SHADOW_FBL_BOOT_SW_FPRINT_START)
#define SHADOW_FBL_APPL_SW_FPRINT_START         (EE_FBL_BOOT_SW_FPRINT_SIZE+SHADOW_FBL_BOOT_SW_FPRINT_OFST)
#define SHADOW_FBL_APPL_SW_FPRINT_OFST          (SHADOW_FBL_APPL_SW_FPRINT_START)
#define SHADOW_FBL_APPL_DATA_FPRINT_START       (EE_FBL_APPL_SW_FPRINT_SIZE+SHADOW_FBL_APPL_SW_FPRINT_OFST)
#define SHADOW_FBL_APPL_DATA_FPRINT_OFST        (SHADOW_FBL_APPL_DATA_FPRINT_START)
#define SHADOW_FBL_SPARE_PNO_START              (EE_FBL_APPL_DATA_FPRINT_SIZE+SHADOW_FBL_APPL_DATA_FPRINT_OFST)
#define SHADOW_FBL_SPARE_PNO_OFST               (SHADOW_FBL_SPARE_PNO_START)
#define SHADOW_FBL_FIAT_ECU_HW_NO_START         (EE_FBL_SPARE_PNO_SIZE+SHADOW_FBL_SPARE_PNO_OFST)
#define SHADOW_FBL_FIAT_ECU_HW_NO_OFST          (SHADOW_FBL_FIAT_ECU_HW_NO_START)
#define SHADOW_FBL_FIAT_ECU_HW_VERSION_START    (EE_FBL_FIAT_ECU_HW_NO_SIZE+SHADOW_FBL_FIAT_ECU_HW_NO_OFST)
#define SHADOW_FBL_FIAT_ECU_HW_VERSION_OFST     (SHADOW_FBL_FIAT_ECU_HW_VERSION_START)
#define SHADOW_FBL_FIAT_ECU_SW_NO_START         (EE_FBL_FIAT_ECU_HW_VERSION_SIZE+SHADOW_FBL_FIAT_ECU_HW_VERSION_OFST)
#define SHADOW_FBL_FIAT_ECU_SW_NO_OFST          (SHADOW_FBL_FIAT_ECU_SW_NO_START)
#define SHADOW_FBL_FIAT_ECU_SW_VERSION_START    (EE_FBL_FIAT_ECU_SW_NO_SIZE+SHADOW_FBL_FIAT_ECU_SW_NO_OFST)
#define SHADOW_FBL_FIAT_ECU_SW_VERSION_OFST     (SHADOW_FBL_FIAT_ECU_SW_VERSION_START)
#define SHADOW_FBL_APPROVAL_NO_START            (EE_FBL_FIAT_ECU_SW_VERSION_SIZE+SHADOW_FBL_FIAT_ECU_SW_VERSION_OFST)
#define SHADOW_FBL_APPROVAL_NO_OFST             (SHADOW_FBL_APPROVAL_NO_START)
#define SHADOW_FBL_ADDRESS_IDENT_START          (EE_FBL_APPROVAL_NO_SIZE+SHADOW_FBL_APPROVAL_NO_OFST)
#define SHADOW_FBL_ADDRESS_IDENT_OFST           (SHADOW_FBL_ADDRESS_IDENT_START)
#define SHADOW_FBL_CHR_APPL_SW_PNO_START        (EE_FBL_ADDRESS_IDENT_SIZE+SHADOW_FBL_ADDRESS_IDENT_OFST)
#define SHADOW_FBL_CHR_APPL_SW_PNO_OFST         (SHADOW_FBL_CHR_APPL_SW_PNO_START)
#define SHADOW_FBL_CHR_DATA_SW_PNO_START        (EE_FBL_CHR_APPL_SW_PNO_SIZE+SHADOW_FBL_CHR_APPL_SW_PNO_OFST)
#define SHADOW_FBL_CHR_DATA_SW_PNO_OFST         (SHADOW_FBL_CHR_DATA_SW_PNO_START)
#define SHADOW_FBL_CHR_ECU_PNO_START            (EE_FBL_CHR_DATA_SW_PNO_SIZE+SHADOW_FBL_CHR_DATA_SW_PNO_OFST)
#define SHADOW_FBL_CHR_ECU_PNO_OFST             (SHADOW_FBL_CHR_ECU_PNO_START)
#define SHADOW_SERIAL_NUMBER_START              (EE_FBL_CHR_ECU_PNO_SIZE+SHADOW_FBL_CHR_ECU_PNO_OFST)
#define SHADOW_SERIAL_NUMBER_OFST               (SHADOW_SERIAL_NUMBER_START)
#define SHADOW_VIN_ORIGINAL_START               (EE_SERIAL_NUMBER_SIZE+SHADOW_SERIAL_NUMBER_OFST)
#define SHADOW_VIN_ORIGINAL_OFST                (SHADOW_VIN_ORIGINAL_START)

#define SHADOW_CXSUM_FAILED_FLAGS_START         (EE_VIN_ORIGINAL_SIZE + SHADOW_VIN_ORIGINAL_OFST)
#define SHADOW_CXSUM_FAILED_FLAGS2_START        (4 + SHADOW_CXSUM_FAILED_FLAGS_START)
#define SHADOW_CXSUM_FAILED_FLAGS3_START        (4 + SHADOW_CXSUM_FAILED_FLAGS2_START)
#define SHADOW_BACKUPS_INITALIZED_START         (EE_CXSUM_FAILED_FLAGS_SIZE + SHADOW_CXSUM_FAILED_FLAGS3_START)
#define SHADOW_VRGN_FLAG_END                    (EE_SECTOR_SIZE + SHADOW_BACKUPS_INITALIZED_START)


/*****************************************************************************/
/*****************************************************************************************************
*   Local Type Definitions
*****************************************************************************************************/

/*****************************************************************************************************
*   Local Function Declarations
*****************************************************************************************************/

/*****************************************************************************************************
*   Global Variable Definitions
*****************************************************************************************************/

/* Default values */
const UINT8 GainDefault[] = {10};
const UInt8 FailedFlagsDefault[EE_CXSUM_FAILED_FLAGS_SIZE] = {0};

/* Eventually, we can have offsets for each location in the EEPROM map generated */
//const SEEBlockTableEntry aEEBlockTable[EE_NUM_BLOCKS] = { {},
const SEEBlockTableEntry aEEBlockTable[] = {
/*----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
/*  sChecksumLoc                                              sControlFlags         eDefaultLoc             wOffset                           wBackupOffset                          wSize                                 wRAMOffset                            */
/*  CxSection                 |BkupCxSection                  |Sleep Refresh |Rsvd |ROM Default             |EE Offset                        |Backup EE Offset                      |Size                                 |Shadow Offset          |Description  */
/*----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/


/*      0                        0                            0                    0                   0                              0xFFFF                                  0                                     0 */
/*00*/  {{EE_CXSUM_NONE,            EE_CXSUM_NONE},                 {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     0x0000,                            EE_NO_BKUP_OFST,                        0x00,                                 0},                     /*0 EE_INVALID */
/*      0                        0                            0                    0                   0                              0xFFFF                                  4                                     0*/
/*01*/  {{EE_CXSUM_NONE,            EE_CXSUM_NONE},                 {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_VRGN_FLAG_START,                EE_NO_BKUP_OFST,                        EE_SECTOR_SIZE,                       0},                     /*1 EE_VRGN_FLAG_START_BLOCK */
/*02*/  {{EE_CXSUM_DATA_BLOCK,      EE_CXSUM_DATA_BKUP_BLOCK},      {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_LOAD_DEFAULTS_OFST,             EE_LOAD_DEFAULTS_BKUP_OFST,             EE_LOAD_DEFAULTS_SIZE,                SHADOW_LOAD_DEFAULTS_OFST},                     
/*03*/  {{EE_CXSUM_DATA_BLOCK,      EE_CXSUM_DATA_BKUP_BLOCK},      {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_EEPROM_VERSION_OFST,            EE_EEPROM_VERSION_BKUP_OFST,            EE_EEPROM_VERSION_SIZE,               SHADOW_EEPROM_VERSION_OFST},                     
/*04*/  {{EE_CXSUM_DATA_BLOCK,      EE_CXSUM_DATA_BKUP_BLOCK},      {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_AUTOSAR_100P_PWM_OFST,          EE_AUTOSAR_100P_PWM_BKUP_OFST,          EE_AUTOSAR_100P_PWM_SIZE,             SHADOW_AUTOSAR_100P_PWM_OFST},
/*05*/  {{EE_CXSUM_DATA_BLOCK,      EE_CXSUM_DATA_BKUP_BLOCK},      {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_HEAT_FAULT_THRESHOLDS_OFST,     EE_HEAT_FAULT_THRESHOLDS_BKUP_OFST,     EE_HEAT_FAULT_THRESHOLDS_SIZE,        SHADOW_HEAT_FAULT_THRESHOLDS_OFST},
/*06*/  {{EE_CXSUM_DATA_BLOCK,      EE_CXSUM_DATA_BKUP_BLOCK},      {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_RELAY_THRESHOLDS_OFST,          EE_RELAY_THRESHOLDS_BKUP_OFST,          EE_RELAY_THRESHOLDS_SIZE,             SHADOW_RELAY_THRESHOLDS_OFST},
/*07*/  {{EE_CXSUM_DATA_BLOCK,      EE_CXSUM_DATA_BKUP_BLOCK},      {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_VENT_PWM_DATA_OFST,             EE_VENT_PWM_DATA_BKUP_OFST,             EE_VENT_PWM_DATA_SIZE,                SHADOW_VENT_PWM_DATA_OFST},
/*08*/  {{EE_CXSUM_DATA_BLOCK,      EE_CXSUM_DATA_BKUP_BLOCK},      {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_VENT_FLT_CAL_DATA_OFST,         EE_VENT_FLT_CAL_DATA_BKUP_OFST,         EE_VENT_FLT_CAL_DATA_SIZE,            SHADOW_VENT_FLT_CAL_DATA_OFST},
/*09*/  {{EE_CXSUM_DATA_BLOCK,      EE_CXSUM_DATA_BKUP_BLOCK},      {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_TEMPERATURE_CALIB_DATA_OFST,    EE_TEMPERATURE_CALIB_DATA_BKUP_OFST,    EE_TEMPERATURE_CALIB_DATA_SIZE,       SHADOW_TEMPERATURE_CALIB_DATA_OFST},
/*0A*/  {{EE_CXSUM_DATA_BLOCK,      EE_CXSUM_DATA_BKUP_BLOCK},      {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_BATTERY_CALIB_DATA_OFST,        EE_BATTERY_CALIB_DATA_BKUP_OFST,        EE_BATTERY_CALIB_DATA_SIZE,           SHADOW_BATTERY_CALIB_DATA_OFST},
/*0B*/  {{EE_CXSUM_DATA_BLOCK,      EE_CXSUM_DATA_BKUP_BLOCK},      {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_BATT_COMPENSATION_OFST,         EE_BATT_COMPENSATION_BKUP_OFST,         EE_BATT_COMPENSATION_SIZE,            SHADOW_BATT_COMPENSATION_OFST},
/*0C*/  {{EE_CXSUM_DATA_BLOCK,      EE_CXSUM_DATA_BKUP_BLOCK},      {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_WHEEL_CAL_DATA_OFST,            EE_WHEEL_CAL_DATA_BKUP_OFST,            EE_WHEEL_CALIB_DATA_SIZE,             SHADOW_WHEEL_CAL_DATA_OFST},

/*0D*/  {{EE_CXSUM_DATA_BLOCK,      EE_CXSUM_DATA_BKUP_BLOCK},      {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_WHEEL_FAULT_THRESHOLDS_OFST,    EE_WHEEL_FAULT_THRESHOLDS_BKUP_OFST,    EE_WHEEL_FAULT_THRESHOLDS_SIZE,       SHADOW_WHEEL_FAULT_THRESHOLDS_OFST},
/*0E*/  {{EE_CXSUM_DATA_BLOCK,      EE_CXSUM_DATA_BKUP_BLOCK},      {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_HEAT_CAL_DATA_PF_OFST,          EE_HEAT_CAL_DATA_PF_BKUP_OFST,          EE_HEAT_CAL_DATA_PF_SIZE,             SHADOW_HEAT_CAL_DATA_PF_OFST},
/*0F*/  {{EE_CXSUM_DATA_BLOCK,      EE_CXSUM_DATA_BKUP_BLOCK},      {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_PLL_FAULT_TIME_LMT_OFST,        EE_PLL_FAULT_TIME_LMT_BKUP_OFST,        EE_PLL_FAULT_TIME_LMT_SIZE,           SHADOW_PLL_FAULT_TIME_LMT_OFST},
/*10*/  {{EE_CXSUM_DATA_BLOCK,      EE_CXSUM_DATA_BKUP_BLOCK},      {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_REMOTE_CTRL_STAT_OFST,          EE_REMOTE_CTRL_STAT_BKUP_OFST,          EE_REMOTE_CTRL_STAT_SIZE,             SHADOW_REMOTE_CTRL_STAT_OFST},
/*11*/  {{EE_CXSUM_DATA_BLOCK,      EE_CXSUM_DATA_BKUP_BLOCK},      {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_REMOTE_CTRL_HEAT_AMBTEMP_OFST,  EE_REMOTE_CTRL_HEAT_AMBTEMP_BKUP_OFST,  EE_REMOTE_CTRL_HEAT_AMBTEMP_SIZE,     SHADOW_REMOTE_CTRL_HEAT_AMBTEMP_OFST},
/*12*/  {{EE_CXSUM_DATA_BLOCK,      EE_CXSUM_DATA_BKUP_BLOCK},      {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_REMOTE_CTRL_VENT_AMBTEMP_OFST,  EE_REMOTE_CTRL_VENT_AMBTEMP_BKUP_OFST,  EE_REMOTE_CTRL_VENT_AMBTEMP_SIZE,     SHADOW_REMOTE_CTRL_VENT_AMBTEMP_OFST},
/*13*/  {{EE_CXSUM_DATA_BLOCK,      EE_CXSUM_DATA_BKUP_BLOCK},      {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_ENG_RPM_LIMIT_OFST,             EE_ENG_RPM_LIMIT_BKUP_OFST,             EE_ENG_RPM_LIMIT_SIZE,                SHADOW_ENG_RPM_LIMIT_OFST},
/*14*/  {{EE_CXSUM_DATA_BLOCK,      EE_CXSUM_DATA_BKUP_BLOCK},      {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_SEC_FAA_OFST,                   EE_SEC_FAA_BKUP_OFST,                   EE_SEC_FAA_SIZE,                      SHADOW_SEC_FAA_OFST},
/*15*/  {{EE_CXSUM_DATA_BLOCK,      EE_CXSUM_DATA_BKUP_BLOCK},      {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_OVER_TEMP_FAILSAFE_OFST,        EE_OVER_TEMP_FAILSAFE_BKUP_OFST,        EE_OVER_TEMP_FAILSAFE_SIZE,           SHADOW_OVER_TEMP_FAILSAFE_OFST},
/*16*/  {{EE_CXSUM_DATA_BLOCK,      EE_CXSUM_DATA_BKUP_BLOCK},      {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_HEAT_NTC_FAILSAFE_OFST,         EE_HEAT_NTC_FAILSAFE_BKUP_OFST,         EE_HEAT_NTC_FAILSAFE_SIZE,            SHADOW_HEAT_NTC_FAILSAFE_OFST},
/*17*/  {{EE_CXSUM_DATA_BLOCK,      EE_CXSUM_DATA_BKUP_BLOCK},      {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_CHRONO_STACK_OFST,              EE_CHRONO_STACK_BKUP_OFST,              EE_CHRONO_STACK_SIZE,                 SHADOW_CHRONO_STACK_OFST},                     
/*19*/  {{EE_CXSUM_DATA_BLOCK,      EE_CXSUM_DATA_BKUP_BLOCK},      {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_INTERNAL_FLT_BYTE0_OFST,        EE_INTERNAL_FLT_BYTE0_BKUP_OFST,        EE_INTERNAL_FLT_SIZE,                 SHADOW_INTERNAL_FLT_BYTE0_OFST},
/*1A*/  {{EE_CXSUM_DATA_BLOCK,      EE_CXSUM_DATA_BKUP_BLOCK},      {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_INTERNAL_FLT_BYTE1_OFST,        EE_INTERNAL_FLT_BYTE1_BKUP_OFST,        EE_INTERNAL_FLT_SIZE,                 SHADOW_INTERNAL_FLT_BYTE1_OFST},
/*1B*/  {{EE_CXSUM_DATA_BLOCK,      EE_CXSUM_DATA_BKUP_BLOCK},      {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_INTERNAL_FLT_BYTE2_OFST,        EE_INTERNAL_FLT_BYTE2_BKUP_OFST,        EE_INTERNAL_FLT_SIZE,                 SHADOW_INTERNAL_FLT_BYTE2_OFST},
/*1C*/  {{EE_CXSUM_DATA_BLOCK,      EE_CXSUM_DATA_BKUP_BLOCK},      {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_INTERNAL_FLT_BYTE3_OFST,        EE_INTERNAL_FLT_BYTE3_BKUP_OFST,        EE_INTERNAL_FLT_SIZE,                 SHADOW_INTERNAL_FLT_BYTE3_OFST},
/*1D*/  {{EE_CXSUM_DATA_BLOCK,      EE_CXSUM_DATA_BKUP_BLOCK},      {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_INTERNAL_FLT_CFG_OFST,          EE_INTERNAL_FLT_CFG_BKUP_OFST,          EE_INTERNAL_FLT_CFG_SIZE,             SHADOW_INTERNAL_FLT_CFG_OFST},
///*1E*/  {{EE_CXSUM_DATA_BLOCK,      EE_CXSUM_DATA_BKUP_BLOCK},      {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_SERIAL_NUMBER_OFST,             EE_SERIAL_NUMBER_BKUP_OFST,             EE_SERIAL_NUMBER_SIZE,                SHADOW_SERIAL_NUMBER_OFST},                     
/*18*/  {{EE_CXSUM_DATA_BLOCK,      EE_CXSUM_DATA_BKUP_BLOCK},      {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_PROXI_CFG_OFST,                 EE_PROXI_CFG_BKUP_OFST,                 EE_PROXI_CFG_SIZE,                    SHADOW_PROXI_CFG_OFST},
/*18*/  {{EE_CXSUM_DATA_BLOCK,      EE_CXSUM_DATA_BKUP_BLOCK},      {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_PROXI_FAILURE_OFST,             EE_PROXI_FAILURE_BKUP_OFST,             EE_PROXI_FAILURE_SIZE,                SHADOW_PROXI_FAILURE_OFST},
/*18*/  {{EE_CXSUM_DATA_BLOCK,      EE_CXSUM_DATA_BKUP_BLOCK},      {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_PROXI_LEARNED_OFST,             EE_PROXI_LEARNED_BKUP_OFST,             EE_PROXI_LEARNED_SIZE,                SHADOW_PROXI_LEARNED_OFST},
/*1F*/  {{EE_CXSUM_DATA_BLOCK,      EE_CXSUM_DATA_BKUP_BLOCK},      {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_RESERVE_DATAS2_OFST,            EE_RESERVE_DATAS2_BKUP_OFST,            EE_RESERVE_DATAS2_SIZE,               SHADOW_RESERVE_DATAS2_OFST},                     

/*20*/  {{EE_CXSUM_DATA_BLOCK,      EE_CXSUM_DATA_BKUP_BLOCK},      {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_DTC_STATUS_OFST,               EE_DTC_STATUS_BKUP_OFST,                 EE_DTC_STATUS_SIZE,                   SHADOW_DTC_STATUS_OFST},                     
/*21*/  {{EE_CXSUM_DATA_BLOCK,      EE_CXSUM_DATA_BKUP_BLOCK},      {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_HISTORICAL_STACK_OFST,         EE_HISTORICAL_STACK_BKUP_OFST,           EE_HISTORICAL_STACK_SIZE,             SHADOW_HISTORICAL_STACK_OFST},                     
///*22*/  {{EE_CXSUM_DATA_BLOCK,      EE_CXSUM_DATA_BKUP_BLOCK},      {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_VIN_ORIGINAL_OFST,             EE_VIN_ORIGINAL_BKUP_OFST,               EE_VIN_ORIGINAL_SIZE,                 SHADOW_VIN_ORIGINAL_OFST},                     
/*23*/  {{EE_CXSUM_DATA_BLOCK,      EE_CXSUM_DATA_BKUP_BLOCK},      {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_CTRL_BYTE_FINAL_OFST,          EE_CTRL_BYTE_FINAL_BKUP_OFST,            EE_CTRL_BYTE_FINAL_SIZE,              SHADOW_CTRL_BYTE_FINAL_OFST},                     
/*24*/  {{EE_CXSUM_DATA_BLOCK,      EE_CXSUM_DATA_BKUP_BLOCK},      {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_BOARD_TEMIC_BOM_NUMBER_OFST,   EE_BOARD_TEMIC_BOM_NUMBER_BKUP_OFST,     EE_BOARD_TEMIC_BOM_NUMBER_SIZE,       SHADOW_BOARD_TEMIC_BOM_NUMBER_OFST},                     
/*25*/  {{EE_CXSUM_DATA_BLOCK,      EE_CXSUM_DATA_BKUP_BLOCK},      {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_ODOMETER_OFST,                 EE_ODOMETER_BKUP_OFST,                   EE_ODOMETER_SIZE,                     SHADOW_ODOMETER_OFST},
/*26*/  {{EE_CXSUM_DATA_BLOCK,      EE_CXSUM_DATA_BKUP_BLOCK},      {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_ODOMETER_LAST_PGM_OFST,        EE_ODOMETER_LAST_PGM_BKUP_OFST,          EE_ODOMETER_LAST_PGM_SIZE,            SHADOW_ODOMETER_LAST_PGM_OFST},
/*27*/  {{EE_CXSUM_DATA_BLOCK,      EE_CXSUM_DATA_BKUP_BLOCK},      {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_ECU_TIMESTAMPS_OFST,           EE_ECU_TIMESTAMPS_BKUP_OFST,             EE_ECU_TIMESTAMPS_SIZE,               SHADOW_ECU_TIMESTAMPS_OFST},
/*28*/  {{EE_CXSUM_DATA_BLOCK,      EE_CXSUM_DATA_BKUP_BLOCK},      {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_ECU_TIME_KEYON_OFST,           EE_ECU_TIME_KEYON_BKUP_OFST,             EE_ECU_TIME_KEYON_SIZE,               SHADOW_ECU_TIME_KEYON_OFST},
/*29*/  {{EE_CXSUM_DATA_BLOCK,      EE_CXSUM_DATA_BKUP_BLOCK},      {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_ECU_KEYON_COUNTER_OFST,        EE_ECU_KEYON_COUNTER_BKUP_OFST,          EE_ECU_KEYON_COUNTER_SIZE,            SHADOW_ECU_KEYON_COUNTER_OFST},
/*2A*/  {{EE_CXSUM_DATA_BLOCK,      EE_CXSUM_DATA_BKUP_BLOCK},      {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_ECU_TIME_FIRSTDTC_OFST,        EE_ECU_TIME_FIRSTDTC_BKUP_OFST,          EE_ECU_TIME_FIRSTDTC_SIZE,            SHADOW_ECU_TIME_FIRSTDTC_OFST},
/*2B*/  {{EE_CXSUM_DATA_BLOCK,      EE_CXSUM_DATA_BKUP_BLOCK},      {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_ECU_TIME_KEYON_FIRSTDTC_OFST,  EE_ECU_TIME_KEYON_FIRSTDTC_BKUP_OFST,    EE_ECU_TIME_KEYON_FIRSTDTC_SIZE,      SHADOW_ECU_TIME_KEYON_FIRSTDTC_OFST},
/*2C*/  {{EE_CXSUM_DATA_BLOCK,      EE_CXSUM_DATA_BKUP_BLOCK},      {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_FIRST_CONF_DTC_OFST,           EE_FIRST_CONF_DTC_BKUP_OFST,             EE_DTC_SIZE,                          SHADOW_FIRST_CONF_DTC_OFST},
/*2D*/  {{EE_CXSUM_DATA_BLOCK,      EE_CXSUM_DATA_BKUP_BLOCK},      {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_RECENT_CONF_DTC_OFST,          EE_RECENT_CONF_DTC_BKUP_OFST,            EE_DTC_SIZE,                          SHADOW_RECENT_CONF_DTC_OFST},
/*2E*/  {{EE_CXSUM_DATA_BLOCK,      EE_CXSUM_DATA_BKUP_BLOCK},      {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_RESERVE_DATAS3_OFST,           EE_RESERVE_DATAS3_BKUP_OFST,             EE_RESERVE_DATAS3_SIZE,               SHADOW_RESERVE_DATAS3_OFST},

/*--*/  {{EE_CXSUM_NONE,            EE_CXSUM_NONE},                 {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_CXSUM_DATA_OFST,               EE_NO_BKUP_OFST,                         EE_CFG_CXSUM_SIZE,                    SHADOW_CXSUM_DATA_OFST},                     
/*--*/  {{EE_CXSUM_NONE,            EE_CXSUM_NONE},                 {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_CXSUM_DATA_BKUP_OFST,          EE_NO_BKUP_OFST,                         EE_CFG_CXSUM_SIZE,                    SHADOW_CXSUM_DATA_BKUP_OFST},                     

/*--*/  {{EE_CXSUM_NONE,            EE_CXSUM_NONE},                 {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_FBL_TOTAL_START,                EE_FBL_TOTAL_BKUP_OFST,                 EE_FBL_TOTAL_SIZE,                    SHADOW_FBL_TOTAL_OFST},
/*--*/  {{EE_CXSUM_NONE,            EE_CXSUM_NONE},                 {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_FBL_PROGREQ_FLAGS_START,        EE_NO_BKUP_OFST,                        EE_FBL_PROGREQ_FLAGS_SIZE,            SHADOW_FBL_PROGREQ_FLAGS_OFST},                     
/*--*/  {{EE_CXSUM_NONE,            EE_CXSUM_NONE},                 {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_FBL_APPL_VALID_FLAG_OFST,      EE_NO_BKUP_OFST,                        EE_FBL_APPL_VALID_FLAG_SIZE,          SHADOW_FBL_APPL_VALID_FLAG_OFST},                     
/*--*/  {{EE_CXSUM_NONE,            EE_CXSUM_NONE},                 {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_FBL_PROG_ATTEMPTS_OFST,        EE_NO_BKUP_OFST,                        EE_FBL_PROG_ATTEMPTS_SIZE,            SHADOW_FBL_PROG_ATTEMPTS_OFST},                     
/*--*/  {{EE_CXSUM_NONE,            EE_CXSUM_NONE},                 {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_FBL_PROG_STATUS_OFST,          EE_NO_BKUP_OFST,                        EE_FBL_PROG_STATUS_SIZE,              SHADOW_FBL_PROG_STATUS_OFST}, 
/*--*/  {{EE_CXSUM_NONE,            EE_CXSUM_NONE},                 {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_FBL_REPROG_REQ_OFST,           EE_NO_BKUP_OFST,                        EE_FBL_REPROG_REQ_SIZE,               SHADOW_FBL_REPROG_REQ_OFST}, 

/*--*/  {{EE_CXSUM_NONE,            EE_CXSUM_NONE},                 {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_FBL_BOOT_SW_IDENT_OFST,        EE_NO_BKUP_OFST,                        EE_FBL_BOOT_SW_IDENT_SIZE,            SHADOW_FBL_BOOT_SW_IDENT_OFST},                     
/*--*/  {{EE_CXSUM_NONE,            EE_CXSUM_NONE},                 {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_FBL_APPL_SW_IDENT_OFST,        EE_NO_BKUP_OFST,                        EE_FBL_APPL_SW_IDENT_SIZE,            SHADOW_FBL_APPL_SW_IDENT_OFST},                     
/*--*/  {{EE_CXSUM_NONE,            EE_CXSUM_NONE},                 {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_FBL_APPL_DATA_IDENT_OFST,      EE_NO_BKUP_OFST,                        EE_FBL_APPL_DATA_IDENT_SIZE,          SHADOW_FBL_APPL_DATA_IDENT_OFST},                     
/*--*/  {{EE_CXSUM_NONE,            EE_CXSUM_NONE},                 {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_FBL_BOOT_SW_FPRINT_OFST,       EE_NO_BKUP_OFST,                        EE_FBL_BOOT_SW_FPRINT_SIZE,           SHADOW_FBL_BOOT_SW_FPRINT_OFST},                     
/*--*/  {{EE_CXSUM_NONE,            EE_CXSUM_NONE},                 {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_FBL_APPL_SW_FPRINT_OFST,       EE_NO_BKUP_OFST,                        EE_FBL_APPL_SW_FPRINT_SIZE,           SHADOW_FBL_APPL_SW_FPRINT_OFST},                     
/*--*/  {{EE_CXSUM_NONE,            EE_CXSUM_NONE},                 {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_FBL_APPL_DATA_FPRINT_OFST,     EE_NO_BKUP_OFST,                        EE_FBL_APPL_DATA_FPRINT_SIZE,         SHADOW_FBL_APPL_DATA_FPRINT_OFST},                     
/*--*/  {{EE_CXSUM_NONE,            EE_CXSUM_NONE},                 {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_FBL_SPARE_PNO_OFST,            EE_NO_BKUP_OFST,                        EE_FBL_SPARE_PNO_SIZE,                SHADOW_FBL_SPARE_PNO_OFST},                     
/*--*/  {{EE_CXSUM_NONE,            EE_CXSUM_NONE},                 {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_FBL_FIAT_ECU_HW_NO_OFST,       EE_NO_BKUP_OFST,                        EE_FBL_FIAT_ECU_HW_NO_SIZE,           SHADOW_FBL_FIAT_ECU_HW_NO_OFST},                     
/*--*/  {{EE_CXSUM_NONE,            EE_CXSUM_NONE},                 {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_FBL_FIAT_ECU_HW_VERSION_OFST,  EE_NO_BKUP_OFST,                        EE_FBL_FIAT_ECU_HW_VERSION_SIZE,      SHADOW_FBL_FIAT_ECU_HW_VERSION_OFST},                     
/*--*/  {{EE_CXSUM_NONE,            EE_CXSUM_NONE},                 {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_FBL_FIAT_ECU_SW_NO_OFST,       EE_NO_BKUP_OFST,                        EE_FBL_FIAT_ECU_SW_NO_SIZE,           SHADOW_FBL_FIAT_ECU_SW_NO_OFST},                     
/*--*/  {{EE_CXSUM_NONE,            EE_CXSUM_NONE},                 {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_FBL_FIAT_ECU_SW_VERSION_OFST,  EE_NO_BKUP_OFST,                        EE_FBL_FIAT_ECU_SW_VERSION_SIZE,      SHADOW_FBL_FIAT_ECU_SW_VERSION_OFST},                     
/*--*/  {{EE_CXSUM_NONE,            EE_CXSUM_NONE},                 {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_FBL_APPROVAL_NO_OFST,          EE_NO_BKUP_OFST,                        EE_FBL_APPROVAL_NO_SIZE,              SHADOW_FBL_APPROVAL_NO_OFST},                     
/*--*/  {{EE_CXSUM_NONE,            EE_CXSUM_NONE},                 {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_FBL_ADDRESS_IDENT_OFST,        EE_NO_BKUP_OFST,                        EE_FBL_ADDRESS_IDENT_SIZE,            SHADOW_FBL_ADDRESS_IDENT_OFST},                     
/*--*/  {{EE_CXSUM_NONE,            EE_CXSUM_NONE},                 {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_FBL_CHR_APPL_SW_PNO_OFST,      EE_NO_BKUP_OFST,                        EE_FBL_CHR_APPL_SW_PNO_SIZE,          SHADOW_FBL_CHR_APPL_SW_PNO_OFST},                     
/*--*/  {{EE_CXSUM_NONE,            EE_CXSUM_NONE},                 {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_FBL_CHR_DATA_SW_PNO_OFST,      EE_NO_BKUP_OFST,                        EE_FBL_CHR_DATA_SW_PNO_SIZE,          SHADOW_FBL_CHR_DATA_SW_PNO_OFST},                     
/*--*/  {{EE_CXSUM_NONE,            EE_CXSUM_NONE},                 {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_FBL_CHR_ECU_PNO_OFST,          EE_NO_BKUP_OFST,                        EE_FBL_CHR_ECU_PNO_SIZE,              SHADOW_FBL_CHR_ECU_PNO_OFST},
/*--*/  {{EE_CXSUM_NONE,            EE_CXSUM_NONE},                 {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_SERIAL_NUMBER_OFST,            EE_NO_BKUP_OFST,                        EE_SERIAL_NUMBER_SIZE,                SHADOW_SERIAL_NUMBER_OFST},
/*--*/  {{EE_CXSUM_NONE,            EE_CXSUM_NONE},                 {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_VIN_ORIGINAL_OFST,             EE_NO_BKUP_OFST,                        EE_VIN_ORIGINAL_SIZE,                 SHADOW_VIN_ORIGINAL_OFST},


/*      0                        0                            0                    6                   0x7700                           0xffff                                 4                                     898 (0382) */
/*--*/  {{EE_CXSUM_NONE,            EE_CXSUM_NONE},                 {EE_SLP_NO_RFSH, 0}, EE_FAILED_DEFAULT, EE_CXSUM_FAILED_FLAGS_START,       EE_NO_BKUP_OFST,                        EE_CXSUM_FAILED_FLAGS_SIZE,           SHADOW_CXSUM_FAILED_FLAGS_START},/*71 EE_CXSUM_FAILED_FLAGS_BLOCK */
/*      0                        0                            0                    6                   0x7800                           0xffff                                 4                                     902 (0386)*/
/*--*/  {{EE_CXSUM_NONE,            EE_CXSUM_NONE},                 {EE_SLP_NO_RFSH, 0}, EE_FAILED_DEFAULT, EE_CXSUM_FAILED_FLAGS2_START,      EE_NO_BKUP_OFST,                        EE_CXSUM_FAILED_FLAGS_SIZE,           SHADOW_CXSUM_FAILED_FLAGS2_START},/*72 EE_CXSUM_FAILED_FLAGS_BLOCK */
/*      0                        0                            0                    6                   0x7900                           0xffff                                 4                                     906 (0x38a)*/
/*--*/  {{EE_CXSUM_NONE,            EE_CXSUM_NONE},                 {EE_SLP_NO_RFSH, 0}, EE_FAILED_DEFAULT, EE_CXSUM_FAILED_FLAGS3_START,      EE_NO_BKUP_OFST,                        EE_CXSUM_FAILED_FLAGS_SIZE,           SHADOW_CXSUM_FAILED_FLAGS3_START},/*73 EE_CXSUM_FAILED_FLAGS_BLOCK */
/*      0                        0                            0                    0                   0x7a00                           0xffff                                 4                                     910 (0x390)*/
/*--*/  {{EE_CXSUM_NONE,            EE_CXSUM_NONE},                 {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_BACKUPS_INITALIZED_START,       EE_NO_BKUP_OFST,                        EE_SECTOR_SIZE,                       SHADOW_BACKUPS_INITALIZED_START},/*74 EE_BACKUPS_INITALIZED_BLOCK */
/*      0                        0                            0                    0                   0x7b00                           0xffff                                 4                                     914 (0x394)*/
/*--*/  {{EE_CXSUM_NONE,            EE_CXSUM_NONE},                 {EE_SLP_NO_RFSH, 0}, EE_NO_DEFAULT,     EE_VRGN_FLAG_END,                  EE_NO_BKUP_OFST,                        EE_SECTOR_SIZE,                       SHADOW_VRGN_FLAG_END}, /*75 EE_VRGN_FLAG_END_BLOCK */

};

//const SEECxsumTableEntry aEECxsumTable[EE_CXSUM_NUM_BLOCKS] = {
const SEECxsumTableEntry aEECxsumTable[] = {
/*index      EEBlockLocation             |wStartOffset               |wEndOffset            |EEBlockStart */
/* 0                           0x000                          0x000             0            */
/* 00 */     {EE_CXSUM_NONE,             0x0000,                     0x0000,                 0x0000},
/* 01 */     {EE_CXSUM_DATA,             EE_DATA_START,              EE_DATA_END,            EE_EEPROM_VERSION},
/* 02 */     {EE_CXSUM_DATA_BKUP,        EE_DATA_BKUP_START,         EE_DATA_BKUP_END,       EE_EEPROM_VERSION},
};


//const SEEDefaultsTableEntry aEEDefaultsTable[EE_NUM_DEAFULTS] = { {},
const SEEDefaultsTableEntry aEEDefaultsTable[] = {
{(UInt8 *)(0x0000)},
/* 0 */
{(UInt8 *)(&GainDefault)},
/* {0} */
{(UInt8 *)(&FailedFlagsDefault)},
};

//const SEECxsumSectionTableEntry aEECxsumSectionTable[EE_CXSUM_NUM_SECTIONS] = {
const SEECxsumSectionTableEntry aEECxsumSectionTable[] = {
{EE_CXSUM_DATA_BLOCK,             EE_CXSUM_DATA_BKUP_BLOCK},
};

//const SEEBlockAddrTableEntry aEEBlockAddrTable[EEADDR_TAB_SIZE] = { {},
const SEEBlockAddrTableEntry aEEBlockAddrTable[] = {
/*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
/*       eeBlockID                             |eeOffset                            |wsize  */
/*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/


/*  00                                0                                    0             */
/*00*/  {EE_INVALID,                           0x0000,                              0x00               },    
/*  01                                 0                                    4             */
/*01*/  {EE_VRGN_FLAG_START_BLOCK,             EE_VRGN_FLAG_START,                  EE_SECTOR_SIZE     }, 
/*02*/  {EE_LOAD_DEFAULTS,                     EE_LOAD_DEFAULTS_OFST,               EE_LOAD_DEFAULTS_SIZE     },
/*03*/  {EE_EEPROM_VERSION,                    EE_EEPROM_VERSION_OFST,              EE_EEPROM_VERSION_SIZE     },    
/*04*/  {EE_AUTOSAR_100P_PWM,                  EE_AUTOSAR_100P_PWM_OFST,            EE_AUTOSAR_100P_PWM_SIZE    },
/*05*/  {EE_HEAT_FAULT_THRESHOLDS,             EE_HEAT_FAULT_THRESHOLDS_OFST,       EE_HEAT_FAULT_THRESHOLDS_SIZE    },
/*06*/  {EE_RELAY_THRESHOLDS,                  EE_RELAY_THRESHOLDS_OFST,            EE_RELAY_THRESHOLDS_SIZE    },    
/*07*/  {EE_VENT_PWM_DATA,                     EE_VENT_PWM_DATA_OFST,               EE_VENT_PWM_DATA_SIZE    },    
/*08*/  {EE_VENT_FLT_CAL_DATA,                 EE_VENT_FLT_CAL_DATA_OFST,           EE_VENT_FLT_CAL_DATA_SIZE    },    
/*09*/  {EE_TEMPERATURE_CALIB_DATA,            EE_TEMPERATURE_CALIB_DATA_OFST,      EE_TEMPERATURE_CALIB_DATA_SIZE    },
/*0A*/  {EE_BATTERY_CALIB_DATA,                EE_BATTERY_CALIB_DATA_OFST,          EE_BATTERY_CALIB_DATA_SIZE    },
/*0B*/  {EE_BATT_COMPENSATION_DATA,            EE_BATT_COMPENSATION_OFST,           EE_BATT_COMPENSATION_SIZE  },  
/*0C*/  {EE_WHEEL_CAL_DATA,                    EE_WHEEL_CAL_DATA_OFST,              EE_WHEEL_CALIB_DATA_SIZE    },

/*0D*/  {EE_WHEEL_FAULT_THRESHOLDS,            EE_WHEEL_FAULT_THRESHOLDS_OFST,      EE_WHEEL_FAULT_THRESHOLDS_SIZE}, 
/*0E*/  {EE_HEAT_CAL_DATA_PF,                  EE_HEAT_CAL_DATA_PF_OFST,           EE_HEAT_CAL_DATA_PF_SIZE}, 
/*0F*/  {EE_PLL_FAULT_TIME_LMT,                EE_PLL_FAULT_TIME_LMT_OFST,          EE_PLL_FAULT_TIME_LMT_SIZE    },
/*10*/  {EE_REMOTE_CTRL_STAT,                  EE_REMOTE_CTRL_STAT_OFST,            EE_REMOTE_CTRL_STAT_SIZE    },
/*11*/  {EE_REMOTE_CTRL_HEAT_AMBTEMP,          EE_REMOTE_CTRL_HEAT_AMBTEMP_OFST,    EE_REMOTE_CTRL_HEAT_AMBTEMP_SIZE    },    
/*12*/  {EE_REMOTE_CTRL_VENT_AMBTEMP,          EE_REMOTE_CTRL_VENT_AMBTEMP_OFST,    EE_REMOTE_CTRL_VENT_AMBTEMP_SIZE    },
/*13*/  {EE_ENG_RPM_LIMIT,                     EE_ENG_RPM_LIMIT_OFST,               EE_ENG_RPM_LIMIT_SIZE    },    
/*14*/  {EE_SEC_FAA,                           EE_SEC_FAA_OFST,                     EE_SEC_FAA_SIZE    },
/*15*/  {EE_OVER_TEMP_FAILSAFE,                EE_OVER_TEMP_FAILSAFE_OFST,          EE_OVER_TEMP_FAILSAFE_SIZE    },
/*16*/  {EE_HEAT_NTC_FAILSAFE,                 EE_HEAT_NTC_FAILSAFE_OFST,           EE_HEAT_NTC_FAILSAFE_SIZE    },
/*17*/  {EE_CHRONO_STACK,                      EE_CHRONO_STACK_OFST,                EE_CHRONO_STACK_SIZE    },    
/*19*/  {EE_INTERNAL_FLT_BYTE0,                EE_INTERNAL_FLT_BYTE0_OFST,          EE_INTERNAL_FLT_SIZE    },
/*1A*/  {EE_INTERNAL_FLT_BYTE1,                EE_INTERNAL_FLT_BYTE1_OFST,          EE_INTERNAL_FLT_SIZE    },
/*1B*/  {EE_INTERNAL_FLT_BYTE2,                EE_INTERNAL_FLT_BYTE2_OFST,          EE_INTERNAL_FLT_SIZE    },
/*1C*/  {EE_INTERNAL_FLT_BYTE3,                EE_INTERNAL_FLT_BYTE3_OFST,          EE_INTERNAL_FLT_SIZE    },
/*1D*/  {EE_INTERNAL_FLT_CFG,                  EE_INTERNAL_FLT_CFG_OFST,            EE_INTERNAL_FLT_CFG_SIZE    },
///*1E*/  {EE_SERIAL_NUMBER,                     EE_SERIAL_NUMBER_OFST,               EE_SERIAL_NUMBER_SIZE},    
/*18*/  {EE_PROXI_CFG,                         EE_PROXI_CFG_OFST,                   EE_PROXI_CFG_SIZE    },
/*18*/  {EE_PROXI_FAILURE,                     EE_PROXI_FAILURE_OFST,               EE_PROXI_FAILURE_SIZE    },
/*18*/  {EE_PROXI_LEARNED,                     EE_PROXI_LEARNED_OFST,               EE_PROXI_LEARNED_SIZE    },
/*1F*/  {EE_RESERVE_DATAS2,                    EE_RESERVE_DATAS2_OFST,              EE_RESERVE_DATAS2_SIZE    },    

/*20*/  {EE_DTC_STATUS,                        EE_DTC_STATUS_OFST,                  EE_DTC_STATUS_SIZE    },    
/*21*/  {EE_HISTORICAL_STACK,                  EE_HISTORICAL_STACK_OFST,            EE_HISTORICAL_STACK_SIZE    },    
///*22*/  {EE_VIN_ORIGINAL,                      EE_VIN_ORIGINAL_OFST,                EE_VIN_ORIGINAL_SIZE},    
/*23*/  {EE_CTRL_BYTE_FINAL,                   EE_CTRL_BYTE_FINAL_OFST,             EE_CTRL_BYTE_FINAL_SIZE}, 
/*24*/  {EE_BOARD_TEMIC_BOM_NUMBER,            EE_BOARD_TEMIC_BOM_NUMBER_OFST,      EE_BOARD_TEMIC_BOM_NUMBER_SIZE},    
/*25*/  {EE_ODOMETER,                          EE_ODOMETER_OFST,                    EE_ODOMETER_SIZE}, 
/*26*/  {EE_ODOMETER_LAST_PGM,                 EE_ODOMETER_LAST_PGM_OFST,           EE_ODOMETER_LAST_PGM_SIZE}, 
/*27*/  {EE_ECU_TIMESTAMPS,                    EE_ECU_TIMESTAMPS_OFST,              EE_ECU_TIMESTAMPS_SIZE}, 
/*28*/  {EE_ECU_TIME_KEYON,                    EE_ECU_TIME_KEYON_OFST,              EE_ECU_TIME_KEYON_SIZE}, 
/*29*/  {EE_ECU_KEYON_COUNTER,                 EE_ECU_KEYON_COUNTER_OFST,           EE_ECU_KEYON_COUNTER_SIZE}, 
/*2A*/  {EE_ECU_TIME_FIRSTDTC,                 EE_ECU_TIME_FIRSTDTC_OFST,           EE_ECU_TIME_FIRSTDTC_SIZE}, 
/*2B*/  {EE_ECU_TIME_KEYON_FIRSTDTC,           EE_ECU_TIME_KEYON_FIRSTDTC_OFST,     EE_ECU_TIME_KEYON_FIRSTDTC_SIZE}, 
/*2C*/  {EE_FIRST_CONF_DTC,                    EE_FIRST_CONF_DTC_OFST,              EE_DTC_SIZE}, 
/*2D*/  {EE_RECENT_CONF_DTC,                   EE_RECENT_CONF_DTC_OFST,             EE_DTC_SIZE}, 
/*2E*/  {EE_RESERVE_DATAS3,                    EE_RESERVE_DATAS3_OFST,              EE_RESERVE_DATAS3_SIZE}, 

/*--*/  {EE_FBL_TOTAL,                         EE_FBL_TOTAL_OFST,                   EE_FBL_TOTAL_SIZE    },
/*--*/  {EE_FBL_PROGREQ_FLAGS,                 EE_FBL_PROGREQ_FLAGS_OFST,           EE_FBL_PROGREQ_FLAGS_SIZE    },    
/*--*/  {EE_FBL_APPL_VALID_FLAG,               EE_FBL_APPL_VALID_FLAG_OFST,         EE_FBL_APPL_VALID_FLAG_SIZE    },
/*--*/  {EE_FBL_PROG_ATTEMPTS,                 EE_FBL_PROG_ATTEMPTS_OFST,           EE_FBL_PROG_ATTEMPTS_SIZE    },    
/*--*/  {EE_FBL_PROG_STATUS,                   EE_FBL_PROG_STATUS_OFST,             EE_FBL_PROG_STATUS_SIZE    },
/*--*/  {EE_FBL_REPROG_REQ,                    EE_FBL_REPROG_REQ_OFST,              EE_FBL_REPROG_REQ_SIZE    },
/*--*/  {EE_FBL_BOOT_SW_IDENT,                 EE_FBL_BOOT_SW_IDENT_OFST,           EE_FBL_BOOT_SW_IDENT_SIZE    },    
/*--*/  {EE_FBL_APPL_SW_IDENT,                 EE_FBL_APPL_SW_IDENT_OFST,           EE_FBL_APPL_SW_IDENT_SIZE    },    
/*--*/  {EE_FBL_APPL_DATA_IDENT,               EE_FBL_APPL_DATA_IDENT_OFST,         EE_FBL_APPL_DATA_IDENT_SIZE    },    
/*--*/  {EE_FBL_BOOT_SW_FPRINT,                EE_FBL_BOOT_SW_FPRINT_OFST,          EE_FBL_BOOT_SW_FPRINT_SIZE    },    
/*--*/  {EE_FBL_APPL_SW_FPRINT,                EE_FBL_APPL_SW_FPRINT_OFST,          EE_FBL_APPL_SW_FPRINT_SIZE    },    
/*--*/  {EE_FBL_APPL_DATA_FPRINT,              EE_FBL_APPL_DATA_FPRINT_OFST,        EE_FBL_APPL_DATA_FPRINT_SIZE    },    
/*--*/  {EE_FBL_SPARE_PNO,                     EE_FBL_SPARE_PNO_OFST,               EE_FBL_SPARE_PNO_SIZE    },
/*--*/  {EE_FBL_FIAT_ECU_HW_NO,                EE_FBL_FIAT_ECU_HW_NO_OFST,          EE_FBL_FIAT_ECU_HW_NO_SIZE    },    
/*--*/  {EE_FBL_FIAT_ECU_HW_VERSION,           EE_FBL_FIAT_ECU_HW_VERSION_OFST,     EE_FBL_FIAT_ECU_HW_VERSION_SIZE    },
/*--*/  {EE_FBL_FIAT_ECU_SW_NO,                EE_FBL_FIAT_ECU_SW_NO_OFST,          EE_FBL_FIAT_ECU_SW_NO_SIZE    },    
/*--*/  {EE_FBL_FIAT_ECU_SW_VERSION,           EE_FBL_FIAT_ECU_SW_VERSION_OFST,     EE_FBL_FIAT_ECU_SW_VERSION_SIZE    },
/*--*/  {EE_FBL_APPROVAL_NO,                   EE_FBL_APPROVAL_NO_OFST,             EE_FBL_APPROVAL_NO_SIZE    },
/*--*/  {EE_FBL_ADDRESS_IDENT,                 EE_FBL_ADDRESS_IDENT_OFST,           EE_FBL_ADDRESS_IDENT_SIZE    },    
/*--*/  {EE_FBL_CHR_APPL_SW_PNO,               EE_FBL_CHR_APPL_SW_PNO_OFST,         EE_FBL_CHR_APPL_SW_PNO_SIZE    },
/*--*/  {EE_FBL_CHR_DATA_SW_PNO,               EE_FBL_CHR_DATA_SW_PNO_OFST,         EE_FBL_CHR_DATA_SW_PNO_SIZE    },
/*--*/  {EE_FBL_CHR_ECU_PNO,                   EE_FBL_CHR_ECU_PNO_OFST,             EE_FBL_CHR_ECU_PNO_SIZE    },
/*--*/  {EE_SERIAL_NUMBER,                     EE_SERIAL_NUMBER_OFST,               EE_SERIAL_NUMBER_SIZE    },
/*--*/  {EE_VIN_ORIGINAL,                      EE_VIN_ORIGINAL_OFST,                EE_VIN_ORIGINAL_SIZE    },


/*--*/  {EE_CXSUM_FAILED_FLAGS_BLOCK,          EE_CXSUM_FAILED_FLAGS_START,         EE_CXSUM_FAILED_FLAGS_SIZE },
/*    72                               0x7800                               4                        */
/*--*/  {EE_CXSUM_FAILED_FLAGS2_BLOCK,         EE_CXSUM_FAILED_FLAGS2_START,        EE_CXSUM_FAILED_FLAGS_SIZE },
/*    73                               0x7900                               4                        */
/*--*/  {EE_CXSUM_FAILED_FLAGS3_BLOCK,         EE_CXSUM_FAILED_FLAGS3_START,        EE_CXSUM_FAILED_FLAGS_SIZE },
/*    74                               0x7a00                               4                        */
/*--*/  {EE_BACKUPS_INITALIZED_BLOCK,          EE_BACKUPS_INITALIZED_START,         EE_SECTOR_SIZE             },
/*    75                               0x7b00                               4                        */
/*--*/  {EE_VRGN_FLAG_END_BLOCK,               EE_VRGN_FLAG_END,                    EE_SECTOR_SIZE             }

};

/*****************************************************************************************************
*   Static Variable Definitions
*****************************************************************************************************/

/*****************************************************************************************************
*   Global and Static Function Definitions
*****************************************************************************************************/



