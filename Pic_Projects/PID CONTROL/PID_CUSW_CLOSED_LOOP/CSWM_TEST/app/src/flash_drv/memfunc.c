/******************************************************************************
*   (c) Copyright Continental AG 2008
*   All Rights Reserved.
*   Continental AG Confidential Proprietary
*   The Copyright symbol does not also indicate public availability
*   or publication.
*
*   Filename:   memfunc.c 
*
*   Created_by: L. Kaplan
*       
*   Date_created: 03/25/2008
*
*   Description: CCP memory access
*
*
    *******************************************************************************
*   Revision History:
*
*   Date          CQ#           Author    
*   MM-DD-YY      oem00000000   Initials   Description of change.
*   -----------   -----------   --------   ------------------------------------
*   03-25-08      oem00023671   LK         EEPROM driver ported from TIPM
******************************************************************************/

/*****************************************************************************************************
*   Include Files
*****************************************************************************************************/
#include "itbm_basapi.h"
#include "cpu.h"

#include "hwio_eeprom.h"
#include "memfunc.h"
#include "memtype.h"
#include "memdef.h"

/*****************************************************************************************************
*   Local Macro Definitions
*****************************************************************************************************/
#define _suspendInterrupts() \
    _asm("xref _MemFuncInterrupt\n"); \
    _asm("xref _RPAGE\n"); \
    _asm("pshd\n"); \
    _asm("tpa\n");                              /* load old CCR to A */ \
    _asm("sei\n");                              /* disable interrupts */ \
    _asm("ldab #page(_MemFuncInterrupt)\n");    /* setup the rpage */ \
    _asm("stab _RPAGE\n");                      /* set the rpage */ \
    _asm("staa _MemFuncInterrupt\n");           /* save old CCR in CCP_Interrupt */ \
    _asm("puld\n");

#define _resumeInterrupts() \
    _asm("psha\n"); \
    _asm("ldaa #page(_MemFuncInterrupt)\n");    /* setup the rpage */ \
    _asm("staa _RPAGE\n");                      /* set the rpage */ \
    _asm("ldaa _MemFuncInterrupt\n");           /* load the old CCR from CCP_Interrupt */ \
    _asm("tap\n");                              /* restore CCR from stack */ \
    _asm("pula\n");

#define MEM_SUSPEND_INTERRUPTS _suspendInterrupts();
#define MEM_RESUME_INTERRUPTS _resumeInterrupts();

#if 0
#define MEM_SERVICE_WDT()  \
            WATCHDOG_KickSoftwareDog(); \
            WATCHDOG_KickHardwareDog();
#else
#define MEM_SERVICE_WDT() 
#endif

#define MEMFUNC_MEM @far

#define NUM_FLASH_PAGES     32
#define SIZE_OF_FLASH_PAGE  0x4000
/*****************************************************************************************************
*   Local Type Definitions
*****************************************************************************************************/
typedef volatile UInt16 * RAM_TO_TEST_PTR;

/*****************************************************************************************************
*   Local Function Declarations
*****************************************************************************************************/

/*****************************************************************************************************
*   Global Variable Definitions
*****************************************************************************************************/

UInt8 MemFuncInterrupt;


/* special container for save_data.  MUST BE WORD ALIGNED */
/* Placed in it's own section at the start of RAM to ensure word alignment */
//#pragma section [savedata]
UInt16 save_data;
//#pragma section ()

/*****************************************************************************************************
*  Static Variable definitions
*****************************************************************************************************/

/*****************************************************************************************************
*   Global and Static Function Definitions
*****************************************************************************************************/

/*****************************************************************************************************
*   Function:    MemSet
*
*   Description: Sets a buffer with all of a byte value.
*
*   Caveats:     only used logical addresses
*
*****************************************************************************************************/
void MEMFUNC_CALL MemSet( unsigned int destination, unsigned int size, unsigned char value )
{
    unsigned int index;
    
    for (index=0; index < size; index++)
    {
        *((unsigned char*)destination) = value;
        destination++; 
    }    
}


/*****************************************************************************************************
*   Function:    GetGPage
*
*   Description: Returns the gpage when given a global address
*
*   Caveats:     none
*
*****************************************************************************************************/
unsigned char MEMFUNC_CALL GetGPage( unsigned long GlobalAddress )
{
    return ((S12XGlobalType*)&GlobalAddress)->gpage;
}

/*****************************************************************************************************
*   Function:    GetPPage
*
*   Description: Returns the ppage when given a global address
*
*   Caveats:     No checks to make sure the value is in the flash range
*
*****************************************************************************************************/
unsigned char MEMFUNC_CALL GetPPage( unsigned long GlobalAddress )
{
    return ((S12XFlashType*)&GlobalAddress)->ppage;
}

/*****************************************************************************************************
*   Function:    GetRPage
*
*   Description: Returns the rpage from a given global address
*
*   Caveats:     No checks to make sure the value is in the ram address
*
*****************************************************************************************************/
unsigned char MEMFUNC_CALL GetRPage( unsigned long GlobalAddress )
{
    return ((S12XRAMType*)&GlobalAddress)->rpage;
}   

/*****************************************************************************************************
*   Function:    GetEPage
*
*   Description: Returns the epage from a given global address
*
*   Caveats:     No checks to make sure the value is in the eeprom address
*
*****************************************************************************************************/
unsigned char MEMFUNC_CALL GetEPage( unsigned long GlobalAddress )
{
    return ((S12XEepromType*)&GlobalAddress)->epage;
}

/*****************************************************************************************************
*   Function:    GetGOffset
*
*   Description: Returns the gpage offset when given a global address
*
*   Caveats:     none
*
*****************************************************************************************************/
unsigned int MEMFUNC_CALL GetGOffset( unsigned long GlobalAddress )
{
    return ((S12XGlobalType*)&GlobalAddress)->localAddr;
}

/*****************************************************************************************************
*   Function:    GetPOffset
*
*   Description: Returns the ppage offset when given a global address
*
*   Caveats:     No checks to make sure the value is in the flash range
*
*****************************************************************************************************/
unsigned int MEMFUNC_CALL GetPOffset( unsigned long GlobalAddress )
{
    return ((S12XFlashType*)&GlobalAddress)->localOffset;
}

/*****************************************************************************************************
*   Function:    GetROffset
*
*   Description: Returns the rpage offset from a given global address
*
*   Caveats:     No checks to make sure the value is in the ram address
*
*****************************************************************************************************/
unsigned int GetROffset( unsigned long GlobalAddress )
{
    return ((S12XRAMType*)&GlobalAddress)->localOffset;
}   

/*****************************************************************************************************
*   Function:    GetEOffset
*
*   Description: Returns the epage offset from a given global address
*
*   Caveats:     No checks to make sure the value is in the eeprom address
*
*****************************************************************************************************/
unsigned int MEMFUNC_CALL GetEOffset( unsigned long GlobalAddress )
{
    return ((S12XEepromType*)&GlobalAddress)->localOffset;
}

/*****************************************************************************************************
*   Function:    UniversalMemCopy
*
*   Description: Copies buffers from any location to any location
*
*   Caveats:     no error checking
*
*****************************************************************************************************/
void MEMFUNC_CALL UniversalMemCopy( unsigned long dest, unsigned long src, unsigned int size )
{
    /* check if the destination is logical or global */
    if ( (dest <= LOGICAL_MEMORY_END) &&
         (src <= LOGICAL_MEMORY_END) )
    {   /* use logical addressing */
        LogicalMemCopy( (unsigned int)dest, (unsigned int)src, size );
    }
    else if ( (dest > LOGICAL_MEMORY_END) &&
            (src > LOGICAL_MEMORY_END) )
    {
        /* global addressing for reading and writing */
        GlobalMemCopy( dest, src, size );
    }
    else if ( (dest < LOGICAL_MEMORY_END) &&
         (src > LOGICAL_MEMORY_END) )
    {
        /* global addressing for reading and local addressing for writing */
        Global2LogicalMemCopy( (unsigned int)dest, src, size );
    }
    else
    {
        /* global addressing for writing and local addressing for reading */
        Logical2GlobalMemCopy( dest, (unsigned int)src, size );
    }
}

/*****************************************************************************************************
*   Function:    LogicalMemCopy
*
*   Description: Copies a block of logically addressed memory.
*
*   Caveats:     no error checking
*
*****************************************************************************************************/
void MEMFUNC_CALL LogicalMemCopy( unsigned int dest, unsigned int src, unsigned int size )
{
    unsigned int byteindex;
    unsigned char * destination;
    unsigned char * source;
    
    destination = (unsigned char*)dest;
    source = (unsigned char*)src;
    
    MEM_SUSPEND_INTERRUPTS
    for (byteindex=0; byteindex<size; byteindex++)
    {
        *destination++ = *source++;
    }
    MEM_RESUME_INTERRUPTS
}
/*****************************************************************************************************
*   Function:    Global2LogicalMemCopy
*
*   Description: Uses the GPAGE to copy to logical address from global map.
*
*   Caveats:     none
*
*****************************************************************************************************/
void MEMFUNC_CALL Global2LogicalMemCopy( unsigned int destination, unsigned long source, unsigned int size )
{
    unsigned int  byteindex;
    unsigned char previousgpage;
    unsigned char srcgpage;

    MEM_SUSPEND_INTERRUPTS
    
    previousgpage = GPAGE;

    /* start copying bytes */
    for (byteindex = 0; byteindex < size; byteindex++)
    {
        srcgpage = ((S12XGlobalType*)(&source))->gpage;
        
        GPAGE = srcgpage;
        /* use the gpage commands */
        *((unsigned char*)destination) = *((@far @gpage unsigned char *)source);
        destination++;
        source++;
    }
    
    /* restore the gpage to it's previous value to not break other users of GPAGE */
    GPAGE = previousgpage;
    
    MEM_RESUME_INTERRUPTS
}

/*****************************************************************************************************
*   Function:    Local2GlobalMemCopy
*
*   Description: Copies block of data from logical address to global address
*
*   Caveats:     none
*
*****************************************************************************************************/
void MEMFUNC_CALL Logical2GlobalMemCopy( unsigned long destination, unsigned int source, unsigned int size )
{
    unsigned int byteindex;
    unsigned char previousgpage;
    unsigned char destgpage;
    
    MEM_SUSPEND_INTERRUPTS
    
    previousgpage = GPAGE;

    /* start copying bytes */
    for (byteindex = 0; byteindex < size; byteindex++)
    {
        destgpage = ((S12XGlobalType*)(&destination))->gpage;
        
        GPAGE = destgpage;
        /* use the gpage commands */
        *((@far @gpage unsigned char*)destination) = *((unsigned char *)source);
        destination++;
        source++;
    }
    
    /* restore the gpage to it's previous value to not break other users of GPAGE */
    GPAGE = previousgpage;
    
    MEM_RESUME_INTERRUPTS

}

/*****************************************************************************************************
*   Function:    GlobalMemCopy
*
*   Description: Uses the GPAGE to copy to and from using global map.  Can be used to copy over multiple
*               pages.
*
*   Caveats:     none
*
*****************************************************************************************************/
void MEMFUNC_CALL GlobalMemCopy( unsigned long destination, unsigned long source, unsigned int size )
{
    unsigned int  byteindex;
    unsigned char previousgpage;
    unsigned char destgpage;
    unsigned char srcgpage;
    unsigned char tempbyte;

    MEM_SUSPEND_INTERRUPTS
    
    previousgpage = GPAGE;

    for (byteindex = 0; byteindex < size; byteindex++)
    {
        srcgpage = ((S12XGlobalType*)(&source))->gpage;
        destgpage = ((S12XGlobalType*)(&destination))->gpage;
        
        GPAGE = srcgpage;
        /* use the gpage commands */
        tempbyte = *((@far @gpage unsigned char *)source++);
        
        GPAGE = destgpage;
        *(@far @gpage unsigned char *) destination++ = tempbyte;
    }
    
    /* restore the gpage to it's previous value to not break other users of GPAGE */
    GPAGE = previousgpage;

    MEM_RESUME_INTERRUPTS
}

/*****************************************************************************************************
*   Function:    GlobalRead16
*
*   Description: Reads 1 16 bit value from the global map.
*
*   Caveats:     none
*
*****************************************************************************************************/
unsigned int MEMFUNC_CALL GlobalRead16( unsigned long longaddr )
{
    unsigned int returnval;
    unsigned char previousgpage;
    unsigned char tempgpage;
    returnval = 0;
    tempgpage = ((S12XGlobalType*)(&longaddr))->gpage;
    
    MEM_SUSPEND_INTERRUPTS
    
    /* store the current GPAGE so we don't break anything */
    previousgpage = GPAGE;
    
    GPAGE = tempgpage;
    returnval = *((@far @gpage unsigned int *)longaddr);
    
    /* restore the gpage to it's previous value to not break other users of GPAGE */
    GPAGE = previousgpage;
    MEM_RESUME_INTERRUPTS
    
    return returnval;
}

/*****************************************************************************************************
*   Function:    GlobalRead8
*
*   Description: Reads 1 8 bit value from the global map.
*
*   Caveats:     none
*
*****************************************************************************************************/
unsigned char MEMFUNC_CALL GlobalRead8( unsigned long longaddr )
{
    unsigned char returnval;
    unsigned char previousgpage;
    unsigned char tempgpage;
    returnval = 0;
    tempgpage = ((S12XGlobalType*)(&longaddr))->gpage;
    
    MEM_SUSPEND_INTERRUPTS
    
    /* store the current GPAGE so we don't break anything */
    previousgpage = GPAGE;
    
    GPAGE = tempgpage;
    returnval = *((@far @gpage unsigned char *)longaddr);
    
    /* restore the gpage to it's previous value to not break other users of GPAGE */
    GPAGE = previousgpage;
    MEM_RESUME_INTERRUPTS
    
    return returnval;
}

/******************************************************************************
* Name         :  FlashMemCheck 
* Called by    :  main() 
* Preconditions:  None
* Parameters   :  None
* Return code  :  ITBM_BOOLEAN
* Description  :  Memory check for all flash memory  
******************************************************************************/
ITBM_BOOLEAN MEMFUNC_CALL FlashMemCheck(void)
{
    UInt32  checksum;
    UInt16  j;
    UInt8   previouspage;
    UInt32  pCurrentByte;
    UInt8   SegmentNr;
    UInt32  SegmentLength;
    UInt32  StoredCRC;
    UInt8   i;
    ITBM_BOOLEAN bReturnVal;
    
    /* check status of EEPROM before trying to read */
    if( EE_IsBusy() == ITBM_TRUE )
    {
        /* we can't do a flash checksum now, return with no error */
        return ITBM_TRUE;
    }

    checksum = 0;
    SegmentNr = (UInt8)GlobalRead16(kEepAddressSegmentNumber); 

    if( SegmentNr < 32 ) /* we can't have more than 32 segments */
    { 
        for(i=0;i<SegmentNr;i++)         /* Loop for all memory segments */
        {
            MEM_SERVICE_WDT();               /* Service Watch Dog */    
            /* Read Segment Starting Address and Length from eeprom */
            UniversalMemCopy((UInt32)&pCurrentByte,(kEepAddressSegmentStartAddr+kEepSizeSegmentInfo*i),kEepSizeSegmentStartAddr);
            UniversalMemCopy((UInt32)&SegmentLength,(kEepAddressSegmentLength+kEepSizeSegmentInfo*i),kEepSizeSegmentLength);

            if(i==0)     /* Strip 4 bytes checksum */
            {
                SegmentLength -= 4;
            }

            MEM_SERVICE_WDT();               /* Service Watch Dog */    
            /* Calculate memory checksum of one segment */                
            for(j=0;j<SegmentLength;j++)
            {
                /* Disable Interrupts and store current gpage value */
                MEM_SUSPEND_INTERRUPTS
                previouspage = GPAGE;         
                GPAGE = GetGPage(pCurrentByte);

                checksum += *((@far @gpage UInt8*)pCurrentByte);
                pCurrentByte++; 

                /* Enable interrupts and restore previous gpage value */
                GPAGE = previouspage;
                MEM_RESUME_INTERRUPTS
            }
            MEM_SERVICE_WDT();               /* Service Watch Dog */    
        }

        /* Read application code CRC from EEPROM */
        UniversalMemCopy((UInt32)&StoredCRC,kEepAddressCRCLocal,kEepSizeCRCLocal);

        bReturnVal = (ITBM_BOOLEAN)(StoredCRC==checksum);
    }
    else
    {
        /* pass that there was no flash checksum error */
        bReturnVal = ITBM_TRUE;
    }
    
    return bReturnVal;   
}

/******************************************************************************
* Name         :  MMRTFlashMemCheck 
* Called by    :  main() 
* Preconditions:  None
* Parameters   :  None
* Return code  :  unsigned long
* Description  :  Memory check for all flash memory for MMRT mode
******************************************************************************/
unsigned long MEMFUNC_CALL MMRTFlashMemCheck(void)
{
    UInt32  checksum;
    UInt16  byPageIndex;
    UInt8   previouspage;
    UInt32  pCurrentByte;
    UInt8   byindex;

    checksum = 0;
    pCurrentByte = FLASH_GLOBAL_START;

    for(byindex=0; byindex<NUM_FLASH_PAGES; byindex++)         /* Loop for all memory segments */
    {
        MEM_SERVICE_WDT();               /* Service Watch Dog */

        /* Calculate memory checksum of one segment */                
        for(byPageIndex=0; byPageIndex<SIZE_OF_FLASH_PAGE; byPageIndex++)
        {
            /* Disable Interrupts and store current gpage value */
            MEM_SUSPEND_INTERRUPTS
            previouspage = GPAGE;         
            GPAGE = GetGPage(pCurrentByte);

            checksum += *((@far @gpage UInt8*)pCurrentByte);
            pCurrentByte++; 

            /* Enable interrupts and restore previous gpage value */
            GPAGE = previouspage;
            MEM_RESUME_INTERRUPTS
        }
        
        MEM_SERVICE_WDT();               /* Service Watch Dog */
    }

    return checksum;
}
/*****************************************************************************
*
* Function: ram_test_word
*
* Description: This routine is used to test the specified RAM block without
*              destroying its original contents.
*              This routine is copied from FSCM project.
*                     
*
* Caveats:
*   This function tests discontinuous RAM blocks word by word
*   This function cannot utilize the stack when setting the test patterns
*
*****************************************************************************/
UInt8 ram_test_word(RAM_TO_TEST_PTR test_addr)
{

#asm
    xref _save_data

    tfr d,y             ;put address in y so it doesn't get blown away   
    
    ldd 0,y             ;save_data = *test_addr;
    std _save_data
    
    ldd #0xAA55         ;*test_addr = 0xAA55;
    staa 0,y            ;write test pattern 1 byte at a time to address under test 
    stab 1,y            
    
    ldaa 0,y            ; test first byte
    cmpa #0xAA
    bne  ram_test_fail  ; signal fail if non zero
    
    ldab 1,y            ; test second byte
    cmpb #0x55
    bne  ram_test_fail  ; signal fail if non zero

    ldd #0x55AA         ;*test_addr = 0x55AA;
    staa 0,y            ;write test pattern 1 byte at a time to address under test 
    stab 1,y             

    ldaa 0,y            ; test first byte
    cmpa #0x55
    bne  ram_test_fail  ; signal fail if non zero
    
    ldab 1,y            ; test second byte
    cmpb #0xAA
    bne  ram_test_fail  ; signal fail if non zero

ram_test_pass:
    ldd _save_data      ;restore data
    std 0,y

    ldab #0             ;set return value in b

    bra ram_test_end

ram_test_fail:
    ldd _save_data      ;restore data
    std 0,y

    ldab #1             ;set return value in b

ram_test_end:
#endasm

}

ITBM_BOOLEAN	RamPageCheck(UInt8  RamPage)
{
    UInt8	RPAGETemp;
    UInt8   Status = 0;
    UInt16  RAMEND;

    volatile    UInt16* pCurrentWord;

    RAMEND = 0x2000;

    pCurrentWord = (UInt16*)0x1000;

    while( ((UInt16)pCurrentWord < RAMEND) && (Status == 0) )
    {
        MEM_SUSPEND_INTERRUPTS
        RPAGETemp = RPAGE;      /* Store RPAGE */
        RPAGE = RamPage;        /* Go to the Ram Page to be tested.*/

        if(ram_test_word(pCurrentWord))
        {
            Status = ram_test_word(pCurrentWord);    
        }
        pCurrentWord++;        

	    RPAGE = RPAGETemp;
        MEM_RESUME_INTERRUPTS
    }

    return ( Status == 0 );

}

/******************************************************************************
* Name         :  RamMemCheck 
* Called by    :  main() 
* Preconditions:  None
* Parameters   :  None
* Return code  :  ITBM_BOOLEAN
* Description  :  Memory check for ram  
******************************************************************************/
ITBM_BOOLEAN MEMFUNC_CALL RamMemCheck(void)
{
    ITBM_BOOLEAN status;
    UInt8   bCurRamPage;

    status = ITBM_TRUE; 

    for(bCurRamPage=LASTRAMPAGE; bCurRamPage>=FIRSTRAMPAGE; bCurRamPage--)
    {
        MEM_SERVICE_WDT();               /* Service Watch Dog */    
        status &=   RamPageCheck(bCurRamPage);     
        MEM_SERVICE_WDT();               /* Service Watch Dog */    
    }
//    MEM_SERVICE_WDT();               /* Service Watch Dog */    

    return  status;

}

