/****************************************************************************
| Project Name: EEPROM Driver
|    File Name: EepIO.c
|
|  Description: MCS12XE EEPROM Driver
|
|-----------------------------------------------------------------------------
|               C O P Y R I G H T
|-----------------------------------------------------------------------------
| Copyright (c) 2004-2007 by Vector Informatik GmbH, all rights reserved.
|
| This software is copyright protected and proprietary 
| to Vector Informatik GmbH. Vector Informatik GmbH 
| grants to you only those rights as set out in the 
| license conditions. All other rights remain with 
| Vector Informatik GmbH.
|
|-----------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-----------------------------------------------------------------------------
| Initials      Name                   Company
| --------      --------------------   ---------------------------------------
| WM            Marco Wierer           Vector Informatik GmbH
|-----------------------------------------------------------------------------
|               R E V I S I O N   H I S T O R Y
|-----------------------------------------------------------------------------
| Date        Version   Author  Description
| ----------  --------  ------  ----------------------------------------------
| 2007-02-16  01.00.00  WM      Implementation
| 2007-04-25  01.01.00  WM      Corrected prescaler calculation
|*****************************************************************************/

/******************************************************************************
*   Revision History:
*
*   Date          CQ#           Author    
*   MM-DD-YY      oem00000000   Initials   Description of change.
*   -----------   -----------   --------   ------------------------------------
*   03-25-08      oem00023689   FS         Change EepromDriver_InitSync() by
*                                          removing ee-emulation and partition
*                                          Add comments in EE_SubmitCommand()
*****************************************************************************/

/* Includes ******************************************************************/
#include "itbm_basapi.h"
#include "cpu.h"
#include "mw_tmrm.h"
#include "hwio_eeprom_direct.h"
#include "mw_eem.h"

/* -- Flags of the Flash configuration Register (FCNFG) ----------- */
#define CCIE      0x80u   /* Command complete INT-Enable            */

/* -- Flags of the Flash Status Register (FSTAT) ------------------ */
#define CCIF       0x80u   /* Command complete int-flag             */
#define FPVIOL     0x10u   /* Protection violation                  */
#define ACCERR     0x20u   /* Access error (command sequence error) */
#define MGBUSY     0x08u   /* Memory controller busy flag           */

/* -- Flags of the Flash Clock Devider Register (FCLKDIV) --------- */
#define FDIVLD    0x80u    /* Flash Clock divider loaded            */

/* -- Flags of the Flash Command (FCMD) --------------------------- */
#define ERASE_VERIFY_ALL_BLOCKS        0x01u
#define ERASE_VERIFY_BLOCK             0x02u
#define ERASE_VERIFY_P_FLASH_SECTION   0x03u
#define READ_ONCE                      0x04u
#define LOAD_DATA_FIELD                0x05u
#define PROGRAM_PFLASH                 0x06u
#define PROGRAM_ONCE                   0x07u
#define ERASE_ALL_BLOCKS               0x08u
#define ERASE_PFLASH_BLOCK             0x09u
#define ERASE_PFLASH_SECTOR            0x0Au
#define UNSECURE_FLASH                 0x0Bu
#define VERIFY_BACKDOOR_ACCESS_KEY     0x0Cu
#define SET_USER_MARGIN_LEVEL          0x0Du
#define SET_FIELD_MARGIN_LEVEL         0x0Eu
#define ENABLE_DFLASH                  0x0Fu
#define ERASE_VERIFY_DFLASH_SECTION    0x10u
#define PROGRAM_DFLASH                 0x11u
#define ERASE_DFLASH_SECTOR            0x12u
#define ENABLE_EEPROM_EMULATION        0x13u
#define DISABLE_EEPROM_EMULATION       0x14u
#define CANCEL_EEPROM_EMULATION        0x15u


# define FCLK_PRESCALER 0x0F


/*****************************************************************************************************
* 	Local Macro Definitions
*****************************************************************************************************/

/* dflash constants */
#define  DFLASH_WORDS_PER_WRITE     4
#define  DFLASH_WORDS_PER_SECTOR    128
#define  DFLASH_BYTES_PER_SECTOR    ((unsigned int)DFLASH_WORDS_PER_SECTOR << 1)
#define EEPROM_LOGICAL_START    0x0800

/* FCCOB commands used */
#define  PROGRAM_D_FLASH_WORDS      0x11
#define  ERASE_D_FLASH_SECTOR       0x12

/* Flags for error checking.  These may be configured as the user requires */
#define EE_CHECK_ADDRESS_VALIDITY       0
#define EE_MAX_RETRY                    3
#define EE_CALL_AT_WRITE_COMPLETION     0

/* Flags for simulating failures or other debugging.  These should NEVER be set in production!! */
#define EE_SIMULATE_MODIFY_FAILURES     0
#define EE_SIMULATE_WRITE_FAILURES      0
#define EE_SIMULATE_ACCERR              0
#define EE_NO_PIPELINING                0
#define EE_ERASE_EEPROM_ON_POWERUP      0



#define Flash_EnableCmdCompleteInterrupt()      (FCNFG |= CCIE)
#define Flash_DisableCmdCompleteInterrupt()     (FCNFG &= ~CCIE)

#define Flash_IsClockDivLoaded()            ((FCLKDIV & FDIVLD) == FDIVLD)
#define Flash_IsCommandComplete()            ((FSTAT & CCIF) == CCIF)
#define Flash_SetCommand()                    (FSTAT = CCIF)

#define Flash_IsProtectionViolation()        ((FSTAT & FPVIOL) == FPVIOL)
#define Flash_IsAccessError()                ((FSTAT & ACCERR) == ACCERR)

#define Flash_ClearProtectionViolation()     (FSTAT |= FPVIOL)
#define Flash_ClearAccessError()             (FSTAT |= ACCERR)

/* macros for accessing paged EEPROM */

#define EE_GetLogicalAddr(addr)     (unsigned char *)(EEPROM_LOGICAL_START + GetEOffset(addr))
#define EE_SetEPAGE(addr)           EPAGE = GetEPage(addr)

/* Given a global address, this macro returns the sector's global address (one sector has 256bytes) */
#define DFlash_GetSectorAddr(addr)         ((unsigned long)(addr & 0xFFFFFF00))

#define EE_SectorAddress(addr)          		( (unsigned int *)(((unsigned int)(*(&addr))) & 0xFF00) )
#define EE_AddressSectorOffset(addr)    		( ((unsigned int)(*(&addr))) & 0x00FF )

#define FBL_DFLASH_MAIN_COPY_START_ADDR   ((unsigned long)0x101f00)

#define NUM_D_FLASH_SECTORS      128

#define TRUE  1
#define FALSE  0

#define IO_E_OK   0

#define FBL_CHECKSUM_SECTOR_OFFSET 0xC3


#define HWIO_PetTheDog()       \
	ARMCOP = 0x55;			   \
	ARMCOP = 0xAA

/*****************************************************************************************************
* 	Local Type Definitions
*****************************************************************************************************/
/* This union provides different representations of a sector. */
typedef union {
    struct {
        unsigned int        wUpperHalf;
        unsigned int        wLowerHalf;
    } sWordWise;

    unsigned long           ulWhole;

    unsigned char           pbyBytes[4];
} UEESectorBuffer;

/* This struct is for bookkeeping during the write process */
typedef struct {
    unsigned long           lAddress;
    unsigned char           *pbyData;
    unsigned char           byLength;
    unsigned char           byBytesWritten;
    unsigned char           byRetryCount;
} SEEPendingWrite;

/* Type for our state variable */
typedef enum {
    EE_IDLE,
    EE_WRITE_SEQUENCE_INITIATED,
    EE_WAIT_FOR_ERASE_COMPLETION,
	EE_WAIT_FOR_WRITE_COMPLETION,
    EE_RETRY
} EEEState;


/* struct used to submit a command to the S12XE Flash state machine */
typedef struct 
{
    unsigned char  bFlashCmd;
    unsigned char  bGlblAddr_Bit22To16;
    unsigned int wGlblAddr_Bit15To0;
    unsigned int wData0;
    unsigned int wData1;
    unsigned int wData2;
    unsigned int wData3;
} SFCCOBBlock; 

/* This union provides different representations of a sector. */
typedef union {
    unsigned int          pbyWords[DFLASH_WORDS_PER_SECTOR];
    unsigned char         pbyBytes[((unsigned int)DFLASH_WORDS_PER_SECTOR << 1)];
} UDFlashDataBuffer;

typedef struct
{
    UEESectorBuffer SectorAddr;
    UDFlashDataBuffer SectorData;
    unsigned char DFlash_buffer_index;
} UDFlashSectorBuffer;


typedef struct
{
    unsigned long localOffset:10;
    unsigned long epage:8;
    unsigned long :14;
} S12XEepromType;

/*****************************************************************************************************
* 	Local Function Declarations
*****************************************************************************************************/
static unsigned char    EE_PrepDataForWrite(void);
static unsigned char    EE_SubmitCommand(void);
static void             EE_ErrorHook                (void);
static void             EE_SetNextStateTo           (EEEState enew_state);
static void             EE_WriteSequenceInitiated   (void);
static void             EE_WaitForWriteCompletion   (void);
static void             EE_Retry                    (void);
static void             EE_WaitForHookCompletion    (void);
static void             EraseAllDFlash              (void);
static void             EE_Burst_Write       (void);
static unsigned char    GetEPage( unsigned long GlobalAddress );
static unsigned int GetEOffset( unsigned long GlobalAddress );
static unsigned char EE_ForcedWrite_Local(unsigned long ee_dest, unsigned char *pbysrc, unsigned char bysize); 
static unsigned char    EE_IsBusy_Local(void);      
static void EE_Handler_Local(void);

static unsigned char  EepromDriver_RReadSync(unsigned char* readBuffer, unsigned char readLength, unsigned long readAddress);
static unsigned char  EepromDriver_RWriteSync(unsigned char* writeBuffer, unsigned char writeLength, unsigned long writeAddress);
static unsigned char VerifyFBLDFlashChecksum(unsigned long dflashSectorStartAddr);
static unsigned char Calc_Checksum(unsigned char * StartAddress, unsigned char Size);
static void EE_Handler_Disable_Interrupt(void);

/* Prototypes ****************************************************************/

static EepRead( void *dest, void *source, unsigned short count );




/*****************************************************************************************************
* 	Static Variable Definitions
*****************************************************************************************************/

/* All of the information regarding the current pending write */
static SEEPendingWrite      sEEPendingWrite;

/*move 256 bytes to page memory */
static UDFlashSectorBuffer  uDFlashSectorBuffer;

/* Current state the EEPROM programming is in */
static EEEState             eEECurrentState;

static SFCCOBBlock          sFCCOBBlock;

/*****************************************************************************/
/* Implementation                                                            */ 
/*****************************************************************************/

/******************************************************************************
* Name         : EepromDriver_InitSync
* Called by    : Application
* Preconditions: None
* Parameters   : None
* Return code  : The return code shows the success of the initialization
* Description  : Initialize the Eeprom algorithm
*******************************************************************************/
UINT8 EepromDriver_InitSync( void* address )
{
   UINT8 i;

#ifdef V_ENABLE_USE_DUMMY_STATEMENT
   /* Address not used, satisfy lint */
   address = address;
#endif

   /* Wait for FTM reset to complete */
   while ((FSTAT & CCIF) == 0x00)
   {
      ;
   }

   /* Initialize eeprom prescaler divider */
   FCLKDIV = (FCLK_PRESCALER & 0x7F);

   /* disable EEE interrupt */
   FERCNFG = 0;

   /* disable flash interrupt and fault detection */
   FCNFG = 0x10;

   if ((FCLKDIV & FDIVLD) != FDIVLD)
   {
      return(~IO_E_OK);
   }
   
   return(IO_E_OK);
}


/******************************************************************************
* Name         :  EepromDriver_RWriteSync
* Called by    :  Application
* Preconditions:  Eeprom driver has to be initialized
* Parameters   :  Data buffer, write length and write address
* Return code  :  Status of Eeprom programming 
* Description  :  Program Eeprom
******************************************************************************/
UINT8 EepromDriver_RWriteSync( unsigned char * writeBuffer, unsigned char writeLength, 
   unsigned long writeAddress )
{
   if (writeLength > 0)
   {
      EE_ForcedWrite_Local(writeAddress, writeBuffer, writeLength);

      /* Wait for FTM to complete any pending write operations */
      while (EE_IsBusy_Local() == TRUE)
      {
         HWIO_PetTheDog();

         EE_Handler_Local();
      }
   }

   return(IO_E_OK);
}



/******************************************************************************
* Name         :  EepromDriver_RReadSync
* Called by    :  Application
* Preconditions:  Eeprom driver has to be initialized
* Parameters   :  Startaddress, data buffer and size of data to be read
* Return code  :  Status of Eeprom read
* Description  :  Read Eeprom memory
******************************************************************************/
UINT8 EepromDriver_RReadSync( unsigned char * readBuffer, unsigned char readLength, 
   unsigned long readAddress )
{
   unsigned char PrevGpage;
   
   if (readLength > 0)
   {
      PrevGpage = GPAGE;
      EepRead((void *)readBuffer, (void *)(readAddress & 0xFFFF), readLength);
      GPAGE = PrevGpage;
   }

   return(IO_E_OK);
}


/******************************************************************************
* Name         :  EepRead
* Called by    :  EepromDriver_RReadSync
* Preconditions:  FCLKDIV to be initialized
* Parameters   :  Destination address, source address, count
* Return code  :  None
* Description  :  Read Eeprom area
******************************************************************************/
static EepRead( void *dest, void *source, unsigned short count )
{
#asm
         tfr d,y                  ; Y -> destination
         ldd 4,sp                 ; D -> count
         beq read_exit            ; Exit if count == 0
         ldx 2,sp                 ; X -> source
         movb #$10, $10           ; Init GPAGE
      read_loop:
         pshd                     ; Save counter
         gldaa   x
         staa    y
         inx
         iny
         puld                     ; Restore counter
         dbne    d,read_loop      ; Count down and loop back
      read_exit:
#endasm
}
                    


/*****************************************************************************************************
*   Function:    EE_Handler_Local
*
*   Description: This is the main EEPROM handler that must be a scheduled task in order to run the 
*                state machine.  The call rate will have an effect on performace.. the faster the 
*                better with diminishing returns, of course.
*
*   Caveats:     
*
*****************************************************************************************************/

void EE_Handler_Local(void)
{
    /* Execute the state machine */
    switch(eEECurrentState)
    {
    case EE_WRITE_SEQUENCE_INITIATED:
        EE_WriteSequenceInitiated();
        break;

    case EE_WAIT_FOR_ERASE_COMPLETION:
        EE_Burst_Write();
        break;

    case EE_WAIT_FOR_WRITE_COMPLETION:
        EE_WaitForWriteCompletion();
        break;

    case EE_RETRY:
        EE_Retry();
        break;

    case EE_IDLE:
        break;

    default:
        break;
    }
}

/*****************************************************************************************************
*   Function:    EE_IsBusy
*
*   Description: Returns true if the driver is not idle.
*
*   Caveats:     EEPROM_memcpy can ONLY be called and direct reads from EE can only be done when 
*                the driver is non-idle.
*
*****************************************************************************************************/
unsigned char EE_IsBusy_Local(void)
{
    unsigned char bystatus;

    if(eEECurrentState == EE_IDLE)
    {
        bystatus = FALSE;
    }
    else
    {
        bystatus = TRUE;
    }

    return bystatus;
}

/*****************************************************************************************************
*   Function:    EE_ForcedWrite
*
*   Description: Given the EE destination address, source address, and size, this function sets up
*                the write to EE by populating the sEEPendingWrite structure and starting the state
*                machine off in the EE_WRITE_SEQUENCE_INITIATED state.
*
*   Caveats:     This function should never be called while the state machine is in any state 
*                other than EE_IDLE.
*
*****************************************************************************************************/
unsigned char EE_ForcedWrite_Local(unsigned long ee_dest, unsigned char *pbysrc, unsigned char bysize)
{
    sEEPendingWrite.lAddress = ee_dest;
    
    /* Store input data address to global pointer */
    sEEPendingWrite.pbyData = pbysrc;

    /* Store requested data size for programming */ 
    sEEPendingWrite.byLength = bysize;

    sEEPendingWrite.byRetryCount = 0;

    /* copy sEEPendingWrite to uDFlashSectorBuffer */
    if (EE_PrepDataForWrite())
    {
       /* Initiate the EEPROM programming */
       EE_SetNextStateTo(EE_WRITE_SEQUENCE_INITIATED);
    }

    return TRUE;
}

/*****************************************************************************************************
*   Function:    EE_SetNextStateTo
*
*   Description: This function changes the current state to the given state.  It provides a single
*                point for all state changes for debugging purposes.
*
*   Caveats:     
*
*****************************************************************************************************/
void EE_SetNextStateTo(EEEState enew_state)
{
    eEECurrentState = enew_state;
}

/*****************************************************************************************************
 *   Function:       EE_SubmitCommand
 *
 *   Description:    This function submits a command to the micro's internal state machine.  The
 *                   currently tested commands are ERASE_D_FLASH_SECTOR and PROGRAM_D_FLASH.  This
 *                   function follows the process outlined in the databook.  It returns TRUE if the
 *                   submission suceeds and FALSE if it fails.
 *
 *   Caveats:        The ACCERR and PVIOL bits are always clear after exiting this function, even if
 *                   there was an access error or protection violation.
 *
 *****************************************************************************************************/

unsigned char EE_SubmitCommand(void)
{
    unsigned char byresult;

    /* clear flag before start DFlash writing */
    if (Flash_IsProtectionViolation())
    {
        Flash_ClearProtectionViolation();
    }
    
    if (Flash_IsAccessError())
    {
       Flash_ClearAccessError();
    }
    /* set command to erase or program dflash */
    FCCOBIX = 0; 
    FCCOB = (((unsigned int)sFCCOBBlock.bFlashCmd) << 8) + sFCCOBBlock.bGlblAddr_Bit22To16;
    
    switch (sFCCOBBlock.bFlashCmd)
    {
        /* 0x12 */
        case ERASE_D_FLASH_SECTOR:
            FCCOBIX = 1;
            FCCOB = sFCCOBBlock.wGlblAddr_Bit15To0;
            break;

        /* 0x11 */
        case PROGRAM_D_FLASH_WORDS:
            FCCOBIX = 1;
            FCCOB = sFCCOBBlock.wGlblAddr_Bit15To0;
            FCCOBIX = 2;
            FCCOB = sFCCOBBlock.wData0;
            FCCOBIX = 3;
            FCCOB = sFCCOBBlock.wData1;
            FCCOBIX = 4;
            FCCOB = sFCCOBBlock.wData2;
            FCCOBIX = 5;
            FCCOB = sFCCOBBlock.wData3;
            break;

        default:  
            break;
    }

    /* FCCOB params are loaded, now start the command */
    Flash_SetCommand();
    
    if (Flash_IsProtectionViolation() || Flash_IsAccessError())
    {
       if (Flash_IsProtectionViolation())
       {
           Flash_ClearProtectionViolation();
       }
        
       if(Flash_IsAccessError())
       {
           Flash_ClearAccessError();
       }
        /* We'll have to retry this command */
        byresult = FALSE;
    }
    else
    {
        /* The command submission was successful */
        byresult = TRUE;
    }

    return byresult;
}


/*****************************************************************************************************
*   Function:    EE_PrepDataForWrite
*
*   Description: This function takes a SEEPendingWrite struct and sets it up for a write operation
*                by fetching the image of the destination EE sector and putting it in pusector_buffer.
*                It then writes in the desired data to that image so that that buffer can be used
*                in EE_SubmitCommand calls.
*
*   Caveats:     Data in the SEEPendingWrite structure is modified.  After each _complete_ sector 
*                write (a modify + write command sequence), if the byLength field is non-zero, 
*                advance the destination address by the byBytesWritten and call this again.
*
*****************************************************************************************************/

unsigned char EE_PrepDataForWrite(void)
{
    unsigned int bycopy_index;
    unsigned int *pbysector_address;
    unsigned char *pbyAddress;
    unsigned char byresult;
    
    if (sEEPendingWrite.byLength != 0)
    {
        /* get the logical address and epage bit from the physical address */
        EE_SetEPAGE(sEEPendingWrite.lAddress);
        pbyAddress = EE_GetLogicalAddr(sEEPendingWrite.lAddress);

        /* Figure out what sector this data belongs in */
        pbysector_address = EE_SectorAddress(pbyAddress);

        /* Copy the whole sector into our buffer */
        for (bycopy_index = 0; bycopy_index < DFLASH_WORDS_PER_SECTOR; bycopy_index++)
        {
           // check if  pbysector_address read correctly!!!
           uDFlashSectorBuffer.SectorData.pbyWords[bycopy_index] = *(unsigned int *)pbysector_address;
           pbysector_address++;
        }

        /* This is a new write, so clear the number of bytes written */
        sEEPendingWrite.byBytesWritten = 0;

        /* Copy the new data on top of our buffer.  We start at where the data
         * points in the sector and continue until we're either at the end of the
         * sector or there's no more data to write 
         */
        for(bycopy_index = EE_AddressSectorOffset(pbyAddress); 
            ((bycopy_index < DFLASH_BYTES_PER_SECTOR) && (sEEPendingWrite.byLength > 0)); bycopy_index++)
        {
            /* Copy from the source to the destination in our image of the sector */
            uDFlashSectorBuffer.SectorData.pbyBytes[bycopy_index] = *(sEEPendingWrite.pbyData);

            /* Increment the address we're reading from so that next time, we'll 
             * point to the next byte in the write that has yet to be copied
             */
            sEEPendingWrite.pbyData++;

            /* We'll need to keep track of this as well because */
            sEEPendingWrite.byBytesWritten++;
            
            /* Decrement the number of bytes to write in the pending write */
            sEEPendingWrite.byLength--;
        }
        
        /* store sector start physical address */
        uDFlashSectorBuffer.SectorAddr.ulWhole = DFlash_GetSectorAddr(sEEPendingWrite.lAddress);

        /* bytes of words written */
        uDFlashSectorBuffer.DFlash_buffer_index = 0;

        byresult = TRUE;
    }
    else
    {
        byresult = FALSE;
    }
    
    return byresult;
}

/*****************************************************************************************************
*   Function:    EE_WriteSequenceInitiated
*
*   Description: This is the state function for EE_WRITE_SEQUENCE_INITIATED. In this state, the write
*                sequence has been initiated by the EE_Write() function. First, a MODIFY command will
*                be issued to erase the sector and rewrite the high word (lower addresses).  Then, if
*                the micro can accept another command, this state will immediately issue the WRITE
*                command for the lower word (higher addresses) and go to EE_WAIT_FOR_WRITE_COMPLETION.  
*                If the micro cannot accept the cmd, we go to EE_WAIT_FOR_MODIFY_COMPLETION.  If 
*                either of these command submissions fail, we go to EE_RETRY.
*
*   Caveats:     
*
*****************************************************************************************************/
void EE_WriteSequenceInitiated(void)
{

    /* flash command complete */
    if (Flash_IsCommandComplete())
    {
        /* update sFCCOBBlock structure, and trigger DFlash erase */
        sFCCOBBlock.bFlashCmd = ERASE_D_FLASH_SECTOR;
        sFCCOBBlock.bGlblAddr_Bit22To16 = uDFlashSectorBuffer.SectorAddr.pbyBytes[1];
        sFCCOBBlock.wGlblAddr_Bit15To0 = uDFlashSectorBuffer.SectorAddr.sWordWise.wLowerHalf;

        /* erase command is issued successfully */
        if (EE_SubmitCommand())
        {

            /* the command is accepted, move to the next state */
            EE_SetNextStateTo(EE_WAIT_FOR_ERASE_COMPLETION);

        }
        else
        {
            /* The command wasn't accepted, we need to retry the whole process */
            EE_SetNextStateTo(EE_RETRY);
        }
    }
}

/*****************************************************************************************************
 *   Function:       EE_Burst_Write
 *
 *   Description:    This is the state function for EE_WAIT_FOR_ERASE_COMPLETION. In this state,
 *                   ERASE command has already been issued, but the micro could not accept another
 *                   cmd in its pipeline.  We must wait for the ERASE to complete and then submit the
 *                   WRITE command to write the upper word of the sector (lower addresses).  
 *
 *                   First, the WRITE command for the higher word will be issued. Then, if the micro 
 *                   can accept another command, this state will immediately issue the WRITE command 
 *                   for the lower word and go to EE_WAIT_FOR_WRITE_COMPLETION..  
 *                   If the micro cannot accept the cmd, we go to EE_WAIT_FOR_WORD1_COMPLETION.  
 *                   If either of these command submissions fail, we	go to EE_RETRY.
 *
 *   Caveats:        Called by interrupt service routine
 *
 *****************************************************************************************************/

void EE_Burst_Write (void)
{
    /* previous eeprom command is complete */
    if (Flash_IsCommandComplete())
    {
    
        /* update sFCCOBBlock structure, and trigger DFlash erase */
        sFCCOBBlock.bFlashCmd = PROGRAM_D_FLASH_WORDS;
        sFCCOBBlock.bGlblAddr_Bit22To16 = uDFlashSectorBuffer.SectorAddr.pbyBytes[1];

        /* update data */
        if (uDFlashSectorBuffer.DFlash_buffer_index <= (DFLASH_WORDS_PER_SECTOR - DFLASH_WORDS_PER_WRITE))
        {
            /*--------------------------
            issue eeprom write command 
            --------------------------*/
            sFCCOBBlock.wGlblAddr_Bit15To0 = uDFlashSectorBuffer.SectorAddr.sWordWise.wLowerHalf + (uDFlashSectorBuffer.DFlash_buffer_index << 1);
            sFCCOBBlock.wData0 = uDFlashSectorBuffer.SectorData.pbyWords[uDFlashSectorBuffer.DFlash_buffer_index];
            uDFlashSectorBuffer.DFlash_buffer_index++;
            sFCCOBBlock.wData1 = uDFlashSectorBuffer.SectorData.pbyWords[uDFlashSectorBuffer.DFlash_buffer_index] ;
            uDFlashSectorBuffer.DFlash_buffer_index++;
            sFCCOBBlock.wData2 = uDFlashSectorBuffer.SectorData.pbyWords[uDFlashSectorBuffer.DFlash_buffer_index] ;
            uDFlashSectorBuffer.DFlash_buffer_index++;
            sFCCOBBlock.wData3 = uDFlashSectorBuffer.SectorData.pbyWords[uDFlashSectorBuffer.DFlash_buffer_index] ;
            uDFlashSectorBuffer.DFlash_buffer_index++;

            /*--------------------------
            transition to next state 
            ----------------------------*/

            /* eeprom program error occured */
            if (!EE_SubmitCommand())
            {

                /* The command wasn't accepted, we need to retry the whole process */
                uDFlashSectorBuffer.DFlash_buffer_index = 0;

                EE_SetNextStateTo(EE_RETRY);
            }
        }

        /* finish writing the sector */
        else
        {
            /* handover to EE_Handler() */
            EE_SetNextStateTo(EE_WAIT_FOR_WRITE_COMPLETION);
        }
    }
}

/*****************************************************************************************************
*   Function:    EE_WaitForWriteCompletion
*
*   Description: This is the state function for EE_WAIT_FOR_WRITE_COMPLETION. In this state, the 
*                WRITE command has been issued and we are waiting for it to complete.  When it does
*                finish writing, we'll verify the contents of the EE sector against the sector 
*                buffer.  If they're the same, the entire write sequence was successful and we 
*                need to see if there is any other data to write.  If so, advance the destination
*                pointer by the number of bytes that were just written and prepare for the next
*                write sequence.  If there is no more data to write, go to EE_IDLE.
*
*   Caveats:     
*
*****************************************************************************************************/
void EE_WaitForWriteCompletion(void)
{
    unsigned int *pbysector_address;
    unsigned char *pbyAddress;

    unsigned int bycopy_index;
    unsigned char endloop;

    if(Flash_IsCommandComplete())
    {
    
        /* get the logical address and epage bit from the physical address */
        EE_SetEPAGE(sEEPendingWrite.lAddress);
        pbyAddress = EE_GetLogicalAddr(sEEPendingWrite.lAddress);

        /* Figure out what sector this data belongs in */
        pbysector_address = EE_SectorAddress(pbyAddress);
        
        /* go through each word and verify all words were written correctly */
        endloop = FALSE;
        for (uDFlashSectorBuffer.DFlash_buffer_index = 0; 
             ((uDFlashSectorBuffer.DFlash_buffer_index < DFLASH_WORDS_PER_SECTOR) && (endloop == FALSE)); 
             (uDFlashSectorBuffer.DFlash_buffer_index++))
        {
           if (uDFlashSectorBuffer.SectorData.pbyWords[uDFlashSectorBuffer.DFlash_buffer_index] != *(unsigned int *)pbysector_address)
           {
              endloop = TRUE;
           }
           pbysector_address++;
        }


        if (endloop == TRUE)
        {
            /* The contents are different, we need to retry this block. */
            EE_SetNextStateTo(EE_RETRY);
        }
        else
        {
            if(sEEPendingWrite.byLength != 0)
            {
                /* We're going to continue writing at the address of the next sector */
                sEEPendingWrite.lAddress += sEEPendingWrite.byBytesWritten;

                /* update uDFlashSectorBuffer with sEEPendingWrite */
                if (EE_PrepDataForWrite())
                {
                   EE_SetNextStateTo(EE_WRITE_SEQUENCE_INITIATED);
                }
            }
            else
            {
                EE_SetNextStateTo(EE_IDLE);
            }
        }
    }
}

/*****************************************************************************************************
*   Function:    EE_Retry
*
*   Description: This is the state function for EE_RETRY.  We clean up the ACCERR and PVIOL flags
*                and then wait for any command that is pending (just in case the error happened
*                when pipelining a command after one was already in progress).  Then we clear the
*                flags again and do our retry strategy.
*
*   Caveats:     
*
*****************************************************************************************************/
void EE_Retry(void)
{
    /* What caused an operation to fail probably set one of these flags.  Clear them. */
    if (Flash_IsProtectionViolation())
    {
       Flash_ClearProtectionViolation();
    }
    
    if (Flash_IsAccessError())
    {
       Flash_ClearAccessError();
    }
    
    /* wait for command to complete */
    if(Flash_IsCommandComplete())
    {
        /* We don't know if the flags may have been set again as a result of the command 
        * we just waited for.  So clear the flags again. 
        */
        if (Flash_IsProtectionViolation())
        {
            Flash_ClearProtectionViolation();
        }

        if (Flash_IsAccessError())
        {
            Flash_ClearAccessError();
        }

        if(sEEPendingWrite.byRetryCount < EE_MAX_RETRY)
        {
            /* Increment our retry counter and go back to reinitiate the write sequence */
            sEEPendingWrite.byRetryCount++;

            EE_ErrorHook();

            EE_SetNextStateTo(EE_WRITE_SEQUENCE_INITIATED);
        }
        else
        {
            /* This write has been attempted too many times, give up */
            EE_SetNextStateTo(EE_IDLE);

            EE_ErrorHook();
        }
    }
}

/*****************************************************************************************************
*   Function: EE_ErrorHook
*
*   Description: This function is called everytime something erroneous happens.  This could be when
*                a command submission fails or a modify or write fails.  It provides a single point
*                where these errors can be trapped for debugging (esp with the BDM).
*
*   Caveats:
*
*****************************************************************************************************/
void EE_ErrorHook(void)
{
    _asm("nop");
}

/*****************************************************************************************************
*   Function:    GetEPage
*
*   Description: Returns the epage from a given global address
*
*   Caveats:     No checks to make sure the value is in the eeprom address
*
*****************************************************************************************************/
unsigned char GetEPage( unsigned long GlobalAddress )
{
    return ((S12XEepromType*)&GlobalAddress)->epage;
}

/*****************************************************************************************************
*   Function:    GetEOffset
*
*   Description: Returns the epage offset from a given global address
*
*   Caveats:     No checks to make sure the value is in the eeprom address
*
*****************************************************************************************************/
unsigned int GetEOffset( unsigned long GlobalAddress )
{
    return ((S12XEepromType*)&GlobalAddress)->localOffset;
}

/*****************************************************************************************************
 *   Function:       WriteFBLDFlashByte()
 *
 *   Description:  write one byte to FBL DFlash location 
 *
 *	 caveat:    eeprom must be idle !!! 
 *			 
 *****************************************************************************************************/

void WriteFBLDFlashByte(unsigned long mainCopyDFlashAddr, unsigned char* dataToWrite,unsigned char TotalSize)
{

    unsigned char buffer_tmp[FBL_CHECKSUM_SECTOR_OFFSET+1];
    unsigned char Epage_tmp;
    unsigned char *pbyAddress;
    unsigned char *StartAddress;
    unsigned char index_tmp;
    unsigned char total_new = 0; //Harsha added
    Epage_tmp = EPAGE;

    /* set EPAGE */
    EE_SetEPAGE(mainCopyDFlashAddr);
    /* get logic address */
    pbyAddress = EE_GetLogicalAddr(mainCopyDFlashAddr);
    /* get sector start logic address */
    StartAddress = EE_GetLogicalAddr(DFlash_GetSectorAddr(mainCopyDFlashAddr));

    /* init checksum */
    buffer_tmp[FBL_CHECKSUM_SECTOR_OFFSET] = 0;
    
    for (index_tmp = 0; index_tmp < FBL_CHECKSUM_SECTOR_OFFSET; index_tmp++)
    {
        if (pbyAddress != StartAddress)
        { 
           buffer_tmp[index_tmp] = *(unsigned char *)StartAddress;
        }
        else
        {
          //buffer_tmp[index_tmp] = dataToWrite; //Harsha commented 
        	buffer_tmp[index_tmp] = *(unsigned char *)dataToWrite; //Harsha added 
          if((TotalSize > 1) && (total_new < TotalSize))
          {
        	  pbyAddress++; 
        	  dataToWrite++;
        	  total_new++;
          }
        }
        
        /* calculate checksum. ok for overflow */                                                      
        buffer_tmp[FBL_CHECKSUM_SECTOR_OFFSET] += buffer_tmp[index_tmp];

        StartAddress++;
    }

    EPAGE = Epage_tmp;
    
    EE_BlockWrite(EE_FBL_TOTAL, (unsigned char *)&buffer_tmp[0]);

    /* write main copy Harsha commented*/
    //EepromDriver_RWriteSync(buffer_tmp, (FBL_CHECKSUM_SECTOR_OFFSET+1), DFlash_GetSectorAddr(mainCopyDFlashAddr));
    /* write backup copy Harsha commented*/
    //EepromDriver_RWriteSync(buffer_tmp, (FBL_CHECKSUM_SECTOR_OFFSET+1), (DFlash_GetSectorAddr(mainCopyDFlashAddr) - FBL_DFLASH_BACKUP_OFFSET));
}

/*****************************************************************************************************
 *   Function:       ReadFBLDFlash()
 *
 *   Description:  read one byte to FBL DFlash location 
 *
 *	 caveat:     
 *			 
 *****************************************************************************************************/

unsigned char ReadFBLDFlash(unsigned long dflashMainCopyAddr, unsigned char* readData, unsigned char size)
{

    unsigned char return_value;
    unsigned char GoodFBLDFlashChecksum;
     
    return_value = TRUE;

    /* check primary copy checksum */
    GoodFBLDFlashChecksum = VerifyFBLDFlashChecksum(FBL_DFLASH_MAIN_COPY_START_ADDR);
    
    /* good checksum */
    if (GoodFBLDFlashChecksum == TRUE)
    {
       /* read from DFlash */
       EepromDriver_RReadSync(readData, size, dflashMainCopyAddr);
    }
    /* the primary copy has bad checksum */
    else
    {
       /* check backup checksum */
       GoodFBLDFlashChecksum = VerifyFBLDFlashChecksum((FBL_DFLASH_MAIN_COPY_START_ADDR - FBL_DFLASH_BACKUP_OFFSET));

       if  (GoodFBLDFlashChecksum == TRUE)
       {
          /* read from DFlash */
          EepromDriver_RReadSync(readData, size, (dflashMainCopyAddr - FBL_DFLASH_BACKUP_OFFSET));
       } 
       /* wrong checksum */
       else
       {
          /* both copies have wrong checksum.  Should never coming here */
          return_value = FALSE;
       }
    }

    return return_value;
}


/*****************************************************************************************************
 *   Function:       VerifyFBLDFlashChecksum()
 *
 *   Description:  Calculate checksum for fbl DFlash block 
 *
 *	 caveat:       called when eeprom is idle!!!
 *			 
 *****************************************************************************************************/

unsigned char VerifyFBLDFlashChecksum(unsigned long dflashSectorStartAddr)
{
    unsigned char Epage_tmp;
    unsigned char StoredChecksum_tmp;
    unsigned char CalcChecksum_tmp;
    unsigned char *pbyAddress;

    Epage_tmp = EPAGE;

    /* get the logical address and epage bit from the physical address */
    EE_SetEPAGE(dflashSectorStartAddr);
    pbyAddress = EE_GetLogicalAddr(dflashSectorStartAddr);

    /* calculate checksum */
    CalcChecksum_tmp = Calc_Checksum(pbyAddress, FBL_CHECKSUM_SECTOR_OFFSET);

    StoredChecksum_tmp = *(unsigned char *)(pbyAddress + FBL_CHECKSUM_SECTOR_OFFSET);

    EPAGE = Epage_tmp;

    return (CalcChecksum_tmp == StoredChecksum_tmp);
}

/*****************************************************************************************************
 *   Function:       Calc_Checksum()
 *
 *   Description:  Calculate checksum 
 *
 *	 caveat:     
 *			 
 *****************************************************************************************************/

unsigned char Calc_Checksum(unsigned char * StartAddress, unsigned char Size)
{
    unsigned char checksum_tmp;
    unsigned char index_tmp;

    checksum_tmp = *(unsigned char *)StartAddress;
    for (index_tmp = 1; index_tmp < Size; index_tmp++)
    {
        /* ok for overflow*/
        checksum_tmp += *(unsigned char *)(StartAddress + index_tmp);
    }

    return checksum_tmp;
}

