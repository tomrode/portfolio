#ifndef MW_CFGM_H
#define MW_CFGM_H
/******************************************************************************
*   (c) Copyright Continental AG 2007
*   All Rights Reserved.
*   Continental AG Confidential Proprietary
*   The Copyright symbol does not also indicate public availability
*   or publication.
*
*   Filename:   mw_cfgm.h 
*
*   Created_by: M. Gorshkova
*       
*   Date_created: 10/8/2007
*
*   Description: Header file for Configuration database API 
*
*
*******************************************************************************
*   Revision History:
*
*   Date          CQ#           Author    
*   MM-DD-YY      oem00000000   Initials   Description of change.
*   -----------   -----------   --------   ------------------------------------
*   10-08-07      oem00023000   MG         Initial version
*   11-07-07      oem00023080   MG         correction after code review
*   11-30-07      oem00023225   MG         parameter names (port, pin) changed
*   12-11-07      oem00023254   AN         Header files usage is optimized
*   12-14-07      oem00023253   AN         Code is distributed between pages
*   12-28-07      oem00023225   MG         Corrected after code review
*   02-19-08      oem00023530   MG         Init function added to initialize all signals 
*                                          using init value from DB
*   02-27-08      oem00023543   AN         Param and DTC ranges are added
*   04-04-08      oem00023695   LP         Added new DTC parameter - DTC_PRIORITY, DTC_Maturetime, 
*                                          DTC_WarningInd
*   04-07-08      oem00023695   LP         Added new DTC parameter - DTC_EventFlg
*   04-18-08      oem00023695   LP         Added self healing criteria
*   04-22-08      oem00023695   LP         Review changes from FTR00110826
*   04-30-08      oem00023887   LP         To add new parameter DTC_DeMaturetime
*   06-06-08      oem00024039   LP         New parameter DTC_Readiness and renamed DTC_EventFlg to DTC_OccFault
*   06-12-08      oem00024039   LP         New parameter for enabling/disabling each DTC.
******************************************************************************/

/******************************************************************************
*   Include Files
******************************************************************************/ 
#include "itbm_basapi.h"
#include "mw_cfgm_defs.h"               /* common definition for signals */
#include "mw_cfgm_hwo_cfg.h"            /* HWO types header file*/
#include "mw_cfgm_hwi_cfg.h"            /* HWI types header file*/
#include "mw_cfgm_appd_cfg.h"           /* APPD types header file*/
#include "mw_cfgm_param_cfg.h"          /* param types header file*/
#include "mw_cfgm_dtc_cfg.h"            /* DTC types header file*/
#include "hwio_display.h"

/******************************************************************************
*   Macro Definitions  
******************************************************************************/

/*****************************************************************************
 *  Local Macro Definitions
 *****************************************************************************/

/******************************************************************************
*   Type Definitions
******************************************************************************/
/* IDs of Application Data constant parameters */
typedef enum {
    /* target to data receive\transmit */
    Device,
    /* Port definition  */
    param1,
    /* Channel/bit number of port  */
    param2,
    /* Debounce strategy (analog/digital/none)*/
    Dbnc,
    /*DTC Setting for specific DTC - Enabled or Disabled*/
    DTC_Enable,
    /* identifier */
    DTC_ID,
    /* DTC priority */
    DTC_PRIORITY,
    /*DTC Readiness Indicator supported flag*/
    DTC_Readiness, 
    /*DTC Mature time*/
    DTC_Maturetime,
    /*DTC Demature time*/
    DTC_DeMaturetime,
    /*DTC Occurrence/Fault flag*/
    DTC_OccFault, 
    /*DTC Warning indicator requested*/
    DTC_WarningInd,
    /*DTC Limp in action*/
    DTC_LimpInAction,
    /*Self Healing criteria*/
    DTC_SelfHealing,
    /* Input/Output/Application Data total size in bytes */
    Data_Size,
    /* Initial value of Input/Output/Application Data */
    Init_Value,
    /* Active input/output value */
    Active_Low,
    /* Flag to indicate that the data are stored in configuration table and ready to output */
    isReceived,
    /* Current error code of input Application Data */
    Error_Code
} eITBM_Param_ID;

/******************************************************************************
*   External Variables
******************************************************************************/

/******************************************************************************
*   Global Function Prototypes  
******************************************************************************/
Status_Type MEM_FAR ITBM_cfgmGetData(AppDataIDType ID, AccessRef pValue, UINT8 size);
Status_Type MEM_FAR ITBM_cfgmSetData(AppDataIDType ID, ConstAccessRef pValue, UINT8 size);
Status_Type MEM_FAR ITBM_GetParam (AppDataIDType ID, const eITBM_Param_ID ParamID, AccessRef pValue, UINT8 size);
Status_Type MEM_FAR ITBM_SetParam(AppDataIDType ID, const eITBM_Param_ID ParamID, ConstAccessRef pValue, UINT8 size);
void MEM_FAR ITBM_cfgmInit(void);

#endif /* MW_CFGM_H */
