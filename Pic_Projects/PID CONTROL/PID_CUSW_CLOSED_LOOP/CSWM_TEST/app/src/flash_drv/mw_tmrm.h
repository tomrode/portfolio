#ifndef TMR_MANAGER_H
#define TMR_MANAGER_H
/******************************************************************************
*   (c) Copyright Continental AG 2007
*   All Rights Reserved.
*   Continental AG Confidential Proprietary
*   The Copyright symbol does not also indicate public availability
*   or publication.
*
*   Filename:   mw_tmrm.c 
*
*   Created_by: V.Shvetsov
*       
*   Date_created: 10/26/2007
*
*   Description:  
*
*
*******************************************************************************
*   Revision History:
*
*   Date          CQ#           Author    
*   MM-DD-YY      oem00000000   Initials   Description of change.
*   -----------   -----------   --------   ------------------------------------
*   10-26-07      oem00023081   VS         Initial version. Reused from Palladian.
*   12-11-07      oem00023254   AN         Header files usage is optimized
*   12-14-07      oem00023253   AN         Code is distributed between pages
*   04-11-08      oem00023689   FS         Add HWIO_PetTheDog() micro
******************************************************************************/

/*******************************************************************************************************
*   Include Files
*******************************************************************************************************/
#include "itbm_basapi.h" 
#include "mw_tmrm_cfg.h"

/*******************************************************************************************************
*   Macro Definitions
*******************************************************************************************************/

/*******************************************************************************************************
*   External Variables
*******************************************************************************************************/

/*******************************************************************************************************
*   Types Definitions
*******************************************************************************************************/
typedef UINT32 Timer32;

/*******************************************************************************************************
*   Global Function Prototypes
*******************************************************************************************************/
void MEM_FAR ITBM_tmrmPowerupInit(void);
void MEM_FAR ITBM_tmrmMain(void);
ITBM_BOOLEAN MEM_FAR ITBM_tmrmSetTimer(TimerID, Timer32);
void MEM_FAR ITBM_tmrmDisableTimer(TimerID);
ITBM_BOOLEAN MEM_FAR ITBM_tmrmExpired(TimerID);
ITBM_BOOLEAN MEM_FAR ITBM_tmrmIsDisabled(TimerID);
ITBM_BOOLEAN MEM_FAR ITBM_tmrmIsRunning(TimerID);
Timer32 MEM_FAR ITBM_tmrmGetRemaining(TimerID);

#endif
