
/* Ino.H,C,6.2,6.0,0,4A8AC237! */
/* Ino.000C0 */

/******************************************************************************
*   (c) Copyright Continental AG 2008
*   All Rights Reserved.
*   Continental AG Confidential Proprietary
*   The Copyright symbol does not also indicate public availability
*   or publication.
*
*   Filename:   mw_eem.h 
*
*   Created_by: L. Kaplan
*       
*   Date_created: 03/25/2008
*
*   Description: Header for EEPROM manager
*
*
    *******************************************************************************
*   Revision History:
*
*   Date          CQ#           Author    
*   MM-DD-YY      oem00000000   Initials   Description of change.
*   -----------   -----------   --------   ------------------------------------
*   03-25-08      oem00023671   LK         EEPROM driver ported from TIPM
*   04-22-08      oem00023840   AN         Some fixes are made to make it work
*   04-28-08      oem00023876   LK         DTC bitmasks storing to EEPROM added
*   06-05-08      oem00023678   AN         Flag of intended reset is added
*   06-07-08      oem00023975   DT         Flag of illegal opcode added
*   06-16-08      oem00024125   DT         Relay status flag added
*   06-30-08      oem00024189   DT         $22 diag service-related updates
*   07-08-08      oem00024224   AN         ETC services 0xD60x are added
*   06-26-08      oem00024187   LK         EEPROM changes
*   07-21-08      oem00024249   LK         EEPROM backup sections
*   08-??-08      --            CK         CSWM_MY11 project. Modified function declerations.
*   09-29-08      --            CK         EEPROM changes regarding to internal faults
*   10-30-08      --            CK         EEPROM changes regarding to battery compensation
*   01-08-09      --            CK         EEPROM changes regarding to steering wheel low side off delay
*   01-22-09      --            CK         PC-Lint fixes.
*   04-08-09      28078         CK         EEPROM updates regarding to Seat heaters and STW current calculation
*   04-14-09      --            HY         EEP Updates/ GAIN Removed and added ROE_CFG 
*                                       -  Added ECU_CFG variable for Service parts
*   05-01-2009                  HY         Adjusted the EEPROM structure to have all new entries at end
*   08-21-2009   MKS CR32456    CC         To add different thresholds to be different with Front and Rear seats
*   02-17-2010   MKS 47356      HY         New variables added to have last known Load shed 2/4 odo stamps
*   04-14-2010   MKS 50039      HY         Set of 136 variables (17 each for 8 differnt vehicle lines )added for new feature.
*                                          Previously used 17 parameters (All vehicle lines uses same calibration) commented and made that space as reserved
*                MKS 50111      HY         Moved 9 Individual Heater Thresholds to detect Faults into 1 Block
*                                          Moved 6 Individual Relay Thresholds to 1 single Block. 
*                MKS 50168      HY         hs_ntc_failsafe_s (5 byte info Block created for NTC shut OFF read)
*   06-11-2010   MKS 52372      HY         hs_fault_thresholds_c changed from 9 parameters to 7 parameters
*                                          future_reserve_data increased from 11 to 13 bytes
*                                          structure hs_calib_info updated with Unique Partial OPEN thresholds across vehicle lines
*   10.05-2010   MKS 57313      HY         struct heat_info size reduced to 6 from 8.
*                                          New parameter added to get seat or leather configuration (EE_SEAT_CFG) and
*                                          reduced future reserve data to 12 bytes.
******************************************************************************/
/******************************************************************************
 * 									CUSW UPDATES
 *  04.13.2011   MKS 69496      HY         Add new set of EEPROM blocks for the Boot loader support.
 *  06-02-2011   MKS 74315		HY			Adjusted EEPROM Table to fit full 3 pages data. 
 *  08-05-2011   MKS 81845		HY			EEP Map adjusted. Now ECU Serial Number and VIN located
 * 											in Boot area, so APPL and BOOT both can share the information. 
 * 				 MKS 81838		HY			PROXI Information also added.	 
******************************************************************************/

#include "itbm_basapi.h"
#include "Typedef.h"
#include "DiagWrite.h"


#ifndef MW_EEM_H
#define MW_EEM_H

/*****************************************************************************************************
*   Local Macro Definitions
*****************************************************************************************************/
#define EE_SHADOW_SIZE          (0x1000)
#define EE_PAGE_SIZE            (0x400)

/* The EE Manager calls will be far */
#define EE_MANAGER_CALL         MEM_FAR

/* defines the number of VTA jump blocks we support 
   old value is 64 and total write is 64*10,000 */
#define NUM_OF_VTA_JUMP_BLOCKS  13
#define SIZE_OF_VTA_DATA        5

#define EE_NUM_BYTES_IN_CXSUM_FLAGS_ARRAY   ((unsigned char)(EE_CXSUM_NUM_BLOCKS / 8) + 1)

/* we are going to use a voting strategy for these flags */
#define EE_NUM_FAILED_FLAG_COPIES           3

#define SHADOW_BACKUP_START_OFFSET         1000

/* Have one dirty bit for each sector 
   primary: abyEEDirtyBits[0] ~ abyEEDirtyBits[EE_NUM_BYTES_IN_PRIMARY_DIRTY_ARRAY-1]
   backup: abyEEDirtyBits[EE_NUM_BYTES_IN_PRIMARY_DIRTY_ARRAY] ~ abyEEDirtyBits[EE_NUM_BYTES_IN_DIRTY_ARRAY-1] */
#define EE_NUM_BYTES_IN_PRIMARY_DIRTY_ARRAY         ((unsigned char)(EE_NUM_BLOCKS / 8) + 1)
#define EE_NUM_BYTES_IN_DIRTY_ARRAY         (EE_NUM_BYTES_IN_PRIMARY_DIRTY_ARRAY << 1)

/* primary ID: 1 to (EE_NUM_PRIMARY_IDS - 1)  
   backup ID: EE_NUM_PRIMARY_IDS to (2 * EE_NUM_PRIMARY_IDS - 1)  */
#define EE_NUM_PRIMARY_IDS                    (EE_NUM_BYTES_IN_PRIMARY_DIRTY_ARRAY << 3)


/*****************************************************************************************************
*   Local Type Definitions
*****************************************************************************************************/
typedef enum {
/*00*/    EE_INVALID,

/*01*/    EE_FIRST_LOCATION = 1,
/*01*/    EE_VRGN_FLAG_START_BLOCK = EE_FIRST_LOCATION,
/* PAGE 1 */
EE_LOAD_DEFAULTS,
EE_EEPROM_VERSION,
EE_AUTOSAR_100P_PWM,
EE_HEAT_FAULT_THRESHOLDS, 
EE_RELAY_THRESHOLDS, 
EE_VENT_PWM_DATA,
EE_VENT_FLT_CAL_DATA,
EE_TEMPERATURE_CALIB_DATA,
EE_BATTERY_CALIB_DATA,
EE_BATT_COMPENSATION_DATA,
EE_WHEEL_CAL_DATA,

/* PAGE 2 */
EE_WHEEL_FAULT_THRESHOLDS,
EE_HEAT_CAL_DATA_PF,
EE_PLL_FAULT_TIME_LMT,
EE_REMOTE_CTRL_STAT,
EE_REMOTE_CTRL_HEAT_AMBTEMP,
EE_REMOTE_CTRL_VENT_AMBTEMP,
EE_ENG_RPM_LIMIT,
EE_SEC_FAA, 
EE_OVER_TEMP_FAILSAFE,
EE_HEAT_NTC_FAILSAFE,                 //New addition MKS 50168
EE_CHRONO_STACK,
EE_INTERNAL_FLT_BYTE0,
EE_INTERNAL_FLT_BYTE1,
EE_INTERNAL_FLT_BYTE2,
EE_INTERNAL_FLT_BYTE3,
EE_INTERNAL_FLT_CFG,
//EE_SERIAL_NUMBER,
EE_PROXI_CFG,							//MKS 81838 PROXI
EE_PROXI_FAILURE,						//MKS 81838 PROXI
EE_PROXI_LEARNED,						//MKS 81838 PROXI
EE_RESERVE_DATAS2,

/* PAGE 3 */
EE_DTC_STATUS,                        
EE_HISTORICAL_STACK,
//EE_VIN_ORIGINAL,
EE_CTRL_BYTE_FINAL,
EE_BOARD_TEMIC_BOM_NUMBER,
EE_ODOMETER,             		//CUSW: 3 Byte: ODO METER Value
EE_ODOMETER_LAST_PGM,    		//CUSW: 3 Byte: Copy EE_ODOMETER after succesful Reflash
EE_ECU_TIMESTAMPS,       		//CUSW: 4 Byte: Total Power Supply Time of the ECU (Incremented by 1min resolution)
EE_ECU_TIME_KEYON,       		//CUSW: 2 Byte: Total Power Supply of ECU from KEY ON (Incremented by 15sec resolution)
EE_ECU_KEYON_COUNTER,    		//CUSW: 2 Byte: Each IGN Cycle counter (Each Key ON recoginsed by system)
EE_ECU_TIME_FIRSTDTC,    		//CUSW: 4 Byte: Copy EE_ECU_TIMESTAMPS when First DTC detected. CLear No DTCs in memory
EE_ECU_TIME_KEYON_FIRSTDTC, 	//CUSW: 2 Byte: Copy EE_ECU_TIME_KEYON when First DTC detected. Clear No DTCs in Memory.
EE_FIRST_CONF_DTC,              //CUSW: 3 Byte: First confirmed DTC
EE_RECENT_CONF_DTC,             //CUSW: 3 Byte: Recent confirmed DTC. 
EE_RESERVE_DATAS3,

EE_CXSUM_DATA,
EE_CXSUM_DATA_BKUP,
/* BOOT */
EE_FBL_TOTAL,
EE_FBL_PROGREQ_FLAGS,
EE_FBL_APPL_VALID_FLAG,
EE_FBL_PROG_ATTEMPTS,
EE_FBL_PROG_STATUS,
EE_FBL_REPROG_REQ,				// sets flag to know application Started after reflash
EE_FBL_BOOT_SW_IDENT,
EE_FBL_APPL_SW_IDENT,
EE_FBL_APPL_DATA_IDENT,
EE_FBL_BOOT_SW_FPRINT,
EE_FBL_APPL_SW_FPRINT,
EE_FBL_APPL_DATA_FPRINT,
EE_FBL_SPARE_PNO,
EE_FBL_FIAT_ECU_HW_NO,
EE_FBL_FIAT_ECU_HW_VERSION,
EE_FBL_FIAT_ECU_SW_NO,
EE_FBL_FIAT_ECU_SW_VERSION,
EE_FBL_APPROVAL_NO,
EE_FBL_ADDRESS_IDENT,
EE_FBL_CHR_APPL_SW_PNO,
EE_FBL_CHR_DATA_SW_PNO,
EE_FBL_CHR_ECU_PNO,
EE_SERIAL_NUMBER,					//MKS 81845: Moved from Appl section to Boot, this info is required in Boot also.
EE_VIN_ORIGINAL,					//MKS 81845: Moved from Appl section to Boot, this info is required in Boot also.

/*08*/    EE_CXSUM_FAILED_FLAGS_BLOCK,
/*09*/    EE_CXSUM_FAILED_FLAGS2_BLOCK,
/*0A*/    EE_CXSUM_FAILED_FLAGS3_BLOCK,
/*0B*/    EE_BACKUPS_INITALIZED_BLOCK,

/*0C*/    EE_VRGN_FLAG_END_BLOCK,

/*0C*/    EE_LAST_BLOCK = EE_VRGN_FLAG_END_BLOCK,
/*0D*/    EE_NUM_BLOCKS
} EEBlockLocation;

typedef enum { 
/*00*/     EE_CXSUM_NONE,
/*01*/     EE_CXSUM_FIRST_LOCATION = 1, 
/*01*/     EE_CXSUM_DATA_BLOCK = EE_CXSUM_FIRST_LOCATION,
/*02*/     EE_CXSUM_DATA_BKUP_BLOCK,
/*--*/     EE_CXSUM_NUM_BLOCKS
} EEECxsumBlock;

typedef enum {
/*00*/    EE_NO_DEFAULT,
/*01*/    EE_FIRST_DEFAULT = 1,
/*01*/    EE_GAIN_DEFAULT = EE_FIRST_DEFAULT,
          EE_FAILED_DEFAULT,
/*02*/    EE_NUM_DEAFULTS
} EEDefaultLocation;  

typedef struct {
    u_8Bit /*UInt8*/               byJumpBlockInit;
    u_8Bit /*UInt8*/               LId74_data[SIZE_OF_VTA_DATA];
    u_16Bit /*UInt16*/              wJumpBlockWriteCntr;
} VTABlockWriteData_t;


typedef enum {
/*00*/    EE_CXSUM_DATA_SECTION,
/*--*/    EE_CXSUM_NUM_SECTIONS
} EECxsumSection_t;

typedef struct {
    EEECxsumBlock       byChecksumBlock       :4;
    EEECxsumBlock       byBkupChecksumBlock   :4;
} SEEChecksumLoc;

typedef struct {
    u_8Bit               bSleepRefresh         :1;
    u_8Bit               bReserved0            :7;
} SEEControlFlags;

typedef struct {
    SEEChecksumLoc      sChecksumLoc;
    SEEControlFlags     sControlFlags;
    EEDefaultLocation   eDefaultLoc;
    u_16Bit              wOffset;
    u_16Bit              wBackupOffset;
    u_16Bit              wSize;
    u_16Bit              wRAMOffset;
} SEEBlockTableEntry;

typedef struct {
    EEBlockLocation     eCxsumBlockLoc;
    u_16Bit              wStartOffset;
    u_16Bit              wEndOffset;
    EEBlockLocation     EEBlockStart;
} SEECxsumTableEntry;

typedef struct {
    u_8Bit*              pRomAddr;
} SEEDefaultsTableEntry;

typedef struct {
    EEECxsumBlock       eCxsumPrimaryBlock;
    EEECxsumBlock       eCxsumBackupBlock;
} SEECxsumSectionTableEntry;

typedef struct {
    unsigned char eeCxsumIsValid        :1;
    unsigned char eeManagerBusy         :1;
    unsigned char eeInitialized         :1;
    unsigned char eeBkupInitializeReq   :1;
    unsigned char eeWriteChecksumFlag   :1;
    unsigned char reserved0             :3;
} SEEStatusFlags_t;

typedef struct {
    EEBlockLocation     sEEBlockLoc;
    u_16Bit              wOffset;
    u_16Bit              wSize;
} SEEBlockAddrTableEntry;


/*Harsha Added */
struct vsdata_s 
{
    unsigned char PWM_a[3];        // Pwm duty cycles (3 bytes)
};

struct vs_info_data 
{
	struct vsdata_s vs_info_data_s[6]; 
};

struct vsflt_s
{
    unsigned char Flt_Dtc[4];      // Fault detection thresholds (4 bytes)
    unsigned short start_up_tm;    // Motor start up time in seconds (2 bytes)
    unsigned char vs_2sec_mature_time;   // Regular Fault detection time (except open.)
    unsigned char vs_10sec_mature_time;  //For vents Open (Tracker Id#43)
};
struct Temperature_data_s
{
        unsigned char HS_NTC_Lw_Thrshld;         // Low threshold used to set a NTC fault short to ground
        unsigned char HS_NTC_Hgh_Thrshld;        // High threshold used to set a NTC fault open or short to battery
        unsigned char NTC_Supply_V_Low_Thrshld;  // NTC Supply Voltage Low Threshold
        unsigned char NTCOpen_OutputOntime_lmt;  // Output ontime before detecting NTC open at ambient < -10 C (in min)
        unsigned short PCB_Set_OverTemp;         // Threshold used to set an PCB over temperature fault
        unsigned short PCB_Clear_OverTemp;       // Threshold used to clear an PCB over temperature fault
};

struct Battery_data_s 
{
        unsigned char Thrshld_a[5];            // Voltage thresholds (5 bytes)
        unsigned char Timing_a[3];             // Voltage timings (3 bytes)
};

struct overtemp_data_s
{
	  unsigned short pcb_odo_stamp_first_w;  //ODO stamp when first time PCB over Temp occurs.
	  unsigned short pcb_odo_stamp_latest_w; //ODO stamp last time PCB over Temp occurs. 
	  unsigned char pcb_overtemp_counter_c;  // No of times PCB Over temp condition happened
	  unsigned char pcb_overtemp_status_c;   //Present PCB Over Temp Status
};

struct stW_data_s
{
	   unsigned char TimeOut_c[5];          // Timeout for the stWheel heater (5 bytes)
	   unsigned char st_Pwm_a[5];           // Pwm duty cycles (5 bytes)
	   unsigned char WL1Temp_Threshld;      // Maximum Temperature thresholds for WL1 (1 byte)
	   unsigned char WL2Temp_Threshld;      // Maximum Temperature thresholds for WL2 (1 byte)
	   unsigned char WL3Temp_Threshld;      // Maximum Temperature thresholds for WL3 (1 byte)  
	   unsigned char WL4Temp_Threshld;      // Maximum Temperature thresholds for WL4 (1 byte)  
	   unsigned char MaxTemp_Threshld;      // Maximum Temperature thresholds for stWheel (1 byte) 
	   unsigned char WL1TempChck_Time;      // Additional timer to automatically update wheel level 1 in min (1 bytes) 
	   unsigned char WL2TempChck_Time;      // Additional timer to automatically update wheel level in min (1 bytes) 
	   unsigned char WL3TempChck_Time;      // Additional timers to automatically update wheel level in min (1 bytes)
	   unsigned char WL4TempChck_Time;      // Additional timer to automatically update wheel level 4 in min (1 bytes)
};

struct stW_data_info
{
	struct  stW_data_s stW_data_info_s[10]; 
};

struct hs_calib_info
{
	unsigned char pwm_c[4];
	unsigned char max_timeout_c[4];
	unsigned char NTC_cutoff_c[5];
	unsigned char partial_open_c; 
};

struct heat_info
{
	struct hs_calib_info heat_info_s[6]; //Each vehicle line consists of 6 sets.
};

//MKS 50168
struct hs_ntc_failsafe_s
{
	unsigned short failsafe_odo_stamp_first_w;
	unsigned short failsafe_odo_stamp_latest_w;
	unsigned char failure_counter_c;
};

//ALL DTCs Status Information (Total of 46 DTCs * 2)
struct AllDtcStatus
{
 unsigned char Dtcstat[92];
};

struct dtc_info
{
 unsigned char freq_counter;              /* Frequency counter */
 unsigned char op_cycle_counter;          /* Operation Cycle Counter */
 unsigned char dtc_RomIndex;              /* DTC Index */
 unsigned char ecu_lifetime_l[4];         // ECU Time stamps (Life time in 1min increments)
 unsigned short ecutime_keyon_w;          //ECU Time since KEY ON in this IGN cycle (in 15sec increments)
 unsigned short keyon_counter_w;          // No of IGN Cycles

};

struct ChronoInfo
{
struct dtc_info dtc_info_s[10]; //10 Records of Chrono stack
};

struct history_info
{
 unsigned char    hisecu_lifetime_l[4];        // ECU Time stamps (Life time in 1min increments)
 unsigned short   hisecu_time_keyon_w;      //ECU Time since KEY ON in this IGN cycle (in 15sec increments)
 unsigned short   hiskey_on_counter_w;      // No of IGN Cycles
 unsigned char    hisdtc_RomIndex;        // DTC code index im ROM
};

struct HistoricalInfo
{
 struct history_info history_info_s[10];  //10 records of Snap shot 2nd data
};

//struct ecu_cfg_s
//{
//	  unsigned char model_year_c;            //MY Information
//	  unsigned char vehicle_line_c;          //Vehicle line information (CT/DS)
//	  unsigned char ecu_cfg_c;               //Known ECU configuration  
//	  unsigned char seat_cfg_c;              //Known SEAT configuration
//	  unsigned char wheel_cfg_c;             //Known Wheel Type (Leather/Wood)
//};

typedef struct 
{
  /* APPL PAGE 1 */	
  unsigned char load_defaults_c;                //Load application defaults (0x55- load defaults, 0xAA-defaults loaded)
  unsigned short eeprom_version_w;              //Eeprom Version
  unsigned short autosar_100pwm_w;              //Default 100% PWM for Autosar values
  unsigned char hs_fault_thresholds_c[7];       //Fault thresholds for heaters, mature,demature times
  unsigned char rl_thresholds_c[6];             //Relay Delay Thresholds (Above 6 moved into 1 block)
  struct vs_info_data;                             // Vent PWM parameters (3 bytes)
  struct vsflt_s;              //Vent calib Parameters 8 Bytes
  struct Temperature_data_s;   // Temperature calibration parameters (8 bytes)
  struct Battery_data_s ;           // Battery calibration parameters (8 bytes)
  unsigned char batt_compensation_c[6];                  // For battery voltage compensation while the outputs are on
  struct stW_data_info;                              // StWheel calibration parameters (19 bytes)

  /* APPL PAGE 2 */	
  unsigned char stw_fault_thresholds_c[6];          //Wheel Fault thresholds.
  struct heat_info;                                //Heated Seat Calibration Parameters for different vehicle lines
  unsigned char PLL_fault_tm_lmt_c;                    // PLL unlock fault set DTC time limit
  unsigned char RemCntrl_stat_c;                        // Stored Remote Start status. Enabled (0x55) / Disabled (0xAA)
  unsigned char remote_heat_ambtemp_c;         //Ambient temp to start heating in remote start (4.4c)
  unsigned char remote_vent_ambtemp_c;         //Ambient temp to start vent in remote start (26.7c)
  unsigned char EngRpm_Low_Limit_c;                   // Engine RPM low limit that disables CSWM outputs
  unsigned char security_faa_c;          //False Access Attempt
  struct overtemp_data_s;                //Over temperature Data
  struct hs_ntc_failsafe_s;             //Heater NTC FailSafe Failure Info for Diagnostic Read
  struct ChronoInfo;                     //CHrono stack
  unsigned char internal_flt_byte0_c;                 //internal fault byte 0
  unsigned char internal_flt_byte1_c;                 //internal fault byte 1
  unsigned char internal_flt_byte2_c;                 //internal fault byte 2
  unsigned char internal_flt_byte3_c;                 //internal fault byte 3
  unsigned char internal_flt_config_c;                 //Enable/Disable high priority fault detection
//  unsigned char ecu_serial_no[15];                 //Temic/ECU Serial Number
  U_PROXI_DATA ru_def_eep_map;
  S_REJECTION_FEEDBACK_REC def_Feedback_Tbl[PROXI_MAX_NUM_ERRORS];
  unsigned char proxi_learned;
  unsigned char future_reserve_data_s2_c[13];          //Reserve data for future use Segment 2
  
  /* APPL PAGE 3 */
  struct AllDtcStatus;                   //DTC Status Information 
  struct HistoricalInfo;                 //Historical Stack
//  unsigned char vin_numb_orig_c[17];              //Original VIN
  unsigned char ctrl_byte_final_c;
  unsigned char temic_BOM_No_c[15];            //Temic BOM Number
  unsigned char odometer_c[3];                     //Odometer  CUSW
  unsigned char odometer_last_pgm_c[3];            //Odometer last pgm CUSW 
  unsigned char ecu_timestamps_l[4];			   // ECU time stamps CUSW
  unsigned short ecu_timekeyon_w;			      // ECU time keyon CUSW
  unsigned short ecu_keyon_counter_w;		      // ECU  keyon counter CUSW
  unsigned char ecu_time_firstdtc_l[4];		  // ECU time keyon CUSW
  unsigned short ecu_timekeyon_firstdtc_w;		  // ECU  keyon counter CUSW
  unsigned char first_conf_dtc_index;
  unsigned char recent_conf_dtc_index;
  unsigned char future_reserve_data_s3_c[36];          //Reserve data for future use Segment 3

  /* APPL PAGE 4 */
  
  /* BOOT PAGE */
//  unsigned char bootsw_ident_c[13];                //Boot SW Identification.
//  unsigned char applsw_ident_c[13];                //Appl SW Identification.
//  unsigned char appldata_ident_c[13];              //Appl Data Identification.
//  unsigned char fiat_spare_pno_c[11];				//FIAT SPARE PART NUMBER
  unsigned char fiat_ecu_hw_number_c[11];			//FIAT ECU HW Part NUMBER
  unsigned char fiat_ecu_hw_version_c;			   //FIAT ECU HW Version
  unsigned char fiat_ecu_sw_number_c[11];			//FIAT ECU SW Part NUMBER
//  unsigned char fiat_ecu_sw_version_c[2];			//FIAT ECU SW Version
//  unsigned char fiat_approval_no_c[6];             //FIAT approval number
//  unsigned char fiat_address_ident_c[5];           //FIAT Address Ident
//  unsigned char chr_sw_part_number[10];            //Chrysler SW Part Number
//  unsigned char chr_ecu_part_number[10];           //Chrysler ECU Part Number 

}SEEPelements_t; 

/*****************************************************************************************************
*   Local Function Declarations
*****************************************************************************************************/
@far void /*EE_MANAGER_CALL*/ EE_PowerupInit(void);
@far void /*EE_MANAGER_CALL*/ EE_Manager(void);
@far SEEStatusFlags_t /*EE_MANAGER_CALL*/ EE_GetStatusFlags(void);
@far u_8Bit /*EE_MANAGER_CALL*/ EE_ManagerIsBusy(void);
@far void /*EE_MANAGER_CALL*/ EE_BlockWrite(EEBlockLocation elocation, u_8Bit *pbysrc);
@far void /*EE_MANAGER_CALL*/ EE_BlockRead(EEBlockLocation elocation, u_8Bit *pbydest);
@far void /*EE_MANAGER_CALL*/ EE_FarBlockWrite(EEBlockLocation elocation, @far u_8Bit *pbysrc);
@far void /*EE_MANAGER_CALL*/ EE_FarBlockRead(EEBlockLocation elocation, @far u_8Bit *pbydest);

@far void /*EE_MANAGER_CALL*/ EE_LoadDefaults(void);
//Harsha changed eedestaddr size from u_32bit to u_16bit
@far u_8Bit /*EE_MANAGER_CALL*/ EE_DirectWrite(u_32Bit eedestaddr, u_8Bit *pbysrc, u_8Bit bysize); 
@far void /*EE_MANAGER_CALL*/ EE_SID31_FACallback(void);
@far u_16Bit /*EE_MANAGER_CALL*/ EE_ValidateCxsum(EEECxsumBlock eCxsumBlock);

@far ITBM_BOOLEAN /*EE_MANAGER_CALL*/ EE_IsChecksumsValid(void);
@far ITBM_BOOLEAN /*EE_MANAGER_CALL*/ EE_IsVersionValid(void);

@far void /*MEM_FAR*/ EE_WaitPendingWrites(void);

#ifdef EE_DEBUG_SELF_TEST
@far void /*MEM_FAR*/ EE_DebugSelfTest(void);
#endif

/*****************************************************************************************************
*   Global Variable Definitions
*****************************************************************************************************/

/*****************************************************************************************************
*   Static Variable Definitions
*****************************************************************************************************/
#endif /* MW_EEM_H */

/* Ino.400C0 */


