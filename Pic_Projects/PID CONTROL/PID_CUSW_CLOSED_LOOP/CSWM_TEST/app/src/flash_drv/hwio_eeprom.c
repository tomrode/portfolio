/******************************************************************************
*   (c) Copyright Continental AG 2008
*   All Rights Reserved.
*   Continental AG Confidential Proprietary
*   The Copyright symbol does not also indicate public availability
*   or publication.
*
*   Filename:   hwio_eeprom.c 
*
*   Created_by: L. Kaplan
*       
*   Date_created: 03/25/2008
*
*   Description: EEPROM driver
*******************************************************************************

*******************************************************************************
*               A U T H O R   I D E N T I T Y
*******************************************************************************
* Initials      Name                   Company
* --------      --------------------   ---------------------------------------
* HY            Harsha Yekollu         Continental Automotive Systems
* CK            Can Kulduk             Continental Automotive Systems
*
*
*******************************************************************************

*******************************************************************************
*   Revision History:
*
*   Date          CQ#           Author    
*   MM-DD-YY      oem00000000   Initials   Description of change.
*   -----------   -----------   --------   ------------------------------------
*   03-25-08      oem00023671   LK         EEPROM driver ported from TIPM
*   08-13-08      -             CK         Flash Driver is ported from ITBM project.
*   08-14-08      -             CK         Modified function decleration of 
*				           IsrFlashCmdCompleteInterrupt in order 
*                                          to get the file compiling with Cosmic 
*                                          Compiler version CX12.478 in our setup.
*
*
******************************************************************************/

/*****************************************************************************************************
*   Include Files
*****************************************************************************************************/
#include "itbm_basapi.h"
#include "cpu.h"
#include "memdef.h"
#include "memfunc.h"
#include "hwio_eeprom.h"

/*****************************************************************************************************
*   Local Macro Definitions
*****************************************************************************************************/

/* dflash constants */
#define  DFLASH_WORDS_PER_WRITE     4
#define  DFLASH_WORDS_PER_SECTOR    128
#define  DFLASH_BYTES_PER_SECTOR    ((UInt16)DFLASH_WORDS_PER_SECTOR << 1)

/* FCCOB commands used */
#define  PROGRAM_D_FLASH_WORDS      0x11
#define  ERASE_D_FLASH_SECTOR       0x12
#define  EMULATION_QUERY            0x15
#define  PARTITION_DFLASH           0x20

/* Flags for error checking.  These may be configured as the user requires */
#define EE_CHECK_ADDRESS_VALIDITY       0
#define EE_MAX_RETRY                    3
#define EE_CALL_AT_WRITE_COMPLETION     0

/* Flags for simulating failures or other debugging.  These should NEVER be set in production!! */
#define EE_SIMULATE_MODIFY_FAILURES     0
#define EE_SIMULATE_WRITE_FAILURES      0
#define EE_SIMULATE_ACCERR              0
#define EE_NO_PIPELINING                0
#define EE_ERASE_EEPROM_ON_POWERUP      0

#define Flash_EnableCmdCompleteInterrupt()      (FCNFG |= CCIE)
#define Flash_DisableCmdCompleteInterrupt()     (FCNFG &= ~CCIE)

#define Flash_IsClockDivLoaded()            ((FCLKDIV & FDIVLD) == FDIVLD)
#define Flash_IsCommandComplete()            ((FSTAT & CCIF) == CCIF)
#define Flash_SetCommand()                    (FSTAT = CCIF)

#define Flash_IsProtectionViolation()        ((FSTAT & FPVIOL) == FPVIOL)
#define Flash_IsAccessError()                ((FSTAT & ACCERR) == ACCERR)

#define Flash_ClearProtectionViolation()     (FSTAT |= FPVIOL)
#define Flash_ClearAccessError()             (FSTAT |= ACCERR)

/* macros for accessing paged EEPROM */
#define EE_GetLogicalAddr(addr)     (unsigned char *)(EEPROM_LOGICAL_START + GetEOffset(addr))
#define EE_SetEPAGE(addr)           EPAGE = GetEPage(addr)

/* Given a global address, this macro returns the sector's global address (one sector has 256bytes) */
#define DFlash_GetSectorAddr(addr)         ((UInt32)(addr & 0xFFFFFF00))

#define DFPART 128
#define ERPART 0
#define NUM_D_FLASH_SECTORS      32

/*****************************************************************************************************
*   Local Type Definitions
*****************************************************************************************************/
/* This union provides different representations of a sector. */
typedef union {
    struct {
        unsigned int        wUpperHalf;
        unsigned int        wLowerHalf;
    } sWordWise;

    unsigned long           ulWhole;

    unsigned char           pbyBytes[4];
} UEESectorBuffer;

/* This struct is for bookkeeping during the write process */
typedef struct {
    unsigned long           lAddress;
    unsigned char           *pbyData;
    unsigned char           byLength;
    unsigned char           byBytesWritten;
    unsigned char           byRetryCount;
} SEEPendingWrite;

/* Type for our state variable */
typedef enum {
    EE_IDLE,
    EE_WRITE_SEQUENCE_INITIATED,
    EE_WAIT_FOR_ERASE_COMPLETION,
    EE_WAIT_FOR_WRITE_COMPLETION,
    EE_RETRY
} EEEState;


/* struct used to submit a command to the S12XE Flash state machine */
typedef struct 
{
    UInt8  bFlashCmd;
    UInt8  bGlblAddr_Bit22To16;
    UInt16 wGlblAddr_Bit15To0;
    UInt16 wData0;
    UInt16 wData1;
    UInt16 wData2;
    UInt16 wData3;
} SFCCOBBlock; 

/* This union provides different representations of a sector. */
typedef union {
    UInt16          pbyWords[DFLASH_WORDS_PER_SECTOR];
    UInt8           pbyBytes[((UInt16)DFLASH_WORDS_PER_SECTOR << 1)];
} UDFlashDataBuffer;

typedef struct
{
    UEESectorBuffer SectorAddr;
    UDFlashDataBuffer SectorData;
    UInt8 DFlash_buffer_index;
} UDFlashSectorBuffer;

/*****************************************************************************************************
*   Local Function Declarations
*****************************************************************************************************/
static ITBM_BOOLEAN             EE_PrepDataForWrite(void);
static ITBM_BOOLEAN             EE_SubmitCommand(void);
static void             EE_ErrorHook                (void);
static void             EE_SetNextStateTo           (EEEState enew_state);
static void             EE_WriteSequenceInitiated   (void);
static void             EE_WaitForWriteCompletion   (void);
static void             EE_Retry                    (void);
static void             EE_WaitForHookCompletion    (void);
static void             EraseAllDFlash              (void);
       
/*****************************************************************************************************
*   Global Variable Definitions
*****************************************************************************************************/

/*****************************************************************************************************
*   Static Variable Definitions
*****************************************************************************************************/
#if EE_SIMULATE_MODIFY_FAILURES
static unsigned char        byModifyTTF;
#endif

#if EE_SIMULATE_WRITE_FAILURES
static unsigned char        byWriteTTF;
#endif

#if EE_SIMULATE_ACCERR
static unsigned char        byAccerrTTF;
#endif

/* All of the information regarding the current pending write */
static SEEPendingWrite      sEEPendingWrite;

/*move 256 bytes to page memory */
static UDFlashSectorBuffer  uDFlashSectorBuffer;

/* Current state the EEPROM programming is in */
static EEEState             eEECurrentState;
       
static SFCCOBBlock          sFCCOBBlock;


/*****************************************************************************************************
*   Global and Static Function Definitions
*****************************************************************************************************/

/*****************************************************************************************************
*   Function:    EE_DriverPowerupInit
*
*   Description: This function initializes the micro's internal EEPROM state machine giving it the
*                right clock settings and protection settings.
*
*   Caveats:     This function must be called before using any other function in this driver.
*
*****************************************************************************************************/

void EE_DRIVER_CALL EE_DriverPowerupInit(void)
{
    UInt16 wERPART, wDFPART;

    /* wait for flash command complete */
    while (!Flash_IsCommandComplete());

    /* set flash clock */
    FCLKDIV = FLASH_CLOCK_DIV;

    /* disable EEE interrupt */
    FERCNFG = 0;

    /* disable flash interrupt and fault detection */
    FCNFG = IGNSF;

#if(0)
/* By default, DFlash is used and buffer RAM is not used. 
   Doing partition for this configuration will cause MGSTAT1 and MGSTAT0 error set. */

    /*---------------------------------------
     write EE congiguration once -- move to FBL 
     ---------------------------------------*/

    /* when idle, request emulation configuration */
    while (!Flash_IsCommandComplete());
    sFCCOBBlock.bFlashCmd = EMULATION_QUERY;

    if (EE_SubmitCommand())
    {
        /* if command complete, read emulation configuration */
        while(!Flash_IsCommandComplete());
        FCCOBIX = 1;
        wDFPART = FCCOB;
        FCCOBIX = 2;
        wERPART = FCCOB;

        /* if the first time run, do partionn */
        if ( (DFPART != wDFPART) || (ERPART != wERPART) )
        {
            EraseAllDFlash();

            /* set partition command */
            sFCCOBBlock.bFlashCmd = PARTITION_DFLASH;
            sFCCOBBlock.wGlblAddr_Bit15To0 = DFPART;
            sFCCOBBlock.wData0 = ERPART;

            if (EE_SubmitCommand())
            {
                while(!Flash_IsCommandComplete()); /*  wait for cmd to complete */
                /* read back EE congiguration -- degug */
                sFCCOBBlock.bFlashCmd = EMULATION_QUERY;
                if (EE_SubmitCommand())
                {
                    /* if command complete, read emulation configuration */
                    while(!Flash_IsCommandComplete());

                    FCCOBIX = 1;
                    wDFPART = FCCOB;
                    FCCOBIX = 2;
                    wERPART = FCCOB;
                }
            }
        }
    }
#endif    
}

/*****************************************************************************************************
*   Function:    EE_Handler
*
*   Description: This is the main EEPROM handler that must be a scheduled task in order to run the 
*                state machine.  The call rate will have an effect on performace.. the faster the 
*                better with diminishing returns, of course.
*
*   Caveats:     
*
*****************************************************************************************************/

void EE_DRIVER_CALL EE_Handler(void)
{
    /* Execute the state machine */
    switch(eEECurrentState)
    {
    case EE_WRITE_SEQUENCE_INITIATED:
        EE_WriteSequenceInitiated();
        break;

    case EE_WAIT_FOR_ERASE_COMPLETION:
        /* move to interrupt service routine to speedup eeprom write */
        // EE_Burst_Write();
        break;

    case EE_WAIT_FOR_WRITE_COMPLETION:
        EE_WaitForWriteCompletion();
        break;

    case EE_RETRY:
        EE_Retry();
        break;

    case EE_IDLE:
        break;

    default:
        break;
    }
}


/*****************************************************************************************************
*   Function:    EE_IsBusy
*
*   Description: Returns true if the driver is not idle.
*
*   Caveats:     EEPROM_memcpy can ONLY be called and direct reads from EE can only be done when 
*                the driver is non-idle.
*
*****************************************************************************************************/
unsigned char EE_DRIVER_CALL EE_IsBusy(void)
{
    UInt8 bystatus;

    if(eEECurrentState == EE_IDLE)
    {
        bystatus = ITBM_FALSE;
    }
    else
    {
        bystatus = ITBM_TRUE;
    }

    return bystatus;
}

/*****************************************************************************************************
*   Function:    EE_ForcedWrite
*
*   Description: Given the EE destination address, source address, and size, this function sets up
*                the write to EE by populating the sEEPendingWrite structure and starting the state
*                machine off in the EE_WRITE_SEQUENCE_INITIATED state.
*
*   Caveats:     This function should never be called while the state machine is in any state 
*                other than EE_IDLE.
*
*****************************************************************************************************/
unsigned char EE_DRIVER_CALL EE_ForcedWrite(unsigned long ee_dest, unsigned char *pbysrc, unsigned char bysize)
{
    unsigned char *physrc;
    
    sEEPendingWrite.lAddress = ee_dest;
    
    /* Store input data address to global pointer */
    sEEPendingWrite.pbyData = pbysrc;

    /* Store requested data size for programming */ 
    sEEPendingWrite.byLength = bysize;

    sEEPendingWrite.byRetryCount = 0;

    /* copy sEEPendingWrite to uDFlashSectorBuffer */
    if (EE_PrepDataForWrite())
    {
       /* Initiate the EEPROM programming */
       EE_SetNextStateTo(EE_WRITE_SEQUENCE_INITIATED);
    }

    return 1;
}

/*****************************************************************************************************
*   Function:    EE_SetNextStateTo
*
*   Description: This function changes the current state to the given state.  It provides a single
*                point for all state changes for debugging purposes.
*
*   Caveats:     
*
*****************************************************************************************************/
void EE_SetNextStateTo(EEEState enew_state)
{
    eEECurrentState = enew_state;
}

/*****************************************************************************************************
 *   Function:       EE_SubmitCommand
 *
 *   Description:    This function submits a command to the micro's internal state machine.  The
 *                   currently tested commands are ERASE_D_FLASH_SECTOR and PROGRAM_D_FLASH.  This
 *                   function follows the process outlined in the databook.  It returns ITBM_TRUE if the
 *                   submission suceeds and ITBM_FALSE if it fails.
 *
 *   Caveats:        The ACCERR and PVIOL bits are always clear after exiting this function, even if
 *                   there was an access error or protection violation.
 *
 *****************************************************************************************************/

ITBM_BOOLEAN EE_SubmitCommand(void)
{
    ITBM_BOOLEAN byresult;

    /* clear flag before start DFlash writing */
    if (Flash_IsProtectionViolation())
    {
        Flash_ClearProtectionViolation();
    }
    
    if (Flash_IsAccessError())
    {
       Flash_ClearAccessError();
    }
    /* set command to erase or program dflash */
    FCCOBIX = 0; 
    FCCOB = (((UInt16)sFCCOBBlock.bFlashCmd) << 8) + sFCCOBBlock.bGlblAddr_Bit22To16;
    
    switch (sFCCOBBlock.bFlashCmd)
    {
        /* 0x12 */
        case ERASE_D_FLASH_SECTOR:
            FCCOBIX = 1;
            FCCOB = sFCCOBBlock.wGlblAddr_Bit15To0;
            break;

        /* 0x11 */
        case PROGRAM_D_FLASH_WORDS:
            FCCOBIX = 1;
//            FCCOB = sFCCOBBlock.wGlblAddr_Bit15To0 + uDFlashSectorBuffer.DFlash_buffer_index - DFLASH_WORDS_PER_WRITE;
            FCCOB = sFCCOBBlock.wGlblAddr_Bit15To0;
            FCCOBIX = 2;
            FCCOB = sFCCOBBlock.wData0;
            FCCOBIX = 3;
            FCCOB = sFCCOBBlock.wData1;
            FCCOBIX = 4;
            FCCOB = sFCCOBBlock.wData2;
            FCCOBIX = 5;
            FCCOB = sFCCOBBlock.wData3;
            break;

        /* 0x15 */
        case EMULATION_QUERY:
            break;

        /* 0x20 */
        case PARTITION_DFLASH:
            FCCOBIX = 1;
            FCCOB = sFCCOBBlock.wGlblAddr_Bit15To0;
            FCCOBIX = 2;
            FCCOB = sFCCOBBlock.wData0;
            break;

        default:  
            break;
    }

    /* FCCOB params are loaded, now start the command */
    Flash_SetCommand();
    
    if (Flash_IsProtectionViolation() || Flash_IsAccessError())
    {
       if (Flash_IsProtectionViolation())
       {
           Flash_ClearProtectionViolation();
       }
        
       if(Flash_IsAccessError())
       {
           Flash_ClearAccessError();
       }
        /* We'll have to retry this command */
        byresult = ITBM_FALSE;
    }
    else
    {
        /* The command submission was successful */
        byresult = ITBM_TRUE;
    }

    return byresult;
}


/*****************************************************************************************************
*   Function:    EE_PrepDataForWrite
*
*   Description: This function takes a SEEPendingWrite struct and sets it up for a write operation
*                by fetching the image of the destination EE sector and putting it in pusector_buffer.
*                It then writes in the desired data to that image so that that buffer can be used
*                in EE_SubmitCommand calls.
*
*   Caveats:     Data in the SEEPendingWrite structure is modified.  After each _complete_ sector 
*                write (a modify + write command sequence), if the byLength field is non-zero, 
*                advance the destination address by the byBytesWritten and call this again.
*
*****************************************************************************************************/

ITBM_BOOLEAN EE_PrepDataForWrite(void)
{
    UInt16 bycopy_index;
    unsigned int *pbysector_address;
    unsigned char *pbyAddress;
    ITBM_BOOLEAN byresult;
    
    if (sEEPendingWrite.byLength != 0)
    {
        /* get the logical address and epage bit from the physical address */
        EE_SetEPAGE(sEEPendingWrite.lAddress);
        pbyAddress = EE_GetLogicalAddr(sEEPendingWrite.lAddress);

        /* Figure out what sector this data belongs in */
        pbysector_address = EE_SectorAddress(pbyAddress);

        /* Copy the whole sector into our buffer */
        for (bycopy_index = 0; bycopy_index < DFLASH_WORDS_PER_SECTOR; bycopy_index++)
        {
           // check if  pbysector_address read correctly!!!
           uDFlashSectorBuffer.SectorData.pbyWords[bycopy_index] = *(UInt16 *)pbysector_address;
           pbysector_address++;
        }

        /* This is a new write, so clear the number of bytes written */
        sEEPendingWrite.byBytesWritten = 0;

        /* Copy the new data on top of our buffer.  We start at where the data
         * points in the sector and continue until we're either at the end of the
         * sector or there's no more data to write 
         */
        for(bycopy_index = EE_AddressSectorOffset(pbyAddress); 
            ((bycopy_index < DFLASH_BYTES_PER_SECTOR) && (sEEPendingWrite.byLength > 0)); bycopy_index++)
        {
            /* Copy from the source to the destination in our image of the sector */
            uDFlashSectorBuffer.SectorData.pbyBytes[bycopy_index] = *(sEEPendingWrite.pbyData);

            /* Increment the address we're reading from so that next time, we'll 
             * point to the next byte in the write that has yet to be copied
             */
            sEEPendingWrite.pbyData++;

            /* We'll need to keep track of this as well because */
            sEEPendingWrite.byBytesWritten++;
            
            /* Decrement the number of bytes to write in the pending write */
            sEEPendingWrite.byLength--;
        }
        
        /* store sector start physical address */
        uDFlashSectorBuffer.SectorAddr.ulWhole = DFlash_GetSectorAddr(sEEPendingWrite.lAddress);

        /* bytes of words written */
        uDFlashSectorBuffer.DFlash_buffer_index = 0;

        byresult = ITBM_TRUE;
    }
    else
    {
        byresult = ITBM_FALSE;
    }
    
    return byresult;
}

/*****************************************************************************************************
*   Function:    EE_WriteSequenceInitiated
*
*   Description: This is the state function for EE_WRITE_SEQUENCE_INITIATED. In this state, the write
*                sequence has been initiated by the EE_Write() function. First, a MODIFY command will
*                be issued to erase the sector and rewrite the high word (lower addresses).  Then, if
*                the micro can accept another command, this state will immediately issue the WRITE
*                command for the lower word (higher addresses) and go to EE_WAIT_FOR_WRITE_COMPLETION.  
*                If the micro cannot accept the cmd, we go to EE_WAIT_FOR_MODIFY_COMPLETION.  If 
*                either of these command submissions fail, we go to EE_RETRY.
*
*   Caveats:     
*
*****************************************************************************************************/
void EE_WriteSequenceInitiated(void)
{

    /* flash command complete */
    if (Flash_IsCommandComplete())
    {
        /* update sFCCOBBlock structure, and trigger DFlash erase */
        sFCCOBBlock.bFlashCmd = ERASE_D_FLASH_SECTOR;
        sFCCOBBlock.bGlblAddr_Bit22To16 = uDFlashSectorBuffer.SectorAddr.pbyBytes[1];
        sFCCOBBlock.wGlblAddr_Bit15To0 = uDFlashSectorBuffer.SectorAddr.sWordWise.wLowerHalf;

        /* erase command is issued successfully */
        if (EE_SubmitCommand())
        {
            /* enable flash complete interrupt */
            Flash_EnableCmdCompleteInterrupt();

            /* the command is accepted, move to the next state */
            EE_SetNextStateTo(EE_WAIT_FOR_ERASE_COMPLETION);

        }
        else
        {
            /* The command wasn't accepted, we need to retry the whole process */
            EE_SetNextStateTo(EE_RETRY);
        }
    }
}

/*****************************************************************************************************
 *   Function:       EE_Burst_Write
 *
 *   Description:    This is the state function for EE_WAIT_FOR_ERASE_COMPLETION. In this state,
 *                   ERASE command has already been issued, but the micro could not accept another
 *                   cmd in its pipeline.  We must wait for the ERASE to complete and then submit the
 *                   WRITE command to write the upper word of the sector (lower addresses).  
 *
 *                   First, the WRITE command for the higher word will be issued. Then, if the micro 
 *                   can accept another command, this state will immediately issue the WRITE command 
 *                   for the lower word and go to EE_WAIT_FOR_WRITE_COMPLETION..  
 *                   If the micro cannot accept the cmd, we go to EE_WAIT_FOR_WORD1_COMPLETION.  
 *                   If either of these command submissions fail, we    go to EE_RETRY.
 *
 *   Caveats:        Called by interrupt service routine
 *
 *****************************************************************************************************/

void EE_DRIVER_CALL EE_Burst_Write(void)
{
    /* previous eeprom command is complete */
    if (Flash_IsCommandComplete())
    {
    
        /* update sFCCOBBlock structure, and trigger DFlash erase */
        sFCCOBBlock.bFlashCmd = PROGRAM_D_FLASH_WORDS;
        sFCCOBBlock.bGlblAddr_Bit22To16 = uDFlashSectorBuffer.SectorAddr.pbyBytes[1];

        /* update data */
        if (uDFlashSectorBuffer.DFlash_buffer_index <= (DFLASH_WORDS_PER_SECTOR - DFLASH_WORDS_PER_WRITE))
        {
            /*--------------------------
            issue eeprom write command 
            --------------------------*/
            sFCCOBBlock.wGlblAddr_Bit15To0 = uDFlashSectorBuffer.SectorAddr.sWordWise.wLowerHalf + (uDFlashSectorBuffer.DFlash_buffer_index << 1);
            sFCCOBBlock.wData0 = uDFlashSectorBuffer.SectorData.pbyWords[uDFlashSectorBuffer.DFlash_buffer_index];
            uDFlashSectorBuffer.DFlash_buffer_index++;
            sFCCOBBlock.wData1 = uDFlashSectorBuffer.SectorData.pbyWords[uDFlashSectorBuffer.DFlash_buffer_index] ;
            uDFlashSectorBuffer.DFlash_buffer_index++;
            sFCCOBBlock.wData2 = uDFlashSectorBuffer.SectorData.pbyWords[uDFlashSectorBuffer.DFlash_buffer_index] ;
            uDFlashSectorBuffer.DFlash_buffer_index++;
            sFCCOBBlock.wData3 = uDFlashSectorBuffer.SectorData.pbyWords[uDFlashSectorBuffer.DFlash_buffer_index] ;
            uDFlashSectorBuffer.DFlash_buffer_index++;

            /*--------------------------
            transition to next state 
            ----------------------------*/

            /* eeprom program error occured */
            if (!EE_SubmitCommand())
            {
                /* disble the interrupt to handover to EE_Handler() */
                Flash_DisableCmdCompleteInterrupt();

                /* The command wasn't accepted, we need to retry the whole process */
                uDFlashSectorBuffer.DFlash_buffer_index = 0;

                EE_SetNextStateTo(EE_RETRY);
            }
        }

        /* finish writing the sector */
        else
        {
            /* disble the interrupt to handover to EE_Handler() */
            Flash_DisableCmdCompleteInterrupt();

            /* handover to EE_Handler() */
            EE_SetNextStateTo(EE_WAIT_FOR_WRITE_COMPLETION);
        }
    }
}

/*****************************************************************************************************
*   Function:    EE_WaitForWriteCompletion
*
*   Description: This is the state function for EE_WAIT_FOR_WRITE_COMPLETION. In this state, the 
*                WRITE command has been issued and we are waiting for it to complete.  When it does
*                finish writing, we'll verify the contents of the EE sector against the sector 
*                buffer.  If they're the same, the entire write sequence was successful and we 
*                need to see if there is any other data to write.  If so, advance the destination
*                pointer by the number of bytes that were just written and prepare for the next
*                write sequence.  If there is no more data to write, go to EE_IDLE.
*
*   Caveats:     
*
*****************************************************************************************************/
void EE_WaitForWriteCompletion(void)
{
    unsigned int *pbysector_address;
    unsigned char *pbyAddress;

    UInt16 bycopy_index;
    ITBM_BOOLEAN endloop;

    if(Flash_IsCommandComplete())
    {
    
        /* get the logical address and epage bit from the physical address */
        EE_SetEPAGE(sEEPendingWrite.lAddress);
        pbyAddress = EE_GetLogicalAddr(sEEPendingWrite.lAddress);

        /* Figure out what sector this data belongs in */
        pbysector_address = EE_SectorAddress(pbyAddress);
        
        /* go through each word and verify all words were written correctly */
        endloop = ITBM_FALSE;
        for (uDFlashSectorBuffer.DFlash_buffer_index = 0; 
             ((uDFlashSectorBuffer.DFlash_buffer_index < DFLASH_WORDS_PER_SECTOR) && (endloop == ITBM_FALSE)); 
             (uDFlashSectorBuffer.DFlash_buffer_index++))
        {
           if (uDFlashSectorBuffer.SectorData.pbyWords[uDFlashSectorBuffer.DFlash_buffer_index] != *(UInt16 *)pbysector_address)
           {
              endloop = ITBM_TRUE;
           }
           pbysector_address++;
        }


        if (endloop == ITBM_TRUE)
        {
            /* The contents are different, we need to retry this block. */
            EE_SetNextStateTo(EE_RETRY);
        }
        else
        {
            if(sEEPendingWrite.byLength != 0)
            {
                /* We're going to continue writing at the address of the next sector */
                sEEPendingWrite.lAddress += sEEPendingWrite.byBytesWritten;

                /* update uDFlashSectorBuffer with sEEPendingWrite */
                if (EE_PrepDataForWrite())
                {
                   EE_SetNextStateTo(EE_WRITE_SEQUENCE_INITIATED);
                }
            }
            else
            {
                EE_SetNextStateTo(EE_IDLE);
            }
        }
    }
}

/*****************************************************************************************************
*   Function:    EE_Retry
*
*   Description: This is the state function for EE_RETRY.  We clean up the ACCERR and PVIOL flags
*                and then wait for any command that is pending (just in case the error happened
*                when pipelining a command after one was already in progress).  Then we clear the
*                flags again and do our retry strategy.
*
*   Caveats:     
*
*****************************************************************************************************/
void EE_Retry(void)
{
    /* What caused an operation to fail probably set one of these flags.  Clear them. */
    if (Flash_IsProtectionViolation())
    {
       Flash_ClearProtectionViolation();
    }
    
    if (Flash_IsAccessError())
    {
       Flash_ClearAccessError();
    }
    
    /* wait for command to complete */
    if(Flash_IsCommandComplete())
    {
        /* We don't know if the flags may have been set again as a result of the command 
        * we just waited for.  So clear the flags again. 
        */
        if (Flash_IsProtectionViolation())
        {
            Flash_ClearProtectionViolation();
        }

        if (Flash_IsAccessError())
        {
            Flash_ClearAccessError();
        }

        if(sEEPendingWrite.byRetryCount < EE_MAX_RETRY)
        {
            /* Increment our retry counter and go back to reinitiate the write sequence */
            sEEPendingWrite.byRetryCount++;

            EE_ErrorHook();

            EE_SetNextStateTo(EE_WRITE_SEQUENCE_INITIATED);
        }
        else
        {
            /* This write has been attempted too many times, give up */
            EE_SetNextStateTo(EE_IDLE);

            EE_ErrorHook();
        }
    }
}

/*****************************************************************************************************
*   Function: EE_ErrorHook
*
*   Description: This function is called everytime something erroneous happens.  This could be when
*                a command submission fails or a modify or write fails.  It provides a single point
*                where these errors can be trapped for debugging (esp with the BDM).
*
*   Caveats:
*
*****************************************************************************************************/
void EE_ErrorHook(void)
{
    _asm("nop");
}



/*****************************************************************************************************
*   Function:    EraseAllDFlash
*
*   Description: This function erases all of the D Flash, one sector at a time.
*                This is for the Flash module on the S12XE.
*
*   Caveats:     we might not need the function as we write to flash directly
*                
*                
*
*****************************************************************************************************/

#if(0)
void EraseAllDFlash( void )
{
    UInt8 wIndex;

    /* clear flag before start DFlash writing */
    if (Flash_IsProtectionViolation())
    {
        Flash_ClearProtectionViolation();
    }

    if (Flash_IsAccessError())
    {
        Flash_ClearAccessError();
    }

    FCCOBIX = 0;
    FCCOB = (ERASE_D_FLASH_SECTOR << 8) + 0x10;
    FCCOBIX = 1;
    FCCOB = 0x0000;

    /* for each sector, call to erase */
    for ( wIndex = 0; wIndex < NUM_D_FLASH_SECTORS; wIndex++ )
    {
        while (!Flash_IsCommandComplete());

        /* FCCOB params are loaded, now start the command */
        Flash_SetCommand();

        /* set command to erase or program dflash */
        if (Flash_IsProtectionViolation())
        {
            Flash_ClearProtectionViolation();
        }

        if (Flash_IsAccessError())
        {
            Flash_ClearAccessError();
        }

        sFCCOBBlock.wGlblAddr_Bit15To0 += 256;
    }
}
#endif

/*****************************************************************************************************
*   Function:    EE_Driver_Static_Init
*
*   Description: This function erases all of the D Flash, one sector at a time.
*                This is for the Flash module on the S12XE.
*
*   Caveats:     we might not need the function as we write to flash directly
*                
*                
*
*****************************************************************************************************/
void EE_DRIVER_CALL EE_Driver_Static_Init (void)
{
   eEECurrentState = EE_IDLE; 
}


#pragma section (inttext)
/***************************************************************************
 * Function:    IsrFlashCmdCompleteInterrupt
 *
 * Description: Calls eeprom driver function, to handle DFLASH complete interrupt
 *
 * Returns:
 *
 * Notes:
 *
 **************************************************************************/ 
 /* MODIFIED this interrupt decleration */
//@interrupt @near void IsrFlashCmdCompleteInterrupt(void)
interrupt near void IsrFlashCmdCompleteInterrupt(void)
{
    EE_Burst_Write();
}

#pragma section ()
