/******************************************************************************
*   (c) Copyright Continental AG 2007
*   All Rights Reserved.
*   Continental AG Confidential Proprietary
*   The Copyright symbol does not also indicate public availability
*   or publication.
*
*   Filename:   mw_tmrm.c 
*
*   Created_by: V.Shvetsov
*       
*   Date_created: 10/26/2007
*
*   Description:  
*
*
*******************************************************************************
*   Revision History:
*
*   Date          CQ#           Author    
*   MM-DD-YY      oem00000000   Initials   Description of change.
*   -----------   -----------   --------   ------------------------------------
*   10-26-07      oem00023081   VS         Initial version. Reused from Palladian.
*   12-11-07      oem00023254   AN         Header files usage is optimized
*   12-14-07      oem00023253   AN         Code is distributed between pages
*   01-08-08      oem00023464   FS         MEM_FAR is removed for aTimerValues and wCurrTime
*   01-08-08      oem00023464   FS         MEM_FAR is removed for aTimerValues and wCurrTime
*   07-21-08      oem00024253   VS         CPU utilisation adjustment and issues fixing
******************************************************************************/
                                                                          
/*****************************************************************************************************
*   Include Files            
*****************************************************************************************************/
#include "itbm_basapi.h" 
#include "mw_tmrm_cfg.h"
#include "mw_tmrm.h"
#include "cpu.h"


/*****************************************************************************************************
*   Local Macro Definitions            
*****************************************************************************************************/
#define TIMER_DISABLED      0xFFFFFFFFUL
#define TIMER_EXPIRED       0x00000000UL
#define TIMER_MAX_VALUE     0x7FFFFFFFUL


#define GLOBAL_5MS          (TRUE)
#define GLOBAL_10MS         ((wCurrTime%2) == 0)
#define GLOBAL_20MS         ((wCurrTime%4) == 0)
#define GLOBAL_1SEC         ((wCurrTime%200) == 0)
#define GLOBAL_1MIN         ((wCurrTime%12000) == 0)

#define IS_TIMER_EXPIRED(TIDX)      (aTimerValues[TIDX] == TIMER_EXPIRED)
#define IS_TIMER_DISABLED(TIDX)     (aTimerValues[TIDX] == TIMER_DISABLED)
#define IS_TIMER_RUNNING(TIDX)      !(IS_TIMER_EXPIRED(TIDX) || IS_TIMER_DISABLED(TIDX))

#define SET_TIMER(TIDX, TVAL)       ((aTimerValues[TIDX] = TVAL))
#define GET_TIMER_VAL(TIDX)         (aTimerValues[TIDX])


/*****************************************************************************************************
*   Local Type Definitions            
*****************************************************************************************************/

/*****************************************************************************************************
*   Local Function Declarations            
*****************************************************************************************************/
static void  TMR_DecTimer(TimerID);

/*****************************************************************************************************
*   Constant Definitions            
*****************************************************************************************************/

/*****************************************************************************************************
*   Global Variable Definitions            
*****************************************************************************************************/
UINT16 ITBM_TMR_EXEC_PERIOD = 10;
//extern const TimersListType ITBM_GPAGE ITBM_TimersList[ITBM_TMR_TIMERS_NUMBER];
Timer32    aTimerValues[NUM_EVENT_TIMERS];
UINT16     wCurrTime;

/*****************************************************************************************************
*  Static Variable definitions           
*****************************************************************************************************/

/*****************************************************************************************************
*   Global and Static Function Definitions            
*****************************************************************************************************/

/*****************************************************************************************************                                                                        
*   Function: ITBM_tmrmPowerupInit()
*
*   Description: This is power up initialization function.                                                                           
*
*   Caveats: None
*****************************************************************************************************/ 
void MEM_FAR ITBM_tmrmPowerupInit(void)
{
    TimerID byindex;

    /* Initialize Counters */
    for(byindex=TIMER_FIRST_10MS; byindex<NUM_EVENT_TIMERS; byindex++)
    {
        aTimerValues[byindex] = TIMER_DISABLED;
    }

    wCurrTime = 0;
//    ITBM_schGetManagerExecTime( Timer, &ITBM_TMR_EXEC_PERIOD );
/*
    wFreeRunning10ms = 0;
    wFreeRunning1sec = 0;
*/
}

/*****************************************************************************************************                                                                        
*   Function: ITBM_tmrmMain()
*
*   Description: This function maintains active counters.  It is called every 5ms                                                                          
*
*   Caveats: None
*****************************************************************************************************/ 
void MEM_FAR ITBM_tmrmMain(void)
{
/*    TimerID byindex;*/
    UINT16  i, j;

#if defined(ITBM_MEAS_TIMING_CYCLE)
    ITBM_measFinish(CYCLE, Timer);
    ITBM_measCalculate(CYCLE, Timer, Timer);
    ITBM_measStart(CYCLE, Timer);                        
#endif /* defined(ITBM_MEAS_TIMING_CYCLE) */                        

#if defined(ITBM_MEAS_TIMING_MAN)
        ITBM_measStart(MAN, Timer);
#endif /* defined(ITBM_MEAS_TIMING_MAN) */

    wCurrTime++;

    if(wCurrTime >= ITBM_TMR_MAX_TIMER_TIME)
    {
        wCurrTime = 0;
    }


    /*for (i=0; i<ITBM_TMR_TIMERS_NUMBER; i++)
    {
        if ( (wCurrTime%(ITBM_TimersList[i].period/ITBM_TMR_EXEC_PERIOD)) == 0)
        {
            for (j=ITBM_TimersList[i].firstID+1; j<ITBM_TimersList[i].lastID; j++)
            {
                TMR_DecTimer((TimerID)j);
            }
            
        }
    }*/

#if defined(ITBM_MEAS_TIMING_MAN)
    ITBM_measFinish(MAN, Timer);
    ITBM_measCalculate(MAN, Timer, Timer);
#endif /* defined(ITBM_MEAS_TIMING_MAN) */

}

/*****************************************************************************************************                                                                        
*   Function: TMR_DecTimer()
*
*   Description: This function decrements the specified counter as long as the 
*                timer is in a running state                                                                          
*
*   Caveats: None
*****************************************************************************************************/ 
static void TMR_DecTimer(TimerID Tindex)
{
    if(IS_TIMER_RUNNING(Tindex))
    {
        aTimerValues[Tindex]--;
/*        aTimerValues[Tindex] = aTimerValues[Tindex] - ITBM_TMR_EXEC_PERIOD;*/
    }
}

/*****************************************************************************************************                                                                        
*   Function: ITBM_tmrmSetTimer()
*
*   Description: This function sets the expiration time of a counter                                                                          
*
*   Caveats: None
*****************************************************************************************************/ 
ITBM_BOOLEAN MEM_FAR ITBM_tmrmSetTimer(TimerID Tindex, Timer32 byCounts)
{
    /* Timer value can not exceed 8-bit maximum */
    if(byCounts > TIMER_MAX_VALUE)
    {
        return ITBM_FALSE;
    }

    SET_TIMER(Tindex, byCounts);

    return ITBM_TRUE;
}

/*****************************************************************************************************                                                                        
*   Function: ITBM_tmrmDisableTimer()
*
*   Description: This function clears (turns off) the selected timer                                                                          
*
*   Caveats: None
*****************************************************************************************************/ 
void MEM_FAR ITBM_tmrmDisableTimer(TimerID Tindex)
{
    SET_TIMER(Tindex, TIMER_DISABLED);
}


/*****************************************************************************************************                                                                        
*   Function: ITBM_tmrmGetTimerValues()
*
*   Description: This function sets the expiration time of a counter                                                                          
*
*   Caveats: None
*****************************************************************************************************/ 
static UINT8 ITBM_tmrmGetTimerValues(TimerID Tindex)
{
    return GET_TIMER_VAL(Tindex);
}


/*****************************************************************************************************                                                                        
*   Function: ITBM_tmrmExpired()
*
*   Description: This function returns the timer expired status
*
*   Caveats: 
*****************************************************************************************************/ 
ITBM_BOOLEAN MEM_FAR ITBM_tmrmExpired(TimerID Tindex)
{
    return IS_TIMER_EXPIRED(Tindex);
}

/*****************************************************************************************************                                                                        
*   Function: ITBM_tmrmIsDisabled()
*
*   Description: This function returns the timer disabled status
*
*   Caveats: 
*****************************************************************************************************/ 
ITBM_BOOLEAN MEM_FAR ITBM_tmrmIsDisabled(TimerID Tindex)
{
    return IS_TIMER_DISABLED(Tindex);
}

/*****************************************************************************************************                                                                        
*   Function: ITBM_tmrmIsRunning()
*
*   Description: This function returns the timer running status 
*
*   Caveats: 
*****************************************************************************************************/ 
ITBM_BOOLEAN MEM_FAR ITBM_tmrmIsRunning(TimerID Tindex)
{
    return IS_TIMER_RUNNING(Tindex);
}

/*****************************************************************************************************                                                                        
*   Function: ITBM_tmrmIsRunning()
*
*   Description: This function returns the timer remaining count 
*
*   Caveats: 
*****************************************************************************************************/ 
Timer32 MEM_FAR ITBM_tmrmGetRemaining(TimerID Tindex)
{
    return GET_TIMER_VAL(Tindex);
}
