/******************************************************************************
*   (c) Copyright Continental AG 2008
*   All Rights Reserved.
*   Continental AG Confidential Proprietary
*   The Copyright symbol does not also indicate public availability
*   or publication.
*
*   Filename:   mw_diagm_hw.c 
*
*   Created_by: D. Tatarinov
*       
*   Date_created: 02-19-08
*
*   Description:  HW Diagnostic
*
*
*******************************************************************************
*   Revision History:
*
*   Date          CQ#           Author    
*   MM-DD-YY      oem00000000   Initials   Description of change.
*   -----------   -----------   --------   ------------------------------------
*   02-19-08      oem00023537   DT         Initial version
*   04-28-08      oem00023876   DT         DTC bitmasks-related functions added
*   04-30-08      oem00023897   AN         Corrections to the code are made
*   05-04-08      oem00023899   DT         Disable interrupts during diagnostic and
*                                          HSD WD reset added in Accelerometer ST
*   05-14-08      oem00023917   VS         Update for S0 hardware
*   05-17-08      oem00023971   VS         Fixed issues during debugging on S0
*   05-28-08      oem00024045   VS         Ignition voltage test is updated
*   06-16-08      oem00024125   DT         HW diagnostic update
*   07-17-08      oem00024246   VS         Failsafe Circuit test is removed
*   07-23-08      oem00024285   VS         Issues fixing after S0B release testing
******************************************************************************/

/******************************************************************************
*   Include Files
******************************************************************************/ 
#include "itbm_basapi.h"
#include "itbm_errapi.h"
#include "cpu.h"
#include "mw_cfgm.h"
#include "mw_diagm.h"
#include "mw_diagm_hw.h"
#include "mw_diagm_wd.h"
#include "mw_diagm_cfg.h"
#include "mw_iom_mc33982.h"
#include "mw_iom.h"
#include "mw_eem.h"
#include "hwio_display.h"
#include "hwio_adc.h"
#include "hwio_dio.h"
#include "hwio_pwm.h"
#include "app_common.h"
#include "mw_diagm_calibration.h"

/******************************************************************************
*   Local Macro Definitions
******************************************************************************/ 
#define VOLT_DIVIDER ( 6090 / 110 ) /* this coefficient = (49.9k + 11k)*10 / 11k, look at the schematic */

/******************************************************************************
*   Local Type Definitions
******************************************************************************/

/******************************************************************************
*   Local Function Declarations
******************************************************************************/ 

/******************************************************************************
*   Global Variable Definitions
******************************************************************************/

/******************************************************************************
*   Static Variable Definitions
******************************************************************************/

/******************************************************************************
*   Global and Static Function Definitions
******************************************************************************/

/***************************************************************************
 * Function:    ITBM_diagmHW_BitmaskSet
 *
 * Description: the function sets or clears DTC bitmask to 
 *              distinguish troubles that have the same DTC
 *
 * Arguments:   DTC_name - the name of DTC
 *              mask     - the mask to be set
 *
 * Returns: E_ITBM_OK    -   No errors
 *          E_ITBM_PARAM -   there is no parameter with this name
 *
 * Notes:
 *
 **************************************************************************/ 
StatusType MEM_FAR ITBM_diagmHW_BitmaskSetORClear( AppDataIDType DTC_name, UINT16 mask, ITBM_BOOLEAN criteria )
{
    StatusType e_value = E_ITBM_OK;
    UINT16 bitmask;

    /* get current bitmask */
    e_value =  ITBM_cfgmGetData( DTC_name, &bitmask, sizeof( bitmask ) );

    if( E_ITBM_OK == e_value )
    {
        if( criteria )
        {
            bitmask |= mask; /* set mask */
        }
        else
        {
            bitmask &= ~mask; /* clear mask */
        }

        /* set new mask */
        e_value = ITBM_cfgmSetData( DTC_name, &bitmask, sizeof( bitmask ) );
    }

    return( e_value );
}


/***************************************************************************
 * Function:    ITBM_diagmHW_GetParameters
 *
 * Description: the function gets parameters from CM DB
 *
 * Arguments:   signal_name - the name of a signal whose parameters should be defined
 *
 * Returns: E_ITBM_OK    -   No errors
 *          E_ITBM_PARAM -   there is no parameter with this name
 *
 * Notes:
 *
 **************************************************************************/ 
StatusType MEM_FAR ITBM_diagmHW_GetParameters( AppDataIDType signal_name, eITBM_Param_ID *para1, eITBM_Param_ID *para2 )
{
    StatusType e_value = E_ITBM_OK;

    e_value = ITBM_GetParam( signal_name, param1, para1, sizeof( *para1 ) );
    if( E_ITBM_OK == e_value )
    {
        e_value = ITBM_GetParam( signal_name, param2, para2, sizeof( *para2 ) );
    }

    return( e_value );
}


/***************************************************************************
 * Function:    ITBM_diagm_Ignition_Voltage
 *
 * Description: Ignition Voltage Check
 *
 * Arguments:   mode - 0 - init mode, 1 - run_mode  
 *
 * Returns: E_ITBM_OK    -   No errors
 *          E_ITBM_ID    -   Identifier is incorrect (there is no such identifier)
 *          E_ITBM_PARAM -   there is no parameter with this name
 *          E_ITBM_SIZE  -   Size of requested data does not correspond to data size stored 
 *                           in Configuration table
 * Notes:
 *
 **************************************************************************/ 
StatusType ITBM_diagm_Ignition_Voltage( void )
{
    StatusType e_value = E_ITBM_OK;
    ITBM_BOOLEAN dtc_flag[2] = { ITBM_FALSE, ITBM_FALSE };
    UINT16 max_ign_volt;
    UINT16 min_ign_volt;
    UINT16 ign_volt;
    UINT32 temp;
    eITBM_Param_ID par1;
    eITBM_Param_ID par2;

    e_value = ITBM_cfgmGetData( ignition, &ign_volt, sizeof( ign_volt ));

    /* Get threshold values */
    if( E_ITBM_OK == e_value )
    {
        e_value = ITBM_cfgmGetData( ignition_max_voltage, &max_ign_volt, sizeof( max_ign_volt ));
    }

    if( E_ITBM_OK == e_value )
    {
        e_value = ITBM_cfgmGetData( ignition_min_voltage, &min_ign_volt, sizeof( min_ign_volt ));
    }

    if( E_ITBM_OK == e_value )
    {
        temp = (UINT32)ign_volt * (UINT32)ADC_PRECISION * (UINT32)VOLT_DIVIDER;
        // In s0 schematic a diode is used, so we need to add 600mV.
        ign_volt = (UINT16)((temp / 10) + 600);
    
        /* Check ignition voltage */
        if( ign_volt < min_ign_volt )
        {
            /* Ignition voltage less than minimum */
            dtc_flag[0] = ITBM_TRUE;
        }
    
    
        if( ign_volt > max_ign_volt )
        {
            /* Ignition voltage greater than maximum */
            dtc_flag[1] = ITBM_TRUE;
        }
    }

    /* set or clear DTC masks */
    if( E_ITBM_OK == e_value )
    {
        e_value = ITBM_diagmHW_BitmaskSetORClear( DTC_IGN_VOLTAGE_LOW, DTC_BM_IGN_VOLTAGE_LOW, dtc_flag[0] );
    }

    if( E_ITBM_OK == e_value )
    {
        e_value = ITBM_diagmHW_BitmaskSetORClear( DTC_IGN_VOLTAGE_HIGH, DTC_BM_IGN_VOLTAGE_HIGH, dtc_flag[1] );
    }

    return( e_value );
}

/***************************************************************************
 * Function:    ITBM_diagm_Accelerometer
 *
 * Description: Accelerometer Self Test
 *
 * Arguments:   none  
 *
 * Returns: E_ITBM_OK    -   No errors
 *          E_ITBM_ID    -   Identifier is incorrect (there is no such identifier)
 *          E_ITBM_PARAM -   there is no parameter with this name
 *          E_ITBM_SIZE  -   Size of requested data does not correspond to data size stored 
 *                           in Configuration table
 * Notes:
 *
 **************************************************************************/ 
StatusType ITBM_diagm_Accelerometer( void )
{
    StatusType e_value = E_ITBM_OK;
    ITBM_BOOLEAN dtc_flag = ITBM_FALSE;
    UINT32 max_acc;
    UINT32 min_acc;
    UINT32 acc_1;
    UINT32 acc_2;
    UINT32 acc;
    UINT16 i;
    eITBM_Param_ID par1;
    eITBM_Param_ID par2;
    eITBM_Param_ID par3;
    eITBM_Param_ID par5;
    eITBM_Param_ID par6;


    e_value = ITBM_diagmHW_GetParameters( acc_st, &par1, &par2 );

    if( E_ITBM_OK == e_value )
    {
        e_value = ITBM_GetParam( acc_st, Active_Low, &par3, sizeof( par3 ) );
    }

    if( E_ITBM_OK == e_value )
    {
        e_value = ITBM_diagmHW_GetParameters( acc_x_pwm, &par5, &par6 );
    }

    Enable_Maskable_Interrupts();

    /* wait reliable data from accelerometer */
    while( ITBM_FALSE == ITBM_pwmReadyStatus( par5 )  )
    {
        ITBM_diagWdMain();
        ITBM_iomWDTResMC33982();
    }
    while( ITBM_TRUE == ITBM_pwmReadyStatus( par5 ) )
    {
        ITBM_diagWdMain();
        ITBM_iomWDTResMC33982();
    }
    while( ITBM_FALSE == ITBM_pwmReadyStatus( par5 ) )
    {
        ITBM_diagWdMain();
        ITBM_iomWDTResMC33982();
    }

    Disable_Maskable_Interrupts();

    /* Get actual value of the accelerometer after ST pin is active */
    if( E_ITBM_OK == e_value )
    {
        e_value = ITBM_pwmGetDuty( (UINT8)par5, &acc_1 );
    }

    /* set ST output to active state */
    if( E_ITBM_OK == e_value )
    {
        e_value = ITBM_dioWriteData( (UINT8)par1, (UINT8)par2, ITBM_TRUE ^ (ITBM_BOOLEAN)par3 );
    }

    Enable_Maskable_Interrupts();

    /* wait for transition period expired */
    for( i = 0; i < 10000; i++ )
    {
        ITBM_diagWdMain();
        ITBM_iomWDTResMC33982();
    }

    /* wait reliable data from accelerometer */
    while( ITBM_FALSE == ITBM_pwmReadyStatus( par5 ) )
    {
        ITBM_diagWdMain();
        ITBM_iomWDTResMC33982();
    }
    while( ITBM_TRUE == ITBM_pwmReadyStatus( par5 ) )
    {
        ITBM_diagWdMain();
        ITBM_iomWDTResMC33982();
    }
    while( ITBM_FALSE == ITBM_pwmReadyStatus( par5 ) )
    {
        ITBM_diagWdMain();
        ITBM_iomWDTResMC33982();
    }

    Disable_Maskable_Interrupts();

    /* Get actual value of the accelerometer after ST pin is active */
    if( E_ITBM_OK == e_value )
    {
        e_value = ITBM_pwmGetDuty( (UINT8)par5, &acc_2 );
    }

    /* set ST output to inactive state */
    if( E_ITBM_OK == e_value )
    {
        e_value = ITBM_dioWriteData( (UINT8)par1, (UINT8)par2, ITBM_FALSE ^ (ITBM_BOOLEAN)par3 );
    }


    /* Get threshold values */
    if( E_ITBM_OK == e_value )
    {
        e_value = ITBM_cfgmGetData( accel_selftest_min, &min_acc, sizeof( min_acc ));
    }
    if( E_ITBM_OK == e_value )
    {
        e_value = ITBM_cfgmGetData( accel_selftest_max, &max_acc, sizeof( max_acc ));
    }

    if( E_ITBM_OK == e_value )
    {
        acc = acc_1 - acc_2;
    
        /* Check accelerometer output */
        if( ( acc < min_acc ) || ( acc > max_acc ) )
        {
            /* dyty cycle is less than minimum or greater than maximum */
            dtc_flag = ITBM_TRUE;
        }

        /* set DTC bitmask if we have a problems */
        e_value = ITBM_diagmHW_BitmaskSetORClear( DTC_ACCELEROMETER_OOR, DTC_BM_ACCELEROMETER_ST, dtc_flag );
    }

    return( e_value );
}

/***************************************************************************
 * Function:    ITBM_diagm_Manual_LeverRef
 *
 * Description: Manual Lever Test
 *
 * Arguments:   none  
 *
 * Returns: E_ITBM_OK    -   No errors
 *          E_ITBM_ID    -   Identifier is incorrect (there is no such identifier)
 *          E_ITBM_PARAM -   there is no parameter with this name
 *          E_ITBM_SIZE  -   Size of requested data does not correspond to data size stored 
 *                           in Configuration table
 * Notes:
 *
 **************************************************************************/ 
StatusType ITBM_diagm_Manual_LeverRef( void )
{
    StatusType e_value = E_ITBM_OK;
    UINT16 max_man_lev_ref;
    UINT16 min_man_lev_ref;
    UINT16 man_lev_ref;
    ITBM_BOOLEAN dtc_flag = ITBM_FALSE;
    UINT32 temp;
    eITBM_Param_ID par1;
    eITBM_Param_ID par2;

    e_value = ITBM_cfgmGetData( lever_ref, &man_lev_ref, sizeof( man_lev_ref ));

    /* Get threshold values */
    if( E_ITBM_OK == e_value )
    {
        e_value = ITBM_cfgmGetData( manual_lever_ref_min, &min_man_lev_ref, sizeof( min_man_lev_ref ));
    }
    if( E_ITBM_OK == e_value )
    {
        e_value = ITBM_cfgmGetData( manual_lever_ref_max, &max_man_lev_ref, sizeof( max_man_lev_ref ));
    }

    if( E_ITBM_OK == e_value )
    {
        man_lev_ref = man_lev_ref * ADC_PRECISION;
    
        /* Check manual lever reference voltage */
        if( ( man_lev_ref < min_man_lev_ref ) || ( man_lev_ref > max_man_lev_ref ) )
        {
            /* lever ref voltage less than minimum or greater than maximum */
            dtc_flag = ITBM_TRUE;
        }
    }

    // NOTE: Manual_apply and Manual_apply_re are compared in GetCalibratedManualLeverPos().

    /* set or clear DTC masks */
    if( E_ITBM_OK == e_value )
    {
        e_value = ITBM_diagmHW_BitmaskSetORClear( DTC_MANUAL_LEVEL_OOR, DTC_BM_MANUAL_LEVEL_REF_OOR, dtc_flag );
    }

    return( e_value );
}

/***************************************************************************
 * Function:    ITBM_diagm_Manual_LeverStartUp
 *
 * Description: Manual Lever Test
 *
 * Arguments:   none  
 *
 * Returns: E_ITBM_OK    -   No errors
 *          E_ITBM_ID    -   Identifier is incorrect (there is no such identifier)
 *          E_ITBM_PARAM -   there is no parameter with this name
 *          E_ITBM_SIZE  -   Size of requested data does not correspond to data size stored 
 *                           in Configuration table
 * Notes:
 *
 **************************************************************************/ 
StatusType ITBM_diagm_Manual_LeverStartUp( void )
{
    StatusType e_value = E_ITBM_OK;
    UINT16 max_man_lev_ref;
    UINT16 min_man_lev_ref;
    UINT16 man_lev_ref;
    UINT16 man_lev;
    UINT16 man_lev_ref_adjusted;
    UINT16 man_lev_adjusted;
    UINT16 tol;
    ITBM_BOOLEAN dtc_flag[2] = { ITBM_FALSE, ITBM_FALSE };
    UINT32 temp;
    eITBM_Param_ID par1;
    eITBM_Param_ID par2;
    UINT16 delta;

    /* Get actual value of manual lever reference voltage */
    e_value = ITBM_diagmHW_GetParameters( lever_ref, &par1, &par2 );
    if( E_ITBM_OK == e_value )
    {
        e_value = ITBM_adcConvert( (UINT8)par1, (UINT8)par2, 1 );
    }
    if( E_ITBM_OK == e_value )
    {
        e_value = ITBM_adcReadData( (UINT8)par1, (UINT8)par2, &man_lev_ref );
    }

    /* Get actual value of manual lever voltage */
    if( E_ITBM_OK == e_value )
    {
        e_value = ITBM_diagmHW_GetParameters( man_app, &par1, &par2 );
    }
    if( E_ITBM_OK == e_value )
    {
        e_value = ITBM_adcConvert( (UINT8)par1, (UINT8)par2, 1 );
    }
    if( E_ITBM_OK == e_value )
    {
        e_value = ITBM_adcReadData( (UINT8)par1, (UINT8)par2, &man_lev );
    }


    /* Get threshold values */
    if( E_ITBM_OK == e_value )
    {
        e_value = ITBM_cfgmGetData( manual_lever_ref_min, &min_man_lev_ref, sizeof( min_man_lev_ref ));
    }
    if( E_ITBM_OK == e_value )
    {
        e_value = ITBM_cfgmGetData( manual_lever_ref_max, &max_man_lev_ref, sizeof( max_man_lev_ref ));
    }

    /* Get manual lever voltage tolerance value */
    if( E_ITBM_OK == e_value )
    {
        e_value = ITBM_cfgmGetData( manual_lever_voltage_tolerance, &tol, sizeof( tol ));
    }

    if( E_ITBM_OK == e_value )
    {
        man_lev_ref = man_lev_ref * ADC_PRECISION;
        man_lev = man_lev * ADC_PRECISION;

        /* Check manual lever reference voltage */
        if( ( man_lev_ref < min_man_lev_ref ) || ( man_lev_ref > max_man_lev_ref ) )
        {
            /* lever ref voltage less than minimum or greater than maximum */
            dtc_flag[0] = ITBM_TRUE;
        }
    
        /* take into account the divider ratios */ 
        /* AN07 (man_ref) is 49/49 or 0.5
           AN00 (man_app) is 49/49 or 0.5 */
        man_lev_ref_adjusted = man_lev_ref; //(UINT16)((UINT32)((UINT32)man_lev_ref * 50)/100);
        man_lev_adjusted = (UINT16)((UINT32)((UINT32)man_lev * 50)/100);

        if( man_lev_adjusted >= man_lev_ref_adjusted )
        {
            delta = man_lev_adjusted - man_lev_ref_adjusted;
        }
        else
        {
            delta = man_lev_ref_adjusted - man_lev_adjusted;
        }
    
        if( delta > tol )
        {
            /* lever ref voltage is OK, now check lever voltage */
            dtc_flag[1] = ITBM_TRUE;
        }
    }

    /* set or clear DTC masks */
    if( E_ITBM_OK == e_value )
    {
        e_value = ITBM_diagmHW_BitmaskSetORClear( DTC_MANUAL_LEVEL_OOR, DTC_BM_MANUAL_LEVEL_REF_OOR, dtc_flag[0] );
    }

    if( E_ITBM_OK == e_value )
    {
        e_value = ITBM_diagmHW_BitmaskSetORClear( DTC_MANUAL_LEVEL_OOR, DTC_BM_MANUAL_LEVEL_OUTPUT_OOR_INIT, dtc_flag[1] );
    }

    return( e_value );
}

/***************************************************************************
 * Function:    ITBM_diagm_HSD_Faults
 *
 * Description: HSD Faults Check
 *
 * Arguments:   mode - 0 - init mode, 1 - run mode  
 *
 * Returns: E_ITBM_OK    -   No errors
 *          E_ITBM_ID    -   Identifier is incorrect (there is no such identifier)
 *          E_ITBM_PARAM -   there is no parameter with this name
 *          E_ITBM_SIZE  -   Size of requested data does not correspond to data size stored 
 *                           in Configuration table
 *          E_ITBM_IOERROR - SPI Driver couldn't read data from the I/O Device
 * Notes:
 *
 **************************************************************************/ 
StatusType ITBM_diagm_HSD_Faults( void )
{
    StatusType e_value = E_ITBM_OK;
    ITBM_BOOLEAN dtc_flag[3] = { ITBM_FALSE, ITBM_FALSE, ITBM_FALSE };
    ITBM_BOOLEAN fs;
    UINT8 hsd_status;
    eITBM_Param_ID par1;
    eITBM_Param_ID par2;

    e_value = ITBM_cfgmGetData( hsd_fs, &fs, sizeof( fs ));

    if( E_ITBM_OK == e_value )
    {
        fs ^= (ITBM_BOOLEAN)par1;
    
        if( ITBM_TRUE == fs )/* there is a fail */
        {
            /* get HSD status register */
            e_value = ITBM_iomReadMC33982( MC33982_STATR, &hsd_status );
    
            /* parse hsd status data */
            if( hsd_status & HSD_ST_OTF )
            {
                dtc_flag[0] = ITBM_TRUE; /* overtemperature fail occurs */
            }
            if( ( hsd_status & HSD_ST_UVF ) || ( hsd_status & HSD_ST_OVF ) )
            {
                dtc_flag[1] = ITBM_TRUE; /* overvoltage or under voltage fail occurs */
            }
            if( ( hsd_status & HSD_ST_OCHF) || ( hsd_status & HSD_ST_OCLF ) )
            {
                dtc_flag[2] = ITBM_TRUE; /* overcurrent fail occurs */
            }
    
            /* get WDT status */
            e_value = ITBM_iomReadMC33982( MC33982_WDR, &hsd_status );
    
            if( hsd_status & HSD_ST_WDTO )
            {
                 dtc_flag[1] = ITBM_TRUE; /* WDT has expired */
            }
        }
    
        if( E_ITBM_OK == e_value )
        {
            e_value = ITBM_diagmHW_BitmaskSetORClear( DTC_OVERTEMP, DTC_BM_OVERTEMP, dtc_flag[0] );
        }
        if( E_ITBM_OK == e_value )
        {
            e_value = ITBM_diagmHW_BitmaskSetORClear( DTC_HSD_BRAKE_OUTPUT, DTC_BM_HSD_BRAKE_OUTPUT_STATR, dtc_flag[1] );
        }
        if( E_ITBM_OK == e_value )
        {
            e_value = ITBM_diagmHW_BitmaskSetORClear( DTC_BRAKE_OUTPUT_OVERCURRENT, DTC_BM_BRAKE_OUTPUT_OVERCURRENT_SATR, dtc_flag[2] );
        }
    }

    return( e_value );
}

/***************************************************************************
 * Function:    ITBM_diagm_HSD
 *
 * Description: HSD Test (Battery voltage)
 *
 * Arguments:  
 *
 * Returns: E_ITBM_OK    -   No errors
 *          E_ITBM_ID    -   Identifier is incorrect (there is no such identifier)
 *          E_ITBM_PARAM -   there is no parameter with this name
 *          E_ITBM_SIZE  -   Size of requested data does not correspond to data size stored 
 *                           in Configuration table
 * Notes:
 *
 **************************************************************************/ 
StatusType ITBM_diagm_HSD( void )
{
    StatusType e_value = E_ITBM_OK;
    ITBM_BOOLEAN dtc_flag = ITBM_FALSE;
    UINT16 batt_voltage;
    UINT16 ign_volt;
    UINT16 tol;
    eITBM_Param_ID par1;
    eITBM_Param_ID par2;
    UINT16 delta;
    UINT16 b_force;

    /* get current brake_force value */
    e_value = ITBM_cfgmGetData( brake_force, &b_force, sizeof( b_force ) );

    /* ITBM_diagm_HSD shall not be executed in RUN mode if brake output is active */
    if( 0 == b_force )
    {
        if( E_ITBM_OK == e_value )
        {
            e_value = ITBM_cfgmGetData( vbatt, &batt_voltage, sizeof( batt_voltage ));
        }

        if( E_ITBM_OK == e_value )
        {
            e_value = ITBM_cfgmGetData( ignition, &ign_volt, sizeof( ign_volt ));
        }

        /* Get battery voltage tolerance value */
        if( E_ITBM_OK == e_value )
        {
            e_value = ITBM_cfgmGetData( battery_voltage_tolerance, &tol, sizeof( tol ));
        }
    
        if( E_ITBM_OK == e_value )
        {
            batt_voltage = batt_voltage * (UINT16)ADC_PRECISION;
        
            ign_volt = ign_volt * (UINT16)ADC_PRECISION;
        
            if( batt_voltage >= ign_volt )
            {
                delta = batt_voltage - ign_volt;
                /* Get battery voltage tolerance value */
                ITBM_cfgmGetData( battery_voltage_tolerance, &tol, sizeof( tol ));
            }
            else
            {
                delta = ign_volt - batt_voltage;
                /* Get battery voltage tolerance value */
                ITBM_cfgmGetData( battery_voltage_tolerance2, &tol, sizeof( tol ));
            }
         
            /* Check battery voltage */
            if( delta > tol )
            {
                 /* Battery voltage less than minimum or greater than maximum */
                 dtc_flag = ITBM_TRUE;
            }
    
            /* set DTC if we have a problems */
            e_value = ITBM_diagmHW_BitmaskSetORClear( DTC_HSD_BRAKE_OUTPUT, DTC_BM_HSD_BRAKE_OUTPUT_VBATT, dtc_flag );
        }
    }

    return( e_value );
}

/***************************************************************************
 * Function:    ITBM_diagm_BrakeOut_Current
 *
 * Description: Brake Out Current Test
 *
 * Arguments:   none  
 *
 * Returns: E_ITBM_OK    -   No errors
 *          E_ITBM_ID    -   Identifier is incorrect (there is no such identifier)
 *          E_ITBM_PARAM -   there is no parameter with this name
 *          E_ITBM_SIZE  -   Size of requested data does not correspond to data size stored 
 *                           in Configuration table
 * Notes:
 *
 **************************************************************************/ 
StatusType ITBM_diagm_BrakeOut_Current( void )
{
    StatusType e_value = E_ITBM_OK;
    ITBM_BOOLEAN dtc_flag = ITBM_FALSE;
    UINT16 out_curr;
    UINT16 imax1;
    eITBM_Param_ID par1;
    eITBM_Param_ID par2;
    UINT16 b_force;

    /* get current brake_force value */
    e_value = ITBM_cfgmGetData( brake_force, &b_force, sizeof( b_force ) );

    /* Brake Current test also shall be executed if brake output is active only */
    if( ( b_force != 0 ) && ( E_ITBM_OK == e_value ) )
    {
        if( E_ITBM_OK == e_value )
        {
            e_value = ITBM_cfgmGetData( hsd_csns, &out_curr, sizeof( out_curr ));
        }

        /* Get threshold of current values */
        if( E_ITBM_OK == e_value )
        {
            e_value = ITBM_cfgmGetData( brake_current_low, &imax1, sizeof( imax1 ));
        }
    
        /* set DTC if we have a problems */
        if( E_ITBM_OK == e_value )
        {
            out_curr = out_curr * ADC_PRECISION;
    
            if( out_curr > imax1 )
            {
                dtc_flag = ITBM_TRUE;
            }
        }
    }

    e_value = ITBM_diagmHW_BitmaskSetORClear( DTC_BRAKE_OUTPUT_OVERCURRENT, DTC_BM_BRAKE_OUTPUT_OVERCURRENT, dtc_flag );

    return( e_value );
}

/***************************************************************************
 * Function:    ITBM_diagm_RelayStatus_Test
 *
 * Description: Relay status Test
 *
 * Arguments:   none  
 *
 * Returns: E_ITBM_OK    -   No errors
 *          E_ITBM_ID    -   Identifier is incorrect (there is no such identifier)
 *          E_ITBM_SIZE  -   Size of requested data does not correspond to data size stored 
 *                           in Configuration table
 * Notes:
 *
 **************************************************************************/ 
StatusType ITBM_diagm_RelayStatus_Test( void )
{
    StatusType e_value = E_ITBM_OK;
    ITBM_BOOLEAN relst = ITBM_FALSE;
    UINT8 ee_data = 0;

    /* get current RELAY_ST pin */
    e_value = ITBM_cfgmGetData( relay_st, &relst, sizeof( relst ) );

    EE_BlockRead( EE_RELAY_STATUS_FLAG, &ee_data ); /* read stored relay status */

    /*check status, it must be FALSE */
    if( 1 == (ee_data ^ (UINT8)relst) )
    {
        EE_BlockWrite( EE_RELAY_STATUS_FLAG, &relst ); /* write status to eeprom if we need it */
    }

    return( e_value );
}

/***************************************************************************
 * Function:    ITBM_diagmHW_InitMode
 *
 * Description: HW diagnostic during start up
 *
 * Arguments:   none  
 *
 * Returns: E_ITBM_OK    -   No errors
 *          E_ITBM_ID    -   Identifier is incorrect (there is no such identifier)
 *          E_ITBM_PARAM -   there is no parameter with this name
 *          E_ITBM_SIZE  -   Size of requested data does not correspond to data size stored 
 *                           in Configuration table
 *          E_ITBM_IOERROR - SPI Driver couldn't read data from the I/O Device
 *
 * Notes:
 *
 **************************************************************************/ 
StatusType MEM_FAR ITBM_diagmHW_InitMode( void )
{
    StatusType e_value = E_ITBM_OK;
    ITBM_BOOLEAN justFlashed = ITBM_diagmIsJustFlashed(); /* check whether the application is just flashed */

    e_value = ITBM_diagm_Accelerometer();

    if( E_ITBM_OK == e_value )
    {
        e_value = ITBM_diagm_Manual_LeverStartUp();
    }

    /* enable external interrupt from 3rd input capture channel */
    TIE |= 0x08;

    if( ITBM_TRUE == justFlashed )
    {
        UINT8 ee_data = 0;
        EE_BlockWrite( EE_RELAY_STATUS_FLAG, &ee_data ); /* clear EE_RELAY_STATUS_FLAG */
    }

    return( e_value );
}

/***************************************************************************
 * Function:    ITBM_diagmHW_RunMode
 *
 * Description: HW diagnostic during run time
 *
 * Arguments:   none  
 *
 * Returns: E_ITBM_OK    -   No errors
 *          E_ITBM_ID    -   Identifier is incorrect (there is no such identifier)
 *          E_ITBM_PARAM -   there is no parameter with this name
 *          E_ITBM_SIZE  -   Size of requested data does not correspond to data size stored 
 *                           in Configuration table
 *          E_ITBM_IOERROR - SPI Driver couldn't read data from the I/O Device
 *
 * Notes:
 *
 **************************************************************************/ 
StatusType MEM_FAR ITBM_diagmHW_RunMode( void )
{
    StatusType e_value = E_ITBM_OK;


    if( E_ITBM_OK == e_value )
    {
        e_value = ITBM_diagm_Ignition_Voltage();
    }
    if( E_ITBM_OK == e_value )
    {
        e_value = ITBM_diagm_Manual_LeverRef();
    }
    if( E_ITBM_OK == e_value )
    {
        e_value = ITBM_diagm_HSD_Faults();
    }
    if( E_ITBM_OK == e_value )
    {
        e_value = ITBM_diagm_HSD();
    }
    if( E_ITBM_OK == e_value )
    {
        e_value = ITBM_diagm_BrakeOut_Current();
    }
    if( E_ITBM_OK == e_value )
    {
        e_value = ITBM_diagm_RelayStatus_Test();
    }

    return( e_value );
}
