#ifndef ITBM_DISPLAY_H
#define ITBM_DISPLAY_H
/******************************************************************************
*   (c) Copyright Continental AG 2007
*   All Rights Reserved.
*   Continental AG Confidential Proprietary
*   The Copyright symbol does not also indicate public availability
*   or publication.
*
*   Filename:   hwio_display.h
*
*   Created_by: D. Tatarinov
*       
*   Date_created: 10/25/07
*
*   Description: Display Driver
*
*
*   Revision history:
*
*   Date          CQ#           Author     
*   MM-DD-YY      oem00000000   Initials   Description of change
*   -----------   -----------   --------   ------------------------------------
*   10-25-07      oem00023172   DT         Initial version
*   12-11-07      oem00023254   AN         Header files usage is optimized
*   12-14-07      oem00023253   AN         Code is distributed between pages
*   12-24-07      oem00023236   DT         The changes were made to protoboard be working
*   02-07-08      oem00023488   AN         Segment definitions are revised
*   04-25-08      oem00023873   LK         Added ITBM_Display_GetData
*   05-14-08      oem00023917   VS         Update for S0 hardware
*   06-23-08      oem00023723   DT         Unused pins made as output low
******************************************************************************/

/******************************************************************************
*   Include Files
******************************************************************************/ 
#include "itbm_basapi.h"

/******************************************************************************
*   Macro Definitions  
******************************************************************************/

/* Segments control for the Display */
/* Segment position for HDSP-521:
           
   -8--     -1--
 13|  |9   6|  |2
   -14-     -7--
 12|  |10  5|  |3
   -11-     -4--
           
*/
#define SEGMENT_DIS_OFF           0
#define SEGMENT_DIS_UP            0x01
#define SEGMENT_DIS_UP_RIGHT      0x02
#define SEGMENT_DIS_DOWN_RIGHT    0x04
#define SEGMENT_DIS_DOWN          0x08
#define SEGMENT_DIS_DOWN_LEFT     0x10
#define SEGMENT_DIS_UP_LEFT       0x20
#define SEGMENT_DIS_MIDDLE        0x40

#define SEGMENT_DIS_DECIMAL_POINT 0x80


/* Segments control for the Bar */
/* Segment position:
               HDSP-4850                       D010  D011
   -------------------------------------
   |1  |2  |3  |4  |5  |6  |7  |8  |9  |10     o 11  o 12         
   -------------------------------------        
*/
#define SEGMENT_BAR_OFF  0
#define SEGMENT_BAR_1    0x0001
#define SEGMENT_BAR_2    0x0002
#define SEGMENT_BAR_3    0x0004
#define SEGMENT_BAR_4    0x0008
#define SEGMENT_BAR_5    0x0010
#define SEGMENT_BAR_6    0x0020
#define SEGMENT_BAR_7    0x0040
#define SEGMENT_BAR_8    0x0080
#define SEGMENT_BAR_9    0x0100
#define SEGMENT_BAR_10   0x0200

#define BICOLOR_LED_1    0x0800
#define BICOLOR_LED_2    0x1000
#define BICOLOR_LED_3    0x0400
#define BICOLOR_LED_4    0x2000

#define BAR_DATA_MASK1   0x003f
#define BAR_DATA_MASK2   0x00ff

#define BAR_SEGMENTS_MASK1  0x0300
#define BAR_SEGMENTS_MASK2  0x00ff

#define BAR_SEGMENTS_MASK   (BAR_SEGMENTS_MASK1 | BAR_SEGMENTS_MASK2)

#define BAR_BICOLOR_MASK    (BICOLOR_LED_1 | BICOLOR_LED_2 | BICOLOR_LED_3 | BICOLOR_LED_4)

#define BAR_SEGMENT_RED       (BICOLOR_LED_2 | BICOLOR_LED_4)
#define BAR_SEGMENT_GREEN     (BICOLOR_LED_1 | BICOLOR_LED_3)

#define BAR_GRAPH_ALL_SEGMENTS   0xFFFF


#define DISP_DATA_MASK   0x00ff
#define DISP_DATA_MASK2  0x007f /* mask is used for PortA to avoid writing 1 in 7th bit (unused) */
#define INTENSITY_MASK   0x00ff

#define DUTY_100         100

/******************************************************************************
*   Type Definitions
******************************************************************************/

/******************************************************************************
*   External Variables
******************************************************************************/
 
/******************************************************************************
*   Global Function Prototypes  
******************************************************************************/

Status_Type MEM_FAR ITBM_Display_Out( void );
Status_Type MEM_FAR ITBM_Display_Init( void );

Status_Type MEM_FAR ITBM_Display_SetData( UINT8, UINT16 );
Status_Type MEM_FAR ITBM_Display_GetData( UINT8, UINT16 * );


/* extern INTERRUPT void ITBM_Display_Periodic( void ); */

#endif /* ITBM_DISPLAY_H */
