#ifndef CPU_H
#define CPU_H
/******************************************************************************
*   (c) Copyright Continental AG 2007
*   All Rights Reserved.
*   Continental AG Confidential Proprietary
*   The Copyright symbol does not also indicate public availability
*   or publication.
*
*   Filename:   cpu.h 
*
*   Created_by: M. Gorshkova
*       
*   Date_created: 10/2/2007
*
*   Description: Header file for control CPU features 
*******************************************************************************

*******************************************************************************
*               A U T H O R   I D E N T I T Y
*******************************************************************************
* Initials      Name                   Company
* --------      --------------------   ---------------------------------------
* HY            Harsha Yekollu         Continental Automotive Systems
* CK            Can Kulduk             Continental Automotive Systems
*
*
*******************************************************************************

*******************************************************************************
*   Revision history:
*
*   Date          CQ#           Author     
*   MM-DD-YY      oem00000000   Initials   Description of change
*   -----------   -----------   --------   ------------------------------------
*   10-02-07      oem00023000   MG         Initial version
*   12-11-07      oem00023254   AN         Header files usage is optimized
*   12-21-07      oem00023281   MG         define for PLLCLK added
*   12-24-07      oem00023236   DT         The changes were made to protoboard be working
*   08-13-08      -             CK         Ported from ITBM project.
*   08-14-08      -             CK         Included the mc9s12xs128.h 
*				           (our register definition file) file 
*					   and took out ioxdp512.h file.
*
*
******************************************************************************/
 
/******************************************************************************
*   Include Files
******************************************************************************/ 
/* MODIFIED: We are using the mc9s12xs128.h file from HalCogen */
//#include "ioxdp512.h"
#include "mc9s12xs128.h"

/******************************************************************************
*   Macro Definitions  
******************************************************************************/

/********* Initialize Clock Generation ***********************************/

/* OSCCLK for the current board */
#define OSCCLK 16

#if (OSCCLK == 8) 
/* The SYSCLK= PLLCLK = 2*OSCCLK*((SYNR+1)/(REFDV+1)) = 2*8*(3/1) = 48MHz when OSCCLK = 8MHz */

#define SYNR_INIT                           0x02    
#define REFDV_INIT                          0x00    

#else
#if (OSCCLK == 16) 
/* The SYSCLK = PLLCLK = 2*OSCCLK*((SYNR+1)/(REFDV+1)) = 2*16*(3/2) = 48MHz when OSCCLK = 16MHz */
/* !!! Keep in your mind that formula use SYNR+1 and REDV+1 !!! */

#define SYNR_INIT                           0x02   
#define REFDV_INIT                          0x01    

#else
#error Wrong clock frequency defined 
#endif     /* (OSCCLK == 16)*/

#endif     /* (OSCCLK == 8)*/

#define PLLCLK    2*OSCCLK*((SYNR+1)/(REFDV+1))

/* enable Clock Monitor and enable Phase Lock Loop Auto Mode */
#define PLLCTL_INIT                         (CME+PLLON+AUTO)
/* disable Clock Monitor and disable Phase Lock Loop Auto Mode */
#define PLLCTL_OFF                          0x00

/* Set SYSCLK source to PLL and stops in Wait Mode */
#define CLKSEL_INIT                         (PLLSEL)

/* Enable Clock Monitor and normal operation COP with Time-out equal to 32.76ms (>10ms) */
/* OSCCLK = 8MHz divided by 2^18 = 30.51Hz ==> COP time-out = 1/30.51 = 32.76ms */
#define COPCTL_INIT                         (CR1+CR0)
/* OSCCLK = 16MHz divided by 2^20 = 15.25Hz ==> COP time-out = 1/15.25 = 65.53ms  */
#define COPCTL_EVAL                         (CR2)
#define COPCTL_OFF                          0x00

/* enable Clock Monitor and enable Phase Lock Loop Auto Mode */
/* PLLCTL */
#define PLLCTL_USED                         (CME|PLLON|AUTO|SCME)
#define PLLCTL_UNUSED                       (CME|SCME)

/* Set SYSCLK source to PLL and stops in Wait Mode */
#define CLKSEL_INIT                         (PLLSEL)

/* CRGFLG */
#define SCM            0x01
#define LOCK           0x08

/* PLLCTL */
#define CME            0x80
#define PLLON          0x40
#define AUTO           0x20
#define SCME           0x01

/* CLKSEL */
#define PLLSEL         0x80             

/******************************************************************************
*   Type Definitions
******************************************************************************/

/******************************************************************************
*   External Variables
******************************************************************************/
 
/******************************************************************************
*   Global Function Prototypes  
******************************************************************************/
void ITBM_CPU_Init(void);

#endif /* CPU_H */
