/****************************************************************************
| Project Name: EEPROM Driver
|    File Name: EepIO.h
|
|  Description: MCS12XE EEPROM Driver
|
|-----------------------------------------------------------------------------
|               C O P Y R I G H T
|-----------------------------------------------------------------------------
| Copyright (c) 2004-2007 by Vector Informatik GmbH, all rights reserved.
|
| This software is copyright protected and proprietary 
| to Vector Informatik GmbH. Vector Informatik GmbH 
| grants to you only those rights as set out in the 
| license conditions. All other rights remain with 
| Vector Informatik GmbH.
|
|-----------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-----------------------------------------------------------------------------
| Initials      Name                   Company
| --------      --------------------   ---------------------------------------
| WM            Marco Wierer           Vector Informatik GmbH
|-----------------------------------------------------------------------------
|               R E V I S I O N   H I S T O R Y
|-----------------------------------------------------------------------------
| Date        Version   Author  Description
| ----------  --------  ------  ----------------------------------------------
| 2007-02-16  01.00.00  WM      Implementation
| 2007-04-25  01.01.00  WM      No changes
|*****************************************************************************/

#ifndef __EEP_IO_H__
#define __EEP_IO_H__

/* --- Version --- */
/* ##V_CFG_MANAGEMENT ##CQProject : FblWrapperEeprom_Mcs12xLl18His CQComponent : Implementation */
#define FBLWRAPPEREEPROM_MCS12XLL18HIS_VERSION           0x0101
#define FBLWRAPPEREEPROM_MCS12XLL18HIS_RELEASE_VERSION   0x00

/* Defines *******************************************************************/
#define IO_DRIVERMODUS  SYNCRON
#define IO_DEVICETYPE   RANDOM

#define EEPROM_DRIVER_VERSION_MAJOR    (FBLWRAPPEREEPROM_MCS12XLL18HIS_VERSION>>8)
#define EEPROM_DRIVER_VERSION_MINOR    (FBLWRAPPEREEPROM_MCS12XLL18HIS_VERSION & 0xFF)
#define EEPROM_DRIVER_VERSION_PATCH    FBLWRAPPEREEPROM_MCS12XLL18HIS_RELEASE_VERSION

#define FBL_DFLASH_BACKUP_OFFSET           0x100

#define EepromDriver_GetVersionOfDriver()  ((IO_U32)(EEPROM_DRIVER_VERSION_MAJOR<<16) | \
                                           (IO_U32)(EEPROM_DRIVER_VERSION_MINOR<<8)   | \
                                           EEPROM_DRIVER_VERSION_PATCH)

/* Prototypes ****************************************************************/

extern unsigned char  EepromDriver_InitSync(void*);
//Harsha commented the below and added with extra parameter for testing
//extern void WriteFBLDFlashByte(unsigned long dflashAddr, unsigned char dataToWrite);
extern void WriteFBLDFlashByte(unsigned long dflashAddr, unsigned char* dataToWrite,unsigned char TotalSize);
extern unsigned char ReadFBLDFlash(unsigned long dflashMainCopyAddr, unsigned char* readData, unsigned char size);

#endif   /* __EEP_IO_H__ */

/******************************************************************************
******************************************************************************/
