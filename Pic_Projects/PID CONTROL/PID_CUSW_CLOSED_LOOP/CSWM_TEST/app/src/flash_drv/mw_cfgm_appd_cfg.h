#ifndef MW_CFGM_APPD_CFG_H
#define MW_CFGM_APPD_CFG_H
/******************************************************************************
*   (c) Copyright Continental AG 2007-2008
*   All Rights Reserved.
*   Continental AG Confidential Proprietary
*   The Copyright symbol does not also indicate public availability
*   or publication.
*
*   Filename:   mw_cfgm_appd_cfg.h 
*
*   Created_by: M. Gorshkova
*       
*   Date_created: 10/24/2007
*
*   Description: Header file: Configuration manager database - application data 
*
*
*******************************************************************************
*   Revision History:
*
*   Date          CQ#           Author    
*   MM-DD-YY      oem00000000   Initials   Description of change.
*   -----------   -----------   --------   ------------------------------------
*   10-24-07      oem00023080   MG         Initial version
*   11-07-07      oem00023080   MG         correction after code review
*   12-11-07      oem00023254   AN         Header files usage is optimized
*   01-11-08      oem00023356   DT         parameters for brake control added
*   01-15-08      oem00023303   AN         Speed Compensation application is added
*   01-15-08      oem00023302   AN         Autobrake application is added
*   01-18-08      oem00023390   MG         trailer_present, GoToSleepMode added
*   01-23-08      oem00023218   AN         Gain Control application is added
*   01-24-08      oem00023370   AN         Accelerometer Calibration application is added
*   01-24-08      oem00023197   AN         Accelerometer Polling application is added
*   01-29-08      oem00023419   AN         Manual Brake Calibration application is added
*   01-29-08      oem00023211   AN         Manual Brake application is added
*   01-30-08      oem00023228   AN         Dimming Control application is added
*   01-30-08      oem00023464   DT         spi_enable signal added      
*   02-05-08      oem00023424   AN         Signal table is updated
*   02-19-08      oem00023522   AN         Accelerometer precision is improved
*   02-22-08      oem00023544   AN         Voltage Compensation application is added
*   02-26-08      oem00023571   AN         Brake Control algorithm is modified
*   02-27-08      oem00023543   AN         Parameters are removed
*   03-24-08      oem00023687   AN         CAN ignition signal is added
*   03-27-08      oem00023537   DT         HW diagnostic added
*   04-10-08      oem00023695   LP         Add odometer and ignition status signals
*   04-22-08      oem00023695   LP         Review changes from FTR00110826
*   04-22-08      oem00023840   AN         Signal table is updated
*   04-23-08      oem00023853   DT         Display data added
*   04-24-08      oem00023695   LP         Added new Appd ID 
*   04-29-08      oem00023876   AN         External variables are removed
*   05-19-08      oem00023971   MG         blinking signal added
*   06-02-08      oem00023678   AN         Brake Active signal is added
*   06-30-08      oem00024189   DT         Trailer connection status added for diagnostic
******************************************************************************/

/******************************************************************************
*   Include Files
******************************************************************************/ 
#include "itbm_basapi.h"
#include "mw_cfgm_defs.h"               /* common definitions for signals */

/*****************************************************************************
 *  Local Macro Definitions
 *****************************************************************************/

/******************************************************************************
*   Type Definitions
******************************************************************************/
typedef enum {
    /*0x2000*/ OpMode = LOCAL_AD_FIRST_ID,
    /*0x2001*/ gain,
    /*0x2002*/ trailer_present,
    /*0x2003*/ acc_offset,
    /*0x2004*/ auto_brake_out_target,
    /*0x2005*/ accel_nom,
    /*0x2006*/ brake_pedal_pressed,
    /*0x2007*/ manual_lever_min,
    /*0x2008*/ manual_lever_max,
    /*0x2009*/ manual_brake_out_target,
    /*0x200A*/ speed,
    /*0x200B*/ panel_intensity,
    /*0x200C*/ spi_enable,
    /*0x200D*/ filter_a_output,
    /*0x200E*/ blinking_mode_trailer_short,
    /*0x200F*/ gain_display,
    /*0x2010*/ odometer,
    /*0x2011*/ dtc_error_display,
    /*0x2012*/ short_to_gnd_display,
    /*0x2013*/ short_to_batt_display,
    /*0x2014*/ blinking_mode_dtc,
    /*0x2015*/ low_power_mode,
    /*0x2016*/ brake_active,
    /*0x2017*/ vin,
    /*0x2018*/ chime_request,
    /*0x2019*/ display_request,
    /*0x201a*/ trailer_connection_status,

    APPD_MAX_INPUTS
} eApp_APPD_ID;

typedef struct {
    /*  Application Data total size in bytes */
    UINT8               Data_Size;
    /* Initial value of  Application Data */
    AccessRef           pInitValue;
    /* Current value of  Application Data */
    AccessRef           pDATA;
    /* Flag to indicate that the data are stored in configuration table and ready to output */
    ITBM_BOOLEAN        isReceived;
    /* Current error code of Application Data */
    UINT8               Error_Code;
} ITBM_APPD_TableType;

/******************************************************************************
*   Macro Definitions  
******************************************************************************/
/* Constants and Macros for HW Input Application Data*/
#define ITBM_APPD_ARRAY_ELEMENTS (APPD_MAX_INPUTS - LOCAL_AD_FIRST_ID)
#define IS_APPDATA(j)  ( ( ( (j) >= LOCAL_AD_FIRST_ID) && ( (j) < APPD_MAX_INPUTS) ) ? ITBM_TRUE : ITBM_FALSE)
/* for accessing data in the mapping tables by the real index */
#define TRUE_APPD_IDX(id)  (id - LOCAL_AD_FIRST_ID)

/******************************************************************************
*   External Variables
******************************************************************************/
extern ITBM_APPD_TableType ITBM_APPD_Table[ITBM_APPD_ARRAY_ELEMENTS];

/******************************************************************************
*   Global Function Prototypes  
******************************************************************************/

#endif /* MW_CFGM_APPD_CFG_H */
