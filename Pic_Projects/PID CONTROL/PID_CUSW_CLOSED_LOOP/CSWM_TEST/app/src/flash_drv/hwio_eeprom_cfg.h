/******************************************************************************
*   (c) Copyright Continental AG 2008
*   All Rights Reserved.
*   Continental AG Confidential Proprietary
*   The Copyright symbol does not also indicate public availability
*   or publication.
*
*   Filename:   hwio_eeprom_cfg.h 
*
*   Created_by: L. Kaplan
*       
*   Date_created: 03/25/2008
*
*   Description: Header for EEPROM calibration tables
*
*
    *******************************************************************************
*   Revision History:
*
*   Date          CQ#           Author    
*   MM-DD-YY      oem00000000   Initials   Description of change.
*   -----------   -----------   --------   ------------------------------------
*   03-25-08      oem00023671   LK         EEPROM driver ported from TIPM
******************************************************************************/
#ifndef HWIO_EEPROM_CFG_H
#define HWIO_EEPROM_CFG_H

/*****************************************************************************************************
*   Local Macro Definitions
*****************************************************************************************************/
/* if there is no backup block, this constant will be used - checked by EE_Block_Write*/
#define EE_NO_BKUP_OFST     0xFFFF

#define EE_SLP_NO_RFSH      0
#define EE_SLP_RFSH         1

//#define EEADDR_TAB_SIZE     125
#define EEADDR_TAB_SIZE     76 //Total Number of Block Ids before Boot section

/*****************************************************************************************************
*   Local Type Definitions
*****************************************************************************************************/

/*****************************************************************************************************
*   Local Function Declarations
*****************************************************************************************************/

/*****************************************************************************************************
*   Global Variable Definitions
*****************************************************************************************************/
//extern const SEEBlockTableEntry aEEBlockTable[EE_NUM_BLOCKS];
//extern const SEECxsumTableEntry aEECxsumTable[EE_CXSUM_NUM_BLOCKS];
//extern const SEEDefaultsTableEntry aEEDefaultsTable[EE_NUM_DEAFULTS];
//extern const SEECxsumSectionTableEntry aEECxsumSectionTable[EE_CXSUM_NUM_SECTIONS];

extern const SEEBlockTableEntry aEEBlockTable[];
extern const SEECxsumTableEntry aEECxsumTable[];
extern const SEEDefaultsTableEntry aEEDefaultsTable[];
extern const SEECxsumSectionTableEntry aEECxsumSectionTable[];

extern const SEEBlockAddrTableEntry aEEBlockAddrTable[];

#endif /* HWIO_EEPROM_CFG_H */
