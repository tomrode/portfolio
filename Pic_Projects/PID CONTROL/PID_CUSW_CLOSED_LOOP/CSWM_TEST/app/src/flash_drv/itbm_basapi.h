#ifndef ITBM_BASAPI_H
#define ITBM_BASAPI_H
/******************************************************************************
*   (c) Copyright Continental AG 2007-2008
*   All Rights Reserved.
*   Continental AG Confidential Proprietary
*   The Copyright symbol does not also indicate public availability
*   or publication.
*
*   Filename:   itbm_basapi.h 
*
*   Created_by: M. Gorshkova
*       
*   Date_created: 10/2/2007
*
*   Description:  
*
*
*   Revision history:
*
*   Date          CQ#           Author     
*   MM-DD-YY      oem00000000   Initials   Description of change
*   -----------   -----------   --------   ------------------------------------
*   10-02-07      oem00023000   MG         Initial version
*   11-02-07      oem00023000   MG         typedef for Operation mode added
*   10-03-07      oem00023001   DT         ITBM_BOOLEAN type changed
*   01-29-08      oem00023433   MG         Operation mode  changed
*   02-14-08      oem00023488   AN         Operation mode is changed to bit mask
*   02-19-08      oem00023522   AN         Operation mode constants are added
*   04-07-08      oem00023695   LP         Added new typedef, UINT24[3]
*   04-24-08      oem00023695   LP         Added new Group defines
*   05-14-08      oem00023917   VS         Update for S0 hardware
*   05-15-08      oem00023917   MG         Remove IO manager for ETC tests
*   06-06-08      oem00024059   AN         GROUP_STATIONARY is added
******************************************************************************/

/* Type definitions */
typedef unsigned char   UINT8;       /* unsigned 8-bit  */
typedef unsigned char   UInt8;
typedef unsigned int    UINT16;      /* unsigned 16-bit */
typedef unsigned int    UInt16;
typedef unsigned char   UINT24[3];   /* unsigned 24-bit to store 3-byte DTC_ID*/
typedef unsigned long   UINT32;      /* unsigned 32-bit */
typedef unsigned long   UInt32;
typedef signed char     INT8;        /* signed 8-bit    */
typedef int             INT16;       /* signed 16-bit   */
typedef long int        INT32;       /* signed 32-bit   */

typedef UINT16  AppDataIDType; /* Identifier of the Application Data */

typedef void*   AccessRef;     /* Reference to the message data      */
typedef const void* ConstAccessRef; /* Reference to the const message data      */


typedef unsigned char       *BYTEPTR;

typedef unsigned char   Status_Type;  /* Type for the status of the call */

typedef UINT8 ITBM_ERROR_TYPE; /* Error type */

typedef enum
{
    ITBM_FALSE = 0,
    ITBM_TRUE  = 1
}ITBM_BOOLEAN;


/* Definition for 100% */
#define ITBM100_PERCENT 100

/* Time definitions */
#define ITBM_1ms       1 /* Period of time - 1ms (for tasks, debounce strategies)    */
#define ITBM_2ms       2 /* Period of time - 2ms (for tasks, debounce strategies)    */
#define ITBM_3ms       3 /* Period of time - 3ms (for tasks, debounce strategies)    */
#define ITBM_4ms       4 /* Period of time - 4ms (for tasks, debounce strategies)    */
#define ITBM_5ms       5 /* Period of time - 5ms (for tasks, debounce strategies)    */
#define ITBM_6ms       6 /* Period of time - 6ms (for tasks, debounce strategies)    */
#define ITBM_7ms       7 /* Period of time - 7ms (for tasks, debounce strategies)    */
#define ITBM_8ms       8 /* Period of time - 8ms (for tasks, debounce strategies)    */
#define ITBM_9ms       9 /* Period of time - 9ms (for tasks, debounce strategies)    */
#define ITBM_10ms     10 /* Period of time - 10ms (for tasks, debounce strategies)   */
#define ITBM_15ms     15 /* Period of time - 15ms (for tasks, debounce strategies)   */
#define ITBM_20ms     20 /* Period of time - 20ms (for tasks, debounce strategies)   */
#define ITBM_30ms     30 /* Period of time - 30ms (for tasks, debounce strategies)   */
#define ITBM_40ms     40 /* Period of time - 40ms (for tasks, debounce strategies)   */
#define ITBM_160ms   160 /* Period of time - 160ms (for tasks, debounce strategies)  */
#define ITBM_2000ms 2000 /* Period of time - 2000ms (for tasks, debounce strategies) */

#define ITBM_CLEAR    0x0
#define ITBM_NULL     0xFF
#define ITBM_NULL_PTR ((void *)0)
#define ITBM_MAX10BIT 0x3FF
#define ITBM_NULL16   0xFFFF
#define ITBM_NULLREF  (void *)0xFFFF

/* Definitions for enabling/disabling device */
#define ITBM_DISABLE 0 /* Device is disabled */
#define ITBM_ENABLE  1 /* Device is enabled  */

#define ITBM_DEBUG_LEVEL 0
#define ITBM_DEBUG_HIGH  1

/* Interrupt management macros */
#define Disable_Maskable_Interrupts() _asm("sei\n")
#define Enable_Maskable_Interrupts() _asm("cli\n")

/* ON and OFF */
#define ITBM_OFF         0x00
#define ITBM_ON          0x01


typedef UINT8 DEVICE_TYPE;

/* operation mode type */
typedef UINT32 OpModeType;

/* execution groups */
#define GROUP_IO            0x00001
#define GROUP_TIMER         0x00002
#define GROUP_DIAG          0x00004
#define GROUP_STARTUP       0x00008
#define GROUP_ACC           0x00010
#define GROUP_AUTO_BRAKE    0x00020
#define GROUP_COMPENSATION  0x00040
#define GROUP_STATIONARY    0x00080
#define GROUP_CONTROL       0x00100
#define GROUP_INTERFACE     0x00200
#define GROUP_MANUAL_BRAKE  0x00400
#define GROUP_HW_TESTS      0x00800
#define GROUP_UNIT_TESTS    0x01000
#define GROUP_ETC           0x02000
#define GROUP_WD            0x04000


#define GROUP_BRAKES        (GROUP_AUTO_BRAKE | GROUP_MANUAL_BRAKE)

/* standard execution modes */
#define MODE_BASE           (GROUP_IO | GROUP_TIMER | GROUP_DIAG | GROUP_WD)
#define MODE_STARTUP        (MODE_BASE | GROUP_STARTUP)
#define MODE_NORMAL         (MODE_BASE | GROUP_ACC | GROUP_BRAKES | GROUP_COMPENSATION | GROUP_STATIONARY | GROUP_CONTROL | GROUP_INTERFACE | GROUP_HW_TESTS)
#define MODE_HW_TESTS       (GROUP_IO | GROUP_TIMER | GROUP_WD | GROUP_HW_TESTS)
#define MODE_UNIT_TESTS     (GROUP_IO | GROUP_TIMER | GROUP_WD | GROUP_UNIT_TESTS)
#define MODE_ETC            (GROUP_ETC | GROUP_WD)
#define MODE_OFF            0
#define MODE_MAX            0xFFFF


#define INTERRUPT @interrupt

#define MEM_FAR @far
#define ITBM_GPAGE @gpage

#endif /* ITBM_BASAPI_H */
