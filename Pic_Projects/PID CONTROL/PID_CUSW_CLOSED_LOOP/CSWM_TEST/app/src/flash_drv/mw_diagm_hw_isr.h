#ifndef MW_DIAGM_HW_ISR_H
#define MW_DIAGM_HW_ISR_H
/******************************************************************************
*   (c) Copyright Continental AG 2008
*   All Rights Reserved.
*   Continental AG Confidential Proprietary
*   The Copyright symbol does not also indicate public availability
*   or publication.
*
*   Filename:   mw_diagm_hw_isr.h 
*
*   Created_by: D. Tatarinov
*       
*   Date_created: 03/12/08
*
*   Description:  Header file for HW diagnostic-related interrupt handler
*
*
*   Revision history:
*
*   Date          CQ#           Author     
*   MM-DD-YY      oem00000000   Initials   Description of change
*   -----------   -----------   --------   ------------------------------------
*   03-12-08      oem00023537   DT         Initial version
******************************************************************************/
#include "itbm_basapi.h"

/******************************************************************************
*   Macro Definitions  
******************************************************************************/

/******************************************************************************
*   Type Definitions
******************************************************************************/

/******************************************************************************
*   External Variables
******************************************************************************/
 
/******************************************************************************
*   Global Function Prototypes  
******************************************************************************/
/* Modified function prototype */
//INTERRUPT void ITBM_diagm_BrakeOut_Voltage( void );
interrupt void ITBM_diagm_BrakeOut_Voltage( void );

#endif /* MW_DIAGM_HW_ISR_H */
