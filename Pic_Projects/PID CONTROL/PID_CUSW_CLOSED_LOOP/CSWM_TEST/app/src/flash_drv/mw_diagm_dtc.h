#ifndef MW_DIAGM_DTC_H
#define MW_DIAGM_DTC_H
/******************************************************************************
*   (c) Copyright Continental AG 2008
*   All Rights Reserved.
*   Continental AG Confidential Proprietary
*   The Copyright symbol does not also indicate public availability
*   or publication.
*
*   Filename:   mw_diagm_dtc.h 
*
*   Created_by: L. Pauly       
*   Date_created: 3/31/2008
*
*   Description:  
*******************************************************************************

*******************************************************************************
*               A U T H O R   I D E N T I T Y
*******************************************************************************
* Initials      Name                   Company
* --------      --------------------   ---------------------------------------
* HY            Harsha Yekollu         Continental Automotive Systems
* CK            Can Kulduk             Continental Automotive Systems
*
*
*******************************************************************************

*******************************************************************************
*   Revision history:
*
*   Date          CQ#           Author     
*   MM-DD-YY      oem00000000   Initials   Description of change
*   -----------   -----------   --------   ------------------------------------
*   03-31-08      oem00023695   LP         Initial Version
*   04-22-08      oem00023695   LP         Review changes from FTR00110826
*   04-28-08      oem00023876   DT         DTC bitmasks storing to EEPROM added
*   04-30-08      oem00023887   LP         To add demature time
*   06-18-08      oem00024039   LP         Review changes - Add new API
*   08-13-08      -             CK         Ported from ITBM project.
*   08-14-08      -             CK         Commented functions prototypes for
*					   ITBM_diagmClearDiagnosticInformation and
* 					   ITBM_diagmReadDiagnosticInformation. We 
*                                          will use our own functions for these tasks.
*
*
******************************************************************************/

#include "v_inc.h"
#include "desc.h"
#include "appdesc.h"
/******************************************************************************
*   Macro Definitions  
******************************************************************************/
/*Range of DTC values*/
#define D_EMISSION_RELATED_SYSTEMS_K            0x000000
#define D_ALL_POWERTRAIN_DTC_K                  0x000001
#define D_ALL_CHASSIS_DTC_K                     0x400000
#define D_ALL_BODY_DTC_K                        0x800000
#define D_ALL_NET_DTC_K                         0xC00000
#define D_ALL_DTC_K                             0xFFFFFF
/*Format identifier for DTC ID*/
#define D_ISO15031_6DTC_FORMAT_K                0x00
#define D_ISO14229_1DTC_FORMAT_K                0x01
#define D_SAEJ1939_73DTC_FORMAT_K               0x02
/*DTC priority definitions*/
#define D_PRIO_1_K                              0x01
#define D_PRIO_2_K                              0x02
#define D_PRIO_3_K                              0x03
/*Status bits supported by the ITBM module - only 0, 3, 4 and 7 are supported */
#define ITBM_DTC_AVAILABILITY_MASK              0x99 
/* to be used with UINT16 variable */
#define LOWBYTE(x)  (x & 0x00FF)            
#define HIGHBYTE(x) ((x >> 8) & 0x00FF)     
/*size of the chrono-stack and historical stack*/ 
#define SIZEOF_DTC_MEMORY                         8          
#define SIZEOF_DTC_HISTORICAL_MEMORY          SIZEOF_DTC_MEMORY
/*Flags to indicate if DTC is present in chrono-stack*/
#define D_NOT_FOUND_K                        0x00
#define D_FOUND_K                            0x10
/*number of DTCs supported*/
#define SIZEOF_ALL_SUPPORTED_DTC             ITBM_DTC_ARRAY_ELEMENTS
/* Macro definitions for the subfunctions of ReadDtcInformation*/
#define REPORT_NUM_DTC                  0x01
#define REPORT_DTC                      0x02
#define REPORT_EXT_DATA_REC             0x06
#define REPORT_SUPPORTED_DTC            0x0A
#define REPORT_MIRROR_MEM_DTC           0x0F
#define REPORT_MIRROR_MEM_EXT_DATA_REC  0x10
#define REPORT_NUM_MIRROR_MEM_DTC       0x11
#define REPORT_FAULT_COUNTER            0x14
/******************************************************************************
*   Type Definitions
******************************************************************************/
enum FAULTMEMORY_TYP  /*types of fault memory*/
{
 FAULT_MEMORY,      /* chrono stack */
 HISTORICAL_MEMORY  /* historical stack */
};


enum ERROR_STATES  /*types of error states */
{
 ERROR_OFF_K      = 0,    /*fault is not present*/
 ERROR_ON_K       = 1,    /*fault is present*/
 ERROR_NO_CHECK_K = 2     /*fault has not been checked*/
};

/* UDS DTC Status information Bits */
/* If the warning indicator is on for a given DTC, then Confirmed DTC shall be also be set to '1' */
struct STATE            /* DTC Status */
{
  UINT8         TestFailed:                             1;       
  UINT8         TestFailedThisMonitoringCycle:          1;       
  UINT8         Pending:                                1;       
  UINT8         Confirmed:                              1;       
  UINT8         ReadinessIndicator:                     1;  
  UINT8         TestFailedSinceLastClear:               1;       
  UINT8         TestNotCompletedThisMonitoringCycle:    1;       
  UINT8         WarningIndicatorRequested:              1;       
};

struct DTC_MEMORY_ELEMENT
{
 UINT16         original_odometer;      /* Odometer reading with entry of the error in error memory */
 UINT16         most_recent_odometer;   /* current odometer reading of the error in the error memory */
 UINT8          frequency_counter;      /* number of times the error occured */
 UINT8          operation_cycle_counter;/* Ignition cycle counter */
 UINT16         dtc_bitmask;            /* DTC bitmask */
 UINT8          dtc_RomIndex;           /* DTCcodeindex im ROM */
};

/*DTC_Memory contains max of 8 DTC information*/
struct DTC_MEMORY                                         
{
  struct DTC_MEMORY_ELEMENT elem[SIZEOF_DTC_MEMORY];     
};

struct HISTORICAL_INTERROG_RECORD
{
 UINT8          counter;          /* Frequency Counter*/
 UINT8          Kilometer_high;   /* Odometer reading high */
 UINT8          Kilometer_low;    /* Odometer reading low */
};

/*Individual DTC in Historical stack*/
struct DTC_HISTORICAL_MEMORY_ELEMENT
{
 UINT16  original_odometer;   /* original odo value in Historical stack (Kilometerstand) */
 union
  {
   UINT8    all;                             
   struct STATE   state;                           
  } DtcState;                /* DTC status */
 UINT8  dtc_RomIndex;        /* DTC code index in ROM */ 
 UINT16 dtc_bitmask;            /* DTC bitmask */

};


/* EEPROM Historical Table ..Historical memory max of 8 DTCs in Historical stack */
struct DTC_HISTORICAL_MEMORY
{
  struct DTC_HISTORICAL_MEMORY_ELEMENT elem[SIZEOF_DTC_HISTORICAL_MEMORY]; 
};

/* Structure for diagnostic accesses to RAM */
struct DTC_RAM_STR
{
 UINT16              dtc_on_time;   /*Mature time of a DTC*/
 UINT16              dtc_off_time;  /*Demature time of a DTC*/
 enum ERROR_STATES  reported_state  :2; /*The error state of DTC*/
 UINT8              fault_counter;  /*Fault detection counter*/
 union
  {
   UINT8        all;                             
   struct STATE         state;                           
  } DtcState; /*DTC Status*/
};

/******************************************************************************
*   External Variables
******************************************************************************/
/*Flag that allows or prevent the setting or processing of DTCs*/
extern ITBM_BOOLEAN d_DtcSettingModeEnabled_t;
/******************************************************************************
*   Global Function Prototypes  
******************************************************************************/
/*DTC handler Init function*/
Status_Type MEM_FAR ITBM_diagmInitDtc( void );
/*Cyclic DTC task updates the fault memory*/
Status_Type MEM_FAR ITBM_diagmCyclicDtcTask( void );
/*Function to enable or disable DTC setting*/
void MEM_FAR ITBM_diagmDTCSetting( ITBM_BOOLEAN DTC_setting );
/*API to check if there any DTCs are active*/
ITBM_BOOLEAN MEM_FAR ITBM_diagmActiveDTC( void );

/* MODIFIED: Following functions are not used */
/*Function implementing the service Clear Diagnostic Information*/
//void MEM_FAR ITBM_diagmClearDiagnosticInformation(enum DIAG_RESPONSE* Response, DescMsgContext* pMsgContext);
/*Function implementing the service Read diagnostic Information*/
//void MEM_FAR ITBM_diagmReadDiagnosticInformation(enum DIAG_RESPONSE* Response, UINT16* DataLen, DescMsgContext* pMsgContext);



/* MW_DIAGM_DTC_H */
#endif
