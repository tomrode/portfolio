#ifndef MW_CFGM_PARAM_CFG_H
#define MW_CFGM_PARAM_CFG_H
/******************************************************************************
*   (c) Copyright Continental AG 2008
*   All Rights Reserved.
*   Continental AG Confidential Proprietary
*   The Copyright symbol does not also indicate public availability
*   or publication.
*
*   Filename:   mw_cfgm_param_cfg.h 
*
*   Created_by: A. Nikitin
*       
*   Date_created: 02/27/2008
*
*   Description: Header file: Configuration manager database - parameter data 
*
*
*******************************************************************************
*   Revision History:
*
*   Date          CQ#           Author    
*   MM-DD-YY      oem00000000   Initials   Description of change.
*   -----------   -----------   --------   ------------------------------------
*   02-27-08      oem00023543   AN         Initial version
*   03-12-08      oem00023537   DT         HW diagnostic-related parameters added
*   04-22-08      oem00023840   AN         Parameter table is updated
*   04-22-08      oem00023853   DT         Trailer diagnostic parameters added
*   04-25-08      oem00023873   LK         Added s/w version parameters
*   04-29-08      oem00023876   AN         External variables are removed
*   06-02-08      oem00023678   AN         Vehicle normal voltage limit parameters are added
*   06-03-08      oem00024059   AN         Parameters for Vehicle Stationary application are added
*   06-07-08      oem00024085   AN         Parameters for Manual Lever are added
*   06-18-08      oem00024147   MG         Parameters for manual lever (was changed or not)
*   06-27-08      oem00024189   DT         Redundance parameters deleted
*   07-23-08      oem00024285   VS         Issues fixing after S0B release testing
******************************************************************************/

/******************************************************************************
*   Include Files
******************************************************************************/ 
#include "itbm_basapi.h"
#include "mw_cfgm_defs.h"               /* common definitions for signals */

/*****************************************************************************
 *  Local Macro Definitions
 *****************************************************************************/

/******************************************************************************
*   Type Definitions
******************************************************************************/
typedef enum {
    /*0x3000*/ brake_init_a = PARAM_FIRST_ID,
    /*0x3001*/ accel_low,
    /*0x3002*/ accel_hi,
    /*0x3003*/ accel_rmax,
    /*0x3004*/ accel_fmax,
    /*0x3005*/ accel_fault_min,
    /*0x3006*/ accel_fault_max,
    /*0x3007*/ ml_active,
    /*0x3008*/ ml_min,
    /*0x3009*/ brake_b,
    /*0x300A*/ up_step1,
    /*0x300B*/ up_step2,
    /*0x300C*/ down_step1,
    /*0x300D*/ brk_out1_thrshld,
    /*0x300E*/ speed_limit,
    /*0x300F*/ spd_factor_min,
    /*0x3010*/ ext_bicolor_intensity,
    /*0x3011*/ intensity_scaler,
    /*0x3012*/ acc_poll_coef_a,
    /*0x3013*/ acc_poll_coef_b,
    /*0x3014*/ acc_poll_coef_c,
    /*0x3015*/ acc_calib_coef_d,
    /*0x3016*/ acc_calib_coef_e,
    /*0x3017*/ acc_calib_coef_f,
    /*0x3018*/ brk_out1_thrshld_manual,
    /*0x3019*/ up_step1_manual,
    /*0x301A*/ up_step2_manual,
    /*0x301B*/ down_step1_manual,
    /*0x301C*/ count_max2,
    /*0x301D*/ vehicle_normal_voltage,
    /*0x301E*/ ignition_max_voltage,
    /*0x301F*/ ignition_min_voltage,
    /*0x3020*/ accel_selftest_min,
    /*0x3021*/ accel_selftest_max,
    /*0x3022*/ manual_lever_ref_min,
    /*0x3023*/ manual_lever_ref_max,
    /*0x3024*/ brake_out_voltage_tolerance1,
    /*0x3025*/ manual_lever_voltage_tolerance,
    /*0x3026*/ battery_voltage_tolerance,
    /*0x3027*/ battery_voltage_tolerance2,
    /*0x3028*/ failsafe_freq_tolerance,
    /*0x3029*/ failsafe_duty_tolerance,
    /*0x302A*/ brake_current_low,
    /*0x302B*/ count_max1,
    /*0x302C*/ gen_freq,
    /*0x302D*/ gen_low_volt,
    /*0x302E*/ gen_high_volt,
    /*0x302F*/ gen_k,
    /*0x3030*/ gen_b,
    /*0x3031*/ delta_lever_voltage_tolerance,
    /*0x3032*/ brake_out_voltage_tolerance2,
    /*0x3033*/ max_speed,
    /*0x3034*/ short_to_vbatt_threshold,
    /*0x3035*/ short_to_gnd_threshold,
    /*0x3036*/ trailer_connect_threshold,
    /*0x3037*/ vehicle_voltage_min,
    /*0x3038*/ vehicle_voltage_max,
    /*0x3039*/ bp_time,
    /*0x303A*/ bp_default,
    /*0x303B*/ bp_rate,
    /*0x303C*/ manual_lever_changing_tolerance,
    /*0x303D*/ manual_lever_lower_offset,
    /*0x303E*/ manual_lever_upper_offset,

    PARAM_MAX_INPUTS
} eApp_Param_ID;

typedef struct {
    /*  Application Data total size in bytes */
    UINT8               Data_Size;
    /* Initial value of  Application Data */
    AccessRef           pInitValue;
    /* Current value of  Application Data */
    AccessRef           pDATA;
    /* Current error code of Application Data */
    UINT8               Error_Code;
} ITBM_Param_TableType;

/******************************************************************************
*   Macro Definitions  
******************************************************************************/
/* Constants and Macros for Param Data*/
#define ITBM_PARAM_ARRAY_ELEMENTS (PARAM_MAX_INPUTS - PARAM_FIRST_ID)
#define IS_PARAMDATA(j)  ( ( ( (j) >= PARAM_FIRST_ID) && ( (j) < PARAM_MAX_INPUTS) ) ? ITBM_TRUE : ITBM_FALSE)
/* for accessing data in the mapping tables by the real index */
#define TRUE_PARAM_IDX(id)  (id - PARAM_FIRST_ID)

/******************************************************************************
*   External Variables
******************************************************************************/

extern ITBM_Param_TableType ITBM_Param_Table[ITBM_PARAM_ARRAY_ELEMENTS];

/******************************************************************************
*   Global Function Prototypes  
******************************************************************************/

#endif /* MW_CFGM_PARAM_CFG_H */
