/*****************************************************************************
| Project Name: EEPROM Driver
|    File Name: EepMap.h
|
|  Description: Fiat EEPROM memory mapping 
|
|-----------------------------------------------------------------------------
|               C O P Y R I G H T
|-----------------------------------------------------------------------------
| Copyright (c) 2007-2010 by Vector Informatik GmbH, all rights reserved
|
| This software is copyright protected and proprietary 
| to Vector Informatik GmbH. Vector Informatik GmbH 
| grants to you only those rights as set out in the 
| license conditions. All other rights remain with 
| Vector Informatik GmbH.
|
|-----------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-----------------------------------------------------------------------------
| Initials      Name                   Company
| --------      --------------------   ---------------------------------------
| CB            Christian Baeuerle     Vector Informatik GmbH
|-----------------------------------------------------------------------------
|               R E V I S I O N   H I S T O R Y
|-----------------------------------------------------------------------------
| Date        Version   Author  Description
| ----------  --------  ------  ----------------------------------------------
| 2007-07-15  1.00.00   CB      Implementation
| 2007-08-22  1.01.00   CB      Support for Fiat 07274
| 2007-09-17  1.02.00   CB      Minor changes for Fiat 07209 
| 2008-02-13  1.03.00   CB      Added Reprog-flag 
| 2008-03-14  1.04.00   CB      Minor changes
| 2010-12-14  1.05.00   CB      Chrysler addon
|*****************************************************************************/
/*******************************************************************************
*   Continental Revision History:
*
*   Date          MKS Ticket    Author    
*   MM-DD-YYYY    MKS xxxxx     Initials   Description of change.
*   -----------   -----------   --------   ------------------------------------
*   04-12-2011    MKS 69496     H.Yekollu  Add the Primary and Backup start address for the eeprom
*   06-14-2011					H.Yekollu  Added the Reprogram req flag to know that appl started
* 										   after Reflash.
* 	08-04-2011	  MKS 81845		H.Yekollu  Added the ECU Serial Number and VIN Information in Boot MAP.
* 										   So these two can be accessed from both the areas.					  
******************************************************************************/

#ifndef __EEP_MAP_H__
#define __EEP_MAP_H__



/* Base address for FBL EEPROM area ******************************************/
/* Harsha commented original   */
//#define kEepFblBaseAddress             ((FBL_ADDR_TYPE)eepData)

/* Base address for FBL EEPROM area ******************************************/
#define FBL_ADDR_TYPE    					 UINT16

#define kEepFblBaseAddress                ((FBL_ADDR_TYPE)0x101f00)
#define kEepFblBackupBaseAddress          ((FBL_ADDR_TYPE)0x101e00)
#define FBL_DFLASH_BACKUP_OFFSET           0x100


/* Defines for general information part **************************************/

#if defined (FBL_ENABLE_FBL_START)
# define kEepSizeReprogFlag            ((UInt8)0x00)
#else
# define kEepSizeReprogFlag            ((UInt8)0x01)
#endif
/* Size of validation information */
#define kEepSizeValidityFlags          ((UInt8)0x01)
/* Programming attempt counter size */
#define kEepSizeProgAttempts           ((UInt8)0x01)
/* Programming status flags */
#define kEepSizeProgState              ((UInt8)0x04)
/* Size of Reprogram complete flag */
#define kEepSizeReprogReq			   ((UInt8)0x01)

/* Defines for meta data part ************************************************/
/* Software identification */
#define kEepSizeIdentification         ((UInt8)0x0D) /* 13 bytes */
#define kEepSizeApplSwIdent            kEepSizeIdentification
#define kEepSizeApplDataIdent          kEepSizeIdentification
#define kEepSizeBootSwIdent            kEepSizeIdentification

/* Tester fingerprint size */
#define kEepSizeFingerprint            ((UInt8)0x0D) /* 13 bytes */
#define kEepSizeApplSwFingerprint      kEepSizeFingerprint
#define kEepSizeApplDataFingerprint    kEepSizeFingerprint
#define kEepSizeBootSwFingerprint      kEepSizeFingerprint


#define kEepSizeSparePartNo            11u
#define kEepSizeEcuHwNo                11u
#define kEepSizeEcuHwVersionNo         1u
#define kEepSizeEcuSwNo                11u
#define kEepSizeEcuSwVersionNo         2u
#define kEepSizeApprovalNo             6u
#define kEepSizeIdentOption            5u

//# if defined( FBL_ENABLE_CHRYSLER_ADDON )
#define kEepSizeSoftwarePartNo         10u
#define kEepSizeEcuPartNo              10u
//# endif 

#define KEepSizeEcuSerialNo			   15u
#define KEepSizeVIN  				   17u	


/* EEPROM mapping ************************************************************/
#define kEepAddressReprogFlag          (kEepFblBaseAddress)
#define kEepAddressValidityFlags       (kEepAddressReprogFlag+kEepSizeReprogFlag)
#define kEepAddressProgAttempts        (kEepAddressValidityFlags+kEepSizeValidityFlags)
#define kEepAddressProgState           (kEepAddressProgAttempts+kEepSizeProgAttempts)
#define kEepAddressReProgReq		   (kEepAddressProgState+kEepSizeProgState)

/* Fiat reprogramming information block */
/* F180, F181, F182: bootSoftware-, applicationSoftware-, applicationDataIdentification */
#define kEepAddressBootSwIdent         (kEepAddressReProgReq+kEepSizeReprogReq) 
#define kEepAddressApplSwIdent         (kEepAddressBootSwIdent+kEepSizeBootSwIdent)
#define kEepAddressApplDataIdent       (kEepAddressApplSwIdent+kEepSizeApplSwIdent)

/* F183, F184, F185: bootSoftware-, applicationSoftware-, applicationDataFingerprint */ 
#define kEepAddressBootSwFingerprint   (kEepAddressApplDataIdent+kEepSizeApplDataIdent)
#define kEepAddressApplSwFingerprint   (kEepAddressBootSwFingerprint+kEepSizeBootSwFingerprint)
#define kEepAddressApplDataFingerprint (kEepAddressApplSwFingerprint+kEepSizeApplSwFingerprint)

/* F187: sparePartNumber */
#define kEepAddressSparePartNumber     (kEepAddressApplDataFingerprint+kEepSizeApplDataFingerprint)

/* F192 - F195: Hardware and software version numbers */
#define kEepAddressEcuHwNumber         (kEepAddressSparePartNumber+kEepSizeSparePartNo)
#define kEepAddressEcuHwVerNumber      (kEepAddressEcuHwNumber+kEepSizeEcuHwNo)
#define kEepAddressEcuSwNumber         (kEepAddressEcuHwVerNumber+kEepSizeEcuHwVersionNo)
#define kEepAddressEcuSwVerNumber      (kEepAddressEcuSwNumber+kEepSizeEcuSwNo)

/* F196: exhaustRegulationOrTypeApprovalNumber */
#define kEepAddressApprovalNumber      (kEepAddressEcuSwVerNumber+kEepSizeEcuSwVersionNo)

/* F1A5: identificationOption (ISO Code) */
#define kEepAddressIdentOption         (kEepAddressApprovalNumber+kEepSizeApprovalNo)

//# if defined( FBL_ENABLE_CHRYSLER_ADDON )
/* F122: Software part number - Application */
#define kEepAddressSoftwarePartNoAppl  (kEepAddressIdentOption+kEepSizeIdentOption)

/* F122: Software part number - Applikation data */
#define kEepAddressSoftwarePartNoData  (kEepAddressSoftwarePartNoAppl+kEepSizeSoftwarePartNo)

/* F132: ECU part number */
#define kEepAddressEcuPartNo           (kEepAddressSoftwarePartNoData+kEepSizeSoftwarePartNo)
//# endif

#define KEepAddressEcuSerialNo			(kEepAddressEcuPartNo+kEepSizeEcuPartNo)
#define KEepAddressVIN					(KEepAddressEcuSerialNo+KEepSizeEcuSerialNo)
#endif   /* __EEP_MAP_H__ */
