/******************************************************************************
*   (c) Copyright Continental AG 2008
*   All Rights Reserved.
*   Continental AG Confidential Proprietary
*   The Copyright symbol does not also indicate public availability
*   or publication.
*
*   Filename:   memtype.h 
*
*   Created_by: L. Kaplan
*       
*   Date_created: 03/25/2008
*
*   Description: memtype.h
*
*
    *******************************************************************************
*   Revision History:
*
*   Date          CQ#           Author    
*   MM-DD-YY      oem00000000   Initials   Description of change.
*   -----------   -----------   --------   ------------------------------------
*   03-25-08      oem00023671   LK         EEPROM driver ported from TIPM
******************************************************************************/
#ifndef _MEMTYPE_H_
#define _MEMTYPE_H_

/*****************************************************************************************************
*   Include Files
*****************************************************************************************************/

/*****************************************************************************************************
*   Macro Definitions
*****************************************************************************************************/

/*****************************************************************************************************
*   Type Definitions
*****************************************************************************************************/
typedef struct
{
    unsigned long localOffset:12;
    unsigned long rpage:8;
    unsigned long :12;
} S12XRAMType;

typedef struct
{
    unsigned long localOffset:14;
    unsigned long ppage:8;
    unsigned long :10;
} S12XFlashType;

typedef struct
{
    unsigned long localOffset:10;
    unsigned long epage:8;
    unsigned long :14;
} S12XEepromType;

typedef struct
{
    unsigned long localAddr:16;
    unsigned long gpage:7;
    unsigned long :9;
} S12XGlobalType;

typedef union
{
    unsigned long  lwGlobalAddr;
    S12XRAMType    ramAddr;
    S12XFlashType  flashAddr;
    S12XEepromType eeAddr;
    S12XGlobalType globalPageAddr;
} S12XMemType;

typedef struct
{
    unsigned int  addr;
    unsigned char page;
    unsigned char zero;
} SFarPointer;

/*****************************************************************************************************
*   Function Declarations
*****************************************************************************************************/

/*****************************************************************************************************
*   Variable Definitions
*****************************************************************************************************/

/* _MEMTYPE_H_ */
#endif
