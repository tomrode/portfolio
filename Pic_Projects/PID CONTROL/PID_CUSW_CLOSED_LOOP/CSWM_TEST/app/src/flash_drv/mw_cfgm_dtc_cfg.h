#ifndef MW_CFGM_DTC_CFG_H
#define MW_CFGM_DTC_CFG_H
/******************************************************************************
*   (c) Copyright Continental AG 2008
*   All Rights Reserved.
*   Continental AG Confidential Proprietary
*   The Copyright symbol does not also indicate public availability
*   or publication.
*
*   Filename:   mw_cfgm_dtc_cfg.h 
*
*   Created_by: A. Nikitin
*       
*   Date_created: 02/27/2008
*
*   Description: Header file: Configuration manager database - DTC data 
*
*
*******************************************************************************
*   Revision History:
*
*   Date          CQ#           Author    
*   MM-DD-YY      oem00000000   Initials   Description of change.
*   -----------   -----------   --------   ------------------------------------
*   02-27-08      oem00023543   AN         Initial version
*   03-12-08      oem00023537   DT         HW diagnostic DTC added
*   03-14-08      oem00023626   AN         CAN diagnostics are added
*   03-24-08      oem00023687   AN         CAN DTCs are added
*   04-04-08      oem00023695   LP         Added new DTC parameter - DTC_PRIORITY, DTC_Maturetime, 
*                                          DTC_WarningInd
*   04-07-08      oem00023695   LP         Added new DTC parameter - DTC_EventFlg
*   04-07-08      oem00023695   LP         Changed the DTC_ID type 
*   04-22-08      oem00023695   LP         Review changes from FTR00110826
*   04-22-08      oem00023840   AN         DTC table is updated
*   04-24-08      oem00023695   LP         Code changes for limp in action
*   04-29-08      oem00023876   AN         External variables are removed
*   04-28-08      oem00023876   DT         DTC bitmasks added
*   05-04-08      oem00023899   MG         Gain EEPROM corrupted bitmask
*   05-07-08      oem00023899   DT         New bitmasks for DTC_MANUAL_LEVEL_OOR added
*   04-30-08      oem00023887   LP         To add new parameter DTC_DeMaturetime
*   05-14-08      oem00023917   VS         Update for S0 hardware 
*   05-17-08      oem00023971   VS         DTC_CIRCUIT_FAULTS is removed.
*   06-03-08      oem00023678   AN         DTCs for VMM 820 are added
*   05-19-08      oem00023975   DT         Redundant bitmasks has been removed and new mask added
*   06-06-08      oem00024039   LP         New parameter DTC_Readiness and renamed DTC_EventFlg to DTC_OccFault
*   06-12-08      oem00024039   LP         New parameter for enabling/disabling each DTC.
*   06-24-08      oem00024176   VS         Update DTC table in CM according to latest Criteria Matrix
*   07-17-08      oem00024242   LK         Added DTC_BM_ETC_FAULT_STATUS
******************************************************************************/

/******************************************************************************
*   Include Files
******************************************************************************/ 
#include "itbm_basapi.h"
#include "mw_cfgm_defs.h"               /* common definitions for signals */

/*****************************************************************************
 *  Local Macro Definitions
 *****************************************************************************/

/******************************************************************************
*   Type Definitions
******************************************************************************/
typedef enum {
    /*0x4000*/   DTC_IGN_VOLTAGE_LOW = DTC_FIRST_ID,
    /*0x4001*/   DTC_IGN_VOLTAGE_HIGH,
    /*0x4002*/   DTC_OVERTEMP,
    /*0x4003*/   DTC_BRAKE_OUTPUT_OVERCURRENT,
    /*0x4004*/   DTC_CAN_BUS_OFF,
    /*0x4005*/   DTC_CAN_212_MISSING_MSG,
    /*0x4006*/   DTC_CAN_215_MISSING_MSG,
    /*0x4007*/   DTC_CAN_5B0_MISSING_MSG,
    /*0x4008*/   DTC_CAN_212_DATA_ERROR,
    /*0x4009*/   DTC_CAN_215_DATA_ERROR,
    /*0x400A*/   DTC_CAN_5B0_DATA_ERROR,
    /*0x400B*/   DTC_ECU_NOT_CONFIGURED,
    /*0x400C*/   DTC_HSD_BRAKE_OUTPUT,
    /*0x400D*/   DTC_ECU_FAULT_INTERNAL,
    /*0x400E*/   DTC_ACCELEROMETER_OOR,
    /*0x400F*/   DTC_MANUAL_LEVEL_OOR,
    /*0x4010*/   DTC_ECU_FAULT,
    /*0x4011*/   DTC_APP_SW_MISSING,
    /*0x4012*/   DTC_CAN_325_MISSING_MSG,
    /*0x4013*/   DTC_CAN_6FA_MISSING_MSG,
	/*0x4014*/   DTC_CAN_325_DATA_ERROR,
	/*0x4015*/   DTC_CAN_6FA_DATA_ERROR,

    DTC_MAX_INPUTS
} eApp_DTC_ID;

typedef enum {
    /* No action */
    NO_ACTION,
    /*Shut off the output (both for Automatic and Manual)*/
    SHUT_OFF_BOTH_OUTPUTS,
    /*Only Manual Lever is available, display warning on numeric display
      (Automatic is disabled) */
    SHUT_OFF_AUTO_BRAKE_WARNING, 
    /* if the brake pedal status signal is missing, turn off the brake */
    SHUT_OFF_AUTO_OUTPUT,
    /*Automatic is disabled*/
    SHUT_OFF_AUTO_BRAKE_ALGORITHM,
    /* Disable the vehicle speed compensation. Algorithm rely on the accelerometer input */
    DISABLE_SPEED_COMPENSATION,
    /* Go with the default Panel intensity (100) */
    SET_DEFAULT_PANEL_INTENSITY
    
} eApp_LIMP_IN_ACTION_ID;


typedef struct {
    /*  Application Data total size in bytes */
    UINT8               Data_Size;
    /*DTC Config - Enable, Disable*/
    ITBM_BOOLEAN        DTC_Enable;
    /* DTC identifier */
    UINT24              DTC_ID;
    /* DTC priority */
    UINT8               DTC_PRIORITY;
    /*DTC Readiness indicator supported Flag*/
    ITBM_BOOLEAN        DTC_Readiness;
    /*DTC Mature time*/
    UINT16              DTC_Maturetime;
    /*DTC Demature time*/
    UINT16              DTC_DeMaturetime;
    /*DTC occurrence or fault flag*/
    ITBM_BOOLEAN        DTC_OccFault;
    /*DTC Warning Indicator requested*/
    ITBM_BOOLEAN        DTC_WarningInd;
    /*Limp-in action ID*/
    eApp_LIMP_IN_ACTION_ID DTC_LimpInAction;
    /*Self healing criteria for DTC*/
    UINT8               DTC_SelfHealing;
    /* Initial value of  Application Data */
    AccessRef           pInitValue;
    /* Current value of  Application Data */
    AccessRef           pDATA;
    /* Flag to indicate that the data are stored in configuration table and ready to output */
    ITBM_BOOLEAN        isReceived;
    /* Current error code of Application Data */
    UINT8               Error_Code;
} ITBM_DTC_TableType;

/******************************************************************************
*   Macro Definitions  
******************************************************************************/
/* Constants and Macros for DTC Data*/
#define ITBM_DTC_ARRAY_ELEMENTS (DTC_MAX_INPUTS - DTC_FIRST_ID)
#define IS_DTCDATA(j)  ( ( ( (j) >= DTC_FIRST_ID) && ( (j) < DTC_MAX_INPUTS) ) ? ITBM_TRUE : ITBM_FALSE)
/* for accessing data in the mapping tables by the real index */
#define TRUE_DTC_IDX(id)  (id - DTC_FIRST_ID)

/* DTC BITMASKS */
#define DTC_BM_ACCELEROMETER_ST                    0x0001 /* fault occurs during self test mode */
#define DTC_BM_ACCELEROMETER_OOR_CALIBRATION       0x0002 /* out of range fault occurs */
#define DTC_BM_ACCELEROMETER_OOR_POLLING           0x0004 /* out of range fault occurs */
#define DTC_BM_IGN_VOLTAGE_LOW                     0x0001
#define DTC_BM_IGN_VOLTAGE_HIGH                    0x0001
#define DTC_BM_CAN_BUS_OFF                         0x0001
#define DTC_BM_CAN_5B0_MISSING_MSG                 0x0001
#define DTC_BM_CAN_215_MISSING_MSG                 0x0001
#define DTC_BM_CAN_212_MISSING_MSG                 0x0001
#define DTC_BM_CAN_315_MISSING_MSG                 0x0001
#define DTC_BM_CAN_325_MISSING_MSG                 0x0001
#define DTC_BM_CAN_6FA_MISSING_MSG                 0x0001
#define DTC_BM_CAN_5B0_DATA_ERROR                  0x0001
#define DTC_BM_CAN_215_DATA_ERROR                  0x0001
#define DTC_BM_CAN_215_DATA_NA                     0x0002
#define DTC_BM_CAN_212_DATA_ERROR                  0x0001
#define DTC_BM_CAN_212_DATA_NA                     0x0002
#define DTC_BM_CAN_315_DATA_ERROR                  0x0001
#define DTC_BM_CAN_315_DATA_NA                     0x0002
#define DTC_BM_CAN_325_DATA_ERROR                  0x0001
#define DTC_BM_CAN_325_DATA_NA                     0x0002
#define DTC_BM_CAN_6FA_DATA_ERROR                  0x0001
#define DTC_BM_CAN_6FA_DATA_NA                     0x0002
#define DTC_BM_OVERTEMP                            0x0001
#define DTC_BM_BRAKE_OUTPUT_OVERCURRENT            0x0001 /* Overcurrent on brake output occurs */
#define DTC_BM_BRAKE_OUTPUT_OVERCURRENT_SATR       0x0002 /* Overcurrent flag is set in STATR of HSD */
#define DTC_BM_HSD_BRAKE_OUTPUT                    0x0001 /* Brake output voltage is out of range */
#define DTC_BM_HSD_BRAKE_OUTPUT_VBATT              0x0002 /* Battery voltage is out of range */
#define DTC_BM_HSD_BRAKE_OUTPUT_STATR              0x0004 /* Over- or undervoltage flags are set in STATR of HSD */
#define DTC_BM_ELECTRIC_BRAKE_OPEN                 0x0001
#define DTC_BM_ECU_FAULT_INTERNAL_CIRCUIT_FAULTS   0x0001 /* Failsafe Circuit Test */
#define DTC_BM_ECU_FAULT_INTERNAL_COP              0x0002 /* Watchdog timer */
#define DTC_BM_ECU_FAULT_INTERNAL_CLOCK_FREQ       0x0004 /* clock frequency falls below the limit */
#define DTC_BM_MANUAL_LEVEL_REF_OOR                0x0001 /* Manual lever reference voltage out of range */
#define DTC_BM_MANUAL_LEVEL_OUTPUT_OOR_INIT        0x0002 /* Fault occurs during init mode, probably manual lever isn't in initial state */
#define DTC_BM_MANUAL_LEVEL_OUTPUT_OOR_RUN         0x0004 /* Fault occurs during run mode */
#define DTC_BM_RES_MANUAL_LEVEL_OUTPUT_OOR_RUN     0x0008 /* Reserved manual lever voltage out of range */
#define DTC_BM_MANUAL_LEVEL_DELTA_OOR              0x0010 /* Manual lever voltage and reserved manual lever voltage are not equal */
#define DTC_BM_ECU_FAULT_RAM_CORRUPTED             0x0001 /* Microcontroller RAM write/read test detected a corrupt RAM memory location */
#define DTC_BM_ECU_FAULT_FLASH_CORRUPTED           0x0002 /* Microcontroller checksum calculation detected a corrupt Flash memory location */
#define DTC_BM_ECU_FAULT_EEPROM_CORRUPTED          0x0004 /* EEPROM redundancy check detected a corrupt EEPROM memory location */
#define DTC_BM_ECU_GAIN_FAULT_EEPROM_CORRUPTED     0x0008 /* EEPROM redundancy check detected a corrupt EEPROM memory location (Gain) */
#define DTC_BM_ECU_FAULT_ILLEGAL_BP                0x0020 /* Program attempts to an illegal address, a reset occurs */
#define DTC_BM_ECU_FAULT_ILLEGAL_OPCODE            0x0080 /* Program attempts to execute an unused opcode, an unimplemented opcode trap occurs */
#define DTC_BM_ECU_FAULT_STACK_OVERFLOW            0x0100 /* Stack overflow occurs */
#define DTC_BM_APP_SW_MISSING                      0x0001
#define DTC_BM_ETC_FAULT_STATUS                    0x8000

/******************************************************************************
*   External Variables
******************************************************************************/

extern ITBM_DTC_TableType ITBM_DTC_Table[ITBM_DTC_ARRAY_ELEMENTS];

/******************************************************************************
*   Global Function Prototypes  
******************************************************************************/

#endif /* MW_CFGM_DTC_CFG_H */
