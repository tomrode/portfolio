#ifndef MW_CFGM_HWO_CFG_H
#define MW_CFGM_HWO_CFG_H
/******************************************************************************
*   (c) Copyright Continental AG 2007
*   All Rights Reserved.
*   Continental AG Confidential Proprietary
*   The Copyright symbol does not also indicate public availability
*   or publication.
*
*   Filename:   mw_cfgm_hwo_cfg.h 
*
*   Created_by: M. Gorshkova
*       
*   Date_created: 10/18/2007
*
*   Description: Header file for HWO configuration 
*
*
*******************************************************************************
*   Revision History:
*
*   Date          CQ#           Author    
*   MM-DD-YY      oem00000000   Initials   Description of change.
*   -----------   -----------   --------   ------------------------------------
*   10-18-07      oem00023080   MG         Initial version
*   11-07-07      oem00023080   MG         output signals correction
*   11-23-07      oem00023080   MG         Data type for PWM signals corrected
*   11-30-07      oem00023225   MG         Parameter names (port, pin) changed
*   12-11-07      oem00023254   AN         Header files usage is optimized
*   12-24-07      oem00023236   DT         Intensity for BAR and BICOLOR leds added
*   12-24-07      oem00023236   DT         The changes were made to protoboard be working
*   04-29-08      oem00023876   AN         External variables are removed
*   05-23-08      oem00023971   VS         Driver issue corrected
*   06-23-08      oem00023723   DT         Unused pins made as output low
******************************************************************************/

/******************************************************************************
*   Include Files
******************************************************************************/ 
#include    "itbm_basapi.h"
#include    "mw_cfgm_defs.h"               /* common definition for signals */

/*****************************************************************************
 *  Local Macro Definitions
 *****************************************************************************/

/******************************************************************************
*   Type Definitions
******************************************************************************/
typedef enum {
    can_sb = HWO_FIRST_ID,
    acc_st,
    display,
    bar, 
    disp_intens,           
    lit_intens,    
    brake_force,    
    hsd_reset,    
    out_diag,    
    bar_intens,           
    bicolor_intens,           

    /* Unused pins */
    unused1,
    unused2,
    unused3,
    unused4,
    unused5,
    unused6,
#if ITBM_PJ6_DIRECTION == ITBM_PJ6_OUTPUT 
    unused7, /* PJ6 is unused, make it output */
#endif
    HWO_MAX_OUTPUTS
} eApp_HWO_ID;

typedef struct {
    /* target to data receive\transmit */
    DEVICE_TYPE        Device;
    /* Port definition  */
    UINT8              param1;
    /* Channel/bit number of port  */
    UINT8              param2;
    /* Flag to indicate that the data are stored in configuration table and ready to output */
    ITBM_BOOLEAN       isReceived;
    /* Application Data total size in bytes */
    UINT8              Data_Size;
    /* Initial value of output Application Data */
    AccessRef          pInitValue;
    /* Current value of output Application Data */
    AccessRef          pDATA;
    /* Active output value */
    ITBM_BOOLEAN   Active_Low;
    /* Current error code of output Application Data */
    UINT8              Error_Code;
} ITBM_HWO_TableType;

/******************************************************************************
*   Macro Definitions  
******************************************************************************/
/* Constants and Macros for HW Output Application Data*/
#define ITBM_HWO_ARRAY_ELEMENTS (HWO_MAX_OUTPUTS - HWO_FIRST_ID)
#define IS_OUTPUT(j)  ( ( ( (j) >= HWO_FIRST_ID) && ( (j) < HWO_MAX_OUTPUTS) ) ? ITBM_TRUE : ITBM_FALSE)
/* for accessing data in the mapping tables by the real index */
#define TRUE_HWO_IDX(id)  (id - HWO_FIRST_ID)

/******************************************************************************
*   External Variables
******************************************************************************/
extern ITBM_HWO_TableType ITBM_HWO_Table[ITBM_HWO_ARRAY_ELEMENTS];

/******************************************************************************
*   Global Function Prototypes  
******************************************************************************/

#endif /* MW_CFGM_HWO_CFG_H */
