#ifndef ITBM_TMR_MANAGER_CONFIG_H
#define ITBM_TMR_MANAGER_CONFIG_H
/******************************************************************************
*   (c) Copyright Continental AG 2007
*   All Rights Reserved.
*   Continental AG Confidential Proprietary
*   The Copyright symbol does not also indicate public availability
*   or publication.
*
*   Filename:   mw_tmrm_cfg.c 
*
*   Created_by: V.Shvetsov
*       
*   Date_created: 10/26/2007
*
*   Description:  
*
*
*******************************************************************************
*   Revision History:
*
*   Date          CQ#           Author    
*   MM-DD-YY      oem00000000   Initials   Description of change.
*   -----------   -----------   --------   ------------------------------------
*   10-26-07      oem00023081   VS         Initial version
*   12-11-07      oem00023254   AN         Header files usage is optimized
*   01-23-08      oem00023218   AN         Gain Control application is added
*   03-25-08      oem00023671   LK         EEPROM driver ported from TIPM
*   05-17-08      oem00023971   AN         Fixed issues during debugging on S0
*   06-03-08      oem00024059   AN         Timer for Vehicle Stationary application is added
*   06-18-08      oem00024147   MG         Timer for shutting of the output and blank 
*                                          the display after Fault_Timeout is added
*   06-11-08      oem00024090   AN         CAN bus-off algorithm implemented
*   07-16-08      oem00024224   AN         Obsolete timer is removed
*   07-21-08      oem00024253   VS         CPU utilisation adjustment and issues fixing
*   07-23-08      oem00024285   VS         Issues fixing after S0B release testing
******************************************************************************/

/******************************************************************************
*   Include Files
******************************************************************************/ 
#include "itbm_basapi.h" 

/*******************************************************************************************************
*   Type Definitions
*******************************************************************************************************/
typedef enum
{

    /* Start of 5ms Timers */
/*    TIMER_FIRST_5MS,
        TIMER_TEST_5MS,
    TIMER_LAST_5MS,
*/
    /* Start of 10ms Timers */
    TIMER_FIRST_10MS,
        TIMER_BUS_OFF_10MS,       
    TIMER_LAST_10MS,

    /* Start of 20ms Timers */
    TIMER_FIRST_20MS,
        TIMER_20MS_EE_WRITE_HOLDOFF,
    TIMER_LAST_20MS,

    /* Start of 100ms Timers */
    TIMER_FIRST_100MS,
        TIMER_HW_TEST_WAIT_100MS,              
        TIMER_UTEST_WAIT_100MS,
        TIMER_STATIONARY_DELAY_100MS,
        TIMER_TEST_100MS,              
    TIMER_LAST_100MS,

    /* Start of 1000ms Timers */
    TIMER_FIRST_1000MS,
        TIMER_GAIN_STORE_WAIT_1000MS,
        TIMER_GAIN_BLANK_WAIT_1000MS,
        TIMER_FAULT_TIMEOUT_1000MS,              
        TIMER_TEST_1000MS,              
    TIMER_LAST_1000MS,
   
    NUM_EVENT_TIMERS
} TimerID;

typedef struct {
    UINT16 period;
    UINT16 firstID;
    UINT16 lastID;
} TimersListType;


#define ITBM_TMR_TIMERS_NUMBER 4
#define ITBM_TMR_MAX_TIMER_TIME 1000

#endif

