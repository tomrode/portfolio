#--------------------------------------------------------------------------------------------------------------
#       Linker Configuration File for the CSWM MY10
#	This file is used for the creation of the S record of the CSWM MY10
#       Version: 1.0 
#--------------------------------------------------------------------------------------------------------------

#--------------------------------------------------------------------------------------------------------------
# RAM Space (Size for XS128 is 8K. RPAGE register value = 0xFD after POR)
#--------------------------------------------------------------------------------------------------------------
+seg .bsct -b 0x0fe000 -o 0x2000 -m 0x1dde -n .direct	          # Set start address of RAM. Set the max. size of RAM to 7646 bytes
+seg .safe_ram -b 0x0ffdde -o 0x3dde -n .safe_ram		  # Define segment with secured RAM (can be used for RAM test if we like to)	
# This set up allows stack to have an available size of 512 bytes

#--------------------------------------------------------------------------------------------------------------
# Global data and definitions for crtsx
#--------------------------------------------------------------------------------------------------------------
+seg .gconst -b 0x7e0000 -n .gconst 		# Global constants (not used. Just for the crtsx)
+seg .gbss -a .gconst -n .gbss 			# Global bss (not used. Just for the crtsx)
+seg .fdata -a .gbss -n .fdata			# Place for far data (for Halcogen)
+seg .gdata -a .fdata -n .gdata 	 	# global data start address (not used. Just for the crtsx)

+def __sgdata=start(.gdata)			# start address of global data (Just for the crtsx)
+def __sgbss=@.gbss				# start address of global bss (Just for the crtsx)
+def __sgdflt=__sgdata				# to be defined with +modd (Just for the crtsx)

+def __sbss=@.bss				# start address of bss 
+def __subsct=@.ubsct				# start address of bsct (Just for the crtsx)
+def __sgconst=start(.gconst)			# start address of global page (Just for the crtsx)
+def __sdirect=start(.direct)			# start address of direct page (Just for the crtsx)

#--------------------------------------------------------------------------------------------------------------
# (Page 0xFD: UnBanked) Text Segment 
#--------------------------------------------------------------------------------------------------------------
+seg .vector_boot -b 0x7F4010 -o 0x4010 -w 0x168 -n .vector_boot -m 0x168 -ck        # vector table start address (with boot)
+seg .text -b 0x7f4178 -o 0x4178 -m0x3E88 -n.text -ck                                # Fixed Page 0xFD
+seg .const -a .text -n.const                                                        # Place all const after text segment (automatically included in checksum)
+seg .cksum -a .const -n cksum -ik                                                   # Place ROM check sum segment after const
+def __ROMTstStart=@.cksum                                                           # The start address of the Checksum (used in ROM test)
".\obj\crtsx.o"                                 # Start up code (at 0x4000)
".\obj\main.o"                                  # main
".\obj\mc9s12xs128.o"                           # micro definitions
".\obj\Mcu_Irq.o"                               # MCU Interrupts
".\obj\Gpt_Irq.o"                               # General Purpose Timer Interrupts
".\obj\Pwm_Irq.o"                               # Pulse Width Modulation Interrupts
".\obj\Adc_Irq.o"                               # Analog to Digital Convertion Interrupts
".\obj\can_isr.o"                               # CAN Interrupts
".\obj\ROM_Test.o"                              # ROM Test
".\obj\AD_Filter.o"                             # AD Filter and releated functions

#---------------
# Halcogen Files
#---------------
".\obj\Adc.o"
".\obj\Adc_Cfg.o"
".\obj\AdcIf_Cbk.o"
".\obj\Dio.o"
".\obj\Dio_Cfg.o"
".\obj\EcuM.o"
".\obj\Gpt.o"
".\obj\Gpt_Cfg.o"
".\obj\GptIf_Cbk.o"
".\obj\Mcu.o"
".\obj\Mcu_Cfg.o"
".\obj\Port.o"
".\obj\Port_Cfg.o"
".\obj\Pwm.o"
".\obj\Pwm_Cfg.o"
".\obj\PwmIf_Cbk.o"
".\obj\RamTst.o"
".\obj\RamTst_Algo.o"
".\obj\RamTst_Cfg.o"
".\obj\RamTstIf_Cbk.o"
".\obj\Wdg.o"
".\obj\Wdg_Cfg.o"
".\obj\WdgIf.o"

#--------------------------------------------------------------------------------------------------------------
# This area is reserved for FLASH EMULATED EEPROM (Data Flash: Size for XS128 is 8K. EPAGE register value = 0xFE after POR)
#--------------------------------------------------------------------------------------------------------------
+seg .dflash 	-b 0x100000 -o 0x0800 -m8192 -n.dflash      	   # 8K Emulated eeprom start address (Data Flash)

#--------------------------------------------------------------------------------------------------------------
# (Page 0xF8) Segment: (PPAGE register value should be 0xFE => 0x8000 to 0xBFFF linear space in local map).
#--------------------------------------------------------------------------------------------------------------
+seg .ftext -b 0x7e1000 -o 0x9000 -m 0x3000 -n .page1 -s flash -ck # Page 1 # Start at 0x7e1000 to avoid overlap with .fdata segment
".\obj\Temperature.o"           # Hence, we only have 15K available in this page
".\obj\Internal_Faults.o"
".\obj\FaultMemory.o"
".\obj\FaultMemoryLib.o"
					           				
#--------------------------------------------------------------------------------------------------------------
# (Page 0xF9) Segment: 
#--------------------------------------------------------------------------------------------------------------
+seg .ftext -b 0x7e4000 -o 0x8000 -m 0x4000 -n .page2 -s flash -ck # Page 2
".\obj\appdescdev.o"
#".\obj\AppDesc.o"                                   #not linked
".\obj\can_drv.o"
".\obj\can_par.o"
".\obj\desc.o"
".\obj\drv_par.o"
".\obj\FNmBSl15.o"                                    
".\obj\il.o"
".\obj\il_par.o"
".\obj\nm_par.o"
".\obj\tp_par.o"
".\obj\tpmc.o"
".\obj\v_par.o"
".\obj\vstdlib.o"
".\obj\sip_vers.o"
".\obj\xcan_drv.o"

#--------------------------------------------------------------------------------------------------------------
# (Page 0xFA) Segment: Application segment 1
#--------------------------------------------------------------------------------------------------------------
+seg .ftext -b 0x7e8000 -o 0x8000 -m 0x4000 -n .page3 -s flash -ck # Page 3
".\obj\Canio.o"
".\obj\Network_Management.o"
".\obj\BattV.o"
".\obj\Relay.o"
".\obj\Pwm_Stagger.o"

#--------------------------------------------------------------------------------------------------------------
# (Page 0xFB) Segment: Application segment 2
#--------------------------------------------------------------------------------------------------------------
+seg .ftext -b 0x7ec000 -o 0x8000 -m 0x4000 -n .page4 -s flash -ck # Page 4
".\obj\DiagMain.o"
".\obj\DiagRead.o"
".\obj\DiagSec.o"
".\obj\DiagWrite.o"
".\obj\DiagIOCtrl.o"
".\obj\DiagRtnCtrl.o"

#--------------------------------------------------------------------------------------------------------------
# (Page 0xFC) Segment: Application segment 3
#--------------------------------------------------------------------------------------------------------------
+seg .ftext -b 0x7f0000 -o 0x8000 -m 0x4000 -n .page5 -s flash -ck # Page 5
".\obj\Heating.o"
".\obj\Venting.o"
".\obj\StWheel_Heating.o"

#--------------------------------------------------------------------------------------------------------------
# (Page 0xFE) Segment: Application segment 3
#--------------------------------------------------------------------------------------------------------------
+seg .APPLSWHDR -b 0x7F8000 -o 0x8000 -w 0x6A -n .APPLSWHDR  -m 0x6A -ck  # APPL SW address	
+seg .APPLDATAHDR -b 0x7F8400 -o 0x8400 -w 0x2D -n .APPLDATAHDR -m 0x2D -ck  # APPL DATA address	
+seg .ftext -b 0x7f8800 -o 0x8800 -m 0x3200 -n .page6 -s flash -ck # Page 6

#---------------
# FLASH Driver Files
#---------------
".\obj\hwio_eeprom.o"
".\obj\hwio_eeprom_cfg.o"
".\obj\hwio_eeprom_direct.o"
".\obj\memfunc.o"
".\obj\mw_eem.o"
".\obj\mw_tmrm.o"
".\obj\FblIf.o"             # FBlIf - Flash Boot Interface

#--------------------------------------------------------------------------------------------------------------
# (Page 0xFF: UnBanked) 0x7fc000 to 0x7fffff is reserved for Bootloader
#--------------------------------------------------------------------------------------------------------------

#--------------------------------------------------------------------------------------------------------------
# Vector Segment and its start address
#--------------------------------------------------------------------------------------------------------------
+seg .vector -b 0x7FFF10 -o 0xff10 -ck        		   # vector table start address
".\obj\vector.o"

#--------------------------------------------------------------------------------------------------------------
# Libraries
#--------------------------------------------------------------------------------------------------------------
libi.x12                # C Library (if needed)
libm.x12                # Machine Library

#--------------------------------------------------------------------------------------------------------------
# Other Defitinions
#--------------------------------------------------------------------------------------------------------------
+def __memory=@.bss				# end of bss
+def __eubsct=@.ubsct				# end of ubsct
+def __egbss=@.gbss				# end of global bss
+def __stack=0x4000				# stack pointer initial value. This sets available stack size to 0.5k = 512 bytes

# Register Address Definitions for crtsx
+def _GPAGE=0x0010				# GPAGE register address
+def _DIRECT=0x0011				# DIRECT register address
+def _PPAGE=0x0015				# PPAGE register address
+def _RPAGE=0x0016				# RPAGE register address
