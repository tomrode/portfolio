#define DIAGMAIN_C

/*****************************************************************************************************
*  (c) Copyright 2011 Continental Automotive Systems, all rights reserved
*
*  All material contained herein is CONTINENTAL CONFIDENTIAL and PROPRIETARY.
*  Any reproduction of this material without written consent from 
*  Continental AG is strictly forbidden.                                  
*
*  Filename:     diagmain.c
*
*  Created_by:   Harsha Yekollu
*
*  Date_created: 01/19/2011
*
*  Description:  Main file for UDS Diagnostics. 
*
*  Revision_history:
*
*  MKS ID       Author         Date      Description of Change(s)
*  --------  -------------  --------  --------------------------------------
*  xxxxxxx   H. Yekollu     01/19/11  Initial creation for FIAT CUSW MY12 Program
*  xxxxxxx   H. Yekollu     01/19/11  
*  75458	 H. Yekollu     06/15/11   Updated with new DDT (60-00-005)
* 									   SW Version updated with 11.25.00		 
*  81827	 H. Yekollu		08/22/11   Updated with new DDT (60-00-006). SW Version with 11.34.00
* 									   Part Number stays with AA. 	 
*  75322	 H. Yekollu	    10/06/11   Cleanup the code.
*  86223	 H. Yekollu		10/12/11   DDT 60-00-009 Updates. 
*  xxxxx     H. Yekollu		12/08/11   SW Release AC updated with 00 08 release
*  98062     H. Yekollu     02/13/12   SW Release AD updated with 00 09 release
*                                      Update Parameters and release info for AD 4.0 release
* 103556     C. Cortes      05/03/12   SW Release AE updated with 00 10 release
******************************************************************************************************/
/*****************************************************************************************************
* 									DIAGMAIN
*  This module handles the UDS Services. Except the Diagnostic Trouble Code part.
*  When ever the external test tool requests any data(Except DTC information) from the module,
*  respective callback function gets called. The callback function handling will be done here.
* 
* Though all the detailed functionalities sub devided into other files, few of the services processed
* in this module.
* 
* 1. All callback services processed here (Except DTC related). Processing sub services sub-devided into other files.
* 2. ECU Reset complete processing done here.
* 3. Part Number and Version Info logic Implemented here.
*****************************************************************************************************/

/*****************************************************************************************************
*     								Include Files
******************************************************************************************************/
/* CAN_NWM Related Include files */
#include "il_inc.h"
#include "v_inc.h"
#include "desc.h"
#include "appdesc.h"

#include "TYPEDEF.h"
#include "main.h"
#include "DiagMain.h"
#include "DiagSec.h"
//#include "DiagRead.h"
#include "DiagWrite.h"
#include "DiagIOCtrl.h"
#include "canio.h"
#include "FaultMemory.h"
#include "FaultMemoryLib.h" 
//#include "Rom_Test.h"


#include "heating.h"
#include "Venting.h"
//#include "StWheel_Heating.h"
//#include "Temperature.h"
//#include "Internal_Faults.h"
#include "AD_Filter.h"
//#include "BattV.h"
#include "mw_eem.h"
#include "mc9s12xs128.h"
#include "fbl_def.h"

/* Autosar include Files */
#include "Mcu.h"
//#include "Port.h"
//#include "Dio.h"
//#include "Gpt.h"
//#include "Pwm.h"
//#include "Adc.h"
//#include "Wdg.h"
/*****************************************************************************************************
*     								EXTERNAL FUNCTION CALLS
******************************************************************************************************/
extern unsigned char /*EE_DRIVER_CALL */EE_IsBusy (void);
/* Main */
extern void near main(void);

//extern void diagSecF_RequestSeed(enum DIAG_RESPONSE* Response, u_temp_l* DataLen, DescMsgContext* pMsgContext);
//extern void diagSecF_ProcessKey(enum DIAG_RESPONSE* Response, u_temp_l* DataLen, DescMsgContext* pMsgContext);

/*****************************************************************************************************
*     								LOCAL ENUMS
******************************************************************************************************/
enum RESET_REQUEST
{
 NO_RESET_REQUEST = 0x00,
 RESET_REQUEST = 0x22,
 PREPARE_RESET_REQUEST = 0x55,
 BOOTMODE_REQUEST = 0xAA
};

enum RESET_REQUEST d_ResetReq_t;
static enum DIAG_RESPONSE Response;
/*****************************************************************************************************
*     								LOCAL #DEFINES
******************************************************************************************************/
//#define HARD_RESET                0x01 //Not used PC Lint Info 750 10/06/2011

#define D_FBL_BOOTMODE_VAL_K      0xC3 //KfblReProgRequested.

/* Software Version Defines */
#define SW_YEAR						0x00    // Main Release.
#define SW_WEEK						0x0A    // Version.	 MKS 98062 
//#define SW_PATCH					00    // CUSW //Not used PC Lint Info 750 10/06/2011

/*****************************************************************************************************
*     								LOCAL VARIABLES
******************************************************************************************************/
union char_bit diag_partno_updates_c;
#define sw_partno_update_b       diag_partno_updates_c.b.b0
#define ecu_partno_update_b      diag_partno_updates_c.b.b2
#define sw_version_update_b      diag_partno_updates_c.b.b3

/* Shows which write mode service we are in..0-No writes */
unsigned char diag_writedata_mode_c;
u_temp_l DataLength;
//unsigned char d_PassiveModeState_c;

unsigned char diag_def_ecu_pno[10];
unsigned char diag_def_sw_pno[10];

/*****************************************************************************************************
*     								LOCAL FUNCTION CALLS
******************************************************************************************************/
//@far void DESC_API_CALLBACK_TYPE ApplDescInitPowerOn(void);
/* This function monitors contineously for any reset requests are present, if so resets the module. */
void diagLF_CheckResetRequest(void);
/* This function first updates the latest Part Number info to Application defaults.                 */
/* If actual PNo are differ from defaults, this function copies the appl defaults to actual values. */
/* The same way SW Version info gets updated                                                        */
void diagLF_PartNo_SwVer_Update(void);
void diagLF_SessionTimeout(void);
extern @far void diagSecF_DefSession_Init(void);
extern @far void diagF_DiagResponse(enum DIAG_RESPONSE Response, u_temp_l Length, DescMsgContext* pMsgContext);
/*****************************************************************************************
*   							ApplDescInitPowerOn  								
*****************************************************************************************/
//@far void DESC_API_CALLBACK_TYPE ApplDescInitPowerOn(void)
//{
//	  u_8Bit Eep_resetRspns = 0;	//  stores the value of kEepAddressProgReqFlags
//	  //ProgRequestFlag = kDescFblReprogReqNone;
//	  
//	  /* Read reset response flag */
//	  //EE_BlockRead(EE_FBL_RESETRESPONSE_FLAG, &Eep_resetRspns); 	  
//	  
//	  /* Check Reset Response Type */
//	  switch (Eep_resetRspns)
//	  {  	  	
//	    /* ECU Reset Response */
//	  	case 0x01: /*RESET_RESPONSE_ECURESET_REQUIRED:*/
//	      /* Reset response flag and send response */
//	  	  diag_load_def_eemap_b = TRUE; //Time to update the default eep parameters
//	  	  //diag_load_bl_eep_backup_b = TRUE; //Time to take backup copy of boot
//	      Eep_resetRspns = 0x03;/*RESET_RESPONSE_NOT_REQUIRED;*/
//	      //EE_BlockWrite(EE_FBL_RESETRESPONSE_FLAG, &Eep_resetRspns);
//	      /*Function not available in FIAT Desc.c*/
//        //DescSendPosRespFBL(kDescSendFblPosRespEcuHardReset);	      
//	      break;
//	    /* SDS (what does SDS mean?) Response */
//	    case 0x02: /*RESET_RESPONSE_SDS_REQUIRED:*/
//	      /* 02.11.2009: We saw kEepAddressResetResponseFlag = 0x01 in the backup copy (module return form the Buck) */
//	      //diag_load_bl_eep_backup_b = TRUE; 	// Set the flag tp update the backup copy of boot
//	      /* Reset response flag and send response */
//	      Eep_resetRspns = 0x03;/*RESET_RESPONSE_NOT_REQUIRED;*/
//	      //EE_BlockWrite(EE_FBL_RESETRESPONSE_FLAG, &Eep_resetRspns);
//	      /*Function not available in FIAT Desc.c*/
//        //DescSendPosRespFBL(kDescSendFblPosRespDscDefault);
//	      break;
//	    default:
//	    	// Response is not required
//
//	      break;
//	  }
//}

/***************************************************************************************
 *  FUNCTION:       dumainF_Init                                                       *
 *  FILENAME:       M:\SCM_DCX_CSWM_MY_12\Application_Paged\src\diagmain.c             *
 *  PARAMETERS:     (none)                                                             *
 *  DESCRIPTION:    Use for initializing diagmain module                               *
 *                  This function gets called only once at the module start up.        *
 *  RETURNS:        (none)                                                             *
 ***************************************************************************************/
@far void diagF_Init(void)
{
	d_ResetReq_t = NO_RESET_REQUEST;
	//Needs to clearIO..Function to be written to clear all the IO diagnsotic data including the lock bits
	diagLF_SessionTimeout();

	diagSecF_Main_Init ();

	/* By default enable the communication to be Active */
	/* By each Power On Reset Coomunication will be active. */
	//d_PassiveModeState_c = kDescPassiveModeDisable;
	DescInitPowerOn(kDescPowerOnInitParam);
	
	/* Place a call to ApplDescInitPowerOn to check Reset Response Flag */
	//ApplDescInitPowerOn();
}

/*  *****************************************************************************************
 * Function name:ApplDescCheckSessionTransition												*
 * Description:Check if the given session transition is allowed or your ECU is currently not*
 * able to perform it.																		*
 * Returns:  nothing																		*
 * Parameter(s):																			*
 *   - newState:																			*
 *       - Contains the new state in which the state group will be set.						*
 *       - Access type: read																*
 *   - formerState:																			*
 *       - Contains the current state of the state group.									*
 *       - Access type: read																*
 * Particularitie(s) and limitation(s):														*
 *   - The function "DescSessionTransitionChecked" may be called.							*
 *   - The function "DescSetNegResponse" may be called.										*
 *******************************************************************************************/
void DESC_API_CALLBACK_TYPE ApplDescCheckSessionTransition(DESC_CONTEXT_FORMAL_PARAM_DEF_FIRST DescStateGroup newState, DescStateGroup formerState)
{
	/* Local variable(s) */
	u_8Bit diag_PCBTemp_stat_c = 0;        // Stores the PCB temperature status
	u_8Bit diag_Cswm_actvtn_stat = 0;	   // Stored CSWM output activation status
	u_8Bit diag_PCB_overtemp_stat = 0;	   // stores PCB over temperature status
	u_8Bit temp_BootSW_verStatus = 0;	   // stores bootloader version status (valid or not valid)
	
	/* is the new transition state ECU Programming? */
	if (newState == kDescStateSessionProgramming)
	{
	
		/* New version */
		d_ResetReq_t = BOOTMODE_REQUEST;
		//ECU_Flash_Reprogramming Session. To be filled the requirements
		diag_session_active_b = 1;
		//Added Newly.As we did not complete the process before going to reset, issue response pending at this moment
		DescSetNegResponse(kDescNrcResponsePending);
		DescProcessingDone();
		
	}
	else
	{
		DescSessionTransitionChecked(DESC_CONTEXT_FORMAL_PARAM_DEF_FIRST);
	}
}

/*  *********************************************************************************
 * Function name:ApplDescOnTransitionSession										*
 * Description:Notification function for state change of the given state group,		* 
 * defined by CANdelaStudio.														*
 * Returns:  nothing																*
 * Parameter(s):																	*
 *   - newState:																	*
 *       - The state which will be set.												*
 *       - Access type: read														*
 *   - formerState:																	*
 *       - The current state of this state group.									*
 *       - Access type: read														*
 * Particularitie(s) and limitation(s):												*
 *   - The function "DescProcessingDone" may not be called.							*
 *   - The function "DescSetNegResponse" may not be called.							*
 ***********************************************************************************/
void DESC_API_CALLBACK_TYPE ApplDescOnTransitionSession(DescStateGroup newState, DescStateGroup formerState)
{
	switch (newState) {
		case kDescStateSessionDefault:
		{
			/* Default Session. TO be filled the requirements like IO should be cleared, DTC disable criteira to be cleared etc..*/
			diag_session_active_b = 0;
			//Needs to clearIO..Function to be written to clear all the IO diagnsotic data including the lock bits
			diagLF_SessionTimeout();
			
			diagSecF_DefSession_Init ();
			
			break;
		}
		case kDescStateSessionProgramming:
		{
			//ECU_Flash_Reprogramming Session. To be filled the requirements
			diag_session_active_b = 1;
			/* Requested for Flash programming. We will be going from here to boot.                        */
			/* Set the security access state to ECU_POWERUP_STATE and clear the Security access unlock bit */
			diag_sec_access1_state_c = ECU_POWERUP_STATE;
			sec_acc_unlock_b = 0;
			d_ResetReq_t = BOOTMODE_REQUEST;
			break;
		}
		case kDescStateSessionExtendedDiagnostic:
		{
			diagSecF_ExtSession_Init ();
			break;
		}
		default:
		{
			/*Lint Info 744: switch statement has no default */
		}
	}
}

/*  ********************************************************************************
 * Function name:ApplDescResetHardReset (Service request header:$11 $1 )
 * Description: not available 

 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescResetHardReset(DescMsgContext* pMsgContext)
{ 
  
	u_temp_l SubFunctionRequest;
  
  /* Set RESET request */
	d_ResetReq_t = PREPARE_RESET_REQUEST;
  SubFunctionRequest = pMsgContext->reqData[0];
  /*Response Value */
  pMsgContext->resData[0] = 0x01;
  /*"Response Length */
  pMsgContext->resDataLen = 1;
  
  /* Supporting "Response Requiered" and "Response NOT Requiered" */
  if(SubFunctionRequest & D_NO_POSITIV_RESPONSE_REQUIRED_K)
  {
      /*"Response NOT Requiered" */ 
  }
  else
  { 
      /*"Response Requiered" */
      Response = RESPONSE_OK;
  }  
  diagF_DiagResponse(Response, pMsgContext->reqDataLen, pMsgContext); 
}	

/*  ********************************************************************************
 * Function name:ApplDescProcessReadMemoryByAddress (Service request header: )
 * Description: not available 
 * Returns:  nothing
 ***********************************************************************************/
//void DESC_API_CALLBACK_TYPE ApplDescProcessReadMemoryByAddress(DescMsgContext* pMsgContext)
//{
//
//	unsigned char *pChar;
//	/*Lint Error 64: Type mismatch diagF_DiagResponse (arg. no. 1) (enum DIAG_RESPONSE = unsigned char) */
//	//unsigned char Response;
//
//	DataLength = pMsgContext->reqData[3];
//
//	/* Lint Info 826: Suspicious pointer-to-pointer conversion (area too small) */
//	/* MKS 44695 Unable to read PCB Serial Number at EOL. */
//	pChar = (unsigned char *)(*(unsigned int *)(&pMsgContext->reqData[1]));
//	//pChar = (unsigned char *)(&pMsgContext->reqData[1]);
//
//	memcpy( &pMsgContext->reqData[3],pChar,pMsgContext->reqData[0]);
//
//	Response = RESPONSE_OK;
//
//	diagF_DiagResponse(Response, DataLength, pMsgContext);
//
//}

/*  ********************************************************************************
 * Function name:ApplDescProcessWriteMemoryByAddress (Service request header: )
 * Description: not available 
 * Returns:  nothing
 ***********************************************************************************/
//void DESC_API_CALLBACK_TYPE ApplDescProcessWriteMemoryByAddress(DescMsgContext* pMsgContext)
//{
//	unsigned char i;
//	/*Lint Error 64: Type mismatch diagF_DiagResponse (arg. no. 1) (enum DIAG_RESPONSE = unsigned char) */
//	//unsigned char Response;
//
//	/* Check to see any diagnostic writes are pending. If pending just return Conditions are not correct. */
//	if (!diag_writedata_mode_c)
//	{
//		/* Copy the data just received from External tool to the local variable */
//		for ( i = 0; i < 10; i++ )
//		{
//			/* $/$* Get all the, to be modified contents from External tool to Local variable $*$/ */
//			diag_temp_buffer_c[ i ] = pMsgContext->reqData[ i ];
//		}
//		/* Before proceeding to the next step, check to see the user got the security access to write the contents. If yes proceed to the logic,else */
//		/* Check to see the values we got from user are in range or not. If values are in range proceed to the next step. Else */
//		if ( hsF_CheckDiagnoseParameter( ECU_WRITE_MEMORY, &diag_temp_buffer_c[ 0 ] ))
//		{
//			/* Data is OK Set the flag to write contents into EEPROM. Based on this flag the cyclic task will be executed */
//			diag_writedata_b = 1;
//			diag_writedata_mode_c = ECU_WRITE_MEMORY;
//			Response = RESPONSE_OK;
//		}
//		else
//		{
//			/* User sent wrong data. do not proceed to write. Return Request out of range negative response. */
//			diag_writedata_b = 0;
//			diag_writedata_mode_c = 0;
//			DataLength = 1;
//			Response = REQUEST_OUT_OF_RANGE;
//		}
//	}
//	else
//	{
//		/* Other diagnostic writes are pending. Return Conditions not correct response to the tester tool. */
//		DataLength = 1;
//		Response = CONDITIONS_NOT_CORRECT;
//	}
//	diagF_DiagResponse(Response, DataLength, pMsgContext);
//}

/* Check the communication control activity */
/* ********************************************************************************
 * Function name:ApplDescCheckCommCtrl
 * Description:Check if the requested communication manipulation is possible to be performed by the ECU.
 * Returns:  nothing
 * Parameter(s):
 *   - iContext:
 *       - Diagnostic request handle used only in multi-context system (kDescNumContexts > 1).
 *       - Access type: read
 *   - commControlInfo->subNetTxNumber:
 *       - The application shall use this parameter to decied on which physical channels the communiaction will be manipulated.
 *       - Access type: read
 *   - commControlInfo->commCtrlChannel:
 *       - The application determines on which channel the communication will be manipulated (kDescCommControlCanCh01 - kDescCommControlCanCh14).
 *       - Access type: write
 *   - commControlInfo->rxPathState:
 *       - Activity type: kDescCommControlStateEnable - enables the communication; kDescCommControlStateDisable - disables it.
 *       - Access type: read
 *   - commControlInfo->txPathState:
 *       - Activity type: kDescCommControlStateEnable - enables the communication; kDescCommControlStateDisable - disables it.
 *       - Access type: read
 *   - commControlInfo->msgTypes:
 *       - Message group: kDescCommControlMsgAppl - application; kDescCommControlMsgNm - NM; 
 *       - Access type: read
 *   - commControlInfo->reqCommChannel:
 *       - The current CAN channel on which the CommControl request is received.
 *       - Access type: read
 * Particularitie(s) and limitation(s):
 *   - Call "DescCommCtrlChecked" (here or later) to confirm the check.
 *   - Call "DescSetNegResponse" to reject the service.
 ******************************************************************************** */
void DESC_API_CALLBACK_TYPE ApplDescCheckCommCtrl(DESC_CONTEXT_FORMAL_PARAM_DEF_FIRST DescOemCommControlInfo *commControlInfo)
{
  /*<<TBD>> Remove this comment once you have completely implemented this function!!!*/

#if defined (DESC_ENABLE_COMM_CTRL_SUBNET_SUPPORT)
  switch(commControlInfo->subNetNumber)
  {
    case kDescCommControlSubNetNumAll:/* Enable/Disable all comm channels (CAN, LIN, etc.)*/
      /* Nothing to do - later the ApplDescSetCommMode will enable/disbale the LIN channels if needed. */
      break;
    case kDescCommControlSubNetNumRx:/* Enable/Disable only the request RX comm channel (CAN)*/
      /* Nothing to do - CANdesc handles the CAN channels */
      break;
    default:
      /* Dispatch subnetworks */
      /* Assumed subnet to CAN mapping: 
       * SubNet 1: LIN 
       * SubNet 2: CAN1 
       * SubNet 3: CAN2 
       */
      switch(commControlInfo->subNetNumber)
      {
        case kDescCommControlSubNetNum01:
          /* LIN network */
          commControlInfo->commCtrlChannel = kDescCommControlCanChNone;
          break;
/* The part is only needed if a multi CAN system is designed !!! */
# if defined(DESC_ENABLE_MULTI_CHANNEL_SUPPORT)
        case kDescCommControlSubNetNum02:
          /* CAN1 */
          commControlInfo->commCtrlChannel = kDescCommControlCanCh01;
          break;
        case kDescCommControlSubNetNum03:
          /* CAN2 */
          commControlInfo->commCtrlChannel = kDescCommControlCanCh02;
          break;
# endif
        default:
          /* Subnet numbers out of range */
          DescSetNegResponse(DESC_CONTEXT_PARAM_FIRST kDescNrcRequestOutOfRange);
          break;
      }
  }
#endif
  /* Confirm the communication status will be accepted. 
   * This can be done later by storing the iContext parameter (if exists) globaly to be able to give the correct acknowledgment.
   */
  DescCommCtrlChecked(DESC_CONTEXT_PARAM_ONLY);
}

/* ApplDescOnCommunicationDisable	*/
/* Notification function. Just for our testing disable this flag */
/* When comm is enabled, this flag will be set to ZERO always */
/* When comm is disabled, this flag will be set to ONE untill comm enables*/
void DESC_API_CALLBACK_TYPE ApplDescOnCommunicationDisable(void)
{
	comn_ctrl_disable_b = TRUE;
}

/* ApplDescOnCommunicationEnable	*/
/* Notification function. Just for our testing disable this flag */
/* When comm is enabled, this flag will be set to ZERO always */
/* When comm is disabled, this flag will be set to ONE untill comm enables*/
void DESC_API_CALLBACK_TYPE ApplDescOnCommunicationEnable(void)
{
	comn_ctrl_disable_b = FALSE;
}

#if defined (DESC_ENABLE_DEBUG_USER ) || defined (DESC_ENABLE_DEBUG_INTERNAL)
# if defined (CCL_ENABLE_ERROR_HOOK) || defined (CCLCOM_ENABLE_ERROR_HOOK)
/* CCL takes care about this function */
# else
/* ********************************************************************************
 * Function name:ApplDescFatalError
 * Description: This function will be called each time while the debug mode is active a
 * CANdesc fault has been detected. If you reach this function it makes no sence to continue the tests since CANdesc
 * will not operate properly until next start of the ECU.
 * Returns:  nothing
 * Parameter(s):
 *   - errorCode:
 *       - The assert code text equivalent can be found in desc.h (kDescAssert....).
 *       - Access type: read
 *   - lineNumber:
 *       - Since the same fault could be cales on many places the line number shows where exactly it occured.
 *       - Access type: read
 * Particularitie(s) and limitation(s):
 *   - Set a break point at this place to know during the ECU development if you ran onto it.
 ******************************************************************************** */
void DESC_API_CALLBACK_TYPE ApplDescFatalError(vuint8 errorCode, vuint16 lineNumber)
{
  /*<<TBD>> Remove this comment once you have completely implemented this function!!!*/
  /* Avoid warnings */
  DESC_IGNORE_UNREF_PARAM(errorCode);
  DESC_IGNORE_UNREF_PARAM(lineNumber);

  /* When fatal error occurs, cause an ECU hang up at this point.
   * Please set break point at this line to investigate both parameter values. */
  for(;;);
}
# endif
#endif

/*  ********************************************************************************
 * Function name:ApplDescRcrRpConfirmation
 * Description:Will be called only if "DescForceRcrRpResponse" has been previously called.
 * Returns:  nothing
 * Parameter(s):
 *   - status:
 *       - Current RCR-RP transmission status (kDescOk/kDescFailed).
 *       - Access type: read
 * Particularitie(s) and limitation(s):
 *   - The function "DescProcessingDone" may be called to close the service processing.
 *   - The function "DescSetNegResponse" may be called to set a negative response.
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescRcrRpConfirmation(vuint8 status)
{
  /* <<TBD>> Remove this comment once you have completely implemented this function!!! */
    /* Avoid warnings */
  DESC_CONTEXT_PARAM_DUMMY_USE;

  /* Check the transmission status */
  if(status == kDescOk)
  {
    /* "Response Pending" was just successfully sent, you can perform any further action knowing that
     * from now on the tester has restarted its response timeout timer and set the next time to P2* (P2Ex) 
     */
  }
  else
  {
    /* There was some transmission error and the tester didn't received the RCR-RP response. You can decide to go on
     * or to reset the application activity you have started and wait for a new request from the tester. 
     */
  }
;
}

/*  ********************************************************************************
 * Function name:ApplDescRequestDownload (Service request header:$34 )
 * Description:WARNING !!!! The configuration parameters have to be updated according to
 * application.
 * Returns:  nothing
 * Parameter(s):
 *   - pMsgContext->reqData:
 *       - Points to the first service request data byte.
 *       - Access type: read
 *   - pMsgContext->resData:
 *       - Points to the first writeable byte for the service response data.
 *       - Access type: read/write
 *   - pMsgContext->reqDataLen:
 *       - Contains the count of the service request data bytes (Sid is excluded).
 *       - Access type: read
 *   - pMsgContext->resDataLen:
 *       - Must be initialized with the count of the service response data bytes (Sid is excluded).
 *       - Access type: read/write
 *   - pMsgContext->msgAddInfo.reqType:
 *       - Shows the addressing type of the request (kDescPhysReq or kDescFuncReq).
 *       - Access type: read
 *   - pMsgContext->msgAddInfo.resOnReq:
 *       - Indicates if there will be response. Allowed only to write only 0 !!!
 *       - Access type: read/write
 *   - pMsgContext->msgAddInfo.suppPosRes:
 *       - UDS only!If set no positive response will be sent on this request.
 *       - Access type: read
 * Particularitie(s) and limitation(s):
 *   - The function "DescProcessingDone" must be called from now on (within this
 * main-handler or later).
 *   - The function "DescSetNegResponse" can be called within this main-handler or later
 * but before calling "DescProcessingDone".
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescRequestDownload(DescMsgContext* pMsgContext)
{
//  /* <<TBD>> Remove this comment once you have completely implemented this function!!! */
//  /* Dummy example how to access the request data. */
//  /* Assumming the FIRST DATA byte contains important data which has to be less than a constant value. */
//  if(pMsgContext->reqData[0] < 0xFF)
//  {
//    /* Received data is in range process further. */
//    /* Dummy example of how to write the response data. */
//    pMsgContext->resData[0] = 0xFF;
//    /* Always set the correct length of the response data. */
//    pMsgContext->resDataLen = 3;
//  }
//  else
//  {
//    /* Request contains invalid data - send negative response! */
//    DescSetNegResponse(kDescNrcRequestOutOfRange);
//  }
  /* User service processing finished. */
  DescProcessingDone();
}


/*  ********************************************************************************
 * Function name:ApplDescTransferDataDownload (Service request header:$36 )
 * Description:WARNING !!!! The configuration parameters have to be updated according to
 * application.
 * Returns:  nothing
 * Parameter(s):
 *   - pMsgContext->reqData:
 *       - Points to the first service request data byte.
 *       - Access type: read
 *   - pMsgContext->resData:
 *       - Points to the first writeable byte for the service response data.
 *       - Access type: read/write
 *   - pMsgContext->reqDataLen:
 *       - Contains the count of the service request data bytes (Sid is excluded).
 *       - Access type: read
 *   - pMsgContext->resDataLen:
 *       - Must be initialized with the count of the service response data bytes (Sid is excluded).
 *       - Access type: read/write
 *   - pMsgContext->msgAddInfo.reqType:
 *       - Shows the addressing type of the request (kDescPhysReq or kDescFuncReq).
 *       - Access type: read
 *   - pMsgContext->msgAddInfo.resOnReq:
 *       - Indicates if there will be response. Allowed only to write only 0 !!!
 *       - Access type: read/write
 *   - pMsgContext->msgAddInfo.suppPosRes:
 *       - UDS only!If set no positive response will be sent on this request.
 *       - Access type: read
 * Particularitie(s) and limitation(s):
 *   - The function "DescProcessingDone" must be called from now on (within this
 * main-handler or later).
 *   - The function "DescSetNegResponse" can be called within this main-handler or later
 * but before calling "DescProcessingDone".
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescTransferDataDownload(DescMsgContext* pMsgContext)
{
//  /* <<TBD>> Remove this comment once you have completely implemented this function!!! */
//  /* The application must check the requested data length. */
//  if(pMsgContext->reqDataLen <= (DescMsgLen)0x0FFF)
//  {
//    /* Dummy example how to access the request data. */
//    /* Assumming the FIRST DATA byte contains important data which has to be less than a constant value. */
//    if(pMsgContext->reqData[0] < 0xFF)
//    {
//      /* Received data is in range process further. */
//      /* Dummy example of how to write the response data. */
//      pMsgContext->resData[0] = 0xFF;
//      /* Always set the correct length of the response data. */
//      pMsgContext->resDataLen = 1;
//    }
//    else
//    {
//      /* Request contains invalid data - send negative response! */
//      DescSetNegResponse(kDescNrcRequestOutOfRange);
//    }
//  }
//  else
//  {
//    /* Request length invalid - send negative response! */
//    DescSetNegResponse(kDescNrcInvalidFormat);
//  }
  /* User service processing finished. */
  DescProcessingDone();
}


/*  ********************************************************************************
 * Function name:ApplDescReqTransferExitDownload (Service request header:$37 )
 * Description:WARNING !!!! The configuration parameters have to be updated according to
 * application.
 * Returns:  nothing
 * Particularitie(s) and limitation(s):
 *   - The function "DescProcessingDone" must be called from now on (within this
 * main-handler or later).
 *   - The function "DescSetNegResponse" can be called within this main-handler or later
 * but before calling "DescProcessingDone".
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescReqTransferExitDownload(DescMsgContext* pMsgContext)
{
  /* <<TBD>> Remove this comment once you have completely implemented this function!!! */
  /* Contains no request data */
  /* Contains no response data */
  /* User service processing finished. */
  DescProcessingDone();
}


/*  ********************************************************************************
 * Function name:ApplDescTester_PrresentTesterPresent (Service request header:$3E $0 )
 * Description:If the External Tool sends $3E $80  non response will be sent by ECU.
 * Returns:  nothing
 * Parameter(s):
 *   - pMsgContext->reqData:
 *       - Points to the first service request data byte.
 *       - Access type: read
 *   - pMsgContext->resData:
 *       - Points to the first writeable byte for the service response data.
 *       - Access type: read/write
 *   - pMsgContext->reqDataLen:
 *       - Contains the count of the service request data bytes (Sid is excluded).
 *       - Access type: read
 *   - pMsgContext->resDataLen:
 *       - Must be initialized with the count of the service response data bytes (Sid is excluded).
 *       - Access type: read/write
 *   - pMsgContext->msgAddInfo.reqType:
 *       - Shows the addressing type of the request (kDescPhysReq or kDescFuncReq).
 *       - Access type: read
 *   - pMsgContext->msgAddInfo.resOnReq:
 *       - Indicates if there will be response. Allowed only to write only 0 !!!
 *       - Access type: read/write
 *   - pMsgContext->msgAddInfo.suppPosRes:
 *       - UDS only!If set no positive response will be sent on this request.
 *       - Access type: read
 * Particularitie(s) and limitation(s):
 *   - The function "DescProcessingDone" must be called from now on (within this
 * main-handler or later).
 *   - The function "DescSetNegResponse" can be called within this main-handler or later
 * but before calling "DescProcessingDone".
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescTester_PrresentTesterPresent(DescMsgContext* pMsgContext)
{
  /* <<TBD>> Remove this comment once you have completely implemented this function!!! */
  /* Contains no request data */
  /* Contains no response data */
  /* User service processing finished. */
  DescProcessingDone();
}



/* ********************************************************************************
 * Function name:ApplDescSetCommMode
 * Description:Manipulate application specific channels (LIN, MOST, etc.)
 * Returns:  nothing
 * Parameter(s):
 *   - commControlInfo->subNetTxNumber:
 *       - The application shall use this parameter to decied on which physical channels the communiaction will be manipulated.
 *       - Access type: read
 *   - commControlInfo->commCtrlChannel:
 *       - The application determines on which channel the communication will be manipulated (kDescCommControlCanCh01 - kDescCommControlCanCh14).
 *       - Access type: write
 *   - commControlInfo->rxPathState:
 *       - Activity type: kDescCommControlStateEnable - enables the communication; kDescCommControlStateDisable - disables it.
 *       - Access type: read
 *   - commControlInfo->txPathState:
 *       - Activity type: kDescCommControlStateEnable - enables the communication; kDescCommControlStateDisable - disables it.
 *       - Access type: read
 *   - commControlInfo->msgTypes:
 *       - Message group: kDescCommControlMsgAppl - application; kDescCommControlMsgNm - NM; 
 *       - Access type: read
 *   - commControlInfo->reqCommChannel:
 *       - The current CAN channel on which the CommControl request is received.
 *       - Access type: read
 * Particularitie(s) and limitation(s):
 *   - The function "DescProcessingDone" may not be called.
 *   - The function "DescSetNegResponse" may not be called.
 ******************************************************************************** */
void DESC_API_CALLBACK_TYPE ApplDescSetCommMode(DescOemCommControlInfo *commControlInfo)
{
  switch(commControlInfo->subNetNumber)
  {
    case kDescCommControlSubNetNumAll:/* Enable/Disable all comm channels (CAN, LIN, etc.)*/
      /* !!! Process application specific channels (e.g. LIN) CAN are already enabled/disabled by CANdesc */
      break;
    case kDescCommControlSubNetNumRx:/* Enable/Disable only the request RX comm channel (CAN)*/
      /* Nothing to do - CANdesc handles the CAN channels */
      break;
    default:
      /* Dispatch subnetworks */
      /* Assumed subnet to CAN mapping: 
       * SubNet 1: LIN 
       * SubNet 2: CAN1 
       * SubNet 3: CAN2 
       */
      switch(commControlInfo->subNetNumber)
      {
        case kDescCommControlSubNetNum01:
          /* LIN network */
          /* !!! Process application specific channels (e.g. LIN)*/
          break;
        case kDescCommControlSubNetNum02:          /* CAN1 */
        case kDescCommControlSubNetNum03:          /* CAN2 */
          /* Nothing to do - CANdesc handles all CANs */
          break;
        default:break;
      }
  }
}

#if defined (DESC_ENABLE_RX_COMM_CONTROL)
/* ********************************************************************************
 * Function name:ApplDescSetCommModeOnRxPath
 * Description: Manipulates only the RX path on CAN. For the other networks (if any) such LIN, MOST, etc. reffer to the
 *              ApplDescSetCommMode API.
 * Returns:  nothing
 * Parameter(s):
 *   - commControlInfo->subNetTxNumber:
 *       - The application shall use this parameter to decied on which physical channels the communiaction will be manipulated.
 *       - Access type: read
 *   - commControlInfo->commCtrlChannel:
 *       - The application determines on which channel the communication will be manipulated (kDescCommControlCanCh01 - kDescCommControlCanCh14).
 *       - Access type: write
 *   - commControlInfo->rxPathState:
 *       - Activity type: kDescCommControlStateEnable - enables the communication; kDescCommControlStateDisable - disables it.
 *       - Access type: read
*   - commControlInfo->rxPathState: - irrelevant for this API since it processes only the RX path!!!
 *       - Activity type: kDescCommControlStateEnable - enables the communication; kDescCommControlStateDisable - disables it.
 *       - Access type: read
 *   - commControlInfo->msgTypes:
 *       - Message group: kDescCommControlMsgAppl - application; kDescCommControlMsgNm - NM; 
 *       - Access type: read
 *   - commControlInfo->reqCommChannel:
 *       - The current CAN channel on which the CommControl request is received.
 *       - Access type: read
 * Particularitie(s) and limitation(s):
 *   - The function "DescProcessingDone" may not be called.
 *   - The function "DescSetNegResponse" may not be called.
 ******************************************************************************** */
void DESC_API_CALLBACK_TYPE ApplDescSetCommModeOnRxPath(DescOemCommControlInfo *commControlInfo)
{
  if((commControlInfo->rxPathState & kDescCommControlStateEnable) != 0)
  {
    /* _DrvCanSetRxOnlineMode(commControlInfo.commCtrlChannel, g_descCommControlInfo.msgTypes); */
  }
  else
  {
    /* _DrvCanSetRxOfflineMode(commControlInfo.commCtrlChannel, g_descCommControlInfo.msgTypes); */
  }
}
#endif
/* diagF_DiagResponse	*/
/* $/$****** Standard error codes ******$*$/                                                     */
/* $/$**************  ************$*$/                                                           */
/* $/$****** kDescNrcGeneralReject                                         0x10 ************$*$/ */
/* $/$****** kDescNrcServiceNotSupported                                   0x11 ************$*$/ */
/* $/$****** kDescNrcSubfunctionNotSupported                               0x12 ************$*$/ */
/* $/$****** kDescNrcInvalidFormat                                         0x13 ************$*$/ */
/* $/$****** kDescNrcBusyRepeatRequest                                     0x21 ************$*$/ */
/* $/$****** kDescNrcConditionsNotCorrect                                  0x22 ************$*$/ */
/* $/$****** kDescNrcRequestSequenceError                                  0x24 ************$*$/ */
/* $/$****** kDescNrcRequestOutOfRange                                     0x31 ************$*$/ */
/* $/$****** kDescNrcAccessDenied                                          0x33 ************$*$/ */
/* $/$****** kDescNrcInvalidKey                                            0x35 ************$*$/ */
/* $/$****** kDescNrcExceedNumOfAttempts                                   0x36 ************$*$/ */
/* $/$****** kDescNrcTimeDelayNotExpired                                   0x37 ************$*$/ */
/* $/$****** kDescNrcUploadDownloadNotAccepted                             0x70 ************$*$/ */
/* $/$****** kDescNrcTransferDataSuspended                                 0x71 ************$*$/ */
/* $/$****** kDescNrcGeneralProgrammingFailure                             0x72 ************$*$/ */
/* $/$****** kDescNrcWrongBlockSequenceCounter                             0x73 ************$*$/ */
/* $/$****** kDescNrcResponsePending                                       0x78 ************$*$/ */
/* $/$****** kDescNrcSubfunctionNotSupportedInActiveSession                0x7E ************$*$/ */
/* $/$****** kDescNrcServiceNotSupportedInActiveSession                    0x7F ************$*$/ */
/* $/$****** kDescNrcRpmTooHigh                                            0x81 ************$*$/ */
/* $/$****** kDescNrcRpmTooLow                                             0x82 ************$*$/ */
/* $/$****** kDescNrcEngineIsRunning                                       0x83 ************$*$/ */
/* $/$****** kDescNrcEngineIsNotRunning                                    0x84 ************$*$/ */
/* $/$****** kDescNrcEngineRunTimeTooLow                                   0x85 ************$*$/ */
/* $/$****** kDescNrcTemperatureTooHigh                                    0x86 ************$*$/ */
/* $/$****** kDescNrcTemperatureTooLow                                     0x87 ************$*$/ */
/* $/$****** kDescNrcVehicleSpeedTooHigh                                   0x88 ************$*$/ */
/* $/$****** kDescNrcVehicleSpeedTooLow                                    0x89 ************$*$/ */
/* $/$****** kDescNrcThrottleSpeedTooHigh                                  0x8A ************$*$/ */
/* $/$****** kDescNrcThrottleSpeedTooLow                                   0x8B ************$*$/ */
/* $/$****** kDescNrcTransmissionRangeInNeutral                            0x8C ************$*$/ */
/* $/$****** kDescNrcTransmissionRangeInGears                              0x8D ************$*$/ */
/* $/$****** kDescNrcBrakeSwitchNotClosed                                  0x8F ************$*$/ */
/* $/$****** kDescNrcShifterLeverNotInPark                                 0x90 ************$*$/ */
/* $/$****** kDescNrcTorqueConverterClutchLocked                           0x91 ************$*$/ */
/* $/$****** kDescNrcVoltageTooHigh                                        0x92 ************$*$/ */
/* $/$****** kDescNrcVoltageTooLow                                         0x93 ************$*$/ */
/* 																								 */
/* Lint Info 734: diagF_DiagResponse Loss of precision (arg. no. 2) (16 bits to 8 bits) Changed by CK 04.24.09 */
//void diagF_DiagResponse(enum DIAG_RESPONSE Response, unsigned char Length, DescMsgContext* pMsgContext)
void diagF_DiagResponse(enum DIAG_RESPONSE Response, u_temp_l Length, DescMsgContext* pMsgContext)
{
	switch (Response) {
		case NO_RESPONSE:
		{
			//Nothing to do, just return
			break;
		}
		case RESPONSE_OK:
		{
			/* Fill the datalength in response buffer */
			pMsgContext->resDataLen = Length;
			DescProcessingDone();
			break;
		}
		case RESPONSE_PENDING:
		{
			DescSetNegResponse(kDescNrcResponsePending);
			DescProcessingDone();
			break;
		}
		case BUSY_REPEAT_REQUEST:
		{
			DescSetNegResponse(kDescNrcBusyRepeatRequest);
			DescProcessingDone();
			break;
		}
		case REQUEST_OUT_OF_RANGE:
		{
			DescSetNegResponse(kDescNrcRequestOutOfRange);
			DescProcessingDone();
			break;
		}
		case LENGTH_INVALID_FORMAT:
		{
			DescSetNegResponse(kDescNrcInvalidFormat);
			DescProcessingDone();
			break;
		}
		case CONDITIONS_NOT_CORRECT:
		{
			DescSetNegResponse(kDescNrcConditionsNotCorrect);
			DescProcessingDone();
			break;
		}
		case SUBFUNCTION_NOT_SUPPORTED:
		{
			DescSetNegResponse(kDescNrcSubfunctionNotSupported);
			DescProcessingDone();
			break;
		}
		case SECURITY_ACCESS_DENIED:
		{
			DescSetNegResponse(kDescNrcAccessDenied);
			DescProcessingDone();
			break;
		}
		case INVALID_KEY:
		{
			DescSetNegResponse(kDescNrcInvalidKey);
			DescProcessingDone();
			break;
		}
		case EXCEED_NO_OF_ATTEMPTS:
		{
			DescSetNegResponse(kDescNrcExceedNumOfAttempts);
			DescProcessingDone();
			break;
		}
		case TIMEDELAY_NOT_EXPIRED:
		{
			DescSetNegResponse(kDescNrcTimeDelayNotExpired);
			DescProcessingDone();
			break;
		}
		case REQUEST_SEQUENCE_ERROR:
		{
			DescSetNegResponse(kDescNrcRequestSequenceError);
			DescProcessingDone();
			break;
		}
		default :
		{
		}
	}
}


/* ********************************************************************************
 * Function diagF_CyclicWriteTask
 * Description: Cyclic Task performed for all diagnostic activities which require the scheduler
 * 				Few of the commands require the attention all the time when module is running.
 * 				Like Reset Requests, Seed/Key security, Writes to EEPROM
 * 				Version Updates
 * Returns:	None
 * Parameters: None
 ******************************************************************************** */
@far void diagF_CyclicWriteTask(void)
{
	
	diagWriteF_CyclicTask ();
	/* Check for monitoring any reset requests are present are not */
	diagLF_CheckResetRequest();
	
	diagSecF_CyclicTask ();
	
	/* Update the ECU Part No and SW Version Info update */
	diagLF_PartNo_SwVer_Update();

	/* Harsha commented 10/19/2010: Soft Reset Not supported for CUSW FIAT */
//	if (soft_reset_b == TRUE)
//	{
//		soft_reset_b = FALSE;
//		/* Go to Main */
//		main();
//	}
}

/* ********************************************************************************
 * Function diagLF_SessionTimeout
 * Description: This function gets called when ever there is a timeout happens for the diagnostic session.
 * 				When timeout happens for diagnostic session, basically goes to the default session.
 * 				Return to Default Session might happen when there is a POR or ECU Reset through $$11 $$01 or Module does not see diagnsotic present message of 5sec
 * 				When any of the above conditions happen, for security purposes we Clear any of the locked IO bits, enable DTC setting mode etc.
 * 
 ******************************************************************************** */
void diagLF_SessionTimeout(void)
{
	unsigned char i;
  extern u_16Bit diag_DCX_Test_cntr_w;
	/* DTC Setting */
	/* Enable the DTC Setting default to Enable */
	d_DtcSettingModeEnabled_t = TRUE;

	/* IO Controlling */
	/* Heated Seats. Clear the IO Lock Bits and IO Control values to Zero */
	for (i=0;i<4;i++)
	{
		diag_io_hs_lock_flags_c[i].cb = 0;

		diag_io_hs_rq_c[i] = 0;
		diag_io_hs_state_c[i] = 0;
	}
	/* Vent Seats. Clear the IO Lock Bits and IO Control values to Zero */
	for (i=0;i<2;i++)
	{
		diag_io_vs_lock_flags_c[i].cb = 0;

		diag_io_vs_rq_c [i] = 0;
		diag_io_vs_state_c [i] = 0;
	}
	/* Other IO Lock Bits to Zero */
	diag_io_lock_flags_c.cb = 0;
	diag_io_lock2_flags_c.cb = 0;
	diag_io_lock3_flags_c.cb = 0;

	/* Other IO Control command values to Zero */
	diag_io_load_shed_c = 0;
	diag_io_ign_state_c = 0;
	diag_io_vs_all_out_c = 0;
	diag_io_st_whl_rq_c = 0;
	diag_io_st_whl_state_c = 0;
	
	/* Clear Chrysler Routine Control (for Output Test) flags, status, and counter */
	diag_rc_lock_flags_c.cb = 0;
	diag_op_rc_status_c = 0;
	diag_op_rc_preCondition_stat_c = 0;
	diag_DCX_Test_cntr_w = 0;
	
}

/* ********************************************************************************
 * Function diagLF_PartNo_SwVer_Update
 * Description: This function gets called from diagF_CyclicWriteTask.
 * 				Checks for the variant configuration, based up on the varaint config it stores the Part Numbers.
 * 				Checks the new P/N info with eeprom value. If different it updates the eeprom else not.
 * 				Same with the SW Version Info
 * 
 ******************************************************************************** */

void diagLF_PartNo_SwVer_Update(void)
{
	unsigned char temp_swversion_c[3];
//	unsigned char temp_hwversion_c[3];
	unsigned char temp_partno_c[10];

//The first 6 bytes of information is same for all variants	
//For CUSW: 560465xxxx
//	diag_def_sw_pno[0]  = 0x35;
//	diag_def_sw_pno[1]  = 0x36;
//	diag_def_sw_pno[2]  = 0x30;
//	diag_def_sw_pno[3]  = 0x34;
//	diag_def_sw_pno[4]  = 0x36;
//	diag_def_sw_pno[5]  = 0x35;
//		
//	diag_def_ecu_pno[0]  = 0x35;
//	diag_def_ecu_pno[1]  = 0x36;
//	diag_def_ecu_pno[2]  = 0x30;
//	diag_def_ecu_pno[3]  = 0x34;
//	diag_def_ecu_pno[4]  = 0x36;
//	diag_def_ecu_pno[5]  = 0x35;

	/* Update Application Default Part No Information based on Variant */
	/* Modified on 02.06.2009 by CK: We look at the hardware input pins and assign Part Numbers accoringly. */
	switch (main_variant_select_c /*CSWM_config_c*/){
		case CSWM_V1/*CSWM_2HS_VARIANT_1_K*/:
		{
		   /* Variant V1: Heated Seats Front 										   */
		   /* CUSW Architecture: MY12 PF, PJ 										   */	
     	   /* SW Part Number: 560465 92AE                                              */
		   /* ECU Part Number:560465 92AE                                              */
		   /*MKS 98062 

			/* update ECU Part Number */
			diag_def_ecu_pno[0]  = 0x35;
			diag_def_ecu_pno[1]  = 0x36;
			diag_def_ecu_pno[2]  = 0x30;
			diag_def_ecu_pno[3]  = 0x34;
			diag_def_ecu_pno[4]  = 0x36;
			diag_def_ecu_pno[5]  = 0x35;
			diag_def_ecu_pno[6]  = 0x39;
			diag_def_ecu_pno[7]  = 0x32;
			diag_def_ecu_pno[8]  = 0x41;
			diag_def_ecu_pno[9]  = 0x45;
			break;
		}
		
		case CSWM_V2/*CSWM_4HS_VARIANT_2_K*/:
		{
		   /* Variant V2: Heated Seats Front + Rear									   */
		   /* CUSW Architecture: MY12 PF, PJ 										   */	
     	   /* SW Part Number: 560465 93AE                                              */
		   /* ECU Part Number:560465 93AE                                              */
		   /* MKS 98062 

			/* update ECU Part Number */
			diag_def_ecu_pno[0]  = 0x35;
			diag_def_ecu_pno[1]  = 0x36;
			diag_def_ecu_pno[2]  = 0x30;
			diag_def_ecu_pno[3]  = 0x34;
			diag_def_ecu_pno[4]  = 0x36;
			diag_def_ecu_pno[5]  = 0x35;
			diag_def_ecu_pno[6]  = 0x39;
			diag_def_ecu_pno[7]  = 0x33;
			diag_def_ecu_pno[8]  = 0x41;
			diag_def_ecu_pno[9]  = 0x45;
			break;
		}
		
		case CSWM_V3/*CSWM_2HS_2VS_HSW_VARIANT_3_K*/:
		{
		   /* Variant V3: Heated Seats Front + Wheel + Vents						   */
		   /* CUSW Architecture: MY12 PF, PJ 										   */	
     	   /* SW Part Number: 681375 44AE                                              */
		   /* ECU Part Number:681375 44AE                                              */
		   /* MKS 98062 
	
			/* update ECU Part Number */
			diag_def_ecu_pno[0]  = 0x36;
			diag_def_ecu_pno[1]  = 0x38;
			diag_def_ecu_pno[2]  = 0x31;
			diag_def_ecu_pno[3]  = 0x33;
			diag_def_ecu_pno[4]  = 0x37;
			diag_def_ecu_pno[5]  = 0x35;
			diag_def_ecu_pno[6]  = 0x34;
			diag_def_ecu_pno[7]  = 0x34;
			diag_def_ecu_pno[8]  = 0x41;
			diag_def_ecu_pno[9]  = 0x45;
			break;
		}

		case CSWM_V5/*CSWM_2HS_2VS_VARIANT_5_K*/:
		{
		   /* Variant V5: Heated Seats Front + Vents								   */
		   /* CUSW Architecture: MY12 PF, PJ 										   */	
	       /* SW Part Number: 681375 45AE                                              */
	       /* ECU Part Number:681375 45AE                                              */
		   /* MKS 98062 
	
			/* update ECU Part Number */
			diag_def_ecu_pno[0]  = 0x36;
			diag_def_ecu_pno[1]  = 0x38;
			diag_def_ecu_pno[2]  = 0x31;
			diag_def_ecu_pno[3]  = 0x33;
			diag_def_ecu_pno[4]  = 0x37;
			diag_def_ecu_pno[5]  = 0x35;
			diag_def_ecu_pno[6]  = 0x34;
			diag_def_ecu_pno[7]  = 0x35;
			diag_def_ecu_pno[8]  = 0x41;
			diag_def_ecu_pno[9]  = 0x45;
			break;
		}

		case CSWM_V6/*CSWM_4HS_HSW_VARIANT_6_K*/:
		{
		   /* Variant V6: Heated Seats Front + Rear + Wheel							   */
		   /* CUSW Architecture: MY12 PF, PJ 										   */	
     	   /* SW Part Number: 560465 95AE                                              */
		   /* ECU Part Number:560465 95AE                                              */
		   /* MKS 98062 
	
			/* update ECU Part Number */
			diag_def_ecu_pno[0]  = 0x35;
			diag_def_ecu_pno[1]  = 0x36;
			diag_def_ecu_pno[2]  = 0x30;
			diag_def_ecu_pno[3]  = 0x34;
			diag_def_ecu_pno[4]  = 0x36;
			diag_def_ecu_pno[5]  = 0x35;
			diag_def_ecu_pno[6]  = 0x39;
			diag_def_ecu_pno[7]  = 0x35;
			diag_def_ecu_pno[8]  = 0x41;
			diag_def_ecu_pno[9]  = 0x45;
			break;
		}

		case CSWM_V7/*CSWM_4HS_2VS_HSW_VARIANT_7_K*/:
		{
		   /* Variant V6: Heated Seats Front + Rear + Wheel	+ Vents					   */
		   /* CUSW Architecture: MY12 PF, PJ 										   */	
		   /* SW Part Number: 681375 46AE                                              */
		   /* ECU Part Number:681375 46AE                                              */
		   /* MKS 98062 
	
			/* update ECU Part Number */
			diag_def_ecu_pno[0]  = 0x36;
			diag_def_ecu_pno[1]  = 0x38;
			diag_def_ecu_pno[2]  = 0x31;
			diag_def_ecu_pno[3]  = 0x33;
			diag_def_ecu_pno[4]  = 0x37;
			diag_def_ecu_pno[5]  = 0x35;
			diag_def_ecu_pno[6]  = 0x34;
			diag_def_ecu_pno[7]  = 0x36;
			diag_def_ecu_pno[8]  = 0x41;
			diag_def_ecu_pno[9]  = 0x45;
			break;
		}

		/* default CSWM_2HS_HSW_VARIANT_4_K (Less populated) */
		default :
		{
		   /* Variant V4: Heated Seats Front + Wheel		    					   */
		   /* CUSW Architecture: MY12 PF, PJ 										   */	
     	   /* SW Part Number: 560465 94AE                                              */
		   /* ECU Part Number:560465 94AE                                              */
		   /* MKS 98062 
	
			/* update ECU Part Number */
			diag_def_ecu_pno[0]  = 'P';//0x35;
			diag_def_ecu_pno[1]  = 'I';//0x36;
			diag_def_ecu_pno[2]  = 'D';//0x30;
			diag_def_ecu_pno[3]  = 'C';//0x34;
			diag_def_ecu_pno[4]  = 'O';//0x36;
			diag_def_ecu_pno[5]  = 'N';//0x35;
			diag_def_ecu_pno[6]  = 'N';//0x39;
			diag_def_ecu_pno[7]  = 'U';//0x34;
			diag_def_ecu_pno[8]  = 'M';//0x41;
			diag_def_ecu_pno[9]  = '2';//0x45;
		}
	}
	
	/* Read the SW Part Number from the EEPROM */
	EE_BlockRead(EE_FBL_CHR_APPL_SW_PNO, &temp_partno_c[0]);
	/* Update SW Part No, If actual SW Part No and Default SW/ECU Part Nos are different */
	if ( (temp_partno_c[5] != diag_def_ecu_pno[5]) ||
	    (temp_partno_c[6] != diag_def_ecu_pno[6]) ||
	    (temp_partno_c[7] != diag_def_ecu_pno[7]) ||
	    (temp_partno_c[8] != diag_def_ecu_pno[8]) ||
	    (temp_partno_c[9] != diag_def_ecu_pno[9]) )
	{
		temp_partno_c[0] = diag_def_ecu_pno[0];
		temp_partno_c[1] = diag_def_ecu_pno[1];
		temp_partno_c[2] = diag_def_ecu_pno[2];
		temp_partno_c[3] = diag_def_ecu_pno[3];
		temp_partno_c[4] = diag_def_ecu_pno[4];
		temp_partno_c[5] = diag_def_ecu_pno[5];
		temp_partno_c[6] = diag_def_ecu_pno[6];
		temp_partno_c[7] = diag_def_ecu_pno[7];
		temp_partno_c[8] = diag_def_ecu_pno[8];
		temp_partno_c[9] = diag_def_ecu_pno[9];

		EE_BlockWrite(EE_FBL_CHR_APPL_SW_PNO, &temp_partno_c[0]);
		sw_partno_update_b = TRUE;
	}

	/* Read the ECU Part Number from EEPROM */
	EE_BlockRead(EE_FBL_CHR_ECU_PNO, &temp_partno_c[0]);
	/* Update ECU Part No, If actual ECU Part No and Default ECU Part Nos are different */
	if ( (temp_partno_c[5] != diag_def_ecu_pno[5]) ||
	    (temp_partno_c[6] != diag_def_ecu_pno[6]) ||
	    (temp_partno_c[7] != diag_def_ecu_pno[7]) ||
	    (temp_partno_c[8] != diag_def_ecu_pno[8]) ||
	    (temp_partno_c[9] != diag_def_ecu_pno[9]))
	{
		temp_partno_c[0] = diag_def_ecu_pno[0];
		temp_partno_c[1] = diag_def_ecu_pno[1];
		temp_partno_c[2] = diag_def_ecu_pno[2];
		temp_partno_c[3] = diag_def_ecu_pno[3];
		temp_partno_c[4] = diag_def_ecu_pno[4];
		temp_partno_c[5] = diag_def_ecu_pno[5];
		temp_partno_c[6] = diag_def_ecu_pno[6];
		temp_partno_c[7] = diag_def_ecu_pno[7];
		temp_partno_c[8] = diag_def_ecu_pno[8];
		temp_partno_c[9] = diag_def_ecu_pno[9];

		EE_BlockWrite(EE_FBL_CHR_ECU_PNO, &temp_partno_c[0]);
		ecu_partno_update_b = TRUE;
	}

    /* Update SW Version, If it is different from defaults */
	EE_BlockRead(EE_FBL_FIAT_ECU_SW_VERSION, &temp_swversion_c[0]);

	if ((temp_swversion_c[0] != SW_YEAR) ||
	    (temp_swversion_c[1] != SW_WEEK) /*||
	    (temp_swversion_c[2] != SW_PATCH)*/)
	{

		temp_swversion_c[0] = SW_YEAR;
		temp_swversion_c[1] = SW_WEEK;
		//temp_swversion_c[2] = SW_PATCH;
		
		EE_BlockWrite(EE_FBL_FIAT_ECU_SW_VERSION, &temp_swversion_c[0]);
		sw_version_update_b = TRUE;
	}

	/* Updates to EEPROM */
	if ((sw_partno_update_b == TRUE)  ||
	    (ecu_partno_update_b == TRUE) ||
	    (sw_version_update_b == TRUE) )
	{

//		FlashF_UpdateEEPROM_Immediate ();
		diag_load_bl_eep_backup_b = TRUE; //Time to take boot backup
		sw_partno_update_b = FALSE;
		ecu_partno_update_b = FALSE;
		sw_version_update_b = FALSE;
	}
} //End of function

/* ********************************************************************************
 * Function diagLF_CheckResetRequest
 * Description: This function gets called from diagF_CyclicWriteTask.
 * 				If module receives straight ECU Reset($$11 $$01), this function serves RESET_REQUEST mode
 * 				If module receives BOOTMODE Request ($$10 $$02), First writes Programming Request Flag in EEPROM and then
 * 				update the backup copy of boot. Once the EEPROM is idle, Logic issues ECU reset
 *  
 ******************************************************************************** */
void diagLF_CheckResetRequest(void)
{
	unsigned char temp,temp_load_data_c;
	switch (d_ResetReq_t) {
		case BOOTMODE_REQUEST:
		{
			/* Write to EEPROM with 0xC3, with this Boot knows there is an external prm request */
			temp = D_FBL_BOOTMODE_VAL_K;
            EE_BlockWrite(EE_FBL_PROGREQ_FLAGS,&temp);
            /* Write to EEPROM with 0x55, with this appl knows once return from EEP, stores the default values */
//    		temp_load_data_c = 0x55;
//    		EE_BlockWrite(EE_FBL_REPROG_REQ, &temp_load_data_c);
            /* Update the backup copy, else we stay in application */
  	  	    diag_load_bl_eep_backup_b = TRUE; //Time to take backup copy of boot
  	  	    /* Go to the next step */
  	  	    d_ResetReq_t = PREPARE_RESET_REQUEST;
			break;
		}
		case PREPARE_RESET_REQUEST:
		{
//			if (/*Check for programming pre conditions*/0) {
				/* Disable the passive mode */
				//d_PassiveModeState_c = kDescPassiveModeDisable;
				//Write EEPROM checksum to invalid
				d_ResetReq_t = RESET_REQUEST;
//			}
//			else {
//				//If not enabled the DESC_ENABLE_PASSIVE_MODE just directly go to 
//				d_ResetReq_t = RESET_REQUEST;
//			}
			break;
		}
		case RESET_REQUEST:
		{
			/* If No EEPROM Writes are pending */
			if ( EE_IsBusy() != TRUE) {
				
				main_ECU_reset_rq_c = TRUE;
//				DescProcessingDone(); //Already called once. 
				
				break;
			}
			else {
				//Writes not completed. Wait
			}
			break;
		}
		default :
		{
		}
	}
}

