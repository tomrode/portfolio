                   list      p=16f887
                   #include  <p16f887.inc>

;******************************************************************************
; Device Configuration
;******************************************************************************

                   __CONFIG  _CONFIG1, _LVP_OFF & _FCMEN_ON & _IESO_OFF & _BOR_OFF & _CPD_OFF & _CP_OFF & _MCLRE_ON & _PWRTE_ON & _WDT_OFF & _INTRC_OSC_NOCLKOUT
                   __CONFIG  _CONFIG2, _WRT_OFF & _BOR21V

;******************************************************************************
; Variable Definitions
;******************************************************************************
tempoverflow      equ       0x77 
temp8             equ       0x78
temp7             equ       0x79 
temp6             equ       0x7A
temp5             equ       0x7B
temp4             equ       0x7C
temp3             equ       0x7D
temp2             equ       0x7E
temp1             equ       0x7F

;******************************************************************************
; Reset Vector
;******************************************************************************

                   org       0x000

                   goto      main

;******************************************************************************
; Interrupt Vector
;******************************************************************************
                   org       0x400
                   retfie

;******************************************************************************
; Main Program
;******************************************************************************

main
                   movlw     0x61                          ; Select internal 4 MHz clock.
                   banksel   OSCCON
                   movwf     OSCCON
wait_stable
                   btfss     OSCCON,HTS                    ; Is the high-speed internal oscillator stable?
                  ; goto      wait_stable                   ; If not, wait until it is.

                   banksel   INTCON
                   bcf       INTCON,GIE
                               
 
                 

;******************************************************************************
; Main Loop
;******************************************************************************

main_loop           
             
                   call      sub1                           ; Initialize the software timer.
                   goto      main_loop


;******************************************************************************
; sub1 ()
;
; Description:  Nested subroutine
;               
;
; Inputs:       
;
; Outputs:      None
;******************************************************************************

sub1              
                   movlw       0x01
                   banksel     temp1
                   movwf       temp1
                   call        sub2                  ; Globally enable interrupts.
                   
                   return    
sub2
                   
                   movlw       0x02
                   banksel     temp2
                   movwf       temp2
                   call        sub3 
                   return

sub3               
                   
                   movlw       0x03
                   banksel     temp3
                   movwf       temp3  
                   call        sub4 
                   return

sub4 
                   
                   movlw       0x04
                   banksel     temp4
                   movwf       temp4
                   call        sub5
                   return 
sub5 
                  
                   movlw       0x05
                   banksel     temp5
                   movwf       temp5
                   call        sub6 
                   return
sub6 
                   
                   movlw       0x06
                   banksel     temp6
                   movwf       temp6
                   call        sub7 
                   return
sub7
                   
                   movlw       0x07
                   banksel     temp7
                   movwf       temp7
                   call        sub8
                   return

sub8               
                  
                   movlw       0x08
                   banksel     temp8
                   movwf       temp8
                   call        sub9
                   return
sub9
                   movlw       0x08
                   banksel     temp8
                   movwf       temp8
                   return
;******************************************************************************
; End of program
;******************************************************************************

                   end
