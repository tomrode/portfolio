

#include "typedefs.h"
#include "Sec_Seed.h"
 
/***********************************************************************************/
/* Function NAME:  security_alg();                                                 */
/*                                                                                 */
/* Description:  The security algorithm takes as input a 32-bit seed and           */
/*               returns as output a 32-bit key Example taken from                 */
/*               Security Algo CSWM_CUSW M1_5 psw.docx..                           */
/* INPUTS:       32 bit seed value, 32 bit CONSTANT_1, and 32bit CONSTANT_2 values */
/***********************************************************************************/

uint32_t security_alg(uint32_t *Seed, uint32_t *Constant_1, uint32_t *Constant_2)

{
     uint32_t Key;                                                    //variables 
     uint32_t Constant_2_local_var;
	 
     Key = ((*Seed << 8) & 0xFF000000) + ((*Seed >> 8) & 0x00FF0000) +
           ((*Seed << 8) & 0x0000FF00) + ((*Seed >> 8) & 0x000000FF);  //Re-arrange original Seed from 0xGGHHIIJJ to modified Seed 0xHHGGJJII

     Key = ((Key << 11) + (Key >> 22)) & 0xFFFFFFFF;                 //(Shift modified Seed up 11 times) + (Shift modified Seed down 22 times) 
       
     Constant_2_local_var = (*Constant_2 & *Seed) & 0xFFFFFFFF;                  //Constant_2 and with original Seed

                                                      
     Key = (Key ^ *Constant_1 ^ Constant_2_local_var) & 0xFFFFFFFF;             //Exclusive OR operations
 

     return Key;
}

