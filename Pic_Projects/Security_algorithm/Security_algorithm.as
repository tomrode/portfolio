opt subtitle "HI-TECH Software Omniscient Code Generator (Lite mode) build 6738"

opt pagewidth 120

	opt lm

	processor	16F887
clrc	macro
	bcf	3,0
	endm
clrz	macro
	bcf	3,2
	endm
setc	macro
	bsf	3,0
	endm
setz	macro
	bsf	3,2
	endm
skipc	macro
	btfss	3,0
	endm
skipz	macro
	btfss	3,2
	endm
skipnc	macro
	btfsc	3,0
	endm
skipnz	macro
	btfsc	3,2
	endm
indf	equ	0
indf0	equ	0
pc	equ	2
pcl	equ	2
status	equ	3
fsr	equ	4
fsr0	equ	4
c	equ	1
z	equ	0
pclath	equ	10
# 7 "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Security_algorithm\main.c"
	psect config,class=CONFIG,delta=2 ;#
# 7 "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Security_algorithm\main.c"
	dw 0x1FFF & 0x3FFF & 0x3FFF & 0x3BFF & 0x3EFF & 0x3FFF & 0x3FFF & 0x3FFF & 0x3FEF & 0x3FF7 & 0x3FFD ;#
# 8 "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Security_algorithm\main.c"
	psect config,class=CONFIG,delta=2 ;#
# 8 "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Security_algorithm\main.c"
	dw 0x3FFF ;#
	FNCALL	_main,_sfreg
	FNCALL	_main,_security_alg
	FNCALL	_main,_left_shifted
	FNROOT	_main
	global	_security_seed_2constants
psect	idataBANK0,class=CODE,space=0,delta=2
global __pidataBANK0
__pidataBANK0:
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Security_algorithm\main.c"
	line	22

;initializer for _security_seed_2constants
	retlw	040h
	retlw	0A4h
	retlw	0CCh
	retlw	0FBh

	retlw	04Fh
	retlw	0E8h
	retlw	08Fh
	retlw	06Ch

	retlw	0BEh
	retlw	0D3h
	retlw	030h
	retlw	09Ch

	global	_security_seed_2constant_ptr
	global	_tmr1_counter
	global	_toms_variable
	global	_persistent_watch_dog_count
psect	nvBANK0,class=BANK0,space=1
global __pnvBANK0
__pnvBANK0:
_persistent_watch_dog_count:
       ds      2

	global	_PORTD
_PORTD	set	8
	global	_T1CON
_T1CON	set	16
	global	_GIE
_GIE	set	95
	global	_TO
_TO	set	28
	global	_OPTION
_OPTION	set	129
	global	_OSCCON
_OSCCON	set	143
	global	_TRISD
_TRISD	set	136
	global	_TRISE
_TRISE	set	137
	global	_WDTCON
_WDTCON	set	261
	global	_ANSEL
_ANSEL	set	392
	file	"Security_algorithm.as"
	line	#
psect cinit,class=CODE,delta=2
global start_initialization
start_initialization:

psect	bssCOMMON,class=COMMON,space=1
global __pbssCOMMON
__pbssCOMMON:
_toms_variable:
       ds      1

psect	bssBANK0,class=BANK0,space=1
global __pbssBANK0
__pbssBANK0:
_security_seed_2constant_ptr:
       ds      1

_tmr1_counter:
       ds      1

psect	dataBANK0,class=BANK0,space=1
global __pdataBANK0
__pdataBANK0:
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Security_algorithm\main.c"
_security_seed_2constants:
       ds      12

; Clear objects allocated to COMMON
psect cinit,class=CODE,delta=2
	clrf	((__pbssCOMMON)+0)&07Fh
; Clear objects allocated to BANK0
psect cinit,class=CODE,delta=2
	clrf	((__pbssBANK0)+0)&07Fh
	clrf	((__pbssBANK0)+1)&07Fh
global btemp
psect inittext,class=CODE,delta=2
global init_fetch,btemp
;	Called with low address in FSR and high address in W
init_fetch:
	movf btemp,w
	movwf pclath
	movf btemp+1,w
	movwf pc
global init_ram
;Called with:
;	high address of idata address in btemp 
;	low address of idata address in btemp+1 
;	low address of data in FSR
;	high address + 1 of data in btemp-1
init_ram:
	fcall init_fetch
	movwf indf,f
	incf fsr,f
	movf fsr,w
	xorwf btemp-1,w
	btfsc status,2
	retlw 0
	incf btemp+1,f
	btfsc status,2
	incf btemp,f
	goto init_ram
; Initialize objects allocated to BANK0
psect cinit,class=CODE,delta=2
global init_ram, __pidataBANK0
	bcf	status, 7	;select IRP bank0
	movlw low(__pdataBANK0+12)
	movwf btemp-1,f
	movlw high(__pidataBANK0)
	movwf btemp,f
	movlw low(__pidataBANK0)
	movwf btemp+1,f
	movlw low(__pdataBANK0)
	movwf fsr,f
	fcall init_ram
psect cinit,class=CODE,delta=2
global end_of_initialization

;End of C runtime variable initialization code

end_of_initialization:
clrf status
ljmp _main	;jump to C main() function
psect	cstackCOMMON,class=COMMON,space=1
global __pcstackCOMMON
__pcstackCOMMON:
	global	?_sfreg
?_sfreg:	; 0 bytes @ 0x0
	global	??_sfreg
??_sfreg:	; 0 bytes @ 0x0
	global	?_main
?_main:	; 2 bytes @ 0x0
	global	?_security_alg
?_security_alg:	; 4 bytes @ 0x0
	global	?_left_shifted
?_left_shifted:	; 4 bytes @ 0x0
	global	security_alg@Constant_1
security_alg@Constant_1:	; 1 bytes @ 0x0
	global	left_shifted@initial
left_shifted@initial:	; 4 bytes @ 0x0
	ds	1
	global	security_alg@Constant_2
security_alg@Constant_2:	; 1 bytes @ 0x1
	ds	3
	global	left_shifted@times
left_shifted@times:	; 1 bytes @ 0x4
	global	security_alg@Constant_2_local_var
security_alg@Constant_2_local_var:	; 4 bytes @ 0x4
	ds	1
	global	??_left_shifted
??_left_shifted:	; 0 bytes @ 0x5
	ds	3
	global	security_alg@Key
security_alg@Key:	; 4 bytes @ 0x8
	ds	4
	global	security_alg@Seed
security_alg@Seed:	; 1 bytes @ 0xC
	ds	1
psect	cstackBANK0,class=BANK0,space=1
global __pcstackBANK0
__pcstackBANK0:
	global	??_security_alg
??_security_alg:	; 0 bytes @ 0x0
	global	left_shifted@new_initial_temp
left_shifted@new_initial_temp:	; 4 bytes @ 0x0
	ds	4
	global	left_shifted@temp_initial
left_shifted@temp_initial:	; 4 bytes @ 0x4
	ds	4
	global	left_shifted@temp_times
left_shifted@temp_times:	; 1 bytes @ 0x8
	ds	1
	global	left_shifted@i
left_shifted@i:	; 1 bytes @ 0x9
	ds	1
	global	left_shifted@overflow_count
left_shifted@overflow_count:	; 1 bytes @ 0xA
	ds	1
	global	left_shifted@carry_count
left_shifted@carry_count:	; 1 bytes @ 0xB
	ds	1
	global	left_shifted@current_temp
left_shifted@current_temp:	; 4 bytes @ 0xC
	ds	20
	global	??_main
??_main:	; 0 bytes @ 0x20
	ds	2
	global	main@returned_keyval
main@returned_keyval:	; 4 bytes @ 0x22
	ds	4
	global	main@returned_shift
main@returned_shift:	; 4 bytes @ 0x26
	ds	4
	global	main@watchdog
main@watchdog:	; 1 bytes @ 0x2A
	ds	1
;;Data sizes: Strings 0, constant 0, data 12, bss 3, persistent 2 stack 0
;;Auto spaces:   Size  Autos    Used
;; COMMON          14     13      14
;; BANK0           80     43      59
;; BANK1           80      0       0
;; BANK3           96      0       0
;; BANK2           96      0       0

;;
;; Pointer list with targets:

;; ?_left_shifted	unsigned long  size(1) Largest target is 0
;;
;; ?_security_alg	unsigned long  size(1) Largest target is 0
;;
;; security_seed_2constant_ptr	PTR unsigned long  size(1) Largest target is 0
;;		 -> NULL(NULL[0]), 
;;
;; security_alg@Constant_1	PTR unsigned long  size(1) Largest target is 12
;;		 -> security_seed_2constants(BANK0[12]), 
;;
;; security_alg@Constant_2	PTR unsigned long  size(1) Largest target is 12
;;		 -> security_seed_2constants(BANK0[12]), 
;;
;; security_alg@Seed	PTR unsigned long  size(1) Largest target is 12
;;		 -> security_seed_2constants(BANK0[12]), 
;;


;;
;; Critical Paths under _main in COMMON
;;
;;   _main->_security_alg
;;
;; Critical Paths under _main in BANK0
;;
;;   _main->_security_alg
;;
;; Critical Paths under _main in BANK1
;;
;;   None.
;;
;; Critical Paths under _main in BANK3
;;
;;   None.
;;
;; Critical Paths under _main in BANK2
;;
;;   None.

;;
;;Main: autosize = 0, tempsize = 2, incstack = 0, save=0
;;

;;
;;Call Graph Tables:
;;
;; ---------------------------------------------------------------------------------
;; (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;; ---------------------------------------------------------------------------------
;; (0) _main                                                11    11      0     547
;;                                             32 BANK0     11    11      0
;;                              _sfreg
;;                       _security_alg
;;                       _left_shifted
;; ---------------------------------------------------------------------------------
;; (1) _left_shifted                                        25    20      5     276
;;                                              0 COMMON     9     4      5
;;                                              0 BANK0     16    16      0
;; ---------------------------------------------------------------------------------
;; (1) _security_alg                                        45    41      4     268
;;                                              0 COMMON    13     9      4
;;                                              0 BANK0     32    32      0
;; ---------------------------------------------------------------------------------
;; (1) _sfreg                                                0     0      0       0
;; ---------------------------------------------------------------------------------
;; Estimated maximum stack depth 1
;; ---------------------------------------------------------------------------------

;; Call Graph Graphs:

;; _main (ROOT)
;;   _sfreg
;;   _security_alg
;;   _left_shifted
;;

;; Address spaces:

;;Name               Size   Autos  Total    Cost      Usage
;;BANK3               60      0       0       9        0.0%
;;BITBANK3            60      0       0       8        0.0%
;;SFR3                 0      0       0       4        0.0%
;;BITSFR3              0      0       0       4        0.0%
;;BANK2               60      0       0      11        0.0%
;;BITBANK2            60      0       0      10        0.0%
;;SFR2                 0      0       0       5        0.0%
;;BITSFR2              0      0       0       5        0.0%
;;SFR1                 0      0       0       2        0.0%
;;BITSFR1              0      0       0       2        0.0%
;;BANK1               50      0       0       7        0.0%
;;BITBANK1            50      0       0       6        0.0%
;;CODE                 0      0       0       0        0.0%
;;DATA                 0      0      4A      12        0.0%
;;ABS                  0      0      49       3        0.0%
;;NULL                 0      0       0       0        0.0%
;;STACK                0      0       1       2        0.0%
;;BANK0               50     2B      3B       5       73.8%
;;BITBANK0            50      0       0       4        0.0%
;;SFR0                 0      0       0       1        0.0%
;;BITSFR0              0      0       0       1        0.0%
;;COMMON               E      D       E       1      100.0%
;;BITCOMMON            E      0       0       0        0.0%
;;EEDATA             100      0       0       0        0.0%

	global	_main
psect	maintext,global,class=CODE,delta=2
global __pmaintext
__pmaintext:

;; *************** function _main *****************
;; Defined at:
;;		line 32 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Security_algorithm\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  returned_shi    4   38[BANK0 ] unsigned long 
;;  returned_key    4   34[BANK0 ] unsigned long 
;;  watchdog        1   42[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;                  2  850[COMMON] int 
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 17F/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       9       0       0       0
;;      Temps:          0       2       0       0       0
;;      Totals:         0      11       0       0       0
;;Total ram usage:       11 bytes
;; Hardware stack levels required when called:    1
;; This function calls:
;;		_sfreg
;;		_security_alg
;;		_left_shifted
;; This function is called by:
;;		Startup code after reset
;; This function uses a non-reentrant model
;;
psect	maintext
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Security_algorithm\main.c"
	line	32
	global	__size_of_main
	__size_of_main	equ	__end_of_main-_main
	
_main:	
	opt	stack 7
; Regs used in _main: [wreg-fsr0h+status,2+status,0+pclath+cstack]
	line	41
	
l2161:	
;main.c: 35: uint8_t watchdog;
;main.c: 36: uint32_t returned_keyval;
;main.c: 37: uint32_t returned_shift;
;main.c: 41: sfreg();
	fcall	_sfreg
	line	42
	
l2163:	
;main.c: 42: (GIE = 0);
	bcf	(95/8),(95)&7
	goto	l2165
	line	50
;main.c: 50: while(1)
	
l851:	
	line	52
	
l2165:	
;main.c: 51: {
;main.c: 52: if(TO == 1)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfss	(28/8),(28)&7
	goto	u2451
	goto	u2450
u2451:
	goto	l2175
u2450:
	line	54
	
l2167:	
# 54 "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Security_algorithm\main.c"
nop ;#
psect	maintext
	line	56
	
l2169:	
;main.c: 56: returned_keyval = security_alg(&security_seed_2constants[0],&security_seed_2constants[1], &security_seed_2constants[2]);
	movlw	(_security_seed_2constants+04h)&0ffh
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(?_security_alg)
	movlw	(_security_seed_2constants+08h)&0ffh
	movwf	(??_main+1)+0
	movf	(??_main+1)+0,w
	movwf	0+(?_security_alg)+01h
	movlw	(_security_seed_2constants)&0ffh
	fcall	_security_alg
	movf	(3+(?_security_alg)),w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(main@returned_keyval+3)
	movf	(2+(?_security_alg)),w
	movwf	(main@returned_keyval+2)
	movf	(1+(?_security_alg)),w
	movwf	(main@returned_keyval+1)
	movf	(0+(?_security_alg)),w
	movwf	(main@returned_keyval)

	line	59
	
l2171:	
;main.c: 59: returned_shift = left_shifted(0x00F0000, 100);
	movlw	0
	movwf	(?_left_shifted+3)
	movlw	0Fh
	movwf	(?_left_shifted+2)
	movlw	0
	movwf	(?_left_shifted+1)
	movlw	0
	movwf	(?_left_shifted)

	movlw	(064h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	0+(?_left_shifted)+04h
	fcall	_left_shifted
	movf	(3+(?_left_shifted)),w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(main@returned_shift+3)
	movf	(2+(?_left_shifted)),w
	movwf	(main@returned_shift+2)
	movf	(1+(?_left_shifted)),w
	movwf	(main@returned_shift+1)
	movf	(0+(?_left_shifted)),w
	movwf	(main@returned_shift)

	line	60
	
l2173:	
# 60 "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Security_algorithm\main.c"
clrwdt ;#
psect	maintext
	line	61
;main.c: 61: }
	goto	l2165
	line	62
	
l852:	
	line	64
	
l2175:	
;main.c: 62: else
;main.c: 63: {
;main.c: 64: watchdog = TO;
	movlw	0
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfsc	(28/8),(28)&7
	movlw	1
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(main@watchdog)
	line	65
;main.c: 65: PORTD = persistent_watch_dog_count++;
	movf	(_persistent_watch_dog_count),w
	movwf	(8)	;volatile
	movlw	low(01h)
	addwf	(_persistent_watch_dog_count),f
	skipnc
	incf	(_persistent_watch_dog_count+1),f
	movlw	high(01h)
	addwf	(_persistent_watch_dog_count+1),f
	line	66
	
l2177:	
# 66 "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Security_algorithm\main.c"
clrwdt ;#
psect	maintext
	goto	l2165
	line	67
	
l853:	
	goto	l2165
	line	68
	
l854:	
	line	50
	goto	l2165
	
l855:	
	line	69
	
l856:	
	global	start
	ljmp	start
	opt stack 0
GLOBAL	__end_of_main
	__end_of_main:
;; =============== function _main ends ============

	signat	_main,90
	global	_left_shifted
psect	text127,local,class=CODE,delta=2
global __ptext127
__ptext127:

;; *************** function _left_shifted *****************
;; Defined at:
;;		line 96 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Security_algorithm\main.c"
;; Parameters:    Size  Location     Type
;;  initial         4    0[COMMON] unsigned long 
;;  times           1    4[COMMON] unsigned char 
;; Auto vars:     Size  Location     Type
;;  current_temp    4   12[BANK0 ] unsigned long 
;;  temp_initial    4    4[BANK0 ] unsigned long 
;;  new_initial_    4    0[BANK0 ] unsigned long 
;;  carry_count     1   11[BANK0 ] unsigned char 
;;  overflow_cou    1   10[BANK0 ] unsigned char 
;;  i               1    9[BANK0 ] unsigned char 
;;  temp_times      1    8[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;                  4    0[COMMON] unsigned long 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         5       0       0       0       0
;;      Locals:         0      16       0       0       0
;;      Temps:          4       0       0       0       0
;;      Totals:         9      16       0       0       0
;;Total ram usage:       25 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text127
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Security_algorithm\main.c"
	line	96
	global	__size_of_left_shifted
	__size_of_left_shifted	equ	__end_of_left_shifted-_left_shifted
	
_left_shifted:	
	opt	stack 7
; Regs used in _left_shifted: [wreg+status,2+status,0]
	line	98
	
l2129:	
;main.c: 98: uint8_t temp_times = times;
	movf	(left_shifted@times),w
	movwf	(??_left_shifted+0)+0
	movf	(??_left_shifted+0)+0,w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(left_shifted@temp_times)
	line	99
	
l2131:	
;main.c: 99: uint8_t i = 0;
	clrc
	movlw	0
	btfsc	status,0
	movlw	1
	movwf	(left_shifted@i)
	line	100
	
l2133:	
;main.c: 100: uint8_t overflow_count = 0;
	clrc
	movlw	0
	btfsc	status,0
	movlw	1
	movwf	(left_shifted@overflow_count)
	line	101
	
l2135:	
;main.c: 101: uint8_t carry_count = 0;
	clrc
	movlw	0
	btfsc	status,0
	movlw	1
	movwf	(left_shifted@carry_count)
	line	102
	
l2137:	
;main.c: 102: uint32_t temp_initial = initial;
	movf	(left_shifted@initial+3),w
	movwf	(left_shifted@temp_initial+3)
	movf	(left_shifted@initial+2),w
	movwf	(left_shifted@temp_initial+2)
	movf	(left_shifted@initial+1),w
	movwf	(left_shifted@temp_initial+1)
	movf	(left_shifted@initial),w
	movwf	(left_shifted@temp_initial)

	line	106
	
l2139:	
;main.c: 103: uint32_t new_initial_temp;
;main.c: 104: uint32_t current_temp;
;main.c: 106: current_temp = initial;
	movf	(left_shifted@initial+3),w
	movwf	(left_shifted@current_temp+3)
	movf	(left_shifted@initial+2),w
	movwf	(left_shifted@current_temp+2)
	movf	(left_shifted@initial+1),w
	movwf	(left_shifted@current_temp+1)
	movf	(left_shifted@initial),w
	movwf	(left_shifted@current_temp)

	line	107
;main.c: 107: while ((i < temp_times) && (overflow_count < (4)))
	goto	l2153
	
l863:	
	line	109
	
l2141:	
;main.c: 108: {
;main.c: 109: i++;
	movlw	(01h)
	movwf	(??_left_shifted+0)+0
	movf	(??_left_shifted+0)+0,w
	addwf	(left_shifted@i),f
	line	110
;main.c: 110: temp_initial = (current_temp << 1);
	movf	(left_shifted@current_temp),w
	movwf	(??_left_shifted+0)+0
	movf	(left_shifted@current_temp+1),w
	movwf	((??_left_shifted+0)+0+1)
	movf	(left_shifted@current_temp+2),w
	movwf	((??_left_shifted+0)+0+2)
	movf	(left_shifted@current_temp+3),w
	movwf	((??_left_shifted+0)+0+3)
	movlw	01h
u2405:
	clrc
	rlf	(??_left_shifted+0)+0,f
	rlf	(??_left_shifted+0)+1,f
	rlf	(??_left_shifted+0)+2,f
	rlf	(??_left_shifted+0)+3,f
u2400:
	addlw	-1
	skipz
	goto	u2405
	movf	3+(??_left_shifted+0)+0,w
	movwf	(left_shifted@temp_initial+3)
	movf	2+(??_left_shifted+0)+0,w
	movwf	(left_shifted@temp_initial+2)
	movf	1+(??_left_shifted+0)+0,w
	movwf	(left_shifted@temp_initial+1)
	movf	0+(??_left_shifted+0)+0,w
	movwf	(left_shifted@temp_initial)

	line	111
	
l2143:	
;main.c: 111: current_temp = temp_initial;
	movf	(left_shifted@temp_initial+3),w
	movwf	(left_shifted@current_temp+3)
	movf	(left_shifted@temp_initial+2),w
	movwf	(left_shifted@current_temp+2)
	movf	(left_shifted@temp_initial+1),w
	movwf	(left_shifted@current_temp+1)
	movf	(left_shifted@temp_initial),w
	movwf	(left_shifted@current_temp)

	line	112
	
l2145:	
;main.c: 112: if( current_temp > (0x10000000))
	movlw	010h
	subwf	(left_shifted@current_temp+3),w
	skipz
	goto	u2415
	movlw	0
	subwf	(left_shifted@current_temp+2),w
	skipz
	goto	u2415
	movlw	0
	subwf	(left_shifted@current_temp+1),w
	skipz
	goto	u2415
	movlw	01h
	subwf	(left_shifted@current_temp),w
u2415:
	skipc
	goto	u2411
	goto	u2410
u2411:
	goto	l2153
u2410:
	line	114
	
l2147:	
;main.c: 113: {
;main.c: 114: carry_count++;
	movlw	(01h)
	movwf	(??_left_shifted+0)+0
	movf	(??_left_shifted+0)+0,w
	addwf	(left_shifted@carry_count),f
	line	115
	
l2149:	
;main.c: 115: if (carry_count == (4))
	movf	(left_shifted@carry_count),w
	xorlw	04h
	skipz
	goto	u2421
	goto	u2420
u2421:
	goto	l2153
u2420:
	line	117
	
l2151:	
;main.c: 116: {
;main.c: 117: overflow_count = (4);
	movlw	(04h)
	movwf	(??_left_shifted+0)+0
	movf	(??_left_shifted+0)+0,w
	movwf	(left_shifted@overflow_count)
	line	118
;main.c: 118: }
	goto	l2153
	line	119
	
l865:	
	goto	l2153
	line	121
;main.c: 119: else
;main.c: 120: {
	
l866:	
	line	122
;main.c: 121: }
;main.c: 122: }
	goto	l2153
	line	123
	
l864:	
	goto	l2153
	line	125
;main.c: 123: else
;main.c: 124: {
	
l867:	
	goto	l2153
	line	126
	
l862:	
	line	107
	
l2153:	
	movf	(left_shifted@temp_times),w
	subwf	(left_shifted@i),w
	skipnc
	goto	u2431
	goto	u2430
u2431:
	goto	l2157
u2430:
	
l2155:	
	movlw	(04h)
	subwf	(left_shifted@overflow_count),w
	skipc
	goto	u2441
	goto	u2440
u2441:
	goto	l2141
u2440:
	goto	l2157
	
l869:	
	goto	l2157
	
l870:	
	line	128
	
l2157:	
;main.c: 125: }
;main.c: 126: }
;main.c: 128: new_initial_temp = current_temp;
	movf	(left_shifted@current_temp+3),w
	movwf	(left_shifted@new_initial_temp+3)
	movf	(left_shifted@current_temp+2),w
	movwf	(left_shifted@new_initial_temp+2)
	movf	(left_shifted@current_temp+1),w
	movwf	(left_shifted@new_initial_temp+1)
	movf	(left_shifted@current_temp),w
	movwf	(left_shifted@new_initial_temp)

	line	129
;main.c: 129: return new_initial_temp;
	movf	(left_shifted@new_initial_temp+3),w
	movwf	(?_left_shifted+3)
	movf	(left_shifted@new_initial_temp+2),w
	movwf	(?_left_shifted+2)
	movf	(left_shifted@new_initial_temp+1),w
	movwf	(?_left_shifted+1)
	movf	(left_shifted@new_initial_temp),w
	movwf	(?_left_shifted)

	goto	l871
	
l2159:	
	line	130
	
l871:	
	return
	opt stack 0
GLOBAL	__end_of_left_shifted
	__end_of_left_shifted:
;; =============== function _left_shifted ends ============

	signat	_left_shifted,8316
	global	_security_alg
psect	text128,local,class=CODE,delta=2
global __ptext128
__ptext128:

;; *************** function _security_alg *****************
;; Defined at:
;;		line 17 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Security_algorithm\Sec_Seed.c"
;; Parameters:    Size  Location     Type
;;  Seed            1    wreg     PTR unsigned long 
;;		 -> security_seed_2constants(12), 
;;  Constant_1      1    0[COMMON] PTR unsigned long 
;;		 -> security_seed_2constants(12), 
;;  Constant_2      1    1[COMMON] PTR unsigned long 
;;		 -> security_seed_2constants(12), 
;; Auto vars:     Size  Location     Type
;;  Seed            1   12[COMMON] PTR unsigned long 
;;		 -> security_seed_2constants(12), 
;;  Key             4    8[COMMON] unsigned long 
;;  Constant_2_l    4    4[COMMON] unsigned long 
;; Return value:  Size  Location     Type
;;                  4    0[COMMON] unsigned long 
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         4       0       0       0       0
;;      Locals:         9       0       0       0       0
;;      Temps:          0      32       0       0       0
;;      Totals:        13      32       0       0       0
;;Total ram usage:       45 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text128
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Security_algorithm\Sec_Seed.c"
	line	17
	global	__size_of_security_alg
	__size_of_security_alg	equ	__end_of_security_alg-_security_alg
	
_security_alg:	
	opt	stack 7
; Regs used in _security_alg: [wreg-fsr0h+status,2+status,0]
;security_alg@Seed stored from wreg
	line	22
	movwf	(security_alg@Seed)
	
l2117:	
;Sec_Seed.c: 18: uint32_t Key;
;Sec_Seed.c: 19: uint32_t Constant_2_local_var;
;Sec_Seed.c: 21: Key = ((*Seed << 8) & 0xFF000000) + ((*Seed >> 8) & 0x00FF0000) +
;Sec_Seed.c: 22: ((*Seed << 8) & 0x0000FF00) + ((*Seed >> 8) & 0x000000FF);
	movlw	0FFh
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	((??_security_alg+0)+0)
	movlw	0
	movwf	((??_security_alg+0)+0+1)
	movlw	0
	movwf	((??_security_alg+0)+0+2)
	movlw	0
	movwf	((??_security_alg+0)+0+3)
	movf	(security_alg@Seed),w
	movwf	fsr0
	bcf	status, 7	;select IRP bank0
	movf	indf,w
	movwf	(??_security_alg+4)+0+0
	incf	fsr0,f
	movf	indf,w
	movwf	(??_security_alg+4)+0+1
	incf	fsr0,f
	movf	indf,w
	movwf	(??_security_alg+4)+0+2
	incf	fsr0,f
	movf	indf,w
	movwf	(??_security_alg+4)+0+3
	movlw	08h
u2305:
	clrc
	rrf	(??_security_alg+4)+3,f
	rrf	(??_security_alg+4)+2,f
	rrf	(??_security_alg+4)+1,f
	rrf	(??_security_alg+4)+0,f
u2300:
	addlw	-1
	skipz
	goto	u2305
	movf	0+(??_security_alg+4)+0,w
	andwf	(??_security_alg+0)+0,f
	movf	1+(??_security_alg+4)+0,w
	andwf	(??_security_alg+0)+1,f
	movf	2+(??_security_alg+4)+0,w
	andwf	(??_security_alg+0)+2,f
	movf	3+(??_security_alg+4)+0,w
	andwf	(??_security_alg+0)+3,f
	movlw	0
	movwf	((??_security_alg+8)+0)
	movlw	0FFh
	movwf	((??_security_alg+8)+0+1)
	movlw	0
	movwf	((??_security_alg+8)+0+2)
	movlw	0
	movwf	((??_security_alg+8)+0+3)
	movf	(security_alg@Seed),w
	movwf	fsr0
	movf	indf,w
	movwf	(??_security_alg+12)+0+0
	incf	fsr0,f
	movf	indf,w
	movwf	(??_security_alg+12)+0+1
	incf	fsr0,f
	movf	indf,w
	movwf	(??_security_alg+12)+0+2
	incf	fsr0,f
	movf	indf,w
	movwf	(??_security_alg+12)+0+3
	movlw	08h
u2315:
	clrc
	rlf	(??_security_alg+12)+0,f
	rlf	(??_security_alg+12)+1,f
	rlf	(??_security_alg+12)+2,f
	rlf	(??_security_alg+12)+3,f
u2310:
	addlw	-1
	skipz
	goto	u2315
	movf	0+(??_security_alg+12)+0,w
	andwf	(??_security_alg+8)+0,f
	movf	1+(??_security_alg+12)+0,w
	andwf	(??_security_alg+8)+1,f
	movf	2+(??_security_alg+12)+0,w
	andwf	(??_security_alg+8)+2,f
	movf	3+(??_security_alg+12)+0,w
	andwf	(??_security_alg+8)+3,f
	movlw	0
	movwf	((??_security_alg+16)+0)
	movlw	0
	movwf	((??_security_alg+16)+0+1)
	movlw	0FFh
	movwf	((??_security_alg+16)+0+2)
	movlw	0
	movwf	((??_security_alg+16)+0+3)
	movf	(security_alg@Seed),w
	movwf	fsr0
	movf	indf,w
	movwf	(??_security_alg+20)+0+0
	incf	fsr0,f
	movf	indf,w
	movwf	(??_security_alg+20)+0+1
	incf	fsr0,f
	movf	indf,w
	movwf	(??_security_alg+20)+0+2
	incf	fsr0,f
	movf	indf,w
	movwf	(??_security_alg+20)+0+3
	movlw	08h
u2325:
	clrc
	rrf	(??_security_alg+20)+3,f
	rrf	(??_security_alg+20)+2,f
	rrf	(??_security_alg+20)+1,f
	rrf	(??_security_alg+20)+0,f
u2320:
	addlw	-1
	skipz
	goto	u2325
	movf	0+(??_security_alg+20)+0,w
	andwf	(??_security_alg+16)+0,f
	movf	1+(??_security_alg+20)+0,w
	andwf	(??_security_alg+16)+1,f
	movf	2+(??_security_alg+20)+0,w
	andwf	(??_security_alg+16)+2,f
	movf	3+(??_security_alg+20)+0,w
	andwf	(??_security_alg+16)+3,f
	movlw	0
	movwf	((??_security_alg+24)+0)
	movlw	0
	movwf	((??_security_alg+24)+0+1)
	movlw	0
	movwf	((??_security_alg+24)+0+2)
	movlw	0FFh
	movwf	((??_security_alg+24)+0+3)
	movf	(security_alg@Seed),w
	movwf	fsr0
	movf	indf,w
	movwf	(??_security_alg+28)+0+0
	incf	fsr0,f
	movf	indf,w
	movwf	(??_security_alg+28)+0+1
	incf	fsr0,f
	movf	indf,w
	movwf	(??_security_alg+28)+0+2
	incf	fsr0,f
	movf	indf,w
	movwf	(??_security_alg+28)+0+3
	movlw	08h
u2335:
	clrc
	rlf	(??_security_alg+28)+0,f
	rlf	(??_security_alg+28)+1,f
	rlf	(??_security_alg+28)+2,f
	rlf	(??_security_alg+28)+3,f
u2330:
	addlw	-1
	skipz
	goto	u2335
	movf	0+(??_security_alg+28)+0,w
	andwf	(??_security_alg+24)+0,f
	movf	1+(??_security_alg+28)+0,w
	andwf	(??_security_alg+24)+1,f
	movf	2+(??_security_alg+28)+0,w
	andwf	(??_security_alg+24)+2,f
	movf	3+(??_security_alg+28)+0,w
	andwf	(??_security_alg+24)+3,f
	movf	0+(??_security_alg+24)+0,w
	addwf	(??_security_alg+16)+0,f
	movf	1+(??_security_alg+24)+0,w
	skipnc
	incfsz	1+(??_security_alg+24)+0,w
	goto	u2340
	goto	u2341
u2340:
	addwf	(??_security_alg+16)+1,f
u2341:
	movf	2+(??_security_alg+24)+0,w
	skipnc
	incfsz	2+(??_security_alg+24)+0,w
	goto	u2342
	goto	u2343
u2342:
	addwf	(??_security_alg+16)+2,f
u2343:
	movf	3+(??_security_alg+24)+0,w
	skipnc
	incf	3+(??_security_alg+24)+0,w
	addwf	(??_security_alg+16)+3,f
	movf	0+(??_security_alg+16)+0,w
	addwf	(??_security_alg+8)+0,f
	movf	1+(??_security_alg+16)+0,w
	skipnc
	incfsz	1+(??_security_alg+16)+0,w
	goto	u2350
	goto	u2351
u2350:
	addwf	(??_security_alg+8)+1,f
u2351:
	movf	2+(??_security_alg+16)+0,w
	skipnc
	incfsz	2+(??_security_alg+16)+0,w
	goto	u2352
	goto	u2353
u2352:
	addwf	(??_security_alg+8)+2,f
u2353:
	movf	3+(??_security_alg+16)+0,w
	skipnc
	incf	3+(??_security_alg+16)+0,w
	addwf	(??_security_alg+8)+3,f
	movf	0+(??_security_alg+8)+0,w
	addwf	(??_security_alg+0)+0,f
	movf	1+(??_security_alg+8)+0,w
	skipnc
	incfsz	1+(??_security_alg+8)+0,w
	goto	u2360
	goto	u2361
u2360:
	addwf	(??_security_alg+0)+1,f
u2361:
	movf	2+(??_security_alg+8)+0,w
	skipnc
	incfsz	2+(??_security_alg+8)+0,w
	goto	u2362
	goto	u2363
u2362:
	addwf	(??_security_alg+0)+2,f
u2363:
	movf	3+(??_security_alg+8)+0,w
	skipnc
	incf	3+(??_security_alg+8)+0,w
	addwf	(??_security_alg+0)+3,f
	movf	3+(??_security_alg+0)+0,w
	movwf	(security_alg@Key+3)
	movf	2+(??_security_alg+0)+0,w
	movwf	(security_alg@Key+2)
	movf	1+(??_security_alg+0)+0,w
	movwf	(security_alg@Key+1)
	movf	0+(??_security_alg+0)+0,w
	movwf	(security_alg@Key)

	line	24
	
l2119:	
;Sec_Seed.c: 24: Key = ((Key << 11) + (Key >> 22)) & 0xFFFFFFFF;
	movf	(security_alg@Key),w
	movwf	(??_security_alg+0)+0
	movf	(security_alg@Key+1),w
	movwf	((??_security_alg+0)+0+1)
	movf	(security_alg@Key+2),w
	movwf	((??_security_alg+0)+0+2)
	movf	(security_alg@Key+3),w
	movwf	((??_security_alg+0)+0+3)
	movlw	016h
u2375:
	clrc
	rrf	(??_security_alg+0)+3,f
	rrf	(??_security_alg+0)+2,f
	rrf	(??_security_alg+0)+1,f
	rrf	(??_security_alg+0)+0,f
u2370:
	addlw	-1
	skipz
	goto	u2375
	movf	(security_alg@Key),w
	movwf	(??_security_alg+4)+0
	movf	(security_alg@Key+1),w
	movwf	((??_security_alg+4)+0+1)
	movf	(security_alg@Key+2),w
	movwf	((??_security_alg+4)+0+2)
	movf	(security_alg@Key+3),w
	movwf	((??_security_alg+4)+0+3)
	movlw	0Bh
u2385:
	clrc
	rlf	(??_security_alg+4)+0,f
	rlf	(??_security_alg+4)+1,f
	rlf	(??_security_alg+4)+2,f
	rlf	(??_security_alg+4)+3,f
u2380:
	addlw	-1
	skipz
	goto	u2385
	movf	0+(??_security_alg+4)+0,w
	addwf	(??_security_alg+0)+0,f
	movf	1+(??_security_alg+4)+0,w
	skipnc
	incfsz	1+(??_security_alg+4)+0,w
	goto	u2390
	goto	u2391
u2390:
	addwf	(??_security_alg+0)+1,f
u2391:
	movf	2+(??_security_alg+4)+0,w
	skipnc
	incfsz	2+(??_security_alg+4)+0,w
	goto	u2392
	goto	u2393
u2392:
	addwf	(??_security_alg+0)+2,f
u2393:
	movf	3+(??_security_alg+4)+0,w
	skipnc
	incf	3+(??_security_alg+4)+0,w
	addwf	(??_security_alg+0)+3,f
	movf	3+(??_security_alg+0)+0,w
	movwf	(security_alg@Key+3)
	movf	2+(??_security_alg+0)+0,w
	movwf	(security_alg@Key+2)
	movf	1+(??_security_alg+0)+0,w
	movwf	(security_alg@Key+1)
	movf	0+(??_security_alg+0)+0,w
	movwf	(security_alg@Key)

	line	26
	
l2121:	
;Sec_Seed.c: 26: Constant_2_local_var = (*Constant_2 & *Seed) & 0xFFFFFFFF;
	movf	(security_alg@Seed),w
	movwf	fsr0
	movf	indf,w
	movwf	(??_security_alg+0)+0+0
	incf	fsr0,f
	movf	indf,w
	movwf	(??_security_alg+0)+0+1
	incf	fsr0,f
	movf	indf,w
	movwf	(??_security_alg+0)+0+2
	incf	fsr0,f
	movf	indf,w
	movwf	(??_security_alg+0)+0+3
	movf	(security_alg@Constant_2),w
	movwf	fsr0
	movf	indf,w
	movwf	(??_security_alg+4)+0+0
	incf	fsr0,f
	movf	indf,w
	movwf	(??_security_alg+4)+0+1
	incf	fsr0,f
	movf	indf,w
	movwf	(??_security_alg+4)+0+2
	incf	fsr0,f
	movf	indf,w
	movwf	(??_security_alg+4)+0+3
	movf	0+(??_security_alg+0)+0,w
	andwf	0+(??_security_alg+4)+0,w
	movwf	(security_alg@Constant_2_local_var)
	movf	1+(??_security_alg+0)+0,w
	andwf	1+(??_security_alg+4)+0,w
	movwf	1+(security_alg@Constant_2_local_var)
	movf	2+(??_security_alg+0)+0,w
	andwf	2+(??_security_alg+4)+0,w
	movwf	2+(security_alg@Constant_2_local_var)
	movf	3+(??_security_alg+0)+0,w
	andwf	3+(??_security_alg+4)+0,w
	movwf	3+(security_alg@Constant_2_local_var)
	line	29
	
l2123:	
;Sec_Seed.c: 29: Key = (Key ^ *Constant_1 ^ Constant_2_local_var) & 0xFFFFFFFF;
	movf	(security_alg@Constant_1),w
	movwf	fsr0
	movf	(security_alg@Key),w
	xorwf	indf,w
	movwf	(??_security_alg+0)+0
	incf	fsr0,f
	movf	(security_alg@Key+1),w
	xorwf	indf,w
	movwf	((??_security_alg+0)+0+1)
	incf	fsr0,f
	movf	(security_alg@Key+2),w
	xorwf	indf,w
	movwf	((??_security_alg+0)+0+2)
	incf	fsr0,f
	movf	(security_alg@Key+3),w
	xorwf	indf,w
	movwf	((??_security_alg+0)+0+3)
	movf	(security_alg@Constant_2_local_var),w
	xorwf	0+(??_security_alg+0)+0,w
	movwf	(security_alg@Key)
	movf	(security_alg@Constant_2_local_var+1),w
	xorwf	1+(??_security_alg+0)+0,w
	movwf	1+(security_alg@Key)
	movf	(security_alg@Constant_2_local_var+2),w
	xorwf	2+(??_security_alg+0)+0,w
	movwf	2+(security_alg@Key)
	movf	(security_alg@Constant_2_local_var+3),w
	xorwf	3+(??_security_alg+0)+0,w
	movwf	3+(security_alg@Key)
	line	32
	
l2125:	
;Sec_Seed.c: 32: return Key;
	movf	(security_alg@Key+3),w
	movwf	(?_security_alg+3)
	movf	(security_alg@Key+2),w
	movwf	(?_security_alg+2)
	movf	(security_alg@Key+1),w
	movwf	(?_security_alg+1)
	movf	(security_alg@Key),w
	movwf	(?_security_alg)

	goto	l876
	
l2127:	
	line	33
	
l876:	
	return
	opt stack 0
GLOBAL	__end_of_security_alg
	__end_of_security_alg:
;; =============== function _security_alg ends ============

	signat	_security_alg,12412
	global	_sfreg
psect	text129,local,class=CODE,delta=2
global __ptext129
__ptext129:

;; *************** function _sfreg *****************
;; Defined at:
;;		line 72 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Security_algorithm\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text129
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Security_algorithm\main.c"
	line	72
	global	__size_of_sfreg
	__size_of_sfreg	equ	__end_of_sfreg-_sfreg
	
_sfreg:	
	opt	stack 7
; Regs used in _sfreg: [wreg+status,2]
	line	73
	
l2107:	
;main.c: 73: WDTCON = 0x17;
	movlw	(017h)
	bcf	status, 5	;RP0=0, select bank2
	bsf	status, 6	;RP1=1, select bank2
	movwf	(261)^0100h	;volatile
	line	74
;main.c: 74: OSCCON = 0x61;
	movlw	(061h)
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movwf	(143)^080h	;volatile
	line	75
	
l2109:	
;main.c: 75: OPTION = 0x00;
	clrf	(129)^080h	;volatile
	line	76
	
l2111:	
;main.c: 76: TRISE = 0x01;
	movlw	(01h)
	movwf	(137)^080h	;volatile
	line	77
;main.c: 77: TRISD = 0x00;
	clrf	(136)^080h	;volatile
	line	78
	
l2113:	
;main.c: 78: T1CON = 0x35;
	movlw	(035h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(16)	;volatile
	line	79
	
l2115:	
;main.c: 79: ANSEL &=0x00;
	bsf	status, 5	;RP0=1, select bank3
	bsf	status, 6	;RP1=1, select bank3
	clrf	(392)^0180h	;volatile
	line	80
	
l859:	
	return
	opt stack 0
GLOBAL	__end_of_sfreg
	__end_of_sfreg:
;; =============== function _sfreg ends ============

	signat	_sfreg,88
psect	text130,local,class=CODE,delta=2
global __ptext130
__ptext130:
	global	btemp
	btemp set 07Eh

	DABS	1,126,2	;btemp
	global	wtemp0
	wtemp0 set btemp
	end
