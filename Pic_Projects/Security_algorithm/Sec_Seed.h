/*****************************************************************************/
/* FILE NAME:  Sec_seed.h                                                    */
/*                                                                           */
/* Description:  Constanst for security algorithm.                            */
/*****************************************************************************/

#include "typedefs.h"

#ifndef SECSEED_H
#define SECSEED_H

/* Constants for example*/ 
#define CONSTANT_1  0x6C8FE84F
#define CONSTANT_2  0x9C30D3BE
 

/* Function definitions */
extern uint32_t security_alg(uint32_t *Seed, uint32_t *Constant_1, uint32_t *Constant_2);

#endif
