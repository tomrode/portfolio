#include "htc.h"
#include "typedefs.h"
#include "sec_Seed.h"
#define  MAX_OVERFLOW     (4)
#define  OVERFLOW_TRIGGER (0x10000000) 
/*PIC16F887*/
__CONFIG(DEBUGEN & LVPEN & FCMEN & IESODIS & BORXSLP & DUNPROTECT & UNPROTECT & MCLREN & PWRTEN & WDTDIS & INTCLK);//HS);
__CONFIG(BORV40);
/* STRUCTURES */
 
/* Function prototypes*/
void sfreg(void);                                   // Initializr  the hard ware
uint32_t left_shifted(uint32_t initial, uint8_t times);
/* non writeable ram variable location*/
persistent int persistent_watch_dog_count;

/* Volatile variable*/
vuint8_t tmr1_counter;      // volatile isr variable for timer 1

/*arrays*/

uint32_t security_seed_2constants[] = {
                                        0xFBCCA440,  /* index 0 seed value*/
                                        0x6C8FE84F,  /* index 1 constant 1*/
                                        0x9C30D3BE   /* index 2 constant 2*/
                                      }; 

uint32_t security_seed_3constants[] = {
                                        0x00000010,  /* index 0 seed value*/
                                        0x00000020,  /* index 1 constant 1*/
                                        0x00000030   /* index 2 constant 2*/
                                      }; 
uint32_t *security_seed_2constant_ptr;
 /*********************************************************************/
/*        THE MAIN LOOP                                              */         
/*********************************************************************/
int main (void)
{
 
  /* variables for the main*/
  uint8_t  watchdog;
  uint32_t returned_keyval;                           // A test for SEED and KEY algo
  uint32_t returned_shift;
  uint16_t *ptr1;
  uint16_t **ptr2; 
  uint16_t ***ptr3; 
  uint16_t aVar = 0x3D55; 
  uint16_t Val;
/***********************************************************************/
/*          INITIALIZATIONS
/***********************************************************************/

  ptr1 = &aVar;
  ptr2 = &ptr1;
  ptr3 = &ptr2;
  Val = ***ptr3;
  
  sfreg();                                            // initialize sfr's   
  di();                                               // disable all interrupts
  
/*************************************************************************/
/*           CODE WILL REMAIN IN while loop if no watch dog reset
/*************************************************************************/ 
// while (HTS == 0)
//  {}                                                 // wait until clock stable 

  while(1)
  {
    if(TO == 1)                                       // No watchdog reset proceed with adc read      
    {
     asm("nop");                     // Seed   //CONSTANT_1  //CONSTANT_2
     //returned_keyval = security_alg(0xFBCCA440,0x6C8FE84F, 0x9C30D3BE);
     returned_keyval = security_alg(&security_seed_2constants[0],&security_seed_2constants[1], &security_seed_2constants[2]); 
     

     returned_shift =  left_shifted(0x00F0000, 100);        // Give a vlaue and times to shift it left
     CLRWDT();
     }
    else                                            // a watchdog reset happend update this count
    { 
      watchdog = TO;
      PORTD = persistent_watch_dog_count++;         // Keep a un-accessed RAM count of wdt 
      CLRWDT();
    }
  }
}

void sfreg(void)
{
WDTCON = 0x17;       // WDTPS3=1|WDTPS2=0|WDTPS1=1|WDTPS0=1|SWDTEN=1 5 seconds before watch dog expires
OSCCON = 0x61;       // 4 Mhz clock
OPTION = 0x00;       // |PSA=Prescale to WDT|most prescale rate|
TRISE  = 0x01;       // MAKE PORTE RE1 an input
TRISD  = 0x00;       // Port D as outputs
T1CON  = 0x35;       // T1CKPS1,T1CKPS0 = 1:8 prescaler,T1SYNC = do not synchronise external clock input ,TMR1ON = enabled
ANSEL  &=0x00;       // Make a digital I/O
}

/*************************************************************************************************************************/
/*  Function Name:  left_shifted();                                                                                      */
/*  Description:    This function takes in a 32 bit value and shifts the returned value by a number                      */
/*                  specified from the second input parameter.                                                           */                                                               
/*                                                                                                                       */
/*  Inputs:         initial -> 32 bit value to be shifted left.                                                          */
/*                  times   -> 8 bit vale of Desired times to shift left.                                                */
/*                                                                                                                       */
/*  Outputs:        result -> 32 bit left shifted value of a desired amount of times.                                    */
/*                                                                                                                       */ 
/*  Caveat:         Code portable on any device.                                                                         */
/*************************************************************************************************************************/

uint32_t left_shifted(uint32_t initial, uint8_t times)  
{
  /* variables */
  uint8_t temp_times = times;
  uint8_t i = 0;
  uint8_t overflow_count = 0;
  uint8_t carry_count = 0;
  uint32_t temp_initial = initial;
  uint32_t new_initial_temp;
  uint32_t current_temp;

  current_temp = initial;                                       /* get this value from function call */ 
  while ((i < temp_times) && (overflow_count < MAX_OVERFLOW))   /* This will prevent overflow */
  { 
    i++;
    temp_initial = (current_temp << 1);                         /* shifting left action */
    current_temp = temp_initial;                                /* new value after shift */ 
    if( current_temp > OVERFLOW_TRIGGER)                        /* make a count to not exceed over flow */ 
    {
      carry_count++;
      if (carry_count == MAX_OVERFLOW)                          /* Carry count = 4 and not Part specific bit used */ 
      {
        overflow_count = MAX_OVERFLOW;                          /* make overflow_count = to 4 when reentering while loop test condition */ 
      }
      else
      {
      }
    }
    else
    {
    }                                           
  }

  new_initial_temp = current_temp;                              /* the value to be passed back with desired amount of shifts */                               
  return new_initial_temp;
}