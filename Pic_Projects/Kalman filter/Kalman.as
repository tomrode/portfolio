opt subtitle "HI-TECH Software Omniscient Code Generator (Lite mode) build 6738"

opt pagewidth 120

	opt lm

	processor	16F887
clrc	macro
	bcf	3,0
	endm
clrz	macro
	bcf	3,2
	endm
setc	macro
	bsf	3,0
	endm
setz	macro
	bsf	3,2
	endm
skipc	macro
	btfss	3,0
	endm
skipz	macro
	btfss	3,2
	endm
skipnc	macro
	btfsc	3,0
	endm
skipnz	macro
	btfsc	3,2
	endm
indf	equ	0
indf0	equ	0
pc	equ	2
pcl	equ	2
status	equ	3
fsr	equ	4
fsr0	equ	4
c	equ	1
z	equ	0
pclath	equ	10
# 8 "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Kalman filter\Src\main.c"
	psect config,class=CONFIG,delta=2 ;#
# 8 "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Kalman filter\Src\main.c"
	dw 0x1FFF & 0x3FFF & 0x3FFF & 0x3BFF & 0x3EFF & 0x3FFF & 0x3FFF & 0x3FFF & 0x3FEF & 0x3FF7 & 0x3FFD & 0x3FBF ;#
# 9 "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Kalman filter\Src\main.c"
	psect config,class=CONFIG,delta=2 ;#
# 9 "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Kalman filter\Src\main.c"
	dw 0x3FFF ;#
	FNCALL	_main,_KalmanFilter
	FNCALL	_main,_Eric_main
	FNCALL	_Eric_main,_kalman_init
	FNCALL	_Eric_main,___fttol
	FNCALL	_Eric_main,_kalman_set
	FNCALL	_Eric_main,_kalman_filter
	FNCALL	_Eric_main,_kalman_get_filtered_level
	FNCALL	_Eric_main,___ftadd
	FNCALL	_kalman_filter,___ftmul
	FNCALL	_kalman_filter,___ftadd
	FNCALL	_kalman_filter,___ftdiv
	FNCALL	_kalman_filter,___ftneg
	FNCALL	_kalman_set,___altoft
	FNCALL	_KalmanFilter,___ftadd
	FNCALL	_KalmanFilter,___ftdiv
	FNCALL	_KalmanFilter,___ftneg
	FNCALL	_KalmanFilter,___lbtoft
	FNCALL	_KalmanFilter,___ftmul
	FNCALL	_KalmanFilter,___fttol
	FNCALL	_KalmanFilter,___lwtoft
	FNCALL	___altoft,___ftpack
	FNCALL	___lwtoft,___ftpack
	FNCALL	___lbtoft,___ftpack
	FNCALL	___ftmul,___ftpack
	FNCALL	___ftdiv,___ftpack
	FNCALL	___ftadd,___ftpack
	FNROOT	_main
	global	KalmanFilter@MeasErr
psect	strings,class=STRING,delta=2
global __pstrings
__pstrings:
;	global	stringdir,stringtab,__stringbase
stringtab:
;	String table - string pointers are 1 byte each
stringcode:stringdir:
movlw high(stringdir)
movwf pclath
movf fsr,w
incf fsr
	addwf pc
__stringbase:
	retlw	0
psect	strings
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Kalman filter\Src\main.c"
	line	66
KalmanFilter@MeasErr:
	retlw	0x0
	retlw	0x80
	retlw	0x40

	global	KalmanFilter@GainScaler
psect	strings
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Kalman filter\Src\main.c"
	line	70
KalmanFilter@GainScaler:
	retlw	02h
	global	KalmanFilter@MeasErr
	global	KalmanFilter@GainScaler
	global	KalmanFilter@KalGain
	global	KalmanFilter@CurrentEst
	global	KalmanFilter@CurrentEstErr
	global	KalmanFilter@PreviousEst
	global	_toms_variable
	global	KalmanFilter@PreviousEstErr
	file	"Kalman.as"
	line	#
psect cinit,class=CODE,delta=2
global start_initialization
start_initialization:

psect	bssBANK0,class=BANK0,space=1
global __pbssBANK0
__pbssBANK0:
KalmanFilter@KalGain:
       ds      3

psect	bssBANK1,class=BANK1,space=1
global __pbssBANK1
__pbssBANK1:
KalmanFilter@CurrentEst:
       ds      3

KalmanFilter@CurrentEstErr:
       ds      3

KalmanFilter@PreviousEst:
       ds      3

_toms_variable:
       ds      1

KalmanFilter@PreviousEstErr:
       ds      3

; Clear objects allocated to BANK0
psect cinit,class=CODE,delta=2
	clrf	((__pbssBANK0)+0)&07Fh
	clrf	((__pbssBANK0)+1)&07Fh
	clrf	((__pbssBANK0)+2)&07Fh
; Clear objects allocated to BANK1
psect cinit,class=CODE,delta=2
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	clrf	((__pbssBANK1)+0)&07Fh
	clrf	((__pbssBANK1)+1)&07Fh
	clrf	((__pbssBANK1)+2)&07Fh
	clrf	((__pbssBANK1)+3)&07Fh
	clrf	((__pbssBANK1)+4)&07Fh
	clrf	((__pbssBANK1)+5)&07Fh
	clrf	((__pbssBANK1)+6)&07Fh
	clrf	((__pbssBANK1)+7)&07Fh
	clrf	((__pbssBANK1)+8)&07Fh
	clrf	((__pbssBANK1)+9)&07Fh
	clrf	((__pbssBANK1)+10)&07Fh
	clrf	((__pbssBANK1)+11)&07Fh
	clrf	((__pbssBANK1)+12)&07Fh
psect cinit,class=CODE,delta=2
global end_of_initialization

;End of C runtime variable initialization code

end_of_initialization:
clrf status
ljmp _main	;jump to C main() function
psect	cstackBANK1,class=BANK1,space=1
global __pcstackBANK1
__pcstackBANK1:
	global	Eric_main@SystemStateLevel
Eric_main@SystemStateLevel:	; 4 bytes @ 0x0
	ds	4
	global	Eric_main@i
Eric_main@i:	; 3 bytes @ 0x4
	ds	3
	global	Eric_main@measurement
Eric_main@measurement:	; 3 bytes @ 0x7
	ds	3
	global	Eric_main@LoopCnt
Eric_main@LoopCnt:	; 2 bytes @ 0xA
	ds	2
	global	Eric_main@kalman
Eric_main@kalman:	; 34 bytes @ 0xC
	ds	34
psect	cstackCOMMON,class=COMMON,space=1
global __pcstackCOMMON
__pcstackCOMMON:
	global	?_main
?_main:	; 0 bytes @ 0x0
	global	??_kalman_init
??_kalman_init:	; 0 bytes @ 0x0
	global	?_kalman_filter
?_kalman_filter:	; 0 bytes @ 0x0
	global	?_Eric_main
?_Eric_main:	; 2 bytes @ 0x0
	global	?___ftpack
?___ftpack:	; 3 bytes @ 0x0
	global	?_kalman_get_filtered_level
?_kalman_get_filtered_level:	; 3 bytes @ 0x0
	global	___ftpack@arg
___ftpack@arg:	; 3 bytes @ 0x0
	ds	3
	global	??_kalman_get_filtered_level
??_kalman_get_filtered_level:	; 0 bytes @ 0x3
	global	kalman_get_filtered_level@kalman
kalman_get_filtered_level@kalman:	; 1 bytes @ 0x3
	global	___ftpack@exp
___ftpack@exp:	; 1 bytes @ 0x3
	ds	1
	global	?___fttol
?___fttol:	; 4 bytes @ 0x4
	global	___ftpack@sign
___ftpack@sign:	; 1 bytes @ 0x4
	global	___fttol@f1
___fttol@f1:	; 3 bytes @ 0x4
	ds	1
	global	??___ftpack
??___ftpack:	; 0 bytes @ 0x5
	ds	3
	global	??___fttol
??___fttol:	; 0 bytes @ 0x8
	global	?___lbtoft
?___lbtoft:	; 3 bytes @ 0x8
	global	?___ftneg
?___ftneg:	; 3 bytes @ 0x8
	global	?___altoft
?___altoft:	; 3 bytes @ 0x8
	global	___ftneg@f1
___ftneg@f1:	; 3 bytes @ 0x8
	global	___altoft@c
___altoft@c:	; 4 bytes @ 0x8
	ds	3
	global	??___ftneg
??___ftneg:	; 0 bytes @ 0xB
	global	___lbtoft@c
___lbtoft@c:	; 1 bytes @ 0xB
	ds	1
	global	??_kalman_filter
??_kalman_filter:	; 0 bytes @ 0xC
	global	??___lwtoft
??___lwtoft:	; 0 bytes @ 0xC
	global	??___altoft
??___altoft:	; 0 bytes @ 0xC
	ds	1
	global	??_KalmanFilter
??_KalmanFilter:	; 0 bytes @ 0xD
	ds	1
	global	??_main
??_main:	; 0 bytes @ 0xE
	global	??_kalman_set
??_kalman_set:	; 0 bytes @ 0xE
psect	cstackBANK0,class=BANK0,space=1
global __pcstackBANK0
__pcstackBANK0:
	global	??___lbtoft
??___lbtoft:	; 0 bytes @ 0x0
	global	?_kalman_init
?_kalman_init:	; 34 bytes @ 0x0
	global	___fttol@sign1
___fttol@sign1:	; 1 bytes @ 0x0
	global	___altoft@exp
___altoft@exp:	; 1 bytes @ 0x0
	ds	1
	global	___altoft@sign
___altoft@sign:	; 1 bytes @ 0x1
	global	___fttol@lval
___fttol@lval:	; 4 bytes @ 0x1
	ds	3
	global	?___ftmul
?___ftmul:	; 3 bytes @ 0x4
	global	___ftmul@f1
___ftmul@f1:	; 3 bytes @ 0x4
	ds	1
	global	___fttol@exp1
___fttol@exp1:	; 1 bytes @ 0x5
	ds	1
	global	?_kalman_set
?_kalman_set:	; 0 bytes @ 0x6
	global	?___lwtoft
?___lwtoft:	; 3 bytes @ 0x6
	global	___lwtoft@c
___lwtoft@c:	; 2 bytes @ 0x6
	global	kalman_set@z
kalman_set@z:	; 4 bytes @ 0x6
	ds	1
	global	___ftmul@f2
___ftmul@f2:	; 3 bytes @ 0x7
	ds	3
	global	??___ftmul
??___ftmul:	; 0 bytes @ 0xA
	global	kalman_set@u
kalman_set@u:	; 3 bytes @ 0xA
	ds	3
	global	kalman_set@b
kalman_set@b:	; 3 bytes @ 0xD
	ds	1
	global	___ftmul@exp
___ftmul@exp:	; 1 bytes @ 0xE
	ds	1
	global	___ftmul@f3_as_product
___ftmul@f3_as_product:	; 3 bytes @ 0xF
	ds	1
	global	kalman_set@a
kalman_set@a:	; 3 bytes @ 0x10
	ds	2
	global	___ftmul@cntr
___ftmul@cntr:	; 1 bytes @ 0x12
	ds	1
	global	___ftmul@sign
___ftmul@sign:	; 1 bytes @ 0x13
	global	kalman_set@c
kalman_set@c:	; 3 bytes @ 0x13
	ds	1
	global	?___ftadd
?___ftadd:	; 3 bytes @ 0x14
	global	___ftadd@f1
___ftadd@f1:	; 3 bytes @ 0x14
	ds	2
	global	kalman_set@kalman
kalman_set@kalman:	; 1 bytes @ 0x16
	ds	1
	global	___ftadd@f2
___ftadd@f2:	; 3 bytes @ 0x17
	ds	3
	global	??___ftadd
??___ftadd:	; 0 bytes @ 0x1A
	ds	4
	global	___ftadd@sign
___ftadd@sign:	; 1 bytes @ 0x1E
	ds	1
	global	___ftadd@exp2
___ftadd@exp2:	; 1 bytes @ 0x1F
	ds	1
	global	___ftadd@exp1
___ftadd@exp1:	; 1 bytes @ 0x20
	ds	1
	global	?___ftdiv
?___ftdiv:	; 3 bytes @ 0x21
	global	___ftdiv@f2
___ftdiv@f2:	; 3 bytes @ 0x21
	ds	1
	global	kalman_init@kalman
kalman_init@kalman:	; 34 bytes @ 0x22
	ds	2
	global	___ftdiv@f1
___ftdiv@f1:	; 3 bytes @ 0x24
	ds	3
	global	??___ftdiv
??___ftdiv:	; 0 bytes @ 0x27
	ds	4
	global	___ftdiv@cntr
___ftdiv@cntr:	; 1 bytes @ 0x2B
	ds	1
	global	___ftdiv@f3
___ftdiv@f3:	; 3 bytes @ 0x2C
	ds	3
	global	___ftdiv@exp
___ftdiv@exp:	; 1 bytes @ 0x2F
	ds	1
	global	___ftdiv@sign
___ftdiv@sign:	; 1 bytes @ 0x30
	ds	1
	global	?_KalmanFilter
?_KalmanFilter:	; 3 bytes @ 0x31
	global	KalmanFilter@KalStMch
KalmanFilter@KalStMch:	; 1 bytes @ 0x31
	global	_kalman_filter$922
_kalman_filter$922:	; 3 bytes @ 0x31
	ds	3
	global	kalman_filter@kalman
kalman_filter@kalman:	; 1 bytes @ 0x34
	global	KalmanFilter@InitEst
KalmanFilter@InitEst:	; 3 bytes @ 0x34
	ds	3
	global	KalmanFilter@Measurement
KalmanFilter@Measurement:	; 1 bytes @ 0x37
	ds	1
	global	_KalmanFilter$921
_KalmanFilter$921:	; 3 bytes @ 0x38
	ds	3
	global	KalmanFilter@InitEstErr
KalmanFilter@InitEstErr:	; 3 bytes @ 0x3B
	ds	9
	global	??_Eric_main
??_Eric_main:	; 0 bytes @ 0x44
	ds	4
	global	main@Filtered
main@Filtered:	; 3 bytes @ 0x48
	ds	3
	global	main@LoopCnt
main@LoopCnt:	; 2 bytes @ 0x4B
	ds	2
;;Data sizes: Strings 0, constant 4, data 0, bss 16, persistent 0 stack 0
;;Auto spaces:   Size  Autos    Used
;; COMMON          14     14      14
;; BANK0           80     77      80
;; BANK1           80     46      59
;; BANK3           96      0       0
;; BANK2           96      0       0

;;
;; Pointer list with targets:

;; ?___lbtoft	float  size(1) Largest target is 0
;;
;; ?___ftpack	float  size(1) Largest target is 34
;;		 -> Eric_main@kalman(BANK1[34]), KalmanFilter@PreviousEstErr(BANK1[3]), KalmanFilter@InitEstErr(BANK0[3]), 
;;
;; ?_kalman_get_filtered_level	float  size(1) Largest target is 0
;;
;; ?___altoft	float  size(1) Largest target is 0
;;
;; ?_kalman_init	struct . size(1) Largest target is 0
;;
;; ?___lwtoft	float  size(1) Largest target is 0
;;
;; ?___fttol	long  size(1) Largest target is 0
;;
;; ?___ftneg	float  size(1) Largest target is 3
;;		 -> KalmanFilter@PreviousEstErr(BANK1[3]), KalmanFilter@InitEstErr(BANK0[3]), 
;;
;; ?___ftmul	float  size(1) Largest target is 0
;;
;; ?___ftadd	float  size(1) Largest target is 34
;;		 -> Eric_main@kalman(BANK1[34]), KalmanFilter@PreviousEstErr(BANK1[3]), KalmanFilter@InitEstErr(BANK0[3]), 
;;
;; ?___ftdiv	float  size(1) Largest target is 0
;;
;; ?_KalmanFilter	float  size(1) Largest target is 0
;;
;; kalman_filter@kalman	PTR struct . size(1) Largest target is 34
;;		 -> Eric_main@kalman(BANK1[34]), 
;;
;; kalman_get_filtered_level@kalman	PTR struct . size(1) Largest target is 34
;;		 -> Eric_main@kalman(BANK1[34]), 
;;
;; kalman_set@kalman	PTR struct . size(1) Largest target is 34
;;		 -> Eric_main@kalman(BANK1[34]), 
;;


;;
;; Critical Paths under _main in COMMON
;;
;;   _kalman_set->___altoft
;;   _KalmanFilter->___lwtoft
;;   ___altoft->___ftpack
;;   ___lwtoft->___fttol
;;   ___lbtoft->___ftpack
;;   ___ftmul->___lbtoft
;;   ___ftdiv->___lbtoft
;;   ___ftadd->___lbtoft
;;   ___ftneg->___ftpack
;;   ___fttol->_kalman_get_filtered_level
;;
;; Critical Paths under _main in BANK0
;;
;;   _main->_Eric_main
;;   _Eric_main->_kalman_init
;;   _kalman_filter->___ftdiv
;;   _kalman_set->___fttol
;;   _KalmanFilter->___ftdiv
;;   ___lwtoft->___fttol
;;   ___ftmul->___lbtoft
;;   ___ftdiv->___ftadd
;;   ___ftadd->___ftmul
;;
;; Critical Paths under _main in BANK1
;;
;;   _main->_Eric_main
;;
;; Critical Paths under _main in BANK3
;;
;;   None.
;;
;; Critical Paths under _main in BANK2
;;
;;   None.

;;
;;Main: autosize = 0, tempsize = 0, incstack = 0, save=0
;;

;;
;;Call Graph Tables:
;;
;; ---------------------------------------------------------------------------------
;; (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;; ---------------------------------------------------------------------------------
;; (0) _main                                                 5     5      0    8585
;;                                             72 BANK0      5     5      0
;;                       _KalmanFilter
;;                          _Eric_main
;; ---------------------------------------------------------------------------------
;; (1) _Eric_main                                           50    50      0    5389
;;                                             68 BANK0      4     4      0
;;                                              0 BANK1     46    46      0
;;                        _kalman_init
;;                            ___fttol
;;                         _kalman_set
;;                      _kalman_filter
;;          _kalman_get_filtered_level
;;                            ___ftadd
;; ---------------------------------------------------------------------------------
;; (2) _kalman_filter                                       13    13      0    3058
;;                                             49 BANK0      4     4      0
;;                            ___ftmul
;;                            ___ftadd
;;                            ___ftdiv
;;                            ___ftneg
;; ---------------------------------------------------------------------------------
;; (2) _kalman_set                                          17     1     16     567
;;                                              6 BANK0     17     1     16
;;                           ___altoft
;;                            ___fttol (ARG)
;; ---------------------------------------------------------------------------------
;; (1) _KalmanFilter                                        16    13      3    3102
;;                                             49 BANK0     13    10      3
;;                            ___ftadd
;;                            ___ftdiv
;;                            ___ftneg
;;                           ___lbtoft
;;                            ___ftmul
;;                            ___fttol
;;                           ___lwtoft
;; ---------------------------------------------------------------------------------
;; (3) ___altoft                                             8     4      4     347
;;                                              8 COMMON     6     2      4
;;                                              0 BANK0      2     2      0
;;                           ___ftpack
;; ---------------------------------------------------------------------------------
;; (2) ___lwtoft                                             4     1      3     231
;;                                             12 COMMON     1     1      0
;;                                              6 BANK0      3     0      3
;;                           ___ftpack
;;                            ___fttol (ARG)
;; ---------------------------------------------------------------------------------
;; (2) ___lbtoft                                             8     5      3     231
;;                                              8 COMMON     4     1      3
;;                                              0 BANK0      4     4      0
;;                           ___ftpack
;; ---------------------------------------------------------------------------------
;; (3) ___ftmul                                             16    10      6     535
;;                                              4 BANK0     16    10      6
;;                           ___ftpack
;;                            ___ftneg (ARG)
;;                           ___lbtoft (ARG)
;; ---------------------------------------------------------------------------------
;; (3) ___ftdiv                                             16    10      6     489
;;                                             33 BANK0     16    10      6
;;                           ___ftpack
;;                            ___ftadd (ARG)
;;                            ___ftmul (ARG)
;;                           ___lbtoft (ARG)
;; ---------------------------------------------------------------------------------
;; (2) ___ftadd                                             13     7      6    1049
;;                                             20 BANK0     13     7      6
;;                           ___ftpack
;;                            ___ftmul (ARG)
;;                            ___ftneg (ARG)
;;                           ___lbtoft (ARG)
;; ---------------------------------------------------------------------------------
;; (3) ___ftneg                                              3     0      3      45
;;                                              8 COMMON     3     0      3
;;                           ___ftpack (ARG)
;; ---------------------------------------------------------------------------------
;; (2) ___fttol                                             14    10      4     252
;;                                              4 COMMON     8     4      4
;;                                              0 BANK0      6     6      0
;;          _kalman_get_filtered_level (ARG)
;; ---------------------------------------------------------------------------------
;; (3) ___ftpack                                             8     3      5     209
;;                                              0 COMMON     8     3      5
;; ---------------------------------------------------------------------------------
;; (2) _kalman_get_filtered_level                            4     1      3      22
;;                                              0 COMMON     4     1      3
;; ---------------------------------------------------------------------------------
;; (2) _kalman_init                                         72    38     34     237
;;                                              0 COMMON     4     4      0
;;                                              0 BANK0     68    34     34
;; ---------------------------------------------------------------------------------
;; Estimated maximum stack depth 3
;; ---------------------------------------------------------------------------------

;; Call Graph Graphs:

;; _main (ROOT)
;;   _KalmanFilter
;;     ___ftadd
;;       ___ftpack
;;       ___ftmul (ARG)
;;         ___ftpack
;;         ___ftneg (ARG)
;;           ___ftpack (ARG)
;;         ___lbtoft (ARG)
;;           ___ftpack
;;       ___ftneg (ARG)
;;         ___ftpack (ARG)
;;       ___lbtoft (ARG)
;;         ___ftpack
;;     ___ftdiv
;;       ___ftpack
;;       ___ftadd (ARG)
;;         ___ftpack
;;         ___ftmul (ARG)
;;           ___ftpack
;;           ___ftneg (ARG)
;;             ___ftpack (ARG)
;;           ___lbtoft (ARG)
;;             ___ftpack
;;         ___ftneg (ARG)
;;           ___ftpack (ARG)
;;         ___lbtoft (ARG)
;;           ___ftpack
;;       ___ftmul (ARG)
;;         ___ftpack
;;         ___ftneg (ARG)
;;           ___ftpack (ARG)
;;         ___lbtoft (ARG)
;;           ___ftpack
;;       ___lbtoft (ARG)
;;         ___ftpack
;;     ___ftneg
;;       ___ftpack (ARG)
;;     ___lbtoft
;;       ___ftpack
;;     ___ftmul
;;       ___ftpack
;;       ___ftneg (ARG)
;;         ___ftpack (ARG)
;;       ___lbtoft (ARG)
;;         ___ftpack
;;     ___fttol
;;       _kalman_get_filtered_level (ARG)
;;     ___lwtoft
;;       ___ftpack
;;       ___fttol (ARG)
;;         _kalman_get_filtered_level (ARG)
;;   _Eric_main
;;     _kalman_init
;;     ___fttol
;;       _kalman_get_filtered_level (ARG)
;;     _kalman_set
;;       ___altoft
;;         ___ftpack
;;       ___fttol (ARG)
;;         _kalman_get_filtered_level (ARG)
;;     _kalman_filter
;;       ___ftmul
;;         ___ftpack
;;         ___ftneg (ARG)
;;           ___ftpack (ARG)
;;         ___lbtoft (ARG)
;;           ___ftpack
;;       ___ftadd
;;         ___ftpack
;;         ___ftmul (ARG)
;;           ___ftpack
;;           ___ftneg (ARG)
;;             ___ftpack (ARG)
;;           ___lbtoft (ARG)
;;             ___ftpack
;;         ___ftneg (ARG)
;;           ___ftpack (ARG)
;;         ___lbtoft (ARG)
;;           ___ftpack
;;       ___ftdiv
;;         ___ftpack
;;         ___ftadd (ARG)
;;           ___ftpack
;;           ___ftmul (ARG)
;;             ___ftpack
;;             ___ftneg (ARG)
;;               ___ftpack (ARG)
;;             ___lbtoft (ARG)
;;               ___ftpack
;;           ___ftneg (ARG)
;;             ___ftpack (ARG)
;;           ___lbtoft (ARG)
;;             ___ftpack
;;         ___ftmul (ARG)
;;           ___ftpack
;;           ___ftneg (ARG)
;;             ___ftpack (ARG)
;;           ___lbtoft (ARG)
;;             ___ftpack
;;         ___lbtoft (ARG)
;;           ___ftpack
;;       ___ftneg
;;         ___ftpack (ARG)
;;     _kalman_get_filtered_level
;;     ___ftadd
;;       ___ftpack
;;       ___ftmul (ARG)
;;         ___ftpack
;;         ___ftneg (ARG)
;;           ___ftpack (ARG)
;;         ___lbtoft (ARG)
;;           ___ftpack
;;       ___ftneg (ARG)
;;         ___ftpack (ARG)
;;       ___lbtoft (ARG)
;;         ___ftpack
;;

;; Address spaces:

;;Name               Size   Autos  Total    Cost      Usage
;;BITCOMMON            E      0       0       0        0.0%
;;EEDATA             100      0       0       0        0.0%
;;NULL                 0      0       0       0        0.0%
;;CODE                 0      0       0       0        0.0%
;;COMMON               E      E       E       1      100.0%
;;BITSFR0              0      0       0       1        0.0%
;;SFR0                 0      0       0       1        0.0%
;;BITSFR1              0      0       0       2        0.0%
;;SFR1                 0      0       0       2        0.0%
;;STACK                0      0       4       2        0.0%
;;ABS                  0      0      99       3        0.0%
;;BITBANK0            50      0       0       4        0.0%
;;BITSFR3              0      0       0       4        0.0%
;;SFR3                 0      0       0       4        0.0%
;;BANK0               50     4D      50       5      100.0%
;;BITSFR2              0      0       0       5        0.0%
;;SFR2                 0      0       0       5        0.0%
;;BITBANK1            50      0       0       6        0.0%
;;BANK1               50     2E      3B       7       73.8%
;;BITBANK3            60      0       0       8        0.0%
;;BANK3               60      0       0       9        0.0%
;;BITBANK2            60      0       0      10        0.0%
;;BANK2               60      0       0      11        0.0%
;;DATA                 0      0      9D      12        0.0%

	global	_main
psect	maintext,global,class=CODE,delta=2
global __pmaintext
__pmaintext:

;; *************** function _main *****************
;; Defined at:
;;		line 27 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Kalman filter\Src\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  Filtered        3   72[BANK0 ] float 
;;  LoopCnt         2   75[BANK0 ] int 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, btemp+1, pclath, cstack
;; Tracked objects:
;;		On entry : 17F/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       5       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         0       5       0       0       0
;;Total ram usage:        5 bytes
;; Hardware stack levels required when called:    4
;; This function calls:
;;		_KalmanFilter
;;		_Eric_main
;; This function is called by:
;;		Startup code after reset
;; This function uses a non-reentrant model
;;
psect	maintext
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Kalman filter\Src\main.c"
	line	27
	global	__size_of_main
	__size_of_main	equ	__end_of_main-_main
	
_main:	
	opt	stack 4
; Regs used in _main: [wreg-fsr0h+status,2+status,0+btemp+1+pclath+cstack]
	line	29
	
l3437:	
;main.c: 28: float Filtered;
;main.c: 29: int LoopCnt = 0;
	movlw	low(0)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(main@LoopCnt)
	movlw	high(0)
	movwf	((main@LoopCnt))+1
	line	31
	
l3439:	
;main.c: 31: Filtered = KalmanFilter(0, INIT_MEASUREMENT);
	clrc
	movlw	0
	btfsc	status,0
	movlw	1
	movwf	(?_KalmanFilter)
	movlw	(0)
	fcall	_KalmanFilter
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(0+(?_KalmanFilter)),w
	movwf	(main@Filtered)
	movf	(1+(?_KalmanFilter)),w
	movwf	(main@Filtered+1)
	movf	(2+(?_KalmanFilter)),w
	movwf	(main@Filtered+2)
	line	32
	
l3441:	
# 32 "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Kalman filter\Src\main.c"
nop ;#
psect	maintext
	line	35
	
l3443:	
;main.c: 35: Eric_main();
	fcall	_Eric_main
	goto	l3445
	line	37
;main.c: 37: while(1)
	
l841:	
	line	39
	
l3445:	
;main.c: 38: {
;main.c: 39: for(LoopCnt =0; LoopCnt<= 1000; LoopCnt++)
	movlw	low(0)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(main@LoopCnt)
	movlw	high(0)
	movwf	((main@LoopCnt))+1
	
l3447:	
	movf	(main@LoopCnt+1),w
	xorlw	80h
	movwf	btemp+1
	movlw	(high(03E9h))^80h
	subwf	btemp+1,w
	skipz
	goto	u3025
	movlw	low(03E9h)
	subwf	(main@LoopCnt),w
u3025:

	skipc
	goto	u3021
	goto	u3020
u3021:
	goto	l842
u3020:
	goto	l3445
	
l3449:	
	goto	l3445
	line	40
	
l842:	
	line	41
# 41 "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Kalman filter\Src\main.c"
nop ;#
psect	maintext
	line	43
	
l3451:	
;main.c: 43: Filtered = KalmanFilter(20, NORMAL_MEASUREMENT);
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(?_KalmanFilter)
	bsf	status,0
	rlf	(?_KalmanFilter),f
	movlw	(014h)
	fcall	_KalmanFilter
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(0+(?_KalmanFilter)),w
	movwf	(main@Filtered)
	movf	(1+(?_KalmanFilter)),w
	movwf	(main@Filtered+1)
	movf	(2+(?_KalmanFilter)),w
	movwf	(main@Filtered+2)
	line	44
	
l3453:	
# 44 "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Kalman filter\Src\main.c"
nop ;#
psect	maintext
	line	47
	
l3455:	
;main.c: 47: if ((LoopCnt >= 55) && (LoopCnt<= 65))
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(main@LoopCnt+1),w
	xorlw	80h
	movwf	btemp+1
	movlw	(high(037h))^80h
	subwf	btemp+1,w
	skipz
	goto	u3035
	movlw	low(037h)
	subwf	(main@LoopCnt),w
u3035:

	skipc
	goto	u3031
	goto	u3030
u3031:
	goto	l3461
u3030:
	
l3457:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(main@LoopCnt+1),w
	xorlw	80h
	movwf	btemp+1
	movlw	(high(042h))^80h
	subwf	btemp+1,w
	skipz
	goto	u3045
	movlw	low(042h)
	subwf	(main@LoopCnt),w
u3045:

	skipnc
	goto	u3041
	goto	u3040
u3041:
	goto	l3461
u3040:
	line	48
	
l3459:	
;main.c: 48: { Filtered = KalmanFilter(49, NORMAL_MEASUREMENT); }
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(?_KalmanFilter)
	bsf	status,0
	rlf	(?_KalmanFilter),f
	movlw	(031h)
	fcall	_KalmanFilter
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(0+(?_KalmanFilter)),w
	movwf	(main@Filtered)
	movf	(1+(?_KalmanFilter)),w
	movwf	(main@Filtered+1)
	movf	(2+(?_KalmanFilter)),w
	movwf	(main@Filtered+2)
	goto	l3461
	
l844:	
	line	50
	
l3461:	
# 50 "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Kalman filter\Src\main.c"
nop ;#
psect	maintext
	line	39
	
l3463:	
	movlw	low(01h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	addwf	(main@LoopCnt),f
	skipnc
	incf	(main@LoopCnt+1),f
	movlw	high(01h)
	addwf	(main@LoopCnt+1),f
	
l3465:	
	movf	(main@LoopCnt+1),w
	xorlw	80h
	movwf	btemp+1
	movlw	(high(03E9h))^80h
	subwf	btemp+1,w
	skipz
	goto	u3055
	movlw	low(03E9h)
	subwf	(main@LoopCnt),w
u3055:

	skipc
	goto	u3051
	goto	u3050
u3051:
	goto	l842
u3050:
	goto	l3445
	
l843:	
	goto	l3445
	line	52
	
l845:	
	line	37
	goto	l3445
	
l846:	
	line	53
	
l847:	
	global	start
	ljmp	start
	opt stack 0
GLOBAL	__end_of_main
	__end_of_main:
;; =============== function _main ends ============

	signat	_main,88
	global	_Eric_main
psect	text364,local,class=CODE,delta=2
global __ptext364
__ptext364:

;; *************** function _Eric_main *****************
;; Defined at:
;;		line 126 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Kalman filter\Src\Kalman.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  kalman         34   12[BANK1 ] struct .
;;  SystemStateL    4    0[BANK1 ] long 
;;  measurement     3    7[BANK1 ] float 
;;  i               3    4[BANK1 ] float 
;;  LoopCnt         2   10[BANK1 ] int 
;; Return value:  Size  Location     Type
;;                  2  838[COMMON] int 
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, btemp+1, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0      46       0       0
;;      Temps:          0       4       0       0       0
;;      Totals:         0       4      46       0       0
;;Total ram usage:       50 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		_kalman_init
;;		___fttol
;;		_kalman_set
;;		_kalman_filter
;;		_kalman_get_filtered_level
;;		___ftadd
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text364
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Kalman filter\Src\Kalman.c"
	line	126
	global	__size_of_Eric_main
	__size_of_Eric_main	equ	__end_of_Eric_main-_Eric_main
	
_Eric_main:	
	opt	stack 4
; Regs used in _Eric_main: [wreg-fsr0h+status,2+status,0+btemp+1+pclath+cstack]
	line	127
	
l3405:	
;Kalman.c: 127: KALMAN_FILTER_T kalman = kalman_init();
	fcall	_kalman_init
	movlw	(Eric_main@kalman)&0ffh
	movwf	fsr0
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movlw	low((?_kalman_init))
	movwf	(??_Eric_main+0)+0
	movf	fsr0,w
	movwf	((??_Eric_main+0)+0+1)
	movlw	34
	movwf	((??_Eric_main+0)+0+2)
u2990:
	movf	(??_Eric_main+0)+0,w
	movwf	fsr0
	bcf	status, 7	;select IRP bank0
	
	movf	indf,w
	movwf	((??_Eric_main+0)+0+3)
	incf	(??_Eric_main+0)+0,f
	movf	((??_Eric_main+0)+0+1),w
	movwf	fsr0
	
	movf	((??_Eric_main+0)+0+3),w
	movwf	indf
	incf	((??_Eric_main+0)+0+1),f
	decfsz	((??_Eric_main+0)+0+2),f
	goto	u2990
	line	128
	
l3407:	
;Kalman.c: 128: int LoopCnt = 0;;
	movlw	low(0)
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movwf	(Eric_main@LoopCnt)^080h
	movlw	high(0)
	movwf	((Eric_main@LoopCnt)^080h)+1
	line	129
	
l3409:	
;Kalman.c: 129: float i = 0.0;
	movlw	0x0
	movwf	(Eric_main@i)^080h
	movlw	0x0
	movwf	(Eric_main@i+1)^080h
	movlw	0x0
	movwf	(Eric_main@i+2)^080h
	line	130
	
l3411:	
;Kalman.c: 130: float measurement = 0;
	movlw	0x0
	movwf	(Eric_main@measurement)^080h
	movlw	0x0
	movwf	(Eric_main@measurement+1)^080h
	movlw	0x0
	movwf	(Eric_main@measurement+2)^080h
	goto	l3413
	line	133
;Kalman.c: 131: sint32_t SystemStateLevel;
;Kalman.c: 133: while (1)
	
l886:	
	line	135
	
l3413:	
;Kalman.c: 134: {
;Kalman.c: 135: measurement = -20;
	movlw	0x0
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movwf	(Eric_main@measurement)^080h
	movlw	0xa0
	movwf	(Eric_main@measurement+1)^080h
	movlw	0xc1
	movwf	(Eric_main@measurement+2)^080h
	line	136
	
l3415:	
;Kalman.c: 136: if ((LoopCnt >= 55) && (LoopCnt <= 65))
	movf	(Eric_main@LoopCnt+1)^080h,w
	xorlw	80h
	movwf	btemp+1
	movlw	(high(037h))^80h
	subwf	btemp+1,w
	skipz
	goto	u3005
	movlw	low(037h)
	subwf	(Eric_main@LoopCnt)^080h,w
u3005:

	skipc
	goto	u3001
	goto	u3000
u3001:
	goto	l3421
u3000:
	
l3417:	
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movf	(Eric_main@LoopCnt+1)^080h,w
	xorlw	80h
	movwf	btemp+1
	movlw	(high(042h))^80h
	subwf	btemp+1,w
	skipz
	goto	u3015
	movlw	low(042h)
	subwf	(Eric_main@LoopCnt)^080h,w
u3015:

	skipnc
	goto	u3011
	goto	u3010
u3011:
	goto	l3421
u3010:
	line	137
	
l3419:	
;Kalman.c: 137: { measurement = 49; }
	movlw	0x0
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movwf	(Eric_main@measurement)^080h
	movlw	0x44
	movwf	(Eric_main@measurement+1)^080h
	movlw	0x42
	movwf	(Eric_main@measurement+2)^080h
	goto	l3421
	
l887:	
	line	138
	
l3421:	
# 138 "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Kalman filter\Src\Kalman.c"
nop ;#
psect	text364
	line	144
	
l3423:	
;Kalman.c: 139: kalman_set( &kalman
;Kalman.c: 140: , measurement
;Kalman.c: 141: , 0
;Kalman.c: 142: , 0
;Kalman.c: 143: , 1
;Kalman.c: 144: , 1 );
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movf	(Eric_main@measurement)^080h,w
	movwf	(?___fttol)
	movf	(Eric_main@measurement+1)^080h,w
	movwf	(?___fttol+1)
	movf	(Eric_main@measurement+2)^080h,w
	movwf	(?___fttol+2)
	fcall	___fttol
	movf	(3+(?___fttol)),w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(?_kalman_set+3)
	movf	(2+(?___fttol)),w
	movwf	(?_kalman_set+2)
	movf	(1+(?___fttol)),w
	movwf	(?_kalman_set+1)
	movf	(0+(?___fttol)),w
	movwf	(?_kalman_set)

	movlw	0x0
	movwf	0+(?_kalman_set)+04h
	movlw	0x0
	movwf	1+(?_kalman_set)+04h
	movlw	0x0
	movwf	2+(?_kalman_set)+04h
	movlw	0x0
	movwf	0+(?_kalman_set)+07h
	movlw	0x0
	movwf	1+(?_kalman_set)+07h
	movlw	0x0
	movwf	2+(?_kalman_set)+07h
	movlw	0x0
	movwf	0+(?_kalman_set)+0Ah
	movlw	0x80
	movwf	1+(?_kalman_set)+0Ah
	movlw	0x3f
	movwf	2+(?_kalman_set)+0Ah
	movlw	0x0
	movwf	0+(?_kalman_set)+0Dh
	movlw	0x80
	movwf	1+(?_kalman_set)+0Dh
	movlw	0x3f
	movwf	2+(?_kalman_set)+0Dh
	movlw	(Eric_main@kalman)&0ffh
	fcall	_kalman_set
	line	146
	
l3425:	
;Kalman.c: 146: kalman_filter(&kalman);
	movlw	(Eric_main@kalman)&0ffh
	fcall	_kalman_filter
	line	147
	
l3427:	
# 147 "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Kalman filter\Src\Kalman.c"
nop ;#
psect	text364
	line	149
	
l3429:	
;Kalman.c: 149: SystemStateLevel = kalman_get_filtered_level(&kalman);
	movlw	(Eric_main@kalman)&0ffh
	fcall	_kalman_get_filtered_level
	movf	(0+(?_kalman_get_filtered_level)),w
	movwf	(?___fttol)
	movf	(1+(?_kalman_get_filtered_level)),w
	movwf	(?___fttol+1)
	movf	(2+(?_kalman_get_filtered_level)),w
	movwf	(?___fttol+2)
	fcall	___fttol
	movf	(3+(?___fttol)),w
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movwf	(Eric_main@SystemStateLevel+3)^080h
	movf	(2+(?___fttol)),w
	movwf	(Eric_main@SystemStateLevel+2)^080h
	movf	(1+(?___fttol)),w
	movwf	(Eric_main@SystemStateLevel+1)^080h
	movf	(0+(?___fttol)),w
	movwf	(Eric_main@SystemStateLevel)^080h

	line	151
	
l3431:	
;Kalman.c: 151: i += 0.01;
	movlw	0xd7
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(?___ftadd)
	movlw	0x23
	movwf	(?___ftadd+1)
	movlw	0x3c
	movwf	(?___ftadd+2)
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movf	(Eric_main@i)^080h,w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	0+(?___ftadd)+03h
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movf	(Eric_main@i+1)^080h,w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	1+(?___ftadd)+03h
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movf	(Eric_main@i+2)^080h,w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	2+(?___ftadd)+03h
	fcall	___ftadd
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(0+(?___ftadd)),w
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movwf	(Eric_main@i)^080h
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(1+(?___ftadd)),w
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movwf	(Eric_main@i+1)^080h
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(2+(?___ftadd)),w
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movwf	(Eric_main@i+2)^080h
	line	152
	
l3433:	
;Kalman.c: 152: LoopCnt++;
	movlw	low(01h)
	addwf	(Eric_main@LoopCnt)^080h,f
	skipnc
	incf	(Eric_main@LoopCnt+1)^080h,f
	movlw	high(01h)
	addwf	(Eric_main@LoopCnt+1)^080h,f
	line	153
	
l3435:	
# 153 "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Kalman filter\Src\Kalman.c"
nop ;#
psect	text364
	goto	l3413
	line	154
	
l888:	
	line	133
	goto	l3413
	
l889:	
	line	157
;Kalman.c: 154: }
;Kalman.c: 156: return 0;
;	Return value of _Eric_main is never used
	
l890:	
	return
	opt stack 0
GLOBAL	__end_of_Eric_main
	__end_of_Eric_main:
;; =============== function _Eric_main ends ============

	signat	_Eric_main,90
	global	_kalman_filter
psect	text365,local,class=CODE,delta=2
global __ptext365
__ptext365:

;; *************** function _kalman_filter *****************
;; Defined at:
;;		line 94 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Kalman filter\Src\Kalman.c"
;; Parameters:    Size  Location     Type
;;  kalman          1    wreg     PTR struct .
;;		 -> Eric_main@kalman(34), 
;; Auto vars:     Size  Location     Type
;;  kalman          1   52[BANK0 ] PTR struct .
;;		 -> Eric_main@kalman(34), 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       4       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         0       4       0       0       0
;;Total ram usage:        4 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		___ftmul
;;		___ftadd
;;		___ftdiv
;;		___ftneg
;; This function is called by:
;;		_Eric_main
;; This function uses a non-reentrant model
;;
psect	text365
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Kalman filter\Src\Kalman.c"
	line	94
	global	__size_of_kalman_filter
	__size_of_kalman_filter	equ	__end_of_kalman_filter-_kalman_filter
	
_kalman_filter:	
	opt	stack 4
; Regs used in _kalman_filter: [wreg-fsr0h+status,2+status,0+pclath+cstack]
;kalman_filter@kalman stored from wreg
	line	99
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(kalman_filter@kalman)
	
l3403:	
;Kalman.c: 99: kalman->system_state.level = (kalman->system_state.scalar * kalman->system_state.level) + (kalman->control.scalar * kalman->control.level);
	movf	(kalman_filter@kalman),w
	addlw	03h
	movwf	fsr0
	bcf	status, 7	;select IRP bank0
	movf	indf,w
	movwf	(?___ftmul)
	incf	fsr0,f
	movf	indf,w
	movwf	(?___ftmul+1)
	incf	fsr0,f
	movf	indf,w
	movwf	(?___ftmul+2)
	movf	(kalman_filter@kalman),w
	movwf	fsr0
	movf	indf,w
	movwf	0+(?___ftmul)+03h
	incf	fsr0,f
	movf	indf,w
	movwf	1+(?___ftmul)+03h
	incf	fsr0,f
	movf	indf,w
	movwf	2+(?___ftmul)+03h
	fcall	___ftmul
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(0+(?___ftmul)),w
	movwf	(?___ftadd)
	movf	(1+(?___ftmul)),w
	movwf	(?___ftadd+1)
	movf	(2+(?___ftmul)),w
	movwf	(?___ftadd+2)
	movf	(kalman_filter@kalman),w
	addlw	09h
	movwf	fsr0
	bcf	status, 7	;select IRP bank0
	movf	indf,w
	movwf	(?___ftmul)
	incf	fsr0,f
	movf	indf,w
	movwf	(?___ftmul+1)
	incf	fsr0,f
	movf	indf,w
	movwf	(?___ftmul+2)
	movf	(kalman_filter@kalman),w
	addlw	06h
	movwf	fsr0
	movf	indf,w
	movwf	0+(?___ftmul)+03h
	incf	fsr0,f
	movf	indf,w
	movwf	1+(?___ftmul)+03h
	incf	fsr0,f
	movf	indf,w
	movwf	2+(?___ftmul)+03h
	fcall	___ftmul
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(0+(?___ftmul)),w
	movwf	0+(?___ftadd)+03h
	movf	(1+(?___ftmul)),w
	movwf	1+(?___ftadd)+03h
	movf	(2+(?___ftmul)),w
	movwf	2+(?___ftadd)+03h
	fcall	___ftadd
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(kalman_filter@kalman),w
	addlw	09h
	movwf	fsr0
	movf	(0+(?___ftadd)),w
	bcf	status, 7	;select IRP bank0
	movwf	indf
	incf	fsr0,f
	movf	(1+(?___ftadd)),w
	movwf	indf
	incf	fsr0,f
	movf	(2+(?___ftadd)),w
	movwf	indf
	line	102
;Kalman.c: 102: kalman->covariance = kalman->covariance * kalman->system_state.scalar * kalman->system_state.scalar;
	movf	(kalman_filter@kalman),w
	addlw	06h
	movwf	fsr0
	movf	indf,w
	movwf	(?___ftmul)
	incf	fsr0,f
	movf	indf,w
	movwf	(?___ftmul+1)
	incf	fsr0,f
	movf	indf,w
	movwf	(?___ftmul+2)
	movf	(kalman_filter@kalman),w
	addlw	018h
	movwf	fsr0
	movf	indf,w
	movwf	0+(?___ftmul)+03h
	incf	fsr0,f
	movf	indf,w
	movwf	1+(?___ftmul)+03h
	incf	fsr0,f
	movf	indf,w
	movwf	2+(?___ftmul)+03h
	fcall	___ftmul
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(0+(?___ftmul)),w
	movwf	(_kalman_filter$922)
	movf	(1+(?___ftmul)),w
	movwf	(_kalman_filter$922+1)
	movf	(2+(?___ftmul)),w
	movwf	(_kalman_filter$922+2)
;Kalman.c: 102: kalman->covariance = kalman->covariance * kalman->system_state.scalar * kalman->system_state.scalar;
	movf	(kalman_filter@kalman),w
	addlw	06h
	movwf	fsr0
	bcf	status, 7	;select IRP bank0
	movf	indf,w
	movwf	(?___ftmul)
	incf	fsr0,f
	movf	indf,w
	movwf	(?___ftmul+1)
	incf	fsr0,f
	movf	indf,w
	movwf	(?___ftmul+2)
	movf	(_kalman_filter$922),w
	movwf	0+(?___ftmul)+03h
	movf	(_kalman_filter$922+1),w
	movwf	1+(?___ftmul)+03h
	movf	(_kalman_filter$922+2),w
	movwf	2+(?___ftmul)+03h
	fcall	___ftmul
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(kalman_filter@kalman),w
	addlw	018h
	movwf	fsr0
	movf	(0+(?___ftmul)),w
	bcf	status, 7	;select IRP bank0
	movwf	indf
	incf	fsr0,f
	movf	(1+(?___ftmul)),w
	movwf	indf
	incf	fsr0,f
	movf	(2+(?___ftmul)),w
	movwf	indf
	line	111
;Kalman.c: 110: kalman->gain = ( kalman->covariance * kalman->measurement.scalar)
;Kalman.c: 111: / ((kalman->covariance * kalman->measurement.scalar * kalman->measurement.scalar) + kalman->sensor_accuracy);
	movf	(kalman_filter@kalman),w
	addlw	0Ch
	movwf	fsr0
	movf	indf,w
	movwf	(?___ftmul)
	incf	fsr0,f
	movf	indf,w
	movwf	(?___ftmul+1)
	incf	fsr0,f
	movf	indf,w
	movwf	(?___ftmul+2)
	movf	(kalman_filter@kalman),w
	addlw	018h
	movwf	fsr0
	movf	indf,w
	movwf	0+(?___ftmul)+03h
	incf	fsr0,f
	movf	indf,w
	movwf	1+(?___ftmul)+03h
	incf	fsr0,f
	movf	indf,w
	movwf	2+(?___ftmul)+03h
	fcall	___ftmul
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(0+(?___ftmul)),w
	movwf	(_kalman_filter$922)
	movf	(1+(?___ftmul)),w
	movwf	(_kalman_filter$922+1)
	movf	(2+(?___ftmul)),w
	movwf	(_kalman_filter$922+2)
;Kalman.c: 110: kalman->gain = ( kalman->covariance * kalman->measurement.scalar)
;Kalman.c: 111: / ((kalman->covariance * kalman->measurement.scalar * kalman->measurement.scalar) + kalman->sensor_accuracy);
	movf	(kalman_filter@kalman),w
	addlw	01Eh
	movwf	fsr0
	bcf	status, 7	;select IRP bank0
	movf	indf,w
	movwf	(?___ftadd)
	incf	fsr0,f
	movf	indf,w
	movwf	(?___ftadd+1)
	incf	fsr0,f
	movf	indf,w
	movwf	(?___ftadd+2)
	movf	(kalman_filter@kalman),w
	addlw	0Ch
	movwf	fsr0
	movf	indf,w
	movwf	(?___ftmul)
	incf	fsr0,f
	movf	indf,w
	movwf	(?___ftmul+1)
	incf	fsr0,f
	movf	indf,w
	movwf	(?___ftmul+2)
	movf	(_kalman_filter$922),w
	movwf	0+(?___ftmul)+03h
	movf	(_kalman_filter$922+1),w
	movwf	1+(?___ftmul)+03h
	movf	(_kalman_filter$922+2),w
	movwf	2+(?___ftmul)+03h
	fcall	___ftmul
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(0+(?___ftmul)),w
	movwf	0+(?___ftadd)+03h
	movf	(1+(?___ftmul)),w
	movwf	1+(?___ftadd)+03h
	movf	(2+(?___ftmul)),w
	movwf	2+(?___ftadd)+03h
	fcall	___ftadd
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(0+(?___ftadd)),w
	movwf	(?___ftdiv)
	movf	(1+(?___ftadd)),w
	movwf	(?___ftdiv+1)
	movf	(2+(?___ftadd)),w
	movwf	(?___ftdiv+2)
	movf	(kalman_filter@kalman),w
	addlw	0Ch
	movwf	fsr0
	bcf	status, 7	;select IRP bank0
	movf	indf,w
	movwf	(?___ftmul)
	incf	fsr0,f
	movf	indf,w
	movwf	(?___ftmul+1)
	incf	fsr0,f
	movf	indf,w
	movwf	(?___ftmul+2)
	movf	(kalman_filter@kalman),w
	addlw	018h
	movwf	fsr0
	movf	indf,w
	movwf	0+(?___ftmul)+03h
	incf	fsr0,f
	movf	indf,w
	movwf	1+(?___ftmul)+03h
	incf	fsr0,f
	movf	indf,w
	movwf	2+(?___ftmul)+03h
	fcall	___ftmul
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(0+(?___ftmul)),w
	movwf	0+(?___ftdiv)+03h
	movf	(1+(?___ftmul)),w
	movwf	1+(?___ftdiv)+03h
	movf	(2+(?___ftmul)),w
	movwf	2+(?___ftdiv)+03h
	fcall	___ftdiv
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(kalman_filter@kalman),w
	addlw	01Bh
	movwf	fsr0
	movf	(0+(?___ftdiv)),w
	bcf	status, 7	;select IRP bank0
	movwf	indf
	incf	fsr0,f
	movf	(1+(?___ftdiv)),w
	movwf	indf
	incf	fsr0,f
	movf	(2+(?___ftdiv)),w
	movwf	indf
	line	116
;Kalman.c: 116: kalman->system_state.level = kalman->system_state.level + (kalman->gain * (kalman->measurement.level - (kalman->measurement.scalar * kalman->system_state.level)));
	movf	(kalman_filter@kalman),w
	addlw	0Fh
	movwf	fsr0
	movf	indf,w
	movwf	(?___ftadd)
	incf	fsr0,f
	movf	indf,w
	movwf	(?___ftadd+1)
	incf	fsr0,f
	movf	indf,w
	movwf	(?___ftadd+2)
	movf	(kalman_filter@kalman),w
	addlw	0Ch
	movwf	fsr0
	movf	indf,w
	movwf	0+(?___ftmul)+03h
	incf	fsr0,f
	movf	indf,w
	movwf	1+(?___ftmul)+03h
	incf	fsr0,f
	movf	indf,w
	movwf	2+(?___ftmul)+03h
	movf	(kalman_filter@kalman),w
	addlw	09h
	movwf	fsr0
	movf	indf,w
	movwf	(?___ftmul)
	incf	fsr0,f
	movf	indf,w
	movwf	(?___ftmul+1)
	incf	fsr0,f
	movf	indf,w
	movwf	(?___ftmul+2)
	fcall	___ftmul
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(0+(?___ftmul)),w
	movwf	(?___ftneg)
	movf	(1+(?___ftmul)),w
	movwf	(?___ftneg+1)
	movf	(2+(?___ftmul)),w
	movwf	(?___ftneg+2)
	fcall	___ftneg
	movf	(0+(?___ftneg)),w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	0+(?___ftadd)+03h
	movf	(1+(?___ftneg)),w
	movwf	1+(?___ftadd)+03h
	movf	(2+(?___ftneg)),w
	movwf	2+(?___ftadd)+03h
	fcall	___ftadd
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(0+(?___ftadd)),w
	movwf	(_kalman_filter$922)
	movf	(1+(?___ftadd)),w
	movwf	(_kalman_filter$922+1)
	movf	(2+(?___ftadd)),w
	movwf	(_kalman_filter$922+2)
;Kalman.c: 116: kalman->system_state.level = kalman->system_state.level + (kalman->gain * (kalman->measurement.level - (kalman->measurement.scalar * kalman->system_state.level)));
	movf	(kalman_filter@kalman),w
	addlw	09h
	movwf	fsr0
	bcf	status, 7	;select IRP bank0
	movf	indf,w
	movwf	(?___ftadd)
	incf	fsr0,f
	movf	indf,w
	movwf	(?___ftadd+1)
	incf	fsr0,f
	movf	indf,w
	movwf	(?___ftadd+2)
	movf	(kalman_filter@kalman),w
	addlw	01Bh
	movwf	fsr0
	movf	indf,w
	movwf	(?___ftmul)
	incf	fsr0,f
	movf	indf,w
	movwf	(?___ftmul+1)
	incf	fsr0,f
	movf	indf,w
	movwf	(?___ftmul+2)
	movf	(_kalman_filter$922),w
	movwf	0+(?___ftmul)+03h
	movf	(_kalman_filter$922+1),w
	movwf	1+(?___ftmul)+03h
	movf	(_kalman_filter$922+2),w
	movwf	2+(?___ftmul)+03h
	fcall	___ftmul
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(0+(?___ftmul)),w
	movwf	0+(?___ftadd)+03h
	movf	(1+(?___ftmul)),w
	movwf	1+(?___ftadd)+03h
	movf	(2+(?___ftmul)),w
	movwf	2+(?___ftadd)+03h
	fcall	___ftadd
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(kalman_filter@kalman),w
	addlw	09h
	movwf	fsr0
	movf	(0+(?___ftadd)),w
	bcf	status, 7	;select IRP bank0
	movwf	indf
	incf	fsr0,f
	movf	(1+(?___ftadd)),w
	movwf	indf
	incf	fsr0,f
	movf	(2+(?___ftadd)),w
	movwf	indf
	line	119
;Kalman.c: 119: kalman->covariance = (1 - (kalman->gain * kalman->measurement.scalar)) * kalman->covariance;
	movlw	0x0
	movwf	(?___ftadd)
	movlw	0x80
	movwf	(?___ftadd+1)
	movlw	0x3f
	movwf	(?___ftadd+2)
	movf	(kalman_filter@kalman),w
	addlw	01Bh
	movwf	fsr0
	movf	indf,w
	movwf	0+(?___ftmul)+03h
	incf	fsr0,f
	movf	indf,w
	movwf	1+(?___ftmul)+03h
	incf	fsr0,f
	movf	indf,w
	movwf	2+(?___ftmul)+03h
	movf	(kalman_filter@kalman),w
	addlw	0Ch
	movwf	fsr0
	movf	indf,w
	movwf	(?___ftmul)
	incf	fsr0,f
	movf	indf,w
	movwf	(?___ftmul+1)
	incf	fsr0,f
	movf	indf,w
	movwf	(?___ftmul+2)
	fcall	___ftmul
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(0+(?___ftmul)),w
	movwf	(?___ftneg)
	movf	(1+(?___ftmul)),w
	movwf	(?___ftneg+1)
	movf	(2+(?___ftmul)),w
	movwf	(?___ftneg+2)
	fcall	___ftneg
	movf	(0+(?___ftneg)),w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	0+(?___ftadd)+03h
	movf	(1+(?___ftneg)),w
	movwf	1+(?___ftadd)+03h
	movf	(2+(?___ftneg)),w
	movwf	2+(?___ftadd)+03h
	fcall	___ftadd
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(0+(?___ftadd)),w
	movwf	(_kalman_filter$922)
	movf	(1+(?___ftadd)),w
	movwf	(_kalman_filter$922+1)
	movf	(2+(?___ftadd)),w
	movwf	(_kalman_filter$922+2)
;Kalman.c: 119: kalman->covariance = (1 - (kalman->gain * kalman->measurement.scalar)) * kalman->covariance;
	movf	(kalman_filter@kalman),w
	addlw	018h
	movwf	fsr0
	bcf	status, 7	;select IRP bank0
	movf	indf,w
	movwf	(?___ftmul)
	incf	fsr0,f
	movf	indf,w
	movwf	(?___ftmul+1)
	incf	fsr0,f
	movf	indf,w
	movwf	(?___ftmul+2)
	movf	(_kalman_filter$922),w
	movwf	0+(?___ftmul)+03h
	movf	(_kalman_filter$922+1),w
	movwf	1+(?___ftmul)+03h
	movf	(_kalman_filter$922+2),w
	movwf	2+(?___ftmul)+03h
	fcall	___ftmul
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(kalman_filter@kalman),w
	addlw	018h
	movwf	fsr0
	movf	(0+(?___ftmul)),w
	bcf	status, 7	;select IRP bank0
	movwf	indf
	incf	fsr0,f
	movf	(1+(?___ftmul)),w
	movwf	indf
	incf	fsr0,f
	movf	(2+(?___ftmul)),w
	movwf	indf
	line	121
	
l883:	
	return
	opt stack 0
GLOBAL	__end_of_kalman_filter
	__end_of_kalman_filter:
;; =============== function _kalman_filter ends ============

	signat	_kalman_filter,4216
	global	_kalman_set
psect	text366,local,class=CODE,delta=2
global __ptext366
__ptext366:

;; *************** function _kalman_set *****************
;; Defined at:
;;		line 80 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Kalman filter\Src\Kalman.c"
;; Parameters:    Size  Location     Type
;;  kalman          1    wreg     PTR struct .
;;		 -> Eric_main@kalman(34), 
;;  z               4    6[BANK0 ] long 
;;  u               3   10[BANK0 ] float 
;;  b               3   13[BANK0 ] float 
;;  a               3   16[BANK0 ] float 
;;  c               3   19[BANK0 ] float 
;; Auto vars:     Size  Location     Type
;;  kalman          1   22[BANK0 ] PTR struct .
;;		 -> Eric_main@kalman(34), 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0      16       0       0       0
;;      Locals:         0       1       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         0      17       0       0       0
;;Total ram usage:       17 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		___altoft
;; This function is called by:
;;		_Eric_main
;; This function uses a non-reentrant model
;;
psect	text366
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Kalman filter\Src\Kalman.c"
	line	80
	global	__size_of_kalman_set
	__size_of_kalman_set	equ	__end_of_kalman_set-_kalman_set
	
_kalman_set:	
	opt	stack 4
; Regs used in _kalman_set: [wreg-fsr0h+status,2+status,0+pclath+cstack]
;kalman_set@kalman stored from wreg
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(kalman_set@kalman)
	line	81
	
l3399:	
;Kalman.c: 81: kalman->control.scalar = b;
	movf	(kalman_set@kalman),w
	movwf	fsr0
	movf	(kalman_set@b),w
	bcf	status, 7	;select IRP bank0
	movwf	indf
	incf	fsr0,f
	movf	(kalman_set@b+1),w
	movwf	indf
	incf	fsr0,f
	movf	(kalman_set@b+2),w
	movwf	indf
	line	82
;Kalman.c: 82: kalman->control.level = u;
	movf	(kalman_set@kalman),w
	addlw	03h
	movwf	fsr0
	movf	(kalman_set@u),w
	movwf	indf
	incf	fsr0,f
	movf	(kalman_set@u+1),w
	movwf	indf
	incf	fsr0,f
	movf	(kalman_set@u+2),w
	movwf	indf
	line	83
;Kalman.c: 83: kalman->system_state.scalar = a;
	movf	(kalman_set@kalman),w
	addlw	06h
	movwf	fsr0
	movf	(kalman_set@a),w
	movwf	indf
	incf	fsr0,f
	movf	(kalman_set@a+1),w
	movwf	indf
	incf	fsr0,f
	movf	(kalman_set@a+2),w
	movwf	indf
	line	84
;Kalman.c: 84: kalman->measurement.scalar = c;
	movf	(kalman_set@kalman),w
	addlw	0Ch
	movwf	fsr0
	movf	(kalman_set@c),w
	movwf	indf
	incf	fsr0,f
	movf	(kalman_set@c+1),w
	movwf	indf
	incf	fsr0,f
	movf	(kalman_set@c+2),w
	movwf	indf
	line	85
	
l3401:	
;Kalman.c: 85: kalman->measurement.level = z;
	movf	(kalman_set@z+3),w
	movwf	(?___altoft+3)
	movf	(kalman_set@z+2),w
	movwf	(?___altoft+2)
	movf	(kalman_set@z+1),w
	movwf	(?___altoft+1)
	movf	(kalman_set@z),w
	movwf	(?___altoft)

	fcall	___altoft
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(kalman_set@kalman),w
	addlw	0Fh
	movwf	fsr0
	movf	(0+(?___altoft)),w
	bcf	status, 7	;select IRP bank0
	movwf	indf
	incf	fsr0,f
	movf	(1+(?___altoft)),w
	movwf	indf
	incf	fsr0,f
	movf	(2+(?___altoft)),w
	movwf	indf
	line	86
	
l877:	
	return
	opt stack 0
GLOBAL	__end_of_kalman_set
	__end_of_kalman_set:
;; =============== function _kalman_set ends ============

	signat	_kalman_set,24696
	global	_KalmanFilter
psect	text367,local,class=CODE,delta=2
global __ptext367
__ptext367:

;; *************** function _KalmanFilter *****************
;; Defined at:
;;		line 56 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Kalman filter\Src\main.c"
;; Parameters:    Size  Location     Type
;;  Measurement     1    wreg     unsigned char 
;;  KalStMch        1   49[BANK0 ] unsigned char 
;; Auto vars:     Size  Location     Type
;;  Measurement     1   55[BANK0 ] unsigned char 
;;  InitEstErr      3   59[BANK0 ] float 
;;  InitEst         3   52[BANK0 ] float 
;; Return value:  Size  Location     Type
;;                  3   49[BANK0 ] float 
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       3       0       0       0
;;      Locals:         0      10       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         0      13       0       0       0
;;Total ram usage:       13 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		___ftadd
;;		___ftdiv
;;		___ftneg
;;		___lbtoft
;;		___ftmul
;;		___fttol
;;		___lwtoft
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text367
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Kalman filter\Src\main.c"
	line	56
	global	__size_of_KalmanFilter
	__size_of_KalmanFilter	equ	__end_of_KalmanFilter-_KalmanFilter
	
_KalmanFilter:	
	opt	stack 5
; Regs used in _KalmanFilter: [wreg-fsr0h+status,2+status,0+pclath+cstack]
;KalmanFilter@Measurement stored from wreg
	line	65
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(KalmanFilter@Measurement)
	
l3365:	
;main.c: 57: static float KalGain;
;main.c: 58: static float CurrentEstErr;
;main.c: 59: static float PreviousEstErr;
;main.c: 60: static float CurrentEst;
;main.c: 61: static float PreviousEst;
;main.c: 65: float InitEstErr = 1;
	movlw	0x0
	movwf	(KalmanFilter@InitEstErr)
	movlw	0x80
	movwf	(KalmanFilter@InitEstErr+1)
	movlw	0x3f
	movwf	(KalmanFilter@InitEstErr+2)
	line	67
;main.c: 66: const float MeasErr = 4;
;main.c: 67: float InitEst = 0;
	movlw	0x0
	movwf	(KalmanFilter@InitEst)
	movlw	0x0
	movwf	(KalmanFilter@InitEst+1)
	movlw	0x0
	movwf	(KalmanFilter@InitEst+2)
	line	73
;main.c: 70: const uint8_t GainScaler = 2;
;main.c: 73: switch(KalStMch)
	goto	l3393
	line	75
;main.c: 74: {
;main.c: 75: case INIT_MEASUREMENT:
	
l865:	
	line	77
	
l3367:	
;main.c: 77: KalGain = ( InitEstErr / ( InitEstErr + MeasErr ) );
	movlw	(KalmanFilter@MeasErr-__stringbase)
	movwf	fsr0
	fcall	stringdir
	movwf	(?___ftadd)
	fcall	stringdir
	movwf	(?___ftadd+1)
	fcall	stringdir
	movwf	(?___ftadd+2)
	movf	(KalmanFilter@InitEstErr),w
	movwf	0+(?___ftadd)+03h
	movf	(KalmanFilter@InitEstErr+1),w
	movwf	1+(?___ftadd)+03h
	movf	(KalmanFilter@InitEstErr+2),w
	movwf	2+(?___ftadd)+03h
	fcall	___ftadd
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(0+(?___ftadd)),w
	movwf	(?___ftdiv)
	movf	(1+(?___ftadd)),w
	movwf	(?___ftdiv+1)
	movf	(2+(?___ftadd)),w
	movwf	(?___ftdiv+2)
	movf	(KalmanFilter@InitEstErr),w
	movwf	0+(?___ftdiv)+03h
	movf	(KalmanFilter@InitEstErr+1),w
	movwf	1+(?___ftdiv)+03h
	movf	(KalmanFilter@InitEstErr+2),w
	movwf	2+(?___ftdiv)+03h
	fcall	___ftdiv
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(0+(?___ftdiv)),w
	movwf	(KalmanFilter@KalGain)
	movf	(1+(?___ftdiv)),w
	movwf	(KalmanFilter@KalGain+1)
	movf	(2+(?___ftdiv)),w
	movwf	(KalmanFilter@KalGain+2)
	line	80
	
l3369:	
;main.c: 80: CurrentEst = ( InitEst + ( KalGain * (Measurement - InitEst)));
	movf	(KalmanFilter@KalGain),w
	movwf	(?___ftmul)
	movf	(KalmanFilter@KalGain+1),w
	movwf	(?___ftmul+1)
	movf	(KalmanFilter@KalGain+2),w
	movwf	(?___ftmul+2)
	movf	(KalmanFilter@InitEst),w
	movwf	(?___ftneg)
	movf	(KalmanFilter@InitEst+1),w
	movwf	(?___ftneg+1)
	movf	(KalmanFilter@InitEst+2),w
	movwf	(?___ftneg+2)
	fcall	___ftneg
	movf	(0+(?___ftneg)),w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(?___ftadd)
	movf	(1+(?___ftneg)),w
	movwf	(?___ftadd+1)
	movf	(2+(?___ftneg)),w
	movwf	(?___ftadd+2)
	movf	(KalmanFilter@Measurement),w
	fcall	___lbtoft
	movf	(0+(?___lbtoft)),w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	0+(?___ftadd)+03h
	movf	(1+(?___lbtoft)),w
	movwf	1+(?___ftadd)+03h
	movf	(2+(?___lbtoft)),w
	movwf	2+(?___ftadd)+03h
	fcall	___ftadd
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(0+(?___ftadd)),w
	movwf	0+(?___ftmul)+03h
	movf	(1+(?___ftadd)),w
	movwf	1+(?___ftmul)+03h
	movf	(2+(?___ftadd)),w
	movwf	2+(?___ftmul)+03h
	fcall	___ftmul
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(0+(?___ftmul)),w
	movwf	(_KalmanFilter$921)
	movf	(1+(?___ftmul)),w
	movwf	(_KalmanFilter$921+1)
	movf	(2+(?___ftmul)),w
	movwf	(_KalmanFilter$921+2)
	
l3371:	
;main.c: 80: CurrentEst = ( InitEst + ( KalGain * (Measurement - InitEst)));
	movf	(KalmanFilter@InitEst),w
	movwf	(?___ftadd)
	movf	(KalmanFilter@InitEst+1),w
	movwf	(?___ftadd+1)
	movf	(KalmanFilter@InitEst+2),w
	movwf	(?___ftadd+2)
	movf	(_KalmanFilter$921),w
	movwf	0+(?___ftadd)+03h
	movf	(_KalmanFilter$921+1),w
	movwf	1+(?___ftadd)+03h
	movf	(_KalmanFilter$921+2),w
	movwf	2+(?___ftadd)+03h
	fcall	___ftadd
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(0+(?___ftadd)),w
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movwf	(KalmanFilter@CurrentEst)^080h
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(1+(?___ftadd)),w
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movwf	(KalmanFilter@CurrentEst+1)^080h
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(2+(?___ftadd)),w
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movwf	(KalmanFilter@CurrentEst+2)^080h
	line	81
	
l3373:	
;main.c: 81: PreviousEst = CurrentEst;
	movf	(KalmanFilter@CurrentEst)^080h,w
	movwf	(KalmanFilter@PreviousEst)^080h
	movf	(KalmanFilter@CurrentEst+1)^080h,w
	movwf	(KalmanFilter@PreviousEst+1)^080h
	movf	(KalmanFilter@CurrentEst+2)^080h,w
	movwf	(KalmanFilter@PreviousEst+2)^080h
	line	84
	
l3375:	
;main.c: 84: CurrentEstErr = ( (1 - KalGain) * (InitEstErr) );
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(KalmanFilter@InitEstErr),w
	movwf	(?___ftmul)
	movf	(KalmanFilter@InitEstErr+1),w
	movwf	(?___ftmul+1)
	movf	(KalmanFilter@InitEstErr+2),w
	movwf	(?___ftmul+2)
	movlw	0x0
	movwf	(?___ftadd)
	movlw	0x80
	movwf	(?___ftadd+1)
	movlw	0x3f
	movwf	(?___ftadd+2)
	movf	(KalmanFilter@KalGain),w
	movwf	(?___ftneg)
	movf	(KalmanFilter@KalGain+1),w
	movwf	(?___ftneg+1)
	movf	(KalmanFilter@KalGain+2),w
	movwf	(?___ftneg+2)
	fcall	___ftneg
	movf	(0+(?___ftneg)),w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	0+(?___ftadd)+03h
	movf	(1+(?___ftneg)),w
	movwf	1+(?___ftadd)+03h
	movf	(2+(?___ftneg)),w
	movwf	2+(?___ftadd)+03h
	fcall	___ftadd
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(0+(?___ftadd)),w
	movwf	0+(?___ftmul)+03h
	movf	(1+(?___ftadd)),w
	movwf	1+(?___ftmul)+03h
	movf	(2+(?___ftadd)),w
	movwf	2+(?___ftmul)+03h
	fcall	___ftmul
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(0+(?___ftmul)),w
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movwf	(KalmanFilter@CurrentEstErr)^080h
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(1+(?___ftmul)),w
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movwf	(KalmanFilter@CurrentEstErr+1)^080h
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(2+(?___ftmul)),w
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movwf	(KalmanFilter@CurrentEstErr+2)^080h
	line	85
	
l3377:	
;main.c: 85: PreviousEstErr = CurrentEstErr;
	movf	(KalmanFilter@CurrentEstErr)^080h,w
	movwf	(KalmanFilter@PreviousEstErr)^080h
	movf	(KalmanFilter@CurrentEstErr+1)^080h,w
	movwf	(KalmanFilter@PreviousEstErr+1)^080h
	movf	(KalmanFilter@CurrentEstErr+2)^080h,w
	movwf	(KalmanFilter@PreviousEstErr+2)^080h
	line	87
;main.c: 87: break;
	goto	l3395
	line	89
;main.c: 89: case NORMAL_MEASUREMENT:
	
l867:	
	line	91
	
l3379:	
;main.c: 91: KalGain = ( CurrentEstErr * GainScaler / ( CurrentEstErr + MeasErr ) );
	movlw	(KalmanFilter@MeasErr-__stringbase)
	movwf	fsr0
	fcall	stringdir
	bcf	status, 5	;RP0=0, select bank0
	movwf	(?___ftadd)
	fcall	stringdir
	movwf	(?___ftadd+1)
	fcall	stringdir
	movwf	(?___ftadd+2)
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movf	(KalmanFilter@CurrentEstErr)^080h,w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	0+(?___ftadd)+03h
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movf	(KalmanFilter@CurrentEstErr+1)^080h,w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	1+(?___ftadd)+03h
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movf	(KalmanFilter@CurrentEstErr+2)^080h,w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	2+(?___ftadd)+03h
	fcall	___ftadd
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(0+(?___ftadd)),w
	movwf	(?___ftdiv)
	movf	(1+(?___ftadd)),w
	movwf	(?___ftdiv+1)
	movf	(2+(?___ftadd)),w
	movwf	(?___ftdiv+2)
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movf	(KalmanFilter@CurrentEstErr)^080h,w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(?___ftmul)
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movf	(KalmanFilter@CurrentEstErr+1)^080h,w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(?___ftmul+1)
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movf	(KalmanFilter@CurrentEstErr+2)^080h,w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(?___ftmul+2)
	movlw	(KalmanFilter@GainScaler-__stringbase)
	movwf	fsr0
	fcall	stringdir
	fcall	___lbtoft
	movf	(0+(?___lbtoft)),w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	0+(?___ftmul)+03h
	movf	(1+(?___lbtoft)),w
	movwf	1+(?___ftmul)+03h
	movf	(2+(?___lbtoft)),w
	movwf	2+(?___ftmul)+03h
	fcall	___ftmul
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(0+(?___ftmul)),w
	movwf	0+(?___ftdiv)+03h
	movf	(1+(?___ftmul)),w
	movwf	1+(?___ftdiv)+03h
	movf	(2+(?___ftmul)),w
	movwf	2+(?___ftdiv)+03h
	fcall	___ftdiv
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(0+(?___ftdiv)),w
	movwf	(KalmanFilter@KalGain)
	movf	(1+(?___ftdiv)),w
	movwf	(KalmanFilter@KalGain+1)
	movf	(2+(?___ftdiv)),w
	movwf	(KalmanFilter@KalGain+2)
	line	94
	
l3381:	
;main.c: 94: CurrentEst = ( PreviousEst + ( KalGain * (Measurement - PreviousEst )));
	movf	(KalmanFilter@KalGain),w
	movwf	(?___ftmul)
	movf	(KalmanFilter@KalGain+1),w
	movwf	(?___ftmul+1)
	movf	(KalmanFilter@KalGain+2),w
	movwf	(?___ftmul+2)
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movf	(KalmanFilter@PreviousEst)^080h,w
	movwf	(?___ftneg)
	movf	(KalmanFilter@PreviousEst+1)^080h,w
	movwf	(?___ftneg+1)
	movf	(KalmanFilter@PreviousEst+2)^080h,w
	movwf	(?___ftneg+2)
	fcall	___ftneg
	movf	(0+(?___ftneg)),w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(?___ftadd)
	movf	(1+(?___ftneg)),w
	movwf	(?___ftadd+1)
	movf	(2+(?___ftneg)),w
	movwf	(?___ftadd+2)
	movf	(KalmanFilter@Measurement),w
	fcall	___lbtoft
	movf	(0+(?___lbtoft)),w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	0+(?___ftadd)+03h
	movf	(1+(?___lbtoft)),w
	movwf	1+(?___ftadd)+03h
	movf	(2+(?___lbtoft)),w
	movwf	2+(?___ftadd)+03h
	fcall	___ftadd
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(0+(?___ftadd)),w
	movwf	0+(?___ftmul)+03h
	movf	(1+(?___ftadd)),w
	movwf	1+(?___ftmul)+03h
	movf	(2+(?___ftadd)),w
	movwf	2+(?___ftmul)+03h
	fcall	___ftmul
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(0+(?___ftmul)),w
	movwf	(_KalmanFilter$921)
	movf	(1+(?___ftmul)),w
	movwf	(_KalmanFilter$921+1)
	movf	(2+(?___ftmul)),w
	movwf	(_KalmanFilter$921+2)
	
l3383:	
;main.c: 94: CurrentEst = ( PreviousEst + ( KalGain * (Measurement - PreviousEst )));
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movf	(KalmanFilter@PreviousEst)^080h,w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(?___ftadd)
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movf	(KalmanFilter@PreviousEst+1)^080h,w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(?___ftadd+1)
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movf	(KalmanFilter@PreviousEst+2)^080h,w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(?___ftadd+2)
	movf	(_KalmanFilter$921),w
	movwf	0+(?___ftadd)+03h
	movf	(_KalmanFilter$921+1),w
	movwf	1+(?___ftadd)+03h
	movf	(_KalmanFilter$921+2),w
	movwf	2+(?___ftadd)+03h
	fcall	___ftadd
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(0+(?___ftadd)),w
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movwf	(KalmanFilter@CurrentEst)^080h
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(1+(?___ftadd)),w
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movwf	(KalmanFilter@CurrentEst+1)^080h
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(2+(?___ftadd)),w
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movwf	(KalmanFilter@CurrentEst+2)^080h
	line	95
	
l3385:	
;main.c: 95: PreviousEst = CurrentEst;
	movf	(KalmanFilter@CurrentEst)^080h,w
	movwf	(KalmanFilter@PreviousEst)^080h
	movf	(KalmanFilter@CurrentEst+1)^080h,w
	movwf	(KalmanFilter@PreviousEst+1)^080h
	movf	(KalmanFilter@CurrentEst+2)^080h,w
	movwf	(KalmanFilter@PreviousEst+2)^080h
	line	98
	
l3387:	
;main.c: 98: CurrentEstErr = ( (1 - KalGain) * (PreviousEstErr) );
	movf	(KalmanFilter@PreviousEstErr)^080h,w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(?___ftmul)
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movf	(KalmanFilter@PreviousEstErr+1)^080h,w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(?___ftmul+1)
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movf	(KalmanFilter@PreviousEstErr+2)^080h,w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(?___ftmul+2)
	movlw	0x0
	movwf	(?___ftadd)
	movlw	0x80
	movwf	(?___ftadd+1)
	movlw	0x3f
	movwf	(?___ftadd+2)
	movf	(KalmanFilter@KalGain),w
	movwf	(?___ftneg)
	movf	(KalmanFilter@KalGain+1),w
	movwf	(?___ftneg+1)
	movf	(KalmanFilter@KalGain+2),w
	movwf	(?___ftneg+2)
	fcall	___ftneg
	movf	(0+(?___ftneg)),w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	0+(?___ftadd)+03h
	movf	(1+(?___ftneg)),w
	movwf	1+(?___ftadd)+03h
	movf	(2+(?___ftneg)),w
	movwf	2+(?___ftadd)+03h
	fcall	___ftadd
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(0+(?___ftadd)),w
	movwf	0+(?___ftmul)+03h
	movf	(1+(?___ftadd)),w
	movwf	1+(?___ftmul)+03h
	movf	(2+(?___ftadd)),w
	movwf	2+(?___ftmul)+03h
	fcall	___ftmul
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(0+(?___ftmul)),w
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movwf	(KalmanFilter@CurrentEstErr)^080h
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(1+(?___ftmul)),w
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movwf	(KalmanFilter@CurrentEstErr+1)^080h
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(2+(?___ftmul)),w
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movwf	(KalmanFilter@CurrentEstErr+2)^080h
	line	99
	
l3389:	
;main.c: 99: PreviousEstErr = CurrentEstErr;
	movf	(KalmanFilter@CurrentEstErr)^080h,w
	movwf	(KalmanFilter@PreviousEstErr)^080h
	movf	(KalmanFilter@CurrentEstErr+1)^080h,w
	movwf	(KalmanFilter@PreviousEstErr+1)^080h
	movf	(KalmanFilter@CurrentEstErr+2)^080h,w
	movwf	(KalmanFilter@PreviousEstErr+2)^080h
	line	101
;main.c: 101: break;
	goto	l3395
	line	103
;main.c: 103: default:
	
l868:	
	line	105
;main.c: 105: break;
	goto	l3395
	line	106
	
l3391:	
;main.c: 106: }
	goto	l3395
	line	73
	
l864:	
	
l3393:	
	bcf	status, 5	;RP0=0, select bank0
	movf	(KalmanFilter@KalStMch),w
	; Switch size 1, requested type "space"
; Number of cases is 2, Range of values is 0 to 1
; switch strategies available:
; Name         Bytes Cycles
; simple_byte     7     4 (average)
; direct_byte    25    19 (fixed)
;	Chosen strategy is simple_byte

	xorlw	0^0	; case 0
	skipnz
	goto	l3367
	xorlw	1^0	; case 1
	skipnz
	goto	l3379
	goto	l3395

	line	106
	
l866:	
	line	109
	
l3395:	
;main.c: 109: return (uint16_t)(CurrentEst);
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movf	(KalmanFilter@CurrentEst)^080h,w
	movwf	(?___fttol)
	movf	(KalmanFilter@CurrentEst+1)^080h,w
	movwf	(?___fttol+1)
	movf	(KalmanFilter@CurrentEst+2)^080h,w
	movwf	(?___fttol+2)
	fcall	___fttol
	movf	1+(((0+(?___fttol)))),w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(?___lwtoft+1)
	addwf	(?___lwtoft+1)
	movf	0+(((0+(?___fttol)))),w
	clrf	(?___lwtoft)
	addwf	(?___lwtoft)

	fcall	___lwtoft
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(0+(?___lwtoft)),w
	movwf	(?_KalmanFilter)
	movf	(1+(?___lwtoft)),w
	movwf	(?_KalmanFilter+1)
	movf	(2+(?___lwtoft)),w
	movwf	(?_KalmanFilter+2)
	goto	l869
	
l3397:	
	line	110
	
l869:	
	return
	opt stack 0
GLOBAL	__end_of_KalmanFilter
	__end_of_KalmanFilter:
;; =============== function _KalmanFilter ends ============

	signat	_KalmanFilter,8315
	global	___altoft
psect	text368,local,class=CODE,delta=2
global __ptext368
__ptext368:

;; *************** function ___altoft *****************
;; Defined at:
;;		line 43 in file "C:\Program Files (x86)\HI-TECH Software\PICC\9.80\sources\altoft.c"
;; Parameters:    Size  Location     Type
;;  c               4    8[COMMON] long 
;; Auto vars:     Size  Location     Type
;;  sign            1    1[BANK0 ] unsigned char 
;;  exp             1    0[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;                  3    8[COMMON] float 
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         4       0       0       0       0
;;      Locals:         0       2       0       0       0
;;      Temps:          2       0       0       0       0
;;      Totals:         6       2       0       0       0
;;Total ram usage:        8 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		___ftpack
;; This function is called by:
;;		_kalman_set
;; This function uses a non-reentrant model
;;
psect	text368
	file	"C:\Program Files (x86)\HI-TECH Software\PICC\9.80\sources\altoft.c"
	line	43
	global	__size_of___altoft
	__size_of___altoft	equ	__end_of___altoft-___altoft
	
___altoft:	
	opt	stack 4
; Regs used in ___altoft: [wreg+status,2+status,0+pclath+cstack]
	line	45
	
l3347:	
	clrc
	movlw	0
	btfsc	status,0
	movlw	1
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(___altoft@sign)
	line	46
	
l3349:	
	movlw	(08Eh)
	movwf	(??___altoft+0)+0
	movf	(??___altoft+0)+0,w
	movwf	(___altoft@exp)
	line	47
	
l3351:	
	btfss	(___altoft@c+3),7
	goto	u2961
	goto	u2960
u2961:
	goto	l3359
u2960:
	line	48
	
l3353:	
	comf	(___altoft@c),f
	comf	(___altoft@c+1),f
	comf	(___altoft@c+2),f
	comf	(___altoft@c+3),f
	incf	(___altoft@c),f
	skipnz
	incf	(___altoft@c+1),f
	skipnz
	incf	(___altoft@c+2),f
	skipnz
	incf	(___altoft@c+3),f
	line	49
	
l3355:	
	clrf	(___altoft@sign)
	bsf	status,0
	rlf	(___altoft@sign),f
	goto	l3359
	line	50
	
l1127:	
	line	52
	goto	l3359
	
l1129:	
	line	53
	
l3357:	
	movlw	01h
u2975:
	clrc
	rrf	(___altoft@c+3),f
	rrf	(___altoft@c+2),f
	rrf	(___altoft@c+1),f
	rrf	(___altoft@c),f
	addlw	-1
	skipz
	goto	u2975

	line	54
	movlw	(01h)
	movwf	(??___altoft+0)+0
	movf	(??___altoft+0)+0,w
	addwf	(___altoft@exp),f
	goto	l3359
	line	55
	
l1128:	
	line	52
	
l3359:	
	movlw	high highword(-16777216)
	andwf	(___altoft@c+3),w
	btfss	status,2
	goto	u2981
	goto	u2980
u2981:
	goto	l3357
u2980:
	goto	l3361
	
l1130:	
	line	56
	
l3361:	
	movf	(___altoft@c),w
	movwf	(?___ftpack)
	movf	(___altoft@c+1),w
	movwf	(?___ftpack+1)
	movf	(___altoft@c+2),w
	movwf	(?___ftpack+2)
	movf	(___altoft@exp),w
	movwf	(??___altoft+0)+0
	movf	(??___altoft+0)+0,w
	movwf	0+(?___ftpack)+03h
	movf	(___altoft@sign),w
	movwf	(??___altoft+1)+0
	movf	(??___altoft+1)+0,w
	movwf	0+(?___ftpack)+04h
	fcall	___ftpack
	movf	(0+(?___ftpack)),w
	movwf	(?___altoft)
	movf	(1+(?___ftpack)),w
	movwf	(?___altoft+1)
	movf	(2+(?___ftpack)),w
	movwf	(?___altoft+2)
	goto	l1131
	
l3363:	
	line	57
	
l1131:	
	return
	opt stack 0
GLOBAL	__end_of___altoft
	__end_of___altoft:
;; =============== function ___altoft ends ============

	signat	___altoft,4219
	global	___lwtoft
psect	text369,local,class=CODE,delta=2
global __ptext369
__ptext369:

;; *************** function ___lwtoft *****************
;; Defined at:
;;		line 29 in file "C:\Program Files (x86)\HI-TECH Software\PICC\9.80\sources\lwtoft.c"
;; Parameters:    Size  Location     Type
;;  c               2    6[BANK0 ] unsigned int 
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;                  3    6[BANK0 ] float 
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       3       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          1       0       0       0       0
;;      Totals:         1       3       0       0       0
;;Total ram usage:        4 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		___ftpack
;; This function is called by:
;;		_KalmanFilter
;; This function uses a non-reentrant model
;;
psect	text369
	file	"C:\Program Files (x86)\HI-TECH Software\PICC\9.80\sources\lwtoft.c"
	line	29
	global	__size_of___lwtoft
	__size_of___lwtoft	equ	__end_of___lwtoft-___lwtoft
	
___lwtoft:	
	opt	stack 5
; Regs used in ___lwtoft: [wreg+status,2+status,0+pclath+cstack]
	line	30
	
l3343:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(___lwtoft@c),w
	movwf	(?___ftpack)
	movf	(___lwtoft@c+1),w
	movwf	(?___ftpack+1)
	clrf	(?___ftpack+2)
	movlw	(08Eh)
	movwf	(??___lwtoft+0)+0
	movf	(??___lwtoft+0)+0,w
	movwf	0+(?___ftpack)+03h
	clrc
	movlw	0
	btfsc	status,0
	movlw	1
	movwf	0+(?___ftpack)+04h
	fcall	___ftpack
	movf	(0+(?___ftpack)),w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(?___lwtoft)
	movf	(1+(?___ftpack)),w
	movwf	(?___lwtoft+1)
	movf	(2+(?___ftpack)),w
	movwf	(?___lwtoft+2)
	goto	l1122
	
l3345:	
	line	31
	
l1122:	
	return
	opt stack 0
GLOBAL	__end_of___lwtoft
	__end_of___lwtoft:
;; =============== function ___lwtoft ends ============

	signat	___lwtoft,4219
	global	___lbtoft
psect	text370,local,class=CODE,delta=2
global __ptext370
__ptext370:

;; *************** function ___lbtoft *****************
;; Defined at:
;;		line 28 in file "C:\Program Files (x86)\HI-TECH Software\PICC\9.80\sources\lbtoft.c"
;; Parameters:    Size  Location     Type
;;  c               1    wreg     unsigned char 
;; Auto vars:     Size  Location     Type
;;  c               1   11[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;                  3    8[COMMON] float 
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         3       0       0       0       0
;;      Locals:         1       0       0       0       0
;;      Temps:          0       4       0       0       0
;;      Totals:         4       4       0       0       0
;;Total ram usage:        8 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		___ftpack
;; This function is called by:
;;		_KalmanFilter
;; This function uses a non-reentrant model
;;
psect	text370
	file	"C:\Program Files (x86)\HI-TECH Software\PICC\9.80\sources\lbtoft.c"
	line	28
	global	__size_of___lbtoft
	__size_of___lbtoft	equ	__end_of___lbtoft-___lbtoft
	
___lbtoft:	
	opt	stack 5
; Regs used in ___lbtoft: [wreg+status,2+status,0+pclath+cstack]
;___lbtoft@c stored from wreg
	movwf	(___lbtoft@c)
	line	29
	
l3339:	
	movf	(___lbtoft@c),w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	((??___lbtoft+0)+0)
	clrf	((??___lbtoft+0)+0+1)
	clrf	((??___lbtoft+0)+0+2)
	movf	0+(??___lbtoft+0)+0,w
	movwf	(?___ftpack)
	movf	1+(??___lbtoft+0)+0,w
	movwf	(?___ftpack+1)
	movf	2+(??___lbtoft+0)+0,w
	movwf	(?___ftpack+2)
	movlw	(08Eh)
	movwf	(??___lbtoft+3)+0
	movf	(??___lbtoft+3)+0,w
	movwf	0+(?___ftpack)+03h
	clrc
	movlw	0
	btfsc	status,0
	movlw	1
	movwf	0+(?___ftpack)+04h
	fcall	___ftpack
	movf	(0+(?___ftpack)),w
	movwf	(?___lbtoft)
	movf	(1+(?___ftpack)),w
	movwf	(?___lbtoft+1)
	movf	(2+(?___ftpack)),w
	movwf	(?___lbtoft+2)
	goto	l1024
	
l3341:	
	line	30
	
l1024:	
	return
	opt stack 0
GLOBAL	__end_of___lbtoft
	__end_of___lbtoft:
;; =============== function ___lbtoft ends ============

	signat	___lbtoft,4219
	global	___ftmul
psect	text371,local,class=CODE,delta=2
global __ptext371
__ptext371:

;; *************** function ___ftmul *****************
;; Defined at:
;;		line 52 in file "C:\Program Files (x86)\HI-TECH Software\PICC\9.80\sources\ftmul.c"
;; Parameters:    Size  Location     Type
;;  f1              3    4[BANK0 ] float 
;;  f2              3    7[BANK0 ] float 
;; Auto vars:     Size  Location     Type
;;  f3_as_produc    3   15[BANK0 ] unsigned um
;;  sign            1   19[BANK0 ] unsigned char 
;;  cntr            1   18[BANK0 ] unsigned char 
;;  exp             1   14[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;                  3    4[BANK0 ] float 
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       6       0       0       0
;;      Locals:         0       6       0       0       0
;;      Temps:          0       4       0       0       0
;;      Totals:         0      16       0       0       0
;;Total ram usage:       16 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		___ftpack
;; This function is called by:
;;		_KalmanFilter
;;		_kalman_filter
;; This function uses a non-reentrant model
;;
psect	text371
	file	"C:\Program Files (x86)\HI-TECH Software\PICC\9.80\sources\ftmul.c"
	line	52
	global	__size_of___ftmul
	__size_of___ftmul	equ	__end_of___ftmul-___ftmul
	
___ftmul:	
	opt	stack 4
; Regs used in ___ftmul: [wreg+status,2+status,0+pclath+cstack]
	line	56
	
l3289:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(___ftmul@f1),w
	movwf	((??___ftmul+0)+0)
	movf	(___ftmul@f1+1),w
	movwf	((??___ftmul+0)+0+1)
	movf	(___ftmul@f1+2),w
	movwf	((??___ftmul+0)+0+2)
	clrc
	rlf	(??___ftmul+0)+1,w
	rlf	(??___ftmul+0)+2,w
	movwf	(??___ftmul+3)+0
	movf	(??___ftmul+3)+0,w
	movwf	(___ftmul@exp)
	movf	((___ftmul@exp)),f
	skipz
	goto	u2821
	goto	u2820
u2821:
	goto	l3295
u2820:
	line	57
	
l3291:	
	movlw	0x0
	movwf	(?___ftmul)
	movlw	0x0
	movwf	(?___ftmul+1)
	movlw	0x0
	movwf	(?___ftmul+2)
	goto	l998
	
l3293:	
	goto	l998
	
l997:	
	line	58
	
l3295:	
	movf	(___ftmul@f2),w
	movwf	((??___ftmul+0)+0)
	movf	(___ftmul@f2+1),w
	movwf	((??___ftmul+0)+0+1)
	movf	(___ftmul@f2+2),w
	movwf	((??___ftmul+0)+0+2)
	clrc
	rlf	(??___ftmul+0)+1,w
	rlf	(??___ftmul+0)+2,w
	movwf	(??___ftmul+3)+0
	movf	(??___ftmul+3)+0,w
	movwf	(___ftmul@sign)
	movf	((___ftmul@sign)),f
	skipz
	goto	u2831
	goto	u2830
u2831:
	goto	l3301
u2830:
	line	59
	
l3297:	
	movlw	0x0
	movwf	(?___ftmul)
	movlw	0x0
	movwf	(?___ftmul+1)
	movlw	0x0
	movwf	(?___ftmul+2)
	goto	l998
	
l3299:	
	goto	l998
	
l999:	
	line	60
	
l3301:	
	movf	(___ftmul@sign),w
	addlw	07Bh
	movwf	(??___ftmul+0)+0
	movf	(??___ftmul+0)+0,w
	addwf	(___ftmul@exp),f
	line	61
	movf	(___ftmul@f1),w
	movwf	((??___ftmul+0)+0)
	movf	(___ftmul@f1+1),w
	movwf	((??___ftmul+0)+0+1)
	movf	(___ftmul@f1+2),w
	movwf	((??___ftmul+0)+0+2)
	movlw	010h
u2845:
	clrc
	rrf	(??___ftmul+0)+2,f
	rrf	(??___ftmul+0)+1,f
	rrf	(??___ftmul+0)+0,f
u2840:
	addlw	-1
	skipz
	goto	u2845
	movf	0+(??___ftmul+0)+0,w
	movwf	(??___ftmul+3)+0
	movf	(??___ftmul+3)+0,w
	movwf	(___ftmul@sign)
	line	62
	movf	(___ftmul@f2),w
	movwf	((??___ftmul+0)+0)
	movf	(___ftmul@f2+1),w
	movwf	((??___ftmul+0)+0+1)
	movf	(___ftmul@f2+2),w
	movwf	((??___ftmul+0)+0+2)
	movlw	010h
u2855:
	clrc
	rrf	(??___ftmul+0)+2,f
	rrf	(??___ftmul+0)+1,f
	rrf	(??___ftmul+0)+0,f
u2850:
	addlw	-1
	skipz
	goto	u2855
	movf	0+(??___ftmul+0)+0,w
	movwf	(??___ftmul+3)+0
	movf	(??___ftmul+3)+0,w
	xorwf	(___ftmul@sign),f
	line	63
	movlw	(080h)
	movwf	(??___ftmul+0)+0
	movf	(??___ftmul+0)+0,w
	andwf	(___ftmul@sign),f
	line	64
	
l3303:	
	bsf	(___ftmul@f1)+(15/8),(15)&7
	line	66
	
l3305:	
	bsf	(___ftmul@f2)+(15/8),(15)&7
	line	67
	
l3307:	
	movlw	0FFh
	andwf	(___ftmul@f2),f
	movlw	0FFh
	andwf	(___ftmul@f2+1),f
	movlw	0
	andwf	(___ftmul@f2+2),f
	line	68
	
l3309:	
	movlw	0
	movwf	(___ftmul@f3_as_product)
	movlw	0
	movwf	(___ftmul@f3_as_product+1)
	movlw	0
	movwf	(___ftmul@f3_as_product+2)
	line	69
	
l3311:	
	movlw	(07h)
	movwf	(??___ftmul+0)+0
	movf	(??___ftmul+0)+0,w
	movwf	(___ftmul@cntr)
	goto	l3313
	line	70
	
l1000:	
	line	71
	
l3313:	
	btfss	(___ftmul@f1),(0)&7
	goto	u2861
	goto	u2860
u2861:
	goto	l3317
u2860:
	line	72
	
l3315:	
	movf	(___ftmul@f2),w
	addwf	(___ftmul@f3_as_product),f
	movf	(___ftmul@f2+1),w
	clrz
	skipnc
	incf	(___ftmul@f2+1),w
	skipnz
	goto	u2871
	addwf	(___ftmul@f3_as_product+1),f
u2871:
	movf	(___ftmul@f2+2),w
	clrz
	skipnc
	incf	(___ftmul@f2+2),w
	skipnz
	goto	u2872
	addwf	(___ftmul@f3_as_product+2),f
u2872:

	goto	l3317
	
l1001:	
	line	73
	
l3317:	
	movlw	01h
u2885:
	clrc
	rrf	(___ftmul@f1+2),f
	rrf	(___ftmul@f1+1),f
	rrf	(___ftmul@f1),f
	addlw	-1
	skipz
	goto	u2885

	line	74
	
l3319:	
	movlw	01h
u2895:
	clrc
	rlf	(___ftmul@f2),f
	rlf	(___ftmul@f2+1),f
	rlf	(___ftmul@f2+2),f
	addlw	-1
	skipz
	goto	u2895
	line	75
	
l3321:	
	movlw	low(01h)
	subwf	(___ftmul@cntr),f
	btfss	status,2
	goto	u2901
	goto	u2900
u2901:
	goto	l3313
u2900:
	goto	l3323
	
l1002:	
	line	76
	
l3323:	
	movlw	(09h)
	movwf	(??___ftmul+0)+0
	movf	(??___ftmul+0)+0,w
	movwf	(___ftmul@cntr)
	goto	l3325
	line	77
	
l1003:	
	line	78
	
l3325:	
	btfss	(___ftmul@f1),(0)&7
	goto	u2911
	goto	u2910
u2911:
	goto	l3329
u2910:
	line	79
	
l3327:	
	movf	(___ftmul@f2),w
	addwf	(___ftmul@f3_as_product),f
	movf	(___ftmul@f2+1),w
	clrz
	skipnc
	incf	(___ftmul@f2+1),w
	skipnz
	goto	u2921
	addwf	(___ftmul@f3_as_product+1),f
u2921:
	movf	(___ftmul@f2+2),w
	clrz
	skipnc
	incf	(___ftmul@f2+2),w
	skipnz
	goto	u2922
	addwf	(___ftmul@f3_as_product+2),f
u2922:

	goto	l3329
	
l1004:	
	line	80
	
l3329:	
	movlw	01h
u2935:
	clrc
	rrf	(___ftmul@f1+2),f
	rrf	(___ftmul@f1+1),f
	rrf	(___ftmul@f1),f
	addlw	-1
	skipz
	goto	u2935

	line	81
	
l3331:	
	movlw	01h
u2945:
	clrc
	rrf	(___ftmul@f3_as_product+2),f
	rrf	(___ftmul@f3_as_product+1),f
	rrf	(___ftmul@f3_as_product),f
	addlw	-1
	skipz
	goto	u2945

	line	82
	
l3333:	
	movlw	low(01h)
	subwf	(___ftmul@cntr),f
	btfss	status,2
	goto	u2951
	goto	u2950
u2951:
	goto	l3325
u2950:
	goto	l3335
	
l1005:	
	line	83
	
l3335:	
	movf	(___ftmul@f3_as_product),w
	movwf	(?___ftpack)
	movf	(___ftmul@f3_as_product+1),w
	movwf	(?___ftpack+1)
	movf	(___ftmul@f3_as_product+2),w
	movwf	(?___ftpack+2)
	movf	(___ftmul@exp),w
	movwf	(??___ftmul+0)+0
	movf	(??___ftmul+0)+0,w
	movwf	0+(?___ftpack)+03h
	movf	(___ftmul@sign),w
	movwf	(??___ftmul+1)+0
	movf	(??___ftmul+1)+0,w
	movwf	0+(?___ftpack)+04h
	fcall	___ftpack
	movf	(0+(?___ftpack)),w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(?___ftmul)
	movf	(1+(?___ftpack)),w
	movwf	(?___ftmul+1)
	movf	(2+(?___ftpack)),w
	movwf	(?___ftmul+2)
	goto	l998
	
l3337:	
	line	84
	
l998:	
	return
	opt stack 0
GLOBAL	__end_of___ftmul
	__end_of___ftmul:
;; =============== function ___ftmul ends ============

	signat	___ftmul,8315
	global	___ftdiv
psect	text372,local,class=CODE,delta=2
global __ptext372
__ptext372:

;; *************** function ___ftdiv *****************
;; Defined at:
;;		line 50 in file "C:\Program Files (x86)\HI-TECH Software\PICC\9.80\sources\ftdiv.c"
;; Parameters:    Size  Location     Type
;;  f2              3   33[BANK0 ] float 
;;  f1              3   36[BANK0 ] float 
;; Auto vars:     Size  Location     Type
;;  f3              3   44[BANK0 ] float 
;;  sign            1   48[BANK0 ] unsigned char 
;;  exp             1   47[BANK0 ] unsigned char 
;;  cntr            1   43[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;                  3   33[BANK0 ] float 
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       6       0       0       0
;;      Locals:         0       6       0       0       0
;;      Temps:          0       4       0       0       0
;;      Totals:         0      16       0       0       0
;;Total ram usage:       16 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		___ftpack
;; This function is called by:
;;		_KalmanFilter
;;		_kalman_filter
;; This function uses a non-reentrant model
;;
psect	text372
	file	"C:\Program Files (x86)\HI-TECH Software\PICC\9.80\sources\ftdiv.c"
	line	50
	global	__size_of___ftdiv
	__size_of___ftdiv	equ	__end_of___ftdiv-___ftdiv
	
___ftdiv:	
	opt	stack 4
; Regs used in ___ftdiv: [wreg+status,2+status,0+pclath+cstack]
	line	55
	
l3247:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(___ftdiv@f1),w
	movwf	((??___ftdiv+0)+0)
	movf	(___ftdiv@f1+1),w
	movwf	((??___ftdiv+0)+0+1)
	movf	(___ftdiv@f1+2),w
	movwf	((??___ftdiv+0)+0+2)
	clrc
	rlf	(??___ftdiv+0)+1,w
	rlf	(??___ftdiv+0)+2,w
	movwf	(??___ftdiv+3)+0
	movf	(??___ftdiv+3)+0,w
	movwf	(___ftdiv@exp)
	movf	((___ftdiv@exp)),f
	skipz
	goto	u2741
	goto	u2740
u2741:
	goto	l3253
u2740:
	line	56
	
l3249:	
	movlw	0x0
	movwf	(?___ftdiv)
	movlw	0x0
	movwf	(?___ftdiv+1)
	movlw	0x0
	movwf	(?___ftdiv+2)
	goto	l988
	
l3251:	
	goto	l988
	
l987:	
	line	57
	
l3253:	
	movf	(___ftdiv@f2),w
	movwf	((??___ftdiv+0)+0)
	movf	(___ftdiv@f2+1),w
	movwf	((??___ftdiv+0)+0+1)
	movf	(___ftdiv@f2+2),w
	movwf	((??___ftdiv+0)+0+2)
	clrc
	rlf	(??___ftdiv+0)+1,w
	rlf	(??___ftdiv+0)+2,w
	movwf	(??___ftdiv+3)+0
	movf	(??___ftdiv+3)+0,w
	movwf	(___ftdiv@sign)
	movf	((___ftdiv@sign)),f
	skipz
	goto	u2751
	goto	u2750
u2751:
	goto	l3259
u2750:
	line	58
	
l3255:	
	movlw	0x0
	movwf	(?___ftdiv)
	movlw	0x0
	movwf	(?___ftdiv+1)
	movlw	0x0
	movwf	(?___ftdiv+2)
	goto	l988
	
l3257:	
	goto	l988
	
l989:	
	line	59
	
l3259:	
	movlw	0
	movwf	(___ftdiv@f3)
	movlw	0
	movwf	(___ftdiv@f3+1)
	movlw	0
	movwf	(___ftdiv@f3+2)
	line	60
	
l3261:	
	movlw	(089h)
	addwf	(___ftdiv@sign),w
	movwf	(??___ftdiv+0)+0
	movf	0+(??___ftdiv+0)+0,w
	subwf	(___ftdiv@exp),f
	line	61
	
l3263:	
	movf	(___ftdiv@f1),w
	movwf	((??___ftdiv+0)+0)
	movf	(___ftdiv@f1+1),w
	movwf	((??___ftdiv+0)+0+1)
	movf	(___ftdiv@f1+2),w
	movwf	((??___ftdiv+0)+0+2)
	movlw	010h
u2765:
	clrc
	rrf	(??___ftdiv+0)+2,f
	rrf	(??___ftdiv+0)+1,f
	rrf	(??___ftdiv+0)+0,f
u2760:
	addlw	-1
	skipz
	goto	u2765
	movf	0+(??___ftdiv+0)+0,w
	movwf	(??___ftdiv+3)+0
	movf	(??___ftdiv+3)+0,w
	movwf	(___ftdiv@sign)
	line	62
	
l3265:	
	movf	(___ftdiv@f2),w
	movwf	((??___ftdiv+0)+0)
	movf	(___ftdiv@f2+1),w
	movwf	((??___ftdiv+0)+0+1)
	movf	(___ftdiv@f2+2),w
	movwf	((??___ftdiv+0)+0+2)
	movlw	010h
u2775:
	clrc
	rrf	(??___ftdiv+0)+2,f
	rrf	(??___ftdiv+0)+1,f
	rrf	(??___ftdiv+0)+0,f
u2770:
	addlw	-1
	skipz
	goto	u2775
	movf	0+(??___ftdiv+0)+0,w
	movwf	(??___ftdiv+3)+0
	movf	(??___ftdiv+3)+0,w
	xorwf	(___ftdiv@sign),f
	line	63
	
l3267:	
	movlw	(080h)
	movwf	(??___ftdiv+0)+0
	movf	(??___ftdiv+0)+0,w
	andwf	(___ftdiv@sign),f
	line	64
	
l3269:	
	bsf	(___ftdiv@f1)+(15/8),(15)&7
	line	65
	movlw	0FFh
	andwf	(___ftdiv@f1),f
	movlw	0FFh
	andwf	(___ftdiv@f1+1),f
	movlw	0
	andwf	(___ftdiv@f1+2),f
	line	66
	
l3271:	
	bsf	(___ftdiv@f2)+(15/8),(15)&7
	line	67
	movlw	0FFh
	andwf	(___ftdiv@f2),f
	movlw	0FFh
	andwf	(___ftdiv@f2+1),f
	movlw	0
	andwf	(___ftdiv@f2+2),f
	line	68
	movlw	(018h)
	movwf	(??___ftdiv+0)+0
	movf	(??___ftdiv+0)+0,w
	movwf	(___ftdiv@cntr)
	goto	l3273
	line	69
	
l990:	
	line	70
	
l3273:	
	movlw	01h
u2785:
	clrc
	rlf	(___ftdiv@f3),f
	rlf	(___ftdiv@f3+1),f
	rlf	(___ftdiv@f3+2),f
	addlw	-1
	skipz
	goto	u2785
	line	71
	
l3275:	
	movf	(___ftdiv@f2+2),w
	subwf	(___ftdiv@f1+2),w
	skipz
	goto	u2795
	movf	(___ftdiv@f2+1),w
	subwf	(___ftdiv@f1+1),w
	skipz
	goto	u2795
	movf	(___ftdiv@f2),w
	subwf	(___ftdiv@f1),w
u2795:
	skipc
	goto	u2791
	goto	u2790
u2791:
	goto	l3281
u2790:
	line	72
	
l3277:	
	movf	(___ftdiv@f2),w
	subwf	(___ftdiv@f1),f
	movf	(___ftdiv@f2+1),w
	skipc
	incfsz	(___ftdiv@f2+1),w
	subwf	(___ftdiv@f1+1),f
	movf	(___ftdiv@f2+2),w
	skipc
	incf	(___ftdiv@f2+2),w
	subwf	(___ftdiv@f1+2),f
	line	73
	
l3279:	
	bsf	(___ftdiv@f3)+(0/8),(0)&7
	goto	l3281
	line	74
	
l991:	
	line	75
	
l3281:	
	movlw	01h
u2805:
	clrc
	rlf	(___ftdiv@f1),f
	rlf	(___ftdiv@f1+1),f
	rlf	(___ftdiv@f1+2),f
	addlw	-1
	skipz
	goto	u2805
	line	76
	
l3283:	
	movlw	low(01h)
	subwf	(___ftdiv@cntr),f
	btfss	status,2
	goto	u2811
	goto	u2810
u2811:
	goto	l3273
u2810:
	goto	l3285
	
l992:	
	line	77
	
l3285:	
	movf	(___ftdiv@f3),w
	movwf	(?___ftpack)
	movf	(___ftdiv@f3+1),w
	movwf	(?___ftpack+1)
	movf	(___ftdiv@f3+2),w
	movwf	(?___ftpack+2)
	movf	(___ftdiv@exp),w
	movwf	(??___ftdiv+0)+0
	movf	(??___ftdiv+0)+0,w
	movwf	0+(?___ftpack)+03h
	movf	(___ftdiv@sign),w
	movwf	(??___ftdiv+1)+0
	movf	(??___ftdiv+1)+0,w
	movwf	0+(?___ftpack)+04h
	fcall	___ftpack
	movf	(0+(?___ftpack)),w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(?___ftdiv)
	movf	(1+(?___ftpack)),w
	movwf	(?___ftdiv+1)
	movf	(2+(?___ftpack)),w
	movwf	(?___ftdiv+2)
	goto	l988
	
l3287:	
	line	78
	
l988:	
	return
	opt stack 0
GLOBAL	__end_of___ftdiv
	__end_of___ftdiv:
;; =============== function ___ftdiv ends ============

	signat	___ftdiv,8315
	global	___ftadd
psect	text373,local,class=CODE,delta=2
global __ptext373
__ptext373:

;; *************** function ___ftadd *****************
;; Defined at:
;;		line 87 in file "C:\Program Files (x86)\HI-TECH Software\PICC\9.80\sources\ftadd.c"
;; Parameters:    Size  Location     Type
;;  f1              3   20[BANK0 ] float 
;;  f2              3   23[BANK0 ] float 
;; Auto vars:     Size  Location     Type
;;  exp1            1   32[BANK0 ] unsigned char 
;;  exp2            1   31[BANK0 ] unsigned char 
;;  sign            1   30[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;                  3   20[BANK0 ] float 
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       6       0       0       0
;;      Locals:         0       3       0       0       0
;;      Temps:          0       4       0       0       0
;;      Totals:         0      13       0       0       0
;;Total ram usage:       13 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		___ftpack
;; This function is called by:
;;		_KalmanFilter
;;		_kalman_filter
;;		_Eric_main
;;		___ftsub
;; This function uses a non-reentrant model
;;
psect	text373
	file	"C:\Program Files (x86)\HI-TECH Software\PICC\9.80\sources\ftadd.c"
	line	87
	global	__size_of___ftadd
	__size_of___ftadd	equ	__end_of___ftadd-___ftadd
	
___ftadd:	
	opt	stack 5
; Regs used in ___ftadd: [wreg+status,2+status,0+pclath+cstack]
	line	90
	
l3175:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(___ftadd@f1),w
	movwf	((??___ftadd+0)+0)
	movf	(___ftadd@f1+1),w
	movwf	((??___ftadd+0)+0+1)
	movf	(___ftadd@f1+2),w
	movwf	((??___ftadd+0)+0+2)
	clrc
	rlf	(??___ftadd+0)+1,w
	rlf	(??___ftadd+0)+2,w
	movwf	(??___ftadd+3)+0
	movf	(??___ftadd+3)+0,w
	movwf	(___ftadd@exp1)
	line	91
	movf	(___ftadd@f2),w
	movwf	((??___ftadd+0)+0)
	movf	(___ftadd@f2+1),w
	movwf	((??___ftadd+0)+0+1)
	movf	(___ftadd@f2+2),w
	movwf	((??___ftadd+0)+0+2)
	clrc
	rlf	(??___ftadd+0)+1,w
	rlf	(??___ftadd+0)+2,w
	movwf	(??___ftadd+3)+0
	movf	(??___ftadd+3)+0,w
	movwf	(___ftadd@exp2)
	line	92
	
l3177:	
	movf	(___ftadd@exp1),w
	skipz
	goto	u2500
	goto	l3183
u2500:
	
l3179:	
	movf	(___ftadd@exp2),w
	subwf	(___ftadd@exp1),w
	skipnc
	goto	u2511
	goto	u2510
u2511:
	goto	l3187
u2510:
	
l3181:	
	decf	(___ftadd@exp1),w
	xorlw	0ffh
	addwf	(___ftadd@exp2),w
	movwf	(??___ftadd+0)+0
	movlw	(019h)
	subwf	0+(??___ftadd+0)+0,w
	skipc
	goto	u2521
	goto	u2520
u2521:
	goto	l3187
u2520:
	goto	l3183
	
l945:	
	line	93
	
l3183:	
	movf	(___ftadd@f2),w
	movwf	(?___ftadd)
	movf	(___ftadd@f2+1),w
	movwf	(?___ftadd+1)
	movf	(___ftadd@f2+2),w
	movwf	(?___ftadd+2)
	goto	l946
	
l3185:	
	goto	l946
	
l943:	
	line	94
	
l3187:	
	movf	(___ftadd@exp2),w
	skipz
	goto	u2530
	goto	l949
u2530:
	
l3189:	
	movf	(___ftadd@exp1),w
	subwf	(___ftadd@exp2),w
	skipnc
	goto	u2541
	goto	u2540
u2541:
	goto	l3193
u2540:
	
l3191:	
	decf	(___ftadd@exp2),w
	xorlw	0ffh
	addwf	(___ftadd@exp1),w
	movwf	(??___ftadd+0)+0
	movlw	(019h)
	subwf	0+(??___ftadd+0)+0,w
	skipc
	goto	u2551
	goto	u2550
u2551:
	goto	l3193
u2550:
	
l949:	
	line	95
	goto	l946
	
l947:	
	line	96
	
l3193:	
	movlw	(06h)
	movwf	(??___ftadd+0)+0
	movf	(??___ftadd+0)+0,w
	movwf	(___ftadd@sign)
	line	97
	
l3195:	
	btfss	(___ftadd@f1+2),(23)&7
	goto	u2561
	goto	u2560
u2561:
	goto	l950
u2560:
	line	98
	
l3197:	
	bsf	(___ftadd@sign)+(7/8),(7)&7
	
l950:	
	line	99
	btfss	(___ftadd@f2+2),(23)&7
	goto	u2571
	goto	u2570
u2571:
	goto	l951
u2570:
	line	100
	
l3199:	
	bsf	(___ftadd@sign)+(6/8),(6)&7
	
l951:	
	line	101
	bsf	(___ftadd@f1)+(15/8),(15)&7
	line	102
	
l3201:	
	movlw	0FFh
	andwf	(___ftadd@f1),f
	movlw	0FFh
	andwf	(___ftadd@f1+1),f
	movlw	0
	andwf	(___ftadd@f1+2),f
	line	103
	
l3203:	
	bsf	(___ftadd@f2)+(15/8),(15)&7
	line	104
	movlw	0FFh
	andwf	(___ftadd@f2),f
	movlw	0FFh
	andwf	(___ftadd@f2+1),f
	movlw	0
	andwf	(___ftadd@f2+2),f
	line	106
	movf	(___ftadd@exp2),w
	subwf	(___ftadd@exp1),w
	skipnc
	goto	u2581
	goto	u2580
u2581:
	goto	l3215
u2580:
	goto	l3205
	line	109
	
l953:	
	line	110
	
l3205:	
	movlw	01h
u2595:
	clrc
	rlf	(___ftadd@f2),f
	rlf	(___ftadd@f2+1),f
	rlf	(___ftadd@f2+2),f
	addlw	-1
	skipz
	goto	u2595
	line	111
	movlw	low(01h)
	subwf	(___ftadd@exp2),f
	line	112
	
l3207:	
	movf	(___ftadd@exp2),w
	xorwf	(___ftadd@exp1),w
	skipnz
	goto	u2601
	goto	u2600
u2601:
	goto	l3213
u2600:
	
l3209:	
	movlw	low(01h)
	subwf	(___ftadd@sign),f
	movf	((___ftadd@sign)),w
	andlw	07h
	btfss	status,2
	goto	u2611
	goto	u2610
u2611:
	goto	l3205
u2610:
	goto	l3213
	
l955:	
	goto	l3213
	
l956:	
	line	113
	goto	l3213
	
l958:	
	line	114
	
l3211:	
	movlw	01h
u2625:
	clrc
	rrf	(___ftadd@f1+2),f
	rrf	(___ftadd@f1+1),f
	rrf	(___ftadd@f1),f
	addlw	-1
	skipz
	goto	u2625

	line	115
	movlw	(01h)
	movwf	(??___ftadd+0)+0
	movf	(??___ftadd+0)+0,w
	addwf	(___ftadd@exp1),f
	goto	l3213
	line	116
	
l957:	
	line	113
	
l3213:	
	movf	(___ftadd@exp1),w
	xorwf	(___ftadd@exp2),w
	skipz
	goto	u2631
	goto	u2630
u2631:
	goto	l3211
u2630:
	goto	l960
	
l959:	
	line	117
	goto	l960
	
l952:	
	
l3215:	
	movf	(___ftadd@exp1),w
	subwf	(___ftadd@exp2),w
	skipnc
	goto	u2641
	goto	u2640
u2641:
	goto	l960
u2640:
	goto	l3217
	line	120
	
l962:	
	line	121
	
l3217:	
	movlw	01h
u2655:
	clrc
	rlf	(___ftadd@f1),f
	rlf	(___ftadd@f1+1),f
	rlf	(___ftadd@f1+2),f
	addlw	-1
	skipz
	goto	u2655
	line	122
	movlw	low(01h)
	subwf	(___ftadd@exp1),f
	line	123
	
l3219:	
	movf	(___ftadd@exp2),w
	xorwf	(___ftadd@exp1),w
	skipnz
	goto	u2661
	goto	u2660
u2661:
	goto	l3225
u2660:
	
l3221:	
	movlw	low(01h)
	subwf	(___ftadd@sign),f
	movf	((___ftadd@sign)),w
	andlw	07h
	btfss	status,2
	goto	u2671
	goto	u2670
u2671:
	goto	l3217
u2670:
	goto	l3225
	
l964:	
	goto	l3225
	
l965:	
	line	124
	goto	l3225
	
l967:	
	line	125
	
l3223:	
	movlw	01h
u2685:
	clrc
	rrf	(___ftadd@f2+2),f
	rrf	(___ftadd@f2+1),f
	rrf	(___ftadd@f2),f
	addlw	-1
	skipz
	goto	u2685

	line	126
	movlw	(01h)
	movwf	(??___ftadd+0)+0
	movf	(??___ftadd+0)+0,w
	addwf	(___ftadd@exp2),f
	goto	l3225
	line	127
	
l966:	
	line	124
	
l3225:	
	movf	(___ftadd@exp1),w
	xorwf	(___ftadd@exp2),w
	skipz
	goto	u2691
	goto	u2690
u2691:
	goto	l3223
u2690:
	goto	l960
	
l968:	
	goto	l960
	line	128
	
l961:	
	line	129
	
l960:	
	btfss	(___ftadd@sign),(7)&7
	goto	u2701
	goto	u2700
u2701:
	goto	l3229
u2700:
	line	131
	
l3227:	
	movlw	0FFh
	xorwf	(___ftadd@f1),f
	movlw	0FFh
	xorwf	(___ftadd@f1+1),f
	movlw	0FFh
	xorwf	(___ftadd@f1+2),f
	line	132
	movlw	01h
	addwf	(___ftadd@f1),f
	movlw	0
	skipnc
movlw 1
	addwf	(___ftadd@f1+1),f
	movlw	0
	skipnc
movlw 1
	addwf	(___ftadd@f1+2),f
	goto	l3229
	line	133
	
l969:	
	line	134
	
l3229:	
	btfss	(___ftadd@sign),(6)&7
	goto	u2711
	goto	u2710
u2711:
	goto	l3233
u2710:
	line	136
	
l3231:	
	movlw	0FFh
	xorwf	(___ftadd@f2),f
	movlw	0FFh
	xorwf	(___ftadd@f2+1),f
	movlw	0FFh
	xorwf	(___ftadd@f2+2),f
	line	137
	movlw	01h
	addwf	(___ftadd@f2),f
	movlw	0
	skipnc
movlw 1
	addwf	(___ftadd@f2+1),f
	movlw	0
	skipnc
movlw 1
	addwf	(___ftadd@f2+2),f
	goto	l3233
	line	138
	
l970:	
	line	139
	
l3233:	
	clrc
	movlw	0
	btfsc	status,0
	movlw	1
	movwf	(___ftadd@sign)
	line	140
	
l3235:	
	movf	(___ftadd@f1),w
	addwf	(___ftadd@f2),f
	movf	(___ftadd@f1+1),w
	clrz
	skipnc
	incf	(___ftadd@f1+1),w
	skipnz
	goto	u2721
	addwf	(___ftadd@f2+1),f
u2721:
	movf	(___ftadd@f1+2),w
	clrz
	skipnc
	incf	(___ftadd@f1+2),w
	skipnz
	goto	u2722
	addwf	(___ftadd@f2+2),f
u2722:

	line	141
	
l3237:	
	btfss	(___ftadd@f2+2),(23)&7
	goto	u2731
	goto	u2730
u2731:
	goto	l3243
u2730:
	line	142
	
l3239:	
	movlw	0FFh
	xorwf	(___ftadd@f2),f
	movlw	0FFh
	xorwf	(___ftadd@f2+1),f
	movlw	0FFh
	xorwf	(___ftadd@f2+2),f
	line	143
	movlw	01h
	addwf	(___ftadd@f2),f
	movlw	0
	skipnc
movlw 1
	addwf	(___ftadd@f2+1),f
	movlw	0
	skipnc
movlw 1
	addwf	(___ftadd@f2+2),f
	line	144
	
l3241:	
	clrf	(___ftadd@sign)
	bsf	status,0
	rlf	(___ftadd@sign),f
	goto	l3243
	line	145
	
l971:	
	line	146
	
l3243:	
	movf	(___ftadd@f2),w
	movwf	(?___ftpack)
	movf	(___ftadd@f2+1),w
	movwf	(?___ftpack+1)
	movf	(___ftadd@f2+2),w
	movwf	(?___ftpack+2)
	movf	(___ftadd@exp1),w
	movwf	(??___ftadd+0)+0
	movf	(??___ftadd+0)+0,w
	movwf	0+(?___ftpack)+03h
	movf	(___ftadd@sign),w
	movwf	(??___ftadd+1)+0
	movf	(??___ftadd+1)+0,w
	movwf	0+(?___ftpack)+04h
	fcall	___ftpack
	movf	(0+(?___ftpack)),w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(?___ftadd)
	movf	(1+(?___ftpack)),w
	movwf	(?___ftadd+1)
	movf	(2+(?___ftpack)),w
	movwf	(?___ftadd+2)
	goto	l946
	
l3245:	
	line	148
	
l946:	
	return
	opt stack 0
GLOBAL	__end_of___ftadd
	__end_of___ftadd:
;; =============== function ___ftadd ends ============

	signat	___ftadd,8315
	global	___ftneg
psect	text374,local,class=CODE,delta=2
global __ptext374
__ptext374:

;; *************** function ___ftneg *****************
;; Defined at:
;;		line 16 in file "C:\Program Files (x86)\HI-TECH Software\PICC\9.80\sources\ftneg.c"
;; Parameters:    Size  Location     Type
;;  f1              3    8[COMMON] float 
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;                  3    8[COMMON] float 
;; Registers used:
;;		wreg
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         3       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         3       0       0       0       0
;;Total ram usage:        3 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_KalmanFilter
;;		_kalman_filter
;; This function uses a non-reentrant model
;;
psect	text374
	file	"C:\Program Files (x86)\HI-TECH Software\PICC\9.80\sources\ftneg.c"
	line	16
	global	__size_of___ftneg
	__size_of___ftneg	equ	__end_of___ftneg-___ftneg
	
___ftneg:	
	opt	stack 5
; Regs used in ___ftneg: [wreg]
	line	17
	
l3141:	
	movf	(___ftneg@f1+2),w
	iorwf	(___ftneg@f1+1),w
	iorwf	(___ftneg@f1),w
	skipnz
	goto	u2461
	goto	u2460
u2461:
	goto	l3145
u2460:
	line	18
	
l3143:	
	movlw	080h
	xorwf	(___ftneg@f1+2),f
	goto	l3145
	
l1062:	
	line	19
	
l3145:	
	goto	l1063
	
l3147:	
	line	20
	
l1063:	
	return
	opt stack 0
GLOBAL	__end_of___ftneg
	__end_of___ftneg:
;; =============== function ___ftneg ends ============

	signat	___ftneg,4219
	global	___fttol
psect	text375,local,class=CODE,delta=2
global __ptext375
__ptext375:

;; *************** function ___fttol *****************
;; Defined at:
;;		line 45 in file "C:\Program Files (x86)\HI-TECH Software\PICC\9.80\sources\fttol.c"
;; Parameters:    Size  Location     Type
;;  f1              3    4[COMMON] float 
;; Auto vars:     Size  Location     Type
;;  lval            4    1[BANK0 ] unsigned long 
;;  exp1            1    5[BANK0 ] unsigned char 
;;  sign1           1    0[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;                  4    4[COMMON] long 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         4       0       0       0       0
;;      Locals:         0       6       0       0       0
;;      Temps:          4       0       0       0       0
;;      Totals:         8       6       0       0       0
;;Total ram usage:       14 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_KalmanFilter
;;		_Eric_main
;; This function uses a non-reentrant model
;;
psect	text375
	file	"C:\Program Files (x86)\HI-TECH Software\PICC\9.80\sources\fttol.c"
	line	45
	global	__size_of___fttol
	__size_of___fttol	equ	__end_of___fttol-___fttol
	
___fttol:	
	opt	stack 6
; Regs used in ___fttol: [wreg+status,2+status,0]
	line	49
	
l3077:	
	movf	(___fttol@f1),w
	movwf	((??___fttol+0)+0)
	movf	(___fttol@f1+1),w
	movwf	((??___fttol+0)+0+1)
	movf	(___fttol@f1+2),w
	movwf	((??___fttol+0)+0+2)
	clrc
	rlf	(??___fttol+0)+1,w
	rlf	(??___fttol+0)+2,w
	movwf	(??___fttol+3)+0
	movf	(??___fttol+3)+0,w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(___fttol@exp1)
	movf	((___fttol@exp1)),f
	skipz
	goto	u2341
	goto	u2340
u2341:
	goto	l3083
u2340:
	line	50
	
l3079:	
	movlw	0
	movwf	(?___fttol+3)
	movlw	0
	movwf	(?___fttol+2)
	movlw	0
	movwf	(?___fttol+1)
	movlw	0
	movwf	(?___fttol)

	goto	l1009
	
l3081:	
	goto	l1009
	
l1008:	
	line	51
	
l3083:	
	movf	(___fttol@f1),w
	movwf	((??___fttol+0)+0)
	movf	(___fttol@f1+1),w
	movwf	((??___fttol+0)+0+1)
	movf	(___fttol@f1+2),w
	movwf	((??___fttol+0)+0+2)
	movlw	017h
u2355:
	clrc
	rrf	(??___fttol+0)+2,f
	rrf	(??___fttol+0)+1,f
	rrf	(??___fttol+0)+0,f
u2350:
	addlw	-1
	skipz
	goto	u2355
	movf	0+(??___fttol+0)+0,w
	movwf	(??___fttol+3)+0
	movf	(??___fttol+3)+0,w
	movwf	(___fttol@sign1)
	line	52
	
l3085:	
	bsf	(___fttol@f1)+(15/8),(15)&7
	line	53
	
l3087:	
	movlw	0FFh
	andwf	(___fttol@f1),f
	movlw	0FFh
	andwf	(___fttol@f1+1),f
	movlw	0
	andwf	(___fttol@f1+2),f
	line	54
	
l3089:	
	movf	(___fttol@f1),w
	movwf	(___fttol@lval)
	movf	(___fttol@f1+1),w
	movwf	((___fttol@lval))+1
	movf	(___fttol@f1+2),w
	movwf	((___fttol@lval))+2
	clrf	((___fttol@lval))+3
	line	55
	
l3091:	
	movlw	low(08Eh)
	subwf	(___fttol@exp1),f
	line	56
	
l3093:	
	btfss	(___fttol@exp1),7
	goto	u2361
	goto	u2360
u2361:
	goto	l3103
u2360:
	line	57
	
l3095:	
	movf	(___fttol@exp1),w
	xorlw	80h
	addlw	-((-15)^80h)
	skipnc
	goto	u2371
	goto	u2370
u2371:
	goto	l3101
u2370:
	line	58
	
l3097:	
	movlw	0
	movwf	(?___fttol+3)
	movlw	0
	movwf	(?___fttol+2)
	movlw	0
	movwf	(?___fttol+1)
	movlw	0
	movwf	(?___fttol)

	goto	l1009
	
l3099:	
	goto	l1009
	
l1011:	
	goto	l3101
	line	59
	
l1012:	
	line	60
	
l3101:	
	movlw	01h
u2385:
	clrc
	rrf	(___fttol@lval+3),f
	rrf	(___fttol@lval+2),f
	rrf	(___fttol@lval+1),f
	rrf	(___fttol@lval),f
	addlw	-1
	skipz
	goto	u2385

	line	61
	movlw	(01h)
	movwf	(??___fttol+0)+0
	movf	(??___fttol+0)+0,w
	addwf	(___fttol@exp1),f
	btfss	status,2
	goto	u2391
	goto	u2390
u2391:
	goto	l3101
u2390:
	goto	l3113
	
l1013:	
	line	62
	goto	l3113
	
l1010:	
	line	63
	
l3103:	
	movlw	(018h)
	subwf	(___fttol@exp1),w
	skipc
	goto	u2401
	goto	u2400
u2401:
	goto	l3111
u2400:
	line	64
	
l3105:	
	movlw	0
	movwf	(?___fttol+3)
	movlw	0
	movwf	(?___fttol+2)
	movlw	0
	movwf	(?___fttol+1)
	movlw	0
	movwf	(?___fttol)

	goto	l1009
	
l3107:	
	goto	l1009
	
l1015:	
	line	65
	goto	l3111
	
l1017:	
	line	66
	
l3109:	
	movlw	01h
	movwf	(??___fttol+0)+0
u2415:
	clrc
	rlf	(___fttol@lval),f
	rlf	(___fttol@lval+1),f
	rlf	(___fttol@lval+2),f
	rlf	(___fttol@lval+3),f
	decfsz	(??___fttol+0)+0
	goto	u2415
	line	67
	movlw	low(01h)
	subwf	(___fttol@exp1),f
	goto	l3111
	line	68
	
l1016:	
	line	65
	
l3111:	
	movf	(___fttol@exp1),f
	skipz
	goto	u2421
	goto	u2420
u2421:
	goto	l3109
u2420:
	goto	l3113
	
l1018:	
	goto	l3113
	line	69
	
l1014:	
	line	70
	
l3113:	
	movf	(___fttol@sign1),w
	skipz
	goto	u2430
	goto	l3117
u2430:
	line	71
	
l3115:	
	comf	(___fttol@lval),f
	comf	(___fttol@lval+1),f
	comf	(___fttol@lval+2),f
	comf	(___fttol@lval+3),f
	incf	(___fttol@lval),f
	skipnz
	incf	(___fttol@lval+1),f
	skipnz
	incf	(___fttol@lval+2),f
	skipnz
	incf	(___fttol@lval+3),f
	goto	l3117
	
l1019:	
	line	72
	
l3117:	
	movf	(___fttol@lval+3),w
	movwf	(?___fttol+3)
	movf	(___fttol@lval+2),w
	movwf	(?___fttol+2)
	movf	(___fttol@lval+1),w
	movwf	(?___fttol+1)
	movf	(___fttol@lval),w
	movwf	(?___fttol)

	goto	l1009
	
l3119:	
	line	73
	
l1009:	
	return
	opt stack 0
GLOBAL	__end_of___fttol
	__end_of___fttol:
;; =============== function ___fttol ends ============

	signat	___fttol,4220
	global	___ftpack
psect	text376,local,class=CODE,delta=2
global __ptext376
__ptext376:

;; *************** function ___ftpack *****************
;; Defined at:
;;		line 63 in file "C:\Program Files (x86)\HI-TECH Software\PICC\9.80\sources\float.c"
;; Parameters:    Size  Location     Type
;;  arg             3    0[COMMON] unsigned um
;;  exp             1    3[COMMON] unsigned char 
;;  sign            1    4[COMMON] unsigned char 
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;                  3    0[COMMON] float 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         5       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          3       0       0       0       0
;;      Totals:         8       0       0       0       0
;;Total ram usage:        8 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		___ftadd
;;		___ftdiv
;;		___ftmul
;;		___lbtoft
;;		___lwtoft
;;		___altoft
;;		___abtoft
;;		___awtoft
;;		___lltoft
;;		___attoft
;;		___lttoft
;; This function uses a non-reentrant model
;;
psect	text376
	file	"C:\Program Files (x86)\HI-TECH Software\PICC\9.80\sources\float.c"
	line	63
	global	__size_of___ftpack
	__size_of___ftpack	equ	__end_of___ftpack-___ftpack
	
___ftpack:	
	opt	stack 5
; Regs used in ___ftpack: [wreg+status,2+status,0]
	line	64
	
l3041:	
	movf	(___ftpack@exp),w
	skipz
	goto	u2230
	goto	l3045
u2230:
	
l3043:	
	movf	(___ftpack@arg+2),w
	iorwf	(___ftpack@arg+1),w
	iorwf	(___ftpack@arg),w
	skipz
	goto	u2241
	goto	u2240
u2241:
	goto	l3051
u2240:
	goto	l3045
	
l1233:	
	line	65
	
l3045:	
	movlw	0x0
	movwf	(?___ftpack)
	movlw	0x0
	movwf	(?___ftpack+1)
	movlw	0x0
	movwf	(?___ftpack+2)
	goto	l1234
	
l3047:	
	goto	l1234
	
l1231:	
	line	66
	goto	l3051
	
l1236:	
	line	67
	
l3049:	
	movlw	(01h)
	movwf	(??___ftpack+0)+0
	movf	(??___ftpack+0)+0,w
	addwf	(___ftpack@exp),f
	line	68
	movlw	01h
u2255:
	clrc
	rrf	(___ftpack@arg+2),f
	rrf	(___ftpack@arg+1),f
	rrf	(___ftpack@arg),f
	addlw	-1
	skipz
	goto	u2255

	goto	l3051
	line	69
	
l1235:	
	line	66
	
l3051:	
	movlw	low highword(0FE0000h)
	andwf	(___ftpack@arg+2),w
	btfss	status,2
	goto	u2261
	goto	u2260
u2261:
	goto	l3049
u2260:
	goto	l1238
	
l1237:	
	line	70
	goto	l1238
	
l1239:	
	line	71
	
l3053:	
	movlw	(01h)
	movwf	(??___ftpack+0)+0
	movf	(??___ftpack+0)+0,w
	addwf	(___ftpack@exp),f
	line	72
	
l3055:	
	movlw	01h
	addwf	(___ftpack@arg),f
	movlw	0
	skipnc
movlw 1
	addwf	(___ftpack@arg+1),f
	movlw	0
	skipnc
movlw 1
	addwf	(___ftpack@arg+2),f
	line	73
	
l3057:	
	movlw	01h
u2275:
	clrc
	rrf	(___ftpack@arg+2),f
	rrf	(___ftpack@arg+1),f
	rrf	(___ftpack@arg),f
	addlw	-1
	skipz
	goto	u2275

	line	74
	
l1238:	
	line	70
	movlw	low highword(0FF0000h)
	andwf	(___ftpack@arg+2),w
	btfss	status,2
	goto	u2281
	goto	u2280
u2281:
	goto	l3053
u2280:
	goto	l3061
	
l1240:	
	line	75
	goto	l3061
	
l1242:	
	line	76
	
l3059:	
	movlw	low(01h)
	subwf	(___ftpack@exp),f
	line	77
	movlw	01h
u2295:
	clrc
	rlf	(___ftpack@arg),f
	rlf	(___ftpack@arg+1),f
	rlf	(___ftpack@arg+2),f
	addlw	-1
	skipz
	goto	u2295
	goto	l3061
	line	78
	
l1241:	
	line	75
	
l3061:	
	btfss	(___ftpack@arg+1),(15)&7
	goto	u2301
	goto	u2300
u2301:
	goto	l3059
u2300:
	
l1243:	
	line	79
	btfsc	(___ftpack@exp),(0)&7
	goto	u2311
	goto	u2310
u2311:
	goto	l1244
u2310:
	line	80
	
l3063:	
	movlw	0FFh
	andwf	(___ftpack@arg),f
	movlw	07Fh
	andwf	(___ftpack@arg+1),f
	movlw	0FFh
	andwf	(___ftpack@arg+2),f
	
l1244:	
	line	81
	clrc
	rrf	(___ftpack@exp),f

	line	82
	
l3065:	
	movf	(___ftpack@exp),w
	movwf	((??___ftpack+0)+0)
	clrf	((??___ftpack+0)+0+1)
	clrf	((??___ftpack+0)+0+2)
	movlw	010h
u2325:
	clrc
	rlf	(??___ftpack+0)+0,f
	rlf	(??___ftpack+0)+1,f
	rlf	(??___ftpack+0)+2,f
u2320:
	addlw	-1
	skipz
	goto	u2325
	movf	0+(??___ftpack+0)+0,w
	iorwf	(___ftpack@arg),f
	movf	1+(??___ftpack+0)+0,w
	iorwf	(___ftpack@arg+1),f
	movf	2+(??___ftpack+0)+0,w
	iorwf	(___ftpack@arg+2),f
	line	83
	
l3067:	
	movf	(___ftpack@sign),w
	skipz
	goto	u2330
	goto	l1245
u2330:
	line	84
	
l3069:	
	bsf	(___ftpack@arg)+(23/8),(23)&7
	
l1245:	
	line	85
	line	86
	
l1234:	
	return
	opt stack 0
GLOBAL	__end_of___ftpack
	__end_of___ftpack:
;; =============== function ___ftpack ends ============

	signat	___ftpack,12411
	global	_kalman_get_filtered_level
psect	text377,local,class=CODE,delta=2
global __ptext377
__ptext377:

;; *************** function _kalman_get_filtered_level *****************
;; Defined at:
;;		line 89 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Kalman filter\Src\Kalman.c"
;; Parameters:    Size  Location     Type
;;  kalman          1    wreg     PTR struct .
;;		 -> Eric_main@kalman(34), 
;; Auto vars:     Size  Location     Type
;;  kalman          1    3[COMMON] PTR struct .
;;		 -> Eric_main@kalman(34), 
;; Return value:  Size  Location     Type
;;                  3    0[COMMON] float 
;; Registers used:
;;		wreg, fsr0l, fsr0h
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         3       0       0       0       0
;;      Locals:         1       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         4       0       0       0       0
;;Total ram usage:        4 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_Eric_main
;; This function uses a non-reentrant model
;;
psect	text377
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Kalman filter\Src\Kalman.c"
	line	89
	global	__size_of_kalman_get_filtered_level
	__size_of_kalman_get_filtered_level	equ	__end_of_kalman_get_filtered_level-_kalman_get_filtered_level
	
_kalman_get_filtered_level:	
	opt	stack 6
; Regs used in _kalman_get_filtered_level: [wregfsr0]
;kalman_get_filtered_level@kalman stored from wreg
	movwf	(kalman_get_filtered_level@kalman)
	line	90
	
l3037:	
;Kalman.c: 90: return kalman->system_state.level;
	movf	(kalman_get_filtered_level@kalman),w
	addlw	09h
	movwf	fsr0
	bcf	status, 7	;select IRP bank0
	movf	indf,w
	movwf	(?_kalman_get_filtered_level)
	incf	fsr0,f
	movf	indf,w
	movwf	(?_kalman_get_filtered_level+1)
	incf	fsr0,f
	movf	indf,w
	movwf	(?_kalman_get_filtered_level+2)
	goto	l880
	
l3039:	
	line	91
	
l880:	
	return
	opt stack 0
GLOBAL	__end_of_kalman_get_filtered_level
	__end_of_kalman_get_filtered_level:
;; =============== function _kalman_get_filtered_level ends ============

	signat	_kalman_get_filtered_level,4219
	global	_kalman_init
psect	text378,local,class=CODE,delta=2
global __ptext378
__ptext378:

;; *************** function _kalman_init *****************
;; Defined at:
;;		line 57 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Kalman filter\Src\Kalman.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  kalman         34   34[BANK0 ] struct .
;; Return value:  Size  Location     Type
;;                  34    0[BANK0 ] struct .
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0      34       0       0       0
;;      Locals:         0      34       0       0       0
;;      Temps:          4       0       0       0       0
;;      Totals:         4      68       0       0       0
;;Total ram usage:       72 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_Eric_main
;; This function uses a non-reentrant model
;;
psect	text378
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Kalman filter\Src\Kalman.c"
	line	57
	global	__size_of_kalman_init
	__size_of_kalman_init	equ	__end_of_kalman_init-_kalman_init
	
_kalman_init:	
	opt	stack 6
; Regs used in _kalman_init: [wreg-fsr0h+status,2+status,0]
	line	59
	
l3029:	
;Kalman.c: 58: KALMAN_FILTER_T kalman;
;Kalman.c: 59: kalman.control.scalar = 0;
	movlw	0x0
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(kalman_init@kalman)
	movlw	0x0
	movwf	(kalman_init@kalman+1)
	movlw	0x0
	movwf	(kalman_init@kalman+2)
	line	60
;Kalman.c: 60: kalman.control.level = 0;
	movlw	0x0
	movwf	0+(kalman_init@kalman)+03h
	movlw	0x0
	movwf	1+(kalman_init@kalman)+03h
	movlw	0x0
	movwf	2+(kalman_init@kalman)+03h
	line	61
;Kalman.c: 61: kalman.system_state.scalar = 2;
	movlw	0x0
	movwf	0+(kalman_init@kalman)+06h
	movlw	0x0
	movwf	1+(kalman_init@kalman)+06h
	movlw	0x40
	movwf	2+(kalman_init@kalman)+06h
	line	62
;Kalman.c: 62: kalman.system_state.level = 0;
	movlw	0x0
	movwf	0+(kalman_init@kalman)+09h
	movlw	0x0
	movwf	1+(kalman_init@kalman)+09h
	movlw	0x0
	movwf	2+(kalman_init@kalman)+09h
	line	63
;Kalman.c: 63: kalman.measurement.scalar = 0;
	movlw	0x0
	movwf	0+(kalman_init@kalman)+0Ch
	movlw	0x0
	movwf	1+(kalman_init@kalman)+0Ch
	movlw	0x0
	movwf	2+(kalman_init@kalman)+0Ch
	line	64
;Kalman.c: 64: kalman.measurement.level = 0;
	movlw	0x0
	movwf	0+(kalman_init@kalman)+0Fh
	movlw	0x0
	movwf	1+(kalman_init@kalman)+0Fh
	movlw	0x0
	movwf	2+(kalman_init@kalman)+0Fh
	line	65
;Kalman.c: 65: kalman.covariance = 1;
	movlw	0x0
	movwf	0+(kalman_init@kalman)+018h
	movlw	0x80
	movwf	1+(kalman_init@kalman)+018h
	movlw	0x3f
	movwf	2+(kalman_init@kalman)+018h
	line	66
;Kalman.c: 66: kalman.gain = 1;
	movlw	0x0
	movwf	0+(kalman_init@kalman)+01Bh
	movlw	0x80
	movwf	1+(kalman_init@kalman)+01Bh
	movlw	0x3f
	movwf	2+(kalman_init@kalman)+01Bh
	line	67
;Kalman.c: 67: kalman.sensor_accuracy = 1;
	movlw	0x0
	movwf	0+(kalman_init@kalman)+01Eh
	movlw	0x80
	movwf	1+(kalman_init@kalman)+01Eh
	movlw	0x3f
	movwf	2+(kalman_init@kalman)+01Eh
	line	68
	
l3031:	
;Kalman.c: 68: kalman.initialized = 0;
	clrc
	movlw	0
	btfsc	status,0
	movlw	1
	movwf	0+(kalman_init@kalman)+021h
	line	70
	
l3033:	
;Kalman.c: 70: return kalman;
	movlw	(?_kalman_init)&0ffh
	movwf	fsr0
	movlw	low(kalman_init@kalman)
	movwf	(??_kalman_init+0)+0
	movf	fsr0,w
	movwf	((??_kalman_init+0)+0+1)
	movlw	34
	movwf	((??_kalman_init+0)+0+2)
u2220:
	movf	(??_kalman_init+0)+0,w
	movwf	fsr0
	bcf	status, 7	;select IRP bank0
	
	movf	indf,w
	movwf	((??_kalman_init+0)+0+3)
	incf	(??_kalman_init+0)+0,f
	movf	((??_kalman_init+0)+0+1),w
	movwf	fsr0
	
	movf	((??_kalman_init+0)+0+3),w
	movwf	indf
	incf	((??_kalman_init+0)+0+1),f
	decfsz	((??_kalman_init+0)+0+2),f
	goto	u2220
	goto	l874
	
l3035:	
	line	71
	
l874:	
	return
	opt stack 0
GLOBAL	__end_of_kalman_init
	__end_of_kalman_init:
;; =============== function _kalman_init ends ============

	signat	_kalman_init,95
psect	text379,local,class=CODE,delta=2
global __ptext379
__ptext379:
	global	btemp
	btemp set 07Eh

	DABS	1,126,2	;btemp
	global	wtemp0
	wtemp0 set btemp
	end
