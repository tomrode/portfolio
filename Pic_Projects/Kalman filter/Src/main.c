#include <string.h>
#include <stdio.h>
#include "htc.h"
#include "typedefs.h"
#include "Kalman.h"

/*PIC16F887*/
__CONFIG(DEBUGEN & LVPEN & FCMEN & IESODIS & BORXSLP & DUNPROTECT & UNPROTECT & MCLREN & PWRTEN & WDTDIS & INTCLK & CP);//HS code protected);
__CONFIG(BORV40);
/* STRUCTURES */

typedef enum
{
   INIT_MEASUREMENT = 0,
   NORMAL_MEASUREMENT= 1
}kalman_states_t;	
   
/* Function prototypes */ 
void main(void);                                  
float KalmanFilter(uint8_t Measurement, uint8_t KalStMch);

 /*********************************************************************/
/*        THE MAIN LOOP                                              */         
/*********************************************************************/

void main(void)
{
    float Filtered;
    int LoopCnt = 0; 
    /**- Initialise Filter  */ 
    Filtered = KalmanFilter(0, INIT_MEASUREMENT);
    asm("nop");
    
   /* Evaluate Eric Schnipke solution here */
   Eric_main();

    while(1)
    {  
         for(LoopCnt =0; LoopCnt<= 1000; LoopCnt++)
         {
             asm("nop");
             // Large transition //
             Filtered = KalmanFilter(20, NORMAL_MEASUREMENT);
             asm("nop");
          
             // introduce noise
             if ((LoopCnt >= 55) && (LoopCnt<= 65))
             { Filtered = KalmanFilter(49, NORMAL_MEASUREMENT); }
             
             asm("nop");
         }         
    }
}                                 

float KalmanFilter(uint8_t Measurement, uint8_t KalStMch)
{
	static float KalGain;
	static float CurrentEstErr;
    static float PreviousEstErr;
	static float CurrentEst;
    static float PreviousEst;
    
    
	// Initial values 	
	float InitEstErr = 1; //2;
    const float MeasErr = 4;
	float InitEst = 0;//68;

    // Gain scaler constant
    const uint8_t GainScaler = 2; //1.5;

	
	switch(KalStMch)
	{
	    case INIT_MEASUREMENT:
		    // Calculate initial Kalman gain
	        KalGain = ( InitEstErr / ( InitEstErr + MeasErr ) );
			
			// Calculate current estimate
			CurrentEst = ( InitEst +  ( KalGain * (Measurement - InitEst)));
			PreviousEst = CurrentEst;
 
			// Calculate new error in the estimate 
			CurrentEstErr = ( (1 - KalGain) * (InitEstErr) ); 
		    PreviousEstErr = CurrentEstErr;
			
	    break;

        case NORMAL_MEASUREMENT:
             // Calculate Kalman gain
	        KalGain = ( CurrentEstErr * GainScaler / ( CurrentEstErr + MeasErr ) );

            // Calculate current estimate
			CurrentEst = ( PreviousEst +  ( KalGain * (Measurement - PreviousEst )));
            PreviousEst = CurrentEst;

           	// Calculate new error in the estimate 
			CurrentEstErr = ( (1 - KalGain) * (PreviousEstErr) );
            PreviousEstErr = CurrentEstErr;  
            
        break; 
	
	    default:
		
		  break;
	}
    // This will be what the sensor output filtered 
	//return (CurrentEst);
      return (uint16_t)(CurrentEst);     		  
}