// Kalman.cpp : Defines the entry point for the console application.
//

//#include "stdafx.h"
#include <math.h>
#include <stdlib.h>
#include "typedefs.h"
#include "Kalman.h"

/*
 * Simple Kalman Filter
 *
 * The Kalman filter fuses multiple sources of information about a signal into
 * a weighted average, using the weighted average to identify and reduce signal
 * noise.  This "data fusion" provides a more accurate estimate of the true
 * signal value than any one contributing source might be able to provide.
 *
 * Multiple sources of information might include:
 * - control inputs to the signal
 * - measurements (from a sensor or network of sensors) of the signal
 * - predictions of the future behavior of the signal
*/

/*
typedef struct
{
	float scalar;
	float level;
} TRANSFORMATION_T;
*/
/*
typedef struct
{
	TRANSFORMATION_T control;
	TRANSFORMATION_T system_state;
	TRANSFORMATION_T measurement;
	struct
	{
		float optimal;
		float fixed;
	}gain_t;
	float covariance;
	float gain;
	float sensor_accuracy;
	uint8_t initialized;
} KALMAN_FILTER_T;
*/


//typedef enum bool_states
//{
//        char false = 0,
//        char true  = 1
//} BOOL_STATES_T;

static KALMAN_FILTER_T kalman_init(void)
{
	KALMAN_FILTER_T kalman;
	kalman.control.scalar      = 0;
	kalman.control.level       = 0;
	kalman.system_state.scalar = 2;//0;
	kalman.system_state.level  = 0;
	kalman.measurement.scalar  = 0;
	kalman.measurement.level   = 0;
	kalman.covariance          = 1;//0.1;
	kalman.gain                = 1;
	kalman.sensor_accuracy     = 1;
	kalman.initialized         = 0;//false;

	return kalman;
}

static void kalman_set( KALMAN_FILTER_T* kalman
                      , sint32_t z//float z
                      , float u
	                  , float b
                      , float a
                      , float c )
                      //, float g )
{
	kalman->control.scalar      = b;
	kalman->control.level       = u;
	kalman->system_state.scalar = a;
	kalman->measurement.scalar  = c;
	kalman->measurement.level   = z;
}

static float kalman_get_filtered_level(KALMAN_FILTER_T* kalman)
{
	return kalman->system_state.level;
}

static void kalman_filter( KALMAN_FILTER_T* kalman )
{
	/*****************/
	/* PREDICT STAGE */
	/*****************/
	/* Estimate signal level based on theoretical model of signal hysteresis and signal response to an input. */
	kalman->system_state.level = (kalman->system_state.scalar * kalman->system_state.level) + (kalman->control.scalar * kalman->control.level);

	/* Estimate covariance due to signal hysteresis. */
	kalman->covariance = kalman->covariance * kalman->system_state.scalar * kalman->system_state.scalar;

	/****************/
	/* UPDATE STAGE */
	/****************/
	/* Adjust gain based on measurement error (`measurement.scalar`, the
	mapping from true to measured signal level) and inherent sensor accuracy
	(`sensor_accuracy`, a property of the sensor). */
	kalman->gain = ( kalman->covariance * kalman->measurement.scalar)
		         / ((kalman->covariance * kalman->measurement.scalar * kalman->measurement.scalar) + kalman->sensor_accuracy);
	
	/* Adjust estimated signal level based on the last estimated signal level
	and the gain-adjusted error between the measured signal level and
	(previous) estimated signal level. */
	kalman->system_state.level = kalman->system_state.level + (kalman->gain * (kalman->measurement.level - (kalman->measurement.scalar * kalman->system_state.level)));
	
	/* Adjust covariance between the gain and estimated signal level. */
	kalman->covariance = (1 - (kalman->gain * kalman->measurement.scalar)) * kalman->covariance;

}



int Eric_main(void)
{
	KALMAN_FILTER_T kalman = kalman_init();
    int LoopCnt = 0;;
	float i = 0.0;
	float measurement = 0;
    sint32_t SystemStateLevel;

	while (1)
	{
		measurement =  -20;    //sin(i)*100 + (rand() % 10); 
        if ((LoopCnt >= 55) && (LoopCnt <= 65))    // introduce some noise
        {  	measurement =  49; }        
        asm("nop");
		kalman_set( &kalman
			      , measurement    // measured signal level
		          , 0    // control level to system
				  , 0    // control input scalar
		          , 1    // system state scalar for hysteresis
			      , 1 ); // mapping from measured signal level to true signal level
			      
		kalman_filter(&kalman);
        asm("nop");
		//printf("%f %f\n", measurement, kalman_get_filtered_level(&kalman));
        SystemStateLevel = kalman_get_filtered_level(&kalman);

		i += 0.01;
        LoopCnt++;
        asm("nop");
	}

    return 0;
}
