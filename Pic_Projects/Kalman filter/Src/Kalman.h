#ifndef KALMAN
#define KALMAN


/* Typedefs */

typedef struct
{
	float scalar;
	float level;
} TRANSFORMATION_T;

typedef struct
{
	TRANSFORMATION_T control;
	TRANSFORMATION_T system_state;
	TRANSFORMATION_T measurement;
	struct
	{
		float optimal;
		float fixed;
	}gain_t;
	float covariance;
	float gain;
	float sensor_accuracy;
	char initialized;
} KALMAN_FILTER_T;


/* FUNCTION PROTOTYPE's */
static void kalman_set( KALMAN_FILTER_T* kalman, /*float z*/sint32_t z, float u, float b, float a, float c);//, float g );
static float kalman_get_filtered_level(KALMAN_FILTER_T* kalman);
static void kalman_filter( KALMAN_FILTER_T* kalman );
int Eric_main(void);


#endif