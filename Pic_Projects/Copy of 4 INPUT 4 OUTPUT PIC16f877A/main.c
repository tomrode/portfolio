/***************************************************************/          
/* 29 NOV 2011 using PIC16F877A                                */ 
/* I want to exapand on UART capabilities for reading in and   */
/* sending out data from CANoe environment to real world.      */ 
/* Issue's I see is first setting up baud rate with 8 MHz res  */   
/* Then getting ADC working first one channel then multiple    */
/* channels (4 in total) (8 digital outputs).                  */
/* I will need some kind of protocol                           */
/* ANd have CANoe controll how many channels desired           */
/*         CHANGE HISTORY                                      */
/*           23 DEC 2011                                       */
/*   - Added in logic to choose one 0f three operating modes   */       
/*     Added in wait delay to prompt user to choose operating   */
/*     mode. Fixed watch dog time to max time 2.56 sec         */
/*     Added in progresson flags using Stuct for debug purposes*/ 
/*           27 DEC 2011                                       */
/*   - Fixed the operating mode indicator logic, added in      */
/*     return = 0 on else statements with in blink_led().      */
/***************************************************************/
#include <htc.h>
#include "typedefs.h"
#include "timer.h"
#include "adc_func.h"
#include "uart_func.h"
#define ONLY_TRIGGER      (0x0A)
#define ONLY_ADC          (0x0B)
#define BOTH_ADC_TRIGGER  (0X0C)
#define INFINITE_LED      (100)
#define INDICATOR_LED     (1000)
#define BLINKING_OFFSET   (0x0A) 
/**********************************************************/
/* Device Configuration
/**********************************************************/
     
      /* PIC16F877A*/
__CONFIG (WDTEN & PWRTDIS & BORDIS & LVPEN & WRTEN & 
          DEBUGDIS & DUNPROT & UNPROTECT & HS);  //HS means resonator       
      
       
 
/**********************************************************/
/* Function Prototypes 
/**********************************************************/

void init(void);                  /* SFR inits*/
void blink_infinite_led(void);    /* Initial led blink waiting */
int blink_led(int blinks);        /* Amount of blinks indicator*/
/***********************************************************/
/* Variable Definition
/***********************************************************/
//unsigned int watch_dog_count=0;


 
persistent int watch_dog_count;                /* will remain with a reset  */
int main(void)
{
/************************************************************/
/* LOCAL MAIN VARIABLES                                     */
/************************************************************/
watch_dog_count = 0;
uint8_t test_chnsel=0;                   
uint16_t adc_result;
uint8_t rcv_chn;
uint8_t while_condition;
uint8_t blinking_while_condition;
uint8_t exit=0;

struct 
    {
    uint8_t inits           : 1;
    uint8_t infinite_blink  : 1;
    uint8_t indicator_blink : 1;
    uint8_t padding         : 1;
    uint8_t while_con       : 4; 
    } 
    mainprogressflags;
/* test the address increments*/
/***********************************************************/
/* INITIALIZATIONS                                         */
/***********************************************************/ 
mainprogressflags.inits = FALSE;                            /************************************/
mainprogressflags.infinite_blink = FALSE;                   /*                                  */        
mainprogressflags.indicator_blink = FALSE;                  /* SET TEST POINT FLAGS ALL TO FALSE*/
mainprogressflags.padding = FALSE;                          /*                                  */
mainprogressflags.while_con = FALSE;                        /************************************/

init();                                                     /* Initialize SFR*/                                            
 
mainprogressflags.inits = TRUE;                             /* First test of where I am at*/ 

TMR1IE = 1;                                                 /* Timer 1 interrupt enable*/
ADIE = 1;                                                   /* AtD interrupt enable*/
//RCIE = 1;                                                 /* Enable UART INT*/ 
PEIE = 1;                                                   /* enable all peripheral interrupts*/
ei();                                                       /* enable all interrupts*/
adc_init();                                                 /* initialize the Atd module*/
uart_init();                                                /* UART SETUP */

/*************************************************************/
/*  WAITING FOR USER INPUT FROM CANOE                        */
/*************************************************************/
timer_set(TIMER_1,10);                                      /* initialize TIMER1*/
while((uart_rx() != ONLY_TRIGGER) && (uart_rx() != ONLY_ADC) && (uart_rx() != BOTH_ADC_TRIGGER )) /* 0x0A = only_triggers, 0x0B = only_ADC, 0x0C both ADC _triggers*/
{
  blink_infinite_led();   /* Blink the led for some time*/
  CLRWDT();                           /* clear if WD timed out*/

}
mainprogressflags.infinite_blink = TRUE;                     /* SECOND test of where I am at*/ 

while_condition = uart_rx();
blinking_while_condition = while_condition - BLINKING_OFFSET;   /* Set the blinking leds */
timer_set(TIMER_1,10);

/**************************************************************/
/*  WAITING TO SEND OUT LIGHT INDICATOR                       */
/**************************************************************/
while (exit == 0)
{
exit = blink_led(blinking_while_condition);                   /* Blink the led for some time*/
asm("nop");
CLRWDT();                           /* clear if WD timed out*/ 
}
mainprogressflags.indicator_blink = TRUE;                     /* THIRD test of where I am at*/ 
mainprogressflags.while_con = while_condition;                /* Fourth test of where I am at*/ 



/************************************************************/
/*  MAIN LOOP OVERALL                                       */
/************************************************************/
while(1)                   
{ 

while(while_condition == ONLY_TRIGGER)
 {
  if (TO == 1)                          /*STATUS reg, bit 4 1=After power up,CLRWDT or sleep instruction 0=A WDT time-out occured*/ 
   {
    rcv_chn = uart_rx();                /* Get from CAPL 1*/  
    led_rx(rcv_chn);                    /* which led on*/
    CLRWDT();                           /* clear if WD timed out*/
   }
   else
     {
       CLRWDT();                                 // Clear watchdog timer
     } 
  }

while(while_condition == ONLY_ADC)
 {
  if (TO == 1)                          /*STATUS reg, bit 4 1=After power up,CLRWDT or sleep instruction 0=A WDT time-out occured*/ 
   {
    rcv_chn = uart_rx();                /* Get from CAPL 1*/  
    uart_tx(rcv_chn);                   /* which adc channel or channels*/
    CLRWDT();                           /* clear if WD timed out*/
   }
   else
     {
       CLRWDT();                                 // Clear watchdog timer
     } 
  }


while(while_condition == BOTH_ADC_TRIGGER)
 {
  if (TO == 1)                          /*STATUS reg, bit 4 1=After power up,CLRWDT or sleep instruction 0=A WDT time-out occured*/ 
   {
    rcv_chn = uart_rx();                /* Get from CAPL 1*/  
    PORTD = rcv_chn;
    //uart_tx(rcv_chn);                   /* which adc channel or channels*/
    //led_rx(rcv_chn);                    /* which led on*/
    CLRWDT();                           /* clear if WD timed out*/
   }
   else
     {
       CLRWDT();                                 // Clear watchdog timer
     } 
  }
 } 
} 


 void init(void)
{
OPTION     = 0x8F;       /*|PSA=Prescale to WDT|most prescale rate|PS2=1|PS1=1|PS0=1| 20ms/Div*/
TRISA      = 0x0F;       /* Setup for analog input RA0,RA1,RA2,RA3  its was 0x01 for AN0 only*/
TRISB      = 0x00;
TRISD      = 0x00;       /* port directions: 1=input, 0=output*/
PORTB      = 0x00;
PORTD      = 0x00;       /* port D all off*/ 
//PIE1       = 0x01;       /* TMR1IE = enabled*/ 
//INTCON     = 0x40;       /* PEIE = enabled*/
T1CON      = 0x35;       /* T1CKPS1,T1CKPS0 = 1:8 prescaler,T1SYNC = do not synchronise external clock input ,TMR1ON = enabled*/ 
eeprom_write(0x10,0x00); /* Test the amount of Over runs this will rest the EEPROM each power reset */

}

void blink_infinite_led(void)
{
static int led_blink = 1;     /* Jump into case 1*/
switch(led_blink)
 {
  case 0:
  if (get_timer(TIMER_1) == 0)
   {
   PORTB = 0x00;  // OFF
   led_blink = 1; 
   timer_set(TIMER_1,INFINITE_LED);
   }
   else
    {
    }
  break;

  case 1:
   if (get_timer(TIMER_1) == 0)
   {
   PORTB = 0xFF;   // ON
   led_blink = 0;
   timer_set(TIMER_1,INFINITE_LED);
   }
   else
    {
    }
   default:
    break;
  
  break;
 }
}


int blink_led(int blinks)
{
static int led_blink =3;
static int count;

 switch(led_blink)
 {
  case 0:
  if (get_timer(TIMER_1) == 0)
  {
    PORTB = 0x00;  // OFF
    led_blink = 1;   /* GOTO CASE 1*/
    timer_set(TIMER_1,INDICATOR_LED); 
    return 0;
   }
   else
    {
    return 0;
    }
  
  break;

 case 1:
 if (get_timer(TIMER_1) == 0)
  {
  PORTB = 0xFF;   // ON
  led_blink = 2;    /* GOTO CASE 2*/
  count++;
  timer_set(TIMER_1,INDICATOR_LED);
  asm("nop"); 
  return 0;
  }
  else
  {
   return 0;
  }
  break;

  case 2:
  if(count <= blinks)
    {
     led_blink = 0;  /* restart the OF to On transition*/
     return 0;
    }
    else
    {
    return 1; /*get out of while loop*/
    } 
  break;

  case 3:
  
   PORTB = 0xFF;
   led_blink = 0; /*go to case 0*/
   timer_set(TIMER_1, INDICATOR_LED);
   asm("nop");
   return 0;
  break;

  default:
  break;
  
 }
}