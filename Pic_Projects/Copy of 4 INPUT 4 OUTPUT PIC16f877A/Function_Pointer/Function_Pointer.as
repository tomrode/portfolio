opt subtitle "HI-TECH Software Omniscient Code Generator (Lite mode) build 6738"

opt pagewidth 120

	opt lm

	processor	16F887
clrc	macro
	bcf	3,0
	endm
clrz	macro
	bcf	3,2
	endm
setc	macro
	bsf	3,0
	endm
setz	macro
	bsf	3,2
	endm
skipc	macro
	btfss	3,0
	endm
skipz	macro
	btfss	3,2
	endm
skipnc	macro
	btfsc	3,0
	endm
skipnz	macro
	btfsc	3,2
	endm
indf	equ	0
indf0	equ	0
pc	equ	2
pcl	equ	2
status	equ	3
fsr	equ	4
fsr0	equ	4
c	equ	1
z	equ	0
pclath	equ	10
	FNCALL	_main,_copy
	FNCALL	_main,_another_ptr_func
	FNCALL	_main,_cubed
	FNCALL	_main,_square
	FNCALL	_main,_decrement
	FNCALL	_main,_increment
	FNCALL	_another_ptr_func,_square
	FNCALL	_cubed,___wmul
	FNCALL	_square,___wmul
	FNROOT	_main
	global	_func_array
psect	strings,class=STRING,delta=2
global __pstrings
__pstrings:
;	global	stringdir,stringtab,__stringbase
stringtab:
;	String table - string pointers are 1 byte each
stringcode:stringdir:
movlw high(stringdir)
movwf pclath
movf fsr,w
incf fsr
	addwf pc
__stringbase:
	retlw	0
psect	strings
	file	"d:\users\f53368a\Desktop\Various Documents\pic_projects\Function_Pointer\main.c"
	line	86
_func_array:
	retlw	(fp__increment-fpbase)&0ffh
	retlw	(fp__decrement-fpbase)&0ffh
	retlw	(fp__square-fpbase)&0ffh
	retlw	(fp__cubed-fpbase)&0ffh
	global	_func_array
	file	"Function_Pointer.as"
	line	#
psect cinit,class=CODE,delta=2
global start_initialization
start_initialization:

psect cinit,class=CODE,delta=2
global end_of_initialization

;End of C runtime variable initialization code

end_of_initialization:
clrf status
ljmp _main	;jump to C main() function
psect	cstackCOMMON,class=COMMON,space=1
global __pcstackCOMMON
__pcstackCOMMON:
	global	?_copy
?_copy:	; 0 bytes @ 0x0
	global	??_copy
??_copy:	; 0 bytes @ 0x0
	global	?_main
?_main:	; 0 bytes @ 0x0
	global	?___wmul
?___wmul:	; 2 bytes @ 0x0
	global	___wmul@multiplier
___wmul@multiplier:	; 2 bytes @ 0x0
	ds	2
	global	___wmul@multiplicand
___wmul@multiplicand:	; 2 bytes @ 0x2
	ds	2
	global	??___wmul
??___wmul:	; 0 bytes @ 0x4
	global	___wmul@product
___wmul@product:	; 2 bytes @ 0x4
	ds	2
	global	?_increment
?_increment:	; 2 bytes @ 0x6
	global	?_decrement
?_decrement:	; 2 bytes @ 0x6
	global	?_square
?_square:	; 2 bytes @ 0x6
	global	?_cubed
?_cubed:	; 2 bytes @ 0x6
	global	increment@x
increment@x:	; 2 bytes @ 0x6
	global	decrement@x
decrement@x:	; 2 bytes @ 0x6
	global	square@x
square@x:	; 2 bytes @ 0x6
	global	cubed@x
cubed@x:	; 2 bytes @ 0x6
	ds	2
	global	??_increment
??_increment:	; 0 bytes @ 0x8
	global	??_decrement
??_decrement:	; 0 bytes @ 0x8
	global	??_square
??_square:	; 0 bytes @ 0x8
	global	??_cubed
??_cubed:	; 0 bytes @ 0x8
	global	increment@y
increment@y:	; 2 bytes @ 0x8
	global	decrement@y
decrement@y:	; 2 bytes @ 0x8
	global	square@y
square@y:	; 2 bytes @ 0x8
	global	cubed@y
cubed@y:	; 2 bytes @ 0x8
	ds	2
	global	?_another_ptr_func
?_another_ptr_func:	; 2 bytes @ 0xA
	global	another_ptr_func@b
another_ptr_func@b:	; 2 bytes @ 0xA
	ds	2
	global	another_ptr_func@func_ptr
another_ptr_func@func_ptr:	; 1 bytes @ 0xC
	ds	1
	global	??_another_ptr_func
??_another_ptr_func:	; 0 bytes @ 0xD
psect	cstackBANK0,class=BANK0,space=1
global __pcstackBANK0
__pcstackBANK0:
	global	another_ptr_func@result
another_ptr_func@result:	; 2 bytes @ 0x0
	global	copy@from_main_copy
copy@from_main_copy:	; 12 bytes @ 0x0
	ds	12
	global	copy@dtc_structure_ptr
copy@dtc_structure_ptr:	; 1 bytes @ 0xC
	ds	1
	global	copy@i
copy@i:	; 2 bytes @ 0xD
	ds	2
	global	??_main
??_main:	; 0 bytes @ 0xF
	ds	4
	global	main@test_dtc_copy
main@test_dtc_copy:	; 12 bytes @ 0x13
	ds	12
	global	main@d
main@d:	; 2 bytes @ 0x1F
	ds	2
	global	main@b
main@b:	; 2 bytes @ 0x21
	ds	2
	global	main@c
main@c:	; 2 bytes @ 0x23
	ds	2
	global	main@a
main@a:	; 2 bytes @ 0x25
	ds	2
	global	main@test_dtc
main@test_dtc:	; 12 bytes @ 0x27
	ds	12
;;Data sizes: Strings 0, constant 4, data 0, bss 0, persistent 0 stack 0
;;Auto spaces:   Size  Autos    Used
;; COMMON          14     13      13
;; BANK0           80     51      51
;; BANK1           80      0       0
;; BANK3           96      0       0
;; BANK2           96      0       0

;;
;; Pointer list with targets:

;; ?_cubed	int  size(1) Largest target is 0
;;
;; ?_square	int  size(1) Largest target is 0
;;
;; ?___wmul	unsigned int  size(1) Largest target is 0
;;
;; ?_decrement	int  size(1) Largest target is 0
;;
;; ?_increment	int  size(1) Largest target is 0
;;
;; ?_another_ptr_func	int  size(1) Largest target is 0
;;
;; another_ptr_func@func_ptr	PTR FTN(int ,)int  size(1) Largest target is 2
;;		 -> square(), 
;;
;; copy@dtc_structure_ptr	PTR int  size(1) Largest target is 12
;;		 -> main@test_dtc(BANK0[12]), 
;;
;; func_array	const PTR FTN(int ,)int [4] size(1) Largest target is 2
;;		 -> cubed(), square(), decrement(), increment(), 
;;


;;
;; Critical Paths under _main in COMMON
;;
;;   _main->_another_ptr_func
;;   _another_ptr_func->_square
;;   _cubed->___wmul
;;   _square->___wmul
;;
;; Critical Paths under _main in BANK0
;;
;;   _main->_copy
;;
;; Critical Paths under _main in BANK1
;;
;;   None.
;;
;; Critical Paths under _main in BANK3
;;
;;   None.
;;
;; Critical Paths under _main in BANK2
;;
;;   None.

;;
;;Main: autosize = 0, tempsize = 4, incstack = 0, save=0
;;

;;
;;Call Graph Tables:
;;
;; ---------------------------------------------------------------------------------
;; (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;; ---------------------------------------------------------------------------------
;; (0) _main                                                36    36      0    1056
;;                                             15 BANK0     36    36      0
;;                               _copy
;;                   _another_ptr_func
;;                              _cubed
;;                             _square
;;                          _decrement
;;                          _increment
;; ---------------------------------------------------------------------------------
;; (1) _another_ptr_func                                     5     2      3     226
;;                                             10 COMMON     3     0      3
;;                                              0 BANK0      2     2      0
;;                             _square
;; ---------------------------------------------------------------------------------
;; (1) _cubed                                                4     2      2     181
;;                                              6 COMMON     4     2      2
;;                             ___wmul
;; ---------------------------------------------------------------------------------
;; (1) _square                                               4     2      2     159
;;                                              6 COMMON     4     2      2
;;                             ___wmul
;; ---------------------------------------------------------------------------------
;; (2) ___wmul                                               6     2      4      92
;;                                              0 COMMON     6     2      4
;; ---------------------------------------------------------------------------------
;; (1) _decrement                                            4     2      2      45
;;                                              6 COMMON     4     2      2
;; ---------------------------------------------------------------------------------
;; (1) _increment                                            4     2      2      45
;;                                              6 COMMON     4     2      2
;; ---------------------------------------------------------------------------------
;; (1) _copy                                                18    18      0     114
;;                                              0 COMMON     3     3      0
;;                                              0 BANK0     15    15      0
;; ---------------------------------------------------------------------------------
;; Estimated maximum stack depth 2
;; ---------------------------------------------------------------------------------

;; Call Graph Graphs:

;; _main (ROOT)
;;   _copy
;;   _another_ptr_func
;;     _square
;;       ___wmul
;;   _cubed
;;     ___wmul
;;   _square
;;     ___wmul
;;   _decrement
;;   _increment
;;

;; Address spaces:

;;Name               Size   Autos  Total    Cost      Usage
;;BITCOMMON            E      0       0       0        0.0%
;;EEDATA             100      0       0       0        0.0%
;;NULL                 0      0       0       0        0.0%
;;CODE                 0      0       0       0        0.0%
;;COMMON               E      D       D       1       92.9%
;;BITSFR0              0      0       0       1        0.0%
;;SFR0                 0      0       0       1        0.0%
;;BITSFR1              0      0       0       2        0.0%
;;SFR1                 0      0       0       2        0.0%
;;STACK                0      0       3       2        0.0%
;;ABS                  0      0       0       3        0.0%
;;BITBANK0            50      0       0       4        0.0%
;;BITSFR3              0      0       0       4        0.0%
;;SFR3                 0      0       0       4        0.0%
;;BANK0               50     33      33       5       63.8%
;;BITSFR2              0      0       0       5        0.0%
;;SFR2                 0      0       0       5        0.0%
;;BITBANK1            50      0       0       6        0.0%
;;BANK1               50      0       0       7        0.0%
;;BITBANK3            60      0       0       8        0.0%
;;BANK3               60      0       0       9        0.0%
;;BITBANK2            60      0       0      10        0.0%
;;BANK2               60      0       0      11        0.0%
;;DATA                 0      0       0      12        0.0%

	global	_main
psect	maintext,global,class=CODE,delta=2
global __pmaintext
__pmaintext:

;; *************** function _main *****************
;; Defined at:
;;		line 121 in file "d:\users\f53368a\Desktop\Various Documents\pic_projects\Function_Pointer\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  test_dtc       12   39[BANK0 ] struct .
;;  test_dtc_cop   12   19[BANK0 ] struct .
;;  a               2   37[BANK0 ] int 
;;  c               2   35[BANK0 ] int 
;;  b               2   33[BANK0 ] int 
;;  d               2   31[BANK0 ] int 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, btemp+1, pclath, cstack
;; Tracked objects:
;;		On entry : 17F/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0      32       0       0       0
;;      Temps:          0       4       0       0       0
;;      Totals:         0      36       0       0       0
;;Total ram usage:       36 bytes
;; Hardware stack levels required when called:    3
;; This function calls:
;;		_copy
;;		_another_ptr_func
;;		_cubed
;;		_square
;;		_decrement
;;		_increment
;; This function is called by:
;;		Startup code after reset
;; This function uses a non-reentrant model
;;
psect	maintext
	file	"d:\users\f53368a\Desktop\Various Documents\pic_projects\Function_Pointer\main.c"
	line	121
	global	__size_of_main
	__size_of_main	equ	__end_of_main-_main
	
_main:	
	opt	stack 5
; Regs used in _main: [wreg-fsr0h+status,2+status,0+btemp+1+pclath+cstack]
	line	131
	
l1362:	
;main.c: 124: int a;
;main.c: 125: int b;
;main.c: 126: int c,d;
;main.c: 127: DTC_STRUC test_dtc;
;main.c: 128: DTC_STRUC test_dtc_copy;
;main.c: 131: test_dtc.dtc_ar1[0] = 0xBB;
	movlw	low(0BBh)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(main@test_dtc)
	movlw	high(0BBh)
	movwf	((main@test_dtc))+1
	line	132
;main.c: 132: test_dtc.dtc_ar1[1] = 0xAA;
	movlw	low(0AAh)
	movwf	0+(main@test_dtc)+02h
	movlw	high(0AAh)
	movwf	(0+(main@test_dtc)+02h)+1
	line	133
;main.c: 133: test_dtc.dtc_ar2[0] = 0xDD;
	movlw	low(0DDh)
	movwf	0+(main@test_dtc)+04h
	movlw	high(0DDh)
	movwf	(0+(main@test_dtc)+04h)+1
	line	134
;main.c: 134: test_dtc.dtc_ar2[1] = 0xBB;
	movlw	low(0BBh)
	movwf	0+(main@test_dtc)+06h
	movlw	high(0BBh)
	movwf	(0+(main@test_dtc)+06h)+1
	line	135
;main.c: 135: test_dtc.dtc_ar3[0] = 0xEE;
	movlw	low(0EEh)
	movwf	0+(main@test_dtc)+08h
	movlw	high(0EEh)
	movwf	(0+(main@test_dtc)+08h)+1
	line	136
;main.c: 136: test_dtc.dtc_ar3[1] = 0xFF;
	movlw	low(0FFh)
	movwf	0+(main@test_dtc)+0Ah
	movlw	high(0FFh)
	movwf	(0+(main@test_dtc)+0Ah)+1
	line	139
	
l1364:	
;main.c: 139: test_dtc_copy = test_dtc;
	movlw	(main@test_dtc_copy)&0ffh
	movwf	fsr0
	movlw	low(main@test_dtc)
	movwf	(??_main+0)+0
	movf	fsr0,w
	movwf	((??_main+0)+0+1)
	movlw	12
	movwf	((??_main+0)+0+2)
u2230:
	movf	(??_main+0)+0,w
	movwf	fsr0
	bcf	status, 7	;select IRP bank0
	
	movf	indf,w
	movwf	((??_main+0)+0+3)
	incf	(??_main+0)+0,f
	movf	((??_main+0)+0+1),w
	movwf	fsr0
	
	movf	((??_main+0)+0+3),w
	movwf	indf
	incf	((??_main+0)+0+1),f
	decfsz	((??_main+0)+0+2),f
	goto	u2230
	line	142
	
l1366:	
;main.c: 142: copy(&test_dtc.dtc_ar1[0]);
	movlw	(main@test_dtc)&0ffh
	fcall	_copy
	line	144
	
l1368:	
;main.c: 144: c = 3;
	movlw	low(03h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(main@c)
	movlw	high(03h)
	movwf	((main@c))+1
	line	146
	
l1370:	
;main.c: 146: d = another_ptr_func (c,&square);
	movf	(main@c+1),w
	clrf	(?_another_ptr_func+1)
	addwf	(?_another_ptr_func+1)
	movf	(main@c),w
	clrf	(?_another_ptr_func)
	addwf	(?_another_ptr_func)

	movlw	((fp__square-fpbase))&0ffh
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	0+(?_another_ptr_func)+02h
	fcall	_another_ptr_func
	movf	(1+(?_another_ptr_func)),w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(main@d+1)
	addwf	(main@d+1)
	movf	(0+(?_another_ptr_func)),w
	clrf	(main@d)
	addwf	(main@d)

	line	148
	
l1372:	
;main.c: 148: a = 5;
	movlw	low(05h)
	movwf	(main@a)
	movlw	high(05h)
	movwf	((main@a))+1
	line	149
	
l1374:	
;main.c: 149: b = (*func_array[0])(a);
	movf	(main@a+1),w
	clrf	(?_cubed+1)
	addwf	(?_cubed+1)
	movf	(main@a),w
	clrf	(?_cubed)
	addwf	(?_cubed)

	movlw	(_func_array-__stringbase)
	movwf	fsr0
	fcall	stringdir
	fcall	fptable
	movf	(1+(?_cubed)),w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(main@b+1)
	addwf	(main@b+1)
	movf	(0+(?_cubed)),w
	clrf	(main@b)
	addwf	(main@b)

	line	152
	
l1376:	
;main.c: 152: a = 66;
	movlw	low(042h)
	movwf	(main@a)
	movlw	high(042h)
	movwf	((main@a))+1
	line	153
	
l1378:	
;main.c: 153: b = (*func_array[1])(a);
	movf	(main@a+1),w
	clrf	(?_cubed+1)
	addwf	(?_cubed+1)
	movf	(main@a),w
	clrf	(?_cubed)
	addwf	(?_cubed)

	movlw	(_func_array-__stringbase)+01h
	movwf	fsr0
	fcall	stringdir
	fcall	fptable
	movf	(1+(?_cubed)),w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(main@b+1)
	addwf	(main@b+1)
	movf	(0+(?_cubed)),w
	clrf	(main@b)
	addwf	(main@b)

	line	156
	
l1380:	
;main.c: 156: a = 3;
	movlw	low(03h)
	movwf	(main@a)
	movlw	high(03h)
	movwf	((main@a))+1
	line	157
	
l1382:	
;main.c: 157: b = (*func_array[2])(a);
	movf	(main@a+1),w
	clrf	(?_cubed+1)
	addwf	(?_cubed+1)
	movf	(main@a),w
	clrf	(?_cubed)
	addwf	(?_cubed)

	movlw	(_func_array-__stringbase)+02h
	movwf	fsr0
	fcall	stringdir
	fcall	fptable
	movf	(1+(?_cubed)),w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(main@b+1)
	addwf	(main@b+1)
	movf	(0+(?_cubed)),w
	clrf	(main@b)
	addwf	(main@b)

	line	160
	
l1384:	
;main.c: 160: a = 3;
	movlw	low(03h)
	movwf	(main@a)
	movlw	high(03h)
	movwf	((main@a))+1
	line	161
	
l1386:	
;main.c: 161: b = (*func_array[3])(a);
	movf	(main@a+1),w
	clrf	(?_cubed+1)
	addwf	(?_cubed+1)
	movf	(main@a),w
	clrf	(?_cubed)
	addwf	(?_cubed)

	movlw	(_func_array-__stringbase)+03h
	movwf	fsr0
	fcall	stringdir
	fcall	fptable
	movf	(1+(?_cubed)),w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(main@b+1)
	addwf	(main@b+1)
	movf	(0+(?_cubed)),w
	clrf	(main@b)
	addwf	(main@b)

	line	163
	
l21:	
	global	start
	ljmp	start
	opt stack 0
GLOBAL	__end_of_main
	__end_of_main:
;; =============== function _main ends ============

	signat	_main,88
	global	_another_ptr_func
psect	text168,local,class=CODE,delta=2
global __ptext168
__ptext168:

;; *************** function _another_ptr_func *****************
;; Defined at:
;;		line 113 in file "d:\users\f53368a\Desktop\Various Documents\pic_projects\Function_Pointer\main.c"
;; Parameters:    Size  Location     Type
;;  b               2   10[COMMON] int 
;;  func_ptr        1   12[COMMON] PTR FTN(int ,)int 
;;		 -> square(2), 
;; Auto vars:     Size  Location     Type
;;  result          2    0[BANK0 ] int 
;; Return value:  Size  Location     Type
;;                  2   10[COMMON] int 
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         3       0       0       0       0
;;      Locals:         0       2       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         3       2       0       0       0
;;Total ram usage:        5 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		_square
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text168
	file	"d:\users\f53368a\Desktop\Various Documents\pic_projects\Function_Pointer\main.c"
	line	113
	global	__size_of_another_ptr_func
	__size_of_another_ptr_func	equ	__end_of_another_ptr_func-_another_ptr_func
	
_another_ptr_func:	
	opt	stack 5
; Regs used in _another_ptr_func: [wreg+status,2+status,0+pclath+cstack]
	line	115
	
l1356:	
;main.c: 114: int result;
;main.c: 115: result = func_ptr(b);
	movf	(another_ptr_func@b+1),w
	clrf	(?_square+1)
	addwf	(?_square+1)
	movf	(another_ptr_func@b),w
	clrf	(?_square)
	addwf	(?_square)

	movf	(another_ptr_func@func_ptr),w
	fcall	fptable
	movf	(1+(?_square)),w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(another_ptr_func@result+1)
	addwf	(another_ptr_func@result+1)
	movf	(0+(?_square)),w
	clrf	(another_ptr_func@result)
	addwf	(another_ptr_func@result)

	line	116
	
l1358:	
;main.c: 116: return result;
	movf	(another_ptr_func@result+1),w
	clrf	(?_another_ptr_func+1)
	addwf	(?_another_ptr_func+1)
	movf	(another_ptr_func@result),w
	clrf	(?_another_ptr_func)
	addwf	(?_another_ptr_func)

	goto	l18
	
l1360:	
	line	117
	
l18:	
	return
	opt stack 0
GLOBAL	__end_of_another_ptr_func
	__end_of_another_ptr_func:
;; =============== function _another_ptr_func ends ============

	signat	_another_ptr_func,8314
	global	_cubed
	global	_increment
	global	_decrement
	global	_square
psect	text169,local,class=CODE,delta=2
global __ptext169
__ptext169:

;; *************** function _square *****************
;; Defined at:
;;		line 187 in file "d:\users\f53368a\Desktop\Various Documents\pic_projects\Function_Pointer\main.c"
;; Parameters:    Size  Location     Type
;;  x               2    6[COMMON] int 
;; Auto vars:     Size  Location     Type
;;  y               2    8[COMMON] int 
;; Return value:  Size  Location     Type
;;                  2    6[COMMON] int 
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         2       0       0       0       0
;;      Locals:         2       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         4       0       0       0       0
;;Total ram usage:        4 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		___wmul
;; This function is called by:
;;		_main
;;		_another_ptr_func
;; This function uses a non-reentrant model
;;
psect	text169
	file	"d:\users\f53368a\Desktop\Various Documents\pic_projects\Function_Pointer\main.c"
	line	187
	global	__size_of_square
	__size_of_square	equ	__end_of_square-_square
	
_square:	
	opt	stack 6
; Regs used in _square: [wreg+status,2+status,0+pclath+cstack]
	line	190
	
l1344:	
;main.c: 188: int y;
;main.c: 190: y = x*x;
	movf	(square@x+1),w
	clrf	(?___wmul+1)
	addwf	(?___wmul+1)
	movf	(square@x),w
	clrf	(?___wmul)
	addwf	(?___wmul)

	movf	(square@x+1),w
	clrf	1+(?___wmul)+02h
	addwf	1+(?___wmul)+02h
	movf	(square@x),w
	clrf	0+(?___wmul)+02h
	addwf	0+(?___wmul)+02h

	fcall	___wmul
	movf	(1+(?___wmul)),w
	clrf	(square@y+1)
	addwf	(square@y+1)
	movf	(0+(?___wmul)),w
	clrf	(square@y)
	addwf	(square@y)

	line	192
	
l1346:	
;main.c: 192: return y;
	movf	(square@y+1),w
	clrf	(?_square+1)
	addwf	(?_square+1)
	movf	(square@y),w
	clrf	(?_square)
	addwf	(?_square)

	goto	l30
	
l1348:	
	line	193
	
l30:	
	return
	opt stack 0
GLOBAL	__end_of_square
	__end_of_square:
;; =============== function _square ends ============

	signat	_square,4218
psect	text170,local,class=CODE,delta=2
global __ptext170
__ptext170:

;; *************** function _decrement *****************
;; Defined at:
;;		line 178 in file "d:\users\f53368a\Desktop\Various Documents\pic_projects\Function_Pointer\main.c"
;; Parameters:    Size  Location     Type
;;  x               2    6[COMMON] int 
;; Auto vars:     Size  Location     Type
;;  y               2    8[COMMON] int 
;; Return value:  Size  Location     Type
;;                  2    6[COMMON] int 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         2       0       0       0       0
;;      Locals:         2       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         4       0       0       0       0
;;Total ram usage:        4 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text170
	file	"d:\users\f53368a\Desktop\Various Documents\pic_projects\Function_Pointer\main.c"
	line	178
	global	__size_of_decrement
	__size_of_decrement	equ	__end_of_decrement-_decrement
	
_decrement:	
	opt	stack 7
; Regs used in _decrement: [wreg+status,2+status,0]
	line	181
	
l1328:	
;main.c: 179: int y;
;main.c: 181: y = x - 1;
	movf	(decrement@x),w
	addlw	low(-1)
	movwf	(decrement@y)
	movf	(decrement@x+1),w
	skipnc
	addlw	1
	addlw	high(-1)
	movwf	1+(decrement@y)
	line	183
;main.c: 183: return y;
	movf	(decrement@y+1),w
	clrf	(?_decrement+1)
	addwf	(?_decrement+1)
	movf	(decrement@y),w
	clrf	(?_decrement)
	addwf	(?_decrement)

	goto	l27
	
l1330:	
	line	184
	
l27:	
	return
	opt stack 0
GLOBAL	__end_of_decrement
	__end_of_decrement:
;; =============== function _decrement ends ============

	signat	_decrement,4218
psect	text171,local,class=CODE,delta=2
global __ptext171
__ptext171:

;; *************** function _increment *****************
;; Defined at:
;;		line 169 in file "d:\users\f53368a\Desktop\Various Documents\pic_projects\Function_Pointer\main.c"
;; Parameters:    Size  Location     Type
;;  x               2    6[COMMON] int 
;; Auto vars:     Size  Location     Type
;;  y               2    8[COMMON] int 
;; Return value:  Size  Location     Type
;;                  2    6[COMMON] int 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         2       0       0       0       0
;;      Locals:         2       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         4       0       0       0       0
;;Total ram usage:        4 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text171
	file	"d:\users\f53368a\Desktop\Various Documents\pic_projects\Function_Pointer\main.c"
	line	169
	global	__size_of_increment
	__size_of_increment	equ	__end_of_increment-_increment
	
_increment:	
	opt	stack 7
; Regs used in _increment: [wreg+status,2+status,0]
	line	172
	
l1324:	
;main.c: 170: int y;
;main.c: 172: y = x + 1;
	movf	(increment@x),w
	addlw	low(01h)
	movwf	(increment@y)
	movf	(increment@x+1),w
	skipnc
	addlw	1
	addlw	high(01h)
	movwf	1+(increment@y)
	line	174
;main.c: 174: return y;
	movf	(increment@y+1),w
	clrf	(?_increment+1)
	addwf	(?_increment+1)
	movf	(increment@y),w
	clrf	(?_increment)
	addwf	(?_increment)

	goto	l24
	
l1326:	
	line	175
	
l24:	
	return
	opt stack 0
GLOBAL	__end_of_increment
	__end_of_increment:
;; =============== function _increment ends ============

	signat	_increment,4218
psect	text172,local,class=CODE,delta=2
global __ptext172
__ptext172:

;; *************** function _cubed *****************
;; Defined at:
;;		line 196 in file "d:\users\f53368a\Desktop\Various Documents\pic_projects\Function_Pointer\main.c"
;; Parameters:    Size  Location     Type
;;  x               2    6[COMMON] int 
;; Auto vars:     Size  Location     Type
;;  y               2    8[COMMON] int 
;; Return value:  Size  Location     Type
;;                  2    6[COMMON] int 
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         2       0       0       0       0
;;      Locals:         2       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         4       0       0       0       0
;;Total ram usage:        4 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		___wmul
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text172
	file	"d:\users\f53368a\Desktop\Various Documents\pic_projects\Function_Pointer\main.c"
	line	196
	global	__size_of_cubed
	__size_of_cubed	equ	__end_of_cubed-_cubed
	
_cubed:	
	opt	stack 6
; Regs used in _cubed: [wreg+status,2+status,0+pclath+cstack]
	line	199
	
l1350:	
;main.c: 197: int y;
;main.c: 199: y = x*x*x;
	movf	(cubed@x+1),w
	clrf	(?___wmul+1)
	addwf	(?___wmul+1)
	movf	(cubed@x),w
	clrf	(?___wmul)
	addwf	(?___wmul)

	movf	(cubed@x+1),w
	clrf	1+(?___wmul)+02h
	addwf	1+(?___wmul)+02h
	movf	(cubed@x),w
	clrf	0+(?___wmul)+02h
	addwf	0+(?___wmul)+02h

	fcall	___wmul
	movf	(1+(?___wmul)),w
	clrf	(?___wmul+1)
	addwf	(?___wmul+1)
	movf	(0+(?___wmul)),w
	clrf	(?___wmul)
	addwf	(?___wmul)

	movf	(cubed@x+1),w
	clrf	1+(?___wmul)+02h
	addwf	1+(?___wmul)+02h
	movf	(cubed@x),w
	clrf	0+(?___wmul)+02h
	addwf	0+(?___wmul)+02h

	fcall	___wmul
	movf	(1+(?___wmul)),w
	clrf	(cubed@y+1)
	addwf	(cubed@y+1)
	movf	(0+(?___wmul)),w
	clrf	(cubed@y)
	addwf	(cubed@y)

	line	201
	
l1352:	
;main.c: 201: return y;
	movf	(cubed@y+1),w
	clrf	(?_cubed+1)
	addwf	(?_cubed+1)
	movf	(cubed@y),w
	clrf	(?_cubed)
	addwf	(?_cubed)

	goto	l33
	
l1354:	
	line	202
	
l33:	
	return
	opt stack 0
GLOBAL	__end_of_cubed
	__end_of_cubed:
;; =============== function _cubed ends ============

	signat	_cubed,4218
	global	___wmul
psect	text173,local,class=CODE,delta=2
global __ptext173
__ptext173:

;; *************** function ___wmul *****************
;; Defined at:
;;		line 3 in file "C:\Program Files (x86)\HI-TECH Software\PICC\9.80\sources\wmul.c"
;; Parameters:    Size  Location     Type
;;  multiplier      2    0[COMMON] unsigned int 
;;  multiplicand    2    2[COMMON] unsigned int 
;; Auto vars:     Size  Location     Type
;;  product         2    4[COMMON] unsigned int 
;; Return value:  Size  Location     Type
;;                  2    0[COMMON] unsigned int 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         4       0       0       0       0
;;      Locals:         2       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         6       0       0       0       0
;;Total ram usage:        6 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_square
;;		_cubed
;; This function uses a non-reentrant model
;;
psect	text173
	file	"C:\Program Files (x86)\HI-TECH Software\PICC\9.80\sources\wmul.c"
	line	3
	global	__size_of___wmul
	__size_of___wmul	equ	__end_of___wmul-___wmul
	
___wmul:	
	opt	stack 6
; Regs used in ___wmul: [wreg+status,2+status,0]
	line	4
	
l1332:	
	movlw	low(0)
	movwf	(___wmul@product)
	movlw	high(0)
	movwf	((___wmul@product))+1
	goto	l1334
	line	6
	
l42:	
	line	7
	
l1334:	
	btfss	(___wmul@multiplier),(0)&7
	goto	u2191
	goto	u2190
u2191:
	goto	l43
u2190:
	line	8
	
l1336:	
	movf	(___wmul@multiplicand),w
	addwf	(___wmul@product),f
	skipnc
	incf	(___wmul@product+1),f
	movf	(___wmul@multiplicand+1),w
	addwf	(___wmul@product+1),f
	
l43:	
	line	9
	movlw	01h
	
u2205:
	clrc
	rlf	(___wmul@multiplicand),f
	rlf	(___wmul@multiplicand+1),f
	addlw	-1
	skipz
	goto	u2205
	line	10
	
l1338:	
	movlw	01h
	
u2215:
	clrc
	rrf	(___wmul@multiplier+1),f
	rrf	(___wmul@multiplier),f
	addlw	-1
	skipz
	goto	u2215
	line	11
	movf	((___wmul@multiplier+1)),w
	iorwf	((___wmul@multiplier)),w
	skipz
	goto	u2221
	goto	u2220
u2221:
	goto	l1334
u2220:
	goto	l1340
	
l44:	
	line	12
	
l1340:	
	movf	(___wmul@product+1),w
	clrf	(?___wmul+1)
	addwf	(?___wmul+1)
	movf	(___wmul@product),w
	clrf	(?___wmul)
	addwf	(?___wmul)

	goto	l45
	
l1342:	
	line	13
	
l45:	
	return
	opt stack 0
GLOBAL	__end_of___wmul
	__end_of___wmul:
;; =============== function ___wmul ends ============

	signat	___wmul,8314
	global	_copy
psect	text174,local,class=CODE,delta=2
global __ptext174
__ptext174:

;; *************** function _copy *****************
;; Defined at:
;;		line 99 in file "d:\users\f53368a\Desktop\Various Documents\pic_projects\Function_Pointer\main.c"
;; Parameters:    Size  Location     Type
;;  dtc_structur    1    wreg     PTR int 
;;		 -> main@test_dtc(12), 
;; Auto vars:     Size  Location     Type
;;  dtc_structur    1   12[BANK0 ] PTR int 
;;		 -> main@test_dtc(12), 
;;  from_main_co   12    0[BANK0 ] struct .
;;  i               2   13[BANK0 ] int 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, btemp+1
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0      15       0       0       0
;;      Temps:          3       0       0       0       0
;;      Totals:         3      15       0       0       0
;;Total ram usage:       18 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text174
	file	"d:\users\f53368a\Desktop\Various Documents\pic_projects\Function_Pointer\main.c"
	line	99
	global	__size_of_copy
	__size_of_copy	equ	__end_of_copy-_copy
	
_copy:	
	opt	stack 7
; Regs used in _copy: [wreg-fsr0h+status,2+status,0+btemp+1]
;copy@dtc_structure_ptr stored from wreg
	line	103
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(copy@dtc_structure_ptr)
	
l1310:	
;main.c: 101: DTC_STRUC from_main_copy;
;main.c: 102: int i;
;main.c: 103: for (i=0; i<=5; i++)
	movlw	low(0)
	movwf	(copy@i)
	movlw	high(0)
	movwf	((copy@i))+1
	
l1312:	
	movf	(copy@i+1),w
	xorlw	80h
	movwf	btemp+1
	movlw	(high(06h))^80h
	subwf	btemp+1,w
	skipz
	goto	u2175
	movlw	low(06h)
	subwf	(copy@i),w
u2175:

	skipc
	goto	u2171
	goto	u2170
u2171:
	goto	l1316
u2170:
	goto	l14
	
l1314:	
	goto	l14
	line	104
	
l13:	
	line	105
	
l1316:	
;main.c: 104: {
;main.c: 105: from_main_copy.dtc_ar1[i] = *dtc_structure_ptr;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(copy@dtc_structure_ptr),w
	movwf	fsr0
	bcf	status, 7	;select IRP bank0
	movf	indf,w
	movwf	(??_copy+0)+0+0
	incf	fsr0,f
	movf	indf,w
	movwf	(??_copy+0)+0+1
	movf	(copy@i),w
	movwf	(??_copy+2)+0
	addwf	(??_copy+2)+0,w
	addlw	copy@from_main_copy&0ffh
	movwf	fsr0
	movf	0+(??_copy+0)+0,w
	movwf	indf
	incf	fsr0,f
	movf	1+(??_copy+0)+0,w
	movwf	indf
	line	106
	
l1318:	
;main.c: 106: dtc_structure_ptr++;
	movlw	(02h)
	movwf	(??_copy+0)+0
	movf	(??_copy+0)+0,w
	addwf	(copy@dtc_structure_ptr),f
	line	103
	
l1320:	
	movlw	low(01h)
	addwf	(copy@i),f
	skipnc
	incf	(copy@i+1),f
	movlw	high(01h)
	addwf	(copy@i+1),f
	
l1322:	
	movf	(copy@i+1),w
	xorlw	80h
	movwf	btemp+1
	movlw	(high(06h))^80h
	subwf	btemp+1,w
	skipz
	goto	u2185
	movlw	low(06h)
	subwf	(copy@i),w
u2185:

	skipc
	goto	u2181
	goto	u2180
u2181:
	goto	l1316
u2180:
	
l14:	
	line	108
# 108 "d:\users\f53368a\Desktop\Various Documents\pic_projects\Function_Pointer\main.c"
nop ;#
psect	text174
	line	110
	
l15:	
	return
	opt stack 0
GLOBAL	__end_of_copy
	__end_of_copy:
;; =============== function _copy ends ============

	signat	_copy,4216
	global	fptotal
fptotal equ 4
	file ""
	line	0
psect	functab,class=CODE,delta=2,reloc=256
global __pfunctab
__pfunctab:
	global	fptable
fptable:
	movwf (btemp+1)&07Fh
	movlw high(fptable)
	movwf pclath
	movf (btemp+1)&07Fh,w
	addwf pc
	global	fpbase
fpbase:
	goto fpbase	; Call via a null pointer and you will get stuck here.
fp__increment:
	ljmp	_increment
	file ""
	line	0
psect	functab
fp__decrement:
	ljmp	_decrement
	file ""
	line	0
psect	functab
fp__square:
	ljmp	_square
	file ""
	line	0
psect	functab
fp__cubed:
	ljmp	_cubed
	global	btemp
	btemp set 07Eh

	DABS	1,126,2	;btemp
	global	wtemp0
	wtemp0 set btemp
	end
