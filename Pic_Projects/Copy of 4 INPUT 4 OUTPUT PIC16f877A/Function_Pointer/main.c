#include "stdio.h"
#include "stdlib.h"

#define FUNCTION_POINTER_ARRAY
//#define CALL_BACK
#ifdef FUNCTION_POINTER_NO_ARRAY
/********************************************************************/
/* Function prototypes                                              */
/********************************************************************/
float Plus (float a, float b);
float Minus (float a, float b);
float Multiply(float a, float b);
float Divide  (float a, float b);
void Switch_With_Function_Pointer(float a, float b, float (*pt2Func)(float, float ));
void main (void);



/********************************************************************/
/* Function definitions                                             */
/********************************************************************/


void main(void)
{
  float result1;

  result1 = Switch_With_Function_Pointer(7, 5, &Multiply);      /*pointer function to desired artihmetic operation*/

  result1 = Switch_With_Function_Pointer(12, 7, &Plus);      /*pointer function to desired artihmetic operation*/
}


float Plus (float a, float b) { return a+b; }
float Minus   (float a, float b) { return a-b; }
float Multiply(float a, float b) { return a*b; }
float Divide  (float a, float b) { return a/b; }
// Solution with a function pointer - <pt2Func> is a function pointer and points to
// a function which takes two floats and returns a float. The function pointer
// "specifies" which operation shall be executed.
void Switch_With_Function_Pointer(float a, float b, float (*pt2Func)(float, float))
{
  float result, var;
  result = pt2Func(a, b);  // call using function pointer
  var = 2;

}

#endif

#ifdef FUNCTION_POINTER_ARRAY


/********************************************************************/
/* Type definitions                                                 */
/********************************************************************/

typedef int(*T_FUNCPTR_1)(int);
typedef struct
           {
             int dtc_ar1[2];
             int dtc_ar2[2];
             int dtc_ar3[2];
           }DTC_STRUC;


/********************************************************************/
/* Function prototypes                                              */
/********************************************************************/
void copy(int *dtc_structure_ptr); 
int another_ptr_func (int b,int (*func_ptr)(int));
int increment(int x);
int decrement(int x);
int square(int x);
int cubed(int x);
void main(void);

//int (*func_ptr)(int y);


/********************************************************************/

/* Constant Definitions                                             */
/********************************************************************/

const T_FUNCPTR_1 func_array[] =
{
   increment,
   decrement,
   square,
   cubed
};

/********************************************************************/
/* Function definitions                                             */
/********************************************************************/

void copy(int *dtc_structure_ptr)
{
    /* Type definition*/
    DTC_STRUC from_main_copy;
    int i;  
    for (i=0; i<=5; i++)
      {
        from_main_copy.dtc_ar1[i] = *dtc_structure_ptr;
        dtc_structure_ptr++;
      }  
  asm("nop");

}

int another_ptr_func (int b,int (*func_ptr)(int))
{
  int result;
  result = func_ptr(b);
  return  result;
}

void main(void)

{
  /*local vars and types*/
 
  int a;
  int b;
  int c,d;
  DTC_STRUC test_dtc;
  DTC_STRUC test_dtc_copy;

  /* initialize the structure with data*/
  test_dtc.dtc_ar1[0] = 0xBB;
  test_dtc.dtc_ar1[1] = 0xAA;
  test_dtc.dtc_ar2[0] = 0xDD;
  test_dtc.dtc_ar2[1] = 0xBB;
  test_dtc.dtc_ar3[0] = 0xEE;
  test_dtc.dtc_ar3[1] = 0xFF;
  
 /* copy one structure to another*/
  test_dtc_copy = test_dtc;
 
  /* send this out to the function */
   copy(&test_dtc.dtc_ar1[0]); 
    
  c = 3;
 // d = (&square)(c);
  d = another_ptr_func (c,&square);
    /* JOHNS EXAMPLE BELOW*/
  a = 5;
  b = (*func_array[0])(a);
  //printf("%f + 1 = %f \n",a,b);

  a = 66;
  b = (*func_array[1])(a);
 // printf("%f - 1 = %f \n",a,b);

  a = 3;
  b = (*func_array[2])(a);
 // printf("%f ^ 2 = %f \n",a,b);

  a = 3;
  b = (*func_array[3])(a);
 // printf("1 / %f = %f \n",a,b);
}




int increment(int x)
{
  int y;

  y = x + 1;

  return y;
}

int decrement(int x)
{
  int y;

  y = x - 1;

  return y;
}

int square(int x)
{
  int y;

  y = x*x;

  return y;
}

int cubed(int x)
{
  int y;

  y = x*x*x;

  return y;
}
#endif

#ifdef CALL_BACK


/* Function prototypes */
void PrintTwoNumbers(int (*numberSourced)(void));
int overNineThousand(void);
int meaningOfLife(void);
void main(void);




/* Here we call PrintTwoNumbers() with three different callbacks. */
void main(void)

{

    printf( "PrintTwoNumbers(rand) is:");//,PrintTwoNumbers(rand));
    PrintTwoNumbers(rand);
    PrintTwoNumbers(overNineThousand);
    PrintTwoNumbers(meaningOfLife);

}

/* The calling function takes a single callback as a parameter. */
void PrintTwoNumbers(int (*numberSourced)(void))
{
    printf("%d and %d\n", numberSourced(), numberSourced());
}

/* A possible callback */
int overNineThousand(void)
{
    return (rand() % 1000) + 9001;
}


/* Another possible callback. */
int meaningOfLife(void)
{
    return 42;
}


#endif


