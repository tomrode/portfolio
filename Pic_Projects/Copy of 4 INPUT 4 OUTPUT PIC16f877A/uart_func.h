#include <htc.h>
#include "typedefs.h"

/*global variables*/

extern uint16_t eorr_count;

/* Function prototypes */

void uarttx_isr(void);
void uartrx_isr(void);
void uart_init(void);
void uart_tx(uint8_t chn_sel);
uint8_t uart_rx(void);
void led_rx(uint8_t led_on);