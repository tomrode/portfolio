
#include <htc.h>
#include "uart_func.h"
#include "typedefs.h"
#include "adc_func.h"

#define ADC_CHANNEL1   (0x00)
#define ADC_CHANNEL2   (0x10)
#define ADC_CHANNEL3   (0x20)
#define ADC_CHANNEL4   (0x30)
#define TRUE           (0x01)
#define FALSE          (0x00)

const uint8_t portd_on_val[] = 
 {
  0x01,               /*LED#1   index=0*/
  0x02,               /*LED#2   index=1*/
  0x04,               /*LED#3   index=2*/
  0x08,               /*LED#4   index=3*/
  0x10,               /*LED#5   index=4*/
  0x20,               /*LED#6   index=5*/
  0x40,               /*LED#7   index=6*/
  0x80                /*LED#8   index=7*/
  
 };         

const uint8_t portd_off_val[] = 
 {
  0xFE,              /*LED#1 off  index=0*/
  0xFD,              /*LED#2 off  index=1*/
  0xFB,              /*LED#3 off  index=2*/
  0xF7,              /*LED#4 off  index=3*/
  0xEF,              /*LED#5 off  index=4*/
  0xDF,              /*LED#6 off  index=5*/
  0xBF,              /*LED#7 off  index=6*/
  0x7F               /*LED#8 off  index=7*/

 };         
/* Global variables to this module*/
uint8_t uarttx_isr_flag;
uint8_t uartrx_isr_flag;

/*****************************************************************************************************/
/* function name: uart_tx()
/* DATE 1 DEC 2011
/* DESCRIPTION: This function when called will take in channel desired then out put raw value of ADC 
/*              plus channel out the uart      
/*
/* INPUTS-> None, But I see I will need to pass the ADC channel to read in
/* OUTPUTS-> None
/*                          TABLE OF ADC channel versus its assignment out the uart bit 
/*                                  AN0 = 0x10  Channel 1
/*                                  AN1 = 0x20  Channel 2
/*                                  AN2 = 0x30  Channel 3
/*                                  AN3 = 0x40  Channel 4
/*****************************************************************************************************/
void uart_tx(uint8_t chn_sel)
{
uint8_t delay_count;

uint16_t adc_lo;
uint16_t adc_hi;                               
uint8_t adc_hiaddrch;                        /* This variable will contain the adress of ADC channel*/
uint16_t rawadc_in;
/* TURN ON Which ADC CHANNEL  */
ADCON0 &= 0xC7;                               /* clear out ADCON0 bits CH2,CH1,CH0*/
ADCON0 |= (chn_sel<<3);                       /* then populate the channel select bits*/


asm("nop");
asm("nop");
asm("nop");
asm("nop");
asm("nop");

rawadc_in = adc_convert();                   /* Do the conversion*/

adc_lo = rawadc_in;
adc_lo = (uint8_t)(adc_lo);                   /*type cast to break up low byte*/

adc_hi = (rawadc_in & 0x0300);                /* strip out the low 8 bits*/ 
adc_hi = (adc_hi >> 8);                       /* shift it back to the right*/
adc_hi = (uint8_t)(adc_hi);                   /* type cast to break out high byte*/
/* Send out the hi byte with channel select multiplexed in*/
adc_hiaddrch = (adc_hi | (chn_sel << 4));    /* form the new adressed byte with adc channel*/ 

/*******************************SEND SOME ACTUALL BYTES OUT HERE**************************************/ 
TXREG = adc_hiaddrch;                         /* Send out hi byte with adc address */             
    di();                                     /* Nothing should interrupt putting this into a polling mode*/   
    while (TRMT = 0)                          /* TMRT = transmit buffer Reg Empty flag*/                   
      {
       /* Wait here until transmittion is complete */
      }
   
     _delay(3000);                            /* 3000 a bit under 1 ms */
TXREG = adc_lo;//0x55;                        /*Send out lo byte with adc address */              
    while (TRMT = 0)                          /* TMRT = transmit buffer Reg Empty flag*/                   
      {
       /* Wait here until transmittion is complete */
      }
     _delay(9000);
    ei();

}

/*********************************************************************************************/
/* function name: uart_rx()
/* Date 4 DEC 2011
/* DESCRIPTION: 
/*
/* INPUTS  -> NONE
/* OUTPUTS -> 8-bit received read (in this case from CAPL)
/* Update history:
/* - 21 DEC 2011: Fixed the lock up added in a test for overrun if condition then 
/*                re-initialize the UART.
/*********************************************************************************************/
uint8_t uart_rx(void)
{
uint8_t rcv_byte;
persistent uint16_t eorr_count;

rcv_byte = RCREG;        /* init a receive */ 
  
if (OERR ==1)
 {
  eorr_count++;
  eeprom_write(0x10,eorr_count); /* write the occurance into eeprom*/ 
  CREN = 0;                      /* Shut off Receive */
  uart_init();                   /* try re-initialise*/
  
 }
else
 {
  while ((RCIF = 1) && (OERR != 1))
  {
  /* Wait until flag clears */
  }
 } 

return rcv_byte;  
}

/*********************************************************************************************/
/* function name: led_rx()
/* Date 19 DEC 2011
/* DESCRIPTION: 
/*
/* INPUTS  -> 8 bit value upper nibble led number 1-8 lower nibble led on off state 
/* OUTPUTS -> Trigger led 1-8, led on/off is offset +1 i.e. for 0ff lower nibble = 1 
/*              On lower nibble = 2.
/* Update history
/* - 21 DEC 2011: Implemented logic for for shutting off any of the triggers. Could not use
/*                a logic Xor, Had to use another lookl up table  
/*********************************************************************************************/
void led_rx(uint8_t led_on)
{
static uint8_t rcv_byte;
uint8_t led_num;
uint8_t led_state;
uint8_t final_led_val;
uint8_t rcv_byte_new;
uint8_t index;
rcv_byte_new = led_on;                   /* most upto date value */

//if(rcv_byte_new == rcv_byte)              /* has it update ? no, then keep cuurent value*/
// {
    
  led_num = (rcv_byte_new & 0xF0);
  led_state = ((rcv_byte_new & 0x0F) - 1);
  final_led_val = (led_num | led_state);    /* this is now reformatted:  LED_number upper four nibbles state lower 4 nibbles */
  led_num = led_num >> 4;
  if (led_state == TRUE)                 /*0x01*/
   {
   index = led_num - 1;
   PORTD = portd_on_val[index];      /* get value from table and subtract 1*/
   }
   else if(led_state == FALSE)
    {
      index = led_num-1 ;
      PORTD &= portd_off_val[index];               /*  this is over kill but worked PORTD did not shut off all the way with xor*/ 
      asm("nop");
    }
     else
     {
     }
 //}
 //else
// {
// rcv_byte = rcv_byte_new; 
// } 

}

void uart_init(void)
{
//TRISC    = 0x80;       /* Pin 26 RC7/RX/DT  PIN 25 Rc6/TX/DT 
SPBRG      = 0x19;       /*19 made 52.2 us scope 19200 baud says 33 for 104 us Baud rate = FOSC/(16(X+1))  "51" at 8MHz at 9600 baud "25" at 8MHz at 19200 baud */ 
TXSTA      = 0x24;       /* |CSRC=0|TX9=0|TXEN=1|SYNC=0| -  |BRGH=1|TRMT=0|TX9D=0| */
RCSTA      = 0x90;       /* |SPEN=1(Serial port enabled)|RX9=0|SREN=0|CREN=1(RECEIVER ON)|ADDEN=0|FERR=0|OERR=0|RX9D=0|*/
}



void uartrx_isr(void)
{

  if ((RCIF==1)&&(RCIE==1))
  {
    //RCIF=0;             // cleared by hardware
    uartrx_isr_flag=1;  // set the indication 
  }
  else 
  {
  }
}

 


void uarttx_isr(void)
{
  if ((TXIF)&&(TXIE))
  {
    
    TXIF=0;             // clear flag
    uarttx_isr_flag=1;  // set the indication 
     
  }
  else 
  {
  }
}