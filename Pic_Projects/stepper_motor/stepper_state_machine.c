#include "typedefs.h"
#include "htc.h"
#include "timer.h"
#include "stepper_state_machine.h"
#define TIMER1_VALUE 500

/* Enumeration definitions */

enum
{
  E_STEP_FUNC_DEFAULT,
  E_STEP_FUNC_FORWARD,
  E_STEP_FUNC_REVERSE,
} E_STEP_FUNC;

/* Variable definitions */
uint8_t look_up_array [] = 
                      {
                      0x0A,  //index 0
                      0x09,  //index 1
                      0x05,  //index 2
                      0x06   //index 3
                      }; 
//uint8_t index;              
static uint8_t step_func_output;
static uint8_t step_func_temp;

/* Function definitions */

/***************************************************************************/
/*    full_step_state_machine();
/*     DESCRIPTION: Generate pulses on PORTD (RD0-RD3)in a specific order   
/*                  to step from one position to another for stepper motor.
/*                                TRUTH TABLE
/*                      _______________________________
/*                      |STEP| A1 | A2 | B1 |B2 | PORTD| 
/*                      ------------------------------- 
/*             START -> | 1  | 1  | 0  | 1  | 0 | 0x0A | |
/*                      -------------------------------  |
/*                      | 2  | 1  | 0  | 0  | 1 | 0x09 | |
/*                      -------------------------------  
/*                      | 3  | 0  | 1  | 0  | 1 | 0x05 |                           
/*                      -------------------------------
/*                      | 4  | 0  | 1  | 1  | 0 | 0x06 |
/*                      -------------------------------
/*              END ->  | 1  | 1  | 0  | 1  | 0 | 0x0A |
/*                      -------------------------------
/*                   
/*     INPUTS: NONE
/*     OUPUTS: NONE  
/***************************************************************************/

void full_step_state_machine(void)
{
  static uint8_t step_func_state = E_STEP_FUNC_DEFAULT;
  static uint8_t index; 
  switch (step_func_state)
  {
     case E_STEP_FUNC_FORWARD:
        if (get_timer(TIMER_1) == 0) 
       {  
         step_func_temp = look_up_array[index];                 // Get the array to element zer
         step_func_output = step_func_temp;
         timer_set(TIMER_1, TIMER1_VALUE);
         index++;
         step_func_state = E_STEP_FUNC_FORWARD;
         if (RE0 == 1)                                          // Check the switch 
          {
         step_func_state = E_STEP_FUNC_REVERSE;
          }
         if (index > 3)                                        // restart the whole process  
         {
          index = 0;                                           // reset the sequence       
          step_func_temp = look_up_array[index];
         }
         else
         {
           /* Do not shift the LED output. */
         }
       }
       else
       {
         /* Do nothing except remain the same state. */
       }

       break;

     case E_STEP_FUNC_REVERSE:
       if (get_timer(TIMER_1) == 0)
       { 
         step_func_temp = look_up_array[index];                 // Get the array to element 
         step_func_output = step_func_temp;
         timer_set(TIMER_1, TIMER1_VALUE);
         step_func_state = E_STEP_FUNC_REVERSE;
         if (RE0 == 0)                                         // check the switch
          {
          step_func_state = E_STEP_FUNC_FORWARD;
          }  
         if (index > 0)                                        // This will prevent rolling back to 0xFF           
          {
          index--;
          }  
         else if (index == 0)                                  // restart the whole process  THIS else if fixed the missing state 
          {
           index = 3;                                          // But from the top of the array
          }
        else
         {
           /* Do not shift the LED output. */
         }
       }
       else
       {
         /* Do nothing except remain the same state. */
       }

       break;

     default:
       index = 0;
       step_func_output = 0xFF;                           // output everything on PORTD initially
       timer_set(TIMER_1, TIMER1_VALUE);                  // Set the timer
      if (RE0 == 0)
       {      
       step_func_state = E_STEP_FUNC_FORWARD;             // Set for the state machione to go forward off the bat.
       }
       else
       {
       step_func_state = E_STEP_FUNC_REVERSE;             // Set for the state machione to go reverse off the bat
       }
       break;
  }

  /* Actions independent of state */
  PORTD = step_func_output;
}

     

