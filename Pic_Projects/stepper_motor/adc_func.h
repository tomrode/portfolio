#include "typedefs.h"


/* Variables*/
 struct {
         unsigned lo  : 1;
         unsigned mid : 6;
         unsigned hi  : 1;
         }teststruct = {0,0,0};  


/*Function Prototypes*/

uint16_t adc_convert(void);
uint8_t adc_toms_graph(uint16_t value);
uint8_t adc_bargraph(uint16_t value);
uint8_t adc_bargraph2(uint16_t value);
uint16_t linear_motor(void);
uint16_t adc_volt_convert(void);
void adc_isr(void);
void adc_init(void);