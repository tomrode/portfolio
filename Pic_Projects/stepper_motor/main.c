#include "htc.h"
#include "typedefs.h"
#include "timer.h"
#include "stepper_state_machine.h"
#include "adc_func.h"
/*PIC16F887*/
__CONFIG(DEBUGEN & LVPEN & FCMEN & IESODIS & BORXSLP & DUNPROTECT & UNPROTECT & MCLREN & PWRTEN & WDTDIS & INTCLK);//HS);
__CONFIG(BORV40);
/* STRUCTURES */
/* typedef struct
        {
        uint8_t c1;
        uint8_t c2;
        uint8_t c3;
        uint8_t c4;
        uint8_t c5;
        uint8_t c6;
        uint8_t c7;
        uint8_t c8;
        uint8_t c9;
        }COMPLEX;
*/ 
/* Function prototypes*/
void sfreg(void);                                   // Initializr  the hard ware

/* non writeable ram variable location*/
persistent int persistent_watch_dog_count;

/* Volatile variable*/
vuint8_t tmr1_counter;      // volatile isr variable for timer 1

      
 /*********************************************************************/
/*        THE MAIN LOOP                                              */         
/*********************************************************************/
int main (void)
{
 
  /* variables for the main*/
  
uint8_t  watchdog;
uint16_t adc_result;
 // COMPLEX vector;
  
/***********************************************************************/
/*          INITIALIZATIONS
/***********************************************************************/
  sfreg();                                            // initialize sfr's   
  TMR1IE = 1;                                         // Timer 1 interrupt enable
  ADIE = 1;                                           // AtD intrrupt enable
  PEIE = 1;                                           // enable all peripheral interrupts
  ei();                                               // enable all interrupts
  adc_init();                                         // initialize the Atd module
  //half_step_state_machine();
  full_step_state_machine();                          // set the state machine to default initially
/*************************************************************************/
/*           CODE WILL REMAIN IN while loop if no watch dog reset
/*************************************************************************/ 
 while (HTS == 0)
  {}                                                 // wait until clock stable 

  while(1)
  {
    if(TO == 1)                                       // No watchdog reset proceed with adc read      
    {
     //half_step_state_machine();  
     full_step_state_machine();
     watchdog = TO;
     CLRWDT();
     }
    else                                            // a watchdog reset happend update this count
    { 
      watchdog = TO;
      PORTD = persistent_watch_dog_count++;         // Keep a un-accessed RAM count of wdt 
      CLRWDT();
    }
  }
}

void sfreg(void)
{
WDTCON = 0x17;       // WDTPS3=1|WDTPS2=0|WDTPS1=1|WDTPS0=1|SWDTEN=1 5 seconds before watch dog expires
OSCCON = 0x61;       // 4 Mhz clock
OPTION = 0x00;       // |PSA=Prescale to WDT|most prescale rate|
TRISA =  0x02;       // MAKE RA1 an input for A/D potentiometer
//TRISA  = 0x01;       // Make RA0 an input for A/D the potentiometer
TRISE  = 0x01;       // MAKE PORTE RE1 an input Pushbutton
TRISD  = 0x00;       // Port D as outputs
PORTD  = 0x00;       // test the port comes on for some reason
T1CON  = 0x35;       // T1CKPS1,T1CKPS0 = 1:8 prescaler,T1SYNC = do not synchronise external clock input ,TMR1ON = enabled
//ANSEL  &=0x00;     // Make a digital I/O
//ANSEL  = 0x01;     // Select channel ANS0
ANSEL = 0x02;        // Select channel ANS1
}





