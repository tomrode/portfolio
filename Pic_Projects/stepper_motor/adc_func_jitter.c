
#include "typedefs.h"
#include "htc.h"
#include "adc_func.h"
#include "timer.h"

#define ADC_MAX  (0x3FF)
#define ADC_BITS (10)
/* Macro tested in main*/

#define ADC_FULL_SCALE_MV         (5000)
#define ADC_BITS                  (10)
#define ADC_MULTIPLY(x,y)         ((uint32_t)(x)*(uint32_t)(y))
#define adc_volt_convert_macro(x) ((uint16_t)(ADC_MULTIPLY(x,ADC_FULL_SCALE_MV) >> ADC_BITS))


//#define  adc_volt_convert_macro(x) (((x)*(.00488))*(1000)) // conversion macro raw adc count to volts in mV
/* variables*/
uint16_t int_count=0;
uint16_t adc_isr_flag;
const uint8_t adc_bargraph_array[] = 
{
  0x00,
  0x01,
  0x03,
  0x03,
  0x07,
  0x07,
  0x0F,
  0x0F,
  0x1F,
  0x1F,
  0x3F,
  0x3F,
  0x7F,
  0x7F,
  0xFF,
  0xFF
};

const uint8_t adc_bargraph_array2[] =
{
  0x00,
  0x01,
  0x02,
  0x04,
  0x08,
  0x10,	
  0x20,
  0x40,
  0x80,
};

const uint16_t motor_speed_array[] =  
{                           // FROM EXCELL
  0x000A,                   // index 0  speed 125 RPM
  0x000B,                   // index 1  speed 115 RPM  
  0x000C,                   // index 2  speed 105 RPM
  0x000D,                   // index 3  speed 95  RPM
  0x000F,                   // index 4  speed 85  RPM
  0x0011,                   // index 5  speed 75  RPM
  0x0013,                   // index 6  speed 65  RPM                  
  0x0017,                   // index 7  speed 55  RPM
  0x001C,                   // index 8  speed 45  RPM
  0x0024,                   // index 9  speed 35  RPM
  0x0032,                   // index 10 speed 25  RPM
  0x0053,                   // index 11 speed 15  RPM
  0x00FA,                   // index 12 speed 05  RPM
  0x01A1,                   // index 13 speed 03  RPM
  0x0271,                   // index 14 speed 02  RPM  
  0x1388                    // index 15 VERY slow
};
/*function prototypes*/


void adc_isr(void);

uint16_t adc_volt_convert(void)
{
/* Variables*/
uint16_t adc_rawvalue;
uint16_t adc_millivolt_macroval;
uint16_t adc_millivolt=0;

adc_rawvalue = adc_convert();                                  // Get Adc value
PORTC = 0x40;
adc_millivolt_macroval = adc_volt_convert_macro(adc_rawvalue); // invoke the macro 
PORTC = 0x00;
adc_millivolt = adc_millivolt_macroval;

asm("nop");
return adc_millivolt;                                          // send out this to any call that wants it 

}


uint16_t adc_convert(void)
{
  uint16_t adresh;                   //return value from the atd conversion 
  uint16_t adresl;                   //return value from the atd conversion
  uint16_t result; 
  adc_isr_flag = 0;                  //*CRITICAL* INTERRUPT FLAG MUST BE SET BEFORE CONVERTING
 
  GODONE = 1;                        //*CRITICAL* INTERRUPT FLAG MUST BE SET BEFORE CONVERTING
  
  while (adc_isr_flag == 0)           // Did an interrupt happened to a conversion
  {}
  adresl = ADRESL;                    // 
  adresh = ADRESH;                    //
  result = ((adresh << 8) |(adresl)); // get into word format

  return result;
}

uint8_t adc_toms_graph(uint16_t value)
{ 
  uint8_t result;

  if(value <= 0x71 )                 //0-113
  {
     result=0x00;
  }

  if((value >= 0x72)&&(value <= 0xE3))//114-227
  {
    result=0x01;
  }

  if((value >= 0xE4)&&(value <=0x0155))//228-341
  {
    result=0x02;
  }

  if((value >= 0x0156) && (value <= 0x01C7))//342-455
  {
    result=0x04;
  }

  if ((value >= 0x01C8) && (value <= 0x0239))//456-569
  {
    result=0x08;
  } 

  if ((value >= 0x0240) && (value <= 0x02AB))//570-683
  {
    result=0x10;
  }

  if ((value >= 0x02AC) && (value <= 0x31D))//684-797
  {
   result=0x20;
  }

  if ((value >= 0x031E) && (value <= 0x038F))//798-911
  { 
   result=0x40;
  }

  if ((value >= 0x0390) && (value <= 0x03FF))//912-1024
  {
   result=0x80;
  }
  else
  {
  }

  return result;
}

uint8_t adc_bargraph(uint16_t value)
{
  uint8_t result;

  if (value > ADC_MAX)
  {
    value = ADC_MAX;
  }

  result = adc_bargraph_array[value >> (ADC_BITS - 4)];

  return result;
}
//////////////////////////////////////////////////////////////////////////////////////
/*    linear_motor()
/*    Description: This function will return a table value given raw adc count
/*                 table value derived from index which linearizes potentiometer sweep    
/*                 Implemented a jitter test filter for varing adc counts. It will not pass 
/*                 indexed value if count greater than 5.    
/*    INPUTS:     None
/*    OUTPUTS:    value from array for time.
/*
/////////////////////////////////////////////////////////////////////////////////////*/    
uint16_t linear_motor(void)
{
  uint16_t raw_adc;                      // the raw value from adc conversion
  uint8_t index;                         // used for motoer speed array
  uint16_t old_adc_readin;               // compare first raw adc read 
  static uint16_t current_adc_readin;    // to most current adc read
  uint8_t adc_changed_flag;              // a flag for condition

  raw_adc = adc_convert();

  old_adc_readin = current_adc_readin;
  //current_adc_readin = adc_convert();                    // test the raw adc read
  if (current_adc_readin == 0x03BF)
  {
    current_adc_readin = 0x03C0;
  }
  else
  {
    current_adc_readin = 0x03BF;
  }
  
  if(current_adc_readin != old_adc_readin)                 // this is for filtering jitter of 5 ADC counts 
  {  
    adc_changed_flag++;
  }
  else
  {
    adc_changed_flag = 0;
  }

  if(adc_changed_flag > 5)
  {  
    index = (raw_adc >> 6);                                // less than 5 counts therfor pass indexed value
  }
  else
  {
    /* Do nothing but pass the same index */  
  }
  return motor_speed_array[index];
}


uint8_t adc_bargraph2(uint16_t value)
{
  uint8_t result;

  if (value > ADC_MAX)
  {
    value = ADC_MAX;
  }

  result = adc_bargraph_array2[value/114];

  return result;
}

void adc_init(void)
{
  ADCON0 = 0xC5;    //Setting FRC internal A/D clock,  analog channel = RA1 ,ADON = selected
  ADCON1 = 0x80;    //Right justify
}

void adc_isr(void)
{
  if ((ADIF)&&(ADIE))
  {
    
    ADIF=0;          // clear flag
    adc_isr_flag=1;  // set the indication 
    int_count++; 
  }
  else 
  {
  }
}

