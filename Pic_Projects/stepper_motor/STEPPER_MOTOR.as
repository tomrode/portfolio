opt subtitle "HI-TECH Software Omniscient Code Generator (Lite mode) build 6738"

opt pagewidth 120

	opt lm

	processor	16F887
clrc	macro
	bcf	3,0
	endm
clrz	macro
	bcf	3,2
	endm
setc	macro
	bsf	3,0
	endm
setz	macro
	bsf	3,2
	endm
skipc	macro
	btfss	3,0
	endm
skipz	macro
	btfss	3,2
	endm
skipnc	macro
	btfsc	3,0
	endm
skipnz	macro
	btfsc	3,2
	endm
indf	equ	0
indf0	equ	0
pc	equ	2
pcl	equ	2
status	equ	3
fsr	equ	4
fsr0	equ	4
c	equ	1
z	equ	0
pclath	equ	10
# 7 "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\stepper_motor\main.c"
	psect config,class=CONFIG,delta=2 ;#
# 7 "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\stepper_motor\main.c"
	dw 0x1FFF & 0x3FFF & 0x3FFF & 0x3BFF & 0x3EFF & 0x3FFF & 0x3FFF & 0x3FFF & 0x3FEF & 0x3FF7 & 0x3FFD ;#
# 8 "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\stepper_motor\main.c"
	psect config,class=CONFIG,delta=2 ;#
# 8 "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\stepper_motor\main.c"
	dw 0x3FFF ;#
	FNCALL	_main,_sfreg
	FNCALL	_main,_adc_init
	FNCALL	_main,_full_step_state_machine
	FNCALL	_full_step_state_machine,_linear_motor
	FNCALL	_full_step_state_machine,_get_timer
	FNCALL	_full_step_state_machine,_timer_set
	FNCALL	_full_step_state_machine,_eep_tally
	FNCALL	_eep_tally,_get_timer
	FNCALL	_eep_tally,_eeprom_write
	FNCALL	_eep_tally,_timer_set
	FNCALL	_eep_tally,_eeprom_read
	FNCALL	_linear_motor,_adc_convert
	FNROOT	_main
	FNCALL	_interrupt_handler,_timer_isr
	FNCALL	_interrupt_handler,_adc_isr
	FNCALL	intlevel1,_interrupt_handler
	global	intlevel1
	FNROOT	intlevel1
	global	_motor_speed_array
psect	strings,class=STRING,delta=2
global __pstrings
__pstrings:
;	global	stringdir,stringtab,__stringbase
stringtab:
;	String table - string pointers are 1 byte each
stringcode:stringdir:
movlw high(stringdir)
movwf pclath
movf fsr,w
incf fsr
	addwf pc
__stringbase:
	retlw	0
psect	strings
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\stepper_motor\adc_func.c"
	line	54
_motor_speed_array:
	retlw	0Ah
	retlw	0

	retlw	0Bh
	retlw	0

	retlw	0Ch
	retlw	0

	retlw	0Dh
	retlw	0

	retlw	0Fh
	retlw	0

	retlw	011h
	retlw	0

	retlw	013h
	retlw	0

	retlw	017h
	retlw	0

	retlw	01Ch
	retlw	0

	retlw	024h
	retlw	0

	retlw	032h
	retlw	0

	retlw	053h
	retlw	0

	retlw	0FAh
	retlw	0

	retlw	0A1h
	retlw	01h

	retlw	071h
	retlw	02h

	retlw	088h
	retlw	013h

	global	_adc_bargraph_array
psect	strings
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\stepper_motor\adc_func.c"
	line	21
_adc_bargraph_array:
	retlw	0
	retlw	01h
	retlw	03h
	retlw	03h
	retlw	07h
	retlw	07h
	retlw	0Fh
	retlw	0Fh
	retlw	01Fh
	retlw	01Fh
	retlw	03Fh
	retlw	03Fh
	retlw	07Fh
	retlw	07Fh
	retlw	0FFh
	retlw	0FFh
	global	_adc_bargraph_array2
psect	strings
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\stepper_motor\adc_func.c"
	line	41
_adc_bargraph_array2:
	retlw	0
	retlw	01h
	retlw	02h
	retlw	04h
	retlw	08h
	retlw	010h
	retlw	020h
	retlw	040h
	retlw	080h
	global	_halfstep_look_up_array
psect	strings
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\stepper_motor\stepper_state_machine_half_full_time_potchange.c"
	line	34
_halfstep_look_up_array:
	retlw	01h
	retlw	03h
	retlw	02h
	retlw	06h
	retlw	04h
	retlw	0Ch
	retlw	08h
	retlw	09h
	global	_look_up_array
psect	strings
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\stepper_motor\stepper_state_machine_half_full_time_potchange.c"
	line	27
_look_up_array:
	retlw	03h
	retlw	06h
	retlw	0Ch
	retlw	09h
	global	_motor_speed_array
	global	_adc_bargraph_array
	global	_adc_bargraph_array2
	global	_halfstep_look_up_array
	global	_look_up_array
	global	_timer_array
	global	eep_tally@result_read32_t
	global	_adc_isr_flag
	global	_average_result
	global	_int_count
	global	eep_tally@ee_addr
	global	full_step_state_machine@adc_result_state_machine
	global	half_step_state_machine@adc_result_state_machine
	global	half_step_state_machine@old_pot_val
	global	linear_motor@adc_match_count
	global	linear_motor@old_raw_adc
	global	linear_motor@raw_adc
	global	_E_HALFSTEP_FUNC
	global	_E_STEP_FUNC
	global	_average_speed_count_state_machi
	global	_step_func_output
	global	_step_func_temp
	global	_tmr1_counter
	global	_tmr1_isr_counter
	global	full_step_state_machine@index
	global	full_step_state_machine@step_func_state
	global	half_step_state_machine@current_pot_val
	global	_teststruct
	global	_toms_variable
	global	half_step_state_machine@index
	global	half_step_state_machine@step_func_state
	global	_persistent_watch_dog_count
psect	nvBANK0,class=BANK0,space=1
global __pnvBANK0
__pnvBANK0:
_persistent_watch_dog_count:
       ds      2

	global	full_step_state_machine@current_pot_val
full_step_state_machine@current_pot_val:
       ds      2

	global	full_step_state_machine@old_pot_val
full_step_state_machine@old_pot_val:
       ds      2

	global	_ADCON0
_ADCON0	set	31
	global	_ADRESH
_ADRESH	set	30
	global	_PORTC
_PORTC	set	7
	global	_PORTD
_PORTD	set	8
	global	_T1CON
_T1CON	set	16
	global	_TMR1H
_TMR1H	set	15
	global	_TMR1L
_TMR1L	set	14
	global	_ADIF
_ADIF	set	102
	global	_CARRY
_CARRY	set	24
	global	_GIE
_GIE	set	95
	global	_GODONE
_GODONE	set	249
	global	_PEIE
_PEIE	set	94
	global	_RE0
_RE0	set	72
	global	_TMR1IF
_TMR1IF	set	96
	global	_TO
_TO	set	28
	global	_ADCON1
_ADCON1	set	159
	global	_ADRESL
_ADRESL	set	158
	global	_OPTION
_OPTION	set	129
	global	_OSCCON
_OSCCON	set	143
	global	_TRISA
_TRISA	set	133
	global	_TRISD
_TRISD	set	136
	global	_TRISE
_TRISE	set	137
	global	_ADIE
_ADIE	set	1126
	global	_HTS
_HTS	set	1146
	global	_TMR1IE
_TMR1IE	set	1120
	global	_EEADR
_EEADR	set	269
	global	_EEDATA
_EEDATA	set	268
	global	_WDTCON
_WDTCON	set	261
	global	_ANSEL
_ANSEL	set	392
	global	_EECON1
_EECON1	set	396
	global	_EECON2
_EECON2	set	397
	global	_RD
_RD	set	3168
	global	_WR
_WR	set	3169
	global	_WREN
_WREN	set	3170
	file	"STEPPER_MOTOR.as"
	line	#
psect cinit,class=CODE,delta=2
global start_initialization
start_initialization:

psect	bssCOMMON,class=COMMON,space=1
global __pbssCOMMON
__pbssCOMMON:
half_step_state_machine@current_pot_val:
       ds      2

_teststruct:
       ds      1

_toms_variable:
       ds      1

half_step_state_machine@index:
       ds      1

half_step_state_machine@step_func_state:
       ds      1

psect	bssBANK0,class=BANK0,space=1
global __pbssBANK0
__pbssBANK0:
_timer_array:
       ds      6

eep_tally@result_read32_t:
       ds      4

_adc_isr_flag:
       ds      2

_average_result:
       ds      2

_int_count:
       ds      2

eep_tally@ee_addr:
       ds      2

full_step_state_machine@adc_result_state_machine:
       ds      2

half_step_state_machine@adc_result_state_machine:
       ds      2

half_step_state_machine@old_pot_val:
       ds      2

linear_motor@adc_match_count:
       ds      2

linear_motor@old_raw_adc:
       ds      2

linear_motor@raw_adc:
       ds      2

_E_HALFSTEP_FUNC:
       ds      1

_E_STEP_FUNC:
       ds      1

_average_speed_count_state_machi:
       ds      1

_step_func_output:
       ds      1

_step_func_temp:
       ds      1

_tmr1_counter:
       ds      1

_tmr1_isr_counter:
       ds      1

full_step_state_machine@index:
       ds      1

full_step_state_machine@step_func_state:
       ds      1

psect clrtext,class=CODE,delta=2
global clear_ram
;	Called with FSR containing the base address, and
;	W with the last address+1
clear_ram:
	clrwdt			;clear the watchdog before getting into this loop
clrloop:
	clrf	indf		;clear RAM location pointed to by FSR
	incf	fsr,f		;increment pointer
	xorwf	fsr,w		;XOR with final address
	btfsc	status,2	;have we reached the end yet?
	retlw	0		;all done for this memory range, return
	xorwf	fsr,w		;XOR again to restore value
	goto	clrloop		;do the next byte

; Clear objects allocated to COMMON
psect cinit,class=CODE,delta=2
	clrf	((__pbssCOMMON)+0)&07Fh
	clrf	((__pbssCOMMON)+1)&07Fh
	clrf	((__pbssCOMMON)+2)&07Fh
	clrf	((__pbssCOMMON)+3)&07Fh
	clrf	((__pbssCOMMON)+4)&07Fh
	clrf	((__pbssCOMMON)+5)&07Fh
; Clear objects allocated to BANK0
psect cinit,class=CODE,delta=2
	bcf	status, 7	;select IRP bank0
	movlw	low(__pbssBANK0)
	movwf	fsr
	movlw	low((__pbssBANK0)+027h)
	fcall	clear_ram
psect cinit,class=CODE,delta=2
global end_of_initialization

;End of C runtime variable initialization code

end_of_initialization:
clrf status
ljmp _main	;jump to C main() function
psect	cstackCOMMON,class=COMMON,space=1
global __pcstackCOMMON
__pcstackCOMMON:
	global	?_timer_isr
?_timer_isr:	; 0 bytes @ 0x0
	global	??_timer_isr
??_timer_isr:	; 0 bytes @ 0x0
	global	?_adc_isr
?_adc_isr:	; 0 bytes @ 0x0
	global	??_adc_isr
??_adc_isr:	; 0 bytes @ 0x0
	global	?_sfreg
?_sfreg:	; 0 bytes @ 0x0
	global	?_adc_init
?_adc_init:	; 0 bytes @ 0x0
	global	?_full_step_state_machine
?_full_step_state_machine:	; 0 bytes @ 0x0
	global	?_interrupt_handler
?_interrupt_handler:	; 0 bytes @ 0x0
	global	?_eeprom_read
?_eeprom_read:	; 1 bytes @ 0x0
	global	?_main
?_main:	; 2 bytes @ 0x0
	ds	1
	global	timer_isr@i
timer_isr@i:	; 1 bytes @ 0x1
	ds	1
	global	??_interrupt_handler
??_interrupt_handler:	; 0 bytes @ 0x2
	ds	4
psect	cstackBANK0,class=BANK0,space=1
global __pcstackBANK0
__pcstackBANK0:
	global	??_sfreg
??_sfreg:	; 0 bytes @ 0x0
	global	??_adc_init
??_adc_init:	; 0 bytes @ 0x0
	global	?_eeprom_write
?_eeprom_write:	; 0 bytes @ 0x0
	global	??_eeprom_read
??_eeprom_read:	; 0 bytes @ 0x0
	global	?_timer_set
?_timer_set:	; 0 bytes @ 0x0
	global	?_adc_convert
?_adc_convert:	; 2 bytes @ 0x0
	global	?_get_timer
?_get_timer:	; 2 bytes @ 0x0
	global	eeprom_write@value
eeprom_write@value:	; 1 bytes @ 0x0
	global	timer_set@value
timer_set@value:	; 2 bytes @ 0x0
	ds	1
	global	??_eeprom_write
??_eeprom_write:	; 0 bytes @ 0x1
	ds	1
	global	??_adc_convert
??_adc_convert:	; 0 bytes @ 0x2
	global	??_timer_set
??_timer_set:	; 0 bytes @ 0x2
	global	??_get_timer
??_get_timer:	; 0 bytes @ 0x2
	global	eeprom_write@addr
eeprom_write@addr:	; 1 bytes @ 0x2
	ds	1
	global	timer_set@index
timer_set@index:	; 1 bytes @ 0x3
	global	eeprom_read@addr
eeprom_read@addr:	; 1 bytes @ 0x3
	global	get_timer@result
get_timer@result:	; 2 bytes @ 0x3
	ds	1
	global	adc_convert@adresh
adc_convert@adresh:	; 2 bytes @ 0x4
	ds	1
	global	get_timer@index
get_timer@index:	; 1 bytes @ 0x5
	ds	1
	global	?_eep_tally
?_eep_tally:	; 0 bytes @ 0x6
	global	adc_convert@adresl
adc_convert@adresl:	; 2 bytes @ 0x6
	global	eep_tally@step_funcout
eep_tally@step_funcout:	; 2 bytes @ 0x6
	ds	2
	global	??_eep_tally
??_eep_tally:	; 0 bytes @ 0x8
	global	adc_convert@result
adc_convert@result:	; 2 bytes @ 0x8
	ds	2
	global	?_linear_motor
?_linear_motor:	; 2 bytes @ 0xA
	ds	2
	global	??_linear_motor
??_linear_motor:	; 0 bytes @ 0xC
	ds	3
	global	linear_motor@index
linear_motor@index:	; 1 bytes @ 0xF
	ds	1
	global	eep_tally@result_read16_t
eep_tally@result_read16_t:	; 2 bytes @ 0x10
	ds	2
	global	eep_tally@average_speed_count_state_machi
eep_tally@average_speed_count_state_machi:	; 1 bytes @ 0x12
	ds	1
	global	eep_tally@lo_read
eep_tally@lo_read:	; 1 bytes @ 0x13
	ds	1
	global	eep_tally@hi_read
eep_tally@hi_read:	; 1 bytes @ 0x14
	ds	1
	global	eep_tally@lo_result
eep_tally@lo_result:	; 1 bytes @ 0x15
	ds	1
	global	eep_tally@hi_result
eep_tally@hi_result:	; 1 bytes @ 0x16
	ds	1
	global	??_full_step_state_machine
??_full_step_state_machine:	; 0 bytes @ 0x17
	ds	1
	global	full_step_state_machine@pot_changed_flag
full_step_state_machine@pot_changed_flag:	; 1 bytes @ 0x18
	ds	1
	global	??_main
??_main:	; 0 bytes @ 0x19
	ds	1
	global	main@watchdog
main@watchdog:	; 1 bytes @ 0x1A
	ds	1
;;Data sizes: Strings 0, constant 69, data 0, bss 45, persistent 6 stack 0
;;Auto spaces:   Size  Autos    Used
;; COMMON          14      6      12
;; BANK0           80     27      72
;; BANK1           80      0       0
;; BANK3           96      0       0
;; BANK2           96      0       0

;;
;; Pointer list with targets:

;; ?_linear_motor	unsigned short  size(1) Largest target is 0
;;
;; ?_adc_convert	unsigned short  size(1) Largest target is 0
;;
;; ?_get_timer	unsigned short  size(1) Largest target is 0
;;


;;
;; Critical Paths under _main in COMMON
;;
;;   None.
;;
;; Critical Paths under _interrupt_handler in COMMON
;;
;;   _interrupt_handler->_timer_isr
;;
;; Critical Paths under _main in BANK0
;;
;;   _main->_full_step_state_machine
;;   _full_step_state_machine->_eep_tally
;;   _eep_tally->_get_timer
;;   _linear_motor->_adc_convert
;;
;; Critical Paths under _interrupt_handler in BANK0
;;
;;   None.
;;
;; Critical Paths under _main in BANK1
;;
;;   None.
;;
;; Critical Paths under _interrupt_handler in BANK1
;;
;;   None.
;;
;; Critical Paths under _main in BANK3
;;
;;   None.
;;
;; Critical Paths under _interrupt_handler in BANK3
;;
;;   None.
;;
;; Critical Paths under _main in BANK2
;;
;;   None.
;;
;; Critical Paths under _interrupt_handler in BANK2
;;
;;   None.

;;
;;Main: autosize = 0, tempsize = 1, incstack = 0, save=0
;;

;;
;;Call Graph Tables:
;;
;; ---------------------------------------------------------------------------------
;; (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;; ---------------------------------------------------------------------------------
;; (0) _main                                                 4     4      0    1083
;;                                             25 BANK0      2     2      0
;;                              _sfreg
;;                           _adc_init
;;            _full_step_state_machine
;; ---------------------------------------------------------------------------------
;; (1) _full_step_state_machine                              2     2      0    1077
;;                                             23 BANK0      2     2      0
;;                       _linear_motor
;;                          _get_timer
;;                          _timer_set
;;                          _eep_tally
;; ---------------------------------------------------------------------------------
;; (2) _eep_tally                                           18    16      2     681
;;                                              6 BANK0     17    15      2
;;                          _get_timer
;;                       _eeprom_write
;;                          _timer_set
;;                        _eeprom_read
;; ---------------------------------------------------------------------------------
;; (2) _linear_motor                                         6     4      2     136
;;                                             10 BANK0      6     4      2
;;                        _adc_convert
;; ---------------------------------------------------------------------------------
;; (3) _eeprom_read                                          4     4      0      31
;;                                              0 BANK0      4     4      0
;; ---------------------------------------------------------------------------------
;; (3) _eeprom_write                                         3     2      1      62
;;                                              0 BANK0      3     2      1
;; ---------------------------------------------------------------------------------
;; (3) _adc_convert                                         10     8      2     102
;;                                              0 BANK0     10     8      2
;; ---------------------------------------------------------------------------------
;; (3) _get_timer                                            6     4      2      99
;;                                              0 BANK0      6     4      2
;; ---------------------------------------------------------------------------------
;; (3) _timer_set                                            6     4      2      93
;;                                              0 BANK0      4     2      2
;; ---------------------------------------------------------------------------------
;; (1) _adc_init                                             0     0      0       0
;; ---------------------------------------------------------------------------------
;; (1) _sfreg                                                0     0      0       0
;; ---------------------------------------------------------------------------------
;; Estimated maximum stack depth 3
;; ---------------------------------------------------------------------------------
;; (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;; ---------------------------------------------------------------------------------
;; (4) _interrupt_handler                                    4     4      0      90
;;                                              2 COMMON     4     4      0
;;                          _timer_isr
;;                            _adc_isr
;; ---------------------------------------------------------------------------------
;; (5) _adc_isr                                              0     0      0       0
;; ---------------------------------------------------------------------------------
;; (5) _timer_isr                                            2     2      0      90
;;                                              0 COMMON     2     2      0
;; ---------------------------------------------------------------------------------
;; Estimated maximum stack depth 5
;; ---------------------------------------------------------------------------------

;; Call Graph Graphs:

;; _main (ROOT)
;;   _sfreg
;;   _adc_init
;;   _full_step_state_machine
;;     _linear_motor
;;       _adc_convert
;;     _get_timer
;;     _timer_set
;;     _eep_tally
;;       _get_timer
;;       _eeprom_write
;;       _timer_set
;;       _eeprom_read
;;
;; _interrupt_handler (ROOT)
;;   _timer_isr
;;   _adc_isr
;;

;; Address spaces:

;;Name               Size   Autos  Total    Cost      Usage
;;BITCOMMON            E      0       0       0        0.0%
;;EEDATA             100      0       0       0        0.0%
;;NULL                 0      0       0       0        0.0%
;;CODE                 0      0       0       0        0.0%
;;COMMON               E      6       C       1       85.7%
;;BITSFR0              0      0       0       1        0.0%
;;SFR0                 0      0       0       1        0.0%
;;BITSFR1              0      0       0       2        0.0%
;;SFR1                 0      0       0       2        0.0%
;;STACK                0      0       7       2        0.0%
;;ABS                  0      0      54       3        0.0%
;;BITBANK0            50      0       0       4        0.0%
;;BITSFR3              0      0       0       4        0.0%
;;SFR3                 0      0       0       4        0.0%
;;BANK0               50     1B      48       5       90.0%
;;BITSFR2              0      0       0       5        0.0%
;;SFR2                 0      0       0       5        0.0%
;;BITBANK1            50      0       0       6        0.0%
;;BANK1               50      0       0       7        0.0%
;;BITBANK3            60      0       0       8        0.0%
;;BANK3               60      0       0       9        0.0%
;;BITBANK2            60      0       0      10        0.0%
;;BANK2               60      0       0      11        0.0%
;;DATA                 0      0      5B      12        0.0%

	global	_main
psect	maintext,global,class=CODE,delta=2
global __pmaintext
__pmaintext:

;; *************** function _main *****************
;; Defined at:
;;		line 37 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\stepper_motor\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  adc_result      2    0        unsigned short 
;;  watchdog        1   26[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;                  2  1691[COMMON] int 
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, btemp+1, pclath, cstack
;; Tracked objects:
;;		On entry : 17F/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       1       0       0       0
;;      Temps:          0       1       0       0       0
;;      Totals:         0       2       0       0       0
;;Total ram usage:        2 bytes
;; Hardware stack levels required when called:    5
;; This function calls:
;;		_sfreg
;;		_adc_init
;;		_full_step_state_machine
;; This function is called by:
;;		Startup code after reset
;; This function uses a non-reentrant model
;;
psect	maintext
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\stepper_motor\main.c"
	line	37
	global	__size_of_main
	__size_of_main	equ	__end_of_main-_main
	
_main:	
	opt	stack 3
; Regs used in _main: [wreg-fsr0h+status,2+status,0+btemp+1+pclath+cstack]
	line	48
	
l7776:	
;main.c: 41: uint8_t watchdog;
;main.c: 42: uint16_t adc_result;
;main.c: 48: sfreg();
	fcall	_sfreg
	line	49
	
l7778:	
;main.c: 49: TMR1IE = 1;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bsf	(1120/8)^080h,(1120)&7
	line	50
	
l7780:	
;main.c: 50: ADIE = 1;
	bsf	(1126/8)^080h,(1126)&7
	line	51
	
l7782:	
;main.c: 51: PEIE = 1;
	bsf	(94/8),(94)&7
	line	52
	
l7784:	
;main.c: 52: (GIE = 1);
	bsf	(95/8),(95)&7
	line	53
;main.c: 53: adc_init();
	fcall	_adc_init
	line	55
	
l7786:	
;main.c: 55: full_step_state_machine();
	fcall	_full_step_state_machine
	line	59
;main.c: 59: while (HTS == 0)
	goto	l1692
	
l1693:	
	line	60
;main.c: 60: {}
	
l1692:	
	line	59
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	btfss	(1146/8)^080h,(1146)&7
	goto	u3851
	goto	u3850
u3851:
	goto	l1692
u3850:
	goto	l1695
	
l1694:	
	line	62
;main.c: 62: while(1)
	
l1695:	
	line	64
;main.c: 63: {
;main.c: 64: if(TO == 1)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfss	(28/8),(28)&7
	goto	u3861
	goto	u3860
u3861:
	goto	l7794
u3860:
	line	67
	
l7788:	
;main.c: 65: {
;main.c: 67: full_step_state_machine();
	fcall	_full_step_state_machine
	line	68
	
l7790:	
;main.c: 68: watchdog = TO;
	movlw	0
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfsc	(28/8),(28)&7
	movlw	1
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(main@watchdog)
	line	69
	
l7792:	
# 69 "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\stepper_motor\main.c"
clrwdt ;#
psect	maintext
	line	70
;main.c: 70: }
	goto	l1695
	line	71
	
l1696:	
	line	73
	
l7794:	
;main.c: 71: else
;main.c: 72: {
;main.c: 73: watchdog = TO;
	movlw	0
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfsc	(28/8),(28)&7
	movlw	1
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(main@watchdog)
	line	74
;main.c: 74: PORTD = persistent_watch_dog_count++;
	movf	(_persistent_watch_dog_count),w
	movwf	(8)	;volatile
	movlw	low(01h)
	addwf	(_persistent_watch_dog_count),f
	skipnc
	incf	(_persistent_watch_dog_count+1),f
	movlw	high(01h)
	addwf	(_persistent_watch_dog_count+1),f
	line	75
	
l7796:	
# 75 "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\stepper_motor\main.c"
clrwdt ;#
psect	maintext
	goto	l1695
	line	76
	
l1697:	
	goto	l1695
	line	77
	
l1698:	
	line	62
	goto	l1695
	
l1699:	
	line	78
	
l1700:	
	global	start
	ljmp	start
	opt stack 0
GLOBAL	__end_of_main
	__end_of_main:
;; =============== function _main ends ============

	signat	_main,90
	global	_full_step_state_machine
psect	text543,local,class=CODE,delta=2
global __ptext543
__ptext543:

;; *************** function _full_step_state_machine *****************
;; Defined at:
;;		line 79 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\stepper_motor\stepper_state_machine_half_full_time_potchange.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  pot_changed_    1   24[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, btemp+1, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       1       0       0       0
;;      Temps:          0       1       0       0       0
;;      Totals:         0       2       0       0       0
;;Total ram usage:        2 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    4
;; This function calls:
;;		_linear_motor
;;		_get_timer
;;		_timer_set
;;		_eep_tally
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text543
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\stepper_motor\stepper_state_machine_half_full_time_potchange.c"
	line	79
	global	__size_of_full_step_state_machine
	__size_of_full_step_state_machine	equ	__end_of_full_step_state_machine-_full_step_state_machine
	
_full_step_state_machine:	
	opt	stack 3
; Regs used in _full_step_state_machine: [wreg-fsr0h+status,2+status,0+btemp+1+pclath+cstack]
	line	88
	
l7678:	
;stepper_state_machine_half_full_time_potchange.c: 81: static uint8_t step_func_state = E_STEP_FUNC_MOTOR_OFF;
;stepper_state_machine_half_full_time_potchange.c: 82: static uint8_t index;
;stepper_state_machine_half_full_time_potchange.c: 83: static uint16_t adc_result_state_machine;
;stepper_state_machine_half_full_time_potchange.c: 84: static uint16_t old_pot_val;
;stepper_state_machine_half_full_time_potchange.c: 85: static uint16_t current_pot_val;
;stepper_state_machine_half_full_time_potchange.c: 86: uint8_t pot_changed_flag;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(full_step_state_machine@current_pot_val+1),w
	clrf	(full_step_state_machine@old_pot_val+1)
	addwf	(full_step_state_machine@old_pot_val+1)
	movf	(full_step_state_machine@current_pot_val),w
	clrf	(full_step_state_machine@old_pot_val)
	addwf	(full_step_state_machine@old_pot_val)

	line	89
	
l7680:	
;stepper_state_machine_half_full_time_potchange.c: 89: current_pot_val = linear_motor();
	fcall	_linear_motor
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(1+(?_linear_motor)),w
	clrf	(full_step_state_machine@current_pot_val+1)
	addwf	(full_step_state_machine@current_pot_val+1)
	movf	(0+(?_linear_motor)),w
	clrf	(full_step_state_machine@current_pot_val)
	addwf	(full_step_state_machine@current_pot_val)

	line	91
	
l7682:	
;stepper_state_machine_half_full_time_potchange.c: 91: if(current_pot_val == old_pot_val)
	movf	(full_step_state_machine@old_pot_val+1),w
	xorwf	(full_step_state_machine@current_pot_val+1),w
	skipz
	goto	u3725
	movf	(full_step_state_machine@old_pot_val),w
	xorwf	(full_step_state_machine@current_pot_val),w
u3725:

	skipz
	goto	u3721
	goto	u3720
u3721:
	goto	l7686
u3720:
	line	93
	
l7684:	
;stepper_state_machine_half_full_time_potchange.c: 92: {
;stepper_state_machine_half_full_time_potchange.c: 93: pot_changed_flag = FALSE;
	clrc
	movlw	0
	btfsc	status,0
	movlw	1
	movwf	(full_step_state_machine@pot_changed_flag)
	line	94
;stepper_state_machine_half_full_time_potchange.c: 94: }
	goto	l7688
	line	95
	
l3434:	
	line	97
	
l7686:	
;stepper_state_machine_half_full_time_potchange.c: 95: else
;stepper_state_machine_half_full_time_potchange.c: 96: {
;stepper_state_machine_half_full_time_potchange.c: 97: pot_changed_flag = TRUE;
	clrf	(full_step_state_machine@pot_changed_flag)
	bsf	status,0
	rlf	(full_step_state_machine@pot_changed_flag),f
	goto	l7688
	line	98
	
l3435:	
	line	100
	
l7688:	
;stepper_state_machine_half_full_time_potchange.c: 98: }
;stepper_state_machine_half_full_time_potchange.c: 100: if (current_pot_val == 0x1388)
	movlw	high(01388h)
	xorwf	(full_step_state_machine@current_pot_val+1),w
	skipz
	goto	u3735
	movlw	low(01388h)
	xorwf	(full_step_state_machine@current_pot_val),w
u3735:

	skipz
	goto	u3731
	goto	u3730
u3731:
	goto	l7692
u3730:
	line	102
	
l7690:	
;stepper_state_machine_half_full_time_potchange.c: 101: {
;stepper_state_machine_half_full_time_potchange.c: 102: step_func_state = E_STEP_FUNC_MOTOR_OFF;
	clrc
	movlw	0
	btfsc	status,0
	movlw	1
	movwf	(full_step_state_machine@step_func_state)
	line	103
;stepper_state_machine_half_full_time_potchange.c: 103: }
	goto	l7770
	line	104
	
l3436:	
	line	106
	
l7692:	
;stepper_state_machine_half_full_time_potchange.c: 104: else
;stepper_state_machine_half_full_time_potchange.c: 105: {
;stepper_state_machine_half_full_time_potchange.c: 106: adc_result_state_machine = current_pot_val;
	movf	(full_step_state_machine@current_pot_val+1),w
	clrf	(full_step_state_machine@adc_result_state_machine+1)
	addwf	(full_step_state_machine@adc_result_state_machine+1)
	movf	(full_step_state_machine@current_pot_val),w
	clrf	(full_step_state_machine@adc_result_state_machine)
	addwf	(full_step_state_machine@adc_result_state_machine)

	goto	l7770
	line	107
	
l3437:	
	line	109
;stepper_state_machine_half_full_time_potchange.c: 107: }
;stepper_state_machine_half_full_time_potchange.c: 109: switch (step_func_state)
	goto	l7770
	line	111
;stepper_state_machine_half_full_time_potchange.c: 110: {
;stepper_state_machine_half_full_time_potchange.c: 111: case E_STEP_FUNC_FORWARD:
	
l3439:	
	line	112
	
l7694:	
;stepper_state_machine_half_full_time_potchange.c: 112: if((get_timer(TIMER_1) == 0) || (pot_changed_flag == TRUE))
	movlw	(0)
	fcall	_get_timer
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(1+(?_get_timer)),w
	iorwf	(0+(?_get_timer)),w
	skipnz
	goto	u3741
	goto	u3740
u3741:
	goto	l7698
u3740:
	
l7696:	
	movf	(full_step_state_machine@pot_changed_flag),w
	xorlw	01h
	skipz
	goto	u3751
	goto	u3750
u3751:
	goto	l7772
u3750:
	goto	l7698
	
l3442:	
	line	114
	
l7698:	
;stepper_state_machine_half_full_time_potchange.c: 113: {
;stepper_state_machine_half_full_time_potchange.c: 114: step_func_temp = look_up_array[index];
	movf	(full_step_state_machine@index),w
	addlw	low((_look_up_array-__stringbase))
	movwf	fsr0
	fcall	stringdir
	movwf	(??_full_step_state_machine+0)+0
	movf	(??_full_step_state_machine+0)+0,w
	movwf	(_step_func_temp)
	line	115
	
l7700:	
;stepper_state_machine_half_full_time_potchange.c: 115: step_func_output = step_func_temp;
	movf	(_step_func_temp),w
	movwf	(??_full_step_state_machine+0)+0
	movf	(??_full_step_state_machine+0)+0,w
	movwf	(_step_func_output)
	line	116
	
l7702:	
;stepper_state_machine_half_full_time_potchange.c: 116: timer_set(TIMER_1,adc_result_state_machine);
	movf	(full_step_state_machine@adc_result_state_machine+1),w
	clrf	(?_timer_set+1)
	addwf	(?_timer_set+1)
	movf	(full_step_state_machine@adc_result_state_machine),w
	clrf	(?_timer_set)
	addwf	(?_timer_set)

	movlw	(0)
	fcall	_timer_set
	line	117
	
l7704:	
;stepper_state_machine_half_full_time_potchange.c: 117: index++;
	movlw	(01h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_full_step_state_machine+0)+0
	movf	(??_full_step_state_machine+0)+0,w
	addwf	(full_step_state_machine@index),f
	line	118
	
l7706:	
;stepper_state_machine_half_full_time_potchange.c: 118: step_func_state = E_STEP_FUNC_FORWARD;
	clrf	(full_step_state_machine@step_func_state)
	bsf	status,0
	rlf	(full_step_state_machine@step_func_state),f
	line	120
	
l7708:	
;stepper_state_machine_half_full_time_potchange.c: 120: if (RE0 == 1)
	btfss	(72/8),(72)&7
	goto	u3761
	goto	u3760
u3761:
	goto	l3443
u3760:
	line	122
	
l7710:	
;stepper_state_machine_half_full_time_potchange.c: 121: {
;stepper_state_machine_half_full_time_potchange.c: 122: step_func_state = E_STEP_FUNC_REVERSE;
	movlw	(02h)
	movwf	(??_full_step_state_machine+0)+0
	movf	(??_full_step_state_machine+0)+0,w
	movwf	(full_step_state_machine@step_func_state)
	line	123
	
l3443:	
	line	124
;stepper_state_machine_half_full_time_potchange.c: 123: }
;stepper_state_machine_half_full_time_potchange.c: 124: if (index > 3)
	movlw	(04h)
	subwf	(full_step_state_machine@index),w
	skipc
	goto	u3771
	goto	u3770
u3771:
	goto	l7772
u3770:
	line	126
	
l7712:	
;stepper_state_machine_half_full_time_potchange.c: 125: {
;stepper_state_machine_half_full_time_potchange.c: 126: index = 0;
	clrc
	movlw	0
	btfsc	status,0
	movlw	1
	movwf	(full_step_state_machine@index)
	line	127
	
l7714:	
;stepper_state_machine_half_full_time_potchange.c: 127: step_func_temp = look_up_array[index];
	movf	(full_step_state_machine@index),w
	addlw	low((_look_up_array-__stringbase))
	movwf	fsr0
	fcall	stringdir
	movwf	(??_full_step_state_machine+0)+0
	movf	(??_full_step_state_machine+0)+0,w
	movwf	(_step_func_temp)
	line	128
;stepper_state_machine_half_full_time_potchange.c: 128: }
	goto	l7772
	line	129
	
l3444:	
	goto	l7772
	line	132
;stepper_state_machine_half_full_time_potchange.c: 129: else
;stepper_state_machine_half_full_time_potchange.c: 130: {
	
l3445:	
	line	133
;stepper_state_machine_half_full_time_potchange.c: 132: }
;stepper_state_machine_half_full_time_potchange.c: 133: }
	goto	l7772
	line	134
	
l3440:	
	goto	l7772
	line	137
;stepper_state_machine_half_full_time_potchange.c: 134: else
;stepper_state_machine_half_full_time_potchange.c: 135: {
	
l3446:	
	line	139
;stepper_state_machine_half_full_time_potchange.c: 137: }
;stepper_state_machine_half_full_time_potchange.c: 139: break;
	goto	l7772
	line	141
;stepper_state_machine_half_full_time_potchange.c: 141: case E_STEP_FUNC_REVERSE:
	
l3448:	
	line	142
	
l7716:	
;stepper_state_machine_half_full_time_potchange.c: 142: if((get_timer(TIMER_1) == 0) || (pot_changed_flag == TRUE))
	movlw	(0)
	fcall	_get_timer
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(1+(?_get_timer)),w
	iorwf	(0+(?_get_timer)),w
	skipnz
	goto	u3781
	goto	u3780
u3781:
	goto	l7720
u3780:
	
l7718:	
	movf	(full_step_state_machine@pot_changed_flag),w
	xorlw	01h
	skipz
	goto	u3791
	goto	u3790
u3791:
	goto	l7772
u3790:
	goto	l7720
	
l3451:	
	line	144
	
l7720:	
;stepper_state_machine_half_full_time_potchange.c: 143: {
;stepper_state_machine_half_full_time_potchange.c: 144: step_func_temp = look_up_array[index];
	movf	(full_step_state_machine@index),w
	addlw	low((_look_up_array-__stringbase))
	movwf	fsr0
	fcall	stringdir
	movwf	(??_full_step_state_machine+0)+0
	movf	(??_full_step_state_machine+0)+0,w
	movwf	(_step_func_temp)
	line	145
	
l7722:	
;stepper_state_machine_half_full_time_potchange.c: 145: step_func_output = step_func_temp;
	movf	(_step_func_temp),w
	movwf	(??_full_step_state_machine+0)+0
	movf	(??_full_step_state_machine+0)+0,w
	movwf	(_step_func_output)
	line	146
	
l7724:	
;stepper_state_machine_half_full_time_potchange.c: 146: timer_set(TIMER_1, adc_result_state_machine);
	movf	(full_step_state_machine@adc_result_state_machine+1),w
	clrf	(?_timer_set+1)
	addwf	(?_timer_set+1)
	movf	(full_step_state_machine@adc_result_state_machine),w
	clrf	(?_timer_set)
	addwf	(?_timer_set)

	movlw	(0)
	fcall	_timer_set
	line	147
	
l7726:	
;stepper_state_machine_half_full_time_potchange.c: 147: step_func_state = E_STEP_FUNC_REVERSE;
	movlw	(02h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_full_step_state_machine+0)+0
	movf	(??_full_step_state_machine+0)+0,w
	movwf	(full_step_state_machine@step_func_state)
	line	149
	
l7728:	
;stepper_state_machine_half_full_time_potchange.c: 149: if (RE0 == 0)
	btfsc	(72/8),(72)&7
	goto	u3801
	goto	u3800
u3801:
	goto	l7732
u3800:
	line	151
	
l7730:	
;stepper_state_machine_half_full_time_potchange.c: 150: {
;stepper_state_machine_half_full_time_potchange.c: 151: step_func_state = E_STEP_FUNC_FORWARD;
	clrf	(full_step_state_machine@step_func_state)
	bsf	status,0
	rlf	(full_step_state_machine@step_func_state),f
	goto	l7732
	line	152
	
l3452:	
	line	154
	
l7732:	
;stepper_state_machine_half_full_time_potchange.c: 152: }
;stepper_state_machine_half_full_time_potchange.c: 154: if (index > 0)
	movf	(full_step_state_machine@index),w
	skipz
	goto	u3810
	goto	l7736
u3810:
	line	156
	
l7734:	
;stepper_state_machine_half_full_time_potchange.c: 155: {
;stepper_state_machine_half_full_time_potchange.c: 156: index--;
	movlw	low(01h)
	subwf	(full_step_state_machine@index),f
	line	157
;stepper_state_machine_half_full_time_potchange.c: 157: }
	goto	l7772
	line	158
	
l3453:	
	
l7736:	
;stepper_state_machine_half_full_time_potchange.c: 158: else if (index == 0)
	movf	(full_step_state_machine@index),f
	skipz
	goto	u3821
	goto	u3820
u3821:
	goto	l7772
u3820:
	line	160
	
l7738:	
;stepper_state_machine_half_full_time_potchange.c: 159: {
;stepper_state_machine_half_full_time_potchange.c: 160: index = 3;
	movlw	(03h)
	movwf	(??_full_step_state_machine+0)+0
	movf	(??_full_step_state_machine+0)+0,w
	movwf	(full_step_state_machine@index)
	line	161
;stepper_state_machine_half_full_time_potchange.c: 161: }
	goto	l7772
	line	162
	
l3455:	
	goto	l7772
	line	165
;stepper_state_machine_half_full_time_potchange.c: 162: else
;stepper_state_machine_half_full_time_potchange.c: 163: {
	
l3456:	
	goto	l7772
	
l3454:	
	line	166
;stepper_state_machine_half_full_time_potchange.c: 165: }
;stepper_state_machine_half_full_time_potchange.c: 166: }
	goto	l7772
	line	167
	
l3449:	
	goto	l7772
	line	170
;stepper_state_machine_half_full_time_potchange.c: 167: else
;stepper_state_machine_half_full_time_potchange.c: 168: {
	
l3457:	
	line	172
;stepper_state_machine_half_full_time_potchange.c: 170: }
;stepper_state_machine_half_full_time_potchange.c: 172: break;
	goto	l7772
	line	174
;stepper_state_machine_half_full_time_potchange.c: 174: case E_STEP_FUNC_MOTOR_OFF:
	
l3458:	
	line	175
	
l7740:	
;stepper_state_machine_half_full_time_potchange.c: 175: index = 0;
	clrc
	movlw	0
	btfsc	status,0
	movlw	1
	movwf	(full_step_state_machine@index)
	line	176
	
l7742:	
;stepper_state_machine_half_full_time_potchange.c: 176: step_func_output = 0xF0;
	movlw	(0F0h)
	movwf	(??_full_step_state_machine+0)+0
	movf	(??_full_step_state_machine+0)+0,w
	movwf	(_step_func_output)
	line	177
	
l7744:	
;stepper_state_machine_half_full_time_potchange.c: 177: adc_result_state_machine = current_pot_val;
	movf	(full_step_state_machine@current_pot_val+1),w
	clrf	(full_step_state_machine@adc_result_state_machine+1)
	addwf	(full_step_state_machine@adc_result_state_machine+1)
	movf	(full_step_state_machine@current_pot_val),w
	clrf	(full_step_state_machine@adc_result_state_machine)
	addwf	(full_step_state_machine@adc_result_state_machine)

	line	179
	
l7746:	
;stepper_state_machine_half_full_time_potchange.c: 179: timer_set(TIMER_1,adc_result_state_machine);
	movf	(full_step_state_machine@adc_result_state_machine+1),w
	clrf	(?_timer_set+1)
	addwf	(?_timer_set+1)
	movf	(full_step_state_machine@adc_result_state_machine),w
	clrf	(?_timer_set)
	addwf	(?_timer_set)

	movlw	(0)
	fcall	_timer_set
	line	180
	
l7748:	
;stepper_state_machine_half_full_time_potchange.c: 180: if (RE0 == 0)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfsc	(72/8),(72)&7
	goto	u3831
	goto	u3830
u3831:
	goto	l7752
u3830:
	line	182
	
l7750:	
;stepper_state_machine_half_full_time_potchange.c: 181: {
;stepper_state_machine_half_full_time_potchange.c: 182: step_func_state = E_STEP_FUNC_FORWARD;
	clrf	(full_step_state_machine@step_func_state)
	bsf	status,0
	rlf	(full_step_state_machine@step_func_state),f
	line	183
;stepper_state_machine_half_full_time_potchange.c: 183: }
	goto	l7772
	line	184
	
l3459:	
	line	186
	
l7752:	
;stepper_state_machine_half_full_time_potchange.c: 184: else
;stepper_state_machine_half_full_time_potchange.c: 185: {
;stepper_state_machine_half_full_time_potchange.c: 186: step_func_state = E_STEP_FUNC_REVERSE;
	movlw	(02h)
	movwf	(??_full_step_state_machine+0)+0
	movf	(??_full_step_state_machine+0)+0,w
	movwf	(full_step_state_machine@step_func_state)
	goto	l7772
	line	187
	
l3460:	
	line	188
;stepper_state_machine_half_full_time_potchange.c: 187: }
;stepper_state_machine_half_full_time_potchange.c: 188: break;
	goto	l7772
	line	190
;stepper_state_machine_half_full_time_potchange.c: 190: default:
	
l3461:	
	line	191
	
l7754:	
;stepper_state_machine_half_full_time_potchange.c: 191: index = 0;
	clrc
	movlw	0
	btfsc	status,0
	movlw	1
	movwf	(full_step_state_machine@index)
	line	192
	
l7756:	
;stepper_state_machine_half_full_time_potchange.c: 192: step_func_output = 0xF0;
	movlw	(0F0h)
	movwf	(??_full_step_state_machine+0)+0
	movf	(??_full_step_state_machine+0)+0,w
	movwf	(_step_func_output)
	line	193
	
l7758:	
;stepper_state_machine_half_full_time_potchange.c: 193: adc_result_state_machine = current_pot_val;
	movf	(full_step_state_machine@current_pot_val+1),w
	clrf	(full_step_state_machine@adc_result_state_machine+1)
	addwf	(full_step_state_machine@adc_result_state_machine+1)
	movf	(full_step_state_machine@current_pot_val),w
	clrf	(full_step_state_machine@adc_result_state_machine)
	addwf	(full_step_state_machine@adc_result_state_machine)

	line	195
	
l7760:	
;stepper_state_machine_half_full_time_potchange.c: 195: timer_set(TIMER_1,adc_result_state_machine);
	movf	(full_step_state_machine@adc_result_state_machine+1),w
	clrf	(?_timer_set+1)
	addwf	(?_timer_set+1)
	movf	(full_step_state_machine@adc_result_state_machine),w
	clrf	(?_timer_set)
	addwf	(?_timer_set)

	movlw	(0)
	fcall	_timer_set
	line	196
	
l7762:	
;stepper_state_machine_half_full_time_potchange.c: 196: if (RE0 == 0)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfsc	(72/8),(72)&7
	goto	u3841
	goto	u3840
u3841:
	goto	l7766
u3840:
	line	198
	
l7764:	
;stepper_state_machine_half_full_time_potchange.c: 197: {
;stepper_state_machine_half_full_time_potchange.c: 198: step_func_state = E_STEP_FUNC_FORWARD;
	clrf	(full_step_state_machine@step_func_state)
	bsf	status,0
	rlf	(full_step_state_machine@step_func_state),f
	line	199
;stepper_state_machine_half_full_time_potchange.c: 199: }
	goto	l7772
	line	200
	
l3462:	
	line	202
	
l7766:	
;stepper_state_machine_half_full_time_potchange.c: 200: else
;stepper_state_machine_half_full_time_potchange.c: 201: {
;stepper_state_machine_half_full_time_potchange.c: 202: step_func_state = E_STEP_FUNC_REVERSE;
	movlw	(02h)
	movwf	(??_full_step_state_machine+0)+0
	movf	(??_full_step_state_machine+0)+0,w
	movwf	(full_step_state_machine@step_func_state)
	goto	l7772
	line	203
	
l3463:	
	line	205
;stepper_state_machine_half_full_time_potchange.c: 203: }
;stepper_state_machine_half_full_time_potchange.c: 205: break;
	goto	l7772
	line	206
	
l7768:	
;stepper_state_machine_half_full_time_potchange.c: 206: }
	goto	l7772
	line	109
	
l3438:	
	
l7770:	
	movf	(full_step_state_machine@step_func_state),w
	; Switch size 1, requested type "space"
; Number of cases is 3, Range of values is 0 to 2
; switch strategies available:
; Name         Bytes Cycles
; simple_byte    10     6 (average)
; direct_byte    28    19 (fixed)
;	Chosen strategy is simple_byte

	xorlw	0^0	; case 0
	skipnz
	goto	l7740
	xorlw	1^0	; case 1
	skipnz
	goto	l7694
	xorlw	2^1	; case 2
	skipnz
	goto	l7716
	goto	l7754

	line	206
	
l3447:	
	line	209
	
l7772:	
;stepper_state_machine_half_full_time_potchange.c: 209: PORTD = step_func_output;
	movf	(_step_func_output),w
	movwf	(8)	;volatile
	line	210
	
l7774:	
;stepper_state_machine_half_full_time_potchange.c: 210: eep_tally(adc_result_state_machine);
	movf	(full_step_state_machine@adc_result_state_machine+1),w
	clrf	(?_eep_tally+1)
	addwf	(?_eep_tally+1)
	movf	(full_step_state_machine@adc_result_state_machine),w
	clrf	(?_eep_tally)
	addwf	(?_eep_tally)

	fcall	_eep_tally
	line	211
	
l3464:	
	return
	opt stack 0
GLOBAL	__end_of_full_step_state_machine
	__end_of_full_step_state_machine:
;; =============== function _full_step_state_machine ends ============

	signat	_full_step_state_machine,88
	global	_eep_tally
psect	text544,local,class=CODE,delta=2
global __ptext544
__ptext544:

;; *************** function _eep_tally *****************
;; Defined at:
;;		line 27 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\stepper_motor\eeprom.c"
;; Parameters:    Size  Location     Type
;;  step_funcout    2    6[BANK0 ] unsigned short 
;; Auto vars:     Size  Location     Type
;;  result_read1    2   16[BANK0 ] unsigned short 
;;  hi_result       1   22[BANK0 ] unsigned char 
;;  lo_result       1   21[BANK0 ] unsigned char 
;;  hi_read         1   20[BANK0 ] unsigned char 
;;  lo_read         1   19[BANK0 ] unsigned char 
;;  average_spee    1   18[BANK0 ] unsigned char 
;;  i               1    0        unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, btemp+1, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       2       0       0       0
;;      Locals:         0       7       0       0       0
;;      Temps:          0       8       0       0       0
;;      Totals:         0      17       0       0       0
;;Total ram usage:       17 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		_get_timer
;;		_eeprom_write
;;		_timer_set
;;		_eeprom_read
;; This function is called by:
;;		_full_step_state_machine
;; This function uses a non-reentrant model
;;
psect	text544
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\stepper_motor\eeprom.c"
	line	27
	global	__size_of_eep_tally
	__size_of_eep_tally	equ	__end_of_eep_tally-_eep_tally
	
_eep_tally:	
	opt	stack 3
; Regs used in _eep_tally: [wreg-fsr0h+status,2+status,0+btemp+1+pclath+cstack]
	line	39
	
l7614:	
;eeprom.c: 29: static uint16_t ee_addr;
;eeprom.c: 30: static uint32_t result_read32_t;
;eeprom.c: 31: uint8_t i;
;eeprom.c: 32: uint8_t lo_result;
;eeprom.c: 33: uint8_t hi_result;
;eeprom.c: 34: uint8_t average_speed_count_state_machi;
;eeprom.c: 35: uint8_t lo_read;
;eeprom.c: 36: uint8_t hi_read;
;eeprom.c: 37: uint16_t result_read16_t;
;eeprom.c: 39: if(get_timer (TIMER_EEPROM) == 0)
	movlw	(02h)
	fcall	_get_timer
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	((1+(?_get_timer))),w
	iorwf	((0+(?_get_timer))),w
	skipz
	goto	u3651
	goto	u3650
u3651:
	goto	l5268
u3650:
	line	41
	
l7616:	
;eeprom.c: 40: {
;eeprom.c: 41: if (ee_addr < (254) )
	movlw	high(0FEh)
	subwf	(eep_tally@ee_addr+1),w
	movlw	low(0FEh)
	skipnz
	subwf	(eep_tally@ee_addr),w
	skipnc
	goto	u3661
	goto	u3660
u3661:
	goto	l7632
u3660:
	line	43
	
l7618:	
;eeprom.c: 42: {
;eeprom.c: 43: hi_result = (step_funcout >> 8);
	movf	(eep_tally@step_funcout+1),w
	movwf	(??_eep_tally+0)+0
	movf	(??_eep_tally+0)+0,w
	movwf	(eep_tally@hi_result)
	line	44
	
l7620:	
;eeprom.c: 44: eeprom_write(ee_addr, hi_result);
	movf	(eep_tally@hi_result),w
	movwf	(??_eep_tally+0)+0
	movf	(??_eep_tally+0)+0,w
	movwf	(?_eeprom_write)
	movf	(eep_tally@ee_addr),w
	fcall	_eeprom_write
	line	45
	
l7622:	
;eeprom.c: 45: ee_addr++;
	movlw	low(01h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	addwf	(eep_tally@ee_addr),f
	skipnc
	incf	(eep_tally@ee_addr+1),f
	movlw	high(01h)
	addwf	(eep_tally@ee_addr+1),f
	line	47
	
l7624:	
;eeprom.c: 47: lo_result = (step_funcout & 0xFF);
	movf	(eep_tally@step_funcout),w
	movwf	(??_eep_tally+0)+0
	movf	(??_eep_tally+0)+0,w
	movwf	(eep_tally@lo_result)
	line	48
	
l7626:	
;eeprom.c: 48: eeprom_write(ee_addr, lo_result);
	movf	(eep_tally@lo_result),w
	movwf	(??_eep_tally+0)+0
	movf	(??_eep_tally+0)+0,w
	movwf	(?_eeprom_write)
	movf	(eep_tally@ee_addr),w
	fcall	_eeprom_write
	line	49
	
l7628:	
;eeprom.c: 49: ee_addr++;
	movlw	low(01h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	addwf	(eep_tally@ee_addr),f
	skipnc
	incf	(eep_tally@ee_addr+1),f
	movlw	high(01h)
	addwf	(eep_tally@ee_addr+1),f
	line	51
	
l7630:	
;eeprom.c: 51: timer_set(TIMER_EEPROM, (80));
	movlw	low(050h)
	movwf	(?_timer_set)
	movlw	high(050h)
	movwf	((?_timer_set))+1
	movlw	(02h)
	fcall	_timer_set
	line	52
;eeprom.c: 52: }
	goto	l5268
	line	53
	
l5256:	
	line	55
	
l7632:	
;eeprom.c: 53: else
;eeprom.c: 54: {
;eeprom.c: 55: hi_result = (step_funcout >> 8);
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(eep_tally@step_funcout+1),w
	movwf	(??_eep_tally+0)+0
	movf	(??_eep_tally+0)+0,w
	movwf	(eep_tally@hi_result)
	line	56
	
l7634:	
;eeprom.c: 56: eeprom_write(ee_addr, hi_result);
	movf	(eep_tally@hi_result),w
	movwf	(??_eep_tally+0)+0
	movf	(??_eep_tally+0)+0,w
	movwf	(?_eeprom_write)
	movf	(eep_tally@ee_addr),w
	fcall	_eeprom_write
	line	57
	
l7636:	
;eeprom.c: 57: ee_addr++;
	movlw	low(01h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	addwf	(eep_tally@ee_addr),f
	skipnc
	incf	(eep_tally@ee_addr+1),f
	movlw	high(01h)
	addwf	(eep_tally@ee_addr+1),f
	line	59
	
l7638:	
;eeprom.c: 59: lo_result = (step_funcout & 0xFF);
	movf	(eep_tally@step_funcout),w
	movwf	(??_eep_tally+0)+0
	movf	(??_eep_tally+0)+0,w
	movwf	(eep_tally@lo_result)
	line	60
	
l7640:	
;eeprom.c: 60: eeprom_write(ee_addr, lo_result);
	movf	(eep_tally@lo_result),w
	movwf	(??_eep_tally+0)+0
	movf	(??_eep_tally+0)+0,w
	movwf	(?_eeprom_write)
	movf	(eep_tally@ee_addr),w
	fcall	_eeprom_write
	line	61
	
l7642:	
;eeprom.c: 61: ee_addr++;
	movlw	low(01h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	addwf	(eep_tally@ee_addr),f
	skipnc
	incf	(eep_tally@ee_addr+1),f
	movlw	high(01h)
	addwf	(eep_tally@ee_addr+1),f
	line	63
	
l7644:	
;eeprom.c: 63: if (RE0 == 1)
	btfss	(72/8),(72)&7
	goto	u3671
	goto	u3670
u3671:
	goto	l7674
u3670:
	line	65
	
l7646:	
;eeprom.c: 64: {
;eeprom.c: 65: average_speed_count_state_machi = E_SUMMATION_FOR_AVERAGE_SPEED_S;
	clrc
	movlw	0
	btfsc	status,0
	movlw	1
	movwf	(eep_tally@average_speed_count_state_machi)
	line	66
	
l7648:	
;eeprom.c: 66: ee_addr = (0x00);
	movlw	low(0)
	movwf	(eep_tally@ee_addr)
	movlw	high(0)
	movwf	((eep_tally@ee_addr))+1
	line	67
	
l7650:	
;eeprom.c: 67: result_read32_t = 0x00000000;
	movlw	0
	movwf	(eep_tally@result_read32_t+3)
	movlw	0
	movwf	(eep_tally@result_read32_t+2)
	movlw	0
	movwf	(eep_tally@result_read32_t+1)
	movlw	0
	movwf	(eep_tally@result_read32_t)

	line	68
;eeprom.c: 68: switch(average_speed_count_state_machi)
	goto	l7672
	line	70
;eeprom.c: 69: {
;eeprom.c: 70: case E_SUMMATION_FOR_AVERAGE_SPEED_S:
	
l5260:	
	line	71
;eeprom.c: 71: while( ee_addr <= 511)
	goto	l7664
	
l5262:	
	line	73
	
l7652:	
;eeprom.c: 72: {
;eeprom.c: 73: hi_read = eeprom_read(ee_addr);
	movf	(eep_tally@ee_addr),w
	fcall	_eeprom_read
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_eep_tally+0)+0
	movf	(??_eep_tally+0)+0,w
	movwf	(eep_tally@hi_read)
	line	74
	
l7654:	
;eeprom.c: 74: ee_addr++;
	movlw	low(01h)
	addwf	(eep_tally@ee_addr),f
	skipnc
	incf	(eep_tally@ee_addr+1),f
	movlw	high(01h)
	addwf	(eep_tally@ee_addr+1),f
	line	75
	
l7656:	
;eeprom.c: 75: lo_read = eeprom_read(ee_addr);
	movf	(eep_tally@ee_addr),w
	fcall	_eeprom_read
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_eep_tally+0)+0
	movf	(??_eep_tally+0)+0,w
	movwf	(eep_tally@lo_read)
	line	76
	
l7658:	
;eeprom.c: 76: result_read16_t = ((hi_read << 8) | lo_read );
	movf	(eep_tally@hi_read),w
	movwf	(??_eep_tally+0)+0
	clrf	(??_eep_tally+0)+0+1
	movlw	08h
	movwf	btemp+1
u3685:
	clrc
	rlf	(??_eep_tally+0)+0,f
	rlf	(??_eep_tally+0)+1,f
	decfsz	btemp+1,f
	goto	u3685
	movf	(eep_tally@lo_read),w
	iorwf	0+(??_eep_tally+0)+0,w
	movwf	(eep_tally@result_read16_t)
	movf	1+(??_eep_tally+0)+0,w
	movwf	1+(eep_tally@result_read16_t)
	line	77
	
l7660:	
;eeprom.c: 77: ee_addr++;
	movlw	low(01h)
	addwf	(eep_tally@ee_addr),f
	skipnc
	incf	(eep_tally@ee_addr+1),f
	movlw	high(01h)
	addwf	(eep_tally@ee_addr+1),f
	line	78
	
l7662:	
;eeprom.c: 78: result_read32_t = (result_read16_t + result_read32_t);
	movf	(eep_tally@result_read32_t),w
	movwf	(??_eep_tally+0)+0
	movf	(eep_tally@result_read32_t+1),w
	movwf	((??_eep_tally+0)+0+1)
	movf	(eep_tally@result_read32_t+2),w
	movwf	((??_eep_tally+0)+0+2)
	movf	(eep_tally@result_read32_t+3),w
	movwf	((??_eep_tally+0)+0+3)
	movf	(eep_tally@result_read16_t),w
	movwf	((??_eep_tally+4)+0)
	movf	(eep_tally@result_read16_t+1),w
	movwf	((??_eep_tally+4)+0+1)
	clrf	((??_eep_tally+4)+0+2)
	clrf	((??_eep_tally+4)+0+3)
	movf	0+(??_eep_tally+4)+0,w
	addwf	(??_eep_tally+0)+0,f
	movf	1+(??_eep_tally+4)+0,w
	skipnc
	incfsz	1+(??_eep_tally+4)+0,w
	goto	u3690
	goto	u3691
u3690:
	addwf	(??_eep_tally+0)+1,f
u3691:
	movf	2+(??_eep_tally+4)+0,w
	skipnc
	incfsz	2+(??_eep_tally+4)+0,w
	goto	u3692
	goto	u3693
u3692:
	addwf	(??_eep_tally+0)+2,f
u3693:
	movf	3+(??_eep_tally+4)+0,w
	skipnc
	incf	3+(??_eep_tally+4)+0,w
	addwf	(??_eep_tally+0)+3,f
	movf	3+(??_eep_tally+0)+0,w
	movwf	(eep_tally@result_read32_t+3)
	movf	2+(??_eep_tally+0)+0,w
	movwf	(eep_tally@result_read32_t+2)
	movf	1+(??_eep_tally+0)+0,w
	movwf	(eep_tally@result_read32_t+1)
	movf	0+(??_eep_tally+0)+0,w
	movwf	(eep_tally@result_read32_t)

	goto	l7664
	line	79
	
l5261:	
	line	71
	
l7664:	
	movlw	high(0200h)
	subwf	(eep_tally@ee_addr+1),w
	movlw	low(0200h)
	skipnz
	subwf	(eep_tally@ee_addr),w
	skipc
	goto	u3701
	goto	u3700
u3701:
	goto	l7652
u3700:
	goto	l7666
	
l5263:	
	line	80
	
l7666:	
;eeprom.c: 79: }
;eeprom.c: 80: average_result = (result_read32_t/256);
	movf	(eep_tally@result_read32_t),w
	movwf	(??_eep_tally+0)+0
	movf	(eep_tally@result_read32_t+1),w
	movwf	((??_eep_tally+0)+0+1)
	movf	(eep_tally@result_read32_t+2),w
	movwf	((??_eep_tally+0)+0+2)
	movf	(eep_tally@result_read32_t+3),w
	movwf	((??_eep_tally+0)+0+3)
	movlw	08h
u3715:
	clrc
	rrf	(??_eep_tally+0)+3,f
	rrf	(??_eep_tally+0)+2,f
	rrf	(??_eep_tally+0)+1,f
	rrf	(??_eep_tally+0)+0,f
u3710:
	addlw	-1
	skipz
	goto	u3715
	movf	1+(??_eep_tally+0)+0,w
	clrf	(_average_result+1)
	addwf	(_average_result+1)
	movf	0+(??_eep_tally+0)+0,w
	clrf	(_average_result)
	addwf	(_average_result)

	line	81
	
l7668:	
# 81 "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\stepper_motor\eeprom.c"
nop ;#
psect	text544
	line	82
;eeprom.c: 82: break;
	goto	l7674
	line	84
;eeprom.c: 84: default:
	
l5265:	
	line	85
;eeprom.c: 85: break;
	goto	l7674
	line	86
	
l7670:	
;eeprom.c: 86: }
	goto	l7674
	line	68
	
l5259:	
	
l7672:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(eep_tally@average_speed_count_state_machi),w
	; Switch size 1, requested type "space"
; Number of cases is 1, Range of values is 0 to 0
; switch strategies available:
; Name         Bytes Cycles
; simple_byte     4     3 (average)
; direct_byte    22    19 (fixed)
;	Chosen strategy is simple_byte

	xorlw	0^0	; case 0
	skipnz
	goto	l7664
	goto	l7674

	line	86
	
l5264:	
	line	87
;eeprom.c: 87: }
	goto	l7674
	line	88
	
l5258:	
	goto	l7674
	line	90
;eeprom.c: 88: else
;eeprom.c: 89: {
	
l5266:	
	line	92
	
l7674:	
;eeprom.c: 90: }
;eeprom.c: 92: ee_addr = (0x00);
	movlw	low(0)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(eep_tally@ee_addr)
	movlw	high(0)
	movwf	((eep_tally@ee_addr))+1
	line	93
	
l7676:	
;eeprom.c: 93: timer_set(TIMER_EEPROM, (80));
	movlw	low(050h)
	movwf	(?_timer_set)
	movlw	high(050h)
	movwf	((?_timer_set))+1
	movlw	(02h)
	fcall	_timer_set
	goto	l5268
	line	94
	
l5257:	
	line	95
;eeprom.c: 94: }
;eeprom.c: 95: }
	goto	l5268
	line	96
	
l5255:	
	goto	l5268
	line	98
;eeprom.c: 96: else
;eeprom.c: 97: {
	
l5267:	
	line	100
	
l5268:	
	return
	opt stack 0
GLOBAL	__end_of_eep_tally
	__end_of_eep_tally:
;; =============== function _eep_tally ends ============

	signat	_eep_tally,4216
	global	_linear_motor
psect	text545,local,class=CODE,delta=2
global __ptext545
__ptext545:

;; *************** function _linear_motor *****************
;; Defined at:
;;		line 194 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\stepper_motor\adc_func.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  index           1   15[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;                  2   10[BANK0 ] unsigned short 
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, btemp+1, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       2       0       0       0
;;      Locals:         0       1       0       0       0
;;      Temps:          0       3       0       0       0
;;      Totals:         0       6       0       0       0
;;Total ram usage:        6 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		_adc_convert
;; This function is called by:
;;		_full_step_state_machine
;;		_half_step_state_machine
;; This function uses a non-reentrant model
;;
psect	text545
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\stepper_motor\adc_func.c"
	line	194
	global	__size_of_linear_motor
	__size_of_linear_motor	equ	__end_of_linear_motor-_linear_motor
	
_linear_motor:	
	opt	stack 3
; Regs used in _linear_motor: [wreg-fsr0h+status,2+status,0+btemp+1+pclath+cstack]
	line	200
	
l7602:	
;adc_func.c: 195: static uint16_t raw_adc;
;adc_func.c: 196: static uint16_t old_raw_adc;
;adc_func.c: 197: static uint16_t adc_match_count;
;adc_func.c: 198: uint8_t index;
;adc_func.c: 200: old_raw_adc = raw_adc;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(linear_motor@raw_adc+1),w
	clrf	(linear_motor@old_raw_adc+1)
	addwf	(linear_motor@old_raw_adc+1)
	movf	(linear_motor@raw_adc),w
	clrf	(linear_motor@old_raw_adc)
	addwf	(linear_motor@old_raw_adc)

	line	201
	
l7604:	
;adc_func.c: 201: raw_adc = adc_convert();
	fcall	_adc_convert
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(1+(?_adc_convert)),w
	clrf	(linear_motor@raw_adc+1)
	addwf	(linear_motor@raw_adc+1)
	movf	(0+(?_adc_convert)),w
	clrf	(linear_motor@raw_adc)
	addwf	(linear_motor@raw_adc)

	line	203
	
l7606:	
# 203 "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\stepper_motor\adc_func.c"
nop ;#
psect	text545
	line	206
	
l7608:	
;adc_func.c: 206: index = (raw_adc >> 6);
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(linear_motor@raw_adc+1),w
	movwf	(??_linear_motor+0)+0+1
	movf	(linear_motor@raw_adc),w
	movwf	(??_linear_motor+0)+0
	movlw	06h
u3645:
	clrc
	rrf	(??_linear_motor+0)+1,f
	rrf	(??_linear_motor+0)+0,f
	addlw	-1
	skipz
	goto	u3645
	movf	0+(??_linear_motor+0)+0,w
	movwf	(??_linear_motor+2)+0
	movf	(??_linear_motor+2)+0,w
	movwf	(linear_motor@index)
	line	208
	
l7610:	
;adc_func.c: 208: return motor_speed_array[index];
	movf	(linear_motor@index),w
	movwf	(??_linear_motor+0)+0
	addwf	(??_linear_motor+0)+0,w
	addlw	low((_motor_speed_array-__stringbase))
	movwf	fsr0
	fcall	stringdir
	movwf	(?_linear_motor)
	fcall	stringdir
	movwf	(?_linear_motor+1)
	goto	l4390
	
l7612:	
	line	209
	
l4390:	
	return
	opt stack 0
GLOBAL	__end_of_linear_motor
	__end_of_linear_motor:
;; =============== function _linear_motor ends ============

	signat	_linear_motor,90
	global	_eeprom_read
psect	text546,local,class=CODE,delta=2
global __ptext546
__ptext546:

;; *************** function _eeprom_read *****************
;; Defined at:
;;		line 8 in file "eeread.c"
;; Parameters:    Size  Location     Type
;;  addr            1    wreg     unsigned char 
;; Auto vars:     Size  Location     Type
;;  addr            1    3[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       1       0       0       0
;;      Temps:          0       3       0       0       0
;;      Totals:         0       4       0       0       0
;;Total ram usage:        4 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_eep_tally
;; This function uses a non-reentrant model
;;
psect	text546
	file	"C:\Program Files (x86)\HI-TECH Software\PICC\9.80\sources\eeread.c"
	line	8
	global	__size_of_eeprom_read
	__size_of_eeprom_read	equ	__end_of_eeprom_read-_eeprom_read
	
_eeprom_read:	
	opt	stack 3
; Regs used in _eeprom_read: [wreg+status,2+status,0]
;eeprom_read@addr stored from wreg
	line	10
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(eeprom_read@addr)
	line	9
	
l5281:	
	line	10
# 10 "C:\Program Files (x86)\HI-TECH Software\PICC\9.80\sources\eeread.c"
clrwdt ;#
psect	text546
	line	11
	bsf	status, 5	;RP0=1, select bank3
	bsf	status, 6	;RP1=1, select bank3
	btfsc	(3169/8)^0180h,(3169)&7
	goto	u3631
	goto	u3630
u3631:
	goto	l5281
u3630:
	goto	l7598
	
l5282:	
	line	12
	
l7598:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(eeprom_read@addr),w
	bcf	status, 5	;RP0=0, select bank2
	bsf	status, 6	;RP1=1, select bank2
	movwf	(269)^0100h	;volatile
	movlw	(03Fh)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_eeprom_read+0)+0
	movf	(??_eeprom_read+0)+0,w
	bsf	status, 5	;RP0=1, select bank3
	bsf	status, 6	;RP1=1, select bank3
	andwf	(396)^0180h,f	;volatile
	bsf	(3168/8)^0180h,(3168)&7
	clrc
	btfsc	(3168/8)^0180h,(3168)&7
	setc
	movlw	0
	skipnc
	movlw	1

	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_eeprom_read+1)+0
	clrf	(??_eeprom_read+1)+0+1
	bcf	status, 5	;RP0=0, select bank2
	bsf	status, 6	;RP1=1, select bank2
	movf	(268)^0100h,w	;volatile
	goto	l5283
	
l7600:	
	line	13
	
l5283:	
	return
	opt stack 0
GLOBAL	__end_of_eeprom_read
	__end_of_eeprom_read:
;; =============== function _eeprom_read ends ============

	signat	_eeprom_read,4217
	global	_eeprom_write
psect	text547,local,class=CODE,delta=2
global __ptext547
__ptext547:

;; *************** function _eeprom_write *****************
;; Defined at:
;;		line 8 in file "C:\Program Files (x86)\HI-TECH Software\PICC\9.80\sources\eewrite.c"
;; Parameters:    Size  Location     Type
;;  addr            1    wreg     unsigned char 
;;  value           1    0[BANK0 ] unsigned char 
;; Auto vars:     Size  Location     Type
;;  addr            1    2[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       1       0       0       0
;;      Locals:         0       1       0       0       0
;;      Temps:          0       1       0       0       0
;;      Totals:         0       3       0       0       0
;;Total ram usage:        3 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_eep_tally
;; This function uses a non-reentrant model
;;
psect	text547
	file	"C:\Program Files (x86)\HI-TECH Software\PICC\9.80\sources\eewrite.c"
	line	8
	global	__size_of_eeprom_write
	__size_of_eeprom_write	equ	__end_of_eeprom_write-_eeprom_write
	
_eeprom_write:	
	opt	stack 3
; Regs used in _eeprom_write: [wreg+status,2+status,0]
;eeprom_write@addr stored from wreg
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(eeprom_write@addr)
	line	9
	
l5302:	
	goto	l5303
	
l5304:	
	
l5303:	
	bsf	status, 5	;RP0=1, select bank3
	bsf	status, 6	;RP1=1, select bank3
	btfsc	(3169/8)^0180h,(3169)&7
	goto	u3601
	goto	u3600
u3601:
	goto	l5303
u3600:
	goto	l7578
	
l5305:	
	
l7578:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(eeprom_write@addr),w
	bcf	status, 5	;RP0=0, select bank2
	bsf	status, 6	;RP1=1, select bank2
	movwf	(269)^0100h	;volatile
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(eeprom_write@value),w
	bcf	status, 5	;RP0=0, select bank2
	bsf	status, 6	;RP1=1, select bank2
	movwf	(268)^0100h	;volatile
	
l7580:	
	movlw	(03Fh)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_eeprom_write+0)+0
	movf	(??_eeprom_write+0)+0,w
	bsf	status, 5	;RP0=1, select bank3
	bsf	status, 6	;RP1=1, select bank3
	andwf	(396)^0180h,f	;volatile
	
l7582:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	bcf	(24/8),(24)&7
	
l7584:	
	btfss	(95/8),(95)&7
	goto	u3611
	goto	u3610
u3611:
	goto	l5306
u3610:
	
l7586:	
	bsf	(24/8),(24)&7
	
l5306:	
	bcf	(95/8),(95)&7
	bsf	status, 5	;RP0=1, select bank3
	bsf	status, 6	;RP1=1, select bank3
	bsf	(3170/8)^0180h,(3170)&7
	
l7588:	
	movlw	(055h)
	movwf	(397)^0180h	;volatile
	movlw	(0AAh)
	movwf	(397)^0180h	;volatile
	
l7590:	
	bsf	(3169/8)^0180h,(3169)&7
	
l7592:	
	bcf	(3170/8)^0180h,(3170)&7
	
l7594:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfss	(24/8),(24)&7
	goto	u3621
	goto	u3620
u3621:
	goto	l5309
u3620:
	
l7596:	
	bsf	(95/8),(95)&7
	goto	l5309
	
l5307:	
	goto	l5309
	
l5308:	
	line	10
	
l5309:	
	return
	opt stack 0
GLOBAL	__end_of_eeprom_write
	__end_of_eeprom_write:
;; =============== function _eeprom_write ends ============

	signat	_eeprom_write,8312
	global	_adc_convert
psect	text548,local,class=CODE,delta=2
global __ptext548
__ptext548:

;; *************** function _adc_convert *****************
;; Defined at:
;;		line 98 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\stepper_motor\adc_func.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  result          2    8[BANK0 ] unsigned short 
;;  adresl          2    6[BANK0 ] unsigned short 
;;  adresh          2    4[BANK0 ] unsigned short 
;; Return value:  Size  Location     Type
;;                  2    0[BANK0 ] unsigned short 
;; Registers used:
;;		wreg, status,2, status,0, btemp+1
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       2       0       0       0
;;      Locals:         0       6       0       0       0
;;      Temps:          0       2       0       0       0
;;      Totals:         0      10       0       0       0
;;Total ram usage:       10 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_linear_motor
;;		_adc_volt_convert
;; This function uses a non-reentrant model
;;
psect	text548
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\stepper_motor\adc_func.c"
	line	98
	global	__size_of_adc_convert
	__size_of_adc_convert	equ	__end_of_adc_convert-_adc_convert
	
_adc_convert:	
	opt	stack 3
; Regs used in _adc_convert: [wreg+status,2+status,0+btemp+1]
	line	102
	
l7564:	
;adc_func.c: 99: uint16_t adresh;
;adc_func.c: 100: uint16_t adresl;
;adc_func.c: 101: uint16_t result;
;adc_func.c: 102: adc_isr_flag = 0;
	movlw	low(0)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(_adc_isr_flag)
	movlw	high(0)
	movwf	((_adc_isr_flag))+1
	line	104
	
l7566:	
;adc_func.c: 104: GODONE = 1;
	bsf	(249/8),(249)&7
	line	106
;adc_func.c: 106: while (adc_isr_flag == 0)
	goto	l7568
	
l4362:	
	goto	l7568
	line	107
;adc_func.c: 107: {}
	
l4361:	
	line	106
	
l7568:	
	movf	(_adc_isr_flag+1),w
	iorwf	(_adc_isr_flag),w
	skipnz
	goto	u3581
	goto	u3580
u3581:
	goto	l7568
u3580:
	goto	l7570
	
l4363:	
	line	108
	
l7570:	
;adc_func.c: 108: adresl = ADRESL;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movf	(158)^080h,w	;volatile
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_adc_convert+0)+0
	clrf	(??_adc_convert+0)+0+1
	movf	0+(??_adc_convert+0)+0,w
	movwf	(adc_convert@adresl)
	movf	1+(??_adc_convert+0)+0,w
	movwf	(adc_convert@adresl+1)
	line	109
;adc_func.c: 109: adresh = ADRESH;
	movf	(30),w	;volatile
	movwf	(??_adc_convert+0)+0
	clrf	(??_adc_convert+0)+0+1
	movf	0+(??_adc_convert+0)+0,w
	movwf	(adc_convert@adresh)
	movf	1+(??_adc_convert+0)+0,w
	movwf	(adc_convert@adresh+1)
	line	110
	
l7572:	
;adc_func.c: 110: result = ((adresh << 8) |(adresl));
	movf	(adc_convert@adresh+1),w
	movwf	(??_adc_convert+0)+0+1
	movf	(adc_convert@adresh),w
	movwf	(??_adc_convert+0)+0
	movlw	08h
	movwf	btemp+1
u3595:
	clrc
	rlf	(??_adc_convert+0)+0,f
	rlf	(??_adc_convert+0)+1,f
	decfsz	btemp+1,f
	goto	u3595
	movf	(adc_convert@adresl),w
	iorwf	0+(??_adc_convert+0)+0,w
	movwf	(adc_convert@result)
	movf	(adc_convert@adresl+1),w
	iorwf	1+(??_adc_convert+0)+0,w
	movwf	1+(adc_convert@result)
	line	112
	
l7574:	
;adc_func.c: 112: return result;
	movf	(adc_convert@result+1),w
	clrf	(?_adc_convert+1)
	addwf	(?_adc_convert+1)
	movf	(adc_convert@result),w
	clrf	(?_adc_convert)
	addwf	(?_adc_convert)

	goto	l4364
	
l7576:	
	line	113
	
l4364:	
	return
	opt stack 0
GLOBAL	__end_of_adc_convert
	__end_of_adc_convert:
;; =============== function _adc_convert ends ============

	signat	_adc_convert,90
	global	_get_timer
psect	text549,local,class=CODE,delta=2
global __ptext549
__ptext549:

;; *************** function _get_timer *****************
;; Defined at:
;;		line 33 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\stepper_motor\timer.c"
;; Parameters:    Size  Location     Type
;;  index           1    wreg     unsigned char 
;; Auto vars:     Size  Location     Type
;;  index           1    5[BANK0 ] unsigned char 
;;  result          2    3[BANK0 ] unsigned short 
;; Return value:  Size  Location     Type
;;                  2    0[BANK0 ] unsigned short 
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       2       0       0       0
;;      Locals:         0       3       0       0       0
;;      Temps:          0       1       0       0       0
;;      Totals:         0       6       0       0       0
;;Total ram usage:        6 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_full_step_state_machine
;;		_eep_tally
;;		_half_step_state_machine
;; This function uses a non-reentrant model
;;
psect	text549
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\stepper_motor\timer.c"
	line	33
	global	__size_of_get_timer
	__size_of_get_timer	equ	__end_of_get_timer-_get_timer
	
_get_timer:	
	opt	stack 3
; Regs used in _get_timer: [wreg-fsr0h+status,2+status,0]
;get_timer@index stored from wreg
	line	36
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(get_timer@index)
	
l7442:	
;timer.c: 34: uint16_t result;
;timer.c: 36: if (index < TIMER_MAX)
	movlw	(03h)
	subwf	(get_timer@index),w
	skipnc
	goto	u3431
	goto	u3430
u3431:
	goto	l7450
u3430:
	line	38
	
l7444:	
;timer.c: 37: {
;timer.c: 38: (GIE = 0);
	bcf	(95/8),(95)&7
	line	39
	
l7446:	
;timer.c: 39: result = timer_array[index];
	movf	(get_timer@index),w
	movwf	(??_get_timer+0)+0
	addwf	(??_get_timer+0)+0,w
	addlw	_timer_array&0ffh
	movwf	fsr0
	bcf	status, 7	;select IRP bank0
	movf	indf,w
	movwf	(get_timer@result)
	incf	fsr0,f
	movf	indf,w
	movwf	(get_timer@result+1)
	line	40
	
l7448:	
;timer.c: 40: (GIE = 1);
	bsf	(95/8),(95)&7
	line	41
;timer.c: 41: }
	goto	l7452
	line	42
	
l2549:	
	line	44
	
l7450:	
;timer.c: 42: else
;timer.c: 43: {
;timer.c: 44: result = 0;
	movlw	low(0)
	movwf	(get_timer@result)
	movlw	high(0)
	movwf	((get_timer@result))+1
	goto	l7452
	line	45
	
l2550:	
	line	47
	
l7452:	
;timer.c: 45: }
;timer.c: 47: return result;
	movf	(get_timer@result+1),w
	clrf	(?_get_timer+1)
	addwf	(?_get_timer+1)
	movf	(get_timer@result),w
	clrf	(?_get_timer)
	addwf	(?_get_timer)

	goto	l2551
	
l7454:	
	line	49
	
l2551:	
	return
	opt stack 0
GLOBAL	__end_of_get_timer
	__end_of_get_timer:
;; =============== function _get_timer ends ============

	signat	_get_timer,4218
	global	_timer_set
psect	text550,local,class=CODE,delta=2
global __ptext550
__ptext550:

;; *************** function _timer_set *****************
;; Defined at:
;;		line 18 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\stepper_motor\timer.c"
;; Parameters:    Size  Location     Type
;;  index           1    wreg     unsigned char 
;;  value           2    0[BANK0 ] unsigned short 
;; Auto vars:     Size  Location     Type
;;  index           1    3[BANK0 ] unsigned char 
;;  array_conten    2    0        unsigned short 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       2       0       0       0
;;      Locals:         0       1       0       0       0
;;      Temps:          0       1       0       0       0
;;      Totals:         0       4       0       0       0
;;Total ram usage:        4 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_full_step_state_machine
;;		_eep_tally
;;		_half_step_state_machine
;; This function uses a non-reentrant model
;;
psect	text550
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\stepper_motor\timer.c"
	line	18
	global	__size_of_timer_set
	__size_of_timer_set	equ	__end_of_timer_set-_timer_set
	
_timer_set:	
	opt	stack 3
; Regs used in _timer_set: [wreg-fsr0h+status,2+status,0]
;timer_set@index stored from wreg
	line	20
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(timer_set@index)
	
l7434:	
;timer.c: 19: uint16_t array_contents;
;timer.c: 20: if (index < TIMER_MAX)
	movlw	(03h)
	subwf	(timer_set@index),w
	skipnc
	goto	u3421
	goto	u3420
u3421:
	goto	l2546
u3420:
	line	22
	
l7436:	
;timer.c: 21: {
;timer.c: 22: (GIE = 0);
	bcf	(95/8),(95)&7
	line	23
	
l7438:	
;timer.c: 23: timer_array[index] = value;
	movf	(timer_set@index),w
	movwf	(??_timer_set+0)+0
	addwf	(??_timer_set+0)+0,w
	addlw	_timer_array&0ffh
	movwf	fsr0
	movf	(timer_set@value),w
	bcf	status, 7	;select IRP bank0
	movwf	indf
	incf	fsr0,f
	movf	(timer_set@value+1),w
	movwf	indf
	line	24
	
l7440:	
;timer.c: 24: (GIE = 1);
	bsf	(95/8),(95)&7
	line	25
;timer.c: 25: }
	goto	l2546
	line	26
	
l2544:	
	goto	l2546
	line	28
;timer.c: 26: else
;timer.c: 27: {
	
l2545:	
	line	29
	
l2546:	
	return
	opt stack 0
GLOBAL	__end_of_timer_set
	__end_of_timer_set:
;; =============== function _timer_set ends ============

	signat	_timer_set,8312
	global	_adc_init
psect	text551,local,class=CODE,delta=2
global __ptext551
__ptext551:

;; *************** function _adc_init *****************
;; Defined at:
;;		line 227 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\stepper_motor\adc_func.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text551
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\stepper_motor\adc_func.c"
	line	227
	global	__size_of_adc_init
	__size_of_adc_init	equ	__end_of_adc_init-_adc_init
	
_adc_init:	
	opt	stack 5
; Regs used in _adc_init: [wreg]
	line	228
	
l7432:	
;adc_func.c: 228: ADCON0 = 0xC5;
	movlw	(0C5h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(31)	;volatile
	line	229
;adc_func.c: 229: ADCON1 = 0x80;
	movlw	(080h)
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movwf	(159)^080h	;volatile
	line	230
	
l4397:	
	return
	opt stack 0
GLOBAL	__end_of_adc_init
	__end_of_adc_init:
;; =============== function _adc_init ends ============

	signat	_adc_init,88
	global	_sfreg
psect	text552,local,class=CODE,delta=2
global __ptext552
__ptext552:

;; *************** function _sfreg *****************
;; Defined at:
;;		line 81 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\stepper_motor\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text552
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\stepper_motor\main.c"
	line	81
	global	__size_of_sfreg
	__size_of_sfreg	equ	__end_of_sfreg-_sfreg
	
_sfreg:	
	opt	stack 5
; Regs used in _sfreg: [wreg+status,2]
	line	82
	
l7416:	
;main.c: 82: WDTCON = 0x17;
	movlw	(017h)
	bcf	status, 5	;RP0=0, select bank2
	bsf	status, 6	;RP1=1, select bank2
	movwf	(261)^0100h	;volatile
	line	83
;main.c: 83: OSCCON = 0x61;
	movlw	(061h)
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movwf	(143)^080h	;volatile
	line	84
	
l7418:	
;main.c: 84: OPTION = 0x00;
	clrf	(129)^080h	;volatile
	line	85
	
l7420:	
;main.c: 85: TRISA = 0x02;
	movlw	(02h)
	movwf	(133)^080h	;volatile
	line	87
	
l7422:	
;main.c: 87: TRISE = 0x01;
	movlw	(01h)
	movwf	(137)^080h	;volatile
	line	88
	
l7424:	
;main.c: 88: TRISD = 0x00;
	clrf	(136)^080h	;volatile
	line	89
	
l7426:	
;main.c: 89: PORTD = 0x00;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(8)	;volatile
	line	90
	
l7428:	
;main.c: 90: T1CON = 0x35;
	movlw	(035h)
	movwf	(16)	;volatile
	line	93
	
l7430:	
;main.c: 93: ANSEL = 0x02;
	movlw	(02h)
	bsf	status, 5	;RP0=1, select bank3
	bsf	status, 6	;RP1=1, select bank3
	movwf	(392)^0180h	;volatile
	line	94
	
l1703:	
	return
	opt stack 0
GLOBAL	__end_of_sfreg
	__end_of_sfreg:
;; =============== function _sfreg ends ============

	signat	_sfreg,88
	global	_interrupt_handler
psect	text553,local,class=CODE,delta=2
global __ptext553
__ptext553:

;; *************** function _interrupt_handler *****************
;; Defined at:
;;		line 6 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\stepper_motor\interrupt.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          4       0       0       0       0
;;      Totals:         4       0       0       0       0
;;Total ram usage:        4 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		_timer_isr
;;		_adc_isr
;; This function is called by:
;;		Interrupt level 1
;; This function uses a non-reentrant model
;;
psect	text553
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\stepper_motor\interrupt.c"
	line	6
	global	__size_of_interrupt_handler
	__size_of_interrupt_handler	equ	__end_of_interrupt_handler-_interrupt_handler
	
_interrupt_handler:	
	opt	stack 3
; Regs used in _interrupt_handler: [wreg-fsr0h+status,2+status,0+pclath+cstack]
psect	intentry,class=CODE,delta=2
global __pintentry
__pintentry:
global interrupt_function
interrupt_function:
	global saved_w
	saved_w	set	btemp+0
	movwf	saved_w
	movf	status,w
	movwf	(??_interrupt_handler+0)
	movf	fsr0,w
	movwf	(??_interrupt_handler+1)
	movf	pclath,w
	movwf	(??_interrupt_handler+2)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	btemp+1,w
	movwf	(??_interrupt_handler+3)
	ljmp	_interrupt_handler
psect	text553
	line	7
	
i1l6982:	
;interrupt.c: 7: timer_isr();
	fcall	_timer_isr
	line	8
	
i1l6984:	
;interrupt.c: 8: adc_isr();
	fcall	_adc_isr
	line	9
	
i1l843:	
	movf	(??_interrupt_handler+3),w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	btemp+1
	movf	(??_interrupt_handler+2),w
	movwf	pclath
	movf	(??_interrupt_handler+1),w
	movwf	fsr0
	movf	(??_interrupt_handler+0),w
	movwf	status
	swapf	saved_w,f
	swapf	saved_w,w
	retfie
	opt stack 0
GLOBAL	__end_of_interrupt_handler
	__end_of_interrupt_handler:
;; =============== function _interrupt_handler ends ============

	signat	_interrupt_handler,88
	global	_adc_isr
psect	text554,local,class=CODE,delta=2
global __ptext554
__ptext554:

;; *************** function _adc_isr *****************
;; Defined at:
;;		line 233 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\stepper_motor\adc_func.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_interrupt_handler
;; This function uses a non-reentrant model
;;
psect	text554
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\stepper_motor\adc_func.c"
	line	233
	global	__size_of_adc_isr
	__size_of_adc_isr	equ	__end_of_adc_isr-_adc_isr
	
_adc_isr:	
	opt	stack 3
; Regs used in _adc_isr: [wreg]
	line	234
	
i1l7012:	
;adc_func.c: 234: if ((ADIF)&&(ADIE))
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfss	(102/8),(102)&7
	goto	u293_21
	goto	u293_20
u293_21:
	goto	i1l4402
u293_20:
	
i1l7014:	
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	btfss	(1126/8)^080h,(1126)&7
	goto	u294_21
	goto	u294_20
u294_21:
	goto	i1l4402
u294_20:
	line	237
	
i1l7016:	
;adc_func.c: 235: {
;adc_func.c: 237: ADIF=0;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	bcf	(102/8),(102)&7
	line	238
	
i1l7018:	
;adc_func.c: 238: adc_isr_flag=1;
	movlw	low(01h)
	movwf	(_adc_isr_flag)
	movlw	high(01h)
	movwf	((_adc_isr_flag))+1
	line	239
;adc_func.c: 239: int_count++;
	movlw	low(01h)
	addwf	(_int_count),f
	skipnc
	incf	(_int_count+1),f
	movlw	high(01h)
	addwf	(_int_count+1),f
	line	240
;adc_func.c: 240: }
	goto	i1l4402
	line	241
	
i1l4400:	
	goto	i1l4402
	line	243
;adc_func.c: 241: else
;adc_func.c: 242: {
	
i1l4401:	
	line	244
	
i1l4402:	
	return
	opt stack 0
GLOBAL	__end_of_adc_isr
	__end_of_adc_isr:
;; =============== function _adc_isr ends ============

	signat	_adc_isr,88
	global	_timer_isr
psect	text555,local,class=CODE,delta=2
global __ptext555
__ptext555:

;; *************** function _timer_isr *****************
;; Defined at:
;;		line 54 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\stepper_motor\timer.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  i               1    1[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         1       0       0       0       0
;;      Temps:          1       0       0       0       0
;;      Totals:         2       0       0       0       0
;;Total ram usage:        2 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_interrupt_handler
;; This function uses a non-reentrant model
;;
psect	text555
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\stepper_motor\timer.c"
	line	54
	global	__size_of_timer_isr
	__size_of_timer_isr	equ	__end_of_timer_isr-_timer_isr
	
_timer_isr:	
	opt	stack 3
; Regs used in _timer_isr: [wreg-fsr0h+status,2+status,0]
	line	56
	
i1l6986:	
;timer.c: 55: uint8_t i;
;timer.c: 56: if((TMR1IE)&&(TMR1IF))
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	btfss	(1120/8)^080h,(1120)&7
	goto	u288_21
	goto	u288_20
u288_21:
	goto	i1l2560
u288_20:
	
i1l6988:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfss	(96/8),(96)&7
	goto	u289_21
	goto	u289_20
u289_21:
	goto	i1l2560
u289_20:
	line	59
	
i1l6990:	
;timer.c: 57: {
;timer.c: 59: TMR1IF=0;
	bcf	(96/8),(96)&7
	line	60
	
i1l6992:	
;timer.c: 60: T1CON &= ~(0x01);
	movlw	(0FEh)
	movwf	(??_timer_isr+0)+0
	movf	(??_timer_isr+0)+0,w
	andwf	(16),f	;volatile
	line	61
	
i1l6994:	
;timer.c: 61: TMR1L = 0x89;
	movlw	(089h)
	movwf	(14)	;volatile
	line	62
	
i1l6996:	
;timer.c: 62: TMR1H = 0xFF;
	movlw	(0FFh)
	movwf	(15)	;volatile
	line	63
	
i1l6998:	
;timer.c: 63: T1CON |= 0x01;
	bsf	(16)+(0/8),(0)&7	;volatile
	line	65
;timer.c: 65: for (i = 0; i < TIMER_MAX; i++)
	clrc
	movlw	0
	btfsc	status,0
	movlw	1
	movwf	(timer_isr@i)
	
i1l7000:	
	movlw	(03h)
	subwf	(timer_isr@i),w
	skipc
	goto	u290_21
	goto	u290_20
u290_21:
	goto	i1l7004
u290_20:
	goto	i1l2560
	
i1l7002:	
	goto	i1l2560
	line	66
	
i1l2555:	
	line	67
	
i1l7004:	
;timer.c: 66: {
;timer.c: 67: if (timer_array[i] != 0)
	movf	(timer_isr@i),w
	movwf	(??_timer_isr+0)+0
	addwf	(??_timer_isr+0)+0,w
	addlw	_timer_array&0ffh
	movwf	fsr0
	bcf	status, 7	;select IRP bank0
	movf	indf,w
	incf	fsr0,f
	iorwf	indf,w
	skipnz
	goto	u291_21
	goto	u291_20
u291_21:
	goto	i1l7008
u291_20:
	line	69
	
i1l7006:	
;timer.c: 68: {
;timer.c: 69: timer_array[i]--;
	movf	(timer_isr@i),w
	movwf	(??_timer_isr+0)+0
	addwf	(??_timer_isr+0)+0,w
	addlw	_timer_array&0ffh
	movwf	fsr0
	movlw	low(01h)
	subwf	indf,f
	incfsz	fsr0,f
	movlw	high(01h)
	skipc
	decf	indf,f
	subwf	indf,f
	decf	fsr0,f
	line	70
;timer.c: 70: }
	goto	i1l7008
	line	71
	
i1l2557:	
	goto	i1l7008
	line	73
;timer.c: 71: else
;timer.c: 72: {
	
i1l2558:	
	line	65
	
i1l7008:	
	movlw	(01h)
	movwf	(??_timer_isr+0)+0
	movf	(??_timer_isr+0)+0,w
	addwf	(timer_isr@i),f
	
i1l7010:	
	movlw	(03h)
	subwf	(timer_isr@i),w
	skipc
	goto	u292_21
	goto	u292_20
u292_21:
	goto	i1l7004
u292_20:
	goto	i1l2560
	
i1l2556:	
	line	76
;timer.c: 73: }
;timer.c: 74: }
;timer.c: 76: }
	goto	i1l2560
	line	77
	
i1l2554:	
	goto	i1l2560
	line	79
;timer.c: 77: else
;timer.c: 78: {
	
i1l2559:	
	line	80
	
i1l2560:	
	return
	opt stack 0
GLOBAL	__end_of_timer_isr
	__end_of_timer_isr:
;; =============== function _timer_isr ends ============

	signat	_timer_isr,88
psect	text556,local,class=CODE,delta=2
global __ptext556
__ptext556:
	global	btemp
	btemp set 07Eh

	DABS	1,126,2	;btemp
	global	wtemp0
	wtemp0 set btemp
	end
