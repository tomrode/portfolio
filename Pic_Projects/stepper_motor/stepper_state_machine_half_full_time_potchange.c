#include "typedefs.h"
#include "htc.h"
#include "timer.h"
#include "stepper_state_machine.h"
#include "adc_func.h"
#include "eeprom.h"
#define TIMER1_VALUE (0x7D0)   // Will not use this due to doing a conversion


/* Enumeration definitions */

enum
{
  E_STEP_FUNC_MOTOR_OFF,
  E_STEP_FUNC_FORWARD,
  E_STEP_FUNC_REVERSE,
} E_STEP_FUNC;

enum
{
  E_HALFSTEP_FUNC_MOTOR_OFF,
  E_HALFSTEP_FUNC_FORWARD,
  E_HALFSTEP_FUNC_REVERSE,
} E_HALFSTEP_FUNC;   

/* Variable definitions */
const uint8_t look_up_array [] = 
                      {
                      0x03,  //index 0
                      0x06,  //index 1
                      0x0C,  //index 2
                      0x09   //index 3
                      };
const uint8_t halfstep_look_up_array [] =
                      {
                      0x01,  //index 0
                      0x03,  //index 1
                      0x02,  //index 2
                      0x06,  //index 3
                      0x04,  //index 4
                      0x0C,  //index 5
                      0x08,  //index 6
                      0x09   //index 7
                      };

                       
//uint8_t index;              
static uint8_t step_func_output;
static uint8_t step_func_temp;


/* Function definitions */

/***************************************************************************/
/*    full_step_state_machine();
/*     DESCRIPTION: Generate pulses on PORTD (RD0-RD3)in a specific order   
/*                  to step from one position to another for stepper motor.
/*                  This is high torque (standard application).
/*                  full step (best precision step angle 7.50�/phase)
/*                                TRUTH TABLE
/*                      _______________________________
/*                      |STEP| A1 | A2 | B1 |B2 | PORTD| 
/*                      ------------------------------- 
/*             START -> | 1  | 0  | 0  | 1  | 1 | 0x03 | |
/*                      -------------------------------  |
/*                      | 2  | 0  | 1  | 1  | 0 | 0x06 | |
/*                      -------------------------------  
/*                      | 3  | 1  | 1  | 0  | 0 | 0x0C |                           
/*                      -------------------------------
/*                      | 4  | 1  | 0  | 0  | 1 | 0x09 |
/*                      -------------------------------
/*              END ->  | 1  | 1  | 0  | 1  | 0 | 0x03 |
/*                      -------------------------------
/*     INPUTS: NONE
/*     OUPUTS: NONE  
/***************************************************************************/

void full_step_state_machine(void)
{
 
    static uint8_t step_func_state = E_STEP_FUNC_MOTOR_OFF;        // was static type
    static uint8_t index;                                          // was static type
    static uint16_t adc_result_state_machine;
    static uint16_t old_pot_val;
    static uint16_t current_pot_val;
           uint8_t pot_changed_flag;
  
    old_pot_val = current_pot_val;                               // the first reading from last time in state  
    current_pot_val = linear_motor();                            // Get the mostcurrent indexed value
     
    if(current_pot_val == old_pot_val)                           // make the test for equal to see if pot changed
    {
        pot_changed_flag = FALSE;                                  // they are egual not pot change FALSE = 0
    }
    else
    {
        pot_changed_flag = TRUE;                                   // they are not equal so pot did change TRUE = 1
    }
   
    if (current_pot_val == 0x1388)
    {
        step_func_state = E_STEP_FUNC_MOTOR_OFF;
    }
    else
    {
        adc_result_state_machine = current_pot_val;                 //The time setting from and adc read in
    }
   
    switch (step_func_state)
    {
        case E_STEP_FUNC_FORWARD:    
        if((get_timer(TIMER_1) == 0) || (pot_changed_flag == TRUE))  // check if timer counted down or poteniometer changed   
        { 
            step_func_temp = look_up_array[index];               // Get the array to element zero
            step_func_output = step_func_temp;
            timer_set(TIMER_1,adc_result_state_machine);         //the value of a conversion in place of TIMER1_VALUE);
            index++;
            step_func_state = E_STEP_FUNC_FORWARD;
         
            if (RE0 == 1)                                        // Check the switch 
            {
                step_func_state = E_STEP_FUNC_REVERSE;
            }
            if (index > 3)                                        // restart the whole process  
            {
                index = 0;                                           // reset the sequence       
                step_func_temp = look_up_array[index];
            }
            else
            {
               /* Do not shift the LED output. */
            }
        }
        else
        {
            /* Do nothing except remain the same state. */
        }

        break;

        case E_STEP_FUNC_REVERSE:
        if((get_timer(TIMER_1) == 0) || (pot_changed_flag == TRUE)) // check if timer counted down or poteniometer changed   
        { 
            step_func_temp = look_up_array[index];                 // Get the array to element 
            step_func_output = step_func_temp;
            timer_set(TIMER_1, adc_result_state_machine);          //the value of a conversion in place of TIMER1_VALUE);
            step_func_state = E_STEP_FUNC_REVERSE;
           
            if (RE0 == 0)                                         // check the switch
            {
                step_func_state = E_STEP_FUNC_FORWARD;
            }
   
            if (index > 0)                                        // This will prevent rolling back to 0xFF           
            {
                index--;
            }  
            else if (index == 0)                                  // restart the whole process  THIS else if fixed the missing state 
            {
                index = 3;                                          // But from the top of the array
            }
            else
            {
                /* Do not shift the LED output. */
            }
        }
        else
        {
           /* Do nothing except remain the same state. */
        }

        break;
   
        case E_STEP_FUNC_MOTOR_OFF:
        index = 0;
        step_func_output = 0xF0;                           // output everything on PORTD initially
        adc_result_state_machine = current_pot_val;//linear_motor();     //The time setting from and adc read in
      
        timer_set(TIMER_1,adc_result_state_machine);                          //the value for start up); 
        if (RE0 == 0)
        {      
            step_func_state = E_STEP_FUNC_FORWARD;             // Set for the state machione to go forward off the bat.
        }
        else
        {
            step_func_state = E_STEP_FUNC_REVERSE;             // Set for the state machione to go reverse off the bat
        }
        break;
     
        default:
        index = 0;
        step_func_output = 0xF0;                           // output everything on PORTD initially
        adc_result_state_machine = current_pot_val;//linear_motor();     //The time setting from and adc read in
      
        timer_set(TIMER_1,adc_result_state_machine);                          //the value for start up); 
        if (RE0 == 0)
        {      
            step_func_state = E_STEP_FUNC_FORWARD;             // Set for the state machione to go forward off the bat.
        }
        else
        {
            step_func_state = E_STEP_FUNC_REVERSE;             // Set for the state machione to go reverse off the bat
        }
        
        break;
    }

    /* Actions independent of state */
    PORTD = step_func_output;
    eep_tally(adc_result_state_machine); /*adc_result_state_machine use this for the average time */
}

/***************************************************************************/
/*    half_step_state_machine();
/*     DESCRIPTION: Generate pulses on PORTD (RD0-RD3)in a specific order   
/*                  to step from one position to another for stepper motor.
/*                  Half step (best precision step angle 3.75�/phase)
/*                                TRUTH TABLE
/*                      _______________________________
/*                      |STEP| A1 | A2 | B1 |B2 | PORTD| 
/*                      ------------------------------- 
/*             START -> | 1  | 0  | 0  | 0  | 1 | 0x01 | 
/*                      -------------------------------  
/*                      | 2  | 0  | 0  | 1  | 1 | 0x03 | 
/*                      -------------------------------  
/*                      | 3  | 0  | 0  | 1  | 0 | 0x02 |                           
/*                      -------------------------------
/*                      | 4  | 0  | 1  | 1  | 0 | 0x06 |
/*                      -------------------------------
/*                      | 5  | 0  | 1  | 0  | 0 | 0x04 |
/*                      -------------------------------  
/*                      | 6  | 1  | 1  | 0  | 0 | 0x0C | 
/*                      -------------------------------  
/*                      | 7  | 1  | 0  | 0  | 0 | 0x08 |                           
/*                      -------------------------------
/*                      | 8  | 1  | 0  | 0  | 1 | 0x09 |
/*                      -------------------------------
/*              END ->  | 1  | 1  | 0  | 1  | 0 | 0x0A |
/*                      -------------------------------
/*
/*
/*                   
/*     INPUTS: NONE
/*     OUPUTS: NONE  
/***************************************************************************/


void half_step_state_machine(void)
{
    static uint8_t step_func_state = E_HALFSTEP_FUNC_MOTOR_OFF;    // was static type
    static uint8_t index;                                          // was static type
    static uint16_t adc_result_state_machine;
    static uint16_t old_pot_val;
    static uint16_t current_pot_val;
           uint8_t pot_changed_flag;
  
    old_pot_val = current_pot_val;                                 // the first reading from last time in state  
    current_pot_val = linear_motor();                              // Get the mostcurrent indexed value
     
    if(current_pot_val == old_pot_val)                             // make the test for equal to see if pot changed
    {  
        pot_changed_flag = FALSE;                                  // they are egual not pot change FALSE = 0
    }
    else
    {
        pot_changed_flag = TRUE;                                   // they are not equal so pot did change TRUE = 1
    }
   
    if (current_pot_val == 0x1388)
    {
        step_func_state = E_STEP_FUNC_MOTOR_OFF;
    }
    else
    {
        adc_result_state_machine = current_pot_val;                 //The time setting from and adc read in
    }

    switch (step_func_state)
    {
        case E_HALFSTEP_FUNC_FORWARD:
        if ((get_timer(TIMER_1) == 0) || (pot_changed_flag == TRUE) ) // check if timer counted down or poteniometer changed   
        {  
            step_func_temp = halfstep_look_up_array[index];                 // Get the array to element zero
            step_func_output = step_func_temp;
            timer_set(TIMER_1, current_pot_val);
            index++;
            step_func_state = E_HALFSTEP_FUNC_FORWARD;
            
            if (RE0 == 1)                                          // Check the switch 
            {
                step_func_state = E_HALFSTEP_FUNC_REVERSE;
            }
            if (index > 7)                                        // restart the whole process  
            {
                index = 0;                                           // reset the sequence       
                step_func_temp = halfstep_look_up_array[index];
            }
            else
            {
                /* Do not shift the LED output. */
            }
        }
        else
        {
           /* Do nothing except remain the same state. */
        }

        break;

        case E_HALFSTEP_FUNC_REVERSE:
        if ((get_timer(TIMER_1) == 0) || (pot_changed_flag == TRUE) ) // check if timer counted down or poteniometer changed  
        { 
            step_func_temp = halfstep_look_up_array[index];                 // Get the array to element 
            step_func_output = step_func_temp;
            timer_set(TIMER_1, current_pot_val);
            step_func_state = E_HALFSTEP_FUNC_REVERSE;
            
            if (RE0 == 0)                                         // check the switch
            {
                step_func_state = E_HALFSTEP_FUNC_FORWARD;
            }  
            if (index > 0)                                        // This will prevent rolling back to 0xFF           
            {
                index--;
            }  
            else if (index == 0)                                  // restart the whole process  THIS else if fixed the missing state 
            {
                index = 7;                                          // But from the top of the array
            }
            else
            {
                /* Do not shift the LED output. */
            }
        }
        else
        {
            /* Do nothing except remain the same state. */
        }

        break;

        case E_HALFSTEP_FUNC_MOTOR_OFF:
        index = 0;
        step_func_output = 0xF0;                           // output everything on PORTD initially
        timer_set(TIMER_1,current_pot_val);                  // Set the timer
        if (RE0 == 0)
        {      
           step_func_state = E_HALFSTEP_FUNC_FORWARD;             // Set for the state machione to go forward off the bat.
        }
        else
        {
           step_func_state = E_HALFSTEP_FUNC_REVERSE;             // Set for the state machione to go reverse off the bat
        }
        break;

     default:
       index = 0;
       step_func_output = 0xF0;                           // output everything on PORTD initially
       timer_set(TIMER_1,current_pot_val);                  // Set the timer
       if (RE0 == 0)
       {      
           step_func_state = E_HALFSTEP_FUNC_FORWARD;             // Set for the state machione to go forward off the bat.
       }
       else
       {
           step_func_state = E_HALFSTEP_FUNC_REVERSE;             // Set for the state machione to go reverse off the bat
       }
       break;
  }

  /* Actions independent of state */
  PORTD = step_func_output;
}
     

