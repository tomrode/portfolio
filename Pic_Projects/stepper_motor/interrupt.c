#include "typedefs.h"
#include "htc.h"
#include "timer.h"
#include "adc_func.h"
void interrupt interrupt_handler(void)
{
  timer_isr();
  adc_isr(); 
}
