/* Includes*/
#include "htc.h"
#include "typedefs.h"
#include "timer.h"
#include "eeprom.h"

/* Constants*/
#define MAX_EEADDR   (254)
#define CLEAR_EEADDR (256)
#define ADDR_ZERO    (0x00)
#define EEP_TIMER    (80)

/* Global to this module Enums*/
enum
{
E_SUMMATION_FOR_AVERAGE_SPEED_STATE_COUNTING,
AVERAGE_SPEED_STATE_COUNTING
}average_speed_count_state_machine;

/* External Variables*/
uint16_t average_result;



/* Functions*/
void eep_tally(uint16_t step_funcout)
{
/* Function Variables*/
static   uint16_t ee_addr;
static uint32_t result_read32_t;
uint8_t  i;
uint8_t  lo_result;
uint8_t  hi_result;
uint8_t  average_speed_count_state_machine;
uint8_t  lo_read;
uint8_t  hi_read; 
uint16_t result_read16_t;

if(get_timer (TIMER_EEPROM) == 0) 
 {
 if (ee_addr < MAX_EEADDR )
  { 
    hi_result = (step_funcout >> 8);
    eeprom_write(ee_addr, hi_result);
    ee_addr++;
    
    lo_result = (step_funcout & 0xFF);
    eeprom_write(ee_addr, lo_result);                                /* write the current step position*/
    ee_addr++;
    
    timer_set(TIMER_EEPROM, EEP_TIMER);
  }
  else
  { 
    hi_result = (step_funcout >> 8);                                  /* Here there is 254 bytes */
    eeprom_write(ee_addr, hi_result);
    ee_addr++;                                                        /* Now 255 bytes */
    
    lo_result = (step_funcout & 0xFF);
    eeprom_write(ee_addr, lo_result);                                 /* write the current step position*/
    ee_addr++;
  /************* SEE IF THE ONE PUSH BUTTON IS PRESSED IF NOT SKIP THIS CODE********************************/ 
     if (RE0 == 1)                                                     /* Using this push button will read after 12.5 seconds*/
       {
       average_speed_count_state_machine = E_SUMMATION_FOR_AVERAGE_SPEED_STATE_COUNTING; /* Get initialized */  
       ee_addr = ADDR_ZERO;
       result_read32_t = 0x00000000; 
      switch(average_speed_count_state_machine)
         {  
           case E_SUMMATION_FOR_AVERAGE_SPEED_STATE_COUNTING:
             while( ee_addr <= 511)
              {
                 hi_read = eeprom_read(ee_addr);                   
                 ee_addr++;
                 lo_read = eeprom_read(ee_addr);
                 result_read16_t = ((hi_read << 8) | lo_read );
                 ee_addr++;
                 result_read32_t = (result_read16_t + result_read32_t);                    /* cummulative count*/
              } 
            average_result = (result_read32_t/256); 
            asm("nop");
             break;
  
          default:
           break; 
        }  
      }   
     else
      {
      } 
  /*************RESUME HERE IF PUSH BUTTON NOT PRESSED*****************************************************/            
    ee_addr = ADDR_ZERO;
    timer_set(TIMER_EEPROM, EEP_TIMER);
  }
 }
 else
  {
  }
 
}