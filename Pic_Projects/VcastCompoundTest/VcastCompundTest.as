opt subtitle "HI-TECH Software Omniscient Code Generator (Lite mode) build 6738"

opt pagewidth 120

	opt lm

	processor	16F887
clrc	macro
	bcf	3,0
	endm
clrz	macro
	bcf	3,2
	endm
setc	macro
	bsf	3,0
	endm
setz	macro
	bsf	3,2
	endm
skipc	macro
	btfss	3,0
	endm
skipz	macro
	btfss	3,2
	endm
skipnc	macro
	btfsc	3,0
	endm
skipnz	macro
	btfsc	3,2
	endm
indf	equ	0
indf0	equ	0
pc	equ	2
pcl	equ	2
status	equ	3
fsr	equ	4
fsr0	equ	4
c	equ	1
z	equ	0
pclath	equ	10
# 7 "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\VcastCompoundTest\main.c"
	psect config,class=CONFIG,delta=2 ;#
# 7 "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\VcastCompoundTest\main.c"
	dw 0x1FFF & 0x3FFF & 0x3FFF & 0x3BFF & 0x3EFF & 0x3FFF & 0x3FFF & 0x3FFF & 0x3FEF & 0x3FF7 & 0x3FFD & 0x3FBF ;#
# 8 "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\VcastCompoundTest\main.c"
	psect config,class=CONFIG,delta=2 ;#
# 8 "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\VcastCompoundTest\main.c"
	dw 0x3FFF ;#
	FNCALL	_main,_Initialize
	FNCALL	_main,_Process1
	FNROOT	_main
	global	_AlarmTimes
psect	strings,class=STRING,delta=2
global __pstrings
__pstrings:
;	global	stringdir,stringtab,__stringbase
stringtab:
;	String table - string pointers are 1 byte each
stringcode:stringdir:
movlw high(stringdir)
movwf pclath
movf fsr,w
incf fsr
	addwf pc
__stringbase:
	retlw	0
psect	strings
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\VcastCompoundTest\main.c"
	line	39
_AlarmTimes:
	retlw	0
	retlw	01h
	retlw	0
	retlw	0
	retlw	0
	retlw	01h
	retlw	0
	retlw	0
	retlw	01h
	retlw	01h
	retlw	01h
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	01h
	retlw	02h
	retlw	02h
	retlw	05h
	retlw	05h
	retlw	0
	retlw	0
	retlw	023h
	retlw	01h
	retlw	03h
	retlw	03h
	retlw	05h
	retlw	05h
	retlw	0
	retlw	0
	retlw	064h
	retlw	01h
	retlw	04h
	retlw	01h
	retlw	032h
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	05h
	retlw	01h
	retlw	05h
	retlw	05h
	retlw	05h
	retlw	05h
	retlw	0
	retlw	0
	retlw	06h
	retlw	01h
	retlw	06h
	retlw	013h
	retlw	019h
	retlw	0C8h
	retlw	0
	retlw	01h
	retlw	07h
	retlw	01h
	retlw	06h
	retlw	013h
	retlw	019h
	retlw	032h
	retlw	0
	retlw	01h
	retlw	08h
	retlw	01h
	retlw	0Ch
	retlw	0Ch
	retlw	0Ch
	retlw	0Ch
	retlw	0
	retlw	01h
	retlw	09h
	retlw	01h
	retlw	064h
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0Ah
	retlw	01h
	retlw	0C8h
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	global	_AlarmTimes
	global	_toms_variable
	global	_InitData
	file	"VcastCompundTest.as"
	line	#
psect cinit,class=CODE,delta=2
global start_initialization
start_initialization:

psect	bssCOMMON,class=COMMON,space=1
global __pbssCOMMON
__pbssCOMMON:
_toms_variable:
       ds      1

psect	bssBANK0,class=BANK0,space=1
global __pbssBANK0
__pbssBANK0:
_InitData:
       ds      7

; Clear objects allocated to COMMON
psect cinit,class=CODE,delta=2
	clrf	((__pbssCOMMON)+0)&07Fh
; Clear objects allocated to BANK0
psect cinit,class=CODE,delta=2
	clrf	((__pbssBANK0)+0)&07Fh
	clrf	((__pbssBANK0)+1)&07Fh
	clrf	((__pbssBANK0)+2)&07Fh
	clrf	((__pbssBANK0)+3)&07Fh
	clrf	((__pbssBANK0)+4)&07Fh
	clrf	((__pbssBANK0)+5)&07Fh
	clrf	((__pbssBANK0)+6)&07Fh
psect cinit,class=CODE,delta=2
global end_of_initialization

;End of C runtime variable initialization code

end_of_initialization:
clrf status
ljmp _main	;jump to C main() function
psect	cstackCOMMON,class=COMMON,space=1
global __pcstackCOMMON
__pcstackCOMMON:
	global	?_Initialize
?_Initialize:	; 0 bytes @ 0x0
	global	?_Process1
?_Process1:	; 0 bytes @ 0x0
	global	??_Process1
??_Process1:	; 0 bytes @ 0x0
	global	?_main
?_main:	; 2 bytes @ 0x0
	global	Initialize@pAlarmObj
Initialize@pAlarmObj:	; 1 bytes @ 0x0
	ds	1
	global	??_Initialize
??_Initialize:	; 0 bytes @ 0x1
	ds	1
	global	Initialize@pSetup
Initialize@pSetup:	; 1 bytes @ 0x2
	ds	1
	global	??_main
??_main:	; 0 bytes @ 0x3
	ds	1
psect	cstackBANK0,class=BANK0,space=1
global __pcstackBANK0
__pcstackBANK0:
	global	Process1@Process1Alarm
Process1@Process1Alarm:	; 8 bytes @ 0x0
	ds	8
	global	Process1@Process1
Process1@Process1:	; 7 bytes @ 0x8
	ds	7
;;Data sizes: Strings 0, constant 88, data 0, bss 8, persistent 0 stack 0
;;Auto spaces:   Size  Autos    Used
;; COMMON          14      4       5
;; BANK0           80     15      22
;; BANK1           80      0       0
;; BANK3           96      0       0
;; BANK2           96      0       0

;;
;; Pointer list with targets:

;; Process1.pAlarm	PTR const struct status size(1) Largest target is 88
;;		 -> AlarmTimes(CODE[88]), 
;;
;; Initialize@pAlarmObj	PTR const struct status size(1) Largest target is 88
;;		 -> AlarmTimes(CODE[88]), 
;;
;; Initialize@pSetup.pAlarm	PTR const struct status size(1) Largest target is 88
;;		 -> AlarmTimes(CODE[88]), 
;;
;; Initialize@pSetup	PTR struct data size(1) Largest target is 8
;;		 -> InitData(BANK0[7]), 
;;
;; S458data$pAlarm	PTR const struct status size(1) Largest target is 88
;;		 -> AlarmTimes(CODE[88]), 
;;
;; InitData.pAlarm	PTR const struct status size(1) Largest target is 88
;;		 -> AlarmTimes(CODE[88]), 
;;


;;
;; Critical Paths under _main in COMMON
;;
;;   _main->_Initialize
;;
;; Critical Paths under _main in BANK0
;;
;;   _main->_Process1
;;
;; Critical Paths under _main in BANK1
;;
;;   None.
;;
;; Critical Paths under _main in BANK3
;;
;;   None.
;;
;; Critical Paths under _main in BANK2
;;
;;   None.

;;
;;Main: autosize = 0, tempsize = 1, incstack = 0, save=0
;;

;;
;;Call Graph Tables:
;;
;; ---------------------------------------------------------------------------------
;; (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;; ---------------------------------------------------------------------------------
;; (0) _main                                                 1     1      0     396
;;                                              3 COMMON     1     1      0
;;                         _Initialize
;;                           _Process1
;; ---------------------------------------------------------------------------------
;; (2) _Process1                                            16    16      0     242
;;                                              0 COMMON     1     1      0
;;                                              0 BANK0     15    15      0
;; ---------------------------------------------------------------------------------
;; (1) _Initialize                                           3     2      1     154
;;                                              0 COMMON     3     2      1
;; ---------------------------------------------------------------------------------
;; Estimated maximum stack depth 2
;; ---------------------------------------------------------------------------------

;; Call Graph Graphs:

;; _main (ROOT)
;;   _Initialize
;;   _Process1
;;

;; Address spaces:

;;Name               Size   Autos  Total    Cost      Usage
;;BITCOMMON            E      0       0       0        0.0%
;;EEDATA             100      0       0       0        0.0%
;;NULL                 0      0       0       0        0.0%
;;CODE                 0      0       0       0        0.0%
;;COMMON               E      4       5       1       35.7%
;;BITSFR0              0      0       0       1        0.0%
;;SFR0                 0      0       0       1        0.0%
;;BITSFR1              0      0       0       2        0.0%
;;SFR1                 0      0       0       2        0.0%
;;STACK                0      0       2       2        0.0%
;;ABS                  0      0      1B       3        0.0%
;;BITBANK0            50      0       0       4        0.0%
;;BITSFR3              0      0       0       4        0.0%
;;SFR3                 0      0       0       4        0.0%
;;BANK0               50      F      16       5       27.5%
;;BITSFR2              0      0       0       5        0.0%
;;SFR2                 0      0       0       5        0.0%
;;BITBANK1            50      0       0       6        0.0%
;;BANK1               50      0       0       7        0.0%
;;BITBANK3            60      0       0       8        0.0%
;;BANK3               60      0       0       9        0.0%
;;BITBANK2            60      0       0      10        0.0%
;;BANK2               60      0       0      11        0.0%
;;DATA                 0      0      1D      12        0.0%

	global	_main
psect	maintext,global,class=CODE,delta=2
global __pmaintext
__pmaintext:

;; *************** function _main *****************
;; Defined at:
;;		line 79 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\VcastCompoundTest\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;                  2  844[COMMON] int 
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 17F/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          1       0       0       0       0
;;      Totals:         1       0       0       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels required when called:    2
;; This function calls:
;;		_Initialize
;;		_Process1
;; This function is called by:
;;		Startup code after reset
;; This function uses a non-reentrant model
;;
psect	maintext
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\VcastCompoundTest\main.c"
	line	79
	global	__size_of_main
	__size_of_main	equ	__end_of_main-_main
	
_main:	
	opt	stack 6
; Regs used in _main: [wreg-fsr0h+status,2+status,0+pclath+cstack]
	line	82
	
l2045:	
;main.c: 82: Initialize(&InitData, &AlarmTimes[3]);
	movlw	((_AlarmTimes-__stringbase)+018h)&0ffh
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(?_Initialize)
	movlw	(_InitData)&0ffh
	fcall	_Initialize
	line	84
;main.c: 84: Process1();
	fcall	_Process1
	goto	l846
	line	87
;main.c: 87: while(1)
	
l845:	
	line	91
;main.c: 88: {
	
l846:	
	line	87
	goto	l846
	
l847:	
	line	92
	
l848:	
	global	start
	ljmp	start
	opt stack 0
GLOBAL	__end_of_main
	__end_of_main:
;; =============== function _main ends ============

	signat	_main,90
	global	_Process1
psect	text91,local,class=CODE,delta=2
global __ptext91
__ptext91:

;; *************** function _Process1 *****************
;; Defined at:
;;		line 112 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\VcastCompoundTest\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  Process1Alar    8    0[BANK0 ] struct status
;;  Process1        7    8[BANK0 ] struct data
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, pclath
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0      15       0       0       0
;;      Temps:          1       0       0       0       0
;;      Totals:         1      15       0       0       0
;;Total ram usage:       16 bytes
;; Hardware stack levels used:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text91
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\VcastCompoundTest\main.c"
	line	112
	global	__size_of_Process1
	__size_of_Process1	equ	__end_of_Process1-_Process1
	
_Process1:	
	opt	stack 6
; Regs used in _Process1: [wreg-fsr0h+status,2+status,0+pclath]
	line	117
	
l2033:	
;main.c: 113: data_t Process1;
;main.c: 114: ALARM_DEFINITIONS_t Process1Alarm;
;main.c: 117: if(InitData.pAlarm->ubNumOfCycles == 2)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(0+_InitData+06h),w
	addlw	01h
	movwf	fsr0
	fcall	stringdir
	xorlw	02h
	skipz
	goto	u2161
	goto	u2160
u2161:
	goto	l2039
u2160:
	line	119
	
l2035:	
;main.c: 118: {
;main.c: 119: Process1.ID = 22;
	movlw	low(016h)
	movwf	(Process1@Process1)
	movlw	high(016h)
	movwf	((Process1@Process1))+1
	line	120
;main.c: 120: Process1.data[0] = 0xCA;
	movlw	(0CAh)
	movwf	(??_Process1+0)+0
	movf	(??_Process1+0)+0,w
	movwf	0+(Process1@Process1)+02h
	line	121
;main.c: 121: Process1.data[1] = 0xFE;
	movlw	(0FEh)
	movwf	(??_Process1+0)+0
	movf	(??_Process1+0)+0,w
	movwf	0+(Process1@Process1)+03h
	line	122
;main.c: 122: Process1.data[2] = 0xFA;
	movlw	(0FAh)
	movwf	(??_Process1+0)+0
	movf	(??_Process1+0)+0,w
	movwf	0+(Process1@Process1)+04h
	line	123
;main.c: 123: Process1.data[3] = 0xCE;
	movlw	(0CEh)
	movwf	(??_Process1+0)+0
	movf	(??_Process1+0)+0,w
	movwf	0+(Process1@Process1)+05h
	line	125
	
l2037:	
;main.c: 125: Process1Alarm.ubNumOfCycles = InitData.pAlarm->ubNumOfCycles;
	movf	(0+_InitData+06h),w
	addlw	01h
	movwf	fsr0
	fcall	stringdir
	movwf	(??_Process1+0)+0
	movf	(??_Process1+0)+0,w
	movwf	0+(Process1@Process1Alarm)+01h
	goto	l2039
	line	126
	
l854:	
	line	129
	
l2039:	
;main.c: 126: }
;main.c: 129: if(InitData.pAlarm->ubNumOfCycles == 3)
	movf	(0+_InitData+06h),w
	addlw	01h
	movwf	fsr0
	fcall	stringdir
	xorlw	03h
	skipz
	goto	u2171
	goto	u2170
u2171:
	goto	l856
u2170:
	line	131
	
l2041:	
;main.c: 130: {
;main.c: 131: Process1.ID = 23;
	movlw	low(017h)
	movwf	(Process1@Process1)
	movlw	high(017h)
	movwf	((Process1@Process1))+1
	line	132
;main.c: 132: Process1.data[0] = 0x11;
	movlw	(011h)
	movwf	(??_Process1+0)+0
	movf	(??_Process1+0)+0,w
	movwf	0+(Process1@Process1)+02h
	line	133
;main.c: 133: Process1.data[1] = 0x22;
	movlw	(022h)
	movwf	(??_Process1+0)+0
	movf	(??_Process1+0)+0,w
	movwf	0+(Process1@Process1)+03h
	line	134
;main.c: 134: Process1.data[2] = 0x33;
	movlw	(033h)
	movwf	(??_Process1+0)+0
	movf	(??_Process1+0)+0,w
	movwf	0+(Process1@Process1)+04h
	line	135
;main.c: 135: Process1.data[3] = 0x44;
	movlw	(044h)
	movwf	(??_Process1+0)+0
	movf	(??_Process1+0)+0,w
	movwf	0+(Process1@Process1)+05h
	line	137
	
l2043:	
;main.c: 137: Process1Alarm.ubNumOfCycles = InitData.pAlarm->ubNumOfCycles;
	movf	(0+_InitData+06h),w
	addlw	01h
	movwf	fsr0
	fcall	stringdir
	movwf	(??_Process1+0)+0
	movf	(??_Process1+0)+0,w
	movwf	0+(Process1@Process1Alarm)+01h
	goto	l856
	line	138
	
l855:	
	line	139
	
l856:	
	return
	opt stack 0
GLOBAL	__end_of_Process1
	__end_of_Process1:
;; =============== function _Process1 ends ============

	signat	_Process1,88
	global	_Initialize
psect	text92,local,class=CODE,delta=2
global __ptext92
__ptext92:

;; *************** function _Initialize *****************
;; Defined at:
;;		line 98 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\VcastCompoundTest\main.c"
;; Parameters:    Size  Location     Type
;;  pSetup          1    wreg     PTR struct data
;;		 -> InitData(7), 
;;  pAlarmObj       1    0[COMMON] PTR const struct status
;;		 -> AlarmTimes(88), 
;; Auto vars:     Size  Location     Type
;;  pSetup          1    2[COMMON] PTR struct data
;;		 -> InitData(7), 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         1       0       0       0       0
;;      Locals:         1       0       0       0       0
;;      Temps:          1       0       0       0       0
;;      Totals:         3       0       0       0       0
;;Total ram usage:        3 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text92
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\VcastCompoundTest\main.c"
	line	98
	global	__size_of_Initialize
	__size_of_Initialize	equ	__end_of_Initialize-_Initialize
	
_Initialize:	
	opt	stack 7
; Regs used in _Initialize: [wregfsr0]
;Initialize@pSetup stored from wreg
	line	100
	movwf	(Initialize@pSetup)
	
l2031:	
;main.c: 100: pSetup->ID = 21;
	movf	(Initialize@pSetup),w
	movwf	fsr0
	movlw	low(015h)
	bcf	status, 7	;select IRP bank0
	movwf	indf
	incf	fsr0,f
	movlw	high(015h)
	movwf	indf
	line	101
;main.c: 101: pSetup->data[0] = 0xDE;
	movlw	(0DEh)
	movwf	(??_Initialize+0)+0
	movf	(Initialize@pSetup),w
	addlw	02h
	movwf	fsr0
	movf	(??_Initialize+0)+0,w
	movwf	indf
	line	102
;main.c: 102: pSetup->data[1] = 0xAD;
	movlw	(0ADh)
	movwf	(??_Initialize+0)+0
	movf	(Initialize@pSetup),w
	addlw	03h
	movwf	fsr0
	movf	(??_Initialize+0)+0,w
	movwf	indf
	line	103
;main.c: 103: pSetup->data[2] = 0xBE;
	movlw	(0BEh)
	movwf	(??_Initialize+0)+0
	movf	(Initialize@pSetup),w
	addlw	04h
	movwf	fsr0
	movf	(??_Initialize+0)+0,w
	movwf	indf
	line	104
;main.c: 104: pSetup->data[3] = 0xEF;
	movlw	(0EFh)
	movwf	(??_Initialize+0)+0
	movf	(Initialize@pSetup),w
	addlw	05h
	movwf	fsr0
	movf	(??_Initialize+0)+0,w
	movwf	indf
	line	105
;main.c: 105: pSetup->pAlarm = pAlarmObj;
	movf	(Initialize@pAlarmObj),w
	movwf	(??_Initialize+0)+0
	movf	(Initialize@pSetup),w
	addlw	06h
	movwf	fsr0
	movf	(??_Initialize+0)+0,w
	movwf	indf
	line	106
	
l851:	
	return
	opt stack 0
GLOBAL	__end_of_Initialize
	__end_of_Initialize:
;; =============== function _Initialize ends ============

	signat	_Initialize,8312
psect	text93,local,class=CODE,delta=2
global __ptext93
__ptext93:
	global	btemp
	btemp set 07Eh

	DABS	1,126,2	;btemp
	global	wtemp0
	wtemp0 set btemp
	end
