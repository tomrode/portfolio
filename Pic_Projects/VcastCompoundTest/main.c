#include <stdio.h>
#include <stdlib.h>
#include "htc.h"
#include "typedefs.h"

/*PIC16F887*/
__CONFIG(DEBUGEN & LVPEN & FCMEN & IESODIS & BORXSLP & DUNPROTECT & UNPROTECT & MCLREN & PWRTEN & WDTDIS & INTCLK & CP);//HS code protected);
__CONFIG(BORV40);


/* STRUCTURES */
typedef enum
{
   RESET_ALARM            =  0u,
   CONTINOUS_ALARM        =  1u,  
   BEEP_BEEP_ALARM        =  2u, 
   BEEP3_ALARM            =  3u, 
   BEEP_ONCE_ALARM        =  4u,
   BEEP2_ONCE_ALARM       =  5u,
   BEEP_STATUS_ALARM      =  6u, 
   BEEP_ALERT_ALARM       =  7u, 
   BEEP_SLOWDOWN_ALARM    =  8u,
   ONE_SEC_ALARM          =  9u, 
   TWO_SEC_ALARM          = 10u
}

typedef struct status
    {
        char ubAlarmType;       // Control id
        char ubNumOfCycles;     // Number of cycles between on & off
        char ubOnTime1;         // Alarm on time length 1
        char ubOffTime1;        // Alarm off time length 1
        char ubOnTime2;         // Alarm on time length 2
        char ubOffTime2;        // Alarm off time length 2
        char ubPauseTime;       // Pause time if repeating pattern
        char fCycleRepeat;     // Repeat Cycle
    }ALARM_DEFINITIONS_t;

 const ALARM_DEFINITIONS_t AlarmTimes[]=
    {   //Alarm Type          Cycle  T1on   T1off  T2on  T2off  TPause  Repeat
        {RESET_ALARM,         1,     0,     0,     0,     1,     0,     0},
        {CONTINOUS_ALARM,     1,     1,     0,     0,     0,     0,     1},
        {BEEP_BEEP_ALARM,     2,     5,     5,     0,     0,    35,     1},
        {BEEP3_ALARM,         3,     5,     5,     0,     0,   100,     1},
        {BEEP_ONCE_ALARM,     1,    50,     0,     0,     0,     0,     0},
        {BEEP2_ONCE_ALARM,    1,     5,     5,     5,     5,     0,     0},
        {BEEP_STATUS_ALARM,   1,     6,    19,    25,   200,     0,     1},
        {BEEP_ALERT_ALARM,    1,     6,    19,    25,    50,     0,     1},
        {BEEP_SLOWDOWN_ALARM, 1,    12,    12,    12,    12,     0,     1},
        {ONE_SEC_ALARM,       1,   100,     0,     0,     0,     0,     0},
        {TWO_SEC_ALARM,       1,   200,     0,     0,     0,     0,     0}
    };

typedef struct data
{
    int ID;
    char data[4];
    const ALARM_DEFINITIONS_t *pAlarm;
}data_t;


/* Function prototypes*/
int main (void);                                   // Initializr  the hard ware
void Initialize(data_t *pSetup, const ALARM_DEFINITIONS_t *pAlarmObj);
void Process1(void);


/* non writeable ram variable location*/
/* Volatile variable*/

/* Local scope variable*/
data_t InitData;

/*********************************************************************/
/*        THE MAIN LOOP                                              */         
/*********************************************************************/

int main (void)
{
    /* Call Initialize */
    
    Initialize(&InitData, &AlarmTimes[3]);
    
    Process1();

    /**- This is like an implied time 10 ms in this case */ 
    while(1)
    {  
      
  
    }
}                                 

/*********************************************************************/
/*        Initialization                                             */         
/*********************************************************************/
void Initialize(data_t *pSetup, const ALARM_DEFINITIONS_t *pAlarmObj)
{  
    /** - Initialization input */
    pSetup->ID = 21;
    pSetup->data[0] = 0xDE;
    pSetup->data[1] = 0xAD;
    pSetup->data[2] = 0xBE;
    pSetup->data[3] = 0xEF; 
    pSetup->pAlarm = pAlarmObj;   
}    
 
/*********************************************************************/
/*        Process1                                                   */         
/*********************************************************************/    
void Process1(void)
{
 data_t Process1;
 ALARM_DEFINITIONS_t Process1Alarm;
 
     /** - Make some decisions from InitData */
     if(InitData.pAlarm->ubNumOfCycles == 2)
     {
        Process1.ID      = 22;
        Process1.data[0] = 0xCA;
        Process1.data[1] = 0xFE;
        Process1.data[2] = 0xFA;
        Process1.data[3] = 0xCE; 

        Process1Alarm.ubNumOfCycles = InitData.pAlarm->ubNumOfCycles;          
     }

     /** - Make some decisions from InitData */
     if(InitData.pAlarm->ubNumOfCycles == 3)
     {
        Process1.ID      = 23;
        Process1.data[0] = 0x11;
        Process1.data[1] = 0x22;
        Process1.data[2] = 0x33;
        Process1.data[3] = 0x44; 

        Process1Alarm.ubNumOfCycles = InitData.pAlarm->ubNumOfCycles;          
     }
}

