
#ifndef REG_CALLBACK
#define REG_CALLBACK

#include "typedefs.h"

/* reg_callback.h */
typedef void (*callback_t)(void);

//void register_callback(void (*callback_t)(void), int x);
void register_callback(void (*callback_t)(void));  
#endif
