#include "func2.h"
#include "func1.h"


#define IMAP_TCM_Implausible(x)           (IMAP_ImplausibilityControl.status_c_transmission_implausible = x)
#define IMAP_DCM_A1_Implausible(x)        (IMAP_ImplausibilityControl.dtcm_a1_implausible               = x)
#define IMAP_DCM_STATUS_C_Implausible(x)  (IMAP_ImplausibilityControl.status_c_dtcm_implausible         = x)
#define IMAP_ETM_LTM_Implausible(x)       (IMAP_ImplausibilityControl.tgw_a1_implausible                = x)

/* Global Vars */
IMAP_ImplausibilityControl_t IMAP_ImplausibilityControl;

/* CONSTANTS */
const NETM_msg_cfg_t NETM_msg_cfg[5] = {
/* uint16 BitMsgPos uint8NodeIdent     uint8MsgType       uint8 Fctptr(uint8)*/
  {0xDEAD,          0x08,              0x99,              incrementFunc1}, 
  {0xBEEF,          0x0C,              0xED,              decrementFunc1},
  {0xFEED,          0x0A,              0x09,              squareFunc1},
  {0xBEAD,          0x03,              0xDE,              cubedFunc1},
  {0xFADE,          0x94,              0xFF,              NETM_IMAP_DCM_STATUS_C_Implausible},
};

/****************************************************************************************************
/NAME: FuncPtr2()
/ROLE: This function called from main demonstrates the power of the function pointer.
/STEP. 1:Define the type somewhere, in this case it was defined in func1.h as "NETM_msg_cfg_t" 
/        with these attributes.
/  typedef struct
/  {
/    uint16_t BitMsgPos;                     //position of message timeout in MsgTimeOut 
/    uint8_t NodeIdent;                      //Node id (owner of message in timeout) 
/    uint8_t MsgType;                        //message type 
/    uint8_t(*MsgMissRecoveryFunc)(uint8_t); //Recovery Callback function ptr 
/  }NETM_msg_cfg_t;
/STEP. 2: Most of the time there is a constant involved with the function pointer. Therefor the constant
/         has to be of the same type as the main overall type-def. This constant is the inputs and the Function list
/          
/STEP. 3: In the function where the process of using the function pointer scheme is used The steps are as follows.
/          A.Declare a pointer of the same type as the function pointer.     
/          B.Make the pointer Deference to the constant for example: NETM_msg_cfgLocal_ptr = (NETM_msg_cfg_t *)&NETM_msg_cfg[i]
/          C.Once done then deref the intems in the constant to place value and then 
/          D.The function call can be used ad in this case it will take in the argument.
/            NETM_MsgMissRecoveryFunc = (*NETM_msg_cfg[i].MsgMissRecoveryFunc)(NETM_NodeNetId); 
/STEP. 4:  Please note that the function this takes place will show the variables involved will change since they         
/          were dereferneced and note the NETM_MsgMissRecoveryFunc will take and use func pointer with the NETM_NodeNetId
/          argument. At the end please watch the last or fifth indexed line from constant array will execute the function
/          Which calls the macro associated with it and pass the original pointer function argument.   
/
/****************************************************************************************************/
void FunctPtr2(void)
{
 /* Local var's*/
  uint8_t                i; 
  uint8_t                NETM_NodeNetId;
  uint8_t                NETM_MsgMissRecoveryFunc;
  uint8_t                NETM_MsgType;
  uint16_t               NETM_BitMsgPos;
  NETM_msg_cfg_t         *NETM_msg_cfgLocal_ptr;

  NETM_msg_cfg_t         NETM_msg_cfg_TestVar;


  NETM_IMAP_ETM_LTM_Implausible(0x01);
  for(i = 0; i<=5; i++)
  {  
    NETM_msg_cfgLocal_ptr = (NETM_msg_cfg_t *)&NETM_msg_cfg[i];  
    /* Deref prt*/
    NETM_NodeNetId = NETM_msg_cfgLocal_ptr->NodeIdent;                    
    NETM_BitMsgPos = NETM_msg_cfgLocal_ptr->BitMsgPos;
    NETM_MsgType   = NETM_msg_cfgLocal_ptr->MsgType;
    NETM_MsgMissRecoveryFunc = (*NETM_msg_cfg[i].MsgMissRecoveryFunc)(NETM_NodeNetId);
    asm("nop");
  }  
  
  /** More excersise with the pointer to call different functions*/ 
  NETM_msg_cfgLocal_ptr = (NETM_msg_cfg_t *)&NETM_msg_cfg_TestVar;
  NETM_msg_cfg_TestVar.BitMsgPos = 0x1010;
  NETM_msg_cfg_TestVar.NodeIdent = 2;
  NETM_msg_cfg_TestVar.MsgType = 26;
  NETM_msg_cfg_TestVar.MsgMissRecoveryFunc = squareFunc1(NETM_msg_cfg_TestVar.NodeIdent);
}

uint8_t incrementFunc2(uint8_t x)
{
  return (x+1);
}

uint8_t decrementFunc2(uint8_t x)
{
  return (x-1);
}

void NETM_IMAP_TCM_Implausible( bool_t Status )
{
  IMAP_TCM_Implausible(Status);  
}

void NETM_IMAP_DCM_A1_Implausible( bool_t Status )
{
  IMAP_DCM_A1_Implausible(Status);  
}
void NETM_IMAP_DCM_STATUS_C_Implausible( bool_t Status )
{
  IMAP_DCM_STATUS_C_Implausible(Status);  
}
void NETM_IMAP_ETM_LTM_Implausible( bool_t Status )
{
   IMAP_ETM_LTM_Implausible(Status); 
}