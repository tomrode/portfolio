#include "stdio.h"
#include "stdlib.h"
#include "typedefs.h"
#include "func1.h"
#include "func2.h"
#include "reg_callback.h"

#define FUNCTION_POINTER_ARRAY
#ifdef FUNCTION_POINTER_ARRAY


/********************************************************************/
/* Type definitions                                                 */
/********************************************************************/
/* function pointers */
typedef uint8_t(*T_FUNCPTR_1)(uint8_t);

typedef int(*T_FUNCTPTR_2)(int);
 
typedef struct
           {
             int dtc_ar1[2];
             int dtc_ar2[2];
             int dtc_ar3[2];
           }DTC_STRUC;

/********************************************************************/
/* Function prototypes                                              */
/********************************************************************/
void copy(int *dtc_structure_ptr); 
void main(void);
void copy_structure_as_parameter(DTC_STRUC dtc_var); 
//int another_ptr_func (int h,int (*func_ptr)(int));
uint8_t increment(uint8_t x);
uint8_t decrement(uint8_t x);
uint8_t square(uint8_t x);
uint8_t cubed(uint8_t x);
void my_callback(void);                

/********************************************************************/

/* Constant Definitions                                             */
/********************************************************************/

const T_FUNCPTR_1 func_array[] =
{
   increment,
   decrement, 
   square,
   cubed
};

const T_FUNCTPTR_2 func_array2[] = 
{
   increment,
   decrement 
};

const int TWODARRAY [2][2] = {
                               {12,13},
                               {14,15}
                             };           
/********************************************************************/
/* Function definitions                                             */
/********************************************************************/

/* callback function definition goes here */
void my_callback(void)
{
    asm("nop");
    //printf("inside my_callback\n");
}


void main(void)

{
  /*local vars and types*/
  uint8_t a;
  uint8_t b;
  uint8_t c,d;
  uint8_t e;
  //uint8_t (*ptrFunc)(uint8_t,uint8_t); 
  T_FUNCPTR_1 ptrFunc;
  DTC_STRUC test_dtc;
  DTC_STRUC test_dtc_copy;
 
  /* Function pointer type*/
 
 // EXAMPLE2_FUNCTION_POINTER_t  *EXAMPLE_FUNCTION_POINTER;
  /* initialize the structure with data*/
  
//  EXAMPLE_FUNCTION_POINTER = (EXAMPLE2_FUNCTION_POINTER_t *)&func_array2[0];   /* example from BCM netm function*/

  /* assigning ptrFunc to func's address */ 
  ptrFunc = &cubed;   
  e = ptrFunc(3); 
  e = (*func_array2[INCREMENT])(2);

  /** - Internet example */
  /**- Good working example of a call back function*/
  register_callback(&my_callback);       

  /** - Complex example */
  FunctPtr2();
  
  test_dtc.dtc_ar1[0] = 0xBB;
  test_dtc.dtc_ar1[1] = 0xAA;
  test_dtc.dtc_ar2[0] = 0xDD;
  test_dtc.dtc_ar2[1] = 0xBB;
  test_dtc.dtc_ar3[0] = 0xEE;
  test_dtc.dtc_ar3[1] = 0xFF;


 /* copy one structure to another*/
  test_dtc_copy = test_dtc;
 
  /* send this out to the function pointer method */
   copy(&test_dtc.dtc_ar1[0]); 
  /* Send this out to a function passing a structure*/  
  /* THis takes some time just to pass a small structure*/  
  copy_structure_as_parameter(test_dtc);
 
  
  c = 3;
 // d = (&square)(c);
 // d = another_ptr_func (c,&square);

  a = 5;
  b = (*func_array[0])(a);
  //printf("%f + 1 = %f \n",a,b);

  a = 66;
  b = (*func_array[1])(a);
 // printf("%f - 1 = %f \n",a,b);

  a = 3;
  b = (*func_array[2])(a);
 // printf("%f ^ 2 = %f \n",a,b);

  a = 3;
  b = (*func_array[3])(a);
 // printf("1 / %f = %f \n",a,b);
}



void copy(int *dtc_structure_ptr)
{
    /* Type definition*/
    DTC_STRUC from_main_copy;
    int i;  
    for (i=0; i<=5; i++)
      {
        from_main_copy.dtc_ar1[i] = *dtc_structure_ptr;
        dtc_structure_ptr++;
      }  
  asm("nop");

}

//int another_ptr_func (int h,int(*func_ptr)(int))
//{
//  int result;
//  result = func_ptr(h);
//  return  result;
//}

void copy_structure_as_parameter(DTC_STRUC dtc_var)
{
  DTC_STRUC yet_another;
  asm("nop");
  yet_another = dtc_var;
 
} 

uint8_t increment(uint8_t x)
{
  return (x+1);
}

uint8_t decrement(uint8_t x)
{
  return (x-1);
}

uint8_t square(uint8_t x)
{
  return (x*x);
}

uint8_t cubed(uint8_t x)
{
  return (x*x*x);
}
#endif

