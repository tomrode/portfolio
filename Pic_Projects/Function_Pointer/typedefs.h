/*****************************************************************************/
/* FILE NAME:  typedefs.h                                                    */
/*                                                                           */
/* Description:  Standard type definitions.                                  */
/*****************************************************************************/

#ifndef TYPEDEFS_H
#define TYPEDEFS_H

enum
{
FALSE,                       // implied to be zero
TRUE,                        // implied to be one
DEFAULT
};

typedef signed char sint8_t;
typedef unsigned char uint8_t;
typedef uint8_t bool_t;
typedef volatile signed char vsint8_t;
typedef volatile unsigned char vuint8_t;

typedef signed short sint16_t;
typedef unsigned short uint16_t;
typedef volatile signed short vsint16_t;
typedef volatile unsigned short vuint16_t;

typedef signed long sint32_t;
typedef unsigned long uint32_t;
typedef volatile signed long vsint32_t;
typedef volatile unsigned long vuint32_t;

/******************************************************************/
/* Global variables to entire project                             */
/*                                                                */
/*                                                                */       
/******************************************************************/

volatile uint8_t toms_variable;
extern uint8_t __resetbits;

#endif
