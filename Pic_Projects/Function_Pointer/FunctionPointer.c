/****************************************************************************************************
/*
/*            30 JAN 2013  Function Pointer further example
/*            31 JAN 2013  Expanded a bit further with another function pointer and Macro
/***************************************************************************************************/
#include "stdlib.h"

/*type definitions*/
typedef int(*T_FUNCPTR_1)(int);
typedef enum
{
  INCREMENT,
  DECREMENT,
  SQUARE,
  CUBED
}MATH;

/*MACRO's*/
#define INCREMENT_BYONE(x)   (increment(x)) 

/*Function prototypes*/
int increment(int x);
int decrement(int x);
int square(int x);
int cubed(int x);
int(*FUNC_PTR2)(int);
void main(void);

/*Constant Definitions */
const T_FUNCPTR_1 func_array[] =
{
   increment,
   decrement,
   square,
   cubed
};

/*FUNCTIONS*/

int increment(int x)
{ return (x + 1); }

int decrement(int x)
{ return (x - 1); }

int square(int x)
{ return (x * x); }

int cubed(int x)
{ return (x*x*x); }

/*Main loop*/
void main(void)
{
 int a,b;

while(1)
{
   a = 5;
   b = (*func_array[INCREMENT])(a);
   b = 0;
   b = (*func_array[DECREMENT])(a);
   b = 0;
   b = (*func_array[SQUARE])(a);
   b = 0;
   b = (*func_array[CUBED])(a);
   b = 0;
   b = (&square)(a);          /* To me its more intutive to pass the address of the the function desired*/
   a = 55;
   b =  INCREMENT_BYONE(a); 
   b = 0;
 }
}
 