opt subtitle "HI-TECH Software Omniscient Code Generator (Lite mode) build 6738"

opt pagewidth 120

	opt lm

	processor	16F887
clrc	macro
	bcf	3,0
	endm
clrz	macro
	bcf	3,2
	endm
setc	macro
	bsf	3,0
	endm
setz	macro
	bsf	3,2
	endm
skipc	macro
	btfss	3,0
	endm
skipz	macro
	btfss	3,2
	endm
skipnc	macro
	btfsc	3,0
	endm
skipnz	macro
	btfsc	3,2
	endm
indf	equ	0
indf0	equ	0
pc	equ	2
pcl	equ	2
status	equ	3
fsr	equ	4
fsr0	equ	4
c	equ	1
z	equ	0
pclath	equ	10
	FNCALL	_main,_cubed
	FNCALL	_main,_decrement
	FNCALL	_main,_increment
	FNCALL	_main,_register_callback
	FNCALL	_main,_FunctPtr2
	FNCALL	_main,_copy
	FNCALL	_main,_copy_structure_as_parameter
	FNCALL	_main,_square
	FNCALL	_FunctPtr2,_NETM_IMAP_ETM_LTM_Implausible
	FNCALL	_FunctPtr2,___bmul
	FNCALL	_FunctPtr2,Fake
	FNCALL	_FunctPtr2,_NETM_IMAP_DCM_STATUS_C_Implausi
	FNCALL	_FunctPtr2,_cubedFunc1
	FNCALL	_FunctPtr2,_squareFunc1
	FNCALL	_FunctPtr2,_decrementFunc1
	FNCALL	_FunctPtr2,_incrementFunc1
	FNCALL	_cubedFunc1,___bmul
	FNCALL	_squareFunc1,___bmul
	FNCALL	_cubed,___bmul
	FNCALL	_square,___bmul
	FNCALL	_register_callback,_my_callback
	FNROOT	_main
	global	_TWODARRAY
psect	strings,class=STRING,delta=2
global __pstrings
__pstrings:
;	global	stringdir,stringtab,__stringbase
stringtab:
;	String table - string pointers are 1 byte each
	btfsc	(btemp+1),7
	ljmp	stringcode
	bcf	status,7
	btfsc	(btemp+1),0
	bsf	status,7
	movf	indf,w
	incf fsr
skipnz
incf btemp+1
	return
stringcode:stringdir:
movlw high(stringdir)
movwf pclath
movf fsr,w
incf fsr
	addwf pc
__stringbase:
	retlw	0
psect	strings
	file	"C:\Users\t0028919\OneDrive - Crown Equipment Corporation\User_Data\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Function_Pointer\main.c"
	line	59
_TWODARRAY:
	retlw	0Ch
	retlw	0

	retlw	0Dh
	retlw	0

	retlw	0Eh
	retlw	0

	retlw	0Fh
	retlw	0

	global	_func_array
psect	strings
	file	"C:\Users\t0028919\OneDrive - Crown Equipment Corporation\User_Data\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Function_Pointer\main.c"
	line	45
_func_array:
	retlw	(fp__increment-fpbase)&0ffh
	retlw	(fp__decrement-fpbase)&0ffh
	retlw	(fp__square-fpbase)&0ffh
	retlw	(fp__cubed-fpbase)&0ffh
	global	_func_array2
psect	strings
	file	"C:\Users\t0028919\OneDrive - Crown Equipment Corporation\User_Data\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Function_Pointer\main.c"
	line	53
_func_array2:
	retlw	(fp__increment-fpbase)&0ffh
	retlw	(fp__decrement-fpbase)&0ffh
	global	_NETM_msg_cfg
psect	strings
	file	"C:\Users\t0028919\OneDrive - Crown Equipment Corporation\User_Data\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Function_Pointer\func2.c"
	line	14
_NETM_msg_cfg:
	retlw	0ADh
	retlw	0DEh

	retlw	08h
	retlw	099h
	retlw	(fp__incrementFunc1-fpbase)&0ffh
	retlw	0EFh
	retlw	0BEh

	retlw	0Ch
	retlw	0EDh
	retlw	(fp__decrementFunc1-fpbase)&0ffh
	retlw	0EDh
	retlw	0FEh

	retlw	0Ah
	retlw	09h
	retlw	(fp__squareFunc1-fpbase)&0ffh
	retlw	0ADh
	retlw	0BEh

	retlw	03h
	retlw	0DEh
	retlw	(fp__cubedFunc1-fpbase)&0ffh
	retlw	0DEh
	retlw	0FAh

	retlw	094h
	retlw	0FFh
	retlw	(fp__NETM_IMAP_DCM_STATUS_C_Implausi-fpbase)&0ffh
	global	_TWODARRAY
	global	_func_array
	global	_func_array2
	global	_NETM_msg_cfg
	global	_IMAP_ImplausibilityControl
	global	_toms_variable
	file	"Function_Pointer.as"
	line	#
psect cinit,class=CODE,delta=2
global start_initialization
start_initialization:

psect	bssCOMMON,class=COMMON,space=1
global __pbssCOMMON
__pbssCOMMON:
_IMAP_ImplausibilityControl:
       ds      1

_toms_variable:
       ds      1

; Clear objects allocated to COMMON
psect cinit,class=CODE,delta=2
	clrf	((__pbssCOMMON)+0)&07Fh
	clrf	((__pbssCOMMON)+1)&07Fh
psect cinit,class=CODE,delta=2
global end_of_initialization

;End of C runtime variable initialization code

end_of_initialization:
clrf status
ljmp _main	;jump to C main() function
psect	cstackCOMMON,class=COMMON,space=1
global __pcstackCOMMON
__pcstackCOMMON:
	global	?_register_callback
?_register_callback:	; 0 bytes @ 0x0
	global	??_register_callback
??_register_callback:	; 0 bytes @ 0x0
	global	?_FunctPtr2
?_FunctPtr2:	; 0 bytes @ 0x0
	global	?_copy
?_copy:	; 0 bytes @ 0x0
	global	??_copy
??_copy:	; 0 bytes @ 0x0
	global	?_copy_structure_as_parameter
?_copy_structure_as_parameter:	; 0 bytes @ 0x0
	global	?_NETM_IMAP_ETM_LTM_Implausible
?_NETM_IMAP_ETM_LTM_Implausible:	; 0 bytes @ 0x0
	global	??_NETM_IMAP_ETM_LTM_Implausible
??_NETM_IMAP_ETM_LTM_Implausible:	; 0 bytes @ 0x0
	global	?_my_callback
?_my_callback:	; 0 bytes @ 0x0
	global	??_my_callback
??_my_callback:	; 0 bytes @ 0x0
	global	?_main
?_main:	; 0 bytes @ 0x0
	global	?___bmul
?___bmul:	; 1 bytes @ 0x0
	global	NETM_IMAP_ETM_LTM_Implausible@Status
NETM_IMAP_ETM_LTM_Implausible@Status:	; 1 bytes @ 0x0
	global	register_callback@callback_t
register_callback@callback_t:	; 1 bytes @ 0x0
	global	___bmul@multiplicand
___bmul@multiplicand:	; 1 bytes @ 0x0
	global	copy_structure_as_parameter@dtc_var
copy_structure_as_parameter@dtc_var:	; 12 bytes @ 0x0
	ds	1
	global	??___bmul
??___bmul:	; 0 bytes @ 0x1
	ds	1
	global	___bmul@product
___bmul@product:	; 1 bytes @ 0x2
	ds	1
	global	___bmul@multiplier
___bmul@multiplier:	; 1 bytes @ 0x3
	ds	1
	global	?_NETM_IMAP_DCM_STATUS_C_Implausi
?_NETM_IMAP_DCM_STATUS_C_Implausi:	; 0 bytes @ 0x4
	global	?_increment
?_increment:	; 1 bytes @ 0x4
	global	?_decrement
?_decrement:	; 1 bytes @ 0x4
	global	?_square
?_square:	; 1 bytes @ 0x4
	global	?_cubed
?_cubed:	; 1 bytes @ 0x4
	global	increment@x
increment@x:	; 1 bytes @ 0x4
	global	decrement@x
decrement@x:	; 1 bytes @ 0x4
	global	square@x
square@x:	; 1 bytes @ 0x4
	global	cubed@x
cubed@x:	; 1 bytes @ 0x4
	global	incrementFunc1@x
incrementFunc1@x:	; 1 bytes @ 0x4
	global	decrementFunc1@x
decrementFunc1@x:	; 1 bytes @ 0x4
	global	squareFunc1@x
squareFunc1@x:	; 1 bytes @ 0x4
	global	cubedFunc1@x
cubedFunc1@x:	; 1 bytes @ 0x4
	global	NETM_IMAP_DCM_STATUS_C_Implausi@Status
NETM_IMAP_DCM_STATUS_C_Implausi@Status:	; 1 bytes @ 0x4
	ds	1
	global	??_increment
??_increment:	; 0 bytes @ 0x5
	global	??_decrement
??_decrement:	; 0 bytes @ 0x5
	global	??_square
??_square:	; 0 bytes @ 0x5
	global	??_cubed
??_cubed:	; 0 bytes @ 0x5
	global	??_NETM_IMAP_DCM_STATUS_C_Implausi
??_NETM_IMAP_DCM_STATUS_C_Implausi:	; 0 bytes @ 0x5
	global	??_incrementFunc1
??_incrementFunc1:	; 0 bytes @ 0x5
	global	??_decrementFunc1
??_decrementFunc1:	; 0 bytes @ 0x5
	global	??_squareFunc1
??_squareFunc1:	; 0 bytes @ 0x5
	global	??_cubedFunc1
??_cubedFunc1:	; 0 bytes @ 0x5
	ds	2
	global	_cubed$550
_cubed$550:	; 1 bytes @ 0x7
	global	_cubedFunc1$551
_cubedFunc1$551:	; 1 bytes @ 0x7
	ds	1
	global	??_FunctPtr2
??_FunctPtr2:	; 0 bytes @ 0x8
	ds	4
psect	cstackBANK0,class=BANK0,space=1
global __pcstackBANK0
__pcstackBANK0:
	global	??_copy_structure_as_parameter
??_copy_structure_as_parameter:	; 0 bytes @ 0x0
	global	FunctPtr2@NETM_BitMsgPos
FunctPtr2@NETM_BitMsgPos:	; 2 bytes @ 0x0
	global	copy@from_main_copy
copy@from_main_copy:	; 12 bytes @ 0x0
	ds	2
	global	FunctPtr2@NETM_MsgMissRecoveryFunc
FunctPtr2@NETM_MsgMissRecoveryFunc:	; 1 bytes @ 0x2
	ds	1
	global	FunctPtr2@NETM_MsgType
FunctPtr2@NETM_MsgType:	; 1 bytes @ 0x3
	ds	1
	global	_FunctPtr2$576
_FunctPtr2$576:	; 1 bytes @ 0x4
	global	copy_structure_as_parameter@yet_another
copy_structure_as_parameter@yet_another:	; 12 bytes @ 0x4
	ds	1
	global	FunctPtr2@NETM_NodeNetId
FunctPtr2@NETM_NodeNetId:	; 1 bytes @ 0x5
	ds	1
	global	FunctPtr2@NETM_msg_cfgLocal_ptr
FunctPtr2@NETM_msg_cfgLocal_ptr:	; 2 bytes @ 0x6
	ds	2
	global	FunctPtr2@i
FunctPtr2@i:	; 1 bytes @ 0x8
	ds	1
	global	FunctPtr2@NETM_msg_cfg_TestVar
FunctPtr2@NETM_msg_cfg_TestVar:	; 5 bytes @ 0x9
	ds	3
	global	copy@dtc_structure_ptr
copy@dtc_structure_ptr:	; 1 bytes @ 0xC
	ds	1
	global	copy@i
copy@i:	; 2 bytes @ 0xD
	ds	3
	global	??_main
??_main:	; 0 bytes @ 0x10
	ds	4
	global	main@test_dtc_copy
main@test_dtc_copy:	; 12 bytes @ 0x14
	ds	12
	global	main@c
main@c:	; 1 bytes @ 0x20
	ds	1
	global	main@e
main@e:	; 1 bytes @ 0x21
	ds	1
	global	main@b
main@b:	; 1 bytes @ 0x22
	ds	1
	global	main@ptrFunc
main@ptrFunc:	; 1 bytes @ 0x23
	ds	1
	global	main@a
main@a:	; 1 bytes @ 0x24
	ds	1
	global	main@test_dtc
main@test_dtc:	; 12 bytes @ 0x25
	ds	12
;;Data sizes: Strings 0, constant 39, data 0, bss 2, persistent 0 stack 0
;;Auto spaces:   Size  Autos    Used
;; COMMON          14     12      14
;; BANK0           80     49      49
;; BANK1           80      0       0
;; BANK3           96      0       0
;; BANK2           96      0       0

;;
;; Pointer list with targets:

;; FunctPtr2$576	const PTR FTN(unsigned char ,)unsigned char  size(1) Largest target is 1
;;		 -> Absolute function(), NETM_IMAP_DCM_STATUS_C_Implausi(), cubedFunc1(), squareFunc1(), 
;;		 -> decrementFunc1(), incrementFunc1(), 
;;
;; register_callback@callback_t	PTR FTN()void  size(1) Largest target is 0
;;		 -> my_callback(), 
;;
;; FunctPtr2@NETM_msg_cfgLocal_ptr.MsgMissRecoveryFunc	PTR FTN(unsigned char ,)unsigned char  size(1) Largest target is 1
;;		 -> Absolute function(), NETM_IMAP_DCM_STATUS_C_Implausi(), cubedFunc1(), squareFunc1(), 
;;		 -> decrementFunc1(), incrementFunc1(), 
;;
;; FunctPtr2@NETM_msg_cfgLocal_ptr	PTR struct . size(2) Largest target is 25
;;		 -> FunctPtr2@NETM_msg_cfg_TestVar(BANK0[5]), NETM_msg_cfg(CODE[25]), 
;;
;; NETM_msg_cfg_TestVar.MsgMissRecoveryFunc	PTR FTN(unsigned char ,)unsigned char  size(1) Largest target is 1
;;		 -> Absolute function(), NETM_IMAP_DCM_STATUS_C_Implausi(), cubedFunc1(), squareFunc1(), 
;;		 -> decrementFunc1(), incrementFunc1(), 
;;
;; S166$MsgMissRecoveryFunc	PTR FTN(unsigned char ,)unsigned char  size(1) Largest target is 1
;;		 -> Absolute function(), NETM_IMAP_DCM_STATUS_C_Implausi(), cubedFunc1(), squareFunc1(), 
;;		 -> decrementFunc1(), incrementFunc1(), 
;;
;; NETM_msg_cfg.MsgMissRecoveryFunc	PTR FTN(unsigned char ,)unsigned char  size(1) Largest target is 1
;;		 -> Absolute function(), NETM_IMAP_DCM_STATUS_C_Implausi(), cubedFunc1(), squareFunc1(), 
;;		 -> decrementFunc1(), incrementFunc1(), 
;;
;; copy@dtc_structure_ptr	PTR int  size(1) Largest target is 12
;;		 -> main@test_dtc(BANK0[12]), 
;;
;; main@ptrFunc	PTR FTN(unsigned char ,)unsigned char  size(1) Largest target is 1
;;		 -> cubed(), 
;;
;; func_array2	const PTR FTN(int ,)int [2] size(1) Largest target is 1
;;		 -> decrement(), increment(), 
;;
;; func_array	const PTR FTN(unsigned char ,)unsigned char [4] size(1) Largest target is 1
;;		 -> cubed(), square(), decrement(), increment(), 
;;


;;
;; Critical Paths under _main in COMMON
;;
;;   _main->_copy_structure_as_parameter
;;   _FunctPtr2->_cubedFunc1
;;   _cubedFunc1->___bmul
;;   _squareFunc1->___bmul
;;   _cubed->___bmul
;;   _square->___bmul
;;   _NETM_IMAP_DCM_STATUS_C_Implausi->___bmul
;;   _decrementFunc1->___bmul
;;   _incrementFunc1->___bmul
;;
;; Critical Paths under _main in BANK0
;;
;;   _main->_copy_structure_as_parameter
;;
;; Critical Paths under _main in BANK1
;;
;;   None.
;;
;; Critical Paths under _main in BANK3
;;
;;   None.
;;
;; Critical Paths under _main in BANK2
;;
;;   None.

;;
;;Main: autosize = 0, tempsize = 4, incstack = 0, save=0
;;

;;
;;Call Graph Tables:
;;
;; ---------------------------------------------------------------------------------
;; (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;; ---------------------------------------------------------------------------------
;; (0) _main                                                34    34      0    1649
;;                                             16 BANK0     33    33      0
;;                              _cubed
;;                          _decrement
;;                          _increment
;;                  _register_callback
;;                          _FunctPtr2
;;                               _copy
;;        _copy_structure_as_parameter
;;                             _square
;; ---------------------------------------------------------------------------------
;; (1) _FunctPtr2                                           16    16      0     819
;;                                              8 COMMON     2     2      0
;;                                              0 BANK0     14    14      0
;;      _NETM_IMAP_ETM_LTM_Implausible
;;                             ___bmul
;;                   Absolute function
;;    _NETM_IMAP_DCM_STATUS_C_Implausi
;;                         _cubedFunc1
;;                        _squareFunc1
;;                     _decrementFunc1
;;                     _incrementFunc1
;; ---------------------------------------------------------------------------------
;; (2) _cubedFunc1                                           4     3      1     181
;;                                              4 COMMON     4     3      1
;;                             ___bmul
;; ---------------------------------------------------------------------------------
;; (2) _squareFunc1                                          2     1      1     136
;;                                              4 COMMON     2     1      1
;;                             ___bmul
;; ---------------------------------------------------------------------------------
;; (1) _cubed                                                4     3      1     181
;;                                              4 COMMON     4     3      1
;;                             ___bmul
;; ---------------------------------------------------------------------------------
;; (1) _square                                               2     1      1     136
;;                                              4 COMMON     2     1      1
;;                             ___bmul
;; ---------------------------------------------------------------------------------
;; (1) _register_callback                                    1     1      0      22
;;                                              0 COMMON     1     1      0
;;                        _my_callback
;; ---------------------------------------------------------------------------------
;; (2) ___bmul                                               4     3      1      92
;;                                              0 COMMON     4     3      1
;; ---------------------------------------------------------------------------------
;; (2) _NETM_IMAP_DCM_STATUS_C_Implausi                      1     0      1      22
;;                                              4 COMMON     1     0      1
;;                             ___bmul (ARG)
;; ---------------------------------------------------------------------------------
;; (2) Absolute function(Fake)                               1     0      1       0
;;                                              0 COMMON     1     0      1
;; ---------------------------------------------------------------------------------
;; (2) _NETM_IMAP_ETM_LTM_Implausible                        1     1      0      22
;;                                              0 COMMON     1     1      0
;; ---------------------------------------------------------------------------------
;; (2) _decrementFunc1                                       1     0      1      22
;;                                              4 COMMON     1     0      1
;;                             ___bmul (ARG)
;; ---------------------------------------------------------------------------------
;; (2) _incrementFunc1                                       1     0      1      22
;;                                              4 COMMON     1     0      1
;;                             ___bmul (ARG)
;; ---------------------------------------------------------------------------------
;; (1) _decrement                                            1     0      1      22
;;                                              4 COMMON     1     0      1
;; ---------------------------------------------------------------------------------
;; (1) _increment                                            1     0      1      22
;;                                              4 COMMON     1     0      1
;; ---------------------------------------------------------------------------------
;; (1) _copy_structure_as_parameter                         28    16     12      23
;;                                              0 COMMON    12     0     12
;;                                              0 BANK0     16    16      0
;; ---------------------------------------------------------------------------------
;; (1) _copy                                                18    18      0     114
;;                                              0 COMMON     3     3      0
;;                                              0 BANK0     15    15      0
;; ---------------------------------------------------------------------------------
;; (2) _my_callback                                          0     0      0       0
;; ---------------------------------------------------------------------------------
;; Estimated maximum stack depth 2
;; ---------------------------------------------------------------------------------

;; Call Graph Graphs:

;; _main (ROOT)
;;   _cubed
;;     ___bmul
;;   _decrement
;;   _increment
;;   _register_callback
;;     _my_callback
;;   _FunctPtr2
;;     _NETM_IMAP_ETM_LTM_Implausible
;;     ___bmul
;;     Absolute function(Fake)
;;     _NETM_IMAP_DCM_STATUS_C_Implausi
;;       ___bmul (ARG)
;;     _cubedFunc1
;;       ___bmul
;;     _squareFunc1
;;       ___bmul
;;     _decrementFunc1
;;       ___bmul (ARG)
;;     _incrementFunc1
;;       ___bmul (ARG)
;;   _copy
;;   _copy_structure_as_parameter
;;   _square
;;     ___bmul
;;

;; Address spaces:

;;Name               Size   Autos  Total    Cost      Usage
;;BITCOMMON            E      0       0       0        0.0%
;;EEDATA             100      0       0       0        0.0%
;;NULL                 0      0       0       0        0.0%
;;CODE                 0      0       0       0        0.0%
;;COMMON               E      C       E       1      100.0%
;;BITSFR0              0      0       0       1        0.0%
;;SFR0                 0      0       0       1        0.0%
;;BITSFR1              0      0       0       2        0.0%
;;SFR1                 0      0       0       2        0.0%
;;STACK                0      0       3       2        0.0%
;;ABS                  0      0      3F       3        0.0%
;;BITBANK0            50      0       0       4        0.0%
;;BITSFR3              0      0       0       4        0.0%
;;SFR3                 0      0       0       4        0.0%
;;BANK0               50     31      31       5       61.3%
;;BITSFR2              0      0       0       5        0.0%
;;SFR2                 0      0       0       5        0.0%
;;BITBANK1            50      0       0       6        0.0%
;;BANK1               50      0       0       7        0.0%
;;BITBANK3            60      0       0       8        0.0%
;;BANK3               60      0       0       9        0.0%
;;BITBANK2            60      0       0      10        0.0%
;;BANK2               60      0       0      11        0.0%
;;DATA                 0      0      42      12        0.0%

	global	_main
psect	maintext,global,class=CODE,delta=2
global __pmaintext
__pmaintext:

;; *************** function _main *****************
;; Defined at:
;;		line 77 in file "C:\Users\t0028919\OneDrive - Crown Equipment Corporation\User_Data\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Function_Pointer\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  test_dtc       12   37[BANK0 ] struct .
;;  test_dtc_cop   12   20[BANK0 ] struct .
;;  a               1   36[BANK0 ] unsigned char 
;;  ptrFunc         1   35[BANK0 ] PTR FTN(unsigned char ,)
;;		 -> cubed(1), 
;;  b               1   34[BANK0 ] unsigned char 
;;  e               1   33[BANK0 ] unsigned char 
;;  c               1   32[BANK0 ] unsigned char 
;;  d               1    0        unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, btemp+1, pclath, cstack
;; Tracked objects:
;;		On entry : 17F/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0      29       0       0       0
;;      Temps:          0       4       0       0       0
;;      Totals:         0      33       0       0       0
;;Total ram usage:       33 bytes
;; Hardware stack levels required when called:    3
;; This function calls:
;;		_cubed
;;		_decrement
;;		_increment
;;		_register_callback
;;		_FunctPtr2
;;		_copy
;;		_copy_structure_as_parameter
;;		_square
;; This function is called by:
;;		Startup code after reset
;; This function uses a non-reentrant model
;;
psect	maintext
	file	"C:\Users\t0028919\OneDrive - Crown Equipment Corporation\User_Data\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Function_Pointer\main.c"
	line	77
	global	__size_of_main
	__size_of_main	equ	__end_of_main-_main
	
_main:	
	opt	stack 5
; Regs used in _main: [wreg-fsr0h+status,2+status,0+btemp+1+pclath+cstack]
	line	96
	
l1675:	
;main.c: 79: uint8_t a;
;main.c: 80: uint8_t b;
;main.c: 81: uint8_t c,d;
;main.c: 82: uint8_t e;
;main.c: 84: T_FUNCPTR_1 ptrFunc;
;main.c: 85: DTC_STRUC test_dtc;
;main.c: 86: DTC_STRUC test_dtc_copy;
;main.c: 96: ptrFunc = &cubed;
	movlw	((fp__cubed-fpbase))&0ffh
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(main@ptrFunc)
	line	97
	
l1677:	
;main.c: 97: e = ptrFunc(3);
	movlw	(03h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(?_cubed)
	movf	(main@ptrFunc),w
	fcall	fptable
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_main+1)+0
	movf	(??_main+1)+0,w
	movwf	(main@e)
	line	98
	
l1679:	
;main.c: 98: e = (*func_array2[INCREMENT])(2);
	movlw	02h
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(?_decrement)
	movlw	(_func_array2-__stringbase)
	movwf	fsr0
	fcall	stringdir
	fcall	fptable
	movf	(0+(?_decrement)),w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_main+1)+0
	movf	(??_main+1)+0,w
	movwf	(main@e)
	line	102
	
l1681:	
;main.c: 102: register_callback(&my_callback);
	movlw	((fp__my_callback-fpbase))&0ffh
	fcall	_register_callback
	line	105
	
l1683:	
;main.c: 105: FunctPtr2();
	fcall	_FunctPtr2
	line	107
	
l1685:	
;main.c: 107: test_dtc.dtc_ar1[0] = 0xBB;
	movlw	low(0BBh)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(main@test_dtc)
	movlw	high(0BBh)
	movwf	((main@test_dtc))+1
	line	108
	
l1687:	
;main.c: 108: test_dtc.dtc_ar1[1] = 0xAA;
	movlw	low(0AAh)
	movwf	0+(main@test_dtc)+02h
	movlw	high(0AAh)
	movwf	(0+(main@test_dtc)+02h)+1
	line	109
	
l1689:	
;main.c: 109: test_dtc.dtc_ar2[0] = 0xDD;
	movlw	low(0DDh)
	movwf	0+(main@test_dtc)+04h
	movlw	high(0DDh)
	movwf	(0+(main@test_dtc)+04h)+1
	line	110
	
l1691:	
;main.c: 110: test_dtc.dtc_ar2[1] = 0xBB;
	movlw	low(0BBh)
	movwf	0+(main@test_dtc)+06h
	movlw	high(0BBh)
	movwf	(0+(main@test_dtc)+06h)+1
	line	111
	
l1693:	
;main.c: 111: test_dtc.dtc_ar3[0] = 0xEE;
	movlw	low(0EEh)
	movwf	0+(main@test_dtc)+08h
	movlw	high(0EEh)
	movwf	(0+(main@test_dtc)+08h)+1
	line	112
	
l1695:	
;main.c: 112: test_dtc.dtc_ar3[1] = 0xFF;
	movlw	low(0FFh)
	movwf	0+(main@test_dtc)+0Ah
	movlw	high(0FFh)
	movwf	(0+(main@test_dtc)+0Ah)+1
	line	116
	
l1697:	
;main.c: 116: test_dtc_copy = test_dtc;
	movlw	(main@test_dtc_copy)&0ffh
	movwf	fsr0
	movlw	low(main@test_dtc)
	movwf	(??_main+0)+0
	movf	fsr0,w
	movwf	((??_main+0)+0+1)
	movlw	12
	movwf	((??_main+0)+0+2)
u2320:
	movf	(??_main+0)+0,w
	movwf	fsr0
	bcf	status, 7	;select IRP bank0
	
	movf	indf,w
	movwf	((??_main+0)+0+3)
	incf	(??_main+0)+0,f
	movf	((??_main+0)+0+1),w
	movwf	fsr0
	
	movf	((??_main+0)+0+3),w
	movwf	indf
	incf	((??_main+0)+0+1),f
	decfsz	((??_main+0)+0+2),f
	goto	u2320
	line	119
	
l1699:	
;main.c: 119: copy(&test_dtc.dtc_ar1[0]);
	movlw	(main@test_dtc)&0ffh
	fcall	_copy
	line	122
	
l1701:	
;main.c: 122: copy_structure_as_parameter(test_dtc);
	movlw	(?_copy_structure_as_parameter)&0ffh
	movwf	fsr0
	movlw	low(main@test_dtc)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_main+0)+0
	movf	fsr0,w
	movwf	((??_main+0)+0+1)
	movlw	12
	movwf	((??_main+0)+0+2)
u2330:
	movf	(??_main+0)+0,w
	movwf	fsr0
	bcf	status, 7	;select IRP bank0
	
	movf	indf,w
	movwf	((??_main+0)+0+3)
	incf	(??_main+0)+0,f
	movf	((??_main+0)+0+1),w
	movwf	fsr0
	
	movf	((??_main+0)+0+3),w
	movwf	indf
	incf	((??_main+0)+0+1),f
	decfsz	((??_main+0)+0+2),f
	goto	u2330
	fcall	_copy_structure_as_parameter
	line	125
	
l1703:	
;main.c: 125: c = 3;
	movlw	(03h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(main@c)
	line	129
	
l1705:	
;main.c: 129: a = 5;
	movlw	(05h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(main@a)
	line	130
	
l1707:	
;main.c: 130: b = (*func_array[0])(a);
	movf	(main@a),w
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(?_cubed)
	movlw	(_func_array-__stringbase)
	movwf	fsr0
	fcall	stringdir
	fcall	fptable
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_main+1)+0
	movf	(??_main+1)+0,w
	movwf	(main@b)
	line	133
	
l1709:	
;main.c: 133: a = 66;
	movlw	(042h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(main@a)
	line	134
	
l1711:	
;main.c: 134: b = (*func_array[1])(a);
	movf	(main@a),w
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(?_cubed)
	movlw	(_func_array-__stringbase)+01h
	movwf	fsr0
	fcall	stringdir
	fcall	fptable
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_main+1)+0
	movf	(??_main+1)+0,w
	movwf	(main@b)
	line	137
	
l1713:	
;main.c: 137: a = 3;
	movlw	(03h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(main@a)
	line	138
	
l1715:	
;main.c: 138: b = (*func_array[2])(a);
	movf	(main@a),w
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(?_cubed)
	movlw	(_func_array-__stringbase)+02h
	movwf	fsr0
	fcall	stringdir
	fcall	fptable
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_main+1)+0
	movf	(??_main+1)+0,w
	movwf	(main@b)
	line	141
	
l1717:	
;main.c: 141: a = 3;
	movlw	(03h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(main@a)
	line	142
	
l1719:	
;main.c: 142: b = (*func_array[3])(a);
	movf	(main@a),w
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(?_cubed)
	movlw	(_func_array-__stringbase)+03h
	movwf	fsr0
	fcall	stringdir
	fcall	fptable
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_main+1)+0
	movf	(??_main+1)+0,w
	movwf	(main@b)
	line	144
	
l30:	
	global	start
	ljmp	start
	opt stack 0
GLOBAL	__end_of_main
	__end_of_main:
;; =============== function _main ends ============

	signat	_main,88
	global	_FunctPtr2
psect	text287,local,class=CODE,delta=2
global __ptext287
__ptext287:

;; *************** function _FunctPtr2 *****************
;; Defined at:
;;		line 51 in file "C:\Users\t0028919\OneDrive - Crown Equipment Corporation\User_Data\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Function_Pointer\func2.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  NETM_msg_cfg    5    9[BANK0 ] struct .
;;  NETM_msg_cfg    2    6[BANK0 ] PTR struct .
;;		 -> FunctPtr2@NETM_msg_cfg_TestVar(5), NETM_msg_cfg(25), 
;;  NETM_BitMsgP    2    0[BANK0 ] unsigned short 
;;  i               1    8[BANK0 ] unsigned char 
;;  NETM_NodeNet    1    5[BANK0 ] unsigned char 
;;  NETM_MsgType    1    3[BANK0 ] unsigned char 
;;  NETM_MsgMiss    1    2[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, btemp+1, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0      14       0       0       0
;;      Temps:          2       0       0       0       0
;;      Totals:         2      14       0       0       0
;;Total ram usage:       16 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		_NETM_IMAP_ETM_LTM_Implausible
;;		___bmul
;;		Absolute function
;;		_NETM_IMAP_DCM_STATUS_C_Implausi
;;		_cubedFunc1
;;		_squareFunc1
;;		_decrementFunc1
;;		_incrementFunc1
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text287
	file	"C:\Users\t0028919\OneDrive - Crown Equipment Corporation\User_Data\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Function_Pointer\func2.c"
	line	51
	global	__size_of_FunctPtr2
	__size_of_FunctPtr2	equ	__end_of_FunctPtr2-_FunctPtr2
	
_FunctPtr2:	
	opt	stack 5
; Regs used in _FunctPtr2: [wreg-fsr0h+status,2+status,0+btemp+1+pclath+cstack]
	line	63
	
l1639:	
;func2.c: 53: uint8_t i;
;func2.c: 54: uint8_t NETM_NodeNetId;
;func2.c: 55: uint8_t NETM_MsgMissRecoveryFunc;
;func2.c: 56: uint8_t NETM_MsgType;
;func2.c: 57: uint16_t NETM_BitMsgPos;
;func2.c: 58: NETM_msg_cfg_t *NETM_msg_cfgLocal_ptr;
;func2.c: 60: NETM_msg_cfg_t NETM_msg_cfg_TestVar;
;func2.c: 63: NETM_IMAP_ETM_LTM_Implausible(0x01);
	movlw	(01h)
	fcall	_NETM_IMAP_ETM_LTM_Implausible
	line	64
	
l1641:	
;func2.c: 64: for(i = 0; i<=5; i++)
	clrc
	movlw	0
	btfsc	status,0
	movlw	1
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(FunctPtr2@i)
	
l1643:	
	movlw	(06h)
	subwf	(FunctPtr2@i),w
	skipc
	goto	u2301
	goto	u2300
u2301:
	goto	l1647
u2300:
	goto	l1665
	
l1645:	
	goto	l1665
	line	65
	
l85:	
	line	66
	
l1647:	
;func2.c: 65: {
;func2.c: 66: NETM_msg_cfgLocal_ptr = (NETM_msg_cfg_t *)&NETM_msg_cfg[i];
	movlw	(05h)
	movwf	(??_FunctPtr2+0)+0
	movf	(??_FunctPtr2+0)+0,w
	movwf	(?___bmul)
	movf	(FunctPtr2@i),w
	fcall	___bmul
	addlw	low((_NETM_msg_cfg-__stringbase))
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(FunctPtr2@NETM_msg_cfgLocal_ptr)
	movlw	80h
	movwf	(FunctPtr2@NETM_msg_cfgLocal_ptr+1)
	line	68
	
l1649:	
;func2.c: 68: NETM_NodeNetId = NETM_msg_cfgLocal_ptr->NodeIdent;
	movlw	02h
	addwf	(FunctPtr2@NETM_msg_cfgLocal_ptr),w
	movwf	fsr0
	movf	(FunctPtr2@NETM_msg_cfgLocal_ptr+1),w
	skipnc
	incf	(FunctPtr2@NETM_msg_cfgLocal_ptr+1),w
	movwf	btemp+1
	fcall	stringtab
	movwf	(??_FunctPtr2+0)+0
	movf	(??_FunctPtr2+0)+0,w
	movwf	(FunctPtr2@NETM_NodeNetId)
	line	69
	
l1651:	
;func2.c: 69: NETM_BitMsgPos = NETM_msg_cfgLocal_ptr->BitMsgPos;
	movf	(FunctPtr2@NETM_msg_cfgLocal_ptr+1),w
	movwf	btemp+1
	movf	(FunctPtr2@NETM_msg_cfgLocal_ptr),w
	movwf	fsr0
	fcall	stringtab
	movwf	(FunctPtr2@NETM_BitMsgPos)
	fcall	stringtab
	movwf	(FunctPtr2@NETM_BitMsgPos+1)
	line	70
	
l1653:	
;func2.c: 70: NETM_MsgType = NETM_msg_cfgLocal_ptr->MsgType;
	movlw	03h
	addwf	(FunctPtr2@NETM_msg_cfgLocal_ptr),w
	movwf	fsr0
	movf	(FunctPtr2@NETM_msg_cfgLocal_ptr+1),w
	skipnc
	incf	(FunctPtr2@NETM_msg_cfgLocal_ptr+1),w
	movwf	btemp+1
	fcall	stringtab
	movwf	(??_FunctPtr2+0)+0
	movf	(??_FunctPtr2+0)+0,w
	movwf	(FunctPtr2@NETM_MsgType)
	line	71
	
l1655:	
;func2.c: 71: NETM_MsgMissRecoveryFunc = (*NETM_msg_cfg[i].MsgMissRecoveryFunc)(NETM_NodeNetId);
	movlw	(05h)
	movwf	(??_FunctPtr2+0)+0
	movf	(??_FunctPtr2+0)+0,w
	movwf	(?___bmul)
	movf	(FunctPtr2@i),w
	fcall	___bmul
	addlw	04h
	addlw	low((_NETM_msg_cfg-__stringbase))
	movwf	fsr0
	fcall	stringdir
	movwf	(??_FunctPtr2+1)+0
	movf	(??_FunctPtr2+1)+0,w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(_FunctPtr2$576)
	
l1657:	
;func2.c: 71: NETM_MsgMissRecoveryFunc = (*NETM_msg_cfg[i].MsgMissRecoveryFunc)(NETM_NodeNetId);
	movf	(FunctPtr2@NETM_NodeNetId),w
	movwf	(??_FunctPtr2+0)+0
	movf	(??_FunctPtr2+0)+0,w
	movwf	(?_NETM_IMAP_DCM_STATUS_C_Implausi)
	movf	(_FunctPtr2$576),w
	fcall	fptable
	movwf	(??_FunctPtr2+1)+0
	movf	(??_FunctPtr2+1)+0,w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(FunctPtr2@NETM_MsgMissRecoveryFunc)
	line	72
	
l1659:	
# 72 "C:\Users\t0028919\OneDrive - Crown Equipment Corporation\User_Data\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Function_Pointer\func2.c"
nop ;#
psect	text287
	line	64
	
l1661:	
	movlw	(01h)
	movwf	(??_FunctPtr2+0)+0
	movf	(??_FunctPtr2+0)+0,w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	addwf	(FunctPtr2@i),f
	
l1663:	
	movlw	(06h)
	subwf	(FunctPtr2@i),w
	skipc
	goto	u2311
	goto	u2310
u2311:
	goto	l1647
u2310:
	goto	l1665
	
l86:	
	line	76
	
l1665:	
;func2.c: 73: }
;func2.c: 76: NETM_msg_cfgLocal_ptr = (NETM_msg_cfg_t *)&NETM_msg_cfg_TestVar;
	movlw	(FunctPtr2@NETM_msg_cfg_TestVar&0ffh)
	movwf	(FunctPtr2@NETM_msg_cfgLocal_ptr)
	movlw	(0x0/2)
	movwf	(FunctPtr2@NETM_msg_cfgLocal_ptr+1)
	line	77
	
l1667:	
;func2.c: 77: NETM_msg_cfg_TestVar.BitMsgPos = 0x1010;
	movlw	low(01010h)
	movwf	(FunctPtr2@NETM_msg_cfg_TestVar)
	movlw	high(01010h)
	movwf	((FunctPtr2@NETM_msg_cfg_TestVar))+1
	line	78
	
l1669:	
;func2.c: 78: NETM_msg_cfg_TestVar.NodeIdent = 2;
	movlw	(02h)
	movwf	(??_FunctPtr2+0)+0
	movf	(??_FunctPtr2+0)+0,w
	movwf	0+(FunctPtr2@NETM_msg_cfg_TestVar)+02h
	line	79
	
l1671:	
;func2.c: 79: NETM_msg_cfg_TestVar.MsgType = 26;
	movlw	(01Ah)
	movwf	(??_FunctPtr2+0)+0
	movf	(??_FunctPtr2+0)+0,w
	movwf	0+(FunctPtr2@NETM_msg_cfg_TestVar)+03h
	line	80
	
l1673:	
;func2.c: 80: NETM_msg_cfg_TestVar.MsgMissRecoveryFunc = squareFunc1(NETM_msg_cfg_TestVar.NodeIdent);
	movf	0+(FunctPtr2@NETM_msg_cfg_TestVar)+02h,w
	movwf	(??_FunctPtr2+0)+0
	movf	(??_FunctPtr2+0)+0,w
	movwf	(?_NETM_IMAP_DCM_STATUS_C_Implausi)
	fcall	_squareFunc1
	movwf	(??_FunctPtr2+1)+0
	movf	(??_FunctPtr2+1)+0,w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(0+FunctPtr2@NETM_msg_cfg_TestVar+04h)
	line	81
	
l87:	
	return
	opt stack 0
GLOBAL	__end_of_FunctPtr2
	__end_of_FunctPtr2:
;; =============== function _FunctPtr2 ends ============

	signat	_FunctPtr2,88
	global	_cubedFunc1
	global	_incrementFunc1
	global	_decrementFunc1
	global	_squareFunc1
	global	_NETM_IMAP_DCM_STATUS_C_Implausi
psect	text288,local,class=CODE,delta=2
global __ptext288
__ptext288:

;; *************** function _NETM_IMAP_DCM_STATUS_C_Implausi *****************
;; Defined at:
;;		line 103 in file "C:\Users\t0028919\OneDrive - Crown Equipment Corporation\User_Data\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Function_Pointer\func2.c"
;; Parameters:    Size  Location     Type
;;  Status          1    4[COMMON] unsigned char 
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         1       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         1       0       0       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_FunctPtr2
;; This function uses a non-reentrant model
;;
psect	text288
	file	"C:\Users\t0028919\OneDrive - Crown Equipment Corporation\User_Data\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Function_Pointer\func2.c"
	line	103
	global	__size_of_NETM_IMAP_DCM_STATUS_C_Implausi
	__size_of_NETM_IMAP_DCM_STATUS_C_Implausi	equ	__end_of_NETM_IMAP_DCM_STATUS_C_Implausi-_NETM_IMAP_DCM_STATUS_C_Implausi
	
_NETM_IMAP_DCM_STATUS_C_Implausi:	
	opt	stack 6
; Regs used in _NETM_IMAP_DCM_STATUS_C_Implausi: [wreg]
	line	104
	
l1591:	
;func2.c: 104: (IMAP_ImplausibilityControl.status_c_dtcm_implausible = Status);
	movf	(NETM_IMAP_DCM_STATUS_C_Implausi@Status),w
	bcf	(_IMAP_ImplausibilityControl),2
	skipz
	bsf	(_IMAP_ImplausibilityControl),2
	line	105
	
l102:	
	return
	opt stack 0
GLOBAL	__end_of_NETM_IMAP_DCM_STATUS_C_Implausi
	__end_of_NETM_IMAP_DCM_STATUS_C_Implausi:
;; =============== function _NETM_IMAP_DCM_STATUS_C_Implausi ends ============

	signat	_NETM_IMAP_DCM_STATUS_C_Implausi,4216
psect	text289,local,class=CODE,delta=2
global __ptext289
__ptext289:

;; *************** function _squareFunc1 *****************
;; Defined at:
;;		line 18 in file "C:\Users\t0028919\OneDrive - Crown Equipment Corporation\User_Data\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Function_Pointer\func1.c"
;; Parameters:    Size  Location     Type
;;  x               1    4[COMMON] unsigned char 
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         1       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          1       0       0       0       0
;;      Totals:         2       0       0       0       0
;;Total ram usage:        2 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		___bmul
;; This function is called by:
;;		_FunctPtr2
;; This function uses a non-reentrant model
;;
psect	text289
	file	"C:\Users\t0028919\OneDrive - Crown Equipment Corporation\User_Data\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Function_Pointer\func1.c"
	line	18
	global	__size_of_squareFunc1
	__size_of_squareFunc1	equ	__end_of_squareFunc1-_squareFunc1
	
_squareFunc1:	
	opt	stack 5
; Regs used in _squareFunc1: [wreg+status,2+status,0+pclath+cstack]
	line	19
	
l1631:	
;func1.c: 19: return (x*x);
	movf	(squareFunc1@x),w
	movwf	(??_squareFunc1+0)+0
	movf	(??_squareFunc1+0)+0,w
	movwf	(?___bmul)
	movf	(squareFunc1@x),w
	fcall	___bmul
	goto	l61
	
l1633:	
	line	20
	
l61:	
	return
	opt stack 0
GLOBAL	__end_of_squareFunc1
	__end_of_squareFunc1:
;; =============== function _squareFunc1 ends ============

	signat	_squareFunc1,4217
psect	text290,local,class=CODE,delta=2
global __ptext290
__ptext290:

;; *************** function _decrementFunc1 *****************
;; Defined at:
;;		line 13 in file "C:\Users\t0028919\OneDrive - Crown Equipment Corporation\User_Data\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Function_Pointer\func1.c"
;; Parameters:    Size  Location     Type
;;  x               1    4[COMMON] unsigned char 
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         1       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         1       0       0       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_FunctPtr2
;; This function uses a non-reentrant model
;;
psect	text290
	file	"C:\Users\t0028919\OneDrive - Crown Equipment Corporation\User_Data\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Function_Pointer\func1.c"
	line	13
	global	__size_of_decrementFunc1
	__size_of_decrementFunc1	equ	__end_of_decrementFunc1-_decrementFunc1
	
_decrementFunc1:	
	opt	stack 6
; Regs used in _decrementFunc1: [wreg+status,2+status,0]
	line	14
	
l1585:	
;func1.c: 14: return (x-1);
	movf	(decrementFunc1@x),w
	addlw	0FFh
	goto	l58
	
l1587:	
	line	15
	
l58:	
	return
	opt stack 0
GLOBAL	__end_of_decrementFunc1
	__end_of_decrementFunc1:
;; =============== function _decrementFunc1 ends ============

	signat	_decrementFunc1,4217
psect	text291,local,class=CODE,delta=2
global __ptext291
__ptext291:

;; *************** function _incrementFunc1 *****************
;; Defined at:
;;		line 8 in file "C:\Users\t0028919\OneDrive - Crown Equipment Corporation\User_Data\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Function_Pointer\func1.c"
;; Parameters:    Size  Location     Type
;;  x               1    4[COMMON] unsigned char 
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         1       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         1       0       0       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_FunctPtr2
;; This function uses a non-reentrant model
;;
psect	text291
	file	"C:\Users\t0028919\OneDrive - Crown Equipment Corporation\User_Data\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Function_Pointer\func1.c"
	line	8
	global	__size_of_incrementFunc1
	__size_of_incrementFunc1	equ	__end_of_incrementFunc1-_incrementFunc1
	
_incrementFunc1:	
	opt	stack 6
; Regs used in _incrementFunc1: [wreg+status,2+status,0]
	line	9
	
l1581:	
;func1.c: 9: return (x+1);
	movf	(incrementFunc1@x),w
	addlw	01h
	goto	l55
	
l1583:	
	line	10
	
l55:	
	return
	opt stack 0
GLOBAL	__end_of_incrementFunc1
	__end_of_incrementFunc1:
;; =============== function _incrementFunc1 ends ============

	signat	_incrementFunc1,4217
psect	text292,local,class=CODE,delta=2
global __ptext292
__ptext292:

;; *************** function _cubedFunc1 *****************
;; Defined at:
;;		line 23 in file "C:\Users\t0028919\OneDrive - Crown Equipment Corporation\User_Data\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Function_Pointer\func1.c"
;; Parameters:    Size  Location     Type
;;  x               1    4[COMMON] unsigned char 
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         1       0       0       0       0
;;      Locals:         1       0       0       0       0
;;      Temps:          2       0       0       0       0
;;      Totals:         4       0       0       0       0
;;Total ram usage:        4 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		___bmul
;; This function is called by:
;;		_FunctPtr2
;; This function uses a non-reentrant model
;;
psect	text292
	file	"C:\Users\t0028919\OneDrive - Crown Equipment Corporation\User_Data\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Function_Pointer\func1.c"
	line	23
	global	__size_of_cubedFunc1
	__size_of_cubedFunc1	equ	__end_of_cubedFunc1-_cubedFunc1
	
_cubedFunc1:	
	opt	stack 5
; Regs used in _cubedFunc1: [wreg+status,2+status,0+pclath+cstack]
	line	24
	
l1635:	
;func1.c: 24: return (x*x*x);
	movf	(cubedFunc1@x),w
	movwf	(??_cubedFunc1+0)+0
	movf	(??_cubedFunc1+0)+0,w
	movwf	(?___bmul)
	movf	(cubedFunc1@x),w
	fcall	___bmul
	movwf	(??_cubedFunc1+1)+0
	movf	(??_cubedFunc1+1)+0,w
	movwf	(_cubedFunc1$551)
;func1.c: 24: return (x*x*x);
	movf	(cubedFunc1@x),w
	movwf	(??_cubedFunc1+0)+0
	movf	(??_cubedFunc1+0)+0,w
	movwf	(?___bmul)
	movf	(_cubedFunc1$551),w
	fcall	___bmul
	goto	l64
	
l1637:	
	line	25
	
l64:	
	return
	opt stack 0
GLOBAL	__end_of_cubedFunc1
	__end_of_cubedFunc1:
;; =============== function _cubedFunc1 ends ============

	signat	_cubedFunc1,4217
	global	_cubed
	global	_increment
	global	_decrement
	global	_square
psect	text293,local,class=CODE,delta=2
global __ptext293
__ptext293:

;; *************** function _square *****************
;; Defined at:
;;		line 188 in file "C:\Users\t0028919\OneDrive - Crown Equipment Corporation\User_Data\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Function_Pointer\main.c"
;; Parameters:    Size  Location     Type
;;  x               1    4[COMMON] unsigned char 
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         1       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          1       0       0       0       0
;;      Totals:         2       0       0       0       0
;;Total ram usage:        2 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		___bmul
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text293
	file	"C:\Users\t0028919\OneDrive - Crown Equipment Corporation\User_Data\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Function_Pointer\main.c"
	line	188
	global	__size_of_square
	__size_of_square	equ	__end_of_square-_square
	
_square:	
	opt	stack 6
; Regs used in _square: [wreg+status,2+status,0+pclath+cstack]
	line	189
	
l1623:	
;main.c: 189: return (x*x);
	movf	(square@x),w
	movwf	(??_square+0)+0
	movf	(??_square+0)+0,w
	movwf	(?___bmul)
	movf	(square@x),w
	fcall	___bmul
	goto	l47
	
l1625:	
	line	190
	
l47:	
	return
	opt stack 0
GLOBAL	__end_of_square
	__end_of_square:
;; =============== function _square ends ============

	signat	_square,4217
psect	text294,local,class=CODE,delta=2
global __ptext294
__ptext294:

;; *************** function _decrement *****************
;; Defined at:
;;		line 183 in file "C:\Users\t0028919\OneDrive - Crown Equipment Corporation\User_Data\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Function_Pointer\main.c"
;; Parameters:    Size  Location     Type
;;  x               1    4[COMMON] unsigned char 
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         1       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         1       0       0       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text294
	file	"C:\Users\t0028919\OneDrive - Crown Equipment Corporation\User_Data\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Function_Pointer\main.c"
	line	183
	global	__size_of_decrement
	__size_of_decrement	equ	__end_of_decrement-_decrement
	
_decrement:	
	opt	stack 7
; Regs used in _decrement: [wreg+status,2+status,0]
	line	184
	
l1577:	
;main.c: 184: return (x-1);
	movf	(decrement@x),w
	addlw	0FFh
	goto	l44
	
l1579:	
	line	185
	
l44:	
	return
	opt stack 0
GLOBAL	__end_of_decrement
	__end_of_decrement:
;; =============== function _decrement ends ============

	signat	_decrement,4217
psect	text295,local,class=CODE,delta=2
global __ptext295
__ptext295:

;; *************** function _increment *****************
;; Defined at:
;;		line 178 in file "C:\Users\t0028919\OneDrive - Crown Equipment Corporation\User_Data\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Function_Pointer\main.c"
;; Parameters:    Size  Location     Type
;;  x               1    4[COMMON] unsigned char 
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         1       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         1       0       0       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text295
	file	"C:\Users\t0028919\OneDrive - Crown Equipment Corporation\User_Data\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Function_Pointer\main.c"
	line	178
	global	__size_of_increment
	__size_of_increment	equ	__end_of_increment-_increment
	
_increment:	
	opt	stack 7
; Regs used in _increment: [wreg+status,2+status,0]
	line	179
	
l1573:	
;main.c: 179: return (x+1);
	movf	(increment@x),w
	addlw	01h
	goto	l41
	
l1575:	
	line	180
	
l41:	
	return
	opt stack 0
GLOBAL	__end_of_increment
	__end_of_increment:
;; =============== function _increment ends ============

	signat	_increment,4217
psect	text296,local,class=CODE,delta=2
global __ptext296
__ptext296:

;; *************** function _cubed *****************
;; Defined at:
;;		line 193 in file "C:\Users\t0028919\OneDrive - Crown Equipment Corporation\User_Data\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Function_Pointer\main.c"
;; Parameters:    Size  Location     Type
;;  x               1    4[COMMON] unsigned char 
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         1       0       0       0       0
;;      Locals:         1       0       0       0       0
;;      Temps:          2       0       0       0       0
;;      Totals:         4       0       0       0       0
;;Total ram usage:        4 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		___bmul
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text296
	file	"C:\Users\t0028919\OneDrive - Crown Equipment Corporation\User_Data\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Function_Pointer\main.c"
	line	193
	global	__size_of_cubed
	__size_of_cubed	equ	__end_of_cubed-_cubed
	
_cubed:	
	opt	stack 6
; Regs used in _cubed: [wreg+status,2+status,0+pclath+cstack]
	line	194
	
l1627:	
;main.c: 194: return (x*x*x);
	movf	(cubed@x),w
	movwf	(??_cubed+0)+0
	movf	(??_cubed+0)+0,w
	movwf	(?___bmul)
	movf	(cubed@x),w
	fcall	___bmul
	movwf	(??_cubed+1)+0
	movf	(??_cubed+1)+0,w
	movwf	(_cubed$550)
;main.c: 194: return (x*x*x);
	movf	(cubed@x),w
	movwf	(??_cubed+0)+0
	movf	(??_cubed+0)+0,w
	movwf	(?___bmul)
	movf	(_cubed$550),w
	fcall	___bmul
	goto	l50
	
l1629:	
	line	195
	
l50:	
	return
	opt stack 0
GLOBAL	__end_of_cubed
	__end_of_cubed:
;; =============== function _cubed ends ============

	signat	_cubed,4217
	global	_register_callback
psect	text297,local,class=CODE,delta=2
global __ptext297
__ptext297:

;; *************** function _register_callback *****************
;; Defined at:
;;		line 8 in file "C:\Users\t0028919\OneDrive - Crown Equipment Corporation\User_Data\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Function_Pointer\reg_callback.c"
;; Parameters:    Size  Location     Type
;;  callback_t      1    wreg     PTR FTN()void 
;;		 -> my_callback(0), 
;; Auto vars:     Size  Location     Type
;;  callback_t      1    0[COMMON] PTR FTN()void 
;;		 -> my_callback(0), 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         1       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         1       0       0       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		_my_callback
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text297
	file	"C:\Users\t0028919\OneDrive - Crown Equipment Corporation\User_Data\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Function_Pointer\reg_callback.c"
	line	8
	global	__size_of_register_callback
	__size_of_register_callback	equ	__end_of_register_callback-_register_callback
	
_register_callback:	
	opt	stack 6
; Regs used in _register_callback: [wreg+status,2+status,0+pclath+cstack]
;register_callback@callback_t stored from wreg
	line	13
	movwf	(register_callback@callback_t)
	
l1621:	
;reg_callback.c: 13: (*callback_t)();
	movf	(register_callback@callback_t),w
	fcall	fptable
	line	14
	
l110:	
	return
	opt stack 0
GLOBAL	__end_of_register_callback
	__end_of_register_callback:
;; =============== function _register_callback ends ============

	signat	_register_callback,4216
	global	___bmul
psect	text298,local,class=CODE,delta=2
global __ptext298
__ptext298:

;; *************** function ___bmul *****************
;; Defined at:
;;		line 3 in file "bmul.c"
;; Parameters:    Size  Location     Type
;;  multiplier      1    wreg     unsigned char 
;;  multiplicand    1    0[COMMON] unsigned char 
;; Auto vars:     Size  Location     Type
;;  multiplier      1    3[COMMON] unsigned char 
;;  product         1    2[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         1       0       0       0       0
;;      Locals:         2       0       0       0       0
;;      Temps:          1       0       0       0       0
;;      Totals:         4       0       0       0       0
;;Total ram usage:        4 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_square
;;		_cubed
;;		_squareFunc1
;;		_cubedFunc1
;;		_FunctPtr2
;; This function uses a non-reentrant model
;;
psect	text298
	file	"C:\Program Files (x86)\HI-TECH Software\PICC\9.80\sources\bmul.c"
	line	3
	global	__size_of___bmul
	__size_of___bmul	equ	__end_of___bmul-___bmul
	
___bmul:	
	opt	stack 6
; Regs used in ___bmul: [wreg+status,2+status,0]
;___bmul@multiplier stored from wreg
	movwf	(___bmul@multiplier)
	line	4
	
l1593:	
	clrc
	movlw	0
	btfsc	status,0
	movlw	1
	movwf	(___bmul@product)
	goto	l1595
	line	6
	
l113:	
	line	7
	
l1595:	
	btfss	(___bmul@multiplier),(0)&7
	goto	u2241
	goto	u2240
u2241:
	goto	l1599
u2240:
	line	8
	
l1597:	
	movf	(___bmul@multiplicand),w
	movwf	(??___bmul+0)+0
	movf	(??___bmul+0)+0,w
	addwf	(___bmul@product),f
	goto	l1599
	
l114:	
	line	9
	
l1599:	
	clrc
	rlf	(___bmul@multiplicand),f

	line	10
	
l1601:	
	clrc
	rrf	(___bmul@multiplier),f

	line	11
	
l1603:	
	movf	(___bmul@multiplier),f
	skipz
	goto	u2251
	goto	u2250
u2251:
	goto	l1595
u2250:
	goto	l1605
	
l115:	
	line	12
	
l1605:	
	movf	(___bmul@product),w
	goto	l116
	
l1607:	
	line	13
	
l116:	
	return
	opt stack 0
GLOBAL	__end_of___bmul
	__end_of___bmul:
;; =============== function ___bmul ends ============

	signat	___bmul,8313
	global	_NETM_IMAP_ETM_LTM_Implausible
psect	text299,local,class=CODE,delta=2
global __ptext299
__ptext299:

;; *************** function _NETM_IMAP_ETM_LTM_Implausible *****************
;; Defined at:
;;		line 107 in file "C:\Users\t0028919\OneDrive - Crown Equipment Corporation\User_Data\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Function_Pointer\func2.c"
;; Parameters:    Size  Location     Type
;;  Status          1    wreg     unsigned char 
;; Auto vars:     Size  Location     Type
;;  Status          1    0[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         1       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         1       0       0       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_FunctPtr2
;; This function uses a non-reentrant model
;;
psect	text299
	file	"C:\Users\t0028919\OneDrive - Crown Equipment Corporation\User_Data\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Function_Pointer\func2.c"
	line	107
	global	__size_of_NETM_IMAP_ETM_LTM_Implausible
	__size_of_NETM_IMAP_ETM_LTM_Implausible	equ	__end_of_NETM_IMAP_ETM_LTM_Implausible-_NETM_IMAP_ETM_LTM_Implausible
	
_NETM_IMAP_ETM_LTM_Implausible:	
	opt	stack 6
; Regs used in _NETM_IMAP_ETM_LTM_Implausible: [wreg]
;NETM_IMAP_ETM_LTM_Implausible@Status stored from wreg
	movwf	(NETM_IMAP_ETM_LTM_Implausible@Status)
	line	108
	
l1589:	
;func2.c: 108: (IMAP_ImplausibilityControl.tgw_a1_implausible = Status);
	movf	(NETM_IMAP_ETM_LTM_Implausible@Status),w
	bcf	(_IMAP_ImplausibilityControl),3
	skipz
	bsf	(_IMAP_ImplausibilityControl),3
	line	109
	
l105:	
	return
	opt stack 0
GLOBAL	__end_of_NETM_IMAP_ETM_LTM_Implausible
	__end_of_NETM_IMAP_ETM_LTM_Implausible:
;; =============== function _NETM_IMAP_ETM_LTM_Implausible ends ============

	signat	_NETM_IMAP_ETM_LTM_Implausible,4216
	global	_copy_structure_as_parameter
psect	text300,local,class=CODE,delta=2
global __ptext300
__ptext300:

;; *************** function _copy_structure_as_parameter *****************
;; Defined at:
;;		line 170 in file "C:\Users\t0028919\OneDrive - Crown Equipment Corporation\User_Data\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Function_Pointer\main.c"
;; Parameters:    Size  Location     Type
;;  dtc_var        12    0[COMMON] struct .
;; Auto vars:     Size  Location     Type
;;  yet_another    12    4[BANK0 ] struct .
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:        12       0       0       0       0
;;      Locals:         0      12       0       0       0
;;      Temps:          0       4       0       0       0
;;      Totals:        12      16       0       0       0
;;Total ram usage:       28 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text300
	file	"C:\Users\t0028919\OneDrive - Crown Equipment Corporation\User_Data\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Function_Pointer\main.c"
	line	170
	global	__size_of_copy_structure_as_parameter
	__size_of_copy_structure_as_parameter	equ	__end_of_copy_structure_as_parameter-_copy_structure_as_parameter
	
_copy_structure_as_parameter:	
	opt	stack 7
; Regs used in _copy_structure_as_parameter: [wregfsr0]
	line	172
	
l1569:	
# 172 "C:\Users\t0028919\OneDrive - Crown Equipment Corporation\User_Data\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Function_Pointer\main.c"
nop ;#
psect	text300
	line	173
	
l1571:	
;main.c: 173: yet_another = dtc_var;
	movlw	(copy_structure_as_parameter@yet_another)&0ffh
	movwf	fsr0
	movlw	low(copy_structure_as_parameter@dtc_var)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_copy_structure_as_parameter+0)+0
	movf	fsr0,w
	movwf	((??_copy_structure_as_parameter+0)+0+1)
	movlw	12
	movwf	((??_copy_structure_as_parameter+0)+0+2)
u2230:
	movf	(??_copy_structure_as_parameter+0)+0,w
	movwf	fsr0
	
	movf	indf,w
	movwf	((??_copy_structure_as_parameter+0)+0+3)
	incf	(??_copy_structure_as_parameter+0)+0,f
	movf	((??_copy_structure_as_parameter+0)+0+1),w
	movwf	fsr0
	bcf	status, 7	;select IRP bank0
	
	movf	((??_copy_structure_as_parameter+0)+0+3),w
	movwf	indf
	incf	((??_copy_structure_as_parameter+0)+0+1),f
	decfsz	((??_copy_structure_as_parameter+0)+0+2),f
	goto	u2230
	line	175
	
l38:	
	return
	opt stack 0
GLOBAL	__end_of_copy_structure_as_parameter
	__end_of_copy_structure_as_parameter:
;; =============== function _copy_structure_as_parameter ends ============

	signat	_copy_structure_as_parameter,4216
	global	_copy
psect	text301,local,class=CODE,delta=2
global __ptext301
__ptext301:

;; *************** function _copy *****************
;; Defined at:
;;		line 149 in file "C:\Users\t0028919\OneDrive - Crown Equipment Corporation\User_Data\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Function_Pointer\main.c"
;; Parameters:    Size  Location     Type
;;  dtc_structur    1    wreg     PTR int 
;;		 -> main@test_dtc(12), 
;; Auto vars:     Size  Location     Type
;;  dtc_structur    1   12[BANK0 ] PTR int 
;;		 -> main@test_dtc(12), 
;;  from_main_co   12    0[BANK0 ] struct .
;;  i               2   13[BANK0 ] int 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, btemp+1
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0      15       0       0       0
;;      Temps:          3       0       0       0       0
;;      Totals:         3      15       0       0       0
;;Total ram usage:       18 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text301
	file	"C:\Users\t0028919\OneDrive - Crown Equipment Corporation\User_Data\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Function_Pointer\main.c"
	line	149
	global	__size_of_copy
	__size_of_copy	equ	__end_of_copy-_copy
	
_copy:	
	opt	stack 7
; Regs used in _copy: [wreg-fsr0h+status,2+status,0+btemp+1]
;copy@dtc_structure_ptr stored from wreg
	line	153
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(copy@dtc_structure_ptr)
	
l1555:	
;main.c: 151: DTC_STRUC from_main_copy;
;main.c: 152: int i;
;main.c: 153: for (i=0; i<=5; i++)
	movlw	low(0)
	movwf	(copy@i)
	movlw	high(0)
	movwf	((copy@i))+1
	
l1557:	
	movf	(copy@i+1),w
	xorlw	80h
	movwf	btemp+1
	movlw	(high(06h))^80h
	subwf	btemp+1,w
	skipz
	goto	u2215
	movlw	low(06h)
	subwf	(copy@i),w
u2215:

	skipc
	goto	u2211
	goto	u2210
u2211:
	goto	l1561
u2210:
	goto	l34
	
l1559:	
	goto	l34
	line	154
	
l33:	
	line	155
	
l1561:	
;main.c: 154: {
;main.c: 155: from_main_copy.dtc_ar1[i] = *dtc_structure_ptr;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(copy@dtc_structure_ptr),w
	movwf	fsr0
	bcf	status, 7	;select IRP bank0
	movf	indf,w
	movwf	(??_copy+0)+0+0
	incf	fsr0,f
	movf	indf,w
	movwf	(??_copy+0)+0+1
	movf	(copy@i),w
	movwf	(??_copy+2)+0
	addwf	(??_copy+2)+0,w
	addlw	copy@from_main_copy&0ffh
	movwf	fsr0
	movf	0+(??_copy+0)+0,w
	movwf	indf
	incf	fsr0,f
	movf	1+(??_copy+0)+0,w
	movwf	indf
	line	156
	
l1563:	
;main.c: 156: dtc_structure_ptr++;
	movlw	(02h)
	movwf	(??_copy+0)+0
	movf	(??_copy+0)+0,w
	addwf	(copy@dtc_structure_ptr),f
	line	153
	
l1565:	
	movlw	low(01h)
	addwf	(copy@i),f
	skipnc
	incf	(copy@i+1),f
	movlw	high(01h)
	addwf	(copy@i+1),f
	
l1567:	
	movf	(copy@i+1),w
	xorlw	80h
	movwf	btemp+1
	movlw	(high(06h))^80h
	subwf	btemp+1,w
	skipz
	goto	u2225
	movlw	low(06h)
	subwf	(copy@i),w
u2225:

	skipc
	goto	u2221
	goto	u2220
u2221:
	goto	l1561
u2220:
	
l34:	
	line	158
# 158 "C:\Users\t0028919\OneDrive - Crown Equipment Corporation\User_Data\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Function_Pointer\main.c"
nop ;#
psect	text301
	line	160
	
l35:	
	return
	opt stack 0
GLOBAL	__end_of_copy
	__end_of_copy:
;; =============== function _copy ends ============

	signat	_copy,4216
	global	_my_callback
psect	text302,local,class=CODE,delta=2
global __ptext302
__ptext302:

;; *************** function _my_callback *****************
;; Defined at:
;;		line 69 in file "C:\Users\t0028919\OneDrive - Crown Equipment Corporation\User_Data\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Function_Pointer\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		None
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;;		_register_callback
;; This function uses a non-reentrant model
;;
psect	text302
	file	"C:\Users\t0028919\OneDrive - Crown Equipment Corporation\User_Data\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Function_Pointer\main.c"
	line	69
	global	__size_of_my_callback
	__size_of_my_callback	equ	__end_of_my_callback-_my_callback
	
_my_callback:	
	opt	stack 6
; Regs used in _my_callback: []
	line	70
	
l1553:	
# 70 "C:\Users\t0028919\OneDrive - Crown Equipment Corporation\User_Data\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Function_Pointer\main.c"
nop ;#
psect	text302
	line	72
	
l27:	
	return
	opt stack 0
GLOBAL	__end_of_my_callback
	__end_of_my_callback:
;; =============== function _my_callback ends ============

	signat	_my_callback,88
	global	fptotal
fptotal equ 10
	file ""
	line	0
psect	functab,class=CODE,delta=2,reloc=256
global __pfunctab
__pfunctab:
	global	fptable
fptable:
	movwf (btemp+1)&07Fh
	movlw high(fptable)
	movwf pclath
	movf (btemp+1)&07Fh,w
	addwf pc
	global	fpbase
fpbase:
	goto fpbase	; Call via a null pointer and you will get stuck here.
fp__increment:
	ljmp	_increment
	file ""
	line	0
psect	functab
fp__decrement:
	ljmp	_decrement
	file ""
	line	0
psect	functab
fp__square:
	ljmp	_square
	file ""
	line	0
psect	functab
fp__cubed:
	ljmp	_cubed
	file ""
	line	0
psect	functab
fp__my_callback:
	ljmp	_my_callback
	file ""
	line	0
psect	functab
fp__incrementFunc1:
	ljmp	_incrementFunc1
	file ""
	line	0
psect	functab
fp__decrementFunc1:
	ljmp	_decrementFunc1
	file ""
	line	0
psect	functab
fp__squareFunc1:
entry__squareFunc1:
	global	entry__squareFunc1
	ljmp	_squareFunc1
	file ""
	line	0
psect	functab
fp__cubedFunc1:
	ljmp	_cubedFunc1
	file ""
	line	0
psect	functab
fp__NETM_IMAP_DCM_STATUS_C_Implausi:
	ljmp	_NETM_IMAP_DCM_STATUS_C_Implausi
	global	btemp
	btemp set 07Eh

	DABS	1,126,2	;btemp
	global	wtemp0
	wtemp0 set btemp
	end
