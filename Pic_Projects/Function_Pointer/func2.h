#ifndef FUNC2
#define FUNC2

#include "typedefs.h"
typedef struct
{
  uint8_t status_c_transmission_implausible   :1;
  uint8_t dtcm_a1_implausible                 :1; 
  uint8_t status_c_dtcm_implausible           :1;
  uint8_t tgw_a1_implausible                  :1;
  uint8_t spare                               :4;
} IMAP_ImplausibilityControl_t;

/* GLOBAL Data */
extern IMAP_ImplausibilityControl_t IMAP_ImplausibilityControl;

/* FUNCTION PROTOTYPE's */
void FunctPtr2(void);
uint8_t incrementFunc2(uint8_t x);
uint8_t decrementFunc2(uint8_t x);
void NETM_IMAP_TCM_Implausible( bool_t Status );
void NETM_IMAP_DCM_A1_Implausible( bool_t Status );
void NETM_IMAP_DCM_STATUS_C_Implausible( bool_t Status );
void NETM_IMAP_ETM_LTM_Implausible( bool_t Status );

#endif