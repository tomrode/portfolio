#ifndef FUNC1
#define FUNC1

#include "typedefs.h"

/* TYPE DEFINITIONS */
typedef struct
  {
    uint16_t BitMsgPos;     /* position of message timeout in MsgTimeOut */
    uint8_t NodeIdent;      /* Node id (owner of message in timeout) */
    uint8_t MsgType;        /* message type */
    uint8_t(*MsgMissRecoveryFunc)(uint8_t);/* Recovery Callback function ptr */
  }NETM_msg_cfg_t;

 typedef enum
{
  INCREMENT,
  DECREMENT,
  SQUARE,
  CUBED
}MATH;

/* FUNCTION PROTOTYPE's */
uint8_t incrementFunc1(uint8_t x);
uint8_t decrementFunc1(uint8_t x);
uint8_t squareFunc1(uint8_t x);
uint8_t cubedFunc1(uint8_t x);


/* CONSTANTS */
//const NETM_msg_cfg_t NETM_msg_cfg[5] = {
/* uint16 BitMsgPos uint8NodeIdent     uint8MsgType       uint8 Fctptr(uint8)*/
//  {0xDEAD,          0x08,              0x99,              incrementFunc1}, 
//  {0xBEEF,          0x0C,              0xED,              decrementFunc1},
//  {0xFEED,          0x0A,              0x09,              squareFunc1},
//  {0xBEAD,          0x03,              0xDE,              cubedFunc1},
//  {0xFADE,          0x94,              0xFF,              NETM_IMAP_DCM_STATUS_C_Implausible},
//};

#endif