/* reg_callback.c */
#include<stdio.h>
#include"reg_callback.h"
 
/* registration goes here */
//void register_callback(void (*callback_t)(void), int x)
void register_callback(void (*callback_t)(void))
{
     
    
    //printf("inside register_callback\n");
    /* calling our callback function my_callback */
    (*callback_t)();                                  
}
