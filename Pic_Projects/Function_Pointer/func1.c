#include "func1.h"
#include "func2.h"




uint8_t incrementFunc1(uint8_t x)
{
  return (x+1);
}

uint8_t decrementFunc1(uint8_t x)
{
  return (x-1);
}

uint8_t squareFunc1(uint8_t x)
{
  return (x*x);
}

uint8_t cubedFunc1(uint8_t x)
{
  return (x*x*x);
}