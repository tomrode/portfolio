
/***********************************************
/*DATE 14 FEB 2013 
/* PROJECT NAME: UNIONS
/*
/*
/**********************************************/
#include "typedefs.h"

/*CONSTANT's*/
/*NO CONSTANTS*/

/*typedef*/
/*No typedefs*/

/*global variables*/
CANTXMESSAGE *buffer;
CANTXMESSAGE  Toms_message;
CANTXMESSAGE  TxIgnStat;
/*Function prototypes*/
void main(void);
void IgnMessage(void);
uint32_t Convert_EID(uint32_t EID_Message);
uint16_t Convert_SID(uint32_t EID_Message);


void main(void)
{
uint16_t SID;
uint32_t EID;
uint32_t DiagMessage = 0x18DA40F1;

SID = Convert_SID(DiagMessage);
EID = Convert_EID(DiagMessage);

  while(1)
  {
     buffer = &Toms_message; 
     buffer -> messageword[0] = 0;
     buffer -> messageword[1] = 0;
     buffer -> messageword[2] = 0;
     buffer -> messageword[3] = 0;
     buffer -> msgSID.SID = 0x636; /* CAN ID*/
     buffer -> msgEID.EID = 0x240F1;
     buffer -> msgEID.DLC = 4;     /* 4 bytes of payload*/
     buffer -> msgEID.SRR = 2;     /* SRR reg*/
     buffer -> data[0]    = 0xDE;
     buffer -> data[1]    = 0xAD;
     buffer -> data[2]    = 0xBE;
     buffer -> data[3]    = 0xEF;
     IgnMessage();   
   }
}
void IgnMessage(void)
{
     buffer = &TxIgnStat; 
     buffer -> messageword[0] = 0;
     buffer -> messageword[1] = 0;
     buffer -> messageword[2] = 0;
     buffer -> messageword[3] = 0;
     buffer -> msgSID.SID = 0x123; /* CAN ID*/
     buffer -> msgEID.DLC = 4;     /* 4 bytes of payload*/
     buffer -> msgEID.SRR = 2;     /* SRR reg*/
     buffer -> data[0]    = 0x12;
     buffer -> data[1]    = 0x33;
     buffer -> data[2]    = 0x66;
     buffer -> data[3]    = 0x88;
}

uint32_t Convert_EID(uint32_t EID_Message)
{
  return(EID_Message & 0x0003FFFF);
}

uint16_t Convert_SID(uint32_t EID_Message)
{
  uint32_t Hi; 
  Hi = (EID_Message >> 2);          /*0x18DA40F1 -> 0x0636903C */
  return(Hi >> 16);
}

         