;**********************************************************************
;   This file is a basic code template for assembly code generation   *
;   on the PIC16F887. This file contains the basic code               *
;   building blocks to build upon.                                    *
;                                                                     *
;   Refer to the MPASM User's Guide for additional information on     *
;   features of the assembler (Document DS33014).                     *
;                                                                     *
;   Refer to the respective PIC data sheet for additional             *
;   information on the instruction set.                               *
;                                                                     *
;**********************************************************************
;                                                                     *
;    Filename:	   A-D with Uart TX.asm                               *
;    Date:                                                            *
;    File Version:                                                    *
;                                                                     *
;    Author:                                                          *
;    Company:                                                         *
;                                                                     *
;                                                                     *
;**********************************************************************
;                                                                     *
;    Files Required: P16F887.INC                                      *
;                                                                     *
;**********************************************************************
;                                                                     *
;    Notes:   This will send out the high 2 bits first then the 
;             low 8 bits out the TXREG                                *
;                                                                     *
;**********************************************************************


	list		p=16f887	; list directive to define processor
	#include	<p16f887.inc>	; processor specific variable definitions


; '__CONFIG' directive is used to embed configuration data within .asm file.
; The labels following the directive are located in the respective .inc file.
; See respective data sheet for additional information on configuration word.

	__CONFIG    _CONFIG1, _LVP_OFF & _FCMEN_ON & _IESO_OFF & _BOR_OFF & _CPD_OFF & _CP_OFF & _MCLRE_ON & _PWRTE_ON & _WDT_OFF & _HS_OSC 
	__CONFIG    _CONFIG2, _WRT_OFF & _BOR21V



;***** VARIABLE DEFINITIONS
resultlo     EQU    0x7C        ; Result of AD lo
resulthi     EQU    0x7D        ; Result of AD hi
status_temp	 EQU	0x7E		; variable used for context saving
pclath_temp	 EQU	0x7F		; variable used for context saving


;**********************************************************************
	ORG     0x000             ; processor reset vector

	nop
    goto    main              ; go to beginning of program
   

	ORG     0x004             ; interrupt vector location

    retfie

; isr code can go here or be located as a call subroutine elsewhere


main
                   
                   movlw     0x70                ;Select External osc 8MHZ                     .
                   banksel   OSCCON
                   movwf     OSCCON        
                 
wait_stable
                   btfss     OSCCON,HTS          ; Is the high-speed internal oscillator stable?
                   goto      wait_stable         ; If not, wait until it is.

                   movlw     0x00                ; Disable all interrupts.
                   banksel   INTCON
                   movwf     INTCON
                   banksel   PIE1
                   movwf     PIE1
                  
                   movlw     0x00                ; SCKP = 0, BRG16 = 0 (8-BIT), WUE = 0, ABDEN = 0
                   banksel   BAUDCTL
                   movwf     BAUDCTL

                   movlw     33                  ; SRBR (LSB) =  33 is for 9600 baud
                   banksel   SPBRG
                   movwf     SPBRG
                   movlw     0                   ; SPBR (MSB) = 0
                   banksel   SPBRGH
                   movwf     SPBRGH

                   movlw     0x90              ; SPEN = 1, RX9 = 0, SREN = 0 (DONT CARE), CREN = 1 (RECEIVER ON), ADDEN = 0
                   banksel   RCSTA
                   movwf     RCSTA

                   movlw     0x24              ; CSRC = 0, TX9 = 0, TXEN = 1, SYNC = 0 (ASYNC MODE), SENDB = 0, BRGH = 1 (HIGH SPEED), TX9D = 0
                   banksel   TXSTA
                   movwf     TXSTA
 
                   movlw     0x00 
                   banksel   TRISD
                   movwf     TRISD  

                   banksel   TRISC
                   bsf       TRISC,RC7           ; RC7 is an input (RX).  

;**************************************************Timer 0 set up***************************************************************
                   banksel   TMR0                             
                   clrwdt                     ; timer0 initialization, clear wdT and prescaler   
                 
                   banksel   OPTION_REG       
                   
                   movlw     0xD0             ; Mask TMR0 select  ;Clearing:  T0CS,PSA,PS2,PS1,PS0
                   andwf     OPTION_REG,W     ; prescale bits
                   iorlw     0x07             ; set to 1:256 before it rolls over 128 micro-seconds later Setting: PS2,PS1,PS0
                   movwf     OPTION_REG                           
                     
;*****************************************************A/D Set up******************************************************************                   
                  
                   banksel    ADCON1
                   bsf        ADCON1,ADFM       ;right justify 
                    
                   movlw      0x01              ;make RA0 in input
                   banksel    TRISA
                   movwf      TRISA
                    
                   banksel    ANSEL
                   bsf        ANSEL,ANS0       ; Set RA0 as a analog input

                   movlw      0x01             ;setting FRC internal A/D clock,  analog channel = RA0 ,ADON = selected FRC is 2-6 us 
                   banksel    ADCON0
                   movwf      ADCON0


;******************************************************WAIT FROM CAPL HERE BEFORE DOING CONVERSION**************************************
start_receive
                   banksel    PIR1                   
                   btfss      PIR1,RCIF          ;test if receive bit happened after a transmit this is as of 4/13/2011
                   goto       start_receive
                   banksel    RCREG
                   movf       RCREG,W            ;used to clear the RCIF FLAG 
;******************************************************GOT SOMETHING FROM CAPL HERE************************************************************
;loop                   
                   bsf        ADCON0,GO           ; Start conversion
conversion_done 
                   btfsc      ADCON0,GO
                   goto       conversion_done
                  
                   banksel    ADRESH
                   movf       ADRESH,W           ;store lobyte in W then put into RAM variable
                   banksel    resulthi           ; this is all for the low 8 bits
                   movwf      resulthi 
                   banksel    PORTD
                   movwf      PORTD   
                   banksel    TXREG
                   movwf      TXREG


wait_resulthi                  
                   banksel    PIR1                   
                   btfss      PIR1,TXIF          ;test if stop bit happened after a transmit
                   goto       wait_resulthi
                  
                   movlw      0xF0                ; Set the timer full roll over of 32ms 
                   banksel    TMR0
                   movwf      TMR0

                   banksel    INTCON 
                   bcf        INTCON,T0IF         ; This must be cleared after a roll over

                   banksel    INTCON 
t0if_count         btfss      INTCON,T0IF          ; this will set every time timer0 rolls over but this flag must be reset 
                   goto       t0if_count                   

                  ; banksel    INTCON 
                  ; bcf        INTCON,T0IF         ; This must be cleared after a roll over
                 
                   banksel    ADRESL
                   movf       ADRESL,W
                   banksel    resultlo
                   movwf      resultlo            ;teston PORTD the low byte          
                   banksel    PORTD
                   movwf      PORTD 
                   banksel    TXREG
                   movwf      TXREG 
                   
                   ;movlw      0x00                ;Set the full roll over of 32ms 
                   ;banksel    TMR0
                   ;clrf       TMR0
                 

wait_resultlo                  
                   banksel    PIR1
                   btfss      PIR1,TXIF           ;test if stop bit happened after a transmit
                   goto       wait_resultlo
                   goto       start_receive  ;loop
	END                                         ; directive 'end of program'

