/*********************************************************************
 *
 *                  SPI API implementation file
 *
 *********************************************************************
 * FileName:        _spi_io_tbl_lipb.c
 * Dependencies:	Spi.h
 * 					Ports.h
 * 					_spi_io_tbl.h
 *
 * Processor:       PIC32
 *
 * Complier:        MPLAB C32
 *                  MPLAB IDE
 * Company:         Microchip Technology Inc..
 *
 * Software License Agreement
 *
 * The software supplied herewith by Microchip Technology Incorporated
 * (the �Company�) for its PIC32 Microcontroller is intended
 * and supplied to you, the Company�s customer, for use solely and
 * exclusively on Microchip PIC32 Microcontroller products.
 * The software is owned by the Company and/or its supplier, and is
 * protected under applicable copyright laws. All rights are reserved.
 * Any use in violation of the foregoing restrictions may subject the
 * user to criminal sanctions under applicable laws, as well as to
 * civil liability for the breach of the terms and conditions of this
 * license.
 *
 * THIS SOFTWARE IS PROVIDED IN AN �AS IS� CONDITION. NO WARRANTIES,
 * WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED
 * TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. THE COMPANY SHALL NOT,
 * IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL OR
 * CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 *
 * $Id$
 * $Name$
 *
 ********************************************************************/


#include <peripheral/spi.h>
#include <peripheral/ports.h>

#include "_spi_io_tbl.h"

#ifdef _SPI_DEF_CHN_

#if (((__PIC32_FEATURE_SET__ >= 300) && (__PIC32_FEATURE_SET__ <= 499)) || defined(__32MXGENERIC__))
	// adjusting for different naming PIC32MX3xxx vs PIC32MX5xxx and higher
	#ifdef _SPI1
		#define	_SPI1_CLK_PORT	SPI1_CLK_PORT
		#define	_SPI1_CLK_BIT	SPI1_CLK_BIT
		#define	_SPI1_SDO_PORT	SPI1_SDO_PORT
		#define	_SPI1_SDO_BIT	SPI1_SDO_BIT
		#define	_SPI1_SDI_PORT	SPI1_SDI_PORT
		#define	_SPI1_SDI_BIT	SPI1_SDI_BIT
		#define	_SPI1_SS_PORT	SPI1_SS_PORT
		#define	_SPI1_SS_BIT	SPI1_SS_BIT
	#endif // _SPI1

	#ifdef _SPI2
		#define	_SPI2_CLK_PORT	SPI2_CLK_PORT
		#define	_SPI2_CLK_BIT	SPI2_CLK_BIT
		#define	_SPI2_SDO_PORT	SPI2_SDO_PORT
		#define	_SPI2_SDO_BIT	SPI2_SDO_BIT
		#define	_SPI2_SDI_PORT	SPI2_SDI_PORT
		#define	_SPI2_SDI_BIT	SPI2_SDI_BIT
		#define	_SPI2_SS_PORT	SPI2_SS_PORT
		#define	_SPI2_SS_BIT	SPI2_SS_BIT
	#endif // _SPI2

#endif


	// table with values of the SPI IO ports and pins
	const _SpiIoDcpt	_SpiIoTbl[]=
	{
		// NOTE: SPI channel number 0 is invalid. For legacy reasons, the first SPI channel is 1. 
		{
			IOPORT_NUM, 0,		// invalid/unused entries
			IOPORT_NUM, 0,
			IOPORT_NUM, 0,
			IOPORT_NUM, 0,
		},


	#ifdef _SPI1
		{
			_SPI1_CLK_PORT, _SPI1_CLK_BIT,
			_SPI1_SDO_PORT, _SPI1_SDO_BIT,
			_SPI1_SDI_PORT, _SPI1_SDI_BIT,
			_SPI1_SS_PORT, _SPI1_SS_BIT
		},
	#else
		{
			IOPORT_NUM, 0,
			IOPORT_NUM, 0,
			IOPORT_NUM, 0,
			IOPORT_NUM, 0,
		},
	#endif	// _SPI1
		
	#if defined(_SPI2) 
		{
			_SPI2_CLK_PORT, _SPI2_CLK_BIT,
			_SPI2_SDO_PORT, _SPI2_SDO_BIT,
			_SPI2_SDI_PORT, _SPI2_SDI_BIT,
			_SPI2_SS_PORT, _SPI2_SS_BIT
		},
	#else
		{
			IOPORT_NUM, 0,
			IOPORT_NUM, 0,
			IOPORT_NUM, 0,
			IOPORT_NUM, 0,
		},
	#endif	// _SPI2 
			
	#ifdef _SPI3
		{
			_SPI3_CLK_PORT, _SPI3_CLK_BIT,
			_SPI3_SDO_PORT, _SPI3_SDO_BIT,
			_SPI3_SDI_PORT, _SPI3_SDI_BIT,
			_SPI3_SS_PORT, _SPI3_SS_BIT
		},
	#else
		{
			IOPORT_NUM, 0,
			IOPORT_NUM, 0,
			IOPORT_NUM, 0,
			IOPORT_NUM, 0,
		},
	#endif	// _SPI3

	#ifdef _SPI4
		{
			_SPI4_CLK_PORT, _SPI4_CLK_BIT,
			_SPI4_SDO_PORT, _SPI4_SDO_BIT,
			_SPI4_SDI_PORT, _SPI4_SDI_BIT,
			_SPI4_SS_PORT, _SPI4_SS_BIT
		},
	#else
		{
			IOPORT_NUM, 0,
			IOPORT_NUM, 0,
			IOPORT_NUM, 0,
			IOPORT_NUM, 0,
		},
	#endif	// _SPI4
		
	};



#endif	// _SPI_DEF_CHN_


