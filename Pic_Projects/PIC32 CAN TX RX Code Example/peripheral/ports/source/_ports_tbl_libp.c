/*********************************************************************
 *
 *                  Ports device specific table
 *
 *********************************************************************
 * FileName:        _PortsTblLibp.c
 * Dependencies:
 *
 * Processor:       PIC32
 *
 * Complier:        MPLAB C32
 *                  MPLAB IDE
 * Company:         Microchip Technology Inc..
 *
 * Software License Agreement
 *
 * The software supplied herewith by Microchip Technology Incorporated
 * (the �Company�) for its PIC32/PIC24 Microcontroller is intended
 * and supplied to you, the Company�s customer, for use solely and
 * exclusively on Microchip PIC32/PIC24 Microcontroller products.
 * The software is owned by the Company and/or its supplier, and is
 * protected under applicable copyright laws. All rights are reserved.
 * Any use in violation of the foregoing restrictions may subject the
 * user to criminal sanctions under applicable laws, as well as to
 * civil liability for the breach of the terms and conditions of this
 * license.
 *
 * THIS SOFTWARE IS PROVIDED IN AN �AS IS� CONDITION. NO WARRANTIES,
 * WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED
 * TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. THE COMPANY SHALL NOT,
 * IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL OR
 * CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 *
 * $Id$
 *
 ********************************************************************/
#include <p32xxxx.h>
#include <plib.h>
#include "_ports_tbl_libp.h"


// Case 1: implementation using IOPORT enumeration

const PortRegDcpt	PortRegTbl[IOPORT_NUM]=
{
#if defined _PORTA
	{(PortRegMap*)&TRISA,		0x00000000,		0x0},
#endif
#if defined _PORTB
	{(PortRegMap*)&TRISB,		0x0000ffff,		(AnConReg*)&AD1PCFG},
#endif
#if defined _PORTC
	{(PortRegMap*)&TRISC,		0x00000000,		0x0},
#endif
#if defined _PORTD
	{(PortRegMap*)&TRISD,		0x00000000,		0x0},
#endif
#if defined _PORTE
	{(PortRegMap*)&TRISE,		0x00000000,		0x0},
#endif
#if defined _PORTF
	{(PortRegMap*)&TRISF,		0x00000000,		0x0},
#endif
#if defined _PORTG
	{(PortRegMap*)&TRISG,		0x00000000,		0x0}
#endif
};

