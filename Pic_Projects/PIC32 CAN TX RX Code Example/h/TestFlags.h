#include "GenericTypeDefs.h"
#ifndef TEST_FLAGS_H
#define TEST_FLAGS_H
/* External Structure*/

       struct
            {
             UINT16 mainflag     : 1;
             UINT16 CAN1Init     : 1;  
             UINT16 CAN2Init     : 1;
             UINT16 Toggle_Portd : 1;
             UINT16 Timer1Init   : 1;
             UINT16 IsOneSecondUp: 1;
             UINT16 DONE         : 10;

             }TEST_FLAGS;
#endif