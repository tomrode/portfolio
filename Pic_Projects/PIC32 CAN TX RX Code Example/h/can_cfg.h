/**************************************************************
*  File D:\rtc_wa_KL15\BCM_NET_DIAG\CUSW\TOOLS\CANGEN\Sources\can_cfg.h
*  generated at Wed Dec 04 09:34:27 2013
*             Toolversion:   427
*               Bussystem:   CAN
*
*  generated out of CANdb:   D:\rtc_wa_KL15\BCM_NET_DIAG\CUSW\TOOLS\CANGEN\CCAN.dbc
*                            D:\rtc_wa_KL15\BCM_NET_DIAG\CUSW\TOOLS\CANGEN\BCAN.dbc

*            Manufacturer:   Fiat
*                for node:   BCM
*   Generation parameters:   Target system = FR60
*                            Compiler      = Fujitsu / Softune
*
* License information:       
*   -    Serialnumber:       CBD0800214
*   - Date of license:       19.9.2008
*
***************************************************************
Software is licensed for:    
Magneti Marelli Sistemi Elettronici S.p.A.
Fiat / SLP2 / MB91460P / Fujitsu Softune V60L01 / MB91F467
**************************************************************/
#ifndef CAN_CFG_H
#define CAN_CFG_H


#define DRV_API_CALL_TYPE
#define DRV_API_CALLBACK_TYPE


#define NM_API_CALL_TYPE
#define NM_API_CALLBACK_TYPE

#define IL_API_CALL_TYPE
#define IL_API_CALLBACK_TYPE

#define DIAG_API_CALL_TYPE
#define DIAG_API_CALLBACK_TYPE

#define TP_API_CALL_TYPE
#define TP_API_CALLBACK_TYPE

#define IL_INTERNAL_CALL_TYPE
#define DIAG_INTERNAL_CALL_TYPE
#define GW_INTERNAL_CALL_TYPE
#define NM_INTERNAL_CALL_TYPE
#define TP_INTERNAL_CALL_TYPE

#define C_DISABLE_CANCEL_IN_HW
#include "h\v_def.h"


#define kCanNumberOfChannels 2
#define kCanNumberOfInitObjects 2
#define C_ENABLE_OSEK_OS
#define C_ENABLE_MULTICHANNEL_API
#define C_ENABLE_RECEIVE_FCT
#define C_DISABLE_ECU_SWITCH_PASS
#define C_ENABLE_TRANSMIT_QUEUE
#define C_DISABLE_OVERRUN
#define C_DISABLE_CAN_TX_CONF_FCT
#define C_DISABLE_INTCTRL_BY_APPL
#define C_DISABLE_USER_CHECK
#define C_DISABLE_HARDWARE_CHECK
#define C_DISABLE_GEN_CHECK
#define C_DISABLE_INTERNAL_CHECK
#define C_DISABLE_RX_MSG_INDIRECTION
#define C_ENABLE_DYN_TX_OBJECTS
#define C_ENABLE_DYN_TX_ID
#define C_ENABLE_DYN_TX_DLC
#define C_DISABLE_DYN_TX_DATAPTR
#define C_DISABLE_DYN_TX_PRETRANS_FCT
#define C_DISABLE_DYN_TX_CONF_FCT
#define C_ENABLE_EXTENDED_ID
#define C_ENABLE_MIXED_ID
#define C_ENABLE_RANGE_EXTENDED_ID
#define C_ENABLE_EXTENDED_STATUS
#define C_DISABLE_TX_OBSERVE
#define C_ENABLE_HW_LOOP_TIMER
#define C_DISABLE_NOT_MATCHED_FCT
#define C_SECURITY_LEVEL 10
#define C_ENABLE_PART_OFFLINE
#define C_ENABLE_MSG_TRANSMIT
#define C_DISABLE_MSG_TRANSMIT_CONF_FCT
#define C_ENABLE_RX_BASICCAN_OBJECTS
#define C_ENABLE_RX_FULLCAN_OBJECTS
#define C_DISABLE_TX_FULLCAN_OBJECTS

#define C_DISABLE_RANGE_0
#define C_ENABLE_RANGE_1
#define C_ENABLE_RANGE_2
#define C_DISABLE_RANGE_3

#define C_ENABLE_MULTICAN
#define kCanNumberOfTxStatObjects 71

#define kCanNumberOfTxDynObjects 4

#define kCanNumberOfTxObjects (kCanNumberOfTxStatObjects+kCanNumberOfTxDynObjects)

#define kCanNumberOfRxFullCANObjects 50
#define kCanNumberOfRxBasicCANObjects 73

#define kCanNumberOfTxDirectObjects 0


#define kCanNumberOfConfFlags 71
#define kCanNumberOfIndFlags  121

#define kCanNumberOfHwChannels kCanNumberOfChannels

#define kCanNumberOfConfirmationFlags 9
#define kCanNumberOfIndicationFlags 16
#define kCanNumberOfRxObjects (kCanNumberOfRxFullCANObjects+kCanNumberOfRxBasicCANObjects)
#define kCanNumberOfUsedTxCANObjects 4
#define kCanNumberOfUsedRxBasicCANObjects 8
#define kCanNumberOfUsedCanTxIdTables 2
#define kCanNumberOfUsedCanRxIdTables 2
#define kCanNumberOfUnusedObjects 2


#define C_ENABLE_CONFIRMATION_FLAG
#define C_ENABLE_INDICATION_FLAG
#define C_ENABLE_PRETRANSMIT_FCT
#define C_ENABLE_CONFIRMATION_FCT
#define C_ENABLE_INDICATION_FCT
#define C_ENABLE_PRECOPY_FCT
#define C_ENABLE_COPY_TX_DATA
#define C_ENABLE_COPY_RX_DATA
#define C_ENABLE_DLC_CHECK
#define C_DISABLE_VARIABLE_DLC
#define C_DISABLE_DLC_FAILED_FCT
#define C_DISABLE_VARIABLE_RX_DATALEN
#define C_DISABLE_VARIABLE_RX_DATALEN_COPY
#define C_DISABLE_GENERIC_PRECOPY

#define C_SEND_GRP_NONE 0x0
#define C_SEND_GRP_ALL 0xff
#define C_SEND_GRP_APPLICATION 0x1
#define C_SEND_GRP_NETMANAGEMENT 0x2
#define C_SEND_GRP_2 0x4
#define C_SEND_GRP_3 0x8
#define C_SEND_GRP_4 0x10
#define C_SEND_GRP_5 0x20
#define C_SEND_GRP_6 0x40
#define C_SEND_GRP_7 0x80

/* CAN channel assignment */
#define CANCHANNEL_0
#define CANCHANNEL_1
#define kCanIndex0 0
#define kCanIndex1 1




#define C_DISABLE_XGATE_USED
#define C_SEARCH_LINEAR
#define C_DISABLE_FULLCAN_OVERRUN
#define C_DISABLE_INTERRUPT_LOCK_LEVEL
#define C_DISABLE_TX_POLLING
#define C_DISABLE_RX_FULLCAN_POLLING
#define C_DISABLE_RX_BASICCAN_POLLING
#define C_DISABLE_ERROR_POLLING
#define C_DISABLE_WAKEUP_POLLING
#define kCanTxIntIDMask 0x1ffffff0
#define CHANNEL_0_ICR	0x450

#define CHANNEL_1_ICR	0x450

#define C_ENABLE_INT_OSCAT2
#define C_PHYS_CAN_CHANNEL_0	0
#define C_LOG_CAN_CHANNEL_0	C_PHYS_CAN_CHANNEL_0
#define C_PHYS_CAN_CHANNEL_1	1
#define C_LOG_CAN_CHANNEL_1	C_PHYS_CAN_CHANNEL_1


#if !defined(C_ENABLE_APPLCANPRERXQUEUE)
/* all messages handled in the FIFO-Queue */
# define ApplCanPreRxQueue(a)  (kCanCopyData)
# define C_DISABLE_APPLCANPRERXQUEUE
#endif

#endif
