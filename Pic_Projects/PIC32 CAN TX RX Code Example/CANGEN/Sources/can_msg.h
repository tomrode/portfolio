/**************************************************************
*  File D:\rtc_wa_KL15\BCM_NET_DIAG\CUSW\TOOLS\CANGEN\Sources\can_msg.h
*  generated at Wed Dec 04 09:34:25 2013
*             Toolversion:   427
*               Bussystem:   CAN
*
*  generated out of CANdb:   D:\rtc_wa_KL15\BCM_NET_DIAG\CUSW\TOOLS\CANGEN\CCAN.dbc
*                            D:\rtc_wa_KL15\BCM_NET_DIAG\CUSW\TOOLS\CANGEN\BCAN.dbc

*            Manufacturer:   Fiat
*                for node:   BCM
*   Generation parameters:   Target system = FR60
*                            Compiler      = Fujitsu / Softune
*
* License information:       
*   -    Serialnumber:       CBD0800214
*   - Date of license:       19.9.2008
*
***************************************************************
Software is licensed for:    
Magneti Marelli Sistemi Elettronici S.p.A.
Fiat / SLP2 / MB91460P / Fujitsu Softune V60L01 / MB91F467
**************************************************************/

#ifndef CAN_BCM_H
#define CAN_BCM_H

#define __BCM__

#define VERSIONYEAR_0 0x13
#define VERSIONMONTH_0 0x00
#define VERSIONWEEK_0 0x25
#define VERSIONDAY_0 0x00

#define VERSIONYEAR_1 0x13
#define VERSIONMONTH_1 0x00
#define VERSIONWEEK_1 0x25
#define VERSIONDAY_1 0x00

#ifndef TOOLVERSION
#define TOOLVERSION 427
#endif
#ifndef DLLVERSION
#define DLLVERSION 427
#endif


#ifndef GEN_VERSION
#define GEN_VERSION 0x0427
#define GEN_BUGFIX_VERSION 0x60

V_MEMROM0 extern V_MEMROM1 canuint8 V_MEMROM2 kGenMainVersion;
V_MEMROM0 extern V_MEMROM1 canuint8 V_MEMROM2 kGenSubVersion;
V_MEMROM0 extern V_MEMROM1 canuint8 V_MEMROM2 kGenBugfixVersion;

#endif
/* Start FR60 skeleton H-File ----------------------------------------*/


#ifndef CAN_SKL_H
#define CAN_SKL_H

/*Version check*/
#define VTOOL_SKELETON_VERSION 0x01
#define VTOOL_SKELETON_BUGFIX_VERSION 0x00 

#endif /* CAN_SKL_H */

/* End FR60 skeleton H-File ------------------------------------------*/


/*************************************************************/
/* Signal structures                                          */
/*************************************************************/

/* Send messages */

typedef struct {
	canbittype 	STATUS_C_BCM2BatteryVoltageLevel: 7;
	canbittype 	unused0: 1;
	canbittype 	unused1: 8;
	canbittype 	unused2: 8;
	canbittype 	STATUS_C_BCM2EngineOffTimer_1: 8;
	canbittype 	STATUS_C_BCM2EngineOffTimer_0: 8;
	canbittype 	unused3: 8;
	canbittype 	unused4: 4;
	canbittype 	STATUS_C_BCM2IgnitionOffTimer_1: 4;
	canbittype 	STATUS_C_BCM2IgnitionOffTimer_0: 8;
} _c_STATUS_C_BCM2_msgType;

typedef struct {
	canbittype 	HVAC_INFOFT_HVAC_MD_STATSts: 4;
	canbittype 	unused0: 4;
	canbittype 	unused1: 8;
	canbittype 	unused2: 8;
	canbittype 	unused3: 8;
	canbittype 	unused4: 8;
	canbittype 	unused5: 7;
	canbittype 	HVAC_INFOStopStartEnable_1: 1;
	canbittype 	HVAC_INFOStopStartEnable_0: 2;
	canbittype 	unused6: 6;
	canbittype 	unused7: 8;
} _c_HVAC_INFO_msgType;

typedef struct {
	canbittype 	CBC_PT4WPR_SYS_OFF: 1;
	canbittype 	CBC_PT4WPR_SYS_LOW: 1;
	canbittype 	CBC_PT4WPR_SYS_HIGH: 1;
	canbittype 	CBC_PT4WPR_SYS_INT: 1;
	canbittype 	CBC_PT4WPR_SYS_AUTO: 1;
	canbittype 	CBC_PT4WPR_SYS_MIST: 1;
	canbittype 	CBC_PT4WPR_SYS_WASH: 1;
	canbittype 	CBC_PT4WPR_SYS_FLT: 1;
	canbittype 	CBC_PT4SpSt_Pad1: 3;
	canbittype 	CBC_PT4SpSt_Pad2: 3;
	canbittype 	CBC_PT4E_Mode_Sts: 2;
} _c_CBC_PT4_msgType;

typedef struct {
	canbittype 	BCM_KEYON_COUNTER_C_CANKeyOnCounter_1: 8;
	canbittype 	BCM_KEYON_COUNTER_C_CANKeyOnCounter_0: 8;
} _c_BCM_KEYON_COUNTER_C_CAN_msgType;

typedef struct {
	canbittype 	IBS4IBS_Tm_Lst_Reset_Sec_1: 8;
	canbittype 	IBS4IBS_Tm_Lst_Reset_Sec_0: 8;
	canbittype 	unused0: 8;
} _c_IBS4_msgType;

typedef struct {
	canbittype 	ECU_APPL_BCM_0ECU_APPL_BCM_7: 8;
	canbittype 	ECU_APPL_BCM_0ECU_APPL_BCM_6: 8;
	canbittype 	ECU_APPL_BCM_0ECU_APPL_BCM_5: 8;
	canbittype 	ECU_APPL_BCM_0ECU_APPL_BCM_4: 8;
	canbittype 	ECU_APPL_BCM_0ECU_APPL_BCM_3: 8;
	canbittype 	ECU_APPL_BCM_0ECU_APPL_BCM_2: 8;
	canbittype 	ECU_APPL_BCM_0ECU_APPL_BCM_1: 8;
	canbittype 	ECU_APPL_BCM_0ECU_APPL_BCM_0: 8;
} _c_ECU_APPL_BCM_0_msgType;

typedef struct {
	canbittype 	CONFIGURATION_DATA_CODE_REQUEST_0Digit_11: 4;
	canbittype 	CONFIGURATION_DATA_CODE_REQUEST_0Digit_10: 4;
	canbittype 	CONFIGURATION_DATA_CODE_REQUEST_0Digit_09: 4;
	canbittype 	CONFIGURATION_DATA_CODE_REQUEST_0Digit_08: 4;
	canbittype 	CONFIGURATION_DATA_CODE_REQUEST_0Digit_07: 4;
	canbittype 	CONFIGURATION_DATA_CODE_REQUEST_0Digit_06: 4;
	canbittype 	CONFIGURATION_DATA_CODE_REQUEST_0Digit_05: 4;
	canbittype 	CONFIGURATION_DATA_CODE_REQUEST_0Digit_04: 4;
	canbittype 	CONFIGURATION_DATA_CODE_REQUEST_0Digit_03: 4;
	canbittype 	CONFIGURATION_DATA_CODE_REQUEST_0Digit_02: 4;
	canbittype 	CONFIGURATION_DATA_CODE_REQUEST_0Digit_01: 4;
	canbittype 	unused0: 4;
} _c_CONFIGURATION_DATA_CODE_REQUEST_0_msgType;

typedef struct {
	canbittype 	unused0: 5;
	canbittype 	CBC_VTA_0TheftAlarmStatus: 3;
} _c_CBC_VTA_0_msgType;

typedef struct {
	canbittype 	DIAGNOSTIC_RESPONSE_BCM_0N_PDU_7: 8;
	canbittype 	DIAGNOSTIC_RESPONSE_BCM_0N_PDU_6: 8;
	canbittype 	DIAGNOSTIC_RESPONSE_BCM_0N_PDU_5: 8;
	canbittype 	DIAGNOSTIC_RESPONSE_BCM_0N_PDU_4: 8;
	canbittype 	DIAGNOSTIC_RESPONSE_BCM_0N_PDU_3: 8;
	canbittype 	DIAGNOSTIC_RESPONSE_BCM_0N_PDU_2: 8;
	canbittype 	DIAGNOSTIC_RESPONSE_BCM_0N_PDU_1: 8;
	canbittype 	DIAGNOSTIC_RESPONSE_BCM_0N_PDU_0: 8;
} _c_DIAGNOSTIC_RESPONSE_BCM_0_msgType;

typedef struct {
	canbittype 	unused0: 8;
	canbittype 	unused2: 4;
	canbittype 	STATUS_B_CAN2PowerModeSts: 2;
	canbittype 	unused1: 2;
	canbittype 	unused4: 5;
	canbittype 	STATUS_B_CAN2BCMAutoCrankSts: 1;
	canbittype 	unused3: 2;
	canbittype 	unused5: 8;
	canbittype 	unused6: 8;
	canbittype 	unused7: 8;
	canbittype 	unused8: 8;
	canbittype 	unused9: 8;
} _c_STATUS_B_CAN2_msgType;

typedef struct {
	canbittype 	EXTERNAL_LIGHTS_0BrakePedalSts: 1;
	canbittype 	unused0: 7;
	canbittype 	EXTERNAL_LIGHTS_0StopLightSts: 1;
	canbittype 	EXTERNAL_LIGHTS_0RHParkingLightSts: 1;
	canbittype 	EXTERNAL_LIGHTS_0LHParkingLightSts: 1;
	canbittype 	EXTERNAL_LIGHTS_0HighBeamSts: 1;
	canbittype 	EXTERNAL_LIGHTS_0LowBeamSts: 1;
	canbittype 	EXTERNAL_LIGHTS_0FrontFogLightSts: 1;
	canbittype 	unused1: 2;
	canbittype 	unused3: 1;
	canbittype 	EXTERNAL_LIGHTS_0LHTurnSignalSts: 1;
	canbittype 	EXTERNAL_LIGHTS_0RHTurnSignalSts: 1;
	canbittype 	unused2: 5;
	canbittype 	EXTERNAL_LIGHTS_0RHTurnLightFault: 1;
	canbittype 	EXTERNAL_LIGHTS_0LHTurnLightFault: 1;
	canbittype 	unused5: 1;
	canbittype 	EXTERNAL_LIGHTS_0StopLightFault: 1;
	canbittype 	unused4: 4;
	canbittype 	unused7: 3;
	canbittype 	EXTERNAL_LIGHTS_0ParkingLightFault: 1;
	canbittype 	unused6: 3;
	canbittype 	EXTERNAL_LIGHTS_0HighBeamFault: 1;
	canbittype 	EXTERNAL_LIGHTS_0LowBeamFault: 1;
	canbittype 	unused8: 7;
} _c_EXTERNAL_LIGHTS_0_msgType;

typedef struct {
	canbittype 	ASBM1_CONTROLPAMRequestSts: 1;
	canbittype 	ASBM1_CONTROLLDWRequestSts: 1;
	canbittype 	ASBM1_CONTROLFCWRequestSts: 1;
	canbittype 	ASBM1_CONTROLPPARequestSts: 1;
	canbittype 	unused0: 4;
	canbittype 	unused1: 8;
	canbittype 	unused2: 8;
	canbittype 	unused3: 8;
} _c_ASBM1_CONTROL_msgType;

typedef struct {
	canbittype 	HVAC_INFO2A2D_FT_HVAC_BLOWER_VOLTAGE_1: 8;
	canbittype 	HVAC_INFO2A2D_FT_HVAC_BLOWER_VOLTAGE_0: 2;
	canbittype 	HVAC_INFO2A2D_DRV_TEMP_DR_POS_1: 6;
	canbittype 	HVAC_INFO2A2D_DRV_TEMP_DR_POS_0: 4;
	canbittype 	unused0: 4;
	canbittype 	unused1: 8;
} _c_HVAC_INFO2_msgType;

typedef struct {
	canbittype 	IBS2PN14_LS_Actv: 1;
	canbittype 	unused0: 7;
	canbittype 	unused1: 5;
	canbittype 	IBS2IBS_SOC_1: 3;
	canbittype 	IBS2IBS_SOC_0: 5;
	canbittype 	IBS2IBS_SOF_Q_1: 3;
	canbittype 	IBS2IBS_SOF_Q_0: 5;
	canbittype 	IBS2IBS_SOH_Q_1: 3;
	canbittype 	IBS2IBS_SOH_Q_0: 7;
	canbittype 	IBS2Batt_ST_Crit: 1;
	canbittype 	IBS2IBS_SOF_t_1: 8;
	canbittype 	IBS2IBS_SOF_t_0: 2;
	canbittype 	IBS2IBS_SOC_Accuracy: 2;
	canbittype 	IBS2IBS_SOF_Q_Accuracy: 2;
	canbittype 	IBS2IBS_SOH_Q_Accuracy: 2;
	canbittype 	IBS2IBS_SOF_t_Accuracy: 2;
	canbittype 	IBS2PLG_Req_PE: 1;
	canbittype 	unused2: 5;
} _c_IBS2_msgType;

typedef struct {
	canbittype 	unused0: 2;
	canbittype 	VEHICLE_SPEED_ODOMETER_0VehicleSpeedFailSts: 1;
	canbittype 	VEHICLE_SPEED_ODOMETER_0VehicleSpeed_1: 5;
	canbittype 	VEHICLE_SPEED_ODOMETER_0VehicleSpeed_0: 8;
	canbittype 	VEHICLE_SPEED_ODOMETER_0FUEL_VOLT2: 8;
	canbittype 	VEHICLE_SPEED_ODOMETER_0TravelDistance: 8;
	canbittype 	VEHICLE_SPEED_ODOMETER_0TotalOdometer_2: 8;
	canbittype 	VEHICLE_SPEED_ODOMETER_0TotalOdometer_1: 8;
	canbittype 	VEHICLE_SPEED_ODOMETER_0TotalOdometer_0: 4;
	canbittype 	unused1: 4;
	canbittype 	VEHICLE_SPEED_ODOMETER_0FUEL_VOLT: 8;
} _c_VEHICLE_SPEED_ODOMETER_0_msgType;

typedef struct {
	canbittype 	unused1: 1;
	canbittype 	STATUS_C_BCMBrakeFluidLevelSts: 1;
	canbittype 	STATUS_C_BCMHandBrakeSts: 1;
	canbittype 	unused0: 5;
	canbittype 	STATUS_C_BCMBonnetSts: 1;
	canbittype 	STATUS_C_BCMRHatchSts: 1;
	canbittype 	STATUS_C_BCMRHRDoorSts: 1;
	canbittype 	STATUS_C_BCMLHRDoorSts: 1;
	canbittype 	STATUS_C_BCMPsngrDoorSts: 1;
	canbittype 	STATUS_C_BCMDriverDoorSts: 1;
	canbittype 	unused2: 2;
	canbittype 	unused3: 8;
	canbittype 	unused4: 5;
	canbittype 	STATUS_C_BCMDoorLockLastElSts: 2;
	canbittype 	STATUS_C_BCMRearLockLastElSts: 1;
	canbittype 	unused5: 8;
	canbittype 	unused6: 8;
	canbittype 	STATUS_C_BCMExteriorRearReleaseSwitchSts: 1;
	canbittype 	unused7: 7;
	canbittype 	unused8: 8;
} _c_STATUS_C_BCM_msgType;

typedef struct {
	canbittype 	unused0: 8;
	canbittype 	unused2: 5;
	canbittype 	STATUS_B_CANCompressorACReqSts: 1;
	canbittype 	STATUS_B_CANCityModeSts: 1;
	canbittype 	unused1: 1;
	canbittype 	unused3: 8;
	canbittype 	STATUS_B_CANExternalTemperature: 8;
	canbittype 	STATUS_B_CANExternalTemperatureFailSts: 1;
	canbittype 	unused6: 1;
	canbittype 	STATUS_B_CANDriveStyleSts: 3;
	canbittype 	unused5: 1;
	canbittype 	STATUS_B_CANRechargeSts: 1;
	canbittype 	unused4: 1;
	canbittype 	unused7: 8;
	canbittype 	STATUS_B_CANBrakePadWearSts: 1;
	canbittype 	STATUS_B_CANBrakeFluidLevelSts: 1;
	canbittype 	STATUS_B_CANHandBrakeSts: 1;
	canbittype 	STATUS_B_CANRHeatedWindowSts: 1;
	canbittype 	unused9: 2;
	canbittype 	STATUS_B_CANFrontWiperMoveSts: 1;
	canbittype 	unused8: 1;
	canbittype 	unused10: 8;
} _c_STATUS_B_CAN_msgType;

typedef struct {
	canbittype 	IBS3IBS_SOF_V: 8;
	canbittype 	unused0: 3;
	canbittype 	IBS3IBS_Error_NVM: 1;
	canbittype 	IBS3IBS_Error_Ident: 1;
	canbittype 	IBS3IBS_Error_Calib: 1;
	canbittype 	IBS3IBS_SOF_V_Accuracy: 2;
	canbittype 	IBS3Converter_2_State: 2;
	canbittype 	IBS3Converter_2_Mode: 2;
	canbittype 	IBS3Converter_1_Mode: 2;
	canbittype 	IBS3Converter_1_State: 2;
	canbittype 	IBS3IBS_SOF_V1: 8;
} _c_IBS3_msgType;

typedef struct {
	canbittype 	IBS1IBS_V_BATT_1: 8;
	canbittype 	IBS1IBS_V_BATT_0: 6;
	canbittype 	IBS1IBS_I_BATT_2: 2;
	canbittype 	IBS1IBS_I_BATT_1: 8;
	canbittype 	IBS1IBS_I_BATT_0: 6;
	canbittype 	IBS1IBS_T_BATT_1: 2;
	canbittype 	IBS1IBS_T_BATT_0: 6;
	canbittype 	IBS1IBS_PreWakeupVoltage_1: 2;
	canbittype 	IBS1IBS_PreWakeupVoltage_0: 8;
	canbittype 	IBS1IBS_Current_Status: 2;
	canbittype 	IBS1IBS_Voltage_Status: 2;
	canbittype 	IBS1IBS_Temp_Status: 2;
	canbittype 	IBS1IBS_I_RANGE: 2;
	canbittype 	unused0: 8;
} _c_IBS1_msgType;

typedef struct {
	canbittype 	BATTERY_INFOBatteryVoltageLevelHP_1: 8;
	canbittype 	BATTERY_INFOBatteryVoltageLevelHP_0: 1;
	canbittype 	unused0: 7;
	canbittype 	BATTERY_INFOVoltageMin: 8;
	canbittype 	BATTERY_INFOVoltageMax: 8;
	canbittype 	unused1: 8;
	canbittype 	unused2: 8;
	canbittype 	unused4: 4;
	canbittype 	BATTERY_INFOBCMSAMFailSts: 1;
	canbittype 	unused3: 3;
	canbittype 	unused5: 8;
} _c_BATTERY_INFO_msgType;

typedef struct {
	canbittype 	CFG_FeatureCFG_FeatureCntrl: 8;
	canbittype 	unused0: 6;
	canbittype 	CFG_FeatureCFG_STAT_RQCntrl: 2;
	canbittype 	CFG_FeatureCFG_SETCntrl: 8;
} _c_CFG_Feature_msgType;

typedef struct {
	canbittype 	CBC_PT3DriverDoorAjarRawValSts: 4;
	canbittype 	unused0: 4;
	canbittype 	CBC_PT3PsngrDoorAjarRawValSts: 4;
	canbittype 	unused1: 4;
	canbittype 	unused2: 4;
	canbittype 	CBC_PT3BonnetAjarRawValSts: 4;
	canbittype 	CBC_PT3DriverDoorAjarXtionSts: 2;
	canbittype 	CBC_PT3PsngrDoorAjarXtionSts: 2;
	canbittype 	CBC_PT3DriverDoor2AjarRawValSts: 4;
	canbittype 	CBC_PT3DriverDoor2AjarXtionSts: 2;
	canbittype 	unused3: 6;
} _c_CBC_PT3_msgType;

typedef struct {
	canbittype 	CBC_PT1PanelIntensitySts: 8;
	canbittype 	CBC_PT1TRAC_PSDSts: 1;
	canbittype 	CBC_PT1HILL_DES_Rq: 2;
	canbittype 	CBC_PT1SelectSpdSwSts: 2;
	canbittype 	unused0: 1;
	canbittype 	CBC_PT1FT_WPR_NOT_PRK: 1;
	canbittype 	CBC_PT1InternalLightSts: 1;
	canbittype 	CBC_PT1ExternalTemperatureAD_Voltage: 8;
	canbittype 	unused1: 8;
	canbittype 	unused2: 8;
	canbittype 	CBC_PT1AcOutputCurrentSts: 8;
} _c_CBC_PT1_msgType;

typedef struct {
	canbittype 	WAKE_C_BCMMainWakeSts: 1;
	canbittype 	WAKE_C_BCMWakeRsn: 7;
	canbittype 	WAKE_C_BCMWakeCntr: 8;
	canbittype 	unused0: 8;
	canbittype 	unused2: 2;
	canbittype 	WAKE_C_BCMNode21: 1;
	canbittype 	unused1: 5;
	canbittype 	unused3: 8;
	canbittype 	unused5: 2;
	canbittype 	WAKE_C_BCMNode5: 1;
	canbittype 	WAKE_C_BCMNode4: 1;
	canbittype 	unused4: 1;
	canbittype 	WAKE_C_BCMNode2: 1;
	canbittype 	WAKE_C_BCMNode1: 1;
	canbittype 	WAKE_C_BCMNode0: 1;
	canbittype 	unused6: 8;
	canbittype 	unused7: 8;
} _c_WAKE_C_BCM_msgType;

typedef struct {
	canbittype 	CBC_PT2ACC_DLY_ACTSts: 1;
	canbittype 	CBC_PT2RemStActvSts: 1;
	canbittype 	CBC_PT2StTypSts: 3;
	canbittype 	CBC_PT2CmdIgnSts: 3;
	canbittype 	CBC_PT2RemSt_InhibitSts: 6;
	canbittype 	CBC_PT2KeyInIgnSts: 2;
	canbittype 	CBC_PT2FOBSearchRequest: 2;
	canbittype 	CBC_PT2StartRelayBCMSts: 1;
	canbittype 	CBC_PT2StartRelayBCMFault: 1;
	canbittype 	CBC_PT2PanicModeActive: 1;
	canbittype 	unused0: 3;
	canbittype 	unused1: 4;
	canbittype 	CBC_PT2MessageCounter: 4;
	canbittype 	CBC_PT2CRC: 8;
} _c_CBC_PT2_msgType;

typedef struct {
	canbittype 	CBC_PT2ACC_DLY_ACTSts: 1;
	canbittype 	CBC_PT2RemStActvSts: 1;
	canbittype 	CBC_PT2StTypSts: 3;
	canbittype 	CBC_PT2CmdIgnSts: 3;
	canbittype 	CBC_PT2RemSt_InhibitSts: 6;
	canbittype 	CBC_PT2KeyInIgnSts: 2;
	canbittype 	CBC_PT2FOBSearchRequest: 2;
	canbittype 	CBC_PT2StartRelayBCMSts: 1;
	canbittype 	CBC_PT2StartRelayBCMFault: 1;
	canbittype 	CBC_PT2PanicModeActive: 1;
	canbittype 	unused0: 3;
	canbittype 	unused1: 4;
	canbittype 	CBC_PT2MessageCounter: 4;
	canbittype 	CBC_PT2CRC: 8;
} _c_CBC_PT2_RDS_msgType;

typedef struct {
	canbittype 	BCM_COMMANDTerrainSwStat: 2;
	canbittype 	BCM_COMMANDBrakePedalSwitchNOFailSts: 1;
	canbittype 	BCM_COMMANDBrakePedalSwitchNCFailSts: 1;
	canbittype 	unused0: 4;
	canbittype 	unused1: 4;
	canbittype 	BCM_COMMANDAWDLowSwRq: 2;
	canbittype 	BCM_COMMANDAWDNeutralSwRq: 2;
	canbittype 	BCM_COMMANDELockerSwRq: 2;
	canbittype 	unused3: 3;
	canbittype 	BCM_COMMANDBrakePedalSwitchNOSts: 1;
	canbittype 	BCM_COMMANDBrakePedalSwitchNCSts: 1;
	canbittype 	unused2: 1;
	canbittype 	unused4: 4;
	canbittype 	BCM_COMMANDMessageCounter: 4;
	canbittype 	BCM_COMMANDCRC: 8;
} _c_BCM_COMMAND_msgType;

typedef struct {
	canbittype 	BCM_COMMANDTerrainSwStat: 2;
	canbittype 	BCM_COMMANDBrakePedalSwitchNOFailSts: 1;
	canbittype 	BCM_COMMANDBrakePedalSwitchNCFailSts: 1;
	canbittype 	unused0: 4;
	canbittype 	unused1: 4;
	canbittype 	BCM_COMMANDAWDLowSwRq: 2;
	canbittype 	BCM_COMMANDAWDNeutralSwRq: 2;
	canbittype 	BCM_COMMANDELockerSwRq: 2;
	canbittype 	unused3: 3;
	canbittype 	BCM_COMMANDBrakePedalSwitchNOSts: 1;
	canbittype 	BCM_COMMANDBrakePedalSwitchNCSts: 1;
	canbittype 	unused2: 1;
	canbittype 	unused4: 4;
	canbittype 	BCM_COMMANDMessageCounter: 4;
	canbittype 	BCM_COMMANDCRC: 8;
} _c_BCM_COMMAND_RDS_msgType;

typedef struct {
	canbittype 	BCM_CODE_TRM_REQUESTControlEncoding: 8;
	canbittype 	BCM_CODE_TRM_REQUESTMiniCryptFCode_1: 8;
	canbittype 	BCM_CODE_TRM_REQUESTMiniCryptFCode_0: 8;
	canbittype 	BCM_CODE_TRM_REQUESTMiniCrypt_Rnd_3: 8;
	canbittype 	BCM_CODE_TRM_REQUESTMiniCrypt_Rnd_2: 8;
	canbittype 	BCM_CODE_TRM_REQUESTMiniCrypt_Rnd_1: 8;
	canbittype 	BCM_CODE_TRM_REQUESTMiniCrypt_Rnd_0: 8;
	canbittype 	BCM_CODE_TRM_REQUESTTxpAuthRequest: 1;
	canbittype 	BCM_CODE_TRM_REQUESTTxpReadRequest: 1;
	canbittype 	unused0: 6;
} _c_BCM_CODE_TRM_REQUEST_msgType;

typedef struct {
	canbittype 	BCM_CODE_ESL_REQUESTControlEncoding: 8;
	canbittype 	BCM_CODE_ESL_REQUESTMiniCryptFCode_1: 8;
	canbittype 	BCM_CODE_ESL_REQUESTMiniCryptFCode_0: 8;
	canbittype 	BCM_CODE_ESL_REQUESTMiniCrypt_Rnd_3: 8;
	canbittype 	BCM_CODE_ESL_REQUESTMiniCrypt_Rnd_2: 8;
	canbittype 	BCM_CODE_ESL_REQUESTMiniCrypt_Rnd_1: 8;
	canbittype 	BCM_CODE_ESL_REQUESTMiniCrypt_Rnd_0: 8;
	canbittype 	BCM_CODE_ESL_REQUESTESLLockUnlockReq: 2;
	canbittype 	unused0: 6;
} _c_BCM_CODE_ESL_REQUEST_msgType;

typedef struct {
	canbittype 	BCM_MINICRYPT_ACKMinicryptReceptionSts: 1;
	canbittype 	unused0: 7;
} _c_BCM_MINICRYPT_ACK_msgType;

typedef struct {
	canbittype 	IMMO_CODE_RESPONSEControlEncoding: 8;
	canbittype 	IMMO_CODE_RESPONSEMKKey1org21: 8;
	canbittype 	IMMO_CODE_RESPONSEMKKey2org22: 8;
	canbittype 	IMMO_CODE_RESPONSEMKKey_3: 8;
	canbittype 	IMMO_CODE_RESPONSEMKKey_4: 8;
	canbittype 	IMMO_CODE_RESPONSEMKKey_5: 8;
	canbittype 	IMMO_CODE_RESPONSEMKKey_6: 8;
} _c_IMMO_CODE_RESPONSE_msgType;

typedef struct {
	canbittype 	unused0: 8;
	canbittype 	unused1: 8;
	canbittype 	unused2: 8;
	canbittype 	unused3: 8;
	canbittype 	unused4: 8;
	canbittype 	unused5: 8;
	canbittype 	unused6: 8;
	canbittype 	unused7: 8;
} _c_TxDynamicMsg0_0_msgType;

typedef struct {
	canbittype 	unused0: 8;
	canbittype 	unused1: 8;
	canbittype 	unused2: 8;
	canbittype 	unused3: 8;
	canbittype 	unused4: 8;
	canbittype 	unused5: 8;
	canbittype 	unused6: 8;
	canbittype 	unused7: 8;
} _c_TxDynamicMsg1_0_msgType;

typedef struct {
	canbittype 	ECU_APPL_BCM_1ECU_APPL_BCM_7: 8;
	canbittype 	ECU_APPL_BCM_1ECU_APPL_BCM_6: 8;
	canbittype 	ECU_APPL_BCM_1ECU_APPL_BCM_5: 8;
	canbittype 	ECU_APPL_BCM_1ECU_APPL_BCM_4: 8;
	canbittype 	ECU_APPL_BCM_1ECU_APPL_BCM_3: 8;
	canbittype 	ECU_APPL_BCM_1ECU_APPL_BCM_2: 8;
	canbittype 	ECU_APPL_BCM_1ECU_APPL_BCM_1: 8;
	canbittype 	ECU_APPL_BCM_1ECU_APPL_BCM_0: 8;
} _c_ECU_APPL_BCM_1_msgType;

typedef struct {
	canbittype 	DTO_BCMData_0: 8;
	canbittype 	DTO_BCMData_1: 8;
	canbittype 	DTO_BCMData_2: 8;
	canbittype 	DTO_BCMData_3: 8;
	canbittype 	DTO_BCMData_4: 8;
	canbittype 	DTO_BCMData_5: 8;
	canbittype 	DTO_BCMData_6: 8;
	canbittype 	DTO_BCMData_7: 8;
} _c_DTO_BCM_msgType;

typedef struct {
	canbittype 	CONFIGURATION_DATA_CODE_REQUEST_1Digit_11: 4;
	canbittype 	CONFIGURATION_DATA_CODE_REQUEST_1Digit_10: 4;
	canbittype 	CONFIGURATION_DATA_CODE_REQUEST_1Digit_09: 4;
	canbittype 	CONFIGURATION_DATA_CODE_REQUEST_1Digit_08: 4;
	canbittype 	CONFIGURATION_DATA_CODE_REQUEST_1Digit_07: 4;
	canbittype 	CONFIGURATION_DATA_CODE_REQUEST_1Digit_06: 4;
	canbittype 	CONFIGURATION_DATA_CODE_REQUEST_1Digit_05: 4;
	canbittype 	CONFIGURATION_DATA_CODE_REQUEST_1Digit_04: 4;
	canbittype 	CONFIGURATION_DATA_CODE_REQUEST_1Digit_03: 4;
	canbittype 	CONFIGURATION_DATA_CODE_REQUEST_1Digit_02: 4;
	canbittype 	CONFIGURATION_DATA_CODE_REQUEST_1Digit_01: 4;
	canbittype 	unused0: 4;
} _c_CONFIGURATION_DATA_CODE_REQUEST_1_msgType;

typedef struct {
	canbittype 	IBS_2PN14_LS_Actv: 1;
	canbittype 	IBS_2PN14_Key_Off_Load_Shed: 2;
	canbittype 	IBS_2PN14_LS_Lvl1: 1;
	canbittype 	IBS_2PN14_LS_Lvl2: 1;
	canbittype 	IBS_2PN14_LS_Lvl3: 1;
	canbittype 	IBS_2PN14_LS_Lvl4: 1;
	canbittype 	IBS_2PN14_LS_Lvl5: 1;
	canbittype 	IBS_2PN14_LS_Lvl6: 1;
	canbittype 	IBS_2PN14_LS_Lvl7: 1;
	canbittype 	unused1: 3;
	canbittype 	IBS_2Batt_ST_Crit: 1;
	canbittype 	unused0: 2;
	canbittype 	unused2: 8;
	canbittype 	unused3: 8;
} _c_IBS_2_msgType;

typedef struct {
	canbittype 	HUMIDITY_1000LGLASSTEMP_1: 8;
	canbittype 	HUMIDITY_1000LGLASSTEMP_0: 3;
	canbittype 	HUMIDITY_1000LHumidityAirTmp_1: 5;
	canbittype 	HUMIDITY_1000LHumidityAirTmp_0: 6;
	canbittype 	HUMIDITY_1000LDewpointTmp_2: 2;
	canbittype 	HUMIDITY_1000LDewpointTmp_1: 8;
	canbittype 	HUMIDITY_1000LDewpointTmp_0: 1;
	canbittype 	HUMIDITY_1000LRELHUMIDITY_1: 7;
	canbittype 	HUMIDITY_1000LRELHUMIDITY_0: 1;
	canbittype 	unused0: 7;
	canbittype 	unused1: 8;
	canbittype 	unused2: 8;
} _c_HUMIDITY_1000L_msgType;

typedef struct {
	canbittype 	COMPASS_A1VAR_VAL: 4;
	canbittype 	COMPASS_A1CMP_DIR: 4;
	canbittype 	unused1: 4;
	canbittype 	COMPASS_A1IN_CAL_MD: 1;
	canbittype 	unused0: 2;
	canbittype 	COMPASS_A1CMP_DIR_RAW_1: 1;
	canbittype 	COMPASS_A1CMP_DIR_RAW_0: 8;
} _c_COMPASS_A1_msgType;

typedef struct {
	canbittype 	CBC_I5WPR_SYS_OFF: 1;
	canbittype 	CBC_I5WPR_SYS_LOW: 1;
	canbittype 	CBC_I5WPR_SYS_HIGH: 1;
	canbittype 	CBC_I5WPR_SYS_INT: 1;
	canbittype 	CBC_I5WPR_SYS_AUTO: 1;
	canbittype 	CBC_I5WPR_SYS_MIST: 1;
	canbittype 	CBC_I5WPR_SYS_WASH: 1;
	canbittype 	CBC_I5WPR_SYS_FLT: 1;
	canbittype 	CBC_I5IGN_OFF_TIME_LNG_1: 8;
	canbittype 	CBC_I5IGN_OFF_TIME_LNG_0: 4;
	canbittype 	unused0: 4;
	canbittype 	unused1: 8;
} _c_CBC_I5_msgType;

typedef struct {
	canbittype 	BCM_KEYON_COUNTER_B_CANKeyOnCounter_1: 8;
	canbittype 	BCM_KEYON_COUNTER_B_CANKeyOnCounter_0: 8;
} _c_BCM_KEYON_COUNTER_B_CAN_msgType;

typedef struct {
	canbittype 	AMB_TEMP_DISPAMB_TEMP_AVG_F: 8;
	canbittype 	AMB_TEMP_DISPAMB_TEMP_AVG_C: 8;
} _c_AMB_TEMP_DISP_msgType;

typedef struct {
	canbittype 	WCPM_STATUSDevice_Docked: 1;
	canbittype 	WCPM_STATUSDevice_Battery_Status: 4;
	canbittype 	WCPM_STATUSWCPM_Error: 1;
	canbittype 	WCPM_STATUSForeign_Object: 1;
	canbittype 	unused0: 1;
	canbittype 	unused1: 8;
} _c_WCPM_STATUS_msgType;

typedef struct {
	canbittype 	GE_BLwsAngleSts_1: 8;
	canbittype 	GE_BLwsAngleSts_0: 8;
	canbittype 	unused0: 5;
	canbittype 	GE_BLwsFailSts: 2;
	canbittype 	GE_BLwsAngleValidDataSts: 1;
} _c_GE_B_msgType;

typedef struct {
	canbittype 	ENVIRONMENTAL_CONDITIONSExternalTemperature: 8;
	canbittype 	ENVIRONMENTAL_CONDITIONSExternalTemperatureFailSts: 1;
	canbittype 	ENVIRONMENTAL_CONDITIONSBatteryVoltageLevel: 7;
	canbittype 	unused0: 8;
	canbittype 	unused1: 8;
	canbittype 	unused2: 8;
	canbittype 	ENVIRONMENTAL_CONDITIONSAtmosphericPressure: 8;
	canbittype 	unused3: 8;
	canbittype 	unused4: 8;
} _c_ENVIRONMENTAL_CONDITIONS_msgType;

typedef struct {
	canbittype 	unused0: 5;
	canbittype 	CBC_VTA_1TheftAlarmStatus: 3;
} _c_CBC_VTA_1_msgType;

typedef struct {
	canbittype 	DIAGNOSTIC_RESPONSE_BCM_1N_PDU_7: 8;
	canbittype 	DIAGNOSTIC_RESPONSE_BCM_1N_PDU_6: 8;
	canbittype 	DIAGNOSTIC_RESPONSE_BCM_1N_PDU_5: 8;
	canbittype 	DIAGNOSTIC_RESPONSE_BCM_1N_PDU_4: 8;
	canbittype 	DIAGNOSTIC_RESPONSE_BCM_1N_PDU_3: 8;
	canbittype 	DIAGNOSTIC_RESPONSE_BCM_1N_PDU_2: 8;
	canbittype 	DIAGNOSTIC_RESPONSE_BCM_1N_PDU_1: 8;
	canbittype 	DIAGNOSTIC_RESPONSE_BCM_1N_PDU_0: 8;
} _c_DIAGNOSTIC_RESPONSE_BCM_1_msgType;

typedef struct {
	canbittype 	unused0: 8;
	canbittype 	unused3: 2;
	canbittype 	GW_C_I2DynamicGridSts: 1;
	canbittype 	unused2: 2;
	canbittype 	GW_C_I2CsoPEEnableDisableSts: 1;
	canbittype 	unused1: 2;
	canbittype 	unused4: 8;
	canbittype 	unused6: 1;
	canbittype 	GW_C_I2ESP_DSBL: 1;
	canbittype 	GW_C_I2SpStSwStat: 2;
	canbittype 	unused5: 1;
	canbittype 	GW_C_I2EPBHoldSts: 3;
	canbittype 	GW_C_I2DRLEnable: 1;
	canbittype 	unused7: 2;
	canbittype 	GW_C_I2E_Mode_Sts: 2;
	canbittype 	GW_C_I2ZoomSts: 2;
	canbittype 	GW_C_I2StaticGridSts: 1;
} _c_GW_C_I2_msgType;

typedef struct {
	canbittype 	DAS_BDAS_CHIME_TYPSts: 2;
	canbittype 	DAS_BDAS_LF_CHIME_RQSts: 1;
	canbittype 	DAS_BDAS_RF_CHIME_RQSts: 1;
	canbittype 	unused0: 4;
	canbittype 	unused2: 2;
	canbittype 	DAS_BFCW_Setting: 1;
	canbittype 	DAS_BFCW_Brk: 1;
	canbittype 	unused1: 4;
	canbittype 	unused3: 8;
	canbittype 	unused4: 8;
} _c_DAS_B_msgType;

typedef struct {
	canbittype 	CBC_I2PanelIntensitySts: 8;
	canbittype 	CBC_I2FLASH_LKSts: 1;
	canbittype 	CBC_I2HL_WPRSts: 1;
	canbittype 	unused0: 1;
	canbittype 	CBC_I2AutoVehHoldSts: 2;
	canbittype 	CBC_I2CFG_AUTO_WPRSts: 1;
	canbittype 	CBC_I2SOUND_RSSts: 1;
	canbittype 	CBC_I2ACC_DLY_ACTSts: 1;
	canbittype 	CBC_I2ACC_DLY_TIMESts_1: 8;
	canbittype 	CBC_I2ACC_DLY_TIMESts_0: 8;
	canbittype 	CBC_I2ILL_APRCH_TIMESts: 8;
	canbittype 	CBC_I2HL_DLY_TIMESts: 8;
	canbittype 	unused2: 1;
	canbittype 	CBC_I2AHB_ENBLSts: 1;
	canbittype 	CBC_I2AHB_ACTSts: 1;
	canbittype 	unused1: 5;
	canbittype 	CBC_I2CsoAudibleAlertEnableDisableSts: 1;
	canbittype 	CBC_I2CsoAutoUnlockEnableDisableSts: 1;
	canbittype 	CBC_I2CsoDriverDoorFirstEnableDisableS: 1;
	canbittype 	unused4: 1;
	canbittype 	CBC_I2CsoAutoLockEnableDisableSts: 1;
	canbittype 	unused3: 2;
	canbittype 	CBC_I2PanicModeActive: 1;
} _c_CBC_I2_msgType;

typedef struct {
	canbittype 	CBC_I1E_CALL_PSD: 1;
	canbittype 	CBC_I1U_CALL_PSD: 1;
	canbittype 	CBC_I1NotFilteredExternalTempFailSts: 1;
	canbittype 	unused0: 5;
	canbittype 	CBC_I1NotFilteredExternalTempSts: 8;
	canbittype 	CBC_I1EC_MirrStat_CRV: 2;
	canbittype 	CBC_I1IMPACTCommand: 1;
	canbittype 	CBC_I1IMPACTConfirm: 1;
	canbittype 	unused1: 4;
} _c_CBC_I1_msgType;

typedef struct {
	canbittype 	unused0: 8;
	canbittype 	unused1: 8;
	canbittype 	unused2: 8;
	canbittype 	unused4: 1;
	canbittype 	STATUS_B_EPBServiceModeSts: 1;
	canbittype 	STATUS_B_EPBEPBChimeReq: 3;
	canbittype 	STATUS_B_EPBAutoParkSts: 2;
	canbittype 	unused3: 1;
	canbittype 	unused5: 3;
	canbittype 	STATUS_B_EPBTextDisplay: 5;
	canbittype 	STATUS_B_EPBEPBWarningLampReq: 2;
	canbittype 	unused6: 6;
	canbittype 	unused7: 8;
} _c_STATUS_B_EPB_msgType;

typedef struct {
	canbittype 	STATUS_C_CANEngineSts: 2;
	canbittype 	unused0: 6;
	canbittype 	unused1: 8;
	canbittype 	unused2: 8;
	canbittype 	unused3: 8;
} _c_STATUS_C_CAN_msgType;

typedef struct {
	canbittype 	unused0: 8;
	canbittype 	unused1: 8;
	canbittype 	unused2: 5;
	canbittype 	STATUS_B_HALFConfigSts: 3;
	canbittype 	unused4: 2;
	canbittype 	STATUS_B_HALFTorqueIntensitySts: 3;
	canbittype 	unused3: 3;
	canbittype 	unused5: 8;
	canbittype 	unused6: 8;
	canbittype 	unused7: 8;
	canbittype 	unused8: 8;
} _c_STATUS_B_HALF_msgType;

typedef struct {
	canbittype 	unused0: 6;
	canbittype 	HALF_B_Warning_RQHALF_RF_CHIME_RQSts: 1;
	canbittype 	HALF_B_Warning_RQHALF_LF_CHIME_RQSts: 1;
	canbittype 	HALF_B_Warning_RQHALF_CHIME_REP_RATESts: 4;
	canbittype 	HALF_B_Warning_RQHALF_CHIME_TYPESts: 4;
} _c_HALF_B_Warning_RQ_msgType;

typedef struct {
	canbittype 	unused0: 5;
	canbittype 	SWS_8StW_TempSts_1: 3;
	canbittype 	SWS_8StW_TempSts_0: 5;
	canbittype 	unused1: 2;
	canbittype 	SWS_8Command_15Sts_1: 1;
	canbittype 	SWS_8Command_15Sts_0: 1;
	canbittype 	SWS_8Command_14Sts: 2;
	canbittype 	SWS_8Command_13Sts: 2;
	canbittype 	SWS_8Command_12Sts: 2;
	canbittype 	SWS_8Command_11Sts_1: 1;
	canbittype 	SWS_8Command_11Sts_0: 1;
	canbittype 	SWS_8Command_10Sts: 2;
	canbittype 	SWS_8Command_09Sts: 2;
	canbittype 	SWS_8Command_08Sts: 2;
	canbittype 	SWS_8Command_07Sts_1: 1;
	canbittype 	SWS_8Command_07Sts_0: 1;
	canbittype 	unused2: 2;
	canbittype 	SWS_8Command_05Sts: 2;
	canbittype 	SWS_8Command_04Sts: 2;
	canbittype 	SWS_8Command_03Sts_1: 1;
	canbittype 	SWS_8Command_03Sts_0: 1;
	canbittype 	SWS_8Command_02Sts: 2;
	canbittype 	SWS_8Command_01Sts: 2;
	canbittype 	SWS_8StW_TempSensSts: 2;
	canbittype 	unused3: 1;
	canbittype 	unused4: 8;
	canbittype 	unused5: 8;
} _c_SWS_8_msgType;

typedef struct {
	canbittype 	unused0: 6;
	canbittype 	StW_Actn_Rq_1RT_TURN_RQSts: 1;
	canbittype 	StW_Actn_Rq_1LT_TURN_RQSts: 1;
	canbittype 	unused1: 8;
	canbittype 	unused2: 8;
	canbittype 	unused3: 8;
	canbittype 	unused4: 8;
	canbittype 	unused5: 8;
	canbittype 	unused6: 8;
} _c_StW_Actn_Rq_1_msgType;

typedef struct {
	canbittype 	PAM_BPAM_STATESts: 1;
	canbittype 	PAM_BPAM_MUTE_RQSts: 1;
	canbittype 	unused1: 2;
	canbittype 	PAM_BRR_PAM_StopControlSts: 1;
	canbittype 	PAM_BPAM_CFG_STATSts: 2;
	canbittype 	unused0: 1;
	canbittype 	unused3: 2;
	canbittype 	PAM_BPAM_LF_CHIME_RQSts: 1;
	canbittype 	PAM_BPAM_RF_CHIME_RQSts: 1;
	canbittype 	PAM_BPAM_FT_STATESts: 1;
	canbittype 	PAM_BPAM_RR_STATESts: 1;
	canbittype 	unused2: 2;
	canbittype 	unused4: 8;
	canbittype 	unused5: 2;
	canbittype 	PAM_BPAM_RR_CHIME_RQSts: 1;
	canbittype 	PAM_BPAM_LR_CHIME_RQSts: 1;
	canbittype 	PAM_BPAM_VOL_F: 2;
	canbittype 	PAM_BPAM_VOL_R: 2;
	canbittype 	unused6: 1;
	canbittype 	PAM_BPAM_CHIME_TYPESts: 3;
	canbittype 	PAM_BPAM_CHIME_REP_RATESts: 4;
} _c_PAM_B_msgType;

typedef struct {
	canbittype 	unused0: 3;
	canbittype 	GW_C_I3ESS_ENG_ST: 4;
	canbittype 	GW_C_I3ACPressure_1: 1;
	canbittype 	GW_C_I3ACPressure_0: 8;
} _c_GW_C_I3_msgType;

typedef struct {
	canbittype 	unused0: 6;
	canbittype 	VIN_1VIN_MSG: 2;
	canbittype 	VIN_1VIN_DATA_6: 8;
	canbittype 	VIN_1VIN_DATA_5: 8;
	canbittype 	VIN_1VIN_DATA_4: 8;
	canbittype 	VIN_1VIN_DATA_3: 8;
	canbittype 	VIN_1VIN_DATA_2: 8;
	canbittype 	VIN_1VIN_DATA_1: 8;
	canbittype 	VIN_1VIN_DATA_0: 8;
} _c_VIN_1_msgType;

typedef struct {
	canbittype 	unused0: 3;
	canbittype 	VEHICLE_SPEED_ODOMETER_1VehicleSpeed_1: 5;
	canbittype 	VEHICLE_SPEED_ODOMETER_1VehicleSpeed_0: 8;
	canbittype 	VEHICLE_SPEED_ODOMETER_1VehicleSpeedFailSts: 1;
	canbittype 	unused1: 7;
	canbittype 	VEHICLE_SPEED_ODOMETER_1TravelDistance: 8;
} _c_VEHICLE_SPEED_ODOMETER_1_msgType;

typedef struct {
	canbittype 	unused0: 8;
	canbittype 	unused1: 6;
	canbittype 	GW_C_I6SpStSwStat: 2;
	canbittype 	unused2: 8;
	canbittype 	unused3: 8;
} _c_GW_C_I6_msgType;

typedef struct {
	canbittype 	DYNAMIC_VEHICLE_INFO2LHRFastPulseCounterFailStS: 1;
	canbittype 	DYNAMIC_VEHICLE_INFO2RHRFastPulseCounterFailStS: 1;
	canbittype 	DYNAMIC_VEHICLE_INFO2LHRFastPulseCounter_1: 6;
	canbittype 	DYNAMIC_VEHICLE_INFO2LHRFastPulseCounter_0: 2;
	canbittype 	DYNAMIC_VEHICLE_INFO2RHRFastPulseCounter_1: 6;
	canbittype 	DYNAMIC_VEHICLE_INFO2RHRFastPulseCounter_0: 2;
	canbittype 	DYNAMIC_VEHICLE_INFO2LHFFastPulseCounter_1: 6;
	canbittype 	DYNAMIC_VEHICLE_INFO2LHFFastPulseCounter_0: 2;
	canbittype 	DYNAMIC_VEHICLE_INFO2RHFFastPulseCounter_1: 6;
	canbittype 	DYNAMIC_VEHICLE_INFO2RHFFastPulseCounter_0: 2;
	canbittype 	DYNAMIC_VEHICLE_INFO2LHFFastPulseCounterFailStS: 1;
	canbittype 	DYNAMIC_VEHICLE_INFO2RHFFastPulseCounterFailStS: 1;
	canbittype 	DYNAMIC_VEHICLE_INFO2LongAcceleration_1: 4;
	canbittype 	DYNAMIC_VEHICLE_INFO2LongAcceleration_0: 8;
	canbittype 	DYNAMIC_VEHICLE_INFO2LongAcceleration_Offset: 8;
	canbittype 	unused0: 8;
} _c_DYNAMIC_VEHICLE_INFO2_msgType;

typedef struct {
	canbittype 	unused0: 8;
	canbittype 	unused1: 5;
	canbittype 	STATUS_B_TRANSMISSIONShiftLeverPosition: 3;
	canbittype 	unused2: 4;
	canbittype 	STATUS_B_TRANSMISSIONActualGearForDisplay: 4;
	canbittype 	unused3: 8;
	canbittype 	unused4: 8;
	canbittype 	unused5: 8;
	canbittype 	unused6: 8;
	canbittype 	unused7: 8;
} _c_STATUS_B_TRANSMISSION_msgType;

typedef struct {
	canbittype 	NWM_BCMZero_byte: 8;
	canbittype 	NWM_BCMD_ES: 2;
	canbittype 	NWM_BCMP_ES: 1;
	canbittype 	NWM_BCMGenericFailSts: 1;
	canbittype 	NWM_BCMEOL: 1;
	canbittype 	NWM_BCMActiveLoadMaster: 1;
	canbittype 	NWM_BCMSystemCommand: 2;
	canbittype 	NWM_BCMNode31: 1;
	canbittype 	NWM_BCMNode30: 1;
	canbittype 	NWM_BCMNode29: 1;
	canbittype 	NWM_BCMNode28: 1;
	canbittype 	NWM_BCMNode27: 1;
	canbittype 	NWM_BCMNode26: 1;
	canbittype 	NWM_BCMNode25: 1;
	canbittype 	NWM_BCMNode24: 1;
	canbittype 	NWM_BCMNode23: 1;
	canbittype 	NWM_BCMNode22: 1;
	canbittype 	NWM_BCMNode21: 1;
	canbittype 	NWM_BCMNode20: 1;
	canbittype 	NWM_BCMNode19: 1;
	canbittype 	NWM_BCMNode18: 1;
	canbittype 	NWM_BCMNode17: 1;
	canbittype 	NWM_BCMNode16: 1;
	canbittype 	NWM_BCMNode15: 1;
	canbittype 	NWM_BCMNode14: 1;
	canbittype 	NWM_BCMNode13: 1;
	canbittype 	NWM_BCMNode12: 1;
	canbittype 	NWM_BCMNode11: 1;
	canbittype 	NWM_BCMNode10: 1;
	canbittype 	NWM_BCMNode9: 1;
	canbittype 	NWM_BCMNode8: 1;
	canbittype 	NWM_BCMNode7: 1;
	canbittype 	NWM_BCMNode6: 1;
	canbittype 	NWM_BCMNode5: 1;
	canbittype 	NWM_BCMNode4: 1;
	canbittype 	NWM_BCMNode3: 1;
	canbittype 	NWM_BCMNode2: 1;
	canbittype 	NWM_BCMNode1: 1;
	canbittype 	NWM_BCMNode0: 1;
} _c_NWM_BCM_msgType;

typedef struct {
	canbittype 	STATUS_BCM2BatteryLampSts: 2;
	canbittype 	unused1: 3;
	canbittype 	STATUS_BCM2PowerModeSts: 2;
	canbittype 	unused0: 1;
	canbittype 	unused3: 5;
	canbittype 	STATUS_BCM2NightAndDaySensorFailSts: 1;
	canbittype 	unused2: 2;
	canbittype 	unused4: 8;
	canbittype 	unused5: 8;
	canbittype 	unused6: 8;
	canbittype 	unused7: 8;
	canbittype 	unused8: 7;
	canbittype 	STATUS_BCM2WashLightWarningSts: 1;
	canbittype 	unused9: 8;
} _c_STATUS_BCM2_msgType;

typedef struct {
	canbittype 	unused1: 4;
	canbittype 	STATUS_BCMInternalLightSts: 1;
	canbittype 	STATUS_BCMSysEOLSts: 1;
	canbittype 	unused0: 2;
	canbittype 	STATUS_BCMBonnetSts: 1;
	canbittype 	STATUS_BCMRHatchSts: 1;
	canbittype 	STATUS_BCMRHRDoorSts: 1;
	canbittype 	STATUS_BCMLHRDoorSts: 1;
	canbittype 	STATUS_BCMPsngrDoorSts: 1;
	canbittype 	STATUS_BCMDriverDoorSts: 1;
	canbittype 	unused2: 1;
	canbittype 	STATUS_BCMRearLockLastElSts: 1;
	canbittype 	unused4: 4;
	canbittype 	STATUS_BCMRechargeSts: 1;
	canbittype 	unused3: 3;
	canbittype 	unused6: 3;
	canbittype 	STATUS_BCMMoveWheelSts: 1;
	canbittype 	STATUS_BCMESLLockFailSts: 1;
	canbittype 	STATUS_BCMESLUnlockFailSts: 1;
	canbittype 	unused5: 1;
	canbittype 	STATUS_BCMDoorLockLastElSts_1: 1;
	canbittype 	STATUS_BCMDoorLockLastElSts_0: 1;
	canbittype 	unused7: 7;
	canbittype 	unused8: 8;
	canbittype 	unused9: 8;
	canbittype 	STATUS_BCMRainSensorFailSts: 1;
	canbittype 	unused11: 5;
	canbittype 	STATUS_BCMExteriorRearReleaseSwitchSts: 1;
	canbittype 	unused10: 1;
} _c_STATUS_BCM_msgType;

typedef struct {
	canbittype 	unused0: 8;
	canbittype 	EXTERNAL_LIGHTS_1StopLightSts: 1;
	canbittype 	EXTERNAL_LIGHTS_1RHParkingLightSts: 1;
	canbittype 	EXTERNAL_LIGHTS_1LHParkingLightSts: 1;
	canbittype 	EXTERNAL_LIGHTS_1HighBeamSts: 1;
	canbittype 	EXTERNAL_LIGHTS_1LowBeamSts: 1;
	canbittype 	EXTERNAL_LIGHTS_1FrontFogLightSts: 1;
	canbittype 	EXTERNAL_LIGHTS_1RearFogLightSts: 1;
	canbittype 	unused1: 1;
	canbittype 	unused3: 1;
	canbittype 	EXTERNAL_LIGHTS_1LHTurnSignalSts: 1;
	canbittype 	EXTERNAL_LIGHTS_1RHTurnSignalSts: 1;
	canbittype 	EXTERNAL_LIGHTS_1RHeatedWindowSts: 1;
	canbittype 	unused2: 4;
	canbittype 	EXTERNAL_LIGHTS_1RHTurnLightFault: 1;
	canbittype 	EXTERNAL_LIGHTS_1LHTurnLightFault: 1;
	canbittype 	unused5: 4;
	canbittype 	EXTERNAL_LIGHTS_1RearFogLightFault: 1;
	canbittype 	unused4: 1;
	canbittype 	unused7: 3;
	canbittype 	EXTERNAL_LIGHTS_1ParkingLightFault: 1;
	canbittype 	EXTERNAL_LIGHTS_1PlateLightFault: 1;
	canbittype 	unused6: 1;
	canbittype 	EXTERNAL_LIGHTS_1ReverseGearLightFault: 1;
	canbittype 	EXTERNAL_LIGHTS_1HighBeamFault: 1;
	canbittype 	EXTERNAL_LIGHTS_1LowBeamFault: 1;
	canbittype 	unused8: 7;
} _c_EXTERNAL_LIGHTS_1_msgType;

typedef struct {
	canbittype 	RFHUB_B_A2RFReq: 3;
	canbittype 	RFHUB_B_A2RFFuncReq: 5;
	canbittype 	unused0: 8;
	canbittype 	RFHUB_B_A2RFFobNum: 4;
	canbittype 	unused1: 4;
	canbittype 	unused2: 8;
	canbittype 	unused3: 8;
} _c_RFHUB_B_A2_msgType;

typedef struct {
	canbittype 	unused0: 1;
	canbittype 	CBC_I4RemStActvSts: 1;
	canbittype 	CBC_I4StTypSts: 3;
	canbittype 	CBC_I4CmdIgnSts: 3;
	canbittype 	CBC_I4ShiftLeverPositionReq: 3;
	canbittype 	CBC_I4ShiftLeverPositionReqValidData: 1;
	canbittype 	unused1: 2;
	canbittype 	CBC_I4KeyInIgnSts: 2;
	canbittype 	CBC_I4Court_Lmp: 8;
	canbittype 	CBC_I4Aprch_Lmp: 8;
	canbittype 	CBC_I4MOOD_LGT_INTS: 8;
} _c_CBC_I4_msgType;

typedef struct {
	canbittype 	RFHUB_B_A4RF_FobNum: 4;
	canbittype 	RFHUB_B_A4RFMemRq: 1;
	canbittype 	unused0: 3;
} _c_RFHUB_B_A4_msgType;

typedef struct {
	canbittype 	unused0: 8;
	canbittype 	unused1: 8;
	canbittype 	unused2: 8;
	canbittype 	unused3: 8;
	canbittype 	unused4: 8;
	canbittype 	unused6: 4;
	canbittype 	STATUS_B_ECM2StartRelayBCMFeedbackFault: 1;
	canbittype 	STATUS_B_ECM2StartRelayECMFeedbackFault: 1;
	canbittype 	unused5: 2;
	canbittype 	unused8: 1;
	canbittype 	STATUS_B_ECM2SAMInfo: 4;
	canbittype 	unused7: 3;
	canbittype 	unused9: 8;
} _c_STATUS_B_ECM2_msgType;

typedef struct {
	canbittype 	unused0: 8;
	canbittype 	unused3: 2;
	canbittype 	STATUS_B_ECMFuelWaterPresentSts: 1;
	canbittype 	unused2: 2;
	canbittype 	STATUS_B_ECMFuelWaterPresentFailSts: 1;
	canbittype 	unused1: 2;
	canbittype 	STATUS_B_ECMCompressorSts: 2;
	canbittype 	unused5: 3;
	canbittype 	STATUS_B_ECMEngineWaterTempFailSts: 1;
	canbittype 	unused4: 1;
	canbittype 	STATUS_B_ECMEngineWaterTempWarningLightSts: 1;
	canbittype 	STATUS_B_ECMEngineWaterTemp: 8;
	canbittype 	unused6: 8;
	canbittype 	unused7: 8;
	canbittype 	STATUS_B_ECMEngineSpeed: 8;
	canbittype 	unused10: 2;
	canbittype 	STATUS_B_ECMEngineSpeedValidData: 1;
	canbittype 	unused9: 2;
	canbittype 	STATUS_B_ECMReverseGearSts: 1;
	canbittype 	unused8: 2;
} _c_STATUS_B_ECM_msgType;

typedef struct {
	canbittype 	unused0: 8;
	canbittype 	unused2: 1;
	canbittype 	STATUS_B_BSMDSTFailSts: 1;
	canbittype 	unused1: 6;
	canbittype 	unused3: 1;
	canbittype 	STATUS_B_BSMESCControlSts: 1;
	canbittype 	STATUS_B_BSMHHCustomerDisableSts: 1;
	canbittype 	STATUS_B_BSMVehicleSpeed_1: 5;
	canbittype 	STATUS_B_BSMVehicleSpeed_0: 8;
	canbittype 	STATUS_B_BSMVehicleSpeedFailSts: 1;
	canbittype 	STATUS_B_BSMLHRPulseCounterFailSts: 1;
	canbittype 	STATUS_B_BSMRHRPulseCounterFailSts: 1;
	canbittype 	STATUS_B_BSMLHRPulseCounter_1: 5;
	canbittype 	STATUS_B_BSMLHRPulseCounter_0: 8;
	canbittype 	unused4: 3;
	canbittype 	STATUS_B_BSMRHRPulseCounter_1: 5;
	canbittype 	STATUS_B_BSMRHRPulseCounter_0: 8;
} _c_STATUS_B_BSM_msgType;

typedef struct {
	canbittype 	unused0: 8;
	canbittype 	unused1: 8;
	canbittype 	unused2: 8;
	canbittype 	unused3: 8;
	canbittype 	unused4: 8;
	canbittype 	unused5: 8;
	canbittype 	unused6: 8;
	canbittype 	unused7: 8;
} _c_TxDynamicMsg0_1_msgType;

typedef struct {
	canbittype 	unused0: 8;
	canbittype 	unused1: 8;
	canbittype 	unused2: 8;
	canbittype 	unused3: 8;
	canbittype 	unused4: 8;
	canbittype 	unused5: 8;
	canbittype 	unused6: 8;
	canbittype 	unused7: 8;
} _c_TxDynamicMsg1_1_msgType;

/* Receive messages */

typedef struct {
	canbittype 	unused0: 5;
	canbittype 	STATUS_C_OCMEOL: 1;
	canbittype 	STATUS_C_OCMCurrentFailSts: 1;
	canbittype 	STATUS_C_OCMGenericFailSts: 1;
	canbittype 	unused1: 8;
	canbittype 	unused2: 8;
} _c_STATUS_C_OCM_msgType;

typedef struct {
	canbittype 	unused0: 5;
	canbittype 	STATUS_C_OCMEOL: 1;
	canbittype 	STATUS_C_OCMCurrentFailSts: 1;
	canbittype 	STATUS_C_OCMGenericFailSts: 1;
	canbittype 	unused1: 8;
	canbittype 	unused2: 8;
} _c_STATUS_C_OCM_RDS_msgType;

typedef struct {
	canbittype 	CFG_DATA_CODE_RSP_OCMDigit_11: 4;
	canbittype 	CFG_DATA_CODE_RSP_OCMDigit_10: 4;
	canbittype 	CFG_DATA_CODE_RSP_OCMDigit_09: 4;
	canbittype 	CFG_DATA_CODE_RSP_OCMDigit_08: 4;
	canbittype 	CFG_DATA_CODE_RSP_OCMDigit_07: 4;
	canbittype 	CFG_DATA_CODE_RSP_OCMDigit_06: 4;
	canbittype 	CFG_DATA_CODE_RSP_OCMDigit_05: 4;
	canbittype 	CFG_DATA_CODE_RSP_OCMDigit_04: 4;
	canbittype 	CFG_DATA_CODE_RSP_OCMDigit_03: 4;
	canbittype 	CFG_DATA_CODE_RSP_OCMDigit_02: 4;
	canbittype 	CFG_DATA_CODE_RSP_OCMDigit_01: 4;
	canbittype 	unused0: 4;
} _c_CFG_DATA_CODE_RSP_OCM_msgType;

typedef struct {
	canbittype 	CFG_DATA_CODE_RSP_OCMDigit_11: 4;
	canbittype 	CFG_DATA_CODE_RSP_OCMDigit_10: 4;
	canbittype 	CFG_DATA_CODE_RSP_OCMDigit_09: 4;
	canbittype 	CFG_DATA_CODE_RSP_OCMDigit_08: 4;
	canbittype 	CFG_DATA_CODE_RSP_OCMDigit_07: 4;
	canbittype 	CFG_DATA_CODE_RSP_OCMDigit_06: 4;
	canbittype 	CFG_DATA_CODE_RSP_OCMDigit_05: 4;
	canbittype 	CFG_DATA_CODE_RSP_OCMDigit_04: 4;
	canbittype 	CFG_DATA_CODE_RSP_OCMDigit_03: 4;
	canbittype 	CFG_DATA_CODE_RSP_OCMDigit_02: 4;
	canbittype 	CFG_DATA_CODE_RSP_OCMDigit_01: 4;
	canbittype 	unused0: 4;
} _c_CFG_DATA_CODE_RSP_OCM_RDS_msgType;

typedef struct {
	canbittype 	CFG_DATA_CODE_RSP_AHLMDigit_11: 4;
	canbittype 	CFG_DATA_CODE_RSP_AHLMDigit_10: 4;
	canbittype 	CFG_DATA_CODE_RSP_AHLMDigit_09: 4;
	canbittype 	CFG_DATA_CODE_RSP_AHLMDigit_08: 4;
	canbittype 	CFG_DATA_CODE_RSP_AHLMDigit_07: 4;
	canbittype 	CFG_DATA_CODE_RSP_AHLMDigit_06: 4;
	canbittype 	CFG_DATA_CODE_RSP_AHLMDigit_05: 4;
	canbittype 	CFG_DATA_CODE_RSP_AHLMDigit_04: 4;
	canbittype 	CFG_DATA_CODE_RSP_AHLMDigit_03: 4;
	canbittype 	CFG_DATA_CODE_RSP_AHLMDigit_02: 4;
	canbittype 	CFG_DATA_CODE_RSP_AHLMDigit_01: 4;
	canbittype 	unused0: 4;
} _c_CFG_DATA_CODE_RSP_AHLM_msgType;

typedef struct {
	canbittype 	CFG_DATA_CODE_RSP_AHLMDigit_11: 4;
	canbittype 	CFG_DATA_CODE_RSP_AHLMDigit_10: 4;
	canbittype 	CFG_DATA_CODE_RSP_AHLMDigit_09: 4;
	canbittype 	CFG_DATA_CODE_RSP_AHLMDigit_08: 4;
	canbittype 	CFG_DATA_CODE_RSP_AHLMDigit_07: 4;
	canbittype 	CFG_DATA_CODE_RSP_AHLMDigit_06: 4;
	canbittype 	CFG_DATA_CODE_RSP_AHLMDigit_05: 4;
	canbittype 	CFG_DATA_CODE_RSP_AHLMDigit_04: 4;
	canbittype 	CFG_DATA_CODE_RSP_AHLMDigit_03: 4;
	canbittype 	CFG_DATA_CODE_RSP_AHLMDigit_02: 4;
	canbittype 	CFG_DATA_CODE_RSP_AHLMDigit_01: 4;
	canbittype 	unused0: 4;
} _c_CFG_DATA_CODE_RSP_AHLM_RDS_msgType;

typedef struct {
	canbittype 	CFG_DATA_CODE_RSP_DTCMDigit_11: 4;
	canbittype 	CFG_DATA_CODE_RSP_DTCMDigit_10: 4;
	canbittype 	CFG_DATA_CODE_RSP_DTCMDigit_09: 4;
	canbittype 	CFG_DATA_CODE_RSP_DTCMDigit_08: 4;
	canbittype 	CFG_DATA_CODE_RSP_DTCMDigit_07: 4;
	canbittype 	CFG_DATA_CODE_RSP_DTCMDigit_06: 4;
	canbittype 	CFG_DATA_CODE_RSP_DTCMDigit_05: 4;
	canbittype 	CFG_DATA_CODE_RSP_DTCMDigit_04: 4;
	canbittype 	CFG_DATA_CODE_RSP_DTCMDigit_03: 4;
	canbittype 	CFG_DATA_CODE_RSP_DTCMDigit_02: 4;
	canbittype 	CFG_DATA_CODE_RSP_DTCMDigit_01: 4;
	canbittype 	unused0: 4;
} _c_CFG_DATA_CODE_RSP_DTCM_msgType;

typedef struct {
	canbittype 	CFG_DATA_CODE_RSP_DTCMDigit_11: 4;
	canbittype 	CFG_DATA_CODE_RSP_DTCMDigit_10: 4;
	canbittype 	CFG_DATA_CODE_RSP_DTCMDigit_09: 4;
	canbittype 	CFG_DATA_CODE_RSP_DTCMDigit_08: 4;
	canbittype 	CFG_DATA_CODE_RSP_DTCMDigit_07: 4;
	canbittype 	CFG_DATA_CODE_RSP_DTCMDigit_06: 4;
	canbittype 	CFG_DATA_CODE_RSP_DTCMDigit_05: 4;
	canbittype 	CFG_DATA_CODE_RSP_DTCMDigit_04: 4;
	canbittype 	CFG_DATA_CODE_RSP_DTCMDigit_03: 4;
	canbittype 	CFG_DATA_CODE_RSP_DTCMDigit_02: 4;
	canbittype 	CFG_DATA_CODE_RSP_DTCMDigit_01: 4;
	canbittype 	unused0: 4;
} _c_CFG_DATA_CODE_RSP_DTCM_RDS_msgType;

typedef struct {
	canbittype 	CFG_DATA_CODE_RSP_RFHMDigit_11: 4;
	canbittype 	CFG_DATA_CODE_RSP_RFHMDigit_10: 4;
	canbittype 	CFG_DATA_CODE_RSP_RFHMDigit_09: 4;
	canbittype 	CFG_DATA_CODE_RSP_RFHMDigit_08: 4;
	canbittype 	CFG_DATA_CODE_RSP_RFHMDigit_07: 4;
	canbittype 	CFG_DATA_CODE_RSP_RFHMDigit_06: 4;
	canbittype 	CFG_DATA_CODE_RSP_RFHMDigit_05: 4;
	canbittype 	CFG_DATA_CODE_RSP_RFHMDigit_04: 4;
	canbittype 	CFG_DATA_CODE_RSP_RFHMDigit_03: 4;
	canbittype 	CFG_DATA_CODE_RSP_RFHMDigit_02: 4;
	canbittype 	CFG_DATA_CODE_RSP_RFHMDigit_01: 4;
	canbittype 	unused0: 4;
} _c_CFG_DATA_CODE_RSP_RFHM_msgType;

typedef struct {
	canbittype 	CFG_DATA_CODE_RSP_RFHMDigit_11: 4;
	canbittype 	CFG_DATA_CODE_RSP_RFHMDigit_10: 4;
	canbittype 	CFG_DATA_CODE_RSP_RFHMDigit_09: 4;
	canbittype 	CFG_DATA_CODE_RSP_RFHMDigit_08: 4;
	canbittype 	CFG_DATA_CODE_RSP_RFHMDigit_07: 4;
	canbittype 	CFG_DATA_CODE_RSP_RFHMDigit_06: 4;
	canbittype 	CFG_DATA_CODE_RSP_RFHMDigit_05: 4;
	canbittype 	CFG_DATA_CODE_RSP_RFHMDigit_04: 4;
	canbittype 	CFG_DATA_CODE_RSP_RFHMDigit_03: 4;
	canbittype 	CFG_DATA_CODE_RSP_RFHMDigit_02: 4;
	canbittype 	CFG_DATA_CODE_RSP_RFHMDigit_01: 4;
	canbittype 	unused0: 4;
} _c_CFG_DATA_CODE_RSP_RFHM_RDS_msgType;

typedef struct {
	canbittype 	CFG_DATA_CODE_RSP_DASMDigit_11: 4;
	canbittype 	CFG_DATA_CODE_RSP_DASMDigit_10: 4;
	canbittype 	CFG_DATA_CODE_RSP_DASMDigit_09: 4;
	canbittype 	CFG_DATA_CODE_RSP_DASMDigit_08: 4;
	canbittype 	CFG_DATA_CODE_RSP_DASMDigit_07: 4;
	canbittype 	CFG_DATA_CODE_RSP_DASMDigit_06: 4;
	canbittype 	CFG_DATA_CODE_RSP_DASMDigit_05: 4;
	canbittype 	CFG_DATA_CODE_RSP_DASMDigit_04: 4;
	canbittype 	CFG_DATA_CODE_RSP_DASMDigit_03: 4;
	canbittype 	CFG_DATA_CODE_RSP_DASMDigit_02: 4;
	canbittype 	CFG_DATA_CODE_RSP_DASMDigit_01: 4;
	canbittype 	unused0: 4;
} _c_CFG_DATA_CODE_RSP_DASM_msgType;

typedef struct {
	canbittype 	CFG_DATA_CODE_RSP_DASMDigit_11: 4;
	canbittype 	CFG_DATA_CODE_RSP_DASMDigit_10: 4;
	canbittype 	CFG_DATA_CODE_RSP_DASMDigit_09: 4;
	canbittype 	CFG_DATA_CODE_RSP_DASMDigit_08: 4;
	canbittype 	CFG_DATA_CODE_RSP_DASMDigit_07: 4;
	canbittype 	CFG_DATA_CODE_RSP_DASMDigit_06: 4;
	canbittype 	CFG_DATA_CODE_RSP_DASMDigit_05: 4;
	canbittype 	CFG_DATA_CODE_RSP_DASMDigit_04: 4;
	canbittype 	CFG_DATA_CODE_RSP_DASMDigit_03: 4;
	canbittype 	CFG_DATA_CODE_RSP_DASMDigit_02: 4;
	canbittype 	CFG_DATA_CODE_RSP_DASMDigit_01: 4;
	canbittype 	unused0: 4;
} _c_CFG_DATA_CODE_RSP_DASM_RDS_msgType;

typedef struct {
	canbittype 	CFG_DATA_CODE_RSP_SCCMDigit_11: 4;
	canbittype 	CFG_DATA_CODE_RSP_SCCMDigit_10: 4;
	canbittype 	CFG_DATA_CODE_RSP_SCCMDigit_09: 4;
	canbittype 	CFG_DATA_CODE_RSP_SCCMDigit_08: 4;
	canbittype 	CFG_DATA_CODE_RSP_SCCMDigit_07: 4;
	canbittype 	CFG_DATA_CODE_RSP_SCCMDigit_06: 4;
	canbittype 	CFG_DATA_CODE_RSP_SCCMDigit_05: 4;
	canbittype 	CFG_DATA_CODE_RSP_SCCMDigit_04: 4;
	canbittype 	CFG_DATA_CODE_RSP_SCCMDigit_03: 4;
	canbittype 	CFG_DATA_CODE_RSP_SCCMDigit_02: 4;
	canbittype 	CFG_DATA_CODE_RSP_SCCMDigit_01: 4;
	canbittype 	unused0: 4;
} _c_CFG_DATA_CODE_RSP_SCCM_msgType;

typedef struct {
	canbittype 	CFG_DATA_CODE_RSP_SCCMDigit_11: 4;
	canbittype 	CFG_DATA_CODE_RSP_SCCMDigit_10: 4;
	canbittype 	CFG_DATA_CODE_RSP_SCCMDigit_09: 4;
	canbittype 	CFG_DATA_CODE_RSP_SCCMDigit_08: 4;
	canbittype 	CFG_DATA_CODE_RSP_SCCMDigit_07: 4;
	canbittype 	CFG_DATA_CODE_RSP_SCCMDigit_06: 4;
	canbittype 	CFG_DATA_CODE_RSP_SCCMDigit_05: 4;
	canbittype 	CFG_DATA_CODE_RSP_SCCMDigit_04: 4;
	canbittype 	CFG_DATA_CODE_RSP_SCCMDigit_03: 4;
	canbittype 	CFG_DATA_CODE_RSP_SCCMDigit_02: 4;
	canbittype 	CFG_DATA_CODE_RSP_SCCMDigit_01: 4;
	canbittype 	unused0: 4;
} _c_CFG_DATA_CODE_RSP_SCCM_RDS_msgType;

typedef struct {
	canbittype 	CFG_DATA_CODE_RSP_HALFDigit_11: 4;
	canbittype 	CFG_DATA_CODE_RSP_HALFDigit_10: 4;
	canbittype 	CFG_DATA_CODE_RSP_HALFDigit_09: 4;
	canbittype 	CFG_DATA_CODE_RSP_HALFDigit_08: 4;
	canbittype 	CFG_DATA_CODE_RSP_HALFDigit_07: 4;
	canbittype 	CFG_DATA_CODE_RSP_HALFDigit_06: 4;
	canbittype 	CFG_DATA_CODE_RSP_HALFDigit_05: 4;
	canbittype 	CFG_DATA_CODE_RSP_HALFDigit_04: 4;
	canbittype 	CFG_DATA_CODE_RSP_HALFDigit_03: 4;
	canbittype 	CFG_DATA_CODE_RSP_HALFDigit_02: 4;
	canbittype 	CFG_DATA_CODE_RSP_HALFDigit_01: 4;
	canbittype 	unused0: 4;
} _c_CFG_DATA_CODE_RSP_HALF_msgType;

typedef struct {
	canbittype 	CFG_DATA_CODE_RSP_HALFDigit_11: 4;
	canbittype 	CFG_DATA_CODE_RSP_HALFDigit_10: 4;
	canbittype 	CFG_DATA_CODE_RSP_HALFDigit_09: 4;
	canbittype 	CFG_DATA_CODE_RSP_HALFDigit_08: 4;
	canbittype 	CFG_DATA_CODE_RSP_HALFDigit_07: 4;
	canbittype 	CFG_DATA_CODE_RSP_HALFDigit_06: 4;
	canbittype 	CFG_DATA_CODE_RSP_HALFDigit_05: 4;
	canbittype 	CFG_DATA_CODE_RSP_HALFDigit_04: 4;
	canbittype 	CFG_DATA_CODE_RSP_HALFDigit_03: 4;
	canbittype 	CFG_DATA_CODE_RSP_HALFDigit_02: 4;
	canbittype 	CFG_DATA_CODE_RSP_HALFDigit_01: 4;
	canbittype 	unused0: 4;
} _c_CFG_DATA_CODE_RSP_HALF_RDS_msgType;

typedef struct {
	canbittype 	CFG_DATA_CODE_RSP_ORCDigit_11: 4;
	canbittype 	CFG_DATA_CODE_RSP_ORCDigit_10: 4;
	canbittype 	CFG_DATA_CODE_RSP_ORCDigit_09: 4;
	canbittype 	CFG_DATA_CODE_RSP_ORCDigit_08: 4;
	canbittype 	CFG_DATA_CODE_RSP_ORCDigit_07: 4;
	canbittype 	CFG_DATA_CODE_RSP_ORCDigit_06: 4;
	canbittype 	CFG_DATA_CODE_RSP_ORCDigit_05: 4;
	canbittype 	CFG_DATA_CODE_RSP_ORCDigit_04: 4;
	canbittype 	CFG_DATA_CODE_RSP_ORCDigit_03: 4;
	canbittype 	CFG_DATA_CODE_RSP_ORCDigit_02: 4;
	canbittype 	CFG_DATA_CODE_RSP_ORCDigit_01: 4;
	canbittype 	unused0: 4;
} _c_CFG_DATA_CODE_RSP_ORC_msgType;

typedef struct {
	canbittype 	CFG_DATA_CODE_RSP_ORCDigit_11: 4;
	canbittype 	CFG_DATA_CODE_RSP_ORCDigit_10: 4;
	canbittype 	CFG_DATA_CODE_RSP_ORCDigit_09: 4;
	canbittype 	CFG_DATA_CODE_RSP_ORCDigit_08: 4;
	canbittype 	CFG_DATA_CODE_RSP_ORCDigit_07: 4;
	canbittype 	CFG_DATA_CODE_RSP_ORCDigit_06: 4;
	canbittype 	CFG_DATA_CODE_RSP_ORCDigit_05: 4;
	canbittype 	CFG_DATA_CODE_RSP_ORCDigit_04: 4;
	canbittype 	CFG_DATA_CODE_RSP_ORCDigit_03: 4;
	canbittype 	CFG_DATA_CODE_RSP_ORCDigit_02: 4;
	canbittype 	CFG_DATA_CODE_RSP_ORCDigit_01: 4;
	canbittype 	unused0: 4;
} _c_CFG_DATA_CODE_RSP_ORC_RDS_msgType;

typedef struct {
	canbittype 	CFG_DATA_CODE_RSP_PAMDigit_11: 4;
	canbittype 	CFG_DATA_CODE_RSP_PAMDigit_10: 4;
	canbittype 	CFG_DATA_CODE_RSP_PAMDigit_09: 4;
	canbittype 	CFG_DATA_CODE_RSP_PAMDigit_08: 4;
	canbittype 	CFG_DATA_CODE_RSP_PAMDigit_07: 4;
	canbittype 	CFG_DATA_CODE_RSP_PAMDigit_06: 4;
	canbittype 	CFG_DATA_CODE_RSP_PAMDigit_05: 4;
	canbittype 	CFG_DATA_CODE_RSP_PAMDigit_04: 4;
	canbittype 	CFG_DATA_CODE_RSP_PAMDigit_03: 4;
	canbittype 	CFG_DATA_CODE_RSP_PAMDigit_02: 4;
	canbittype 	CFG_DATA_CODE_RSP_PAMDigit_01: 4;
	canbittype 	unused0: 4;
} _c_CFG_DATA_CODE_RSP_PAM_msgType;

typedef struct {
	canbittype 	CFG_DATA_CODE_RSP_PAMDigit_11: 4;
	canbittype 	CFG_DATA_CODE_RSP_PAMDigit_10: 4;
	canbittype 	CFG_DATA_CODE_RSP_PAMDigit_09: 4;
	canbittype 	CFG_DATA_CODE_RSP_PAMDigit_08: 4;
	canbittype 	CFG_DATA_CODE_RSP_PAMDigit_07: 4;
	canbittype 	CFG_DATA_CODE_RSP_PAMDigit_06: 4;
	canbittype 	CFG_DATA_CODE_RSP_PAMDigit_05: 4;
	canbittype 	CFG_DATA_CODE_RSP_PAMDigit_04: 4;
	canbittype 	CFG_DATA_CODE_RSP_PAMDigit_03: 4;
	canbittype 	CFG_DATA_CODE_RSP_PAMDigit_02: 4;
	canbittype 	CFG_DATA_CODE_RSP_PAMDigit_01: 4;
	canbittype 	unused0: 4;
} _c_CFG_DATA_CODE_RSP_PAM_RDS_msgType;

typedef struct {
	canbittype 	CFG_DATA_CODE_RSP_EPBDigit_11: 4;
	canbittype 	CFG_DATA_CODE_RSP_EPBDigit_10: 4;
	canbittype 	CFG_DATA_CODE_RSP_EPBDigit_09: 4;
	canbittype 	CFG_DATA_CODE_RSP_EPBDigit_08: 4;
	canbittype 	CFG_DATA_CODE_RSP_EPBDigit_07: 4;
	canbittype 	CFG_DATA_CODE_RSP_EPBDigit_06: 4;
	canbittype 	CFG_DATA_CODE_RSP_EPBDigit_05: 4;
	canbittype 	CFG_DATA_CODE_RSP_EPBDigit_04: 4;
	canbittype 	CFG_DATA_CODE_RSP_EPBDigit_03: 4;
	canbittype 	CFG_DATA_CODE_RSP_EPBDigit_02: 4;
	canbittype 	CFG_DATA_CODE_RSP_EPBDigit_01: 4;
	canbittype 	unused0: 4;
} _c_CFG_DATA_CODE_RSP_EPB_msgType;

typedef struct {
	canbittype 	CFG_DATA_CODE_RSP_EPBDigit_11: 4;
	canbittype 	CFG_DATA_CODE_RSP_EPBDigit_10: 4;
	canbittype 	CFG_DATA_CODE_RSP_EPBDigit_09: 4;
	canbittype 	CFG_DATA_CODE_RSP_EPBDigit_08: 4;
	canbittype 	CFG_DATA_CODE_RSP_EPBDigit_07: 4;
	canbittype 	CFG_DATA_CODE_RSP_EPBDigit_06: 4;
	canbittype 	CFG_DATA_CODE_RSP_EPBDigit_05: 4;
	canbittype 	CFG_DATA_CODE_RSP_EPBDigit_04: 4;
	canbittype 	CFG_DATA_CODE_RSP_EPBDigit_03: 4;
	canbittype 	CFG_DATA_CODE_RSP_EPBDigit_02: 4;
	canbittype 	CFG_DATA_CODE_RSP_EPBDigit_01: 4;
	canbittype 	unused0: 4;
} _c_CFG_DATA_CODE_RSP_EPB_RDS_msgType;

typedef struct {
	canbittype 	CFG_DATA_CODE_RSP_TRANSMISSIONDigit_11: 4;
	canbittype 	CFG_DATA_CODE_RSP_TRANSMISSIONDigit_10: 4;
	canbittype 	CFG_DATA_CODE_RSP_TRANSMISSIONDigit_09: 4;
	canbittype 	CFG_DATA_CODE_RSP_TRANSMISSIONDigit_08: 4;
	canbittype 	CFG_DATA_CODE_RSP_TRANSMISSIONDigit_07: 4;
	canbittype 	CFG_DATA_CODE_RSP_TRANSMISSIONDigit_06: 4;
	canbittype 	CFG_DATA_CODE_RSP_TRANSMISSIONDigit_05: 4;
	canbittype 	CFG_DATA_CODE_RSP_TRANSMISSIONDigit_04: 4;
	canbittype 	CFG_DATA_CODE_RSP_TRANSMISSIONDigit_03: 4;
	canbittype 	CFG_DATA_CODE_RSP_TRANSMISSIONDigit_02: 4;
	canbittype 	CFG_DATA_CODE_RSP_TRANSMISSIONDigit_01: 4;
	canbittype 	unused0: 4;
} _c_CFG_DATA_CODE_RSP_TRANSMISSION_msgType;

typedef struct {
	canbittype 	CFG_DATA_CODE_RSP_TRANSMISSIONDigit_11: 4;
	canbittype 	CFG_DATA_CODE_RSP_TRANSMISSIONDigit_10: 4;
	canbittype 	CFG_DATA_CODE_RSP_TRANSMISSIONDigit_09: 4;
	canbittype 	CFG_DATA_CODE_RSP_TRANSMISSIONDigit_08: 4;
	canbittype 	CFG_DATA_CODE_RSP_TRANSMISSIONDigit_07: 4;
	canbittype 	CFG_DATA_CODE_RSP_TRANSMISSIONDigit_06: 4;
	canbittype 	CFG_DATA_CODE_RSP_TRANSMISSIONDigit_05: 4;
	canbittype 	CFG_DATA_CODE_RSP_TRANSMISSIONDigit_04: 4;
	canbittype 	CFG_DATA_CODE_RSP_TRANSMISSIONDigit_03: 4;
	canbittype 	CFG_DATA_CODE_RSP_TRANSMISSIONDigit_02: 4;
	canbittype 	CFG_DATA_CODE_RSP_TRANSMISSIONDigit_01: 4;
	canbittype 	unused0: 4;
} _c_CFG_DATA_CODE_RSP_TRANSMISSION_RDS_msgType;

typedef struct {
	canbittype 	CFG_DATA_CODE_RSP_BSMDigit_11: 4;
	canbittype 	CFG_DATA_CODE_RSP_BSMDigit_10: 4;
	canbittype 	CFG_DATA_CODE_RSP_BSMDigit_09: 4;
	canbittype 	CFG_DATA_CODE_RSP_BSMDigit_08: 4;
	canbittype 	CFG_DATA_CODE_RSP_BSMDigit_07: 4;
	canbittype 	CFG_DATA_CODE_RSP_BSMDigit_06: 4;
	canbittype 	CFG_DATA_CODE_RSP_BSMDigit_05: 4;
	canbittype 	CFG_DATA_CODE_RSP_BSMDigit_04: 4;
	canbittype 	CFG_DATA_CODE_RSP_BSMDigit_03: 4;
	canbittype 	CFG_DATA_CODE_RSP_BSMDigit_02: 4;
	canbittype 	CFG_DATA_CODE_RSP_BSMDigit_01: 4;
	canbittype 	unused0: 4;
} _c_CFG_DATA_CODE_RSP_BSM_msgType;

typedef struct {
	canbittype 	CFG_DATA_CODE_RSP_BSMDigit_11: 4;
	canbittype 	CFG_DATA_CODE_RSP_BSMDigit_10: 4;
	canbittype 	CFG_DATA_CODE_RSP_BSMDigit_09: 4;
	canbittype 	CFG_DATA_CODE_RSP_BSMDigit_08: 4;
	canbittype 	CFG_DATA_CODE_RSP_BSMDigit_07: 4;
	canbittype 	CFG_DATA_CODE_RSP_BSMDigit_06: 4;
	canbittype 	CFG_DATA_CODE_RSP_BSMDigit_05: 4;
	canbittype 	CFG_DATA_CODE_RSP_BSMDigit_04: 4;
	canbittype 	CFG_DATA_CODE_RSP_BSMDigit_03: 4;
	canbittype 	CFG_DATA_CODE_RSP_BSMDigit_02: 4;
	canbittype 	CFG_DATA_CODE_RSP_BSMDigit_01: 4;
	canbittype 	unused0: 4;
} _c_CFG_DATA_CODE_RSP_BSM_RDS_msgType;

typedef struct {
	canbittype 	CFG_DATA_CODE_RSP_EPSDigit_11: 4;
	canbittype 	CFG_DATA_CODE_RSP_EPSDigit_10: 4;
	canbittype 	CFG_DATA_CODE_RSP_EPSDigit_09: 4;
	canbittype 	CFG_DATA_CODE_RSP_EPSDigit_08: 4;
	canbittype 	CFG_DATA_CODE_RSP_EPSDigit_07: 4;
	canbittype 	CFG_DATA_CODE_RSP_EPSDigit_06: 4;
	canbittype 	CFG_DATA_CODE_RSP_EPSDigit_05: 4;
	canbittype 	CFG_DATA_CODE_RSP_EPSDigit_04: 4;
	canbittype 	CFG_DATA_CODE_RSP_EPSDigit_03: 4;
	canbittype 	CFG_DATA_CODE_RSP_EPSDigit_02: 4;
	canbittype 	CFG_DATA_CODE_RSP_EPSDigit_01: 4;
	canbittype 	unused0: 4;
} _c_CFG_DATA_CODE_RSP_EPS_msgType;

typedef struct {
	canbittype 	CFG_DATA_CODE_RSP_EPSDigit_11: 4;
	canbittype 	CFG_DATA_CODE_RSP_EPSDigit_10: 4;
	canbittype 	CFG_DATA_CODE_RSP_EPSDigit_09: 4;
	canbittype 	CFG_DATA_CODE_RSP_EPSDigit_08: 4;
	canbittype 	CFG_DATA_CODE_RSP_EPSDigit_07: 4;
	canbittype 	CFG_DATA_CODE_RSP_EPSDigit_06: 4;
	canbittype 	CFG_DATA_CODE_RSP_EPSDigit_05: 4;
	canbittype 	CFG_DATA_CODE_RSP_EPSDigit_04: 4;
	canbittype 	CFG_DATA_CODE_RSP_EPSDigit_03: 4;
	canbittype 	CFG_DATA_CODE_RSP_EPSDigit_02: 4;
	canbittype 	CFG_DATA_CODE_RSP_EPSDigit_01: 4;
	canbittype 	unused0: 4;
} _c_CFG_DATA_CODE_RSP_EPS_RDS_msgType;

typedef struct {
	canbittype 	CFG_DATA_CODE_RSP_ECMDigit_11: 4;
	canbittype 	CFG_DATA_CODE_RSP_ECMDigit_10: 4;
	canbittype 	CFG_DATA_CODE_RSP_ECMDigit_09: 4;
	canbittype 	CFG_DATA_CODE_RSP_ECMDigit_08: 4;
	canbittype 	CFG_DATA_CODE_RSP_ECMDigit_07: 4;
	canbittype 	CFG_DATA_CODE_RSP_ECMDigit_06: 4;
	canbittype 	CFG_DATA_CODE_RSP_ECMDigit_05: 4;
	canbittype 	CFG_DATA_CODE_RSP_ECMDigit_04: 4;
	canbittype 	CFG_DATA_CODE_RSP_ECMDigit_03: 4;
	canbittype 	CFG_DATA_CODE_RSP_ECMDigit_02: 4;
	canbittype 	CFG_DATA_CODE_RSP_ECMDigit_01: 4;
	canbittype 	unused0: 4;
} _c_CFG_DATA_CODE_RSP_ECM_msgType;

typedef struct {
	canbittype 	CFG_DATA_CODE_RSP_ECMDigit_11: 4;
	canbittype 	CFG_DATA_CODE_RSP_ECMDigit_10: 4;
	canbittype 	CFG_DATA_CODE_RSP_ECMDigit_09: 4;
	canbittype 	CFG_DATA_CODE_RSP_ECMDigit_08: 4;
	canbittype 	CFG_DATA_CODE_RSP_ECMDigit_07: 4;
	canbittype 	CFG_DATA_CODE_RSP_ECMDigit_06: 4;
	canbittype 	CFG_DATA_CODE_RSP_ECMDigit_05: 4;
	canbittype 	CFG_DATA_CODE_RSP_ECMDigit_04: 4;
	canbittype 	CFG_DATA_CODE_RSP_ECMDigit_03: 4;
	canbittype 	CFG_DATA_CODE_RSP_ECMDigit_02: 4;
	canbittype 	CFG_DATA_CODE_RSP_ECMDigit_01: 4;
	canbittype 	unused0: 4;
} _c_CFG_DATA_CODE_RSP_ECM_RDS_msgType;

typedef struct {
	canbittype 	unused0: 5;
	canbittype 	STATUS_C_DASMEOL: 1;
	canbittype 	STATUS_C_DASMCurrentFailSts: 1;
	canbittype 	STATUS_C_DASMGenericFailSts: 1;
	canbittype 	STATUS_C_DASMASBM_LedControlSts: 2;
	canbittype 	unused1: 6;
} _c_STATUS_C_DASM_msgType;

typedef struct {
	canbittype 	unused0: 5;
	canbittype 	STATUS_C_DASMEOL: 1;
	canbittype 	STATUS_C_DASMCurrentFailSts: 1;
	canbittype 	STATUS_C_DASMGenericFailSts: 1;
	canbittype 	STATUS_C_DASMASBM_LedControlSts: 2;
	canbittype 	unused1: 6;
} _c_STATUS_C_DASM_RDS_msgType;

typedef struct {
	canbittype 	unused0: 5;
	canbittype 	STATUS_C_AHLMEOL: 1;
	canbittype 	STATUS_C_AHLMCurrentFailSts: 1;
	canbittype 	STATUS_C_AHLMGenericFailSts: 1;
	canbittype 	unused1: 8;
} _c_STATUS_C_AHLM_msgType;

typedef struct {
	canbittype 	unused0: 5;
	canbittype 	STATUS_C_AHLMEOL: 1;
	canbittype 	STATUS_C_AHLMCurrentFailSts: 1;
	canbittype 	STATUS_C_AHLMGenericFailSts: 1;
	canbittype 	unused1: 8;
} _c_STATUS_C_AHLM_RDS_msgType;

typedef struct {
	canbittype 	DIAGNOSTIC_REQUEST_BCM_0N_PDU: 8;
	canbittype 	unused0: 8;
	canbittype 	unused1: 8;
	canbittype 	unused2: 8;
	canbittype 	unused3: 8;
	canbittype 	unused4: 8;
	canbittype 	unused5: 8;
	canbittype 	unused6: 8;
} _c_DIAGNOSTIC_REQUEST_BCM_0_RDS_msgType;

typedef struct {
	canbittype 	unused0: 5;
	canbittype 	STATUS_C_HALFEOL: 1;
	canbittype 	STATUS_C_HALFCurrentFailSts: 1;
	canbittype 	STATUS_C_HALFGenericFailSts: 1;
	canbittype 	unused1: 8;
	canbittype 	unused2: 5;
	canbittype 	STATUS_C_HALFConfigSts: 3;
	canbittype 	STATUS_C_HALFASBM_LedControlSts: 2;
	canbittype 	STATUS_C_HALFTorqueIntensitySts: 3;
	canbittype 	STATUS_C_HALFAHB_ON: 1;
	canbittype 	unused3: 2;
	canbittype 	unused4: 8;
	canbittype 	unused5: 8;
	canbittype 	unused6: 8;
	canbittype 	unused7: 8;
} _c_STATUS_C_HALF_msgType;

typedef struct {
	canbittype 	unused0: 5;
	canbittype 	STATUS_C_HALFEOL: 1;
	canbittype 	STATUS_C_HALFCurrentFailSts: 1;
	canbittype 	STATUS_C_HALFGenericFailSts: 1;
	canbittype 	unused1: 8;
	canbittype 	unused2: 5;
	canbittype 	STATUS_C_HALFConfigSts: 3;
	canbittype 	STATUS_C_HALFASBM_LedControlSts: 2;
	canbittype 	STATUS_C_HALFTorqueIntensitySts: 3;
	canbittype 	STATUS_C_HALFAHB_ON: 1;
	canbittype 	unused3: 2;
	canbittype 	unused4: 8;
	canbittype 	unused5: 8;
	canbittype 	unused6: 8;
	canbittype 	unused7: 8;
} _c_STATUS_C_HALF_RDS_msgType;

typedef struct {
	canbittype 	unused0: 5;
	canbittype 	STATUS_C_ESLEOL: 1;
	canbittype 	STATUS_C_ESLCurrentFailSts: 1;
	canbittype 	STATUS_C_ESLGenericFailSts: 1;
	canbittype 	unused2: 1;
	canbittype 	STATUS_C_ESLKeyOffWithHighSpeedSts: 1;
	canbittype 	STATUS_C_ESLESLSts: 3;
	canbittype 	unused1: 3;
} _c_STATUS_C_ESL_msgType;

typedef struct {
	canbittype 	unused0: 5;
	canbittype 	STATUS_C_ESLEOL: 1;
	canbittype 	STATUS_C_ESLCurrentFailSts: 1;
	canbittype 	STATUS_C_ESLGenericFailSts: 1;
	canbittype 	unused2: 1;
	canbittype 	STATUS_C_ESLKeyOffWithHighSpeedSts: 1;
	canbittype 	STATUS_C_ESLESLSts: 3;
	canbittype 	unused1: 3;
} _c_STATUS_C_ESL_RDS_msgType;

typedef struct {
	canbittype 	unused0: 5;
	canbittype 	STATUS_C_EPBEOL: 1;
	canbittype 	STATUS_C_EPBCurrentFailSts: 1;
	canbittype 	STATUS_C_EPBGenericFailSts: 1;
	canbittype 	unused1: 8;
	canbittype 	unused2: 6;
	canbittype 	STATUS_C_EPBStopLampActive: 2;
	canbittype 	unused3: 1;
	canbittype 	STATUS_C_EPBServiceModeSts: 1;
	canbittype 	STATUS_C_EPBEPBChimeReq: 3;
	canbittype 	STATUS_C_EPBAutoParkSts: 2;
	canbittype 	STATUS_C_EPBEPBSts: 1;
	canbittype 	unused4: 3;
	canbittype 	STATUS_C_EPBTextDisplay: 5;
	canbittype 	STATUS_C_EPBEPBWarningLampReq: 2;
	canbittype 	STATUS_C_EPBFunctionLamp: 2;
	canbittype 	unused5: 4;
	canbittype 	unused6: 8;
} _c_STATUS_C_EPB_msgType;

typedef struct {
	canbittype 	unused0: 5;
	canbittype 	STATUS_C_EPBEOL: 1;
	canbittype 	STATUS_C_EPBCurrentFailSts: 1;
	canbittype 	STATUS_C_EPBGenericFailSts: 1;
	canbittype 	unused1: 8;
	canbittype 	unused2: 6;
	canbittype 	STATUS_C_EPBStopLampActive: 2;
	canbittype 	unused3: 1;
	canbittype 	STATUS_C_EPBServiceModeSts: 1;
	canbittype 	STATUS_C_EPBEPBChimeReq: 3;
	canbittype 	STATUS_C_EPBAutoParkSts: 2;
	canbittype 	STATUS_C_EPBEPBSts: 1;
	canbittype 	unused4: 3;
	canbittype 	STATUS_C_EPBTextDisplay: 5;
	canbittype 	STATUS_C_EPBEPBWarningLampReq: 2;
	canbittype 	STATUS_C_EPBFunctionLamp: 2;
	canbittype 	unused5: 4;
	canbittype 	unused6: 8;
} _c_STATUS_C_EPB_RDS_msgType;

typedef struct {
	canbittype 	unused0: 6;
	canbittype 	HALF_C_Warning_RQHALF_RF_CHIME_RQSts: 1;
	canbittype 	HALF_C_Warning_RQHALF_LF_CHIME_RQSts: 1;
	canbittype 	HALF_C_Warning_RQHALF_CHIME_REP_RATESts: 4;
	canbittype 	HALF_C_Warning_RQHALF_CHIME_TYPESts: 4;
} _c_HALF_C_Warning_RQ_msgType;

typedef struct {
	canbittype 	unused0: 6;
	canbittype 	HALF_C_Warning_RQHALF_RF_CHIME_RQSts: 1;
	canbittype 	HALF_C_Warning_RQHALF_LF_CHIME_RQSts: 1;
	canbittype 	HALF_C_Warning_RQHALF_CHIME_REP_RATESts: 4;
	canbittype 	HALF_C_Warning_RQHALF_CHIME_TYPESts: 4;
} _c_HALF_C_Warning_RQ_RDS_msgType;

typedef struct {
	canbittype 	unused1: 2;
	canbittype 	StW_Actn_Rq_0WprWashSw_Psd: 2;
	canbittype 	unused0: 2;
	canbittype 	StW_Actn_Rq_0RT_TURN_RQ: 1;
	canbittype 	StW_Actn_Rq_0LT_TURN_RQ: 1;
	canbittype 	StW_Actn_Rq_0HI_BEAM_LMP_RQ: 2;
	canbittype 	unused2: 4;
	canbittype 	StW_Actn_Rq_0WprWash_R_Sw_Posn_V3: 2;
	canbittype 	unused3: 4;
	canbittype 	StW_Actn_Rq_0WprSw6Posn: 4;
	canbittype 	unused4: 8;
	canbittype 	unused5: 8;
	canbittype 	unused6: 8;
	canbittype 	unused7: 8;
} _c_StW_Actn_Rq_0_msgType;

typedef struct {
	canbittype 	unused1: 2;
	canbittype 	StW_Actn_Rq_0WprWashSw_Psd: 2;
	canbittype 	unused0: 2;
	canbittype 	StW_Actn_Rq_0RT_TURN_RQ: 1;
	canbittype 	StW_Actn_Rq_0LT_TURN_RQ: 1;
	canbittype 	StW_Actn_Rq_0HI_BEAM_LMP_RQ: 2;
	canbittype 	unused2: 4;
	canbittype 	StW_Actn_Rq_0WprWash_R_Sw_Posn_V3: 2;
	canbittype 	unused3: 4;
	canbittype 	StW_Actn_Rq_0WprSw6Posn: 4;
	canbittype 	unused4: 8;
	canbittype 	unused5: 8;
	canbittype 	unused6: 8;
	canbittype 	unused7: 8;
} _c_StW_Actn_Rq_0_RDS_msgType;

typedef struct {
	canbittype 	unused0: 5;
	canbittype 	STATUS_C_TRANSMISSIONEOL: 1;
	canbittype 	STATUS_C_TRANSMISSIONCurrentFailSts: 1;
	canbittype 	STATUS_C_TRANSMISSIONGenericFailSts: 1;
	canbittype 	unused1: 1;
	canbittype 	STATUS_C_TRANSMISSIONShiftModeSts: 1;
	canbittype 	STATUS_C_TRANSMISSIONDriveModeSts: 3;
	canbittype 	STATUS_C_TRANSMISSIONShiftLeverPosition: 3;
	canbittype 	unused2: 4;
	canbittype 	STATUS_C_TRANSMISSIONActualGearForDisplay: 4;
	canbittype 	unused3: 8;
	canbittype 	unused4: 8;
	canbittype 	unused5: 8;
	canbittype 	unused6: 8;
	canbittype 	unused7: 8;
} _c_STATUS_C_TRANSMISSION_msgType;

typedef struct {
	canbittype 	unused0: 5;
	canbittype 	STATUS_C_TRANSMISSIONEOL: 1;
	canbittype 	STATUS_C_TRANSMISSIONCurrentFailSts: 1;
	canbittype 	STATUS_C_TRANSMISSIONGenericFailSts: 1;
	canbittype 	unused1: 1;
	canbittype 	STATUS_C_TRANSMISSIONShiftModeSts: 1;
	canbittype 	STATUS_C_TRANSMISSIONDriveModeSts: 3;
	canbittype 	STATUS_C_TRANSMISSIONShiftLeverPosition: 3;
	canbittype 	unused2: 4;
	canbittype 	STATUS_C_TRANSMISSIONActualGearForDisplay: 4;
	canbittype 	unused3: 8;
	canbittype 	unused4: 8;
	canbittype 	unused5: 8;
	canbittype 	unused6: 8;
	canbittype 	unused7: 8;
} _c_STATUS_C_TRANSMISSION_RDS_msgType;

typedef struct {
	canbittype 	unused0: 6;
	canbittype 	VIN_0VIN_MSG: 2;
	canbittype 	VIN_0VIN_DATA_6: 8;
	canbittype 	VIN_0VIN_DATA_5: 8;
	canbittype 	VIN_0VIN_DATA_4: 8;
	canbittype 	VIN_0VIN_DATA_3: 8;
	canbittype 	VIN_0VIN_DATA_2: 8;
	canbittype 	VIN_0VIN_DATA_1: 8;
	canbittype 	VIN_0VIN_DATA_0: 8;
} _c_VIN_0_msgType;

typedef struct {
	canbittype 	unused0: 6;
	canbittype 	VIN_0VIN_MSG: 2;
	canbittype 	VIN_0VIN_DATA_6: 8;
	canbittype 	VIN_0VIN_DATA_5: 8;
	canbittype 	VIN_0VIN_DATA_4: 8;
	canbittype 	VIN_0VIN_DATA_3: 8;
	canbittype 	VIN_0VIN_DATA_2: 8;
	canbittype 	VIN_0VIN_DATA_1: 8;
	canbittype 	VIN_0VIN_DATA_0: 8;
} _c_VIN_0_RDS_msgType;

typedef struct {
	canbittype 	unused0: 2;
	canbittype 	MOT3ACPressure_1: 6;
	canbittype 	MOT3ACPressure_0: 3;
	canbittype 	unused1: 5;
	canbittype 	MOT3AtmosphericPressure: 8;
	canbittype 	unused2: 8;
	canbittype 	unused4: 2;
	canbittype 	MOT3EngDriveModeSts: 3;
	canbittype 	unused3: 2;
	canbittype 	MOT3FuelFilterHeaterReq: 1;
	canbittype 	unused5: 8;
	canbittype 	unused6: 8;
	canbittype 	unused7: 8;
} _c_MOT3_msgType;

typedef struct {
	canbittype 	unused0: 2;
	canbittype 	MOT3ACPressure_1: 6;
	canbittype 	MOT3ACPressure_0: 3;
	canbittype 	unused1: 5;
	canbittype 	MOT3AtmosphericPressure: 8;
	canbittype 	unused2: 8;
	canbittype 	unused4: 2;
	canbittype 	MOT3EngDriveModeSts: 3;
	canbittype 	unused3: 2;
	canbittype 	MOT3FuelFilterHeaterReq: 1;
	canbittype 	unused5: 8;
	canbittype 	unused6: 8;
	canbittype 	unused7: 8;
} _c_MOT3_RDS_msgType;

typedef struct {
	canbittype 	unused0: 8;
	canbittype 	unused2: 5;
	canbittype 	ECM_1CHRG_FAIL: 1;
	canbittype 	unused1: 2;
	canbittype 	unused3: 8;
	canbittype 	unused4: 8;
	canbittype 	unused5: 8;
	canbittype 	unused7: 3;
	canbittype 	ECM_1SpStSwStat: 2;
	canbittype 	unused6: 3;
	canbittype 	unused8: 8;
	canbittype 	unused9: 8;
} _c_ECM_1_msgType;

typedef struct {
	canbittype 	unused0: 8;
	canbittype 	unused2: 5;
	canbittype 	ECM_1CHRG_FAIL: 1;
	canbittype 	unused1: 2;
	canbittype 	unused3: 8;
	canbittype 	unused4: 8;
	canbittype 	unused5: 8;
	canbittype 	unused7: 3;
	canbittype 	ECM_1SpStSwStat: 2;
	canbittype 	unused6: 3;
	canbittype 	unused8: 8;
	canbittype 	unused9: 8;
} _c_ECM_1_RDS_msgType;

typedef struct {
	canbittype 	unused0: 5;
	canbittype 	STATUS_C_DTCMEOL: 1;
	canbittype 	STATUS_C_DTCMCurrentFailSts: 1;
	canbittype 	STATUS_C_DTCMGenericFailSts: 1;
	canbittype 	unused1: 6;
	canbittype 	STATUS_C_DTCMTerrainMudSandLmp: 2;
	canbittype 	unused3: 2;
	canbittype 	STATUS_C_DTCMAWD4LowLmp: 2;
	canbittype 	STATUS_C_DTCMAWDNeutralLmp: 2;
	canbittype 	unused2: 2;
	canbittype 	STATUS_C_DTCMTerrainModeStat: 4;
	canbittype 	STATUS_C_DTCMTerrainSportLmp: 2;
	canbittype 	STATUS_C_DTCMTerrainAutoLmp: 2;
	canbittype 	STATUS_C_DTCMTerrainSnowLmp: 2;
	canbittype 	STATUS_C_DTCMTerrainRockLmp: 2;
	canbittype 	STATUS_C_DTCMELockerStat: 2;
	canbittype 	STATUS_C_DTCMELockerLmp: 2;
	canbittype 	STATUS_C_DTCMTerrain_HMI_Rq: 3;
	canbittype 	unused4: 5;
	canbittype 	unused5: 8;
	canbittype 	unused6: 8;
} _c_STATUS_C_DTCM_msgType;

typedef struct {
	canbittype 	unused0: 5;
	canbittype 	STATUS_C_DTCMEOL: 1;
	canbittype 	STATUS_C_DTCMCurrentFailSts: 1;
	canbittype 	STATUS_C_DTCMGenericFailSts: 1;
	canbittype 	unused1: 6;
	canbittype 	STATUS_C_DTCMTerrainMudSandLmp: 2;
	canbittype 	unused3: 2;
	canbittype 	STATUS_C_DTCMAWD4LowLmp: 2;
	canbittype 	STATUS_C_DTCMAWDNeutralLmp: 2;
	canbittype 	unused2: 2;
	canbittype 	STATUS_C_DTCMTerrainModeStat: 4;
	canbittype 	STATUS_C_DTCMTerrainSportLmp: 2;
	canbittype 	STATUS_C_DTCMTerrainAutoLmp: 2;
	canbittype 	STATUS_C_DTCMTerrainSnowLmp: 2;
	canbittype 	STATUS_C_DTCMTerrainRockLmp: 2;
	canbittype 	STATUS_C_DTCMELockerStat: 2;
	canbittype 	STATUS_C_DTCMELockerLmp: 2;
	canbittype 	STATUS_C_DTCMTerrain_HMI_Rq: 3;
	canbittype 	unused4: 5;
	canbittype 	unused5: 8;
	canbittype 	unused6: 8;
} _c_STATUS_C_DTCM_RDS_msgType;

typedef struct {
	canbittype 	unused0: 7;
	canbittype 	DAS_A2FCW_Setting: 1;
	canbittype 	DAS_A2FCW_Brk: 1;
	canbittype 	unused1: 7;
	canbittype 	unused2: 8;
	canbittype 	unused3: 8;
	canbittype 	unused4: 8;
	canbittype 	unused5: 8;
	canbittype 	unused6: 8;
} _c_DAS_A2_msgType;

typedef struct {
	canbittype 	unused0: 7;
	canbittype 	DAS_A2FCW_Setting: 1;
	canbittype 	DAS_A2FCW_Brk: 1;
	canbittype 	unused1: 7;
	canbittype 	unused2: 8;
	canbittype 	unused3: 8;
	canbittype 	unused4: 8;
	canbittype 	unused5: 8;
	canbittype 	unused6: 8;
} _c_DAS_A2_RDS_msgType;

typedef struct {
	canbittype 	unused0: 8;
	canbittype 	unused1: 8;
	canbittype 	unused2: 6;
	canbittype 	PAM_2PAM_LF_CHIME_RQSts: 1;
	canbittype 	PAM_2PAM_RF_CHIME_RQSts: 1;
	canbittype 	unused3: 7;
	canbittype 	PAM_2PAM_VOL_F_1: 1;
	canbittype 	PAM_2PAM_VOL_F_0: 1;
	canbittype 	unused4: 7;
} _c_PAM_2_msgType;

typedef struct {
	canbittype 	unused0: 8;
	canbittype 	unused1: 8;
	canbittype 	unused2: 6;
	canbittype 	PAM_2PAM_LF_CHIME_RQSts: 1;
	canbittype 	PAM_2PAM_RF_CHIME_RQSts: 1;
	canbittype 	unused3: 7;
	canbittype 	PAM_2PAM_VOL_F_1: 1;
	canbittype 	PAM_2PAM_VOL_F_0: 1;
	canbittype 	unused4: 7;
} _c_PAM_2_RDS_msgType;

typedef struct {
	canbittype 	EPB_A1EPBHoldSts: 3;
	canbittype 	unused0: 5;
	canbittype 	unused1: 8;
	canbittype 	unused2: 8;
} _c_EPB_A1_msgType;

typedef struct {
	canbittype 	EPB_A1EPBHoldSts: 3;
	canbittype 	unused0: 5;
	canbittype 	unused1: 8;
	canbittype 	unused2: 8;
} _c_EPB_A1_RDS_msgType;

typedef struct {
	canbittype 	unused0: 8;
	canbittype 	unused1: 8;
	canbittype 	unused3: 4;
	canbittype 	DAS_A1DAS_CHIME_TYPSts: 2;
	canbittype 	unused2: 2;
	canbittype 	unused4: 8;
	canbittype 	unused6: 5;
	canbittype 	DAS_A1DAS_LF_CHIME_RQSts: 1;
	canbittype 	unused5: 2;
	canbittype 	unused7: 7;
	canbittype 	DAS_A1DAS_RF_CHIME_RQSts: 1;
	canbittype 	unused8: 8;
	canbittype 	unused9: 8;
} _c_DAS_A1_msgType;

typedef struct {
	canbittype 	unused0: 8;
	canbittype 	unused1: 8;
	canbittype 	unused3: 4;
	canbittype 	DAS_A1DAS_CHIME_TYPSts: 2;
	canbittype 	unused2: 2;
	canbittype 	unused4: 8;
	canbittype 	unused6: 5;
	canbittype 	DAS_A1DAS_LF_CHIME_RQSts: 1;
	canbittype 	unused5: 2;
	canbittype 	unused7: 7;
	canbittype 	DAS_A1DAS_RF_CHIME_RQSts: 1;
	canbittype 	unused8: 8;
	canbittype 	unused9: 8;
} _c_DAS_A1_RDS_msgType;

typedef struct {
	canbittype 	BSM_4ESP_DSBL: 1;
	canbittype 	unused0: 7;
	canbittype 	unused2: 3;
	canbittype 	BSM_4HILL_DES_STAT: 2;
	canbittype 	unused1: 3;
	canbittype 	unused3: 8;
	canbittype 	unused4: 8;
	canbittype 	unused5: 4;
	canbittype 	BSM_4SelectSpdLmp: 2;
	canbittype 	BSM_4SelectSpdSts: 2;
	canbittype 	unused6: 8;
	canbittype 	unused8: 2;
	canbittype 	BSM_4HDCLmp: 2;
	canbittype 	unused7: 4;
	canbittype 	unused9: 8;
} _c_BSM_4_msgType;

typedef struct {
	canbittype 	BSM_4ESP_DSBL: 1;
	canbittype 	unused0: 7;
	canbittype 	unused2: 3;
	canbittype 	BSM_4HILL_DES_STAT: 2;
	canbittype 	unused1: 3;
	canbittype 	unused3: 8;
	canbittype 	unused4: 8;
	canbittype 	unused5: 4;
	canbittype 	BSM_4SelectSpdLmp: 2;
	canbittype 	BSM_4SelectSpdSts: 2;
	canbittype 	unused6: 8;
	canbittype 	unused8: 2;
	canbittype 	BSM_4HDCLmp: 2;
	canbittype 	unused7: 4;
	canbittype 	unused9: 8;
} _c_BSM_4_RDS_msgType;

typedef struct {
	canbittype 	unused0: 8;
	canbittype 	unused1: 8;
	canbittype 	unused2: 8;
	canbittype 	unused3: 8;
	canbittype 	unused4: 8;
	canbittype 	unused6: 4;
	canbittype 	BSM_2AutoVehHoldSts: 2;
	canbittype 	unused5: 2;
	canbittype 	unused7: 8;
	canbittype 	unused8: 8;
} _c_BSM_2_msgType;

typedef struct {
	canbittype 	unused0: 8;
	canbittype 	unused1: 8;
	canbittype 	unused2: 8;
	canbittype 	unused3: 8;
	canbittype 	unused4: 8;
	canbittype 	unused6: 4;
	canbittype 	BSM_2AutoVehHoldSts: 2;
	canbittype 	unused5: 2;
	canbittype 	unused7: 8;
	canbittype 	unused8: 8;
} _c_BSM_2_RDS_msgType;

typedef struct {
	canbittype 	unused0: 8;
	canbittype 	unused1: 8;
	canbittype 	unused2: 8;
	canbittype 	unused3: 8;
	canbittype 	unused4: 8;
	canbittype 	unused6: 2;
	canbittype 	DTCM_A1AWDSysStat: 3;
	canbittype 	unused5: 3;
	canbittype 	unused7: 8;
	canbittype 	unused8: 8;
} _c_DTCM_A1_msgType;

typedef struct {
	canbittype 	unused0: 8;
	canbittype 	unused1: 8;
	canbittype 	unused2: 8;
	canbittype 	unused3: 8;
	canbittype 	unused4: 8;
	canbittype 	unused6: 2;
	canbittype 	DTCM_A1AWDSysStat: 3;
	canbittype 	unused5: 3;
	canbittype 	unused7: 8;
	canbittype 	unused8: 8;
} _c_DTCM_A1_RDS_msgType;

typedef struct {
	canbittype 	unused1: 6;
	canbittype 	SCShiftLeverPositionReqValidData: 1;
	canbittype 	unused0: 1;
	canbittype 	unused3: 2;
	canbittype 	SCShiftLeverPositionReq: 3;
	canbittype 	unused2: 3;
	canbittype 	unused4: 8;
	canbittype 	unused5: 8;
} _c_SC_msgType;

typedef struct {
	canbittype 	unused1: 6;
	canbittype 	SCShiftLeverPositionReqValidData: 1;
	canbittype 	unused0: 1;
	canbittype 	unused3: 2;
	canbittype 	SCShiftLeverPositionReq: 3;
	canbittype 	unused2: 3;
	canbittype 	unused4: 8;
	canbittype 	unused5: 8;
} _c_SC_RDS_msgType;

typedef struct {
	canbittype 	unused0: 4;
	canbittype 	ORC_YRS_DATALongAcceleration_1: 4;
	canbittype 	ORC_YRS_DATALongAcceleration_0: 8;
	canbittype 	unused1: 8;
	canbittype 	unused2: 8;
	canbittype 	ORC_YRS_DATAYawRate_1: 8;
	canbittype 	ORC_YRS_DATAYawRate_0: 4;
	canbittype 	unused3: 4;
	canbittype 	unused4: 8;
} _c_ORC_YRS_DATA_msgType;

typedef struct {
	canbittype 	unused0: 4;
	canbittype 	ORC_YRS_DATALongAcceleration_1: 4;
	canbittype 	ORC_YRS_DATALongAcceleration_0: 8;
	canbittype 	unused1: 8;
	canbittype 	unused2: 8;
	canbittype 	ORC_YRS_DATAYawRate_1: 8;
	canbittype 	ORC_YRS_DATAYawRate_0: 4;
	canbittype 	unused3: 4;
	canbittype 	unused4: 8;
} _c_ORC_YRS_DATA_RDS_msgType;

typedef struct {
	canbittype 	unused0: 8;
	canbittype 	unused1: 8;
	canbittype 	MOTGEAR2ESS_ENG_ST: 4;
	canbittype 	unused2: 4;
	canbittype 	unused3: 8;
	canbittype 	unused4: 8;
} _c_MOTGEAR2_msgType;

typedef struct {
	canbittype 	unused0: 8;
	canbittype 	unused1: 8;
	canbittype 	MOTGEAR2ESS_ENG_ST: 4;
	canbittype 	unused2: 4;
	canbittype 	unused3: 8;
	canbittype 	unused4: 8;
} _c_MOTGEAR2_RDS_msgType;

typedef struct {
	canbittype 	unused0: 8;
	canbittype 	unused1: 8;
	canbittype 	unused2: 8;
	canbittype 	unused3: 8;
	canbittype 	unused5: 4;
	canbittype 	MOT4StartRelayBCMCmd: 1;
	canbittype 	unused4: 3;
	canbittype 	unused6: 8;
	canbittype 	unused8: 1;
	canbittype 	MOT4EngineSts: 2;
	canbittype 	MOT4CrankHold: 1;
	canbittype 	unused7: 4;
	canbittype 	unused9: 8;
} _c_MOT4_msgType;

typedef struct {
	canbittype 	unused0: 8;
	canbittype 	unused1: 8;
	canbittype 	unused2: 8;
	canbittype 	unused3: 8;
	canbittype 	unused5: 4;
	canbittype 	MOT4StartRelayBCMCmd: 1;
	canbittype 	unused4: 3;
	canbittype 	unused6: 8;
	canbittype 	unused8: 1;
	canbittype 	MOT4EngineSts: 2;
	canbittype 	MOT4CrankHold: 1;
	canbittype 	unused7: 4;
	canbittype 	unused9: 8;
} _c_MOT4_RDS_msgType;

typedef struct {
	canbittype 	BSM_YRS_DATALongAcceleration_Offset: 8;
	canbittype 	unused0: 8;
	canbittype 	BSM_YRS_DATAYawRate_Offset: 8;
	canbittype 	unused1: 8;
} _c_BSM_YRS_DATA_msgType;

typedef struct {
	canbittype 	BSM_YRS_DATALongAcceleration_Offset: 8;
	canbittype 	unused0: 8;
	canbittype 	BSM_YRS_DATAYawRate_Offset: 8;
	canbittype 	unused1: 8;
} _c_BSM_YRS_DATA_RDS_msgType;

typedef struct {
	canbittype 	unused0: 2;
	canbittype 	ASR4LHRFastPulseCounterFailStS: 1;
	canbittype 	ASR4RHRFastPulseCounterFailStS: 1;
	canbittype 	ASR4LHRFastPulseCounter_1: 4;
	canbittype 	ASR4LHRFastPulseCounter_0: 4;
	canbittype 	ASR4RHRFastPulseCounter_1: 4;
	canbittype 	ASR4RHRFastPulseCounter_0: 4;
	canbittype 	unused1: 2;
	canbittype 	ASR4LHFFastPulseCounter_1: 2;
	canbittype 	ASR4LHFFastPulseCounter_0: 6;
	canbittype 	ASR4RHFFastPulseCounter_1: 2;
	canbittype 	ASR4RHFFastPulseCounter_0: 6;
	canbittype 	ASR4LHFFastPulseCounterFailStS: 1;
	canbittype 	ASR4RHFFastPulseCounterFailStS: 1;
	canbittype 	unused2: 8;
	canbittype 	unused3: 8;
	canbittype 	unused4: 8;
} _c_ASR4_msgType;

typedef struct {
	canbittype 	unused0: 2;
	canbittype 	ASR4LHRFastPulseCounterFailStS: 1;
	canbittype 	ASR4RHRFastPulseCounterFailStS: 1;
	canbittype 	ASR4LHRFastPulseCounter_1: 4;
	canbittype 	ASR4LHRFastPulseCounter_0: 4;
	canbittype 	ASR4RHRFastPulseCounter_1: 4;
	canbittype 	ASR4RHRFastPulseCounter_0: 4;
	canbittype 	unused1: 2;
	canbittype 	ASR4LHFFastPulseCounter_1: 2;
	canbittype 	ASR4LHFFastPulseCounter_0: 6;
	canbittype 	ASR4RHFFastPulseCounter_1: 2;
	canbittype 	ASR4RHFFastPulseCounter_0: 6;
	canbittype 	ASR4LHFFastPulseCounterFailStS: 1;
	canbittype 	ASR4RHFFastPulseCounterFailStS: 1;
	canbittype 	unused2: 8;
	canbittype 	unused3: 8;
	canbittype 	unused4: 8;
} _c_ASR4_RDS_msgType;

typedef struct {
	canbittype 	unused0: 8;
	canbittype 	unused1: 8;
	canbittype 	ASR3VehicleSpeedVSOSigFailSts: 1;
	canbittype 	unused2: 7;
	canbittype 	unused3: 8;
	canbittype 	unused4: 3;
	canbittype 	ASR3VehicleSpeedVSOSig_1: 5;
	canbittype 	ASR3VehicleSpeedVSOSig_0: 8;
	canbittype 	unused5: 8;
	canbittype 	unused6: 8;
} _c_ASR3_msgType;

typedef struct {
	canbittype 	unused0: 8;
	canbittype 	unused1: 8;
	canbittype 	ASR3VehicleSpeedVSOSigFailSts: 1;
	canbittype 	unused2: 7;
	canbittype 	unused3: 8;
	canbittype 	unused4: 3;
	canbittype 	ASR3VehicleSpeedVSOSig_1: 5;
	canbittype 	ASR3VehicleSpeedVSOSig_0: 8;
	canbittype 	unused5: 8;
	canbittype 	unused6: 8;
} _c_ASR3_RDS_msgType;

typedef struct {
	canbittype 	RFHUB_A4RF_FobNum: 4;
	canbittype 	RFHUB_A4RFMemRq: 1;
	canbittype 	unused0: 3;
} _c_RFHUB_A4_msgType;

typedef struct {
	canbittype 	RFHUB_A4RF_FobNum: 4;
	canbittype 	RFHUB_A4RFMemRq: 1;
	canbittype 	unused0: 3;
} _c_RFHUB_A4_RDS_msgType;

typedef struct {
	canbittype 	IPC_CFG_FeatureCFG_FeatureCntrl: 8;
	canbittype 	unused0: 6;
	canbittype 	IPC_CFG_FeatureCFG_STAT_RQCntrl: 2;
	canbittype 	IPC_CFG_FeatureCFG_SETCntrl: 8;
} _c_IPC_CFG_Feature_msgType;

typedef struct {
	canbittype 	IPC_CFG_FeatureCFG_FeatureCntrl: 8;
	canbittype 	unused0: 6;
	canbittype 	IPC_CFG_FeatureCFG_STAT_RQCntrl: 2;
	canbittype 	IPC_CFG_FeatureCFG_SETCntrl: 8;
} _c_IPC_CFG_Feature_RDS_msgType;

typedef struct {
	canbittype 	ESL_CODE_RESPONSEControlEncoding: 8;
	canbittype 	ESL_CODE_RESPONSEMiniCryptGCode_1: 8;
	canbittype 	ESL_CODE_RESPONSEMiniCryptGCode_0: 8;
	canbittype 	unused1: 1;
	canbittype 	ESL_CODE_RESPONSEESLLockFailSts: 1;
	canbittype 	ESL_CODE_RESPONSEESLUnlockFailSts: 1;
	canbittype 	unused0: 1;
	canbittype 	ESL_CODE_RESPONSECodeCheckSts: 2;
	canbittype 	ESL_CODE_RESPONSEESLFeedbackCmd_1: 2;
	canbittype 	ESL_CODE_RESPONSEESLFeedbackCmd_0: 1;
	canbittype 	unused2: 7;
} _c_ESL_CODE_RESPONSE_msgType;

typedef struct {
	canbittype 	ESL_CODE_RESPONSEControlEncoding: 8;
	canbittype 	ESL_CODE_RESPONSEMiniCryptGCode_1: 8;
	canbittype 	ESL_CODE_RESPONSEMiniCryptGCode_0: 8;
	canbittype 	unused1: 1;
	canbittype 	ESL_CODE_RESPONSEESLLockFailSts: 1;
	canbittype 	ESL_CODE_RESPONSEESLUnlockFailSts: 1;
	canbittype 	unused0: 1;
	canbittype 	ESL_CODE_RESPONSECodeCheckSts: 2;
	canbittype 	ESL_CODE_RESPONSEESLFeedbackCmd_1: 2;
	canbittype 	ESL_CODE_RESPONSEESLFeedbackCmd_0: 1;
	canbittype 	unused2: 7;
} _c_ESL_CODE_RESPONSE_RDS_msgType;

typedef struct {
	canbittype 	IMMO_CODE_REQUESTControlEncoding: 8;
	canbittype 	IMMO_CODE_REQUESTrnd_1: 8;
	canbittype 	IMMO_CODE_REQUESTrnd_2: 8;
	canbittype 	IMMO_CODE_REQUESTrnd_3: 8;
	canbittype 	IMMO_CODE_REQUESTrnd_4: 8;
	canbittype 	IMMO_CODE_REQUESTf1_1: 8;
	canbittype 	IMMO_CODE_REQUESTf1_2: 8;
} _c_IMMO_CODE_REQUEST_msgType;

typedef struct {
	canbittype 	IMMO_CODE_REQUESTControlEncoding: 8;
	canbittype 	IMMO_CODE_REQUESTrnd_1: 8;
	canbittype 	IMMO_CODE_REQUESTrnd_2: 8;
	canbittype 	IMMO_CODE_REQUESTrnd_3: 8;
	canbittype 	IMMO_CODE_REQUESTrnd_4: 8;
	canbittype 	IMMO_CODE_REQUESTf1_1: 8;
	canbittype 	IMMO_CODE_REQUESTf1_2: 8;
} _c_IMMO_CODE_REQUEST_RDS_msgType;

typedef struct {
	canbittype 	TRM_MKP_KEYTotalFrameNumber: 4;
	canbittype 	TRM_MKP_KEYFrameNumber: 4;
	canbittype 	TRM_MKP_KEYDATA1: 8;
	canbittype 	TRM_MKP_KEYDATA2: 8;
	canbittype 	TRM_MKP_KEYDATA3: 8;
	canbittype 	TRM_MKP_KEYDATA4: 8;
	canbittype 	TRM_MKP_KEYDATA5: 8;
	canbittype 	TRM_MKP_KEYDATA6: 8;
	canbittype 	TRM_MKP_KEYDATA7: 8;
} _c_TRM_MKP_KEY_msgType;

typedef struct {
	canbittype 	TRM_MKP_KEYTotalFrameNumber: 4;
	canbittype 	TRM_MKP_KEYFrameNumber: 4;
	canbittype 	TRM_MKP_KEYDATA1: 8;
	canbittype 	TRM_MKP_KEYDATA2: 8;
	canbittype 	TRM_MKP_KEYDATA3: 8;
	canbittype 	TRM_MKP_KEYDATA4: 8;
	canbittype 	TRM_MKP_KEYDATA5: 8;
	canbittype 	TRM_MKP_KEYDATA6: 8;
	canbittype 	TRM_MKP_KEYDATA7: 8;
} _c_TRM_MKP_KEY_RDS_msgType;

typedef struct {
	canbittype 	unused0: 7;
	canbittype 	RFHUB_A3CsoPEEnableDisableSts: 1;
	canbittype 	unused1: 8;
} _c_RFHUB_A3_msgType;

typedef struct {
	canbittype 	unused0: 7;
	canbittype 	RFHUB_A3CsoPEEnableDisableSts: 1;
	canbittype 	unused1: 8;
} _c_RFHUB_A3_RDS_msgType;

typedef struct {
	canbittype 	APPL_ECU_BCM_0APPL_ECU_BCM_7: 8;
	canbittype 	APPL_ECU_BCM_0APPL_ECU_BCM_6: 8;
	canbittype 	APPL_ECU_BCM_0APPL_ECU_BCM_5: 8;
	canbittype 	APPL_ECU_BCM_0APPL_ECU_BCM_4: 8;
	canbittype 	APPL_ECU_BCM_0APPL_ECU_BCM_3: 8;
	canbittype 	APPL_ECU_BCM_0APPL_ECU_BCM_2: 8;
	canbittype 	APPL_ECU_BCM_0APPL_ECU_BCM_1: 8;
	canbittype 	APPL_ECU_BCM_0APPL_ECU_BCM_0: 8;
} _c_APPL_ECU_BCM_0_msgType;

typedef struct {
	canbittype 	APPL_ECU_BCM_0APPL_ECU_BCM_7: 8;
	canbittype 	APPL_ECU_BCM_0APPL_ECU_BCM_6: 8;
	canbittype 	APPL_ECU_BCM_0APPL_ECU_BCM_5: 8;
	canbittype 	APPL_ECU_BCM_0APPL_ECU_BCM_4: 8;
	canbittype 	APPL_ECU_BCM_0APPL_ECU_BCM_3: 8;
	canbittype 	APPL_ECU_BCM_0APPL_ECU_BCM_2: 8;
	canbittype 	APPL_ECU_BCM_0APPL_ECU_BCM_1: 8;
	canbittype 	APPL_ECU_BCM_0APPL_ECU_BCM_0: 8;
} _c_APPL_ECU_BCM_0_RDS_msgType;

typedef struct {
	canbittype 	unused0: 5;
	canbittype 	STATUS_C_SCCMEOL: 1;
	canbittype 	STATUS_C_SCCMCurrentFailSts: 1;
	canbittype 	STATUS_C_SCCMGenericFailSts: 1;
	canbittype 	unused1: 8;
	canbittype 	unused2: 8;
	canbittype 	unused3: 8;
} _c_STATUS_C_SCCM_msgType;

typedef struct {
	canbittype 	unused0: 5;
	canbittype 	STATUS_C_SCCMEOL: 1;
	canbittype 	STATUS_C_SCCMCurrentFailSts: 1;
	canbittype 	STATUS_C_SCCMGenericFailSts: 1;
	canbittype 	unused1: 8;
	canbittype 	unused2: 8;
	canbittype 	unused3: 8;
} _c_STATUS_C_SCCM_RDS_msgType;

typedef struct {
	canbittype 	unused0: 5;
	canbittype 	STATUS_C_RFHMEOL: 1;
	canbittype 	STATUS_C_RFHMCurrentFailSts: 1;
	canbittype 	STATUS_C_RFHMGenericFailSts: 1;
	canbittype 	STATUS_C_RFHMPowerModeSts: 2;
	canbittype 	unused1: 6;
	canbittype 	unused2: 8;
	canbittype 	unused3: 8;
} _c_STATUS_C_RFHM_msgType;

typedef struct {
	canbittype 	unused0: 5;
	canbittype 	STATUS_C_RFHMEOL: 1;
	canbittype 	STATUS_C_RFHMCurrentFailSts: 1;
	canbittype 	STATUS_C_RFHMGenericFailSts: 1;
	canbittype 	STATUS_C_RFHMPowerModeSts: 2;
	canbittype 	unused1: 6;
	canbittype 	unused2: 8;
	canbittype 	unused3: 8;
} _c_STATUS_C_RFHM_RDS_msgType;

typedef struct {
	canbittype 	IPC_A1FuelLvlLow: 1;
	canbittype 	unused0: 7;
	canbittype 	unused1: 8;
	canbittype 	unused2: 8;
	canbittype 	unused3: 8;
} _c_IPC_A1_msgType;

typedef struct {
	canbittype 	IPC_A1FuelLvlLow: 1;
	canbittype 	unused0: 7;
	canbittype 	unused1: 8;
	canbittype 	unused2: 8;
	canbittype 	unused3: 8;
} _c_IPC_A1_RDS_msgType;

typedef struct {
	canbittype 	unused0: 5;
	canbittype 	STATUS_C_SPMEOL: 1;
	canbittype 	STATUS_C_SPMCurrentFailSts: 1;
	canbittype 	STATUS_C_SPMGenericFailSts: 1;
	canbittype 	unused1: 8;
	canbittype 	unused2: 8;
	canbittype 	unused3: 8;
	canbittype 	unused4: 8;
	canbittype 	unused5: 8;
	canbittype 	unused7: 5;
	canbittype 	STATUS_C_SPMASBM_LedControlSts: 2;
	canbittype 	unused6: 1;
	canbittype 	unused8: 8;
} _c_STATUS_C_SPM_msgType;

typedef struct {
	canbittype 	unused0: 5;
	canbittype 	STATUS_C_SPMEOL: 1;
	canbittype 	STATUS_C_SPMCurrentFailSts: 1;
	canbittype 	STATUS_C_SPMGenericFailSts: 1;
	canbittype 	unused1: 8;
	canbittype 	unused2: 8;
	canbittype 	unused3: 8;
	canbittype 	unused4: 8;
	canbittype 	unused5: 8;
	canbittype 	unused7: 5;
	canbittype 	STATUS_C_SPMASBM_LedControlSts: 2;
	canbittype 	unused6: 1;
	canbittype 	unused8: 8;
} _c_STATUS_C_SPM_RDS_msgType;

typedef struct {
	canbittype 	unused0: 5;
	canbittype 	STATUS_C_IPCEOL: 1;
	canbittype 	STATUS_C_IPCCurrentFailSts: 1;
	canbittype 	STATUS_C_IPCGenericFailSts: 1;
	canbittype 	unused1: 8;
	canbittype 	unused2: 8;
	canbittype 	unused3: 8;
	canbittype 	unused4: 8;
	canbittype 	unused5: 8;
	canbittype 	unused6: 8;
	canbittype 	unused7: 8;
} _c_STATUS_C_IPC_msgType;

typedef struct {
	canbittype 	unused0: 5;
	canbittype 	STATUS_C_IPCEOL: 1;
	canbittype 	STATUS_C_IPCCurrentFailSts: 1;
	canbittype 	STATUS_C_IPCGenericFailSts: 1;
	canbittype 	unused1: 8;
	canbittype 	unused2: 8;
	canbittype 	unused3: 8;
	canbittype 	unused4: 8;
	canbittype 	unused5: 8;
	canbittype 	unused6: 8;
	canbittype 	unused7: 8;
} _c_STATUS_C_IPC_RDS_msgType;

typedef struct {
	canbittype 	unused0: 5;
	canbittype 	STATUS_C_GSMEOL: 1;
	canbittype 	STATUS_C_GSMCurrentFailSts: 1;
	canbittype 	STATUS_C_GSMGenericFailSts: 1;
	canbittype 	unused1: 8;
} _c_STATUS_C_GSM_msgType;

typedef struct {
	canbittype 	unused0: 5;
	canbittype 	STATUS_C_GSMEOL: 1;
	canbittype 	STATUS_C_GSMCurrentFailSts: 1;
	canbittype 	STATUS_C_GSMGenericFailSts: 1;
	canbittype 	unused1: 8;
} _c_STATUS_C_GSM_RDS_msgType;

typedef struct {
	canbittype 	unused0: 5;
	canbittype 	STATUS_C_EPSEOL: 1;
	canbittype 	STATUS_C_EPSCurrentFailSts: 1;
	canbittype 	STATUS_C_EPSGenericFailSts: 1;
	canbittype 	unused1: 8;
} _c_STATUS_C_EPS_msgType;

typedef struct {
	canbittype 	unused0: 5;
	canbittype 	STATUS_C_EPSEOL: 1;
	canbittype 	STATUS_C_EPSCurrentFailSts: 1;
	canbittype 	STATUS_C_EPSGenericFailSts: 1;
	canbittype 	unused1: 8;
} _c_STATUS_C_EPS_RDS_msgType;

typedef struct {
	canbittype 	unused0: 5;
	canbittype 	STATUS_C_BSMEOL: 1;
	canbittype 	STATUS_C_BSMCurrentFailSts: 1;
	canbittype 	STATUS_C_BSMGenericFailSts: 1;
	canbittype 	unused2: 1;
	canbittype 	STATUS_C_BSMDSTFailSts: 1;
	canbittype 	unused1: 6;
	canbittype 	unused3: 1;
	canbittype 	STATUS_C_BSMESCControlSts: 1;
	canbittype 	STATUS_C_BSMHHCustomerDisableSts: 1;
	canbittype 	STATUS_C_BSMVehicleSpeed_1: 5;
	canbittype 	STATUS_C_BSMVehicleSpeed_0: 8;
	canbittype 	STATUS_C_BSMVehicleSpeedFailSts: 1;
	canbittype 	STATUS_C_BSMLHRPulseCounterFailSts: 1;
	canbittype 	STATUS_C_BSMRHRPulseCounterFailSts: 1;
	canbittype 	STATUS_C_BSMLHRPulseCounter_1: 5;
	canbittype 	STATUS_C_BSMLHRPulseCounter_0: 8;
	canbittype 	STATUS_C_BSMStopLightActivationFromNACReq: 1;
	canbittype 	unused4: 2;
	canbittype 	STATUS_C_BSMRHRPulseCounter_1: 5;
	canbittype 	STATUS_C_BSMRHRPulseCounter_0: 8;
} _c_STATUS_C_BSM_msgType;

typedef struct {
	canbittype 	unused0: 5;
	canbittype 	STATUS_C_BSMEOL: 1;
	canbittype 	STATUS_C_BSMCurrentFailSts: 1;
	canbittype 	STATUS_C_BSMGenericFailSts: 1;
	canbittype 	unused2: 1;
	canbittype 	STATUS_C_BSMDSTFailSts: 1;
	canbittype 	unused1: 6;
	canbittype 	unused3: 1;
	canbittype 	STATUS_C_BSMESCControlSts: 1;
	canbittype 	STATUS_C_BSMHHCustomerDisableSts: 1;
	canbittype 	STATUS_C_BSMVehicleSpeed_1: 5;
	canbittype 	STATUS_C_BSMVehicleSpeed_0: 8;
	canbittype 	STATUS_C_BSMVehicleSpeedFailSts: 1;
	canbittype 	STATUS_C_BSMLHRPulseCounterFailSts: 1;
	canbittype 	STATUS_C_BSMRHRPulseCounterFailSts: 1;
	canbittype 	STATUS_C_BSMLHRPulseCounter_1: 5;
	canbittype 	STATUS_C_BSMLHRPulseCounter_0: 8;
	canbittype 	STATUS_C_BSMStopLightActivationFromNACReq: 1;
	canbittype 	unused4: 2;
	canbittype 	STATUS_C_BSMRHRPulseCounter_1: 5;
	canbittype 	STATUS_C_BSMRHRPulseCounter_0: 8;
} _c_STATUS_C_BSM_RDS_msgType;

typedef struct {
	canbittype 	unused0: 8;
	canbittype 	unused1: 8;
	canbittype 	unused2: 8;
	canbittype 	unused3: 8;
	canbittype 	unused4: 8;
	canbittype 	unused6: 4;
	canbittype 	STATUS_C_ECM2StartRelayBCMFeedbackFault: 1;
	canbittype 	STATUS_C_ECM2StartRelayECMFeedbackFault: 1;
	canbittype 	unused5: 2;
	canbittype 	unused8: 1;
	canbittype 	STATUS_C_ECM2SAMInfo: 4;
	canbittype 	unused7: 2;
	canbittype 	STATUS_C_ECM2ColdStartLampIndication_1: 1;
	canbittype 	STATUS_C_ECM2ColdStartLampIndication_0: 1;
	canbittype 	unused9: 7;
} _c_STATUS_C_ECM2_msgType;

typedef struct {
	canbittype 	unused0: 8;
	canbittype 	unused1: 8;
	canbittype 	unused2: 8;
	canbittype 	unused3: 8;
	canbittype 	unused4: 8;
	canbittype 	unused6: 4;
	canbittype 	STATUS_C_ECM2StartRelayBCMFeedbackFault: 1;
	canbittype 	STATUS_C_ECM2StartRelayECMFeedbackFault: 1;
	canbittype 	unused5: 2;
	canbittype 	unused8: 1;
	canbittype 	STATUS_C_ECM2SAMInfo: 4;
	canbittype 	unused7: 2;
	canbittype 	STATUS_C_ECM2ColdStartLampIndication_1: 1;
	canbittype 	STATUS_C_ECM2ColdStartLampIndication_0: 1;
	canbittype 	unused9: 7;
} _c_STATUS_C_ECM2_RDS_msgType;

typedef struct {
	canbittype 	unused0: 5;
	canbittype 	STATUS_C_ECMEOL: 1;
	canbittype 	STATUS_C_ECMCurrentFailSts: 1;
	canbittype 	STATUS_C_ECMGenericFailSts: 1;
	canbittype 	STATUS_C_ECMOilPressureSts: 1;
	canbittype 	unused3: 1;
	canbittype 	STATUS_C_ECMFuelWaterPresentSts: 1;
	canbittype 	STATUS_C_ECMGlowPlugLampSts: 1;
	canbittype 	unused2: 1;
	canbittype 	STATUS_C_ECMFuelWaterPresentFailSts: 1;
	canbittype 	unused1: 1;
	canbittype 	STATUS_C_ECMD_Signal: 1;
	canbittype 	STATUS_C_ECMCompressorSts: 2;
	canbittype 	unused5: 1;
	canbittype 	STATUS_C_ECMEMSFailSts: 2;
	canbittype 	STATUS_C_ECMEngineWaterTempFailSts: 1;
	canbittype 	unused4: 1;
	canbittype 	STATUS_C_ECMEngineWaterTempWarningLightSts: 1;
	canbittype 	STATUS_C_ECMEngineWaterTemp: 8;
	canbittype 	unused6: 8;
	canbittype 	unused7: 8;
	canbittype 	STATUS_C_ECMEngineSpeed: 8;
	canbittype 	STATUS_C_ECMDPFSts: 1;
	canbittype 	unused10: 1;
	canbittype 	STATUS_C_ECMEngineSpeedValidData: 1;
	canbittype 	unused9: 2;
	canbittype 	STATUS_C_ECMReverseGearSts: 1;
	canbittype 	unused8: 2;
} _c_STATUS_C_ECM_msgType;

typedef struct {
	canbittype 	unused0: 5;
	canbittype 	STATUS_C_ECMEOL: 1;
	canbittype 	STATUS_C_ECMCurrentFailSts: 1;
	canbittype 	STATUS_C_ECMGenericFailSts: 1;
	canbittype 	STATUS_C_ECMOilPressureSts: 1;
	canbittype 	unused3: 1;
	canbittype 	STATUS_C_ECMFuelWaterPresentSts: 1;
	canbittype 	STATUS_C_ECMGlowPlugLampSts: 1;
	canbittype 	unused2: 1;
	canbittype 	STATUS_C_ECMFuelWaterPresentFailSts: 1;
	canbittype 	unused1: 1;
	canbittype 	STATUS_C_ECMD_Signal: 1;
	canbittype 	STATUS_C_ECMCompressorSts: 2;
	canbittype 	unused5: 1;
	canbittype 	STATUS_C_ECMEMSFailSts: 2;
	canbittype 	STATUS_C_ECMEngineWaterTempFailSts: 1;
	canbittype 	unused4: 1;
	canbittype 	STATUS_C_ECMEngineWaterTempWarningLightSts: 1;
	canbittype 	STATUS_C_ECMEngineWaterTemp: 8;
	canbittype 	unused6: 8;
	canbittype 	unused7: 8;
	canbittype 	STATUS_C_ECMEngineSpeed: 8;
	canbittype 	STATUS_C_ECMDPFSts: 1;
	canbittype 	unused10: 1;
	canbittype 	STATUS_C_ECMEngineSpeedValidData: 1;
	canbittype 	unused9: 2;
	canbittype 	STATUS_C_ECMReverseGearSts: 1;
	canbittype 	unused8: 2;
} _c_STATUS_C_ECM_RDS_msgType;

typedef struct {
	canbittype 	unused0: 5;
	canbittype 	STATUS_C_ORCEOL: 1;
	canbittype 	STATUS_C_ORCCurrentFailSts: 1;
	canbittype 	STATUS_C_ORCGenericFailSts: 1;
	canbittype 	unused1: 8;
	canbittype 	unused2: 8;
	canbittype 	unused3: 8;
	canbittype 	unused4: 8;
	canbittype 	unused5: 8;
	canbittype 	unused6: 8;
	canbittype 	unused7: 8;
} _c_STATUS_C_ORC_msgType;

typedef struct {
	canbittype 	unused0: 5;
	canbittype 	STATUS_C_ORCEOL: 1;
	canbittype 	STATUS_C_ORCCurrentFailSts: 1;
	canbittype 	STATUS_C_ORCGenericFailSts: 1;
	canbittype 	unused1: 8;
	canbittype 	unused2: 8;
	canbittype 	unused3: 8;
	canbittype 	unused4: 8;
	canbittype 	unused5: 8;
	canbittype 	unused6: 8;
	canbittype 	unused7: 8;
} _c_STATUS_C_ORC_RDS_msgType;

typedef struct {
	canbittype 	RFHUB_A2RFReq: 3;
	canbittype 	RFHUB_A2RFFuncReq: 5;
	canbittype 	RFHUB_A2RFCfgReq: 3;
	canbittype 	unused0: 5;
	canbittype 	RFHUB_A2RFFobNum: 4;
	canbittype 	unused1: 4;
	canbittype 	RFHUB_A2FOBSearchResult: 8;
	canbittype 	unused2: 8;
} _c_RFHUB_A2_msgType;

typedef struct {
	canbittype 	RFHUB_A2RFReq: 3;
	canbittype 	RFHUB_A2RFFuncReq: 5;
	canbittype 	RFHUB_A2RFCfgReq: 3;
	canbittype 	unused0: 5;
	canbittype 	RFHUB_A2RFFobNum: 4;
	canbittype 	unused1: 4;
	canbittype 	RFHUB_A2FOBSearchResult: 8;
	canbittype 	unused2: 8;
} _c_RFHUB_A2_RDS_msgType;

typedef struct {
	canbittype 	unused0: 1;
	canbittype 	RFHUB_A1ValidKeySts: 2;
	canbittype 	RFHUB_A1IgnPosSts: 3;
	canbittype 	RFHUB_A1CustKeyInIgnSts: 2;
} _c_RFHUB_A1_msgType;

typedef struct {
	canbittype 	unused0: 1;
	canbittype 	RFHUB_A1ValidKeySts: 2;
	canbittype 	RFHUB_A1IgnPosSts: 3;
	canbittype 	RFHUB_A1CustKeyInIgnSts: 2;
} _c_RFHUB_A1_RDS_msgType;

typedef struct {
	canbittype 	PAM_1PAM_STATESts: 1;
	canbittype 	unused1: 4;
	canbittype 	PAM_1ASBM_LedControlSts: 2;
	canbittype 	unused0: 1;
	canbittype 	PAM_1PAM_AlrRC: 4;
	canbittype 	PAM_1PAM_AlrRL: 4;
	canbittype 	unused2: 4;
	canbittype 	PAM_1PAM_AlrRR: 4;
	canbittype 	unused4: 2;
	canbittype 	PAM_1PAM_RR_CHIME_RQSts: 1;
	canbittype 	PAM_1PAM_LR_CHIME_RQSts: 1;
	canbittype 	unused3: 2;
	canbittype 	PAM_1PAM_CHIME_TYPESts_1: 2;
	canbittype 	PAM_1PAM_CHIME_TYPESts_0: 1;
	canbittype 	PAM_1RR_PAM_StopControlSts: 1;
	canbittype 	PAM_1PAM_CHIME_REP_RATESts: 4;
	canbittype 	PAM_1PAM_CFG_STATSts: 2;
	canbittype 	PAM_1PAM_VOL_R: 2;
	canbittype 	unused5: 6;
	canbittype 	unused6: 8;
	canbittype 	unused7: 8;
} _c_PAM_1_msgType;

typedef struct {
	canbittype 	PAM_1PAM_STATESts: 1;
	canbittype 	unused1: 4;
	canbittype 	PAM_1ASBM_LedControlSts: 2;
	canbittype 	unused0: 1;
	canbittype 	PAM_1PAM_AlrRC: 4;
	canbittype 	PAM_1PAM_AlrRL: 4;
	canbittype 	unused2: 4;
	canbittype 	PAM_1PAM_AlrRR: 4;
	canbittype 	unused4: 2;
	canbittype 	PAM_1PAM_RR_CHIME_RQSts: 1;
	canbittype 	PAM_1PAM_LR_CHIME_RQSts: 1;
	canbittype 	unused3: 2;
	canbittype 	PAM_1PAM_CHIME_TYPESts_1: 2;
	canbittype 	PAM_1PAM_CHIME_TYPESts_0: 1;
	canbittype 	PAM_1RR_PAM_StopControlSts: 1;
	canbittype 	PAM_1PAM_CHIME_REP_RATESts: 4;
	canbittype 	PAM_1PAM_CFG_STATSts: 2;
	canbittype 	PAM_1PAM_VOL_R: 2;
	canbittype 	unused5: 6;
	canbittype 	unused6: 8;
	canbittype 	unused7: 8;
} _c_PAM_1_RDS_msgType;

typedef struct {
	canbittype 	WAKE_C_RFHMWakeReq: 1;
	canbittype 	WAKE_C_RFHMWakeRsn: 7;
	canbittype 	WAKE_C_RFHMWakeCntr: 8;
	canbittype 	WAKE_C_RFHMWakeUpNodeAddress: 8;
	canbittype 	unused0: 8;
} _c_WAKE_C_RFHM_msgType;

typedef struct {
	canbittype 	WAKE_C_RFHMWakeReq: 1;
	canbittype 	WAKE_C_RFHMWakeRsn: 7;
	canbittype 	WAKE_C_RFHMWakeCntr: 8;
	canbittype 	WAKE_C_RFHMWakeUpNodeAddress: 8;
	canbittype 	unused0: 8;
} _c_WAKE_C_RFHM_RDS_msgType;

typedef struct {
	canbittype 	WAKE_C_SCCMWakeReq: 1;
	canbittype 	WAKE_C_SCCMWakeRsn: 7;
	canbittype 	WAKE_C_SCCMWakeCntr: 8;
	canbittype 	WAKE_C_SCCMWakeUpNodeAddress: 8;
	canbittype 	unused0: 8;
} _c_WAKE_C_SCCM_msgType;

typedef struct {
	canbittype 	WAKE_C_SCCMWakeReq: 1;
	canbittype 	WAKE_C_SCCMWakeRsn: 7;
	canbittype 	WAKE_C_SCCMWakeCntr: 8;
	canbittype 	WAKE_C_SCCMWakeUpNodeAddress: 8;
	canbittype 	unused0: 8;
} _c_WAKE_C_SCCM_RDS_msgType;

typedef struct {
	canbittype 	WAKE_C_ESLWakeReq: 1;
	canbittype 	WAKE_C_ESLWakeRsn: 7;
	canbittype 	WAKE_C_ESLWakeCntr: 8;
	canbittype 	WAKE_C_ESLWakeUpNodeAddress: 8;
	canbittype 	unused0: 8;
} _c_WAKE_C_ESL_msgType;

typedef struct {
	canbittype 	WAKE_C_ESLWakeReq: 1;
	canbittype 	WAKE_C_ESLWakeRsn: 7;
	canbittype 	WAKE_C_ESLWakeCntr: 8;
	canbittype 	WAKE_C_ESLWakeUpNodeAddress: 8;
	canbittype 	unused0: 8;
} _c_WAKE_C_ESL_RDS_msgType;

typedef struct {
	canbittype 	WAKE_C_EPBWakeReq: 1;
	canbittype 	WAKE_C_EPBWakeRsn: 7;
	canbittype 	WAKE_C_EPBWakeCntr: 8;
	canbittype 	WAKE_C_EPBWakeUpNodeAddress: 8;
	canbittype 	unused0: 8;
} _c_WAKE_C_EPB_msgType;

typedef struct {
	canbittype 	WAKE_C_EPBWakeReq: 1;
	canbittype 	WAKE_C_EPBWakeRsn: 7;
	canbittype 	WAKE_C_EPBWakeCntr: 8;
	canbittype 	WAKE_C_EPBWakeUpNodeAddress: 8;
	canbittype 	unused0: 8;
} _c_WAKE_C_EPB_RDS_msgType;

typedef struct {
	canbittype 	WAKE_C_BSMWakeReq: 1;
	canbittype 	WAKE_C_BSMWakeRsn: 7;
	canbittype 	WAKE_C_BSMWakeCntr: 8;
	canbittype 	WAKE_C_BSMWakeUpNodeAddress: 8;
	canbittype 	unused0: 8;
} _c_WAKE_C_BSM_msgType;

typedef struct {
	canbittype 	WAKE_C_BSMWakeReq: 1;
	canbittype 	WAKE_C_BSMWakeRsn: 7;
	canbittype 	WAKE_C_BSMWakeCntr: 8;
	canbittype 	WAKE_C_BSMWakeUpNodeAddress: 8;
	canbittype 	unused0: 8;
} _c_WAKE_C_BSM_RDS_msgType;

typedef struct {
	canbittype 	unused0: 8;
	canbittype 	unused1: 8;
	canbittype 	MOT1GasPedalPosition: 8;
	canbittype 	unused2: 8;
	canbittype 	unused4: 3;
	canbittype 	MOT1GasPedalPositionValidData: 1;
	canbittype 	unused3: 4;
	canbittype 	unused5: 8;
	canbittype 	unused6: 8;
	canbittype 	unused7: 8;
} _c_MOT1_msgType;

typedef struct {
	canbittype 	unused0: 8;
	canbittype 	unused1: 8;
	canbittype 	MOT1GasPedalPosition: 8;
	canbittype 	unused2: 8;
	canbittype 	unused4: 3;
	canbittype 	MOT1GasPedalPositionValidData: 1;
	canbittype 	unused3: 4;
	canbittype 	unused5: 8;
	canbittype 	unused6: 8;
	canbittype 	unused7: 8;
} _c_MOT1_RDS_msgType;

typedef struct {
	canbittype 	GELwsAngle_1: 8;
	canbittype 	GELwsAngle_0: 8;
	canbittype 	unused1: 2;
	canbittype 	GELwsAngleValidData: 1;
	canbittype 	unused0: 5;
	canbittype 	unused2: 8;
	canbittype 	unused4: 1;
	canbittype 	GELWSFailSts: 2;
	canbittype 	unused3: 5;
	canbittype 	unused5: 8;
} _c_GE_msgType;

typedef struct {
	canbittype 	GELwsAngle_1: 8;
	canbittype 	GELwsAngle_0: 8;
	canbittype 	unused1: 2;
	canbittype 	GELwsAngleValidData: 1;
	canbittype 	unused0: 5;
	canbittype 	unused2: 8;
	canbittype 	unused4: 1;
	canbittype 	GELWSFailSts: 2;
	canbittype 	unused3: 5;
	canbittype 	unused5: 8;
} _c_GE_RDS_msgType;

typedef struct {
	canbittype 	TRM_CODE_RESPONSEControlEncoding: 8;
	canbittype 	TRM_CODE_RESPONSEMiniCryptGCode_1: 8;
	canbittype 	TRM_CODE_RESPONSEMiniCryptGCode_0: 8;
	canbittype 	TRM_CODE_RESPONSETxpFound: 1;
	canbittype 	TRM_CODE_RESPONSETxpID: 4;
	canbittype 	TRM_CODE_RESPONSEProgrammedSts: 1;
	canbittype 	TRM_CODE_RESPONSECodeCheckSts: 2;
	canbittype 	unused0: 8;
	canbittype 	unused1: 8;
} _c_TRM_CODE_RESPONSE_msgType;

typedef struct {
	canbittype 	TRM_CODE_RESPONSEControlEncoding: 8;
	canbittype 	TRM_CODE_RESPONSEMiniCryptGCode_1: 8;
	canbittype 	TRM_CODE_RESPONSEMiniCryptGCode_0: 8;
	canbittype 	TRM_CODE_RESPONSETxpFound: 1;
	canbittype 	TRM_CODE_RESPONSETxpID: 4;
	canbittype 	TRM_CODE_RESPONSEProgrammedSts: 1;
	canbittype 	TRM_CODE_RESPONSECodeCheckSts: 2;
	canbittype 	unused0: 8;
	canbittype 	unused1: 8;
} _c_TRM_CODE_RESPONSE_RDS_msgType;

typedef struct {
	canbittype 	ORC_A2IMPACTConfirm: 1;
	canbittype 	unused0: 7;
	canbittype 	ORC_A2IMPACTCommand: 1;
	canbittype 	unused1: 7;
} _c_ORC_A2_msgType;

typedef struct {
	canbittype 	ORC_A2IMPACTConfirm: 1;
	canbittype 	unused0: 7;
	canbittype 	ORC_A2IMPACTCommand: 1;
	canbittype 	unused1: 7;
} _c_ORC_A2_RDS_msgType;

typedef struct {
	canbittype 	CRO_BCMData_0: 8;
	canbittype 	CRO_BCMData_1: 8;
	canbittype 	CRO_BCMData_2: 8;
	canbittype 	CRO_BCMData_3: 8;
	canbittype 	CRO_BCMData_4: 8;
	canbittype 	CRO_BCMData_5: 8;
	canbittype 	CRO_BCMData_6: 8;
	canbittype 	CRO_BCMData_7: 8;
} _c_CRO_BCM_msgType;

typedef struct {
	canbittype 	CRO_BCMData_0: 8;
	canbittype 	CRO_BCMData_1: 8;
	canbittype 	CRO_BCMData_2: 8;
	canbittype 	CRO_BCMData_3: 8;
	canbittype 	CRO_BCMData_4: 8;
	canbittype 	CRO_BCMData_5: 8;
	canbittype 	CRO_BCMData_6: 8;
	canbittype 	CRO_BCMData_7: 8;
} _c_CRO_BCM_RDS_msgType;

typedef struct {
	canbittype 	CFG_DATA_CODE_RSP_RBSSDigit_11: 4;
	canbittype 	CFG_DATA_CODE_RSP_RBSSDigit_10: 4;
	canbittype 	CFG_DATA_CODE_RSP_RBSSDigit_09: 4;
	canbittype 	CFG_DATA_CODE_RSP_RBSSDigit_08: 4;
	canbittype 	CFG_DATA_CODE_RSP_RBSSDigit_07: 4;
	canbittype 	CFG_DATA_CODE_RSP_RBSSDigit_06: 4;
	canbittype 	CFG_DATA_CODE_RSP_RBSSDigit_05: 4;
	canbittype 	CFG_DATA_CODE_RSP_RBSSDigit_04: 4;
	canbittype 	CFG_DATA_CODE_RSP_RBSSDigit_03: 4;
	canbittype 	CFG_DATA_CODE_RSP_RBSSDigit_02: 4;
	canbittype 	CFG_DATA_CODE_RSP_RBSSDigit_01: 4;
	canbittype 	unused0: 4;
} _c_CFG_DATA_CODE_RSP_RBSS_msgType;

typedef struct {
	canbittype 	CFG_DATA_CODE_RSP_RBSSDigit_11: 4;
	canbittype 	CFG_DATA_CODE_RSP_RBSSDigit_10: 4;
	canbittype 	CFG_DATA_CODE_RSP_RBSSDigit_09: 4;
	canbittype 	CFG_DATA_CODE_RSP_RBSSDigit_08: 4;
	canbittype 	CFG_DATA_CODE_RSP_RBSSDigit_07: 4;
	canbittype 	CFG_DATA_CODE_RSP_RBSSDigit_06: 4;
	canbittype 	CFG_DATA_CODE_RSP_RBSSDigit_05: 4;
	canbittype 	CFG_DATA_CODE_RSP_RBSSDigit_04: 4;
	canbittype 	CFG_DATA_CODE_RSP_RBSSDigit_03: 4;
	canbittype 	CFG_DATA_CODE_RSP_RBSSDigit_02: 4;
	canbittype 	CFG_DATA_CODE_RSP_RBSSDigit_01: 4;
	canbittype 	unused0: 4;
} _c_CFG_DATA_CODE_RSP_RBSS_RDS_msgType;

typedef struct {
	canbittype 	CFG_DATA_CODE_RSP_LBSSDigit_11: 4;
	canbittype 	CFG_DATA_CODE_RSP_LBSSDigit_10: 4;
	canbittype 	CFG_DATA_CODE_RSP_LBSSDigit_09: 4;
	canbittype 	CFG_DATA_CODE_RSP_LBSSDigit_08: 4;
	canbittype 	CFG_DATA_CODE_RSP_LBSSDigit_07: 4;
	canbittype 	CFG_DATA_CODE_RSP_LBSSDigit_06: 4;
	canbittype 	CFG_DATA_CODE_RSP_LBSSDigit_05: 4;
	canbittype 	CFG_DATA_CODE_RSP_LBSSDigit_04: 4;
	canbittype 	CFG_DATA_CODE_RSP_LBSSDigit_03: 4;
	canbittype 	CFG_DATA_CODE_RSP_LBSSDigit_02: 4;
	canbittype 	CFG_DATA_CODE_RSP_LBSSDigit_01: 4;
	canbittype 	unused0: 4;
} _c_CFG_DATA_CODE_RSP_LBSS_msgType;

typedef struct {
	canbittype 	CFG_DATA_CODE_RSP_LBSSDigit_11: 4;
	canbittype 	CFG_DATA_CODE_RSP_LBSSDigit_10: 4;
	canbittype 	CFG_DATA_CODE_RSP_LBSSDigit_09: 4;
	canbittype 	CFG_DATA_CODE_RSP_LBSSDigit_08: 4;
	canbittype 	CFG_DATA_CODE_RSP_LBSSDigit_07: 4;
	canbittype 	CFG_DATA_CODE_RSP_LBSSDigit_06: 4;
	canbittype 	CFG_DATA_CODE_RSP_LBSSDigit_05: 4;
	canbittype 	CFG_DATA_CODE_RSP_LBSSDigit_04: 4;
	canbittype 	CFG_DATA_CODE_RSP_LBSSDigit_03: 4;
	canbittype 	CFG_DATA_CODE_RSP_LBSSDigit_02: 4;
	canbittype 	CFG_DATA_CODE_RSP_LBSSDigit_01: 4;
	canbittype 	unused0: 4;
} _c_CFG_DATA_CODE_RSP_LBSS_RDS_msgType;

typedef struct {
	canbittype 	CFG_DATA_CODE_RSP_ICSDigit_11: 4;
	canbittype 	CFG_DATA_CODE_RSP_ICSDigit_10: 4;
	canbittype 	CFG_DATA_CODE_RSP_ICSDigit_09: 4;
	canbittype 	CFG_DATA_CODE_RSP_ICSDigit_08: 4;
	canbittype 	CFG_DATA_CODE_RSP_ICSDigit_07: 4;
	canbittype 	CFG_DATA_CODE_RSP_ICSDigit_06: 4;
	canbittype 	CFG_DATA_CODE_RSP_ICSDigit_05: 4;
	canbittype 	CFG_DATA_CODE_RSP_ICSDigit_04: 4;
	canbittype 	CFG_DATA_CODE_RSP_ICSDigit_03: 4;
	canbittype 	CFG_DATA_CODE_RSP_ICSDigit_02: 4;
	canbittype 	CFG_DATA_CODE_RSP_ICSDigit_01: 4;
	canbittype 	unused0: 4;
} _c_CFG_DATA_CODE_RSP_ICS_msgType;

typedef struct {
	canbittype 	CFG_DATA_CODE_RSP_ICSDigit_11: 4;
	canbittype 	CFG_DATA_CODE_RSP_ICSDigit_10: 4;
	canbittype 	CFG_DATA_CODE_RSP_ICSDigit_09: 4;
	canbittype 	CFG_DATA_CODE_RSP_ICSDigit_08: 4;
	canbittype 	CFG_DATA_CODE_RSP_ICSDigit_07: 4;
	canbittype 	CFG_DATA_CODE_RSP_ICSDigit_06: 4;
	canbittype 	CFG_DATA_CODE_RSP_ICSDigit_05: 4;
	canbittype 	CFG_DATA_CODE_RSP_ICSDigit_04: 4;
	canbittype 	CFG_DATA_CODE_RSP_ICSDigit_03: 4;
	canbittype 	CFG_DATA_CODE_RSP_ICSDigit_02: 4;
	canbittype 	CFG_DATA_CODE_RSP_ICSDigit_01: 4;
	canbittype 	unused0: 4;
} _c_CFG_DATA_CODE_RSP_ICS_RDS_msgType;

typedef struct {
	canbittype 	CFG_DATA_CODE_RSP_CSWMDigit_11: 4;
	canbittype 	CFG_DATA_CODE_RSP_CSWMDigit_10: 4;
	canbittype 	CFG_DATA_CODE_RSP_CSWMDigit_09: 4;
	canbittype 	CFG_DATA_CODE_RSP_CSWMDigit_08: 4;
	canbittype 	CFG_DATA_CODE_RSP_CSWMDigit_07: 4;
	canbittype 	CFG_DATA_CODE_RSP_CSWMDigit_06: 4;
	canbittype 	CFG_DATA_CODE_RSP_CSWMDigit_05: 4;
	canbittype 	CFG_DATA_CODE_RSP_CSWMDigit_04: 4;
	canbittype 	CFG_DATA_CODE_RSP_CSWMDigit_03: 4;
	canbittype 	CFG_DATA_CODE_RSP_CSWMDigit_02: 4;
	canbittype 	CFG_DATA_CODE_RSP_CSWMDigit_01: 4;
	canbittype 	unused0: 4;
} _c_CFG_DATA_CODE_RSP_CSWM_msgType;

typedef struct {
	canbittype 	CFG_DATA_CODE_RSP_CSWMDigit_11: 4;
	canbittype 	CFG_DATA_CODE_RSP_CSWMDigit_10: 4;
	canbittype 	CFG_DATA_CODE_RSP_CSWMDigit_09: 4;
	canbittype 	CFG_DATA_CODE_RSP_CSWMDigit_08: 4;
	canbittype 	CFG_DATA_CODE_RSP_CSWMDigit_07: 4;
	canbittype 	CFG_DATA_CODE_RSP_CSWMDigit_06: 4;
	canbittype 	CFG_DATA_CODE_RSP_CSWMDigit_05: 4;
	canbittype 	CFG_DATA_CODE_RSP_CSWMDigit_04: 4;
	canbittype 	CFG_DATA_CODE_RSP_CSWMDigit_03: 4;
	canbittype 	CFG_DATA_CODE_RSP_CSWMDigit_02: 4;
	canbittype 	CFG_DATA_CODE_RSP_CSWMDigit_01: 4;
	canbittype 	unused0: 4;
} _c_CFG_DATA_CODE_RSP_CSWM_RDS_msgType;

typedef struct {
	canbittype 	CFG_DATA_CODE_RSP_CDMDigit_11: 4;
	canbittype 	CFG_DATA_CODE_RSP_CDMDigit_10: 4;
	canbittype 	CFG_DATA_CODE_RSP_CDMDigit_09: 4;
	canbittype 	CFG_DATA_CODE_RSP_CDMDigit_08: 4;
	canbittype 	CFG_DATA_CODE_RSP_CDMDigit_07: 4;
	canbittype 	CFG_DATA_CODE_RSP_CDMDigit_06: 4;
	canbittype 	CFG_DATA_CODE_RSP_CDMDigit_05: 4;
	canbittype 	CFG_DATA_CODE_RSP_CDMDigit_04: 4;
	canbittype 	CFG_DATA_CODE_RSP_CDMDigit_03: 4;
	canbittype 	CFG_DATA_CODE_RSP_CDMDigit_02: 4;
	canbittype 	CFG_DATA_CODE_RSP_CDMDigit_01: 4;
	canbittype 	unused0: 4;
} _c_CFG_DATA_CODE_RSP_CDM_msgType;

typedef struct {
	canbittype 	CFG_DATA_CODE_RSP_CDMDigit_11: 4;
	canbittype 	CFG_DATA_CODE_RSP_CDMDigit_10: 4;
	canbittype 	CFG_DATA_CODE_RSP_CDMDigit_09: 4;
	canbittype 	CFG_DATA_CODE_RSP_CDMDigit_08: 4;
	canbittype 	CFG_DATA_CODE_RSP_CDMDigit_07: 4;
	canbittype 	CFG_DATA_CODE_RSP_CDMDigit_06: 4;
	canbittype 	CFG_DATA_CODE_RSP_CDMDigit_05: 4;
	canbittype 	CFG_DATA_CODE_RSP_CDMDigit_04: 4;
	canbittype 	CFG_DATA_CODE_RSP_CDMDigit_03: 4;
	canbittype 	CFG_DATA_CODE_RSP_CDMDigit_02: 4;
	canbittype 	CFG_DATA_CODE_RSP_CDMDigit_01: 4;
	canbittype 	unused0: 4;
} _c_CFG_DATA_CODE_RSP_CDM_RDS_msgType;

typedef struct {
	canbittype 	CFG_DATA_CODE_RSP_AMPDigit_11: 4;
	canbittype 	CFG_DATA_CODE_RSP_AMPDigit_10: 4;
	canbittype 	CFG_DATA_CODE_RSP_AMPDigit_09: 4;
	canbittype 	CFG_DATA_CODE_RSP_AMPDigit_08: 4;
	canbittype 	CFG_DATA_CODE_RSP_AMPDigit_07: 4;
	canbittype 	CFG_DATA_CODE_RSP_AMPDigit_06: 4;
	canbittype 	CFG_DATA_CODE_RSP_AMPDigit_05: 4;
	canbittype 	CFG_DATA_CODE_RSP_AMPDigit_04: 4;
	canbittype 	CFG_DATA_CODE_RSP_AMPDigit_03: 4;
	canbittype 	CFG_DATA_CODE_RSP_AMPDigit_02: 4;
	canbittype 	CFG_DATA_CODE_RSP_AMPDigit_01: 4;
	canbittype 	unused0: 4;
} _c_CFG_DATA_CODE_RSP_AMP_msgType;

typedef struct {
	canbittype 	CFG_DATA_CODE_RSP_AMPDigit_11: 4;
	canbittype 	CFG_DATA_CODE_RSP_AMPDigit_10: 4;
	canbittype 	CFG_DATA_CODE_RSP_AMPDigit_09: 4;
	canbittype 	CFG_DATA_CODE_RSP_AMPDigit_08: 4;
	canbittype 	CFG_DATA_CODE_RSP_AMPDigit_07: 4;
	canbittype 	CFG_DATA_CODE_RSP_AMPDigit_06: 4;
	canbittype 	CFG_DATA_CODE_RSP_AMPDigit_05: 4;
	canbittype 	CFG_DATA_CODE_RSP_AMPDigit_04: 4;
	canbittype 	CFG_DATA_CODE_RSP_AMPDigit_03: 4;
	canbittype 	CFG_DATA_CODE_RSP_AMPDigit_02: 4;
	canbittype 	CFG_DATA_CODE_RSP_AMPDigit_01: 4;
	canbittype 	unused0: 4;
} _c_CFG_DATA_CODE_RSP_AMP_RDS_msgType;

typedef struct {
	canbittype 	CFG_DATA_CODE_RSP_ETMDigit11: 4;
	canbittype 	CFG_DATA_CODE_RSP_ETMDigit10: 4;
	canbittype 	CFG_DATA_CODE_RSP_ETMDigit09: 4;
	canbittype 	CFG_DATA_CODE_RSP_ETMDigit08: 4;
	canbittype 	CFG_DATA_CODE_RSP_ETMDigit07: 4;
	canbittype 	CFG_DATA_CODE_RSP_ETMDigit06: 4;
	canbittype 	CFG_DATA_CODE_RSP_ETMDigit05: 4;
	canbittype 	CFG_DATA_CODE_RSP_ETMDigit04: 4;
	canbittype 	CFG_DATA_CODE_RSP_ETMDigit03: 4;
	canbittype 	CFG_DATA_CODE_RSP_ETMDigit02: 4;
	canbittype 	CFG_DATA_CODE_RSP_ETMDigit01: 4;
	canbittype 	unused0: 4;
} _c_CFG_DATA_CODE_RSP_ETM_msgType;

typedef struct {
	canbittype 	CFG_DATA_CODE_RSP_ETMDigit11: 4;
	canbittype 	CFG_DATA_CODE_RSP_ETMDigit10: 4;
	canbittype 	CFG_DATA_CODE_RSP_ETMDigit09: 4;
	canbittype 	CFG_DATA_CODE_RSP_ETMDigit08: 4;
	canbittype 	CFG_DATA_CODE_RSP_ETMDigit07: 4;
	canbittype 	CFG_DATA_CODE_RSP_ETMDigit06: 4;
	canbittype 	CFG_DATA_CODE_RSP_ETMDigit05: 4;
	canbittype 	CFG_DATA_CODE_RSP_ETMDigit04: 4;
	canbittype 	CFG_DATA_CODE_RSP_ETMDigit03: 4;
	canbittype 	CFG_DATA_CODE_RSP_ETMDigit02: 4;
	canbittype 	CFG_DATA_CODE_RSP_ETMDigit01: 4;
	canbittype 	unused0: 4;
} _c_CFG_DATA_CODE_RSP_ETM_RDS_msgType;

typedef struct {
	canbittype 	CFG_DATA_CODE_RSP_DSMDigit_11: 4;
	canbittype 	CFG_DATA_CODE_RSP_DSMDigit_10: 4;
	canbittype 	CFG_DATA_CODE_RSP_DSMDigit_09: 4;
	canbittype 	CFG_DATA_CODE_RSP_DSMDigit_08: 4;
	canbittype 	CFG_DATA_CODE_RSP_DSMDigit_07: 4;
	canbittype 	CFG_DATA_CODE_RSP_DSMDigit_06: 4;
	canbittype 	CFG_DATA_CODE_RSP_DSMDigit_05: 4;
	canbittype 	CFG_DATA_CODE_RSP_DSMDigit_04: 4;
	canbittype 	CFG_DATA_CODE_RSP_DSMDigit_03: 4;
	canbittype 	CFG_DATA_CODE_RSP_DSMDigit_02: 4;
	canbittype 	CFG_DATA_CODE_RSP_DSMDigit_01: 4;
	canbittype 	unused0: 4;
} _c_CFG_DATA_CODE_RSP_DSM_msgType;

typedef struct {
	canbittype 	CFG_DATA_CODE_RSP_DSMDigit_11: 4;
	canbittype 	CFG_DATA_CODE_RSP_DSMDigit_10: 4;
	canbittype 	CFG_DATA_CODE_RSP_DSMDigit_09: 4;
	canbittype 	CFG_DATA_CODE_RSP_DSMDigit_08: 4;
	canbittype 	CFG_DATA_CODE_RSP_DSMDigit_07: 4;
	canbittype 	CFG_DATA_CODE_RSP_DSMDigit_06: 4;
	canbittype 	CFG_DATA_CODE_RSP_DSMDigit_05: 4;
	canbittype 	CFG_DATA_CODE_RSP_DSMDigit_04: 4;
	canbittype 	CFG_DATA_CODE_RSP_DSMDigit_03: 4;
	canbittype 	CFG_DATA_CODE_RSP_DSMDigit_02: 4;
	canbittype 	CFG_DATA_CODE_RSP_DSMDigit_01: 4;
	canbittype 	unused0: 4;
} _c_CFG_DATA_CODE_RSP_DSM_RDS_msgType;

typedef struct {
	canbittype 	CFG_DATA_CODE_RSP_PDMDigit_11: 4;
	canbittype 	CFG_DATA_CODE_RSP_PDMDigit_10: 4;
	canbittype 	CFG_DATA_CODE_RSP_PDMDigit_09: 4;
	canbittype 	CFG_DATA_CODE_RSP_PDMDigit_08: 4;
	canbittype 	CFG_DATA_CODE_RSP_PDMDigit_07: 4;
	canbittype 	CFG_DATA_CODE_RSP_PDMDigit_06: 4;
	canbittype 	CFG_DATA_CODE_RSP_PDMDigit_05: 4;
	canbittype 	CFG_DATA_CODE_RSP_PDMDigit_04: 4;
	canbittype 	CFG_DATA_CODE_RSP_PDMDigit_03: 4;
	canbittype 	CFG_DATA_CODE_RSP_PDMDigit_02: 4;
	canbittype 	CFG_DATA_CODE_RSP_PDMDigit_01: 4;
	canbittype 	unused0: 4;
} _c_CFG_DATA_CODE_RSP_PDM_msgType;

typedef struct {
	canbittype 	CFG_DATA_CODE_RSP_PDMDigit_11: 4;
	canbittype 	CFG_DATA_CODE_RSP_PDMDigit_10: 4;
	canbittype 	CFG_DATA_CODE_RSP_PDMDigit_09: 4;
	canbittype 	CFG_DATA_CODE_RSP_PDMDigit_08: 4;
	canbittype 	CFG_DATA_CODE_RSP_PDMDigit_07: 4;
	canbittype 	CFG_DATA_CODE_RSP_PDMDigit_06: 4;
	canbittype 	CFG_DATA_CODE_RSP_PDMDigit_05: 4;
	canbittype 	CFG_DATA_CODE_RSP_PDMDigit_04: 4;
	canbittype 	CFG_DATA_CODE_RSP_PDMDigit_03: 4;
	canbittype 	CFG_DATA_CODE_RSP_PDMDigit_02: 4;
	canbittype 	CFG_DATA_CODE_RSP_PDMDigit_01: 4;
	canbittype 	unused0: 4;
} _c_CFG_DATA_CODE_RSP_PDM_RDS_msgType;

typedef struct {
	canbittype 	CFG_DATA_CODE_RSP_ECCDigit_11: 4;
	canbittype 	CFG_DATA_CODE_RSP_ECCDigit_10: 4;
	canbittype 	CFG_DATA_CODE_RSP_ECCDigit_09: 4;
	canbittype 	CFG_DATA_CODE_RSP_ECCDigit_08: 4;
	canbittype 	CFG_DATA_CODE_RSP_ECCDigit_07: 4;
	canbittype 	CFG_DATA_CODE_RSP_ECCDigit_06: 4;
	canbittype 	CFG_DATA_CODE_RSP_ECCDigit_05: 4;
	canbittype 	CFG_DATA_CODE_RSP_ECCDigit_04: 4;
	canbittype 	CFG_DATA_CODE_RSP_ECCDigit_03: 4;
	canbittype 	CFG_DATA_CODE_RSP_ECCDigit_02: 4;
	canbittype 	CFG_DATA_CODE_RSP_ECCDigit_01: 4;
	canbittype 	unused0: 4;
} _c_CFG_DATA_CODE_RSP_ECC_msgType;

typedef struct {
	canbittype 	CFG_DATA_CODE_RSP_ECCDigit_11: 4;
	canbittype 	CFG_DATA_CODE_RSP_ECCDigit_10: 4;
	canbittype 	CFG_DATA_CODE_RSP_ECCDigit_09: 4;
	canbittype 	CFG_DATA_CODE_RSP_ECCDigit_08: 4;
	canbittype 	CFG_DATA_CODE_RSP_ECCDigit_07: 4;
	canbittype 	CFG_DATA_CODE_RSP_ECCDigit_06: 4;
	canbittype 	CFG_DATA_CODE_RSP_ECCDigit_05: 4;
	canbittype 	CFG_DATA_CODE_RSP_ECCDigit_04: 4;
	canbittype 	CFG_DATA_CODE_RSP_ECCDigit_03: 4;
	canbittype 	CFG_DATA_CODE_RSP_ECCDigit_02: 4;
	canbittype 	CFG_DATA_CODE_RSP_ECCDigit_01: 4;
	canbittype 	unused0: 4;
} _c_CFG_DATA_CODE_RSP_ECC_RDS_msgType;

typedef struct {
	canbittype 	CFG_DATA_CODE_RSP_DDMDigit_11: 4;
	canbittype 	CFG_DATA_CODE_RSP_DDMDigit_10: 4;
	canbittype 	CFG_DATA_CODE_RSP_DDMDigit_09: 4;
	canbittype 	CFG_DATA_CODE_RSP_DDMDigit_08: 4;
	canbittype 	CFG_DATA_CODE_RSP_DDMDigit_07: 4;
	canbittype 	CFG_DATA_CODE_RSP_DDMDigit_06: 4;
	canbittype 	CFG_DATA_CODE_RSP_DDMDigit_05: 4;
	canbittype 	CFG_DATA_CODE_RSP_DDMDigit_04: 4;
	canbittype 	CFG_DATA_CODE_RSP_DDMDigit_03: 4;
	canbittype 	CFG_DATA_CODE_RSP_DDMDigit_02: 4;
	canbittype 	CFG_DATA_CODE_RSP_DDMDigit_01: 4;
	canbittype 	unused0: 4;
} _c_CFG_DATA_CODE_RSP_DDM_msgType;

typedef struct {
	canbittype 	CFG_DATA_CODE_RSP_DDMDigit_11: 4;
	canbittype 	CFG_DATA_CODE_RSP_DDMDigit_10: 4;
	canbittype 	CFG_DATA_CODE_RSP_DDMDigit_09: 4;
	canbittype 	CFG_DATA_CODE_RSP_DDMDigit_08: 4;
	canbittype 	CFG_DATA_CODE_RSP_DDMDigit_07: 4;
	canbittype 	CFG_DATA_CODE_RSP_DDMDigit_06: 4;
	canbittype 	CFG_DATA_CODE_RSP_DDMDigit_05: 4;
	canbittype 	CFG_DATA_CODE_RSP_DDMDigit_04: 4;
	canbittype 	CFG_DATA_CODE_RSP_DDMDigit_03: 4;
	canbittype 	CFG_DATA_CODE_RSP_DDMDigit_02: 4;
	canbittype 	CFG_DATA_CODE_RSP_DDMDigit_01: 4;
	canbittype 	unused0: 4;
} _c_CFG_DATA_CODE_RSP_DDM_RDS_msgType;

typedef struct {
	canbittype 	CFG_DATA_CODE_RSP_IPCDigit_11: 4;
	canbittype 	CFG_DATA_CODE_RSP_IPCDigit_10: 4;
	canbittype 	CFG_DATA_CODE_RSP_IPCDigit_09: 4;
	canbittype 	CFG_DATA_CODE_RSP_IPCDigit_08: 4;
	canbittype 	CFG_DATA_CODE_RSP_IPCDigit_07: 4;
	canbittype 	CFG_DATA_CODE_RSP_IPCDigit_06: 4;
	canbittype 	CFG_DATA_CODE_RSP_IPCDigit_05: 4;
	canbittype 	CFG_DATA_CODE_RSP_IPCDigit_04: 4;
	canbittype 	CFG_DATA_CODE_RSP_IPCDigit_03: 4;
	canbittype 	CFG_DATA_CODE_RSP_IPCDigit_02: 4;
	canbittype 	CFG_DATA_CODE_RSP_IPCDigit_01: 4;
	canbittype 	unused0: 4;
} _c_CFG_DATA_CODE_RSP_IPC_msgType;

typedef struct {
	canbittype 	CFG_DATA_CODE_RSP_IPCDigit_11: 4;
	canbittype 	CFG_DATA_CODE_RSP_IPCDigit_10: 4;
	canbittype 	CFG_DATA_CODE_RSP_IPCDigit_09: 4;
	canbittype 	CFG_DATA_CODE_RSP_IPCDigit_08: 4;
	canbittype 	CFG_DATA_CODE_RSP_IPCDigit_07: 4;
	canbittype 	CFG_DATA_CODE_RSP_IPCDigit_06: 4;
	canbittype 	CFG_DATA_CODE_RSP_IPCDigit_05: 4;
	canbittype 	CFG_DATA_CODE_RSP_IPCDigit_04: 4;
	canbittype 	CFG_DATA_CODE_RSP_IPCDigit_03: 4;
	canbittype 	CFG_DATA_CODE_RSP_IPCDigit_02: 4;
	canbittype 	CFG_DATA_CODE_RSP_IPCDigit_01: 4;
	canbittype 	unused0: 4;
} _c_CFG_DATA_CODE_RSP_IPC_RDS_msgType;

typedef struct {
	canbittype 	unused0: 6;
	canbittype 	STATUS_ICSPowerModeSts: 2;
	canbittype 	unused1: 8;
} _c_STATUS_ICS_msgType;

typedef struct {
	canbittype 	unused0: 6;
	canbittype 	STATUS_ICSPowerModeSts: 2;
	canbittype 	unused1: 8;
} _c_STATUS_ICS_RDS_msgType;

typedef struct {
	canbittype 	unused0: 6;
	canbittype 	STATUS_CDMPowerModeSts: 2;
	canbittype 	unused1: 8;
} _c_STATUS_CDM_msgType;

typedef struct {
	canbittype 	unused0: 6;
	canbittype 	STATUS_CDMPowerModeSts: 2;
	canbittype 	unused1: 8;
} _c_STATUS_CDM_RDS_msgType;

typedef struct {
	canbittype 	unused0: 6;
	canbittype 	STATUS_AMPPowerModeSts: 2;
	canbittype 	unused1: 8;
} _c_STATUS_AMP_msgType;

typedef struct {
	canbittype 	unused0: 6;
	canbittype 	STATUS_AMPPowerModeSts: 2;
	canbittype 	unused1: 8;
} _c_STATUS_AMP_RDS_msgType;

typedef struct {
	canbittype 	ENVIRONMENTAL_CONDITIONSExternalTemperature: 8;
	canbittype 	ENVIRONMENTAL_CONDITIONSExternalTemperatureFailSts: 1;
	canbittype 	unused0: 7;
	canbittype 	unused1: 8;
	canbittype 	unused2: 8;
	canbittype 	unused3: 8;
	canbittype 	unused4: 8;
	canbittype 	unused5: 8;
	canbittype 	unused6: 8;
} _c_ENVIRONMENTAL_CONDITIONS_RDS_msgType;

typedef struct {
	canbittype 	unused0: 8;
	canbittype 	unused1: 8;
	canbittype 	unused2: 8;
	canbittype 	FT_HVAC_STATFT_HVAC_BLW_FN_SPSts: 7;
	canbittype 	unused3: 1;
	canbittype 	FT_HVAC_STATFT_HVAC_MD_STATSts: 4;
	canbittype 	unused4: 4;
	canbittype 	unused5: 8;
} _c_FT_HVAC_STAT_msgType;

typedef struct {
	canbittype 	unused0: 8;
	canbittype 	unused1: 8;
	canbittype 	unused2: 8;
	canbittype 	FT_HVAC_STATFT_HVAC_BLW_FN_SPSts: 7;
	canbittype 	unused3: 1;
	canbittype 	FT_HVAC_STATFT_HVAC_MD_STATSts: 4;
	canbittype 	unused4: 4;
	canbittype 	unused5: 8;
} _c_FT_HVAC_STAT_RDS_msgType;

typedef struct {
	canbittype 	unused0: 7;
	canbittype 	DCM_P_MSGCLS_UnlkSw_Psd_P: 1;
	canbittype 	DCM_P_MSGCLS_LKSw_Psd_P: 1;
	canbittype 	DCM_P_MSGWinPos_P_Sts: 7;
} _c_DCM_P_MSG_msgType;

typedef struct {
	canbittype 	unused0: 7;
	canbittype 	DCM_P_MSGCLS_UnlkSw_Psd_P: 1;
	canbittype 	DCM_P_MSGCLS_LKSw_Psd_P: 1;
	canbittype 	DCM_P_MSGWinPos_P_Sts: 7;
} _c_DCM_P_MSG_RDS_msgType;

typedef struct {
	canbittype 	unused0: 6;
	canbittype 	DCM_D_MSGWinPos_D_Sts_1: 2;
	canbittype 	DCM_D_MSGWinPos_D_Sts_0: 5;
	canbittype 	DCM_D_MSGCLS_UnlkSw_Psd_D: 1;
	canbittype 	DCM_D_MSGCLS_LKSw_Psd_D: 1;
	canbittype 	unused1: 1;
} _c_DCM_D_MSG_msgType;

typedef struct {
	canbittype 	unused0: 6;
	canbittype 	DCM_D_MSGWinPos_D_Sts_1: 2;
	canbittype 	DCM_D_MSGWinPos_D_Sts_0: 5;
	canbittype 	DCM_D_MSGCLS_UnlkSw_Psd_D: 1;
	canbittype 	DCM_D_MSGCLS_LKSw_Psd_D: 1;
	canbittype 	unused1: 1;
} _c_DCM_D_MSG_RDS_msgType;

typedef struct {
	canbittype 	NWM_PLGMZero_byte: 8;
	canbittype 	unused0: 2;
	canbittype 	NWM_PLGMCurrentFailSts: 1;
	canbittype 	NWM_PLGMGenericFailSts: 1;
	canbittype 	NWM_PLGMEOL: 1;
	canbittype 	NWM_PLGMActiveLoadSlave: 1;
	canbittype 	NWM_PLGMSystemStatus: 2;
} _c_NWM_PLGM_msgType;

typedef struct {
	canbittype 	NWM_PLGMZero_byte: 8;
	canbittype 	unused0: 2;
	canbittype 	NWM_PLGMCurrentFailSts: 1;
	canbittype 	NWM_PLGMGenericFailSts: 1;
	canbittype 	NWM_PLGMEOL: 1;
	canbittype 	NWM_PLGMActiveLoadSlave: 1;
	canbittype 	NWM_PLGMSystemStatus: 2;
} _c_NWM_PLGM_RDS_msgType;

typedef struct {
	canbittype 	NWM_ETMZero_byte: 8;
	canbittype 	unused0: 2;
	canbittype 	NWM_ETMCurrentFailSts: 1;
	canbittype 	NWM_ETMGenericFailSts: 1;
	canbittype 	NWM_ETMEOL: 1;
	canbittype 	NWM_ETMActiveLoadSlave: 1;
	canbittype 	NWM_ETMSystemStatus: 2;
} _c_NWM_ETM_msgType;

typedef struct {
	canbittype 	NWM_ETMZero_byte: 8;
	canbittype 	unused0: 2;
	canbittype 	NWM_ETMCurrentFailSts: 1;
	canbittype 	NWM_ETMGenericFailSts: 1;
	canbittype 	NWM_ETMEOL: 1;
	canbittype 	NWM_ETMActiveLoadSlave: 1;
	canbittype 	NWM_ETMSystemStatus: 2;
} _c_NWM_ETM_RDS_msgType;

typedef struct {
	canbittype 	NWM_DSMZero_byte: 8;
	canbittype 	unused0: 2;
	canbittype 	NWM_DSMCurrentFailSts: 1;
	canbittype 	NWM_DSMGenericFailSts: 1;
	canbittype 	NWM_DSMEOL: 1;
	canbittype 	NWM_DSMActiveLoadSlave: 1;
	canbittype 	NWM_DSMSystemStatus: 2;
} _c_NWM_DSM_msgType;

typedef struct {
	canbittype 	NWM_DSMZero_byte: 8;
	canbittype 	unused0: 2;
	canbittype 	NWM_DSMCurrentFailSts: 1;
	canbittype 	NWM_DSMGenericFailSts: 1;
	canbittype 	NWM_DSMEOL: 1;
	canbittype 	NWM_DSMActiveLoadSlave: 1;
	canbittype 	NWM_DSMSystemStatus: 2;
} _c_NWM_DSM_RDS_msgType;

typedef struct {
	canbittype 	NWM_PDMZero_byte: 8;
	canbittype 	unused0: 2;
	canbittype 	NWM_PDMCurrentFailSts: 1;
	canbittype 	NWM_PDMGenericFailSts: 1;
	canbittype 	NWM_PDMEOL: 1;
	canbittype 	NWM_PDMActiveLoadSlave: 1;
	canbittype 	NWM_PDMSystemStatus: 2;
} _c_NWM_PDM_msgType;

typedef struct {
	canbittype 	NWM_PDMZero_byte: 8;
	canbittype 	unused0: 2;
	canbittype 	NWM_PDMCurrentFailSts: 1;
	canbittype 	NWM_PDMGenericFailSts: 1;
	canbittype 	NWM_PDMEOL: 1;
	canbittype 	NWM_PDMActiveLoadSlave: 1;
	canbittype 	NWM_PDMSystemStatus: 2;
} _c_NWM_PDM_RDS_msgType;

typedef struct {
	canbittype 	NWM_DDMZero_byte: 8;
	canbittype 	unused0: 2;
	canbittype 	NWM_DDMCurrentFailSts: 1;
	canbittype 	NWM_DDMGenericFailSts: 1;
	canbittype 	NWM_DDMEOL: 1;
	canbittype 	NWM_DDMActiveLoadSlave: 1;
	canbittype 	NWM_DDMSystemStatus: 2;
} _c_NWM_DDM_msgType;

typedef struct {
	canbittype 	NWM_DDMZero_byte: 8;
	canbittype 	unused0: 2;
	canbittype 	NWM_DDMCurrentFailSts: 1;
	canbittype 	NWM_DDMGenericFailSts: 1;
	canbittype 	NWM_DDMEOL: 1;
	canbittype 	NWM_DDMActiveLoadSlave: 1;
	canbittype 	NWM_DDMSystemStatus: 2;
} _c_NWM_DDM_RDS_msgType;

typedef struct {
	canbittype 	APPL_ECU_BCM_1APPL_ECU_BCM_7: 8;
	canbittype 	APPL_ECU_BCM_1APPL_ECU_BCM_6: 8;
	canbittype 	APPL_ECU_BCM_1APPL_ECU_BCM_5: 8;
	canbittype 	APPL_ECU_BCM_1APPL_ECU_BCM_4: 8;
	canbittype 	APPL_ECU_BCM_1APPL_ECU_BCM_3: 8;
	canbittype 	APPL_ECU_BCM_1APPL_ECU_BCM_2: 8;
	canbittype 	APPL_ECU_BCM_1APPL_ECU_BCM_1: 8;
	canbittype 	APPL_ECU_BCM_1APPL_ECU_BCM_0: 8;
} _c_APPL_ECU_BCM_1_msgType;

typedef struct {
	canbittype 	APPL_ECU_BCM_1APPL_ECU_BCM_7: 8;
	canbittype 	APPL_ECU_BCM_1APPL_ECU_BCM_6: 8;
	canbittype 	APPL_ECU_BCM_1APPL_ECU_BCM_5: 8;
	canbittype 	APPL_ECU_BCM_1APPL_ECU_BCM_4: 8;
	canbittype 	APPL_ECU_BCM_1APPL_ECU_BCM_3: 8;
	canbittype 	APPL_ECU_BCM_1APPL_ECU_BCM_2: 8;
	canbittype 	APPL_ECU_BCM_1APPL_ECU_BCM_1: 8;
	canbittype 	APPL_ECU_BCM_1APPL_ECU_BCM_0: 8;
} _c_APPL_ECU_BCM_1_RDS_msgType;

typedef struct {
	canbittype 	unused0: 8;
	canbittype 	unused1: 4;
	canbittype 	TRIP_A_BTotalOdometer_2: 4;
	canbittype 	TRIP_A_BTotalOdometer_1: 8;
	canbittype 	TRIP_A_BTotalOdometer_0: 8;
	canbittype 	unused2: 8;
	canbittype 	unused3: 8;
	canbittype 	unused4: 8;
	canbittype 	unused5: 8;
} _c_TRIP_A_B_msgType;

typedef struct {
	canbittype 	unused0: 8;
	canbittype 	unused1: 4;
	canbittype 	TRIP_A_BTotalOdometer_2: 4;
	canbittype 	TRIP_A_BTotalOdometer_1: 8;
	canbittype 	TRIP_A_BTotalOdometer_0: 8;
	canbittype 	unused2: 8;
	canbittype 	unused3: 8;
	canbittype 	unused4: 8;
	canbittype 	unused5: 8;
} _c_TRIP_A_B_RDS_msgType;

typedef struct {
	canbittype 	DIAGNOSTIC_REQUEST_BCM_1N_PDU: 8;
	canbittype 	unused0: 8;
	canbittype 	unused1: 8;
	canbittype 	unused2: 8;
	canbittype 	unused3: 8;
	canbittype 	unused4: 8;
	canbittype 	unused5: 8;
	canbittype 	unused6: 8;
} _c_DIAGNOSTIC_REQUEST_BCM_1_RDS_msgType;

typedef struct {
	canbittype 	unused1: 3;
	canbittype 	TGW_A1Zoom_RQSts: 2;
	canbittype 	unused0: 3;
	canbittype 	unused2: 8;
	canbittype 	unused3: 8;
	canbittype 	TGW_A1EModeSwPsdSts: 2;
	canbittype 	unused4: 6;
} _c_TGW_A1_msgType;

typedef struct {
	canbittype 	unused1: 3;
	canbittype 	TGW_A1Zoom_RQSts: 2;
	canbittype 	unused0: 3;
	canbittype 	unused2: 8;
	canbittype 	unused3: 8;
	canbittype 	TGW_A1EModeSwPsdSts: 2;
	canbittype 	unused4: 6;
} _c_TGW_A1_RDS_msgType;

typedef struct {
	canbittype 	unused0: 8;
	canbittype 	unused1: 8;
	canbittype 	unused2: 8;
	canbittype 	unused3: 8;
	canbittype 	unused4: 8;
	canbittype 	unused6: 6;
	canbittype 	ICS_MSGTRAC_PSDSts: 1;
	canbittype 	unused5: 1;
	canbittype 	unused7: 8;
	canbittype 	ICS_MSGSpSt_Pad2: 3;
	canbittype 	ICS_MSGSpSt_Pad1: 3;
	canbittype 	unused8: 2;
} _c_ICS_MSG_msgType;

typedef struct {
	canbittype 	unused0: 8;
	canbittype 	unused1: 8;
	canbittype 	unused2: 8;
	canbittype 	unused3: 8;
	canbittype 	unused4: 8;
	canbittype 	unused6: 6;
	canbittype 	ICS_MSGTRAC_PSDSts: 1;
	canbittype 	unused5: 1;
	canbittype 	unused7: 8;
	canbittype 	ICS_MSGSpSt_Pad2: 3;
	canbittype 	ICS_MSGSpSt_Pad1: 3;
	canbittype 	unused8: 2;
} _c_ICS_MSG_RDS_msgType;

typedef struct {
	canbittype 	unused0: 7;
	canbittype 	HVAC_A1TRAC_PSDSts: 1;
	canbittype 	unused2: 3;
	canbittype 	HVAC_A1EBL_StatSts: 2;
	canbittype 	unused1: 3;
	canbittype 	unused3: 8;
	canbittype 	unused4: 8;
	canbittype 	unused5: 8;
	canbittype 	HVAC_A1LT_AMB_SENSSts: 8;
} _c_HVAC_A1_msgType;

typedef struct {
	canbittype 	unused0: 7;
	canbittype 	HVAC_A1TRAC_PSDSts: 1;
	canbittype 	unused2: 3;
	canbittype 	HVAC_A1EBL_StatSts: 2;
	canbittype 	unused1: 3;
	canbittype 	unused3: 8;
	canbittype 	unused4: 8;
	canbittype 	unused5: 8;
	canbittype 	HVAC_A1LT_AMB_SENSSts: 8;
} _c_HVAC_A1_RDS_msgType;

typedef struct {
	canbittype 	unused0: 8;
	canbittype 	unused2: 1;
	canbittype 	DIRECT_INFOHU_CMPSts: 4;
	canbittype 	unused1: 3;
	canbittype 	unused3: 8;
	canbittype 	unused4: 8;
	canbittype 	unused5: 8;
} _c_DIRECT_INFO_msgType;

typedef struct {
	canbittype 	unused0: 8;
	canbittype 	unused2: 1;
	canbittype 	DIRECT_INFOHU_CMPSts: 4;
	canbittype 	unused1: 3;
	canbittype 	unused3: 8;
	canbittype 	unused4: 8;
	canbittype 	unused5: 8;
} _c_DIRECT_INFO_RDS_msgType;

typedef struct {
	canbittype 	HVAC_A4A2D_FT_HVAC_BLOWER_VOLTAGE_1: 8;
	canbittype 	HVAC_A4A2D_FT_HVAC_BLOWER_VOLTAGE_0: 2;
	canbittype 	HVAC_A4A2D_DRV_TEMP_DR_POS_1: 6;
	canbittype 	HVAC_A4A2D_DRV_TEMP_DR_POS_0: 4;
	canbittype 	unused0: 4;
	canbittype 	unused1: 8;
} _c_HVAC_A4_msgType;

typedef struct {
	canbittype 	HVAC_A4A2D_FT_HVAC_BLOWER_VOLTAGE_1: 8;
	canbittype 	HVAC_A4A2D_FT_HVAC_BLOWER_VOLTAGE_0: 2;
	canbittype 	HVAC_A4A2D_DRV_TEMP_DR_POS_1: 6;
	canbittype 	HVAC_A4A2D_DRV_TEMP_DR_POS_0: 4;
	canbittype 	unused0: 4;
	canbittype 	unused1: 8;
} _c_HVAC_A4_RDS_msgType;

typedef struct {
	canbittype 	STATUS_RRMPowerModeSts: 2;
	canbittype 	unused0: 6;
	canbittype 	unused1: 8;
	canbittype 	unused2: 8;
	canbittype 	unused3: 8;
	canbittype 	unused4: 8;
	canbittype 	unused5: 8;
	canbittype 	unused6: 8;
	canbittype 	unused7: 8;
} _c_STATUS_RRM_msgType;

typedef struct {
	canbittype 	STATUS_RRMPowerModeSts: 2;
	canbittype 	unused0: 6;
	canbittype 	unused1: 8;
	canbittype 	unused2: 8;
	canbittype 	unused3: 8;
	canbittype 	unused4: 8;
	canbittype 	unused5: 8;
	canbittype 	unused6: 8;
	canbittype 	unused7: 8;
} _c_STATUS_RRM_RDS_msgType;

typedef struct {
	canbittype 	unused0: 8;
	canbittype 	unused1: 8;
	canbittype 	unused2: 8;
	canbittype 	unused3: 8;
	canbittype 	unused4: 8;
	canbittype 	unused6: 2;
	canbittype 	STATUS_IPCPowerModeSts: 2;
	canbittype 	unused5: 4;
	canbittype 	unused7: 8;
	canbittype 	unused8: 8;
} _c_STATUS_IPC_msgType;

typedef struct {
	canbittype 	unused0: 8;
	canbittype 	unused1: 8;
	canbittype 	unused2: 8;
	canbittype 	unused3: 8;
	canbittype 	unused4: 8;
	canbittype 	unused6: 2;
	canbittype 	STATUS_IPCPowerModeSts: 2;
	canbittype 	unused5: 4;
	canbittype 	unused7: 8;
	canbittype 	unused8: 8;
} _c_STATUS_IPC_RDS_msgType;

typedef struct {
	canbittype 	unused2: 2;
	canbittype 	STATUS_ECCRHeatedWindowCntrl: 1;
	canbittype 	unused1: 2;
	canbittype 	STATUS_ECCCompressorACReqSts: 1;
	canbittype 	unused0: 2;
	canbittype 	unused4: 2;
	canbittype 	STATUS_ECCPowerModeSts: 2;
	canbittype 	unused3: 1;
	canbittype 	STATUS_ECCPTCSts: 2;
	canbittype 	STATUS_ECCPTCFailSts: 1;
	canbittype 	STATUS_ECCStopStartEnable: 3;
	canbittype 	unused5: 5;
} _c_STATUS_ECC_msgType;

typedef struct {
	canbittype 	unused2: 2;
	canbittype 	STATUS_ECCRHeatedWindowCntrl: 1;
	canbittype 	unused1: 2;
	canbittype 	STATUS_ECCCompressorACReqSts: 1;
	canbittype 	unused0: 2;
	canbittype 	unused4: 2;
	canbittype 	STATUS_ECCPowerModeSts: 2;
	canbittype 	unused3: 1;
	canbittype 	STATUS_ECCPTCSts: 2;
	canbittype 	STATUS_ECCPTCFailSts: 1;
	canbittype 	STATUS_ECCStopStartEnable: 3;
	canbittype 	unused5: 5;
} _c_STATUS_ECC_RDS_msgType;

typedef struct {
	canbittype 	unused0: 2;
	canbittype 	HVAC_A2SpSt_Pad2: 3;
	canbittype 	HVAC_A2SpSt_Pad1: 3;
	canbittype 	HVAC_A2EVAP_TEMPSts: 8;
	canbittype 	HVAC_A2EvapTempTarSts: 8;
} _c_HVAC_A2_msgType;

typedef struct {
	canbittype 	unused0: 2;
	canbittype 	HVAC_A2SpSt_Pad2: 3;
	canbittype 	HVAC_A2SpSt_Pad1: 3;
	canbittype 	HVAC_A2EVAP_TEMPSts: 8;
	canbittype 	HVAC_A2EvapTempTarSts: 8;
} _c_HVAC_A2_RDS_msgType;

typedef struct {
	canbittype 	PLG_A1PLG_Req_PE: 1;
	canbittype 	unused0: 5;
	canbittype 	PLG_A1GT_DL_Stat_1: 2;
	canbittype 	PLG_A1GT_DL_Stat_0: 1;
	canbittype 	unused1: 7;
} _c_PLG_A1_msgType;

typedef struct {
	canbittype 	PLG_A1PLG_Req_PE: 1;
	canbittype 	unused0: 5;
	canbittype 	PLG_A1GT_DL_Stat_1: 2;
	canbittype 	PLG_A1GT_DL_Stat_0: 1;
	canbittype 	unused1: 7;
} _c_PLG_A1_RDS_msgType;

typedef struct {
	canbittype 	ECC_A1AcOutputCurrentSts: 8;
	canbittype 	unused0: 8;
} _c_ECC_A1_msgType;

typedef struct {
	canbittype 	ECC_A1AcOutputCurrentSts: 8;
	canbittype 	unused0: 8;
} _c_ECC_A1_RDS_msgType;

typedef struct {
	canbittype 	NWM_RBSSZero_byte: 8;
	canbittype 	unused0: 2;
	canbittype 	NWM_RBSSCurrentFailSts: 1;
	canbittype 	NWM_RBSSGenericFailSts: 1;
	canbittype 	NWM_RBSSEOL: 1;
	canbittype 	NWM_RBSSActiveLoadSlave: 1;
	canbittype 	NWM_RBSSSystemStatus: 2;
} _c_NWM_RBSS_msgType;

typedef struct {
	canbittype 	NWM_RBSSZero_byte: 8;
	canbittype 	unused0: 2;
	canbittype 	NWM_RBSSCurrentFailSts: 1;
	canbittype 	NWM_RBSSGenericFailSts: 1;
	canbittype 	NWM_RBSSEOL: 1;
	canbittype 	NWM_RBSSActiveLoadSlave: 1;
	canbittype 	NWM_RBSSSystemStatus: 2;
} _c_NWM_RBSS_RDS_msgType;

typedef struct {
	canbittype 	NWM_LBSSZero_byte: 8;
	canbittype 	unused0: 2;
	canbittype 	NWM_LBSSCurrentFailSts: 1;
	canbittype 	NWM_LBSSGenericFailSts: 1;
	canbittype 	NWM_LBSSEOL: 1;
	canbittype 	NWM_LBSSActiveLoadSlave: 1;
	canbittype 	NWM_LBSSSystemStatus: 2;
} _c_NWM_LBSS_msgType;

typedef struct {
	canbittype 	NWM_LBSSZero_byte: 8;
	canbittype 	unused0: 2;
	canbittype 	NWM_LBSSCurrentFailSts: 1;
	canbittype 	NWM_LBSSGenericFailSts: 1;
	canbittype 	NWM_LBSSEOL: 1;
	canbittype 	NWM_LBSSActiveLoadSlave: 1;
	canbittype 	NWM_LBSSSystemStatus: 2;
} _c_NWM_LBSS_RDS_msgType;

typedef struct {
	canbittype 	NWM_ICSZero_byte: 8;
	canbittype 	unused0: 2;
	canbittype 	NWM_ICSCurrentFailSts: 1;
	canbittype 	NWM_ICSGenericFailSts: 1;
	canbittype 	NWM_ICSEOL: 1;
	canbittype 	NWM_ICSActiveLoadSlave: 1;
	canbittype 	NWM_ICSSystemStatus: 2;
} _c_NWM_ICS_msgType;

typedef struct {
	canbittype 	NWM_ICSZero_byte: 8;
	canbittype 	unused0: 2;
	canbittype 	NWM_ICSCurrentFailSts: 1;
	canbittype 	NWM_ICSGenericFailSts: 1;
	canbittype 	NWM_ICSEOL: 1;
	canbittype 	NWM_ICSActiveLoadSlave: 1;
	canbittype 	NWM_ICSSystemStatus: 2;
} _c_NWM_ICS_RDS_msgType;

typedef struct {
	canbittype 	NWM_CSWMZero_byte: 8;
	canbittype 	unused0: 2;
	canbittype 	NWM_CSWMCurrentFailSts: 1;
	canbittype 	NWM_CSWMGenericFailSts: 1;
	canbittype 	NWM_CSWMEOL: 1;
	canbittype 	NWM_CSWMActiveLoadSlave: 1;
	canbittype 	NWM_CSWMSystemStatus: 2;
} _c_NWM_CSWM_msgType;

typedef struct {
	canbittype 	NWM_CSWMZero_byte: 8;
	canbittype 	unused0: 2;
	canbittype 	NWM_CSWMCurrentFailSts: 1;
	canbittype 	NWM_CSWMGenericFailSts: 1;
	canbittype 	NWM_CSWMEOL: 1;
	canbittype 	NWM_CSWMActiveLoadSlave: 1;
	canbittype 	NWM_CSWMSystemStatus: 2;
} _c_NWM_CSWM_RDS_msgType;

typedef struct {
	canbittype 	NWM_CDMZero_byte: 8;
	canbittype 	unused0: 2;
	canbittype 	NWM_CDMCurrentFailSts: 1;
	canbittype 	NWM_CDMGenericFailSts: 1;
	canbittype 	NWM_CDMEOL: 1;
	canbittype 	NWM_CDMActiveLoadSlave: 1;
	canbittype 	NWM_CDMSystemStatus: 2;
} _c_NWM_CDM_msgType;

typedef struct {
	canbittype 	NWM_CDMZero_byte: 8;
	canbittype 	unused0: 2;
	canbittype 	NWM_CDMCurrentFailSts: 1;
	canbittype 	NWM_CDMGenericFailSts: 1;
	canbittype 	NWM_CDMEOL: 1;
	canbittype 	NWM_CDMActiveLoadSlave: 1;
	canbittype 	NWM_CDMSystemStatus: 2;
} _c_NWM_CDM_RDS_msgType;

typedef struct {
	canbittype 	NWM_AMPZero_byte: 8;
	canbittype 	unused0: 2;
	canbittype 	NWM_AMPCurrentFailSts: 1;
	canbittype 	NWM_AMPGenericFailSts: 1;
	canbittype 	NWM_AMPEOL: 1;
	canbittype 	NWM_AMPActiveLoadSlave: 1;
	canbittype 	NWM_AMPSystemStatus: 2;
} _c_NWM_AMP_msgType;

typedef struct {
	canbittype 	NWM_AMPZero_byte: 8;
	canbittype 	unused0: 2;
	canbittype 	NWM_AMPCurrentFailSts: 1;
	canbittype 	NWM_AMPGenericFailSts: 1;
	canbittype 	NWM_AMPEOL: 1;
	canbittype 	NWM_AMPActiveLoadSlave: 1;
	canbittype 	NWM_AMPSystemStatus: 2;
} _c_NWM_AMP_RDS_msgType;

typedef struct {
	canbittype 	NWM_ECCZero_byte: 8;
	canbittype 	unused0: 2;
	canbittype 	NWM_ECCCurrentFailSts: 1;
	canbittype 	NWM_ECCGenericFailSts: 1;
	canbittype 	NWM_ECCEOL: 1;
	canbittype 	NWM_ECCActiveLoadSlave: 1;
	canbittype 	NWM_ECCSystemStatus: 2;
} _c_NWM_ECC_msgType;

typedef struct {
	canbittype 	NWM_ECCZero_byte: 8;
	canbittype 	unused0: 2;
	canbittype 	NWM_ECCCurrentFailSts: 1;
	canbittype 	NWM_ECCGenericFailSts: 1;
	canbittype 	NWM_ECCEOL: 1;
	canbittype 	NWM_ECCActiveLoadSlave: 1;
	canbittype 	NWM_ECCSystemStatus: 2;
} _c_NWM_ECC_RDS_msgType;

typedef struct {
	canbittype 	NWM_IPCZero_byte: 8;
	canbittype 	unused0: 2;
	canbittype 	NWM_IPCCurrentFailSts: 1;
	canbittype 	NWM_IPCGenericFailSts: 1;
	canbittype 	NWM_IPCEOL: 1;
	canbittype 	NWM_IPCActiveLoadSlave: 1;
	canbittype 	NWM_IPCSystemStatus: 2;
} _c_NWM_IPC_msgType;

typedef struct {
	canbittype 	NWM_IPCZero_byte: 8;
	canbittype 	unused0: 2;
	canbittype 	NWM_IPCCurrentFailSts: 1;
	canbittype 	NWM_IPCGenericFailSts: 1;
	canbittype 	NWM_IPCEOL: 1;
	canbittype 	NWM_IPCActiveLoadSlave: 1;
	canbittype 	NWM_IPCSystemStatus: 2;
} _c_NWM_IPC_RDS_msgType;

typedef struct {
	canbittype 	unused0: 1;
	canbittype 	CBC_I4RemStActvSts: 1;
	canbittype 	CBC_I4StTypSts: 3;
	canbittype 	CBC_I4CmdIgnSts: 3;
	canbittype 	unused1: 6;
	canbittype 	CBC_I4KeyInIgnSts: 2;
	canbittype 	unused2: 8;
	canbittype 	unused3: 8;
	canbittype 	unused4: 8;
} _c_CBC_I4_RDS_msgType;

typedef struct {
	canbittype 	CMCM_UNLKU_CALL_STAT: 2;
	canbittype 	CMCM_UNLKE_CALL_STAT: 2;
	canbittype 	unused0: 4;
	canbittype 	unused1: 8;
	canbittype 	unused2: 8;
} _c_CMCM_UNLK_msgType;

typedef struct {
	canbittype 	CMCM_UNLKU_CALL_STAT: 2;
	canbittype 	CMCM_UNLKE_CALL_STAT: 2;
	canbittype 	unused0: 4;
	canbittype 	unused1: 8;
	canbittype 	unused2: 8;
} _c_CMCM_UNLK_RDS_msgType;

typedef struct {
	canbittype 	CFG_RQCFG_FeatureCntrl: 8;
	canbittype 	unused0: 6;
	canbittype 	CFG_RQCFG_STAT_RQCntrl: 2;
	canbittype 	CFG_RQCFG_SETCntrl: 8;
} _c_CFG_RQ_msgType;

typedef struct {
	canbittype 	CFG_RQCFG_FeatureCntrl: 8;
	canbittype 	unused0: 6;
	canbittype 	CFG_RQCFG_STAT_RQCntrl: 2;
	canbittype 	CFG_RQCFG_SETCntrl: 8;
} _c_CFG_RQ_RDS_msgType;

/*************************************************************/
/* Message unions              */
/*************************************************************/

/* Send messages */

typedef union {
	canuint8 _c[8];
	canuint16 _w[4];
	_c_STATUS_C_BCM2_msgType	status_c_bcm2;
} _c_STATUS_C_BCM2_buf;

typedef union {
	canuint8 _c[8];
	canuint16 _w[4];
	_c_HVAC_INFO_msgType	hvac_info;
} _c_HVAC_INFO_buf;

typedef union {
	canuint8 _c[2];
	canuint16 _w[1];
	_c_CBC_PT4_msgType	cbc_pt4;
} _c_CBC_PT4_buf;

typedef union {
	canuint8 _c[2];
	canuint16 _w[1];
	_c_BCM_KEYON_COUNTER_C_CAN_msgType	bcm_keyon_counter_c_can;
	_c_BCM_KEYON_COUNTER_B_CAN_msgType	bcm_keyon_counter_b_can;
} _c_BCM_KEYON_COUNTER_C_CAN_buf;

typedef union {
	canuint8 _c[3];
	canuint16 _w[2];
	_c_IBS4_msgType	ibs4;
} _c_IBS4_buf;

typedef union {
	canuint8 _c[8];
	canuint16 _w[4];
	_c_ECU_APPL_BCM_0_msgType	ecu_appl_bcm_0;
} _c_ECU_APPL_BCM_0_buf;

typedef union {
	canuint8 _c[6];
	canuint16 _w[3];
	_c_CONFIGURATION_DATA_CODE_REQUEST_0_msgType	configuration_data_code_request_0;
} _c_CONFIGURATION_DATA_CODE_REQUEST_0_buf;

typedef union {
	canuint8 _c[1];
	canuint16 _w[1];
	_c_CBC_VTA_0_msgType	cbc_vta_0;
} _c_CBC_VTA_0_buf;

typedef union {
	canuint8 _c[8];
	canuint16 _w[4];
	_c_DIAGNOSTIC_RESPONSE_BCM_0_msgType	diagnostic_response_bcm_0;
} _c_DIAGNOSTIC_RESPONSE_BCM_0_buf;

typedef union {
	canuint8 _c[8];
	canuint16 _w[4];
	_c_STATUS_B_CAN2_msgType	status_b_can2;
} _c_STATUS_B_CAN2_buf;

typedef union {
	canuint8 _c[6];
	canuint16 _w[3];
	_c_EXTERNAL_LIGHTS_0_msgType	external_lights_0;
} _c_EXTERNAL_LIGHTS_0_buf;

typedef union {
	canuint8 _c[4];
	canuint16 _w[2];
	_c_ASBM1_CONTROL_msgType	asbm1_control;
} _c_ASBM1_CONTROL_buf;

typedef union {
	canuint8 _c[8];
	canuint16 _w[4];
	_c_IBS2_msgType	ibs2;
} _c_IBS2_buf;

typedef union {
	canuint8 _c[8];
	canuint16 _w[4];
	_c_VEHICLE_SPEED_ODOMETER_0_msgType	vehicle_speed_odometer_0;
} _c_VEHICLE_SPEED_ODOMETER_0_buf;

typedef union {
	canuint8 _c[8];
	canuint16 _w[4];
	_c_STATUS_C_BCM_msgType	status_c_bcm;
} _c_STATUS_C_BCM_buf;

typedef union {
	canuint8 _c[8];
	canuint16 _w[4];
	_c_STATUS_B_CAN_msgType	status_b_can;
} _c_STATUS_B_CAN_buf;

typedef union {
	canuint8 _c[4];
	canuint16 _w[2];
	_c_IBS3_msgType	ibs3;
} _c_IBS3_buf;

typedef union {
	canuint8 _c[8];
	canuint16 _w[4];
	_c_IBS1_msgType	ibs1;
} _c_IBS1_buf;

typedef union {
	canuint8 _c[8];
	canuint16 _w[4];
	_c_BATTERY_INFO_msgType	battery_info;
} _c_BATTERY_INFO_buf;

typedef union {
	canuint8 _c[3];
	canuint16 _w[2];
	_c_CFG_Feature_msgType	cfg_feature;
} _c_CFG_Feature_buf;

typedef union {
	canuint8 _c[5];
	canuint16 _w[3];
	_c_CBC_PT3_msgType	cbc_pt3;
} _c_CBC_PT3_buf;

typedef union {
	canuint8 _c[6];
	canuint16 _w[3];
	_c_CBC_PT1_msgType	cbc_pt1;
} _c_CBC_PT1_buf;

typedef union {
	canuint8 _c[8];
	canuint16 _w[4];
	_c_WAKE_C_BCM_msgType	wake_c_bcm;
} _c_WAKE_C_BCM_buf;

typedef union {
	canuint8 _c[5];
	canuint16 _w[3];
	_c_CBC_PT2_msgType	cbc_pt2;
} _c_CBC_PT2_buf;

typedef union {
	canuint8 _c[5];
	canuint16 _w[3];
	_c_BCM_COMMAND_msgType	bcm_command;
} _c_BCM_COMMAND_buf;

typedef union {
	canuint8 _c[8];
	canuint16 _w[4];
	_c_BCM_CODE_TRM_REQUEST_msgType	bcm_code_trm_request;
} _c_BCM_CODE_TRM_REQUEST_buf;

typedef union {
	canuint8 _c[8];
	canuint16 _w[4];
	_c_BCM_CODE_ESL_REQUEST_msgType	bcm_code_esl_request;
} _c_BCM_CODE_ESL_REQUEST_buf;

typedef union {
	canuint8 _c[1];
	canuint16 _w[1];
	_c_BCM_MINICRYPT_ACK_msgType	bcm_minicrypt_ack;
} _c_BCM_MINICRYPT_ACK_buf;

typedef union {
	canuint8 _c[7];
	canuint16 _w[4];
	_c_IMMO_CODE_RESPONSE_msgType	immo_code_response;
} _c_IMMO_CODE_RESPONSE_buf;

typedef union {
	canuint8 _c[8];
	canuint16 _w[4];
} _c_TxDynamicMsg0_0_buf;

typedef union {
	canuint8 _c[8];
	canuint16 _w[4];
} _c_TxDynamicMsg1_0_buf;

typedef union {
	canuint8 _c[8];
	canuint16 _w[4];
	_c_ECU_APPL_BCM_1_msgType	ecu_appl_bcm_1;
} _c_ECU_APPL_BCM_1_buf;

typedef union {
	canuint8 _c[8];
	canuint16 _w[4];
	_c_DTO_BCM_msgType	dto_bcm;
} _c_DTO_BCM_buf;

typedef union {
	canuint8 _c[6];
	canuint16 _w[3];
	_c_CONFIGURATION_DATA_CODE_REQUEST_1_msgType	configuration_data_code_request_1;
} _c_CONFIGURATION_DATA_CODE_REQUEST_1_buf;

typedef union {
	canuint8 _c[4];
	canuint16 _w[2];
	_c_IBS_2_msgType	ibs_2;
} _c_IBS_2_buf;

typedef union {
	canuint8 _c[8];
	canuint16 _w[4];
	_c_HUMIDITY_1000L_msgType	humidity_1000l;
} _c_HUMIDITY_1000L_buf;

typedef union {
	canuint8 _c[3];
	canuint16 _w[2];
	_c_COMPASS_A1_msgType	compass_a1;
} _c_COMPASS_A1_buf;

typedef union {
	canuint8 _c[4];
	canuint16 _w[2];
	_c_CBC_I5_msgType	cbc_i5;
} _c_CBC_I5_buf;

typedef union {
	canuint8 _c[2];
	canuint16 _w[1];
	_c_AMB_TEMP_DISP_msgType	amb_temp_disp;
} _c_AMB_TEMP_DISP_buf;

typedef union {
	canuint8 _c[2];
	canuint16 _w[1];
	_c_WCPM_STATUS_msgType	wcpm_status;
} _c_WCPM_STATUS_buf;

typedef union {
	canuint8 _c[3];
	canuint16 _w[2];
	_c_GE_B_msgType	ge_b;
} _c_GE_B_buf;

typedef union {
	canuint8 _c[8];
	canuint16 _w[4];
	_c_ENVIRONMENTAL_CONDITIONS_msgType	environmental_conditions;
} _c_ENVIRONMENTAL_CONDITIONS_buf;

typedef union {
	canuint8 _c[1];
	canuint16 _w[1];
	_c_CBC_VTA_1_msgType	cbc_vta_1;
} _c_CBC_VTA_1_buf;

typedef union {
	canuint8 _c[8];
	canuint16 _w[4];
	_c_DIAGNOSTIC_RESPONSE_BCM_1_msgType	diagnostic_response_bcm_1;
} _c_DIAGNOSTIC_RESPONSE_BCM_1_buf;

typedef union {
	canuint8 _c[5];
	canuint16 _w[3];
	_c_GW_C_I2_msgType	gw_c_i2;
} _c_GW_C_I2_buf;

typedef union {
	canuint8 _c[4];
	canuint16 _w[2];
	_c_DAS_B_msgType	das_b;
} _c_DAS_B_buf;

typedef union {
	canuint8 _c[8];
	canuint16 _w[4];
	_c_CBC_I2_msgType	cbc_i2;
} _c_CBC_I2_buf;

typedef union {
	canuint8 _c[3];
	canuint16 _w[2];
	_c_CBC_I1_msgType	cbc_i1;
} _c_CBC_I1_buf;

typedef union {
	canuint8 _c[4];
	canuint16 _w[2];
	_c_STATUS_C_CAN_msgType	status_c_can;
} _c_STATUS_C_CAN_buf;

typedef union {
	canuint8 _c[8];
	canuint16 _w[4];
	_c_SWS_8_msgType	sws_8;
} _c_SWS_8_buf;

typedef union {
	canuint8 _c[7];
	canuint16 _w[4];
	_c_StW_Actn_Rq_1_msgType	stw_actn_rq_1;
} _c_StW_Actn_Rq_1_buf;

typedef union {
	canuint8 _c[5];
	canuint16 _w[3];
	_c_PAM_B_msgType	pam_b;
} _c_PAM_B_buf;

typedef union {
	canuint8 _c[2];
	canuint16 _w[1];
	_c_GW_C_I3_msgType	gw_c_i3;
} _c_GW_C_I3_buf;

typedef union {
	canuint8 _c[4];
	canuint16 _w[2];
	_c_VEHICLE_SPEED_ODOMETER_1_msgType	vehicle_speed_odometer_1;
} _c_VEHICLE_SPEED_ODOMETER_1_buf;

typedef union {
	canuint8 _c[4];
	canuint16 _w[2];
	_c_GW_C_I6_msgType	gw_c_i6;
} _c_GW_C_I6_buf;

typedef union {
	canuint8 _c[8];
	canuint16 _w[4];
	_c_DYNAMIC_VEHICLE_INFO2_msgType	dynamic_vehicle_info2;
} _c_DYNAMIC_VEHICLE_INFO2_buf;

typedef union {
	canuint8 _c[6];
	canuint16 _w[3];
	_c_NWM_BCM_msgType	nwm_bcm;
} _c_NWM_BCM_buf;

typedef union {
	canuint8 _c[8];
	canuint16 _w[4];
	_c_STATUS_BCM2_msgType	status_bcm2;
} _c_STATUS_BCM2_buf;

typedef union {
	canuint8 _c[8];
	canuint16 _w[4];
	_c_STATUS_BCM_msgType	status_bcm;
} _c_STATUS_BCM_buf;

typedef union {
	canuint8 _c[6];
	canuint16 _w[3];
	_c_EXTERNAL_LIGHTS_1_msgType	external_lights_1;
} _c_EXTERNAL_LIGHTS_1_buf;

typedef union {
	canuint8 _c[5];
	canuint16 _w[3];
	_c_CBC_I4_msgType	cbc_i4;
} _c_CBC_I4_buf;

typedef union {
	canuint8 _c[8];
	canuint16 _w[4];
} _c_TxDynamicMsg0_1_buf;

typedef union {
	canuint8 _c[8];
	canuint16 _w[4];
} _c_TxDynamicMsg1_1_buf;

/* Receive messages */

typedef union {
	canuint8 _c[3];
	canuint16 _w[2];
	_c_STATUS_C_OCM_msgType	status_c_ocm;
} _c_STATUS_C_OCM_buf;

typedef union {
	canuint8 _c[6];
	canuint16 _w[3];
	_c_CFG_DATA_CODE_RSP_OCM_msgType	cfg_data_code_rsp_ocm;
} _c_CFG_DATA_CODE_RSP_OCM_buf;

typedef union {
	canuint8 _c[6];
	canuint16 _w[3];
	_c_CFG_DATA_CODE_RSP_AHLM_msgType	cfg_data_code_rsp_ahlm;
} _c_CFG_DATA_CODE_RSP_AHLM_buf;

typedef union {
	canuint8 _c[6];
	canuint16 _w[3];
	_c_CFG_DATA_CODE_RSP_DTCM_msgType	cfg_data_code_rsp_dtcm;
} _c_CFG_DATA_CODE_RSP_DTCM_buf;

typedef union {
	canuint8 _c[6];
	canuint16 _w[3];
	_c_CFG_DATA_CODE_RSP_RFHM_msgType	cfg_data_code_rsp_rfhm;
} _c_CFG_DATA_CODE_RSP_RFHM_buf;

typedef union {
	canuint8 _c[6];
	canuint16 _w[3];
	_c_CFG_DATA_CODE_RSP_DASM_msgType	cfg_data_code_rsp_dasm;
} _c_CFG_DATA_CODE_RSP_DASM_buf;

typedef union {
	canuint8 _c[6];
	canuint16 _w[3];
	_c_CFG_DATA_CODE_RSP_SCCM_msgType	cfg_data_code_rsp_sccm;
} _c_CFG_DATA_CODE_RSP_SCCM_buf;

typedef union {
	canuint8 _c[6];
	canuint16 _w[3];
	_c_CFG_DATA_CODE_RSP_HALF_msgType	cfg_data_code_rsp_half;
} _c_CFG_DATA_CODE_RSP_HALF_buf;

typedef union {
	canuint8 _c[6];
	canuint16 _w[3];
	_c_CFG_DATA_CODE_RSP_ORC_msgType	cfg_data_code_rsp_orc;
} _c_CFG_DATA_CODE_RSP_ORC_buf;

typedef union {
	canuint8 _c[6];
	canuint16 _w[3];
	_c_CFG_DATA_CODE_RSP_PAM_msgType	cfg_data_code_rsp_pam;
} _c_CFG_DATA_CODE_RSP_PAM_buf;

typedef union {
	canuint8 _c[6];
	canuint16 _w[3];
	_c_CFG_DATA_CODE_RSP_EPB_msgType	cfg_data_code_rsp_epb;
} _c_CFG_DATA_CODE_RSP_EPB_buf;

typedef union {
	canuint8 _c[6];
	canuint16 _w[3];
	_c_CFG_DATA_CODE_RSP_TRANSMISSION_msgType	cfg_data_code_rsp_transmission;
} _c_CFG_DATA_CODE_RSP_TRANSMISSION_buf;

typedef union {
	canuint8 _c[6];
	canuint16 _w[3];
	_c_CFG_DATA_CODE_RSP_BSM_msgType	cfg_data_code_rsp_bsm;
} _c_CFG_DATA_CODE_RSP_BSM_buf;

typedef union {
	canuint8 _c[6];
	canuint16 _w[3];
	_c_CFG_DATA_CODE_RSP_EPS_msgType	cfg_data_code_rsp_eps;
} _c_CFG_DATA_CODE_RSP_EPS_buf;

typedef union {
	canuint8 _c[6];
	canuint16 _w[3];
	_c_CFG_DATA_CODE_RSP_ECM_msgType	cfg_data_code_rsp_ecm;
} _c_CFG_DATA_CODE_RSP_ECM_buf;

typedef union {
	canuint8 _c[2];
	canuint16 _w[1];
	_c_STATUS_C_DASM_msgType	status_c_dasm;
} _c_STATUS_C_DASM_buf;

typedef union {
	canuint8 _c[2];
	canuint16 _w[1];
	_c_STATUS_C_AHLM_msgType	status_c_ahlm;
} _c_STATUS_C_AHLM_buf;

typedef union {
	canuint8 _c[8];
	canuint16 _w[4];
	_c_STATUS_C_HALF_msgType	status_c_half;
	_c_STATUS_B_HALF_msgType	status_b_half;
} _c_STATUS_C_HALF_buf;

typedef union {
	canuint8 _c[2];
	canuint16 _w[1];
	_c_STATUS_C_ESL_msgType	status_c_esl;
} _c_STATUS_C_ESL_buf;

typedef union {
	canuint8 _c[7];
	canuint16 _w[4];
	_c_STATUS_C_EPB_msgType	status_c_epb;
	_c_STATUS_B_EPB_msgType	status_b_epb;
} _c_STATUS_C_EPB_buf;

typedef union {
	canuint8 _c[2];
	canuint16 _w[1];
	_c_HALF_C_Warning_RQ_msgType	half_c_warning_rq;
	_c_HALF_B_Warning_RQ_msgType	half_b_warning_rq;
} _c_HALF_C_Warning_RQ_buf;

typedef union {
	canuint8 _c[7];
	canuint16 _w[4];
	_c_StW_Actn_Rq_0_msgType	stw_actn_rq_0;
} _c_StW_Actn_Rq_0_buf;

typedef union {
	canuint8 _c[8];
	canuint16 _w[4];
	_c_STATUS_C_TRANSMISSION_msgType	status_c_transmission;
	_c_STATUS_B_TRANSMISSION_msgType	status_b_transmission;
} _c_STATUS_C_TRANSMISSION_buf;

typedef union {
	canuint8 _c[8];
	canuint16 _w[4];
	_c_VIN_0_msgType	vin_0;
	_c_VIN_1_msgType	vin_1;
} _c_VIN_0_buf;

typedef union {
	canuint8 _c[8];
	canuint16 _w[4];
	_c_MOT3_msgType	mot3;
} _c_MOT3_buf;

typedef union {
	canuint8 _c[8];
	canuint16 _w[4];
	_c_ECM_1_msgType	ecm_1;
} _c_ECM_1_buf;

typedef union {
	canuint8 _c[8];
	canuint16 _w[4];
	_c_STATUS_C_DTCM_msgType	status_c_dtcm;
} _c_STATUS_C_DTCM_buf;

typedef union {
	canuint8 _c[7];
	canuint16 _w[4];
	_c_DAS_A2_msgType	das_a2;
} _c_DAS_A2_buf;

typedef union {
	canuint8 _c[5];
	canuint16 _w[3];
	_c_PAM_2_msgType	pam_2;
} _c_PAM_2_buf;

typedef union {
	canuint8 _c[3];
	canuint16 _w[2];
	_c_EPB_A1_msgType	epb_a1;
} _c_EPB_A1_buf;

typedef union {
	canuint8 _c[8];
	canuint16 _w[4];
	_c_DAS_A1_msgType	das_a1;
} _c_DAS_A1_buf;

typedef union {
	canuint8 _c[8];
	canuint16 _w[4];
	_c_BSM_4_msgType	bsm_4;
} _c_BSM_4_buf;

typedef union {
	canuint8 _c[8];
	canuint16 _w[4];
	_c_BSM_2_msgType	bsm_2;
} _c_BSM_2_buf;

typedef union {
	canuint8 _c[8];
	canuint16 _w[4];
	_c_DTCM_A1_msgType	dtcm_a1;
} _c_DTCM_A1_buf;

typedef union {
	canuint8 _c[4];
	canuint16 _w[2];
	_c_SC_msgType	sc;
} _c_SC_buf;

typedef union {
	canuint8 _c[7];
	canuint16 _w[4];
	_c_ORC_YRS_DATA_msgType	orc_yrs_data;
} _c_ORC_YRS_DATA_buf;

typedef union {
	canuint8 _c[5];
	canuint16 _w[3];
	_c_MOTGEAR2_msgType	motgear2;
} _c_MOTGEAR2_buf;

typedef union {
	canuint8 _c[8];
	canuint16 _w[4];
	_c_MOT4_msgType	mot4;
} _c_MOT4_buf;

typedef union {
	canuint8 _c[4];
	canuint16 _w[2];
	_c_BSM_YRS_DATA_msgType	bsm_yrs_data;
} _c_BSM_YRS_DATA_buf;

typedef union {
	canuint8 _c[8];
	canuint16 _w[4];
	_c_ASR4_msgType	asr4;
} _c_ASR4_buf;

typedef union {
	canuint8 _c[8];
	canuint16 _w[4];
	_c_ASR3_msgType	asr3;
} _c_ASR3_buf;

typedef union {
	canuint8 _c[1];
	canuint16 _w[1];
	_c_RFHUB_A4_msgType	rfhub_a4;
	_c_RFHUB_B_A4_msgType	rfhub_b_a4;
} _c_RFHUB_A4_buf;

typedef union {
	canuint8 _c[3];
	canuint16 _w[2];
	_c_IPC_CFG_Feature_msgType	ipc_cfg_feature;
} _c_IPC_CFG_Feature_buf;

typedef union {
	canuint8 _c[5];
	canuint16 _w[3];
	_c_ESL_CODE_RESPONSE_msgType	esl_code_response;
} _c_ESL_CODE_RESPONSE_buf;

typedef union {
	canuint8 _c[7];
	canuint16 _w[4];
	_c_IMMO_CODE_REQUEST_msgType	immo_code_request;
} _c_IMMO_CODE_REQUEST_buf;

typedef union {
	canuint8 _c[8];
	canuint16 _w[4];
	_c_TRM_MKP_KEY_msgType	trm_mkp_key;
} _c_TRM_MKP_KEY_buf;

typedef union {
	canuint8 _c[2];
	canuint16 _w[1];
	_c_RFHUB_A3_msgType	rfhub_a3;
} _c_RFHUB_A3_buf;

typedef union {
	canuint8 _c[8];
	canuint16 _w[4];
	_c_APPL_ECU_BCM_0_msgType	appl_ecu_bcm_0;
} _c_APPL_ECU_BCM_0_buf;

typedef union {
	canuint8 _c[4];
	canuint16 _w[2];
	_c_STATUS_C_SCCM_msgType	status_c_sccm;
} _c_STATUS_C_SCCM_buf;

typedef union {
	canuint8 _c[4];
	canuint16 _w[2];
	_c_STATUS_C_RFHM_msgType	status_c_rfhm;
} _c_STATUS_C_RFHM_buf;

typedef union {
	canuint8 _c[4];
	canuint16 _w[2];
	_c_IPC_A1_msgType	ipc_a1;
} _c_IPC_A1_buf;

typedef union {
	canuint8 _c[8];
	canuint16 _w[4];
	_c_STATUS_C_SPM_msgType	status_c_spm;
} _c_STATUS_C_SPM_buf;

typedef union {
	canuint8 _c[8];
	canuint16 _w[4];
	_c_STATUS_C_IPC_msgType	status_c_ipc;
} _c_STATUS_C_IPC_buf;

typedef union {
	canuint8 _c[2];
	canuint16 _w[1];
	_c_STATUS_C_GSM_msgType	status_c_gsm;
} _c_STATUS_C_GSM_buf;

typedef union {
	canuint8 _c[2];
	canuint16 _w[1];
	_c_STATUS_C_EPS_msgType	status_c_eps;
} _c_STATUS_C_EPS_buf;

typedef union {
	canuint8 _c[8];
	canuint16 _w[4];
	_c_STATUS_C_BSM_msgType	status_c_bsm;
	_c_STATUS_B_BSM_msgType	status_b_bsm;
} _c_STATUS_C_BSM_buf;

typedef union {
	canuint8 _c[8];
	canuint16 _w[4];
	_c_STATUS_C_ECM2_msgType	status_c_ecm2;
	_c_STATUS_B_ECM2_msgType	status_b_ecm2;
} _c_STATUS_C_ECM2_buf;

typedef union {
	canuint8 _c[8];
	canuint16 _w[4];
	_c_STATUS_C_ECM_msgType	status_c_ecm;
	_c_STATUS_B_ECM_msgType	status_b_ecm;
} _c_STATUS_C_ECM_buf;

typedef union {
	canuint8 _c[8];
	canuint16 _w[4];
	_c_STATUS_C_ORC_msgType	status_c_orc;
} _c_STATUS_C_ORC_buf;

typedef union {
	canuint8 _c[5];
	canuint16 _w[3];
	_c_RFHUB_A2_msgType	rfhub_a2;
	_c_RFHUB_B_A2_msgType	rfhub_b_a2;
} _c_RFHUB_A2_buf;

typedef union {
	canuint8 _c[1];
	canuint16 _w[1];
	_c_RFHUB_A1_msgType	rfhub_a1;
} _c_RFHUB_A1_buf;

typedef union {
	canuint8 _c[8];
	canuint16 _w[4];
	_c_PAM_1_msgType	pam_1;
} _c_PAM_1_buf;

typedef union {
	canuint8 _c[4];
	canuint16 _w[2];
	_c_WAKE_C_RFHM_msgType	wake_c_rfhm;
} _c_WAKE_C_RFHM_buf;

typedef union {
	canuint8 _c[4];
	canuint16 _w[2];
	_c_WAKE_C_SCCM_msgType	wake_c_sccm;
} _c_WAKE_C_SCCM_buf;

typedef union {
	canuint8 _c[4];
	canuint16 _w[2];
	_c_WAKE_C_ESL_msgType	wake_c_esl;
} _c_WAKE_C_ESL_buf;

typedef union {
	canuint8 _c[4];
	canuint16 _w[2];
	_c_WAKE_C_EPB_msgType	wake_c_epb;
} _c_WAKE_C_EPB_buf;

typedef union {
	canuint8 _c[4];
	canuint16 _w[2];
	_c_WAKE_C_BSM_msgType	wake_c_bsm;
} _c_WAKE_C_BSM_buf;

typedef union {
	canuint8 _c[8];
	canuint16 _w[4];
	_c_MOT1_msgType	mot1;
} _c_MOT1_buf;

typedef union {
	canuint8 _c[6];
	canuint16 _w[3];
	_c_GE_msgType	ge;
} _c_GE_buf;

typedef union {
	canuint8 _c[6];
	canuint16 _w[3];
	_c_TRM_CODE_RESPONSE_msgType	trm_code_response;
} _c_TRM_CODE_RESPONSE_buf;

typedef union {
	canuint8 _c[2];
	canuint16 _w[1];
	_c_ORC_A2_msgType	orc_a2;
} _c_ORC_A2_buf;

typedef union {
	canuint8 _c[8];
	canuint16 _w[4];
	_c_CRO_BCM_msgType	cro_bcm;
} _c_CRO_BCM_buf;

typedef union {
	canuint8 _c[6];
	canuint16 _w[3];
	_c_CFG_DATA_CODE_RSP_RBSS_msgType	cfg_data_code_rsp_rbss;
} _c_CFG_DATA_CODE_RSP_RBSS_buf;

typedef union {
	canuint8 _c[6];
	canuint16 _w[3];
	_c_CFG_DATA_CODE_RSP_LBSS_msgType	cfg_data_code_rsp_lbss;
} _c_CFG_DATA_CODE_RSP_LBSS_buf;

typedef union {
	canuint8 _c[6];
	canuint16 _w[3];
	_c_CFG_DATA_CODE_RSP_ICS_msgType	cfg_data_code_rsp_ics;
} _c_CFG_DATA_CODE_RSP_ICS_buf;

typedef union {
	canuint8 _c[6];
	canuint16 _w[3];
	_c_CFG_DATA_CODE_RSP_CSWM_msgType	cfg_data_code_rsp_cswm;
} _c_CFG_DATA_CODE_RSP_CSWM_buf;

typedef union {
	canuint8 _c[6];
	canuint16 _w[3];
	_c_CFG_DATA_CODE_RSP_CDM_msgType	cfg_data_code_rsp_cdm;
} _c_CFG_DATA_CODE_RSP_CDM_buf;

typedef union {
	canuint8 _c[6];
	canuint16 _w[3];
	_c_CFG_DATA_CODE_RSP_AMP_msgType	cfg_data_code_rsp_amp;
} _c_CFG_DATA_CODE_RSP_AMP_buf;

typedef union {
	canuint8 _c[6];
	canuint16 _w[3];
	_c_CFG_DATA_CODE_RSP_ETM_msgType	cfg_data_code_rsp_etm;
} _c_CFG_DATA_CODE_RSP_ETM_buf;

typedef union {
	canuint8 _c[6];
	canuint16 _w[3];
	_c_CFG_DATA_CODE_RSP_DSM_msgType	cfg_data_code_rsp_dsm;
} _c_CFG_DATA_CODE_RSP_DSM_buf;

typedef union {
	canuint8 _c[6];
	canuint16 _w[3];
	_c_CFG_DATA_CODE_RSP_PDM_msgType	cfg_data_code_rsp_pdm;
} _c_CFG_DATA_CODE_RSP_PDM_buf;

typedef union {
	canuint8 _c[6];
	canuint16 _w[3];
	_c_CFG_DATA_CODE_RSP_ECC_msgType	cfg_data_code_rsp_ecc;
} _c_CFG_DATA_CODE_RSP_ECC_buf;

typedef union {
	canuint8 _c[6];
	canuint16 _w[3];
	_c_CFG_DATA_CODE_RSP_DDM_msgType	cfg_data_code_rsp_ddm;
} _c_CFG_DATA_CODE_RSP_DDM_buf;

typedef union {
	canuint8 _c[6];
	canuint16 _w[3];
	_c_CFG_DATA_CODE_RSP_IPC_msgType	cfg_data_code_rsp_ipc;
} _c_CFG_DATA_CODE_RSP_IPC_buf;

typedef union {
	canuint8 _c[2];
	canuint16 _w[1];
	_c_STATUS_ICS_msgType	status_ics;
} _c_STATUS_ICS_buf;

typedef union {
	canuint8 _c[2];
	canuint16 _w[1];
	_c_STATUS_CDM_msgType	status_cdm;
} _c_STATUS_CDM_buf;

typedef union {
	canuint8 _c[2];
	canuint16 _w[1];
	_c_STATUS_AMP_msgType	status_amp;
} _c_STATUS_AMP_buf;

typedef union {
	canuint8 _c[6];
	canuint16 _w[3];
	_c_FT_HVAC_STAT_msgType	ft_hvac_stat;
} _c_FT_HVAC_STAT_buf;

typedef union {
	canuint8 _c[2];
	canuint16 _w[1];
	_c_DCM_P_MSG_msgType	dcm_p_msg;
} _c_DCM_P_MSG_buf;

typedef union {
	canuint8 _c[2];
	canuint16 _w[1];
	_c_DCM_D_MSG_msgType	dcm_d_msg;
} _c_DCM_D_MSG_buf;

typedef union {
	canuint8 _c[2];
	canuint16 _w[1];
	_c_NWM_PLGM_msgType	nwm_plgm;
} _c_NWM_PLGM_buf;

typedef union {
	canuint8 _c[2];
	canuint16 _w[1];
	_c_NWM_ETM_msgType	nwm_etm;
} _c_NWM_ETM_buf;

typedef union {
	canuint8 _c[2];
	canuint16 _w[1];
	_c_NWM_DSM_msgType	nwm_dsm;
} _c_NWM_DSM_buf;

typedef union {
	canuint8 _c[2];
	canuint16 _w[1];
	_c_NWM_PDM_msgType	nwm_pdm;
} _c_NWM_PDM_buf;

typedef union {
	canuint8 _c[2];
	canuint16 _w[1];
	_c_NWM_DDM_msgType	nwm_ddm;
} _c_NWM_DDM_buf;

typedef union {
	canuint8 _c[8];
	canuint16 _w[4];
	_c_APPL_ECU_BCM_1_msgType	appl_ecu_bcm_1;
} _c_APPL_ECU_BCM_1_buf;

typedef union {
	canuint8 _c[8];
	canuint16 _w[4];
	_c_TRIP_A_B_msgType	trip_a_b;
} _c_TRIP_A_B_buf;

typedef union {
	canuint8 _c[4];
	canuint16 _w[2];
	_c_TGW_A1_msgType	tgw_a1;
} _c_TGW_A1_buf;

typedef union {
	canuint8 _c[8];
	canuint16 _w[4];
	_c_ICS_MSG_msgType	ics_msg;
} _c_ICS_MSG_buf;

typedef union {
	canuint8 _c[6];
	canuint16 _w[3];
	_c_HVAC_A1_msgType	hvac_a1;
} _c_HVAC_A1_buf;

typedef union {
	canuint8 _c[5];
	canuint16 _w[3];
	_c_DIRECT_INFO_msgType	direct_info;
} _c_DIRECT_INFO_buf;

typedef union {
	canuint8 _c[4];
	canuint16 _w[2];
	_c_HVAC_A4_msgType	hvac_a4;
	_c_HVAC_INFO2_msgType	hvac_info2;
} _c_HVAC_A4_buf;

typedef union {
	canuint8 _c[8];
	canuint16 _w[4];
	_c_STATUS_RRM_msgType	status_rrm;
} _c_STATUS_RRM_buf;

typedef union {
	canuint8 _c[8];
	canuint16 _w[4];
	_c_STATUS_IPC_msgType	status_ipc;
} _c_STATUS_IPC_buf;

typedef union {
	canuint8 _c[3];
	canuint16 _w[2];
	_c_STATUS_ECC_msgType	status_ecc;
} _c_STATUS_ECC_buf;

typedef union {
	canuint8 _c[3];
	canuint16 _w[2];
	_c_HVAC_A2_msgType	hvac_a2;
} _c_HVAC_A2_buf;

typedef union {
	canuint8 _c[2];
	canuint16 _w[1];
	_c_PLG_A1_msgType	plg_a1;
} _c_PLG_A1_buf;

typedef union {
	canuint8 _c[2];
	canuint16 _w[1];
	_c_ECC_A1_msgType	ecc_a1;
} _c_ECC_A1_buf;

typedef union {
	canuint8 _c[2];
	canuint16 _w[1];
	_c_NWM_RBSS_msgType	nwm_rbss;
} _c_NWM_RBSS_buf;

typedef union {
	canuint8 _c[2];
	canuint16 _w[1];
	_c_NWM_LBSS_msgType	nwm_lbss;
} _c_NWM_LBSS_buf;

typedef union {
	canuint8 _c[2];
	canuint16 _w[1];
	_c_NWM_ICS_msgType	nwm_ics;
} _c_NWM_ICS_buf;

typedef union {
	canuint8 _c[2];
	canuint16 _w[1];
	_c_NWM_CSWM_msgType	nwm_cswm;
} _c_NWM_CSWM_buf;

typedef union {
	canuint8 _c[2];
	canuint16 _w[1];
	_c_NWM_CDM_msgType	nwm_cdm;
} _c_NWM_CDM_buf;

typedef union {
	canuint8 _c[2];
	canuint16 _w[1];
	_c_NWM_AMP_msgType	nwm_amp;
} _c_NWM_AMP_buf;

typedef union {
	canuint8 _c[2];
	canuint16 _w[1];
	_c_NWM_ECC_msgType	nwm_ecc;
} _c_NWM_ECC_buf;

typedef union {
	canuint8 _c[2];
	canuint16 _w[1];
	_c_NWM_IPC_msgType	nwm_ipc;
} _c_NWM_IPC_buf;

typedef union {
	canuint8 _c[3];
	canuint16 _w[2];
	_c_CMCM_UNLK_msgType	cmcm_unlk;
} _c_CMCM_UNLK_buf;

typedef union {
	canuint8 _c[3];
	canuint16 _w[2];
	_c_CFG_RQ_msgType	cfg_rq;
} _c_CFG_RQ_buf;

/* CAN buffer */

typedef union {
	canuint8 _c[8];
	_c_CBC_PT2_RDS_msgType	cbc_pt2;
	_c_BCM_COMMAND_RDS_msgType	bcm_command;
}_c_RDSTx_0_buf;

typedef union {
	canuint8 _c[8];
}_c_RDSTx_1_buf;

typedef union {
	canuint8 _c[8];
	_c_DIAGNOSTIC_REQUEST_BCM_0_RDS_msgType	diagnostic_request_bcm_0;
}_c_RDSBasic_0_buf;

typedef union {
	canuint8 _c[8];
	/*Rx ID: 0x7d2*/
	_c_RFHUB_A3_RDS_msgType	rfhub_a3;
}_c_RDS4_0_buf;

typedef union {
	canuint8 _c[8];
	/*Rx ID: 0x1e7cafe5*/
	_c_APPL_ECU_BCM_0_RDS_msgType	appl_ecu_bcm_0;
}_c_RDS5_0_buf;

typedef union {
	canuint8 _c[8];
	/*Rx ID: 0x6dc*/
	_c_STATUS_C_SCCM_RDS_msgType	status_c_sccm;
}_c_RDS6_0_buf;

typedef union {
	canuint8 _c[8];
	/*Rx ID: 0x6da*/
	_c_STATUS_C_RFHM_RDS_msgType	status_c_rfhm;
}_c_RDS7_0_buf;

typedef union {
	canuint8 _c[8];
	/*Rx ID: 0x659*/
	_c_IPC_A1_RDS_msgType	ipc_a1;
}_c_RDS8_0_buf;

typedef union {
	canuint8 _c[8];
	/*Rx ID: 0x5e6*/
	_c_STATUS_C_SPM_RDS_msgType	status_c_spm;
}_c_RDS9_0_buf;

typedef union {
	canuint8 _c[8];
	/*Rx ID: 0x4ea*/
	_c_STATUS_C_IPC_RDS_msgType	status_c_ipc;
}_c_RDS10_0_buf;

typedef union {
	canuint8 _c[8];
	/*Rx ID: 0x4e8*/
	_c_STATUS_C_GSM_RDS_msgType	status_c_gsm;
}_c_RDS11_0_buf;

typedef union {
	canuint8 _c[8];
	/*Rx ID: 0x4e6*/
	_c_STATUS_C_EPS_RDS_msgType	status_c_eps;
}_c_RDS12_0_buf;

typedef union {
	canuint8 _c[8];
	/*Rx ID: 0x4e4*/
	_c_STATUS_C_BSM_RDS_msgType	status_c_bsm;
}_c_RDS13_0_buf;

typedef union {
	canuint8 _c[8];
	/*Rx ID: 0x3ec*/
	_c_STATUS_C_ECM2_RDS_msgType	status_c_ecm2;
}_c_RDS14_0_buf;

typedef union {
	canuint8 _c[8];
	/*Rx ID: 0x3ea*/
	_c_STATUS_C_ECM_RDS_msgType	status_c_ecm;
}_c_RDS15_0_buf;

typedef union {
	canuint8 _c[8];
	/*Rx ID: 0x36b*/
	_c_STATUS_C_ORC_RDS_msgType	status_c_orc;
}_c_RDS16_0_buf;

typedef union {
	canuint8 _c[8];
	/*Rx ID: 0x2f8*/
	_c_RFHUB_A2_RDS_msgType	rfhub_a2;
}_c_RDS17_0_buf;

typedef union {
	canuint8 _c[8];
	/*Rx ID: 0x2f6*/
	_c_RFHUB_A1_RDS_msgType	rfhub_a1;
}_c_RDS18_0_buf;

typedef union {
	canuint8 _c[8];
	/*Rx ID: 0x2f2*/
	_c_PAM_1_RDS_msgType	pam_1;
}_c_RDS19_0_buf;

typedef union {
	canuint8 _c[8];
	/*Rx ID: 0x817a041*/
	_c_WAKE_C_RFHM_RDS_msgType	wake_c_rfhm;
}_c_RDS20_0_buf;

typedef union {
	canuint8 _c[8];
	/*Rx ID: 0x817a035*/
	_c_WAKE_C_SCCM_RDS_msgType	wake_c_sccm;
}_c_RDS21_0_buf;

typedef union {
	canuint8 _c[8];
	/*Rx ID: 0x817a015*/
	_c_WAKE_C_ESL_RDS_msgType	wake_c_esl;
}_c_RDS22_0_buf;

typedef union {
	canuint8 _c[8];
	/*Rx ID: 0x817a00d*/
	_c_WAKE_C_EPB_RDS_msgType	wake_c_epb;
}_c_RDS23_0_buf;

typedef union {
	canuint8 _c[8];
	/*Rx ID: 0x817a006*/
	_c_WAKE_C_BSM_RDS_msgType	wake_c_bsm;
}_c_RDS24_0_buf;

typedef union {
	canuint8 _c[8];
	/*Rx ID: 0x1f8*/
	_c_MOT1_RDS_msgType	mot1;
}_c_RDS25_0_buf;

typedef union {
	canuint8 _c[8];
	/*Rx ID: 0x1ee*/
	_c_GE_RDS_msgType	ge;
}_c_RDS26_0_buf;

typedef union {
	canuint8 _c[8];
	/*Rx ID: 0xfd*/
	_c_TRM_CODE_RESPONSE_RDS_msgType	trm_code_response;
}_c_RDS27_0_buf;

typedef union {
	canuint8 _c[8];
	/*Rx ID: 0xfc*/
	_c_ORC_A2_RDS_msgType	orc_a2;
}_c_RDS28_0_buf;

typedef union {
	canuint8 _c[8];
}_c_RDSBasic_1_buf;

typedef union {
	canuint8 _c[8];
	/*Rx ID: 0x1e7ca0fe*/
	_c_APPL_ECU_BCM_1_RDS_msgType	appl_ecu_bcm_1;
}_c_RDS4_1_buf;

typedef union {
	canuint8 _c[8];
	/*Rx ID: 0x77e*/
	_c_TRIP_A_B_RDS_msgType	trip_a_b;
}_c_RDS5_1_buf;

typedef union {
	canuint8 _c[8];
	/*Rx ID: 0x18da40f1*/
	_c_DIAGNOSTIC_REQUEST_BCM_1_RDS_msgType	diagnostic_request_bcm_1;
}_c_RDS6_1_buf;

typedef union {
	canuint8 _c[8];
	/*Rx ID: 0x5e6*/
	_c_TGW_A1_RDS_msgType	tgw_a1;
}_c_RDS7_1_buf;

typedef union {
	canuint8 _c[8];
	/*Rx ID: 0x5e2*/
	_c_ICS_MSG_RDS_msgType	ics_msg;
}_c_RDS8_1_buf;

typedef union {
	canuint8 _c[8];
	/*Rx ID: 0x5de*/
	_c_HVAC_A1_RDS_msgType	hvac_a1;
}_c_RDS9_1_buf;

typedef union {
	canuint8 _c[8];
	/*Rx ID: 0x5d2*/
	_c_DIRECT_INFO_RDS_msgType	direct_info;
}_c_RDS10_1_buf;

typedef union {
	canuint8 _c[8];
	/*Rx ID: 0x546*/
	_c_HVAC_A4_RDS_msgType	hvac_a4;
}_c_RDS11_1_buf;

typedef union {
	canuint8 _c[8];
	/*Rx ID: 0x49f*/
	_c_STATUS_RRM_RDS_msgType	status_rrm;
}_c_RDS12_1_buf;

typedef union {
	canuint8 _c[8];
	/*Rx ID: 0x49d*/
	_c_STATUS_IPC_RDS_msgType	status_ipc;
}_c_RDS13_1_buf;

typedef union {
	canuint8 _c[8];
	/*Rx ID: 0x49b*/
	_c_STATUS_ECC_RDS_msgType	status_ecc;
}_c_RDS14_1_buf;

typedef union {
	canuint8 _c[8];
	/*Rx ID: 0x495*/
	_c_HVAC_A2_RDS_msgType	hvac_a2;
}_c_RDS15_1_buf;

typedef union {
	canuint8 _c[8];
	/*Rx ID: 0x489*/
	_c_PLG_A1_RDS_msgType	plg_a1;
}_c_RDS16_1_buf;

typedef union {
	canuint8 _c[8];
	/*Rx ID: 0x3cd*/
	_c_ECC_A1_RDS_msgType	ecc_a1;
}_c_RDS17_1_buf;

typedef union {
	canuint8 _c[8];
	/*Rx ID: 0xe094034*/
	_c_NWM_RBSS_RDS_msgType	nwm_rbss;
}_c_RDS18_1_buf;

typedef union {
	canuint8 _c[8];
	/*Rx ID: 0xe094033*/
	_c_NWM_LBSS_RDS_msgType	nwm_lbss;
}_c_RDS19_1_buf;

typedef union {
	canuint8 _c[8];
	/*Rx ID: 0xe094032*/
	_c_NWM_ICS_RDS_msgType	nwm_ics;
}_c_RDS20_1_buf;

typedef union {
	canuint8 _c[8];
	/*Rx ID: 0xe094030*/
	_c_NWM_CSWM_RDS_msgType	nwm_cswm;
}_c_RDS21_1_buf;

typedef union {
	canuint8 _c[8];
	/*Rx ID: 0xe09402f*/
	_c_NWM_CDM_RDS_msgType	nwm_cdm;
}_c_RDS22_1_buf;

typedef union {
	canuint8 _c[8];
	/*Rx ID: 0xe09402e*/
	_c_NWM_AMP_RDS_msgType	nwm_amp;
}_c_RDS23_1_buf;

typedef union {
	canuint8 _c[8];
	/*Rx ID: 0xe09400a*/
	_c_NWM_ECC_RDS_msgType	nwm_ecc;
}_c_RDS24_1_buf;

typedef union {
	canuint8 _c[8];
	/*Rx ID: 0xe094003*/
	_c_NWM_IPC_RDS_msgType	nwm_ipc;
}_c_RDS25_1_buf;

typedef union {
	canuint8 _c[8];
	/*Rx ID: 0x190*/
	_c_CBC_I4_RDS_msgType	cbc_i4;
}_c_RDS26_1_buf;

typedef union {
	canuint8 _c[8];
	/*Rx ID: 0x8c*/
	_c_CMCM_UNLK_RDS_msgType	cmcm_unlk;
}_c_RDS27_1_buf;

typedef union {
	canuint8 _c[8];
	/*Rx ID: 0x8a*/
	_c_CFG_RQ_RDS_msgType	cfg_rq;
}_c_RDS28_1_buf;

/*************************************************************/
/* Signal groups                                             */
/*************************************************************/

/*************************************************************/
/* Message masks (receive messages)                      */
/*************************************************************/


#define NETC_TX_c1_STATUS_C_OCM_Mask 0x7
#define NETC_TX_c2_STATUS_C_OCM_Mask 0x0
#define NETC_TX_c3_STATUS_C_OCM_Mask 0x0

#define NETC_TX_c1_CFG_DATA_CODE_RSP_OCM_Mask 0xff
#define NETC_TX_c2_CFG_DATA_CODE_RSP_OCM_Mask 0xff
#define NETC_TX_c3_CFG_DATA_CODE_RSP_OCM_Mask 0xff
#define NETC_TX_c4_CFG_DATA_CODE_RSP_OCM_Mask 0xff
#define NETC_TX_c5_CFG_DATA_CODE_RSP_OCM_Mask 0xff
#define NETC_TX_c6_CFG_DATA_CODE_RSP_OCM_Mask 0xf0

#define NETC_TX_c1_CFG_DATA_CODE_RSP_AHLM_Mask 0xff
#define NETC_TX_c2_CFG_DATA_CODE_RSP_AHLM_Mask 0xff
#define NETC_TX_c3_CFG_DATA_CODE_RSP_AHLM_Mask 0xff
#define NETC_TX_c4_CFG_DATA_CODE_RSP_AHLM_Mask 0xff
#define NETC_TX_c5_CFG_DATA_CODE_RSP_AHLM_Mask 0xff
#define NETC_TX_c6_CFG_DATA_CODE_RSP_AHLM_Mask 0xf0

#define NETC_TX_c1_CFG_DATA_CODE_RSP_DTCM_Mask 0xff
#define NETC_TX_c2_CFG_DATA_CODE_RSP_DTCM_Mask 0xff
#define NETC_TX_c3_CFG_DATA_CODE_RSP_DTCM_Mask 0xff
#define NETC_TX_c4_CFG_DATA_CODE_RSP_DTCM_Mask 0xff
#define NETC_TX_c5_CFG_DATA_CODE_RSP_DTCM_Mask 0xff
#define NETC_TX_c6_CFG_DATA_CODE_RSP_DTCM_Mask 0xf0

#define NETC_TX_c1_CFG_DATA_CODE_RSP_RFHM_Mask 0xff
#define NETC_TX_c2_CFG_DATA_CODE_RSP_RFHM_Mask 0xff
#define NETC_TX_c3_CFG_DATA_CODE_RSP_RFHM_Mask 0xff
#define NETC_TX_c4_CFG_DATA_CODE_RSP_RFHM_Mask 0xff
#define NETC_TX_c5_CFG_DATA_CODE_RSP_RFHM_Mask 0xff
#define NETC_TX_c6_CFG_DATA_CODE_RSP_RFHM_Mask 0xf0

#define NETC_TX_c1_CFG_DATA_CODE_RSP_DASM_Mask 0xff
#define NETC_TX_c2_CFG_DATA_CODE_RSP_DASM_Mask 0xff
#define NETC_TX_c3_CFG_DATA_CODE_RSP_DASM_Mask 0xff
#define NETC_TX_c4_CFG_DATA_CODE_RSP_DASM_Mask 0xff
#define NETC_TX_c5_CFG_DATA_CODE_RSP_DASM_Mask 0xff
#define NETC_TX_c6_CFG_DATA_CODE_RSP_DASM_Mask 0xf0

#define NETC_TX_c1_CFG_DATA_CODE_RSP_SCCM_Mask 0xff
#define NETC_TX_c2_CFG_DATA_CODE_RSP_SCCM_Mask 0xff
#define NETC_TX_c3_CFG_DATA_CODE_RSP_SCCM_Mask 0xff
#define NETC_TX_c4_CFG_DATA_CODE_RSP_SCCM_Mask 0xff
#define NETC_TX_c5_CFG_DATA_CODE_RSP_SCCM_Mask 0xff
#define NETC_TX_c6_CFG_DATA_CODE_RSP_SCCM_Mask 0xf0

#define NETC_TX_c1_CFG_DATA_CODE_RSP_HALF_Mask 0xff
#define NETC_TX_c2_CFG_DATA_CODE_RSP_HALF_Mask 0xff
#define NETC_TX_c3_CFG_DATA_CODE_RSP_HALF_Mask 0xff
#define NETC_TX_c4_CFG_DATA_CODE_RSP_HALF_Mask 0xff
#define NETC_TX_c5_CFG_DATA_CODE_RSP_HALF_Mask 0xff
#define NETC_TX_c6_CFG_DATA_CODE_RSP_HALF_Mask 0xf0

#define NETC_TX_c1_CFG_DATA_CODE_RSP_ORC_Mask 0xff
#define NETC_TX_c2_CFG_DATA_CODE_RSP_ORC_Mask 0xff
#define NETC_TX_c3_CFG_DATA_CODE_RSP_ORC_Mask 0xff
#define NETC_TX_c4_CFG_DATA_CODE_RSP_ORC_Mask 0xff
#define NETC_TX_c5_CFG_DATA_CODE_RSP_ORC_Mask 0xff
#define NETC_TX_c6_CFG_DATA_CODE_RSP_ORC_Mask 0xf0

#define NETC_TX_c1_CFG_DATA_CODE_RSP_PAM_Mask 0xff
#define NETC_TX_c2_CFG_DATA_CODE_RSP_PAM_Mask 0xff
#define NETC_TX_c3_CFG_DATA_CODE_RSP_PAM_Mask 0xff
#define NETC_TX_c4_CFG_DATA_CODE_RSP_PAM_Mask 0xff
#define NETC_TX_c5_CFG_DATA_CODE_RSP_PAM_Mask 0xff
#define NETC_TX_c6_CFG_DATA_CODE_RSP_PAM_Mask 0xf0

#define NETC_TX_c1_CFG_DATA_CODE_RSP_EPB_Mask 0xff
#define NETC_TX_c2_CFG_DATA_CODE_RSP_EPB_Mask 0xff
#define NETC_TX_c3_CFG_DATA_CODE_RSP_EPB_Mask 0xff
#define NETC_TX_c4_CFG_DATA_CODE_RSP_EPB_Mask 0xff
#define NETC_TX_c5_CFG_DATA_CODE_RSP_EPB_Mask 0xff
#define NETC_TX_c6_CFG_DATA_CODE_RSP_EPB_Mask 0xf0

#define NETC_TX_c1_CFG_DATA_CODE_RSP_TRANSMISSION_Mask 0xff
#define NETC_TX_c2_CFG_DATA_CODE_RSP_TRANSMISSION_Mask 0xff
#define NETC_TX_c3_CFG_DATA_CODE_RSP_TRANSMISSION_Mask 0xff
#define NETC_TX_c4_CFG_DATA_CODE_RSP_TRANSMISSION_Mask 0xff
#define NETC_TX_c5_CFG_DATA_CODE_RSP_TRANSMISSION_Mask 0xff
#define NETC_TX_c6_CFG_DATA_CODE_RSP_TRANSMISSION_Mask 0xf0

#define NETC_TX_c1_CFG_DATA_CODE_RSP_BSM_Mask 0xff
#define NETC_TX_c2_CFG_DATA_CODE_RSP_BSM_Mask 0xff
#define NETC_TX_c3_CFG_DATA_CODE_RSP_BSM_Mask 0xff
#define NETC_TX_c4_CFG_DATA_CODE_RSP_BSM_Mask 0xff
#define NETC_TX_c5_CFG_DATA_CODE_RSP_BSM_Mask 0xff
#define NETC_TX_c6_CFG_DATA_CODE_RSP_BSM_Mask 0xf0

#define NETC_TX_c1_CFG_DATA_CODE_RSP_EPS_Mask 0xff
#define NETC_TX_c2_CFG_DATA_CODE_RSP_EPS_Mask 0xff
#define NETC_TX_c3_CFG_DATA_CODE_RSP_EPS_Mask 0xff
#define NETC_TX_c4_CFG_DATA_CODE_RSP_EPS_Mask 0xff
#define NETC_TX_c5_CFG_DATA_CODE_RSP_EPS_Mask 0xff
#define NETC_TX_c6_CFG_DATA_CODE_RSP_EPS_Mask 0xf0

#define NETC_TX_c1_CFG_DATA_CODE_RSP_ECM_Mask 0xff
#define NETC_TX_c2_CFG_DATA_CODE_RSP_ECM_Mask 0xff
#define NETC_TX_c3_CFG_DATA_CODE_RSP_ECM_Mask 0xff
#define NETC_TX_c4_CFG_DATA_CODE_RSP_ECM_Mask 0xff
#define NETC_TX_c5_CFG_DATA_CODE_RSP_ECM_Mask 0xff
#define NETC_TX_c6_CFG_DATA_CODE_RSP_ECM_Mask 0xf0

#define NETC_TX_c1_STATUS_C_DASM_Mask 0x7
#define NETC_TX_c2_STATUS_C_DASM_Mask 0xc0

#define NETC_TX_c1_STATUS_C_AHLM_Mask 0x7
#define NETC_TX_c2_STATUS_C_AHLM_Mask 0x0

#define NETC_TX_c1_DIAGNOSTIC_REQUEST_BCM_0_Mask 0xff
#define NETC_TX_c2_DIAGNOSTIC_REQUEST_BCM_0_Mask 0x0
#define NETC_TX_c3_DIAGNOSTIC_REQUEST_BCM_0_Mask 0x0
#define NETC_TX_c4_DIAGNOSTIC_REQUEST_BCM_0_Mask 0x0
#define NETC_TX_c5_DIAGNOSTIC_REQUEST_BCM_0_Mask 0x0
#define NETC_TX_c6_DIAGNOSTIC_REQUEST_BCM_0_Mask 0x0
#define NETC_TX_c7_DIAGNOSTIC_REQUEST_BCM_0_Mask 0x0
#define NETC_TX_c8_DIAGNOSTIC_REQUEST_BCM_0_Mask 0x0

#define NETC_TX_c1_STATUS_C_HALF_Mask 0x7
#define NETC_TX_c2_STATUS_C_HALF_Mask 0x0
#define NETC_TX_c3_STATUS_C_HALF_Mask 0x7
#define NETC_TX_c4_STATUS_C_HALF_Mask 0xfc
#define NETC_TX_c5_STATUS_C_HALF_Mask 0x0
#define NETC_TX_c6_STATUS_C_HALF_Mask 0x0
#define NETC_TX_c7_STATUS_C_HALF_Mask 0x0
#define NETC_TX_c8_STATUS_C_HALF_Mask 0x0

#define NETC_TX_c1_STATUS_C_ESL_Mask 0x7
#define NETC_TX_c2_STATUS_C_ESL_Mask 0x78

#define NETC_TX_c1_STATUS_C_EPB_Mask 0x7
#define NETC_TX_c2_STATUS_C_EPB_Mask 0x0
#define NETC_TX_c3_STATUS_C_EPB_Mask 0x3
#define NETC_TX_c4_STATUS_C_EPB_Mask 0x7f
#define NETC_TX_c5_STATUS_C_EPB_Mask 0x1f
#define NETC_TX_c6_STATUS_C_EPB_Mask 0xf0
#define NETC_TX_c7_STATUS_C_EPB_Mask 0x0

#define NETC_TX_c1_HALF_C_Warning_RQ_Mask 0x3
#define NETC_TX_c2_HALF_C_Warning_RQ_Mask 0xff

#define NETC_TX_c1_StW_Actn_Rq_0_Mask 0x33
#define NETC_TX_c2_StW_Actn_Rq_0_Mask 0xc3
#define NETC_TX_c3_StW_Actn_Rq_0_Mask 0xf
#define NETC_TX_c4_StW_Actn_Rq_0_Mask 0x0
#define NETC_TX_c5_StW_Actn_Rq_0_Mask 0x0
#define NETC_TX_c6_StW_Actn_Rq_0_Mask 0x0
#define NETC_TX_c7_StW_Actn_Rq_0_Mask 0x0

#define NETC_TX_c1_STATUS_C_TRANSMISSION_Mask 0x7
#define NETC_TX_c2_STATUS_C_TRANSMISSION_Mask 0x7f
#define NETC_TX_c3_STATUS_C_TRANSMISSION_Mask 0xf
#define NETC_TX_c4_STATUS_C_TRANSMISSION_Mask 0x0
#define NETC_TX_c5_STATUS_C_TRANSMISSION_Mask 0x0
#define NETC_TX_c6_STATUS_C_TRANSMISSION_Mask 0x0
#define NETC_TX_c7_STATUS_C_TRANSMISSION_Mask 0x0
#define NETC_TX_c8_STATUS_C_TRANSMISSION_Mask 0x0

#define NETC_TX_c1_VIN_0_Mask 0x3
#define NETC_TX_c2_VIN_0_Mask 0xff
#define NETC_TX_c3_VIN_0_Mask 0xff
#define NETC_TX_c4_VIN_0_Mask 0xff
#define NETC_TX_c5_VIN_0_Mask 0xff
#define NETC_TX_c6_VIN_0_Mask 0xff
#define NETC_TX_c7_VIN_0_Mask 0xff
#define NETC_TX_c8_VIN_0_Mask 0xff

#define NETC_TX_c1_MOT3_Mask 0x3f
#define NETC_TX_c2_MOT3_Mask 0xe0
#define NETC_TX_c3_MOT3_Mask 0xff
#define NETC_TX_c4_MOT3_Mask 0x0
#define NETC_TX_c5_MOT3_Mask 0x39
#define NETC_TX_c6_MOT3_Mask 0x0
#define NETC_TX_c7_MOT3_Mask 0x0
#define NETC_TX_c8_MOT3_Mask 0x0

#define NETC_TX_c1_ECM_1_Mask 0x0
#define NETC_TX_c2_ECM_1_Mask 0x4
#define NETC_TX_c3_ECM_1_Mask 0x0
#define NETC_TX_c4_ECM_1_Mask 0x0
#define NETC_TX_c5_ECM_1_Mask 0x0
#define NETC_TX_c6_ECM_1_Mask 0x18
#define NETC_TX_c7_ECM_1_Mask 0x0
#define NETC_TX_c8_ECM_1_Mask 0x0

#define NETC_TX_c1_STATUS_C_DTCM_Mask 0x7
#define NETC_TX_c2_STATUS_C_DTCM_Mask 0x3
#define NETC_TX_c3_STATUS_C_DTCM_Mask 0x3c
#define NETC_TX_c4_STATUS_C_DTCM_Mask 0xff
#define NETC_TX_c5_STATUS_C_DTCM_Mask 0xff
#define NETC_TX_c6_STATUS_C_DTCM_Mask 0xe0
#define NETC_TX_c7_STATUS_C_DTCM_Mask 0x0
#define NETC_TX_c8_STATUS_C_DTCM_Mask 0x0

#define NETC_TX_c1_DAS_A2_Mask 0x1
#define NETC_TX_c2_DAS_A2_Mask 0x80
#define NETC_TX_c3_DAS_A2_Mask 0x0
#define NETC_TX_c4_DAS_A2_Mask 0x0
#define NETC_TX_c5_DAS_A2_Mask 0x0
#define NETC_TX_c6_DAS_A2_Mask 0x0
#define NETC_TX_c7_DAS_A2_Mask 0x0

#define NETC_TX_c1_PAM_2_Mask 0x0
#define NETC_TX_c2_PAM_2_Mask 0x0
#define NETC_TX_c3_PAM_2_Mask 0x3
#define NETC_TX_c4_PAM_2_Mask 0x1
#define NETC_TX_c5_PAM_2_Mask 0x1

#define NETC_TX_c1_EPB_A1_Mask 0xe0
#define NETC_TX_c2_EPB_A1_Mask 0x0
#define NETC_TX_c3_EPB_A1_Mask 0x0

#define NETC_TX_c1_DAS_A1_Mask 0x0
#define NETC_TX_c2_DAS_A1_Mask 0x0
#define NETC_TX_c3_DAS_A1_Mask 0xc
#define NETC_TX_c4_DAS_A1_Mask 0x0
#define NETC_TX_c5_DAS_A1_Mask 0x4
#define NETC_TX_c6_DAS_A1_Mask 0x1
#define NETC_TX_c7_DAS_A1_Mask 0x0
#define NETC_TX_c8_DAS_A1_Mask 0x0

#define NETC_TX_c1_BSM_4_Mask 0x80
#define NETC_TX_c2_BSM_4_Mask 0x18
#define NETC_TX_c3_BSM_4_Mask 0x0
#define NETC_TX_c4_BSM_4_Mask 0x0
#define NETC_TX_c5_BSM_4_Mask 0xf
#define NETC_TX_c6_BSM_4_Mask 0x0
#define NETC_TX_c7_BSM_4_Mask 0x30
#define NETC_TX_c8_BSM_4_Mask 0x0

#define NETC_TX_c1_BSM_2_Mask 0x0
#define NETC_TX_c2_BSM_2_Mask 0x0
#define NETC_TX_c3_BSM_2_Mask 0x0
#define NETC_TX_c4_BSM_2_Mask 0x0
#define NETC_TX_c5_BSM_2_Mask 0x0
#define NETC_TX_c6_BSM_2_Mask 0xc
#define NETC_TX_c7_BSM_2_Mask 0x0
#define NETC_TX_c8_BSM_2_Mask 0x0

#define NETC_TX_c1_DTCM_A1_Mask 0x0
#define NETC_TX_c2_DTCM_A1_Mask 0x0
#define NETC_TX_c3_DTCM_A1_Mask 0x0
#define NETC_TX_c4_DTCM_A1_Mask 0x0
#define NETC_TX_c5_DTCM_A1_Mask 0x0
#define NETC_TX_c6_DTCM_A1_Mask 0x38
#define NETC_TX_c7_DTCM_A1_Mask 0x0
#define NETC_TX_c8_DTCM_A1_Mask 0x0

#define NETC_TX_c1_SC_Mask 0x2
#define NETC_TX_c2_SC_Mask 0x38
#define NETC_TX_c3_SC_Mask 0x0
#define NETC_TX_c4_SC_Mask 0x0

#define NETC_TX_c1_ORC_YRS_DATA_Mask 0xf
#define NETC_TX_c2_ORC_YRS_DATA_Mask 0xff
#define NETC_TX_c3_ORC_YRS_DATA_Mask 0x0
#define NETC_TX_c4_ORC_YRS_DATA_Mask 0x0
#define NETC_TX_c5_ORC_YRS_DATA_Mask 0xff
#define NETC_TX_c6_ORC_YRS_DATA_Mask 0xf0
#define NETC_TX_c7_ORC_YRS_DATA_Mask 0x0

#define NETC_TX_c1_MOTGEAR2_Mask 0x0
#define NETC_TX_c2_MOTGEAR2_Mask 0x0
#define NETC_TX_c3_MOTGEAR2_Mask 0xf0
#define NETC_TX_c4_MOTGEAR2_Mask 0x0
#define NETC_TX_c5_MOTGEAR2_Mask 0x0

#define NETC_TX_c1_MOT4_Mask 0x0
#define NETC_TX_c2_MOT4_Mask 0x0
#define NETC_TX_c3_MOT4_Mask 0x0
#define NETC_TX_c4_MOT4_Mask 0x0
#define NETC_TX_c5_MOT4_Mask 0x8
#define NETC_TX_c6_MOT4_Mask 0x0
#define NETC_TX_c7_MOT4_Mask 0x70
#define NETC_TX_c8_MOT4_Mask 0x0

#define NETC_TX_c1_BSM_YRS_DATA_Mask 0xff
#define NETC_TX_c2_BSM_YRS_DATA_Mask 0x0
#define NETC_TX_c3_BSM_YRS_DATA_Mask 0xff
#define NETC_TX_c4_BSM_YRS_DATA_Mask 0x0

#define NETC_TX_c1_ASR4_Mask 0x3f
#define NETC_TX_c2_ASR4_Mask 0xf
#define NETC_TX_c3_ASR4_Mask 0xf
#define NETC_TX_c4_ASR4_Mask 0x3f
#define NETC_TX_c5_ASR4_Mask 0x3f
#define NETC_TX_c6_ASR4_Mask 0x0
#define NETC_TX_c7_ASR4_Mask 0x0
#define NETC_TX_c8_ASR4_Mask 0x0

#define NETC_TX_c1_ASR3_Mask 0x0
#define NETC_TX_c2_ASR3_Mask 0x0
#define NETC_TX_c3_ASR3_Mask 0x80
#define NETC_TX_c4_ASR3_Mask 0x0
#define NETC_TX_c5_ASR3_Mask 0x1f
#define NETC_TX_c6_ASR3_Mask 0xff
#define NETC_TX_c7_ASR3_Mask 0x0
#define NETC_TX_c8_ASR3_Mask 0x0

#define NETC_TX_c1_RFHUB_A4_Mask 0xf8

#define NETC_TX_c1_IPC_CFG_Feature_Mask 0xff
#define NETC_TX_c2_IPC_CFG_Feature_Mask 0x3
#define NETC_TX_c3_IPC_CFG_Feature_Mask 0xff

#define NETC_TX_c1_ESL_CODE_RESPONSE_Mask 0xff
#define NETC_TX_c2_ESL_CODE_RESPONSE_Mask 0xff
#define NETC_TX_c3_ESL_CODE_RESPONSE_Mask 0xff
#define NETC_TX_c4_ESL_CODE_RESPONSE_Mask 0x6f
#define NETC_TX_c5_ESL_CODE_RESPONSE_Mask 0x1

#define NETC_TX_c1_IMMO_CODE_REQUEST_Mask 0xff
#define NETC_TX_c2_IMMO_CODE_REQUEST_Mask 0xff
#define NETC_TX_c3_IMMO_CODE_REQUEST_Mask 0xff
#define NETC_TX_c4_IMMO_CODE_REQUEST_Mask 0xff
#define NETC_TX_c5_IMMO_CODE_REQUEST_Mask 0xff
#define NETC_TX_c6_IMMO_CODE_REQUEST_Mask 0xff
#define NETC_TX_c7_IMMO_CODE_REQUEST_Mask 0xff

#define NETC_TX_c1_TRM_MKP_KEY_Mask 0xff
#define NETC_TX_c2_TRM_MKP_KEY_Mask 0xff
#define NETC_TX_c3_TRM_MKP_KEY_Mask 0xff
#define NETC_TX_c4_TRM_MKP_KEY_Mask 0xff
#define NETC_TX_c5_TRM_MKP_KEY_Mask 0xff
#define NETC_TX_c6_TRM_MKP_KEY_Mask 0xff
#define NETC_TX_c7_TRM_MKP_KEY_Mask 0xff
#define NETC_TX_c8_TRM_MKP_KEY_Mask 0xff

#define NETC_TX_c1_RFHUB_A3_Mask 0x1
#define NETC_TX_c2_RFHUB_A3_Mask 0x0

#define NETC_TX_c1_APPL_ECU_BCM_0_Mask 0xff
#define NETC_TX_c2_APPL_ECU_BCM_0_Mask 0xff
#define NETC_TX_c3_APPL_ECU_BCM_0_Mask 0xff
#define NETC_TX_c4_APPL_ECU_BCM_0_Mask 0xff
#define NETC_TX_c5_APPL_ECU_BCM_0_Mask 0xff
#define NETC_TX_c6_APPL_ECU_BCM_0_Mask 0xff
#define NETC_TX_c7_APPL_ECU_BCM_0_Mask 0xff
#define NETC_TX_c8_APPL_ECU_BCM_0_Mask 0xff

#define NETC_TX_c1_STATUS_C_SCCM_Mask 0x7
#define NETC_TX_c2_STATUS_C_SCCM_Mask 0x0
#define NETC_TX_c3_STATUS_C_SCCM_Mask 0x0
#define NETC_TX_c4_STATUS_C_SCCM_Mask 0x0

#define NETC_TX_c1_STATUS_C_RFHM_Mask 0x7
#define NETC_TX_c2_STATUS_C_RFHM_Mask 0xc0
#define NETC_TX_c3_STATUS_C_RFHM_Mask 0x0
#define NETC_TX_c4_STATUS_C_RFHM_Mask 0x0

#define NETC_TX_c1_IPC_A1_Mask 0x80
#define NETC_TX_c2_IPC_A1_Mask 0x0
#define NETC_TX_c3_IPC_A1_Mask 0x0
#define NETC_TX_c4_IPC_A1_Mask 0x0

#define NETC_TX_c1_STATUS_C_SPM_Mask 0x7
#define NETC_TX_c2_STATUS_C_SPM_Mask 0x0
#define NETC_TX_c3_STATUS_C_SPM_Mask 0x0
#define NETC_TX_c4_STATUS_C_SPM_Mask 0x0
#define NETC_TX_c5_STATUS_C_SPM_Mask 0x0
#define NETC_TX_c6_STATUS_C_SPM_Mask 0x0
#define NETC_TX_c7_STATUS_C_SPM_Mask 0x6
#define NETC_TX_c8_STATUS_C_SPM_Mask 0x0

#define NETC_TX_c1_STATUS_C_IPC_Mask 0x7
#define NETC_TX_c2_STATUS_C_IPC_Mask 0x0
#define NETC_TX_c3_STATUS_C_IPC_Mask 0x0
#define NETC_TX_c4_STATUS_C_IPC_Mask 0x0
#define NETC_TX_c5_STATUS_C_IPC_Mask 0x0
#define NETC_TX_c6_STATUS_C_IPC_Mask 0x0
#define NETC_TX_c7_STATUS_C_IPC_Mask 0x0
#define NETC_TX_c8_STATUS_C_IPC_Mask 0x0

#define NETC_TX_c1_STATUS_C_GSM_Mask 0x7
#define NETC_TX_c2_STATUS_C_GSM_Mask 0x0

#define NETC_TX_c1_STATUS_C_EPS_Mask 0x7
#define NETC_TX_c2_STATUS_C_EPS_Mask 0x0

#define NETC_TX_c1_STATUS_C_BSM_Mask 0x7
#define NETC_TX_c2_STATUS_C_BSM_Mask 0x40
#define NETC_TX_c3_STATUS_C_BSM_Mask 0x7f
#define NETC_TX_c4_STATUS_C_BSM_Mask 0xff
#define NETC_TX_c5_STATUS_C_BSM_Mask 0xff
#define NETC_TX_c6_STATUS_C_BSM_Mask 0xff
#define NETC_TX_c7_STATUS_C_BSM_Mask 0x9f
#define NETC_TX_c8_STATUS_C_BSM_Mask 0xff

#define NETC_TX_c1_STATUS_C_ECM2_Mask 0x0
#define NETC_TX_c2_STATUS_C_ECM2_Mask 0x0
#define NETC_TX_c3_STATUS_C_ECM2_Mask 0x0
#define NETC_TX_c4_STATUS_C_ECM2_Mask 0x0
#define NETC_TX_c5_STATUS_C_ECM2_Mask 0x0
#define NETC_TX_c6_STATUS_C_ECM2_Mask 0xc
#define NETC_TX_c7_STATUS_C_ECM2_Mask 0x79
#define NETC_TX_c8_STATUS_C_ECM2_Mask 0x1

#define NETC_TX_c1_STATUS_C_ECM_Mask 0x7
#define NETC_TX_c2_STATUS_C_ECM_Mask 0xb5
#define NETC_TX_c3_STATUS_C_ECM_Mask 0xdd
#define NETC_TX_c4_STATUS_C_ECM_Mask 0xff
#define NETC_TX_c5_STATUS_C_ECM_Mask 0x0
#define NETC_TX_c6_STATUS_C_ECM_Mask 0x0
#define NETC_TX_c7_STATUS_C_ECM_Mask 0xff
#define NETC_TX_c8_STATUS_C_ECM_Mask 0xa4

#define NETC_TX_c1_STATUS_C_ORC_Mask 0x7
#define NETC_TX_c2_STATUS_C_ORC_Mask 0x0
#define NETC_TX_c3_STATUS_C_ORC_Mask 0x0
#define NETC_TX_c4_STATUS_C_ORC_Mask 0x0
#define NETC_TX_c5_STATUS_C_ORC_Mask 0x0
#define NETC_TX_c6_STATUS_C_ORC_Mask 0x0
#define NETC_TX_c7_STATUS_C_ORC_Mask 0x0
#define NETC_TX_c8_STATUS_C_ORC_Mask 0x0

#define NETC_TX_c1_RFHUB_A2_Mask 0xff
#define NETC_TX_c2_RFHUB_A2_Mask 0xe0
#define NETC_TX_c3_RFHUB_A2_Mask 0xf0
#define NETC_TX_c4_RFHUB_A2_Mask 0xff
#define NETC_TX_c5_RFHUB_A2_Mask 0x0

#define NETC_TX_c1_RFHUB_A1_Mask 0x7f

#define NETC_TX_c1_PAM_1_Mask 0x86
#define NETC_TX_c2_PAM_1_Mask 0xff
#define NETC_TX_c3_PAM_1_Mask 0xf
#define NETC_TX_c4_PAM_1_Mask 0x33
#define NETC_TX_c5_PAM_1_Mask 0x7f
#define NETC_TX_c6_PAM_1_Mask 0xc0
#define NETC_TX_c7_PAM_1_Mask 0x0
#define NETC_TX_c8_PAM_1_Mask 0x0

#define NETC_TX_c1_WAKE_C_RFHM_Mask 0xff
#define NETC_TX_c2_WAKE_C_RFHM_Mask 0xff
#define NETC_TX_c3_WAKE_C_RFHM_Mask 0xff
#define NETC_TX_c4_WAKE_C_RFHM_Mask 0x0

#define NETC_TX_c1_WAKE_C_SCCM_Mask 0xff
#define NETC_TX_c2_WAKE_C_SCCM_Mask 0xff
#define NETC_TX_c3_WAKE_C_SCCM_Mask 0xff
#define NETC_TX_c4_WAKE_C_SCCM_Mask 0x0

#define NETC_TX_c1_WAKE_C_ESL_Mask 0xff
#define NETC_TX_c2_WAKE_C_ESL_Mask 0xff
#define NETC_TX_c3_WAKE_C_ESL_Mask 0xff
#define NETC_TX_c4_WAKE_C_ESL_Mask 0x0

#define NETC_TX_c1_WAKE_C_EPB_Mask 0xff
#define NETC_TX_c2_WAKE_C_EPB_Mask 0xff
#define NETC_TX_c3_WAKE_C_EPB_Mask 0xff
#define NETC_TX_c4_WAKE_C_EPB_Mask 0x0

#define NETC_TX_c1_WAKE_C_BSM_Mask 0xff
#define NETC_TX_c2_WAKE_C_BSM_Mask 0xff
#define NETC_TX_c3_WAKE_C_BSM_Mask 0xff
#define NETC_TX_c4_WAKE_C_BSM_Mask 0x0

#define NETC_TX_c1_MOT1_Mask 0x0
#define NETC_TX_c2_MOT1_Mask 0x0
#define NETC_TX_c3_MOT1_Mask 0xff
#define NETC_TX_c4_MOT1_Mask 0x0
#define NETC_TX_c5_MOT1_Mask 0x10
#define NETC_TX_c6_MOT1_Mask 0x0
#define NETC_TX_c7_MOT1_Mask 0x0
#define NETC_TX_c8_MOT1_Mask 0x0

#define NETC_TX_c1_GE_Mask 0xff
#define NETC_TX_c2_GE_Mask 0xff
#define NETC_TX_c3_GE_Mask 0x20
#define NETC_TX_c4_GE_Mask 0x0
#define NETC_TX_c5_GE_Mask 0x60
#define NETC_TX_c6_GE_Mask 0x0

#define NETC_TX_c1_TRM_CODE_RESPONSE_Mask 0xff
#define NETC_TX_c2_TRM_CODE_RESPONSE_Mask 0xff
#define NETC_TX_c3_TRM_CODE_RESPONSE_Mask 0xff
#define NETC_TX_c4_TRM_CODE_RESPONSE_Mask 0xff
#define NETC_TX_c5_TRM_CODE_RESPONSE_Mask 0x0
#define NETC_TX_c6_TRM_CODE_RESPONSE_Mask 0x0

#define NETC_TX_c1_ORC_A2_Mask 0x80
#define NETC_TX_c2_ORC_A2_Mask 0x80

#define NETC_TX_c1_CRO_BCM_Mask 0xff
#define NETC_TX_c2_CRO_BCM_Mask 0xff
#define NETC_TX_c3_CRO_BCM_Mask 0xff
#define NETC_TX_c4_CRO_BCM_Mask 0xff
#define NETC_TX_c5_CRO_BCM_Mask 0xff
#define NETC_TX_c6_CRO_BCM_Mask 0xff
#define NETC_TX_c7_CRO_BCM_Mask 0xff
#define NETC_TX_c8_CRO_BCM_Mask 0xff

#define NETC_TX_c1_CFG_DATA_CODE_RSP_RBSS_Mask 0xff
#define NETC_TX_c2_CFG_DATA_CODE_RSP_RBSS_Mask 0xff
#define NETC_TX_c3_CFG_DATA_CODE_RSP_RBSS_Mask 0xff
#define NETC_TX_c4_CFG_DATA_CODE_RSP_RBSS_Mask 0xff
#define NETC_TX_c5_CFG_DATA_CODE_RSP_RBSS_Mask 0xff
#define NETC_TX_c6_CFG_DATA_CODE_RSP_RBSS_Mask 0xf0

#define NETC_TX_c1_CFG_DATA_CODE_RSP_LBSS_Mask 0xff
#define NETC_TX_c2_CFG_DATA_CODE_RSP_LBSS_Mask 0xff
#define NETC_TX_c3_CFG_DATA_CODE_RSP_LBSS_Mask 0xff
#define NETC_TX_c4_CFG_DATA_CODE_RSP_LBSS_Mask 0xff
#define NETC_TX_c5_CFG_DATA_CODE_RSP_LBSS_Mask 0xff
#define NETC_TX_c6_CFG_DATA_CODE_RSP_LBSS_Mask 0xf0

#define NETC_TX_c1_CFG_DATA_CODE_RSP_ICS_Mask 0xff
#define NETC_TX_c2_CFG_DATA_CODE_RSP_ICS_Mask 0xff
#define NETC_TX_c3_CFG_DATA_CODE_RSP_ICS_Mask 0xff
#define NETC_TX_c4_CFG_DATA_CODE_RSP_ICS_Mask 0xff
#define NETC_TX_c5_CFG_DATA_CODE_RSP_ICS_Mask 0xff
#define NETC_TX_c6_CFG_DATA_CODE_RSP_ICS_Mask 0xf0

#define NETC_TX_c1_CFG_DATA_CODE_RSP_CSWM_Mask 0xff
#define NETC_TX_c2_CFG_DATA_CODE_RSP_CSWM_Mask 0xff
#define NETC_TX_c3_CFG_DATA_CODE_RSP_CSWM_Mask 0xff
#define NETC_TX_c4_CFG_DATA_CODE_RSP_CSWM_Mask 0xff
#define NETC_TX_c5_CFG_DATA_CODE_RSP_CSWM_Mask 0xff
#define NETC_TX_c6_CFG_DATA_CODE_RSP_CSWM_Mask 0xf0

#define NETC_TX_c1_CFG_DATA_CODE_RSP_CDM_Mask 0xff
#define NETC_TX_c2_CFG_DATA_CODE_RSP_CDM_Mask 0xff
#define NETC_TX_c3_CFG_DATA_CODE_RSP_CDM_Mask 0xff
#define NETC_TX_c4_CFG_DATA_CODE_RSP_CDM_Mask 0xff
#define NETC_TX_c5_CFG_DATA_CODE_RSP_CDM_Mask 0xff
#define NETC_TX_c6_CFG_DATA_CODE_RSP_CDM_Mask 0xf0

#define NETC_TX_c1_CFG_DATA_CODE_RSP_AMP_Mask 0xff
#define NETC_TX_c2_CFG_DATA_CODE_RSP_AMP_Mask 0xff
#define NETC_TX_c3_CFG_DATA_CODE_RSP_AMP_Mask 0xff
#define NETC_TX_c4_CFG_DATA_CODE_RSP_AMP_Mask 0xff
#define NETC_TX_c5_CFG_DATA_CODE_RSP_AMP_Mask 0xff
#define NETC_TX_c6_CFG_DATA_CODE_RSP_AMP_Mask 0xf0

#define NETC_TX_c1_CFG_DATA_CODE_RSP_ETM_Mask 0xff
#define NETC_TX_c2_CFG_DATA_CODE_RSP_ETM_Mask 0xff
#define NETC_TX_c3_CFG_DATA_CODE_RSP_ETM_Mask 0xff
#define NETC_TX_c4_CFG_DATA_CODE_RSP_ETM_Mask 0xff
#define NETC_TX_c5_CFG_DATA_CODE_RSP_ETM_Mask 0xff
#define NETC_TX_c6_CFG_DATA_CODE_RSP_ETM_Mask 0xf0

#define NETC_TX_c1_CFG_DATA_CODE_RSP_DSM_Mask 0xff
#define NETC_TX_c2_CFG_DATA_CODE_RSP_DSM_Mask 0xff
#define NETC_TX_c3_CFG_DATA_CODE_RSP_DSM_Mask 0xff
#define NETC_TX_c4_CFG_DATA_CODE_RSP_DSM_Mask 0xff
#define NETC_TX_c5_CFG_DATA_CODE_RSP_DSM_Mask 0xff
#define NETC_TX_c6_CFG_DATA_CODE_RSP_DSM_Mask 0xf0

#define NETC_TX_c1_CFG_DATA_CODE_RSP_PDM_Mask 0xff
#define NETC_TX_c2_CFG_DATA_CODE_RSP_PDM_Mask 0xff
#define NETC_TX_c3_CFG_DATA_CODE_RSP_PDM_Mask 0xff
#define NETC_TX_c4_CFG_DATA_CODE_RSP_PDM_Mask 0xff
#define NETC_TX_c5_CFG_DATA_CODE_RSP_PDM_Mask 0xff
#define NETC_TX_c6_CFG_DATA_CODE_RSP_PDM_Mask 0xf0

#define NETC_TX_c1_CFG_DATA_CODE_RSP_ECC_Mask 0xff
#define NETC_TX_c2_CFG_DATA_CODE_RSP_ECC_Mask 0xff
#define NETC_TX_c3_CFG_DATA_CODE_RSP_ECC_Mask 0xff
#define NETC_TX_c4_CFG_DATA_CODE_RSP_ECC_Mask 0xff
#define NETC_TX_c5_CFG_DATA_CODE_RSP_ECC_Mask 0xff
#define NETC_TX_c6_CFG_DATA_CODE_RSP_ECC_Mask 0xf0

#define NETC_TX_c1_CFG_DATA_CODE_RSP_DDM_Mask 0xff
#define NETC_TX_c2_CFG_DATA_CODE_RSP_DDM_Mask 0xff
#define NETC_TX_c3_CFG_DATA_CODE_RSP_DDM_Mask 0xff
#define NETC_TX_c4_CFG_DATA_CODE_RSP_DDM_Mask 0xff
#define NETC_TX_c5_CFG_DATA_CODE_RSP_DDM_Mask 0xff
#define NETC_TX_c6_CFG_DATA_CODE_RSP_DDM_Mask 0xf0

#define NETC_TX_c1_CFG_DATA_CODE_RSP_IPC_Mask 0xff
#define NETC_TX_c2_CFG_DATA_CODE_RSP_IPC_Mask 0xff
#define NETC_TX_c3_CFG_DATA_CODE_RSP_IPC_Mask 0xff
#define NETC_TX_c4_CFG_DATA_CODE_RSP_IPC_Mask 0xff
#define NETC_TX_c5_CFG_DATA_CODE_RSP_IPC_Mask 0xff
#define NETC_TX_c6_CFG_DATA_CODE_RSP_IPC_Mask 0xf0

#define NETC_TX_c1_STATUS_ICS_Mask 0x3
#define NETC_TX_c2_STATUS_ICS_Mask 0x0

#define NETC_TX_c1_STATUS_CDM_Mask 0x3
#define NETC_TX_c2_STATUS_CDM_Mask 0x0

#define NETC_TX_c1_STATUS_AMP_Mask 0x3
#define NETC_TX_c2_STATUS_AMP_Mask 0x0

#define NETC_TX_c1_ENVIRONMENTAL_CONDITIONS_Mask 0xff
#define NETC_TX_c2_ENVIRONMENTAL_CONDITIONS_Mask 0x80
#define NETC_TX_c3_ENVIRONMENTAL_CONDITIONS_Mask 0x0
#define NETC_TX_c4_ENVIRONMENTAL_CONDITIONS_Mask 0x0
#define NETC_TX_c5_ENVIRONMENTAL_CONDITIONS_Mask 0x0
#define NETC_TX_c6_ENVIRONMENTAL_CONDITIONS_Mask 0x0
#define NETC_TX_c7_ENVIRONMENTAL_CONDITIONS_Mask 0x0
#define NETC_TX_c8_ENVIRONMENTAL_CONDITIONS_Mask 0x0

#define NETC_TX_c1_FT_HVAC_STAT_Mask 0x0
#define NETC_TX_c2_FT_HVAC_STAT_Mask 0x0
#define NETC_TX_c3_FT_HVAC_STAT_Mask 0x0
#define NETC_TX_c4_FT_HVAC_STAT_Mask 0xfe
#define NETC_TX_c5_FT_HVAC_STAT_Mask 0xf0
#define NETC_TX_c6_FT_HVAC_STAT_Mask 0x0

#define NETC_TX_c1_DCM_P_MSG_Mask 0x1
#define NETC_TX_c2_DCM_P_MSG_Mask 0xff

#define NETC_TX_c1_DCM_D_MSG_Mask 0x3
#define NETC_TX_c2_DCM_D_MSG_Mask 0x1f

#define NETC_TX_c1_NWM_PLGM_Mask 0xff
#define NETC_TX_c2_NWM_PLGM_Mask 0x3f

#define NETC_TX_c1_NWM_ETM_Mask 0xff
#define NETC_TX_c2_NWM_ETM_Mask 0x3f

#define NETC_TX_c1_NWM_DSM_Mask 0xff
#define NETC_TX_c2_NWM_DSM_Mask 0x3f

#define NETC_TX_c1_NWM_PDM_Mask 0xff
#define NETC_TX_c2_NWM_PDM_Mask 0x3f

#define NETC_TX_c1_NWM_DDM_Mask 0xff
#define NETC_TX_c2_NWM_DDM_Mask 0x3f

#define NETC_TX_c1_APPL_ECU_BCM_1_Mask 0xff
#define NETC_TX_c2_APPL_ECU_BCM_1_Mask 0xff
#define NETC_TX_c3_APPL_ECU_BCM_1_Mask 0xff
#define NETC_TX_c4_APPL_ECU_BCM_1_Mask 0xff
#define NETC_TX_c5_APPL_ECU_BCM_1_Mask 0xff
#define NETC_TX_c6_APPL_ECU_BCM_1_Mask 0xff
#define NETC_TX_c7_APPL_ECU_BCM_1_Mask 0xff
#define NETC_TX_c8_APPL_ECU_BCM_1_Mask 0xff

#define NETC_TX_c1_TRIP_A_B_Mask 0x0
#define NETC_TX_c2_TRIP_A_B_Mask 0xf
#define NETC_TX_c3_TRIP_A_B_Mask 0xff
#define NETC_TX_c4_TRIP_A_B_Mask 0xff
#define NETC_TX_c5_TRIP_A_B_Mask 0x0
#define NETC_TX_c6_TRIP_A_B_Mask 0x0
#define NETC_TX_c7_TRIP_A_B_Mask 0x0
#define NETC_TX_c8_TRIP_A_B_Mask 0x0

#define NETC_TX_c1_TGW_A1_Mask 0x18
#define NETC_TX_c2_TGW_A1_Mask 0x0
#define NETC_TX_c3_TGW_A1_Mask 0x0
#define NETC_TX_c4_TGW_A1_Mask 0xc0

#define NETC_TX_c1_ICS_MSG_Mask 0x0
#define NETC_TX_c2_ICS_MSG_Mask 0x0
#define NETC_TX_c3_ICS_MSG_Mask 0x0
#define NETC_TX_c4_ICS_MSG_Mask 0x0
#define NETC_TX_c5_ICS_MSG_Mask 0x0
#define NETC_TX_c6_ICS_MSG_Mask 0x2
#define NETC_TX_c7_ICS_MSG_Mask 0x0
#define NETC_TX_c8_ICS_MSG_Mask 0xfc

#define NETC_TX_c1_HVAC_A1_Mask 0x1
#define NETC_TX_c2_HVAC_A1_Mask 0x18
#define NETC_TX_c3_HVAC_A1_Mask 0x0
#define NETC_TX_c4_HVAC_A1_Mask 0x0
#define NETC_TX_c5_HVAC_A1_Mask 0x0
#define NETC_TX_c6_HVAC_A1_Mask 0xff

#define NETC_TX_c1_DIRECT_INFO_Mask 0x0
#define NETC_TX_c2_DIRECT_INFO_Mask 0x78
#define NETC_TX_c3_DIRECT_INFO_Mask 0x0
#define NETC_TX_c4_DIRECT_INFO_Mask 0x0
#define NETC_TX_c5_DIRECT_INFO_Mask 0x0

#define NETC_TX_c1_HVAC_A4_Mask 0xff
#define NETC_TX_c2_HVAC_A4_Mask 0xff
#define NETC_TX_c3_HVAC_A4_Mask 0xf0
#define NETC_TX_c4_HVAC_A4_Mask 0x0

#define NETC_TX_c1_STATUS_RRM_Mask 0xc0
#define NETC_TX_c2_STATUS_RRM_Mask 0x0
#define NETC_TX_c3_STATUS_RRM_Mask 0x0
#define NETC_TX_c4_STATUS_RRM_Mask 0x0
#define NETC_TX_c5_STATUS_RRM_Mask 0x0
#define NETC_TX_c6_STATUS_RRM_Mask 0x0
#define NETC_TX_c7_STATUS_RRM_Mask 0x0
#define NETC_TX_c8_STATUS_RRM_Mask 0x0

#define NETC_TX_c1_STATUS_IPC_Mask 0x0
#define NETC_TX_c2_STATUS_IPC_Mask 0x0
#define NETC_TX_c3_STATUS_IPC_Mask 0x0
#define NETC_TX_c4_STATUS_IPC_Mask 0x0
#define NETC_TX_c5_STATUS_IPC_Mask 0x0
#define NETC_TX_c6_STATUS_IPC_Mask 0x30
#define NETC_TX_c7_STATUS_IPC_Mask 0x0
#define NETC_TX_c8_STATUS_IPC_Mask 0x0

#define NETC_TX_c1_STATUS_ECC_Mask 0x24
#define NETC_TX_c2_STATUS_ECC_Mask 0x37
#define NETC_TX_c3_STATUS_ECC_Mask 0xe0

#define NETC_TX_c1_HVAC_A2_Mask 0x3f
#define NETC_TX_c2_HVAC_A2_Mask 0xff
#define NETC_TX_c3_HVAC_A2_Mask 0xff

#define NETC_TX_c1_PLG_A1_Mask 0x83
#define NETC_TX_c2_PLG_A1_Mask 0x1

#define NETC_TX_c1_ECC_A1_Mask 0xff
#define NETC_TX_c2_ECC_A1_Mask 0x0

#define NETC_TX_c1_NWM_RBSS_Mask 0xff
#define NETC_TX_c2_NWM_RBSS_Mask 0x3f

#define NETC_TX_c1_NWM_LBSS_Mask 0xff
#define NETC_TX_c2_NWM_LBSS_Mask 0x3f

#define NETC_TX_c1_NWM_ICS_Mask 0xff
#define NETC_TX_c2_NWM_ICS_Mask 0x3f

#define NETC_TX_c1_NWM_CSWM_Mask 0xff
#define NETC_TX_c2_NWM_CSWM_Mask 0x3f

#define NETC_TX_c1_NWM_CDM_Mask 0xff
#define NETC_TX_c2_NWM_CDM_Mask 0x3f

#define NETC_TX_c1_NWM_AMP_Mask 0xff
#define NETC_TX_c2_NWM_AMP_Mask 0x3f

#define NETC_TX_c1_NWM_ECC_Mask 0xff
#define NETC_TX_c2_NWM_ECC_Mask 0x3f

#define NETC_TX_c1_NWM_IPC_Mask 0xff
#define NETC_TX_c2_NWM_IPC_Mask 0x3f

#define NETC_TX_c1_CBC_I4_Mask 0x7f
#define NETC_TX_c2_CBC_I4_Mask 0x3
#define NETC_TX_c3_CBC_I4_Mask 0x0
#define NETC_TX_c4_CBC_I4_Mask 0x0
#define NETC_TX_c5_CBC_I4_Mask 0x0

#define NETC_TX_c1_CMCM_UNLK_Mask 0xf0
#define NETC_TX_c2_CMCM_UNLK_Mask 0x0
#define NETC_TX_c3_CMCM_UNLK_Mask 0x0

#define NETC_TX_c1_CFG_RQ_Mask 0xff
#define NETC_TX_c2_CFG_RQ_Mask 0x3
#define NETC_TX_c3_CFG_RQ_Mask 0xff

/* Pretransmit functions */
extern canuint8 VF606_CBC_PT2_PreTransmit(CanTxInfoStruct txStruct);
extern canuint8 VF606_BCM_COMMAND_PreTransmit(CanTxInfoStruct txStruct);

/* Confirmation functions */
extern void VF606_CBC_PT2_Confirmation(CanTransmitHandle txObject);
extern void VF606_BCM_COMMAND_Confirmation(CanTransmitHandle txObject);
extern void TpDrvTxConfirmation(CanTransmitHandle txObject);
extern void TpDrvRxConfirmation(CanTransmitHandle txObject);
extern void CCP_DTO_Confirmation(CanTransmitHandle txObject);


/* Precopy functions */
extern canuint8 TpFuncPrecopy(CanRxInfoStructPtr preParam);
extern canuint8 TpPrecopy(CanRxInfoStructPtr preParam);


/* Indication functions */
extern void NETC_NetworkMsgIndication(CanReceiveHandle rxObject);
extern void CCP_CRO_Indication(CanReceiveHandle rxObject);

/* Hook function prototypes */

extern void ApplCanWakeUp(CanChannelHandle channel);
extern void ApplCanCancelNotification(CanChannelHandle channel);
extern canuint8 ApplCanMsgReceived(CanRxInfoStructPtr rxStruct);
extern void ApplCanBusOff(CanChannelHandle channel);

extern canuint8 TpPrecopy(CanRxInfoStructPtr preParam);
extern canuint8 TpFuncPrecopy(CanRxInfoStructPtr preParam);

#define RDSTx_0 (*((_c_RDSTx_0_buf *)(&txMsgObj[0].Data.ucData[0])))
#define RDS4_0 (*((_c_RDS4_0_buf *)(&rxMsgObj[0].Data[0])))
#define RDS5_0 (*((_c_RDS5_0_buf *)(&rxMsgObj[0].Data[0])))
#define RDS6_0 (*((_c_RDS6_0_buf *)(&rxMsgObj[0].Data[0])))
#define RDS7_0 (*((_c_RDS7_0_buf *)(&rxMsgObj[0].Data[0])))
#define RDS8_0 (*((_c_RDS8_0_buf *)(&rxMsgObj[0].Data[0])))
#define RDS9_0 (*((_c_RDS9_0_buf *)(&rxMsgObj[0].Data[0])))
#define RDS10_0 (*((_c_RDS10_0_buf *)(&rxMsgObj[0].Data[0])))
#define RDS11_0 (*((_c_RDS11_0_buf *)(&rxMsgObj[0].Data[0])))
#define RDS12_0 (*((_c_RDS12_0_buf *)(&rxMsgObj[0].Data[0])))
#define RDS13_0 (*((_c_RDS13_0_buf *)(&rxMsgObj[0].Data[0])))
#define RDS14_0 (*((_c_RDS14_0_buf *)(&rxMsgObj[0].Data[0])))
#define RDS15_0 (*((_c_RDS15_0_buf *)(&rxMsgObj[0].Data[0])))
#define RDS16_0 (*((_c_RDS16_0_buf *)(&rxMsgObj[0].Data[0])))
#define RDS17_0 (*((_c_RDS17_0_buf *)(&rxMsgObj[0].Data[0])))
#define RDS18_0 (*((_c_RDS18_0_buf *)(&rxMsgObj[0].Data[0])))
#define RDS19_0 (*((_c_RDS19_0_buf *)(&rxMsgObj[0].Data[0])))
#define RDS20_0 (*((_c_RDS20_0_buf *)(&rxMsgObj[0].Data[0])))
#define RDS21_0 (*((_c_RDS21_0_buf *)(&rxMsgObj[0].Data[0])))
#define RDS22_0 (*((_c_RDS22_0_buf *)(&rxMsgObj[0].Data[0])))
#define RDS23_0 (*((_c_RDS23_0_buf *)(&rxMsgObj[0].Data[0])))
#define RDS24_0 (*((_c_RDS24_0_buf *)(&rxMsgObj[0].Data[0])))
#define RDS25_0 (*((_c_RDS25_0_buf *)(&rxMsgObj[0].Data[0])))
#define RDS26_0 (*((_c_RDS26_0_buf *)(&rxMsgObj[0].Data[0])))
#define RDS27_0 (*((_c_RDS27_0_buf *)(&rxMsgObj[0].Data[0])))
#define RDS28_0 (*((_c_RDS28_0_buf *)(&rxMsgObj[0].Data[0])))
#define RDSBasic_0 (*((_c_RDSBasic_0_buf *)(&rxMsgObj[0].Data[0])))
#define RDSTx_1 (*((_c_RDSTx_1_buf *)(&txMsgObj[2].Data.ucData[0])))
#define RDS4_1 (*((_c_RDS4_1_buf *)(&rxMsgObj[1].Data[0])))
#define RDS5_1 (*((_c_RDS5_1_buf *)(&rxMsgObj[1].Data[0])))
#define RDS6_1 (*((_c_RDS6_1_buf *)(&rxMsgObj[1].Data[0])))
#define RDS7_1 (*((_c_RDS7_1_buf *)(&rxMsgObj[1].Data[0])))
#define RDS8_1 (*((_c_RDS8_1_buf *)(&rxMsgObj[1].Data[0])))
#define RDS9_1 (*((_c_RDS9_1_buf *)(&rxMsgObj[1].Data[0])))
#define RDS10_1 (*((_c_RDS10_1_buf *)(&rxMsgObj[1].Data[0])))
#define RDS11_1 (*((_c_RDS11_1_buf *)(&rxMsgObj[1].Data[0])))
#define RDS12_1 (*((_c_RDS12_1_buf *)(&rxMsgObj[1].Data[0])))
#define RDS13_1 (*((_c_RDS13_1_buf *)(&rxMsgObj[1].Data[0])))
#define RDS14_1 (*((_c_RDS14_1_buf *)(&rxMsgObj[1].Data[0])))
#define RDS15_1 (*((_c_RDS15_1_buf *)(&rxMsgObj[1].Data[0])))
#define RDS16_1 (*((_c_RDS16_1_buf *)(&rxMsgObj[1].Data[0])))
#define RDS17_1 (*((_c_RDS17_1_buf *)(&rxMsgObj[1].Data[0])))
#define RDS18_1 (*((_c_RDS18_1_buf *)(&rxMsgObj[1].Data[0])))
#define RDS19_1 (*((_c_RDS19_1_buf *)(&rxMsgObj[1].Data[0])))
#define RDS20_1 (*((_c_RDS20_1_buf *)(&rxMsgObj[1].Data[0])))
#define RDS21_1 (*((_c_RDS21_1_buf *)(&rxMsgObj[1].Data[0])))
#define RDS22_1 (*((_c_RDS22_1_buf *)(&rxMsgObj[1].Data[0])))
#define RDS23_1 (*((_c_RDS23_1_buf *)(&rxMsgObj[1].Data[0])))
#define RDS24_1 (*((_c_RDS24_1_buf *)(&rxMsgObj[1].Data[0])))
#define RDS25_1 (*((_c_RDS25_1_buf *)(&rxMsgObj[1].Data[0])))
#define RDS26_1 (*((_c_RDS26_1_buf *)(&rxMsgObj[1].Data[0])))
#define RDS27_1 (*((_c_RDS27_1_buf *)(&rxMsgObj[1].Data[0])))
#define RDS28_1 (*((_c_RDS28_1_buf *)(&rxMsgObj[1].Data[0])))
#define RDSBasic_1 (*((_c_RDSBasic_1_buf *)(&rxMsgObj[1].Data[0])))

/*************************************************************/
/* Databuffer for send objects                               */
/*************************************************************/
extern _c_STATUS_C_BCM2_buf MEMORY_FAR  STATUS_C_BCM2;
extern _c_HVAC_INFO_buf MEMORY_FAR  HVAC_INFO;
extern _c_CBC_PT4_buf MEMORY_FAR  CBC_PT4;
extern _c_BCM_KEYON_COUNTER_C_CAN_buf MEMORY_FAR  BCM_KEYON_COUNTER_C_CAN;
extern _c_IBS4_buf MEMORY_FAR  IBS4;
extern _c_ECU_APPL_BCM_0_buf MEMORY_FAR  ECU_APPL_BCM_0;
extern _c_CONFIGURATION_DATA_CODE_REQUEST_0_buf MEMORY_FAR  CONFIGURATION_DATA_CODE_REQUEST_0;
extern _c_CBC_VTA_0_buf MEMORY_FAR  CBC_VTA_0;
extern _c_DIAGNOSTIC_RESPONSE_BCM_0_buf MEMORY_FAR  DIAGNOSTIC_RESPONSE_BCM_0;
extern _c_STATUS_B_CAN2_buf MEMORY_FAR  STATUS_B_CAN2;
extern _c_EXTERNAL_LIGHTS_0_buf MEMORY_FAR  EXTERNAL_LIGHTS_0;
extern _c_ASBM1_CONTROL_buf MEMORY_FAR  ASBM1_CONTROL;
extern _c_IBS2_buf MEMORY_FAR  IBS2;
extern _c_VEHICLE_SPEED_ODOMETER_0_buf MEMORY_FAR  VEHICLE_SPEED_ODOMETER_0;
extern _c_STATUS_C_BCM_buf MEMORY_FAR  STATUS_C_BCM;
extern _c_STATUS_B_CAN_buf MEMORY_FAR  STATUS_B_CAN;
extern _c_IBS3_buf MEMORY_FAR  IBS3;
extern _c_IBS1_buf MEMORY_FAR  IBS1;
extern _c_BATTERY_INFO_buf MEMORY_FAR  BATTERY_INFO;
extern _c_CFG_Feature_buf MEMORY_FAR  CFG_Feature;
extern _c_CBC_PT3_buf MEMORY_FAR  CBC_PT3;
extern _c_CBC_PT1_buf MEMORY_FAR  CBC_PT1;
extern _c_WAKE_C_BCM_buf MEMORY_FAR  WAKE_C_BCM;
extern _c_CBC_PT2_buf MEMORY_FAR  CBC_PT2;
extern _c_BCM_COMMAND_buf MEMORY_FAR  BCM_COMMAND;
extern _c_BCM_CODE_TRM_REQUEST_buf MEMORY_FAR  BCM_CODE_TRM_REQUEST;
extern _c_BCM_CODE_ESL_REQUEST_buf MEMORY_FAR  BCM_CODE_ESL_REQUEST;
extern _c_BCM_MINICRYPT_ACK_buf MEMORY_FAR  BCM_MINICRYPT_ACK;
extern _c_IMMO_CODE_RESPONSE_buf MEMORY_FAR  IMMO_CODE_RESPONSE;
extern _c_TxDynamicMsg0_0_buf MEMORY_FAR  TxDynamicMsg0_0;
extern _c_TxDynamicMsg1_0_buf MEMORY_FAR  TxDynamicMsg1_0;
extern _c_ECU_APPL_BCM_1_buf MEMORY_FAR  ECU_APPL_BCM_1;
extern _c_DTO_BCM_buf MEMORY_FAR  DTO_BCM;
extern _c_CONFIGURATION_DATA_CODE_REQUEST_1_buf MEMORY_FAR  CONFIGURATION_DATA_CODE_REQUEST_1;
extern _c_IBS_2_buf MEMORY_FAR  IBS_2;
extern _c_HUMIDITY_1000L_buf MEMORY_FAR  HUMIDITY_1000L;
extern _c_COMPASS_A1_buf MEMORY_FAR  COMPASS_A1;
extern _c_CBC_I5_buf MEMORY_FAR  CBC_I5;
extern _c_AMB_TEMP_DISP_buf MEMORY_FAR  AMB_TEMP_DISP;
extern _c_WCPM_STATUS_buf MEMORY_FAR  WCPM_STATUS;
extern _c_GE_B_buf MEMORY_FAR  GE_B;
extern _c_ENVIRONMENTAL_CONDITIONS_buf MEMORY_FAR  ENVIRONMENTAL_CONDITIONS;
extern _c_CBC_VTA_1_buf MEMORY_FAR  CBC_VTA_1;
extern _c_DIAGNOSTIC_RESPONSE_BCM_1_buf MEMORY_FAR  DIAGNOSTIC_RESPONSE_BCM_1;
extern _c_GW_C_I2_buf MEMORY_FAR  GW_C_I2;
extern _c_DAS_B_buf MEMORY_FAR  DAS_B;
extern _c_CBC_I2_buf MEMORY_FAR  CBC_I2;
extern _c_CBC_I1_buf MEMORY_FAR  CBC_I1;
extern _c_STATUS_C_CAN_buf MEMORY_FAR  STATUS_C_CAN;
extern _c_SWS_8_buf MEMORY_FAR  SWS_8;
extern _c_StW_Actn_Rq_1_buf MEMORY_FAR  StW_Actn_Rq_1;
extern _c_PAM_B_buf MEMORY_FAR  PAM_B;
extern _c_GW_C_I3_buf MEMORY_FAR  GW_C_I3;
extern _c_VEHICLE_SPEED_ODOMETER_1_buf MEMORY_FAR  VEHICLE_SPEED_ODOMETER_1;
extern _c_GW_C_I6_buf MEMORY_FAR  GW_C_I6;
extern _c_DYNAMIC_VEHICLE_INFO2_buf MEMORY_FAR  DYNAMIC_VEHICLE_INFO2;
extern _c_NWM_BCM_buf MEMORY_FAR  NWM_BCM;
extern _c_STATUS_BCM2_buf MEMORY_FAR  STATUS_BCM2;
extern _c_STATUS_BCM_buf MEMORY_FAR  STATUS_BCM;
extern _c_EXTERNAL_LIGHTS_1_buf MEMORY_FAR  EXTERNAL_LIGHTS_1;
extern _c_CBC_I4_buf MEMORY_FAR  CBC_I4;
extern _c_TxDynamicMsg0_1_buf MEMORY_FAR  TxDynamicMsg0_1;
extern _c_TxDynamicMsg1_1_buf MEMORY_FAR  TxDynamicMsg1_1;

/*************************************************************/
/* Byte access to databuffer of send objects                 */
/*************************************************************/
#define NETC_TX_c1_STATUS_C_BCM2		STATUS_C_BCM2._c[0]
#define NETC_TX_c2_STATUS_C_BCM2		STATUS_C_BCM2._c[1]
#define NETC_TX_c3_STATUS_C_BCM2		STATUS_C_BCM2._c[2]
#define NETC_TX_c4_STATUS_C_BCM2		STATUS_C_BCM2._c[3]
#define NETC_TX_c5_STATUS_C_BCM2		STATUS_C_BCM2._c[4]
#define NETC_TX_c6_STATUS_C_BCM2		STATUS_C_BCM2._c[5]
#define NETC_TX_c7_STATUS_C_BCM2		STATUS_C_BCM2._c[6]
#define NETC_TX_c8_STATUS_C_BCM2		STATUS_C_BCM2._c[7]

#define NETC_TX_c1_HVAC_INFO		HVAC_INFO._c[0]
#define NETC_TX_c2_HVAC_INFO		HVAC_INFO._c[1]
#define NETC_TX_c3_HVAC_INFO		HVAC_INFO._c[2]
#define NETC_TX_c4_HVAC_INFO		HVAC_INFO._c[3]
#define NETC_TX_c5_HVAC_INFO		HVAC_INFO._c[4]
#define NETC_TX_c6_HVAC_INFO		HVAC_INFO._c[5]
#define NETC_TX_c7_HVAC_INFO		HVAC_INFO._c[6]
#define NETC_TX_c8_HVAC_INFO		HVAC_INFO._c[7]

#define NETC_TX_c1_CBC_PT4		CBC_PT4._c[0]
#define NETC_TX_c2_CBC_PT4		CBC_PT4._c[1]

#define NETC_TX_c1_BCM_KEYON_COUNTER_C_CAN		BCM_KEYON_COUNTER_C_CAN._c[0]
#define NETC_TX_c2_BCM_KEYON_COUNTER_C_CAN		BCM_KEYON_COUNTER_C_CAN._c[1]

#define NETC_TX_c1_IBS4		IBS4._c[0]
#define NETC_TX_c2_IBS4		IBS4._c[1]
#define NETC_TX_c3_IBS4		IBS4._c[2]

#define NETC_TX_c1_ECU_APPL_BCM_0		ECU_APPL_BCM_0._c[0]
#define NETC_TX_c2_ECU_APPL_BCM_0		ECU_APPL_BCM_0._c[1]
#define NETC_TX_c3_ECU_APPL_BCM_0		ECU_APPL_BCM_0._c[2]
#define NETC_TX_c4_ECU_APPL_BCM_0		ECU_APPL_BCM_0._c[3]
#define NETC_TX_c5_ECU_APPL_BCM_0		ECU_APPL_BCM_0._c[4]
#define NETC_TX_c6_ECU_APPL_BCM_0		ECU_APPL_BCM_0._c[5]
#define NETC_TX_c7_ECU_APPL_BCM_0		ECU_APPL_BCM_0._c[6]
#define NETC_TX_c8_ECU_APPL_BCM_0		ECU_APPL_BCM_0._c[7]

#define NETC_TX_c1_CONFIGURATION_DATA_CODE_REQUEST_0		CONFIGURATION_DATA_CODE_REQUEST_0._c[0]
#define NETC_TX_c2_CONFIGURATION_DATA_CODE_REQUEST_0		CONFIGURATION_DATA_CODE_REQUEST_0._c[1]
#define NETC_TX_c3_CONFIGURATION_DATA_CODE_REQUEST_0		CONFIGURATION_DATA_CODE_REQUEST_0._c[2]
#define NETC_TX_c4_CONFIGURATION_DATA_CODE_REQUEST_0		CONFIGURATION_DATA_CODE_REQUEST_0._c[3]
#define NETC_TX_c5_CONFIGURATION_DATA_CODE_REQUEST_0		CONFIGURATION_DATA_CODE_REQUEST_0._c[4]
#define NETC_TX_c6_CONFIGURATION_DATA_CODE_REQUEST_0		CONFIGURATION_DATA_CODE_REQUEST_0._c[5]

#define NETC_TX_c1_CBC_VTA_0		CBC_VTA_0._c[0]

#define NETC_TX_c1_DIAGNOSTIC_RESPONSE_BCM_0		DIAGNOSTIC_RESPONSE_BCM_0._c[0]
#define NETC_TX_c2_DIAGNOSTIC_RESPONSE_BCM_0		DIAGNOSTIC_RESPONSE_BCM_0._c[1]
#define NETC_TX_c3_DIAGNOSTIC_RESPONSE_BCM_0		DIAGNOSTIC_RESPONSE_BCM_0._c[2]
#define NETC_TX_c4_DIAGNOSTIC_RESPONSE_BCM_0		DIAGNOSTIC_RESPONSE_BCM_0._c[3]
#define NETC_TX_c5_DIAGNOSTIC_RESPONSE_BCM_0		DIAGNOSTIC_RESPONSE_BCM_0._c[4]
#define NETC_TX_c6_DIAGNOSTIC_RESPONSE_BCM_0		DIAGNOSTIC_RESPONSE_BCM_0._c[5]
#define NETC_TX_c7_DIAGNOSTIC_RESPONSE_BCM_0		DIAGNOSTIC_RESPONSE_BCM_0._c[6]
#define NETC_TX_c8_DIAGNOSTIC_RESPONSE_BCM_0		DIAGNOSTIC_RESPONSE_BCM_0._c[7]

#define NETC_TX_c1_STATUS_B_CAN2		STATUS_B_CAN2._c[0]
#define NETC_TX_c2_STATUS_B_CAN2		STATUS_B_CAN2._c[1]
#define NETC_TX_c3_STATUS_B_CAN2		STATUS_B_CAN2._c[2]
#define NETC_TX_c4_STATUS_B_CAN2		STATUS_B_CAN2._c[3]
#define NETC_TX_c5_STATUS_B_CAN2		STATUS_B_CAN2._c[4]
#define NETC_TX_c6_STATUS_B_CAN2		STATUS_B_CAN2._c[5]
#define NETC_TX_c7_STATUS_B_CAN2		STATUS_B_CAN2._c[6]
#define NETC_TX_c8_STATUS_B_CAN2		STATUS_B_CAN2._c[7]

#define NETC_TX_c1_EXTERNAL_LIGHTS_0		EXTERNAL_LIGHTS_0._c[0]
#define NETC_TX_c2_EXTERNAL_LIGHTS_0		EXTERNAL_LIGHTS_0._c[1]
#define NETC_TX_c3_EXTERNAL_LIGHTS_0		EXTERNAL_LIGHTS_0._c[2]
#define NETC_TX_c4_EXTERNAL_LIGHTS_0		EXTERNAL_LIGHTS_0._c[3]
#define NETC_TX_c5_EXTERNAL_LIGHTS_0		EXTERNAL_LIGHTS_0._c[4]
#define NETC_TX_c6_EXTERNAL_LIGHTS_0		EXTERNAL_LIGHTS_0._c[5]

#define NETC_TX_c1_ASBM1_CONTROL		ASBM1_CONTROL._c[0]
#define NETC_TX_c2_ASBM1_CONTROL		ASBM1_CONTROL._c[1]
#define NETC_TX_c3_ASBM1_CONTROL		ASBM1_CONTROL._c[2]
#define NETC_TX_c4_ASBM1_CONTROL		ASBM1_CONTROL._c[3]

#define NETC_TX_c1_HVAC_INFO2		HVAC_A4._c[0]
#define NETC_TX_c2_HVAC_INFO2		HVAC_A4._c[1]
#define NETC_TX_c3_HVAC_INFO2		HVAC_A4._c[2]
#define NETC_TX_c4_HVAC_INFO2		HVAC_A4._c[3]

#define NETC_TX_c1_IBS2		IBS2._c[0]
#define NETC_TX_c2_IBS2		IBS2._c[1]
#define NETC_TX_c3_IBS2		IBS2._c[2]
#define NETC_TX_c4_IBS2		IBS2._c[3]
#define NETC_TX_c5_IBS2		IBS2._c[4]
#define NETC_TX_c6_IBS2		IBS2._c[5]
#define NETC_TX_c7_IBS2		IBS2._c[6]
#define NETC_TX_c8_IBS2		IBS2._c[7]

#define NETC_TX_c1_VEHICLE_SPEED_ODOMETER_0		VEHICLE_SPEED_ODOMETER_0._c[0]
#define NETC_TX_c2_VEHICLE_SPEED_ODOMETER_0		VEHICLE_SPEED_ODOMETER_0._c[1]
#define NETC_TX_c3_VEHICLE_SPEED_ODOMETER_0		VEHICLE_SPEED_ODOMETER_0._c[2]
#define NETC_TX_c4_VEHICLE_SPEED_ODOMETER_0		VEHICLE_SPEED_ODOMETER_0._c[3]
#define NETC_TX_c5_VEHICLE_SPEED_ODOMETER_0		VEHICLE_SPEED_ODOMETER_0._c[4]
#define NETC_TX_c6_VEHICLE_SPEED_ODOMETER_0		VEHICLE_SPEED_ODOMETER_0._c[5]
#define NETC_TX_c7_VEHICLE_SPEED_ODOMETER_0		VEHICLE_SPEED_ODOMETER_0._c[6]
#define NETC_TX_c8_VEHICLE_SPEED_ODOMETER_0		VEHICLE_SPEED_ODOMETER_0._c[7]

#define NETC_TX_c1_STATUS_C_BCM		STATUS_C_BCM._c[0]
#define NETC_TX_c2_STATUS_C_BCM		STATUS_C_BCM._c[1]
#define NETC_TX_c3_STATUS_C_BCM		STATUS_C_BCM._c[2]
#define NETC_TX_c4_STATUS_C_BCM		STATUS_C_BCM._c[3]
#define NETC_TX_c5_STATUS_C_BCM		STATUS_C_BCM._c[4]
#define NETC_TX_c6_STATUS_C_BCM		STATUS_C_BCM._c[5]
#define NETC_TX_c7_STATUS_C_BCM		STATUS_C_BCM._c[6]
#define NETC_TX_c8_STATUS_C_BCM		STATUS_C_BCM._c[7]

#define NETC_TX_c1_STATUS_B_CAN		STATUS_B_CAN._c[0]
#define NETC_TX_c2_STATUS_B_CAN		STATUS_B_CAN._c[1]
#define NETC_TX_c3_STATUS_B_CAN		STATUS_B_CAN._c[2]
#define NETC_TX_c4_STATUS_B_CAN		STATUS_B_CAN._c[3]
#define NETC_TX_c5_STATUS_B_CAN		STATUS_B_CAN._c[4]
#define NETC_TX_c6_STATUS_B_CAN		STATUS_B_CAN._c[5]
#define NETC_TX_c7_STATUS_B_CAN		STATUS_B_CAN._c[6]
#define NETC_TX_c8_STATUS_B_CAN		STATUS_B_CAN._c[7]

#define NETC_TX_c1_IBS3		IBS3._c[0]
#define NETC_TX_c2_IBS3		IBS3._c[1]
#define NETC_TX_c3_IBS3		IBS3._c[2]
#define NETC_TX_c4_IBS3		IBS3._c[3]

#define NETC_TX_c1_IBS1		IBS1._c[0]
#define NETC_TX_c2_IBS1		IBS1._c[1]
#define NETC_TX_c3_IBS1		IBS1._c[2]
#define NETC_TX_c4_IBS1		IBS1._c[3]
#define NETC_TX_c5_IBS1		IBS1._c[4]
#define NETC_TX_c6_IBS1		IBS1._c[5]
#define NETC_TX_c7_IBS1		IBS1._c[6]
#define NETC_TX_c8_IBS1		IBS1._c[7]

#define NETC_TX_c1_BATTERY_INFO		BATTERY_INFO._c[0]
#define NETC_TX_c2_BATTERY_INFO		BATTERY_INFO._c[1]
#define NETC_TX_c3_BATTERY_INFO		BATTERY_INFO._c[2]
#define NETC_TX_c4_BATTERY_INFO		BATTERY_INFO._c[3]
#define NETC_TX_c5_BATTERY_INFO		BATTERY_INFO._c[4]
#define NETC_TX_c6_BATTERY_INFO		BATTERY_INFO._c[5]
#define NETC_TX_c7_BATTERY_INFO		BATTERY_INFO._c[6]
#define NETC_TX_c8_BATTERY_INFO		BATTERY_INFO._c[7]

#define NETC_TX_c1_CFG_Feature		CFG_Feature._c[0]
#define NETC_TX_c2_CFG_Feature		CFG_Feature._c[1]
#define NETC_TX_c3_CFG_Feature		CFG_Feature._c[2]

#define NETC_TX_c1_CBC_PT3		CBC_PT3._c[0]
#define NETC_TX_c2_CBC_PT3		CBC_PT3._c[1]
#define NETC_TX_c3_CBC_PT3		CBC_PT3._c[2]
#define NETC_TX_c4_CBC_PT3		CBC_PT3._c[3]
#define NETC_TX_c5_CBC_PT3		CBC_PT3._c[4]

#define NETC_TX_c1_CBC_PT1		CBC_PT1._c[0]
#define NETC_TX_c2_CBC_PT1		CBC_PT1._c[1]
#define NETC_TX_c3_CBC_PT1		CBC_PT1._c[2]
#define NETC_TX_c4_CBC_PT1		CBC_PT1._c[3]
#define NETC_TX_c5_CBC_PT1		CBC_PT1._c[4]
#define NETC_TX_c6_CBC_PT1		CBC_PT1._c[5]

#define NETC_TX_c1_WAKE_C_BCM		WAKE_C_BCM._c[0]
#define NETC_TX_c2_WAKE_C_BCM		WAKE_C_BCM._c[1]
#define NETC_TX_c3_WAKE_C_BCM		WAKE_C_BCM._c[2]
#define NETC_TX_c4_WAKE_C_BCM		WAKE_C_BCM._c[3]
#define NETC_TX_c5_WAKE_C_BCM		WAKE_C_BCM._c[4]
#define NETC_TX_c6_WAKE_C_BCM		WAKE_C_BCM._c[5]
#define NETC_TX_c7_WAKE_C_BCM		WAKE_C_BCM._c[6]
#define NETC_TX_c8_WAKE_C_BCM		WAKE_C_BCM._c[7]

#define NETC_TX_c1_CBC_PT2		CBC_PT2._c[0]
#define NETC_TX_c2_CBC_PT2		CBC_PT2._c[1]
#define NETC_TX_c3_CBC_PT2		CBC_PT2._c[2]
#define NETC_TX_c4_CBC_PT2		CBC_PT2._c[3]
#define NETC_TX_c5_CBC_PT2		CBC_PT2._c[4]

#define NETC_TX_c1_BCM_COMMAND		BCM_COMMAND._c[0]
#define NETC_TX_c2_BCM_COMMAND		BCM_COMMAND._c[1]
#define NETC_TX_c3_BCM_COMMAND		BCM_COMMAND._c[2]
#define NETC_TX_c4_BCM_COMMAND		BCM_COMMAND._c[3]
#define NETC_TX_c5_BCM_COMMAND		BCM_COMMAND._c[4]

#define NETC_TX_c1_BCM_CODE_TRM_REQUEST		BCM_CODE_TRM_REQUEST._c[0]
#define NETC_TX_c2_BCM_CODE_TRM_REQUEST		BCM_CODE_TRM_REQUEST._c[1]
#define NETC_TX_c3_BCM_CODE_TRM_REQUEST		BCM_CODE_TRM_REQUEST._c[2]
#define NETC_TX_c4_BCM_CODE_TRM_REQUEST		BCM_CODE_TRM_REQUEST._c[3]
#define NETC_TX_c5_BCM_CODE_TRM_REQUEST		BCM_CODE_TRM_REQUEST._c[4]
#define NETC_TX_c6_BCM_CODE_TRM_REQUEST		BCM_CODE_TRM_REQUEST._c[5]
#define NETC_TX_c7_BCM_CODE_TRM_REQUEST		BCM_CODE_TRM_REQUEST._c[6]
#define NETC_TX_c8_BCM_CODE_TRM_REQUEST		BCM_CODE_TRM_REQUEST._c[7]

#define NETC_TX_c1_BCM_CODE_ESL_REQUEST		BCM_CODE_ESL_REQUEST._c[0]
#define NETC_TX_c2_BCM_CODE_ESL_REQUEST		BCM_CODE_ESL_REQUEST._c[1]
#define NETC_TX_c3_BCM_CODE_ESL_REQUEST		BCM_CODE_ESL_REQUEST._c[2]
#define NETC_TX_c4_BCM_CODE_ESL_REQUEST		BCM_CODE_ESL_REQUEST._c[3]
#define NETC_TX_c5_BCM_CODE_ESL_REQUEST		BCM_CODE_ESL_REQUEST._c[4]
#define NETC_TX_c6_BCM_CODE_ESL_REQUEST		BCM_CODE_ESL_REQUEST._c[5]
#define NETC_TX_c7_BCM_CODE_ESL_REQUEST		BCM_CODE_ESL_REQUEST._c[6]
#define NETC_TX_c8_BCM_CODE_ESL_REQUEST		BCM_CODE_ESL_REQUEST._c[7]

#define NETC_TX_c1_BCM_MINICRYPT_ACK		BCM_MINICRYPT_ACK._c[0]

#define NETC_TX_c1_IMMO_CODE_RESPONSE		IMMO_CODE_RESPONSE._c[0]
#define NETC_TX_c2_IMMO_CODE_RESPONSE		IMMO_CODE_RESPONSE._c[1]
#define NETC_TX_c3_IMMO_CODE_RESPONSE		IMMO_CODE_RESPONSE._c[2]
#define NETC_TX_c4_IMMO_CODE_RESPONSE		IMMO_CODE_RESPONSE._c[3]
#define NETC_TX_c5_IMMO_CODE_RESPONSE		IMMO_CODE_RESPONSE._c[4]
#define NETC_TX_c6_IMMO_CODE_RESPONSE		IMMO_CODE_RESPONSE._c[5]
#define NETC_TX_c7_IMMO_CODE_RESPONSE		IMMO_CODE_RESPONSE._c[6]

#define NETC_TX_c1_TxDynamicMsg0_0		TxDynamicMsg0_0._c[0]
#define NETC_TX_c2_TxDynamicMsg0_0		TxDynamicMsg0_0._c[1]
#define NETC_TX_c3_TxDynamicMsg0_0		TxDynamicMsg0_0._c[2]
#define NETC_TX_c4_TxDynamicMsg0_0		TxDynamicMsg0_0._c[3]
#define NETC_TX_c5_TxDynamicMsg0_0		TxDynamicMsg0_0._c[4]
#define NETC_TX_c6_TxDynamicMsg0_0		TxDynamicMsg0_0._c[5]
#define NETC_TX_c7_TxDynamicMsg0_0		TxDynamicMsg0_0._c[6]
#define NETC_TX_c8_TxDynamicMsg0_0		TxDynamicMsg0_0._c[7]

#define NETC_TX_c1_TxDynamicMsg1_0		TxDynamicMsg1_0._c[0]
#define NETC_TX_c2_TxDynamicMsg1_0		TxDynamicMsg1_0._c[1]
#define NETC_TX_c3_TxDynamicMsg1_0		TxDynamicMsg1_0._c[2]
#define NETC_TX_c4_TxDynamicMsg1_0		TxDynamicMsg1_0._c[3]
#define NETC_TX_c5_TxDynamicMsg1_0		TxDynamicMsg1_0._c[4]
#define NETC_TX_c6_TxDynamicMsg1_0		TxDynamicMsg1_0._c[5]
#define NETC_TX_c7_TxDynamicMsg1_0		TxDynamicMsg1_0._c[6]
#define NETC_TX_c8_TxDynamicMsg1_0		TxDynamicMsg1_0._c[7]

#define NETC_TX_c1_ECU_APPL_BCM_1		ECU_APPL_BCM_1._c[0]
#define NETC_TX_c2_ECU_APPL_BCM_1		ECU_APPL_BCM_1._c[1]
#define NETC_TX_c3_ECU_APPL_BCM_1		ECU_APPL_BCM_1._c[2]
#define NETC_TX_c4_ECU_APPL_BCM_1		ECU_APPL_BCM_1._c[3]
#define NETC_TX_c5_ECU_APPL_BCM_1		ECU_APPL_BCM_1._c[4]
#define NETC_TX_c6_ECU_APPL_BCM_1		ECU_APPL_BCM_1._c[5]
#define NETC_TX_c7_ECU_APPL_BCM_1		ECU_APPL_BCM_1._c[6]
#define NETC_TX_c8_ECU_APPL_BCM_1		ECU_APPL_BCM_1._c[7]

#define NETC_TX_c1_DTO_BCM		DTO_BCM._c[0]
#define NETC_TX_c2_DTO_BCM		DTO_BCM._c[1]
#define NETC_TX_c3_DTO_BCM		DTO_BCM._c[2]
#define NETC_TX_c4_DTO_BCM		DTO_BCM._c[3]
#define NETC_TX_c5_DTO_BCM		DTO_BCM._c[4]
#define NETC_TX_c6_DTO_BCM		DTO_BCM._c[5]
#define NETC_TX_c7_DTO_BCM		DTO_BCM._c[6]
#define NETC_TX_c8_DTO_BCM		DTO_BCM._c[7]

#define NETC_TX_c1_CONFIGURATION_DATA_CODE_REQUEST_1		CONFIGURATION_DATA_CODE_REQUEST_1._c[0]
#define NETC_TX_c2_CONFIGURATION_DATA_CODE_REQUEST_1		CONFIGURATION_DATA_CODE_REQUEST_1._c[1]
#define NETC_TX_c3_CONFIGURATION_DATA_CODE_REQUEST_1		CONFIGURATION_DATA_CODE_REQUEST_1._c[2]
#define NETC_TX_c4_CONFIGURATION_DATA_CODE_REQUEST_1		CONFIGURATION_DATA_CODE_REQUEST_1._c[3]
#define NETC_TX_c5_CONFIGURATION_DATA_CODE_REQUEST_1		CONFIGURATION_DATA_CODE_REQUEST_1._c[4]
#define NETC_TX_c6_CONFIGURATION_DATA_CODE_REQUEST_1		CONFIGURATION_DATA_CODE_REQUEST_1._c[5]

#define NETC_TX_c1_IBS_2		IBS_2._c[0]
#define NETC_TX_c2_IBS_2		IBS_2._c[1]
#define NETC_TX_c3_IBS_2		IBS_2._c[2]
#define NETC_TX_c4_IBS_2		IBS_2._c[3]

#define NETC_TX_c1_HUMIDITY_1000L		HUMIDITY_1000L._c[0]
#define NETC_TX_c2_HUMIDITY_1000L		HUMIDITY_1000L._c[1]
#define NETC_TX_c3_HUMIDITY_1000L		HUMIDITY_1000L._c[2]
#define NETC_TX_c4_HUMIDITY_1000L		HUMIDITY_1000L._c[3]
#define NETC_TX_c5_HUMIDITY_1000L		HUMIDITY_1000L._c[4]
#define NETC_TX_c6_HUMIDITY_1000L		HUMIDITY_1000L._c[5]
#define NETC_TX_c7_HUMIDITY_1000L		HUMIDITY_1000L._c[6]
#define NETC_TX_c8_HUMIDITY_1000L		HUMIDITY_1000L._c[7]

#define NETC_TX_c1_COMPASS_A1		COMPASS_A1._c[0]
#define NETC_TX_c2_COMPASS_A1		COMPASS_A1._c[1]
#define NETC_TX_c3_COMPASS_A1		COMPASS_A1._c[2]

#define NETC_TX_c1_CBC_I5		CBC_I5._c[0]
#define NETC_TX_c2_CBC_I5		CBC_I5._c[1]
#define NETC_TX_c3_CBC_I5		CBC_I5._c[2]
#define NETC_TX_c4_CBC_I5		CBC_I5._c[3]

#define NETC_TX_c1_BCM_KEYON_COUNTER_B_CAN		BCM_KEYON_COUNTER_C_CAN._c[0]
#define NETC_TX_c2_BCM_KEYON_COUNTER_B_CAN		BCM_KEYON_COUNTER_C_CAN._c[1]

#define NETC_TX_c1_AMB_TEMP_DISP		AMB_TEMP_DISP._c[0]
#define NETC_TX_c2_AMB_TEMP_DISP		AMB_TEMP_DISP._c[1]

#define NETC_TX_c1_WCPM_STATUS		WCPM_STATUS._c[0]
#define NETC_TX_c2_WCPM_STATUS		WCPM_STATUS._c[1]

#define NETC_TX_c1_GE_B		GE_B._c[0]
#define NETC_TX_c2_GE_B		GE_B._c[1]
#define NETC_TX_c3_GE_B		GE_B._c[2]

#define NETC_TX_c1_ENVIRONMENTAL_CONDITIONS		ENVIRONMENTAL_CONDITIONS._c[0]
#define NETC_TX_c2_ENVIRONMENTAL_CONDITIONS		ENVIRONMENTAL_CONDITIONS._c[1]
#define NETC_TX_c3_ENVIRONMENTAL_CONDITIONS		ENVIRONMENTAL_CONDITIONS._c[2]
#define NETC_TX_c4_ENVIRONMENTAL_CONDITIONS		ENVIRONMENTAL_CONDITIONS._c[3]
#define NETC_TX_c5_ENVIRONMENTAL_CONDITIONS		ENVIRONMENTAL_CONDITIONS._c[4]
#define NETC_TX_c6_ENVIRONMENTAL_CONDITIONS		ENVIRONMENTAL_CONDITIONS._c[5]
#define NETC_TX_c7_ENVIRONMENTAL_CONDITIONS		ENVIRONMENTAL_CONDITIONS._c[6]
#define NETC_TX_c8_ENVIRONMENTAL_CONDITIONS		ENVIRONMENTAL_CONDITIONS._c[7]

#define NETC_TX_c1_CBC_VTA_1		CBC_VTA_1._c[0]

#define NETC_TX_c1_DIAGNOSTIC_RESPONSE_BCM_1		DIAGNOSTIC_RESPONSE_BCM_1._c[0]
#define NETC_TX_c2_DIAGNOSTIC_RESPONSE_BCM_1		DIAGNOSTIC_RESPONSE_BCM_1._c[1]
#define NETC_TX_c3_DIAGNOSTIC_RESPONSE_BCM_1		DIAGNOSTIC_RESPONSE_BCM_1._c[2]
#define NETC_TX_c4_DIAGNOSTIC_RESPONSE_BCM_1		DIAGNOSTIC_RESPONSE_BCM_1._c[3]
#define NETC_TX_c5_DIAGNOSTIC_RESPONSE_BCM_1		DIAGNOSTIC_RESPONSE_BCM_1._c[4]
#define NETC_TX_c6_DIAGNOSTIC_RESPONSE_BCM_1		DIAGNOSTIC_RESPONSE_BCM_1._c[5]
#define NETC_TX_c7_DIAGNOSTIC_RESPONSE_BCM_1		DIAGNOSTIC_RESPONSE_BCM_1._c[6]
#define NETC_TX_c8_DIAGNOSTIC_RESPONSE_BCM_1		DIAGNOSTIC_RESPONSE_BCM_1._c[7]

#define NETC_TX_c1_GW_C_I2		GW_C_I2._c[0]
#define NETC_TX_c2_GW_C_I2		GW_C_I2._c[1]
#define NETC_TX_c3_GW_C_I2		GW_C_I2._c[2]
#define NETC_TX_c4_GW_C_I2		GW_C_I2._c[3]
#define NETC_TX_c5_GW_C_I2		GW_C_I2._c[4]

#define NETC_TX_c1_DAS_B		DAS_B._c[0]
#define NETC_TX_c2_DAS_B		DAS_B._c[1]
#define NETC_TX_c3_DAS_B		DAS_B._c[2]
#define NETC_TX_c4_DAS_B		DAS_B._c[3]

#define NETC_TX_c1_CBC_I2		CBC_I2._c[0]
#define NETC_TX_c2_CBC_I2		CBC_I2._c[1]
#define NETC_TX_c3_CBC_I2		CBC_I2._c[2]
#define NETC_TX_c4_CBC_I2		CBC_I2._c[3]
#define NETC_TX_c5_CBC_I2		CBC_I2._c[4]
#define NETC_TX_c6_CBC_I2		CBC_I2._c[5]
#define NETC_TX_c7_CBC_I2		CBC_I2._c[6]
#define NETC_TX_c8_CBC_I2		CBC_I2._c[7]

#define NETC_TX_c1_CBC_I1		CBC_I1._c[0]
#define NETC_TX_c2_CBC_I1		CBC_I1._c[1]
#define NETC_TX_c3_CBC_I1		CBC_I1._c[2]

#define NETC_TX_c1_STATUS_B_EPB		STATUS_C_EPB._c[0]
#define NETC_TX_c2_STATUS_B_EPB		STATUS_C_EPB._c[1]
#define NETC_TX_c3_STATUS_B_EPB		STATUS_C_EPB._c[2]
#define NETC_TX_c4_STATUS_B_EPB		STATUS_C_EPB._c[3]
#define NETC_TX_c5_STATUS_B_EPB		STATUS_C_EPB._c[4]
#define NETC_TX_c6_STATUS_B_EPB		STATUS_C_EPB._c[5]
#define NETC_TX_c7_STATUS_B_EPB		STATUS_C_EPB._c[6]

#define NETC_TX_c1_STATUS_C_CAN		STATUS_C_CAN._c[0]
#define NETC_TX_c2_STATUS_C_CAN		STATUS_C_CAN._c[1]
#define NETC_TX_c3_STATUS_C_CAN		STATUS_C_CAN._c[2]
#define NETC_TX_c4_STATUS_C_CAN		STATUS_C_CAN._c[3]

#define NETC_TX_c1_STATUS_B_HALF		STATUS_C_HALF._c[0]
#define NETC_TX_c2_STATUS_B_HALF		STATUS_C_HALF._c[1]
#define NETC_TX_c3_STATUS_B_HALF		STATUS_C_HALF._c[2]
#define NETC_TX_c4_STATUS_B_HALF		STATUS_C_HALF._c[3]
#define NETC_TX_c5_STATUS_B_HALF		STATUS_C_HALF._c[4]
#define NETC_TX_c6_STATUS_B_HALF		STATUS_C_HALF._c[5]
#define NETC_TX_c7_STATUS_B_HALF		STATUS_C_HALF._c[6]
#define NETC_TX_c8_STATUS_B_HALF		STATUS_C_HALF._c[7]

#define NETC_TX_c1_HALF_B_Warning_RQ		HALF_C_Warning_RQ._c[0]
#define NETC_TX_c2_HALF_B_Warning_RQ		HALF_C_Warning_RQ._c[1]

#define NETC_TX_c1_SWS_8		SWS_8._c[0]
#define NETC_TX_c2_SWS_8		SWS_8._c[1]
#define NETC_TX_c3_SWS_8		SWS_8._c[2]
#define NETC_TX_c4_SWS_8		SWS_8._c[3]
#define NETC_TX_c5_SWS_8		SWS_8._c[4]
#define NETC_TX_c6_SWS_8		SWS_8._c[5]
#define NETC_TX_c7_SWS_8		SWS_8._c[6]
#define NETC_TX_c8_SWS_8		SWS_8._c[7]

#define NETC_TX_c1_StW_Actn_Rq_1		StW_Actn_Rq_1._c[0]
#define NETC_TX_c2_StW_Actn_Rq_1		StW_Actn_Rq_1._c[1]
#define NETC_TX_c3_StW_Actn_Rq_1		StW_Actn_Rq_1._c[2]
#define NETC_TX_c4_StW_Actn_Rq_1		StW_Actn_Rq_1._c[3]
#define NETC_TX_c5_StW_Actn_Rq_1		StW_Actn_Rq_1._c[4]
#define NETC_TX_c6_StW_Actn_Rq_1		StW_Actn_Rq_1._c[5]
#define NETC_TX_c7_StW_Actn_Rq_1		StW_Actn_Rq_1._c[6]

#define NETC_TX_c1_PAM_B		PAM_B._c[0]
#define NETC_TX_c2_PAM_B		PAM_B._c[1]
#define NETC_TX_c3_PAM_B		PAM_B._c[2]
#define NETC_TX_c4_PAM_B		PAM_B._c[3]
#define NETC_TX_c5_PAM_B		PAM_B._c[4]

#define NETC_TX_c1_GW_C_I3		GW_C_I3._c[0]
#define NETC_TX_c2_GW_C_I3		GW_C_I3._c[1]

#define NETC_TX_c1_VIN_1		VIN_0._c[0]
#define NETC_TX_c2_VIN_1		VIN_0._c[1]
#define NETC_TX_c3_VIN_1		VIN_0._c[2]
#define NETC_TX_c4_VIN_1		VIN_0._c[3]
#define NETC_TX_c5_VIN_1		VIN_0._c[4]
#define NETC_TX_c6_VIN_1		VIN_0._c[5]
#define NETC_TX_c7_VIN_1		VIN_0._c[6]
#define NETC_TX_c8_VIN_1		VIN_0._c[7]

#define NETC_TX_c1_VEHICLE_SPEED_ODOMETER_1		VEHICLE_SPEED_ODOMETER_1._c[0]
#define NETC_TX_c2_VEHICLE_SPEED_ODOMETER_1		VEHICLE_SPEED_ODOMETER_1._c[1]
#define NETC_TX_c3_VEHICLE_SPEED_ODOMETER_1		VEHICLE_SPEED_ODOMETER_1._c[2]
#define NETC_TX_c4_VEHICLE_SPEED_ODOMETER_1		VEHICLE_SPEED_ODOMETER_1._c[3]

#define NETC_TX_c1_GW_C_I6		GW_C_I6._c[0]
#define NETC_TX_c2_GW_C_I6		GW_C_I6._c[1]
#define NETC_TX_c3_GW_C_I6		GW_C_I6._c[2]
#define NETC_TX_c4_GW_C_I6		GW_C_I6._c[3]

#define NETC_TX_c1_DYNAMIC_VEHICLE_INFO2		DYNAMIC_VEHICLE_INFO2._c[0]
#define NETC_TX_c2_DYNAMIC_VEHICLE_INFO2		DYNAMIC_VEHICLE_INFO2._c[1]
#define NETC_TX_c3_DYNAMIC_VEHICLE_INFO2		DYNAMIC_VEHICLE_INFO2._c[2]
#define NETC_TX_c4_DYNAMIC_VEHICLE_INFO2		DYNAMIC_VEHICLE_INFO2._c[3]
#define NETC_TX_c5_DYNAMIC_VEHICLE_INFO2		DYNAMIC_VEHICLE_INFO2._c[4]
#define NETC_TX_c6_DYNAMIC_VEHICLE_INFO2		DYNAMIC_VEHICLE_INFO2._c[5]
#define NETC_TX_c7_DYNAMIC_VEHICLE_INFO2		DYNAMIC_VEHICLE_INFO2._c[6]
#define NETC_TX_c8_DYNAMIC_VEHICLE_INFO2		DYNAMIC_VEHICLE_INFO2._c[7]

#define NETC_TX_c1_STATUS_B_TRANSMISSION		STATUS_C_TRANSMISSION._c[0]
#define NETC_TX_c2_STATUS_B_TRANSMISSION		STATUS_C_TRANSMISSION._c[1]
#define NETC_TX_c3_STATUS_B_TRANSMISSION		STATUS_C_TRANSMISSION._c[2]
#define NETC_TX_c4_STATUS_B_TRANSMISSION		STATUS_C_TRANSMISSION._c[3]
#define NETC_TX_c5_STATUS_B_TRANSMISSION		STATUS_C_TRANSMISSION._c[4]
#define NETC_TX_c6_STATUS_B_TRANSMISSION		STATUS_C_TRANSMISSION._c[5]
#define NETC_TX_c7_STATUS_B_TRANSMISSION		STATUS_C_TRANSMISSION._c[6]
#define NETC_TX_c8_STATUS_B_TRANSMISSION		STATUS_C_TRANSMISSION._c[7]

#define NETC_TX_c1_NWM_BCM		NWM_BCM._c[0]
#define NETC_TX_c2_NWM_BCM		NWM_BCM._c[1]
#define NETC_TX_c3_NWM_BCM		NWM_BCM._c[2]
#define NETC_TX_c4_NWM_BCM		NWM_BCM._c[3]
#define NETC_TX_c5_NWM_BCM		NWM_BCM._c[4]
#define NETC_TX_c6_NWM_BCM		NWM_BCM._c[5]

#define NETC_TX_c1_STATUS_BCM2		STATUS_BCM2._c[0]
#define NETC_TX_c2_STATUS_BCM2		STATUS_BCM2._c[1]
#define NETC_TX_c3_STATUS_BCM2		STATUS_BCM2._c[2]
#define NETC_TX_c4_STATUS_BCM2		STATUS_BCM2._c[3]
#define NETC_TX_c5_STATUS_BCM2		STATUS_BCM2._c[4]
#define NETC_TX_c6_STATUS_BCM2		STATUS_BCM2._c[5]
#define NETC_TX_c7_STATUS_BCM2		STATUS_BCM2._c[6]
#define NETC_TX_c8_STATUS_BCM2		STATUS_BCM2._c[7]

#define NETC_TX_c1_STATUS_BCM		STATUS_BCM._c[0]
#define NETC_TX_c2_STATUS_BCM		STATUS_BCM._c[1]
#define NETC_TX_c3_STATUS_BCM		STATUS_BCM._c[2]
#define NETC_TX_c4_STATUS_BCM		STATUS_BCM._c[3]
#define NETC_TX_c5_STATUS_BCM		STATUS_BCM._c[4]
#define NETC_TX_c6_STATUS_BCM		STATUS_BCM._c[5]
#define NETC_TX_c7_STATUS_BCM		STATUS_BCM._c[6]
#define NETC_TX_c8_STATUS_BCM		STATUS_BCM._c[7]

#define NETC_TX_c1_EXTERNAL_LIGHTS_1		EXTERNAL_LIGHTS_1._c[0]
#define NETC_TX_c2_EXTERNAL_LIGHTS_1		EXTERNAL_LIGHTS_1._c[1]
#define NETC_TX_c3_EXTERNAL_LIGHTS_1		EXTERNAL_LIGHTS_1._c[2]
#define NETC_TX_c4_EXTERNAL_LIGHTS_1		EXTERNAL_LIGHTS_1._c[3]
#define NETC_TX_c5_EXTERNAL_LIGHTS_1		EXTERNAL_LIGHTS_1._c[4]
#define NETC_TX_c6_EXTERNAL_LIGHTS_1		EXTERNAL_LIGHTS_1._c[5]

#define NETC_TX_c1_RFHUB_B_A2		RFHUB_A2._c[0]
#define NETC_TX_c2_RFHUB_B_A2		RFHUB_A2._c[1]
#define NETC_TX_c3_RFHUB_B_A2		RFHUB_A2._c[2]
#define NETC_TX_c4_RFHUB_B_A2		RFHUB_A2._c[3]
#define NETC_TX_c5_RFHUB_B_A2		RFHUB_A2._c[4]

#define NETC_TX_c1_CBC_I4		CBC_I4._c[0]
#define NETC_TX_c2_CBC_I4		CBC_I4._c[1]
#define NETC_TX_c3_CBC_I4		CBC_I4._c[2]
#define NETC_TX_c4_CBC_I4		CBC_I4._c[3]
#define NETC_TX_c5_CBC_I4		CBC_I4._c[4]

#define NETC_TX_c1_RFHUB_B_A4		RFHUB_A4._c[0]

#define NETC_TX_c1_STATUS_B_ECM2		STATUS_C_ECM2._c[0]
#define NETC_TX_c2_STATUS_B_ECM2		STATUS_C_ECM2._c[1]
#define NETC_TX_c3_STATUS_B_ECM2		STATUS_C_ECM2._c[2]
#define NETC_TX_c4_STATUS_B_ECM2		STATUS_C_ECM2._c[3]
#define NETC_TX_c5_STATUS_B_ECM2		STATUS_C_ECM2._c[4]
#define NETC_TX_c6_STATUS_B_ECM2		STATUS_C_ECM2._c[5]
#define NETC_TX_c7_STATUS_B_ECM2		STATUS_C_ECM2._c[6]
#define NETC_TX_c8_STATUS_B_ECM2		STATUS_C_ECM2._c[7]

#define NETC_TX_c1_STATUS_B_ECM		STATUS_C_ECM._c[0]
#define NETC_TX_c2_STATUS_B_ECM		STATUS_C_ECM._c[1]
#define NETC_TX_c3_STATUS_B_ECM		STATUS_C_ECM._c[2]
#define NETC_TX_c4_STATUS_B_ECM		STATUS_C_ECM._c[3]
#define NETC_TX_c5_STATUS_B_ECM		STATUS_C_ECM._c[4]
#define NETC_TX_c6_STATUS_B_ECM		STATUS_C_ECM._c[5]
#define NETC_TX_c7_STATUS_B_ECM		STATUS_C_ECM._c[6]
#define NETC_TX_c8_STATUS_B_ECM		STATUS_C_ECM._c[7]

#define NETC_TX_c1_STATUS_B_BSM		STATUS_C_BSM._c[0]
#define NETC_TX_c2_STATUS_B_BSM		STATUS_C_BSM._c[1]
#define NETC_TX_c3_STATUS_B_BSM		STATUS_C_BSM._c[2]
#define NETC_TX_c4_STATUS_B_BSM		STATUS_C_BSM._c[3]
#define NETC_TX_c5_STATUS_B_BSM		STATUS_C_BSM._c[4]
#define NETC_TX_c6_STATUS_B_BSM		STATUS_C_BSM._c[5]
#define NETC_TX_c7_STATUS_B_BSM		STATUS_C_BSM._c[6]
#define NETC_TX_c8_STATUS_B_BSM		STATUS_C_BSM._c[7]

#define NETC_TX_c1_TxDynamicMsg0_1		TxDynamicMsg0_1._c[0]
#define NETC_TX_c2_TxDynamicMsg0_1		TxDynamicMsg0_1._c[1]
#define NETC_TX_c3_TxDynamicMsg0_1		TxDynamicMsg0_1._c[2]
#define NETC_TX_c4_TxDynamicMsg0_1		TxDynamicMsg0_1._c[3]
#define NETC_TX_c5_TxDynamicMsg0_1		TxDynamicMsg0_1._c[4]
#define NETC_TX_c6_TxDynamicMsg0_1		TxDynamicMsg0_1._c[5]
#define NETC_TX_c7_TxDynamicMsg0_1		TxDynamicMsg0_1._c[6]
#define NETC_TX_c8_TxDynamicMsg0_1		TxDynamicMsg0_1._c[7]

#define NETC_TX_c1_TxDynamicMsg1_1		TxDynamicMsg1_1._c[0]
#define NETC_TX_c2_TxDynamicMsg1_1		TxDynamicMsg1_1._c[1]
#define NETC_TX_c3_TxDynamicMsg1_1		TxDynamicMsg1_1._c[2]
#define NETC_TX_c4_TxDynamicMsg1_1		TxDynamicMsg1_1._c[3]
#define NETC_TX_c5_TxDynamicMsg1_1		TxDynamicMsg1_1._c[4]
#define NETC_TX_c6_TxDynamicMsg1_1		TxDynamicMsg1_1._c[5]
#define NETC_TX_c7_TxDynamicMsg1_1		TxDynamicMsg1_1._c[6]
#define NETC_TX_c8_TxDynamicMsg1_1		TxDynamicMsg1_1._c[7]


/*************************************************************/
/* Handles of send objects                                   */
/*************************************************************/

#define NETC_TX_STATUS_C_BCM2	0
#define NETC_TX_HVAC_INFO	1
#define NETC_TX_CBC_PT4	2
#define NETC_TX_BCM_KEYON_COUNTER_C_CAN	3
#define NETC_TX_IBS4	4
#define NETC_TX_ECU_APPL_BCM_0	5
#define NETC_TX_CONFIGURATION_DATA_CODE_REQUEST_0	6
#define NETC_TX_CBC_VTA_0	7
#define NETC_TX_DIAGNOSTIC_RESPONSE_BCM_0	8
#define NETC_TX_STATUS_B_CAN2	9
#define NETC_TX_EXTERNAL_LIGHTS_0	10
#define NETC_TX_ASBM1_CONTROL	11
#define NETC_TX_HVAC_INFO2	12
#define NETC_TX_IBS2	13
#define NETC_TX_VEHICLE_SPEED_ODOMETER_0	14
#define NETC_TX_STATUS_C_BCM	15
#define NETC_TX_STATUS_B_CAN	16
#define NETC_TX_IBS3	17
#define NETC_TX_IBS1	18
#define NETC_TX_BATTERY_INFO	19
#define NETC_TX_CFG_Feature	20
#define NETC_TX_CBC_PT3	21
#define NETC_TX_CBC_PT1	22
#define NETC_TX_WAKE_C_BCM	23
#define NETC_TX_CBC_PT2	24
#define NETC_TX_BCM_COMMAND	25
#define NETC_TX_BCM_CODE_TRM_REQUEST	26
#define NETC_TX_BCM_CODE_ESL_REQUEST	27
#define NETC_TX_BCM_MINICRYPT_ACK	28
#define NETC_TX_IMMO_CODE_RESPONSE	29
#define NETC_TX_TxDynamicMsg0_0	30
#define NETC_TX_TxDynamicMsg1_0	31
#define NETC_TX_ECU_APPL_BCM_1	32
#define NETC_TX_DTO_BCM	33
#define NETC_TX_CONFIGURATION_DATA_CODE_REQUEST_1	34
#define NETC_TX_IBS_2	35
#define NETC_TX_HUMIDITY_1000L	36
#define NETC_TX_COMPASS_A1	37
#define NETC_TX_CBC_I5	38
#define NETC_TX_BCM_KEYON_COUNTER_B_CAN	39
#define NETC_TX_AMB_TEMP_DISP	40
#define NETC_TX_WCPM_STATUS	41
#define NETC_TX_GE_B	42
#define NETC_TX_ENVIRONMENTAL_CONDITIONS	43
#define NETC_TX_CBC_VTA_1	44
#define NETC_TX_DIAGNOSTIC_RESPONSE_BCM_1	45
#define NETC_TX_GW_C_I2	46
#define NETC_TX_DAS_B	47
#define NETC_TX_CBC_I2	48
#define NETC_TX_CBC_I1	49
#define NETC_TX_STATUS_B_EPB	50
#define NETC_TX_STATUS_C_CAN	51
#define NETC_TX_STATUS_B_HALF	52
#define NETC_TX_HALF_B_Warning_RQ	53
#define NETC_TX_SWS_8	54
#define NETC_TX_StW_Actn_Rq_1	55
#define NETC_TX_PAM_B	56
#define NETC_TX_GW_C_I3	57
#define NETC_TX_VIN_1	58
#define NETC_TX_VEHICLE_SPEED_ODOMETER_1	59
#define NETC_TX_GW_C_I6	60
#define NETC_TX_DYNAMIC_VEHICLE_INFO2	61
#define NETC_TX_STATUS_B_TRANSMISSION	62
#define NETC_TX_NWM_BCM	63
#define NETC_TX_STATUS_BCM2	64
#define NETC_TX_STATUS_BCM	65
#define NETC_TX_EXTERNAL_LIGHTS_1	66
#define NETC_TX_RFHUB_B_A2	67
#define NETC_TX_CBC_I4	68
#define NETC_TX_RFHUB_B_A4	69
#define NETC_TX_STATUS_B_ECM2	70
#define NETC_TX_STATUS_B_ECM	71
#define NETC_TX_STATUS_B_BSM	72
#define NETC_TX_TxDynamicMsg0_1	73
#define NETC_TX_TxDynamicMsg1_1	74

#if ! defined (CanStartFlagWriteSync)
	#error "Incompatible CAN driver - CanStartFlagWriteSync is missing."
#endif
#if ! defined (CanEndFlagWriteSync)
	#error "Incompatible CAN driver - CanEndFlagWriteSync is missing."
#endif

/*************************************************************/
/* Databuffer for confirmationflags                          */
/*************************************************************/

#ifdef C_ENABLE_CONFIRMATION_FLAG

#define NETC_TX_STATUS_C_BCM2_conf_flag 	CanConfirmationFlags.w[0].b0
#define CanWriteSyncSTATUS_C_BCM2_conf_flag(x)\
		CanStartFlagWriteSync();\
		NETC_TX_STATUS_C_BCM2_conf_flag = x;\
		CanEndFlagWriteSync();

#define NETC_TX_HVAC_INFO_conf_flag 	CanConfirmationFlags.w[0].b1
#define CanWriteSyncHVAC_INFO_conf_flag(x)\
		CanStartFlagWriteSync();\
		NETC_TX_HVAC_INFO_conf_flag = x;\
		CanEndFlagWriteSync();

#define NETC_TX_CBC_PT4_conf_flag 	CanConfirmationFlags.w[0].b2
#define CanWriteSyncCBC_PT4_conf_flag(x)\
		CanStartFlagWriteSync();\
		NETC_TX_CBC_PT4_conf_flag = x;\
		CanEndFlagWriteSync();

#define NETC_TX_BCM_KEYON_COUNTER_C_CAN_conf_flag 	CanConfirmationFlags.w[0].b3
#define CanWriteSyncBCM_KEYON_COUNTER_C_CAN_conf_flag(x)\
		CanStartFlagWriteSync();\
		NETC_TX_BCM_KEYON_COUNTER_C_CAN_conf_flag = x;\
		CanEndFlagWriteSync();

#define NETC_TX_IBS4_conf_flag 	CanConfirmationFlags.w[0].b4
#define CanWriteSyncIBS4_conf_flag(x)\
		CanStartFlagWriteSync();\
		NETC_TX_IBS4_conf_flag = x;\
		CanEndFlagWriteSync();

#define NETC_TX_ECU_APPL_BCM_0_conf_flag 	CanConfirmationFlags.w[0].b5
#define CanWriteSyncECU_APPL_BCM_0_conf_flag(x)\
		CanStartFlagWriteSync();\
		NETC_TX_ECU_APPL_BCM_0_conf_flag = x;\
		CanEndFlagWriteSync();

#define NETC_TX_CONFIGURATION_DATA_CODE_REQUEST_0_conf_flag 	CanConfirmationFlags.w[0].b6
#define CanWriteSyncCONFIGURATION_DATA_CODE_REQUEST_0_conf_flag(x)\
		CanStartFlagWriteSync();\
		NETC_TX_CONFIGURATION_DATA_CODE_REQUEST_0_conf_flag = x;\
		CanEndFlagWriteSync();

#define NETC_TX_CBC_VTA_0_conf_flag 	CanConfirmationFlags.w[0].b7
#define CanWriteSyncCBC_VTA_0_conf_flag(x)\
		CanStartFlagWriteSync();\
		NETC_TX_CBC_VTA_0_conf_flag = x;\
		CanEndFlagWriteSync();

#define NETC_TX_DIAGNOSTIC_RESPONSE_BCM_0_conf_flag 	CanConfirmationFlags.w[0].b10
#define CanWriteSyncDIAGNOSTIC_RESPONSE_BCM_0_conf_flag(x)\
		CanStartFlagWriteSync();\
		NETC_TX_DIAGNOSTIC_RESPONSE_BCM_0_conf_flag = x;\
		CanEndFlagWriteSync();

#define NETC_TX_STATUS_B_CAN2_conf_flag 	CanConfirmationFlags.w[0].b11
#define CanWriteSyncSTATUS_B_CAN2_conf_flag(x)\
		CanStartFlagWriteSync();\
		NETC_TX_STATUS_B_CAN2_conf_flag = x;\
		CanEndFlagWriteSync();

#define NETC_TX_EXTERNAL_LIGHTS_0_conf_flag 	CanConfirmationFlags.w[0].b12
#define CanWriteSyncEXTERNAL_LIGHTS_0_conf_flag(x)\
		CanStartFlagWriteSync();\
		NETC_TX_EXTERNAL_LIGHTS_0_conf_flag = x;\
		CanEndFlagWriteSync();

#define NETC_TX_ASBM1_CONTROL_conf_flag 	CanConfirmationFlags.w[0].b13
#define CanWriteSyncASBM1_CONTROL_conf_flag(x)\
		CanStartFlagWriteSync();\
		NETC_TX_ASBM1_CONTROL_conf_flag = x;\
		CanEndFlagWriteSync();

#define NETC_TX_HVAC_INFO2_conf_flag 	CanConfirmationFlags.w[0].b14
#define CanWriteSyncHVAC_INFO2_conf_flag(x)\
		CanStartFlagWriteSync();\
		NETC_TX_HVAC_INFO2_conf_flag = x;\
		CanEndFlagWriteSync();

#define NETC_TX_IBS2_conf_flag 	CanConfirmationFlags.w[0].b15
#define CanWriteSyncIBS2_conf_flag(x)\
		CanStartFlagWriteSync();\
		NETC_TX_IBS2_conf_flag = x;\
		CanEndFlagWriteSync();

#define NETC_TX_VEHICLE_SPEED_ODOMETER_0_conf_flag 	CanConfirmationFlags.w[0].b16
#define CanWriteSyncVEHICLE_SPEED_ODOMETER_0_conf_flag(x)\
		CanStartFlagWriteSync();\
		NETC_TX_VEHICLE_SPEED_ODOMETER_0_conf_flag = x;\
		CanEndFlagWriteSync();

#define NETC_TX_STATUS_C_BCM_conf_flag 	CanConfirmationFlags.w[0].b17
#define CanWriteSyncSTATUS_C_BCM_conf_flag(x)\
		CanStartFlagWriteSync();\
		NETC_TX_STATUS_C_BCM_conf_flag = x;\
		CanEndFlagWriteSync();

#define NETC_TX_STATUS_B_CAN_conf_flag 	CanConfirmationFlags.w[1].b0
#define CanWriteSyncSTATUS_B_CAN_conf_flag(x)\
		CanStartFlagWriteSync();\
		NETC_TX_STATUS_B_CAN_conf_flag = x;\
		CanEndFlagWriteSync();

#define NETC_TX_IBS3_conf_flag 	CanConfirmationFlags.w[1].b1
#define CanWriteSyncIBS3_conf_flag(x)\
		CanStartFlagWriteSync();\
		NETC_TX_IBS3_conf_flag = x;\
		CanEndFlagWriteSync();

#define NETC_TX_IBS1_conf_flag 	CanConfirmationFlags.w[1].b2
#define CanWriteSyncIBS1_conf_flag(x)\
		CanStartFlagWriteSync();\
		NETC_TX_IBS1_conf_flag = x;\
		CanEndFlagWriteSync();

#define NETC_TX_BATTERY_INFO_conf_flag 	CanConfirmationFlags.w[1].b3
#define CanWriteSyncBATTERY_INFO_conf_flag(x)\
		CanStartFlagWriteSync();\
		NETC_TX_BATTERY_INFO_conf_flag = x;\
		CanEndFlagWriteSync();

#define NETC_TX_CFG_Feature_conf_flag 	CanConfirmationFlags.w[1].b4
#define CanWriteSyncCFG_Feature_conf_flag(x)\
		CanStartFlagWriteSync();\
		NETC_TX_CFG_Feature_conf_flag = x;\
		CanEndFlagWriteSync();

#define NETC_TX_CBC_PT3_conf_flag 	CanConfirmationFlags.w[1].b5
#define CanWriteSyncCBC_PT3_conf_flag(x)\
		CanStartFlagWriteSync();\
		NETC_TX_CBC_PT3_conf_flag = x;\
		CanEndFlagWriteSync();

#define NETC_TX_CBC_PT1_conf_flag 	CanConfirmationFlags.w[1].b6
#define CanWriteSyncCBC_PT1_conf_flag(x)\
		CanStartFlagWriteSync();\
		NETC_TX_CBC_PT1_conf_flag = x;\
		CanEndFlagWriteSync();

#define NETC_TX_WAKE_C_BCM_conf_flag 	CanConfirmationFlags.w[1].b7
#define CanWriteSyncWAKE_C_BCM_conf_flag(x)\
		CanStartFlagWriteSync();\
		NETC_TX_WAKE_C_BCM_conf_flag = x;\
		CanEndFlagWriteSync();

#define NETC_TX_CBC_PT2_conf_flag 	CanConfirmationFlags.w[1].b10
#define CanWriteSyncCBC_PT2_conf_flag(x)\
		CanStartFlagWriteSync();\
		NETC_TX_CBC_PT2_conf_flag = x;\
		CanEndFlagWriteSync();

#define NETC_TX_BCM_COMMAND_conf_flag 	CanConfirmationFlags.w[1].b11
#define CanWriteSyncBCM_COMMAND_conf_flag(x)\
		CanStartFlagWriteSync();\
		NETC_TX_BCM_COMMAND_conf_flag = x;\
		CanEndFlagWriteSync();

#define NETC_TX_BCM_CODE_TRM_REQUEST_conf_flag 	CanConfirmationFlags.w[1].b12
#define CanWriteSyncBCM_CODE_TRM_REQUEST_conf_flag(x)\
		CanStartFlagWriteSync();\
		NETC_TX_BCM_CODE_TRM_REQUEST_conf_flag = x;\
		CanEndFlagWriteSync();

#define NETC_TX_BCM_CODE_ESL_REQUEST_conf_flag 	CanConfirmationFlags.w[1].b13
#define CanWriteSyncBCM_CODE_ESL_REQUEST_conf_flag(x)\
		CanStartFlagWriteSync();\
		NETC_TX_BCM_CODE_ESL_REQUEST_conf_flag = x;\
		CanEndFlagWriteSync();

#define NETC_TX_BCM_MINICRYPT_ACK_conf_flag 	CanConfirmationFlags.w[1].b14
#define CanWriteSyncBCM_MINICRYPT_ACK_conf_flag(x)\
		CanStartFlagWriteSync();\
		NETC_TX_BCM_MINICRYPT_ACK_conf_flag = x;\
		CanEndFlagWriteSync();

#define NETC_TX_IMMO_CODE_RESPONSE_conf_flag 	CanConfirmationFlags.w[1].b15
#define CanWriteSyncIMMO_CODE_RESPONSE_conf_flag(x)\
		CanStartFlagWriteSync();\
		NETC_TX_IMMO_CODE_RESPONSE_conf_flag = x;\
		CanEndFlagWriteSync();

#define NETC_TX_ECU_APPL_BCM_1_conf_flag 	CanConfirmationFlags.w[1].b16
#define CanWriteSyncECU_APPL_BCM_1_conf_flag(x)\
		CanStartFlagWriteSync();\
		NETC_TX_ECU_APPL_BCM_1_conf_flag = x;\
		CanEndFlagWriteSync();

#define NETC_TX_DTO_BCM_conf_flag 	CanConfirmationFlags.w[1].b17
#define CanWriteSyncDTO_BCM_conf_flag(x)\
		CanStartFlagWriteSync();\
		NETC_TX_DTO_BCM_conf_flag = x;\
		CanEndFlagWriteSync();

#define NETC_TX_CONFIGURATION_DATA_CODE_REQUEST_1_conf_flag 	CanConfirmationFlags.w[2].b0
#define CanWriteSyncCONFIGURATION_DATA_CODE_REQUEST_1_conf_flag(x)\
		CanStartFlagWriteSync();\
		NETC_TX_CONFIGURATION_DATA_CODE_REQUEST_1_conf_flag = x;\
		CanEndFlagWriteSync();

#define NETC_TX_IBS_2_conf_flag 	CanConfirmationFlags.w[2].b1
#define CanWriteSyncIBS_2_conf_flag(x)\
		CanStartFlagWriteSync();\
		NETC_TX_IBS_2_conf_flag = x;\
		CanEndFlagWriteSync();

#define NETC_TX_HUMIDITY_1000L_conf_flag 	CanConfirmationFlags.w[2].b2
#define CanWriteSyncHUMIDITY_1000L_conf_flag(x)\
		CanStartFlagWriteSync();\
		NETC_TX_HUMIDITY_1000L_conf_flag = x;\
		CanEndFlagWriteSync();

#define NETC_TX_COMPASS_A1_conf_flag 	CanConfirmationFlags.w[2].b3
#define CanWriteSyncCOMPASS_A1_conf_flag(x)\
		CanStartFlagWriteSync();\
		NETC_TX_COMPASS_A1_conf_flag = x;\
		CanEndFlagWriteSync();

#define NETC_TX_CBC_I5_conf_flag 	CanConfirmationFlags.w[2].b4
#define CanWriteSyncCBC_I5_conf_flag(x)\
		CanStartFlagWriteSync();\
		NETC_TX_CBC_I5_conf_flag = x;\
		CanEndFlagWriteSync();

#define NETC_TX_BCM_KEYON_COUNTER_B_CAN_conf_flag 	CanConfirmationFlags.w[2].b5
#define CanWriteSyncBCM_KEYON_COUNTER_B_CAN_conf_flag(x)\
		CanStartFlagWriteSync();\
		NETC_TX_BCM_KEYON_COUNTER_B_CAN_conf_flag = x;\
		CanEndFlagWriteSync();

#define NETC_TX_AMB_TEMP_DISP_conf_flag 	CanConfirmationFlags.w[2].b6
#define CanWriteSyncAMB_TEMP_DISP_conf_flag(x)\
		CanStartFlagWriteSync();\
		NETC_TX_AMB_TEMP_DISP_conf_flag = x;\
		CanEndFlagWriteSync();

#define NETC_TX_WCPM_STATUS_conf_flag 	CanConfirmationFlags.w[2].b7
#define CanWriteSyncWCPM_STATUS_conf_flag(x)\
		CanStartFlagWriteSync();\
		NETC_TX_WCPM_STATUS_conf_flag = x;\
		CanEndFlagWriteSync();

#define NETC_TX_GE_B_conf_flag 	CanConfirmationFlags.w[2].b10
#define CanWriteSyncGE_B_conf_flag(x)\
		CanStartFlagWriteSync();\
		NETC_TX_GE_B_conf_flag = x;\
		CanEndFlagWriteSync();

#define NETC_TX_ENVIRONMENTAL_CONDITIONS_conf_flag 	CanConfirmationFlags.w[2].b11
#define CanWriteSyncENVIRONMENTAL_CONDITIONS_conf_flag(x)\
		CanStartFlagWriteSync();\
		NETC_TX_ENVIRONMENTAL_CONDITIONS_conf_flag = x;\
		CanEndFlagWriteSync();

#define NETC_TX_CBC_VTA_1_conf_flag 	CanConfirmationFlags.w[2].b12
#define CanWriteSyncCBC_VTA_1_conf_flag(x)\
		CanStartFlagWriteSync();\
		NETC_TX_CBC_VTA_1_conf_flag = x;\
		CanEndFlagWriteSync();

#define NETC_TX_DIAGNOSTIC_RESPONSE_BCM_1_conf_flag 	CanConfirmationFlags.w[2].b13
#define CanWriteSyncDIAGNOSTIC_RESPONSE_BCM_1_conf_flag(x)\
		CanStartFlagWriteSync();\
		NETC_TX_DIAGNOSTIC_RESPONSE_BCM_1_conf_flag = x;\
		CanEndFlagWriteSync();

#define NETC_TX_GW_C_I2_conf_flag 	CanConfirmationFlags.w[2].b14
#define CanWriteSyncGW_C_I2_conf_flag(x)\
		CanStartFlagWriteSync();\
		NETC_TX_GW_C_I2_conf_flag = x;\
		CanEndFlagWriteSync();

#define NETC_TX_DAS_B_conf_flag 	CanConfirmationFlags.w[2].b15
#define CanWriteSyncDAS_B_conf_flag(x)\
		CanStartFlagWriteSync();\
		NETC_TX_DAS_B_conf_flag = x;\
		CanEndFlagWriteSync();

#define NETC_TX_CBC_I2_conf_flag 	CanConfirmationFlags.w[2].b16
#define CanWriteSyncCBC_I2_conf_flag(x)\
		CanStartFlagWriteSync();\
		NETC_TX_CBC_I2_conf_flag = x;\
		CanEndFlagWriteSync();

#define NETC_TX_CBC_I1_conf_flag 	CanConfirmationFlags.w[2].b17
#define CanWriteSyncCBC_I1_conf_flag(x)\
		CanStartFlagWriteSync();\
		NETC_TX_CBC_I1_conf_flag = x;\
		CanEndFlagWriteSync();

#define NETC_TX_STATUS_B_EPB_conf_flag 	CanConfirmationFlags.w[3].b0
#define CanWriteSyncSTATUS_B_EPB_conf_flag(x)\
		CanStartFlagWriteSync();\
		NETC_TX_STATUS_B_EPB_conf_flag = x;\
		CanEndFlagWriteSync();

#define NETC_TX_STATUS_C_CAN_conf_flag 	CanConfirmationFlags.w[3].b1
#define CanWriteSyncSTATUS_C_CAN_conf_flag(x)\
		CanStartFlagWriteSync();\
		NETC_TX_STATUS_C_CAN_conf_flag = x;\
		CanEndFlagWriteSync();

#define NETC_TX_STATUS_B_HALF_conf_flag 	CanConfirmationFlags.w[3].b2
#define CanWriteSyncSTATUS_B_HALF_conf_flag(x)\
		CanStartFlagWriteSync();\
		NETC_TX_STATUS_B_HALF_conf_flag = x;\
		CanEndFlagWriteSync();

#define NETC_TX_HALF_B_Warning_RQ_conf_flag 	CanConfirmationFlags.w[3].b3
#define CanWriteSyncHALF_B_Warning_RQ_conf_flag(x)\
		CanStartFlagWriteSync();\
		NETC_TX_HALF_B_Warning_RQ_conf_flag = x;\
		CanEndFlagWriteSync();

#define NETC_TX_SWS_8_conf_flag 	CanConfirmationFlags.w[3].b4
#define CanWriteSyncSWS_8_conf_flag(x)\
		CanStartFlagWriteSync();\
		NETC_TX_SWS_8_conf_flag = x;\
		CanEndFlagWriteSync();

#define NETC_TX_StW_Actn_Rq_1_conf_flag 	CanConfirmationFlags.w[3].b5
#define CanWriteSyncStW_Actn_Rq_1_conf_flag(x)\
		CanStartFlagWriteSync();\
		NETC_TX_StW_Actn_Rq_1_conf_flag = x;\
		CanEndFlagWriteSync();

#define NETC_TX_PAM_B_conf_flag 	CanConfirmationFlags.w[3].b6
#define CanWriteSyncPAM_B_conf_flag(x)\
		CanStartFlagWriteSync();\
		NETC_TX_PAM_B_conf_flag = x;\
		CanEndFlagWriteSync();

#define NETC_TX_GW_C_I3_conf_flag 	CanConfirmationFlags.w[3].b7
#define CanWriteSyncGW_C_I3_conf_flag(x)\
		CanStartFlagWriteSync();\
		NETC_TX_GW_C_I3_conf_flag = x;\
		CanEndFlagWriteSync();

#define NETC_TX_VIN_1_conf_flag 	CanConfirmationFlags.w[3].b10
#define CanWriteSyncVIN_1_conf_flag(x)\
		CanStartFlagWriteSync();\
		NETC_TX_VIN_1_conf_flag = x;\
		CanEndFlagWriteSync();

#define NETC_TX_VEHICLE_SPEED_ODOMETER_1_conf_flag 	CanConfirmationFlags.w[3].b11
#define CanWriteSyncVEHICLE_SPEED_ODOMETER_1_conf_flag(x)\
		CanStartFlagWriteSync();\
		NETC_TX_VEHICLE_SPEED_ODOMETER_1_conf_flag = x;\
		CanEndFlagWriteSync();

#define NETC_TX_GW_C_I6_conf_flag 	CanConfirmationFlags.w[3].b12
#define CanWriteSyncGW_C_I6_conf_flag(x)\
		CanStartFlagWriteSync();\
		NETC_TX_GW_C_I6_conf_flag = x;\
		CanEndFlagWriteSync();

#define NETC_TX_DYNAMIC_VEHICLE_INFO2_conf_flag 	CanConfirmationFlags.w[3].b13
#define CanWriteSyncDYNAMIC_VEHICLE_INFO2_conf_flag(x)\
		CanStartFlagWriteSync();\
		NETC_TX_DYNAMIC_VEHICLE_INFO2_conf_flag = x;\
		CanEndFlagWriteSync();

#define NETC_TX_STATUS_B_TRANSMISSION_conf_flag 	CanConfirmationFlags.w[3].b14
#define CanWriteSyncSTATUS_B_TRANSMISSION_conf_flag(x)\
		CanStartFlagWriteSync();\
		NETC_TX_STATUS_B_TRANSMISSION_conf_flag = x;\
		CanEndFlagWriteSync();

#define NETC_TX_NWM_BCM_conf_flag 	CanConfirmationFlags.w[3].b15
#define CanWriteSyncNWM_BCM_conf_flag(x)\
		CanStartFlagWriteSync();\
		NETC_TX_NWM_BCM_conf_flag = x;\
		CanEndFlagWriteSync();

#define NETC_TX_STATUS_BCM2_conf_flag 	CanConfirmationFlags.w[3].b16
#define CanWriteSyncSTATUS_BCM2_conf_flag(x)\
		CanStartFlagWriteSync();\
		NETC_TX_STATUS_BCM2_conf_flag = x;\
		CanEndFlagWriteSync();

#define NETC_TX_STATUS_BCM_conf_flag 	CanConfirmationFlags.w[3].b17
#define CanWriteSyncSTATUS_BCM_conf_flag(x)\
		CanStartFlagWriteSync();\
		NETC_TX_STATUS_BCM_conf_flag = x;\
		CanEndFlagWriteSync();

#define NETC_TX_EXTERNAL_LIGHTS_1_conf_flag 	CanConfirmationFlags.w[4].b0
#define CanWriteSyncEXTERNAL_LIGHTS_1_conf_flag(x)\
		CanStartFlagWriteSync();\
		NETC_TX_EXTERNAL_LIGHTS_1_conf_flag = x;\
		CanEndFlagWriteSync();

#define NETC_TX_RFHUB_B_A2_conf_flag 	CanConfirmationFlags.w[4].b1
#define CanWriteSyncRFHUB_B_A2_conf_flag(x)\
		CanStartFlagWriteSync();\
		NETC_TX_RFHUB_B_A2_conf_flag = x;\
		CanEndFlagWriteSync();

#define NETC_TX_CBC_I4_conf_flag 	CanConfirmationFlags.w[4].b2
#define CanWriteSyncCBC_I4_conf_flag(x)\
		CanStartFlagWriteSync();\
		NETC_TX_CBC_I4_conf_flag = x;\
		CanEndFlagWriteSync();

#define NETC_TX_RFHUB_B_A4_conf_flag 	CanConfirmationFlags.w[4].b3
#define CanWriteSyncRFHUB_B_A4_conf_flag(x)\
		CanStartFlagWriteSync();\
		NETC_TX_RFHUB_B_A4_conf_flag = x;\
		CanEndFlagWriteSync();

#define NETC_TX_STATUS_B_ECM2_conf_flag 	CanConfirmationFlags.w[4].b4
#define CanWriteSyncSTATUS_B_ECM2_conf_flag(x)\
		CanStartFlagWriteSync();\
		NETC_TX_STATUS_B_ECM2_conf_flag = x;\
		CanEndFlagWriteSync();

#define NETC_TX_STATUS_B_ECM_conf_flag 	CanConfirmationFlags.w[4].b5
#define CanWriteSyncSTATUS_B_ECM_conf_flag(x)\
		CanStartFlagWriteSync();\
		NETC_TX_STATUS_B_ECM_conf_flag = x;\
		CanEndFlagWriteSync();

#define NETC_TX_STATUS_B_BSM_conf_flag 	CanConfirmationFlags.w[4].b6
#define CanWriteSyncSTATUS_B_BSM_conf_flag(x)\
		CanStartFlagWriteSync();\
		NETC_TX_STATUS_B_BSM_conf_flag = x;\
		CanEndFlagWriteSync();

#endif


/*************************************************************/
/* Databuffer for receive objects                            */
/*************************************************************/
extern _c_STATUS_C_OCM_buf MEMORY_FAR  STATUS_C_OCM;
extern _c_CFG_DATA_CODE_RSP_OCM_buf MEMORY_FAR  CFG_DATA_CODE_RSP_OCM;
extern _c_CFG_DATA_CODE_RSP_AHLM_buf MEMORY_FAR  CFG_DATA_CODE_RSP_AHLM;
extern _c_CFG_DATA_CODE_RSP_DTCM_buf MEMORY_FAR  CFG_DATA_CODE_RSP_DTCM;
extern _c_CFG_DATA_CODE_RSP_RFHM_buf MEMORY_FAR  CFG_DATA_CODE_RSP_RFHM;
extern _c_CFG_DATA_CODE_RSP_DASM_buf MEMORY_FAR  CFG_DATA_CODE_RSP_DASM;
extern _c_CFG_DATA_CODE_RSP_SCCM_buf MEMORY_FAR  CFG_DATA_CODE_RSP_SCCM;
extern _c_CFG_DATA_CODE_RSP_HALF_buf MEMORY_FAR  CFG_DATA_CODE_RSP_HALF;
extern _c_CFG_DATA_CODE_RSP_ORC_buf MEMORY_FAR  CFG_DATA_CODE_RSP_ORC;
extern _c_CFG_DATA_CODE_RSP_PAM_buf MEMORY_FAR  CFG_DATA_CODE_RSP_PAM;
extern _c_CFG_DATA_CODE_RSP_EPB_buf MEMORY_FAR  CFG_DATA_CODE_RSP_EPB;
extern _c_CFG_DATA_CODE_RSP_TRANSMISSION_buf MEMORY_FAR  CFG_DATA_CODE_RSP_TRANSMISSION;
extern _c_CFG_DATA_CODE_RSP_BSM_buf MEMORY_FAR  CFG_DATA_CODE_RSP_BSM;
extern _c_CFG_DATA_CODE_RSP_EPS_buf MEMORY_FAR  CFG_DATA_CODE_RSP_EPS;
extern _c_CFG_DATA_CODE_RSP_ECM_buf MEMORY_FAR  CFG_DATA_CODE_RSP_ECM;
extern _c_STATUS_C_DASM_buf MEMORY_FAR  STATUS_C_DASM;
extern _c_STATUS_C_AHLM_buf MEMORY_FAR  STATUS_C_AHLM;
extern _c_STATUS_C_HALF_buf MEMORY_FAR  STATUS_C_HALF;
extern _c_STATUS_C_ESL_buf MEMORY_FAR  STATUS_C_ESL;
extern _c_STATUS_C_EPB_buf MEMORY_FAR  STATUS_C_EPB;
extern _c_HALF_C_Warning_RQ_buf MEMORY_FAR  HALF_C_Warning_RQ;
extern _c_StW_Actn_Rq_0_buf MEMORY_FAR  StW_Actn_Rq_0;
extern _c_STATUS_C_TRANSMISSION_buf MEMORY_FAR  STATUS_C_TRANSMISSION;
extern _c_VIN_0_buf MEMORY_FAR  VIN_0;
extern _c_MOT3_buf MEMORY_FAR  MOT3;
extern _c_ECM_1_buf MEMORY_FAR  ECM_1;
extern _c_STATUS_C_DTCM_buf MEMORY_FAR  STATUS_C_DTCM;
extern _c_DAS_A2_buf MEMORY_FAR  DAS_A2;
extern _c_PAM_2_buf MEMORY_FAR  PAM_2;
extern _c_EPB_A1_buf MEMORY_FAR  EPB_A1;
extern _c_DAS_A1_buf MEMORY_FAR  DAS_A1;
extern _c_BSM_4_buf MEMORY_FAR  BSM_4;
extern _c_BSM_2_buf MEMORY_FAR  BSM_2;
extern _c_DTCM_A1_buf MEMORY_FAR  DTCM_A1;
extern _c_SC_buf MEMORY_FAR  SC;
extern _c_ORC_YRS_DATA_buf MEMORY_FAR  ORC_YRS_DATA;
extern _c_MOTGEAR2_buf MEMORY_FAR  MOTGEAR2;
extern _c_MOT4_buf MEMORY_FAR  MOT4;
extern _c_BSM_YRS_DATA_buf MEMORY_FAR  BSM_YRS_DATA;
extern _c_ASR4_buf MEMORY_FAR  ASR4;
extern _c_ASR3_buf MEMORY_FAR  ASR3;
extern _c_RFHUB_A4_buf MEMORY_FAR  RFHUB_A4;
extern _c_IPC_CFG_Feature_buf MEMORY_FAR  IPC_CFG_Feature;
extern _c_ESL_CODE_RESPONSE_buf MEMORY_FAR  ESL_CODE_RESPONSE;
extern _c_IMMO_CODE_REQUEST_buf MEMORY_FAR  IMMO_CODE_REQUEST;
extern _c_TRM_MKP_KEY_buf MEMORY_FAR  TRM_MKP_KEY;
extern _c_RFHUB_A3_buf MEMORY_FAR  RFHUB_A3;
extern _c_APPL_ECU_BCM_0_buf MEMORY_FAR  APPL_ECU_BCM_0;
extern _c_STATUS_C_SCCM_buf MEMORY_FAR  STATUS_C_SCCM;
extern _c_STATUS_C_RFHM_buf MEMORY_FAR  STATUS_C_RFHM;
extern _c_IPC_A1_buf MEMORY_FAR  IPC_A1;
extern _c_STATUS_C_SPM_buf MEMORY_FAR  STATUS_C_SPM;
extern _c_STATUS_C_IPC_buf MEMORY_FAR  STATUS_C_IPC;
extern _c_STATUS_C_GSM_buf MEMORY_FAR  STATUS_C_GSM;
extern _c_STATUS_C_EPS_buf MEMORY_FAR  STATUS_C_EPS;
extern _c_STATUS_C_BSM_buf MEMORY_FAR  STATUS_C_BSM;
extern _c_STATUS_C_ECM2_buf MEMORY_FAR  STATUS_C_ECM2;
extern _c_STATUS_C_ECM_buf MEMORY_FAR  STATUS_C_ECM;
extern _c_STATUS_C_ORC_buf MEMORY_FAR  STATUS_C_ORC;
extern _c_RFHUB_A2_buf MEMORY_FAR  RFHUB_A2;
extern _c_RFHUB_A1_buf MEMORY_FAR  RFHUB_A1;
extern _c_PAM_1_buf MEMORY_FAR  PAM_1;
extern _c_WAKE_C_RFHM_buf MEMORY_FAR  WAKE_C_RFHM;
extern _c_WAKE_C_SCCM_buf MEMORY_FAR  WAKE_C_SCCM;
extern _c_WAKE_C_ESL_buf MEMORY_FAR  WAKE_C_ESL;
extern _c_WAKE_C_EPB_buf MEMORY_FAR  WAKE_C_EPB;
extern _c_WAKE_C_BSM_buf MEMORY_FAR  WAKE_C_BSM;
extern _c_MOT1_buf MEMORY_FAR  MOT1;
extern _c_GE_buf MEMORY_FAR  GE;
extern _c_TRM_CODE_RESPONSE_buf MEMORY_FAR  TRM_CODE_RESPONSE;
extern _c_ORC_A2_buf MEMORY_FAR  ORC_A2;
extern _c_CRO_BCM_buf MEMORY_FAR  CRO_BCM;
extern _c_CFG_DATA_CODE_RSP_RBSS_buf MEMORY_FAR  CFG_DATA_CODE_RSP_RBSS;
extern _c_CFG_DATA_CODE_RSP_LBSS_buf MEMORY_FAR  CFG_DATA_CODE_RSP_LBSS;
extern _c_CFG_DATA_CODE_RSP_ICS_buf MEMORY_FAR  CFG_DATA_CODE_RSP_ICS;
extern _c_CFG_DATA_CODE_RSP_CSWM_buf MEMORY_FAR  CFG_DATA_CODE_RSP_CSWM;
extern _c_CFG_DATA_CODE_RSP_CDM_buf MEMORY_FAR  CFG_DATA_CODE_RSP_CDM;
extern _c_CFG_DATA_CODE_RSP_AMP_buf MEMORY_FAR  CFG_DATA_CODE_RSP_AMP;
extern _c_CFG_DATA_CODE_RSP_ETM_buf MEMORY_FAR  CFG_DATA_CODE_RSP_ETM;
extern _c_CFG_DATA_CODE_RSP_DSM_buf MEMORY_FAR  CFG_DATA_CODE_RSP_DSM;
extern _c_CFG_DATA_CODE_RSP_PDM_buf MEMORY_FAR  CFG_DATA_CODE_RSP_PDM;
extern _c_CFG_DATA_CODE_RSP_ECC_buf MEMORY_FAR  CFG_DATA_CODE_RSP_ECC;
extern _c_CFG_DATA_CODE_RSP_DDM_buf MEMORY_FAR  CFG_DATA_CODE_RSP_DDM;
extern _c_CFG_DATA_CODE_RSP_IPC_buf MEMORY_FAR  CFG_DATA_CODE_RSP_IPC;
extern _c_STATUS_ICS_buf MEMORY_FAR  STATUS_ICS;
extern _c_STATUS_CDM_buf MEMORY_FAR  STATUS_CDM;
extern _c_STATUS_AMP_buf MEMORY_FAR  STATUS_AMP;
extern _c_FT_HVAC_STAT_buf MEMORY_FAR  FT_HVAC_STAT;
extern _c_DCM_P_MSG_buf MEMORY_FAR  DCM_P_MSG;
extern _c_DCM_D_MSG_buf MEMORY_FAR  DCM_D_MSG;
extern _c_NWM_PLGM_buf MEMORY_FAR  NWM_PLGM;
extern _c_NWM_ETM_buf MEMORY_FAR  NWM_ETM;
extern _c_NWM_DSM_buf MEMORY_FAR  NWM_DSM;
extern _c_NWM_PDM_buf MEMORY_FAR  NWM_PDM;
extern _c_NWM_DDM_buf MEMORY_FAR  NWM_DDM;
extern _c_APPL_ECU_BCM_1_buf MEMORY_FAR  APPL_ECU_BCM_1;
extern _c_TRIP_A_B_buf MEMORY_FAR  TRIP_A_B;
extern _c_TGW_A1_buf MEMORY_FAR  TGW_A1;
extern _c_ICS_MSG_buf MEMORY_FAR  ICS_MSG;
extern _c_HVAC_A1_buf MEMORY_FAR  HVAC_A1;
extern _c_DIRECT_INFO_buf MEMORY_FAR  DIRECT_INFO;
extern _c_HVAC_A4_buf MEMORY_FAR  HVAC_A4;
extern _c_STATUS_RRM_buf MEMORY_FAR  STATUS_RRM;
extern _c_STATUS_IPC_buf MEMORY_FAR  STATUS_IPC;
extern _c_STATUS_ECC_buf MEMORY_FAR  STATUS_ECC;
extern _c_HVAC_A2_buf MEMORY_FAR  HVAC_A2;
extern _c_PLG_A1_buf MEMORY_FAR  PLG_A1;
extern _c_ECC_A1_buf MEMORY_FAR  ECC_A1;
extern _c_NWM_RBSS_buf MEMORY_FAR  NWM_RBSS;
extern _c_NWM_LBSS_buf MEMORY_FAR  NWM_LBSS;
extern _c_NWM_ICS_buf MEMORY_FAR  NWM_ICS;
extern _c_NWM_CSWM_buf MEMORY_FAR  NWM_CSWM;
extern _c_NWM_CDM_buf MEMORY_FAR  NWM_CDM;
extern _c_NWM_AMP_buf MEMORY_FAR  NWM_AMP;
extern _c_NWM_ECC_buf MEMORY_FAR  NWM_ECC;
extern _c_NWM_IPC_buf MEMORY_FAR  NWM_IPC;
extern _c_CMCM_UNLK_buf MEMORY_FAR  CMCM_UNLK;
extern _c_CFG_RQ_buf MEMORY_FAR  CFG_RQ;

/*************************************************************/
/* Byte access to databuffer of receive objects              */
/*************************************************************/
#define NETC_RX_c1_STATUS_C_OCM		STATUS_C_OCM._c[0]
#define NETC_RX_c2_STATUS_C_OCM		STATUS_C_OCM._c[1]
#define NETC_RX_c3_STATUS_C_OCM		STATUS_C_OCM._c[2]

#define NETC_RX_c1_CFG_DATA_CODE_RSP_OCM		CFG_DATA_CODE_RSP_OCM._c[0]
#define NETC_RX_c2_CFG_DATA_CODE_RSP_OCM		CFG_DATA_CODE_RSP_OCM._c[1]
#define NETC_RX_c3_CFG_DATA_CODE_RSP_OCM		CFG_DATA_CODE_RSP_OCM._c[2]
#define NETC_RX_c4_CFG_DATA_CODE_RSP_OCM		CFG_DATA_CODE_RSP_OCM._c[3]
#define NETC_RX_c5_CFG_DATA_CODE_RSP_OCM		CFG_DATA_CODE_RSP_OCM._c[4]
#define NETC_RX_c6_CFG_DATA_CODE_RSP_OCM		CFG_DATA_CODE_RSP_OCM._c[5]

#define NETC_RX_c1_CFG_DATA_CODE_RSP_AHLM		CFG_DATA_CODE_RSP_AHLM._c[0]
#define NETC_RX_c2_CFG_DATA_CODE_RSP_AHLM		CFG_DATA_CODE_RSP_AHLM._c[1]
#define NETC_RX_c3_CFG_DATA_CODE_RSP_AHLM		CFG_DATA_CODE_RSP_AHLM._c[2]
#define NETC_RX_c4_CFG_DATA_CODE_RSP_AHLM		CFG_DATA_CODE_RSP_AHLM._c[3]
#define NETC_RX_c5_CFG_DATA_CODE_RSP_AHLM		CFG_DATA_CODE_RSP_AHLM._c[4]
#define NETC_RX_c6_CFG_DATA_CODE_RSP_AHLM		CFG_DATA_CODE_RSP_AHLM._c[5]

#define NETC_RX_c1_CFG_DATA_CODE_RSP_DTCM		CFG_DATA_CODE_RSP_DTCM._c[0]
#define NETC_RX_c2_CFG_DATA_CODE_RSP_DTCM		CFG_DATA_CODE_RSP_DTCM._c[1]
#define NETC_RX_c3_CFG_DATA_CODE_RSP_DTCM		CFG_DATA_CODE_RSP_DTCM._c[2]
#define NETC_RX_c4_CFG_DATA_CODE_RSP_DTCM		CFG_DATA_CODE_RSP_DTCM._c[3]
#define NETC_RX_c5_CFG_DATA_CODE_RSP_DTCM		CFG_DATA_CODE_RSP_DTCM._c[4]
#define NETC_RX_c6_CFG_DATA_CODE_RSP_DTCM		CFG_DATA_CODE_RSP_DTCM._c[5]

#define NETC_RX_c1_CFG_DATA_CODE_RSP_RFHM		CFG_DATA_CODE_RSP_RFHM._c[0]
#define NETC_RX_c2_CFG_DATA_CODE_RSP_RFHM		CFG_DATA_CODE_RSP_RFHM._c[1]
#define NETC_RX_c3_CFG_DATA_CODE_RSP_RFHM		CFG_DATA_CODE_RSP_RFHM._c[2]
#define NETC_RX_c4_CFG_DATA_CODE_RSP_RFHM		CFG_DATA_CODE_RSP_RFHM._c[3]
#define NETC_RX_c5_CFG_DATA_CODE_RSP_RFHM		CFG_DATA_CODE_RSP_RFHM._c[4]
#define NETC_RX_c6_CFG_DATA_CODE_RSP_RFHM		CFG_DATA_CODE_RSP_RFHM._c[5]

#define NETC_RX_c1_CFG_DATA_CODE_RSP_DASM		CFG_DATA_CODE_RSP_DASM._c[0]
#define NETC_RX_c2_CFG_DATA_CODE_RSP_DASM		CFG_DATA_CODE_RSP_DASM._c[1]
#define NETC_RX_c3_CFG_DATA_CODE_RSP_DASM		CFG_DATA_CODE_RSP_DASM._c[2]
#define NETC_RX_c4_CFG_DATA_CODE_RSP_DASM		CFG_DATA_CODE_RSP_DASM._c[3]
#define NETC_RX_c5_CFG_DATA_CODE_RSP_DASM		CFG_DATA_CODE_RSP_DASM._c[4]
#define NETC_RX_c6_CFG_DATA_CODE_RSP_DASM		CFG_DATA_CODE_RSP_DASM._c[5]

#define NETC_RX_c1_CFG_DATA_CODE_RSP_SCCM		CFG_DATA_CODE_RSP_SCCM._c[0]
#define NETC_RX_c2_CFG_DATA_CODE_RSP_SCCM		CFG_DATA_CODE_RSP_SCCM._c[1]
#define NETC_RX_c3_CFG_DATA_CODE_RSP_SCCM		CFG_DATA_CODE_RSP_SCCM._c[2]
#define NETC_RX_c4_CFG_DATA_CODE_RSP_SCCM		CFG_DATA_CODE_RSP_SCCM._c[3]
#define NETC_RX_c5_CFG_DATA_CODE_RSP_SCCM		CFG_DATA_CODE_RSP_SCCM._c[4]
#define NETC_RX_c6_CFG_DATA_CODE_RSP_SCCM		CFG_DATA_CODE_RSP_SCCM._c[5]

#define NETC_RX_c1_CFG_DATA_CODE_RSP_HALF		CFG_DATA_CODE_RSP_HALF._c[0]
#define NETC_RX_c2_CFG_DATA_CODE_RSP_HALF		CFG_DATA_CODE_RSP_HALF._c[1]
#define NETC_RX_c3_CFG_DATA_CODE_RSP_HALF		CFG_DATA_CODE_RSP_HALF._c[2]
#define NETC_RX_c4_CFG_DATA_CODE_RSP_HALF		CFG_DATA_CODE_RSP_HALF._c[3]
#define NETC_RX_c5_CFG_DATA_CODE_RSP_HALF		CFG_DATA_CODE_RSP_HALF._c[4]
#define NETC_RX_c6_CFG_DATA_CODE_RSP_HALF		CFG_DATA_CODE_RSP_HALF._c[5]

#define NETC_RX_c1_CFG_DATA_CODE_RSP_ORC		CFG_DATA_CODE_RSP_ORC._c[0]
#define NETC_RX_c2_CFG_DATA_CODE_RSP_ORC		CFG_DATA_CODE_RSP_ORC._c[1]
#define NETC_RX_c3_CFG_DATA_CODE_RSP_ORC		CFG_DATA_CODE_RSP_ORC._c[2]
#define NETC_RX_c4_CFG_DATA_CODE_RSP_ORC		CFG_DATA_CODE_RSP_ORC._c[3]
#define NETC_RX_c5_CFG_DATA_CODE_RSP_ORC		CFG_DATA_CODE_RSP_ORC._c[4]
#define NETC_RX_c6_CFG_DATA_CODE_RSP_ORC		CFG_DATA_CODE_RSP_ORC._c[5]

#define NETC_RX_c1_CFG_DATA_CODE_RSP_PAM		CFG_DATA_CODE_RSP_PAM._c[0]
#define NETC_RX_c2_CFG_DATA_CODE_RSP_PAM		CFG_DATA_CODE_RSP_PAM._c[1]
#define NETC_RX_c3_CFG_DATA_CODE_RSP_PAM		CFG_DATA_CODE_RSP_PAM._c[2]
#define NETC_RX_c4_CFG_DATA_CODE_RSP_PAM		CFG_DATA_CODE_RSP_PAM._c[3]
#define NETC_RX_c5_CFG_DATA_CODE_RSP_PAM		CFG_DATA_CODE_RSP_PAM._c[4]
#define NETC_RX_c6_CFG_DATA_CODE_RSP_PAM		CFG_DATA_CODE_RSP_PAM._c[5]

#define NETC_RX_c1_CFG_DATA_CODE_RSP_EPB		CFG_DATA_CODE_RSP_EPB._c[0]
#define NETC_RX_c2_CFG_DATA_CODE_RSP_EPB		CFG_DATA_CODE_RSP_EPB._c[1]
#define NETC_RX_c3_CFG_DATA_CODE_RSP_EPB		CFG_DATA_CODE_RSP_EPB._c[2]
#define NETC_RX_c4_CFG_DATA_CODE_RSP_EPB		CFG_DATA_CODE_RSP_EPB._c[3]
#define NETC_RX_c5_CFG_DATA_CODE_RSP_EPB		CFG_DATA_CODE_RSP_EPB._c[4]
#define NETC_RX_c6_CFG_DATA_CODE_RSP_EPB		CFG_DATA_CODE_RSP_EPB._c[5]

#define NETC_RX_c1_CFG_DATA_CODE_RSP_TRANSMISSION		CFG_DATA_CODE_RSP_TRANSMISSION._c[0]
#define NETC_RX_c2_CFG_DATA_CODE_RSP_TRANSMISSION		CFG_DATA_CODE_RSP_TRANSMISSION._c[1]
#define NETC_RX_c3_CFG_DATA_CODE_RSP_TRANSMISSION		CFG_DATA_CODE_RSP_TRANSMISSION._c[2]
#define NETC_RX_c4_CFG_DATA_CODE_RSP_TRANSMISSION		CFG_DATA_CODE_RSP_TRANSMISSION._c[3]
#define NETC_RX_c5_CFG_DATA_CODE_RSP_TRANSMISSION		CFG_DATA_CODE_RSP_TRANSMISSION._c[4]
#define NETC_RX_c6_CFG_DATA_CODE_RSP_TRANSMISSION		CFG_DATA_CODE_RSP_TRANSMISSION._c[5]

#define NETC_RX_c1_CFG_DATA_CODE_RSP_BSM		CFG_DATA_CODE_RSP_BSM._c[0]
#define NETC_RX_c2_CFG_DATA_CODE_RSP_BSM		CFG_DATA_CODE_RSP_BSM._c[1]
#define NETC_RX_c3_CFG_DATA_CODE_RSP_BSM		CFG_DATA_CODE_RSP_BSM._c[2]
#define NETC_RX_c4_CFG_DATA_CODE_RSP_BSM		CFG_DATA_CODE_RSP_BSM._c[3]
#define NETC_RX_c5_CFG_DATA_CODE_RSP_BSM		CFG_DATA_CODE_RSP_BSM._c[4]
#define NETC_RX_c6_CFG_DATA_CODE_RSP_BSM		CFG_DATA_CODE_RSP_BSM._c[5]

#define NETC_RX_c1_CFG_DATA_CODE_RSP_EPS		CFG_DATA_CODE_RSP_EPS._c[0]
#define NETC_RX_c2_CFG_DATA_CODE_RSP_EPS		CFG_DATA_CODE_RSP_EPS._c[1]
#define NETC_RX_c3_CFG_DATA_CODE_RSP_EPS		CFG_DATA_CODE_RSP_EPS._c[2]
#define NETC_RX_c4_CFG_DATA_CODE_RSP_EPS		CFG_DATA_CODE_RSP_EPS._c[3]
#define NETC_RX_c5_CFG_DATA_CODE_RSP_EPS		CFG_DATA_CODE_RSP_EPS._c[4]
#define NETC_RX_c6_CFG_DATA_CODE_RSP_EPS		CFG_DATA_CODE_RSP_EPS._c[5]

#define NETC_RX_c1_CFG_DATA_CODE_RSP_ECM		CFG_DATA_CODE_RSP_ECM._c[0]
#define NETC_RX_c2_CFG_DATA_CODE_RSP_ECM		CFG_DATA_CODE_RSP_ECM._c[1]
#define NETC_RX_c3_CFG_DATA_CODE_RSP_ECM		CFG_DATA_CODE_RSP_ECM._c[2]
#define NETC_RX_c4_CFG_DATA_CODE_RSP_ECM		CFG_DATA_CODE_RSP_ECM._c[3]
#define NETC_RX_c5_CFG_DATA_CODE_RSP_ECM		CFG_DATA_CODE_RSP_ECM._c[4]
#define NETC_RX_c6_CFG_DATA_CODE_RSP_ECM		CFG_DATA_CODE_RSP_ECM._c[5]

#define NETC_RX_c1_STATUS_C_DASM		STATUS_C_DASM._c[0]
#define NETC_RX_c2_STATUS_C_DASM		STATUS_C_DASM._c[1]

#define NETC_RX_c1_STATUS_C_AHLM		STATUS_C_AHLM._c[0]
#define NETC_RX_c2_STATUS_C_AHLM		STATUS_C_AHLM._c[1]

#define NETC_RX_c1_STATUS_C_HALF		STATUS_C_HALF._c[0]
#define NETC_RX_c2_STATUS_C_HALF		STATUS_C_HALF._c[1]
#define NETC_RX_c3_STATUS_C_HALF		STATUS_C_HALF._c[2]
#define NETC_RX_c4_STATUS_C_HALF		STATUS_C_HALF._c[3]
#define NETC_RX_c5_STATUS_C_HALF		STATUS_C_HALF._c[4]
#define NETC_RX_c6_STATUS_C_HALF		STATUS_C_HALF._c[5]
#define NETC_RX_c7_STATUS_C_HALF		STATUS_C_HALF._c[6]
#define NETC_RX_c8_STATUS_C_HALF		STATUS_C_HALF._c[7]

#define NETC_RX_c1_STATUS_C_ESL		STATUS_C_ESL._c[0]
#define NETC_RX_c2_STATUS_C_ESL		STATUS_C_ESL._c[1]

#define NETC_RX_c1_STATUS_C_EPB		STATUS_C_EPB._c[0]
#define NETC_RX_c2_STATUS_C_EPB		STATUS_C_EPB._c[1]
#define NETC_RX_c3_STATUS_C_EPB		STATUS_C_EPB._c[2]
#define NETC_RX_c4_STATUS_C_EPB		STATUS_C_EPB._c[3]
#define NETC_RX_c5_STATUS_C_EPB		STATUS_C_EPB._c[4]
#define NETC_RX_c6_STATUS_C_EPB		STATUS_C_EPB._c[5]
#define NETC_RX_c7_STATUS_C_EPB		STATUS_C_EPB._c[6]

#define NETC_RX_c1_HALF_C_Warning_RQ		HALF_C_Warning_RQ._c[0]
#define NETC_RX_c2_HALF_C_Warning_RQ		HALF_C_Warning_RQ._c[1]

#define NETC_RX_c1_StW_Actn_Rq_0		StW_Actn_Rq_0._c[0]
#define NETC_RX_c2_StW_Actn_Rq_0		StW_Actn_Rq_0._c[1]
#define NETC_RX_c3_StW_Actn_Rq_0		StW_Actn_Rq_0._c[2]
#define NETC_RX_c4_StW_Actn_Rq_0		StW_Actn_Rq_0._c[3]
#define NETC_RX_c5_StW_Actn_Rq_0		StW_Actn_Rq_0._c[4]
#define NETC_RX_c6_StW_Actn_Rq_0		StW_Actn_Rq_0._c[5]
#define NETC_RX_c7_StW_Actn_Rq_0		StW_Actn_Rq_0._c[6]

#define NETC_RX_c1_STATUS_C_TRANSMISSION		STATUS_C_TRANSMISSION._c[0]
#define NETC_RX_c2_STATUS_C_TRANSMISSION		STATUS_C_TRANSMISSION._c[1]
#define NETC_RX_c3_STATUS_C_TRANSMISSION		STATUS_C_TRANSMISSION._c[2]
#define NETC_RX_c4_STATUS_C_TRANSMISSION		STATUS_C_TRANSMISSION._c[3]
#define NETC_RX_c5_STATUS_C_TRANSMISSION		STATUS_C_TRANSMISSION._c[4]
#define NETC_RX_c6_STATUS_C_TRANSMISSION		STATUS_C_TRANSMISSION._c[5]
#define NETC_RX_c7_STATUS_C_TRANSMISSION		STATUS_C_TRANSMISSION._c[6]
#define NETC_RX_c8_STATUS_C_TRANSMISSION		STATUS_C_TRANSMISSION._c[7]

#define NETC_RX_c1_VIN_0		VIN_0._c[0]
#define NETC_RX_c2_VIN_0		VIN_0._c[1]
#define NETC_RX_c3_VIN_0		VIN_0._c[2]
#define NETC_RX_c4_VIN_0		VIN_0._c[3]
#define NETC_RX_c5_VIN_0		VIN_0._c[4]
#define NETC_RX_c6_VIN_0		VIN_0._c[5]
#define NETC_RX_c7_VIN_0		VIN_0._c[6]
#define NETC_RX_c8_VIN_0		VIN_0._c[7]

#define NETC_RX_c1_MOT3		MOT3._c[0]
#define NETC_RX_c2_MOT3		MOT3._c[1]
#define NETC_RX_c3_MOT3		MOT3._c[2]
#define NETC_RX_c4_MOT3		MOT3._c[3]
#define NETC_RX_c5_MOT3		MOT3._c[4]
#define NETC_RX_c6_MOT3		MOT3._c[5]
#define NETC_RX_c7_MOT3		MOT3._c[6]
#define NETC_RX_c8_MOT3		MOT3._c[7]

#define NETC_RX_c1_ECM_1		ECM_1._c[0]
#define NETC_RX_c2_ECM_1		ECM_1._c[1]
#define NETC_RX_c3_ECM_1		ECM_1._c[2]
#define NETC_RX_c4_ECM_1		ECM_1._c[3]
#define NETC_RX_c5_ECM_1		ECM_1._c[4]
#define NETC_RX_c6_ECM_1		ECM_1._c[5]
#define NETC_RX_c7_ECM_1		ECM_1._c[6]
#define NETC_RX_c8_ECM_1		ECM_1._c[7]

#define NETC_RX_c1_STATUS_C_DTCM		STATUS_C_DTCM._c[0]
#define NETC_RX_c2_STATUS_C_DTCM		STATUS_C_DTCM._c[1]
#define NETC_RX_c3_STATUS_C_DTCM		STATUS_C_DTCM._c[2]
#define NETC_RX_c4_STATUS_C_DTCM		STATUS_C_DTCM._c[3]
#define NETC_RX_c5_STATUS_C_DTCM		STATUS_C_DTCM._c[4]
#define NETC_RX_c6_STATUS_C_DTCM		STATUS_C_DTCM._c[5]
#define NETC_RX_c7_STATUS_C_DTCM		STATUS_C_DTCM._c[6]
#define NETC_RX_c8_STATUS_C_DTCM		STATUS_C_DTCM._c[7]

#define NETC_RX_c1_DAS_A2		DAS_A2._c[0]
#define NETC_RX_c2_DAS_A2		DAS_A2._c[1]
#define NETC_RX_c3_DAS_A2		DAS_A2._c[2]
#define NETC_RX_c4_DAS_A2		DAS_A2._c[3]
#define NETC_RX_c5_DAS_A2		DAS_A2._c[4]
#define NETC_RX_c6_DAS_A2		DAS_A2._c[5]
#define NETC_RX_c7_DAS_A2		DAS_A2._c[6]

#define NETC_RX_c1_PAM_2		PAM_2._c[0]
#define NETC_RX_c2_PAM_2		PAM_2._c[1]
#define NETC_RX_c3_PAM_2		PAM_2._c[2]
#define NETC_RX_c4_PAM_2		PAM_2._c[3]
#define NETC_RX_c5_PAM_2		PAM_2._c[4]

#define NETC_RX_c1_EPB_A1		EPB_A1._c[0]
#define NETC_RX_c2_EPB_A1		EPB_A1._c[1]
#define NETC_RX_c3_EPB_A1		EPB_A1._c[2]

#define NETC_RX_c1_DAS_A1		DAS_A1._c[0]
#define NETC_RX_c2_DAS_A1		DAS_A1._c[1]
#define NETC_RX_c3_DAS_A1		DAS_A1._c[2]
#define NETC_RX_c4_DAS_A1		DAS_A1._c[3]
#define NETC_RX_c5_DAS_A1		DAS_A1._c[4]
#define NETC_RX_c6_DAS_A1		DAS_A1._c[5]
#define NETC_RX_c7_DAS_A1		DAS_A1._c[6]
#define NETC_RX_c8_DAS_A1		DAS_A1._c[7]

#define NETC_RX_c1_BSM_4		BSM_4._c[0]
#define NETC_RX_c2_BSM_4		BSM_4._c[1]
#define NETC_RX_c3_BSM_4		BSM_4._c[2]
#define NETC_RX_c4_BSM_4		BSM_4._c[3]
#define NETC_RX_c5_BSM_4		BSM_4._c[4]
#define NETC_RX_c6_BSM_4		BSM_4._c[5]
#define NETC_RX_c7_BSM_4		BSM_4._c[6]
#define NETC_RX_c8_BSM_4		BSM_4._c[7]

#define NETC_RX_c1_BSM_2		BSM_2._c[0]
#define NETC_RX_c2_BSM_2		BSM_2._c[1]
#define NETC_RX_c3_BSM_2		BSM_2._c[2]
#define NETC_RX_c4_BSM_2		BSM_2._c[3]
#define NETC_RX_c5_BSM_2		BSM_2._c[4]
#define NETC_RX_c6_BSM_2		BSM_2._c[5]
#define NETC_RX_c7_BSM_2		BSM_2._c[6]
#define NETC_RX_c8_BSM_2		BSM_2._c[7]

#define NETC_RX_c1_DTCM_A1		DTCM_A1._c[0]
#define NETC_RX_c2_DTCM_A1		DTCM_A1._c[1]
#define NETC_RX_c3_DTCM_A1		DTCM_A1._c[2]
#define NETC_RX_c4_DTCM_A1		DTCM_A1._c[3]
#define NETC_RX_c5_DTCM_A1		DTCM_A1._c[4]
#define NETC_RX_c6_DTCM_A1		DTCM_A1._c[5]
#define NETC_RX_c7_DTCM_A1		DTCM_A1._c[6]
#define NETC_RX_c8_DTCM_A1		DTCM_A1._c[7]

#define NETC_RX_c1_SC		SC._c[0]
#define NETC_RX_c2_SC		SC._c[1]
#define NETC_RX_c3_SC		SC._c[2]
#define NETC_RX_c4_SC		SC._c[3]

#define NETC_RX_c1_ORC_YRS_DATA		ORC_YRS_DATA._c[0]
#define NETC_RX_c2_ORC_YRS_DATA		ORC_YRS_DATA._c[1]
#define NETC_RX_c3_ORC_YRS_DATA		ORC_YRS_DATA._c[2]
#define NETC_RX_c4_ORC_YRS_DATA		ORC_YRS_DATA._c[3]
#define NETC_RX_c5_ORC_YRS_DATA		ORC_YRS_DATA._c[4]
#define NETC_RX_c6_ORC_YRS_DATA		ORC_YRS_DATA._c[5]
#define NETC_RX_c7_ORC_YRS_DATA		ORC_YRS_DATA._c[6]

#define NETC_RX_c1_MOTGEAR2		MOTGEAR2._c[0]
#define NETC_RX_c2_MOTGEAR2		MOTGEAR2._c[1]
#define NETC_RX_c3_MOTGEAR2		MOTGEAR2._c[2]
#define NETC_RX_c4_MOTGEAR2		MOTGEAR2._c[3]
#define NETC_RX_c5_MOTGEAR2		MOTGEAR2._c[4]

#define NETC_RX_c1_MOT4		MOT4._c[0]
#define NETC_RX_c2_MOT4		MOT4._c[1]
#define NETC_RX_c3_MOT4		MOT4._c[2]
#define NETC_RX_c4_MOT4		MOT4._c[3]
#define NETC_RX_c5_MOT4		MOT4._c[4]
#define NETC_RX_c6_MOT4		MOT4._c[5]
#define NETC_RX_c7_MOT4		MOT4._c[6]
#define NETC_RX_c8_MOT4		MOT4._c[7]

#define NETC_RX_c1_BSM_YRS_DATA		BSM_YRS_DATA._c[0]
#define NETC_RX_c2_BSM_YRS_DATA		BSM_YRS_DATA._c[1]
#define NETC_RX_c3_BSM_YRS_DATA		BSM_YRS_DATA._c[2]
#define NETC_RX_c4_BSM_YRS_DATA		BSM_YRS_DATA._c[3]

#define NETC_RX_c1_ASR4		ASR4._c[0]
#define NETC_RX_c2_ASR4		ASR4._c[1]
#define NETC_RX_c3_ASR4		ASR4._c[2]
#define NETC_RX_c4_ASR4		ASR4._c[3]
#define NETC_RX_c5_ASR4		ASR4._c[4]
#define NETC_RX_c6_ASR4		ASR4._c[5]
#define NETC_RX_c7_ASR4		ASR4._c[6]
#define NETC_RX_c8_ASR4		ASR4._c[7]

#define NETC_RX_c1_ASR3		ASR3._c[0]
#define NETC_RX_c2_ASR3		ASR3._c[1]
#define NETC_RX_c3_ASR3		ASR3._c[2]
#define NETC_RX_c4_ASR3		ASR3._c[3]
#define NETC_RX_c5_ASR3		ASR3._c[4]
#define NETC_RX_c6_ASR3		ASR3._c[5]
#define NETC_RX_c7_ASR3		ASR3._c[6]
#define NETC_RX_c8_ASR3		ASR3._c[7]

#define NETC_RX_c1_RFHUB_A4		RFHUB_A4._c[0]

#define NETC_RX_c1_IPC_CFG_Feature		IPC_CFG_Feature._c[0]
#define NETC_RX_c2_IPC_CFG_Feature		IPC_CFG_Feature._c[1]
#define NETC_RX_c3_IPC_CFG_Feature		IPC_CFG_Feature._c[2]

#define NETC_RX_c1_ESL_CODE_RESPONSE		ESL_CODE_RESPONSE._c[0]
#define NETC_RX_c2_ESL_CODE_RESPONSE		ESL_CODE_RESPONSE._c[1]
#define NETC_RX_c3_ESL_CODE_RESPONSE		ESL_CODE_RESPONSE._c[2]
#define NETC_RX_c4_ESL_CODE_RESPONSE		ESL_CODE_RESPONSE._c[3]
#define NETC_RX_c5_ESL_CODE_RESPONSE		ESL_CODE_RESPONSE._c[4]

#define NETC_RX_c1_IMMO_CODE_REQUEST		IMMO_CODE_REQUEST._c[0]
#define NETC_RX_c2_IMMO_CODE_REQUEST		IMMO_CODE_REQUEST._c[1]
#define NETC_RX_c3_IMMO_CODE_REQUEST		IMMO_CODE_REQUEST._c[2]
#define NETC_RX_c4_IMMO_CODE_REQUEST		IMMO_CODE_REQUEST._c[3]
#define NETC_RX_c5_IMMO_CODE_REQUEST		IMMO_CODE_REQUEST._c[4]
#define NETC_RX_c6_IMMO_CODE_REQUEST		IMMO_CODE_REQUEST._c[5]
#define NETC_RX_c7_IMMO_CODE_REQUEST		IMMO_CODE_REQUEST._c[6]

#define NETC_RX_c1_TRM_MKP_KEY		TRM_MKP_KEY._c[0]
#define NETC_RX_c2_TRM_MKP_KEY		TRM_MKP_KEY._c[1]
#define NETC_RX_c3_TRM_MKP_KEY		TRM_MKP_KEY._c[2]
#define NETC_RX_c4_TRM_MKP_KEY		TRM_MKP_KEY._c[3]
#define NETC_RX_c5_TRM_MKP_KEY		TRM_MKP_KEY._c[4]
#define NETC_RX_c6_TRM_MKP_KEY		TRM_MKP_KEY._c[5]
#define NETC_RX_c7_TRM_MKP_KEY		TRM_MKP_KEY._c[6]
#define NETC_RX_c8_TRM_MKP_KEY		TRM_MKP_KEY._c[7]

#define NETC_RX_c1_RFHUB_A3		RFHUB_A3._c[0]
#define NETC_RX_c2_RFHUB_A3		RFHUB_A3._c[1]

#define NETC_RX_c1_APPL_ECU_BCM_0		APPL_ECU_BCM_0._c[0]
#define NETC_RX_c2_APPL_ECU_BCM_0		APPL_ECU_BCM_0._c[1]
#define NETC_RX_c3_APPL_ECU_BCM_0		APPL_ECU_BCM_0._c[2]
#define NETC_RX_c4_APPL_ECU_BCM_0		APPL_ECU_BCM_0._c[3]
#define NETC_RX_c5_APPL_ECU_BCM_0		APPL_ECU_BCM_0._c[4]
#define NETC_RX_c6_APPL_ECU_BCM_0		APPL_ECU_BCM_0._c[5]
#define NETC_RX_c7_APPL_ECU_BCM_0		APPL_ECU_BCM_0._c[6]
#define NETC_RX_c8_APPL_ECU_BCM_0		APPL_ECU_BCM_0._c[7]

#define NETC_RX_c1_STATUS_C_SCCM		STATUS_C_SCCM._c[0]
#define NETC_RX_c2_STATUS_C_SCCM		STATUS_C_SCCM._c[1]
#define NETC_RX_c3_STATUS_C_SCCM		STATUS_C_SCCM._c[2]
#define NETC_RX_c4_STATUS_C_SCCM		STATUS_C_SCCM._c[3]

#define NETC_RX_c1_STATUS_C_RFHM		STATUS_C_RFHM._c[0]
#define NETC_RX_c2_STATUS_C_RFHM		STATUS_C_RFHM._c[1]
#define NETC_RX_c3_STATUS_C_RFHM		STATUS_C_RFHM._c[2]
#define NETC_RX_c4_STATUS_C_RFHM		STATUS_C_RFHM._c[3]

#define NETC_RX_c1_IPC_A1		IPC_A1._c[0]
#define NETC_RX_c2_IPC_A1		IPC_A1._c[1]
#define NETC_RX_c3_IPC_A1		IPC_A1._c[2]
#define NETC_RX_c4_IPC_A1		IPC_A1._c[3]

#define NETC_RX_c1_STATUS_C_SPM		STATUS_C_SPM._c[0]
#define NETC_RX_c2_STATUS_C_SPM		STATUS_C_SPM._c[1]
#define NETC_RX_c3_STATUS_C_SPM		STATUS_C_SPM._c[2]
#define NETC_RX_c4_STATUS_C_SPM		STATUS_C_SPM._c[3]
#define NETC_RX_c5_STATUS_C_SPM		STATUS_C_SPM._c[4]
#define NETC_RX_c6_STATUS_C_SPM		STATUS_C_SPM._c[5]
#define NETC_RX_c7_STATUS_C_SPM		STATUS_C_SPM._c[6]
#define NETC_RX_c8_STATUS_C_SPM		STATUS_C_SPM._c[7]

#define NETC_RX_c1_STATUS_C_IPC		STATUS_C_IPC._c[0]
#define NETC_RX_c2_STATUS_C_IPC		STATUS_C_IPC._c[1]
#define NETC_RX_c3_STATUS_C_IPC		STATUS_C_IPC._c[2]
#define NETC_RX_c4_STATUS_C_IPC		STATUS_C_IPC._c[3]
#define NETC_RX_c5_STATUS_C_IPC		STATUS_C_IPC._c[4]
#define NETC_RX_c6_STATUS_C_IPC		STATUS_C_IPC._c[5]
#define NETC_RX_c7_STATUS_C_IPC		STATUS_C_IPC._c[6]
#define NETC_RX_c8_STATUS_C_IPC		STATUS_C_IPC._c[7]

#define NETC_RX_c1_STATUS_C_GSM		STATUS_C_GSM._c[0]
#define NETC_RX_c2_STATUS_C_GSM		STATUS_C_GSM._c[1]

#define NETC_RX_c1_STATUS_C_EPS		STATUS_C_EPS._c[0]
#define NETC_RX_c2_STATUS_C_EPS		STATUS_C_EPS._c[1]

#define NETC_RX_c1_STATUS_C_BSM		STATUS_C_BSM._c[0]
#define NETC_RX_c2_STATUS_C_BSM		STATUS_C_BSM._c[1]
#define NETC_RX_c3_STATUS_C_BSM		STATUS_C_BSM._c[2]
#define NETC_RX_c4_STATUS_C_BSM		STATUS_C_BSM._c[3]
#define NETC_RX_c5_STATUS_C_BSM		STATUS_C_BSM._c[4]
#define NETC_RX_c6_STATUS_C_BSM		STATUS_C_BSM._c[5]
#define NETC_RX_c7_STATUS_C_BSM		STATUS_C_BSM._c[6]
#define NETC_RX_c8_STATUS_C_BSM		STATUS_C_BSM._c[7]

#define NETC_RX_c1_STATUS_C_ECM2		STATUS_C_ECM2._c[0]
#define NETC_RX_c2_STATUS_C_ECM2		STATUS_C_ECM2._c[1]
#define NETC_RX_c3_STATUS_C_ECM2		STATUS_C_ECM2._c[2]
#define NETC_RX_c4_STATUS_C_ECM2		STATUS_C_ECM2._c[3]
#define NETC_RX_c5_STATUS_C_ECM2		STATUS_C_ECM2._c[4]
#define NETC_RX_c6_STATUS_C_ECM2		STATUS_C_ECM2._c[5]
#define NETC_RX_c7_STATUS_C_ECM2		STATUS_C_ECM2._c[6]
#define NETC_RX_c8_STATUS_C_ECM2		STATUS_C_ECM2._c[7]

#define NETC_RX_c1_STATUS_C_ECM		STATUS_C_ECM._c[0]
#define NETC_RX_c2_STATUS_C_ECM		STATUS_C_ECM._c[1]
#define NETC_RX_c3_STATUS_C_ECM		STATUS_C_ECM._c[2]
#define NETC_RX_c4_STATUS_C_ECM		STATUS_C_ECM._c[3]
#define NETC_RX_c5_STATUS_C_ECM		STATUS_C_ECM._c[4]
#define NETC_RX_c6_STATUS_C_ECM		STATUS_C_ECM._c[5]
#define NETC_RX_c7_STATUS_C_ECM		STATUS_C_ECM._c[6]
#define NETC_RX_c8_STATUS_C_ECM		STATUS_C_ECM._c[7]

#define NETC_RX_c1_STATUS_C_ORC		STATUS_C_ORC._c[0]
#define NETC_RX_c2_STATUS_C_ORC		STATUS_C_ORC._c[1]
#define NETC_RX_c3_STATUS_C_ORC		STATUS_C_ORC._c[2]
#define NETC_RX_c4_STATUS_C_ORC		STATUS_C_ORC._c[3]
#define NETC_RX_c5_STATUS_C_ORC		STATUS_C_ORC._c[4]
#define NETC_RX_c6_STATUS_C_ORC		STATUS_C_ORC._c[5]
#define NETC_RX_c7_STATUS_C_ORC		STATUS_C_ORC._c[6]
#define NETC_RX_c8_STATUS_C_ORC		STATUS_C_ORC._c[7]

#define NETC_RX_c1_RFHUB_A2		RFHUB_A2._c[0]
#define NETC_RX_c2_RFHUB_A2		RFHUB_A2._c[1]
#define NETC_RX_c3_RFHUB_A2		RFHUB_A2._c[2]
#define NETC_RX_c4_RFHUB_A2		RFHUB_A2._c[3]
#define NETC_RX_c5_RFHUB_A2		RFHUB_A2._c[4]

#define NETC_RX_c1_RFHUB_A1		RFHUB_A1._c[0]

#define NETC_RX_c1_PAM_1		PAM_1._c[0]
#define NETC_RX_c2_PAM_1		PAM_1._c[1]
#define NETC_RX_c3_PAM_1		PAM_1._c[2]
#define NETC_RX_c4_PAM_1		PAM_1._c[3]
#define NETC_RX_c5_PAM_1		PAM_1._c[4]
#define NETC_RX_c6_PAM_1		PAM_1._c[5]
#define NETC_RX_c7_PAM_1		PAM_1._c[6]
#define NETC_RX_c8_PAM_1		PAM_1._c[7]

#define NETC_RX_c1_WAKE_C_RFHM		WAKE_C_RFHM._c[0]
#define NETC_RX_c2_WAKE_C_RFHM		WAKE_C_RFHM._c[1]
#define NETC_RX_c3_WAKE_C_RFHM		WAKE_C_RFHM._c[2]
#define NETC_RX_c4_WAKE_C_RFHM		WAKE_C_RFHM._c[3]

#define NETC_RX_c1_WAKE_C_SCCM		WAKE_C_SCCM._c[0]
#define NETC_RX_c2_WAKE_C_SCCM		WAKE_C_SCCM._c[1]
#define NETC_RX_c3_WAKE_C_SCCM		WAKE_C_SCCM._c[2]
#define NETC_RX_c4_WAKE_C_SCCM		WAKE_C_SCCM._c[3]

#define NETC_RX_c1_WAKE_C_ESL		WAKE_C_ESL._c[0]
#define NETC_RX_c2_WAKE_C_ESL		WAKE_C_ESL._c[1]
#define NETC_RX_c3_WAKE_C_ESL		WAKE_C_ESL._c[2]
#define NETC_RX_c4_WAKE_C_ESL		WAKE_C_ESL._c[3]

#define NETC_RX_c1_WAKE_C_EPB		WAKE_C_EPB._c[0]
#define NETC_RX_c2_WAKE_C_EPB		WAKE_C_EPB._c[1]
#define NETC_RX_c3_WAKE_C_EPB		WAKE_C_EPB._c[2]
#define NETC_RX_c4_WAKE_C_EPB		WAKE_C_EPB._c[3]

#define NETC_RX_c1_WAKE_C_BSM		WAKE_C_BSM._c[0]
#define NETC_RX_c2_WAKE_C_BSM		WAKE_C_BSM._c[1]
#define NETC_RX_c3_WAKE_C_BSM		WAKE_C_BSM._c[2]
#define NETC_RX_c4_WAKE_C_BSM		WAKE_C_BSM._c[3]

#define NETC_RX_c1_MOT1		MOT1._c[0]
#define NETC_RX_c2_MOT1		MOT1._c[1]
#define NETC_RX_c3_MOT1		MOT1._c[2]
#define NETC_RX_c4_MOT1		MOT1._c[3]
#define NETC_RX_c5_MOT1		MOT1._c[4]
#define NETC_RX_c6_MOT1		MOT1._c[5]
#define NETC_RX_c7_MOT1		MOT1._c[6]
#define NETC_RX_c8_MOT1		MOT1._c[7]

#define NETC_RX_c1_GE		GE._c[0]
#define NETC_RX_c2_GE		GE._c[1]
#define NETC_RX_c3_GE		GE._c[2]
#define NETC_RX_c4_GE		GE._c[3]
#define NETC_RX_c5_GE		GE._c[4]
#define NETC_RX_c6_GE		GE._c[5]

#define NETC_RX_c1_TRM_CODE_RESPONSE		TRM_CODE_RESPONSE._c[0]
#define NETC_RX_c2_TRM_CODE_RESPONSE		TRM_CODE_RESPONSE._c[1]
#define NETC_RX_c3_TRM_CODE_RESPONSE		TRM_CODE_RESPONSE._c[2]
#define NETC_RX_c4_TRM_CODE_RESPONSE		TRM_CODE_RESPONSE._c[3]
#define NETC_RX_c5_TRM_CODE_RESPONSE		TRM_CODE_RESPONSE._c[4]
#define NETC_RX_c6_TRM_CODE_RESPONSE		TRM_CODE_RESPONSE._c[5]

#define NETC_RX_c1_ORC_A2		ORC_A2._c[0]
#define NETC_RX_c2_ORC_A2		ORC_A2._c[1]

#define NETC_RX_c1_CRO_BCM		CRO_BCM._c[0]
#define NETC_RX_c2_CRO_BCM		CRO_BCM._c[1]
#define NETC_RX_c3_CRO_BCM		CRO_BCM._c[2]
#define NETC_RX_c4_CRO_BCM		CRO_BCM._c[3]
#define NETC_RX_c5_CRO_BCM		CRO_BCM._c[4]
#define NETC_RX_c6_CRO_BCM		CRO_BCM._c[5]
#define NETC_RX_c7_CRO_BCM		CRO_BCM._c[6]
#define NETC_RX_c8_CRO_BCM		CRO_BCM._c[7]

#define NETC_RX_c1_CFG_DATA_CODE_RSP_RBSS		CFG_DATA_CODE_RSP_RBSS._c[0]
#define NETC_RX_c2_CFG_DATA_CODE_RSP_RBSS		CFG_DATA_CODE_RSP_RBSS._c[1]
#define NETC_RX_c3_CFG_DATA_CODE_RSP_RBSS		CFG_DATA_CODE_RSP_RBSS._c[2]
#define NETC_RX_c4_CFG_DATA_CODE_RSP_RBSS		CFG_DATA_CODE_RSP_RBSS._c[3]
#define NETC_RX_c5_CFG_DATA_CODE_RSP_RBSS		CFG_DATA_CODE_RSP_RBSS._c[4]
#define NETC_RX_c6_CFG_DATA_CODE_RSP_RBSS		CFG_DATA_CODE_RSP_RBSS._c[5]

#define NETC_RX_c1_CFG_DATA_CODE_RSP_LBSS		CFG_DATA_CODE_RSP_LBSS._c[0]
#define NETC_RX_c2_CFG_DATA_CODE_RSP_LBSS		CFG_DATA_CODE_RSP_LBSS._c[1]
#define NETC_RX_c3_CFG_DATA_CODE_RSP_LBSS		CFG_DATA_CODE_RSP_LBSS._c[2]
#define NETC_RX_c4_CFG_DATA_CODE_RSP_LBSS		CFG_DATA_CODE_RSP_LBSS._c[3]
#define NETC_RX_c5_CFG_DATA_CODE_RSP_LBSS		CFG_DATA_CODE_RSP_LBSS._c[4]
#define NETC_RX_c6_CFG_DATA_CODE_RSP_LBSS		CFG_DATA_CODE_RSP_LBSS._c[5]

#define NETC_RX_c1_CFG_DATA_CODE_RSP_ICS		CFG_DATA_CODE_RSP_ICS._c[0]
#define NETC_RX_c2_CFG_DATA_CODE_RSP_ICS		CFG_DATA_CODE_RSP_ICS._c[1]
#define NETC_RX_c3_CFG_DATA_CODE_RSP_ICS		CFG_DATA_CODE_RSP_ICS._c[2]
#define NETC_RX_c4_CFG_DATA_CODE_RSP_ICS		CFG_DATA_CODE_RSP_ICS._c[3]
#define NETC_RX_c5_CFG_DATA_CODE_RSP_ICS		CFG_DATA_CODE_RSP_ICS._c[4]
#define NETC_RX_c6_CFG_DATA_CODE_RSP_ICS		CFG_DATA_CODE_RSP_ICS._c[5]

#define NETC_RX_c1_CFG_DATA_CODE_RSP_CSWM		CFG_DATA_CODE_RSP_CSWM._c[0]
#define NETC_RX_c2_CFG_DATA_CODE_RSP_CSWM		CFG_DATA_CODE_RSP_CSWM._c[1]
#define NETC_RX_c3_CFG_DATA_CODE_RSP_CSWM		CFG_DATA_CODE_RSP_CSWM._c[2]
#define NETC_RX_c4_CFG_DATA_CODE_RSP_CSWM		CFG_DATA_CODE_RSP_CSWM._c[3]
#define NETC_RX_c5_CFG_DATA_CODE_RSP_CSWM		CFG_DATA_CODE_RSP_CSWM._c[4]
#define NETC_RX_c6_CFG_DATA_CODE_RSP_CSWM		CFG_DATA_CODE_RSP_CSWM._c[5]

#define NETC_RX_c1_CFG_DATA_CODE_RSP_CDM		CFG_DATA_CODE_RSP_CDM._c[0]
#define NETC_RX_c2_CFG_DATA_CODE_RSP_CDM		CFG_DATA_CODE_RSP_CDM._c[1]
#define NETC_RX_c3_CFG_DATA_CODE_RSP_CDM		CFG_DATA_CODE_RSP_CDM._c[2]
#define NETC_RX_c4_CFG_DATA_CODE_RSP_CDM		CFG_DATA_CODE_RSP_CDM._c[3]
#define NETC_RX_c5_CFG_DATA_CODE_RSP_CDM		CFG_DATA_CODE_RSP_CDM._c[4]
#define NETC_RX_c6_CFG_DATA_CODE_RSP_CDM		CFG_DATA_CODE_RSP_CDM._c[5]

#define NETC_RX_c1_CFG_DATA_CODE_RSP_AMP		CFG_DATA_CODE_RSP_AMP._c[0]
#define NETC_RX_c2_CFG_DATA_CODE_RSP_AMP		CFG_DATA_CODE_RSP_AMP._c[1]
#define NETC_RX_c3_CFG_DATA_CODE_RSP_AMP		CFG_DATA_CODE_RSP_AMP._c[2]
#define NETC_RX_c4_CFG_DATA_CODE_RSP_AMP		CFG_DATA_CODE_RSP_AMP._c[3]
#define NETC_RX_c5_CFG_DATA_CODE_RSP_AMP		CFG_DATA_CODE_RSP_AMP._c[4]
#define NETC_RX_c6_CFG_DATA_CODE_RSP_AMP		CFG_DATA_CODE_RSP_AMP._c[5]

#define NETC_RX_c1_CFG_DATA_CODE_RSP_ETM		CFG_DATA_CODE_RSP_ETM._c[0]
#define NETC_RX_c2_CFG_DATA_CODE_RSP_ETM		CFG_DATA_CODE_RSP_ETM._c[1]
#define NETC_RX_c3_CFG_DATA_CODE_RSP_ETM		CFG_DATA_CODE_RSP_ETM._c[2]
#define NETC_RX_c4_CFG_DATA_CODE_RSP_ETM		CFG_DATA_CODE_RSP_ETM._c[3]
#define NETC_RX_c5_CFG_DATA_CODE_RSP_ETM		CFG_DATA_CODE_RSP_ETM._c[4]
#define NETC_RX_c6_CFG_DATA_CODE_RSP_ETM		CFG_DATA_CODE_RSP_ETM._c[5]

#define NETC_RX_c1_CFG_DATA_CODE_RSP_DSM		CFG_DATA_CODE_RSP_DSM._c[0]
#define NETC_RX_c2_CFG_DATA_CODE_RSP_DSM		CFG_DATA_CODE_RSP_DSM._c[1]
#define NETC_RX_c3_CFG_DATA_CODE_RSP_DSM		CFG_DATA_CODE_RSP_DSM._c[2]
#define NETC_RX_c4_CFG_DATA_CODE_RSP_DSM		CFG_DATA_CODE_RSP_DSM._c[3]
#define NETC_RX_c5_CFG_DATA_CODE_RSP_DSM		CFG_DATA_CODE_RSP_DSM._c[4]
#define NETC_RX_c6_CFG_DATA_CODE_RSP_DSM		CFG_DATA_CODE_RSP_DSM._c[5]

#define NETC_RX_c1_CFG_DATA_CODE_RSP_PDM		CFG_DATA_CODE_RSP_PDM._c[0]
#define NETC_RX_c2_CFG_DATA_CODE_RSP_PDM		CFG_DATA_CODE_RSP_PDM._c[1]
#define NETC_RX_c3_CFG_DATA_CODE_RSP_PDM		CFG_DATA_CODE_RSP_PDM._c[2]
#define NETC_RX_c4_CFG_DATA_CODE_RSP_PDM		CFG_DATA_CODE_RSP_PDM._c[3]
#define NETC_RX_c5_CFG_DATA_CODE_RSP_PDM		CFG_DATA_CODE_RSP_PDM._c[4]
#define NETC_RX_c6_CFG_DATA_CODE_RSP_PDM		CFG_DATA_CODE_RSP_PDM._c[5]

#define NETC_RX_c1_CFG_DATA_CODE_RSP_ECC		CFG_DATA_CODE_RSP_ECC._c[0]
#define NETC_RX_c2_CFG_DATA_CODE_RSP_ECC		CFG_DATA_CODE_RSP_ECC._c[1]
#define NETC_RX_c3_CFG_DATA_CODE_RSP_ECC		CFG_DATA_CODE_RSP_ECC._c[2]
#define NETC_RX_c4_CFG_DATA_CODE_RSP_ECC		CFG_DATA_CODE_RSP_ECC._c[3]
#define NETC_RX_c5_CFG_DATA_CODE_RSP_ECC		CFG_DATA_CODE_RSP_ECC._c[4]
#define NETC_RX_c6_CFG_DATA_CODE_RSP_ECC		CFG_DATA_CODE_RSP_ECC._c[5]

#define NETC_RX_c1_CFG_DATA_CODE_RSP_DDM		CFG_DATA_CODE_RSP_DDM._c[0]
#define NETC_RX_c2_CFG_DATA_CODE_RSP_DDM		CFG_DATA_CODE_RSP_DDM._c[1]
#define NETC_RX_c3_CFG_DATA_CODE_RSP_DDM		CFG_DATA_CODE_RSP_DDM._c[2]
#define NETC_RX_c4_CFG_DATA_CODE_RSP_DDM		CFG_DATA_CODE_RSP_DDM._c[3]
#define NETC_RX_c5_CFG_DATA_CODE_RSP_DDM		CFG_DATA_CODE_RSP_DDM._c[4]
#define NETC_RX_c6_CFG_DATA_CODE_RSP_DDM		CFG_DATA_CODE_RSP_DDM._c[5]

#define NETC_RX_c1_CFG_DATA_CODE_RSP_IPC		CFG_DATA_CODE_RSP_IPC._c[0]
#define NETC_RX_c2_CFG_DATA_CODE_RSP_IPC		CFG_DATA_CODE_RSP_IPC._c[1]
#define NETC_RX_c3_CFG_DATA_CODE_RSP_IPC		CFG_DATA_CODE_RSP_IPC._c[2]
#define NETC_RX_c4_CFG_DATA_CODE_RSP_IPC		CFG_DATA_CODE_RSP_IPC._c[3]
#define NETC_RX_c5_CFG_DATA_CODE_RSP_IPC		CFG_DATA_CODE_RSP_IPC._c[4]
#define NETC_RX_c6_CFG_DATA_CODE_RSP_IPC		CFG_DATA_CODE_RSP_IPC._c[5]

#define NETC_RX_c1_STATUS_ICS		STATUS_ICS._c[0]
#define NETC_RX_c2_STATUS_ICS		STATUS_ICS._c[1]

#define NETC_RX_c1_STATUS_CDM		STATUS_CDM._c[0]
#define NETC_RX_c2_STATUS_CDM		STATUS_CDM._c[1]

#define NETC_RX_c1_STATUS_AMP		STATUS_AMP._c[0]
#define NETC_RX_c2_STATUS_AMP		STATUS_AMP._c[1]

#define NETC_RX_c1_ENVIRONMENTAL_CONDITIONS		ENVIRONMENTAL_CONDITIONS._c[0]
#define NETC_RX_c2_ENVIRONMENTAL_CONDITIONS		ENVIRONMENTAL_CONDITIONS._c[1]
#define NETC_RX_c3_ENVIRONMENTAL_CONDITIONS		ENVIRONMENTAL_CONDITIONS._c[2]
#define NETC_RX_c4_ENVIRONMENTAL_CONDITIONS		ENVIRONMENTAL_CONDITIONS._c[3]
#define NETC_RX_c5_ENVIRONMENTAL_CONDITIONS		ENVIRONMENTAL_CONDITIONS._c[4]
#define NETC_RX_c6_ENVIRONMENTAL_CONDITIONS		ENVIRONMENTAL_CONDITIONS._c[5]
#define NETC_RX_c7_ENVIRONMENTAL_CONDITIONS		ENVIRONMENTAL_CONDITIONS._c[6]
#define NETC_RX_c8_ENVIRONMENTAL_CONDITIONS		ENVIRONMENTAL_CONDITIONS._c[7]

#define NETC_RX_c1_FT_HVAC_STAT		FT_HVAC_STAT._c[0]
#define NETC_RX_c2_FT_HVAC_STAT		FT_HVAC_STAT._c[1]
#define NETC_RX_c3_FT_HVAC_STAT		FT_HVAC_STAT._c[2]
#define NETC_RX_c4_FT_HVAC_STAT		FT_HVAC_STAT._c[3]
#define NETC_RX_c5_FT_HVAC_STAT		FT_HVAC_STAT._c[4]
#define NETC_RX_c6_FT_HVAC_STAT		FT_HVAC_STAT._c[5]

#define NETC_RX_c1_DCM_P_MSG		DCM_P_MSG._c[0]
#define NETC_RX_c2_DCM_P_MSG		DCM_P_MSG._c[1]

#define NETC_RX_c1_DCM_D_MSG		DCM_D_MSG._c[0]
#define NETC_RX_c2_DCM_D_MSG		DCM_D_MSG._c[1]

#define NETC_RX_c1_NWM_PLGM		NWM_PLGM._c[0]
#define NETC_RX_c2_NWM_PLGM		NWM_PLGM._c[1]

#define NETC_RX_c1_NWM_ETM		NWM_ETM._c[0]
#define NETC_RX_c2_NWM_ETM		NWM_ETM._c[1]

#define NETC_RX_c1_NWM_DSM		NWM_DSM._c[0]
#define NETC_RX_c2_NWM_DSM		NWM_DSM._c[1]

#define NETC_RX_c1_NWM_PDM		NWM_PDM._c[0]
#define NETC_RX_c2_NWM_PDM		NWM_PDM._c[1]

#define NETC_RX_c1_NWM_DDM		NWM_DDM._c[0]
#define NETC_RX_c2_NWM_DDM		NWM_DDM._c[1]

#define NETC_RX_c1_APPL_ECU_BCM_1		APPL_ECU_BCM_1._c[0]
#define NETC_RX_c2_APPL_ECU_BCM_1		APPL_ECU_BCM_1._c[1]
#define NETC_RX_c3_APPL_ECU_BCM_1		APPL_ECU_BCM_1._c[2]
#define NETC_RX_c4_APPL_ECU_BCM_1		APPL_ECU_BCM_1._c[3]
#define NETC_RX_c5_APPL_ECU_BCM_1		APPL_ECU_BCM_1._c[4]
#define NETC_RX_c6_APPL_ECU_BCM_1		APPL_ECU_BCM_1._c[5]
#define NETC_RX_c7_APPL_ECU_BCM_1		APPL_ECU_BCM_1._c[6]
#define NETC_RX_c8_APPL_ECU_BCM_1		APPL_ECU_BCM_1._c[7]

#define NETC_RX_c1_TRIP_A_B		TRIP_A_B._c[0]
#define NETC_RX_c2_TRIP_A_B		TRIP_A_B._c[1]
#define NETC_RX_c3_TRIP_A_B		TRIP_A_B._c[2]
#define NETC_RX_c4_TRIP_A_B		TRIP_A_B._c[3]
#define NETC_RX_c5_TRIP_A_B		TRIP_A_B._c[4]
#define NETC_RX_c6_TRIP_A_B		TRIP_A_B._c[5]
#define NETC_RX_c7_TRIP_A_B		TRIP_A_B._c[6]
#define NETC_RX_c8_TRIP_A_B		TRIP_A_B._c[7]

#define NETC_RX_c1_TGW_A1		TGW_A1._c[0]
#define NETC_RX_c2_TGW_A1		TGW_A1._c[1]
#define NETC_RX_c3_TGW_A1		TGW_A1._c[2]
#define NETC_RX_c4_TGW_A1		TGW_A1._c[3]

#define NETC_RX_c1_ICS_MSG		ICS_MSG._c[0]
#define NETC_RX_c2_ICS_MSG		ICS_MSG._c[1]
#define NETC_RX_c3_ICS_MSG		ICS_MSG._c[2]
#define NETC_RX_c4_ICS_MSG		ICS_MSG._c[3]
#define NETC_RX_c5_ICS_MSG		ICS_MSG._c[4]
#define NETC_RX_c6_ICS_MSG		ICS_MSG._c[5]
#define NETC_RX_c7_ICS_MSG		ICS_MSG._c[6]
#define NETC_RX_c8_ICS_MSG		ICS_MSG._c[7]

#define NETC_RX_c1_HVAC_A1		HVAC_A1._c[0]
#define NETC_RX_c2_HVAC_A1		HVAC_A1._c[1]
#define NETC_RX_c3_HVAC_A1		HVAC_A1._c[2]
#define NETC_RX_c4_HVAC_A1		HVAC_A1._c[3]
#define NETC_RX_c5_HVAC_A1		HVAC_A1._c[4]
#define NETC_RX_c6_HVAC_A1		HVAC_A1._c[5]

#define NETC_RX_c1_DIRECT_INFO		DIRECT_INFO._c[0]
#define NETC_RX_c2_DIRECT_INFO		DIRECT_INFO._c[1]
#define NETC_RX_c3_DIRECT_INFO		DIRECT_INFO._c[2]
#define NETC_RX_c4_DIRECT_INFO		DIRECT_INFO._c[3]
#define NETC_RX_c5_DIRECT_INFO		DIRECT_INFO._c[4]

#define NETC_RX_c1_HVAC_A4		HVAC_A4._c[0]
#define NETC_RX_c2_HVAC_A4		HVAC_A4._c[1]
#define NETC_RX_c3_HVAC_A4		HVAC_A4._c[2]
#define NETC_RX_c4_HVAC_A4		HVAC_A4._c[3]

#define NETC_RX_c1_STATUS_RRM		STATUS_RRM._c[0]
#define NETC_RX_c2_STATUS_RRM		STATUS_RRM._c[1]
#define NETC_RX_c3_STATUS_RRM		STATUS_RRM._c[2]
#define NETC_RX_c4_STATUS_RRM		STATUS_RRM._c[3]
#define NETC_RX_c5_STATUS_RRM		STATUS_RRM._c[4]
#define NETC_RX_c6_STATUS_RRM		STATUS_RRM._c[5]
#define NETC_RX_c7_STATUS_RRM		STATUS_RRM._c[6]
#define NETC_RX_c8_STATUS_RRM		STATUS_RRM._c[7]

#define NETC_RX_c1_STATUS_IPC		STATUS_IPC._c[0]
#define NETC_RX_c2_STATUS_IPC		STATUS_IPC._c[1]
#define NETC_RX_c3_STATUS_IPC		STATUS_IPC._c[2]
#define NETC_RX_c4_STATUS_IPC		STATUS_IPC._c[3]
#define NETC_RX_c5_STATUS_IPC		STATUS_IPC._c[4]
#define NETC_RX_c6_STATUS_IPC		STATUS_IPC._c[5]
#define NETC_RX_c7_STATUS_IPC		STATUS_IPC._c[6]
#define NETC_RX_c8_STATUS_IPC		STATUS_IPC._c[7]

#define NETC_RX_c1_STATUS_ECC		STATUS_ECC._c[0]
#define NETC_RX_c2_STATUS_ECC		STATUS_ECC._c[1]
#define NETC_RX_c3_STATUS_ECC		STATUS_ECC._c[2]

#define NETC_RX_c1_HVAC_A2		HVAC_A2._c[0]
#define NETC_RX_c2_HVAC_A2		HVAC_A2._c[1]
#define NETC_RX_c3_HVAC_A2		HVAC_A2._c[2]

#define NETC_RX_c1_PLG_A1		PLG_A1._c[0]
#define NETC_RX_c2_PLG_A1		PLG_A1._c[1]

#define NETC_RX_c1_ECC_A1		ECC_A1._c[0]
#define NETC_RX_c2_ECC_A1		ECC_A1._c[1]

#define NETC_RX_c1_NWM_RBSS		NWM_RBSS._c[0]
#define NETC_RX_c2_NWM_RBSS		NWM_RBSS._c[1]

#define NETC_RX_c1_NWM_LBSS		NWM_LBSS._c[0]
#define NETC_RX_c2_NWM_LBSS		NWM_LBSS._c[1]

#define NETC_RX_c1_NWM_ICS		NWM_ICS._c[0]
#define NETC_RX_c2_NWM_ICS		NWM_ICS._c[1]

#define NETC_RX_c1_NWM_CSWM		NWM_CSWM._c[0]
#define NETC_RX_c2_NWM_CSWM		NWM_CSWM._c[1]

#define NETC_RX_c1_NWM_CDM		NWM_CDM._c[0]
#define NETC_RX_c2_NWM_CDM		NWM_CDM._c[1]

#define NETC_RX_c1_NWM_AMP		NWM_AMP._c[0]
#define NETC_RX_c2_NWM_AMP		NWM_AMP._c[1]

#define NETC_RX_c1_NWM_ECC		NWM_ECC._c[0]
#define NETC_RX_c2_NWM_ECC		NWM_ECC._c[1]

#define NETC_RX_c1_NWM_IPC		NWM_IPC._c[0]
#define NETC_RX_c2_NWM_IPC		NWM_IPC._c[1]

#define NETC_RX_c1_CBC_I4		CBC_I4._c[0]
#define NETC_RX_c2_CBC_I4		CBC_I4._c[1]
#define NETC_RX_c3_CBC_I4		CBC_I4._c[2]
#define NETC_RX_c4_CBC_I4		CBC_I4._c[3]
#define NETC_RX_c5_CBC_I4		CBC_I4._c[4]

#define NETC_RX_c1_CMCM_UNLK		CMCM_UNLK._c[0]
#define NETC_RX_c2_CMCM_UNLK		CMCM_UNLK._c[1]
#define NETC_RX_c3_CMCM_UNLK		CMCM_UNLK._c[2]

#define NETC_RX_c1_CFG_RQ		CFG_RQ._c[0]
#define NETC_RX_c2_CFG_RQ		CFG_RQ._c[1]
#define NETC_RX_c3_CFG_RQ		CFG_RQ._c[2]


/*************************************************************/
/* Handles of  receive objects                               */
/*************************************************************/

#define NETC_RX_STATUS_C_OCM	0
#define NETC_RX_CFG_DATA_CODE_RSP_OCM	1
#define NETC_RX_CFG_DATA_CODE_RSP_AHLM	2
#define NETC_RX_CFG_DATA_CODE_RSP_DTCM	3
#define NETC_RX_CFG_DATA_CODE_RSP_RFHM	4
#define NETC_RX_CFG_DATA_CODE_RSP_DASM	5
#define NETC_RX_CFG_DATA_CODE_RSP_SCCM	6
#define NETC_RX_CFG_DATA_CODE_RSP_HALF	7
#define NETC_RX_CFG_DATA_CODE_RSP_ORC	8
#define NETC_RX_CFG_DATA_CODE_RSP_PAM	9
#define NETC_RX_CFG_DATA_CODE_RSP_EPB	10
#define NETC_RX_CFG_DATA_CODE_RSP_TRANSMISSION	11
#define NETC_RX_CFG_DATA_CODE_RSP_BSM	12
#define NETC_RX_CFG_DATA_CODE_RSP_EPS	13
#define NETC_RX_CFG_DATA_CODE_RSP_ECM	14
#define NETC_RX_STATUS_C_DASM	15
#define NETC_RX_STATUS_C_AHLM	16
#define NETC_RX_DIAGNOSTIC_REQUEST_FUNCTIONAL	17
#define NETC_RX_DIAGNOSTIC_REQUEST_BCM_0	18
#define NETC_RX_STATUS_C_HALF	19
#define NETC_RX_STATUS_C_ESL	20
#define NETC_RX_STATUS_C_EPB	21
#define NETC_RX_HALF_C_Warning_RQ	22
#define NETC_RX_StW_Actn_Rq_0	23
#define NETC_RX_STATUS_C_TRANSMISSION	24
#define NETC_RX_VIN_0	25
#define NETC_RX_MOT3	26
#define NETC_RX_ECM_1	27
#define NETC_RX_STATUS_C_DTCM	28
#define NETC_RX_DAS_A2	29
#define NETC_RX_PAM_2	30
#define NETC_RX_EPB_A1	31
#define NETC_RX_DAS_A1	32
#define NETC_RX_BSM_4	33
#define NETC_RX_BSM_2	34
#define NETC_RX_DTCM_A1	35
#define NETC_RX_SC	36
#define NETC_RX_ORC_YRS_DATA	37
#define NETC_RX_MOTGEAR2	38
#define NETC_RX_MOT4	39
#define NETC_RX_BSM_YRS_DATA	40
#define NETC_RX_ASR4	41
#define NETC_RX_ASR3	42
#define NETC_RX_RFHUB_A4	43
#define NETC_RX_IPC_CFG_Feature	44
#define NETC_RX_ESL_CODE_RESPONSE	45
#define NETC_RX_IMMO_CODE_REQUEST	46
#define NETC_RX_TRM_MKP_KEY	47
#define NETC_RX_RFHUB_A3	48
#define NETC_RX_APPL_ECU_BCM_0	49
#define NETC_RX_STATUS_C_SCCM	50
#define NETC_RX_STATUS_C_RFHM	51
#define NETC_RX_IPC_A1	52
#define NETC_RX_STATUS_C_SPM	53
#define NETC_RX_STATUS_C_IPC	54
#define NETC_RX_STATUS_C_GSM	55
#define NETC_RX_STATUS_C_EPS	56
#define NETC_RX_STATUS_C_BSM	57
#define NETC_RX_STATUS_C_ECM2	58
#define NETC_RX_STATUS_C_ECM	59
#define NETC_RX_STATUS_C_ORC	60
#define NETC_RX_RFHUB_A2	61
#define NETC_RX_RFHUB_A1	62
#define NETC_RX_PAM_1	63
#define NETC_RX_WAKE_C_RFHM	64
#define NETC_RX_WAKE_C_SCCM	65
#define NETC_RX_WAKE_C_ESL	66
#define NETC_RX_WAKE_C_EPB	67
#define NETC_RX_WAKE_C_BSM	68
#define NETC_RX_MOT1	69
#define NETC_RX_GE	70
#define NETC_RX_TRM_CODE_RESPONSE	71
#define NETC_RX_ORC_A2	72
#define NETC_RX_CRO_BCM	73
#define NETC_RX_CFG_DATA_CODE_RSP_RBSS	74
#define NETC_RX_CFG_DATA_CODE_RSP_LBSS	75
#define NETC_RX_CFG_DATA_CODE_RSP_ICS	76
#define NETC_RX_CFG_DATA_CODE_RSP_CSWM	77
#define NETC_RX_CFG_DATA_CODE_RSP_CDM	78
#define NETC_RX_CFG_DATA_CODE_RSP_AMP	79
#define NETC_RX_CFG_DATA_CODE_RSP_ETM	80
#define NETC_RX_CFG_DATA_CODE_RSP_DSM	81
#define NETC_RX_CFG_DATA_CODE_RSP_PDM	82
#define NETC_RX_CFG_DATA_CODE_RSP_ECC	83
#define NETC_RX_CFG_DATA_CODE_RSP_DDM	84
#define NETC_RX_CFG_DATA_CODE_RSP_IPC	85
#define NETC_RX_STATUS_ICS	86
#define NETC_RX_STATUS_CDM	87
#define NETC_RX_STATUS_AMP	88
#define NETC_RX_ENVIRONMENTAL_CONDITIONS	89
#define NETC_RX_FT_HVAC_STAT	90
#define NETC_RX_DCM_P_MSG	91
#define NETC_RX_DCM_D_MSG	92
#define NETC_RX_NWM_PLGM	93
#define NETC_RX_NWM_ETM	94
#define NETC_RX_NWM_DSM	95
#define NETC_RX_NWM_PDM	96
#define NETC_RX_NWM_DDM	97
#define NETC_RX_APPL_ECU_BCM_1	98
#define NETC_RX_TRIP_A_B	99
#define NETC_RX_DIAGNOSTIC_REQUEST_BCM_1	100
#define NETC_RX_TGW_A1	101
#define NETC_RX_ICS_MSG	102
#define NETC_RX_HVAC_A1	103
#define NETC_RX_DIRECT_INFO	104
#define NETC_RX_HVAC_A4	105
#define NETC_RX_STATUS_RRM	106
#define NETC_RX_STATUS_IPC	107
#define NETC_RX_STATUS_ECC	108
#define NETC_RX_HVAC_A2	109
#define NETC_RX_PLG_A1	110
#define NETC_RX_ECC_A1	111
#define NETC_RX_NWM_RBSS	112
#define NETC_RX_NWM_LBSS	113
#define NETC_RX_NWM_ICS	114
#define NETC_RX_NWM_CSWM	115
#define NETC_RX_NWM_CDM	116
#define NETC_RX_NWM_AMP	117
#define NETC_RX_NWM_ECC	118
#define NETC_RX_NWM_IPC	119
#define NETC_RX_CBC_I4	120
#define NETC_RX_CMCM_UNLK	121
#define NETC_RX_CFG_RQ	122


/*************************************************************/
/* Databuffer for indicationflags                                */
/*************************************************************/

#define NETC_RX_STATUS_C_OCM_ind_flag 	CanIndicationFlags.w[0].b0
#define CanWriteSyncSTATUS_C_OCM_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_STATUS_C_OCM_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_CFG_DATA_CODE_RSP_OCM_ind_flag 	CanIndicationFlags.w[0].b1
#define CanWriteSyncCFG_DATA_CODE_RSP_OCM_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_CFG_DATA_CODE_RSP_OCM_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_CFG_DATA_CODE_RSP_AHLM_ind_flag 	CanIndicationFlags.w[0].b2
#define CanWriteSyncCFG_DATA_CODE_RSP_AHLM_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_CFG_DATA_CODE_RSP_AHLM_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_CFG_DATA_CODE_RSP_DTCM_ind_flag 	CanIndicationFlags.w[0].b3
#define CanWriteSyncCFG_DATA_CODE_RSP_DTCM_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_CFG_DATA_CODE_RSP_DTCM_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_CFG_DATA_CODE_RSP_RFHM_ind_flag 	CanIndicationFlags.w[0].b4
#define CanWriteSyncCFG_DATA_CODE_RSP_RFHM_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_CFG_DATA_CODE_RSP_RFHM_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_CFG_DATA_CODE_RSP_DASM_ind_flag 	CanIndicationFlags.w[0].b5
#define CanWriteSyncCFG_DATA_CODE_RSP_DASM_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_CFG_DATA_CODE_RSP_DASM_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_CFG_DATA_CODE_RSP_SCCM_ind_flag 	CanIndicationFlags.w[0].b6
#define CanWriteSyncCFG_DATA_CODE_RSP_SCCM_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_CFG_DATA_CODE_RSP_SCCM_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_CFG_DATA_CODE_RSP_HALF_ind_flag 	CanIndicationFlags.w[0].b7
#define CanWriteSyncCFG_DATA_CODE_RSP_HALF_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_CFG_DATA_CODE_RSP_HALF_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_CFG_DATA_CODE_RSP_ORC_ind_flag 	CanIndicationFlags.w[0].b10
#define CanWriteSyncCFG_DATA_CODE_RSP_ORC_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_CFG_DATA_CODE_RSP_ORC_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_CFG_DATA_CODE_RSP_PAM_ind_flag 	CanIndicationFlags.w[0].b11
#define CanWriteSyncCFG_DATA_CODE_RSP_PAM_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_CFG_DATA_CODE_RSP_PAM_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_CFG_DATA_CODE_RSP_EPB_ind_flag 	CanIndicationFlags.w[0].b12
#define CanWriteSyncCFG_DATA_CODE_RSP_EPB_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_CFG_DATA_CODE_RSP_EPB_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_CFG_DATA_CODE_RSP_TRANSMISSION_ind_flag 	CanIndicationFlags.w[0].b13
#define CanWriteSyncCFG_DATA_CODE_RSP_TRANSMISSION_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_CFG_DATA_CODE_RSP_TRANSMISSION_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_CFG_DATA_CODE_RSP_BSM_ind_flag 	CanIndicationFlags.w[0].b14
#define CanWriteSyncCFG_DATA_CODE_RSP_BSM_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_CFG_DATA_CODE_RSP_BSM_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_CFG_DATA_CODE_RSP_EPS_ind_flag 	CanIndicationFlags.w[0].b15
#define CanWriteSyncCFG_DATA_CODE_RSP_EPS_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_CFG_DATA_CODE_RSP_EPS_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_CFG_DATA_CODE_RSP_ECM_ind_flag 	CanIndicationFlags.w[0].b16
#define CanWriteSyncCFG_DATA_CODE_RSP_ECM_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_CFG_DATA_CODE_RSP_ECM_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_STATUS_C_DASM_ind_flag 	CanIndicationFlags.w[0].b17
#define CanWriteSyncSTATUS_C_DASM_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_STATUS_C_DASM_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_STATUS_C_AHLM_ind_flag 	CanIndicationFlags.w[1].b0
#define CanWriteSyncSTATUS_C_AHLM_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_STATUS_C_AHLM_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_DIAGNOSTIC_REQUEST_BCM_0_ind_flag 	CanIndicationFlags.w[1].b1
#define CanWriteSyncDIAGNOSTIC_REQUEST_BCM_0_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_DIAGNOSTIC_REQUEST_BCM_0_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_STATUS_C_HALF_ind_flag 	CanIndicationFlags.w[1].b2
#define CanWriteSyncSTATUS_C_HALF_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_STATUS_C_HALF_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_STATUS_C_ESL_ind_flag 	CanIndicationFlags.w[1].b3
#define CanWriteSyncSTATUS_C_ESL_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_STATUS_C_ESL_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_STATUS_C_EPB_ind_flag 	CanIndicationFlags.w[1].b4
#define CanWriteSyncSTATUS_C_EPB_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_STATUS_C_EPB_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_HALF_C_Warning_RQ_ind_flag 	CanIndicationFlags.w[1].b5
#define CanWriteSyncHALF_C_Warning_RQ_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_HALF_C_Warning_RQ_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_StW_Actn_Rq_0_ind_flag 	CanIndicationFlags.w[1].b6
#define CanWriteSyncStW_Actn_Rq_0_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_StW_Actn_Rq_0_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_STATUS_C_TRANSMISSION_ind_flag 	CanIndicationFlags.w[1].b7
#define CanWriteSyncSTATUS_C_TRANSMISSION_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_STATUS_C_TRANSMISSION_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_VIN_0_ind_flag 	CanIndicationFlags.w[1].b10
#define CanWriteSyncVIN_0_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_VIN_0_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_MOT3_ind_flag 	CanIndicationFlags.w[1].b11
#define CanWriteSyncMOT3_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_MOT3_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_ECM_1_ind_flag 	CanIndicationFlags.w[1].b12
#define CanWriteSyncECM_1_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_ECM_1_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_STATUS_C_DTCM_ind_flag 	CanIndicationFlags.w[1].b13
#define CanWriteSyncSTATUS_C_DTCM_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_STATUS_C_DTCM_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_DAS_A2_ind_flag 	CanIndicationFlags.w[1].b14
#define CanWriteSyncDAS_A2_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_DAS_A2_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_PAM_2_ind_flag 	CanIndicationFlags.w[1].b15
#define CanWriteSyncPAM_2_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_PAM_2_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_EPB_A1_ind_flag 	CanIndicationFlags.w[1].b16
#define CanWriteSyncEPB_A1_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_EPB_A1_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_DAS_A1_ind_flag 	CanIndicationFlags.w[1].b17
#define CanWriteSyncDAS_A1_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_DAS_A1_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_BSM_4_ind_flag 	CanIndicationFlags.w[2].b0
#define CanWriteSyncBSM_4_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_BSM_4_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_BSM_2_ind_flag 	CanIndicationFlags.w[2].b1
#define CanWriteSyncBSM_2_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_BSM_2_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_DTCM_A1_ind_flag 	CanIndicationFlags.w[2].b2
#define CanWriteSyncDTCM_A1_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_DTCM_A1_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_SC_ind_flag 	CanIndicationFlags.w[2].b3
#define CanWriteSyncSC_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_SC_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_ORC_YRS_DATA_ind_flag 	CanIndicationFlags.w[2].b4
#define CanWriteSyncORC_YRS_DATA_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_ORC_YRS_DATA_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_MOTGEAR2_ind_flag 	CanIndicationFlags.w[2].b5
#define CanWriteSyncMOTGEAR2_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_MOTGEAR2_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_MOT4_ind_flag 	CanIndicationFlags.w[2].b6
#define CanWriteSyncMOT4_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_MOT4_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_BSM_YRS_DATA_ind_flag 	CanIndicationFlags.w[2].b7
#define CanWriteSyncBSM_YRS_DATA_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_BSM_YRS_DATA_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_ASR4_ind_flag 	CanIndicationFlags.w[2].b10
#define CanWriteSyncASR4_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_ASR4_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_ASR3_ind_flag 	CanIndicationFlags.w[2].b11
#define CanWriteSyncASR3_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_ASR3_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_RFHUB_A4_ind_flag 	CanIndicationFlags.w[2].b12
#define CanWriteSyncRFHUB_A4_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_RFHUB_A4_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_IPC_CFG_Feature_ind_flag 	CanIndicationFlags.w[2].b13
#define CanWriteSyncIPC_CFG_Feature_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_IPC_CFG_Feature_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_ESL_CODE_RESPONSE_ind_flag 	CanIndicationFlags.w[2].b14
#define CanWriteSyncESL_CODE_RESPONSE_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_ESL_CODE_RESPONSE_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_IMMO_CODE_REQUEST_ind_flag 	CanIndicationFlags.w[2].b15
#define CanWriteSyncIMMO_CODE_REQUEST_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_IMMO_CODE_REQUEST_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_TRM_MKP_KEY_ind_flag 	CanIndicationFlags.w[2].b16
#define CanWriteSyncTRM_MKP_KEY_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_TRM_MKP_KEY_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_RFHUB_A3_ind_flag 	CanIndicationFlags.w[2].b17
#define CanWriteSyncRFHUB_A3_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_RFHUB_A3_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_APPL_ECU_BCM_0_ind_flag 	CanIndicationFlags.w[3].b0
#define CanWriteSyncAPPL_ECU_BCM_0_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_APPL_ECU_BCM_0_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_STATUS_C_SCCM_ind_flag 	CanIndicationFlags.w[3].b1
#define CanWriteSyncSTATUS_C_SCCM_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_STATUS_C_SCCM_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_STATUS_C_RFHM_ind_flag 	CanIndicationFlags.w[3].b2
#define CanWriteSyncSTATUS_C_RFHM_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_STATUS_C_RFHM_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_IPC_A1_ind_flag 	CanIndicationFlags.w[3].b3
#define CanWriteSyncIPC_A1_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_IPC_A1_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_STATUS_C_SPM_ind_flag 	CanIndicationFlags.w[3].b4
#define CanWriteSyncSTATUS_C_SPM_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_STATUS_C_SPM_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_STATUS_C_IPC_ind_flag 	CanIndicationFlags.w[3].b5
#define CanWriteSyncSTATUS_C_IPC_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_STATUS_C_IPC_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_STATUS_C_GSM_ind_flag 	CanIndicationFlags.w[3].b6
#define CanWriteSyncSTATUS_C_GSM_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_STATUS_C_GSM_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_STATUS_C_EPS_ind_flag 	CanIndicationFlags.w[3].b7
#define CanWriteSyncSTATUS_C_EPS_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_STATUS_C_EPS_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_STATUS_C_BSM_ind_flag 	CanIndicationFlags.w[3].b10
#define CanWriteSyncSTATUS_C_BSM_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_STATUS_C_BSM_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_STATUS_C_ECM2_ind_flag 	CanIndicationFlags.w[3].b11
#define CanWriteSyncSTATUS_C_ECM2_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_STATUS_C_ECM2_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_STATUS_C_ECM_ind_flag 	CanIndicationFlags.w[3].b12
#define CanWriteSyncSTATUS_C_ECM_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_STATUS_C_ECM_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_STATUS_C_ORC_ind_flag 	CanIndicationFlags.w[3].b13
#define CanWriteSyncSTATUS_C_ORC_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_STATUS_C_ORC_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_RFHUB_A2_ind_flag 	CanIndicationFlags.w[3].b14
#define CanWriteSyncRFHUB_A2_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_RFHUB_A2_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_RFHUB_A1_ind_flag 	CanIndicationFlags.w[3].b15
#define CanWriteSyncRFHUB_A1_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_RFHUB_A1_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_PAM_1_ind_flag 	CanIndicationFlags.w[3].b16
#define CanWriteSyncPAM_1_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_PAM_1_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_WAKE_C_RFHM_ind_flag 	CanIndicationFlags.w[3].b17
#define CanWriteSyncWAKE_C_RFHM_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_WAKE_C_RFHM_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_WAKE_C_SCCM_ind_flag 	CanIndicationFlags.w[4].b0
#define CanWriteSyncWAKE_C_SCCM_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_WAKE_C_SCCM_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_WAKE_C_ESL_ind_flag 	CanIndicationFlags.w[4].b1
#define CanWriteSyncWAKE_C_ESL_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_WAKE_C_ESL_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_WAKE_C_EPB_ind_flag 	CanIndicationFlags.w[4].b2
#define CanWriteSyncWAKE_C_EPB_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_WAKE_C_EPB_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_WAKE_C_BSM_ind_flag 	CanIndicationFlags.w[4].b3
#define CanWriteSyncWAKE_C_BSM_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_WAKE_C_BSM_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_MOT1_ind_flag 	CanIndicationFlags.w[4].b4
#define CanWriteSyncMOT1_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_MOT1_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_GE_ind_flag 	CanIndicationFlags.w[4].b5
#define CanWriteSyncGE_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_GE_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_TRM_CODE_RESPONSE_ind_flag 	CanIndicationFlags.w[4].b6
#define CanWriteSyncTRM_CODE_RESPONSE_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_TRM_CODE_RESPONSE_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_ORC_A2_ind_flag 	CanIndicationFlags.w[4].b7
#define CanWriteSyncORC_A2_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_ORC_A2_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_CRO_BCM_ind_flag 	CanIndicationFlags.w[4].b10
#define CanWriteSyncCRO_BCM_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_CRO_BCM_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_CFG_DATA_CODE_RSP_RBSS_ind_flag 	CanIndicationFlags.w[4].b11
#define CanWriteSyncCFG_DATA_CODE_RSP_RBSS_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_CFG_DATA_CODE_RSP_RBSS_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_CFG_DATA_CODE_RSP_LBSS_ind_flag 	CanIndicationFlags.w[4].b12
#define CanWriteSyncCFG_DATA_CODE_RSP_LBSS_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_CFG_DATA_CODE_RSP_LBSS_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_CFG_DATA_CODE_RSP_ICS_ind_flag 	CanIndicationFlags.w[4].b13
#define CanWriteSyncCFG_DATA_CODE_RSP_ICS_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_CFG_DATA_CODE_RSP_ICS_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_CFG_DATA_CODE_RSP_CSWM_ind_flag 	CanIndicationFlags.w[4].b14
#define CanWriteSyncCFG_DATA_CODE_RSP_CSWM_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_CFG_DATA_CODE_RSP_CSWM_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_CFG_DATA_CODE_RSP_CDM_ind_flag 	CanIndicationFlags.w[4].b15
#define CanWriteSyncCFG_DATA_CODE_RSP_CDM_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_CFG_DATA_CODE_RSP_CDM_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_CFG_DATA_CODE_RSP_AMP_ind_flag 	CanIndicationFlags.w[4].b16
#define CanWriteSyncCFG_DATA_CODE_RSP_AMP_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_CFG_DATA_CODE_RSP_AMP_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_CFG_DATA_CODE_RSP_ETM_ind_flag 	CanIndicationFlags.w[4].b17
#define CanWriteSyncCFG_DATA_CODE_RSP_ETM_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_CFG_DATA_CODE_RSP_ETM_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_CFG_DATA_CODE_RSP_DSM_ind_flag 	CanIndicationFlags.w[5].b0
#define CanWriteSyncCFG_DATA_CODE_RSP_DSM_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_CFG_DATA_CODE_RSP_DSM_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_CFG_DATA_CODE_RSP_PDM_ind_flag 	CanIndicationFlags.w[5].b1
#define CanWriteSyncCFG_DATA_CODE_RSP_PDM_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_CFG_DATA_CODE_RSP_PDM_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_CFG_DATA_CODE_RSP_ECC_ind_flag 	CanIndicationFlags.w[5].b2
#define CanWriteSyncCFG_DATA_CODE_RSP_ECC_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_CFG_DATA_CODE_RSP_ECC_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_CFG_DATA_CODE_RSP_DDM_ind_flag 	CanIndicationFlags.w[5].b3
#define CanWriteSyncCFG_DATA_CODE_RSP_DDM_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_CFG_DATA_CODE_RSP_DDM_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_CFG_DATA_CODE_RSP_IPC_ind_flag 	CanIndicationFlags.w[5].b4
#define CanWriteSyncCFG_DATA_CODE_RSP_IPC_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_CFG_DATA_CODE_RSP_IPC_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_STATUS_ICS_ind_flag 	CanIndicationFlags.w[5].b5
#define CanWriteSyncSTATUS_ICS_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_STATUS_ICS_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_STATUS_CDM_ind_flag 	CanIndicationFlags.w[5].b6
#define CanWriteSyncSTATUS_CDM_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_STATUS_CDM_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_STATUS_AMP_ind_flag 	CanIndicationFlags.w[5].b7
#define CanWriteSyncSTATUS_AMP_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_STATUS_AMP_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_ENVIRONMENTAL_CONDITIONS_ind_flag 	CanIndicationFlags.w[5].b10
#define CanWriteSyncENVIRONMENTAL_CONDITIONS_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_ENVIRONMENTAL_CONDITIONS_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_FT_HVAC_STAT_ind_flag 	CanIndicationFlags.w[5].b11
#define CanWriteSyncFT_HVAC_STAT_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_FT_HVAC_STAT_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_DCM_P_MSG_ind_flag 	CanIndicationFlags.w[5].b12
#define CanWriteSyncDCM_P_MSG_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_DCM_P_MSG_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_DCM_D_MSG_ind_flag 	CanIndicationFlags.w[5].b13
#define CanWriteSyncDCM_D_MSG_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_DCM_D_MSG_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_NWM_PLGM_ind_flag 	CanIndicationFlags.w[5].b14
#define CanWriteSyncNWM_PLGM_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_NWM_PLGM_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_NWM_ETM_ind_flag 	CanIndicationFlags.w[5].b15
#define CanWriteSyncNWM_ETM_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_NWM_ETM_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_NWM_DSM_ind_flag 	CanIndicationFlags.w[5].b16
#define CanWriteSyncNWM_DSM_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_NWM_DSM_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_NWM_PDM_ind_flag 	CanIndicationFlags.w[5].b17
#define CanWriteSyncNWM_PDM_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_NWM_PDM_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_NWM_DDM_ind_flag 	CanIndicationFlags.w[6].b0
#define CanWriteSyncNWM_DDM_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_NWM_DDM_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_APPL_ECU_BCM_1_ind_flag 	CanIndicationFlags.w[6].b1
#define CanWriteSyncAPPL_ECU_BCM_1_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_APPL_ECU_BCM_1_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_TRIP_A_B_ind_flag 	CanIndicationFlags.w[6].b2
#define CanWriteSyncTRIP_A_B_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_TRIP_A_B_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_TGW_A1_ind_flag 	CanIndicationFlags.w[6].b3
#define CanWriteSyncTGW_A1_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_TGW_A1_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_ICS_MSG_ind_flag 	CanIndicationFlags.w[6].b4
#define CanWriteSyncICS_MSG_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_ICS_MSG_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_HVAC_A1_ind_flag 	CanIndicationFlags.w[6].b5
#define CanWriteSyncHVAC_A1_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_HVAC_A1_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_DIRECT_INFO_ind_flag 	CanIndicationFlags.w[6].b6
#define CanWriteSyncDIRECT_INFO_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_DIRECT_INFO_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_HVAC_A4_ind_flag 	CanIndicationFlags.w[6].b7
#define CanWriteSyncHVAC_A4_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_HVAC_A4_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_STATUS_RRM_ind_flag 	CanIndicationFlags.w[6].b10
#define CanWriteSyncSTATUS_RRM_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_STATUS_RRM_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_STATUS_IPC_ind_flag 	CanIndicationFlags.w[6].b11
#define CanWriteSyncSTATUS_IPC_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_STATUS_IPC_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_STATUS_ECC_ind_flag 	CanIndicationFlags.w[6].b12
#define CanWriteSyncSTATUS_ECC_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_STATUS_ECC_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_HVAC_A2_ind_flag 	CanIndicationFlags.w[6].b13
#define CanWriteSyncHVAC_A2_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_HVAC_A2_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_PLG_A1_ind_flag 	CanIndicationFlags.w[6].b14
#define CanWriteSyncPLG_A1_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_PLG_A1_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_ECC_A1_ind_flag 	CanIndicationFlags.w[6].b15
#define CanWriteSyncECC_A1_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_ECC_A1_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_NWM_RBSS_ind_flag 	CanIndicationFlags.w[6].b16
#define CanWriteSyncNWM_RBSS_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_NWM_RBSS_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_NWM_LBSS_ind_flag 	CanIndicationFlags.w[6].b17
#define CanWriteSyncNWM_LBSS_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_NWM_LBSS_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_NWM_ICS_ind_flag 	CanIndicationFlags.w[7].b0
#define CanWriteSyncNWM_ICS_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_NWM_ICS_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_NWM_CSWM_ind_flag 	CanIndicationFlags.w[7].b1
#define CanWriteSyncNWM_CSWM_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_NWM_CSWM_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_NWM_CDM_ind_flag 	CanIndicationFlags.w[7].b2
#define CanWriteSyncNWM_CDM_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_NWM_CDM_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_NWM_AMP_ind_flag 	CanIndicationFlags.w[7].b3
#define CanWriteSyncNWM_AMP_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_NWM_AMP_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_NWM_ECC_ind_flag 	CanIndicationFlags.w[7].b4
#define CanWriteSyncNWM_ECC_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_NWM_ECC_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_NWM_IPC_ind_flag 	CanIndicationFlags.w[7].b5
#define CanWriteSyncNWM_IPC_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_NWM_IPC_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_CBC_I4_ind_flag 	CanIndicationFlags.w[7].b6
#define CanWriteSyncCBC_I4_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_CBC_I4_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_CMCM_UNLK_ind_flag 	CanIndicationFlags.w[7].b7
#define CanWriteSyncCMCM_UNLK_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_CMCM_UNLK_ind_flag = x;\
		CanEndFlagWriteSync();

#define NETC_RX_CFG_RQ_ind_flag 	CanIndicationFlags.w[7].b10
#define CanWriteSyncCFG_RQ_ind_flag(x)\
		CanStartFlagWriteSync();\
		NETC_RX_CFG_RQ_ind_flag = x;\
		CanEndFlagWriteSync();


/*************************************************************/
/* Signals of send messages                                  */
/*************************************************************/

#define NETC_TX_STATUS_C_BCM2BatteryVoltageLevel_bit	 STATUS_C_BCM2.status_c_bcm2.STATUS_C_BCM2BatteryVoltageLevel
#define NETC_TX_STATUS_C_BCM2EngineOffTimer_bit_0 STATUS_C_BCM2.status_c_bcm2.STATUS_C_BCM2EngineOffTimer_0
#define NETC_TX_STATUS_C_BCM2EngineOffTimer_bit_1 STATUS_C_BCM2.status_c_bcm2.STATUS_C_BCM2EngineOffTimer_1                                                                     
#define NETC_TX_STATUS_C_BCM2EngineOffTimer_bit(c)    { NETC_TX_STATUS_C_BCM2EngineOffTimer_bit_0 = ((vuint8)(((vuint16)(c) & 0xff)));\
                                        NETC_TX_STATUS_C_BCM2EngineOffTimer_bit_1 = ((vuint8)(((vuint16)(c) & 0xff00) >> 8));\
                                      }
#define NETC_TX_STATUS_C_BCM2IgnitionOffTimer_bit_0 STATUS_C_BCM2.status_c_bcm2.STATUS_C_BCM2IgnitionOffTimer_0
#define NETC_TX_STATUS_C_BCM2IgnitionOffTimer_bit_1 STATUS_C_BCM2.status_c_bcm2.STATUS_C_BCM2IgnitionOffTimer_1                                                                     
#define NETC_TX_STATUS_C_BCM2IgnitionOffTimer_bit(c)    { NETC_TX_STATUS_C_BCM2IgnitionOffTimer_bit_0 = ((vuint8)(((vuint16)(c) & 0xff)));\
                                        NETC_TX_STATUS_C_BCM2IgnitionOffTimer_bit_1 = ((vuint8)(((vuint16)(c) & 0xff00) >> 8));\
                                      }

#define NETC_TX_HVAC_INFOFT_HVAC_MD_STATSts_bit	 HVAC_INFO.hvac_info.HVAC_INFOFT_HVAC_MD_STATSts
#define NETC_TX_HVAC_INFOStopStartEnable_bit_0 HVAC_INFO.hvac_info.HVAC_INFOStopStartEnable_0
#define NETC_TX_HVAC_INFOStopStartEnable_bit_1 HVAC_INFO.hvac_info.HVAC_INFOStopStartEnable_1                                                                     
#define NETC_TX_HVAC_INFOStopStartEnable_bit(c)    { NETC_TX_HVAC_INFOStopStartEnable_bit_0 = ((vuint8)(((vuint16)(c) & 0x3)));\
                                        NETC_TX_HVAC_INFOStopStartEnable_bit_1 = ((vuint8)(((vuint16)(c) & 0x3fc) >> 2));\
                                      }

#define NETC_TX_CBC_PT4WPR_SYS_FLT_bit	 CBC_PT4.cbc_pt4.CBC_PT4WPR_SYS_FLT
#define NETC_TX_CBC_PT4WPR_SYS_WASH_bit	 CBC_PT4.cbc_pt4.CBC_PT4WPR_SYS_WASH
#define NETC_TX_CBC_PT4WPR_SYS_MIST_bit	 CBC_PT4.cbc_pt4.CBC_PT4WPR_SYS_MIST
#define NETC_TX_CBC_PT4WPR_SYS_AUTO_bit	 CBC_PT4.cbc_pt4.CBC_PT4WPR_SYS_AUTO
#define NETC_TX_CBC_PT4WPR_SYS_INT_bit	 CBC_PT4.cbc_pt4.CBC_PT4WPR_SYS_INT
#define NETC_TX_CBC_PT4WPR_SYS_HIGH_bit	 CBC_PT4.cbc_pt4.CBC_PT4WPR_SYS_HIGH
#define NETC_TX_CBC_PT4WPR_SYS_LOW_bit	 CBC_PT4.cbc_pt4.CBC_PT4WPR_SYS_LOW
#define NETC_TX_CBC_PT4WPR_SYS_OFF_bit	 CBC_PT4.cbc_pt4.CBC_PT4WPR_SYS_OFF
#define NETC_TX_CBC_PT4E_Mode_Sts_bit	 CBC_PT4.cbc_pt4.CBC_PT4E_Mode_Sts
#define NETC_TX_CBC_PT4SpSt_Pad2_bit	 CBC_PT4.cbc_pt4.CBC_PT4SpSt_Pad2
#define NETC_TX_CBC_PT4SpSt_Pad1_bit	 CBC_PT4.cbc_pt4.CBC_PT4SpSt_Pad1

#define NETC_TX_BCM_KEYON_COUNTER_C_CANKeyOnCounter_bit_0 BCM_KEYON_COUNTER_C_CAN.bcm_keyon_counter_c_can.BCM_KEYON_COUNTER_C_CANKeyOnCounter_0
#define NETC_TX_BCM_KEYON_COUNTER_C_CANKeyOnCounter_bit_1 BCM_KEYON_COUNTER_C_CAN.bcm_keyon_counter_c_can.BCM_KEYON_COUNTER_C_CANKeyOnCounter_1                                                                     
#define NETC_TX_BCM_KEYON_COUNTER_C_CANKeyOnCounter_bit(c)    { NETC_TX_BCM_KEYON_COUNTER_C_CANKeyOnCounter_bit_0 = ((vuint8)(((vuint16)(c) & 0xff)));\
                                        NETC_TX_BCM_KEYON_COUNTER_C_CANKeyOnCounter_bit_1 = ((vuint8)(((vuint16)(c) & 0xff00) >> 8));\
                                      }

#define NETC_TX_IBS4IBS_Tm_Lst_Reset_Sec_bit_0 IBS4.ibs4.IBS4IBS_Tm_Lst_Reset_Sec_0
#define NETC_TX_IBS4IBS_Tm_Lst_Reset_Sec_bit_1 IBS4.ibs4.IBS4IBS_Tm_Lst_Reset_Sec_1                                                                     
#define NETC_TX_IBS4IBS_Tm_Lst_Reset_Sec_bit(c)    { NETC_TX_IBS4IBS_Tm_Lst_Reset_Sec_bit_0 = ((vuint8)(((vuint16)(c) & 0xff)));\
                                        NETC_TX_IBS4IBS_Tm_Lst_Reset_Sec_bit_1 = ((vuint8)(((vuint16)(c) & 0xff00) >> 8));\
                                      }

#define NETC_TX_ECU_APPL_BCM_0ECU_APPL_BCM_bit_0 ECU_APPL_BCM_0.ecu_appl_bcm_0.ECU_APPL_BCM_0ECU_APPL_BCM_0
#define NETC_TX_ECU_APPL_BCM_0ECU_APPL_BCM_bit_1 ECU_APPL_BCM_0.ecu_appl_bcm_0.ECU_APPL_BCM_0ECU_APPL_BCM_1
#define NETC_TX_ECU_APPL_BCM_0ECU_APPL_BCM_bit_2 ECU_APPL_BCM_0.ecu_appl_bcm_0.ECU_APPL_BCM_0ECU_APPL_BCM_2
#define NETC_TX_ECU_APPL_BCM_0ECU_APPL_BCM_bit_3 ECU_APPL_BCM_0.ecu_appl_bcm_0.ECU_APPL_BCM_0ECU_APPL_BCM_3
#define NETC_TX_ECU_APPL_BCM_0ECU_APPL_BCM_bit_4 ECU_APPL_BCM_0.ecu_appl_bcm_0.ECU_APPL_BCM_0ECU_APPL_BCM_4
#define NETC_TX_ECU_APPL_BCM_0ECU_APPL_BCM_bit_5 ECU_APPL_BCM_0.ecu_appl_bcm_0.ECU_APPL_BCM_0ECU_APPL_BCM_5
#define NETC_TX_ECU_APPL_BCM_0ECU_APPL_BCM_bit_6 ECU_APPL_BCM_0.ecu_appl_bcm_0.ECU_APPL_BCM_0ECU_APPL_BCM_6
#define NETC_TX_ECU_APPL_BCM_0ECU_APPL_BCM_bit_7 ECU_APPL_BCM_0.ecu_appl_bcm_0.ECU_APPL_BCM_0ECU_APPL_BCM_7

#define NETC_TX_CONFIGURATION_DATA_CODE_REQUEST_0Digit_10_bit	 CONFIGURATION_DATA_CODE_REQUEST_0.configuration_data_code_request_0.CONFIGURATION_DATA_CODE_REQUEST_0Digit_10
#define NETC_TX_CONFIGURATION_DATA_CODE_REQUEST_0Digit_11_bit	 CONFIGURATION_DATA_CODE_REQUEST_0.configuration_data_code_request_0.CONFIGURATION_DATA_CODE_REQUEST_0Digit_11
#define NETC_TX_CONFIGURATION_DATA_CODE_REQUEST_0Digit_08_bit	 CONFIGURATION_DATA_CODE_REQUEST_0.configuration_data_code_request_0.CONFIGURATION_DATA_CODE_REQUEST_0Digit_08
#define NETC_TX_CONFIGURATION_DATA_CODE_REQUEST_0Digit_09_bit	 CONFIGURATION_DATA_CODE_REQUEST_0.configuration_data_code_request_0.CONFIGURATION_DATA_CODE_REQUEST_0Digit_09
#define NETC_TX_CONFIGURATION_DATA_CODE_REQUEST_0Digit_06_bit	 CONFIGURATION_DATA_CODE_REQUEST_0.configuration_data_code_request_0.CONFIGURATION_DATA_CODE_REQUEST_0Digit_06
#define NETC_TX_CONFIGURATION_DATA_CODE_REQUEST_0Digit_07_bit	 CONFIGURATION_DATA_CODE_REQUEST_0.configuration_data_code_request_0.CONFIGURATION_DATA_CODE_REQUEST_0Digit_07
#define NETC_TX_CONFIGURATION_DATA_CODE_REQUEST_0Digit_04_bit	 CONFIGURATION_DATA_CODE_REQUEST_0.configuration_data_code_request_0.CONFIGURATION_DATA_CODE_REQUEST_0Digit_04
#define NETC_TX_CONFIGURATION_DATA_CODE_REQUEST_0Digit_05_bit	 CONFIGURATION_DATA_CODE_REQUEST_0.configuration_data_code_request_0.CONFIGURATION_DATA_CODE_REQUEST_0Digit_05
#define NETC_TX_CONFIGURATION_DATA_CODE_REQUEST_0Digit_02_bit	 CONFIGURATION_DATA_CODE_REQUEST_0.configuration_data_code_request_0.CONFIGURATION_DATA_CODE_REQUEST_0Digit_02
#define NETC_TX_CONFIGURATION_DATA_CODE_REQUEST_0Digit_03_bit	 CONFIGURATION_DATA_CODE_REQUEST_0.configuration_data_code_request_0.CONFIGURATION_DATA_CODE_REQUEST_0Digit_03
#define NETC_TX_CONFIGURATION_DATA_CODE_REQUEST_0Digit_01_bit	 CONFIGURATION_DATA_CODE_REQUEST_0.configuration_data_code_request_0.CONFIGURATION_DATA_CODE_REQUEST_0Digit_01

#define NETC_TX_CBC_VTA_0TheftAlarmStatus_bit	 CBC_VTA_0.cbc_vta_0.CBC_VTA_0TheftAlarmStatus

#define NETC_TX_DIAGNOSTIC_RESPONSE_BCM_0N_PDU_bit_0 DIAGNOSTIC_RESPONSE_BCM_0.diagnostic_response_bcm_0.DIAGNOSTIC_RESPONSE_BCM_0N_PDU_0
#define NETC_TX_DIAGNOSTIC_RESPONSE_BCM_0N_PDU_bit_1 DIAGNOSTIC_RESPONSE_BCM_0.diagnostic_response_bcm_0.DIAGNOSTIC_RESPONSE_BCM_0N_PDU_1
#define NETC_TX_DIAGNOSTIC_RESPONSE_BCM_0N_PDU_bit_2 DIAGNOSTIC_RESPONSE_BCM_0.diagnostic_response_bcm_0.DIAGNOSTIC_RESPONSE_BCM_0N_PDU_2
#define NETC_TX_DIAGNOSTIC_RESPONSE_BCM_0N_PDU_bit_3 DIAGNOSTIC_RESPONSE_BCM_0.diagnostic_response_bcm_0.DIAGNOSTIC_RESPONSE_BCM_0N_PDU_3
#define NETC_TX_DIAGNOSTIC_RESPONSE_BCM_0N_PDU_bit_4 DIAGNOSTIC_RESPONSE_BCM_0.diagnostic_response_bcm_0.DIAGNOSTIC_RESPONSE_BCM_0N_PDU_4
#define NETC_TX_DIAGNOSTIC_RESPONSE_BCM_0N_PDU_bit_5 DIAGNOSTIC_RESPONSE_BCM_0.diagnostic_response_bcm_0.DIAGNOSTIC_RESPONSE_BCM_0N_PDU_5
#define NETC_TX_DIAGNOSTIC_RESPONSE_BCM_0N_PDU_bit_6 DIAGNOSTIC_RESPONSE_BCM_0.diagnostic_response_bcm_0.DIAGNOSTIC_RESPONSE_BCM_0N_PDU_6
#define NETC_TX_DIAGNOSTIC_RESPONSE_BCM_0N_PDU_bit_7 DIAGNOSTIC_RESPONSE_BCM_0.diagnostic_response_bcm_0.DIAGNOSTIC_RESPONSE_BCM_0N_PDU_7

#define NETC_TX_STATUS_B_CAN2PowerModeSts_bit	 STATUS_B_CAN2.status_b_can2.STATUS_B_CAN2PowerModeSts
#define NETC_TX_STATUS_B_CAN2BCMAutoCrankSts_bit	 STATUS_B_CAN2.status_b_can2.STATUS_B_CAN2BCMAutoCrankSts

#define NETC_TX_EXTERNAL_LIGHTS_0BrakePedalSts_bit	 EXTERNAL_LIGHTS_0.external_lights_0.EXTERNAL_LIGHTS_0BrakePedalSts
#define NETC_TX_EXTERNAL_LIGHTS_0FrontFogLightSts_bit	 EXTERNAL_LIGHTS_0.external_lights_0.EXTERNAL_LIGHTS_0FrontFogLightSts
#define NETC_TX_EXTERNAL_LIGHTS_0LowBeamSts_bit	 EXTERNAL_LIGHTS_0.external_lights_0.EXTERNAL_LIGHTS_0LowBeamSts
#define NETC_TX_EXTERNAL_LIGHTS_0HighBeamSts_bit	 EXTERNAL_LIGHTS_0.external_lights_0.EXTERNAL_LIGHTS_0HighBeamSts
#define NETC_TX_EXTERNAL_LIGHTS_0LHParkingLightSts_bit	 EXTERNAL_LIGHTS_0.external_lights_0.EXTERNAL_LIGHTS_0LHParkingLightSts
#define NETC_TX_EXTERNAL_LIGHTS_0RHParkingLightSts_bit	 EXTERNAL_LIGHTS_0.external_lights_0.EXTERNAL_LIGHTS_0RHParkingLightSts
#define NETC_TX_EXTERNAL_LIGHTS_0StopLightSts_bit	 EXTERNAL_LIGHTS_0.external_lights_0.EXTERNAL_LIGHTS_0StopLightSts
#define NETC_TX_EXTERNAL_LIGHTS_0RHTurnSignalSts_bit	 EXTERNAL_LIGHTS_0.external_lights_0.EXTERNAL_LIGHTS_0RHTurnSignalSts
#define NETC_TX_EXTERNAL_LIGHTS_0LHTurnSignalSts_bit	 EXTERNAL_LIGHTS_0.external_lights_0.EXTERNAL_LIGHTS_0LHTurnSignalSts
#define NETC_TX_EXTERNAL_LIGHTS_0StopLightFault_bit	 EXTERNAL_LIGHTS_0.external_lights_0.EXTERNAL_LIGHTS_0StopLightFault
#define NETC_TX_EXTERNAL_LIGHTS_0LHTurnLightFault_bit	 EXTERNAL_LIGHTS_0.external_lights_0.EXTERNAL_LIGHTS_0LHTurnLightFault
#define NETC_TX_EXTERNAL_LIGHTS_0RHTurnLightFault_bit	 EXTERNAL_LIGHTS_0.external_lights_0.EXTERNAL_LIGHTS_0RHTurnLightFault
#define NETC_TX_EXTERNAL_LIGHTS_0HighBeamFault_bit	 EXTERNAL_LIGHTS_0.external_lights_0.EXTERNAL_LIGHTS_0HighBeamFault
#define NETC_TX_EXTERNAL_LIGHTS_0ParkingLightFault_bit	 EXTERNAL_LIGHTS_0.external_lights_0.EXTERNAL_LIGHTS_0ParkingLightFault
#define NETC_TX_EXTERNAL_LIGHTS_0LowBeamFault_bit	 EXTERNAL_LIGHTS_0.external_lights_0.EXTERNAL_LIGHTS_0LowBeamFault

#define NETC_TX_ASBM1_CONTROLPPARequestSts_bit	 ASBM1_CONTROL.asbm1_control.ASBM1_CONTROLPPARequestSts
#define NETC_TX_ASBM1_CONTROLFCWRequestSts_bit	 ASBM1_CONTROL.asbm1_control.ASBM1_CONTROLFCWRequestSts
#define NETC_TX_ASBM1_CONTROLLDWRequestSts_bit	 ASBM1_CONTROL.asbm1_control.ASBM1_CONTROLLDWRequestSts
#define NETC_TX_ASBM1_CONTROLPAMRequestSts_bit	 ASBM1_CONTROL.asbm1_control.ASBM1_CONTROLPAMRequestSts

#define NETC_TX_HVAC_INFO2A2D_FT_HVAC_BLOWER_VOLTAGE_bit_0 HVAC_A4.hvac_info2.HVAC_INFO2A2D_FT_HVAC_BLOWER_VOLTAGE_0
#define NETC_TX_HVAC_INFO2A2D_FT_HVAC_BLOWER_VOLTAGE_bit_1 HVAC_A4.hvac_info2.HVAC_INFO2A2D_FT_HVAC_BLOWER_VOLTAGE_1                                                                     
#define NETC_TX_HVAC_INFO2A2D_FT_HVAC_BLOWER_VOLTAGE_bit(c)    { NETC_TX_HVAC_INFO2A2D_FT_HVAC_BLOWER_VOLTAGE_bit_0 = ((vuint8)(((vuint16)(c) & 0x3)));\
                                        NETC_TX_HVAC_INFO2A2D_FT_HVAC_BLOWER_VOLTAGE_bit_1 = ((vuint8)(((vuint16)(c) & 0x3fc) >> 2));\
                                      }
#define NETC_TX_HVAC_INFO2A2D_DRV_TEMP_DR_POS_bit_0 HVAC_A4.hvac_info2.HVAC_INFO2A2D_DRV_TEMP_DR_POS_0
#define NETC_TX_HVAC_INFO2A2D_DRV_TEMP_DR_POS_bit_1 HVAC_A4.hvac_info2.HVAC_INFO2A2D_DRV_TEMP_DR_POS_1                                                                     
#define NETC_TX_HVAC_INFO2A2D_DRV_TEMP_DR_POS_bit(c)    { NETC_TX_HVAC_INFO2A2D_DRV_TEMP_DR_POS_bit_0 = ((vuint8)(((vuint16)(c) & 0xf)));\
                                        NETC_TX_HVAC_INFO2A2D_DRV_TEMP_DR_POS_bit_1 = ((vuint8)(((vuint16)(c) & 0xff0) >> 4));\
                                      }

#define NETC_TX_IBS2PN14_LS_Actv_bit	 IBS2.ibs2.IBS2PN14_LS_Actv
#define NETC_TX_IBS2IBS_SOC_byte_0 IBS2.ibs2.IBS2IBS_SOC_0
#define NETC_TX_IBS2IBS_SOC_byte_1 IBS2.ibs2.IBS2IBS_SOC_1                                                                     
#define NETC_TX_IBS2IBS_SOC_byte(c)    { NETC_TX_IBS2IBS_SOC_byte_0 = ((vuint8)(((vuint16)(c) & 0x1f)));\
                                        NETC_TX_IBS2IBS_SOC_byte_1 = ((vuint8)(((vuint16)(c) & 0x1fe0) >> 5));\
                                      }
#define NETC_TX_IBS2IBS_SOF_Q_byte_0 IBS2.ibs2.IBS2IBS_SOF_Q_0
#define NETC_TX_IBS2IBS_SOF_Q_byte_1 IBS2.ibs2.IBS2IBS_SOF_Q_1                                                                     
#define NETC_TX_IBS2IBS_SOF_Q_byte(c)    { NETC_TX_IBS2IBS_SOF_Q_byte_0 = ((vuint8)(((vuint16)(c) & 0x1f)));\
                                        NETC_TX_IBS2IBS_SOF_Q_byte_1 = ((vuint8)(((vuint16)(c) & 0x1fe0) >> 5));\
                                      }
#define NETC_TX_IBS2IBS_SOH_Q_bit_0 IBS2.ibs2.IBS2IBS_SOH_Q_0
#define NETC_TX_IBS2IBS_SOH_Q_bit_1 IBS2.ibs2.IBS2IBS_SOH_Q_1                                                                     
#define NETC_TX_IBS2IBS_SOH_Q_bit(c)    { NETC_TX_IBS2IBS_SOH_Q_bit_0 = ((vuint8)(((vuint16)(c) & 0x7f)));\
                                        NETC_TX_IBS2IBS_SOH_Q_bit_1 = ((vuint8)(((vuint16)(c) & 0x7f80) >> 7));\
                                      }
#define NETC_TX_IBS2Batt_ST_Crit_bit	 IBS2.ibs2.IBS2Batt_ST_Crit
#define NETC_TX_IBS2IBS_SOF_t_bit_0 IBS2.ibs2.IBS2IBS_SOF_t_0
#define NETC_TX_IBS2IBS_SOF_t_bit_1 IBS2.ibs2.IBS2IBS_SOF_t_1                                                                     
#define NETC_TX_IBS2IBS_SOF_t_bit(c)    { NETC_TX_IBS2IBS_SOF_t_bit_0 = ((vuint8)(((vuint16)(c) & 0x3)));\
                                        NETC_TX_IBS2IBS_SOF_t_bit_1 = ((vuint8)(((vuint16)(c) & 0x3fc) >> 2));\
                                      }
#define NETC_TX_IBS2IBS_SOH_Q_Accuracy_bit	 IBS2.ibs2.IBS2IBS_SOH_Q_Accuracy
#define NETC_TX_IBS2IBS_SOF_Q_Accuracy_bit	 IBS2.ibs2.IBS2IBS_SOF_Q_Accuracy
#define NETC_TX_IBS2IBS_SOC_Accuracy_bit	 IBS2.ibs2.IBS2IBS_SOC_Accuracy
#define NETC_TX_IBS2PLG_Req_PE_bit	 IBS2.ibs2.IBS2PLG_Req_PE
#define NETC_TX_IBS2IBS_SOF_t_Accuracy_bit	 IBS2.ibs2.IBS2IBS_SOF_t_Accuracy

#define NETC_TX_VEHICLE_SPEED_ODOMETER_0VehicleSpeed_bit_0 VEHICLE_SPEED_ODOMETER_0.vehicle_speed_odometer_0.VEHICLE_SPEED_ODOMETER_0VehicleSpeed_0
#define NETC_TX_VEHICLE_SPEED_ODOMETER_0VehicleSpeed_bit_1 VEHICLE_SPEED_ODOMETER_0.vehicle_speed_odometer_0.VEHICLE_SPEED_ODOMETER_0VehicleSpeed_1                                                                     
#define NETC_TX_VEHICLE_SPEED_ODOMETER_0VehicleSpeed_bit(c)    { NETC_TX_VEHICLE_SPEED_ODOMETER_0VehicleSpeed_bit_0 = ((vuint8)(((vuint16)(c) & 0xff)));\
                                        NETC_TX_VEHICLE_SPEED_ODOMETER_0VehicleSpeed_bit_1 = ((vuint8)(((vuint16)(c) & 0xff00) >> 8));\
                                      }
#define NETC_TX_VEHICLE_SPEED_ODOMETER_0VehicleSpeedFailSts_bit	 VEHICLE_SPEED_ODOMETER_0.vehicle_speed_odometer_0.VEHICLE_SPEED_ODOMETER_0VehicleSpeedFailSts
#define NETC_TX_VEHICLE_SPEED_ODOMETER_0FUEL_VOLT2_byte	 VEHICLE_SPEED_ODOMETER_0.vehicle_speed_odometer_0.VEHICLE_SPEED_ODOMETER_0FUEL_VOLT2
#define NETC_TX_VEHICLE_SPEED_ODOMETER_0TravelDistance_byte	 VEHICLE_SPEED_ODOMETER_0.vehicle_speed_odometer_0.VEHICLE_SPEED_ODOMETER_0TravelDistance
#define NETC_TX_VEHICLE_SPEED_ODOMETER_0TotalOdometer_bit_0 VEHICLE_SPEED_ODOMETER_0.vehicle_speed_odometer_0.VEHICLE_SPEED_ODOMETER_0TotalOdometer_0
#define NETC_TX_VEHICLE_SPEED_ODOMETER_0TotalOdometer_bit_1 VEHICLE_SPEED_ODOMETER_0.vehicle_speed_odometer_0.VEHICLE_SPEED_ODOMETER_0TotalOdometer_1
#define NETC_TX_VEHICLE_SPEED_ODOMETER_0TotalOdometer_bit_2 VEHICLE_SPEED_ODOMETER_0.vehicle_speed_odometer_0.VEHICLE_SPEED_ODOMETER_0TotalOdometer_2                                                                     
#define NETC_TX_VEHICLE_SPEED_ODOMETER_0TotalOdometer_bit(c)    { NETC_TX_VEHICLE_SPEED_ODOMETER_0TotalOdometer_bit_0 = ((vuint8)(((vuint32)(c) & 0xf)));\
                                        NETC_TX_VEHICLE_SPEED_ODOMETER_0TotalOdometer_bit_1 = ((vuint8)(((vuint32)(c) & 0xff0) >> 4));\
                                        NETC_TX_VEHICLE_SPEED_ODOMETER_0TotalOdometer_bit_2 = ((vuint8)(((vuint32)(c) & 0xff000) >> 12));\
                                      }
#define NETC_TX_VEHICLE_SPEED_ODOMETER_0FUEL_VOLT_byte	 VEHICLE_SPEED_ODOMETER_0.vehicle_speed_odometer_0.VEHICLE_SPEED_ODOMETER_0FUEL_VOLT

#define NETC_TX_STATUS_C_BCMHandBrakeSts_bit	 STATUS_C_BCM.status_c_bcm.STATUS_C_BCMHandBrakeSts
#define NETC_TX_STATUS_C_BCMBrakeFluidLevelSts_bit	 STATUS_C_BCM.status_c_bcm.STATUS_C_BCMBrakeFluidLevelSts
#define NETC_TX_STATUS_C_BCMDriverDoorSts_bit	 STATUS_C_BCM.status_c_bcm.STATUS_C_BCMDriverDoorSts
#define NETC_TX_STATUS_C_BCMPsngrDoorSts_bit	 STATUS_C_BCM.status_c_bcm.STATUS_C_BCMPsngrDoorSts
#define NETC_TX_STATUS_C_BCMLHRDoorSts_bit	 STATUS_C_BCM.status_c_bcm.STATUS_C_BCMLHRDoorSts
#define NETC_TX_STATUS_C_BCMRHRDoorSts_bit	 STATUS_C_BCM.status_c_bcm.STATUS_C_BCMRHRDoorSts
#define NETC_TX_STATUS_C_BCMRHatchSts_bit	 STATUS_C_BCM.status_c_bcm.STATUS_C_BCMRHatchSts
#define NETC_TX_STATUS_C_BCMBonnetSts_bit	 STATUS_C_BCM.status_c_bcm.STATUS_C_BCMBonnetSts
#define NETC_TX_STATUS_C_BCMRearLockLastElSts_bit	 STATUS_C_BCM.status_c_bcm.STATUS_C_BCMRearLockLastElSts
#define NETC_TX_STATUS_C_BCMDoorLockLastElSts_bit	 STATUS_C_BCM.status_c_bcm.STATUS_C_BCMDoorLockLastElSts
#define NETC_TX_STATUS_C_BCMExteriorRearReleaseSwitchSts_bit	 STATUS_C_BCM.status_c_bcm.STATUS_C_BCMExteriorRearReleaseSwitchSts

#define NETC_TX_STATUS_B_CANCityModeSts_bit	 STATUS_B_CAN.status_b_can.STATUS_B_CANCityModeSts
#define NETC_TX_STATUS_B_CANCompressorACReqSts_bit	 STATUS_B_CAN.status_b_can.STATUS_B_CANCompressorACReqSts
#define NETC_TX_STATUS_B_CANExternalTemperature_byte	 STATUS_B_CAN.status_b_can.STATUS_B_CANExternalTemperature
#define NETC_TX_STATUS_B_CANRechargeSts_bit	 STATUS_B_CAN.status_b_can.STATUS_B_CANRechargeSts
#define NETC_TX_STATUS_B_CANDriveStyleSts_bit	 STATUS_B_CAN.status_b_can.STATUS_B_CANDriveStyleSts
#define NETC_TX_STATUS_B_CANExternalTemperatureFailSts_bit	 STATUS_B_CAN.status_b_can.STATUS_B_CANExternalTemperatureFailSts
#define NETC_TX_STATUS_B_CANFrontWiperMoveSts_bit	 STATUS_B_CAN.status_b_can.STATUS_B_CANFrontWiperMoveSts
#define NETC_TX_STATUS_B_CANRHeatedWindowSts_bit	 STATUS_B_CAN.status_b_can.STATUS_B_CANRHeatedWindowSts
#define NETC_TX_STATUS_B_CANHandBrakeSts_bit	 STATUS_B_CAN.status_b_can.STATUS_B_CANHandBrakeSts
#define NETC_TX_STATUS_B_CANBrakeFluidLevelSts_bit	 STATUS_B_CAN.status_b_can.STATUS_B_CANBrakeFluidLevelSts
#define NETC_TX_STATUS_B_CANBrakePadWearSts_bit	 STATUS_B_CAN.status_b_can.STATUS_B_CANBrakePadWearSts

#define NETC_TX_IBS3IBS_SOF_V_byte	 IBS3.ibs3.IBS3IBS_SOF_V
#define NETC_TX_IBS3IBS_SOF_V_Accuracy_bit	 IBS3.ibs3.IBS3IBS_SOF_V_Accuracy
#define NETC_TX_IBS3IBS_Error_Calib_bit	 IBS3.ibs3.IBS3IBS_Error_Calib
#define NETC_TX_IBS3IBS_Error_Ident_bit	 IBS3.ibs3.IBS3IBS_Error_Ident
#define NETC_TX_IBS3IBS_Error_NVM_bit	 IBS3.ibs3.IBS3IBS_Error_NVM
#define NETC_TX_IBS3Converter_1_State_bit	 IBS3.ibs3.IBS3Converter_1_State
#define NETC_TX_IBS3Converter_1_Mode_bit	 IBS3.ibs3.IBS3Converter_1_Mode
#define NETC_TX_IBS3Converter_2_Mode_bit	 IBS3.ibs3.IBS3Converter_2_Mode
#define NETC_TX_IBS3Converter_2_State_bit	 IBS3.ibs3.IBS3Converter_2_State
#define NETC_TX_IBS3IBS_SOF_V1_byte	 IBS3.ibs3.IBS3IBS_SOF_V1

#define NETC_TX_IBS1IBS_V_BATT_bit_0 IBS1.ibs1.IBS1IBS_V_BATT_0
#define NETC_TX_IBS1IBS_V_BATT_bit_1 IBS1.ibs1.IBS1IBS_V_BATT_1                                                                     
#define NETC_TX_IBS1IBS_V_BATT_bit(c)    { NETC_TX_IBS1IBS_V_BATT_bit_0 = ((vuint8)(((vuint16)(c) & 0x3f)));\
                                        NETC_TX_IBS1IBS_V_BATT_bit_1 = ((vuint8)(((vuint16)(c) & 0x3fc0) >> 6));\
                                      }
#define NETC_TX_IBS1IBS_I_BATT_bit_0 IBS1.ibs1.IBS1IBS_I_BATT_0
#define NETC_TX_IBS1IBS_I_BATT_bit_1 IBS1.ibs1.IBS1IBS_I_BATT_1
#define NETC_TX_IBS1IBS_I_BATT_bit_2 IBS1.ibs1.IBS1IBS_I_BATT_2                                                                     
#define NETC_TX_IBS1IBS_I_BATT_bit(c)    { NETC_TX_IBS1IBS_I_BATT_bit_0 = ((vuint8)(((vuint32)(c) & 0x3f)));\
                                        NETC_TX_IBS1IBS_I_BATT_bit_1 = ((vuint8)(((vuint32)(c) & 0x3fc0) >> 6));\
                                        NETC_TX_IBS1IBS_I_BATT_bit_2 = ((vuint8)(((vuint32)(c) & 0x3fc000) >> 14));\
                                      }
#define NETC_TX_IBS1IBS_T_BATT_byte_0 IBS1.ibs1.IBS1IBS_T_BATT_0
#define NETC_TX_IBS1IBS_T_BATT_byte_1 IBS1.ibs1.IBS1IBS_T_BATT_1                                                                     
#define NETC_TX_IBS1IBS_T_BATT_byte(c)    { NETC_TX_IBS1IBS_T_BATT_byte_0 = ((vuint8)(((vuint16)(c) & 0x3f)));\
                                        NETC_TX_IBS1IBS_T_BATT_byte_1 = ((vuint8)(((vuint16)(c) & 0x3fc0) >> 6));\
                                      }
#define NETC_TX_IBS1IBS_PreWakeupVoltage_bit_0 IBS1.ibs1.IBS1IBS_PreWakeupVoltage_0
#define NETC_TX_IBS1IBS_PreWakeupVoltage_bit_1 IBS1.ibs1.IBS1IBS_PreWakeupVoltage_1                                                                     
#define NETC_TX_IBS1IBS_PreWakeupVoltage_bit(c)    { NETC_TX_IBS1IBS_PreWakeupVoltage_bit_0 = ((vuint8)(((vuint16)(c) & 0xff)));\
                                        NETC_TX_IBS1IBS_PreWakeupVoltage_bit_1 = ((vuint8)(((vuint16)(c) & 0xff00) >> 8));\
                                      }
#define NETC_TX_IBS1IBS_I_RANGE_bit	 IBS1.ibs1.IBS1IBS_I_RANGE
#define NETC_TX_IBS1IBS_Temp_Status_bit	 IBS1.ibs1.IBS1IBS_Temp_Status
#define NETC_TX_IBS1IBS_Voltage_Status_bit	 IBS1.ibs1.IBS1IBS_Voltage_Status
#define NETC_TX_IBS1IBS_Current_Status_bit	 IBS1.ibs1.IBS1IBS_Current_Status

#define NETC_TX_BATTERY_INFOBatteryVoltageLevelHP_bit_0 BATTERY_INFO.battery_info.BATTERY_INFOBatteryVoltageLevelHP_0
#define NETC_TX_BATTERY_INFOBatteryVoltageLevelHP_bit_1 BATTERY_INFO.battery_info.BATTERY_INFOBatteryVoltageLevelHP_1                                                                     
#define NETC_TX_BATTERY_INFOBatteryVoltageLevelHP_bit(c)    { NETC_TX_BATTERY_INFOBatteryVoltageLevelHP_bit_0 = ((vuint8)(((vuint16)(c) & 0x1)));\
                                        NETC_TX_BATTERY_INFOBatteryVoltageLevelHP_bit_1 = ((vuint8)(((vuint16)(c) & 0x1fe) >> 1));\
                                      }
#define NETC_TX_BATTERY_INFOVoltageMin_byte	 BATTERY_INFO.battery_info.BATTERY_INFOVoltageMin
#define NETC_TX_BATTERY_INFOVoltageMax_byte	 BATTERY_INFO.battery_info.BATTERY_INFOVoltageMax
#define NETC_TX_BATTERY_INFOBCMSAMFailSts_bit	 BATTERY_INFO.battery_info.BATTERY_INFOBCMSAMFailSts

#define NETC_TX_CFG_FeatureCFG_FeatureCntrl_byte	 CFG_Feature.cfg_feature.CFG_FeatureCFG_FeatureCntrl
#define NETC_TX_CFG_FeatureCFG_STAT_RQCntrl_bit	 CFG_Feature.cfg_feature.CFG_FeatureCFG_STAT_RQCntrl
#define NETC_TX_CFG_FeatureCFG_SETCntrl_byte	 CFG_Feature.cfg_feature.CFG_FeatureCFG_SETCntrl

#define NETC_TX_CBC_PT3DriverDoorAjarRawValSts_bit	 CBC_PT3.cbc_pt3.CBC_PT3DriverDoorAjarRawValSts
#define NETC_TX_CBC_PT3PsngrDoorAjarRawValSts_bit	 CBC_PT3.cbc_pt3.CBC_PT3PsngrDoorAjarRawValSts
#define NETC_TX_CBC_PT3BonnetAjarRawValSts_bit	 CBC_PT3.cbc_pt3.CBC_PT3BonnetAjarRawValSts
#define NETC_TX_CBC_PT3DriverDoor2AjarRawValSts_bit	 CBC_PT3.cbc_pt3.CBC_PT3DriverDoor2AjarRawValSts
#define NETC_TX_CBC_PT3PsngrDoorAjarXtionSts_bit	 CBC_PT3.cbc_pt3.CBC_PT3PsngrDoorAjarXtionSts
#define NETC_TX_CBC_PT3DriverDoorAjarXtionSts_bit	 CBC_PT3.cbc_pt3.CBC_PT3DriverDoorAjarXtionSts
#define NETC_TX_CBC_PT3DriverDoor2AjarXtionSts_bit	 CBC_PT3.cbc_pt3.CBC_PT3DriverDoor2AjarXtionSts

#define NETC_TX_CBC_PT1PanelIntensitySts_byte	 CBC_PT1.cbc_pt1.CBC_PT1PanelIntensitySts
#define NETC_TX_CBC_PT1InternalLightSts_bit	 CBC_PT1.cbc_pt1.CBC_PT1InternalLightSts
#define NETC_TX_CBC_PT1FT_WPR_NOT_PRK_bit	 CBC_PT1.cbc_pt1.CBC_PT1FT_WPR_NOT_PRK
#define NETC_TX_CBC_PT1SelectSpdSwSts_bit	 CBC_PT1.cbc_pt1.CBC_PT1SelectSpdSwSts
#define NETC_TX_CBC_PT1HILL_DES_Rq_bit	 CBC_PT1.cbc_pt1.CBC_PT1HILL_DES_Rq
#define NETC_TX_CBC_PT1TRAC_PSDSts_bit	 CBC_PT1.cbc_pt1.CBC_PT1TRAC_PSDSts
#define NETC_TX_CBC_PT1ExternalTemperatureAD_Voltage_byte	 CBC_PT1.cbc_pt1.CBC_PT1ExternalTemperatureAD_Voltage
#define NETC_TX_CBC_PT1AcOutputCurrentSts_byte	 CBC_PT1.cbc_pt1.CBC_PT1AcOutputCurrentSts

#define NETC_TX_WAKE_C_BCMWakeRsn_bit	 WAKE_C_BCM.wake_c_bcm.WAKE_C_BCMWakeRsn
#define NETC_TX_WAKE_C_BCMMainWakeSts_bit	 WAKE_C_BCM.wake_c_bcm.WAKE_C_BCMMainWakeSts
#define NETC_TX_WAKE_C_BCMWakeCntr_byte	 WAKE_C_BCM.wake_c_bcm.WAKE_C_BCMWakeCntr
#define NETC_TX_WAKE_C_BCMNode21_bit	 WAKE_C_BCM.wake_c_bcm.WAKE_C_BCMNode21
#define NETC_TX_WAKE_C_BCMNode0_bit	 WAKE_C_BCM.wake_c_bcm.WAKE_C_BCMNode0
#define NETC_TX_WAKE_C_BCMNode1_bit	 WAKE_C_BCM.wake_c_bcm.WAKE_C_BCMNode1
#define NETC_TX_WAKE_C_BCMNode2_bit	 WAKE_C_BCM.wake_c_bcm.WAKE_C_BCMNode2
#define NETC_TX_WAKE_C_BCMNode4_bit	 WAKE_C_BCM.wake_c_bcm.WAKE_C_BCMNode4
#define NETC_TX_WAKE_C_BCMNode5_bit	 WAKE_C_BCM.wake_c_bcm.WAKE_C_BCMNode5

#define NETC_TX_CBC_PT2CmdIgnSts_bit	 CBC_PT2.cbc_pt2.CBC_PT2CmdIgnSts
#define NETC_TX_CBC_PT2StTypSts_bit	 CBC_PT2.cbc_pt2.CBC_PT2StTypSts
#define NETC_TX_CBC_PT2RemStActvSts_bit	 CBC_PT2.cbc_pt2.CBC_PT2RemStActvSts
#define NETC_TX_CBC_PT2ACC_DLY_ACTSts_bit	 CBC_PT2.cbc_pt2.CBC_PT2ACC_DLY_ACTSts
#define NETC_TX_CBC_PT2KeyInIgnSts_bit	 CBC_PT2.cbc_pt2.CBC_PT2KeyInIgnSts
#define NETC_TX_CBC_PT2RemSt_InhibitSts_bit	 CBC_PT2.cbc_pt2.CBC_PT2RemSt_InhibitSts
#define NETC_TX_CBC_PT2PanicModeActive_bit	 CBC_PT2.cbc_pt2.CBC_PT2PanicModeActive
#define NETC_TX_CBC_PT2StartRelayBCMFault_bit	 CBC_PT2.cbc_pt2.CBC_PT2StartRelayBCMFault
#define NETC_TX_CBC_PT2StartRelayBCMSts_bit	 CBC_PT2.cbc_pt2.CBC_PT2StartRelayBCMSts
#define NETC_TX_CBC_PT2FOBSearchRequest_bit	 CBC_PT2.cbc_pt2.CBC_PT2FOBSearchRequest
#define NETC_TX_CBC_PT2MessageCounter_bit	 CBC_PT2.cbc_pt2.CBC_PT2MessageCounter
#define NETC_TX_CBC_PT2CRC_byte	 CBC_PT2.cbc_pt2.CBC_PT2CRC
#define NETC_TX_CAN_CBC_PT2CmdIgnSts_bit	 RDSTx_0.cbc_pt2.CBC_PT2CmdIgnSts
#define NETC_TX_CAN_CBC_PT2StTypSts_bit	 RDSTx_0.cbc_pt2.CBC_PT2StTypSts
#define NETC_TX_CAN_CBC_PT2RemStActvSts_bit	 RDSTx_0.cbc_pt2.CBC_PT2RemStActvSts
#define NETC_TX_CAN_CBC_PT2ACC_DLY_ACTSts_bit	 RDSTx_0.cbc_pt2.CBC_PT2ACC_DLY_ACTSts
#define NETC_TX_CAN_CBC_PT2KeyInIgnSts_bit	 RDSTx_0.cbc_pt2.CBC_PT2KeyInIgnSts
#define NETC_TX_CAN_CBC_PT2RemSt_InhibitSts_bit	 RDSTx_0.cbc_pt2.CBC_PT2RemSt_InhibitSts
#define NETC_TX_CAN_CBC_PT2PanicModeActive_bit	 RDSTx_0.cbc_pt2.CBC_PT2PanicModeActive
#define NETC_TX_CAN_CBC_PT2StartRelayBCMFault_bit	 RDSTx_0.cbc_pt2.CBC_PT2StartRelayBCMFault
#define NETC_TX_CAN_CBC_PT2StartRelayBCMSts_bit	 RDSTx_0.cbc_pt2.CBC_PT2StartRelayBCMSts
#define NETC_TX_CAN_CBC_PT2FOBSearchRequest_bit	 RDSTx_0.cbc_pt2.CBC_PT2FOBSearchRequest
#define NETC_TX_CAN_CBC_PT2MessageCounter_bit	 RDSTx_0.cbc_pt2.CBC_PT2MessageCounter
#define NETC_TX_CAN_CBC_PT2CRC_byte	 RDSTx_0.cbc_pt2.CBC_PT2CRC

#define NETC_TX_BCM_COMMANDBrakePedalSwitchNCFailSts_bit	 BCM_COMMAND.bcm_command.BCM_COMMANDBrakePedalSwitchNCFailSts
#define NETC_TX_BCM_COMMANDBrakePedalSwitchNOFailSts_bit	 BCM_COMMAND.bcm_command.BCM_COMMANDBrakePedalSwitchNOFailSts
#define NETC_TX_BCM_COMMANDTerrainSwStat_bit	 BCM_COMMAND.bcm_command.BCM_COMMANDTerrainSwStat
#define NETC_TX_BCM_COMMANDAWDNeutralSwRq_bit	 BCM_COMMAND.bcm_command.BCM_COMMANDAWDNeutralSwRq
#define NETC_TX_BCM_COMMANDAWDLowSwRq_bit	 BCM_COMMAND.bcm_command.BCM_COMMANDAWDLowSwRq
#define NETC_TX_BCM_COMMANDBrakePedalSwitchNCSts_bit	 BCM_COMMAND.bcm_command.BCM_COMMANDBrakePedalSwitchNCSts
#define NETC_TX_BCM_COMMANDBrakePedalSwitchNOSts_bit	 BCM_COMMAND.bcm_command.BCM_COMMANDBrakePedalSwitchNOSts
#define NETC_TX_BCM_COMMANDELockerSwRq_bit	 BCM_COMMAND.bcm_command.BCM_COMMANDELockerSwRq
#define NETC_TX_BCM_COMMANDMessageCounter_bit	 BCM_COMMAND.bcm_command.BCM_COMMANDMessageCounter
#define NETC_TX_BCM_COMMANDCRC_byte	 BCM_COMMAND.bcm_command.BCM_COMMANDCRC
#define NETC_TX_CAN_BCM_COMMANDBrakePedalSwitchNCFailSts_bit	 RDSTx_0.bcm_command.BCM_COMMANDBrakePedalSwitchNCFailSts
#define NETC_TX_CAN_BCM_COMMANDBrakePedalSwitchNOFailSts_bit	 RDSTx_0.bcm_command.BCM_COMMANDBrakePedalSwitchNOFailSts
#define NETC_TX_CAN_BCM_COMMANDTerrainSwStat_bit	 RDSTx_0.bcm_command.BCM_COMMANDTerrainSwStat
#define NETC_TX_CAN_BCM_COMMANDAWDNeutralSwRq_bit	 RDSTx_0.bcm_command.BCM_COMMANDAWDNeutralSwRq
#define NETC_TX_CAN_BCM_COMMANDAWDLowSwRq_bit	 RDSTx_0.bcm_command.BCM_COMMANDAWDLowSwRq
#define NETC_TX_CAN_BCM_COMMANDBrakePedalSwitchNCSts_bit	 RDSTx_0.bcm_command.BCM_COMMANDBrakePedalSwitchNCSts
#define NETC_TX_CAN_BCM_COMMANDBrakePedalSwitchNOSts_bit	 RDSTx_0.bcm_command.BCM_COMMANDBrakePedalSwitchNOSts
#define NETC_TX_CAN_BCM_COMMANDELockerSwRq_bit	 RDSTx_0.bcm_command.BCM_COMMANDELockerSwRq
#define NETC_TX_CAN_BCM_COMMANDMessageCounter_bit	 RDSTx_0.bcm_command.BCM_COMMANDMessageCounter
#define NETC_TX_CAN_BCM_COMMANDCRC_byte	 RDSTx_0.bcm_command.BCM_COMMANDCRC

#define NETC_TX_BCM_CODE_TRM_REQUESTControlEncoding_byte	 BCM_CODE_TRM_REQUEST.bcm_code_trm_request.BCM_CODE_TRM_REQUESTControlEncoding
#define NETC_TX_BCM_CODE_TRM_REQUESTMiniCryptFCode_bit_0 BCM_CODE_TRM_REQUEST.bcm_code_trm_request.BCM_CODE_TRM_REQUESTMiniCryptFCode_0
#define NETC_TX_BCM_CODE_TRM_REQUESTMiniCryptFCode_bit_1 BCM_CODE_TRM_REQUEST.bcm_code_trm_request.BCM_CODE_TRM_REQUESTMiniCryptFCode_1                                                                     
#define NETC_TX_BCM_CODE_TRM_REQUESTMiniCryptFCode_bit(c)    { NETC_TX_BCM_CODE_TRM_REQUESTMiniCryptFCode_bit_0 = ((vuint8)(((vuint16)(c) & 0xff)));\
                                        NETC_TX_BCM_CODE_TRM_REQUESTMiniCryptFCode_bit_1 = ((vuint8)(((vuint16)(c) & 0xff00) >> 8));\
                                      }
#define NETC_TX_BCM_CODE_TRM_REQUESTMiniCrypt_Rnd_bit_0 BCM_CODE_TRM_REQUEST.bcm_code_trm_request.BCM_CODE_TRM_REQUESTMiniCrypt_Rnd_0
#define NETC_TX_BCM_CODE_TRM_REQUESTMiniCrypt_Rnd_bit_1 BCM_CODE_TRM_REQUEST.bcm_code_trm_request.BCM_CODE_TRM_REQUESTMiniCrypt_Rnd_1
#define NETC_TX_BCM_CODE_TRM_REQUESTMiniCrypt_Rnd_bit_2 BCM_CODE_TRM_REQUEST.bcm_code_trm_request.BCM_CODE_TRM_REQUESTMiniCrypt_Rnd_2
#define NETC_TX_BCM_CODE_TRM_REQUESTMiniCrypt_Rnd_bit_3 BCM_CODE_TRM_REQUEST.bcm_code_trm_request.BCM_CODE_TRM_REQUESTMiniCrypt_Rnd_3                                                                     
#define NETC_TX_BCM_CODE_TRM_REQUESTMiniCrypt_Rnd_bit(c)    { NETC_TX_BCM_CODE_TRM_REQUESTMiniCrypt_Rnd_bit_0 = ((vuint8)(((vuint32)(c) & 0xff)));\
                                        NETC_TX_BCM_CODE_TRM_REQUESTMiniCrypt_Rnd_bit_1 = ((vuint8)(((vuint32)(c) & 0xff00) >> 8));\
                                        NETC_TX_BCM_CODE_TRM_REQUESTMiniCrypt_Rnd_bit_2 = ((vuint8)(((vuint32)(c) & 0xff0000) >> 16));\
                                        NETC_TX_BCM_CODE_TRM_REQUESTMiniCrypt_Rnd_bit_3 = ((vuint8)(((vuint32)(c) & 0xff000000) >> 24));\
                                      }
#define NETC_TX_BCM_CODE_TRM_REQUESTTxpReadRequest_bit	 BCM_CODE_TRM_REQUEST.bcm_code_trm_request.BCM_CODE_TRM_REQUESTTxpReadRequest
#define NETC_TX_BCM_CODE_TRM_REQUESTTxpAuthRequest_bit	 BCM_CODE_TRM_REQUEST.bcm_code_trm_request.BCM_CODE_TRM_REQUESTTxpAuthRequest

#define NETC_TX_BCM_CODE_ESL_REQUESTControlEncoding_byte	 BCM_CODE_ESL_REQUEST.bcm_code_esl_request.BCM_CODE_ESL_REQUESTControlEncoding
#define NETC_TX_BCM_CODE_ESL_REQUESTMiniCryptFCode_bit_0 BCM_CODE_ESL_REQUEST.bcm_code_esl_request.BCM_CODE_ESL_REQUESTMiniCryptFCode_0
#define NETC_TX_BCM_CODE_ESL_REQUESTMiniCryptFCode_bit_1 BCM_CODE_ESL_REQUEST.bcm_code_esl_request.BCM_CODE_ESL_REQUESTMiniCryptFCode_1                                                                     
#define NETC_TX_BCM_CODE_ESL_REQUESTMiniCryptFCode_bit(c)    { NETC_TX_BCM_CODE_ESL_REQUESTMiniCryptFCode_bit_0 = ((vuint8)(((vuint16)(c) & 0xff)));\
                                        NETC_TX_BCM_CODE_ESL_REQUESTMiniCryptFCode_bit_1 = ((vuint8)(((vuint16)(c) & 0xff00) >> 8));\
                                      }
#define NETC_TX_BCM_CODE_ESL_REQUESTMiniCrypt_Rnd_bit_0 BCM_CODE_ESL_REQUEST.bcm_code_esl_request.BCM_CODE_ESL_REQUESTMiniCrypt_Rnd_0
#define NETC_TX_BCM_CODE_ESL_REQUESTMiniCrypt_Rnd_bit_1 BCM_CODE_ESL_REQUEST.bcm_code_esl_request.BCM_CODE_ESL_REQUESTMiniCrypt_Rnd_1
#define NETC_TX_BCM_CODE_ESL_REQUESTMiniCrypt_Rnd_bit_2 BCM_CODE_ESL_REQUEST.bcm_code_esl_request.BCM_CODE_ESL_REQUESTMiniCrypt_Rnd_2
#define NETC_TX_BCM_CODE_ESL_REQUESTMiniCrypt_Rnd_bit_3 BCM_CODE_ESL_REQUEST.bcm_code_esl_request.BCM_CODE_ESL_REQUESTMiniCrypt_Rnd_3                                                                     
#define NETC_TX_BCM_CODE_ESL_REQUESTMiniCrypt_Rnd_bit(c)    { NETC_TX_BCM_CODE_ESL_REQUESTMiniCrypt_Rnd_bit_0 = ((vuint8)(((vuint32)(c) & 0xff)));\
                                        NETC_TX_BCM_CODE_ESL_REQUESTMiniCrypt_Rnd_bit_1 = ((vuint8)(((vuint32)(c) & 0xff00) >> 8));\
                                        NETC_TX_BCM_CODE_ESL_REQUESTMiniCrypt_Rnd_bit_2 = ((vuint8)(((vuint32)(c) & 0xff0000) >> 16));\
                                        NETC_TX_BCM_CODE_ESL_REQUESTMiniCrypt_Rnd_bit_3 = ((vuint8)(((vuint32)(c) & 0xff000000) >> 24));\
                                      }
#define NETC_TX_BCM_CODE_ESL_REQUESTESLLockUnlockReq_bit	 BCM_CODE_ESL_REQUEST.bcm_code_esl_request.BCM_CODE_ESL_REQUESTESLLockUnlockReq

#define NETC_TX_BCM_MINICRYPT_ACKMinicryptReceptionSts_bit	 BCM_MINICRYPT_ACK.bcm_minicrypt_ack.BCM_MINICRYPT_ACKMinicryptReceptionSts

#define NETC_TX_IMMO_CODE_RESPONSEControlEncoding_byte	 IMMO_CODE_RESPONSE.immo_code_response.IMMO_CODE_RESPONSEControlEncoding
#define NETC_TX_IMMO_CODE_RESPONSEMKKey1org21_byte	 IMMO_CODE_RESPONSE.immo_code_response.IMMO_CODE_RESPONSEMKKey1org21
#define NETC_TX_IMMO_CODE_RESPONSEMKKey2org22_byte	 IMMO_CODE_RESPONSE.immo_code_response.IMMO_CODE_RESPONSEMKKey2org22
#define NETC_TX_IMMO_CODE_RESPONSEMKKey_3_byte	 IMMO_CODE_RESPONSE.immo_code_response.IMMO_CODE_RESPONSEMKKey_3
#define NETC_TX_IMMO_CODE_RESPONSEMKKey_4_byte	 IMMO_CODE_RESPONSE.immo_code_response.IMMO_CODE_RESPONSEMKKey_4
#define NETC_TX_IMMO_CODE_RESPONSEMKKey_5_byte	 IMMO_CODE_RESPONSE.immo_code_response.IMMO_CODE_RESPONSEMKKey_5
#define NETC_TX_IMMO_CODE_RESPONSEMKKey_6_byte	 IMMO_CODE_RESPONSE.immo_code_response.IMMO_CODE_RESPONSEMKKey_6

#define NETC_TX_ECU_APPL_BCM_1ECU_APPL_BCM_bit_0 ECU_APPL_BCM_1.ecu_appl_bcm_1.ECU_APPL_BCM_1ECU_APPL_BCM_0
#define NETC_TX_ECU_APPL_BCM_1ECU_APPL_BCM_bit_1 ECU_APPL_BCM_1.ecu_appl_bcm_1.ECU_APPL_BCM_1ECU_APPL_BCM_1
#define NETC_TX_ECU_APPL_BCM_1ECU_APPL_BCM_bit_2 ECU_APPL_BCM_1.ecu_appl_bcm_1.ECU_APPL_BCM_1ECU_APPL_BCM_2
#define NETC_TX_ECU_APPL_BCM_1ECU_APPL_BCM_bit_3 ECU_APPL_BCM_1.ecu_appl_bcm_1.ECU_APPL_BCM_1ECU_APPL_BCM_3
#define NETC_TX_ECU_APPL_BCM_1ECU_APPL_BCM_bit_4 ECU_APPL_BCM_1.ecu_appl_bcm_1.ECU_APPL_BCM_1ECU_APPL_BCM_4
#define NETC_TX_ECU_APPL_BCM_1ECU_APPL_BCM_bit_5 ECU_APPL_BCM_1.ecu_appl_bcm_1.ECU_APPL_BCM_1ECU_APPL_BCM_5
#define NETC_TX_ECU_APPL_BCM_1ECU_APPL_BCM_bit_6 ECU_APPL_BCM_1.ecu_appl_bcm_1.ECU_APPL_BCM_1ECU_APPL_BCM_6
#define NETC_TX_ECU_APPL_BCM_1ECU_APPL_BCM_bit_7 ECU_APPL_BCM_1.ecu_appl_bcm_1.ECU_APPL_BCM_1ECU_APPL_BCM_7

#define NETC_TX_DTO_BCMData_bit_0	 DTO_BCM.dto_bcm.DTO_BCMData_0
#define NETC_TX_DTO_BCMData_bit_1	 DTO_BCM.dto_bcm.DTO_BCMData_1
#define NETC_TX_DTO_BCMData_bit_2	 DTO_BCM.dto_bcm.DTO_BCMData_2
#define NETC_TX_DTO_BCMData_bit_3	 DTO_BCM.dto_bcm.DTO_BCMData_3
#define NETC_TX_DTO_BCMData_bit_4	 DTO_BCM.dto_bcm.DTO_BCMData_4
#define NETC_TX_DTO_BCMData_bit_5	 DTO_BCM.dto_bcm.DTO_BCMData_5
#define NETC_TX_DTO_BCMData_bit_6	 DTO_BCM.dto_bcm.DTO_BCMData_6
#define NETC_TX_DTO_BCMData_bit_7	 DTO_BCM.dto_bcm.DTO_BCMData_7

#define NETC_TX_CONFIGURATION_DATA_CODE_REQUEST_1Digit_10_bit	 CONFIGURATION_DATA_CODE_REQUEST_1.configuration_data_code_request_1.CONFIGURATION_DATA_CODE_REQUEST_1Digit_10
#define NETC_TX_CONFIGURATION_DATA_CODE_REQUEST_1Digit_11_bit	 CONFIGURATION_DATA_CODE_REQUEST_1.configuration_data_code_request_1.CONFIGURATION_DATA_CODE_REQUEST_1Digit_11
#define NETC_TX_CONFIGURATION_DATA_CODE_REQUEST_1Digit_08_bit	 CONFIGURATION_DATA_CODE_REQUEST_1.configuration_data_code_request_1.CONFIGURATION_DATA_CODE_REQUEST_1Digit_08
#define NETC_TX_CONFIGURATION_DATA_CODE_REQUEST_1Digit_09_bit	 CONFIGURATION_DATA_CODE_REQUEST_1.configuration_data_code_request_1.CONFIGURATION_DATA_CODE_REQUEST_1Digit_09
#define NETC_TX_CONFIGURATION_DATA_CODE_REQUEST_1Digit_06_bit	 CONFIGURATION_DATA_CODE_REQUEST_1.configuration_data_code_request_1.CONFIGURATION_DATA_CODE_REQUEST_1Digit_06
#define NETC_TX_CONFIGURATION_DATA_CODE_REQUEST_1Digit_07_bit	 CONFIGURATION_DATA_CODE_REQUEST_1.configuration_data_code_request_1.CONFIGURATION_DATA_CODE_REQUEST_1Digit_07
#define NETC_TX_CONFIGURATION_DATA_CODE_REQUEST_1Digit_04_bit	 CONFIGURATION_DATA_CODE_REQUEST_1.configuration_data_code_request_1.CONFIGURATION_DATA_CODE_REQUEST_1Digit_04
#define NETC_TX_CONFIGURATION_DATA_CODE_REQUEST_1Digit_05_bit	 CONFIGURATION_DATA_CODE_REQUEST_1.configuration_data_code_request_1.CONFIGURATION_DATA_CODE_REQUEST_1Digit_05
#define NETC_TX_CONFIGURATION_DATA_CODE_REQUEST_1Digit_02_bit	 CONFIGURATION_DATA_CODE_REQUEST_1.configuration_data_code_request_1.CONFIGURATION_DATA_CODE_REQUEST_1Digit_02
#define NETC_TX_CONFIGURATION_DATA_CODE_REQUEST_1Digit_03_bit	 CONFIGURATION_DATA_CODE_REQUEST_1.configuration_data_code_request_1.CONFIGURATION_DATA_CODE_REQUEST_1Digit_03
#define NETC_TX_CONFIGURATION_DATA_CODE_REQUEST_1Digit_01_bit	 CONFIGURATION_DATA_CODE_REQUEST_1.configuration_data_code_request_1.CONFIGURATION_DATA_CODE_REQUEST_1Digit_01

#define NETC_TX_IBS_2PN14_LS_Lvl5_bit	 IBS_2.ibs_2.IBS_2PN14_LS_Lvl5
#define NETC_TX_IBS_2PN14_LS_Lvl4_bit	 IBS_2.ibs_2.IBS_2PN14_LS_Lvl4
#define NETC_TX_IBS_2PN14_LS_Lvl3_bit	 IBS_2.ibs_2.IBS_2PN14_LS_Lvl3
#define NETC_TX_IBS_2PN14_LS_Lvl2_bit	 IBS_2.ibs_2.IBS_2PN14_LS_Lvl2
#define NETC_TX_IBS_2PN14_LS_Lvl1_bit	 IBS_2.ibs_2.IBS_2PN14_LS_Lvl1
#define NETC_TX_IBS_2PN14_Key_Off_Load_Shed_bit	 IBS_2.ibs_2.IBS_2PN14_Key_Off_Load_Shed
#define NETC_TX_IBS_2PN14_LS_Actv_bit	 IBS_2.ibs_2.IBS_2PN14_LS_Actv
#define NETC_TX_IBS_2Batt_ST_Crit_bit	 IBS_2.ibs_2.IBS_2Batt_ST_Crit
#define NETC_TX_IBS_2PN14_LS_Lvl7_bit	 IBS_2.ibs_2.IBS_2PN14_LS_Lvl7
#define NETC_TX_IBS_2PN14_LS_Lvl6_bit	 IBS_2.ibs_2.IBS_2PN14_LS_Lvl6

#define NETC_TX_HUMIDITY_1000LGLASSTEMP_bit_0 HUMIDITY_1000L.humidity_1000l.HUMIDITY_1000LGLASSTEMP_0
#define NETC_TX_HUMIDITY_1000LGLASSTEMP_bit_1 HUMIDITY_1000L.humidity_1000l.HUMIDITY_1000LGLASSTEMP_1                                                                     
#define NETC_TX_HUMIDITY_1000LGLASSTEMP_bit(c)    { NETC_TX_HUMIDITY_1000LGLASSTEMP_bit_0 = ((vuint8)(((vuint16)(c) & 0x7)));\
                                        NETC_TX_HUMIDITY_1000LGLASSTEMP_bit_1 = ((vuint8)(((vuint16)(c) & 0x7f8) >> 3));\
                                      }
#define NETC_TX_HUMIDITY_1000LHumidityAirTmp_bit_0 HUMIDITY_1000L.humidity_1000l.HUMIDITY_1000LHumidityAirTmp_0
#define NETC_TX_HUMIDITY_1000LHumidityAirTmp_bit_1 HUMIDITY_1000L.humidity_1000l.HUMIDITY_1000LHumidityAirTmp_1                                                                     
#define NETC_TX_HUMIDITY_1000LHumidityAirTmp_bit(c)    { NETC_TX_HUMIDITY_1000LHumidityAirTmp_bit_0 = ((vuint8)(((vuint16)(c) & 0x3f)));\
                                        NETC_TX_HUMIDITY_1000LHumidityAirTmp_bit_1 = ((vuint8)(((vuint16)(c) & 0x3fc0) >> 6));\
                                      }
#define NETC_TX_HUMIDITY_1000LDewpointTmp_bit_0 HUMIDITY_1000L.humidity_1000l.HUMIDITY_1000LDewpointTmp_0
#define NETC_TX_HUMIDITY_1000LDewpointTmp_bit_1 HUMIDITY_1000L.humidity_1000l.HUMIDITY_1000LDewpointTmp_1
#define NETC_TX_HUMIDITY_1000LDewpointTmp_bit_2 HUMIDITY_1000L.humidity_1000l.HUMIDITY_1000LDewpointTmp_2                                                                     
#define NETC_TX_HUMIDITY_1000LDewpointTmp_bit(c)    { NETC_TX_HUMIDITY_1000LDewpointTmp_bit_0 = ((vuint8)(((vuint32)(c) & 0x1)));\
                                        NETC_TX_HUMIDITY_1000LDewpointTmp_bit_1 = ((vuint8)(((vuint32)(c) & 0x1fe) >> 1));\
                                        NETC_TX_HUMIDITY_1000LDewpointTmp_bit_2 = ((vuint8)(((vuint32)(c) & 0x1fe00) >> 9));\
                                      }
#define NETC_TX_HUMIDITY_1000LRELHUMIDITY_byte_0 HUMIDITY_1000L.humidity_1000l.HUMIDITY_1000LRELHUMIDITY_0
#define NETC_TX_HUMIDITY_1000LRELHUMIDITY_byte_1 HUMIDITY_1000L.humidity_1000l.HUMIDITY_1000LRELHUMIDITY_1                                                                     
#define NETC_TX_HUMIDITY_1000LRELHUMIDITY_byte(c)    { NETC_TX_HUMIDITY_1000LRELHUMIDITY_byte_0 = ((vuint8)(((vuint16)(c) & 0x1)));\
                                        NETC_TX_HUMIDITY_1000LRELHUMIDITY_byte_1 = ((vuint8)(((vuint16)(c) & 0x1fe) >> 1));\
                                      }

#define NETC_TX_COMPASS_A1CMP_DIR_bit	 COMPASS_A1.compass_a1.COMPASS_A1CMP_DIR
#define NETC_TX_COMPASS_A1VAR_VAL_bit	 COMPASS_A1.compass_a1.COMPASS_A1VAR_VAL
#define NETC_TX_COMPASS_A1CMP_DIR_RAW_bit_0 COMPASS_A1.compass_a1.COMPASS_A1CMP_DIR_RAW_0
#define NETC_TX_COMPASS_A1CMP_DIR_RAW_bit_1 COMPASS_A1.compass_a1.COMPASS_A1CMP_DIR_RAW_1                                                                     
#define NETC_TX_COMPASS_A1CMP_DIR_RAW_bit(c)    { NETC_TX_COMPASS_A1CMP_DIR_RAW_bit_0 = ((vuint8)(((vuint16)(c) & 0xff)));\
                                        NETC_TX_COMPASS_A1CMP_DIR_RAW_bit_1 = ((vuint8)(((vuint16)(c) & 0xff00) >> 8));\
                                      }
#define NETC_TX_COMPASS_A1IN_CAL_MD_bit	 COMPASS_A1.compass_a1.COMPASS_A1IN_CAL_MD

#define NETC_TX_CBC_I5WPR_SYS_FLT_bit	 CBC_I5.cbc_i5.CBC_I5WPR_SYS_FLT
#define NETC_TX_CBC_I5WPR_SYS_WASH_bit	 CBC_I5.cbc_i5.CBC_I5WPR_SYS_WASH
#define NETC_TX_CBC_I5WPR_SYS_MIST_bit	 CBC_I5.cbc_i5.CBC_I5WPR_SYS_MIST
#define NETC_TX_CBC_I5WPR_SYS_AUTO_bit	 CBC_I5.cbc_i5.CBC_I5WPR_SYS_AUTO
#define NETC_TX_CBC_I5WPR_SYS_INT_bit	 CBC_I5.cbc_i5.CBC_I5WPR_SYS_INT
#define NETC_TX_CBC_I5WPR_SYS_HIGH_bit	 CBC_I5.cbc_i5.CBC_I5WPR_SYS_HIGH
#define NETC_TX_CBC_I5WPR_SYS_LOW_bit	 CBC_I5.cbc_i5.CBC_I5WPR_SYS_LOW
#define NETC_TX_CBC_I5WPR_SYS_OFF_bit	 CBC_I5.cbc_i5.CBC_I5WPR_SYS_OFF
#define NETC_TX_CBC_I5IGN_OFF_TIME_LNG_bit_0 CBC_I5.cbc_i5.CBC_I5IGN_OFF_TIME_LNG_0
#define NETC_TX_CBC_I5IGN_OFF_TIME_LNG_bit_1 CBC_I5.cbc_i5.CBC_I5IGN_OFF_TIME_LNG_1                                                                     
#define NETC_TX_CBC_I5IGN_OFF_TIME_LNG_bit(c)    { NETC_TX_CBC_I5IGN_OFF_TIME_LNG_bit_0 = ((vuint8)(((vuint16)(c) & 0xf)));\
                                        NETC_TX_CBC_I5IGN_OFF_TIME_LNG_bit_1 = ((vuint8)(((vuint16)(c) & 0xff0) >> 4));\
                                      }

#define NETC_TX_BCM_KEYON_COUNTER_B_CANKeyOnCounter_bit_0 BCM_KEYON_COUNTER_C_CAN.bcm_keyon_counter_b_can.BCM_KEYON_COUNTER_B_CANKeyOnCounter_0
#define NETC_TX_BCM_KEYON_COUNTER_B_CANKeyOnCounter_bit_1 BCM_KEYON_COUNTER_C_CAN.bcm_keyon_counter_b_can.BCM_KEYON_COUNTER_B_CANKeyOnCounter_1                                                                     
#define NETC_TX_BCM_KEYON_COUNTER_B_CANKeyOnCounter_bit(c)    { NETC_TX_BCM_KEYON_COUNTER_B_CANKeyOnCounter_bit_0 = ((vuint8)(((vuint16)(c) & 0xff)));\
                                        NETC_TX_BCM_KEYON_COUNTER_B_CANKeyOnCounter_bit_1 = ((vuint8)(((vuint16)(c) & 0xff00) >> 8));\
                                      }

#define NETC_TX_AMB_TEMP_DISPAMB_TEMP_AVG_F_byte	 AMB_TEMP_DISP.amb_temp_disp.AMB_TEMP_DISPAMB_TEMP_AVG_F
#define NETC_TX_AMB_TEMP_DISPAMB_TEMP_AVG_C_byte	 AMB_TEMP_DISP.amb_temp_disp.AMB_TEMP_DISPAMB_TEMP_AVG_C

#define NETC_TX_WCPM_STATUSForeign_Object_bit	 WCPM_STATUS.wcpm_status.WCPM_STATUSForeign_Object
#define NETC_TX_WCPM_STATUSWCPM_Error_bit	 WCPM_STATUS.wcpm_status.WCPM_STATUSWCPM_Error
#define NETC_TX_WCPM_STATUSDevice_Battery_Status_bit	 WCPM_STATUS.wcpm_status.WCPM_STATUSDevice_Battery_Status
#define NETC_TX_WCPM_STATUSDevice_Docked_bit	 WCPM_STATUS.wcpm_status.WCPM_STATUSDevice_Docked

#define NETC_TX_GE_BLwsAngleSts_bit_0 GE_B.ge_b.GE_BLwsAngleSts_0
#define NETC_TX_GE_BLwsAngleSts_bit_1 GE_B.ge_b.GE_BLwsAngleSts_1                                                                     
#define NETC_TX_GE_BLwsAngleSts_bit(c)    { NETC_TX_GE_BLwsAngleSts_bit_0 = ((vuint8)(((vuint16)(c) & 0xff)));\
                                        NETC_TX_GE_BLwsAngleSts_bit_1 = ((vuint8)(((vuint16)(c) & 0xff00) >> 8));\
                                      }
#define NETC_TX_GE_BLwsAngleValidDataSts_bit	 GE_B.ge_b.GE_BLwsAngleValidDataSts
#define NETC_TX_GE_BLwsFailSts_bit	 GE_B.ge_b.GE_BLwsFailSts

#define NETC_TX_ENVIRONMENTAL_CONDITIONSExternalTemperature_byte	 ENVIRONMENTAL_CONDITIONS.environmental_conditions.ENVIRONMENTAL_CONDITIONSExternalTemperature
#define NETC_TX_ENVIRONMENTAL_CONDITIONSBatteryVoltageLevel_bit	 ENVIRONMENTAL_CONDITIONS.environmental_conditions.ENVIRONMENTAL_CONDITIONSBatteryVoltageLevel
#define NETC_TX_ENVIRONMENTAL_CONDITIONSExternalTemperatureFailSts_bit	 ENVIRONMENTAL_CONDITIONS.environmental_conditions.ENVIRONMENTAL_CONDITIONSExternalTemperatureFailSts
#define NETC_TX_ENVIRONMENTAL_CONDITIONSAtmosphericPressure_byte	 ENVIRONMENTAL_CONDITIONS.environmental_conditions.ENVIRONMENTAL_CONDITIONSAtmosphericPressure

#define NETC_TX_CBC_VTA_1TheftAlarmStatus_bit	 CBC_VTA_1.cbc_vta_1.CBC_VTA_1TheftAlarmStatus

#define NETC_TX_DIAGNOSTIC_RESPONSE_BCM_1N_PDU_bit_0 DIAGNOSTIC_RESPONSE_BCM_1.diagnostic_response_bcm_1.DIAGNOSTIC_RESPONSE_BCM_1N_PDU_0
#define NETC_TX_DIAGNOSTIC_RESPONSE_BCM_1N_PDU_bit_1 DIAGNOSTIC_RESPONSE_BCM_1.diagnostic_response_bcm_1.DIAGNOSTIC_RESPONSE_BCM_1N_PDU_1
#define NETC_TX_DIAGNOSTIC_RESPONSE_BCM_1N_PDU_bit_2 DIAGNOSTIC_RESPONSE_BCM_1.diagnostic_response_bcm_1.DIAGNOSTIC_RESPONSE_BCM_1N_PDU_2
#define NETC_TX_DIAGNOSTIC_RESPONSE_BCM_1N_PDU_bit_3 DIAGNOSTIC_RESPONSE_BCM_1.diagnostic_response_bcm_1.DIAGNOSTIC_RESPONSE_BCM_1N_PDU_3
#define NETC_TX_DIAGNOSTIC_RESPONSE_BCM_1N_PDU_bit_4 DIAGNOSTIC_RESPONSE_BCM_1.diagnostic_response_bcm_1.DIAGNOSTIC_RESPONSE_BCM_1N_PDU_4
#define NETC_TX_DIAGNOSTIC_RESPONSE_BCM_1N_PDU_bit_5 DIAGNOSTIC_RESPONSE_BCM_1.diagnostic_response_bcm_1.DIAGNOSTIC_RESPONSE_BCM_1N_PDU_5
#define NETC_TX_DIAGNOSTIC_RESPONSE_BCM_1N_PDU_bit_6 DIAGNOSTIC_RESPONSE_BCM_1.diagnostic_response_bcm_1.DIAGNOSTIC_RESPONSE_BCM_1N_PDU_6
#define NETC_TX_DIAGNOSTIC_RESPONSE_BCM_1N_PDU_bit_7 DIAGNOSTIC_RESPONSE_BCM_1.diagnostic_response_bcm_1.DIAGNOSTIC_RESPONSE_BCM_1N_PDU_7

#define NETC_TX_GW_C_I2CsoPEEnableDisableSts_bit	 GW_C_I2.gw_c_i2.GW_C_I2CsoPEEnableDisableSts
#define NETC_TX_GW_C_I2DynamicGridSts_bit	 GW_C_I2.gw_c_i2.GW_C_I2DynamicGridSts
#define NETC_TX_GW_C_I2EPBHoldSts_bit	 GW_C_I2.gw_c_i2.GW_C_I2EPBHoldSts
#define NETC_TX_GW_C_I2SpStSwStat_bit	 GW_C_I2.gw_c_i2.GW_C_I2SpStSwStat
#define NETC_TX_GW_C_I2ESP_DSBL_bit	 GW_C_I2.gw_c_i2.GW_C_I2ESP_DSBL
#define NETC_TX_GW_C_I2StaticGridSts_bit	 GW_C_I2.gw_c_i2.GW_C_I2StaticGridSts
#define NETC_TX_GW_C_I2ZoomSts_bit	 GW_C_I2.gw_c_i2.GW_C_I2ZoomSts
#define NETC_TX_GW_C_I2E_Mode_Sts_bit	 GW_C_I2.gw_c_i2.GW_C_I2E_Mode_Sts
#define NETC_TX_GW_C_I2DRLEnable_bit	 GW_C_I2.gw_c_i2.GW_C_I2DRLEnable

#define NETC_TX_DAS_BDAS_RF_CHIME_RQSts_bit	 DAS_B.das_b.DAS_BDAS_RF_CHIME_RQSts
#define NETC_TX_DAS_BDAS_LF_CHIME_RQSts_bit	 DAS_B.das_b.DAS_BDAS_LF_CHIME_RQSts
#define NETC_TX_DAS_BDAS_CHIME_TYPSts_bit	 DAS_B.das_b.DAS_BDAS_CHIME_TYPSts
#define NETC_TX_DAS_BFCW_Brk_bit	 DAS_B.das_b.DAS_BFCW_Brk
#define NETC_TX_DAS_BFCW_Setting_bit	 DAS_B.das_b.DAS_BFCW_Setting

#define NETC_TX_CBC_I2PanelIntensitySts_byte	 CBC_I2.cbc_i2.CBC_I2PanelIntensitySts
#define NETC_TX_CBC_I2ACC_DLY_ACTSts_bit	 CBC_I2.cbc_i2.CBC_I2ACC_DLY_ACTSts
#define NETC_TX_CBC_I2SOUND_RSSts_bit	 CBC_I2.cbc_i2.CBC_I2SOUND_RSSts
#define NETC_TX_CBC_I2CFG_AUTO_WPRSts_bit	 CBC_I2.cbc_i2.CBC_I2CFG_AUTO_WPRSts
#define NETC_TX_CBC_I2AutoVehHoldSts_bit	 CBC_I2.cbc_i2.CBC_I2AutoVehHoldSts
#define NETC_TX_CBC_I2HL_WPRSts_bit	 CBC_I2.cbc_i2.CBC_I2HL_WPRSts
#define NETC_TX_CBC_I2FLASH_LKSts_bit	 CBC_I2.cbc_i2.CBC_I2FLASH_LKSts
#define NETC_TX_CBC_I2ACC_DLY_TIMESts_bit_0 CBC_I2.cbc_i2.CBC_I2ACC_DLY_TIMESts_0
#define NETC_TX_CBC_I2ACC_DLY_TIMESts_bit_1 CBC_I2.cbc_i2.CBC_I2ACC_DLY_TIMESts_1                                                                     
#define NETC_TX_CBC_I2ACC_DLY_TIMESts_bit(c)    { NETC_TX_CBC_I2ACC_DLY_TIMESts_bit_0 = ((vuint8)(((vuint16)(c) & 0xff)));\
                                        NETC_TX_CBC_I2ACC_DLY_TIMESts_bit_1 = ((vuint8)(((vuint16)(c) & 0xff00) >> 8));\
                                      }
#define NETC_TX_CBC_I2ILL_APRCH_TIMESts_byte	 CBC_I2.cbc_i2.CBC_I2ILL_APRCH_TIMESts
#define NETC_TX_CBC_I2HL_DLY_TIMESts_byte	 CBC_I2.cbc_i2.CBC_I2HL_DLY_TIMESts
#define NETC_TX_CBC_I2AHB_ACTSts_bit	 CBC_I2.cbc_i2.CBC_I2AHB_ACTSts
#define NETC_TX_CBC_I2AHB_ENBLSts_bit	 CBC_I2.cbc_i2.CBC_I2AHB_ENBLSts
#define NETC_TX_CBC_I2PanicModeActive_bit	 CBC_I2.cbc_i2.CBC_I2PanicModeActive
#define NETC_TX_CBC_I2CsoAutoLockEnableDisableSts_bit	 CBC_I2.cbc_i2.CBC_I2CsoAutoLockEnableDisableSts
#define NETC_TX_CBC_I2CsoDriverDoorFirstEnableDisableS_bit	 CBC_I2.cbc_i2.CBC_I2CsoDriverDoorFirstEnableDisableS
#define NETC_TX_CBC_I2CsoAutoUnlockEnableDisableSts_bit	 CBC_I2.cbc_i2.CBC_I2CsoAutoUnlockEnableDisableSts
#define NETC_TX_CBC_I2CsoAudibleAlertEnableDisableSts_bit	 CBC_I2.cbc_i2.CBC_I2CsoAudibleAlertEnableDisableSts

#define NETC_TX_CBC_I1NotFilteredExternalTempFailSts_bit	 CBC_I1.cbc_i1.CBC_I1NotFilteredExternalTempFailSts
#define NETC_TX_CBC_I1U_CALL_PSD_bit	 CBC_I1.cbc_i1.CBC_I1U_CALL_PSD
#define NETC_TX_CBC_I1E_CALL_PSD_bit	 CBC_I1.cbc_i1.CBC_I1E_CALL_PSD
#define NETC_TX_CBC_I1NotFilteredExternalTempSts_byte	 CBC_I1.cbc_i1.CBC_I1NotFilteredExternalTempSts
#define NETC_TX_CBC_I1IMPACTConfirm_bit	 CBC_I1.cbc_i1.CBC_I1IMPACTConfirm
#define NETC_TX_CBC_I1IMPACTCommand_bit	 CBC_I1.cbc_i1.CBC_I1IMPACTCommand
#define NETC_TX_CBC_I1EC_MirrStat_CRV_bit	 CBC_I1.cbc_i1.CBC_I1EC_MirrStat_CRV

#define NETC_TX_STATUS_B_EPBAutoParkSts_bit	 STATUS_C_EPB.status_b_epb.STATUS_B_EPBAutoParkSts
#define NETC_TX_STATUS_B_EPBEPBChimeReq_bit	 STATUS_C_EPB.status_b_epb.STATUS_B_EPBEPBChimeReq
#define NETC_TX_STATUS_B_EPBServiceModeSts_bit	 STATUS_C_EPB.status_b_epb.STATUS_B_EPBServiceModeSts
#define NETC_TX_STATUS_B_EPBTextDisplay_bit	 STATUS_C_EPB.status_b_epb.STATUS_B_EPBTextDisplay
#define NETC_TX_STATUS_B_EPBEPBWarningLampReq_bit	 STATUS_C_EPB.status_b_epb.STATUS_B_EPBEPBWarningLampReq

#define NETC_TX_STATUS_C_CANEngineSts_bit	 STATUS_C_CAN.status_c_can.STATUS_C_CANEngineSts

#define NETC_TX_STATUS_B_HALFConfigSts_bit	 STATUS_C_HALF.status_b_half.STATUS_B_HALFConfigSts
#define NETC_TX_STATUS_B_HALFTorqueIntensitySts_bit	 STATUS_C_HALF.status_b_half.STATUS_B_HALFTorqueIntensitySts

#define NETC_TX_HALF_B_Warning_RQHALF_LF_CHIME_RQSts_bit	 HALF_C_Warning_RQ.half_b_warning_rq.HALF_B_Warning_RQHALF_LF_CHIME_RQSts
#define NETC_TX_HALF_B_Warning_RQHALF_RF_CHIME_RQSts_bit	 HALF_C_Warning_RQ.half_b_warning_rq.HALF_B_Warning_RQHALF_RF_CHIME_RQSts
#define NETC_TX_HALF_B_Warning_RQHALF_CHIME_TYPESts_bit	 HALF_C_Warning_RQ.half_b_warning_rq.HALF_B_Warning_RQHALF_CHIME_TYPESts
#define NETC_TX_HALF_B_Warning_RQHALF_CHIME_REP_RATESts_bit	 HALF_C_Warning_RQ.half_b_warning_rq.HALF_B_Warning_RQHALF_CHIME_REP_RATESts

#define NETC_TX_SWS_8StW_TempSts_byte_0 SWS_8.sws_8.SWS_8StW_TempSts_0
#define NETC_TX_SWS_8StW_TempSts_byte_1 SWS_8.sws_8.SWS_8StW_TempSts_1                                                                     
#define NETC_TX_SWS_8StW_TempSts_byte(c)    { NETC_TX_SWS_8StW_TempSts_byte_0 = ((vuint8)(((vuint16)(c) & 0x1f)));\
                                        NETC_TX_SWS_8StW_TempSts_byte_1 = ((vuint8)(((vuint16)(c) & 0x1fe0) >> 5));\
                                      }
#define NETC_TX_SWS_8Command_15Sts_bit_0 SWS_8.sws_8.SWS_8Command_15Sts_0
#define NETC_TX_SWS_8Command_15Sts_bit_1 SWS_8.sws_8.SWS_8Command_15Sts_1                                                                     
#define NETC_TX_SWS_8Command_15Sts_bit(c)    { NETC_TX_SWS_8Command_15Sts_bit_0 = ((vuint8)(((vuint16)(c) & 0x1)));\
                                        NETC_TX_SWS_8Command_15Sts_bit_1 = ((vuint8)(((vuint16)(c) & 0x1fe) >> 1));\
                                      }
#define NETC_TX_SWS_8Command_11Sts_bit_0 SWS_8.sws_8.SWS_8Command_11Sts_0
#define NETC_TX_SWS_8Command_11Sts_bit_1 SWS_8.sws_8.SWS_8Command_11Sts_1                                                                     
#define NETC_TX_SWS_8Command_11Sts_bit(c)    { NETC_TX_SWS_8Command_11Sts_bit_0 = ((vuint8)(((vuint16)(c) & 0x1)));\
                                        NETC_TX_SWS_8Command_11Sts_bit_1 = ((vuint8)(((vuint16)(c) & 0x1fe) >> 1));\
                                      }
#define NETC_TX_SWS_8Command_12Sts_bit	 SWS_8.sws_8.SWS_8Command_12Sts
#define NETC_TX_SWS_8Command_13Sts_bit	 SWS_8.sws_8.SWS_8Command_13Sts
#define NETC_TX_SWS_8Command_14Sts_bit	 SWS_8.sws_8.SWS_8Command_14Sts
#define NETC_TX_SWS_8Command_07Sts_bit_0 SWS_8.sws_8.SWS_8Command_07Sts_0
#define NETC_TX_SWS_8Command_07Sts_bit_1 SWS_8.sws_8.SWS_8Command_07Sts_1                                                                     
#define NETC_TX_SWS_8Command_07Sts_bit(c)    { NETC_TX_SWS_8Command_07Sts_bit_0 = ((vuint8)(((vuint16)(c) & 0x1)));\
                                        NETC_TX_SWS_8Command_07Sts_bit_1 = ((vuint8)(((vuint16)(c) & 0x1fe) >> 1));\
                                      }
#define NETC_TX_SWS_8Command_08Sts_bit	 SWS_8.sws_8.SWS_8Command_08Sts
#define NETC_TX_SWS_8Command_09Sts_bit	 SWS_8.sws_8.SWS_8Command_09Sts
#define NETC_TX_SWS_8Command_10Sts_bit	 SWS_8.sws_8.SWS_8Command_10Sts
#define NETC_TX_SWS_8Command_03Sts_bit_0 SWS_8.sws_8.SWS_8Command_03Sts_0
#define NETC_TX_SWS_8Command_03Sts_bit_1 SWS_8.sws_8.SWS_8Command_03Sts_1                                                                     
#define NETC_TX_SWS_8Command_03Sts_bit(c)    { NETC_TX_SWS_8Command_03Sts_bit_0 = ((vuint8)(((vuint16)(c) & 0x1)));\
                                        NETC_TX_SWS_8Command_03Sts_bit_1 = ((vuint8)(((vuint16)(c) & 0x1fe) >> 1));\
                                      }
#define NETC_TX_SWS_8Command_04Sts_bit	 SWS_8.sws_8.SWS_8Command_04Sts
#define NETC_TX_SWS_8Command_05Sts_bit	 SWS_8.sws_8.SWS_8Command_05Sts
#define NETC_TX_SWS_8StW_TempSensSts_bit	 SWS_8.sws_8.SWS_8StW_TempSensSts
#define NETC_TX_SWS_8Command_01Sts_bit	 SWS_8.sws_8.SWS_8Command_01Sts
#define NETC_TX_SWS_8Command_02Sts_bit	 SWS_8.sws_8.SWS_8Command_02Sts

#define NETC_TX_StW_Actn_Rq_1LT_TURN_RQSts_bit	 StW_Actn_Rq_1.stw_actn_rq_1.StW_Actn_Rq_1LT_TURN_RQSts
#define NETC_TX_StW_Actn_Rq_1RT_TURN_RQSts_bit	 StW_Actn_Rq_1.stw_actn_rq_1.StW_Actn_Rq_1RT_TURN_RQSts

#define NETC_TX_PAM_BPAM_CFG_STATSts_bit	 PAM_B.pam_b.PAM_BPAM_CFG_STATSts
#define NETC_TX_PAM_BRR_PAM_StopControlSts_bit	 PAM_B.pam_b.PAM_BRR_PAM_StopControlSts
#define NETC_TX_PAM_BPAM_MUTE_RQSts_bit	 PAM_B.pam_b.PAM_BPAM_MUTE_RQSts
#define NETC_TX_PAM_BPAM_STATESts_bit	 PAM_B.pam_b.PAM_BPAM_STATESts
#define NETC_TX_PAM_BPAM_RR_STATESts_bit	 PAM_B.pam_b.PAM_BPAM_RR_STATESts
#define NETC_TX_PAM_BPAM_FT_STATESts_bit	 PAM_B.pam_b.PAM_BPAM_FT_STATESts
#define NETC_TX_PAM_BPAM_RF_CHIME_RQSts_bit	 PAM_B.pam_b.PAM_BPAM_RF_CHIME_RQSts
#define NETC_TX_PAM_BPAM_LF_CHIME_RQSts_bit	 PAM_B.pam_b.PAM_BPAM_LF_CHIME_RQSts
#define NETC_TX_PAM_BPAM_VOL_R_bit	 PAM_B.pam_b.PAM_BPAM_VOL_R
#define NETC_TX_PAM_BPAM_VOL_F_bit	 PAM_B.pam_b.PAM_BPAM_VOL_F
#define NETC_TX_PAM_BPAM_LR_CHIME_RQSts_bit	 PAM_B.pam_b.PAM_BPAM_LR_CHIME_RQSts
#define NETC_TX_PAM_BPAM_RR_CHIME_RQSts_bit	 PAM_B.pam_b.PAM_BPAM_RR_CHIME_RQSts
#define NETC_TX_PAM_BPAM_CHIME_REP_RATESts_bit	 PAM_B.pam_b.PAM_BPAM_CHIME_REP_RATESts
#define NETC_TX_PAM_BPAM_CHIME_TYPESts_bit	 PAM_B.pam_b.PAM_BPAM_CHIME_TYPESts

#define NETC_TX_GW_C_I3ACPressure_bit_0 GW_C_I3.gw_c_i3.GW_C_I3ACPressure_0
#define NETC_TX_GW_C_I3ACPressure_bit_1 GW_C_I3.gw_c_i3.GW_C_I3ACPressure_1                                                                     
#define NETC_TX_GW_C_I3ACPressure_bit(c)    { NETC_TX_GW_C_I3ACPressure_bit_0 = ((vuint8)(((vuint16)(c) & 0xff)));\
                                        NETC_TX_GW_C_I3ACPressure_bit_1 = ((vuint8)(((vuint16)(c) & 0xff00) >> 8));\
                                      }
#define NETC_TX_GW_C_I3ESS_ENG_ST_bit	 GW_C_I3.gw_c_i3.GW_C_I3ESS_ENG_ST

#define NETC_TX_VIN_1VIN_MSG_bit	 VIN_0.vin_1.VIN_1VIN_MSG
#define NETC_TX_VIN_1VIN_DATA_bit_0 VIN_0.vin_1.VIN_1VIN_DATA_0
#define NETC_TX_VIN_1VIN_DATA_bit_1 VIN_0.vin_1.VIN_1VIN_DATA_1
#define NETC_TX_VIN_1VIN_DATA_bit_2 VIN_0.vin_1.VIN_1VIN_DATA_2
#define NETC_TX_VIN_1VIN_DATA_bit_3 VIN_0.vin_1.VIN_1VIN_DATA_3
#define NETC_TX_VIN_1VIN_DATA_bit_4 VIN_0.vin_1.VIN_1VIN_DATA_4
#define NETC_TX_VIN_1VIN_DATA_bit_5 VIN_0.vin_1.VIN_1VIN_DATA_5
#define NETC_TX_VIN_1VIN_DATA_bit_6 VIN_0.vin_1.VIN_1VIN_DATA_6

#define NETC_TX_VEHICLE_SPEED_ODOMETER_1VehicleSpeed_bit_0 VEHICLE_SPEED_ODOMETER_1.vehicle_speed_odometer_1.VEHICLE_SPEED_ODOMETER_1VehicleSpeed_0
#define NETC_TX_VEHICLE_SPEED_ODOMETER_1VehicleSpeed_bit_1 VEHICLE_SPEED_ODOMETER_1.vehicle_speed_odometer_1.VEHICLE_SPEED_ODOMETER_1VehicleSpeed_1                                                                     
#define NETC_TX_VEHICLE_SPEED_ODOMETER_1VehicleSpeed_bit(c)    { NETC_TX_VEHICLE_SPEED_ODOMETER_1VehicleSpeed_bit_0 = ((vuint8)(((vuint16)(c) & 0xff)));\
                                        NETC_TX_VEHICLE_SPEED_ODOMETER_1VehicleSpeed_bit_1 = ((vuint8)(((vuint16)(c) & 0xff00) >> 8));\
                                      }
#define NETC_TX_VEHICLE_SPEED_ODOMETER_1VehicleSpeedFailSts_bit	 VEHICLE_SPEED_ODOMETER_1.vehicle_speed_odometer_1.VEHICLE_SPEED_ODOMETER_1VehicleSpeedFailSts
#define NETC_TX_VEHICLE_SPEED_ODOMETER_1TravelDistance_byte	 VEHICLE_SPEED_ODOMETER_1.vehicle_speed_odometer_1.VEHICLE_SPEED_ODOMETER_1TravelDistance

#define NETC_TX_GW_C_I6SpStSwStat_bit	 GW_C_I6.gw_c_i6.GW_C_I6SpStSwStat

#define NETC_TX_DYNAMIC_VEHICLE_INFO2LHRFastPulseCounter_byte_0 DYNAMIC_VEHICLE_INFO2.dynamic_vehicle_info2.DYNAMIC_VEHICLE_INFO2LHRFastPulseCounter_0
#define NETC_TX_DYNAMIC_VEHICLE_INFO2LHRFastPulseCounter_byte_1 DYNAMIC_VEHICLE_INFO2.dynamic_vehicle_info2.DYNAMIC_VEHICLE_INFO2LHRFastPulseCounter_1                                                                     
#define NETC_TX_DYNAMIC_VEHICLE_INFO2LHRFastPulseCounter_byte(c)    { NETC_TX_DYNAMIC_VEHICLE_INFO2LHRFastPulseCounter_byte_0 = ((vuint8)(((vuint16)(c) & 0x3)));\
                                        NETC_TX_DYNAMIC_VEHICLE_INFO2LHRFastPulseCounter_byte_1 = ((vuint8)(((vuint16)(c) & 0x3fc) >> 2));\
                                      }
#define NETC_TX_DYNAMIC_VEHICLE_INFO2RHRFastPulseCounterFailStS_bit	 DYNAMIC_VEHICLE_INFO2.dynamic_vehicle_info2.DYNAMIC_VEHICLE_INFO2RHRFastPulseCounterFailStS
#define NETC_TX_DYNAMIC_VEHICLE_INFO2LHRFastPulseCounterFailStS_bit	 DYNAMIC_VEHICLE_INFO2.dynamic_vehicle_info2.DYNAMIC_VEHICLE_INFO2LHRFastPulseCounterFailStS
#define NETC_TX_DYNAMIC_VEHICLE_INFO2RHRFastPulseCounter_byte_0 DYNAMIC_VEHICLE_INFO2.dynamic_vehicle_info2.DYNAMIC_VEHICLE_INFO2RHRFastPulseCounter_0
#define NETC_TX_DYNAMIC_VEHICLE_INFO2RHRFastPulseCounter_byte_1 DYNAMIC_VEHICLE_INFO2.dynamic_vehicle_info2.DYNAMIC_VEHICLE_INFO2RHRFastPulseCounter_1                                                                     
#define NETC_TX_DYNAMIC_VEHICLE_INFO2RHRFastPulseCounter_byte(c)    { NETC_TX_DYNAMIC_VEHICLE_INFO2RHRFastPulseCounter_byte_0 = ((vuint8)(((vuint16)(c) & 0x3)));\
                                        NETC_TX_DYNAMIC_VEHICLE_INFO2RHRFastPulseCounter_byte_1 = ((vuint8)(((vuint16)(c) & 0x3fc) >> 2));\
                                      }
#define NETC_TX_DYNAMIC_VEHICLE_INFO2LHFFastPulseCounter_byte_0 DYNAMIC_VEHICLE_INFO2.dynamic_vehicle_info2.DYNAMIC_VEHICLE_INFO2LHFFastPulseCounter_0
#define NETC_TX_DYNAMIC_VEHICLE_INFO2LHFFastPulseCounter_byte_1 DYNAMIC_VEHICLE_INFO2.dynamic_vehicle_info2.DYNAMIC_VEHICLE_INFO2LHFFastPulseCounter_1                                                                     
#define NETC_TX_DYNAMIC_VEHICLE_INFO2LHFFastPulseCounter_byte(c)    { NETC_TX_DYNAMIC_VEHICLE_INFO2LHFFastPulseCounter_byte_0 = ((vuint8)(((vuint16)(c) & 0x3)));\
                                        NETC_TX_DYNAMIC_VEHICLE_INFO2LHFFastPulseCounter_byte_1 = ((vuint8)(((vuint16)(c) & 0x3fc) >> 2));\
                                      }
#define NETC_TX_DYNAMIC_VEHICLE_INFO2RHFFastPulseCounter_byte_0 DYNAMIC_VEHICLE_INFO2.dynamic_vehicle_info2.DYNAMIC_VEHICLE_INFO2RHFFastPulseCounter_0
#define NETC_TX_DYNAMIC_VEHICLE_INFO2RHFFastPulseCounter_byte_1 DYNAMIC_VEHICLE_INFO2.dynamic_vehicle_info2.DYNAMIC_VEHICLE_INFO2RHFFastPulseCounter_1                                                                     
#define NETC_TX_DYNAMIC_VEHICLE_INFO2RHFFastPulseCounter_byte(c)    { NETC_TX_DYNAMIC_VEHICLE_INFO2RHFFastPulseCounter_byte_0 = ((vuint8)(((vuint16)(c) & 0x3)));\
                                        NETC_TX_DYNAMIC_VEHICLE_INFO2RHFFastPulseCounter_byte_1 = ((vuint8)(((vuint16)(c) & 0x3fc) >> 2));\
                                      }
#define NETC_TX_DYNAMIC_VEHICLE_INFO2LongAcceleration_bit_0 DYNAMIC_VEHICLE_INFO2.dynamic_vehicle_info2.DYNAMIC_VEHICLE_INFO2LongAcceleration_0
#define NETC_TX_DYNAMIC_VEHICLE_INFO2LongAcceleration_bit_1 DYNAMIC_VEHICLE_INFO2.dynamic_vehicle_info2.DYNAMIC_VEHICLE_INFO2LongAcceleration_1                                                                     
#define NETC_TX_DYNAMIC_VEHICLE_INFO2LongAcceleration_bit(c)    { NETC_TX_DYNAMIC_VEHICLE_INFO2LongAcceleration_bit_0 = ((vuint8)(((vuint16)(c) & 0xff)));\
                                        NETC_TX_DYNAMIC_VEHICLE_INFO2LongAcceleration_bit_1 = ((vuint8)(((vuint16)(c) & 0xff00) >> 8));\
                                      }
#define NETC_TX_DYNAMIC_VEHICLE_INFO2RHFFastPulseCounterFailStS_bit	 DYNAMIC_VEHICLE_INFO2.dynamic_vehicle_info2.DYNAMIC_VEHICLE_INFO2RHFFastPulseCounterFailStS
#define NETC_TX_DYNAMIC_VEHICLE_INFO2LHFFastPulseCounterFailStS_bit	 DYNAMIC_VEHICLE_INFO2.dynamic_vehicle_info2.DYNAMIC_VEHICLE_INFO2LHFFastPulseCounterFailStS
#define NETC_TX_DYNAMIC_VEHICLE_INFO2LongAcceleration_Offset_byte	 DYNAMIC_VEHICLE_INFO2.dynamic_vehicle_info2.DYNAMIC_VEHICLE_INFO2LongAcceleration_Offset

#define NETC_TX_STATUS_B_TRANSMISSIONShiftLeverPosition_bit	 STATUS_C_TRANSMISSION.status_b_transmission.STATUS_B_TRANSMISSIONShiftLeverPosition
#define NETC_TX_STATUS_B_TRANSMISSIONActualGearForDisplay_bit	 STATUS_C_TRANSMISSION.status_b_transmission.STATUS_B_TRANSMISSIONActualGearForDisplay

#define NETC_TX_NWM_BCMZero_byte_byte	 NWM_BCM.nwm_bcm.NWM_BCMZero_byte
#define NETC_TX_NWM_BCMSystemCommand_bit	 NWM_BCM.nwm_bcm.NWM_BCMSystemCommand
#define NETC_TX_NWM_BCMActiveLoadMaster_bit	 NWM_BCM.nwm_bcm.NWM_BCMActiveLoadMaster
#define NETC_TX_NWM_BCMEOL_bit	 NWM_BCM.nwm_bcm.NWM_BCMEOL
#define NETC_TX_NWM_BCMGenericFailSts_bit	 NWM_BCM.nwm_bcm.NWM_BCMGenericFailSts
#define NETC_TX_NWM_BCMP_ES_bit	 NWM_BCM.nwm_bcm.NWM_BCMP_ES
#define NETC_TX_NWM_BCMD_ES_bit	 NWM_BCM.nwm_bcm.NWM_BCMD_ES
#define NETC_TX_NWM_BCMNode24_bit	 NWM_BCM.nwm_bcm.NWM_BCMNode24
#define NETC_TX_NWM_BCMNode25_bit	 NWM_BCM.nwm_bcm.NWM_BCMNode25
#define NETC_TX_NWM_BCMNode26_bit	 NWM_BCM.nwm_bcm.NWM_BCMNode26
#define NETC_TX_NWM_BCMNode27_bit	 NWM_BCM.nwm_bcm.NWM_BCMNode27
#define NETC_TX_NWM_BCMNode28_bit	 NWM_BCM.nwm_bcm.NWM_BCMNode28
#define NETC_TX_NWM_BCMNode29_bit	 NWM_BCM.nwm_bcm.NWM_BCMNode29
#define NETC_TX_NWM_BCMNode30_bit	 NWM_BCM.nwm_bcm.NWM_BCMNode30
#define NETC_TX_NWM_BCMNode31_bit	 NWM_BCM.nwm_bcm.NWM_BCMNode31
#define NETC_TX_NWM_BCMNode16_bit	 NWM_BCM.nwm_bcm.NWM_BCMNode16
#define NETC_TX_NWM_BCMNode17_bit	 NWM_BCM.nwm_bcm.NWM_BCMNode17
#define NETC_TX_NWM_BCMNode18_bit	 NWM_BCM.nwm_bcm.NWM_BCMNode18
#define NETC_TX_NWM_BCMNode19_bit	 NWM_BCM.nwm_bcm.NWM_BCMNode19
#define NETC_TX_NWM_BCMNode20_bit	 NWM_BCM.nwm_bcm.NWM_BCMNode20
#define NETC_TX_NWM_BCMNode21_bit	 NWM_BCM.nwm_bcm.NWM_BCMNode21
#define NETC_TX_NWM_BCMNode22_bit	 NWM_BCM.nwm_bcm.NWM_BCMNode22
#define NETC_TX_NWM_BCMNode23_bit	 NWM_BCM.nwm_bcm.NWM_BCMNode23
#define NETC_TX_NWM_BCMNode8_bit	 NWM_BCM.nwm_bcm.NWM_BCMNode8
#define NETC_TX_NWM_BCMNode9_bit	 NWM_BCM.nwm_bcm.NWM_BCMNode9
#define NETC_TX_NWM_BCMNode10_bit	 NWM_BCM.nwm_bcm.NWM_BCMNode10
#define NETC_TX_NWM_BCMNode11_bit	 NWM_BCM.nwm_bcm.NWM_BCMNode11
#define NETC_TX_NWM_BCMNode12_bit	 NWM_BCM.nwm_bcm.NWM_BCMNode12
#define NETC_TX_NWM_BCMNode13_bit	 NWM_BCM.nwm_bcm.NWM_BCMNode13
#define NETC_TX_NWM_BCMNode14_bit	 NWM_BCM.nwm_bcm.NWM_BCMNode14
#define NETC_TX_NWM_BCMNode15_bit	 NWM_BCM.nwm_bcm.NWM_BCMNode15
#define NETC_TX_NWM_BCMNode0_bit	 NWM_BCM.nwm_bcm.NWM_BCMNode0
#define NETC_TX_NWM_BCMNode1_bit	 NWM_BCM.nwm_bcm.NWM_BCMNode1
#define NETC_TX_NWM_BCMNode2_bit	 NWM_BCM.nwm_bcm.NWM_BCMNode2
#define NETC_TX_NWM_BCMNode3_bit	 NWM_BCM.nwm_bcm.NWM_BCMNode3
#define NETC_TX_NWM_BCMNode4_bit	 NWM_BCM.nwm_bcm.NWM_BCMNode4
#define NETC_TX_NWM_BCMNode5_bit	 NWM_BCM.nwm_bcm.NWM_BCMNode5
#define NETC_TX_NWM_BCMNode6_bit	 NWM_BCM.nwm_bcm.NWM_BCMNode6
#define NETC_TX_NWM_BCMNode7_bit	 NWM_BCM.nwm_bcm.NWM_BCMNode7

#define NETC_TX_STATUS_BCM2PowerModeSts_bit	 STATUS_BCM2.status_bcm2.STATUS_BCM2PowerModeSts
#define NETC_TX_STATUS_BCM2BatteryLampSts_bit	 STATUS_BCM2.status_bcm2.STATUS_BCM2BatteryLampSts
#define NETC_TX_STATUS_BCM2NightAndDaySensorFailSts_bit	 STATUS_BCM2.status_bcm2.STATUS_BCM2NightAndDaySensorFailSts
#define NETC_TX_STATUS_BCM2WashLightWarningSts_bit	 STATUS_BCM2.status_bcm2.STATUS_BCM2WashLightWarningSts

#define NETC_TX_STATUS_BCMSysEOLSts_bit	 STATUS_BCM.status_bcm.STATUS_BCMSysEOLSts
#define NETC_TX_STATUS_BCMInternalLightSts_bit	 STATUS_BCM.status_bcm.STATUS_BCMInternalLightSts
#define NETC_TX_STATUS_BCMRearLockLastElSts_bit	 STATUS_BCM.status_bcm.STATUS_BCMRearLockLastElSts
#define NETC_TX_STATUS_BCMDriverDoorSts_bit	 STATUS_BCM.status_bcm.STATUS_BCMDriverDoorSts
#define NETC_TX_STATUS_BCMPsngrDoorSts_bit	 STATUS_BCM.status_bcm.STATUS_BCMPsngrDoorSts
#define NETC_TX_STATUS_BCMLHRDoorSts_bit	 STATUS_BCM.status_bcm.STATUS_BCMLHRDoorSts
#define NETC_TX_STATUS_BCMRHRDoorSts_bit	 STATUS_BCM.status_bcm.STATUS_BCMRHRDoorSts
#define NETC_TX_STATUS_BCMRHatchSts_bit	 STATUS_BCM.status_bcm.STATUS_BCMRHatchSts
#define NETC_TX_STATUS_BCMBonnetSts_bit	 STATUS_BCM.status_bcm.STATUS_BCMBonnetSts
#define NETC_TX_STATUS_BCMRechargeSts_bit	 STATUS_BCM.status_bcm.STATUS_BCMRechargeSts
#define NETC_TX_STATUS_BCMDoorLockLastElSts_bit_0 STATUS_BCM.status_bcm.STATUS_BCMDoorLockLastElSts_0
#define NETC_TX_STATUS_BCMDoorLockLastElSts_bit_1 STATUS_BCM.status_bcm.STATUS_BCMDoorLockLastElSts_1                                                                     
#define NETC_TX_STATUS_BCMDoorLockLastElSts_bit(c)    { NETC_TX_STATUS_BCMDoorLockLastElSts_bit_0 = ((vuint8)(((vuint16)(c) & 0x1)));\
                                        NETC_TX_STATUS_BCMDoorLockLastElSts_bit_1 = ((vuint8)(((vuint16)(c) & 0x1fe) >> 1));\
                                      }
#define NETC_TX_STATUS_BCMESLUnlockFailSts_bit	 STATUS_BCM.status_bcm.STATUS_BCMESLUnlockFailSts
#define NETC_TX_STATUS_BCMESLLockFailSts_bit	 STATUS_BCM.status_bcm.STATUS_BCMESLLockFailSts
#define NETC_TX_STATUS_BCMMoveWheelSts_bit	 STATUS_BCM.status_bcm.STATUS_BCMMoveWheelSts
#define NETC_TX_STATUS_BCMExteriorRearReleaseSwitchSts_bit	 STATUS_BCM.status_bcm.STATUS_BCMExteriorRearReleaseSwitchSts
#define NETC_TX_STATUS_BCMRainSensorFailSts_bit	 STATUS_BCM.status_bcm.STATUS_BCMRainSensorFailSts

#define NETC_TX_EXTERNAL_LIGHTS_1RearFogLightSts_bit	 EXTERNAL_LIGHTS_1.external_lights_1.EXTERNAL_LIGHTS_1RearFogLightSts
#define NETC_TX_EXTERNAL_LIGHTS_1FrontFogLightSts_bit	 EXTERNAL_LIGHTS_1.external_lights_1.EXTERNAL_LIGHTS_1FrontFogLightSts
#define NETC_TX_EXTERNAL_LIGHTS_1LowBeamSts_bit	 EXTERNAL_LIGHTS_1.external_lights_1.EXTERNAL_LIGHTS_1LowBeamSts
#define NETC_TX_EXTERNAL_LIGHTS_1HighBeamSts_bit	 EXTERNAL_LIGHTS_1.external_lights_1.EXTERNAL_LIGHTS_1HighBeamSts
#define NETC_TX_EXTERNAL_LIGHTS_1LHParkingLightSts_bit	 EXTERNAL_LIGHTS_1.external_lights_1.EXTERNAL_LIGHTS_1LHParkingLightSts
#define NETC_TX_EXTERNAL_LIGHTS_1RHParkingLightSts_bit	 EXTERNAL_LIGHTS_1.external_lights_1.EXTERNAL_LIGHTS_1RHParkingLightSts
#define NETC_TX_EXTERNAL_LIGHTS_1StopLightSts_bit	 EXTERNAL_LIGHTS_1.external_lights_1.EXTERNAL_LIGHTS_1StopLightSts
#define NETC_TX_EXTERNAL_LIGHTS_1RHeatedWindowSts_bit	 EXTERNAL_LIGHTS_1.external_lights_1.EXTERNAL_LIGHTS_1RHeatedWindowSts
#define NETC_TX_EXTERNAL_LIGHTS_1RHTurnSignalSts_bit	 EXTERNAL_LIGHTS_1.external_lights_1.EXTERNAL_LIGHTS_1RHTurnSignalSts
#define NETC_TX_EXTERNAL_LIGHTS_1LHTurnSignalSts_bit	 EXTERNAL_LIGHTS_1.external_lights_1.EXTERNAL_LIGHTS_1LHTurnSignalSts
#define NETC_TX_EXTERNAL_LIGHTS_1RearFogLightFault_bit	 EXTERNAL_LIGHTS_1.external_lights_1.EXTERNAL_LIGHTS_1RearFogLightFault
#define NETC_TX_EXTERNAL_LIGHTS_1LHTurnLightFault_bit	 EXTERNAL_LIGHTS_1.external_lights_1.EXTERNAL_LIGHTS_1LHTurnLightFault
#define NETC_TX_EXTERNAL_LIGHTS_1RHTurnLightFault_bit	 EXTERNAL_LIGHTS_1.external_lights_1.EXTERNAL_LIGHTS_1RHTurnLightFault
#define NETC_TX_EXTERNAL_LIGHTS_1HighBeamFault_bit	 EXTERNAL_LIGHTS_1.external_lights_1.EXTERNAL_LIGHTS_1HighBeamFault
#define NETC_TX_EXTERNAL_LIGHTS_1ReverseGearLightFault_bit	 EXTERNAL_LIGHTS_1.external_lights_1.EXTERNAL_LIGHTS_1ReverseGearLightFault
#define NETC_TX_EXTERNAL_LIGHTS_1PlateLightFault_bit	 EXTERNAL_LIGHTS_1.external_lights_1.EXTERNAL_LIGHTS_1PlateLightFault
#define NETC_TX_EXTERNAL_LIGHTS_1ParkingLightFault_bit	 EXTERNAL_LIGHTS_1.external_lights_1.EXTERNAL_LIGHTS_1ParkingLightFault
#define NETC_TX_EXTERNAL_LIGHTS_1LowBeamFault_bit	 EXTERNAL_LIGHTS_1.external_lights_1.EXTERNAL_LIGHTS_1LowBeamFault

#define NETC_TX_RFHUB_B_A2RFFuncReq_bit	 RFHUB_A2.rfhub_b_a2.RFHUB_B_A2RFFuncReq
#define NETC_TX_RFHUB_B_A2RFReq_bit	 RFHUB_A2.rfhub_b_a2.RFHUB_B_A2RFReq
#define NETC_TX_RFHUB_B_A2RFFobNum_bit	 RFHUB_A2.rfhub_b_a2.RFHUB_B_A2RFFobNum

#define NETC_TX_CBC_I4CmdIgnSts_bit	 CBC_I4.cbc_i4.CBC_I4CmdIgnSts
#define NETC_TX_CBC_I4StTypSts_bit	 CBC_I4.cbc_i4.CBC_I4StTypSts
#define NETC_TX_CBC_I4RemStActvSts_bit	 CBC_I4.cbc_i4.CBC_I4RemStActvSts
#define NETC_TX_CBC_I4KeyInIgnSts_bit	 CBC_I4.cbc_i4.CBC_I4KeyInIgnSts
#define NETC_TX_CBC_I4ShiftLeverPositionReqValidData_bit	 CBC_I4.cbc_i4.CBC_I4ShiftLeverPositionReqValidData
#define NETC_TX_CBC_I4ShiftLeverPositionReq_bit	 CBC_I4.cbc_i4.CBC_I4ShiftLeverPositionReq
#define NETC_TX_CBC_I4Court_Lmp_byte	 CBC_I4.cbc_i4.CBC_I4Court_Lmp
#define NETC_TX_CBC_I4Aprch_Lmp_byte	 CBC_I4.cbc_i4.CBC_I4Aprch_Lmp
#define NETC_TX_CBC_I4MOOD_LGT_INTS_byte	 CBC_I4.cbc_i4.CBC_I4MOOD_LGT_INTS

#define NETC_TX_RFHUB_B_A4RFMemRq_bit	 RFHUB_A4.rfhub_b_a4.RFHUB_B_A4RFMemRq
#define NETC_TX_RFHUB_B_A4RF_FobNum_bit	 RFHUB_A4.rfhub_b_a4.RFHUB_B_A4RF_FobNum

#define NETC_TX_STATUS_B_ECM2StartRelayECMFeedbackFault_bit	 STATUS_C_ECM2.status_b_ecm2.STATUS_B_ECM2StartRelayECMFeedbackFault
#define NETC_TX_STATUS_B_ECM2StartRelayBCMFeedbackFault_bit	 STATUS_C_ECM2.status_b_ecm2.STATUS_B_ECM2StartRelayBCMFeedbackFault
#define NETC_TX_STATUS_B_ECM2SAMInfo_bit	 STATUS_C_ECM2.status_b_ecm2.STATUS_B_ECM2SAMInfo

#define NETC_TX_STATUS_B_ECMFuelWaterPresentFailSts_bit	 STATUS_C_ECM.status_b_ecm.STATUS_B_ECMFuelWaterPresentFailSts
#define NETC_TX_STATUS_B_ECMFuelWaterPresentSts_bit	 STATUS_C_ECM.status_b_ecm.STATUS_B_ECMFuelWaterPresentSts
#define NETC_TX_STATUS_B_ECMEngineWaterTempWarningLightSts_bit	 STATUS_C_ECM.status_b_ecm.STATUS_B_ECMEngineWaterTempWarningLightSts
#define NETC_TX_STATUS_B_ECMEngineWaterTempFailSts_bit	 STATUS_C_ECM.status_b_ecm.STATUS_B_ECMEngineWaterTempFailSts
#define NETC_TX_STATUS_B_ECMCompressorSts_bit	 STATUS_C_ECM.status_b_ecm.STATUS_B_ECMCompressorSts
#define NETC_TX_STATUS_B_ECMEngineWaterTemp_byte	 STATUS_C_ECM.status_b_ecm.STATUS_B_ECMEngineWaterTemp
#define NETC_TX_STATUS_B_ECMEngineSpeed_byte	 STATUS_C_ECM.status_b_ecm.STATUS_B_ECMEngineSpeed
#define NETC_TX_STATUS_B_ECMReverseGearSts_bit	 STATUS_C_ECM.status_b_ecm.STATUS_B_ECMReverseGearSts
#define NETC_TX_STATUS_B_ECMEngineSpeedValidData_bit	 STATUS_C_ECM.status_b_ecm.STATUS_B_ECMEngineSpeedValidData

#define NETC_TX_STATUS_B_BSMDSTFailSts_bit	 STATUS_C_BSM.status_b_bsm.STATUS_B_BSMDSTFailSts
#define NETC_TX_STATUS_B_BSMVehicleSpeed_bit_0 STATUS_C_BSM.status_b_bsm.STATUS_B_BSMVehicleSpeed_0
#define NETC_TX_STATUS_B_BSMVehicleSpeed_bit_1 STATUS_C_BSM.status_b_bsm.STATUS_B_BSMVehicleSpeed_1                                                                     
#define NETC_TX_STATUS_B_BSMVehicleSpeed_bit(c)    { NETC_TX_STATUS_B_BSMVehicleSpeed_bit_0 = ((vuint8)(((vuint16)(c) & 0xff)));\
                                        NETC_TX_STATUS_B_BSMVehicleSpeed_bit_1 = ((vuint8)(((vuint16)(c) & 0xff00) >> 8));\
                                      }
#define NETC_TX_STATUS_B_BSMHHCustomerDisableSts_bit	 STATUS_C_BSM.status_b_bsm.STATUS_B_BSMHHCustomerDisableSts
#define NETC_TX_STATUS_B_BSMESCControlSts_bit	 STATUS_C_BSM.status_b_bsm.STATUS_B_BSMESCControlSts
#define NETC_TX_STATUS_B_BSMLHRPulseCounter_bit_0 STATUS_C_BSM.status_b_bsm.STATUS_B_BSMLHRPulseCounter_0
#define NETC_TX_STATUS_B_BSMLHRPulseCounter_bit_1 STATUS_C_BSM.status_b_bsm.STATUS_B_BSMLHRPulseCounter_1                                                                     
#define NETC_TX_STATUS_B_BSMLHRPulseCounter_bit(c)    { NETC_TX_STATUS_B_BSMLHRPulseCounter_bit_0 = ((vuint8)(((vuint16)(c) & 0xff)));\
                                        NETC_TX_STATUS_B_BSMLHRPulseCounter_bit_1 = ((vuint8)(((vuint16)(c) & 0xff00) >> 8));\
                                      }
#define NETC_TX_STATUS_B_BSMRHRPulseCounterFailSts_bit	 STATUS_C_BSM.status_b_bsm.STATUS_B_BSMRHRPulseCounterFailSts
#define NETC_TX_STATUS_B_BSMLHRPulseCounterFailSts_bit	 STATUS_C_BSM.status_b_bsm.STATUS_B_BSMLHRPulseCounterFailSts
#define NETC_TX_STATUS_B_BSMVehicleSpeedFailSts_bit	 STATUS_C_BSM.status_b_bsm.STATUS_B_BSMVehicleSpeedFailSts
#define NETC_TX_STATUS_B_BSMRHRPulseCounter_bit_0 STATUS_C_BSM.status_b_bsm.STATUS_B_BSMRHRPulseCounter_0
#define NETC_TX_STATUS_B_BSMRHRPulseCounter_bit_1 STATUS_C_BSM.status_b_bsm.STATUS_B_BSMRHRPulseCounter_1                                                                     
#define NETC_TX_STATUS_B_BSMRHRPulseCounter_bit(c)    { NETC_TX_STATUS_B_BSMRHRPulseCounter_bit_0 = ((vuint8)(((vuint16)(c) & 0xff)));\
                                        NETC_TX_STATUS_B_BSMRHRPulseCounter_bit_1 = ((vuint8)(((vuint16)(c) & 0xff00) >> 8));\
                                      }

/*************************************************************/
/* Signals of receive messages                               */
/*************************************************************/

#define NETC_RX_STATUS_C_OCMGenericFailSts_bit	 STATUS_C_OCM.status_c_ocm.STATUS_C_OCMGenericFailSts
#define NETC_RX_STATUS_C_OCMCurrentFailSts_bit	 STATUS_C_OCM.status_c_ocm.STATUS_C_OCMCurrentFailSts
#define NETC_RX_STATUS_C_OCMEOL_bit	 STATUS_C_OCM.status_c_ocm.STATUS_C_OCMEOL

#define NETC_RX_CFG_DATA_CODE_RSP_OCMDigit_10_bit	 CFG_DATA_CODE_RSP_OCM.cfg_data_code_rsp_ocm.CFG_DATA_CODE_RSP_OCMDigit_10
#define NETC_RX_CFG_DATA_CODE_RSP_OCMDigit_11_bit	 CFG_DATA_CODE_RSP_OCM.cfg_data_code_rsp_ocm.CFG_DATA_CODE_RSP_OCMDigit_11
#define NETC_RX_CFG_DATA_CODE_RSP_OCMDigit_08_bit	 CFG_DATA_CODE_RSP_OCM.cfg_data_code_rsp_ocm.CFG_DATA_CODE_RSP_OCMDigit_08
#define NETC_RX_CFG_DATA_CODE_RSP_OCMDigit_09_bit	 CFG_DATA_CODE_RSP_OCM.cfg_data_code_rsp_ocm.CFG_DATA_CODE_RSP_OCMDigit_09
#define NETC_RX_CFG_DATA_CODE_RSP_OCMDigit_06_bit	 CFG_DATA_CODE_RSP_OCM.cfg_data_code_rsp_ocm.CFG_DATA_CODE_RSP_OCMDigit_06
#define NETC_RX_CFG_DATA_CODE_RSP_OCMDigit_07_bit	 CFG_DATA_CODE_RSP_OCM.cfg_data_code_rsp_ocm.CFG_DATA_CODE_RSP_OCMDigit_07
#define NETC_RX_CFG_DATA_CODE_RSP_OCMDigit_04_bit	 CFG_DATA_CODE_RSP_OCM.cfg_data_code_rsp_ocm.CFG_DATA_CODE_RSP_OCMDigit_04
#define NETC_RX_CFG_DATA_CODE_RSP_OCMDigit_05_bit	 CFG_DATA_CODE_RSP_OCM.cfg_data_code_rsp_ocm.CFG_DATA_CODE_RSP_OCMDigit_05
#define NETC_RX_CFG_DATA_CODE_RSP_OCMDigit_02_bit	 CFG_DATA_CODE_RSP_OCM.cfg_data_code_rsp_ocm.CFG_DATA_CODE_RSP_OCMDigit_02
#define NETC_RX_CFG_DATA_CODE_RSP_OCMDigit_03_bit	 CFG_DATA_CODE_RSP_OCM.cfg_data_code_rsp_ocm.CFG_DATA_CODE_RSP_OCMDigit_03
#define NETC_RX_CFG_DATA_CODE_RSP_OCMDigit_01_bit	 CFG_DATA_CODE_RSP_OCM.cfg_data_code_rsp_ocm.CFG_DATA_CODE_RSP_OCMDigit_01

#define NETC_RX_CFG_DATA_CODE_RSP_AHLMDigit_10_bit	 CFG_DATA_CODE_RSP_AHLM.cfg_data_code_rsp_ahlm.CFG_DATA_CODE_RSP_AHLMDigit_10
#define NETC_RX_CFG_DATA_CODE_RSP_AHLMDigit_11_bit	 CFG_DATA_CODE_RSP_AHLM.cfg_data_code_rsp_ahlm.CFG_DATA_CODE_RSP_AHLMDigit_11
#define NETC_RX_CFG_DATA_CODE_RSP_AHLMDigit_08_bit	 CFG_DATA_CODE_RSP_AHLM.cfg_data_code_rsp_ahlm.CFG_DATA_CODE_RSP_AHLMDigit_08
#define NETC_RX_CFG_DATA_CODE_RSP_AHLMDigit_09_bit	 CFG_DATA_CODE_RSP_AHLM.cfg_data_code_rsp_ahlm.CFG_DATA_CODE_RSP_AHLMDigit_09
#define NETC_RX_CFG_DATA_CODE_RSP_AHLMDigit_06_bit	 CFG_DATA_CODE_RSP_AHLM.cfg_data_code_rsp_ahlm.CFG_DATA_CODE_RSP_AHLMDigit_06
#define NETC_RX_CFG_DATA_CODE_RSP_AHLMDigit_07_bit	 CFG_DATA_CODE_RSP_AHLM.cfg_data_code_rsp_ahlm.CFG_DATA_CODE_RSP_AHLMDigit_07
#define NETC_RX_CFG_DATA_CODE_RSP_AHLMDigit_04_bit	 CFG_DATA_CODE_RSP_AHLM.cfg_data_code_rsp_ahlm.CFG_DATA_CODE_RSP_AHLMDigit_04
#define NETC_RX_CFG_DATA_CODE_RSP_AHLMDigit_05_bit	 CFG_DATA_CODE_RSP_AHLM.cfg_data_code_rsp_ahlm.CFG_DATA_CODE_RSP_AHLMDigit_05
#define NETC_RX_CFG_DATA_CODE_RSP_AHLMDigit_02_bit	 CFG_DATA_CODE_RSP_AHLM.cfg_data_code_rsp_ahlm.CFG_DATA_CODE_RSP_AHLMDigit_02
#define NETC_RX_CFG_DATA_CODE_RSP_AHLMDigit_03_bit	 CFG_DATA_CODE_RSP_AHLM.cfg_data_code_rsp_ahlm.CFG_DATA_CODE_RSP_AHLMDigit_03
#define NETC_RX_CFG_DATA_CODE_RSP_AHLMDigit_01_bit	 CFG_DATA_CODE_RSP_AHLM.cfg_data_code_rsp_ahlm.CFG_DATA_CODE_RSP_AHLMDigit_01

#define NETC_RX_CFG_DATA_CODE_RSP_DTCMDigit_10_bit	 CFG_DATA_CODE_RSP_DTCM.cfg_data_code_rsp_dtcm.CFG_DATA_CODE_RSP_DTCMDigit_10
#define NETC_RX_CFG_DATA_CODE_RSP_DTCMDigit_11_bit	 CFG_DATA_CODE_RSP_DTCM.cfg_data_code_rsp_dtcm.CFG_DATA_CODE_RSP_DTCMDigit_11
#define NETC_RX_CFG_DATA_CODE_RSP_DTCMDigit_08_bit	 CFG_DATA_CODE_RSP_DTCM.cfg_data_code_rsp_dtcm.CFG_DATA_CODE_RSP_DTCMDigit_08
#define NETC_RX_CFG_DATA_CODE_RSP_DTCMDigit_09_bit	 CFG_DATA_CODE_RSP_DTCM.cfg_data_code_rsp_dtcm.CFG_DATA_CODE_RSP_DTCMDigit_09
#define NETC_RX_CFG_DATA_CODE_RSP_DTCMDigit_06_bit	 CFG_DATA_CODE_RSP_DTCM.cfg_data_code_rsp_dtcm.CFG_DATA_CODE_RSP_DTCMDigit_06
#define NETC_RX_CFG_DATA_CODE_RSP_DTCMDigit_07_bit	 CFG_DATA_CODE_RSP_DTCM.cfg_data_code_rsp_dtcm.CFG_DATA_CODE_RSP_DTCMDigit_07
#define NETC_RX_CFG_DATA_CODE_RSP_DTCMDigit_04_bit	 CFG_DATA_CODE_RSP_DTCM.cfg_data_code_rsp_dtcm.CFG_DATA_CODE_RSP_DTCMDigit_04
#define NETC_RX_CFG_DATA_CODE_RSP_DTCMDigit_05_bit	 CFG_DATA_CODE_RSP_DTCM.cfg_data_code_rsp_dtcm.CFG_DATA_CODE_RSP_DTCMDigit_05
#define NETC_RX_CFG_DATA_CODE_RSP_DTCMDigit_02_bit	 CFG_DATA_CODE_RSP_DTCM.cfg_data_code_rsp_dtcm.CFG_DATA_CODE_RSP_DTCMDigit_02
#define NETC_RX_CFG_DATA_CODE_RSP_DTCMDigit_03_bit	 CFG_DATA_CODE_RSP_DTCM.cfg_data_code_rsp_dtcm.CFG_DATA_CODE_RSP_DTCMDigit_03
#define NETC_RX_CFG_DATA_CODE_RSP_DTCMDigit_01_bit	 CFG_DATA_CODE_RSP_DTCM.cfg_data_code_rsp_dtcm.CFG_DATA_CODE_RSP_DTCMDigit_01

#define NETC_RX_CFG_DATA_CODE_RSP_RFHMDigit_10_bit	 CFG_DATA_CODE_RSP_RFHM.cfg_data_code_rsp_rfhm.CFG_DATA_CODE_RSP_RFHMDigit_10
#define NETC_RX_CFG_DATA_CODE_RSP_RFHMDigit_11_bit	 CFG_DATA_CODE_RSP_RFHM.cfg_data_code_rsp_rfhm.CFG_DATA_CODE_RSP_RFHMDigit_11
#define NETC_RX_CFG_DATA_CODE_RSP_RFHMDigit_08_bit	 CFG_DATA_CODE_RSP_RFHM.cfg_data_code_rsp_rfhm.CFG_DATA_CODE_RSP_RFHMDigit_08
#define NETC_RX_CFG_DATA_CODE_RSP_RFHMDigit_09_bit	 CFG_DATA_CODE_RSP_RFHM.cfg_data_code_rsp_rfhm.CFG_DATA_CODE_RSP_RFHMDigit_09
#define NETC_RX_CFG_DATA_CODE_RSP_RFHMDigit_06_bit	 CFG_DATA_CODE_RSP_RFHM.cfg_data_code_rsp_rfhm.CFG_DATA_CODE_RSP_RFHMDigit_06
#define NETC_RX_CFG_DATA_CODE_RSP_RFHMDigit_07_bit	 CFG_DATA_CODE_RSP_RFHM.cfg_data_code_rsp_rfhm.CFG_DATA_CODE_RSP_RFHMDigit_07
#define NETC_RX_CFG_DATA_CODE_RSP_RFHMDigit_04_bit	 CFG_DATA_CODE_RSP_RFHM.cfg_data_code_rsp_rfhm.CFG_DATA_CODE_RSP_RFHMDigit_04
#define NETC_RX_CFG_DATA_CODE_RSP_RFHMDigit_05_bit	 CFG_DATA_CODE_RSP_RFHM.cfg_data_code_rsp_rfhm.CFG_DATA_CODE_RSP_RFHMDigit_05
#define NETC_RX_CFG_DATA_CODE_RSP_RFHMDigit_02_bit	 CFG_DATA_CODE_RSP_RFHM.cfg_data_code_rsp_rfhm.CFG_DATA_CODE_RSP_RFHMDigit_02
#define NETC_RX_CFG_DATA_CODE_RSP_RFHMDigit_03_bit	 CFG_DATA_CODE_RSP_RFHM.cfg_data_code_rsp_rfhm.CFG_DATA_CODE_RSP_RFHMDigit_03
#define NETC_RX_CFG_DATA_CODE_RSP_RFHMDigit_01_bit	 CFG_DATA_CODE_RSP_RFHM.cfg_data_code_rsp_rfhm.CFG_DATA_CODE_RSP_RFHMDigit_01

#define NETC_RX_CFG_DATA_CODE_RSP_DASMDigit_10_bit	 CFG_DATA_CODE_RSP_DASM.cfg_data_code_rsp_dasm.CFG_DATA_CODE_RSP_DASMDigit_10
#define NETC_RX_CFG_DATA_CODE_RSP_DASMDigit_11_bit	 CFG_DATA_CODE_RSP_DASM.cfg_data_code_rsp_dasm.CFG_DATA_CODE_RSP_DASMDigit_11
#define NETC_RX_CFG_DATA_CODE_RSP_DASMDigit_08_bit	 CFG_DATA_CODE_RSP_DASM.cfg_data_code_rsp_dasm.CFG_DATA_CODE_RSP_DASMDigit_08
#define NETC_RX_CFG_DATA_CODE_RSP_DASMDigit_09_bit	 CFG_DATA_CODE_RSP_DASM.cfg_data_code_rsp_dasm.CFG_DATA_CODE_RSP_DASMDigit_09
#define NETC_RX_CFG_DATA_CODE_RSP_DASMDigit_06_bit	 CFG_DATA_CODE_RSP_DASM.cfg_data_code_rsp_dasm.CFG_DATA_CODE_RSP_DASMDigit_06
#define NETC_RX_CFG_DATA_CODE_RSP_DASMDigit_07_bit	 CFG_DATA_CODE_RSP_DASM.cfg_data_code_rsp_dasm.CFG_DATA_CODE_RSP_DASMDigit_07
#define NETC_RX_CFG_DATA_CODE_RSP_DASMDigit_04_bit	 CFG_DATA_CODE_RSP_DASM.cfg_data_code_rsp_dasm.CFG_DATA_CODE_RSP_DASMDigit_04
#define NETC_RX_CFG_DATA_CODE_RSP_DASMDigit_05_bit	 CFG_DATA_CODE_RSP_DASM.cfg_data_code_rsp_dasm.CFG_DATA_CODE_RSP_DASMDigit_05
#define NETC_RX_CFG_DATA_CODE_RSP_DASMDigit_02_bit	 CFG_DATA_CODE_RSP_DASM.cfg_data_code_rsp_dasm.CFG_DATA_CODE_RSP_DASMDigit_02
#define NETC_RX_CFG_DATA_CODE_RSP_DASMDigit_03_bit	 CFG_DATA_CODE_RSP_DASM.cfg_data_code_rsp_dasm.CFG_DATA_CODE_RSP_DASMDigit_03
#define NETC_RX_CFG_DATA_CODE_RSP_DASMDigit_01_bit	 CFG_DATA_CODE_RSP_DASM.cfg_data_code_rsp_dasm.CFG_DATA_CODE_RSP_DASMDigit_01

#define NETC_RX_CFG_DATA_CODE_RSP_SCCMDigit_10_bit	 CFG_DATA_CODE_RSP_SCCM.cfg_data_code_rsp_sccm.CFG_DATA_CODE_RSP_SCCMDigit_10
#define NETC_RX_CFG_DATA_CODE_RSP_SCCMDigit_11_bit	 CFG_DATA_CODE_RSP_SCCM.cfg_data_code_rsp_sccm.CFG_DATA_CODE_RSP_SCCMDigit_11
#define NETC_RX_CFG_DATA_CODE_RSP_SCCMDigit_08_bit	 CFG_DATA_CODE_RSP_SCCM.cfg_data_code_rsp_sccm.CFG_DATA_CODE_RSP_SCCMDigit_08
#define NETC_RX_CFG_DATA_CODE_RSP_SCCMDigit_09_bit	 CFG_DATA_CODE_RSP_SCCM.cfg_data_code_rsp_sccm.CFG_DATA_CODE_RSP_SCCMDigit_09
#define NETC_RX_CFG_DATA_CODE_RSP_SCCMDigit_06_bit	 CFG_DATA_CODE_RSP_SCCM.cfg_data_code_rsp_sccm.CFG_DATA_CODE_RSP_SCCMDigit_06
#define NETC_RX_CFG_DATA_CODE_RSP_SCCMDigit_07_bit	 CFG_DATA_CODE_RSP_SCCM.cfg_data_code_rsp_sccm.CFG_DATA_CODE_RSP_SCCMDigit_07
#define NETC_RX_CFG_DATA_CODE_RSP_SCCMDigit_04_bit	 CFG_DATA_CODE_RSP_SCCM.cfg_data_code_rsp_sccm.CFG_DATA_CODE_RSP_SCCMDigit_04
#define NETC_RX_CFG_DATA_CODE_RSP_SCCMDigit_05_bit	 CFG_DATA_CODE_RSP_SCCM.cfg_data_code_rsp_sccm.CFG_DATA_CODE_RSP_SCCMDigit_05
#define NETC_RX_CFG_DATA_CODE_RSP_SCCMDigit_02_bit	 CFG_DATA_CODE_RSP_SCCM.cfg_data_code_rsp_sccm.CFG_DATA_CODE_RSP_SCCMDigit_02
#define NETC_RX_CFG_DATA_CODE_RSP_SCCMDigit_03_bit	 CFG_DATA_CODE_RSP_SCCM.cfg_data_code_rsp_sccm.CFG_DATA_CODE_RSP_SCCMDigit_03
#define NETC_RX_CFG_DATA_CODE_RSP_SCCMDigit_01_bit	 CFG_DATA_CODE_RSP_SCCM.cfg_data_code_rsp_sccm.CFG_DATA_CODE_RSP_SCCMDigit_01

#define NETC_RX_CFG_DATA_CODE_RSP_HALFDigit_10_bit	 CFG_DATA_CODE_RSP_HALF.cfg_data_code_rsp_half.CFG_DATA_CODE_RSP_HALFDigit_10
#define NETC_RX_CFG_DATA_CODE_RSP_HALFDigit_11_bit	 CFG_DATA_CODE_RSP_HALF.cfg_data_code_rsp_half.CFG_DATA_CODE_RSP_HALFDigit_11
#define NETC_RX_CFG_DATA_CODE_RSP_HALFDigit_08_bit	 CFG_DATA_CODE_RSP_HALF.cfg_data_code_rsp_half.CFG_DATA_CODE_RSP_HALFDigit_08
#define NETC_RX_CFG_DATA_CODE_RSP_HALFDigit_09_bit	 CFG_DATA_CODE_RSP_HALF.cfg_data_code_rsp_half.CFG_DATA_CODE_RSP_HALFDigit_09
#define NETC_RX_CFG_DATA_CODE_RSP_HALFDigit_06_bit	 CFG_DATA_CODE_RSP_HALF.cfg_data_code_rsp_half.CFG_DATA_CODE_RSP_HALFDigit_06
#define NETC_RX_CFG_DATA_CODE_RSP_HALFDigit_07_bit	 CFG_DATA_CODE_RSP_HALF.cfg_data_code_rsp_half.CFG_DATA_CODE_RSP_HALFDigit_07
#define NETC_RX_CFG_DATA_CODE_RSP_HALFDigit_04_bit	 CFG_DATA_CODE_RSP_HALF.cfg_data_code_rsp_half.CFG_DATA_CODE_RSP_HALFDigit_04
#define NETC_RX_CFG_DATA_CODE_RSP_HALFDigit_05_bit	 CFG_DATA_CODE_RSP_HALF.cfg_data_code_rsp_half.CFG_DATA_CODE_RSP_HALFDigit_05
#define NETC_RX_CFG_DATA_CODE_RSP_HALFDigit_02_bit	 CFG_DATA_CODE_RSP_HALF.cfg_data_code_rsp_half.CFG_DATA_CODE_RSP_HALFDigit_02
#define NETC_RX_CFG_DATA_CODE_RSP_HALFDigit_03_bit	 CFG_DATA_CODE_RSP_HALF.cfg_data_code_rsp_half.CFG_DATA_CODE_RSP_HALFDigit_03
#define NETC_RX_CFG_DATA_CODE_RSP_HALFDigit_01_bit	 CFG_DATA_CODE_RSP_HALF.cfg_data_code_rsp_half.CFG_DATA_CODE_RSP_HALFDigit_01

#define NETC_RX_CFG_DATA_CODE_RSP_ORCDigit_10_bit	 CFG_DATA_CODE_RSP_ORC.cfg_data_code_rsp_orc.CFG_DATA_CODE_RSP_ORCDigit_10
#define NETC_RX_CFG_DATA_CODE_RSP_ORCDigit_11_bit	 CFG_DATA_CODE_RSP_ORC.cfg_data_code_rsp_orc.CFG_DATA_CODE_RSP_ORCDigit_11
#define NETC_RX_CFG_DATA_CODE_RSP_ORCDigit_08_bit	 CFG_DATA_CODE_RSP_ORC.cfg_data_code_rsp_orc.CFG_DATA_CODE_RSP_ORCDigit_08
#define NETC_RX_CFG_DATA_CODE_RSP_ORCDigit_09_bit	 CFG_DATA_CODE_RSP_ORC.cfg_data_code_rsp_orc.CFG_DATA_CODE_RSP_ORCDigit_09
#define NETC_RX_CFG_DATA_CODE_RSP_ORCDigit_06_bit	 CFG_DATA_CODE_RSP_ORC.cfg_data_code_rsp_orc.CFG_DATA_CODE_RSP_ORCDigit_06
#define NETC_RX_CFG_DATA_CODE_RSP_ORCDigit_07_bit	 CFG_DATA_CODE_RSP_ORC.cfg_data_code_rsp_orc.CFG_DATA_CODE_RSP_ORCDigit_07
#define NETC_RX_CFG_DATA_CODE_RSP_ORCDigit_04_bit	 CFG_DATA_CODE_RSP_ORC.cfg_data_code_rsp_orc.CFG_DATA_CODE_RSP_ORCDigit_04
#define NETC_RX_CFG_DATA_CODE_RSP_ORCDigit_05_bit	 CFG_DATA_CODE_RSP_ORC.cfg_data_code_rsp_orc.CFG_DATA_CODE_RSP_ORCDigit_05
#define NETC_RX_CFG_DATA_CODE_RSP_ORCDigit_02_bit	 CFG_DATA_CODE_RSP_ORC.cfg_data_code_rsp_orc.CFG_DATA_CODE_RSP_ORCDigit_02
#define NETC_RX_CFG_DATA_CODE_RSP_ORCDigit_03_bit	 CFG_DATA_CODE_RSP_ORC.cfg_data_code_rsp_orc.CFG_DATA_CODE_RSP_ORCDigit_03
#define NETC_RX_CFG_DATA_CODE_RSP_ORCDigit_01_bit	 CFG_DATA_CODE_RSP_ORC.cfg_data_code_rsp_orc.CFG_DATA_CODE_RSP_ORCDigit_01

#define NETC_RX_CFG_DATA_CODE_RSP_PAMDigit_10_bit	 CFG_DATA_CODE_RSP_PAM.cfg_data_code_rsp_pam.CFG_DATA_CODE_RSP_PAMDigit_10
#define NETC_RX_CFG_DATA_CODE_RSP_PAMDigit_11_bit	 CFG_DATA_CODE_RSP_PAM.cfg_data_code_rsp_pam.CFG_DATA_CODE_RSP_PAMDigit_11
#define NETC_RX_CFG_DATA_CODE_RSP_PAMDigit_08_bit	 CFG_DATA_CODE_RSP_PAM.cfg_data_code_rsp_pam.CFG_DATA_CODE_RSP_PAMDigit_08
#define NETC_RX_CFG_DATA_CODE_RSP_PAMDigit_09_bit	 CFG_DATA_CODE_RSP_PAM.cfg_data_code_rsp_pam.CFG_DATA_CODE_RSP_PAMDigit_09
#define NETC_RX_CFG_DATA_CODE_RSP_PAMDigit_06_bit	 CFG_DATA_CODE_RSP_PAM.cfg_data_code_rsp_pam.CFG_DATA_CODE_RSP_PAMDigit_06
#define NETC_RX_CFG_DATA_CODE_RSP_PAMDigit_07_bit	 CFG_DATA_CODE_RSP_PAM.cfg_data_code_rsp_pam.CFG_DATA_CODE_RSP_PAMDigit_07
#define NETC_RX_CFG_DATA_CODE_RSP_PAMDigit_04_bit	 CFG_DATA_CODE_RSP_PAM.cfg_data_code_rsp_pam.CFG_DATA_CODE_RSP_PAMDigit_04
#define NETC_RX_CFG_DATA_CODE_RSP_PAMDigit_05_bit	 CFG_DATA_CODE_RSP_PAM.cfg_data_code_rsp_pam.CFG_DATA_CODE_RSP_PAMDigit_05
#define NETC_RX_CFG_DATA_CODE_RSP_PAMDigit_02_bit	 CFG_DATA_CODE_RSP_PAM.cfg_data_code_rsp_pam.CFG_DATA_CODE_RSP_PAMDigit_02
#define NETC_RX_CFG_DATA_CODE_RSP_PAMDigit_03_bit	 CFG_DATA_CODE_RSP_PAM.cfg_data_code_rsp_pam.CFG_DATA_CODE_RSP_PAMDigit_03
#define NETC_RX_CFG_DATA_CODE_RSP_PAMDigit_01_bit	 CFG_DATA_CODE_RSP_PAM.cfg_data_code_rsp_pam.CFG_DATA_CODE_RSP_PAMDigit_01

#define NETC_RX_CFG_DATA_CODE_RSP_EPBDigit_10_bit	 CFG_DATA_CODE_RSP_EPB.cfg_data_code_rsp_epb.CFG_DATA_CODE_RSP_EPBDigit_10
#define NETC_RX_CFG_DATA_CODE_RSP_EPBDigit_11_bit	 CFG_DATA_CODE_RSP_EPB.cfg_data_code_rsp_epb.CFG_DATA_CODE_RSP_EPBDigit_11
#define NETC_RX_CFG_DATA_CODE_RSP_EPBDigit_08_bit	 CFG_DATA_CODE_RSP_EPB.cfg_data_code_rsp_epb.CFG_DATA_CODE_RSP_EPBDigit_08
#define NETC_RX_CFG_DATA_CODE_RSP_EPBDigit_09_bit	 CFG_DATA_CODE_RSP_EPB.cfg_data_code_rsp_epb.CFG_DATA_CODE_RSP_EPBDigit_09
#define NETC_RX_CFG_DATA_CODE_RSP_EPBDigit_06_bit	 CFG_DATA_CODE_RSP_EPB.cfg_data_code_rsp_epb.CFG_DATA_CODE_RSP_EPBDigit_06
#define NETC_RX_CFG_DATA_CODE_RSP_EPBDigit_07_bit	 CFG_DATA_CODE_RSP_EPB.cfg_data_code_rsp_epb.CFG_DATA_CODE_RSP_EPBDigit_07
#define NETC_RX_CFG_DATA_CODE_RSP_EPBDigit_04_bit	 CFG_DATA_CODE_RSP_EPB.cfg_data_code_rsp_epb.CFG_DATA_CODE_RSP_EPBDigit_04
#define NETC_RX_CFG_DATA_CODE_RSP_EPBDigit_05_bit	 CFG_DATA_CODE_RSP_EPB.cfg_data_code_rsp_epb.CFG_DATA_CODE_RSP_EPBDigit_05
#define NETC_RX_CFG_DATA_CODE_RSP_EPBDigit_02_bit	 CFG_DATA_CODE_RSP_EPB.cfg_data_code_rsp_epb.CFG_DATA_CODE_RSP_EPBDigit_02
#define NETC_RX_CFG_DATA_CODE_RSP_EPBDigit_03_bit	 CFG_DATA_CODE_RSP_EPB.cfg_data_code_rsp_epb.CFG_DATA_CODE_RSP_EPBDigit_03
#define NETC_RX_CFG_DATA_CODE_RSP_EPBDigit_01_bit	 CFG_DATA_CODE_RSP_EPB.cfg_data_code_rsp_epb.CFG_DATA_CODE_RSP_EPBDigit_01

#define NETC_RX_CFG_DATA_CODE_RSP_TRANSMISSIONDigit_10_bit	 CFG_DATA_CODE_RSP_TRANSMISSION.cfg_data_code_rsp_transmission.CFG_DATA_CODE_RSP_TRANSMISSIONDigit_10
#define NETC_RX_CFG_DATA_CODE_RSP_TRANSMISSIONDigit_11_bit	 CFG_DATA_CODE_RSP_TRANSMISSION.cfg_data_code_rsp_transmission.CFG_DATA_CODE_RSP_TRANSMISSIONDigit_11
#define NETC_RX_CFG_DATA_CODE_RSP_TRANSMISSIONDigit_08_bit	 CFG_DATA_CODE_RSP_TRANSMISSION.cfg_data_code_rsp_transmission.CFG_DATA_CODE_RSP_TRANSMISSIONDigit_08
#define NETC_RX_CFG_DATA_CODE_RSP_TRANSMISSIONDigit_09_bit	 CFG_DATA_CODE_RSP_TRANSMISSION.cfg_data_code_rsp_transmission.CFG_DATA_CODE_RSP_TRANSMISSIONDigit_09
#define NETC_RX_CFG_DATA_CODE_RSP_TRANSMISSIONDigit_06_bit	 CFG_DATA_CODE_RSP_TRANSMISSION.cfg_data_code_rsp_transmission.CFG_DATA_CODE_RSP_TRANSMISSIONDigit_06
#define NETC_RX_CFG_DATA_CODE_RSP_TRANSMISSIONDigit_07_bit	 CFG_DATA_CODE_RSP_TRANSMISSION.cfg_data_code_rsp_transmission.CFG_DATA_CODE_RSP_TRANSMISSIONDigit_07
#define NETC_RX_CFG_DATA_CODE_RSP_TRANSMISSIONDigit_04_bit	 CFG_DATA_CODE_RSP_TRANSMISSION.cfg_data_code_rsp_transmission.CFG_DATA_CODE_RSP_TRANSMISSIONDigit_04
#define NETC_RX_CFG_DATA_CODE_RSP_TRANSMISSIONDigit_05_bit	 CFG_DATA_CODE_RSP_TRANSMISSION.cfg_data_code_rsp_transmission.CFG_DATA_CODE_RSP_TRANSMISSIONDigit_05
#define NETC_RX_CFG_DATA_CODE_RSP_TRANSMISSIONDigit_02_bit	 CFG_DATA_CODE_RSP_TRANSMISSION.cfg_data_code_rsp_transmission.CFG_DATA_CODE_RSP_TRANSMISSIONDigit_02
#define NETC_RX_CFG_DATA_CODE_RSP_TRANSMISSIONDigit_03_bit	 CFG_DATA_CODE_RSP_TRANSMISSION.cfg_data_code_rsp_transmission.CFG_DATA_CODE_RSP_TRANSMISSIONDigit_03
#define NETC_RX_CFG_DATA_CODE_RSP_TRANSMISSIONDigit_01_bit	 CFG_DATA_CODE_RSP_TRANSMISSION.cfg_data_code_rsp_transmission.CFG_DATA_CODE_RSP_TRANSMISSIONDigit_01

#define NETC_RX_CFG_DATA_CODE_RSP_BSMDigit_10_bit	 CFG_DATA_CODE_RSP_BSM.cfg_data_code_rsp_bsm.CFG_DATA_CODE_RSP_BSMDigit_10
#define NETC_RX_CFG_DATA_CODE_RSP_BSMDigit_11_bit	 CFG_DATA_CODE_RSP_BSM.cfg_data_code_rsp_bsm.CFG_DATA_CODE_RSP_BSMDigit_11
#define NETC_RX_CFG_DATA_CODE_RSP_BSMDigit_08_bit	 CFG_DATA_CODE_RSP_BSM.cfg_data_code_rsp_bsm.CFG_DATA_CODE_RSP_BSMDigit_08
#define NETC_RX_CFG_DATA_CODE_RSP_BSMDigit_09_bit	 CFG_DATA_CODE_RSP_BSM.cfg_data_code_rsp_bsm.CFG_DATA_CODE_RSP_BSMDigit_09
#define NETC_RX_CFG_DATA_CODE_RSP_BSMDigit_06_bit	 CFG_DATA_CODE_RSP_BSM.cfg_data_code_rsp_bsm.CFG_DATA_CODE_RSP_BSMDigit_06
#define NETC_RX_CFG_DATA_CODE_RSP_BSMDigit_07_bit	 CFG_DATA_CODE_RSP_BSM.cfg_data_code_rsp_bsm.CFG_DATA_CODE_RSP_BSMDigit_07
#define NETC_RX_CFG_DATA_CODE_RSP_BSMDigit_04_bit	 CFG_DATA_CODE_RSP_BSM.cfg_data_code_rsp_bsm.CFG_DATA_CODE_RSP_BSMDigit_04
#define NETC_RX_CFG_DATA_CODE_RSP_BSMDigit_05_bit	 CFG_DATA_CODE_RSP_BSM.cfg_data_code_rsp_bsm.CFG_DATA_CODE_RSP_BSMDigit_05
#define NETC_RX_CFG_DATA_CODE_RSP_BSMDigit_02_bit	 CFG_DATA_CODE_RSP_BSM.cfg_data_code_rsp_bsm.CFG_DATA_CODE_RSP_BSMDigit_02
#define NETC_RX_CFG_DATA_CODE_RSP_BSMDigit_03_bit	 CFG_DATA_CODE_RSP_BSM.cfg_data_code_rsp_bsm.CFG_DATA_CODE_RSP_BSMDigit_03
#define NETC_RX_CFG_DATA_CODE_RSP_BSMDigit_01_bit	 CFG_DATA_CODE_RSP_BSM.cfg_data_code_rsp_bsm.CFG_DATA_CODE_RSP_BSMDigit_01

#define NETC_RX_CFG_DATA_CODE_RSP_EPSDigit_10_bit	 CFG_DATA_CODE_RSP_EPS.cfg_data_code_rsp_eps.CFG_DATA_CODE_RSP_EPSDigit_10
#define NETC_RX_CFG_DATA_CODE_RSP_EPSDigit_11_bit	 CFG_DATA_CODE_RSP_EPS.cfg_data_code_rsp_eps.CFG_DATA_CODE_RSP_EPSDigit_11
#define NETC_RX_CFG_DATA_CODE_RSP_EPSDigit_08_bit	 CFG_DATA_CODE_RSP_EPS.cfg_data_code_rsp_eps.CFG_DATA_CODE_RSP_EPSDigit_08
#define NETC_RX_CFG_DATA_CODE_RSP_EPSDigit_09_bit	 CFG_DATA_CODE_RSP_EPS.cfg_data_code_rsp_eps.CFG_DATA_CODE_RSP_EPSDigit_09
#define NETC_RX_CFG_DATA_CODE_RSP_EPSDigit_06_bit	 CFG_DATA_CODE_RSP_EPS.cfg_data_code_rsp_eps.CFG_DATA_CODE_RSP_EPSDigit_06
#define NETC_RX_CFG_DATA_CODE_RSP_EPSDigit_07_bit	 CFG_DATA_CODE_RSP_EPS.cfg_data_code_rsp_eps.CFG_DATA_CODE_RSP_EPSDigit_07
#define NETC_RX_CFG_DATA_CODE_RSP_EPSDigit_04_bit	 CFG_DATA_CODE_RSP_EPS.cfg_data_code_rsp_eps.CFG_DATA_CODE_RSP_EPSDigit_04
#define NETC_RX_CFG_DATA_CODE_RSP_EPSDigit_05_bit	 CFG_DATA_CODE_RSP_EPS.cfg_data_code_rsp_eps.CFG_DATA_CODE_RSP_EPSDigit_05
#define NETC_RX_CFG_DATA_CODE_RSP_EPSDigit_02_bit	 CFG_DATA_CODE_RSP_EPS.cfg_data_code_rsp_eps.CFG_DATA_CODE_RSP_EPSDigit_02
#define NETC_RX_CFG_DATA_CODE_RSP_EPSDigit_03_bit	 CFG_DATA_CODE_RSP_EPS.cfg_data_code_rsp_eps.CFG_DATA_CODE_RSP_EPSDigit_03
#define NETC_RX_CFG_DATA_CODE_RSP_EPSDigit_01_bit	 CFG_DATA_CODE_RSP_EPS.cfg_data_code_rsp_eps.CFG_DATA_CODE_RSP_EPSDigit_01

#define NETC_RX_CFG_DATA_CODE_RSP_ECMDigit_10_bit	 CFG_DATA_CODE_RSP_ECM.cfg_data_code_rsp_ecm.CFG_DATA_CODE_RSP_ECMDigit_10
#define NETC_RX_CFG_DATA_CODE_RSP_ECMDigit_11_bit	 CFG_DATA_CODE_RSP_ECM.cfg_data_code_rsp_ecm.CFG_DATA_CODE_RSP_ECMDigit_11
#define NETC_RX_CFG_DATA_CODE_RSP_ECMDigit_08_bit	 CFG_DATA_CODE_RSP_ECM.cfg_data_code_rsp_ecm.CFG_DATA_CODE_RSP_ECMDigit_08
#define NETC_RX_CFG_DATA_CODE_RSP_ECMDigit_09_bit	 CFG_DATA_CODE_RSP_ECM.cfg_data_code_rsp_ecm.CFG_DATA_CODE_RSP_ECMDigit_09
#define NETC_RX_CFG_DATA_CODE_RSP_ECMDigit_06_bit	 CFG_DATA_CODE_RSP_ECM.cfg_data_code_rsp_ecm.CFG_DATA_CODE_RSP_ECMDigit_06
#define NETC_RX_CFG_DATA_CODE_RSP_ECMDigit_07_bit	 CFG_DATA_CODE_RSP_ECM.cfg_data_code_rsp_ecm.CFG_DATA_CODE_RSP_ECMDigit_07
#define NETC_RX_CFG_DATA_CODE_RSP_ECMDigit_04_bit	 CFG_DATA_CODE_RSP_ECM.cfg_data_code_rsp_ecm.CFG_DATA_CODE_RSP_ECMDigit_04
#define NETC_RX_CFG_DATA_CODE_RSP_ECMDigit_05_bit	 CFG_DATA_CODE_RSP_ECM.cfg_data_code_rsp_ecm.CFG_DATA_CODE_RSP_ECMDigit_05
#define NETC_RX_CFG_DATA_CODE_RSP_ECMDigit_02_bit	 CFG_DATA_CODE_RSP_ECM.cfg_data_code_rsp_ecm.CFG_DATA_CODE_RSP_ECMDigit_02
#define NETC_RX_CFG_DATA_CODE_RSP_ECMDigit_03_bit	 CFG_DATA_CODE_RSP_ECM.cfg_data_code_rsp_ecm.CFG_DATA_CODE_RSP_ECMDigit_03
#define NETC_RX_CFG_DATA_CODE_RSP_ECMDigit_01_bit	 CFG_DATA_CODE_RSP_ECM.cfg_data_code_rsp_ecm.CFG_DATA_CODE_RSP_ECMDigit_01

#define NETC_RX_STATUS_C_DASMGenericFailSts_bit	 STATUS_C_DASM.status_c_dasm.STATUS_C_DASMGenericFailSts
#define NETC_RX_STATUS_C_DASMCurrentFailSts_bit	 STATUS_C_DASM.status_c_dasm.STATUS_C_DASMCurrentFailSts
#define NETC_RX_STATUS_C_DASMEOL_bit	 STATUS_C_DASM.status_c_dasm.STATUS_C_DASMEOL
#define NETC_RX_STATUS_C_DASMASBM_LedControlSts_bit	 STATUS_C_DASM.status_c_dasm.STATUS_C_DASMASBM_LedControlSts

#define NETC_RX_STATUS_C_AHLMGenericFailSts_bit	 STATUS_C_AHLM.status_c_ahlm.STATUS_C_AHLMGenericFailSts
#define NETC_RX_STATUS_C_AHLMCurrentFailSts_bit	 STATUS_C_AHLM.status_c_ahlm.STATUS_C_AHLMCurrentFailSts
#define NETC_RX_STATUS_C_AHLMEOL_bit	 STATUS_C_AHLM.status_c_ahlm.STATUS_C_AHLMEOL

#define NETC_RX_CAN_DIAGNOSTIC_REQUEST_BCM_0N_PDU_byte	 RDSBasic_0.diagnostic_request_bcm_0.DIAGNOSTIC_REQUEST_BCM_0N_PDU

#define NETC_RX_STATUS_C_HALFGenericFailSts_bit	 STATUS_C_HALF.status_c_half.STATUS_C_HALFGenericFailSts
#define NETC_RX_STATUS_C_HALFCurrentFailSts_bit	 STATUS_C_HALF.status_c_half.STATUS_C_HALFCurrentFailSts
#define NETC_RX_STATUS_C_HALFEOL_bit	 STATUS_C_HALF.status_c_half.STATUS_C_HALFEOL
#define NETC_RX_STATUS_C_HALFConfigSts_bit	 STATUS_C_HALF.status_c_half.STATUS_C_HALFConfigSts
#define NETC_RX_STATUS_C_HALFAHB_ON_bit	 STATUS_C_HALF.status_c_half.STATUS_C_HALFAHB_ON
#define NETC_RX_STATUS_C_HALFTorqueIntensitySts_bit	 STATUS_C_HALF.status_c_half.STATUS_C_HALFTorqueIntensitySts
#define NETC_RX_STATUS_C_HALFASBM_LedControlSts_bit	 STATUS_C_HALF.status_c_half.STATUS_C_HALFASBM_LedControlSts

#define NETC_RX_STATUS_C_ESLGenericFailSts_bit	 STATUS_C_ESL.status_c_esl.STATUS_C_ESLGenericFailSts
#define NETC_RX_STATUS_C_ESLCurrentFailSts_bit	 STATUS_C_ESL.status_c_esl.STATUS_C_ESLCurrentFailSts
#define NETC_RX_STATUS_C_ESLEOL_bit	 STATUS_C_ESL.status_c_esl.STATUS_C_ESLEOL
#define NETC_RX_STATUS_C_ESLESLSts_bit	 STATUS_C_ESL.status_c_esl.STATUS_C_ESLESLSts
#define NETC_RX_STATUS_C_ESLKeyOffWithHighSpeedSts_bit	 STATUS_C_ESL.status_c_esl.STATUS_C_ESLKeyOffWithHighSpeedSts

#define NETC_RX_STATUS_C_EPBGenericFailSts_bit	 STATUS_C_EPB.status_c_epb.STATUS_C_EPBGenericFailSts
#define NETC_RX_STATUS_C_EPBCurrentFailSts_bit	 STATUS_C_EPB.status_c_epb.STATUS_C_EPBCurrentFailSts
#define NETC_RX_STATUS_C_EPBEOL_bit	 STATUS_C_EPB.status_c_epb.STATUS_C_EPBEOL
#define NETC_RX_STATUS_C_EPBStopLampActive_bit	 STATUS_C_EPB.status_c_epb.STATUS_C_EPBStopLampActive
#define NETC_RX_STATUS_C_EPBEPBSts_bit	 STATUS_C_EPB.status_c_epb.STATUS_C_EPBEPBSts
#define NETC_RX_STATUS_C_EPBAutoParkSts_bit	 STATUS_C_EPB.status_c_epb.STATUS_C_EPBAutoParkSts
#define NETC_RX_STATUS_C_EPBEPBChimeReq_bit	 STATUS_C_EPB.status_c_epb.STATUS_C_EPBEPBChimeReq
#define NETC_RX_STATUS_C_EPBServiceModeSts_bit	 STATUS_C_EPB.status_c_epb.STATUS_C_EPBServiceModeSts
#define NETC_RX_STATUS_C_EPBTextDisplay_bit	 STATUS_C_EPB.status_c_epb.STATUS_C_EPBTextDisplay
#define NETC_RX_STATUS_C_EPBFunctionLamp_bit	 STATUS_C_EPB.status_c_epb.STATUS_C_EPBFunctionLamp
#define NETC_RX_STATUS_C_EPBEPBWarningLampReq_bit	 STATUS_C_EPB.status_c_epb.STATUS_C_EPBEPBWarningLampReq

#define NETC_RX_HALF_C_Warning_RQHALF_LF_CHIME_RQSts_bit	 HALF_C_Warning_RQ.half_c_warning_rq.HALF_C_Warning_RQHALF_LF_CHIME_RQSts
#define NETC_RX_HALF_C_Warning_RQHALF_RF_CHIME_RQSts_bit	 HALF_C_Warning_RQ.half_c_warning_rq.HALF_C_Warning_RQHALF_RF_CHIME_RQSts
#define NETC_RX_HALF_C_Warning_RQHALF_CHIME_TYPESts_bit	 HALF_C_Warning_RQ.half_c_warning_rq.HALF_C_Warning_RQHALF_CHIME_TYPESts
#define NETC_RX_HALF_C_Warning_RQHALF_CHIME_REP_RATESts_bit	 HALF_C_Warning_RQ.half_c_warning_rq.HALF_C_Warning_RQHALF_CHIME_REP_RATESts

#define NETC_RX_StW_Actn_Rq_0LT_TURN_RQ_bit	 StW_Actn_Rq_0.stw_actn_rq_0.StW_Actn_Rq_0LT_TURN_RQ
#define NETC_RX_StW_Actn_Rq_0RT_TURN_RQ_bit	 StW_Actn_Rq_0.stw_actn_rq_0.StW_Actn_Rq_0RT_TURN_RQ
#define NETC_RX_StW_Actn_Rq_0WprWashSw_Psd_bit	 StW_Actn_Rq_0.stw_actn_rq_0.StW_Actn_Rq_0WprWashSw_Psd
#define NETC_RX_StW_Actn_Rq_0WprWash_R_Sw_Posn_V3_bit	 StW_Actn_Rq_0.stw_actn_rq_0.StW_Actn_Rq_0WprWash_R_Sw_Posn_V3
#define NETC_RX_StW_Actn_Rq_0HI_BEAM_LMP_RQ_bit	 StW_Actn_Rq_0.stw_actn_rq_0.StW_Actn_Rq_0HI_BEAM_LMP_RQ
#define NETC_RX_StW_Actn_Rq_0WprSw6Posn_bit	 StW_Actn_Rq_0.stw_actn_rq_0.StW_Actn_Rq_0WprSw6Posn

#define NETC_RX_STATUS_C_TRANSMISSIONGenericFailSts_bit	 STATUS_C_TRANSMISSION.status_c_transmission.STATUS_C_TRANSMISSIONGenericFailSts
#define NETC_RX_STATUS_C_TRANSMISSIONCurrentFailSts_bit	 STATUS_C_TRANSMISSION.status_c_transmission.STATUS_C_TRANSMISSIONCurrentFailSts
#define NETC_RX_STATUS_C_TRANSMISSIONEOL_bit	 STATUS_C_TRANSMISSION.status_c_transmission.STATUS_C_TRANSMISSIONEOL
#define NETC_RX_STATUS_C_TRANSMISSIONShiftLeverPosition_bit	 STATUS_C_TRANSMISSION.status_c_transmission.STATUS_C_TRANSMISSIONShiftLeverPosition
#define NETC_RX_STATUS_C_TRANSMISSIONDriveModeSts_bit	 STATUS_C_TRANSMISSION.status_c_transmission.STATUS_C_TRANSMISSIONDriveModeSts
#define NETC_RX_STATUS_C_TRANSMISSIONShiftModeSts_bit	 STATUS_C_TRANSMISSION.status_c_transmission.STATUS_C_TRANSMISSIONShiftModeSts
#define NETC_RX_STATUS_C_TRANSMISSIONActualGearForDisplay_bit	 STATUS_C_TRANSMISSION.status_c_transmission.STATUS_C_TRANSMISSIONActualGearForDisplay

#define NETC_RX_VIN_0VIN_MSG_bit	 VIN_0.vin_0.VIN_0VIN_MSG
#define NETC_RX_VIN_0VIN_DATA_bit_0 VIN_0.vin_0.VIN_0VIN_DATA_0
#define NETC_RX_VIN_0VIN_DATA_bit_1 VIN_0.vin_0.VIN_0VIN_DATA_1
#define NETC_RX_VIN_0VIN_DATA_bit_2 VIN_0.vin_0.VIN_0VIN_DATA_2
#define NETC_RX_VIN_0VIN_DATA_bit_3 VIN_0.vin_0.VIN_0VIN_DATA_3
#define NETC_RX_VIN_0VIN_DATA_bit_4 VIN_0.vin_0.VIN_0VIN_DATA_4
#define NETC_RX_VIN_0VIN_DATA_bit_5 VIN_0.vin_0.VIN_0VIN_DATA_5
#define NETC_RX_VIN_0VIN_DATA_bit_6 VIN_0.vin_0.VIN_0VIN_DATA_6

#define NETC_RX_MOT3ACPressure_bit_0 MOT3.mot3.MOT3ACPressure_0
#define NETC_RX_MOT3ACPressure_bit_1 MOT3.mot3.MOT3ACPressure_1                                          
#define NETC_RX_MOT3ACPressure_bit   (vuint16)(((vuint16)(NETC_RX_MOT3ACPressure_bit_1) << 3)\
                                        | (NETC_RX_MOT3ACPressure_bit_0))
#define NETC_RX_MOT3AtmosphericPressure_byte	 MOT3.mot3.MOT3AtmosphericPressure
#define NETC_RX_MOT3FuelFilterHeaterReq_bit	 MOT3.mot3.MOT3FuelFilterHeaterReq
#define NETC_RX_MOT3EngDriveModeSts_bit	 MOT3.mot3.MOT3EngDriveModeSts

#define NETC_RX_ECM_1CHRG_FAIL_bit	 ECM_1.ecm_1.ECM_1CHRG_FAIL
#define NETC_RX_ECM_1SpStSwStat_bit	 ECM_1.ecm_1.ECM_1SpStSwStat

#define NETC_RX_STATUS_C_DTCMGenericFailSts_bit	 STATUS_C_DTCM.status_c_dtcm.STATUS_C_DTCMGenericFailSts
#define NETC_RX_STATUS_C_DTCMCurrentFailSts_bit	 STATUS_C_DTCM.status_c_dtcm.STATUS_C_DTCMCurrentFailSts
#define NETC_RX_STATUS_C_DTCMEOL_bit	 STATUS_C_DTCM.status_c_dtcm.STATUS_C_DTCMEOL
#define NETC_RX_STATUS_C_DTCMTerrainMudSandLmp_bit	 STATUS_C_DTCM.status_c_dtcm.STATUS_C_DTCMTerrainMudSandLmp
#define NETC_RX_STATUS_C_DTCMAWDNeutralLmp_bit	 STATUS_C_DTCM.status_c_dtcm.STATUS_C_DTCMAWDNeutralLmp
#define NETC_RX_STATUS_C_DTCMAWD4LowLmp_bit	 STATUS_C_DTCM.status_c_dtcm.STATUS_C_DTCMAWD4LowLmp
#define NETC_RX_STATUS_C_DTCMTerrainAutoLmp_bit	 STATUS_C_DTCM.status_c_dtcm.STATUS_C_DTCMTerrainAutoLmp
#define NETC_RX_STATUS_C_DTCMTerrainSportLmp_bit	 STATUS_C_DTCM.status_c_dtcm.STATUS_C_DTCMTerrainSportLmp
#define NETC_RX_STATUS_C_DTCMTerrainModeStat_bit	 STATUS_C_DTCM.status_c_dtcm.STATUS_C_DTCMTerrainModeStat
#define NETC_RX_STATUS_C_DTCMELockerLmp_bit	 STATUS_C_DTCM.status_c_dtcm.STATUS_C_DTCMELockerLmp
#define NETC_RX_STATUS_C_DTCMELockerStat_bit	 STATUS_C_DTCM.status_c_dtcm.STATUS_C_DTCMELockerStat
#define NETC_RX_STATUS_C_DTCMTerrainRockLmp_bit	 STATUS_C_DTCM.status_c_dtcm.STATUS_C_DTCMTerrainRockLmp
#define NETC_RX_STATUS_C_DTCMTerrainSnowLmp_bit	 STATUS_C_DTCM.status_c_dtcm.STATUS_C_DTCMTerrainSnowLmp
#define NETC_RX_STATUS_C_DTCMTerrain_HMI_Rq_bit	 STATUS_C_DTCM.status_c_dtcm.STATUS_C_DTCMTerrain_HMI_Rq

#define NETC_RX_DAS_A2FCW_Setting_bit	 DAS_A2.das_a2.DAS_A2FCW_Setting
#define NETC_RX_DAS_A2FCW_Brk_bit	 DAS_A2.das_a2.DAS_A2FCW_Brk

#define NETC_RX_PAM_2PAM_RF_CHIME_RQSts_bit	 PAM_2.pam_2.PAM_2PAM_RF_CHIME_RQSts
#define NETC_RX_PAM_2PAM_LF_CHIME_RQSts_bit	 PAM_2.pam_2.PAM_2PAM_LF_CHIME_RQSts
#define NETC_RX_PAM_2PAM_VOL_F_bit_0 PAM_2.pam_2.PAM_2PAM_VOL_F_0
#define NETC_RX_PAM_2PAM_VOL_F_bit_1 PAM_2.pam_2.PAM_2PAM_VOL_F_1                                          
#define NETC_RX_PAM_2PAM_VOL_F_bit   (vuint16)(((vuint16)(NETC_RX_PAM_2PAM_VOL_F_bit_1) << 1)\
                                        | (NETC_RX_PAM_2PAM_VOL_F_bit_0))

#define NETC_RX_EPB_A1EPBHoldSts_bit	 EPB_A1.epb_a1.EPB_A1EPBHoldSts

#define NETC_RX_DAS_A1DAS_CHIME_TYPSts_bit	 DAS_A1.das_a1.DAS_A1DAS_CHIME_TYPSts
#define NETC_RX_DAS_A1DAS_LF_CHIME_RQSts_bit	 DAS_A1.das_a1.DAS_A1DAS_LF_CHIME_RQSts
#define NETC_RX_DAS_A1DAS_RF_CHIME_RQSts_bit	 DAS_A1.das_a1.DAS_A1DAS_RF_CHIME_RQSts

#define NETC_RX_BSM_4ESP_DSBL_bit	 BSM_4.bsm_4.BSM_4ESP_DSBL
#define NETC_RX_BSM_4HILL_DES_STAT_bit	 BSM_4.bsm_4.BSM_4HILL_DES_STAT
#define NETC_RX_BSM_4SelectSpdSts_bit	 BSM_4.bsm_4.BSM_4SelectSpdSts
#define NETC_RX_BSM_4SelectSpdLmp_bit	 BSM_4.bsm_4.BSM_4SelectSpdLmp
#define NETC_RX_BSM_4HDCLmp_bit	 BSM_4.bsm_4.BSM_4HDCLmp

#define NETC_RX_BSM_2AutoVehHoldSts_bit	 BSM_2.bsm_2.BSM_2AutoVehHoldSts

#define NETC_RX_DTCM_A1AWDSysStat_bit	 DTCM_A1.dtcm_a1.DTCM_A1AWDSysStat

#define NETC_RX_SCShiftLeverPositionReqValidData_bit	 SC.sc.SCShiftLeverPositionReqValidData
#define NETC_RX_SCShiftLeverPositionReq_bit	 SC.sc.SCShiftLeverPositionReq

#define NETC_RX_ORC_YRS_DATALongAcceleration_bit_0 ORC_YRS_DATA.orc_yrs_data.ORC_YRS_DATALongAcceleration_0
#define NETC_RX_ORC_YRS_DATALongAcceleration_bit_1 ORC_YRS_DATA.orc_yrs_data.ORC_YRS_DATALongAcceleration_1                            
#define NETC_RX_ORC_YRS_DATALongAcceleration_bit   (vuint16)(((vuint16)(NETC_RX_ORC_YRS_DATALongAcceleration_bit_1) << 8)\
                                        | (NETC_RX_ORC_YRS_DATALongAcceleration_bit_0))
#define NETC_RX_ORC_YRS_DATAYawRate_bit_0 ORC_YRS_DATA.orc_yrs_data.ORC_YRS_DATAYawRate_0
#define NETC_RX_ORC_YRS_DATAYawRate_bit_1 ORC_YRS_DATA.orc_yrs_data.ORC_YRS_DATAYawRate_1                                     
#define NETC_RX_ORC_YRS_DATAYawRate_bit   (vuint16)(((vuint16)(NETC_RX_ORC_YRS_DATAYawRate_bit_1) << 4)\
                                        | (NETC_RX_ORC_YRS_DATAYawRate_bit_0))

#define NETC_RX_MOTGEAR2ESS_ENG_ST_bit	 MOTGEAR2.motgear2.MOTGEAR2ESS_ENG_ST

#define NETC_RX_MOT4StartRelayBCMCmd_bit	 MOT4.mot4.MOT4StartRelayBCMCmd
#define NETC_RX_MOT4CrankHold_bit	 MOT4.mot4.MOT4CrankHold
#define NETC_RX_MOT4EngineSts_bit	 MOT4.mot4.MOT4EngineSts

#define NETC_RX_BSM_YRS_DATALongAcceleration_Offset_byte	 BSM_YRS_DATA.bsm_yrs_data.BSM_YRS_DATALongAcceleration_Offset
#define NETC_RX_BSM_YRS_DATAYawRate_Offset_byte	 BSM_YRS_DATA.bsm_yrs_data.BSM_YRS_DATAYawRate_Offset

#define NETC_RX_ASR4LHRFastPulseCounter_byte_0 ASR4.asr4.ASR4LHRFastPulseCounter_0
#define NETC_RX_ASR4LHRFastPulseCounter_byte_1 ASR4.asr4.ASR4LHRFastPulseCounter_1                                
#define NETC_RX_ASR4LHRFastPulseCounter_byte   (vuint16)(((vuint16)(NETC_RX_ASR4LHRFastPulseCounter_byte_1) << 4)\
                                        | (NETC_RX_ASR4LHRFastPulseCounter_byte_0))
#define NETC_RX_ASR4RHRFastPulseCounterFailStS_bit	 ASR4.asr4.ASR4RHRFastPulseCounterFailStS
#define NETC_RX_ASR4LHRFastPulseCounterFailStS_bit	 ASR4.asr4.ASR4LHRFastPulseCounterFailStS
#define NETC_RX_ASR4RHRFastPulseCounter_byte_0 ASR4.asr4.ASR4RHRFastPulseCounter_0
#define NETC_RX_ASR4RHRFastPulseCounter_byte_1 ASR4.asr4.ASR4RHRFastPulseCounter_1                                
#define NETC_RX_ASR4RHRFastPulseCounter_byte   (vuint16)(((vuint16)(NETC_RX_ASR4RHRFastPulseCounter_byte_1) << 4)\
                                        | (NETC_RX_ASR4RHRFastPulseCounter_byte_0))
#define NETC_RX_ASR4LHFFastPulseCounter_byte_0 ASR4.asr4.ASR4LHFFastPulseCounter_0
#define NETC_RX_ASR4LHFFastPulseCounter_byte_1 ASR4.asr4.ASR4LHFFastPulseCounter_1                                
#define NETC_RX_ASR4LHFFastPulseCounter_byte   (vuint16)(((vuint16)(NETC_RX_ASR4LHFFastPulseCounter_byte_1) << 6)\
                                        | (NETC_RX_ASR4LHFFastPulseCounter_byte_0))
#define NETC_RX_ASR4RHFFastPulseCounter_byte_0 ASR4.asr4.ASR4RHFFastPulseCounter_0
#define NETC_RX_ASR4RHFFastPulseCounter_byte_1 ASR4.asr4.ASR4RHFFastPulseCounter_1                                
#define NETC_RX_ASR4RHFFastPulseCounter_byte   (vuint16)(((vuint16)(NETC_RX_ASR4RHFFastPulseCounter_byte_1) << 6)\
                                        | (NETC_RX_ASR4RHFFastPulseCounter_byte_0))
#define NETC_RX_ASR4RHFFastPulseCounterFailStS_bit	 ASR4.asr4.ASR4RHFFastPulseCounterFailStS
#define NETC_RX_ASR4LHFFastPulseCounterFailStS_bit	 ASR4.asr4.ASR4LHFFastPulseCounterFailStS

#define NETC_RX_ASR3VehicleSpeedVSOSigFailSts_bit	 ASR3.asr3.ASR3VehicleSpeedVSOSigFailSts
#define NETC_RX_ASR3VehicleSpeedVSOSig_bit_0 ASR3.asr3.ASR3VehicleSpeedVSOSig_0
#define NETC_RX_ASR3VehicleSpeedVSOSig_bit_1 ASR3.asr3.ASR3VehicleSpeedVSOSig_1                                  
#define NETC_RX_ASR3VehicleSpeedVSOSig_bit   (vuint16)(((vuint16)(NETC_RX_ASR3VehicleSpeedVSOSig_bit_1) << 8)\
                                        | (NETC_RX_ASR3VehicleSpeedVSOSig_bit_0))

#define NETC_RX_RFHUB_A4RFMemRq_bit	 RFHUB_A4.rfhub_a4.RFHUB_A4RFMemRq
#define NETC_RX_RFHUB_A4RF_FobNum_bit	 RFHUB_A4.rfhub_a4.RFHUB_A4RF_FobNum

#define NETC_RX_IPC_CFG_FeatureCFG_FeatureCntrl_byte	 IPC_CFG_Feature.ipc_cfg_feature.IPC_CFG_FeatureCFG_FeatureCntrl
#define NETC_RX_IPC_CFG_FeatureCFG_STAT_RQCntrl_bit	 IPC_CFG_Feature.ipc_cfg_feature.IPC_CFG_FeatureCFG_STAT_RQCntrl
#define NETC_RX_IPC_CFG_FeatureCFG_SETCntrl_byte	 IPC_CFG_Feature.ipc_cfg_feature.IPC_CFG_FeatureCFG_SETCntrl

#define NETC_RX_ESL_CODE_RESPONSEControlEncoding_byte	 ESL_CODE_RESPONSE.esl_code_response.ESL_CODE_RESPONSEControlEncoding
#define NETC_RX_ESL_CODE_RESPONSEMiniCryptGCode_bit_0 ESL_CODE_RESPONSE.esl_code_response.ESL_CODE_RESPONSEMiniCryptGCode_0
#define NETC_RX_ESL_CODE_RESPONSEMiniCryptGCode_bit_1 ESL_CODE_RESPONSE.esl_code_response.ESL_CODE_RESPONSEMiniCryptGCode_1                         
#define NETC_RX_ESL_CODE_RESPONSEMiniCryptGCode_bit   (vuint16)(((vuint16)(NETC_RX_ESL_CODE_RESPONSEMiniCryptGCode_bit_1) << 8)\
                                        | (NETC_RX_ESL_CODE_RESPONSEMiniCryptGCode_bit_0))
#define NETC_RX_ESL_CODE_RESPONSEESLFeedbackCmd_bit_0 ESL_CODE_RESPONSE.esl_code_response.ESL_CODE_RESPONSEESLFeedbackCmd_0
#define NETC_RX_ESL_CODE_RESPONSEESLFeedbackCmd_bit_1 ESL_CODE_RESPONSE.esl_code_response.ESL_CODE_RESPONSEESLFeedbackCmd_1                         
#define NETC_RX_ESL_CODE_RESPONSEESLFeedbackCmd_bit   (vuint16)(((vuint16)(NETC_RX_ESL_CODE_RESPONSEESLFeedbackCmd_bit_1) << 1)\
                                        | (NETC_RX_ESL_CODE_RESPONSEESLFeedbackCmd_bit_0))
#define NETC_RX_ESL_CODE_RESPONSECodeCheckSts_bit	 ESL_CODE_RESPONSE.esl_code_response.ESL_CODE_RESPONSECodeCheckSts
#define NETC_RX_ESL_CODE_RESPONSEESLUnlockFailSts_bit	 ESL_CODE_RESPONSE.esl_code_response.ESL_CODE_RESPONSEESLUnlockFailSts
#define NETC_RX_ESL_CODE_RESPONSEESLLockFailSts_bit	 ESL_CODE_RESPONSE.esl_code_response.ESL_CODE_RESPONSEESLLockFailSts

#define NETC_RX_IMMO_CODE_REQUESTControlEncoding_byte	 IMMO_CODE_REQUEST.immo_code_request.IMMO_CODE_REQUESTControlEncoding
#define NETC_RX_IMMO_CODE_REQUESTrnd_1_byte	 IMMO_CODE_REQUEST.immo_code_request.IMMO_CODE_REQUESTrnd_1
#define NETC_RX_IMMO_CODE_REQUESTrnd_2_byte	 IMMO_CODE_REQUEST.immo_code_request.IMMO_CODE_REQUESTrnd_2
#define NETC_RX_IMMO_CODE_REQUESTrnd_3_byte	 IMMO_CODE_REQUEST.immo_code_request.IMMO_CODE_REQUESTrnd_3
#define NETC_RX_IMMO_CODE_REQUESTrnd_4_byte	 IMMO_CODE_REQUEST.immo_code_request.IMMO_CODE_REQUESTrnd_4
#define NETC_RX_IMMO_CODE_REQUESTf1_1_byte	 IMMO_CODE_REQUEST.immo_code_request.IMMO_CODE_REQUESTf1_1
#define NETC_RX_IMMO_CODE_REQUESTf1_2_byte	 IMMO_CODE_REQUEST.immo_code_request.IMMO_CODE_REQUESTf1_2

#define NETC_RX_TRM_MKP_KEYFrameNumber_bit	 TRM_MKP_KEY.trm_mkp_key.TRM_MKP_KEYFrameNumber
#define NETC_RX_TRM_MKP_KEYTotalFrameNumber_bit	 TRM_MKP_KEY.trm_mkp_key.TRM_MKP_KEYTotalFrameNumber
#define NETC_RX_TRM_MKP_KEYDATA1_byte	 TRM_MKP_KEY.trm_mkp_key.TRM_MKP_KEYDATA1
#define NETC_RX_TRM_MKP_KEYDATA2_byte	 TRM_MKP_KEY.trm_mkp_key.TRM_MKP_KEYDATA2
#define NETC_RX_TRM_MKP_KEYDATA3_byte	 TRM_MKP_KEY.trm_mkp_key.TRM_MKP_KEYDATA3
#define NETC_RX_TRM_MKP_KEYDATA4_byte	 TRM_MKP_KEY.trm_mkp_key.TRM_MKP_KEYDATA4
#define NETC_RX_TRM_MKP_KEYDATA5_byte	 TRM_MKP_KEY.trm_mkp_key.TRM_MKP_KEYDATA5
#define NETC_RX_TRM_MKP_KEYDATA6_byte	 TRM_MKP_KEY.trm_mkp_key.TRM_MKP_KEYDATA6
#define NETC_RX_TRM_MKP_KEYDATA7_byte	 TRM_MKP_KEY.trm_mkp_key.TRM_MKP_KEYDATA7

#define NETC_RX_RFHUB_A3CsoPEEnableDisableSts_bit	 RFHUB_A3.rfhub_a3.RFHUB_A3CsoPEEnableDisableSts
#define NETC_RX_CAN_RFHUB_A3CsoPEEnableDisableSts_bit	 RDS4_0.rfhub_a3.RFHUB_A3CsoPEEnableDisableSts

#define NETC_RX_APPL_ECU_BCM_0APPL_ECU_BCM_bit_0 APPL_ECU_BCM_0.appl_ecu_bcm_0.APPL_ECU_BCM_0APPL_ECU_BCM_0
#define NETC_RX_APPL_ECU_BCM_0APPL_ECU_BCM_bit_1 APPL_ECU_BCM_0.appl_ecu_bcm_0.APPL_ECU_BCM_0APPL_ECU_BCM_1
#define NETC_RX_APPL_ECU_BCM_0APPL_ECU_BCM_bit_2 APPL_ECU_BCM_0.appl_ecu_bcm_0.APPL_ECU_BCM_0APPL_ECU_BCM_2
#define NETC_RX_APPL_ECU_BCM_0APPL_ECU_BCM_bit_3 APPL_ECU_BCM_0.appl_ecu_bcm_0.APPL_ECU_BCM_0APPL_ECU_BCM_3
#define NETC_RX_APPL_ECU_BCM_0APPL_ECU_BCM_bit_4 APPL_ECU_BCM_0.appl_ecu_bcm_0.APPL_ECU_BCM_0APPL_ECU_BCM_4
#define NETC_RX_APPL_ECU_BCM_0APPL_ECU_BCM_bit_5 APPL_ECU_BCM_0.appl_ecu_bcm_0.APPL_ECU_BCM_0APPL_ECU_BCM_5
#define NETC_RX_APPL_ECU_BCM_0APPL_ECU_BCM_bit_6 APPL_ECU_BCM_0.appl_ecu_bcm_0.APPL_ECU_BCM_0APPL_ECU_BCM_6
#define NETC_RX_APPL_ECU_BCM_0APPL_ECU_BCM_bit_7 APPL_ECU_BCM_0.appl_ecu_bcm_0.APPL_ECU_BCM_0APPL_ECU_BCM_7
#define NETC_RX_CAN_APPL_ECU_BCM_0APPL_ECU_BCM_bit_0 RDS5_0.appl_ecu_bcm_0.APPL_ECU_BCM_0APPL_ECU_BCM_0
#define NETC_RX_CAN_APPL_ECU_BCM_0APPL_ECU_BCM_bit_1 RDS5_0.appl_ecu_bcm_0.APPL_ECU_BCM_0APPL_ECU_BCM_1
#define NETC_RX_CAN_APPL_ECU_BCM_0APPL_ECU_BCM_bit_2 RDS5_0.appl_ecu_bcm_0.APPL_ECU_BCM_0APPL_ECU_BCM_2
#define NETC_RX_CAN_APPL_ECU_BCM_0APPL_ECU_BCM_bit_3 RDS5_0.appl_ecu_bcm_0.APPL_ECU_BCM_0APPL_ECU_BCM_3
#define NETC_RX_CAN_APPL_ECU_BCM_0APPL_ECU_BCM_bit_4 RDS5_0.appl_ecu_bcm_0.APPL_ECU_BCM_0APPL_ECU_BCM_4
#define NETC_RX_CAN_APPL_ECU_BCM_0APPL_ECU_BCM_bit_5 RDS5_0.appl_ecu_bcm_0.APPL_ECU_BCM_0APPL_ECU_BCM_5
#define NETC_RX_CAN_APPL_ECU_BCM_0APPL_ECU_BCM_bit_6 RDS5_0.appl_ecu_bcm_0.APPL_ECU_BCM_0APPL_ECU_BCM_6
#define NETC_RX_CAN_APPL_ECU_BCM_0APPL_ECU_BCM_bit_7 RDS5_0.appl_ecu_bcm_0.APPL_ECU_BCM_0APPL_ECU_BCM_7

#define NETC_RX_STATUS_C_SCCMGenericFailSts_bit	 STATUS_C_SCCM.status_c_sccm.STATUS_C_SCCMGenericFailSts
#define NETC_RX_STATUS_C_SCCMCurrentFailSts_bit	 STATUS_C_SCCM.status_c_sccm.STATUS_C_SCCMCurrentFailSts
#define NETC_RX_STATUS_C_SCCMEOL_bit	 STATUS_C_SCCM.status_c_sccm.STATUS_C_SCCMEOL
#define NETC_RX_CAN_STATUS_C_SCCMGenericFailSts_bit	 RDS6_0.status_c_sccm.STATUS_C_SCCMGenericFailSts
#define NETC_RX_CAN_STATUS_C_SCCMCurrentFailSts_bit	 RDS6_0.status_c_sccm.STATUS_C_SCCMCurrentFailSts
#define NETC_RX_CAN_STATUS_C_SCCMEOL_bit	 RDS6_0.status_c_sccm.STATUS_C_SCCMEOL

#define NETC_RX_STATUS_C_RFHMGenericFailSts_bit	 STATUS_C_RFHM.status_c_rfhm.STATUS_C_RFHMGenericFailSts
#define NETC_RX_STATUS_C_RFHMCurrentFailSts_bit	 STATUS_C_RFHM.status_c_rfhm.STATUS_C_RFHMCurrentFailSts
#define NETC_RX_STATUS_C_RFHMEOL_bit	 STATUS_C_RFHM.status_c_rfhm.STATUS_C_RFHMEOL
#define NETC_RX_STATUS_C_RFHMPowerModeSts_bit	 STATUS_C_RFHM.status_c_rfhm.STATUS_C_RFHMPowerModeSts
#define NETC_RX_CAN_STATUS_C_RFHMGenericFailSts_bit	 RDS7_0.status_c_rfhm.STATUS_C_RFHMGenericFailSts
#define NETC_RX_CAN_STATUS_C_RFHMCurrentFailSts_bit	 RDS7_0.status_c_rfhm.STATUS_C_RFHMCurrentFailSts
#define NETC_RX_CAN_STATUS_C_RFHMEOL_bit	 RDS7_0.status_c_rfhm.STATUS_C_RFHMEOL
#define NETC_RX_CAN_STATUS_C_RFHMPowerModeSts_bit	 RDS7_0.status_c_rfhm.STATUS_C_RFHMPowerModeSts

#define NETC_RX_IPC_A1FuelLvlLow_bit	 IPC_A1.ipc_a1.IPC_A1FuelLvlLow
#define NETC_RX_CAN_IPC_A1FuelLvlLow_bit	 RDS8_0.ipc_a1.IPC_A1FuelLvlLow

#define NETC_RX_STATUS_C_SPMGenericFailSts_bit	 STATUS_C_SPM.status_c_spm.STATUS_C_SPMGenericFailSts
#define NETC_RX_STATUS_C_SPMCurrentFailSts_bit	 STATUS_C_SPM.status_c_spm.STATUS_C_SPMCurrentFailSts
#define NETC_RX_STATUS_C_SPMEOL_bit	 STATUS_C_SPM.status_c_spm.STATUS_C_SPMEOL
#define NETC_RX_STATUS_C_SPMASBM_LedControlSts_bit	 STATUS_C_SPM.status_c_spm.STATUS_C_SPMASBM_LedControlSts
#define NETC_RX_CAN_STATUS_C_SPMGenericFailSts_bit	 RDS9_0.status_c_spm.STATUS_C_SPMGenericFailSts
#define NETC_RX_CAN_STATUS_C_SPMCurrentFailSts_bit	 RDS9_0.status_c_spm.STATUS_C_SPMCurrentFailSts
#define NETC_RX_CAN_STATUS_C_SPMEOL_bit	 RDS9_0.status_c_spm.STATUS_C_SPMEOL
#define NETC_RX_CAN_STATUS_C_SPMASBM_LedControlSts_bit	 RDS9_0.status_c_spm.STATUS_C_SPMASBM_LedControlSts

#define NETC_RX_STATUS_C_IPCGenericFailSts_bit	 STATUS_C_IPC.status_c_ipc.STATUS_C_IPCGenericFailSts
#define NETC_RX_STATUS_C_IPCCurrentFailSts_bit	 STATUS_C_IPC.status_c_ipc.STATUS_C_IPCCurrentFailSts
#define NETC_RX_STATUS_C_IPCEOL_bit	 STATUS_C_IPC.status_c_ipc.STATUS_C_IPCEOL
#define NETC_RX_CAN_STATUS_C_IPCGenericFailSts_bit	 RDS10_0.status_c_ipc.STATUS_C_IPCGenericFailSts
#define NETC_RX_CAN_STATUS_C_IPCCurrentFailSts_bit	 RDS10_0.status_c_ipc.STATUS_C_IPCCurrentFailSts
#define NETC_RX_CAN_STATUS_C_IPCEOL_bit	 RDS10_0.status_c_ipc.STATUS_C_IPCEOL

#define NETC_RX_STATUS_C_GSMGenericFailSts_bit	 STATUS_C_GSM.status_c_gsm.STATUS_C_GSMGenericFailSts
#define NETC_RX_STATUS_C_GSMCurrentFailSts_bit	 STATUS_C_GSM.status_c_gsm.STATUS_C_GSMCurrentFailSts
#define NETC_RX_STATUS_C_GSMEOL_bit	 STATUS_C_GSM.status_c_gsm.STATUS_C_GSMEOL
#define NETC_RX_CAN_STATUS_C_GSMGenericFailSts_bit	 RDS11_0.status_c_gsm.STATUS_C_GSMGenericFailSts
#define NETC_RX_CAN_STATUS_C_GSMCurrentFailSts_bit	 RDS11_0.status_c_gsm.STATUS_C_GSMCurrentFailSts
#define NETC_RX_CAN_STATUS_C_GSMEOL_bit	 RDS11_0.status_c_gsm.STATUS_C_GSMEOL

#define NETC_RX_STATUS_C_EPSGenericFailSts_bit	 STATUS_C_EPS.status_c_eps.STATUS_C_EPSGenericFailSts
#define NETC_RX_STATUS_C_EPSCurrentFailSts_bit	 STATUS_C_EPS.status_c_eps.STATUS_C_EPSCurrentFailSts
#define NETC_RX_STATUS_C_EPSEOL_bit	 STATUS_C_EPS.status_c_eps.STATUS_C_EPSEOL
#define NETC_RX_CAN_STATUS_C_EPSGenericFailSts_bit	 RDS12_0.status_c_eps.STATUS_C_EPSGenericFailSts
#define NETC_RX_CAN_STATUS_C_EPSCurrentFailSts_bit	 RDS12_0.status_c_eps.STATUS_C_EPSCurrentFailSts
#define NETC_RX_CAN_STATUS_C_EPSEOL_bit	 RDS12_0.status_c_eps.STATUS_C_EPSEOL

#define NETC_RX_STATUS_C_BSMGenericFailSts_bit	 STATUS_C_BSM.status_c_bsm.STATUS_C_BSMGenericFailSts
#define NETC_RX_STATUS_C_BSMCurrentFailSts_bit	 STATUS_C_BSM.status_c_bsm.STATUS_C_BSMCurrentFailSts
#define NETC_RX_STATUS_C_BSMEOL_bit	 STATUS_C_BSM.status_c_bsm.STATUS_C_BSMEOL
#define NETC_RX_STATUS_C_BSMDSTFailSts_bit	 STATUS_C_BSM.status_c_bsm.STATUS_C_BSMDSTFailSts
#define NETC_RX_STATUS_C_BSMVehicleSpeed_bit_0 STATUS_C_BSM.status_c_bsm.STATUS_C_BSMVehicleSpeed_0
#define NETC_RX_STATUS_C_BSMVehicleSpeed_bit_1 STATUS_C_BSM.status_c_bsm.STATUS_C_BSMVehicleSpeed_1                                
#define NETC_RX_STATUS_C_BSMVehicleSpeed_bit   (vuint16)(((vuint16)(NETC_RX_STATUS_C_BSMVehicleSpeed_bit_1) << 8)\
                                        | (NETC_RX_STATUS_C_BSMVehicleSpeed_bit_0))
#define NETC_RX_STATUS_C_BSMHHCustomerDisableSts_bit	 STATUS_C_BSM.status_c_bsm.STATUS_C_BSMHHCustomerDisableSts
#define NETC_RX_STATUS_C_BSMESCControlSts_bit	 STATUS_C_BSM.status_c_bsm.STATUS_C_BSMESCControlSts
#define NETC_RX_STATUS_C_BSMLHRPulseCounter_bit_0 STATUS_C_BSM.status_c_bsm.STATUS_C_BSMLHRPulseCounter_0
#define NETC_RX_STATUS_C_BSMLHRPulseCounter_bit_1 STATUS_C_BSM.status_c_bsm.STATUS_C_BSMLHRPulseCounter_1                             
#define NETC_RX_STATUS_C_BSMLHRPulseCounter_bit   (vuint16)(((vuint16)(NETC_RX_STATUS_C_BSMLHRPulseCounter_bit_1) << 8)\
                                        | (NETC_RX_STATUS_C_BSMLHRPulseCounter_bit_0))
#define NETC_RX_STATUS_C_BSMRHRPulseCounterFailSts_bit	 STATUS_C_BSM.status_c_bsm.STATUS_C_BSMRHRPulseCounterFailSts
#define NETC_RX_STATUS_C_BSMLHRPulseCounterFailSts_bit	 STATUS_C_BSM.status_c_bsm.STATUS_C_BSMLHRPulseCounterFailSts
#define NETC_RX_STATUS_C_BSMVehicleSpeedFailSts_bit	 STATUS_C_BSM.status_c_bsm.STATUS_C_BSMVehicleSpeedFailSts
#define NETC_RX_STATUS_C_BSMRHRPulseCounter_bit_0 STATUS_C_BSM.status_c_bsm.STATUS_C_BSMRHRPulseCounter_0
#define NETC_RX_STATUS_C_BSMRHRPulseCounter_bit_1 STATUS_C_BSM.status_c_bsm.STATUS_C_BSMRHRPulseCounter_1                             
#define NETC_RX_STATUS_C_BSMRHRPulseCounter_bit   (vuint16)(((vuint16)(NETC_RX_STATUS_C_BSMRHRPulseCounter_bit_1) << 8)\
                                        | (NETC_RX_STATUS_C_BSMRHRPulseCounter_bit_0))
#define NETC_RX_STATUS_C_BSMStopLightActivationFromNACReq_bit	 STATUS_C_BSM.status_c_bsm.STATUS_C_BSMStopLightActivationFromNACReq
#define NETC_RX_CAN_STATUS_C_BSMGenericFailSts_bit	 RDS13_0.status_c_bsm.STATUS_C_BSMGenericFailSts
#define NETC_RX_CAN_STATUS_C_BSMCurrentFailSts_bit	 RDS13_0.status_c_bsm.STATUS_C_BSMCurrentFailSts
#define NETC_RX_CAN_STATUS_C_BSMEOL_bit	 RDS13_0.status_c_bsm.STATUS_C_BSMEOL
#define NETC_RX_CAN_STATUS_C_BSMDSTFailSts_bit	 RDS13_0.status_c_bsm.STATUS_C_BSMDSTFailSts
#define NETC_RX_CAN_STATUS_C_BSMVehicleSpeed_bit_0 RDS13_0.status_c_bsm.STATUS_C_BSMVehicleSpeed_0
#define NETC_RX_CAN_STATUS_C_BSMVehicleSpeed_bit_1 RDS13_0.status_c_bsm.STATUS_C_BSMVehicleSpeed_1                            
#define NETC_RX_CAN_STATUS_C_BSMVehicleSpeed_bit   (vuint16)(((vuint16)(NETC_RX_CAN_STATUS_C_BSMVehicleSpeed_bit_1) << 8)\
                                        | (NETC_RX_CAN_STATUS_C_BSMVehicleSpeed_bit_0))
#define NETC_RX_CAN_STATUS_C_BSMHHCustomerDisableSts_bit	 RDS13_0.status_c_bsm.STATUS_C_BSMHHCustomerDisableSts
#define NETC_RX_CAN_STATUS_C_BSMESCControlSts_bit	 RDS13_0.status_c_bsm.STATUS_C_BSMESCControlSts
#define NETC_RX_CAN_STATUS_C_BSMLHRPulseCounter_bit_0 RDS13_0.status_c_bsm.STATUS_C_BSMLHRPulseCounter_0
#define NETC_RX_CAN_STATUS_C_BSMLHRPulseCounter_bit_1 RDS13_0.status_c_bsm.STATUS_C_BSMLHRPulseCounter_1                         
#define NETC_RX_CAN_STATUS_C_BSMLHRPulseCounter_bit   (vuint16)(((vuint16)(NETC_RX_CAN_STATUS_C_BSMLHRPulseCounter_bit_1) << 8)\
                                        | (NETC_RX_CAN_STATUS_C_BSMLHRPulseCounter_bit_0))
#define NETC_RX_CAN_STATUS_C_BSMRHRPulseCounterFailSts_bit	 RDS13_0.status_c_bsm.STATUS_C_BSMRHRPulseCounterFailSts
#define NETC_RX_CAN_STATUS_C_BSMLHRPulseCounterFailSts_bit	 RDS13_0.status_c_bsm.STATUS_C_BSMLHRPulseCounterFailSts
#define NETC_RX_CAN_STATUS_C_BSMVehicleSpeedFailSts_bit	 RDS13_0.status_c_bsm.STATUS_C_BSMVehicleSpeedFailSts
#define NETC_RX_CAN_STATUS_C_BSMRHRPulseCounter_bit_0 RDS13_0.status_c_bsm.STATUS_C_BSMRHRPulseCounter_0
#define NETC_RX_CAN_STATUS_C_BSMRHRPulseCounter_bit_1 RDS13_0.status_c_bsm.STATUS_C_BSMRHRPulseCounter_1                         
#define NETC_RX_CAN_STATUS_C_BSMRHRPulseCounter_bit   (vuint16)(((vuint16)(NETC_RX_CAN_STATUS_C_BSMRHRPulseCounter_bit_1) << 8)\
                                        | (NETC_RX_CAN_STATUS_C_BSMRHRPulseCounter_bit_0))
#define NETC_RX_CAN_STATUS_C_BSMStopLightActivationFromNACReq_bit	 RDS13_0.status_c_bsm.STATUS_C_BSMStopLightActivationFromNACReq

#define NETC_RX_STATUS_C_ECM2StartRelayECMFeedbackFault_bit	 STATUS_C_ECM2.status_c_ecm2.STATUS_C_ECM2StartRelayECMFeedbackFault
#define NETC_RX_STATUS_C_ECM2StartRelayBCMFeedbackFault_bit	 STATUS_C_ECM2.status_c_ecm2.STATUS_C_ECM2StartRelayBCMFeedbackFault
#define NETC_RX_STATUS_C_ECM2ColdStartLampIndication_bit_0 STATUS_C_ECM2.status_c_ecm2.STATUS_C_ECM2ColdStartLampIndication_0
#define NETC_RX_STATUS_C_ECM2ColdStartLampIndication_bit_1 STATUS_C_ECM2.status_c_ecm2.STATUS_C_ECM2ColdStartLampIndication_1                    
#define NETC_RX_STATUS_C_ECM2ColdStartLampIndication_bit   (vuint16)(((vuint16)(NETC_RX_STATUS_C_ECM2ColdStartLampIndication_bit_1) << 1)\
                                        | (NETC_RX_STATUS_C_ECM2ColdStartLampIndication_bit_0))
#define NETC_RX_STATUS_C_ECM2SAMInfo_bit	 STATUS_C_ECM2.status_c_ecm2.STATUS_C_ECM2SAMInfo
#define NETC_RX_CAN_STATUS_C_ECM2StartRelayECMFeedbackFault_bit	 RDS14_0.status_c_ecm2.STATUS_C_ECM2StartRelayECMFeedbackFault
#define NETC_RX_CAN_STATUS_C_ECM2StartRelayBCMFeedbackFault_bit	 RDS14_0.status_c_ecm2.STATUS_C_ECM2StartRelayBCMFeedbackFault
#define NETC_RX_CAN_STATUS_C_ECM2ColdStartLampIndication_bit_0 RDS14_0.status_c_ecm2.STATUS_C_ECM2ColdStartLampIndication_0
#define NETC_RX_CAN_STATUS_C_ECM2ColdStartLampIndication_bit_1 RDS14_0.status_c_ecm2.STATUS_C_ECM2ColdStartLampIndication_1                
#define NETC_RX_CAN_STATUS_C_ECM2ColdStartLampIndication_bit   (vuint16)(((vuint16)(NETC_RX_CAN_STATUS_C_ECM2ColdStartLampIndication_bit_1) << 1)\
                                        | (NETC_RX_CAN_STATUS_C_ECM2ColdStartLampIndication_bit_0))
#define NETC_RX_CAN_STATUS_C_ECM2SAMInfo_bit	 RDS14_0.status_c_ecm2.STATUS_C_ECM2SAMInfo

#define NETC_RX_STATUS_C_ECMGenericFailSts_bit	 STATUS_C_ECM.status_c_ecm.STATUS_C_ECMGenericFailSts
#define NETC_RX_STATUS_C_ECMCurrentFailSts_bit	 STATUS_C_ECM.status_c_ecm.STATUS_C_ECMCurrentFailSts
#define NETC_RX_STATUS_C_ECMEOL_bit	 STATUS_C_ECM.status_c_ecm.STATUS_C_ECMEOL
#define NETC_RX_STATUS_C_ECMD_Signal_bit	 STATUS_C_ECM.status_c_ecm.STATUS_C_ECMD_Signal
#define NETC_RX_STATUS_C_ECMFuelWaterPresentFailSts_bit	 STATUS_C_ECM.status_c_ecm.STATUS_C_ECMFuelWaterPresentFailSts
#define NETC_RX_STATUS_C_ECMGlowPlugLampSts_bit	 STATUS_C_ECM.status_c_ecm.STATUS_C_ECMGlowPlugLampSts
#define NETC_RX_STATUS_C_ECMFuelWaterPresentSts_bit	 STATUS_C_ECM.status_c_ecm.STATUS_C_ECMFuelWaterPresentSts
#define NETC_RX_STATUS_C_ECMOilPressureSts_bit	 STATUS_C_ECM.status_c_ecm.STATUS_C_ECMOilPressureSts
#define NETC_RX_STATUS_C_ECMEngineWaterTempWarningLightSts_bit	 STATUS_C_ECM.status_c_ecm.STATUS_C_ECMEngineWaterTempWarningLightSts
#define NETC_RX_STATUS_C_ECMEngineWaterTempFailSts_bit	 STATUS_C_ECM.status_c_ecm.STATUS_C_ECMEngineWaterTempFailSts
#define NETC_RX_STATUS_C_ECMEMSFailSts_bit	 STATUS_C_ECM.status_c_ecm.STATUS_C_ECMEMSFailSts
#define NETC_RX_STATUS_C_ECMCompressorSts_bit	 STATUS_C_ECM.status_c_ecm.STATUS_C_ECMCompressorSts
#define NETC_RX_STATUS_C_ECMEngineWaterTemp_byte	 STATUS_C_ECM.status_c_ecm.STATUS_C_ECMEngineWaterTemp
#define NETC_RX_STATUS_C_ECMEngineSpeed_byte	 STATUS_C_ECM.status_c_ecm.STATUS_C_ECMEngineSpeed
#define NETC_RX_STATUS_C_ECMReverseGearSts_bit	 STATUS_C_ECM.status_c_ecm.STATUS_C_ECMReverseGearSts
#define NETC_RX_STATUS_C_ECMEngineSpeedValidData_bit	 STATUS_C_ECM.status_c_ecm.STATUS_C_ECMEngineSpeedValidData
#define NETC_RX_STATUS_C_ECMDPFSts_bit	 STATUS_C_ECM.status_c_ecm.STATUS_C_ECMDPFSts
#define NETC_RX_CAN_STATUS_C_ECMGenericFailSts_bit	 RDS15_0.status_c_ecm.STATUS_C_ECMGenericFailSts
#define NETC_RX_CAN_STATUS_C_ECMCurrentFailSts_bit	 RDS15_0.status_c_ecm.STATUS_C_ECMCurrentFailSts
#define NETC_RX_CAN_STATUS_C_ECMEOL_bit	 RDS15_0.status_c_ecm.STATUS_C_ECMEOL
#define NETC_RX_CAN_STATUS_C_ECMD_Signal_bit	 RDS15_0.status_c_ecm.STATUS_C_ECMD_Signal
#define NETC_RX_CAN_STATUS_C_ECMFuelWaterPresentFailSts_bit	 RDS15_0.status_c_ecm.STATUS_C_ECMFuelWaterPresentFailSts
#define NETC_RX_CAN_STATUS_C_ECMGlowPlugLampSts_bit	 RDS15_0.status_c_ecm.STATUS_C_ECMGlowPlugLampSts
#define NETC_RX_CAN_STATUS_C_ECMFuelWaterPresentSts_bit	 RDS15_0.status_c_ecm.STATUS_C_ECMFuelWaterPresentSts
#define NETC_RX_CAN_STATUS_C_ECMOilPressureSts_bit	 RDS15_0.status_c_ecm.STATUS_C_ECMOilPressureSts
#define NETC_RX_CAN_STATUS_C_ECMEngineWaterTempWarningLightSts_bit	 RDS15_0.status_c_ecm.STATUS_C_ECMEngineWaterTempWarningLightSts
#define NETC_RX_CAN_STATUS_C_ECMEngineWaterTempFailSts_bit	 RDS15_0.status_c_ecm.STATUS_C_ECMEngineWaterTempFailSts
#define NETC_RX_CAN_STATUS_C_ECMEMSFailSts_bit	 RDS15_0.status_c_ecm.STATUS_C_ECMEMSFailSts
#define NETC_RX_CAN_STATUS_C_ECMCompressorSts_bit	 RDS15_0.status_c_ecm.STATUS_C_ECMCompressorSts
#define NETC_RX_CAN_STATUS_C_ECMEngineWaterTemp_byte	 RDS15_0.status_c_ecm.STATUS_C_ECMEngineWaterTemp
#define NETC_RX_CAN_STATUS_C_ECMEngineSpeed_byte	 RDS15_0.status_c_ecm.STATUS_C_ECMEngineSpeed
#define NETC_RX_CAN_STATUS_C_ECMReverseGearSts_bit	 RDS15_0.status_c_ecm.STATUS_C_ECMReverseGearSts
#define NETC_RX_CAN_STATUS_C_ECMEngineSpeedValidData_bit	 RDS15_0.status_c_ecm.STATUS_C_ECMEngineSpeedValidData
#define NETC_RX_CAN_STATUS_C_ECMDPFSts_bit	 RDS15_0.status_c_ecm.STATUS_C_ECMDPFSts

#define NETC_RX_STATUS_C_ORCGenericFailSts_bit	 STATUS_C_ORC.status_c_orc.STATUS_C_ORCGenericFailSts
#define NETC_RX_STATUS_C_ORCCurrentFailSts_bit	 STATUS_C_ORC.status_c_orc.STATUS_C_ORCCurrentFailSts
#define NETC_RX_STATUS_C_ORCEOL_bit	 STATUS_C_ORC.status_c_orc.STATUS_C_ORCEOL
#define NETC_RX_CAN_STATUS_C_ORCGenericFailSts_bit	 RDS16_0.status_c_orc.STATUS_C_ORCGenericFailSts
#define NETC_RX_CAN_STATUS_C_ORCCurrentFailSts_bit	 RDS16_0.status_c_orc.STATUS_C_ORCCurrentFailSts
#define NETC_RX_CAN_STATUS_C_ORCEOL_bit	 RDS16_0.status_c_orc.STATUS_C_ORCEOL

#define NETC_RX_RFHUB_A2RFFuncReq_bit	 RFHUB_A2.rfhub_a2.RFHUB_A2RFFuncReq
#define NETC_RX_RFHUB_A2RFReq_bit	 RFHUB_A2.rfhub_a2.RFHUB_A2RFReq
#define NETC_RX_RFHUB_A2RFCfgReq_bit	 RFHUB_A2.rfhub_a2.RFHUB_A2RFCfgReq
#define NETC_RX_RFHUB_A2RFFobNum_bit	 RFHUB_A2.rfhub_a2.RFHUB_A2RFFobNum
#define NETC_RX_RFHUB_A2FOBSearchResult_byte	 RFHUB_A2.rfhub_a2.RFHUB_A2FOBSearchResult
#define NETC_RX_CAN_RFHUB_A2RFFuncReq_bit	 RDS17_0.rfhub_a2.RFHUB_A2RFFuncReq
#define NETC_RX_CAN_RFHUB_A2RFReq_bit	 RDS17_0.rfhub_a2.RFHUB_A2RFReq
#define NETC_RX_CAN_RFHUB_A2RFCfgReq_bit	 RDS17_0.rfhub_a2.RFHUB_A2RFCfgReq
#define NETC_RX_CAN_RFHUB_A2RFFobNum_bit	 RDS17_0.rfhub_a2.RFHUB_A2RFFobNum
#define NETC_RX_CAN_RFHUB_A2FOBSearchResult_byte	 RDS17_0.rfhub_a2.RFHUB_A2FOBSearchResult

#define NETC_RX_RFHUB_A1CustKeyInIgnSts_bit	 RFHUB_A1.rfhub_a1.RFHUB_A1CustKeyInIgnSts
#define NETC_RX_RFHUB_A1IgnPosSts_bit	 RFHUB_A1.rfhub_a1.RFHUB_A1IgnPosSts
#define NETC_RX_RFHUB_A1ValidKeySts_bit	 RFHUB_A1.rfhub_a1.RFHUB_A1ValidKeySts
#define NETC_RX_CAN_RFHUB_A1CustKeyInIgnSts_bit	 RDS18_0.rfhub_a1.RFHUB_A1CustKeyInIgnSts
#define NETC_RX_CAN_RFHUB_A1IgnPosSts_bit	 RDS18_0.rfhub_a1.RFHUB_A1IgnPosSts
#define NETC_RX_CAN_RFHUB_A1ValidKeySts_bit	 RDS18_0.rfhub_a1.RFHUB_A1ValidKeySts

#define NETC_RX_PAM_1ASBM_LedControlSts_bit	 PAM_1.pam_1.PAM_1ASBM_LedControlSts
#define NETC_RX_PAM_1PAM_STATESts_bit	 PAM_1.pam_1.PAM_1PAM_STATESts
#define NETC_RX_PAM_1PAM_AlrRL_bit	 PAM_1.pam_1.PAM_1PAM_AlrRL
#define NETC_RX_PAM_1PAM_AlrRC_bit	 PAM_1.pam_1.PAM_1PAM_AlrRC
#define NETC_RX_PAM_1PAM_AlrRR_bit	 PAM_1.pam_1.PAM_1PAM_AlrRR
#define NETC_RX_PAM_1PAM_CHIME_TYPESts_bit_0 PAM_1.pam_1.PAM_1PAM_CHIME_TYPESts_0
#define NETC_RX_PAM_1PAM_CHIME_TYPESts_bit_1 PAM_1.pam_1.PAM_1PAM_CHIME_TYPESts_1                                  
#define NETC_RX_PAM_1PAM_CHIME_TYPESts_bit   (vuint16)(((vuint16)(NETC_RX_PAM_1PAM_CHIME_TYPESts_bit_1) << 1)\
                                        | (NETC_RX_PAM_1PAM_CHIME_TYPESts_bit_0))
#define NETC_RX_PAM_1PAM_LR_CHIME_RQSts_bit	 PAM_1.pam_1.PAM_1PAM_LR_CHIME_RQSts
#define NETC_RX_PAM_1PAM_RR_CHIME_RQSts_bit	 PAM_1.pam_1.PAM_1PAM_RR_CHIME_RQSts
#define NETC_RX_PAM_1PAM_CFG_STATSts_bit	 PAM_1.pam_1.PAM_1PAM_CFG_STATSts
#define NETC_RX_PAM_1PAM_CHIME_REP_RATESts_bit	 PAM_1.pam_1.PAM_1PAM_CHIME_REP_RATESts
#define NETC_RX_PAM_1RR_PAM_StopControlSts_bit	 PAM_1.pam_1.PAM_1RR_PAM_StopControlSts
#define NETC_RX_PAM_1PAM_VOL_R_bit	 PAM_1.pam_1.PAM_1PAM_VOL_R
#define NETC_RX_CAN_PAM_1ASBM_LedControlSts_bit	 RDS19_0.pam_1.PAM_1ASBM_LedControlSts
#define NETC_RX_CAN_PAM_1PAM_STATESts_bit	 RDS19_0.pam_1.PAM_1PAM_STATESts
#define NETC_RX_CAN_PAM_1PAM_AlrRL_bit	 RDS19_0.pam_1.PAM_1PAM_AlrRL
#define NETC_RX_CAN_PAM_1PAM_AlrRC_bit	 RDS19_0.pam_1.PAM_1PAM_AlrRC
#define NETC_RX_CAN_PAM_1PAM_AlrRR_bit	 RDS19_0.pam_1.PAM_1PAM_AlrRR
#define NETC_RX_CAN_PAM_1PAM_CHIME_TYPESts_bit_0 RDS19_0.pam_1.PAM_1PAM_CHIME_TYPESts_0
#define NETC_RX_CAN_PAM_1PAM_CHIME_TYPESts_bit_1 RDS19_0.pam_1.PAM_1PAM_CHIME_TYPESts_1                              
#define NETC_RX_CAN_PAM_1PAM_CHIME_TYPESts_bit   (vuint16)(((vuint16)(NETC_RX_CAN_PAM_1PAM_CHIME_TYPESts_bit_1) << 1)\
                                        | (NETC_RX_CAN_PAM_1PAM_CHIME_TYPESts_bit_0))
#define NETC_RX_CAN_PAM_1PAM_LR_CHIME_RQSts_bit	 RDS19_0.pam_1.PAM_1PAM_LR_CHIME_RQSts
#define NETC_RX_CAN_PAM_1PAM_RR_CHIME_RQSts_bit	 RDS19_0.pam_1.PAM_1PAM_RR_CHIME_RQSts
#define NETC_RX_CAN_PAM_1PAM_CFG_STATSts_bit	 RDS19_0.pam_1.PAM_1PAM_CFG_STATSts
#define NETC_RX_CAN_PAM_1PAM_CHIME_REP_RATESts_bit	 RDS19_0.pam_1.PAM_1PAM_CHIME_REP_RATESts
#define NETC_RX_CAN_PAM_1RR_PAM_StopControlSts_bit	 RDS19_0.pam_1.PAM_1RR_PAM_StopControlSts
#define NETC_RX_CAN_PAM_1PAM_VOL_R_bit	 RDS19_0.pam_1.PAM_1PAM_VOL_R

#define NETC_RX_WAKE_C_RFHMWakeRsn_bit	 WAKE_C_RFHM.wake_c_rfhm.WAKE_C_RFHMWakeRsn
#define NETC_RX_WAKE_C_RFHMWakeReq_bit	 WAKE_C_RFHM.wake_c_rfhm.WAKE_C_RFHMWakeReq
#define NETC_RX_WAKE_C_RFHMWakeCntr_byte	 WAKE_C_RFHM.wake_c_rfhm.WAKE_C_RFHMWakeCntr
#define NETC_RX_WAKE_C_RFHMWakeUpNodeAddress_byte	 WAKE_C_RFHM.wake_c_rfhm.WAKE_C_RFHMWakeUpNodeAddress
#define NETC_RX_CAN_WAKE_C_RFHMWakeRsn_bit	 RDS20_0.wake_c_rfhm.WAKE_C_RFHMWakeRsn
#define NETC_RX_CAN_WAKE_C_RFHMWakeReq_bit	 RDS20_0.wake_c_rfhm.WAKE_C_RFHMWakeReq
#define NETC_RX_CAN_WAKE_C_RFHMWakeCntr_byte	 RDS20_0.wake_c_rfhm.WAKE_C_RFHMWakeCntr
#define NETC_RX_CAN_WAKE_C_RFHMWakeUpNodeAddress_byte	 RDS20_0.wake_c_rfhm.WAKE_C_RFHMWakeUpNodeAddress

#define NETC_RX_WAKE_C_SCCMWakeRsn_bit	 WAKE_C_SCCM.wake_c_sccm.WAKE_C_SCCMWakeRsn
#define NETC_RX_WAKE_C_SCCMWakeReq_bit	 WAKE_C_SCCM.wake_c_sccm.WAKE_C_SCCMWakeReq
#define NETC_RX_WAKE_C_SCCMWakeCntr_byte	 WAKE_C_SCCM.wake_c_sccm.WAKE_C_SCCMWakeCntr
#define NETC_RX_WAKE_C_SCCMWakeUpNodeAddress_byte	 WAKE_C_SCCM.wake_c_sccm.WAKE_C_SCCMWakeUpNodeAddress
#define NETC_RX_CAN_WAKE_C_SCCMWakeRsn_bit	 RDS21_0.wake_c_sccm.WAKE_C_SCCMWakeRsn
#define NETC_RX_CAN_WAKE_C_SCCMWakeReq_bit	 RDS21_0.wake_c_sccm.WAKE_C_SCCMWakeReq
#define NETC_RX_CAN_WAKE_C_SCCMWakeCntr_byte	 RDS21_0.wake_c_sccm.WAKE_C_SCCMWakeCntr
#define NETC_RX_CAN_WAKE_C_SCCMWakeUpNodeAddress_byte	 RDS21_0.wake_c_sccm.WAKE_C_SCCMWakeUpNodeAddress

#define NETC_RX_WAKE_C_ESLWakeRsn_bit	 WAKE_C_ESL.wake_c_esl.WAKE_C_ESLWakeRsn
#define NETC_RX_WAKE_C_ESLWakeReq_bit	 WAKE_C_ESL.wake_c_esl.WAKE_C_ESLWakeReq
#define NETC_RX_WAKE_C_ESLWakeCntr_byte	 WAKE_C_ESL.wake_c_esl.WAKE_C_ESLWakeCntr
#define NETC_RX_WAKE_C_ESLWakeUpNodeAddress_byte	 WAKE_C_ESL.wake_c_esl.WAKE_C_ESLWakeUpNodeAddress
#define NETC_RX_CAN_WAKE_C_ESLWakeRsn_bit	 RDS22_0.wake_c_esl.WAKE_C_ESLWakeRsn
#define NETC_RX_CAN_WAKE_C_ESLWakeReq_bit	 RDS22_0.wake_c_esl.WAKE_C_ESLWakeReq
#define NETC_RX_CAN_WAKE_C_ESLWakeCntr_byte	 RDS22_0.wake_c_esl.WAKE_C_ESLWakeCntr
#define NETC_RX_CAN_WAKE_C_ESLWakeUpNodeAddress_byte	 RDS22_0.wake_c_esl.WAKE_C_ESLWakeUpNodeAddress

#define NETC_RX_WAKE_C_EPBWakeRsn_bit	 WAKE_C_EPB.wake_c_epb.WAKE_C_EPBWakeRsn
#define NETC_RX_WAKE_C_EPBWakeReq_bit	 WAKE_C_EPB.wake_c_epb.WAKE_C_EPBWakeReq
#define NETC_RX_WAKE_C_EPBWakeCntr_byte	 WAKE_C_EPB.wake_c_epb.WAKE_C_EPBWakeCntr
#define NETC_RX_WAKE_C_EPBWakeUpNodeAddress_byte	 WAKE_C_EPB.wake_c_epb.WAKE_C_EPBWakeUpNodeAddress
#define NETC_RX_CAN_WAKE_C_EPBWakeRsn_bit	 RDS23_0.wake_c_epb.WAKE_C_EPBWakeRsn
#define NETC_RX_CAN_WAKE_C_EPBWakeReq_bit	 RDS23_0.wake_c_epb.WAKE_C_EPBWakeReq
#define NETC_RX_CAN_WAKE_C_EPBWakeCntr_byte	 RDS23_0.wake_c_epb.WAKE_C_EPBWakeCntr
#define NETC_RX_CAN_WAKE_C_EPBWakeUpNodeAddress_byte	 RDS23_0.wake_c_epb.WAKE_C_EPBWakeUpNodeAddress

#define NETC_RX_WAKE_C_BSMWakeRsn_bit	 WAKE_C_BSM.wake_c_bsm.WAKE_C_BSMWakeRsn
#define NETC_RX_WAKE_C_BSMWakeReq_bit	 WAKE_C_BSM.wake_c_bsm.WAKE_C_BSMWakeReq
#define NETC_RX_WAKE_C_BSMWakeCntr_byte	 WAKE_C_BSM.wake_c_bsm.WAKE_C_BSMWakeCntr
#define NETC_RX_WAKE_C_BSMWakeUpNodeAddress_byte	 WAKE_C_BSM.wake_c_bsm.WAKE_C_BSMWakeUpNodeAddress
#define NETC_RX_CAN_WAKE_C_BSMWakeRsn_bit	 RDS24_0.wake_c_bsm.WAKE_C_BSMWakeRsn
#define NETC_RX_CAN_WAKE_C_BSMWakeReq_bit	 RDS24_0.wake_c_bsm.WAKE_C_BSMWakeReq
#define NETC_RX_CAN_WAKE_C_BSMWakeCntr_byte	 RDS24_0.wake_c_bsm.WAKE_C_BSMWakeCntr
#define NETC_RX_CAN_WAKE_C_BSMWakeUpNodeAddress_byte	 RDS24_0.wake_c_bsm.WAKE_C_BSMWakeUpNodeAddress

#define NETC_RX_MOT1GasPedalPosition_byte	 MOT1.mot1.MOT1GasPedalPosition
#define NETC_RX_MOT1GasPedalPositionValidData_bit	 MOT1.mot1.MOT1GasPedalPositionValidData
#define NETC_RX_CAN_MOT1GasPedalPosition_byte	 RDS25_0.mot1.MOT1GasPedalPosition
#define NETC_RX_CAN_MOT1GasPedalPositionValidData_bit	 RDS25_0.mot1.MOT1GasPedalPositionValidData

#define NETC_RX_GELwsAngle_bit_0 GE.ge.GELwsAngle_0
#define NETC_RX_GELwsAngle_bit_1 GE.ge.GELwsAngle_1                                              
#define NETC_RX_GELwsAngle_bit   (vuint16)(((vuint16)(NETC_RX_GELwsAngle_bit_1) << 8)\
                                        | (NETC_RX_GELwsAngle_bit_0))
#define NETC_RX_GELwsAngleValidData_bit	 GE.ge.GELwsAngleValidData
#define NETC_RX_GELWSFailSts_bit	 GE.ge.GELWSFailSts
#define NETC_RX_CAN_GELwsAngle_bit_0 RDS26_0.ge.GELwsAngle_0
#define NETC_RX_CAN_GELwsAngle_bit_1 RDS26_0.ge.GELwsAngle_1                                          
#define NETC_RX_CAN_GELwsAngle_bit   (vuint16)(((vuint16)(NETC_RX_CAN_GELwsAngle_bit_1) << 8)\
                                        | (NETC_RX_CAN_GELwsAngle_bit_0))
#define NETC_RX_CAN_GELwsAngleValidData_bit	 RDS26_0.ge.GELwsAngleValidData
#define NETC_RX_CAN_GELWSFailSts_bit	 RDS26_0.ge.GELWSFailSts

#define NETC_RX_TRM_CODE_RESPONSEControlEncoding_byte	 TRM_CODE_RESPONSE.trm_code_response.TRM_CODE_RESPONSEControlEncoding
#define NETC_RX_TRM_CODE_RESPONSEMiniCryptGCode_bit_0 TRM_CODE_RESPONSE.trm_code_response.TRM_CODE_RESPONSEMiniCryptGCode_0
#define NETC_RX_TRM_CODE_RESPONSEMiniCryptGCode_bit_1 TRM_CODE_RESPONSE.trm_code_response.TRM_CODE_RESPONSEMiniCryptGCode_1                         
#define NETC_RX_TRM_CODE_RESPONSEMiniCryptGCode_bit   (vuint16)(((vuint16)(NETC_RX_TRM_CODE_RESPONSEMiniCryptGCode_bit_1) << 8)\
                                        | (NETC_RX_TRM_CODE_RESPONSEMiniCryptGCode_bit_0))
#define NETC_RX_TRM_CODE_RESPONSECodeCheckSts_bit	 TRM_CODE_RESPONSE.trm_code_response.TRM_CODE_RESPONSECodeCheckSts
#define NETC_RX_TRM_CODE_RESPONSEProgrammedSts_bit	 TRM_CODE_RESPONSE.trm_code_response.TRM_CODE_RESPONSEProgrammedSts
#define NETC_RX_TRM_CODE_RESPONSETxpID_bit	 TRM_CODE_RESPONSE.trm_code_response.TRM_CODE_RESPONSETxpID
#define NETC_RX_TRM_CODE_RESPONSETxpFound_bit	 TRM_CODE_RESPONSE.trm_code_response.TRM_CODE_RESPONSETxpFound
#define NETC_RX_CAN_TRM_CODE_RESPONSEControlEncoding_byte	 RDS27_0.trm_code_response.TRM_CODE_RESPONSEControlEncoding
#define NETC_RX_CAN_TRM_CODE_RESPONSEMiniCryptGCode_bit_0 RDS27_0.trm_code_response.TRM_CODE_RESPONSEMiniCryptGCode_0
#define NETC_RX_CAN_TRM_CODE_RESPONSEMiniCryptGCode_bit_1 RDS27_0.trm_code_response.TRM_CODE_RESPONSEMiniCryptGCode_1                     
#define NETC_RX_CAN_TRM_CODE_RESPONSEMiniCryptGCode_bit   (vuint16)(((vuint16)(NETC_RX_CAN_TRM_CODE_RESPONSEMiniCryptGCode_bit_1) << 8)\
                                        | (NETC_RX_CAN_TRM_CODE_RESPONSEMiniCryptGCode_bit_0))
#define NETC_RX_CAN_TRM_CODE_RESPONSECodeCheckSts_bit	 RDS27_0.trm_code_response.TRM_CODE_RESPONSECodeCheckSts
#define NETC_RX_CAN_TRM_CODE_RESPONSEProgrammedSts_bit	 RDS27_0.trm_code_response.TRM_CODE_RESPONSEProgrammedSts
#define NETC_RX_CAN_TRM_CODE_RESPONSETxpID_bit	 RDS27_0.trm_code_response.TRM_CODE_RESPONSETxpID
#define NETC_RX_CAN_TRM_CODE_RESPONSETxpFound_bit	 RDS27_0.trm_code_response.TRM_CODE_RESPONSETxpFound

#define NETC_RX_ORC_A2IMPACTConfirm_bit	 ORC_A2.orc_a2.ORC_A2IMPACTConfirm
#define NETC_RX_ORC_A2IMPACTCommand_bit	 ORC_A2.orc_a2.ORC_A2IMPACTCommand
#define NETC_RX_CAN_ORC_A2IMPACTConfirm_bit	 RDS28_0.orc_a2.ORC_A2IMPACTConfirm
#define NETC_RX_CAN_ORC_A2IMPACTCommand_bit	 RDS28_0.orc_a2.ORC_A2IMPACTCommand

#define NETC_RX_CRO_BCMData_bit_0	 CRO_BCM.cro_bcm.CRO_BCMData_0
#define NETC_RX_CRO_BCMData_bit_1	 CRO_BCM.cro_bcm.CRO_BCMData_1
#define NETC_RX_CRO_BCMData_bit_2	 CRO_BCM.cro_bcm.CRO_BCMData_2
#define NETC_RX_CRO_BCMData_bit_3	 CRO_BCM.cro_bcm.CRO_BCMData_3
#define NETC_RX_CRO_BCMData_bit_4	 CRO_BCM.cro_bcm.CRO_BCMData_4
#define NETC_RX_CRO_BCMData_bit_5	 CRO_BCM.cro_bcm.CRO_BCMData_5
#define NETC_RX_CRO_BCMData_bit_6	 CRO_BCM.cro_bcm.CRO_BCMData_6
#define NETC_RX_CRO_BCMData_bit_7	 CRO_BCM.cro_bcm.CRO_BCMData_7

#define NETC_RX_CFG_DATA_CODE_RSP_RBSSDigit_10_bit	 CFG_DATA_CODE_RSP_RBSS.cfg_data_code_rsp_rbss.CFG_DATA_CODE_RSP_RBSSDigit_10
#define NETC_RX_CFG_DATA_CODE_RSP_RBSSDigit_11_bit	 CFG_DATA_CODE_RSP_RBSS.cfg_data_code_rsp_rbss.CFG_DATA_CODE_RSP_RBSSDigit_11
#define NETC_RX_CFG_DATA_CODE_RSP_RBSSDigit_08_bit	 CFG_DATA_CODE_RSP_RBSS.cfg_data_code_rsp_rbss.CFG_DATA_CODE_RSP_RBSSDigit_08
#define NETC_RX_CFG_DATA_CODE_RSP_RBSSDigit_09_bit	 CFG_DATA_CODE_RSP_RBSS.cfg_data_code_rsp_rbss.CFG_DATA_CODE_RSP_RBSSDigit_09
#define NETC_RX_CFG_DATA_CODE_RSP_RBSSDigit_06_bit	 CFG_DATA_CODE_RSP_RBSS.cfg_data_code_rsp_rbss.CFG_DATA_CODE_RSP_RBSSDigit_06
#define NETC_RX_CFG_DATA_CODE_RSP_RBSSDigit_07_bit	 CFG_DATA_CODE_RSP_RBSS.cfg_data_code_rsp_rbss.CFG_DATA_CODE_RSP_RBSSDigit_07
#define NETC_RX_CFG_DATA_CODE_RSP_RBSSDigit_04_bit	 CFG_DATA_CODE_RSP_RBSS.cfg_data_code_rsp_rbss.CFG_DATA_CODE_RSP_RBSSDigit_04
#define NETC_RX_CFG_DATA_CODE_RSP_RBSSDigit_05_bit	 CFG_DATA_CODE_RSP_RBSS.cfg_data_code_rsp_rbss.CFG_DATA_CODE_RSP_RBSSDigit_05
#define NETC_RX_CFG_DATA_CODE_RSP_RBSSDigit_02_bit	 CFG_DATA_CODE_RSP_RBSS.cfg_data_code_rsp_rbss.CFG_DATA_CODE_RSP_RBSSDigit_02
#define NETC_RX_CFG_DATA_CODE_RSP_RBSSDigit_03_bit	 CFG_DATA_CODE_RSP_RBSS.cfg_data_code_rsp_rbss.CFG_DATA_CODE_RSP_RBSSDigit_03
#define NETC_RX_CFG_DATA_CODE_RSP_RBSSDigit_01_bit	 CFG_DATA_CODE_RSP_RBSS.cfg_data_code_rsp_rbss.CFG_DATA_CODE_RSP_RBSSDigit_01

#define NETC_RX_CFG_DATA_CODE_RSP_LBSSDigit_10_bit	 CFG_DATA_CODE_RSP_LBSS.cfg_data_code_rsp_lbss.CFG_DATA_CODE_RSP_LBSSDigit_10
#define NETC_RX_CFG_DATA_CODE_RSP_LBSSDigit_11_bit	 CFG_DATA_CODE_RSP_LBSS.cfg_data_code_rsp_lbss.CFG_DATA_CODE_RSP_LBSSDigit_11
#define NETC_RX_CFG_DATA_CODE_RSP_LBSSDigit_08_bit	 CFG_DATA_CODE_RSP_LBSS.cfg_data_code_rsp_lbss.CFG_DATA_CODE_RSP_LBSSDigit_08
#define NETC_RX_CFG_DATA_CODE_RSP_LBSSDigit_09_bit	 CFG_DATA_CODE_RSP_LBSS.cfg_data_code_rsp_lbss.CFG_DATA_CODE_RSP_LBSSDigit_09
#define NETC_RX_CFG_DATA_CODE_RSP_LBSSDigit_06_bit	 CFG_DATA_CODE_RSP_LBSS.cfg_data_code_rsp_lbss.CFG_DATA_CODE_RSP_LBSSDigit_06
#define NETC_RX_CFG_DATA_CODE_RSP_LBSSDigit_07_bit	 CFG_DATA_CODE_RSP_LBSS.cfg_data_code_rsp_lbss.CFG_DATA_CODE_RSP_LBSSDigit_07
#define NETC_RX_CFG_DATA_CODE_RSP_LBSSDigit_04_bit	 CFG_DATA_CODE_RSP_LBSS.cfg_data_code_rsp_lbss.CFG_DATA_CODE_RSP_LBSSDigit_04
#define NETC_RX_CFG_DATA_CODE_RSP_LBSSDigit_05_bit	 CFG_DATA_CODE_RSP_LBSS.cfg_data_code_rsp_lbss.CFG_DATA_CODE_RSP_LBSSDigit_05
#define NETC_RX_CFG_DATA_CODE_RSP_LBSSDigit_02_bit	 CFG_DATA_CODE_RSP_LBSS.cfg_data_code_rsp_lbss.CFG_DATA_CODE_RSP_LBSSDigit_02
#define NETC_RX_CFG_DATA_CODE_RSP_LBSSDigit_03_bit	 CFG_DATA_CODE_RSP_LBSS.cfg_data_code_rsp_lbss.CFG_DATA_CODE_RSP_LBSSDigit_03
#define NETC_RX_CFG_DATA_CODE_RSP_LBSSDigit_01_bit	 CFG_DATA_CODE_RSP_LBSS.cfg_data_code_rsp_lbss.CFG_DATA_CODE_RSP_LBSSDigit_01

#define NETC_RX_CFG_DATA_CODE_RSP_ICSDigit_10_bit	 CFG_DATA_CODE_RSP_ICS.cfg_data_code_rsp_ics.CFG_DATA_CODE_RSP_ICSDigit_10
#define NETC_RX_CFG_DATA_CODE_RSP_ICSDigit_11_bit	 CFG_DATA_CODE_RSP_ICS.cfg_data_code_rsp_ics.CFG_DATA_CODE_RSP_ICSDigit_11
#define NETC_RX_CFG_DATA_CODE_RSP_ICSDigit_08_bit	 CFG_DATA_CODE_RSP_ICS.cfg_data_code_rsp_ics.CFG_DATA_CODE_RSP_ICSDigit_08
#define NETC_RX_CFG_DATA_CODE_RSP_ICSDigit_09_bit	 CFG_DATA_CODE_RSP_ICS.cfg_data_code_rsp_ics.CFG_DATA_CODE_RSP_ICSDigit_09
#define NETC_RX_CFG_DATA_CODE_RSP_ICSDigit_06_bit	 CFG_DATA_CODE_RSP_ICS.cfg_data_code_rsp_ics.CFG_DATA_CODE_RSP_ICSDigit_06
#define NETC_RX_CFG_DATA_CODE_RSP_ICSDigit_07_bit	 CFG_DATA_CODE_RSP_ICS.cfg_data_code_rsp_ics.CFG_DATA_CODE_RSP_ICSDigit_07
#define NETC_RX_CFG_DATA_CODE_RSP_ICSDigit_04_bit	 CFG_DATA_CODE_RSP_ICS.cfg_data_code_rsp_ics.CFG_DATA_CODE_RSP_ICSDigit_04
#define NETC_RX_CFG_DATA_CODE_RSP_ICSDigit_05_bit	 CFG_DATA_CODE_RSP_ICS.cfg_data_code_rsp_ics.CFG_DATA_CODE_RSP_ICSDigit_05
#define NETC_RX_CFG_DATA_CODE_RSP_ICSDigit_02_bit	 CFG_DATA_CODE_RSP_ICS.cfg_data_code_rsp_ics.CFG_DATA_CODE_RSP_ICSDigit_02
#define NETC_RX_CFG_DATA_CODE_RSP_ICSDigit_03_bit	 CFG_DATA_CODE_RSP_ICS.cfg_data_code_rsp_ics.CFG_DATA_CODE_RSP_ICSDigit_03
#define NETC_RX_CFG_DATA_CODE_RSP_ICSDigit_01_bit	 CFG_DATA_CODE_RSP_ICS.cfg_data_code_rsp_ics.CFG_DATA_CODE_RSP_ICSDigit_01

#define NETC_RX_CFG_DATA_CODE_RSP_CSWMDigit_10_bit	 CFG_DATA_CODE_RSP_CSWM.cfg_data_code_rsp_cswm.CFG_DATA_CODE_RSP_CSWMDigit_10
#define NETC_RX_CFG_DATA_CODE_RSP_CSWMDigit_11_bit	 CFG_DATA_CODE_RSP_CSWM.cfg_data_code_rsp_cswm.CFG_DATA_CODE_RSP_CSWMDigit_11
#define NETC_RX_CFG_DATA_CODE_RSP_CSWMDigit_08_bit	 CFG_DATA_CODE_RSP_CSWM.cfg_data_code_rsp_cswm.CFG_DATA_CODE_RSP_CSWMDigit_08
#define NETC_RX_CFG_DATA_CODE_RSP_CSWMDigit_09_bit	 CFG_DATA_CODE_RSP_CSWM.cfg_data_code_rsp_cswm.CFG_DATA_CODE_RSP_CSWMDigit_09
#define NETC_RX_CFG_DATA_CODE_RSP_CSWMDigit_06_bit	 CFG_DATA_CODE_RSP_CSWM.cfg_data_code_rsp_cswm.CFG_DATA_CODE_RSP_CSWMDigit_06
#define NETC_RX_CFG_DATA_CODE_RSP_CSWMDigit_07_bit	 CFG_DATA_CODE_RSP_CSWM.cfg_data_code_rsp_cswm.CFG_DATA_CODE_RSP_CSWMDigit_07
#define NETC_RX_CFG_DATA_CODE_RSP_CSWMDigit_04_bit	 CFG_DATA_CODE_RSP_CSWM.cfg_data_code_rsp_cswm.CFG_DATA_CODE_RSP_CSWMDigit_04
#define NETC_RX_CFG_DATA_CODE_RSP_CSWMDigit_05_bit	 CFG_DATA_CODE_RSP_CSWM.cfg_data_code_rsp_cswm.CFG_DATA_CODE_RSP_CSWMDigit_05
#define NETC_RX_CFG_DATA_CODE_RSP_CSWMDigit_02_bit	 CFG_DATA_CODE_RSP_CSWM.cfg_data_code_rsp_cswm.CFG_DATA_CODE_RSP_CSWMDigit_02
#define NETC_RX_CFG_DATA_CODE_RSP_CSWMDigit_03_bit	 CFG_DATA_CODE_RSP_CSWM.cfg_data_code_rsp_cswm.CFG_DATA_CODE_RSP_CSWMDigit_03
#define NETC_RX_CFG_DATA_CODE_RSP_CSWMDigit_01_bit	 CFG_DATA_CODE_RSP_CSWM.cfg_data_code_rsp_cswm.CFG_DATA_CODE_RSP_CSWMDigit_01

#define NETC_RX_CFG_DATA_CODE_RSP_CDMDigit_10_bit	 CFG_DATA_CODE_RSP_CDM.cfg_data_code_rsp_cdm.CFG_DATA_CODE_RSP_CDMDigit_10
#define NETC_RX_CFG_DATA_CODE_RSP_CDMDigit_11_bit	 CFG_DATA_CODE_RSP_CDM.cfg_data_code_rsp_cdm.CFG_DATA_CODE_RSP_CDMDigit_11
#define NETC_RX_CFG_DATA_CODE_RSP_CDMDigit_08_bit	 CFG_DATA_CODE_RSP_CDM.cfg_data_code_rsp_cdm.CFG_DATA_CODE_RSP_CDMDigit_08
#define NETC_RX_CFG_DATA_CODE_RSP_CDMDigit_09_bit	 CFG_DATA_CODE_RSP_CDM.cfg_data_code_rsp_cdm.CFG_DATA_CODE_RSP_CDMDigit_09
#define NETC_RX_CFG_DATA_CODE_RSP_CDMDigit_06_bit	 CFG_DATA_CODE_RSP_CDM.cfg_data_code_rsp_cdm.CFG_DATA_CODE_RSP_CDMDigit_06
#define NETC_RX_CFG_DATA_CODE_RSP_CDMDigit_07_bit	 CFG_DATA_CODE_RSP_CDM.cfg_data_code_rsp_cdm.CFG_DATA_CODE_RSP_CDMDigit_07
#define NETC_RX_CFG_DATA_CODE_RSP_CDMDigit_04_bit	 CFG_DATA_CODE_RSP_CDM.cfg_data_code_rsp_cdm.CFG_DATA_CODE_RSP_CDMDigit_04
#define NETC_RX_CFG_DATA_CODE_RSP_CDMDigit_05_bit	 CFG_DATA_CODE_RSP_CDM.cfg_data_code_rsp_cdm.CFG_DATA_CODE_RSP_CDMDigit_05
#define NETC_RX_CFG_DATA_CODE_RSP_CDMDigit_02_bit	 CFG_DATA_CODE_RSP_CDM.cfg_data_code_rsp_cdm.CFG_DATA_CODE_RSP_CDMDigit_02
#define NETC_RX_CFG_DATA_CODE_RSP_CDMDigit_03_bit	 CFG_DATA_CODE_RSP_CDM.cfg_data_code_rsp_cdm.CFG_DATA_CODE_RSP_CDMDigit_03
#define NETC_RX_CFG_DATA_CODE_RSP_CDMDigit_01_bit	 CFG_DATA_CODE_RSP_CDM.cfg_data_code_rsp_cdm.CFG_DATA_CODE_RSP_CDMDigit_01

#define NETC_RX_CFG_DATA_CODE_RSP_AMPDigit_10_bit	 CFG_DATA_CODE_RSP_AMP.cfg_data_code_rsp_amp.CFG_DATA_CODE_RSP_AMPDigit_10
#define NETC_RX_CFG_DATA_CODE_RSP_AMPDigit_11_bit	 CFG_DATA_CODE_RSP_AMP.cfg_data_code_rsp_amp.CFG_DATA_CODE_RSP_AMPDigit_11
#define NETC_RX_CFG_DATA_CODE_RSP_AMPDigit_08_bit	 CFG_DATA_CODE_RSP_AMP.cfg_data_code_rsp_amp.CFG_DATA_CODE_RSP_AMPDigit_08
#define NETC_RX_CFG_DATA_CODE_RSP_AMPDigit_09_bit	 CFG_DATA_CODE_RSP_AMP.cfg_data_code_rsp_amp.CFG_DATA_CODE_RSP_AMPDigit_09
#define NETC_RX_CFG_DATA_CODE_RSP_AMPDigit_06_bit	 CFG_DATA_CODE_RSP_AMP.cfg_data_code_rsp_amp.CFG_DATA_CODE_RSP_AMPDigit_06
#define NETC_RX_CFG_DATA_CODE_RSP_AMPDigit_07_bit	 CFG_DATA_CODE_RSP_AMP.cfg_data_code_rsp_amp.CFG_DATA_CODE_RSP_AMPDigit_07
#define NETC_RX_CFG_DATA_CODE_RSP_AMPDigit_04_bit	 CFG_DATA_CODE_RSP_AMP.cfg_data_code_rsp_amp.CFG_DATA_CODE_RSP_AMPDigit_04
#define NETC_RX_CFG_DATA_CODE_RSP_AMPDigit_05_bit	 CFG_DATA_CODE_RSP_AMP.cfg_data_code_rsp_amp.CFG_DATA_CODE_RSP_AMPDigit_05
#define NETC_RX_CFG_DATA_CODE_RSP_AMPDigit_02_bit	 CFG_DATA_CODE_RSP_AMP.cfg_data_code_rsp_amp.CFG_DATA_CODE_RSP_AMPDigit_02
#define NETC_RX_CFG_DATA_CODE_RSP_AMPDigit_03_bit	 CFG_DATA_CODE_RSP_AMP.cfg_data_code_rsp_amp.CFG_DATA_CODE_RSP_AMPDigit_03
#define NETC_RX_CFG_DATA_CODE_RSP_AMPDigit_01_bit	 CFG_DATA_CODE_RSP_AMP.cfg_data_code_rsp_amp.CFG_DATA_CODE_RSP_AMPDigit_01

#define NETC_RX_CFG_DATA_CODE_RSP_ETMDigit10_bit	 CFG_DATA_CODE_RSP_ETM.cfg_data_code_rsp_etm.CFG_DATA_CODE_RSP_ETMDigit10
#define NETC_RX_CFG_DATA_CODE_RSP_ETMDigit11_bit	 CFG_DATA_CODE_RSP_ETM.cfg_data_code_rsp_etm.CFG_DATA_CODE_RSP_ETMDigit11
#define NETC_RX_CFG_DATA_CODE_RSP_ETMDigit08_bit	 CFG_DATA_CODE_RSP_ETM.cfg_data_code_rsp_etm.CFG_DATA_CODE_RSP_ETMDigit08
#define NETC_RX_CFG_DATA_CODE_RSP_ETMDigit09_bit	 CFG_DATA_CODE_RSP_ETM.cfg_data_code_rsp_etm.CFG_DATA_CODE_RSP_ETMDigit09
#define NETC_RX_CFG_DATA_CODE_RSP_ETMDigit06_bit	 CFG_DATA_CODE_RSP_ETM.cfg_data_code_rsp_etm.CFG_DATA_CODE_RSP_ETMDigit06
#define NETC_RX_CFG_DATA_CODE_RSP_ETMDigit07_bit	 CFG_DATA_CODE_RSP_ETM.cfg_data_code_rsp_etm.CFG_DATA_CODE_RSP_ETMDigit07
#define NETC_RX_CFG_DATA_CODE_RSP_ETMDigit04_bit	 CFG_DATA_CODE_RSP_ETM.cfg_data_code_rsp_etm.CFG_DATA_CODE_RSP_ETMDigit04
#define NETC_RX_CFG_DATA_CODE_RSP_ETMDigit05_bit	 CFG_DATA_CODE_RSP_ETM.cfg_data_code_rsp_etm.CFG_DATA_CODE_RSP_ETMDigit05
#define NETC_RX_CFG_DATA_CODE_RSP_ETMDigit02_bit	 CFG_DATA_CODE_RSP_ETM.cfg_data_code_rsp_etm.CFG_DATA_CODE_RSP_ETMDigit02
#define NETC_RX_CFG_DATA_CODE_RSP_ETMDigit03_bit	 CFG_DATA_CODE_RSP_ETM.cfg_data_code_rsp_etm.CFG_DATA_CODE_RSP_ETMDigit03
#define NETC_RX_CFG_DATA_CODE_RSP_ETMDigit01_bit	 CFG_DATA_CODE_RSP_ETM.cfg_data_code_rsp_etm.CFG_DATA_CODE_RSP_ETMDigit01

#define NETC_RX_CFG_DATA_CODE_RSP_DSMDigit_10_bit	 CFG_DATA_CODE_RSP_DSM.cfg_data_code_rsp_dsm.CFG_DATA_CODE_RSP_DSMDigit_10
#define NETC_RX_CFG_DATA_CODE_RSP_DSMDigit_11_bit	 CFG_DATA_CODE_RSP_DSM.cfg_data_code_rsp_dsm.CFG_DATA_CODE_RSP_DSMDigit_11
#define NETC_RX_CFG_DATA_CODE_RSP_DSMDigit_08_bit	 CFG_DATA_CODE_RSP_DSM.cfg_data_code_rsp_dsm.CFG_DATA_CODE_RSP_DSMDigit_08
#define NETC_RX_CFG_DATA_CODE_RSP_DSMDigit_09_bit	 CFG_DATA_CODE_RSP_DSM.cfg_data_code_rsp_dsm.CFG_DATA_CODE_RSP_DSMDigit_09
#define NETC_RX_CFG_DATA_CODE_RSP_DSMDigit_06_bit	 CFG_DATA_CODE_RSP_DSM.cfg_data_code_rsp_dsm.CFG_DATA_CODE_RSP_DSMDigit_06
#define NETC_RX_CFG_DATA_CODE_RSP_DSMDigit_07_bit	 CFG_DATA_CODE_RSP_DSM.cfg_data_code_rsp_dsm.CFG_DATA_CODE_RSP_DSMDigit_07
#define NETC_RX_CFG_DATA_CODE_RSP_DSMDigit_04_bit	 CFG_DATA_CODE_RSP_DSM.cfg_data_code_rsp_dsm.CFG_DATA_CODE_RSP_DSMDigit_04
#define NETC_RX_CFG_DATA_CODE_RSP_DSMDigit_05_bit	 CFG_DATA_CODE_RSP_DSM.cfg_data_code_rsp_dsm.CFG_DATA_CODE_RSP_DSMDigit_05
#define NETC_RX_CFG_DATA_CODE_RSP_DSMDigit_02_bit	 CFG_DATA_CODE_RSP_DSM.cfg_data_code_rsp_dsm.CFG_DATA_CODE_RSP_DSMDigit_02
#define NETC_RX_CFG_DATA_CODE_RSP_DSMDigit_03_bit	 CFG_DATA_CODE_RSP_DSM.cfg_data_code_rsp_dsm.CFG_DATA_CODE_RSP_DSMDigit_03
#define NETC_RX_CFG_DATA_CODE_RSP_DSMDigit_01_bit	 CFG_DATA_CODE_RSP_DSM.cfg_data_code_rsp_dsm.CFG_DATA_CODE_RSP_DSMDigit_01

#define NETC_RX_CFG_DATA_CODE_RSP_PDMDigit_10_bit	 CFG_DATA_CODE_RSP_PDM.cfg_data_code_rsp_pdm.CFG_DATA_CODE_RSP_PDMDigit_10
#define NETC_RX_CFG_DATA_CODE_RSP_PDMDigit_11_bit	 CFG_DATA_CODE_RSP_PDM.cfg_data_code_rsp_pdm.CFG_DATA_CODE_RSP_PDMDigit_11
#define NETC_RX_CFG_DATA_CODE_RSP_PDMDigit_08_bit	 CFG_DATA_CODE_RSP_PDM.cfg_data_code_rsp_pdm.CFG_DATA_CODE_RSP_PDMDigit_08
#define NETC_RX_CFG_DATA_CODE_RSP_PDMDigit_09_bit	 CFG_DATA_CODE_RSP_PDM.cfg_data_code_rsp_pdm.CFG_DATA_CODE_RSP_PDMDigit_09
#define NETC_RX_CFG_DATA_CODE_RSP_PDMDigit_06_bit	 CFG_DATA_CODE_RSP_PDM.cfg_data_code_rsp_pdm.CFG_DATA_CODE_RSP_PDMDigit_06
#define NETC_RX_CFG_DATA_CODE_RSP_PDMDigit_07_bit	 CFG_DATA_CODE_RSP_PDM.cfg_data_code_rsp_pdm.CFG_DATA_CODE_RSP_PDMDigit_07
#define NETC_RX_CFG_DATA_CODE_RSP_PDMDigit_04_bit	 CFG_DATA_CODE_RSP_PDM.cfg_data_code_rsp_pdm.CFG_DATA_CODE_RSP_PDMDigit_04
#define NETC_RX_CFG_DATA_CODE_RSP_PDMDigit_05_bit	 CFG_DATA_CODE_RSP_PDM.cfg_data_code_rsp_pdm.CFG_DATA_CODE_RSP_PDMDigit_05
#define NETC_RX_CFG_DATA_CODE_RSP_PDMDigit_02_bit	 CFG_DATA_CODE_RSP_PDM.cfg_data_code_rsp_pdm.CFG_DATA_CODE_RSP_PDMDigit_02
#define NETC_RX_CFG_DATA_CODE_RSP_PDMDigit_03_bit	 CFG_DATA_CODE_RSP_PDM.cfg_data_code_rsp_pdm.CFG_DATA_CODE_RSP_PDMDigit_03
#define NETC_RX_CFG_DATA_CODE_RSP_PDMDigit_01_bit	 CFG_DATA_CODE_RSP_PDM.cfg_data_code_rsp_pdm.CFG_DATA_CODE_RSP_PDMDigit_01

#define NETC_RX_CFG_DATA_CODE_RSP_ECCDigit_10_bit	 CFG_DATA_CODE_RSP_ECC.cfg_data_code_rsp_ecc.CFG_DATA_CODE_RSP_ECCDigit_10
#define NETC_RX_CFG_DATA_CODE_RSP_ECCDigit_11_bit	 CFG_DATA_CODE_RSP_ECC.cfg_data_code_rsp_ecc.CFG_DATA_CODE_RSP_ECCDigit_11
#define NETC_RX_CFG_DATA_CODE_RSP_ECCDigit_08_bit	 CFG_DATA_CODE_RSP_ECC.cfg_data_code_rsp_ecc.CFG_DATA_CODE_RSP_ECCDigit_08
#define NETC_RX_CFG_DATA_CODE_RSP_ECCDigit_09_bit	 CFG_DATA_CODE_RSP_ECC.cfg_data_code_rsp_ecc.CFG_DATA_CODE_RSP_ECCDigit_09
#define NETC_RX_CFG_DATA_CODE_RSP_ECCDigit_06_bit	 CFG_DATA_CODE_RSP_ECC.cfg_data_code_rsp_ecc.CFG_DATA_CODE_RSP_ECCDigit_06
#define NETC_RX_CFG_DATA_CODE_RSP_ECCDigit_07_bit	 CFG_DATA_CODE_RSP_ECC.cfg_data_code_rsp_ecc.CFG_DATA_CODE_RSP_ECCDigit_07
#define NETC_RX_CFG_DATA_CODE_RSP_ECCDigit_04_bit	 CFG_DATA_CODE_RSP_ECC.cfg_data_code_rsp_ecc.CFG_DATA_CODE_RSP_ECCDigit_04
#define NETC_RX_CFG_DATA_CODE_RSP_ECCDigit_05_bit	 CFG_DATA_CODE_RSP_ECC.cfg_data_code_rsp_ecc.CFG_DATA_CODE_RSP_ECCDigit_05
#define NETC_RX_CFG_DATA_CODE_RSP_ECCDigit_02_bit	 CFG_DATA_CODE_RSP_ECC.cfg_data_code_rsp_ecc.CFG_DATA_CODE_RSP_ECCDigit_02
#define NETC_RX_CFG_DATA_CODE_RSP_ECCDigit_03_bit	 CFG_DATA_CODE_RSP_ECC.cfg_data_code_rsp_ecc.CFG_DATA_CODE_RSP_ECCDigit_03
#define NETC_RX_CFG_DATA_CODE_RSP_ECCDigit_01_bit	 CFG_DATA_CODE_RSP_ECC.cfg_data_code_rsp_ecc.CFG_DATA_CODE_RSP_ECCDigit_01

#define NETC_RX_CFG_DATA_CODE_RSP_DDMDigit_10_bit	 CFG_DATA_CODE_RSP_DDM.cfg_data_code_rsp_ddm.CFG_DATA_CODE_RSP_DDMDigit_10
#define NETC_RX_CFG_DATA_CODE_RSP_DDMDigit_11_bit	 CFG_DATA_CODE_RSP_DDM.cfg_data_code_rsp_ddm.CFG_DATA_CODE_RSP_DDMDigit_11
#define NETC_RX_CFG_DATA_CODE_RSP_DDMDigit_08_bit	 CFG_DATA_CODE_RSP_DDM.cfg_data_code_rsp_ddm.CFG_DATA_CODE_RSP_DDMDigit_08
#define NETC_RX_CFG_DATA_CODE_RSP_DDMDigit_09_bit	 CFG_DATA_CODE_RSP_DDM.cfg_data_code_rsp_ddm.CFG_DATA_CODE_RSP_DDMDigit_09
#define NETC_RX_CFG_DATA_CODE_RSP_DDMDigit_06_bit	 CFG_DATA_CODE_RSP_DDM.cfg_data_code_rsp_ddm.CFG_DATA_CODE_RSP_DDMDigit_06
#define NETC_RX_CFG_DATA_CODE_RSP_DDMDigit_07_bit	 CFG_DATA_CODE_RSP_DDM.cfg_data_code_rsp_ddm.CFG_DATA_CODE_RSP_DDMDigit_07
#define NETC_RX_CFG_DATA_CODE_RSP_DDMDigit_04_bit	 CFG_DATA_CODE_RSP_DDM.cfg_data_code_rsp_ddm.CFG_DATA_CODE_RSP_DDMDigit_04
#define NETC_RX_CFG_DATA_CODE_RSP_DDMDigit_05_bit	 CFG_DATA_CODE_RSP_DDM.cfg_data_code_rsp_ddm.CFG_DATA_CODE_RSP_DDMDigit_05
#define NETC_RX_CFG_DATA_CODE_RSP_DDMDigit_02_bit	 CFG_DATA_CODE_RSP_DDM.cfg_data_code_rsp_ddm.CFG_DATA_CODE_RSP_DDMDigit_02
#define NETC_RX_CFG_DATA_CODE_RSP_DDMDigit_03_bit	 CFG_DATA_CODE_RSP_DDM.cfg_data_code_rsp_ddm.CFG_DATA_CODE_RSP_DDMDigit_03
#define NETC_RX_CFG_DATA_CODE_RSP_DDMDigit_01_bit	 CFG_DATA_CODE_RSP_DDM.cfg_data_code_rsp_ddm.CFG_DATA_CODE_RSP_DDMDigit_01

#define NETC_RX_CFG_DATA_CODE_RSP_IPCDigit_10_bit	 CFG_DATA_CODE_RSP_IPC.cfg_data_code_rsp_ipc.CFG_DATA_CODE_RSP_IPCDigit_10
#define NETC_RX_CFG_DATA_CODE_RSP_IPCDigit_11_bit	 CFG_DATA_CODE_RSP_IPC.cfg_data_code_rsp_ipc.CFG_DATA_CODE_RSP_IPCDigit_11
#define NETC_RX_CFG_DATA_CODE_RSP_IPCDigit_08_bit	 CFG_DATA_CODE_RSP_IPC.cfg_data_code_rsp_ipc.CFG_DATA_CODE_RSP_IPCDigit_08
#define NETC_RX_CFG_DATA_CODE_RSP_IPCDigit_09_bit	 CFG_DATA_CODE_RSP_IPC.cfg_data_code_rsp_ipc.CFG_DATA_CODE_RSP_IPCDigit_09
#define NETC_RX_CFG_DATA_CODE_RSP_IPCDigit_06_bit	 CFG_DATA_CODE_RSP_IPC.cfg_data_code_rsp_ipc.CFG_DATA_CODE_RSP_IPCDigit_06
#define NETC_RX_CFG_DATA_CODE_RSP_IPCDigit_07_bit	 CFG_DATA_CODE_RSP_IPC.cfg_data_code_rsp_ipc.CFG_DATA_CODE_RSP_IPCDigit_07
#define NETC_RX_CFG_DATA_CODE_RSP_IPCDigit_04_bit	 CFG_DATA_CODE_RSP_IPC.cfg_data_code_rsp_ipc.CFG_DATA_CODE_RSP_IPCDigit_04
#define NETC_RX_CFG_DATA_CODE_RSP_IPCDigit_05_bit	 CFG_DATA_CODE_RSP_IPC.cfg_data_code_rsp_ipc.CFG_DATA_CODE_RSP_IPCDigit_05
#define NETC_RX_CFG_DATA_CODE_RSP_IPCDigit_02_bit	 CFG_DATA_CODE_RSP_IPC.cfg_data_code_rsp_ipc.CFG_DATA_CODE_RSP_IPCDigit_02
#define NETC_RX_CFG_DATA_CODE_RSP_IPCDigit_03_bit	 CFG_DATA_CODE_RSP_IPC.cfg_data_code_rsp_ipc.CFG_DATA_CODE_RSP_IPCDigit_03
#define NETC_RX_CFG_DATA_CODE_RSP_IPCDigit_01_bit	 CFG_DATA_CODE_RSP_IPC.cfg_data_code_rsp_ipc.CFG_DATA_CODE_RSP_IPCDigit_01

#define NETC_RX_STATUS_ICSPowerModeSts_bit	 STATUS_ICS.status_ics.STATUS_ICSPowerModeSts

#define NETC_RX_STATUS_CDMPowerModeSts_bit	 STATUS_CDM.status_cdm.STATUS_CDMPowerModeSts

#define NETC_RX_STATUS_AMPPowerModeSts_bit	 STATUS_AMP.status_amp.STATUS_AMPPowerModeSts

#define NETC_RX_ENVIRONMENTAL_CONDITIONSExternalTemperature_byte	 ENVIRONMENTAL_CONDITIONS.environmental_conditions.ENVIRONMENTAL_CONDITIONSExternalTemperature
#define NETC_RX_ENVIRONMENTAL_CONDITIONSExternalTemperatureFailSts_bit	 ENVIRONMENTAL_CONDITIONS.environmental_conditions.ENVIRONMENTAL_CONDITIONSExternalTemperatureFailSts

#define NETC_RX_FT_HVAC_STATFT_HVAC_BLW_FN_SPSts_bit	 FT_HVAC_STAT.ft_hvac_stat.FT_HVAC_STATFT_HVAC_BLW_FN_SPSts
#define NETC_RX_FT_HVAC_STATFT_HVAC_MD_STATSts_bit	 FT_HVAC_STAT.ft_hvac_stat.FT_HVAC_STATFT_HVAC_MD_STATSts

#define NETC_RX_DCM_P_MSGCLS_UnlkSw_Psd_P_bit	 DCM_P_MSG.dcm_p_msg.DCM_P_MSGCLS_UnlkSw_Psd_P
#define NETC_RX_DCM_P_MSGWinPos_P_Sts_bit	 DCM_P_MSG.dcm_p_msg.DCM_P_MSGWinPos_P_Sts
#define NETC_RX_DCM_P_MSGCLS_LKSw_Psd_P_bit	 DCM_P_MSG.dcm_p_msg.DCM_P_MSGCLS_LKSw_Psd_P

#define NETC_RX_DCM_D_MSGWinPos_D_Sts_bit_0 DCM_D_MSG.dcm_d_msg.DCM_D_MSGWinPos_D_Sts_0
#define NETC_RX_DCM_D_MSGWinPos_D_Sts_bit_1 DCM_D_MSG.dcm_d_msg.DCM_D_MSGWinPos_D_Sts_1                                   
#define NETC_RX_DCM_D_MSGWinPos_D_Sts_bit   (vuint16)(((vuint16)(NETC_RX_DCM_D_MSGWinPos_D_Sts_bit_1) << 5)\
                                        | (NETC_RX_DCM_D_MSGWinPos_D_Sts_bit_0))
#define NETC_RX_DCM_D_MSGCLS_LKSw_Psd_D_bit	 DCM_D_MSG.dcm_d_msg.DCM_D_MSGCLS_LKSw_Psd_D
#define NETC_RX_DCM_D_MSGCLS_UnlkSw_Psd_D_bit	 DCM_D_MSG.dcm_d_msg.DCM_D_MSGCLS_UnlkSw_Psd_D

#define NETC_RX_NWM_PLGMZero_byte_byte	 NWM_PLGM.nwm_plgm.NWM_PLGMZero_byte
#define NETC_RX_NWM_PLGMSystemStatus_bit	 NWM_PLGM.nwm_plgm.NWM_PLGMSystemStatus
#define NETC_RX_NWM_PLGMActiveLoadSlave_bit	 NWM_PLGM.nwm_plgm.NWM_PLGMActiveLoadSlave
#define NETC_RX_NWM_PLGMEOL_bit	 NWM_PLGM.nwm_plgm.NWM_PLGMEOL
#define NETC_RX_NWM_PLGMGenericFailSts_bit	 NWM_PLGM.nwm_plgm.NWM_PLGMGenericFailSts
#define NETC_RX_NWM_PLGMCurrentFailSts_bit	 NWM_PLGM.nwm_plgm.NWM_PLGMCurrentFailSts

#define NETC_RX_NWM_ETMZero_byte_byte	 NWM_ETM.nwm_etm.NWM_ETMZero_byte
#define NETC_RX_NWM_ETMSystemStatus_bit	 NWM_ETM.nwm_etm.NWM_ETMSystemStatus
#define NETC_RX_NWM_ETMActiveLoadSlave_bit	 NWM_ETM.nwm_etm.NWM_ETMActiveLoadSlave
#define NETC_RX_NWM_ETMEOL_bit	 NWM_ETM.nwm_etm.NWM_ETMEOL
#define NETC_RX_NWM_ETMGenericFailSts_bit	 NWM_ETM.nwm_etm.NWM_ETMGenericFailSts
#define NETC_RX_NWM_ETMCurrentFailSts_bit	 NWM_ETM.nwm_etm.NWM_ETMCurrentFailSts

#define NETC_RX_NWM_DSMZero_byte_byte	 NWM_DSM.nwm_dsm.NWM_DSMZero_byte
#define NETC_RX_NWM_DSMSystemStatus_bit	 NWM_DSM.nwm_dsm.NWM_DSMSystemStatus
#define NETC_RX_NWM_DSMActiveLoadSlave_bit	 NWM_DSM.nwm_dsm.NWM_DSMActiveLoadSlave
#define NETC_RX_NWM_DSMEOL_bit	 NWM_DSM.nwm_dsm.NWM_DSMEOL
#define NETC_RX_NWM_DSMGenericFailSts_bit	 NWM_DSM.nwm_dsm.NWM_DSMGenericFailSts
#define NETC_RX_NWM_DSMCurrentFailSts_bit	 NWM_DSM.nwm_dsm.NWM_DSMCurrentFailSts

#define NETC_RX_NWM_PDMZero_byte_byte	 NWM_PDM.nwm_pdm.NWM_PDMZero_byte
#define NETC_RX_NWM_PDMSystemStatus_bit	 NWM_PDM.nwm_pdm.NWM_PDMSystemStatus
#define NETC_RX_NWM_PDMActiveLoadSlave_bit	 NWM_PDM.nwm_pdm.NWM_PDMActiveLoadSlave
#define NETC_RX_NWM_PDMEOL_bit	 NWM_PDM.nwm_pdm.NWM_PDMEOL
#define NETC_RX_NWM_PDMGenericFailSts_bit	 NWM_PDM.nwm_pdm.NWM_PDMGenericFailSts
#define NETC_RX_NWM_PDMCurrentFailSts_bit	 NWM_PDM.nwm_pdm.NWM_PDMCurrentFailSts

#define NETC_RX_NWM_DDMZero_byte_byte	 NWM_DDM.nwm_ddm.NWM_DDMZero_byte
#define NETC_RX_NWM_DDMSystemStatus_bit	 NWM_DDM.nwm_ddm.NWM_DDMSystemStatus
#define NETC_RX_NWM_DDMActiveLoadSlave_bit	 NWM_DDM.nwm_ddm.NWM_DDMActiveLoadSlave
#define NETC_RX_NWM_DDMEOL_bit	 NWM_DDM.nwm_ddm.NWM_DDMEOL
#define NETC_RX_NWM_DDMGenericFailSts_bit	 NWM_DDM.nwm_ddm.NWM_DDMGenericFailSts
#define NETC_RX_NWM_DDMCurrentFailSts_bit	 NWM_DDM.nwm_ddm.NWM_DDMCurrentFailSts

#define NETC_RX_APPL_ECU_BCM_1APPL_ECU_BCM_bit_0 APPL_ECU_BCM_1.appl_ecu_bcm_1.APPL_ECU_BCM_1APPL_ECU_BCM_0
#define NETC_RX_APPL_ECU_BCM_1APPL_ECU_BCM_bit_1 APPL_ECU_BCM_1.appl_ecu_bcm_1.APPL_ECU_BCM_1APPL_ECU_BCM_1
#define NETC_RX_APPL_ECU_BCM_1APPL_ECU_BCM_bit_2 APPL_ECU_BCM_1.appl_ecu_bcm_1.APPL_ECU_BCM_1APPL_ECU_BCM_2
#define NETC_RX_APPL_ECU_BCM_1APPL_ECU_BCM_bit_3 APPL_ECU_BCM_1.appl_ecu_bcm_1.APPL_ECU_BCM_1APPL_ECU_BCM_3
#define NETC_RX_APPL_ECU_BCM_1APPL_ECU_BCM_bit_4 APPL_ECU_BCM_1.appl_ecu_bcm_1.APPL_ECU_BCM_1APPL_ECU_BCM_4
#define NETC_RX_APPL_ECU_BCM_1APPL_ECU_BCM_bit_5 APPL_ECU_BCM_1.appl_ecu_bcm_1.APPL_ECU_BCM_1APPL_ECU_BCM_5
#define NETC_RX_APPL_ECU_BCM_1APPL_ECU_BCM_bit_6 APPL_ECU_BCM_1.appl_ecu_bcm_1.APPL_ECU_BCM_1APPL_ECU_BCM_6
#define NETC_RX_APPL_ECU_BCM_1APPL_ECU_BCM_bit_7 APPL_ECU_BCM_1.appl_ecu_bcm_1.APPL_ECU_BCM_1APPL_ECU_BCM_7
#define NETC_RX_CAN_APPL_ECU_BCM_1APPL_ECU_BCM_bit_0 RDS4_1.appl_ecu_bcm_1.APPL_ECU_BCM_1APPL_ECU_BCM_0
#define NETC_RX_CAN_APPL_ECU_BCM_1APPL_ECU_BCM_bit_1 RDS4_1.appl_ecu_bcm_1.APPL_ECU_BCM_1APPL_ECU_BCM_1
#define NETC_RX_CAN_APPL_ECU_BCM_1APPL_ECU_BCM_bit_2 RDS4_1.appl_ecu_bcm_1.APPL_ECU_BCM_1APPL_ECU_BCM_2
#define NETC_RX_CAN_APPL_ECU_BCM_1APPL_ECU_BCM_bit_3 RDS4_1.appl_ecu_bcm_1.APPL_ECU_BCM_1APPL_ECU_BCM_3
#define NETC_RX_CAN_APPL_ECU_BCM_1APPL_ECU_BCM_bit_4 RDS4_1.appl_ecu_bcm_1.APPL_ECU_BCM_1APPL_ECU_BCM_4
#define NETC_RX_CAN_APPL_ECU_BCM_1APPL_ECU_BCM_bit_5 RDS4_1.appl_ecu_bcm_1.APPL_ECU_BCM_1APPL_ECU_BCM_5
#define NETC_RX_CAN_APPL_ECU_BCM_1APPL_ECU_BCM_bit_6 RDS4_1.appl_ecu_bcm_1.APPL_ECU_BCM_1APPL_ECU_BCM_6
#define NETC_RX_CAN_APPL_ECU_BCM_1APPL_ECU_BCM_bit_7 RDS4_1.appl_ecu_bcm_1.APPL_ECU_BCM_1APPL_ECU_BCM_7

#define NETC_RX_TRIP_A_BTotalOdometer_bit_0 TRIP_A_B.trip_a_b.TRIP_A_BTotalOdometer_0
#define NETC_RX_TRIP_A_BTotalOdometer_bit_1 TRIP_A_B.trip_a_b.TRIP_A_BTotalOdometer_1
#define NETC_RX_TRIP_A_BTotalOdometer_bit_2 TRIP_A_B.trip_a_b.TRIP_A_BTotalOdometer_2                                   
#define NETC_RX_TRIP_A_BTotalOdometer_bit   (vuint32)(((vuint32)(NETC_RX_TRIP_A_BTotalOdometer_bit_2) << 16)\
                                        | ((vuint32)(NETC_RX_TRIP_A_BTotalOdometer_bit_1) << 8)\
                                        | (NETC_RX_TRIP_A_BTotalOdometer_bit_0))
#define NETC_RX_CAN_TRIP_A_BTotalOdometer_bit_0 RDS5_1.trip_a_b.TRIP_A_BTotalOdometer_0
#define NETC_RX_CAN_TRIP_A_BTotalOdometer_bit_1 RDS5_1.trip_a_b.TRIP_A_BTotalOdometer_1
#define NETC_RX_CAN_TRIP_A_BTotalOdometer_bit_2 RDS5_1.trip_a_b.TRIP_A_BTotalOdometer_2                               
#define NETC_RX_CAN_TRIP_A_BTotalOdometer_bit   (vuint32)(((vuint32)(NETC_RX_CAN_TRIP_A_BTotalOdometer_bit_2) << 16)\
                                        | ((vuint32)(NETC_RX_CAN_TRIP_A_BTotalOdometer_bit_1) << 8)\
                                        | (NETC_RX_CAN_TRIP_A_BTotalOdometer_bit_0))

#define NETC_RX_TGW_A1Zoom_RQSts_bit	 TGW_A1.tgw_a1.TGW_A1Zoom_RQSts
#define NETC_RX_TGW_A1EModeSwPsdSts_bit	 TGW_A1.tgw_a1.TGW_A1EModeSwPsdSts
#define NETC_RX_CAN_TGW_A1Zoom_RQSts_bit	 RDS7_1.tgw_a1.TGW_A1Zoom_RQSts
#define NETC_RX_CAN_TGW_A1EModeSwPsdSts_bit	 RDS7_1.tgw_a1.TGW_A1EModeSwPsdSts

#define NETC_RX_ICS_MSGTRAC_PSDSts_bit	 ICS_MSG.ics_msg.ICS_MSGTRAC_PSDSts
#define NETC_RX_ICS_MSGSpSt_Pad1_bit	 ICS_MSG.ics_msg.ICS_MSGSpSt_Pad1
#define NETC_RX_ICS_MSGSpSt_Pad2_bit	 ICS_MSG.ics_msg.ICS_MSGSpSt_Pad2
#define NETC_RX_CAN_ICS_MSGTRAC_PSDSts_bit	 RDS8_1.ics_msg.ICS_MSGTRAC_PSDSts
#define NETC_RX_CAN_ICS_MSGSpSt_Pad1_bit	 RDS8_1.ics_msg.ICS_MSGSpSt_Pad1
#define NETC_RX_CAN_ICS_MSGSpSt_Pad2_bit	 RDS8_1.ics_msg.ICS_MSGSpSt_Pad2

#define NETC_RX_HVAC_A1TRAC_PSDSts_bit	 HVAC_A1.hvac_a1.HVAC_A1TRAC_PSDSts
#define NETC_RX_HVAC_A1EBL_StatSts_bit	 HVAC_A1.hvac_a1.HVAC_A1EBL_StatSts
#define NETC_RX_HVAC_A1LT_AMB_SENSSts_byte	 HVAC_A1.hvac_a1.HVAC_A1LT_AMB_SENSSts
#define NETC_RX_CAN_HVAC_A1TRAC_PSDSts_bit	 RDS9_1.hvac_a1.HVAC_A1TRAC_PSDSts
#define NETC_RX_CAN_HVAC_A1EBL_StatSts_bit	 RDS9_1.hvac_a1.HVAC_A1EBL_StatSts
#define NETC_RX_CAN_HVAC_A1LT_AMB_SENSSts_byte	 RDS9_1.hvac_a1.HVAC_A1LT_AMB_SENSSts

#define NETC_RX_DIRECT_INFOHU_CMPSts_bit	 DIRECT_INFO.direct_info.DIRECT_INFOHU_CMPSts
#define NETC_RX_CAN_DIRECT_INFOHU_CMPSts_bit	 RDS10_1.direct_info.DIRECT_INFOHU_CMPSts

#define NETC_RX_HVAC_A4A2D_FT_HVAC_BLOWER_VOLTAGE_bit_0 HVAC_A4.hvac_a4.HVAC_A4A2D_FT_HVAC_BLOWER_VOLTAGE_0
#define NETC_RX_HVAC_A4A2D_FT_HVAC_BLOWER_VOLTAGE_bit_1 HVAC_A4.hvac_a4.HVAC_A4A2D_FT_HVAC_BLOWER_VOLTAGE_1                       
#define NETC_RX_HVAC_A4A2D_FT_HVAC_BLOWER_VOLTAGE_bit   (vuint16)(((vuint16)(NETC_RX_HVAC_A4A2D_FT_HVAC_BLOWER_VOLTAGE_bit_1) << 2)\
                                        | (NETC_RX_HVAC_A4A2D_FT_HVAC_BLOWER_VOLTAGE_bit_0))
#define NETC_RX_HVAC_A4A2D_DRV_TEMP_DR_POS_bit_0 HVAC_A4.hvac_a4.HVAC_A4A2D_DRV_TEMP_DR_POS_0
#define NETC_RX_HVAC_A4A2D_DRV_TEMP_DR_POS_bit_1 HVAC_A4.hvac_a4.HVAC_A4A2D_DRV_TEMP_DR_POS_1                              
#define NETC_RX_HVAC_A4A2D_DRV_TEMP_DR_POS_bit   (vuint16)(((vuint16)(NETC_RX_HVAC_A4A2D_DRV_TEMP_DR_POS_bit_1) << 4)\
                                        | (NETC_RX_HVAC_A4A2D_DRV_TEMP_DR_POS_bit_0))
#define NETC_RX_CAN_HVAC_A4A2D_FT_HVAC_BLOWER_VOLTAGE_bit_0 RDS11_1.hvac_a4.HVAC_A4A2D_FT_HVAC_BLOWER_VOLTAGE_0
#define NETC_RX_CAN_HVAC_A4A2D_FT_HVAC_BLOWER_VOLTAGE_bit_1 RDS11_1.hvac_a4.HVAC_A4A2D_FT_HVAC_BLOWER_VOLTAGE_1                   
#define NETC_RX_CAN_HVAC_A4A2D_FT_HVAC_BLOWER_VOLTAGE_bit   (vuint16)(((vuint16)(NETC_RX_CAN_HVAC_A4A2D_FT_HVAC_BLOWER_VOLTAGE_bit_1) << 2)\
                                        | (NETC_RX_CAN_HVAC_A4A2D_FT_HVAC_BLOWER_VOLTAGE_bit_0))
#define NETC_RX_CAN_HVAC_A4A2D_DRV_TEMP_DR_POS_bit_0 RDS11_1.hvac_a4.HVAC_A4A2D_DRV_TEMP_DR_POS_0
#define NETC_RX_CAN_HVAC_A4A2D_DRV_TEMP_DR_POS_bit_1 RDS11_1.hvac_a4.HVAC_A4A2D_DRV_TEMP_DR_POS_1                          
#define NETC_RX_CAN_HVAC_A4A2D_DRV_TEMP_DR_POS_bit   (vuint16)(((vuint16)(NETC_RX_CAN_HVAC_A4A2D_DRV_TEMP_DR_POS_bit_1) << 4)\
                                        | (NETC_RX_CAN_HVAC_A4A2D_DRV_TEMP_DR_POS_bit_0))

#define NETC_RX_STATUS_RRMPowerModeSts_bit	 STATUS_RRM.status_rrm.STATUS_RRMPowerModeSts
#define NETC_RX_CAN_STATUS_RRMPowerModeSts_bit	 RDS12_1.status_rrm.STATUS_RRMPowerModeSts

#define NETC_RX_STATUS_IPCPowerModeSts_bit	 STATUS_IPC.status_ipc.STATUS_IPCPowerModeSts
#define NETC_RX_CAN_STATUS_IPCPowerModeSts_bit	 RDS13_1.status_ipc.STATUS_IPCPowerModeSts

#define NETC_RX_STATUS_ECCCompressorACReqSts_bit	 STATUS_ECC.status_ecc.STATUS_ECCCompressorACReqSts
#define NETC_RX_STATUS_ECCRHeatedWindowCntrl_bit	 STATUS_ECC.status_ecc.STATUS_ECCRHeatedWindowCntrl
#define NETC_RX_STATUS_ECCPTCFailSts_bit	 STATUS_ECC.status_ecc.STATUS_ECCPTCFailSts
#define NETC_RX_STATUS_ECCPTCSts_bit	 STATUS_ECC.status_ecc.STATUS_ECCPTCSts
#define NETC_RX_STATUS_ECCPowerModeSts_bit	 STATUS_ECC.status_ecc.STATUS_ECCPowerModeSts
#define NETC_RX_STATUS_ECCStopStartEnable_bit	 STATUS_ECC.status_ecc.STATUS_ECCStopStartEnable
#define NETC_RX_CAN_STATUS_ECCCompressorACReqSts_bit	 RDS14_1.status_ecc.STATUS_ECCCompressorACReqSts
#define NETC_RX_CAN_STATUS_ECCRHeatedWindowCntrl_bit	 RDS14_1.status_ecc.STATUS_ECCRHeatedWindowCntrl
#define NETC_RX_CAN_STATUS_ECCPTCFailSts_bit	 RDS14_1.status_ecc.STATUS_ECCPTCFailSts
#define NETC_RX_CAN_STATUS_ECCPTCSts_bit	 RDS14_1.status_ecc.STATUS_ECCPTCSts
#define NETC_RX_CAN_STATUS_ECCPowerModeSts_bit	 RDS14_1.status_ecc.STATUS_ECCPowerModeSts
#define NETC_RX_CAN_STATUS_ECCStopStartEnable_bit	 RDS14_1.status_ecc.STATUS_ECCStopStartEnable

#define NETC_RX_HVAC_A2SpSt_Pad1_bit	 HVAC_A2.hvac_a2.HVAC_A2SpSt_Pad1
#define NETC_RX_HVAC_A2SpSt_Pad2_bit	 HVAC_A2.hvac_a2.HVAC_A2SpSt_Pad2
#define NETC_RX_HVAC_A2EVAP_TEMPSts_byte	 HVAC_A2.hvac_a2.HVAC_A2EVAP_TEMPSts
#define NETC_RX_HVAC_A2EvapTempTarSts_byte	 HVAC_A2.hvac_a2.HVAC_A2EvapTempTarSts
#define NETC_RX_CAN_HVAC_A2SpSt_Pad1_bit	 RDS15_1.hvac_a2.HVAC_A2SpSt_Pad1
#define NETC_RX_CAN_HVAC_A2SpSt_Pad2_bit	 RDS15_1.hvac_a2.HVAC_A2SpSt_Pad2
#define NETC_RX_CAN_HVAC_A2EVAP_TEMPSts_byte	 RDS15_1.hvac_a2.HVAC_A2EVAP_TEMPSts
#define NETC_RX_CAN_HVAC_A2EvapTempTarSts_byte	 RDS15_1.hvac_a2.HVAC_A2EvapTempTarSts

#define NETC_RX_PLG_A1GT_DL_Stat_bit_0 PLG_A1.plg_a1.PLG_A1GT_DL_Stat_0
#define NETC_RX_PLG_A1GT_DL_Stat_bit_1 PLG_A1.plg_a1.PLG_A1GT_DL_Stat_1                                        
#define NETC_RX_PLG_A1GT_DL_Stat_bit   (vuint16)(((vuint16)(NETC_RX_PLG_A1GT_DL_Stat_bit_1) << 1)\
                                        | (NETC_RX_PLG_A1GT_DL_Stat_bit_0))
#define NETC_RX_PLG_A1PLG_Req_PE_bit	 PLG_A1.plg_a1.PLG_A1PLG_Req_PE
#define NETC_RX_CAN_PLG_A1GT_DL_Stat_bit_0 RDS16_1.plg_a1.PLG_A1GT_DL_Stat_0
#define NETC_RX_CAN_PLG_A1GT_DL_Stat_bit_1 RDS16_1.plg_a1.PLG_A1GT_DL_Stat_1                                    
#define NETC_RX_CAN_PLG_A1GT_DL_Stat_bit   (vuint16)(((vuint16)(NETC_RX_CAN_PLG_A1GT_DL_Stat_bit_1) << 1)\
                                        | (NETC_RX_CAN_PLG_A1GT_DL_Stat_bit_0))
#define NETC_RX_CAN_PLG_A1PLG_Req_PE_bit	 RDS16_1.plg_a1.PLG_A1PLG_Req_PE

#define NETC_RX_ECC_A1AcOutputCurrentSts_byte	 ECC_A1.ecc_a1.ECC_A1AcOutputCurrentSts
#define NETC_RX_CAN_ECC_A1AcOutputCurrentSts_byte	 RDS17_1.ecc_a1.ECC_A1AcOutputCurrentSts

#define NETC_RX_NWM_RBSSZero_byte_byte	 NWM_RBSS.nwm_rbss.NWM_RBSSZero_byte
#define NETC_RX_NWM_RBSSSystemStatus_bit	 NWM_RBSS.nwm_rbss.NWM_RBSSSystemStatus
#define NETC_RX_NWM_RBSSActiveLoadSlave_bit	 NWM_RBSS.nwm_rbss.NWM_RBSSActiveLoadSlave
#define NETC_RX_NWM_RBSSEOL_bit	 NWM_RBSS.nwm_rbss.NWM_RBSSEOL
#define NETC_RX_NWM_RBSSGenericFailSts_bit	 NWM_RBSS.nwm_rbss.NWM_RBSSGenericFailSts
#define NETC_RX_NWM_RBSSCurrentFailSts_bit	 NWM_RBSS.nwm_rbss.NWM_RBSSCurrentFailSts
#define NETC_RX_CAN_NWM_RBSSZero_byte_byte	 RDS18_1.nwm_rbss.NWM_RBSSZero_byte
#define NETC_RX_CAN_NWM_RBSSSystemStatus_bit	 RDS18_1.nwm_rbss.NWM_RBSSSystemStatus
#define NETC_RX_CAN_NWM_RBSSActiveLoadSlave_bit	 RDS18_1.nwm_rbss.NWM_RBSSActiveLoadSlave
#define NETC_RX_CAN_NWM_RBSSEOL_bit	 RDS18_1.nwm_rbss.NWM_RBSSEOL
#define NETC_RX_CAN_NWM_RBSSGenericFailSts_bit	 RDS18_1.nwm_rbss.NWM_RBSSGenericFailSts
#define NETC_RX_CAN_NWM_RBSSCurrentFailSts_bit	 RDS18_1.nwm_rbss.NWM_RBSSCurrentFailSts

#define NETC_RX_NWM_LBSSZero_byte_byte	 NWM_LBSS.nwm_lbss.NWM_LBSSZero_byte
#define NETC_RX_NWM_LBSSSystemStatus_bit	 NWM_LBSS.nwm_lbss.NWM_LBSSSystemStatus
#define NETC_RX_NWM_LBSSActiveLoadSlave_bit	 NWM_LBSS.nwm_lbss.NWM_LBSSActiveLoadSlave
#define NETC_RX_NWM_LBSSEOL_bit	 NWM_LBSS.nwm_lbss.NWM_LBSSEOL
#define NETC_RX_NWM_LBSSGenericFailSts_bit	 NWM_LBSS.nwm_lbss.NWM_LBSSGenericFailSts
#define NETC_RX_NWM_LBSSCurrentFailSts_bit	 NWM_LBSS.nwm_lbss.NWM_LBSSCurrentFailSts
#define NETC_RX_CAN_NWM_LBSSZero_byte_byte	 RDS19_1.nwm_lbss.NWM_LBSSZero_byte
#define NETC_RX_CAN_NWM_LBSSSystemStatus_bit	 RDS19_1.nwm_lbss.NWM_LBSSSystemStatus
#define NETC_RX_CAN_NWM_LBSSActiveLoadSlave_bit	 RDS19_1.nwm_lbss.NWM_LBSSActiveLoadSlave
#define NETC_RX_CAN_NWM_LBSSEOL_bit	 RDS19_1.nwm_lbss.NWM_LBSSEOL
#define NETC_RX_CAN_NWM_LBSSGenericFailSts_bit	 RDS19_1.nwm_lbss.NWM_LBSSGenericFailSts
#define NETC_RX_CAN_NWM_LBSSCurrentFailSts_bit	 RDS19_1.nwm_lbss.NWM_LBSSCurrentFailSts

#define NETC_RX_NWM_ICSZero_byte_byte	 NWM_ICS.nwm_ics.NWM_ICSZero_byte
#define NETC_RX_NWM_ICSSystemStatus_bit	 NWM_ICS.nwm_ics.NWM_ICSSystemStatus
#define NETC_RX_NWM_ICSActiveLoadSlave_bit	 NWM_ICS.nwm_ics.NWM_ICSActiveLoadSlave
#define NETC_RX_NWM_ICSEOL_bit	 NWM_ICS.nwm_ics.NWM_ICSEOL
#define NETC_RX_NWM_ICSGenericFailSts_bit	 NWM_ICS.nwm_ics.NWM_ICSGenericFailSts
#define NETC_RX_NWM_ICSCurrentFailSts_bit	 NWM_ICS.nwm_ics.NWM_ICSCurrentFailSts
#define NETC_RX_CAN_NWM_ICSZero_byte_byte	 RDS20_1.nwm_ics.NWM_ICSZero_byte
#define NETC_RX_CAN_NWM_ICSSystemStatus_bit	 RDS20_1.nwm_ics.NWM_ICSSystemStatus
#define NETC_RX_CAN_NWM_ICSActiveLoadSlave_bit	 RDS20_1.nwm_ics.NWM_ICSActiveLoadSlave
#define NETC_RX_CAN_NWM_ICSEOL_bit	 RDS20_1.nwm_ics.NWM_ICSEOL
#define NETC_RX_CAN_NWM_ICSGenericFailSts_bit	 RDS20_1.nwm_ics.NWM_ICSGenericFailSts
#define NETC_RX_CAN_NWM_ICSCurrentFailSts_bit	 RDS20_1.nwm_ics.NWM_ICSCurrentFailSts

#define NETC_RX_NWM_CSWMZero_byte_byte	 NWM_CSWM.nwm_cswm.NWM_CSWMZero_byte
#define NETC_RX_NWM_CSWMSystemStatus_bit	 NWM_CSWM.nwm_cswm.NWM_CSWMSystemStatus
#define NETC_RX_NWM_CSWMActiveLoadSlave_bit	 NWM_CSWM.nwm_cswm.NWM_CSWMActiveLoadSlave
#define NETC_RX_NWM_CSWMEOL_bit	 NWM_CSWM.nwm_cswm.NWM_CSWMEOL
#define NETC_RX_NWM_CSWMGenericFailSts_bit	 NWM_CSWM.nwm_cswm.NWM_CSWMGenericFailSts
#define NETC_RX_NWM_CSWMCurrentFailSts_bit	 NWM_CSWM.nwm_cswm.NWM_CSWMCurrentFailSts
#define NETC_RX_CAN_NWM_CSWMZero_byte_byte	 RDS21_1.nwm_cswm.NWM_CSWMZero_byte
#define NETC_RX_CAN_NWM_CSWMSystemStatus_bit	 RDS21_1.nwm_cswm.NWM_CSWMSystemStatus
#define NETC_RX_CAN_NWM_CSWMActiveLoadSlave_bit	 RDS21_1.nwm_cswm.NWM_CSWMActiveLoadSlave
#define NETC_RX_CAN_NWM_CSWMEOL_bit	 RDS21_1.nwm_cswm.NWM_CSWMEOL
#define NETC_RX_CAN_NWM_CSWMGenericFailSts_bit	 RDS21_1.nwm_cswm.NWM_CSWMGenericFailSts
#define NETC_RX_CAN_NWM_CSWMCurrentFailSts_bit	 RDS21_1.nwm_cswm.NWM_CSWMCurrentFailSts

#define NETC_RX_NWM_CDMZero_byte_byte	 NWM_CDM.nwm_cdm.NWM_CDMZero_byte
#define NETC_RX_NWM_CDMSystemStatus_bit	 NWM_CDM.nwm_cdm.NWM_CDMSystemStatus
#define NETC_RX_NWM_CDMActiveLoadSlave_bit	 NWM_CDM.nwm_cdm.NWM_CDMActiveLoadSlave
#define NETC_RX_NWM_CDMEOL_bit	 NWM_CDM.nwm_cdm.NWM_CDMEOL
#define NETC_RX_NWM_CDMGenericFailSts_bit	 NWM_CDM.nwm_cdm.NWM_CDMGenericFailSts
#define NETC_RX_NWM_CDMCurrentFailSts_bit	 NWM_CDM.nwm_cdm.NWM_CDMCurrentFailSts
#define NETC_RX_CAN_NWM_CDMZero_byte_byte	 RDS22_1.nwm_cdm.NWM_CDMZero_byte
#define NETC_RX_CAN_NWM_CDMSystemStatus_bit	 RDS22_1.nwm_cdm.NWM_CDMSystemStatus
#define NETC_RX_CAN_NWM_CDMActiveLoadSlave_bit	 RDS22_1.nwm_cdm.NWM_CDMActiveLoadSlave
#define NETC_RX_CAN_NWM_CDMEOL_bit	 RDS22_1.nwm_cdm.NWM_CDMEOL
#define NETC_RX_CAN_NWM_CDMGenericFailSts_bit	 RDS22_1.nwm_cdm.NWM_CDMGenericFailSts
#define NETC_RX_CAN_NWM_CDMCurrentFailSts_bit	 RDS22_1.nwm_cdm.NWM_CDMCurrentFailSts

#define NETC_RX_NWM_AMPZero_byte_byte	 NWM_AMP.nwm_amp.NWM_AMPZero_byte
#define NETC_RX_NWM_AMPSystemStatus_bit	 NWM_AMP.nwm_amp.NWM_AMPSystemStatus
#define NETC_RX_NWM_AMPActiveLoadSlave_bit	 NWM_AMP.nwm_amp.NWM_AMPActiveLoadSlave
#define NETC_RX_NWM_AMPEOL_bit	 NWM_AMP.nwm_amp.NWM_AMPEOL
#define NETC_RX_NWM_AMPGenericFailSts_bit	 NWM_AMP.nwm_amp.NWM_AMPGenericFailSts
#define NETC_RX_NWM_AMPCurrentFailSts_bit	 NWM_AMP.nwm_amp.NWM_AMPCurrentFailSts
#define NETC_RX_CAN_NWM_AMPZero_byte_byte	 RDS23_1.nwm_amp.NWM_AMPZero_byte
#define NETC_RX_CAN_NWM_AMPSystemStatus_bit	 RDS23_1.nwm_amp.NWM_AMPSystemStatus
#define NETC_RX_CAN_NWM_AMPActiveLoadSlave_bit	 RDS23_1.nwm_amp.NWM_AMPActiveLoadSlave
#define NETC_RX_CAN_NWM_AMPEOL_bit	 RDS23_1.nwm_amp.NWM_AMPEOL
#define NETC_RX_CAN_NWM_AMPGenericFailSts_bit	 RDS23_1.nwm_amp.NWM_AMPGenericFailSts
#define NETC_RX_CAN_NWM_AMPCurrentFailSts_bit	 RDS23_1.nwm_amp.NWM_AMPCurrentFailSts

#define NETC_RX_NWM_ECCZero_byte_byte	 NWM_ECC.nwm_ecc.NWM_ECCZero_byte
#define NETC_RX_NWM_ECCSystemStatus_bit	 NWM_ECC.nwm_ecc.NWM_ECCSystemStatus
#define NETC_RX_NWM_ECCActiveLoadSlave_bit	 NWM_ECC.nwm_ecc.NWM_ECCActiveLoadSlave
#define NETC_RX_NWM_ECCEOL_bit	 NWM_ECC.nwm_ecc.NWM_ECCEOL
#define NETC_RX_NWM_ECCGenericFailSts_bit	 NWM_ECC.nwm_ecc.NWM_ECCGenericFailSts
#define NETC_RX_NWM_ECCCurrentFailSts_bit	 NWM_ECC.nwm_ecc.NWM_ECCCurrentFailSts
#define NETC_RX_CAN_NWM_ECCZero_byte_byte	 RDS24_1.nwm_ecc.NWM_ECCZero_byte
#define NETC_RX_CAN_NWM_ECCSystemStatus_bit	 RDS24_1.nwm_ecc.NWM_ECCSystemStatus
#define NETC_RX_CAN_NWM_ECCActiveLoadSlave_bit	 RDS24_1.nwm_ecc.NWM_ECCActiveLoadSlave
#define NETC_RX_CAN_NWM_ECCEOL_bit	 RDS24_1.nwm_ecc.NWM_ECCEOL
#define NETC_RX_CAN_NWM_ECCGenericFailSts_bit	 RDS24_1.nwm_ecc.NWM_ECCGenericFailSts
#define NETC_RX_CAN_NWM_ECCCurrentFailSts_bit	 RDS24_1.nwm_ecc.NWM_ECCCurrentFailSts

#define NETC_RX_NWM_IPCZero_byte_byte	 NWM_IPC.nwm_ipc.NWM_IPCZero_byte
#define NETC_RX_NWM_IPCSystemStatus_bit	 NWM_IPC.nwm_ipc.NWM_IPCSystemStatus
#define NETC_RX_NWM_IPCActiveLoadSlave_bit	 NWM_IPC.nwm_ipc.NWM_IPCActiveLoadSlave
#define NETC_RX_NWM_IPCEOL_bit	 NWM_IPC.nwm_ipc.NWM_IPCEOL
#define NETC_RX_NWM_IPCGenericFailSts_bit	 NWM_IPC.nwm_ipc.NWM_IPCGenericFailSts
#define NETC_RX_NWM_IPCCurrentFailSts_bit	 NWM_IPC.nwm_ipc.NWM_IPCCurrentFailSts
#define NETC_RX_CAN_NWM_IPCZero_byte_byte	 RDS25_1.nwm_ipc.NWM_IPCZero_byte
#define NETC_RX_CAN_NWM_IPCSystemStatus_bit	 RDS25_1.nwm_ipc.NWM_IPCSystemStatus
#define NETC_RX_CAN_NWM_IPCActiveLoadSlave_bit	 RDS25_1.nwm_ipc.NWM_IPCActiveLoadSlave
#define NETC_RX_CAN_NWM_IPCEOL_bit	 RDS25_1.nwm_ipc.NWM_IPCEOL
#define NETC_RX_CAN_NWM_IPCGenericFailSts_bit	 RDS25_1.nwm_ipc.NWM_IPCGenericFailSts
#define NETC_RX_CAN_NWM_IPCCurrentFailSts_bit	 RDS25_1.nwm_ipc.NWM_IPCCurrentFailSts

#define NETC_RX_CBC_I4CmdIgnSts_bit	 CBC_I4.cbc_i4.CBC_I4CmdIgnSts
#define NETC_RX_CBC_I4StTypSts_bit	 CBC_I4.cbc_i4.CBC_I4StTypSts
#define NETC_RX_CBC_I4RemStActvSts_bit	 CBC_I4.cbc_i4.CBC_I4RemStActvSts
#define NETC_RX_CBC_I4KeyInIgnSts_bit	 CBC_I4.cbc_i4.CBC_I4KeyInIgnSts
#define NETC_RX_CAN_CBC_I4CmdIgnSts_bit	 RDS26_1.cbc_i4.CBC_I4CmdIgnSts
#define NETC_RX_CAN_CBC_I4StTypSts_bit	 RDS26_1.cbc_i4.CBC_I4StTypSts
#define NETC_RX_CAN_CBC_I4RemStActvSts_bit	 RDS26_1.cbc_i4.CBC_I4RemStActvSts
#define NETC_RX_CAN_CBC_I4KeyInIgnSts_bit	 RDS26_1.cbc_i4.CBC_I4KeyInIgnSts

#define NETC_RX_CMCM_UNLKE_CALL_STAT_bit	 CMCM_UNLK.cmcm_unlk.CMCM_UNLKE_CALL_STAT
#define NETC_RX_CMCM_UNLKU_CALL_STAT_bit	 CMCM_UNLK.cmcm_unlk.CMCM_UNLKU_CALL_STAT
#define NETC_RX_CAN_CMCM_UNLKE_CALL_STAT_bit	 RDS27_1.cmcm_unlk.CMCM_UNLKE_CALL_STAT
#define NETC_RX_CAN_CMCM_UNLKU_CALL_STAT_bit	 RDS27_1.cmcm_unlk.CMCM_UNLKU_CALL_STAT

#define NETC_RX_CFG_RQCFG_FeatureCntrl_byte	 CFG_RQ.cfg_rq.CFG_RQCFG_FeatureCntrl
#define NETC_RX_CFG_RQCFG_STAT_RQCntrl_bit	 CFG_RQ.cfg_rq.CFG_RQCFG_STAT_RQCntrl
#define NETC_RX_CFG_RQCFG_SETCntrl_byte	 CFG_RQ.cfg_rq.CFG_RQCFG_SETCntrl
#define NETC_RX_CAN_CFG_RQCFG_FeatureCntrl_byte	 RDS28_1.cfg_rq.CFG_RQCFG_FeatureCntrl
#define NETC_RX_CAN_CFG_RQCFG_STAT_RQCntrl_bit	 RDS28_1.cfg_rq.CFG_RQCFG_STAT_RQCntrl
#define NETC_RX_CAN_CFG_RQCFG_SETCntrl_byte	 RDS28_1.cfg_rq.CFG_RQCFG_SETCntrl


/* Initialization configurations */
#define kCanInitObj1 0



/*************************************************************/
/* Support for diagnostics/transport protocol            */
/*************************************************************/

#define tpCanTxData kTpTxData


#define TpTransmitDiag(dataPtr, dataLen)	TpTransmit(0, dataPtr, dataLen)
#define TpTransmitSFDiag(dataLen)	TpTransmitSF(0, dataLen)



#endif

