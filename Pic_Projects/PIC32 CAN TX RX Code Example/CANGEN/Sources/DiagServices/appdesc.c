/* -----------------------------------------------------------------------------
  Filename:    appdesc.c
  Description: Implementation example for the proper usage with CANdesc.
                
                Manufacturer: Fiat
                EcuDocFile:   d:\synergy\ccm_wa\bcmcuswl7_root-pf_1.0.3_zucca\bcmcuswl7_root\bcmcuswl7_client\tools\can_gen\cdd templates\cusw_v0.3_20110224.40.cdd
                Variant:      CommonDiagnostics

  Generated by CANdelaGen, Mon Mar 07 15:06:26 2011
 
 ----------------------------------------------------------------------------- */
/* -----------------------------------------------------------------------------
  C O P Y R I G H T
 -------------------------------------------------------------------------------
  Copyright (c) 2001-2008 by Vector Informatik GmbH. All rights reserved.
 
  This software is copyright protected and proprietary to Vector Informatik 
  GmbH.
  
  Vector Informatik GmbH grants to you only those rights as set out in the 
  license conditions.
  
  All other rights remain with Vector Informatik GmbH.
 -------------------------------------------------------------------------------
 ----------------------------------------------------------------------------- */

/* -----------------------------------------------------------------------------
    &&&~ History
 ----------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------
  Please note, that the demo and example programs only show special aspects of 
  the software. With regard to the fact that these programs are meant for 
  demonstration purposes only, Vector Informatik�s liability shall be expressly 
  excluded in cases of ordinary negligence, to the extent admissible by law or 
  statute.
  ---------------------------------------------------------------------------- */

/* 
------------------------------------------------------------------------------------------------------------------------
                                             A U T H O R   I D E N T I T Y                                              
------------------------------------------------------------------------------------------------------------------------
Initials   Name                  Company                                                                                
---------- --------------------- ---------------------------------------------------------------------------------------
Gz         Oliver Garnatz        Vector Informatik GmbH                                                                 
Ktw        Katrin Thurow         Vector Informatik GmbH                                                                 
Mhe        Matthias Heil         Vector Informatik GmbH                                                                 
Sa         Mishel Shishmanyan    Vector Informatik GmbH                                                                 
------------------------------------------------------------------------------------------------------------------------
                                            R E V I S I O N   H I S T O R Y                                             
------------------------------------------------------------------------------------------------------------------------
Date       Version  Author Modification type Issue Id      Affects                        Description                    
---------- -------- ------ ----------------- ------------- ------------------------------ -------------------------------
2006-10-20 05.00.00
                    Sa     Added             ESCAN00018584 Fiat(UDS)                      Allow support of DIDs with dynamic data length for service $22.
                    Sa     Modified          ESCAN00016884 UDS                            ComControl service compliance with the latest ISO-UDS (2005-05-29).
                    Sa     Modified          ESCAN00018098 All                            Unified security access management and API.
                    Sa     Added             ESCAN00017752 All                            OBD II compliance with regard to the NRC usage and response behavior.
                    Sa     Added             ESCAN00018099 All                            Add support for CANdela format 5.0.
2007-02-16 05.01.00
                    Sa     Fixed             ESCAN00018114 Fiat(UDS)                      CANdesc not compilable if permanent repeated call is turned on.
                    Sa     Fixed             ESCAN00018916 Fiat(UDS)                      Compile error on multi-channel system and service $28 support.
                    Sa     Fixed             ESCAN00019149 Fiat(UDS)                      Wrong processing of service $22 with active permanent multi-service-call option.
                    Sa     Fixed             ESCAN00018576 All                            Service instance linear look-up routine fails finding a requested service.
                    Sa     Fixed             ESCAN00019220 All                            Compile error for wrong syntax after preprocesing defines: DescUsdtNetStateTask and DescUsdtNetTimerTask.
                    Sa     Fixed             ESCAN00019334 All                            Generator: Unable to generate CANdesc with multi channel TP and CANgen newer than 4.30.03.
                    Sa     Modified          ESCAN00018872 All                            Wrong main-handler comment for constants names kDescReqTypePhys (-Func).
                    Sa     Modified          ESCAN00019321 All                            Remove file-path from the #include "node.h"
                    Sa     Modified          ESCAN00019735 All                            Use CPU type optimized data definition types (uintx_least).
                    Sa     Added             ESCAN00018320 All                            Generate P2 and P2* task-call constants with a justification of one task call less.
                    Sa     Added             ESCAN00018592 All                            DescRingBufferCancel API added.
2007-04-13 05.02.00
                    Sa     Added             ESCAN00020069 UDS                            Support for unified PID application handling.
2007-05-30 05.03.00
                    Sa     Added             ESCAN00020623 Fiat(UDS)                      Enable maximum number of RCR-RP transmission limitation
                    Sa     Fixed             ESCAN00020760 UDS                            Service $22 responds with wrong NRC $13 instead of $31 in case single DID mode has been selected
                    Sa     Modified          ESCAN00020840 All                            Compiler error message on local variable pointer to ROM using V_MEMROM0 define
                    Sa     Modified          ESCAN00019437 All                            Compiler error in desc.c with mem-model huge
2007-07-23 05.04.00
                    Sa     Fixed             ESCAN00021556 Fiat(UDS)                      Wrong tester address reported with DescGetTesterAddress()
                    Gz     Added             ESCAN00021391 Fiat(UDS)                      Changed NRC handling - session check before format check
                    Sa     Added             ESCAN00021530 UDS                            Support for custom rx communication path handling on service $28
                    Sa     Fixed             ESCAN00021336 All                            use lower case characters for names of generated files (AppDesc.h => appdesc.h)
                    Sa     Modified          ESCAN00021523 All                            Removed the first frame length dependency on Vector TP for for ring-buffer usage
                    Sa     Added             ESCAN00021528 All                            Stopping RepeatedServiceCall in RingBufferCancel
                    Sa     Added             ESCAN00021567 All                            Use case catch on RingBuffer usage with SPRMIB=1
2007-09-07 05.05.00
                    Sa     Fixed             ESCAN00021652 Fiat(UDS)                      No session check on services $22, $2A and $2C
                    Sa     Modified          ESCAN00021747 Fiat(UDS)                      Changed limitation of maximum number of elements per dynamically defined DID from 32 to 255
                    Sa     Fixed             ESCAN00021610 UDS                            Incorrect NRC on an invalidly formatted writeDataByIdentifier ($2e) request for non-writeable DID
                    Sa     Fixed             ESCAN00021615 UDS                            Incorrect NRC on service with parameter identifier not supported in active session
                    Sa     Fixed             ESCAN00021800 UDS                            Wrong response lenght on service $22 in single DID mode
                    Sa     Added             ESCAN00022074 UDS                            Unified PID support on service $2E
                    Sa     Added             ESCAN00022075 UDS                            Unified PID support on service $22 in single PID mode
                    Sa     Fixed             ESCAN00022068 All                            Include appDesc.h changed to appdesc.h
                    Sa     Modified          ESCAN00021654 All                            Explicit suppression of NRC $11 on functional request
                    Sa     Modified          ESCAN00021252 All                            Compiler warnings for unreferenced variable and function declaration
                    Sa     Modified          ESCAN00022254 All                            Adaptions for the interrupt support of the latest VStdLib version
                    Sa     Added             ESCAN00021847 All                            Access to the current communication parameters
2007-10-22 05.05.01
                    Sa     Fixed             ESCAN00022373 Fiat(UDS)                      Service $2C processor deletes all DID definitions on command clear single DID definition
                    Sa     Modified          ESCAN00022853 UDS                            Allow for service $10 the P2 time report not to be present
2007-10-26 05.05.02
                    -      -                 -             -                              No relevant changes available in this version.
2007-11-15 05.05.03
                    Sa     Added             ESCAN00023231 All                            Support multiple ECU numbers   
2007-12-17 05.05.04
                    -      -                 -             -                              No relevant changes available in this version.
2007-12-20 05.06.00
                    Sa     Fixed             ESCAN00022373 Fiat(UDS)                      Service $2C processor deletes all DID definitions on command clear single DID definition
                    Sa     Fixed             ESCAN00023846 Fiat(UDS)                      Service $2C processor always deletes all DID definitions on command clear single DID definition
                    Sa     Modified          ESCAN00022853 UDS                            Allow for service $10 the P2 time report not to be present
                    Sa     Modified          ESCAN00022868 UDS                            Consider CDD files having only SID based information (CANdescBasic like)
                    Sa     Added             ESCAN00023231 All                            Support multiple ECU numbers   
2008-01-22 05.06.01
                    Sa     Fixed             ESCAN00023944 Fiat(UDS)                      Invalid data responded with service $22 (ReadDataByIdentifier)
                    Sa     Modified          ESCAN00024147 UDS                            Specify suppressPosRes DescMsgContext member as read-only item and not read-write
                    Sa     Fixed             ESCAN00026046 All                            CANdesc responds on wrong CAN Id
                    Mhe    Added             ESCAN00024011 All                            Support dynamic normal addressing
                    Mhe    Added             ESCAN00024019 All                            Support far RAM Tp API         
2008-02-04 05.06.02
                    Sa     Fixed             ESCAN00024458 Fiat(UDS)                      Unexpected negative response on 'clear dynamic DID' request
2008-01-11 05.06.03
                    Sa     Fixed             ESCAN00024489 Fiat(UDS)                      Clearing a DynDID while in use may cause unpredictable behavior of CANdesc
                    Sa     Fixed             ESCAN00024523 Fiat(UDS)                      Compile error when service $2C is enabled
2008-03-14 05.06.04
                    Sa     Fixed             ESCAN00025027 Fiat(UDS)                      Service $2C accepts delete DynDID command with more request data than allowed
                    Mhe    Added             ESCAN00024951 UDS                            Internal support for read/write memory by address
                    Sa     Added             ESCAN00024929 UDS                            Allow unified PID handling for service $22 without multiple PID support
2008-04-03 05.06.05
                    Sa     Fixed             ESCAN00025753 UDS                            Supported PIDs will not be recognized if service $2F is also available in the system
2008-04-21 05.06.06
                    Sa     Fixed             ESCAN00026335 All                            Compile error with DynamicTP   
2008-05-23 05.06.07
                    Sa     Fixed             ESCAN00027058 UDS                            Compile error on generated main-handler with ring-buffer access an single PID mode
                    Sa     Fixed             ESCAN00026907 All                            On TMS320 wrong vstdlib memcopy API used
                    Sa     Fixed             ESCAN00027059 All                            Array out of boundary access error on generated main-handler with ring-buffer access
                    Sa     Modified          ESCAN00027061 All                            Minor DescICN driver optimization
                    Sa     Modified          ESCAN00027063 All                            MainHandler req-/resDataLen has misleading description for CANdescBasic
                    Ktw    Modified          ESCAN00027057 All                            Compile error on missing semicolon for DescSetNpmOnCanChannelActive macro expansion
                    Mhe    Added             ESCAN00026452 All                            Posthandler support for generated $23
2008-05-26 05.06.08
                    Sa     Fixed             ESCAN00027060 UDS                            Wrong assertion in case of negative response on service 0x22 and multiple PID mode
                    Sa     Fixed             ESCAN00027085 UDS                            Compile error on configurations without service $2A or $2C
                    Sa     Fixed             ESCAN00027084 All                            Compile error on non TMS320 platforms
2008-06-05 05.06.09
                    Sa     Fixed             ESCAN00027180 Fiat(UDS)                      Compile error if multiple DIDs supported with service 0x22
                    Sa     Fixed             ESCAN00027181 Fiat(UDS)                      Overwritten memory and wrong responses on service 0x2A with USDT responses reading DynDefinedDIDs
                    Sa     Fixed             ESCAN00027303 Fiat(UDS)                      No USDT periodic transmissions after single frame request with response
                    Sa     Fixed             ESCAN00026136 Fiat(UDS)                      FIAT SLP2: store F2xx in NVRAM and store F3xx in VRAM
2008-06-16 05.06.10
                    -      -                 -             -                              No relevant changes available in this version.
2008-06-27 05.07.00
                    Sa     Fixed             ESCAN00023944 Fiat(UDS)                      Invalid data responded with service $22 (ReadDataByIdentifier)
                    Sa     Fixed             ESCAN00024458 Fiat(UDS)                      Unexpected negative response on 'clear dynamic DID' request
                    Sa     Fixed             ESCAN00025027 Fiat(UDS)                      Service $2C accepts delete DynDID command with more request data than allowed
                    Sa     Fixed             ESCAN00027181 Fiat(UDS)                      Overwritten memory and wrong responses on service 0x2A with USDT responses reading DynDefinedDIDs
                    Sa     Fixed             ESCAN00027303 Fiat(UDS)                      No USDT periodic transmissions after single frame request with response
                    Sa     Fixed             ESCAN00026136 Fiat(UDS)                      FIAT SLP2: store F2xx in NVRAM and store F3xx in VRAM
                    Sa     Fixed             ESCAN00027058 UDS                            Compile error on generated main-handler with ring-buffer access an single PID mode
                    Sa     Fixed             ESCAN00027060 UDS                            Wrong assertion in case of negative response on service 0x22 and multiple PID mode
                    Sa     Modified          ESCAN00024147 UDS                            Specify suppressPosRes DescMsgContext member as read-only item and not read-write
                    Sa     Added             ESCAN00024929 UDS                            Allow unified PID handling for service $22 without multiple PID support
                    Mhe    Added             ESCAN00024951 UDS                            Internal support for read/write memory by address
                    Sa     Fixed             ESCAN00026046 All                            CANdesc responds on wrong CAN Id
                    Sa     Fixed             ESCAN00026907 All                            On TMS320 wrong vstdlib memcopy API used
                    Sa     Fixed             ESCAN00027059 All                            Array out of boundary access error on generated main-handler with ring-buffer access
                    Sa     Modified          ESCAN00027061 All                            Minor DescICN driver optimization
                    Sa     Modified          ESCAN00027063 All                            MainHandler req-/resDataLen has misleading description for CANdescBasic
                    Sa     Modified          ESCAN00027057 All                            Compile error on missing semicolon for DescSetNpmOnCanChannelActive macro expansion
                    Mhe    Added             ESCAN00024011 All                            Support dynamic normal addressing
                    Mhe    Added             ESCAN00024019 All                            Support far RAM Tp API         
                    Mhe    Added             ESCAN00026452 All                            Posthandler support for generated $23
2008-07-11 05.07.01
                    -      -                 -             -                              No relevant changes available in this version.
2008-08-08 05.07.02
                    Sa     Fixed             ESCAN00028409 All                            No response on request         
2008-10-02 05.07.03
                    Sa     Modified          ESCAN00029770 All                            Compiler warning for enumeration item with value 0xFF on enum-type = char
                    Sa     Added             ESCAN00030116 All                            Provide IsoTp-Callback prototypes in desc.h/danisisotp.h
                    Sa     Added             ESCAN00030087 All                            Support of internal diagnostic requests over IsoTP
2008-10-30 05.07.04
                    Sa     Fixed             ESCAN00030759 All                            Generator stops with internal error during processing 'MainHandlerOnProtocolService' that contains muxed component
                    Sa     Fixed             ESCAN00030981 All                            Switch to surpress multitiple addressing check
                    Sa     Fixed             ESCAN00029915 All                            CANdesc files can not be written
                    Sa     Modified          ESCAN00031158 All                            Ring-buffer feature enabled for non-ISO-TP DANIS drivers
                    Sa     Added             ESCAN00031156 All                            Added DANIS abstraction for testerId and comChannel
2008-11-26 05.07.05
                    -      -                 -             -                              No relevant changes available in this version.
2008-12-15 05.07.06
                    Ktw    Fixed             ESCAN00031533 All                            Compiler Warning: Unused variable tpTxChannel
2009-02-02 05.07.07
                    Sa     Fixed             ESCAN00027182 All                            Compiler Warning: variable "svcInstFailedBytePosMask" is possibly uninitialized
2009-02-17 05.07.08
                    -      -                 -             -                              No relevant changes available in this version.
2009-03-09 05.07.09
                    Sa     Fixed             ESCAN00033611 Fiat(UDS)                      Missing final response if service 0x2A transmits over USDT messages
------------------------------------------------------------------------------------------------------------------------
 */


/* -----------------------------------------------------------------------------
    &&&~ Includes
 ----------------------------------------------------------------------------- */

/* Physical layer specific API */
#include "can_inc.h"
/* Check if CCL is available and take into account its configuration */
#if defined (VGEN_ENABLE_CCL)
# include "ccl_cfg.h"
#endif

/* Check if CCLcom is available and take into account its configuration */
#if defined (VGEN_ENABLE_CCLCOM)
# include "cclcom_cfg.h"
#endif

/* Make all CANdesc specific typedefs available */
#include "desc.h"
/* Include the implementation prototypes for prototype checks */
#include "appdesc.h"



/* -----------------------------------------------------------------------------
    &&&~ Implementation
 ----------------------------------------------------------------------------- */

#if defined (DESC_ENABLE_DEBUG_USER ) || defined (DESC_ENABLE_DEBUG_INTERNAL)
# if defined (CCL_ENABLE_ERROR_HOOK) || defined (CCLCOM_ENABLE_ERROR_HOOK)
/* CCL takes care about this function */
# else
/* ********************************************************************************
 * Function name:ApplDescFatalError
 * Description: This function will be called each time while the debug mode is active a
 * CANdesc fault has been detected. If you reach this function it makes no sence to continue the tests since CANdesc
 * will not operate properly until next start of the ECU.
 * Returns:  nothing
 * Parameter(s):
 *   - errorCode:
 *       - The assert code text equivalent can be found in desc.h (kDescAssert....).
 *       - Access type: read
 *   - lineNumber:
 *       - Since the same fault could be cales on many places the line number shows where exactly it occured.
 *       - Access type: read
 * Particularitie(s) and limitation(s):
 *   - Set a break point at this place to know during the ECU development if you ran onto it.
 ******************************************************************************** */
void DESC_API_CALLBACK_TYPE ApplDescFatalError(vuint8 errorCode, vuint16 lineNumber)
{
  /*<<TBD>> Remove this comment once you have completely implemented this function!!!*/
  /* Avoid warnings */
  DESC_IGNORE_UNREF_PARAM(errorCode);
  DESC_IGNORE_UNREF_PARAM(lineNumber);

  /* When fatal error occurs, cause an ECU hang up at this point.
   * Please set break point at this line to investigate both parameter values. */
  for(;;);
}
# endif
#endif

/*  ********************************************************************************
 * Function name:ApplDescRcrRpConfirmation
 * Description:Will be called only if "DescForceRcrRpResponse" has been previously called.
 * Returns:  nothing
 * Parameter(s):
 *   - status:
 *       - Current RCR-RP transmission status (kDescOk/kDescFailed).
 *       - Access type: read
 * Particularitie(s) and limitation(s):
 *   - The function "DescProcessingDone" may be called to close the service processing.
 *   - The function "DescSetNegResponse" may be called to set a negative response.
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescRcrRpConfirmation(vuint8 status)
{
  /* <<TBD>> Remove this comment once you have completely implemented this function!!! */
    /* Avoid warnings */
  DESC_CONTEXT_PARAM_DUMMY_USE;

  /* Check the transmission status */
  if(status == kDescOk)
  {
    /* "Response Pending" was just successfully sent, you can perform any further action knowing that
     * from now on the tester has restarted its response timeout timer and set the next time to P2* (P2Ex) 
     */
  }
  else
  {
    /* There was some transmission error and the tester didn't received the RCR-RP response. You can decide to go on
     * or to reset the application activity you have started and wait for a new request from the tester. 
     */
  }
;
}


/*  ********************************************************************************
 * Function name:ApplDescOnTransitionSecurityAccess
 * Description:Notification function for state change of the given state group, defined by CANdelaStudio.
 * Returns:  nothing
 * Parameter(s):
 *   - newState:
 *       - The state which will be set.
 *       - Access type: read
 *   - formerState:
 *       - The current state of this state group.
 *       - Access type: read
 * Particularitie(s) and limitation(s):
 *   - The function "DescProcessingDone" may not be called.
 *   - The function "DescSetNegResponse" may not be called.
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescOnTransitionSecurityAccess(DescStateGroup newState, DescStateGroup formerState)
{
  /* <<TBD>> Remove this comment once you have completely implemented this function!!! */
   /* This is only a notification function. Using the "formerState" and the "newState" 
   * parameter you can distinguish the different transitions for this state group.
   */
  /* Avoids warnings */
  DESC_IGNORE_UNREF_PARAM(newState);
  DESC_IGNORE_UNREF_PARAM(formerState);
;
}








/*  ********************************************************************************
 * Function name:ApplDescTester_PrresentTesterPresent (Service request header:$3E $0 )
 * Description:If the External Tool sends $3E $80  non response will be sent by ECU.
 * Returns:  nothing
 * Parameter(s):
 ********************************************************************************  */
void DESC_API_CALLBACK_TYPE ApplDescTester_PrresentTesterPresent(DescMsgContext* pMsgContext)
{
  /* <<TBD>> Remove this comment once you have completely implemented this function!!! */
  /* Contains no request data */
  /* Contains no response data */
  /* User service processing finished. */
  DescProcessingDone();
}

/* ********************************************************************************
 * Function name:ApplDescOnCommunicationDisable
 * Description: Notification function that the communication has been disabled.
 * Returns:  none
 * Parameter(s):none
 * Particularitie(s) and limitation(s):
 *   - The function "DescProcessingDone" may not be called.
 *   - The function "DescSetNegResponse" may not be called.
 ******************************************************************************** */
void DESC_API_CALLBACK_TYPE ApplDescOnCommunicationDisable(void)
{
  /*<<TBD>> Remove this comment once you have completely implemented this function!!!*/
  /* The requested group of messages will no more be sent onto the communication bus. */
}

/* ********************************************************************************
 * Function name:ApplDescOnCommunicationEnable
 * Description: Notification function that the communication has been restored.
 * Returns:  none
 * Parameter(s):none
 * Particularitie(s) and limitation(s):
 *   - The function "DescProcessingDone" may not be called.
 *   - The function "DescSetNegResponse" may not be called.
 ******************************************************************************** */
void DESC_API_CALLBACK_TYPE ApplDescOnCommunicationEnable(void)
{
  /*<<TBD>> Remove this comment once you have completely implemented this function!!!*/
  /* The requested group of messages will be able to be sent onto the communication bus. */
}

#if defined (DESC_ENABLE_COMM_CTRL_SUBNET_SUPPORT)
/* ********************************************************************************
 * Function name:ApplDescSetCommMode
 * Description:Manipulate application specific channels (LIN, MOST, etc.)
 * Returns:  nothing
 * Parameter(s):
 *   - commControlInfo->subNetTxNumber:
 *       - The application shall use this parameter to decied on which physical channels the communiaction will be manipulated.
 *       - Access type: read
 *   - commControlInfo->commCtrlChannel:
 *       - The application determines on which channel the communication will be manipulated (kDescCommControlCanCh01 - kDescCommControlCanCh14).
 *       - Access type: write
 *   - commControlInfo->rxPathState:
 *       - Activity type: kDescCommControlStateEnable - enables the communication; kDescCommControlStateDisable - disables it.
 *       - Access type: read
 *   - commControlInfo->txPathState:
 *       - Activity type: kDescCommControlStateEnable - enables the communication; kDescCommControlStateDisable - disables it.
 *       - Access type: read
 *   - commControlInfo->msgTypes:
 *       - Message group: kDescCommControlMsgAppl - application; kDescCommControlMsgNm - NM; 
 *       - Access type: read
 *   - commControlInfo->reqCommChannel:
 *       - The current CAN channel on which the CommControl request is received.
 *       - Access type: read
 * Particularitie(s) and limitation(s):
 *   - The function "DescProcessingDone" may not be called.
 *   - The function "DescSetNegResponse" may not be called.
 ******************************************************************************** */
void DESC_API_CALLBACK_TYPE ApplDescSetCommMode(DescOemCommControlInfo *commControlInfo)
{
  switch(commControlInfo->subNetNumber)
  {
    case kDescCommControlSubNetNumAll:/* Enable/Disable all comm channels (CAN, LIN, etc.)*/
      /* !!! Process application specific channels (e.g. LIN) CAN are already enabled/disabled by CANdesc */
      break;
    case kDescCommControlSubNetNumRx:/* Enable/Disable only the request RX comm channel (CAN)*/
      /* Nothing to do - CANdesc handles the CAN channels */
      break;
    default:
      /* Dispatch subnetworks */
      /* Assumed subnet to CAN mapping: 
       * SubNet 1: LIN 
       * SubNet 2: CAN1 
       * SubNet 3: CAN2 
       */
      switch(commControlInfo->subNetNumber)
      {
        case kDescCommControlSubNetNum01:
          /* LIN network */
          /* !!! Process application specific channels (e.g. LIN)*/
          break;
        case kDescCommControlSubNetNum02:          /* CAN1 */
        case kDescCommControlSubNetNum03:          /* CAN2 */
          /* Nothing to do - CANdesc handles all CANs */
          break;
        default:break;
      }
  }
}
#endif

#if defined (DESC_ENABLE_RX_COMM_CONTROL)
/* ********************************************************************************
 * Function name:ApplDescSetCommModeOnRxPath
 * Description: Manipulates only the RX path on CAN. For the other networks (if any) such LIN, MOST, etc. reffer to the
 *              ApplDescSetCommMode API.
 * Returns:  nothing
 * Parameter(s):
 *   - commControlInfo->subNetTxNumber:
 *       - The application shall use this parameter to decied on which physical channels the communiaction will be manipulated.
 *       - Access type: read
 *   - commControlInfo->commCtrlChannel:
 *       - The application determines on which channel the communication will be manipulated (kDescCommControlCanCh01 - kDescCommControlCanCh14).
 *       - Access type: write
 *   - commControlInfo->rxPathState:
 *       - Activity type: kDescCommControlStateEnable - enables the communication; kDescCommControlStateDisable - disables it.
 *       - Access type: read
*   - commControlInfo->rxPathState: - irrelevant for this API since it processes only the RX path!!!
 *       - Activity type: kDescCommControlStateEnable - enables the communication; kDescCommControlStateDisable - disables it.
 *       - Access type: read
 *   - commControlInfo->msgTypes:
 *       - Message group: kDescCommControlMsgAppl - application; kDescCommControlMsgNm - NM; 
 *       - Access type: read
 *   - commControlInfo->reqCommChannel:
 *       - The current CAN channel on which the CommControl request is received.
 *       - Access type: read
 * Particularitie(s) and limitation(s):
 *   - The function "DescProcessingDone" may not be called.
 *   - The function "DescSetNegResponse" may not be called.
 ******************************************************************************** */
void DESC_API_CALLBACK_TYPE ApplDescSetCommModeOnRxPath(DescOemCommControlInfo *commControlInfo)
{
  if((commControlInfo->rxPathState & kDescCommControlStateEnable) != 0)
  {
    /* _DrvCanSetRxOnlineMode(commControlInfo.commCtrlChannel, g_descCommControlInfo.msgTypes); */
  }
  else
  {
    /* _DrvCanSetRxOfflineMode(commControlInfo.commCtrlChannel, g_descCommControlInfo.msgTypes); */
  }
}
#endif


#if defined (DESC_ENABLE_MULTI_CFG_SUPPORT)
/* ********************************************************************************
 * Function name:ApplDescIsDataIdSupported
 * Description: Additionaly reject a supported PID (multi ECU configuration)
 * Returns:  kDescTrue - if still supported, kDescFalse - if not supported
 * Parameter(s):The PID number
 * Particularitie(s) and limitation(s):
 *   - The function "DescProcessingDone" may not be called.
 *   - The function "DescSetNegResponse" may not be called.
 ******************************************************************************** */
DescBool ApplDescIsDataIdSupported(vuint16 pid)
{
  return kDescTrue;
}
#endif



