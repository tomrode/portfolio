/**************************************************************
*  File D:\rtc_wa_KL15\BCM_NET_DIAG\CUSW\TOOLS\CANGEN\Sources\tp_par.c
*  generated at Wed Dec 04 09:34:27 2013
*             Toolversion:   427
*               Bussystem:   CAN
*
*  generated out of CANdb:   D:\rtc_wa_KL15\BCM_NET_DIAG\CUSW\TOOLS\CANGEN\CCAN.dbc
*                            D:\rtc_wa_KL15\BCM_NET_DIAG\CUSW\TOOLS\CANGEN\BCAN.dbc

*            Manufacturer:   Fiat
*                for node:   BCM
*   Generation parameters:   Target system = FR60
*                            Compiler      = Fujitsu / Softune
*
* License information:       
*   -    Serialnumber:       CBD0800214
*   - Date of license:       19.9.2008
*
***************************************************************
Software is licensed for:    
Magneti Marelli Sistemi Elettronici S.p.A.
Fiat / SLP2 / MB91460P / Fujitsu Softune V60L01 / MB91F467
**************************************************************/
#include "tp_cfg.h"

#ifdef OSEKTPMC_C_MODULE



V_MEMROM0 V_MEMROM1 CanTransmitHandle V_MEMROM2 kTpTxHandle_Field[2] = {
	 NETC_TX_TxDynamicMsg0_0
	,NETC_TX_TxDynamicMsg0_1
 };

V_MEMROM0 V_MEMROM1 CanTransmitHandle V_MEMROM2 kTpRxHandle_Field[2] = {
	 NETC_TX_TxDynamicMsg1_0
	,NETC_TX_TxDynamicMsg1_1
 };


#else
#error "This file is included by the TPMC module. Don't include it in your makefile!"
#endif


