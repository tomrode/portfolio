/**************************************************************
*  File D:\rtc_wa_KL15\BCM_NET_DIAG\CUSW\TOOLS\CANGEN\Sources\can_msg.c
*  generated at Wed Dec 04 09:34:27 2013
*             Toolversion:   427
*               Bussystem:   CAN
*
*  generated out of CANdb:   D:\rtc_wa_KL15\BCM_NET_DIAG\CUSW\TOOLS\CANGEN\CCAN.dbc
*                            D:\rtc_wa_KL15\BCM_NET_DIAG\CUSW\TOOLS\CANGEN\BCAN.dbc

*            Manufacturer:   Fiat
*                for node:   BCM
*   Generation parameters:   Target system = FR60
*                            Compiler      = Fujitsu / Softune
*
* License information:       
*   -    Serialnumber:       CBD0800214
*   - Date of license:       19.9.2008
*
***************************************************************
Software is licensed for:    
Magneti Marelli Sistemi Elettronici S.p.A.
Fiat / SLP2 / MB91460P / Fujitsu Softune V60L01 / MB91F467
**************************************************************/
//#include "can_inc.h"
#include "h\v_def.h"
#include "h\can_def.h"
#include "h\can_cfg.h"
#include "h\can_msg.h"

#if defined (V_MEMROM0)
#else
	#define V_MEMROM0
#endif



V_MEMROM0 V_MEMROM1 canuint8 V_MEMROM2 kGenMainVersion   = (canuint8)(0xff & ((GEN_VERSION) >> 8));
V_MEMROM0 V_MEMROM1 canuint8 V_MEMROM2 kGenSubVersion    = (canuint8)(0xff & GEN_VERSION);
V_MEMROM0 V_MEMROM1 canuint8 V_MEMROM2 kGenBugfixVersion = (canuint8)(0xff & GEN_BUGFIX_VERSION);

/* Start FR60 skeleton C-File ----------------------------------------*/

/******************************************************************************
| Version check
|*****************************************************************************/
#if( VTOOL_SKELETON_VERSION != 0x01 )
  #error "Header and source skeleton file in tool directory are inconsistent!"
#endif
#if( VTOOL_SKELETON_BUGFIX_VERSION != 0x00 )
  #error "Different versions of bugfix in Header and Source skeleton file used!"
#endif


/* End FR60 skeleton C-File ------------------------------------------*/


/*************************************************************/
/* Databuffer for send objects                               */
/*************************************************************/
_c_STATUS_C_BCM2_buf MEMORY_FAR  STATUS_C_BCM2;
_c_HVAC_INFO_buf MEMORY_FAR  HVAC_INFO;
_c_CBC_PT4_buf MEMORY_FAR  CBC_PT4;
_c_BCM_KEYON_COUNTER_C_CAN_buf MEMORY_FAR  BCM_KEYON_COUNTER_C_CAN;
_c_IBS4_buf MEMORY_FAR  IBS4;
_c_ECU_APPL_BCM_0_buf MEMORY_FAR  ECU_APPL_BCM_0;
_c_CONFIGURATION_DATA_CODE_REQUEST_0_buf MEMORY_FAR  CONFIGURATION_DATA_CODE_REQUEST_0;
_c_CBC_VTA_0_buf MEMORY_FAR  CBC_VTA_0;
_c_DIAGNOSTIC_RESPONSE_BCM_0_buf MEMORY_FAR  DIAGNOSTIC_RESPONSE_BCM_0;
_c_STATUS_B_CAN2_buf MEMORY_FAR  STATUS_B_CAN2;
_c_EXTERNAL_LIGHTS_0_buf MEMORY_FAR  EXTERNAL_LIGHTS_0;
_c_ASBM1_CONTROL_buf MEMORY_FAR  ASBM1_CONTROL;
_c_IBS2_buf MEMORY_FAR  IBS2;
_c_VEHICLE_SPEED_ODOMETER_0_buf MEMORY_FAR  VEHICLE_SPEED_ODOMETER_0;
_c_STATUS_C_BCM_buf MEMORY_FAR  STATUS_C_BCM;
_c_STATUS_B_CAN_buf MEMORY_FAR  STATUS_B_CAN;
_c_IBS3_buf MEMORY_FAR  IBS3;
_c_IBS1_buf MEMORY_FAR  IBS1;
_c_BATTERY_INFO_buf MEMORY_FAR  BATTERY_INFO;
_c_CFG_Feature_buf MEMORY_FAR  CFG_Feature;
_c_CBC_PT3_buf MEMORY_FAR  CBC_PT3;
_c_CBC_PT1_buf MEMORY_FAR  CBC_PT1;
_c_WAKE_C_BCM_buf MEMORY_FAR  WAKE_C_BCM;
_c_CBC_PT2_buf MEMORY_FAR  CBC_PT2;
_c_BCM_COMMAND_buf MEMORY_FAR  BCM_COMMAND;
_c_BCM_CODE_TRM_REQUEST_buf MEMORY_FAR  BCM_CODE_TRM_REQUEST;
_c_BCM_CODE_ESL_REQUEST_buf MEMORY_FAR  BCM_CODE_ESL_REQUEST;
_c_BCM_MINICRYPT_ACK_buf MEMORY_FAR  BCM_MINICRYPT_ACK;
_c_IMMO_CODE_RESPONSE_buf MEMORY_FAR  IMMO_CODE_RESPONSE;
_c_TxDynamicMsg0_0_buf MEMORY_FAR  TxDynamicMsg0_0;
_c_TxDynamicMsg1_0_buf MEMORY_FAR  TxDynamicMsg1_0;
_c_ECU_APPL_BCM_1_buf MEMORY_FAR  ECU_APPL_BCM_1;
_c_DTO_BCM_buf MEMORY_FAR  DTO_BCM;
_c_CONFIGURATION_DATA_CODE_REQUEST_1_buf MEMORY_FAR  CONFIGURATION_DATA_CODE_REQUEST_1;
_c_IBS_2_buf MEMORY_FAR  IBS_2;
_c_HUMIDITY_1000L_buf MEMORY_FAR  HUMIDITY_1000L;
_c_COMPASS_A1_buf MEMORY_FAR  COMPASS_A1;
_c_CBC_I5_buf MEMORY_FAR  CBC_I5;
_c_AMB_TEMP_DISP_buf MEMORY_FAR  AMB_TEMP_DISP;
_c_WCPM_STATUS_buf MEMORY_FAR  WCPM_STATUS;
_c_GE_B_buf MEMORY_FAR  GE_B;
_c_ENVIRONMENTAL_CONDITIONS_buf MEMORY_FAR  ENVIRONMENTAL_CONDITIONS;
_c_CBC_VTA_1_buf MEMORY_FAR  CBC_VTA_1;
_c_DIAGNOSTIC_RESPONSE_BCM_1_buf MEMORY_FAR  DIAGNOSTIC_RESPONSE_BCM_1;
_c_GW_C_I2_buf MEMORY_FAR  GW_C_I2;
_c_DAS_B_buf MEMORY_FAR  DAS_B;
_c_CBC_I2_buf MEMORY_FAR  CBC_I2;
_c_CBC_I1_buf MEMORY_FAR  CBC_I1;
_c_STATUS_C_CAN_buf MEMORY_FAR  STATUS_C_CAN;
_c_SWS_8_buf MEMORY_FAR  SWS_8;
_c_StW_Actn_Rq_1_buf MEMORY_FAR  StW_Actn_Rq_1;
_c_PAM_B_buf MEMORY_FAR  PAM_B;
_c_GW_C_I3_buf MEMORY_FAR  GW_C_I3;
_c_VEHICLE_SPEED_ODOMETER_1_buf MEMORY_FAR  VEHICLE_SPEED_ODOMETER_1;
_c_GW_C_I6_buf MEMORY_FAR  GW_C_I6;
_c_DYNAMIC_VEHICLE_INFO2_buf MEMORY_FAR  DYNAMIC_VEHICLE_INFO2;
_c_NWM_BCM_buf MEMORY_FAR  NWM_BCM;
_c_STATUS_BCM2_buf MEMORY_FAR  STATUS_BCM2;
_c_STATUS_BCM_buf MEMORY_FAR  STATUS_BCM;
_c_EXTERNAL_LIGHTS_1_buf MEMORY_FAR  EXTERNAL_LIGHTS_1;
_c_CBC_I4_buf MEMORY_FAR  CBC_I4;
_c_TxDynamicMsg0_1_buf MEMORY_FAR  TxDynamicMsg0_1;
_c_TxDynamicMsg1_1_buf MEMORY_FAR  TxDynamicMsg1_1;


/*************************************************************/
/* Databuffer for receive objects                            */
/*************************************************************/
_c_STATUS_C_OCM_buf MEMORY_FAR  STATUS_C_OCM;
_c_CFG_DATA_CODE_RSP_OCM_buf MEMORY_FAR  CFG_DATA_CODE_RSP_OCM;
_c_CFG_DATA_CODE_RSP_AHLM_buf MEMORY_FAR  CFG_DATA_CODE_RSP_AHLM;
_c_CFG_DATA_CODE_RSP_DTCM_buf MEMORY_FAR  CFG_DATA_CODE_RSP_DTCM;
_c_CFG_DATA_CODE_RSP_RFHM_buf MEMORY_FAR  CFG_DATA_CODE_RSP_RFHM;
_c_CFG_DATA_CODE_RSP_DASM_buf MEMORY_FAR  CFG_DATA_CODE_RSP_DASM;
_c_CFG_DATA_CODE_RSP_SCCM_buf MEMORY_FAR  CFG_DATA_CODE_RSP_SCCM;
_c_CFG_DATA_CODE_RSP_HALF_buf MEMORY_FAR  CFG_DATA_CODE_RSP_HALF;
_c_CFG_DATA_CODE_RSP_ORC_buf MEMORY_FAR  CFG_DATA_CODE_RSP_ORC;
_c_CFG_DATA_CODE_RSP_PAM_buf MEMORY_FAR  CFG_DATA_CODE_RSP_PAM;
_c_CFG_DATA_CODE_RSP_EPB_buf MEMORY_FAR  CFG_DATA_CODE_RSP_EPB;
_c_CFG_DATA_CODE_RSP_TRANSMISSION_buf MEMORY_FAR  CFG_DATA_CODE_RSP_TRANSMISSION;
_c_CFG_DATA_CODE_RSP_BSM_buf MEMORY_FAR  CFG_DATA_CODE_RSP_BSM;
_c_CFG_DATA_CODE_RSP_EPS_buf MEMORY_FAR  CFG_DATA_CODE_RSP_EPS;
_c_CFG_DATA_CODE_RSP_ECM_buf MEMORY_FAR  CFG_DATA_CODE_RSP_ECM;
_c_STATUS_C_DASM_buf MEMORY_FAR  STATUS_C_DASM;
_c_STATUS_C_AHLM_buf MEMORY_FAR  STATUS_C_AHLM;
_c_STATUS_C_HALF_buf MEMORY_FAR  STATUS_C_HALF;
_c_STATUS_C_ESL_buf MEMORY_FAR  STATUS_C_ESL;
_c_STATUS_C_EPB_buf MEMORY_FAR  STATUS_C_EPB;
_c_HALF_C_Warning_RQ_buf MEMORY_FAR  HALF_C_Warning_RQ;
_c_StW_Actn_Rq_0_buf MEMORY_FAR  StW_Actn_Rq_0;
_c_STATUS_C_TRANSMISSION_buf MEMORY_FAR  STATUS_C_TRANSMISSION;
_c_VIN_0_buf MEMORY_FAR  VIN_0;
_c_MOT3_buf MEMORY_FAR  MOT3;
_c_ECM_1_buf MEMORY_FAR  ECM_1;
_c_STATUS_C_DTCM_buf MEMORY_FAR  STATUS_C_DTCM;
_c_DAS_A2_buf MEMORY_FAR  DAS_A2;
_c_PAM_2_buf MEMORY_FAR  PAM_2;
_c_EPB_A1_buf MEMORY_FAR  EPB_A1;
_c_DAS_A1_buf MEMORY_FAR  DAS_A1;
_c_BSM_4_buf MEMORY_FAR  BSM_4;
_c_BSM_2_buf MEMORY_FAR  BSM_2;
_c_DTCM_A1_buf MEMORY_FAR  DTCM_A1;
_c_SC_buf MEMORY_FAR  SC;
_c_ORC_YRS_DATA_buf MEMORY_FAR  ORC_YRS_DATA;
_c_MOTGEAR2_buf MEMORY_FAR  MOTGEAR2;
_c_MOT4_buf MEMORY_FAR  MOT4;
_c_BSM_YRS_DATA_buf MEMORY_FAR  BSM_YRS_DATA;
_c_ASR4_buf MEMORY_FAR  ASR4;
_c_ASR3_buf MEMORY_FAR  ASR3;
_c_RFHUB_A4_buf MEMORY_FAR  RFHUB_A4;
_c_IPC_CFG_Feature_buf MEMORY_FAR  IPC_CFG_Feature;
_c_ESL_CODE_RESPONSE_buf MEMORY_FAR  ESL_CODE_RESPONSE;
_c_IMMO_CODE_REQUEST_buf MEMORY_FAR  IMMO_CODE_REQUEST;
_c_TRM_MKP_KEY_buf MEMORY_FAR  TRM_MKP_KEY;
_c_RFHUB_A3_buf MEMORY_FAR  RFHUB_A3;
_c_APPL_ECU_BCM_0_buf MEMORY_FAR  APPL_ECU_BCM_0;
_c_STATUS_C_SCCM_buf MEMORY_FAR  STATUS_C_SCCM;
_c_STATUS_C_RFHM_buf MEMORY_FAR  STATUS_C_RFHM;
_c_IPC_A1_buf MEMORY_FAR  IPC_A1;
_c_STATUS_C_SPM_buf MEMORY_FAR  STATUS_C_SPM;
_c_STATUS_C_IPC_buf MEMORY_FAR  STATUS_C_IPC;
_c_STATUS_C_GSM_buf MEMORY_FAR  STATUS_C_GSM;
_c_STATUS_C_EPS_buf MEMORY_FAR  STATUS_C_EPS;
_c_STATUS_C_BSM_buf MEMORY_FAR  STATUS_C_BSM;
_c_STATUS_C_ECM2_buf MEMORY_FAR  STATUS_C_ECM2;
_c_STATUS_C_ECM_buf MEMORY_FAR  STATUS_C_ECM;
_c_STATUS_C_ORC_buf MEMORY_FAR  STATUS_C_ORC;
_c_RFHUB_A2_buf MEMORY_FAR  RFHUB_A2;
_c_RFHUB_A1_buf MEMORY_FAR  RFHUB_A1;
_c_PAM_1_buf MEMORY_FAR  PAM_1;
_c_WAKE_C_RFHM_buf MEMORY_FAR  WAKE_C_RFHM;
_c_WAKE_C_SCCM_buf MEMORY_FAR  WAKE_C_SCCM;
_c_WAKE_C_ESL_buf MEMORY_FAR  WAKE_C_ESL;
_c_WAKE_C_EPB_buf MEMORY_FAR  WAKE_C_EPB;
_c_WAKE_C_BSM_buf MEMORY_FAR  WAKE_C_BSM;
_c_MOT1_buf MEMORY_FAR  MOT1;
_c_GE_buf MEMORY_FAR  GE;
_c_TRM_CODE_RESPONSE_buf MEMORY_FAR  TRM_CODE_RESPONSE;
_c_ORC_A2_buf MEMORY_FAR  ORC_A2;
_c_CRO_BCM_buf MEMORY_FAR  CRO_BCM;
_c_CFG_DATA_CODE_RSP_RBSS_buf MEMORY_FAR  CFG_DATA_CODE_RSP_RBSS;
_c_CFG_DATA_CODE_RSP_LBSS_buf MEMORY_FAR  CFG_DATA_CODE_RSP_LBSS;
_c_CFG_DATA_CODE_RSP_ICS_buf MEMORY_FAR  CFG_DATA_CODE_RSP_ICS;
_c_CFG_DATA_CODE_RSP_CSWM_buf MEMORY_FAR  CFG_DATA_CODE_RSP_CSWM;
_c_CFG_DATA_CODE_RSP_CDM_buf MEMORY_FAR  CFG_DATA_CODE_RSP_CDM;
_c_CFG_DATA_CODE_RSP_AMP_buf MEMORY_FAR  CFG_DATA_CODE_RSP_AMP;
_c_CFG_DATA_CODE_RSP_ETM_buf MEMORY_FAR  CFG_DATA_CODE_RSP_ETM;
_c_CFG_DATA_CODE_RSP_DSM_buf MEMORY_FAR  CFG_DATA_CODE_RSP_DSM;
_c_CFG_DATA_CODE_RSP_PDM_buf MEMORY_FAR  CFG_DATA_CODE_RSP_PDM;
_c_CFG_DATA_CODE_RSP_ECC_buf MEMORY_FAR  CFG_DATA_CODE_RSP_ECC;
_c_CFG_DATA_CODE_RSP_DDM_buf MEMORY_FAR  CFG_DATA_CODE_RSP_DDM;
_c_CFG_DATA_CODE_RSP_IPC_buf MEMORY_FAR  CFG_DATA_CODE_RSP_IPC;
_c_STATUS_ICS_buf MEMORY_FAR  STATUS_ICS;
_c_STATUS_CDM_buf MEMORY_FAR  STATUS_CDM;
_c_STATUS_AMP_buf MEMORY_FAR  STATUS_AMP;
_c_FT_HVAC_STAT_buf MEMORY_FAR  FT_HVAC_STAT;
_c_DCM_P_MSG_buf MEMORY_FAR  DCM_P_MSG;
_c_DCM_D_MSG_buf MEMORY_FAR  DCM_D_MSG;
_c_NWM_PLGM_buf MEMORY_FAR  NWM_PLGM;
_c_NWM_ETM_buf MEMORY_FAR  NWM_ETM;
_c_NWM_DSM_buf MEMORY_FAR  NWM_DSM;
_c_NWM_PDM_buf MEMORY_FAR  NWM_PDM;
_c_NWM_DDM_buf MEMORY_FAR  NWM_DDM;
_c_APPL_ECU_BCM_1_buf MEMORY_FAR  APPL_ECU_BCM_1;
_c_TRIP_A_B_buf MEMORY_FAR  TRIP_A_B;
_c_TGW_A1_buf MEMORY_FAR  TGW_A1;
_c_ICS_MSG_buf MEMORY_FAR  ICS_MSG;
_c_HVAC_A1_buf MEMORY_FAR  HVAC_A1;
_c_DIRECT_INFO_buf MEMORY_FAR  DIRECT_INFO;
_c_HVAC_A4_buf MEMORY_FAR  HVAC_A4;
_c_STATUS_RRM_buf MEMORY_FAR  STATUS_RRM;
_c_STATUS_IPC_buf MEMORY_FAR  STATUS_IPC;
_c_STATUS_ECC_buf MEMORY_FAR  STATUS_ECC;
_c_HVAC_A2_buf MEMORY_FAR  HVAC_A2;
_c_PLG_A1_buf MEMORY_FAR  PLG_A1;
_c_ECC_A1_buf MEMORY_FAR  ECC_A1;
_c_NWM_RBSS_buf MEMORY_FAR  NWM_RBSS;
_c_NWM_LBSS_buf MEMORY_FAR  NWM_LBSS;
_c_NWM_ICS_buf MEMORY_FAR  NWM_ICS;
_c_NWM_CSWM_buf MEMORY_FAR  NWM_CSWM;
_c_NWM_CDM_buf MEMORY_FAR  NWM_CDM;
_c_NWM_AMP_buf MEMORY_FAR  NWM_AMP;
_c_NWM_ECC_buf MEMORY_FAR  NWM_ECC;
_c_NWM_IPC_buf MEMORY_FAR  NWM_IPC;
_c_CMCM_UNLK_buf MEMORY_FAR  CMCM_UNLK;
_c_CFG_RQ_buf MEMORY_FAR  CFG_RQ;


/*************************************************************/
/* Send structures                                           */
/*************************************************************/
V_MEMROM0 V_MEMROM1 tCanTxId0 V_MEMROM2 CanTxId0[kCanNumberOfTxObjects] = {
	 MK_STDID0(0x7d4)		/* id: 0x7D4, Name: "STATUS_C_BCM2", Handle: 0, [BC] */
	,MK_STDID0(0x7d0)		/* id: 0x7D0, Name: "HVAC_INFO", Handle: 1, [BC] */
	,MK_STDID0(0x7cc)		/* id: 0x7CC, Name: "CBC_PT4", Handle: 2, [BC] */
	,MK_STDID0(0x7ca)		/* id: 0x7CA, Name: "BCM_KEYON_COUNTER_C_CAN", Handle: 3, [BC] */
	,MK_STDID0(0x7c8)		/* id: 0x7C8, Name: "IBS4", Handle: 4, [BC] */
	,MK_EXTID0(0x1e7caff6)		/* id: 0x1E7CAFF6, Name: "ECU_APPL_BCM_0", Handle: 5, [BC] */
	,MK_EXTID0(0x1e114000)		/* id: 0x1E114000, Name: "CONFIGURATION_DATA_CODE_REQUEST_0", Handle: 6, [BC] */
	,MK_STDID0(0x6d2)		/* id: 0x6D2, Name: "CBC_VTA_0", Handle: 7, [BC] */
	,MK_EXTID0(0x18daf140)		/* id: 0x18DAF140, Name: "DIAGNOSTIC_RESPONSE_BCM_0", Handle: 8, [BC] */
	,MK_STDID0(0x5de)		/* id: 0x5DE, Name: "STATUS_B_CAN2", Handle: 9, [BC] */
	,MK_STDID0(0x5d8)		/* id: 0x5D8, Name: "EXTERNAL_LIGHTS_0", Handle: 10, [BC] */
	,MK_STDID0(0x5d6)		/* id: 0x5D6, Name: "ASBM1_CONTROL", Handle: 11, [BC] */
	,MK_STDID0(0x5d4)		/* id: 0x5D4, Name: "HVAC_INFO2", Handle: 12, [BC] */
	,MK_STDID0(0x55f)		/* id: 0x55F, Name: "IBS2", Handle: 13, [BC] */
	,MK_STDID0(0x4f4)		/* id: 0x4F4, Name: "VEHICLE_SPEED_ODOMETER_0", Handle: 14, [BC] */
	,MK_STDID0(0x4e2)		/* id: 0x4E2, Name: "STATUS_C_BCM", Handle: 15, [BC] */
	,MK_STDID0(0x4e0)		/* id: 0x4E0, Name: "STATUS_B_CAN", Handle: 16, [BC] */
	,MK_STDID0(0x4d6)		/* id: 0x4D6, Name: "IBS3", Handle: 17, [BC] */
	,MK_STDID0(0x4d4)		/* id: 0x4D4, Name: "IBS1", Handle: 18, [BC] */
	,MK_STDID0(0x4ce)		/* id: 0x4CE, Name: "BATTERY_INFO", Handle: 19, [BC] */
	,MK_STDID0(0x3e8)		/* id: 0x3E8, Name: "CFG_Feature", Handle: 20, [BC] */
	,MK_STDID0(0x3e6)		/* id: 0x3E6, Name: "CBC_PT3", Handle: 21, [BC] */
	,MK_STDID0(0x3e4)		/* id: 0x3E4, Name: "CBC_PT1", Handle: 22, [BC] */
	,MK_EXTID0(0xc1cd000)		/* id: 0x0C1CD000, Name: "WAKE_C_BCM", Handle: 23, [BC] */
	,MK_STDID0(0x2ea)		/* id: 0x2EA, Name: "CBC_PT2", Handle: 24, [BC] */
	,MK_STDID0(0x1e8)		/* id: 0x1E8, Name: "BCM_COMMAND", Handle: 25, [BC] */
	,MK_STDID0(0xf7)		/* id: 0x0F7, Name: "BCM_CODE_TRM_REQUEST", Handle: 26, [BC] */
	,MK_STDID0(0xf6)		/* id: 0x0F6, Name: "BCM_CODE_ESL_REQUEST", Handle: 27, [BC] */
	,MK_STDID0(0xf5)		/* id: 0x0F5, Name: "BCM_MINICRYPT_ACK", Handle: 28, [BC] */
	,MK_STDID0(0xf4)		/* id: 0x0F4, Name: "IMMO_CODE_RESPONSE", Handle: 29, [BC] */
	,MK_STDID0(0x800)		/* id: 0x800, Name: "TxDynamicMsg0_0", Handle: 30, [BC] */
	,MK_STDID0(0x800)		/* id: 0x800, Name: "TxDynamicMsg1_0", Handle: 31, [BC] */
	,MK_EXTID0(0x1e7ca0ff)		/* id: 0x1E7CA0FF, Name: "ECU_APPL_BCM_1", Handle: 32, [BC] */
	,MK_EXTID0(0x1e3d4000)		/* id: 0x1E3D4000, Name: "DTO_BCM", Handle: 33, [BC] */
	,MK_EXTID0(0x1e114000)		/* id: 0x1E114000, Name: "CONFIGURATION_DATA_CODE_REQUEST_1", Handle: 34, [BC] */
	,MK_STDID0(0x772)		/* id: 0x772, Name: "IBS_2", Handle: 35, [BC] */
	,MK_STDID0(0x76d)		/* id: 0x76D, Name: "HUMIDITY_1000L", Handle: 36, [BC] */
	,MK_STDID0(0x762)		/* id: 0x762, Name: "COMPASS_A1", Handle: 37, [BC] */
	,MK_STDID0(0x760)		/* id: 0x760, Name: "CBC_I5", Handle: 38, [BC] */
	,MK_STDID0(0x75c)		/* id: 0x75C, Name: "BCM_KEYON_COUNTER_B_CAN", Handle: 39, [BC] */
	,MK_STDID0(0x75a)		/* id: 0x75A, Name: "AMB_TEMP_DISP", Handle: 40, [BC] */
	,MK_STDID0(0x6a8)		/* id: 0x6A8, Name: "WCPM_STATUS", Handle: 41, [BC] */
	,MK_STDID0(0x6a6)		/* id: 0x6A6, Name: "GE_B", Handle: 42, [BC] */
	,MK_STDID0(0x6a4)		/* id: 0x6A4, Name: "ENVIRONMENTAL_CONDITIONS", Handle: 43, [BC] */
	,MK_STDID0(0x6a2)		/* id: 0x6A2, Name: "CBC_VTA_1", Handle: 44, [BC] */
	,MK_EXTID0(0x18daf140)		/* id: 0x18DAF140, Name: "DIAGNOSTIC_RESPONSE_BCM_1", Handle: 45, [BC] */
	,MK_STDID0(0x5dc)		/* id: 0x5DC, Name: "GW_C_I2", Handle: 46, [BC] */
	,MK_STDID0(0x5cc)		/* id: 0x5CC, Name: "DAS_B", Handle: 47, [BC] */
	,MK_STDID0(0x5ca)		/* id: 0x5CA, Name: "CBC_I2", Handle: 48, [BC] */
	,MK_STDID0(0x5c8)		/* id: 0x5C8, Name: "CBC_I1", Handle: 49, [BC] */
	,MK_STDID0(0x550)		/* id: 0x550, Name: "STATUS_B_EPB", Handle: 50, [BC] */
	,MK_STDID0(0x548)		/* id: 0x548, Name: "STATUS_C_CAN", Handle: 51, [BC] */
	,MK_STDID0(0x52e)		/* id: 0x52E, Name: "STATUS_B_HALF", Handle: 52, [BC] */
	,MK_STDID0(0x52d)		/* id: 0x52D, Name: "HALF_B_Warning_RQ", Handle: 53, [BC] */
	,MK_STDID0(0x4a3)		/* id: 0x4A3, Name: "SWS_8", Handle: 54, [BC] */
	,MK_STDID0(0x4a1)		/* id: 0x4A1, Name: "StW_Actn_Rq_1", Handle: 55, [BC] */
	,MK_STDID0(0x499)		/* id: 0x499, Name: "PAM_B", Handle: 56, [BC] */
	,MK_STDID0(0x493)		/* id: 0x493, Name: "GW_C_I3", Handle: 57, [BC] */
	,MK_STDID0(0x3d3)		/* id: 0x3D3, Name: "VIN_1", Handle: 58, [BC] */
	,MK_STDID0(0x3d1)		/* id: 0x3D1, Name: "VEHICLE_SPEED_ODOMETER_1", Handle: 59, [BC] */
	,MK_STDID0(0x3cf)		/* id: 0x3CF, Name: "GW_C_I6", Handle: 60, [BC] */
	,MK_STDID0(0x3cb)		/* id: 0x3CB, Name: "DYNAMIC_VEHICLE_INFO2", Handle: 61, [BC] */
	,MK_STDID0(0x385)		/* id: 0x385, Name: "STATUS_B_TRANSMISSION", Handle: 62, [BC] */
	,MK_EXTID0(0xe094000)		/* id: 0x0E094000, Name: "NWM_BCM", Handle: 63, [BC] */
	,MK_STDID0(0x33b)		/* id: 0x33B, Name: "STATUS_BCM2", Handle: 64, [BC] */
	,MK_STDID0(0x339)		/* id: 0x339, Name: "STATUS_BCM", Handle: 65, [BC] */
	,MK_STDID0(0x337)		/* id: 0x337, Name: "EXTERNAL_LIGHTS_1", Handle: 66, [BC] */
	,MK_STDID0(0x309)		/* id: 0x309, Name: "RFHUB_B_A2", Handle: 67, [BC] */
	,MK_STDID0(0x190)		/* id: 0x190, Name: "CBC_I4", Handle: 68, [BC] */
	,MK_STDID0(0xa4)		/* id: 0x0A4, Name: "RFHUB_B_A4", Handle: 69, [BC] */
	,MK_STDID0(0x9c)		/* id: 0x09C, Name: "STATUS_B_ECM2", Handle: 70, [BC] */
	,MK_STDID0(0x9a)		/* id: 0x09A, Name: "STATUS_B_ECM", Handle: 71, [BC] */
	,MK_STDID0(0x98)		/* id: 0x098, Name: "STATUS_B_BSM", Handle: 72, [BC] */
	,MK_STDID0(0x800)		/* id: 0x800, Name: "TxDynamicMsg0_1", Handle: 73, [BC] */
	,MK_STDID0(0x800)		/* id: 0x800, Name: "TxDynamicMsg1_1", Handle: 74, [BC] */
};

V_MEMROM0 V_MEMROM1 tCanTxId1 V_MEMROM2 CanTxId1[kCanNumberOfTxObjects] = {
	 MK_STDID1(0x7d4)		/* id: 0x7D4, Name: "STATUS_C_BCM2", Handle: 0, [BC] */
	,MK_STDID1(0x7d0)		/* id: 0x7D0, Name: "HVAC_INFO", Handle: 1, [BC] */
	,MK_STDID1(0x7cc)		/* id: 0x7CC, Name: "CBC_PT4", Handle: 2, [BC] */
	,MK_STDID1(0x7ca)		/* id: 0x7CA, Name: "BCM_KEYON_COUNTER_C_CAN", Handle: 3, [BC] */
	,MK_STDID1(0x7c8)		/* id: 0x7C8, Name: "IBS4", Handle: 4, [BC] */
	,MK_EXTID1(0x1e7caff6)		/* id: 0x1E7CAFF6, Name: "ECU_APPL_BCM_0", Handle: 5, [BC] */
	,MK_EXTID1(0x1e114000)		/* id: 0x1E114000, Name: "CONFIGURATION_DATA_CODE_REQUEST_0", Handle: 6, [BC] */
	,MK_STDID1(0x6d2)		/* id: 0x6D2, Name: "CBC_VTA_0", Handle: 7, [BC] */
	,MK_EXTID1(0x18daf140)		/* id: 0x18DAF140, Name: "DIAGNOSTIC_RESPONSE_BCM_0", Handle: 8, [BC] */
	,MK_STDID1(0x5de)		/* id: 0x5DE, Name: "STATUS_B_CAN2", Handle: 9, [BC] */
	,MK_STDID1(0x5d8)		/* id: 0x5D8, Name: "EXTERNAL_LIGHTS_0", Handle: 10, [BC] */
	,MK_STDID1(0x5d6)		/* id: 0x5D6, Name: "ASBM1_CONTROL", Handle: 11, [BC] */
	,MK_STDID1(0x5d4)		/* id: 0x5D4, Name: "HVAC_INFO2", Handle: 12, [BC] */
	,MK_STDID1(0x55f)		/* id: 0x55F, Name: "IBS2", Handle: 13, [BC] */
	,MK_STDID1(0x4f4)		/* id: 0x4F4, Name: "VEHICLE_SPEED_ODOMETER_0", Handle: 14, [BC] */
	,MK_STDID1(0x4e2)		/* id: 0x4E2, Name: "STATUS_C_BCM", Handle: 15, [BC] */
	,MK_STDID1(0x4e0)		/* id: 0x4E0, Name: "STATUS_B_CAN", Handle: 16, [BC] */
	,MK_STDID1(0x4d6)		/* id: 0x4D6, Name: "IBS3", Handle: 17, [BC] */
	,MK_STDID1(0x4d4)		/* id: 0x4D4, Name: "IBS1", Handle: 18, [BC] */
	,MK_STDID1(0x4ce)		/* id: 0x4CE, Name: "BATTERY_INFO", Handle: 19, [BC] */
	,MK_STDID1(0x3e8)		/* id: 0x3E8, Name: "CFG_Feature", Handle: 20, [BC] */
	,MK_STDID1(0x3e6)		/* id: 0x3E6, Name: "CBC_PT3", Handle: 21, [BC] */
	,MK_STDID1(0x3e4)		/* id: 0x3E4, Name: "CBC_PT1", Handle: 22, [BC] */
	,MK_EXTID1(0xc1cd000)		/* id: 0x0C1CD000, Name: "WAKE_C_BCM", Handle: 23, [BC] */
	,MK_STDID1(0x2ea)		/* id: 0x2EA, Name: "CBC_PT2", Handle: 24, [BC] */
	,MK_STDID1(0x1e8)		/* id: 0x1E8, Name: "BCM_COMMAND", Handle: 25, [BC] */
	,MK_STDID1(0xf7)		/* id: 0x0F7, Name: "BCM_CODE_TRM_REQUEST", Handle: 26, [BC] */
	,MK_STDID1(0xf6)		/* id: 0x0F6, Name: "BCM_CODE_ESL_REQUEST", Handle: 27, [BC] */
	,MK_STDID1(0xf5)		/* id: 0x0F5, Name: "BCM_MINICRYPT_ACK", Handle: 28, [BC] */
	,MK_STDID1(0xf4)		/* id: 0x0F4, Name: "IMMO_CODE_RESPONSE", Handle: 29, [BC] */
	,MK_STDID1(0x800)		/* id: 0x800, Name: "TxDynamicMsg0_0", Handle: 30, [BC] */
	,MK_STDID1(0x800)		/* id: 0x800, Name: "TxDynamicMsg1_0", Handle: 31, [BC] */
	,MK_EXTID1(0x1e7ca0ff)		/* id: 0x1E7CA0FF, Name: "ECU_APPL_BCM_1", Handle: 32, [BC] */
	,MK_EXTID1(0x1e3d4000)		/* id: 0x1E3D4000, Name: "DTO_BCM", Handle: 33, [BC] */
	,MK_EXTID1(0x1e114000)		/* id: 0x1E114000, Name: "CONFIGURATION_DATA_CODE_REQUEST_1", Handle: 34, [BC] */
	,MK_STDID1(0x772)		/* id: 0x772, Name: "IBS_2", Handle: 35, [BC] */
	,MK_STDID1(0x76d)		/* id: 0x76D, Name: "HUMIDITY_1000L", Handle: 36, [BC] */
	,MK_STDID1(0x762)		/* id: 0x762, Name: "COMPASS_A1", Handle: 37, [BC] */
	,MK_STDID1(0x760)		/* id: 0x760, Name: "CBC_I5", Handle: 38, [BC] */
	,MK_STDID1(0x75c)		/* id: 0x75C, Name: "BCM_KEYON_COUNTER_B_CAN", Handle: 39, [BC] */
	,MK_STDID1(0x75a)		/* id: 0x75A, Name: "AMB_TEMP_DISP", Handle: 40, [BC] */
	,MK_STDID1(0x6a8)		/* id: 0x6A8, Name: "WCPM_STATUS", Handle: 41, [BC] */
	,MK_STDID1(0x6a6)		/* id: 0x6A6, Name: "GE_B", Handle: 42, [BC] */
	,MK_STDID1(0x6a4)		/* id: 0x6A4, Name: "ENVIRONMENTAL_CONDITIONS", Handle: 43, [BC] */
	,MK_STDID1(0x6a2)		/* id: 0x6A2, Name: "CBC_VTA_1", Handle: 44, [BC] */
	,MK_EXTID1(0x18daf140)		/* id: 0x18DAF140, Name: "DIAGNOSTIC_RESPONSE_BCM_1", Handle: 45, [BC] */
	,MK_STDID1(0x5dc)		/* id: 0x5DC, Name: "GW_C_I2", Handle: 46, [BC] */
	,MK_STDID1(0x5cc)		/* id: 0x5CC, Name: "DAS_B", Handle: 47, [BC] */
	,MK_STDID1(0x5ca)		/* id: 0x5CA, Name: "CBC_I2", Handle: 48, [BC] */
	,MK_STDID1(0x5c8)		/* id: 0x5C8, Name: "CBC_I1", Handle: 49, [BC] */
	,MK_STDID1(0x550)		/* id: 0x550, Name: "STATUS_B_EPB", Handle: 50, [BC] */
	,MK_STDID1(0x548)		/* id: 0x548, Name: "STATUS_C_CAN", Handle: 51, [BC] */
	,MK_STDID1(0x52e)		/* id: 0x52E, Name: "STATUS_B_HALF", Handle: 52, [BC] */
	,MK_STDID1(0x52d)		/* id: 0x52D, Name: "HALF_B_Warning_RQ", Handle: 53, [BC] */
	,MK_STDID1(0x4a3)		/* id: 0x4A3, Name: "SWS_8", Handle: 54, [BC] */
	,MK_STDID1(0x4a1)		/* id: 0x4A1, Name: "StW_Actn_Rq_1", Handle: 55, [BC] */
	,MK_STDID1(0x499)		/* id: 0x499, Name: "PAM_B", Handle: 56, [BC] */
	,MK_STDID1(0x493)		/* id: 0x493, Name: "GW_C_I3", Handle: 57, [BC] */
	,MK_STDID1(0x3d3)		/* id: 0x3D3, Name: "VIN_1", Handle: 58, [BC] */
	,MK_STDID1(0x3d1)		/* id: 0x3D1, Name: "VEHICLE_SPEED_ODOMETER_1", Handle: 59, [BC] */
	,MK_STDID1(0x3cf)		/* id: 0x3CF, Name: "GW_C_I6", Handle: 60, [BC] */
	,MK_STDID1(0x3cb)		/* id: 0x3CB, Name: "DYNAMIC_VEHICLE_INFO2", Handle: 61, [BC] */
	,MK_STDID1(0x385)		/* id: 0x385, Name: "STATUS_B_TRANSMISSION", Handle: 62, [BC] */
	,MK_EXTID1(0xe094000)		/* id: 0x0E094000, Name: "NWM_BCM", Handle: 63, [BC] */
	,MK_STDID1(0x33b)		/* id: 0x33B, Name: "STATUS_BCM2", Handle: 64, [BC] */
	,MK_STDID1(0x339)		/* id: 0x339, Name: "STATUS_BCM", Handle: 65, [BC] */
	,MK_STDID1(0x337)		/* id: 0x337, Name: "EXTERNAL_LIGHTS_1", Handle: 66, [BC] */
	,MK_STDID1(0x309)		/* id: 0x309, Name: "RFHUB_B_A2", Handle: 67, [BC] */
	,MK_STDID1(0x190)		/* id: 0x190, Name: "CBC_I4", Handle: 68, [BC] */
	,MK_STDID1(0xa4)		/* id: 0x0A4, Name: "RFHUB_B_A4", Handle: 69, [BC] */
	,MK_STDID1(0x9c)		/* id: 0x09C, Name: "STATUS_B_ECM2", Handle: 70, [BC] */
	,MK_STDID1(0x9a)		/* id: 0x09A, Name: "STATUS_B_ECM", Handle: 71, [BC] */
	,MK_STDID1(0x98)		/* id: 0x098, Name: "STATUS_B_BSM", Handle: 72, [BC] */
	,MK_STDID1(0x800)		/* id: 0x800, Name: "TxDynamicMsg0_1", Handle: 73, [BC] */
	,MK_STDID1(0x800)		/* id: 0x800, Name: "TxDynamicMsg1_1", Handle: 74, [BC] */
};

V_MEMROM0 V_MEMROM1 canuint8 V_MEMROM2 CanTxDLC[kCanNumberOfTxObjects] = {
	 MK_TX_DLC(8)		/* id: 0x7D4, Name: "STATUS_C_BCM2", Handle: 0, [BC] */
	,MK_TX_DLC(8)		/* id: 0x7D0, Name: "HVAC_INFO", Handle: 1, [BC] */
	,MK_TX_DLC(2)		/* id: 0x7CC, Name: "CBC_PT4", Handle: 2, [BC] */
	,MK_TX_DLC(2)		/* id: 0x7CA, Name: "BCM_KEYON_COUNTER_C_CAN", Handle: 3, [BC] */
	,MK_TX_DLC(3)		/* id: 0x7C8, Name: "IBS4", Handle: 4, [BC] */
	,MK_TX_DLC_EXT(8)		/* id: 0x1E7CAFF6, Name: "ECU_APPL_BCM_0", Handle: 5, [BC] */
	,MK_TX_DLC_EXT(6)		/* id: 0x1E114000, Name: "CONFIGURATION_DATA_CODE_REQUEST_0", Handle: 6, [BC] */
	,MK_TX_DLC(1)		/* id: 0x6D2, Name: "CBC_VTA_0", Handle: 7, [BC] */
	,MK_TX_DLC_EXT(8)		/* id: 0x18DAF140, Name: "DIAGNOSTIC_RESPONSE_BCM_0", Handle: 8, [BC] */
	,MK_TX_DLC(8)		/* id: 0x5DE, Name: "STATUS_B_CAN2", Handle: 9, [BC] */
	,MK_TX_DLC(6)		/* id: 0x5D8, Name: "EXTERNAL_LIGHTS_0", Handle: 10, [BC] */
	,MK_TX_DLC(4)		/* id: 0x5D6, Name: "ASBM1_CONTROL", Handle: 11, [BC] */
	,MK_TX_DLC(4)		/* id: 0x5D4, Name: "HVAC_INFO2", Handle: 12, [BC] */
	,MK_TX_DLC(8)		/* id: 0x55F, Name: "IBS2", Handle: 13, [BC] */
	,MK_TX_DLC(8)		/* id: 0x4F4, Name: "VEHICLE_SPEED_ODOMETER_0", Handle: 14, [BC] */
	,MK_TX_DLC(8)		/* id: 0x4E2, Name: "STATUS_C_BCM", Handle: 15, [BC] */
	,MK_TX_DLC(8)		/* id: 0x4E0, Name: "STATUS_B_CAN", Handle: 16, [BC] */
	,MK_TX_DLC(4)		/* id: 0x4D6, Name: "IBS3", Handle: 17, [BC] */
	,MK_TX_DLC(8)		/* id: 0x4D4, Name: "IBS1", Handle: 18, [BC] */
	,MK_TX_DLC(8)		/* id: 0x4CE, Name: "BATTERY_INFO", Handle: 19, [BC] */
	,MK_TX_DLC(3)		/* id: 0x3E8, Name: "CFG_Feature", Handle: 20, [BC] */
	,MK_TX_DLC(5)		/* id: 0x3E6, Name: "CBC_PT3", Handle: 21, [BC] */
	,MK_TX_DLC(6)		/* id: 0x3E4, Name: "CBC_PT1", Handle: 22, [BC] */
	,MK_TX_DLC_EXT(8)		/* id: 0x0C1CD000, Name: "WAKE_C_BCM", Handle: 23, [BC] */
	,MK_TX_DLC(5)		/* id: 0x2EA, Name: "CBC_PT2", Handle: 24, [BC] */
	,MK_TX_DLC(5)		/* id: 0x1E8, Name: "BCM_COMMAND", Handle: 25, [BC] */
	,MK_TX_DLC(8)		/* id: 0x0F7, Name: "BCM_CODE_TRM_REQUEST", Handle: 26, [BC] */
	,MK_TX_DLC(8)		/* id: 0x0F6, Name: "BCM_CODE_ESL_REQUEST", Handle: 27, [BC] */
	,MK_TX_DLC(1)		/* id: 0x0F5, Name: "BCM_MINICRYPT_ACK", Handle: 28, [BC] */
	,MK_TX_DLC(7)		/* id: 0x0F4, Name: "IMMO_CODE_RESPONSE", Handle: 29, [BC] */
	,MK_TX_DLC_EXT(8)		/* id: 0x800, Name: "TxDynamicMsg0_0", Handle: 30, [BC] */
	,MK_TX_DLC_EXT(8)		/* id: 0x800, Name: "TxDynamicMsg1_0", Handle: 31, [BC] */
	,MK_TX_DLC_EXT(8)		/* id: 0x1E7CA0FF, Name: "ECU_APPL_BCM_1", Handle: 32, [BC] */
	,MK_TX_DLC_EXT(8)		/* id: 0x1E3D4000, Name: "DTO_BCM", Handle: 33, [BC] */
	,MK_TX_DLC_EXT(6)		/* id: 0x1E114000, Name: "CONFIGURATION_DATA_CODE_REQUEST_1", Handle: 34, [BC] */
	,MK_TX_DLC(4)		/* id: 0x772, Name: "IBS_2", Handle: 35, [BC] */
	,MK_TX_DLC(8)		/* id: 0x76D, Name: "HUMIDITY_1000L", Handle: 36, [BC] */
	,MK_TX_DLC(3)		/* id: 0x762, Name: "COMPASS_A1", Handle: 37, [BC] */
	,MK_TX_DLC(4)		/* id: 0x760, Name: "CBC_I5", Handle: 38, [BC] */
	,MK_TX_DLC(2)		/* id: 0x75C, Name: "BCM_KEYON_COUNTER_B_CAN", Handle: 39, [BC] */
	,MK_TX_DLC(2)		/* id: 0x75A, Name: "AMB_TEMP_DISP", Handle: 40, [BC] */
	,MK_TX_DLC(2)		/* id: 0x6A8, Name: "WCPM_STATUS", Handle: 41, [BC] */
	,MK_TX_DLC(3)		/* id: 0x6A6, Name: "GE_B", Handle: 42, [BC] */
	,MK_TX_DLC(8)		/* id: 0x6A4, Name: "ENVIRONMENTAL_CONDITIONS", Handle: 43, [BC] */
	,MK_TX_DLC(1)		/* id: 0x6A2, Name: "CBC_VTA_1", Handle: 44, [BC] */
	,MK_TX_DLC_EXT(8)		/* id: 0x18DAF140, Name: "DIAGNOSTIC_RESPONSE_BCM_1", Handle: 45, [BC] */
	,MK_TX_DLC(5)		/* id: 0x5DC, Name: "GW_C_I2", Handle: 46, [BC] */
	,MK_TX_DLC(4)		/* id: 0x5CC, Name: "DAS_B", Handle: 47, [BC] */
	,MK_TX_DLC(8)		/* id: 0x5CA, Name: "CBC_I2", Handle: 48, [BC] */
	,MK_TX_DLC(3)		/* id: 0x5C8, Name: "CBC_I1", Handle: 49, [BC] */
	,MK_TX_DLC(7)		/* id: 0x550, Name: "STATUS_B_EPB", Handle: 50, [BC] */
	,MK_TX_DLC(4)		/* id: 0x548, Name: "STATUS_C_CAN", Handle: 51, [BC] */
	,MK_TX_DLC(8)		/* id: 0x52E, Name: "STATUS_B_HALF", Handle: 52, [BC] */
	,MK_TX_DLC(2)		/* id: 0x52D, Name: "HALF_B_Warning_RQ", Handle: 53, [BC] */
	,MK_TX_DLC(8)		/* id: 0x4A3, Name: "SWS_8", Handle: 54, [BC] */
	,MK_TX_DLC(7)		/* id: 0x4A1, Name: "StW_Actn_Rq_1", Handle: 55, [BC] */
	,MK_TX_DLC(5)		/* id: 0x499, Name: "PAM_B", Handle: 56, [BC] */
	,MK_TX_DLC(2)		/* id: 0x493, Name: "GW_C_I3", Handle: 57, [BC] */
	,MK_TX_DLC(8)		/* id: 0x3D3, Name: "VIN_1", Handle: 58, [BC] */
	,MK_TX_DLC(4)		/* id: 0x3D1, Name: "VEHICLE_SPEED_ODOMETER_1", Handle: 59, [BC] */
	,MK_TX_DLC(4)		/* id: 0x3CF, Name: "GW_C_I6", Handle: 60, [BC] */
	,MK_TX_DLC(8)		/* id: 0x3CB, Name: "DYNAMIC_VEHICLE_INFO2", Handle: 61, [BC] */
	,MK_TX_DLC(8)		/* id: 0x385, Name: "STATUS_B_TRANSMISSION", Handle: 62, [BC] */
	,MK_TX_DLC_EXT(6)		/* id: 0x0E094000, Name: "NWM_BCM", Handle: 63, [BC] */
	,MK_TX_DLC(8)		/* id: 0x33B, Name: "STATUS_BCM2", Handle: 64, [BC] */
	,MK_TX_DLC(8)		/* id: 0x339, Name: "STATUS_BCM", Handle: 65, [BC] */
	,MK_TX_DLC(6)		/* id: 0x337, Name: "EXTERNAL_LIGHTS_1", Handle: 66, [BC] */
	,MK_TX_DLC(5)		/* id: 0x309, Name: "RFHUB_B_A2", Handle: 67, [BC] */
	,MK_TX_DLC(5)		/* id: 0x190, Name: "CBC_I4", Handle: 68, [BC] */
	,MK_TX_DLC(1)		/* id: 0x0A4, Name: "RFHUB_B_A4", Handle: 69, [BC] */
	,MK_TX_DLC(8)		/* id: 0x09C, Name: "STATUS_B_ECM2", Handle: 70, [BC] */
	,MK_TX_DLC(8)		/* id: 0x09A, Name: "STATUS_B_ECM", Handle: 71, [BC] */
	,MK_TX_DLC(8)		/* id: 0x098, Name: "STATUS_B_BSM", Handle: 72, [BC] */
	,MK_TX_DLC_EXT(8)		/* id: 0x800, Name: "TxDynamicMsg0_1", Handle: 73, [BC] */
	,MK_TX_DLC_EXT(8)		/* id: 0x800, Name: "TxDynamicMsg1_1", Handle: 74, [BC] */
};

V_MEMROM0 V_MEMROM1 TxDataPtr V_MEMROM2 CanTxDataPtr[kCanNumberOfTxObjects] = {
	 (TxDataPtr)STATUS_C_BCM2._c		/* id: 0x7D4, Name: "STATUS_C_BCM2", Handle: 0, [BC] */
	,(TxDataPtr)HVAC_INFO._c		/* id: 0x7D0, Name: "HVAC_INFO", Handle: 1, [BC] */
	,(TxDataPtr)CBC_PT4._c		/* id: 0x7CC, Name: "CBC_PT4", Handle: 2, [BC] */
	,(TxDataPtr)BCM_KEYON_COUNTER_C_CAN._c		/* id: 0x7CA, Name: "BCM_KEYON_COUNTER_C_CAN", Handle: 3, [BC] */
	,(TxDataPtr)IBS4._c		/* id: 0x7C8, Name: "IBS4", Handle: 4, [BC] */
	,(TxDataPtr)ECU_APPL_BCM_0._c		/* id: 0x1E7CAFF6, Name: "ECU_APPL_BCM_0", Handle: 5, [BC] */
	,(TxDataPtr)CONFIGURATION_DATA_CODE_REQUEST_0._c		/* id: 0x1E114000, Name: "CONFIGURATION_DATA_CODE_REQUEST_0", Handle: 6, [BC] */
	,(TxDataPtr)CBC_VTA_0._c		/* id: 0x6D2, Name: "CBC_VTA_0", Handle: 7, [BC] */
	,(TxDataPtr)DIAGNOSTIC_RESPONSE_BCM_0._c		/* id: 0x18DAF140, Name: "DIAGNOSTIC_RESPONSE_BCM_0", Handle: 8, [BC] */
	,(TxDataPtr)STATUS_B_CAN2._c		/* id: 0x5DE, Name: "STATUS_B_CAN2", Handle: 9, [BC] */
	,(TxDataPtr)EXTERNAL_LIGHTS_0._c		/* id: 0x5D8, Name: "EXTERNAL_LIGHTS_0", Handle: 10, [BC] */
	,(TxDataPtr)ASBM1_CONTROL._c		/* id: 0x5D6, Name: "ASBM1_CONTROL", Handle: 11, [BC] */
	,(TxDataPtr)HVAC_A4._c		/* id: 0x5D4, Name: "HVAC_INFO2", Handle: 12, [BC] */
	,(TxDataPtr)IBS2._c		/* id: 0x55F, Name: "IBS2", Handle: 13, [BC] */
	,(TxDataPtr)VEHICLE_SPEED_ODOMETER_0._c		/* id: 0x4F4, Name: "VEHICLE_SPEED_ODOMETER_0", Handle: 14, [BC] */
	,(TxDataPtr)STATUS_C_BCM._c		/* id: 0x4E2, Name: "STATUS_C_BCM", Handle: 15, [BC] */
	,(TxDataPtr)STATUS_B_CAN._c		/* id: 0x4E0, Name: "STATUS_B_CAN", Handle: 16, [BC] */
	,(TxDataPtr)IBS3._c		/* id: 0x4D6, Name: "IBS3", Handle: 17, [BC] */
	,(TxDataPtr)IBS1._c		/* id: 0x4D4, Name: "IBS1", Handle: 18, [BC] */
	,(TxDataPtr)BATTERY_INFO._c		/* id: 0x4CE, Name: "BATTERY_INFO", Handle: 19, [BC] */
	,(TxDataPtr)CFG_Feature._c		/* id: 0x3E8, Name: "CFG_Feature", Handle: 20, [BC] */
	,(TxDataPtr)CBC_PT3._c		/* id: 0x3E6, Name: "CBC_PT3", Handle: 21, [BC] */
	,(TxDataPtr)CBC_PT1._c		/* id: 0x3E4, Name: "CBC_PT1", Handle: 22, [BC] */
	,(TxDataPtr)WAKE_C_BCM._c		/* id: 0x0C1CD000, Name: "WAKE_C_BCM", Handle: 23, [BC] */
	,(TxDataPtr)CBC_PT2._c		/* id: 0x2EA, Name: "CBC_PT2", Handle: 24, [BC] */
	,(TxDataPtr)BCM_COMMAND._c		/* id: 0x1E8, Name: "BCM_COMMAND", Handle: 25, [BC] */
	,(TxDataPtr)BCM_CODE_TRM_REQUEST._c		/* id: 0x0F7, Name: "BCM_CODE_TRM_REQUEST", Handle: 26, [BC] */
	,(TxDataPtr)BCM_CODE_ESL_REQUEST._c		/* id: 0x0F6, Name: "BCM_CODE_ESL_REQUEST", Handle: 27, [BC] */
	,(TxDataPtr)BCM_MINICRYPT_ACK._c		/* id: 0x0F5, Name: "BCM_MINICRYPT_ACK", Handle: 28, [BC] */
	,(TxDataPtr)IMMO_CODE_RESPONSE._c		/* id: 0x0F4, Name: "IMMO_CODE_RESPONSE", Handle: 29, [BC] */
	,(TxDataPtr)TxDynamicMsg0_0._c		/* id: 0x800, Name: "TxDynamicMsg0_0", Handle: 30, [BC] */
	,(TxDataPtr)TxDynamicMsg1_0._c		/* id: 0x800, Name: "TxDynamicMsg1_0", Handle: 31, [BC] */
	,(TxDataPtr)ECU_APPL_BCM_1._c		/* id: 0x1E7CA0FF, Name: "ECU_APPL_BCM_1", Handle: 32, [BC] */
	,(TxDataPtr)DTO_BCM._c		/* id: 0x1E3D4000, Name: "DTO_BCM", Handle: 33, [BC] */
	,(TxDataPtr)CONFIGURATION_DATA_CODE_REQUEST_1._c		/* id: 0x1E114000, Name: "CONFIGURATION_DATA_CODE_REQUEST_1", Handle: 34, [BC] */
	,(TxDataPtr)IBS_2._c		/* id: 0x772, Name: "IBS_2", Handle: 35, [BC] */
	,(TxDataPtr)HUMIDITY_1000L._c		/* id: 0x76D, Name: "HUMIDITY_1000L", Handle: 36, [BC] */
	,(TxDataPtr)COMPASS_A1._c		/* id: 0x762, Name: "COMPASS_A1", Handle: 37, [BC] */
	,(TxDataPtr)CBC_I5._c		/* id: 0x760, Name: "CBC_I5", Handle: 38, [BC] */
	,(TxDataPtr)BCM_KEYON_COUNTER_C_CAN._c		/* id: 0x75C, Name: "BCM_KEYON_COUNTER_B_CAN", Handle: 39, [BC] */
	,(TxDataPtr)AMB_TEMP_DISP._c		/* id: 0x75A, Name: "AMB_TEMP_DISP", Handle: 40, [BC] */
	,(TxDataPtr)WCPM_STATUS._c		/* id: 0x6A8, Name: "WCPM_STATUS", Handle: 41, [BC] */
	,(TxDataPtr)GE_B._c		/* id: 0x6A6, Name: "GE_B", Handle: 42, [BC] */
	,(TxDataPtr)ENVIRONMENTAL_CONDITIONS._c		/* id: 0x6A4, Name: "ENVIRONMENTAL_CONDITIONS", Handle: 43, [BC] */
	,(TxDataPtr)CBC_VTA_1._c		/* id: 0x6A2, Name: "CBC_VTA_1", Handle: 44, [BC] */
	,(TxDataPtr)DIAGNOSTIC_RESPONSE_BCM_1._c		/* id: 0x18DAF140, Name: "DIAGNOSTIC_RESPONSE_BCM_1", Handle: 45, [BC] */
	,(TxDataPtr)GW_C_I2._c		/* id: 0x5DC, Name: "GW_C_I2", Handle: 46, [BC] */
	,(TxDataPtr)DAS_B._c		/* id: 0x5CC, Name: "DAS_B", Handle: 47, [BC] */
	,(TxDataPtr)CBC_I2._c		/* id: 0x5CA, Name: "CBC_I2", Handle: 48, [BC] */
	,(TxDataPtr)CBC_I1._c		/* id: 0x5C8, Name: "CBC_I1", Handle: 49, [BC] */
	,(TxDataPtr)STATUS_C_EPB._c		/* id: 0x550, Name: "STATUS_B_EPB", Handle: 50, [BC] */
	,(TxDataPtr)STATUS_C_CAN._c		/* id: 0x548, Name: "STATUS_C_CAN", Handle: 51, [BC] */
	,(TxDataPtr)STATUS_C_HALF._c		/* id: 0x52E, Name: "STATUS_B_HALF", Handle: 52, [BC] */
	,(TxDataPtr)HALF_C_Warning_RQ._c		/* id: 0x52D, Name: "HALF_B_Warning_RQ", Handle: 53, [BC] */
	,(TxDataPtr)SWS_8._c		/* id: 0x4A3, Name: "SWS_8", Handle: 54, [BC] */
	,(TxDataPtr)StW_Actn_Rq_1._c		/* id: 0x4A1, Name: "StW_Actn_Rq_1", Handle: 55, [BC] */
	,(TxDataPtr)PAM_B._c		/* id: 0x499, Name: "PAM_B", Handle: 56, [BC] */
	,(TxDataPtr)GW_C_I3._c		/* id: 0x493, Name: "GW_C_I3", Handle: 57, [BC] */
	,(TxDataPtr)VIN_0._c		/* id: 0x3D3, Name: "VIN_1", Handle: 58, [BC] */
	,(TxDataPtr)VEHICLE_SPEED_ODOMETER_1._c		/* id: 0x3D1, Name: "VEHICLE_SPEED_ODOMETER_1", Handle: 59, [BC] */
	,(TxDataPtr)GW_C_I6._c		/* id: 0x3CF, Name: "GW_C_I6", Handle: 60, [BC] */
	,(TxDataPtr)DYNAMIC_VEHICLE_INFO2._c		/* id: 0x3CB, Name: "DYNAMIC_VEHICLE_INFO2", Handle: 61, [BC] */
	,(TxDataPtr)STATUS_C_TRANSMISSION._c		/* id: 0x385, Name: "STATUS_B_TRANSMISSION", Handle: 62, [BC] */
	,(TxDataPtr)NWM_BCM._c		/* id: 0x0E094000, Name: "NWM_BCM", Handle: 63, [BC] */
	,(TxDataPtr)STATUS_BCM2._c		/* id: 0x33B, Name: "STATUS_BCM2", Handle: 64, [BC] */
	,(TxDataPtr)STATUS_BCM._c		/* id: 0x339, Name: "STATUS_BCM", Handle: 65, [BC] */
	,(TxDataPtr)EXTERNAL_LIGHTS_1._c		/* id: 0x337, Name: "EXTERNAL_LIGHTS_1", Handle: 66, [BC] */
	,(TxDataPtr)RFHUB_A2._c		/* id: 0x309, Name: "RFHUB_B_A2", Handle: 67, [BC] */
	,(TxDataPtr)CBC_I4._c		/* id: 0x190, Name: "CBC_I4", Handle: 68, [BC] */
	,(TxDataPtr)RFHUB_A4._c		/* id: 0x0A4, Name: "RFHUB_B_A4", Handle: 69, [BC] */
	,(TxDataPtr)STATUS_C_ECM2._c		/* id: 0x09C, Name: "STATUS_B_ECM2", Handle: 70, [BC] */
	,(TxDataPtr)STATUS_C_ECM._c		/* id: 0x09A, Name: "STATUS_B_ECM", Handle: 71, [BC] */
	,(TxDataPtr)STATUS_C_BSM._c		/* id: 0x098, Name: "STATUS_B_BSM", Handle: 72, [BC] */
	,(TxDataPtr)TxDynamicMsg0_1._c		/* id: 0x800, Name: "TxDynamicMsg0_1", Handle: 73, [BC] */
	,(TxDataPtr)TxDynamicMsg1_1._c		/* id: 0x800, Name: "TxDynamicMsg1_1", Handle: 74, [BC] */
};

#ifdef C_ENABLE_CONFIRMATION_FCT

V_MEMROM0 V_MEMROM1 ApplConfirmationFct V_MEMROM2 CanTxApplConfirmationPtr[kCanNumberOfTxObjects] = {
	 NULL					/* id: 0x7D4, Name: "STATUS_C_BCM2", Handle: 0, [BC] */
	,NULL					/* id: 0x7D0, Name: "HVAC_INFO", Handle: 1, [BC] */
	,NULL					/* id: 0x7CC, Name: "CBC_PT4", Handle: 2, [BC] */
	,NULL					/* id: 0x7CA, Name: "BCM_KEYON_COUNTER_C_CAN", Handle: 3, [BC] */
	,NULL					/* id: 0x7C8, Name: "IBS4", Handle: 4, [BC] */
	,NULL					/* id: 0x1E7CAFF6, Name: "ECU_APPL_BCM_0", Handle: 5, [BC] */
	,NULL					/* id: 0x1E114000, Name: "CONFIGURATION_DATA_CODE_REQUEST_0", Handle: 6, [BC] */
	,NULL					/* id: 0x6D2, Name: "CBC_VTA_0", Handle: 7, [BC] */
	,NULL					/* id: 0x18DAF140, Name: "DIAGNOSTIC_RESPONSE_BCM_0", Handle: 8, [BC] */
	,NULL					/* id: 0x5DE, Name: "STATUS_B_CAN2", Handle: 9, [BC] */
	,NULL					/* id: 0x5D8, Name: "EXTERNAL_LIGHTS_0", Handle: 10, [BC] */
	,NULL					/* id: 0x5D6, Name: "ASBM1_CONTROL", Handle: 11, [BC] */
	,NULL					/* id: 0x5D4, Name: "HVAC_INFO2", Handle: 12, [BC] */
	,NULL					/* id: 0x55F, Name: "IBS2", Handle: 13, [BC] */
	,NULL					/* id: 0x4F4, Name: "VEHICLE_SPEED_ODOMETER_0", Handle: 14, [BC] */
	,NULL					/* id: 0x4E2, Name: "STATUS_C_BCM", Handle: 15, [BC] */
	,NULL					/* id: 0x4E0, Name: "STATUS_B_CAN", Handle: 16, [BC] */
	,NULL					/* id: 0x4D6, Name: "IBS3", Handle: 17, [BC] */
	,NULL					/* id: 0x4D4, Name: "IBS1", Handle: 18, [BC] */
	,NULL					/* id: 0x4CE, Name: "BATTERY_INFO", Handle: 19, [BC] */
	,NULL					/* id: 0x3E8, Name: "CFG_Feature", Handle: 20, [BC] */
	,NULL					/* id: 0x3E6, Name: "CBC_PT3", Handle: 21, [BC] */
	,NULL					/* id: 0x3E4, Name: "CBC_PT1", Handle: 22, [BC] */
	,NULL					/* id: 0x0C1CD000, Name: "WAKE_C_BCM", Handle: 23, [BC] */
	,VF606_CBC_PT2_Confirmation		/* id: 0x2EA, Name: "CBC_PT2", Handle: 24, [BC] */
	,VF606_BCM_COMMAND_Confirmation		/* id: 0x1E8, Name: "BCM_COMMAND", Handle: 25, [BC] */
	,NULL					/* id: 0x0F7, Name: "BCM_CODE_TRM_REQUEST", Handle: 26, [BC] */
	,NULL					/* id: 0x0F6, Name: "BCM_CODE_ESL_REQUEST", Handle: 27, [BC] */
	,NULL					/* id: 0x0F5, Name: "BCM_MINICRYPT_ACK", Handle: 28, [BC] */
	,NULL					/* id: 0x0F4, Name: "IMMO_CODE_RESPONSE", Handle: 29, [BC] */
	,TpDrvTxConfirmation		/* id: 0x800, Name: "TxDynamicMsg0_0", Handle: 30, [BC] */
	,TpDrvRxConfirmation		/* id: 0x800, Name: "TxDynamicMsg1_0", Handle: 31, [BC] */
	,NULL					/* id: 0x1E7CA0FF, Name: "ECU_APPL_BCM_1", Handle: 32, [BC] */
	,CCP_DTO_Confirmation		/* id: 0x1E3D4000, Name: "DTO_BCM", Handle: 33, [BC] */
	,NULL					/* id: 0x1E114000, Name: "CONFIGURATION_DATA_CODE_REQUEST_1", Handle: 34, [BC] */
	,NULL					/* id: 0x772, Name: "IBS_2", Handle: 35, [BC] */
	,NULL					/* id: 0x76D, Name: "HUMIDITY_1000L", Handle: 36, [BC] */
	,NULL					/* id: 0x762, Name: "COMPASS_A1", Handle: 37, [BC] */
	,NULL					/* id: 0x760, Name: "CBC_I5", Handle: 38, [BC] */
	,NULL					/* id: 0x75C, Name: "BCM_KEYON_COUNTER_B_CAN", Handle: 39, [BC] */
	,NULL					/* id: 0x75A, Name: "AMB_TEMP_DISP", Handle: 40, [BC] */
	,NULL					/* id: 0x6A8, Name: "WCPM_STATUS", Handle: 41, [BC] */
	,NULL					/* id: 0x6A6, Name: "GE_B", Handle: 42, [BC] */
	,NULL					/* id: 0x6A4, Name: "ENVIRONMENTAL_CONDITIONS", Handle: 43, [BC] */
	,NULL					/* id: 0x6A2, Name: "CBC_VTA_1", Handle: 44, [BC] */
	,NULL					/* id: 0x18DAF140, Name: "DIAGNOSTIC_RESPONSE_BCM_1", Handle: 45, [BC] */
	,NULL					/* id: 0x5DC, Name: "GW_C_I2", Handle: 46, [BC] */
	,NULL					/* id: 0x5CC, Name: "DAS_B", Handle: 47, [BC] */
	,NULL					/* id: 0x5CA, Name: "CBC_I2", Handle: 48, [BC] */
	,NULL					/* id: 0x5C8, Name: "CBC_I1", Handle: 49, [BC] */
	,NULL					/* id: 0x550, Name: "STATUS_B_EPB", Handle: 50, [BC] */
	,NULL					/* id: 0x548, Name: "STATUS_C_CAN", Handle: 51, [BC] */
	,NULL					/* id: 0x52E, Name: "STATUS_B_HALF", Handle: 52, [BC] */
	,NULL					/* id: 0x52D, Name: "HALF_B_Warning_RQ", Handle: 53, [BC] */
	,NULL					/* id: 0x4A3, Name: "SWS_8", Handle: 54, [BC] */
	,NULL					/* id: 0x4A1, Name: "StW_Actn_Rq_1", Handle: 55, [BC] */
	,NULL					/* id: 0x499, Name: "PAM_B", Handle: 56, [BC] */
	,NULL					/* id: 0x493, Name: "GW_C_I3", Handle: 57, [BC] */
	,NULL					/* id: 0x3D3, Name: "VIN_1", Handle: 58, [BC] */
	,NULL					/* id: 0x3D1, Name: "VEHICLE_SPEED_ODOMETER_1", Handle: 59, [BC] */
	,NULL					/* id: 0x3CF, Name: "GW_C_I6", Handle: 60, [BC] */
	,NULL					/* id: 0x3CB, Name: "DYNAMIC_VEHICLE_INFO2", Handle: 61, [BC] */
	,NULL					/* id: 0x385, Name: "STATUS_B_TRANSMISSION", Handle: 62, [BC] */
	,NULL					/* id: 0x0E094000, Name: "NWM_BCM", Handle: 63, [BC] */
	,NULL					/* id: 0x33B, Name: "STATUS_BCM2", Handle: 64, [BC] */
	,NULL					/* id: 0x339, Name: "STATUS_BCM", Handle: 65, [BC] */
	,NULL					/* id: 0x337, Name: "EXTERNAL_LIGHTS_1", Handle: 66, [BC] */
	,NULL					/* id: 0x309, Name: "RFHUB_B_A2", Handle: 67, [BC] */
	,NULL					/* id: 0x190, Name: "CBC_I4", Handle: 68, [BC] */
	,NULL					/* id: 0x0A4, Name: "RFHUB_B_A4", Handle: 69, [BC] */
	,NULL					/* id: 0x09C, Name: "STATUS_B_ECM2", Handle: 70, [BC] */
	,NULL					/* id: 0x09A, Name: "STATUS_B_ECM", Handle: 71, [BC] */
	,NULL					/* id: 0x098, Name: "STATUS_B_BSM", Handle: 72, [BC] */
	,TpDrvTxConfirmation		/* id: 0x800, Name: "TxDynamicMsg0_1", Handle: 73, [BC] */
	,TpDrvRxConfirmation		/* id: 0x800, Name: "TxDynamicMsg1_1", Handle: 74, [BC] */
};
#endif


#ifdef C_ENABLE_PRETRANSMIT_FCT

V_MEMROM0 V_MEMROM1 ApplPreTransmitFct V_MEMROM2 CanTxApplPreTransmitPtr[kCanNumberOfTxObjects] = {
	 NULL					/* id: 0x7D4, Name: "STATUS_C_BCM2", Handle: 0, [BC] */
	,NULL					/* id: 0x7D0, Name: "HVAC_INFO", Handle: 1, [BC] */
	,NULL					/* id: 0x7CC, Name: "CBC_PT4", Handle: 2, [BC] */
	,NULL					/* id: 0x7CA, Name: "BCM_KEYON_COUNTER_C_CAN", Handle: 3, [BC] */
	,NULL					/* id: 0x7C8, Name: "IBS4", Handle: 4, [BC] */
	,NULL					/* id: 0x1E7CAFF6, Name: "ECU_APPL_BCM_0", Handle: 5, [BC] */
	,NULL					/* id: 0x1E114000, Name: "CONFIGURATION_DATA_CODE_REQUEST_0", Handle: 6, [BC] */
	,NULL					/* id: 0x6D2, Name: "CBC_VTA_0", Handle: 7, [BC] */
	,NULL					/* id: 0x18DAF140, Name: "DIAGNOSTIC_RESPONSE_BCM_0", Handle: 8, [BC] */
	,NULL					/* id: 0x5DE, Name: "STATUS_B_CAN2", Handle: 9, [BC] */
	,NULL					/* id: 0x5D8, Name: "EXTERNAL_LIGHTS_0", Handle: 10, [BC] */
	,NULL					/* id: 0x5D6, Name: "ASBM1_CONTROL", Handle: 11, [BC] */
	,NULL					/* id: 0x5D4, Name: "HVAC_INFO2", Handle: 12, [BC] */
	,NULL					/* id: 0x55F, Name: "IBS2", Handle: 13, [BC] */
	,NULL					/* id: 0x4F4, Name: "VEHICLE_SPEED_ODOMETER_0", Handle: 14, [BC] */
	,NULL					/* id: 0x4E2, Name: "STATUS_C_BCM", Handle: 15, [BC] */
	,NULL					/* id: 0x4E0, Name: "STATUS_B_CAN", Handle: 16, [BC] */
	,NULL					/* id: 0x4D6, Name: "IBS3", Handle: 17, [BC] */
	,NULL					/* id: 0x4D4, Name: "IBS1", Handle: 18, [BC] */
	,NULL					/* id: 0x4CE, Name: "BATTERY_INFO", Handle: 19, [BC] */
	,NULL					/* id: 0x3E8, Name: "CFG_Feature", Handle: 20, [BC] */
	,NULL					/* id: 0x3E6, Name: "CBC_PT3", Handle: 21, [BC] */
	,NULL					/* id: 0x3E4, Name: "CBC_PT1", Handle: 22, [BC] */
	,NULL					/* id: 0x0C1CD000, Name: "WAKE_C_BCM", Handle: 23, [BC] */
	,VF606_CBC_PT2_PreTransmit		/* id: 0x2EA, Name: "CBC_PT2", Handle: 24, [BC] */
	,VF606_BCM_COMMAND_PreTransmit		/* id: 0x1E8, Name: "BCM_COMMAND", Handle: 25, [BC] */
	,NULL					/* id: 0x0F7, Name: "BCM_CODE_TRM_REQUEST", Handle: 26, [BC] */
	,NULL					/* id: 0x0F6, Name: "BCM_CODE_ESL_REQUEST", Handle: 27, [BC] */
	,NULL					/* id: 0x0F5, Name: "BCM_MINICRYPT_ACK", Handle: 28, [BC] */
	,NULL					/* id: 0x0F4, Name: "IMMO_CODE_RESPONSE", Handle: 29, [BC] */
	,NULL					/* id: 0x800, Name: "TxDynamicMsg0_0", Handle: 30, [BC] */
	,NULL					/* id: 0x800, Name: "TxDynamicMsg1_0", Handle: 31, [BC] */
	,NULL					/* id: 0x1E7CA0FF, Name: "ECU_APPL_BCM_1", Handle: 32, [BC] */
	,NULL					/* id: 0x1E3D4000, Name: "DTO_BCM", Handle: 33, [BC] */
	,NULL					/* id: 0x1E114000, Name: "CONFIGURATION_DATA_CODE_REQUEST_1", Handle: 34, [BC] */
	,NULL					/* id: 0x772, Name: "IBS_2", Handle: 35, [BC] */
	,NULL					/* id: 0x76D, Name: "HUMIDITY_1000L", Handle: 36, [BC] */
	,NULL					/* id: 0x762, Name: "COMPASS_A1", Handle: 37, [BC] */
	,NULL					/* id: 0x760, Name: "CBC_I5", Handle: 38, [BC] */
	,NULL					/* id: 0x75C, Name: "BCM_KEYON_COUNTER_B_CAN", Handle: 39, [BC] */
	,NULL					/* id: 0x75A, Name: "AMB_TEMP_DISP", Handle: 40, [BC] */
	,NULL					/* id: 0x6A8, Name: "WCPM_STATUS", Handle: 41, [BC] */
	,NULL					/* id: 0x6A6, Name: "GE_B", Handle: 42, [BC] */
	,NULL					/* id: 0x6A4, Name: "ENVIRONMENTAL_CONDITIONS", Handle: 43, [BC] */
	,NULL					/* id: 0x6A2, Name: "CBC_VTA_1", Handle: 44, [BC] */
	,NULL					/* id: 0x18DAF140, Name: "DIAGNOSTIC_RESPONSE_BCM_1", Handle: 45, [BC] */
	,NULL					/* id: 0x5DC, Name: "GW_C_I2", Handle: 46, [BC] */
	,NULL					/* id: 0x5CC, Name: "DAS_B", Handle: 47, [BC] */
	,NULL					/* id: 0x5CA, Name: "CBC_I2", Handle: 48, [BC] */
	,NULL					/* id: 0x5C8, Name: "CBC_I1", Handle: 49, [BC] */
	,NULL					/* id: 0x550, Name: "STATUS_B_EPB", Handle: 50, [BC] */
	,NULL					/* id: 0x548, Name: "STATUS_C_CAN", Handle: 51, [BC] */
	,NULL					/* id: 0x52E, Name: "STATUS_B_HALF", Handle: 52, [BC] */
	,NULL					/* id: 0x52D, Name: "HALF_B_Warning_RQ", Handle: 53, [BC] */
	,NULL					/* id: 0x4A3, Name: "SWS_8", Handle: 54, [BC] */
	,NULL					/* id: 0x4A1, Name: "StW_Actn_Rq_1", Handle: 55, [BC] */
	,NULL					/* id: 0x499, Name: "PAM_B", Handle: 56, [BC] */
	,NULL					/* id: 0x493, Name: "GW_C_I3", Handle: 57, [BC] */
	,NULL					/* id: 0x3D3, Name: "VIN_1", Handle: 58, [BC] */
	,NULL					/* id: 0x3D1, Name: "VEHICLE_SPEED_ODOMETER_1", Handle: 59, [BC] */
	,NULL					/* id: 0x3CF, Name: "GW_C_I6", Handle: 60, [BC] */
	,NULL					/* id: 0x3CB, Name: "DYNAMIC_VEHICLE_INFO2", Handle: 61, [BC] */
	,NULL					/* id: 0x385, Name: "STATUS_B_TRANSMISSION", Handle: 62, [BC] */
	,NULL					/* id: 0x0E094000, Name: "NWM_BCM", Handle: 63, [BC] */
	,NULL					/* id: 0x33B, Name: "STATUS_BCM2", Handle: 64, [BC] */
	,NULL					/* id: 0x339, Name: "STATUS_BCM", Handle: 65, [BC] */
	,NULL					/* id: 0x337, Name: "EXTERNAL_LIGHTS_1", Handle: 66, [BC] */
	,NULL					/* id: 0x309, Name: "RFHUB_B_A2", Handle: 67, [BC] */
	,NULL					/* id: 0x190, Name: "CBC_I4", Handle: 68, [BC] */
	,NULL					/* id: 0x0A4, Name: "RFHUB_B_A4", Handle: 69, [BC] */
	,NULL					/* id: 0x09C, Name: "STATUS_B_ECM2", Handle: 70, [BC] */
	,NULL					/* id: 0x09A, Name: "STATUS_B_ECM", Handle: 71, [BC] */
	,NULL					/* id: 0x098, Name: "STATUS_B_BSM", Handle: 72, [BC] */
	,NULL					/* id: 0x800, Name: "TxDynamicMsg0_1", Handle: 73, [BC] */
	,NULL					/* id: 0x800, Name: "TxDynamicMsg1_1", Handle: 74, [BC] */
};
#endif


#if defined (C_ENABLE_PART_OFFLINE)
V_MEMROM0 V_MEMROM1 canuint8 V_MEMROM2 CanTxSendMask[kCanNumberOfTxObjects] = {
	 C_SEND_GRP_APPLICATION		/* id: 0x7D4, Name: "STATUS_C_BCM2", Handle: 0, [BC] */
	,C_SEND_GRP_APPLICATION		/* id: 0x7D0, Name: "HVAC_INFO", Handle: 1, [BC] */
	,C_SEND_GRP_NONE		/* id: 0x7CC, Name: "CBC_PT4", Handle: 2, [BC] */
	,C_SEND_GRP_APPLICATION		/* id: 0x7CA, Name: "BCM_KEYON_COUNTER_C_CAN", Handle: 3, [BC] */
	,C_SEND_GRP_NONE		/* id: 0x7C8, Name: "IBS4", Handle: 4, [BC] */
	,C_SEND_GRP_APPLICATION		/* id: 0x1E7CAFF6, Name: "ECU_APPL_BCM_0", Handle: 5, [BC] */
	,C_SEND_GRP_NONE		/* id: 0x1E114000, Name: "CONFIGURATION_DATA_CODE_REQUEST_0", Handle: 6, [BC] */
	,C_SEND_GRP_APPLICATION		/* id: 0x6D2, Name: "CBC_VTA_0", Handle: 7, [BC] */
	,C_SEND_GRP_NONE		/* id: 0x18DAF140, Name: "DIAGNOSTIC_RESPONSE_BCM_0", Handle: 8, [BC] */
	,C_SEND_GRP_APPLICATION		/* id: 0x5DE, Name: "STATUS_B_CAN2", Handle: 9, [BC] */
	,C_SEND_GRP_APPLICATION		/* id: 0x5D8, Name: "EXTERNAL_LIGHTS_0", Handle: 10, [BC] */
	,C_SEND_GRP_NONE		/* id: 0x5D6, Name: "ASBM1_CONTROL", Handle: 11, [BC] */
	,C_SEND_GRP_NONE		/* id: 0x5D4, Name: "HVAC_INFO2", Handle: 12, [BC] */
	,C_SEND_GRP_NONE		/* id: 0x55F, Name: "IBS2", Handle: 13, [BC] */
	,C_SEND_GRP_APPLICATION		/* id: 0x4F4, Name: "VEHICLE_SPEED_ODOMETER_0", Handle: 14, [BC] */
	,C_SEND_GRP_APPLICATION		/* id: 0x4E2, Name: "STATUS_C_BCM", Handle: 15, [BC] */
	,C_SEND_GRP_APPLICATION		/* id: 0x4E0, Name: "STATUS_B_CAN", Handle: 16, [BC] */
	,C_SEND_GRP_NONE		/* id: 0x4D6, Name: "IBS3", Handle: 17, [BC] */
	,C_SEND_GRP_NONE		/* id: 0x4D4, Name: "IBS1", Handle: 18, [BC] */
	,C_SEND_GRP_NONE		/* id: 0x4CE, Name: "BATTERY_INFO", Handle: 19, [BC] */
	,C_SEND_GRP_APPLICATION		/* id: 0x3E8, Name: "CFG_Feature", Handle: 20, [BC] */
	,C_SEND_GRP_NONE		/* id: 0x3E6, Name: "CBC_PT3", Handle: 21, [BC] */
	,C_SEND_GRP_APPLICATION		/* id: 0x3E4, Name: "CBC_PT1", Handle: 22, [BC] */
	,C_SEND_GRP_NETMANAGEMENT		/* id: 0x0C1CD000, Name: "WAKE_C_BCM", Handle: 23, [BC] */
	,C_SEND_GRP_APPLICATION		/* id: 0x2EA, Name: "CBC_PT2", Handle: 24, [BC] */
	,C_SEND_GRP_APPLICATION		/* id: 0x1E8, Name: "BCM_COMMAND", Handle: 25, [BC] */
	,C_SEND_GRP_NONE		/* id: 0x0F7, Name: "BCM_CODE_TRM_REQUEST", Handle: 26, [BC] */
	,C_SEND_GRP_NONE		/* id: 0x0F6, Name: "BCM_CODE_ESL_REQUEST", Handle: 27, [BC] */
	,C_SEND_GRP_NONE		/* id: 0x0F5, Name: "BCM_MINICRYPT_ACK", Handle: 28, [BC] */
	,C_SEND_GRP_NONE		/* id: 0x0F4, Name: "IMMO_CODE_RESPONSE", Handle: 29, [BC] */
	,C_SEND_GRP_NONE		/* id: 0x800, Name: "TxDynamicMsg0_0", Handle: 30, [BC] */
	,C_SEND_GRP_NONE		/* id: 0x800, Name: "TxDynamicMsg1_0", Handle: 31, [BC] */
	,C_SEND_GRP_APPLICATION		/* id: 0x1E7CA0FF, Name: "ECU_APPL_BCM_1", Handle: 32, [BC] */
	,C_SEND_GRP_NONE		/* id: 0x1E3D4000, Name: "DTO_BCM", Handle: 33, [BC] */
	,C_SEND_GRP_APPLICATION		/* id: 0x1E114000, Name: "CONFIGURATION_DATA_CODE_REQUEST_1", Handle: 34, [BC] */
	,C_SEND_GRP_NONE		/* id: 0x772, Name: "IBS_2", Handle: 35, [BC] */
	,C_SEND_GRP_NONE		/* id: 0x76D, Name: "HUMIDITY_1000L", Handle: 36, [BC] */
	,C_SEND_GRP_APPLICATION		/* id: 0x762, Name: "COMPASS_A1", Handle: 37, [BC] */
	,C_SEND_GRP_APPLICATION		/* id: 0x760, Name: "CBC_I5", Handle: 38, [BC] */
	,C_SEND_GRP_APPLICATION		/* id: 0x75C, Name: "BCM_KEYON_COUNTER_B_CAN", Handle: 39, [BC] */
	,C_SEND_GRP_APPLICATION		/* id: 0x75A, Name: "AMB_TEMP_DISP", Handle: 40, [BC] */
	,C_SEND_GRP_NONE		/* id: 0x6A8, Name: "WCPM_STATUS", Handle: 41, [BC] */
	,C_SEND_GRP_APPLICATION		/* id: 0x6A6, Name: "GE_B", Handle: 42, [BC] */
	,C_SEND_GRP_APPLICATION		/* id: 0x6A4, Name: "ENVIRONMENTAL_CONDITIONS", Handle: 43, [BC] */
	,C_SEND_GRP_APPLICATION		/* id: 0x6A2, Name: "CBC_VTA_1", Handle: 44, [BC] */
	,C_SEND_GRP_7		/* id: 0x18DAF140, Name: "DIAGNOSTIC_RESPONSE_BCM_1", Handle: 45, [BC] */
	,C_SEND_GRP_APPLICATION		/* id: 0x5DC, Name: "GW_C_I2", Handle: 46, [BC] */
	,C_SEND_GRP_NONE		/* id: 0x5CC, Name: "DAS_B", Handle: 47, [BC] */
	,C_SEND_GRP_APPLICATION		/* id: 0x5CA, Name: "CBC_I2", Handle: 48, [BC] */
	,C_SEND_GRP_APPLICATION		/* id: 0x5C8, Name: "CBC_I1", Handle: 49, [BC] */
	,C_SEND_GRP_NONE		/* id: 0x550, Name: "STATUS_B_EPB", Handle: 50, [BC] */
	,C_SEND_GRP_APPLICATION		/* id: 0x548, Name: "STATUS_C_CAN", Handle: 51, [BC] */
	,C_SEND_GRP_NONE		/* id: 0x52E, Name: "STATUS_B_HALF", Handle: 52, [BC] */
	,C_SEND_GRP_NONE		/* id: 0x52D, Name: "HALF_B_Warning_RQ", Handle: 53, [BC] */
	,C_SEND_GRP_NONE		/* id: 0x4A3, Name: "SWS_8", Handle: 54, [BC] */
	,C_SEND_GRP_APPLICATION		/* id: 0x4A1, Name: "StW_Actn_Rq_1", Handle: 55, [BC] */
	,C_SEND_GRP_APPLICATION		/* id: 0x499, Name: "PAM_B", Handle: 56, [BC] */
	,C_SEND_GRP_APPLICATION		/* id: 0x493, Name: "GW_C_I3", Handle: 57, [BC] */
	,C_SEND_GRP_APPLICATION		/* id: 0x3D3, Name: "VIN_1", Handle: 58, [BC] */
	,C_SEND_GRP_APPLICATION		/* id: 0x3D1, Name: "VEHICLE_SPEED_ODOMETER_1", Handle: 59, [BC] */
	,C_SEND_GRP_APPLICATION		/* id: 0x3CF, Name: "GW_C_I6", Handle: 60, [BC] */
	,C_SEND_GRP_NONE		/* id: 0x3CB, Name: "DYNAMIC_VEHICLE_INFO2", Handle: 61, [BC] */
	,C_SEND_GRP_NONE		/* id: 0x385, Name: "STATUS_B_TRANSMISSION", Handle: 62, [BC] */
	,C_SEND_GRP_NETMANAGEMENT		/* id: 0x0E094000, Name: "NWM_BCM", Handle: 63, [BC] */
	,C_SEND_GRP_APPLICATION		/* id: 0x33B, Name: "STATUS_BCM2", Handle: 64, [BC] */
	,C_SEND_GRP_APPLICATION		/* id: 0x339, Name: "STATUS_BCM", Handle: 65, [BC] */
	,C_SEND_GRP_APPLICATION		/* id: 0x337, Name: "EXTERNAL_LIGHTS_1", Handle: 66, [BC] */
	,C_SEND_GRP_NONE		/* id: 0x309, Name: "RFHUB_B_A2", Handle: 67, [BC] */
	,C_SEND_GRP_APPLICATION		/* id: 0x190, Name: "CBC_I4", Handle: 68, [BC] */
	,C_SEND_GRP_NONE		/* id: 0x0A4, Name: "RFHUB_B_A4", Handle: 69, [BC] */
	,C_SEND_GRP_APPLICATION		/* id: 0x09C, Name: "STATUS_B_ECM2", Handle: 70, [BC] */
	,C_SEND_GRP_APPLICATION		/* id: 0x09A, Name: "STATUS_B_ECM", Handle: 71, [BC] */
	,C_SEND_GRP_APPLICATION		/* id: 0x098, Name: "STATUS_B_BSM", Handle: 72, [BC] */
	,C_SEND_GRP_NONE		/* id: 0x800, Name: "TxDynamicMsg0_1", Handle: 73, [BC] */
	,C_SEND_GRP_NONE		/* id: 0x800, Name: "TxDynamicMsg1_1", Handle: 74, [BC] */
};
#endif



/*************************************************************/
/* Databuffer for confirmationflags                          */
/*************************************************************/

#ifdef C_ENABLE_CONFIRMATION_FLAG
V_MEMROM0 V_MEMROM1 canuint8 V_MEMROM2 CanConfirmationOffset[kCanNumberOfTxObjects] = {
	   0	,  0	,  0	,  0	,  0	,  0	,  0	,  0
	,  1	,  1	,  1	,  1	,  1	,  1	,  1	,  1
	,  2	,  2	,  2	,  2	,  2	,  2	,  2	,  2
	,  3	,  3	,  3	,  3	,  3	,  3	,  0	,  0
	,  3	,  3	,  4	,  4	,  4	,  4	,  4	,  4
	,  4	,  4	,  5	,  5	,  5	,  5	,  5	,  5
	,  5	,  5	,  6	,  6	,  6	,  6	,  6	,  6
	,  6	,  6	,  7	,  7	,  7	,  7	,  7	,  7
	,  7	,  7	,  8	,  8	,  8	,  8	,  8	,  8
	,  8	,  0	,  0
};

V_MEMROM0 V_MEMROM1 canuint8 V_MEMROM2 CanConfirmationMask[kCanNumberOfTxObjects] = {
	 0x01	,0x02	,0x04	,0x08	,0x10	,0x20	,0x40	,0x80
	,0x01	,0x02	,0x04	,0x08	,0x10	,0x20	,0x40	,0x80
	,0x01	,0x02	,0x04	,0x08	,0x10	,0x20	,0x40	,0x80
	,0x01	,0x02	,0x04	,0x08	,0x10	,0x20	,0x00	,0x00
	,0x40	,0x80	,0x01	,0x02	,0x04	,0x08	,0x10	,0x20
	,0x40	,0x80	,0x01	,0x02	,0x04	,0x08	,0x10	,0x20
	,0x40	,0x80	,0x01	,0x02	,0x04	,0x08	,0x10	,0x20
	,0x40	,0x80	,0x01	,0x02	,0x04	,0x08	,0x10	,0x20
	,0x40	,0x80	,0x01	,0x02	,0x04	,0x08	,0x10	,0x20
	,0x40	,0x00	,0x00
};
#endif


/*************************************************************/
/* Receive structures                                        */
/*************************************************************/
V_MEMROM0 V_MEMROM1 tCanRxId0 V_MEMROM2 CanRxId0[kCanNumberOfRxObjects] = {
	 MK_STDID0(0x7d6)		/* id: 0x7D6, Name: "STATUS_C_OCM", Handle: 0, [BC] */
	,MK_EXTID0(0x1e11404e)		/* id: 0x1E11404E, Name: "CFG_DATA_CODE_RSP_OCM", Handle: 1, [BC] */
	,MK_EXTID0(0x1e11404c)		/* id: 0x1E11404C, Name: "CFG_DATA_CODE_RSP_AHLM", Handle: 2, [BC] */
	,MK_EXTID0(0x1e114043)		/* id: 0x1E114043, Name: "CFG_DATA_CODE_RSP_DTCM", Handle: 3, [BC] */
	,MK_EXTID0(0x1e114041)		/* id: 0x1E114041, Name: "CFG_DATA_CODE_RSP_RFHM", Handle: 4, [BC] */
	,MK_EXTID0(0x1e114039)		/* id: 0x1E114039, Name: "CFG_DATA_CODE_RSP_DASM", Handle: 5, [BC] */
	,MK_EXTID0(0x1e114035)		/* id: 0x1E114035, Name: "CFG_DATA_CODE_RSP_SCCM", Handle: 6, [BC] */
	,MK_EXTID0(0x1e11401e)		/* id: 0x1E11401E, Name: "CFG_DATA_CODE_RSP_HALF", Handle: 7, [BC] */
	,MK_EXTID0(0x1e11401a)		/* id: 0x1E11401A, Name: "CFG_DATA_CODE_RSP_ORC", Handle: 8, [BC] */
	,MK_EXTID0(0x1e114018)		/* id: 0x1E114018, Name: "CFG_DATA_CODE_RSP_PAM", Handle: 9, [BC] */
	,MK_EXTID0(0x1e11400d)		/* id: 0x1E11400D, Name: "CFG_DATA_CODE_RSP_EPB", Handle: 10, [BC] */
	,MK_EXTID0(0x1e11400b)		/* id: 0x1E11400B, Name: "CFG_DATA_CODE_RSP_TRANSMISSION", Handle: 11, [BC] */
	,MK_EXTID0(0x1e114006)		/* id: 0x1E114006, Name: "CFG_DATA_CODE_RSP_BSM", Handle: 12, [BC] */
	,MK_EXTID0(0x1e114002)		/* id: 0x1E114002, Name: "CFG_DATA_CODE_RSP_EPS", Handle: 13, [BC] */
	,MK_EXTID0(0x1e114001)		/* id: 0x1E114001, Name: "CFG_DATA_CODE_RSP_ECM", Handle: 14, [BC] */
	,MK_STDID0(0x6d8)		/* id: 0x6D8, Name: "STATUS_C_DASM", Handle: 15, [BC] */
	,MK_STDID0(0x6d6)		/* id: 0x6D6, Name: "STATUS_C_AHLM", Handle: 16, [BC] */
	,MK_EXTID0(0x18dbfef1)		/* id: 0x18DBFEF1, Name: "DIAGNOSTIC_REQUEST_FUNCTIONAL", Handle: 17, [BC] */
	,MK_EXTID0(0x18da40f1)		/* id: 0x18DA40F1, Name: "DIAGNOSTIC_REQUEST_BCM_0", Handle: 18, [BC] */
	,MK_STDID0(0x5e4)		/* id: 0x5E4, Name: "STATUS_C_HALF", Handle: 19, [BC] */
	,MK_STDID0(0x5e2)		/* id: 0x5E2, Name: "STATUS_C_ESL", Handle: 20, [BC] */
	,MK_STDID0(0x5e0)		/* id: 0x5E0, Name: "STATUS_C_EPB", Handle: 21, [BC] */
	,MK_STDID0(0x5da)		/* id: 0x5DA, Name: "HALF_C_Warning_RQ", Handle: 22, [BC] */
	,MK_STDID0(0x4f0)		/* id: 0x4F0, Name: "StW_Actn_Rq_0", Handle: 23, [BC] */
	,MK_STDID0(0x4ee)		/* id: 0x4EE, Name: "STATUS_C_TRANSMISSION", Handle: 24, [BC] */
	,MK_STDID0(0x4ec)		/* id: 0x4EC, Name: "VIN_0", Handle: 25, [BC] */
	,MK_STDID0(0x4d8)		/* id: 0x4D8, Name: "MOT3", Handle: 26, [BC] */
	,MK_STDID0(0x4d0)		/* id: 0x4D0, Name: "ECM_1", Handle: 27, [BC] */
	,MK_STDID0(0x4cc)		/* id: 0x4CC, Name: "STATUS_C_DTCM", Handle: 28, [BC] */
	,MK_STDID0(0x3ee)		/* id: 0x3EE, Name: "DAS_A2", Handle: 29, [BC] */
	,MK_STDID0(0x2f4)		/* id: 0x2F4, Name: "PAM_2", Handle: 30, [BC] */
	,MK_STDID0(0x2ee)		/* id: 0x2EE, Name: "EPB_A1", Handle: 31, [BC] */
	,MK_STDID0(0x2ec)		/* id: 0x2EC, Name: "DAS_A1", Handle: 32, [BC] */
	,MK_STDID0(0x2e8)		/* id: 0x2E8, Name: "BSM_4", Handle: 33, [BC] */
	,MK_STDID0(0x2e4)		/* id: 0x2E4, Name: "BSM_2", Handle: 34, [BC] */
	,MK_STDID0(0x2e0)		/* id: 0x2E0, Name: "DTCM_A1", Handle: 35, [BC] */
	,MK_STDID0(0x20a)		/* id: 0x20A, Name: "SC", Handle: 36, [BC] */
	,MK_STDID0(0x208)		/* id: 0x208, Name: "ORC_YRS_DATA", Handle: 37, [BC] */
	,MK_STDID0(0x202)		/* id: 0x202, Name: "MOTGEAR2", Handle: 38, [BC] */
	,MK_STDID0(0x1fc)		/* id: 0x1FC, Name: "MOT4", Handle: 39, [BC] */
	,MK_STDID0(0x1ea)		/* id: 0x1EA, Name: "BSM_YRS_DATA", Handle: 40, [BC] */
	,MK_STDID0(0x1e6)		/* id: 0x1E6, Name: "ASR4", Handle: 41, [BC] */
	,MK_STDID0(0x1e4)		/* id: 0x1E4, Name: "ASR3", Handle: 42, [BC] */
	,MK_STDID0(0x100)		/* id: 0x100, Name: "RFHUB_A4", Handle: 43, [BC] */
	,MK_STDID0(0xfa)		/* id: 0x0FA, Name: "IPC_CFG_Feature", Handle: 44, [BC] */
	,MK_STDID0(0xf8)		/* id: 0x0F8, Name: "ESL_CODE_RESPONSE", Handle: 45, [BC] */
	,MK_STDID0(0xf2)		/* id: 0x0F2, Name: "IMMO_CODE_REQUEST", Handle: 46, [BC] */
	,MK_EXTID0(0xf3)		/* id: 0x000000F3, Name: "TRM_MKP_KEY", Handle: 47, [BC] */
	,MK_STDID0(0x7d2)		/* id: 0x7D2, Name: "RFHUB_A3", Handle: 48, [FC] */
	,MK_EXTID0(0x1e7cafe5)		/* id: 0x1E7CAFE5, Name: "APPL_ECU_BCM_0", Handle: 49, [FC] */
	,MK_STDID0(0x6dc)		/* id: 0x6DC, Name: "STATUS_C_SCCM", Handle: 50, [FC] */
	,MK_STDID0(0x6da)		/* id: 0x6DA, Name: "STATUS_C_RFHM", Handle: 51, [FC] */
	,MK_STDID0(0x659)		/* id: 0x659, Name: "IPC_A1", Handle: 52, [FC] */
	,MK_STDID0(0x5e6)		/* id: 0x5E6, Name: "STATUS_C_SPM", Handle: 53, [FC] */
	,MK_STDID0(0x4ea)		/* id: 0x4EA, Name: "STATUS_C_IPC", Handle: 54, [FC] */
	,MK_STDID0(0x4e8)		/* id: 0x4E8, Name: "STATUS_C_GSM", Handle: 55, [FC] */
	,MK_STDID0(0x4e6)		/* id: 0x4E6, Name: "STATUS_C_EPS", Handle: 56, [FC] */
	,MK_STDID0(0x4e4)		/* id: 0x4E4, Name: "STATUS_C_BSM", Handle: 57, [FC] */
	,MK_STDID0(0x3ec)		/* id: 0x3EC, Name: "STATUS_C_ECM2", Handle: 58, [FC] */
	,MK_STDID0(0x3ea)		/* id: 0x3EA, Name: "STATUS_C_ECM", Handle: 59, [FC] */
	,MK_STDID0(0x36b)		/* id: 0x36B, Name: "STATUS_C_ORC", Handle: 60, [FC] */
	,MK_STDID0(0x2f8)		/* id: 0x2F8, Name: "RFHUB_A2", Handle: 61, [FC] */
	,MK_STDID0(0x2f6)		/* id: 0x2F6, Name: "RFHUB_A1", Handle: 62, [FC] */
	,MK_STDID0(0x2f2)		/* id: 0x2F2, Name: "PAM_1", Handle: 63, [FC] */
	,MK_EXTID0(0x817a041)		/* id: 0x0817A041, Name: "WAKE_C_RFHM", Handle: 64, [FC] */
	,MK_EXTID0(0x817a035)		/* id: 0x0817A035, Name: "WAKE_C_SCCM", Handle: 65, [FC] */
	,MK_EXTID0(0x817a015)		/* id: 0x0817A015, Name: "WAKE_C_ESL", Handle: 66, [FC] */
	,MK_EXTID0(0x817a00d)		/* id: 0x0817A00D, Name: "WAKE_C_EPB", Handle: 67, [FC] */
	,MK_EXTID0(0x817a006)		/* id: 0x0817A006, Name: "WAKE_C_BSM", Handle: 68, [FC] */
	,MK_STDID0(0x1f8)		/* id: 0x1F8, Name: "MOT1", Handle: 69, [FC] */
	,MK_STDID0(0x1ee)		/* id: 0x1EE, Name: "GE", Handle: 70, [FC] */
	,MK_STDID0(0xfd)		/* id: 0x0FD, Name: "TRM_CODE_RESPONSE", Handle: 71, [FC] */
	,MK_STDID0(0xfc)		/* id: 0x0FC, Name: "ORC_A2", Handle: 72, [FC] */
	,MK_EXTID0(0x1e394000)		/* id: 0x1E394000, Name: "CRO_BCM", Handle: 73, [BC] */
	,MK_EXTID0(0x1e114034)		/* id: 0x1E114034, Name: "CFG_DATA_CODE_RSP_RBSS", Handle: 74, [BC] */
	,MK_EXTID0(0x1e114033)		/* id: 0x1E114033, Name: "CFG_DATA_CODE_RSP_LBSS", Handle: 75, [BC] */
	,MK_EXTID0(0x1e114032)		/* id: 0x1E114032, Name: "CFG_DATA_CODE_RSP_ICS", Handle: 76, [BC] */
	,MK_EXTID0(0x1e114030)		/* id: 0x1E114030, Name: "CFG_DATA_CODE_RSP_CSWM", Handle: 77, [BC] */
	,MK_EXTID0(0x1e11402f)		/* id: 0x1E11402F, Name: "CFG_DATA_CODE_RSP_CDM", Handle: 78, [BC] */
	,MK_EXTID0(0x1e11402e)		/* id: 0x1E11402E, Name: "CFG_DATA_CODE_RSP_AMP", Handle: 79, [BC] */
	,MK_EXTID0(0x1e114024)		/* id: 0x1E114024, Name: "CFG_DATA_CODE_RSP_ETM", Handle: 80, [BC] */
	,MK_EXTID0(0x1e114012)		/* id: 0x1E114012, Name: "CFG_DATA_CODE_RSP_DSM", Handle: 81, [BC] */
	,MK_EXTID0(0x1e114011)		/* id: 0x1E114011, Name: "CFG_DATA_CODE_RSP_PDM", Handle: 82, [BC] */
	,MK_EXTID0(0x1e11400a)		/* id: 0x1E11400A, Name: "CFG_DATA_CODE_RSP_ECC", Handle: 83, [BC] */
	,MK_EXTID0(0x1e114008)		/* id: 0x1E114008, Name: "CFG_DATA_CODE_RSP_DDM", Handle: 84, [BC] */
	,MK_EXTID0(0x1e114003)		/* id: 0x1E114003, Name: "CFG_DATA_CODE_RSP_IPC", Handle: 85, [BC] */
	,MK_STDID0(0x77a)		/* id: 0x77A, Name: "STATUS_ICS", Handle: 86, [BC] */
	,MK_STDID0(0x778)		/* id: 0x778, Name: "STATUS_CDM", Handle: 87, [BC] */
	,MK_STDID0(0x776)		/* id: 0x776, Name: "STATUS_AMP", Handle: 88, [BC] */
	,MK_STDID0(0x6a4)		/* id: 0x6A4, Name: "ENVIRONMENTAL_CONDITIONS", Handle: 89, [BC] */
	,MK_STDID0(0x5da)		/* id: 0x5DA, Name: "FT_HVAC_STAT", Handle: 90, [BC] */
	,MK_STDID0(0x5d0)		/* id: 0x5D0, Name: "DCM_P_MSG", Handle: 91, [BC] */
	,MK_STDID0(0x5ce)		/* id: 0x5CE, Name: "DCM_D_MSG", Handle: 92, [BC] */
	,MK_EXTID0(0xe094045)		/* id: 0x0E094045, Name: "NWM_PLGM", Handle: 93, [BC] */
	,MK_EXTID0(0xe094024)		/* id: 0x0E094024, Name: "NWM_ETM", Handle: 94, [BC] */
	,MK_EXTID0(0xe094012)		/* id: 0x0E094012, Name: "NWM_DSM", Handle: 95, [BC] */
	,MK_EXTID0(0xe094011)		/* id: 0x0E094011, Name: "NWM_PDM", Handle: 96, [BC] */
	,MK_EXTID0(0xe094008)		/* id: 0x0E094008, Name: "NWM_DDM", Handle: 97, [BC] */
	,MK_EXTID0(0x1e7ca0fe)		/* id: 0x1E7CA0FE, Name: "APPL_ECU_BCM_1", Handle: 98, [FC] */
	,MK_STDID0(0x77e)		/* id: 0x77E, Name: "TRIP_A_B", Handle: 99, [FC] */
	,MK_EXTID0(0x18da40f1)		/* id: 0x18DA40F1, Name: "DIAGNOSTIC_REQUEST_BCM_1", Handle: 100, [FC] */
	,MK_STDID0(0x5e6)		/* id: 0x5E6, Name: "TGW_A1", Handle: 101, [FC] */
	,MK_STDID0(0x5e2)		/* id: 0x5E2, Name: "ICS_MSG", Handle: 102, [FC] */
	,MK_STDID0(0x5de)		/* id: 0x5DE, Name: "HVAC_A1", Handle: 103, [FC] */
	,MK_STDID0(0x5d2)		/* id: 0x5D2, Name: "DIRECT_INFO", Handle: 104, [FC] */
	,MK_STDID0(0x546)		/* id: 0x546, Name: "HVAC_A4", Handle: 105, [FC] */
	,MK_STDID0(0x49f)		/* id: 0x49F, Name: "STATUS_RRM", Handle: 106, [FC] */
	,MK_STDID0(0x49d)		/* id: 0x49D, Name: "STATUS_IPC", Handle: 107, [FC] */
	,MK_STDID0(0x49b)		/* id: 0x49B, Name: "STATUS_ECC", Handle: 108, [FC] */
	,MK_STDID0(0x495)		/* id: 0x495, Name: "HVAC_A2", Handle: 109, [FC] */
	,MK_STDID0(0x489)		/* id: 0x489, Name: "PLG_A1", Handle: 110, [FC] */
	,MK_STDID0(0x3cd)		/* id: 0x3CD, Name: "ECC_A1", Handle: 111, [FC] */
	,MK_EXTID0(0xe094034)		/* id: 0x0E094034, Name: "NWM_RBSS", Handle: 112, [FC] */
	,MK_EXTID0(0xe094033)		/* id: 0x0E094033, Name: "NWM_LBSS", Handle: 113, [FC] */
	,MK_EXTID0(0xe094032)		/* id: 0x0E094032, Name: "NWM_ICS", Handle: 114, [FC] */
	,MK_EXTID0(0xe094030)		/* id: 0x0E094030, Name: "NWM_CSWM", Handle: 115, [FC] */
	,MK_EXTID0(0xe09402f)		/* id: 0x0E09402F, Name: "NWM_CDM", Handle: 116, [FC] */
	,MK_EXTID0(0xe09402e)		/* id: 0x0E09402E, Name: "NWM_AMP", Handle: 117, [FC] */
	,MK_EXTID0(0xe09400a)		/* id: 0x0E09400A, Name: "NWM_ECC", Handle: 118, [FC] */
	,MK_EXTID0(0xe094003)		/* id: 0x0E094003, Name: "NWM_IPC", Handle: 119, [FC] */
	,MK_STDID0(0x190)		/* id: 0x190, Name: "CBC_I4", Handle: 120, [FC] */
	,MK_STDID0(0x8c)		/* id: 0x08C, Name: "CMCM_UNLK", Handle: 121, [FC] */
	,MK_STDID0(0x8a)		/* id: 0x08A, Name: "CFG_RQ", Handle: 122, [FC] */
};

V_MEMROM0 V_MEMROM1 tCanRxId1 V_MEMROM2 CanRxId1[kCanNumberOfRxObjects] = {
	 MK_STDID1(0x7d6)		/* id: 0x7D6, Name: "STATUS_C_OCM", Handle: 0, [BC] */
	,MK_EXTID1(0x1e11404e)		/* id: 0x1E11404E, Name: "CFG_DATA_CODE_RSP_OCM", Handle: 1, [BC] */
	,MK_EXTID1(0x1e11404c)		/* id: 0x1E11404C, Name: "CFG_DATA_CODE_RSP_AHLM", Handle: 2, [BC] */
	,MK_EXTID1(0x1e114043)		/* id: 0x1E114043, Name: "CFG_DATA_CODE_RSP_DTCM", Handle: 3, [BC] */
	,MK_EXTID1(0x1e114041)		/* id: 0x1E114041, Name: "CFG_DATA_CODE_RSP_RFHM", Handle: 4, [BC] */
	,MK_EXTID1(0x1e114039)		/* id: 0x1E114039, Name: "CFG_DATA_CODE_RSP_DASM", Handle: 5, [BC] */
	,MK_EXTID1(0x1e114035)		/* id: 0x1E114035, Name: "CFG_DATA_CODE_RSP_SCCM", Handle: 6, [BC] */
	,MK_EXTID1(0x1e11401e)		/* id: 0x1E11401E, Name: "CFG_DATA_CODE_RSP_HALF", Handle: 7, [BC] */
	,MK_EXTID1(0x1e11401a)		/* id: 0x1E11401A, Name: "CFG_DATA_CODE_RSP_ORC", Handle: 8, [BC] */
	,MK_EXTID1(0x1e114018)		/* id: 0x1E114018, Name: "CFG_DATA_CODE_RSP_PAM", Handle: 9, [BC] */
	,MK_EXTID1(0x1e11400d)		/* id: 0x1E11400D, Name: "CFG_DATA_CODE_RSP_EPB", Handle: 10, [BC] */
	,MK_EXTID1(0x1e11400b)		/* id: 0x1E11400B, Name: "CFG_DATA_CODE_RSP_TRANSMISSION", Handle: 11, [BC] */
	,MK_EXTID1(0x1e114006)		/* id: 0x1E114006, Name: "CFG_DATA_CODE_RSP_BSM", Handle: 12, [BC] */
	,MK_EXTID1(0x1e114002)		/* id: 0x1E114002, Name: "CFG_DATA_CODE_RSP_EPS", Handle: 13, [BC] */
	,MK_EXTID1(0x1e114001)		/* id: 0x1E114001, Name: "CFG_DATA_CODE_RSP_ECM", Handle: 14, [BC] */
	,MK_STDID1(0x6d8)		/* id: 0x6D8, Name: "STATUS_C_DASM", Handle: 15, [BC] */
	,MK_STDID1(0x6d6)		/* id: 0x6D6, Name: "STATUS_C_AHLM", Handle: 16, [BC] */
	,MK_EXTID1(0x18dbfef1)		/* id: 0x18DBFEF1, Name: "DIAGNOSTIC_REQUEST_FUNCTIONAL", Handle: 17, [BC] */
	,MK_EXTID1(0x18da40f1)		/* id: 0x18DA40F1, Name: "DIAGNOSTIC_REQUEST_BCM_0", Handle: 18, [BC] */
	,MK_STDID1(0x5e4)		/* id: 0x5E4, Name: "STATUS_C_HALF", Handle: 19, [BC] */
	,MK_STDID1(0x5e2)		/* id: 0x5E2, Name: "STATUS_C_ESL", Handle: 20, [BC] */
	,MK_STDID1(0x5e0)		/* id: 0x5E0, Name: "STATUS_C_EPB", Handle: 21, [BC] */
	,MK_STDID1(0x5da)		/* id: 0x5DA, Name: "HALF_C_Warning_RQ", Handle: 22, [BC] */
	,MK_STDID1(0x4f0)		/* id: 0x4F0, Name: "StW_Actn_Rq_0", Handle: 23, [BC] */
	,MK_STDID1(0x4ee)		/* id: 0x4EE, Name: "STATUS_C_TRANSMISSION", Handle: 24, [BC] */
	,MK_STDID1(0x4ec)		/* id: 0x4EC, Name: "VIN_0", Handle: 25, [BC] */
	,MK_STDID1(0x4d8)		/* id: 0x4D8, Name: "MOT3", Handle: 26, [BC] */
	,MK_STDID1(0x4d0)		/* id: 0x4D0, Name: "ECM_1", Handle: 27, [BC] */
	,MK_STDID1(0x4cc)		/* id: 0x4CC, Name: "STATUS_C_DTCM", Handle: 28, [BC] */
	,MK_STDID1(0x3ee)		/* id: 0x3EE, Name: "DAS_A2", Handle: 29, [BC] */
	,MK_STDID1(0x2f4)		/* id: 0x2F4, Name: "PAM_2", Handle: 30, [BC] */
	,MK_STDID1(0x2ee)		/* id: 0x2EE, Name: "EPB_A1", Handle: 31, [BC] */
	,MK_STDID1(0x2ec)		/* id: 0x2EC, Name: "DAS_A1", Handle: 32, [BC] */
	,MK_STDID1(0x2e8)		/* id: 0x2E8, Name: "BSM_4", Handle: 33, [BC] */
	,MK_STDID1(0x2e4)		/* id: 0x2E4, Name: "BSM_2", Handle: 34, [BC] */
	,MK_STDID1(0x2e0)		/* id: 0x2E0, Name: "DTCM_A1", Handle: 35, [BC] */
	,MK_STDID1(0x20a)		/* id: 0x20A, Name: "SC", Handle: 36, [BC] */
	,MK_STDID1(0x208)		/* id: 0x208, Name: "ORC_YRS_DATA", Handle: 37, [BC] */
	,MK_STDID1(0x202)		/* id: 0x202, Name: "MOTGEAR2", Handle: 38, [BC] */
	,MK_STDID1(0x1fc)		/* id: 0x1FC, Name: "MOT4", Handle: 39, [BC] */
	,MK_STDID1(0x1ea)		/* id: 0x1EA, Name: "BSM_YRS_DATA", Handle: 40, [BC] */
	,MK_STDID1(0x1e6)		/* id: 0x1E6, Name: "ASR4", Handle: 41, [BC] */
	,MK_STDID1(0x1e4)		/* id: 0x1E4, Name: "ASR3", Handle: 42, [BC] */
	,MK_STDID1(0x100)		/* id: 0x100, Name: "RFHUB_A4", Handle: 43, [BC] */
	,MK_STDID1(0xfa)		/* id: 0x0FA, Name: "IPC_CFG_Feature", Handle: 44, [BC] */
	,MK_STDID1(0xf8)		/* id: 0x0F8, Name: "ESL_CODE_RESPONSE", Handle: 45, [BC] */
	,MK_STDID1(0xf2)		/* id: 0x0F2, Name: "IMMO_CODE_REQUEST", Handle: 46, [BC] */
	,MK_EXTID1(0xf3)		/* id: 0x000000F3, Name: "TRM_MKP_KEY", Handle: 47, [BC] */
	,MK_STDID1(0x7d2)		/* id: 0x7D2, Name: "RFHUB_A3", Handle: 48, [FC] */
	,MK_EXTID1(0x1e7cafe5)		/* id: 0x1E7CAFE5, Name: "APPL_ECU_BCM_0", Handle: 49, [FC] */
	,MK_STDID1(0x6dc)		/* id: 0x6DC, Name: "STATUS_C_SCCM", Handle: 50, [FC] */
	,MK_STDID1(0x6da)		/* id: 0x6DA, Name: "STATUS_C_RFHM", Handle: 51, [FC] */
	,MK_STDID1(0x659)		/* id: 0x659, Name: "IPC_A1", Handle: 52, [FC] */
	,MK_STDID1(0x5e6)		/* id: 0x5E6, Name: "STATUS_C_SPM", Handle: 53, [FC] */
	,MK_STDID1(0x4ea)		/* id: 0x4EA, Name: "STATUS_C_IPC", Handle: 54, [FC] */
	,MK_STDID1(0x4e8)		/* id: 0x4E8, Name: "STATUS_C_GSM", Handle: 55, [FC] */
	,MK_STDID1(0x4e6)		/* id: 0x4E6, Name: "STATUS_C_EPS", Handle: 56, [FC] */
	,MK_STDID1(0x4e4)		/* id: 0x4E4, Name: "STATUS_C_BSM", Handle: 57, [FC] */
	,MK_STDID1(0x3ec)		/* id: 0x3EC, Name: "STATUS_C_ECM2", Handle: 58, [FC] */
	,MK_STDID1(0x3ea)		/* id: 0x3EA, Name: "STATUS_C_ECM", Handle: 59, [FC] */
	,MK_STDID1(0x36b)		/* id: 0x36B, Name: "STATUS_C_ORC", Handle: 60, [FC] */
	,MK_STDID1(0x2f8)		/* id: 0x2F8, Name: "RFHUB_A2", Handle: 61, [FC] */
	,MK_STDID1(0x2f6)		/* id: 0x2F6, Name: "RFHUB_A1", Handle: 62, [FC] */
	,MK_STDID1(0x2f2)		/* id: 0x2F2, Name: "PAM_1", Handle: 63, [FC] */
	,MK_EXTID1(0x817a041)		/* id: 0x0817A041, Name: "WAKE_C_RFHM", Handle: 64, [FC] */
	,MK_EXTID1(0x817a035)		/* id: 0x0817A035, Name: "WAKE_C_SCCM", Handle: 65, [FC] */
	,MK_EXTID1(0x817a015)		/* id: 0x0817A015, Name: "WAKE_C_ESL", Handle: 66, [FC] */
	,MK_EXTID1(0x817a00d)		/* id: 0x0817A00D, Name: "WAKE_C_EPB", Handle: 67, [FC] */
	,MK_EXTID1(0x817a006)		/* id: 0x0817A006, Name: "WAKE_C_BSM", Handle: 68, [FC] */
	,MK_STDID1(0x1f8)		/* id: 0x1F8, Name: "MOT1", Handle: 69, [FC] */
	,MK_STDID1(0x1ee)		/* id: 0x1EE, Name: "GE", Handle: 70, [FC] */
	,MK_STDID1(0xfd)		/* id: 0x0FD, Name: "TRM_CODE_RESPONSE", Handle: 71, [FC] */
	,MK_STDID1(0xfc)		/* id: 0x0FC, Name: "ORC_A2", Handle: 72, [FC] */
	,MK_EXTID1(0x1e394000)		/* id: 0x1E394000, Name: "CRO_BCM", Handle: 73, [BC] */
	,MK_EXTID1(0x1e114034)		/* id: 0x1E114034, Name: "CFG_DATA_CODE_RSP_RBSS", Handle: 74, [BC] */
	,MK_EXTID1(0x1e114033)		/* id: 0x1E114033, Name: "CFG_DATA_CODE_RSP_LBSS", Handle: 75, [BC] */
	,MK_EXTID1(0x1e114032)		/* id: 0x1E114032, Name: "CFG_DATA_CODE_RSP_ICS", Handle: 76, [BC] */
	,MK_EXTID1(0x1e114030)		/* id: 0x1E114030, Name: "CFG_DATA_CODE_RSP_CSWM", Handle: 77, [BC] */
	,MK_EXTID1(0x1e11402f)		/* id: 0x1E11402F, Name: "CFG_DATA_CODE_RSP_CDM", Handle: 78, [BC] */
	,MK_EXTID1(0x1e11402e)		/* id: 0x1E11402E, Name: "CFG_DATA_CODE_RSP_AMP", Handle: 79, [BC] */
	,MK_EXTID1(0x1e114024)		/* id: 0x1E114024, Name: "CFG_DATA_CODE_RSP_ETM", Handle: 80, [BC] */
	,MK_EXTID1(0x1e114012)		/* id: 0x1E114012, Name: "CFG_DATA_CODE_RSP_DSM", Handle: 81, [BC] */
	,MK_EXTID1(0x1e114011)		/* id: 0x1E114011, Name: "CFG_DATA_CODE_RSP_PDM", Handle: 82, [BC] */
	,MK_EXTID1(0x1e11400a)		/* id: 0x1E11400A, Name: "CFG_DATA_CODE_RSP_ECC", Handle: 83, [BC] */
	,MK_EXTID1(0x1e114008)		/* id: 0x1E114008, Name: "CFG_DATA_CODE_RSP_DDM", Handle: 84, [BC] */
	,MK_EXTID1(0x1e114003)		/* id: 0x1E114003, Name: "CFG_DATA_CODE_RSP_IPC", Handle: 85, [BC] */
	,MK_STDID1(0x77a)		/* id: 0x77A, Name: "STATUS_ICS", Handle: 86, [BC] */
	,MK_STDID1(0x778)		/* id: 0x778, Name: "STATUS_CDM", Handle: 87, [BC] */
	,MK_STDID1(0x776)		/* id: 0x776, Name: "STATUS_AMP", Handle: 88, [BC] */
	,MK_STDID1(0x6a4)		/* id: 0x6A4, Name: "ENVIRONMENTAL_CONDITIONS", Handle: 89, [BC] */
	,MK_STDID1(0x5da)		/* id: 0x5DA, Name: "FT_HVAC_STAT", Handle: 90, [BC] */
	,MK_STDID1(0x5d0)		/* id: 0x5D0, Name: "DCM_P_MSG", Handle: 91, [BC] */
	,MK_STDID1(0x5ce)		/* id: 0x5CE, Name: "DCM_D_MSG", Handle: 92, [BC] */
	,MK_EXTID1(0xe094045)		/* id: 0x0E094045, Name: "NWM_PLGM", Handle: 93, [BC] */
	,MK_EXTID1(0xe094024)		/* id: 0x0E094024, Name: "NWM_ETM", Handle: 94, [BC] */
	,MK_EXTID1(0xe094012)		/* id: 0x0E094012, Name: "NWM_DSM", Handle: 95, [BC] */
	,MK_EXTID1(0xe094011)		/* id: 0x0E094011, Name: "NWM_PDM", Handle: 96, [BC] */
	,MK_EXTID1(0xe094008)		/* id: 0x0E094008, Name: "NWM_DDM", Handle: 97, [BC] */
	,MK_EXTID1(0x1e7ca0fe)		/* id: 0x1E7CA0FE, Name: "APPL_ECU_BCM_1", Handle: 98, [FC] */
	,MK_STDID1(0x77e)		/* id: 0x77E, Name: "TRIP_A_B", Handle: 99, [FC] */
	,MK_EXTID1(0x18da40f1)		/* id: 0x18DA40F1, Name: "DIAGNOSTIC_REQUEST_BCM_1", Handle: 100, [FC] */
	,MK_STDID1(0x5e6)		/* id: 0x5E6, Name: "TGW_A1", Handle: 101, [FC] */
	,MK_STDID1(0x5e2)		/* id: 0x5E2, Name: "ICS_MSG", Handle: 102, [FC] */
	,MK_STDID1(0x5de)		/* id: 0x5DE, Name: "HVAC_A1", Handle: 103, [FC] */
	,MK_STDID1(0x5d2)		/* id: 0x5D2, Name: "DIRECT_INFO", Handle: 104, [FC] */
	,MK_STDID1(0x546)		/* id: 0x546, Name: "HVAC_A4", Handle: 105, [FC] */
	,MK_STDID1(0x49f)		/* id: 0x49F, Name: "STATUS_RRM", Handle: 106, [FC] */
	,MK_STDID1(0x49d)		/* id: 0x49D, Name: "STATUS_IPC", Handle: 107, [FC] */
	,MK_STDID1(0x49b)		/* id: 0x49B, Name: "STATUS_ECC", Handle: 108, [FC] */
	,MK_STDID1(0x495)		/* id: 0x495, Name: "HVAC_A2", Handle: 109, [FC] */
	,MK_STDID1(0x489)		/* id: 0x489, Name: "PLG_A1", Handle: 110, [FC] */
	,MK_STDID1(0x3cd)		/* id: 0x3CD, Name: "ECC_A1", Handle: 111, [FC] */
	,MK_EXTID1(0xe094034)		/* id: 0x0E094034, Name: "NWM_RBSS", Handle: 112, [FC] */
	,MK_EXTID1(0xe094033)		/* id: 0x0E094033, Name: "NWM_LBSS", Handle: 113, [FC] */
	,MK_EXTID1(0xe094032)		/* id: 0x0E094032, Name: "NWM_ICS", Handle: 114, [FC] */
	,MK_EXTID1(0xe094030)		/* id: 0x0E094030, Name: "NWM_CSWM", Handle: 115, [FC] */
	,MK_EXTID1(0xe09402f)		/* id: 0x0E09402F, Name: "NWM_CDM", Handle: 116, [FC] */
	,MK_EXTID1(0xe09402e)		/* id: 0x0E09402E, Name: "NWM_AMP", Handle: 117, [FC] */
	,MK_EXTID1(0xe09400a)		/* id: 0x0E09400A, Name: "NWM_ECC", Handle: 118, [FC] */
	,MK_EXTID1(0xe094003)		/* id: 0x0E094003, Name: "NWM_IPC", Handle: 119, [FC] */
	,MK_STDID1(0x190)		/* id: 0x190, Name: "CBC_I4", Handle: 120, [FC] */
	,MK_STDID1(0x8c)		/* id: 0x08C, Name: "CMCM_UNLK", Handle: 121, [FC] */
	,MK_STDID1(0x8a)		/* id: 0x08A, Name: "CFG_RQ", Handle: 122, [FC] */
};

V_MEMROM0 V_MEMROM1 canuint8 V_MEMROM2 CanRxDataLen[kCanNumberOfRxObjects] = {
	 3		/* id: 0x7D6, Name: "STATUS_C_OCM", Handle: 0, [BC] */
	,6		/* id: 0x1E11404E, Name: "CFG_DATA_CODE_RSP_OCM", Handle: 1, [BC] */
	,6		/* id: 0x1E11404C, Name: "CFG_DATA_CODE_RSP_AHLM", Handle: 2, [BC] */
	,6		/* id: 0x1E114043, Name: "CFG_DATA_CODE_RSP_DTCM", Handle: 3, [BC] */
	,6		/* id: 0x1E114041, Name: "CFG_DATA_CODE_RSP_RFHM", Handle: 4, [BC] */
	,6		/* id: 0x1E114039, Name: "CFG_DATA_CODE_RSP_DASM", Handle: 5, [BC] */
	,6		/* id: 0x1E114035, Name: "CFG_DATA_CODE_RSP_SCCM", Handle: 6, [BC] */
	,6		/* id: 0x1E11401E, Name: "CFG_DATA_CODE_RSP_HALF", Handle: 7, [BC] */
	,6		/* id: 0x1E11401A, Name: "CFG_DATA_CODE_RSP_ORC", Handle: 8, [BC] */
	,6		/* id: 0x1E114018, Name: "CFG_DATA_CODE_RSP_PAM", Handle: 9, [BC] */
	,6		/* id: 0x1E11400D, Name: "CFG_DATA_CODE_RSP_EPB", Handle: 10, [BC] */
	,6		/* id: 0x1E11400B, Name: "CFG_DATA_CODE_RSP_TRANSMISSION", Handle: 11, [BC] */
	,6		/* id: 0x1E114006, Name: "CFG_DATA_CODE_RSP_BSM", Handle: 12, [BC] */
	,6		/* id: 0x1E114002, Name: "CFG_DATA_CODE_RSP_EPS", Handle: 13, [BC] */
	,6		/* id: 0x1E114001, Name: "CFG_DATA_CODE_RSP_ECM", Handle: 14, [BC] */
	,2		/* id: 0x6D8, Name: "STATUS_C_DASM", Handle: 15, [BC] */
	,2		/* id: 0x6D6, Name: "STATUS_C_AHLM", Handle: 16, [BC] */
	,0		/* id: 0x18DBFEF1, Name: "DIAGNOSTIC_REQUEST_FUNCTIONAL", Handle: 17, [BC] */
	,8		/* id: 0x18DA40F1, Name: "DIAGNOSTIC_REQUEST_BCM_0", Handle: 18, [BC] */
	,8		/* id: 0x5E4, Name: "STATUS_C_HALF", Handle: 19, [BC] */
	,2		/* id: 0x5E2, Name: "STATUS_C_ESL", Handle: 20, [BC] */
	,7		/* id: 0x5E0, Name: "STATUS_C_EPB", Handle: 21, [BC] */
	,2		/* id: 0x5DA, Name: "HALF_C_Warning_RQ", Handle: 22, [BC] */
	,7		/* id: 0x4F0, Name: "StW_Actn_Rq_0", Handle: 23, [BC] */
	,8		/* id: 0x4EE, Name: "STATUS_C_TRANSMISSION", Handle: 24, [BC] */
	,8		/* id: 0x4EC, Name: "VIN_0", Handle: 25, [BC] */
	,8		/* id: 0x4D8, Name: "MOT3", Handle: 26, [BC] */
	,8		/* id: 0x4D0, Name: "ECM_1", Handle: 27, [BC] */
	,8		/* id: 0x4CC, Name: "STATUS_C_DTCM", Handle: 28, [BC] */
	,7		/* id: 0x3EE, Name: "DAS_A2", Handle: 29, [BC] */
	,5		/* id: 0x2F4, Name: "PAM_2", Handle: 30, [BC] */
	,3		/* id: 0x2EE, Name: "EPB_A1", Handle: 31, [BC] */
	,8		/* id: 0x2EC, Name: "DAS_A1", Handle: 32, [BC] */
	,8		/* id: 0x2E8, Name: "BSM_4", Handle: 33, [BC] */
	,8		/* id: 0x2E4, Name: "BSM_2", Handle: 34, [BC] */
	,8		/* id: 0x2E0, Name: "DTCM_A1", Handle: 35, [BC] */
	,4		/* id: 0x20A, Name: "SC", Handle: 36, [BC] */
	,7		/* id: 0x208, Name: "ORC_YRS_DATA", Handle: 37, [BC] */
	,5		/* id: 0x202, Name: "MOTGEAR2", Handle: 38, [BC] */
	,8		/* id: 0x1FC, Name: "MOT4", Handle: 39, [BC] */
	,4		/* id: 0x1EA, Name: "BSM_YRS_DATA", Handle: 40, [BC] */
	,8		/* id: 0x1E6, Name: "ASR4", Handle: 41, [BC] */
	,8		/* id: 0x1E4, Name: "ASR3", Handle: 42, [BC] */
	,1		/* id: 0x100, Name: "RFHUB_A4", Handle: 43, [BC] */
	,3		/* id: 0x0FA, Name: "IPC_CFG_Feature", Handle: 44, [BC] */
	,5		/* id: 0x0F8, Name: "ESL_CODE_RESPONSE", Handle: 45, [BC] */
	,7		/* id: 0x0F2, Name: "IMMO_CODE_REQUEST", Handle: 46, [BC] */
	,8		/* id: 0x000000F3, Name: "TRM_MKP_KEY", Handle: 47, [BC] */
	,2		/* id: 0x7D2, Name: "RFHUB_A3", Handle: 48, [FC] */
	,8		/* id: 0x1E7CAFE5, Name: "APPL_ECU_BCM_0", Handle: 49, [FC] */
	,4		/* id: 0x6DC, Name: "STATUS_C_SCCM", Handle: 50, [FC] */
	,4		/* id: 0x6DA, Name: "STATUS_C_RFHM", Handle: 51, [FC] */
	,4		/* id: 0x659, Name: "IPC_A1", Handle: 52, [FC] */
	,8		/* id: 0x5E6, Name: "STATUS_C_SPM", Handle: 53, [FC] */
	,8		/* id: 0x4EA, Name: "STATUS_C_IPC", Handle: 54, [FC] */
	,2		/* id: 0x4E8, Name: "STATUS_C_GSM", Handle: 55, [FC] */
	,2		/* id: 0x4E6, Name: "STATUS_C_EPS", Handle: 56, [FC] */
	,8		/* id: 0x4E4, Name: "STATUS_C_BSM", Handle: 57, [FC] */
	,8		/* id: 0x3EC, Name: "STATUS_C_ECM2", Handle: 58, [FC] */
	,8		/* id: 0x3EA, Name: "STATUS_C_ECM", Handle: 59, [FC] */
	,8		/* id: 0x36B, Name: "STATUS_C_ORC", Handle: 60, [FC] */
	,5		/* id: 0x2F8, Name: "RFHUB_A2", Handle: 61, [FC] */
	,1		/* id: 0x2F6, Name: "RFHUB_A1", Handle: 62, [FC] */
	,8		/* id: 0x2F2, Name: "PAM_1", Handle: 63, [FC] */
	,4		/* id: 0x0817A041, Name: "WAKE_C_RFHM", Handle: 64, [FC] */
	,4		/* id: 0x0817A035, Name: "WAKE_C_SCCM", Handle: 65, [FC] */
	,4		/* id: 0x0817A015, Name: "WAKE_C_ESL", Handle: 66, [FC] */
	,4		/* id: 0x0817A00D, Name: "WAKE_C_EPB", Handle: 67, [FC] */
	,4		/* id: 0x0817A006, Name: "WAKE_C_BSM", Handle: 68, [FC] */
	,8		/* id: 0x1F8, Name: "MOT1", Handle: 69, [FC] */
	,6		/* id: 0x1EE, Name: "GE", Handle: 70, [FC] */
	,6		/* id: 0x0FD, Name: "TRM_CODE_RESPONSE", Handle: 71, [FC] */
	,2		/* id: 0x0FC, Name: "ORC_A2", Handle: 72, [FC] */
	,8		/* id: 0x1E394000, Name: "CRO_BCM", Handle: 73, [BC] */
	,6		/* id: 0x1E114034, Name: "CFG_DATA_CODE_RSP_RBSS", Handle: 74, [BC] */
	,6		/* id: 0x1E114033, Name: "CFG_DATA_CODE_RSP_LBSS", Handle: 75, [BC] */
	,6		/* id: 0x1E114032, Name: "CFG_DATA_CODE_RSP_ICS", Handle: 76, [BC] */
	,6		/* id: 0x1E114030, Name: "CFG_DATA_CODE_RSP_CSWM", Handle: 77, [BC] */
	,6		/* id: 0x1E11402F, Name: "CFG_DATA_CODE_RSP_CDM", Handle: 78, [BC] */
	,6		/* id: 0x1E11402E, Name: "CFG_DATA_CODE_RSP_AMP", Handle: 79, [BC] */
	,6		/* id: 0x1E114024, Name: "CFG_DATA_CODE_RSP_ETM", Handle: 80, [BC] */
	,6		/* id: 0x1E114012, Name: "CFG_DATA_CODE_RSP_DSM", Handle: 81, [BC] */
	,6		/* id: 0x1E114011, Name: "CFG_DATA_CODE_RSP_PDM", Handle: 82, [BC] */
	,6		/* id: 0x1E11400A, Name: "CFG_DATA_CODE_RSP_ECC", Handle: 83, [BC] */
	,6		/* id: 0x1E114008, Name: "CFG_DATA_CODE_RSP_DDM", Handle: 84, [BC] */
	,6		/* id: 0x1E114003, Name: "CFG_DATA_CODE_RSP_IPC", Handle: 85, [BC] */
	,2		/* id: 0x77A, Name: "STATUS_ICS", Handle: 86, [BC] */
	,2		/* id: 0x778, Name: "STATUS_CDM", Handle: 87, [BC] */
	,2		/* id: 0x776, Name: "STATUS_AMP", Handle: 88, [BC] */
	,8		/* id: 0x6A4, Name: "ENVIRONMENTAL_CONDITIONS", Handle: 89, [BC] */
	,6		/* id: 0x5DA, Name: "FT_HVAC_STAT", Handle: 90, [BC] */
	,2		/* id: 0x5D0, Name: "DCM_P_MSG", Handle: 91, [BC] */
	,2		/* id: 0x5CE, Name: "DCM_D_MSG", Handle: 92, [BC] */
	,2		/* id: 0x0E094045, Name: "NWM_PLGM", Handle: 93, [BC] */
	,2		/* id: 0x0E094024, Name: "NWM_ETM", Handle: 94, [BC] */
	,2		/* id: 0x0E094012, Name: "NWM_DSM", Handle: 95, [BC] */
	,2		/* id: 0x0E094011, Name: "NWM_PDM", Handle: 96, [BC] */
	,2		/* id: 0x0E094008, Name: "NWM_DDM", Handle: 97, [BC] */
	,8		/* id: 0x1E7CA0FE, Name: "APPL_ECU_BCM_1", Handle: 98, [FC] */
	,8		/* id: 0x77E, Name: "TRIP_A_B", Handle: 99, [FC] */
	,8		/* id: 0x18DA40F1, Name: "DIAGNOSTIC_REQUEST_BCM_1", Handle: 100, [FC] */
	,4		/* id: 0x5E6, Name: "TGW_A1", Handle: 101, [FC] */
	,8		/* id: 0x5E2, Name: "ICS_MSG", Handle: 102, [FC] */
	,6		/* id: 0x5DE, Name: "HVAC_A1", Handle: 103, [FC] */
	,5		/* id: 0x5D2, Name: "DIRECT_INFO", Handle: 104, [FC] */
	,4		/* id: 0x546, Name: "HVAC_A4", Handle: 105, [FC] */
	,8		/* id: 0x49F, Name: "STATUS_RRM", Handle: 106, [FC] */
	,8		/* id: 0x49D, Name: "STATUS_IPC", Handle: 107, [FC] */
	,3		/* id: 0x49B, Name: "STATUS_ECC", Handle: 108, [FC] */
	,3		/* id: 0x495, Name: "HVAC_A2", Handle: 109, [FC] */
	,2		/* id: 0x489, Name: "PLG_A1", Handle: 110, [FC] */
	,2		/* id: 0x3CD, Name: "ECC_A1", Handle: 111, [FC] */
	,2		/* id: 0x0E094034, Name: "NWM_RBSS", Handle: 112, [FC] */
	,2		/* id: 0x0E094033, Name: "NWM_LBSS", Handle: 113, [FC] */
	,2		/* id: 0x0E094032, Name: "NWM_ICS", Handle: 114, [FC] */
	,2		/* id: 0x0E094030, Name: "NWM_CSWM", Handle: 115, [FC] */
	,2		/* id: 0x0E09402F, Name: "NWM_CDM", Handle: 116, [FC] */
	,2		/* id: 0x0E09402E, Name: "NWM_AMP", Handle: 117, [FC] */
	,2		/* id: 0x0E09400A, Name: "NWM_ECC", Handle: 118, [FC] */
	,2		/* id: 0x0E094003, Name: "NWM_IPC", Handle: 119, [FC] */
	,5		/* id: 0x190, Name: "CBC_I4", Handle: 120, [FC] */
	,3		/* id: 0x08C, Name: "CMCM_UNLK", Handle: 121, [FC] */
	,3		/* id: 0x08A, Name: "CFG_RQ", Handle: 122, [FC] */
};

V_MEMROM0 V_MEMROM1 RxDataPtr V_MEMROM2 CanRxDataPtr[kCanNumberOfRxObjects] = {
	 (RxDataPtr)STATUS_C_OCM._c		/* id: 0x7D6, Name: "STATUS_C_OCM", Handle: 0, [BC] */
	,(RxDataPtr)CFG_DATA_CODE_RSP_OCM._c		/* id: 0x1E11404E, Name: "CFG_DATA_CODE_RSP_OCM", Handle: 1, [BC] */
	,(RxDataPtr)CFG_DATA_CODE_RSP_AHLM._c		/* id: 0x1E11404C, Name: "CFG_DATA_CODE_RSP_AHLM", Handle: 2, [BC] */
	,(RxDataPtr)CFG_DATA_CODE_RSP_DTCM._c		/* id: 0x1E114043, Name: "CFG_DATA_CODE_RSP_DTCM", Handle: 3, [BC] */
	,(RxDataPtr)CFG_DATA_CODE_RSP_RFHM._c		/* id: 0x1E114041, Name: "CFG_DATA_CODE_RSP_RFHM", Handle: 4, [BC] */
	,(RxDataPtr)CFG_DATA_CODE_RSP_DASM._c		/* id: 0x1E114039, Name: "CFG_DATA_CODE_RSP_DASM", Handle: 5, [BC] */
	,(RxDataPtr)CFG_DATA_CODE_RSP_SCCM._c		/* id: 0x1E114035, Name: "CFG_DATA_CODE_RSP_SCCM", Handle: 6, [BC] */
	,(RxDataPtr)CFG_DATA_CODE_RSP_HALF._c		/* id: 0x1E11401E, Name: "CFG_DATA_CODE_RSP_HALF", Handle: 7, [BC] */
	,(RxDataPtr)CFG_DATA_CODE_RSP_ORC._c		/* id: 0x1E11401A, Name: "CFG_DATA_CODE_RSP_ORC", Handle: 8, [BC] */
	,(RxDataPtr)CFG_DATA_CODE_RSP_PAM._c		/* id: 0x1E114018, Name: "CFG_DATA_CODE_RSP_PAM", Handle: 9, [BC] */
	,(RxDataPtr)CFG_DATA_CODE_RSP_EPB._c		/* id: 0x1E11400D, Name: "CFG_DATA_CODE_RSP_EPB", Handle: 10, [BC] */
	,(RxDataPtr)CFG_DATA_CODE_RSP_TRANSMISSION._c		/* id: 0x1E11400B, Name: "CFG_DATA_CODE_RSP_TRANSMISSION", Handle: 11, [BC] */
	,(RxDataPtr)CFG_DATA_CODE_RSP_BSM._c		/* id: 0x1E114006, Name: "CFG_DATA_CODE_RSP_BSM", Handle: 12, [BC] */
	,(RxDataPtr)CFG_DATA_CODE_RSP_EPS._c		/* id: 0x1E114002, Name: "CFG_DATA_CODE_RSP_EPS", Handle: 13, [BC] */
	,(RxDataPtr)CFG_DATA_CODE_RSP_ECM._c		/* id: 0x1E114001, Name: "CFG_DATA_CODE_RSP_ECM", Handle: 14, [BC] */
	,(RxDataPtr)STATUS_C_DASM._c		/* id: 0x6D8, Name: "STATUS_C_DASM", Handle: 15, [BC] */
	,(RxDataPtr)STATUS_C_AHLM._c		/* id: 0x6D6, Name: "STATUS_C_AHLM", Handle: 16, [BC] */
	,(RxDataPtr)0			/* id: 0x18DBFEF1, Name: "DIAGNOSTIC_REQUEST_FUNCTIONAL", Handle: 17, [BC] */
	,(RxDataPtr)0			/* id: 0x18DA40F1, Name: "DIAGNOSTIC_REQUEST_BCM_0", Handle: 18, [BC] */
	,(RxDataPtr)STATUS_C_HALF._c		/* id: 0x5E4, Name: "STATUS_C_HALF", Handle: 19, [BC] */
	,(RxDataPtr)STATUS_C_ESL._c		/* id: 0x5E2, Name: "STATUS_C_ESL", Handle: 20, [BC] */
	,(RxDataPtr)STATUS_C_EPB._c		/* id: 0x5E0, Name: "STATUS_C_EPB", Handle: 21, [BC] */
	,(RxDataPtr)HALF_C_Warning_RQ._c		/* id: 0x5DA, Name: "HALF_C_Warning_RQ", Handle: 22, [BC] */
	,(RxDataPtr)StW_Actn_Rq_0._c		/* id: 0x4F0, Name: "StW_Actn_Rq_0", Handle: 23, [BC] */
	,(RxDataPtr)STATUS_C_TRANSMISSION._c		/* id: 0x4EE, Name: "STATUS_C_TRANSMISSION", Handle: 24, [BC] */
	,(RxDataPtr)VIN_0._c		/* id: 0x4EC, Name: "VIN_0", Handle: 25, [BC] */
	,(RxDataPtr)MOT3._c		/* id: 0x4D8, Name: "MOT3", Handle: 26, [BC] */
	,(RxDataPtr)ECM_1._c		/* id: 0x4D0, Name: "ECM_1", Handle: 27, [BC] */
	,(RxDataPtr)STATUS_C_DTCM._c		/* id: 0x4CC, Name: "STATUS_C_DTCM", Handle: 28, [BC] */
	,(RxDataPtr)DAS_A2._c		/* id: 0x3EE, Name: "DAS_A2", Handle: 29, [BC] */
	,(RxDataPtr)PAM_2._c		/* id: 0x2F4, Name: "PAM_2", Handle: 30, [BC] */
	,(RxDataPtr)EPB_A1._c		/* id: 0x2EE, Name: "EPB_A1", Handle: 31, [BC] */
	,(RxDataPtr)DAS_A1._c		/* id: 0x2EC, Name: "DAS_A1", Handle: 32, [BC] */
	,(RxDataPtr)BSM_4._c		/* id: 0x2E8, Name: "BSM_4", Handle: 33, [BC] */
	,(RxDataPtr)BSM_2._c		/* id: 0x2E4, Name: "BSM_2", Handle: 34, [BC] */
	,(RxDataPtr)DTCM_A1._c		/* id: 0x2E0, Name: "DTCM_A1", Handle: 35, [BC] */
	,(RxDataPtr)SC._c		/* id: 0x20A, Name: "SC", Handle: 36, [BC] */
	,(RxDataPtr)ORC_YRS_DATA._c		/* id: 0x208, Name: "ORC_YRS_DATA", Handle: 37, [BC] */
	,(RxDataPtr)MOTGEAR2._c		/* id: 0x202, Name: "MOTGEAR2", Handle: 38, [BC] */
	,(RxDataPtr)MOT4._c		/* id: 0x1FC, Name: "MOT4", Handle: 39, [BC] */
	,(RxDataPtr)BSM_YRS_DATA._c		/* id: 0x1EA, Name: "BSM_YRS_DATA", Handle: 40, [BC] */
	,(RxDataPtr)ASR4._c		/* id: 0x1E6, Name: "ASR4", Handle: 41, [BC] */
	,(RxDataPtr)ASR3._c		/* id: 0x1E4, Name: "ASR3", Handle: 42, [BC] */
	,(RxDataPtr)RFHUB_A4._c		/* id: 0x100, Name: "RFHUB_A4", Handle: 43, [BC] */
	,(RxDataPtr)IPC_CFG_Feature._c		/* id: 0x0FA, Name: "IPC_CFG_Feature", Handle: 44, [BC] */
	,(RxDataPtr)ESL_CODE_RESPONSE._c		/* id: 0x0F8, Name: "ESL_CODE_RESPONSE", Handle: 45, [BC] */
	,(RxDataPtr)IMMO_CODE_REQUEST._c		/* id: 0x0F2, Name: "IMMO_CODE_REQUEST", Handle: 46, [BC] */
	,(RxDataPtr)TRM_MKP_KEY._c		/* id: 0x000000F3, Name: "TRM_MKP_KEY", Handle: 47, [BC] */
	,(RxDataPtr)RFHUB_A3._c		/* id: 0x7D2, Name: "RFHUB_A3", Handle: 48, [FC] */
	,(RxDataPtr)APPL_ECU_BCM_0._c		/* id: 0x1E7CAFE5, Name: "APPL_ECU_BCM_0", Handle: 49, [FC] */
	,(RxDataPtr)STATUS_C_SCCM._c		/* id: 0x6DC, Name: "STATUS_C_SCCM", Handle: 50, [FC] */
	,(RxDataPtr)STATUS_C_RFHM._c		/* id: 0x6DA, Name: "STATUS_C_RFHM", Handle: 51, [FC] */
	,(RxDataPtr)IPC_A1._c		/* id: 0x659, Name: "IPC_A1", Handle: 52, [FC] */
	,(RxDataPtr)STATUS_C_SPM._c		/* id: 0x5E6, Name: "STATUS_C_SPM", Handle: 53, [FC] */
	,(RxDataPtr)STATUS_C_IPC._c		/* id: 0x4EA, Name: "STATUS_C_IPC", Handle: 54, [FC] */
	,(RxDataPtr)STATUS_C_GSM._c		/* id: 0x4E8, Name: "STATUS_C_GSM", Handle: 55, [FC] */
	,(RxDataPtr)STATUS_C_EPS._c		/* id: 0x4E6, Name: "STATUS_C_EPS", Handle: 56, [FC] */
	,(RxDataPtr)STATUS_C_BSM._c		/* id: 0x4E4, Name: "STATUS_C_BSM", Handle: 57, [FC] */
	,(RxDataPtr)STATUS_C_ECM2._c		/* id: 0x3EC, Name: "STATUS_C_ECM2", Handle: 58, [FC] */
	,(RxDataPtr)STATUS_C_ECM._c		/* id: 0x3EA, Name: "STATUS_C_ECM", Handle: 59, [FC] */
	,(RxDataPtr)STATUS_C_ORC._c		/* id: 0x36B, Name: "STATUS_C_ORC", Handle: 60, [FC] */
	,(RxDataPtr)RFHUB_A2._c		/* id: 0x2F8, Name: "RFHUB_A2", Handle: 61, [FC] */
	,(RxDataPtr)RFHUB_A1._c		/* id: 0x2F6, Name: "RFHUB_A1", Handle: 62, [FC] */
	,(RxDataPtr)PAM_1._c		/* id: 0x2F2, Name: "PAM_1", Handle: 63, [FC] */
	,(RxDataPtr)WAKE_C_RFHM._c		/* id: 0x0817A041, Name: "WAKE_C_RFHM", Handle: 64, [FC] */
	,(RxDataPtr)WAKE_C_SCCM._c		/* id: 0x0817A035, Name: "WAKE_C_SCCM", Handle: 65, [FC] */
	,(RxDataPtr)WAKE_C_ESL._c		/* id: 0x0817A015, Name: "WAKE_C_ESL", Handle: 66, [FC] */
	,(RxDataPtr)WAKE_C_EPB._c		/* id: 0x0817A00D, Name: "WAKE_C_EPB", Handle: 67, [FC] */
	,(RxDataPtr)WAKE_C_BSM._c		/* id: 0x0817A006, Name: "WAKE_C_BSM", Handle: 68, [FC] */
	,(RxDataPtr)MOT1._c		/* id: 0x1F8, Name: "MOT1", Handle: 69, [FC] */
	,(RxDataPtr)GE._c		/* id: 0x1EE, Name: "GE", Handle: 70, [FC] */
	,(RxDataPtr)TRM_CODE_RESPONSE._c		/* id: 0x0FD, Name: "TRM_CODE_RESPONSE", Handle: 71, [FC] */
	,(RxDataPtr)ORC_A2._c		/* id: 0x0FC, Name: "ORC_A2", Handle: 72, [FC] */
	,(RxDataPtr)CRO_BCM._c		/* id: 0x1E394000, Name: "CRO_BCM", Handle: 73, [BC] */
	,(RxDataPtr)CFG_DATA_CODE_RSP_RBSS._c		/* id: 0x1E114034, Name: "CFG_DATA_CODE_RSP_RBSS", Handle: 74, [BC] */
	,(RxDataPtr)CFG_DATA_CODE_RSP_LBSS._c		/* id: 0x1E114033, Name: "CFG_DATA_CODE_RSP_LBSS", Handle: 75, [BC] */
	,(RxDataPtr)CFG_DATA_CODE_RSP_ICS._c		/* id: 0x1E114032, Name: "CFG_DATA_CODE_RSP_ICS", Handle: 76, [BC] */
	,(RxDataPtr)CFG_DATA_CODE_RSP_CSWM._c		/* id: 0x1E114030, Name: "CFG_DATA_CODE_RSP_CSWM", Handle: 77, [BC] */
	,(RxDataPtr)CFG_DATA_CODE_RSP_CDM._c		/* id: 0x1E11402F, Name: "CFG_DATA_CODE_RSP_CDM", Handle: 78, [BC] */
	,(RxDataPtr)CFG_DATA_CODE_RSP_AMP._c		/* id: 0x1E11402E, Name: "CFG_DATA_CODE_RSP_AMP", Handle: 79, [BC] */
	,(RxDataPtr)CFG_DATA_CODE_RSP_ETM._c		/* id: 0x1E114024, Name: "CFG_DATA_CODE_RSP_ETM", Handle: 80, [BC] */
	,(RxDataPtr)CFG_DATA_CODE_RSP_DSM._c		/* id: 0x1E114012, Name: "CFG_DATA_CODE_RSP_DSM", Handle: 81, [BC] */
	,(RxDataPtr)CFG_DATA_CODE_RSP_PDM._c		/* id: 0x1E114011, Name: "CFG_DATA_CODE_RSP_PDM", Handle: 82, [BC] */
	,(RxDataPtr)CFG_DATA_CODE_RSP_ECC._c		/* id: 0x1E11400A, Name: "CFG_DATA_CODE_RSP_ECC", Handle: 83, [BC] */
	,(RxDataPtr)CFG_DATA_CODE_RSP_DDM._c		/* id: 0x1E114008, Name: "CFG_DATA_CODE_RSP_DDM", Handle: 84, [BC] */
	,(RxDataPtr)CFG_DATA_CODE_RSP_IPC._c		/* id: 0x1E114003, Name: "CFG_DATA_CODE_RSP_IPC", Handle: 85, [BC] */
	,(RxDataPtr)STATUS_ICS._c		/* id: 0x77A, Name: "STATUS_ICS", Handle: 86, [BC] */
	,(RxDataPtr)STATUS_CDM._c		/* id: 0x778, Name: "STATUS_CDM", Handle: 87, [BC] */
	,(RxDataPtr)STATUS_AMP._c		/* id: 0x776, Name: "STATUS_AMP", Handle: 88, [BC] */
	,(RxDataPtr)ENVIRONMENTAL_CONDITIONS._c		/* id: 0x6A4, Name: "ENVIRONMENTAL_CONDITIONS", Handle: 89, [BC] */
	,(RxDataPtr)FT_HVAC_STAT._c		/* id: 0x5DA, Name: "FT_HVAC_STAT", Handle: 90, [BC] */
	,(RxDataPtr)DCM_P_MSG._c		/* id: 0x5D0, Name: "DCM_P_MSG", Handle: 91, [BC] */
	,(RxDataPtr)DCM_D_MSG._c		/* id: 0x5CE, Name: "DCM_D_MSG", Handle: 92, [BC] */
	,(RxDataPtr)NWM_PLGM._c		/* id: 0x0E094045, Name: "NWM_PLGM", Handle: 93, [BC] */
	,(RxDataPtr)NWM_ETM._c		/* id: 0x0E094024, Name: "NWM_ETM", Handle: 94, [BC] */
	,(RxDataPtr)NWM_DSM._c		/* id: 0x0E094012, Name: "NWM_DSM", Handle: 95, [BC] */
	,(RxDataPtr)NWM_PDM._c		/* id: 0x0E094011, Name: "NWM_PDM", Handle: 96, [BC] */
	,(RxDataPtr)NWM_DDM._c		/* id: 0x0E094008, Name: "NWM_DDM", Handle: 97, [BC] */
	,(RxDataPtr)APPL_ECU_BCM_1._c		/* id: 0x1E7CA0FE, Name: "APPL_ECU_BCM_1", Handle: 98, [FC] */
	,(RxDataPtr)TRIP_A_B._c		/* id: 0x77E, Name: "TRIP_A_B", Handle: 99, [FC] */
	,(RxDataPtr)0			/* id: 0x18DA40F1, Name: "DIAGNOSTIC_REQUEST_BCM_1", Handle: 100, [FC] */
	,(RxDataPtr)TGW_A1._c		/* id: 0x5E6, Name: "TGW_A1", Handle: 101, [FC] */
	,(RxDataPtr)ICS_MSG._c		/* id: 0x5E2, Name: "ICS_MSG", Handle: 102, [FC] */
	,(RxDataPtr)HVAC_A1._c		/* id: 0x5DE, Name: "HVAC_A1", Handle: 103, [FC] */
	,(RxDataPtr)DIRECT_INFO._c		/* id: 0x5D2, Name: "DIRECT_INFO", Handle: 104, [FC] */
	,(RxDataPtr)HVAC_A4._c		/* id: 0x546, Name: "HVAC_A4", Handle: 105, [FC] */
	,(RxDataPtr)STATUS_RRM._c		/* id: 0x49F, Name: "STATUS_RRM", Handle: 106, [FC] */
	,(RxDataPtr)STATUS_IPC._c		/* id: 0x49D, Name: "STATUS_IPC", Handle: 107, [FC] */
	,(RxDataPtr)STATUS_ECC._c		/* id: 0x49B, Name: "STATUS_ECC", Handle: 108, [FC] */
	,(RxDataPtr)HVAC_A2._c		/* id: 0x495, Name: "HVAC_A2", Handle: 109, [FC] */
	,(RxDataPtr)PLG_A1._c		/* id: 0x489, Name: "PLG_A1", Handle: 110, [FC] */
	,(RxDataPtr)ECC_A1._c		/* id: 0x3CD, Name: "ECC_A1", Handle: 111, [FC] */
	,(RxDataPtr)NWM_RBSS._c		/* id: 0x0E094034, Name: "NWM_RBSS", Handle: 112, [FC] */
	,(RxDataPtr)NWM_LBSS._c		/* id: 0x0E094033, Name: "NWM_LBSS", Handle: 113, [FC] */
	,(RxDataPtr)NWM_ICS._c		/* id: 0x0E094032, Name: "NWM_ICS", Handle: 114, [FC] */
	,(RxDataPtr)NWM_CSWM._c		/* id: 0x0E094030, Name: "NWM_CSWM", Handle: 115, [FC] */
	,(RxDataPtr)NWM_CDM._c		/* id: 0x0E09402F, Name: "NWM_CDM", Handle: 116, [FC] */
	,(RxDataPtr)NWM_AMP._c		/* id: 0x0E09402E, Name: "NWM_AMP", Handle: 117, [FC] */
	,(RxDataPtr)NWM_ECC._c		/* id: 0x0E09400A, Name: "NWM_ECC", Handle: 118, [FC] */
	,(RxDataPtr)NWM_IPC._c		/* id: 0x0E094003, Name: "NWM_IPC", Handle: 119, [FC] */
	,(RxDataPtr)CBC_I4._c		/* id: 0x190, Name: "CBC_I4", Handle: 120, [FC] */
	,(RxDataPtr)CMCM_UNLK._c		/* id: 0x08C, Name: "CMCM_UNLK", Handle: 121, [FC] */
	,(RxDataPtr)CFG_RQ._c		/* id: 0x08A, Name: "CFG_RQ", Handle: 122, [FC] */
};

#ifdef C_ENABLE_PRECOPY_FCT

V_MEMROM0 V_MEMROM1 ApplPrecopyFct V_MEMROM2 CanRxApplPrecopyPtr[kCanNumberOfRxObjects] = {
	 NULL					/* id: 0x7D6, Name: "STATUS_C_OCM", Handle: 0, [BC] */
	,NULL					/* id: 0x1E11404E, Name: "CFG_DATA_CODE_RSP_OCM", Handle: 1, [BC] */
	,NULL					/* id: 0x1E11404C, Name: "CFG_DATA_CODE_RSP_AHLM", Handle: 2, [BC] */
	,NULL					/* id: 0x1E114043, Name: "CFG_DATA_CODE_RSP_DTCM", Handle: 3, [BC] */
	,NULL					/* id: 0x1E114041, Name: "CFG_DATA_CODE_RSP_RFHM", Handle: 4, [BC] */
	,NULL					/* id: 0x1E114039, Name: "CFG_DATA_CODE_RSP_DASM", Handle: 5, [BC] */
	,NULL					/* id: 0x1E114035, Name: "CFG_DATA_CODE_RSP_SCCM", Handle: 6, [BC] */
	,NULL					/* id: 0x1E11401E, Name: "CFG_DATA_CODE_RSP_HALF", Handle: 7, [BC] */
	,NULL					/* id: 0x1E11401A, Name: "CFG_DATA_CODE_RSP_ORC", Handle: 8, [BC] */
	,NULL					/* id: 0x1E114018, Name: "CFG_DATA_CODE_RSP_PAM", Handle: 9, [BC] */
	,NULL					/* id: 0x1E11400D, Name: "CFG_DATA_CODE_RSP_EPB", Handle: 10, [BC] */
	,NULL					/* id: 0x1E11400B, Name: "CFG_DATA_CODE_RSP_TRANSMISSION", Handle: 11, [BC] */
	,NULL					/* id: 0x1E114006, Name: "CFG_DATA_CODE_RSP_BSM", Handle: 12, [BC] */
	,NULL					/* id: 0x1E114002, Name: "CFG_DATA_CODE_RSP_EPS", Handle: 13, [BC] */
	,NULL					/* id: 0x1E114001, Name: "CFG_DATA_CODE_RSP_ECM", Handle: 14, [BC] */
	,NULL					/* id: 0x6D8, Name: "STATUS_C_DASM", Handle: 15, [BC] */
	,NULL					/* id: 0x6D6, Name: "STATUS_C_AHLM", Handle: 16, [BC] */
	,TpFuncPrecopy		/* id: 0x18DBFEF1, Name: "DIAGNOSTIC_REQUEST_FUNCTIONAL", Handle: 17, [BC] */
	,TpPrecopy		/* id: 0x18DA40F1, Name: "DIAGNOSTIC_REQUEST_BCM_0", Handle: 18, [BC] */
	,NULL					/* id: 0x5E4, Name: "STATUS_C_HALF", Handle: 19, [BC] */
	,NULL					/* id: 0x5E2, Name: "STATUS_C_ESL", Handle: 20, [BC] */
	,NULL					/* id: 0x5E0, Name: "STATUS_C_EPB", Handle: 21, [BC] */
	,NULL					/* id: 0x5DA, Name: "HALF_C_Warning_RQ", Handle: 22, [BC] */
	,NULL					/* id: 0x4F0, Name: "StW_Actn_Rq_0", Handle: 23, [BC] */
	,NULL					/* id: 0x4EE, Name: "STATUS_C_TRANSMISSION", Handle: 24, [BC] */
	,NULL					/* id: 0x4EC, Name: "VIN_0", Handle: 25, [BC] */
	,NULL					/* id: 0x4D8, Name: "MOT3", Handle: 26, [BC] */
	,NULL					/* id: 0x4D0, Name: "ECM_1", Handle: 27, [BC] */
	,NULL					/* id: 0x4CC, Name: "STATUS_C_DTCM", Handle: 28, [BC] */
	,NULL					/* id: 0x3EE, Name: "DAS_A2", Handle: 29, [BC] */
	,NULL					/* id: 0x2F4, Name: "PAM_2", Handle: 30, [BC] */
	,NULL					/* id: 0x2EE, Name: "EPB_A1", Handle: 31, [BC] */
	,NULL					/* id: 0x2EC, Name: "DAS_A1", Handle: 32, [BC] */
	,NULL					/* id: 0x2E8, Name: "BSM_4", Handle: 33, [BC] */
	,NULL					/* id: 0x2E4, Name: "BSM_2", Handle: 34, [BC] */
	,NULL					/* id: 0x2E0, Name: "DTCM_A1", Handle: 35, [BC] */
	,NULL					/* id: 0x20A, Name: "SC", Handle: 36, [BC] */
	,NULL					/* id: 0x208, Name: "ORC_YRS_DATA", Handle: 37, [BC] */
	,NULL					/* id: 0x202, Name: "MOTGEAR2", Handle: 38, [BC] */
	,NULL					/* id: 0x1FC, Name: "MOT4", Handle: 39, [BC] */
	,NULL					/* id: 0x1EA, Name: "BSM_YRS_DATA", Handle: 40, [BC] */
	,NULL					/* id: 0x1E6, Name: "ASR4", Handle: 41, [BC] */
	,NULL					/* id: 0x1E4, Name: "ASR3", Handle: 42, [BC] */
	,NULL					/* id: 0x100, Name: "RFHUB_A4", Handle: 43, [BC] */
	,NULL					/* id: 0x0FA, Name: "IPC_CFG_Feature", Handle: 44, [BC] */
	,NULL					/* id: 0x0F8, Name: "ESL_CODE_RESPONSE", Handle: 45, [BC] */
	,NULL					/* id: 0x0F2, Name: "IMMO_CODE_REQUEST", Handle: 46, [BC] */
	,NULL					/* id: 0x000000F3, Name: "TRM_MKP_KEY", Handle: 47, [BC] */
	,NULL					/* id: 0x7D2, Name: "RFHUB_A3", Handle: 48, [FC] */
	,NULL					/* id: 0x1E7CAFE5, Name: "APPL_ECU_BCM_0", Handle: 49, [FC] */
	,NULL					/* id: 0x6DC, Name: "STATUS_C_SCCM", Handle: 50, [FC] */
	,NULL					/* id: 0x6DA, Name: "STATUS_C_RFHM", Handle: 51, [FC] */
	,NULL					/* id: 0x659, Name: "IPC_A1", Handle: 52, [FC] */
	,NULL					/* id: 0x5E6, Name: "STATUS_C_SPM", Handle: 53, [FC] */
	,NULL					/* id: 0x4EA, Name: "STATUS_C_IPC", Handle: 54, [FC] */
	,NULL					/* id: 0x4E8, Name: "STATUS_C_GSM", Handle: 55, [FC] */
	,NULL					/* id: 0x4E6, Name: "STATUS_C_EPS", Handle: 56, [FC] */
	,NULL					/* id: 0x4E4, Name: "STATUS_C_BSM", Handle: 57, [FC] */
	,NULL					/* id: 0x3EC, Name: "STATUS_C_ECM2", Handle: 58, [FC] */
	,NULL					/* id: 0x3EA, Name: "STATUS_C_ECM", Handle: 59, [FC] */
	,NULL					/* id: 0x36B, Name: "STATUS_C_ORC", Handle: 60, [FC] */
	,NULL					/* id: 0x2F8, Name: "RFHUB_A2", Handle: 61, [FC] */
	,NULL					/* id: 0x2F6, Name: "RFHUB_A1", Handle: 62, [FC] */
	,NULL					/* id: 0x2F2, Name: "PAM_1", Handle: 63, [FC] */
	,NULL					/* id: 0x0817A041, Name: "WAKE_C_RFHM", Handle: 64, [FC] */
	,NULL					/* id: 0x0817A035, Name: "WAKE_C_SCCM", Handle: 65, [FC] */
	,NULL					/* id: 0x0817A015, Name: "WAKE_C_ESL", Handle: 66, [FC] */
	,NULL					/* id: 0x0817A00D, Name: "WAKE_C_EPB", Handle: 67, [FC] */
	,NULL					/* id: 0x0817A006, Name: "WAKE_C_BSM", Handle: 68, [FC] */
	,NULL					/* id: 0x1F8, Name: "MOT1", Handle: 69, [FC] */
	,NULL					/* id: 0x1EE, Name: "GE", Handle: 70, [FC] */
	,NULL					/* id: 0x0FD, Name: "TRM_CODE_RESPONSE", Handle: 71, [FC] */
	,NULL					/* id: 0x0FC, Name: "ORC_A2", Handle: 72, [FC] */
	,NULL					/* id: 0x1E394000, Name: "CRO_BCM", Handle: 73, [BC] */
	,NULL					/* id: 0x1E114034, Name: "CFG_DATA_CODE_RSP_RBSS", Handle: 74, [BC] */
	,NULL					/* id: 0x1E114033, Name: "CFG_DATA_CODE_RSP_LBSS", Handle: 75, [BC] */
	,NULL					/* id: 0x1E114032, Name: "CFG_DATA_CODE_RSP_ICS", Handle: 76, [BC] */
	,NULL					/* id: 0x1E114030, Name: "CFG_DATA_CODE_RSP_CSWM", Handle: 77, [BC] */
	,NULL					/* id: 0x1E11402F, Name: "CFG_DATA_CODE_RSP_CDM", Handle: 78, [BC] */
	,NULL					/* id: 0x1E11402E, Name: "CFG_DATA_CODE_RSP_AMP", Handle: 79, [BC] */
	,NULL					/* id: 0x1E114024, Name: "CFG_DATA_CODE_RSP_ETM", Handle: 80, [BC] */
	,NULL					/* id: 0x1E114012, Name: "CFG_DATA_CODE_RSP_DSM", Handle: 81, [BC] */
	,NULL					/* id: 0x1E114011, Name: "CFG_DATA_CODE_RSP_PDM", Handle: 82, [BC] */
	,NULL					/* id: 0x1E11400A, Name: "CFG_DATA_CODE_RSP_ECC", Handle: 83, [BC] */
	,NULL					/* id: 0x1E114008, Name: "CFG_DATA_CODE_RSP_DDM", Handle: 84, [BC] */
	,NULL					/* id: 0x1E114003, Name: "CFG_DATA_CODE_RSP_IPC", Handle: 85, [BC] */
	,NULL					/* id: 0x77A, Name: "STATUS_ICS", Handle: 86, [BC] */
	,NULL					/* id: 0x778, Name: "STATUS_CDM", Handle: 87, [BC] */
	,NULL					/* id: 0x776, Name: "STATUS_AMP", Handle: 88, [BC] */
	,NULL					/* id: 0x6A4, Name: "ENVIRONMENTAL_CONDITIONS", Handle: 89, [BC] */
	,NULL					/* id: 0x5DA, Name: "FT_HVAC_STAT", Handle: 90, [BC] */
	,NULL					/* id: 0x5D0, Name: "DCM_P_MSG", Handle: 91, [BC] */
	,NULL					/* id: 0x5CE, Name: "DCM_D_MSG", Handle: 92, [BC] */
	,NULL					/* id: 0x0E094045, Name: "NWM_PLGM", Handle: 93, [BC] */
	,NULL					/* id: 0x0E094024, Name: "NWM_ETM", Handle: 94, [BC] */
	,NULL					/* id: 0x0E094012, Name: "NWM_DSM", Handle: 95, [BC] */
	,NULL					/* id: 0x0E094011, Name: "NWM_PDM", Handle: 96, [BC] */
	,NULL					/* id: 0x0E094008, Name: "NWM_DDM", Handle: 97, [BC] */
	,NULL					/* id: 0x1E7CA0FE, Name: "APPL_ECU_BCM_1", Handle: 98, [FC] */
	,NULL					/* id: 0x77E, Name: "TRIP_A_B", Handle: 99, [FC] */
	,TpPrecopy		/* id: 0x18DA40F1, Name: "DIAGNOSTIC_REQUEST_BCM_1", Handle: 100, [FC] */
	,NULL					/* id: 0x5E6, Name: "TGW_A1", Handle: 101, [FC] */
	,NULL					/* id: 0x5E2, Name: "ICS_MSG", Handle: 102, [FC] */
	,NULL					/* id: 0x5DE, Name: "HVAC_A1", Handle: 103, [FC] */
	,NULL					/* id: 0x5D2, Name: "DIRECT_INFO", Handle: 104, [FC] */
	,NULL					/* id: 0x546, Name: "HVAC_A4", Handle: 105, [FC] */
	,NULL					/* id: 0x49F, Name: "STATUS_RRM", Handle: 106, [FC] */
	,NULL					/* id: 0x49D, Name: "STATUS_IPC", Handle: 107, [FC] */
	,NULL					/* id: 0x49B, Name: "STATUS_ECC", Handle: 108, [FC] */
	,NULL					/* id: 0x495, Name: "HVAC_A2", Handle: 109, [FC] */
	,NULL					/* id: 0x489, Name: "PLG_A1", Handle: 110, [FC] */
	,NULL					/* id: 0x3CD, Name: "ECC_A1", Handle: 111, [FC] */
	,NULL					/* id: 0x0E094034, Name: "NWM_RBSS", Handle: 112, [FC] */
	,NULL					/* id: 0x0E094033, Name: "NWM_LBSS", Handle: 113, [FC] */
	,NULL					/* id: 0x0E094032, Name: "NWM_ICS", Handle: 114, [FC] */
	,NULL					/* id: 0x0E094030, Name: "NWM_CSWM", Handle: 115, [FC] */
	,NULL					/* id: 0x0E09402F, Name: "NWM_CDM", Handle: 116, [FC] */
	,NULL					/* id: 0x0E09402E, Name: "NWM_AMP", Handle: 117, [FC] */
	,NULL					/* id: 0x0E09400A, Name: "NWM_ECC", Handle: 118, [FC] */
	,NULL					/* id: 0x0E094003, Name: "NWM_IPC", Handle: 119, [FC] */
	,NULL					/* id: 0x190, Name: "CBC_I4", Handle: 120, [FC] */
	,NULL					/* id: 0x08C, Name: "CMCM_UNLK", Handle: 121, [FC] */
	,NULL					/* id: 0x08A, Name: "CFG_RQ", Handle: 122, [FC] */
};
#endif


#ifdef C_ENABLE_INDICATION_FCT

V_MEMROM0 V_MEMROM1 ApplIndicationFct V_MEMROM2 CanRxApplIndicationPtr[kCanNumberOfRxObjects] = {
	 NETC_NetworkMsgIndication		/* id: 0x7D6, Name: "STATUS_C_OCM", Handle: 0, [BC] */
	,NETC_NetworkMsgIndication		/* id: 0x1E11404E, Name: "CFG_DATA_CODE_RSP_OCM", Handle: 1, [BC] */
	,NETC_NetworkMsgIndication		/* id: 0x1E11404C, Name: "CFG_DATA_CODE_RSP_AHLM", Handle: 2, [BC] */
	,NETC_NetworkMsgIndication		/* id: 0x1E114043, Name: "CFG_DATA_CODE_RSP_DTCM", Handle: 3, [BC] */
	,NETC_NetworkMsgIndication		/* id: 0x1E114041, Name: "CFG_DATA_CODE_RSP_RFHM", Handle: 4, [BC] */
	,NETC_NetworkMsgIndication		/* id: 0x1E114039, Name: "CFG_DATA_CODE_RSP_DASM", Handle: 5, [BC] */
	,NULL					/* id: 0x1E114035, Name: "CFG_DATA_CODE_RSP_SCCM", Handle: 6, [BC] */
	,NETC_NetworkMsgIndication		/* id: 0x1E11401E, Name: "CFG_DATA_CODE_RSP_HALF", Handle: 7, [BC] */
	,NETC_NetworkMsgIndication		/* id: 0x1E11401A, Name: "CFG_DATA_CODE_RSP_ORC", Handle: 8, [BC] */
	,NETC_NetworkMsgIndication		/* id: 0x1E114018, Name: "CFG_DATA_CODE_RSP_PAM", Handle: 9, [BC] */
	,NETC_NetworkMsgIndication		/* id: 0x1E11400D, Name: "CFG_DATA_CODE_RSP_EPB", Handle: 10, [BC] */
	,NETC_NetworkMsgIndication		/* id: 0x1E11400B, Name: "CFG_DATA_CODE_RSP_TRANSMISSION", Handle: 11, [BC] */
	,NETC_NetworkMsgIndication		/* id: 0x1E114006, Name: "CFG_DATA_CODE_RSP_BSM", Handle: 12, [BC] */
	,NETC_NetworkMsgIndication		/* id: 0x1E114002, Name: "CFG_DATA_CODE_RSP_EPS", Handle: 13, [BC] */
	,NETC_NetworkMsgIndication		/* id: 0x1E114001, Name: "CFG_DATA_CODE_RSP_ECM", Handle: 14, [BC] */
	,NETC_NetworkMsgIndication		/* id: 0x6D8, Name: "STATUS_C_DASM", Handle: 15, [BC] */
	,NETC_NetworkMsgIndication		/* id: 0x6D6, Name: "STATUS_C_AHLM", Handle: 16, [BC] */
	,NULL					/* id: 0x18DBFEF1, Name: "DIAGNOSTIC_REQUEST_FUNCTIONAL", Handle: 17, [BC] */
	,NULL					/* id: 0x18DA40F1, Name: "DIAGNOSTIC_REQUEST_BCM_0", Handle: 18, [BC] */
	,NETC_NetworkMsgIndication		/* id: 0x5E4, Name: "STATUS_C_HALF", Handle: 19, [BC] */
	,NETC_NetworkMsgIndication		/* id: 0x5E2, Name: "STATUS_C_ESL", Handle: 20, [BC] */
	,NETC_NetworkMsgIndication		/* id: 0x5E0, Name: "STATUS_C_EPB", Handle: 21, [BC] */
	,NETC_NetworkMsgIndication		/* id: 0x5DA, Name: "HALF_C_Warning_RQ", Handle: 22, [BC] */
	,NETC_NetworkMsgIndication		/* id: 0x4F0, Name: "StW_Actn_Rq_0", Handle: 23, [BC] */
	,NETC_NetworkMsgIndication		/* id: 0x4EE, Name: "STATUS_C_TRANSMISSION", Handle: 24, [BC] */
	,NETC_NetworkMsgIndication		/* id: 0x4EC, Name: "VIN_0", Handle: 25, [BC] */
	,NETC_NetworkMsgIndication		/* id: 0x4D8, Name: "MOT3", Handle: 26, [BC] */
	,NETC_NetworkMsgIndication		/* id: 0x4D0, Name: "ECM_1", Handle: 27, [BC] */
	,NETC_NetworkMsgIndication		/* id: 0x4CC, Name: "STATUS_C_DTCM", Handle: 28, [BC] */
	,NETC_NetworkMsgIndication		/* id: 0x3EE, Name: "DAS_A2", Handle: 29, [BC] */
	,NETC_NetworkMsgIndication		/* id: 0x2F4, Name: "PAM_2", Handle: 30, [BC] */
	,NETC_NetworkMsgIndication		/* id: 0x2EE, Name: "EPB_A1", Handle: 31, [BC] */
	,NETC_NetworkMsgIndication		/* id: 0x2EC, Name: "DAS_A1", Handle: 32, [BC] */
	,NETC_NetworkMsgIndication		/* id: 0x2E8, Name: "BSM_4", Handle: 33, [BC] */
	,NETC_NetworkMsgIndication		/* id: 0x2E4, Name: "BSM_2", Handle: 34, [BC] */
	,NETC_NetworkMsgIndication		/* id: 0x2E0, Name: "DTCM_A1", Handle: 35, [BC] */
	,NETC_NetworkMsgIndication		/* id: 0x20A, Name: "SC", Handle: 36, [BC] */
	,NETC_NetworkMsgIndication		/* id: 0x208, Name: "ORC_YRS_DATA", Handle: 37, [BC] */
	,NETC_NetworkMsgIndication		/* id: 0x202, Name: "MOTGEAR2", Handle: 38, [BC] */
	,NETC_NetworkMsgIndication		/* id: 0x1FC, Name: "MOT4", Handle: 39, [BC] */
	,NETC_NetworkMsgIndication		/* id: 0x1EA, Name: "BSM_YRS_DATA", Handle: 40, [BC] */
	,NETC_NetworkMsgIndication		/* id: 0x1E6, Name: "ASR4", Handle: 41, [BC] */
	,NETC_NetworkMsgIndication		/* id: 0x1E4, Name: "ASR3", Handle: 42, [BC] */
	,NETC_NetworkMsgIndication		/* id: 0x100, Name: "RFHUB_A4", Handle: 43, [BC] */
	,NETC_NetworkMsgIndication		/* id: 0x0FA, Name: "IPC_CFG_Feature", Handle: 44, [BC] */
	,NETC_NetworkMsgIndication		/* id: 0x0F8, Name: "ESL_CODE_RESPONSE", Handle: 45, [BC] */
	,NETC_NetworkMsgIndication		/* id: 0x0F2, Name: "IMMO_CODE_REQUEST", Handle: 46, [BC] */
	,NETC_NetworkMsgIndication		/* id: 0x000000F3, Name: "TRM_MKP_KEY", Handle: 47, [BC] */
	,NETC_NetworkMsgIndication		/* id: 0x7D2, Name: "RFHUB_A3", Handle: 48, [FC] */
	,NETC_NetworkMsgIndication		/* id: 0x1E7CAFE5, Name: "APPL_ECU_BCM_0", Handle: 49, [FC] */
	,NETC_NetworkMsgIndication		/* id: 0x6DC, Name: "STATUS_C_SCCM", Handle: 50, [FC] */
	,NETC_NetworkMsgIndication		/* id: 0x6DA, Name: "STATUS_C_RFHM", Handle: 51, [FC] */
	,NETC_NetworkMsgIndication		/* id: 0x659, Name: "IPC_A1", Handle: 52, [FC] */
	,NETC_NetworkMsgIndication		/* id: 0x5E6, Name: "STATUS_C_SPM", Handle: 53, [FC] */
	,NETC_NetworkMsgIndication		/* id: 0x4EA, Name: "STATUS_C_IPC", Handle: 54, [FC] */
	,NETC_NetworkMsgIndication		/* id: 0x4E8, Name: "STATUS_C_GSM", Handle: 55, [FC] */
	,NETC_NetworkMsgIndication		/* id: 0x4E6, Name: "STATUS_C_EPS", Handle: 56, [FC] */
	,NETC_NetworkMsgIndication		/* id: 0x4E4, Name: "STATUS_C_BSM", Handle: 57, [FC] */
	,NETC_NetworkMsgIndication		/* id: 0x3EC, Name: "STATUS_C_ECM2", Handle: 58, [FC] */
	,NETC_NetworkMsgIndication		/* id: 0x3EA, Name: "STATUS_C_ECM", Handle: 59, [FC] */
	,NETC_NetworkMsgIndication		/* id: 0x36B, Name: "STATUS_C_ORC", Handle: 60, [FC] */
	,NETC_NetworkMsgIndication		/* id: 0x2F8, Name: "RFHUB_A2", Handle: 61, [FC] */
	,NETC_NetworkMsgIndication		/* id: 0x2F6, Name: "RFHUB_A1", Handle: 62, [FC] */
	,NETC_NetworkMsgIndication		/* id: 0x2F2, Name: "PAM_1", Handle: 63, [FC] */
	,NETC_NetworkMsgIndication		/* id: 0x0817A041, Name: "WAKE_C_RFHM", Handle: 64, [FC] */
	,NETC_NetworkMsgIndication		/* id: 0x0817A035, Name: "WAKE_C_SCCM", Handle: 65, [FC] */
	,NETC_NetworkMsgIndication		/* id: 0x0817A015, Name: "WAKE_C_ESL", Handle: 66, [FC] */
	,NETC_NetworkMsgIndication		/* id: 0x0817A00D, Name: "WAKE_C_EPB", Handle: 67, [FC] */
	,NETC_NetworkMsgIndication		/* id: 0x0817A006, Name: "WAKE_C_BSM", Handle: 68, [FC] */
	,NETC_NetworkMsgIndication		/* id: 0x1F8, Name: "MOT1", Handle: 69, [FC] */
	,NETC_NetworkMsgIndication		/* id: 0x1EE, Name: "GE", Handle: 70, [FC] */
	,NETC_NetworkMsgIndication		/* id: 0x0FD, Name: "TRM_CODE_RESPONSE", Handle: 71, [FC] */
	,NETC_NetworkMsgIndication		/* id: 0x0FC, Name: "ORC_A2", Handle: 72, [FC] */
	,CCP_CRO_Indication		/* id: 0x1E394000, Name: "CRO_BCM", Handle: 73, [BC] */
	,NETC_NetworkMsgIndication		/* id: 0x1E114034, Name: "CFG_DATA_CODE_RSP_RBSS", Handle: 74, [BC] */
	,NETC_NetworkMsgIndication		/* id: 0x1E114033, Name: "CFG_DATA_CODE_RSP_LBSS", Handle: 75, [BC] */
	,NETC_NetworkMsgIndication		/* id: 0x1E114032, Name: "CFG_DATA_CODE_RSP_ICS", Handle: 76, [BC] */
	,NETC_NetworkMsgIndication		/* id: 0x1E114030, Name: "CFG_DATA_CODE_RSP_CSWM", Handle: 77, [BC] */
	,NETC_NetworkMsgIndication		/* id: 0x1E11402F, Name: "CFG_DATA_CODE_RSP_CDM", Handle: 78, [BC] */
	,NETC_NetworkMsgIndication		/* id: 0x1E11402E, Name: "CFG_DATA_CODE_RSP_AMP", Handle: 79, [BC] */
	,NETC_NetworkMsgIndication		/* id: 0x1E114024, Name: "CFG_DATA_CODE_RSP_ETM", Handle: 80, [BC] */
	,NETC_NetworkMsgIndication		/* id: 0x1E114012, Name: "CFG_DATA_CODE_RSP_DSM", Handle: 81, [BC] */
	,NETC_NetworkMsgIndication		/* id: 0x1E114011, Name: "CFG_DATA_CODE_RSP_PDM", Handle: 82, [BC] */
	,NETC_NetworkMsgIndication		/* id: 0x1E11400A, Name: "CFG_DATA_CODE_RSP_ECC", Handle: 83, [BC] */
	,NETC_NetworkMsgIndication		/* id: 0x1E114008, Name: "CFG_DATA_CODE_RSP_DDM", Handle: 84, [BC] */
	,NETC_NetworkMsgIndication		/* id: 0x1E114003, Name: "CFG_DATA_CODE_RSP_IPC", Handle: 85, [BC] */
	,NETC_NetworkMsgIndication		/* id: 0x77A, Name: "STATUS_ICS", Handle: 86, [BC] */
	,NETC_NetworkMsgIndication		/* id: 0x778, Name: "STATUS_CDM", Handle: 87, [BC] */
	,NETC_NetworkMsgIndication		/* id: 0x776, Name: "STATUS_AMP", Handle: 88, [BC] */
	,NETC_NetworkMsgIndication		/* id: 0x6A4, Name: "ENVIRONMENTAL_CONDITIONS", Handle: 89, [BC] */
	,NETC_NetworkMsgIndication		/* id: 0x5DA, Name: "FT_HVAC_STAT", Handle: 90, [BC] */
	,NETC_NetworkMsgIndication		/* id: 0x5D0, Name: "DCM_P_MSG", Handle: 91, [BC] */
	,NETC_NetworkMsgIndication		/* id: 0x5CE, Name: "DCM_D_MSG", Handle: 92, [BC] */
	,NETC_NetworkMsgIndication		/* id: 0x0E094045, Name: "NWM_PLGM", Handle: 93, [BC] */
	,NETC_NetworkMsgIndication		/* id: 0x0E094024, Name: "NWM_ETM", Handle: 94, [BC] */
	,NETC_NetworkMsgIndication		/* id: 0x0E094012, Name: "NWM_DSM", Handle: 95, [BC] */
	,NETC_NetworkMsgIndication		/* id: 0x0E094011, Name: "NWM_PDM", Handle: 96, [BC] */
	,NETC_NetworkMsgIndication		/* id: 0x0E094008, Name: "NWM_DDM", Handle: 97, [BC] */
	,NETC_NetworkMsgIndication		/* id: 0x1E7CA0FE, Name: "APPL_ECU_BCM_1", Handle: 98, [FC] */
	,NETC_NetworkMsgIndication		/* id: 0x77E, Name: "TRIP_A_B", Handle: 99, [FC] */
	,NULL					/* id: 0x18DA40F1, Name: "DIAGNOSTIC_REQUEST_BCM_1", Handle: 100, [FC] */
	,NETC_NetworkMsgIndication		/* id: 0x5E6, Name: "TGW_A1", Handle: 101, [FC] */
	,NETC_NetworkMsgIndication		/* id: 0x5E2, Name: "ICS_MSG", Handle: 102, [FC] */
	,NETC_NetworkMsgIndication		/* id: 0x5DE, Name: "HVAC_A1", Handle: 103, [FC] */
	,NETC_NetworkMsgIndication		/* id: 0x5D2, Name: "DIRECT_INFO", Handle: 104, [FC] */
	,NETC_NetworkMsgIndication		/* id: 0x546, Name: "HVAC_A4", Handle: 105, [FC] */
	,NETC_NetworkMsgIndication		/* id: 0x49F, Name: "STATUS_RRM", Handle: 106, [FC] */
	,NETC_NetworkMsgIndication		/* id: 0x49D, Name: "STATUS_IPC", Handle: 107, [FC] */
	,NETC_NetworkMsgIndication		/* id: 0x49B, Name: "STATUS_ECC", Handle: 108, [FC] */
	,NETC_NetworkMsgIndication		/* id: 0x495, Name: "HVAC_A2", Handle: 109, [FC] */
	,NETC_NetworkMsgIndication		/* id: 0x489, Name: "PLG_A1", Handle: 110, [FC] */
	,NETC_NetworkMsgIndication		/* id: 0x3CD, Name: "ECC_A1", Handle: 111, [FC] */
	,NETC_NetworkMsgIndication		/* id: 0x0E094034, Name: "NWM_RBSS", Handle: 112, [FC] */
	,NETC_NetworkMsgIndication		/* id: 0x0E094033, Name: "NWM_LBSS", Handle: 113, [FC] */
	,NETC_NetworkMsgIndication		/* id: 0x0E094032, Name: "NWM_ICS", Handle: 114, [FC] */
	,NETC_NetworkMsgIndication		/* id: 0x0E094030, Name: "NWM_CSWM", Handle: 115, [FC] */
	,NETC_NetworkMsgIndication		/* id: 0x0E09402F, Name: "NWM_CDM", Handle: 116, [FC] */
	,NETC_NetworkMsgIndication		/* id: 0x0E09402E, Name: "NWM_AMP", Handle: 117, [FC] */
	,NETC_NetworkMsgIndication		/* id: 0x0E09400A, Name: "NWM_ECC", Handle: 118, [FC] */
	,NETC_NetworkMsgIndication		/* id: 0x0E094003, Name: "NWM_IPC", Handle: 119, [FC] */
	,NETC_NetworkMsgIndication		/* id: 0x190, Name: "CBC_I4", Handle: 120, [FC] */
	,NETC_NetworkMsgIndication		/* id: 0x08C, Name: "CMCM_UNLK", Handle: 121, [FC] */
	,NETC_NetworkMsgIndication		/* id: 0x08A, Name: "CFG_RQ", Handle: 122, [FC] */
};
#endif



/*************************************************************/
/* Databuffer for indicationflags                                */
/*************************************************************/
V_MEMROM0 V_MEMROM1 canuint8 V_MEMROM2 CanIndicationOffset[kCanNumberOfRxObjects] = {
	   0	,  0	,  0	,  0	,  0	,  0	,  0	,  0
	,  1	,  1	,  1	,  1	,  1	,  1	,  1	,  1
	,  2	,  0	,  2	,  2	,  2	,  2	,  2	,  2
	,  2	,  3	,  3	,  3	,  3	,  3	,  3	,  3
	,  3	,  4	,  4	,  4	,  4	,  4	,  4	,  4
	,  4	,  5	,  5	,  5	,  5	,  5	,  5	,  5
	,  5	,  6	,  6	,  6	,  6	,  6	,  6	,  6
	,  6	,  7	,  7	,  7	,  7	,  7	,  7	,  7
	,  7	,  8	,  8	,  8	,  8	,  8	,  8	,  8
	,  8	,  9	,  9	,  9	,  9	,  9	,  9	,  9
	,  9	, 10	, 10	, 10	, 10	, 10	, 10	, 10
	, 10	, 11	, 11	, 11	, 11	, 11	, 11	, 11
	, 11	, 12	, 12	, 12	,  0	, 12	, 12	, 12
	, 12	, 12	, 13	, 13	, 13	, 13	, 13	, 13
	, 13	, 13	, 14	, 14	, 14	, 14	, 14	, 14
	, 14	, 14	, 15
};

V_MEMROM0 V_MEMROM1 canuint8 V_MEMROM2 CanIndicationMask[kCanNumberOfRxObjects] = {
	 0x01	,0x02	,0x04	,0x08	,0x10	,0x20	,0x40	,0x80
	,0x01	,0x02	,0x04	,0x08	,0x10	,0x20	,0x40	,0x80
	,0x01	,0x00	,0x02	,0x04	,0x08	,0x10	,0x20	,0x40
	,0x80	,0x01	,0x02	,0x04	,0x08	,0x10	,0x20	,0x40
	,0x80	,0x01	,0x02	,0x04	,0x08	,0x10	,0x20	,0x40
	,0x80	,0x01	,0x02	,0x04	,0x08	,0x10	,0x20	,0x40
	,0x80	,0x01	,0x02	,0x04	,0x08	,0x10	,0x20	,0x40
	,0x80	,0x01	,0x02	,0x04	,0x08	,0x10	,0x20	,0x40
	,0x80	,0x01	,0x02	,0x04	,0x08	,0x10	,0x20	,0x40
	,0x80	,0x01	,0x02	,0x04	,0x08	,0x10	,0x20	,0x40
	,0x80	,0x01	,0x02	,0x04	,0x08	,0x10	,0x20	,0x40
	,0x80	,0x01	,0x02	,0x04	,0x08	,0x10	,0x20	,0x40
	,0x80	,0x01	,0x02	,0x04	,0x00	,0x08	,0x10	,0x20
	,0x40	,0x80	,0x01	,0x02	,0x04	,0x08	,0x10	,0x20
	,0x40	,0x80	,0x01	,0x02	,0x04	,0x08	,0x10	,0x20
	,0x40	,0x80	,0x01
};


V_MEMROM0 V_MEMROM1 CanTransmitHandle V_MEMROM2 CanTxStartIndex[3] =  {
	 0
	,32
	,75
};


V_MEMROM0 V_MEMROM1 CanReceiveHandle V_MEMROM2 CanRxStartIndex[3] =  {
	 0
	,73
	,123
};


V_MEMROM0 V_MEMROM1 tCanChannelObject V_MEMROM2 CanChannelObject[kCanNumberOfChannels] = {
	{
		 ApplCanWakeUp
		,ApplCanMsgReceived
		,
		{
			 CAN_RANGE_FCT_DUMMY
			,TpPrecopy
			,TpFuncPrecopy
			,CAN_RANGE_FCT_DUMMY
		}
		,ApplCanBusOff
		,ApplCanTxHandleDummy
		,ApplCanChannelDummy
		,
		{
			 MK_RX_RANGE_MASK(0x0)
			,MK_RX_RANGE_MASK(0xff)
			,MK_RX_RANGE_MASK(0xff)
			,MK_RX_RANGE_MASK(0x0)
		}
		,
		{
			 MK_RX_RANGE_CODE(0x0)
			,MK_RX_RANGE_CODE(0x18da4000)
			,MK_RX_RANGE_CODE(0x18dbfe00)
			,MK_RX_RANGE_CODE(0x0)
		}
		,0x6
	}
	,{
		 ApplCanWakeUp
		,ApplCanMsgReceived
		,
		{
			 CAN_RANGE_FCT_DUMMY
			,CAN_RANGE_FCT_DUMMY
			,CAN_RANGE_FCT_DUMMY
			,CAN_RANGE_FCT_DUMMY
		}
		,ApplCanBusOff
		,ApplCanTxHandleDummy
		,ApplCanChannelDummy
		,
		{
			 MK_RX_RANGE_MASK(0x0)
			,MK_RX_RANGE_MASK(0x0)
			,MK_RX_RANGE_MASK(0x0)
			,MK_RX_RANGE_MASK(0x0)
		}
		,
		{
			 MK_RX_RANGE_CODE(0x0)
			,MK_RX_RANGE_CODE(0x0)
			,MK_RX_RANGE_CODE(0x0)
			,MK_RX_RANGE_CODE(0x0)
		}
		,0x0
	}
};

V_MEMROM0 V_MEMROM1 CanChannelHandle V_MEMROM2 CanTxMsgHandleToChannel[kCanNumberOfTxObjects] =  {
	 0		/* id: 0x7D4, Name: "STATUS_C_BCM2", Handle: 0, [BC] */
	,0		/* id: 0x7D0, Name: "HVAC_INFO", Handle: 1, [BC] */
	,0		/* id: 0x7CC, Name: "CBC_PT4", Handle: 2, [BC] */
	,0		/* id: 0x7CA, Name: "BCM_KEYON_COUNTER_C_CAN", Handle: 3, [BC] */
	,0		/* id: 0x7C8, Name: "IBS4", Handle: 4, [BC] */
	,0		/* id: 0x1E7CAFF6, Name: "ECU_APPL_BCM_0", Handle: 5, [BC] */
	,0		/* id: 0x1E114000, Name: "CONFIGURATION_DATA_CODE_REQUEST_0", Handle: 6, [BC] */
	,0		/* id: 0x6D2, Name: "CBC_VTA_0", Handle: 7, [BC] */
	,0		/* id: 0x18DAF140, Name: "DIAGNOSTIC_RESPONSE_BCM_0", Handle: 8, [BC] */
	,0		/* id: 0x5DE, Name: "STATUS_B_CAN2", Handle: 9, [BC] */
	,0		/* id: 0x5D8, Name: "EXTERNAL_LIGHTS_0", Handle: 10, [BC] */
	,0		/* id: 0x5D6, Name: "ASBM1_CONTROL", Handle: 11, [BC] */
	,0		/* id: 0x5D4, Name: "HVAC_INFO2", Handle: 12, [BC] */
	,0		/* id: 0x55F, Name: "IBS2", Handle: 13, [BC] */
	,0		/* id: 0x4F4, Name: "VEHICLE_SPEED_ODOMETER_0", Handle: 14, [BC] */
	,0		/* id: 0x4E2, Name: "STATUS_C_BCM", Handle: 15, [BC] */
	,0		/* id: 0x4E0, Name: "STATUS_B_CAN", Handle: 16, [BC] */
	,0		/* id: 0x4D6, Name: "IBS3", Handle: 17, [BC] */
	,0		/* id: 0x4D4, Name: "IBS1", Handle: 18, [BC] */
	,0		/* id: 0x4CE, Name: "BATTERY_INFO", Handle: 19, [BC] */
	,0		/* id: 0x3E8, Name: "CFG_Feature", Handle: 20, [BC] */
	,0		/* id: 0x3E6, Name: "CBC_PT3", Handle: 21, [BC] */
	,0		/* id: 0x3E4, Name: "CBC_PT1", Handle: 22, [BC] */
	,0		/* id: 0x0C1CD000, Name: "WAKE_C_BCM", Handle: 23, [BC] */
	,0		/* id: 0x2EA, Name: "CBC_PT2", Handle: 24, [BC] */
	,0		/* id: 0x1E8, Name: "BCM_COMMAND", Handle: 25, [BC] */
	,0		/* id: 0x0F7, Name: "BCM_CODE_TRM_REQUEST", Handle: 26, [BC] */
	,0		/* id: 0x0F6, Name: "BCM_CODE_ESL_REQUEST", Handle: 27, [BC] */
	,0		/* id: 0x0F5, Name: "BCM_MINICRYPT_ACK", Handle: 28, [BC] */
	,0		/* id: 0x0F4, Name: "IMMO_CODE_RESPONSE", Handle: 29, [BC] */
	,0		/* id: 0x800, Name: "TxDynamicMsg0_0", Handle: 30, [BC] */
	,0		/* id: 0x800, Name: "TxDynamicMsg1_0", Handle: 31, [BC] */
	,1		/* id: 0x1E7CA0FF, Name: "ECU_APPL_BCM_1", Handle: 32, [BC] */
	,1		/* id: 0x1E3D4000, Name: "DTO_BCM", Handle: 33, [BC] */
	,1		/* id: 0x1E114000, Name: "CONFIGURATION_DATA_CODE_REQUEST_1", Handle: 34, [BC] */
	,1		/* id: 0x772, Name: "IBS_2", Handle: 35, [BC] */
	,1		/* id: 0x76D, Name: "HUMIDITY_1000L", Handle: 36, [BC] */
	,1		/* id: 0x762, Name: "COMPASS_A1", Handle: 37, [BC] */
	,1		/* id: 0x760, Name: "CBC_I5", Handle: 38, [BC] */
	,1		/* id: 0x75C, Name: "BCM_KEYON_COUNTER_B_CAN", Handle: 39, [BC] */
	,1		/* id: 0x75A, Name: "AMB_TEMP_DISP", Handle: 40, [BC] */
	,1		/* id: 0x6A8, Name: "WCPM_STATUS", Handle: 41, [BC] */
	,1		/* id: 0x6A6, Name: "GE_B", Handle: 42, [BC] */
	,1		/* id: 0x6A4, Name: "ENVIRONMENTAL_CONDITIONS", Handle: 43, [BC] */
	,1		/* id: 0x6A2, Name: "CBC_VTA_1", Handle: 44, [BC] */
	,1		/* id: 0x18DAF140, Name: "DIAGNOSTIC_RESPONSE_BCM_1", Handle: 45, [BC] */
	,1		/* id: 0x5DC, Name: "GW_C_I2", Handle: 46, [BC] */
	,1		/* id: 0x5CC, Name: "DAS_B", Handle: 47, [BC] */
	,1		/* id: 0x5CA, Name: "CBC_I2", Handle: 48, [BC] */
	,1		/* id: 0x5C8, Name: "CBC_I1", Handle: 49, [BC] */
	,1		/* id: 0x550, Name: "STATUS_B_EPB", Handle: 50, [BC] */
	,1		/* id: 0x548, Name: "STATUS_C_CAN", Handle: 51, [BC] */
	,1		/* id: 0x52E, Name: "STATUS_B_HALF", Handle: 52, [BC] */
	,1		/* id: 0x52D, Name: "HALF_B_Warning_RQ", Handle: 53, [BC] */
	,1		/* id: 0x4A3, Name: "SWS_8", Handle: 54, [BC] */
	,1		/* id: 0x4A1, Name: "StW_Actn_Rq_1", Handle: 55, [BC] */
	,1		/* id: 0x499, Name: "PAM_B", Handle: 56, [BC] */
	,1		/* id: 0x493, Name: "GW_C_I3", Handle: 57, [BC] */
	,1		/* id: 0x3D3, Name: "VIN_1", Handle: 58, [BC] */
	,1		/* id: 0x3D1, Name: "VEHICLE_SPEED_ODOMETER_1", Handle: 59, [BC] */
	,1		/* id: 0x3CF, Name: "GW_C_I6", Handle: 60, [BC] */
	,1		/* id: 0x3CB, Name: "DYNAMIC_VEHICLE_INFO2", Handle: 61, [BC] */
	,1		/* id: 0x385, Name: "STATUS_B_TRANSMISSION", Handle: 62, [BC] */
	,1		/* id: 0x0E094000, Name: "NWM_BCM", Handle: 63, [BC] */
	,1		/* id: 0x33B, Name: "STATUS_BCM2", Handle: 64, [BC] */
	,1		/* id: 0x339, Name: "STATUS_BCM", Handle: 65, [BC] */
	,1		/* id: 0x337, Name: "EXTERNAL_LIGHTS_1", Handle: 66, [BC] */
	,1		/* id: 0x309, Name: "RFHUB_B_A2", Handle: 67, [BC] */
	,1		/* id: 0x190, Name: "CBC_I4", Handle: 68, [BC] */
	,1		/* id: 0x0A4, Name: "RFHUB_B_A4", Handle: 69, [BC] */
	,1		/* id: 0x09C, Name: "STATUS_B_ECM2", Handle: 70, [BC] */
	,1		/* id: 0x09A, Name: "STATUS_B_ECM", Handle: 71, [BC] */
	,1		/* id: 0x098, Name: "STATUS_B_BSM", Handle: 72, [BC] */
	,1		/* id: 0x800, Name: "TxDynamicMsg0_1", Handle: 73, [BC] */
	,1		/* id: 0x800, Name: "TxDynamicMsg1_1", Handle: 74, [BC] */
};


V_MEMROM0 V_MEMROM1 CanTransmitHandle V_MEMROM2 CanTxStatStartIndex[kCanNumberOfChannels+1] = {
	 0
	,32
	,75
};


V_MEMROM0 V_MEMROM1 CanTransmitHandle V_MEMROM2 CanTxDynRomStartIndex[kCanNumberOfChannels+1] = {
	 30
	,73
	,75
};


V_MEMROM0 V_MEMROM1 CanTransmitHandle V_MEMROM2 CanTxDynRamStartIndex[kCanNumberOfChannels+1] = {
	 0
	,2
	,4
};


V_MEMROM0 V_MEMROM1 CanReceiveHandle V_MEMROM2 CanRxBasicStartIndex[kCanNumberOfChannels+1] = {
	 0
	,73
	,123
};


V_MEMROM0 V_MEMROM1 CanReceiveHandle V_MEMROM2 CanRxFullStartIndex[kCanNumberOfHwChannels+1] = {
	 48
	,98
	,123
};


V_MEMROM0 V_MEMROM1 CanObjectHandle V_MEMROM2 CanLogHwTxStartIndex[kCanNumberOfHwChannels+1] = {
	 0
	,2
	,4
};


V_MEMROM0 V_MEMROM1 CanObjectHandle V_MEMROM2 CanHwTxStartIndex[kCanNumberOfHwChannels] = {
	 1
	,1
};


V_MEMROM0 V_MEMROM1 CanObjectHandle V_MEMROM2 CanHwRxFullStartIndex[kCanNumberOfHwChannels] = {
	 4
	,4
};


V_MEMROM0 V_MEMROM1 CanObjectHandle V_MEMROM2 CanHwRxBasicStartIndex[kCanNumberOfHwChannels] = {
	 29
	,29
};


V_MEMROM0 V_MEMROM1 CanObjectHandle V_MEMROM2 CanHwTxNormalIndex[kCanNumberOfHwChannels] = {
	 1
	,1
};


V_MEMROM0 V_MEMROM1 vsintx V_MEMROM2 CanTxOffsetHwToLog[kCanNumberOfHwChannels] = {
	 -1
	,1
};


V_MEMROM0 V_MEMROM1 CanObjectHandle V_MEMROM2 CanHwUnusedStartIndex[kCanNumberOfHwChannels] = {
	 3
	,3
};


V_MEMROM0 V_MEMROM1 CanObjectHandle V_MEMROM2 CanHwMsgTransmitIndex[kCanNumberOfHwChannels] = {
	 2
	,2
};


V_MEMROM0 V_MEMROM1 CanObjectHandle V_MEMROM2 CanHwTxStopIndex[kCanNumberOfHwChannels] = {
	 3
	,3
};


V_MEMROM0 V_MEMROM1 CanObjectHandle V_MEMROM2 CanHwRxFullStopIndex[kCanNumberOfHwChannels] = {
	 29
	,29
};


V_MEMROM0 V_MEMROM1 CanObjectHandle V_MEMROM2 CanHwRxBasicStopIndex[kCanNumberOfHwChannels] = {
	 33
	,33
};


V_MEMROM0 V_MEMROM1 CanObjectHandle V_MEMROM2 CanHwUnusedStopIndex[kCanNumberOfHwChannels] = {
	 4
	,4
};


/* CAN buffer configuration */

#ifdef C_ENABLE_TX_FULLCAN_OBJECTS
V_MEMROM0 V_MEMROM1 canuint8 V_MEMROM2 CanTxHwObj[kCanNumberOfTxObjects] = {
	 0x1		/* id: 0x7D4, Name: "STATUS_C_BCM2", Handle: 0, [BC] */
	,0x1		/* id: 0x7D0, Name: "HVAC_INFO", Handle: 1, [BC] */
	,0x1		/* id: 0x7CC, Name: "CBC_PT4", Handle: 2, [BC] */
	,0x1		/* id: 0x7CA, Name: "BCM_KEYON_COUNTER_C_CAN", Handle: 3, [BC] */
	,0x1		/* id: 0x7C8, Name: "IBS4", Handle: 4, [BC] */
	,0x1		/* id: 0x1E7CAFF6, Name: "ECU_APPL_BCM_0", Handle: 5, [BC] */
	,0x1		/* id: 0x1E114000, Name: "CONFIGURATION_DATA_CODE_REQUEST_0", Handle: 6, [BC] */
	,0x1		/* id: 0x6D2, Name: "CBC_VTA_0", Handle: 7, [BC] */
	,0x1		/* id: 0x18DAF140, Name: "DIAGNOSTIC_RESPONSE_BCM_0", Handle: 8, [BC] */
	,0x1		/* id: 0x5DE, Name: "STATUS_B_CAN2", Handle: 9, [BC] */
	,0x1		/* id: 0x5D8, Name: "EXTERNAL_LIGHTS_0", Handle: 10, [BC] */
	,0x1		/* id: 0x5D6, Name: "ASBM1_CONTROL", Handle: 11, [BC] */
	,0x1		/* id: 0x5D4, Name: "HVAC_INFO2", Handle: 12, [BC] */
	,0x1		/* id: 0x55F, Name: "IBS2", Handle: 13, [BC] */
	,0x1		/* id: 0x4F4, Name: "VEHICLE_SPEED_ODOMETER_0", Handle: 14, [BC] */
	,0x1		/* id: 0x4E2, Name: "STATUS_C_BCM", Handle: 15, [BC] */
	,0x1		/* id: 0x4E0, Name: "STATUS_B_CAN", Handle: 16, [BC] */
	,0x1		/* id: 0x4D6, Name: "IBS3", Handle: 17, [BC] */
	,0x1		/* id: 0x4D4, Name: "IBS1", Handle: 18, [BC] */
	,0x1		/* id: 0x4CE, Name: "BATTERY_INFO", Handle: 19, [BC] */
	,0x1		/* id: 0x3E8, Name: "CFG_Feature", Handle: 20, [BC] */
	,0x1		/* id: 0x3E6, Name: "CBC_PT3", Handle: 21, [BC] */
	,0x1		/* id: 0x3E4, Name: "CBC_PT1", Handle: 22, [BC] */
	,0x1		/* id: 0x0C1CD000, Name: "WAKE_C_BCM", Handle: 23, [BC] */
	,0x1		/* id: 0x2EA, Name: "CBC_PT2", Handle: 24, [BC] */
	,0x1		/* id: 0x1E8, Name: "BCM_COMMAND", Handle: 25, [BC] */
	,0x1		/* id: 0x0F7, Name: "BCM_CODE_TRM_REQUEST", Handle: 26, [BC] */
	,0x1		/* id: 0x0F6, Name: "BCM_CODE_ESL_REQUEST", Handle: 27, [BC] */
	,0x1		/* id: 0x0F5, Name: "BCM_MINICRYPT_ACK", Handle: 28, [BC] */
	,0x1		/* id: 0x0F4, Name: "IMMO_CODE_RESPONSE", Handle: 29, [BC] */
	,0x1		/* id: 0x800, Name: "TxDynamicMsg0_0", Handle: 30, [BC] */
	,0x1		/* id: 0x800, Name: "TxDynamicMsg1_0", Handle: 31, [BC] */
	,0x1		/* id: 0x1E7CA0FF, Name: "ECU_APPL_BCM_1", Handle: 32, [BC] */
	,0x1		/* id: 0x1E3D4000, Name: "DTO_BCM", Handle: 33, [BC] */
	,0x1		/* id: 0x1E114000, Name: "CONFIGURATION_DATA_CODE_REQUEST_1", Handle: 34, [BC] */
	,0x1		/* id: 0x772, Name: "IBS_2", Handle: 35, [BC] */
	,0x1		/* id: 0x76D, Name: "HUMIDITY_1000L", Handle: 36, [BC] */
	,0x1		/* id: 0x762, Name: "COMPASS_A1", Handle: 37, [BC] */
	,0x1		/* id: 0x760, Name: "CBC_I5", Handle: 38, [BC] */
	,0x1		/* id: 0x75C, Name: "BCM_KEYON_COUNTER_B_CAN", Handle: 39, [BC] */
	,0x1		/* id: 0x75A, Name: "AMB_TEMP_DISP", Handle: 40, [BC] */
	,0x1		/* id: 0x6A8, Name: "WCPM_STATUS", Handle: 41, [BC] */
	,0x1		/* id: 0x6A6, Name: "GE_B", Handle: 42, [BC] */
	,0x1		/* id: 0x6A4, Name: "ENVIRONMENTAL_CONDITIONS", Handle: 43, [BC] */
	,0x1		/* id: 0x6A2, Name: "CBC_VTA_1", Handle: 44, [BC] */
	,0x1		/* id: 0x18DAF140, Name: "DIAGNOSTIC_RESPONSE_BCM_1", Handle: 45, [BC] */
	,0x1		/* id: 0x5DC, Name: "GW_C_I2", Handle: 46, [BC] */
	,0x1		/* id: 0x5CC, Name: "DAS_B", Handle: 47, [BC] */
	,0x1		/* id: 0x5CA, Name: "CBC_I2", Handle: 48, [BC] */
	,0x1		/* id: 0x5C8, Name: "CBC_I1", Handle: 49, [BC] */
	,0x1		/* id: 0x550, Name: "STATUS_B_EPB", Handle: 50, [BC] */
	,0x1		/* id: 0x548, Name: "STATUS_C_CAN", Handle: 51, [BC] */
	,0x1		/* id: 0x52E, Name: "STATUS_B_HALF", Handle: 52, [BC] */
	,0x1		/* id: 0x52D, Name: "HALF_B_Warning_RQ", Handle: 53, [BC] */
	,0x1		/* id: 0x4A3, Name: "SWS_8", Handle: 54, [BC] */
	,0x1		/* id: 0x4A1, Name: "StW_Actn_Rq_1", Handle: 55, [BC] */
	,0x1		/* id: 0x499, Name: "PAM_B", Handle: 56, [BC] */
	,0x1		/* id: 0x493, Name: "GW_C_I3", Handle: 57, [BC] */
	,0x1		/* id: 0x3D3, Name: "VIN_1", Handle: 58, [BC] */
	,0x1		/* id: 0x3D1, Name: "VEHICLE_SPEED_ODOMETER_1", Handle: 59, [BC] */
	,0x1		/* id: 0x3CF, Name: "GW_C_I6", Handle: 60, [BC] */
	,0x1		/* id: 0x3CB, Name: "DYNAMIC_VEHICLE_INFO2", Handle: 61, [BC] */
	,0x1		/* id: 0x385, Name: "STATUS_B_TRANSMISSION", Handle: 62, [BC] */
	,0x1		/* id: 0x0E094000, Name: "NWM_BCM", Handle: 63, [BC] */
	,0x1		/* id: 0x33B, Name: "STATUS_BCM2", Handle: 64, [BC] */
	,0x1		/* id: 0x339, Name: "STATUS_BCM", Handle: 65, [BC] */
	,0x1		/* id: 0x337, Name: "EXTERNAL_LIGHTS_1", Handle: 66, [BC] */
	,0x1		/* id: 0x309, Name: "RFHUB_B_A2", Handle: 67, [BC] */
	,0x1		/* id: 0x190, Name: "CBC_I4", Handle: 68, [BC] */
	,0x1		/* id: 0x0A4, Name: "RFHUB_B_A4", Handle: 69, [BC] */
	,0x1		/* id: 0x09C, Name: "STATUS_B_ECM2", Handle: 70, [BC] */
	,0x1		/* id: 0x09A, Name: "STATUS_B_ECM", Handle: 71, [BC] */
	,0x1		/* id: 0x098, Name: "STATUS_B_BSM", Handle: 72, [BC] */
	,0x1		/* id: 0x800, Name: "TxDynamicMsg0_1", Handle: 73, [BC] */
	,0x1		/* id: 0x800, Name: "TxDynamicMsg1_1", Handle: 74, [BC] */
};
#endif
V_MEMROM0 V_MEMROM1 canuint16 V_MEMROM2 CanInitBasic0IdType[kCanNumberOfInitObjects] = { kCanIdTypeStd, kCanIdTypeStd }; 
V_MEMROM0 V_MEMROM1 canuint16 V_MEMROM2 CanInitBasic1IdType[kCanNumberOfInitObjects] = { kCanIdTypeExt, kCanIdTypeExt }; 

V_MEMROM0 V_MEMROM1 CanChipRegPtr V_MEMROM2 CanBasisAdr[kCanNumberOfChannels] = {
	(CanChipRegPtr)(0xC000)
	,(CanChipRegPtr)(0xC100)
};

/*************************************************************/
/*************************************************************/

#if defined ( C_MULTIPLE_RECEIVE_CHANNEL )
V_MEMROM0 V_MEMROM1 canuint8 V_MEMROM2 CanInitObjectStartIndex[3] = {
	 0
	,1
	,2
};
#endif


/*************************************************************/
/* Init structure                                            */
/*************************************************************/
V_MEMROM0 V_MEMROM1 canuint16 V_MEMROM2 CanInitBitTiming[] = { 0x2B81, 0x2B87 }; 
V_MEMROM0 V_MEMROM1 canuint16 V_MEMROM2 CanInitBasicMsk[] = { 0xe007, 0xf007 }; 
V_MEMROM0 V_MEMROM1 canuint16 V_MEMROM2 CanInitBasicCod[] = { 0x00, 0x1000 }; 
V_MEMROM0 V_MEMROM1 canuint16 V_MEMROM2 CanInitBRP_Reg[] = {0x00, 0x00}; 		/* Prescaler register */

V_MEMROM0 V_MEMROM1 canuint16 V_MEMROM2 CanInitBasicMskLo[] = { 0xffff, 0xffff }; 
V_MEMROM0 V_MEMROM1 canuint16 V_MEMROM2 CanInitBasicCodLo[] = { 0x00, 0x00 }; 
V_MEMROM0 V_MEMROM1 canuint16 V_MEMROM2 CanInitBasic1Msk[] = { 0xe124, 0xefc7 }; 
V_MEMROM0 V_MEMROM1 canuint16 V_MEMROM2 CanInitBasic1MskLo[] = { 0x100, 0xff80 }; 
V_MEMROM0 V_MEMROM1 canuint16 V_MEMROM2 CanInitBasic1Cod[] = { 0x4000, 0x4e01 }; 
V_MEMROM0 V_MEMROM1 canuint16 V_MEMROM2 CanInitBasic1CodLo[] = { 0x00, 0x4000 }; 


