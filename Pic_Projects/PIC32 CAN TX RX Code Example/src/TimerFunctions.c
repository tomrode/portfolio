/**********************************************************************
* � 2009 Microchip Technology Inc.
*
* FileName:        TimerFunctions.c
* Dependencies:    Header (.h) files if applicable, see below
* Processor:       PIC32
* Compiler:        MPLAB� C32 
*
* SOFTWARE LICENSE AGREEMENT:
* Microchip Technology Incorporated ("Microchip") retains all ownership and 
* intellectual property rights in the code accompanying this message and in all 
* derivatives hereto.  You may use this code, and any derivatives created by 
* any person or entity by or on your behalf, exclusively with Microchip's
* proprietary products.  Your acceptance and/or use of this code constitutes 
* agreement to the terms and conditions of this notice.
*
* CODE ACCOMPANYING THIS MESSAGE IS SUPPLIED BY MICROCHIP "AS IS".  NO 
* WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED 
* TO, IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS FOR A 
* PARTICULAR PURPOSE APPLY TO THIS CODE, ITS INTERACTION WITH MICROCHIP'S 
* PRODUCTS, COMBINATION WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION. 
*
* YOU ACKNOWLEDGE AND AGREE THAT, IN NO EVENT, SHALL MICROCHIP BE LIABLE, WHETHER 
* IN CONTRACT, WARRANTY, TORT (INCLUDING NEGLIGENCE OR BREACH OF STATUTORY DUTY), 
* STRICT LIABILITY, INDEMNITY, CONTRIBUTION, OR OTHERWISE, FOR ANY INDIRECT, SPECIAL, 
* PUNITIVE, EXEMPLARY, INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, FOR COST OR EXPENSE OF 
* ANY KIND WHATSOEVER RELATED TO THE CODE, HOWSOEVER CAUSED, EVEN IF MICROCHIP HAS BEEN 
* ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE.  TO THE FULLEST EXTENT 
* ALLOWABLE BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN ANY WAY RELATED TO 
* THIS CODE, SHALL NOT EXCEED THE PRICE YOU PAID DIRECTLY TO MICROCHIP SPECIFICALLY TO 
* HAVE THIS CODE DEVELOPED.
*
* You agree that you are solely responsible for testing the code and 
* determining its suitability.  Microchip has no obligation to modify, test, 
* certify, or support the code.
************************************************************************/

#include "h\TimerFunctions.h"

static BOOL   oneSecondUp;
static UINT32 _1000msecCounter;

/* added 2_21_2012*/
static BOOL   _500SecondUp;
static UINT32 _500msecCounter;
/* added 2/23/2013*/
static BOOL   _250msSecondUp; 
static UINT32 _250msecCounter; 

static BOOL   _100msSecondUp; 
static UINT32 _100msecCounter; 

static BOOL   _50msSecondUp; 
static UINT32 _50msecCounter; 

static BOOL   _25msSecondUp; 
static UINT32 _25msecCounter; 

static BOOL   _10msSecondUp; 
static UINT32 _10msecCounter;

static BOOL   _5msSecondUp; 
static UINT32 _5msecCounter;  
void Timer1Init(void)
{
	/* This function will intialize the Timer 1
	 * for basic timer operation. It will enable
	 * timer interrupt. The one second flag is
	 * initialized and the millisecond counter is 
	 * initialized. */

	T1CON = 0x0;			/* Basic Timer Operation				*/
	PR1 = TIMER1_PERIOD;	/* Timer period in TimerFunctions.h 	*/

	IFS0CLR = 0x10;			/* Clear interrupt flag and enable		*/
	IEC0SET = 0x10;			/* timer 1 interrupt. 					*/
	IPC1bits.T1IP = 4;		/* Timer 1 priority is 4				*/

	oneSecondUp = FALSE;	/* Intialize the one second flag		*/
	_1000msecCounter = 0;	/* and the millisecond counter.			*/
						
    /* added 2_21_2012*/
    _500SecondUp = FALSE;
    _500msecCounter = 0;
 
    _250msSecondUp = FALSE;
    _250msecCounter = 0;

     _100msSecondUp = FALSE;
     _100msecCounter = 0;

     _50msSecondUp = FALSE;
     _50msecCounter = 0;

     _25msSecondUp = FALSE;
     _25msecCounter = 0; 

     _10msSecondUp = FALSE;
     _10msecCounter = 0; 

     _5msSecondUp = FALSE;
     _5msecCounter = 0; 

    T1CONSET = 0x8000;		/* Start the timer.	*/ 
}

BOOL _1000ms_task(void)
{
	/* This function will return TRUE if
	 * a second has expired since the last
	 * time the function had returned
	 * TRUE. If not then function returns
	 * FALSE. */

	BOOL result;

	if (oneSecondUp == TRUE)
	{
		/* If a second has expired
		 * then return true and reset 
		 * the one second flag. */

		result = TRUE;	
		oneSecondUp = FALSE;
	}
	else
	{
		result = FALSE;
	}
	
	return(result);
}


BOOL _500ms_task(void)
{
	/* This function will return TRUE if
	 * a half second has expired since the last
	 * time the function had returned
	 * TRUE. If not then function returns
	 * FALSE. Please keep in mind the interrupt handler 
     * will set msecCounter to zero independantly and may 
     * result in longer time out for its copy here.     */

	BOOL result;
   
	if (_500SecondUp == TRUE)   
	{
		/* If a half second has expired
		 * then return true and reset 
		 * the one second flag. */

		result = TRUE;	
		_500SecondUp = 0;               /* reset the counter */
       
	}
	else
	{
		result = FALSE;
	}
	
	return(result);
}

BOOL _250ms_task(void)
{
	/* This function will return TRUE if
	 * a 250 ms has expired since the last
	 * time the function had returned
	 * TRUE. If not then function returns
	 * FALSE. Please keep in mind the interrupt handler 
     * will set msecCounter to zero independantly and may 
     * result in longer time out for its copy here.     */

	BOOL result;
   
	if (_250msSecondUp  == TRUE)   
	{
		/* If a 250ms has expired
		 * then return true and reset 
		 * the one second flag. */

		result = TRUE;	
		_250msSecondUp = 0;               /* reset the counter */
       
	}
	else
	{
		result = FALSE;
	}
	
	return(result);
}

BOOL _100ms_task(void)
{
	/* This function will return TRUE if
	 * a 100ms has expired since the last
	 * time the function had returned
	 * TRUE. If not then function returns
	 * FALSE. Please keep in mind the interrupt handler 
     * will set msecCounter to zero independantly and may 
     * result in longer time out for its copy here.     */

	BOOL result;
   
	if (_100msSecondUp  == TRUE)   
	{
		/* If a 100ms has expired
		 * then return true and reset 
		 * the one second flag. */

		result = TRUE;	
		_100msSecondUp = 0;               /* reset the counter */
       
	}
	else
	{
		result = FALSE;
	}
	
	return(result);
}

BOOL _50ms_task(void)
{
	/* This function will return TRUE if
	 * a 50 ms has expired since the last
	 * time the function had returned
	 * TRUE. If not then function returns
	 * FALSE. Please keep in mind the interrupt handler 
     * will set msecCounter to zero independantly and may 
     * result in longer time out for its copy here.     */

	BOOL result;
   
	if (_50msSecondUp  == TRUE)   
	{
		/* If a 50ms has expired
		 * then return true and reset 
		 * the one second flag. */

		result = TRUE;	
		_50msSecondUp = 0;               /* reset the counter */
       
	}
	else
	{
		result = FALSE;
	}
	
	return(result);
}

BOOL _25ms_task(void)
{
	/* This function will return TRUE if
	 * a 25 ms has expired since the last
	 * time the function had returned
	 * TRUE. If not then function returns
	 * FALSE. Please keep in mind the interrupt handler 
     * will set msecCounter to zero independantly and may 
     * result in longer time out for its copy here.     */

	BOOL result;
   
	if (_25msSecondUp  == TRUE)   
	{
		/* If a 20ms has expired
		 * then return true and reset 
		 * the one second flag. */

		result = TRUE;	
		_25msSecondUp = 0;               /* reset the counter */
       
	}
	else
	{
		result = FALSE;
	}
	
	return(result);
}


BOOL _10ms_task(void)
{
	/* This function will return TRUE if
	 * a 10 ms has expired since the last
	 * time the function had returned
	 * TRUE. If not then function returns
	 * FALSE. Please keep in mind the interrupt handler 
     * will set msecCounter to zero independantly and may 
     * result in longer time out for its copy here.     */

	BOOL result;
   
	if (_10msSecondUp  == TRUE)   
	{
		/* If a 20ms has expired
		 * then return true and reset 
		 * the one second flag. */

		result = TRUE;	
		_10msSecondUp = 0;               /* reset the counter */
       
	}
	else
	{
		result = FALSE;
	}
	
	return(result);
}
BOOL _5ms_task(void)
{
	/* This function will return TRUE if
	 * a 5 ms has expired since the last
	 * time the function had returned
	 * TRUE. If not then function returns
	 * FALSE. Please keep in mind the interrupt handler 
     * will set msecCounter to zero independantly and may 
     * result in longer time out for its copy here.     */

	BOOL result;
   
	if (_5msSecondUp  == TRUE)   
	{
		/* If a 5ms has expired
		 * then return true and reset 
		 * the one second flag. */

		result = TRUE;	
		_5msSecondUp = 0;               /* reset the counter */
       
	}
	else
	{
		result = FALSE;
	}
	
	return(result);
}
void __attribute__((vector(4), interrupt(ipl4), nomips16)) Timer1InterruptHandler(void)
{
	/* This is the Timer 1 ISR */
	
	IFS0CLR = 0x10; 	/* Clear the Interrupt Flag	*/
	
	_1000msecCounter ++;		/* Increment millisecond counter.	*/ 
	_500msecCounter++;  	    /* Increment millisecond counter.	*/
    _250msecCounter++;          /* Increment millisecond counter.	*/
    _100msecCounter++;          /* Increment millisecond counter.	*/
    _50msecCounter++;          /* Increment millisecond counter.	*/
    _25msecCounter++;          /* Increment millisecond counter.	*/
    _10msecCounter++;          /* Increment millisecond counter.	*/
    _5msecCounter++;          /* Increment millisecond counter.	*/
	if(_1000msecCounter == _1000_MSEC) 
  	{
		/* This means that one second
		 * has expired since the last time
		 * msecCounter was 0. */

		oneSecondUp = TRUE;	/* Indicate that one second is up	*/
		_1000msecCounter = 0;	/* Reset the millisecond counter.	*/
	}
  
   if(_500msecCounter ==  _500_MSEC) 
  	{
		/* This means that 500 ms
		 * has expired since the last time
		 * msecCounter was 0. */

		_500SecondUp = TRUE;	/* Indicate that one half second is up	*/
		_500msecCounter = 0;	/* Reset the millisecond counter.	*/
	}
   if(_250msecCounter ==  _250_MSEC) 
  	{
		/* This means that 250 ms
		 * has expired since the last time
		 * msecCounter was 0. */

	    _250msSecondUp  = TRUE;	/* Indicate that 250 mssecond is up	*/
	   	_250msecCounter = 0;	/* Reset the millisecond counter.	*/
	} 
    if(_100msecCounter ==  _100_MSEC) 
  	{
		/* This means that 100 ms
		 * has expired since the last time
		 * msecCounter was 0. */

	    _100msSecondUp  = TRUE;	/* Indicate that 100 mssecond  is up	*/
	   	_100msecCounter = 0;	/* Reset the millisecond counter.	*/
	} 
    if(_50msecCounter ==  _50_MSEC) 
  	{
		/* This means that 50 ms
		 * has expired since the last time
		 * msecCounter was 0. */

	    _50msSecondUp  = TRUE;	/* Indicate that 50 mssecond  is up	*/
	   	_50msecCounter = 0;	/* Reset the millisecond counter.	*/
	} 
    if(_25msecCounter ==  _25_MSEC) 
  	{
		/* This means that 25 ms
		 * has expired since the last time
		 * msecCounter was 0. */

	    _25msSecondUp  = TRUE;	/* Indicate that 25 mssecond  is up	*/
	   	_25msecCounter = 0;	/* Reset the millisecond counter.	*/
	} 
     if(_10msecCounter ==  _10_MSEC) 
  	{
		/* This means that 10 ms
		 * has expired since the last time
		 * msecCounter was 0. */

	    _10msSecondUp  = TRUE;	/* Indicate that 10 mssecond  is up	*/
	   	_10msecCounter = 0;	/* Reset the millisecond counter.	*/
	} 
     if(_5msecCounter ==  _5_MSEC) 
  	{
		/* This means that 5 ms
		 * has expired since the last time
		 * msecCounter was 0. */

	    _5msSecondUp  = TRUE;	/* Indicate that 5 mssecond is up	*/
	   	_5msecCounter = 0;	/* Reset the millisecond counter.	*/
	} 
}
