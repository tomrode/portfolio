opt subtitle "HI-TECH Software Omniscient Code Generator (Lite mode) build 6738"

opt pagewidth 120

	opt lm

	processor	16F887
clrc	macro
	bcf	3,0
	endm
clrz	macro
	bcf	3,2
	endm
setc	macro
	bsf	3,0
	endm
setz	macro
	bsf	3,2
	endm
skipc	macro
	btfss	3,0
	endm
skipz	macro
	btfss	3,2
	endm
skipnc	macro
	btfsc	3,0
	endm
skipnz	macro
	btfsc	3,2
	endm
indf	equ	0
indf0	equ	0
pc	equ	2
pcl	equ	2
status	equ	3
fsr	equ	4
fsr0	equ	4
c	equ	1
z	equ	0
pclath	equ	10
# 7 "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Linked_list\main.c"
	psect config,class=CONFIG,delta=2 ;#
# 7 "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Linked_list\main.c"
	dw 0x1FFF & 0x3FFF & 0x3FFF & 0x3BFF & 0x3EFF & 0x3FFF & 0x3FFF & 0x3FFF & 0x3FEF & 0x3FF7 & 0x3FFD & 0x3FBF ;#
# 8 "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Linked_list\main.c"
	psect config,class=CONFIG,delta=2 ;#
# 8 "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Linked_list\main.c"
	dw 0x3FFF ;#
	FNCALL	_main,_malloc
	FNROOT	_main
	global	_toms_variable
psect	maintext,global,class=CODE,delta=2
global __pmaintext
__pmaintext:
	file	"Linked_List.as"
	line	#
psect cinit,class=CODE,delta=2
global start_initialization
start_initialization:

psect	bssCOMMON,class=COMMON,space=1
global __pbssCOMMON
__pbssCOMMON:
_toms_variable:
       ds      1

; Clear objects allocated to COMMON
psect cinit,class=CODE,delta=2
	clrf	((__pbssCOMMON)+0)&07Fh
psect cinit,class=CODE,delta=2
global end_of_initialization

;End of C runtime variable initialization code

end_of_initialization:
clrf status
ljmp _main	;jump to C main() function
psect	cstackCOMMON,class=COMMON,space=1
global __pcstackCOMMON
__pcstackCOMMON:
	global	?_malloc
?_malloc:	; 2 bytes @ 0x0
	global	?_main
?_main:	; 2 bytes @ 0x0
	global	_malloc$0
_malloc$0:	; 2 bytes @ 0x0
	ds	2
	global	??_malloc
??_malloc:	; 0 bytes @ 0x2
	global	??_main
??_main:	; 0 bytes @ 0x2
	global	main@str
main@str:	; 2 bytes @ 0x2
	ds	2
;;Data sizes: Strings 0, constant 0, data 0, bss 1, persistent 0 stack 0
;;Auto spaces:   Size  Autos    Used
;; COMMON          14      4       5
;; BANK0           80      0       0
;; BANK1           80      0       0
;; BANK3           96      0       0
;; BANK2           96      0       0

;;
;; Pointer list with targets:

;; sp__malloc	PTR void  size(2) Largest target is 512
;;		 -> RAM(DATA[512]), 
;;
;; main@str	PTR unsigned char  size(2) Largest target is 512
;;		 -> RAM(DATA[512]), 
;;


;;
;; Critical Paths under _main in COMMON
;;
;;   _main->_malloc
;;
;; Critical Paths under _main in BANK0
;;
;;   None.
;;
;; Critical Paths under _main in BANK1
;;
;;   None.
;;
;; Critical Paths under _main in BANK3
;;
;;   None.
;;
;; Critical Paths under _main in BANK2
;;
;;   None.

;;
;;Main: autosize = 0, tempsize = 0, incstack = 0, save=0
;;

;;
;;Call Graph Tables:
;;
;; ---------------------------------------------------------------------------------
;; (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;; ---------------------------------------------------------------------------------
;; (0) _main                                                 6     6      0       0
;;                                              2 COMMON     2     2      0
;;                             _malloc
;; ---------------------------------------------------------------------------------
;; (1) _malloc                                               2     0      2       0
;;                                              0 COMMON     2     0      2
;; ---------------------------------------------------------------------------------
;; Estimated maximum stack depth 1
;; ---------------------------------------------------------------------------------

;; Call Graph Graphs:

;; _main (ROOT)
;;   _malloc
;;

;; Address spaces:

;;Name               Size   Autos  Total    Cost      Usage
;;BITCOMMON            E      0       0       0        0.0%
;;EEDATA             100      0       0       0        0.0%
;;NULL                 0      0       0       0        0.0%
;;CODE                 0      0       0       0        0.0%
;;COMMON               E      4       5       1       35.7%
;;BITSFR0              0      0       0       1        0.0%
;;SFR0                 0      0       0       1        0.0%
;;BITSFR1              0      0       0       2        0.0%
;;SFR1                 0      0       0       2        0.0%
;;STACK                0      0       1       2        0.0%
;;ABS                  0      0       5       3        0.0%
;;BITBANK0            50      0       0       4        0.0%
;;BITSFR3              0      0       0       4        0.0%
;;SFR3                 0      0       0       4        0.0%
;;BANK0               50      0       0       5        0.0%
;;BITSFR2              0      0       0       5        0.0%
;;SFR2                 0      0       0       5        0.0%
;;BITBANK1            50      0       0       6        0.0%
;;BANK1               50      0       0       7        0.0%
;;BITBANK3            60      0       0       8        0.0%
;;BANK3               60      0       0       9        0.0%
;;BITBANK2            60      0       0      10        0.0%
;;BANK2               60      0       0      11        0.0%
;;DATA                 0      0       6      12        0.0%

	global	_main
psect	maintext

;; *************** function _main *****************
;; Defined at:
;;		line 31 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Linked_list\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  str             2    2[COMMON] PTR unsigned char 
;;		 -> RAM(512), 
;;  test            2    0        int 
;;  root            2    0        PTR struct node
;; Return value:  Size  Location     Type
;;                  2  838[COMMON] int 
;; Registers used:
;;		wreg, fsr0l, fsr0h, fsr1l, fsr1h, status,2, status,0, btemp+1, pclath, cstack
;; Tracked objects:
;;		On entry : 17F/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         2       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         2       0       0       0       0
;;Total ram usage:        2 bytes
;; Hardware stack levels required when called:    1
;; This function calls:
;;		_malloc
;; This function is called by:
;;		Startup code after reset
;; This function uses a non-reentrant model
;;
psect	maintext
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\Linked_list\main.c"
	line	31
	global	__size_of_main
	__size_of_main	equ	__end_of_main-_main
	
_main:	
	opt	stack 7
; Regs used in _main: [allreg]
	line	38
	
l2001:	
;main.c: 33: struct node *root;
;main.c: 34: char *str;
;main.c: 35: int test;
;main.c: 38: str = (char *) malloc(15);
	movlw	low(0Fh)
	movwf	(?_malloc)
	movlw	high(0Fh)
	movwf	((?_malloc))+1
	fcall	_malloc
	movf	(1+(?_malloc)),w
	clrf	(main@str+1)
	addwf	(main@str+1)
	movf	(0+(?_malloc)),w
	clrf	(main@str)
	addwf	(main@str)

	goto	l840
	line	44
;main.c: 44: while(1)
	
l839:	
	line	48
;main.c: 45: {
	
l840:	
	line	44
	goto	l840
	
l841:	
	line	49
	
l842:	
	global	start
	ljmp	start
	opt stack 0
GLOBAL	__end_of_main
	__end_of_main:
;; =============== function _main ends ============

	signat	_main,90
	global	_malloc
psect	maintext
	global	btemp
	btemp set 07Eh

	DABS	1,126,2	;btemp
	global	wtemp0
	wtemp0 set btemp
	end
