#include <htc.h>
#include "spi_func.h"
#include "timer.h"
#include "adc_func.h"

#define FRAM_WREN1  (0x06)             /* Set Write enble latch*/
#define FRAM_WRITE1 (0x02)             /* Write Memory Data*/
#define FRAM_WRDIS1 (0x04)             /* Write Disable*/
#define FRAM_WRSR1  (0x01)             /* Write Status Register*/
#define FRAM_A81    (0x08)             /* Bit 3 in the op-code corresponds to address A8*/
#define FRAM_RDSR1  (0x05)             /* Read Status Register*/
#define FRAM_READ1  (0x03)             /* Read Memory Data*/
#define   SPI_TXOUT   (1)      
enum                                 /* USED IN FRAM STATE MACHINE*/
   {
   FRAM_DEFAULT,                    
   FRAM_WRITEEN,
   FRAM_WRITE,
   FRAM_WRITEDIS,
   FRAM_READEN, 
   FRAM_READ,
   };
  
 
const uint8_t FRAM_OPCODES[] =  
{
        0x06,                        /* Set Write enble latch. WREN  INDEX 0 */ 
        0x04,                        /* Write Disable.         WRDI  INDEX 1 */   
        0x05,                        /* Read Status Register.  RDSR  INDEX 2 */
        0x01,                        /* Write staus Register.  WRSR  INDEX 3 */
        0x03,                        /* Read Memory Data.      READ  INDEX 4 */
        0x02                         /* Write Memory Data      WRITE INDEX 5 */
}; 

const uint16_t FRAM_ADDRESS[] =

{       0x00,                         /*  location   0                        */
        0x01,                         /*             |                        */
        0x02,                         /*             |                        */
        0x03,                         /*             |                        */
        0x04,                         /*             |                        */
        0x05,                         /*             |                        */
        0x06,                         /*             |                        */
        0x07,                         /*             |                        */ 
        0x08,                         /*             |                        */
        0x09,                         /*             |                        */
        0x0A                          /*             |                        */
                                      /*   location  512                      */
};  

uint8_t FRAM_DATA[] = 

{
       'M', 
       'Y',
       'N',
       'A',
       'M',
       'E', 
       'I',
       'S',
       'T',
       'O',
       'M',
       'R',
       'O',
       'D',
       'E'     

};

uint8_t FRAM_DATA2[] = 

{
       'N', 
       'O',
       'T', 
       'H',
       'I',      
       'N',
       'G'
};

uint8_t FRAM_DATA_READ[15]; 


void spi_fram(void)
{ uint8_t *ptr;
  uint8_t i;
  uint8_t index;
  uint8_t  tx_addr;
  uint8_t  tx_data;
  uint16_t tx_opcode;
  static uint8_t fram_state_machine = FRAM_DEFAULT;       /* sequence for accessing FRAM OPCODE->Address->Data if a write*/
  WCOL = 0;                          /* Clear the Write Collision flag. */
  SSPOV = 0;                         /* Clear the Receive Overflow flag. */
  SSPIF = 0;                         /* Clear the RX complete flag. */
 // PORTD = 0xFD;//0xFE;               /* Enable SS low (RD0 Pin 19 = 0). */
  
switch(fram_state_machine)
{ 
  case FRAM_WRITEEN:                   /* THE WRITING SEQUENCE*/
  
  if (tx_opcode == FRAM_OPCODES[0])   /* Set Write enable */
   { 
     RD1 = 0;                         /* Enable SS low (RD0 Pin 19, RD1 pin 20). */
     SSPBUF = tx_opcode;              /* Write the SPI transmit buffer for opcode WREN 0x06. */
     while (SSPIF == 1)               /* Wait until the TX data is sent. */
     {
     }
     SSPIF = 0;                       /* Clear the RX complete flag. */
     //RD1 = 1;                         /* Set SS back high*/ 
     tx_opcode = FRAM_OPCODES[5];     /* set the write memory data index 5*/
     fram_state_machine = FRAM_WRITE;  
   }
   else
   {
   }
  
    break; 
   
  case FRAM_WRITE:
  
  if (tx_opcode == FRAM_OPCODES[5])     /*Check for Write Memory Data      WRITE INDEX 5*/  
     {
       WCOL = 0;                          /* Clear the Write Collision flag. */
       SSPOV = 0;                         /* Clear the Receive Overflow flag. */
       RD1 = 0;                         /* Enable SS low (RD0 Pin 19, RD1 pin 20). */
      
      /* STOPS HERE 0x02 does get out  */
       SSPBUF = tx_opcode;              /* Write the SPI transmit buffer for WRITE opcode 0x02. */
       while (SSPIF == 1)               /* Wait until the TX data is sent. */
       {
       }
       SSPIF = 0;                       /* Clear the RX complete flag. */
       SSPBUF = tx_addr;                 /* send the address for the first byte*/
       while (SSPIF == 1)               /* Wait until the TX data is sent. */
       {
       }
       SSPIF = 0;                       /*clear RX complete flag*/
       index = 0;                       /* set for first data Byte*/
       for (i=0;index<=7;i++)   /* For test purposes only 7 elements*/
        { 
          WCOL = 0;                          /* Clear the Write Collision flag. */
          SSPOV = 0;                         /* Clear the Receive Overflow flag. */
          tx_data = FRAM_DATA[index];
          index++;
          SSPBUF = tx_data;      /*Current value FRAM_DATA[0];Data value. */
          asm("nop");
          while (SSPIF == 1)            /* Wait until the TX data is sent. */
          {
          }
          SSPIF = 0;                    /* Clear the RX complete flag. */ 
        }
       RD1 = 1;                         /* Set SS back high*/ 
     fram_state_machine = FRAM_WRITE; 
     }    
     else
      {
      }
    
            
       break;
  
  case FRAM_WRITEDIS:
  
       tx_opcode = FRAM_OPCODES[1];     /* 0x04,Write Disable. WRDI*/ 
       RD1 = 0;                         /* Enable SS low (RD0 Pin 19, RD1 pin 20). */
       SSPBUF = tx_opcode;              /* Write the SPI transmit buffer for WRITE opcode 0x02. */
       while (SSPIF == 1)               /* Wait until the TX data is sent. */
       {
       }
       SSPIF = 0; 
       RD1 = 1;                         /* Set SS back high*/ 
   
       break;  
    

      default:
      ptr = &FRAM_DATA[0];  
      fram_state_machine = FRAM_WRITEEN; 
      tx_opcode = FRAM_OPCODES[0];       /* Write data enable latch*/
      tx_addr = FRAM_ADDRESS[1];         /* Address Value*/ 
      tx_data = FRAM_DATA[0];            /* Data value. */
      PORTD = 0xFF;                      /* dis-able the Slave select*/ 
     
      break;
     
 }
}




void spi_txdata(void)
{
  uint8_t tx_data;
  
  if (get_timer(TIMER_1) == 0)       /* Has the SPI timer expired?  If so... */
  {  
     //PORTD = 0xFD;                 /* Enable SS (RD1 Pin 20 = 0). */
     tx_data = linear_motor();       /* Read the potentiometer value. */
     WCOL = 0;                       /* Clear the Write Collision flag. */
     SSPOV = 0;                      /* Clear the Receive Overflow flag. */
     SSPIF = 0;                      /* Clear the RX complete flag. */
     PORTD = 0xFD;                   /* Enable SS (RD1 Pin 20 = 0). */
     SSPBUF = tx_data;               /* Write the SPI transmit buffer. */

     while (SSPIF == 0)              /* Wait until the TX data is sent. */
     {
     }

     PORTD = 0xFF;                   /* Disable SS (RD1 Pin 20 = 1). */
    
     timer_set(TIMER_1,SPI_TXOUT);   /* Re-initialize the transmit timer. */
  }
}
/*****************************************************************************************************/
/*   fram_write()
/*   Descrition: Function enables RAMTRON_FM25040A, FRAM part for writes. It sends Write enable opcode  
/*               the the WRITE command followed by a 9 bit address and 8 bytes of data.
/*   INPUT1 ->   Pointer to Data array ,
/*   INPUT2 ->   Address to device
/*   INPUT3 ->   Amount of Bytes to be sent 
/*   
/*   OUPTUS ->   NONE
/*****************************************************************************************************/

void fram_write(uint8_t *FRAM_DATA, uint16_t address, uint16_t count)
{
  uint8_t index;
  uint16_t test;
  test = sizeof(test);
  if (count != 0)
  {
    /* Enable Writes */
    RD1 = 0;                         /* Enable SS low (RD0 Pin 19, RD1 pin 20). */
    SSPIF = 0;                       /* Clear the RX complete flag. */
    SSPBUF = FRAM_WREN1;             /* Write the SPI transmit buffer for opcode WREN 0x06. */
    //while (!SSPIF)                   /* Wait until the TX data is sent. */
    //{
    //}
    RD1 = 1;                         /* Set SS back high*/ 
    WCOL = 0;                        /* Clear the Write Collision flag. */
    SSPOV = 0;                       /* Clear the Receive Overflow flag. */

    /* Write data */
    RD1 = 0;                         /* Enable SS low (RD0 Pin 19, RD1 pin 20). */
    SSPIF = 0;                       /* Clear the RX complete flag. */
    if (address & 0x100)
    {
      SSPBUF = FRAM_WRITE1 | FRAM_A81;
    }
    else
    {
      SSPBUF = FRAM_WRITE1;          /* 0x02 */
    }
    
    //while (!SSPIF)                   /* Wait until the TX data is sent. */
    //{
    //}

    SSPIF = 0;                       /* Clear the RX complete flag. */
    SSPBUF = (uint8_t)(address);     /* type cast */
    //while (!SSPIF)                   /* Wait until the TX data is sent. */
    //{
    //}

    for (index = 0; index < count; index++)
    {
      SSPIF = 0;                     /* Clear the RX complete flag. */
      SSPBUF = FRAM_DATA[index];
     // while (!SSPIF)                 /* Wait until the TX data is sent. */
      //{
      //}
    }
    RD1 = 1;                         /* Set SS back high*/ 

    /* Disable Writes */
    RD1 = 0;                         /* Enable SS low (RD0 Pin 19, RD1 pin 20). */
    SSPIF = 0; 
    SSPBUF = FRAM_WRDIS1;            /* Write the SPI transmit buffer for WRITE opcode 0x02. */
    while (!SSPIF)                   /* Wait until the TX data is sent. */
    {
    }
    RD1 = 1;                         /* Set SS back high*/ 
  }
}


/*****************************************************************************************************/
/*   fram_read()
/*   Descrition: Function enables RAMTRON_FM25040A, FRAM part for reads. It sends read enable opcode  
/*               the the Read Memory Data command followed by a 9 bit address and 8 bytes of data.
/*   INPUT1 ->   Pointer to Data array ,
/*   INPUT2 ->   Address to device
/*   INPUT3 ->   Amount of Bytes to be read 
/*   
/*   OUPTUS ->   NONE
/*  
/*    1.  Is the byte count zero?  If so, go to step (9).
/*    2.  Write a zero byte.
/*    3.  Transmission complete?  If not, go to step (3).
/*    4.  Read the SPI buffer.
/*    5.  Store it in the array at the current array index.
/*    6.  Increment the index.
/*    7.  Decrement the byte count.
/*    8.  Go to step (1)
/*    9.  Done.

/*****************************************************************************************************/
void fram_read(uint8_t *FRAM_DATA_READ, uint16_t address, uint16_t count)
{
 uint8_t index;
 if (count != 0)
  {
    /* Enable Reads */
    RD1 = 0;                         /* Enable SS low (RD0 Pin 19, RD1 pin 20). */
    SSPIF = 0;                       /* Clear the RX complete flag. */
   
    WCOL = 0;                        /* Clear the Write Collision flag. */
    SSPOV = 0;                       /* Clear the Receive Overflow flag. */

   if (address & 0x100)
    {
      SSPBUF = FRAM_READ1 | FRAM_A81;
    }
    else
    {
      SSPBUF = FRAM_READ1;          /* 0x03 */
    }
    
    while (!SSPIF)                   /* Wait until the TX data is sent. */
    {
    }

    SSPIF = 0;                       /* Clear the RX complete flag. */
    SSPBUF = (uint8_t)(address);     /* type cast */
    while (!SSPIF)                   /* Wait until the TX data is sent. */
    {
    }

    for (index = 0; index < count; index++)
    { 
      SSPIF = 0;                     /* Clear the RX complete flag. */
      SSPBUF = 0x00;                 /* Padding byte to get out data desired*/
      while (!SSPIF)                 /* Wait until the TX data is sent. */
      {
      }
      FRAM_DATA_READ[index] = SSPBUF;
      //SSPIF = 0;                     /* Clear the RX complete flag. */
      
     // while (!SSPIF)                 /*Read in SPI buffer. */
     // {
     // }
    }
    RD1 = 1;                         /* Set SS back high*/ 
 }
}