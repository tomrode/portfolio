#include <htc.h>
#include "typedefs.h"


extern uint8_t FRAM_DATA[];
extern uint8_t FRAM_DATA2[];
extern uint8_t FRAM_DATA_READ[];
/*Function prototypes*/

void spi_txdata(void);
void spi_fram (void);
void fram_write(uint8_t *FRAM_DATA, uint16_t address, uint16_t count);
void fram_read(uint8_t *FRAM_DATA_READ, uint16_t address, uint16_t count);