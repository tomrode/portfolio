/**************************************************************/          
/* 4 OCT 2011 using PIC16F877A                                */ 
/* Setting up for SPI MASTER, Testing data out worked         */
/* sent out a dummy byte 0x55 and 0x53 appears the            */ 
/* format looks right except the levels are inverted on scope */
/* may need to reconfigure CKE bit                            */
/* Going to try ADC and try to send out value                 */
/* 18 OCT 2011 Will now install spi.c to remove from main in  */
/* effort to integrate timer so to not blast slave with       */
/* constant data                                              */   
/**************************************************************/
#include <htc.h>
#include "typedefs.h"
#include "timer.h"
#include "adc_func.h"
#include "spi_func.h"
/**********************************************************/
/* Device Configuration
/**********************************************************/
     
      /* PIC16F877A*/
__CONFIG (WDTDIS & PWRTDIS & BORDIS & LVPEN & WRTEN &
          DEBUGDIS & DUNPROT & UNPROTECT & HS);  //HS means resonator       
      
       
 
/**********************************************************/
/* Function Prototypes 
/**********************************************************/

void init(void);     // SFR inits

/***********************************************************/
/* Variable Definition
/***********************************************************/
//unsigned int watch_dog_count=0;

persistent int watch_dog_count;                /* will remain with a reset  */
int main(void)
{
/************************************************************/
/* LOCAL MAIN VARIABLES                                     */
/************************************************************/
watch_dog_count=0;                    
uint8_t dummy;                                 // send a message to test
uint8_t *ptr;
uint8_t FRAM_DATA_MAIN[] = {'B','A','D','B','E','E','F','M','A','I','N'};

ptr = &FRAM_DATA_MAIN[0];   /* Dereference to see address*/
(*ptr++);                   /* test the address increments*/
/***********************************************************/
/* INITIALIZATIONS                                         */
/***********************************************************/
init();                                             // Initialize SFR
TMR1IE = 1;                                         // Timer 1 interrupt enable
ADIE = 1;                                           // AtD intrrupt enable
PEIE = 1;                                           // enable all peripheral interrupts
ei();                                               // enable all interrupts
adc_init();                                         // initialize the Atd module
//timer_set(TIMER_2,1);                               // Set this initially
//spi_fram();
/************************************************************/
/*  MAIN LOOP OVERALL                                       */
/************************************************************/
while(1)                   
 { 
  if (TO == 1)                            //STATUS reg, bit 4 1=After power up,CLRWDT or sleep instruction 0=A WDT time-out occured 
   {
    
    fram_write(&FRAM_DATA2,0,7);                /*Write to the fram i know this works 23NOV2011 Using an external variable from spi_func.c*/
    //fram_read(FRAM_DATA_READ,0xF9,14);       /* read from and store to array from fram*/
    //asm("nop");
    //spi_fram();                                                    
    //spi_txdata();                         // TRIGGER FUNCTION NOW             
    while (BF == 1)                       //see what is in the buffer from a receive SSPBUF
     {
     dummy = SSPBUF; 
     }  
     CLRWDT();                               // clear if WD timed out
     }
   else
      {
      
      CLRWDT();                                 // Clear watchdog timer
      } 
  }
 } 


 void init(void)
{
OPTION     = 0x80;       /*|PSA=Prescale to WDT|most prescale rate|*/
TRISA      = 0x01;       /* Setup for analog input RA0*/
TRISD      = 0x00;       /* port directions: 1=input, 0=output*/
PORTD      = 0xFF;       /* port D all off*/ 
PIE1       = 0x01;       /* TMR1IE = enabled*/ 
INTCON     = 0x40;       /* PEIE = enabled*/
T1CON      = 0x35;       /* T1CKPS1,T1CKPS0 = 1:8 prescaler,T1SYNC = do not synchronise external clock input ,TMR1ON = enabled*/ 
         /* Make sure to enable SCK,SDO,SDI and SS pin here */  
TRISC      = 0x14;       /* |bit 7|bit 6|bit 5 SD0=0|bit 4 SS=1|bit 3 master mode =0|bit 2| bit 1 | bit 0 |*/   
SSPSTAT    = 0x40;       /* |SMP=data sampled at end|CKE = 1 Xmit occurs Active to Idle a neg edge|everything else I2C stuff*/
SSPCON     = 0x21;       /* |WCOL = |SSPOV = 0 slave mode only|SSPEN = 1 enable SCK,SD),SDI,SS as serial pins |CKP=0|SSPM3|SSPM2|SSPM1|SSPM0*/

}


