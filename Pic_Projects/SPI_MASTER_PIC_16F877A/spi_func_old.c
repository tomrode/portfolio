#include <htc.h>
#include "spi_func.h"
#include "timer.h"
#include "adc_func.h"

#define   SPI_TXOUT   (1)      


void spi_txdata(void)
{
if(get_timer(TIMER_1) == 0)
 {
 PORTD  = 0xFD;               //RD1 pin 20 = 0 Make this the SS enable 
 SSPBUF = linear_motor();     // Send this out the spi buffer 
 WCOL   = 1;                  //The SSPBUF reg is written while it is trasmitting the previous word. THis must be cleared
 timer_set(TIMER_1,SPI_TXOUT); // Set timer initially
 }
 else
  {
  PORTD = 0xFF;               // send SS to high shut ooff comm               
  } 
}