opt subtitle "HI-TECH Software Omniscient Code Generator (Lite mode) build 6738"

opt pagewidth 120

	opt lm

	processor	16F877A
clrc	macro
	bcf	3,0
	endm
clrz	macro
	bcf	3,2
	endm
setc	macro
	bsf	3,0
	endm
setz	macro
	bsf	3,2
	endm
skipc	macro
	btfss	3,0
	endm
skipz	macro
	btfss	3,2
	endm
skipnc	macro
	btfsc	3,0
	endm
skipnz	macro
	btfsc	3,2
	endm
indf	equ	0
indf0	equ	0
pc	equ	2
pcl	equ	2
status	equ	3
fsr	equ	4
fsr0	equ	4
c	equ	1
z	equ	0
pclath	equ	10
# 22 "G:\pic_projects\SPI_MASTER_PIC_16F877A\main.c"
	psect config,class=CONFIG,delta=2 ;#
# 22 "G:\pic_projects\SPI_MASTER_PIC_16F877A\main.c"
	dw 0x3FFB & 0x3FFF & 0x3FBF & 0x3FFF & 0x3FFF & 0x3FFF & 0x3FFF & 0x3FFF & 0x3FFE ;#
	FNCALL	_main,_init
	FNCALL	_main,_adc_init
	FNCALL	_main,_fram_write
	FNROOT	_main
	FNCALL	_interrupt_handler,_timer_isr
	FNCALL	_interrupt_handler,_adc_isr
	FNCALL	intlevel1,_interrupt_handler
	global	intlevel1
	FNROOT	intlevel1
	global	main@F579
	global	_FRAM_DATA
	global	_FRAM_DATA2
psect	idataBANK0,class=CODE,space=0,delta=2
global __pidataBANK0
__pidataBANK0:
	file	"G:\pic_projects\SPI_MASTER_PIC_16F877A\main.c"
	line	47

;initializer for main@F579
	retlw	042h
	retlw	041h
	retlw	044h
	retlw	042h
	retlw	045h
	retlw	045h
	retlw	046h
	retlw	04Dh
	retlw	041h
	retlw	049h
	retlw	04Eh
	file	"G:\pic_projects\SPI_MASTER_PIC_16F877A\spi_func.c"
	line	51

;initializer for _FRAM_DATA
	retlw	04Dh
	retlw	059h
	retlw	04Eh
	retlw	041h
	retlw	04Dh
	retlw	045h
	retlw	049h
	retlw	053h
	retlw	054h
	retlw	04Fh
	retlw	04Dh
	retlw	052h
	retlw	04Fh
	retlw	044h
	retlw	045h
	line	72

;initializer for _FRAM_DATA2
	retlw	04Eh
	retlw	04Fh
	retlw	054h
	retlw	048h
	retlw	049h
	retlw	04Eh
	retlw	047h
	global	_motor_speed_array
psect	strings,class=STRING,delta=2
global __pstrings
__pstrings:
;	global	stringdir,stringtab,__stringbase
stringtab:
;	String table - string pointers are 1 byte each
stringcode:stringdir:
movlw high(stringdir)
movwf pclath
movf fsr,w
incf fsr
	addwf pc
__stringbase:
	retlw	0
psect	strings
	file	"G:\pic_projects\SPI_MASTER_PIC_16F877A\adc_func.c"
	line	23
_motor_speed_array:
	retlw	0Ah
	retlw	0

	retlw	0Bh
	retlw	0

	retlw	0Ch
	retlw	0

	retlw	0Dh
	retlw	0

	retlw	0Fh
	retlw	0

	retlw	011h
	retlw	0

	retlw	013h
	retlw	0

	retlw	017h
	retlw	0

	retlw	01Ch
	retlw	0

	retlw	024h
	retlw	0

	retlw	032h
	retlw	0

	retlw	053h
	retlw	0

	retlw	0FAh
	retlw	0

	retlw	0A1h
	retlw	01h

	retlw	071h
	retlw	02h

	retlw	088h
	retlw	013h

	global	_FRAM_ADDRESS
psect	strings
	file	"G:\pic_projects\SPI_MASTER_PIC_16F877A\spi_func.c"
	line	35
_FRAM_ADDRESS:
	retlw	0
	retlw	0

	retlw	01h
	retlw	0

	retlw	02h
	retlw	0

	retlw	03h
	retlw	0

	retlw	04h
	retlw	0

	retlw	05h
	retlw	0

	retlw	06h
	retlw	0

	retlw	07h
	retlw	0

	retlw	08h
	retlw	0

	retlw	09h
	retlw	0

	retlw	0Ah
	retlw	0

	global	_FRAM_OPCODES
psect	strings
	file	"G:\pic_projects\SPI_MASTER_PIC_16F877A\spi_func.c"
	line	25
_FRAM_OPCODES:
	retlw	06h
	retlw	04h
	retlw	05h
	retlw	01h
	retlw	03h
	retlw	02h
	global	_motor_speed_array
	global	_FRAM_ADDRESS
	global	_FRAM_OPCODES
	global	_timer_array
	global	_adc_isr_flag
	global	_int_count
	global	linear_motor@adc_match_count
	global	linear_motor@old_raw_adc
	global	linear_motor@raw_adc
	global	_teststruct
	global	_tmr1_isr_counter
	global	_toms_variable
	global	spi_fram@fram_state_machine
	global	_FRAM_DATA_READ
	global	_watch_dog_count
psect	nvBANK0,class=BANK0,space=1
global __pnvBANK0
__pnvBANK0:
_watch_dog_count:
       ds      2

	global	_ADCON0
_ADCON0	set	31
	global	_ADRESH
_ADRESH	set	30
	global	_INTCON
_INTCON	set	11
	global	_PORTC
_PORTC	set	7
	global	_PORTD
_PORTD	set	8
	global	_SSPBUF
_SSPBUF	set	19
	global	_SSPCON
_SSPCON	set	20
	global	_T1CON
_T1CON	set	16
	global	_TMR1H
_TMR1H	set	15
	global	_TMR1L
_TMR1L	set	14
	global	_ADIF
_ADIF	set	102
	global	_GIE
_GIE	set	95
	global	_GODONE
_GODONE	set	250
	global	_PEIE
_PEIE	set	94
	global	_RD1
_RD1	set	65
	global	_SSPIF
_SSPIF	set	99
	global	_SSPOV
_SSPOV	set	166
	global	_TMR1IF
_TMR1IF	set	96
	global	_TO
_TO	set	28
	global	_WCOL
_WCOL	set	167
	global	_ADCON1
_ADCON1	set	159
	global	_ADRESL
_ADRESL	set	158
	global	_OPTION
_OPTION	set	129
	global	_PIE1
_PIE1	set	140
	global	_SSPSTAT
_SSPSTAT	set	148
	global	_TRISA
_TRISA	set	133
	global	_TRISC
_TRISC	set	135
	global	_TRISD
_TRISD	set	136
	global	_ADIE
_ADIE	set	1126
	global	_BF
_BF	set	1184
	global	_TMR1IE
_TMR1IE	set	1120
	file	"SPI_MASTER.as"
	line	#
psect cinit,class=CODE,delta=2
global start_initialization
start_initialization:

psect	bssCOMMON,class=COMMON,space=1
global __pbssCOMMON
__pbssCOMMON:
linear_motor@raw_adc:
       ds      2

_teststruct:
       ds      1

_tmr1_isr_counter:
       ds      1

_toms_variable:
       ds      1

spi_fram@fram_state_machine:
       ds      1

psect	bssBANK0,class=BANK0,space=1
global __pbssBANK0
__pbssBANK0:
_timer_array:
       ds      4

_adc_isr_flag:
       ds      2

_int_count:
       ds      2

linear_motor@adc_match_count:
       ds      2

linear_motor@old_raw_adc:
       ds      2

psect	dataBANK0,class=BANK0,space=1
global __pdataBANK0
__pdataBANK0:
	file	"G:\pic_projects\SPI_MASTER_PIC_16F877A\main.c"
	line	47
main@F579:
       ds      11

psect	dataBANK0
	file	"G:\pic_projects\SPI_MASTER_PIC_16F877A\spi_func.c"
	line	51
_FRAM_DATA:
       ds      15

psect	dataBANK0
	file	"G:\pic_projects\SPI_MASTER_PIC_16F877A\spi_func.c"
	line	72
_FRAM_DATA2:
       ds      7

psect	bssBANK1,class=BANK1,space=1
global __pbssBANK1
__pbssBANK1:
_FRAM_DATA_READ:
       ds      15

psect clrtext,class=CODE,delta=2
global clear_ram
;	Called with FSR containing the base address, and
;	W with the last address+1
clear_ram:
	clrwdt			;clear the watchdog before getting into this loop
clrloop:
	clrf	indf		;clear RAM location pointed to by FSR
	incf	fsr,f		;increment pointer
	xorwf	fsr,w		;XOR with final address
	btfsc	status,2	;have we reached the end yet?
	retlw	0		;all done for this memory range, return
	xorwf	fsr,w		;XOR again to restore value
	goto	clrloop		;do the next byte

; Clear objects allocated to COMMON
psect cinit,class=CODE,delta=2
	clrf	((__pbssCOMMON)+0)&07Fh
	clrf	((__pbssCOMMON)+1)&07Fh
	clrf	((__pbssCOMMON)+2)&07Fh
	clrf	((__pbssCOMMON)+3)&07Fh
	clrf	((__pbssCOMMON)+4)&07Fh
	clrf	((__pbssCOMMON)+5)&07Fh
; Clear objects allocated to BANK0
psect cinit,class=CODE,delta=2
	bcf	status, 7	;select IRP bank0
	movlw	low(__pbssBANK0)
	movwf	fsr
	movlw	low((__pbssBANK0)+0Ch)
	fcall	clear_ram
; Clear objects allocated to BANK1
psect cinit,class=CODE,delta=2
	movlw	low(__pbssBANK1)
	movwf	fsr
	movlw	low((__pbssBANK1)+0Fh)
	fcall	clear_ram
global btemp
psect inittext,class=CODE,delta=2
global init_fetch,btemp
;	Called with low address in FSR and high address in W
init_fetch:
	movf btemp,w
	movwf pclath
	movf btemp+1,w
	movwf pc
global init_ram
;Called with:
;	high address of idata address in btemp 
;	low address of idata address in btemp+1 
;	low address of data in FSR
;	high address + 1 of data in btemp-1
init_ram:
	fcall init_fetch
	movwf indf,f
	incf fsr,f
	movf fsr,w
	xorwf btemp-1,w
	btfsc status,2
	retlw 0
	incf btemp+1,f
	btfsc status,2
	incf btemp,f
	goto init_ram
; Initialize objects allocated to BANK0
psect cinit,class=CODE,delta=2
global init_ram, __pidataBANK0
	movlw low(__pdataBANK0+33)
	movwf btemp-1,f
	movlw high(__pidataBANK0)
	movwf btemp,f
	movlw low(__pidataBANK0)
	movwf btemp+1,f
	movlw low(__pdataBANK0)
	movwf fsr,f
	fcall init_ram
psect cinit,class=CODE,delta=2
global end_of_initialization

;End of C runtime variable initialization code

end_of_initialization:
clrf status
ljmp _main	;jump to C main() function
psect	cstackCOMMON,class=COMMON,space=1
global __pcstackCOMMON
__pcstackCOMMON:
	global	?_init
?_init:	; 0 bytes @ 0x0
	global	?_adc_init
?_adc_init:	; 0 bytes @ 0x0
	global	?_timer_isr
?_timer_isr:	; 0 bytes @ 0x0
	global	??_timer_isr
??_timer_isr:	; 0 bytes @ 0x0
	global	?_adc_isr
?_adc_isr:	; 0 bytes @ 0x0
	global	??_adc_isr
??_adc_isr:	; 0 bytes @ 0x0
	global	?_interrupt_handler
?_interrupt_handler:	; 0 bytes @ 0x0
	global	?_main
?_main:	; 2 bytes @ 0x0
	ds	1
	global	timer_isr@i
timer_isr@i:	; 1 bytes @ 0x1
	ds	1
	global	??_interrupt_handler
??_interrupt_handler:	; 0 bytes @ 0x2
	ds	4
psect	cstackBANK0,class=BANK0,space=1
global __pcstackBANK0
__pcstackBANK0:
	global	??_init
??_init:	; 0 bytes @ 0x0
	global	??_adc_init
??_adc_init:	; 0 bytes @ 0x0
	global	?_fram_write
?_fram_write:	; 0 bytes @ 0x0
	global	fram_write@address
fram_write@address:	; 2 bytes @ 0x0
	ds	2
	global	fram_write@count
fram_write@count:	; 2 bytes @ 0x2
	ds	2
	global	??_fram_write
??_fram_write:	; 0 bytes @ 0x4
	ds	2
	global	fram_write@test
fram_write@test:	; 2 bytes @ 0x6
	ds	2
	global	fram_write@FRAM_DATA
fram_write@FRAM_DATA:	; 1 bytes @ 0x8
	ds	1
	global	fram_write@index
fram_write@index:	; 1 bytes @ 0x9
	ds	1
	global	??_main
??_main:	; 0 bytes @ 0xA
	ds	4
	global	main@FRAM_DATA_MAIN
main@FRAM_DATA_MAIN:	; 11 bytes @ 0xE
	ds	11
	global	main@dummy
main@dummy:	; 1 bytes @ 0x19
	ds	1
	global	main@ptr
main@ptr:	; 1 bytes @ 0x1A
	ds	1
;;Data sizes: Strings 0, constant 60, data 33, bss 33, persistent 2 stack 0
;;Auto spaces:   Size  Autos    Used
;; COMMON          14      6      12
;; BANK0           80     27      74
;; BANK1           80      0      15
;; BANK3           96      0       0
;; BANK2           96      0       0

;;
;; Pointer list with targets:

;; fram_write@FRAM_DATA	PTR unsigned char  size(1) Largest target is 7
;;		 -> FRAM_DATA2(BANK0[7]), 
;;
;; main@ptr	PTR unsigned char  size(1) Largest target is 11
;;		 -> main@FRAM_DATA_MAIN(BANK0[11]), 
;;


;;
;; Critical Paths under _main in COMMON
;;
;;   None.
;;
;; Critical Paths under _interrupt_handler in COMMON
;;
;;   _interrupt_handler->_timer_isr
;;
;; Critical Paths under _main in BANK0
;;
;;   _main->_fram_write
;;
;; Critical Paths under _interrupt_handler in BANK0
;;
;;   None.
;;
;; Critical Paths under _main in BANK1
;;
;;   None.
;;
;; Critical Paths under _interrupt_handler in BANK1
;;
;;   None.
;;
;; Critical Paths under _main in BANK3
;;
;;   None.
;;
;; Critical Paths under _interrupt_handler in BANK3
;;
;;   None.
;;
;; Critical Paths under _main in BANK2
;;
;;   None.
;;
;; Critical Paths under _interrupt_handler in BANK2
;;
;;   None.

;;
;;Main: autosize = 0, tempsize = 4, incstack = 0, save=0
;;

;;
;;Call Graph Tables:
;;
;; ---------------------------------------------------------------------------------
;; (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;; ---------------------------------------------------------------------------------
;; (0) _main                                                17    17      0     269
;;                                             10 BANK0     17    17      0
;;                               _init
;;                           _adc_init
;;                         _fram_write
;; ---------------------------------------------------------------------------------
;; (1) _fram_write                                          10     6      4     226
;;                                              0 BANK0     10     6      4
;; ---------------------------------------------------------------------------------
;; (1) _adc_init                                             0     0      0       0
;; ---------------------------------------------------------------------------------
;; (1) _init                                                 0     0      0       0
;; ---------------------------------------------------------------------------------
;; Estimated maximum stack depth 1
;; ---------------------------------------------------------------------------------
;; (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;; ---------------------------------------------------------------------------------
;; (2) _interrupt_handler                                    4     4      0      90
;;                                              2 COMMON     4     4      0
;;                          _timer_isr
;;                            _adc_isr
;; ---------------------------------------------------------------------------------
;; (3) _adc_isr                                              0     0      0       0
;; ---------------------------------------------------------------------------------
;; (3) _timer_isr                                            2     2      0      90
;;                                              0 COMMON     2     2      0
;; ---------------------------------------------------------------------------------
;; Estimated maximum stack depth 3
;; ---------------------------------------------------------------------------------

;; Call Graph Graphs:

;; _main (ROOT)
;;   _init
;;   _adc_init
;;   _fram_write
;;
;; _interrupt_handler (ROOT)
;;   _timer_isr
;;   _adc_isr
;;

;; Address spaces:

;;Name               Size   Autos  Total    Cost      Usage
;;BANK3               60      0       0       9        0.0%
;;BITBANK3            60      0       0       8        0.0%
;;SFR3                 0      0       0       4        0.0%
;;BITSFR3              0      0       0       4        0.0%
;;BANK2               60      0       0      11        0.0%
;;BITBANK2            60      0       0      10        0.0%
;;SFR2                 0      0       0       5        0.0%
;;BITSFR2              0      0       0       5        0.0%
;;SFR1                 0      0       0       2        0.0%
;;BITSFR1              0      0       0       2        0.0%
;;BANK1               50      0       F       7       18.8%
;;BITBANK1            50      0       0       6        0.0%
;;CODE                 0      0       0       0        0.0%
;;DATA                 0      0      6A      12        0.0%
;;ABS                  0      0      65       3        0.0%
;;NULL                 0      0       0       0        0.0%
;;STACK                0      0       5       2        0.0%
;;BANK0               50     1B      4A       5       92.5%
;;BITBANK0            50      0       0       4        0.0%
;;SFR0                 0      0       0       1        0.0%
;;BITSFR0              0      0       0       1        0.0%
;;COMMON               E      6       C       1       85.7%
;;BITCOMMON            E      0       0       0        0.0%
;;EEDATA             100      0       0       0        0.0%

	global	_main
psect	maintext,global,class=CODE,delta=2
global __pmaintext
__pmaintext:

;; *************** function _main *****************
;; Defined at:
;;		line 40 in file "G:\pic_projects\SPI_MASTER_PIC_16F877A\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  FRAM_DATA_MA   11   14[BANK0 ] unsigned char [11]
;;  ptr             1   26[BANK0 ] PTR unsigned char 
;;		 -> main@FRAM_DATA_MAIN(11), 
;;  dummy           1   25[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;                  2  572[COMMON] int 
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 17F/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0      13       0       0       0
;;      Temps:          0       4       0       0       0
;;      Totals:         0      17       0       0       0
;;Total ram usage:       17 bytes
;; Hardware stack levels required when called:    3
;; This function calls:
;;		_init
;;		_adc_init
;;		_fram_write
;; This function is called by:
;;		Startup code after reset
;; This function uses a non-reentrant model
;;
psect	maintext
	file	"G:\pic_projects\SPI_MASTER_PIC_16F877A\main.c"
	line	40
	global	__size_of_main
	__size_of_main	equ	__end_of_main-_main
	
_main:	
	opt	stack 5
; Regs used in _main: [wreg-fsr0h+status,2+status,0+pclath+cstack]
	line	44
	
l4779:	
;main.c: 44: watch_dog_count=0;
	movlw	low(0)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(_watch_dog_count)
	movlw	high(0)
	movwf	((_watch_dog_count))+1
	line	47
	
l4781:	
;main.c: 45: uint8_t dummy;
;main.c: 46: uint8_t *ptr;
;main.c: 47: uint8_t FRAM_DATA_MAIN[] = {'B','A','D','B','E','E','F','M','A','I','N'};
	movlw	(main@FRAM_DATA_MAIN)&0ffh
	movwf	fsr0
	movlw	low(main@F579)
	movwf	(??_main+0)+0
	movf	fsr0,w
	movwf	((??_main+0)+0+1)
	movlw	11
	movwf	((??_main+0)+0+2)
u2900:
	movf	(??_main+0)+0,w
	movwf	fsr0
	bcf	status, 7	;select IRP bank0
	
	movf	indf,w
	movwf	((??_main+0)+0+3)
	incf	(??_main+0)+0,f
	movf	((??_main+0)+0+1),w
	movwf	fsr0
	
	movf	((??_main+0)+0+3),w
	movwf	indf
	incf	((??_main+0)+0+1),f
	decfsz	((??_main+0)+0+2),f
	goto	u2900
	line	49
	
l4783:	
;main.c: 49: ptr = &FRAM_DATA_MAIN[0];
	movlw	(main@FRAM_DATA_MAIN)&0ffh
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(main@ptr)
	line	50
	
l4785:	
;main.c: 50: (*ptr++);
	movlw	(01h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	addwf	(main@ptr),f
	line	54
	
l4787:	
;main.c: 54: init();
	fcall	_init
	line	55
	
l4789:	
;main.c: 55: TMR1IE = 1;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bsf	(1120/8)^080h,(1120)&7
	line	56
	
l4791:	
;main.c: 56: ADIE = 1;
	bsf	(1126/8)^080h,(1126)&7
	line	57
	
l4793:	
;main.c: 57: PEIE = 1;
	bsf	(94/8),(94)&7
	line	58
	
l4795:	
;main.c: 58: (GIE = 1);
	bsf	(95/8),(95)&7
	line	59
	
l4797:	
;main.c: 59: adc_init();
	fcall	_adc_init
	goto	l4799
	line	65
;main.c: 65: while(1)
	
l575:	
	line	67
	
l4799:	
;main.c: 66: {
;main.c: 67: if (TO == 1)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfss	(28/8),(28)&7
	goto	u2911
	goto	u2910
u2911:
	goto	l576
u2910:
	line	70
	
l4801:	
;main.c: 68: {
;main.c: 70: fram_write(&FRAM_DATA2,0,7);
	movlw	low(0)
	movwf	(?_fram_write)
	movlw	high(0)
	movwf	((?_fram_write))+1
	movlw	low(07h)
	movwf	0+(?_fram_write)+02h
	movlw	high(07h)
	movwf	(0+(?_fram_write)+02h)+1
	movlw	(_FRAM_DATA2)&0ffh
	fcall	_fram_write
	line	75
;main.c: 75: while (BF == 1)
	goto	l4805
	
l578:	
	line	77
	
l4803:	
;main.c: 76: {
;main.c: 77: dummy = SSPBUF;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(19),w	;volatile
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(main@dummy)
	goto	l4805
	line	78
	
l577:	
	line	75
	
l4805:	
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	btfsc	(1184/8)^080h,(1184)&7
	goto	u2921
	goto	u2920
u2921:
	goto	l4803
u2920:
	
l579:	
	line	79
# 79 "G:\pic_projects\SPI_MASTER_PIC_16F877A\main.c"
clrwdt ;#
psect	maintext
	line	80
;main.c: 80: }
	goto	l4799
	line	81
	
l576:	
	line	84
# 84 "G:\pic_projects\SPI_MASTER_PIC_16F877A\main.c"
clrwdt ;#
psect	maintext
	goto	l4799
	line	85
	
l580:	
	goto	l4799
	line	86
	
l581:	
	line	65
	goto	l4799
	
l582:	
	line	87
	
l583:	
	global	start
	ljmp	start
	opt stack 0
GLOBAL	__end_of_main
	__end_of_main:
;; =============== function _main ends ============

	signat	_main,90
	global	_fram_write
psect	text315,local,class=CODE,delta=2
global __ptext315
__ptext315:

;; *************** function _fram_write *****************
;; Defined at:
;;		line 230 in file "G:\pic_projects\SPI_MASTER_PIC_16F877A\spi_func.c"
;; Parameters:    Size  Location     Type
;;  FRAM_DATA       1    wreg     PTR unsigned char 
;;		 -> FRAM_DATA2(7), 
;;  address         2    0[BANK0 ] unsigned short 
;;  count           2    2[BANK0 ] unsigned short 
;; Auto vars:     Size  Location     Type
;;  FRAM_DATA       1    8[BANK0 ] PTR unsigned char 
;;		 -> FRAM_DATA2(7), 
;;  test            2    6[BANK0 ] unsigned short 
;;  index           1    9[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       4       0       0       0
;;      Locals:         0       4       0       0       0
;;      Temps:          0       2       0       0       0
;;      Totals:         0      10       0       0       0
;;Total ram usage:       10 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text315
	file	"G:\pic_projects\SPI_MASTER_PIC_16F877A\spi_func.c"
	line	230
	global	__size_of_fram_write
	__size_of_fram_write	equ	__end_of_fram_write-_fram_write
	
_fram_write:	
	opt	stack 5
; Regs used in _fram_write: [wreg-fsr0h+status,2+status,0]
;fram_write@FRAM_DATA stored from wreg
	line	233
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(fram_write@FRAM_DATA)
	
l4745:	
;spi_func.c: 231: uint8_t index;
;spi_func.c: 232: uint16_t test;
;spi_func.c: 233: test = sizeof(test);
	movlw	low(02h)
	movwf	(fram_write@test)
	movlw	high(02h)
	movwf	((fram_write@test))+1
	line	234
;spi_func.c: 234: if (count != 0)
	movf	(fram_write@count+1),w
	iorwf	(fram_write@count),w
	skipnz
	goto	u2861
	goto	u2860
u2861:
	goto	l2366
u2860:
	line	237
	
l4747:	
;spi_func.c: 235: {
;spi_func.c: 237: RD1 = 0;
	bcf	(65/8),(65)&7
	line	238
;spi_func.c: 238: SSPIF = 0;
	bcf	(99/8),(99)&7
	line	239
	
l4749:	
;spi_func.c: 239: SSPBUF = (0x06);
	movlw	(06h)
	movwf	(19)	;volatile
	line	243
	
l4751:	
;spi_func.c: 243: RD1 = 1;
	bsf	(65/8),(65)&7
	line	244
	
l4753:	
;spi_func.c: 244: WCOL = 0;
	bcf	(167/8),(167)&7
	line	245
	
l4755:	
;spi_func.c: 245: SSPOV = 0;
	bcf	(166/8),(166)&7
	line	248
	
l4757:	
;spi_func.c: 248: RD1 = 0;
	bcf	(65/8),(65)&7
	line	249
	
l4759:	
;spi_func.c: 249: SSPIF = 0;
	bcf	(99/8),(99)&7
	line	250
	
l4761:	
;spi_func.c: 250: if (address & 0x100)
	btfss	(fram_write@address+1),(8)&7
	goto	u2871
	goto	u2870
u2871:
	goto	l4765
u2870:
	line	252
	
l4763:	
;spi_func.c: 251: {
;spi_func.c: 252: SSPBUF = (0x02) | (0x08);
	movlw	(0Ah)
	movwf	(19)	;volatile
	line	253
;spi_func.c: 253: }
	goto	l4767
	line	254
	
l2358:	
	line	256
	
l4765:	
;spi_func.c: 254: else
;spi_func.c: 255: {
;spi_func.c: 256: SSPBUF = (0x02);
	movlw	(02h)
	movwf	(19)	;volatile
	goto	l4767
	line	257
	
l2359:	
	line	263
	
l4767:	
;spi_func.c: 257: }
;spi_func.c: 263: SSPIF = 0;
	bcf	(99/8),(99)&7
	line	264
;spi_func.c: 264: SSPBUF = (uint8_t)(address);
	movf	(fram_write@address),w
	movwf	(19)	;volatile
	line	269
	
l4769:	
;spi_func.c: 269: for (index = 0; index < count; index++)
	clrc
	movlw	0
	btfsc	status,0
	movlw	1
	movwf	(fram_write@index)
	goto	l4775
	line	270
	
l2361:	
	line	271
;spi_func.c: 270: {
;spi_func.c: 271: SSPIF = 0;
	bcf	(99/8),(99)&7
	line	272
	
l4771:	
;spi_func.c: 272: SSPBUF = FRAM_DATA[index];
	movf	(fram_write@index),w
	addwf	(fram_write@FRAM_DATA),w
	movwf	(??_fram_write+0)+0
	movf	0+(??_fram_write+0)+0,w
	movwf	fsr0
	bcf	status, 7	;select IRP bank0
	movf	indf,w
	movwf	(19)	;volatile
	line	269
	
l4773:	
	movlw	(01h)
	movwf	(??_fram_write+0)+0
	movf	(??_fram_write+0)+0,w
	addwf	(fram_write@index),f
	goto	l4775
	
l2360:	
	
l4775:	
	movf	(fram_write@index),w
	movwf	(??_fram_write+0)+0
	clrf	(??_fram_write+0)+0+1
	movf	(fram_write@count+1),w
	subwf	1+(??_fram_write+0)+0,w
	skipz
	goto	u2885
	movf	(fram_write@count),w
	subwf	0+(??_fram_write+0)+0,w
u2885:
	skipc
	goto	u2881
	goto	u2880
u2881:
	goto	l2361
u2880:
	
l2362:	
	line	277
;spi_func.c: 276: }
;spi_func.c: 277: RD1 = 1;
	bsf	(65/8),(65)&7
	line	280
;spi_func.c: 280: RD1 = 0;
	bcf	(65/8),(65)&7
	line	281
;spi_func.c: 281: SSPIF = 0;
	bcf	(99/8),(99)&7
	line	282
	
l4777:	
;spi_func.c: 282: SSPBUF = (0x04);
	movlw	(04h)
	movwf	(19)	;volatile
	line	283
;spi_func.c: 283: while (!SSPIF)
	goto	l2363
	
l2364:	
	line	285
;spi_func.c: 284: {
	
l2363:	
	line	283
	btfss	(99/8),(99)&7
	goto	u2891
	goto	u2890
u2891:
	goto	l2363
u2890:
	
l2365:	
	line	286
;spi_func.c: 285: }
;spi_func.c: 286: RD1 = 1;
	bsf	(65/8),(65)&7
	goto	l2366
	line	287
	
l2357:	
	line	288
	
l2366:	
	return
	opt stack 0
GLOBAL	__end_of_fram_write
	__end_of_fram_write:
;; =============== function _fram_write ends ============

	signat	_fram_write,12408
	global	_adc_init
psect	text316,local,class=CODE,delta=2
global __ptext316
__ptext316:

;; *************** function _adc_init *****************
;; Defined at:
;;		line 140 in file "G:\pic_projects\SPI_MASTER_PIC_16F877A\adc_func.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text316
	file	"G:\pic_projects\SPI_MASTER_PIC_16F877A\adc_func.c"
	line	140
	global	__size_of_adc_init
	__size_of_adc_init	equ	__end_of_adc_init-_adc_init
	
_adc_init:	
	opt	stack 5
; Regs used in _adc_init: [wreg]
	line	141
	
l4743:	
;adc_func.c: 141: ADCON0 = 0xC5;
	movlw	(0C5h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(31)	;volatile
	line	142
;adc_func.c: 142: ADCON1 = 0x80;
	movlw	(080h)
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movwf	(159)^080h	;volatile
	line	143
	
l2973:	
	return
	opt stack 0
GLOBAL	__end_of_adc_init
	__end_of_adc_init:
;; =============== function _adc_init ends ============

	signat	_adc_init,88
	global	_init
psect	text317,local,class=CODE,delta=2
global __ptext317
__ptext317:

;; *************** function _init *****************
;; Defined at:
;;		line 91 in file "G:\pic_projects\SPI_MASTER_PIC_16F877A\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text317
	file	"G:\pic_projects\SPI_MASTER_PIC_16F877A\main.c"
	line	91
	global	__size_of_init
	__size_of_init	equ	__end_of_init-_init
	
_init:	
	opt	stack 5
; Regs used in _init: [wreg+status,2]
	line	92
	
l4725:	
;main.c: 92: OPTION = 0x80;
	movlw	(080h)
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movwf	(129)^080h	;volatile
	line	93
;main.c: 93: TRISA = 0x01;
	movlw	(01h)
	movwf	(133)^080h	;volatile
	line	94
	
l4727:	
;main.c: 94: TRISD = 0x00;
	clrf	(136)^080h	;volatile
	line	95
	
l4729:	
;main.c: 95: PORTD = 0xFF;
	movlw	(0FFh)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(8)	;volatile
	line	96
	
l4731:	
;main.c: 96: PIE1 = 0x01;
	movlw	(01h)
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movwf	(140)^080h	;volatile
	line	97
	
l4733:	
;main.c: 97: INTCON = 0x40;
	movlw	(040h)
	movwf	(11)	;volatile
	line	98
	
l4735:	
;main.c: 98: T1CON = 0x35;
	movlw	(035h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(16)	;volatile
	line	100
	
l4737:	
;main.c: 100: TRISC = 0x14;
	movlw	(014h)
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movwf	(135)^080h	;volatile
	line	101
	
l4739:	
;main.c: 101: SSPSTAT = 0x40;
	movlw	(040h)
	movwf	(148)^080h	;volatile
	line	102
	
l4741:	
;main.c: 102: SSPCON = 0x21;
	movlw	(021h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(20)	;volatile
	line	104
	
l586:	
	return
	opt stack 0
GLOBAL	__end_of_init
	__end_of_init:
;; =============== function _init ends ============

	signat	_init,88
	global	_interrupt_handler
psect	text318,local,class=CODE,delta=2
global __ptext318
__ptext318:

;; *************** function _interrupt_handler *****************
;; Defined at:
;;		line 7 in file "G:\pic_projects\SPI_MASTER_PIC_16F877A\interrupt.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          4       0       0       0       0
;;      Totals:         4       0       0       0       0
;;Total ram usage:        4 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		_timer_isr
;;		_adc_isr
;; This function is called by:
;;		Interrupt level 1
;; This function uses a non-reentrant model
;;
psect	text318
	file	"G:\pic_projects\SPI_MASTER_PIC_16F877A\interrupt.c"
	line	7
	global	__size_of_interrupt_handler
	__size_of_interrupt_handler	equ	__end_of_interrupt_handler-_interrupt_handler
	
_interrupt_handler:	
	opt	stack 5
; Regs used in _interrupt_handler: [wreg-fsr0h+status,2+status,0+pclath+cstack]
psect	intentry,class=CODE,delta=2
global __pintentry
__pintentry:
global interrupt_function
interrupt_function:
	global saved_w
	saved_w	set	btemp+0
	movwf	saved_w
	movf	status,w
	movwf	(??_interrupt_handler+0)
	movf	fsr0,w
	movwf	(??_interrupt_handler+1)
	movf	pclath,w
	movwf	(??_interrupt_handler+2)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	btemp+1,w
	movwf	(??_interrupt_handler+3)
	ljmp	_interrupt_handler
psect	text318
	line	8
	
i1l4503:	
;interrupt.c: 8: timer_isr();
	fcall	_timer_isr
	line	9
	
i1l4505:	
;interrupt.c: 9: adc_isr();
	fcall	_adc_isr
	line	10
	
i1l1153:	
	movf	(??_interrupt_handler+3),w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	btemp+1
	movf	(??_interrupt_handler+2),w
	movwf	pclath
	movf	(??_interrupt_handler+1),w
	movwf	fsr0
	movf	(??_interrupt_handler+0),w
	movwf	status
	swapf	saved_w,f
	swapf	saved_w,w
	retfie
	opt stack 0
GLOBAL	__end_of_interrupt_handler
	__end_of_interrupt_handler:
;; =============== function _interrupt_handler ends ============

	signat	_interrupt_handler,88
	global	_adc_isr
psect	text319,local,class=CODE,delta=2
global __ptext319
__ptext319:

;; *************** function _adc_isr *****************
;; Defined at:
;;		line 146 in file "G:\pic_projects\SPI_MASTER_PIC_16F877A\adc_func.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_interrupt_handler
;; This function uses a non-reentrant model
;;
psect	text319
	file	"G:\pic_projects\SPI_MASTER_PIC_16F877A\adc_func.c"
	line	146
	global	__size_of_adc_isr
	__size_of_adc_isr	equ	__end_of_adc_isr-_adc_isr
	
_adc_isr:	
	opt	stack 5
; Regs used in _adc_isr: [wreg]
	line	147
	
i1l4533:	
;adc_func.c: 147: if ((ADIF)&&(ADIE))
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfss	(102/8),(102)&7
	goto	u262_21
	goto	u262_20
u262_21:
	goto	i1l2978
u262_20:
	
i1l4535:	
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	btfss	(1126/8)^080h,(1126)&7
	goto	u263_21
	goto	u263_20
u263_21:
	goto	i1l2978
u263_20:
	line	150
	
i1l4537:	
;adc_func.c: 148: {
;adc_func.c: 150: ADIF=0;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	bcf	(102/8),(102)&7
	line	151
	
i1l4539:	
;adc_func.c: 151: adc_isr_flag=1;
	movlw	low(01h)
	movwf	(_adc_isr_flag)
	movlw	high(01h)
	movwf	((_adc_isr_flag))+1
	line	152
;adc_func.c: 152: int_count++;
	movlw	low(01h)
	addwf	(_int_count),f
	skipnc
	incf	(_int_count+1),f
	movlw	high(01h)
	addwf	(_int_count+1),f
	line	153
;adc_func.c: 153: }
	goto	i1l2978
	line	154
	
i1l2976:	
	goto	i1l2978
	line	156
;adc_func.c: 154: else
;adc_func.c: 155: {
	
i1l2977:	
	line	157
	
i1l2978:	
	return
	opt stack 0
GLOBAL	__end_of_adc_isr
	__end_of_adc_isr:
;; =============== function _adc_isr ends ============

	signat	_adc_isr,88
	global	_timer_isr
psect	text320,local,class=CODE,delta=2
global __ptext320
__ptext320:

;; *************** function _timer_isr *****************
;; Defined at:
;;		line 54 in file "G:\pic_projects\SPI_MASTER_PIC_16F877A\timer.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  i               1    1[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         1       0       0       0       0
;;      Temps:          1       0       0       0       0
;;      Totals:         2       0       0       0       0
;;Total ram usage:        2 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_interrupt_handler
;; This function uses a non-reentrant model
;;
psect	text320
	file	"G:\pic_projects\SPI_MASTER_PIC_16F877A\timer.c"
	line	54
	global	__size_of_timer_isr
	__size_of_timer_isr	equ	__end_of_timer_isr-_timer_isr
	
_timer_isr:	
	opt	stack 5
; Regs used in _timer_isr: [wreg-fsr0h+status,2+status,0]
	line	56
	
i1l4507:	
;timer.c: 55: uint8_t i;
;timer.c: 56: if((TMR1IE)&&(TMR1IF))
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	btfss	(1120/8)^080h,(1120)&7
	goto	u257_21
	goto	u257_20
u257_21:
	goto	i1l1734
u257_20:
	
i1l4509:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfss	(96/8),(96)&7
	goto	u258_21
	goto	u258_20
u258_21:
	goto	i1l1734
u258_20:
	line	59
	
i1l4511:	
;timer.c: 57: {
;timer.c: 59: TMR1IF=0;
	bcf	(96/8),(96)&7
	line	60
	
i1l4513:	
;timer.c: 60: T1CON &= ~(0x01);
	movlw	(0FEh)
	movwf	(??_timer_isr+0)+0
	movf	(??_timer_isr+0)+0,w
	andwf	(16),f	;volatile
	line	61
	
i1l4515:	
;timer.c: 61: TMR1L = 0x08;
	movlw	(08h)
	movwf	(14)	;volatile
	line	62
	
i1l4517:	
;timer.c: 62: TMR1H = 0xFF;
	movlw	(0FFh)
	movwf	(15)	;volatile
	line	63
	
i1l4519:	
;timer.c: 63: T1CON |= 0x01;
	bsf	(16)+(0/8),(0)&7	;volatile
	line	65
;timer.c: 65: for (i = 0; i < TIMER_MAX; i++)
	clrc
	movlw	0
	btfsc	status,0
	movlw	1
	movwf	(timer_isr@i)
	
i1l4521:	
	movlw	(02h)
	subwf	(timer_isr@i),w
	skipc
	goto	u259_21
	goto	u259_20
u259_21:
	goto	i1l4525
u259_20:
	goto	i1l1734
	
i1l4523:	
	goto	i1l1734
	line	66
	
i1l1729:	
	line	67
	
i1l4525:	
;timer.c: 66: {
;timer.c: 67: if (timer_array[i] != 0)
	movf	(timer_isr@i),w
	movwf	(??_timer_isr+0)+0
	addwf	(??_timer_isr+0)+0,w
	addlw	_timer_array&0ffh
	movwf	fsr0
	bcf	status, 7	;select IRP bank0
	movf	indf,w
	incf	fsr0,f
	iorwf	indf,w
	skipnz
	goto	u260_21
	goto	u260_20
u260_21:
	goto	i1l4529
u260_20:
	line	69
	
i1l4527:	
;timer.c: 68: {
;timer.c: 69: timer_array[i]--;
	movf	(timer_isr@i),w
	movwf	(??_timer_isr+0)+0
	addwf	(??_timer_isr+0)+0,w
	addlw	_timer_array&0ffh
	movwf	fsr0
	movlw	low(01h)
	subwf	indf,f
	incfsz	fsr0,f
	movlw	high(01h)
	skipc
	decf	indf,f
	subwf	indf,f
	decf	fsr0,f
	line	70
;timer.c: 70: }
	goto	i1l4529
	line	71
	
i1l1731:	
	goto	i1l4529
	line	73
;timer.c: 71: else
;timer.c: 72: {
	
i1l1732:	
	line	65
	
i1l4529:	
	movlw	(01h)
	movwf	(??_timer_isr+0)+0
	movf	(??_timer_isr+0)+0,w
	addwf	(timer_isr@i),f
	
i1l4531:	
	movlw	(02h)
	subwf	(timer_isr@i),w
	skipc
	goto	u261_21
	goto	u261_20
u261_21:
	goto	i1l4525
u261_20:
	goto	i1l1734
	
i1l1730:	
	line	76
;timer.c: 73: }
;timer.c: 74: }
;timer.c: 76: }
	goto	i1l1734
	line	77
	
i1l1728:	
	goto	i1l1734
	line	79
;timer.c: 77: else
;timer.c: 78: {
	
i1l1733:	
	line	80
	
i1l1734:	
	return
	opt stack 0
GLOBAL	__end_of_timer_isr
	__end_of_timer_isr:
;; =============== function _timer_isr ends ============

	signat	_timer_isr,88
psect	text321,local,class=CODE,delta=2
global __ptext321
__ptext321:
	global	btemp
	btemp set 07Eh

	DABS	1,126,2	;btemp
	global	wtemp0
	wtemp0 set btemp
	end
