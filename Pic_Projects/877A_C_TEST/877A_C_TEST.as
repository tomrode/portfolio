opt subtitle "HI-TECH Software Omniscient Code Generator (Lite mode) build 6738"

opt pagewidth 120

	opt lm

	processor	16F877A
clrc	macro
	bcf	3,0
	endm
clrz	macro
	bcf	3,2
	endm
setc	macro
	bsf	3,0
	endm
setz	macro
	bsf	3,2
	endm
skipc	macro
	btfss	3,0
	endm
skipz	macro
	btfss	3,2
	endm
skipnc	macro
	btfsc	3,0
	endm
skipnz	macro
	btfsc	3,2
	endm
indf	equ	0
indf0	equ	0
pc	equ	2
pcl	equ	2
status	equ	3
fsr	equ	4
fsr0	equ	4
c	equ	1
z	equ	0
pclath	equ	10
# 12 "C:\pic_projects\877A_C_TEST\main.c"
	psect config,class=CONFIG,delta=2 ;#
# 12 "C:\pic_projects\877A_C_TEST\main.c"
	dw 0x3FFF & 0x3FFF & 0x3FBF & 0x3FFF & 0x3FFF & 0x3FFF & 0x3FFF & 0x3FFF & 0x3FFE ;#
	FNCALL	_main,_eeprom_clear
	FNCALL	_main,_init
	FNCALL	_main,_eeprom_write
	FNCALL	_main,_rd_count
	FNCALL	_rd_count,_eeprom_write
	FNCALL	_eeprom_clear,_eeprom_write
	FNROOT	_main
	FNCALL	intlevel1,_my_isr
	global	intlevel1
	FNROOT	intlevel1
	global	_eeprom_rd1
	global	_eeprom_rd2
	global	_eeprom_rd3
	global	_eeprom_rd4
	global	_eeprom_rd5
	global	_eeprom_rd6
	global	_eeprom_rd7
	global	_eeprom_wdt
	global	_eeprom_main_counter
psect	idataBANK0,class=CODE,space=0,delta=2
global __pidataBANK0
__pidataBANK0:
	file	"C:\pic_projects\877A_C_TEST\main.c"
	line	37

;initializer for _eeprom_rd1
	retlw	01h
	retlw	0

	line	38

;initializer for _eeprom_rd2
	retlw	02h
	retlw	0

	line	39

;initializer for _eeprom_rd3
	retlw	03h
	retlw	0

	line	40

;initializer for _eeprom_rd4
	retlw	04h
	retlw	0

	line	41

;initializer for _eeprom_rd5
	retlw	05h
	retlw	0

	line	42

;initializer for _eeprom_rd6
	retlw	06h
	retlw	0

	line	43

;initializer for _eeprom_rd7
	retlw	07h
	retlw	0

	line	45

;initializer for _eeprom_wdt
	retlw	09h
	retlw	0

psect	idataCOMMON,class=CODE,space=0,delta=2
global __pidataCOMMON
__pidataCOMMON:
	line	44

;initializer for _eeprom_main_counter
	retlw	08h
	retlw	0

	global	_masked_count_rd0
	global	_masked_count_rd1
	global	_masked_count_rd2
	global	_masked_count_rd3
	global	_masked_count_rd4
	global	_masked_count_rd5
	global	_masked_count_rd6
	global	_masked_count_rd7
	global	_rd0_count
	global	_rd1_count
	global	_rd2_count
	global	_rd3_count
	global	_rd4_count
	global	_rd5_count
	global	_rd6_count
	global	_rd7_count
	global	_eeprom_rd0
	global	_counter_flag
	global	_main_counter
	global	_watch_dog_count
psect	nvBANK0,class=BANK0,space=1
global __pnvBANK0
__pnvBANK0:
_watch_dog_count:
       ds      2

	global	_INTCON
_INTCON	set	11
	global	_PORTD
_PORTD	set	8
	global	_T1CON
_T1CON	set	16
	global	_TMR1L
_TMR1L	set	14
	global	_CARRY
_CARRY	set	24
	global	_GIE
_GIE	set	95
	global	_TMR1IF
_TMR1IF	set	96
	global	_TO
_TO	set	28
	global	_OPTION
_OPTION	set	129
	global	_PIE1
_PIE1	set	140
	global	_TRISD
_TRISD	set	136
	global	_TMR1IE
_TMR1IE	set	1120
	global	_EEADR
_EEADR	set	269
	global	_EEDATA
_EEDATA	set	268
	global	_EECON1
_EECON1	set	396
	global	_EECON2
_EECON2	set	397
	global	_WR
_WR	set	3169
	global	_WREN
_WREN	set	3170
	file	"877A_C_TEST.as"
	line	#
psect cinit,class=CODE,delta=2
global start_initialization
start_initialization:

psect	bssCOMMON,class=COMMON,space=1
global __pbssCOMMON
__pbssCOMMON:
_eeprom_rd0:
       ds      2

_counter_flag:
       ds      1

_main_counter:
       ds      1

psect	dataCOMMON,class=COMMON,space=1
global __pdataCOMMON
__pdataCOMMON:
	file	"C:\pic_projects\877A_C_TEST\main.c"
_eeprom_main_counter:
       ds      2

psect	bssBANK0,class=BANK0,space=1
global __pbssBANK0
__pbssBANK0:
_masked_count_rd0:
       ds      2

_masked_count_rd1:
       ds      2

_masked_count_rd2:
       ds      2

_masked_count_rd3:
       ds      2

_masked_count_rd4:
       ds      2

_masked_count_rd5:
       ds      2

_masked_count_rd6:
       ds      2

_masked_count_rd7:
       ds      2

_rd0_count:
       ds      2

_rd1_count:
       ds      2

_rd2_count:
       ds      2

_rd3_count:
       ds      2

_rd4_count:
       ds      2

_rd5_count:
       ds      2

_rd6_count:
       ds      2

_rd7_count:
       ds      2

psect	dataBANK0,class=BANK0,space=1
global __pdataBANK0
__pdataBANK0:
	file	"C:\pic_projects\877A_C_TEST\main.c"
	line	37
_eeprom_rd1:
       ds      2

psect	dataBANK0
	file	"C:\pic_projects\877A_C_TEST\main.c"
	line	38
_eeprom_rd2:
       ds      2

psect	dataBANK0
	file	"C:\pic_projects\877A_C_TEST\main.c"
	line	39
_eeprom_rd3:
       ds      2

psect	dataBANK0
	file	"C:\pic_projects\877A_C_TEST\main.c"
	line	40
_eeprom_rd4:
       ds      2

psect	dataBANK0
	file	"C:\pic_projects\877A_C_TEST\main.c"
	line	41
_eeprom_rd5:
       ds      2

psect	dataBANK0
	file	"C:\pic_projects\877A_C_TEST\main.c"
	line	42
_eeprom_rd6:
       ds      2

psect	dataBANK0
	file	"C:\pic_projects\877A_C_TEST\main.c"
	line	43
_eeprom_rd7:
       ds      2

psect	dataBANK0
	file	"C:\pic_projects\877A_C_TEST\main.c"
	line	45
_eeprom_wdt:
       ds      2

psect clrtext,class=CODE,delta=2
global clear_ram
;	Called with FSR containing the base address, and
;	W with the last address+1
clear_ram:
	clrwdt			;clear the watchdog before getting into this loop
clrloop:
	clrf	indf		;clear RAM location pointed to by FSR
	incf	fsr,f		;increment pointer
	xorwf	fsr,w		;XOR with final address
	btfsc	status,2	;have we reached the end yet?
	retlw	0		;all done for this memory range, return
	xorwf	fsr,w		;XOR again to restore value
	goto	clrloop		;do the next byte

; Clear objects allocated to COMMON
psect cinit,class=CODE,delta=2
	clrf	((__pbssCOMMON)+0)&07Fh
	clrf	((__pbssCOMMON)+1)&07Fh
	clrf	((__pbssCOMMON)+2)&07Fh
	clrf	((__pbssCOMMON)+3)&07Fh
; Clear objects allocated to BANK0
psect cinit,class=CODE,delta=2
	bcf	status, 7	;select IRP bank0
	movlw	low(__pbssBANK0)
	movwf	fsr
	movlw	low((__pbssBANK0)+020h)
	fcall	clear_ram
global btemp
psect inittext,class=CODE,delta=2
global init_fetch,btemp
;	Called with low address in FSR and high address in W
init_fetch:
	movf btemp,w
	movwf pclath
	movf btemp+1,w
	movwf pc
global init_ram
;Called with:
;	high address of idata address in btemp 
;	low address of idata address in btemp+1 
;	low address of data in FSR
;	high address + 1 of data in btemp-1
init_ram:
	fcall init_fetch
	movwf indf,f
	incf fsr,f
	movf fsr,w
	xorwf btemp-1,w
	btfsc status,2
	retlw 0
	incf btemp+1,f
	btfsc status,2
	incf btemp,f
	goto init_ram
; Initialize objects allocated to BANK0
psect cinit,class=CODE,delta=2
global init_ram, __pidataBANK0
	movlw low(__pdataBANK0+16)
	movwf btemp-1,f
	movlw high(__pidataBANK0)
	movwf btemp,f
	movlw low(__pidataBANK0)
	movwf btemp+1,f
	movlw low(__pdataBANK0)
	movwf fsr,f
	fcall init_ram
; Initialize objects allocated to COMMON
	global __pidataCOMMON
psect cinit,class=CODE,delta=2
	fcall	__pidataCOMMON+0		;fetch initializer
	movwf	__pdataCOMMON+0&07fh		
	fcall	__pidataCOMMON+1		;fetch initializer
	movwf	__pdataCOMMON+1&07fh		
psect cinit,class=CODE,delta=2
global end_of_initialization

;End of C runtime variable initialization code

end_of_initialization:
clrf status
ljmp _main	;jump to C main() function
psect	cstackCOMMON,class=COMMON,space=1
global __pcstackCOMMON
__pcstackCOMMON:
	global	?_eeprom_clear
?_eeprom_clear:	; 0 bytes @ 0x0
	global	?_init
?_init:	; 0 bytes @ 0x0
	global	?_rd_count
?_rd_count:	; 0 bytes @ 0x0
	global	?_my_isr
?_my_isr:	; 0 bytes @ 0x0
	global	??_my_isr
??_my_isr:	; 0 bytes @ 0x0
	global	?_main
?_main:	; 2 bytes @ 0x0
	ds	6
psect	cstackBANK0,class=BANK0,space=1
global __pcstackBANK0
__pcstackBANK0:
	global	??_init
??_init:	; 0 bytes @ 0x0
	global	?_eeprom_write
?_eeprom_write:	; 0 bytes @ 0x0
	global	eeprom_write@value
eeprom_write@value:	; 1 bytes @ 0x0
	ds	1
	global	??_eeprom_write
??_eeprom_write:	; 0 bytes @ 0x1
	ds	1
	global	eeprom_write@addr
eeprom_write@addr:	; 1 bytes @ 0x2
	ds	1
	global	??_eeprom_clear
??_eeprom_clear:	; 0 bytes @ 0x3
	global	??_rd_count
??_rd_count:	; 0 bytes @ 0x3
	ds	1
	global	??_main
??_main:	; 0 bytes @ 0x4
	ds	1
;;Data sizes: Strings 0, constant 0, data 18, bss 36, persistent 2 stack 0
;;Auto spaces:   Size  Autos    Used
;; COMMON          14      6      12
;; BANK0           80      5      55
;; BANK1           80      0       0
;; BANK3           96      0       0
;; BANK2           96      0       0

;;
;; Pointer list with targets:



;;
;; Critical Paths under _main in COMMON
;;
;;   None.
;;
;; Critical Paths under _my_isr in COMMON
;;
;;   None.
;;
;; Critical Paths under _main in BANK0
;;
;;   _main->_eeprom_clear
;;   _main->_rd_count
;;   _rd_count->_eeprom_write
;;   _eeprom_clear->_eeprom_write
;;
;; Critical Paths under _my_isr in BANK0
;;
;;   None.
;;
;; Critical Paths under _main in BANK1
;;
;;   None.
;;
;; Critical Paths under _my_isr in BANK1
;;
;;   None.
;;
;; Critical Paths under _main in BANK3
;;
;;   None.
;;
;; Critical Paths under _my_isr in BANK3
;;
;;   None.
;;
;; Critical Paths under _main in BANK2
;;
;;   None.
;;
;; Critical Paths under _my_isr in BANK2
;;
;;   None.

;;
;;Main: autosize = 0, tempsize = 1, incstack = 0, save=0
;;

;;
;;Call Graph Tables:
;;
;; ---------------------------------------------------------------------------------
;; (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;; ---------------------------------------------------------------------------------
;; (0) _main                                                 1     1      0     186
;;                                              4 BANK0      1     1      0
;;                       _eeprom_clear
;;                               _init
;;                       _eeprom_write
;;                           _rd_count
;; ---------------------------------------------------------------------------------
;; (1) _rd_count                                             1     1      0      62
;;                                              3 BANK0      1     1      0
;;                       _eeprom_write
;; ---------------------------------------------------------------------------------
;; (1) _eeprom_clear                                         1     1      0      62
;;                                              3 BANK0      1     1      0
;;                       _eeprom_write
;; ---------------------------------------------------------------------------------
;; (2) _eeprom_write                                         3     2      1      62
;;                                              0 BANK0      3     2      1
;; ---------------------------------------------------------------------------------
;; (1) _init                                                 0     0      0       0
;; ---------------------------------------------------------------------------------
;; Estimated maximum stack depth 2
;; ---------------------------------------------------------------------------------
;; (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;; ---------------------------------------------------------------------------------
;; (3) _my_isr                                               6     6      0       0
;;                                              0 COMMON     6     6      0
;; ---------------------------------------------------------------------------------
;; Estimated maximum stack depth 3
;; ---------------------------------------------------------------------------------

;; Call Graph Graphs:

;; _main (ROOT)
;;   _eeprom_clear
;;     _eeprom_write
;;   _init
;;   _eeprom_write
;;   _rd_count
;;     _eeprom_write
;;
;; _my_isr (ROOT)
;;

;; Address spaces:

;;Name               Size   Autos  Total    Cost      Usage
;;BANK3               60      0       0       9        0.0%
;;BITBANK3            60      0       0       8        0.0%
;;SFR3                 0      0       0       4        0.0%
;;BITSFR3              0      0       0       4        0.0%
;;BANK2               60      0       0      11        0.0%
;;BITBANK2            60      0       0      10        0.0%
;;SFR2                 0      0       0       5        0.0%
;;BITSFR2              0      0       0       5        0.0%
;;SFR1                 0      0       0       2        0.0%
;;BITSFR1              0      0       0       2        0.0%
;;BANK1               50      0       0       7        0.0%
;;BITBANK1            50      0       0       6        0.0%
;;CODE                 0      0       0       0        0.0%
;;DATA                 0      0      45      12        0.0%
;;ABS                  0      0      43       3        0.0%
;;NULL                 0      0       0       0        0.0%
;;STACK                0      0       2       2        0.0%
;;BANK0               50      5      37       5       68.8%
;;BITBANK0            50      0       0       4        0.0%
;;SFR0                 0      0       0       1        0.0%
;;BITSFR0              0      0       0       1        0.0%
;;COMMON               E      6       C       1       85.7%
;;BITCOMMON            E      0       0       0        0.0%
;;EEDATA             100      0       0       0        0.0%

	global	_main
psect	maintext,global,class=CODE,delta=2
global __pmaintext
__pmaintext:

;; *************** function _main *****************
;; Defined at:
;;		line 58 in file "C:\pic_projects\877A_C_TEST\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;                  2  624[COMMON] int 
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 17F/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       1       0       0       0
;;      Totals:         0       1       0       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels required when called:    3
;; This function calls:
;;		_eeprom_clear
;;		_init
;;		_eeprom_write
;;		_rd_count
;; This function is called by:
;;		Startup code after reset
;; This function uses a non-reentrant model
;;
psect	maintext
	file	"C:\pic_projects\877A_C_TEST\main.c"
	line	58
	global	__size_of_main
	__size_of_main	equ	__end_of_main-_main
	
_main:	
	opt	stack 5
; Regs used in _main: [wreg+status,2+status,0+pclath+cstack]
	line	59
	
l2871:	
;main.c: 59: watch_dog_count=0;
	movlw	low(0)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(_watch_dog_count)
	movlw	high(0)
	movwf	((_watch_dog_count))+1
	line	60
	
l2873:	
;main.c: 60: eeprom_clear();
	fcall	_eeprom_clear
	line	61
	
l2875:	
;main.c: 61: init();
	fcall	_init
	line	62
	
l2877:	
;main.c: 62: (GIE = 1);
	bsf	(95/8),(95)&7
	line	63
;main.c: 63: main_counter=0xFF;
	movlw	(0FFh)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(_main_counter)	;volatile
	goto	l2879
	line	64
;main.c: 64: while(1)
	
l625:	
	line	66
	
l2879:	
;main.c: 65: {
;main.c: 66: if (TO == 0)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfsc	(28/8),(28)&7
	goto	u2551
	goto	u2550
u2551:
	goto	l626
u2550:
	line	68
	
l2881:	
;main.c: 67: {
;main.c: 68: watch_dog_count++;
	movlw	low(01h)
	addwf	(_watch_dog_count),f
	skipnc
	incf	(_watch_dog_count+1),f
	movlw	high(01h)
	addwf	(_watch_dog_count+1),f
	line	69
	
l2883:	
;main.c: 69: eeprom_write(eeprom_wdt,watch_dog_count);
	movf	(_watch_dog_count),w
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(?_eeprom_write)
	movf	(_eeprom_wdt),w
	fcall	_eeprom_write
	line	70
	
l2885:	
;main.c: 70: main_counter=0;
	clrc
	movlw	0
	btfsc	status,0
	movlw	1
	movwf	(_main_counter)	;volatile
	line	71
	
l2887:	
# 71 "C:\pic_projects\877A_C_TEST\main.c"
clrwdt ;#
psect	maintext
	line	72
	
l2889:	
;main.c: 72: init();
	fcall	_init
	line	73
	
l2891:	
;main.c: 73: (GIE = 1);
	bsf	(95/8),(95)&7
	line	74
;main.c: 74: }
	goto	l2879
	line	75
	
l626:	
	line	77
# 77 "C:\pic_projects\877A_C_TEST\main.c"
clrwdt ;#
psect	maintext
	line	78
	
l2893:	
;main.c: 78: PORTD = main_counter;
	movf	(_main_counter),w	;volatile
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(8)	;volatile
	line	79
	
l2895:	
;main.c: 79: eeprom_write(eeprom_main_counter,main_counter);
	movf	(_main_counter),w	;volatile
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(?_eeprom_write)
	movf	(_eeprom_main_counter),w
	fcall	_eeprom_write
	line	80
	
l2897:	
;main.c: 80: rd_count();
	fcall	_rd_count
	line	81
;main.c: 81: if (main_counter==0xFF)
	movf	(_main_counter),w	;volatile
	xorlw	0FFh
	skipz
	goto	u2561
	goto	u2560
u2561:
	goto	l2879
u2560:
	line	83
	
l2899:	
;main.c: 82: {
;main.c: 83: (GIE = 0);
	bcf	(95/8),(95)&7
	line	84
# 84 "C:\pic_projects\877A_C_TEST\main.c"
sleep ;#
psect	maintext
	goto	l2879
	line	85
	
l628:	
	goto	l2879
	line	86
	
l627:	
	goto	l2879
	line	87
	
l629:	
	line	64
	goto	l2879
	
l630:	
	line	88
	
l631:	
	global	start
	ljmp	start
	opt stack 0
GLOBAL	__end_of_main
	__end_of_main:
;; =============== function _main ends ============

	signat	_main,90
	global	_rd_count
psect	text233,local,class=CODE,delta=2
global __ptext233
__ptext233:

;; *************** function _rd_count *****************
;; Defined at:
;;		line 91 in file "C:\pic_projects\877A_C_TEST\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       1       0       0       0
;;      Totals:         0       1       0       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		_eeprom_write
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text233
	file	"C:\pic_projects\877A_C_TEST\main.c"
	line	91
	global	__size_of_rd_count
	__size_of_rd_count	equ	__end_of_rd_count-_rd_count
	
_rd_count:	
	opt	stack 5
; Regs used in _rd_count: [wreg+status,2+status,0+pclath+cstack]
	line	92
	
l2789:	
;main.c: 92: if(masked_count_rd0==1)
	movlw	01h
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	xorwf	(_masked_count_rd0),w	;volatile
	iorwf	(_masked_count_rd0+1),w	;volatile
	skipz
	goto	u2471
	goto	u2470
u2471:
	goto	l2799
u2470:
	line	94
	
l2791:	
;main.c: 93: {
;main.c: 94: rd0_count++;
	movlw	low(01h)
	addwf	(_rd0_count),f
	skipnc
	incf	(_rd0_count+1),f
	movlw	high(01h)
	addwf	(_rd0_count+1),f
	line	95
	
l2793:	
;main.c: 95: eeprom_write(eeprom_rd0,rd0_count);
	movf	(_rd0_count),w
	movwf	(??_rd_count+0)+0
	movf	(??_rd_count+0)+0,w
	movwf	(?_eeprom_write)
	movf	(_eeprom_rd0),w
	fcall	_eeprom_write
	line	96
	
l2795:	
;main.c: 96: TMR1L = 0x00;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(14)	;volatile
	line	97
	
l2797:	
;main.c: 97: masked_count_rd0=0;
	movlw	low(0)
	movwf	(_masked_count_rd0)	;volatile
	movlw	high(0)
	movwf	((_masked_count_rd0))+1	;volatile
	line	98
;main.c: 98: }
	goto	l650
	line	99
	
l634:	
	
l2799:	
;main.c: 99: else if(masked_count_rd1==2)
	movlw	02h
	xorwf	(_masked_count_rd1),w	;volatile
	iorwf	(_masked_count_rd1+1),w	;volatile
	skipz
	goto	u2481
	goto	u2480
u2481:
	goto	l2809
u2480:
	line	101
	
l2801:	
;main.c: 100: {
;main.c: 101: rd1_count++;
	movlw	low(01h)
	addwf	(_rd1_count),f
	skipnc
	incf	(_rd1_count+1),f
	movlw	high(01h)
	addwf	(_rd1_count+1),f
	line	102
	
l2803:	
;main.c: 102: eeprom_write(eeprom_rd1,rd1_count);
	movf	(_rd1_count),w
	movwf	(??_rd_count+0)+0
	movf	(??_rd_count+0)+0,w
	movwf	(?_eeprom_write)
	movf	(_eeprom_rd1),w
	fcall	_eeprom_write
	line	103
	
l2805:	
;main.c: 103: TMR1L = 0x00;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(14)	;volatile
	line	104
	
l2807:	
;main.c: 104: masked_count_rd1=0;
	movlw	low(0)
	movwf	(_masked_count_rd1)	;volatile
	movlw	high(0)
	movwf	((_masked_count_rd1))+1	;volatile
	line	105
;main.c: 105: }
	goto	l650
	line	106
	
l636:	
	
l2809:	
;main.c: 106: else if(masked_count_rd2==4)
	movlw	04h
	xorwf	(_masked_count_rd2),w	;volatile
	iorwf	(_masked_count_rd2+1),w	;volatile
	skipz
	goto	u2491
	goto	u2490
u2491:
	goto	l2819
u2490:
	line	108
	
l2811:	
;main.c: 107: {
;main.c: 108: rd2_count++;
	movlw	low(01h)
	addwf	(_rd2_count),f
	skipnc
	incf	(_rd2_count+1),f
	movlw	high(01h)
	addwf	(_rd2_count+1),f
	line	109
	
l2813:	
;main.c: 109: eeprom_write(eeprom_rd2,rd2_count);
	movf	(_rd2_count),w
	movwf	(??_rd_count+0)+0
	movf	(??_rd_count+0)+0,w
	movwf	(?_eeprom_write)
	movf	(_eeprom_rd2),w
	fcall	_eeprom_write
	line	110
	
l2815:	
;main.c: 110: TMR1L = 0x00;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(14)	;volatile
	line	111
	
l2817:	
;main.c: 111: masked_count_rd2=0;
	movlw	low(0)
	movwf	(_masked_count_rd2)	;volatile
	movlw	high(0)
	movwf	((_masked_count_rd2))+1	;volatile
	line	112
;main.c: 112: }
	goto	l650
	line	113
	
l638:	
	
l2819:	
;main.c: 113: else if(masked_count_rd3==8)
	movlw	08h
	xorwf	(_masked_count_rd3),w	;volatile
	iorwf	(_masked_count_rd3+1),w	;volatile
	skipz
	goto	u2501
	goto	u2500
u2501:
	goto	l2829
u2500:
	line	115
	
l2821:	
;main.c: 114: {
;main.c: 115: rd3_count++;
	movlw	low(01h)
	addwf	(_rd3_count),f
	skipnc
	incf	(_rd3_count+1),f
	movlw	high(01h)
	addwf	(_rd3_count+1),f
	line	116
	
l2823:	
;main.c: 116: eeprom_write(eeprom_rd3,rd3_count);
	movf	(_rd3_count),w
	movwf	(??_rd_count+0)+0
	movf	(??_rd_count+0)+0,w
	movwf	(?_eeprom_write)
	movf	(_eeprom_rd3),w
	fcall	_eeprom_write
	line	117
	
l2825:	
;main.c: 117: TMR1L = 0x00;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(14)	;volatile
	line	118
	
l2827:	
;main.c: 118: masked_count_rd3=0;
	movlw	low(0)
	movwf	(_masked_count_rd3)	;volatile
	movlw	high(0)
	movwf	((_masked_count_rd3))+1	;volatile
	line	119
;main.c: 119: }
	goto	l650
	line	120
	
l640:	
	
l2829:	
;main.c: 120: else if(masked_count_rd4==16)
	movlw	010h
	xorwf	(_masked_count_rd4),w	;volatile
	iorwf	(_masked_count_rd4+1),w	;volatile
	skipz
	goto	u2511
	goto	u2510
u2511:
	goto	l2839
u2510:
	line	122
	
l2831:	
;main.c: 121: {
;main.c: 122: rd4_count++;
	movlw	low(01h)
	addwf	(_rd4_count),f
	skipnc
	incf	(_rd4_count+1),f
	movlw	high(01h)
	addwf	(_rd4_count+1),f
	line	123
	
l2833:	
;main.c: 123: eeprom_write(eeprom_rd4,rd4_count);
	movf	(_rd4_count),w
	movwf	(??_rd_count+0)+0
	movf	(??_rd_count+0)+0,w
	movwf	(?_eeprom_write)
	movf	(_eeprom_rd4),w
	fcall	_eeprom_write
	line	124
	
l2835:	
;main.c: 124: TMR1L = 0x00;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(14)	;volatile
	line	125
	
l2837:	
;main.c: 125: masked_count_rd4=0;
	movlw	low(0)
	movwf	(_masked_count_rd4)	;volatile
	movlw	high(0)
	movwf	((_masked_count_rd4))+1	;volatile
	line	126
;main.c: 126: }
	goto	l650
	line	127
	
l642:	
	
l2839:	
;main.c: 127: else if(masked_count_rd5==32)
	movlw	020h
	xorwf	(_masked_count_rd5),w	;volatile
	iorwf	(_masked_count_rd5+1),w	;volatile
	skipz
	goto	u2521
	goto	u2520
u2521:
	goto	l2849
u2520:
	line	129
	
l2841:	
;main.c: 128: {
;main.c: 129: rd5_count++;
	movlw	low(01h)
	addwf	(_rd5_count),f
	skipnc
	incf	(_rd5_count+1),f
	movlw	high(01h)
	addwf	(_rd5_count+1),f
	line	130
	
l2843:	
;main.c: 130: eeprom_write(eeprom_rd5,rd5_count);
	movf	(_rd5_count),w
	movwf	(??_rd_count+0)+0
	movf	(??_rd_count+0)+0,w
	movwf	(?_eeprom_write)
	movf	(_eeprom_rd5),w
	fcall	_eeprom_write
	line	131
	
l2845:	
;main.c: 131: TMR1L = 0x00;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(14)	;volatile
	line	132
	
l2847:	
;main.c: 132: masked_count_rd5=0;
	movlw	low(0)
	movwf	(_masked_count_rd5)	;volatile
	movlw	high(0)
	movwf	((_masked_count_rd5))+1	;volatile
	line	133
;main.c: 133: }
	goto	l650
	line	134
	
l644:	
	
l2849:	
;main.c: 134: else if(masked_count_rd6==64)
	movlw	040h
	xorwf	(_masked_count_rd6),w	;volatile
	iorwf	(_masked_count_rd6+1),w	;volatile
	skipz
	goto	u2531
	goto	u2530
u2531:
	goto	l2859
u2530:
	line	136
	
l2851:	
;main.c: 135: {
;main.c: 136: rd6_count++;
	movlw	low(01h)
	addwf	(_rd6_count),f
	skipnc
	incf	(_rd6_count+1),f
	movlw	high(01h)
	addwf	(_rd6_count+1),f
	line	137
	
l2853:	
;main.c: 137: eeprom_write(eeprom_rd6,rd6_count);
	movf	(_rd6_count),w
	movwf	(??_rd_count+0)+0
	movf	(??_rd_count+0)+0,w
	movwf	(?_eeprom_write)
	movf	(_eeprom_rd6),w
	fcall	_eeprom_write
	line	138
	
l2855:	
;main.c: 138: TMR1L = 0x00;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(14)	;volatile
	line	139
	
l2857:	
;main.c: 139: masked_count_rd6=0;
	movlw	low(0)
	movwf	(_masked_count_rd6)	;volatile
	movlw	high(0)
	movwf	((_masked_count_rd6))+1	;volatile
	line	140
;main.c: 140: }
	goto	l650
	line	141
	
l646:	
	
l2859:	
;main.c: 141: else if(masked_count_rd7==128)
	movlw	080h
	xorwf	(_masked_count_rd7),w	;volatile
	iorwf	(_masked_count_rd7+1),w	;volatile
	skipz
	goto	u2541
	goto	u2540
u2541:
	goto	l2869
u2540:
	line	143
	
l2861:	
;main.c: 142: {
;main.c: 143: rd7_count++;
	movlw	low(01h)
	addwf	(_rd7_count),f
	skipnc
	incf	(_rd7_count+1),f
	movlw	high(01h)
	addwf	(_rd7_count+1),f
	line	144
	
l2863:	
;main.c: 144: eeprom_write(eeprom_rd7,rd7_count);
	movf	(_rd7_count),w
	movwf	(??_rd_count+0)+0
	movf	(??_rd_count+0)+0,w
	movwf	(?_eeprom_write)
	movf	(_eeprom_rd7),w
	fcall	_eeprom_write
	line	145
	
l2865:	
;main.c: 145: TMR1L = 0x00;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(14)	;volatile
	line	146
	
l2867:	
;main.c: 146: masked_count_rd7=0;
	movlw	low(0)
	movwf	(_masked_count_rd7)	;volatile
	movlw	high(0)
	movwf	((_masked_count_rd7))+1	;volatile
	line	147
;main.c: 147: }
	goto	l650
	line	148
	
l648:	
	line	150
	
l2869:	
;main.c: 148: else
;main.c: 149: {
;main.c: 150: eeprom_write(eeprom_main_counter,main_counter);
	movf	(_main_counter),w	;volatile
	movwf	(??_rd_count+0)+0
	movf	(??_rd_count+0)+0,w
	movwf	(?_eeprom_write)
	movf	(_eeprom_main_counter),w
	fcall	_eeprom_write
	goto	l650
	line	151
	
l649:	
	goto	l650
	
l647:	
	goto	l650
	
l645:	
	goto	l650
	
l643:	
	goto	l650
	
l641:	
	goto	l650
	
l639:	
	goto	l650
	
l637:	
	goto	l650
	
l635:	
	line	152
	
l650:	
	return
	opt stack 0
GLOBAL	__end_of_rd_count
	__end_of_rd_count:
;; =============== function _rd_count ends ============

	signat	_rd_count,88
	global	_eeprom_clear
psect	text234,local,class=CODE,delta=2
global __ptext234
__ptext234:

;; *************** function _eeprom_clear *****************
;; Defined at:
;;		line 166 in file "C:\pic_projects\877A_C_TEST\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       1       0       0       0
;;      Totals:         0       1       0       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		_eeprom_write
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text234
	file	"C:\pic_projects\877A_C_TEST\main.c"
	line	166
	global	__size_of_eeprom_clear
	__size_of_eeprom_clear	equ	__end_of_eeprom_clear-_eeprom_clear
	
_eeprom_clear:	
	opt	stack 5
; Regs used in _eeprom_clear: [wreg+status,2+status,0+pclath+cstack]
	line	167
	
l2787:	
;main.c: 167: eeprom_write(eeprom_rd0,0xFF);
	movlw	(0FFh)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_eeprom_clear+0)+0
	movf	(??_eeprom_clear+0)+0,w
	movwf	(?_eeprom_write)
	movf	(_eeprom_rd0),w
	fcall	_eeprom_write
	line	168
;main.c: 168: eeprom_write(eeprom_rd1,0xFF);
	movlw	(0FFh)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_eeprom_clear+0)+0
	movf	(??_eeprom_clear+0)+0,w
	movwf	(?_eeprom_write)
	movf	(_eeprom_rd1),w
	fcall	_eeprom_write
	line	169
;main.c: 169: eeprom_write(eeprom_rd2,0xFF);
	movlw	(0FFh)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_eeprom_clear+0)+0
	movf	(??_eeprom_clear+0)+0,w
	movwf	(?_eeprom_write)
	movf	(_eeprom_rd2),w
	fcall	_eeprom_write
	line	170
;main.c: 170: eeprom_write(eeprom_rd3,0xFF);
	movlw	(0FFh)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_eeprom_clear+0)+0
	movf	(??_eeprom_clear+0)+0,w
	movwf	(?_eeprom_write)
	movf	(_eeprom_rd3),w
	fcall	_eeprom_write
	line	171
;main.c: 171: eeprom_write(eeprom_rd4,0xFF);
	movlw	(0FFh)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_eeprom_clear+0)+0
	movf	(??_eeprom_clear+0)+0,w
	movwf	(?_eeprom_write)
	movf	(_eeprom_rd4),w
	fcall	_eeprom_write
	line	172
;main.c: 172: eeprom_write(eeprom_rd5,0xFF);
	movlw	(0FFh)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_eeprom_clear+0)+0
	movf	(??_eeprom_clear+0)+0,w
	movwf	(?_eeprom_write)
	movf	(_eeprom_rd5),w
	fcall	_eeprom_write
	line	173
;main.c: 173: eeprom_write(eeprom_rd6,0xFF);
	movlw	(0FFh)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_eeprom_clear+0)+0
	movf	(??_eeprom_clear+0)+0,w
	movwf	(?_eeprom_write)
	movf	(_eeprom_rd6),w
	fcall	_eeprom_write
	line	174
;main.c: 174: eeprom_write(eeprom_rd7,0xFF);
	movlw	(0FFh)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_eeprom_clear+0)+0
	movf	(??_eeprom_clear+0)+0,w
	movwf	(?_eeprom_write)
	movf	(_eeprom_rd7),w
	fcall	_eeprom_write
	line	175
;main.c: 175: eeprom_write(eeprom_main_counter,0xFF);
	movlw	(0FFh)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_eeprom_clear+0)+0
	movf	(??_eeprom_clear+0)+0,w
	movwf	(?_eeprom_write)
	movf	(_eeprom_main_counter),w
	fcall	_eeprom_write
	line	176
	
l656:	
	return
	opt stack 0
GLOBAL	__end_of_eeprom_clear
	__end_of_eeprom_clear:
;; =============== function _eeprom_clear ends ============

	signat	_eeprom_clear,88
	global	_eeprom_write
psect	text235,local,class=CODE,delta=2
global __ptext235
__ptext235:

;; *************** function _eeprom_write *****************
;; Defined at:
;;		line 8 in file "eewrite.c"
;; Parameters:    Size  Location     Type
;;  addr            1    wreg     unsigned char 
;;  value           1    0[BANK0 ] unsigned char 
;; Auto vars:     Size  Location     Type
;;  addr            1    2[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       1       0       0       0
;;      Locals:         0       1       0       0       0
;;      Temps:          0       1       0       0       0
;;      Totals:         0       3       0       0       0
;;Total ram usage:        3 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;;		_rd_count
;;		_eeprom_clear
;; This function uses a non-reentrant model
;;
psect	text235
	file	"C:\Program Files\HI-TECH Software\PICC\9.80\sources\eewrite.c"
	line	8
	global	__size_of_eeprom_write
	__size_of_eeprom_write	equ	__end_of_eeprom_write-_eeprom_write
	
_eeprom_write:	
	opt	stack 5
; Regs used in _eeprom_write: [wreg+status,2+status,0]
;eeprom_write@addr stored from wreg
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(eeprom_write@addr)
	line	9
	
l1255:	
	goto	l1256
	
l1257:	
	
l1256:	
	bsf	status, 5	;RP0=1, select bank3
	bsf	status, 6	;RP1=1, select bank3
	btfsc	(3169/8)^0180h,(3169)&7
	goto	u2441
	goto	u2440
u2441:
	goto	l1256
u2440:
	goto	l2767
	
l1258:	
	
l2767:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(eeprom_write@addr),w
	bcf	status, 5	;RP0=0, select bank2
	bsf	status, 6	;RP1=1, select bank2
	movwf	(269)^0100h	;volatile
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(eeprom_write@value),w
	bcf	status, 5	;RP0=0, select bank2
	bsf	status, 6	;RP1=1, select bank2
	movwf	(268)^0100h	;volatile
	
l2769:	
	movlw	(03Fh)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_eeprom_write+0)+0
	movf	(??_eeprom_write+0)+0,w
	bsf	status, 5	;RP0=1, select bank3
	bsf	status, 6	;RP1=1, select bank3
	andwf	(396)^0180h,f	;volatile
	
l2771:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	bcf	(24/8),(24)&7
	
l2773:	
	btfss	(95/8),(95)&7
	goto	u2451
	goto	u2450
u2451:
	goto	l1259
u2450:
	
l2775:	
	bsf	(24/8),(24)&7
	
l1259:	
	bcf	(95/8),(95)&7
	bsf	status, 5	;RP0=1, select bank3
	bsf	status, 6	;RP1=1, select bank3
	bsf	(3170/8)^0180h,(3170)&7
	
l2777:	
	movlw	(055h)
	movwf	(397)^0180h	;volatile
	movlw	(0AAh)
	movwf	(397)^0180h	;volatile
	
l2779:	
	bsf	(3169/8)^0180h,(3169)&7
	
l2781:	
	bcf	(3170/8)^0180h,(3170)&7
	
l2783:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfss	(24/8),(24)&7
	goto	u2461
	goto	u2460
u2461:
	goto	l1262
u2460:
	
l2785:	
	bsf	(95/8),(95)&7
	goto	l1262
	
l1260:	
	goto	l1262
	
l1261:	
	line	10
	
l1262:	
	return
	opt stack 0
GLOBAL	__end_of_eeprom_write
	__end_of_eeprom_write:
;; =============== function _eeprom_write ends ============

	signat	_eeprom_write,8312
	global	_init
psect	text236,local,class=CODE,delta=2
global __ptext236
__ptext236:

;; *************** function _init *****************
;; Defined at:
;;		line 156 in file "C:\pic_projects\877A_C_TEST\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text236
	file	"C:\pic_projects\877A_C_TEST\main.c"
	line	156
	global	__size_of_init
	__size_of_init	equ	__end_of_init-_init
	
_init:	
	opt	stack 6
; Regs used in _init: [wreg+status,2]
	line	157
	
l2761:	
;main.c: 157: OPTION = 0x80;
	movlw	(080h)
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movwf	(129)^080h	;volatile
	line	158
	
l2763:	
;main.c: 158: TRISD = 0x00;
	clrf	(136)^080h	;volatile
	line	159
	
l2765:	
;main.c: 159: PORTD = 0x00;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(8)	;volatile
	line	160
;main.c: 160: PIE1 = 0x01;
	movlw	(01h)
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movwf	(140)^080h	;volatile
	line	161
;main.c: 161: INTCON = 0x40;
	movlw	(040h)
	movwf	(11)	;volatile
	line	162
;main.c: 162: T1CON = 0x35;
	movlw	(035h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(16)	;volatile
	line	163
	
l653:	
	return
	opt stack 0
GLOBAL	__end_of_init
	__end_of_init:
;; =============== function _init ends ============

	signat	_init,88
	global	_my_isr
psect	text237,local,class=CODE,delta=2
global __ptext237
__ptext237:

;; *************** function _my_isr *****************
;; Defined at:
;;		line 20 in file "C:\pic_projects\HITECH_EXAMPLE\isr_func.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          6       0       0       0       0
;;      Totals:         6       0       0       0       0
;;Total ram usage:        6 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		Interrupt level 1
;; This function uses a non-reentrant model
;;
psect	text237
	file	"C:\pic_projects\HITECH_EXAMPLE\isr_func.c"
	line	20
	global	__size_of_my_isr
	__size_of_my_isr	equ	__end_of_my_isr-_my_isr
	
_my_isr:	
	opt	stack 5
; Regs used in _my_isr: [wreg+status,2+status,0]
psect	intentry,class=CODE,delta=2
global __pintentry
__pintentry:
global interrupt_function
interrupt_function:
	global saved_w
	saved_w	set	btemp+0
	movwf	saved_w
	movf	status,w
	movwf	(??_my_isr+2)
	movf	fsr0,w
	movwf	(??_my_isr+3)
	movf	pclath,w
	movwf	(??_my_isr+4)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	btemp+1,w
	movwf	(??_my_isr+5)
	ljmp	_my_isr
psect	text237
	line	21
	
i1l2615:	
;isr_func.c: 21: if((TMR1IE)&&(TMR1IF))
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	btfss	(1120/8)^080h,(1120)&7
	goto	u232_21
	goto	u232_20
u232_21:
	goto	i1l1236
u232_20:
	
i1l2617:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfss	(96/8),(96)&7
	goto	u233_21
	goto	u233_20
u233_21:
	goto	i1l1236
u233_20:
	line	23
	
i1l2619:	
;isr_func.c: 22: {
;isr_func.c: 23: INTCON = 0x00;
	clrf	(11)	;volatile
	line	24
	
i1l2621:	
;isr_func.c: 24: main_counter++;
	movlw	(01h)
	movwf	(??_my_isr+0)+0
	movf	(??_my_isr+0)+0,w
	addwf	(_main_counter),f	;volatile
	line	25
	
i1l2623:	
;isr_func.c: 25: TMR1IF=0;
	bcf	(96/8),(96)&7
	line	26
	
i1l2625:	
;isr_func.c: 26: counter_flag=1;
	movlw	(01h)
	movwf	(??_my_isr+0)+0
	movf	(??_my_isr+0)+0,w
	movwf	(_counter_flag)	;volatile
	line	27
	
i1l2627:	
;isr_func.c: 27: TMR1L = 0x00;
	clrf	(14)	;volatile
	line	28
	
i1l2629:	
;isr_func.c: 28: masked_count_rd0 = (main_counter&0x01);
	movf	(_main_counter),w
	andlw	01h
	movwf	(??_my_isr+0)+0
	clrf	(??_my_isr+0)+0+1
	movf	0+(??_my_isr+0)+0,w
	movwf	(_masked_count_rd0)	;volatile
	movf	1+(??_my_isr+0)+0,w
	movwf	(_masked_count_rd0+1)	;volatile
	line	29
	
i1l2631:	
;isr_func.c: 29: masked_count_rd1 = (main_counter&0x02);
	movf	(_main_counter),w
	andlw	02h
	movwf	(??_my_isr+0)+0
	clrf	(??_my_isr+0)+0+1
	movf	0+(??_my_isr+0)+0,w
	movwf	(_masked_count_rd1)	;volatile
	movf	1+(??_my_isr+0)+0,w
	movwf	(_masked_count_rd1+1)	;volatile
	line	30
	
i1l2633:	
;isr_func.c: 30: masked_count_rd2 = (main_counter&0x04);
	movf	(_main_counter),w
	andlw	04h
	movwf	(??_my_isr+0)+0
	clrf	(??_my_isr+0)+0+1
	movf	0+(??_my_isr+0)+0,w
	movwf	(_masked_count_rd2)	;volatile
	movf	1+(??_my_isr+0)+0,w
	movwf	(_masked_count_rd2+1)	;volatile
	line	31
	
i1l2635:	
;isr_func.c: 31: masked_count_rd3 = (main_counter&0x08);
	movf	(_main_counter),w
	andlw	08h
	movwf	(??_my_isr+0)+0
	clrf	(??_my_isr+0)+0+1
	movf	0+(??_my_isr+0)+0,w
	movwf	(_masked_count_rd3)	;volatile
	movf	1+(??_my_isr+0)+0,w
	movwf	(_masked_count_rd3+1)	;volatile
	line	32
	
i1l2637:	
;isr_func.c: 32: masked_count_rd4 = (main_counter&0x10);
	movf	(_main_counter),w
	andlw	010h
	movwf	(??_my_isr+0)+0
	clrf	(??_my_isr+0)+0+1
	movf	0+(??_my_isr+0)+0,w
	movwf	(_masked_count_rd4)	;volatile
	movf	1+(??_my_isr+0)+0,w
	movwf	(_masked_count_rd4+1)	;volatile
	line	33
	
i1l2639:	
;isr_func.c: 33: masked_count_rd5 = (main_counter&0x20);
	movf	(_main_counter),w
	andlw	020h
	movwf	(??_my_isr+0)+0
	clrf	(??_my_isr+0)+0+1
	movf	0+(??_my_isr+0)+0,w
	movwf	(_masked_count_rd5)	;volatile
	movf	1+(??_my_isr+0)+0,w
	movwf	(_masked_count_rd5+1)	;volatile
	line	34
	
i1l2641:	
;isr_func.c: 34: masked_count_rd6 = (main_counter&0x40);
	movf	(_main_counter),w
	andlw	040h
	movwf	(??_my_isr+0)+0
	clrf	(??_my_isr+0)+0+1
	movf	0+(??_my_isr+0)+0,w
	movwf	(_masked_count_rd6)	;volatile
	movf	1+(??_my_isr+0)+0,w
	movwf	(_masked_count_rd6+1)	;volatile
	line	35
	
i1l2643:	
;isr_func.c: 35: masked_count_rd7 = (main_counter&0x80);
	movf	(_main_counter),w
	andlw	080h
	movwf	(??_my_isr+0)+0
	clrf	(??_my_isr+0)+0+1
	movf	0+(??_my_isr+0)+0,w
	movwf	(_masked_count_rd7)	;volatile
	movf	1+(??_my_isr+0)+0,w
	movwf	(_masked_count_rd7+1)	;volatile
	line	36
	
i1l2645:	
;isr_func.c: 36: INTCON = 0x40;
	movlw	(040h)
	movwf	(11)	;volatile
	goto	i1l1236
	line	37
	
i1l1235:	
	line	38
	
i1l1236:	
	movf	(??_my_isr+5),w
	bcf	status, 5	;RP0=0, select bank0
	movwf	btemp+1
	movf	(??_my_isr+4),w
	movwf	pclath
	movf	(??_my_isr+3),w
	movwf	fsr0
	movf	(??_my_isr+2),w
	movwf	status
	swapf	saved_w,f
	swapf	saved_w,w
	retfie
	opt stack 0
GLOBAL	__end_of_my_isr
	__end_of_my_isr:
;; =============== function _my_isr ends ============

	signat	_my_isr,88
psect	text238,local,class=CODE,delta=2
global __ptext238
__ptext238:
	global	btemp
	btemp set 07Eh

	DABS	1,126,2	;btemp
	global	wtemp0
	wtemp0 set btemp
	end
