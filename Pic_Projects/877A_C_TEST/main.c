/**********************************************************/          
/* 9 Jun 2011 using PIC16F877A                            */ 
/* PORT D on                                              */
/**********************************************************/
#include <htc.h>

/**********************************************************/
/* Device Configuration
/**********************************************************/
     
      /* PIC16F877A*/
       __CONFIG (WDTEN & PWRTDIS & BORDIS & LVPEN & WRTEN &
       DEBUGDIS & DUNPROT & UNPROTECT & HS);
      
       
 
/**********************************************************/
/* Function Prototypes 
/**********************************************************/

void init(void);     // SFR inits
void rd_count(void); 
void eeprom_clear(void);
/***********************************************************/
/* Variable Definition
/***********************************************************/
//unsigned int watch_dog_count=0;
unsigned int rd0_count = 0;
unsigned int rd1_count = 0;
unsigned int rd2_count = 0;
unsigned int rd3_count = 0;
unsigned int rd4_count = 0;
unsigned int rd5_count = 0;
unsigned int rd6_count = 0;
unsigned int rd7_count = 0;
unsigned int eeprom_rd0 = 0x0000;          // count portd0
unsigned int eeprom_rd1 = 0x0001;          // count portd1
unsigned int eeprom_rd2 = 0x0002;          // count portd2
unsigned int eeprom_rd3 = 0x0003;          // count portd3
unsigned int eeprom_rd4 = 0x0004;          // count portd4
unsigned int eeprom_rd5 = 0x0005;          // count portd5
unsigned int eeprom_rd6 = 0x0006;          // count portd6
unsigned int eeprom_rd7 = 0x0007;          // count portd7 
unsigned int eeprom_main_counter = 0x0008; // main_count
unsigned int eeprom_wdt = 0x0009;          // Used for watch_dog_count
volatile int masked_count_rd0;
volatile int masked_count_rd1;
volatile int masked_count_rd2;
volatile int masked_count_rd3;
volatile int masked_count_rd4;
volatile int masked_count_rd5;
volatile int masked_count_rd6;
volatile int masked_count_rd7;
volatile char main_counter=0;
volatile char counter_flag=0;
persistent int watch_dog_count;   /* will remain with a reset  */
int main(void)
{
watch_dog_count=0;                    
eeprom_clear();                                // clear all counts except watch dog to 0xff;
init();                                        // Initialize SFR
ei();                                          // enable interrupts
main_counter=0xFF;                           
while(1)                   
 { 
  if (TO == 0)                                 //STATUS reg, bit 4 1=After power up,CLRWDT or sleep instruction 0=A WDT time-out occured 
     {
     watch_dog_count++;
     eeprom_write(eeprom_wdt,watch_dog_count); // write the occurance into eeprom 
     main_counter=0;
     CLRWDT();                                 // clear if WD timed out
     init();
     ei();
     }
   else
      {
      CLRWDT();                                 // Clear watchdog timer
      PORTD = main_counter;
      eeprom_write(eeprom_main_counter,main_counter);
      rd_count();                               // fill in the current PORTD count to eeprom
       if (main_counter==0xFF)
       {
        di();
        SLEEP();
       }
       } 
  }
 } 

 void rd_count(void)
{
if(masked_count_rd0==1) 
   {       
   rd0_count++;
   eeprom_write(eeprom_rd0,rd0_count);
   TMR1L = 0x00;
   masked_count_rd0=0;
   }      
else if(masked_count_rd1==2)
  {
   rd1_count++;
   eeprom_write(eeprom_rd1,rd1_count);
   TMR1L = 0x00;
   masked_count_rd1=0;
  } 
else if(masked_count_rd2==4)
   {
   rd2_count++;
   eeprom_write(eeprom_rd2,rd2_count);
   TMR1L = 0x00;
   masked_count_rd2=0;
   }
else if(masked_count_rd3==8)
   {
   rd3_count++;
   eeprom_write(eeprom_rd3,rd3_count);
   TMR1L = 0x00;
   masked_count_rd3=0;
   }
else if(masked_count_rd4==16)
   {
   rd4_count++;
   eeprom_write(eeprom_rd4,rd4_count);
   TMR1L = 0x00;
   masked_count_rd4=0;
   }  
else if(masked_count_rd5==32)
   {
   rd5_count++;
   eeprom_write(eeprom_rd5,rd5_count);
   TMR1L = 0x00;
   masked_count_rd5=0;
   }
else  if(masked_count_rd6==64)
   {
   rd6_count++;
   eeprom_write(eeprom_rd6,rd6_count);
   TMR1L = 0x00;
   masked_count_rd6=0;
   }
else if(masked_count_rd7==128)
   {
   rd7_count++;
   eeprom_write(eeprom_rd7,rd7_count);
   TMR1L = 0x00;
   masked_count_rd7=0;
   }
else
  {
   eeprom_write(eeprom_main_counter,main_counter); //update EEprom with current tallied count
  }
 }


 void init(void)
{
OPTION     = 0x80;       //|PSA=Prescale to WDT|most prescale rate|
TRISD      = 0x00;       // port directions: 1=input, 0=output
PORTD      = 0x00;       // port D all off
PIE1       = 0x01;       // TMR1IE = enabled 
INTCON     = 0x40;       // PEIE = enabled
T1CON      = 0x35;       // T1CKPS1,T1CKPS0 = 1:8 prescaler,T1SYNC = do not synchronise external clock input ,TMR1ON = enabled
}

 void eeprom_clear(void)
{
 eeprom_write(eeprom_rd0,0xFF);
 eeprom_write(eeprom_rd1,0xFF);
 eeprom_write(eeprom_rd2,0xFF);
 eeprom_write(eeprom_rd3,0xFF);
 eeprom_write(eeprom_rd4,0xFF);
 eeprom_write(eeprom_rd5,0xFF);
 eeprom_write(eeprom_rd6,0xFF);
 eeprom_write(eeprom_rd7,0xFF);
 eeprom_write(eeprom_main_counter,0xFF);
}
