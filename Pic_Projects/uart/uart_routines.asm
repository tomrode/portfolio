                   list      p=16f887
                   #include  <p16f887.inc>

;******************************************************************************
; Device Configuration
;******************************************************************************

                   __CONFIG  _CONFIG1, _LVP_OFF & _FCMEN_ON & _IESO_OFF & _BOR_OFF & _CPD_OFF & _CP_OFF & _MCLRE_ON & _PWRTE_ON & _WDT_OFF & _INTRC_OSC_NOCLKOUT
                   __CONFIG  _CONFIG2, _WRT_OFF & _BOR21V

;******************************************************************************
; Variable Definitions
;******************************************************************************

toms_char_low      equ       0x7E
toms_char_high     equ       0x7F

;******************************************************************************
;EEPROM 
;******************************************************************************
eeprom_high_byte          equ       0x01
eeprom_low_byte           equ       0x00   

;******************************************************************************
; Reset Vector
;******************************************************************************

                   org       0x000
                   nop
                   goto      main

;******************************************************************************
; Interrupt Vector
;******************************************************************************

                   org       0x004
                   retfie                        ; Return from interrupt.

;******************************************************************************
; Main Program
;******************************************************************************

main
start_switch       
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;INITIALIZATIONS AND WAITING FOR SWITCH PRESS;;;;;;;;                                                  
                   movlw     0x00                 ; PORTB off
                   banksel   PORTB
                   movwf     PORTB                    

                   movlw     0x00                 ; PORTD off
                   banksel   PORTD
                   movwf     PORTD
                    
                   movlw     0x61                ; Select internal 4 MHz clock.
                   banksel   OSCCON
                   movwf     OSCCON
wait_stable
                   btfss     OSCCON,HTS          ; Is the high-speed internal oscillator stable?
                   goto      wait_stable         ; If not, wait until it is.

                   movlw     0x00                ; Disable all interrupts.
                   banksel   INTCON
                   movwf     INTCON
                   banksel   PIE1
                   movwf     PIE1

                   banksel   TRISC
                   bsf       TRISC,RC7           ; RC7 is an input (RX).
                   bcf       TRISC,RC6           ; RC6 is an output (TX).

                   movlw     0x00                ; SCKP = 0, BRG16 = 0 (8-BIT), WUE = 0, ABDEN = 0
                   banksel   BAUDCTL
                   movwf     BAUDCTL

                   movlw     25                  ; SRBR (LSB) = 25
                   banksel   SPBRG
                   movwf     SPBRG
                   movlw     0                   ; SPBR (MSB) = 0
                   banksel   SPBRGH
                   movwf     SPBRGH

                   movlw     0x80              ; SPEN = 1, RX9 = 0, SREN = 0 (DONT CARE), CREN = 0 (RECEIVER OFF), ADDEN = 0
                   banksel   RCSTA
                   movwf     RCSTA

                   movlw     0x04              ; CSRC = 0, TX9 = 0, TXEN = 0, SYNC = 0 (ASYNC MODE), SENDB = 0, BRGH = 1 (HIGH SPEED), TX9D = 0
                   banksel   TXSTA
                   movwf     TXSTA
 
                   banksel   RCSTA
                   bsf       RCSTA,CREN        ; CREN = 1 (RECEIVER ON)

                   movlw     0x00
                   banksel   TRISB
                   movwf     TRISB

                   movlw     0x00
                   banksel   TRISD
                   movwf     TRISD
                  
                   banksel   TMR0             ;Timer0 initialization                
                   clrwdt                     ;clear wdT and prescaler   
                   banksel   OPTION_REG       
                   movlw     0xD0             ; Mask TMR0 select, Clearing: T0CS = 0,PSA = 0,PS2 = 0,PS1 =0 ,PS0 = 0
                   andwf     OPTION_REG,W     ; prescale bits
                   iorlw     0x07             ; set to 1:256 before it rolls over 256 micro-seconds later Setting: PS2,PS1,PS0
                   movwf     OPTION_REG                                                 
                   
                   banksel   TRISE            ;Waiting for RE0 switch press 
                   bsf       TRISE,RE0                
                   banksel   ANSEL 
                   CLRF      ANSEL 
                   banksel   PORTE
                   btfss     PORTE,RE0
                   goto      start_switch             
;***************************************************************************************************
;**************************THIS IS THE BEGINING, WAITING FOR A SWITCH PRESS**************************     
                 
                   banksel   eeprom_low_byte             ; READ BACK LAST STATE OF EEPROM LOW BYTE 
                   movlw     eeprom_low_byte
                   banksel   EEADR
                   movwf     EEADR                       ;Data Memory Address to write

                   banksel   EECON1 
                   bcf       EECON1, EEPGD               ;Point to DATA memory
                   bsf       EECON1, RD                  ;Enable writes

                   banksel   EEDAT
                   movf      EEDAT,W             
                   banksel   PORTD
                   movwf     PORTD
                
                   banksel   eeprom_high_byte             ; READ BACK LAST STATE OF EEPROM HIGH BYTE 
                   movlw     eeprom_high_byte
                   banksel   EEADR
                   movwf     EEADR                       ;Data Memory Address to write

                   banksel   EECON1 
                   bcf       EECON1, EEPGD               ;Point to DATA memory
                   bsf       EECON1, RD                  ;Enable writes

                   banksel   EEDAT
                   movf      EEDAT,W             
                   banksel   PORTB
                   movwf     PORTB
                  

                        
                   
rcvr
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;GET FIRST BYTE;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
                   call      receive_byte
                   banksel   toms_char_low
                   movwf     toms_char_low

                   banksel   PORTD                   ; Output the current low byte count value
                   movwf     PORTD
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;WRITE EEPROM BYTE 1;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                   
                   banksel   eeprom_low_byte
                   movlw     eeprom_low_byte          ;This will be setting up address 0 in EEPROM 
                     
                   banksel   EEADR
                   movwf     EEADR                       ;Data Memory Address to write
             
                   banksel   PORTD
                   movf      PORTD,W
                  
                   banksel   EEDAT
                   movwf     EEDAT                       ;Data Memory Value to write
                   
                   banksel   EECON1 
                   bcf       EECON1, EEPGD               ;Point to DATA memory
                   bsf       EECON1, WREN                ;Enable writes

                   banksel   EECON2
                   movlw     0x55
                   movwf     EECON2                      ;Write 55h
                   movlw     0xAA
                   movwf     EECON2                      ;Write AAh
                   bsf       EECON1, WR                  ;Set WR bit to begin the write          

                   banksel   EECON1
write_loop         btfsc     EECON1,WR                   ;Has the write completed?
                   goto      write_loop                  ;If not, wait until it has completed.

                   banksel   EECON1
                   bcf       EECON1, WREN                ;Disable writes

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;GET SECOND BYTE and set timer to wait 65 ms for it. If it does not come goto subroutine   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;                   
                    
                   banksel   TMR0                
                   clrf      TMR0                        ;The timer is clear so maximum set time is 65ms of counting up
                   banksel   INTCON
                   bcf       INTCON,T0IF                 ;Make sure this flag is clear initially.

                   call      receive_byte   
                   btfsc     INTCON,T0IF
                   call      highbyte_lost               ; If the T0IF flag is not set go on timer did not lapse
                                                         ; Else go on to the next instruction                   
                   banksel   INTCON
                   bcf       INTCON,T0IF                 ; clear this flag after a single pass through 
                   banksel   toms_char_high
                   movwf     toms_char_high
                  
                   
                   banksel   PORTB                       ; Output the current high byte count value
                   movwf     PORTB
;:::::::::::::::::::::::::::::::::::::::::::::::::::::EEPROM WRITE 2;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

                   banksel   eeprom_high_byte
                   movlw     eeprom_high_byte           ;This will be setting up address 0 in EEPROM 
                     
                   banksel   EEADR
                   movwf     EEADR                      ;Data Memory Address to write
             
                   banksel   toms_char_high
                   movf      toms_char_high,W
                  
                   banksel   EEDAT
                   movwf     EEDAT                       ;Data Memory Value to write
                   
                   banksel   EECON1 
                   bcf       EECON1, EEPGD               ;Point to DATA memory
                   bsf       EECON1, WREN                ;Enable writes

                   banksel   EECON2
                   movlw     0x55
                   movwf     EECON2                      ;Write 55h
                   movlw     0xAA
                   movwf     EECON2                      ;Write AAh
                   bsf       EECON1, WR                  ;Set WR bit to begin the write          

                   banksel   EECON1
write_loop2        btfsc     EECON1,WR                   ;Has the write completed?
                   goto      write_loop2                  ;If not, wait until it has completed.

                   banksel   EECON1
                   bcf       EECON1, WREN                ;Disable writes
   
                   ; Add processing code here.
                                     
                   goto       rcvr

;******************************************************************************
; receive_byte()
;
; Description:  This subroutine receives a serial byte and returns it
;               in the W register.
;
; Inputs:       None
;
; Outputs:      W register -> Contains the received serial byte
;******************************************************************************

receive_byte
                   banksel   PIR1
receive_byte_loop
                   btfss     PIR1,RCIF           ; Has a serial byte been received?
                   goto      receive_byte_loop   ; If not, wait until it has.
                   banksel   RCREG
                   movf      RCREG,W             ; Put the received byte into the W register.
                   banksel   INTCON              ; Darn banksels 
                   bcf       INTCON,T0IF         ; this will clear the timer0 interrupt flag 
                   return                        ; Return from subroutine.


;*********************************************************************************
;highbyte_lost()
;
;Description:   This subroutine executes if a time out occurs due to high_byte is 
;               never recieved, This will turn on all of PORTD leds.   
;
;Inputs:         
;
;Outputs:        Turns on PORTD leds       
;**********************************************************************************
highbyte_lost
                                  
                  movlw     0x00
                  banksel   TRISD
                  movwf     TRISD
led_loop                 
                  movlw     0xFF
                  banksel   PORTD
                  movwf     PORTD
                  goto      led_loop
                  



;*********************************************************************************
; EEPROM Data
;**********************************************************************************

            ;       org       0x2100

             ;      de        0x55

;******************************************************************************
; End of program
;******************************************************************************

                   end

