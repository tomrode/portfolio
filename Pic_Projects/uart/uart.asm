                                 list      p=16f887
                   #include  <p16f887.inc>

;******************************************************************************
; Device Configuration
;******************************************************************************

                   __CONFIG  _CONFIG1, _LVP_OFF & _FCMEN_ON & _IESO_OFF & _BOR_OFF & _CPD_OFF & _CP_OFF & _MCLRE_ON & _PWRTE_ON & _WDT_OFF & _INTRC_OSC_NOCLKOUT
                   __CONFIG  _CONFIG2, _WRT_OFF & _BOR21V



;******************************************************************************
; Variable Definitions
;******************************************************************************
tom_char_byte2           equ       0x7D
high_byte1               equ       0x7E
low_byte0                equ       0x7F

;*****************************************************************************
;EEPROM Data
;*****************************************************************************
eeprom_high_byte1               equ       0x01
eeprom_low_byte0                equ       0x00
 
;******************************************************************************
; Reset Vector
;******************************************************************************

                   org       0x000
                   nop
                   goto      main

;******************************************************************************
; Interrupt Vector
;******************************************************************************

                   org       0x004
                   retfie                        ; Return from interrupt.

;******************************************************************************
; Main Program
;******************************************************************************

main
                   movlw     0x61                ; Select internal 4 MHz clock.
                   banksel   OSCCON
                   movwf     OSCCON
wait_stable
                   btfss     OSCCON,HTS          ; Is the high-speed internal oscillator stable?
                   goto      wait_stable         ; If not, wait until it is.

                   movlw     0x00                ; Disable all interrupts.
                   banksel   INTCON
                   movwf     INTCON
                   banksel   PIE1
                   movwf     PIE1

                   banksel   TRISC
                   bsf       TRISC,7             ; RC7 is an input (RX).
                   bcf       TRISC,6             ; RC6 is an output (TX).

                   movlw     0x00                ; SCKP = 0, BRG16 = 0 (8-BIT), WUE = 0, ABDEN = 0
                   banksel   BAUDCTL
                   movwf     BAUDCTL

                   movlw     25                  ; SRBR (LSB) = 25
                   banksel   SPBRG
                   movwf     SPBRG
                   movlw     0                   ; SPBR (MSB) = 0
                   banksel   SPBRGH
                   movwf     SPBRGH

                   movlw     0x80                ; SPEN = 1, RX9 = 0, SREN = 0 (DONT CARE), CREN = 0 (RECEIVER OFF), ADDEN = 0
                   banksel   RCSTA
                   movwf     RCSTA

                   movlw     0x04                ; CSRC = 0, TX9 = 0, TXEN = 0, SYNC = 0 (ASYNC MODE), SENDB = 0, BRGH = 1 (HIGH SPEED), TX9D = 0
                   banksel   TXSTA
                   movwf     TXSTA
 
                   banksel   RCSTA
                   bsf       RCSTA,4             ; CREN = 1 (RECEIVER ON)

                   movlw     0x00
                   banksel   TRISD
                   movwf     TRISD

                   movlw     0x00
                   banksel   PORTD
                   movwf     PORTD
                   
                   movlw     0x00
                   banksel   TRISB
                   movwf     TRISB                  
 
                   movlw     0x00
                   banksel   PORTB
                   movwf     PORTB

;;;;;;;;;;;;;;;;;;;;;;;;;Start polling FOR SERIAL DATA BYTE NUMBER 1;;;;;;;;;;;;;;;;;

rcvr
                   banksel   PIR1
Rcif_loop1
                   btfss     PIR1,5                       ; RCIF Flag is set witht he 9th bit from the RSR to the receive buffer
                   goto      Rcif_loop1
                   banksel   RCREG
                   movf      RCREG,W
     
                   banksel   PORTD
                   movwf     PORTD

                   banksel   low_byte0 
                   movwf     low_byte0

;;;;;;;;;;;;;;;;;;;;;;;;;;EEPROM write from data sheet page 116 For Byte number 1;;;;;;;                  
                   banksel   eeprom_low_byte0
                   movlw     eeprom_low_byte0          ;This will be setting up address 0 in EEPROM 
                   
                   banksel   EEADR
                   movwf     EEADR                       ;Data Memory Address to write
             
                   banksel   eeprom_low_byte0
                   movf      eeprom_low_byte0,W
                   banksel   EEDAT
                   movwf     EEDAT                       ;Data Memory Value to write
                   
                   banksel   EECON1 
                   bcf       EECON1, EEPGD               ;Point to DATA memory
                   bsf       EECON1, WREN                ;Enable writes

                   banksel   EECON2
                   movlw     0x55
                   movwf     EECON2                      ;Write 55h
                   movlw     0xAA
                   movwf     EECON2                      ;Write AAh
                   bsf       EECON1, WR                  ;Set WR bit to begin the write          

                   banksel   EECON1
write_loop         btfsc     EECON1,WR                   ;Has the write completed?
                   goto      write_loop                  ;If not, wait until it has completed.

                   banksel   EECON1
                   bcf       EECON1, WREN                ;Disable writes
                   goto      rcvr
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;BYTE NUMBER 2;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;                   
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; timer here;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 

                   banksel   PIR1 
Rcif_loop2
                   btfss     PIR1,5                       ; Check again for the RCIF flag for set second byte
                   goto      Rcif_loop2
                  
                   banksel   RCREG
                   movf      RCREG,W
     
                   banksel   PORTB                        ; test PORTB out
                   movwf     PORTB
                                      

                   banksel   high_byte1
                   movwf     high_byte1 
                   goto rcvr
;;;;;;;;;;;;;;;;;;;;;;;;;;EEPROM write from data sheet page 116 For Byte number 1;;;;;;;                  
        ;           banksel   eeprom_high_byte1
         ;          movlw     eeprom_high_byte1          ;This will be setting up address 0 in EEPROM 
                   
          ;         banksel   EEADR
           ;        movwf     EEADR                       ;Data Memory Address to write
             
            ;       banksel   eeprom_high_byte1
              ;     movf      eeprom_high_byte1,W
             ;      banksel   EEDAT
            ;       movwf     EEDAT                       ;Data Memory Value to write
                   
            ;       banksel   EECON1 
            ;       bcf       EECON1, EEPGD               ;Point to DATA memory
            ;       bsf       EECON1, WREN                ;Enable writes

             ;      banksel   EECON2
             ;      movlw     0x55
             ;      movwf     EECON2                      ;Write 55h
             ;      movlw     0xAA
             ;      movwf     EECON2                      ;Write AAh
             ;      bsf       EECON1, WR                  ;Set WR bit to begin the write          

              ;     banksel   EECON1
;write_loop2        btfsc     EECON1,WR                   ;Has the write completed?
 ;                  goto      write_loop2                  ;If not, wait until it has completed.

  ;                 banksel   EECON1
   ;                bcf       EECON1, WREN                ;Disable writes


                  
                   goto      rcvr                  








 
                                          

;******************************************************************************
; End of program
;******************************************************************************

                   end

 
