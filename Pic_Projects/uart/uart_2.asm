;**********************************************************************
;   This file is a basic code template for assembly code generation   *
;   on the PIC16F887. This file contains the basic code               *
;   building blocks to build upon.                                    *
;                                                                     *
;   Refer to the MPASM User's Guide for additional information on     *
;   features of the assembler (Document DS33014).                     *
;                                                                     *
;   Refer to the respective PIC data sheet for additional             *
;   information on the instruction set.                               *
;                                                                     *
;**********************************************************************
;                                                                     *
;    Filename:	    xxx.asm                                           *
;    Date:                                                            *
;    File Version:                                                    *
;                                                                     *
;    Author:                                                          *
;    Company:                                                         *
;                                                                     *
;                                                                     *
;**********************************************************************
;                                                                     *
;    Files Required: P16F887.INC                                      *
;                                                                     *
;**********************************************************************
;                                                                     *
;    Notes:                                                           *
;                                                                     *
;**********************************************************************


	list		p=16f887	; list directive to define processor
	#include	<p16f887.inc>	; processor specific variable definitions


; '__CONFIG' directive is used to embed configuration data within .asm file.
; The labels following the directive are located in the respective .inc file.
; See respective data sheet for additional information on configuration word.

	__CONFIG    _CONFIG1, _LVP_OFF & _FCMEN_ON & _IESO_OFF & _BOR_OFF & _CPD_OFF & _CP_OFF & _MCLRE_ON & _PWRTE_ON & _WDT_OFF & _INTRC_OSC_NOCLKOUT
	__CONFIG    _CONFIG2, _WRT_OFF & _BOR21V



;***** VARIABLE DEFINITIONS
tom_char    EQU 0x7C        ; variable for the  
w_temp		EQU	0x7D		; variable used for context saving
status_temp	EQU	0x7E		; variable used for context saving
pclath_temp	EQU	0x7F		; variable used for context saving


;**********************************************************************
	ORG     0x000             ; processor reset vector

	nop
    goto    main              ; go to beginning of program


	ORG     0x004             ; interrupt vector location

	movwf   w_temp            ; save off current W register contents
	movf	STATUS,w          ; move status register into W register
	movwf	status_temp       ; save off contents of STATUS register
	movf	PCLATH,w	  ; move pclath register into w register
	movwf	pclath_temp	  ; save off contents of PCLATH register

; isr code can go here or be located as a call subroutine elsewhere

	movf	pclath_temp,w	  ; retrieve copy of PCLATH register
	movwf	PCLATH		  ; restore pre-isr PCLATH register contents
	movf    status_temp,w     ; retrieve copy of STATUS register
	movwf	STATUS            ; restore pre-isr STATUS register contents
	swapf   w_temp,f
	swapf   w_temp,w          ; restore pre-isr W register contents
    retfie                    ; return from interrupt



main
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Initialize 
    MOVLW   0x00        ; Disable all interrupts
    BANKSEL INTCON
    MOVWF   INTCON
    BANKSEL PIE1
    MOVWF   PIE1

    BANKSEL TRISC
    BSF     TRISC,7     ; RC7 is an input (RX)
    BCF     TRISC,6     ; RC6 is an output (TX)

    MOVLW   0x00        ; SCKP = 0, BRG16 = 0 (8-BIT), WUE = 0, ABDEN = 0
    BANKSEL BAUDCTL
    MOVWF   BAUDCTL

    MOVLW   25          ; SRBR (LSB) = 25
    BANKSEL SPBRG
    MOVWF   SPBRG
    MOVLW   0           ; SPBR (MSB) = 0
    BANKSEL SPBRGH
    MOVWF   SPBRGH

    MOVLW   0x80        ; SPEN = 1, RX9 = 0, SREN = 0 (DONT CARE), CREN = 0 (RECEIVER OFF), ADDEN = 0
    BANKSEL RCSTA
    MOVWF   RCSTA

    MOVLW   0x04        ; CSRC = 0, TX9 = 0, TXEN = 0, SYNC = 0 (ASYNC MODE), SENDB = 0, BRGH = 1 (HIGH SPEED), TX9D = 0
    BANKSEL TXSTA
    MOVWF   TXSTA
 
    BANKSEL RCSTA
    BSF     RCSTA,4     ; CREN = 1 (RECEIVER ON)

;test   
 
 ;   movlw    0x00 
   ; BANKSEL  TRISD
   ; movwf    TRISD

   ; movlw    0x55  
   ; BANKSEL  PORTD       
   ; movwf    PORTD

;inc_loop
 ;   incf    PORTD,f
  ;  goto    inc_loopa
;;;;;;;;;;;;;;; receive data
     movlw    0x00
     BANKSEL  TRISD
     movwf    TRISD

Rcvr
     BANKSEL  PIR1
loop
     btfss    PIR1,5      ; RCIF Flag
     goto     loop
     BANKSEL  RCREG
     movf     RCREG,W
     
     
     BANKSEL  PORTD
     MOVWF    PORTD
 
;     BANKSEL  tom_char
;     movwf    tom_char
     goto     Rcvr   

;;;;;;;;;;;;;;;;;


 









; example of preloading EEPROM locations

	ORG	0x2100
	DE	5, 4, 3, 2, 1

	END                       ; directive 'end of program'

