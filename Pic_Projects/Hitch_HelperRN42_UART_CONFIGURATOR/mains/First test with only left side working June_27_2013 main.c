/******************************************************************          
*                    HITCH_HELPER_BlUETOOTH_DISPLAY                             
*              Initial Development 29 MAY 2013 
*                Author Tom Rode  intials -TR            
*
*                   MICRO PIN ASSINMENTS
* PORTC Assignment, This will need to be followed exactly
*                    RC6 = UART Tx pin
*                    RC7 - UART Rx pin
*
*                 26 JUNE 2012
* First test seems to indicate communication with sensor processor but display acting errouniuosly.
* Will disable the interrupts. Made dispaly off initially.
*************************************************************************************************************************/
#include <htc.h>
#include "typedefs.h"
#include "timer.h"
#include "uart_func.h"
/*********************************************************/
/*  Constants used in main
/*********************************************************/
#define BLUETOOTH_DISPLAY_STATE_MACHINE_TIME (2000)
#define INIT_DISPLAY_BLINK
/**********************************************************/
/* Device Configuration
/**********************************************************/  
      /*PIC16F887*/
__CONFIG(DEBUGEN & LVPEN & FCMEN & IESODIS & BORXSLP & DUNPROTECT & UNPROTECT & MCLREN & PWRTEN & WDTDIS & INTCLK); //HS means resonator 8Mhz
__CONFIG(BORV40);        
      
 /* Global define for this module*/      

/**********************************************************/
/* Function Prototypes 
/**********************************************************/
int main(void);
void OutLedBox(UART_RECEIVE_SENSOR_STUCTURE UartIn);
void init_micro(void);                  /* SFR inits*/

/***********************************************************/
/* Global Variable Definition in module
/***********************************************************/
uint8_t extern_var;
GeneralFlagsSettingType FlagSet;



/*******************************************************************************
* main()
* Description:     The main function overall. This runs infinately(etc).
* INPUTS: NONE
* OUTPUTS:NONE
*******************************************************************************/ 
int main(void)
{
/************************************************************/
/* LOCAL MAIN VARIABLES                                     */
/************************************************************/
UART_RECEIVE_SENSOR_STUCTURE CompleteUartTransfer;
uint8_t INIT_STATE_MACHINE;
enum
   { 
     LEFT_SIDE,
     RIGHT_SIDE,
     FWD_BCK,
     OFF_DISPLAY_INIT,
     EXIT_INIT
   }INIT_BLINKS_STATES;
/************************************************************/
/* STUCTURES FLAGS IN MAIN
/************************************************************/

/***********************************************************/
/* INITIALIZATIONS                                         */
/***********************************************************/ 
init_micro();                                               /* Initialize SFR*/ 
uart_init();
TMR1IE = 1;                                                 /* Timer 1 interrupt enable*/
timer_init();                                               /* clear out timer one*/
PEIE = 1;                                                   /* enable all peripheral interrupts*/
ei();                                                       /* enable all interrupts*/
timer_set(DISPLAY_BLUETOOTH_TIMER, BLUETOOTH_DISPLAY_STATE_MACHINE_TIME);   /* This will have to be tested for 5 micro seconds adjust the tmer isr*/  
INIT_STATE_MACHINE = LEFT_SIDE;
PORTB = 0, PORTD = 0, PORTE = 0;
/************************************************************/
/*  MAIN LOOP OVERALL                                       */
/************************************************************/
//while (HTS == 0)                                             /* wait until internal clock stable*/ 
//{} 
#ifdef INIT_DISPLAY_BLINK 
while(INIT_STATE_MACHINE != EXIT_INIT)
{  
 switch (INIT_STATE_MACHINE)  
 {
   case LEFT_SIDE:
        if(get_timer(DISPLAY_BLUETOOTH_TIMER) == 0)
        {
          INIT_STATE_MACHINE = RIGHT_SIDE;
          timer_set(DISPLAY_BLUETOOTH_TIMER, BLUETOOTH_DISPLAY_STATE_MACHINE_TIME);
        }
        else
        {
          INIT_STATE_MACHINE = LEFT_SIDE;
          PORTD = 0xFC;                       /* On All To indicate all left side 6 leds*/
        }
        break;

   case RIGHT_SIDE:
        if(get_timer(DISPLAY_BLUETOOTH_TIMER) == 0)
        {
          INIT_STATE_MACHINE = FWD_BCK;
          timer_set(DISPLAY_BLUETOOTH_TIMER, BLUETOOTH_DISPLAY_STATE_MACHINE_TIME);
        }
        else
        {
          INIT_STATE_MACHINE = RIGHT_SIDE;
          PORTB = 0x3C;                       /* On All To indicate all left side 4 leds*/
          PORTE = 0x03;                       /* and the far left two*/
        }
        break;

   case FWD_BCK:
        if(get_timer(DISPLAY_BLUETOOTH_TIMER) == 0)
        {
          INIT_STATE_MACHINE = OFF_DISPLAY_INIT;
          timer_set(DISPLAY_BLUETOOTH_TIMER, BLUETOOTH_DISPLAY_STATE_MACHINE_TIME);
        }
        else
        {
          INIT_STATE_MACHINE = FWD_BCK;
          PORTB = 0xFF;                       /* all on*/
          PORTD = 0xFF;                       /* all on*/
          PORTE = 0xFF;                       /* all on*/
        }
        break;

    case OFF_DISPLAY_INIT:
         if(get_timer(DISPLAY_BLUETOOTH_TIMER) == 0)
         {
           PORTB = 0;                          /* All off*/
           PORTD = 0;
           PORTE = 0;
           INIT_STATE_MACHINE = EXIT_INIT;
           di();                               /* May be the cause of erroniously activity*/
         }
         else
        {
          INIT_STATE_MACHINE = OFF_DISPLAY_INIT;
        } 
        break;

      default:
        break;
 }
} 
#endif /*INIT_DISPLAY_BLINK*/                                                 
 while(1)                   
 { 
  CompleteUartTransfer = *FillRecieveSensorBuff();          /* Get the Buffer filled fro the Uart */
  OutLedBox(CompleteUartTransfer);                          /* Now Display it*/
  asm("nop");
 }
}
/*******************************************************************************
*OutLedBox()
*Description:  This function willl take in the value for Structure 
*              CompleteUartTransfer and desplay onto old hitch helper box
*              This box has the follow wich will be my naming convention:
*INPUTS:  UART_RECEIVE_SENSOR_STUCTURE type structure
*OUTPUTS: NONE
*NOTES:
*                          LOOK AT HITCH HELPER BOX FORWARD
*                       L6 L5 L4 L3 L2 L1 F1 R1 R2 R3 R4 R5 R6
*                        O  O  O  O  O  O  O  O  O  O  O  O  O           
*                                       F2 O
*                                       F3 O
*                                       F4 O
*               PORTD assignment     PORTB assignment
*               RD7 = L6             RB7 = R6 Now RE0 = R6
*               RD6 = L5             RB6 = R5 Now RE1 = R5
*               RD5 = L4             RB5 = R4
*               RD4 = L3             RB4 = R3
*               RD3 = L2             RB3 = R2
*               RD2 = L1             RB2 = R1
*               RD1 = F1             RB1 = F3
*               RD0 = F2             RB0 = F4
*               Sensor1 = Fwd/Back
*               Sensor2 = Right
*               Sensor3 = Left
*Changes: 24 JUNE 2013 remapped RE0 From RB7 and RE1 from RB6 because ICSP shares portb pins.
*        HiVal SENSOR1           LoVal SENSOR1    HiVal SENSOR2           LoVal SENSOR2    HiVal SENSOR3           LoVal SENSOR3       
*   uart_tx(PulseWidth1CurrentCount,Sensor1FineCount,PulseWidth2CurrentCount,Sensor2FineCount,PulseWidth3CurrentCount,Sensor3FineCount);
*******************************************************************************/
void OutLedBox(UART_RECEIVE_SENSOR_STUCTURE UartIn)
{
 /* Process Sensor 3 Left side indication*/ 
 if(UartIn.Sen3HiValUart > 6)
 {
   PORTD &= 0x03;                    /* Maintain RD1 = F1 and RD0 = F2*/ 
   PORTD ^= 0xFC;                    /* On All To indicate out of range*/ 
 }
 if(UartIn.Sen3HiValUart <= 6)
 {
   PORTD &= 0x03;                    /* Maintain RD1 = F1 and RD0 = F2*/
   PORTD ^= 0x80;                    /* On RD7 = L6*/
 }
 if(UartIn.Sen3HiValUart <= 5)
 {
   PORTD &= 0x03;                    /* Maintain RD1 = F1 and RD0 = F2*/
   PORTD ^= 0x40;                    /* On RD6 = L5*/
 }
 if(UartIn.Sen3HiValUart <= 4)
 {
   PORTD &= 0x03;                    /* Maintain RD1 = F1 and RD0 = F2*/
   PORTD ^= 0x20;                    /* On RD5 = L4*/
 }
 if(UartIn.Sen3HiValUart <= 3)
 {
   PORTD &= 0x03;                    /* Maintain RD1 = F1 and RD0 = F2*/
   PORTD ^= 0x10;                    /* On RD4 = L3*/
 }
 if(UartIn.Sen3HiValUart <= 2)
 {
   PORTD &= 0x03;                    /* Maintain RD1 = F1 and RD0 = F2*/
   PORTD ^= 0x08;                    /* On RD3 = L2*/
 }
 if(UartIn.Sen3HiValUart <= 1)
 {
   PORTD &= 0x03;                    /* Maintain RD1 = F1 and RD0 = F2*/
   PORTD ^= 0x04;                    /* On RD2 = L1*/
 }
 /* Process Sensor 2 Right side indication*/ 

 if(UartIn.Sen2HiValUart > 6)
 {
   PORTB &= 0x03;                    /* Maintain RB1 = F3 and RB0 = F4*/
   PORTB ^= 0xFC;                    /* On All To indicate out of range*/
   PORTE  = 0x03;                    /* Replacement for RB6 and RB7*/ 
 }
 if(UartIn.Sen2HiValUart <= 6)
 {
   PORTB &= 0x03;                    /* Maintain RB1 = F3 and RB0 = F4*/
   PORTB ^= 0x80;                    /* On RB7 = R6*/
   PORTE  = 0x01;                    /* Replacement for RB6 and RB7*/ 
 }
 if(UartIn.Sen2HiValUart <= 5)
 {
   PORTB &= 0x03;                    /* Maintain RB1 = F3 and RB0 = F4*/
   PORTB ^= 0x40;                    /* On RB6 = R5*/
   PORTE  = 0x02;                    /* Replacement for RB6 and RB7*/ 
 }
 if(UartIn.Sen2HiValUart <= 4)
 {
   PORTB &= 0x03;                    /* Maintain RB1 = F3 and RB0 = F4*/
   PORTB ^= 0x20;                    /* On RB5 = R4*/
   PORTE  = 0x00;                    /* Replacement for RB6 and RB7*/ 
 }
 if(UartIn.Sen2HiValUart <= 3)
 {
   PORTB &= 0x03;                    /* Maintain RB1 = F3 and RB0 = F4*/
   PORTB ^= 0x10;                    /* On RB4 = R3*/
   PORTE  = 0x00;                    /* Replacement for RB6 and RB7*/ 
 }
 if(UartIn.Sen2HiValUart <= 2)
 {
   PORTB &= 0x03;                    /* Maintain RB1 = F3 and RB0 = F4*/
   PORTB ^= 0x08;                    /* On RB3 = R2*/
   PORTE  = 0x00;                    /* Replacement for RB6 and RB7*/ 
 }
 if(UartIn.Sen2HiValUart <= 1)
 {
   PORTB &= 0x03;                    /* Maintain RB1 = F3 and RB0 = F4*/
   PORTB ^= 0x04;                    /* On RB2 = R1*/
   PORTE  = 0x00;                    /* Replacement for RB6 and RB7*/ 
 }
/* Process Sensor 1 Fwd/Bck indication*/ 
 if(UartIn.Sen1HiValUart > 4)
 { 
   PORTB &= 0xFC;                    /* Maintain Right side indication RB7-RB2*/
   PORTD &= 0xFC;                    /* Maintain Left side indication RD7-RD2*/
   PORTB ^= 0x03;                    /* On All To indicate out of range*/  
   PORTD ^= 0x03;                    /* On All To indicate out of range*/ 
 }
 if(UartIn.Sen1HiValUart <=4)
 {
   PORTB &= 0xFC;                    /* Maintain Right side indication RB7-RB2*/
   PORTD &= 0xFC;                    /* Maintain Left side indication RD7-RD2*/
   PORTB ^= 0x01;                    /* On RB0 = F4*/ 
 }
 if(UartIn.Sen1HiValUart <=3)
 {
   PORTB &= 0xFC;                    /* Maintain Right side indication RB7-RB2*/
   PORTD &= 0xFC;                    /* Maintain Left side indication RD7-RD2*/
   PORTB ^= 0x02;                    /* On RB1 = F3*/ 
 }
 if(UartIn.Sen1HiValUart <=2)
 {
   PORTD &= 0xFC;                    /* Maintain Left side indication RD7-RD2*/
   PORTB &= 0xFC;                    /* Maintain Right side indication RB7-RB2*/
   PORTD ^= 0x01;                    /* On RD0 = F2*/ 
 }
 if(UartIn.Sen1HiValUart <=1)
 {
   PORTD &= 0xFC;                    /* Maintain Left side indication RD7-RD2*/
   PORTB &= 0xFC;                    /* Maintain Right side indication RB7-RB2*/
   PORTD ^= 0x02;                    /* On RD1 = F1*/ 
 }
}
/*******************************************************************************
* init_micro()
* Descrption: Application and Micro Specific Specical Function Register 
*             Settings. Peripherals,ports(etc).
* INPUTS: NONE
* OUTPUTS:NONE
********************************************************************************/ 
void init_micro(void)
{
 FlagSet.ClearAllGeneralFlagBits = 0;
 OSCCON     = 0x71;                    /* 8 Mhz clock*/
 //WDTCON     = 0x15;                  /* WDTCON � � � WDTPS3=1 WDTPS2=0 WSTPS1=1 WDTPS0=1 SWDTEN=1 -> WDT turned on and prescaller value is 1:32768 1160 ms was 1:16384 or 580 ms to pet watchdog at 8 Mhz*/
 OPTION     = 0x08;                    /*|PSA=Prescale to WDT|WD rate 1:1|PS2=0|PS1=0|PS0=0| 20ms/Div*/
 ANSEL      = 0x00;                    /* Make pins digital*/
 ANSELH     = 0x00;                    /* Make pins digital*/
 TRISA      = 0x00;                    /* Indicate forward or back*/
 PORTA      = 0x00;                    /* Turn ofo All portA*/
 TRISB      = 0x00;                    /* Either left or right side */
 //TRISC      = 0x06;                    /* RC7=UART sets|RC6=UART sets|RC5=0|RC4=0|RC3=0|RC2=1|RC1=1|RC0=0| 1=input, 0=output*/
 TRISD      = 0x00;                    /* Either left or right side */
 TRISE      = 0x00;                    /* RE0 Output*/
 T1CON      = 0x31;                    /* T1CKPS1,T1CKPS0 = 1:8 prescaler,T1SYNC = do not synchronise external clock input ,TMR1ON = enabled*/ 
} 

