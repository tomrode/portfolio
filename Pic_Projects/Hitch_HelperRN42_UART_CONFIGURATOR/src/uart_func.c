
#include <htc.h>
#include "uart_func.h"
#include "typedefs.h"

/* FUTURE USE TO ENCODE SPECIFIC BYTES WHILE MAINTAINING ENOUGH PAYLOAD CAPABILITY*/
#define SENSOR1_HI_DECODE   (0x20)
#define SENSOR2_HI_DECODE   (0x40)
#define SENSOR3_HI_DECODE   (0x80)
#define UART_INTERRUPT_DESIRED

     
    
/* Global variables to this module*/
static uint8_t uartrx_isr_flag;
static uint8_t uarttx_isr_flag;
#ifdef UART_TX_ENABLED
/*****************************************************************************************************/
/* function name: uart_loopbakc()
/* DATE 27 Aug 2013
/* DESCRIPTION: send some bytes out uart.
/*
/* INPUT1-> Ptr to array. 
/* INPUT2-> Length. 
/* OUTPUTS-> None
/*                         
/*****************************************************************************************************/
void uart_loopback(void)
{
uint8_t temp;
static uint8_t i;

//di();                                        /* Nothing should interrupt putting this into a polling mode*/ 

   temp = RCREG;
   while((RCIF == 1)&& (OERR != 1))
   {
     /* Wait until flag clears, There is a character in the FIFO */
   }  
   TXREG = temp;                          /* Send out byte */ 
   while (TRMT == 0)                          /* TMRT = transmit buffer Reg Empty flag*/                   
    {
       /* Wait here until transmittion is complete */
    }
   temp = 0;
 // _delay(2000);                             /* 1000 around 330 micro seconds */
//}while(CREN != 1);

//ei();
}




/*****************************************************************************************************/
/* function name: uart_tx()
/* DATE 27 Aug 2013
/* DESCRIPTION: send some bytes out uart.
/*
/* INPUT1-> Ptr to array. 
/* INPUT2-> Length. 
/* OUTPUTS-> None
/*                         
/*****************************************************************************************************/
void uart_tx(uint8_t *buffer, uint8_t length)
{
uint8_t i;
static uint8_t temp[MAX_UART_BYTES];
static uint8_t ActualReceived1[MAX_UART_BYTES];
di();                                        /* Nothing should interrupt putting this into a polling mode*/ 
RN42CTSFLOWCONTROL = FALSE; 
 for(i=0; i<(length); i++)
  {  
    temp[i] = *buffer;  
    TXREG = temp[i];                          /* Send out byte */ 
    while (TRMT = 0)                          /* TMRT = transmit buffer Reg Empty flag*/                   
    {
       /* Wait here until transmittion is complete */
    }
    ActualReceived1[i] = RCREG;
    while ((RCIF == 1) && (OERR != 1))
   {
     /* Wait until flag clears, There is a character in the FIFO */
   }  
     buffer++; 
     _delay(2000);                             /* 1000 around 330 micro seconds */
  } 
RN42CTSFLOWCONTROL = TRUE;  
ei();
}

#endif /* UART_TX_ENABLED*/
/*********************************************************************************************/
/* function name: 27 AUG 20134 DEC 2011
/* DESCRIPTION: 
/*
/* INPUTS  -> NONE
/* OUTPUTS -> 8-bit received read (in this case from CAPL)
/* Update history:
/* - 21 DEC 2011: Fixed the lock up added in a test for overrun if condition then 
/*                re-initialize the UART.
/* - 4 MAY 2013   Will use this with PIC16F887 Not sure if this overrun flag will give issue's
/*                will poll   
/*********************************************************************************************/
const uint8_t Dummy[MAX_UART_BYTES] = {"BADBEEF"}; 
uint8_t *uart_rx(void)
{

static uint8_t ActualReceived[MAX_UART_BYTES];
uint8_t i;
 di();
 //RN42CTSFLOWCONTROL = FALSE;                          /* Make pin 16 CTS low on RN42XV to allow comm , RTS is pin12 also on RN42XV*/
for(i=0; i<MAX_UART_BYTES; i++)
{
  //ActualReceived[i] = Dummy[i];
 while(RCIDL == TRUE)                                 /* Wait until the Master sends out */
 {/* Wait until a byte received*/}
    ActualReceived[i]  = RCREG;                       /* init a receive */   
  if (OERR ==1)
  {
  //  CREN = 0;                                       /* Shut off Receive */
  //  uart_init();                                    /* try re-initialise*/ 
  }
  else
  {
   while ((RCIF == 1) && (OERR != 1))
   {
     /* Wait until flag clears, There is a character in the FIFO */
   } 
  }
} 
// RN42CTSFLOWCONTROL = TRUE;
 ei();

return (&ActualReceived);  
}

/*********************************************************************************************
* uart_init()
* Description: Initialize Universal asynchronous receive and transmitt module.
* 
* INPUTS: None
* OUTPUTS: None
* 
*         NOTES:
*            28 JUNED 2013
* Changed SPBGR timing, found the higher the baud rate the better results from the pulsewidthx
* return.
**********************************************************************************************/
void uart_init(void)
{
//RC6 44 � � � � TX/CK � � � �
//RC7 1 � � � � RX/DT
//TRISC    = 0x80;       /* Pin 26 RC7/RX/DT  PIN 25 Rc6/TX/DT 
//BRG16      = TRUE;              /* Set for higher frequency with SPBRG set to 16 decimal should make 117.6k Hz*/
//SPBRG      = 0x10;
SPBRG      = 0x33;       /*08 made 18us or 55555 bps 19 made 52.2 us scope 19200 baud says 33 for 104 us Baud rate = FOSC/(16(X+1))  "51" at 8MHz at 9600 baud "25" at 8MHz at 19200 baud 33 made 9600 work on Arduino */ 
TXSTA      = 0x24;       /* |CSRC=0|TX9=0|TXEN=1|SYNC=0| -  |BRGH=1|TRMT=0|TX9D=0| */
RCSTA      = 0x90;       /* |SPEN=1(Serial port enabled)|RX9=0|SREN=0|CREN=1(RECEIVER ON)|ADDEN=0|FERR=0|OERR=0|RX9D=0|*/
}


#ifdef UART_INTERRUPT_DESIRED
void uartrx_isr(void)
{

  if ((RCIF==1)&&(RCIE==1))
  {
    RCIF=0;             // cleared by hardware
    uartrx_isr_flag=1;  // set the indication 
  }
  else 
  {
  }
}

 


void uarttx_isr(void)
{
  if ((TXIF)&&(TXIE))
  {
    
    TXIF=0;             // clear flag
    uarttx_isr_flag=1;  // set the indication 
     
  }
  else 
  {
  }
}
#endif //UART_INTERRUPT_DESIRED
