/******************************************************************          
*                    HITCH_HELPER_RN42_UART_CONFIGURATOR                            
*              Initial Development 27 AUG 2013 
*                Author Tom Rode  intials -TR            
*
*                   MICRO PIN ASSINMENTS
* PORTC Assignment, This will need to be followed exactly
*                    RC6 = UART Tx pin
*                    RC7 - UART Rx pin
*
* 27 AUG 2013, Attempt to send at 8 MHZ 110K baud to RN-42 device
*              to hopefully get it out of HID mode.
*******************************************************************/
#include <htc.h>
#include "typedefs.h"
#include "timer.h"
#include "uart_func.h"
/*********************************************************/
/*  Constants used in main
/*********************************************************/
#define UART_TRANSITION_TIME (2000)
#define INIT_DISPLAY_BLINK
const uint8_t EnterCommandMode[] = {'$','$','$'};//,0x0D};
const uint8_t SetSPPProfile[]    = {'s','~',',','0',0x0D};
const uint8_t SetReboot[]        = {'R',',','1',0x0D};
const uint8_t LoopBackTest[]     = {"_This message is from PIC 16f887 as a test."};
/**********************************************************/
/* Device Configuration
/**********************************************************/  
      /*PIC16F887*/
__CONFIG(DEBUGEN & LVPEN & FCMEN & IESODIS & BORXSLP & DUNPROTECT & UNPROTECT & MCLREN & PWRTEN & WDTDIS & INTCLK); //HS means resonator 8Mhz
__CONFIG(BORV40);        
      
 /* Global define for this module*/      

/**********************************************************/
/* Function Prototypes 
/**********************************************************/
int main(void);

void init_micro(void);                  /* SFR inits*/

/***********************************************************/
/* Global Variable Definition in module
/***********************************************************/
uint8_t extern_var;
GeneralFlagsSettingType FlagSet;

enum
{
  TRANSMIT,
  RECEIVE
}UARTSTATES;

/*******************************************************************************
* main()
* Description:     The main function overall. This runs infinately(etc).
* INPUTS: NONE
* OUTPUTS:NONE
*******************************************************************************/ 
int main(void)
{
/************************************************************/
/* LOCAL MAIN VARIABLES                                     */
/************************************************************/

static uint8_t RecieveFromUart[MAX_UART_BYTES];
static uint8_t UartStateMachine;
uint8_t temp,i = 0;
uint8_t *RcvPtr;
int test;

/************************************************************/
/* STUCTURES FLAGS IN MAIN
/************************************************************/

/***********************************************************/
/* INITIALIZATIONS                                         */
/***********************************************************/ 
//test = ObjFunc(4,5);
init_micro();                                               /* Initialize SFR*/ 
uart_init();
TMR1IE = 1;                                                 /* Timer 1 interrupt enable*/
timer_init();                                               /* clear out timer one*/
PEIE = 1;                                                   /* enable all peripheral interrupts*/
ei();                                                       /* enable all interrupts*/
timer_set(UART_TX_RX_TIMER,UART_TRANSITION_TIME);          /* This will have to be tested for 5 micro seconds adjust the tmer isr*/  
UartStateMachine = TRANSMIT;
PORTC = 0, PORTD = 1, PORTE = 0;
/************************************************************/
/*  MAIN LOOP OVERALL                                       */
/************************************************************/
//while (HTS == 0)                                             /* wait until internal clock stable*/ 
//{} 
                                              
 while(1)                   
 {
  switch( UartStateMachine )
  {
    case  TRANSMIT:
      if(get_timer(UART_TX_RX_TIMER) == 0)
      {  
        //test = sizeof(LoopBackTest);
        uart_tx(&LoopBackTest[i],test);    /* Send out the command to RN42*/
        //uart_loopback();
        timer_set(UART_TX_RX_TIMER, UART_TRANSITION_TIME);
        UartStateMachine = TRANSMIT;
       
      }
      else
      {
         UartStateMachine = TRANSMIT;
      }
         
     break;
      
    case  RECEIVE:
      if(get_timer(UART_TX_RX_TIMER) == 0)
      {
        RcvPtr = uart_rx();                                     /* Get the result if RN42 sent it*/
        for(i=0; i<sizeof(LoopBackTest); i++)
        {
          RecieveFromUart[i] = *RcvPtr;
          RcvPtr++;
        }
        timer_set(UART_TX_RX_TIMER,UART_TRANSITION_TIME);
        UartStateMachine = TRANSMIT;
      }  
       else
       {
          UartStateMachine = RECEIVE;
       }
       
     break;
      
    default:
      break;      
  }
 }/*End of while (1)*/
}/* End of main*/

/*******************************************************************************
* init_micro()
* Descrption: Application and Micro Specific Specical Function Register 
*             Settings. Peripherals,ports(etc).
* INPUTS: NONE
* OUTPUTS:NONE
********************************************************************************/ 
void init_micro(void)
{
 FlagSet.ClearAllGeneralFlagBits = 0;
 OSCCON     = 0x71;                    /* 8 Mhz clock*/
 //WDTCON     = 0x15;                  /* WDTCON � � � WDTPS3=1 WDTPS2=0 WSTPS1=1 WDTPS0=1 SWDTEN=1 -> WDT turned on and prescaller value is 1:32768 1160 ms was 1:16384 or 580 ms to pet watchdog at 8 Mhz*/
 OPTION     = 0x08;                    /*|PSA=Prescale to WDT|WD rate 1:1|PS2=0|PS1=0|PS0=0| 20ms/Div*/
 ANSEL      = 0x00;                    /* Make pins digital*/
 ANSELH     = 0x00;                    /* Make pins digital*/
 TRISA      = 0x00;                    /* Indicate forward or back*/
 PORTA      = 0x00;                    /* Turn ofo All portA*/
 TRISB      = 0x00;                    /* Either left or right side */
 TRISC      = 0x00;                    /* RC7=UART sets|RC6=UART sets|RC5=0|RC4=0|RC3=0|RC2=0|RC1=0|RC0=0| 1=input, 0=output*/
 TRISD      = 0x00;                    /* Either left or right side */
 TRISE      = 0x00;                    /* RE0 Output*/
 T1CON      = 0x31;                    /* T1CKPS1,T1CKPS0 = 1:8 prescaler,T1SYNC = do not synchronise external clock input ,TMR1ON = enabled*/ 
} 

