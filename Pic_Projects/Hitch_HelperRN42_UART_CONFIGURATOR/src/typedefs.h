/*****************************************************************************/
/* FILE NAME:  typedefs.h                                                    */
/*                                                                           */
/* Description:  Standard type definitions.                                  */
/*****************************************************************************/

#ifndef TYPEDEFS_H
#define TYPEDEFS_H


enum
{
  FALSE,                       // implied to be zero
  TRUE,                        // implied to be one
  DEFAULT
};



typedef signed char sint8_t;
typedef unsigned char uint8_t;
typedef volatile signed char vsint8_t;
typedef volatile unsigned char vuint8_t;

typedef signed short sint16_t;
typedef unsigned short uint16_t;
typedef volatile signed short vsint16_t;
typedef volatile unsigned short vuint16_t;

typedef signed long sint32_t;
typedef unsigned long uint32_t;
typedef volatile signed long vsint32_t;
typedef volatile unsigned long vuint32_t;

/******************************************************************/
/* Global variables to entire project                             */
/*                                                                */
/*                                                                */       
/******************************************************************/
extern uint8_t extern_var;
typedef union 
{
  struct
      {
        uint8_t Flag1     : 1;
        uint8_t Flag2     : 1;
        uint8_t Flag3     : 1;
        uint8_t Flag4     : 1;
        uint8_t Flag5     : 1;
        uint8_t Flag6     : 1;
        uint8_t padding   : 2;
       }GeneralFlags;
       uint8_t ClearAllGeneralFlagBits;
	}GeneralFlagsSettingType; 

typedef struct
     {
	    uint8_t Sen1HiValUart;
      //uint8_t Sen1LoValUart;
		uint8_t Sen2HiValUart;
	  //uint8_t Sen2LoValUart;
        uint8_t Sen3HiValUart;
	 //uint8_t Sen3LoValUart;
    }UART_RECEIVE_SENSOR_STUCTURE;

typedef struct
     {
	    uint8_t UartRcvAr[7];
     }RN42_UART_RECEIVE_STUCTURE;

#endif
