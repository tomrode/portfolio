#include <htc.h>
#include "typedefs.h"

#ifndef UART_H
#define UART_H
#define UART_TX_ENABLED
#define MAX_UART_BYTES      (50)
#define RN42CTSFLOWCONTROL  (RC5)

/* Function prototypes */
void uart_loopback(void);
void uart_tx(uint8_t *buffer, uint8_t length);
uint8_t *uart_rx(void);
void uart_init(void);
void uarttx_isr(void);
void uartrx_isr(void);
UART_RECEIVE_SENSOR_STUCTURE* FillRecieveSensorBuff(void);
UART_RECEIVE_SENSOR_STUCTURE* FillRecieveSensorBuff3Bytes(void);

#endif


