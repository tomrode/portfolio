
#include "typedefs.h"
#include "htc.h"

#ifndef TIMER_H
#define TIMER_H

/*Function Prototypes*/
uint16_t get_timer(uint8_t index);             // Which timer to use out of enum
void timer_set(uint8_t index, uint16_t value); 
void timer_isr(void);
void timer_init(void);

enum sw_timers_e
{
UART_TX_RX_TIMER,                  /* Time setting for firing the Ultra sonic sensor*/
TIMER_MAX
};
#endif
