#include <htc.h>

/***********************************************************/
/* Variable Definition
/***********************************************************/
extern volatile char counter;

/***********************************************************/
/* isr function
/***********************************************************/ 
void interrupt my_isr(void)
{
 if((TMR1IE)&&(TMR1IF))
    {
     INTCON = 0x00;  //Globaly and peripheraly disable all interruputs
     counter++;
     TMR1IF=0;
     INTCON = 0x40;  //Re-enable peripheral interrupts
    }
}
      