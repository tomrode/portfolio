/**********************************************************/
/* Sample program from HI-TECH quick start .pdf           */
/* 1 Jun 2011 using PIC16F877A                            */ 
/* blink PORT B and I put in my own variable              */
/**********************************************************/
#include <htc.h>
/**********************************************************/
/* Device Configuration
/**********************************************************/
       __CONFIG(XT & WDTDIS & PWRTDIS & BORDIS & LVPEN & WRTEN &
       DEBUGEN & DUNPROT & UNPROTECT & HS);
       __CONFIG(BORV40);
       //extern bit _HS_OSC;  //High speed oscillator, resonator 

/**********************************************************/
/* Function prototype definitions
/**********************************************************/
void init(void);
void main(void);

/**********************************************************/
/* Initialize SFR 
/**********************************************************/
void init(void)
{ 
             
PIE1    = 0x01;       // TMR1IE = enabled 
INTCON  = 0x40;       // PEIE = enabled
OPTION  = 0x80;       // PORTB pull-ups disabled
T1CON   = 0x35;       // T1CKPS1,T1CKPS0 = 1:8 prescaler,T1SYNC = do not synchronise external clock input ,TMR1ON = enabled
TRISD   = 0x00;       // port directions: 1=input, 0=output
TMR1L   = 0xE0;
TMR1H   = 0xFE;
}
/***********************************************************/
/* Variable Definition
/***********************************************************/
volatile char counter;       
unsigned char toms_count;
unsigned int eeprom_low  = 0x0005;
unsigned int eeprom_mid  = 0x0004;
unsigned int eeprom_hi   = 0x0003;
unsigned int ee_copy_low = 0x0025;
unsigned int ee_copy_mid = 0x0024;
unsigned int ee_copy_hi  = 0x0023;
unsigned int ee_copy_low_write;
unsigned int flash_adr   = 0x00A0;
char flash_data;            
void main(void)
{
//flash_data = flash_read(flash_adr);
counter = 0;
toms_count=0;
init();
ei();
ee_copy_low_write=eeprom_read(eeprom_low);   
eeprom_write(ee_copy_low,ee_copy_low_write);  // make a copy initially

while (1)
   {
   PORTD = counter;
  
  if ((counter >= 2)&&(counter <=4))
     {
     //_delay(2000);
     toms_count++;
     eeprom_write(eeprom_low,toms_count); // eeprom write
     }
   else if ((counter >= 5)&&(counter <=8))
     {
      toms_count++;
      eeprom_write(eeprom_mid,toms_count); // eeprom write
     }
     else if ((counter >= 9)&&(counter <=12))   
       {
       toms_count++;
       eeprom_write(eeprom_hi,toms_count); // eeprom write
       } 
       else
        {
        }
   }   


}