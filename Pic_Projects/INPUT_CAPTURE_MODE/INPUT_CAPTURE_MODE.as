opt subtitle "HI-TECH Software Omniscient Code Generator (Lite mode) build 6738"

opt pagewidth 120

	opt lm

	processor	16F887
clrc	macro
	bcf	3,0
	endm
clrz	macro
	bcf	3,2
	endm
setc	macro
	bsf	3,0
	endm
setz	macro
	bsf	3,2
	endm
skipc	macro
	btfss	3,0
	endm
skipz	macro
	btfss	3,2
	endm
skipnc	macro
	btfsc	3,0
	endm
skipnz	macro
	btfsc	3,2
	endm
indf	equ	0
indf0	equ	0
pc	equ	2
pcl	equ	2
status	equ	3
fsr	equ	4
fsr0	equ	4
c	equ	1
z	equ	0
pclath	equ	10
# 10 "d:\users\f53368a\Desktop\Various Documents\pic_projects\INPUT_CAPTURE_MODE\main.c"
	psect config,class=CONFIG,delta=2 ;#
# 10 "d:\users\f53368a\Desktop\Various Documents\pic_projects\INPUT_CAPTURE_MODE\main.c"
	dw 0x1FFF & 0x3FFF & 0x3FFF & 0x3BFF & 0x3EFF & 0x3FFF & 0x3FFF & 0x3FFF & 0x3FEF & 0x3FF7 & 0x3FFD ;#
# 11 "d:\users\f53368a\Desktop\Various Documents\pic_projects\INPUT_CAPTURE_MODE\main.c"
	psect config,class=CONFIG,delta=2 ;#
# 11 "d:\users\f53368a\Desktop\Various Documents\pic_projects\INPUT_CAPTURE_MODE\main.c"
	dw 0x3FFF ;#
	FNCALL	_main,_portpin_init
	FNCALL	_main,_timer1_init
	FNCALL	_main,_capture_mode_init
	FNROOT	_main
	FNCALL	_handler,_timer1_isr
	FNCALL	intlevel1,_handler
	global	intlevel1
	FNROOT	intlevel1
	global	_volatile_timer1_count
	global	main@timer1_counter
psect	nvCOMMON,class=COMMON,space=1
global __pnvCOMMON
__pnvCOMMON:
main@timer1_counter:
       ds      2

	global	_CCP2CON
_CCP2CON	set	29
	global	_CCPR2H
_CCPR2H	set	28
	global	_PORTD
_PORTD	set	8
	global	_TMR1H
_TMR1H	set	15
	global	_TMR1L
_TMR1L	set	14
	global	_GIE
_GIE	set	95
	global	_PEIE
_PEIE	set	94
	global	_T1CKPS0
_T1CKPS0	set	132
	global	_T1CKPS1
_T1CKPS1	set	133
	global	_T1GINV
_T1GINV	set	135
	global	_T1OSCEN
_T1OSCEN	set	131
	global	_T1SYNC
_T1SYNC	set	130
	global	_TMR1CS
_TMR1CS	set	129
	global	_TMR1GE
_TMR1GE	set	134
	global	_TMR1IF
_TMR1IF	set	96
	global	_TMR1ON
_TMR1ON	set	128
	global	_TRISC
_TRISC	set	135
	global	_TRISD
_TRISD	set	136
	global	_TMR1IE
_TMR1IE	set	1120
	file	"INPUT_CAPTURE_MODE.as"
	line	#
psect cinit,class=CODE,delta=2
global start_initialization
start_initialization:

psect	bssCOMMON,class=COMMON,space=1
global __pbssCOMMON
__pbssCOMMON:
_volatile_timer1_count:
       ds      2

; Clear objects allocated to COMMON
psect cinit,class=CODE,delta=2
	clrf	((__pbssCOMMON)+0)&07Fh
	clrf	((__pbssCOMMON)+1)&07Fh
psect cinit,class=CODE,delta=2
global end_of_initialization

;End of C runtime variable initialization code

end_of_initialization:
clrf status
ljmp _main	;jump to C main() function
psect	cstackCOMMON,class=COMMON,space=1
global __pcstackCOMMON
__pcstackCOMMON:
	global	?_portpin_init
?_portpin_init:	; 0 bytes @ 0x0
	global	?_timer1_init
?_timer1_init:	; 0 bytes @ 0x0
	global	?_capture_mode_init
?_capture_mode_init:	; 0 bytes @ 0x0
	global	?_timer1_isr
?_timer1_isr:	; 0 bytes @ 0x0
	global	??_timer1_isr
??_timer1_isr:	; 0 bytes @ 0x0
	global	?_main
?_main:	; 0 bytes @ 0x0
	global	?_handler
?_handler:	; 0 bytes @ 0x0
	global	??_handler
??_handler:	; 0 bytes @ 0x0
	ds	4
	global	??_portpin_init
??_portpin_init:	; 0 bytes @ 0x4
	global	??_timer1_init
??_timer1_init:	; 0 bytes @ 0x4
	global	??_capture_mode_init
??_capture_mode_init:	; 0 bytes @ 0x4
	ds	1
	global	??_main
??_main:	; 0 bytes @ 0x5
;;Data sizes: Strings 0, constant 0, data 0, bss 2, persistent 2 stack 0
;;Auto spaces:   Size  Autos    Used
;; COMMON          14      5       9
;; BANK0           80      0       0
;; BANK1           80      0       0
;; BANK3           96      0       0
;; BANK2           96      0       0

;;
;; Pointer list with targets:



;;
;; Critical Paths under _main in COMMON
;;
;;   _main->_portpin_init
;;
;; Critical Paths under _handler in COMMON
;;
;;   None.
;;
;; Critical Paths under _main in BANK0
;;
;;   None.
;;
;; Critical Paths under _handler in BANK0
;;
;;   None.
;;
;; Critical Paths under _main in BANK1
;;
;;   None.
;;
;; Critical Paths under _handler in BANK1
;;
;;   None.
;;
;; Critical Paths under _main in BANK3
;;
;;   None.
;;
;; Critical Paths under _handler in BANK3
;;
;;   None.
;;
;; Critical Paths under _main in BANK2
;;
;;   None.
;;
;; Critical Paths under _handler in BANK2
;;
;;   None.

;;
;;Main: autosize = 0, tempsize = 0, incstack = 0, save=0
;;

;;
;;Call Graph Tables:
;;
;; ---------------------------------------------------------------------------------
;; (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;; ---------------------------------------------------------------------------------
;; (0) _main                                                 0     0      0       0
;;                       _portpin_init
;;                        _timer1_init
;;                  _capture_mode_init
;; ---------------------------------------------------------------------------------
;; (1) _capture_mode_init                                    0     0      0       0
;; ---------------------------------------------------------------------------------
;; (1) _timer1_init                                          0     0      0       0
;; ---------------------------------------------------------------------------------
;; (1) _portpin_init                                         1     1      0       0
;;                                              4 COMMON     1     1      0
;; ---------------------------------------------------------------------------------
;; Estimated maximum stack depth 1
;; ---------------------------------------------------------------------------------
;; (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;; ---------------------------------------------------------------------------------
;; (2) _handler                                              4     4      0       0
;;                                              0 COMMON     4     4      0
;;                         _timer1_isr
;; ---------------------------------------------------------------------------------
;; (3) _timer1_isr                                           0     0      0       0
;; ---------------------------------------------------------------------------------
;; Estimated maximum stack depth 3
;; ---------------------------------------------------------------------------------

;; Call Graph Graphs:

;; _main (ROOT)
;;   _portpin_init
;;   _timer1_init
;;   _capture_mode_init
;;
;; _handler (ROOT)
;;   _timer1_isr
;;

;; Address spaces:

;;Name               Size   Autos  Total    Cost      Usage
;;BITCOMMON            E      0       0       0        0.0%
;;EEDATA             100      0       0       0        0.0%
;;NULL                 0      0       0       0        0.0%
;;CODE                 0      0       0       0        0.0%
;;COMMON               E      5       9       1       64.3%
;;BITSFR0              0      0       0       1        0.0%
;;SFR0                 0      0       0       1        0.0%
;;BITSFR1              0      0       0       2        0.0%
;;SFR1                 0      0       0       2        0.0%
;;STACK                0      0       5       2        0.0%
;;ABS                  0      0       9       3        0.0%
;;BITBANK0            50      0       0       4        0.0%
;;BITSFR3              0      0       0       4        0.0%
;;SFR3                 0      0       0       4        0.0%
;;BANK0               50      0       0       5        0.0%
;;BITSFR2              0      0       0       5        0.0%
;;SFR2                 0      0       0       5        0.0%
;;BITBANK1            50      0       0       6        0.0%
;;BANK1               50      0       0       7        0.0%
;;BITBANK3            60      0       0       8        0.0%
;;BANK3               60      0       0       9        0.0%
;;BITBANK2            60      0       0      10        0.0%
;;BANK2               60      0       0      11        0.0%
;;DATA                 0      0       E      12        0.0%

	global	_main
psect	maintext,global,class=CODE,delta=2
global __pmaintext
__pmaintext:

;; *************** function _main *****************
;; Defined at:
;;		line 30 in file "d:\users\f53368a\Desktop\Various Documents\pic_projects\INPUT_CAPTURE_MODE\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 17F/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels required when called:    3
;; This function calls:
;;		_portpin_init
;;		_timer1_init
;;		_capture_mode_init
;; This function is called by:
;;		Startup code after reset
;; This function uses a non-reentrant model
;;
psect	maintext
	file	"d:\users\f53368a\Desktop\Various Documents\pic_projects\INPUT_CAPTURE_MODE\main.c"
	line	30
	global	__size_of_main
	__size_of_main	equ	__end_of_main-_main
	
_main:	
	opt	stack 5
; Regs used in _main: [wreg+status,2+status,0+pclath+cstack]
	line	36
	
l2045:	
;main.c: 33: static uint16_t timer1_counter;
;main.c: 36: portpin_init();
	fcall	_portpin_init
	line	37
	
l2047:	
;main.c: 37: timer1_init();
	fcall	_timer1_init
	line	38
	
l2049:	
;main.c: 38: capture_mode_init();
	fcall	_capture_mode_init
	line	39
	
l2051:	
;main.c: 39: (GIE = 1);
	bsf	(95/8),(95)&7
	line	40
	
l2053:	
;main.c: 40: volatile_timer1_count = FALSE;
	movlw	low(0)
	movwf	(_volatile_timer1_count)	;volatile
	movlw	high(0)
	movwf	((_volatile_timer1_count))+1	;volatile
	goto	l2055
	line	42
;main.c: 42: while(1)
	
l847:	
	line	44
	
l2055:	
;main.c: 43: {
;main.c: 44: timer1_counter = volatile_timer1_count;
	movf	(_volatile_timer1_count+1),w	;volatile
	clrf	(main@timer1_counter+1)
	addwf	(main@timer1_counter+1)
	movf	(_volatile_timer1_count),w	;volatile
	clrf	(main@timer1_counter)
	addwf	(main@timer1_counter)

	line	45
	
l2057:	
;main.c: 45: PORTD = CCPR2H;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(28),w	;volatile
	movwf	(8)	;volatile
	goto	l2055
	line	50
	
l848:	
	line	42
	goto	l2055
	
l849:	
	line	52
	
l850:	
	global	start
	ljmp	start
	opt stack 0
GLOBAL	__end_of_main
	__end_of_main:
;; =============== function _main ends ============

	signat	_main,88
	global	_capture_mode_init
psect	text109,local,class=CODE,delta=2
global __ptext109
__ptext109:

;; *************** function _capture_mode_init *****************
;; Defined at:
;;		line 100 in file "d:\users\f53368a\Desktop\Various Documents\pic_projects\INPUT_CAPTURE_MODE\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text109
	file	"d:\users\f53368a\Desktop\Various Documents\pic_projects\INPUT_CAPTURE_MODE\main.c"
	line	100
	global	__size_of_capture_mode_init
	__size_of_capture_mode_init	equ	__end_of_capture_mode_init-_capture_mode_init
	
_capture_mode_init:	
	opt	stack 5
; Regs used in _capture_mode_init: [wreg]
	line	101
	
l1245:	
;main.c: 101: CCP2CON = 0x06;
	movlw	(06h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(29)	;volatile
	line	102
	
l859:	
	return
	opt stack 0
GLOBAL	__end_of_capture_mode_init
	__end_of_capture_mode_init:
;; =============== function _capture_mode_init ends ============

	signat	_capture_mode_init,88
	global	_timer1_init
psect	text110,local,class=CODE,delta=2
global __ptext110
__ptext110:

;; *************** function _timer1_init *****************
;; Defined at:
;;		line 65 in file "d:\users\f53368a\Desktop\Various Documents\pic_projects\INPUT_CAPTURE_MODE\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		status,2
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text110
	file	"d:\users\f53368a\Desktop\Various Documents\pic_projects\INPUT_CAPTURE_MODE\main.c"
	line	65
	global	__size_of_timer1_init
	__size_of_timer1_init	equ	__end_of_timer1_init-_timer1_init
	
_timer1_init:	
	opt	stack 5
; Regs used in _timer1_init: [status,2]
	line	67
	
l1241:	
;main.c: 67: T1GINV = FALSE;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	bcf	(135/8),(135)&7
	line	68
;main.c: 68: TMR1GE = FALSE;
	bcf	(134/8),(134)&7
	line	69
;main.c: 69: T1CKPS1 = FALSE;
	bcf	(133/8),(133)&7
	line	70
;main.c: 70: T1CKPS0 = TRUE;
	bsf	(132/8),(132)&7
	line	71
;main.c: 71: T1OSCEN = FALSE;
	bcf	(131/8),(131)&7
	line	72
;main.c: 72: T1SYNC = FALSE;
	bcf	(130/8),(130)&7
	line	73
;main.c: 73: TMR1CS = FALSE;
	bcf	(129/8),(129)&7
	line	74
;main.c: 74: TMR1ON = TRUE;
	bsf	(128/8),(128)&7
	line	76
;main.c: 76: TMR1IE = TRUE;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bsf	(1120/8)^080h,(1120)&7
	line	77
;main.c: 77: PEIE = TRUE;
	bsf	(94/8),(94)&7
	line	79
	
l1243:	
;main.c: 79: TMR1H = 0u;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(15)	;volatile
	line	80
;main.c: 80: TMR1L = 0u;
	clrf	(14)	;volatile
	line	81
	
l853:	
	return
	opt stack 0
GLOBAL	__end_of_timer1_init
	__end_of_timer1_init:
;; =============== function _timer1_init ends ============

	signat	_timer1_init,88
	global	_portpin_init
psect	text111,local,class=CODE,delta=2
global __ptext111
__ptext111:

;; *************** function _portpin_init *****************
;; Defined at:
;;		line 87 in file "d:\users\f53368a\Desktop\Various Documents\pic_projects\INPUT_CAPTURE_MODE\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          1       0       0       0       0
;;      Totals:         1       0       0       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text111
	file	"d:\users\f53368a\Desktop\Various Documents\pic_projects\INPUT_CAPTURE_MODE\main.c"
	line	87
	global	__size_of_portpin_init
	__size_of_portpin_init	equ	__end_of_portpin_init-_portpin_init
	
_portpin_init:	
	opt	stack 5
; Regs used in _portpin_init: [wreg+status,2+status,0]
	line	90
	
l1237:	
;main.c: 90: TRISC ^= 0xFD;
	movlw	(0FDh)
	movwf	(??_portpin_init+0)+0
	movf	(??_portpin_init+0)+0,w
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	xorwf	(135)^080h,f	;volatile
	line	91
	
l1239:	
;main.c: 91: TRISD = 0x00;
	clrf	(136)^080h	;volatile
	line	93
	
l856:	
	return
	opt stack 0
GLOBAL	__end_of_portpin_init
	__end_of_portpin_init:
;; =============== function _portpin_init ends ============

	signat	_portpin_init,88
	global	_handler
psect	text112,local,class=CODE,delta=2
global __ptext112
__ptext112:

;; *************** function _handler *****************
;; Defined at:
;;		line 109 in file "d:\users\f53368a\Desktop\Various Documents\pic_projects\INPUT_CAPTURE_MODE\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, fsr1l, fsr1h, status,2, status,0, btemp+1, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          4       0       0       0       0
;;      Totals:         4       0       0       0       0
;;Total ram usage:        4 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		_timer1_isr
;; This function is called by:
;;		Interrupt level 1
;; This function uses a non-reentrant model
;;
psect	text112
	file	"d:\users\f53368a\Desktop\Various Documents\pic_projects\INPUT_CAPTURE_MODE\main.c"
	line	109
	global	__size_of_handler
	__size_of_handler	equ	__end_of_handler-_handler
	
_handler:	
	opt	stack 5
; Regs used in _handler: [allreg]
psect	intentry,class=CODE,delta=2
global __pintentry
__pintentry:
global interrupt_function
interrupt_function:
	global saved_w
	saved_w	set	btemp+0
	movwf	saved_w
	movf	status,w
	movwf	(??_handler+0)
	movf	fsr0,w
	movwf	(??_handler+1)
	movf	pclath,w
	movwf	(??_handler+2)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	btemp+1,w
	movwf	(??_handler+3)
	ljmp	_handler
psect	text112
	line	110
	
i1l1247:	
;main.c: 110: timer1_isr();
	fcall	_timer1_isr
	line	111
	
i1l862:	
	movf	(??_handler+3),w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	btemp+1
	movf	(??_handler+2),w
	movwf	pclath
	movf	(??_handler+1),w
	movwf	fsr0
	movf	(??_handler+0),w
	movwf	status
	swapf	saved_w,f
	swapf	saved_w,w
	retfie
	opt stack 0
GLOBAL	__end_of_handler
	__end_of_handler:
;; =============== function _handler ends ============

	signat	_handler,88
	global	_timer1_isr
psect	text113,local,class=CODE,delta=2
global __ptext113
__ptext113:

;; *************** function _timer1_isr *****************
;; Defined at:
;;		line 118 in file "d:\users\f53368a\Desktop\Various Documents\pic_projects\INPUT_CAPTURE_MODE\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_handler
;; This function uses a non-reentrant model
;;
psect	text113
	file	"d:\users\f53368a\Desktop\Various Documents\pic_projects\INPUT_CAPTURE_MODE\main.c"
	line	118
	global	__size_of_timer1_isr
	__size_of_timer1_isr	equ	__end_of_timer1_isr-_timer1_isr
	
_timer1_isr:	
	opt	stack 5
; Regs used in _timer1_isr: [wreg]
	line	120
	
i1l1249:	
;main.c: 119: extern volatile uint16_t volatile_timer1_count;
;main.c: 120: if((TMR1IE)&&(TMR1IF))
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	btfss	(1120/8)^080h,(1120)&7
	goto	u1_21
	goto	u1_20
u1_21:
	goto	i1l868
u1_20:
	
i1l1251:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfss	(96/8),(96)&7
	goto	u2_21
	goto	u2_20
u2_21:
	goto	i1l868
u2_20:
	line	122
	
i1l1253:	
;main.c: 121: {
;main.c: 122: TMR1IF = 0;
	bcf	(96/8),(96)&7
	line	123
	
i1l1255:	
;main.c: 123: volatile_timer1_count++;
	movlw	low(01h)
	addwf	(_volatile_timer1_count),f	;volatile
	skipnc
	incf	(_volatile_timer1_count+1),f	;volatile
	movlw	high(01h)
	addwf	(_volatile_timer1_count+1),f	;volatile
	line	124
	
i1l1257:	
# 124 "d:\users\f53368a\Desktop\Various Documents\pic_projects\INPUT_CAPTURE_MODE\main.c"
nop ;#
psect	text113
	goto	i1l868
	line	125
	
i1l867:	
	line	126
	
i1l868:	
	return
	opt stack 0
GLOBAL	__end_of_timer1_isr
	__end_of_timer1_isr:
;; =============== function _timer1_isr ends ============

	signat	_timer1_isr,88
psect	text114,local,class=CODE,delta=2
global __ptext114
__ptext114:
	global	btemp
	btemp set 07Eh

	DABS	1,126,2	;btemp
	global	wtemp0
	wtemp0 set btemp
	end
