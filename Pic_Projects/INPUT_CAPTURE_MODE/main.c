#include "htc.h"
#include "typedefs.h"
/********************************************************************************************************
/*   INPUT CAPTURE MODE
/*           10/13/2012
/*  
/*
/********************************************************************************************************/
/*PIC16F887*/
__CONFIG(DEBUGEN & LVPEN & FCMEN & IESODIS & BORXSLP & DUNPROTECT & UNPROTECT & MCLREN & PWRTEN & WDTDIS & INTCLK);//HS);
__CONFIG(BORV40);
/* STRUCTURES */
/*NONE*/
/* Constants Arrays*/
/* NONE*/
/* Global var*/
volatile uint16_t volatile_timer1_count;
/* Function prototypes*/
void main (void);
void timer1_init (void);
void portpin_init (void);
void capture_mode_init(void);
void interrupt handler(void);
void timer1_isr(void);

/*********************************************************************/
/*        THE MAIN LOOP                                              */         
/*********************************************************************/
void main (void)
{

/* Local main vars*/ 
static uint16_t timer1_counter;

/*Initializations*/
portpin_init();
timer1_init();
capture_mode_init();
ei();                           /* GIE Unabled hopefully*/ 
volatile_timer1_count = FALSE;  /*initialize this counter*/   

while(1)
 { 
   timer1_counter = volatile_timer1_count;
   PORTD = CCPR2H;
   //TMR1L  = 0x00; 
   //TMR1H  = 0x00;
   //CCPR2L = 0x00;
   //CCPR2H = 0x00; 
 }

}

/*******************************************************************/
/*       INITIALIZATINS SECTION
/*******************************************************************/



/******************************************************************/
/* Timer1: T1CON Register initializations
/*
/******************************************************************/
void timer1_init(void)
{ 
  /* Definitions for T1CON register */
  T1GINV  = FALSE;
  TMR1GE  = FALSE;
  T1CKPS1 = FALSE;
  T1CKPS0 = TRUE;  
  T1OSCEN = FALSE;
  T1SYNC  = FALSE;
  TMR1CS  = FALSE;
  TMR1ON  = TRUE;
 
  TMR1IE  = TRUE;  /* Enable Timer 1 interrupt*/
  PEIE    = TRUE;  /*Enable the Peripheral interrupts*/
  
  TMR1H   = 0u;
  TMR1L   = 0u;
}
/***************************************************************/
/* Micro Port Pins Initializations 
/*
/***************************************************************/
void portpin_init(void)
{
  /*PIN RC2 as input for CCP1*/
  /*PIN RC1 as input for CCP2*/
  TRISC ^= 0xFD;               /*RC1 as input for CCP2*/
  TRISD  = 0x00;               /* Make portd all outputs*/ 
  
}

/****************************************************************/
/*Capture Peripheral
/*
/****************************************************************/
void capture_mode_init(void)
{
  CCP2CON = 0x06;             /*Capture mode every 4th rising edge*/
}

/****************************************************************/
/*Interrupt Handler
/*
/****************************************************************/
void interrupt handler(void)
{  
   timer1_isr();               /* One interupt Source*/ 
}

/****************************************************************/
/*Timer1 Interupt Service Routine
/*
/****************************************************************/
void timer1_isr(void)
{
 extern volatile uint16_t volatile_timer1_count;
  if((TMR1IE)&&(TMR1IF))
   {
     TMR1IF = 0;
     volatile_timer1_count++;
     asm("nop");
   }
}
