#include "typedefs.h"


/* Variables*/
 struct {
         unsigned lo  : 1;
         unsigned mid : 6;
         unsigned hi  : 1;
         }teststruct = {0,0,0};  
/* external*/
    enum
        {
          LEVEL_10=10,
          LEVEL_9 =9,
          LEVEL_8 =8,
          LEVEL_7 =7,
          LEVEL_6 =6,
          LEVEL_5 =5,
          LEVEL_4 =4,
          LEVEL_0 =0
        }MOTOR_ADC_LEVELS; 
  
/*Function Prototypes*/

uint16_t adc_convert(void);
uint16_t adc_volt_convert(void);
uint8_t get_setpoint(void);
uint8_t get_position(void);
void adc_isr(void);
void adc_init(void);