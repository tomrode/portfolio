#include <htc.h>
#include "typedefs.h"
#include "adc_func.h"
//#include "timer.h"

#define ADC_MAX  (0x3FF)
#define ADC_BITS (10)
/* Macro tested in main*/

#define ADC_FULL_SCALE_MV         (5000)
#define ADC_BITS                  (10)
#define ADC_MULTIPLY(x,y)         ((uint32_t)(x)*(uint32_t)(y))
#define adc_volt_convert_macro(x) ((uint16_t)(ADC_MULTIPLY(x,ADC_FULL_SCALE_MV) >> ADC_BITS))
#define MAX_COUNT                 (10)                                                           /* used in linear_Motor()    

//#define  adc_volt_convert_macro(x) (((x)*(.00488))*(1000)) // conversion macro raw adc count to volts in mV
/* variables*/
uint16_t int_count=0;
uint16_t adc_isr_flag;



const uint8_t motor_speed_array[] =  
{                           // FROM EXCELL all estimates
  0x0A,                     // index0  100% pwm
  0x09,                     // index1  93% pwm
  0x09,                     // index2  93% pwm
  0x08,                     // index3  86% pwm
  0x07,                     // index4  72% pwm 
  0x06,                     // index5  58% pwm 
  0x06,                     // index6  58% pwm
  0x05,                     // index7  51% pwm
  0x05,                     // index8  51% pwm
  0x04,                     // index9  44% pwm
  0x03,                     // index10 37% pwm
  0x03,                     // index11 37% pwm
  0x02,                     // index12 30% pwm 
  0x01,                     // index13 16% pwm
  0x01,                     // index14 16% pwm
  0x00                      // index15 0% pwm
};


const uint8_t motor_speed_array1[] =  
{                              // FROM EXCELL all estimates
  LEVEL_10,                    // index0  100% pwm
  LEVEL_9,                     // index1  93% pwm
  LEVEL_8,                     // index2  86% pwm
  LEVEL_7,                     // index3  72% pwm 
  LEVEL_6,                     // index4  58% pwm 
  LEVEL_5,                     // index5  51% pwm
  LEVEL_4,                     // index6  44% pwm  <- is the bad linear region 
  LEVEL_0,                     // index7  0% pwm
};

#ifdef ADC_VOLTS
uint16_t adc_volt_convert(void)
{
/* Variables*/
uint16_t adc_rawvalue;
uint16_t adc_millivolt_macroval;
uint16_t adc_millivolt=0;

adc_rawvalue = adc_convert();                                  // Get Adc value
PORTC = 0x40;
adc_millivolt_macroval = adc_volt_convert_macro(adc_rawvalue); // invoke the macro 
PORTC = 0x00;
adc_millivolt = adc_millivolt_macroval;

asm("nop");
return adc_millivolt;                                          // send out this to any call that wants it 

}
#endif

uint16_t adc_convert(void)
{
  uint16_t adresh;                   //return value from the atd conversion 
  uint16_t adresl;                   //return value from the atd conversion
  uint16_t result; 
  adc_isr_flag = 0;                  //*CRITICAL* INTERRUPT FLAG MUST BE SET BEFORE CONVERTING
 
  GODONE = 1;                        //*CRITICAL* INTERRUPT FLAG MUST BE SET BEFORE CONVERTING
  
  while (adc_isr_flag == 0)           // Did an interrupt happened to a conversion
  {}
  adresl = ADRESL;                    // 
  adresh = ADRESH;                    //
  result = ((adresh << 8) |(adresl)); // get into word format
 
  return result;
}

uint8_t get_setpoint(void)
{
uint16_t current_setpoint;
uint8_t  new_setpoint;

ADCON0 = 0xC5;                              /* Turn on the conversion channel AN1 while maintaing ADCON0 state*/
/* new*/
current_setpoint = (adc_convert() >> 7);    /* this will get 4 most significant bits from 10 bit adc read*/
return (motor_speed_array1[current_setpoint]);

}

uint8_t get_position(void)
{
uint16_t current_position;
uint8_t  new_position;

ADCON0 = 0xC9;                              /* Turn on the conversion channel AN2 while maintaing ADCON0 state*/

current_position = adc_convert();           /* get the raw value*/
current_position = (current_position >> 2); /* now right shift it over 2 of coures this is silly but  need it to work first*/
new_position = current_position;            /* make it 8 bit value*/
return new_position;
}




void adc_init(void)
{
  TRISA =  0x06;     /*Port direction 1=input 0=output |TRISA7=0|TRISA6=0|TRISA5=0|TRISA4=0||TRISA3=0|TRISA2=1||TRISA1=1|TRISA0=0|*/  
  ANSEL =  0x06;     /* Select channel |ANS7=0|ANS6=0|ANS5=0|ANS4=0|ANS3=0|ANS2=0|ANS1=1|ANS0=0| RA2=actual_position and RA1=Setpoint used*/ 
  ADCON0 = 0xC1;     /*|ADCS1 = 1|ADCS0 = 1|CHS3 =0|CHS2 =0|CHS1=0|CHS0=0|Go/DONE=0|ADON = 1|  A/D clock is FRC internal RC, analog channel = AN1 and AN2,ADON = to be used*/
  ADCON1 = 0x80;     /*|ADFM=0 Right justify|*/
}

void adc_isr(void)
{
  if ((ADIF)&&(ADIE))
  {
    
    ADIF=0;          // clear flag
    adc_isr_flag=1;  // set the indication 
    int_count++; 
  }
  else 
  {
  }
}