#include "htc.h"
#include "pwm.h"
#include "typedefs.h"


void pwm_init(void)
{
 PR2 =     0x09;//0x65;       /* Set up comparison value for resetting TMR2 for period. Both set 19.6KHz*/
 T2CON =   0x04;       /* TOUTPS3=0| TOUTPS2=0| TOUTPS1=0| TOUTPS0=0| TMR2ON=1| T2CKPS1=0| T2CKPS0=0 -> T2CKPS<1:0>: Timer2 Clock Prescale Select bits 00 = Prescaler is 1 */
 CCP1CON = 0x0C;       /* P1M1 = 0, P1M0 = 0, DC1B1 = 0, DC1B0 = 0, CCP1M3..CCP1M0 = 1100 1100 = PWM mode; P1A, P1C active-high; P1B, P1D active-high*/

}