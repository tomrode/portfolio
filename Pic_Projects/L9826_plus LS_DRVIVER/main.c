/************************************************************************************************/          
/*   12/22/2012      Low side driver L9826 ST micro. Plan is to test the NON1
/*                   or NON2 with a varible pwm controlled by a pot. 
/*                   Then move on to spi control. Also changing the file structure a little 
/*                   example #include "header\somefile.h"   
/*                   THis was the PID cal so need to remove alot. 
/*   12/28/2012      Setting up first example to run PWM to lowside driver     
/*                   OUTPUT PWM-> RC2 pin 36 on pic16f887 TQFP package is CCP1/P1A 
/*                   INPUT POT -> I/O Pin Analog
/*                                RA1 20  AN1   
/*                   I found 19.6 KHz kept the whining down but running at 200KHz. However as soon as its 80% pwm
/*                   FET gets really hot, it must be in linear region. 
/*                   Real experimentaion found the span
/*                   ADRESH is 0 and ADRESL:0x29 is 100%         CCPR1L = 0x0A
/*                                          0x15 is 50% pwm      CCPR1L = 0x05
/*                                          0x02 is 0%           CCPR1L = 0x00
/*                  changed motor_speed_array[] with CCPR1L value to linearize at this frequency        
/************************************************************************************************/
#include <htc.h>
#include "typedefs.h"
#include "adc_func.h"
#include "pwm.h"
#include "timer.h"
/**********************************************************/
/* Device Configuration
/**********************************************************/
       /*PIC16F887*/
__CONFIG(DEBUGEN & LVPEN & FCMEN & IESODIS & BORXSLP & DUNPROTECT & UNPROTECT & MCLREN & PWRTEN & WDTDIS & INTCLK);//HS);
__CONFIG(BORV40);  //HS means resonator 8Mhz      
      
 /* Global define for this module*/      
#define LOW_SPEED_TIME                 (100)

/**********************************************************/
/* Function Prototypes 
/**********************************************************/
void init(void);                  /* SFR inits*/
int main(void);    

/***********************************************************/
/* Variable Definition Global to this module
/***********************************************************/

int main(void)
{
/************************************************************/
/* LOCAL MAIN VARIABLES                                     */
/************************************************************/
uint8_t LwSpdSt;                                            /* low speed state macine*/
static uint8_t Mot_Spd;

enum
  {
    LWSPD_DEFAULT,
    LWSPD_LO,
    LWSPD_HI
  }LOW_SPEED_STATE;
/***********************************************************/
/* INITIALIZATIONS                                         */
/***********************************************************/ 


init();                                                     /* Initialize SFR*/                                            
adc_init();                                                 /* Initialize adc*/
pwm_init();                                                 /* Initialize PWM*/
TMR1IE = 1;                                                 /* Timer 1 interrupt enable*/
ADIE = 1;                                                   /* AtD interrupt enable*/
PEIE = 1;                                                   /* enable all peripheral interrupts*/
ei();                                                       /* enable all interrupts*/
LwSpdSt = LWSPD_DEFAULT;                                    /* Set the Low speed state to its initial setting*/                                                        
                       
/************************************************************/
/*  MAIN LOOP OVERALL                                       */
/************************************************************/
while(TRUE)                   
 {  
  
  if (TO == TRUE)                                                         /*STATUS reg, bit 4 1=After power up,CLRWDT or sleep instruction 0=A WDT time-out occured*/ 
   {
     Mot_Spd = get_setpoint();                                            /* PWM result result from adc pot table */
     PORTD = Mot_Spd;    
     CCPR1L = Mot_Spd;                                                    /* Output it to PWM module*/
        /* BELOW IS THE LOW MOTOR SPEED STATE MACHINE, TIMES WILL NEED TO BE TWEEKED*/  
     if(Mot_Spd == LEVEL_4)
      { 
        switch(LwSpdSt)
        {
           case LWSPD_DEFAULT:
              timer_set(LOW_SPEED_TIMER,LOW_SPEED_TIME);
              LwSpdSt = LWSPD_LO;
              CCPR1L = LEVEL_5;
              break;

           case LWSPD_LO:
            if(get_timer(LOW_SPEED_TIMER) == FALSE)
             {
               PORTD ^= 0x80;            /* test the timer */
               CCPR1L = LEVEL_4;
               LwSpdSt = LWSPD_HI;
               timer_set(LOW_SPEED_TIMER,LOW_SPEED_TIME);
             } 
             break;

            case LWSPD_HI:
             if(get_timer(LOW_SPEED_TIMER) == FALSE)
              {
               PORTD ^= 0x80;            /* test the timer */
               CCPR1L = LEVEL_5;
               LwSpdSt = LWSPD_LO;
               timer_set(LOW_SPEED_TIMER,LOW_SPEED_TIME);
              } 
              break; 
        } 
      }
   } 
  else
     {
       CLRWDT();                                                         // Clear watchdog timer
     } 

 }   
} 


void init (void)
{
 OSCCON     = 0x61;                    // 4 Mhz clock
 OPTION     = 0x8F;                    /*|PSA=Prescale to WDT|most prescale rate|PS2=1|PS1=1|PS0=1| 20ms/Div*/
 TRISC      = 0x00;                    /* used for RC1 (ccp2)*/
 TRISD      = 0x00;                    /* port directions: 1=input, 0=output*/
 PORTB      = 0x00;
 PORTD      = 0xFF;                    /* port D all on*/
 T1CON      = 0x35;                    /* T1CKPS1,T1CKPS0 = 1:8 prescaler,T1SYNC = do not synchronise external clock input ,TMR1ON = enabled*/ 

}


