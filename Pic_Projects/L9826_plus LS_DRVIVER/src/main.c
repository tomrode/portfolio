/************************************************************************************************/          
/*   12/22/2012      Low side driver L9826 ST micro. Plan is to test the NON1
/*                   or NON2 with a varible pwm controlled by a pot. 
/*                   Then move on to spi control. Also changing the file structure a little 
/*                   example #include "header\somefile.h"   
/*                   THis was the PID cal so need to remove alot.                            
/************************************************************************************************/
#include <htc.h>
//#include "d:\users\F53368A\Desktop\Various Documents\pic_projects\L9826_lowside_pwm\h\typedefs.h"
#include "h\typedefs.h"
//#include "header\adc_func.h"
//#include "pwm.h"
//#include "header\timer.h"
/**********************************************************/
/* Device Configuration
/**********************************************************/
     
   
      /*PIC16F887*/
__CONFIG(DEBUGEN & LVPEN & FCMEN & IESODIS & BORXSLP & DUNPROTECT & UNPROTECT & MCLREN & PWRTEN & WDTDIS & HS);
__CONFIG(BORV40);  //HS means resonator 8Mhz      
      
 /* Global define for this module*/      


/**********************************************************/
/* Function Prototypes 
/**********************************************************/
void init(void);                  /* SFR inits*/
//int main(void);    

/***********************************************************/
/* Variable Definition Global to this module
/***********************************************************/

int main(void)
{
/************************************************************/
/* LOCAL MAIN VARIABLES                                     */
/************************************************************/

/***********************************************************/
/* INITIALIZATIONS                                         */
/***********************************************************/ 


init();                                                     /* Initialize SFR*/                                            
//adc_init();                                                 /* Initialize adc*/
//pwm_init();                                                 /* Initialize PWM*/
TMR1IE = 1;                                                 /* Timer 1 interrupt enable*/
ADIE = 1;                                                   /* AtD interrupt enable*/
PEIE = 1;                                                   /* enable all peripheral interrupts*/
//ei();                                                       /* enable all interrupts*/

//timer_set(PID_TIMER,10);                                    /* initialize PID Timer*/                                                          
                       
/************************************************************/
/*  MAIN LOOP OVERALL                                       */
/************************************************************/
while(1)                   
 {  
  
  if (TO == 1)                                                          /*STATUS reg, bit 4 1=After power up,CLRWDT or sleep instruction 0=A WDT time-out occured*/ 
   {
   
    // PORTD = pid_cal_result;                                            /* PWM result to PORTD I need this */
    //CCPR1L = PORTD;                                                    /* Output it to PWM module*/
   } 
  else
     {
       CLRWDT();                                                         // Clear watchdog timer
     } 

 }   
} 


void init (void)
{
 OPTION     = 0x8F;                    /*|PSA=Prescale to WDT|most prescale rate|PS2=1|PS1=1|PS0=1| 20ms/Div*/
 TRISC      = 0x00;                    /* used for RC1 (ccp2)*/
 TRISD      = 0x00;                    /* port directions: 1=input, 0=output*/
 PORTB      = 0x00;
 PORTD      = 0xFF;                    /* port D all on*/ 
//PIE1       = 0x01;                   /* TMR1IE = enabled*/ 
//INTCON     = 0x40;                   /* PEIE = enabled*/
 T1CON      = 0x35;                    /* T1CKPS1,T1CKPS0 = 1:8 prescaler,T1SYNC = do not synchronise external clock input ,TMR1ON = enabled*/ 
 // _delay(800000); 
 while(1)
  {
  PORTD ^= 0xff;
  _delay(800000); 
  }
}


