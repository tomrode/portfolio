#include "htc.h"
#include "pwm.h"
#include "typedefs.h"


void pwm_init(void)
{
 PR2 =     0x60;       /* Set up comparison value for resetting TMR2 for period*/
 T2CON =   0x06;       /* turn on timer 2 with the prescaler  1:16, by default post scaler is 1:1 */
 CCP1CON = 0x0C;       /* P1M1 = 0, P1M0 = 0, DC1B1 = 0, DC1B0 = 0, CCP1M3..CCP1M0 = 1100 1100 = PWM mode; P1A, P1C active-high; P1B, P1D active-high*/

}