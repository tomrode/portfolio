opt subtitle "HI-TECH Software Omniscient Code Generator (Lite mode) build 6738"

opt pagewidth 120

	opt lm

	processor	16F887
clrc	macro
	bcf	3,0
	endm
clrz	macro
	bcf	3,2
	endm
setc	macro
	bsf	3,0
	endm
setz	macro
	bsf	3,2
	endm
skipc	macro
	btfss	3,0
	endm
skipz	macro
	btfss	3,2
	endm
skipnc	macro
	btfsc	3,0
	endm
skipnz	macro
	btfsc	3,2
	endm
indf	equ	0
indf0	equ	0
pc	equ	2
pcl	equ	2
status	equ	3
fsr	equ	4
fsr0	equ	4
c	equ	1
z	equ	0
pclath	equ	10
# 28 "d:\users\F53368A\Desktop\Various Documents\pic_projects\L9826_plus LS_DRVIVER\main.c"
	psect config,class=CONFIG,delta=2 ;#
# 28 "d:\users\F53368A\Desktop\Various Documents\pic_projects\L9826_plus LS_DRVIVER\main.c"
	dw 0x1FFF & 0x3FFF & 0x3FFF & 0x3BFF & 0x3EFF & 0x3FFF & 0x3FFF & 0x3FFF & 0x3FEF & 0x3FF7 & 0x3FFD ;#
# 29 "d:\users\F53368A\Desktop\Various Documents\pic_projects\L9826_plus LS_DRVIVER\main.c"
	psect config,class=CONFIG,delta=2 ;#
# 29 "d:\users\F53368A\Desktop\Various Documents\pic_projects\L9826_plus LS_DRVIVER\main.c"
	dw 0x3FFF ;#
	FNCALL	_main,_init
	FNCALL	_main,_adc_init
	FNCALL	_main,_pwm_init
	FNCALL	_main,_get_setpoint
	FNCALL	_main,_timer_set
	FNCALL	_main,_get_timer
	FNCALL	_get_setpoint,_adc_convert
	FNROOT	_main
	FNCALL	_interrupt_handler,_adc_isr
	FNCALL	_interrupt_handler,_timer_isr
	FNCALL	intlevel1,_interrupt_handler
	global	intlevel1
	FNROOT	intlevel1
	global	_motor_speed_array
psect	strings,class=STRING,delta=2
global __pstrings
__pstrings:
;	global	stringdir,stringtab,__stringbase
stringtab:
;	String table - string pointers are 1 byte each
stringcode:stringdir:
movlw high(stringdir)
movwf pclath
movf fsr,w
incf fsr
	addwf pc
__stringbase:
	retlw	0
psect	strings
	file	"d:\users\F53368A\Desktop\Various Documents\pic_projects\L9826_plus LS_DRVIVER\adc_func.c"
	line	23
_motor_speed_array:
	retlw	0Ah
	retlw	09h
	retlw	09h
	retlw	08h
	retlw	07h
	retlw	06h
	retlw	06h
	retlw	05h
	retlw	05h
	retlw	04h
	retlw	03h
	retlw	03h
	retlw	02h
	retlw	01h
	retlw	01h
	retlw	0
	global	_motor_speed_array1
psect	strings
	file	"d:\users\F53368A\Desktop\Various Documents\pic_projects\L9826_plus LS_DRVIVER\adc_func.c"
	line	44
_motor_speed_array1:
	retlw	0Ah
	retlw	09h
	retlw	08h
	retlw	07h
	retlw	06h
	retlw	05h
	retlw	04h
	retlw	0
	global	_motor_speed_array
	global	_motor_speed_array1
	global	_timer_array
	global	_int_count
	global	main@Mot_Spd
	global	_adc_isr_flag
	global	_MOTOR_ADC_LEVELS
	global	_teststruct
	global	_tmr1_isr_counter
	global	_toms_variable
	global	_ADCON0
_ADCON0	set	31
	global	_ADRESH
_ADRESH	set	30
	global	_CCP1CON
_CCP1CON	set	23
	global	_CCPR1L
_CCPR1L	set	21
	global	_PORTB
_PORTB	set	6
	global	_PORTD
_PORTD	set	8
	global	_T1CON
_T1CON	set	16
	global	_T2CON
_T2CON	set	18
	global	_TMR1H
_TMR1H	set	15
	global	_TMR1L
_TMR1L	set	14
	global	_ADIF
_ADIF	set	102
	global	_GIE
_GIE	set	95
	global	_GODONE
_GODONE	set	249
	global	_PEIE
_PEIE	set	94
	global	_TMR1IF
_TMR1IF	set	96
	global	_TO
_TO	set	28
	global	_ADCON1
_ADCON1	set	159
	global	_ADRESL
_ADRESL	set	158
	global	_OPTION
_OPTION	set	129
	global	_OSCCON
_OSCCON	set	143
	global	_PR2
_PR2	set	146
	global	_TRISA
_TRISA	set	133
	global	_TRISC
_TRISC	set	135
	global	_TRISD
_TRISD	set	136
	global	_ADIE
_ADIE	set	1126
	global	_TMR1IE
_TMR1IE	set	1120
	global	_ANSEL
_ANSEL	set	392
	file	"L9826_plus LS_DRVIVER.as"
	line	#
psect cinit,class=CODE,delta=2
global start_initialization
start_initialization:

psect	bssCOMMON,class=COMMON,space=1
global __pbssCOMMON
__pbssCOMMON:
_adc_isr_flag:
       ds      2

_MOTOR_ADC_LEVELS:
       ds      1

_teststruct:
       ds      1

_tmr1_isr_counter:
       ds      1

_toms_variable:
       ds      1

psect	bssBANK0,class=BANK0,space=1
global __pbssBANK0
__pbssBANK0:
_timer_array:
       ds      4

_int_count:
       ds      2

main@Mot_Spd:
       ds      1

; Clear objects allocated to COMMON
psect cinit,class=CODE,delta=2
	clrf	((__pbssCOMMON)+0)&07Fh
	clrf	((__pbssCOMMON)+1)&07Fh
	clrf	((__pbssCOMMON)+2)&07Fh
	clrf	((__pbssCOMMON)+3)&07Fh
	clrf	((__pbssCOMMON)+4)&07Fh
	clrf	((__pbssCOMMON)+5)&07Fh
; Clear objects allocated to BANK0
psect cinit,class=CODE,delta=2
	clrf	((__pbssBANK0)+0)&07Fh
	clrf	((__pbssBANK0)+1)&07Fh
	clrf	((__pbssBANK0)+2)&07Fh
	clrf	((__pbssBANK0)+3)&07Fh
	clrf	((__pbssBANK0)+4)&07Fh
	clrf	((__pbssBANK0)+5)&07Fh
	clrf	((__pbssBANK0)+6)&07Fh
psect cinit,class=CODE,delta=2
global end_of_initialization

;End of C runtime variable initialization code

end_of_initialization:
clrf status
ljmp _main	;jump to C main() function
psect	cstackCOMMON,class=COMMON,space=1
global __pcstackCOMMON
__pcstackCOMMON:
	global	?_init
?_init:	; 0 bytes @ 0x0
	global	?_adc_init
?_adc_init:	; 0 bytes @ 0x0
	global	?_pwm_init
?_pwm_init:	; 0 bytes @ 0x0
	global	?_adc_isr
?_adc_isr:	; 0 bytes @ 0x0
	global	??_adc_isr
??_adc_isr:	; 0 bytes @ 0x0
	global	?_timer_isr
?_timer_isr:	; 0 bytes @ 0x0
	global	??_timer_isr
??_timer_isr:	; 0 bytes @ 0x0
	global	?_interrupt_handler
?_interrupt_handler:	; 0 bytes @ 0x0
	global	?_get_setpoint
?_get_setpoint:	; 1 bytes @ 0x0
	global	?_main
?_main:	; 2 bytes @ 0x0
	ds	1
	global	timer_isr@i
timer_isr@i:	; 1 bytes @ 0x1
	ds	1
	global	??_interrupt_handler
??_interrupt_handler:	; 0 bytes @ 0x2
	ds	4
psect	cstackBANK0,class=BANK0,space=1
global __pcstackBANK0
__pcstackBANK0:
	global	??_init
??_init:	; 0 bytes @ 0x0
	global	??_adc_init
??_adc_init:	; 0 bytes @ 0x0
	global	??_pwm_init
??_pwm_init:	; 0 bytes @ 0x0
	global	?_timer_set
?_timer_set:	; 0 bytes @ 0x0
	global	?_get_timer
?_get_timer:	; 2 bytes @ 0x0
	global	?_adc_convert
?_adc_convert:	; 2 bytes @ 0x0
	global	timer_set@value
timer_set@value:	; 2 bytes @ 0x0
	ds	2
	global	??_timer_set
??_timer_set:	; 0 bytes @ 0x2
	global	??_get_timer
??_get_timer:	; 0 bytes @ 0x2
	global	??_adc_convert
??_adc_convert:	; 0 bytes @ 0x2
	ds	1
	global	timer_set@index
timer_set@index:	; 1 bytes @ 0x3
	global	get_timer@result
get_timer@result:	; 2 bytes @ 0x3
	ds	1
	global	adc_convert@adresh
adc_convert@adresh:	; 2 bytes @ 0x4
	ds	1
	global	get_timer@index
get_timer@index:	; 1 bytes @ 0x5
	ds	1
	global	adc_convert@adresl
adc_convert@adresl:	; 2 bytes @ 0x6
	ds	2
	global	adc_convert@result
adc_convert@result:	; 2 bytes @ 0x8
	ds	2
	global	??_get_setpoint
??_get_setpoint:	; 0 bytes @ 0xA
	ds	2
	global	get_setpoint@current_setpoint
get_setpoint@current_setpoint:	; 2 bytes @ 0xC
	ds	2
	global	??_main
??_main:	; 0 bytes @ 0xE
	ds	1
	global	main@LwSpdSt
main@LwSpdSt:	; 1 bytes @ 0xF
	ds	1
;;Data sizes: Strings 0, constant 24, data 0, bss 13, persistent 0 stack 0
;;Auto spaces:   Size  Autos    Used
;; COMMON          14      6      12
;; BANK0           80     16      23
;; BANK1           80      0       0
;; BANK3           96      0       0
;; BANK2           96      0       0

;;
;; Pointer list with targets:

;; ?_get_timer	unsigned short  size(1) Largest target is 0
;;
;; ?_adc_convert	unsigned short  size(1) Largest target is 0
;;


;;
;; Critical Paths under _main in COMMON
;;
;;   None.
;;
;; Critical Paths under _interrupt_handler in COMMON
;;
;;   _interrupt_handler->_timer_isr
;;
;; Critical Paths under _main in BANK0
;;
;;   _main->_get_setpoint
;;   _get_setpoint->_adc_convert
;;
;; Critical Paths under _interrupt_handler in BANK0
;;
;;   None.
;;
;; Critical Paths under _main in BANK1
;;
;;   None.
;;
;; Critical Paths under _interrupt_handler in BANK1
;;
;;   None.
;;
;; Critical Paths under _main in BANK3
;;
;;   None.
;;
;; Critical Paths under _interrupt_handler in BANK3
;;
;;   None.
;;
;; Critical Paths under _main in BANK2
;;
;;   None.
;;
;; Critical Paths under _interrupt_handler in BANK2
;;
;;   None.

;;
;;Main: autosize = 0, tempsize = 1, incstack = 0, save=0
;;

;;
;;Call Graph Tables:
;;
;; ---------------------------------------------------------------------------------
;; (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;; ---------------------------------------------------------------------------------
;; (0) _main                                                 3     3      0     371
;;                                             14 BANK0      2     2      0
;;                               _init
;;                           _adc_init
;;                           _pwm_init
;;                       _get_setpoint
;;                          _timer_set
;;                          _get_timer
;; ---------------------------------------------------------------------------------
;; (1) _get_setpoint                                         5     5      0     136
;;                                             10 BANK0      4     4      0
;;                        _adc_convert
;; ---------------------------------------------------------------------------------
;; (2) _adc_convert                                         10     8      2     102
;;                                              0 BANK0     10     8      2
;; ---------------------------------------------------------------------------------
;; (1) _get_timer                                            6     4      2      99
;;                                              0 BANK0      6     4      2
;; ---------------------------------------------------------------------------------
;; (1) _timer_set                                            6     4      2      93
;;                                              0 BANK0      4     2      2
;; ---------------------------------------------------------------------------------
;; (1) _pwm_init                                             0     0      0       0
;; ---------------------------------------------------------------------------------
;; (1) _adc_init                                             0     0      0       0
;; ---------------------------------------------------------------------------------
;; (1) _init                                                 0     0      0       0
;; ---------------------------------------------------------------------------------
;; Estimated maximum stack depth 2
;; ---------------------------------------------------------------------------------
;; (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;; ---------------------------------------------------------------------------------
;; (3) _interrupt_handler                                    4     4      0      90
;;                                              2 COMMON     4     4      0
;;                            _adc_isr
;;                          _timer_isr
;; ---------------------------------------------------------------------------------
;; (4) _timer_isr                                            2     2      0      90
;;                                              0 COMMON     2     2      0
;; ---------------------------------------------------------------------------------
;; (4) _adc_isr                                              0     0      0       0
;; ---------------------------------------------------------------------------------
;; Estimated maximum stack depth 4
;; ---------------------------------------------------------------------------------

;; Call Graph Graphs:

;; _main (ROOT)
;;   _init
;;   _adc_init
;;   _pwm_init
;;   _get_setpoint
;;     _adc_convert
;;   _timer_set
;;   _get_timer
;;
;; _interrupt_handler (ROOT)
;;   _adc_isr
;;   _timer_isr
;;

;; Address spaces:

;;Name               Size   Autos  Total    Cost      Usage
;;BITCOMMON            E      0       0       0        0.0%
;;EEDATA             100      0       0       0        0.0%
;;NULL                 0      0       0       0        0.0%
;;CODE                 0      0       0       0        0.0%
;;COMMON               E      6       C       1       85.7%
;;BITSFR0              0      0       0       1        0.0%
;;SFR0                 0      0       0       1        0.0%
;;BITSFR1              0      0       0       2        0.0%
;;SFR1                 0      0       0       2        0.0%
;;STACK                0      0       6       2        0.0%
;;ABS                  0      0      23       3        0.0%
;;BITBANK0            50      0       0       4        0.0%
;;BITSFR3              0      0       0       4        0.0%
;;SFR3                 0      0       0       4        0.0%
;;BANK0               50     10      17       5       28.8%
;;BITSFR2              0      0       0       5        0.0%
;;SFR2                 0      0       0       5        0.0%
;;BITBANK1            50      0       0       6        0.0%
;;BANK1               50      0       0       7        0.0%
;;BITBANK3            60      0       0       8        0.0%
;;BANK3               60      0       0       9        0.0%
;;BITBANK2            60      0       0      10        0.0%
;;BANK2               60      0       0      11        0.0%
;;DATA                 0      0      29      12        0.0%

	global	_main
psect	maintext,global,class=CODE,delta=2
global __pmaintext
__pmaintext:

;; *************** function _main *****************
;; Defined at:
;;		line 45 in file "d:\users\F53368A\Desktop\Various Documents\pic_projects\L9826_plus LS_DRVIVER\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  LwSpdSt         1   15[BANK0 ] unsigned char 
;;  LOW_SPEED_ST    1    0        enum E836
;; Return value:  Size  Location     Type
;;                  2  852[COMMON] int 
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, btemp+1, pclath, cstack
;; Tracked objects:
;;		On entry : 17F/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       1       0       0       0
;;      Temps:          0       1       0       0       0
;;      Totals:         0       2       0       0       0
;;Total ram usage:        2 bytes
;; Hardware stack levels required when called:    4
;; This function calls:
;;		_init
;;		_adc_init
;;		_pwm_init
;;		_get_setpoint
;;		_timer_set
;;		_get_timer
;; This function is called by:
;;		Startup code after reset
;; This function uses a non-reentrant model
;;
psect	maintext
	file	"d:\users\F53368A\Desktop\Various Documents\pic_projects\L9826_plus LS_DRVIVER\main.c"
	line	45
	global	__size_of_main
	__size_of_main	equ	__end_of_main-_main
	
_main:	
	opt	stack 4
; Regs used in _main: [wreg-fsr0h+status,2+status,0+btemp+1+pclath+cstack]
	line	63
	
l5844:	
;main.c: 49: uint8_t LwSpdSt;
;main.c: 50: static uint8_t Mot_Spd;
;main.c: 52: enum
;main.c: 53: {
;main.c: 54: LWSPD_DEFAULT,
;main.c: 55: LWSPD_LO,
;main.c: 56: LWSPD_HI
;main.c: 57: }LOW_SPEED_STATE;
;main.c: 63: init();
	fcall	_init
	line	64
;main.c: 64: adc_init();
	fcall	_adc_init
	line	65
;main.c: 65: pwm_init();
	fcall	_pwm_init
	line	66
	
l5846:	
;main.c: 66: TMR1IE = 1;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bsf	(1120/8)^080h,(1120)&7
	line	67
	
l5848:	
;main.c: 67: ADIE = 1;
	bsf	(1126/8)^080h,(1126)&7
	line	68
	
l5850:	
;main.c: 68: PEIE = 1;
	bsf	(94/8),(94)&7
	line	69
	
l5852:	
;main.c: 69: (GIE = 1);
	bsf	(95/8),(95)&7
	line	70
	
l5854:	
;main.c: 70: LwSpdSt = LWSPD_DEFAULT;
	clrc
	movlw	0
	btfsc	status,0
	movlw	1
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(main@LwSpdSt)
	line	75
;main.c: 75: while(TRUE)
	
l856:	
	line	78
;main.c: 76: {
;main.c: 78: if (TO == TRUE)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfss	(28/8),(28)&7
	goto	u2581
	goto	u2580
u2581:
	goto	l857
u2580:
	line	80
	
l5856:	
;main.c: 79: {
;main.c: 80: Mot_Spd = get_setpoint();
	fcall	_get_setpoint
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(main@Mot_Spd)
	line	81
	
l5858:	
;main.c: 81: PORTD = Mot_Spd;
	movf	(main@Mot_Spd),w
	movwf	(8)	;volatile
	line	82
	
l5860:	
;main.c: 82: CCPR1L = Mot_Spd;
	movf	(main@Mot_Spd),w
	movwf	(21)	;volatile
	line	84
	
l5862:	
;main.c: 84: if(Mot_Spd == LEVEL_4)
	movf	(main@Mot_Spd),w
	xorlw	04h
	skipz
	goto	u2591
	goto	u2590
u2591:
	goto	l856
u2590:
	goto	l5892
	line	86
	
l5864:	
;main.c: 85: {
;main.c: 86: switch(LwSpdSt)
	goto	l5892
	line	88
;main.c: 87: {
;main.c: 88: case LWSPD_DEFAULT:
	
l860:	
	line	89
	
l5866:	
;main.c: 89: timer_set(LOW_SPEED_TIMER,(100));
	movlw	low(064h)
	movwf	(?_timer_set)
	movlw	high(064h)
	movwf	((?_timer_set))+1
	movlw	(0)
	fcall	_timer_set
	line	90
	
l5868:	
;main.c: 90: LwSpdSt = LWSPD_LO;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(main@LwSpdSt)
	bsf	status,0
	rlf	(main@LwSpdSt),f
	line	91
	
l5870:	
;main.c: 91: CCPR1L = LEVEL_5;
	movlw	(05h)
	movwf	(21)	;volatile
	line	92
;main.c: 92: break;
	goto	l856
	line	94
;main.c: 94: case LWSPD_LO:
	
l862:	
	line	95
	
l5872:	
;main.c: 95: if(get_timer(LOW_SPEED_TIMER) == FALSE)
	movlw	(0)
	fcall	_get_timer
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	((1+(?_get_timer))),w
	iorwf	((0+(?_get_timer))),w
	skipz
	goto	u2601
	goto	u2600
u2601:
	goto	l856
u2600:
	line	97
	
l5874:	
;main.c: 96: {
;main.c: 97: PORTD ^= 0x80;
	movlw	(080h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	xorwf	(8),f	;volatile
	line	98
	
l5876:	
;main.c: 98: CCPR1L = LEVEL_4;
	movlw	(04h)
	movwf	(21)	;volatile
	line	99
	
l5878:	
;main.c: 99: LwSpdSt = LWSPD_HI;
	movlw	(02h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(main@LwSpdSt)
	line	100
	
l5880:	
;main.c: 100: timer_set(LOW_SPEED_TIMER,(100));
	movlw	low(064h)
	movwf	(?_timer_set)
	movlw	high(064h)
	movwf	((?_timer_set))+1
	movlw	(0)
	fcall	_timer_set
	goto	l856
	line	101
	
l863:	
	line	102
;main.c: 101: }
;main.c: 102: break;
	goto	l856
	line	104
;main.c: 104: case LWSPD_HI:
	
l864:	
	line	105
	
l5882:	
;main.c: 105: if(get_timer(LOW_SPEED_TIMER) == FALSE)
	movlw	(0)
	fcall	_get_timer
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	((1+(?_get_timer))),w
	iorwf	((0+(?_get_timer))),w
	skipz
	goto	u2611
	goto	u2610
u2611:
	goto	l856
u2610:
	line	107
	
l5884:	
;main.c: 106: {
;main.c: 107: PORTD ^= 0x80;
	movlw	(080h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	xorwf	(8),f	;volatile
	line	108
	
l5886:	
;main.c: 108: CCPR1L = LEVEL_5;
	movlw	(05h)
	movwf	(21)	;volatile
	line	109
;main.c: 109: LwSpdSt = LWSPD_LO;
	clrf	(main@LwSpdSt)
	bsf	status,0
	rlf	(main@LwSpdSt),f
	line	110
	
l5888:	
;main.c: 110: timer_set(LOW_SPEED_TIMER,(100));
	movlw	low(064h)
	movwf	(?_timer_set)
	movlw	high(064h)
	movwf	((?_timer_set))+1
	movlw	(0)
	fcall	_timer_set
	goto	l856
	line	111
	
l865:	
	line	112
;main.c: 111: }
;main.c: 112: break;
	goto	l856
	line	113
	
l5890:	
;main.c: 113: }
	goto	l856
	line	86
	
l859:	
	
l5892:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(main@LwSpdSt),w
	; Switch size 1, requested type "space"
; Number of cases is 3, Range of values is 0 to 2
; switch strategies available:
; Name         Bytes Cycles
; simple_byte    10     6 (average)
; direct_byte    28    19 (fixed)
;	Chosen strategy is simple_byte

	xorlw	0^0	; case 0
	skipnz
	goto	l5866
	xorlw	1^0	; case 1
	skipnz
	goto	l5872
	xorlw	2^1	; case 2
	skipnz
	goto	l5882
	goto	l856

	line	113
	
l861:	
	goto	l856
	line	114
	
l858:	
	line	115
;main.c: 114: }
;main.c: 115: }
	goto	l856
	line	116
	
l857:	
	line	118
# 118 "d:\users\F53368A\Desktop\Various Documents\pic_projects\L9826_plus LS_DRVIVER\main.c"
clrwdt ;#
psect	maintext
	goto	l856
	line	119
	
l866:	
	goto	l856
	line	121
	
l855:	
	line	75
	goto	l856
	
l867:	
	line	122
	
l868:	
	global	start
	ljmp	start
	opt stack 0
GLOBAL	__end_of_main
	__end_of_main:
;; =============== function _main ends ============

	signat	_main,90
	global	_get_setpoint
psect	text424,local,class=CODE,delta=2
global __ptext424
__ptext424:

;; *************** function _get_setpoint *****************
;; Defined at:
;;		line 95 in file "d:\users\F53368A\Desktop\Various Documents\pic_projects\L9826_plus LS_DRVIVER\adc_func.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  current_setp    2   12[BANK0 ] unsigned short 
;;  new_setpoint    1    0        unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, btemp+1, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       2       0       0       0
;;      Temps:          0       2       0       0       0
;;      Totals:         0       4       0       0       0
;;Total ram usage:        4 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		_adc_convert
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text424
	file	"d:\users\F53368A\Desktop\Various Documents\pic_projects\L9826_plus LS_DRVIVER\adc_func.c"
	line	95
	global	__size_of_get_setpoint
	__size_of_get_setpoint	equ	__end_of_get_setpoint-_get_setpoint
	
_get_setpoint:	
	opt	stack 4
; Regs used in _get_setpoint: [wreg-fsr0h+status,2+status,0+btemp+1+pclath+cstack]
	line	99
	
l5836:	
;adc_func.c: 96: uint16_t current_setpoint;
;adc_func.c: 97: uint8_t new_setpoint;
;adc_func.c: 99: ADCON0 = 0xC5;
	movlw	(0C5h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(31)	;volatile
	line	101
	
l5838:	
;adc_func.c: 101: current_setpoint = (adc_convert() >> 7);
	fcall	_adc_convert
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(0+?_adc_convert),w
	movwf	(??_get_setpoint+0)+0
	movf	(1+?_adc_convert),w
	movwf	((??_get_setpoint+0)+0+1)
	movlw	07h
u2575:
	clrc
	rrf	(??_get_setpoint+0)+1,f
	rrf	(??_get_setpoint+0)+0,f
	addlw	-1
	skipz
	goto	u2575
	movf	0+(??_get_setpoint+0)+0,w
	movwf	(get_setpoint@current_setpoint)
	movf	1+(??_get_setpoint+0)+0,w
	movwf	(get_setpoint@current_setpoint+1)
	line	102
	
l5840:	
;adc_func.c: 102: return (motor_speed_array1[current_setpoint]);
	movf	(get_setpoint@current_setpoint),w
	addlw	low((_motor_speed_array1-__stringbase))
	movwf	fsr0
	fcall	stringdir
	goto	l3408
	
l5842:	
	line	104
	
l3408:	
	return
	opt stack 0
GLOBAL	__end_of_get_setpoint
	__end_of_get_setpoint:
;; =============== function _get_setpoint ends ============

	signat	_get_setpoint,89
	global	_adc_convert
psect	text425,local,class=CODE,delta=2
global __ptext425
__ptext425:

;; *************** function _adc_convert *****************
;; Defined at:
;;		line 77 in file "d:\users\F53368A\Desktop\Various Documents\pic_projects\L9826_plus LS_DRVIVER\adc_func.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  result          2    8[BANK0 ] unsigned short 
;;  adresl          2    6[BANK0 ] unsigned short 
;;  adresh          2    4[BANK0 ] unsigned short 
;; Return value:  Size  Location     Type
;;                  2    0[BANK0 ] unsigned short 
;; Registers used:
;;		wreg, status,2, status,0, btemp+1
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       2       0       0       0
;;      Locals:         0       6       0       0       0
;;      Temps:          0       2       0       0       0
;;      Totals:         0      10       0       0       0
;;Total ram usage:       10 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_get_setpoint
;;		_get_position
;; This function uses a non-reentrant model
;;
psect	text425
	file	"d:\users\F53368A\Desktop\Various Documents\pic_projects\L9826_plus LS_DRVIVER\adc_func.c"
	line	77
	global	__size_of_adc_convert
	__size_of_adc_convert	equ	__end_of_adc_convert-_adc_convert
	
_adc_convert:	
	opt	stack 4
; Regs used in _adc_convert: [wreg+status,2+status,0+btemp+1]
	line	81
	
l5812:	
;adc_func.c: 78: uint16_t adresh;
;adc_func.c: 79: uint16_t adresl;
;adc_func.c: 80: uint16_t result;
;adc_func.c: 81: adc_isr_flag = 0;
	movlw	low(0)
	movwf	(_adc_isr_flag)
	movlw	high(0)
	movwf	((_adc_isr_flag))+1
	line	83
	
l5814:	
;adc_func.c: 83: GODONE = 1;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	bsf	(249/8),(249)&7
	line	85
;adc_func.c: 85: while (adc_isr_flag == 0)
	goto	l5816
	
l3403:	
	goto	l5816
	line	86
;adc_func.c: 86: {}
	
l3402:	
	line	85
	
l5816:	
	movf	(_adc_isr_flag+1),w
	iorwf	(_adc_isr_flag),w
	skipnz
	goto	u2541
	goto	u2540
u2541:
	goto	l5816
u2540:
	goto	l5818
	
l3404:	
	line	87
	
l5818:	
;adc_func.c: 87: adresl = ADRESL;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movf	(158)^080h,w	;volatile
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_adc_convert+0)+0
	clrf	(??_adc_convert+0)+0+1
	movf	0+(??_adc_convert+0)+0,w
	movwf	(adc_convert@adresl)
	movf	1+(??_adc_convert+0)+0,w
	movwf	(adc_convert@adresl+1)
	line	88
;adc_func.c: 88: adresh = ADRESH;
	movf	(30),w	;volatile
	movwf	(??_adc_convert+0)+0
	clrf	(??_adc_convert+0)+0+1
	movf	0+(??_adc_convert+0)+0,w
	movwf	(adc_convert@adresh)
	movf	1+(??_adc_convert+0)+0,w
	movwf	(adc_convert@adresh+1)
	line	89
	
l5820:	
;adc_func.c: 89: result = ((adresh << 8) |(adresl));
	movf	(adc_convert@adresh+1),w
	movwf	(??_adc_convert+0)+0+1
	movf	(adc_convert@adresh),w
	movwf	(??_adc_convert+0)+0
	movlw	08h
	movwf	btemp+1
u2555:
	clrc
	rlf	(??_adc_convert+0)+0,f
	rlf	(??_adc_convert+0)+1,f
	decfsz	btemp+1,f
	goto	u2555
	movf	(adc_convert@adresl),w
	iorwf	0+(??_adc_convert+0)+0,w
	movwf	(adc_convert@result)
	movf	(adc_convert@adresl+1),w
	iorwf	1+(??_adc_convert+0)+0,w
	movwf	1+(adc_convert@result)
	line	91
	
l5822:	
;adc_func.c: 91: return result;
	movf	(adc_convert@result+1),w
	clrf	(?_adc_convert+1)
	addwf	(?_adc_convert+1)
	movf	(adc_convert@result),w
	clrf	(?_adc_convert)
	addwf	(?_adc_convert)

	goto	l3405
	
l5824:	
	line	92
	
l3405:	
	return
	opt stack 0
GLOBAL	__end_of_adc_convert
	__end_of_adc_convert:
;; =============== function _adc_convert ends ============

	signat	_adc_convert,90
	global	_get_timer
psect	text426,local,class=CODE,delta=2
global __ptext426
__ptext426:

;; *************** function _get_timer *****************
;; Defined at:
;;		line 33 in file "d:\users\F53368A\Desktop\Various Documents\pic_projects\L9826_plus LS_DRVIVER\timer.c"
;; Parameters:    Size  Location     Type
;;  index           1    wreg     unsigned char 
;; Auto vars:     Size  Location     Type
;;  index           1    5[BANK0 ] unsigned char 
;;  result          2    3[BANK0 ] unsigned short 
;; Return value:  Size  Location     Type
;;                  2    0[BANK0 ] unsigned short 
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       2       0       0       0
;;      Locals:         0       3       0       0       0
;;      Temps:          0       1       0       0       0
;;      Totals:         0       6       0       0       0
;;Total ram usage:        6 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text426
	file	"d:\users\F53368A\Desktop\Various Documents\pic_projects\L9826_plus LS_DRVIVER\timer.c"
	line	33
	global	__size_of_get_timer
	__size_of_get_timer	equ	__end_of_get_timer-_get_timer
	
_get_timer:	
	opt	stack 5
; Regs used in _get_timer: [wreg-fsr0h+status,2+status,0]
;get_timer@index stored from wreg
	line	36
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(get_timer@index)
	
l5798:	
;timer.c: 34: uint16_t result;
;timer.c: 36: if (index < TIMER_MAX)
	movlw	(02h)
	subwf	(get_timer@index),w
	skipnc
	goto	u2531
	goto	u2530
u2531:
	goto	l5806
u2530:
	line	38
	
l5800:	
;timer.c: 37: {
;timer.c: 38: (GIE = 0);
	bcf	(95/8),(95)&7
	line	39
	
l5802:	
;timer.c: 39: result = timer_array[index];
	movf	(get_timer@index),w
	movwf	(??_get_timer+0)+0
	addwf	(??_get_timer+0)+0,w
	addlw	_timer_array&0ffh
	movwf	fsr0
	bcf	status, 7	;select IRP bank0
	movf	indf,w
	movwf	(get_timer@result)
	incf	fsr0,f
	movf	indf,w
	movwf	(get_timer@result+1)
	line	40
	
l5804:	
;timer.c: 40: (GIE = 1);
	bsf	(95/8),(95)&7
	line	41
;timer.c: 41: }
	goto	l5808
	line	42
	
l4265:	
	line	44
	
l5806:	
;timer.c: 42: else
;timer.c: 43: {
;timer.c: 44: result = 0;
	movlw	low(0)
	movwf	(get_timer@result)
	movlw	high(0)
	movwf	((get_timer@result))+1
	goto	l5808
	line	45
	
l4266:	
	line	47
	
l5808:	
;timer.c: 45: }
;timer.c: 47: return result;
	movf	(get_timer@result+1),w
	clrf	(?_get_timer+1)
	addwf	(?_get_timer+1)
	movf	(get_timer@result),w
	clrf	(?_get_timer)
	addwf	(?_get_timer)

	goto	l4267
	
l5810:	
	line	49
	
l4267:	
	return
	opt stack 0
GLOBAL	__end_of_get_timer
	__end_of_get_timer:
;; =============== function _get_timer ends ============

	signat	_get_timer,4218
	global	_timer_set
psect	text427,local,class=CODE,delta=2
global __ptext427
__ptext427:

;; *************** function _timer_set *****************
;; Defined at:
;;		line 18 in file "d:\users\F53368A\Desktop\Various Documents\pic_projects\L9826_plus LS_DRVIVER\timer.c"
;; Parameters:    Size  Location     Type
;;  index           1    wreg     unsigned char 
;;  value           2    0[BANK0 ] unsigned short 
;; Auto vars:     Size  Location     Type
;;  index           1    3[BANK0 ] unsigned char 
;;  array_conten    2    0        unsigned short 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       2       0       0       0
;;      Locals:         0       1       0       0       0
;;      Temps:          0       1       0       0       0
;;      Totals:         0       4       0       0       0
;;Total ram usage:        4 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text427
	file	"d:\users\F53368A\Desktop\Various Documents\pic_projects\L9826_plus LS_DRVIVER\timer.c"
	line	18
	global	__size_of_timer_set
	__size_of_timer_set	equ	__end_of_timer_set-_timer_set
	
_timer_set:	
	opt	stack 5
; Regs used in _timer_set: [wreg-fsr0h+status,2+status,0]
;timer_set@index stored from wreg
	line	20
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(timer_set@index)
	
l5790:	
;timer.c: 19: uint16_t array_contents;
;timer.c: 20: if (index < TIMER_MAX)
	movlw	(02h)
	subwf	(timer_set@index),w
	skipnc
	goto	u2521
	goto	u2520
u2521:
	goto	l4262
u2520:
	line	22
	
l5792:	
;timer.c: 21: {
;timer.c: 22: (GIE = 0);
	bcf	(95/8),(95)&7
	line	23
	
l5794:	
;timer.c: 23: timer_array[index] = value;
	movf	(timer_set@index),w
	movwf	(??_timer_set+0)+0
	addwf	(??_timer_set+0)+0,w
	addlw	_timer_array&0ffh
	movwf	fsr0
	movf	(timer_set@value),w
	bcf	status, 7	;select IRP bank0
	movwf	indf
	incf	fsr0,f
	movf	(timer_set@value+1),w
	movwf	indf
	line	24
	
l5796:	
;timer.c: 24: (GIE = 1);
	bsf	(95/8),(95)&7
	line	25
;timer.c: 25: }
	goto	l4262
	line	26
	
l4260:	
	goto	l4262
	line	28
;timer.c: 26: else
;timer.c: 27: {
	
l4261:	
	line	29
	
l4262:	
	return
	opt stack 0
GLOBAL	__end_of_timer_set
	__end_of_timer_set:
;; =============== function _timer_set ends ============

	signat	_timer_set,8312
	global	_pwm_init
psect	text428,local,class=CODE,delta=2
global __ptext428
__ptext428:

;; *************** function _pwm_init *****************
;; Defined at:
;;		line 7 in file "d:\users\F53368A\Desktop\Various Documents\pic_projects\L9826_plus LS_DRVIVER\pwm.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text428
	file	"d:\users\F53368A\Desktop\Various Documents\pic_projects\L9826_plus LS_DRVIVER\pwm.c"
	line	7
	global	__size_of_pwm_init
	__size_of_pwm_init	equ	__end_of_pwm_init-_pwm_init
	
_pwm_init:	
	opt	stack 5
; Regs used in _pwm_init: [wreg]
	line	8
	
l5788:	
;pwm.c: 8: PR2 = 0x09;
	movlw	(09h)
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movwf	(146)^080h	;volatile
	line	9
;pwm.c: 9: T2CON = 0x04;
	movlw	(04h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(18)	;volatile
	line	10
;pwm.c: 10: CCP1CON = 0x0C;
	movlw	(0Ch)
	movwf	(23)	;volatile
	line	12
	
l1708:	
	return
	opt stack 0
GLOBAL	__end_of_pwm_init
	__end_of_pwm_init:
;; =============== function _pwm_init ends ============

	signat	_pwm_init,88
	global	_adc_init
psect	text429,local,class=CODE,delta=2
global __ptext429
__ptext429:

;; *************** function _adc_init *****************
;; Defined at:
;;		line 123 in file "d:\users\F53368A\Desktop\Various Documents\pic_projects\L9826_plus LS_DRVIVER\adc_func.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text429
	file	"d:\users\F53368A\Desktop\Various Documents\pic_projects\L9826_plus LS_DRVIVER\adc_func.c"
	line	123
	global	__size_of_adc_init
	__size_of_adc_init	equ	__end_of_adc_init-_adc_init
	
_adc_init:	
	opt	stack 5
; Regs used in _adc_init: [wreg]
	line	124
	
l5786:	
;adc_func.c: 124: TRISA = 0x06;
	movlw	(06h)
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movwf	(133)^080h	;volatile
	line	125
;adc_func.c: 125: ANSEL = 0x06;
	movlw	(06h)
	bsf	status, 5	;RP0=1, select bank3
	bsf	status, 6	;RP1=1, select bank3
	movwf	(392)^0180h	;volatile
	line	126
;adc_func.c: 126: ADCON0 = 0xC1;
	movlw	(0C1h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(31)	;volatile
	line	127
;adc_func.c: 127: ADCON1 = 0x80;
	movlw	(080h)
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movwf	(159)^080h	;volatile
	line	128
	
l3414:	
	return
	opt stack 0
GLOBAL	__end_of_adc_init
	__end_of_adc_init:
;; =============== function _adc_init ends ============

	signat	_adc_init,88
	global	_init
psect	text430,local,class=CODE,delta=2
global __ptext430
__ptext430:

;; *************** function _init *****************
;; Defined at:
;;		line 126 in file "d:\users\F53368A\Desktop\Various Documents\pic_projects\L9826_plus LS_DRVIVER\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text430
	file	"d:\users\F53368A\Desktop\Various Documents\pic_projects\L9826_plus LS_DRVIVER\main.c"
	line	126
	global	__size_of_init
	__size_of_init	equ	__end_of_init-_init
	
_init:	
	opt	stack 5
; Regs used in _init: [wreg+status,2]
	line	127
	
l5774:	
;main.c: 127: OSCCON = 0x61;
	movlw	(061h)
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movwf	(143)^080h	;volatile
	line	128
;main.c: 128: OPTION = 0x8F;
	movlw	(08Fh)
	movwf	(129)^080h	;volatile
	line	129
	
l5776:	
;main.c: 129: TRISC = 0x00;
	clrf	(135)^080h	;volatile
	line	130
	
l5778:	
;main.c: 130: TRISD = 0x00;
	clrf	(136)^080h	;volatile
	line	131
	
l5780:	
;main.c: 131: PORTB = 0x00;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(6)	;volatile
	line	132
	
l5782:	
;main.c: 132: PORTD = 0xFF;
	movlw	(0FFh)
	movwf	(8)	;volatile
	line	133
	
l5784:	
;main.c: 133: T1CON = 0x35;
	movlw	(035h)
	movwf	(16)	;volatile
	line	135
	
l871:	
	return
	opt stack 0
GLOBAL	__end_of_init
	__end_of_init:
;; =============== function _init ends ============

	signat	_init,88
	global	_interrupt_handler
psect	text431,local,class=CODE,delta=2
global __ptext431
__ptext431:

;; *************** function _interrupt_handler *****************
;; Defined at:
;;		line 6 in file "d:\users\F53368A\Desktop\Various Documents\pic_projects\L9826_plus LS_DRVIVER\interrupt.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          4       0       0       0       0
;;      Totals:         4       0       0       0       0
;;Total ram usage:        4 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		_adc_isr
;;		_timer_isr
;; This function is called by:
;;		Interrupt level 1
;; This function uses a non-reentrant model
;;
psect	text431
	file	"d:\users\F53368A\Desktop\Various Documents\pic_projects\L9826_plus LS_DRVIVER\interrupt.c"
	line	6
	global	__size_of_interrupt_handler
	__size_of_interrupt_handler	equ	__end_of_interrupt_handler-_interrupt_handler
	
_interrupt_handler:	
	opt	stack 4
; Regs used in _interrupt_handler: [wreg-fsr0h+status,2+status,0+pclath+cstack]
psect	intentry,class=CODE,delta=2
global __pintentry
__pintentry:
global interrupt_function
interrupt_function:
	global saved_w
	saved_w	set	btemp+0
	movwf	saved_w
	movf	status,w
	movwf	(??_interrupt_handler+0)
	movf	fsr0,w
	movwf	(??_interrupt_handler+1)
	movf	pclath,w
	movwf	(??_interrupt_handler+2)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	btemp+1,w
	movwf	(??_interrupt_handler+3)
	ljmp	_interrupt_handler
psect	text431
	line	8
	
i1l5642:	
;interrupt.c: 8: adc_isr();
	fcall	_adc_isr
	line	9
	
i1l5644:	
;interrupt.c: 9: timer_isr();
	fcall	_timer_isr
	line	10
	
i1l2553:	
	movf	(??_interrupt_handler+3),w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	btemp+1
	movf	(??_interrupt_handler+2),w
	movwf	pclath
	movf	(??_interrupt_handler+1),w
	movwf	fsr0
	movf	(??_interrupt_handler+0),w
	movwf	status
	swapf	saved_w,f
	swapf	saved_w,w
	retfie
	opt stack 0
GLOBAL	__end_of_interrupt_handler
	__end_of_interrupt_handler:
;; =============== function _interrupt_handler ends ============

	signat	_interrupt_handler,88
	global	_timer_isr
psect	text432,local,class=CODE,delta=2
global __ptext432
__ptext432:

;; *************** function _timer_isr *****************
;; Defined at:
;;		line 54 in file "d:\users\F53368A\Desktop\Various Documents\pic_projects\L9826_plus LS_DRVIVER\timer.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  i               1    1[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         1       0       0       0       0
;;      Temps:          1       0       0       0       0
;;      Totals:         2       0       0       0       0
;;Total ram usage:        2 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_interrupt_handler
;; This function uses a non-reentrant model
;;
psect	text432
	file	"d:\users\F53368A\Desktop\Various Documents\pic_projects\L9826_plus LS_DRVIVER\timer.c"
	line	54
	global	__size_of_timer_isr
	__size_of_timer_isr	equ	__end_of_timer_isr-_timer_isr
	
_timer_isr:	
	opt	stack 4
; Regs used in _timer_isr: [wreg-fsr0h+status,2+status,0]
	line	56
	
i1l5654:	
;timer.c: 55: uint8_t i;
;timer.c: 56: if((TMR1IE)&&(TMR1IF))
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	btfss	(1120/8)^080h,(1120)&7
	goto	u237_21
	goto	u237_20
u237_21:
	goto	i1l4276
u237_20:
	
i1l5656:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfss	(96/8),(96)&7
	goto	u238_21
	goto	u238_20
u238_21:
	goto	i1l4276
u238_20:
	line	59
	
i1l5658:	
;timer.c: 57: {
;timer.c: 59: TMR1IF=0;
	bcf	(96/8),(96)&7
	line	60
	
i1l5660:	
;timer.c: 60: T1CON &= ~(0x01);
	movlw	(0FEh)
	movwf	(??_timer_isr+0)+0
	movf	(??_timer_isr+0)+0,w
	andwf	(16),f	;volatile
	line	61
	
i1l5662:	
;timer.c: 61: TMR1L = 0x08;
	movlw	(08h)
	movwf	(14)	;volatile
	line	62
	
i1l5664:	
;timer.c: 62: TMR1H = 0xFF;
	movlw	(0FFh)
	movwf	(15)	;volatile
	line	63
	
i1l5666:	
;timer.c: 63: T1CON |= 0x01;
	bsf	(16)+(0/8),(0)&7	;volatile
	line	65
;timer.c: 65: for (i = 0; i < TIMER_MAX; i++)
	clrc
	movlw	0
	btfsc	status,0
	movlw	1
	movwf	(timer_isr@i)
	
i1l5668:	
	movlw	(02h)
	subwf	(timer_isr@i),w
	skipc
	goto	u239_21
	goto	u239_20
u239_21:
	goto	i1l5672
u239_20:
	goto	i1l4276
	
i1l5670:	
	goto	i1l4276
	line	66
	
i1l4271:	
	line	67
	
i1l5672:	
;timer.c: 66: {
;timer.c: 67: if (timer_array[i] != 0)
	movf	(timer_isr@i),w
	movwf	(??_timer_isr+0)+0
	addwf	(??_timer_isr+0)+0,w
	addlw	_timer_array&0ffh
	movwf	fsr0
	bcf	status, 7	;select IRP bank0
	movf	indf,w
	incf	fsr0,f
	iorwf	indf,w
	skipnz
	goto	u240_21
	goto	u240_20
u240_21:
	goto	i1l5676
u240_20:
	line	69
	
i1l5674:	
;timer.c: 68: {
;timer.c: 69: timer_array[i]--;
	movf	(timer_isr@i),w
	movwf	(??_timer_isr+0)+0
	addwf	(??_timer_isr+0)+0,w
	addlw	_timer_array&0ffh
	movwf	fsr0
	movlw	low(01h)
	subwf	indf,f
	incfsz	fsr0,f
	movlw	high(01h)
	skipc
	decf	indf,f
	subwf	indf,f
	decf	fsr0,f
	line	70
;timer.c: 70: }
	goto	i1l5676
	line	71
	
i1l4273:	
	goto	i1l5676
	line	73
;timer.c: 71: else
;timer.c: 72: {
	
i1l4274:	
	line	65
	
i1l5676:	
	movlw	(01h)
	movwf	(??_timer_isr+0)+0
	movf	(??_timer_isr+0)+0,w
	addwf	(timer_isr@i),f
	
i1l5678:	
	movlw	(02h)
	subwf	(timer_isr@i),w
	skipc
	goto	u241_21
	goto	u241_20
u241_21:
	goto	i1l5672
u241_20:
	goto	i1l4276
	
i1l4272:	
	line	76
;timer.c: 73: }
;timer.c: 74: }
;timer.c: 76: }
	goto	i1l4276
	line	77
	
i1l4270:	
	goto	i1l4276
	line	79
;timer.c: 77: else
;timer.c: 78: {
	
i1l4275:	
	line	80
	
i1l4276:	
	return
	opt stack 0
GLOBAL	__end_of_timer_isr
	__end_of_timer_isr:
;; =============== function _timer_isr ends ============

	signat	_timer_isr,88
	global	_adc_isr
psect	text433,local,class=CODE,delta=2
global __ptext433
__ptext433:

;; *************** function _adc_isr *****************
;; Defined at:
;;		line 131 in file "d:\users\F53368A\Desktop\Various Documents\pic_projects\L9826_plus LS_DRVIVER\adc_func.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_interrupt_handler
;; This function uses a non-reentrant model
;;
psect	text433
	file	"d:\users\F53368A\Desktop\Various Documents\pic_projects\L9826_plus LS_DRVIVER\adc_func.c"
	line	131
	global	__size_of_adc_isr
	__size_of_adc_isr	equ	__end_of_adc_isr-_adc_isr
	
_adc_isr:	
	opt	stack 4
; Regs used in _adc_isr: [wreg]
	line	132
	
i1l5646:	
;adc_func.c: 132: if ((ADIF)&&(ADIE))
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfss	(102/8),(102)&7
	goto	u235_21
	goto	u235_20
u235_21:
	goto	i1l3419
u235_20:
	
i1l5648:	
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	btfss	(1126/8)^080h,(1126)&7
	goto	u236_21
	goto	u236_20
u236_21:
	goto	i1l3419
u236_20:
	line	135
	
i1l5650:	
;adc_func.c: 133: {
;adc_func.c: 135: ADIF=0;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	bcf	(102/8),(102)&7
	line	136
	
i1l5652:	
;adc_func.c: 136: adc_isr_flag=1;
	movlw	low(01h)
	movwf	(_adc_isr_flag)
	movlw	high(01h)
	movwf	((_adc_isr_flag))+1
	line	137
;adc_func.c: 137: int_count++;
	movlw	low(01h)
	addwf	(_int_count),f
	skipnc
	incf	(_int_count+1),f
	movlw	high(01h)
	addwf	(_int_count+1),f
	line	138
;adc_func.c: 138: }
	goto	i1l3419
	line	139
	
i1l3417:	
	goto	i1l3419
	line	141
;adc_func.c: 139: else
;adc_func.c: 140: {
	
i1l3418:	
	line	142
	
i1l3419:	
	return
	opt stack 0
GLOBAL	__end_of_adc_isr
	__end_of_adc_isr:
;; =============== function _adc_isr ends ============

	signat	_adc_isr,88
psect	text434,local,class=CODE,delta=2
global __ptext434
__ptext434:
	global	btemp
	btemp set 07Eh

	DABS	1,126,2	;btemp
	global	wtemp0
	wtemp0 set btemp
	end
