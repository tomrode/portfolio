/**********************************************************************************
PROJECT:	   	Continental MSM Hall Simulation
Description:	Hall Effect Sensor Simulation code
Date:			10/3/08
PROGRAMMER:		Brian Saloka (248)764-6782

CPU:			PIC18F8722
CLOCK:			16MHz crystal


/*
 * Includes the generic processor header file. The correct processor is
 * selected via the -p command-line option.

	SET XTAL CONFIG to HS!!!

 */
#include <p18F8722.h>
//#include <p18F8527.h>
#include <timers.h>
#include "main.h"

//*****************************************************************************
// I/O Mapping
//****************************************************************************
/*
PORT					FUNC	Name			Comment
----                    ----    ----            -------
PORT A...
RA0/AN0					A/D		FreqIn			Voltage value to set PWM frequency
RD1	
RD2	
RD3	
RD4	
RD5	

PORTB...
RB0	
RB1	
RB2	
RB3	
RB4	
RB5	
RB6	
RB7	

PORTC...
RC0						IN		MOT_1P			Motor1 In +
RC1						IN		MOT_1M			Motor1 In -
RC2						IN		MOT_2P			Motor2 In +
RC3						IN		MOT_2M			Motor2 In -
RC4						IN		MOT_3P			Motor3 In +
RC5						IN		MOT_3M			Motor3 In -
RC6						IN		MOT_4P			Motor4 In +
RC7						IN		MOT_4M			Motor4 In -

PORTD...
RD0	
RD1	
RD2	
RD3	
RD4	
RD5	
RD6	
RD7	

PORTE...
RE0						OUT		HOUT_8			Hall output 8
RE1						OUT		HOUT_7			Hall output 7		
RE2						OUT		HOUT_6			Hall output 6		
RE3						OUT		HOUT_5			Hall output 5
RE4						OUT		HOUT_4			Hall output 4 
RE5						OUT		HOUT_3			Hall output 3		
RE6						OUT		HOUT_2			Hall output 2		
RE7						OUT		HOUT_1			Hall output 1		

PORTF...
RF0						OUT		SEGA			LED Segment A
RF1						OUT		SEGB			LED Segment B
RF2						OUT		SEGC			LED Segment C
RF3						OUT		SEGD			LED Segment D
RF4						OUT		SEGE			LED Segment E
RF5						OUT		SEGF			LED Segment F
RF6						OUT		SEGG			LED Segment G
RF7

PORTG...
RG0						OUT		LED1000			Common for 1000's Digit LED Display
RG1						OUT		LED100			Common for 100's Digit LED Display
RG2						OUT		LED10			Common for 10's Digit LED Display
RG3						OUT		LED1			Common for 1's Digit LED Display

PORTH...
RH0						IN		MOT_5P			Motor5 In +
RH1						IN		MOT_5M			Motor5 In -
RH2						IN		MOT_6P			Motor6 In +
RH3						IN		MOT_6M			Motor6 In -
RH4						IN		MOT_7P			Motor7 In +
RH5						IN		MOT_7M			Motor7 In -
RH6						IN		MOT_8P			Motor8 In +
RH7						IN		MOT_8M			Motor8 In -

PORTJ...
RJ0						IN		STALL1			Motor Stall switch 1
RJ1						IN		STALL2			Motor Stall switch 2
RJ2						IN		STALL3			Motor Stall switch 3
RJ3						IN		STALL4			Motor Stall switch 4
RJ4						IN		STALL5			Motor Stall switch 5
RJ5						IN		STALL6			Motor Stall switch 6
RJ6						IN		STALL7			Motor Stall switch 7
RJ7						IN		STALL8			Motor Stall switch 8

*/


//.............................................................................
// Function prototypes
//.............................................................................
void timer_isr (void);				// Timer interrupt routine
void main_ClearRAM( void );
void main_InitPorts(void);			// Initialize the ports
void main_InitRegisters(void);		// Initialize the SFRs
void main_InitA2D( void );			// Initialize the A/Ds
void mainReadA2D( void );			// Read the A/Ds
void mainDisplay( void );			// Send the frequency value to the displays
void mainCheckMotorInputs( void );
void mainCheckStallSwitches( void );
void mainOutputHalls( void );



//.............................................................................
// RAM
//.............................................................................
#pragma udata gpr0
//#pragma udata
// A2D Variables...
unsigned char iOverflowCount;
unsigned char iFlipOutLF;

unsigned char cA2DCounter;
unsigned int iVAverager;
unsigned char cA2DVBattAvgCount;
unsigned char iVP;
unsigned int iVPot;
unsigned int iAvg;
unsigned int iRevVP;

unsigned char iDigit2Display;
unsigned int iDig0;
unsigned int iDig1;
unsigned int iDig2;
unsigned int iDig3;

unsigned char iPortBouncingTimer;
unsigned char iSTALLSWITCHES; 
unsigned char iOldSTALLPort;
unsigned char MOTOR1;
unsigned char MOTOR2;
unsigned char MOTOR3;
unsigned char MOTOR4;
unsigned char MOTOR5;
unsigned char MOTOR6;
unsigned char MOTOR7;
unsigned char MOTOR8;

unsigned char iHALLEnable;


// Timer related...
unsigned int iTimer1Value;						// Immediate storage of the timer counter when the ISR is entered.
unsigned int iFrequencyCount;
unsigned char iFlipOutput;

unsigned int iNewVal;
unsigned char iTmp;

//.............................................................................
// Constants...
//.............................................................................
#define	DIGIT_1S	PORTGbits.RG3
#define	DIGIT_10S	PORTGbits.RG2
#define	DIGIT_100S	PORTGbits.RG1
#define	DIGIT_1000S	PORTGbits.RG0

//#define STALLPORT PORTJ
#define STALLPORT PORTJ
#define STALLDEBOUNCEPERIOD 5		//20ms

#define ON 1
#define OFF 0
#define FALSE 0
#define TRUE 1

#define A2D_CH0		0x00
#define A2D_CH1		0x04

//.............................................................................
// Constants
//.............................................................................
#define C_TENMS 40000			// (16MHz clock) Timer counts to get to 10ms in the timer
#define C_HUNDUS 400			// (16MHz clock) Timer counts to get to 100uS in the timer
#define C_5HUNDUS 2000			// (16MHz clock) Timer counts to get to 500uS in the timer
#define C_ONEMS 4000			// (16MHz clock) Timer counts to get to 1ms in the timer


// Motor input port definitions...
#define MOT_1P	PORTCbits.RC0
#define MOT_1M	PORTCbits.RC1
#define MOT_2P	PORTCbits.RC2
#define MOT_2M	PORTCbits.RC3
#define MOT_3P	PORTCbits.RC4
#define MOT_3M	PORTCbits.RC5
#define MOT_4P	PORTCbits.RC6
#define MOT_4M	PORTCbits.RC7

#define MOT_5P	PORTHbits.RH0
#define MOT_5M	PORTHbits.RH1
#define MOT_6P	PORTHbits.RH2
#define MOT_6M	PORTHbits.RH3
#define MOT_7P	PORTHbits.RH4
#define MOT_7M	PORTHbits.RH5
#define MOT_8P	PORTHbits.RH6
#define MOT_8M	PORTHbits.RH7

/* For PIC18xxxx devices, the low interrupt vector is found at 000000018h.
 * Change the default code section to the absolute code section named
 * low_vector located at address 0x18. */
#pragma code low_vector=0x18
void low_interrupt (void)
{
  /*Inline assembly that will jump to the ISR. */
	//BS - Although this says "Timer" the SCI interrupt is checked here as well
  _asm GOTO timer_isr _endasm
}

/*Returns the compiler to the default code section. */
#pragma code
//=================================================================================================
// ROM table for mapping numbers 0-9 to 7-Segment LED segments.  Index 10 is a blank digit.
//=================================================================================================
rom char ccLEDMAP[11] = { 0x3F,0x06,0x5B,0x4F,0x66,0x6D,0x7D,0x07,0x7F,0x67,0x00 };

//=================================================================================================
// ROM table for mapping 10Hz steps (0-100) to Compare timer counter values to generate the frequencies
//=================================================================================================
rom int C_FREQTBL[101] =
{
	2000,2020,2041,2062,2083,2105,2128,2151,2174,2198,2222,2247,2273,2299,2326,2353,2381,2410,2439,2469,2500,2532,2564,
	2597,2632,2667,2703,2740,2778,2817,2857,2899,2941,2985,3030,3077,3125,3175,3226,3279,3333,3390,3448,3509,3571,3636,
	3704,3774,3846,3922,4000,4082,4167,4255,4348,4444,4545,4651,4762,4878,5000,5128,5263,5405,5556,5714,5882,6061,6250,
	6452,6667,6897,7143,7407,7692,8000,8333,8696,9091,9524,10000,10526,11111,11765,12500,13333,14286,15385,16667,18182,
	20000,22222,25000,28571,33333,40000,50000,60000,60000,60000,60000
};


/*
 * Specifies the function timer_isr as a low-priority interrupt service
 * routine. This is required in order for the compiler to generate a
 * RETFIE instruction instead of a RETURN instruction for the timer_isr
 * function.
 */
#pragma interruptlow timer_isr

/*
 * Define the timer_isr function. Notice that it does not take any
 * parameters, and does not return anything (as required by ISRs). 
 */

/*****************************************************************************/
void timer_isr (void)
/*=============================================================================
Inputs  -  
Outputs -

Description:
Timer interrupt routine
*******************************************************************************/
{
	// Read the timer value right away to avoid jitter
	iTimer1Value = ReadTimer1();
	iNewVal = iTimer1Value + iFrequencyCount - 203; // <- 203 is a fudge factor because it takes about 203 counts to get here from the interrupt

	//=========================================================================
	// Compare 2 interrupt interrupt
	// Programmed for xxuS @16MHz oscillator
	//=========================================================================
	if( PIR2bits.CCP2IF )
	{
		// Toggle the outputs ("toggle" bits).
		if( iFlipOutput != 0 )
			iFlipOutput = 0;
		else
			iFlipOutput = 0xFF;
	
		// If we are supposed to put out less than 30Hz, then the
		// counter for this OC overflows.  So, we fake the frequency in the 
		// OC below.  Otherwise, AND the enable bits with the "toggle" bits
		// For either PWM ON cycle, PWM OFF cycle, or just OFF.

		iTmp = iFlipOutput & iHALLEnable;
		if( iVP < 98 ) LATE = iTmp; //(iTmp ^ 0xFF);			
	
		// Reset the timer to the next xxuS
		CCPR2 = iNewVal;
		// Clear the flag to re-enable the interrupt
		PIR2bits.CCP2IF = 0;

	}

	//=========================================================================
	// Compare 1 interrupt interrupt
	// Programmed for 4ms @16MHz oscillator
	//=========================================================================
	if( PIR1bits.CCP1IF )
	{
		// Stall switch debounce timer...
		iPortBouncingTimer++;

		// Flip to the next LED digit to turn on.  Make sure we only count 4 digits...
		iDigit2Display++;
		if( iDigit2Display >= 4 ) iDigit2Display = 0;
		
		// If the freq should be 20Hz...
		if( iVP == 98 )
		{
			iOverflowCount++;						// 
			if( iOverflowCount == 6 )				// All of this puts out a 
			{										// 20Hz, 50% PWM
				iFlipOutLF ^= 0xFF;					//
				PORTE = iHALLEnable & iFlipOutLF;	//
				iOverflowCount = 0;					//
			}
		}

		// If the freq should be 10Hz...
		if( iVP == 99 )								//
		{											//
			iOverflowCount++;						// All of this puts out a
			if( iOverflowCount == 12 )				// 10Hz, 50% PWM
			{										//
				iFlipOutLF ^= 0xFF;					//
				PORTE = iHALLEnable & iFlipOutLF;	//
				iOverflowCount = 0;					//
			}
		}

		//______________________________________________________________________
		// Reset the timer to the next ms
		CCPR1 = iTimer1Value + (C_ONEMS*4); // 1ms 
		// Clear the flag to re-enable the interrupt
		PIR1bits.CCP1IF = 0;
	}


	
}
#pragma code

void main (void)
{
	// Initialize the SFRs...
	main_ClearRAM();
	main_InitRegisters();
	main_InitPorts();
	main_InitA2D();

	// General initializaation...

	//Enable global interrupts.
	INTCONbits.GIE = 1;
	INTCONbits.PEIE = 1;

//=============================================================================
// MM     MM    AAA    IIIIII   NN    NN
// MMM   MMM  AA   AA    II     NNN   NN
// MM M M MM  AAAAAAA    II     NN NN NN
// MM  M  MM  AA   AA    II     NN   NNN
// MM     MM  AA   AA  IIIIII   NN    NN
//=============================================================================

	while (1)
	{
		mainReadA2D();				// Read the A/D and determine the frequency from the value
		mainDisplay();				// Calculate what to display on the LEDs
		mainCheckMotorInputs();		// Check the motor inputs and set the correct "ON" bits
		mainCheckStallSwitches();	// Debounce the stall switches and see if any are pressed
		mainOutputHalls();			// Gather all of the information and determine whether or not to PWM an output channel
	}
}



/*****************************************************************************/
void main_ClearRAM( void )
/*=============================================================================
Inputs  -  
Outputs -

Description:
Initialize all of the CPU RAM to 0x00
*******************************************************************************/
{
	iFrequencyCount 	= 0;
	iOverflowCount  	= 0;
	iVPot				= 0;
	iRevVP          	= 0;
	iPortBouncingTimer  = 0;
	iFlipOutLF			= 0;
	
	cA2DCounter			= 0;
	iVAverager			= 0;
	cA2DVBattAvgCount	= 0;
	iVP					= 0;
	iVPot				= 0;
	iAvg				= 0;
	iRevVP				= 0;
	
	iDigit2Display		= 0;
	iSTALLSWITCHES		= 0; 
	iOldSTALLPort		= 0;
	MOTOR1				= 0;
	MOTOR2				= 0;
	MOTOR3				= 0;
	MOTOR4				= 0;
	MOTOR5				= 0;
	MOTOR6				= 0;
	MOTOR7				= 0;
	MOTOR8				= 0;
	iHALLEnable			= 0;
	iFlipOutput			= 0;
}


/*****************************************************************************/
void mainReadA2D( void )
/*=============================================================================
Inputs  -  
Outputs -

Description:
Read the A/D to get the value of the pot so we can set the frequency
*******************************************************************************/
{
	// Increments until desired counts to average is reached
	cA2DCounter++;

	
	ADCON0 &= 0xC3;						//
	ADCON0 |= A2D_CH1;					//
//	ADCON0 |= A2D_CH0;					//
	ADCON0bits.GO_DONE = 1;				// Read the A/D channel
	while( ADCON0bits.GO_DONE );		//
	iVAverager += (ADRES & 0x3F8);		//

	// Once we reach the number of readings we want to average, average them...
	if( cA2DCounter >= 20 )
	{
		// Divide to get the average
		iVPot = iVAverager / 20 ;
		// If we overshoot, make sure we never go above 1000Hz...
		if( iVPot > 1000 ) iVPot = 1000;
		
		// Set the counts for the PWM...
		// We're going in 10Hz increments so we divide by 10, then multiply by 10...
		// Example We read 147
		// Add 5 to round up = 152
		// 152 / 10 = 15
`		// 15 * 10 = 150
		// Value, iVPot = 150

		iVPot = ((iVPot+5) / 10) * 10;
		
		// iVP is the index pointer into the lookup table...
		iVP = iVPot / 10;

		// Get the # of timer counts to add to the OC timer from the lookup table...				
		iFrequencyCount = C_FREQTBL[iVP];

		// We have to swap the value to display it on the LEDs.  A iVPot value of 400 = 600hz!
		iRevVP          = 1000 - iVPot;		
	
		// Reset the counter and averager values for the next set of readings...
		iVAverager      = 0;
		cA2DCounter     = 0;
	}
}





/*****************************************************************************/
void mainDisplay( void )
/*=============================================================================
Inputs  -  
Outputs -

Description:
Send the frequency value to the displays...
*******************************************************************************/
{
	unsigned iRVP;

	// If the value is 1000, then set the 1000s digit to "1"
	if( iRevVP >= 1000 )
	{
		iDig0 = 1;									// 1000s
		iRVP = iRevVP - 1000;						// Make sure we strip the value off the total
	}		
	else
	{
		iDig0 = 0;									// 1000s
		iRVP = iRevVP;								// If < 1000, then just save the actual value
	}

	// Slice and dice the value in RAM to individual digits...
	iDig1 = iRVP / 100;								// 100s
	iDig2  = (iRVP - (iDig1 * 100)) / 10;			// 10s
	iDig3   = iRVP - (iDig1 * 100) - (iDig2 * 10);	// 1s

	// If the value is less than the values below, then set the correct unused digits to 10 (blank digit in the lookup)
	if( iRevVP < 1000) iDig0 = 10;
	if( iRevVP < 100)  iDig1 = 10;
	if( iRevVP < 10)   iDig2 = 10;


	// Turn off ALL digits for a sec...
	PORTG = 0;

	// Turn the correct digit on with the correct value...
	if( iDigit2Display == 0 )
	{
		PORTF = ccLEDMAP[iDig0];
		DIGIT_1000S = 1;
	}
	else if( iDigit2Display == 1 )
	{
		PORTF = ccLEDMAP[iDig1];
		DIGIT_100S = 1;
	}
	else if( iDigit2Display == 2 )
	{
		PORTF = ccLEDMAP[iDig2];
		DIGIT_10S = 1;
	}
	else 
	{
		PORTF = ccLEDMAP[iDig3];
		DIGIT_1S = 1;
	}
}





/*****************************************************************************/
void mainCheckMotorInputs( void )
/*=============================================================================
Inputs  -  
Outputs -

Description:
Check the motor + and - inputs to see if we need to turn on a PWM
The inputs are XORed to see if the motor is active in either the + or - direction
If the result of the XOR = 1 then the inputs are different and the motor is active,
therefore we turn on the PWM
else we turn it off


*******************************************************************************/
{
	// Check Motor 1...
	if( MOT_1P ^ MOT_1M )
		MOTOR1 = ON;
	else
		MOTOR1 = OFF;

	// Check Motor 2...
	if( MOT_2P ^ MOT_2M )
		MOTOR2 = ON;
	else
		MOTOR2 = OFF;
		
	// Check Motor 3...
	if( MOT_3P ^ MOT_3M )
		MOTOR3 = ON;
	else
		MOTOR3 = OFF;
		
	// Check Motor 4...
	if( MOT_4P ^ MOT_4M )
		MOTOR4 = ON;
	else
		MOTOR4 = OFF;
		
	// Check Motor 5...
	if( MOT_5P ^ MOT_5M )
		MOTOR5 = ON;
	else
		MOTOR5 = OFF;
		
	// Check Motor 6...
	if( MOT_6P ^ MOT_6M )
		MOTOR6 = ON;
	else
		MOTOR6 = OFF;
		
	// Check Motor 7...
	if( MOT_7P ^ MOT_7M )
		MOTOR7 = ON;
	else
		MOTOR7 = OFF;
		
	// Check Motor 8...
	if( MOT_8P ^ MOT_8M )
		MOTOR8 = ON;
	else
		MOTOR8 = OFF;
		
}




/*****************************************************************************/
void mainCheckStallSwitches( void )
/*=============================================================================
Inputs  -  
Outputs -

Description:
Debounce and check the stall switches...
If a stall switch is ON then set the "STALL" flag for that motor
*******************************************************************************/
{

// Start out by debouncing the switches
	if( STALLPORT != iOldSTALLPort  )
	{
		// If the port is different, see if it's different and stable for the debounce period
		if( iPortBouncingTimer > STALLDEBOUNCEPERIOD )
		{
			iSTALLSWITCHES = STALLPORT ^ 0xFF;
			iOldSTALLPort = STALLPORT;
		}
	}
	else
	{
		iPortBouncingTimer = 0;
	}

}



/*****************************************************************************/
void mainOutputHalls( void )
/*=============================================================================
Inputs  -  
Outputs -

Description:
Debounce and check the stall switches...
If a stall switch is ON then set the "STALL" flag for that motor
*******************************************************************************/
{
	if( (MOTOR1 == ON) && (!(iSTALLSWITCHES & 1)) )
		iHALLEnable |= 128;
	else
		iHALLEnable &= 0x7F;

	if( (MOTOR2 == ON) && (!(iSTALLSWITCHES & 2)) )
		iHALLEnable |= 64;
	else
		iHALLEnable &= 0xBF;

	if( (MOTOR3 == ON) && (!(iSTALLSWITCHES & 4)) )
		iHALLEnable |= 32;
	else
		iHALLEnable &= 0xDF;

	if( (MOTOR4 == ON) && (!(iSTALLSWITCHES & 8)) )
		iHALLEnable |= 16;
	else
		iHALLEnable &= 0xEF;

	if( (MOTOR5 == ON) && (!(iSTALLSWITCHES & 0x10)) )
		iHALLEnable |= 8;
	else
		iHALLEnable &= 0xF7;

	if( (MOTOR6 == ON) && (!(iSTALLSWITCHES & 0x20)) )
		iHALLEnable |= 4;
	else
		iHALLEnable &= 0xFB;

	if( (MOTOR7 == ON) && (!(iSTALLSWITCHES & 0x40)) )
		iHALLEnable |= 2;
	else
		iHALLEnable &= 0xFD;

	if( (MOTOR8 == ON) && (!(iSTALLSWITCHES & 0x80)) )
		iHALLEnable |= 1;
	else
		iHALLEnable &= 0xFE;


}

/*****************************************************************************/
/*****************************************************************************/
//***            INITIALIZATION CODE								      ***//
/*****************************************************************************/
/*****************************************************************************/
void main_InitRegisters( void )
/*=============================================================================
Inputs  -  
Outputs -

Description:

*******************************************************************************/
{
	unsigned int i;


	//=============================================================================
	// Set up the Capture/Compare registers
	//=============================================================================
	// Timer1 control...
	OpenTimer1(TIMER_INT_OFF & T1_16BIT_RW & T1_SOURCE_INT & T1_PS_1_1 &T1_OSC1EN_OFF & T1_SYNC_EXT_ON );

	IPR1bits.CCP1IP = 0;	// Compare 1 low priority
	PIE1bits.CCP1IE = 0;	// Disable the interrupt for now
		
	IPR2bits.CCP2IP = 0;	// Compare 2 low priority
	PIE2bits.CCP2IE = 0;	// Disable the interrupt for now
		
//	T3CONbits.T3CCP2 = 0;	// Selects clock 1&2 for all CCPs
//	T3CONbits.T3CCP1 = 0;

	// Set up Compare 1 at 1ms intervals...
	i = ReadTimer1();
	CCPR2 = i + C_ONEMS; // 1ms @16MHz
	CCP1CON = 0x0A; // Compare mode, Interrupt, no output toggle

	// Set up Compare 2 at 10ms intervals...
	i = ReadTimer1();
	CCPR2 = i + C_HUNDUS; // 100us @16MHz
	CCP2CON = 0x0A; // Compare mode, Interrupt, no output toggle

	PIR1bits.CCP1IF = 0;
	PIE1bits.CCP1IE = 1; // Enable the compare interrupt

	PIR2bits.CCP2IF = 0;
	PIE2bits.CCP2IE = 1; // Enable the compare interrupt


}


/*****************************************************************************/
void main_InitPorts( void )
/*=============================================================================
Inputs  -  
Outputs -

Description:

*******************************************************************************/
{
	// Initialize PORT A
	PORTA = 0;		// Initialize the port to all 0s
	TRISA = 0xFF;	// PortA Are all inputs for now

	// Initialize PORT B
	PORTB = 0x01;	// Turn all of the LED display segments OFF
	TRISB = 0x7F;	// B0= out, B1..6 = in

	// Initialize PORT C
	PORTC = 0x00;	// Clear PORTC
	TRISC = 0xFF;	//  Inputs

	// Initialize PORT D
	PORTD = 0x00;	// LEDs off on PICdem board
	TRISD = 0xFF;	// All outputs

	// Initialize PORT E
	PORTE = 0x00;	// 
	TRISE = 0x00;	// Outputs

	// Initialize PORT F
	PORTF = 0x00;	// 
	TRISF = 0x00;	// All outputs

	// Initialize PORT G
	PORTG = 0x00;	// 
	TRISG = 0x00;	// All Inputs

	// Initialize PORT H
	PORTH = 0x00;	// 
	TRISH = 0xFF;	// All Inputs

	// Initialize PORT J
	PORTJ = 0xFF;	// 
	TRISJ = 0xFF;	// All Inputs
}




/*****************************************************************************/
void main_InitA2D( void )
/*=============================================================================
Inputs  -  
Outputs -

Description:

*******************************************************************************/
{
	ADCON1 = 0x0C; // Vref internal, Only use channel 0 & 1 & 2
	ADCON2 = 0xA1;	// 10100001
	ADCON0bits.ADON = 1; // Turn the A2D ON
}







