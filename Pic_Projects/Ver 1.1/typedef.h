
#ifndef __TYPEDEF_H
#define __TYPEDEF_H

typedef unsigned char T_UBYTE;
typedef signed char T_SBYTE;
typedef unsigned int T_UWORD;
typedef signed int T_SWORD;
typedef unsigned long T_ULONG;

typedef union 
{
	T_ULONG l;
	T_UBYTE b[4];
} T_LONGBYTES;


typedef union 
{
	T_UWORD w;
	T_UBYTE b[2];
} T_WORDBYTES;

#endif
